$(function() {
	$.get("http://ipinfo.io", function(response) {
		$("#customer_address_phone").intlTelInput({
			defaultCountry: response.country.toLowerCase(),
			validationScript: "/assets/template6/js/libs/isValidNumber.js"
		});
		$("#shipping_phone").intlTelInput({
			defaultCountry: response.country.toLowerCase(),
			validationScript: "/assets/template6/js/libs/isValidNumber.js"
		});
		$("#customer_address_cellPhone").intlTelInput({
			defaultCountry: response.country.toLowerCase(),
			validationScript: "/assets/template6/js/libs/isValidNumber.js"
		});
		$("#shipping_cellPhone").intlTelInput({
			defaultCountry: response.country.toLowerCase(),
			validationScript: "/assets/template6/js/libs/isValidNumber.js"
		});
		// Trigger phone change
		$("#customer_address_phone").change();
		$("#customer_address_cellPhone").change();
		$("#shipping_phone").change();
		$("#shipping_cellPhone").change();
	}, "jsonp");
});
