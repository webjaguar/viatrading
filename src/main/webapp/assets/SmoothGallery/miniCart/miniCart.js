var myVerticalSlide;
window.addEvent('domready', function() {
  myVerticalSlide = new Fx.Slide('vertical_slide', {duration: 300}).hide();
  $('v_slideout').addEvent('click', function(e){
    e.stop();
    myVerticalSlide.slideOut();
  });
  $('vertical_slide').setStyle('display', 'block');
});
function callCart(delayTime) {
  var req1=new Request.HTML({
     method:'get',
     onComplete: function(response) {
        slideCart(delayTime);
     },
     onSuccess: function(html) {
        $('floatCart').set('text', '');
        $('floatCart').adopt(html);
     },
     onFailure: function(f) {
        alert('The request failed. ');
     },
     update: $('floatCart'),
     url: '/viewMiniCart.jhtm'
  }).send();
}
function slideCart(delayTime) {
        myVerticalSlide.slideIn.delay(delayTime, myVerticalSlide);
        myVerticalSlide.slideOut.delay(6000, myVerticalSlide);
}
function callCartQV(delayTime) {
	  var req1=new Request.HTML({
	     method:'get',
	     onSuccess: function(html) {
	        $('floatCart').set('text', '');
	        $('floatCart').adopt(html);
	     },
	     onRequest: function() {
	     },
	     onComplete: function(response) {
	        slideCart(delayTime);
	     },
	     onFailure: function(f) {
	        alert('The request failed. ');
	     },
	     update: $('floatCart'),
	     url: '/viewMiniCart.jhtm'
	  }).send();
}
function callCartShort() {
	  var req=new Request({
	     method:'get',
	     onRequest:function(){
	     },
	     onComplete: function(response) {
	        //$('miniCartView').innerHTML="<span style='color:#c00000;'>"+ response +"</span>";
	     },
	     url: '/viewMiniCartLink.jhtm'
	  }).send();
}