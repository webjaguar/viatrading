$(function() {
	// Off Canvas
	$('[data-toggle="offcanvas"]').click(function() {
		$(".row-offcanvas").toggleClass("active")
	});
	
	if($('[data-toggle="offcanvas"]').length) {
		$("#contentWrapper").swipe({
			swipeLeft: function() {
				$(".row-offcanvas").removeClass("active");
			},
			swipeRight: function() {
				$(".row-offcanvas").addClass("active");
			}
		});
	}

	// init Bootstrap tooltip
	$('[data-toggle="tooltip"]').tooltip({
		container: "body"
	});

	// init tab collaps for responsive tabs
	$("#product-tabs").tabCollapse();

	// init slider
	$("#slider").owlCarousel({
		theme: "slider-theme",
		autoPlay : 3000,
		singleItem : true,
		navigation : true,
		navigationText: false,
		slideSpeed : 300,
		autoHeight : true,
		paginationSpeed : 400,
		stopOnHover : false
	});

	// init products carousel
	$(".products_carousel").owlCarousel({
		theme: "carousel-theme",
		items : 5,
		navigation : true,
		pagination: false,
		navigationText: false,
		slideSpeed : 500,
		scrollPerPage: true
	});

	$(".paymentMethod.radio input[type='radio']").click(function() {
		if($("#creditCard_radio").is(':checked')) {
			$(".creditCardBox").slideDown();
		}
		else {
			$(".creditCardBox").slideUp();
		}
	});
});

$(function() {
	$("#customer_address_country").change(function() {
		if ($(this).val() === "US") {
			$("#customer_address_state").removeAttr("disabled").show();
			$("#customer_address_ca_province").hide().attr("disabled", "disabled");
			$("#customer_address_stateProvince").hide().attr("disabled", "disabled");
			$("#customer_address_stateProvinceNA").hide().attr("disabled", "disabled");
			$("#stateProvinceNA").hide();
		} else if ($(this).val() === "CA") {
			$("#customer_address_state").hide().attr("disabled", "disabled");
			$("#customer_address_ca_province").removeAttr("disabled").show();
			$("#customer_address_stateProvince").hide().attr("disabled", "disabled");
			$("#customer_address_stateProvinceNA").hide().attr("disabled", "disabled");
			$("#stateProvinceNA").hide();
		} else {
			$("#customer_address_state").hide().attr("disabled", "disabled");
			$("#customer_address_ca_province").hide().attr("disabled", "disabled");
			$("#customer_address_stateProvince").removeAttr("disabled").show();
			$("#customer_address_stateProvinceNA").removeAttr("disabled").removeAttr("checked").show();
			$("#stateProvinceNA").show();
		}

		// sync county and phone
		$("#customer_address_phone").intlTelInput("selectCountry", $(this).val().toLowerCase());
		$("#customer_address_cellPhone").intlTelInput("selectCountry", $(this).val().toLowerCase());

		// $('#register_newCustomer_form').data('bootstrapValidator').updateStatus('customer_address_phone', 'NOT_VALIDATED', null).validateField('customer_address_phone');
		
	});

	$("#shipping_country").change(function() {
		if ($(this).val() === "US") {
			$("#shipping_state").removeAttr("disabled").show();
			$("#shipping_ca_province").hide().attr("disabled", "disabled");
			$("#shipping_stateProvince").hide().attr("disabled", "disabled");
			$("#shipping_stateProvinceNA").hide().attr("disabled", "disabled");
			$("#stateProvinceNA_shipping").hide();
		} else if ($(this).val() === "CA") {
			$("#shipping_state").hide().attr("disabled", "disabled");
			$("#shipping_ca_province").removeAttr("disabled").show();
			$("#shipping_stateProvince").hide().attr("disabled", "disabled");
			$("#shipping_stateProvinceNA").hide().attr("disabled", "disabled");
			$("#stateProvinceNA_shipping").hide();
		} else {
			$("#shipping_state").hide().attr("disabled", "disabled");
			$("#shipping_ca_province").hide().attr("disabled", "disabled");
			$("#shipping_stateProvince").removeAttr("disabled").show();
			$("#shipping_stateProvinceNA").removeAttr("disabled").removeAttr("checked").show();
			$("#stateProvinceNA_shipping").show();
		}

		// sync county and phone
		$("#shipping_phone").intlTelInput("selectCountry", $(this).val().toLowerCase());
		$("#shipping_cellPhone").intlTelInput("selectCountry", $(this).val().toLowerCase());
	});

	// sync county and phone
	$("#customer_address_phone").change(function() {
		$("#customer_address_country").val(($(this).intlTelInput("getSelectedCountryData").iso2).toUpperCase());
		$("#customer_address_cellPhone").intlTelInput("selectCountry", $(this).intlTelInput("getSelectedCountryData").iso2);
	});
	$("#shipping_phone").change(function() {
		$("#shipping_country").val(($(this).intlTelInput("getSelectedCountryData").iso2).toUpperCase());
		$("#shipping_cellPhone").intlTelInput("selectCountry", $(this).intlTelInput("getSelectedCountryData").iso2);
	});
	$("#customer_address_cellPhone").change(function() {
		$("#customer_address_country").val(($(this).intlTelInput("getSelectedCountryData").iso2).toUpperCase());
		$("#customer_address_phone").intlTelInput("selectCountry", $(this).intlTelInput("getSelectedCountryData").iso2);
	});
	$("#shipping_cellPhone").change(function() {
		$("#shipping_country").val(($(this).intlTelInput("getSelectedCountryData").iso2).toUpperCase());
		$("#shipping_phone").intlTelInput("selectCountry", $(this).intlTelInput("getSelectedCountryData").iso2);
	});

	$("#customer_address_stateProvinceNA").click(function() {
		if($(this).is(":checked")) {
			$("#customer_address_stateProvince").attr("disabled", "disabled");
		}
		else {
			$("#customer_address_stateProvince").removeAttr("disabled");
		}
	});
	$("#shipping_stateProvinceNA").click(function() {
		if($(this).is(":checked")) {
			$("#shipping_stateProvince").attr("disabled", "disabled");
		}
		else {
			$("#shipping_stateProvince").removeAttr("disabled");
		}
	});
	
	// Trigger country change
	$("#customer_address_country").change();
	$("#shipping_country").change();
	
});



$(function() {
	$("#login_existingCustomer_form").bootstrapValidator({
		feedbackIcons: {
			valid: 'glyphicon glyphicon-ok',
			invalid: 'glyphicon glyphicon-remove',
			validating: 'glyphicon glyphicon-refresh'
		},
		live: "submitted",
		message: "This value is not valid",
		submitButtons: 'button[type="submit"]',
		fields: {
			username: {
				trigger: "blur",
				validators: {
					notEmpty: {
						message: "The email is required."
					},
					emailAddress: {
						message: "This is not a valid email address."
					}
				}
			},
			password: {
				trigger: "blur",
				validators: {
					notEmpty: {
						message: "The password is required."
					}
				}
			}
		}
	});
});


(function($) {
	$.fn.bootstrapValidator.validators.libphonenumber = {
		html5Attributes: {
			message: 'message'
		},
		validate: function(validator, $field, options) {
			var value = $field.val();
			if (value == '') {
				return true;
			}

			if ($field.intlTelInput("isValidNumber")) {
				return true;
			} else {
				return false;
			}

		}
	};
}(window.jQuery));


$(function() {
	$.get("http://ipinfo.io", function(response) {
		$("#customer_address_phone").intlTelInput({
			defaultCountry: response.country.toLowerCase(),
			validationScript: "/assets/template6/js/libs/isValidNumber.js"
		});
		$("#shipping_phone").intlTelInput({
			defaultCountry: response.country.toLowerCase(),
			validationScript: "/assets/template6/js/libs/isValidNumber.js"
		});
		$("#customer_address_cellPhone").intlTelInput({
			defaultCountry: response.country.toLowerCase(),
			validationScript: "/assets/template6/js/libs/isValidNumber.js"
		});
		$("#shipping_cellPhone").intlTelInput({
			defaultCountry: response.country.toLowerCase(),
			validationScript: "/assets/template6/js/libs/isValidNumber.js"
		});
		// Trigger phone change
		$("#customer_address_phone").change();
		$("#customer_address_cellPhone").change();
		$("#shipping_phone").change();
		$("#shipping_cellPhone").change();
	}, "jsonp");
});


$(function() {
	$("#register_newCustomer_form").bootstrapValidator({
		feedbackIcons: {
			valid: 'glyphicon glyphicon-ok',
			invalid: 'glyphicon glyphicon-remove',
			validating: 'glyphicon glyphicon-refresh'
		},
		live: "enabled",
		message: "This value is not valid",
		submitButtons: 'button[type="submit"]',
		fields: {
			customer_username: {
				selector: "#customer_username",
				trigger: "blur",
				validators: {
					notEmpty: {
						message: "Email is required."
					},
					emailAddress: {
						message: "This is not a valid email address."
					}
				}
			},
			customer_password: {
				selector: "#customer_password",
				trigger: "blur",
				validators: {
					notEmpty: {
						message: "Password is required."
					},
					identical: {
						field: "confirm_password",
						message: "Passwords doesn't match."
					},
					stringLength: {
						min: 5,
						message: "The password should be at least 5 characters."
					}
				}
			},
			confirm_password: {
				selector: "#confirm_password",
				trigger: "blur",
				validators: {
					notEmpty: {
						message: "Confirm Password is required."
					},
					identical: {
						field: "customer_password",
						message: "Passwords doesn't match."
					}
				}
			},
			customer_address_firstName: {
				selector: "#customer_address_firstName",
				trigger: "blur",
				validators: {
					notEmpty: {
						message: "First Name is required."
					}
				}
			},
			customer_address_lastName: {
				selector: "#customer_address_lastName",
				trigger: "blur",
				validators: {
					notEmpty: {
						message: "Last Name is required."
					}
				}
			},
			customer_address_country: {
				selector: "#customer_address_country",
				trigger: "blur",
				validators: {
					notEmpty: {
						message: "Country is required."
					}
				}
			},
			customer_address_addr1: {
				selector: "#customer_address_addr1",
				trigger: "blur",
				validators: {
					notEmpty: {
						message: "Address is required."
					}
				}
			},
			customer_address_city: {
				selector: "#customer_address_city",
				trigger: "blur",
				validators: {
					notEmpty: {
						message: "City is required."
					}
				}
			},
			customer_address_stateProvince: {
				selector: "#customer_address_stateProvince",
				trigger: "blur",
				validators: {
					notEmpty: {
						message: "State/Province is required."
					}
				}
			},
			// customer_address_zip: {
			// 	selector: "#customer_address_zip",
			// 	trigger: "blur",
			// 	validators: {
			// 		notEmpty: {
			// 			message: "Zip/Postal Code is required."
			// 		}
			// 	}
			// },
			customer_address_phone: {
				selector: "#customer_address_phone",
				trigger: "blur",
				validators: {
					notEmpty: {
						message: "Phone is required."
					},
					libphonenumber: {
						message: 'Invalid phone number'
					}
				}
			},
			customer_address_cellPhone: {
				selector: "#customer_address_cellPhone",
				trigger: "blur",
				validators: {
					libphonenumber: {
						message: 'Invalid phone number'
					}
				}
			}
		}
	});
});
