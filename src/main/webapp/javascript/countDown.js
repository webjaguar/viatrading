function countdown() {
	var endDate = document.getElementById("endDateValue").value;

	var xmas = new Date(endDate);
	var now = new Date();
	// console.log("endDate: "+endDate+"now: " + now );
	var timeDiff = xmas.getTime() - now.getTime();
	if (timeDiff <= 0) {
		// console.log("cddt");
		clearTimeout(timer);
		document.location.reload(true)
		// $("#counterWrapper").text("Gone");
		return;
	}
	var second = Math.floor(timeDiff / 1000);
	var minutes = Math.floor(second / 60);
	var hours = Math.floor(minutes / 60);
	var days = Math.floor(hours / 24);
	hours %= 24;
	minutes %= 60;
	second %= 60;
	
	document.getElementById("daysBox").innerHTML = days;
	document.getElementById("hoursBox").innerHTML = (hours < 10) ? "0" + hours
			: hours;
	;
	document.getElementById("minsBox").innerHTML = (minutes < 10) ? "0"
			+ minutes : minutes;
	document.getElementById("secsBox").innerHTML = (second < 10) ? "0" + second
			: second;
	var timer = setTimeout('countdown()', 1000);
}