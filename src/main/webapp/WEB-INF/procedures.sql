delimiter ;;
CREATE PROCEDURE `familyTree`( IN root INT, IN direction CHAR(4) )
BEGIN
	DROP TABLE IF EXISTS family_tree;
	CREATE TABLE family_tree (
		user_id INT PRIMARY KEY,
		level SMALLINT
	) ENGINE=HEAP;
	INSERT INTO family_tree VALUES (root, 1);
	IF direction = 'up' THEN
		WHILE ROW_COUNT() > 0 DO
			INSERT IGNORE INTO family_tree
				SELECT parent, NULL FROM users, family_tree WHERE users.id = family_tree.user_id;
		END WHILE;
	ELSE
		WHILE ROW_COUNT() > 0 DO
			INSERT IGNORE INTO family_tree
				SELECT id, level+1 FROM users, family_tree WHERE users.parent = family_tree.user_id;
		END WHILE;
	END IF;	
	SELECT user_id, level, id, parent, username  FROM family_tree LEFT JOIN users ON family_tree.user_id = users.id;
	DROP TABLE family_tree;
END
 ;;
delimiter ;


DROP PROCEDURE IF EXISTS `ListReached`;
delimiter ;;
CREATE PROCEDURE `ListReached`(IN root INT, IN list_type INT)
BEGIN
  DECLARE rows SMALLINT DEFAULT 0;
  DROP TABLE IF EXISTS reached;
  CREATE TABLE reached (
    node_id INT PRIMARY KEY
  ) ENGINE=HEAP;
  INSERT INTO reached VALUES (root );
  SET rows = ROW_COUNT();
  WHILE rows > 0 DO
  IF list_type <= 0 THEN
    INSERT IGNORE INTO reached
      SELECT DISTINCT child_id
      FROM edges AS e
      INNER JOIN reached AS p ON e.parent_id = p.node_id;
   END IF;
    SET rows = ROW_COUNT();
   IF list_type >= 0 THEN
    INSERT IGNORE INTO reached
      SELECT DISTINCT parent_id
      FROM edges AS e
      INNER JOIN reached AS p ON e.child_id = p.node_id;
   END IF;
    SET rows = rows + ROW_COUNT();
  END WHILE;
  SELECT first_name, last_name, reached.node_id FROM reached, address,users, affiliates WHERE affiliates.node_id = reached.node_id and users.default_address_id = address.id and address.userid = reached.node_id;
  DROP TABLE reached;
END
 ;;
delimiter ;
