
CREATE TABLE truckload_processed (
  id int(10) unsigned NOT NULL auto_increment,
  name varchar(80) default NULL,
  date_created varchar(250) default NULL,
  date_last_update varchar(250) default NULL,
  status char(3) default NULL,
  custom1 varchar(80) default NULL,
  custom2 varchar(80) default NULL,
  custom3 varchar(80) default NULL,
  custom4 varchar(250) default NULL,
  ext_msrp double(10,2) default NULL,
  created_by int(10) NOT NULL default '1',
  pt varchar(250) default NULL,
  type varchar(250) DEFAULT '',
  team_hours double DEFAULT '0',
  team_cost double DEFAULT '0',
  num_of_team_members int(11) DEFAULT '0',
  date_started varchar(80) DEFAULT NULL,
  date_ended varchar(80) DEFAULT NULL,
  PRIMARY KEY  (id)
);

CREATE TABLE truckload_original (
  id int(10) unsigned NOT NULL auto_increment,
  name varchar(80) default NULL,
  original_sku varchar(80) default NULL,
  original_qty int(11) default NULL,
  process_id int(11) default NULL,
  value double default NULL,
  packing varchar(80) default NULL,
  field15 varchar(80) default NULL,
  PRIMARY KEY  (id)
);

CREATE TABLE truckload_produced (
  id int(11) NOT NULL auto_increment,
  name varchar(80) default NULL,
  produced_sku varchar(80) default NULL,
  produced_qty int(11) default NULL,
  description varchar(200) default NULL,
  prod_condition varchar(200) default NULL,
  no_of_units varchar(200) default NULL,
  process_id int(11) default NULL,
  produced_unit_price double default '0',
  packing varchar(80) default NULL,
  num_of_pallets varchar(80) default NULL,
  PRIMARY KEY  (id)
);

insert into crm_contact_customer_map
values
("getField1Set", "getCrmContactFields_1Set"),
("getField2Set", "getCrmContactFields_2Set"),
("getField3Set", "getCrmContactFields_3Set"),
("getField4Set", "getCrmContactFields_4Set"),
("getField5Set", "getCrmContactFields_5Set"),
("getField6Set", "getCrmContactFields_6Set"),
("getField7Set", "getCrmContactFields_7Set"),
("getField8Set", "getCrmContactFields_8Set"),
("getField9Set", "getCrmContactFields_9Set"),
("getField10Set", "getCrmContactFields_10Set"),
("getField11Set", "getCrmContactFields_11Set"),
("getField12Set", "getCrmContactFields_12Set"),
("getField13Set", "getCrmContactFields_13Set"),
("getField14Set", "getCrmContactFields_14Set"),
("getField15Set", "getCrmContactFields_15Set"),
("getField16Set", "getCrmContactFields_16Set"),
("getField17Set", "getCrmContactFields_17Set"),
("getField18Set", "getCrmContactFields_18Set"),
("getField19Set", "getCrmContactFields_19Set"),
("getField20Set", "getCrmContactFields_20Set");




