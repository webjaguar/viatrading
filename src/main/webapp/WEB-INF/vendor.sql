/* techdata */
CREATE TABLE techdata_category (
  grp varchar(2),
  cat varchar(2),
  sub varchar(2),
  descr varchar(50),
  cat_id INT unsigned,
  price_change double(10,2) unsigned,
  created datetime,
  last_modified datetime,
  primary key (grp, cat, sub),
  FOREIGN KEY (cat_id) REFERENCES category (id)
  	ON DELETE SET NULL
  	ON UPDATE CASCADE
) ENGINE=InnoDB;

CREATE TABLE techdata_manuf (
  vend_code varchar(6),
  vend_name varchar(25),
  restricted bool NOT NULL,
  created datetime,
  last_modified datetime,
  primary key (vend_code)
);

/* ingram micro */
CREATE TABLE ingrammicro_category (
  cat varchar(2),
  sub varchar(2),
  descr varchar(50),
  cat_id INT unsigned,
  price_change double(10,2) unsigned,
  created datetime,
  last_modified datetime,
  price_change_base varchar(20),  
  primary key (cat, sub),
  FOREIGN KEY (cat_id) REFERENCES category (id)
  	ON DELETE SET NULL
  	ON UPDATE CASCADE
) ENGINE=InnoDB;

/* synnex */
CREATE TABLE synnex_category (
  cat varchar(3),
  sub varchar(3),
  sub_sub varchar(3),
  descr varchar(50),
  cat_id INT unsigned,
  price_change double(10,2) unsigned,
  created datetime,
  last_modified datetime,
  primary key (cat, sub, sub_sub),
  FOREIGN KEY (cat_id) REFERENCES category (id)
  	ON DELETE SET NULL
  	ON UPDATE CASCADE
) ENGINE=InnoDB;

/* dsi */
CREATE TABLE dsi_category (
  manuf varchar(50),
  cat varchar(50),
  sub varchar(50),
  cat_id INT unsigned,
  price_change double(10,2) unsigned,
  created datetime,
  last_modified datetime,
  price_change_base varchar(20),
  primary key (manuf, cat, sub),
  FOREIGN KEY (cat_id) REFERENCES category (id)
  	ON DELETE SET NULL
  	ON UPDATE CASCADE
) ENGINE=InnoDB;

/* asi TO BE REMOVE
CREATE TABLE asi_category (
  supplier varchar(50),
  asi varchar(20),
  cat varchar(50),
  sub varchar(50),
  product_category varchar(150),
  cat_id INT unsigned,
  price_change double(10,2) unsigned,
  created datetime,
  last_modified datetime,
  primary key (supplier, cat, sub),
  FOREIGN KEY (cat_id) REFERENCES category (id)
  	ON DELETE SET NULL
  	ON UPDATE CASCADE
) ENGINE=InnoDB;
*/

/* kole imports */
CREATE TABLE koleimports_category (
  cat varchar(50),
  sub varchar(50),
  cat_id INT unsigned,
  price_change double(10,2) unsigned,
  created datetime,
  last_modified datetime,
  primary key (cat, sub),
  FOREIGN KEY (cat_id) REFERENCES category (id)
  	ON DELETE SET NULL
  	ON UPDATE CASCADE
) ENGINE=InnoDB;

/* dsdi */
CREATE TABLE dsdi_category (
  cat varchar(50),
  sub varchar(50),
  cat_id INT unsigned,
  price_change double(10,2) unsigned,
  created datetime,
  last_modified datetime,
  primary key (cat, sub),
  FOREIGN KEY (cat_id) REFERENCES category (id)
  	ON DELETE SET NULL
  	ON UPDATE CASCADE
) ENGINE=InnoDB;

CREATE TABLE webjaguar_category (
  breadcrumb varchar(255),
  cat varchar(150),
  cat_id INT unsigned,
  created datetime,
  last_modified datetime,
  primary key (cat, breadcrumb),
  FOREIGN KEY (cat_id) REFERENCES category (id)
  	ON DELETE SET NULL
  	ON UPDATE CASCADE
) ENGINE=InnoDB;

