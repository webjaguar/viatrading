CREATE TABLE category (
  id INT unsigned NOT NULL auto_increment,
  external_id  varchar(25),
  name varchar(150) NOT NULL,
  parent INT unsigned default NULL,
  rank INT unsigned default NULL,
  html_code mediumtext default NULL,
  footer_html_code mediumtext,
  rightbar_top_html_code mediumtext,
  rightbar_bottom_html_code mediumtext,
  leftbar_top_html_code mediumtext,
  leftbar_bottom_html_code mediumtext,
  leftbar_type VARCHAR(10),
  protected_html_code mediumtext default NULL,
  url varchar(255),  
  url_es varchar(255),  
  url_target varchar(7),  
  redirect_301 bool NOT NULL,
  created datetime,
  last_modified datetime,
  num_of_clicks INT unsigned NOT NULL default 0,
  subcat_cols tinyint unsigned NOT NULL default 1,
  subcat_levels tinyint unsigned,
  subcat_location varchar(1), 
  show_subcats bool NOT NULL,
  link_type ENUM('visible', 'nonlink', 'hidden') not null default 'visible',
  display_mode varchar(20),
  display_mode_grid varchar(20),
  hide_header bool NOT NULL,
  hide_topbar bool NOT NULL,
  hide_leftbar bool NOT NULL,
  hide_rightbar bool NOT NULL,
  hide_footer bool NOT NULL,  
  hide_breadcrumbs bool NOT NULL,
  head_tag text,
  product_per_page INT unsigned,
  protected_level BIT(64) NOT NULL default b'0',
  layout_id tinyint unsigned NOT NULL default 0,
  cat_ids TEXT,
  sortby VARCHAR(20),
  start_date DATETIME,
  end_date DATETIME,
  search_limit INT, 
  productfield_search bool NOT NULL,
  productfield_search_type VARCHAR(5) NOT NULL default '0',
  productfield_search_position VARCHAR(10) NOT NULL default 'left',
  field_1 VARCHAR(512),
  field_2 VARCHAR(512),
  field_3 VARCHAR(512),
  field_4 VARCHAR(512),
  field_5 VARCHAR(512),
  feed VARCHAR(30) DEFAULT NULL,
  salestag_title VARCHAR(100) default NULL,
  show_on_search bool NOT NULL,
  shared bool NOT NULL default 1,
  setType VARCHAR(15) NOT NULL default 'intersection',
  nem_industry VARCHAR(25),
  nem_category VARCHAR(40),
  sitemap_priority double(2,1) unsigned NOT NULL default 0.5,
  image_url VARCHAR(255) DEFAULT NULL,
  image_url_alt_name VARCHAR(255) DEFAULT NULL,
  parent_child_display tinyint unsigned NOT NULL default 0,
  show_full_width tinyint NOT NULL default 0,
  PRIMARY KEY  (id),
  FOREIGN KEY (parent) REFERENCES category (id)
  	ON DELETE CASCADE
  	ON UPDATE CASCADE
) ENGINE=InnoDB;

CREATE TABLE category_i18n (
  category_id INT unsigned NOT NULL,
  lang varchar(2) NOT NULL,
  i18n_name varchar(150) NOT NULL,
  i18n_html_code mediumtext,
  i18n_protected_html_code mediumtext,
  i18n_head_tag text,
  PRIMARY KEY (category_id, lang),
  FOREIGN KEY (category_id) REFERENCES category (id)
  	ON DELETE CASCADE
  	ON UPDATE CASCADE  
) ENGINE=InnoDB;

CREATE TABLE promos (
  promo_id INT unsigned NOT NULL AUTO_INCREMENT,
  title varchar(100) NOT NULL,
  discount double(10,2) UNSIGNED,
  discount_type VARCHAR(20) default 'order',
  percent bool NOT NULL,
  onetime_use_per_customer bool NOT NULL,
  promo_for_new_customers_only bool NOT NULL,
  onetime_use_only bool NOT NULL default 0,
  isDynamic bool NOT NULL default 0,
  userid INT unsigned default NULL,
  sku VARCHAR(50) unsigned default NULL,
  category_ids VARCHAR(50) default NULL,
  created_by VARCHAR(45)  default NULL,
  created_on datetime  default NULL,
  min_order double(10,2) UNSIGNED,
  min_order_per_brand double(10,2) UNSIGNED,
  min_quantity INT default 0,
  start_date datetime,
  end_date datetime,
  prefix VARCHAR(50)  default NULL,
  rank INT unsigned default NULL,
  html_code mediumtext,
  product_availability bool default 0,
  groups varchar(100) default NULL,
  group_note varchar(100) default NULL,
  UNIQUE KEY (title),
  PRIMARY KEY(promo_id)
) ENGINE = InnoDB;

CREATE TABLE deals (
  deal_id INT unsigned NOT NULL AUTO_INCREMENT,
  title varchar(100) NOT NULL,
  buy_sku varchar(20),
  buy_amount double(10,2) UNSIGNED,
  buy_quantity INT default '1',
  get_sku varchar(20),
  get_quantity INT default '1',
  discount double(10,2) UNSIGNED,
  discount_type bool NOT NULL,
  start_date datetime,
  end_date datetime,
  html_code mediumtext default NULL,
  is_parent_sku tinyint NOT NULL default 0,
  UNIQUE KEY (title),
  PRIMARY KEY(deal_id)
) ENGINE = InnoDB;

CREATE TABLE via_deals (
  deal_id INT unsigned NOT NULL AUTO_INCREMENT,
  title varchar(100) NOT NULL,
  parent_sku varchar(20),
  buy_amount double(10,2) UNSIGNED,
  buy_quantity INT default '1',
  trigger_threshold double(10,2) UNSIGNED,
  trigger_type bool NOT NULL,
  discount double(10,2) UNSIGNED,
  discount_type bool NOT NULL,
  start_date datetime,
  end_date datetime,
  html_code mediumtext default NULL,
  UNIQUE KEY (title),
  PRIMARY KEY(deal_id)
) ENGINE = InnoDB;

CREATE TABLE condition_promo (
  promo_id INT unsigned NOT NULL,  
  type VARCHAR(20),
  value VARCHAR(30),
  operator VARCHAR(20)
) ENGINE = MyISAM;

CREATE TABLE condition_shipping (
  shipping_id INT unsigned NOT NULL,  
  type VARCHAR(20),
  value VARCHAR(20),
  operator VARCHAR(20)
) ENGINE = MyISAM;

CREATE TABLE sales_tags (
  tag_id INT unsigned NOT NULL AUTO_INCREMENT,
  title varchar(100),
  discount double(10,2) UNSIGNED,
  percent bool NOT NULL default 1,   
  start_date datetime,
  end_date datetime,
  PRIMARY KEY(tag_id)
) ENGINE = InnoDB;

CREATE TABLE mail_in_rebates (
  rebate_id INT unsigned NOT NULL AUTO_INCREMENT,
  title varchar(100),
  discount double(10,2) UNSIGNED,
  percent bool NOT NULL default 1,   
  start_date datetime,
  end_date datetime,
  html_code mediumtext default NULL,
  PRIMARY KEY(rebate_id)
) ENGINE = InnoDB;

CREATE TABLE config_i18n (
  language varchar(2) NOT NULL default '',
  new_registration_id int unsigned default NULL,
  new_orders_id int unsigned default NULL,
  PRIMARY KEY (language)
) ENGINE = InnoDB;

CREATE TABLE mobile_carrier (
  id INT unsigned NOT NULL AUTO_INCREMENT ,
  carrier VARCHAR(45) NOT NULL ,
  server VARCHAR(255) NULL ,
  PRIMARY KEY (`id`)
)ENGINE = InnoDB;

INSERT INTO mobile_carrier (`carrier`, `server`) VALUES ('AT&T', '@txt.att.net');
INSERT INTO mobile_carrier (`carrier`, `server`) VALUES ('Verizon ', '@vtext.com');
INSERT INTO mobile_carrier (`carrier`, `server`) VALUES ('T-Mobile', '@tmomail.net');
INSERT INTO mobile_carrier (`carrier`, `server`) VALUES ('Sprint PCS', '@messaging.sprintpcs.com');
INSERT INTO mobile_carrier (`carrier`, `server`) VALUES ('Virgin Mobile', '@vmobl.com');
INSERT INTO mobile_carrier (`carrier`, `server`) VALUES ('US Cellular', '@email.uscc.net');
INSERT INTO mobile_carrier (`carrier`, `server`) VALUES ('Nextel', '@messaging.nextel.com');
INSERT INTO mobile_carrier (`carrier`, `server`) VALUES ('Boost', '@myboostmobile.com');
INSERT INTO mobile_carrier (`carrier`, `server`) VALUES ('Alltel', '@message.alltel.com');
INSERT INTO mobile_carrier (`carrier`, `server`) VALUES ('Alltel Wireless', '@text.wireless.alltel.com');
INSERT INTO mobile_carrier (`carrier`, `server`) VALUES ('Cricket Wireless', '@sms.mycricket.com');
INSERT INTO mobile_carrier (`carrier`, `server`) VALUES ('Sprint', '@messaging.sprintpcs.com');
INSERT INTO mobile_carrier (`carrier`, `server`) VALUES ('MetroPCS', '@mymetropcs.com');
INSERT INTO mobile_carrier (`carrier`, `server`) VALUES ('Boost Mobile', '@myboostmobile.com');



CREATE TABLE site_messages (
  message_id INT unsigned NOT NULL AUTO_INCREMENT,
  message_name VARCHAR(30),
  subject VARCHAR(80),
  message TEXT,
  html TINYINT(1) NOT NULL default 0,
  source_url VARCHAR(200),
  PRIMARY KEY(message_id),
  UNIQUE KEY (message_name)
) ENGINE = InnoDB;

INSERT INTO site_messages VALUES  
 (1,'New Registrations Message','Thank You for registering','Dear #firstname# #lastname#,\r\n\r\nThank you for registering from xxxxx. If you have any questions or comments, feel free to contact us at xxxxxx or email us at xxxxxx. We appreciate your business.\r\n\r\nYour Login Information:\r\nEmail Address: #email#\r\nPassword: #password#\r\n\r\nThank you,\r\nxxxxxxxx',0),
 (2,'New Orders Message','Order #order# from xxxxxx','Dear #firstname# #lastname#,\r\n\r\nThank you for ordering from xxxxx.  Your order has been received for processing.  If you have any questions or comments, feel free to contact us at xxxxxx or email us at xxxxxx. We appreciate your business.\r\n\r\nYour order number is #order#.\r\n\r\nYou can view a receipt of your order by this link\r\n#orderlink#\r\n\r\nThank you,\r\nxxxxxxxx',0),
 (3,'Shipped Orders Message','Order #order# from xxxxxx','Dear #firstname# #lastname#,\r\n\r\nThank you for ordering from xxxxx.  Your order has been shipped.  If you have any questions or comments, feel free to contact us at xxxxxx or email us at xxxxxx. We appreciate your business.\r\n\r\nYour order number is #order#.\r\nStatus: #status#\r\nTracking Number: #tracknum#\r\n\r\nYou can view a receipt of your order by this link\r\n#orderlink#\r\n\r\nThank you,\r\nxxxxxxxx',0),
 (4,'Purchase Orders Message','Purchase Order #ponumber# from xxxxxx','Dear #firstname# #lastname#,\r\n\r\nPlease Review this Purchase Order.\r\n\r\nThank you,\r\nxxxxxxxx',0),
 (5,'Abandon Cart','How Can We help?','<b>Dear #firstname# #lastname#,</b><p>On a recent visit to our website, We noticed you selected some products but didn`t complete your purchase. How can we help?  If you have any questions or comments, feel free to contact us at xxxxxx or email us at xxxxxx. We appreciate your business.</p><style>#cartWrapper img {width:100px;}#cartWrapper ul,#cartWrapper li {list-style-type: none;}#cartWrapper div.proInfoWrapper {float:left;}</style><div><ul id="cartWrapper">
#wjstart# <li>#image# <div class="proInfoWrapper"><div class="proInfo">#productName#</div> <div class="proInfo">#shortDesc#</div><div class="proInfo">#sku#</div></div></li>#wjend#</ul></div><p>Thank you,xxxxxxxx</p>',1),
 (6,'Gift Card Message','You recived a GiftCard','<p>Hello #giftCardRecipient#!!</p><p>&nbsp;</p><p>Congratulations! You received a Gift Card from #giftCardSender#.</p><p>&nbsp;</p><p>#giftCardMessage#</p><p>#giftCard#</p><p>Thank you,</p><p>#giftCardSender#</p>',0);

CREATE TABLE sales_rep (
  sales_rep_id INT unsigned NOT NULL AUTO_INCREMENT,
  salesrep_parent INT unsigned,
  sales_rep_account_num varchar(80),
  name varchar(80) NOT NULL,
  email varchar(80) NOT NULL,
  company varchar(50),
  addr1 varchar(80),
  addr2 varchar(80),  
  city varchar(80),
  state_province varchar(80),
  zip varchar(20),
  country varchar(50),
  phone varchar(80),
  cell_phone varchar(80),
  fax varchar(80),
  note text,
  territory_zip mediumtext,
  sales_rep_created datetime,
  sales_rep_last_modified datetime,
  feed_new bool NOT NULL,
  edit_price bool NOT NULL,
  protected_access BIT(64) NOT NULL default b'0',
  salesrep_password VARCHAR(40),
  salesrep_last_login datetime,
  salesrep_num_of_logins INT unsigned NOT NULL default 0,
  assigned_to_customer_in_cycle bool default 0,
  inactive bool default 0,
  salesrep_group VARCHAR(255) DEFAULT NULL,
  PRIMARY KEY(sales_rep_id),
  FOREIGN KEY (salesrep_parent) REFERENCES sales_rep (sales_rep_id)
  	ON DELETE SET NULL
  	ON UPDATE CASCADE  
) ENGINE = InnoDB;

CREATE TABLE home_page (
  category_id INT unsigned NOT NULL,
  host varchar(50),
  PRIMARY KEY  (category_id),
  FOREIGN KEY (category_id) REFERENCES category (id) 
  	ON DELETE CASCADE
  	ON UPDATE CASCADE
) ENGINE=InnoDB;

CREATE TABLE users (
  id INT unsigned NOT NULL auto_increment,
  prospect_id VARCHAR(45) NULL DEFAULT NULL,
  host varchar(50),
  credit DOUBLE(10,2) unsigned default 0.0,
  loyalty_point DOUBLE(10,2) unsigned default 0.0,
  point_threshold INT unsigned default NULL,
  point_value DOUBLE(5,2) NOT NULL DEFAULT 1.00,
  parent INT unsigned default NULL,
  hide_sub_accts bool NOT NULL,
  default_address_id INT unsigned,
  username VARCHAR(80) NOT NULL,
  username2 VARCHAR(80) default NULL,
  password VARCHAR(40) NOT NULL,
  suspended bool NOT NULL, 
  suspended_event bool NOT NULL, 
  having_walkin bool NOT NULL,
  paying_walkin bool NOT NULL,
  hold_loads bool NOT NULL,
  see_hidden_price bool NOT NULL, 
  created datetime,
  last_modified datetime,
  last_login datetime,
  last_password_reset datetime,
  password_validity INT default -1,
  num_of_logins INT unsigned NOT NULL default 0,
  token varchar(40),
  payment varchar(40),
  trackcode varchar(80),
  tax_id VARCHAR(30),
  promo_code varchar(100),
  domain_name varchar(100),
  category_id int,
  price_table TINYINT unsigned default 0,   
  protected_access BIT(64) NOT NULL default b'0',
  account_number VARCHAR(50),
  login_success_url VARCHAR(50),
  note text,
  affiliate bool NOT NULL,
  sales_rep_id INT unsigned,
  imported bool NOT NULL,
  field_1 varchar(255),
  field_2 varchar(255),
  field_3 varchar(255),
  field_4 varchar(255),
  field_5 varchar(255),
  field_6 varchar(255),
  field_7 varchar(255),
  field_8 varchar(255),
  field_9 varchar(255),
  field_10 varchar(255),
  field_11 varchar(255),
  field_12 varchar(255),
  field_13 varchar(255),
  field_14 varchar(255),
  field_15 varchar(255),
  field_16 varchar(255),
  field_17 varchar(255),
  field_18 varchar(255),
  field_19 varchar(255),
  field_20 varchar(255),
  field_21 varchar(255),
  field_22 varchar(255),
  field_23 varchar(255),
  field_24 varchar(255),
  field_25 varchar(255),
  field_26 varchar(255),
  field_27 varchar(255),
  field_28 varchar(255),
  field_29 varchar(255),
  field_30 varchar(255),
  shipping_title VARCHAR(20),
  custom_note_1 mediumtext,
  custom_note_2 mediumtext,
  custom_note_3 mediumtext,
  email_notify bool NOT NULL default true,
  text_message_notify bool NOT NULL default false,
 whatsapp_notify bool NOT NULL default false,
  ip_address text default NULL,
  po_required VARCHAR(15),
  registered_by VARCHAR(80) default NULL,
  extra_email VARCHAR(255) default NULL,
  supplier_id INT unsigned,
  supplier_prefix VARCHAR(5),
  supplier_product_limit MEDIUMINT unsigned,
  unsubscribe bool NOT NULL default 0, 
  evergreen_suc char(1),
  users_short_desc varchar(512),
  users_long_desc text,
  card_id varchar(50) default NULL,
  card_id_count SMALLINT unsigned default 0,
  brand_report varchar(100),
  crm_contact_id INT unsigned,
  crm_account_id INT unsigned,
  rating1 VARCHAR(45) NULL,
  rating2 VARCHAR(45) NULL,
  credit_allowed DOUBLE(10,2),
  budget_plan VARCHAR(45) NULL,
  payment_alert DOUBLE(10,2),
  gateway_token VARCHAR(45) default NULL,
  gateway_default_payment_id VARCHAR(10) default NULL,
  language VARCHAR(3) NOT NULL default 'en',
  website_url VARCHAR(255),
  ebay_name VARCHAR(100),
  amazon_name VARCHAR(100),
  unsubscribe_reason VARCHAR(255),
  unsubscribe_text_message bool NOT NULL default 0,
  update_new_information bool NOT NULL DEFAULT 0,
  broker_image_url VARCHAR(255) DEFAULT NULL,
  customer_type INT DEFAULT 0,
  main_source VARCHAR(100) NULL,
  paid VARCHAR(100) NULL,
  medium VARCHAR(100) NULL,
  language_field VARCHAR(100) NULL,
  not_call TINYINT(1) NOT NULL DEFAULT 0,
  qualifier INT unsigned DEFAULT 0,
  access_user_id INT(10) DEFAULT NULL,
  PRIMARY KEY(id),
  UNIQUE KEY (username),
  UNIQUE KEY (default_address_id),
  UNIQUE KEY (supplier_prefix),
  UNIQUE KEY (card_id),
  index (created),
  index (supplier_id),
  index (last_login),
  index (last_modified),
  index (num_of_logins),
  index (account_number),
  UNIQUE KEY (token),
  FOREIGN KEY (sales_rep_id) REFERENCES sales_rep (sales_rep_id)
  	ON DELETE RESTRICT
  	ON UPDATE CASCADE,
  FOREIGN KEY (parent) REFERENCES users (id)
  	ON DELETE SET NULL
  	ON UPDATE CASCADE
) ENGINE = InnoDB;

ALTER TABLE users CHANGE COLUMN `credit` `credit` DOUBLE(10,2) NULL DEFAULT '0.00'  ;

CREATE TABLE gateway_payment_methods (
payment_id VARCHAR(10),
payment_name VARCHAR(45),
user_id INT unsigned,
billing_address_id INT unsigned,
card_exp_date VARCHAR(45),
FOREIGN KEY (user_id) REFERENCES users (id)
  	ON DELETE CASCADE
  	ON UPDATE CASCADE
) ENGINE=InnoDB;

CREATE TABLE credit_history (
  transaction_id  VARCHAR(45) NOT NULL,
  userid INT unsigned,
  date datetime,
  date_modified timestamp,
  amount double(10,2) default '0',
  reference VARCHAR(45),
  username VARCHAR(50),
  FOREIGN KEY (userid) REFERENCES users (id)
  	ON DELETE CASCADE
  	ON UPDATE CASCADE
) ENGINE=InnoDB;

CREATE TABLE authorities (
  username varchar(80) NOT NULL,
  authority varchar(50) NOT NULL,
  UNIQUE KEY (username, authority),
  KEY (username),
  FOREIGN KEY (username) REFERENCES users (username) 
  	ON DELETE CASCADE
  	ON UPDATE CASCADE
) ENGINE = InnoDB;

CREATE TABLE affiliates (
  node_id INT unsigned NOT NULL AUTO_INCREMENT,
  product_commission_level1 INT unsigned default NULL,
  product_commission_level2 INT unsigned default NULL,
  invoice_commission_level1 DOUBLE(10,2) unsigned default NULL,
  invoice_commission_level2 DOUBLE(10,2) unsigned default NULL,
  percent_level1 bool NOT NULL,
  percent_level2 bool NOT NULL,
  first_order_commission DOUBLE(10,2) UNSIGNED DEFAULT NULL,
  other_order_commission DOUBLE(10,2) UNSIGNED DEFAULT NULL,
  first_order_percent TINYINT(1) NOT NULL DEFAULT '0',
  other_order_percent TINYINT(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (node_id),
  FOREIGN KEY (node_id) REFERENCES users (id) 
  	ON DELETE CASCADE
  	ON UPDATE CASCADE
) ENGINE = InnoDB;

CREATE TABLE edges (
  child_id INT unsigned NOT NULL default 0,
  parent_id INT unsigned NOT NULL,
  PRIMARY KEY (child_id, parent_id)
) ENGINE = InnoDB;

CREATE TABLE access_group (
  id INT unsigned NOT NULL AUTO_INCREMENT,
  group_name VARCHAR(80) NOT NULL,
  UNIQUE KEY (group_name),
  PRIMARY KEY(id)
) ENGINE = InnoDB;

CREATE TABLE access_group_privilege (
  group_id INT unsigned NOT NULL,
  authority varchar(50) NOT NULL,
  UNIQUE KEY (group_id, authority),
  FOREIGN KEY (group_id) REFERENCES access_group (id) 
  	ON DELETE CASCADE
  	ON UPDATE CASCADE
) ENGINE = InnoDB;

CREATE TABLE access_user (
  id INT unsigned NOT NULL AUTO_INCREMENT,
  username VARCHAR(80) NOT NULL,
  password VARCHAR(40) NOT NULL,
  enable bool DEFAULT 0,
  first_name varchar(80),
  last_name varchar(80),
  email VARCHAR(80) NOT NULL,
  ip_address text default NULL,
  sales_rep_id INT unsigned,
  salesrep_filter bool NOT NULL,  
  UNIQUE KEY (username),
  PRIMARY KEY(id),
  FOREIGN KEY (sales_rep_id) REFERENCES sales_rep (sales_rep_id) 
  	ON DELETE SET NULL
  	ON UPDATE CASCADE
) ENGINE = InnoDB;

CREATE TABLE access_user_access_group (
  user_id INT unsigned NOT NULL,
  group_id INT unsigned NOT NULL,
  PRIMARY KEY  (user_id,group_id),
  KEY (user_id),
  KEY (group_id),
  FOREIGN KEY (user_id) REFERENCES access_user (id) 
  	ON DELETE CASCADE
  	ON UPDATE CASCADE,
  FOREIGN KEY (group_id) REFERENCES access_group (id)
  	ON DELETE CASCADE
  	ON UPDATE CASCADE
) ENGINE=InnoDB;

CREATE TABLE access_privilege (
  username varchar(80) NOT NULL,
  authority varchar(50) NOT NULL,
  UNIQUE KEY (username, authority),
  FOREIGN KEY (username) REFERENCES access_user (username) 
  	ON DELETE CASCADE
  	ON UPDATE CASCADE
) ENGINE = InnoDB;

CREATE TABLE access_user_audit (
  id INT NOT NULL AUTO_INCREMENT,
  username VARCHAR(80) NOT NULL,
  page_visit_date datetime,
  page_seen VARCHAR(50) default NULL,
  ip_address VARCHAR(16) default NULL,
  PRIMARY KEY(id),
  FOREIGN KEY (username) REFERENCES access_user (username) 
  	ON DELETE CASCADE
  	ON UPDATE CASCADE
) ENGINE = InnoDB;

CREATE TABLE users_extrafield (
  id tinyint unsigned NOT NULL auto_increment,
  name VARCHAR(255) NOT NULL,
  name_es VARCHAR(255) NOT NULL,
  enabled bool NOT NULL,
  public_field bool NOT NULL,
  show_on_invoice bool NOT NULL,
  show_on_invoice_export bool NOT NULL,
  pre_value VARCHAR(1024) default NULL,
  required bool NOT NULL default 0,
  multi_selec_on_admin bool NOT NULL default 0,
  PRIMARY KEY (id)
) ENGINE=InnoDB;
insert into users_extrafield (name) values (''), (''), (''), (''), (''), (''), (''), (''), (''), (''), (''), (''), (''), (''), (''), (''), (''), (''), (''), ('');

CREATE TABLE supplier (
  id INT unsigned NOT NULL auto_increment,
  userid INT unsigned,
  account_number varchar(80) null,
  note text,
  default_address_id INT unsigned,
  first_name varchar(80) null,
  last_name varchar(80) null,
  company varchar(50) null,
  vendor_group varchar(50) null,
  vendor_dba varchar(50) null,
  addr1 varchar(80) null,
  addr2 varchar(80) null,  
  city varchar(80) null,
  state_province varchar(80) null,
  zip varchar(20) null,
  country varchar(50) null,
  phone varchar(80) null,
  fax varchar(80) null,
  email varchar(80) null,
  end_qty_pricing bool NOT NULL,
  markup_type INT(1) NOT NULL DEFAULT 0,
  markup_formula INT(1) NOT NULL DEFAULT 0,
  markup_1 DOUBLE(5,2) DEFAULT NULL,
  markup_2 DOUBLE(5,2) DEFAULT NULL,
  markup_3 DOUBLE(5,2) DEFAULT NULL,
  markup_4 DOUBLE(5,2) DEFAULT NULL,
  markup_5 DOUBLE(5,2) DEFAULT NULL,
  markup_6 DOUBLE(5,2) DEFAULT NULL,
  markup_7 DOUBLE(5,2) DEFAULT NULL,
  markup_8 DOUBLE(5,2) DEFAULT NULL,
  markup_9 DOUBLE(5,2) DEFAULT NULL,
  markup_10 DOUBLE(5,2) DEFAULT NULL,
  supplier_prefix VARCHAR(5) DEFAULT NULL,
  active boolean NOT NULL DEFAULT 1,
  payment_preference VARCHAR(20) DEFAULT NULL,
  mail_check_to VARCHAR(100) DEFAULT NULL,
  transfer_money_to VARCHAR(100) DEFAULT NULL,
  
  PRIMARY KEY (id),
  UNIQUE KEY (userid),
  index (company),
  FOREIGN KEY (userid) REFERENCES users (id) 
  	ON DELETE RESTRICT
  	ON UPDATE RESTRICT
) ENGINE=InnoDB;

CREATE TABLE supplier_address (
  id INT unsigned NOT NULL auto_increment,
  supplierid INT unsigned NOT NULL,
  first_name varchar(80),
  last_name varchar(80),
  company varchar(50),
  addr1 varchar(80),
  addr2 varchar(80),  
  city varchar(80),
  state_province varchar(80),
  zip varchar(20),
  country varchar(50),
  phone varchar(80),
  cell_phone varchar(80),
  fax varchar(80),
  residential bool NOT NULL,
  lift_gate bool NOT NULL,
  PRIMARY KEY (id),
  KEY (supplierid),
  FOREIGN KEY (supplierid) REFERENCES supplier (id) 
  	ON DELETE CASCADE
  	ON UPDATE CASCADE
) ENGINE = InnoDB;

CREATE TABLE product (
  id INT unsigned NOT NULL auto_increment,
  default_supplier_id INT unsigned NOT NULL default 0,
  created datetime,
  last_modified datetime,
  created_by VARCHAR(80) DEFAULT NULL,
  last_modified_by VARCHAR(80) DEFAULT NULL,
  sku varchar(50),
  master_sku varchar(50),
  upc varchar(50),
  etilize_id INT unsigned, 
  asi_id INT unsigned,
  asi_unique_id INT unsigned,
  asi_xml mediumtext,
  asi_ignore_price bool NOT NULL default 0,
  soft_link bool NOT NULL default 0,
  feed_freeze bool NOT NULL default 0,
  name varchar(120) NOT NULL,
  short_desc varchar(512),
  long_desc text,
  viewed INT unsigned NOT NULL default 0,
  include_retail_enabled INT unsigned NOT NULL,
  manufacture_name varchar(80) default NULL,
  field_1 tinytext,
  field_2 tinytext,
  field_3 tinytext,
  field_4 tinytext,
  field_5 tinytext,
  field_6 tinytext,
  field_7 tinytext,
  field_8 tinytext,
  field_9 tinytext,
  field_10 tinytext,
  field_11 tinytext,
  field_12 tinytext,
  field_13 tinytext,
  field_14 tinytext,
  field_15 tinytext,
  field_16 tinytext,
  field_17 tinytext,
  field_18 tinytext,
  field_19 tinytext,
  field_20 tinytext,
  field_21 tinytext,
  field_22 tinytext,
  field_23 tinytext,
  field_24 tinytext,
  field_25 tinytext,
  field_26 tinytext,
  field_27 tinytext,
  field_28 tinytext,
  field_29 tinytext,
  field_30 tinytext,
  field_31 tinytext,
  field_32 tinytext,
  field_33 tinytext,
  field_34 tinytext,
  field_35 tinytext,
  field_36 tinytext,
  field_37 tinytext,
  field_38 tinytext,
  field_39 tinytext,
  field_40 tinytext,
  field_41 tinytext,
  field_42 tinytext,
  field_43 tinytext,
  field_44 tinytext,
  field_45 tinytext,
  field_46 tinytext,
  field_47 tinytext,
  field_48 tinytext,
  field_49 tinytext,
  field_50 tinytext,
  field_51 tinytext,
  field_52 tinytext,
  field_53 tinytext,
  field_54 tinytext,
  field_55 tinytext,
  field_56 tinytext,
  field_57 tinytext,
  field_58 tinytext,
  field_59 tinytext,
  field_60 tinytext,
  field_61 tinytext,
  field_62 tinytext,
  field_63 tinytext,
  field_64 tinytext,
  field_65 tinytext,
  field_66 tinytext,
  field_67 tinytext,
  field_68 tinytext,
  field_69 tinytext,
  field_70 tinytext,
  field_71 tinytext,
  field_72 tinytext,
  field_73 tinytext,
  field_74 tinytext,
  field_75 tinytext,
  field_76 tinytext,
  field_77 tinytext,
  field_78 tinytext,
  field_79 tinytext,
  field_80 tinytext,
  field_81 tinytext,
  field_82 tinytext,
  field_83 tinytext,
  field_84 tinytext,
  field_85 tinytext,
  field_86 tinytext,
  field_87 tinytext,
  field_88 tinytext,
  field_89 tinytext,
  field_90 tinytext,
  field_91 tinytext,
  field_92 tinytext,
  field_93 tinytext,
  field_94 tinytext,
  field_95 tinytext,
  field_96 tinytext,
  field_97 tinytext,
  field_98 tinytext,
  field_99 tinytext,
  field_100 tinytext,
  keywords text NOT NULL default '',
  msrp double(10,3),
  price_1 double(10,3),
  price_2 double(10,3),
  price_3 double(10,3),
  price_4 double(10,3),
  price_5 double(10,3),
  price_6 double(10,3),
  price_7 double(10,3),
  price_8 double(10,3),
  price_9 double(10,3),
  price_10 double(10,3),
  cost_1 double(10,3),
  cost_2 double(10,3),
  cost_3 double(10,3),
  cost_4 double(10,3),
  cost_5 double(10,3),
  cost_6 double(10,3),
  cost_7 double(10,3),
  cost_8 double(10,3),
  cost_9 double(10,3),
  cost_10 double(10,3),
  qty_break_1 INT unsigned,
  qty_break_2 INT unsigned,
  qty_break_3 INT unsigned,
  qty_break_4 INT unsigned,
  qty_break_5 INT unsigned,
  qty_break_6 INT unsigned,
  qty_break_7 INT unsigned,
  qty_break_8 INT unsigned,
  qty_break_9 INT unsigned,
  price_table_1 double(10,3),
  price_table_2 double(10,3),
  price_table_3 double(10,3),
  price_table_4 double(10,3),
  price_table_5 double(10,3),
  price_table_6 double(10,3),
  price_table_7 double(10,3),
  price_table_8 double(10,3),
  price_table_9 double(10,3),
  price_table_10 double(10,3),
  price_case_pack_qty int unsigned,
  price_case_pack_1 double(10,3),
  price_case_pack_2 double(10,3),
  price_case_pack_3 double(10,3),
  price_case_pack_4 double(10,3),
  price_case_pack_5 double(10,3),
  price_case_pack_6 double(10,3),
  price_case_pack_7 double(10,3),
  price_case_pack_8 double(10,3),
  price_case_pack_9 double(10,3),
  price_case_pack_10 double(10,3),
  quote bool NOT NULL default 0,
  sales_tag_id INT unsigned,
  rebate_id INT unsigned,
  ordered int unsigned NOT NULL default 0,
  protected_level BIT(64) NOT NULL default b'0',
  weight double(10,2) default NULL,
  package_length double(10,2),
  package_width double(10,2),
  package_height double(10,2),
  ups_max_items_in_package double(6,2),
  usps_max_items_in_package double(6,2),
  login_require bool NOT NULL,
  hide_price bool NOT NULL, 
  hide_msrp bool NOT NULL, 
  recommended_list text,
  recommended_list_title varchar(80),
  recommended_list_display varchar(7),
  also_consider varchar(50),
  case_content int unsigned,
  packing varchar(50),
  hide_header bool NOT NULL,
  hide_topbar bool NOT NULL,
  hide_leftbar bool NOT NULL,
  hide_rightbar bool NOT NULL,
  hide_footer bool NOT NULL,  
  hide_breadcrumbs bool NOT NULL,
  product_layout VARCHAR(5) default '',
  head_tag text,
  html_add_to_cart text,
  add_to_list bool NOT NULL,
  add_to_presentation bool NOT NULL,
  image_folder varchar(20) default NULL,
  box_size INT unsigned,
  box_extra_amt double(10,2),
  temperature INT unsigned,
  compare bool NOT NULL,
  image_layout varchar(2),
  price_by_customer bool NOT NULL,
  taxable bool NOT NULL default 1,
  searchable bool NOT NULL default 1,
  option_code varchar(250),
  inventory INT,
  inventory_afs INT,
  neg_inventory bool NOT NULL,
  low_inventory INT,
  loyalty_point double(10,2) unsigned default NULL,
  show_neg_inventory bool NOT NULL,
  custom_shipping_enabled bool NOT NULL default 1,
  num_custom_lines SMALLINT unsigned default NULL,
  custom_lines_char TINYINT unsigned default NULL,
  subscription bool NOT NULL,
  subscription_discount double(10,2) UNSIGNED,
  cross_items_code varchar(50),
  active bool NOT NULL default 1,
  tab_1 varchar(40) default NULL,
  tab_1_content mediumtext default NULL,
  tab_2 varchar(40) default NULL,
  tab_2_content mediumtext default NULL,
  tab_3 varchar(40) default NULL,
  tab_3_content mediumtext default NULL,
  tab_4 varchar(40) default NULL,
  tab_4_content mediumtext default NULL,
  tab_5 varchar(40) default NULL,
  tab_5_content mediumtext default NULL,
  tab_6 varchar(40) default NULL,
  tab_6_content mediumtext default NULL,
  tab_7 varchar(40) default NULL,
  tab_7_content mediumtext default NULL,
  tab_8 varchar(40) default NULL,
  tab_8_content mediumtext default NULL,
  tab_9 varchar(40) default NULL,
  tab_9_content mediumtext default NULL,
  tab_10 varchar(40) default NULL,
  tab_10_content mediumtext default NULL,
  feed varchar(20),
  feed_new bool NOT NULL,
  feed_id INT unsigned default NULL,
  minimum_qty SMALLINT unsigned default NULL,
  incremental_qty SMALLINT unsigned default NULL,
  enable_rate bool NOT NULL default 1,
  note text,
  restrict_price_change TINYINT(1) NOT NULL default 0,
  list_type INT unsigned default NULL,
  sitemap_priority double(2,1) unsigned NOT NULL default 0.5,
  search_rank INT unsigned NOT NULL default 100,
  cat_ids varchar(45) default NULL,
  redirect_url_en varchar(200) default NULL,
  redirect_url_es varchar(200) default NULL,
  fob_zip_code varchar(50),
  program varchar(50),
  class_field varchar(50)
  PRIMARY KEY (id),
  UNIQUE KEY (sku),
  KEY (default_supplier_id),
  index (created),
  index (last_modified),
  index (sku),
  index (master_sku),
  index (feed_freeze),
  index (asi_unique_id),
  index (active),
  index (protected_level),
  FOREIGN KEY (sales_tag_id) REFERENCES sales_tags (tag_id) 
  	ON DELETE SET NULL 
) ENGINE=InnoDB;


CREATE TABLE product_i18n (
  product_id INT unsigned NOT NULL,
  lang varchar(2) NOT NULL,
  i18n_name varchar(120),
  i18n_short_desc varchar(512),
  i18n_long_desc text,
  i18n_field_1 tinytext,
  i18n_field_2 tinytext,
  i18n_field_3 tinytext,
  i18n_field_4 tinytext,
  i18n_field_5 tinytext,
  i18n_field_6 tinytext,
  i18n_field_7 tinytext,
  i18n_field_8 tinytext,
  i18n_field_9 tinytext,
  i18n_field_10 tinytext,
  i18n_field_11 tinytext,
  i18n_field_12 tinytext,
  i18n_field_13 tinytext,
  i18n_field_14 tinytext,
  i18n_field_15 tinytext,
  i18n_field_16 tinytext,
  i18n_field_17 tinytext,
  i18n_field_18 tinytext,
  i18n_field_19 tinytext,
  i18n_field_20 tinytext,
  i18n_field_21 tinytext,
  i18n_field_22 tinytext,
  i18n_field_23 tinytext,
  i18n_field_24 tinytext,
  i18n_field_25 tinytext,
  i18n_field_26 tinytext,
  i18n_field_27 tinytext,
  i18n_field_28 tinytext,
  i18n_field_29 tinytext,
  i18n_field_30 tinytext,
  i18n_field_31 tinytext,
  i18n_field_32 tinytext,
  i18n_field_33 tinytext,
  i18n_field_34 tinytext,
  i18n_field_35 tinytext,
  i18n_field_36 tinytext,
  i18n_field_37 tinytext,
  i18n_field_38 tinytext,
  i18n_field_39 tinytext,
  i18n_field_40 tinytext,
  i18n_field_41 tinytext,
  i18n_field_42 tinytext,
  i18n_field_43 tinytext,
  i18n_field_44 tinytext,
  i18n_field_45 tinytext,
  i18n_field_46 tinytext,
  i18n_field_47 tinytext,
  i18n_field_48 tinytext,
  i18n_field_49 tinytext,
  i18n_field_50 tinytext,  
  i18n_field_51 tinytext,
  i18n_field_52 tinytext,
  i18n_field_53 tinytext,
  i18n_field_54 tinytext,
  i18n_field_55 tinytext,
  i18n_field_56 tinytext,
  i18n_field_57 tinytext,
  i18n_field_58 tinytext,
  i18n_field_59 tinytext,
  i18n_field_60 tinytext,
  i18n_field_61 tinytext,
  i18n_field_62 tinytext,
  i18n_field_63 tinytext,
  i18n_field_64 tinytext,
  i18n_field_65 tinytext,
  i18n_field_66 tinytext,
  i18n_field_67 tinytext,
  i18n_field_68 tinytext,
  i18n_field_69 tinytext,
  i18n_field_70 tinytext,
  i18n_field_71 tinytext,
  i18n_field_72 tinytext,
  i18n_field_73 tinytext,
  i18n_field_74 tinytext,
  i18n_field_75 tinytext,
  i18n_field_76 tinytext,
  i18n_field_77 tinytext,
  i18n_field_78 tinytext,
  i18n_field_79 tinytext,
  i18n_field_80 tinytext,
  i18n_field_81 tinytext,
  i18n_field_82 tinytext,
  i18n_field_83 tinytext,
  i18n_field_84 tinytext,
  i18n_field_85 tinytext,
  i18n_field_86 tinytext,
  i18n_field_87 tinytext,
  i18n_field_88 tinytext,
  i18n_field_89 tinytext,
  i18n_field_90 tinytext,
  i18n_field_91 tinytext,
  i18n_field_92 tinytext,
  i18n_field_93 tinytext,
  i18n_field_94 tinytext,
  i18n_field_95 tinytext,
  i18n_field_96 tinytext,
  i18n_field_97 tinytext,
  i18n_field_98 tinytext,
  i18n_field_99 tinytext,
  i18n_field_100 tinytext,
  PRIMARY KEY (product_id, lang),
  FOREIGN KEY (product_id) REFERENCES product (id)
  	ON DELETE CASCADE
  	ON UPDATE CASCADE  
) ENGINE=InnoDB;

CREATE TABLE product_variant (
  product_id INT unsigned NOT NULL,
  sku varchar(50),
  name varchar(120),
  short_desc varchar(512),
  price double(10,2),
  INDEX (product_id),
  FOREIGN KEY (product_id) REFERENCES product (id)
  	ON DELETE CASCADE
  	ON UPDATE CASCADE  
) ENGINE=InnoDB;

CREATE TABLE range_value (
  id INT unsigned NOT NULL AUTO_INCREMENT,
  min_value DOUBLE(10,2),
  max_value DOUBLE(10,2),
  range_display_value VARCHAR(50),
  type VARCHAR(45) DEFAULT NULL,
  PRIMARY KEY (id)  
) ENGINE=InnoDB;

CREATE TABLE product_supplier (
 supplier_id INT unsigned NOT NULL,
 sku varchar(50),
 price double(10,2),
 percent bool NOT NULL default 0,
 supplier_sku varchar(50),
 last_modified datetime,
 PRIMARY KEY (supplier_id, sku),
 FOREIGN KEY (supplier_id) REFERENCES supplier (id)
 	ON DELETE CASCADE
 	ON UPDATE CASCADE,
 FOREIGN KEY (sku) REFERENCES product (sku)
 	ON DELETE CASCADE
 	ON UPDATE CASCADE
) ENGINE=InnoDB;

CREATE TABLE product_affiliate_commission (
  product_id INT unsigned NOT NULL,
  commission_table_1 double(10,2),
  commission_table_2 double(10,2),
  commission_table_3 double(10,2),
  commission_table_4 double(10,2),
  commission_table_5 double(10,2),
  commission_table_6 double(10,2),
  commission_table_7 double(10,2),
  commission_table_8 double(10,2),
  commission_table_9 double(10,2),
  commission_table_10 double(10,2),
  PRIMARY KEY (product_id),
  FOREIGN KEY (product_id) REFERENCES product (id)
  	ON DELETE CASCADE
  	ON UPDATE CASCADE  
) ENGINE=InnoDB;

CREATE TABLE product_extrafield (
  id tinyint unsigned NOT NULL,
  rank INT unsigned default NULL, 
  name VARCHAR(255) NOT NULL,
  group_name VARCHAR(255) NOT NULL,
  enabled bool NOT NULL,
  protected_level BIT(64) NOT NULL default b'0',
  quick_mode_field bool NOT NULL,
  quick_mode_2_field bool NOT NULL,
  pre_value VARCHAR(1024) default NULL,
  comparison_field bool NOT NULL,
  show_on_invoice bool NOT NULL,
  show_on_invoice_backend bool NOT NULL,
  show_on_invoice_export bool NOT NULL,
  show_on_drop_down bool NOT NULL,
  show_on_my_list bool NOT NULL,
  show_on_presentation TINYINT(1) default '0',
  show_on_product_form_frontend TINYINT(1) default '0',
  search bool NOT NULL,
  search2 bool NOT NULL,
  service_field bool NOT NULL,
  details_field bool NOT NULL,
  packing_field bool NOT NULL,
  product_export bool NOT NULL,
  index_for_filter bool NOT NULL DEFAULT 0,
  index_for_search bool NOT NULL DEFAULT 0,
  checkbox_filter bool NOT NULL DEFAULT 0,
  range_filter bool NOT NULL DEFAULT 0,
  index_for_predictive_search bool NOT NULL DEFAULT 0,
  type VARCHAR(255) NOT NULL default 'string',
  format_type VARCHAR(255) NULL,
  cat_ids TEXT,
  PRIMARY KEY (id)
) ENGINE=InnoDB;

CREATE TABLE product_extrafield_i18n (
  product_extrafield_id tinyint unsigned NOT NULL,
  lang varchar(2) NOT NULL,
  i18n_name VARCHAR(255),
  i18n_pre_value VARCHAR(1024),
  PRIMARY KEY (product_extrafield_id, lang),
  FOREIGN KEY (product_extrafield_id) REFERENCES product_extrafield (id)
  	ON DELETE CASCADE
  	ON UPDATE CASCADE  
) ENGINE=InnoDB;

CREATE TABLE special_pricing (
  id INT unsigned NOT NULL AUTO_INCREMENT,
  sku VARCHAR(50) NOT NULL,
  customer_id INT unsigned NOT NULL,
  price DOUBLE(10,2),
  enable bool NOT NULL,
  PRIMARY KEY (id),
  UNIQUE KEY (sku, customer_id),
  FOREIGN KEY (sku) REFERENCES product (sku) 
  	ON DELETE CASCADE
  	ON UPDATE CASCADE,
  FOREIGN KEY (customer_id) REFERENCES users (id) 
  	ON DELETE CASCADE
  	ON UPDATE CASCADE	
) ENGINE = InnoDB;

CREATE TABLE inventory_history ( 
  id INT unsigned NOT NULL AUTO_INCREMENT,
  sku VARCHAR(50) NOT NULL,
  date datetime,
  access_user_id INT,
  type VARCHAR(50) NOT NULL,
  reference VARCHAR(20),
  quantity INT,
  inventory INT,
  inv_afs_temp INT,
  note VARCHAR(255),
  PRIMARY KEY (id)
) ENGINE = InnoDB;

/*
CREATE VIEW cat_subcount_view AS select parent AS id, count(id) AS subcount from category GROUP BY parent;
*/

CREATE TABLE policy (
  id INT unsigned NOT NULL auto_increment,
  name varchar(255) NOT NULL,
  content text NOT NULL,
  rank INT unsigned default NULL,
  PRIMARY KEY (id)
) ENGINE=InnoDB;

CREATE TABLE faq_group (
  id INT unsigned NOT NULL auto_increment,
  name varchar(200) default NULL,
  rank INT unsigned default NULL,
  protected_level BIT(64) NOT NULL default b'0',
  PRIMARY KEY  (id)
) ENGINE=InnoDB;

CREATE TABLE faq (
  id INT unsigned NOT NULL auto_increment,
  question varchar(255) default NULL,
  answer text default NULL,
  rank INT unsigned default NULL,
  protected_level BIT(64) NOT NULL default b'0',
  PRIMARY KEY (id)
) ENGINE=InnoDB;

CREATE TABLE faq_faqgroup (
  faq_id INT unsigned NOT NULL,
  faqgroup_id INT unsigned NOT NULL,
  PRIMARY KEY  (faq_id,faqgroup_id),
  KEY (faq_id),
  KEY (faqgroup_id),
  FOREIGN KEY (faq_id) REFERENCES faq (id) 
  	ON DELETE CASCADE
  	ON UPDATE CASCADE,
  FOREIGN KEY (faqgroup_id) REFERENCES faq_group (id)
  	ON DELETE CASCADE
  	ON UPDATE CASCADE
) ENGINE=InnoDB;

CREATE TABLE address (
  id INT unsigned NOT NULL auto_increment,
  userid INT unsigned NOT NULL,
  first_name varchar(80),
  last_name varchar(80),
  company varchar(50),
  addr1 varchar(80),
  addr2 varchar(80),  
  city varchar(80),
  state_province varchar(80),
  zip varchar(20),
  country varchar(50),
  phone varchar(80),
  cell_phone varchar(80),
  fax varchar(80),
  residential bool NOT NULL,
  lift_gate bool NOT NULL,
  address_code varchar(20),
  default_shipping bool NOT NULL,
  mobile_carrier_id INT UNSIGNED,
  PRIMARY KEY (id),
  KEY (userid),
  FOREIGN KEY (userid) REFERENCES users (id) 
  	ON DELETE CASCADE
  	ON UPDATE CASCADE
) ENGINE = InnoDB;

CREATE TABLE category_supplier (
  category_id INT unsigned NOT NULL,
  supplier_id INT unsigned NOT NULL,
  PRIMARY KEY  (category_id,supplier_id),
  KEY (category_id),
  KEY (supplier_id),
  FOREIGN KEY (category_id) REFERENCES category (id) 
  	ON DELETE CASCADE
  	ON UPDATE CASCADE,
  FOREIGN KEY (supplier_id) REFERENCES supplier (id)
  	ON DELETE CASCADE
  	ON UPDATE CASCADE
) ENGINE=InnoDB;

CREATE TABLE category_users (
  category_id INT unsigned NOT NULL,
  users_id INT unsigned NOT NULL,
  PRIMARY KEY (category_id, users_id),
  KEY (category_id),
  KEY (users_id),
  FOREIGN KEY (category_id) REFERENCES category (id) 
  	ON DELETE CASCADE
  	ON UPDATE CASCADE,
  FOREIGN KEY (users_id) REFERENCES users (id)
  	ON DELETE CASCADE
  	ON UPDATE CASCADE
) ENGINE=InnoDB;

CREATE TABLE category_product (
  category_id INT unsigned NOT NULL,
  product_id INT unsigned NOT NULL,
  rank INT unsigned default NULL,
  new_entry bool NOT NULL,
  PRIMARY KEY  (category_id,product_id),
  KEY (category_id),
  KEY (product_id),
  FOREIGN KEY (category_id) REFERENCES category (id) 
  	ON DELETE CASCADE
  	ON UPDATE CASCADE,
  FOREIGN KEY (product_id) REFERENCES product (id)
  	ON DELETE CASCADE
  	ON UPDATE CASCADE
) ENGINE=InnoDB;


ALTER TABLE users ADD FOREIGN KEY (default_address_id) REFERENCES address (id) 
  	ON DELETE RESTRICT
  	ON UPDATE CASCADE;
  	
CREATE TABLE shopping_cart (
  cart_index smallint unsigned NOT NULL,
  userid INT unsigned NOT NULL,
  product_id INT unsigned,
  date_created datetime,
  supplier_id INT unsigned NOT NULL,
  quantity INT unsigned NOT NULL,
  variable_unit_price double(10,2) unsigned DEFAULT NULL,
  box_extra_qty INT unsigned NOT NULL DEFAULT 0,
  box_content_weight double(10,2) NOT NULL DEFAULT 0.0,
  low_inventory_message VARCHAR(80) DEFAULT NULL,
  inventory_message VARCHAR(80) DEFAULT NULL,
  subscription_interval VARCHAR(3),
  manufacture_name VARCHAR(80) DEFAULT NULL,
  custom_xml text,
  custom_image_url varchar(255),
  attachment varchar(50),
  deal_group varchar(50) DEFAULT NULL,
  deal_main_item tinyint(1) DEFAULT 0,
  item_group INTEGER DEFAULT NULL,
  item_group_main_item tinyint(1) DEFAULT 1,
  qty_for_price_break INT DEFAULT NULL,
  qty_multiplier INT DEFAULT NULL,
  asi_additional_charge double(10,2) unsigned DEFAULT NULL,
  asi_unit_price double(10,2) unsigned DEFAULT NULL,
  asi_original_price double(10,2) unsigned DEFAULT NULL,
  temp_cart_total double(10,2) unsigned DEFAULT 0,
  variant_sku varchar(50),
  include_retail_display INT unsigned NOT NULL,
  price_case_pack_qty int unsigned,
  api_cart_index varchar(100),
  PRIMARY KEY(cart_index, userid),
  FOREIGN KEY (userid) REFERENCES users (id)
  	ON DELETE CASCADE
  	ON UPDATE CASCADE,
  FOREIGN KEY (product_id) REFERENCES product (id)
  	ON DELETE CASCADE
  	ON UPDATE CASCADE
) ENGINE = InnoDB;  

CREATE TABLE shopping_cart_attributes (
  cart_index smallint unsigned NOT NULL,
  userid INT unsigned NOT NULL,
  product_id INT unsigned NOT NULL,
  option_code varchar(50),
  option_index tinyint unsigned NOT NULL,
  value_index smallint unsigned NOT NULL,
  custom_value varchar(110),
  box_value varchar(110),
  image_url varchar(255),
  deal_item bool NOT NULL,
  is_custom_attribute boolean default 0,
  option_name varchar(110),
  value_name varchar(110),
  api_cart_index varchar(100),
  FOREIGN KEY (userid) REFERENCES users (id)
  	ON DELETE CASCADE
  	ON UPDATE CASCADE,
  FOREIGN KEY (product_id) REFERENCES product (id)
  	ON DELETE CASCADE
  	ON UPDATE CASCADE
) ENGINE = InnoDB;	

CREATE TABLE shopping_cart_asi_attributes (
  cart_index smallint unsigned NOT NULL,
  userid INT unsigned NOT NULL,
  product_id INT unsigned NOT NULL,
  asi_option_name VARCHAR(255) NOT NULL,
  asi_option_value VARCHAR(255) NOT NULL,
  PRIMARY KEY (cart_index, userid, asi_option_name, asi_option_value),
  FOREIGN KEY (userid) REFERENCES users (id)
  	ON DELETE CASCADE
  	ON UPDATE CASCADE,
  FOREIGN KEY (product_id) REFERENCES product (id)
  	ON DELETE CASCADE
  	ON UPDATE CASCADE
) ENGINE = InnoDB;	

CREATE TABLE shopping_cart_customlines (
  cart_index smallint unsigned NOT NULL,
  userid INT unsigned NOT NULL,
  product_id INT unsigned NOT NULL,
  custom_line text,
  quantity INT unsigned NOT NULL,
  FOREIGN KEY (userid) REFERENCES users (id)
  	ON DELETE CASCADE
  	ON UPDATE CASCADE,
  FOREIGN KEY (product_id) REFERENCES product (id)
  	ON DELETE CASCADE
  	ON UPDATE CASCADE
) ENGINE = InnoDB;

CREATE TABLE shopping_list (
  userid INT unsigned NOT NULL,
  product_id INT unsigned NOT NULL,
  added datetime,
  shopping_list_group_id INT unsigned default 0,
  PRIMARY KEY  (userid,product_id),
  FOREIGN KEY (userid) REFERENCES users (id)
  	ON DELETE CASCADE
  	ON UPDATE CASCADE,
  FOREIGN KEY (product_id) REFERENCES product (id)
  	ON DELETE CASCADE
  	ON UPDATE CASCADE
) ENGINE=InnoDB;

CREATE TABLE shopping_list_group (
  id INT unsigned NOT NULL auto_increment,
  userid INT unsigned NOT NULL,
  name varchar(200) default NULL,
  rank SMALLINT unsigned default NULL,
  date_created datetime,
  date_modified timestamp,
  PRIMARY KEY  (id)
) ENGINE=InnoDB;

CREATE TABLE product_image (
  image_index tinyint unsigned NOT NULL,
  product_id INT unsigned NOT NULL,
  image_url varchar(255),  
  PRIMARY KEY  (product_id,image_index),
  FOREIGN KEY (product_id) REFERENCES product (id)
  	ON DELETE CASCADE
  	ON UPDATE CASCADE
) ENGINE=InnoDB;
  	
CREATE TABLE orders (
  order_id INT unsigned NOT NULL auto_increment,
  external_order_id varchar(20),
  userid INT unsigned,
  qualifier INT unsigned DEFAULT 0,
  prospect_id VARCHAR(45) NULL DEFAULT NULL,
  host varchar(50),
  date_ordered datetime,
  last_modified datetime,
  status varchar(3) NOT NULL default 'p',
  sub_status varchar(3) NOT NULL default '',
  status_date_changed datetime,
  bill_to_first_name varchar(80),
  bill_to_last_name varchar(80),
  bill_to_company varchar(50),
  bill_to_addr1 varchar(80),
  bill_to_addr2 varchar(80),  
  bill_to_city varchar(80),
  bill_to_state_province varchar(80),
  bill_to_zip varchar(20),
  bill_to_country varchar(50),
  bill_to_phone varchar(80),
  bill_to_cell_phone varchar(80),
  bill_to_fax varchar(80),
  ship_to_code varchar(20),
  ship_to_first_name varchar(80),
  ship_to_last_name varchar(80),
  ship_to_company varchar(50),
  ship_to_addr1 varchar(80),
  ship_to_addr2 varchar(80),  
  ship_to_city varchar(80),
  ship_to_state_province varchar(80),
  ship_to_zip varchar(20),
  ship_to_country varchar(50),
  ship_to_phone varchar(80),
  ship_to_cell_phone varchar(80),
  ship_to_fax varchar(80),
  ship_to_residential bool Not NULL,
  ship_to_liftgate bool Not NULL,
  sub_total double(10,2),
  credit_used double(10,2),
  request_for_credit double(10,2) default NULL,
  tax_rate double(10,3),
  shipping_cost double(10,2),
  shipping_method varchar(100),
  shipping_carrier varchar(100) default NULL,
  tax double(10,2),
  grand_total double(10,2),
  promo_id INT unsigned,
  promo_code varchar(100),
  promo_discount double(10,2) unsigned,
  promo_discount_type VARCHAR(20) default 'order',
  promo_percent bool NOT NULL,
  payment_method varchar(80) NOT NULL default '',
  cc_type varchar(10),
  cc_number varchar(16),
  cc_exp_month varchar(2),
  cc_exp_year varchar(4),
  cc_card_code varchar(4),
  cc_trans_id varchar(50), 
  cc_auth_code varchar(50),
  cc_avs_code varchar(2),
  cc_cvv2_code varchar(1),
  cc_payment_date datetime,
  cc_payment_status varchar(20),
  cc_payment_note varchar(200),
  cc_eci varchar(2),
  cc_cavv varchar(50),
  cc_xid varchar(50),  
  cc_token VARCHAR(45) default NULL,
  cc_billing_address TEXT NULL DEFAULT NULL,
  pp_txn_id varchar(17),
  pp_payment_status varchar(20),
  pp_payment_date varchar(30),
  nc_num_aut varchar(6),
  nc_resp_val varchar(1),
  nc_resp_msg varchar(100),
  nc_payment_date datetime,
  ba_txn_response_code varchar(2),
  ba_message varchar(100),
  ba_trans_no varchar(20),
  ba_payment_date datetime, 
  eb_orderrefid varchar(20),
  eb_buyer_accountnumber varchar(20),
  eb_buyer_authstatus varchar(20),
  eb_xml_file text,
  amz_order_channel varchar(50),
  amz_order_id varchar(30),
  amz_notification_type varchar(30),
  amz_notification_refid varchar(50),
  amz_timestamp varchar(30),
  ge_acct_num varchar(16),
  ge_status varchar(20),
  ge_trans_date varchar(25),
  ge_auth_code varchar(10),
  trackcode varchar(80),
  ls_site_id varchar(40),
  ls_date_entered datetime,
  invoice_note text,
  salesrep_id INT unsigned,
  salesrep_processedby_id INT unsigned,
  commission_level1 double(10,2),
  commission_level2 double(10,2),
  commission_level1_status varchar(2) NOT NULL default 'p',
  commission_level2_status varchar(2) NOT NULL default 'p',
  commission_level1_note text,
  commission_level2_note text,
  percent_level1 tinyint(1) not null,
  percent_level2 tinyint(1) not null,
  affiliate_id_level1 int,
  affiliate_id_level2 int,
  check_number_level1 varchar(30),
  check_number_level2 varchar(30),
  date_level1 DATE,
  date_level2 DATE,
  purchase_order varchar(50),
  ip_address varchar(15),
  work_order_num INT unsigned,
  pdf_url varchar(30),
  subscription_code varchar(18),
  printed tinyint(1) not null,
  mas90 bool NOT NULL,
  mas200 bool NOT NULL,
  mas200_orderno varchar(10),
  mas200_status varchar(5),  
  dsi bool NOT NULL,
  triplefin bool NOT NULL,
  bv bool NOT NULL,  
  pnh bool NOT NULL,
  export_success char(1),
  backend_order bool NOT NULL default false,
  approval char(2) default NULL,
  amount_paid double(10,2),
  custom_shipping_title varchar(80) default NULL,
  custom_shipping_cost double(10,2) default NULL,
  turn_over_day SMALLINT unsigned default 0,
  shipping_period SMALLINT unsigned,
  nc tinyint(1) not null default 0,
  cc_fee_rate double(10,2),
  cc_fee double(10,2),
  cc_fee_percent bool NOT NULL default 1,
  order_type varchar(30),
  flag_1 tinyint unsigned,
  custom_field_1 VARCHAR(100),
  custom_field_2 VARCHAR(100),
  custom_field_3 VARCHAR(100),
  user_due_date datetime,
  agreed_payment_date datetime,
  agreed_pickup_date datetime,
  requested_cancel_date datetime,
  google_order_id VARCHAR(20),
  google_payment_date datetime,
  delivery_person varchar(100),  
  order_priority TINYINT unsigned NOT NULL,
  shopworks bool NOT NULL,
  cart_id varchar(20),
  buy_safe_bond_cost double(10,2),
  wants_bond bool default NULL,
  manufacturer_name VARCHAR(80),
  vba_payment_status bool NOT NULL default '0',
  vba_date_of_pay DATE,
  vba_payment_method VARCHAR(15),
  vba_amount_to_pay  double(10,2),
  vba_note VARCHAR(25),
  vba_transaction_id VARCHAR(15),
  payment_alert bool Not NULL default '0',
  language VARCHAR(3) NOT NULL default 'en',
  budget_earned_credits double(10,2),
  is_first TINYINT(1) unsigned default '0',
  deal_original_price DOUBLE(10,2) NULL,
  deal_discount DOUBLE(10,2) NULL,
  quote VARCHAR(255) DEFAULT NULL,
  primary key (order_id),
  index (commission_level1),
  index (commission_level2),
  FOREIGN KEY (salesrep_id) REFERENCES sales_rep (sales_rep_id)
  	ON DELETE RESTRICT
  	ON UPDATE CASCADE,
  FOREIGN KEY (salesrep_processedby_id) REFERENCES sales_rep (sales_rep_id)
  	ON DELETE RESTRICT
  	ON UPDATE CASCADE,
  FOREIGN KEY (userid) REFERENCES users (id)
    ON DELETE RESTRICT
    ON UPDATE CASCADE
) ENGINE=InnoDB;

ALTER TABLE orders AUTO_INCREMENT = 1000001;
  	
CREATE TABLE orders_lineitems (  	
  order_id INT unsigned NOT NULL,
  line_num INT unsigned NOT NULL,
  product_id INT unsigned,
  product_name varchar(120),
  master_sku varchar(50),
  product_sku varchar(50),
  product_feed varchar(20),  
  quantity INT NOT NULL,
  original_quantity INT NOT NULL,
  original_price double(10,3),
  percent bool NOT NULL default true,
  discount double(10,2),
  unit_price double(10,3),
  case_content int unsigned,
  packing varchar(50),
  include_retail_display INT unsigned NOT NULL,
  taxable bool NOT NULL default true,
  low_inventory_message varchar(80) DEFAULT NULL,
  processed INT NOT NULL DEFAULT 0,
  loyalty_point DOUBLE(10,2) unsigned DEFAULT NULL,
  image_url varchar(255), 
  subscription_interval VARCHAR(3),
  subscription_code varchar(18),
  custom_shipping_cost double(10,2) DEFAULT NULL,
  track_num varchar(50),
  date_shipped DATE,
  shipping_method varchar(100),
  line_status varchar(20),  
  supplier_id INT unsigned,
  manufacture_name varchar(80) DEFAULT NULL,
  custom_xml text,
  custom_image_url varchar(255),
  deal_group varchar(50) DEFAULT NULL,
  amz_order_item_code varchar(30),
  promo_id INT unsigned DEFAULT NULL,
  promo_code VARCHAR(100) DEFAULT NULL,
  promo_discount DOUBLE(10,2) unsigned DEFAULT NULL,
  promo_percent bool DEFAULT NULL,
  asi_additional_charge double(10,2) unsigned DEFAULT NULL,
  cost DOUBLE(10,2) unsigned DEFAULT NULL,
  cost_percent bool NOT NULL default 0,
  consignment_product bool NOT NULL default 0,
  price_case_pack_qty int unsigned,
  shipping_days int unsigned DEFAULT NULL,
  vba_payment_status bool NOT NULL default '0',
  vba_date_of_pay DATE,
  vba_payment_method VARCHAR(15),
  vba_amount_to_pay  double(10,2),
  vba_note VARCHAR(25),
  vba_transaction_id VARCHAR(15),
  attachment varchar(50),
  item_group INTEGER DEFAULT NULL,
  item_group_main_item tinyint(1) DEFAULT 1,
  deal_original_price DOUBLE(10,2) NULL DEFAULT NULL  ,
  deal_discount DOUBLE(10,2) NULL DEFAULT NULL ,
  location varchar(50),
  primary key (order_id, line_num),
  index (cost),
  FOREIGN KEY (order_id) REFERENCES orders (order_id)
  	ON DELETE CASCADE
  	ON UPDATE CASCADE
) ENGINE=InnoDB;

CREATE TABLE orders_lineitem_attributes (  	
  id INT unsigned NOT NULL auto_increment,
  order_id INT unsigned NOT NULL,
  line_num INT unsigned NOT NULL,
  option_name varchar(100) NOT NULL default '',
  value_name varchar(110) NOT NULL default '',
  image_url varchar(255), 
  price DOUBLE(10, 2) DEFAULT NULL,
  PRIMARY KEY (id),
  UNIQUE INDEX id_UNIQUE (id ASC),
  FOREIGN KEY (order_id) REFERENCES orders (order_id)
  	ON DELETE CASCADE
  	ON UPDATE CASCADE
) ENGINE=InnoDB;

CREATE TABLE orders_lineitem_customlines (  	
  order_id INT unsigned NOT NULL,
  line_num INT unsigned NOT NULL,
  custom_line text,
  quantity INT unsigned NOT NULL,
  FOREIGN KEY (order_id) REFERENCES orders (order_id)
  	ON DELETE CASCADE
  	ON UPDATE CASCADE
) ENGINE=InnoDB;

CREATE TABLE orders_lineitem_serialnums (  	
  order_id INT unsigned NOT NULL,
  line_num INT unsigned NOT NULL,
  serial_num varchar(50), 
  FOREIGN KEY (order_id) REFERENCES orders (order_id)
  	ON DELETE CASCADE
  	ON UPDATE CASCADE
) ENGINE=InnoDB;

CREATE TABLE orders_status_history (
  id INT unsigned NOT NULL auto_increment,
  order_id INT unsigned NOT NULL,
  status varchar(3) NOT NULL default 'p',
  date_changed datetime,
  user_notified bool NOT NULL,
  comments text,
  carrier text,
  track_num varchar(50),
  username varchar(50) default NULL,
  sub_status varchar(3) default NULL,
  cancel_reason VARCHAR(80),
  amz_doc_trans_id BIGINT,
  PRIMARY KEY (id),
  FOREIGN KEY (order_id) REFERENCES orders (order_id)
  	ON DELETE CASCADE
  	ON UPDATE CASCADE
) ENGINE=InnoDB;
  
CREATE TABLE orders_approval_denial_history (
  order_id INT unsigned NOT NULL auto_increment,
  user_id INT unsigned NOT NULL,
  date timestamp,
  note varchar(200) default NULL,
  type varchar(20) default NULL,
  FOREIGN KEY (order_id) REFERENCES orders (order_id)
  	ON DELETE CASCADE
  	ON UPDATE CASCADE,
  FOREIGN KEY (user_id) REFERENCES users (id)
    ON DELETE CASCADE
    ON UPDATE CASCADE
) ENGINE=InnoDB;

CREATE TABLE options (
  id INT unsigned NOT NULL auto_increment,
  option_code varchar(50),
  created datetime,
  last_modified datetime,
  protected_level BIT(64) NOT NULL default b'0',
  PRIMARY KEY (id),
  UNIQUE KEY (option_code) 
) ENGINE=InnoDB;

CREATE TABLE product_options (
  product_id INT unsigned NOT NULL,
  language varchar(2) NOT NULL default 'en',
  option_index tinyint unsigned NOT NULL,
  name varchar(100) NOT NULL default '',
  type varchar(20) NOT NULL default '',
  number_of_colors INT unsigned NOT NULL default 0,
  help_text varchar(255) default NULL,
  PRIMARY KEY (product_id, language, option_index),
  FOREIGN KEY (product_id) REFERENCES options (id)
  	ON DELETE CASCADE
  	ON UPDATE CASCADE  
) ENGINE=InnoDB;

CREATE TABLE product_option_values (
  product_id INT unsigned NOT NULL,
  language varchar(2) NOT NULL default 'en',
  option_index tinyint unsigned NOT NULL,
  value_index tinyint unsigned NOT NULL,
  name varchar(50) NOT NULL default '',
  option_price DOUBLE(10,2) default NULL,
  one_time_price tinyint unsigned NOT NULL DEFAULT 0,
  option_weight DOUBLE(10,2) default NULL,
  image_url varchar(255),
  depending_option_ids varchar(30),
  description varchar(255),
  assigned_product_id INT,
  assigned_product_sku VARCHAR(50),
  assigned_sku_attachment TINYINT(1) default 0,
  included_products TEXT default NULL,
  PRIMARY KEY (product_id, language, option_index, value_index),
  FOREIGN KEY (product_id) REFERENCES options (id)
  	ON DELETE CASCADE
  	ON UPDATE CASCADE  
) ENGINE=InnoDB;

CREATE TABLE kit_parts (
  kit_sku varchar(50) NOT NULL,
  kit_parts_sku varchar(50) NOT NULL,
  quantity INT unsigned NOT NULL,
  UNIQUE KEY (kit_sku, kit_parts_sku ),
  FOREIGN KEY (kit_sku) REFERENCES product (sku)
    ON DELETE CASCADE
  	ON UPDATE CASCADE 
) ENGINE=InnoDB;

CREATE TABLE orders_lineitem_kit (
  kit_sku varchar(50) NOT NULL,
  kit_parts_sku varchar(50) NOT NULL,
  quantity INT unsigned NOT NULL,
  line_num INT unsigned NOT NULL,
  order_id INT unsigned NOT NULL
) ENGINE=InnoDB;

CREATE TABLE counties (
  country char(2) NOT NULL,
  state varchar(30) NOT NULL,
  county varchar(30) NOT NULL,
  tax_rate DOUBLE(5,3),
  UNIQUE KEY (country, state, county)
) ENGINE=InnoDB;

CREATE TABLE cities (
  country char(2) NOT NULL,
  state varchar(30) NOT NULL,
  county varchar(30) NOT NULL,
  city varchar(50) NOT NULL,
  tax_rate DOUBLE(5,3),
  UNIQUE KEY (country, state, county, city)
) ENGINE=InnoDB;

CREATE TABLE country (
  code char(2) NOT NULL,
  name varchar(100) NOT NULL,
  enabled bool NOT NULL,
  rank INT unsigned default 250,
  tax_rate double(5,3) NOT NULL default 0, 
  region VARCHAR(50) NOT NULL,
  PRIMARY KEY (code),
  UNIQUE KEY (name)
) ENGINE=InnoDB;

CREATE TABLE state (
  code char(2) NOT NULL,
  country char(2) NOT NULL,
  name varchar(100) NOT NULL,
  region VARCHAR(50) NOT NULL,
  tax_rate double(5,3) NOT NULL, 
  apply_shipping_tax TINYINT(1) NOT NULL DEFAULT 0,
  time_zone VARCHAR(45),
  PRIMARY KEY (code, country)
) ENGINE=InnoDB;

UPDATE state SET `time_zone`='CT' WHERE `code`='AL' and`country`='US';
UPDATE state SET `time_zone`='MT' WHERE `code`='AZ' and`country`='US';
UPDATE state SET `time_zone`='CT' WHERE `code`='AR' and`country`='US';
UPDATE state SET `time_zone`='PT' WHERE `code`='CA' and`country`='US';
UPDATE state SET `time_zone`='MT' WHERE `code`='CO' and`country`='US';
UPDATE state SET `time_zone`='ET' WHERE `code`='CT' and`country`='US';
UPDATE state SET `time_zone`='ET' WHERE `code`='DE' and`country`='US';
UPDATE state SET `time_zone`='ET' WHERE `code`='FL' and`country`='US';
UPDATE state SET `time_zone`='ET' WHERE `code`='GA' and`country`='US';
UPDATE state SET `time_zone`='MT' WHERE `code`='ID' and`country`='US';
UPDATE state SET `time_zone`='CT' WHERE `code`='IL' and`country`='US';
UPDATE state SET `time_zone`='ET' WHERE `code`='IN' and`country`='US';
UPDATE state SET `time_zone`='CT' WHERE `code`='IA' and`country`='US';
UPDATE state SET `time_zone`='CT' WHERE `code`='KS' and`country`='US';
UPDATE state SET `time_zone`='ET' WHERE `code`='KY' and`country`='US';
UPDATE state SET `time_zone`='CT' WHERE `code`='LA' and`country`='US';
UPDATE state SET `time_zone`='ET' WHERE `code`='ME' and`country`='US';
UPDATE state SET `time_zone`='ET' WHERE `code`='MD' and`country`='US';
UPDATE state SET `time_zone`='ET' WHERE `code`='MA' and`country`='US';
UPDATE state SET `time_zone`='ET' WHERE `code`='MI' and`country`='US';
UPDATE state SET `time_zone`='CT' WHERE `code`='MN' and`country`='US';
UPDATE state SET `time_zone`='CT' WHERE `code`='MS' and`country`='US';
UPDATE state SET `time_zone`='CT' WHERE `code`='MO' and`country`='US';
UPDATE state SET `time_zone`='MT' WHERE `code`='MT' and`country`='US';
UPDATE state SET `time_zone`='CT' WHERE `code`='NE' and`country`='US';
UPDATE state SET `time_zone`='PT' WHERE `code`='NV' and`country`='US';
UPDATE state SET `time_zone`='ET' WHERE `code`='NH' and`country`='US';
UPDATE state SET `time_zone`='ET' WHERE `code`='NJ' and`country`='US';
UPDATE state SET `time_zone`='MT' WHERE `code`='NM' and`country`='US';
UPDATE state SET `time_zone`='ET' WHERE `code`='NY' and`country`='US';
UPDATE state SET `time_zone`='ET' WHERE `code`='NC' and`country`='US';
UPDATE state SET `time_zone`='CT' WHERE `code`='ND' and`country`='US';
UPDATE state SET `time_zone`='ET' WHERE `code`='OH' and`country`='US';
UPDATE state SET `time_zone`='CT' WHERE `code`='OK' and`country`='US';
UPDATE state SET `time_zone`='PT' WHERE `code`='OR' and`country`='US';
UPDATE state SET `time_zone`='ET' WHERE `code`='PA' and`country`='US';
UPDATE state SET `time_zone`='ET' WHERE `code`='RI' and`country`='US';
UPDATE state SET `time_zone`='ET' WHERE `code`='SC' and`country`='US';
UPDATE state SET `time_zone`='CT' WHERE `code`='SD' and`country`='US';
UPDATE state SET `time_zone`='CT' WHERE `code`='TN' and`country`='US';
UPDATE state SET `time_zone`='CT' WHERE `code`='TX' and`country`='US';
UPDATE state SET `time_zone`='MT' WHERE `code`='UT' and`country`='US';
UPDATE state SET `time_zone`='ET' WHERE `code`='VT' and`country`='US';
UPDATE state SET `time_zone`='ET' WHERE `code`='VA' and`country`='US';
UPDATE state SET `time_zone`='PT' WHERE `code`='WA' and`country`='US';
UPDATE state SET `time_zone`='ET' WHERE `code`='WV' and`country`='US';
UPDATE state SET `time_zone`='CT' WHERE `code`='WI' and`country`='US';
UPDATE state SET `time_zone`='MT' WHERE `code`='WY' and`country`='US';


LOCK TABLES country WRITE;
INSERT INTO country (code, name, enabled, rank) VALUES  ('AD','Andorra',1,250),
 ('AE','United Arab Emirates',1,250),
 ('AF','Afghanistan',1,250),
 ('AG','Antigua and Barbuda',1,250),
 ('AI','Anguilla',1,250),
 ('AL','Albania',1,250),
 ('AM','Armenia',1,250),
 ('AN','Netherlands Antilles',1,250),
 ('AO','Angola',1,250),
 ('AQ','Antarctica',1,250),
 ('AR','Argentina',1,250),
 ('AS','American Samoa',1,250),
 ('AT','Austria',1,250),
 ('AU','Australia',1,250),
 ('AW','Aruba',1,250),
 ('AZ','Azerbaijan',1,250),
 ('BA','Bosnia and Herzegowina',1,250),
 ('BB','Barbados',1,250),
 ('BD','Bangladesh',1,250),
 ('BE','Belgium',1,250),
 ('BF','Burkina Faso',1,250),
 ('BG','Bulgaria',1,250),
 ('BH','Bahrain',1,250),
 ('BI','Burundi',1,250),
 ('BJ','Benin',1,250),
 ('BM','Bermuda',1,250),
 ('BN','Brunei Darussalam',1,250),
 ('BO','Bolivia',1,250),
 ('BR','Brazil',1,250),
 ('BS','Bahamas',1,250),
 ('BT','Bhutan',1,250),
 ('BV','Bouvet Island',1,250),
 ('BW','Botswana',1,250),
 ('BY','Belarus',1,250),
 ('BZ','Belize',1,250),
 ('CA','Canada',1,250),
 ('CC','Cocos (Keeling) Islands',1,250);
INSERT INTO country (code, name, enabled, rank) VALUES  ('CF','Central African Republic',1,250),
 ('CG','Congo',1,250),
 ('CH','Switzerland',1,250),
 ('CI','Cote D\'Ivoire',1,250),
 ('CK','Cook Islands',1,250),
 ('CL','Chile',1,250),
 ('CM','Cameroon',1,250),
 ('CN','China',1,250),
 ('CO','Colombia',1,250),
 ('CR','Costa Rica',1,250),
 ('CU','Cuba',1,250),
 ('CV','Cape Verde',1,250),
 ('CX','Christmas Island',1,250),
 ('CY','Cyprus',1,250),
 ('CZ','Czech Republic',1,250),
 ('DE','Germany',1,250),
 ('DJ','Djibouti',1,250),
 ('DK','Denmark',1,250),
 ('DM','Dominica',1,250),
 ('DO','Dominican Republic',1,250),
 ('DZ','Algeria',1,250),
 ('EC','Ecuador',1,250),
 ('EE','Estonia',1,250),
 ('EG','Egypt',1,250),
 ('EH','Western Sahara',1,250),
 ('ER','Eritrea',1,250),
 ('ES','Spain',1,250),
 ('ET','Ethiopia',1,250),
 ('FI','Finland',1,250),
 ('FJ','Fiji',1,250),
 ('FK','Falkland Islands (Malvinas)',1,250),
 ('FM','Micronesia, Federated States of',1,250),
 ('FO','Faroe Islands',1,250),
 ('FR','France',1,250),
 ('FX','France, Metropolitan',1,250),
 ('GA','Gabon',1,250);
INSERT INTO country (code, name, enabled, rank) VALUES  ('GB','United Kingdom',1,250),
 ('GD','Grenada',1,250),
 ('GE','Georgia',1,250),
 ('GF','French Guiana',1,250),
 ('GH','Ghana',1,250),
 ('GI','Gibraltar',1,250),
 ('GL','Greenland',1,250),
 ('GM','Gambia',1,250),
 ('GN','Guinea',1,250),
 ('GP','Guadeloupe',1,250),
 ('GQ','Equatorial Guinea',1,250),
 ('GR','Greece',1,250),
 ('GS','South Georgia and the South Sandwich Islands',1,250),
 ('GT','Guatemala',1,250),
 ('GU','Guam',1,250),
 ('GW','Guinea-bissau',1,250),
 ('GY','Guyana',1,250),
 ('HK','Hong Kong',1,250),
 ('HM','Heard and Mc Donald Islands',1,250),
 ('HN','Honduras',1,250),
 ('HR','Croatia',1,250),
 ('HT','Haiti',1,250),
 ('HU','Hungary',1,250),
 ('ID','Indonesia',1,250),
 ('IE','Ireland',1,250),
 ('IL','Israel',1,250),
 ('IN','India',1,250),
 ('IO','British Indian Ocean Territory',1,250),
 ('IQ','Iraq',1,250),
 ('IR','Iran (Islamic Republic of)',1,250),
 ('IS','Iceland',1,250),
 ('IT','Italy',1,250),
 ('JM','Jamaica',1,250),
 ('JO','Jordan',1,250),
 ('JP','Japan',1,250);
INSERT INTO country (code, name, enabled, rank) VALUES  ('KE','Kenya',1,250),
 ('KG','Kyrgyzstan',1,250),
 ('KH','Cambodia',1,250),
 ('KI','Kiribati',1,250),
 ('KM','Comoros',1,250),
 ('KN','Saint Kitts and Nevis',1,250),
 ('KP','Korea, Democratic People\'s Republic of',1,250),
 ('KR','Korea, Republic of',1,250),
 ('KW','Kuwait',1,250),
 ('KY','Cayman Islands',1,250),
 ('KZ','Kazakhstan',1,250),
 ('LA','Lao People\'s Democratic Republic',1,250),
 ('LB','Lebanon',1,250),
 ('LC','Saint Lucia',1,250),
 ('LI','Liechtenstein',1,250),
 ('LK','Sri Lanka',1,250),
 ('LR','Liberia',1,250),
 ('LS','Lesotho',1,250),
 ('LT','Lithuania',1,250),
 ('LU','Luxembourg',1,250),
 ('LV','Latvia',1,250),
 ('LY','Libya',1,250),
 ('MA','Morocco',1,250),
 ('MC','Monaco',1,250),
 ('MD','Moldova, Republic of',1,250),
 ('MG','Madagascar',1,250),
 ('MH','Marshall Islands',1,250),
 ('MK','Macedonia, The Former Yugoslav Republic of',1,250),
 ('ML','Mali',1,250),
 ('MM','Myanmar',1,250),
 ('MN','Mongolia',1,250),
 ('MO','Macau',1,250),
 ('MP','Northern Mariana Islands',1,250);
INSERT INTO country (code, name, enabled, rank) VALUES  ('MQ','Martinique',1,250),
 ('MR','Mauritania',1,250),
 ('MS','Montserrat',1,250),
 ('MT','Malta',1,250),
 ('MU','Mauritius',1,250),
 ('MV','Maldives',1,250),
 ('MW','Malawi',1,250),
 ('MX','Mexico',1,250),
 ('MY','Malaysia',1,250),
 ('MZ','Mozambique',1,250),
 ('NA','Namibia',1,250),
 ('NC','New Caledonia',1,250),
 ('NE','Niger',1,250),
 ('NF','Norfolk Island',1,250),
 ('NG','Nigeria',1,250),
 ('NI','Nicaragua',1,250),
 ('NL','Netherlands',1,250),
 ('NO','Norway',1,250),
 ('NP','Nepal',1,250),
 ('NR','Nauru',1,250),
 ('NU','Niue',1,250),
 ('NZ','New Zealand',1,250),
 ('OM','Oman',1,250),
 ('PA','Panama',1,250),
 ('PE','Peru',1,250),
 ('PF','French Polynesia',1,250),
 ('PG','Papua New Guinea',1,250),
 ('PH','Philippines',1,250),
 ('PK','Pakistan',1,250),
 ('PL','Poland',1,250),
 ('PM','St. Pierre and Miquelon',1,250),
 ('PN','Pitcairn',1,250),
 ('PR','Puerto Rico',1,250),
 ('PT','Portugal',1,250),
 ('PW','Palau',1,250),
 ('PY','Paraguay',1,250),
 ('QA','Qatar',1,250),
 ('RE','Reunion',1,250);
INSERT INTO country (code, name, enabled, rank) VALUES  ('RO','Romania',1,250),
 ('RU','Russian Federation',1,250),
 ('RW','Rwanda',1,250),
 ('SA','Saudi Arabia',1,250),
 ('SB','Solomon Islands',1,250),
 ('SC','Seychelles',1,250),
 ('SD','Sudan',1,250),
 ('SE','Sweden',1,250),
 ('SG','Singapore',1,250),
 ('SH','St. Helena',1,250),
 ('SI','Slovenia',1,250),
 ('SJ','Svalbard and Jan Mayen Islands',1,250),
 ('SK','Slovakia (Slovak Republic)',1,250),
 ('SL','Sierra Leone',1,250),
 ('SM','San Marino',1,250),
 ('SN','Senegal',1,250),
 ('SO','Somalia',1,250),
 ('SR','Suriname',1,250),
 ('ST','Sao Tome and Principe',1,250),
 ('SV','El Salvador',1,250),
 ('SY','Syrian Arab Republic',1,250),
 ('SZ','Swaziland',1,250),
 ('TC','Turks and Caicos Islands',1,250),
 ('TD','Chad',1,250),
 ('TF','French Southern Territories',1,250),
 ('TG','Togo',1,250),
 ('TH','Thailand',1,250),
 ('TJ','Tajikistan',1,250),
 ('TK','Tokelau',1,250),
 ('TM','Turkmenistan',1,250),
 ('TN','Tunisia',1,250),
 ('TO','Tonga',1,250),
 ('TP','East Timor',1,250),
 ('TR','Turkey',1,250);
INSERT INTO country (code, name, enabled, rank) VALUES  ('TT','Trinidad and Tobago',1,250),
 ('TV','Tuvalu',1,250),
 ('TW','Taiwan',1,250),
 ('TZ','Tanzania, United Republic of',1,250),
 ('UA','Ukraine',1,250),
 ('UG','Uganda',1,250),
 ('UM','United States Minor Outlying Islands',1,250),
 ('US','United States',1,1),
 ('UY','Uruguay',1,250),
 ('UZ','Uzbekistan',1,250),
 ('VA','Vatican City State (Holy See)',1,250),
 ('VC','Saint Vincent and the Grenadines',1,250),
 ('VE','Venezuela',1,250),
 ('VG','Virgin Islands (British)',1,250),
 ('VI','Virgin Islands (U.S.)',1,250),
 ('VN','Viet Nam',1,250),
 ('VU','Vanuatu',1,250),
 ('WF','Wallis and Futuna Islands',1,250),
 ('WS','Samoa',1,250),
 ('YE','Yemen',1,250),
 ('YT','Mayotte',1,250),
 ('YU','Yugoslavia',1,250),
 ('ZA','South Africa',1,250),
 ('ZM','Zambia',1,250),
 ('ZR','Zaire',1,250),
 ('ZW','Zimbabwe',1,250);
UNLOCK TABLES;

LOCK TABLES state WRITE;
INSERT INTO state (code, country, name, tax_rate) VALUES  ('AK', 'US', 'Alaska',0),
 ('AL','US','Alabama',0),
 ('AR','US','Arkansas',0),
 ('AZ','US','Arizona',0),
 ('CA','US','California',0),
 ('CO','US','Colorado',0),
 ('CT','US','Connecticut',0),
 ('DC','US','District of Columbia',0),
 ('DE','US','Delaware',0),
 ('FL','US','Florida',0),
 ('GA','US','Georgia',0),
 ('HI','US','Hawaii',0),
 ('IA','US','Iowa',0),
 ('ID','US','Idaho',0),
 ('IL','US','Illinois',0),
 ('IN','US','Indiana',0),
 ('KS','US','Kansas',0),
 ('KY','US','Kentucky',0),
 ('LA','US','Louisiana',0),
 ('MA','US','Massachusetts',0),
 ('MD','US','Maryland',0),
 ('ME','US','Maine',0),
 ('MI','US','Michigan',0),
 ('MN','US','Minnesota',0),
 ('MO','US','Missouri',0),
 ('MS','US','Mississippi',0),
 ('MT','US','Montana',0),
 ('NC','US','North Carolina',0),
 ('ND','US','North Dakota',0),
 ('NE','US','Nebraska',0),
 ('NH','US','New Hampshire',0),
 ('NJ','US','New Jersey',0),
 ('NM','US','New Mexico',0),
 ('NV','US','Nevada',0),
 ('NY','US','New York',0),
 ('OH','US','Ohio',0),
 ('OK','US','Oklahoma',0),
 ('OR','US','Oregon',0),
 ('PA','US','Pennsylvania',0),
 ('RI','US','Rhode Island',0),
 ('SC','US','South Carolina',0),
 ('SD','US','South Dakota',0),
 ('TN','US','Tennessee',0),
 ('TX','US','Texas',0),
 ('UT','US','Utah',0),
 ('VA','US','Virginia',0),
 ('VT','US','Vermont',0),
 ('WA','US','Washington',0),
 ('WI','US','Wisconsin',0),
 ('WV','US','West Virginia',0),
 ('WY','US','Wyoming',0),
 ('PR','US','Puerto Rico',0),
 ('VI','US','Virgin Islands',0),
 ('MP','US','Northern Marina Islands',0),
 ('GU','US','Guam',0),
 ('AS','US','American Samoa',0),
 ('PW','US','Palau',0),
 ('AA','US','Armed Forces(AA)',0),
 ('AE','US','Armed Forces(AE)',0),
 ('AP','US','Armed Forces(AP)',0);
INSERT INTO state (code, country, name, tax_rate) VALUES  
 ('AB','CA','Alberta',0),
 ('BC','CA','British Columbia',0),
 ('MB','CA','Manitoba',0),
 ('NB','CA','New Brunswick',0),
 ('NL','CA','New Foundland and Labrador',0),
 ('NT','CA','Northwest Territory',0),
 ('NS','CA','Nova Scotia',0),
 ('NU','CA','Nunavut',0),
 ('ON','CA','Ontario',0), 
 ('PE','CA','Prince Edward Island',0),
 ('QC','CA','Quebec',0),
 ('SK','CA','Saskatchewan',0), 
 ('YT','CA','Yukon Territory',0); 
UNLOCK TABLES;

/* Military 
INSERT INTO state VALUES 
 ('AA', 'US','Armed Forces Americas',0),
 ('AE','US','Armed Forces Europe',0),
 ('AP','US','Armed Forces Pacific',0);
*/

CREATE TABLE site_config (
  config_key varchar(64) NOT NULL,
  config_value varchar(1024) NOT NULL,
  PRIMARY KEY (config_key)
) ENGINE = InnoDB;

LOCK TABLES site_config WRITE;
INSERT INTO site_config VALUES  
 ('SITE_TITLE','WebJaguar'),
 ('SITE_NAME','WebJaguar.net'),
 ('CONTACT_EMAIL',''),
 ('CUSTOM_REG_EXP_EMAIL',''),
 ('SITE_URL','http://www.webjaguar.net/'),
 ('SECURE_URL','https://www.webjaguar.net/'),
 ('SUBCAT_LEVELS','1'),
 ('SUBCAT_MAX','20'),
 ('MAS90',''),
 ('MAS200',''),
 ('DSI',''),
 ('DSI_PRICETABLE_CHANGE',''),
 ('INGRAM',''),
 ('INGRAM_EXTRA',''),
 ('INGRAM_PRICETABLE1_CHANGE',''),
 ('INGRAM_PRICETABLE2_CHANGE',''),
 ('INGRAM_PRICETABLE3_CHANGE',''),
 ('INGRAM_PRICETABLE4_CHANGE',''),
 ('INGRAM_PRICETABLE5_CHANGE',''),
 ('INGRAM_CUSTOM_FORMULA_MARGIN',''),
 ('TRIPLEFIN',''),
 ('EVERGREEN',''),
 ('PREMIER_DISTRIBUTOR',''),
 ('CONCORD_PRICECHANGE',''),
 ('CONCORD',''),
 ('FRAGRANCENET_PRICECHANGE',''),
 ('FRAGRANCENET_PRICETABLE1_CHANGE',''),
 ('FRAGRANCENET_PRICETABLE2_CHANGE',''),
 ('FRAGRANCENET_PRICETABLE3_CHANGE',''),
 ('FRAGRANCENET',''),
 ('TECHDATA',''),
 ('TECHDATA_EXTRA',''),
 ('SYNNEX',''),
 ('SYNNEX_EXTRA',''),
 ('ETILIZE',''),
 ('BUSINESS_VISION',''),
 ('PLOW_AND_HEARTH',''),
 ('CARTSPAN_API_CODE',''),
 ('KOLE_IMPORTS',''),
 ('DSDI_FILE_LINK',''),
 ('WATERMARK_TEXT','put watermark text here'),
 ('WATERMARK_FONT_SIZE','12'),
 ('WATERMARK_FONT_COLOR_R','0'),
 ('WATERMARK_FONT_COLOR_G','0'),
 ('WATERMARK_FONT_COLOR_B','0'),
 ('SEARCH_ACCOUNT',''),
 ('SEARCH_NOTHING_FOUND_MESSAGE',''),
 ('SEARCH_DEFAULT_KEYWORDS_CONJUNCTION','OR'),
 ('SEARCH_KEYWORDS_DELIMITER',''),
 ('END_DELIMITER_PRESENT', 'true'), 
 ('SEARCH_KEYWORDS_MINIMUM_CHARACTERS','3'), 
 ('SEARCH_SORT','name'), 
 ('SEARCH_KEYWORDS_CONJUNCTION','true'),
 ('SEARCH_MINMAXPRICE','true'), 
 ('SEARCH_FIELDS',''), 
 ('SEARCH_FIELDS_MULTIPLE',''),
 ('SEARCH_INFLECTOR','0'),
 ('GROUP_SEARCH',''), 
 ('GROUP_PRODUCTS_QUOTE',''),
 ('SOURCE_STATEPROVINCE',''),
 ('SOURCE_ZIP',''),
 ('SOURCE_COUNTRY',''),
 ('SOURCE_CITY',''),
 ('SOURCE_ADDRESS',''),
 ('PAYPAL_BUSINESS',''),
 ('PAYPAL_URL','https://www.paypal.com/'),
 ('PAYPAL_RETURN',''),  
 ('PAYPAL_CANCEL_RETURN',''),
 ('PAYPAL_PAYMENT_METHOD_NOTE',''),
 ('PAYPALPRO_ENVIRONMENT',''),
 ('EBILLME_MERCHANTTOKEN',''),
 ('EBILLME_PAYEETOKEN',''),
 ('EBILLME_UID',''),
 ('EBILLME_PWD',''),
 ('EBILLME_CANCEL_URL',''),
 ('EBILLME_ERROR_URL',''),
 ('EBILLME_URL',''),
 ('AMAZON_MERCHANT_ID',''),
 ('AMAZON_ACCESS_KEY',''),
 ('AMAZON_SECRET_KEY',''),
 ('AMAZON_URL',''),
 ('AMAZON_MERCHANT_TOKEN',''),
 ('AMAZON_MERCHANT_NAME',''),
 ('AMAZON_EMAIL',''),
 ('AMAZON_PASSWORD',''),
 ('AMAZON_ORDER_ALLOW_EDIT',''),
 ('CENTINEL_URL',''),
 ('CENTINEL_PROCESSORID',''),
 ('CENTINEL_MERCHANTID',''),
 ('CENTINEL_TRANSACTIONPWD',''),
 ('GEMONEY_DOMAIN',''),
 ('GEMONEY_MERCHANTID',''),
 ('GEMONEY_PASSWORD',''),
 ('GEMONEY_PROGRAM_NAME','GE Money'),
 ('GEMONEY_PROMOCODE','0'),
 ('CREDIT_ACCOUNT',''),
 ('NET_COMMERCE_MD5_KEY',''),
 ('CREDIT_PASSWORD',''),
 ('CREDIT_AUTO_CHARGE',''),
 ('CREDIT_MERCHANT_ID',''),
 ('CREDIT_BIN',''),
 ('AUTHORIZE_TEST_MODE','false'),
 ('INTERNATIONAL_CHECKOUT',''),
 ('INTERNATIONAL_CHECKOUT_P_VALUE',''),
 ('CC_FAILURE_URL',''),
 ('SITE_MESSAGE_ID_FOR_NEW_REGISTRATION','1'),
 ('SITE_MESSAGE_ID_FOR_NEW_ORDERS','2'),
 ('SITE_MESSAGE_ID_FOR_BUDGET_ORDERS',''),
 ('SITE_MESSAGE_ID_FOR_SHIPPED_ORDERS','0'),
 ('SITE_MESSAGE_ID_FOR_SUPPLIER_ORDERS',''),
 ('SITE_MESSAGE_ID_FOR_SALES_REP',''),
 ('SITE_MESSAGE_ID_FOR_WORLD_SHIP',''),
 ('SITE_MESSAGE_ID_FOR_NEW_QUOTE',''),
 ('SITE_MESSAGE_ID_FOR_SALESREP_NOTIFICATION',''),
 ('SITE_MESSAGE_ID_FOR_SALESREP_NOTIFICATION_CONTACT',''),
 ('SITE_MESSAGE_ID_FOR_GIFT_CARD','6'),
 ('SITE_MESSAGE_ID_FOR_COUPON_CODE',''),
 ('SITE_MESSAGE_ID_FOR_SUPPLIER_PAYMENT',''),
 ('SITE_MESSAGE_ID_FOR_AFFILIATE_PAYMENT',''),
 ('ORDER_FULFILMENT',''),
 ('SITE_MESSAGE_ID_FOR_FEDEX_SHIPPING_LABEL',''),
 ('WORLD_SHIP_EMAIL_NOTIFICATION','false'),
 ('WORLD_SHIP_IMPORT_ORDERS',''),
 ('FEDEX_SHIPPING_LABEL_EMAIL_NOTIFICATION',''),
 ('COMPANY_NAME',''),
 ('COMPANY_CONTACT_FIRSTNAME',''),
 ('COMPANY_CONTACT_LASTNAME',''),
 ('COMPANY_CONTACT_PERSON',''),
 ('COMPANY_ADDRESS1',''),
 ('COMPANY_ADDRESS2',''),
 ('COMPANY_CITY',''),
 ('COMPANY_STATE_PROVINCE',''),
 ('COMPANY_ZIPCODE',''),
 ('COMPANY_PHONE',''),
 ('COMPANY_FAX',''),
 ('COMPANY_COUNTRY',''),
 ('SHOW_SKU','true'),
 ('QUOTE_ADMIN_EMAIL_SUBJECT',''),
 ('QUICK_MODE_THUMB_HEIGHT','0'),
 ('QUICK_MODE_SHORT_DESC_TITLE','Description'),
 ('DETAILS_IMAGE_WIDTH','300'),
 ('DETAILS_IMAGE_LOCATION','left'),
 ('DETAILS_SHORT_DESC','true'),
 ('DETAILS_LONG_DESC_LOCATION',''),
 ('DETAILS_LONG_DESC_HTML',''),
 ('PRODUCT_FIELDS_TITLE','Specification'),
 ('PRODUCT_IMAGE_LAYOUTS',''),
 ('PRODUCT_IMAGE_LAYOUT',''),
 ('PRODUCT_QTY_DROPDOWN',''),
 ('CATEGORY_DISPLAY_MODE','1'),
 ('CATEGORY_DISPLAY_MODE_GRID_LIST',''),
 ('SEARCH_DISPLAY_MODE','quick'),
 ('NUMBER_PRODUCT_PER_ROW','1'),
 ('NUMBER_PRODUCT_PER_PAGE','10'),
 ('NUMBER_PRODUCT_PER_PAGE_SEARCH','10'),
 ('CUSTOMERS_REQUIRED_COMPANY','false'),
 ('ADDRESS_RESIDENTIAL_COMMERCIAL','1'),
 ('CASE_UNIT_TITLE','Unit'),
 ('NO_SHIPPING','false'),
 ('SHIPPING_PER_SUPPLIER','false'),
 ('RECOMMENDED_LIST_TITLE','Recommended Items'),
 ('ALSO_CONSIDER_TITLE','Also Consider'),
 ('ALSO_CONSIDER_THUMB_HEIGHT','50'),
 ('ALSO_CONSIDER_DETAILS_ONLY','false'),
 ('CURRENCY','USD'),
 ('MAX_FRACTION_DIGIT','2'),
 ('PRICE_ONSALE_TITLE','Sale Price'),
 ('PRICE_BEFORE_TITLE',''),
 ('EXCLUDE_FEEDTYPE_FROM_PRODUCT_EXPORT',''),
 ('SUBCAT_DISPLAY',''),
 ('RECOMMENDED_LIST_DISPLAY',''),
 ('RECOMMENDED_LIST_SHOW_BREADCRUMB','false'),
 ('INVOICE_MESSAGE',''),
 ('INVOICE_LINEITEMS_SORTING',''),
 ('BREADCRUMBS_HOME_TITLE','Home'),
 ('CUSTOM_TEXT_PRODUCT','false'),
 ('LEFTBAR_LAYOUT','1'),
 ('SHOW_CHILD_INVOICE','false'),
 ('LEFTBAR_HIDE_ON_SYSTEM_PAGES','false'),
 ('MYLIST_THUMBNAIL','false'),
 ('SUBCAT_PRODUCT_COUNT','true'),
 ('PRODUCT_SORTING','name'),
 ('LOCATION_SORTING','name'),
 ('PRODUCT_SORT_BY_FRONTEND','true'),
 ('SHARED_CATEGORIES_POSITION','2'),
 ('SHARED_CATEGORIES_LAYOUT','1'),
 ('MINIMUM_ORDER','false'),
 ('MINIMUM_ORDER_AMOUNT','0.0'),
 ('MINIMUM_ORDER_AMOUNT_WHOLESALE','0.0'),
 ('MINIMUM_MESSAGE','Minimum to order is #min# '),
 ('INCREMENTAL_MESSAGE','and in multiple of #inc#'),
 ('APPLY_MIN_INC_TO_CUSTOMER',''),
 ('CREDIT_CARD_PAYMENT','VISA,AMEX,MC,DISC'),
 ('NUMBER_PRODUCT_COMMISSION_TABLE','0'),
 ('COMPARISON_TITLE','Product Comparison'),
 ('COMPARISON_MAX','5'),
 ('THUMBNAIL_RESIZE','200'),
 ('COMPARISON_SHORT_DESC_TITLE','Description'),
 ('USPS_USERID',''),
 ('SHOW_ZERO_PRICE','true'),
 ('SERVICEABLE_ITEM_TITLE','Printer'), 
 ('CUSTOMER_CUSTOM_NOTE_1',''), 
 ('CUSTOMER_CUSTOM_NOTE_2',''),
 ('CUSTOMER_CUSTOM_NOTE_3',''),
 ('TRIGUIDE_URL','pxml.tgoservices.com'),
 ('PRODUCT_FIELDS_SEARCH_TYPE',''),
 ('ABOUT_US_ID',''),
 ('CONTACT_US_ID',''),
 ('SPECIAL_INSTRUCTIONS','Special Instructions'),
 ('PO_SPECIAL_INSTRUCTIONS','Purchase Order Special Instructions(To Supplier)'),
 ('CHANGE_ORDER_DATE','false'),
 ('CATEGORY_INTERSECTION','false'),
 ('CATEGORY_NOT_FOUND_URL',''),
 ('CATEGORY_OVERRIDE_CANONICAL',''),
 ('CUSTOMER_IP_ADDRESS','false'),
 ('SHIPPING_LTL','false'),
 ('MY_LIST_TITLE',''),
 ('MY_ORDER_LIST_TITLE',''),
 ('VIEW_MY_ORDER_LIST_TITLE',''),  
 ('MY_ORDER_HISTORY_STATUS_TITLE',''),  
 ('VIEW_MY_ORDER_HISTORY_STATUS_TITLE',''), 
 ('MSRP_TITLE',''),
 ('IP_ADDRESS_ERROR_MESSAGE','Error: Please contact Web Site Administrator to login.'),
 ('TICKET_CONTACT_EMAIL',''),
 ('TICKET_MESSAGE_ON_EMAIL','false'), 
 ('TICKET_INTERNAL_ACCESS_USER','false'),
 ('TICKET_TYPE',''),
 ('OUT_OF_STOCK_TITLE','<img border="0" src="assets/Image/Layout/button_outofstock.gif" />'),
 ('OUT_OF_STOCK_CONDITION','0'),
 ('LOW_STOCK_TITLE','Low Stock'),
 ('INVENTORY_ADJUST_QTY_TITLE','Quantity Adjusted!'),
 ('INVENTORY_ON_PRODUCT_IMPORT_EXPORT','false'),
 ('INVENTORY_UPDATE_FORM','false'),
 ('INVENTORY_SHOW_ONHAND','true'),
 ('INVENTORY_ON_HAND',''),
 ('ALLOW_EDIT_INVOICE_SHIPPED','false'),
 ('ALLOW_EDIT_PAYMENT_SHIPPED','false'),
 ('EXT_QUANTITY_TITLE',''),
 ('INVENTORY_DEDUCT_TYPE','ship'),
 ('GENERATE_PACKING_LIST','true'),
 ('PROTECTED_ACCESS_ANONYMOUS','0'),
 ('PROTECTED_ACCESS_NEW_REGISTRANT','0'),
 ('NEW_REGISTRANT_PRICE_PROTECTED_ACCESS','0'),
 ('PROTECTED_HOST',''),
 ('PROTECTED_HOST_PROTECTED_LEVEL','0'),
 ('INVOICE_EXPORT',''),
 ('PRODUCT_EXPORT','xls'),
 ('INVOICE_PDF_UPLOAD','false'),
 ('GIFTCARD_MIN_AMOUNT','5'),
 ('GIFTCARD_MAX_AMOUNT','500'),
 ('POINT_TO_CREDIT','false'),
 ('SHOW_IMAGE_ON_INVOICE','true'),
 ('CONFIRM_USERNAME','false'),
 ('CURRENT_PASSWORD_CHECK','true'),
 ('SHOW_CC_ON_PRINT_FRIENDLY','false'),
 ('SHOW_PAYMENT_HISTORY_ON_PRINT_FRIENDLY','false'), 
 ('DEFAULT_LIST_SIZE','10'),
 ('FEDEX_SIGNATURE_TYPE','NO_SIGNATURE_REQUIRED'),
 ('FEDEX_DROP_OFF_TYPE','REGULAR_PICKUP'),
 ('FEDEX_PACKAGING_TYPE','YOUR_PACKAGING'),
 ('FEDEX_ACCOUNT_NUMBER', '248976535'),
 ('PACKAGING_DIMENSION_HEIGHT',''),
 ('PACKAGING_DIMENSION_WIDTH',''),
 ('PACKAGING_DIMENSION_LENGTH',''),
 ('PACKAGING_DIMENSION_UNIT','in'),
 ('FEDEX_USER_KEY',''),
 ('FEDEX_USER_METERNUMBER',''),
 ('FEDEX_USER_PASSWORD',''),
 ('FEDEX_SATURDAY_DELIVERY','false'),
 ('TRACK_CODE',''),
 ('ORDER_OVERVIEW','false'),
 ('ORDER_ADD_VALID_SKU','false'),
 ('ORDER_FLAG1_NAME','Flag1'),
 ('ORDER_CUSTOM_FIELD1_NAME',''),
 ('ORDER_CUSTOM_FIELD1_VALUE',''),
 ('ORDER_CUSTOM_FIELD2_NAME',''),
 ('ORDER_CUSTOM_FIELD2_VALUE',''),
 ('ORDER_TRACKCODE_REQUIRED','false'),
 ('ORDER_TYPE','walkIn,internet,phone,email,other'),
 ('SHIPPING_TURNOVER_TIME','0'),
 ('TRACK_CODE_PROTECTED_ACCESS_NEW_REGISTRANT','0'),
 ('PRODUCT_DETAIL_LAYOUT','000'),
 ('PRODUCT_DEFAULT_DETAIL_LAYOUT','000'),
 ('SUPPLIER_PRODUCT_CATEGORY','0'),
 ('BUY_REQUEST_CATEGORY_ID','0'),
 ('SITE_TIME_ZONE',''),
 ('AUTO_IMPORT_SERVER',''),
 ('AUTO_IMPORT_FILENAME',''),
 ('AUTO_IMPORT_USERNAME',''),
 ('AUTO_IMPORT_PASSWORD',''),
 ('AUTO_IMPORT_DELETE',''),
 ('AUTO_IMPORT_EMAIL',''),
 ('AUTO_IMPORT_CUSTOMER',''),
 ('AUTO_SHOPPINGCART_EXPORT',''),
 ('COOKIE_AGE','-1'),
 ('CUSTOM_SHIPPING_FLAG',''),
 ('CUSTOM_SHIPPING_FIELD1',''),
 ('CUSTOM_SHIPPING_FIELD2',''),
 ('CUSTOM_SHIPPING_TITLE1',''),
 ('CUSTOM_SHIPPING_TITLE2',''),
 ('CUSTOM_SHIPPING_FIXED_COST',''),
 ('LOGIN_SUCCESS_URL','account.jhtm'),
 ('COOKIE_AGE_KEEP_ME_LOGIN','-1'),
 ('REGISTRATION_SUCCESS_URL','account.jhtm'),
 ('DELIVERY_TIME','false'),
 ('EDIT_EMAIL_ON_FRONTEND','true'),
 ('EDIT_ADDRESS_ON_FRONTEND','true'), 
 ('ADD_ADDRESS_ON_CHECKOUT','true'),
 ('ADMIN_MESSAGE',''),
 ('NO_COMMISSION_FLAG', ''),
 ('MASS_EMAIL_POINT', 50),
 ('MASS_EMAIL_JOB_NAME', ''),
 ('MASS_EMAIL_UNIQUEPER_ORDER','false'),
 ('MASS_EMAIL_CONTACT',''),
 ('SHOW_GRANDTOTAL_BILLING_METHOD',''),
 ('UPS_USERID','bachir'),
 ('UPS_PASSWORD','aemsol'),
 ('UPS_ACCESS_LICENSE_NUMBER','EBEE3E934BADAE08'),
 ('UPS_PICKUPTYPE','01'),
 ('UPS_CALCULATOR',''),
 ('UPS_SHIPPER_NUMBER',''),
 ('GOOGLE_MAP_KEY',''),
 ('ATTACHE_PDF_ON_PLACE_ORDER','false'),
 ('PRODUCT_SYNC',''),
 ('TERRITORY_ZIPCODE','false'),
 ('SALES_REP_SEQUENTIAL_ASSIGNEMENT','false'),
 ('BRAND_MIN_ORDER_MESSAGE',''),
 ('PRODUCT_IMPORT_EMAIL',''),
 ('PRODUCT_IMPORT_FILENAME',''),
 ('RECAPTCHA_PRIVATE_KEY',''),
 ('DEFAULT_CREDITCARD_FEE_RATE','0.0'),
 ('DEFAULT_ORDER_TYPE','internet'),
 ('LUCENE_REINDEX',''),
 ('LUCENE_GROUP_FIELDS_INTO_ONE',''),
 ('LUCENE_RELEVANCE_SEARCH',''),
 ('LUCENE_SEARCH_ON_MAX_PRICE_FILTER_ONLY',''),
 ('LUCENE_SEARCH_EXACT_MIN_QTY',''),
 ('LUCENE_PROXIMITY_VALUE','50'),
 ('LUCENE_FILTERS_RANK',''),
 ('LUCENE_SEARCH_SHOW_INDEXED_PRICE',''),
 ('DEFAULT_LAYOUT_ID','1'),
 ('SAVE_LAYOUT_ON_COOKIE','false'),
 ('ADMIN_LANDING_PAGE','catalog/categoryList.jhtm'),
 ('HTML_SCROLLER','false'),
 ('HTML_SCROLLER_WITH_PRICE','false'),
 ('ASI',''),
 ('ASI_ENDQTYPRICING',''),
 ('ASI_ENDQTYPRICING_CUSTOMER',''),
 ('ASI_FEED_PRODUCT_TOKEN',''),
 ('ASI_FEED_SUPPLIER_TOKEN',''),
 ('ASI_PRICE_MARK_UP_FOR_ALL_PRODUCTS',''),
 ('ASI_PRICE_MARK_UP_TYPE','0'),
 ('ASI_PRICE_MARK_UP_FORMULA','0'),
 ('ASI_PRICE_MARK_UP_CHANGE_BASE_PRICE','true'),
 ('ASI_KEY',''),
 ('ASI_SECRET',''),
 ('ASI_PRODUCTS_CATEGORY',''),
 ('ASI_POPUP_DISPLAY', 'false'),
 ('CXML_UNSPSC',''),
 ('SUPPLIER_MULTI_ADDRESS','false'),
 ('PO_FLAG1_NAME','Flag1'),
 ('PO_FLAG1_PREVALUE','Yes,No'),
 ('PO_REMOVE_CUSTOMER_CONTACT','false'),
 ('MASTERSKU_OUT_OF_STOCK_URL',''),
 ('SUBACCOUNT_LOGINAS_FRONTEND','false'),
 ('LOYALTY_SHOW_ON_MYACCOUNT','true'),
 ('USER_EXPECTED_DELIVERY_TIME','false'),
 ('REQUESTED_CANCEL_DATE','false'),
 ('GOOGLE_CURRENCY_CONVERTER','false'),
 ('CUSTOM_SHIPPING_CONTACT_INFO','false'),
 ('SALESREP_LOGIN','false'),
 ('SALESREP_PARENT',''),
 ('SALESREP_PROTECTED_ACCESS',''),
 ('APPLIANCE_SPEC_URL',''),
 ('APPLIANCE_SPEC_BRANDNAME',''),
 ('CUSTOMIZE_CUSTOMER_LIST','firstName,lastName,email,company,accountNumber,phoHIDEne,taxxId,created,imported,logins,orders,trackCode,salesRep,multiStore,registeredBy,quote'),
 ('CUSTOMIZE_ORDER_LIST','billedTo,company,orderNumber,po,dateOrdered,subTotal,grandTotal,amountPaid,paymentMethod,approval,status,dateShipped,trackCode,salesRep,multiStore,workOrder'),
 ('CUSTOMIZE_SUBACCOUNT_LIST','email,lastName,firstName,accountNumber,companyName,orders,quote,orderTotal,subAccounts,brand,loginAs'),
 ('CUSTOMIZE_VIEW_MY_ORDER_HISTORY_LIST','orderDate,invoice,salesOrder,po,status,substatus,total,reorder,payment,srhipDate,trrackcode,carrrier'),
 ('SHOPATRON',''),
 ('SHOPATRON_REQUIRE_LOGIN','true'),
 ('SHOW_COST','false'),
 ('SITEMAP','false'),
 ('SUB_STATUS','0'),
 ('SUB_STATUS_ON_FRONTEND',''),
 ('SHOPPINGCART_SEARCH','false'),
 ('SHOPPING_CART_RECOMMENDED_LIST','false'),
 ('PRODUCT_REVIEW_TYPE','login'),
 ('PRODUCT_RATE','false'),
 ('PRODUCT_REVIEW_THIRDPARTY','false'),
 ('PRODUCT_REVIEW_DEFAULT','true'),
 ('PRODUCT_SEARCHABLE_DEFAULT','true'),
 ('PRODUCT_REVIEW_EMAIL',''),
 ('PRODUCT_PAGE_SIZE','10,20,25,50,100'),
 ('POWER_REVIEW_XML',''),
 ('EVENT_LIST',''),
 ('EDIT_COMMISSION','false'),
 ('UPSELL_RECOMMENDED_LIST','true'),
 ('UPSELL_DEFAULT_RECOMMENDED_LIST',''),
 ('ZERO_PRICE_HTML',''),
 ('CUSTOMER_MASS_EMAIL','false'),
 ('CUSTOMER_MASS_EMAIL_SUBSCRIBE_DEFAULT','false'),
 ('CUSTOMER_PASSWORD_RESET',''),
 ('CUSTOMER_PASSWORD_LENGTH','5'),
 ('CUSTOMER_PASSWORD_STRONG','0'),
 ('CUSTOMER_REG_EXP_EMAIL',''),
 ('CUSTOMER_RATING_1','Good,Average,Bad'),
 ('CUSTOMER_RATING_2','1,2,3,4,5,6,7,8,9,10'),
 ('CHANGE_PO_DATE','false'),
 ('BATCH_PRINT_INVOICE','false'),
 ('BATCH_PRINT_PRODUCT_LABEL','false'),
 ('ECHOSIGN_API_KEY',''),
 ('ECHOSIGN_USER_KEY',''),  
 ('TAX_ID_REQUIRED','false'),
 ('TAX_ID_ON_PRINT_INVOICE',''),
 ('CRM_CUSTOMER_SYNCH','false'),
 ('GOOGLE_CHECKOUT_URL',''),
 ('GOOGLE_MERCHANT_KEY',''),
 ('GOOGLE_MERCHANT_ID',''),
 ('DEFAULT_FAQ_LIST_SIZE','20'),
 ('ACCESS_PRIVILEGE_GROUP','false'),
 ('SHOPWORKS','false'),
 ('LEFTBAR_DEFAULT_TYPE','1'),
 ('LEFTBAR_TYPE','1'),
 ('ACCORDION_TYPE','0'),
 ('DOCTYPE','<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">'),
 ('CUSTOMIZE_MYLIST','name,sku,shortDescription,price,dateAdded,quantity'),
 ('TAX_ID_NOTE','Note: A sales tax may apply unless we have a copy of your sellers permit. Fax a copy of your sellers permit to xxxxx.'),
 ('SUGAR_SYNC_CREDENTIALS',''),
 ('SUGAR_SYNC_AUTHENTICATION',''),
 ('FREESHIPPING_OPTIONCODE',''),
 ('PRODUCT_INACTIVE_REDIRECT_FIELD',''),
 ('NOTE_ON_REGISTRATION',''),
 ('BUY_SAFE_URL',''),
 ('BUY_SAFE_USERNAME',''),
 ('BUY_SAFE_PASSWORD',''),
 ('BUY_SAFE_VERSION',''),
 ('BUY_SAFE_STORE_TOKEN',''),
 ('CUSTOM_SHIPPING_LIMIT','10'),
 ('GIFT_CARD_THANK_YOU_ID',''),
 ('MOOTOOLS','false'),
 ('MINI_CART','false'),
 ('PRODUCT_QUICK_VIEW','false'),
 ('PRODUCT_QUICK_MODE_2','false'),
 ('PRODUCT_QUICK_MODE_3','false'),
 ('INVENTORY_HISTORY',''),
 ('HORIZONTAL_DYNAMIC_MENU',''),
 ('PRODUCT_FIELDS_ON_INVOICE_EXPORT',''),
 ('CREDIT_CARD_ON_INVOICE_EXPORT',''),
 ('CUSTOMER_LOCATOR_FIELD',''),
 ('CUSTOMER_FIELDS_ON_INVOICE_EXPORT',''),
 ('PRODUCT_QUOTE','false'),
 ('TINYMCE','false'),
 ('TEMPLATE','template4'),
 ('CASE_CONTENT_LAYOUT',''),
 ('MOD_REWRITE_PRODUCT','product'),
 ('MOD_REWRITE_CATEGORY','category'),
 ('WO_ALERT_INTERVAL_CUSTOMER_FIELD',''), 
 ('WO_ALERT_INTERVAL_DEFAULT','2'), 
 ('CHECKOUT2_VISIBILITY','-1'),
 ('CHECKOUT_DIRECTORY',''),
 ('CHECKOUT1_DIRECTORY',''),
 ('PRODUCT1_DIRECTORY',''),
 ('CATEGORY1_DIRECTORY',''),
 ('TECHNOLOGO',''),
 ('THUB_AUTHENTICATION',''),
 ('RR_ACTUAL_CLASS',''),
 ('RR_ORIGIN_TYPE',''),
 ('RR_ORIGIN_ZIP',''),
 ('RR_PAYMENT_TYPE',''),
 ('RR_USERNAME',''),
 ('RR_PASSWORD',''),
 ('INVOICE_REMOVE_VALIDATION','true'),
 ('SMART_DROPDOWN',''),
 ('COMPARISON_ON_CATEGORY', ''),
 ('STONEEDGE_AUTHENTICATION', ''),
 ('PRODUCT_MULTI_OPTION','0'),
 ('CREATE_TASK_ON_EMAIL', ''),
 ('DATA_FEED_MEMBER_TYPE', ''),
 ('SALES_TAG_ON_PRICE_TABLE', ''),
 ('SALES_TAG_COUNTDOWN', ''),
 ('CDS_AND_DVDS', ''),
 ('FEED_NAME', 'Webjaguar'),
 ('ADD_PRODUCT_ON_FRONTEND','false'),
 ('PRODUCT_SEARCH_NOT_FOUND_URL',''),
 ('MASTER_SKU_PRODUCT_FIELD',''),
 ('SEARCH_RESULT_DISPALY_FIELD_ID',''),
 ('APPLY_PARENT_SKU_OPTION',''),
 ('COLOR_FIELD_ID',''),
 ('SIZE_FIELD_ID',''),
 ('CONSIGNMENT_NOTE_FIELD','field_1'),
 ('VBA_PAYMENT_OPTIONS',''),
 ('BUDGET_PERCENTAGE',''),
 ('BUDGET_ADD_PARTNER','false'),
 ('PARENT_CHILDERN_DISPLAY','0'),
 ('COST_TIERS','false'),
 ('PROMO_ON_SHOPPING_CART','false'),
 ('PRODUCT_CONFIGURATOR', 'false'),
 ('PAYMENT_ALERT_START_DATE',''),
 ('PCI_FRIENDLY_EBIZCHARGE','false'),
 ('QUOTE_VIEW_LIST_THANK_YOU_ID',''),
 ('CUSTOM_SEARCH_JSP', 'false'),
 ('CC_BILLING_ADDRESS', 'false'),
 ('L19_TEST_MODE', 'false'),
 ('PRODUCT_URL_FIELD_REDIRECT',''),
 ('CATEGORY_HTML_TABLE',''),
 ('TSR_DIRECTORY',''),
 ('FRONTEND_REGISTRATION_DIRECTORY',''),
 ('FRONTEND_LOGIN_DIRECTORY',''),
 ('DYNAMIC_PROMO',''),
 ('ORDER_CANCEL_REASONS','');
 ('PO_PRICING_INSTRUCTIONS','');
 ('PO_STAGING_CHECKLIST','');
 ('PO_LOCATION_AFTERSTAGING','');

UNLOCK TABLES;

CREATE TABLE layout (
  id INT unsigned NOT NULL,
  lang varchar(2) NOT NULL default '',
  host varchar(50) NOT NULL default '',
  name varchar(20),
  header_html text NOT NULL default '',
  topbar_html text NOT NULL default '',
  leftbar_top_html text NOT NULL default '',
  leftbar_bottom_html text NOT NULL default '',
  rightbar_top_html text NOT NULL default '',
  rightbar_bottom_html text NOT NULL default '',
  footer_html text NOT NULL default '',  
  header_html_login text NOT NULL default '',
  topbar_html_login text NOT NULL default '',
  leftbar_top_html_login text NOT NULL default '',
  leftbar_bottom_html_login text NOT NULL default '',
  rightbar_top_html_login text NOT NULL default '',
  rightbar_bottom_html_login text NOT NULL default '',
  footer_html_login text NOT NULL default '',  
  aem_footer text NOT NULL default '',  
  left_pane_width varchar(4) default '',
  head_tag text NOT NULL default '',
header_html_mobile text NOT NULL default '',
  topbar_html_mobile text NOT NULL default '',
  leftbar_top_html_mobile text NOT NULL default '',
  leftbar_bottom_html_mobile text NOT NULL default '',
  rightbar_top_html_mobile text NOT NULL default '',
  rightbar_bottom_html_mobile text NOT NULL default '',
  footer_html_mobile text NOT NULL default '',
  header_html_login_mobile text NOT NULL default '',
  topbar_html_login_mobile text NOT NULL default '',
  leftbar_top_html_login_mobile text NOT NULL default '',
  leftbar_bottom_html_login_mobile text NOT NULL default '',
  rightbar_top_html_login_mobile text NOT NULL default '',
  rightbar_bottom_html_login_mobile text NOT NULL default '',
  footer_html_login_mobile text NOT NULL default '',
  head_tag_mobile text NOT NULL default '',
  PRIMARY KEY (id, lang, host)
) ENGINE = InnoDB;
insert into layout (id) values (1), (2), (3), (4), (5), (6), (7), (8), (9), (10), (11), (12), (13), (14), (15);
UPDATE layout set aem_footer = '<div class="pageSubFooterContent" style="text-align:center;padding:5px;">Powered by WebJaguar <a href="http://advancedemedia.com" target="_blank" title="b2b e-commerce">B2B E-Commerce</a> & <a href="http://advancedemedia.com" target="_blank" title="b2b e-commerce">B2C E-Commerce Solution</a> by <a href="http://advancedemedia.com" target="_blank" title="e-commerce service">Advanced EMedia</a></div>';
UPDATE layout SET head_tag = '<title>Site Name</title>\r\n<meta name="description" content="Add your Site description Here.">\r\n<link href=\"/assets/stylesheetDiv.css\" rel=\"stylesheet\" type=\"text/css\">\r\n<link href=\"/assets/menu.css\" rel=\"stylesheet\" type=\"text/css\">';
UPDATE layout SET header_html = '<div class=\"header\">\r\n<div class=\"logo\">\r\n	<a href=\"/\">\r\n<img alt=\"AEM\" src=\"https://www.webjaguar.net/logo/aem.gif\" />\r\n</a>\r\n</div>\r\n<div class=\"contact-info\">\r\n<a class=\"social-link\" href=\"#\">\r\n<img title=\"Follow Us on Twitter\" src=\"/assets/Image/Layout/social-twitter.png\"></a>\r\n<a class=\"social-link\" href=\"#\"><img title=\"Join Our Facebook Page\" src=\"/assets/Image/Layout/social-facebook.png\"></a>\r\n</div>\r\n<div id=\"search\" class=\"searchWrapper\">\r\n<form method=\"post\" action=\"/search.jhtm\">\r\n<input id=\"keyword\" type=\"text\" onblur=\"if (this.value == \'\') {this.value = \'Search\';}\" onfocus=\"if (this.value == \'Search\') {this.value = \'\';}\" name=\"keywords\" value=\"Search\">\r\n<input id=\"searchsubmit\" type=\"submit\" value=\"Search\">\r\n</form>\r\n</div>\r\n</div>';
UPDATE layout SET topbar_html = '<div id=\"topMenu\" class=\"topMenu\">\r\n<ul><li><a href=\"\home.jhtm\" class=\"topbar_link\" >Home</a></li>\r\n<li><a href=\"\account.jhtm\" class=\"topbar_link\">My Account</a></li>\r\n    <li><a href=\"\faq.jhtm\" class=\"topbar_link\">FAQ</a></li>\r\n<li><a href=\"\policy.jhtm\" class=\"topbar_link\">Policy</a></li>\r\n<li><a href=\"\viewCart.jhtm\" class=\"topbar_link\">View Cart</a></li>\r\n  </ul> \r\n</div>';

CREATE TABLE system_layout (
  type varchar(50) NOT NULL default '',
  lang varchar(2) NOT NULL default '',
  host varchar(50) NOT NULL default '',
  name varchar(50) NOT NULL default '',
  header_html text NOT NULL default '',
  footer_html text NOT NULL default '',  
  head_tag text NOT NULL default '',
  header_html_mobile text NOT NULL default '',
  footer_html_mobile text NOT NULL default '',  
  head_tag_mobile text NOT NULL default '',
  leftbar_top_html_mobile text NOT NULL default '',
  rightbar_top_html_mobile text NOT NULL default '',
  leftbar_top_html text NOT NULL default '',
  rightbar_top_html text NOT NULL default '',
  PRIMARY KEY (type, lang, host)
) ENGINE = InnoDB;
insert into system_layout (type, header_html, footer_html, head_tag) values 
 ('cart','<div class=\"shoppingCartHdrImage\"></div>\r\n<div class="shoppingCartHdr">Shopping Cart</div>','','<title>Cart</title>\r\n<link href=\"/assets/stylesheetDiv.css\" rel=\"stylesheet\" type=\"text/css\">\r\n<link href=\"/assets/menu.css\" rel=\"stylesheet\" type=\"text/css\">\r\n<link href=\"/assets/style/cart.css\" rel=\"stylesheet\" type=\"text/css\">'), 
 ('login','','','<title>Login</title>\r\n<link href=\"/assets/stylesheetDiv.css\" rel=\"stylesheet\" type=\"text/css\">\r\n<link href=\"/assets/menu.css\" rel=\"stylesheet\" type=\"text/css\">\r\n<link href=\"/assets/register.css\" rel=\"stylesheet\" type=\"text/css\">'), 
 ('faq','<div class="faqTitle">Frequently Asked Questions (FAQ):</div>','',''), 
 ('policy','<div class="policyTitle">Policy:</div>','',''), 
 ('invoice','<div class="invoiceHdr">Invoice</div>','','<title>Invoice</title>\r\n<link href="/assets/invoice.css" rel="stylesheet" type="text/css">'),
 ('invoice2','<div class="invoiceHdr">Invoice</div>','','<title>Invoice</title>\r\n<link href="/assets/invoice.css" rel="stylesheet" type="text/css">'),
 ('pickingSheet','<div class="invoiceHdr">Picking Sheet</div>','','<title>Picking Sheet</title>\r\n<link href="/assets/invoice.css" rel="stylesheet" type="text/css">'),
 ('pinvoice','<div class="invoiceHdr">Preliminary Invoice</div>','[ <a href="viewCart.jhtm">Change Quantities</a> | <a href="home.jhtm">Shop Some More</a> ]','<title>Preliminary Invoice</title>\r\n<link href="/assets/invoice.css" rel="stylesheet" type="text/css">'),
 ('finvoice','<div class="invoiceHdr">Final Invoice</div>','<div class="finalinvoice_link"><a href="home.jhtm">Start a New Shopping Trip</a></div>','<title>Thank You for Your Order!</title>\r\n<link href="/assets/invoice.css" rel="stylesheet" type="text/css">'),
 ('purchaseOrder','<div class="invoiceHdr">Purchase Order</div>','',''),
 ('packing','<div class="invoiceHdr">Packing</div>','',''),
 ('packing2','<div class="invoiceHdr">Packing 2</div>','',''),
 ('packing3','<div class="invoiceHdr">Packing 3</div>','',''),
 ('packing4','<div class="invoiceHdr">Packing 4</div>','',''),
 ('packing5','<div class="invoiceHdr">Walk-In</div>','',''),
 ('subscription','<div class="listingsHdr1">Subscriptions</div>','',''),
 ('giftCard','<div class="listingsHdr1">GiftCard</div>','',''),
 ('mylist','<div class="listingsHdr1">My List</div>','',''),
 ('myaccount','<div class="listingsHdr1">My Account Information</div>','','<title>My Account</title>\r\n<link href=\"/assets/stylesheetDiv.css\" rel=\"stylesheet\" type=\"text/css\">\r\n<link href=\"/assets/menu.css\" rel=\"stylesheet\" type=\"text/css\">\r\n<link href=\"/assets/register.css\" rel=\"stylesheet\" type=\"text/css\">'),
 ('myaddress','<div class="listingsHdr1">My Address Information</div>','',''),
 ('myorderhistory','<div class="listingsHdr1">My Order History/Status Information</div>','',''),
 ('myquotehistory','<div class="listingsHdr1">My Quote History Information</div>','',''), 
 ('sugarSync','<div class="listingsHdr1">My Files</div>','','<title>My Files</title>\r\n<link href=\"/assets/stylesheetDiv.css\" rel=\"stylesheet\" type=\"text/css\">\r\n<link href=\"/assets/menu.css\" rel=\"stylesheet\" type=\"text/css\">\r\n<link href=\"/assets/register.css\" rel=\"stylesheet\" type=\"text/css\">'),
 ('subAccount','<div class="accountHdr">Sub-account</div>','',''),
 ('quotelayout','<div class="invoiceHdr">Quote</div>','',''),
 ('myorderlist','<div class="listingsHdr1">My Order List Information</div>','',''),
 ('product','','','<title>#name#</title>\r\n<link href=\"/assets/stylesheetDiv.css\" rel=\"stylesheet\" type=\"text/css\">\r\n<link href=\"/assets/menu.css\" rel=\"stylesheet\" type=\"text/css\">'),
 ('product1','','',''),
 ('product2','','',''),
 ('productSearch','','',''),
 ('productNotFound','','',''),
 ('category','','','<title>#name#</title>\r\n<link href=\"/assets/stylesheetDiv.css\" rel=\"stylesheet\" type=\"text/css\">\r\n<link href=\"/assets/menu.css\" rel=\"stylesheet\" type=\"text/css\">'),
 ('evergreen-order','','',''),
 ('evergreen-orders','','',''),
 ('evergreen-orderFulfillmentSchedule','','',''),
 ('evergreen-orderFulfillmentDetails','','',''),
 ('evergreen-myCreditsDamagedReportsList','','',''),
 ('evergreen-myCreditsDamagedReport','','',''),
 ('jbd-order','','',''),
 ('jbd-orders','','',''),
 ('jbd-invoice','','',''),
 ('jbd-invoices','','',''),
 ('workorder','','',''),
 ('location','<div class="locationHdr">Store Locator</div>','',''),
 ('shipping','<div class="checkoutHdr">Shipping Information</div>','',''),
 ('billing','<div class="checkoutHdr">Billing Information</div>','',''),
 ('ccfailed','<div class="checkoutHdr">Billing Information</div>','','<title>Credit Card Failed</title>\r\n<link href="/assets/stylesheet.css" rel="stylesheet" type="text/css">'),
 ('attachFiles','<div class="checkoutHdr">Attach Files</div>','',''),
 ('cardinalCentinel','<div align="center"><b>For your security, please fill out the form below to complete your order.</b>\r\nDo not click the refresh or back button or this transaction may be interrupted or cancelled.</div>','','<title></title>\r\n<link href="/assets/stylesheet.css" rel="stylesheet" type="text/css">'),
 ('cardinalCentinelFailed','<div class="checkoutHdr">Billing Information</div>','','<title>Credit Card Authorization Failed</title>\r\n<link href="/assets/stylesheet.css" rel="stylesheet" type="text/css">'),
 ('createAccount','','','<title>New Account</title>\r\n<link href=\"/assets/stylesheetDiv.css\" rel=\"stylesheet\" type=\"text/css\">\r\n<link href=\"/assets/menu.css\" rel=\"stylesheet\" type=\"text/css\">\r\n<link href=\"/assets/register.css\" rel=\"stylesheet\" type=\"text/css\">'),
 ('verifyAccount','','','<title>Verify Account</title>\r\n<link href=\"/assets/stylesheetDiv.css\" rel=\"stylesheet\" type=\"text/css\">\r\n<link href=\"/assets/menu.css\" rel=\"stylesheet\" type=\"text/css\">\r\n<link href=\"/assets/register.css\" rel=\"stylesheet\" type=\"text/css\">'),
 ('productReview','','',''),
 ('companyReview','','',''),
 ('salesRep','','',''),
 ('srLogin','','',''),
 ('tickets','','',''),
 ('newTicket','','',''),
 ('billingShipping','<div class=\"shipBillHdrImage\"></div>','','<link href=\"/assets/stylesheetDiv.css\" rel=\"stylesheet\" type=\"text/css\">\r\n<link href="/assets/billingShipping.css" rel="stylesheet" type="text/css" />'),
 ('billing1','<div class=\"shipBillHdrImage\"></div>','','<link href=\"/assets/stylesheetDiv.css\" rel=\"stylesheet\" type=\"text/css\">\r\n<link rel="stylesheet" href="/assets/billingShipping.css" type="text/css" />'),
 ('shipping1','<div class=\"shipBillHdrImage\"></div>','','<link href=\"/assets/stylesheetDiv.css\" rel=\"stylesheet\" type=\"text/css\">\r\n<link rel="stylesheet" href="/assets/billingShipping.css" type="text/css" />'),
 ('pinvoice1','<div class=\"shipBillHdrImage\"></div>\r\n<div class="invoiceHdr">Preliminary Invoice</div>','','<link href=\"/assets/stylesheetDiv.css\" rel=\"stylesheet\" type=\"text/css\">\r\n<link href="/assets/pinvoice1.css" rel="stylesheet"  type="text/css" />'),
 ('finvoice1','<div class=\"finalOrderHdrImage\"></div>\r\n<div class="invoiceHdr">Final Invoice</div>','','<link href=\"/assets/stylesheetDiv.css\" rel=\"stylesheet\" type=\"text/css\">\r\n<link href="/assets/invoice.css" rel="stylesheet"  type="text/css" />'),
 ('suppliersalelayout','','',''),
 ('viewMyBalanceLayout','<div class="header">My Balance</div>','',''),
 ('viewMyProductsLayout','<div class="header">My Products</div>','',''),
 ('viewMySalesLayout','<div class="header">My Sales</div>','',''),
 ('viewMyReportLayout','<div class="header">My Report</div>','',''),
 ('quoteList','<div class="quoteHdr">Quote List</div>','',''),
 ('deliveryTime','','',''),
 ('updateTicket','','',''),
 ('changeEmailPassword','<div class="header"></div>','',''),
 ('manageStoredAddress','<div class="header"></div>','',''),
 ('viewEditMyAccountPage','<div class="header"></div>','',''),
 ('eventRegister','<div class="header"></div>','',''),
 ('forgetPassword', '', '', '');

CREATE TABLE shipping_method (
  id INT unsigned NOT NULL auto_increment,
  carrier varchar(100) default NULL,
  shipping_code varchar(150) default NULL,
  shipping_title varchar(150) default NULL,
  shipping_active tinyint(1) default NULL,
  shipping_rank INT unsigned default NULL,
  weather_temp INT unsigned NOT NULL default 0,
  min_price double(10,2),
  max_price double(10,2),
  PRIMARY KEY  (id)
) ENGINE=InnoDB;

CREATE TABLE shipping_handling (
  price_limit double(10,2),
  new_price_cost double(10,2),
  new_shipping_title varchar(80),
  shipping_base double(10,2),
  shipping_incremental double(10,2),
  handling_base double(10,2),
  handling_incremental double(10,2),
  handling_type varchar(6) default 'weight',
  shipping_method varchar(3) default 'crr',
  shipping_title varchar(80),
  weight_limit double(10,2),
  tbd_enable bool NOT NULL,
  tbd_title varchar(80) default 'To Be Determined',
  zero_weight_new_price_cost double(10,2),
  zero_weight_new_shipping_title varchar(80)
) ENGINE=InnoDB;
insert into shipping_handling (shipping_title) values ('');

INSERT INTO shipping_method (carrier, shipping_code, shipping_title, shipping_active, shipping_rank) VALUES 
 ('fedex','FEDEX_2_DAY','FedEx 2Day<sup>&#174;</sup>',0,1),
 ('fedex','FEDEX_EXPRESS_SAVER','FedEx Express Saver<sup>&#174;</sup>',0,1),
 ('fedex','FEDEX_GROUND','FedEx Ground<sup>&#174;</sup>',0,1),
 ('fedex','FIRST_OVERNIGHT','FedEx First Overnight<sup>&#174;</sup>',0,1),
 ('fedex','GROUND_HOME_DELIVERY','FedEx Home Delivery<sup>&#174;</sup>',0,1),
 ('fedex','INTERNATIONAL_ECONOMY','FedEx International Economy<sup>&#174;</sup>',0,1),
 ('fedex','INTERNATIONAL_FIRST','FedEx International First<sup>&#174;</sup>',0,1),
 ('fedex','INTERNATIONAL_PRIORITY','FedEx International Priority<sup>&#174;</sup>',0,1),
 ('fedex','PRIORITY_OVERNIGHT','FedEx Priority Overnight<sup>&#174;</sup>',0,1),
 ('fedex','STANDARD_OVERNIGHT','FedEx Standard Overnight<sup>&#174;</sup>',0,1),
 ('usps','0','Usps First-Class',0,1),
 ('usps','1','Usps Priority Mail',0,1),
 ('usps','2','Usps Express Mail Hold for Pickup',0,1),
 ('usps','3','Usps Express Mail PO to Addressee',0,1),
 ('usps','4','Usps Parcel Post',0,1),
 ('usps','5','Usps Bound Printed Matter',0,1),
 ('usps','6','Usps Media Mail',0,1),
 ('usps','7','Usps Library',0,1),
 ('usps','12','Usps First-Class Postcard Stamped',0,1),
 ('usps','13','Usps Express Mail Flat-Rate Envelope',0,1),
 ('usps','16','Usps Priority Mail Flat-Rate Envelope',0,1),
 ('usps','17','Usps Priority Mail Regular Flat-Rate Box',0,1),
 ('usps','18','Usps Priority Mail Keys and IDs',0,1),
 ('usps','19','Usps First-Class Keys and IDs',0,1),
 ('usps','22','Usps Priority Mail Flat-Rate Large Box',0,1),
 ('usps','23','Usps Express Mail Sunday/Holiday',0,1),
 ('usps','25','Usps Express Mail Flat-Rate Envelope Sunday/Holiday',0,1),
 ('usps','27','Usps Express Mail Flat-Rate Envelope Hold For Pickup',0,1),
 ('usps','28','Usps Priority Mail Small Flat-Rate Box',0,1),
 ('usps','Int 1','Usps Express Mail International',0,1),
 ('usps','Int 2','Usps Priority Mail International',0,1),
 ('usps','Int 4','Usps Global Express Guaranteed (Document and Non-document)',0,1),
 ('usps','Int 5','Usps Global Express Guaranteed Document used',0,1),
 ('usps','Int 6','Usps Global Express Guaranteed Non-Document Rectangular shape',0,1),
 ('usps','Int 7','Usps Global Express Guaranteed Non-Document Non-Rectangular',0,1),
 ('usps','Int 8','Usps Priority Mail Flat Rate Envelope',0,1),
 ('usps','Int 9','Usps Priority Mail Flat Rate Box',0,1),
 ('usps','Int 10','Usps Express Mail International Flat Rate Envelope',0,1),
 ('usps','Int 11','Usps Priority Mail Large Flat Rate Box',0,1),
 ('usps','Int 12','Usps Global Express Guaranteed Envelope',0,1),
 ('usps','Int 13','Usps First Class Mail International Letters',0,1),
 ('usps','Int 14','Usps First Class Mail International Flats',0,1),
 ('usps','Int 15','Usps First Class Mail International Parcels',0,1),
 ('usps','Int 16','Usps Priority Mail Small Flat Rate Box',0,1),
 ('usps','Int 21','Usps PostCards',0,1),
 ('dhl','Express','Dhl Express',0,1),
 ('dhl','Next afternoon','Dhl Next afternoon',0,1),
 ('dhl','Second day service','Dhl Second day service',0,1),
 ('dhl','Ground','Dhl Ground',0,1),
 ('dhl','Express 10:30AM','Dhl Express 10:30AM',0,1),
 ('dhl','Express Saturday','Dhl Express Saturday',0,1),
 ('ups','01','UPS Next Day Air',0,1),
 ('ups','02','UPS 2nd Day Air',0,1),
 ('ups','03','UPS Ground',1,1),
 ('ups','07','UPS Worldwide Express',0,1),
 ('ups','08','UPS Worldwide Expedited',0,1),
 ('ups','11','UPS Standard',0,1),
 ('ups','12','UPS 3 Day Select',0,1),
 ('ups','13','UPS Next Day Air Saver',0,1),
 ('ups','14','UPS Next Day Air Early A.M.',0,1),
 ('ups','54','UPS Worldwide Express Plus',0,1),
 ('ups','59','UPS 2nd Day Air A.M.',0,1),
 ('ups','65','UPS Express Saver',0,1);

CREATE TABLE shipping_custom (
  id INT unsigned NOT NULL auto_increment,
  title varchar(80),
  price double(10,2),
  internal bool NOT NULL,
  ignore_carriers bool NOT NULL default 0,
  min_price double(10,2),
  max_price double(10,2),
  min_weight double(10,2),
  max_weight double(10,2),
  handling double(10,2),
  description TEXT,
  country varchar(2) NOT NULL,
  host varchar(50) NOT NULL,
  carrier_id INT unsigned,
  customer_type tinyint unsigned,
  percent smallint(1) default 0,
  category_id INT unsigned,
  PRIMARY KEY  (id),
  FOREIGN KEY (carrier_id) REFERENCES shipping_method (id)
  	ON DELETE SET NULL
  	ON UPDATE CASCADE,
  FOREIGN KEY (category_id) REFERENCES category (id)
  	ON DELETE SET NULL
  	ON UPDATE CASCADE
) ENGINE=InnoDB;
insert into shipping_custom (title) values (''), (''), (''), (''), ('');

CREATE TABLE shipping_custom_contact_info (
  id INT unsigned NOT NULL auto_increment,
  company varchar(50) default null,
  contact_name varchar(30) default null,
  email varchar(30) default null,
  tel1 varchar(15) default null,
  tel2 varchar(15) default null,
  website varchar(50) default null,
  trackUrl varchar(255) default null,
  active TINYINT(1) not null default 1,
  PRIMARY KEY  (id)
) ENGINE=InnoDB;

CREATE TABLE group_user (
  id int NOT NULL auto_increment,
  name varchar(100) NOT NULL,
  active bool NOT NULL,
  created datetime,
  created_by VARCHAR(80) DEFAULT NULL,
  PRIMARY KEY (id),
  UNIQUE KEY (name)
) ENGINE=InnoDB;

CREATE TABLE group_user_join (
  group_id INT unsigned NOT NULL,
  user_id INT unsigned NOT NULL,
  new_entry bool NOT NULL,
  PRIMARY KEY  (group_id,user_id),
  FOREIGN KEY (user_id) REFERENCES users (id)
  	ON DELETE CASCADE
  	ON UPDATE CASCADE
) ENGINE=InnoDB;

CREATE TABLE group_sitemessage (
  id int NOT NULL auto_increment,
  name varchar(100) NOT NULL,
  active bool NOT NULL,
  created datetime,
  PRIMARY KEY (id),
  UNIQUE KEY (name)
) ENGINE=InnoDB;

CREATE TABLE group_sitemessage_join (
  group_id INT unsigned NOT NULL,
  message_id INT unsigned NOT NULL,
  PRIMARY KEY  (group_id,message_id),
  FOREIGN KEY (message_id) REFERENCES site_messages (message_id)
  	ON DELETE CASCADE
  	ON UPDATE CASCADE
) ENGINE=InnoDB;

CREATE TABLE payment_method (
  id INT unsigned NOT NULL auto_increment,
  title varchar(80),
  enabled bool NOT NULL,
  internal bool NOT NULL,
  group_id INT,
  code varchar(30),
  rank SMALLINT unsigned default NULL,
  PRIMARY KEY  (id),
  FOREIGN KEY (group_id) REFERENCES group_user (id)
  	ON DELETE SET NULL
  	ON UPDATE CASCADE
) ENGINE=InnoDB;
insert into payment_method (title) values ('Net 15'), ('Net 30'), ('Bill Me Later'), (''), (''), (''), (''), (''), (''), ('');

CREATE TABLE payments (
  id INT unsigned NOT NULL auto_increment,
  userid INT unsigned,
  payment_amount double(10,2),
  payment_date date,
  memo varchar(255),
  cancel_payment bool NOT NULL default '0',
  cancelled_amount DOUBLE(10,2) default NULL,
  cancelled_by VARCHAR(45) default NULL,
  payment_method varchar(80) NOT NULL default '',
  export_success char(1) DEFAULT '0',
  PRIMARY KEY (id),
  added_by varchar(255),
  FOREIGN KEY (userid) REFERENCES users (id) 
  	ON DELETE RESTRICT
  	ON UPDATE CASCADE
) ENGINE=InnoDB;

CREATE TABLE payments_orders (
  payment_id INT unsigned NOT NULL,
  order_id INT unsigned NOT NULL,
  payment_amount double(10,2),
  cancel_payment bool NOT NULL default '0',
  cancelled_amount DOUBLE(10,2) default NULL,
  cancelled_by VARCHAR(45) default NULL,	
  last_modified datetime,
  PRIMARY KEY (payment_id, order_id),
  FOREIGN KEY (payment_id) REFERENCES payments (id)
  	ON DELETE CASCADE
  	ON UPDATE CASCADE,
  FOREIGN KEY (order_id) REFERENCES orders (order_id)
  	ON DELETE CASCADE
  	ON UPDATE CASCADE
) ENGINE=InnoDB;

CREATE TABLE serviceable_item (
  id INT unsigned NOT NULL auto_increment,
  userid INT unsigned,
  item_id varchar(50),
  serial_num varchar(50),
  sku varchar(50),
  first_name varchar(80),
  last_name varchar(80),
  company varchar(50),
  addr1 varchar(80),
  addr2 varchar(80),  
  city varchar(80),
  state_province varchar(80),
  zip varchar(20),
  country varchar(50),
  phone varchar(80),
  fax varchar(80),
  cell_phone varchar(80),
  email tinytext,
  item_active bool NOT NULL default 1,
  alternate_contacts text,
  PRIMARY KEY (id),
  UNIQUE KEY (item_id),
  UNIQUE KEY (serial_num),
  FOREIGN KEY (userid) REFERENCES users (id)
    ON DELETE SET NULL
    ON UPDATE CASCADE  
) ENGINE=InnoDB;

CREATE TABLE service (
  service_num INT unsigned NOT NULL auto_increment,
  serviceable_item_id INT unsigned NOT NULL,
  host varchar(50),
  report_date datetime,
  last_modified datetime,
  complete_date date,  
  status varchar(30) NOT NULL default 'Pending',
  service_type varchar(8),
  comments text,
  problem text,
  tracknum_in varchar(50),
  tracknum_in_carrier varchar(10),
  tracknum_out varchar(50),
  tracknum_out_carrier varchar(10),
  item_from varchar(100),
  item_to varchar(100),
  ship_date date, 
  service_pdf_url varchar(30),
  service_notes text,
  service_priority TINYINT unsigned NOT NULL,
  purchase_order varchar(50),
  purchase_order_pdf_url varchar(30),
  wo_3rd varchar(50),
  wo_3rd_pdf_url varchar(30),
  po_supplier varchar(50),
  service_notes_2 varchar(50),
  ref_num varchar(50),
  manuf_warranty varchar(50),
  alternate_contact text,
  PRIMARY KEY (service_num),
  FOREIGN KEY (serviceable_item_id) REFERENCES serviceable_item (id)
    ON DELETE RESTRICT
    ON UPDATE CASCADE 
) ENGINE=InnoDB;

CREATE TABLE work_order (
  service_num INT unsigned NOT NULL,
  created datetime,
  last_modified datetime,
  technician varchar(100),
  work_order_date date,
  service_date date,
  start_time time,
  stop_time time,
  bw_count int unsigned,
  color_count int unsigned,
  install_percentage tinyint unsigned,
  in_sn varchar(50),
  out_sn varchar(50),
  work_order_type varchar(30),
  work_order_printed bool NOT NULL,
  PRIMARY KEY (service_num),
  FOREIGN KEY (service_num) REFERENCES service (service_num)
    ON DELETE RESTRICT
    ON UPDATE CASCADE 
) ENGINE=InnoDB;

CREATE TABLE work_order_lineitems (  	
  service_num INT unsigned NOT NULL,
  line_num INT unsigned NOT NULL,
  product_name varchar(120),
  product_sku varchar(50),
  quantity INT NOT NULL,
  case_content int unsigned,
  packing varchar(50),
  notes varchar(255),
  in_sn varchar(50),
  out_sn varchar(50),
  primary key (service_num, line_num),
  FOREIGN KEY (service_num) REFERENCES work_order (service_num)
  	ON DELETE CASCADE
  	ON UPDATE CASCADE
) ENGINE=InnoDB;

ALTER TABLE service AUTO_INCREMENT = 1000001;

CREATE TABLE data_feed (
  feed varchar(15),
  rank INT unsigned,
  field_name varchar(30) not null default '',
  description text,
  method_name varchar(15) not null default '',
  enabled bool NOT NULL,
  required bool NOT NULL,
  default_value varchar(255),
  exclude_rule varchar(15) not null default '',
  UNIQUE KEY (feed, field_name)
) ENGINE=InnoDB;

INSERT INTO data_feed (feed, rank, required, field_name, method_name, description) values
 ('blueCherry',10,true,'Active','','(optional) - if the product is still active on your site.'),
 ('blueCherry',20,true,'Available','','(optional) - Values are "Yes" or "No".'),
 ('blueCherry',30,true,'Caption','','(optional) - Sales text message. Only available at a few comparison portals.'),
 ('blueCherry',40,true,'Condition','','(optional) - default value is "New"'),
 ('blueCherry',50,true,'Contents','','(optional)'),
 ('blueCherry',60,true,'Cost','','(optional) - this is the landed cost of the product.'),
 ('blueCherry',70,true,'Category','','(mandatory) - main category for item'),
 ('blueCherry',80,true,'Sub-Category','','(mandatory) - Sub category for item'),
 ('blueCherry',90,true,'Small Image URL','thumb','(mandatory)'),
 ('blueCherry',100,true,'Large Image URL','detailsbig','(mandatory)'),
 ('blueCherry',110,true,'Manufacturer','','(mandatory)'),
 ('blueCherry',120,true,'Model Number','','(optonal for soft products, mandatory for hard products).'),
 ('blueCherry',130,true,'Product Code','getSku','(mandatory) - this is your internal product code.'),
 ('blueCherry',140,true,'Product Name','getName','(mandatory) - for soft products, use this field to describe the product. Use keywords like the category name in the product name (EG. "Abby Black Leather Sofa"). Your list ranking will depend on the value in this field.'),
 ('blueCherry',150,true,'Product Description','getShortDesc','(mandatory for soft products). For soft products, please provide the product description and information like size, weight, etc. This is displayed on the comparison portals and can minimize unrelated clicks.'),
 ('blueCherry',160,true,'Product URL','url','(mandatory)'),
 ('blueCherry',170,true,'Quantity','','(optional)'),
 ('blueCherry',180,true,'Sale Price','getPrice1','(mandatory)'),
 ('blueCherry',190,true,'Shipping Cost','','(optional) - if this field is not supplied then the shipping weight field is mandatory.'),
 ('blueCherry',200,true,'Shipping Weight','getWeight','(optional) - if this field is not supplied then the shipping cost field is mandatory.'),
 ('blueCherry',210,true,'UPC','','(optional)'),
 ('blueCherry',220,true,'ISBN','','(optional) - mandatory for books.'),
 ('blueCherry',230,true,'Manufacturer Part Number','','(optional for soft products)'),
 ('priceGrabber',10,true,'Unique Retailer SKU','getSku','Any product id, even numbering products 1,2,3..etc. as long as it will be the same in every feed.'),
 ('priceGrabber',20,true,'Product Name','getName','Title of the product. Limit 255 characters. General format: First letter of the word capitalized, remaining letters lowercase.'),
 ('priceGrabber',30,false,'Detailed Description','getShortDesc','Long description of the products. This should be thorough but concise. No HTML allowed. No newlines or carriage returns please. Each product should only take up one line.'),
 ('priceGrabber',40,true,'Product Condition','','Default value is New. Available conditions include Refurbished, Generic, Used, OEM, Like New.'),
 ('priceGrabber',50,true,'Categorization','','Categorization text preferably according to PriceGrabber.com\'s taxonomy (category hierarchy).'),
 ('priceGrabber',60,false,'Manufacturer Name','','Manufacturer or the Brand Name of the product.'),
 ('priceGrabber',70,true,'Manufacturer Part Number','','Unique universal code of numbers and characters that identify the specific product. - Please refrain from using duplicate manufacturer part numbers as only the first item will be recognized. - This is case insensitive and nonalphanumeric characters will be removed e.g. K4-56lt# will be read in as K4561 (pluses "+" are not removed).'),
 ('priceGrabber',80,false,'UPC','','Unique universal code of 14 numbers.'),
 ('priceGrabber',90,false,'ISBN','','Unique universal code of 13 numbers or letters.'),
 ('priceGrabber',100,true,'Selling Price','getPrice1','Price of the product. Please omit dollar signs.'),
 ('priceGrabber',110,false,'Shipping Cost','','Flat rate shipping fee per product. Omit dollar signs. If shipping is based on weight, include the weight and dimensions and name the columns "Weight", "Length", "Width", "Height" instead.'),
 ('priceGrabber',120,false,'Weight','',''),
 ('priceGrabber',130,false,'Length','',''),
 ('priceGrabber',140,false,'Width','',''),
 ('priceGrabber',150,false,'Height','',''),
 ('priceGrabber',160,true,'Availability','','Value is "Yes" or "No". Indicates whether the product is in stock or not.'),
 ('priceGrabber',170,true,'Referring Product URL','url','Link to the product page on the retailers site. Absolute link is required. Must include "http://".'),
 ('priceGrabber',180,false,'Image URL','detailsbig','Location of the item\'s picture on your site. Absolute link is required. No thumbnails please. The bigger the image, the better.'),
 ('sortPrice',10,true,'Name','getName','The name of the product'),
 ('sortPrice',20,true,'Manufacturer','','The product manufacturer name, if not known leave blank'),
 ('sortPrice',30,true,'Category','','For best results use SortPrice.com\'s category path. If you are unable to provide this, please provide the full category path on your site'),
 ('sortPrice',40,true,'Price','getPrice1','The price of the product on your site'),
 ('sortPrice',50,true,'URL','url','The URL for that product'),
 ('sortPrice',60,true,'Image URL','detailsbig','The URL to the image of the product on your site'),
 ('sortPrice',70,true,'SKU','getSku','The product SKU, if not known leave blank'),
 ('sortPrice',80,true,'Description','getShortDesc','A detailed description of the product'),
 ('mShopper',10,false,'PROGRAMNAME','','(optional) - The company/merchant name or the advertiser program associated with the product.'),
 ('mShopper',20,true,'MANUFACTURER','','(mandatory) - The brand or manufacturer of the product.'),
 ('mShopper',30,true,'NAME','getName','(mandatory) - The product\'s name.'),
 ('mShopper',40,false,'DESCRIPTION','getShortDesc','(highly desirable) - The product\'s description in paragraph-style text.'),
 ('mShopper',50,true,'MANUFACTURERID','','(mandatory) - Manufacturer\'s unique model or style number.  For soft goods we may use the NAME field if model/style are not available.'),
 ('mShopper',60,false,'IMAGEURL','detailsbig','(highly desirable) - Location of the advertiser-hosted product image.'),
 ('mShopper',70,true,'BUYURL','url','(mandatory) - The URL of the page where the customer can buy your product. Must include the mShopper affiliate ID to link the order to mShopper. Requires 1 BUYURL per individual product.'),
 ('mShopper',80,true,'ADVERTISERCATEGORY','','(mandatory) - mShopper applies up to 5 category levels to most effectively replicate your PC internet shopping experience. In your datafeed, provide each top level category with up to 5 subcategories.'),
 ('mShopper',90,true,'PRICE','getPrice1','(mandatory) - Sales price before rebates.'),
 ('mShopper',100,true,'SKU','getSku','(mandatory) - Your unique ID for product.'),
 ('mShopper',110,false,'UPC','','(optional) - 12 digit Universal Product Code.'),
 ('mShopper',120,false,'AUTHOR','','(optional) - For books.'),
 ('mShopper',130,false,'ARTIST','','(optonal) - For recorded media.'),
 ('mShopper',140,false,'TITLE','','(optional) - For books and recorded media.'),
 ('newEggMall',10,true,'OwnerSKUNumber','getSku','A unique identification number for each product, assigned by the merchant. The SKU must be unique for every product, including variations of the same product (e.g. different colors of the same style of sweater--see ParentSKUNumber for more information).'),
 ('newEggMall',20,true,'ActivationMark','','The ActivationMark\'s default setting is "True" on all uploaded products because all products must be activated to be visible on NeweggMall.com. You may set it to "False" if you wish to hide your product. If left blank, the default is "True"'),
 ('newEggMall',30,true,'AmountInventory','getInventory','Enter the quantity of the item you are making available for sale.  This is your current INVENTORY commitment to Newegg Mall.  If left blank when you first submit this product\'s information, the item will be displayed as "Out of stock."'),
 ('newEggMall',40,false,'WarehouseNumber','','Not currently used - do not mark.'),
 ('newEggMall',45,false,'Industry','','NewEggMall Industry.'),
 ('newEggMall',50,true,'CategoryDescription','','Enter the pre-defined category name that the product falls under. A list of categories can be found on the PropertyMaster Spreadsheet included in this file.'),
 ('newEggMall',60,false,'UPCCode','','Enter the Product UPC code.  This value is not required, but '),
 ('newEggMall',70,false,'Keywords','getKeywords','Enter no more than 10 Keywords that best describes the product.'),
 ('newEggMall',80,false,'ParentSKUNumber','','If this product is a child product (i.e. it is a variant of another ("parent") item with its own SKU number), list the parent product\'s OwnerSKUNumber here. Please enter a Size and/or Color value for each "child" product, so that we can correctly setup the product selection options on the website.'),
 ('newEggMall',90,false,'ColorMapping','','If the color used to describe the product is nonstandard (e.g., aqua), specify the standard color you wish to map it to (e.g., blue). '),
 ('newEggMall',100,false,'DetailPageURL','','Enter the URL for the Manufacturer\'s product page.'),
 ('newEggMall',110,true,'ManufacturerPartsNumber','','Enter the Manufacturer\'s unique part number.'),
 ('newEggMall',120,true,'ManufacturerName','','Enter the Manufacturer name for the product.'),
 ('newEggMall',130,false,'ManufacturerURL','','Enter the URL for the Manufacturer\'s web site.'),
 ('newEggMall',140,true,'BrandName','','Enter the Brand name for the product.'),
 ('newEggMall',150,false,'BrandURL','','Enter the URL for the Brand\'s web site.'),
 ('newEggMall',160,false,'BrandLogoURL','','Enter the URL for the Manufacturer or Brand logo image. URL must be direct link to the actual image (i.e. should end in an image file name such as .jpg, .gif, .bmp, or .png).'),
 ('newEggMall',170,false,'ManufacturerWarrantyMark','','If this product is warranted by the manufacturer, enter "True" for this field. If left blank, the default is "False"'),
 ('newEggMall',180,true,'Prop65Mark','','You must tell us if your product is subject to Prop 65 rules and regulations.  Prop 65 requires merchants to provide California consumers with special warnings for products that contain chemicals known to cause cancer, birth defects, or other reproductive harm if those products expose consumers to such materials above certain threshold levels.  The default value for this is "False" so if you do not populate this column then we assume your product is not subject to this rule.  Please view this website for more information: http://www.oehha.ca.gov/. If left blank, the default is "False"'),
 ('newEggMall',190,false,'ClearanceMark','','Not currently used - do not mark.'),
 ('newEggMall',200,false,'LegalDisclaimer','','You may enter any legal disclaimer you wish to be displayed on this item\'s product page here.'),
 ('newEggMall',210,false,'MerchantWarranty','','Describes any product-specific warranty information that is not covered in your overall Merchant Warranty Terms.'),
 ('newEggMall',220,false,'Weight','getWeight','Individual unit shipping weight in pounds'),
 ('newEggMall',230,false,'SizeWidth','','Individual unit shipping width in inches'),
 ('newEggMall',240,false,'SizeLength','','Individual unit shipping length in inches'),
 ('newEggMall',250,false,'SizeHeight','','Individual unit shipping height in inches'),
 ('newEggMall',260,true,'Price','getPrice1','The selling price at which you will offer the product for sale, expressed in U.S. dollars.'),
 ('newEggMall',270,false,'NonTaxable','nontaxable','Specify if this product is exempt from sales tax. If left blank, the default is "False"'),
 ('newEggMall',280,false,'EWRA','','If applicable, enter the amount of EWRA fee that needs to be collected for this product.'),
 ('newEggMall',290,false,'DiscountType','','If left blank, the default setting: "NoDiscount" - No discount offered for this product. "DiscountInstant" - Amount specified under "Discount" will be instantly applied to the selling price in shopping cart. "MailInRebate" - MIR offer will be indicated on product page, but discount amount will not be applied in cart.'),
 ('newEggMall',300,false,'Discount','','The discount amount applied to the normal selling price, expressed in U.S. dollars.  The site will strikethrough the item\'s normal price, indicate that the item is on sale minus the amount specified here, and automatically calculate the new sale price.'),
 ('newEggMall',310,false,'DiscountDescription','','This space is used to describe any special conditions of a discount (especially if it is a mail-in-rebate discount).'),
 ('newEggMall',320,false,'MailInRebateUrl','','Enter the URL for the mail in rebate document. URL must be to the actual document.'),
 ('newEggMall',330,false,'StartDate','','Enter the date and time the discount will begin.'),
 ('newEggMall',340,false,'EndDate','','Enter the date and time the discount will end. '),
 ('newEggMall',350,false,'ShippingCharge','','If you selected the "Order Total", "Weight"  or "Per Unit" based shipping calculation under your Account Settings. Leave this field "Blank"to default to the selected shipping calculation method. If you selected "Per Product" shipping calculation under your Account Settings. Enter this product\'s shipping charge based on Standard Shipping (3-5 days), expressed in U.S. dollars. If "0.00" is listed, the "Free Shipping" message will be displayed.'),
 ('newEggMall',360,false,'VolumeQty1','getQtyBreak1','If you wish, you may give customers a discount for buying in bulk. This is the first level of 3 possible levels of quantity discounts you may give. Enter the number of products a customer must buy at once to trigger Quantity Discount 1.'),
 ('newEggMall',370,false,'VolumePrice1','getPrice2','Enter per product selling price for bulk orders that qualify for Quantity Discount 1, expressed in U.S. dollars. This amount should be lower than this product\'s normal selling price. However, if you wish only to give a shipping discount for bulk orders, leave this price the same as the original and enter a lower-than-normal shipping price under VolumeShippingCharge1.'),
 ('newEggMall',380,false,'VolumeShippingCharge1','','You may give a shipping discount for customers buying in bulk. Enter the per product shipping charge for bulk orders that qualify for Quantity Discount 1, expressed in U.S. dollars.'),
 ('newEggMall',390,false,'VolumeQty2','getQtyBreak2','This is the second level of 3 possible levels of quantity discounts you may give. Enter the number of products a customer must buy at once to trigger Quantity Discount 2 (this quantity should be greater than the quantity required for Quantity Discount 1).'),
 ('newEggMall',400,false,'VolumePrice2','getPrice3','Enter per product selling price for bulk orders that qualify for Quantity Discount 2, expressed in U.S. dollars.'),
 ('newEggMall',410,false,'VolumeShippingCharge2','','Enter per product shipping charge based on quantity specified in VolumeQty2, expressed in U.S. dollars.'),
 ('newEggMall',420,false,'VolumeQty3','getQtyBreak3','This is the third level of 3 possible levels of quantity discounts you may give. Enter the number of products a customer must buy at once to trigger Quantity Discount 3 (this quantity should be greater than the quantity required for Quantity Discount 2).'),
 ('newEggMall',430,false,'VolumePrice3','getPrice4','Enter per product selling price for bulk orders that qualify for Quantity Discount 3, expressed in U.S. dollars.'),
 ('newEggMall',440,false,'VolumeShippingCharge3','','Enter per product shipping charge based on quantity specified in VolumeQty3, expressed in U.S. dollars.'),
 ('newEggMall',450,false,'LimitQuantity','','If you wish, you may limit the quantity of this product a customer buys per purchase. Enter the maximum number of this product a customer may order from you here. If left empty, no limit will be applied.'),
 ('newEggMall',460,true,'TitleDescription','getName','Enter a short, easily identifiable product name for this product. It will be displayed on all store pages, Sales Orders and receipts. (100 Character Limit). '),
 ('newEggMall',470,true,'LineDescription','getShortDesc','Enter a longer, more descriptive product name for this product that will be displayed on the product page. (200 Character Limit).'),
 ('newEggMall',480,true,'DetailDescription','getLongDesc','Enter a detailed description to be featured on the product page. This description should enumerate the features and functions of the product.'),
 ('newEggMall',490,true,'ItemImages','thumb','Enter the URL for this item\'s product image(s).All URLs must be a direct link to the actual image (i.e. should end in an image file name such as .jpg, .gif, .bmp, or .png). Note: when entering more than one URL, use a comma (,) to separate each URL.'),
 ('newEggMall',500,false,'Size-Dimensions','','MUST include the Color, Size, or both if item will have a Product relation.  Without the Color or Size, no drop down will be present on the Product page of the site.'),
 ('googleBase',600,true,'title','getName','MUST include the Color, Size, or both if item will have a Product relation.  Without the Color or Size, no drop down will be present on the Product page of the site.'),
 ('googleBase',610,true,'description','','MUST include the Color, Size, or both if item will have a Product relation.  Without the Color or Size, no drop down will be present on the Product page of the site.'),
 ('googleBase',620,true,'price','getPrice1','MUST include the Color, Size, or both if item will have a Product relation.  Without the Color or Size, no drop down will be present on the Product page of the site.'),
 ('googleBase',630,true,'link','url','MUST include the Color, Size, or both if item will have a Product relation.  Without the Color or Size, no drop down will be present on the Product page of the site.'),
 ('googleBase',640,true,'condition','','The condition of the product. Supported values include: new, used, and refurbished'),
 ('googleBase',645,true,'availability','','The availability of the product. Supported values include: available and not available'),
 ('googleBase',650,true,'id','getSku','A unique alphanumeric identifier for the item - e.g., your internal ID code. IMPORTANT: Once you submit an item with a unique id, this identifier must not change when you send in a new data feed. Each item must retain the same id in subsequent feeds.'),
 ('googleBase',655,false,'shipping','','The shipping of the product. Please put shipping related info of the product here. Google gives warning if not present.'),
 ('googleBase',656,false,'tax','','The tax of the product. Please put tax related info of the product here. Google gives warning if not present.'),
 ('googleBase',660,false,'image_link','thumb','MUST include the Color, Size, or both if item will have a Product relation.  Without the Color or Size, no drop down will be present on the Product page of the site.'),
 ('googleBase',670,false,'product_type','','The type of product being offered. Text. You may include multiple product types. ie. Electronics > Audio > Audio Accessories MP3 Player Accessories","Health & Beauty > Healthcare > Biometric Monitors > Pedometers'),
 ('googleBase',680,false,'color','','The color of the product.'),
 ('googleBase',690,false,'department','','The store department that the product falls under.'),
 ('googleBase',700,false,'feature','','A feature of the product.'),
 ('googleBase',710,false,'height','','The height of a product.'),
 ('googleBase',720,false,'width','','The width of a product.'),
 ('googleBase',730,false,'length','','The length of a product.'),
 ('googleBase',740,false,'weight','getWeight','The weight of a product.'),
 ('googleBase',750,false,'brand','','The brand name of the product.'),
 ('googleBase',760,false,'made_in','','The location where the product was made.'),
 ('googleBase',770,false,'manufacturer','','The company that manufactures an item.'),
 ('googleBase',780,false,'model_number','','The unique ID code assigned to a product by its manufacturer.'),
 ('googleBase',790,false,'mpn','','Manufacturer`s Part Number is a unique code determined by the manufacturer for that product. Required for all product types except: Apparel, Beauty, Furniture, Garden, Jewelry, and Media.'),
 ('googleBase',800,false,'size','','The size of an item.'),
 ('singleFeed',10,true,'Unique Internal Code','getId','The ID of a product.'),
 ('singleFeed',20,true,'Product Name','getName','The name of the product.'),
 ('singleFeed',30,true,'Product Description','getShortDesc','A detailed description of the product.'),
 ('singleFeed',40,true,'Product Price','getPrice1','The price of the product on your site.'),
 ('singleFeed',50,true,'Product Url','url','The URL for that product.'),
 ('singleFeed',60,true,'Image Url','detailsbig','The URL to the image of the product on your site.'),
 ('singleFeed',70,true,'Category','','The Category that this product belongs.'),
 ('singleFeed',80,true,'Manufacturer','','Enter the Manufacturer name for the product.'),
 ('singleFeed',90,false,'Stock Status','getInventory','Y, N. If left blank, SingleFeed will enter a default value of \'Y\'.'),
 ('singleFeed',100,false,'Condition (IF NOT NEW)','','New, Used, OEM, Refurbished. If left blank, SingleFeed will enter a defalt value of \'New\'.'),
 ('singleFeed',110,false,'MSRP','getMsrp','The manufacturer`s suggested retail price for the item. US dollars, with a decimal point and no $ sign'),
 ('singleFeed',120,false,'Shipping Weight','getWeight','Optional, but recommended if you price shipping according to weight.'),
 ('singleFeed',130,false,'Size','','Optional, but strongly recommended for clothing. Specify the size: S, M, L, XL, or 2, 4, 6, 8. Separate values with a comma.'),
 ('singleFeed',140,false,'Color','','Optional, but strongly recommended for clothing and other items.'),
 ('singleFeed',150,false,'Material','','For Google Base Only. The material the item is made out of.'),
 ('singleFeed',160,false,'Product Type','','Type of that product.'),
 ('singleFeed',170,false,'Payment Type','','Type of the payment.'),
 ('singleFeed',180,false,'Keywords','','Keywords for the product.'),
 ('singleFeed',190,false,'Sale Price','','Sale Price of the product.'),
 ('singleFeed',200,false,'Shipping Cost','','Shipping Cost for the product.'),
 ('singleFeed',210,false,'Promotional Message','','Promotional Message for the product.'),
 ('singleFeed',220,false,'Gender','','Gender of the customer'),
 ('singleFeed',230,false,'Department','','Department for the product'),
 ('equiteam',10,true,'Unique Retailer SKU','getSku','Any product id, even numbering products 1,2,3..etc. as long as it will be the same in every feed.'),
 ('equiteam',20,true,'Product Name','getName','The name of the product.'),
 ('equiteam',30,true,'Product Description','getShortDesc','A detailed description of the product.'),
 ('equiteam',40,true,'Product Qty and Price','getVarient','The price of the product on your site and the qty available on your site. Qty hardcoded to 20'),
 ('equiteam',50,true,'Brand','','The Brand that this product belongs.'),
 ('equiteam',60,true,'Image Url','detailsbig','The URL to the image of the product on your site.'),
 ('equiteam',70,true,'Product Price','getPriceTable5','The priceTable5 of the product on your site.'),
 ('equiteam',80,true,'Category','','The Category that this product belongs.'),
 ('myTradeZone',1000,false,'Size','getField1','Size of the product'),
 ('myTradeZone',1010,false,'Color','getField2','Color of the product'),
 ('myTradeZone',1020,false,'Condition','getField3','Condition of the product'),
 ('myTradeZone',1030,false,'Type','getField4','Type of the product'),
 ('myTradeZone',1040,false,'Shape','getField5','Shape of the product'),
 ('myTradeZone',1050,false,'Style','getField6','Style of the product'),
 ('myTradeZone',1060,false,'Texture','getField7','Texture of the product'),
 ('myTradeZone',1070,false,'Material','getField8','Material of the product'),
 ('myTradeZone',1080,false,'Flavor','getField9','Flavor of the product'),
 ('myTradeZone',1090,false,'Brand Name','getField10','Brand Name of the product'),
 ('myTradeZone',1100,false,'Manufacturer Part Number (MPN)','getField11','MPN of the product'),
 ('myTradeZone',1110,false,'Made In/Place Of Origin','getField12','Made In Country of the product'),
 ('myTradeZone',1120,false,'UPC Code','getField13','UPC Code of the product'),
 ('myTradeZone',1130,false,'FOB','getField14','FOB of the product'),
 ('myTradeZone',1140,false,'Availability','getField15','Availability of the product'),
 ('myTradeZone',1150,false,'Gender','getField16','Gender of the person'),
 ('myTradeZone',1160,false,'Age Group','getField17','Age Group of the person'),
 ('myTradeZone',1170,false,'Platform','getField18','Platform of the product'),
 ('myTradeZone',1180,false,'Season','getField19','Season in which product is available'),
 ('myTradeZone',1190,false,'Eco-Friendly','getField20','If product s Eco-friendly'),
 ('myTradeZone',1200,false,'Author','getField21','Author of the product'),
 ('myTradeZone',1210,false,'Publisher','getField22','Publisher of the product.'),
 ('myTradeZone',1220,false,'Year','getField23','Year of the product'),
 ('myTradeZone',1230,false,'Handmade','getField24','If the product is handmade'),
 ('myTradeZone',1240,false,'Ingredients','getField25','Ingredients of the product'),
 ('myTradeZone',1250,false,'Main Category','getField26','Main Category of the product'),
 ('myTradeZone',1260,false,'Main Sub-Category','getField27','Main Sub-Category of the product'),
 ('myTradeZone',1270,false,'Secondary Category','getField28','Secondary Category of the product'),
 ('myTradeZone',1280,false,'Secondary Sub-Category','getField29','Secondary Sub-Category of the product'),
 ('myTradeZone',1290,false,'Redirect URL','getField30','Redirect URL of the product');
 

CREATE TABLE data_feed_config (
  feed varchar(15),
  server varchar(30) not null default '',
  username varchar(30) not null default '',
  password varchar(30) not null default '',
  filename varchar(50) not null default '',
  price double(10,2) default null,
  inventory INT default null,
  client_id INT default null,
  prefix VARCHAR(10) default null,
  category INT default null,
  PRIMARY KEY  (feed)
) ENGINE=InnoDB;

INSERT INTO data_feed_config (feed, filename) values
 ('blueCherry','blueCherry.txt'),
 ('priceGrabber','priceGrabber.txt'),
 ('sortPrice','sortPrice.txt'),
 ('mShopper','mShopper.txt'),
 ('newEggMall','newEggMall.txt'),
 ('googleBase','googleBase.txt'),
 ('singleFeed','singleFeed.txt'),
 ('equiteam','equiteam.txt');
 
CREATE TABLE webjaguar_data_feed (
  id INT NOT NULL AUTO_INCREMENT,
  token VARCHAR(40) NOT NULL,
  name VARCHAR(15),
  server varchar(30) not null default '',
  username varchar(30) not null default '',
  password varchar(30) not null default '',
  filename varchar(50) not null default '',
  file_type varchar(5) not null default 'xml',
  subscriber boolean default false,
  feed_type VARCHAR(10) NOT NULL DEFAULT 'webjaguar',
  active boolean default true,
  prefix VARCHAR(10) default null,
  category INT default null,
  price double(10,2) default null,
  inventory INT default null,
  PRIMARY KEY  (id)
) ENGINE=InnoDB;

CREATE TABLE ticket (
  id INT NOT NULL AUTO_INCREMENT,
  host varchar(50),
  subject VARCHAR(255) NOT NULL,
  question TEXT NOT NULL,
  status VARCHAR(10) NOT NULL default 'open',
  created DATETIME,
  last_modified DATETIME,
  user_id INT UNSIGNED,
  created_by VARCHAR(80) DEFAULT NULL,
  version_created_by_type VARCHAR(10),
  type VARCHAR(50) DEFAULT NULL,
  PRIMARY KEY (id)
) ENGINE = InnoDB;

CREATE TABLE ticket_versions (
  id INT NOT NULL AUTO_INCREMENT,
  owner_id INT,
  question_desc TEXT,
  created DATETIME,
  created_by_type VARCHAR(10),
  for_access_user_id INT UNSIGNED DEFAULT NULL,
  created_by VARCHAR(80) DEFAULT NULL,
  PRIMARY KEY (id)
) ENGINE = InnoDB;

CREATE TABLE purchase_orders (
  po_id INT NOT NULL AUTO_INCREMENT,
  po_number VARCHAR(30) NOT NULL,
  status varchar(10) DEFAULT NULL,
  created DATETIME,
  due_date DATETIME NULL DEFAULT NULL,
  scheduled_date DATETIME NULL DEFAULT NULL,
  docked_date DATETIME DEFAULT NULL,
  docked_in VARCHAR(20),
  docked_out VARCHAR(20),
  supplier_id INT,
  sub_total DOUBLE(10,2),
  note text,
  special_instruction text,
  billing_address text,
  shipping_address text,
  ship_via varchar(50),
  ship_via_contact_id INT unsigned default NULL,
  order_by varchar(50), 
  order_id INT UNSIGNED DEFAULT NULL,
  dropship bool NOT NULL,
  supplier_invoice_num VARCHAR(20),
  shipping_quote DOUBLE(10,2),
  flag_1 varchar(50) DEFAULT NULL,
  pt VARCHAR(250) DEFAULT NULL,
  coming_to_via TINYINT(1) NOT NULL DEFAULT 0,
  picture_needed TINYINT(1) NOT NULL DEFAULT 0, 
  po_sold VARCHAR(45) NULL,
  trailer VARCHAR(45) NULL,
  check_before_staging VARCHAR(45) NULL,
  check_notes VARCHAR(200) NULL, 
  pricing_instructions VARCHAR(45) NULL, 
  location_after_staging VARCHAR(45) NULL, 
  docks VARCHAR(45) NULL,
  resources VARCHAR(45) NULL,
  total_num_of_pallets_received INT NULL DEFAULT NULL 
  PRIMARY KEY (po_id),
  FOREIGN KEY (ship_via_contact_id) REFERENCES shipping_custom_contact_info (id)
    ON DELETE RESTRICT
    ON UPDATE CASCADE
) ENGINE = InnoDB;

CREATE TABLE purchase_order_lineitems (
  po_id INT NOT NULL AUTO_INCREMENT,
  line_number INT UNSIGNED NOT NULL,
  sku VARCHAR(50),
  supplier_sku VARCHAR(50),
  qty INT NOT NULL,
  unit_cost DOUBLE(10,2),
  product_name varchar(120) DEFAULT NULL,
  PRIMARY KEY(po_id, line_number),
  FOREIGN KEY (po_id) REFERENCES purchase_orders (po_id)
    ON DELETE CASCADE
    ON UPDATE CASCADE
) ENGINE = InnoDB;


CREATE TABLE purchase_order_status_history (
  po_id INT NOT NULL,
  status varchar(10) NOT NULL default 'pend',
  date_changed DATETIME,
  comments TEXT,
  updated_by VARCHAR(80),
  send_email bool NOT NULL,
  FOREIGN KEY (po_id) REFERENCES purchase_orders (po_id)
  	ON DELETE CASCADE
  	ON UPDATE CASCADE
) ENGINE = InnoDB;

CREATE TABLE import_export_history (
  type varchar(10) NOT NULL,
  created DATETIME,
  username VARCHAR(80) NOT NULL,
  imported bool NOT NULL
) ENGINE = InnoDB;

CREATE TABLE orders_packinglist (
  packinglist_num VARCHAR(255) NOT NULL,
  order_id INT UNSIGNED NOT NULL,
  tracking VARCHAR(255),
  created DATETIME,
  last_modified DATETIME,
  user_notified bool NOT NULL,
  shipped DATE,
  shipping_cost DOUBLE(10,2),
  sub_total double(10,2),
  grand_total double(10,2),
  primary key (packinglist_num)
) ENGINE = InnoDB;

CREATE TABLE orders_packinglist_lineitems (  	
  packinglist_num VARCHAR(255) NOT NULL,
  line_num INT UNSIGNED NOT NULL,
  sku VARCHAR(50),
  quantity INT NOT NULL,
  primary key (packinglist_num, line_num),
  FOREIGN KEY (packinglist_num) REFERENCES orders_packinglist (packinglist_num)
  	ON DELETE CASCADE
  	ON UPDATE CASCADE
) ENGINE=InnoDB;

CREATE TABLE multi_store (
  host varchar(50) NOT NULL default '',
  store_id varchar(2),
  contact_email varchar(80) NOT NULL default '',
  mid_new_registration INT unsigned,
  registration_success_url VARCHAR(80) default 'account.jhtm',
  mid_new_orders INT unsigned,
  mid_shipped_orders INT unsigned,
  min_order_amt double(10,2),
  min_order_amt_wholesale double(10,2),
  PRIMARY KEY (host)
) ENGINE=InnoDB;

CREATE TABLE giftcard_orders (
  giftcard_orderid INT NOT NULL AUTO_INCREMENT,
  date_created DATE,
  sub_total double(10,2),
  qty_giftcard INT unsigned NOT NULL,
  cc_type varchar(10),
  cc_number varchar(16),
  cc_exp_month varchar(2),
  cc_exp_year varchar(4),
  cc_card_code varchar(4),
  cc_trans_id varchar(20), 
  cc_payment_date datetime,
  cc_payment_status varchar(10),
  cc_payment_note varchar(200),
  bill_to_first_name varchar(80),
  bill_to_last_name varchar(80),
  bill_to_company varchar(50),
  bill_to_addr1 varchar(80),
  bill_to_addr2 varchar(80),  
  bill_to_city varchar(80),
  bill_to_state_province varchar(80),
  bill_to_zip varchar(20),
  bill_to_country varchar(50),
  bill_to_phone varchar(80),
  bill_to_cell_phone varchar(80),
  bill_to_fax varchar(80),
  status varchar(3) default NULL,
  payment_method varchar(20) NOT NULL,
  status_date_changed datetime,
  PRIMARY KEY (giftcard_orderid)
) ENGINE=InnoDB;	

CREATE TABLE giftCard_status_history (
  id INT unsigned NOT NULL auto_increment,
  giftcard_orderid INT NOT NULL,
  status varchar(3) default NULL,
  date_changed datetime,
  comments text,
  username varchar(50) default NULL,
  PRIMARY KEY (id),
  FOREIGN KEY (giftcard_orderid) REFERENCES giftcard_orders (giftcard_orderid)
  	ON DELETE CASCADE
  	ON UPDATE CASCADE
) ENGINE=InnoDB;

CREATE TABLE giftcards (
  giftcard_orderid INT,
  code char(18) NOT NULL,
  userid INT unsigned NOT NULL,
  amount DOUBLE(10,2) NOT NULL,
  date_created DATE,
  recipient_email varchar(80) NOT NULL,
  recipient_firstname varchar(80) default NULL,
  recipient_lastname varchar(80) default NULL,
  sender_firstname varchar(80) default NULL,
  sender_lastname varchar(80) default NULL,
  message varchar(255),
  date_redeemed DATE,
  amount_redeemed DOUBLE(10,2) NOT NULL DEFAULT 0.0,
  redeemed_by_id INT default NULL,
  redeemed_by_firstname varchar(80) default NULL,
  redeemed_by_lastname varchar(80) default NULL,
  active bool NOT NULL default 1,
  type bool NOT NULL,
  UNIQUE KEY (code),
  PRIMARY KEY (code),
  FOREIGN KEY (giftcard_orderid) REFERENCES giftcard_orders (giftcard_orderid)
    ON DELETE CASCADE
    ON UPDATE CASCADE
) ENGINE=InnoDB;

ALTER TABLE giftcard_orders AUTO_INCREMENT = 1000001;

CREATE TABLE subscriptions (
  code char(18) NOT NULL,
  userid INT unsigned NOT NULL,
  created datetime,
  last_modified datetime,
  product_name varchar(120),
  product_sku varchar(50),
  quantity INT NOT NULL,
  unit_price double(10,2),  
  case_content int unsigned,
  packing varchar(50),
  taxable bool NOT NULL default true,
  enabled bool NOT NULL,
  interval_unit tinyint unsigned default 1,
  interval_type char(1) default 'm',
  next_order date NOT NULL,
  last_order date,
  order_id_orig INT unsigned,
  order_id_last INT unsigned,
  PRIMARY KEY (code),
  index(next_order),
  FOREIGN KEY (userid) REFERENCES users (id)
    ON DELETE CASCADE
    ON UPDATE CASCADE
) ENGINE = InnoDB;

CREATE TABLE subscriptions_attributes (  	
  subscription_code char(18) NOT NULL,
  option_name varchar(100) NOT NULL default '',
  value_name varchar(110) NOT NULL default '',
  FOREIGN KEY (subscription_code) REFERENCES subscriptions (code)
  	ON DELETE CASCADE
  	ON UPDATE CASCADE
) ENGINE=InnoDB;

CREATE TABLE rma (
  rma_num INT unsigned NOT NULL auto_increment,
  order_id INT unsigned NOT NULL,
  serial_num varchar(50),
  product_sku varchar(50),
  product_name varchar(120),
  unit_price double(10,2),
  report_date datetime,
  last_modified datetime,
  complete_date date,  
  status varchar(20) NOT NULL default 'Pending',
  action varchar(20),
  comments text,
  problem text,
  PRIMARY KEY (rma_num)
) ENGINE=InnoDB;

ALTER TABLE rma AUTO_INCREMENT = 1000001;

CREATE TABLE csv_feed (
  feed varchar(25),
  column_num SMALLINT unsigned,
  header_name varchar(30),
  column_name varchar(50) NOT NULL default '',
  zero_as_null bool NOT NULL,
  is_numeric bool NOT NULL,
  whole_num bool NOT NULL,
  override_value varchar(30) default NULL,
  UNIQUE KEY (feed, header_name),
  UNIQUE KEY (feed, column_num)
) ENGINE=InnoDB;

CREATE TABLE admin_message (
  message_id INT unsigned NOT NULL AUTO_INCREMENT,
  message MEDIUMTEXT,
  PRIMARY KEY(message_id)
) ENGINE = InnoDB;

INSERT INTO admin_message VALUES  
 (1, null);
 
CREATE TABLE brand (
  brand_id INT unsigned NOT NULL AUTO_INCREMENT,
  sku_prefix varchar(10),
  brand_name varchar(50),
  option_name varchar(100),
  value_name varchar(110),
  value_title varchar(50),
  PRIMARY KEY (brand_id)
) ENGINE = InnoDB;
 
CREATE TABLE budget_brand (
  userid INT unsigned NOT NULL,
  brand_id INT unsigned NOT NULL,
  budget double(10,2),
  PRIMARY KEY  (userid, brand_id),
  FOREIGN KEY (userid) REFERENCES users (id)
    ON DELETE CASCADE
    ON UPDATE CASCADE,
  FOREIGN KEY (brand_id) REFERENCES brand (brand_id)
  	ON DELETE CASCADE
  	ON UPDATE CASCADE
) ENGINE = InnoDB;

CREATE TABLE manufacturers (
  manufacturers_id int NOT NULL auto_increment,
  name varchar(80) NOT NULL,
  image_url varchar(255),
  date_added datetime,
  last_modified timestamp,
  min_order double(10,2) unsigned default NULL,
  UNIQUE KEY (name),
  PRIMARY KEY (manufacturers_id)
);

CREATE TABLE email_campaign (
  campain_id INT unsigned NOT NULL AUTO_INCREMENT,
  campaign_name varchar(150),
  message TEXT,
  sender varchar(100),
  subject varchar(150),
  created datetime,
  scheduled datetime,
  sent datetime,
  status varchar(3) NOT NULL default 'sch',
  email_sent INT unsigned default 0,
  email_to_send INT unsigned default 0,
  email_bounced INT(10) unsigned NOT NULL default 0,
  opened_count INT(10) NOT NULL DEFAULT '0',
  PRIMARY KEY  (campain_id)
) ENGINE = InnoDB;

CREATE TABLE email_campaign_point (
  id INT unsigned NOT NULL AUTO_INCREMENT,
  created datetime,
  user varchar(100),
  note varchar(500) default NULL,
  point INT unsigned default NULL,
  amount double(10,2),
  balance INT unsigned default 0,
  trans_id varchar(50) default NULL,
  payment_status varchar(50) default NULL,
  payment_method varchar(50) default NULL,
  PRIMARY KEY  (id)
) ENGINE = InnoDB;

INSERT INTO email_campaign_point values
 (1,now(),'AEM','Initial Point',50,0.50,50,'','','AEM Credit');
 
CREATE TABLE `email_campaign_click_stats` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `campaign_id` int(10) unsigned DEFAULT NULL,
  `url` varchar(255) DEFAULT NULL,
  `click_count` int(10) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `id_url` (`campaign_id`,`url`)
) ENGINE=InnoDB;

CREATE TABLE `unsubscribe_click_detail` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `campaign_id` int(10) unsigned DEFAULT NULL,
  `user_id` int(10) unsigned DEFAULT NULL,
  `email` varchar(50) DEFAULT NULL,
  `ip_address` varchar(15) DEFAULT NULL,
  `date` datetime DEFAULT NULL,
  KEY `id` (`id`),
  CONSTRAINT `unsubscribe_click_detail_ibfk_1` FOREIGN KEY (`campaign_id`) REFERENCES `email_campaign` (`campain_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB;


CREATE TABLE `email_campaign_click_detail` (
  `click_id` int(10) unsigned NOT NULL,
  `user_id` int(10) unsigned DEFAULT NULL,
  `contact_id` int(10) unsigned DEFAULT NULL,
  `email` varchar(50) DEFAULT NULL,
  `ip_address` varchar(15) DEFAULT NULL,
  `date` datetime DEFAULT NULL,
  KEY `click_id` (`click_id`),
  CONSTRAINT `email_campaign_click_detail_ibfk_1` FOREIGN KEY (`click_id`) REFERENCES `email_campaign_click_stats` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB;

CREATE TABLE `email_campaign_open_detail` (
  `campaign_id` int(10) unsigned DEFAULT NULL,
  `user_id` int(10) unsigned DEFAULT NULL,
  `contact_id` int(10) unsigned DEFAULT NULL,
  `email` varchar(50) DEFAULT NULL,
  `opened_count` int(10) NOT NULL DEFAULT '0',
  `date` datetime DEFAULT NULL,
  UNIQUE KEY `email` (`email`,`campaign_id`),
  KEY `campaign_id` (`campaign_id`),
  CONSTRAINT `email_campaign_open_detail_ibfk_1` FOREIGN KEY (`campaign_id`) REFERENCES `email_campaign` (`campain_id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB;    

CREATE TABLE locations (
  id INT unsigned NOT NULL AUTO_INCREMENT,
  userid INT unsigned default NULL,
  name varchar(100),
  keywords text NOT NULL default '',
  addr1 varchar(80),
  addr2 varchar(80),  
  city varchar(80),
  state_province varchar(80),
  zip varchar(20),
  country varchar(50),
  phone varchar(80),
  fax varchar(80),
  html_code mediumtext default NULL,
  PRIMARY KEY  (id)
) ENGINE = InnoDB; 

CREATE TABLE location_list (
  userid INT unsigned NOT NULL,
  location_id INT unsigned NOT NULL,
  added datetime,
  location_group_id INT unsigned default 0,
  PRIMARY KEY  (userid,location_id),
  FOREIGN KEY (userid) REFERENCES users (id)
  	ON DELETE CASCADE
  	ON UPDATE CASCADE,
  FOREIGN KEY (location_id) REFERENCES locations (id)
  	ON DELETE CASCADE
  	ON UPDATE CASCADE
) ENGINE=InnoDB;

CREATE TABLE buy_request (
  id INT unsigned NOT NULL AUTO_INCREMENT,
  owner_id INT unsigned NOT NULL,
  status VARCHAR(10) NOT NULL default 'open',
  name varchar(120) NOT NULL,
  short_desc varchar(512),
  long_desc text,
  created datetime,
  last_modified timestamp,
  active_from datetime,
  active_to datetime,
  protected_level BIT(64) NOT NULL default b'0',
  keywords text NOT NULL default '',
  PRIMARY KEY  (id),
  index (created)
) ENGINE = InnoDB; 

CREATE TABLE buy_request_versions (
  id INT unsigned NOT NULL AUTO_INCREMENT,
  buy_request_id INT unsigned NOT NULL,
  dialog TEXT,
  created DATETIME,
  owner_id INT unsigned NOT NULL,
  user_id INT unsigned NOT NULL,
  PRIMARY KEY (id)
) ENGINE = InnoDB;

CREATE TABLE buy_request_list (
  userid INT unsigned NOT NULL,
  buy_request_id INT unsigned NOT NULL,
  created datetime,
  PRIMARY KEY  (userid,buy_request_id),
  FOREIGN KEY (userid) REFERENCES users (id)
  	ON DELETE CASCADE
  	ON UPDATE CASCADE,
  FOREIGN KEY (buy_request_id) REFERENCES buy_request (id)
  	ON DELETE CASCADE
  	ON UPDATE CASCADE
) ENGINE=InnoDB;

CREATE TABLE category_buyrequest (
  category_id INT unsigned NOT NULL,
  buyrequest_id INT unsigned NOT NULL,
  new_entry bool NOT NULL,
  PRIMARY KEY (category_id, buyrequest_id),
  KEY (category_id),
  KEY (buyrequest_id),
  FOREIGN KEY (category_id) REFERENCES category (id) 
  	ON DELETE CASCADE
  	ON UPDATE CASCADE,
  FOREIGN KEY (buyrequest_id) REFERENCES buy_request (id)
  	ON DELETE CASCADE
  	ON UPDATE CASCADE
) ENGINE=InnoDB;

CREATE TABLE product_reviews (
  id int NOT NULL auto_increment,
  active bool NOT NULL default 0,
  product_sku varchar(50),
  user_id INT unsigned NOT NULL,
  user_name varchar(100) NOT NULL,
  reviews_rating INT(1),
  date_added datetime,
  last_modified datetime,
  reviews_title varchar(100) NOT NULL,
  reviews_text text NOT NULL,
  review_type varchar(10),
  ip_address text,
  PRIMARY KEY (id),
  FOREIGN KEY (product_sku) REFERENCES product (sku)
  	ON DELETE CASCADE
  	ON UPDATE CASCADE  
) ENGINE=InnoDB;

CREATE TABLE company_reviews (
  id int NOT NULL auto_increment,
  active bool NOT NULL default 0,
  company_id INT unsigned NOT NULL,
  user_id INT unsigned NOT NULL,
  user_name varchar(100) NOT NULL,
  reviews_rating INT(1),
  date_added datetime,
  last_modified datetime,
  reviews_title varchar(100) NOT NULL,
  reviews_text text NOT NULL,
  PRIMARY KEY (id),
  FOREIGN KEY (company_id) REFERENCES users (id)
  	ON DELETE CASCADE
  	ON UPDATE CASCADE  
) ENGINE=InnoDB;

CREATE TABLE labels (
  label_type varchar(20) NOT NULL default '',
  label_title varchar(100),
  PRIMARY KEY (label_type)
);

CREATE TABLE event (
  id INT unsigned NOT NULL auto_increment,
  event_name varchar(30),
  start_date datetime,
  end_date datetime,
  active bool,
  active_user_only bool,
  html_code mediumtext,
  PRIMARY KEY  (id)
) ENGINE=InnoDB;

CREATE TABLE event_user (
  event_id INT unsigned NOT NULL,
  user_id INT unsigned NOT NULL,
  username VARCHAR(80) NOT NULL,
  first_name varchar(80),
  last_name varchar(80),
  card_id varchar(50) default NULL,
  user_index INT unsigned NOT NULL,
  salesrep_id INT unsigned,
  register_date datetime,
  html_code mediumtext,
  comment VARCHAR(255),
  field_1 tinytext,
  field_2 tinytext,
  field_3 tinytext,
  field_4 tinytext,
  field_5 tinytext,
  PRIMARY KEY  (event_id,user_id),
  FOREIGN KEY (event_id) REFERENCES event (id)
  	ON DELETE RESTRICT
  	ON UPDATE CASCADE,
  FOREIGN KEY (user_id) REFERENCES users (id)
    ON DELETE RESTRICT
    ON UPDATE CASCADE
) ENGINE=InnoDB;

CREATE TABLE budget_product (  
  id INT unsigned NOT NULL auto_increment,
  userid INT unsigned,
  product_sku varchar(50),
  quantity INT NOT NULL,
  date_entered date,
  PRIMARY KEY (id),
  FOREIGN KEY (userid) REFERENCES users (id) 
  	ON DELETE SET NULL
  	ON UPDATE CASCADE
) ENGINE=InnoDB;

CREATE TABLE echosign (  
  document_key varchar(20),
  date_sent datetime,	
  last_update datetime,
  api_key varchar(20),
  user_key varchar(20),
  megasign_document_key varchar(20),
  widget_document_key varchar(20),  
  agreement_status varchar(20),
  document_url varchar(255),
  history text,
  document_name varchar(255),
  file_name varchar(100),
  userid INT unsigned,
  PRIMARY KEY (document_key)
) ENGINE=InnoDB;

CREATE TABLE echosign_widget (  
  date_created datetime,
  api_key varchar(20),
  user_key varchar(20),
  document_key varchar(20),
  document_name varchar(255),
  file_name varchar(100),
  callback_url varchar(255),
  completion_url varchar(255),
  javascript varchar(255),
  widget_url varchar(255)
) ENGINE=InnoDB;

CREATE TABLE zipcode_handling (  
  country CHAR(2) NOT NULL,
  zipcode VARCHAR(6) NOT NULL,
  handling DOUBLE(8,2),
  PRIMARY KEY (country, zipcode)
) ENGINE=InnoDB;

CREATE TABLE presentation (
  presentation_id INT NOT NULL AUTO_INCREMENT,
  name varchar(150) NOT NULL,
  template_id INT DEFAULT NULL,
  salesrep_id INT DEFAULT NULL,
  added datetime DEFAULT NULL,
  modified datetime DEFAULT NULL,
  viewed INT NOT NULL DEFAULT '0',
  user_id INT DEFAULT NULL,
  title varchar(150) NOT NULL DEFAULT ' ',
  notes mediumtext default NULL,
  last_updated_by INT DEFAULT NULL,
  PRIMARY KEY (presentation_id)
) ENGINE=InnoDB;

CREATE TABLE presentation_product (
  id INT NOT NULL AUTO_INCREMENT,
  sku varchar(50) NOT NULL,
  presentation_id INT DEFAULT NULL,
  sale_rep_id varchar(45) DEFAULT NULL,
  price double NOT NULL DEFAULT '0',
  PRIMARY KEY (id),
  UNIQUE KEY unique_product_presentation (sku,sale_rep_id,presentation_id),
  KEY presentation_id (presentation_id)
) ENGINE=InnoDB;

CREATE TABLE presentation_template (
  template_id INT NOT NULL AUTO_INCREMENT,
  name varchar(150) DEFAULT NULL,
  no_of_product INT NOT NULL DEFAULT '0',
  design text NOT NULL default '',
  footer_html_code mediumtext NOT NULL default '',
  PRIMARY KEY (template_id),
  UNIQUE KEY (template_id),
  UNIQUE KEY (name)
) ENGINE=InnoDB;

CREATE TABLE product_lable_templates (
    id INT unsigned NOT NULL auto_increment,
    active bool NOT NULL default 1,
    name varchar(45) DEFAULT NULL,
    row_1 varchar(45) DEFAULT NULL,
    row_2 varchar(45) DEFAULT NULL,
    row_3 varchar(45) DEFAULT NULL,
    row_4 varchar(45) DEFAULT NULL,
    row_5 varchar(45) DEFAULT NULL,
    row_6 varchar(45) DEFAULT NULL,
    top_left varchar(45) DEFAULT NULL,
    top_right varchar(45) DEFAULT NULL,
    bottom_left varchar(45) DEFAULT NULL,
    bottom_right varchar(45) DEFAULT NULL,
	PRIMARY KEY (id)
) ENGINE=InnoDB;

INSERT INTO product_lable_templates
  (id,active, name,row_1,row_4,row_6,bottom_left)
VALUES(1,1,'template 1','#sku#,12,1','#name#,10,0','$#price1#,9,0','#date#');

CREATE  TABLE budget_partners (
  id INT NOT NULL auto_increment,
  active bool NOT NULL default 1,
  partner_name VARCHAR(45) NULL,
  created_by VARCHAR(45) NULL,
  created_date DATETIME NULL,
  modified_date DATETIME NULL,
  modified_by VARCHAR(45) NULL,
  PRIMARY KEY (id) 
) ENGINE=InnoDB;

CREATE TABLE customer_budget_partners (
  partner_id INT NOT NULL,
  partner_name VARCHAR(45) NULL,
  user_id Int DEFAULT NULL,
  date datetime DEFAULT NULL,
  created_by varchar(45) DEFAULT NULL,
  modified_by varchar(45) DEFAULT NULL,
  modified_date DATETIME NULL,
  amount double DEFAULT NULL,
  FOREIGN KEY (partner_id) REFERENCES budget_partners (id)
) ENGINE=InnoDB;

CREATE  TABLE customer_partner_history (
  user_id INT NULL,
  partner_id INT NULL,
  partner_name VARCHAR(45) NULL,
  order_id INT NULL,
  amount DOUBLE NULL,
  created_by VARCHAR(45) NULL,
  date DATETIME NULL
) ENGINE=InnoDB;



CREATE TABLE crm_qualifier (
  id INT UNSIGNED NOT NULL auto_increment,
  qualifier_number VARCHAR(45) NULL,
  qualifier_name VARCHAR(50) NOT NULL,
  PRIMARY KEY (id),
  UNIQUE KEY (qualifier_name)
) ENGINE = InnoDB;

/**************  CRM  Start *****************/
CREATE TABLE crm_account (
  id INT UNSIGNED NOT NULL auto_increment,
  account_number VARCHAR(45) NULL,
  account_name VARCHAR(80) NOT NULL,
  account_owner VARCHAR(80) NULL,
  account_type VARCHAR(45) NULL,
  access_user_id INT UNSIGNED,
  created DATETIME NULL,
  last_modified DATETIME NULL,
  phone VARCHAR(25) NULL,
  fax VARCHAR(15) NULL,
  description MEDIUMTEXT NULL,
  industry VARCHAR(45) NULL,
  employees INT UNSIGNED NULL,
  website VARCHAR(45) NULL,
  rating VARCHAR(45) NULL,
  annual_revenue INT UNSIGNED NULL,
  shipping_street VARCHAR(80) NULL,
  shipping_city VARCHAR(80) NULL,
  shipping_state_province VARCHAR(80) NULL,
  shipping_zip VARCHAR(20) NULL,
  shipping_country VARCHAR(50) NULL,
  billing_street VARCHAR(45) NULL,
  billing_city VARCHAR(45) NULL,
  billing_state_province VARCHAR(45) NULL,
  billing_zip VARCHAR(20) NULL,
  billing_country VARCHAR(50) NULL,
  PRIMARY KEY (id),
  UNIQUE KEY (account_name)
) ENGINE = InnoDB;

INSERT INTO crm_account (id, account_name, account_owner) VALUES  (1, 'Leads', 'Admin');

CREATE  TABLE crm_contact (
  id INT UNSIGNED NOT NULL auto_increment,
  prospect_id VARCHAR(45) NULL DEFAULT NULL,
  account_id INT UNSIGNED NOT NULL,
  account_name VARCHAR(80) NOT NULL,
  access_user_id INT UNSIGNED default NULL,
  qualifier INT unsigned DEFAULT 0,
  first_name VARCHAR(80) NULL,
  last_name VARCHAR(80) NULL,
  account_number VARCHAR(45) NULL,
  contact_type VARCHAR(45) default NULL,
  rating VARCHAR(45) default NULL,
  created DATETIME NULL,
  created_by VARCHAR(80) NULL,
  last_modified DATETIME NULL,
  lead_source VARCHAR(80) NULL,
  title VARCHAR(80) NULL,
  short_desc VARCHAR(255) default NULL,
  description TEXT NULL,
  department VARCHAR(100) NULL,
  unsubscribe bool NOT NULL default 0,
  verified bool NOT NULL default 1,
  token VARCHAR(40),
  email_1 VARCHAR(80) NULL,
  email_2 VARCHAR(80) NULL,
  website VARCHAR(50) NULL,
  bounced bool NOT NULL default 0,
  phone_1 VARCHAR(25) NULL,
  phone_2 VARCHAR(25) NULL,
  fax VARCHAR(15) NULL,
  street VARCHAR(255) NULL,
  city VARCHAR(80) NULL ,
  state_province VARCHAR(80) NULL,
  zip VARCHAR(20) NULL,
  country VARCHAR(80) NULL,
  address_type VARCHAR(20) NULL,
  other_street VARCHAR(225) NULL,
  other_city VARCHAR(80) NULL,
  other_state_province VARCHAR(80) NULL,
  other_zip VARCHAR(20) NULL,
  other_country VARCHAR(80) NULL ,
  other_address_type VARCHAR(20) NULL,
  trackcode VARCHAR(80) default NULL,
  converted_on DATETIME,
  user_id int UNSIGNED NULL,
  ip_address TEXT default NULL,
  rating1 VARCHAR(45) DEFAULT NULL,
  rating2 VARCHAR(45) DEFAULT NULL,
  not_call TINYINT(1) NOT NULL DEFAULT 0,
  language VARCHAR(3) NOT NULL default 'en',
  duplicate_contact tinyint unsigned default 0,
  PRIMARY KEY (id, account_id),
  INDEX (account_id ASC),
  FOREIGN KEY (account_id) REFERENCES crm_account (id)
    ON DELETE CASCADE
    ON UPDATE CASCADE,
  FOREIGN KEY (access_user_id) REFERENCES access_user (id)
    ON DELETE SET NULL
    ON UPDATE CASCADE
) ENGINE = InnoDB;

CREATE TABLE crm_task (
  id INT UNSIGNED NOT NULL AUTO_INCREMENT,
  account_id INT UNSIGNED NOT NULL,
  contact_id INT UNSIGNED NOT NULL,
  assigned_to_id INT UNSIGNED NOT NULL,
  title VARCHAR(255) NOT NULL,
  type VARCHAR(45) NULL,
  due_date DATETIME NULL,
  description MEDIUMTEXT NULL,
  status VARCHAR(45) NULL,
  priority VARCHAR(45) NULL,
  action VARCHAR(45) NULL,
  created DATETIME NULL,
  last_modified DATETIME NULL,
  reminder DATETIME NULL,
  contact_email VARCHAR(80) NULL,
  contact_phone VARCHAR(15) NULL,
  PRIMARY KEY (id, account_id),
  INDEX (account_id ASC),
  FOREIGN KEY (contact_id) REFERENCES crm_contact (id)
    ON DELETE CASCADE
    ON UPDATE CASCADE,
  FOREIGN KEY (account_id) REFERENCES crm_contact (account_id)
    ON DELETE CASCADE
    ON UPDATE CASCADE  
) ENGINE = InnoDB;

CREATE TABLE crm_group_contact (
  id INT UNSIGNED NOT NULL AUTO_INCREMENT,
  contact_id INT UNSIGNED NOT NULL,
  name varchar(100) NOT NULL,
  active bool NOT NULL DEFAULT 0,
  created datetime,
  last_modified timestamp,
  message_id INT NULL,
  PRIMARY KEY (id),
  UNIQUE KEY (name)
) ENGINE = InnoDB;

CREATE TABLE crm_group_contact_join (
  group_id INT unsigned NOT NULL,
  contact_id INT unsigned NOT NULL,
  new_entry bool NOT NULL,
  PRIMARY KEY  (group_id,contact_id),
  FOREIGN KEY (contact_id) REFERENCES crm_contact (id)
  	ON DELETE CASCADE
  	ON UPDATE CASCADE
) ENGINE=InnoDB;

CREATE TABLE crm_contact_field (
  id INT unsigned NOT NULL auto_increment,
  global_name VARCHAR(15) NOT NULL,
  enabled bool default 0,
  field_name VARCHAR(255),
  field_type INT NOT NULL default 1,
  options VARCHAR(64500),
  PRIMARY KEY (id)
) ENGINE=InnoDB;
INSERT INTO crm_contact_field (global_name) VALUES ('Field 1'), ('Field 2'),('Field 3'), ('Field 4'),('Field 5'), ('Field 6'),('Field 7'), ('Field 8'), ('Field 9'), ('Field 10'),('Field 11'), ('Field 12'),('Field 13'), ('Field 14'),('Field 15'), ('Field 16'),('Field 17'), ('Field 18'), ('Field 19'), ('Field 20');

CREATE TABLE crm_contact_contactfield (
  contact_id INT UNSIGNED NOT NULL,
  field_id INT unsigned NOT NULL,
  field_value VARCHAR(1024) default NULL,
  PRIMARY KEY  (contact_id,field_id),
  KEY (contact_id),
  KEY (field_id),
  FOREIGN KEY (contact_id) REFERENCES crm_contact (id) 
  	ON DELETE CASCADE
  	ON UPDATE CASCADE,
  FOREIGN KEY (field_id) REFERENCES crm_contact_field (id)
  	ON DELETE CASCADE
  	ON UPDATE CASCADE
) ENGINE=InnoDB;

CREATE TABLE crm_form (
  form_id INT unsigned NOT NULL auto_increment,
  form_name VARCHAR(30) NOT NULL,
  recipient_email VARCHAR(80),
  crm_form.recipient_cc_email VARCHAR(80),
  crm_form.recipient_bcc_email VARCHAR(80),
  return_url VARCHAR(255),
  number_of_fields INT default 1,
  form_number VARCHAR(40) NOT NULL,
  double_opt_in bool NOT NULL default 0,
  header TEXT,
  footer TEXT,
  send_button VARCHAR(20),
  PRIMARY KEY (form_id)
) ENGINE=InnoDB;

CREATE TABLE crm_form_field (
  field_id INT unsigned NOT NULL auto_increment,
  form_id INT unsigned NOT NULL,
  field_name VARCHAR(100) NOT NULL,
  type VARCHAR(20) NOT NULL default 'text',
  required bool default 0,
  contact_field_id INT unsigned,
  options VARCHAR(1024),
  PRIMARY KEY (field_id),
  FOREIGN KEY (form_id) REFERENCES crm_form (form_id)
  	ON DELETE CASCADE
  	ON UPDATE CASCADE
) ENGINE=InnoDB;

INSERT INTO site_config VALUES  
 ('CRM_LEAD_SOURCE',''),
 ('CRM_TASK_TYPE','Call,Email,Fax,Letter,Visit,Meeting,Event'),
 ('CRM_TASK_RANK','High, Normal, Low'),
 ('CRM_ACCOUNT_INDUSTRY','Apparel,Banking,Biotechnolgy,Chemicals,Communications,Constructions,Consulting,Education,Electronics,Energy,Engineering,Entertainment,Enviornmental,Finance,Govenment,Healthcare,Hospitality,Insurance,Machinery,Manufacturing,Media,Not For Profit,Recreation,Retail,Shipping,Technology,Telecommunications,Transportations,Utilies,Other'),
 ('CRM_ACCOUNT_CONTACT_TYPE','Analyst,Competitor,Customer,Integrator,Investor,Partner,Press,Prospect,Reseller,Other'),
 ('CRM_RATING','Sold,Closing,Hot A+,Hot A,Hot B,Hot C,Warm,Cold,Dead,WholesalerTop100'),
 ('CRM_TASK_ACTION','');
 
CREATE TABLE crm_contact_customer_map (
  customer_property VARCHAR(50) NOT NULL,
  crm_contact_property VARCHAR(50) NOT NULL
) ENGINE=InnoDB;

INSERT INTO crm_contact_customer_map (customer_property, crm_contact_property) VALUES ('getAddress_getFirstName','getFirstName'), ('getAddress_getLastName','getLastName'), ('getAddress_getAddr1','getStreet'), ('getAddress_getAddr2',''),
('getAddress_getCity','getCity'), ('getAddress_getStateProvince','getStateProvince'), ('getAddress_getZip','getZip'), ('getAddress_getCountry','getCountry'),('getAddress_getPhone','getPhone1'), ('getAddress_getCellPhone','getPhone2'), ('getAddress_getFax','getFax'), ('getUsername','getEmail1'),('getTaxId',''),
( 'getNote','getDescription'), ('getTrackcode','getTrackcode'),('getRegisteredBy','getLeadSource'),('getField1',''),('getField2',''),('getField3',''),('getField4',''),('getField5',''),
('getField6',''),('getField7',''),('getField8',''),('getField9',''),('getField10',''),('getField11',''),('getField12',''),('getField13',''),('getField14',''),('getField15',''),
('getField16',''), ('getField17',''),('getField18',''),('getField19',''),('getField20',''),('getRating1','getRating1'),('getRating2','getRating2'),('isDoNotCall','isDoNotCall'),('getQualifier','getQualifier'),('getLanguage','getLanguage');

CREATE  TABLE next_index (
  name VARCHAR(45) NOT NULL ,
  next_index INT(11) UNSIGNED NOT NULL DEFAULT '1' ,
  PRIMARY KEY (name) 
)ENGINE=InnoDB;
  
INSERT INTO next_index (name, next_index) VALUES 
('customer_account_number', '1');

INSERT INTO site_config (config_key, config_value) VALUES 
('CUSTOMER_AUTOGENERATE_ACCOUNTNUMBER', 'False');

CREATE  TABLE dialing_note_history (
  note_id INT UNSIGNED NOT NULL AUTO_INCREMENT ,
  crm_contact_id INT UNSIGNED,
  customer_id INT UNSIGNED,
  note TEXT NULL ,
  create_date DATETIME NULL ,
  PRIMARY KEY (note_id) )ENGINE=InnoDB;
/**************  CRM  End *****************/

/**************  SearchEngine Prospect Start *****************/
CREATE TABLE search_engine_prospect (
  id int(11) NOT NULL AUTO_INCREMENT,
  prospect_id varchar(45) NOT NULL,
  referrer varchar(45) NOT NULL,
  query_string text,
  timestamp timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (id)
) ENGINE=InnoDB;

/**************  SearchEngine Prospect  End *****************/

/**************  Views Start *****************/
CREATE VIEW affiliate_numchild_view AS select child_id, count(parent_id) as affiliate_num_child from edges GROUP BY child_id;  

CREATE VIEW orders_customer_email_view AS select order_id, orders.userid, username from orders, users where orders.userid = users.id;

CREATE VIEW orders_customer_view AS select userid, sum(grand_total) AS grand_total, count(order_id) AS order_count, max(date_ordered) AS last_ordered_date, Round(AVG (grand_total),2) AS average_order from orders where status not like 'x%' GROUP BY userid;

CREATE VIEW quotes_customer_view AS select userid, sum(grand_total) AS grand_total, count(order_id) AS quote_count, max(date_ordered) AS last_ordered_date, Round(AVG (grand_total),2) AS average_quotes from orders where  status  = 'xq' GROUP BY userid;

CREATE VIEW orders_shipped_date_view AS SELECT order_id, Max(shipped) as shipped FROM orders_packinglist WHERE shipped is not null group by order_id;

CREATE VIEW users_subcount_view AS select parent AS id, count(id) AS subcount from users GROUP BY parent;

CREATE VIEW workorder_lineitems_view AS SELECT work_order_num, product_sku, unit_price FROM orders, orders_lineitems WHERE orders.order_id = orders_lineitems.order_id AND work_order_num IS NOT NULL GROUP BY work_order_num, product_sku;	

CREATE VIEW customer_affiliate_view 
AS 
SELECT 
	order_id AS order_id
  , NULL AS product_sku
  , 0 AS order_line_num
  , affiliate_id_level1 AS user_id
  , 0 AS supplier_id
  , commission_level1 AS commission
  , 0 AS consignment
  , sub_total AS order_sub_total
  , vba_payment_status AS vba_payment_status
  , orders.date_ordered AS date_ordered
  , (SELECT date_ordered from orders as orders_first where orders_first.userid= orders.userid and is_first is true) as date_first_ordered
FROM 
	orders 
WHERE 
	commission_level1 is not null
UNION ALL
SELECT
    order_id AS order_id
  , NULL AS product_sku
  , 0 AS order_line_num
  , affiliate_id_level2 AS user_id
  , 0 AS supplier_id
  , commission_level2 AS commission
  , 0 AS consignment
  , sub_total AS order_sub_total
  , vba_payment_status AS vba_payment_status
  , orders.date_ordered AS date_ordered
  , (SELECT date_ordered from orders as orders_first where orders_first.userid= orders.userid and is_first is true) as date_first_ordered
FROM 
	orders 
WHERE 
	commission_level2 is not null;

CREATE VIEW customer_consignment_view
AS
SELECT 
       order_id AS order_id
     , product_sku AS product_sku
  	 , line_num AS order_line_num
     , (SELECT id from users where users.supplier_id=orders_lineitems.supplier_id) AS user_id
     , supplier_id AS supplier_id
     , 0 AS commission
     , round((case when (cost_percent = '1') then ((unit_price * quantity) * (cost / 100)) else (cost * quantity) end),3) AS consignment
     , (unit_price * quantity) AS order_sub_total
     , vba_payment_status AS vba_payment_status 
FROM
 	orders_lineitems
WHERE
    consignment_product = 1;

CREATE VIEW  customer_affiliate_consignment_totals_view 
AS 
SELECT 
	order_id AS order_id,
	product_sku AS product_sku,
	order_line_num AS order_line_num,
	user_id AS user_id,
	supplier_id AS supplier_id,
	commission AS commission,
	consignment AS consignment,
	order_sub_total AS order_sub_total,
	vba_payment_status AS vba_payment_status,
	customer_affiliate_view.date_ordered as date_ordered,
	customer_affiliate_view.date_first_ordered as date_first_ordered	
FROM
 customer_affiliate_view 

UNION ALL

SELECT 
	order_id AS order_id,
	product_sku AS product_sku,
	order_line_num AS order_line_num,
	user_id AS user_id,
	supplier_id AS supplier_id,
	commission AS commission,
	consignment AS consignment,
	order_sub_total AS order_sub_total,
	vba_payment_status AS vba_payment_status,
	null as date_ordered,
	null as date_first_ordered
FROM
 customer_consignment_view;
 
 CREATE VIEW order_status_shipped_unique 
 AS 	 
 SELECT * FROM orders_status_history 
 WHERE  orders_status_history.status='s'
 GROUP BY order_id
/**************  Views End *****************/


/**************  Procedures Start *****************/
delimiter ;;
CREATE PROCEDURE `familyTree`( IN root INT, IN direction CHAR(4) )
BEGIN
	DROP TABLE IF EXISTS family_tree;
	CREATE TABLE family_tree (
		user_id INT PRIMARY KEY,
		level SMALLINT
	) ENGINE=HEAP;
	INSERT INTO family_tree VALUES (root, 1);
	IF direction = 'up' THEN
		WHILE ROW_COUNT() > 0 DO
			INSERT IGNORE INTO family_tree
				SELECT parent, NULL FROM users, family_tree WHERE users.id = family_tree.user_id;
		END WHILE;
	ELSE
		WHILE ROW_COUNT() > 0 DO
			INSERT IGNORE INTO family_tree
				SELECT id, level+1 FROM users, family_tree WHERE users.parent = family_tree.user_id;
		END WHILE;
	END IF;	
	SELECT user_id, level, id, parent, username  FROM family_tree LEFT JOIN users ON family_tree.user_id = users.id;
	DROP TABLE family_tree;
END
 ;;
delimiter ;


DROP PROCEDURE IF EXISTS `ListReached`;
delimiter ;;
CREATE PROCEDURE `ListReached`(IN root INT, IN list_type INT)
BEGIN
  DECLARE rows SMALLINT DEFAULT 0;
  DROP TABLE IF EXISTS reached;
  CREATE TABLE reached (
    node_id INT PRIMARY KEY
  ) ENGINE=HEAP;
  INSERT INTO reached VALUES (root );
  SET rows = ROW_COUNT();
  WHILE rows > 0 DO
  IF list_type <= 0 THEN
    INSERT IGNORE INTO reached
      SELECT DISTINCT child_id
      FROM edges AS e
      INNER JOIN reached AS p ON e.parent_id = p.node_id;
   END IF;
    SET rows = ROW_COUNT();
   IF list_type >= 0 THEN
    INSERT IGNORE INTO reached
      SELECT DISTINCT parent_id
      FROM edges AS e
      INNER JOIN reached AS p ON e.child_id = p.node_id;
   END IF;
    SET rows = rows + ROW_COUNT();
  END WHILE;
  SELECT first_name, last_name, reached.node_id FROM reached, address,users, affiliates WHERE affiliates.node_id = reached.node_id and users.default_address_id = address.id and address.userid = reached.node_id;
  DROP TABLE reached;
END
 ;;
delimiter ;
/**************  Procedures End *****************/



/* Add category  */
INSERT INTO `category` (`id`,`name`,`created`,`last_modified`,`protected_level`) VALUES (1,'Category1',now(),NULL,0);
INSERT INTO `category` (`id`,`name`,`created`,`last_modified`,`protected_level`) VALUES (2,'Category2',now(),NULL,0);
INSERT INTO `category` (`id`,`name`,`created`,`last_modified`,`protected_level`) VALUES (3,'Category3',now(),NULL,0);

/* Add Product  */
INSERT INTO `product` (`id`,`created`,`created_by`,`sku`,`name`,`protected_level`,`keywords`,`taxable`,`searchable`,`active`,`price_1`) VALUES (1,'2012-12-13 12:05:06','Admin','p1','Product1',0,'product1',1,1,1,50.000);
INSERT INTO `product` (`id`,`created`,`created_by`,`sku`,`name`,`protected_level`,`keywords`,`taxable`,`searchable`,`active`,`price_1`) VALUES (2,'2012-12-13 12:05:41','Admin','p2','Product2',0,'product2',1,1,1,100.000);
INSERT INTO `product` (`id`,`created`,`created_by`,`sku`,`name`,`protected_level`,`keywords`,`taxable`,`searchable`,`active`,`price_1`) VALUES (3,'2012-12-13 12:06:32','Admin','p3','Product3',0,'product3',1,1,1,150.000);

/* Add Product Category Association */
INSERT INTO `category_product` (`category_id`,`product_id`,`rank`,`new_entry`) VALUES (1,1,NULL,0);
INSERT INTO `category_product` (`category_id`,`product_id`,`rank`,`new_entry`) VALUES (1,2,NULL,0);
INSERT INTO `category_product` (`category_id`,`product_id`,`rank`,`new_entry`) VALUES (2,1,NULL,0);
INSERT INTO `category_product` (`category_id`,`product_id`,`rank`,`new_entry`) VALUES (2,2,NULL,0);
INSERT INTO `category_product` (`category_id`,`product_id`,`rank`,`new_entry`) VALUES (2,3,NULL,0);
INSERT INTO `category_product` (`category_id`,`product_id`,`rank`,`new_entry`) VALUES (3,2,NULL,0);
INSERT INTO `category_product` (`category_id`,`product_id`,`rank`,`new_entry`) VALUES (3,3,NULL,0);

CREATE TABLE crm_contact_durations (
	email varchar(150) NOT NULL,
	crm_time BIGINT UNSIGNED NOT NULL
);

CREATE  TABLE mass_email_history (
  id INT unsigned NOT NULL auto_increment,
  message_id INT UNSIGNED NOT NULL,
  crm_contact_id INT UNSIGNED,
  customer_id INT UNSIGNED,
  created DATETIME NULL,
  PRIMARY KEY  (id)
)ENGINE=InnoDB;


/**************  invoice import staging tableDB Start*****************/

CREATE TABLE product (
  id INT unsigned NOT NULL,
  field_32 varchar(50),
  field_47 varchar(50)
) ENGINE=InnoDB;

CREATE TABLE product_category (
  id INT unsigned NOT NULL,
  category_id INT unsigned NOT NULL
) ENGINE=InnoDB;

CREATE TABLE remove_category (
  id INT unsigned NOT NULL,
  category_id INT unsigned NOT NULL
) ENGINE=InnoDB;

CREATE TABLE user_notifications (
  user_id INT unsigned NOT NULL,
  crm_id INT unsigned NOT NULL,
  email_notify bool NOT NULL default 0, 
  unsubscribe bool NOT NULL default 0,
  text_message_notify bool NOT NULL default 0,
  tax_id  VARCHAR(80) default NULL, 
  field_1  VARCHAR(80) default NULL, 
  field_2  VARCHAR(80) default NULL, 
  field_3  VARCHAR(80) default NULL, 
  field_4  VARCHAR(80) default NULL, 
  field_5  VARCHAR(80) default NULL, 
  field_6  VARCHAR(80) default NULL, 
  field_7  VARCHAR(80) default NULL, 
  field_8  VARCHAR(80) default NULL, 
  field_9  VARCHAR(80) default NULL, 
  field_10  VARCHAR(80) default NULL, 
  field_11  VARCHAR(80) default NULL, 
  field_12  VARCHAR(80) default NULL, 
  field_13  VARCHAR(80) default NULL, 
  field_14  VARCHAR(80) default NULL, 
  field_15  VARCHAR(80) default NULL, 
  field_16  VARCHAR(80) default NULL, 
  field_17 VARCHAR(80) default NULL, 
  field_18  VARCHAR(80) default NULL, 
  field_19  VARCHAR(80) default NULL, 
  field_20  VARCHAR(80) default NULL
) ENGINE=InnoDB;

/************** invimport staging tableDB End *****************/


/**************  order quote staging tableDB Start*****************/

CREATE TABLE order_quote (
  order_id INT unsigned NOT NULL,
  quote varchar(255)
) ENGINE=InnoDB;

/************** order quote tableDB End *****************/
/*** 14-04-2023 ***/
CREATE TABLE user_ext_contact (
  id int(10) unsigned NOT NULL AUTO_INCREMENT,
  user_id int(10) unsigned NOT NULL,
  name varchar(80) DEFAULT NULL,
  position varchar(80) DEFAULT NULL,
  email varchar(255) DEFAULT NULL,
  phone1 varchar(80) DEFAULT NULL,
  phone2 varchar(80) DEFAULT NULL,
  PRIMARY KEY (id),
  KEY user_id (user_id),
  CONSTRAINT userextcontact_ibfk_1 FOREIGN KEY (user_id) REFERENCES users (id) ON DELETE CASCADE ON UPDATE NO ACTION
) ENGINE=InnoDB;


ALTER TABLE product
ADD COLUMN program VARCHAR(50) NULL AFTER `class_field`;
