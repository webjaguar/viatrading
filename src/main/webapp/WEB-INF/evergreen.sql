CREATE TABLE evergreen_claim_header (
  ae_claim_id INT unsigned NOT NULL auto_increment,
  client_id varchar(50),  
  order_id varchar(50),
  contact_name varchar(255),  
  contact_phone varchar(50),  
  claim_desc text,
  created datetime,
  evergreen_suc char(1),
  PRIMARY KEY  (ae_claim_id)
) ENGINE=InnoDB;

insert into evergreen_claim_header (ae_claim_id) values (1000000);

CREATE TABLE evergreen_claim_detail (
  ae_claim_id INT unsigned NOT NULL,
  claim_sku varchar(100),  
  claim_qty INT NOT NULL,
  claim_type varchar(20),  
  FOREIGN KEY (ae_claim_id) REFERENCES evergreen_claim_header (ae_claim_id)
  	ON DELETE CASCADE
  	ON UPDATE CASCADE  
) ENGINE=InnoDB;

