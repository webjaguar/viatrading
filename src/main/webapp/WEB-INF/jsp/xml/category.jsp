<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%><%-- 
--%><?xml version='1.0'?>
<Category hasImage="${category.hasImage}" displayMode="${category.displayMode}" productPerRow="${category.productPerRow}">

<BreadCrumbs>
<c:forEach items="${breadCrumbs}" var="category">
<Category id="${category.id}">
<Name><c:out value="${category.name}"/></Name>
</Category>
</c:forEach>
</BreadCrumbs>

<SubCategories columns="${category.subcatCols}" levels="${category.subcatLevels}" display="${SUBCAT_DISPLAY}" productPerPage="${category.productPerPage}"
	location="${category.subcatLocation}">
<c:forEach items="${subCategories}" var="category">
<Category id="${category.id}" hasImage="${category.hasImage}">
<Name><c:out value="${category.name}"/></Name>
<SubCategories>
<c:forEach items="${category.subCategories}" var="subCat">
<Category id="${subCat.id}">
<Name><c:out value="${subCat.name}"/></Name>
</Category>
</c:forEach>
</SubCategories>
</Category>
</c:forEach>
</SubCategories>

<Products>
<c:forEach items="${products}" var="product">
<Product id="${product.id}">
<Sku><c:out value="${product.sku}"/></Sku>
</Product>
</c:forEach>
</Products>

</Category>