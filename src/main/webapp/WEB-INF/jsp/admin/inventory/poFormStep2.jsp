<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="/WEB-INF/tlds/spring.tld" prefix="spring" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib uri="http://www.springframework.org/security/tags" prefix="sec" %>

<tiles:insertDefinition name="admin.inventory.po" flush="true">
  <tiles:putAttribute name="content" type="string"> 
<sec:authorize ifAnyGranted="ROLE_AEM,ROLE_ADMIN_${prop_site_id},ROLE_PURCHASE_ORDER_UPDATE,ROLE_PURCHASE_ORDER_CREATE">

<script type="text/javascript" src="../javascript/autocompleter1.2.js"></script>
<script type="text/javascript" src="../javascript/autocompleter.Request1.2.js"></script>
<script type="text/javascript" src="../javascript/autocompleter.Local1.2.js"></script>
<script type="text/javascript" src="../javascript/observer.js"></script>
    
<c:if test="${gSiteConfig['gINVENTORY'] and gSiteConfig['gPURCHASE_ORDER']}">
<link href="<c:url value="/assets/invoice.css"/>" rel="stylesheet" type="text/css"/>
<script language="JavaScript" type="text/JavaScript">
<!--
window.addEvent('domready', function(){			
	var tabs1 = new SimpleTabs($('tab-block-1'), { selector: 'h4'});
	var Tips1 = new Tips($$('.toolTipImg'));
	this["el"] = $('addSku');
	new Autocompleter.Request.HTML(this["el"], '../orders/show-ajax-skus.jhtm', {
	'indicatorClass': 'autocompleter-loading',
	'postData': { 'supplierId': '0' }
	 });
});
function toggleMessage() {
   	if (document.getElementById("purchaseOrderStatus.sendEmail1").checked) {
   	  document.getElementById('messageSelect').style.display="block";
   	} else {
   	  document.getElementById('messageSelect').style.display="none";
   	}
}
function chooseMessage(el) {
   	$('subject').value = $('subject_'+el.value).value;
   	$('message').value = $('message_'+el.value).value;
   	if ($('htmlID'+el.value).value == 'true') {
   		$('purchaseOrder.html1').checked = true;
	} else {
	    $('purchaseOrder.html1').checked = false;
	} 
}
function trimString(str){
	 var returnVal = "";
	 for(var i = 0; i < str.length; i++){
	   if(str.charAt(i) != ' '){
	     returnVal += str.charAt(i)
	   }
	 }
	 return returnVal;
	}
function checkMessage() {
    if (document.getElementById('purchaseOrderStatus.sendEmail1').checked) {
      var message = document.getElementById('message' + document.getElementById('messageSelected').value);
      var subject = document.getElementById('subject' + document.getElementById('messageSelected').value);
      if (trimString(subject.value) == "") {
        alert("Please put a subject");
        subject.focus();
    	return false;
      }
      if (trimString(message.value) == "") {
        alert("Please put a message");
        message.focus();
    	return false;
      }
    }
    return true;
}
function chooseAddress(el, divId) {
	$(divId).value = $(divId+'_'+el.value).value;
}
//-->
</script>
<form:form commandName="purchaseOrderForm" method="post" enctype="multipart/form-data" >
<!-- main box -->
  <div id="mboxfull">
  
  <!-- start table -->
  <table cellpadding="0" cellspacing="0" border="0" class="module" >
  <tr>
    <td class="topl_g">
	  
	  <!-- breadcrumb -->
	  <p class="breadcrumb">
	    <a href="../inventory"><fmt:message key='inventory'/></a> &gt;
	    <a href="../inventory/purchaseOrders.jhtm"><fmt:message key='purchaseOrder'/></a> &gt;
	    <c:out value="${purchaseOrderForm.purchaseOrder.poNumber}" />
	    <c:if test="${purchaseOrderForm.purchaseOrder.dropShip}"> ( <fmt:message key="dropShip" /> )</c:if>
	  </p>
	  
	  <!-- Error Message -->
	  <c:if test="${message != null}">
        <div class="message"><spring:message code="${message}"/></div>
      </c:if>
      <c:if test="${purchaseOrderForm.errorMessage != null}">
        <div class="message"><fmt:message key="${purchaseOrderForm.errorMessage}"/></div>
      </c:if>

  
  </td><td class="topr_g" ></td></tr>
  <tr><td class="boxmidlrg" >

 <!-- tabs -->
 <div id="tab-block-1"> 

    <h4 title="<fmt:message key='purchaseOrder'/>"><fmt:message key="step" /> 2</h4>
	<div>
	<!-- start tab -->

        <c:set var="classIndex" value="0" />
	  	<div class="listdivi ln tabdivi"></div>	

		  	<div class="list${classIndex % 2}"><c:set var="classIndex" value="${classIndex+1}" />
			<table border="0" width="100%" cellspacing="3" cellpadding="1">
			  <tr valign="top">
			    <td>
			      <div class="poAddressTitle"><b><fmt:message key="supplier" />:</b></div>
				  <div class="poAddressSelect">
				    <c:if test="${!empty model.supplierAddressList}">
			    	    <select style="font-size:10px;width:306px;" name="addressSelected" id="addressSelected" onChange="chooseAddress(this, 'purchaseOrder.billingAddress')">
						  <c:forEach items="${model.supplierAddressList}" var="address" varStatus="status">
						    <option value="${address.id}"><c:out value="${address.firstName}" /> <c:out value="${address.lastName}" /> <c:out value="${address.addr1}" /> <c:out value="${address.city}" /> <c:out value="${address.zip}" /> <c:out value="${address.stateProvince}" /> <c:out value="${address.country}" /> <c:if test="${address.primary}"> (Primary Address)</c:if></option>
						  </c:forEach>
						</select>
						<c:forEach items="${model.supplierAddressList}" var="address" varStatus="status">
						  <input type="hidden" id="purchaseOrder.billingAddress_${address.id}" value="${address.stringAddress}" />
						</c:forEach>
					</c:if>
				  </div>
				  <div class="poAddress">
				    <form:textarea rows="8" cols="40" path="purchaseOrder.billingAddress" cssClass="textfield" style="width: 300px;" htmlEscape="true" />
				  </div>
			    </td>
				<td>&nbsp;</td>
				<td>
				  <div class="poAddressTitle"><b><fmt:message key="shipTo" />:</b></div>
				  <div class="poAddressSelect">
				    <c:if test="${!empty model.shipToAddressList}">
			    	    <select style="font-size:10px;width:306px;" name="addressSelected" id="addressSelected" onChange="chooseAddress(this, 'purchaseOrder.shippingAddress')">
						  <c:forEach items="${model.shipToAddressList}" var="address" varStatus="status">
						    <option value="${address.id}"><c:out value="${address.firstName}" /> <c:out value="${address.lastName}" /> <c:out value="${address.addr1}" /> <c:out value="${address.city}" /> <c:out value="${address.zip}" /> <c:out value="${address.stateProvince}" /> <c:out value="${address.country}" /> <c:if test="${address.primary}"> (Primary Address)</c:if></option>
						  </c:forEach>
						</select>
						<c:forEach items="${model.shipToAddressList}" var="address" varStatus="status">
						  <input type="hidden" id="purchaseOrder.shippingAddress_${address.id}" value="${address.stringAddress}" />
						</c:forEach>
					</c:if>
				  </div>
				  <div class="poAddress">
				    <form:textarea rows="8" path="purchaseOrder.shippingAddress" cssClass="textfield" style="width: 300px;" htmlEscape="true" />
				  </div>
				</td>
				<td>&nbsp;</td>
				<td>
					  <%@ include file="/WEB-INF/jsp/admin/inventory/poAdditionalFields.jsp" %>			
				</td>
				<td align="right">
				  <table>
				    <tr>
					  <c:choose>
			            <c:when test="${purchaseOrderForm.purchaseOrder.dropShip}">
			              <td><fmt:message key='dropShipNumber'/></td>
			            </c:when>
			            <c:otherwise>
			              <td><fmt:message key='poNumber'/></td>
			            </c:otherwise>
			          </c:choose>
					  <td>:&nbsp;&nbsp;</td>
					  <td><b><c:out value="${purchaseOrderForm.purchaseOrder.poNumber}" /></b></td>
					</tr>
					<c:if test="${!purchaseOrderForm.newPurchaseOrder}">
					<tr>
					  <td><fmt:message key="date" /></td>
					  <td>:&nbsp;&nbsp;</td>
					  <c:choose>
					    <c:when test="${siteConfig['CHANGE_PO_DATE'].value == 'true'}">
						  <td>
						    <form:input path="purchaseOrder.created" size="18"  />
						    <img id="time_start_trigger" class="calendarImage" src="../graphics/calendarIcon.jpg" alt="Pick a date" />
						    <script type="text/javascript">
							  <!--
								Calendar.setup({
								    inputField     :    "purchaseOrder.created",   // id of the input field
								    showsTime      :    true,
								    ifFormat       :    "%m/%d/%Y[%I:%M %p]",   // format of the input field
								    button         :    "time_start_trigger" // trigger for the calendar (button ID)
								});
							  //-->
							</script>
								<form:errors path="purchaseOrder.created" type ="hidden" cssClass="error">
								<span style=color:#E00000><b>!Wrong date format</b></span>
								<img class="toolTipImg" title="Format is 01/01/2017[01:00 PM]" src="../graphics/question.gif" />  
								</form:errors>
							
					      </td> 
						</c:when>
						<c:otherwise>
						  <td>
						    <fmt:formatDate type="both" timeStyle="full" pattern="MM/dd/yyyy" value="${purchaseOrderForm.purchaseOrder.created}"/>
						  </td>
						</c:otherwise>
					  </c:choose>
					</tr>
					</c:if>
					<tr>
					  <td><span class="bold red"><fmt:message key="po_dueDate" /></span></td>
					  <td>:&nbsp;&nbsp;</td>
						  <td>
						    <form:input path="purchaseOrder.dueDate" size="18"  />
						    <img id="due_date_trigger" class="calendarImage" src="../graphics/calendarIcon.jpg" alt="Pick a date" />
						    <script type="text/javascript">
							  <!--
								Calendar.setup({
								    inputField     :    "purchaseOrder.dueDate",   // id of the input field
								    showsTime      :    true,
								    ifFormat       :    "%m/%d/%Y[%I:%M %p]",   // format of the input field
								    button         :    "due_date_trigger" // trigger for the calendar (button ID)
								});
							  //-->
							</script>
							<form:errors path="purchaseOrder.dueDate" type ="hidden" cssClass="error">
							<span style=color:#E00000><b>!Wrong date format</b></span>
							<img class="toolTipImg" title="Format is 01/01/2017[01:00 PM]" src="../graphics/question.gif" />  
							</form:errors>	
					      </td>
					</tr>
				<c:if test="${siteConfig['ORDER_CUSTOM_FIELD1_NAME'].value != ''}">
					  <tr>
					    <td><c:out value="${siteConfig['ORDER_CUSTOM_FIELD1_NAME'].value}" /></td>
					     <td>:&nbsp;&nbsp;</td>
					    <td>
					      <form:select path="purchaseOrder.docks" value="purchaseOrder.docks">
					      <form:option value="">Please Select</form:option>
					 	  <c:forTokens items="${siteConfig['ORDER_CUSTOM_FIELD1_VALUE'].value}" delims="," var="value">
						  		      <form:option value="${value}">${value}</form:option>
						  		   </c:forTokens>
					     </form:select>
					     <form:errors path="purchaseOrder.docks" cssClass="error" />  
					    </td>
					  </tr>
			    </c:if>
			  <c:if test="${siteConfig['ORDER_CUSTOM_FIELD2_NAME'].value != ''}">
				  <tr>
				    <td><c:out value="${siteConfig['ORDER_CUSTOM_FIELD2_NAME'].value}" /></td>
				     <td>:&nbsp;&nbsp;</td>
				    <td>
				       <form:select path="purchaseOrder.resources" value="purchaseOrder.resources">
				      <form:option value="">Please Select</form:option>
				 	  <c:forTokens items="${siteConfig['ORDER_CUSTOM_FIELD2_VALUE'].value}" delims="," var="value">
					  		      <form:option value="${value}">${value}</form:option>
					  		   </c:forTokens>
				     </form:select>
				     <form:errors path="purchaseOrder.resources" cssClass="error" />
				    </td>
				  </tr>
			  </c:if>
					<tr>
					  <td><fmt:message key="account" /> #</td>
					  <td>:&nbsp;&nbsp;</td>
					  <td><c:out value="${purchaseOrderForm.purchaseOrder.supplier.accountNumber}" /></td>
					</tr>
					<tr>
					  <td><fmt:message key="orderBy" /></td>
					  <td>:&nbsp;&nbsp;</td>
					  <td><c:out value="${purchaseOrderForm.purchaseOrder.orderBy}" /></td>
					</tr>
					<tr>
					  <td><fmt:message key="shipVia" /></td>
					  <td>:&nbsp;&nbsp;</td>
					  <td><c:out value="${purchaseOrderForm.purchaseOrder.shipVia}" /></td>
					</tr>
					<c:if test="${purchaseOrderForm.purchaseOrder.orderId != null}">
					<tr>
					  <td><fmt:message key='invoice'/> #</td>
					  <td>:&nbsp;&nbsp;</td>
					  <td><a href="../orders/invoice.jhtm?order=${purchaseOrderForm.purchaseOrder.orderId}" ><b><c:out value="${purchaseOrderForm.purchaseOrder.orderId}" /></b></a></td>
					</tr>
					</c:if>
					<c:if test="${purchaseOrderForm.purchaseOrder.supplierInvoiceNumber != null}">
					<tr>
					  <td><fmt:message key="supplier" /> <fmt:message key="invoiceN" /></td>
					  <td>:&nbsp;&nbsp;</td>
					  <td><c:out value="${purchaseOrderForm.purchaseOrder.supplierInvoiceNumber}" /></td>
					</tr>
					</c:if>
					<c:if test="${purchaseOrderForm.purchaseOrder.shippingQuote != null}">
					<tr>
					  <td><fmt:message key="shippingQuote" /></td>
					  <td>:&nbsp;&nbsp;</td>
					  <td><c:out value="${purchaseOrderForm.purchaseOrder.shippingQuote}" /></td>
					</tr>
					</c:if>

					<c:if test="${siteConfig['CUSTOM_SHIPPING_CONTACT_INFO'].value == 'true'}">
					<tr>
					  <td><fmt:message key="shippingContactInfo" /></td>
					  <td>:&nbsp;&nbsp;</td>
					  <td><form:select path="purchaseOrder.shipViaContactId">
				            <form:option value="" label="--Please Select--"/>
				            <form:options items="${model.customShippingContact}" itemValue="id" itemLabel="company"/>
				        </form:select>
				       </td>
					</tr>
					</c:if>

					<tr>
					  <td><c:out value="${siteConfig['PO_FLAG1_NAME'].value}" /></td>
					  <td>:&nbsp;&nbsp;</td>
					  <td><form:select path="purchaseOrder.flag1">
				  		   <form:option value=""></form:option>
				  		 <c:forTokens items="${siteConfig['PO_FLAG1_PREVALUE'].value}" delims="," var="type">
				  		   <form:option value="${type}">${type}</form:option>
				  		 </c:forTokens>
				  		</form:select></td>
					</tr>
				
				  </table>
				</td>
			  </tr>
			</table>
			
			<table border="0" cellpadding="2" cellspacing="1" width="100%" class="invoice">
			  <tr>
			    <c:set var="cols" value="0"/>
			    <th width="5%" class="invoice" align="center"><fmt:message key="remove" /></th>
			    <th width="5%" class="invoice" align="center"><fmt:message key="line" /> #</th>
			    <th width="25%" class="invoice" align="center"><fmt:message key="productSku" /></th>
			    <th width="10%" class="invoice" align="center"><fmt:message key="productName" /></th>
			    <th class="invoice" align="center"><fmt:message key="supplierSku" /></th>
			    <th width="5%" class="invoice" align="center"><fmt:message key="quantity" /></th>
			    <th width="5%" class="invoice" align="center"><fmt:message key="productPrice" /></th>
			    <th width="15%" class="invoice" align="center"><fmt:message key="total" /></th>
			  </tr>
			<c:forEach var="lineItem" items="${purchaseOrderForm.purchaseOrder.poLineItems}" varStatus="status">
			<tr valign="top">
			  <td class="invoice" align="center"><input type="checkbox" name="__remove_${lineItem.lineNumber}" <c:if test="${purchaseOrderForm.purchaseOrder.status == 'rece'}">disabled="disabled" </c:if> ></td>
			  <td class="invoice" align="center"><c:out value="${status.count}"/></td>
			  <td class="invoice" align="left"><c:out value="${lineItem.sku}"/></td>
			  <c:choose>
			    <c:when test="${!empty purchaseOrderForm.purchaseOrder.orderId}">
			      <td class="invoice"><c:out value="${lineItem.product.name}"/>
				  <c:forEach items="${lineItem.productAttributes}" var="productAttribute">
				    <div class="invoice_lineitem_attributes">- <c:out value="${productAttribute.optionName}"/>: <c:out value="${productAttribute.valueName}"/> 
				    <c:if test="${(! empty productAttribute.optionPriceOriginal) && productAttribute.optionPriceOriginal != 0 }">
			    		<c:out value="${productAttribute.optionPriceOriginal}"/> <fmt:message key="${productAttribute.optionPriceMessageF}"/>
			    	</c:if>
				    </div>
				  </c:forEach>
			      </td>
			    </c:when>
			    <c:otherwise>
			      <td class="invoice" align="center">
                    <div class="po"><input  style="text-align: left;margin:1px;" type="text" name="__productName_<c:out value='${lineItem.lineNumber}'/>" value="${lineItem.productName}" <c:if test="${purchaseOrderForm.purchaseOrder.status == 'rece'}">disabled="disabled" </c:if> size="25"></div>
                  </td> 
			    </c:otherwise>
			  </c:choose>
			  <td class="invoice" align="left">
			  	<c:out value="${lineItem.supplierSku}"/>
			  	<c:forEach items="${lineItem.product.price}" var="price" varStatus="statusPrice">
			  	  <c:if test="${statusPrice.first}"><br/><br/></c:if>
			  	  <c:if test="${not statusPrice.first}">, </c:if>
			  	  <c:out value="${price.qtyFrom}"/><c:choose><c:when test="${price.qtyTo == null}">+</c:when><c:otherwise>-<c:out value="${price.qtyTo}"/></c:otherwise></c:choose> = <fmt:message key="${siteConfig['CURRENCY'].value}" /><c:out value="${price.cost}"/>
			  	</c:forEach>
			  </td>
			  <td class="invoice" align="center">
			  <c:choose>
			      <c:when test="${purchaseOrderForm.purchaseOrder.dropShip}">
			        <div>
			          <select <c:if test="${purchaseOrderForm.purchaseOrder.status == 'rece'}">disabled="disabled" </c:if> name="__quantity_<c:out value='${lineItem.lineNumber}'/>" style="width:65px">
					      <c:forEach begin="0" end="${lineItem.qty}" var="qty">
					        <option value="<c:out value='${qty}'/>" <c:if test="${qty == lineItem.qty}">selected</c:if>><c:out value="${qty}"/></option>
					      </c:forEach>
					    </select>
			        </div>
			      </c:when>
			      <c:otherwise><div class="po"><input style="text-align: right;margin:1px;" type="text" name="__quantity_<c:out value='${lineItem.lineNumber}'/>" value="${lineItem.qty}" <c:if test="${purchaseOrderForm.purchaseOrder.status == 'rece'}">disabled="disabled" </c:if> size="5"></div></c:otherwise>
			    </c:choose>
			  </td>
			  <td class="invoice" align="center">
			    <div class="po"><input  style="text-align: right;margin:1px;" type="text" name="__unitPrice_<c:out value='${lineItem.lineNumber}'/>" value="<fmt:formatNumber value='${lineItem.cost}' pattern='#,##0.00'/>" <c:if test="${purchaseOrderForm.purchaseOrder.status == 'rece'}">disabled="disabled" </c:if> size="8"></div>
			  </td>
			  <td class="invoice" align="right">
			    <div class="po"><input  style="text-align: right;margin:1px;" type="text" name="__totalCost_<c:out value='${lineItem.lineNumber}'/>" value="<fmt:formatNumber value='${lineItem.totalCost}' pattern='#,##0.00'/>" <c:if test="${purchaseOrderForm.purchaseOrder.status == 'rece'}">disabled="disabled" </c:if> size="25"></div>
			  </td>
			</tr>
			</c:forEach>  
			<c:forEach var="lineItem" items="${purchaseOrderForm.purchaseOrder.poInsertedLineItems}" varStatus="status">
			<tr valign="top">
			  <td class="invoice" align="center"><input type="checkbox" name="__remove_new_${status.index}" <c:if test="${purchaseOrderForm.purchaseOrder.status == 'rece'}">disabled="disabled" </c:if> ></td>
			  <td class="invoice" align="center"><c:out value="${purchaseOrderForm.purchaseOrder.size + status.count}"/></td>
			  <td class="invoice" align="left"><c:out value="${lineItem.sku}"/></td>
			  <c:choose>
			    <c:when test="${!empty purchaseOrderForm.purchaseOrder.orderId}">
			      <td class="invoice" align="left"><c:out value="${lineItem.product.name}"/></td>
			    </c:when>
			    <c:otherwise>
			      <td class="invoice" align="left">
			        <div class="po"><input style="text-align: left;margin:1px;" type="text" name="__productName_new_<c:out value='${status.index}'/>" value="${lineItem.productName}" <c:if test="${purchaseOrderForm.purchaseOrder.status == 'rece'}">disabled="disabled" </c:if> size="25"></div>
			      </td>
			    </c:otherwise>
			  </c:choose>
			  <td class="invoice" align="left"><c:out value="${lineItem.supplierSku}"/></td>
			  <td class="invoice" align="left">
			    <div class="po"><input style="text-align: right;margin:1px;" type="text" name="__quantity_new_<c:out value='${status.index}'/>" value="${lineItem.qty}" <c:if test="${purchaseOrderForm.purchaseOrder.status == 'rece'}">disabled="disabled" </c:if> size="5"></div>
			  </td>
			  <td class="invoice" align="center">
			    <div class="po"><input  style="text-align: right;margin:1px;" type="text" name="__unitPrice_new_<c:out value='${status.index}'/>" value="${lineItem.cost}" <c:if test="${purchaseOrderForm.purchaseOrder.status == 'rece'}">disabled="disabled" </c:if> size="8"></div>
			  </td>
			  <td class="invoice" align="right">
			    <div class="po"><input  style="text-align: right;margin:1px;" type="text" name="__totalCost_new_<c:out value='${status.index}'/>" value="${lineItem.totalCost}" <c:if test="${purchaseOrderForm.purchaseOrder.status == 'rece'}">disabled="disabled" </c:if> size="25"></div>
			  </td>
			</tr>
			</c:forEach>
			<c:if test="${purchaseOrderForm.purchaseOrder.status != 'rece' and purchaseOrderForm.purchaseOrder.orderId == null}">
			<tr valign="top">
			  <td class="invoice" align="center"></td>
			  <td class="invoice" align="center"></td>
			  <td class="invoice" align="left">
			    <div class="po"><select name="__sku">
			      <option value=""></option>
			      <c:forEach items="${model.skus}" var="sku">
			       <option value="${sku}"><c:out value="${sku}" /></option>
			      </c:forEach>
		        </select></div>
		        <div class="button"><c:if test="${purchaseOrderForm.purchaseOrder.status != 'rece'}" ><input type="submit" name="_target1" value="<fmt:message key="addSku"/>"></c:if></div>
			  </td>
			  <td class="invoice" align="left"></td>
			  <td class="invoice" align="left"></td>
			  <td class="invoice" align="center"></td>
			  <td class="invoice" align="right"></td>
			  <td class="invoice" align="right"></td>
			</tr>
			</c:if>
			<tr bgcolor="#BBBBBB">
			  <td colspan="${8+cols}">&nbsp;</td>    
			</tr>
			<tr>
			  <td class="invoice" colspan="${7+cols}" align="right"></td>
			  <td class="invoice" align="right">&nbsp;</td>
			</tr>
			<tr>
			  <td class="invoice" colspan="${7+cols}" align="right"><fmt:message key="subTotal" />:</td>
			  <td align="right" bgcolor="#F8FF27"><fmt:message key="${siteConfig['CURRENCY'].value}" /><fmt:formatNumber value="${purchaseOrderForm.purchaseOrder.runningSubTotal}" pattern="#,##0.00" /></td>
			</tr>
			</table>
			</br>
			<c:if test="${! purchaseOrderForm.purchaseOrder.dropShip}">
			<div class="purchaseorder_add_non_supplier_sku">
				<fmt:message key="add" /> <fmt:message key="productSku" /></b>: <input type="text" style="width:250px;" id="addSku" name="__nonSupplierSKU" value="" >
				<div class="button"><c:if test="${purchaseOrderForm.purchaseOrder.status != 'rece'}" ><input type="submit" name="_target1" value="<fmt:message key="addSku"/>"></c:if></div>
			</div>
			</c:if>
		  	<div class="poPt">
		  	<!-- input field -->
		  		PT: <form:input path="purchaseOrder.pt" cssClass="textfield" style="width:250px;" htmlEscape="true"/>
				<form:errors path="purchaseOrder.pt" cssClass="error"/>
		    <!-- end input field -->
		  	</div>
			<c:if test="${siteConfig['PO_SPECIAL_INSTRUCTIONS'].value != ''}">
 			<table class="specialInst">
				<tr >
				  <td valign="top"><br /> 
				    <b><c:out value="${siteConfig['PO_SPECIAL_INSTRUCTIONS'].value}"/>:</b>  	
				  </td>
				</tr>
				<tr>
				  <td>
				  <form:textarea id="specialInstruction" path="purchaseOrder.specialInstruction" rows="5" cols="160"/>
				  <div class="helpNote">
					<p>Contents of this note will be in purchase order print and .pdf</p>
				  </div>
				  </td>
				</tr>
			</table>
			</c:if>
			<table width="100%">
			<tr>
			  <td align="right" class="button"> 
			  <c:choose>
			  <c:when test="${purchaseOrderForm.purchaseOrder.status != 'rece'}">
			  <c:if test="${purchaseOrderForm.purchaseOrder.status != 'x'}">
			    <input type="submit" name="_update" value="<fmt:message key="update"/>">
			    <input type="submit" name="_finish" value="<fmt:message key="saveChanges"/>">
			  </c:if>
			    <input type="submit" value="<fmt:message key="cancel"/>" name="_cancel">
			  </c:when>
			  <c:otherwise>
			    <input type="submit" value="<fmt:message key="back"/>" name="_cancel">
			  </c:otherwise>
			  </c:choose>
			</td>
			</tr>
			</table>
			
			<c:if test="${!purchaseOrderForm.newPurchaseOrder}" >
			<br />
			<hr />
			<br />
			<table border="0" cellpadding="2" cellspacing="1" width="100%" class="statusHistory">
			<tr>
			  <th class="statusHistory"><fmt:message key="date"/></th>
			  <th class="statusHistory"><fmt:message key="status"/></th>
			  <th class="statusHistory"><fmt:message key="notified"/></th>
			  <th class="statusHistory"><fmt:message key="updatedBy"/></th>
			  <th style="width:440px;" class="statusHistory"><fmt:message key="comments"/></th>
			  <th style="width:50px;" align="center" class="statusHistory"><fmt:message key="action"/></th>
			</tr>
			<c:forEach var="orderStatus" items="${model.poStatusHistory}" varStatus="HistoryStatus">
			<tr valign="top">
			  <td class="statusHistory"><fmt:formatDate type="both" timeStyle="full" value="${orderStatus.dateChanged}"/></td>
			  <td class="status" align="center"><fmt:message key="po_${orderStatus.status}"/></td>
			  <td class="status" align="center"><c:choose><c:when test="${orderStatus.sendEmail}"><img src="../graphics/checkbox.png"></c:when><c:otherwise><img src="../graphics/box.png"></c:otherwise></c:choose></td>
			  <td class="statusHistory"><c:out value="${orderStatus.updatedBy}"/></td>
			  <td class="statusHistory"><c:out value="${orderStatus.comments}"/></td>
			  <td class="statusHistory"></td>
			</tr>
			<c:if test="${ orderStatus.status == 'rece' or orderStatus.status == 'x'}">
			  <c:set var="editPO" value="true"/>
			</c:if>
			</c:forEach>
			<c:if test="${editPO != true}">
			<tr>
			  <td class="statusHistory"><fmt:formatDate type="both" timeStyle="full" pattern="MM/dd/yyyy" value="${orderStatus.dateChanged}"/></td>
			  <td class="statusHistory"><div class="po"><form:select path="purchaseOrderStatus.status">
			        <form:option value="pend"><fmt:message key="pending" /></form:option>
			  		<form:option value="send"><fmt:message key="po_send" /></form:option>
			  		<form:option value="send01"><fmt:message key="po_send01" /></form:option>
			  		<form:option value="send05"><fmt:message key="po_send05" /></form:option>
			  		<form:option value="send10"><fmt:message key="po_send10" /></form:option>
			        <form:option value="rece"><fmt:message key="po_rece" /></form:option>
			        <form:option value="x"><fmt:message key="po_x" /></form:option>
			       </form:select></div>
			  </td>
			  <td class="statusHistory"></td>
			  <td class="statusHistory">
			    <c:choose>
		  	      <c:when test="${model.accessUser == null}">
		  	        <form:select path="purchaseOrderStatus.updatedBy">
		              <option value="<fmt:message key="admin" />" ><fmt:message key="admin" /></option>
		              <form:options items="${model.userList}" itemValue="username" itemLabel="username"/>
		            </form:select>
		  	      </c:when>
		  	      <c:otherwise><c:out value="${purchaseOrderForm.purchaseOrderStatus.updatedBy}" /></c:otherwise>
		  	    </c:choose>
		  	  </td>
			  <td class="po"><textarea name="__status_comment"></textarea></td>
			  <td class="statusHistory"><div class="button"><input type="submit" name="__update_status" value="<fmt:message key="updateStatus"/>" onclick="return checkMessage()"></div></td>
			</tr>  
			</c:if>
			</table>
 			</c:if>
 			
 			<c:if test="${!purchaseOrderForm.newPurchaseOrder and purchaseOrderForm.purchaseOrder.status != 'rece'}">
 			Send Email: <form:checkbox path="purchaseOrderStatus.sendEmail" onclick="toggleMessage()"/>
 			<div id="messageSelect" style="display:none;">
			<table border="0" cellpadding="2" cellspacing="1" width="100%" class="sendEmailBox">
			<tr>
		      <td><fmt:message key="from"/>:</td>
		      <td>
		        <form:input id="from" path="purchaseOrder.from" size="50" maxlength="50" />
		      </td>
		    </tr>
			<tr>
		      <td><fmt:message key="to"/>:</td>
		      <td>
		        <form:input id="to" path="purchaseOrder.to" size="50" maxlength="50" disabled="true"/>
		      </td>
		    </tr>
		    <tr>
		      <td><fmt:message key="cc"/>:</td>
		      <td>
		        <form:input id="cc" path="purchaseOrder.cc" size="50" maxlength="50"/>
		      </td>
		    </tr>
		    <%-- <tr>
		      <td><fmt:message key="bcc"/>:</td>
		      <td>
		        <form:input id="bcc" path="purchaseOrder.bcc" size="50" maxlength="50"/>
		      </td>
		    </tr> --%>
		    <tr>
		      <td><fmt:message key="subject"/>:</td>
		      <td>
		        <form:input id="subject" path="purchaseOrder.subject" size="50" maxlength="50"/>
		  		<c:forEach var="message" items="${model.messages}">
		    	<input type="text" style="display:none;" id="subject${message.messageId}" name="subject" value="<c:out value="${message.subject}"/>" size="50" maxlength="50" />
		  		</c:forEach>
		      </td>
		    </tr>
		    <tr>
		      <td>Html:</td>
		      <td>
		        <form:checkbox path="purchaseOrder.html"/>
		  		<c:forEach var="message" items="${model.messages}">
		    	<input type="hidden" id="htmlID${message.messageId}" value="<c:out value="${message.html}"/>" />
		  		</c:forEach>
		      </td>
		    </tr>
		    <tr>
		      <td colspan="2">
		        <form:textarea id="message" path="purchaseOrder.message" rows="20" cols="80"/>
		  		<c:forEach var="message" items="${messages}">
		    	<textarea style="display:none;" id="message${message.messageId}" name="message" rows="5" cols="80"><c:out value="${message.message}"/></textarea>
		  		</c:forEach>
		  		<div id="messageSelect">
			      <select id="messageSelected" onChange="chooseMessage(this)">
			        <option value="">choose a message</option>
				  <c:forEach var="message" items="${model.messages}">
				    <option value="${message.messageId}"><c:out value="${message.messageName}"/></option>
				  </c:forEach>
				  </select>
			   </div>
			   <div class="po_attachment">
			   	<span style="width:100px;text-align:right"><fmt:message key="attachment" />:</span>
          	  	<input value="browse" type="file" name="attachment"/>
			   </div>
		      </td>
      	  </tr>	    
			</table>
			</div>
			<c:forEach items="${model.messages}" var="message"	varStatus="status">
		  	  <input type="hidden" id="subject_${message.messageId}" value="<c:out value='${message.subject}'/>" />
		  	  <input type="hidden" id="message_${message.messageId}" value="<c:out value='${message.message}'/>" />
		    </c:forEach>
		    <input type="hidden" id="subject_" value="" />
		  	<input type="hidden" id="message_" value="" />
		  	</c:if>
			</div> 	
	  	
	<!-- end tab -->        
	</div> 	
 
<!-- end tabs -->			
</div>

<!-- start button -->
<div align="left" class="button"> 
	<input type="submit" value="<fmt:message key="prevStep" />" name="_target0">
</div>
<!-- end button -->	     
  
  <!-- end table -->
    </td><td class="boxmidr" ></td></tr>
  <tr><td class="botl"></td><td class="botr"></td></tr>
  </table>
  
<!-- end main box -->  
</div>
</form:form>
</c:if>
</sec:authorize>

  </tiles:putAttribute>    
</tiles:insertDefinition>