<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="/WEB-INF/tlds/spring.tld" prefix="spring" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib uri="http://www.springframework.org/security/tags" prefix="sec" %>


<tiles:insertDefinition name="admin.inventory.po" flush="true">
  <tiles:putAttribute name="content" type="string"> 
  
<script type="text/javascript" src="../../admin/javascript/autocompleter1.2.js"></script>
<script type="text/javascript" src="../../admin/javascript/autocompleter.Request1.2.js"></script>
<script type="text/javascript" src="../../admin/javascript/autocompleter.Local1.2.js"></script>
<script type="text/javascript" src="../../admin/javascript/observer.js"></script>

<sec:authorize ifAnyGranted="ROLE_AEM,ROLE_ADMIN_${prop_site_id},ROLE_PURCHASE_ORDER_UPDATE,ROLE_PURCHASE_ORDER_CREATE">     
<c:if test="${gSiteConfig['gINVENTORY'] and gSiteConfig['gPURCHASE_ORDER']}">
<script language="JavaScript" type="text/JavaScript">
<!--
window.addEvent('domready', function(){			
			var tabs1 = new SimpleTabs($('tab-block-1'), { selector: 'h4'});
			var Tips1 = new Tips($$('.toolTipImg'));
			var supplier_company = $('supplier_company');	//Ajax Autocompleter 	
			var sDropDown = $('purchaseOrder.supplierId');
			
			new Autocompleter.Request.HTML(supplier_company, '../supplier/show-ajax-suppliers.jhtm', {
				'indicatorClass': 'autocompleter-loading', 'onSelection': function(element, selected, value, input) {
					for ( var i = 0; i < sDropDown.options.length; i++ ) {
				       if ( sDropDown.options[i].text.toLowerCase() == value.toLowerCase() ) {
				       	sDropDown.options[i].selected = true;
				           return;
				     }
				  }
				}
			});
		});
//-->
</script>
<form:form commandName="purchaseOrderForm" method="post" >
<!-- main box -->
  <div id="mboxfull">
  
  <!-- start table -->
  <table cellpadding="0" cellspacing="0" border="0" class="module" >
  <tr>
    <td class="topl_g">
	  
	  <!-- breadcrumb -->
	  <p class="breadcrumb">
	    <a href="../inventory"><fmt:message key="inventory" /></a> &gt;
	    <a href="../inventory/purchaseOrders.jhtm"><fmt:message key="purchaseOrders" /></a> 
	    <c:if test="${purchaseOrderForm.purchaseOrder.dropShip}"> ( <fmt:message key="dropShip" /> )</c:if>
	  </p>
	  
	  <!-- Error Message -->
	  <c:if test="${message != null}">
        <div class="message"><spring:message code="${message}"/></div>
      </c:if>

  
  </td><td class="topr_g" ></td></tr>
  <tr><td class="boxmidlrg" >

 <!-- tabs -->
 <div id="tab-block-1">
	<h4 title="<fmt:message key='purchaseOrder'/>"><fmt:message key="step" /> 1</h4>
	<div>
	<!-- start tab -->

        <c:set var="classIndex" value="0" />
        
	  	<div class="listdivi ln tabdivi"></div>	

		  	<c:if test="${purchaseOrderForm.purchaseOrder.dropShip}">
		  	<div class="list${classIndex % 2}"><c:set var="classIndex" value="${classIndex+1}" />
		  	<div class="listfl"><div class="requiredField"><fmt:message key="invoice" /> #</div></div>
		  	<div class="listp">
		  	<!-- input field -->
		  	    <c:out value="${purchaseOrderForm.purchaseOrder.orderId}" />
		    <!-- end input field -->
		  	</div>
		  	</div>
		  	</c:if>
		  	
		  	<div class="list${classIndex % 2}"><c:set var="classIndex" value="${classIndex+1}" />
		  	<div class="listfl"><div class="requiredField"><fmt:message key="supplier" />:</div></div>
		  	<div class="listp">
		  	<!-- input field -->
		  	    <c:choose>
		  	    <c:when test="${not purchaseOrderForm.newPurchaseOrder or purchaseOrderForm.purchaseOrder.supplierId != null}" >
		  	    <form:select path="purchaseOrder.supplierId" disabled='true'>
		            <form:option value="-" label="--Please Select--"/>
		            <form:options items="${model.supplierlist}" itemValue="id" itemLabel="address.company"/>
		        </form:select>
		        </c:when>
		        <c:otherwise>
		        <form:select path="purchaseOrder.supplierId">
		            <form:option value="-" label="--Please Select--"/>
		            <form:options items="${model.supplierlist}" itemValue="id" itemLabel="address.company"/>
		        </form:select>
		         <input  id="supplier_company" name="" type="text" class="textfield" value="" /> 	
		        </c:otherwise>
		        </c:choose>
		        <form:errors path="purchaseOrder.supplierId" cssClass="error"/>
		    <!-- end input field -->
		  	</div>
		  	</div>
		  	
		  	<div class="list${classIndex % 2}"><c:set var="classIndex" value="${classIndex+1}" />
		  	<div class="listfl"><div class="requiredField"><fmt:message key="poNumber" />:</div></div>
		  	<div class="listp">
		  	<!-- input field -->
		  		<form:input path="purchaseOrder.poNumber" cssClass="textfield" maxlength="100" size="60" htmlEscape="true"/>
				<form:errors path="purchaseOrder.poNumber" cssClass="error"/>
		    <!-- end input field -->
		  	</div>
		  	</div>
		  	
		  	<div class="list${classIndex % 2}"><c:set var="classIndex" value="${classIndex+1}" />
		  	<div class="listfl"><fmt:message key="orderBy" />:</div>
		  	<div class="listp">
		  	<!-- input field -->
		  	  <c:choose>
		  	    <c:when test="${!purchaseOrderForm.newPurchaseOrder}" >
		  	      <c:out value="${purchaseOrderForm.purchaseOrder.orderBy}" />
		  	    </c:when>
		  	    <c:otherwise>
		  	      <c:choose>
		  	      <c:when test="${model.accessUser == null}">
		  	        <form:select path="purchaseOrder.orderBy">
		              <option value="<fmt:message key="admin" />" ><fmt:message key="admin" /></option>
		              <form:options items="${model.userList}" itemValue="username" itemLabel="username"/>
		            </form:select>
		  	      </c:when>
		  	      <c:otherwise><c:out value="${purchaseOrderForm.purchaseOrder.orderBy}" /></c:otherwise>
		  	      </c:choose>
		  	    </c:otherwise>
		  	  </c:choose>
		      <form:errors path="purchaseOrder.orderBy" cssClass="error"/>
		    <!-- end input field -->
		  	</div>
		  	</div>
		  	
		  	<div class="list${classIndex % 2}"><c:set var="classIndex" value="${classIndex+1}" />
		  	<div class="listfl"><fmt:message key="shipVia" />:</div>
		  	<div class="listp">
		  	<!-- input field -->
		  	    <form:select path="purchaseOrder.shipVia">
		            <form:option value="" label="--Please Select--"/>
		            <form:options items="${model.customShippingRateList}" itemValue="title" itemLabel="title"/>
		        </form:select>
		        <form:errors path="purchaseOrder.shipVia" cssClass="error"/>
		    <!-- end input field -->
		  	</div>
		  	</div>
		  	
		  	<c:if test="${siteConfig['CUSTOM_SHIPPING_CONTACT_INFO'].value == 'true'}">
		  	<div class="list${classIndex % 2}"><c:set var="classIndex" value="${classIndex+1}" />
		  	<div class="listfl"><fmt:message key="shippingContactInfo" />:</div>
		  	<div class="listp">
		  	<!-- input field -->
		  	    <form:select path="purchaseOrder.shipViaContactId">
		            <form:option value="" label="--Please Select--"/>
		            <form:options items="${model.customShippingContact}" itemValue="id" itemLabel="company"/>
		        </form:select>
		        <form:errors path="purchaseOrder.shipViaContactId" cssClass="error"/>
		    <!-- end input field -->
		  	</div>
		  	</div>
		  	</c:if>
		  	
		  	<div class="list${classIndex % 2}"><c:set var="classIndex" value="${classIndex+1}" />
		  	<div class="listfl"><fmt:message key="supplier" /> <fmt:message key="invoiceN" />:</div>
		  	<div class="listp">
		  	<!-- input field -->
		  		<form:input path="purchaseOrder.supplierInvoiceNumber" cssClass="textfield" maxlength="100" size="60" htmlEscape="true"/>
				<form:errors path="purchaseOrder.supplierInvoiceNumber" cssClass="error"/>
		    <!-- end input field -->
		  	</div>
		  	</div>
		  	
		  	<div class="list${classIndex % 2}"><c:set var="classIndex" value="${classIndex+1}" />
		  	<div class="listfl"><fmt:message key="shippingQuote" />:</div>
		  	<div class="listp">
		  	<!-- input field -->
		  		<form:input path="purchaseOrder.shippingQuote" cssClass="textfield" maxlength="100" size="60" htmlEscape="true"/>
				<form:errors path="purchaseOrder.shippingQuote" cssClass="error"/>
		    <!-- end input field -->
		  	</div>
		  	</div>

		  	<div class="list${classIndex % 2}"><c:set var="classIndex" value="${classIndex+1}" />
		  	<div class="listfl"><c:out value="${siteConfig['PO_FLAG1_NAME'].value}" />:</div>
		  	<div class="listp">
		  	<!-- input field -->
		  		<form:select path="purchaseOrder.flag1" cssClass="textfield">
		  		   <form:option value=""></form:option>
		  		 <c:forTokens items="${siteConfig['PO_FLAG1_PREVALUE'].value}" delims="," var="type">
		  		   <form:option value="${type}">${type}</form:option>
		  		 </c:forTokens>
		  		</form:select>
				<form:errors path="purchaseOrder.flag1" cssClass="error"/>
		    <!-- end input field -->
		  	</div>
		  	</div>
		  	
		  	<div class="list${classIndex % 2}"><c:set var="classIndex" value="${classIndex+1}" />
		  	<div class="listfl"><fmt:message key="note" />:</div>
		  	<div class="listp">
		  	<!-- input field -->
	   			<form:textarea path="purchaseOrder.note" rows="15" cols="60" cssClass="textfield" htmlEscape="true"/>
			    <form:errors path="purchaseOrder.note" cssClass="error" />
		  	<!-- end input field -->	
		  	<div class="helpNote">
				<p>Contents of this note will NOT be in purchase order print or .pdf</p>
			</div>
		  	</div>
		  	</div>
	  	
	<!-- end tab -->        
	</div> 
	
 
<!-- end tabs -->			
</div>

<!-- start button -->
<div align="left" class="button"> 
	<input type="submit" value="<fmt:message key="nextStep" />" name="_target1">
</div>
<!-- end button -->	     
  
  <!-- end table -->
    </td><td class="boxmidr" ></td></tr>
  <tr><td class="botl"></td><td class="botr"></td></tr>
  </table>
  
<!-- end main box -->  
</div>
</form:form>
</c:if>
</sec:authorize>

  </tiles:putAttribute>    
</tiles:insertDefinition>