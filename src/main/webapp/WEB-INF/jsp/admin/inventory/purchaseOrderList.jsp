<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/security/tags" prefix="sec" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ taglib uri="/WEB-INF/tlds/custom.tld" prefix="wj" %>

<tiles:insertDefinition name="admin.inventory.po" flush="true">
  <tiles:putAttribute name="content" type="string">
  
<sec:authorize ifAnyGranted="ROLE_AEM,ROLE_ADMIN_${prop_site_id},ROLE_PURCHASE_ORDER_CREATE,ROLE_PURCHASE_ORDER_VIEW_LIST">  
<c:if test="${model.purchaseOrders.nrOfElements > 0}">
<script language="JavaScript">
<!--
window.addEvent('domready', function(){
	var box1 = new multiBox('mbOrder', {descClassName: 'descClassName',showNumbers: false,overlay: new overlay()});
	// Create the accordian
	var myAccordion = new Fx.Accordion($$('div.arrow_drop'), $$('div.information'), {
		display: 1, alwaysHide: true,
    	onActive: function() {$('information').removeClass('displayNone');}
	});
	<sec:authorize ifAnyGranted="ROLE_AEM,ROLE_ADMIN_${prop_site_id},ROLE_QUICK_EXPORT">
	$('quickModeTrigger').addEvent('click',function() {
	    $('mboxfull').setStyle('margin', '0');
	    $$('.quickMode').each(function(el){
	    	el.hide();
	    });
	    $$('.quickModeRemove').each(function(el){
	    	el.destroy();
	    });
	    $$('.totals').each(function(el){
	    	el.destroy();
	    });
	    $$('.listingsHdr3').each(function(el){
	    	el.removeProperties('colspan', 'class');
	    });
	    alert('Select the whole page, copy and paste to your excel file.');	   
	  });
	 </sec:authorize> 
});
function deleteSelected() {
	var ids = document.getElementsByName("__selected_id");	
  	if (ids.length == 1) {
      if (document.list_form.__selected_id.checked) {
    	return confirm('Delete permanently?');
      }
    } else {
      for (i = 0; i < ids.length; i++) {
        if (document.list_form.__selected_id[i].checked) {
    	  return confirm('Delete permanently?');
        }
      }
    }

    alert("Please select DropShip(s) to delete.");       
    return false;
}
function toggleAll(el) {
	var ids = document.getElementsByName("__selected_id");	
  	if (ids.length == 1)
      document.list_form.__selected_id.checked = el.checked;
    else
      for (i = 0; i < ids.length; i++)
        document.list_form.__selected_id[i].checked = el.checked;	
}

function optionsSelected() {
	var ids = document.getElementsByName("__selected_id");	
  	if (ids.length == 1) {
      if (document.list_form.__selected_id.checked) {
    	return true;
      }
    } else {
      for (i = 0; i < ids.length; i++) {
        if (document.list_form.__selected_id[i].checked) {
    	  return true;
        }
      }
    }
    alert("Please select product(s).");       
    return false;
}
function optionsSelectedStatus() {
	var submit = false;
	var poIds = "";
	var ids = document.getElementsByName("__selected_id");
  	if (ids.length == 1) {
      if (document.list_form.__selected_id.checked) {
    	  poIds+=document.list_form.__selected_id.value;
    	  poIds+=",";
    	  submit = true;
      }
    } else {
      for (i = 0; i < ids.length; i++) {
        if (document.list_form.__selected_id[i].checked) {
      	  	poIds+=document.list_form.__selected_id[i].value;poIds+=",";
        	submit = true;
        }
      }
    }
  	if(submit){
  		var status = document.getElementById("__po_status").value;
  		var request = new Request({
  			url: "../orders/purchase_order_status_update_ajax.jhtm?status="+status+"&poIds="+poIds,
  			method: 'post',
  			onComplete: function(response) { 		
  				var ids = document.getElementsByName("__selected_id");
  			  	if (ids.length == 1) {
  			      if (document.list_form.__selected_id.checked) {
  			    	document.list_form.__selected_id.checked=false;
  			      }
  			    } else {
  			      for (i = 0; i < ids.length; i++) {
  			        if (document.list_form.__selected_id[i].checked) {
  			        	document.list_form.__selected_id[i].checked=false;	
  			        }
  			      }
  			    }
  			    window.location=window.location;
  			}
  		}).send();
  	}else{
  	    alert("Please select product(s).");       
  	    return false;
  	}
}
function deleteSelected() {
	var ids = document.getElementsByName("__selected_id");	
  	
    for (i = 0; i < ids.length; i++) {
    	document.list_form.__selected_id[i].checked = false;
    }
}

function batchCustomFieldUpdate() {
	var ids = document.getElementsByName("__selected_id");	
  	if (ids.length == 1) {
      if (document.list_form.__selected_id.checked) {
    	return confirm('Batch Scheduled Update?');
      }
    } else {
      for (i = 0; i < ids.length; i++) {
        if (document.list_form.__selected_id[i].checked) {
    	  return confirm('Batch Scheduled Update?');
        }
      }
    }


    alert("Please select orders(s) to batch.");       
    return false;
}
//-->

//
</script>
</c:if>
<c:if test="${gSiteConfig['gINVENTORY'] and gSiteConfig['gPURCHASE_ORDER']}">
<form id="list" action="purchaseOrders.jhtm" method="post" name="list_form">
<input type="hidden" id="sort" name="sort" value="${purchaseOrderSearch.sort}" />
<!-- main box -->
  <div id="mboxfull">
  
  <!-- start table -->
  <table cellpadding="0" cellspacing="0" border="0" class="module" >
  <tr  class="quickMode">
    <td class="topl_g">
	  
	  <!-- breadcrumb -->
	  <p class="breadcrumb">
	    <a href="../inventory"><fmt:message key="inventory" /></a> &gt;
	    <fmt:message key="purchaseOrders" />
	  </p>
	  
	  <!-- Error -->
	  <c:if test="${!empty model.message}">
		<div class="message"><fmt:message key="${model.message}" /></div>
	  </c:if>
	  
	  <!-- header image -->
	  <img class="headerImage" id="quickModeTrigger" src="../graphics/purchaseOrder.gif" /> 

	<!-- Options -->  
	<c:if test="${model.purchaseOrders.nrOfElements > 0}">
		<div class="optionBox"><div class="titlePrivilege"><fmt:message key="options" /></div></div>
		<div class="arrow_drop arrowImageRight"></div>
		<div style="clear: both;"></div>
		<div class="information displayNone" id="information" style="float:left;">
			<div style="float:left;padding: 5px">
	           <table class="productBatchTool" cellpadding="5" cellspacing="5">
	           <tr>
	            <td valign="top">
	             <table cellpadding="1" cellspacing="1">
	              <tr style="height:30px;white-space: nowrap">
	               <td valign="top" width="20px"><img class="toolTipImg" title="PT::The value will orverride PO's pt value" src="../graphics/question.gif" /></td>
	               <td valign="top"><h3>PT</h3></td>
	              </tr>
	             </table>
	            </td>
	           </tr>
	           <tr>
	            <td valign="top">
	             <table cellpadding="1" cellspacing="1">
	              <tr style="">
			       <td>
			        <input name="__po_pt" type="text" size="15"/>
			        <div align="left" class="buttonLeft">
		            <input type="submit" name="__poPtBacher" value="Update" onClick="return optionsSelected()">
		            </div>
			       </td>
			     </tr>
	             </table>
	            </td>
	           </tr>
	           </table>	
	          </div> 
	          
	          <div style="float:left;padding: 5px">
	          <table class="productBatchTool" cellpadding="5" cellspacing="5">
	           <tr>
	            <td valign="top">
	             <table cellpadding="1" cellspacing="1">
	              <tr style="height:30px;white-space: nowrap">
	               <td valign="top" width="20px"><img class="toolTipImg" title="Status::The value will orverride PO's status value" src="../graphics/question.gif" /></td>
	               <td valign="top"><h3>Status</h3></td>
	              </tr>
	             </table>
	            </td>
	           </tr>
	           <tr>
	            <td valign="top">
	             <table cellpadding="1" cellspacing="1">
	              <tr style="">
			       <td>
			        <div align="left" class="buttonLeft">
			       	 <select name="__po_status" id="__po_status" style="width: 180px">
		    		 	<option value="pend">Pending</option>
		    		 	<option value="send">Released</option>
		    		 	<option value="send01">Scheduled</option>
		    		 	<option value="send05">Picked-Up</option>
		    		 	<option value="send10">Arrived</option>
		    		 	<option value="send15">Arrived-Yard</option> 		 	
		    		 	<option value="rece">Staged</option>
		    		 	<option value="x">Cancelled</option>
		    		 </select>	 
		    		 <input type="button" name="__statusPtBacher" value="Update" onClick="return optionsSelectedStatus()">
		            </div>
			       </td>
			     </tr>
	             </table>
	            </td>
	           </tr>
	           </table>		 
			</div>
			
			<!-- Due Date -->
			<div style="float:left;padding: 5px">
	          <table class="productBatchTool" cellpadding="5" cellspacing="5">
	           <tr>
	            <td valign="top">
	             <table cellpadding="1" cellspacing="1">
	              <tr style="height:30px;white-space: nowrap">
	               <td valign="top" width="20px"><img class="toolTipImg" title="Status::The value will orverride PO's Due Date" src="../graphics/question.gif" /></td>
	               <td valign="top"><h3><fmt:message key="dueDate" /></h3></td>
	              </tr>
	             </table>
	            </td>
	           </tr>
	           <tr>
	            <td valign="top">
	             <table cellpadding="1" cellspacing="1">
	              <tr style="">
			       <td>
			       
			      <input id="PO_dueDate" name="PO_dueDate" type=text size="18"/>
                                <img id="time_start_trigger2" class="calendarImage" src="../graphics/calendarIcon.jpg" alt="Pick a date" />

                                  <script type="text/javascript">
                                                    Calendar.setup({
                                                                    inputField     :    "PO_dueDate",   // id of the input field
                                                                    showsTime      :    true,
                                                                    ifFormat       :    "%m/%d/%Y[%I:%M %p]",   // format of the input field
                                                                    button         :    "time_start_trigger2",   // trigger for the calendar (button ID) 
                                                    });
                                   </script> 
                      <div align="left" class="buttonLeft">
		    		 <input type="submit" name="__batchDueDate" value="Update" onClick="return batchCustomFieldUpdate()">
		            </div>
			       </td>
			     </tr>
	             </table>
	            </td>
	           </tr>
	           </table>		 
			</div>
			
			
			<!-- Custom Field1 -->
			 <c:if test="${siteConfig['ORDER_CUSTOM_FIELD1_NAME'].value != ''}">
			
			 <div style="float:left;padding: 5px">
	           <table class="productBatchTool" cellpadding="5" cellspacing="5">
	           <tr>
	            <td valign="top">
	             <table cellpadding="1" cellspacing="1">
	              <tr style="height:30px;white-space: nowrap">
	               <td valign="top" width="20px"><img class="toolTipImg" title="${siteConfig['ORDER_CUSTOM_FIELD1_NAME'].value} ::Change ${siteConfig['ORDER_CUSTOM_FIELD1_NAME'].value} value of multiple orders" src="../graphics/question.gif" /></td>
	               <td valign="top"><h3>${siteConfig['ORDER_CUSTOM_FIELD1_NAME'].value}</h3></td>
	              </tr>
	             </table>
	            </td>
	           </tr>
	           <tr>
	            <td valign="top">
	             <table cellpadding="1" cellspacing="1">
	              <tr style="">
			       <td>
			        <select name=__docks> 
			        	<option value="">Please Select</option>
			          	 <c:forTokens items="${siteConfig['ORDER_CUSTOM_FIELD1_VALUE'].value}" delims="," var="value">
				            <option value="${value}">${value}</option>
				         </c:forTokens>     
				      </select>
			        <div align="left" class="buttonLeft"> 
		            <input type="submit" name="__batchUpdateDocks" value="Update" onClick="return batchCustomFieldUpdate ()"/>
		            </div>
			       </td>
			     </tr>
	             </table>
	            </td>
	           </tr>
	           </table>
	         </div>
	         </c:if>
	      <!-- Custom field2  -->
	      
	    <c:if test="${siteConfig['ORDER_CUSTOM_FIELD2_NAME'].value != ''}">
	      
	       <div style="float:left;padding: 5px">
	           <table class="productBatchTool" cellpadding="5" cellspacing="5">
	           <tr>
	            <td valign="top">
	             <table cellpadding="1" cellspacing="1">
	              <tr style="height:30px;white-space: nowrap">
	               <td valign="top" width="20px"><img class="toolTipImg" title="${siteConfig['ORDER_CUSTOM_FIELD2_NAME'].value} ::Change ${siteConfig['ORDER_CUSTOM_FIELD2_NAME'].value} value of multiple orders" src="../graphics/question.gif" /></td>
	               <td valign="top"><h3>${siteConfig['ORDER_CUSTOM_FIELD2_NAME'].value}</h3></td>
	              </tr>
	             </table>
	            </td>
	           </tr>
	           <tr>
	            <td valign="top">
	             <table cellpadding="1" cellspacing="1">
	              <tr style="">
			       <td>
			        <select name=__resources>   
			        		<option value="">Please Select</option>
			        
			        	<c:forTokens items="${siteConfig['ORDER_CUSTOM_FIELD2_VALUE'].value}" delims="," var="value">
				          <option value="${value}">${value}</option>
				       </c:forTokens>     
				      </select>
			        <div align="left" class="buttonLeft">
		            <input type="submit" name="__batchUpdateResc" value="Update" onClick="return batchCustomFieldUpdate ()"/>
		            </div>
			       </td>
			     </tr>
	             </table>
	            </td>
	           </tr>
	           </table>
	         </div>
	       </c:if>
	         <!-- end -->
		</div>
	</c:if>	  
  </td><td class="topr_g" ></td></tr>
  <tr><td class="boxmidlrg" >

 <!-- tabs -->
 <div class="tab-wrapper">
	<h4 title=""></h4>
	<div>
	<!-- start tab -->

	  	<div class="listdivi"></div>
	    
		  	<div class="listlight">
		  	<!-- input field -->
			<table border="0" cellpadding="0" cellspacing="1" width="100%" class="quickMode">
				<tr>
					<td>&nbsp;
					</td>
					<c:if test="${model.purchaseOrders.nrOfElements > 0}">
						<td class="pageShowing">
							<fmt:message key="showing">
								<fmt:param value="${model.purchaseOrders.firstElementOnPage + 1}" />
								<fmt:param value="${model.purchaseOrders.lastElementOnPage + 1}" />
								<fmt:param value="${model.purchaseOrders.nrOfElements}" />
							</fmt:message>
						</td>
					</c:if>
					<td class="pageNavi">
						<fmt:message key="page" />
						<select name="page" id="page" onchange="submit()">
							<c:forEach begin="1" end="${model.purchaseOrders.pageCount}"
								var="page">
								<option value="${page}"
									<c:if test="${page == (model.purchaseOrders.page+1)}">selected</c:if>>
									${page}
								</option>
							</c:forEach>
						</select> 
						<fmt:message key="of" />
						<c:out value="${model.purchaseOrders.pageCount}" /> 
						|
						<c:if test="${model.purchaseOrders.firstPage}">
							<span class="pageNaviDead"><fmt:message key="previous" /></span>
						</c:if>
						<c:if test="${not model.purchaseOrders.firstPage}">
							<a
								href="<c:url value="purchaseOrders.jhtm"><c:param name="page" value="${model.purchaseOrders.page}"/><c:param name="size" value="${model.purchaseOrders.pageSize}"/></c:url>"
								class="pageNaviLink"><fmt:message key="previous" /></a>
						</c:if>
						|
						<c:if test="${model.purchaseOrders.lastPage}">
							<span class="pageNaviDead"><fmt:message key="next" /></span>
						</c:if>
						<c:if test="${not model.purchaseOrders.lastPage}">
							<a
								href="<c:url value="purchaseOrders.jhtm"><c:param name="page" value="${model.purchaseOrders.page+2}"/><c:param name="size" value="${model.purchaseOrders.pageSize}"/></c:url>"
								class="pageNaviLink"><fmt:message key="next" /></a>
						</c:if>
					</td>
				</tr>
			</table>
		
			<table border="0" cellpadding="0" cellspacing="0" width="100%"
				class="listings">
				<tr class="listingsHdr2">
				  <c:if test="${model.purchaseOrders.nrOfElements > 0}">
			    	<td align="center" class="quickMode"><input type="checkbox" onclick="toggleAll(this)"></td>
				  </c:if>
					<td class="indexCol">&nbsp;</td>
					<td class="listingsHdr3">
			    	  <table cellspacing="0" cellpadding="1">
			    	  <tr>
			    	    <c:choose>
			    	      <c:when test="${purchaseOrderSearch.sort == 'po_number desc'}">
			    	        <td class="listingsHdr3">
			    	        <a class="hdrLink" href="#" onClick="document.getElementById('sort').value='po_number';document.getElementById('list').submit()"><fmt:message key="poNumber" /></a>
			    	        </td>
			    	        <td><img src="../graphics/up.gif" border="0"></td>
			    	      </c:when>
			    	      <c:when test="${purchaseOrderSearch.sort == 'po_number'}">
			    	        <td class="listingsHdr3">
			    	        <a class="hdrLink" href="#" onClick="document.getElementById('sort').value='po_number desc';document.getElementById('list').submit()"><fmt:message key="poNumber" /></a>
			    	        </td>
			    	        <td><img src="../graphics/down.gif" border="0"></td>
			    	      </c:when>
			    	      <c:otherwise>
			    	        <td colspan="2" class="listingsHdr3">
			    	        <a class="hdrLink" href="#" onClick="document.getElementById('sort').value='po_number desc';document.getElementById('list').submit()"><fmt:message key="poNumber" /></a>
			    	        </td>
			    	      </c:otherwise>
			    	    </c:choose>  
			    	  </tr>
			    	  </table>
					</td>
					<td class="indexCol" style="width:45px;">&nbsp;</td>
					<td class="listingsHdr3"><fmt:message key="status" /></td>
				    <td class="listingsHdr3"><fmt:message key="sku" /></td>
					
					<td class="listingsHdr3">
			    	  <table cellspacing="0" cellpadding="1">
			    	  <tr>
			    	    <c:choose>
			    	      <c:when test="${purchaseOrderSearch.sort == 'created desc'}">
			    	        <td class="listingsHdr3">
			    	        <a class="hdrLink" href="#" onClick="document.getElementById('sort').value='created';document.getElementById('list').submit()"><fmt:message key="created" /></a>
			    	        </td>
			    	        <td><img src="../graphics/up.gif" border="0"></td>
			    	      </c:when>
			    	      <c:when test="${purchaseOrderSearch.sort == 'created'}">
			    	        <td class="listingsHdr3">
			    	        <a class="hdrLink" href="#" onClick="document.getElementById('sort').value='created desc';document.getElementById('list').submit()"><fmt:message key="created" /></a>
			    	        </td>
			    	        <td><img src="../graphics/down.gif" border="0"></td>
			    	      </c:when>
			    	      <c:otherwise>
			    	        <td colspan="2" class="listingsHdr3">
			    	        <a class="hdrLink" href="#" onClick="document.getElementById('sort').value='created desc';document.getElementById('list').submit()"><fmt:message key="created" /></a>
			    	        </td>
			    	      </c:otherwise>
			    	    </c:choose>  
			    	  </tr>
			    	  </table>
					</td>
					<td class="listingsHdr3">Arrived</td>
					<td class="listingsHdr3">Staged</td>
					<td class="listingsHdr3">PT</td>
					<td class="listingsHdr3">
			    	  <table cellspacing="0" cellpadding="1">
			    	  <tr>
			    	    <c:choose>
			    	      <c:when test="${purchaseOrderSearch.sort == 'due_date desc'}">
			    	        <td class="listingsHdr3">
			    	        <a class="hdrLink" href="#" onClick="document.getElementById('sort').value='due_date';document.getElementById('list').submit()"><fmt:message key="dueDate" /></a>
			    	        </td>
			    	        <td><img src="../graphics/up.gif" border="0"></td>
			    	      </c:when>
			    	      <c:when test="${purchaseOrderSearch.sort == 'due_date'}">
			    	        <td class="listingsHdr3">
			    	        <a class="hdrLink" href="#" onClick="document.getElementById('sort').value='due_date desc';document.getElementById('list').submit()"><fmt:message key="dueDate" /></a>
			    	        </td>
			    	        <td><img src="../graphics/down.gif" border="0"></td>
			    	      </c:when>
			    	      <c:otherwise>
			    	        <td colspan="2" class="listingsHdr3">
			    	        <a class="hdrLink" href="#" onClick="document.getElementById('sort').value='due_date desc';document.getElementById('list').submit()"><fmt:message key="dueDate" /></a>
			    	        </td>
			    	      </c:otherwise>
			    	    </c:choose>  
			    	  </tr>
			    	  </table>
					</td>
					<td class="listingsHdr3" align="center">
					  <table cellspacing="0" cellpadding="1">
			    	  <tr>
			    	    <c:choose>
			    	      <c:when test="${purchaseOrderSearch.sort == 'sub_total desc'}">
			    	        <td class="listingsHdr3">
			    	        <a class="hdrLink" href="#" onClick="document.getElementById('sort').value='sub_total';document.getElementById('list').submit()"><fmt:message key="total" /></a>
			    	        </td>
			    	        <td><img src="../graphics/up.gif" border="0"></td>
			    	      </c:when>
			    	      <c:when test="${purchaseOrderSearch.sort == 'sub_total'}">
			    	        <td class="listingsHdr3">
			    	        <a class="hdrLink" href="#" onClick="document.getElementById('sort').value='sub_total desc';document.getElementById('list').submit()"><fmt:message key="total" /></a>
			    	        </td>
			    	        <td><img src="../graphics/down.gif" border="0"></td>
			    	      </c:when>
			    	      <c:otherwise>
			    	        <td colspan="2" class="listingsHdr3">
			    	        <a class="hdrLink" href="#" onClick="document.getElementById('sort').value='sub_total desc';document.getElementById('list').submit()"><fmt:message key="total" /></a>
			    	        </td>
			    	      </c:otherwise>
			    	    </c:choose>  
			    	  </tr>
			    	  </table>
			    	</td>
			    	<td class="listingsHdr3" align="center">
					  <table cellspacing="0" cellpadding="1">
			    	  <tr>
			    	    <c:choose>
			    	      <c:when test="${purchaseOrderSearch.sort == 'trailer desc'}">
			    	        <td class="listingsHdr3">
			    	        <a class="hdrLink" href="#" onClick="document.getElementById('sort').value='trailer';document.getElementById('list').submit()"><fmt:message key="trailer" /></a>
			    	        </td>
			    	        <td><img src="../graphics/up.gif" border="0"></td>
			    	      </c:when>
			    	      <c:when test="${purchaseOrderSearch.sort == 'trailer'}">
			    	        <td class="listingsHdr3">
			    	        <a class="hdrLink" href="#" onClick="document.getElementById('sort').value='trailer desc';document.getElementById('list').submit()"><fmt:message key="trailer" /></a>
			    	        </td>
			    	        <td><img src="../graphics/down.gif" border="0"></td>
			    	      </c:when>
			    	      <c:otherwise>
			    	        <td colspan="2" class="listingsHdr3">
			    	        <a class="hdrLink" href="#" onClick="document.getElementById('sort').value='trailer desc';document.getElementById('list').submit()"><fmt:message key="trailer" /></a>
			    	        </td>
			    	      </c:otherwise>
			    	    </c:choose>  
			    	  </tr>
			    	  </table>
			    	</td>
					<td class="listingsHdr3">
					  <table cellspacing="0" cellpadding="1">
			    	  <tr>
			    	    <c:choose>
			    	      <c:when test="${purchaseOrderSearch.sort == 'ship_via desc'}">
			    	        <td class="listingsHdr3">
			    	        <a class="hdrLink" href="#" onClick="document.getElementById('sort').value='ship_via';document.getElementById('list').submit()"><fmt:message key="shipVia" /></a>
			    	        </td>
			    	        <td><img src="../graphics/up.gif" border="0"></td>
			    	      </c:when>
			    	      <c:when test="${purchaseOrderSearch.sort == 'ship_via'}">
			    	        <td class="listingsHdr3">
			    	        <a class="hdrLink" href="#" onClick="document.getElementById('sort').value='ship_via desc';document.getElementById('list').submit()"><fmt:message key="shipVia" /></a>
			    	        </td>
			    	        <td><img src="../graphics/down.gif" border="0"></td>
			    	      </c:when>
			    	      <c:otherwise>
			    	        <td colspan="2" class="listingsHdr3">
			    	        <a class="hdrLink" href="#" onClick="document.getElementById('sort').value='ship_via desc';document.getElementById('list').submit()"><fmt:message key="shipVia" /></a>
			    	        </td>
			    	      </c:otherwise>
			    	    </c:choose>  
			    	  </tr>
			    	  </table>
			    	</td>
					<td class="listingsHdr3" align="center">
					  <table cellspacing="0" cellpadding="1">
			    	  <tr>
			    	    <c:choose>
			    	      <c:when test="${purchaseOrderSearch.sort == 'shipping_quote desc'}">
			    	        <td class="listingsHdr3">
			    	        <a class="hdrLink" href="#" onClick="document.getElementById('sort').value='shipping_quote';document.getElementById('list').submit()"><fmt:message key="shippingQuote" /></a>
			    	        </td>
			    	        <td><img src="../graphics/up.gif" border="0"></td>
			    	      </c:when>
			    	      <c:when test="${purchaseOrderSearch.sort == 'shipping_quote'}">
			    	        <td class="listingsHdr3">
			    	        <a class="hdrLink" href="#" onClick="document.getElementById('sort').value='shipping_quote desc';document.getElementById('list').submit()"><fmt:message key="shippingQuote" /></a>
			    	        </td>
			    	        <td><img src="../graphics/down.gif" border="0"></td>
			    	      </c:when>
			    	      <c:otherwise>
			    	        <td colspan="2" class="listingsHdr3">
			    	        <a class="hdrLink" href="#" onClick="document.getElementById('sort').value='shipping_quote desc';document.getElementById('list').submit()"><fmt:message key="shippingQuote" /></a>
			    	        </td>
			    	      </c:otherwise>
			    	    </c:choose>  
			    	  </tr>
			    	  </table>
			    	</td>
			    	<td class="listingsHdr3">
					  <table cellspacing="0" cellpadding="1">
			    	  <tr>
			    	    <c:choose>
			    	      <c:when test="${purchaseOrderSearch.sort == 'order_by desc'}">
			    	        <td class="listingsHdr3">
			    	        <a class="hdrLink" href="#" onClick="document.getElementById('sort').value='order_by';document.getElementById('list').submit()"><fmt:message key="orderBy" /></a>
			    	        </td>
			    	        <td><img src="../graphics/up.gif" border="0"></td>
			    	      </c:when>
			    	      <c:when test="${purchaseOrderSearch.sort == 'order_by'}">
			    	        <td class="listingsHdr3">
			    	        <a class="hdrLink" href="#" onClick="document.getElementById('sort').value='order_by desc';document.getElementById('list').submit()"><fmt:message key="orderBy" /></a>
			    	        </td>
			    	        <td><img src="../graphics/down.gif" border="0"></td>
			    	      </c:when>
			    	      <c:otherwise>
			    	        <td colspan="2" class="listingsHdr3">
			    	        <a class="hdrLink" href="#" onClick="document.getElementById('sort').value='order_by desc';document.getElementById('list').submit()"><fmt:message key="orderBy" /></a>
			    	        </td>
			    	      </c:otherwise>
			    	    </c:choose>  
			    	  </tr>
			    	  </table>
					</td>
					<td class="listingsHdr3">
					  <table cellspacing="0" cellpadding="1">
			    	  <tr>
			    	    <c:choose>
			    	      <c:when test="${purchaseOrderSearch.sort == 'company desc'}">
			    	        <td class="listingsHdr3">
			    	        <a class="hdrLink" href="#" onClick="document.getElementById('sort').value='company';document.getElementById('list').submit()"><fmt:message key="supplier" /></a>
			    	        </td>
			    	        <td><img src="../graphics/up.gif" border="0"></td>
			    	      </c:when>
			    	      <c:when test="${purchaseOrderSearch.sort == 'company'}">
			    	        <td class="listingsHdr3">
			    	        <a class="hdrLink" href="#" onClick="document.getElementById('sort').value='company desc';document.getElementById('list').submit()"><fmt:message key="supplier" /></a>
			    	        </td>
			    	        <td><img src="../graphics/down.gif" border="0"></td>
			    	      </c:when>
			    	      <c:otherwise>
			    	        <td colspan="2" class="listingsHdr3">
			    	        <a class="hdrLink" href="#" onClick="document.getElementById('sort').value='company desc';document.getElementById('list').submit()"><fmt:message key="supplier" /></a>
			    	        </td>
			    	      </c:otherwise>
			    	    </c:choose>  
			    	  </tr>
			    	  </table>
			    	</td>
			    	<td class="listingsHdr3">
					  <table cellspacing="0" cellpadding="1">
			    	  <tr>
			    	    <c:choose>
			    	      <c:when test="${purchaseOrderSearch.sort == 'supplier_invoice_num desc'}">
			    	        <td class="listingsHdr3">
			    	        <a class="hdrLink" href="#" onClick="document.getElementById('sort').value='supplier_invoice_num';document.getElementById('list').submit()"><fmt:message key="supplierInvoiceNumber" /></a>
			    	        </td>
			    	        <td><img src="../graphics/up.gif" border="0"></td>
			    	      </c:when>
			    	      <c:when test="${purchaseOrderSearch.sort == 'supplier_invoice_num'}">
			    	        <td class="listingsHdr3">
			    	        <a class="hdrLink" href="#" onClick="document.getElementById('sort').value='supplier_invoice_num desc';document.getElementById('list').submit()"><fmt:message key="supplierInvoiceNumber" /></a>
			    	        </td>
			    	        <td><img src="../graphics/down.gif" border="0"></td>
			    	      </c:when>
			    	      <c:otherwise>
			    	        <td colspan="2" class="listingsHdr3">
			    	        <a class="hdrLink" href="#" onClick="document.getElementById('sort').value='supplier_invoice_num desc';document.getElementById('list').submit()"><fmt:message key="supplierInvoiceNumber" /></a>
			    	        </td>
			    	      </c:otherwise>
			    	    </c:choose>  
			    	  </tr>
			    	  </table>
			    	</td> 
			    	<td class="listingsHdr3"><fmt:message key="note" /></td>
				</tr>
				<c:forEach items="${model.purchaseOrders.pageList}" var="po" varStatus="status">
					<tr class="row${status.index % 2}"	onmouseover="changeStyleClass(this,'rowOver')" 	onmouseout="changeStyleClass(this,'row${status.index % 2}')">
					  <td align="center">
					   <c:if test="${model.purchaseOrders.nrOfElements > 0}">
			            <input name="__selected_id" value="${po.poId}" type="checkbox" id="__selected_id_${po.poId}">
			          </c:if> 
			          </td>
						<td class="indexCol">
							<c:out value="${status.count + model.purchaseOrders.firstElementOnPage}" />.
						</td>
						<td class="nameCol"><a href="purchaseOrderForm2.jhtm?poId=${po.poId}"><c:out value="${po.poNumber}" /></a></td>
						<td width="40"><a href="purchaseOrder.jhtm?poId=${po.poId}" onclick="window.open(this.href,'','width=800,height=300,resizable=yes'); return false;"><img src="../graphics/printer.png" class="quickMode" alt="print Purchase Order" title="print Purchase Order" border="0"></a>
						 <c:choose>
						  <c:when test="${po.dropShip}"><img class="quickMode" src="../graphics/checkbox.png" alt="" title="" border="0"></c:when>
						  <c:otherwise><img class="quickMode" src="../graphics/box.png" alt="" title="" border="0"></c:otherwise>
						 </c:choose>
						</td>
						<td class="nameCol"><fmt:message key="${po.statusName}" /></td>
						<!-- Sku Popup -->
						<td class="nameCol">
						<a href="POskuList.jhtm?poId=${po.poId}" rel="width:200,height:230" class="mbOrder quickMode">
						<img class="quickMode" src="../graphics/magnifier.gif" alt="" title="" border="0"></a></td>
						<td class="nameCol" style="font-size:9px"><a href="purchaseOrderForm2.jhtm?poId=${po.poId}"><fmt:formatDate type="both" timeStyle="full" pattern="MM/dd/yyyy hh:mm a" value="${po.created}"/></a></td>
						
						<td class="nameCol" style="font-size:9px">
							<fmt:formatDate type="both" timeStyle="full" pattern="MM/dd/yyyy hh:mm a" value="${po.arrivedDate}"/>
						</td>
						<td class="nameCol" style="font-size:9px">
							<fmt:formatDate type="both" timeStyle="full" pattern="MM/dd/yyyy hh:mm a" value="${po.stagedDate}"/>
						</td>
						
						<td class="nameCol"><c:out value="${po.pt}" /></td>
						<td class="nameCol" style="font-size:9px"><fmt:formatDate type="both" timeStyle="full" pattern="MM/dd/yyyy hh:mm a" value="${po.dueDate}"/></td>
						<td class="numberCol"><fmt:formatNumber value="${po.subTotal}" pattern="#,##0.00"/></td>
						<td class="nameCol"><c:out value="${po.trailer}" /></td>
						<td class="nameCol"><c:out value="${po.shipVia}" /></td>
						<td class="numberCol"><fmt:formatNumber value="${po.shippingQuote}" pattern="#,##0.00"/></td>
						<td class="nameCol"><c:out value="${po.orderBy}" /></td>
						<td class="nameCol"><c:out value="${po.company}" /></td>
						<td class="nameCol"><c:out value="${po.supplierInvoiceNumber }" /></td>
						<td class="nameCol"><c:out value="${po.noteTrimmed}" /></td>
					</tr>
				</c:forEach>
				<c:if test="${model.purchaseOrders.nrOfElements == 0}">
					<tr class="emptyList">
						<td colspan="8">
							&nbsp;
						</td>
					</tr>
				</c:if>
			</table>
			
			<c:if test="${model.purchaseOrders.nrOfElements > 0}">
			</c:if>
			<table border="0" cellpadding="0" cellspacing="1" width="100%" class="quickMode">
				<tr>
					<td class="pageSize">
						<select name="size"
							onchange="document.getElementById('page').value=1;submit()">
							<c:forTokens items="10,25,50,100,500" delims="," var="current">
								<option value="${current}"
									<c:if test="${current == model.purchaseOrders.pageSize}">selected</c:if>>
									${current}
									<fmt:message key="perPage" />
								</option>
							</c:forTokens>
						</select>
					</td>
				</tr>
			</table>
		    <!-- end input field -->  
		    <div class="quickMode">	  
		  	<div class="note">
				<img src="../graphics/checkbox.png" alt="" title="" border="0" class="quickMode"> <fmt:message key="dropShip" />
			</div>
			</div>
		  	<!-- start button -->
		  	<div class="quickMode">
		  	<input type="hidden" name="pid" value="${model.pid}" >
            <div align="left" class="button">
              <sec:authorize ifAnyGranted="ROLE_AEM,ROLE_ADMIN_${prop_site_id},ROLE_PURCHASE_ORDER_CREATE"> 
				<input type="submit" name="__add" value="<fmt:message key='add' /> <fmt:message key='purchaseOrders'/>"/>
				<%-- <input type="submit" name="__delete" value="<fmt:message key='delete' />" onClick="return deleteSelected()"> --%>
			  </sec:authorize>	
	  	    </div>	
	  	    </div>
			<!-- end button -->	
	        </div>	  	
	<!-- end tab -->        
	</div>
<!-- end tabs -->			
</div>
  
  <!-- end table -->
   </td><td class="boxmidr" ></td></tr>
  <tr><td class="botl"></td><td class="botr"></td></tr>
  </table>
  
<!-- end main box -->  
</div>
</form>
</c:if>
</sec:authorize>

  </tiles:putAttribute>
</tiles:insertDefinition>