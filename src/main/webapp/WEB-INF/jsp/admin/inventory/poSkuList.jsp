<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="/WEB-INF/tlds/spring.tld" prefix="spring" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib uri="http://www.springframework.org/security/tags" prefix="sec" %>



<sec:authorize ifAnyGranted="ROLE_AEM,ROLE_ADMIN_${prop_site_id},ROLE_PURCHASE_ORDER_UPDATE,ROLE_PURCHASE_ORDER_CREATE">
<div class="listfl"></div>
PO Number : ${model.PONumber}<br><br>
Sku List <br>
<c:forEach var="lineItem" items="${model.items}" varStatus="status">
${status.index + 1}.
${lineItem.sku} <br>
</c:forEach>
</div>


</sec:authorize>

