<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="/WEB-INF/tlds/spring.tld" prefix="spring" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib uri="http://www.springframework.org/security/tags" prefix="sec" %>

<table>
		<tr><td>Tentative Schedule </td><td> <form:checkbox path="purchaseOrder.comingToVia" value="" name="comingToVia"/></td></tr>
		<tr><td>Is all or Part of PO sold Already</td><td><form:input type="text" name="poSold" path="purchaseOrder.poSold"/></td></tr>
		<tr><td>What to Check for Before Staging</td>
		<td>
			<form:select path="purchaseOrder.checkBeforeStaging">
				<form:option value="" label="Please Select"/>
	            <c:forTokens items="${siteConfig['PO_STAGING_CHECKLIST'].value}" delims="," var="value">
	  		      <form:option value="${value}">${value}</form:option>
	  		   </c:forTokens>
	        </form:select>
		</td>
		</tr>
		<tr><td>What to Check for notes</td><td><form:input type="text" name="notes" path="purchaseOrder.checkNotes"/></td></tr>
		<tr><td>Picture Needed </td>
		<td> <form:checkbox path="purchaseOrder.pictureNeeded" value="" name="pic"/></td>
		</tr>
		<tr><td>Pricing Instructions</td>
		<td><form:select path="purchaseOrder.pricingInstructions">
                <form:option value="" label="Please Select"/>
                <c:forTokens items="${siteConfig['PO_PRICING_INSTRUCTIONS'].value}" delims="," var="value">
  		        <form:option value="${value}">${value}</form:option>
  		        </c:forTokens>				    
  		     </form:select>
  		 </td>
        </tr>
		<tr><td>Location of Merchandise After Staging</td>
		<td>
		<form:select path="purchaseOrder.locAfterStaging">
			<form:option value="" label="Please Select"/>
              <c:forTokens items="${siteConfig['PO_LOCATION_AFTERSTAGING'].value}" delims="," var="value">
  		        <form:option value="${value}">${value}</form:option>
  		      </c:forTokens>				       
  		    </form:select>
  		 </td>
        </tr>
        <tr><td>Trailer</td><td><form:input type="text" name="trailer" path="purchaseOrder.trailer"/></td></tr>
        
	</table>
