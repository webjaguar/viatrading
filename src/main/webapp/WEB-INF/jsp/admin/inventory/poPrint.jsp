<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
  <head>
    <title><fmt:message key="purchaseOrder" /></title>
    <link href="<c:url value="/assets/invoice.css"/>" rel="stylesheet" type="text/css"/>
  </head>  
<body class="invoice">
<div style="margin:0 auto;">

<c:if test="${purchaseOrder != null}">
<div align="right"><a href="javascript:window.print()"><img border="0" src="../graphics/printer.png"></a></div>
<c:out value="${poLayout.headerHtml}" escapeXml="false"/>
<table border="0" width="100%" cellspacing="3" cellpadding="1">
<tr valign="top">
<td>
<b><fmt:message key="supplier" />:</b>
<pre style="font-family:Verdana,Arial,sans-serif;font-size:12px;"><c:out value="${purchaseOrder.billingAddress}" /></pre>
</td>
<td>&nbsp;</td>
<td>
<b><fmt:message key="shipTo" />:</b>
<pre style="font-family:Verdana,Arial,sans-serif;font-size:12px;"><c:out value="${purchaseOrder.shippingAddress}" /></pre>
</td>
<td>&nbsp;</td>
<td align="right">
<table>
  <tr>
    <tr>
      <c:choose>
        <c:when test="${purchaseOrder.dropShip}">
          <td><fmt:message key='dropShipNumber'/>#</td>
        </c:when>
        <c:otherwise>
          <td><fmt:message key='poNumber'/></td>
        </c:otherwise>
      </c:choose> 
	      <td>:&nbsp;&nbsp;</td>
	      <td><b><c:out value="${purchaseOrder.poNumber}" /></b></td>
	  </tr>
	  <tr>
	    <td><fmt:message key="date" /></td>
	    <td>:&nbsp;&nbsp;</td>
	    <td><fmt:formatDate type="both" timeStyle="full" pattern="MM/dd/yyyy" value="${purchaseOrder.created}"/></td>
	  </tr>
	  <tr>
	    <td><fmt:message key="po_dueDate" /></td>
	    <td>:&nbsp;&nbsp;</td>
	    <td><fmt:formatDate type="both" timeStyle="full" pattern="MM/dd/yyyy" value="${purchaseOrder.dueDate}"/></td>
	  </tr>
	  <tr>
	    <td><fmt:message key="accountNumber" /></td>
	    <td>:&nbsp;&nbsp;</td>
	    <td><c:out value="${purchaseOrder.supplier.accountNumber}" /></td>
	  </tr>
	  <tr>
	    <td><fmt:message key="orderBy" /></td>
	    <td>:&nbsp;&nbsp;</td>
	    <td><c:out value="${purchaseOrder.orderBy}" /></td>
	  </tr>
	  <tr>
	    <td><fmt:message key="shipVia" /></td>
	    <td>:&nbsp;&nbsp;</td>
	    <td><c:out value="${purchaseOrder.shipVia}" /></td>
	  </tr>
</table>
</td>
</tr>
</table>
<br/>
<hr>
<table border="0" cellpadding="2" cellspacing="1" width="100%" class="invoice">
  <tr>
    <c:set var="cols" value="0"/>
    <th width="5%" class="invoice" align="center"><fmt:message key="line" />#</th>
    <th class="invoice" align="center"><fmt:message key="productSku" /></th>
    <c:if test="${purchaseOrder.dropShip}">
	  <th class="invoice"><fmt:message key="productName" /></th><c:set var="cols" value="${cols+1}"/>
	</c:if>
    <th class="invoice" align="center"><fmt:message key="supplierSku" /></th>
    <th width="5%" class="invoice" align="center"><fmt:message key="quantity" /></th>
    <th width="5%" class="invoice" align="center"><fmt:message key="productPrice" /></th>
    <th width="15%" class="invoice" align="center"><fmt:message key="total" /></th>
  </tr>
<c:forEach var="lineItem" items="${purchaseOrder.poLineItems}" varStatus="status">
<tr valign="top">
  <td class="invoice" align="center"><c:out value="${status.count}"/></td>
  <td class="invoice" align="left"><c:out value="${lineItem.sku}"/></td>
  <c:if test="${purchaseOrder.dropShip}">
  <td class="invoice"><c:out value="${lineItem.product.name}"/>
	<c:forEach items="${lineItem.productAttributes}" var="productAttribute">
		<div class="invoice_lineitem_attributes">- <c:out value="${productAttribute.optionName}"/>: <c:out value="${productAttribute.valueName}"/>
		<c:if test="${(! empty productAttribute.optionPriceOriginal) && productAttribute.optionPriceOriginal != 0 }">
    		<c:out value="${productAttribute.optionPriceOriginal}"/> <fmt:message key="${productAttribute.optionPriceMessageF}"/>
    	</c:if>
		</div>
	</c:forEach>
  </td>
  </c:if>
  <td class="invoice" align="left"><c:out value="${lineItem.supplierSku}"/></td>
  <td class="invoice" align="left"><c:out value="${lineItem.qty}"/></td>
  <td class="invoice" align="center"><c:out value="${lineItem.cost}" /></td>
  <td class="invoice" align="right"><fmt:formatNumber value="${lineItem.totalCost}" pattern="#,##0.00" /></td>
</tr>
</c:forEach>
  <tr bgcolor="#BBBBBB">
    <td colspan="${6+cols}">&nbsp;</td>    
  </tr>
  <tr>
    <td class="invoice" colspan="${5+cols}" align="right"></td>
    <td class="invoice" align="right">&nbsp;</td>
  </tr>
  <tr>
    <td class="invoice" colspan="${5+cols}" align="right"><fmt:message key="subTotal" />:</td>
    <td align="right" bgcolor="#F8FF27"><fmt:message key="${siteConfig['CURRENCY'].value}" /><fmt:formatNumber value="${purchaseOrder.subTotal}" pattern="#,##0.00" /></td>
  </tr>
</table>

<c:if test="${purchaseOrder.specialInstruction != null and purchaseOrder.specialInstruction != ''}">
<div class="instructionsWrapper">
	  <b><c:out value="${siteConfig['PO_SPECIAL_INSTRUCTIONS'].value}"/>:</b>&nbsp;<c:out value="${purchaseOrder.specialInstruction}" escapeXml="false"/>  	 
</div>
</c:if>

<br />
<c:out value="${poLayout.footerHtml}" escapeXml="false"/>
<p>

</c:if>
</div>

</body>
</html>