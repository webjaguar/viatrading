<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/security/tags" prefix="sec" %>
<link rel="stylesheet" type="text/css" media="all" href="../../admin/javascript/jscalendar-1.0/skins/aqua/theme.css" title="Aqua" /> <!-- calendar stylesheet -->
<script type="text/javascript" src="../../admin/javascript/jscalendar-1.0/calendar.js"></script> <!-- main calendar program -->
<script type="text/javascript" src="../../admin/javascript/jscalendar-1.0/lang/calendar-en.js"></script>
<script type="text/javascript" src="../../admin/javascript/jscalendar-1.0/calendar-setup.js"></script>
<script type="text/javascript" >
function UpdateStartEndDate(type){
var myDate = new Date();
$('endDate').value = (myDate.getMonth()+1) + "/" + myDate.getDate() + "/" + myDate.getFullYear();
	if ( type == 2 ) {
		myDate.setDate(myDate.getDate() - myDate.getDay());
		$('startDate').value = (myDate.getMonth()+1) + "/" + myDate.getDate() + "/" + myDate.getFullYear();
	} else if ( type == 3 ) {
		myDate.setDate(1);
		$('startDate').value = (myDate.getMonth()+1) + "/" + myDate.getDate() + "/" + myDate.getFullYear();
	}else if ( type == 10 ) {
		myDate.setMonth(0, 1);
		$('startDate').value = (myDate.getMonth()+1) + "/" + myDate.getDate() + "/" + myDate.getFullYear();
	}else if ( type == 20 ) {
		$('startDate').value = "";
		$('endDate').value = "";
	}else{
		$('startDate').value = (myDate.getMonth()+1) + "/" + myDate.getDate() + "/" + myDate.getFullYear();
	}
}
</script>

<!-- menu -->
  <div id="lbox" class="quickMode">
   <table cellpadding="0" cellspacing="0" border="0" class="module" style="width: 100%"><tr><td class="topl">
    </td><td class="topr" ></td></tr><tr><td class="boxmid" >

    <c:if test="${gSiteConfig['gINVENTORY']}">	 
    <c:if test="${gSiteConfig['gPURCHASE_ORDER']}">
	<h2 class="menuleft mfirst">P.O</h2>
    <div class="menudiv"></div>
      <a href="purchaseOrders.jhtm" class="userbutton"><fmt:message key="list"/></a>
    <div class="menudiv"></div>
    </c:if>
    
    <sec:authorize ifAnyGranted="ROLE_AEM,ROLE_ADMIN_${prop_site_id},ROLE_INVENTORY_IMPORT_EXPORT">
    <c:if test="${gSiteConfig['gINVENTORY_IMPORT_EXPORT']}">
    <h2 class="menuleft"><fmt:message key="inventory"/></h2>
    <div class="menudiv"></div>
    <a href="inventoryImport.jhtm" class="userbutton"><fmt:message key="import"/></a>
    <a href="inventoryExport.jhtm" class="userbutton"><fmt:message key="export"/></a>
    <div class="menudiv"></div> 
    </c:if>
    </sec:authorize>
    
    <c:if test="${gSiteConfig['gPURCHASE_ORDER']}">
    <c:if test="${param.tab == 'po'}">
    <script type="text/javascript" src="../javascript/side-bar.js"></script>
    <script type="text/javascript" >
    window.addEvent('domready', function(){	
		$('searchHeaderId').addEvent('click',function(){
			document.searchform.submit();
		});
    });
    </script>
    <div class="menudiv"></div>
      <div align="left" class="searchHeader" id="searchHeaderId"><h2><fmt:message key="search" /></h2></div>
      <form name="searchform" action="purchaseOrders.jhtm" method="post">
	  <table class="searchBoxLeft"><tr><td valign="top" style="width: 110px;">
	  <!-- content area -->   
	    <div class="search2">
	      <p><fmt:message key="status"/>:</p>
	      <select name="status">
	        <option value="" <c:if test="${ticketSearch.status == ''}">selected</c:if>><fmt:message key="all"/></option>
	        <option value="pend" <c:if test="${purchaseOrderSearch.status == 'pend'}">selected</c:if>><fmt:message key="pending"/></option>
	        <option value="send" <c:if test="${purchaseOrderSearch.status == 'send'}">selected</c:if>><fmt:message key="po_send"/></option> 
	        <option value="001" <c:if test="${purchaseOrderSearch.status == '001'}">selected</c:if>><fmt:message key="pending"/> + <fmt:message key="po_send"/></option> 
	        <option value="send01" <c:if test="${purchaseOrderSearch.status == 'send01'}">selected</c:if>><fmt:message key="po_send01"/></option>
	        <option value="001s01" <c:if test="${purchaseOrderSearch.status == '001s01'}">selected</c:if>><fmt:message key="pending"/> + <fmt:message key="po_send"/> + <fmt:message key="po_send01"/></option> 
	        <option value="send05" <c:if test="${purchaseOrderSearch.status == 'send05'}">selected</c:if>><fmt:message key="po_send05"/></option>
	        <option value="001s01s05" <c:if test="${purchaseOrderSearch.status == '001s01s05'}">selected</c:if>><fmt:message key="pending"/> + <fmt:message key="po_send"/> + <fmt:message key="po_send01"/> + <fmt:message key="po_send05"/></option>
	        <option value="send10" <c:if test="${purchaseOrderSearch.status == 'send10'}">selected</c:if>><fmt:message key="po_send10"/></option>
	        <option value="send15" <c:if test="${purchaseOrderSearch.status == 'send15'}">selected</c:if>><fmt:message key="po_send15"/></option>	        
	        <option value="001s01s05s10" <c:if test="${purchaseOrderSearch.status == '001s01s05s10'}">selected</c:if>><fmt:message key="pending"/> + <fmt:message key="po_send"/> + <fmt:message key="po_send01"/> + <fmt:message key="po_send05"/> + <fmt:message key="po_send10"/></option>
	        <option value="rece" <c:if test="${purchaseOrderSearch.status == 'rece'}">selected</c:if>><fmt:message key="po_rece"/></option> 
	        <option value="x" <c:if test="${purchaseOrderSearch.status == 'x'}">selected</c:if>><fmt:message key="po_x"/></option> 
	      </select>
	    </div> 
	    <div class="search2">
	      <p><fmt:message key="poNumber"/>:</p>
	      <input name="po_number" type="text" value="<c:out value='${purchaseOrderSearch.poNumber}' />" size="15" />
	    </div> 
	    <div class="search2">
	      <p><fmt:message key="supplier"/>:</p>
	      <input name="supplier" type="text" value="<c:out value='${purchaseOrderSearch.supplier}' />" size="15" />
	    </div>     
	    <div class="search2">
	      <p><fmt:message key="sku"/>:</p>
	      <input name="sku" type="text" value="<c:out value='${purchaseOrderSearch.sku}' />" size="15" />
	    </div>
	    <div class="search2">
	      <p><fmt:message key="shipVia"/>:</p>
	      <select name="shipVia">
	        <option value=""></option>
	        <c:forEach items="${model.customShippingRateList}" var="shipVia">
	  	     <option value="${shipVia.title}" <c:if test="${purchaseOrderSearch.shipVia == shipVia.title}">selected</c:if>><c:out value="${shipVia.title}" /></option>
	       </c:forEach>
	      </select>
	    </div>
	    <c:if test="${siteConfig['CUSTOM_SHIPPING_CONTACT_INFO'].value == 'true'}">
	    <div class="search2">
	      <p><fmt:message key="contactInfo"/>:</p>
	      <select name="contactInfo">
	        <option value=""></option>
	        <c:forEach items="${model.customShippingContact}" var="contactInfo">
	  	     <option value="${contactInfo.id}" <c:if test="${purchaseOrderSearch.contactInfo == contactInfo.id}">selected</c:if>><c:out value="${contactInfo.company}" /></option>
	       </c:forEach>
	      </select>
	    </div>
	    </c:if>
	    <div class="search2">
	      <p><fmt:message key="orderBy"/>:</p>
	      <input name="order_by" type="text" value="<c:out value='${purchaseOrderSearch.orderBy}' />" size="15" />
	    </div>
	    <div class="search2">
	      <p>PT:</p>
			<select name="pt">
			<option value="" ></option>
            <option value="blank"<c:if test="${purchaseOrderSearch.pt == 'blank' }">selected</c:if>>Blank</option>
            <option value="nonblank"<c:if test="${purchaseOrderSearch.pt == 'nonblank' }">selected</c:if>>Non blank</option>
            <c:forEach items="${model.poPTList}" var="pt" varStatus="status">
              <option value="${pt}"<c:if test="${purchaseOrderSearch.pt == pt}">selected</c:if>>${pt}</option>
            </c:forEach>           
        	</select>
	    </div>  
	    <div class="button">
	      <input type="submit" value="Search"/>
	    </div>
	  <!-- content area -->
	  </td>
	  <td valign="top">
	  <div id="sideBar">
	  <a name="#tabContact" ></a>
	  <a href="#tabCOntact" id="sideBarTab"><img id="h_toggle"  src="../graphics/tabSliderLeft.png" alt="sideBar" title="sideBar" /></a>
      <div id="sideBarContents" style="width:0px;">
	  <table cellpadding="0" cellspacing="0" border="0" class="module" >
      <tr>
      <td class="topl_g"><div align="left" class="searchHeaderAdvanced"><h2><fmt:message key="advancedSearch" /></h2></div></td>
      <td class="topr_g" ></td></tr>
      <tr><td class="boxmidlrg" >
  
		<table class="searchBoxLeftAS"><tr><td>
		<div id="sideBarContentsInner">
			<div class="advancedSearch">
			 <table border="0">
		       <tr> <td class="advancedSearchDateInterval"><input style="width:12px;margin-right:5px;border: 0px" name="dateType" type="radio" <c:if test="${purchaseOrderSearch.dateType == 'arrivedDate'}" >checked</c:if> value="arrivedDate" />Arrived Date</td> </tr>
		       <tr> <td class="advancedSearchDateInterval"><input style="width:12px;margin-right:5px;border: 0px" name="dateType" type="radio" <c:if test="${purchaseOrderSearch.dateType == 'createdDate'}" >checked</c:if> value="createdDate" />Created Date</td> </tr>
		       <tr> <td class="advancedSearchDateInterval"><input style="width:12px;margin-right:5px;border: 0px" name="dateType" type="radio" <c:if test="${purchaseOrderSearch.dateType == 'stagedDate'}" >checked</c:if> value="stagedDate" />Staged Date</td> </tr>
		       </table>
		      <p>start:</p>
		      <input style="width:90px;margin-right:5px;" name="startDate" id="startDate" type="text" value="<fmt:formatDate type="date" pattern="MM/dd/yyyy" value="${purchaseOrderSearch.startDate}"/>" readonly size="11" />
			  <img class="calendarImage"  id="startDate_trigger" src="../graphics/calendarIcon.jpg" />
			  <script type="text/javascript">
	    		Calendar.setup({
	        			inputField     :    "startDate",   // id of the input field
	        			showsTime      :    false,
	        			ifFormat       :    "%m/%d/%Y",   // format of the input field
	        			button         :    "startDate_trigger"   // trigger for the calendar (button ID)
	    		});	
			  </script>
		    </div>
		    <div style="clear:both;"></div>    
		    <div class="advancedSearch">
		      <p>end:</p>
		      <input style="width:90px;margin-right:5px;" name="endDate" id="endDate" type="text" value="<fmt:formatDate type="date" pattern="MM/dd/yyyy" value='${purchaseOrderSearch.endDate}'/>" readonly size="11" />
			  <img class="calendarImage"  id="endDate_trigger" src="../graphics/calendarIcon.jpg" />
			  <script type="text/javascript">
	    		Calendar.setup({
	        			inputField     :    "endDate",   // id of the input field
	        			showsTime      :    false,
	        			ifFormat       :    "%m/%d/%Y",   // format of the input field
	        			button         :    "endDate_trigger"   // trigger for the calendar (button ID)
	    		});	
			  </script>
			</div>  
			<div style="clear:both;"></div>    
		    <div class="advancedSearch">
		      <table border="0">
		       <tr> <td class="advancedSearchDateInterval"><input style="width:12px;margin-right:5px;border: 0px" name="intervaldate" id="today" type="radio" onclick="UpdateStartEndDate(1);" />Today</td> </tr>
		       <tr> <td class="advancedSearchDateInterval"><input style="width:12px;margin-right:5px;border: 0px" name="intervaldate" id="weekToDate" type="radio" onclick="UpdateStartEndDate(2);" />Week To Date</td> </tr>
		       <tr> <td class="advancedSearchDateInterval"><input style="width:12px;margin-right:5px;border: 0px" name="intervaldate" id="monthToDate" type="radio" onclick="UpdateStartEndDate(3);" />Month To Date</td> </tr>
		       <tr> <td class="advancedSearchDateInterval"><input style="width:12px;margin-right:5px;border: 0px" name="intervaldate" id="yearToDate" type="radio" onclick="UpdateStartEndDate(10);" />Year To Date</td> </tr>
		       <tr> <td class="advancedSearchDateInterval"><input style="width:12px;margin-right:5px;border: 0px" name="intervaldate" id="reset" type="radio" onclick="UpdateStartEndDate(20);" />Reset</td> </tr>
		      </table>
		    </div>    
		</div>
		</td></tr></table>
	  </td><td class="boxmidr" ></td></tr>
      <tr><td class="botl"></td><td class="botr"></td></tr>
      </table></div>
    
      </div>
	  </td>
	  
	  
	  
	  
	  
	  

	  </tr></table>
	  </form>
    <div class="menudiv"></div> 
    </c:if> 
    </c:if>
    </c:if> 
    
    </td><td class="boxmidr" ></td></tr>
    <tr><td class="botl"></td><td class="botr"></td></tr></table>
  </div>

