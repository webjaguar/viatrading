<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ taglib uri="/WEB-INF/tlds/spring.tld" prefix="spring" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib uri="http://www.springframework.org/security/tags" prefix="sec" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
  <head>
    <title>DropShip</title>
    <link href="<c:url value="../stylesheet${gSiteConfig['gRESELLER']}.css"/>" rel="stylesheet" type="text/css"/>
    <link href="../../assets/invoice.css" rel="stylesheet" type="text/css"/>
    <script type="text/javascript" src="../javascript/mootools-1.2.5-core.js"></script>
    <script type="text/javascript" src="../javascript/mootools-1.2.5.1-more.js"></script>
  </head> 
<style type="text/css">
html {
overflow-x: hidden;
}
</style>   
<body style="background-color:#ffffff; width:840px;">
<script type="text/javascript">
<!--
function updateItems(orderId, supplierId){
	var requestHTMLData = new Request.HTML ({
		url: "../inventory/ajaxShowOrderLineItems.jhtm?&orderId="+orderId+"&supplierId="+supplierId,
		onSuccess: function(response){ 
		},
		update: $('lineItmes')
	}).send();
}
function addToPO(orderId, supplierId){
	var itemSelected = false;
	var urlAppend = '?orderId='+orderId+'&supplierId='+supplierId+'&dropship=true';
	$$('input.__include_item').each(function(input){
		if(input.checked){
			itemSelected = true;
			urlAppend = urlAppend + '&__include_item='+input.value;
		}
	});
	if(!itemSelected) {
		alert("Please select Line Item(s).");       
		return false;
	}
	parent.mbPOBox.close();
	setTimeout(function() {parent.location = "../inventory/purchaseOrderForm2.jhtm"+urlAppend;},250);
}
//-->
</script>

<div id="lineItmes">
<!-- main box -->
  <div id="mboxfullpopup">
      <div class="title">
        <span>Create Purchase Order</span>
      </div>
      <div id="main">
	      <div style="margin: 2px 0 2px 0; font-size: 12px;width:99.9%" >
	        <div style="margin:0 0 0 300px;">
			<fmt:message key="supplier"/>:
			<select name="supplier" onchange="updateItems('${model.order.orderId}', this.value)">
			  <option value="">Please Select</option>
			  <c:forEach items="${model.supplierList}" var="supplier">
				<option value="${supplier.id}" <c:if test="${model.supplierId == supplier.id}">selected="selected"</c:if>> <c:out value="${supplier.address.company}"/> </option>
			  </c:forEach>
			</select>
			</div>
		  </div>
		  
		  <c:if test="${model.lineItems != null and fn:length(model.lineItems) > 0}">
			<div style="padding: 15px 0 0 5px;">
			<table border="0" cellpadding="2" cellspacing="1" width="840px" class="invoice">
			  <tr>
				<c:set var="cols" value="0"/>
				<th width="10%" class="invoice" align="center"><fmt:message key="include" /></th>
				<th width="10%" class="invoice" align="center"><fmt:message key="line" /> #</th>
				<th width="30%" class="invoice" align="center"><fmt:message key="productSku" /></th>
				<th width="40%" class="invoice" align="center"><fmt:message key="productName" /></th>
				<th width="10%" class="invoice" align="center"><fmt:message key="quantity" /></th>
			  </tr>
			  <c:forEach var="lineItem" items="${model.lineItems}" varStatus="status">
			  <tr valign="top" class="invoice0">
				<td class="invoice" align="center"><input type="checkbox" name="__include_item" value="${lineItem.lineNumber}" class="__include_item"/></td>
				<td class="invoice" align="center"><c:out value="${status.count}"/></td>
				<td class="invoice" align="left"><c:out value="${lineItem.product.sku}"/></td>
				<td class="invoice"><c:out value="${lineItem.product.name}"/></td>
				<td class="invoice" align="center">
				  <div class="po"><c:out value="${lineItem.quantity}"/> </div>
				</td>
			  </tr>
			</c:forEach>  
			</table>
			<div align="left" class="button"> 
        		<input type="button" name="submit" value="Add To PO" onclick="addToPO('${model.order.orderId}', '${model.supplierId}')">	
      		</div>
			</div>
	     </c:if>
      </div>
  </div>
</div>  