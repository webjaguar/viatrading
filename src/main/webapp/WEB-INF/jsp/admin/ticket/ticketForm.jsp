<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="/WEB-INF/tlds/spring.tld" prefix="spring" %>
<%@ taglib uri="http://www.springframework.org/security/tags" prefix="sec" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>

<tiles:insertDefinition name="admin.ticket" flush="true">
  <tiles:putAttribute name="content" type="string">
  
<sec:authorize ifAnyGranted="ROLE_AEM,ROLE_ADMIN_${prop_site_id},ROLE_TICKET_CREATE">
<c:if test="${gSiteConfig['gTICKET']}">	 
<script type="text/javascript"> 
<!--
window.addEvent('domready', function(){			
	var tabs1 = new SimpleTabs($('tab-block-1'), { selector: 'h4'});
});
//--> 
</script> 	
<form:form commandName="ticketForm" method="post" enctype="multipart/form-data">  
<!-- main box -->
  <div id="mboxfull">
  
  <!-- start table -->
  <table cellpadding="0" cellspacing="0" border="0" class="module" >
  <tr>
    <td class="topl_g">
	  
	  <!-- breadcrumb -->
	  <p class="breadcrumb">
	    <a href="../ticket">Ticket</a>
	  </p>
	  
	  <!-- Error Message -->
	  <c:if test="${message != null}">
        <div class="message"><spring:message code="${message}"/></div>
      </c:if>
	  <spring:hasBindErrors name="productForm">
          <div class="message">Please fix all errors!</div>
      </spring:hasBindErrors>  

  
  </td><td class="topr_g" ></td></tr>
  <tr><td class="boxmidlrg" >

 <!-- tabs -->
 <div id="tab-block-1">
	<h4 title="ticket">Ticket</h4>
	<div>
	<!-- start tab -->
        <c:set var="classIndex" value="0" />
        
	  	<div class="listdivi ln tabdivi"></div>
	  	<div class="listdivi"></div>


		  <table width="100%" border="0" cellpadding="3" style="background-color:#E9E9DC; border:1px solid #C9C9C9">
		  <tr>
		    <td colspan="2" style="font-weight:700;color:#DD3204"><fmt:message key="submitASupportTicket" /></td>
		  </tr>
		  <tr>
		    <td colspan="2" style="font-weight:700;color:#1C1C1C"><fmt:message key="describeYourProblemOrQuestion" /></td>
		  </tr>  
		  <tr>
		    <td style="padding:15px 5px 2px 5px;text-align:left"><fmt:message key="subject" />:</td>
		  </tr>
		  <tr>  
		    <td><form:input path="ticket.subject" cssClass="ticketNewfield"/><form:errors path="ticket.subject" cssClass="error"/></td>
		  </tr>  
		  <tr>
		    <td style="padding:10px 5px 2px 5px;text-align:left"><fmt:message key="description/question" />:</td>
		  </tr>
		  <tr>  
		    <td><form:textarea path="ticket.question" cssClass="ticketNewtextarea" rows="18" cols="50" htmlEscape="false"/><form:errors path="ticket.question" cssClass="error"/></td>
		  </tr> 
		  <c:if test="${siteConfig['TICKET_ATTACHMENT'].value == 'true'}">
		  <tr class="ticketAttachmentWrapper">
        	<td>
          	  <fieldset id="ticketAttachment" class="attachment">
          		   <legend><fmt:message key="attachment" /></legend>
          		   <dl>
          		     <dd>
          		       <input value="browse" type="file" name="attachment"/>
          		     </dd>
          		   </dl>
          	  </fieldset>
	    	</td>
      	  </tr>  
      	  </c:if>
		  </table>

	<!-- end tab -->        
	</div> 
	

<!-- end tabs -->			
</div>

<!-- start button -->
<div align="left" class="button"> 
	<input type="submit" value="Submit" />
	<input type="submit" value="Cancel" name="_cancel" />
</div>	
<!-- end button -->	     
 
  <!-- end table -->
    </td><td class="boxmidr" ></td></tr>
  <tr><td class="botl"></td><td class="botr"></td></tr>
  </table>
  
<!-- end main box -->  
</div>
</form:form> 
</c:if>

</sec:authorize>

  </tiles:putAttribute>    
</tiles:insertDefinition>