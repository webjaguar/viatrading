<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/security/tags" prefix="sec" %>


<tiles:insertDefinition name="admin.ticket" flush="true">
  <tiles:putAttribute name="content" type="string">
  
<sec:authorize ifAnyGranted="ROLE_AEM,ROLE_ADMIN_${prop_site_id},ROLE_TICKET_VIEW_LIST">  
<c:if test="${gSiteConfig['gTICKET']}">	   
<form id="list" action="ticketList.jhtm" method="post">
<input type="hidden" id="sort" name="sort" value="${ticketSearch.sort}" />
<!-- main box -->
  <div id="mboxfull">
  
  <!-- start table -->
  <table cellpadding="0" cellspacing="0" border="0" class="module" >
  <tr>
    <td class="topl_g">
	  
	  <!-- breadcrumb -->
	  <p class="breadcrumb">
	    <a href="../ticket"><fmt:message key="ticket" /></a> &gt;
	    <fmt:message key="list" />
	  </p>
	  
	  <!-- Error -->
	  <c:if test="${!empty model.message}">
		<div class="message"><fmt:message key="${model.message}" /></div>
	  </c:if> 
	  
	  <!-- header image -->
	  <img class="headerImage" src="../graphics/ticket.gif" /> 
	  
  </td><td class="topr_g" ></td></tr>
  <tr><td class="boxmidlrg" >

 <!-- tabs -->
 <div class="tab-wrapper">
	<h4 title=""></h4>
	<div>
	<!-- start tab -->

	  	<div class="listdivi"></div>
	   
		  	<div class="listlight">
		  	<!-- input field -->
			<table border="0" cellpadding="0" cellspacing="1" width="100%">
			  <tr>
			  <c:if test="${model.tickets.nrOfElements > 0}">
			  <td class="pageShowing">
			  <fmt:message key="showing">
				<fmt:param value="${model.tickets.firstElementOnPage + 1}"/>
				<fmt:param value="${model.tickets.lastElementOnPage + 1}"/>
				<fmt:param value="${model.tickets.nrOfElements}"/>
			  </fmt:message>
			  </td>
			  </c:if>
			  <td class="pageNavi">
			  <fmt:message key="page" />
			  <select id="page" name="page" onchange="submit()">
			  <c:forEach begin="1" end="${model.tickets.pageCount}" var="page">
			  	<option value="${page}" <c:if test="${page == (model.tickets.page+1)}">selected</c:if>>${page}</option>
			  </c:forEach>
			  </select>
			  <fmt:message key="of" /> <c:out value="${model.tickets.pageCount}"/>
			  | 
			  <c:if test="${model.tickets.firstPage}"><span class="pageNaviDead">previous</span></c:if>
			  <c:if test="${not model.tickets.firstPage}"><a href="<c:url value="ticketList.jhtm"><c:param name="page" value="${model.tickets.page}"/></c:url>" class="pageNaviLink">previous</a></c:if>
			  | 
			  <c:if test="${model.tickets.lastPage}"><span class="pageNaviDead">next</span></c:if>
			  <c:if test="${not model.tickets.lastPage}"><a href="<c:url value="ticketList.jhtm"><c:param name="page" value="${model.tickets.page+2}"/></c:url>" class="pageNaviLink">next</a></c:if>
			  </td>
			  </tr>
			</table>
			
			<table border="0" cellpadding="0" cellspacing="0" width="100%" class="listings">
			  <tr class="listingsHdr2">
			    <td class="listingsHdr3">&nbsp;</td>
			    <td class="listingsHdr3"><fmt:message key="status" /></td>
			    <td class="listingsHdr3"><fmt:message key="ticket" /></td>
			    <td class="listingsHdr3"><fmt:message key="company" /></td>
			    <td class="listingsHdr3"><fmt:message key="subject" /></td>
			    <td align="center">
			    	<table cellspacing="0" cellpadding="1">
			    	  <tr>
			    	    <c:choose>
			    	      <c:when test="${ticketSearch.sort == 'last_name DESC'}">
			    	        <td class="listingsHdr3">
			    	        <a class="hdrLink" href="#" onClick="document.getElementById('sort').value='last_name';document.getElementById('list').submit()"><fmt:message key="customer" /></a>
			    	        </td>
			    	        <td><img src="../graphics/up.gif" border="0"></td>
			    	      </c:when>
			    	      <c:when test="${ticketSearch.sort == 'last_name'}">
			    	        <td class="listingsHdr3">
			    	        <a class="hdrLink" href="#" onClick="document.getElementById('sort').value='last_name DESC';document.getElementById('list').submit()"><fmt:message key="customer" /></a>
			    	        </td>
			    	        <td><img src="../graphics/down.gif" border="0"></td>
			    	      </c:when>
			    	      <c:otherwise>
			    	        <td colspan="2" class="listingsHdr3">
			    	        <a class="hdrLink" href="#" onClick="document.getElementById('sort').value='last_name DESC';document.getElementById('list').submit()"><fmt:message key="customer" /></a>
			    	        </td>
			    	      </c:otherwise>
			    	    </c:choose>  
			    	  </tr>
			    	  </table>
			    </td>
			    <td align="center">
			    	<table cellspacing="0" cellpadding="1">
			    	  <tr>
			    	    <c:choose>
			    	      <c:when test="${ticketSearch.sort == 'ticket.created DESC'}">
			    	        <td class="listingsHdr3">
			    	        <a class="hdrLink" href="#" onClick="document.getElementById('sort').value='ticket.created';document.getElementById('list').submit()"><fmt:message key="created" /></a>
			    	        </td>
			    	        <td><img src="../graphics/up.gif" border="0"></td>
			    	      </c:when>
			    	      <c:when test="${ticketSearch.sort == 'ticket.created'}">
			    	        <td class="listingsHdr3">
			    	        <a class="hdrLink" href="#" onClick="document.getElementById('sort').value='ticket.created DESC';document.getElementById('list').submit()"><fmt:message key="created" /></a>
			    	        </td>
			    	        <td><img src="../graphics/down.gif" border="0"></td>
			    	      </c:when>
			    	      <c:otherwise>
			    	        <td colspan="2" class="listingsHdr3">
			    	        <a class="hdrLink" href="#" onClick="document.getElementById('sort').value='ticket.created DESC';document.getElementById('list').submit()"><fmt:message key="created" /></a>
			    	        </td>
			    	      </c:otherwise>
			    	    </c:choose>  
			    	  </tr>
			    	  </table>
			    </td>
			    <td align="center" class="listingsHdr3"><fmt:message key="lastModified" /></td>
			    <c:if test="${gSiteConfig['gMULTI_STORE'] > 0}">
			    <td class="listingsHdr3"><fmt:message key="multiStore" /></td>
				</c:if>
			  </tr>
			<c:forEach items="${model.tickets.pageList}" var="ticket" varStatus="status">
			  <tr class="row${status.index % 2}" onmouseover="changeStyleClass(this,'rowOver')" onmouseout="changeStyleClass(this,'row${status.index % 2}')">
			    <td class="indexCol">
			    <c:choose>
			      <c:when test="${ticket.versionCreatedByType == 'Admin' or ticket.status == 'close'}"><a href="ticketUpdate.jhtm?id=${ticket.ticketId}"><img src="../graphics/flag_green.gif" border="0"  title="Need Customer Feedback"></a></c:when>
			      <c:when test="${ticket.versionCreatedByType == 'AdminUser'}"><a href="ticketUpdate.jhtm?id=${ticket.ticketId}"><img src="../graphics/flag_yellow.gif" border="0"  title="Requiering Attention"></a></c:when>
			      <c:when test="${ticket.versionCreatedByType == 'User' or empty ticket.versionCreatedByType}"><a href="ticketUpdate.jhtm?id=${ticket.ticketId}"><img src="../graphics/flag_red.gif" border="0"  title="Requiering Attention"></a></c:when>
			    </c:choose></td>
			    <td class="nameCol"><c:out value="${ticket.status}"/></td>
			    <td class="nameCol"><a href="ticketUpdate.jhtm?id=${ticket.ticketId}"><c:out value="${ticket.ticketId}"/></a></td>
			    <td class="nameCol"><c:out value="${ticket.companyName}"/></td>
			    <td class="nameCol"><c:out value="${ticket.subject}"/></td>
			    <td class="nameCol"><span title="${ticket.userId}"><a href="../customers/customer.jhtm?id=${ticket.userId}"><c:out value="${ticket.lastName}"/>, <c:out value="${ticket.firstName}"/></a></span></td>
			    <td align="center"><fmt:formatDate type="both" timeStyle="full" pattern="MM/dd/yyyy hh:mm a" value="${ticket.created}"/></td>
			    <td align="center"><fmt:formatDate type="both" timeStyle="full" pattern="MM/dd/yyyy hh:mm a" value="${ticket.lastModified}"/></td>
			    <c:if test="${gSiteConfig['gMULTI_STORE'] > 0}">
			    <td class="nameCol"><c:out value="${ticket.host}"/></td>
				</c:if>
			  </tr>
			</c:forEach>
			<c:if test="${model.tickets.nrOfElements == 0}">
			  <tr class="emptyList"><td colspan="4">&nbsp;</td></tr>
			</c:if>
			</table>
			
			<table border="0" cellpadding="0" cellspacing="1" width="100%">
			  <tr>
			  <td class="pageSize">
			  <select name="size" onchange="document.getElementById('page').value=1;submit()">
			  <c:forTokens items="10,25,50" delims="," var="current">
			  	  <option value="${current}" <c:if test="${current == model.tickets.pageSize}">selected</c:if>>${current} <fmt:message key="perPage" /></option>
			  </c:forTokens>
			  </select>
			  </td>
			  </tr>
			</table>
		    <!-- end input field -->  	  
		  	
		  	<!-- start button -->
			<!-- end button -->	
	        </div>	  	
	<!-- end tab -->        
	</div>
<!-- end tabs -->			
</div>
  
  <!-- end table -->
   </td><td class="boxmidr" ></td></tr>
  <tr><td class="botl"></td><td class="botr"></td></tr>
  </table>
  
<!-- end main box -->  
</div>
</form>
</c:if>
</sec:authorize>

  </tiles:putAttribute>    
</tiles:insertDefinition>
