<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="/WEB-INF/tlds/spring.tld" prefix="spring" %>
<%@ taglib uri="http://www.springframework.org/security/tags" prefix="sec" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ taglib uri="/WEB-INF/tlds/custom.tld" prefix="wj" %>

<tiles:insertDefinition name="admin.ticket" flush="true">
  <tiles:putAttribute name="content" type="string">
  
<sec:authorize ifAnyGranted="ROLE_AEM,ROLE_ADMIN_${prop_site_id},ROLE_TICKET_UPDATE">
<c:if test="${gSiteConfig['gTICKET']}">	 
<script type="text/javascript"> 
<!--
window.addEvent('domready', function(){			
	var tabs1 = new SimpleTabs($('tab-block-1'), { selector: 'h4'});
});
//--> 
</script> 	  
<!-- main box -->
  <div id="mboxfull">
  
  <!-- start table -->
  <table cellpadding="0" cellspacing="0" border="0" class="module" >
  <tr>
    <td class="topl_g">
	  
	  <!-- breadcrumb -->
	  <p class="breadcrumb">
	    <a href="../ticket">Ticket</a> &gt;
	    ${param.id}
	  </p>
	  
	  <!-- Error Message -->
	  <c:if test="${message != null}">
        <div class="message"><spring:message code="${message}"/></div>
      </c:if>
	  <spring:hasBindErrors name="productForm">
          <div class="message">Please fix all errors!</div>
      </spring:hasBindErrors>  

  
  </td><td class="topr_g" ></td></tr>
  <tr><td class="boxmidlrg" >
<form:form  commandName="ticketForm" action="ticketUpdate.jhtm" method="post" enctype="multipart/form-data">
 <!-- tabs -->
 <div id="tab-block-1">
	<h4 title="ticket">Ticket</h4>
	<div>
	<!-- start tab -->
        <c:set var="classIndex" value="0" />
        
	  	<div class="listdivi ln tabdivi"></div>
	  	<div class="listdivi"></div>

			<div style="border-top:1px dotted #DDDDDD;margin:8px 0 5px 10px; font-weight:700;color:#DB0B2B"><fmt:message key="comments" /> <span class="ticketSubject">/ <c:out value="${ticketForm.ticket.subject}"/></span></div>
			<c:forEach items="${questionAnswerList}" var="ticket" varStatus="status">
			 <c:if test="${status.first}">
			 <c:choose>
			     <c:when test="${ticket.ticket.createdBy == 'Admin'}">
			         <div class="ticketAdmin">
			         <div class="ticketAdminHead">
				     <div><h3><c:out value="${siteConfig['COMPANY_NAME'].value}"/> Said:</h3></div>
				     <i><fmt:formatDate type="date" dateStyle="full" value="${ticket.ticket.created}" pattern="MM/dd/yyyy (hh:mm)a zz "/></i>
				     </div>
				     <div class="ticketAdminBody">
				     <c:out value="${wj:newLineToBreakLine(ticket.ticket.question)}" escapeXml="false"/>
					 <c:if test="${ticket.ticketVersion.attachment != null}">
	     			   <div class="attachmentLink">
	       				 <ul>
	  				       <li><a href="../../temp/Ticket/${ticket.ticket.userId}/${ticket.ticketVersion.attachment}"> <c:out value="${ticket.ticketVersion.attachment}"></c:out> </a></li>
	  				     </ul>  
         			   </div>
	   				 </c:if>
    				 </div></div>
			     </c:when>
			     <c:when test="${ticket.ticket.createdBy == NULL}">
			         <div class="ticketUser">
			         <div class="ticketUserHead">
			           <div style="float:left;">
				         <div><h3><c:out value="${user.address.lastName}"/>, <c:out value="${user.address.firstName}"/> Said:</h3></div>
				         <i><fmt:formatDate type="date" dateStyle="full" value="${ticket.ticket.created}" pattern="MM/dd/yyyy (hh:mm)a zz "/></i>
				       </div>
				       <div style="float:right;margin:8px 10px 0 0;"><c:if test="${user.address.company != NULL}"><a href="../customers/customer.jhtm?id=${user.id}" ><c:out value="${user.address.company}" /></a></c:if></div>
				     </div>
				     <div class="ticketUserBody">
				     <c:out value="${wj:newLineToBreakLine(ticket.ticket.question)}" escapeXml="false"/>
				     <c:if test="${ticketForm.ticketVersion.attachment != null}">
	     			   <div class="attachmentLink">
             			 <ul>
	  				       <li><a href="../../temp/Ticket/customer_${ticket.ticket.userId}/${ticketForm.ticketVersion.attachment}"><c:out value="${ticketForm.ticketVersion.attachment}"></c:out></a></li>
	  				     </ul>  
         			   </div>
         			 </c:if>
					 </div></div>
			     </c:when>
			     <c:otherwise>
			         <div class="ticketAdminUser">
			         <div class="ticketAdminUserHead">
				     <div><h3><c:out value="${ticket.ticket.createdBy}"/> Said:</h3></div>
				     <i><fmt:formatDate type="date" dateStyle="full" value="${ticket.ticket.created}" pattern="MM/dd/yyyy (hh:mm)a zz "/></i>
				     </div>
				     <div class="ticketAdminUserBody">
				     <c:out value="${wj:newLineToBreakLine(ticket.ticket.question)}" escapeXml="false"/>
				     <c:if test="${ticketForm.ticketVersion.attachment != null}">
	     			   <div class="attachmentLink">
             			 <ul>
	  				       <li><a href="../../temp/Ticket/customer_${ticket.ticket.userId}/${ticketForm.ticketVersion.attachment}"><c:out value="${ticketForm.ticketVersion.attachment}"></c:out></a></li>
	  				     </ul>  
         			   </div>
         			 </c:if>
					 </div></div>
			     </c:otherwise>
			 </c:choose>  
			 </c:if>
			 <c:if test="${!empty ticket.ticketVersion.questionDescription}">
			   <div class="ticket${ticket.ticketVersion.createdByType}">
			   <c:choose>
			     <c:when test="${ticket.ticketVersion.createdByType == 'Admin'}">
				     <div class="ticket${ticket.ticketVersion.createdByType}Head"><div>
				     <h3>
				     <c:choose>
				       <c:when test="${ticket.ticketVersion.createdBy == null}"><c:out value="${siteConfig['COMPANY_NAME'].value}"/></c:when>
				       <c:otherwise><c:out value="${ticket.ticketVersion.createdBy}"/></c:otherwise>
				     </c:choose>
                     Said:</h3></div>
				     <i><fmt:formatDate type="date" dateStyle="full" value="${ticket.ticketVersion.created}" pattern="MM/dd/yyyy (hh:mm)a zz "/></i>
				     </div>
				     <div class="ticket${ticket.ticketVersion.createdByType}Body">
				     <c:if test="${ticket.ticketVersion.accessUserId != NULL}">
				       <div><span style="font-size:1.4em;width:100px;">Recipients:</span><span style="padding-left:5px;"><c:out value="${accessUserMap[ticket.ticketVersion.accessUserId].username}" /></span></div><br /><br />
				     </c:if> 
				     <c:out value="${wj:newLineToBreakLine(ticket.ticketVersion.questionDescription)}" escapeXml="false"/>
			         <c:if test="${ticket.ticketVersion.attachment != null}">
	  				   <div class="attachmentLink">
	     			     <ul>
	  				       <li><a href="../../temp/Ticket/customer_${ticket.ticket.userId}/${ticket.ticketVersion.attachment}"> <c:out value="${ticket.ticketVersion.attachment}"></c:out> </a></li>
	  				     </ul>  
      				   </div>
					 </c:if>
				     </div> 
			     </c:when>
			     <c:when test="${ticket.ticketVersion.createdByType == 'User'}">
				     <div class="ticket${ticket.ticketVersion.createdByType}Head"><div><h3>
				       <c:out value="${user.address.lastName}"/>, <c:out value="${user.address.firstName}"/>
				     Said:</h3></div>
				     <i><fmt:formatDate type="date" dateStyle="full" value="${ticket.ticketVersion.created}" pattern="MM/dd/yyyy (hh:mm)a zz "/></i>
				     </div>
				     <div class="ticket${ticket.ticketVersion.createdByType}Body"><br /><br />
				     <c:out value="${wj:newLineToBreakLine(ticket.ticketVersion.questionDescription)}" escapeXml="false"/>
				     <c:if test="${ticket.ticketVersion.attachment != null}">
	  				   <div class="attachmentLink">
	  				     <ul>
	  				       <li><a href="../../temp/Ticket/customer_${ticket.ticket.userId}/${ticket.ticketVersion.attachment}"> <c:out value="${ticket.ticketVersion.attachment}"></c:out> </a></li>
	  				     </ul>
      				   </div>
					 </c:if>
				     </div>
			     </c:when>
			     <c:otherwise>
				     <div class="ticket${ticket.ticketVersion.createdByType}Head"><div><h3>
				       <c:out value="${ticket.ticketVersion.createdBy}" />
				     Said:</h3></div>  
				     <i><fmt:formatDate type="date" dateStyle="full" value="${ticket.ticketVersion.created}" pattern="MM/dd/yyyy (hh:mm)a zz "/></i>
				     </div>
				     <div class="ticket${ticket.ticketVersion.createdByType}Body">
				     <c:if test="${ticket.ticketVersion.accessUserId != NULL}">
				       <div><span style="font-size:1.4em;width:100px;">Recipient:</span><span style="padding-left:5px;"><c:out value="${accessUserMap[ticket.ticketVersion.accessUserId].username}" /></span></div><br /><br />
				     </c:if>
				     <c:out value="${wj:newLineToBreakLine(ticket.ticketVersion.questionDescription)}" escapeXml="false"/>
				     <c:if test="${ticket.ticketVersion.attachment != null}">
	  				   <div class="attachmentLink">
	  				     <ul>
	  				       <li><a href="../../temp/Ticket/customer_${ticket.ticket.userId}/${ticket.ticketVersion.attachment}"> <c:out value="${ticket.ticketVersion.attachment}"></c:out> </a></li>
	  				     </ul>
      				   </div>
					 </c:if>
				     </div>
			     </c:otherwise>
			   </c:choose>
			   </div>
			 </c:if>
			</c:forEach>

	   <c:if test="${ticketForm.ticket.status == 'open' }">
		  	<div class="ticketComment">
		  	<div class="ticketCommentHead">
		  		<div><h3>Comment</h3></div>  
			</div>
			<div class="ticketCommentBody">
			<table cellpadding="0" cellspacing="0" width="100%">
			 <tr valign="top">
			   <td style="width:50%">
			   <div class="ticketCommentTextArea">
				<input type="hidden" name="id" value="<c:out value='${ticketForm.ticket.ticketId}'/>" />
				<form:textarea path="ticketVersion.questionDescription" rows="8" cols="50" htmlEscape="true" />
				<form:errors path="ticketVersion.questionDescription" cssClass="error"/>
			   </div>
			   </td>
			   <c:if test="${siteConfig['TICKET_INTERNAL_ACCESS_USER'].value == 'true' or siteConfig['TICKET_MESSAGE_ON_EMAIL'].value == 'true'}">
			   <td style="width:50%">
			     <c:if test="${siteConfig['TICKET_INTERNAL_ACCESS_USER'].value == 'true'}">
				    <div class="ticketCommentRecepient">
					<span>Recipients:</span>
					<form:select path="ticketVersion.accessUserId" >
					  <form:option value=""><c:out value="${user.address.lastName}"/>, <c:out value="${user.address.firstName}"/></form:option>	
					  <form:option value="0">ALL Users</form:option>	
					  <form:options items="${accessUserList}" itemValue="id" itemLabel="username"/>
					</form:select>
					</div>
				  </c:if>
				  <c:if test="${siteConfig['TICKET_MESSAGE_ON_EMAIL'].value == 'true'}">
				    <div class="ticketCommentOnEmail">
					<span>Comment send via Email:</span>
					<form:checkbox path="onEmail" ></form:checkbox>
					</div>
				  </c:if>		
				</td>
			   </c:if>
			 </tr>
			 <c:if test="${siteConfig['TICKET_ATTACHMENT'].value == 'true'}">
			 <tr class="ticketAttachmentWrapper">
        	   <td>
          		 <fieldset id="ticketAttachment" class="attachment">
          		   <legend><fmt:message key="attachment" /></legend>
          		   <dl>
          		     <dd>
          		       <input value="browse" type="file" name="attachment"/>
          		     </dd>
          		   </dl>
          		 </fieldset>
	    	   </td>
      		 </tr>  
      		 </c:if>
			</table>
		    </div>
		    </div>
	  	</c:if> 	  
	<!-- end tab -->        
	</div> 
	

<!-- end tabs -->			
</div>

<!-- start button -->
<c:if test="${ticketForm.ticket.status == 'open' }">
<div align="left" class="button"> 
  <input type="submit" name="__update_ticket" value="<fmt:message key='updateTicket' />">
  <input type="submit" name="__close_ticket" value="<fmt:message key="closeTicket" />" onClick="return confirm('Close this Ticket permanently?')">
</div>
</c:if>
<!-- end button -->	     
</form:form>  
  <!-- end table -->
    </td><td class="boxmidr" ></td></tr>
  <tr><td class="botl"></td><td class="botr"></td></tr>
  </table>
  
<!-- end main box -->  
</div>
</c:if>
</sec:authorize>

  </tiles:putAttribute>    
</tiles:insertDefinition>