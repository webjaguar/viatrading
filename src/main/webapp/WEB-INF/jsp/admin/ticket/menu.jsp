<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<script type="text/javascript" src="../../admin/javascript/autocompleter1.2.js"></script>
<script type="text/javascript" src="../../admin/javascript/autocompleter.Request1.2.js"></script>
<script type="text/javascript" src="../../admin/javascript/autocompleter.Local1.2.js"></script>
<script type="text/javascript" src="../../admin/javascript/observer.js"></script>
<script type="text/javascript">
window.addEvent('domready', function(){	
	var lastName = $('lastName');
	new Autocompleter.Request.HTML(lastName, '../customers/show-ajax-customers.jhtm', {
		'indicatorClass': 'autocompleter-loading',
		'postData': { 'search': 'lastname' },
		'injectChoice': function(choice) {
			var text = choice.getFirst();
			var value = text.innerHTML;
			choice.inputValue = value;
			text.set('html', this.markQueryValue(value));
			this.addChoiceEvents(choice);
		},
		'onSelection': function(input, selection, input_value, selection_value) {
			$('user_id').setProperty('value', selection.getFirst().getNext().innerHTML);
		}
	});
});	
</script>

  <div id="lbox">
   <table cellpadding="0" cellspacing="0" border="0" class="module" style="width: 100%"><tr><td class="topl">
    </td><td class="topr" ></td></tr><tr><td class="boxmid" >

    <c:if test="${gSiteConfig['gTICKET']}">	 
    <h2 class="menuleft mfirst"><fmt:message key="ticketMenuTitle"/></h2>
	<div class="menudiv"></div> 
      <a href="ticketList.jhtm" class="userbutton"><fmt:message key="ticketMenuTitle"/></a>	  
    <div class="menudiv"></div>
    
    <div class="menudiv"></div> 
      <div align="left" class="searchHeader" id="searchHeaderId"><h2><fmt:message key="search" /></h2></div>
	  <table class=searchBoxLeft><tr><td>
	  <form action="ticketList.jhtm" name="searchform" method="post">
	  <!-- content area -->
	    <div class="search2">
	      <p><fmt:message key="lastName"/>:</p>
	      <input name="lastName" id="lastName" type="text" value="" style="width:60px !important" />
	      <input name="user_id" id="user_id" type="text"   <c:if test="${ticketSearch.userId > 0}">value="<c:out value='${ticketSearch.userId}' />" </c:if>style="margin-left:5px; width:40px !important"/><br />
	    </div> 
	    <div class="search2">
	      <p><fmt:message key="company"/>:</p>
	      <input name="company" type="text" value="<c:out value='${ticketSearch.company}' />" size="15" />
	    </div>    
	    <div class="search2">
	      <p><fmt:message key="subject"/>:</p>
	      <input name="subject" type="text" value="<c:out value='${ticketSearch.subject}' />" size="15" />
	    </div> 
	    <div class="search2">
	      <p><fmt:message key="number"/>:</p>
	      <input name="ticket_id" type="text" value="<c:out value='${ticketSearch.ticketId}' />" size="15" />
	    </div>     
	    <div class="search2">
	      <p><fmt:message key="status"/>:</p>
	      <select name="status">
	        <option value="open" <c:if test="${ticketSearch.status == 'open'}">selected</c:if>><fmt:message key="open"/></option>
	        <option value="close" <c:if test="${ticketSearch.status == 'close'}">selected</c:if>><fmt:message key="close"/></option> 
	        <option value="" <c:if test="${ticketSearch.status == ''}">selected</c:if>><fmt:message key="all"/></option> 
	      </select>
	    </div>
	    <div class="search2">
	      <p><fmt:message key="flag"/>:</p>
	      <select name="version_createdby_type" id="ticketFlag">
	        <option value="" <c:if test="${ticketSearch.versionCreatedByType == ''}">selected</c:if>><fmt:message key="all"/></option> 
	        <option value="Admin" <c:if test="${ticketSearch.versionCreatedByType == 'Admin'}">selected</c:if>>Need Customer Feedback</option>
	        <c:if test="${siteConfig['TICKET_INTERNAL_ACCESS_USER'].value == 'true'}">
	        <option value="AdminUser" <c:if test="${ticketSearch.versionCreatedByType == 'AdminUser'}">selected</c:if>>Require Attention(User Replied)</option>
	        </c:if> 
	        <option value="User" <c:if test="${ticketSearch.versionCreatedByType == 'User'}">selected</c:if>>Require Attention</option>
	      </select>
	    </div>  
	    <div class="button">
	      <input type="submit" value="Search"/>
	    </div>
	  <!-- content area -->
	  </form>
	  </td></tr></table>
    <div class="menudiv"></div>  
    </c:if> 
    
    </td><td class="boxmidr" ></td></tr>
    <tr><td class="botl"></td><td class="botr"></td></tr></table>
  </div>