<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/security/tags" prefix="sec" %>


<!-- menu -->
  <div id="lbox">
   <table cellpadding="0" cellspacing="0" border="0" class="module" style="width: 100%"><tr><td class="topl">
    </td><td class="topr" ></td></tr><tr><td class="boxmid" >
  
    <h2 class="menuleft mfirst"><fmt:message key="emailManager"/></h2>
    <div class="menudiv"></div>
    <sec:authorize ifAnyGranted="ROLE_AEM,ROLE_ADMIN_${prop_site_id},ROLE_EMAILMANAGER_VIEW_HISTORY">
    <a href="massEmailList.jhtm" class="userbutton">History <fmt:message key="list"/></a>
    </sec:authorize>
    <a href="../../admin/customers/?unsubscribe=1" class="userbutton"><fmt:message key="unsubscribeEmails"/></a>
    <sec:authorize ifAnyGranted="ROLE_AEM,ROLE_ADMIN_${prop_site_id},ROLE_EMAILMANAGER_BUY_CREDIT">
    <a href="buyCreditList.jhtm" class="userbutton"><fmt:message key="buyCredit"/> <fmt:message key="list"/></a>
    <a href="buyCredit.jhtm" class="userbutton"><fmt:message key="buyCredit"/></a>
    </sec:authorize>
    <div class="menudiv"></div>
	
    </td><td class="boxmidr" ></td></tr>
    <tr><td class="botl"></td><td class="botr"></td></tr></table>
  </div>