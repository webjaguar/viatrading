<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/security/tags" prefix="sec" %>

<tiles:insertDefinition name="admin.massEmail" flush="true">
  <tiles:putAttribute name="content" type="string">
  
<sec:authorize ifAnyGranted="ROLE_AEM,ROLE_ADMIN_${prop_site_id},ROLE_EMAILMANAGER_VIEW_HISTORY">
<script type="text/javascript">
<!--
window.addEvent('domready', function(){
	$$('img.action').each(function(img){
		//containers
		var actionList = img.getParent(); 
		var actionHover = actionList.getElements('div.action-hover')[0];
		actionHover.set('opacity',0);
		//show/hide
		img.addEvent('mouseenter',function() {
			actionHover.setStyle('display','block').fade('in');
		});
		actionHover.addEvent('mouseleave',function(){
			actionHover.fade('out');
		});
		actionList.addEvent('mouseleave',function() {
			actionHover.fade('out');
		});
	});
	var box2 = new multiBox('mbContact', {descClassName: 'multiBoxDesc',waitDuration: 5,showControls: false,overlay: new overlay()});
	// Create the accordian
	var Tips1 = new Tips('.toolTipImg');  
	// Create the accordian
	var myAccordion = new Fx.Accordion($$('div.arrow_drop'), $$('div.information'), {
		display: 1, alwaysHide: true,
    	onActive: function() {$('information').removeClass('displayNone');}
	});
});	    

function toggleAll(el) {
	var ids = document.getElementsByName("__selected_id");	
  	if (ids.length == 1)
      document.list.__selected_id.checked = el.checked;
    else
      for (i = 0; i < ids.length; i++)
        document.list.__selected_id[i].checked = el.checked;	
} 

function optionsSelected() {
	var ids = document.getElementsByName("__selected_id");	
  	if (ids.length == 1) {
      if (document.list.__selected_id.checked) {
    	return true;
      }
    } else {
      for (i = 0; i < ids.length; i++) {
        if (document.list.__selected_id[i].checked) {
    	  return true;
        }
      }
    }
  	if (document.list.__groupAssignAll.checked) {
   	   return true;
     }
  	alert("Please select contact(s).");      
    return false;
} 	
//-->
</script>
<form name="list" id="list" action="">
<input type="hidden" id="sort" name="sort" value="${openMassEmailDetailSearch.sort}" />
<input type="hidden" name="campaignId" value="${model.campaignId}">

<!-- main box -->
  <div id="mboxfull">
  
  <!-- start table -->
  <table cellpadding="0" cellspacing="0" border="0" class="module" >
  <tr>
    <td class="topl_g">
	  
	  <!-- breadcrumb -->
	  <p class="breadcrumb">
	    <a href="../massEmail/"><fmt:message key="massEmail" /></a>  &gt; 
	    <fmt:message key="openDetail" />
	  </p>
	  
	  <!-- Error -->
	  <c:if test="${!empty model.message}">
		<div class="message"><fmt:message key="${model.message}" /></div>
	  </c:if>
	  
	  <!-- header image -->
	  <img class="headerImage" src="../graphics/email40x40.gif" alt=""/> 
	  <!--Options-->
	  <div class="optionBox"><div class="titlePrivilege"><fmt:message key="options" /></div></div>
		<div class="arrow_drop arrowImageRight"></div>
		<div style="clear: both;"></div>
		<div class="information displayNone" id="information" style="float:left;">    
			 <div style="float:left;padding: 5px">
	           <table class="productBatchTool" cellpadding="5" cellspacing="5">
	           <tr>
	            <td valign="top">
	             <table cellpadding="1" cellspacing="1" width="100%">
	              <tr style="height:30px;white-space: nowrap">
	               <td valign="top" width="20px"><img class="toolTipImg" title="Group Id::Associate group Id to the following contact(s) in the list" src="../graphics/question.gif" /></td>
	               <td valign="top"><h3><fmt:message key="groupId" /></h3></td>
	               <td valign="top" align="right"><input type="checkbox" onclick="toggleAll(this)" name="__groupAssignAll" value="true" /> <fmt:message key="all"/></td>
	              </tr>
	             </table>
	            </td>
	           </tr>
	           <tr>
	            <td valign="top">
	             <table cellpadding="1" cellspacing="1">
	              <tr style="">
			       <td>
			        <input name="__group_id" type="text" size="15"/>
			        <div align="left" class="buttonLeft">
		            <input type="submit" name="__groupAssignPacher" value="Apply" onClick="return optionsSelected()">
		            </div>
			       </td>
			       <td >
			        <div><p><input name="__batch_type" type="radio" checked="checked" value="add"/>Add</p></div>
				    <div><p><input name="__batch_type" type="radio" value="remove"/>Remove</p></div>
				    <div><p><input name="__batch_type" type="radio" value="move"/>Move</p></div>
			       </td>
			     </tr>
	             </table>
	            </td>
	           </tr>
	           </table>
	         </div>
	       </div>
	  
  </td><td class="topr_g" ></td></tr>
  <tr><td class="boxmidlrg" >

 <!-- tabs -->
 <div class="tab-wrapper">
	<h4 title=""></h4>
	<div>
	<!-- start tab -->
	
	  	<div class="listdivi ln tabdivi"></div>
	  	<div class="listdivi"></div>
	    
		  	<div class="listlight">
		  	<!-- input field -->
		  	
		  		<table border="0" cellpadding="0" cellspacing="0" width="100%">
				<tr>
					<td>&nbsp;
					</td>
					<c:if test="${model.openDetailsList.nrOfElements > 0}">
						<td class="pageShowing">
							<fmt:message key="showing">
								<fmt:param value="${model.openDetailsList.firstElementOnPage + 1}" />
								<fmt:param value="${model.openDetailsList.lastElementOnPage + 1}" />
								<fmt:param value="${model.openDetailsList.nrOfElements}" />
							</fmt:message>
						</td>
					</c:if>
					<td class="pageNavi">
						<fmt:message key="page" />
						<select name="page" id="page" onchange="submit()">
							<c:forEach begin="1" end="${model.openDetailsList.pageCount}"
								var="page">
								<option value="${page}"
									<c:if test="${page == (model.openDetailsList.page+1)}">selected</c:if>>
									${page}
								</option>
							</c:forEach>
						</select> 
						<fmt:message key="of" />
						<c:out value="${model.openDetailsList.pageCount}" /> 
						|
						<c:if test="${model.openDetailsList.firstPage}">
							<span class="pageNaviDead"><fmt:message key="previous" /></span>
						</c:if>
						<c:if test="${not model.openDetailsList.firstPage}">
							<a
								href="<c:url value="../massEmail/openDetailList.jhtm"><c:param name="page" value="${model.openDetailsList.page}"/><c:param name="size" value="${model.openDetailsList.pageSize}"/><c:param name="campaignId" value="${model.campaignId}"/></c:url>"
								class="pageNaviLink"><fmt:message key="previous" /></a>
						</c:if>
						|
						<c:if test="${model.openDetailsList.lastPage}">
							<span class="pageNaviDead"><fmt:message key="next" /></span>
						</c:if>
						<c:if test="${not model.openDetailsList.lastPage}">
							<a
								href="<c:url value="../massEmail/openDetailList.jhtm"><c:param name="page" value="${model.openDetailsList.page+2}"/><c:param name="size" value="${model.openDetailsList.pageSize}"/><c:param name="campaignId" value="${model.campaignId}"/></c:url>"
								class="pageNaviLink"><fmt:message key="next" /></a>
						</c:if>
					</td>
				</tr>
			</table>
		  	
		  	
		  	<table border="0" cellpadding="0" cellspacing="0" width="100%" class="listings">
			  <tr class="listingsHdr2">
			  <td align="left"><input type="checkbox" onclick="toggleAll(this)"></td>
			  <td >&nbsp;</td>
			   <td class="listingsHdr3">
			    	<fmt:message key="username"/>
			   </td>
			   <td align="center">
			    	<table cellspacing="0" cellpadding="1">
			    	  <tr>
			    	    <c:choose>
			    	      <c:when test="${openMassEmailDetailSearch.sort == 'opened_count DESC'}">
			    	        <td class="listingsHdr3">
			    	        <a class="hdrLink" href="#" onClick="document.getElementById('sort').value='opened_count';document.getElementById('list').submit()"><fmt:message key="openedCount" /></a>
			    	        </td>
			    	        <td><img src="../graphics/up.gif" border="0"></td>
			    	      </c:when>
			    	      <c:when test="${openMassEmailDetailSearch.sort == 'openedCount'}">
			    	        <td class="listingsHdr3">
			    	        <a class="hdrLink" href="#" onClick="document.getElementById('sort').value='opened_count DESC';document.getElementById('list').submit()"><fmt:message key="openedCount" /></a>
			    	        </td>
			    	        <td><img src="../graphics/down.gif" border="0"></td>
			    	      </c:when>
			    	      <c:otherwise>
			    	        <td colspan="2" class="listingsHdr3">
			    	        <a class="hdrLink" href="#" onClick="document.getElementById('sort').value='opened_count DESC';document.getElementById('list').submit()"><fmt:message key="openedCount" /></a>
			    	        </td>
			    	      </c:otherwise>
			    	    </c:choose>  
			    	  </tr>
			    	</table>
			    </td>		    
			   <td class="listingsHdr3">
			    	<fmt:message key="lastOpenedDate"/>
			    </td>
			    <td class="listingsHdr3">
			    	<fmt:message key="salesRepTabTitle"/>
			    </td>
			  </tr>
		  	  <c:forEach items="${model.openDetailsList.pageList}" var="openDetail" varStatus="status">
			  <tr class="row${status.index % 2}" onmouseover="changeStyleClass(this,'rowOver')" onmouseout="changeStyleClass(this,'row${status.index % 2}')">
			  	<td align="left"><input name="__selected_id" value="${openDetail.contactId}" type="checkbox"></td>
			    <td class="indexCol"><c:out value="${status.count + model.campaign.firstElementOnPage}"/>.</td>
			    <td class="nameColCenter"><c:out value="${openDetail.email}"/></td>
			    <td class="nameColCenter"><c:out value="${openDetail.openedCount}"/></td>
			    <td class="nameColCenter"><fmt:formatDate type="date" timeStyle="default" value="${openDetail.date}"/></td>
			    <td class="nameColCenter">
			    <c:choose>
			    <c:when test="${openDetail.customerSalesRepName != null}"><c:out value="${openDetail.customerSalesRepName}"/></c:when>
			    <c:otherwise><c:out value="${openDetail.contactSalesRepName}"/></c:otherwise>
			    </c:choose>
			    </td>		
			  </tr>
			  </c:forEach>
			</table>
			
			<table border="0" cellpadding="0" cellspacing="1" width="100%">
			  <tr>
			  <td>&nbsp;</td>  
			  <td class="pageSize">
			  <select name="size" onchange="document.getElementById('page').value=1;submit()">
			    <c:forTokens items="10,25,50,100,200,500" delims="," var="current">
			  	  <option value="${current}" <c:if test="${current == model.openDetailsList.pageSize}">selected</c:if>>${current} <fmt:message key="perPage" /></option>
			    </c:forTokens>
			  </select>
			  </td>
			  </tr>
			</table>	  
		    <!-- end input field -->  	  
		  	</div>
		  	
		  	<!-- start button -->
			<!-- end button -->	
       	  	
	<!-- end tab -->        
	</div>
<!-- end tabs -->			
</div>
  
  <!-- end table -->
   </td><td class="boxmidr" ></td></tr>
  <tr><td class="botl"></td><td class="botr"></td></tr>
  </table>
  
<!-- end main box -->  
</div>
</form>
</sec:authorize>

  </tiles:putAttribute>
</tiles:insertDefinition>
