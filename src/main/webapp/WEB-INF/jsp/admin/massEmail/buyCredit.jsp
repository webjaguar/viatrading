<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="/WEB-INF/tlds/spring.tld" prefix="spring" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ taglib uri="http://www.springframework.org/security/tags" prefix="sec" %>

<tiles:insertDefinition name="admin.massEmail" flush="true">
  <tiles:putAttribute name="content" type="string">
  
<sec:authorize ifAnyGranted="ROLE_AEM,ROLE_ADMIN_${prop_site_id},ROLE_EMAILMANAGER_BUY_CREDIT">
<script type="text/javascript">
<!--
window.addEvent('domready', function(){			
	var tabs1 = new SimpleTabs($('tab-block-1'), { selector: 'h4'});
	var Tips1 = new Tips($$('.toolTipImg'));
});
//-->
</script>

<!-- main box -->
  <div id="mboxfull">
  
  <!-- start table -->
  <table cellpadding="0" cellspacing="0" border="0" class="module" >
  <tr>
    <td class="topl_g">
	  
	  <!-- breadcrumb -->
	  <p class="breadcrumb">
	    <a href="../faq">Mass Email</a> &gt;
	    form 
	  </p>
	  
	  <!-- Error Message -->
	  <c:if test="${!empty message}">
		  <div class="message"><spring:message code="${message}" /></div>
	  </c:if>
	  <spring:hasBindErrors name="buyCreditForm">
       <span class="error">Please fix all errors!</span>
      </spring:hasBindErrors>

  </td><td class="topr_g" ></td></tr>
  <tr><td class="boxmidlrg" >
<form:form commandName="buyCreditForm" method="post" name="buyCreditForm">  
<input type="hidden" name="_backend_user" value="<sec:authentication property="principal.username"/>" />
 <!-- tabs -->
 <div id="tab-block-1">
	<h4 title="<fmt:message key="buyCredit"/>"><fmt:message key="buyCredit"/><sec:authorize ifAllGranted="ROLE_AEM"><span style="color: #FFA10F;"> (AEM Only)</span></sec:authorize></h4>
	<div>
	<!-- start tab -->

         <c:set var="classIndex" value="0" />
         
	  	<div class="listdivi ln tabdivi"></div>
	  	<div class="listdivi"></div>
	   
		  	<div class="list${classIndex % 2}"><c:set var="classIndex" value="${classIndex+1}" />
		  	<div class="listfl"><div class="requiredField"><fmt:message key="availablePoint" />:</div></div>
		  	<div class="listp">
		  	<!-- input field -->
				<c:out value="${model.point}" />
	 		<!-- end input field -->	
	 		</div>
		  	</div>
		  	
		  	<div class="list${classIndex % 2}">
		  	<div class="listfl">Pricing:</div>
		  	<div class="listp">
		  	<!-- input field -->
				<table width="100%">
                    <tr>
                        <td class="formName"> Number of emails </td>
                        <td> Cost per email </td>
                    </tr>
                    <tr>
                        <td class="formName"> 1 -- 1000 </td>
                        <td>  0.01 </td>
                    </tr>
                    <tr>
                        <td class="formName"> 1001 -- 5000 </td>
                        <td> 0.0085 </td>
                    </tr>
                    <tr>
                        <td class="formName"> 5001 -- 25000 </td>
                        <td>  0.0070 </td>
                    <tr>
                    </tr>
                        <td class="formName"> 25001 -- 100000 </td>
                        <td>  0.0055 </td>
                    </tr>
                    <tr>
                        <td class="formName"> 100001 and over </td>
                        <td> 0.0040 </td>
                    </tr>
                </table>
	 		<!-- end input field -->	
	 		</div>
		  	</div>	

		  	
		  	<div class="list${classIndex % 2}"><c:set var="classIndex" value="${classIndex+1}" />
		  	<div class="listfl"><div class="requiredField">Number Email(s):</div></div>
		  	<div class="listp">
		  	<!-- input field -->
				<form:input path="point" cssClass="textfield30"/>
				<form:errors path="point" cssClass="error"/>
	 		<!-- end input field -->	
	 		</div>
		  	</div>
		  	
		  	<c:if test="${!buyCreditForm.aemUser}" >
		  	<div class="list${classIndex % 2}"><c:set var="classIndex" value="${classIndex+1}" />
		  	<div class="listfl">Billing Address Info:</div>
		  	<div class="listp">
		  	<!-- input field -->
	           
		    <!-- end input field -->   	
		  	</div>
		  	</div>
		  	
		  	<div class="list${classIndex % 2}"><c:set var="classIndex" value="${classIndex+1}" />
		  	<div class="listfl"><div class="requiredField"><fmt:message key='firstName' />:</div></div>
		  	<div class="listp">
		  	<!-- input field -->
			    <form:input path="billing.firstName" cssClass="textfield30"/>
				<form:errors path="billing.firstName" cssClass="error"/>
		    <!-- end input field -->  	  
		  	</div>
			</div>
			
			<div class="list${classIndex % 2}"><c:set var="classIndex" value="${classIndex+1}" />
		  	<div class="listfl"><div class="requiredField"><fmt:message key='lastName' />:</div></div>
		  	<div class="listp">
		  	<!-- input field -->
	           <form:input path="billing.lastName" cssClass="textfield30"/>
			   <form:errors path="billing.lastName" cssClass="error"/>
		    <!-- end input field -->   	
		  	</div>
		  	</div>
		  	
		  	<div class="list${classIndex % 2}"><c:set var="classIndex" value="${classIndex+1}" />
		  	<div class="listfl"><div class="requiredField"><fmt:message key='address' /> 1:</div></div>
		  	<div class="listp">
		  	<!-- input field -->
	           <form:input path="billing.addr1" cssClass="textfield"/>
			   <form:errors path="billing.addr1" cssClass="error"/>
		    <!-- end input field -->   	
		  	</div>
		  	</div>
		  	
		  	<div class="list${classIndex % 2}"><c:set var="classIndex" value="${classIndex+1}" />
		  	<div class="listfl"><fmt:message key='address' /> 2:</div>
		  	<div class="listp">
		  	<!-- input field -->
	           <form:input path="billing.addr2" cssClass="textfield"/>
			   <form:errors path="billing.addr2" cssClass="error"/>
		    <!-- end input field -->   	
		  	</div>
		  	</div>
		  	
		  	<div class="list${classIndex % 2}"><c:set var="classIndex" value="${classIndex+1}" />
		  	<div class="listfl"><div class="requiredField"><fmt:message key='city' />:</div></div>
		  	<div class="listp">
		  	<!-- input field -->
	           <form:input path="billing.city" cssClass="textfield200"/>
			   <form:errors path="billing.city" cssClass="error"/>
		    <!-- end input field -->   	
		  	</div>
		  	</div>
		  	
		  	<div class="list${classIndex % 2}"><c:set var="classIndex" value="${classIndex+1}" />
		  	<div class="listfl"><div class="requiredField"><fmt:message key='state' />:</div></div>
		  	<div class="listp">
		  	<!-- input field -->
	           <form:select path="billing.stateProvince" cssClass="textfield200">
	            <form:option value="" label="--Please Select--"/>
	            <form:options items="${model.states}" itemValue="code" itemLabel="name"/>
	           </form:select>
			   <form:errors path="billing.stateProvince" cssClass="error"/>
		    <!-- end input field -->   	
		  	</div>
		  	</div>
		  	
		  	<div class="list${classIndex % 2}"><c:set var="classIndex" value="${classIndex+1}" />
		  	<div class="listfl"><div class="requiredField"><fmt:message key='zipcode' />:</div></div>
		  	<div class="listp">
		  	<!-- input field -->
	           <form:input path="billing.zip" cssClass="textfield200"/>
			   <form:errors path="billing.zip" cssClass="error"/>
		    <!-- end input field -->   	
		  	</div>
		  	</div>
		  	
		  	<div class="list${classIndex % 2}"><c:set var="classIndex" value="${classIndex+1}" />
		  	<div class="listfl"><div class="requiredField"><fmt:message key='country' />:</div></div>
		  	<div class="listp">
		  	<!-- input field -->
	           <form:select path="billing.country" cssClass="textfield200">
	            <form:option value="" label="--Please Select--"/>
	            <form:options items="${model.countries}" itemValue="code" itemLabel="name"/>
	           </form:select>
			   <form:errors path="billing.country" cssClass="error"/>
		    <!-- end input field -->   	
		  	</div>
		  	</div>
		  	
		  	<div class="list${classIndex % 2}"><c:set var="classIndex" value="${classIndex+1}" />
		  	<div class="listfl"><div class="requiredField"><fmt:message key='phone' />:</div></div>
		  	<div class="listp">
		  	<!-- input field -->
	           <form:input path="billing.phone" cssClass="textfield200"/>
			   <form:errors path="billing.phone" cssClass="error"/>
		    <!-- end input field -->   	
		  	</div>
		  	</div>
		  	
		  	<div class="list${classIndex % 2}"><c:set var="classIndex" value="${classIndex+1}" />
		  	<div class="listfl"><div class="requiredField"><fmt:message key='email' />:</div></div>
		  	<div class="listp">
		  	<!-- input field -->
	           <form:input path="billing.email" cssClass="textfield"/>
			   <form:errors path="billing.email" cssClass="error"/>
		    <!-- end input field -->   	
		  	</div>
		  	</div>
		  	</c:if>
		  	
		  	<div class="list${classIndex % 2}"><c:set var="classIndex" value="${classIndex+1}" />
		  	<div class="listfl"><fmt:message key='note' />:</div>
		  	<div class="listp">
		  	<!-- input field -->
	           <form:input path="note" cssClass="textfield" maxlength="100"/>
			   <form:errors path="note" cssClass="error"/>
		    <!-- end input field -->   	
		  	</div>
		  	</div>
		  	
		  	<c:if test="${!buyCreditForm.aemUser}" >
		  	<div class="list${classIndex % 2}"><c:set var="classIndex" value="${classIndex+1}" />
		  	<div class="listfl">Credit Card Info:</div>
		  	<div class="listp">
		  	<!-- input field -->
	           
		    <!-- end input field -->   	
		  	</div>
		  	</div>
		  	
		  	<div class="list${classIndex % 2}"><c:set var="classIndex" value="${classIndex+1}" />
		  	<div class="listfl"><div class="requiredField"><fmt:message key='cardType' />:</div></div>
		  	<div class="listp">
		  	<!-- input field -->
	           <form:select path="creditCard.type" cssClass="textfield200">
	            <form:option value="VISA">VISA</form:option>
	            <form:option value="MAST">MAST</form:option>
	            <form:option value="DISC">DISC</form:option>
	            <form:option value="AMEX">AMEX</form:option>
	           </form:select>
			   <form:errors path="creditCard.type" cssClass="error"/>
		    <!-- end input field -->   	
		  	</div>
		  	</div>
		  	
		  	<div class="list${classIndex % 2}"><c:set var="classIndex" value="${classIndex+1}" />
		  	<div class="listfl"><div class="requiredField"><fmt:message key='cardNumber' />:</div></div>
		  	<div class="listp">
		  	<!-- input field -->
	           <form:input path="creditCard.number" cssClass="textfield200" maxlength="16"/>
			   <form:errors path="creditCard.number" cssClass="error"/>
		    <!-- end input field -->   	
		  	</div>
		  	</div>
		  	
		  	<div class="list${classIndex % 2}"><c:set var="classIndex" value="${classIndex+1}" />
		  	<div class="listfl"><div class="requiredField"><fmt:message key='verificationCode' />:</div></div>
		  	<div class="listp">
		  	<!-- input field -->
	           <form:input path="creditCard.cardCode" cssClass="textfield200"/>
			   <form:errors path="creditCard.cardCode" cssClass="error"/>
		    <!-- end input field -->   	
		  	</div>
		  	</div>
		  	
		  	<div class="list${classIndex % 2}"><c:set var="classIndex" value="${classIndex+1}" />
		  	<div class="listfl"><div class="requiredField">Exp Date (MM/YY):</div></div>
		  	<div class="listp">
		  	<!-- input field -->
	           <form:select path="creditCard.expireMonth" cssClass="textfield30">
	            <form:option value="01">1 Jan</form:option>
	            <form:option value="02">2 Feb</form:option>
	            <form:option value="03">3 Mar</form:option>
	            <form:option value="04">4 Apr</form:option>
	            <form:option value="05">5 May</form:option>
	            <form:option value="06">6 June</form:option>
	            <form:option value="07">7 July</form:option>
	            <form:option value="08">8 Aug</form:option>
	            <form:option value="09">9 Sept</form:option>
	            <form:option value="10">10 Oct</form:option>
	            <form:option value="11">11 Nov</form:option>
	            <form:option value="12">12 Dec</form:option>
	           </form:select>
			   <form:errors path="creditCard.expireMonth" cssClass="error"/>
			   <form:select path="creditCard.expireYear" cssClass="textfield30">
	            <c:forEach items="${model.years}" var="year" varStatus="status">
	         	 <option value="${year}" <c:if test="${buyCreditForm.creditCard.expireYear == year}">selected</c:if> > ${year}</option>
	       		</c:forEach>
	           </form:select>
			   <form:errors path="creditCard.expireYear" cssClass="error"/>
		    <!-- end input field -->   	
		  	</div>
		  	</div>
			</c:if>
		  	
	<!-- end tab -->        
	</div>         

<!-- end tabs -->			
</div>

<!-- start button -->
	<div align="left" class="button">
	  <input type="submit" value="<fmt:message key="cancel"/>" name="_cancel">
	  <input type="submit" value="<fmt:message key="buy"/>">
	</div>
<!-- end button -->
</form:form>  
  <!-- end table -->
    </td><td class="boxmidr" ></td></tr>
  <tr><td class="botl"></td><td class="botr"></td></tr>
  </table>
  
<!-- end main box -->  
</div>

</sec:authorize> 

  </tiles:putAttribute>    
</tiles:insertDefinition>