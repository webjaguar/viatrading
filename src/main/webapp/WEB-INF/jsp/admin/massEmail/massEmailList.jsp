<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/security/tags" prefix="sec" %>

<tiles:insertDefinition name="admin.massEmail" flush="true">
  <tiles:putAttribute name="content" type="string">
  
<sec:authorize ifAnyGranted="ROLE_AEM,ROLE_ADMIN_${prop_site_id},ROLE_EMAILMANAGER_VIEW_HISTORY">
<script type="text/javascript">
<!--
window.addEvent('domready', function(){
	$$('img.action').each(function(img){
		//containers
		var actionList = img.getParent(); 
		var actionHover = actionList.getElements('div.action-hover')[0];
		actionHover.set('opacity',0);
		//show/hide
		img.addEvent('mouseenter',function() {
			actionHover.setStyle('display','block').fade('in');
		});
		actionHover.addEvent('mouseleave',function(){
			actionHover.fade('out');
		});
		actionList.addEvent('mouseleave',function() {
			actionHover.fade('out');
		});
	});
});
//-->
</script>
<form name="list" id="list" action="">
<input type="hidden" id="sort" name="sort" value="${campaignFilter.sort}" />
<!-- main box -->
  <div id="mboxfull">
  
  <!-- start table -->
  <table cellpadding="0" cellspacing="0" border="0" class="module" >
  <tr>
    <td class="topl_g">
	  
	  <!-- breadcrumb -->
	  <p class="breadcrumb">
	    <a href="../config"><fmt:message key="massEmail" /></a> &gt;
	    <fmt:message key="list" />
	  </p>
	  
	  <!-- Error -->
	  <c:if test="${!empty model.message}">
		<div class="message"><fmt:message key="${model.message}" /></div>
	  </c:if>
	  
	  <!-- header image -->
	  <img class="headerImage" src="../graphics/email40x40.gif" alt=""/> 
	  
  </td><td class="topr_g" ></td></tr>
  <tr><td class="boxmidlrg" >

 <!-- tabs -->
 <div class="tab-wrapper">
	<h4 title=""></h4>
	<div>
	<!-- start tab -->
	
	  	<div class="listdivi"></div>
	    
		  	<div class="listlight">
		  	<!-- input field -->
		  	
		  	<table border="0" cellpadding="0" cellspacing="1" width="100%">
				<tr>
					<td>&nbsp;
					</td>
					<c:if test="${model.compaignList.nrOfElements > 0}">
						<td class="pageShowing">
							<fmt:message key="showing">
								<fmt:param value="${model.compaignList.firstElementOnPage + 1}" />
								<fmt:param value="${model.compaignList.lastElementOnPage + 1}" />
								<fmt:param value="${model.compaignList.nrOfElements}" />
							</fmt:message>
						</td>
					</c:if>
					<td class="pageNavi">
						<fmt:message key="page" />
						<select name="page" id="page" onchange="submit()">
							<c:forEach begin="1" end="${model.compaignList.pageCount}"
								var="page">
								<option value="${page}"
									<c:if test="${page == (model.compaignList.page+1)}">selected</c:if>>
									${page}
								</option>
							</c:forEach>
						</select> 
						<fmt:message key="of" />
						<c:out value="${model.compaignList.pageCount}" /> 
						|
						<c:if test="${model.compaignList.firstPage}">
							<span class="pageNaviDead"><fmt:message key="previous" /></span>
						</c:if>
						<c:if test="${not model.compaignList.firstPage}">
							<a
								href="<c:url value="../massEmail/massEmailList.jhtm"><c:param name="page" value="${model.compaignList.page}"/><c:param name="size" value="${model.compaignList.pageSize}"/></c:url>"
								class="pageNaviLink"><fmt:message key="previous" /></a>
						</c:if>
						|
						<c:if test="${model.compaignList.lastPage}">
							<span class="pageNaviDead"><fmt:message key="next" /></span>
						</c:if>
						<c:if test="${not model.compaignList.lastPage}">
							<a
								href="<c:url value="../massEmail/massEmailList.jhtm"><c:param name="page" value="${model.compaignList.page+2}"/><c:param name="size" value="${model.compaignList.pageSize}"/></c:url>"
								class="pageNaviLink"><fmt:message key="next" /></a>
						</c:if>
					</td>
				</tr>
			</table>
		  	
		  	
		  	<table border="0" cellpadding="2" cellspacing="0" width="100%" class="listings">
			  <tr class="listingsHdr2">
			  <td class="indexCol">&nbsp;</td>
			   <td class="listingsHdr3">
			    	<table cellspacing="0" cellpadding="1">
			    	  <tr>
			    	    <c:choose>
			    	      <c:when test="${campaignFilter.sort == 'sender DESC'}">
			    	        <td class="listingsHdr3">
			    	        <a class="hdrLink" href="#" onClick="document.getElementById('sort').value='sender';document.getElementById('list').submit()"><fmt:message key="sender" /></a>
			    	        </td>
			    	        <td><img src="../graphics/up.gif" border="0"></td>
			    	      </c:when>
			    	      <c:when test="${campaignFilter.sort == 'sender'}">
			    	        <td class="listingsHdr3">
			    	        <a class="hdrLink" href="#" onClick="document.getElementById('sort').value='sender DESC';document.getElementById('list').submit()"><fmt:message key="sender" /></a>
			    	        </td>
			    	        <td><img src="../graphics/down.gif" border="0"></td>
			    	      </c:when>
			    	      <c:otherwise>
			    	        <td colspan="2" class="listingsHdr3">
			    	        <a class="hdrLink" href="#" onClick="document.getElementById('sort').value='sender DESC';document.getElementById('list').submit()"><fmt:message key="sender" /></a>
			    	        </td>
			    	      </c:otherwise>
			    	    </c:choose>  
			    	  </tr>
			    	</table>
			   </td>
			   <td class="listingsHdr3">
			    	<table cellspacing="0" cellpadding="1">
			    	  <tr>
			    	    <c:choose>
			    	      <c:when test="${campaignFilter.sort == 'subject DESC'}">
			    	        <td class="listingsHdr3">
			    	        <a class="hdrLink" href="#" onClick="document.getElementById('sort').value='subject';document.getElementById('list').submit()"><fmt:message key="subject" /></a>
			    	        </td>
			    	        <td><img src="../graphics/up.gif" border="0"></td>
			    	      </c:when>
			    	      <c:when test="${campaignFilter.sort == 'subject'}">
			    	        <td class="listingsHdr3">
			    	        <a class="hdrLink" href="#" onClick="document.getElementById('sort').value='subject DESC';document.getElementById('list').submit()"><fmt:message key="subject" /></a>
			    	        </td>
			    	        <td><img src="../graphics/down.gif" border="0"></td>
			    	      </c:when>
			    	      <c:otherwise>
			    	        <td colspan="2" class="listingsHdr3">
			    	        <a class="hdrLink" href="#" onClick="document.getElementById('sort').value='subject DESC';document.getElementById('list').submit()"><fmt:message key="subject" /></a>
			    	        </td>
			    	      </c:otherwise>
			    	    </c:choose>  
			    	  </tr>
			    	</table>
			    </td>
			    <td class="listingsHdr3">
			    	<table cellspacing="0" cellpadding="1">
			    	  <tr>
			    	    <c:choose>
			    	      <c:when test="${campaignFilter.sort == 'created DESC'}">
			    	        <td class="listingsHdr3">
			    	        <a class="hdrLink" href="#" onClick="document.getElementById('sort').value='created';document.getElementById('list').submit()"><fmt:message key="created" /></a>
			    	        </td>
			    	        <td><img src="../graphics/up.gif" border="0"></td>
			    	      </c:when>
			    	      <c:when test="${campaignFilter.sort == 'created'}">
			    	        <td class="listingsHdr3">
			    	        <a class="hdrLink" href="#" onClick="document.getElementById('sort').value='created DESC';document.getElementById('list').submit()"><fmt:message key="created" /></a>
			    	        </td>
			    	        <td><img src="../graphics/down.gif" border="0"></td>
			    	      </c:when>
			    	      <c:otherwise>
			    	        <td colspan="2" class="listingsHdr3">
			    	        <a class="hdrLink" href="#" onClick="document.getElementById('sort').value='created DESC';document.getElementById('list').submit()"><fmt:message key="created" /></a>
			    	        </td>
			    	      </c:otherwise>
			    	    </c:choose>  
			    	  </tr>
			    	</table>
			    </td>
			    <td class="listingsHdr3">
			    	<table cellspacing="0" cellpadding="1">
			    	  <tr>
			    	    <c:choose>
			    	      <c:when test="${campaignFilter.sort == 'scheduled DESC'}">
			    	        <td class="listingsHdr3">
			    	        <a class="hdrLink" href="#" onClick="document.getElementById('sort').value='scheduled';document.getElementById('list').submit()"><fmt:message key="scheduled" /></a>
			    	        </td>
			    	        <td><img src="../graphics/up.gif" border="0"></td>
			    	      </c:when>
			    	      <c:when test="${campaignFilter.sort == 'scheduled'}">
			    	        <td class="listingsHdr3">
			    	        <a class="hdrLink" href="#" onClick="document.getElementById('sort').value='scheduled DESC';document.getElementById('list').submit()"><fmt:message key="scheduled" /></a>
			    	        </td>
			    	        <td><img src="../graphics/down.gif" border="0"></td>
			    	      </c:when>
			    	      <c:otherwise>
			    	        <td colspan="2" class="listingsHdr3">
			    	        <a class="hdrLink" href="#" onClick="document.getElementById('sort').value='scheduled DESC';document.getElementById('list').submit()"><fmt:message key="scheduled" /></a>
			    	        </td>
			    	      </c:otherwise>
			    	    </c:choose>  
			    	  </tr>
			    	</table>
			    </td>
			    <td class="listingsHdr3">
			    	<table cellspacing="0" cellpadding="1">
			    	  <tr>
			    	    <c:choose>
			    	      <c:when test="${campaignFilter.sort == 'sent DESC'}">
			    	        <td class="listingsHdr3">
			    	        <a class="hdrLink" href="#" onClick="document.getElementById('sort').value='sent';document.getElementById('list').submit()"><fmt:message key="sent" /></a>
			    	        </td>
			    	        <td><img src="../graphics/up.gif" border="0"></td>
			    	      </c:when>
			    	      <c:when test="${campaignFilter.sort == 'sent'}">
			    	        <td class="listingsHdr3">
			    	        <a class="hdrLink" href="#" onClick="document.getElementById('sort').value='sent DESC';document.getElementById('list').submit()"><fmt:message key="sent" /></a>
			    	        </td>
			    	        <td><img src="../graphics/down.gif" border="0"></td>
			    	      </c:when>
			    	      <c:otherwise>
			    	        <td colspan="2" class="listingsHdr3">
			    	        <a class="hdrLink" href="#" onClick="document.getElementById('sort').value='sent DESC';document.getElementById('list').submit()"><fmt:message key="sent" /></a>
			    	        </td>
			    	      </c:otherwise>
			    	    </c:choose>  
			    	  </tr>
			    	</table>
			    </td>
			    <td class="listingsHdr3">
			    	<table cellspacing="0" cellpadding="1">
			    	  <tr>
			    	    <c:choose>
			    	      <c:when test="${campaignFilter.sort == 'status DESC'}">
			    	        <td class="listingsHdr3">
			    	        <a class="hdrLink" href="#" onClick="document.getElementById('sort').value='status';document.getElementById('list').submit()"><fmt:message key="status" /></a>
			    	        </td>
			    	        <td><img src="../graphics/up.gif" border="0"></td>
			    	      </c:when>
			    	      <c:when test="${campaignFilter.sort == 'status'}">
			    	        <td class="listingsHdr3">
			    	        <a class="hdrLink" href="#" onClick="document.getElementById('sort').value='status DESC';document.getElementById('list').submit()"><fmt:message key="status" /></a>
			    	        </td>
			    	        <td><img src="../graphics/down.gif" border="0"></td>
			    	      </c:when>
			    	      <c:otherwise>
			    	        <td colspan="2" class="listingsHdr3">
			    	        <a class="hdrLink" href="#" onClick="document.getElementById('sort').value='status DESC';document.getElementById('list').submit()"><fmt:message key="status" /></a>
			    	        </td>
			    	      </c:otherwise>
			    	    </c:choose>  
			    	  </tr>
			    	</table>
			    </td>
			    <td class="listingsHdr3">
			    	<table cellspacing="0" cellpadding="1">
			    	  <tr>
			    	    <c:choose>
			    	      <c:when test="${campaignFilter.sort == 'email_sent DESC'}">
			    	        <td class="listingsHdr3">
			    	        <a class="hdrLink" href="#" onClick="document.getElementById('sort').value='email_sent';document.getElementById('list').submit()"><fmt:message key="email" /></a>
			    	        </td>
			    	        <td><img src="../graphics/up.gif" border="0"></td>
			    	      </c:when>
			    	      <c:when test="${campaignFilter.sort == 'email_sent'}">
			    	        <td class="listingsHdr3">
			    	        <a class="hdrLink" href="#" onClick="document.getElementById('sort').value='email_sent DESC';document.getElementById('list').submit()"><fmt:message key="email" /></a>
			    	        </td>
			    	        <td><img src="../graphics/down.gif" border="0"></td>
			    	      </c:when>
			    	      <c:otherwise>
			    	        <td colspan="2" class="listingsHdr3">
			    	        <a class="hdrLink" href="#" onClick="document.getElementById('sort').value='email_sent DESC';document.getElementById('list').submit()"><fmt:message key="email" /></a>
			    	        </td>
			    	      </c:otherwise>
			    	    </c:choose>  
			    	  </tr>
			    	</table>
			    </td>
			    <td class="listingsHdr3">
			    	<table cellspacing="0" cellpadding="1">
			    	  <tr>
			    	    <c:choose>
			    	      <c:when test="${campaignFilter.sort == 'opened_unique DESC'}">
			    	        <td class="listingsHdr3">
			    	        <a class="hdrLink" href="#" onClick="document.getElementById('sort').value='opened_unique';document.getElementById('list').submit()"><fmt:message key="Opened" /></a>
			    	        </td>
			    	        <td><img src="../graphics/up.gif" border="0"></td>
			    	      </c:when>
			    	      <c:when test="${campaignFilter.sort == 'opened_unique'}">
			    	        <td class="listingsHdr3">
			    	        <a class="hdrLink" href="#" onClick="document.getElementById('sort').value='opened_unique DESC';document.getElementById('list').submit()"><fmt:message key="Opened" /></a>
			    	        </td>
			    	        <td><img src="../graphics/down.gif" border="0"></td>
			    	      </c:when>
			    	      <c:otherwise>
			    	        <td colspan="2" class="listingsHdr3">
			    	        <a class="hdrLink" href="#" onClick="document.getElementById('sort').value='opened_unique DESC';document.getElementById('list').submit()"><fmt:message key="Opened" /></a>
			    	        </td>
			    	      </c:otherwise>
			    	    </c:choose>  
			    	  </tr>
			    	</table>
			    </td>
			    <td class="listingsHdr3">
			    	<fmt:message key="openRate" />
			    </td>
			     <td class="listingsHdr3">
			    	<table cellspacing="0" cellpadding="1">
			    	  <tr>
			    	    <c:choose>
			    	      <c:when test="${campaignFilter.sort == 'click_unique DESC'}">
			    	        <td class="listingsHdr3">
			    	        <a class="hdrLink" href="#" onClick="document.getElementById('sort').value='click_unique';document.getElementById('list').submit()"><fmt:message key="clickUnique" /></a>
			    	        </td>
			    	        <td><img src="../graphics/up.gif" border="0"></td>
			    	      </c:when>
			    	      <c:when test="${campaignFilter.sort == 'click_unique'}">
			    	        <td class="listingsHdr3">
			    	        <a class="hdrLink" href="#" onClick="document.getElementById('sort').value='click_unique DESC';document.getElementById('list').submit()"><fmt:message key="clickUnique" /></a>
			    	        </td>
			    	        <td><img src="../graphics/down.gif" border="0"></td>
			    	      </c:when>
			    	      <c:otherwise>
			    	        <td colspan="2" class="listingsHdr3">
			    	        <a class="hdrLink" href="#" onClick="document.getElementById('sort').value='click_unique DESC';document.getElementById('list').submit()"><fmt:message key="clickUnique" /></a>
			    	        </td>
			    	      </c:otherwise>
			    	    </c:choose>  
			    	  </tr>
			    	</table>
			    </td>
			     <td class="listingsHdr3">
			    	<fmt:message key="clickThroughRate" />
			    </td>
			    <td class="listingsHdr3">
			    	<fmt:message key="unsubscribe" />
			    </td>
			   
			  </tr>
			  <c:set var="totalEmailSent" value="0" />
		  	<c:forEach items="${model.compaignList.pageList}" var="campaign" varStatus="status">
			  <tr class="row${status.index % 2}" onmouseover="changeStyleClass(this,'rowOver')" onmouseout="changeStyleClass(this,'row${status.index % 2}')">
			  <td align="center">
			    <img class="action" width="23" height="16" alt="Actions" src="../graphics/actions.png"/>
			    <div class="action-hover displayNone">
			        <ul class="round">
			            <li class="action-header">
					      <div class="name"><c:out value="${campaign.sender}"/></div>
					      <div class="menudiv"></div> 
			            </li>
			            <sec:authorize ifAnyGranted="ROLE_AEM,ROLE_ADMIN_${prop_site_id},ROLE_EMAILMANAGER_VIEW_HISTORY">
			            <c:choose>
					      <c:when test="${campaign.status != 'com' and campaign.status != 'can'}">
						    <li class="link"><a href="massEmail-ajax-Cancel.jhtm?id=${campaign.id}&_type=cencel" rel="width:400,height:380"  id="mb${campaign.id}" class="mbCancelCampaign"><fmt:message key="cancel" /></a></li>
						  </c:when>
					      <c:otherwise><c:out value="${campaign.sender}"/></c:otherwise>
					    </c:choose>
				          <li class="link"><a href="../massEmail/clickStatList.jhtm?campaignId=${campaign.id}" ><fmt:message key="stats"/></a></li>
				          <li class="link"><a href="../massEmail/openDetailList.jhtm?campaignId=${campaign.id}" ><fmt:message key="openedCount"/>&nbsp;<c:out value="${campaign.openedCount}"/></a></li>
				        </sec:authorize>
			        </ul>
			    </div>
				</td>			    
				<td class="nameCol"><c:out value="${campaign.sender}"/></td>
			    <td class="nameCol"><c:out value="${campaign.subject}"/></td>
			    <td class="nameCol"><fmt:formatDate type="both" timeStyle="short" value="${campaign.created}"/></td>
			    <td class="nameCol"><fmt:formatDate type="both" timeStyle="short" value="${campaign.scheduled}"/></td>	
			    <td class="nameCol"><fmt:formatDate type="both" timeStyle="short" value="${campaign.sent}"/></td>		
			    <td class="nameCol"><fmt:message key="campaignStatus_${campaign.status}" /></td>
			    <td class="idCol"><c:out value="${campaign.emailSent}"/></td><c:set var="totalEmailSent" value="${campaign.emailSent + totalEmailSent}" />
			    <td class="nameCol"><c:out value="${campaign.openedUnique}"/></td>
			    <td class="idCol">
			  		<a href="../massEmail/openDetailList.jhtm?campaignId=${campaign.id}" ><c:out value="${campaign.openRate}"/>%</a>
			    </td>
			    <c:choose>
			    <c:when test="${campaign.clickUnique!=null}">
			    <td  class="nameCol"><c:out value="${campaign.clickUnique}"/></td>
			    </c:when>
			    <c:otherwise>
			    <td  class="nameCol"><c:out value="0"/></td>
			    </c:otherwise>
			    </c:choose>
		   		<td class="nameCol">
		   		<a href="../massEmail/clickStatList.jhtm?campaignId=${campaign.id}" ><c:out value="${campaign.clickThroughRate}"/>%</a>
		   	    </td>
		   		<td class="nameCol">
		   		<a href="../massEmail/unsubscribeRate.jhtm?campaignId=${campaign.id}" ><c:out value="${campaign.unsubscribeCount}"/></a>
		   	    </td> 			    				   	
			  </tr>
			  </c:forEach>
			  <tr class="totals">
			  <td colspan="10" align="left"><fmt:message key="total" /></td>
			  <td class="numberCol"><fmt:formatNumber value="${totalEmailSent}" pattern="#,##0" /></td>
			  </tr>
			</table>
			
			<table border="0" cellpadding="0" cellspacing="1" width="100%">
			  <tr>
			  <td>&nbsp;</td>  
			  <td class="pageSize">
			  <select name="size" onchange="document.getElementById('page').value=1;submit()">
			    <c:forTokens items="10,25,50,100,200,500" delims="," var="current">
			  	  <option value="${current}" <c:if test="${current == model.compaignList.pageSize}">selected</c:if>>${current} <fmt:message key="perPage" /></option>
			    </c:forTokens>
			  </select>
			  </td>
			  </tr>
			</table>	  
		    <!-- end input field -->  	  
		  	</div>
		  	
		  	<!-- start button -->
			<!-- end button -->	
       	  	
	<!-- end tab -->        
	</div>
<!-- end tabs -->			
</div>
  
  <!-- end table -->
   </td><td class="boxmidr" ></td></tr>
  <tr><td class="botl"></td><td class="botr"></td></tr>
  </table>
  
<!-- end main box -->  
</div>
</form>
</sec:authorize>

  </tiles:putAttribute>
</tiles:insertDefinition>
