<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/security/tags" prefix="sec" %>

<tiles:insertDefinition name="admin.promos.promo" flush="true">
    <tiles:putAttribute name="title" value="Campaign List" />
	<tiles:putAttribute name="content" type="string">
	
<form action="campaignList.jhtm" method="post" name="list_form" id="list">
<input type="hidden" id="sort" name="sort" value="${promoSearch.sort}" />
<!-- main box -->
  <div id="mboxfull">
  
  <!-- start table -->
  <table cellpadding="0" cellspacing="0" border="0" class="module" >
  <tr>
    <td class="topl_g">
	  
	  <!-- breadcrumb -->
	  <p class="breadcrumb">
	    <a href="../promos"><fmt:message key="promosTabTitle"/></a> &gt;
	    Campaign List
	  </p>
	  
	  <!-- header image -->
	  <img class="headerImage" src="../graphics/salesTag.gif" />
	  
	  </td><td class="topr_g" ></td></tr>
  <tr><td class="boxmidlrg" >

 <!-- tabs -->
 <div class="tab-wrapper">
	<h4 title=""></h4>
	<div>
	<!-- start tab -->

	  	<div class="listdivi"></div>
	   
		  	<div class="listlight">
		  	<!-- input field --> 
		  	<table border="0" cellpadding="0" cellspacing="1" width="100%">
				<tr>
					<td>&nbsp;
					</td>
					<c:if test="${model.promotions.nrOfElements > 0}">
						<td class="pageShowing">
							<fmt:message key="showing">
								<fmt:param value="${model.promotions.firstElementOnPage + 1}" />
							    <fmt:param value="${model.promotions.lastElementOnPage + 1}" />
								 <fmt:param value="${model.promotions.nrOfElements}" />
							</fmt:message>
						</td>
					</c:if>
					<td class="pageNavi">
						Page
						<select name="page" id="page" onchange="submit()">
							<c:forEach begin="1" end="${model.promotions.pageCount}" var="page">
								<option value="${page}"
									<c:if test="${page == (model.promotions.page+1)}">selected</c:if>>
									${page}
								</option>
							</c:forEach>
						</select> 
						of
						<c:out value="${model.promotions.pageCount}" /> 
						|
						<c:if test="${model.promotions.firstPage}">
							<span class="pageNaviDead"><fmt:message key="previous" /></span>
						</c:if>
						<c:if test="${not model.promotions.firstPage}">
							<a
								href="<c:url value="CampaignList.jhtm"><c:param name="page" value="${model.promotions.page}"/><c:param name="parent" value="${model.search['parent']}"/><c:param name="size" value="${model.promotions.pageSize}"/></c:url>"
								class="pageNaviLink"><fmt:message key="previous" /></a>
						</c:if>
						|
						<c:if test="${model.promotions.lastPage}">
							<span class="pageNaviDead"><fmt:message key="next" /></span>
						</c:if>
						<c:if test="${not model.promotions.lastPage}">
							<a
								href="<c:url value="CampaignList.jhtm"><c:param name="page" value="${model.promotions.page+2}"/><c:param name="parent" value="${model.search['parent']}"/><c:param name="size" value="${model.promotions.pageSize}"/></c:url>"
								class="pageNaviLink"><fmt:message key="next" /></a>
						</c:if>
					</td>
				</tr>
			</table>

			<table border="0" cellpadding="0" cellspacing="0" width="100%" class="listings">
				<tr class="listingsHdr2">
					
					<c:choose>
			    	<c:when test="${promoSearch.sort == 'title desc'}">
			    	    <td class="listingsHdr3">
			    	      <a class="hdrLink" href="#" onClick="document.getElementById('sort').value='title';document.getElementById('list').submit()"><fmt:message key="CampaignName" /></a><img src="../graphics/up.gif" border="0">
			    	    </td>
			    	</c:when>
			    	<c:when test="${promoSearch.sort == 'title'}">
			    	    <td class="listingsHdr3">
			    	      <a class="hdrLink" href="#" onClick="document.getElementById('sort').value='title desc';document.getElementById('list').submit()"><fmt:message key="CampaignName" /></a><img src="../graphics/down.gif" border="0">
			    	    </td>
			    	</c:when>
			    	<c:otherwise>
			    	  <td class="listingsHdr3 nameCol">
			    	    <a class="hdrLink" href="#" onClick="document.getElementById('sort').value='title desc';document.getElementById('list').submit()"><fmt:message key="CampaignName" /></a>
			    	  </td>
			        </c:otherwise>
			        </c:choose>
			
					<td class="listingsHdr3 nameCol">
						<fmt:message key="PromoCount" />
					</td>
				</tr>
				<c:forEach items="${model.promotions.pageList}" var="promo"	varStatus="status">
					<tr class="row${status.index % 2}"	onmouseover="changeStyleClass(this,'rowOver')" 	onmouseout="changeStyleClass(this,'row${status.index % 2}')">
	
			             
					    
						<td class="nameCol">
							<em><c:out value="${status.count + model.promotions.firstElementOnPage}" />. </em><em><c:out
									value="${promo.prefix}" />
							</em>
						</td>

						<td  class="nameCol">
							<a href="<c:url value="promoDynamicList.jhtm"><c:param name="_prefix" value="${promo.prefix}"/> </c:url>" class="nameLink"><c:out value="${promo.count}" ></c:out></a>
						</td>
					</tr>
				</c:forEach>
				<c:if test="${model.promotions.nrOfElements == 0}">
					<tr class="emptyList">
						<td colspan="8">
							&nbsp;
						</td>
					</tr>
				</c:if>
			</table>

	
			
			
			<table border="0" cellpadding="0" cellspacing="1" width="100%">
				<tr>
				  <td class="pageSize">
					<select name="size"
						onchange="document.getElementById('page').value=1;submit()">
						<c:forTokens items="10,25,50" delims="," var="current">
							<option value="${current}"
								<c:if test="${current == promoSearch.pageSize}">selected</c:if>>
								${current}
								<fmt:message key="perPage" />
							</option>
						</c:forTokens>
					</select>
				  </td>
				</tr>
			</table>
		  	<!-- end input field -->  	  
		  	</div>
		  	

	
	  	
	<!-- end tab -->        
	</div>
<!-- end tabs -->			
</div>
  
  <!-- end table -->
   </td><td class="boxmidr" ></td></tr>
  <tr><td class="botl"></td><td class="botr"></td></tr>
  </table>
  
<!-- end main box -->  
</div>  
</form> 	

	</tiles:putAttribute>
</tiles:insertDefinition>
