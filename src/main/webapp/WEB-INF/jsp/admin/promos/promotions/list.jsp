<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/security/tags" prefix="sec" %>

<tiles:insertDefinition name="admin.promos.promo" flush="true">
    <tiles:putAttribute name="title" value="Promotions Manager" />
	<tiles:putAttribute name="content" type="string">
	
<form action="promoList.jhtm" method="post" name="list_form" id="list">
<input type="hidden" id="sort" name="sort" value="${promoSearch.sort}" />
<!-- main box -->
  <div id="mboxfull">
  
  <!-- start table -->
  <table cellpadding="0" cellspacing="0" border="0" class="module" >
  <tr>
    <td class="topl_g">
	  
	  <!-- breadcrumb -->
	  <p class="breadcrumb">
	    <a href="../promos"><fmt:message key="promosTabTitle"/></a> &gt;
	    promotions 
	  </p>
	  
	  <!-- header image -->
	  <img class="headerImage" src="../graphics/salesTag.gif" />
	  
	  </td><td class="topr_g" ></td></tr>
  <tr><td class="boxmidlrg" >

 <!-- tabs -->
 <div class="tab-wrapper">
	<h4 title=""></h4>
	<div>
	<!-- start tab -->

	  	<div class="listdivi"></div>
	   
		  	<div class="listlight">
		  	<!-- input field --> 
		  	<table border="0" cellpadding="0" cellspacing="1" width="100%">
				<tr>
					<td>&nbsp;
					</td>
					<c:if test="${model.promotions.nrOfElements > 0}">
						<td class="pageShowing">
							<fmt:message key="showing">
								<fmt:param value="${model.promotions.firstElementOnPage + 1}" />
								<fmt:param value="${model.promotions.lastElementOnPage + 1}" />
								<fmt:param value="${model.promotions.nrOfElements}" />
							</fmt:message>
						</td>
					</c:if>
					<td class="pageNavi">
						Page
						<select name="page" id="page" onchange="submit()">
							<c:forEach begin="1" end="${model.promotions.pageCount}"
								var="page">
								<option value="${page}"
									<c:if test="${page == (model.promotions.page+1)}">selected</c:if>>
									${page}
								</option>
							</c:forEach>
						</select> 
						of
						<c:out value="${model.promotions.pageCount}" /> 
						|
						<c:if test="${model.promotions.firstPage}">
							<span class="pageNaviDead"><fmt:message key="previous" /></span>
						</c:if>
						<c:if test="${not model.promotions.firstPage}">
							<a
								href="<c:url value="promoList.jhtm"><c:param name="page" value="${model.promotions.page}"/><c:param name="parent" value="${model.search['parent']}"/><c:param name="size" value="${model.promotions.pageSize}"/></c:url>"
								class="pageNaviLink"><fmt:message key="previous" /></a>
						</c:if>
						|
						<c:if test="${model.promotions.lastPage}">
							<span class="pageNaviDead"><fmt:message key="next" /></span>
						</c:if>
						<c:if test="${not model.promotions.lastPage}">
							<a
								href="<c:url value="promoList.jhtm"><c:param name="page" value="${model.promotions.page+2}"/><c:param name="parent" value="${model.search['parent']}"/><c:param name="size" value="${model.promotions.pageSize}"/></c:url>"
								class="pageNaviLink"><fmt:message key="next" /></a>
						</c:if>
					</td>
				</tr>
			</table>

			<table border="0" cellpadding="0" cellspacing="0" width="100%"
				class="listings">
				<tr class="listingsHdr2">

					<td class="indexCol">&nbsp;</td>
					<c:choose>
			    	<c:when test="${promoSearch.sort == 'title desc'}">
			    	    <td class="listingsHdr3">
			    	      <a class="hdrLink" href="#" onClick="document.getElementById('sort').value='title';document.getElementById('list').submit()"><fmt:message key="promoCode" /></a><img src="../graphics/up.gif" border="0">
			    	    </td>
			    	</c:when>
			    	<c:when test="${promoSearch.sort == 'title'}">
			    	    <td class="listingsHdr3">
			    	      <a class="hdrLink" href="#" onClick="document.getElementById('sort').value='title desc';document.getElementById('list').submit()"><fmt:message key="promoCode" /></a><img src="../graphics/down.gif" border="0">
			    	    </td>
			    	</c:when>
			    	<c:otherwise>
			    	  <td class="listingsHdr3">
			    	    <a class="hdrLink" href="#" onClick="document.getElementById('sort').value='title desc';document.getElementById('list').submit()"><fmt:message key="promoCode" /></a>
			    	  </td>
			        </c:otherwise>
			        </c:choose>
			        <td class="listingsHdr3">
						<fmt:message key="discount" />
					</td>
					<td class="listingsHdr3">
						<fmt:message key="minOrder" />
					</td>
					<td class="listingsHdr3">
						<fmt:message key="promoDate" />
					</td>
					<td class="listingsHdr3">
						<fmt:message key="promoRestriction" />
					</td>
					<td class="listingsHdr3">
						<fmt:message key="inEffect" />
					</td>
					<c:choose>
			    	<c:when test="${promoSearch.sort == 'rank desc'}">
			    	    <td class="listingsHdr3">
			    	      <a class="hdrLink" href="#" onClick="document.getElementById('sort').value='rank';document.getElementById('list').submit()"><fmt:message key="rank" /></a><img src="../graphics/up.gif" border="0">
			    	    </td>
			    	</c:when>
			    	<c:when test="${promoSearch.sort == 'rank'}">
			    	    <td class="listingsHdr3">
			    	      <a class="hdrLink" href="#" onClick="document.getElementById('sort').value='rank desc';document.getElementById('list').submit()"><fmt:message key="rank" /></a><img src="../graphics/down.gif" border="0">
			    	    </td>
			    	</c:when>
			    	<c:otherwise>
			    	  <td class="listingsHdr3">
			    	    <a class="hdrLink" href="#" onClick="document.getElementById('sort').value='rank desc';document.getElementById('list').submit()"><fmt:message key="rank" /></a>
			    	  </td>
			        </c:otherwise>
			      </c:choose>
				</tr>
				<c:forEach items="${model.promotions.pageList}" var="promo"	varStatus="status">
					<tr class="row${status.index % 2}"	onmouseover="changeStyleClass(this,'rowOver')" 	onmouseout="changeStyleClass(this,'row${status.index % 2}')">
						<td class="indexCol"><c:out value="${status.count + model.promotions.firstElementOnPage}" />.</td>
						<td class="nameCol">
							<a href="promo.jhtm?id=${promo.promoId}" class="nameLink"><c:out
									value="${promo.title}" />
							</a>
						</td>
						<td class="nameCol">
							<c:choose>
								<c:when test="${promo.percent == true}"><c:out value="${promo.discount}" />%</c:when>
								<c:when test="${promo.percent == false}">$<c:out value="${promo.discount}" /></c:when>
							</c:choose>
						</td>
						<td class="nameCol">
							<fmt:message key="${siteConfig['CURRENCY'].value}" /> <c:out value="${promo.minOrder}" />
						</td>
						<td class="nameCol">
							<fmt:formatDate type="date" dateStyle="full" value="${promo.startDate}" pattern="MM/dd/yyyy (hh:mm)a zz "/> - <fmt:formatDate type="date" dateStyle="full" value="${promo.endDate}" pattern="MM/dd/yyyy (hh:mm)a zz"/> 
						</td>
						<td class="nameCol">
							<c:out value="${promo.htmlCode}" ></c:out>
						</td>
						<td class="nameCol">
							<c:if test="${promo.inEffect}"><div align="left"><img src="../graphics/checkbox.png" /></div></c:if>
						</td>
						<td class="rankCol">
							<input name="__rank_${promo.promoId}" type="text" value="${promo.rank}" size="5" maxlength="5" class="rankField">
						</td>
					</tr>
				</c:forEach>
				<c:if test="${model.promotions.nrOfElements == 0}">
					<tr class="emptyList">
						<td colspan="8">
							&nbsp;
						</td>
					</tr>
				</c:if>
			</table>

			<c:choose>
				<c:when test="${promo.percent == true}"><c:out value="${promo.discount}" />%</c:when>
				<c:when test="${promo.percent == false}">$<c:out value="${promo.discount}" /></c:when>
			</c:choose>
			
			<c:if test="${model.nOfpromotions > 0}">
				<div class="sup">
					&sup1; <fmt:message key="toEditClickPromoCode" /> 
				</div>
			</c:if>
			<table border="0" cellpadding="0" cellspacing="1" width="100%">
				<tr>
				  <td class="pageSize">
					<select name="size"
						onchange="document.getElementById('page').value=1;submit()">
						<c:forTokens items="10,25,50" delims="," var="current">
							<option value="${current}"
								<c:if test="${current == promoSearch.pageSize}">selected</c:if>>
								${current}
								<fmt:message key="perPage" />
							</option>
						</c:forTokens>
					</select>
				  </td>
				</tr>
			</table>
		  	<!-- end input field -->  	  
		  	</div>
		  	
		  	<!-- start button -->
		  	<div align="left" class="button">
 			<c:choose>
				<c:when test="${gSiteConfig['gNUMBER_PROMOS'] == '-1' or gSiteConfig['gNUMBER_PROMOS'] > model.nOfpromotions}">
				  <sec:authorize ifAnyGranted="ROLE_AEM,ROLE_ADMIN_${prop_site_id},ROLE_PROMOTION_CREATE">
					<input type="submit" name="__add" value="<fmt:message key="promoAdd" />"/>
				  </sec:authorize>	
				  <sec:authorize ifAnyGranted="ROLE_AEM,ROLE_ADMIN_${prop_site_id}">  	
				    <input type="submit" name="__update_ranking" value="<fmt:message key="updateRanking" />">
			  	  </sec:authorize>
				</c:when>
				<c:otherwise>
				  <div class="note_list">
					<fmt:message key="toAddMorePromoContactWebMaster" />
				  </div>	
				</c:otherwise>
			</c:choose>	
			</div>
		  	<!-- end button -->	
	
	  	
	<!-- end tab -->        
	</div>
<!-- end tabs -->			
</div>
  
  <!-- end table -->
   </td><td class="boxmidr" ></td></tr>
  <tr><td class="botl"></td><td class="botr"></td></tr>
  </table>
  
<!-- end main box -->  
</div>  
</form> 	

	</tiles:putAttribute>
</tiles:insertDefinition>
