<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/security/tags" prefix="sec" %>

<tiles:insertDefinition name="admin.dynamicPromo" flush="true">
    <tiles:putAttribute name="title" value="Dynamic Promotions Manager" />
	<tiles:putAttribute name="content" type="string">
	<link rel="stylesheet" type="text/css" media="all" href="../javascript/jscalendar-1.0/skins/aqua/theme.css" title="Aqua">
<script type="text/javascript" src="../javascript/jscalendar-1.0/calendar.js"></script>

<script type="text/javascript" src="../javascript/jscalendar-1.0/lang/calendar-en.js"></script>
<script type="text/javascript" src="../javascript/jscalendar-1.0/calendar-setup.js"></script>
	
	
<form action="promoDynamicList.jhtm" method="post" name="list_form" id="list">
<input type="hidden" id="sort" name="sort" value="${model.promoSearch.sort}" />
<!-- main box -->
  <div id="mboxfull">
  
  <!-- start table -->
  <table cellpadding="0" cellspacing="0" border="0" class="module" >
  <tr>
    <td class="topl_g">
	  
	  <!-- breadcrumb -->
	  <p class="breadcrumb">
	    <a href="../promos"><fmt:message key="promosTabTitle"/></a> &gt;
	    Dynamic promotions
	  </p>
	  
	  <!-- header image -->
	  <img class="headerImage" src="../graphics/salesTag.gif" />
	  
	  </td><td class="topr_g" ></td></tr>
  <tr><td class="boxmidlrg" >

 <!-- tabs -->
 <div class="tab-wrapper">
	<h4 title=""></h4>
	<div>
	<!-- start tab -->

	  	<div class="listdivi"></div>
	   
		  	<div class="listlight">
		  	<!-- input field --> 
		  	<table border="0" cellpadding="0" cellspacing="1" width="100%">
				<tr>
					<td>&nbsp;
					</td>
					<c:if test="${model.promotions.nrOfElements > 0}">
						<td class="pageShowing">
							<fmt:message key="showing">
								<fmt:param value="${model.promotions.firstElementOnPage + 1}" />
								<fmt:param value="${model.promotions.lastElementOnPage + 1}" />
							    <fmt:param value="${model.promotions.nrOfElements}" /> 
							</fmt:message>
						</td>
					</c:if>
					<td class="pageNavi">
						Page
						<select name="page" id="page" onchange="submit()">
							<c:forEach begin="1" end="${model.promotions.pageCount}" var="page">
								<option value="${page}"
									<c:if test="${page == (model.promotions.page+1)}">selected</c:if>>
									${page}
								</option>
							</c:forEach>
						</select> 
						of
						<c:out value="${model.promotions.pageCount}" /> 
						|
						<c:if test="${model.promotions.firstPage}">
							<span class="pageNaviDead"><fmt:message key="previous" /></span>
						</c:if>
						<c:if test="${not model.promotions.firstPage}">
							<a
								href="<c:url value="promoDynamicList.jhtm"><c:param name="page" value="${model.promotions.page}"/><c:param name="parent" value="${model.search['parent']}"/><c:param name="size" value="${model.promotions.pageSize}"/></c:url>"
								class="pageNaviLink"><fmt:message key="previous" /></a>
						</c:if>
						|
						<c:if test="${model.promotions.lastPage}">
							<span class="pageNaviDead"><fmt:message key="next" /></span>
						</c:if>
						<c:if test="${not model.promotions.lastPage}">
							<a
								href="<c:url value="promoDynamicList.jhtm"><c:param name="page" value="${model.promotions.page+2}"/><c:param name="parent" value="${model.search['parent']}"/><c:param name="size" value="${model.promotions.pageSize}"/></c:url>"
								class="pageNaviLink"><fmt:message key="next" /></a>
						</c:if>
					</td>
				</tr>
			</table>

			<table border="0" cellpadding="0" cellspacing="0" width="100%"
				class="listings">
				<tr class="listingsHdr2">

					
					<td align="center" class="listingsHdr3e"><input type="checkbox" onclick="toggleAll(this)"></td>
					<c:choose>
			    	<c:when test="${model.promoSearch.sort == 'title desc'}">
			    	    <td class="listingsHdr3">
			    	      <a class="hdrLink" href="#" onClick="document.getElementById('sort').value='title';document.getElementById('list').submit()"><fmt:message key="promoCode" /></a><img src="../graphics/up.gif" border="0">
			    	    </td>
			    	</c:when>
			    	<c:when test="${model.promoSearch.sort == 'title'}">
			    	    <td class="listingsHdr3">
			    	      <a class="hdrLink" href="#" onClick="document.getElementById('sort').value='title desc';document.getElementById('list').submit()"><fmt:message key="promoCode" /></a><img src="../graphics/down.gif" border="0">
			    	    </td>
			    	</c:when>
			    	<c:otherwise>
			    	
			       
			    	  <td class="listingsHdr3">
			    	    <a class="hdrLink" href="#" onClick="document.getElementById('sort').value='title desc';document.getElementById('list').submit()"><fmt:message key="promoCode" /></a>
			    	  </td>
			        </c:otherwise>
			        </c:choose>
			        <td class="listingsHdr3">
						<fmt:message key="discount" />
					</td>
					<td class="listingsHdr3">
						<fmt:message key="minOrder" />
					</td>
					<td class="listingsHdr3">
						<fmt:message key="promoDate" />
					</td>
					<td class="listingsHdr3">
						<fmt:message key="promoRestriction" />
					</td>
					<td class="listingsHdr3">
						<fmt:message key="inEffect" />
					</td>
					<c:choose>
			    	<c:when test="${model.promoSearch.sort == 'rank desc'}">
			    	    <td class="listingsHdr3">
			    	      <a class="hdrLink" href="#" onClick="document.getElementById('sort').value='rank';document.getElementById('list').submit()"><fmt:message key="rank" /></a><img src="../graphics/up.gif" border="0">
			    	    </td>
			    	</c:when>
			    	<c:when test="${model.promoSearch.sort == 'rank'}">
			    	    <td class="listingsHdr3">
			    	      <a class="hdrLink" href="#" onClick="document.getElementById('sort').value='rank desc';document.getElementById('list').submit()"><fmt:message key="rank" /></a><img src="../graphics/down.gif" border="0">
			    	    </td>
			    	</c:when>
			    	<c:otherwise>
			    	  <td class="listingsHdr3">
			    	    <a class="hdrLink" href="#" onClick="document.getElementById('sort').value='rank desc';document.getElementById('list').submit()"><fmt:message key="rank" /></a>
			    	  </td>
			        </c:otherwise>
			      </c:choose>
				</tr>
				<c:forEach items="${model.promotions.pageList}" var="promo"	varStatus="status">
					<tr class="row${status.index % 2}"	onmouseover="changeStyleClass(this,'rowOver')" 	onmouseout="changeStyleClass(this,'row${status.index % 2}')">
					  
					   <td align="center" class="quickMode">
					       <input name="__selected_id" value="${promo.promoId}" type="checkbox">
			                <input type="hidden" value="${promo.sku}" name="__selected_sku_${promo.promoId}"/> 
			           </td>
			             
					    
						<td class="nameCol">
							<em><c:out value="${status.count + model.promotions.firstElementOnPage}" />. </em><a href="promo.jhtm?id=${promo.promoId}&dyn=true" class="nameLink"><c:out
									value="${promo.title}" />
							</a>
						</td>
						<td class="nameCol">
							<c:choose>
								<c:when test="${promo.percent == true}"><c:out value="${promo.discount}" />%</c:when>
								<c:when test="${promo.percent == false}">$<c:out value="${promo.discount}" /></c:when>
							</c:choose>
						</td>
						<td class="nameCol">
							<fmt:message key="${siteConfig['CURRENCY'].value}" /> <c:out value="${promo.minOrder}" />
						</td>
						<td class="nameCol">
							<fmt:formatDate type="date" dateStyle="full" value="${promo.startDate}" pattern="MM/dd/yyyy (hh:mm)a zz "/> - <fmt:formatDate type="date" dateStyle="full" value="${promo.endDate}" pattern="MM/dd/yyyy (hh:mm)a zz"/> 
						</td>
						<td class="nameCol">
							<c:out value="${promo.htmlCode}" ></c:out>
						</td>
						<td class="nameCol">
							<c:if test="${promo.inEffect}"><div align="left"><img src="../graphics/checkbox.png" /></div></c:if>
						</td>
						<td class="rankCol">
							<input name="__rank_${promo.promoId}" type="text" value="${promo.rank}" size="5" maxlength="5" class="rankField">
						</td>
					</tr>
				</c:forEach>
				<c:if test="${model.promotions.nrOfElements == 0}">
					<tr class="emptyList">
						<td colspan="8">
							&nbsp;
						</td>
					</tr>
				</c:if>
			</table>

			<c:choose>
				<c:when test="${promo.percent == true}"><c:out value="${promo.discount}" />%</c:when>
				<c:when test="${promo.percent == false}">$<c:out value="${promo.discount}" /></c:when>
			</c:choose>
			
			<c:if test="${model.nOfpromotions > 0}">
				<div class="sup">
					&sup1; <fmt:message key="toEditClickPromoCode" /> 
				</div>
			</c:if>
			<table border="0" cellpadding="0" cellspacing="1" width="100%">
				<tr>
				  <td class="pageSize">
					<select name="size"
						onchange="document.getElementById('page').value=1;submit()">
						<c:forTokens items="10,25,50" delims="," var="current">
							<option value="${current}"
								<c:if test="${current == model.promoSearch.pageSize}">selected</c:if>>
								${current}
								<fmt:message key="perPage" />
							</option>
						</c:forTokens>
					</select>
				  </td>
				</tr>
			</table>
		  	<!-- end input field -->  	  
		  	</div>
		  	
		  	<!-- start button -->
		  	<div align="left" class="button">
 			<c:choose>
				<c:when test="${gSiteConfig['gNUMBER_PROMOS'] == '-1' or gSiteConfig['gNUMBER_PROMOS'] > model.nOfpromotions}">
				  <sec:authorize ifAnyGranted="ROLE_AEM,ROLE_ADMIN_${prop_site_id},ROLE_PROMOTION_CREATE">
					<input type="submit" name="__add" value="<fmt:message key="promoAdd" />"/>
				  </sec:authorize>	
				  <sec:authorize ifAnyGranted="ROLE_AEM,ROLE_ADMIN_${prop_site_id}">  	
				    <input type="submit" name="__update_ranking" value="<fmt:message key="updateRanking" />">
			  	  </sec:authorize>
			  	  
			  	<sec:authorize ifAnyGranted="ROLE_AEM,ROLE_ADMIN_${prop_site_id},ROLE_PROMOTION_DELETE">  
			  	  <input type="submit" name="__delete" value="Delete Selected Products" onClick="return deleteSelected()">
			  	</sec:authorize>  
				</c:when>
				<c:otherwise>
				  <div class="note_list">
					<fmt:message key="toAddMorePromoContactWebMaster" />
				  </div>	
				</c:otherwise>
			</c:choose>	
			</div>
		  	<!-- end button -->	
	
	  	
	<!-- end tab -->        
	</div>
<!-- end tabs -->			
</div>
  
  <!-- end table -->
   </td><td class="boxmidr" ></td></tr>
  <tr><td class="botl"></td><td class="botr"></td></tr>
  </table>
  
<!-- end main box -->  
</div>  
</form> 	
<script type="text/javascript">
Calendar.setup({
    inputField     :    "_startDate",   // id of the input field
    showsTime      :    true,
    ifFormat       :    "%m/%d/%Y[%I:%M %p]",   // format of the input field
    button         :    "time_start_trigger1" // trigger for the calendar (button ID)
});

Calendar.setup({
    inputField     :    "_endDate",   // id of the input field
    showsTime      :    true,
    ifFormat       :    "%m/%d/%Y[%I:%M %p]",   // format of the input field
    button         :    "time_start_trigger2" // trigger for the calendar (button ID)
});

function toggleAll(el) {
	var ids = document.getElementsByName("__selected_id");	
  	if (ids.length == 1)
      document.list_form.__selected_id.checked = el.checked;
    else
      for (i = 0; i < ids.length; i++)
        document.list_form.__selected_id[i].checked = el.checked;	
}  
<sec:authorize ifAnyGranted="ROLE_AEM,ROLE_ADMIN_${prop_site_id},ROLE_PRODUCT_DELETE">  
function deleteSelected() {
	var ids = document.getElementsByName("__selected_id");	
  	if (ids.length == 1) {
      if (document.list_form.__selected_id.checked) {
    	return confirm('Delete permanently?');
      }
    } else {
      for (i = 0; i < ids.length; i++) {
        if (document.list_form.__selected_id[i].checked) {
    	  return confirm('Delete permanently?');
        }
      }
    }

    alert("Please select product(s) to delete.");       
    return false;
}
</sec:authorize>
</script>
 

	</tiles:putAttribute>
</tiles:insertDefinition>
