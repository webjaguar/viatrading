<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="/WEB-INF/tlds/spring.tld" prefix="spring" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib uri="http://www.springframework.org/security/tags" prefix="sec" %>


<tiles:insertDefinition name="admin.promos.promo" flush="true">
  <tiles:putAttribute name="title" value="Promotions Manager" />
  <tiles:putAttribute name="content" type="string"> 
<sec:authorize ifAnyGranted="ROLE_AEM,ROLE_ADMIN_${prop_site_id},ROLE_PROMOTION_CREATE,ROLE_PROMOTION_EDIT">
<link rel="stylesheet" type="text/css" media="all" href="./../../admin/javascript/jscalendar-1.0/skins/aqua/theme.css" title="Aqua" /> <!-- calendar stylesheet -->
<script type="text/javascript" src="./../../admin/javascript/jscalendar-1.0/calendar.js"></script> <!-- main calendar program -->
<script type="text/javascript" src="./../../admin/javascript/jscalendar-1.0/lang/calendar-en.js"></script>
<script type="text/javascript" src="./../../admin/javascript/jscalendar-1.0/calendar-setup.js"></script>
<script language="JavaScript"> 
<!--
window.addEvent('domready', function(){			
	var tabs1 = new SimpleTabs($('tab-block-1'), { selector: 'h4'});
	var Tips1 = new Tips($$('.toolTipImg'));
});
function displayOptions(discountType){
	if(discountType == 'shipping') {
		document.getElementById('shippingOptions').style.display = "block";
		document.getElementById('productOptions').style.display = "none";
	} else if(discountType == 'product') {
		document.getElementById('productOptions').style.display = "block";
		document.getElementById('shippingOptions').style.display = "none";
	} else {
		document.getElementById('shippingOptions').style.display = "none";
		document.getElementById('productOptions').style.display = "none";
	}
}
//--> 
</script>
<form:form id="promoForm" commandName="promoForm" method="post" >
<!-- main box -->
  <div id="mboxfull">
  
  <!-- start table -->
  <table cellpadding="0" cellspacing="0" border="0" class="module" >
  <tr>
    <td class="topl_g">
	  
	  <!-- breadcrumb -->
	  <p class="breadcrumb">
	    <a href="../promos"><fmt:message key="promosTabTitle"/></a> &gt;
	    <c:choose>
	    <c:when test = "${model.isDynamic}">
	      <a href="../promos/promoDynamicList.jhtm"><fmt:message key="promotionsDynamicTitle"/></a> &gt;
	    </c:when> 
	    <c:otherwise>
	      <a href="../promos/promoList.jhtm"><fmt:message key="promotionsTitle"/></a> &gt;
	     
	    </c:otherwise>
	    </c:choose>

	    ${promoForm.promo.title} 
	  </p>
	  
	  <!-- Error Message -->
	  <%--<c:if test="${message != null}">
  		<div class="message"><fmt:message key="${message}"/></div>
  	  </c:if> --%>
  	  <spring:hasBindErrors name="promoForm">
		<div class="message">Please fix all errors!</div>
	  </spring:hasBindErrors>
	  
  </td><td class="topr_g" ></td></tr>
  <tr><td class="boxmidlrg" >

 <!-- tabs -->
 <div id="tab-block-1">
	<h4 title="Promotion Form"><fmt:message key="promotionsTitle"/></h4>
	<div>
	<!-- start tab -->
         <c:set var="classIndex" value="0" />
         
	  	<div class="listdivi ln tabdivi"></div>
	  	<div class="listdivi"></div>
	   
		  	<div class="list${classIndex % 2}"><c:set var="classIndex" value="${classIndex+1}" />
		  	<div class="listfl"><div class="requiredField"><fmt:message key='code' />:</div></div>
		  	<div class="listp">
		  	<!-- input field -->
				<form:input path="promo.title" cssClass="textfield" />
				<form:errors path="promo.title" cssClass="error"/>	
	 		<!-- end input field -->	
	 		</div>
		  	</div>			
	
		  	<div class="list${classIndex % 2}"><c:set var="classIndex" value="${classIndex+1}" />
		  	<div class="listfl"><div class="helpImg"><img class="toolTipImg" title="Note:: Restrict the use of this promo to one time per customer." src="../graphics/question.gif" /></div><fmt:message key='onetimeUsePerCustomer' />:</div>
		  	<div class="listp">
		  	<!-- input field -->
				<form:checkbox path="promo.onetimeUsePerCustomer" value="true"/>
	 		<!-- end input field -->	
	 		</div>
		  	</div>
		  	
		  	<!-- NEW CUSTOMER  -->			
			<div class="list${classIndex % 2}"><c:set var="classIndex" value="${classIndex+1}" />
		  	<div class="listfl"><div class="helpImg"><img class="toolTipImg" title="Note:: Restrict the use of this promo to new customer." src="../graphics/question.gif" /></div><fmt:message key='promoForNewCustomersOnly' />:</div>
		  	<div class="listp">
		  	<!-- input field -->
				<form:checkbox path="promo.promoForNewCustomersOnly" value="true"/>
	 		<!-- end input field -->	
	 		</div>
		  	</div>
		  	
		  	
		  	<div class="list${classIndex % 2}"><c:set var="classIndex" value="${classIndex+1}" />
		  	<div class="listfl"><div class="helpImg"><img class="toolTipImg" title="Note:: Restrict the use of this promo to one time only. " src="../graphics/question.gif" /></div><fmt:message key='onetimeUseOnly' />:</div>
		  	<div class="listp">
		  	<!-- input field -->
				<form:checkbox path="promo.onetimeUseOnly" value="true"/>
	 		<!-- end input field -->	
	 		</div>
		  	</div>
		  		

		  	  <c:if test="${model.isDynamic == 'true'}">
 		  	<%--  <p>My name is : <c:out value = "${model.isDynamic } "	/> </p>  --%>
			<div class="list${classIndex % 2}"><c:set var="classIndex" value="${classIndex+1}" />
		  	<div class="listfl"><div ><fmt:message key='customerId' />:</div></div>
		  	<div class="listp">
		  	<!-- input field -->
				<form:input path="promo.userId" cssClass="textfield" />
	 		<!-- end input field -->	
	 		</div>
		  	</div>
		    </c:if>  
 		  	
		  	<div class="list${classIndex % 2}"><c:set var="classIndex" value="${classIndex+1}" />
		  	<div class="listfl"><div class="requiredField"><fmt:message key='discount' />:</div></div>
		  	<div class="listp">
		  	<!-- input field -->
				<form:input path="promo.discount" cssClass="textfield"/>
				<form:select path="promo.percent" cssClass="second">
					<form:option value="true" label="%" />
					<form:option value="false"><fmt:message key="FIXED" /> $</form:option>
				</form:select>
				<form:errors path="promo.discount" cssClass="error"/>
				<span class="helpNote"><p><fmt:message key='maxis100' /> (<fmt:message key='forPercentage' />)</p></span>
	 		<!-- end input field -->	
	 		</div>
		  	</div>			

		  	<div class="list${classIndex % 2}"><c:set var="classIndex" value="${classIndex+1}" />
		  	<div class="listfl"><div class="requiredField"><fmt:message key='minOrder' />$:</div></div>
		  	<div class="listp">
		  	<!-- input field -->
				<form:input path="promo.minOrder" cssClass="textfield" />
				<form:errors path="promo.minOrder" cssClass="error"/>
	 		<!-- end input field -->	
	 		</div>
		  	</div>			

		  	<div class="list${classIndex % 2}"><c:set var="classIndex" value="${classIndex+1}" />
		  	<div class="listfl"><div class="requiredField"><fmt:message key='startDate' />:</div></div>
		  	<div class="listp">
		  	<!-- input field -->
				<span class="helpNote"><p><fmt:message key='currentTime' />: <c:out value="${model.currentTime}"></c:out></p></span>
				<form:input path="promo.startDate" cssClass="textfield" size="19" maxlength="19" />
				  <img src="../graphics/calendarIcon.jpg" class="calendarImage" id="time_start_trigger"/>
				  <script type="text/javascript">
		    		Calendar.setup({
		        			inputField     :    "promo.startDate",   // id of the input field
		        			showsTime      :    true,
		        			ifFormat       :    "%m-%d-%Y %I:%M %p",   // format of the input field
		        			button         :    "time_start_trigger",   // trigger for the calendar (button ID)
		        			timeFormat     :    "12"
    	    		});	
				  </script> 
				<form:errors path="promo.startDate" cssClass="error"/>	
			<!-- end input field -->	
	 		</div>
		  	</div>
		  	
		  	
		  	<div class="list${classIndex % 2}"><c:set var="classIndex" value="${classIndex+1}" />
		  	<div class="listfl"><div class="requiredField"><fmt:message key='endDate' />:</div></div>
		  	<div class="listp">
		  	<!-- input field -->
				<form:input path="promo.endDate" cssClass="textfield" size="19" maxlength="19" />
				  <img src="../graphics/calendarIcon.jpg" class="calendarImage" id="time_end_trigger"/>
				  <script type="text/javascript">
		    		Calendar.setup({
		        			inputField     :    "promo.endDate",   // id of the input field
		        			showsTime      :    true,
		        			ifFormat       :    "%m-%d-%Y %I:%M %p",   // format of the input field
		        			button         :    "time_end_trigger",   // trigger for the calendar (button ID)
		        			timeFormat     :    "12"
       				});	
				  </script> 
				<form:errors path="promo.endDate" cssClass="error"/>		
	 		<!-- end input field -->	
	 		</div>
		  	</div>
		  				
		  	<div class="list${classIndex % 2}"><c:set var="classIndex" value="${classIndex+1}" />
		  	<div class="listfl"><div class="helpImg"><img class="toolTipImg" title="Note:: HTML Code. Include message to appear with Promo " src="../graphics/question.gif" /></div><fmt:message key='htmlCode' />:</div>
		  	<div class="listp">
		  	<!-- input field --> 
		  		<form:textarea path="promo.htmlCode" cssClass="textfield" rows="5"/>
		  	<!-- end input field -->	
	 		</div>
	 		</div>
	 		
	 		<c:if test="${gSiteConfig['gSALES_PROMOTIONS_ADVANCED']}">
		  	<div class="list${classIndex % 2}"><c:set var="classIndex" value="${classIndex+1}" />
		  	<div class="listfl"><div class="helpImg"><img class="toolTipImg" title="Note::Select 'Where to apply promo?'. If 'Product' selected, either 'Products' or 'Brands' must be present." src="../graphics/question.gif" /></div><fmt:message key='discount' />&nbsp; <fmt:message key='type' />:</div>
		  	<div class="listp">
		  	<!-- input field -->
				<form:radiobutton value="order" path="promo.discountType" onclick="displayOptions(this.value);"/>&nbsp;&nbsp;<fmt:message key='order' /><br/>
				<form:radiobutton value="shipping" path="promo.discountType" onclick="displayOptions(this.value);"/>&nbsp;&nbsp;<fmt:message key='shipping'/><br/>
				<form:radiobutton value="product" path="promo.discountType" onclick="displayOptions(this.value);"/>&nbsp;&nbsp;<fmt:message key='product' />
			<!-- end input field -->	
	 		</div>
		  	</div>			
		  	
		  	<div id="shippingOptions">
		  	<div class="list${classIndex % 2}"><c:set var="classIndex" value="${classIndex+1}" />
		  	<div class="listfl"><div class="helpImg"><img class="toolTipImg" title="Note::Custom Shipping. Press 'ctrl' and click to select or remove multiple options. If none of the option is selected, promo will apply to all shipping options." src="../graphics/question.gif" /></div><fmt:message key='shipping' />:</div>
		  	<div class="listp">
		  	<!-- input field -->
	    		<form:select path="promo.shippingIdSet" multiple="multiple">
		          <c:forEach items="${shippingList}" var="shipping">
		            <form:option value="${shipping.id}" label="${shipping.title} Cost: ${shipping.price}"></form:option>
		          </c:forEach>      
		        </form:select>
	 		<!-- end input field -->	
	 		</div>
		  	</div>			
			</div>
			
			<div class="list${classIndex % 2}"><c:set var="classIndex" value="${classIndex+1}" />
		  	<div class="listfl"><div class="helpImg"><img class="toolTipImg" title="Note::Add Group" src="../graphics/question.gif" /></div><fmt:message key='group' />:</div>
		  	<div class="listp">
		  		<form:input path="promo.groups" cssClass="textfield" />
		  		<form:errors path="promo.groups" cssClass="error"/>	
	 		<!-- end input field -->	
	 		</div>
	 		</div>
			
		  	<c:if test="${gSiteConfig['gGROUP_CUSTOMER']}">
		  	<div class="list${classIndex % 2}"><c:set var="classIndex" value="${classIndex+1}" />
		  	<div class="listfl"><div class="helpImg"><img class="toolTipImg" title="Note::Add Group Id. Separate multiple groups with comma.  Include(OR): If customer belongs to any of these groups, promo is available. Exclude(NOR): If customer belongs to any of these groups, promo is not available. Only(AND): Promo is available only if customer belongs to all of these groups." src="../graphics/question.gif" /></div><fmt:message key='groupId' />:</div>
		  	<div class="listp">
		  	<!-- input field --> 
		  		<form:textarea path="promo.groupIds" cssClass="textfield" rows="15"/>
		  		<div ><fmt:message key='include' /><form:radiobutton value="include" path="promo.groupType" />
				<fmt:message key='exclude' /><form:radiobutton value="exclude" path="promo.groupType"/>
				<fmt:message key='only' /><form:radiobutton value="only" path="promo.groupType"/></div>
		  		<form:errors path="promo.groupIds" cssClass="error"/>	
	 		<!-- end input field -->	
	 		</div>
	 		</div>
	 		</c:if>
	 		
	 		<div class="list${classIndex % 2}"><c:set var="classIndex" value="${classIndex+1}" />
		  	<div class="listfl"><div class="helpImg"><img class="toolTipImg" title="Note::Add Group" src="../graphics/question.gif" /></div><fmt:message key='groupNote' />:</div>
		  	<div class="listp">
		  		<form:input path="promo.groupNote" cssClass="textfield" />
		  		<form:errors path="promo.groupNote" cssClass="error"/>	
	 		<!-- end input field -->	
	 		</div>
	 		</div>
	 		
	 		<div class="list${classIndex % 2}"><c:set var="classIndex" value="${classIndex+1}" />
		  	<div class="listfl"><div class="helpImg"><img class="toolTipImg" title="Note::Add State Codes. Separate multiple States with comma. Include(OR): If customer belongs to any of these states, promo is available. Exclude(NOR): If customer belongs to any of these states, promo is not available. " src="../graphics/question.gif" /></div><fmt:message key='state' />:</div>
		  	<div class="listp">
		  	<!-- input field --> 
		  		<form:textarea path="promo.states" cssClass="textfield" rows="15"/>
		  		<div ><fmt:message key='include' /><form:radiobutton value="include" path="promo.stateType" />
				<fmt:message key='exclude' /><form:radiobutton value="exclude" path="promo.stateType"/></div>
		  		<form:errors path="promo.states" cssClass="error"/>	
	 		<!-- end input field -->	
	 		</div>
	 		</div>
	 		
	 		<div id="productOptions">
		  	<c:if test="${gSiteConfig['gACCOUNTING'] == 'EVERGREEN'}">	
	 		<div class="list${classIndex % 2}"><c:set var="classIndex" value="${classIndex+1}" />
		  	<div class="listfl"><div class="helpImg"><img class="toolTipImg" title="Note:: If checked, Promo is applicable only when product status is 'Available'. Either 'Products' or 'Brands' must be present." src="../graphics/question.gif" /></div><fmt:message key='onlyIfProductAvailable' />:</div>
		  	<div class="listp">
		  	<!-- input field --> 
		  		<form:checkbox path="promo.productAvailability"/>
		  	<!-- end input field -->	
	 		</div>
	 		</div>
	 		</c:if>
	 		
	 		<div class="list${classIndex % 2}"><c:set var="classIndex" value="${classIndex+1}" />
		  	<div class="listfl"><div class="helpImg"><img class="toolTipImg" title="Note::Add Skus. Separate multiple Skus with comma. Promo would be applicable on these skus." src="../graphics/question.gif" /></div><fmt:message key='products' />:</div>
		  	<div class="listp">
		  	<!-- input field --> 
		  		<form:textarea path="promo.skus" cssClass="textfield" rows="15"/>
		  		<form:errors path="promo.skus" cssClass="error"/>	
	 		<!-- end input field -->	
	 		</div>
	 		</div>
	 		
	 		<c:if test="${gSiteConfig['gMANUFACTURER']}">
		  	<div class="list${classIndex % 2}"><c:set var="classIndex" value="${classIndex+1}" />
		  	<div class="listfl"><div class="helpImg"><img class="toolTipImg" title="Note::Add Brands. Separate multiple Brands with comma. Promo would be applicable on skus which belongs to these brands. If minimum order per brand is available, only one brand will be allowed here." src="../graphics/question.gif" /></div><fmt:message key='brands' />:</div>
		  	<div class="listp">
		  	<!-- input field --> 
		  		<form:textarea path="promo.brands" cssClass="textfield" rows="15"/>
		  		<form:errors path="promo.brands" cssClass="error"/>	
	 		<!-- end input field -->	
	 		</div>
	 		</div>
	 		
	 		
	 		<div class="list${classIndex % 2}"><c:set var="classIndex" value="${classIndex+1}" />
		  	<div class="listfl"><div class="helpImg"><img class="toolTipImg" title="Note::Brand Minimum. Promo would be applicable only if brand total on shopping cart is greater than this amount." src="../graphics/question.gif" /></div><fmt:message key='minOrderPerBrand' />:</div>
		  	<div class="listp">
		  	<!-- input field -->
				<form:input path="promo.minOrderPerBrand" cssClass="textfield" />
				<form:errors path="promo.minOrderPerBrand" cssClass="error"/>
	 		<!-- end input field -->	
	 		</div>
		  	</div>
		  	</c:if>
		  	
		  	<div class="list${classIndex % 2}"><c:set var="classIndex" value="${classIndex+1}" />
		  	<div class="listfl"><div class="helpImg"><img class="toolTipImg" title="Minimum quantity required to apply promo." src="../graphics/question.gif" /></div><fmt:message key='minimum' /> <fmt:message key='quantity' />:</div>
		  	<div class="listp">
		  	<!-- input field -->
				<form:input path="promo.minQty" cssClass="textfield" />
				<form:errors path="promo.minQty" cssClass="error"/>
	 		<!-- end input field -->	
	 		</div>
		  	</div>
		  	
		  	<!-- product category -->
		  	<div class="list${classIndex % 2}"><c:set var="classIndex" value="${classIndex+1}" />
		  	<div class="listfl"><div class="helpImg"><img class="toolTipImg" title="Note::Add Category Ids. Separate multiple category ids with comma. Promo would be applicable on skus which belongs to these Categories" src="../graphics/question.gif" /></div><fmt:message key='categoryIds' />:</div>
		  	<div class="listp">
		  	<!-- input field --> 
		  		<form:textarea path="promo.categoryIds" cssClass="textfield" rows="15"/>
		  		<form:errors path="promo.categoryIds" cssClass="error"/>	
	 		<!-- end input field -->	
	 		</div>
	 		</div>	 
		  	
		  	<!-- Parent SKU specific skus -->
		  		<c:if test="${gSiteConfig['gMASTER_SKU']}">
		  	<div class="list${classIndex % 2}"><c:set var="classIndex" value="${classIndex+1}" />
		  	<div class="listfl"><div class="helpImg"><img class="toolTipImg" title="Note::Add ParentSkus. Separate multiple Parentskus with comma. Promo would be applicable on skus which belongs to these ParentSkus." src="../graphics/question.gif" /></div><fmt:message key='parentSku' />:</div>
		  	<div class="listp">
		  	<!-- input field --> 
		  		<form:textarea path="promo.parentSkus" cssClass="textfield" rows="15"/>
		  		<form:errors path="promo.parentSkus" cssClass="error"/>	
	 		<!-- end input field -->	
	 		</div>
	 		</div>	 
		  	</c:if>
		  	
			</div>
			
	 		</c:if>  
	<!-- end tab -->		
	</div>
<!-- end tabs -->			
</div>

<!-- start button -->
	<div align="left" class="button">
	<c:choose>
		<c:when test="${promoForm.newPromo}">
		  <sec:authorize ifAnyGranted="ROLE_AEM,ROLE_ADMIN_${prop_site_id},ROLE_PROMOTION_CREATE">
			<input type="submit" value="<fmt:message key='add' />" /> 
		  </sec:authorize>	
		</c:when>
		<c:otherwise>
		  <sec:authorize ifAnyGranted="ROLE_AEM,ROLE_ADMIN_${prop_site_id},ROLE_PROMOTION_EDIT">
			<input type="submit" value="<fmt:message key='Update' />" /> 
		  </sec:authorize>
		  <sec:authorize ifAnyGranted="ROLE_AEM,ROLE_ADMIN_${prop_site_id},ROLE_PROMOTION_DELETE">	
			<input type="submit" name="delete" value="<fmt:message key='delete' />" onclick="return confirm('Are you sure you wish to delete this promotion?')"/> 
		  </sec:authorize>	
		</c:otherwise>
	</c:choose>
	<input type="submit" value="<spring:message code="cancel"/>" name="_cancel">
	</div>
<!-- end button -->	

  <!-- end table -->
    </td><td class="boxmidr" ></td></tr>
  <tr><td class="botl"></td><td class="botr"></td></tr>
  </table>
  
<!-- end main box -->  
</div>
</form:form>
<script type="text/javascript"> 
<!--
	if(document.getElementById('promo.discountType2').checked) {
		document.getElementById('shippingOptions').style.display = "block";
		document.getElementById('productOptions').style.display = "none";
	} else if(document.getElementById('promo.discountType3').checked) {
		document.getElementById('productOptions').style.display = "block";
		document.getElementById('shippingOptions').style.display = "none";
	} else {
		document.getElementById('shippingOptions').style.display = "none";
		document.getElementById('productOptions').style.display = "none";
	}
</script>
  
</sec:authorize> 

  </tiles:putAttribute>    
</tiles:insertDefinition>