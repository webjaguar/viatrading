<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="/WEB-INF/tlds/spring.tld" prefix="spring" %>
<%@ taglib uri="http://www.springframework.org/security/tags" prefix="sec" %>

<tiles:insertDefinition name="admin.promos" flush="true">
  <tiles:putAttribute name="content" type="string">
  
  <script type="text/javascript" src="../../admin/javascript/multiple.open.accordion1.2.js"></script>
<script language="JavaScript" type="text/JavaScript">
<!--
window.addEvent('domready', function(){			
			var tabs1 = new SimpleTabs($('tab-block-1'), { selector: 'h4'});
			var myToggler = $$('div.arrow_drop');
			// Create the accordian
			new MultipleOpenAccordion(myToggler, $$('div.information'), {
				transition: Fx.Transitions.sineOut,display:0,
				onActive: function(myToggler){
						myToggler.removeClass('arrowImageRight').addClass('arrowImageDown');
					},
				onBackground: function(myToggler){
				myToggler.removeClass('arrowImageDown').addClass('arrowImageRight');
					}
				});
		});
//-->
</script>
<sec:authorize ifAnyGranted="ROLE_AEM,ROLE_ADMIN_${prop_site_id}">
<script language="JavaScript" type="text/JavaScript">
<!--
window.addEvent('domready', function(){			
			var tabs1 = new SimpleTabs($('tab-block-1'), { selector: 'h4'});
		});
//-->
</script>
<form method="post" name="uploadForm" enctype="multipart/form-data">
<!-- main box -->
  <div id="mboxfull">
  
  <!-- start table -->
  <table cellpadding="0" cellspacing="0" border="0" class="module" >
  <tr>
    <td class="topl_g">
	  
	  <!-- breadcrumb -->
	  <p class="breadcrumb">
	    <a href="../promos/promoList.jhtm">Promos</a> &gt;
	    import
	  </p>
	  
	  <!-- Error Message -->
	  <c:if test="${!empty message}">
		 <div class="uploadError"><spring:message code="${message}" arguments="${arguments}"/></div>
	  </c:if> 

  
  </td><td class="topr_g" ></td></tr>
  <tr><td class="boxmidlrg" >

 <!-- tabs -->
 <div id="tab-block-1">
	<h4 title="Import">Import</h4>
	<div>
	<!-- start tab -->

        <c:set var="classIndex" value="0" />
        
	  	<div class="listdivi ln tabdivi"></div>
	  	<div class="listdivi"></div>
	   
		  	<div class="list${classIndex % 2}"><c:set var="classIndex" value="${classIndex+1}" />
		  	<div class="listfl">Import Promos:</div>
		  	<div class="listp">
		  	<!-- input field -->

				<c:if test="${invalidItems != null}">
				<table border="0" cellpadding="0" cellspacing="0" width="100%" style="padding-left:5px;padding-right:5px;">
				<tr height="20">
				  <td class="importIndexCol importHdr">&nbsp;</td>
				  <td class="importHdr"><fmt:message key="excelLineNum" /></td>
				  <td class="importHdr"><spring:message code="promoCode"/></td>
				  <td class="importHdr" width="100%"><fmt:message key="reason" /></td>
				</tr>
				<c:forEach items="${invalidItems}" var="item" varStatus="status">
				<tr height="20" class="row${status.index % 2}" onmouseover="changeStyleClass(this,'rowOver')" onmouseout="changeStyleClass(this,'row${status.index % 2}')">
				  <td class="importIndexCol import"><c:out value="${status.count}"/>.</td>
				  <td class="import" align="center"><c:out value="${item['rowNum']}"/></td>
				  <td class="import"><c:out value="${item['emailAddress']}"/></td>
				  <td class="import"><c:out value="${item['reason']}"/></td>
				</tr>
				</c:forEach>
				</table>
				<br><br>
				</c:if>
				
				<table class="form" width="100%">
				  <tr>
				    <td class="formName"><spring:message code="excelFile" text="Excel File"/>:</td>
				    <td>
						<input value="browse" type="file" name="file"/>
				    </td>
				  </tr>
				</table>
		    <!-- end input field -->   
	  	
	<!-- end tab -->        
	</div>  
	
<!-- end tabs -->			
</div>
<div class="list${classIndex % 2}">
	<div class="helpNote">
			Instructions:
	</div>
</div>

<div style="margin:0px 0px 0px 0px;"> 
	<div style="clear: both;"></div>
	<div class="information">
	  <div  style="border: 2px solid #7e96a6;background-color: #E9E9DC;padding:10px;">
		<div class="helpNote">
		   <ol>
		   	  <li>Should have promocode, minimumorder, discount, startdate, enddate, discounttype </li>
			  <li>DATE FORMAT: 
				  <ul>
				  	<li><b>Start Date:</b> 04/27/2012 10:00 AM</li>
				  	<li><b>End Date:</b> 04/27/2012 11:00 PM</li>
				  </ul> 			  	  
			  </li>
			  <li>If Discount field is empty, then the value is set to 0.00 </li>
			  <li>If Minimum Order field is empty, value is set to 0.00</li>
		   </ol>
		</div>
	  </div>
	</div>
</div>
<div class="arrow_drop tabsliderUp"></div>

<!-- start button -->
<div align="left" class="button"> 
    <input type="submit" name="__upload" value="<spring:message code="fileUpload"/>">
</div>
<!-- end button -->	     
  
  <!-- end table -->
    </td><td class="boxmidr" ></td></tr>
  <tr><td class="botl"></td><td class="botr"></td></tr>
  </table>
  
<!-- end main box -->  
</div>
</form>

</sec:authorize>
  </tiles:putAttribute>    
</tiles:insertDefinition>

