<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/security/tags" prefix="sec" %>
====via Deal List
<tiles:insertDefinition name="admin.promos.deals" flush="true">
    <tiles:putAttribute name="title" value="Promotions Manager" />
	<tiles:putAttribute name="content" type="string">
	
<form action="viaDealList.jhtm" method="post">
<!-- main box -->
  <div id="mboxfull">
  
  <!-- start table -->
  <table cellpadding="0" cellspacing="0" border="0" class="module" >
  <tr>
    <td class="topl_g">
	  
	  <!-- breadcrumb -->
	  <p class="breadcrumb">
	    <a href="../promos"><fmt:message key="promosTabTitle"/></a> &gt;
	    Via Deals 
	  </p>
	  
	  <!-- header image -->
	  <img class="headerImage" src="../graphics/salesTag.gif" />
	  
	  </td><td class="topr_g" ></td></tr>
  <tr><td class="boxmidlrg" >

 <!-- tabs -->
 <div class="tab-wrapper">
	<h4 title=""></h4>
	<div>
	<!-- start tab -->

	  	<div class="listdivi"></div>
	   
		  	<div class="listlight">
		  	<!-- input field --> 
		  	<table border="0" cellpadding="0" cellspacing="1" width="100%">
				<tr>
					<td>&nbsp;
					</td>
					<c:if test="${model.viaDeals.nrOfElements > 0}">
						<td class="pageShowing">
							<fmt:message key="showing">
								<fmt:param value="${model.viaDeals.firstElementOnPage + 1}" />
								<fmt:param value="${model.viaDeals.lastElementOnPage + 1}" />
								<fmt:param value="${model.viaDeals.nrOfElements}" />
							</fmt:message>
						</td>
					</c:if>
					<td class="pageNavi">
						Page
						<select name="page" id="page" onchange="submit()">
							<c:forEach begin="1" end="${model.viaDeals.pageCount}"
								var="page">
								<option value="${page}"
									<c:if test="${page == (model.viaDeals.page+1)}">selected</c:if>>
									${page}
								</option>
							</c:forEach>
						</select> 
						of
						<c:out value="${model.viaDeals.pageCount}" /> 
						|
						<c:if test="${model.viaDeals.firstPage}">
							<span class="pageNaviDead"><fmt:message key="previous" /></span>
						</c:if>
						<c:if test="${not model.viaDeals.firstPage}">
							<a
								href="<c:url value="viaDealList.jhtm"><c:param name="page" value="${model.viaDeals.page}"/><c:param name="parent" value="${model.search['parent']}"/><c:param name="size" value="${model.viaDeals.pageSize}"/></c:url>"
								class="pageNaviLink"><fmt:message key="previous" /></a>
						</c:if>
						|
						<c:if test="${model.viaDeals.lastPage}">
							<span class="pageNaviDead"><fmt:message key="next" /></span>
						</c:if>
						<c:if test="${not model.viaDeals.lastPage}">
							<a
								href="<c:url value="viaDealList.jhtm"><c:param name="page" value="${model.viaDeals.page+2}"/><c:param name="parent" value="${model.search['parent']}"/><c:param name="size" value="${model.viaDeals.pageSize}"/></c:url>"
								class="pageNaviLink"><fmt:message key="next" /></a>
						</c:if>
					</td>
				</tr>
			</table>

			<table border="0" cellpadding="0" cellspacing="0" width="100%"
				class="listings">
				<tr class="listingsHdr2">

					<td class="indexCol">&nbsp;</td>
					<td class="listingsHdr3">
						<fmt:message key="title" />
					</td>
					<td class="listingsHdr3">
						Parent Sku
					</td>
<%-- 					<td class="listingsHdr3">
						<fmt:message key="get" />
					</td> --%>
					<td class="listingsHdr3">
						<fmt:message key="discount" />
					</td>
					<td class="listingsHdr3">
						<fmt:message key="dealDate" />
					</td>
					<td class="listingsHdr3">
						<fmt:message key="inEffect" />
					</td>
				</tr>
				<c:forEach items="${model.viaDeals.pageList}" var="deal"	varStatus="status">
					<tr class="row${status.index % 2}"	onmouseover="changeStyleClass(this,'rowOver')" 	onmouseout="changeStyleClass(this,'row${status.index % 2}')">
						<td class="indexCol"><c:out value="${status.count + model.viaDeals.firstElementOnPage}" />.</td>
						<td class="nameCol">
							<a href="viaDeal.jhtm?id=${deal.dealId}" class="nameLink"><c:out
									value="${deal.title}" />
							</a>
						</td>
<%-- 						<td class="nameCol">
							<c:choose><c:when test="${viaDeal.buySku != null}">${viaDeal.buySku}</c:when><c:otherwise><fmt:message key="dollar"/>${viaDeal.buyAmount}</c:otherwise> </c:choose> 
						</td> --%>
						<td class="nameCol">
							<c:out value="${deal.parentSku}"></c:out>
						</td>
						<td class="nameCol">
							   <c:out value="${deal.discount}"></c:out> <c:choose><c:when test="${deal.discountType}">%</c:when><c:otherwise>USD</c:otherwise> </c:choose> 
						</td>
						<td class="nameCol">
							<fmt:formatDate type="date" dateStyle="full" value="${deal.startDate}" pattern="MM/dd/yyyy (hh:mm)a zz "/> - <fmt:formatDate type="date" dateStyle="full" value="${deal.endDate}" pattern="MM/dd/yyyy (hh:mm)a zz"/> 
						</td>
						<td class="nameCol">
							<c:if test="${deal.inEffect}"><div align="left"><img src="../graphics/checkbox.png" /></div></c:if>
						</td>
					</tr>
				</c:forEach>
				<c:if test="${model.viaDeals.nrOfElements == 0}">
					<tr class="emptyList">
						<td colspan="8">
							&nbsp;
						</td>
					</tr>
				</c:if>
			</table>

			<c:if test="${model.nOfdeals > 0}">
				<div class="sup">
					&sup1; <fmt:message key="toEditClickDealTitle" /> 
				</div>
			</c:if>
			<table border="0" cellpadding="0" cellspacing="1" width="100%">
				<tr>
				  <td class="pageSize">
					<select name="size"
						onchange="document.getElementById('page').value=1;submit()">
						<c:forTokens items="10,25,50" delims="," var="current">
							<option value="${current}"
								<c:if test="${current == dealSearch.pageSize}">selected</c:if>>
								${current}
								<fmt:message key="perPage" />
							</option>
						</c:forTokens>
					</select>
				  </td>
				</tr>
			</table>
		  	<!-- end input field -->  	  
		  	</div>
		  	
		  	<!-- start button -->
		  	<div align="left" class="button">
 			<c:choose>
				<c:when test="${gSiteConfig['gDEALS'] > model.nOfdeals}">
				  <sec:authorize ifAnyGranted="ROLE_AEM,ROLE_ADMIN_${prop_site_id},ROLE_PROMOTION_CREATE">
					<input type="submit" name="__addDeal" value="addDeal"/> &nbsp;
				  </sec:authorize>	
				</c:when>
				<c:otherwise>
				  <div class="note_list">
					<fmt:message key="toAddMoreDealContactWebMaster" />
				  </div>	
				</c:otherwise>
			</c:choose>	
			</div>
		  	<!-- end button -->	
	
	  	
	<!-- end tab -->        
	</div>
<!-- end tabs -->			
</div>
  
  <!-- end table -->
   </td><td class="boxmidr" ></td></tr>
  <tr><td class="botl"></td><td class="botr"></td></tr>
  </table>
  
<!-- end main box -->  
</div>  
</form> 	

	</tiles:putAttribute>
</tiles:insertDefinition>
