<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="/WEB-INF/tlds/spring.tld" prefix="spring" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib uri="http://www.springframework.org/security/tags" prefix="sec" %>

<tiles:insertDefinition name="admin.promos.viaDeals" flush="true">
  <tiles:putAttribute name="title" value="Deals Manager" />
  <tiles:putAttribute name="content" type="string">
<sec:authorize ifAnyGranted="ROLE_AEM,ROLE_ADMIN_${prop_site_id},ROLE_DEAL_CREATE">
<link rel="stylesheet" type="text/css" media="all" href="./../../admin/javascript/jscalendar-1.0/skins/aqua/theme.css" title="Aqua" /> <!-- calendar stylesheet -->
<script type="text/javascript" src="./../../admin/javascript/jscalendar-1.0/calendar.js"></script> <!-- main calendar program -->
<script type="text/javascript" src="./../../admin/javascript/jscalendar-1.0/lang/calendar-en.js"></script>
<script type="text/javascript" src="./../../admin/javascript/jscalendar-1.0/calendar-setup.js"></script>
<script language="JavaScript"> 
<!--
window.addEvent('domready', function(){			
	var tabs1 = new SimpleTabs($('tab-block-1'), { selector: 'h4'});
	var Tips1 = new Tips($$('.toolTipImg'));
});
//--> 
</script>
<form:form id="viaDealForm" commandName="viaDealForm" method="post" >
<!-- main box -->
  <div id="mboxfull">
  
  <!-- start table -->
  <table cellpadding="0" cellspacing="0" border="0" class="module" >
  <tr>
    <td class="topl_g">
	  
	  <!-- breadcrumb -->
 	  <p class="breadcrumb">
	    <a href="../promos"><fmt:message key="promosTabTitle"/></a> &gt;
	    <a href="../promos/dealList.jhtm">Via Deals</a> &gt;
	    ${viaDealForm.viaDeal.title} 
	  </p> 
	  
	  <!-- Error Message -->
	  <%--<c:if test="${message != null}">
  		<div class="message"><fmt:message key="${message}"/></div>
  	  </c:if> --%>
  	  <spring:hasBindErrors name="viaDealForm">
		<div class="message">Please fix all errors!</div>
	  </spring:hasBindErrors>
	  
  </td><td class="topr_g" ></td></tr>
  <tr><td class="boxmidlrg" >

 <!-- tabs -->
 <div id="tab-block-1">
	<h4 title="Deal Form"><fmt:message key="deals"/></h4>
	<div>
	<!-- start tab -->
         <c:set var="classIndex" value="0" />
         
	  	<div class="listdivi ln tabdivi"></div>
	  	<div class="listdivi"></div>
	   
		  	<div class="list${classIndex % 2}"><c:set var="classIndex" value="${classIndex+1}" />
		  	<div class="listfl"><div class="helpImg"><img class="toolTipImg" title="Note::Title. Provide as much details as possible. " src="../graphics/question.gif" /></div><div class="requiredField"><fmt:message key='title' />:</div></div>
		  	<div class="listp">
		  	<!-- input field -->
				<form:input path="viaDeal.title" cssClass="textfield" />
				<form:errors path="viaDeal.title" cssClass="error"/>	
	 		<!-- end input field -->	
	 		</div>
		  	</div>			
	
<%-- 		  	<div class="list${classIndex % 2}" id="sku"><c:set var="classIndex" value="${classIndex+1}" />
		  	<div class="listfl"><div class="helpImg"><img class="toolTipImg" title="Note::On purchase of this Sku, customer gets deal. Also, select quantity from drop down." src="../graphics/question.gif" /></div><fmt:message key='buyThisSku' />:</div>
		  	<div class="listp">
		  	<!-- input field -->
				<form:input path="viaDeal.buySku" cssClass="textfield" />
				<form:select path="viaDeal.buyQuantity">
				  <form:option value="0" label="Select Quantity"></form:option>
				  <c:forEach var="number" items="1,2,3,4,5,6,7,8,9,10">
				    <form:option value="${number}" label="${number}"></form:option>
				  </c:forEach>
				</form:select>
				<form:errors path="viaDeal.buySku" cssClass="error"/>	
				<form:errors path="viaDeal.buyQuantity" cssClass="error"/>	
	 		<!-- end input field -->	
	 		</div>
		  	</div> --%>
		  	
		  	<div class="list${classIndex % 2}"><c:set var="classIndex" value="${classIndex+1}" />
	  		<div class="listfl"><div class="helpImg"><img class="toolTipImg" title="Note::the promotion works on all of children sku." src="../graphics/question.gif" /></div><fmt:message key="parentSku" />:</div>
	  		<div class="listp">
	  		<!-- input field -->
				<form:input  path="viaDeal.parentSku" cssClass="textfield"/>
				<form:errors path="viaDeal.parentSku" cssClass="error"/>	
	  	    <!-- end input field -->   	
	  		</div>
	  		</div>		
		  	
		  	<div class="list${classIndex % 2}"><c:set var="classIndex" value="${classIndex+1}" />
	  		<div class="listfl"><div class="helpImg"><img class="toolTipImg" title="Note::trigger if the total qty or fixed amoutn is greater." src="../graphics/question.gif" /></div>Child Sku Discount Trigger:</div>
	  		<div class="listp">
	  		<!-- input field -->
				<%-- <form:checkbox path="deal.parentSku"/> --%>
				<form:input path="viaDeal.triggerThreshold" cssClass="textfield"/>
				<form:select path="viaDeal.triggerType">
					<form:option value="true">QTY (total)</form:option>
					<form:option value="false"><fmt:message key="FIXED" /> $ (total)</form:option>
				</form:select>
				<form:errors path="viaDeal.triggerThreshold" cssClass="error"/>
				<%-- <span class="helpNote"><p><fmt:message key='maxis100' /> (<fmt:message key='forPercentage' />)</p></span> --%>
	  	    <!-- end input field -->   	
	  		</div>
	  		</div>			
	
		  	<div class="list${classIndex % 2}"><c:set var="classIndex" value="${classIndex+1}" />
		  	<div class="listfl"><div class="requiredField"><fmt:message key='discount' />:</div></div>
		  	<div class="listp">
		  	<!-- input field -->
				<form:input path="viaDeal.discount" cssClass="textfield"/>
				<form:select path="viaDeal.discountType">
					<form:option value="true" label="%" />
					<form:option value="false"><fmt:message key="FIXED" /> $</form:option>
				</form:select>
				<form:errors path="viaDeal.discount" cssClass="error"/>
				<span class="helpNote"><p><fmt:message key='maxis100' /> (<fmt:message key='forPercentage' />)</p></span>
	 		<!-- end input field -->	
	 		</div>
		  	</div>			

		  	<div class="list${classIndex % 2}"><c:set var="classIndex" value="${classIndex+1}" />
		  	<div class="listfl"><div class="requiredField"><fmt:message key='startDate' />:</div></div>
		  	<div class="listp">
		  	<!-- input field -->
				<span class="helpNote"><p><fmt:message key='currentTime' />: <c:out value="${model.currentTime}"></c:out></p></span>
				<form:input path="viaDeal.startDate" cssClass="textfield" size="19" maxlength="19" />
				  <img src="../graphics/calendarIcon.jpg" class="calendarImage" id="time_start_trigger"/>
				  <script type="text/javascript">
		    		Calendar.setup({
		        			inputField     :    "viaDeal.startDate",   // id of the input field
		        			showsTime      :    true,
		        			ifFormat       :    "%m-%d-%Y %I:%M %p",   // format of the input field
		        			button         :    "time_start_trigger",   // trigger for the calendar (button ID)
		        			timeFormat     :    "12"
    	    		});	
				  </script> 
				<form:errors path="viaDeal.startDate" cssClass="error"/>	
			<!-- end input field -->	
	 		</div>
		  	</div>
		  	
		  	
		  	<div class="list${classIndex % 2}"><c:set var="classIndex" value="${classIndex+1}" />
		  	<div class="listfl"><div class="requiredField"><fmt:message key='endDate' />:</div></div>
		  	<div class="listp">
		  	<!-- input field -->
				<form:input path="viaDeal.endDate" cssClass="textfield" size="19" maxlength="19" />
				  <img src="../graphics/calendarIcon.jpg" class="calendarImage" id="time_end_trigger"/>
				  <script type="text/javascript">
		    		Calendar.setup({
		        			inputField     :    "viaDeal.endDate",   // id of the input field
		        			showsTime      :    true,
		        			ifFormat       :    "%m-%d-%Y %I:%M %p",   // format of the input field
		        			button         :    "time_end_trigger",   // trigger for the calendar (button ID)
		        			timeFormat     :    "12"
       				});	
				  </script> 
				<form:errors path="viaDeal.endDate" cssClass="error"/>		
	 		<!-- end input field -->	
	 		</div>
		  	</div>
		  	
		  	
		  	<div class="list${classIndex % 2}"><c:set var="classIndex" value="${classIndex+1}" />
		  	<div class="listfl"><div class="helpImg"><img class="toolTipImg" title="Note:: HTML Code. Include message to appear with SKU " src="../graphics/question.gif" /></div><fmt:message key='htmlCode' />:</div>
		  	<div class="listp">
		  	<!-- input field --> 
		  		<form:textarea path="viaDeal.htmlCode" cssClass="textfield" rows="5"/>
		  	<!-- end input field -->	
	 		</div>
	 		</div>
	 					
	<!-- end tab -->		
	</div>
<!-- end tabs -->			
</div>

<!-- start button -->
	<div align="left" class="button">
	<c:choose>
		<c:when test="${viaDealForm.newDeal}">
		  <sec:authorize ifAnyGranted="ROLE_AEM,ROLE_ADMIN_${prop_site_id},ROLE_DEAL_CREATE">
			<input type="submit" value="<fmt:message key='add' />" /> 
		  </sec:authorize>	
		</c:when>
		<c:otherwise>
		  <sec:authorize ifAnyGranted="ROLE_AEM,ROLE_ADMIN_${prop_site_id},ROLE_DEAL_CREATE">
			<input type="submit" value="<fmt:message key='Update' />" /> 
		  </sec:authorize>
		  <sec:authorize ifAnyGranted="ROLE_AEM,ROLE_ADMIN_${prop_site_id},ROLE_DEAL_DELETE">	
			<input type="submit" name="delete" value="<fmt:message key='delete' />" onclick="return confirm('Are you sure you want to delete this deal?')"/> 
		  </sec:authorize>	
		</c:otherwise>
	</c:choose>
	<input type="submit" value="<spring:message code="cancel"/>" name="_cancel">
	</div>
<!-- end button -->	

  <!-- end table -->
    </td><td class="boxmidr" ></td></tr>
  <tr><td class="botl"></td><td class="botr"></td></tr>
  </table>
  
<!-- end main box -->  
</div>
</form:form>
</sec:authorize> 

  </tiles:putAttribute>    
</tiles:insertDefinition>