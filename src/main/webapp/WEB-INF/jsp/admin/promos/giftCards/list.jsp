<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<tiles:insertDefinition name="admin.promos.giftCard" flush="true">
    <tiles:putAttribute name="title"  value="Gift Card Manager" />
	<tiles:putAttribute name="content" type="string">
		
<form action="giftCardList.jhtm" method="post">
<!-- main box -->
  <div id="mboxfull">
  
  <!-- start table -->
  <table cellpadding="0" cellspacing="0" border="0" class="module" >
  <tr>
    <td class="topl_g">
	  
	  <!-- breadcrumb -->
	  <p class="breadcrumb">
	    <a href="../promos">Sales/Promos</a> &gt;
	    <c:choose>
			  <c:when test="${!empty model.giftCards}">
			  <a href="../promos/giftCardList.jhtm">Gift Card</a>
			  </c:when>
			  <c:when test="${!empty model.giftCardOrders}">
			  <a href="../promos/giftCardList.jhtm">Gift Card Orders</a>
			  </c:when>
		</c:choose>	  
	  </p>
	  
	  <!-- header image -->
	  <img class="headerImage" src="../graphics/giftCard.gif" />
	  
	  </td><td class="topr_g" ></td></tr>
  <tr><td class="boxmidlrg" >

 <!-- tabs -->
 <div class="tab-wrapper">
	<h4 title=""></h4>
	<div>
	<!-- start tab -->

	  	<div class="listdivi"></div>
	    
		  	<div class="listlight">
		  	<!-- input field --> 
		  	

			<c:choose>
			  <c:when test="${!empty model.giftCards}">
			  <table border="0" cellpadding="0" cellspacing="1" width="100%">
					<tr>
						<td>&nbsp;
						</td>
						<c:if test="${model.giftCards.nrOfElements > 0}">
							<td class="pageShowing">
								<fmt:message key="showing">
									<fmt:param value="${model.giftCards.firstElementOnPage + 1}" />
									<fmt:param value="${model.giftCards.lastElementOnPage + 1}" />
									<fmt:param value="${model.giftCards.nrOfElements}" />
								</fmt:message>
							</td>
						</c:if>
						<td class="pageNavi">
							Page
							<select name="page" id="page" onchange="submit()">
								<c:forEach begin="1" end="${model.giftCards.pageCount}"
									var="page">
									<option value="${page}"
										<c:if test="${page == (model.giftCards.page+1)}">selected</c:if>>
										${page}
									</option>
								</c:forEach>
							</select> 
							of
							<c:out value="${model.giftCards.pageCount}" /> 
							|
							<c:if test="${model.giftCards.firstPage}">
								<span class="pageNaviDead"><fmt:message key="previous" /></span>
							</c:if>
							<c:if test="${not model.giftCards.firstPage}">
								<a
									href="<c:url value="salesTagList.jhtm"><c:param name="page" value="${model.giftCards.page}"/><c:param name="size" value="${model.giftCards.pageSize}"/></c:url>"
									class="pageNaviLink"><fmt:message key="previous" /></a>
							</c:if>
							|
							<c:if test="${model.giftCards.lastPage}">
								<span class="pageNaviDead"><fmt:message key="next" /></span>
							</c:if>
							<c:if test="${not model.giftCards.lastPage}">
								<a
									href="<c:url value="giftCardList.jhtm"><c:param name="page" value="${model.giftCards.page+2}"/><c:param name="size" value="${model.giftCards.pageSize}"/></c:url>"
									class="pageNaviLink"><fmt:message key="next" /></a>
							</c:if>
						</td>
					</tr>
				</table>
			    <table border="0" cellpadding="0" cellspacing="0" width="100%" class="listings">
					<tr class="listingsHdr2">
	
						<td class="indexCol">&nbsp;</td>
						<td class="listingsHdr3">&nbsp;</td>
						<td class="listingsHdr3"><fmt:message key="claimCode" /></td>
						<td class="listingsHdr3"><fmt:message key="amount" /></td>
						<td class="listingsHdr3"><fmt:message key="dateCreated" /></td>
						<td class="listingsHdr3"><fmt:message key="active" /></td>
						<td class="listingsHdr3"><fmt:message key="amountRedeemed" /></td>
						<td class="listingsHdr3"><fmt:message key="dateRedeemed" /></td>
						<td class="listingsHdr3"><fmt:message key="redeemedBy" /></td>
					</tr>
					<c:forEach items="${model.giftCards.pageList}" var="giftCard"	varStatus="status">
						<tr class="row${status.index % 2}"	onmouseover="changeStyleClass(this,'rowOver')" 	onmouseout="changeStyleClass(this,'row${status.index % 2}')">
							<td class="indexCol">
								<c:set var="claimCode" scope="page" value="${model.giftCard.nrOfElements}" />
								<c:out value="${status.count + model.giftCard.firstElementOnPage}" />.
							</td>
							<td class="nameCol">&nbsp;</td>
							<td class="nameCol"><a href="giftCardForm.jhtm?id=${giftCard.code}"><c:out value="${giftCard.code}" /></a></td>
							<td class="nameCol"><fmt:message key="${siteConfig['CURRENCY'].value}" /><fmt:formatNumber value="${giftCard.amount}" pattern="#,##0.00" /></td>
							<td class="nameCol"><fmt:formatDate type="date" dateStyle="full" value="${giftCard.dateCreated}" pattern="MM/dd/yyyy"/></td>
							<td class="nameCol">
							 <c:choose>
							  <c:when test="${giftCard.active}"><img src="../graphics/checkbox.png" alt="" title="" border="0"></c:when>
							  <c:otherwise><img src="../graphics/box.png" alt="" title="" border="0"></c:otherwise>
							 </c:choose>
							</td>
							<td class="nameCol"><fmt:message key="${siteConfig['CURRENCY'].value}" /><fmt:formatNumber value="${giftCard.amountRedeemed}" pattern="#,##0.00" /></td>
							<td class="nameCol"><fmt:formatDate type="date" dateStyle="full" value="${giftCard.dateRedeemed}" pattern="MM/dd/yyyy"/></td>
							<td class="nameCol"><a href="../customers/customer.jhtm?id=${giftCard.redeemedById}" ><c:out value="${giftCard.redeemedByFirstName}" /> <c:out value="${giftCard.redeemedByLastName}" /></a></td>
						</tr>
					</c:forEach>
				</table>
			  <table border="0" cellpadding="0" cellspacing="1" width="100%">
				<tr>
				  <td class="pageSize">
					<select name="size"
						onchange="document.getElementById('page').value=1;submit()">
						<c:forTokens items="10,25,50" delims="," var="current">
							<option value="${current}"
								<c:if test="${current == model.giftCards.pageSize}">selected</c:if>>
								${current}
								<fmt:message key="perPage" />
							</option>
						</c:forTokens>
					</select>
				  </td>
				</tr>
			</table>
			  </c:when>
			  <c:otherwise>
			  <table border="0" cellpadding="0" cellspacing="1" width="100%">
					<tr>
						<td>&nbsp;
						</td>
						<c:if test="${model.giftCardOrders.nrOfElements > 0}">
							<td class="pageShowing">
								<fmt:message key="showing">
									<fmt:param value="${model.giftCardOrders.firstElementOnPage + 1}" />
									<fmt:param value="${model.giftCardOrders.lastElementOnPage + 1}" />
									<fmt:param value="${model.giftCardOrders.nrOfElements}" />
								</fmt:message>
							</td>
						</c:if>
						<td class="pageNavi">
							Page
							<select name="page" id="page" onchange="submit()">
								<c:forEach begin="1" end="${model.giftCardOrders.pageCount}"
									var="page">
									<option value="${page}"
										<c:if test="${page == (model.giftCardOrders.page+1)}">selected</c:if>>
										${page}
									</option>
								</c:forEach>
							</select> 
							of
							<c:out value="${model.giftCardOrders.pageCount}" /> 
							|
							<c:if test="${model.giftCardOrders.firstPage}">
								<span class="pageNaviDead"><fmt:message key="previous" /></span>
							</c:if>
							<c:if test="${not model.giftCardOrders.firstPage}">
								<a
									href="<c:url value="salesTagList.jhtm"><c:param name="page" value="${model.giftCardOrders.page}"/><c:param name="size" value="${model.giftCardOrders.pageSize}"/></c:url>"
									class="pageNaviLink"><fmt:message key="previous" /></a>
							</c:if>
							|
							<c:if test="${model.giftCardOrders.lastPage}">
								<span class="pageNaviDead"><fmt:message key="next" /></span>
							</c:if>
							<c:if test="${not model.giftCardOrders.lastPage}">
								<a
									href="<c:url value="giftCardList.jhtm"><c:param name="page" value="${model.giftCardOrders.page+2}"/><c:param name="size" value="${model.giftCardOrders.pageSize}"/></c:url>"
									class="pageNaviLink"><fmt:message key="next" /></a>
							</c:if>
						</td>
					</tr>
				</table>
			    <table border="0" cellpadding="0" cellspacing="0" width="100%" class="listings">
					<tr class="listingsHdr2">
						<td class="indexCol">&nbsp;</td>
						<td class="listingsHdr3">&nbsp;</td>
						<td class="listingsHdr3"><fmt:message key="orderNumber" /></td>
						<td class="listingsHdr3"><fmt:message key="orderDate" /></td>
						<td class="listingsHdr3"><fmt:message key="subTotal" /></td>
						<td class="listingsHdr3"><fmt:message key="numberOfGiftCard" /></td>
						<td class="listingsHdr3"><fmt:message key="paymentMethod" /></td>
					</tr>
					<c:forEach items="${model.giftCardOrders.pageList}" var="giftCardOrder"	varStatus="status">
						<tr class="row${status.index % 2}"	onmouseover="changeStyleClass(this,'rowOver')" 	onmouseout="changeStyleClass(this,'row${status.index % 2}')">
							<td class="indexCol">
								<c:set var="claimCode" scope="page" value="${model.giftCardOrders.nrOfElements}" />
								<c:out value="${status.count + model.giftCardOrders.firstElementOnPage}" />.
							</td>
							<td class="nameCol">&nbsp;</td>
							<td class="nameCol"><a href="giftCardList.jhtm?giftcardOrderId=${giftCardOrder.giftCardOrderId}"><c:out value="${giftCardOrder.giftCardOrderId}" /></a></td>
							<td class="nameCol"><fmt:formatDate type="date" timeStyle="default" value="${giftCardOrder.dateCreated}"/></td>
							<td class="nameCol"><fmt:message key="${siteConfig['CURRENCY'].value}" /><fmt:formatNumber value="${giftCardOrder.subTotal}" pattern="#,##0.00" /></td>
							<td class="nameCol"><c:out value="${giftCardOrder.quantity}" /></td>
							<td class="nameCol">
							<c:choose>
								<c:when test="${giftCardOrder.paymentMethod != 'Credit Card'}">
									<c:out value="${giftCardOrder.paymentMethod}"/>
								</c:when>
								<c:otherwise>								
									<fmt:message key='${giftCardOrder.creditCard.type}' />
								</c:otherwise>
							</c:choose>
							</td>
						</tr>
					</c:forEach>
				</table>
				<table border="0" cellpadding="0" cellspacing="1" width="100%">
				<tr>
				  <td class="pageSize">
					<select name="size"
						onchange="document.getElementById('page').value=1;submit()">
						<c:forTokens items="10,25,50" delims="," var="current">
							<option value="${current}"
								<c:if test="${current == model.giftCardOrders.pageSize}">selected</c:if>>
								${current}
								<fmt:message key="perPage" />
							</option>
						</c:forTokens>
					</select>
				  </td>
				</tr>
			  </table>
			  </c:otherwise>
			
			</c:choose>
			

			
		  	<!-- end input field -->  	  
		  	</div>
		  	
		  	<!-- start button --> 
		  	<div align="left" class="button">
		  	</div>
		  	<!-- end button -->	
	  	
	<!-- end tab -->        
	</div>
<!-- end tabs -->			
</div>
  
  <!-- end table -->
   </td><td class="boxmidr" ></td></tr>
  <tr><td class="botl"></td><td class="botr"></td></tr>
  </table>
  
<!-- end main box -->  
</div>  
</form>	

	</tiles:putAttribute>
</tiles:insertDefinition>




