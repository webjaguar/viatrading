<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="/WEB-INF/tlds/spring.tld" prefix="spring" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib uri="http://www.springframework.org/security/tags" prefix="sec" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<link href="<c:url value="/assets/invoice.css"/>" rel="stylesheet" type="text/css"/>


<tiles:insertDefinition name="admin.promos.giftCard" flush="true">
  <tiles:putAttribute name="content" type="string"> 

<script language="JavaScript"> 
<!--
window.addEvent('domready', function(){			
			var tabs1 = new SimpleTabs($('tab-block-1'), { selector: 'h4'});
		});
//--> 
</script>

<!-- main box -->
  <div id="mboxfull">
  
  <!-- start table -->
  <table cellpadding="0" cellspacing="0" border="0" class="module" >
  <tr>
    <td class="topl_g">
	  
	  <form action="giftCardList.jhtm" method="post" >
	  <!-- breadcrumb -->
	  <p class="breadcrumb">
	    <a href="../promos">Sales/Promos</a> &gt;
	    <a href="../promos/giftCardList.jhtm"><fmt:message key='giftCard' /></a> &gt;
	    <a href="../promos/giftCardList.jhtm?giftcardOrderId=${giftCardOrderId}">${giftCardOrderId}</a>
	  </p>
	  </form>
	  
	  <!-- Error Message -->
	  <c:if test="${message != null}">
  		<div class="message"><fmt:message key="${message}"/></div>
  	  </c:if> 
	  
  </td><td class="topr_g" ></td></tr>
  <tr><td class="boxmidlrg" >
<form:form id="giftCardForm" commandName="giftCardForm" method="post" >
 <!-- tabs -->
 <div id="tab-block-1">
	<h4 title="<fmt:message key='giftCard' />"><fmt:message key='giftCard' /></h4>
	<div>
	<!-- start tab -->
        <c:set var="classIndex" value="0" />
         
	  	<div class="listdivi ln tabdivi"></div>
	  	<div class="listdivi"></div>
<p>	   
		  	<div class="list${classIndex % 2}"><c:set var="classIndex" value="${classIndex+1}" />
		  	<div class="listfl">&nbsp;</div>
		  	<div class="listp">
		  	<!-- input field -->
		  	
		  	
				<table border="0" class="giftcard" cellspacing="0" cellpadding="0">
				<tr> <td> &nbsp; </td> </tr><tr> <td> &nbsp; </td> </tr><tr> <td> &nbsp; </td> </tr>
				 <tr> 
				   <td>
				    <p class="fromTo">From: <c:out value="${giftCardForm.giftCard.senderFirstName}" />&nbsp;<c:out value="${giftCardForm.giftCard.senderLastName}" /></p>
				    <p class="fromTo">To: <c:out value="${giftCardForm.giftCard.recipientFirstName}" />&nbsp;<c:out value="${giftCardForm.giftCard.recipientLastName}" /></p>
				   </td>
				 </tr>
				 <tr>
				   <td>
				   <div class="amount"><fmt:message key="${siteConfig['CURRENCY'].value}" /><fmt:formatNumber value="${giftCardForm.giftCard.amount}" pattern="#,##0.00" /></div>
				   </td> 
				 </tr>
				 <tr>
				   <td>
				   <div class="code"><c:out value="${giftCardForm.giftCard.code}" /></div>
				   </td>  
				 </tr>
				 <tr> <td> &nbsp; </td> </tr>
				</table>
				
	 		<!-- end input field -->	
	 		</div>
		  	</div>	
		  	
		  	<div class="list${classIndex % 2}"><c:set var="classIndex" value="${classIndex+1}" />
		  	<div class="listfl"><fmt:message key='active' />:</div>
		  	<div class="listp">
		  	<!-- input field -->
				<form:checkbox path="giftCard.active" value="true"/>
	 		<!-- end input field -->	
	 		</div>
		  	</div>
		  	
		  	<div class="list${classIndex % 2}"><c:set var="classIndex" value="${classIndex+1}" />
		  	<div class="listfl"><fmt:message key='recipientEmail' />:</div>
		  	<div class="listp">
		  	<!-- input field -->
				<c:out value="${giftCardForm.giftCard.recipientEmail}" />
	 		<!-- end input field -->	
	 		</div>
		  	</div>	
		  	
		  	<div class="list${classIndex % 2}"><c:set var="classIndex" value="${classIndex+1}" />
		  	<div class="listfl"><fmt:message key='message' />:</div>
		  	<div class="listp">
		  	<!-- input field -->
				<c:out value="${giftCardForm.giftCard.message}" />
	 		<!-- end input field -->	
	 		</div>
		  	</div>	
		  	
		  	<div class="list${classIndex % 2}"><c:set var="classIndex" value="${classIndex+1}" />
		  	<c:if test="${giftCardForm.giftCard.paymentMethod !='PayPal'}">
		  	<div class="listfl"><fmt:message key='payment' />:</div>
		  	<div class="listp">
		  	
		  	<!-- input field -->
		  	<table>
				  <tr>
				    <td align="right" style="white-space: nowrap"><fmt:message key='cardType' />:</td>
				    <td><c:out value="${giftCardForm.giftCard.creditCard.type}"/></td>
				  </tr>
				  <tr>
				    <td align="right" style="white-space: nowrap"><fmt:message key='cardNumber' />:</td>
				    <td><c:out value="${giftCardForm.giftCard.creditCard.number}"/></td>  </tr>
				  <tr>
				    <td align="right" style="white-space: nowrap"><fmt:message key='expirationDate' />:</td>
				    <td><c:out value="${giftCardForm.giftCard.creditCard.expireMonth}"/>/<c:out value="${giftCardForm.giftCard.creditCard.expireYear}"/></td>
				  </tr>
				  <tr>
				    <td align="right" style="white-space: nowrap"><fmt:message key='verificationCode' />:</td>
				    <td><c:out value="${giftCardForm.giftCard.creditCard.cardCode}"/></td>
				  </tr>
				  <c:if test="${order.creditCard.transId != null}">
				  <tr>
				    <td align="right" style="white-space: nowrap" valign="top"><fmt:message key='transactionID' />:</td>
				    <td><c:out value="${giftCardForm.giftCard.creditCard.transId}" /></td>
				  </tr>
				  </c:if>
				  <c:if test="${giftCardForm.giftCard.creditCard.paymentStatus != null}">
				  <tr>
				    <td align="right" style="white-space: nowrap" valign="top"><fmt:message key='paymentStatus' />:</td>
				    <td><c:out value="${giftCardForm.giftCard.creditCard.paymentStatus}" /></td>
				  </tr>
				  </c:if>
				  <c:if test="${giftCardForm.giftCard.creditCard.paymentNote != null}">
				  <tr>
				    <td align="right" style="white-space: nowrap" valign="top"><fmt:message key='paymentNote' />:</td>
				    <td><c:out value="${giftCardForm.giftCard.creditCard.paymentNote}" /></td>
				  </tr>
				  </c:if>
				  <c:if test="${giftCardForm.giftCard.creditCard.paymentDate != null}">
				  <tr>
				    <td align="right" style="white-space: nowrap" valign="top"><fmt:message key='paymentDate' />:</td>
				    <td><fmt:formatDate type="both" timeStyle="full" value="${giftCardForm.giftCard.creditCard.paymentDate}"/></td>
				  </tr>
				  </c:if> 
			</table>		
	 		</div>	 		
			</c:if>	 
	 		</div>
		  	<div class="list${classIndex % 2}"><c:set var="classIndex" value="${classIndex+1}" />
		  	<table border="0" cellpadding="2" cellspacing="1" width="100%" class="statusHistory">
				<tr>
				  <th class="statusHistory">Date Changed</th>
				  <th class="statusHistory"><fmt:message key="status"/></th>
				  <th width="100%" class="statusHistory"><fmt:message key="comments"/></th>
				  <th width="100%" class="statusHistory"><fmt:message key="user"/></th>
				</tr>
				<c:forEach var="giftCardStatus" items="${giftCardStatusList}">
				<tr valign="top">
				  <td class="statusHistory" style="white-space: nowrap"><fmt:formatDate type="both" timeStyle="full" value="${giftCardStatus.dateChanged}"/></td>
				  <td class="status" align="center" style="white-space: nowrap"><fmt:message key="orderStatus_${giftCardStatus.status}"/></td>				  				 
				  <td style="background-color:#FFFFFF"><c:out value="${giftCardStatus.comments}"/></td>
				  <td style="background-color:#FFFFFF"><c:out value="${giftCardStatus.username}"/></td>
				</tr>
				</c:forEach>
			</table>
		  	
		  
			<table align="left" border="0" class="updateStatus">		
			  <tr>
			    <td>
			      <label><fmt:message key="status"/>:</label>			      
				  <input type="hidden" name="giftCardOrderId" value="${giftCardOrderId}"/>	
			    </td>
			    <td align="right"> 
			      <select name="giftCardStatus">
			       <option value="p" <c:if test="${giftCardForm.giftCard.status == 'p'}">selected</c:if>><fmt:message key="orderStatus_p" />
			        <option value="pr" <c:if test="${giftCardForm.giftCard.status == 'pr'}">selected</c:if>><fmt:message key="orderStatus_pr" />
			        <option value="s" <c:if test="${giftCardForm.giftCard.status == 's'}">selected</c:if>><fmt:message key="orderStatus_s" />
			        <option value="x" <c:if test="${giftCardForm.giftCard.status == 'x'}">selected</c:if>><fmt:message key="orderStatus_x" />
			        <c:if test="${giftCardForm.giftCard.status == 'xp'}">
			          <option value="xp" <c:if test="${giftCardForm.giftCard.status == 'xp'}">selected</c:if>><fmt:message key="orderStatus_xp" />
			        </c:if>
			      </select>
				</td>
			  </tr>
			  <tr>
			    <td colspan="2"><fmt:message key="comments"/>:</td>
			  </tr>
			  <tr>
				<td colspan="2"><textarea id="comments" cols="80" rows="5" name="giftCardComments"></textarea></td>
			  </tr>			  
			  <tr>
				<td colspan="2">
				<sec:authorize ifAnyGranted="ROLE_AEM,ROLE_ADMIN_${prop_site_id},ROLE_INVOICE_STATUS">
				<div class="button">
				  <input type="submit" name="__status" value="Update Status" onclick="return checkStatus();">
				  <input type="reset" value="Reset" onclick="document.getElementById('userNotified1').checked=false;toggleMessage()">
				</div>  
				</sec:authorize>  
				</td>
			  </tr>
			</table> 
		  		
</p>		
<!-- end input field -->	
		  	</div>
		  	  
	<!-- end tab -->		
	</div>
<!-- end tabs -->			
</div>
</form:form>
<!-- start button -->
	<div align="left" class="button">
	<%-- <input type="submit" value="<fmt:message key='Update' />" name="_update"/> --%>
	</div>
<!-- end button -->	

  <!-- end table -->
    </td><td class="boxmidr" ></td></tr>
  <tr><td class="botl"></td><td class="botr"></td></tr>
  </table>
  
<!-- end main box -->  
</div>


  </tiles:putAttribute>    
</tiles:insertDefinition>