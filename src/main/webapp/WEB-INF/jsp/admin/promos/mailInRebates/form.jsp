<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="/WEB-INF/tlds/spring.tld" prefix="spring" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib uri="http://www.springframework.org/security/tags" prefix="sec" %>


<tiles:insertDefinition name="admin.promos" flush="true">
  <tiles:putAttribute name="title"  value="Mail In Rebate Manager" />
  <tiles:putAttribute name="content" type="string">

<sec:authorize ifAnyGranted="ROLE_AEM,ROLE_ADMIN_${prop_site_id},ROLE_MAILINREBATE_CREATE">  
<link rel="stylesheet" type="text/css" media="all" href="./../../admin/javascript/jscalendar-1.0/skins/aqua/theme.css" title="Aqua" /> <!-- calendar stylesheet -->
<script type="text/javascript" src="./../../admin/javascript/jscalendar-1.0/calendar.js"></script> <!-- main calendar program -->
<script type="text/javascript" src="./../../admin/javascript/jscalendar-1.0/lang/calendar-en.js"></script>
<script type="text/javascript" src="./../../admin/javascript/jscalendar-1.0/calendar-setup.js"></script>

<script type="text/javascript" >
<!--
window.addEvent('domready', function(){			
	var tabs1 = new SimpleTabs($('tab-block-1'), { selector: 'h4'});
	var Tips1 = new Tips($$('.toolTipImg'));
});

-->	
</script>

<form:form id="mailInRebateForm" commandName="mailInRebateForm" method="post" enctype="multipart/form-data" >
<!-- main box -->
  <div id="mboxfull">
  
  <!-- start table -->
  <table cellpadding="0" cellspacing="0" border="0" class="module" >
  <tr>
    <td class="topl_g">
	  
	  <!-- breadcrumb -->
	  <p class="breadcrumb">
	    <a href="../promos"><fmt:message key="promosTabTitle"/></a> &gt;
	    <a href="../promos"><fmt:message key="mailInRebates"/></a> &gt;
	    ${mailInRebateForm.mailInRebate.title}  
	  </p>
	  
	  <!-- Error Message -->
	  <c:if test="${model.message != null}">
  		<div class="message"><spring:message code="${model.message}"/></div>
	  </c:if>
	  	  <spring:hasBindErrors name="promoForm">
		<div class="message">Please fix all errors!</div>
	  </spring:hasBindErrors>
	  
  </td><td class="topr_g" ></td></tr>
  <tr><td class="boxmidlrg" >

 <!-- tabs -->
 <div id="tab-block-1">
	<h4 title="Mail In Rebate Form"><fmt:message key="mailInRebates"/></h4>
	<div>
	<!-- start tab -->
	<p>
         <c:set var="classIndex" value="0" />
         
	  	<div class="listdivi ln tabdivi"></div>
	  	<div class="listdivi"></div>
	   
		  	<div class="list${classIndex % 2}"><c:set var="classIndex" value="${classIndex+1}" />
		  	<div class="listfl"><div class="requiredField"><fmt:message key='title' />:</div></div>
		  	<div class="listp">
		  	<!-- input field -->
				<form:input path="mailInRebate.title" cssClass="textfield"/>
				<form:errors path="mailInRebate.title" cssClass="error"/>
	 		<!-- end input field -->	
	 		</div>
		  	</div>	
		  	
		  	<div class="list${classIndex % 2}"><c:set var="classIndex" value="${classIndex+1}" />
		  	<div class="listfl"><fmt:message key='discount' />:</div>
		  	<div class="listp">
		  	<!-- input field -->
				<form:input path="mailInRebate.discount" cssClass="textfield" size="5" maxlength="7"/>
				<form:select path="mailInRebate.percent">
					<form:option value="false"><fmt:message key="FIXED" /> $</form:option>
					<form:option value="true" label="%" />
				</form:select>
				<form:errors path="mailInRebate.discount" cssClass="error"/>
				<span class="helpNote"><p><fmt:message key='maxis100' /></p></span>
	 		<!-- end input field -->	
	 		</div>
		  	</div>	
		  			
		  	<div class="list${classIndex % 2}"><c:set var="classIndex" value="${classIndex+1}" />
		  	<div class="listfl"><fmt:message key='startDate' />:</div>
		  	<div class="listp">
		  	<!-- input field -->
		  		<span class="helpNote"><p><fmt:message key='currentTime' />: <c:out value="${model.currentTime}"></c:out></p></span>
				<form:input path="mailInRebate.startDate" cssClass="textfield" size="19" maxlength="19" />
				  <img src="../graphics/calendarIcon.jpg" class="calendarImage" id="time_start_trigger"/>
				  <script type="text/javascript">
		    		Calendar.setup({
		        			inputField     :    "mailInRebate.startDate",   // id of the input field
		        			showsTime      :    true,
		        			ifFormat       :    "%m-%d-%Y %I:%M %p",   // format of the input field
		        			button         :    "time_start_trigger",   // trigger for the calendar (button ID)
		        			timeFormat     :    "12",
        			});	
				  </script> 
				<form:errors path="mailInRebate.startDate" cssClass="error"/>	 
	 		<!-- end input field -->	
	 		</div>
		  	</div>
		  				
		  	<div class="list${classIndex % 2}"><c:set var="classIndex" value="${classIndex+1}" />
		  	<div class="listfl"><fmt:message key='endDate' />:</div>
		  	<div class="listp">
		  	<!-- input field -->
				<form:input path="mailInRebate.endDate" cssClass="textfield" size="19" maxlength="19" />
				  <img src="../graphics/calendarIcon.jpg" class="calendarImage" id="time_end_trigger"/>
				  <script type="text/javascript">
		    		Calendar.setup({
		        			inputField     :    "mailInRebate.endDate",   // id of the input field
		        			showsTime      :    true,
		        			ifFormat       :    "%m-%d-%Y %I:%M %p",   // format of the input field
		        			button         :    "time_end_trigger",   // trigger for the calendar (button ID)
		        			timeFormat     :    "12",
       				});	
				  </script> 
				<form:errors path="mailInRebate.endDate" cssClass="error"/>	 	
	 		<!-- end input field -->	
	 		</div>
		  	</div>
		  	
		  	<div class="list${classIndex % 2}"><c:set var="classIndex" value="${classIndex+1}" />
		  	<div class="listfl"><div class="helpImg"><img class="toolTipImg" title="Note:: HTML Code. Include message to appear with SKU " src="../graphics/question.gif" /></div><fmt:message key='htmlCode' />:</div>
		  	<div class="listp">
		  	<!-- input field --> 
		  		<form:textarea path="mailInRebate.htmlCode" cssClass="textfield" rows="5"/>
		  	<!-- end input field -->	
	 		</div>
	 		</div>	
	</p>	  			         
	<!-- end tab -->		
	</div>
<!-- end tabs -->			
</div>

<!-- start button -->
	<div align="left" class="button">
		<c:choose>
		<c:when test="${mailInRebateForm.newMailInRebate}">
		  <sec:authorize ifAnyGranted="ROLE_AEM,ROLE_ADMIN_${prop_site_id},ROLE_MAILINREBATE_CREATE">
			<input type="submit" value="<fmt:message key='add' />" /> 
		  </sec:authorize>	
		</c:when>
		<c:otherwise>
		  <sec:authorize ifAnyGranted="ROLE_AEM,ROLE_ADMIN_${prop_site_id},ROLE_MAILINREBATE_CREATE">
			<input type="submit" value="<fmt:message key='Update' />" /> 
		  </sec:authorize>
		  <sec:authorize ifAnyGranted="ROLE_AEM,ROLE_ADMIN_${prop_site_id},ROLE_MAILINREBATE_DELETE">	
			<input type="submit" name ="delete" value="<fmt:message key='delete' />" onclick="return confirm('Are you sure you wish to delete this Mail In Rebate?')"/>
		  </sec:authorize>	
		</c:otherwise>
	</c:choose>
	<input type="submit" name="_cancel" value="<spring:message code="cancel"/>" >
	</div>
<!-- end button -->	

  <!-- end table -->
    </td><td class="boxmidr" ></td></tr>
  <tr><td class="botl"></td><td class="botr"></td></tr>
  </table>
  
<!-- end main box -->  
</div>
</form:form>
 
</sec:authorize> 
  </tiles:putAttribute>    
</tiles:insertDefinition>