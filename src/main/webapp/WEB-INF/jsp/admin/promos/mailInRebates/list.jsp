<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/security/tags" prefix="sec" %>

<tiles:insertDefinition name="admin.promos" flush="true">
    <tiles:putAttribute name="title" value="Promotions Manager" />
	<tiles:putAttribute name="content" type="string">
	
<form action="mailInRebateList.jhtm" method="post">
<!-- main box -->
  <div id="mboxfull">
  
  <!-- start table -->
  <table cellpadding="0" cellspacing="0" border="0" class="module" >
  <tr>
    <td class="topl_g">
	  
	  <!-- breadcrumb -->
	  <p class="breadcrumb">
	    <a href="../promos"><fmt:message key="promosTabTitle"/></a> &gt;
	    Mail In Rebate 
	  </p>
	  
	  <!-- header image -->
	  <img class="headerImage" src="../graphics/salesTag.gif" />
	  
	  </td><td class="topr_g" ></td></tr>
  <tr><td class="boxmidlrg" >

 <!-- tabs -->
 <div class="tab-wrapper">
	<h4 title=""></h4>
	<div>
	<!-- start tab -->

	  	<div class="listdivi"></div>
	   
		  	<div class="listlight">
		  	<!-- input field --> 
		  	<table border="0" cellpadding="0" cellspacing="1" width="100%">
				<tr>
					<td>&nbsp;
					</td>
					<c:if test="${model.mailInRebates.nrOfElements > 0}">
						<td class="pageShowing">
							<fmt:message key="showing">
								<fmt:param value="${model.mailInRebates.firstElementOnPage + 1}" />
								<fmt:param value="${model.mailInRebates.lastElementOnPage + 1}" />
								<fmt:param value="${model.mailInRebates.nrOfElements}" />
							</fmt:message>
						</td>
					</c:if>
					<td class="pageNavi">
						Page
						<select name="page" id="page" onchange="submit()">
							<c:forEach begin="1" end="${model.mailInRebates.pageCount}"
								var="page">
								<option value="${page}"
									<c:if test="${page == (model.mailInRebates.page+1)}">selected</c:if>>
									${page}
								</option>
							</c:forEach>
						</select> 
						of
						<c:out value="${model.mailInRebates.pageCount}" /> 
						|
						<c:if test="${model.mailInRebates.firstPage}">
							<span class="pageNaviDead"><fmt:message key="previous" /></span>
						</c:if>
						<c:if test="${not model.mailInRebates.firstPage}">
							<a
								href="<c:url value="mailInRebateList.jhtm"><c:param name="page" value="${model.mailInRebates.page}"/><c:param name="parent" value="${model.search['parent']}"/><c:param name="size" value="${model.mailInRebates.pageSize}"/></c:url>"
								class="pageNaviLink"><fmt:message key="previous" /></a>
						</c:if>
						|
						<c:if test="${model.mailInRebates.lastPage}">
							<span class="pageNaviDead"><fmt:message key="next" /></span>
						</c:if>
						<c:if test="${not model.mailInRebates.lastPage}">
							<a
								href="<c:url value="mailInRebateList.jhtm"><c:param name="page" value="${model.mailInRebates.page+2}"/><c:param name="parent" value="${model.search['parent']}"/><c:param name="size" value="${model.mailInRebates.pageSize}"/></c:url>"
								class="pageNaviLink"><fmt:message key="next" /></a>
						</c:if>
					</td>
				</tr>
			</table>

			<table border="0" cellpadding="0" cellspacing="0" width="100%"
				class="listings">
				<tr class="listingsHdr2">

					<td class="indexCol">&nbsp;</td>
					<td class="listingsHdr3">
						<fmt:message key="title" />
					</td>
					<td class="listingsHdr3">
						<fmt:message key="discount" />
					</td>
					<td class="listingsHdr3">
						<fmt:message key="mailInRebatesDate" />
					</td>
					<td class="listingsHdr3">
						<fmt:message key="inEffect" />
					</td>
				</tr>
				<c:forEach items="${model.mailInRebates.pageList}" var="rebate"	varStatus="status">
					<tr class="row${status.index % 2}"	onmouseover="changeStyleClass(this,'rowOver')" 	onmouseout="changeStyleClass(this,'row${status.index % 2}')">
						<td class="indexCol"><c:out value="${status.count + model.mailInRebates.firstElementOnPage}" />.</td>
						<td class="nameCol">
							<a href="mailInRebate.jhtm?id=${rebate.rebateId}" class="nameLink"><c:out
									value="${rebate.title}" />
							</a>
						</td>
						<td class="nameCol">
							   <c:out value="${rebate.discount}"></c:out> <c:choose><c:when test="${rebate.percent}">%</c:when><c:otherwise>USD</c:otherwise> </c:choose> 
						</td>
						<td class="nameCol">
							<fmt:formatDate type="date" dateStyle="full" value="${rebate.startDate}" pattern="MM/dd/yyyy (hh:mm)a zz "/> - <fmt:formatDate type="date" dateStyle="full" value="${rebate.endDate}" pattern="MM/dd/yyyy (hh:mm)a zz"/> 
						</td>
						<td class="nameCol">
							<c:if test="${rebate.inEffect}"><div align="left"><img src="../graphics/checkbox.png" /></div></c:if>
						</td>
					</tr>
				</c:forEach>
				<c:if test="${model.mailInRebates.nrOfElements == 0}">
					<tr class="emptyList">
						<td colspan="8">
							&nbsp;
						</td>
					</tr>
				</c:if>
			</table>

			<c:if test="${model.nOfmailInRebates > 0}">
				<div class="sup">
					&sup1; <fmt:message key="toEditClickMailInRebatesTitle" /> 
				</div>
			</c:if>
			<table border="0" cellpadding="0" cellspacing="1" width="100%">
				<tr>
				  <td class="pageSize">
					<select name="size"
						onchange="document.getElementById('page').value=1;submit()">
						<c:forTokens items="10,25,50" delims="," var="current">
							<option value="${current}"
								<c:if test="${current == mailInRebatesSearch.pageSize}">selected</c:if>>
								${current}
								<fmt:message key="perPage" />
							</option>
						</c:forTokens>
					</select>
				  </td>
				</tr>
			</table>
		  	<!-- end input field -->  	  
		  	</div>
		  	
		  	<!-- start button -->
		  	<div align="left" class="button">
 			  <c:choose>
				<c:when test="${gSiteConfig['gMAIL_IN_REBATES'] > model.nOfmailInRebates}">
				  <sec:authorize ifAnyGranted="ROLE_AEM,ROLE_ADMIN_${prop_site_id},ROLE_MAILINREBATE_CREATE">
					<input type="submit" name="__add" value="<fmt:message key="addMailInRebates" />"/> &nbsp;
				  </sec:authorize>	
				</c:when>
				<c:otherwise>
				  <div class="note_list">
					<fmt:message key="toAddMoreMailInRebatesContactWebMaster" />
				  </div>	
				</c:otherwise>
			</c:choose>	
			</div>
		  	<!-- end button -->	
	
	  	
	<!-- end tab -->        
	</div>
<!-- end tabs -->			
</div>
  
  <!-- end table -->
   </td><td class="boxmidr" ></td></tr>
  <tr><td class="botl"></td><td class="botr"></td></tr>
  </table>
  
<!-- end main box -->  
</div>  
</form> 	

	</tiles:putAttribute>
</tiles:insertDefinition>
