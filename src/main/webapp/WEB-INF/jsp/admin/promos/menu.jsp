<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/security/tags" prefix="sec" %>

  <div id="lbox">
   <table cellpadding="0" cellspacing="0" border="0" class="module" style="width: 100%"><tr><td class="topl">
    </td><td class="topr" ></td></tr><tr>
    
    <td class="boxmid" >
  
    <h2 class="menuleft mfirst"><fmt:message key="promosTabTitle"/></h2>
    <div class="menudiv"></div>
    <c:if test="${gSiteConfig['gSALES_PROMOTIONS']}">
    <sec:authorize ifAnyGranted="ROLE_AEM,ROLE_ADMIN_${prop_site_id},ROLE_PROMOTION_CREATE,ROLE_PROMOTION_EDIT,ROLE_PROMOTION_DELETE">
    <a href="promoList.jhtm" class="userbutton"><fmt:message key="promotionsTitle"/></a> 
    <a href="promoImport.jhtm" class="userbutton"><fmt:message key="import"/></a>    
    </sec:authorize>
    <sec:authorize ifAnyGranted="ROLE_AEM,ROLE_ADMIN_${prop_site_id},ROLE_SALES_TAG_CREATE,ROLE_SALES_TAG_EDIT,ROLE_SALES_TAG_DELETE">
    <c:if test="${empty siteConfig['PRODUCT_SYNC'].value}" >
    <a href="salesTagList.jhtm" class="userbutton"><fmt:message key="salesTagsTitle"/></a>
    </c:if>
    </sec:authorize>
    </c:if>
    <sec:authorize ifAnyGranted="ROLE_AEM,ROLE_ADMIN_${prop_site_id}">
    <c:if test="${gSiteConfig['gGIFTCARD']}">
      <a href="giftCardList.jhtm" class="userbutton"><fmt:message key="giftCard"/></a>
    </c:if>
    </sec:authorize>
    <sec:authorize ifAnyGranted="ROLE_AEM,ROLE_ADMIN_${prop_site_id}">
    <c:if test="${gSiteConfig['gDEALS'] > 0}">
      <a href="dealList.jhtm" class="userbutton"><fmt:message key="deals"/></a>
      <a href="viaDealList.jhtm" class="userbutton">Via Deals</a>
    </c:if>
    </sec:authorize>
    <sec:authorize ifAnyGranted="ROLE_AEM,ROLE_ADMIN_${prop_site_id},ROLE_MAILINREBATE_CREATE,ROLE_MAILINREBATE_DELETE">
    <c:if test="${gSiteConfig['gMAIL_IN_REBATES'] > 0}">
       <a href="mailInRebateList.jhtm" class="userbutton"><fmt:message key="mailInRebates"/></a>
    </c:if>
    </sec:authorize>
    <div class="menudiv"></div>

    <c:if test="${gSiteConfig['gGIFTCARD'] and param.tab == 'giftCard'}">	 
    <div class="menudiv"></div>
      <div align="left" class="searchHeader" id="searchHeaderId"><h2><fmt:message key="search" /></h2></div>
	  <table class="searchBoxLeft" width="100%"><tr><td>
	  <form action="giftCardList.jhtm" method="post">
      <!-- content area -->
      <div class="search2">
        <p><fmt:message key="view" />:</p>
        <select name="_viewtype">
	        <option value="order" <c:if test="${giftCardSearch.viewType == 'order'}">selected</c:if>><fmt:message key="order"/></option>
	        <option value="giftcard" <c:if test="${giftCardSearch.viewType == 'giftcard'}">selected</c:if>><fmt:message key="giftCard"/></option>  
	      </select>
      </div> 
      <div class="search2">
        <p><fmt:message key="claimCode" />:</p>
        <input name="claim_code" type="text" value="<c:out value='${giftCardSearch.claimCode}' />" size="15" />
      </div>     
      <div class="button">
	      <input type="submit" value="Search"/>
      </div>
      <!-- content area -->
	  </form>
	  </td></tr></table>
	<div class="menudiv"></div>
	</c:if>
    <c:if test="${gSiteConfig['gDEALS'] > 0 and param.tab == 'dealsTab'}">	 
    <div class="menudiv"></div>
      <div align="left" class="searchHeader" id="searchHeaderId"><h2><fmt:message key="search" /></h2></div>
	  <table class="searchBoxLeft" width="100%"><tr><td>
	  <form action="dealList.jhtm" method="post">
      <!-- content area -->
      <div class="search2">
        <p><fmt:message key="view" />:</p>
        <select name="_viewtype">
	        <option value="" <c:if test="${dealSearch.viewType == ''}">selected</c:if>><fmt:message key="all"/></option>
	        <option value="sku" <c:if test="${dealSearch.viewType == 'sku'}">selected</c:if>><fmt:message key="sku"/></option>
	        <option value="subTotal" <c:if test="${dealSearch.viewType == 'subTotal'}">selected</c:if>><fmt:message key="subTotal"/></option>  
	    </select>
      </div> 
      <div class="button">
	      <input type="submit" value="Search"/>
      </div>
      <!-- content area -->
	  </form>
	  </td></tr></table>
	<div class="menudiv"></div>
	</c:if>
	
	<c:if test="${param.tab == 'dynamicpromos'}">	 
	  <div class="menudiv"></div>
      <div align="left" class="searchHeader" id="searchHeaderId"><h2><fmt:message key="search" /></h2></div>
	  <table class="searchBoxLeft" width="100%"><tr><td>
	  <form action="promoDynamicList.jhtm" method="post">
      <!-- content area -->
      <div class="search2">
        <p><fmt:message key="promoType" />:</p>
        <select name="_promotype">
        	<option value="" <c:if test="${promoSearch.promoType == ''}">selected</c:if> ><fmt:message key="all"/></option>
	        <option value="order" <c:if test="${promoSearch.promoType == 'order'}">selected</c:if> ><fmt:message key="order"/></option>
	        <option value="product" <c:if test="${promoSearch.promoType == 'product'}">selected</c:if>><fmt:message key="product"/></option> 
	        <option value="shipping" <c:if test="${promoSearch.promoType == 'shipping'}">selected</c:if> ><fmt:message key="shipping"/></option>   
	      </select>
      </div> 
      <div class="search2">
        <p><fmt:message key="prefix" />:</p>
        <input name="_prefix" type="text" value="<c:out value='${promoSearch.prefix}' />" size="15" />
      </div>
       <div class="search2">
        <p><fmt:message key="userId" />:</p>
        <input name="_userId" type="text" <c:choose> <c:when test ="${ promoSearch.userId == '0' ||  promoSearch.userId == '-1' }"> value = "" </c:when> <c:otherwise> value="<c:out value='${promoSearch.userId}' />"</c:otherwise> </c:choose> size="15" />
      </div>     
       <div class="search2">
        <p><fmt:message key="productSku" />:</p>
        <input name="_sku" type="text" value="<c:out value='${promoSearch.sku}' />" size="15" />
      </div>  
       <div class="search2">
        <p><fmt:message key="startDate" />:</p>
       
        <input id = "_startDate" name="_startDate" value=" <fmt:formatDate 
         value = "${promoSearch.startDate}" pattern="MM/dd/yyyy[hh:mm aaa]" />" size="15" onkeypress="return false;" />
         <img id="time_start_trigger1" class="calendarImage" src="../graphics/calendarIcon.jpg" alt="Pick a date" />
      </div> 
      <div class="search2">
        <p><fmt:message key="endDate" />:</p>
       
        <input  id = "_endDate"  name="_endDate"  value="<fmt:formatDate 
         value = "${promoSearch.endDate}" pattern="MM/dd/yyyy[hh:mm aaa]" /> " size="15" onkeypress="return false;"  />
         <img id="time_start_trigger2" class="calendarImage" src="../graphics/calendarIcon.jpg" alt="Pick a date" />
      </div> 
      <div class="button">
	      <input type="submit" value="Search"/>
      </div>
      <!-- content area -->
	  </form>
	  </td></tr></table>
    <div class="menudiv"></div>
	</c:if>
	
    </td>
    
    <td class="boxmidr" ></td></tr>
    <tr><td class="botl"></td><td class="botr"></td></tr></table>
  </div>
  