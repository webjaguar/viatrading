<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/security/tags" prefix="sec" %>

<tiles:insertDefinition name="admin.promos.salesTags" flush="true">
    <tiles:putAttribute name="title"  value="Sales Tags Manager" />
	<tiles:putAttribute name="content" type="string">
		
<form action="salesTagList.jhtm" method="post">
<!-- main box -->
  <div id="mboxfull">
  
  <!-- start table -->
  <table cellpadding="0" cellspacing="0" border="0" class="module" >
  <tr>
    <td class="topl_g">
	  
	  <!-- breadcrumb -->
	  <p class="breadcrumb">
	    <a href="../promos"><fmt:message key="promosTabTitle"/></a> &gt;
	    <fmt:message key="salesTag"/>
	  </p>
	  
	  <!-- header image -->
	  <img class="headerImage" src="../graphics/salesTag.gif" />
	  
	  </td><td class="topr_g" ></td></tr>
  <tr><td class="boxmidlrg" >

 <!-- tabs -->
 <div class="tab-wrapper">
	<h4 title=""></h4>
	<div>
	<!-- start tab -->

	  	<div class="listdivi"></div>
	    
		  	<div class="listlight">
		  	<!-- input field --> 
		  	<table border="0" cellpadding="0" cellspacing="1" width="100%">
				<tr>
					<td>&nbsp;
					</td>
					<c:if test="${model.salesTagSearch.nrOfElements > 0}">
						<td class="pageShowing">
							<fmt:message key="showing">
								<fmt:param value="${model.salesTagSearch.firstElementOnPage + 1}" />
								<fmt:param value="${model.salesTagSearch.lastElementOnPage + 1}" />
								<fmt:param value="${model.salesTagSearch.nrOfElements}" />
							</fmt:message>
						</td>
					</c:if>
					<td class="pageNavi">
						Page
						<select name="page" id="page" onchange="submit()">
							<c:forEach begin="1" end="${model.salesTagSearch.pageCount}"
								var="page">
								<option value="${page}"
									<c:if test="${page == (model.salesTagSearch.page+1)}">selected</c:if>>
									${page}
								</option>
							</c:forEach>
						</select> 
						of
						<c:out value="${model.salesTagSearch.pageCount}" /> 
						|
						<c:if test="${model.salesTagSearch.firstPage}">
							<span class="pageNaviDead"><fmt:message key="previous" /></span>
						</c:if>
						<c:if test="${not model.salesTagSearch.firstPage}">
							<a
								href="<c:url value="salesTagList.jhtm"><c:param name="page" value="${model.salesTagSearch.page}"/><c:param name="size" value="${model.salesTagSearch.pageSize}"/></c:url>"
								class="pageNaviLink"><fmt:message key="previous" /></a>
						</c:if>
						|
						<c:if test="${model.salesTagSearch.lastPage}">
							<span class="pageNaviDead"><fmt:message key="next" /></span>
						</c:if>
						<c:if test="${not model.salesTagSearch.lastPage}">
							<a
								href="<c:url value="salesTagList.jhtm"><c:param name="page" value="${model.salesTagSearch.page+2}"/><c:param name="size" value="${model.salesTagSearch.pageSize}"/></c:url>"
								class="pageNaviLink"><fmt:message key="next" /></a>
						</c:if>
					</td>
				</tr>
			</table>

			<table border="0" cellpadding="0" cellspacing="0" width="100%"
				class="listings">
				<tr class="listingsHdr2">

					<td class="indexCol">&nbsp;</td>
					<td class="listingsHdr3">
						&nbsp;
					</td>
					<td class="listingsHdr3">
						<fmt:message key="salesTag" />
					</td>
					<td class="listingsHdr3">
						<fmt:message key="discount" />
					</td>
					<td class="listingsHdr3">
						<fmt:message key="salesTagDate" />
					</td>
					<td class="listingsHdr3">
						<fmt:message key="inEffect" />
					</td>
				</tr>
				<c:forEach items="${model.salesTags.pageList}" var="salesTag"	varStatus="status">
					<tr class="row${status.index % 2}"	onmouseover="changeStyleClass(this,'rowOver')" 	onmouseout="changeStyleClass(this,'row${status.index % 2}')">
						<td class="indexCol">
							<c:set var="salesTagNum" scope="page" value="${model.salesTags.nrOfElements}" />
							<c:out value="${status.count + model.salesTags.firstElementOnPage}" />.
						</td>
						<td class="nameCol">
							<c:choose>
							  <c:when test="${salesTag.image}">
							  <a href="<c:url value='../../assets/Image/PromoSalesTag/salestag_${salesTag.tagId}.gif'/>?rnd=${model.randomNum}"  onclick="window.open(this.href,'','width=300,height=150'); return false;">
							    <img border="0" src="<c:url value='../graphics/camera.gif'/>" alt="" title='Sales Tag has Image'/> </a>
							  </c:when>
							  <c:otherwise>
							    &nbsp;
							  </c:otherwise>
							</c:choose>
						</td>
						<td class="nameCol">
							<a href="salesTag.jhtm?id=${salesTag.tagId}" class="nameLink"><c:out
									value="${salesTag.title}" />
							</a>
						</td>
						<td class="nameCol">
							<c:choose>
								<c:when test="${salesTag.percent == true}"><c:out value="${salesTag.discount}" />%</c:when>
								<c:when test="${salesTag.percent == false}">$<c:out value="${salesTag.discount}" /></c:when>
							</c:choose>
						</td>
						<td class="nameCol">
							<fmt:formatDate type="date" dateStyle="full" value="${salesTag.startDate}" pattern="MM/dd/yyyy (hh:mm)a zz"/> - <fmt:formatDate type="date" dateStyle="full" value="${salesTag.endDate}" pattern="MM/dd/yyyy (hh:mm)a zz"/> 
						</td>
						<td class="nameCol">
							<c:if test="${salesTag.inEffect}"><div align="left"><img src="../graphics/checkbox.png" /></div></c:if>
						</td>
					</tr>
				</c:forEach>
				<c:if test="${model.salesTags.nrOfElements == 0}">
					<tr class="emptyList">
						<td colspan="8">
							&nbsp;
						</td>
					</tr>
				</c:if>
			</table>

			<c:if test="${model.nOfsalesTags > 0}">
				<div class="sup">
					&sup1; <fmt:message key="toEditClickSalesTagCode" /> 
				</div>
			</c:if>
			
			<table border="0" cellpadding="0" cellspacing="1" width="100%">
				<tr>
				  <td class="pageSize">
					<select name="size"
						onchange="document.getElementById('page').value=1;submit()">
						<c:forTokens items="10,25,50" delims="," var="current">
							<option value="${current}"
								<c:if test="${current == salesTagSearch.pageSize}">selected</c:if>>
								${current}
								<fmt:message key="perPage" />
							</option>
						</c:forTokens>
					</select>
				  </td>
				</tr>
			</table>
		  	<!-- end input field -->  	  
		  	</div>
		  	
		  	<!-- start button --> 
		  	<div align="left" class="button">
 			<c:choose>
				<c:when test="${gSiteConfig['gNUMBER_SALES_TAG'] > model.nOfsalesTags}">
				  <sec:authorize ifAnyGranted="ROLE_AEM,ROLE_ADMIN_${prop_site_id},ROLE_SALES_TAG_CREATE">
					<input type="submit" name="__add" value="<fmt:message key="salesTagAdd" />"/>
				  </sec:authorize>	
				</c:when>
				<c:otherwise >
					<div class="note_list">
					  <fmt:message key="toAddMoreSalesTagContactWebMaster" />
					</div>  
				</c:otherwise>
			</c:choose>	
		  	</div>
		  	<!-- end button -->	
	  	
	<!-- end tab -->        
	</div>
<!-- end tabs -->			
</div>
  
  <!-- end table -->
   </td><td class="boxmidr" ></td></tr>
  <tr><td class="botl"></td><td class="botr"></td></tr>
  </table>
  
<!-- end main box -->  
</div>  
</form>	

	</tiles:putAttribute>
</tiles:insertDefinition>