<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="/WEB-INF/tlds/spring.tld" prefix="spring" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib uri="http://www.springframework.org/security/tags" prefix="sec" %>


<tiles:insertDefinition name="admin.promos.salesTags" flush="true">
  <tiles:putAttribute name="title"  value="Sales Tags Manager" />
  <tiles:putAttribute name="content" type="string"> 
<sec:authorize ifAnyGranted="ROLE_AEM,ROLE_ADMIN_${prop_site_id},ROLE_SALES_TAG_EDIT">  
<link rel="stylesheet" type="text/css" media="all" href="./../../admin/javascript/jscalendar-1.0/skins/aqua/theme.css" title="Aqua" /> <!-- calendar stylesheet -->
<script type="text/javascript" src="./../../admin/javascript/jscalendar-1.0/calendar.js"></script> <!-- main calendar program -->
<script type="text/javascript" src="./../../admin/javascript/jscalendar-1.0/lang/calendar-en.js"></script>
<script type="text/javascript" src="./../../admin/javascript/jscalendar-1.0/calendar-setup.js"></script>

<script type="text/javascript" >
<!--
window.addEvent('domready', function(){			
	var tabs1 = new SimpleTabs($('tab-block-1'), { selector: 'h4'});
});
function uploadImage( aform )
{
	aform.upload_salesTag.value = true;
}
function getImage( imageNumber )
{
	document.getElementById("getImage_name").value = imageNumber;
	document.getElementById("preImageGroupId"+imageNumber).checked = true;
	document.getElementById("upload_salesTag").value = true;
}
function disallowDate(date) {
	// date is a JS Date object
	var currentDate = new Date();
	if ( date.getFullYear() < currentDate.getFullYear() ) 
		return true; // disable July 5 2003
	else if ( date.getFullYear() == currentDate.getFullYear() ) 
	{
		if ( date.getMonth() < currentDate.getMonth() )
			return true;
		else if ( date.getMonth() == currentDate.getMonth() )
		{
			if ( date.getDate() < currentDate.getDate() )
				return true;
			else
				return false;
		}
		else
			return false;
	}				
	else
		return false; // enable other dates
}
-->	
</script>
<style>
.borderimage
{
border:1px solid white;
}
</style>
<script language="JavaScript1.2">

/*
Highlight Image Script II- 
� Dynamic Drive (www.dynamicdrive.com)
For full source code, usage terms, and 100's more DHTML scripts, visit http://dynamicdrive.com
*/

function borderit(which,color){
//if IE 4+ or NS 6+
if (document.all||document.getElementById){
which.style.borderColor=color
}
}

</script>  
<form:form id="salesTagForm" commandName="salesTagForm" method="post" enctype="multipart/form-data" >
<!-- main box -->
  <div id="mboxfull">
  
  <!-- start table -->
  <table cellpadding="0" cellspacing="0" border="0" class="module" >
  <tr>
    <td class="topl_g">
	  
	  <!-- breadcrumb -->
	  <p class="breadcrumb">
	    <a href="../promos"><fmt:message key="promosTabTitle"/></a> &gt;
	    <a href="../promos"><fmt:message key="salesTag"/></a> &gt;
	    ${salesTagForm.salesTag.title}  
	  </p>
	  
	  <!-- Error Message -->
	  <c:if test="${model.message != null}">
  		<div class="message"><spring:message code="${model.message}"/></div>
	  </c:if>
	  	  <spring:hasBindErrors name="promoForm">
		<div class="message">Please fix all errors!</div>
	  </spring:hasBindErrors>
	  
  </td><td class="topr_g" ></td></tr>
  <tr><td class="boxmidlrg" >

 <!-- tabs -->
 <div id="tab-block-1">
	<h4 title="Sales tag Form"><fmt:message key="salesTag"/></h4>
	<div>
	<!-- start tab -->
	<p>
         <c:set var="classIndex" value="0" />
         
	  	<div class="listdivi ln tabdivi"></div>
	  	<div class="listdivi"></div>
	   
		  	<div class="list${classIndex % 2}"><c:set var="classIndex" value="${classIndex+1}" />
		  	<div class="listfl"><div class="requiredField"><fmt:message key='code' />:</div></div>
		  	<div class="listp">
		  	<!-- input field -->
				<form:input path="salesTag.title" cssClass="textfield"/>
				<form:errors path="salesTag.title" cssClass="error"/>
	 		<!-- end input field -->	
	 		</div>
		  	</div>	
		  	
		  	<div class="list${classIndex % 2}"><c:set var="classIndex" value="${classIndex+1}" />
		  	<div class="listfl"><div class="requiredField"><fmt:message key='discount' />:</div></div>
		  	<div class="listp">
		  	<!-- input field -->
				<form:input path="salesTag.discount" cssClass="textfield" size="5" maxlength="5"/>
				<form:select path="salesTag.percent" cssClass="second">
					<form:option value="true" label="%" />
					<form:option value="false"><fmt:message key="FIXED" /> $</form:option>
				</form:select>
				<form:errors path="salesTag.discount" cssClass="error"/>
				<span class="helpNote"><p><fmt:message key='maxis100' /></p></span>
	 		<!-- end input field -->	
	 		</div>
		  	</div>	
		  			
		  	<div class="list${classIndex % 2}"><c:set var="classIndex" value="${classIndex+1}" />
		  	<div class="listfl"><fmt:message key='startDate' />:</div>
		  	<div class="listp">
		  	<!-- input field -->
		  		<span class="helpNote"><p><fmt:message key='currentTime' />: <c:out value="${model.currentTime}"></c:out></p></span>
				<form:input path="salesTag.startDate" cssClass="textfield" size="19" maxlength="19" />
				  <img src="../graphics/calendarIcon.jpg" class="calendarImage" id="time_start_trigger"/>
				  <script type="text/javascript">
		    		Calendar.setup({
		        			inputField     :    "salesTag.startDate",   // id of the input field
		        			showsTime      :    true,
		        			ifFormat       :    "%m-%d-%Y %I:%M %p",   // format of the input field
		        			button         :    "time_start_trigger",   // trigger for the calendar (button ID)
		        			timeFormat     :    "12",
        					disableFunc    :    disallowDate
		    		});	
				  </script> 
				<form:errors path="salesTag.startDate" cssClass="error"/>	 
	 		<!-- end input field -->	
	 		</div>
		  	</div>
		  				
		  	<div class="list${classIndex % 2}"><c:set var="classIndex" value="${classIndex+1}" />
		  	<div class="listfl"><fmt:message key='endDate' />:</div>
		  	<div class="listp">
		  	<!-- input field -->
				<form:input path="salesTag.endDate" cssClass="textfield" size="19" maxlength="19" />
				  <img src="../graphics/calendarIcon.jpg" class="calendarImage" id="time_end_trigger"/>
				  <script type="text/javascript">
		    		Calendar.setup({
		        			inputField     :    "salesTag.endDate",   // id of the input field
		        			showsTime      :    true,
		        			ifFormat       :    "%m-%d-%Y %I:%M %p",   // format of the input field
		        			button         :    "time_end_trigger",   // trigger for the calendar (button ID)
		        			timeFormat     :    "12",
       						disableFunc    :    disallowDate
		    		});	
				  </script> 
				<form:errors path="salesTag.endDate" cssClass="error"/>	 	
	 		<!-- end input field -->	
	 		</div>
		  	</div>	
		  	
		  	<div class="list${classIndex % 2}"><c:set var="classIndex" value="${classIndex+1}" />
		  	<div class="listfl">Image:</div>
		  	<div class="listp">
		  	<!-- input field -->
		  	  <c:choose>
		  	  	<c:when test="${!model.imageExist}">
		  	  		<input type="hidden" name="upload_salesTag" id="upload_salesTag"/>	
		  	  		<input type="hidden" name="getImage_name" id="getImage_name"/>	<br /><br /> 	
		  	  		<label>You can choose pictures from our gallery:</label><br /> 
					<c:forEach begin="5" end="50" step="5" var="current" >
						<img class="borderimage" onMouseover="borderit(this,'black')" onMouseout="borderit(this,'white')" border='1' src="<c:url value='../../assets/Image/SalesTagImages/${current}percent.gif'/>"  title='Sales Tag Image'    alt='Please upload an image.' onclick="getImage(<c:out value='${current}' />)"/>
						<input type="radio" name="getImage_salesTag" id="preImageGroupId${current}" value="<c:out value='${current}' />" onclick="getImage(<c:out value='${current}' />)" />
					</c:forEach><br /><br /> 
					<label>Or Upload your image</label> <br />
					<input type="file" name="salesTag_imageName" id="salesTag_imageName"/><br />
					<input type="reset" value="<fmt:message key='reset' />" /> 
					<input type="submit" onclick=uploadImage(this.form) value="<fmt:message key='upload' />" />
					<span class="helpNote"><p>Please be patient. Uploading images may take several minutes.</p></span>
		  	  	</c:when>
		  	  	<c:otherwise>
		  	  		<img border='1' src="<c:url value='/assets/Image/PromoSalesTag/salestag_${salesTagForm.salesTag.tagId}.gif'/>?rnd=${model.randomNum}"   title='Sales Tag Image'    alt='Please upload an image.'/><br />
		  	  		<input type="submit" name="delete_salesTagImage" onclick="return confirm('Are you sure you wish to delete image?')" value="<fmt:message key='deleteImage' />" /><br />
		  	  	</c:otherwise>
		  	  </c:choose>
	 		<!-- end input field -->	
	 		</div>
		  	</div>				
	</p>	  			         
	<!-- end tab -->		
	</div>
<!-- end tabs -->			
</div>

<!-- start button -->
	<div align="left" class="button">
		<c:choose>
		<c:when test="${salesTagForm.newSalesTag}">
		  <sec:authorize ifAnyGranted="ROLE_AEM,ROLE_ADMIN_${prop_site_id},ROLE_SALES_TAG_CREATE">
			<input type="submit" value="<fmt:message key='add' />" /> 
		  </sec:authorize>	
		</c:when>
		<c:otherwise>
		  <sec:authorize ifAnyGranted="ROLE_AEM,ROLE_ADMIN_${prop_site_id},ROLE_SALES_TAG_EDIT">
			<input type="submit" value="<fmt:message key='Update' />" /> 
		  </sec:authorize>
		  <sec:authorize ifAnyGranted="ROLE_AEM,ROLE_ADMIN_${prop_site_id},ROLE_SALES_TAG_DELETE">	
			<input type="submit" name ="delete" value="<fmt:message key='delete' />" onclick="return confirm('Are you sure you wish to delete this sales Tag?')"/>
		  </sec:authorize>	
		</c:otherwise>
	</c:choose>
	<input type="submit" name="_cancel" value="<spring:message code="cancel"/>" >
	</div>
<!-- end button -->	

  <!-- end table -->
    </td><td class="boxmidr" ></td></tr>
  <tr><td class="botl"></td><td class="botr"></td></tr>
  </table>
  
<!-- end main box -->  
</div>
</form:form>
 
</sec:authorize> 
  </tiles:putAttribute>    
</tiles:insertDefinition>