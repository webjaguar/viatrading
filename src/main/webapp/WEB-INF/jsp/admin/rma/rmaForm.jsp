<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="/WEB-INF/tlds/spring.tld" prefix="spring" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>

<c:if test="${gSiteConfig['gRMA']}">
<tiles:insertDefinition name="admin.rma" flush="true">
  <tiles:putAttribute name="tab"  value="rmas" />
  <tiles:putAttribute name="content" type="string">
<script language="JavaScript" type="text/JavaScript">
<!--
window.addEvent('domready', function(){			
			var tabs1 = new SimpleTabs($('tab-block-1'), { selector: 'h4'});
			var Tips1 = new Tips($$('.toolTipImg'));
		});
//-->
</script>	
<link rel="stylesheet" type="text/css" media="all" href="../../admin/javascript/jscalendar-1.0/skins/aqua/theme.css" title="Aqua" /> <!-- calendar stylesheet -->
<script type="text/javascript" src="../../admin/javascript/jscalendar-1.0/calendar.js"></script> <!-- main calendar program -->
<script type="text/javascript" src="../../admin/javascript/jscalendar-1.0/lang/calendar-en.js"></script>
<script type="text/javascript" src="../../admin/javascript/jscalendar-1.0/calendar-setup.js"></script>

<!-- main box -->
  <div id="mboxfull">
<form:form commandName="rmaForm" method="post">  
  <!-- start table -->
  <table cellpadding="0" cellspacing="0" border="0" class="module" >
  <tr>
    <td class="topl_g">
	  
	  <!-- breadcrumb -->
	  <p class="breadcrumb">
	    <a href="../rma"><fmt:message key='rma' /></a> &gt;
	     <fmt:message key='rmaRequest' /> # <c:out value="${rmaForm.rmaNum}"/> 
	  </p>
	  
	  <!-- Error Message -->
	  <spring:hasBindErrors name="rmaForm">
		<div class="error"><fmt:message key='form.hasErrors'/></div>
	  </spring:hasBindErrors>

	  <div align="right">
		<table class="form">
		  <tr>
		    <td align="right"><fmt:message key="reportDate" /> : </td>
		    <td><fmt:formatDate type="both" timeStyle="full" value="${rmaForm.reportDate}"/></td>
		  </tr>
		  <tr>
		    <td align="right"><fmt:message key="lastModified" /> : </td>
		    <td><fmt:formatDate type="both" timeStyle="full" value="${rmaForm.lastModified}"/></td>
		  </tr>
		</table>
	  </div>
  
  </td><td class="topr_g" ></td></tr>
  <tr><td class="boxmidlrg" >

 <!-- tabs -->
 <div id="tab-block-1">
	<h4 title="<fmt:message key='rmaRequest' />"><fmt:message key='rmaRequest' /></h4>
	<div>
	<!-- start tab -->

        <c:set var="classIndex" value="0" />
        
	  	<div class="listdivi ln tabdivi"></div>
	  	<div class="listdivi"></div>
	   
		  	<div class="list${classIndex % 2}"><c:set var="classIndex" value="${classIndex+1}" />
		  	<div class="listfl"><fmt:message key="rmaRequest" /> #:</div>
		  	<div class="listp">
		  	<!-- input field -->
	  			<b><c:out value="${rmaForm.rmaNum}"/></b>
		    <!-- end input field -->   	
		  	</div>
		  	</div>
		  	
		  	<div class="list${classIndex % 2}"><c:set var="classIndex" value="${classIndex+1}" />
		  	<div class="listfl"><fmt:message key="order" /> #:</div>
		  	<div class="listp">
		  	<!-- input field -->
	  			<a href="../orders/invoice.jhtm?order=${rmaForm.orderId}" class="nameLink"><c:out value="${rmaForm.orderId}"/></a>
		    <!-- end input field -->   	
		  	</div>
		  	</div>

		  	<div class="list${classIndex % 2}"><c:set var="classIndex" value="${classIndex+1}" />
		  	<div class="listfl"><fmt:message key="S/N" /> #:</div>
		  	<div class="listp">
		  	<!-- input field -->
	  			<c:out value="${rmaForm.serialNum}"/>
		    <!-- end input field -->   	
		  	</div>
		  	</div>

		  	<div class="list${classIndex % 2}"><c:set var="classIndex" value="${classIndex+1}" />
		  	<div class="listfl"><fmt:message key="sku" />:</div>
		  	<div class="listp">
		  	<!-- input field -->
	  			<c:out value="${rmaForm.lineItem.product.sku}"/>
		    <!-- end input field -->   	
		  	</div>
		  	</div>

		  	<div class="list${classIndex % 2}"><c:set var="classIndex" value="${classIndex+1}" />
		  	<div class="listfl"><fmt:message key="description" />:</div>
		  	<div class="listp">
		  	<!-- input field -->
	  			<c:out value="${rmaForm.lineItem.product.name}"/>
		    <!-- end input field -->   	
		  	</div>
		  	</div>
		
		  	<div class="list${classIndex % 2}"><c:set var="classIndex" value="${classIndex+1}" />
		  	<div class="listfl"><fmt:message key="unitPrice" />:</div>
		  	<div class="listp">
		  	<!-- input field -->
	  			<fmt:message key="${siteConfig['CURRENCY'].value}" /><fmt:formatNumber value="${rmaForm.lineItem.unitPrice}" pattern="#,##0.00"/>
		    <!-- end input field -->   	
		  	</div>
		  	</div>		
		
		  	<div class="list${classIndex % 2}"><c:set var="classIndex" value="${classIndex+1}" />
		  	<div class="listfl"><fmt:message key="status" />:</div>
		  	<div class="listp">
		  	<c:choose>
		  	<c:when test="${fn:toLowerCase(rmaForm.status) == 'completed'}">
		  	  <c:out value="${rmaForm.status}"/>
		  	</c:when>
		  	<c:otherwise>
		  	<!-- input field -->
	  			<form:select path="status">
		          <form:option value="Pending"/>
		          <form:option value="Awaiting Receipt"/>
		          <form:option value="Evaluation"/>
		          <form:option value="Processing"/>
		          <form:option value="Canceled"/>
		          <form:option value="Completed"/>
		        </form:select>
		    <!-- end input field -->   	
		    </c:otherwise>
		    </c:choose>
		  	</div>
		  	</div>

		  	<div class="list${classIndex % 2}"><c:set var="classIndex" value="${classIndex+1}" />
		  	<div class="listfl"><fmt:message key="completeDate" />:</div>
		  	<div class="listp">
		  	<c:choose>
		  	<c:when test="${fn:toLowerCase(rmaForm.status) == 'completed'}">
		  	  <fmt:formatDate type="date" timeStyle="default" value="${rmaForm.completeDate}"/>
		  	</c:when>
		  	<c:otherwise>		  	
		  	<!-- input field -->
	  			<form:input path="completeDate" size="11" maxlength="10" />
				  <img class="calendarImage"  id="complete_date_trigger" src="../graphics/calendarIcon.jpg" />
				  <script type="text/javascript">
		    		Calendar.setup({
		        			inputField     :    "completeDate",   // id of the input field
		        			showsTime      :    false,
		        			ifFormat       :    "%m/%d/%Y",   // format of the input field
		        			button         :    "complete_date_trigger"   // trigger for the calendar (button ID)
		    		});	
				  </script> 
				<form:errors path="completeDate" cssClass="error"/>		  	
		    <!-- end input field -->   	
		    </c:otherwise>
		    </c:choose>
		  	</div>
		  	</div>

		  	<div class="list${classIndex % 2}"><c:set var="classIndex" value="${classIndex+1}" />
		  	<div class="listfl"><fmt:message key="action" />:</div>
		  	<div class="listp">
		  	<c:choose>
		  	<c:when test="${fn:toLowerCase(rmaForm.status) == 'completed'}">
		  	  <c:out value="${rmaForm.action}"/>
		  	</c:when>
		  	<c:otherwise>
		  	<!-- input field -->
	  			<form:select path="action">
		          <form:option value="Replacement"/>
		          <form:option value="Credit"/>
		        </form:select>
		    <!-- end input field -->   	
		    </c:otherwise>
		    </c:choose>
		  	</div>
		  	</div>

		  	<div class="list${classIndex % 2}"><c:set var="classIndex" value="${classIndex+1}" />
		  	<div class="listfl"><fmt:message key="problem" />:</div>
		  	<div class="listp">
		  	<c:choose>
		  	<c:when test="${fn:toLowerCase(rmaForm.status) == 'completed'}">
		  	  <c:out value="${rmaForm.problem}"/>
		  	</c:when>
		  	<c:otherwise>
		  	<!-- input field -->
	  			<form:input path="problem" size="50" maxlength="255" htmlEscape="true"/>
		    <!-- end input field -->   	
		    </c:otherwise>
		    </c:choose>
		  	</div>
		  	</div>

		  	<div class="list${classIndex % 2}"><c:set var="classIndex" value="${classIndex+1}" />
		  	<div class="listfl"><fmt:message key="comments" />:</div>
		  	<div class="listp">
		  	<c:choose>
		  	<c:when test="${fn:toLowerCase(rmaForm.status) == 'completed'}">
		  	  <c:out value="${rmaForm.comments}"/>
		  	</c:when>
		  	<c:otherwise>
		  	<!-- input field -->
	  			<form:textarea rows="8" cols="60" path="comments" htmlEscape="true" />
		    <!-- end input field -->  
		    </c:otherwise>
		    </c:choose> 	
		  	</div>
		  	</div>
  	
	<!-- end tab -->        
	</div> 
	

<!-- end tabs -->			
</div>

<!-- start button -->
<c:if test="${fn:toLowerCase(rmaForm.status) != 'completed'}">
<div align="left" class="button"> 
  <input type="submit" value="<fmt:message key="update"/>">
  <input type="submit" value="<fmt:message key="cancel"/>" name="_cancel">
</div>
</c:if>
<!-- end button -->	     
  
  <!-- end table -->
    </td><td class="boxmidr" ></td></tr>
  <tr><td class="botl"></td><td class="botr"></td></tr>
  </table>
  
</form:form>
<!-- end main box -->  
</div>

 
  </tiles:putAttribute>    
</tiles:insertDefinition>
</c:if>