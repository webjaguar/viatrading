<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

  <div id="lbox">
   <table cellpadding="0" cellspacing="0" border="0" class="module" style="width: 100%"><tr><td class="topl">
    </td><td class="topr" ></td></tr><tr><td class="boxmid" >

    <h2 class="menuleft mfirst"><fmt:message key="rma"/></h2>
    <div class="menudiv"></div>
      <a href="rmas.jhtm" class="userbutton"><fmt:message key="requests"/></a>
	<div class="menudiv"></div> 
	
    <c:if test="${tab == 'rmas'}"> 
    <div class="menudiv"></div>
      <div align="left" class="searchHeader" id="searchHeaderId"><h2><fmt:message key="search" /></h2></div>
	  <table class="searchBoxLeft"  width="100%"><tr><td>
	  <form action="rmas.jhtm" name="searchform" method="post">
	  <!-- content area --> 
	    <div class="search2">
	      <p><fmt:message key="rma" /> #:</p>
          <input name="rmaNum" type="text" value="<c:out value='${rmaSearch.rmaNum}' />" size="15" />
	    </div>     
	    <div class="search2">
	      <p><fmt:message key="order" /> #:</p>
          <input name="orderId" type="text" value="<c:out value='${rmaSearch.orderId}' />" size="15" />
	    </div>     
	    <div class="search2">
	      <p><fmt:message key="S/N" />:</p>
          <input name="serialNum" type="text" value="<c:out value='${rmaSearch.serialNum}' />" size="15" />
	    </div>
	    <div class="search2">
	      <p><fmt:message key="sku" />:</p>
          <input name="sku" type="text" value="<c:out value='${rmaSearch.sku}' />" size="15" />
	    </div>     
	    <div class="button">
	      <input type="submit" value="Search"/>
	    </div>
	  <!-- content area -->
	  </form>
	  </td></tr></table>
	<div class="menudiv"></div>  
    </c:if>

    </td><td class="boxmidr" ></td></tr>
    <tr><td class="botl"></td><td class="botr"></td></tr></table>
  </div>