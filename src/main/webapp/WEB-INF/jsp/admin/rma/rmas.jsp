<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>

<c:if test="${gSiteConfig['gRMA']}">
<tiles:insertDefinition name="admin.rma" flush="true">
  <tiles:putAttribute name="tab"  value="rmas" />
  <tiles:putAttribute name="content" type="string">
    
<form action="rmas.jhtm" id="list" method="post">
<input type="hidden" id="sort" name="sort" value="${rmaSearch.sort}">
  <!-- main box -->
  <div id="mboxfull">
  
  <!-- start table -->
  <table cellpadding="0" cellspacing="0" border="0" class="module" >
  <tr>
    <td class="topl_g">
	  
	  <!-- breadcrumb -->
	  <p class="breadcrumb">
	    <a href="../rma"><fmt:message key="rma" /></a> &gt;
	    <fmt:message key="requests" /> 
	  </p>
	  
	  <!-- Error Message -->
	  
	  </td><td class="topr_g" ></td></tr>
  <tr><td class="boxmidlrg" >

 <!-- tabs -->
 <div class="tab-wrapper">
	<h4 title="List of rmas"></h4>
	<div>
	<!-- start tab -->

	  	<div class="listdivi"></div>
	   
		  	<div class="listlight">
		  	<!-- input field --> 
			<table border="0" cellpadding="0" cellspacing="1" width="100%">
			  <tr>
			  <td class="pageShowing">
			  <c:if test="${model.list.nrOfElements > 0}">
			  <fmt:message key="showing">
				<fmt:param value="${model.list.firstElementOnPage + 1}"/>
				<fmt:param value="${model.list.lastElementOnPage + 1}"/>
				<fmt:param value="${model.list.nrOfElements}"/>
			  </fmt:message>
			  </c:if>
			  </td>
			  <td class="pageNavi">
			  Page 
			  <select name="page" id="page" onchange="submit()">
			  <c:forEach begin="1" end="${model.list.pageCount}" var="page">
			  	<option value="${page}" <c:if test="${page == (model.list.page+1)}">selected</c:if>>${page}</option>
			  </c:forEach>
			  </select>
			  of <c:out value="${model.list.pageCount}"/>
			  | 
			  <c:if test="${model.list.firstPage}"><span class="pageNaviDead"><fmt:message key="previous" /></span></c:if>
			  <c:if test="${not model.list.firstPage}"><a href="<c:url value="rmas.jhtm"><c:param name="page" value="${model.list.page}"/></c:url>" class="pageNaviLink"><fmt:message key="previous" /></a></c:if>
			  | 
			  <c:if test="${model.list.lastPage}"><span class="pageNaviDead"><fmt:message key="next" /></span></c:if>
			  <c:if test="${not model.list.lastPage}"><a href="<c:url value="rmas.jhtm"><c:param name="page" value="${model.list.page+2}"/></c:url>" class="pageNaviLink"><fmt:message key="next" /></a></c:if>
			  </td>
			  </tr>
			</table>
			
			<table border="0" cellpadding="0" cellspacing="0" width="100%" class="listings">
			  <tr class="listingsHdr2">
			    <td class="indexCol">&nbsp;</td>
				<td class="nameCol">
			    	<table cellspacing="0" cellpadding="1">
			    	  <tr>
			    	    <td rowspan="2" class="listingsHdr3"><fmt:message key="rmaRequest" /> #</td>
			    	    <td><c:choose><c:when test="${rmaSearch.sort == 'rma_num'}"><img src="../graphics/down_off.gif" border="0"></c:when><c:otherwise><a href="#" onClick="document.getElementById('sort').value='rma_num';document.getElementById('list').submit()"><img src="../graphics/down.gif" border="0"></a></c:otherwise></c:choose></td>
			    	  </tr>
			    	</table>
				</td>
				<td align="center">
			    	<table cellspacing="0" cellpadding="1">
			    	  <tr>
			    	    <td rowspan="2" class="listingsHdr3"><fmt:message key="order" /> #</td>
			    	    <td><c:choose><c:when test="${rmaSearch.sort == 'order_id'}"><img src="../graphics/down_off.gif" border="0"></c:when><c:otherwise><a href="#" onClick="document.getElementById('sort').value='order_id';document.getElementById('list').submit()"><img src="../graphics/down.gif" border="0"></a></c:otherwise></c:choose></td>
			    	  </tr>
			    	</table>
				</td>
				<td align="center">
			    	<table cellspacing="0" cellpadding="1">
			    	  <tr>
			    	    <td rowspan="2" class="listingsHdr3"><fmt:message key="S/N" /></td>
			    	    <td><c:choose><c:when test="${rmaSearch.sort == 'serial_num'}"><img src="../graphics/down_off.gif" border="0"></c:when><c:otherwise><a href="#" onClick="document.getElementById('sort').value='serial_num';document.getElementById('list').submit()"><img src="../graphics/down.gif" border="0"></a></c:otherwise></c:choose></td>
			    	  </tr>
			    	</table>
				</td>
				<td align="center">
			    	<table cellspacing="0" cellpadding="1">
			    	  <tr>
			    	    <td rowspan="2" class="listingsHdr3"><fmt:message key="sku" /></td>
			    	    <td><c:choose><c:when test="${rmaSearch.sort == 'product_sku'}"><img src="../graphics/down_off.gif" border="0"></c:when><c:otherwise><a href="#" onClick="document.getElementById('sort').value='product_sku';document.getElementById('list').submit()"><img src="../graphics/down.gif" border="0"></a></c:otherwise></c:choose></td>
			    	  </tr>
			    	</table>
				</td>
			    <td align="center">
			    	<table cellspacing="0" cellpadding="1">
			    	  <tr>
			    	    <td rowspan="2" class="listingsHdr3"><fmt:message key="reportDate" /></td>
			    	    <td><c:choose><c:when test="${rmaSearch.sort == 'report_date DESC'}"><img src="../graphics/up_off.gif" border="0"></c:when><c:otherwise><a href="#" onClick="document.getElementById('sort').value='report_date DESC';document.getElementById('list').submit()"><img src="../graphics/up.gif" border="0"></a></c:otherwise></c:choose></td>
			    	  </tr>
			    	  <tr>
			    	    <td><c:choose><c:when test="${rmaSearch.sort == 'report_date'}"><img src="../graphics/down_off.gif" border="0"></c:when><c:otherwise><a href="#" onClick="document.getElementById('sort').value='report_date';document.getElementById('list').submit()"><img src="../graphics/down.gif" border="0"></a></c:otherwise></c:choose></td>
			    	  </tr>
			    	</table>
			    </td>
			    <td align="center">
			    	<table cellspacing="0" cellpadding="1">
			    	  <tr>
			    	    <td rowspan="2" class="listingsHdr3"><fmt:message key="completeDate" /></td>
			    	    <td><c:choose><c:when test="${rmaSearch.sort == 'complete_date DESC'}"><img src="../graphics/up_off.gif" border="0"></c:when><c:otherwise><a href="#" onClick="document.getElementById('sort').value='complete_date DESC';document.getElementById('list').submit()"><img src="../graphics/up.gif" border="0"></a></c:otherwise></c:choose></td>
			    	  </tr>
			    	  <tr>
			    	    <td><c:choose><c:when test="${rmaSearch.sort == 'complete_date'}"><img src="../graphics/down_off.gif" border="0"></c:when><c:otherwise><a href="#" onClick="document.getElementById('sort').value='complete_date';document.getElementById('list').submit()"><img src="../graphics/down.gif" border="0"></a></c:otherwise></c:choose></td>
			    	  </tr>
			    	</table>
			    </td>
				<td align="center">
			    	<table cellspacing="0" cellpadding="1">
			    	  <tr>
			    	    <td rowspan="2" class="listingsHdr3"><fmt:message key="status" /></td>
			    	    <td><c:choose><c:when test="${rmaSearch.sort == 'status'}"><img src="../graphics/down_off.gif" border="0"></c:when><c:otherwise><a href="#" onClick="document.getElementById('sort').value='status';document.getElementById('list').submit()"><img src="../graphics/down.gif" border="0"></a></c:otherwise></c:choose></td>
			    	  </tr>
			    	</table>
				</td>
			  </tr>
			<c:forEach items="${model.list.pageList}" var="rma" varStatus="status">
			  <tr class="row${status.index % 2}" onmouseover="changeStyleClass(this,'rowOver')" onmouseout="changeStyleClass(this,'row${status.index % 2}')">
			    <td class="indexCol"><c:out value="${status.count + model.list.firstElementOnPage}"/>.</td>
			    <td class="nameCol"><a href="rma.jhtm?num=${rma.rmaNum}" class="nameLink"><c:out value="${rma.rmaNum}"/></a></td>
			    <td align="center"><a href="../orders/invoice.jhtm?order=${rma.orderId}" class="nameLink"><c:out value="${rma.orderId}"/></a></td>
			    <td align="center"><c:out value="${rma.serialNum}"/></td>
			    <td align="center"><c:out value="${rma.lineItem.product.sku}"/></td>
			    <td align="center"><fmt:formatDate type="date" timeStyle="default" value="${rma.reportDate}"/></td>	
			    <td align="center"><fmt:formatDate type="date" timeStyle="default" value="${rma.completeDate}"/></td>	
			    <td align="center"><c:out value="${rma.status}"/></td>
			  </tr>
			</c:forEach>
			<c:if test="${model.list.nrOfElements == 0}">
			  <tr class="emptyList"><td colspan="2">&nbsp;</td></tr>
			</c:if>
			</table>
				  
			<table border="0" cellpadding="0" cellspacing="1" width="100%">
			  <tr>
			  <td class="pageSize">
			  <select name="size" onchange="document.getElementById('page').value=1;submit()">
			    <c:forTokens items="10,25,50,100" delims="," var="current">
			  	  <option value="${current}" <c:if test="${current == model.list.pageSize}">selected</c:if>>${current} <fmt:message key="perPage" /></option>
			    </c:forTokens>
			  </select>
			  </td>
			  </tr>
			</table>	  
		  	<!-- end input field -->  	  
		  	</div>
		  	
		  	<!-- start button --> 
		  	<!-- end button -->	
	  	
	<!-- end tab -->        
	</div>
<!-- end tabs -->			
</div>
  
  <!-- end table -->
   </td><td class="boxmidr" ></td></tr>
  <tr><td class="botl"></td><td class="botr"></td></tr>
  </table>
  
<!-- end main box -->  
</div>
</form>  
 
  </tiles:putAttribute>    
</tiles:insertDefinition>
</c:if>
