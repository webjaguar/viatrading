<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<tiles:insertDefinition name="admin.message" flush="true">
  <tiles:putAttribute name="content" type="string">
  
<c:out value="${message}" escapeXml="false" />
  
  </tiles:putAttribute>
</tiles:insertDefinition>