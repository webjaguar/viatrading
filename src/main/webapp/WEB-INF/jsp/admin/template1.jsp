<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<tiles:importAttribute scope="request"/>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
 <head>
  <title><tiles:getAsString name="title"/></title>
  <c:choose>
  <c:when test="${'roi' == gSiteConfig['gRESELLER']}"><link rel="stylesheet" href="../stylesheetroi.css" type="text/css"/></c:when>
  <c:when test="${'via' == gSiteConfig['gRESELLER']}"><link rel="stylesheet" href="../stylesheetvia.css" type="text/css"/></c:when>
  <c:when test="${'datamagic' == gSiteConfig['gRESELLER']}"><link rel="stylesheet" href="../stylesheetdatamagic.css" type="text/css"/></c:when>
  <c:when test="${'demo' == gSiteConfig['gRESELLER']}"><link rel="stylesheet" href="../stylesheetdemo.css" type="text/css"/></c:when>
  <c:otherwise><link rel="stylesheet" href="../stylesheetaem.css" type="text/css"/></c:otherwise>
  </c:choose>
  <script type="text/javascript" src="../javascript/javascript.js"></script>
  <script type="text/javascript" src="../javascript/mootools-1.2.5-core.js"></script>
  <script type="text/javascript" src="../javascript/mootools-1.2.5.1-more.js"></script>
  <script type="text/javascript" src="../javascript/SimpleTabs1.2.js"></script>
  <script type="text/javascript" src="../javascript/multibox.js"></script>
  <script type="text/javascript" src="../javascript/overlay.js"></script>
  <script type="text/javascript" src="../javascript/dropMenu2.js"></script>
<script type="text/javascript"><!--
window.addEvent('domready', function(){		
    <c:if test="${ hideadmin == null }">
	$$('img.toolTipImg').each(function(element,index) {  
	         var content = element.get('title').split('::');  
	         element.store('tip:title', content[0]);  
	         element.store('tip:text', content[1]);  
	     	});
	</c:if>
	var searchHeaderId = $('searchHeaderId');
    if (searchHeaderId) {
    	searchHeaderId.addEvent('click',function(){
    		document.searchform.submit();
    	});
    }
});
//-->
</script>
</head> 
<body>
<c:choose>
 <c:when test="${ hideadmin == null }">  
  <tiles:insertAttribute name="header" flush="false"/>
  <tiles:insertAttribute name="menu" flush="false"/>

  <tiles:insertAttribute name="content" flush="false"/>
  <c:if test="${'aem' == gSiteConfig['gRESELLER']}">
    <tiles:insertAttribute name="footer" flush="false"/>
  </c:if>
 </c:when>
 <c:otherwise>         
 <div style="width: 400px;position: relative;left: 50%;margin-left: -150px;">
 <div class="dialog">

    <!-- content area -->
    <div class="title">Privileges</div>
    <br />
      <div class="titleBoxRed"></div>
      <div class="arrow_drop arrowImageRight"></div>
      <div style="clear: both;"></div>
      <div id="accessDeny" class="information">
        <div class="helpNote">
          <p><strong>Access is denied! Please contact Administrator...</strong></p>
        </div>
      </div>             
    <!-- content area -->
    <div style="float:right;"><a href="../login.jsp">login</a></div>
</div>    
</div>
</c:otherwise>
</c:choose> 
</body>
</html>