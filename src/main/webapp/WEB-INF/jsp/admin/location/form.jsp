<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="/WEB-INF/tlds/spring.tld" prefix="spring" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ taglib uri="http://www.springframework.org/security/tags" prefix="sec" %>


<tiles:insertDefinition name="admin.location" flush="true">
  <tiles:putAttribute name="content" type="string">
<sec:authorize ifAnyGranted="ROLE_AEM,ROLE_ADMIN_${prop_site_id},ROLE_STORE_LOCATER_UPDATE,ROLE_STORE_LOCATER_CREATE">
<script type="text/javascript" src="<c:url value="/FCKeditor/fckeditor.js"/>"></script>
<c:if test="${gSiteConfig['gLOCATION']}">
<script language="JavaScript"> 
<!--
function loadEditor(el) {
  var oFCKeditor = new FCKeditor(el);
  oFCKeditor.BasePath = "<c:url value="/FCKeditor/"/>";
  oFCKeditor.Config["CustomConfigurationsPath"] = '<c:url value="/admin/javascript/fckconfig.js"/>';
  oFCKeditor.ToolbarSet = 'Html';
  oFCKeditor.Width = 650;
  oFCKeditor.Height = 400;
  oFCKeditor.ReplaceTextarea();
  document.getElementById(el + '_link').style.display="none";
}
//--> 
</script> 
<script language="JavaScript"> 
<!--
window.addEvent('domready', function(){			
	var tabs1 = new SimpleTabs($('tab-block-1'), { selector: 'h4'});
	var Tips1 = new Tips($$('.toolTipImg'));
});
//--> 
</script>  
<form:form commandName="locationForm" action="locationForm.jhtm" method="post">  
<!-- main box -->
  <div id="mboxfull">
  
  <!-- start table -->
  <table cellpadding="0" cellspacing="0" border="0" class="module" >
  <tr>
    <td class="topl_g">
	  
	  <!-- breadcrumb -->
	  <p class="breadcrumb">
	    <a href="../location">Location</a> 
	    <c:if test="${!locationForm.newLocation}">
	      &gt; <c:out value="${locationForm.location.name}" />
	    </c:if> 
	  </p>
	  
	  <!-- Error Message -->
	  <c:if test="${message != null}">
  		<div class="message"><fmt:message key="${message}"/></div>
  	  </c:if>
	  <spring:hasBindErrors name="locationForm">
		<div class="message">Please fix all errors!</div>
	  </spring:hasBindErrors>
				
  </td><td class="topr_g" ></td></tr>
  <tr><td class="boxmidlrg" >

 <!-- tabs -->
 <div id="tab-block-1">
	<h4 title="Location">Location</h4>
	<div>
	<!-- start tab -->
         <c:set var="classIndex" value="0" />
         
	  	<div class="listdivi ln tabdivi"></div>
	  	<div class="listdivi"></div>
	   
		  	<div class="list${classIndex % 2}"><c:set var="classIndex" value="${classIndex+1}" />
		  	<div class="listfl"><fmt:message key="locationName" />:</div>
		  	<div class="listp">
		  	<!-- input field -->
				<form:input path="location.name" maxlength="100" size="60" cssClass="textfield" htmlEscape="true"/>
				<form:errors path="location.name" cssClass="error"/>
	 		<!-- end input field -->	
	 		</div>
		  	</div>
		  	
		  	<div class="list${classIndex % 2}"><c:set var="classIndex" value="${classIndex+1}" />
		  	<div class="listfl"><div class="requiredField"><fmt:message key="address" /> 1:</div></div>
		  	<div class="listp">
		  	<!-- input field -->
				<form:input path="location.addr1" maxlength="100" size="60" cssClass="textfield" htmlEscape="true"/>
				<form:errors path="location.addr1" cssClass="error"/>
	 		<!-- end input field -->	
	 		</div>
		  	</div>
		  	
		  	<div class="list${classIndex % 2}"><c:set var="classIndex" value="${classIndex+1}" />
		  	<div class="listfl"><fmt:message key="address" /> 2:</div>
		  	<div class="listp">
		  	<!-- input field -->
				<form:input path="location.addr2" maxlength="100" size="60" cssClass="textfield" htmlEscape="true"/>
				<form:errors path="location.addr2" cssClass="error"/>
	 		<!-- end input field -->	
	 		</div>
		  	</div>
		  	
		  	<div class="list${classIndex % 2}"><c:set var="classIndex" value="${classIndex+1}" />
		  	<div class="listfl"><fmt:message key="country" />:</div>
		  	<div class="listp">
		  	<!-- input field -->
			  	<form:select id="country" path="location.country" onchange="toggleStateProvince(this)">
	             <form:option value="" label="Please Select"/>
	             <form:options items="${countries}" itemValue="code" itemLabel="name"/>
	            </form:select>
	            <form:errors path="location.country" cssClass="error" />	
		  	<!-- end input field -->	
	 		</div>
		  	</div>
		  	
		  	<div class="list${classIndex % 2}"><c:set var="classIndex" value="${classIndex+1}" />
		  	<div class="listfl"><div class="requiredField"><fmt:message key="city" />:</div></div>
		  	<div class="listp">
		  	<!-- input field -->
				<form:input path="location.city" maxlength="100" size="60" cssClass="textfield" htmlEscape="true"/>
				<form:errors path="location.city" cssClass="error"/>
	 		<!-- end input field -->	
	 		</div>
		  	</div>
		  	
		  	<div class="list${classIndex % 2}"><c:set var="classIndex" value="${classIndex+1}" /> 
            <div class="listfl"><div class="requiredField"><fmt:message key="stateProvince" />:</div></div>
		    <div class="listp">
		    <!-- input field -->
	       <table cellspacing="0" cellpadding="0">
	       <tr>
	       <td>
	         <form:select id="state" path="location.stateProvince">
	           <form:option value="" label="Please Select"/>
	           <form:options items="${states}" itemValue="code" itemLabel="name"/>
	         </form:select>
	         <form:select id="ca_province" path="location.stateProvince">
	           <form:option value="" label="Please Select"/>
	           <form:options items="${caProvinceList}" itemValue="code" itemLabel="name"/>
	         </form:select>
	         <form:input id="province" path="location.stateProvince" htmlEscape="true"/>
	       </td>
	       <td>&nbsp;</td>
	       <td>
	         <form:errors path="location.stateProvince" cssClass="error" />
	       </td>
	       </tr>
	      </table>
           <!-- end input field -->   	
		   </div>
		   </div>	
		  	
		  	<div class="list${classIndex % 2}"><c:set var="classIndex" value="${classIndex+1}" />
		  	<div class="listfl"><fmt:message key="zipcode" />:</div>
		  	<div class="listp">
		  	<!-- input field -->
				<form:input path="location.zip" maxlength="100" size="60" cssClass="textfield" htmlEscape="true"/>
				<form:errors path="location.zip" cssClass="error"/>
	 		<!-- end input field -->	
	 		</div>
		  	</div>
		  	
		  	<div class="list${classIndex % 2}"><c:set var="classIndex" value="${classIndex+1}" />
		  	<div class="listfl"><fmt:message key="phone" />:</div>
		  	<div class="listp">
		  	<!-- input field -->
				<form:input path="location.phone" maxlength="100" size="60" cssClass="textfield" htmlEscape="true"/>
				<form:errors path="location.phone" cssClass="error"/>
	 		<!-- end input field -->	
	 		</div>
		  	</div>	
		  	
		  	<div class="list${classIndex % 2}"><c:set var="classIndex" value="${classIndex+1}" />
		  	<div class="listfl"><fmt:message key="fax" />:</div>
		  	<div class="listp">
		  	<!-- input field -->
				<form:input path="location.fax" maxlength="100" size="60" cssClass="textfield" htmlEscape="true"/>
				<form:errors path="location.fax" cssClass="error"/>
	 		<!-- end input field -->	
	 		</div>
		  	</div>
		  	
		  	<div class="list${classIndex % 2}"><c:set var="classIndex" value="${classIndex+1}" />
		    <div class="listfl"><div class="helpImg"><img class="toolTipImg" title="Note::Seperate each Keyword with comma." src="../graphics/question.gif" /></div><fmt:message key="keywords" />:</div>
		  	<div class="listp">
		  	<!-- input field -->
				<form:textarea rows="8" cols="60" path="location.keywords" cssClass="textfield" htmlEscape="true"/>
				<form:errors path="location.keywords" cssClass="error"/>
	 		<!-- end input field -->	
	 		</div>
		  	</div>			

		  	<div class="list${classIndex % 2}"><c:set var="classIndex" value="${classIndex+1}" />
		  	<div class="listfl"><fmt:message key="note" />:</div>
		  	<div class="listp">
		  	<!-- input field -->
				<form:textarea rows="8" cols="60" path="location.htmlCode" cssClass="textfield" htmlEscape="true"/>
				<form:errors path="location.htmlCode" cssClass="error"/>
				<div style="text-align:left; position: relative; left: 330px;" id="location.htmlCode_link"><a href="#" onClick="loadEditor('location.htmlCode')" class="htmleditor">- <fmt:message key="htmlEditor" /> -</a></div>
	 		<!-- end input field -->	
	 		</div>
		  	</div>	  		
	<!-- end tab -->        
	</div>         
<!-- end tabs -->			
</div>

<!-- start button -->
	<div align="left" class="button"> 
	  <c:if test="${locationForm.newLocation}">
	    <sec:authorize ifAnyGranted="ROLE_AEM,ROLE_ADMIN_${prop_site_id},ROLE_STORE_LOCATER_CREATE">
          <input type="submit" value="<spring:message code="add"/>" />
        </sec:authorize>  
      </c:if>
      <c:if test="${!locationForm.newLocation}">
        <sec:authorize ifAnyGranted="ROLE_AEM,ROLE_ADMIN_${prop_site_id},ROLE_STORE_LOCATER_UPDATE">
          <input type="submit" value="<spring:message code="update"/>" />
        </sec:authorize>
        <sec:authorize ifAnyGranted="ROLE_AEM,ROLE_ADMIN_${prop_site_id},ROLE_STORE_LOCATER_DELETE">
          <input type="submit" value="<spring:message code="delete"/>" name="_delete" onClick="return confirm('Delete permanently?')" />
        </sec:authorize>  
      </c:if>	
	</div>
<!-- end button -->	  

  <!-- end table -->
    </td><td class="boxmidr" ></td></tr>
  <tr><td class="botl"></td><td class="botr"></td></tr>
  </table>
  
<!-- end main box -->  
 </div>
</form:form>  

<script language="JavaScript">
<!--
function toggleStateProvince(el) {
  if (el.value == "US") {
      document.getElementById('state').disabled=false;
      document.getElementById('state').style.display="block";
      document.getElementById('ca_province').disabled=true;
      document.getElementById('ca_province').style.display="none";
      document.getElementById('province').disabled=true;
      document.getElementById('province').style.display="none";
      // document.getElementById('addressStateProvinceNA').disabled=true;
      // document.getElementById('stateProvinceNA').style.display="none";
  } else if (el.value == "CA") {
      document.getElementById('state').disabled=true;
      document.getElementById('state').style.display="none";
      document.getElementById('ca_province').disabled=false;
      document.getElementById('ca_province').style.display="block";
      document.getElementById('province').disabled=true;
      document.getElementById('province').style.display="none";
      // document.getElementById('addressStateProvinceNA').disabled=true;
      // document.getElementById('stateProvinceNA').style.display="none";
  } else {
      document.getElementById('state').disabled=true;
      document.getElementById('state').style.display="none";
      document.getElementById('ca_province').disabled=true;
      document.getElementById('ca_province').style.display="none";
      document.getElementById('province').disabled=false;
      document.getElementById('province').style.display="block";
      // document.getElementById('addressStateProvinceNA').disabled=false;
      // document.getElementById('stateProvinceNA').style.display="block";
  }
}
toggleStateProvince(document.getElementById('country'));
//-->
</script>

</c:if>
</sec:authorize>
  </tiles:putAttribute>    
</tiles:insertDefinition>