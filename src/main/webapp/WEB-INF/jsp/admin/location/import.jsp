<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="/WEB-INF/tlds/spring.tld" prefix="spring" %>
<%@ taglib uri="http://www.springframework.org/security/tags" prefix="sec" %>

<tiles:insertDefinition name="admin.location" flush="true">
 <tiles:putAttribute name="content" type="string">
 
<sec:authorize ifAnyGranted="ROLE_AEM,ROLE_ADMIN_${prop_site_id},ROLE_CUSTOMER_IMPORT_EXPORT">
<script language="JavaScript" type="text/JavaScript">
<!--
window.addEvent('domready', function(){			
			var tabs1 = new SimpleTabs($('tab-block-1'), { selector: 'h4'});
		});
//-->
</script>
<form method="post" name="uploadForm" enctype="multipart/form-data">
<!-- main box -->
  <div id="mboxfull">
  
  <!-- start table -->
  <table cellpadding="0" cellspacing="0" border="0" class="module" >
  <tr>
    <td class="topl_g">
	  
	  <!-- breadcrumb -->
	  <p class="breadcrumb">
	    <a href="../location">Location</a>  &gt;
	    <fmt:message key="import"/>
	  </p>
	  
	  <!-- Error Message -->
	  <c:if test="${!empty message}">
		 <div class="uploadError"><spring:message code="${message}" arguments="${arguments}"/></div>
	  </c:if> 

  
  </td><td class="topr_g" ></td></tr>
  <tr><td class="boxmidlrg" >

 <!-- tabs -->
 <div id="tab-block-1">
	<h4 title="Import">Import</h4>
	<div>
	<!-- start tab -->

        <c:set var="classIndex" value="0" />
        
	  	<div class="listdivi ln tabdivi"></div>
	  	<div class="listdivi"></div>
	   
		  	<div class="list${classIndex % 2}"><c:set var="classIndex" value="${classIndex+1}" />
		  	<div class="listfl">Import Locations:</div>
		  	<div class="listp">
		  	<!-- input field -->				
				<table class="form" width="100%">
				  <tr>
				    <td class="formName"><spring:message code="excelFile" text="Excel File"/>:</td>
				    <td>
						<input value="browse" type="file" name="file"/>
				    </td>
				  </tr>
				</table>
		    <!-- end input field -->   	
		  	</div>
		  	</div>
	  	
	<!-- end tab -->        
	</div> 
	
<!-- end tabs -->			
</div>

<!-- start button -->
<div align="left" class="button"> 
    <input type="submit" name="__upload" value="<spring:message code="fileUpload"/>">
</div>
<!-- end button -->	     
  
  <!-- end table -->
    </td><td class="boxmidr" ></td></tr>
  <tr><td class="botl"></td><td class="botr"></td></tr>
  </table>
  
<!-- end main box -->  
</div>
</form>

</sec:authorize>
  </tiles:putAttribute>    
</tiles:insertDefinition>

