<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/security/tags" prefix="sec" %>


<tiles:insertDefinition name="admin.location" flush="true">
  <tiles:putAttribute name="content" type="string">
  
<sec:authorize ifAnyGranted="ROLE_AEM,ROLE_ADMIN_${prop_site_id},ROLE_STORE_LOCATER_VIEW_LIST">  
<c:if test="${gSiteConfig['gLOCATION']}">	   
<form id="list" action="locationList.jhtm" method="post">
<input type="hidden" id="sort" name="sort" value="${locationSearch.sort}" />
<!-- main box -->
  <div id="mboxfull">
  
  <!-- start table -->
  <table cellpadding="0" cellspacing="0" border="0" class="module" >
  <tr>
    <td class="topl_g">
	  
	  <!-- breadcrumb -->
	  <p class="breadcrumb">
	    <a href="../location"><fmt:message key="location" /></a> &gt;
	    <fmt:message key="list" />
	  </p>
	  
	  <!-- Error -->
	  <c:if test="${!empty model.message}">
		<div class="message"><fmt:message key="${model.message}" /></div>
	  </c:if> 
	  
	  <!-- header image -->
	  <img class="headerImage" src="../graphics/map.gif" /> 
	  
  </td><td class="topr_g" ></td></tr>
  <tr><td class="boxmidlrg" >

 <!-- tabs -->
 <div class="tab-wrapper">
	<h4 title=""></h4>
	<div>
	<!-- start tab -->

	  	<div class="listdivi ln tabdivi"></div>
	  	<div class="listdivi"></div>
	   
		  	<div class="listlight">
		  	<!-- input field -->
			<table border="0" cellpadding="0" cellspacing="1" width="100%">
			  <tr>
			  <c:if test="${model.locations.nrOfElements > 0}">
			  <td class="pageShowing">
			  <fmt:message key="showing">
				<fmt:param value="${model.locations.firstElementOnPage + 1}"/>
				<fmt:param value="${model.locations.lastElementOnPage + 1}"/>
				<fmt:param value="${model.locations.nrOfElements}"/>
			  </fmt:message>
			  </td>
			  </c:if>
			  <td class="pageNavi">
			  <fmt:message key="page" />
			  <select id="page" name="page" onchange="submit()">
			  <c:forEach begin="1" end="${model.locations.pageCount}" var="page">
			  	<option value="${page}" <c:if test="${page == (model.locations.page+1)}">selected</c:if>>${page}</option>
			  </c:forEach>
			  </select>
			  <fmt:message key="of" /> <c:out value="${model.locations.pageCount}"/>
			  | 
			  <c:if test="${model.locations.firstPage}"><span class="pageNaviDead">previous</span></c:if>
			  <c:if test="${not model.locations.firstPage}"><a href="<c:url value="locationList.jhtm"><c:param name="page" value="${model.locations.page}"/></c:url>" class="pageNaviLink">previous</a></c:if>
			  | 
			  <c:if test="${model.locations.lastPage}"><span class="pageNaviDead">next</span></c:if>
			  <c:if test="${not model.locations.lastPage}"><a href="<c:url value="locationList.jhtm"><c:param name="page" value="${model.locations.page+2}"/></c:url>" class="pageNaviLink">next</a></c:if>
			  </td>
			  </tr>
			</table>
			
			<table border="0" cellpadding="0" cellspacing="0" width="100%" class="listings">
			  <tr class="listingsHdr2">
			    <td class="listingsHdr3">&nbsp;</td>
			    <td align="left">
			    	<table cellspacing="0" cellpadding="1">
			    	  <tr>
			    	    <c:choose>
			    	      <c:when test="${locationSearch.sort == 'name DESC'}">
			    	        <td class="listingsHdr3">
			    	        <a class="hdrLink" href="#" onClick="document.getElementById('sort').value='name';document.getElementById('list').submit()"><fmt:message key="locationName" /></a>
			    	        </td>
			    	        <td><img src="../graphics/up.gif" border="0"></td>
			    	      </c:when>
			    	      <c:when test="${locationSearch.sort == 'name'}">
			    	        <td class="listingsHdr3">
			    	        <a class="hdrLink" href="#" onClick="document.getElementById('sort').value='name DESC';document.getElementById('list').submit()"><fmt:message key="locationName" /></a>
			    	        </td>
			    	        <td><img src="../graphics/down.gif" border="0"></td>
			    	      </c:when>
			    	      <c:otherwise>
			    	        <td colspan="2" class="listingsHdr3">
			    	        <a class="hdrLink" href="#" onClick="document.getElementById('sort').value='name DESC';document.getElementById('list').submit()"><fmt:message key="locationName" /></a>
			    	        </td>
			    	      </c:otherwise>
			    	    </c:choose>  
			    	  </tr>
			    	</table>
			    </td>
			    <td class="listingsHdr3"><fmt:message key="id" /></td>
			    <td class="listingsHdr3"><fmt:message key="address" /></td>
			    <td align="left">
			    	<table cellspacing="0" cellpadding="1">
			    	  <tr>
			    	    <c:choose>
			    	      <c:when test="${locationSearch.sort == 'city DESC'}">
			    	        <td class="listingsHdr3">
			    	        <a class="hdrLink" href="#" onClick="document.getElementById('sort').value='city';document.getElementById('list').submit()"><fmt:message key="city" /></a>
			    	        </td>
			    	        <td><img src="../graphics/up.gif" border="0"></td>
			    	      </c:when>
			    	      <c:when test="${locationSearch.sort == 'city'}">
			    	        <td class="listingsHdr3">
			    	        <a class="hdrLink" href="#" onClick="document.getElementById('sort').value='city DESC';document.getElementById('list').submit()"><fmt:message key="city" /></a>
			    	        </td>
			    	        <td><img src="../graphics/down.gif" border="0"></td>
			    	      </c:when>
			    	      <c:otherwise>
			    	        <td colspan="2" class="listingsHdr3">
			    	        <a class="hdrLink" href="#" onClick="document.getElementById('sort').value='city DESC';document.getElementById('list').submit()"><fmt:message key="city" /></a>
			    	        </td>
			    	      </c:otherwise>
			    	    </c:choose>  
			    	  </tr>
			    	</table>
			    </td>
			    <td align="left">
			    	<table cellspacing="0" cellpadding="1">
			    	  <tr>
			    	    <c:choose>
			    	      <c:when test="${locationSearch.sort == 'state_province DESC'}">
			    	        <td class="listingsHdr3">
			    	        <a class="hdrLink" href="#" onClick="document.getElementById('sort').value='state_province';document.getElementById('list').submit()"><fmt:message key="state" /></a>
			    	        </td>
			    	        <td><img src="../graphics/up.gif" border="0"></td>
			    	      </c:when>
			    	      <c:when test="${locationSearch.sort == 'state_province'}">
			    	        <td class="listingsHdr3">
			    	        <a class="hdrLink" href="#" onClick="document.getElementById('sort').value='state_province DESC';document.getElementById('list').submit()"><fmt:message key="state" /></a>
			    	        </td>
			    	        <td><img src="../graphics/down.gif" border="0"></td>
			    	      </c:when>
			    	      <c:otherwise>
			    	        <td colspan="2" class="listingsHdr3">
			    	        <a class="hdrLink" href="#" onClick="document.getElementById('sort').value='state_province DESC';document.getElementById('list').submit()"><fmt:message key="state" /></a>
			    	        </td>
			    	      </c:otherwise>
			    	    </c:choose>  
			    	  </tr>
			    	</table>
			    <td class="listingsHdr3"><fmt:message key="zipcode" /></td>
			    <td class="listingsHdr3"><fmt:message key="phone" /></td>
			  </tr>
			<c:forEach items="${model.locations.pageList}" var="location" varStatus="status">
			  <tr class="row${status.index % 2}" onmouseover="changeStyleClass(this,'rowOver')" onmouseout="changeStyleClass(this,'row${status.index % 2}')">
			    <td class="indexCol"><c:out value="${status.index + 1}"/></td>
			    <td class="nameCol"><c:out value="${location.name}"/></td>
			    <td class="nameCol"><sec:authorize ifAnyGranted="ROLE_AEM,ROLE_ADMIN_${prop_site_id},ROLE_STORE_LOCATER_UPDATE"><a href="locationForm.jhtm?id=${location.id}"></sec:authorize><c:out value="${location.id}"/><sec:authorize ifAnyGranted="ROLE_AEM,ROLE_ADMIN_${prop_site_id},ROLE_STORE_LOCATER_UPDATE"></a></sec:authorize></td>
			    <td class="nameCol"><c:out value="${location.addr1}"/></td>
			    <td class="nameCol"><c:out value="${location.city}"/></td>
			    <td class="nameCol"><c:out value="${location.stateProvince}"/></td>
			    <td class="nameCol"><c:out value="${location.zip}"/></td>
			    <td class="nameCol"><c:out value="${location.phone}"/></td>
			  </tr>
			</c:forEach>
			<c:if test="${model.locations.nrOfElements == 0}">
			  <tr class="emptyList"><td colspan="4">&nbsp;</td></tr>
			</c:if>
			</table>
			
			<table border="0" cellpadding="0" cellspacing="1" width="100%">
			  <tr>
			  <td class="pageSize">
			  <select name="size" onchange="document.getElementById('page').value=1;submit()">
			  <c:forTokens items="10,25,50" delims="," var="current">
			  	  <option value="${current}" <c:if test="${current == model.locations.pageSize}">selected</c:if>>${current} <fmt:message key="perPage" /></option>
			  </c:forTokens>
			  </select>
			  </td>
			  </tr>
			</table>
		    <!-- end input field -->  	  
		  	
		  	<!-- start button -->
		  	<sec:authorize ifAnyGranted="ROLE_AEM,ROLE_ADMIN_${prop_site_id},ROLE_STORE_LOCATER_CREATE"> 
		  	<div align="left" class="button">
				<input type="submit" name="__add" value="<fmt:message key="add" />"/>	
			</div>
			</sec:authorize>
			<!-- end button -->	
	        	  	
	<!-- end tab -->        
	</div>
<!-- end tabs -->			
</div>
  
  <!-- end table -->
   </td><td class="boxmidr" ></td></tr>
  <tr><td class="botl"></td><td class="botr"></td></tr>
  </table>
  
<!-- end main box -->  
</div>
</form>
</c:if>
</sec:authorize>

  </tiles:putAttribute>    
</tiles:insertDefinition>