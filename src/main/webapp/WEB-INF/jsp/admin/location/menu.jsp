<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/security/tags" prefix="sec" %>


  <div id="lbox">
   <table cellpadding="0" cellspacing="0" border="0" class="module" style="width: 100%"><tr><td class="topl">
    </td><td class="topr" ></td></tr><tr><td class="boxmid" >
  
    <h2 class="menuleft mfirst"><fmt:message key="locations"/></h2>
    <div class="menudiv"></div>
    <sec:authorize ifAnyGranted="ROLE_AEM,ROLE_ADMIN_${prop_site_id},ROLE_STORE_LOCATER_VIEW_LIST">
    	<a href="locationList.jhtm" class="userbutton"><fmt:message key="list"/></a>
    </sec:authorize>
    <sec:authorize ifAnyGranted="ROLE_AEM,ROLE_ADMIN_${prop_site_id},ROLE_STORE_LOCATER_IMPORT_EXPORT">
    	<a href="locationImport.jhtm" class="userbutton"><fmt:message key="import"/></a>
    </sec:authorize>
	<div class="menudiv"></div> 

    </td><td class="boxmidr" ></td></tr>
    <tr><td class="botl"></td><td class="botr"></td></tr></table>
  </div>