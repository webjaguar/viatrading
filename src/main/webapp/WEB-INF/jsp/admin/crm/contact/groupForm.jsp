<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="/WEB-INF/tlds/spring.tld" prefix="spring" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ taglib uri="http://www.springframework.org/security/tags" prefix="sec" %>

<tiles:insertDefinition name="admin.crm.group" flush="true">
  <tiles:putAttribute name="content" type="string">
<sec:authorize ifAnyGranted="ROLE_AEM,ROLE_ADMIN_${prop_site_id},ROLE_CRM_GROUP">
  
<script language="JavaScript" type="text/JavaScript">
<!--
window.addEvent('domready', function(){			
	var tabs1 = new SimpleTabs($('tab-block-1'), { selector: 'h4'});
});
//-->
</script>
<form:form commandName="crmContactGroupForm" method="post">
<!-- main box -->
  <div id="mboxfull">
  
  <!-- start table -->
  <table cellpadding="0" cellspacing="0" border="0" class="module" >
  <tr>
    <td class="topl_g">
	  
	  <!-- breadcrumb -->
	  <p class="breadcrumb">
	    <a href="../crm">CRM</a> &gt;
	    <a href="../crm/crmContactGroupList.jhtm"><fmt:message key="groups" /></a> &gt;
	    <c:if test="${!crmContactGroupForm.newCrmContactGroup}"><fmt:message key="groupUpdate" /></c:if><c:if test="${crmContactGroupForm.newCrmContactGroup}"><fmt:message key="groupAdd" /></c:if>  
	  </p>
	  
	  <!-- Error Message -->
  	  <c:if test="${!empty message}">
		  <div class="message"><spring:message code="${message}" /></div>
	  </c:if>
	  <spring:hasBindErrors name="customerGroupForm">
       <span class="error">Please fix all errors!</span>
      </spring:hasBindErrors>

	  </td><td class="topr_g" ></td></tr>
  <tr><td class="boxmidlrg" >

 <!-- tabs -->
 <div id="tab-block-1">
	<h4 title="<fmt:message key='group' />"><fmt:message key="group" /></h4>
	<div>
	<!-- start tab -->
        <c:set var="classIndex" value="0" />
         
	  	<div class="listdivi ln tabdivi"></div>
	  	<div class="listdivi"></div>
	  	
	  	<div class="list${classIndex % 2}"><c:set var="classIndex" value="${classIndex+1}" />
		<div class="listfl"><fmt:message key="active" />:</div>
		<div class="listp">
		<!-- input field -->
            <form:checkbox path="crmContactGroup.active" value="true"/>
        <!-- end input field -->   	
		</div>
		</div>
		
	  	<div class="list${classIndex % 2}"><c:set var="classIndex" value="${classIndex+1}" />
		<div class="listfl"><div class="requiredField"><fmt:message key="Name" />:</div></div>
		<div class="listp">
		<!-- input field -->
            <form:input path="crmContactGroup.name" htmlEscape="true" />
            <form:errors path="crmContactGroup.name" cssClass="error" />
		<!-- end input field -->   	
		</div>
		</div>
		
		  	
		 <div class="list${classIndex % 2}"><c:set var="classIndex" value="${classIndex+1}" />
		 <div class="listfl"><fmt:message key="message"/>:</div>
		 <div class="listp">
		 <!-- input field -->
	   		<select id="messageSelected" name="messageSelected">
			   <option value="">choose a message</option>
			   <c:forEach var="message" items="${messages}">
			   <option value="${message.messageId}" <c:if test="${crmContactGroupForm.crmContactGroup.messageId == message.messageId}">selected="selected"</c:if>><c:out value="${message.messageName}"/></option>
			   </c:forEach>
			</select>
		<!-- end input field -->     	
		</div>
		</div> 
		  	

	<!-- end tab -->        
	</div>  
	        	
<!-- end tabs -->			
</div>

<!-- start button -->
<div align="left" class="button"> 
<c:if test="${crmContactGroupForm.newCrmContactGroup}">
    <input type="submit" value="<fmt:message key="groupAdd"/>" /> 
</c:if>
<c:if test="${!crmContactGroupForm.newCrmContactGroup}">
  <input type="submit" value="<fmt:message key="groupUpdate"/>" />
  <input type="submit" value="<fmt:message key="groupDelete"/>" name="_delete" onClick="return confirm('Delete Group will cause other applications to malfunction which requires this Group. Do you still want to delete it permanently?')" />
</c:if>
  <input type="submit" value="<fmt:message key="cancel"/>" name="_cancel" />
</div>
<!-- end button -->	   

  <!-- end table -->
    </td><td class="boxmidr" ></td></tr>
  <tr><td class="botl"></td><td class="botr"></td></tr>
  </table>
  
<!-- end main box -->  
</div>          	
</form:form>
</sec:authorize>
  </tiles:putAttribute>
</tiles:insertDefinition>