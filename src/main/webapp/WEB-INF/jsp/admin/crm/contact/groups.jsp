<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/security/tags" prefix="sec" %>

<tiles:insertDefinition name="admin.crm.group" flush="true">
  <tiles:putAttribute name="content" type="string"> 
<sec:authorize ifAnyGranted="ROLE_AEM,ROLE_ADMIN_${prop_site_id},ROLE_CRM_GROUP">  
<form action="crmContactGroupList.jhtm" method="post" id="list" name="list_form">
<input type="hidden" id="sort" name="sort" value="${groupCrmContactSearch.sort}" />
  <!-- main box -->
  <div id="mboxfull">
  
  <!-- start table -->
  <table cellpadding="0" cellspacing="0" border="0" class="module" >
  <tr>
    <td class="topl_g">
	  
	  <!-- breadcrumb -->
	  <p class="breadcrumb">
	    <a href="../crm">CRM</a> &gt;
	    <fmt:message key="groups" /> 
	  </p>
	  
	  <!-- Error Message -->
	  
	  <!-- header image -->
	  <img class="headerImage" src="../graphics/customer.gif" />
	  
	  </td><td class="topr_g" ></td></tr>
  <tr><td class="boxmidlrg" >

 <!-- tabs -->
 <div class="tab-wrapper">
	<h4 title=""></h4>
	<div>
	<!-- start tab -->
	  	<div class="listdivi ln tabdivi"></div>
	  	<div class="listdivi"></div>
	   
		  	<div class="listlight">
		  	<!-- input field --> 
		  	<table border="0" cellpadding="0" cellspacing="1" width="100%">
			  <tr>
			  <c:if test="${model.groups.nrOfElements > 0}">
			  <td class="pageShowing">
			  <fmt:message key="showing">
				<fmt:param value="${model.groups.firstElementOnPage + 1}"/>
				<fmt:param value="${model.groups.lastElementOnPage + 1}"/>
				<fmt:param value="${model.groups.nrOfElements}"/>
			  </fmt:message>
			  </td>
			  </c:if>
			  <td class="pageNavi">
			  Page 
			  <select id="page" name="page" onchange="submit()">
			  <c:forEach begin="1" end="${model.groups.pageCount}" var="page">
			  	<option value="${page}" <c:if test="${page == (model.groups.page+1)}">selected</c:if>>${page}</option>
			  </c:forEach>
			  </select>
			  of <c:out value="${model.groups.pageCount}"/>
			  | 
			  <c:if test="${model.groups.firstPage}"><span class="pageNaviDead">previous</span></c:if>
			  <c:if test="${not model.groups.firstPage}"><a href="<c:url value="addressList.jhtm"><c:param name="page" value="${model.groups.page}"/><c:param name="id" value="${model.id}"/></c:url>" class="pageNaviLink">previous</a></c:if>
			  | 
			  <c:if test="${model.groups.lastPage}"><span class="pageNaviDead">next</span></c:if>
			  <c:if test="${not model.groups.lastPage}"><a href="<c:url value="addressList.jhtm"><c:param name="page" value="${model.groups.page+2}"/><c:param name="id" value="${model.id}"/></c:url>" class="pageNaviLink">next</a></c:if>
			  </td>
			  </tr>
			</table>
			
			<table border="0" cellpadding="0" cellspacing="0" width="100%" class="listings">
			  <tr class="listingsHdr2">
			    <td class="indexCol">&nbsp;</td>
			    <td>
			        <table cellspacing="0" cellpadding="1">
			    	  <tr>
			    	    <c:choose>
			    	      <c:when test="${groupCrmContactSearch.sort == 'name desc'}">
			    	        <td class="listingsHdr3">
			    	        <a class="hdrLink" href="#" onClick="document.getElementById('sort').value='name';document.getElementById('list').submit()"><fmt:message key="Name" /></a>
			    	        </td>
			    	        <td><img src="../graphics/up.gif" border="0"></td>
			    	      </c:when>
			    	      <c:when test="${groupCrmContactSearch.sort == 'name'}">
			    	        <td class="listingsHdr3">
			    	        <a class="hdrLink" href="#" onClick="document.getElementById('sort').value='name desc';document.getElementById('list').submit()"><fmt:message key="Name" /></a>
			    	        </td>
			    	        <td><img src="../graphics/down.gif" border="0"></td>
			    	      </c:when>
			    	      <c:otherwise>
			    	        <td colspan="2" class="listingsHdr3">
			    	        <a class="hdrLink" href="#" onClick="document.getElementById('sort').value='name desc';document.getElementById('list').submit()"><fmt:message key="Name" /></a>
			    	        </td>
			    	      </c:otherwise>
			    	    </c:choose>  
			    	  </tr>
			    	  </table>
			    </td>
			    <td>
			    	<table cellspacing="0" cellpadding="1">
			    	  <tr>
			    	    <td rowspan="2" class="listingsHdr3"><fmt:message key="id" /></td>
			          </tr>
			    	</table>
			    </td>
			    <td>
			        <table cellspacing="0" cellpadding="1">
			    	  <tr>
			    	    <c:choose>
			    	      <c:when test="${groupCrmContactSearch.sort == 'num_customer desc'}">
			    	        <td class="listingsHdr3">
			    	        <a class="hdrLink" href="#" onClick="document.getElementById('sort').value='num_customer';document.getElementById('list').submit()"><fmt:message key="customers" /></a>
			    	        </td>
			    	        <td><img src="../graphics/up.gif" border="0"></td>
			    	      </c:when>
			    	      <c:when test="${groupCrmContactSearch.sort == 'num_customer'}">
			    	        <td class="listingsHdr3">
			    	        <a class="hdrLink" href="#" onClick="document.getElementById('sort').value='num_customer desc';document.getElementById('list').submit()"><fmt:message key="customers" /></a>
			    	        </td>
			    	        <td><img src="../graphics/down.gif" border="0"></td>
			    	      </c:when>
			    	      <c:otherwise>
			    	        <td colspan="2" class="listingsHdr3">
			    	        <a class="hdrLink" href="#" onClick="document.getElementById('sort').value='num_customer desc';document.getElementById('list').submit()"><fmt:message key="customers" /></a>
			    	        </td>
			    	      </c:otherwise>
			    	    </c:choose>  
			    	  </tr>
			    	  </table>
			    </td>
			    <td>
			    	<table cellspacing="0" cellpadding="1">
			    	  <tr>
			    	    <td rowspan="2" class="listingsHdr3"><fmt:message key="date" /></td>
			          </tr>
			    	</table>
			    </td>
			  </tr>
			<c:forEach items="${model.groups.pageList}" var="group" varStatus="status">
			  <tr class="row${status.index % 2}" onmouseover="changeStyleClass(this,'rowOver')" onmouseout="changeStyleClass(this,'row${status.index % 2}')" >
			    <td class="indexCol"><c:out value="${status.count + model.addresses.firstElementOnPage}"/>.</td>
			    <td class="nameCol">
			     <a href="<c:url value="crmContactGroupForm.jhtm"><c:param name="id" value="${group.id}"/></c:url>" class="nameLink<c:if test="${!group.active}">Inactive</c:if>">
			     <c:out value="${group.name}"/></a>
			    </td>
			    <td class="nameCol"><c:out value="${group.id}"/></td>
			    <td class="nameCol"><a href="<c:url value="crmContactList.jhtm"><c:param name="groupId" value="${group.id}"/></c:url>" class="nameLink">
			     <c:out value="${group.numContact}"/></a>
			    </td>
			    <td class="nameCol"><fmt:formatDate type="date" timeStyle="default" value="${group.created}"/></td>
			  </tr>
			</c:forEach>
			<c:if test="${model.groups.nrOfElements == 0}">
			  <tr class="emptyList"><td colspan="3">&nbsp;</td></tr>
			</c:if>
			</table>
			
			<table border="0" cellpadding="0" cellspacing="1" width="100%">
			  <tr>
			  <td class="pageSize">
			  <select name="size" onchange="document.getElementById('page').value=1;submit()">
			  <c:forTokens items="10,25,50,100,200,500" delims="," var="current">
			  	  <option value="${current}" <c:if test="${current == groupCrmContactSearch.pageSize}">selected</c:if>>${current} <fmt:message key="perPage" /></option>
			  </c:forTokens>
			  </select>
			  </td>
			  </tr>
			</table>
		  	<!-- end input field -->  	  
		  	</div>
		  	
		  	<!-- start button --> 
		  	<div align="left" class="button">
		  	  
			  <input type="submit" name="__add" value="<fmt:message key="add" />">
			</div>
		  	<!-- end button -->	
	<!-- end tab -->        
	</div>
<!-- end tabs -->			
</div>
  
  <!-- end table -->
   </td><td class="boxmidr" ></td></tr>
  <tr><td class="botl"></td><td class="botr"></td></tr>
  </table>
  
<!-- end main box -->  
</div>
</form>  
</sec:authorize>
  </tiles:putAttribute>    
</tiles:insertDefinition>