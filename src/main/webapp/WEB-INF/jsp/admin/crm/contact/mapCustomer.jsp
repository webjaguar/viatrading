<%@ page import="com.webjaguar.listener.SessionListener,java.util.*" %>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="/WEB-INF/tlds/spring.tld" prefix="spring" %>
<%@ taglib uri="http://www.springframework.org/security/tags" prefix="sec" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>

<tiles:insertDefinition name="admin.crm.contact" flush="true">
  <tiles:putAttribute name="content" type="string">
<sec:authorize ifAnyGranted="ROLE_AEM,ROLE_ADMIN_${prop_site_id}">
<script type="text/javascript"> 
<!--
window.addEvent('domready', function(){			
	var tabs1 = new SimpleTabs($('tab-block-1'), { selector: 'h4'});
	var Tips1 = new Tips($$('.toolTipImg'));
});
//-->
</script>	

<form  action="crmContactCustomerMap.jhtm" method="post">
<!-- main box -->
  <div id="mboxfull">
  
  <!-- start table -->
  <table cellpadding="0" cellspacing="0" border="0" class="module" >
  <tr>
    <td class="topl_g">
	  
	  <!-- breadcrumb -->
	  <p class="breadcrumb">
	    <a href="../crm">CRM</a> &gt;
	    <fmt:message key="mapToCustomer"/>
	  </p>
	  
	  <!-- Error Message -->
	  <c:if test="${!empty message}">
		  <div class="message"><spring:message code="${message}" /></div>
	  </c:if>
	  
	  <!-- header image -->
	  <img class="headerImage" src="../graphics/contact.gif" />
	   
	  <sec:authorize ifAllGranted="ROLE_AEM">
		<div align="right">
		
		</div>
		</sec:authorize>
	  
	  
	  </td><td class="topr_g" ></td></tr>
  <tr><td class="boxmidlrg" >

 <!-- tabs -->
 <div id="tab-block-1">
	<h4 title="<fmt:message key="mapToCustomer" />"><fmt:message key="mapToCustomer" /></h4>
	<div>
	<!-- start tab -->

	  	<div class="listdivi ln tabdivi"></div>
	  	<div class="listdivi"></div>

			<c:set var="classIndex" value="0" />
	  		<div class="list${classIndex % 2}"><c:set var="classIndex" value="${classIndex+1}" />
	  		<div class="listfl"><input type="hidden" name="__key" value="getUsername"><fmt:message key="username" />:</div>
	  		<div class="listp">
	  		<!-- input field -->
				<select name="getUsername" >
		  	      <option value="">Please Select</option>
			      <c:forEach items="${fields}" var="field">
		  	        <option value="${field.value}" <c:if test="${field.value == data['getUsername']}">selected</c:if>>${field.key}</option>
				  </c:forEach>		
				</select>
	    	<!-- end input field -->   	
	  		</div>
	  		</div>
	  		
	  		<div class="list${classIndex % 2}"><c:set var="classIndex" value="${classIndex+1}" />
	  		<div class="listfl"><input type="hidden" name="__key" value="getAddress_getFirstName"><fmt:message key="firstName" />:</div>
	  		<div class="listp">
	  		<!-- input field -->
				<select name="getAddress_getFirstName" >
		  	      <option value="">Please Select</option>
			      <c:forEach items="${fields}" var="field">
			       <option value="${field.value}" <c:if test="${field.value == data['getAddress_getFirstName']}">selected</c:if>>${field.key}</option>
				  </c:forEach>		
				</select>
	    	<!-- end input field -->   	
	  		</div>
	  		</div>
	  		
	  		<div class="list${classIndex % 2}"><c:set var="classIndex" value="${classIndex+1}" />
	  		<div class="listfl"><input type="hidden" name="__key" value="getAddress_getLastName"><fmt:message key="lastName" />:</div>
	  		<div class="listp">
	  		<!-- input field -->
				<select name="getAddress_getLastName" >
		  	      <option value="">Please Select</option>
			      <c:forEach items="${fields}" var="field">
		  	        <option value="${field.value}" <c:if test="${field.value == data['getAddress_getLastName']}">selected</c:if>>${field.key}</option>
				  </c:forEach>		
				</select>
	    	<!-- end input field -->   	
	  		</div>
	  		</div>
	  		
	  		<div class="list${classIndex % 2}"><c:set var="classIndex" value="${classIndex+1}" />
	  		<div class="listfl"><input type="hidden" name="__key" value="getAddress_getAddr1"><fmt:message key="address" />1:</div>
	  		<div class="listp">
	  		<!-- input field -->
				<select name="getAddress_getAddr1" >
		  	      <option value="">Please Select</option>
			      <c:forEach items="${fields}" var="field">
		  	        <option value="${field.value}" <c:if test="${field.value == data['getAddress_getAddr1']}">selected</c:if>>${field.key}</option>
				  </c:forEach>		
				</select>
	    	<!-- end input field -->   	
	  		</div>
	  		</div>
	  		
	  		<div class="list${classIndex % 2}"><c:set var="classIndex" value="${classIndex+1}" />
	  		<div class="listfl"><input type="hidden" name="__key" value="getAddress_getAddr2"><fmt:message key="address" />2:</div>
	  		<div class="listp">
	  		<!-- input field -->
				<select name="getAddress_getAddr2" >
		  	      <option value="">Please Select</option>
			      <c:forEach items="${fields}" var="field">
		  	        <option value="${field.value}" <c:if test="${field.value == data['getAddress_getAddr2']}">selected</c:if>>${field.key}</option>
				  </c:forEach>		
				</select>
	    	<!-- end input field -->   	
	  		</div>
	  		</div>
	  		
	  		<div class="list${classIndex % 2}"><c:set var="classIndex" value="${classIndex+1}" />
	  		<div class="listfl"><input type="hidden" name="__key" value="getAddress_getCity"><fmt:message key="city" />:</div>
	  		<div class="listp">
	  		<!-- input field -->
				<select name="getAddress_getCity" >
		  	      <option value="">Please Select</option>
			      <c:forEach items="${fields}" var="field">
		  	        <option value="${field.value}" <c:if test="${field.value == data['getAddress_getCity']}">selected</c:if>>${field.key}</option>
				  </c:forEach>		
				</select>
	    	<!-- end input field -->   	
	  		</div>
	  		</div>
	  		
	  		<div class="list${classIndex % 2}"><c:set var="classIndex" value="${classIndex+1}" />
	  		<div class="listfl"><input type="hidden" name="__key" value="getAddress_getStateProvince"><fmt:message key="stateProvince" />:</div>
	  		<div class="listp">
	  		<!-- input field -->
				<select name="getAddress_getStateProvince" >
		  	      <option value="">Please Select</option>
			      <c:forEach items="${fields}" var="field">
		  	        <option value="${field.value}" <c:if test="${field.value == data['getAddress_getStateProvince']}">selected</c:if>>${field.key}</option>
				  </c:forEach>		
				</select>
	    	<!-- end input field -->   	
	  		</div>
	  		</div>
	  		
	  		<div class="list${classIndex % 2}"><c:set var="classIndex" value="${classIndex+1}" />
	  		<div class="listfl"><input type="hidden" name="__key" value="getAddress_getZip"><fmt:message key="zipCode" />:</div>
	  		<div class="listp">
	  		<!-- input field -->
				<select name="getAddress_getZip" >
		  	      <option value="">Please Select</option>
			      <c:forEach items="${fields}" var="field">
		  	        <option value="${field.value}" <c:if test="${field.value == data['getAddress_getZip']}">selected</c:if>>${field.key}</option>
				  </c:forEach>		
				</select>
	    	<!-- end input field -->   	
	  		</div>
	  		</div>
	  		
	  		<div class="list${classIndex % 2}"><c:set var="classIndex" value="${classIndex+1}" />
	  		<div class="listfl"><input type="hidden" name="__key" value="getAddress_getCountry"><fmt:message key="country" />:</div>
	  		<div class="listp">
	  		<!-- input field -->
				<select name="getAddress_getCountry" >
		  	      <option value="">Please Select</option>
			      <c:forEach items="${fields}" var="field">
		  	        <option value="${field.value}" <c:if test="${field.value == data['getAddress_getCountry']}">selected</c:if>>${field.key}</option>
				  </c:forEach>		
				</select>
	    	<!-- end input field -->   	
	  		</div>
	  		</div>
	  		
	  		<div class="list${classIndex % 2}"><c:set var="classIndex" value="${classIndex+1}" />
	  		<div class="listfl"><input type="hidden" name="__key" value="getAddress_getPhone"><fmt:message key="phone" />:</div>
	  		<div class="listp">
	  		<!-- input field -->
				<select name="getAddress_getPhone" >
		  	      <option value="">Please Select</option>
			      <c:forEach items="${fields}" var="field">
		  	        <option value="${field.value}" <c:if test="${field.value == data['getAddress_getPhone']}">selected</c:if>>${field.key}</option>
				  </c:forEach>		
				</select>
	    	<!-- end input field -->   	
	  		</div>
	  		</div>
	  		
	  		<div class="list${classIndex % 2}"><c:set var="classIndex" value="${classIndex+1}" />
	  		<div class="listfl"><input type="hidden" name="__key" value="getAddress_getCellPhone"><fmt:message key="cellPhone" />:</div>
	  		<div class="listp">
	  		<!-- input field -->
				<select name="getAddress_getCellPhone" >
		  	      <option value="">Please Select</option>
			      <c:forEach items="${fields}" var="field">
		  	        <option value="${field.value}" <c:if test="${field.value == data['getAddress_getCellPhone']}">selected</c:if>>${field.key}</option>
				  </c:forEach>		
				</select>
	    	<!-- end input field -->   	
	  		</div>
	  		</div>
	  		
	  		<div class="list${classIndex % 2}"><c:set var="classIndex" value="${classIndex+1}" />
	  		<div class="listfl"><input type="hidden" name="__key" value="getAddress_getFax"><fmt:message key="fax" />:</div>
	  		<div class="listp">
	  		<!-- input field -->
				<select name="getAddress_getFax" >
		  	      <option value="">Please Select</option>
			      <c:forEach items="${fields}" var="field">
		  	        <option value="${field.value}" <c:if test="${field.value == data['getAddress_getFax']}">selected</c:if>>${field.key}</option>
				  </c:forEach>		
				</select>
	    	<!-- end input field -->   	
	  		</div>
	  		</div>
	  		
	  		<div class="list${classIndex % 2}"><c:set var="classIndex" value="${classIndex+1}" />
	  		<div class="listfl"><input type="hidden" name="__key" value="getAddress_getCompany"><fmt:message key="company" />:</div>
	  		<div class="listp">
	  		<!-- input field -->
				<select name="getAddress_getCompany" >
		  	      <option value="">Please Select</option>
			      <c:forEach items="${fields}" var="field">
		  	        <option value="${field.value}" <c:if test="${field.value == data['getAddress_getCompany']}">selected</c:if>>${field.key}</option>
				  </c:forEach>		
				</select>
	    	<!-- end input field -->   	
	  		</div>
	  		</div>
	  		
	  		<div class="list${classIndex % 2}"><c:set var="classIndex" value="${classIndex+1}" />
	  		<div class="listfl"><input type="hidden" name="__key" value="getTaxId"><fmt:message key="taxId" />:</div>
	  		<div class="listp">
	  		<!-- input field -->
				<select name="getTaxId" >
		  	      <option value="">Please Select</option>
			      <c:forEach items="${fields}" var="field">
		  	        <option value="${field.value}" <c:if test="${field.value == data['getTaxId']}">selected</c:if>>${field.key}</option>
				  </c:forEach>		
				</select>
	    	<!-- end input field -->   	
	  		</div>
	  		</div>
	  		
	  		<div class="list${classIndex % 2}"><c:set var="classIndex" value="${classIndex+1}" />
	  		<div class="listfl"><input type="hidden" name="__key" value="getNote"><fmt:message key="note" />:</div>
	  		<div class="listp">
	  		<!-- input field -->
				<select name="getNote" >
		  	      <option value="">Please Select</option>
			      <c:forEach items="${fields}" var="field">
		  	        <option value="${field.value}" <c:if test="${field.value == data['getNote']}">selected</c:if>>${field.key}</option>
				  </c:forEach>		
				</select>
	    	<!-- end input field -->   	
	  		</div>
	  		</div>
	  		
	  		<div class="list${classIndex % 2}"><c:set var="classIndex" value="${classIndex+1}" />
	  		<div class="listfl"><input type="hidden" name="__key" value="getTrackcode"><fmt:message key="trackcode" />:</div>
	  		<div class="listp">
	  		<!-- input field -->
				<select name="getTrackcode" >
		  	      <option value="">Please Select</option>
			      <c:forEach items="${fields}" var="field">
		  	        <option value="${field.value}" <c:if test="${field.value == data['getTrackcode']}">selected</c:if>>${field.key}</option>
				  </c:forEach>		
				</select>
	    	<!-- end input field -->   	
	  		</div>
	  		</div>
	  		
	  		<div class="list${classIndex % 2}"><c:set var="classIndex" value="${classIndex+1}" />
	  		<div class="listfl"><input type="hidden" name="__key" value="getRegisteredBy"><fmt:message key="registeredBy" />:</div>
	  		<div class="listp">
	  		<!-- input field -->
				<select name="getRegisteredBy" >
		  	      <option value="">Please Select</option>
			      <c:forEach items="${fields}" var="field">
		  	        <option value="${field.value}" <c:if test="${field.value == data['getRegisteredBy']}">selected</c:if>>${field.key}</option>
				  </c:forEach>		
				</select>
	    	<!-- end input field -->   	
	  		</div>
	  		</div>
	  		
	  		<div class="list${classIndex % 2}"><c:set var="classIndex" value="${classIndex+1}" />
	  		<div class="listfl"><input type="hidden" name="__key" value="getQualifier"><fmt:message key="qualifier" />:</div>
	  		<div class="listp">
	  		<!-- input field -->
				<select name="getQualifier" >
		  	      <option value="">Please Select</option>
			      <c:forEach items="${fields}" var="field">
		  	        <option value="${field.value}" <c:if test="${field.value == data['getQualifier']}">selected</c:if>>${field.key}</option>
				  </c:forEach>		
				</select>
	    	<!-- end input field -->   	
	  		</div>
	  		</div>
	  		
	  		<div class="list${classIndex % 2}"><c:set var="classIndex" value="${classIndex+1}" />
	  		<div class="listfl"><input type="hidden" name="__key" value="getLanguage"><fmt:message key="language" />:</div>
	  		<div class="listp">
	  		<!-- input field -->
				<select name="getLanguage" >
		  	      <option value="">Please Select</option>
			      <c:forEach items="${fields}" var="field">
		  	        <option value="${field.value}" <c:if test="${field.value == data['getLanguageCode']}">selected</c:if>>${field.key}</option>
				  </c:forEach>		
				</select>
	    	<!-- end input field -->   	
	  		</div>
	  		</div>
	  		
	  		<%-- <c:forEach begin="1" end="20" var="num"> --%>
	  		<c:forEach items="${customerFields}" var="customerField" varStatus="num"> 
	  		
	  		<c:choose>
		  		<c:when test="${customerField.multiSelecOnAdmin}">
		  		
		  			<c:set value="getField${num.index + 1}Set" var="number"></c:set>
			  		<div class="list${classIndex % 2}"><c:set var="classIndex" value="${classIndex+1}" />
			  		<div class="listfl"><input type="hidden" name="__key" value="getField${num.index + 1}Set"><fmt:message key="field" />${num.index + 1}:</div>
			  		<div class="listp">
			  		<!-- input field -->
						<select name="getField${num.index + 1}Set" >
				  	      <option value="">Please Select</option>
					      <c:forEach items="${fields}" var="field">
				  	        <option value="${field.value}Set" <c:if test="${field.value == data[number]}">selected</c:if>>${field.key}</option>
						  </c:forEach>		
						</select>
			    	<!-- end input field -->   	
			  		</div>
			  		</div>
		  		
		  		
		  		</c:when>
		  		<c:otherwise>
		  		
		  		
		  			 <c:set value="getField${num.index + 1}" var="number"></c:set>
			  		<div class="list${classIndex % 2}"><c:set var="classIndex" value="${classIndex+1}" />
			  		<div class="listfl"><input type="hidden" name="__key" value="getField${num.index + 1}"><fmt:message key="field" />${num.index + 1}:</div>
			  		<div class="listp">
			  		<!-- input field -->
						<select name="getField${num.index + 1}" >
				  	      <option value="">Please Select</option>
					      <c:forEach items="${fields}" var="field">
				  	        <option value="${field.value}" <c:if test="${field.value == data[number]}">selected</c:if>>${field.key}</option>
						  </c:forEach>		
						</select>
			    	<!-- end input field -->   	
			  		</div>
			  		</div>
		  		
		  		</c:otherwise>
	  		</c:choose>
	  		
	  		

	  		</c:forEach>
	  		
	<!-- end tab -->        
	</div>       
  	
<!-- end tabs -->			
</div>

<!-- start button --> 
<div align="left" class="button"> 
  <sec:authorize ifAnyGranted="ROLE_AEM,ROLE_ADMIN_${prop_site_id}">
	<input type="submit" name="__update" value="<fmt:message key="Update" />">
  </sec:authorize>
</div>	
<!-- end button -->	
  
  <!-- end table -->
   </td><td class="boxmidr" ></td></tr>
  <tr><td class="botl"></td><td class="botr"></td></tr>
  </table>
  
<!-- end main box -->  
 </div>  
</form> 
</sec:authorize>
  </tiles:putAttribute>    
</tiles:insertDefinition>


