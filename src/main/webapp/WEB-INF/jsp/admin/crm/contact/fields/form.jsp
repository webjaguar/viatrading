<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ taglib uri="/WEB-INF/tlds/spring.tld" prefix="spring" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ taglib uri="http://www.springframework.org/security/tags" prefix="sec" %>

<tiles:insertDefinition name="admin.crm.contact" flush="true">
  <tiles:putAttribute name="content" type="string">
<sec:authorize ifAnyGranted="ROLE_AEM,ROLE_ADMIN_${prop_site_id},ROLE_CRM_FIELD">
<script language="JavaScript"> 
<!--
window.addEvent('domready', function(){			
	var tabs1 = new SimpleTabs($('tab-block-1'), { selector: 'h4'});
	var Tips1 = new Tips($$('.toolTipImg'));
});
//--> 
</script> 
<form:form commandName="crmContactFieldForm" method="post">
<form:hidden path="newCrmContactField" />

<!-- main box -->
  <div id="mboxfull">
  
  <!-- start table -->
  <table cellpadding="0" cellspacing="0" border="0" class="module" >
  <tr>
    <td class="topl_g">
	  
	  <!-- breadcrumb -->
	  <p class="breadcrumb">
	    <a href="../crm">CRM</a> &gt;
	    <a href="../crm/crmContactFieldList.jhtm">Fields</a> &gt;
	    form 
	  </p>
	  
	  <!-- Error Message -->
	  <spring:hasBindErrors name="crmContactFieldForm">
		<div class="message">Please fix all errors!</div>
	  </spring:hasBindErrors>

  </td><td class="topr_g" ></td></tr>
  <tr><td class="boxmidlrg" >

 <!-- tabs -->
 <div id="tab-block-1">
	<h4 title="<fmt:message key="field"/>"><fmt:message key="field"/></h4>
	<div>
	<!-- start tab -->
 
	    <c:set var="classIndex" value="0" />
		<div class="listdivi ln tabdivi"></div>
		
		 <div class="list${classIndex % 2}"><c:set var="classIndex" value="${classIndex+1}" />
		 <div class="listfl"><div class="helpImg"><img class="toolTipImg" title="Note::If checked, field will appear on Contact." src="../graphics/question.gif" /></div><fmt:message key="enabled" />:</div>
		 <div class="listp">
		 <!-- input field -->
                <form:checkbox path="crmContactField.enabled" />
         <!-- end input field -->   	
		 </div>
		 </div>
		 
		 <div class="list${classIndex % 2}"><c:set var="classIndex" value="${classIndex+1}" />
		 <div class="listfl"><div class="helpImg"><img class="toolTipImg" title="Note::In order to match with Customer fields, ensure names are same. i.e., name should be 'Field 1', 'Field 2', etc." src="../graphics/question.gif" /></div><div class="requiredField"><fmt:message key="field" />:</div></div>
		 <div class="listp">
		 <!-- input field -->
		 		<c:choose>
		 		  <c:when test="${crmContactFieldForm.crmContactField.id lt 21 and crmContactFieldForm.crmContactField.id gt 0}">
		 		    <form:input path="crmContactField.globalName" cssClass="textfield" maxlength="255" size="60" htmlEscape="true" readonly="true"/>
		 		  </c:when>
		 		  <c:otherwise>
		 		     <form:input path="crmContactField.globalName" cssClass="textfield" maxlength="255" size="60" htmlEscape="true" />
		 		  </c:otherwise>
		 		</c:choose>
		 	<form:errors path="crmContactField.globalName" cssClass="error" />
	    <!-- end input field -->	
	 	 </div>
		 </div>
		 
		 <div class="list${classIndex % 2}"><c:set var="classIndex" value="${classIndex+1}" />
		 <div class="listfl"><fmt:message key="name" />:</div>
		 <div class="listp">
		 <!-- input field -->
		 		<form:input path="crmContactField.fieldName" cssClass="textfield" maxlength="255" size="60" htmlEscape="true"/>
		 <!-- end input field -->	
	 	 </div>
		 </div>
		 
		 <div class="list${classIndex % 2}"><c:set var="classIndex" value="${classIndex+1}" />
		 <div class="listfl"><fmt:message key="type" />:</div>
		 <div class="listp">
		 <!-- input field -->
		 		<form:select path="crmContactField.fieldType">
		 		  <form:option value="1" label="Text"></form:option>
		 		  <form:option value="2" label="Text Area"></form:option>
		 		  <form:option value="3" label="Check Box"></form:option>
		 		  <form:option value="4" label="Radio Button"></form:option>
		 		  <form:option value="5" label="Drop Down List"></form:option>
		 		  <form:option value="7" label="Validation"></form:option>
		 		</form:select>
		 <!-- end input field -->	
	 	 </div>
		 </div>
		 
		 <div class="list${classIndex % 2}"><c:set var="classIndex" value="${classIndex+1}" />
		 <div class="listfl"><fmt:message key="options" />:</div>
		 <div class="listp">
		 <!-- input field -->
		 		<form:textarea path="crmContactField.options" cssClass="textarea" htmlEscape="true"/>
		 <!-- end input field -->	
	 	 </div>
		 </div>

	<!-- end tab -->        
	</div> 
</div>	
	

<!-- start button -->
	<div align="left" class="button">
	  <c:if test="${crmContactFieldForm.newCrmContactField}">
	    <sec:authorize ifAnyGranted="ROLE_AEM,ROLE_ADMIN_${prop_site_id},ROLE_CRM">
          <input type="submit" value="<spring:message code="add"/>" />
        </sec:authorize>  
      </c:if>
      <c:if test="${!crmContactFieldForm.newCrmContactField}">
        <sec:authorize ifAnyGranted="ROLE_AEM,ROLE_ADMIN_${prop_site_id},ROLE_CRM">
          <input type="submit" value="<spring:message code="update"/>" />
        </sec:authorize>
        <c:if test="${crmContactFieldForm.crmContactField.id > 20}">
        <sec:authorize ifAnyGranted="ROLE_AEM,ROLE_ADMIN_${prop_site_id},ROLE_CRM">  
          <input type="submit" value="<spring:message code="delete"/>" name="_delete" onClick="return confirm('Delete permanently?')" />
        </sec:authorize>  
        </c:if>
      </c:if>
	</div>
<!-- end button -->
  
  <!-- end table -->
    </td><td class="boxmidr" ></td></tr>
  <tr><td class="botl"></td><td class="botr"></td></tr>
  </table>
  
<!-- end main box -->  
</div>
</form:form>
</sec:authorize> 

  </tiles:putAttribute>    
</tiles:insertDefinition>