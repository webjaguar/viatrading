<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html>
  <head>
    <title>Customer Info</title>
    <link href="<c:url value="../stylesheet${gSiteConfig['gRESELLER']}.css"/>" rel="stylesheet" type="text/css"/>
    <script type="text/javascript" src="../javascript/mootools-1.2.5-core.js"></script>
    <script type="text/javascript" src="../javascript/mootools-1.2.5.1-more.js"></script>
    <script type="text/javascript" src="../javascript/SimpleTabs1.2.js"></script>
    <style type="text/css">
     .userInfo td {font-size:10px;color:#222222;}
     .nameCol {font-size:10px;}
    </style>
  </head>  
<body style="background-color:#ffffff;width:860px !important;">
<script type="text/javascript"> 
window.addEvent('domready', function(){			
			var tabs1 = new SimpleTabs($('tab-block-1'), { selector: 'h4'});
		});
</script>
<!-- main box -->
  <div id="mboxfullpopup">
      <div class="title">
        <span><fmt:message key="contactInformation"/></span>
      </div>
      <div class="print">
      	<a href="javascript:window.print()"><img border="0" src="../graphics/printer.png" /></a>
      </div>
  
  <!-- start table -->
  <table cellpadding="0" cellspacing="0" border="0" style="float:left">
  <tr><td class="boxmidlrg" >
      
 <!-- tabs -->
 <div id="tab-block-1">
	<h4 title="<fmt:message key="contactInformation"/>"><fmt:message key="contactInformation"/></h4>
	<div>
	<!-- start tab -->
		<table border="0" width="100%" style="height:340px;width:865px;border:1px solid #999999;background-color:#f3f4f5;clear:both;">
		<tr style="height:150px;">
	      <td valign="top"><img src="../graphics/customer.png" width="89" height="137" /></td>
	      <td valign="top" >
	        <table border="0" class="userInfo" cellpadding="0" cellspacing="0">
	         <tr>
	          <td><fmt:message key="customer" /></td>
	          <td class="spacer"> : </td>
	          <td><c:out value="${model.contact.firstName}" /> <c:out value="${model.contact.lastName}" /></td>
	         </tr>
	         <c:if test="${gSiteConfig['gACCESS_PRIVILEGE'] and model.contact.accessUserId != null}">
	         <tr>
	           <td><fmt:message key="assignedTo" /></td>
	            <td class="spacer"> : </td>
	            <td><c:out value="${model.accessUserMap[model.contact.accessUserId].name}" /></td>
	         </tr>   
	         </c:if>
	         <tr>
	          <td><fmt:message key="account" /></td>
	          <td class="spacer"> : </td>
	          <td ><c:out value="${model.contact.accountName}" /></td>
	          <td></td>
	         </tr>
	         <c:if test="${!empty model.contact.email1}">
	         <tr>
	          <td><fmt:message key="email" />1</td>
	          <td class="spacer"> : </td>
	          <td><c:out value="${model.contact.email1}" /></td>
	         </tr>
	         </c:if>
	         <c:if test="${!empty model.contact.phone1}">
	         <tr>
	          <td><fmt:message key="phone" />1</td>
	          <td class="spacer"> : </td>
	          <td><c:out value="${model.contact.phone1}" /></td>
	         </tr>
	         </c:if>
	         <c:if test="${!empty model.contact.fax}">
	         <tr>
	          <td><fmt:message key="fax" /></td>
	          <td class="spacer"> : </td>
	          <td><c:out value="${model.contact.fax}" /></td>
	         </tr>
	         </c:if>
	        </table>
	      </td>
	      <td></td>
	      <td valign="top">
	        <table border="0" class="userInfo" cellpadding="0" cellspacing="0">
	         <c:if test="${!empty model.contact.created}">
	         <tr>
	           <td><fmt:message key="created" /></td>
	            <td class="spacer"> : </td>
	            <td><fmt:formatDate pattern="MM-dd-yyyy" value="${model.contact.created}" /></td>
	         </tr>   
	         </c:if>
	         <c:if test="${!empty model.contact.leadSource}">
	         <tr>
	           <td><fmt:message key="leadSource" /></td>
	            <td class="spacer"> : </td>
	            <td><c:out value="${model.contact.leadSource}" /></td>
	         </tr>   
	         </c:if>
	         <c:if test="${!empty model.contact.contactType}">
	         <tr>
	           <td><fmt:message key="crmContactType" /></td>
	            <td class="spacer"> : </td>
	            <td><c:out value="${model.contact.contactType}" /></td>
	         </tr>
	         </c:if>
	         <c:if test="${!empty model.contact.rating}">
	         <tr>
	           <td><fmt:message key="crmRating" /></td>
	            <td class="spacer"> : </td>
	            <td><c:out value="${model.contact.rating}" /></td>
	         </tr>
	         </c:if>
	         <c:if test="${!empty model.contact.department}">
	         <tr>
	           <td><fmt:message key="department" /></td>
	            <td class="spacer"> : </td>
	            <td><c:out value="${model.contact.department}" /></td>
	         </tr>
	         </c:if>
	         </table>
	      </td>
	      </tr>
	      <tr>
	        <td colspan="5" valign="top" style="border-top:1px solid #999999;">
	        <div style="font-size:14px; color:#8C8C8C">Contact Fields</div>
	        <c:forEach items="${model.contact.crmContactFields}" var="contactField" varStatus="status">
	        <div style="padding:3px; margin:3px;">
	        <table class="userInfo" border="0" cellpadding="0" cellspacing="0">
	         <tr>
	          <td><c:out value="${contactField.fieldName}"/></td>
	          <td> : </td>
	          <td><c:out value="${contactField.fieldValue}"/></td>
	         </tr>
	        </table>
	        </div>
	        </c:forEach>
	        </td>
	      </tr>
	      <tr>
	        <td colspan="5" valign="top" style="border-top:1px solid #999999;">
	        <div style="font-size:14px; color:#8C8C8C">Note</div>
	        <table class="userInfo" border="0" cellpadding="0" cellspacing="0">
	         <tr>
	          <td><c:out value="${model.contact.description}" /></td>
	         </tr>
	        </table>  
	        </td>
	      </tr>
	      </table>
	      <!-- end tab -->        
	      </div>
	      	      
	   
		  <h4 title="Purchase History">Activity History</h4>
		  <div>
		  <!-- start tab -->
		  <table border="0" style="clear:both;width:100%;border:1px solid #999999;background-color:#eeeeee;">
		    <tr>
			  <td>
			  <c:forEach items="${model.contactTaskList}" var="task" varStatus="status">
			      <div class="taskMain">
			        <div class="tHeader"><c:out value="${task.title}"/></div>
			        <div class="tBody">
			          <table border="0" cellpadding="0" cellspacing="0" width="100%" class="listings">
			            <tr>
					      <th>Type</th>
						  <td><fmt:message key="task_type_${task.type}" /></td>
						</tr>
						<tr>
					      <th>Creation Date</th>
						  <td><fmt:formatDate type="date" pattern="MM/dd/yyyy" value="${task.created}"/></td>
						</tr>
					    <tr>
					      <th>Due Date</th>
						  <td><fmt:formatDate type="date" pattern="MM/dd/yyyy" value="${task.dueDate}"/></td>
						</tr>
						<tr>
					      <th>Status</th>
						  <td><fmt:message key="taskStatus_${task.status}" /></td>
						</tr>
						<tr>
					      <th>Assigned To</th>
						  <td><c:out value="${task.assignedTo}"  escapeXml="false"/></td>
						</tr>
						<tr>
					      <th>Comment</th>
					<!--  	  <td><textarea rows="10" cols="120" disabled="disabled" readonly="readonly">${task.description}</textarea></td>
					-->
							  <td><c:out value="${task.description}"  escapeXml="false"/></td>
						</tr>
					  </table>
			        </div>
			      </div>
			  </c:forEach>      
			  </td>
			</tr>
	      </table>	
		    <!-- end tab -->   
		    
		  <h4 title="Purchase History">Account History</h4>
		  <div>
		  <!-- start tab -->
		  <table border="0" style="clear:both;width:100%;border:1px solid #999999;background-color:#eeeeee;">
		    <tr>
			  <td>
			  <c:forEach items="${model.accountTaskList}" var="task" varStatus="status">
			      <div class="taskMain">
			        <div class="tHeader"><c:out value="${task.title}"/>  <span style="float: right;"> <c:out value="${task.contactName}"/></span>  </div>
			        <div class="tBody">
			          <table border="0" cellpadding="0" cellspacing="0" width="100%" class="listings">
			            <tr>
					      <th>Type</th>
						  <td><fmt:message key="task_type_${task.type}" /></td>
						</tr>
						<tr>
					      <th>Creation Date</th>
						  <td><fmt:formatDate type="date" pattern="MM/dd/yyyy" value="${task.created}"/></td>
						</tr>
					    <tr>
					      <th>Due Date</th>
						  <td><fmt:formatDate type="date" pattern="MM/dd/yyyy" value="${task.dueDate}"/></td>
						</tr>
						<tr>
					      <th>Status</th>
						  <td><fmt:message key="taskStatus_${task.status}" /></td>
						</tr>
						<tr>
					      <th>Assigned To</th>
						  <td><c:out value="${task.assignedTo}"/></td>
						</tr>
						<tr>
					      <th>Comment</th>
						  <td><textarea rows="10" cols="120" disabled="disabled" readonly="readonly">${task.description}</textarea></td>
						</tr>
					  </table>
			        </div>
			      </div>
			  </c:forEach>      
			  </td>
			</tr>
	      </table>	
		    <!-- end tab -->   
		         
			</div>
						
		 </div>
		 <!-- end tabs --> 
 
 <!-- end table -->
 </td><td class="boxmidr" ></td></tr>
  </table>
  
<!-- end main box -->  
</div>    

</body>
</html>