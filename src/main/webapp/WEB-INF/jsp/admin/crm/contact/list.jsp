<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/security/tags" prefix="sec" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>

<tiles:insertDefinition name="admin.crm.contact" flush="true">
  <tiles:putAttribute name="content" type="string">
 
<sec:authorize ifAnyGranted="ROLE_AEM,ROLE_ADMIN_${prop_site_id},ROLE_CRM">
<script type="text/javascript" src="../../admin/javascript/multiple.open.accordion1.2.js"></script>
<script type="text/javascript">
<!--
	window.addEvent('domready', function(){
		$$('img.action').each(function(img){
			//containers
			var actionList = img.getParent();
			var actionHover = actionList.getElements('div.action-hover')[0];
			actionHover.set('opacity',0);
			//show/hide
			img.addEvent('mouseenter',function() {
				actionHover.setStyle('display','block').fade('in');
			});
			actionHover.addEvent('mouseleave',function(){
				actionHover.fade('out');
			});
			actionList.addEvent('mouseleave',function() {
				actionHover.fade('out');
			});
		});
		$$('div.testHover').each(function(text){
			//containers
			var actionList = text.getParent();
			var actionHover = actionList.getElements('div.action-hover')[0];
			actionHover.set('opacity',0);
			//show/hide
			text.addEvent('mouseenter',function() {
				actionHover.setStyle('display','block').fade('in');
			});
			actionHover.addEvent('mouseleave',function(){
				actionHover.fade('out');
			});
			actionList.addEvent('mouseleave',function() {
				actionHover.fade('out');
			});
		});
		var note = new multiBox('mbNote', {showControls : false, useOverlay: false, showNumbers: false });
		var box2 = new multiBox('mbContact', {descClassName: 'multiBoxDesc',waitDuration: 5,showControls: false,overlay: new overlay()});
		// Create the accordian
		var Tips1 = new Tips('.toolTipImg');  
		// Create the accordian
		var myAccordion = new Fx.Accordion($$('div.arrow_drop'), $$('div.information'), {
			display: 1, alwaysHide: true,
	    	onActive: function() {$('information').removeClass('displayNone');}
		});
		<sec:authorize ifAnyGranted="ROLE_AEM,ROLE_ADMIN_${prop_site_id},ROLE_QUICK_EXPORT">
		$('quickModeTrigger').addEvent('click',function() {
			$('mboxfull').setStyle('margin', '0');
		    $$('.quickMode').each(function(el){
		    	el.hide();
		    });
		    $$('.quickModeRemove').each(function(el){
		    	el.destroy();
		    });
		    	
		    $$('.action').each(function(el){
		    	el.hide();
		    });
		    $$('.listingsHdr3').each(function(el){
		    	el.removeProperties('colspan', 'class');
		    });
		    alert('Select the whole page, copy and paste to your excel file.');
		});
	    </sec:authorize> 
	});	    
function toggleAll(el) {
	var ids = document.getElementsByName("__selected_id");	
  	if (ids.length == 1)
      document.list_form.__selected_id.checked = el.checked;
    else
      for (i = 0; i < ids.length; i++)
        document.list_form.__selected_id[i].checked = el.checked;	
} 
function optionsSelected() {
	var ids = document.getElementsByName("__selected_id");	
  	if (ids.length == 1) {
      if (document.list_form.__selected_id.checked) {
    	return true;
      }
    } else {
      for (i = 0; i < ids.length; i++) {
        if (document.list_form.__selected_id[i].checked) {
    	  return true;
        }
      }
    }
  	if (document.list_form.__groupAssignAll.checked) {
   	   return true;
     }
    alert("Please select contact(s).");       
    return false;
} 
function assignValue(e, ele, id) {
	document.getElementById('__note_'+id).value = e;
}
function addNewNote(contactId) {
	var note = $('__note_'+contactId).value;
	var request = new Request({
		url: "ajax-dialing-note.jhtm?contactId="+contactId+"&note="+note,
		method: 'post',
		onComplete: function(response) { 
			location.reload(true);
		}
	}).send();
}
window.onload = function() { closemenu(); };
function openmenu() {
	document.getElementById("mboxfull").style.margin="7px 0 0 151px";
	document.getElementById("lbox").style.width="144px";
	if(document.getElementById("footer")!=null)
	document.getElementById("footer").style.margin="2px 0 2px 151px";
	document.getElementById('lbox').style.width='144px';
	document.getElementById('module').style.display='block';
	document.getElementById('tbid').style.display='block';
	document.getElementById('topl').style.display='block';
	document.getElementById('botl').style.display='block';
	document.getElementById('botr').style.display='block'; 
	document.getElementById('adjust').value="close menu";
}
	
function closemenu(){	
	document.getElementById("mboxfull").style.margin="7px 0 0 0px";
	document.getElementById("lbox").style.margin="0px";
	if(document.getElementById("footer")!=null)
	document.getElementById("footer").style.margin="2px 0 2px 0px";
	document.getElementById('lbox').style.width='0px';
	document.getElementById('module').style.display='none';
	document.getElementById('tbid').style.display='none';
	document.getElementById('topl').style.display='none';
	document.getElementById('botl').style.display='none';
	document.getElementById('botr').style.display='none';
	document.getElementById('adjust').value="open menu";
}

function adjustPageSize() {
	if(document.getElementById('adjust').value=="open menu"){
		openmenu();
	}else{
		closemenu();
	}
}
//-->
</script>
<form action="crmContactList.jhtm" method="post" id="list" name="list_form">
<input type="hidden" id="sort" name="sort" value="${crmContactSearch.sort}" />
<!-- main box -->
  <div id="mboxfull">
   <input type="button" onClick="adjustPageSize();" id="adjust" value="open menu"/>
  <!-- start table -->
  <table cellpadding="0" cellspacing="0" border="0" class="module" >
  <tr class="quickMode">
    <td class="topl_g">
	  
	  <!-- breadcrumb -->
	  <p class="breadcrumb">
	    <a href="../crm">CRM</a> &gt;
	    Contacts
	  </p>
	  
	  <!-- Error Message -->
	  
	  <!-- header image -->
	  <img id="quickModeTrigger" class="headerImage" src="../graphics/contact.gif" />
	  
	  <!-- Options -->  
	  <c:if test="${model.count > 0}">
	    <div class="optionBox"><div class="titlePrivilege"><fmt:message key="options" /></div></div>
		<div class="arrow_drop arrowImageRight"></div>
		<div style="clear: both;"></div>
		<div class="information displayNone" id="information" style="float:left;">    
			 <div style="float:left;padding: 5px">
	           <table class="productBatchTool" cellpadding="5" cellspacing="5">
	           <tr>
	            <td valign="top">
	             <table cellpadding="1" cellspacing="1" width="100%">
	              <tr style="height:30px;white-space: nowrap">
	               <td valign="top" width="20px"><img class="toolTipImg" title="Group Id::Associate group Id to the following contact(s) in the list" src="../graphics/question.gif" /></td>
	               <td valign="top"><h3><fmt:message key="groupId" /></h3></td>
	               <td valign="top" align="right"><input type="checkbox" onclick="toggleAll(this)" name="__groupAssignAll" value="true" /> <fmt:message key="all"/></td>
	              </tr>
	             </table>
	            </td>
	           </tr>
	           <tr>
	            <td valign="top">
	             <table cellpadding="1" cellspacing="1">
	              <tr style="">
			       <td>
			        <input name="__group_id" type="text" size="15"/>
			        <div align="left" class="buttonLeft">
		            <input type="submit" name="__groupAssignPacher" value="Apply" onClick="return optionsSelected()">
		            </div>
			       </td>
			       <td >
			        <div><p><input name="__batch_type" type="radio" checked="checked" value="add"/>Add</p></div>
				    <div><p><input name="__batch_type" type="radio" value="remove"/>Remove</p></div>
				    <div><p><input name="__batch_type" type="radio" value="move"/>Move</p></div>
			       </td>
			     </tr>
	             </table>
	            </td>
	           </tr>
	           </table>
	         </div>
	         <div style="float:left;padding: 5px">
	           <table class="productBatchTool" cellpadding="5" cellspacing="5">
	           <tr>
	            <td valign="top">
	             <table cellpadding="1" cellspacing="1" width="100%">
	              <tr style="height:10px;white-space: nowrap">
	               <td valign="top" width="20px"><img class="toolTipImg" title="Task ::Associate task to the following contact(s) in the list" src="../graphics/question.gif" /></td>
	               <td valign="top"><h3><fmt:message key="crmTask" /></h3></td>
	               <td valign="top"  align="right"><input type="checkbox" onclick="toggleAll(this)" name="__taskAssignAll" value="true" /> <fmt:message key="all"/></td>
	              </tr>
	             </table>
	            </td>
	           </tr>
	           <tr >
	            <td valign="top">
	             <table cellpadding="1" cellspacing="1">
	              <tr>
			       <td style="font-size: 7px;"><fmt:message key="title" />:</td>
			       <td><input name="task_title" type="text" size="15"/></td>
			      </tr>
	              <tr>
			       <td style="font-size: 7px;"><fmt:message key="type" />:</td>
			       <td>
			         <select name="task_type" style="width: 156px">
		    		 <option value="">Please Select</option>
		    		 <c:forEach items="${model.taskTypes}" var="type" >
		    		   <option value="${type}">${type}</option>
		    		 </c:forEach>
		    		 </select>
		    	  </td>
			      </tr>
	              <tr>
			       <td style="font-size: 7px;"><fmt:message key="assign" />:</td>
			       <td>
			         <select name="task_assignedTo" style="width: 156px"> 
			           <option value="-1">Default</option>
			           <option value="0">Admin</option>
			           <c:forEach items="${model.userList}" var="user">
			             <option value="${user.id}">${user.name}</option>
			           </c:forEach>
			         </select>
			       </td>
			      </tr>
			     </table>
	             <div align="center" class="buttonLeft" style="margin-top: -5px; height:10px;">
			        <input type="submit" name="__taskAssignPacher" value="Apply" onClick="return optionsSelected()">
		       	  </div>
	            </td>
	           </tr>
	           </table>
	         </div>
	         <div style="float:left;padding: 5px">
	           <table class="productBatchTool" cellpadding="5" cellspacing="5">
	           <tr>
	            <td valign="top">
	             <table cellpadding="1" cellspacing="1" width="100%">
	              <tr style="height:10px;white-space: nowrap">
	               <td valign="top" width="20px"><img class="toolTipImg" title="Access User ::Assign User to the following contact(s) in the list" src="../graphics/question.gif" /></td>
	               <td valign="top"><h3><fmt:message key="user" /></h3></td>
	               <td valign="top"  align="right"><input type="checkbox" onclick="toggleAll(this)" name="__userAssignAll" value="true" /> <fmt:message key="all"/></td>
	              </tr>
	             </table>
	            </td>
	           </tr>
	           <tr >
	            <td valign="top">
	             <table cellpadding="1" cellspacing="1">
	              <tr>
			       <td style="font-size: 7px;"><fmt:message key="assign" />:</td>
			       <td>
			         <select name="contact_assignedTo" style="width: 156px"> 
			           <option value="0">Admin</option>
			           <c:forEach items="${model.userList}" var="user">
			             <option value="${user.id}">${user.name}</option>
			           </c:forEach>
			         </select>
			       </td>
			      </tr>
			     </table>
	             <div align="center" class="buttonLeft" style="margin-top: -5px; height:10px;">
			        <input type="submit" name="__userAssignPacher" value="Apply" onClick="return optionsSelected()">
		       	  </div>
	            </td>
	           </tr>
	           </table>
	         </div>
	         <!-- Viatrading no need any more 
	         <div style="float:left;padding: 5px">
	           <table class="productBatchTool" cellpadding="5" cellspacing="5">
	           <tr>
	            <td valign="top">
	             <table cellpadding="1" cellspacing="1" width="100%">
	              <tr style="height:10px;white-space: nowrap">
	               <td valign="top" width="20px"><img class="toolTipImg" title="Rating ::Assign Rating to the following contact(s) in the list " src="../graphics/question.gif" /></td>
	               <td valign="top"><h3><fmt:message key="rating" /></h3></td>
	               <td valign="top"  align="right"><input type="checkbox" onclick="toggleAll(this)" name="__ratingAssignAll" value="true" /> <fmt:message key="all"/></td>
	              </tr>
	             </table>
	            </td>
	           </tr>
	           <tr >
	            <td valign="top">
	             <table cellpadding="1" cellspacing="1">
	              <tr>
			       <td style="font-size: 7px;"><fmt:message key="assign" />:</td>
			       <td>
			        <select name="__rating" style="width: 156px">
	         		 <option value="0">Please Select</option>
	  	   				 <c:forTokens items="${siteConfig['CRM_RATING'].value}" delims="," var="rating">
	         				 <option value="${rating}">${rating}</option>
	  	   				 </c:forTokens>
	  	  			 </select>
			       </td>
			      </tr>
			     </table>
	             <div align="center" class="buttonLeft" style="margin-top: -5px; height:10px;">
			        <input type="submit" name="__ratingAssignPacher" value="Apply" onClick="return optionsSelected()">
		       	  </div>
	            </td>
	           </tr>
	           </table>
	         </div>
	         -->
	         <div style="float:left;padding: 5px">
	           <table class="productBatchTool" cellpadding="5" cellspacing="5">
	           <tr>
	            <td valign="top">
	             <table cellpadding="1" cellspacing="1" width="100%">
	              <tr style="height:10px;white-space: nowrap">
	               <td valign="top" width="20px"><img class="toolTipImg" title="Rating1 ::Assign Rating to the following contact(s) in the list " src="../graphics/question.gif" /></td>
	               <td valign="top"><h3><fmt:message key="rating" /> 1</h3></td>
	               <td valign="top"  align="right"><input type="checkbox" onclick="toggleAll(this)" name="__rating1AssignAll" value="true" /> <fmt:message key="all"/></td>
	              </tr>
	             </table>
	            </td>
	           </tr>
	           <tr >
	            <td valign="top">
	             <table cellpadding="1" cellspacing="1">
	              <tr>
			       <td style="font-size: 7px;"><fmt:message key="assign" />:</td>
			       <td>
			        <select name="__rating1" style="width: 156px">
	         		 <option value="">Please Select</option>
	  	   				 <c:forTokens items="${siteConfig['CUSTOMER_RATING_1'].value}" delims="," var="type">
	         				 <option value ="${type}"><c:out value ="${type}" /></option>
	  	   				 </c:forTokens>
	  	  			</select>
			       </td>
			      </tr>
			     </table>
	             <div align="center" class="buttonLeft" style="margin-top: -5px; height:10px;">
			        <input type="submit" name="__rating1AssignPacher" value="Apply" onClick="return optionsSelected()">
		       	 </div>
	            </td>
	           </tr>
	           </table>
	         </div>
	         
	         <div style="float:left;padding: 5px">
	           <table class="productBatchTool" cellpadding="5" cellspacing="5">
	           <tr>
	            <td valign="top">
	             <table cellpadding="1" cellspacing="1" width="100%">
	              <tr style="height:10px;white-space: nowrap">
	               <td valign="top" width="20px"><img class="toolTipImg" title="Rating2 ::Assign Rating to the following contact(s) in the list " src="../graphics/question.gif" /></td>
	               <td valign="top"><h3><fmt:message key="rating" /> 2</h3></td>
	               <td valign="top"  align="right"><input type="checkbox" onclick="toggleAll(this)" name="__rating2AssignAll" value="true" /> <fmt:message key="all"/></td>
	              </tr>
	             </table>
	            </td>
	           </tr>
	           <tr >
	            <td valign="top">
	             <table cellpadding="1" cellspacing="1">
	              <tr>
			       <td style="font-size: 7px;"><fmt:message key="assign" />:</td>
			       <td>
			        <select name="__rating2" style="width: 156px">
	         		 <option value="">Please Select</option>
	  	   				 <c:forTokens items="${siteConfig['CUSTOMER_RATING_2'].value}" delims="," var="type">
	         				 <option value ="${type}"><c:out value ="${type}" /></option>
	  	   				 </c:forTokens>
	  	  			</select>
			       </td>
			      </tr>
			     </table>
	             <div align="center" class="buttonLeft" style="margin-top: -5px; height:10px;">
			        <input type="submit" name="__rating2AssignPacher" value="Apply" onClick="return optionsSelected()">
		       	 </div>
	            </td>
	           </tr>
	           </table>
	         </div>
	         
	         <div style="float:left;padding: 5px">
	           <table class="productBatchTool" cellpadding="5" cellspacing="5">
	           <tr>
	            <td valign="top">
	             <table cellpadding="1" cellspacing="1" width="100%">
	              <tr style="height:10px;white-space: nowrap">
	               <td valign="top" width="20px"><img class="toolTipImg" title="Qualifier ::Assign Qualifier to the following contact(s) in the list " src="../graphics/question.gif" /></td>
	               <td valign="top"><h3><fmt:message key="qualifier" /></h3></td>
	               <td valign="top"  align="right"><input type="checkbox" onclick="toggleAll(this)" name="__qualifierAll" value="true" /> <fmt:message key="all"/></td>
	              </tr>
	             </table>
	            </td>
	           </tr>
	           <tr >
	            <td valign="top">
	             <table cellpadding="1" cellspacing="1">
	              <tr>
			       <td>
			       <select name="__qualifier_field">
	       			<option value="-1"></option>
	      			<option value="-2" <c:if test="${customerSearch.qualifierId == -2}">selected</c:if>><fmt:message key="noQualifier" /></option>
	       			<c:forEach items="${model.activeQualifiers}" var="qualifier">
	  	     			<option value="${qualifier.id}" <c:if test="${customerSearch.qualifierId == qualifier.id}">selected</c:if>><c:out value="${qualifier.name}" /></option>
	       			</c:forEach>
	      			</select>
			       </td>
			      </tr>
			     </table>
	             <div align="center" class="buttonLeft" style="margin-top: -5px; height:10px;">
			        <input type="submit" name="__qualifierAssignPacher" value="Apply" onClick="return optionsSelected()">
		       	 </div>
	            </td>
	           </tr>
	           </table>
	         </div>
	         
	          <div style="float:left;padding: 5px">
	           <table class="productBatchTool" cellpadding="5" cellspacing="5">
	           <tr>
	            <td valign="top">
	             <table cellpadding="1" cellspacing="1" width="100%">
	              <tr style="height:10px;white-space: nowrap">
	               <td valign="top" width="20px"><img class="toolTipImg" title="Language ::Assign Language to the following contact(s) in the list " src="../graphics/question.gif" /></td>
	               <td valign="top"><h3><fmt:message key="language" /></h3></td>
	               <td valign="top"  align="right"><input type="checkbox" onclick="toggleAll(this)" name="__languageAll" value="true" /> <fmt:message key="all"/></td>
	              </tr>
	             </table>
	            </td>
	           </tr>
	           <tr >
	            <td valign="top">
	             <table cellpadding="1" cellspacing="1">
	              <tr>
			       <td>
			       <select name="__language_field">
	       			<option value=""></option>
	       			<option value="en"><fmt:message key="language_en"/></option> 
	      			<c:forEach items="${model.languageCodes}" var="i18n">
	       				<option value="${i18n.languageCode}"><fmt:message key="language_${i18n.languageCode}"/></option>
          			</c:forEach>
	      			</select>
			       </td>
			      </tr>
			     </table>
	             <div align="center" class="buttonLeft" style="margin-top: -5px; height:10px;">
			        <input type="submit" name="__languageAssignPacher" value="Apply" onClick="return optionsSelected()">
		       	 </div>
	            </td>
	           </tr>
	           </table>
	         </div>
	         
	    </div>
	  </c:if>
	    
	  </td><td class="topr_g" ></td></tr>
  <tr><td class="boxmidlrg" >

 <!-- tabs -->
 <div class="tab-wrapper">
	<h4 title="List of affiliates" class="quickMode"></h4>
	<div>
	<!-- start tab -->

	  	<div class="listdivi"></div>
	   
		  	<div class="listlight">
		  	<!-- input field --> 
		  	<table border="0" cellpadding="0" cellspacing="1" width="100%" class="quickMode">
			  <tr>
			  <c:if test="${model.count > 0}">
			  <td class="pageShowing">
			  <fmt:message key="showing">
				<fmt:param value="${crmContactSearch.offset+1}"/>
				<fmt:param value="${model.pageEnd}"/>
				<fmt:param value="${model.count}"/>
			  </fmt:message>
			  </td>
			  </c:if>
			  <td class="pageNavi">
			  Page
			  <select name="page" id="page" onchange="submit()">
			  <c:forEach begin="1" end="${model.pageCount}" var="page">
			  	<option value="${page}" <c:if test="${page == (crmContactSearch.page)}">selected</c:if>>${page}</option>
			  </c:forEach>
			  </select>
			  of <c:out value="${model.pageCount}"/>
			  | 
			  <c:if test="${crmContactSearch.page == 1}"><span class="pageNaviDead">previous</span></c:if>
			  <c:if test="${crmContactSearch.page != 1}"><a href="<c:url value="crmContactList.jhtm"><c:param name="page" value="${crmContactSearch.page-1}"/></c:url>" class="pageNaviLink">previous</a></c:if>
			  | 
			  <c:if test="${crmContactSearch.page == model.pageCount}"><span class="pageNaviDead">next</span></c:if>
			  <c:if test="${crmContactSearch.page != model.pageCount}"><a href="<c:url value="crmContactList.jhtm"><c:param name="page" value="${crmContactSearch.page+1}"/></c:url>" class="pageNaviLink">next</a></c:if>
			  </td>
			  </tr>
			</table>
			
			
			
			<table border="0" cellpadding="0" cellspacing="0" width="100%" class="listings">
			  <tr class="listingsHdr2">
			    <c:if test="${model.count > 0}">
			    <td align="left" class="quickMode"><input type="checkbox" onclick="toggleAll(this)"></td>
			    </c:if>
			    <td class="indexCol">&nbsp;</td>
			    <td class="listingsHdr3">
			      <table cellspacing="0" cellpadding="1">
			    	  <tr>
			    	    <c:choose>
			    	      <c:when test="${crmContactSearch.sort == 'first_name desc'}">
			    	        <td class="listingsHdr3">
			    	        <a class="hdrLink" href="#" onClick="document.getElementById('sort').value='first_name';document.getElementById('list').submit()"><fmt:message key="crmContactName" /></a>
			    	        </td>
			    	        <td class="quickModeRemove"><img src="../graphics/up.gif" border="0"></td>
			    	      </c:when>
			    	      <c:when test="${crmContactSearch.sort == 'first_name'}">
			    	        <td class="listingsHdr3">
			    	        <a class="hdrLink" href="#" onClick="document.getElementById('sort').value='first_name desc';document.getElementById('list').submit()"><fmt:message key="crmContactName" /></a>
			    	        </td>
			    	        <td class="quickModeRemove"><img src="../graphics/down.gif" border="0"></td>
			    	      </c:when>
			    	      <c:otherwise>
			    	        <td colspan="2" class="listingsHdr3">
			    	        <a class="hdrLink" href="#" onClick="document.getElementById('sort').value='first_name desc';document.getElementById('list').submit()"><fmt:message key="crmContactName" /></a>
			    	        </td>
			    	      </c:otherwise>
			    	    </c:choose>  
			    	  </tr>
			      </table>
			    </td>
			    <td class="listingsHdr3">Cust</td>
			    <td class="listingsHdr3">
			      <table cellspacing="0" cellpadding="1">
			    	  <tr>
			    	    <c:choose>
			    	      <c:when test="${crmContactSearch.sort == 'email_1 desc'}">
			    	        <td class="listingsHdr3">
			    	        <a class="hdrLink" href="#" onClick="document.getElementById('sort').value='email_1';document.getElementById('list').submit()"><fmt:message key="email" />1</a>
			    	        </td>
			    	        <td class="quickModeRemove"><img src="../graphics/up.gif" border="0"></td>
			    	      </c:when>
			    	      <c:when test="${crmContactSearch.sort == 'email_1'}">
			    	        <td class="listingsHdr3">
			    	        <a class="hdrLink" href="#" onClick="document.getElementById('sort').value='email_1 desc';document.getElementById('list').submit()"><fmt:message key="email" />1</a>
			    	        </td>
			    	        <td class="quickModeRemove"><img src="../graphics/down.gif" border="0"></td>
			    	      </c:when>
			    	      <c:otherwise>
			    	        <td colspan="2" class="listingsHdr3">
			    	        <a class="hdrLink" href="#" onClick="document.getElementById('sort').value='email_1 desc';document.getElementById('list').submit()"><fmt:message key="email" />1</a>
			    	        </td>
			    	      </c:otherwise>
			    	    </c:choose>  
			    	  </tr>
			      </table>
			    </td>
					<td class="listingsHdr3">
			      <table cellspacing="0" cellpadding="1">
			    	    <tr>
			    	      <c:choose>
			    	        <c:when test="${crmContactSearch.sort == 'crm_contact_field_2 desc'}">
			    	          <td class="listingsHdr3">
			    	            <a class="hdrLink" href="#" onClick="document.getElementById('sort').value='crm_contact_field_2';document.getElementById('list').submit()">Type Bus</a>
			    	          </td>
			    	          <td class="quickModeRemove"><img src="../graphics/up.gif" border="0"></td>
			    	        </c:when>
			    	        <c:when test="${crmContactSearch.sort == 'crm_contact_field_2'}">
			    	          <td class="listingsHdr3">
			    	        	<a class="hdrLink" href="#" onClick="document.getElementById('sort').value='crm_contact_field_2 desc';document.getElementById('list').submit()">Type Bus</a>
			    	          </td>
			    	          <td class="quickModeRemove"><img src="../graphics/down.gif" border="0"></td>
			    	        </c:when>
			    	        <c:otherwise>
			    	          <td colspan="2" class="listingsHdr3">
			    	        	<a class="hdrLink" href="#" onClick="document.getElementById('sort').value='crm_contact_field_2 desc';document.getElementById('list').submit()">Type Bus</a>
			    	          </td>
			    	        </c:otherwise>
			    	      </c:choose>  
			    	    </tr>
			       </table>
			    </td>
					<td class="listingsHdr3">
			      <table cellspacing="0" cellpadding="1">
			    	    <tr>
			    	      <c:choose>
			    	        <c:when test="${crmContactSearch.sort == 'crm_contact_field_8 desc'}">
			    	          <td class="listingsHdr3">
			    	            <a class="hdrLink" href="#" onClick="document.getElementById('sort').value='crm_contact_field_8';document.getElementById('list').submit()">Year Bus</a>
			    	          </td>
			    	          <td class="quickModeRemove"><img src="../graphics/up.gif" border="0"></td>
			    	        </c:when>
			    	        <c:when test="${crmContactSearch.sort == 'crm_contact_field_8'}">
			    	          <td class="listingsHdr3">
			    	        	<a class="hdrLink" href="#" onClick="document.getElementById('sort').value='crm_contact_field_8 desc';document.getElementById('list').submit()">Year Bus</a>
			    	          </td>
			    	          <td class="quickModeRemove"><img src="../graphics/down.gif" border="0"></td>
			    	        </c:when>
			    	        <c:otherwise>
			    	          <td colspan="2" class="listingsHdr3">
			    	        	<a class="hdrLink" href="#" onClick="document.getElementById('sort').value='crm_contact_field_8 desc';document.getElementById('list').submit()">Year Bus</a>
			    	          </td>
			    	        </c:otherwise>
			    	      </c:choose>  
			    	    </tr>
			      </table>
			    </td> 
			    <td class="listingsHdr3">
			      <table cellspacing="0" cellpadding="1">
			    	    <tr>
			    	      <c:choose>
			    	        <c:when test="${crmContactSearch.sort == 'crm_contact_field_14 desc'}">
			    	          <td class="listingsHdr3">
			    	            <a class="hdrLink" href="#" onClick="document.getElementById('sort').value='crm_contact_field_14';document.getElementById('list').submit()">Look 2 Purchase</a>
			    	          </td>
			    	          <td class="quickModeRemove"><img src="../graphics/up.gif" border="0"></td>
			    	        </c:when>
			    	        <c:when test="${crmContactSearch.sort == 'crm_contact_field_14'}">
			    	          <td class="listingsHdr3">
			    	        	<a class="hdrLink" href="#" onClick="document.getElementById('sort').value='crm_contact_field_14 desc';document.getElementById('list').submit()">Look 2 Purchase</a>
			    	          </td>
			    	          <td class="quickModeRemove"><img src="../graphics/down.gif" border="0"></td>
			    	        </c:when>
			    	        <c:otherwise>
			    	          <td colspan="2" class="listingsHdr3">
			    	        	<a class="hdrLink" href="#" onClick="document.getElementById('sort').value='crm_contact_field_14 desc';document.getElementById('list').submit()">Look 2 Purchase</a>
			    	          </td>
			    	        </c:otherwise>
			    	      </c:choose>  
			    	    </tr>
			      </table>
			    </td>
			    <td class="listingsHdr3">
			      <table cellspacing="0" cellpadding="1">
			    	    <tr>
			    	      <c:choose>
			    	        <c:when test="${crmContactSearch.sort == 'crm_contact_field_26 desc'}">
			    	          <td class="listingsHdr3">
			    	            <a class="hdrLink" href="#" onClick="document.getElementById('sort').value='crm_contact_field_26';document.getElementById('list').submit()">Comun VIA</a>
			    	          </td>
			    	          <td class="quickModeRemove"><img src="../graphics/up.gif" border="0"></td>
			    	        </c:when>
			    	        <c:when test="${crmContactSearch.sort == 'crm_contact_field_26'}">
			    	          <td class="listingsHdr3">
			    	        	<a class="hdrLink" href="#" onClick="document.getElementById('sort').value='crm_contact_field_26 desc';document.getElementById('list').submit()">Comun VIA</a>
			    	          </td>
			    	          <td class="quickModeRemove"><img src="../graphics/down.gif" border="0"></td>
			    	        </c:when>
			    	        <c:otherwise>
			    	          <td colspan="2" class="listingsHdr3">
			    	        	<a class="hdrLink" href="#" onClick="document.getElementById('sort').value='crm_contact_field_26 desc';document.getElementById('list').submit()">Comun VIA</a>
			    	          </td>
			    	        </c:otherwise>
			    	      </c:choose>  
			    	    </tr>
			      </table>
			    </td>
			    <td class="listingsHdr3">
			      <table cellspacing="0" cellpadding="1">
			    	  <tr>
			    	    <c:choose>
			    	      <c:when test="${crmContactSearch.sort == 'account_name desc'}">
			    	        <td class="listingsHdr3">
			    	        <a class="hdrLink" href="#" onClick="document.getElementById('sort').value='account_name';document.getElementById('list').submit()"><fmt:message key="crmAccountName" /></a>
			    	        </td>
			    	        <td class="quickModeRemove"><img src="../graphics/up.gif" border="0"></td>
			    	      </c:when>
			    	      <c:when test="${crmContactSearch.sort == 'account_name'}">
			    	        <td class="listingsHdr3">
			    	        <a class="hdrLink" href="#" onClick="document.getElementById('sort').value='account_name desc';document.getElementById('list').submit()"><fmt:message key="crmAccountName" /></a>
			    	        </td>
			    	        <td class="quickModeRemove"><img src="../graphics/down.gif" border="0"></td>
			    	      </c:when>
			    	      <c:otherwise>
			    	        <td colspan="2" class="listingsHdr3">
			    	        <a class="hdrLink" href="#" onClick="document.getElementById('sort').value='account_name desc';document.getElementById('list').submit()"><fmt:message key="crmAccountName" /></a>
			    	        </td>
			    	      </c:otherwise>
			    	    </c:choose>  
			    	  </tr>
			      </table>
			    </td>
			    <td class="listingsHdr3">
			      <table cellspacing="0" cellpadding="1">
			    	  <tr>
			    	    <c:choose>
			    	      <c:when test="${crmContactSearch.sort == 'rating1 desc'}">
			    	        <td class="listingsHdr3">
			    	        <a class="hdrLink" href="#" onClick="document.getElementById('sort').value='rating1';document.getElementById('list').submit()"><fmt:message key="rating" /> 1</a>
			    	        </td>
			    	        <td class="quickModeRemove"><img src="../graphics/up.gif" border="0"></td>
			    	      </c:when>
			    	      <c:when test="${crmContactSearch.sort == 'rating1'}">
			    	        <td class="listingsHdr3">
			    	        <a class="hdrLink" href="#" onClick="document.getElementById('sort').value='rating1 desc';document.getElementById('list').submit()"><fmt:message key="rating" /> 1</a>
			    	        </td>
			    	        <td class="quickModeRemove"><img src="../graphics/down.gif" border="0"></td>
			    	      </c:when>
			    	      <c:otherwise>
			    	        <td colspan="2" class="listingsHdr3">
			    	        <a class="hdrLink" href="#" onClick="document.getElementById('sort').value='rating1 desc';document.getElementById('list').submit()"><fmt:message key="rating" /> 1</a>
			    	        </td>
			    	      </c:otherwise>
			    	    </c:choose>  
			    	  </tr>
			      </table>
			    </td>
			    <td class="listingsHdr3">
			      <table cellspacing="0" cellpadding="1">
			    	  <tr>
			    	    <c:choose>
			    	      <c:when test="${crmContactSearch.sort == 'rating2 desc'}">
			    	        <td class="listingsHdr3">
			    	        <a class="hdrLink" href="#" onClick="document.getElementById('sort').value='rating2';document.getElementById('list').submit()"><fmt:message key="rating" /> 2</a>
			    	        </td>
			    	        <td class="quickModeRemove"><img src="../graphics/up.gif" border="0"></td>
			    	      </c:when>
			    	      <c:when test="${crmContactSearch.sort == 'rating2'}">
			    	        <td class="listingsHdr3">
			    	        <a class="hdrLink" href="#" onClick="document.getElementById('sort').value='rating2 desc';document.getElementById('list').submit()"><fmt:message key="rating" /> 2</a>
			    	        </td>
			    	        <td class="quickModeRemove"><img src="../graphics/down.gif" border="0"></td>
			    	      </c:when>
			    	      <c:otherwise>
			    	        <td colspan="2" class="listingsHdr3">
			    	        <a class="hdrLink" href="#" onClick="document.getElementById('sort').value='rating2 desc';document.getElementById('list').submit()"><fmt:message key="rating" /> 2</a>
			    	        </td>
			    	      </c:otherwise>
			    	    </c:choose>  
			    	  </tr>
			      </table>
			    </td>
			    <td class="listingsHdr3">
			      <table cellspacing="0" cellpadding="1">
			    	  <tr>
			    	    <c:choose>
			    	      <c:when test="${crmContactSearch.sort == 'created desc'}">
			    	        <td class="listingsHdr3">
			    	        <a class="hdrLink" href="#" onClick="document.getElementById('sort').value='created';document.getElementById('list').submit()"><fmt:message key="crmContactCreated" /></a>
			    	        </td>
			    	        <td class="quickModeRemove"><img src="../graphics/up.gif" border="0"></td>
			    	      </c:when>
			    	      <c:when test="${crmContactSearch.sort == 'created'}">
			    	        <td class="listingsHdr3">
			    	        <a class="hdrLink" href="#" onClick="document.getElementById('sort').value='created desc';document.getElementById('list').submit()"><fmt:message key="crmContactCreated" /></a>
			    	        </td>
			    	        <td class="quickModeRemove"><img src="../graphics/down.gif" border="0"></td>
			    	      </c:when>
			    	      <c:otherwise>
			    	        <td colspan="2" class="listingsHdr3">
			    	        <a class="hdrLink" href="#" onClick="document.getElementById('sort').value='created desc';document.getElementById('list').submit()"><fmt:message key="crmContactCreated" /></a>
			    	        </td>
			    	      </c:otherwise>
			    	    </c:choose>  
			    	  </tr>
			      </table>
			    </td>
			    <td class="listingsHdr3">
			      <table cellspacing="0" cellpadding="1">
			    	  <tr>
			    	    <c:choose>
			    	      <c:when test="${crmContactSearch.sort == 'lead_source desc'}">
			    	        <td class="listingsHdr3">
			    	        <a class="hdrLink" href="#" onClick="document.getElementById('sort').value='lead_source';document.getElementById('list').submit()"><fmt:message key="leadSource" /></a>
			    	        </td>
			    	        <td class="quickModeRemove"><img src="../graphics/up.gif" border="0"></td>
			    	      </c:when>
			    	      <c:when test="${crmContactSearch.sort == 'lead_source'}">
			    	        <td class="listingsHdr3">
			    	        <a class="hdrLink" href="#" onClick="document.getElementById('sort').value='lead_source desc';document.getElementById('list').submit()"><fmt:message key="leadSource" /></a>
			    	        </td>
			    	        <td class="quickModeRemove"><img src="../graphics/down.gif" border="0"></td>
			    	      </c:when>
			    	      <c:otherwise>
			    	        <td colspan="2" class="listingsHdr3">
			    	        <a class="hdrLink" href="#" onClick="document.getElementById('sort').value='lead_source desc';document.getElementById('list').submit()"><fmt:message key="leadSource" /></a>
			    	        </td>
			    	      </c:otherwise>
			    	    </c:choose>  
			    	  </tr>
			      </table>
			    </td>
			    <td class="listingsHdr3">
			      <table cellspacing="0" cellpadding="1">
			    	  <tr>
			    	    <c:choose>
			    	      <c:when test="${crmContactSearch.sort == 'city desc'}">
			    	        <td class="listingsHdr3">
			    	        <a class="hdrLink" href="#" onClick="document.getElementById('sort').value='city';document.getElementById('list').submit()"><fmt:message key="city" /></a>
			    	        </td>
			    	        <td class="quickModeRemove"><img src="../graphics/up.gif" border="0"></td>
			    	      </c:when>
			    	      <c:when test="${crmContactSearch.sort == 'city'}">
			    	        <td class="listingsHdr3">
			    	        <a class="hdrLink" href="#" onClick="document.getElementById('sort').value='city desc';document.getElementById('list').submit()"><fmt:message key="city" /></a>
			    	        </td>
			    	        <td class="quickModeRemove"><img src="../graphics/down.gif" border="0"></td>
			    	      </c:when>
			    	      <c:otherwise>
			    	        <td colspan="2" class="listingsHdr3">
			    	        <a class="hdrLink" href="#" onClick="document.getElementById('sort').value='city desc';document.getElementById('list').submit()"><fmt:message key="city" /></a>
			    	        </td>
			    	      </c:otherwise>
			    	    </c:choose>  
			    	  </tr>
			      </table>
			    </td>
			    <td class="listingsHdr3"><fmt:message key="phone" /></td>
			    <td class="listingsHdr3">
			      <table cellspacing="0" cellpadding="1">
			    	  <tr>
			    	    <c:choose>
			    	      <c:when test="${crmContactSearch.sort == 'trackcode desc'}">
			    	        <td class="listingsHdr3">
			    	        <a class="hdrLink" href="#" onClick="document.getElementById('sort').value='trackcode';document.getElementById('list').submit()"><fmt:message key="trackcode" /></a>
			    	        </td>
			    	        <td class="quickModeRemove"><img src="../graphics/up.gif" border="0"></td>
			    	      </c:when>
			    	      <c:when test="${crmContactSearch.sort == 'trackcode'}">
			    	        <td class="listingsHdr3">
			    	        <a class="hdrLink" href="#" onClick="document.getElementById('sort').value='trackcode desc';document.getElementById('list').submit()"><fmt:message key="trackcode" /></a>
			    	        </td>
			    	        <td class="quickModeRemove"><img src="../graphics/down.gif" border="0"></td>
			    	      </c:when>
			    	      <c:otherwise>
			    	        <td colspan="2" class="listingsHdr3">
			    	        <a class="hdrLink" href="#" onClick="document.getElementById('sort').value='trackcode desc';document.getElementById('list').submit()"><fmt:message key="trackcode" /></a>
			    	        </td>
			    	      </c:otherwise>
			    	    </c:choose>  
			    	  </tr>
			      </table>
			    </td>
			    <td class="listingsHdr3">
			      <table cellspacing="0" cellpadding="1">
			    	  <tr>
			    	    <c:choose>
			    	      <c:when test="${crmContactSearch.sort == 'qualifier desc'}">
			    	        <td class="listingsHdr3">
			    	        <a class="hdrLink" href="#" onClick="document.getElementById('sort').value='qualifier';document.getElementById('list').submit()"><fmt:message key="qualifier" /></a>
			    	        </td>
			    	        <td class="quickModeRemove"><img src="../graphics/up.gif" border="0"></td>
			    	      </c:when>
			    	      <c:when test="${crmContactSearch.sort == 'qualifier'}">
			    	        <td class="listingsHdr3">
			    	        <a class="hdrLink" href="#" onClick="document.getElementById('sort').value='qualifier desc';document.getElementById('list').submit()"><fmt:message key="qualifier" /></a>
			    	        </td>
			    	        <td class="quickModeRemove"><img src="../graphics/down.gif" border="0"></td>
			    	      </c:when>
			    	      <c:otherwise>
			    	        <td colspan="2" class="listingsHdr3">
			    	        <a class="hdrLink" href="#" onClick="document.getElementById('sort').value='qualifier desc';document.getElementById('list').submit()"><fmt:message key="qualifier" /></a>
			    	        </td>
			    	      </c:otherwise>
			    	    </c:choose>  
			    	  </tr>
			      </table>
			    </td>
			    <td class="listingsHdr3">
			      <table cellspacing="0" cellpadding="1">
			    	  <tr>
			    	    <c:choose>
			    	      <c:when test="${crmContactSearch.sort == 'access_user_id desc'}">
			    	        <td class="listingsHdr3">
			    	        <a class="hdrLink" href="#" onClick="document.getElementById('sort').value='access_user_id';document.getElementById('list').submit()"><fmt:message key="user" /></a>
			    	        </td>
			    	        <td class="quickModeRemove"><img src="../graphics/up.gif" border="0"></td>
			    	      </c:when>
			    	      <c:when test="${crmContactSearch.sort == 'access_user_id'}">
			    	        <td class="listingsHdr3">
			    	        <a class="hdrLink" href="#" onClick="document.getElementById('sort').value='access_user_id desc';document.getElementById('list').submit()"><fmt:message key="user" /></a>
			    	        </td>
			    	        <td class="quickModeRemove"><img src="../graphics/down.gif" border="0"></td>
			    	      </c:when>
			    	      <c:otherwise>
			    	        <td colspan="2" class="listingsHdr3">
			    	        <a class="hdrLink" href="#" onClick="document.getElementById('sort').value='access_user_id desc';document.getElementById('list').submit()"><fmt:message key="user" /></a>
			    	        </td>
			    	      </c:otherwise>
			    	    </c:choose>  
			    	  </tr>
			      </table>
			    </td>
			   <td class="listingsHdr3">
			      <table cellspacing="0" cellpadding="1">
			    	  <tr>
			    	    <c:choose>
			    	      <c:when test="${crmContactSearch.sort == 'language desc'}">
			    	        <td class="listingsHdr3">
			    	        <a class="hdrLink" href="#" onClick="document.getElementById('sort').value='language';document.getElementById('list').submit()"><fmt:message key="language" /></a>
			    	        </td>
			    	        <td class="quickModeRemove"><img src="../graphics/up.gif" border="0"></td>
			    	      </c:when>
			    	      <c:when test="${crmContactSearch.sort == 'language'}">
			    	        <td class="listingsHdr3">
			    	        <a class="hdrLink" href="#" onClick="document.getElementById('sort').value='language desc';document.getElementById('list').submit()"><fmt:message key="language" /></a>
			    	        </td>
			    	        <td class="quickModeRemove"><img src="../graphics/down.gif" border="0"></td>
			    	      </c:when>
			    	      <c:otherwise>
			    	        <td colspan="2" class="listingsHdr3">
			    	        <a class="hdrLink" href="#" onClick="document.getElementById('sort').value='language desc';document.getElementById('list').submit()"><fmt:message key="language" /></a>
			    	        </td>
			    	      </c:otherwise>
			    	    </c:choose>  
			    	  </tr>
			      </table>
			    </td>
			   <td class="listingsHdr3"><fmt:message key="description" /></td>
			   <td class="listingsHdr3">
			      <table cellspacing="0" cellpadding="1">
			    	  <tr>
			    	    <c:choose>
			    	      <c:when test="${crmContactSearch.sort == 'last_note desc'}">
			    	        <td class="listingsHdr3">
			    	        <a class="hdrLink" href="#" onClick="document.getElementById('sort').value='last_note';document.getElementById('list').submit()"><fmt:message key="dialingNote" /></a>
			    	        </td>
			    	        <td class="quickModeRemove"><img src="../graphics/up.gif" border="0"></td>
			    	      </c:when>
			    	      <c:when test="${crmContactSearch.sort == 'last_note'}">
			    	        <td class="listingsHdr3">
			    	        <a class="hdrLink" href="#" onClick="document.getElementById('sort').value='last_note desc';document.getElementById('list').submit()"><fmt:message key="dialingNote" /></a>
			    	        </td>
			    	        <td class="quickModeRemove"><img src="../graphics/down.gif" border="0"></td>
			    	      </c:when>
			    	      <c:otherwise>
			    	        <td colspan="2" class="listingsHdr3">
			    	        <a class="hdrLink" href="#" onClick="document.getElementById('sort').value='last_note desc';document.getElementById('list').submit()"><fmt:message key="dialingNote" /></a>
			    	        </td>
			    	      </c:otherwise>
			    	    </c:choose>  
			    	  </tr>
			      </table>
			    </td>
			  </tr>
			<c:forEach items="${model.crmContactList}" var="contact" varStatus="status">
			  <tr class="row${status.index % 2}" onmouseover="changeStyleClass(this,'rowOver')" onmouseout="changeStyleClass(this,'row${status.index % 2}')">
			    <td align="left" class="quickMode"><input name="__selected_id" value="${contact.id}" type="checkbox"></td>
			    <td align="center">
			    <img class="action" width="23" height="16" alt="Actions" src="../graphics/actions.png"/>
			    <div class="action-hover" style="display: none;">
			        <ul class="round">
			            <li class="action-header">
					      <div class="name"><c:out value="${contact.firstName}"/>&nbsp;<c:out value="${contact.lastName}"/></div>
					      <div class="menudiv"></div>
					      <div style="position:relative">
					        <span style="width:16px;float:left;"><a href="crmContactQuickView.jhtm?id=${contact.id}" rel="width:900,height:400" id="mb${contact.id}" class="mbContact" title="<fmt:message key="crmContact" />ID : ${contact.id}"><img src="../graphics/magnifierCrm.gif" border="0" /></a></span>
					        <c:if test="${contact.userId != null}">
					        <span style="width:16px;float:left;"><a href="../customers/customerQuickView.jhtm?cid=${contact.userId}" rel="width:900,height:400" id="mb${contact.userId}" class="mbContact" title="<fmt:message key="customer" />ID : ${contact.userId}"><img src="../graphics/magnifier.gif" border="0" /></a></span>
					        </c:if>     
					      </div>  
			            </li>
			            <sec:authorize ifAnyGranted="ROLE_AEM,ROLE_ADMIN_${prop_site_id},ROLE_CRM">
				          <li class="link"><a href="crmContactForm.jhtm?id=${contact.id}&accountId=${contact.accountId}"><fmt:message key="edit" /></a></li>
				          <li class="link"><a href="email.jhtm?contactId=${contact.id}"><fmt:message key="sendEmail" /></a></li>
				          <li class="link"><a href="crmTaskForm.jhtm?accountId=${contact.accountId}&contactId=${contact.id}"><fmt:message key="addTask" /></a></li>
				          <li class="link"><a href="crmTaskList.jhtm?contactId=${contact.id}"><fmt:message key="viewTask" /></a></li>
				        </sec:authorize>
			        </ul>
			    </div>
				</td>
			    <td class="nameCol"><a href="crmContactForm.jhtm?id=${contact.id}&accountId=${contact.accountId}"><c:out value="${contact.firstName}"/> <c:out value="${contact.lastName}"/></a></td>
			    <td class="nameCol">
			      <c:choose>
			        <c:when test="${contact.userId != null}"><img src="../graphics/checkbox.png" border="0" class="quickMode"></c:when>
			        <c:when test="${contact.userId == null and contact.email1 != null and contact.email1 != '' and contact.verified and contact.accountName != 'Leads'}"><a href="../customers/addCustomer.jhtm?contactId=${contact.id}"><img src="../graphics/box.png" border="0"></a></c:when>
			        <c:otherwise><img src="../graphics/box.png" border="0" class="quickMode"></c:otherwise>
			      </c:choose>
			    </td>
				  <td class="nameCol" class="quickMode"
				  <c:if test="${contact.duplicateContact == 1}">
				  style="color: red;"
				  </c:if>
				  >
				  <c:out value="${contact.email1}"/>
				  </td>
				  
				  <td class="nameCol"><c:out value="${contact.crmContactField2}"/></td>
			      <td class="nameCol"><c:out value="${contact.crmContactField8}"/></td>
				  <td class="nameCol"><c:out value="${contact.crmContactField14}"/></td>
				  <td class="nameCol"><c:out value="${contact.crmContactField26}"/></td>
				  <td class="nameCol">
			      <c:choose>
			      <c:when test="${contact.accountId > 1}">
			        <a href="crmAccountForm.jhtm?id=${contact.accountId}"><c:out value="${contact.accountName}"/></a>
			      </c:when>
			      <c:otherwise>
			        <c:out value="${contact.accountName}"/>
			      </c:otherwise>
			      </c:choose>
			    </td>
			    <td class="nameCol"><c:out value="${contact.rating1}"/></td>
			    <td class="nameCol"><c:out value="${contact.rating2}"/></td>
			    <td class="nameCol"><fmt:formatDate type="date" timeStyle="default" value="${contact.created}"/></td>
			    <td class="nameCol"><c:out value="${contact.leadSource}"/></td>
			    <td class="nameCol"><c:out value="${contact.city}"/></td>
			    <td class="nameCol"><c:out value="${contact.phone1}"/></td>
			    <td class="nameCol"><c:out value="${contact.trackcode}"/></td>
			    <td align="center"><c:out value="${model.qualifierRepMap[contact.qualifier].name}"/></td>
			    <td class="nameCol"><c:out value="${model.accessUserMap[contact.accessUserId].name}"/></td>
			    <td class="nameCol"><c:out value="${contact.language}"/></td>
			    <td class="nameCol"><c:out value="${contact.descriptionTrimmed}"/></td>
			    <td class="nameCol">
			    <c:choose>
			    <c:when test="${contact.dialingNoteHistoryString != null}">
			    	<div class="testHover">
			    		<c:set value="${fn:length(fn:split(contact.dialingNoteHistoryString, '||'))}" var="len"/>
	 			    	<c:out value="${fn:split(contact.dialingNoteHistoryString, '||')[len-1]}"/>
	 			    </div>
				    <div class="action-hover" style="display: none;">
				        <ul class="round">
				        	<c:forEach items="${fn:split(contact.dialingNoteHistoryString, '||')}" var="note">
				            <li class="action-header">
						      	<c:out value="${note}"/>
				            </li>
				            </c:forEach>
				            <li>
				            	<a href="#${contact.id}" rel="type:element" id="mbNote${contact.id}" class="mbNote"><img border="0" src="../graphics/edit.gif" align="center"/> <b>Add New Note</b> 
				            	<div id="${contact.id}" class="miniWindowWrapper inventoryAdj displayNone quickMode">
			  					<div class="header"><fmt:message key="add"/> New Note</div>
				    	  		<fieldset class="top">
				    	    		<label class="AStitle"><fmt:message key="note"/></label>
				    	    		<textarea name="__note_${contact.id}" id="__note_${contact.id}" rows="5" cols="60" onkeyup="assignValue(this.value, '__note_', ${contact.id});"></textarea>
				    	    	</fieldset>
				            	<fieldset>
			    	     		<div class="button">
			      					<input type="submit" value="Add"  name="__add_" onclick="addNewNote(${contact.id});"/>
			      				</div>
			    	  			</fieldset>
				            </li>
				        </ul>
				    </div>
			    </c:when>
			    <c:otherwise>
			    	<div class="testHover"><img border="0" src="../graphics/edit.gif" align="center"/></div>
			    	<div class="action-hover" style="display: none;">
				        <ul class="round">
				        	<c:forEach items="${fn:split(contact.dialingNoteHistoryString, '||')}" var="note">
				            <li class="action-header">
						      	<c:out value="${note}"/>
				            </li>
				            </c:forEach>
				            <li>
				            	<a href="#${contact.id}" rel="type:element" id="mbNote${contact.id}" class="mbNote"><img border="0" src="../graphics/edit.gif" align="center"/> <b>Add New Note</b> 
				            	<div id="${contact.id}" class="miniWindowWrapper inventoryAdj displayNone quickMode">
			  					<div class="header"><fmt:message key="add"/> New Note</div>
				    	  		<fieldset class="top">
				    	    		<label class="AStitle"><fmt:message key="note"/></label>
				    	    		<textarea name="__note_${contact.id}" id="__note_${contact.id}" rows="5" cols="60" onkeyup="assignValue(this.value, '__note_', ${contact.id});"></textarea>
				    	    	</fieldset>
				            	<fieldset>
			    	     		<div class="button">
			      					<input type="submit" value="Add"  name="__add_" onclick="addNewNote(${contact.id});"/>
			      				</div>
			    	  			</fieldset>
				            </li>
				        </ul>
				    </div>
			    </c:otherwise>
			    </c:choose>
			    </td>
			  </tr>
			</c:forEach>
			<c:if test="${model.count == 0}">
			  <tr class="emptyList"><td colspan="3">&nbsp;</td></tr>
			</c:if>
			</table>
			
			<table border="0" cellpadding="0" cellspacing="1" width="100%" class="quickMode">
			  <tr>
			  <td class="pageSize">
			  <select name="size" onchange="document.getElementById('page').value=1;submit()">
			  <c:forTokens items="10,25,50,100,200,500" delims="," var="current">
			  	  <option value="${current}" <c:if test="${current == crmContactSearch.pageSize}">selected</c:if>>${current} <fmt:message key="perPage" /></option>
			  </c:forTokens>
			  </select>
			  </td>
			  </tr>
			</table>
		  	<!-- end input field -->  	  
		  	
		  	<!-- start button --> 
		  	<div class="quickMode">
		  	<div align="left" class="button">
		  	  <sec:authorize ifAnyGranted="ROLE_AEM,ROLE_ADMIN_${prop_site_id},ROLE_CRM">
		  	    <input type="submit" name="__add" value="<fmt:message key="add" />">
		  	    <input type="submit" name="__delete" value="<fmt:message key="delete" />" onClick="return confirm('Delete permanently? Contacts converted to customer will not be deleted.')">
		  	  	<input type="submit" name="__unassign_user" value="<fmt:message key="unassignUser" />">
		  	  </sec:authorize>
			</div>
			</div>
			<!-- end button -->
		  	</div>
	
	<!-- end tab -->        
	</div>
<!-- end tabs -->			
</div>
  <!-- end table -->
   </td><td class="boxmidr" ></td></tr>
  <tr><td class="botl"></td><td class="botr"></td></tr>
  </table>
  
<!-- end main box -->  
</div> 
</form> 
</sec:authorize>

  </tiles:putAttribute>    
</tiles:insertDefinition>