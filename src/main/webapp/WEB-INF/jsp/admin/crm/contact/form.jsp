<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ taglib uri="/WEB-INF/tlds/spring.tld" prefix="spring" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ taglib uri="http://www.springframework.org/security/tags" prefix="sec" %>
<script type="text/javascript" src="../javascript/autocompleter1.2.js"></script>
<script type="text/javascript" src="../javascript/autocompleter.Request1.2.js"></script>
<script type="text/javascript" src="../javascript/observer.js"></script>

<tiles:insertDefinition name="admin.crm.contact" flush="true">
  <tiles:putAttribute name="content" type="string">
<sec:authorize ifAnyGranted="ROLE_AEM,ROLE_ADMIN_${prop_site_id},ROLE_CRM">
<script language="JavaScript"> 
<!--
window.addEvent('domready', function(){			
	var tabs1 = new SimpleTabs($('tab-block-1'), { selector: 'h4'});
	var Tips1 = new Tips($$('.toolTipImg'));

	var formAccountName = $('formAccountName');
	new Autocompleter.Request.HTML(formAccountName, '../crm/ajax-showCrmAccounts.jhtm', {
		'indicatorClass': 'autocompleter-loading',
		'postData': { 'search': 'crmAccountName' },
		'injectChoice': function(choice) {
			var text = choice.getFirst();
			var value = text.innerHTML;
			choice.inputValue = value;
			text.set('html', this.markQueryValue(value));
			this.addChoiceEvents(choice);
		}
	});
});
//--> 
</script>
<script type="text/javascript">

var zChar = new Array(' ', '(', ')', '-', '.');
var maxphonelength = 13;
var phonevalue1;
var phonevalue2;
var cursorposition;

function ParseForNumber1(object){
phonevalue1 = ParseChar(object.value, zChar);
}
function ParseForNumber2(object){
phonevalue2 = ParseChar(object.value, zChar);
}

function backspacerUP(object,e) { 
if(e){ 
e = e 
} else {
e = window.event 
} 
if(e.which){ 
var keycode = e.which 
} else {
var keycode = e.keyCode 
}

ParseForNumber1(object)

if(keycode >= 48){
ValidatePhone(object)
}
}

function backspacerDOWN(object,e) { 
if(e){ 
e = e 
} else {
e = window.event 
} 
if(e.which){ 
var keycode = e.which 
} else {
var keycode = e.keyCode 
}
ParseForNumber2(object)
} 

function GetCursorPosition(){

var t1 = phonevalue1;
var t2 = phonevalue2;
var bool = false
for (i=0; i<t1.length; i++)
{
if (t1.substring(i,1) != t2.substring(i,1)) {
if(!bool) {
cursorposition=i
bool=true
}
}
}
}

function ValidatePhone(object){

var p = phonevalue1

p = p.replace(/[^\d]*/gi,"")

if (p.length < 3) {
object.value=p
} else if(p.length==3){
pp=p;
d4=p.indexOf('(')
d5=p.indexOf(')')
if(d4==-1){
pp="("+pp;
}
if(d5==-1){
pp=pp+")";
}
object.value = pp;
} else if(p.length>3 && p.length < 7){
p ="(" + p; 
l30=p.length;
p30=p.substring(0,4);
p30=p30+")"

p31=p.substring(4,l30);
pp=p30+p31;

object.value = pp; 

} else if(p.length >= 7){
p ="(" + p; 
l30=p.length;
p30=p.substring(0,4);
p30=p30+")"

p31=p.substring(4,l30);
pp=p30+p31;

l40 = pp.length;
p40 = pp.substring(0,8);
p40 = p40 + "-"

p41 = pp.substring(8,l40);
ppp = p40 + p41;

object.value = ppp.substring(0, maxphonelength);
}

GetCursorPosition()

if(cursorposition >= 0){
if (cursorposition == 0) {
cursorposition = 2
} else if (cursorposition <= 2) {
cursorposition = cursorposition + 1
} else if (cursorposition <= 5) {
cursorposition = cursorposition + 2
} else if (cursorposition == 6) {
cursorposition = cursorposition + 2
} else if (cursorposition == 7) {
cursorposition = cursorposition + 4
e1=object.value.indexOf(')')
e2=object.value.indexOf('-')
if (e1>-1 && e2>-1){
if (e2-e1 == 4) {
cursorposition = cursorposition - 1
}
}
} else if (cursorposition < 11) {
cursorposition = cursorposition + 3
} else if (cursorposition == 11) {
cursorposition = cursorposition + 1
} else if (cursorposition >= 12) {
cursorposition = cursorposition
}

var txtRange = object.createTextRange();
txtRange.moveStart( "character", cursorposition);
txtRange.moveEnd( "character", cursorposition - object.value.length);
txtRange.select();
}

}

function ParseChar(sStr, sChar)
{
if (sChar.length == null) 
{
zChar = new Array(sChar);
}
else zChar = sChar;

for (i=0; i<zChar.length; i++)
{
sNewStr = "";

var iStart = 0;
var iEnd = sStr.indexOf(sChar[i]);

while (iEnd != -1)
{
sNewStr += sStr.substring(iStart, iEnd);
iStart = iEnd + 1;
iEnd = sStr.indexOf(sChar[i], iStart);
}
sNewStr += sStr.substring(sStr.lastIndexOf(sChar[i]) + 1, sStr.length);

sStr = sNewStr;
}

return sNewStr;
}
</script> 
<form:form commandName="crmContactForm" action="crmContactForm.jhtm" method="post">
<form:hidden path="newCrmContact" />

<!-- main box -->
  <div id="mboxfull">
  
  <!-- start table -->
  <table cellpadding="0" cellspacing="0" border="0" class="module" >
  <tr>
    <td class="topl_g">
	  
	  <!-- breadcrumb -->
	  <p class="breadcrumb">
	    <a href="../crm">CRM</a> &gt;
	    <a href="../crm/crmAccountForm.jhtm?id=${crmContactForm.crmContact.accountId}"><c:out value="${crmContactForm.crmContact.accountName}" /></a> &gt;
	    <c:if test="${crmContactForm.crmContact.id != null}">
	      <a href="../crm/crmContactForm.jhtm?id=${crmContactForm.crmContact.id}"><c:out value="${crmContactForm.crmContact.contactName}" /></a> &gt;
	    </c:if>
	    <a href="../crm/crmContactList.jhtm">Contacts</a> &gt;
	    form 
	  </p>
	  
	  <!-- Error Message -->
	  <%--<c:if test="${message != null}">
  		<div class="message"><fmt:message key="${message}"/></div>
  	  </c:if> --%>
  	  <spring:hasBindErrors name="crmContactForm">
		<div class="message">Please fix all errors!</div>
	  </spring:hasBindErrors>

  </td><td class="topr_g" ></td></tr>
  <tr><td class="boxmidlrg" >

 <!-- tabs -->
 <div id="tab-block-1">
	<h4 title="Account Information">Contact Information</h4>
	<div>
	<!-- start tab -->

         <c:set var="classIndex" value="0" />
		<div class="listdivi ln tabdivi"></div>
		
		 <div class="list${classIndex % 2}"><c:set var="classIndex" value="${classIndex+1}" />
		 <div class="listfl"><fmt:message key="createdBy" />:</div>
		 <div class="listp">
		 <!-- input field -->
		 		<span class="static"><c:out value="${crmContactForm.crmContact.createdBy}" /></span>
	 	 <!-- end input field -->	
	 	 </div>
		 </div>
		 
		 <div class="list${classIndex % 2}"><c:set var="classIndex" value="${classIndex+1}" />
		 <div class="listfl"><div class="requiredField"><fmt:message key="crmAccountName" />:</div></div>
		 <div class="listp">
		 <!-- input field -->
		 		<form:input path="crmContact.accountName" cssClass="textfield" maxlength="255" size="60" id="formAccountName" htmlEscape="true"/>
		  		<form:errors path="crmContact.accountName" cssClass="error"/>
	 	 <!-- end input field -->	
	 	 </div>
		 </div>
		 
		 <div class="list${classIndex % 2}"><c:set var="classIndex" value="${classIndex+1}" />
		 <div class="listfl"><div class="helpImg"><img class="toolTipImg" title="Assigned To::Assign this account to a user. To manage users goto: More-->Access Privilege." src="../graphics/question.gif" /></div><fmt:message key="assignedTo" />:</div>
		 <div class="listp">
		 <!-- input field -->
	    		<form:select path="crmContact.accessUserId">
	    		    <form:option value="" label="Select"></form:option>
	    		  <c:forEach items="${accessUsers}" var="accessUser">
	    		    <form:option value="${accessUser.id}" label="${accessUser.username}"></form:option>
	    		  </c:forEach>
	    		</form:select>
	     <!-- end input field -->	
	 	 </div>
		 </div>
		 
		 <div class="list${classIndex % 2}"><c:set var="classIndex" value="${classIndex+1}" />
		 <div class="listfl"><div class="requiredField"><fmt:message key="firstName" />:</div></div>
		 <div class="listp">
		 <!-- input field -->
	    		<form:input path="crmContact.firstName" cssClass="textfield" maxlength="255" size="60" htmlEscape="true"/>
		  		<c:if test="${not crmContactForm.newCrmContact}"><a href="crmTaskForm.jhtm?accountId=${crmContactForm.crmContact.accountId}&contactId=${crmContactForm.crmContact.id}"><fmt:message key="addTask" /></a></c:if>
				<form:errors path="crmContact.firstName" cssClass="error"/>
	 	 <!-- end input field -->	
	 	 </div>
		 </div>
		 
		 <div class="list${classIndex % 2}"><c:set var="classIndex" value="${classIndex+1}" />
		 <div class="listfl"><div class="requiredField"><fmt:message key="lastName" />:</div></div>
		 <div class="listp">
		 <!-- input field -->
	    		<form:input path="crmContact.lastName" cssClass="textfield" maxlength="255" size="60" htmlEscape="true"/>
		  		<form:errors path="crmContact.lastName" cssClass="error"/>
	 	 <!-- end input field -->	
	 	 </div>
		 </div>
		 
		 <div class="list${classIndex % 2}"><c:set var="classIndex" value="${classIndex+1}" />
		 <div class="listfl"><fmt:message key="title" />:</div>
		 <div class="listp">
		 <!-- input field -->
	    		<form:input path="crmContact.title" cssClass="textfield" maxlength="255" size="60" htmlEscape="true"/>
		  		<form:errors path="crmContact.title" cssClass="error"/>
	 	 <!-- end input field -->	
	 	 </div>
		 </div>

		 <div class="list${classIndex % 2}"><c:set var="classIndex" value="${classIndex+1}" />
		 <div class="listfl"><fmt:message key="email" /> 1:</div>
		 <div class="listp">
		 <!-- input field -->
				<form:input path="crmContact.email1" cssClass="textfield" maxlength="255" size="60" htmlEscape="true"/>
		  		<c:if test="${not crmContactForm.newCrmContact}"><a href="email.jhtm?contactId=${crmContactForm.crmContact.id}"><fmt:message key="sendEmail" /></a></c:if>
				<form:errors path="crmContact.email1" cssClass="error"/>
	 	 <!-- end input field -->	
	 	 </div>
		 </div>
		 
		 <div class="list${classIndex % 2}"><c:set var="classIndex" value="${classIndex+1}" />
		 <div class="listfl"><fmt:message key="email" /> 2:</div>
		 <div class="listp">
		 <!-- input field -->
				<form:input path="crmContact.email2" cssClass="textfield" maxlength="255" size="60" htmlEscape="true"/>
		 <!-- end input field -->	
	 	 </div>
		 </div>
		 
		 <div class="list${classIndex % 2}"><c:set var="classIndex" value="${classIndex+1}" />
		 <div class="listfl"><fmt:message key="website" /> :</div>
		 <div class="listp">
		 <!-- input field -->
				<form:input path="crmContact.website" cssClass="textfield" maxlength="255" size="60" htmlEscape="true"/>
		 <!-- end input field -->	
	 	 </div>
		 </div>
		 
         <div class="list${classIndex % 2}"><c:set var="classIndex" value="${classIndex+1}" />
		 <div class="listfl"><div class="helpImg"><img class="toolTipImg" title="Note::The Email address will be excluded from Mass Email Module." src="../graphics/question.gif" /></div><fmt:message key="unsubscribeFromGroupMails" />:</div>
		 <div class="listp">
		 <!-- input field -->
                <form:checkbox path="crmContact.unsubscribe" />
         <!-- end input field -->   	
		 </div>
		 </div>
		 
		 <div class="list${classIndex % 2}"><c:set var="classIndex" value="${classIndex+1}" />
		 <div class="listfl"><div class="helpImg"><img class="toolTipImg" title="Note::The crm contact will be not show on Dialing Export." src="../graphics/question.gif" /></div><fmt:message key="doNotCall" />:</div>
		 <div class="listp">
		 <!-- input field -->
                <form:checkbox path="crmContact.doNotCall" />
         <!-- end input field -->   	
		 </div>
		 </div>
		 
		 <div class="list${classIndex % 2}"><c:set var="classIndex" value="${classIndex+1}" />
		 <div class="listfl"><fmt:message key="phone" /> 1:</div>
		 <div class="listp">
		 <!-- input field -->
				<form:input path="crmContact.phone1" cssClass="textfield" maxlength="255" size="60" htmlEscape="true" id="phone1_num"/>
				<form:input path="crmContact.phone1" cssClass="textfield" maxlength="255" size="60" htmlEscape="true" onkeydown="javascript:backspacerDOWN(this,event);" onkeyup="javascript:backspacerUP(this,event);" id="us_phone1_num"/>    
		 <!-- end input field -->	
	 	 </div>
		 </div>
		 
		 <div class="list${classIndex % 2}"><c:set var="classIndex" value="${classIndex+1}" />
		 <div class="listfl"><fmt:message key="phone" /> 2:</div>
		 <div class="listp">
		 <!-- input field -->
				<form:input path="crmContact.phone2" cssClass="textfield" maxlength="255" size="60" htmlEscape="true" id="phone2_num"/>
				<form:input path="crmContact.phone2" cssClass="textfield" maxlength="255" size="60" htmlEscape="true" onkeydown="javascript:backspacerDOWN(this,event);" onkeyup="javascript:backspacerUP(this,event);" id="us_phone2_num"/>    
		 <!-- end input field -->	
	 	 </div>
		 </div>
		 
		 <div class="list${classIndex % 2}"><c:set var="classIndex" value="${classIndex+1}" />
		 <div class="listfl"><fmt:message key="fax" />:</div>
		 <div class="listp">
		 <!-- input field -->
				<form:input path="crmContact.fax" cssClass="textfield" maxlength="255" size="60" htmlEscape="true"/>
		 <!-- end input field -->	
	 	 </div>
		 </div>
		
		<div class="list${classIndex % 2}"><c:set var="classIndex" value="${classIndex+1}" />
	  	<div class="listfl"><div class="helpImg"><img class="toolTipImg" title="Note::Customer Rating 1" src="../graphics/question.gif" /></div> <fmt:message key="rating" /> 1:</div>
	  	<div class="listp">
	  	<!-- input field -->
	  	<form:select path="crmContact.rating1">
	  		<form:option value=""></form:option>
	  		<c:forTokens items="${siteConfig['CUSTOMER_RATING_1'].value}" delims="," var="type" varStatus="status">
				<form:option value ="${type}"><c:out value ="${type}" /></form:option>
		  	</c:forTokens>
		</form:select>
	    <!-- end input field -->   	
	  	</div>
	  	</div>	  	
	  	
	  	<div class="list${classIndex % 2}"><c:set var="classIndex" value="${classIndex+1}" />
	  	<div class="listfl"><div class="helpImg"><img class="toolTipImg" title="Note::Customer Rating 2." src="../graphics/question.gif" /></div> <fmt:message key="rating" /> 2:</div>
	  	<div class="listp">
	  	<!-- input field -->
		<form:select path="crmContact.rating2">
	  		<form:option value=""></form:option>
	  		<c:forTokens items="${siteConfig['CUSTOMER_RATING_2'].value}" delims="," var="type" varStatus="status">
				<form:option value ="${type}"><c:out value ="${type}" /></form:option>
		  	</c:forTokens>
		</form:select>
	    <!-- end input field -->   	
	  	</div>
	  	</div>		
	<!-- end tab -->        
	</div>
	
	<h4 title="Additional Information">Additional Information</h4>
	<div>
	<!-- start tab -->

         <c:set var="classIndex" value="0" />
		 <div class="listdivi ln tabdivi"></div>
		
		 <div class="list${classIndex % 2}"><c:set var="classIndex" value="${classIndex+1}" />
		 <div class="listfl"><div class="helpImg"><img class="toolTipImg" title="Contact Type::Select the type of contact for this contact. To manage the available types of contacts goto: SiteInfo-->Site Configuration-->CRM Configuration." src="../graphics/question.gif" /></div><fmt:message key="crmContactType" />:</div>
		 <div class="listp">
		 <!-- input field -->
				<form:select path="crmContact.contactType">
					<form:option value="" label=""></form:option>
					<c:forTokens items="${types}" delims="," var="type" >
	    		      <form:option value="${type}">${type}</form:option>
	    			</c:forTokens>
	    		</form:select>
		 <!-- end input field -->	
	 	 </div>
		 </div>
		 
		 <div class="list${classIndex % 2}"><c:set var="classIndex" value="${classIndex+1}" />
		 <div class="listfl"><div class="helpImg"><img class="toolTipImg" title="Rating::Select the rating for this contact. To manage the available ratings goto: SiteInfo-->Site Configuration-->CRM Configuration." src="../graphics/question.gif" /></div><fmt:message key="crmRating" />:</div>
		 <div class="listp">
		 <!-- input field -->
				<form:select path="crmContact.rating">
				<form:option value="" label=""></form:option>
				<c:forTokens items="${rating}" delims="," var="rate" >
	    		  <form:option value="${rate}">${rate}</form:option>
	    		</c:forTokens>
	    		</form:select>
		 <!-- end input field -->	
	 	 </div>
		 </div>

		 <div class="list${classIndex % 2}"><c:set var="classIndex" value="${classIndex+1}" />
		 <div class="listfl"><fmt:message key="shortDesc" />:</div>
		 <div class="listp">
		 <!-- input field -->
	    		<form:textarea path="crmContact.shortDesc" cols="45" rows="10" cssClass="textfield" htmlEscape="true"/>
	 	 <!-- end input field -->	
	 	 </div>
		 </div>
		 
		 <div class="list${classIndex % 2}"><c:set var="classIndex" value="${classIndex+1}" />
		 <div class="listfl"><fmt:message key="description" />:</div>
		 <div class="listp">
		 <!-- input field -->
	    		<form:textarea path="crmContact.description" cols="45" rows="10" cssClass="textfield" htmlEscape="true"/>
	 	 <!-- end input field -->	
	 	 </div>
		 </div>
		 
		 <div class="list${classIndex % 2}"><c:set var="classIndex" value="${classIndex+1}" />
		 <div class="listfl"><fmt:message key="dialingNoteHistory" />:</div>
		 <div class="listp" onClick="document.getElementById('information').style.display='block';">
		 View Dialing Note History
		 </div>
		 </div>
		
		 <div class="list${classIndex % 2}" id="information" style="display: none;"><c:set var="classIndex" value="${classIndex+1}" />
		 <div class="listfl"><fmt:message key="dialingNoteHistory" />:</div>
		 <div class="listp">
		 <!-- input field -->
	 	 		<c:forEach items="${crmContactForm.crmContact.dialingNoteHistory}" var="dialingNoteHistory" varStatus="status">
          		<p>
          		Note: <c:out value="${dialingNoteHistory.note}" /> &nbsp; &nbsp; &nbsp; Date: <c:out value="${dialingNoteHistory.created}" /> 
          		</p>
          		</c:forEach>  
	 	 <!-- end input field -->	
	 	 </div>
		 </div>
		 
		 
		 <div class="list${classIndex % 2}"><c:set var="classIndex" value="${classIndex+1}" />
		 <div class="listfl"><fmt:message key="dialingNote" />:</div>
		 <div class="listp">
		 <!-- input field -->
	    		<form:textarea path="crmContact.newDialingNote.note" cols="45" rows="10" cssClass="textfield" htmlEscape="true"/>
	 	 <!-- end input field -->	
	 	 </div>
		 </div>
		 
		 <div class="list${classIndex % 2}"><c:set var="classIndex" value="${classIndex+1}" />
		 <div class="listfl"><fmt:message key="leadSource" />:</div>
		 <div class="listp">
		 <!-- input field -->
				<form:input path="crmContact.leadSource" cssClass="textfield" maxlength="255" size="60" htmlEscape="true"/>
	 	 <!-- end input field -->	
	 	 </div>
		 </div>
		 
		 <div class="list${classIndex % 2}"><c:set var="classIndex" value="${classIndex+1}" />
   		 <div class="listfl"><fmt:message key="qualifier" />:</div>
		 <div class="listp">
		 <!-- input field -->
	        <form:select path="crmContact.qualifier">
	          <form:option value=""><fmt:message key="none" /></form:option>
	          <c:forEach items="${crmQualifierList}" var="qualifier">
	  	        <form:option value="${qualifier.id}"><c:out value="${qualifier.name}" /></form:option>
		      </c:forEach>
	        </form:select>  	
		 </div>
		 </div>
		 
		 <c:if test="${languageCodes != null}">
         <div class="list${classIndex % 2}"><c:set var="classIndex" value="${classIndex+1}" />
	     <div class="listfl"><fmt:message key='language' />:</div>
		 <div class="listp">
		 <!-- input field -->
       		<form:select path="crmContact.language">
       		  <form:option value="">Please Select</form:option>
        	  <form:option value="en"><fmt:message key="language_en"/></form:option>        	  
        	  <c:forEach items="${languageCodes}" var="i18n">
          		<form:option value="${i18n.languageCode}"><fmt:message key="language_${i18n.languageCode}"/></form:option>
          		</c:forEach>
	  		</form:select>
 		 <!-- end input field -->   	
		 </div>
		 </div>
		 </c:if>
		 
		 <div class="list${classIndex % 2}"><c:set var="classIndex" value="${classIndex+1}" />
		 <div class="listfl"><fmt:message key="department" />:</div>
		 <div class="listp">
		 <!-- input field -->
		  		<form:input path="crmContact.department" cssClass="textfield" maxlength="255" size="60" htmlEscape="true"/>
	 	 <!-- end input field -->	
	 	 </div>
		 </div>
		
	<!-- end tab -->        
	</div>
	
	<h4 title="Address Information">Address Information</h4>
	<div>
	<!-- start tab -->

         <c:set var="classIndex" value="0" />
		 <div class="listdivi ln tabdivi"></div>
		 
		 <div class="list${classIndex % 2}"><c:set var="classIndex" value="${classIndex+1}" />
		 <div style="float:left;width:50%">
		 <div class="listfl"><fmt:message key="street" />:</div>
		 <div class="listp">
		 <!-- input field -->
				<form:textarea path="crmContact.street" cssClass="textArea250x70" htmlEscape="true"/>
		 <!-- end input field -->	
	 	 </div>
	 	 </div>
	 	 
	 	 <div style="float:right;width:50%">
	 	 <div class="listfl"><fmt:message key="otherStreet" />:</div>
	 	 <div class="listp">
		 <!-- input field -->
				<form:textarea path="crmContact.otherStreet" cssClass="textArea250x70" htmlEscape="true"/>
		 <!-- end input field -->	
	 	 </div>
		 </div>
		 </div>
		 
		 <div class="list${classIndex % 2}"><c:set var="classIndex" value="${classIndex+1}" />
		 <div style="float:left;width:50%">
		 <div class="listfl"><fmt:message key="city" />:</div>
		 <div class="listp">
		 <!-- input field -->
				<form:input path="crmContact.city" cssClass="textfield250" maxlength="255" size="60" htmlEscape="true"/>
		 <!-- end input field -->	
	 	 </div>
	 	 </div>
	 	 
	 	 <div style="float:right;width:50%">
	 	 <div class="listfl"><fmt:message key="otherCity" />:</div>
	 	 <div class="listp">
		 <!-- input field -->
				<form:input path="crmContact.otherCity" cssClass="textfield250" maxlength="255" size="60" htmlEscape="true"/>
		 <!-- end input field -->	
	 	 </div>
		 </div>
		 </div>
		 
		 <div class="list${classIndex % 2}"><c:set var="classIndex" value="${classIndex+1}" />
		 <div style="float:left;width:50%">
		 <div class="listfl"><fmt:message key="stateProvince" />:</div>
		 <div class="listp">
		 <!-- input field -->
				<form:input path="crmContact.stateProvince" cssClass="textfield250" maxlength="255" size="60" htmlEscape="true"/>
		 <!-- end input field -->	
	 	 </div>
	 	 </div>
	 	 
	 	 <div style="float:right;width:50%">
	 	 <div class="listfl"><fmt:message key="otherState" />:</div>
	 	 <div class="listp">
		 <!-- input field -->
				<form:input path="crmContact.otherStateProvince" cssClass="textfield250" maxlength="255" size="60" htmlEscape="true"/>
		 <!-- end input field -->	
	 	 </div>
		 </div>
		 </div>
		 
		 <div class="list${classIndex % 2}"><c:set var="classIndex" value="${classIndex+1}" />
		 <div style="float:left;width:50%">
		 <div class="listfl"><fmt:message key="zipCode" />:</div>
		 <div class="listp">
		 <!-- input field -->
				<form:input path="crmContact.zip" cssClass="textfield250" maxlength="255" size="60" htmlEscape="true"/>
		 <!-- end input field -->	
	 	 </div>
	 	 </div>
	 	 
	 	 <div style="float:right;width:50%">
	 	 <div class="listfl"><fmt:message key="otherZip" />:</div>
	 	 <div class="listp">
		 <!-- input field -->
				<form:input path="crmContact.otherZip" cssClass="textfield250" maxlength="255" size="60" htmlEscape="true"/>
		 <!-- end input field -->	
	 	 </div>
		 </div>
		 </div>
		 
		 <div class="list${classIndex % 2}"><c:set var="classIndex" value="${classIndex+1}" />
		 <div style="float:left;width:50%">
		 <div class="listfl"><fmt:message key="country" />:</div>
		 <div class="listp">
		 <!-- input field -->
				<form:select id="country" path="crmContact.country" cssClass="textfield250" onchange="toggleStateProvince(this)" >
                  <form:option value="" label="Please Select"/>
                  <form:options items="${countries}" itemValue="code" itemLabel="name"/>
                </form:select>
                <form:errors path="crmContact.country" cssClass="error" />
		 <!-- end input field -->	
	 	 </div>
	 	 </div>
	 	 
	 	 <div style="float:right;width:50%">
	 	 <div class="listfl"><fmt:message key="otherCountry" />:</div>
	 	 <div class="listp">
		 <!-- input field -->
				<form:select id="country" path="crmContact.otherCountry" cssClass="textfield250">
                  <form:option value="" label="Please Select"/>
                  <form:options items="${countries}" itemValue="code" itemLabel="name"/>
                </form:select>
                <form:errors path="crmContact.otherCountry" cssClass="error" />
		 <!-- end input field -->	
	 	 </div>
		 </div>
		 </div>
		 
		 <div class="list${classIndex % 2}"><c:set var="classIndex" value="${classIndex+1}" />
		 <div style="float:left;width:50%">
		 <div class="listfl"><fmt:message key="addressType" />:</div>
		 <div class="listp">
		 <!-- input field -->
				<form:radiobutton path="crmContact.addressType" value="Residential"/> <fmt:message key="residential" /><br /><form:radiobutton path="crmContact.addressType" value="Commercial"/> <fmt:message key="commercial" />
         		<form:errors path="crmContact.addressType" cssClass="error" />
		 <!-- end input field -->	
	 	 </div>
	 	 </div>
	 	 
	 	 <div style="float:right;width:50%">
	 	 <div class="listfl"><fmt:message key="otherAddressType" />:</div>
	 	 <div class="listp">
		 <!-- input field -->
				<form:radiobutton path="crmContact.otherAddressType" value="Residential"/> <fmt:message key="residential" /><br /><form:radiobutton path="crmContact.otherAddressType" value="Commercial"/> <fmt:message key="commercial" />
         		<form:errors path="crmContact.otherAddressType" cssClass="error" />
		 <!-- end input field -->	
	 	 </div>
		 </div>
		 </div>

		
	<!-- end tab -->        
	</div>         

	<c:if test="${fn:length(crmContactForm.crmContact.crmContactFields) gt 0}">
	<h4 title="Contact Fields">Contact Fields</h4>
	<div>
	<!-- start tab -->
		
         <c:set var="classIndex" value="0" />
		 <div class="listdivi ln tabdivi"></div>
		 <c:forEach items="${crmContactForm.crmContact.crmContactFields}" var="field" varStatus="status">
		 <div class="list${classIndex % 2}"><c:set var="classIndex" value="${classIndex+1}" />
		 <div class="listfl">${crmContactForm.crmContact.crmContactFields[status.index].fieldName}:</div>
		 <div class="listp">
		 <!-- input field -->
		 	<c:if test="${field.fieldType == 1 or field.fieldType == 7 }">
		 	  <form:input path="crmContact.crmContactFields[${status.index}].fieldValue" cssClass="textArea250x70" htmlEscape="true"/>
		    </c:if>
		 	<c:if test="${field.fieldType == 2}">
		 	  <form:textarea path="crmContact.crmContactFields[${status.index}].fieldValue" cssClass="textArea250x70" htmlEscape="true"/>
		    </c:if>
		 	<c:if test="${field.fieldType == 3}">
		 	  <c:forTokens items="${field.options}" delims="," var="option">
		 	    <form:checkbox path="crmContact.crmContactFields[${status.index}].valueList" value="${fn:trim(option)}" label="${option}"/>
		 	  </c:forTokens>
		 	</c:if>
		 	<c:if test="${field.fieldType == 4}">
		 	  <c:forTokens items="${field.options}" delims="," var="option">
		 	    <form:radiobutton path="crmContact.crmContactFields[${status.index}].fieldValue"  value="${fn:trim(option)}" label="${option}"/>
		 	  </c:forTokens>
		 	</c:if>
		 	<c:if test="${field.fieldType == 5}" >
		 	  <form:select path="crmContact.crmContactFields[${status.index}].fieldValue">
		 	  <form:option value="" label="Please Select"></form:option>
		 	  <c:forTokens items="${field.options}" delims="," var="option">
		 	    <form:option value="${fn:trim(option)}" label="${option}"></form:option>
		 	  </c:forTokens>
		 	  </form:select>
		 	</c:if>
		 	<form:errors path="crmContact.crmContactFields[${status.index}].fieldValue" cssClass="error" />
		 <!-- end input field -->	
	 	 </div>
	 	 </div>
	 	 </c:forEach>
	 	
	     
	<!-- end tab -->        
	</div>
	</c:if>
	
	<c:if test="${crmContactForm.crmContact.attachmentList != null}">
	<h4 title="Attachments">Attachments</h4>
	<div>
	<!-- start tab -->
		
         <c:set var="classIndex" value="0" />
		 <div class="listdivi ln tabdivi"></div>
		 <c:forEach items="${crmContactForm.crmContact.attachmentList}" var="file" varStatus="status">
		 <div class="list${classIndex % 2}"><c:set var="classIndex" value="${classIndex+1}" />
		 <div class="listfl">
		   <a href="../../assets/CRM/contact_${crmContactForm.crmContact.id}/${file}"> <c:out value="${file}"></c:out> </a>
		 </div>
		 <div class="listp">
		 </div>
	 	 </div>
	 	 </c:forEach>
	 	
	     
	<!-- end tab -->        
	</div>
	</c:if>
<!-- end tabs -->			
</div>

<!-- start button -->
	<div align="left" class="button">
	  <c:if test="${crmContactForm.newCrmContact}">
	    <sec:authorize ifAnyGranted="ROLE_AEM,ROLE_ADMIN_${prop_site_id},ROLE_CRM">
          <input type="submit" value="<spring:message code="add"/>" />
        </sec:authorize>  
      </c:if>
      <c:if test="${!crmContactForm.newCrmContact}">
        <sec:authorize ifAnyGranted="ROLE_AEM,ROLE_ADMIN_${prop_site_id},ROLE_CRM">
          <input type="submit" value="<spring:message code="update"/>" />
        </sec:authorize>
        <sec:authorize ifAnyGranted="ROLE_AEM,ROLE_ADMIN_${prop_site_id},ROLE_CRM">  
          <input type="submit" value="<spring:message code="delete"/>" name="_delete" onClick="return confirm('Delete permanently?')" />
        </sec:authorize>  
      </c:if>
	</div>
<!-- end button -->
  
  <!-- end table -->
    </td><td class="boxmidr" ></td></tr>
  <tr><td class="botl"></td><td class="botr"></td></tr>
  </table>
  
<!-- end main box -->  
</div>
</form:form>

<script language="JavaScript">
<!--
function toggleStateProvince(el) {
	  if (el.value == "US") {
		  document.getElementById('phone1_num').disabled=true;
	      document.getElementById('phone1_num').style.display="none";
	      document.getElementById('us_phone1_num').disabled=false;
	      document.getElementById('us_phone1_num').style.display="block";
	      document.getElementById('phone2_num').disabled=true;
	      document.getElementById('phone2_num').style.display="none";
	      document.getElementById('us_phone2_num').disabled=false;
	      document.getElementById('us_phone2_num').style.display="block";
	  } else if (el.value == "CA") {
		  document.getElementById('us_phone1_num').disabled=true;
	      document.getElementById('us_phone1_num').style.display="none";
	      document.getElementById('phone1_num').disabled=false;
	      document.getElementById('phone1_num').style.display="block";
	      document.getElementById('us_phone2_num').disabled=true;
	      document.getElementById('us_phone2_num').style.display="none";
	      document.getElementById('phone2_num').disabled=false;
	      document.getElementById('phone2_num').style.display="block";
	  } else {
		  document.getElementById('us_phone1_num').disabled=true;
	      document.getElementById('us_phone1_num').style.display="none";
	      document.getElementById('phone1_num').disabled=false;
	      document.getElementById('phone1_num').style.display="block";
	      document.getElementById('us_phone2_num').disabled=true;
	      document.getElementById('us_phone2_num').style.display="none";
	      document.getElementById('phone2_num').disabled=false;
	      document.getElementById('phone2_num').style.display="block";
	  }
}
toggleStateProvince(document.getElementById('country'));

</script>
</sec:authorize> 

  </tiles:putAttribute>    
</tiles:insertDefinition>