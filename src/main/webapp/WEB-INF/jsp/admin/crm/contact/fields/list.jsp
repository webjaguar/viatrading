<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/security/tags" prefix="sec" %>


<tiles:insertDefinition name="admin.crm.contact" flush="true">
  <tiles:putAttribute name="content" type="string">
  
<sec:authorize ifAnyGranted="ROLE_AEM,ROLE_ADMIN_${prop_site_id},ROLE_CRM_FIELD"> 

<form action="crmContactFieldList.jhtm" method="post" id="list" name="list_form">
<input type="hidden" id="sort" name="sort" value="${crmContactSearch.sort}" />
<!-- main box -->
  <div id="mboxfull">
  
  <!-- start table -->
  <table cellpadding="0" cellspacing="0" border="0" class="module" >
  <tr>
    <td class="topl_g">
	  
	  <!-- breadcrumb -->
	  <p class="breadcrumb">
	    <a href="../crm">CRM</a> &gt;
	    Fields
	  </p>
	  
	  <!-- Error Message -->
	  
	  <!-- header image -->
	  <img class="headerImage" src="../graphics/contact.gif" />
	  
	  </td><td class="topr_g" ></td></tr>
  <tr><td class="boxmidlrg" >

 <!-- tabs -->
 <div class="tab-wrapper">
	<div>
	<!-- start tab -->
	  	<div class="listdivi ln tabdivi"></div>
	  	<div class="listdivi"></div>
	   
		  	<div class="listlight">
		  	<!-- input field --> 
		  	<table border="0" cellpadding="0" cellspacing="1" width="100%">
			  <tr>
			  <c:if test="${model.fieldList.nrOfElements > 0}">
			  <td class="pageShowing">
			  <fmt:message key="showing">
				<fmt:param value="${model.fieldList.firstElementOnPage + 1}"/>
				<fmt:param value="${model.fieldList.lastElementOnPage + 1}"/>
				<fmt:param value="${model.fieldList.nrOfElements}"/>
			  </fmt:message>
			  </td>
			  </c:if>
			  <td class="pageNavi">
			  Page
			  <select name="page" id="page" onchange="submit()">
			  <c:forEach begin="1" end="${model.fieldList.pageCount}" var="page">
			  	<option value="${page}" <c:if test="${page == (model.fieldList.page+1)}">selected</c:if>>${page}</option>
			  </c:forEach>
			  </select>
			  of <c:out value="${model.fieldList.pageCount}"/>
			  | 
			  <c:if test="${model.fieldList.firstPage}"><span class="pageNaviDead">previous</span></c:if>
			  <c:if test="${not model.fieldList.firstPage}"><a href="<c:url value="crmContactFieldList.jhtm"><c:param name="page" value="${model.fieldList.page}"/></c:url>" class="pageNaviLink">previous</a></c:if>
			  | 
			  <c:if test="${model.fieldList.lastPage}"><span class="pageNaviDead">next</span></c:if>
			  <c:if test="${not model.fieldList.lastPage}"><a href="<c:url value="crmContactFieldList.jhtm"><c:param name="page" value="${model.fieldList.page+2}"/></c:url>" class="pageNaviLink">next</a></c:if>
			  </td>
			  </tr>
			</table>
			
			<table border="0" cellpadding="0" cellspacing="0" width="100%" class="listings">
			  <tr class="listingsHdr2">
			    <td class="indexCol">&nbsp;</td>
				<td class="listingsHdr3"><fmt:message key="field" /></td>
			    <td class="listingsHdr3"><fmt:message key="name" /></td>
			    <%-- 
			    <td class="listingsHdr3"><fmt:message key="type" /></td>
			    <td class="listingsHdr3"><fmt:message key="required" /></td>
			    --%>
			    <td class="listingsHdr3"><fmt:message key="enabled" /></td>
			  </tr>
			  <c:forEach items="${model.fieldList.pageList}" var="field" varStatus="status">
			  <tr class="row${status.index % 2}" onmouseover="changeStyleClass(this,'rowOver')" onmouseout="changeStyleClass(this,'row${status.index % 2}')">
			    <td class="indexCol"><c:out value="${status.count}" />.</td>
				<td class="nameCol"><a href="crmContactFieldForm.jhtm?id=${field.id}"><c:out value="${field.globalName}"/></a></td>
			    <td class="nameCol"><c:out value="${field.fieldName}"/></td>
			     <%-- 
			    <td class="nameCol">
			      <c:forTokens items="Text,TextArea,CheckBox,RadioButton,DropDown" delims="," var="current" varStatus="status">
			        	  <c:if test="${status.index == field.fieldType}">${current}</c:if>
			      </c:forTokens>
			    </td>
			    <td class="nameCol"><c:if test="${field.required}"><div align="left"><img src="../graphics/checkbox.png" /></div></c:if></td>
			    --%>
				<td class="nameCol"><c:if test="${field.enabled}"><div align="left"><img src="../graphics/checkbox.png" /></div></c:if></td>
			  </tr>
			</c:forEach>
			<c:if test="${model.fieldList.nrOfElements == 0}">
			  <tr class="emptyList"><td colspan="3">&nbsp;</td></tr>
			</c:if>
			</table>
			
			<table border="0" cellpadding="0" cellspacing="1" width="100%">
			  <tr>
			  <td class="pageSize">
			  <select name="size" onchange="document.getElementById('page').value=1;submit()">
			  <c:forTokens items="10,25,50" delims="," var="current">
			  	  <option value="${current}" <c:if test="${current == model.fieldList.pageSize}">selected</c:if>>${current} <fmt:message key="perPage" /></option>
			  </c:forTokens>
			  </select>
			  </td>
			  </tr>
			</table>
		  	<!-- end input field -->  	  
		  	
		  	<!-- start button --> 
			<!-- end button -->	
			</div>
	
			<!-- start button --> 
		  	<div align="left" class="button">
		  	  <sec:authorize ifAnyGranted="ROLE_AEM,ROLE_ADMIN_${prop_site_id},ROLE_CRM">
		  	    <input type="submit" name="__add" value="<fmt:message key="add" />">
		  	  </sec:authorize>
			</div>
			<!-- end button -->
	<!-- end tab -->        
	</div>
<!-- end tabs -->			
</div>
  
  <!-- end table -->
   </td><td class="boxmidr" ></td></tr>
  <tr><td class="botl"></td><td class="botr"></td></tr>
  </table>
  
<!-- end main box -->  
</div> 
</form> 
</sec:authorize>

  </tiles:putAttribute>    
</tiles:insertDefinition>