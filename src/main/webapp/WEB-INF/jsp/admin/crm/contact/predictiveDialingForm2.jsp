<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ taglib uri="/WEB-INF/tlds/spring.tld" prefix="spring" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ taglib uri="http://www.springframework.org/security/tags" prefix="sec" %>
<%@ taglib uri="/WEB-INF/tlds/custom.tld" prefix="wj" %>
<script type="text/javascript" src="../javascript/autocompleter1.2.js"></script>
<script type="text/javascript" src="../javascript/autocompleter.Request1.2.js"></script>
<script type="text/javascript" src="../javascript/observer.js"></script>

<tiles:insertDefinition name="admin.crm.contact" flush="true">
  <tiles:putAttribute name="content" type="string">
<sec:authorize ifAnyGranted="ROLE_AEM,ROLE_ADMIN_${prop_site_id},ROLE_CRM">
<script language="JavaScript"> 
<!--
window.addEvent('domready', function(){			
	var tabs1 = new SimpleTabs($('tab-block-1'), { selector: 'h4'});
	var Tips1 = new Tips($$('.toolTipImg'));

	var formAccountName = $('formAccountName');
	new Autocompleter.Request.HTML(formAccountName, '../crm/ajax-showCrmAccounts.jhtm', {
		'indicatorClass': 'autocompleter-loading',
		'postData': { 'search': 'crmAccountName' },
		'injectChoice': function(choice) {
			var text = choice.getFirst();
			var value = text.innerHTML;
			choice.inputValue = value;
			text.set('html', this.markQueryValue(value));
			this.addChoiceEvents(choice);
		}
	});
	
	var myAccordion = new Fx.Accordion($$('div.arrow_drop'), $$('div.information'), {
		display: 1, alwaysHide: true,
    	onActive: function() {$('information').removeClass('displayNone');}
	});
});
function changeHref(id,salesRepId) {
	$('emailSalesRep').set('href', 'email.jhtm?id=' + id + '&salesrepid=' + salesRepId);
} 
function loadEditor(el) {
	  var oFCKeditor = new FCKeditor(el);
	  oFCKeditor.BasePath = "<c:url value="/FCKeditor/"/>";
	  oFCKeditor.Config["CustomConfigurationsPath"] = '<c:url value="/admin/javascript/fckconfig.js"/>';
	  oFCKeditor.ToolbarSet = 'Html';
	  oFCKeditor.Width = 650;
	  oFCKeditor.Height = 400;
	  oFCKeditor.ReplaceTextarea();
	  document.getElementById(el + '_link').style.display="none";
	}
function automateCityState(zipCode) {
	var request = new Request.JSON({
		url: "${_contextpath}/jsonZipCode.jhtm?zipCode="+zipCode,
		onRequest: function() { 
		},
		onComplete: function(jsonObj) {
			document.getElementById('customer.address.city').value = jsonObj.city;
			document.getElementById('state').value = jsonObj.stateAbbv;
			$$('.highlight').each(function(el) {
				var end = el.getStyle('background-color');
				end = (end == 'transparent') ? '#fff' : end;
				var myFx = new Fx.Tween(el, {duration: 500, wait: false});
				myFx.start('background-color', '#f00', end);
			});
		}
	}).send();
}
//--> 
</script>

<script type="text/javascript">

var zChar = new Array(' ', '(', ')', '-', '.');
var maxphonelength = 13;
var phonevalue1;
var phonevalue2;
var cursorposition;

function ParseForNumber1(object){
phonevalue1 = ParseChar(object.value, zChar);
}
function ParseForNumber2(object){
phonevalue2 = ParseChar(object.value, zChar);
}

function backspacerUP(object,e) { 
if(e){ 
e = e 
} else {
e = window.event 
} 
if(e.which){ 
var keycode = e.which 
} else {
var keycode = e.keyCode 
}

ParseForNumber1(object)

if(keycode >= 48){
ValidatePhone(object)
}
}

function backspacerDOWN(object,e) { 
if(e){ 
e = e 
} else {
e = window.event 
} 
if(e.which){ 
var keycode = e.which 
} else {
var keycode = e.keyCode 
}
ParseForNumber2(object)
} 

function GetCursorPosition(){

var t1 = phonevalue1;
var t2 = phonevalue2;
var bool = false
for (i=0; i<t1.length; i++)
{
if (t1.substring(i,1) != t2.substring(i,1)) {
if(!bool) {
cursorposition=i
bool=true
}
}
}
}

function ValidatePhone(object){

var p = phonevalue1

p = p.replace(/[^\d]*/gi,"")

if (p.length < 3) {
object.value=p
} else if(p.length==3){
pp=p;
d4=p.indexOf('(')
d5=p.indexOf(')')
if(d4==-1){
pp="("+pp;
}
if(d5==-1){
pp=pp+")";
}
object.value = pp;
} else if(p.length>3 && p.length < 7){
p ="(" + p; 
l30=p.length;
p30=p.substring(0,4);
p30=p30+")"

p31=p.substring(4,l30);
pp=p30+p31;

object.value = pp; 

} else if(p.length >= 7){
p ="(" + p; 
l30=p.length;
p30=p.substring(0,4);
p30=p30+")"

p31=p.substring(4,l30);
pp=p30+p31;

l40 = pp.length;
p40 = pp.substring(0,8);
p40 = p40 + "-"

p41 = pp.substring(8,l40);
ppp = p40 + p41;

object.value = ppp.substring(0, maxphonelength);
}

GetCursorPosition()

if(cursorposition >= 0){
if (cursorposition == 0) {
cursorposition = 2
} else if (cursorposition <= 2) {
cursorposition = cursorposition + 1
} else if (cursorposition <= 5) {
cursorposition = cursorposition + 2
} else if (cursorposition == 6) {
cursorposition = cursorposition + 2
} else if (cursorposition == 7) {
cursorposition = cursorposition + 4
e1=object.value.indexOf(')')
e2=object.value.indexOf('-')
if (e1>-1 && e2>-1){
if (e2-e1 == 4) {
cursorposition = cursorposition - 1
}
}
} else if (cursorposition < 11) {
cursorposition = cursorposition + 3
} else if (cursorposition == 11) {
cursorposition = cursorposition + 1
} else if (cursorposition >= 12) {
cursorposition = cursorposition
}

var txtRange = object.createTextRange();
txtRange.moveStart( "character", cursorposition);
txtRange.moveEnd( "character", cursorposition - object.value.length);
txtRange.select();
}

}

function ParseChar(sStr, sChar)
{
if (sChar.length == null) 
{
zChar = new Array(sChar);
}
else zChar = sChar;

for (i=0; i<zChar.length; i++)
{
sNewStr = "";

var iStart = 0;
var iEnd = sStr.indexOf(sChar[i]);

while (iEnd != -1)
{
sNewStr += sStr.substring(iStart, iEnd);
iStart = iEnd + 1;
iEnd = sStr.indexOf(sChar[i], iStart);
}
sNewStr += sStr.substring(sStr.lastIndexOf(sChar[i]) + 1, sStr.length);

sStr = sNewStr;
}

return sNewStr;
}
</script> 
 
<form:form commandName="crmContactForm" action="crmContactForm.jhtm" method="post">
<form:hidden path="newCrmContact" />

<form:hidden path="crmUpdate" value="true"/>
<!-- main box -->
  <div id="mboxfull">

  <!-- start table -->
  <table cellpadding="0" cellspacing="0" border="0" class="module" >
  <tr>
    <td class="topl_g">
	  
	  <!-- breadcrumb -->
	  <p class="breadcrumb">
	    <a href="../crm">CRM</a> &gt;
	    <a href="../crm/crmAccountForm.jhtm?id=${crmContactForm.crmContact.accountId}"><c:out value="${crmContactForm.crmContact.accountName}" /></a> &gt;
	    <c:if test="${crmContactForm.crmContact.id != null}">
	      <a href="../crm/crmContactForm.jhtm?id=${crmContactForm.crmContact.id}"><c:out value="${crmContactForm.crmContact.contactName}" /></a> &gt;
	    </c:if>
	    <a href="../crm/crmContactList.jhtm">Contacts</a> &gt;
	    form 
	  </p>
	  
	  <!-- Error Message -->
	  <%--<c:if test="${message != null}">
  		<div class="message"><fmt:message key="${message}"/></div>
  	  </c:if> --%>
  	  <spring:hasBindErrors name="crmContactForm">
		<div class="message">Please fix all errors!</div>
	  </spring:hasBindErrors>

  </td><td class="topr_g" ></td></tr>
  <tr><td class="boxmidlrg" >

	<div>
	<!-- start tab -->

         <c:set var="classIndex" value="0" />
		<div class="listdivi ln tabdivi"></div>
		
		 <div class="list${classIndex % 2}"><c:set var="classIndex" value="${classIndex+1}" />
		 <div class="listfl"><fmt:message key="createdBy" />:</div>
		 <div class="listp">
		 <!-- input field -->
		 		<span class="static"><c:out value="${crmContactForm.crmContact.createdBy}" /></span>
	 	 <!-- end input field -->	
	 	 </div>
		 </div>
		
		 <div class="list${classIndex % 2}"><c:set var="classIndex" value="${classIndex+1}" />
		 <div class="listfl"><div class="requiredField"><fmt:message key="crmAccountName" />:</div></div>
		 <div class="listp">
		 <!-- input field -->
		 		<form:input path="crmContact.accountName" cssClass="textfield" maxlength="255" size="60" id="formAccountName" htmlEscape="true"/>
		  		<form:errors path="crmContact.accountName" cssClass="error"/>
	 	 <!-- end input field -->	
	 	 </div>
		 </div>
		 
		 <div class="list${classIndex % 2}"><c:set var="classIndex" value="${classIndex+1}" />
		 <div class="listfl"><div class="helpImg"><img class="toolTipImg" title="Assigned To::Assign this account to a user. To manage users goto: More-->Access Privilege." src="../graphics/question.gif" /></div><fmt:message key="assignedTo" />:</div>
		 <div class="listp">
		 <!-- input field -->
	    		<form:select path="crmContact.accessUserId">
	    		    <form:option value="" label="Select"></form:option>
	    		  <c:forEach items="${accessUsers}" var="accessUser">
	    		    <form:option value="${accessUser.id}" label="${accessUser.username}"></form:option>
	    		  </c:forEach>
	    		</form:select>
	     <!-- end input field -->	
	 	 </div>
		 </div>
		 
		 <div class="list${classIndex % 2}"><c:set var="classIndex" value="${classIndex+1}" />
		 <div class="listfl"><div class="requiredField"><fmt:message key="firstName" />:</div></div>
		 <div class="listp">
		 <!-- input field -->
	    		<form:input path="crmContact.firstName" cssClass="textfield" maxlength="255" size="60" htmlEscape="true"/>
		  		<c:if test="${not crmContactForm.newCrmContact}"><a href="crmTaskForm.jhtm?accountId=${crmContactForm.crmContact.accountId}&contactId=${crmContactForm.crmContact.id}"><fmt:message key="addTask" /></a></c:if>
				<form:errors path="crmContact.firstName" cssClass="error"/>
	 	 <!-- end input field -->	
	 	 </div>
		 </div>
		 
		 <div class="list${classIndex % 2}"><c:set var="classIndex" value="${classIndex+1}" />
		 <div class="listfl"><div class="requiredField"><fmt:message key="lastName" />:</div></div>
		 <div class="listp">
		 <!-- input field -->
	    		<form:input path="crmContact.lastName" cssClass="textfield" maxlength="255" size="60" htmlEscape="true"/>
		  		<form:errors path="crmContact.lastName" cssClass="error"/>
	 	 <!-- end input field -->	
	 	 </div>
		 </div>
		 
		 <div class="list${classIndex % 2}"><c:set var="classIndex" value="${classIndex+1}" />
		 <div class="listfl"><fmt:message key="email" /> 1:</div>
		 <div class="listp">
		 <!-- input field -->
				<form:input path="crmContact.email1" cssClass="textfield" maxlength="255" size="60" htmlEscape="true"/>
		  		<c:if test="${not crmContactForm.newCrmContact}"><a href="email.jhtm?contactId=${crmContactForm.crmContact.id}"><fmt:message key="sendEmail" /></a></c:if>
				<form:errors path="crmContact.email1" cssClass="error"/>
	 	 <!-- end input field -->	
	 	 </div>
		 </div>
		 
		 <div class="list${classIndex % 2}"><c:set var="classIndex" value="${classIndex+1}" />
		 <div class="listfl"><fmt:message key="email" /> 2:</div>
		 <div class="listp">
		 <!-- input field -->
				<form:input path="crmContact.email2" cssClass="textfield" maxlength="255" size="60" htmlEscape="true"/>
		 <!-- end input field -->	
	 	 </div>
		 </div>
		 <div class="list${classIndex % 2}"><c:set var="classIndex" value="${classIndex+1}" />
	 	 <div class="listfl"><fmt:message key="convertToCustomer" />:</div>
	 	 <div class="listp">
		 <!-- input field -->
			<c:choose>
			   <c:when test="${crmContactForm.crmContact.userId != null}"><img src="../graphics/checkbox.png" border="0" class="quickMode"></c:when>
			   <c:when test="${crmContactForm.crmContact.userId == null and crmContactForm.crmContact.email1 != null and crmContactForm.crmContact.email1 != '' and crmContactForm.crmContact.verified and crmContactForm.crmContact.accountName != 'Leads'}"><a href="../customers/addCustomer.jhtm?contactId=${crmContactForm.crmContact.id}"><img src="../graphics/box.png" border="0"></a></c:when>
			   <c:otherwise><img src="../graphics/box.png" border="0" class="quickMode"></c:otherwise>
			</c:choose>
		 <!-- end input field -->	
	 	 </div>
		 </div>
		 <div class="list${classIndex % 2}"><c:set var="classIndex" value="${classIndex+1}" />
		 <div class="listfl"><fmt:message key="title" />:</div>
		 <div class="listp">
		 <!-- input field -->
	    		<form:input path="crmContact.title" cssClass="textfield" maxlength="255" size="60" htmlEscape="true"/>
		  		<form:errors path="crmContact.title" cssClass="error"/>
	 	 <!-- end input field -->	
	 	 </div>
		 </div>
		 
		 <div class="list${classIndex % 2}"><c:set var="classIndex" value="${classIndex+1}" />
		 <div class="listfl"><fmt:message key="phone" /> 1:</div>
		 <div class="listp">
		 <!-- input field -->
				<form:input path="crmContact.phone1" cssClass="textfield" maxlength="255" size="60" htmlEscape="true" id="phone1_num"/>
				<form:input path="crmContact.phone1" cssClass="textfield" maxlength="255" size="60" htmlEscape="true" onkeydown="javascript:backspacerDOWN(this,event);" onkeyup="javascript:backspacerUP(this,event);" id="us_phone1_num"/>    
		 <!-- end input field -->	
	 	 </div>
		 </div>
		 
		 <div class="list${classIndex % 2}"><c:set var="classIndex" value="${classIndex+1}" />
		 <div class="listfl"><fmt:message key="phone" /> 2:</div>
		 <div class="listp">
		 <!-- input field -->
				<form:input path="crmContact.phone2" cssClass="textfield" maxlength="255" size="60" htmlEscape="true" id="phone2_num"/>
				<form:input path="crmContact.phone2" cssClass="textfield" maxlength="255" size="60" htmlEscape="true" onkeydown="javascript:backspacerDOWN(this,event);" onkeyup="javascript:backspacerUP(this,event);" id="us_phone2_num"/>    
		 <!-- end input field -->	
	 	 </div>
		 </div>
		 
		 <div class="list${classIndex % 2}"><c:set var="classIndex" value="${classIndex+1}" />
		 <div class="listfl"><fmt:message key="fax" />:</div>
		 <div class="listp">
		 <!-- input field -->
				<form:input path="crmContact.fax" cssClass="textfield" maxlength="255" size="60" htmlEscape="true"/>
		 <!-- end input field -->	
	 	 </div>
		 </div>
		 
		 <div class="list${classIndex % 2}"><c:set var="classIndex" value="${classIndex+1}" />
	  	<div class="listfl"><div class="helpImg"><img class="toolTipImg" title="Note::Customer Rating 1" src="../graphics/question.gif" /></div> <fmt:message key="rating" /> 1:</div>
	  	<div class="listp">
	  	<!-- input field -->
	  	<form:select path="crmContact.rating1">
	  		<form:option value=""></form:option>
	  		<c:forTokens items="${siteConfig['CUSTOMER_RATING_1'].value}" delims="," var="type" varStatus="status">
				<form:option value ="${type}"><c:out value ="${type}" /></form:option>
		  	</c:forTokens>
		</form:select>
	    <!-- end input field -->   	
	  	</div>
	  	</div>	  	
	  	
	  	<div class="list${classIndex % 2}"><c:set var="classIndex" value="${classIndex+1}" />
	  	<div class="listfl"><div class="helpImg"><img class="toolTipImg" title="Note::Customer Rating 2." src="../graphics/question.gif" /></div> <fmt:message key="rating" /> 2:</div>
	  	<div class="listp">
	  	<!-- input field -->
		<form:select path="crmContact.rating2">
	  		<form:option value=""></form:option>
	  		<c:forTokens items="${siteConfig['CUSTOMER_RATING_2'].value}" delims="," var="type" varStatus="status">
				<form:option value ="${type}"><c:out value ="${type}" /></form:option>
		  	</c:forTokens>
		</form:select>
	    <!-- end input field -->   	
	  	</div>
	  	</div>
	  	
	  	<div class="list${classIndex % 2}"><c:set var="classIndex" value="${classIndex+1}" />
		
		 <div class="listfl"><fmt:message key="street" />:</div>
		 <div class="listp">
		 <!-- input field -->
				<form:textarea path="crmContact.street" cssClass="textArea250x70" htmlEscape="true"/>
		 <!-- end input field -->	
	 	 </div>
	 	 </div>
	 	 
	 	 <div class="list${classIndex % 2}"><c:set var="classIndex" value="${classIndex+1}" />
		 <div class="listfl"><fmt:message key="city" />:</div>
		 <div class="listp">
		 <!-- input field -->
				<form:input path="crmContact.city" cssClass="textfield250" maxlength="255" size="60" htmlEscape="true"/>
		 <!-- end input field -->	
	 	 </div>
	 	 </div>
	 	 
	 	 <div class="list${classIndex % 2}"><c:set var="classIndex" value="${classIndex+1}" />
		 <div class="listfl"><fmt:message key="stateProvince" />:</div>
		 <div class="listp">
		 <!-- input field -->
				<form:input path="crmContact.stateProvince" cssClass="textfield250" maxlength="255" size="60" htmlEscape="true"/>
		 <!-- end input field -->	
	 	 </div>
	 	 </div>
	 	 
	 	 <div class="list${classIndex % 2}"><c:set var="classIndex" value="${classIndex+1}" />
		 <div class="listfl"><fmt:message key="zipCode" />:</div>
		 <div class="listp">
		 <!-- input field -->
				<form:input path="crmContact.zip" cssClass="textfield250" maxlength="255" size="60" htmlEscape="true"/>
		 <!-- end input field -->	
	 	 </div>
	 	 </div>
	 	 
	 	 <div class="list${classIndex % 2}"><c:set var="classIndex" value="${classIndex+1}" />
		 <div class="listfl"><fmt:message key="country" />:</div>
		 <div class="listp">
		 <!-- input field -->
				<form:select id="country" path="crmContact.country" cssClass="textfield250" onchange="toggleStateProvince(this)" >
                  <form:option value="" label="Please Select"/>
                  <form:options items="${countries}" itemValue="code" itemLabel="name"/>
                </form:select>
                <form:errors path="crmContact.country" cssClass="error" />
		 <!-- end input field -->	
	 	 </div>
	 	 </div>
	 	 
	 	 <div class="list${classIndex % 2}"><c:set var="classIndex" value="${classIndex+1}" />
		 <div class="listfl"><fmt:message key="addressType" />:</div>
		 <div class="listp">
		 <!-- input field -->
				<form:radiobutton path="crmContact.addressType" value="Residential"/> <fmt:message key="residential" /><br /><form:radiobutton path="crmContact.addressType" value="Commercial"/> <fmt:message key="commercial" />
         		<form:errors path="crmContact.addressType" cssClass="error" />
		 <!-- end input field -->	
	 	 </div>
	 	 </div>
	 	 
	 	 <div class="list${classIndex % 2}"><c:set var="classIndex" value="${classIndex+1}" />
	 	 <div class="listfl"><fmt:message key="otherStreet" />:</div>
	 	 <div class="listp">
		 <!-- input field -->
				<form:textarea path="crmContact.otherStreet" cssClass="textArea250x70" htmlEscape="true"/>
		 <!-- end input field -->	
	 	 </div>
		 </div>

	 	 <div class="list${classIndex % 2}"><c:set var="classIndex" value="${classIndex+1}" />
	 	 <div class="listfl"><fmt:message key="otherCity" />:</div>
	 	 <div class="listp">
		 <!-- input field -->
				<form:input path="crmContact.otherCity" cssClass="textfield250" maxlength="255" size="60" htmlEscape="true"/>
		 <!-- end input field -->	
	 	 </div>
		 </div>
	 	 
	 	 <div class="list${classIndex % 2}"><c:set var="classIndex" value="${classIndex+1}" />
	 	 <div class="listfl"><fmt:message key="otherState" />:</div>
	 	 <div class="listp">
		 <!-- input field -->
				<form:input path="crmContact.otherStateProvince" cssClass="textfield250" maxlength="255" size="60" htmlEscape="true"/>
		 <!-- end input field -->	
	 	 </div>
		 </div>
	 	 
	 	 <div class="list${classIndex % 2}"><c:set var="classIndex" value="${classIndex+1}" />
	 	 <div class="listfl"><fmt:message key="otherZip" />:</div>
	 	 <div class="listp">
		 <!-- input field -->
				<form:input path="crmContact.otherZip" cssClass="textfield250" maxlength="255" size="60" htmlEscape="true"/>
		 <!-- end input field -->	
	 	 </div>
		 </div>
	 	 
	 	 <div class="list${classIndex % 2}"><c:set var="classIndex" value="${classIndex+1}" />
	 	 <div class="listfl"><fmt:message key="otherCountry" />:</div>
	 	 <div class="listp">
		 <!-- input field -->
				<form:select id="country" path="crmContact.otherCountry" cssClass="textfield250">
                  <form:option value="" label="Please Select"/>
                  <form:options items="${countries}" itemValue="code" itemLabel="name"/>
                </form:select>
                <form:errors path="crmContact.otherCountry" cssClass="error" />
		 <!-- end input field -->	
	 	 </div>
		 </div>
	 	 
	 	 <div class="list${classIndex % 2}"><c:set var="classIndex" value="${classIndex+1}" />
	 	 <div class="listfl"><fmt:message key="otherAddressType" />:</div>
	 	 <div class="listp">
		 <!-- input field -->
				<form:radiobutton path="crmContact.otherAddressType" value="Residential"/> <fmt:message key="residential" /><br /><form:radiobutton path="crmContact.otherAddressType" value="Commercial"/> <fmt:message key="commercial" />
         		<form:errors path="crmContact.otherAddressType" cssClass="error" />
		 <!-- end input field -->	
	 	 </div>
		 </div>
		 
		 <div class="list${classIndex % 2}"><c:set var="classIndex" value="${classIndex+1}" />
		 <div class="listfl"><fmt:message key="description" />:</div>
		 <div class="listp">
		 <!-- input field -->
	    		<form:textarea path="crmContact.description" cols="45" rows="10" cssClass="textfield" htmlEscape="true"/>
	 	 <!-- end input field -->	
	 	 </div>
		 </div>
		 
		 <div class="list${classIndex % 2}"><c:set var="classIndex" value="${classIndex+1}" />
		 <div class="listfl"><div class="helpImg"><img class="toolTipImg" title="Note::The crm contact will be not show on Dialing Export." src="../graphics/question.gif" /></div><fmt:message key="doNotCall" />:</div>
		 <div class="listp">
		 <!-- input field -->
                <form:checkbox path="crmContact.doNotCall" />
         <!-- end input field -->   	
		 </div>
		 </div>
		 
		 <div class="list${classIndex % 2}"><c:set var="classIndex" value="${classIndex+1}" />
		 <div class="listfl"><fmt:message key="dialingNoteHistory" />:</div>
		 <div class="listp" onClick="document.getElementById('information').style.display='block';">
		 View Dialing Note History
		 </div>
		 </div>
		
		 <div class="list${classIndex % 2}" id="information" style="display: none;"><c:set var="classIndex" value="${classIndex+1}" />
		 <div class="listfl"><fmt:message key="dialingNoteHistory" />:</div>
		 <div class="listp">
		 <!-- input field -->
	 	 		<c:forEach items="${crmContactForm.crmContact.dialingNoteHistory}" var="dialingNoteHistory" varStatus="status">
          		<p>
          		Note: <c:out value="${dialingNoteHistory.note}" /> &nbsp; &nbsp; &nbsp; Date: <c:out value="${dialingNoteHistory.created}" /> 
          		</p>
          		</c:forEach>  
	 	 <!-- end input field -->	
	 	 </div>
		 </div>
		 
		 
		 <div class="list${classIndex % 2}"><c:set var="classIndex" value="${classIndex+1}" />
		 <div class="listfl"><fmt:message key="dialingNote" />:</div>
		 <div class="listp">
		 <!-- input field -->
	    		<form:textarea path="crmContact.newDialingNote.note" cols="45" rows="10" cssClass="textfield" htmlEscape="true"/>
	 	 <!-- end input field -->	
	 	 </div>
		 </div>
	</div>

	<div align="left" class="button">
		<sec:authorize ifAnyGranted="ROLE_AEM,ROLE_ADMIN_${prop_site_id},ROLE_CRM">
          <input type="submit" value="<spring:message code="update"/>" />
        </sec:authorize>
        <sec:authorize ifAnyGranted="ROLE_AEM,ROLE_ADMIN_${prop_site_id},ROLE_CRM">  
          <input type="submit" value="<spring:message code="delete"/>" name="_delete" onClick="return confirm('Delete permanently?')" />
        </sec:authorize>
	</div>
</td><td class="boxmidr" ></td></tr>
  <tr><td class="botl"></td><td class="botr"></td></tr>
  </table>
  </div>
</form:form>





<form:form commandName="crmContactForm" action="crmContactForm.jhtm" method="post">
<form:hidden path="customerUpdate" value="true"/>

<!-- main box -->
<div id="mboxfull">
  
  <!-- start table -->
  <table cellpadding="0" cellspacing="0" border="0" class="module" >
  <tr>
    <td class="topl_g">
	  
	  <!-- breadcrumb -->
	  <p class="breadcrumb">
	    <a href="../customers">Customers</a> &gt;
	    <c:if test="${!customerForm.newCustomer}">Edit Customer &gt;</c:if><c:if test="${customerForm.newCustomer}">Add Customer</c:if>  
	    ${customerForm.customer.username}
	  </p>
	  
	  <!-- Error Message -->
  	  <c:if test="${!empty message}">
		  <div class="message"><spring:message code="${message}" /></div>
	  </c:if>
	  <spring:hasBindErrors name="customerForm">
       <span class="error">Please fix all errors!</span>
      </spring:hasBindErrors>

	  <div class="info">
	  <c:choose>
		  <c:when test="${gSiteConfig['gSEARCH_ENGINE_PROSPECT'] and !empty searchEngineProspect}">
		  <div align="left" class="left">
		  <table class="form"><tr>
		  <td>&nbsp;</td>
		  <td align="left" style="width:250px;">
		  	<b><fmt:message key="customerReferal" />:</b>
			  <table>
			  <tr>
			    <td><fmt:message key="referrer"/></td>
			    <td>:&nbsp;&nbsp;</td>
			    <td><c:out value="${searchEngineProspect.referrer}" />
			  </td>
			  <tr>
			    <td><fmt:message key="keyPhrase"/></td>
			    <td>:&nbsp;&nbsp;</td>
			    <td><c:out value="${searchEngineProspect.queryString}" />
			  </td>
			  </tr>
			  </table>
		  </td>
		  </tr></table>
		  </div>
		  </c:when>
		  <c:otherwise>
		  <div align="left">
		  <table class="form"><tr><td style="width:20%;">&nbsp;</td></tr></table>
		  </div>
		  </c:otherwise>
	  </c:choose>

	  </div>
	  
	  </td><td class="topr_g" ></td></tr>
  <tr><td class="boxmidlrg" >

 <!-- tabs -->
 <div id="tab-block-1">
	<h4 title="<fmt:message key="emailAddressAndPassword"/>">Convert Crm Contact to Customer</h4>
	<div>
	<!-- start tab -->
         <c:set var="classIndex" value="0" />
         
	  	<div class="listdivi ln tabdivi"></div>
	  	<div class="listdivi"></div>
	  	
	  	<div class="list${classIndex % 2}"><c:set var="classIndex" value="${classIndex+1}" />
	  	<div class="listfl"><div class="requiredField"><fmt:message key='emailAddress' />:</div></div>
	  	<div class="listp">
	  	<!-- input field -->
			<form:input path="customer.username" maxlength="80" size="40" htmlEscape="true"/>
			<c:if test="${not customerForm.newCustomer}"><a href="email.jhtm?id=${customerForm.customer.id}"><fmt:message key="sendEmail" /></a></c:if>
			<form:errors path="customer.username" cssClass="error" delimiter=", "/>
	    <!-- end input field -->   	
	  	</div>
	  	</div>  	
		<div class="list${classIndex % 2}"><c:set var="classIndex" value="${classIndex+1}" />
	  	<div class="listfl"><div class="requiredField"><fmt:message key='newPassword' />:</div></div>
	  	<div class="listp">
	  	<!-- input field -->
			<form:password path="customer.password" value="${customerForm.customer.password}"/>
			<form:errors path="customer.password" cssClass="error" />
	    <!-- end input field -->   	
	  	</div>
	  	</div>  
	  	
	  	<div class="list${classIndex % 2}"><c:set var="classIndex" value="${classIndex+1}" />
	  	<div class="listfl"><div class="requiredField"><fmt:message key='firstName' />:</div></div>
	  	<div class="listp">
	  	<!-- input field -->
            <form:input path="customer.address.firstName"  htmlEscape="true" />
            <form:errors path="customer.address.firstName" cssClass="error" />
	    <!-- end input field -->   	
	  	</div>
	  	</div>
	  	
	  	<div class="list${classIndex % 2}"><c:set var="classIndex" value="${classIndex+1}" />
		<div class="listfl"><div class="requiredField"><fmt:message key='lastName' />:</div></div>
		<div class="listp">
		<!-- input field -->
            <form:input path="customer.address.lastName" htmlEscape="true" />
            <form:errors path="customer.address.lastName" cssClass="error" />
		<!-- end input field -->   	
		</div>
		</div>
		<div class="list${classIndex % 2}"><c:set var="classIndex" value="${classIndex+1}" />
	    <div class="listfl"><div class="requiredField"><fmt:message key='country' />:</div></div>
		<div class="listp">
		<!-- input field -->
            <form:select id="country" path="customer.address.country" onchange="toggleStateProvince2(this)">
             <form:option value="" label="Please Select"/>
            <form:options items="${countries}" itemValue="code" itemLabel="name"/>
            </form:select>
            <form:errors path="customer.address.country" cssClass="error" />
		 <!-- end input field -->   	
		 </div>
		 </div>
		
		 <div class="list${classIndex % 2}"><c:set var="classIndex" value="${classIndex+1}" />
         <div class="listfl"><div class="requiredField"><fmt:message key="zipCode" />:</div></div>
		 <div class="listp">
		 <!-- input field -->
		 	 <form:input id="automate" path="customer.address.zip" htmlEscape="true"  onkeyup="automateCityState(document.getElementById('automate').value)"/>  
		 	 <form:input id="noautomate" path="customer.address.zip" htmlEscape="true" />  
             <form:errors path="customer.address.zip" cssClass="error" />
         <!-- end input field -->
		 </div>
		 </div>
		 
		 <div class="list${classIndex % 2}"><c:set var="classIndex" value="${classIndex+1}" />
         <div class="listfl"><div <c:if test="${siteConfig['CUSTOMERS_REQUIRED_COMPANY'].value == 'true'}">class="requiredField"</c:if>><fmt:message key="company" />:</div></div>
		 <div class="listp">
		 <!-- input field -->
              <form:input path="customer.address.company"  htmlEscape="true" />
              <form:errors path="customer.address.company" cssClass="error" />
         <!-- end input field -->   	
		 </div>
		 </div>

	  	<div class="list${classIndex % 2}"><c:set var="classIndex" value="${classIndex+1}" />
		<div class="listfl"><div class="requiredField"><fmt:message key="address" /> 1:</div></div>
		<div class="listp">
		<!-- input field -->
            <form:input path="customer.address.addr1" htmlEscape="true" />
            <form:errors path="customer.address.addr1" cssClass="error" />
		<!-- end input field -->   	
		</div>
		</div>

	  	<div class="list${classIndex % 2}"><c:set var="classIndex" value="${classIndex+1}" />
		<div class="listfl"><fmt:message key="address" /> 2:</div>
		<div class="listp">
		<!-- input field -->
            <form:input path="customer.address.addr2" htmlEscape="true" />
            <form:errors path="customer.address.addr2" cssClass="error" />
		<!-- end input field -->   	
		</div>
		</div>

         <div class="list${classIndex % 2}"><c:set var="classIndex" value="${classIndex+1}" />
         <div class="listfl"><div class="requiredField"><fmt:message key="city" />:</div></div>
		 <div class="listp">
		 <!-- input field -->
             <form:input path="customer.address.city" htmlEscape="true" cssClass="highlight"/>
             <form:errors path="customer.address.city" cssClass="error" />
         <!-- end input field -->   	
		 </div>
		 </div>

         <div class="list${classIndex % 2}"><c:set var="classIndex" value="${classIndex+1}" /> 
         <div class="listfl"><div class="requiredField"><fmt:message key="stateProvince" />:</div></div>
		 <div class="listp">
		 <!-- input field -->
	      <table cellspacing="0" cellpadding="0">
	       <tr>
	       <td>
	         <form:select id="state" path="customer.address.stateProvince" cssClass="highlight">
	           <form:option value="" label="Please Select"/>
	           <form:options items="${states}" itemValue="code" itemLabel="name"/>
	         </form:select>
	         <form:select id="ca_province" path="customer.address.stateProvince">
	           <form:option value="" label="Please Select"/>
	           <form:options items="${caProvinceList}" itemValue="code" itemLabel="name"/>
	         </form:select>
	         <form:input id="province" path="customer.address.stateProvince" htmlEscape="true"/>
	       </td>
	       <td><div id="stateProvinceNA">&nbsp;<form:checkbox path="customer.address.stateProvinceNA" id="addressStateProvinceNA" value="true" /> Not Applicable</div></td>
	       <td>&nbsp;</td>
	       <td>
	         <form:errors path="customer.address.stateProvince" cssClass="error" />
	       </td>
	       </tr>
	      </table>
         <!-- end input field -->   	
		 </div>
		 </div> 
		 
		 <c:if test="${siteConfig['ADDRESS_RESIDENTIAL_COMMERCIAL'].value >= 2}">
         <div class="list${classIndex % 2}"><c:set var="classIndex" value="${classIndex+1}" />
         <div class="listfl"><fmt:message key="deliveryType" />:</div>
		 <div class="listp">
		 <!-- input field -->
              <form:radiobutton path="customer.address.residential" value="true"/>: <fmt:message key="residential" /><br /><form:radiobutton path="customer.address.residential" value="false"/>: <fmt:message key="commercial" />
         <!-- end input field -->   	
		 </div>
		 </div>
		 
		 <c:if test="${siteConfig['SHIPPING_LTL'].value == 'true'}">
		 <div class="list${classIndex % 2}"><c:set var="classIndex" value="${classIndex+1}" />
		 <div class="listfl"><div class="helpImg"><img class="toolTipImg" title="LTL::" src="../graphics/question.gif" /></div><fmt:message key="liftGateDelivery" />:</div>
		 <div class="listp">
		 <!-- input field -->
              <form:checkbox path="customer.address.liftGate" value="true"/>
         <!-- end input field -->   	
		 </div>
		 </div>
		 </c:if>
		 </c:if>

         <div class="list${classIndex % 2}"><c:set var="classIndex" value="${classIndex+1}" />
         <div class="listfl"><div class="requiredField"><fmt:message key="phone" />:</div></div>
		 <div class="listp">
		 <!-- input field -->
             <form:input path="customer.address.phone" htmlEscape="true" cssErrorClass="errorField" id="phone_num"/>
	  		 <form:input path="customer.address.phone"  onkeydown="javascript:backspacerDOWN(this,event);" onkeyup="javascript:backspacerUP(this,event);" id="us_phone_num"/>    
             <form:errors path="customer.address.phone" cssClass="error" />
             <span class="helpNote"><p>US: xxx-xxx-xxxx or (xxx) xxx-xxxx Ext-xxxxx</p></span>
         <!-- end input field -->   	
		 </div>
		 </div>
         
         <div class="list${classIndex % 2}"><c:set var="classIndex" value="${classIndex+1}" /> 
         <div class="listfl"><fmt:message key="cellPhone" />:</div>
		 <div class="listp">
		 <!-- input field -->
             <form:input path="customer.address.cellPhone" htmlEscape="true" id="cell_phone_num"/>
      		 <form:input path="customer.address.cellPhone"  onkeydown="javascript:backspacerDOWN(this,event);" onkeyup="javascript:backspacerUP(this,event);" id="us_cell_phone_num"/>  	  
             <form:errors path="customer.address.cellPhone" cssClass="error" />
         <!-- end input field -->   	
		 </div>
		 </div>
		 
		<div class="list${classIndex % 2}" id="mobileCarrierIdSelect" style="display:none;"><c:set var="classIndex" value="${classIndex+1}" />
	    <div class="listfl" ><fmt:message key='mobileCarrier' />:</div>
		<div class="listp">
		<!-- input field -->
            <form:select id="mobileCarrierId" path="customer.address.mobileCarrierId">
             <form:option value="" label="Please Select"/>
            <form:options items="${mobileCarriers}" itemValue="id" itemLabel="carrier"/>
            </form:select>
            <form:errors path="customer.address.mobileCarrierId" cssClass="error" />
		 <!-- end input field -->   	
		 </div>
		 </div>
		 
	  	<div class="list${classIndex % 2}"><c:set var="classIndex" value="${classIndex+1}" />
	  	<div class="listfl"><div class="helpImg"><img class="toolTipImg" title="Disclaimer::By checking this box, you authorize Via Trading Corporation to send you alerts regarding Events, New Product Listings and/or promotions by text message.
	  	Standard text messaging rates may apply. Please check with your carrier to see exact costs involved in receiving text messages if any.Notification preferences can be modified at any time by logging in to 'My Account' Section, calling or emailing us" 
	  	src="../graphics/question.gif" /></div><fmt:message key="textMessage" /> <fmt:message key="notification" />:</div>
	  	<div class="listp">
	  	<!-- input field -->
			<form:checkbox path="customer.textMessageNotify"/>
	    <!-- end input field -->   	
	  	</div>
	  	</div>
		 
         <div class="list${classIndex % 2}"><c:set var="classIndex" value="${classIndex+1}" />
         <div class="listfl"><fmt:message key="fax" />:</div>
		 <div class="listp">
		 <!-- input field -->
              <form:input path="customer.address.fax"  htmlEscape="true" />
              <form:errors path="customer.address.fax" cssClass="error" />
         <!-- end input field -->   	
		 </div>
		 </div>
		 
  		 <c:if test="${languageCodes != null}">
         <div class="list${classIndex % 2}"><c:set var="classIndex" value="${classIndex+1}" />
	     <div class="listfl"><fmt:message key='language' />:</div>
		 <div class="listp">
		 <!-- input field -->
       		<form:select path="customer.languageCode">
        	  <form:option value="en"><fmt:message key="language_en"/></form:option>        	  
        	  <c:forEach items="${languageCodes}" var="i18n">
          		<form:option value="${i18n.languageCode}"><fmt:message key="language_${i18n.languageCode}"/></form:option>
          		</c:forEach>
	  		</form:select>
 		 <!-- end input field -->   	
		 </div>
		 </div>
		 </c:if>

		<div class="list${classIndex % 2}"><c:set var="classIndex" value="${classIndex+1}" />
	  	<div class="listfl"><div class="helpImg"><img class="toolTipImg" title="Note::Notify customer for new orders." src="../graphics/question.gif" /></div><fmt:message key="email" /> <fmt:message key="notification" />:</div>
	  	<div class="listp">
	  	<!-- input field -->
			<form:checkbox path="customer.emailNotify"/>
	    <!-- end input field -->   	
	  	</div>
	  	</div>

	  	<div class="list${classIndex % 2}"><c:set var="classIndex" value="${classIndex+1}" />
	  	<div class="listfl"><div class="helpImg"><img class="toolTipImg" title="Note::Customer Rating 1" src="../graphics/question.gif" /></div> <fmt:message key="rating" /> 1:</div>
	  	<div class="listp">
	  	<!-- input field -->
	  	<form:select path="customer.rating1">
	  		<form:option value=""></form:option>
	  		<c:forTokens items="${siteConfig['CUSTOMER_RATING_1'].value}" delims="," var="type" varStatus="status">
				<form:option value ="${type}"><c:out value ="${type}" /></form:option>
		  	</c:forTokens>
		</form:select>
	    <!-- end input field -->   	
	  	</div>
	  	</div>	  	
	  	
	  	<div class="list${classIndex % 2}"><c:set var="classIndex" value="${classIndex+1}" />
	  	<div class="listfl"><div class="helpImg"><img class="toolTipImg" title="Note::Customer Rating 2." src="../graphics/question.gif" /></div> <fmt:message key="rating" /> 2:</div>
	  	<div class="listp">
	  	<!-- input field -->
		<form:select path="customer.rating2">
	  		<form:option value=""></form:option>
	  		<c:forTokens items="${siteConfig['CUSTOMER_RATING_2'].value}" delims="," var="type" varStatus="status">
				<form:option value ="${type}"><c:out value ="${type}" /></form:option>
		  	</c:forTokens>
		</form:select>
	    <!-- end input field -->   	
	  	</div>
	  	</div>
	  	
	  	<div class="list${classIndex % 2}"><c:set var="classIndex" value="${classIndex+1}" />
		 <div class="listfl"><fmt:message key="note" />:</div>
		 <div class="listp">
		 <!-- input field -->
             <form:textarea path="customer.note" rows="15" cols="60" cssClass="textfield" htmlEscape="true" />
			 <div style="text-align:left" id="customer.note_link"><a href="#" onClick="loadEditor('customer.note')" class="htmleditor">- <fmt:message key="htmlEditor" /> -</a></div>
		 <!-- end input field -->   	
		 </div>
		 </div>
		 
		<div class="list${classIndex % 2}"><c:set var="classIndex" value="${classIndex+1}" />
		<div class="listfl"><div class="helpImg"><img class="toolTipImg" title="Note::This customer will not be shown on dialing export." src="../graphics/question.gif" /></div><fmt:message key="doNotCall" />:</div>
		<div class="listp">
		<!-- input field -->
            <form:checkbox path="customer.doNotCall" />
        <!-- end input field -->   	
		</div>
		</div>
		 
		 <div class="list${classIndex % 2}"><c:set var="classIndex" value="${classIndex+1}" />
		 <div class="listfl"><fmt:message key="dialingNoteHistory" />:</div>
		 <div class="listp" onClick="document.getElementById('information2').style.display='block';">
		 View Dialing Note History
		 </div>
		 </div>
		 
		 <div class="list${classIndex % 2}" id="information2" style="display: none;"><c:set var="classIndex" value="${classIndex+1}" />
		 <div class="listfl"><fmt:message key="dialingNoteHistory" />:</div>
		 <div class="listp">
		 <!-- input field -->
         		<c:forEach items="${crmContactForm.customer.dialingNoteHistory}" var="dialingNoteHistory" varStatus="status">
          		<p>
          		Note: <c:out value="${dialingNoteHistory.note}" /> &nbsp; &nbsp; &nbsp; Date: <c:out value="${dialingNoteHistory.created}" /> 
          		</p>
          		</c:forEach>    
         <!-- end input field -->   	
		 </div>
		 </div>
		 
		 <div class="list${classIndex % 2}"><c:set var="classIndex" value="${classIndex+1}" />
		 <div class="listfl"><fmt:message key="dialingNote" />:</div>
		 <div class="listp">
		 <!-- input field -->
             <form:textarea path="customer.newDialingNote.note" rows="15" cols="60" cssClass="textfield" htmlEscape="true" />
			 <div style="text-align:left" id="customer.newDialingNote.note_link"><a href="#" onClick="loadEditor('customer.note')" class="htmleditor">- <fmt:message key="htmlEditor" /> -</a></div>
		 <!-- end input field -->   	
		 </div>
		 </div> 
        
  		 <c:if test="${gSiteConfig['gSHOPPING_CART'] == true and gSiteConfig['gSALES_REP'] == true}">
	  	<div class="list${classIndex % 2}"><c:set var="classIndex" value="${classIndex+1}" />
   		<div class="listfl"><fmt:message key="salesRep" />:</div>
		<div class="listp">
		<!-- input field -->
	        <form:select path="customer.salesRepId" onchange="changeHref(${crmContactForm.customer.id},this.value);">
	        <form:option value=""><fmt:message key="none" /></form:option>
	        <c:forEach items="${salesReps}" var="salesRep">
	  	        <form:option value="${salesRep.id}"><c:out value="${salesRep.name}" /></form:option>
		      </c:forEach>
	        </form:select>
	        <c:if test="${not crmContactForm.newCustomer}"><a id="emailSalesRep" href="email.jhtm?contactId=${crmContactForm.crmContact.id}&salesrepid=${crmContactForm.customer.salesRepId}"><fmt:message key="sendEmail" /></a></c:if>
        <!-- end input field -->   	
		</div>
		</div>
        </c:if> 
	  	
		 
		<c:forEach items="${contactFields}" var="field" varStatus="status">
        <c:if test="${field.globalName == 'Field 1'}">
        <div class="list${classIndex % 2}"><c:set var="classIndex" value="${classIndex+1}" />
         <div class="listfl">How did you hear about us?:</div>
		 <div class="listp">
		 <!-- input field -->
             <form:select path="customer.field1">
		 	 <form:option value="" label="Please Select"></form:option>
		 	 <c:forTokens items="${field.options}" delims="," var="option">
		 	   <form:option value="${fn:trim(option)}" label="${option}"></form:option>
		 	 </c:forTokens>
		 	 </form:select>
         <!-- end input field -->   	
		 </div>
		 </div>
        </c:if>
        
        <c:if test="${field.globalName == 'Field 2'}">
        <div class="list${classIndex % 2}"><c:set var="classIndex" value="${classIndex+1}" />
         <div class="listfl">Main Type of Business :</div>
		 <div class="listp">
		 <!-- input field -->
             <form:select path="customer.field2">
		 	 <form:option value="" label="Please Select"></form:option>
		 	 <c:forTokens items="${field.options}" delims="," var="option">
		 	   <form:option value="${fn:trim(option)}" label="${option}"></form:option>
		 	 </c:forTokens>
		 	 </form:select>
         <!-- end input field -->   	
		 </div>
		 </div>
        </c:if>
        
        <c:if test="${field.globalName == 'Field 3'}">
        <div class="list${classIndex % 2}"><c:set var="classIndex" value="${classIndex+1}" />
         <div class="listfl">
		Do you have an Account Manager/ Have you spoken with anyone? If so, please select their name.:</div>
		 <div class="listp">
		 <!-- input field -->
            <form:select path="customer.field3">
		 	<form:option value="" label="Please Select"></form:option>
		 	<c:forTokens items="${field.options}" delims="," var="option">
		 	  <form:option value="${fn:trim(option)}" label="${option}"></form:option>
		 	</c:forTokens>
		 	</form:select>
         <!-- end input field -->   	
		 </div>
		 </div>
        </c:if>
        
        <c:if test="${field.globalName == 'Field 4'}">
        <div class="list${classIndex % 2}"><c:set var="classIndex" value="${classIndex+1}" />
         <div class="listfl">Have you purchased from Via Trading before?:</div>
		 <div class="listp">
		 <!-- input field -->
             <form:select path="customer.field4">
		 	  <form:option value="" label="Please Select"></form:option>
		 	  <c:forTokens items="${field.options}" delims="," var="option">
		 	    <form:option value="${fn:trim(option)}" label="${option}"></form:option>
		 	  </c:forTokens>
		 	  </form:select>
         <!-- end input field -->   	
		 </div>
		 </div>
		 </c:if>
		 </c:forEach>

</div>           	
<!-- end tabs -->

			
</div>

<!-- start button -->
<div align="left" class="button"> 

  <sec:authorize ifAnyGranted="ROLE_AEM,ROLE_ADMIN_${prop_site_id},ROLE_CUSTOMER_CREATE">
    <input type="submit" value="<spring:message code="customerAdd"/>" />
  </sec:authorize> 

</div>
<!-- end button -->	   

  <!-- end table -->
    </td><td class="boxmidr" ></td></tr>
  <tr><td class="botl"></td><td class="botr"></td></tr>
  </table>
  
<!-- end main box -->  
</div>                   	
</form:form>


<!-- start button -->

<script language="JavaScript">
<!--
function toggleStateProvince(el) {
	  if (el.value == "US") {
		  document.getElementById('phone1_num').disabled=true;
	      document.getElementById('phone1_num').style.display="none";
	      document.getElementById('us_phone1_num').disabled=false;
	      document.getElementById('us_phone1_num').style.display="block";
	      document.getElementById('phone2_num').disabled=true;
	      document.getElementById('phone2_num').style.display="none";
	      document.getElementById('us_phone2_num').disabled=false;
	      document.getElementById('us_phone2_num').style.display="block";
	  } else if (el.value == "CA") {
		  document.getElementById('us_phone1_num').disabled=true;
	      document.getElementById('us_phone1_num').style.display="none";
	      document.getElementById('phone1_num').disabled=false;
	      document.getElementById('phone1_num').style.display="block";
	      document.getElementById('us_phone2_num').disabled=true;
	      document.getElementById('us_phone2_num').style.display="none";
	      document.getElementById('phone2_num').disabled=false;
	      document.getElementById('phone2_num').style.display="block";
	  } else {
		  document.getElementById('us_phone1_num').disabled=true;
	      document.getElementById('us_phone1_num').style.display="none";
	      document.getElementById('phone1_num').disabled=false;
	      document.getElementById('phone1_num').style.display="block";
	      document.getElementById('us_phone2_num').disabled=true;
	      document.getElementById('us_phone2_num').style.display="none";
	      document.getElementById('phone2_num').disabled=false;
	      document.getElementById('phone2_num').style.display="block";
	  }
}
toggleStateProvince(document.getElementById('country'));

</script>
<script language="JavaScript">
<!--
function toggleStateProvince2(el) {
	if (el.value == "US") {
	      document.getElementById('state').disabled=false;
	      document.getElementById('state').style.display="block";
	      document.getElementById('ca_province').disabled=true;
	      document.getElementById('ca_province').style.display="none";
	      document.getElementById('province').disabled=true;
	      document.getElementById('province').style.display="none";
	      document.getElementById('addressStateProvinceNA').disabled=true;
	      document.getElementById('stateProvinceNA').style.display="none";
	      document.getElementById('automate').disabled=false;
	      document.getElementById('automate').style.display="block";
	      document.getElementById('noautomate').disabled=true;
	      document.getElementById('noautomate').style.display="none";
	      document.getElementById('phone_num').disabled=true;
	      document.getElementById('phone_num').style.display="none";
	      document.getElementById('us_phone_num').disabled=false;
	      document.getElementById('us_phone_num').style.display="block";
	      document.getElementById('cell_phone_num').disabled=true;
	      document.getElementById('cell_phone_num').style.display="none";
	      document.getElementById('us_cell_phone_num').disabled=false;
	      document.getElementById('us_cell_phone_num').style.display="block";
	      document.getElementById('mobileCarrierIdSelect').disabled=false;
	      document.getElementById('mobileCarrierIdSelect').style.display="block";
	  } else if (el.value == "CA") {
	      document.getElementById('state').disabled=true;
	      document.getElementById('state').style.display="none";
	      document.getElementById('ca_province').disabled=false;
	      document.getElementById('ca_province').style.display="block";
	      document.getElementById('province').disabled=true;
	      document.getElementById('province').style.display="none";
	      document.getElementById('addressStateProvinceNA').disabled=true;
	      document.getElementById('stateProvinceNA').style.display="none";
	      document.getElementById('automate').disabled=true;
	      document.getElementById('automate').style.display="none";
	      document.getElementById('noautomate').disabled=false;
	      document.getElementById('noautomate').style.display="block";
	      document.getElementById('us_phone_num').disabled=true;
	      document.getElementById('us_phone_num').style.display="none";
	      document.getElementById('phone_num').disabled=false;
	      document.getElementById('phone_num').style.display="block";
	      document.getElementById('us_cell_phone_num').disabled=true;
	      document.getElementById('us_cell_phone_num').style.display="none";
	      document.getElementById('cell_phone_num').disabled=false;
	      document.getElementById('cell_phone_num').style.display="block";
	      document.getElementById('mobileCarrierIdSelect').disabled=true;
	      document.getElementById('mobileCarrierIdSelect').style.display="none";
	  } else {
	      document.getElementById('state').disabled=true;
	      document.getElementById('state').style.display="none";
	      document.getElementById('ca_province').disabled=true;
	      document.getElementById('ca_province').style.display="none";
	      document.getElementById('province').disabled=false;
	      document.getElementById('province').style.display="block";
	      document.getElementById('addressStateProvinceNA').disabled=false;
	      document.getElementById('stateProvinceNA').style.display="block";
	      document.getElementById('automate').disabled=true;
	      document.getElementById('automate').style.display="none";
	      document.getElementById('noautomate').disabled=false;
	      document.getElementById('noautomate').style.display="block";
	      document.getElementById('us_phone_num').disabled=true;
	      document.getElementById('us_phone_num').style.display="none";
	      document.getElementById('phone_num').disabled=false;
	      document.getElementById('phone_num').style.display="block";
	      document.getElementById('us_cell_phone_num').disabled=true;
	      document.getElementById('us_cell_phone_num').style.display="none";
	      document.getElementById('cell_phone_num').disabled=false;
	      document.getElementById('cell_phone_num').style.display="block";
	      document.getElementById('mobileCarrierIdSelect').disabled=true;
	      document.getElementById('mobileCarrierIdSelect').style.display="none";
	  }
}
toggleStateProvince2(document.getElementById('country'));
//-->
</script>
</sec:authorize>
</tiles:putAttribute>
</tiles:insertDefinition>