<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/security/tags" prefix="sec" %>


<tiles:insertDefinition name="admin.crm.contact" flush="true">
  <tiles:putAttribute name="content" type="string">
  
<sec:authorize ifAnyGranted="ROLE_AEM,ROLE_ADMIN_${prop_site_id},ROLE_CRM_FORM">
<script type="text/javascript" src="../../admin/javascript/multiple.open.accordion1.2.js"></script>
<script type="text/javascript">
window.addEvent('domready', function(){
	var box1 = new multiBox('mbForm', {descClassName: 'descClassName',showNumbers: false,showControls: false,overlay: new overlay()});
});
</script>
<form action="crmFormList.jhtm" method="post" id="list" name="list_form">

<!-- main box -->
  <div id="mboxfull">
  
  <!-- start table -->
  <table cellpadding="0" cellspacing="0" border="0" class="module" >
  <tr>
    <td class="topl_g">
	  
	  <!-- breadcrumb -->
	  <p class="breadcrumb">
	    <a href="../crm">CRM</a> &gt;
	    Forms
	  </p>
	  
	  <!-- Error Message -->
	  
	  <!-- header image -->
	  <img class="headerImage" src="../graphics/form.gif" />
	  
	  </td><td class="topr_g" ></td></tr>
  <tr><td class="boxmidlrg" >

 <!-- tabs -->
 <div class="tab-wrapper">
	<h4 title="List of affiliates"></h4>
	<div>
	<!-- start tab -->
	
	  	<div class="listdivi ln tabdivi"></div>
	  	<div class="listdivi"></div>
	   
		  	<div class="listlight">
		  	<!-- input field --> 
		  	<table border="0" cellpadding="0" cellspacing="1" width="100%">
			  <tr>
			  <c:if test="${model.formList.nrOfElements > 0}">
			  <td class="pageShowing">
			  <fmt:message key="showing">
				<fmt:param value="${model.formList.firstElementOnPage + 1}"/>
				<fmt:param value="${model.formList.lastElementOnPage + 1}"/>
				<fmt:param value="${model.formList.nrOfElements}"/>
			  </fmt:message>
			  </td>
			  </c:if>
			  <td class="pageNavi">
			  Page
			  <select name="page" id="page" onchange="submit()">
			  <c:forEach begin="1" end="${model.formList.pageCount}" var="page">
			  	<option value="${page}" <c:if test="${page == (model.formList.page+1)}">selected</c:if>>${page}</option>
			  </c:forEach>
			  </select>
			  of <c:out value="${model.formList.pageCount}"/>
			  | 
			  <c:if test="${model.formList.firstPage}"><span class="pageNaviDead">previous</span></c:if>
			  <c:if test="${not model.formList.firstPage}"><a href="<c:url value="crmFormList.jhtm"><c:param name="page" value="${model.formList.page}"/></c:url>" class="pageNaviLink">previous</a></c:if>
			  | 
			  <c:if test="${model.formList.lastPage}"><span class="pageNaviDead">next</span></c:if>
			  <c:if test="${not model.formList.lastPage}"><a href="<c:url value="crmFormList.jhtm"><c:param name="page" value="${model.formList.page+2}"/></c:url>" class="pageNaviLink">next</a></c:if>
			  </td>
			  </tr>
			</table>
			
			<table border="0" cellpadding="0" cellspacing="0" width="100%" class="listings">
			  <tr class="listingsHdr2">
			    <td class="indexCol">&nbsp;</td>
			    <td class="listingsHdr3">
			      <table cellspacing="0" cellpadding="1">
			    	  <tr>
			    	    <c:choose>
			    	      <c:when test="${crmFormSearch.sort == 'form_name desc'}">
			    	        <td class="listingsHdr3">
			    	        <a class="hdrLink" href="#" onClick="document.getElementById('sort').value='form_name';document.getElementById('list').submit()"><fmt:message key="form" /> <fmt:message key="name" /></a>
			    	        </td>
			    	        <td><img src="../graphics/up.gif" border="0"></td>
			    	      </c:when>
			    	      <c:when test="${crmFormSearch.sort == 'form_name'}">
			    	        <td class="listingsHdr3">
			    	        <a class="hdrLink" href="#" onClick="document.getElementById('sort').value='form_name desc';document.getElementById('list').submit()"><fmt:message key="form" /> <fmt:message key="name" /></a>
			    	        </td>
			    	        <td><img src="../graphics/down.gif" border="0"></td>
			    	      </c:when>
			    	      <c:otherwise>
			    	        <td colspan="2" class="listingsHdr3">
			    	        <a class="hdrLink" href="#" onClick="document.getElementById('sort').value='form_name desc';document.getElementById('list').submit()"><fmt:message key="form" /> <fmt:message key="name" /></a>
			    	        </td>
			    	      </c:otherwise>
			    	    </c:choose>
			    	  </tr>
			      </table>
			    </td>
			    <td class="listingsHdr3"><fmt:message key="htmlCode" /></td>
			    <td class="listingsHdr3"><fmt:message key="preview" /></td>
			  </tr>
			<c:forEach items="${model.formList.pageList}" var="form" varStatus="status">
			  <tr class="row${status.index % 2}" onmouseover="changeStyleClass(this,'rowOver')" onmouseout="changeStyleClass(this,'row${status.index % 2}')">
			    <td align="left"><input name="__selected_id" value="${form.formId}" type="checkbox"></td>
			    <td class="nameCol"><a href="crmForm.jhtm?id=${form.formId}"><c:out value="${form.formName}"/></a></td>
			    <td class="nameCol"><a href="crmFormHtmlCode.jhtm?id=${form.formId}" rel="width:850,height:400" id="mb${form.formId}" class="mbForm" title="<fmt:message key="form" /># : ${form.formId}"><img src="../graphics/magnifier.gif" border="0"></a></td>
			    <td class="nameCol"><a href="crmFormView.jhtm?id=${form.formId}" rel="width:850,height:400" id="mb${form.formId}" class="mbForm" title="<fmt:message key="form" /># : ${form.formId}"><img src="../graphics/magnifier.gif" border="0"></a></td>
			  </tr>
			</c:forEach>
			<c:if test="${model.count == 0}">
			  <tr class="emptyList"><td colspan="3">&nbsp;</td></tr>
			</c:if>
			</table>
			
			<table border="0" cellpadding="0" cellspacing="1" width="100%">
			  <tr>
			  <td class="pageSize">
			  <select name="size" onchange="document.getElementById('page').value=1;submit()">
			  <c:forTokens items="10,25,50" delims="," var="current">
			  	  <option value="${current}" <c:if test="${current == crmFormSearch.pageSize}">selected</c:if>>${current} <fmt:message key="perPage" /></option>
			  </c:forTokens>
			  </select>
			  </td>
			  </tr>
			</table>
		  	<!-- end input field -->  	  
		  	
		  	<!-- start button --> 
		  	<div align="left" class="button">
		  	  <sec:authorize ifAnyGranted="ROLE_AEM,ROLE_ADMIN_${prop_site_id},ROLE_CRM">
		  	    <input type="submit" name="__add" value="<fmt:message key="add" />">
		  	  </sec:authorize>
			</div>
			<!-- end button -->	
			</div>
	
	<!-- end tab -->        
	</div>
<!-- end tabs -->			
</div>
  
  <!-- end table -->
   </td><td class="boxmidr" ></td></tr>
  <tr><td class="botl"></td><td class="botr"></td></tr>
  </table>
  
<!-- end main box -->  
</div>
</form> 
</sec:authorize>

  </tiles:putAttribute>    
</tiles:insertDefinition>