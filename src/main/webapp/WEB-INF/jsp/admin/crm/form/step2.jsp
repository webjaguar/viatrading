<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="/WEB-INF/tlds/spring.tld" prefix="spring" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ taglib uri="http://www.springframework.org/security/tags" prefix="sec" %>

<tiles:insertDefinition name="admin.crm.contact" flush="true">
  <tiles:putAttribute name="content" type="string">
<sec:authorize ifAnyGranted="ROLE_AEM,ROLE_ADMIN_${prop_site_id},ROLE_CRM_FORM">
<script language="JavaScript"> 
<!--
window.addEvent('domready', function(){			
	var tabs1 = new SimpleTabs($('tab-block-1'), { selector: 'h4'});
	var Tips1 = new Tips($$('.toolTipImg'));
});
//--> 
</script> 
<form:form commandName="crmFormBuilderForm" method="post">

<!-- main box -->
  <div id="mboxfull">
  
  <!-- start table -->
  <table cellpadding="0" cellspacing="0" border="0" class="module" >
  <tr>
    <td class="topl_g">
	  
	  <!-- breadcrumb -->
	  <p class="breadcrumb">
	    <a href="../crm">CRM</a> &gt;
	    <a href="../crm/crmFormList.jhtm">Form</a> &gt;
	    Step 3 of 5 
	  </p>
	  
	  <!-- Error Message -->
	  <spring:hasBindErrors name="crmFormBuilderForm">
		<div class="message">Please fix all errors!</div>
	  </spring:hasBindErrors>

  </td><td class="topr_g" ></td></tr>
  <tr><td class="boxmidlrg" >

 <!-- tabs -->
 <div id="tab-block-1">
	<h4 title="<fmt:message key="field"/>">Fields Options</h4>
	<div>
	<!-- start tab -->
	    <div class="listdivi ln tabdivi"></div>
		<table border="0" cellpadding="0" cellspacing="0" width="100%" class="listings">
	      <tr class="listingsHdr2">
		    <td class="indexCol">&nbsp;</td>
			<td class="listingsHdr3"><fmt:message key="number" /></td>
			<td class="listingsHdr3"><fmt:message key="fieldName" /></td>
			<td class="listingsHdr3"><fmt:message key="options" /></td>
		  </tr>
		  <c:forEach items="${crmFormBuilderForm.crmForm.crmFields}"  var="field" varStatus="status">
		  <c:if test="${status.index lt crmFormBuilderForm.crmForm.numberOfFields}">
		  <tr class="row${status.index % 2}" onmouseover="changeStyleClass(this,'rowOver')" onmouseout="changeStyleClass(this,'row${status.index % 2}')">
			<td align="left">
			  <c:if test="${field.type gt 2}">
			  <div class="helpImg"><img class="toolTipImg" title="Note:: Separate options with comma. " src="../graphics/question.gif" /></div>
			  </c:if>
			  <input type="hidden" name="fieldName${status.index}" value="${field.fieldName}"/>
			</td>
			<td class="nameCol"><c:out value="${status.index + 1}"/></td>
			<td class="nameCol"><c:out value="${field.fieldName}"/></td>
			<td class="nameCol">
			  <c:if test="${field.type gt 2 and field.type lt 6}">
			    <form:textarea path="crmForm.crmFields[${status.index}].options"/>
			    <form:errors path="crmForm.crmFields[${status.index}].options" cssClass="error"/>
	 	 	  </c:if>
			</td>
		  </tr>
		  </c:if>
		  </c:forEach>
		</table>
			
	<!-- end tab -->        
	</div> 
</div>	
	

<!-- start button -->
	<div align="left" class="button"> 
		<input type="submit" value="<fmt:message key="prevStep" />" name="_target1"> &nbsp;
		<input type="submit" value="<fmt:message key="nextStep" />" name="_target3"> &nbsp;
		<c:if test="${!crmFormBuilderForm.newCrmForm}">
			<input type="submit" value="<spring:message code="delete"/>" name="_target5" onClick="return confirm('Delete permanently?')" />
        </c:if>
	</div>
<!-- end button -->
  
  <!-- end table -->
    </td><td class="boxmidr" ></td></tr>
  <tr><td class="botl"></td><td class="botr"></td></tr>
  </table>
  
<!-- end main box -->  
</div>
</form:form>
</sec:authorize> 

  </tiles:putAttribute>    
</tiles:insertDefinition>