<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="/WEB-INF/tlds/spring.tld" prefix="spring" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ taglib uri="http://www.springframework.org/security/tags" prefix="sec" %>

<tiles:insertDefinition name="admin.crm.contact" flush="true">
  <tiles:putAttribute name="content" type="string">
<sec:authorize ifAnyGranted="ROLE_AEM,ROLE_ADMIN_${prop_site_id},ROLE_CRM_FORM">
<script language="JavaScript"> 
<!--
window.addEvent('domready', function(){			
	var tabs1 = new SimpleTabs($('tab-block-1'), { selector: 'h4'});
	var Tips1 = new Tips($$('.toolTipImg'));
});
//--> 
</script> 
<form:form commandName="crmFormBuilderForm" method="post">

<!-- main box -->
  <div id="mboxfull">
  
  <!-- start table -->
  <table cellpadding="0" cellspacing="0" border="0" class="module" >
  <tr>
    <td class="topl_g">
	  
	  <!-- breadcrumb -->
	  <p class="breadcrumb">
	    <a href="../crm">CRM</a> &gt;
	    <a href="../crm/crmFormList.jhtm">Form</a> &gt;
	    Step 2 of 5 
	  </p>
	  
	  <!-- Error Message -->
	  <spring:hasBindErrors name="crmFormBuilderForm">
		<div class="message">Please fix all errors!  <form:errors path="crmForm.crmFields" cssClass="error" /></div>
	  </spring:hasBindErrors>

  </td><td class="topr_g" ></td></tr>
  <tr><td class="boxmidlrg" >

 <!-- tabs -->
 <div id="tab-block-1">
	<h4 title="<fmt:message key="field"/>">Fields Details</h4>
	<div>
	<!-- start tab -->
	    <div class="listdivi ln tabdivi"></div>
		<table border="0" cellpadding="0" cellspacing="0" width="100%" class="listings">
			  <tr class="listingsHdr2">
			    <td class="indexCol">&nbsp;</td>
			    <td class="listingsHdr3"><fmt:message key="number" /></td>
			    <td class="listingsHdr3"><fmt:message key="fieldName" /></td>
			    <td class="listingsHdr3"><fmt:message key="mapTo" /></td>
			    <td class="listingsHdr3"><fmt:message key="fieldType" /></td>
			    <td class="listingsHdr3"><fmt:message key="required" /></td>
			  </tr>
			<c:forEach var="field" begin="0" end="${nrOfFileds-1}" varStatus="status">
			  <tr class="row${field % 2}" onmouseover="changeStyleClass(this,'rowOver')" onmouseout="changeStyleClass(this,'row${field % 2}')">
			    <td align="left"></td>
			    <td class="nameCol"><c:out value="${field+1}"/></td>
			    <td class="nameCol">
			      <form:input path="crmForm.crmFields[${field}].fieldName" size="75"/>
			      <form:errors path="crmForm.crmFields[${field}].fieldName" cssClass="error"/>
	 	 		</td>
			    <td class="nameCol">
			      <form:select path="crmForm.crmFields[${field}].contactFieldId">
			        <form:option value="1010" label="First Name" />
				    <form:option value="1020" label="Last Name" />
				    <form:option value="1030" label="Email 1" />
				    <form:option value="1040" label="Email 2" />
				    <form:option value="1050" label="Phone 1" />
				    <form:option value="1060" label="Phone 2" />
				    <form:option value="1070" label="Fax" />
				    <form:option value="1080" label="Street" />
				    <form:option value="1090" label="City" />
				    <form:option value="1100" label="State" />
				    <form:option value="1110" label="Zip" />
				    <form:option value="1120" label="Country" />
				    <form:option value="1140" label="Address Type" />
				    <form:option value="1130" label="Notes" />
				    <form:option value="1150" label="Attachment" />
				    <form:option value="1160" label="Language" />
				    <option disabled="disabled" style="font-weight: bolder; text-align: center;">Custom Fields</option>
				  <c:forEach items="${crmContactFields}" var="crmContactField">
				    <c:if test="${crmContactField.fieldName != ''}">
				    <form:option value="${crmContactField.id}" label="${crmContactField.fieldName}"/>
				    </c:if>
				  </c:forEach>
				  </form:select>
				  <form:errors path="crmForm.crmFields[${field}].contactFieldId" cssClass="error"/>
	 	 		</td>
	 	 		<td class="nameCol">
			      <form:select path="crmForm.crmFields[${field}].type">
			      	<form:option value="1"  label="Text"/>
				    <form:option value="2"  label="Text Area" />
				    <form:option value="3"  label="Check Box" />
				    <form:option value="4"  label="Radio Button" />
				    <form:option value="5"  label="Drop Down List" />
			        <form:option value="6"  label="Attachment" />
			        <form:option value="7"  label="Validation" />
			      </form:select>
			    </td>
			    <td class="nameCol">
			      <form:select path="crmForm.crmFields[${field}].required">
			        <form:option value="false"  label="No" />
				    <form:option value="true"  label="Yes"/>
				  </form:select>
			    </td>
			  </tr>
			</c:forEach>
			</table>
			
	<!-- end tab -->        
	</div> 
</div>	
	

<!-- start button -->
	<div align="left" class="button"> 
		<input type="submit" value="<fmt:message key="prevStep" />" name="_target0"> &nbsp;
		<input type="submit" value="<fmt:message key="nextStep" />" name="_target2"> &nbsp;
		<c:if test="${!crmFormBuilderForm.newCrmForm}">
			<input type="submit" value="<spring:message code="delete"/>" name="_target5" onClick="return confirm('Delete permanently?')" />
        </c:if> 
	</div>
<!-- end button -->
  
  <!-- end table -->
    </td><td class="boxmidr" ></td></tr>
  <tr><td class="botl"></td><td class="botr"></td></tr>
  </table>
  
<!-- end main box -->  
</div>
</form:form>
</sec:authorize> 

  </tiles:putAttribute>    
</tiles:insertDefinition>