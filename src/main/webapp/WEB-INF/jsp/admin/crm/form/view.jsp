<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>


  <c:if test="${model.htmlCode != null}">
  	<h3 align="center"><fmt:message key="code" /></h3><hr/>
    <textarea rows="20" cols="100" > ${model.htmlCode}</textarea><hr/>
  </c:if>
  <c:if test="${model.htmlCode == null}">
     <h3 align="center"><fmt:message key="form" /></h3><hr/>
     ${model.form}<hr/>
  </c:if>
 
