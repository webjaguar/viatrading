<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="/WEB-INF/tlds/spring.tld" prefix="spring" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ taglib uri="http://www.springframework.org/security/tags" prefix="sec" %>

<tiles:insertDefinition name="admin.crm.contact" flush="true">
  <tiles:putAttribute name="content" type="string">
<sec:authorize ifAnyGranted="ROLE_AEM,ROLE_ADMIN_${prop_site_id},ROLE_CRM_FORM">
<script language="JavaScript"> 
<!--
window.addEvent('domready', function(){			
	var tabs1 = new SimpleTabs($('tab-block-1'), { selector: 'h4'});
	var Tips1 = new Tips($$('.toolTipImg'));
});
//--> 
</script> 
<form:form commandName="crmFormBuilderForm" method="post">

<!-- main box -->
  <div id="mboxfull">
  
  <!-- start table -->
  <table cellpadding="0" cellspacing="0" border="0" class="module" >
  <tr>
    <td class="topl_g">
	  
	  <!-- breadcrumb -->
	  <p class="breadcrumb">
	    <a href="../crm">CRM</a> &gt;
	    <a href="../crm/crmFormList.jhtm">Form</a> &gt;
	    Step 4 of 5 
	  </p>
	  
	  <!-- Error Message -->
	  <spring:hasBindErrors name="crmFormBuilderForm">
		<div class="message">Please fix all errors!</div>
	  </spring:hasBindErrors>

  </td><td class="topr_g" ></td></tr>
  <tr><td class="boxmidlrg" >

 <!-- tabs -->
 <div id="tab-block-1">
	<h4 title="<fmt:message key="field"/>">Header and Footer</h4>
	<div>
	<!-- start tab -->
 
	    <c:set var="classIndex" value="0" />
		<div class="listdivi ln tabdivi"></div>
		
		 <div class="list${classIndex % 2}"><c:set var="classIndex" value="${classIndex+1}" />
		 <div class="listfl"><fmt:message key="doubleOptIn" />:</div>
		 <div class="listp">
		 <!-- input field -->
		 		<form:checkbox path="crmForm.doubleOptIn"/>
           		<form:errors path="crmForm.doubleOptIn" cssClass="error"/>
	 	 <!-- end input field -->   	
		 </div>
		 </div>
		 
		 <div class="list${classIndex % 2}"><c:set var="classIndex" value="${classIndex+1}" />
		 <div class="listfl"><fmt:message key="header" />:</div>
		 <div class="listp">
		 <!-- input field -->
                <form:textarea rows="5" cols="60" path="crmForm.header" htmlEscape="true" />
		 <!-- end input field -->   	
		 </div>
		 </div>
		 
		 <div class="list${classIndex % 2}"><c:set var="classIndex" value="${classIndex+1}" />
		 <div class="listfl"><fmt:message key="footer" />:</div>
		 <div class="listp">
		 <!-- input field -->
                <form:textarea rows="5" cols="60" path="crmForm.footer" htmlEscape="true" />
		 <!-- end input field -->   	
		 </div>
		 </div>
		 
		 <div class="list${classIndex % 2}"><c:set var="classIndex" value="${classIndex+1}" />
		 <div class="listfl"><fmt:message key="sendButton" />:</div>
		 <div class="listp">
		 <!-- input field -->
           		<form:input path="crmForm.sendButton" size="15" htmlEscape="true" />
		   		<form:errors path="crmForm.sendButton" cssClass="error"/>
	 	 <!-- end input field -->   	
		 </div>
		 </div>
		 
		 
	<!-- end tab -->        
	</div> 
</div>		
	

<!-- start button -->
	<div align="left" class="button"> 
		<input type="submit" value="<fmt:message key="prevStep" />" name="_target2"> &nbsp;
		<input type="submit" value="<fmt:message key="preview" />" name="_target4"> &nbsp;
		<c:if test="${!crmFormBuilderForm.newCrmForm}">
			<input type="submit" value="<spring:message code="delete"/>" name="_target5" onClick="return confirm('Delete permanently?')" />
        </c:if>
	</div>
<!-- end button -->
  
  <!-- end table -->
    </td><td class="boxmidr" ></td></tr>
  <tr><td class="botl"></td><td class="botr"></td></tr>
  </table>
  
<!-- end main box -->  
</div>
</form:form>
</sec:authorize> 

  </tiles:putAttribute>    
</tiles:insertDefinition>