<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="/WEB-INF/tlds/spring.tld" prefix="spring" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ taglib uri="http://www.springframework.org/security/tags" prefix="sec" %>

<tiles:insertDefinition name="admin.crm.contact" flush="true">
  <tiles:putAttribute name="content" type="string">
<sec:authorize ifAnyGranted="ROLE_AEM,ROLE_ADMIN_${prop_site_id},ROLE_CRM_FORM">
<script language="JavaScript"> 
<!--
window.addEvent('domready', function(){			
	var tabs1 = new SimpleTabs($('tab-block-1'), { selector: 'h4'});
	var Tips1 = new Tips($$('.toolTipImg'));
});
//--> 
</script> 
<form:form commandName="crmFormBuilderForm" method="post">

<!-- main box -->
  <div id="mboxfull">
  
  <!-- start table -->
  <table cellpadding="0" cellspacing="0" border="0" class="module" >
  <tr>
    <td class="topl_g">
	  
	  <!-- breadcrumb -->
	  <p class="breadcrumb">
	    <a href="../crm">CRM</a> &gt;
	    <a href="../crm/crmFormList.jhtm">Form</a> &gt;
	    Step 5 of 5 
	  </p>
	  
  </td><td class="topr_g" ></td></tr>
  <tr><td class="boxmidlrg" >

 <!-- tabs -->
 <div id="tab-block-1">
	<h4 title="<fmt:message key="field"/>">Code and Preview</h4>
	<div>
	<!-- start tab -->
 
	    <c:set var="classIndex" value="0" />
		<div class="listdivi ln tabdivi"></div>
		
		 <div class="list${classIndex % 2}"><c:set var="classIndex" value="${classIndex+1}" />
		 <div class="listfl"><div class="helpImg"><img class="toolTipImg" title="Note:: Do not use this code without saving form first. " src="../graphics/question.gif" /></div><fmt:message key="code" />:</div>
		 <div class="listp">
		 <!-- input field -->
		 		<textarea rows="5" cols="60" > ${htmlCode}</textarea>
		 <!-- end input field -->   	
		 </div>
		 </div>
		 
		 <div class="list${classIndex % 2}"><c:set var="classIndex" value="${classIndex+1}" />
		 <div class="listfl"><fmt:message key="form" />:</div>
		 <div class="listp">
		 <!-- input field -->
		 		<div class="formPreview"> ${form}</div>
		 <!-- end input field -->   	
		 </div>
		 </div>
		 
		 
	<!-- end tab -->        
	</div> 
</div>		
	

<!-- start button -->
	<div align="left" class="button"> 
		<input type="submit" value="<fmt:message key="prevStep" />" name="_target3">
		<c:if test="${crmFormBuilderForm.newCrmForm}">
			<input type="submit" value="<fmt:message key="save" /> <fmt:message key="form" />" name="_finish"> &nbsp;
		</c:if>
    	<c:if test="${!crmFormBuilderForm.newCrmForm}">
			<input type="submit" value="<fmt:message key="update" /> <fmt:message key="form" />" name="_finish"> &nbsp;
		    <input type="submit" value="<spring:message code="delete"/>" name="_target5" onClick="return confirm('Delete permanently?')" />
        </c:if>
		 
    </div>
<!-- end button -->
  
  <!-- end table -->
    </td><td class="boxmidr" ></td></tr>
  <tr><td class="botl"></td><td class="botr"></td></tr>
  </table>
  
<!-- end main box -->  
</div>
</form:form>
</sec:authorize> 

  </tiles:putAttribute>    
</tiles:insertDefinition>