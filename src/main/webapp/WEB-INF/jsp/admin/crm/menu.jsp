<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/security/tags" prefix="sec" %>
<%@page import="com.webjaguar.model.AccessUser"%>

<link rel="stylesheet" type="text/css" media="all" href="../../admin/javascript/jscalendar-1.0/skins/aqua/theme.css" title="Aqua" /> <!-- calendar stylesheet -->
<script type="text/javascript" src="../../admin/javascript/jscalendar-1.0/calendar.js"></script> <!-- main calendar program -->
<script type="text/javascript" src="../../admin/javascript/jscalendar-1.0/lang/calendar-en.js"></script>
<script type="text/javascript" src="../../admin/javascript/jscalendar-1.0/calendar-setup.js"></script>
<script src="../javascript/MenuMatic_0.68.3.js" type="text/javascript" charset="utf-8"></script>
<script type="text/javascript" src="../../admin/javascript/autocompleter1.2.js"></script>
<script type="text/javascript" src="../../admin/javascript/autocompleter.Request1.2.js"></script>
<script type="text/javascript" src="../../admin/javascript/observer.js"></script>
<script type="text/javascript" >
window.addEvent('domready', function() {			
	var myMenu = new MenuMatic({ orientation:'vertical' });	

	var crmAccountName = $('crmAccountName');
	new Autocompleter.Request.HTML(crmAccountName, '../crm/ajax-showCrmAccounts.jhtm', {
		'indicatorClass': 'autocompleter-loading',
		'postData': { 'search': 'crmAccountName' },
		'injectChoice': function(choice) {
			var text = choice.getFirst();
			var value = text.innerHTML;
			choice.inputValue = value;
			text.set('html', this.markQueryValue(value));
			this.addChoiceEvents(choice);
		},
		'onSelection': function(element, selected, value, input) {
	//		lastName.value = selected.getFirst().getNext().getNext().innerHTML;
		}
	});
			
});		

function UpdateStartEndDate(type){
var myDate = new Date();
$('endDate').value = (myDate.getMonth()+1) + "/" + myDate.getDate() + "/" + myDate.getFullYear();
	if ( type == 2 ) {
		myDate.setDate(myDate.getDate() - myDate.getDay());
		$('startDate').value = (myDate.getMonth()+1) + "/" + myDate.getDate() + "/" + myDate.getFullYear();
	} else if ( type == 3 ) {
		myDate.setDate(1);
		$('startDate').value = (myDate.getMonth()+1) + "/" + myDate.getDate() + "/" + myDate.getFullYear();
	}else if ( type == 10 ) {
		myDate.setMonth(0, 1);
		$('startDate').value = (myDate.getMonth()+1) + "/" + myDate.getDate() + "/" + myDate.getFullYear();
	}else if ( type == 20 ) {
		$('startDate').value = "";
		$('endDate').value = "";
	}else{
		$('startDate').value = (myDate.getMonth()+1) + "/" + myDate.getDate() + "/" + myDate.getFullYear();
	}
}
function UpdateStartEndDate2(type){
	var myDate = new Date();
	$('endDueDate').value = (myDate.getMonth()+1) + "/" + myDate.getDate() + "/" + myDate.getFullYear();
		if ( type == 2 ) {
			myDate.setDate(myDate.getDate() - myDate.getDay());
			$('startDueDate').value = (myDate.getMonth()+1) + "/" + myDate.getDate() + "/" + myDate.getFullYear();
		} else if ( type == 3 ) {
			myDate.setDate(1);
			$('startDueDate').value = (myDate.getMonth()+1) + "/" + myDate.getDate() + "/" + myDate.getFullYear();
		}else if ( type == 10 ) {
			myDate.setMonth(0, 1);
			$('startDueDate').value = (myDate.getMonth()+1) + "/" + myDate.getDate() + "/" + myDate.getFullYear();
		}else if ( type == 20 ) {
			$('startDueDate').value = "";
			$('endDueDate').value = "";
		}else{
			$('startDueDate').value = (myDate.getMonth()+1) + "/" + myDate.getDate() + "/" + myDate.getFullYear();
		}
	}
</script>

<!-- menu -->
  <div id="lbox" class="quickMode">
    <table cellpadding="0" cellspacing="0" border="0" class="module" style="width: 100%"><tr><td class="topl" id="topl">
    </td><td class="topr"></td></tr><tr><td class="boxmid" id="tbid">
  	<h2 class="menuleft mfirst"><fmt:message key="crmAccount"/></h2>
    <div class="menudiv"></div>
    <ul id="nav">
      <sec:authorize ifAnyGranted="ROLE_AEM,ROLE_ADMIN_${prop_site_id},ROLE_CRM">
      <li><a href="crmAccountList.jhtm" class="userbutton"><fmt:message key="list"/></a>
      </sec:authorize>
      <sec:authorize ifAnyGranted="ROLE_AEM,ROLE_ADMIN_${prop_site_id},ROLE_CRM_IMPORT_EXPORT">
      <ul>
        <li><a href="crmAccountImport.jhtm" class="userbutton"><fmt:message key="import"/></a></li>
      </ul>
      </sec:authorize>
      </li>
    <h2 class="menuleft"><fmt:message key="crmContact"/></h2>
    <div class="menudiv"></div>
      <sec:authorize ifAnyGranted="ROLE_AEM,ROLE_ADMIN_${prop_site_id},ROLE_CRM">
      <li><a href="crmContactList.jhtm" class="userbutton"><fmt:message key="list"/></a>
      </sec:authorize>
      <sec:authorize ifAnyGranted="ROLE_AEM,ROLE_ADMIN_${prop_site_id},ROLE_CRM_IMPORT_EXPORT">
      <ul>
        <li><a href="crmContactImport.jhtm" class="userbutton"><fmt:message key="import"/></a></li>
        <li class="userbutton"><a href="#" class="userbutton"><fmt:message key="export"/></a>
        <ul>
          <li><a href="crmContactExport.jhtm?fileType=xls&__new=true" class="userbutton">EXCEL</a></li>
          <li><a href="crmContactExport.jhtm?fileType=csv&__new=true" class="userbutton">CSV</a></li>
        </ul>
        </li>
      </ul>
      </sec:authorize>
      </li>
      <sec:authorize ifAnyGranted="ROLE_AEM,ROLE_ADMIN_${prop_site_id},ROLE_CRM_FIELD">
      <li><a href="crmContactFieldList.jhtm" class="userbutton"><fmt:message key="Fields"/></a></li>
      </sec:authorize>
      <sec:authorize ifAnyGranted="ROLE_AEM,ROLE_ADMIN_${prop_site_id},ROLE_CRM_GROUP">
      <li><a href="crmContactGroupList.jhtm" class="userbutton"><fmt:message key="crm"/> <fmt:message key="groups"/></a></li>
      </sec:authorize>
      <sec:authorize ifAnyGranted="ROLE_AEM,ROLE_ADMIN_${prop_site_id},ROLE_CRM_FORM">
      <li><a href="crmFormList.jhtm" class="userbutton"><fmt:message key="crmForm"/></a></li>
      </sec:authorize>
      <sec:authorize ifAnyGranted="ROLE_AEM,ROLE_ADMIN_${prop_site_id}">
	  <li><a href="crmContactCustomerMap.jhtm" class="userbutton"><fmt:message key="mapToCustomer"/></a></li>
      </sec:authorize>
      <sec:authorize ifAnyGranted="ROLE_AEM,ROLE_ADMIN_${prop_site_id},ROLE_EMAILMANAGER_CREATE">
	  <c:if test="${gSiteConfig['gMASS_EMAIL']}">
      <li><a href="crmContactMassEmail.jhtm" class="userbutton"><fmt:message key="massEmail"/></a></li>
      </c:if>
      <sec:authorize ifAnyGranted="ROLE_AEM,ROLE_ADMIN_${prop_site_id},ROLE_CRM_IMPORT_EXPORT">
      <li><a href="crmDialerExport.jhtm" class="userbutton"><fmt:message key="crmDialerExport"/></a></li>
      </sec:authorize>
      </sec:authorize>
    <h2 class="menuleft"><fmt:message key="crmTask"/></h2>
    <div class="menudiv"></div> 
      <sec:authorize ifAnyGranted="ROLE_AEM,ROLE_ADMIN_${prop_site_id},ROLE_CRM">
      <li><a href="crmTaskList.jhtm" class="userbutton"><fmt:message key="list"/></a>
      </sec:authorize>
      <sec:authorize ifAnyGranted="ROLE_AEM,ROLE_ADMIN_${prop_site_id},ROLE_CRM_IMPORT_EXPORT">
      <ul>
        <li class="userbutton"><a href="crmTaskExport.jhtm?fileType=xls&__new=true" class="userbutton"><fmt:message key="export"/></a></li>
      </ul>
      </sec:authorize>
      </li>
	<div class="menudiv"></div>
	</ul>  	  
    <c:if test="${param.tab == 'crmAccount'}"> 
      <div class="menudiv"></div>
      <div align="left" class="searchHeader" id="searchHeaderId"><h2><fmt:message key="search" /></h2></div>
	  <table class="searchBoxLeft" width="100%"><tr><td valign="top" style="width: 110px;">
	  <form action="crmAccountList.jhtm" method="post" name="searchform">	
	  <!-- content area -->
	    <div class="search2">
	      <p>Account Name:</p>
	      <input name="crmAccountName" id="crmAccountName" type="text" value="<c:out value='${crmAccountSearch.accountName}' />" size="15" />
	    </div>     
	    <div class="search2">
	      <p><fmt:message key="username" />:</p>
	      <select name="accessUserId">
		   <c:if test="${model.accessUserTree == null}">
	          <option value=""></option>
	     	  <option value="-2" <c:if test="${crmAccountSearch.accessUserId == -2}">selected</c:if>>Any</option>
	          <option value="-3" <c:if test="${crmAccountSearch.accessUserId == -3}">selected</c:if>>None</option>
	      	  <c:forEach items="${model.accessUserMap}" var="accessUser">
	  	        <option value="${accessUser.key}" <c:if test="${crmAccountSearch.accessUserId == accessUser.key}">selected</c:if>><c:out value="${accessUser.value.username}" /></option>
	          </c:forEach>
	       </c:if>
	       <c:if test="${model.accessUserTree != null}">
	         <c:set var="accessUserTree" value="${model.accessUserTree}"/>
	         <c:set var="crmAccountSearchUserId" value="${crmAccountSearch.accessUserId}"/>
	         <option value="${accessUserTree.id}" <c:if test="${crmAccountSearch.accessUserId == accessUserTree.id}">selected</c:if>><c:out value="${accessUserTree.username}" /></option>
				<c:set var="level" value="1" scope="request"/>
	  			<c:set var="users" value="${accessUserTree.subAccounts}" scope="request"/>
	  			<jsp:include page="/WEB-INF/jsp/admin/crm/recursion.jsp"/>
		   </c:if>
	      </select>
	    </div> 
	    <div class="button">
	      <input type="submit" value="Search"/>
	    </div>
	  <!-- content area -->
	  </form>
	  </td></tr></table>
	<div class="menudiv"></div>    
	</c:if>
	
	<c:if test="${param.tab == 'crmContact'}"> 
	<script type="text/javascript" src="../javascript/side-bar.js"></script>
      <div class="menudiv"></div>
      <div align="left" class="searchHeader" id="searchHeaderId"><h2><fmt:message key="search" /></h2></div>
	  <form action="crmContactList.jhtm" name="searchform" method="post" >	
	  <table class="searchBoxLeft" ><tr><td valign="top" style="width: 110px;">
	  <!-- content area -->
	    <div class="search2">
	     <p>Account Name:</p>
	     <input name="crmAccountName" id="crmAccountName" type="text" value="<c:out value='${crmContactSearch.accountName}' />" size="15" />
	    </div>
	    <div class="search2">
	      <p>Contact Name:</p>
	      <input name="crmContactName" type="text" value="<c:out value='${crmContactSearch.contactName}' />" size="15" />
	    </div>
	    <div class="search2">
	      <p><fmt:message key="email" />:</p>
	      <input name="email" type="text" value="<c:out value='${crmContactSearch.email}' />" size="15" />
	    </div>
	    <div class="search2">
	      <p><fmt:message key="crm"/> <fmt:message key="group" />:</p>
	      <select name="groupId">
	         <option value=""></option>
	      <c:forEach items="${model.groups}" var="group">
	  	     <option value="${group.id}" <c:if test="${crmContactSearch.groupId == group.id}">selected</c:if>><c:out value="${group.name}" /></option>
	       </c:forEach>
	      </select>
	    </div> 
	    <c:if test="${gSiteConfig['gGROUP_CUSTOMER']}">
      	<div class="search2">
	      <p><fmt:message key="customer"/> <fmt:message key="group" />:</p>
	      <select name="customerGroupId">
	         <option value=""></option>
	      <c:forEach items="${model.customerGroups}" var="group">
	  	     <option value="${group.id}" <c:if test="${crmContactSearch.customerGroupId == group.id}">selected</c:if>><c:out value="${group.name}" /></option>
	       </c:forEach>
	      </select>
	    </div>
	    </c:if> 
	    <div class="search2">
	      <p><fmt:message key="username" />:</p>
	      <select name="accessUserId">
		   <c:if test="${model.accessUserTree == null}">
	          <option value=""></option>
	     	  <option value="-2" <c:if test="${crmContactSearch.accessUserId == -2}">selected</c:if>>Any</option>
	          <option value="-3" <c:if test="${crmContactSearch.accessUserId == -3}">selected</c:if>>None</option>
	      	  <c:forEach items="${model.accessUserMap}" var="accessUser">
	  	        <option value="${accessUser.key}" <c:if test="${crmContactSearch.accessUserId == accessUser.key}">selected</c:if>><c:out value="${accessUser.value.username}" /></option>
	          </c:forEach>
	       </c:if>
	       <c:if test="${model.accessUserTree != null}">
	         <c:set var="accessUserTree" value="${model.accessUserTree}"/>
	         <c:set var="crmContactSearchUserId" value="${crmContactSearch.accessUserId}"/>
	         <option value="${accessUserTree.id}" <c:if test="${crmContactSearch.accessUserId == accessUserTree.id}">selected</c:if>><c:out value="${accessUserTree.username}" /></option>
	         	<c:set var="level" value="1" scope="request"/>
	  			<c:set var="users" value="${accessUserTree.subAccounts}" scope="request"/>
	  			<jsp:include page="/WEB-INF/jsp/admin/crm/recursion.jsp"/>
		   </c:if>
	      </select>
	    </div> 
	    <div class="search2">
	      <p><fmt:message key="customer" />:</p>
	      <select name="convertedCustomer">
	        <option value="0" <c:if test="${crmContactSearch.convertedCustomer == 0}">selected</c:if>>All</option>
	     	<option value="1" <c:if test="${crmContactSearch.convertedCustomer == 1}">selected</c:if>>Yes</option>
	        <option value="2" <c:if test="${crmContactSearch.convertedCustomer == 2}">selected</c:if>>No</option>
	      </select>
	    </div> 
	    <div class="search2">
	      <p><fmt:message key="zipcode" />:</p>
	      <input name="zipcode" type="text" value="<c:out value='${crmContactSearch.zipcode}' />" size="15" />
	    </div> 
	    <div class="search2">
	      <p><fmt:message key="city" />:</p>
	      <input name="city" type="text" value="<c:out value='${crmContactSearch.city}' />" size="15" />
	    </div>
	    <div class="search2">
	      <p><fmt:message key="state" />:</p>
	      <input name="state" type="text" value="<c:out value='${crmContactSearch.state}' />" size="15" />
	    </div>
	    <div class="search2">
	      <p><fmt:message key="country" />:</p>
			<select name="country">
            <option value="" ></option>
            <c:forEach items="${model.countries}" var="country" varStatus="status">
              <option value="${country.code}" <c:if test="${crmContactSearch.country == country.code}">selected</c:if>>${country.name}</option>
            </c:forEach>
        	</select>
	    </div>
	    <div class="search2">
	      <p>Description:</p>
	      <input name="taskDescription" type="text" value="<c:out value='${crmContactSearch.taskDescription}' />" size="15" />
	    </div>
	    <div class="search2">
	      <p><fmt:message key="trackcode" />:</p>
	      <input name="trackcode" type="text" value="<c:out value='${crmContactSearch.trackcode}' />" size="15" />
	    </div>
	    <div class="search2">
	      <p><fmt:message key="leadSource" />:</p>
	      <input name="leadSource" type="text" value="<c:out value='${crmContactSearch.leadSource}' />" size="15" />
	    </div>
	    <div class="search2">
	      <p><fmt:message key="qualifier" />:</p>
	      <select name="qualifier">
	       <option value="-1"></option>
	       <option value="-2" <c:if test="${crmContactSearch.qualifierId == -2}">selected</c:if>><fmt:message key="noQualifier" /></option>
	       <c:forEach items="${model.activeQualifiers}" var="qualifier">
	  	     <option value="${qualifier.id}" <c:if test="${crmContactSearch.qualifierId == qualifier.id}">selected</c:if>><c:out value="${qualifier.name}" /></option>
	       </c:forEach>
	      </select>
	    </div>  
	    <div class="search2">
	      <p><fmt:message key="language" />:</p>
	      <select name="languageField">
	       <option value="all"></option>
	       <option value="noLang" <c:if test="${crmContactSearch.language == 'noLang'}">selected</c:if>><fmt:message key="noLang" /></option>
	       <option value="en" <c:if test="${crmContactSearch.language == 'en'}">selected</c:if>><fmt:message key="language_en"/></option> 
	       <c:forEach items="${model.languageCodes}" var="i18n">
	       <option value="${i18n.languageCode}" <c:if test="${crmContactSearch.language == i18n.languageCode}">selected</c:if>><fmt:message key="language_${i18n.languageCode}"/></option>
           </c:forEach>
	      </select>
	     </div>  
	    <!--  
	    <div class="search2">
	      <p>Rating:</p>
	      <select name="rating">
	          <option value=""></option>
	  	    <c:forTokens items="${siteConfig['CRM_RATING'].value}" delims="," var="rating">
	          <option value="${rating}" <c:if test="${crmContactSearch.rating == rating}">selected</c:if>>${rating}</option>
	  	    </c:forTokens>
	  	   </select>
	  	</div>
	  	-->
	  	<div class="search2">
	      <p>Rating1:</p>
	      <select name="rating1">
	          <option value=""></option>
	  	    <c:forTokens items="${siteConfig['CUSTOMER_RATING_1'].value}" delims="," var="rating1">
	          <option value="${rating1}" <c:if test="${crmContactSearch.rating1 == rating1}">selected</c:if>>${rating1}</option>
	  	    </c:forTokens>
	  	   </select>
	  	</div>
	  	<div class="search2">
	      <p>Rating2:</p>
	      <select name="rating2">
	          <option value=""></option>
	  	    <c:forTokens items="${siteConfig['CUSTOMER_RATING_2'].value}" delims="," var="rating2">
	          <option value="${rating2}" <c:if test="${crmContactSearch.rating2 == rating2}">selected</c:if>>${rating2}</option>
	  	    </c:forTokens>
	  	   </select>
	  	</div>
	    <div class="search2">
	      <p><c:set var="contactFieldFlag" value="false" />
	      <select class="extraFieldSearch" name="contactFieldId">
	  	    <c:forEach items="${model.contactFields}" var="contactField">
	  	     <c:if test="${!empty contactField.fieldName}" ><c:set var="contactFieldFlag" value="true" />
	  	     <option style="font-size:normal;" value="${contactField.id}" <c:if test="${crmContactSearch.contactFieldId == contactField.id}">selected</c:if>><c:out value="${contactField.fieldName}" />:</option>
	  	     </c:if>
	  	    </c:forEach>
	      </select>
	      </p>
	      <c:if test="${contactFieldFlag}" >
	       <input name="contactFieldValue" type="text" value="<c:out value='${crmContactSearch.contactFieldValue}' />" size="11" />
	      </c:if>
	    </div>
	    <div class="button">
	      <input type="submit" value="Search"/>
	    </div>
	  <!-- content area -->
	  </td>
	  <td valign="top">
	  <div id="sideBar">
	  <a name="#tabContact" ></a>
	  <a href="#tabCOntact" id="sideBarTab"><img id="h_toggle"  src="../graphics/tabSliderLeft.png" alt="sideBar" title="sideBar" /></a>
      <div id="sideBarContents" style="width:0px;">
	  <table cellpadding="0" cellspacing="0" border="0" class="module" id="module">
      <tr>
      <td class="topl_g"><div align="left" class="searchHeaderAdvanced"><h2><fmt:message key="advancedSearch" /></h2></div></td>
      <td class="topr_g" ></td></tr>
      <tr><td class="boxmidlrg" >
  
		<table class="searchBoxLeftAS"><tr><td>
		<div id="sideBarContentsInner">
			<div class="advancedSearch">
		      <p>start:</p>
		      <input style="width:90px;margin-right:5px;" name="startDate" id="startDate" type="text" value="<fmt:formatDate type="date" pattern="MM/dd/yyyy" value="${crmContactSearch.startDate}"/>" readonly size="11" />
			  <img class="calendarImage"  id="startDate_trigger" src="../graphics/calendarIcon.jpg" />
			  <script type="text/javascript">
	    		Calendar.setup({
	        			inputField     :    "startDate",   // id of the input field
	        			showsTime      :    false,
	        			ifFormat       :    "%m/%d/%Y",   // format of the input field
	        			button         :    "startDate_trigger"   // trigger for the calendar (button ID)
	    		});	
			  </script>
		    </div>
		    <div style="clear:both;"></div>    
		    <div class="advancedSearch">
		      <p>end:</p>
		      <input style="width:90px;margin-right:5px;" name="endDate" id="endDate" type="text" value="<fmt:formatDate type="date" pattern="MM/dd/yyyy" value='${crmContactSearch.endDate}'/>" readonly size="11" />
			  <img class="calendarImage"  id="endDate_trigger" src="../graphics/calendarIcon.jpg" />
			  <script type="text/javascript">
	    		Calendar.setup({
	        			inputField     :    "endDate",   // id of the input field
	        			showsTime      :    false,
	        			ifFormat       :    "%m/%d/%Y",   // format of the input field
	        			button         :    "endDate_trigger"   // trigger for the calendar (button ID)
	    		});	
			  </script>
			</div>  
			<div style="clear:both;"></div>    
		    <div class="advancedSearch">
		      <table border="0">
		       <tr> <td class="advancedSearchDateInterval"><input style="width:12px;margin-right:5px;border: 0px" name="intervaldate" id="today" type="radio" onclick="UpdateStartEndDate(1);" />Today</td> </tr>
		       <tr> <td class="advancedSearchDateInterval"><input style="width:12px;margin-right:5px;border: 0px" name="intervaldate" id="weekToDate" type="radio" onclick="UpdateStartEndDate(2);" />Week To Date</td> </tr>
		       <tr> <td class="advancedSearchDateInterval"><input style="width:12px;margin-right:5px;border: 0px" name="intervaldate" id="monthToDate" type="radio" onclick="UpdateStartEndDate(3);" />Month To Date</td> </tr>
		       <tr> <td class="advancedSearchDateInterval"><input style="width:12px;margin-right:5px;border: 0px" name="intervaldate" id="yearToDate" type="radio" onclick="UpdateStartEndDate(10);" />Year To Date</td> </tr>
		       <tr> <td class="advancedSearchDateInterval"><input style="width:12px;margin-right:5px;border: 0px" name="intervaldate" id="reset" type="radio" onclick="UpdateStartEndDate(20);" />Reset</td> </tr>
		      </table>
		    </div>    
		</div>
		</td></tr></table>
	  </td><td class="boxmidr" ></td></tr>
      <tr><td class="botl" id="botl"></td><td class="botr" id="botr"></td></tr>
      </table></div>
    
      </div>
	  </td>
	  </tr></table>
	  </form>
	<div class="menudiv"></div>    
	</c:if>
	
	<c:if test="${param.tab == 'crmTask'}"> 
	  <script type="text/javascript" src="../javascript/side-bar.js"></script>
      <div class="menudiv"></div>
      <div align="left" class="searchHeader" id="searchHeaderId"><h2><fmt:message key="search" /></h2></div>
	  <form action="crmTaskList.jhtm" name="searchform" method="post">	
	  <table class="searchBoxLeft" width="100%"><tr><td valign="top" style="width: 110px;">
	  <!-- content area -->
	    <%--
	    <div class="search2">
	     <p>Account Name:</p>
	     <select name="crmAccountId">
	         <option value=""></option>
	       <c:forEach items="${model.accounts}" var="account">
	  	     <option value="${account.id}" <c:if test="${crmTaskSearch.accountId == account.id}">selected</c:if>><c:out value="${account.accountName}" /></option>
	       </c:forEach>
	      </select>
	    </div>
	     --%>
	    <div class="search2">
	     <p>Account Name:</p>
	     <input name="crmAccountName" id="crmAccountName" type="text" value="<c:out value='${crmTaskSearch.accountName}' />" size="15" />
	    </div>
	    <div class="search2">
	      <p>Contact Name:</p>
	      <input name="crmContactName" type="text" value="<c:out value='${crmTaskSearch.contactName}' />" size="15" />
	    </div>
	    <div class="search2">
	      <p><fmt:message key="assignedTo" />:</p>
	      <select name="crmAssignedTo">
		   <c:if test="${model.accessUserTree == null}">
	          <option value=""></option>
	     	  <option value="0" <c:if test="${crmTaskSearch.assignedToId == 0}">selected</c:if>>Admin</option>
	          <c:forEach items="${model.accessUserMap}" var="accessUser">
	  	        <option value="${accessUser.key}" <c:if test="${crmTaskSearch.assignedToId == accessUser.key}">selected</c:if>><c:out value="${accessUser.value.username}" /></option>
	          </c:forEach>
	       </c:if> 
	       <c:if test="${model.accessUserTree != null}">
	         <c:set var="accessUserTree" value="${model.accessUserTree}"/>
	         <c:set var="crmTaskSearchUserId" value="${crmTaskSearch.assignedToId}"/>
	         <option value="${accessUserTree.id}" <c:if test="${crmTaskSearch.assignedToId == accessUserTree.id}">selected</c:if>><c:out value="${accessUserTree.username}" /></option>
			 	<c:set var="level" value="1" scope="request"/>
	  			<c:set var="users" value="${accessUserTree.subAccounts}" scope="request"/>
	  			<jsp:include page="/WEB-INF/jsp/admin/crm/recursion.jsp"/>
		   </c:if>
	      </select>
	    </div>
	    <div class="search2">
	      <p><fmt:message key="title" />:</p>
	      <input name="crmTitle" type="text" value="<c:out value='${crmTaskSearch.title}' />" size="15" />
	    </div>
	    <div class="search2">
	      <p><fmt:message key="status" />:</p>
	      <select name="crmStatus">
	  	    <option value=""></option>
	  	    <option value="01" <c:if test="${crmTaskSearch.status == '01'}">selected</c:if>><fmt:message key="taskStatus_01" /></option>
	  	    <option value="02" <c:if test="${crmTaskSearch.status == '02'}">selected</c:if>><fmt:message key="taskStatus_02" /></option>
	  	    <option value="03" <c:if test="${crmTaskSearch.status == '03'}">selected</c:if>><fmt:message key="taskStatus_03" /></option>
	  	    <option value="04" <c:if test="${crmTaskSearch.status == '04'}">selected</c:if>><fmt:message key="taskStatus_04" /></option>
	        <option value="05" <c:if test="${crmTaskSearch.status == '05'}">selected</c:if>><fmt:message key="taskStatus_05" /></option>
	        <option value="06" <c:if test="${crmTaskSearch.status == '06'}">selected</c:if>><fmt:message key="taskStatus_06" /></option>
	       </select>
	    </div>
	    <div class="search2">
	      <p>Description:</p>
	      <input name="crmDescription" type="text" value="<c:out value='${crmTaskSearch.description}' />" size="15" />
	    </div>
	    <div class="search2">
	      <p>Action:</p>
	      <select name="crmAction">
	          <option value=""></option>
	  	    <c:forTokens items="${siteConfig['CRM_TASK_ACTION'].value}" delims="," var="action">
	          <option value="${action}" <c:if test="${crmTaskSearch.action == action}">selected</c:if>>${action}</option>
	  	    </c:forTokens>
	  	   </select>
	  	</div>
	    <div class="search2">
	      <p>Rank:</p>
	      <select name="crmPriority">
	          <option value=""></option>
	  	    <c:forTokens items="${siteConfig['CRM_TASK_RANK'].value}" delims="," var="priority">
	          <option value="${priority}" <c:if test="${crmTaskSearch.priority == priority}">selected</c:if>>${priority}</option>
	  	    </c:forTokens>
	  	   </select>
	  	</div>
	    <div class="search2">
	      <p>Type:</p>
	      <select name="crmType">
	          <option value=""></option>
	  	    <c:forTokens items="${siteConfig['CRM_TASK_TYPE'].value}" delims="," var="type">
	          <option value="${type}" <c:if test="${crmTaskSearch.type == type}">selected</c:if>>${type}</option>
	  	    </c:forTokens>
	  	   </select>
	  	</div>
	    <div class="button">
	      <input type="submit" value="Search"/>
	    </div>
	  <!-- content area -->
	  </td>
	  <td valign="top">
	  <div id="sideBar">
	  <a href="#tabContact" id="sideBarTab"><img id="h_toggle"  src="../graphics/tabSliderLeft.png" alt="sideBar" title="sideBar" /></a>
      <div id="sideBarContents" style="width:0px;">
	  <table cellpadding="0" cellspacing="0" border="0" class="module" >
      <tr>
      <td class="topl_g"><div align="left" class="searchHeaderAdvanced"><h2><fmt:message key="advancedSearch" /></h2></div></td>
      <td class="topr_g" ></td></tr>
      <tr><td class="boxmidlrg" >
  
		<table class="searchBoxLeftAS"><tr><td>
		<div id="sideBarContentsInner">
			<div class="advancedSearch">
		      <p>start:</p>
		      <input style="width:90px;margin-right:5px;" name="startDate" id="startDate" type="text" value="<fmt:formatDate type="date" pattern="MM/dd/yyyy" value="${crmTaskSearch.startDate}"/>" readonly size="11" />
			  <img class="calendarImage"  id="startDate_trigger" src="../graphics/calendarIcon.jpg" />
			  <script type="text/javascript">
	    		Calendar.setup({
	        			inputField     :    "startDate",   // id of the input field
	        			showsTime      :    false,
	        			ifFormat       :    "%m/%d/%Y",   // format of the input field
	        			button         :    "startDate_trigger"   // trigger for the calendar (button ID)
	    		});	
			  </script>
		    </div>
		    <div style="clear:both;"></div>    
		    <div class="advancedSearch">
		      <p>end:</p>
		      <input style="width:90px;margin-right:5px;" name="endDate" id="endDate" type="text" value="<fmt:formatDate type="date" pattern="MM/dd/yyyy" value='${crmTaskSearch.endDate}'/>" readonly size="11" />
			  <img class="calendarImage"  id="endDate_trigger" src="../graphics/calendarIcon.jpg" />
			  <script type="text/javascript">
	    		Calendar.setup({
	        			inputField     :    "endDate",   // id of the input field
	        			showsTime      :    false,
	        			ifFormat       :    "%m/%d/%Y",   // format of the input field
	        			button         :    "endDate_trigger"   // trigger for the calendar (button ID)
	    		});	
			  </script>
			</div>  
			<div style="clear:both;"></div>    
		    <div class="advancedSearch">
		      <table border="0">
		       <tr> <td class="advancedSearchDateInterval"><input style="width:12px;margin-right:5px;border: 0px" name="intervaldate" id="today" type="radio" onclick="UpdateStartEndDate(1);" />Today</td> </tr>
		       <tr> <td class="advancedSearchDateInterval"><input style="width:12px;margin-right:5px;border: 0px" name="intervaldate" id="weekToDate" type="radio" onclick="UpdateStartEndDate(2);" />Week To Date</td> </tr>
		       <tr> <td class="advancedSearchDateInterval"><input style="width:12px;margin-right:5px;border: 0px" name="intervaldate" id="monthToDate" type="radio" onclick="UpdateStartEndDate(3);" />Month To Date</td> </tr>
		       <tr> <td class="advancedSearchDateInterval"><input style="width:12px;margin-right:5px;border: 0px" name="intervaldate" id="yearToDate" type="radio" onclick="UpdateStartEndDate(10);" />Year To Date</td> </tr>
		       <tr> <td class="advancedSearchDateInterval"><input style="width:12px;margin-right:5px;border: 0px" name="intervaldate" id="reset" type="radio" onclick="UpdateStartEndDate(20);" />Reset</td> </tr>
		      </table>
		    </div>    
		</div>
		</td></tr></table>
		<div>
		<h4 style="margin-left:11px;"> <fmt:message key="dueDate"/></h4>
		</div>
	  	<table class="searchBoxLeftAS"><tr><td>
		<div id="sideBarContentsInner">
			<div class="advancedSearch">
		      <p>start:</p>
		      <input style="width:90px;margin-right:5px;" name="startDueDate" id="startDueDate" type="text" value="<fmt:formatDate type="date" pattern="MM/dd/yyyy" value='${crmTaskSearch.startDueDate}'/>"  size="11" />
			  <img class="calendarImage"  id="startDueDate_trigger" src="../graphics/calendarIcon.jpg" alt="" />
			  <script type="text/javascript">
			   Calendar.setup({
					inputField     :    "startDueDate",   // id of the input field
					showsTime      :    false,
					ifFormat       :    "%m/%d/%Y",   // format of the input field
					button         :    "startDueDate_trigger"   // trigger for the calendar (button ID)
			   });	
			  </script>
			</div>  
		    <div style="clear:both;"></div>    
		    <div class="advancedSearch">
		      <p>end:</p>
		      <input style="width:90px;margin-right:5px;" name="endDueDate" id="endDueDate" type="text" value="<fmt:formatDate type="date" pattern="MM/dd/yyyy" value='${crmTaskSearch.endDueDate}'/>"  size="11" />
			  <img class="calendarImage"  id="endDueDate_trigger" src="../graphics/calendarIcon.jpg" alt="" />
			  <script type="text/javascript">
			   Calendar.setup({
					inputField     :    "endDueDate",   // id of the input field
					showsTime      :    false,
					ifFormat       :    "%m/%d/%Y",   // format of the input field
					button         :    "endDueDate_trigger"   // trigger for the calendar (button ID)
			   });	
			  </script>
			</div>  
			<div style="clear:both;"></div>    
		    <div class="advancedSearch">
		      <table border="0">
		       <tr> <td class="advancedSearchDateInterval"><input style="width:12px;margin-right:5px;border: 0px" name="intervaldate" id="today" type="radio" onclick="UpdateStartEndDate2(1);" />Today</td> </tr>
		       <tr> <td class="advancedSearchDateInterval"><input style="width:12px;margin-right:5px;border: 0px" name="intervaldate" id="weekToDate" type="radio" onclick="UpdateStartEndDate2(2);" />Week To Date</td> </tr>
		       <tr> <td class="advancedSearchDateInterval"><input style="width:12px;margin-right:5px;border: 0px" name="intervaldate" id="monthToDate" type="radio" onclick="UpdateStartEndDate2(3);" />Month To Date</td> </tr>
		       <tr> <td class="advancedSearchDateInterval"><input style="width:12px;margin-right:5px;border: 0px" name="intervaldate" id="yearToDate" type="radio" onclick="UpdateStartEndDate2(10);" />Year To Date</td> </tr>
		       <tr> <td class="advancedSearchDateInterval"><input style="width:12px;margin-right:5px;border: 0px" name="intervaldate" id="reset" type="radio" onclick="UpdateStartEndDate2(20);" />Reset</td> </tr>
		      </table>  
		    </div>
		</div>
		</td></tr></table>
		
	  
	  </td><td class="boxmidr" ></td></tr>
      <tr><td class="botl"></td><td class="botr"></td></tr>
      </table></div>
    
      </div>
	  </td></tr></table>
	  </form>
	<div class="menudiv"></div>    
	</c:if>

	<c:if test="${param.tab == 'crmGroup'}"> 
      <div align="left" class="searchHeader" id="searchHeaderId"><h2><fmt:message key="search" /></h2></div>
	  <table class="searchBoxLeft" width="100%"><tr><td valign="top" style="width: 110px;">
	  <form action="crmContactGroupList.jhtm" name="searchform" method="post">	
	  <!-- content area -->
	    <div class="search2">
	      <p>Group Name:</p>
	      <input name="group" type="text" value="<c:out value='${groupCrmContactSearch.group}' />" size="15" />
	    </div>
	    <div class="button">
	      <input type="submit" value="Search"/>
	    </div>
	  <!-- content area -->
	  </form>
	  </td></tr></table>
	<div class="menudiv"></div>    
	</c:if>
	
	<div class="menudiv"></div>  
		  
     		  
    </td><td class="boxmidr" ></td></tr>
    <tr><td class="botl"></td><td class="botr"></td></tr></table>
  </div>