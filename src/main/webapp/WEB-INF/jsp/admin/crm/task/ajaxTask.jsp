<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
 <ul class="round task">
	<li class="action-header" style="width: 99.9%; text-align: center;">
		<div class="name" style="font-weight:700px; color:black !important;"><c:out value="${model.crmTask.title}"/></div>
	</li>
	<li class="view" style="width: 99.9%;">
		<div style="float: left;"><fmt:message key="crmAccountName" /> : <c:out value="${model.crmTask.accountName}" /></div>
		<div style="margin-left: 300px;"><fmt:message key="email" /> : <c:out value="${model.crmTask.contactEmail}" /></div>
	</li>
	<li class="view" style="width: 99.9%;">
		<div style="float: left;"><fmt:message key="crmContactName" /> : <c:out value="${model.crmTask.contactName}" /></div>
		<div style="margin-left: 300px;"><fmt:message key="phone" /> : <c:out value="${model.crmTask.contactPhone}" /></div>
		<div class="menudiv" style="width: 590px;"></div>
	</li>
	<li class="view" style="width: 99.9%;">
		<div style="float: left;"><fmt:message key="type" /> : <c:out value="${model.crmTask.type}" /></div>
		<div style="margin-left: 300px;"><fmt:message key="assignedTo" /> : <c:out value="${model.crmTask.assignedTo}" /></div>
	</li>
	<li class="view" style="width: 99.9%;">
		<div style="float: left;"><fmt:message key="rank" /> : <c:out value="${model.crmTask.priority}" /></div>
		<div style="margin-left: 300px;"><fmt:message key="dueDate" /> : <fmt:formatDate type="date" timeStyle="default" value="${model.crmTask.dueDate}"/></div>
	</li>
	<li class="view" style="width: 99.9%;">
		<div style="float: left;"><fmt:message key="status" /> : <fmt:message key="taskStatus_${model.crmTask.status}" /></div>
		<div style="margin-left: 300px;"><fmt:message key="created" /> : <fmt:formatDate type="date" timeStyle="default" value="${model.crmTask.created}"/></div>
	</li>
	<li class="view" style="width: 99.9%;">
		<div style="float: left;"><fmt:message key="action" /> : <c:out value="${model.crmTask.action}" /></div>
		<div style="margin-left: 300px;"><fmt:message key="reminder" /> : <fmt:formatDate type="date" timeStyle="default" value="${model.crmTask.reminder}"/></div>	</li>
    <li class="view" style="height: 220px;overflow: auto;">
        <fmt:message key="description" /> : <c:out value="${model.crmTask.description}"  escapeXml="false"/>
     </li>	
     <li class="view" style="width: 600px;height: 2px;overflow: auto;"></li>
 </ul>