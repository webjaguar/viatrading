<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/security/tags" prefix="sec" %>

<c:set var="now" value="<%=new java.util.Date()%>" />
<tiles:insertDefinition name="admin.crm.task" flush="true">
  <tiles:putAttribute name="content" type="string">
  
<sec:authorize ifAnyGranted="ROLE_AEM,ROLE_ADMIN_${prop_site_id},ROLE_CRM"> 
<script type="text/javascript"><!--
	window.addEvent('domready', function(){
		var box2 = new multiBox('mbContact', {descClassName: 'multiBoxDesc',waitDuration: 5,showControls: false,overlay: new overlay()});
		$$('img.action').each(function(img){
			//containers
			var actionList = img.getParent(); 
			var actionHover = actionList.getElements('div.action-hover')[0];
			actionHover.set('opacity',0);
			//show/hide
			img.addEvent('mouseenter',function() {
				actionHover.setStyle('display','block').fade('in');
			});
			actionHover.addEvent('mouseleave',function(){
				actionHover.fade('out');
			});
			actionList.addEvent('mouseleave',function() {
				actionHover.fade('out');
			});
		});
		$$('div.taskQuickView').each(function(div){
			var taskId = div.get('id');
			var actionList = div.getParent(); 
			var taskQuickView = actionList.getElements('div.action-hover')[0];
			taskQuickView.set('opacity',0);
			div.addEvent('mouseenter',function() {
				this.setStyle('cursor','pointer'); 
				var request = new Request({
					url: "showAjaxTask.jhtm?id="+taskId,
					method: 'post',
					onComplete: function(response) { 
						$('taskQuickView'+taskId).innerHTML = "<span style='color:#c00000;'>"+response+"</span>";
					}
				}).send();
				taskQuickView.setStyle('display','block').fade('in');
			});
			taskQuickView.addEvent('mouseleave',function() {
				taskQuickView.fade('out');
				this.setStyle('cursor',''); 
			});
			actionList.addEvent('mouseleave',function() {
				taskQuickView.fade('out');
				this.setStyle('cursor',''); 
			});
		});	
	});	    
function toggleAll(el) {
	var ids = document.getElementsByName("__selected_id");	
  	if (ids.length == 1)
      document.list_form.__selected_id.checked = el.checked;
    else
      for (i = 0; i < ids.length; i++)
        document.list_form.__selected_id[i].checked = el.checked;	
} 
function optionsSelected() {
	var ids = document.getElementsByName("__selected_id");	
  	if (ids.length == 1) {
      if (document.list_form.__selected_id.checked) {
    	return true;
      }
    } else {
      for (i = 0; i < ids.length; i++) {
        if (document.list_form.__selected_id[i].checked) {
    	  return true;
        }
      }
    }

    alert("Please select contact(s).");       
    return false;
} 
function showTask(id)
{
if (id=="")
  {
  document.getElementById("ajaxTask").innerHTML="";
  return;
  }  
if (window.XMLHttpRequest)
  {// code for IE7+, Firefox, Chrome, Opera, Safari
  xmlhttp=new XMLHttpRequest();
  }
else
  {// code for IE6, IE5
  xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
  }
xmlhttp.onreadystatechange=function()
  {
  if (xmlhttp.readyState==4 )
    { 
    document.getElementById("ajaxTask"+id).innerHTML=xmlhttp.responseText;
    }
  }
xmlhttp.open("POST","showAjaxTask.jhtm?id="+id,true);
xmlhttp.send();
}
--></script>
<form action="crmTaskList.jhtm" method="post" id="list" name="list_form">
<input type="hidden" id="sort" name="sort" value="${crmTaskSearch.sort}" />
<!-- main box -->
  <div id="mboxfull">
  
  <!-- start table -->
  <table cellpadding="0" cellspacing="0" border="0" class="module" >
  <tr>
    <td class="topl_g">
	  
	  <!-- breadcrumb -->
	  <p class="breadcrumb">
	    <a href="../crm">CRM</a> &gt;
	    Tasks
	  </p>
	  
	  <!-- Error Message -->
	  
	  <!-- header image -->
	  <img class="headerImage" src="../graphics/task.gif" />
	  
	  </td><td class="topr_g" ></td></tr>
  <tr><td class="boxmidlrg" >

 <!-- tabs -->
 <div class="tab-wrapper">
	<h4 title="List of affiliates"></h4>
	<div>
	<!-- start tab -->

	  	<div class="listdivi"></div>
	   
		  	<div class="listlight">
		  	<!-- input field --> 
		  	<table border="0" cellpadding="0" cellspacing="1" width="100%">
			  <tr>
			  <c:if test="${model.count > 0}">
			  <td class="pageShowing">
			  <fmt:message key="showing">
				<fmt:param value="${crmTaskSearch.offset+1}"/>
				<fmt:param value="${model.pageEnd}"/>
				<fmt:param value="${model.count}"/>
			  </fmt:message>
			  </td>
			  </c:if>
			  <td class="pageNavi">
			  Page
			  <select name="page" id="page" onchange="submit()">
			  <c:forEach begin="1" end="${model.pageCount}" var="page">
			  	<option value="${page}" <c:if test="${page == (crmTaskSearch.page)}">selected</c:if>>${page}</option>
			  </c:forEach>
			  </select>
			  of <c:out value="${model.pageCount}"/>
			  | 
			  <c:if test="${crmTaskSearch.page == 1}"><span class="pageNaviDead">previous</span></c:if>
			  <c:if test="${crmTaskSearch.page != 1}"><a href="<c:url value="crmTaskList.jhtm"><c:param name="page" value="${crmTaskSearch.page-1}"/></c:url>" class="pageNaviLink">previous</a></c:if>
			  | 
			  <c:if test="${crmTaskSearch.page == model.pageCount}"><span class="pageNaviDead">next</span></c:if>
			  <c:if test="${crmTaskSearch.page != model.pageCount}"><a href="<c:url value="crmTaskList.jhtm"><c:param name="page" value="${crmTaskSearch.page+1}"/></c:url>" class="pageNaviLink">next</a></c:if>
			  </td>
			  </tr>
			</table>
			
			<table border="0" cellpadding="0" cellspacing="0" width="100%" class="listings">
			  <tr class="listingsHdr2">
			    <c:if test="${model.count > 0}">
			    <td align="left"><input type="checkbox" onclick="toggleAll(this)"></td>
			    </c:if>
			    <td class="indexCol">&nbsp;</td>
			    <td class="listingsHdr3">
			      <table cellspacing="0" cellpadding="1">
			    	  <tr>
			    	    <c:choose>
			    	      <c:when test="${crmTaskSearch.sort == 'title desc'}">
			    	        <td class="listingsHdr3">
			    	        <a class="hdrLink" href="#" onClick="document.getElementById('sort').value='title';document.getElementById('list').submit()"><fmt:message key="title" /></a>
			    	        </td>
			    	        <td><img src="../graphics/up.gif" border="0"></td>
			    	      </c:when>
			    	      <c:when test="${crmTaskSearch.sort == 'title'}">
			    	        <td class="listingsHdr3">
			    	        <a class="hdrLink" href="#" onClick="document.getElementById('sort').value='title desc';document.getElementById('list').submit()"><fmt:message key="title" /></a>
			    	        </td>
			    	        <td><img src="../graphics/down.gif" border="0"></td>
			    	      </c:when>
			    	      <c:otherwise>
			    	        <td colspan="2" class="listingsHdr3">
			    	        <a class="hdrLink" href="#" onClick="document.getElementById('sort').value='title desc';document.getElementById('list').submit()"><fmt:message key="title" /></a>
			    	        </td>
			    	      </c:otherwise>
			    	    </c:choose>  
			    	  </tr>
			      </table>
			    </td>
			    <td class="listingsHdr3">
			      <table cellspacing="0" cellpadding="1">
			    	  <tr>
			    	    <c:choose>
			    	      <c:when test="${crmTaskSearch.sort == 'account_name desc'}">
			    	        <td class="listingsHdr3">
			    	        <a class="hdrLink" href="#" onClick="document.getElementById('sort').value='account_name';document.getElementById('list').submit()"><fmt:message key="crmAccount" /></a>
			    	        </td>
			    	        <td><img src="../graphics/up.gif" border="0"></td>
			    	      </c:when>
			    	      <c:when test="${crmTaskSearch.sort == 'account_name'}">
			    	        <td class="listingsHdr3">
			    	        <a class="hdrLink" href="#" onClick="document.getElementById('sort').value='account_name desc';document.getElementById('list').submit()"><fmt:message key="crmAccount" /></a>
			    	        </td>
			    	        <td><img src="../graphics/down.gif" border="0"></td>
			    	      </c:when>
			    	      <c:otherwise>
			    	        <td colspan="2" class="listingsHdr3">
			    	        <a class="hdrLink" href="#" onClick="document.getElementById('sort').value='account_name desc';document.getElementById('list').submit()"><fmt:message key="crmAccount" /></a>
			    	        </td>
			    	      </c:otherwise>
			    	    </c:choose>  
			    	  </tr>
			      </table>
			    </td>
			    <td class="listingsHdr3">
			      <table cellspacing="0" cellpadding="1">
			    	  <tr>
			    	    <c:choose>
			    	      <c:when test="${crmTaskSearch.sort == 'contact_name desc'}">
			    	        <td class="listingsHdr3">
			    	        <a class="hdrLink" href="#" onClick="document.getElementById('sort').value='contact_name';document.getElementById('list').submit()"><fmt:message key="crmContact" /></a>
			    	        </td>
			    	        <td><img src="../graphics/up.gif" border="0"></td>
			    	      </c:when>
			    	      <c:when test="${crmTaskSearch.sort == 'contact_name'}">
			    	        <td class="listingsHdr3">
			    	        <a class="hdrLink" href="#" onClick="document.getElementById('sort').value='contact_name desc';document.getElementById('list').submit()"><fmt:message key="crmContact" /></a>
			    	        </td>
			    	        <td><img src="../graphics/down.gif" border="0"></td>
			    	      </c:when>
			    	      <c:otherwise>
			    	        <td colspan="2" class="listingsHdr3">
			    	        <a class="hdrLink" href="#" onClick="document.getElementById('sort').value='contact_name desc';document.getElementById('list').submit()"><fmt:message key="crmContact" /></a>
			    	        </td>
			    	      </c:otherwise>
			    	    </c:choose>  
			    	  </tr>
			      </table>
			    </td>
			    <td class="listingsHdr3">
			      <table cellspacing="0" cellpadding="1">
			    	  <tr>
			    	    <c:choose>
			    	      <c:when test="${crmTaskSearch.sort == 'created desc'}">
			    	        <td class="listingsHdr3">
			    	        <a class="hdrLink" href="#" onClick="document.getElementById('sort').value='created';document.getElementById('list').submit()"><fmt:message key="created" /></a>
			    	        </td>
			    	        <td><img src="../graphics/up.gif" border="0"></td>
			    	      </c:when>
			    	      <c:when test="${crmTaskSearch.sort == 'created'}">
			    	        <td class="listingsHdr3">
			    	        <a class="hdrLink" href="#" onClick="document.getElementById('sort').value='created desc';document.getElementById('list').submit()"><fmt:message key="created" /></a>
			    	        </td>
			    	        <td><img src="../graphics/down.gif" border="0"></td>
			    	      </c:when>
			    	      <c:otherwise>
			    	        <td colspan="2" class="listingsHdr3">
			    	        <a class="hdrLink" href="#" onClick="document.getElementById('sort').value='created desc';document.getElementById('list').submit()"><fmt:message key="created" /></a>
			    	        </td>
			    	      </c:otherwise>
			    	    </c:choose>  
			    	  </tr>
			      </table>
			    </td>
			    <td class="listingsHdr3">
			      <table cellspacing="0" cellpadding="1">
			    	  <tr>
			    	    <c:choose>
			    	      <c:when test="${crmTaskSearch.sort == 'due_date desc'}">
			    	        <td class="listingsHdr3">
			    	        <a class="hdrLink" href="#" onClick="document.getElementById('sort').value='due_date';document.getElementById('list').submit()"><fmt:message key="dueDate" /></a>
			    	        </td>
			    	        <td><img src="../graphics/up.gif" border="0"></td>
			    	      </c:when>
			    	      <c:when test="${crmTaskSearch.sort == 'due_date'}">
			    	        <td class="listingsHdr3">
			    	        <a class="hdrLink" href="#" onClick="document.getElementById('sort').value='due_date desc';document.getElementById('list').submit()"><fmt:message key="dueDate" /></a>
			    	        </td>
			    	        <td><img src="../graphics/down.gif" border="0"></td>
			    	      </c:when>
			    	      <c:otherwise>
			    	        <td colspan="2" class="listingsHdr3">
			    	        <a class="hdrLink" href="#" onClick="document.getElementById('sort').value='due_date desc';document.getElementById('list').submit()"><fmt:message key="dueDate" /></a>
			    	        </td>
			    	      </c:otherwise>
			    	    </c:choose>  
			    	  </tr>
			      </table>
			    </td>
			    <td class="listingsHdr3"><fmt:message key="assignedTo" /></td>
			    <td class="listingsHdr3">
			      <table cellspacing="0" cellpadding="1">
			    	  <tr>
			    	    <c:choose>
			    	      <c:when test="${crmTaskSearch.sort == 'status desc'}">
			    	        <td class="listingsHdr3">
			    	        <a class="hdrLink" href="#" onClick="document.getElementById('sort').value='status';document.getElementById('list').submit()"><fmt:message key="status" /></a>
			    	        </td>
			    	        <td><img src="../graphics/up.gif" border="0"></td>
			    	      </c:when>
			    	      <c:when test="${crmTaskSearch.sort == 'status'}">
			    	        <td class="listingsHdr3">
			    	        <a class="hdrLink" href="#" onClick="document.getElementById('sort').value='status desc';document.getElementById('list').submit()"><fmt:message key="status" /></a>
			    	        </td>
			    	        <td><img src="../graphics/down.gif" border="0"></td>
			    	      </c:when>
			    	      <c:otherwise>
			    	        <td colspan="2" class="listingsHdr3">
			    	        <a class="hdrLink" href="#" onClick="document.getElementById('sort').value='status desc';document.getElementById('list').submit()"><fmt:message key="status" /></a>
			    	        </td>
			    	      </c:otherwise>
			    	    </c:choose>  
			    	  </tr>
			      </table>
			    </td>
			    <td class="listingsHdr3">
			      <table cellspacing="0" cellpadding="1">
			    	  <tr>
			    	    <c:choose>
			    	      <c:when test="${crmTaskSearch.sort == 'priority desc'}">
			    	        <td class="listingsHdr3">
			    	        <a class="hdrLink" href="#" onClick="document.getElementById('sort').value='priority';document.getElementById('list').submit()"><fmt:message key="priority" /></a>
			    	        </td>
			    	        <td><img src="../graphics/up.gif" border="0"></td>
			    	      </c:when>
			    	      <c:when test="${crmTaskSearch.sort == 'priority'}">
			    	        <td class="listingsHdr3">
			    	        <a class="hdrLink" href="#" onClick="document.getElementById('sort').value='priority desc';document.getElementById('list').submit()"><fmt:message key="priority" /></a>
			    	        </td>
			    	        <td><img src="../graphics/down.gif" border="0"></td>
			    	      </c:when>
			    	      <c:otherwise>
			    	        <td colspan="2" class="listingsHdr3">
			    	        <a class="hdrLink" href="#" onClick="document.getElementById('sort').value='priority desc';document.getElementById('list').submit()"><fmt:message key="priority" /></a>
			    	        </td>
			    	      </c:otherwise>
			    	    </c:choose>  
			    	  </tr>
			      </table>
			    </td>
			    <td class="listingsHdr3"><fmt:message key="reminder" /></td>
			  </tr>
			<c:forEach items="${model.crmTaskList}" var="task" varStatus="status">
			  <tr class="row${status.index % 2}" onmouseover="changeStyleClass(this,'rowOver')" onmouseout="changeStyleClass(this,'row${status.index % 2}')">
			    <td align="left"><input name="__selected_id" value="${task.id}" type="checkbox"></td>
			    <td align="center">
			    <img class="action" width="23" height="16" alt="Actions" src="../graphics/actions.png"/>
			    <div class="action-hover" style="display: none;">
			        <ul class="round">
			            <li class="action-header">
					      <div class="name"><c:out value="${task.title}"/></div>
					      <div class="menudiv"></div>
					      <div style="position:relative">
					       <span style="width:16px;float:left;"><a href="crmContactQuickView.jhtm?id=${task.contactId}" rel="width:900,height:400" id="mb${task.contactId}" class="mbContact" title="<fmt:message key="crmContact" />ID : ${task.contactId}"><img src="../graphics/magnifier.gif" border="0" /></a></span>
					       <c:if test="${task.userId != null}">
					        <span style="width:16px;float:left;"><a href="../customers/customerQuickView.jhtm?cid=${task.userId}" rel="width:900,height:400" id="mb${task.userId}" class="mbContact" title="<fmt:message key="customer" />ID : ${task.userId}"><img src="../graphics/magnifier.gif" border="0" /></a></span>
					       </c:if>     
					      </div>  
			            </li>
			            <sec:authorize ifAnyGranted="ROLE_AEM,ROLE_ADMIN_${prop_site_id},ROLE_CRM">
				          <li class="edit"><a href="crmTaskForm.jhtm?taskId=${task.id}"><fmt:message key="edit" /></a></li>
				          <li class="edit"><a href="crmTaskForm.jhtm?taskId=${task.id}&fupt=followup"><fmt:message key="followUp" /></a></li>
				        </sec:authorize>
			        </ul>
			    </div>
				</td>
			    <td class="nameCol">
			      <div class="taskQuickView" id="${task.id}"><c:out value="${task.title}"/></div>
			      <div class="action-hover" style="padding-left:10px !important;width: 600px;" id="taskQuickView${task.id}"></div>
			    </td>
			    <td class="nameCol"><c:out value="${task.accountName}"/></td>
			    <td class="nameCol"><a href="crmContactForm.jhtm?id=${task.contactId}"><c:out value="${task.contactName}"/></a></td>
			    <td class="nameCol"><fmt:formatDate type="date" timeStyle="default" value="${task.created}"/></td>
			    <td class="nameCol"><fmt:formatDate type="date" timeStyle="default" value="${task.dueDate}"/></td>
			    <td class="nameCol"><c:out value="${task.assignedTo}"/></td>
			    <td class="nameCol"><fmt:message key="taskStatus_${task.status}" /></td>
			    <td class="nameCol"><c:out value="${task.priority}"/></td>
			    <td class="nameCol"><c:choose><c:when test="${task.reminder > now}"><img src="../graphics/reminder.gif" border="0" title="<fmt:formatDate type="both" timeStyle="full" value="${task.reminder}"/>"/></c:when></c:choose></td>
			  </tr>
			</c:forEach>
			<c:if test="${model.count == 0}">
			  <tr class="emptyList"><td colspan="3">&nbsp;</td></tr>
			</c:if>
			</table>
			
			<table border="0" cellpadding="0" cellspacing="1" width="100%">
			  <tr>
			  <td class="pageSize">
			  <select name="size" onchange="document.getElementById('page').value=1;submit()">
			  <c:forTokens items="10,25,50,100,500" delims="," var="current">
			  	  <option value="${current}" <c:if test="${current == crmTaskSearch.pageSize}">selected</c:if>>${current} <fmt:message key="perPage" /></option>
			  </c:forTokens>
			  </select>
			  </td>
			  </tr>
			</table>
		  	<!-- end input field -->  	  
		  	
		  	<!-- start button --> 
		  	<div align="left" class="button">
		  	  <sec:authorize ifAnyGranted="ROLE_AEM,ROLE_ADMIN_${prop_site_id},ROLE_CRM">
		  	    <input type="submit" name="__delete" value="<fmt:message key="delete" />" onClick="return confirm('Delete permanently?')">
		  	  </sec:authorize>
			</div>
			<!-- end button -->
			</div>
	
	<!-- end tab -->        
	</div>
<!-- end tabs -->			
</div>
  
  <!-- end table -->
   </td><td class="boxmidr" ></td></tr>
  <tr><td class="botl"></td><td class="botr"></td></tr>
  </table>
  
<!-- end main box -->  
</div> 
</form> 
</sec:authorize>

  </tiles:putAttribute>    
</tiles:insertDefinition>