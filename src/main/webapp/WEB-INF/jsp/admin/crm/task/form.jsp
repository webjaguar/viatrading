<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ taglib uri="/WEB-INF/tlds/spring.tld" prefix="spring" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ taglib uri="http://www.springframework.org/security/tags" prefix="sec" %>

<tiles:insertDefinition name="admin.crm.task" flush="true">
  <tiles:putAttribute name="content" type="string">
<sec:authorize ifAnyGranted="ROLE_AEM,ROLE_ADMIN_${prop_site_id},ROLE_CRM">
<script language="JavaScript"> 
<!--
window.addEvent('domready', function(){			
	var tabs1 = new SimpleTabs($('tab-block-1'), { selector: 'h4'});
	var Tips1 = new Tips($$('.toolTipImg'));
});
function disallowDate(date) {
	// date is a JS Date object
	var currentDate = new Date();
	if ( date.getFullYear() < currentDate.getFullYear() ) 
		return true; // disable July 5 2003
	else if ( date.getFullYear() == currentDate.getFullYear() ) 
	{
		if ( date.getMonth() < currentDate.getMonth() )
			return true;
		else if ( date.getMonth() == currentDate.getMonth() )
		{
			if ( date.getDate() < currentDate.getDate() )
				return true;
			else
				return false;
		}
		else
			return false;
	}				
	else
		return false; // enable other dates
};
//--> 
</script> 
<form:form commandName="crmTaskForm" action="crmTaskForm.jhtm" method="post">
<input type="hidden" name="taskId" value="${crmTaskForm.crmTask.id}" />
<form:hidden path="newCrmTask" />

<!-- main box -->
  <div id="mboxfull">
  
  <!-- start table -->
  <table cellpadding="0" cellspacing="0" border="0" class="module" >
  <tr>
    <td class="topl_g">
	  
	  <!-- breadcrumb -->
	  <p class="breadcrumb">
	    <a href="../crm">CRM</a> &gt;
	    <a href="../crm/crmAccountForm.jhtm?id=${crmTaskForm.crmTask.accountId}"><c:out value="${crmTaskForm.crmTask.accountName}" /></a> &gt;
	    <a href="../crm/crmContactForm.jhtm?id=${crmTaskForm.crmTask.contactId}&accountId=${crmTaskForm.crmTask.accountId}"><c:out value="${crmTaskForm.crmTask.contactName}" /></a> &gt;
	    <a href="../crm/crmContactList.jhtm">Contacts</a> &gt;
	    form 
	  </p>
	  
	  <!-- Error Message -->
	  <c:if test="${message != null}">
  		<div class="message"><fmt:message key="${message}"/></div>
  	  </c:if>
  	  <spring:hasBindErrors name="crmContactForm">
		<div class="message">Please fix all errors!</div>
	  </spring:hasBindErrors>
	  

  </td><td class="topr_g" ></td></tr>
  <tr><td class="boxmidlrg" >

 <!-- tabs -->
 <div id="tab-block-1">
	<h4 title="Task Information">Task Information</h4>
	<div>
	<!-- start tab -->

         <c:set var="classIndex" value="0" />
		<div class="listdivi ln tabdivi"></div>
		 
		 <div class="list${classIndex % 2}"><c:set var="classIndex" value="${classIndex+1}" />
		 <div style="float:left;width:50%">
		 <div class="listfl"><div class="requiredField"><fmt:message key="crmAccountName" />:</div></div>
		 <div class="listp">
		 <!-- input field -->
				<span class="static"><c:out value="${crmTaskForm.crmTask.accountName}" /></span>
		 <!-- end input field -->	
	 	 </div>
	 	 </div>
	 	 
	 	 <div style="float:right;width:50%">
		 <div class="listfl"><fmt:message key="phone" />:</div>
		 <div class="listp">
		 <!-- input field -->
				<span class="static"><c:out value="${crmTaskForm.crmTask.contactPhone}" /></span>
		 <!-- end input field -->	
	 	 </div>
	 	 </div>
	 	 
	 	 <div style="float:left;width:50%">
	 	 <div class="listfl"><div class="requiredField"><fmt:message key="crmContactName" />:</div></div>
	 	 <div class="listp">
		 <!-- input field -->
				<span class="static"><c:out value="${crmTaskForm.crmTask.contactName}" /></span>
		 <!-- end input field -->	
	 	 </div>
		 </div>
	 	 
	 	 <div style="float:right;width:50%">
	 	 <div class="listfl"><fmt:message key="email" />:</div>
	 	 <div class="listp">
		 <!-- input field -->
				<span class="static"><c:out value="${crmTaskForm.crmTask.contactEmail}" /></span>
		 <!-- end input field -->	
	 	 </div>
		 </div>
		 </div>
		 
		 <div class="list${classIndex % 2}"><c:set var="classIndex" value="${classIndex+1}" />
		 <div class="listfl"><div class="requiredField"><fmt:message key="title" />:</div></div>
		 <div class="listp">
		 <!-- input field -->
	    		<form:input path="crmTask.title" cssClass="textfield" maxlength="255" size="60" htmlEscape="true"/>
		  		<form:errors path="crmTask.title" cssClass="error"/>
	 	 <!-- end input field -->	
	 	 </div>
		 </div>
		 
		 <div class="list${classIndex % 2}"><c:set var="classIndex" value="${classIndex+1}" />
		 <div class="listfl"><div class="requiredField"><fmt:message key="type" />:</div></div>
		 <div class="listp">
		 <!-- input field -->
	    		<form:select path="crmTask.type">
	    		<form:option value="" label="Please Select" />
	    		<c:forTokens items="${model.taskTypes}" delims="," var="type" >
	    		  <form:option value="${type}" label="${type}"/>
	    		</c:forTokens>
	    		</form:select>
		        <form:errors path="crmTask.type" cssClass="error"/>
	 	 <!-- end input field -->	
	 	 </div>
		 </div>
		 
		 <div class="list${classIndex % 2}"><c:set var="classIndex" value="${classIndex+1}" />
		 <div class="listfl"><div class="requiredField"><fmt:message key="assignedTo" />:</div></div>
		 <div class="listp">
		 <!-- input field -->
	    		<form:select path="crmTask.assignedToId">
		              <option value="0" ><fmt:message key="admin" /></option>
		              <form:options items="${model.userList}" itemValue="id" itemLabel="name"/>
		        </form:select>
		        <form:errors path="crmTask.assignedTo" cssClass="error"/>
	 	 <!-- end input field -->	
	 	 </div>
		 </div>
		 
		 <div class="list${classIndex % 2}"><c:set var="classIndex" value="${classIndex+1}" />
		 <div class="listfl"><fmt:message key="created" />:</div>
		 <div class="listp">
		 <!-- input field -->
				<form:input path="crmTask.created" cssClass="textFieldShort" size="10" maxlength="10" />
				  <img src="../graphics/calendarIcon.jpg" class="calendarImage" id="created_trigger"/>
				  <script type="text/javascript">
		    		Calendar.setup({
		        			inputField     :    "crmTask.created",   // id of the input field
		        			showsTime      :    false,
		        			ifFormat       :    "%m/%d/%Y",   // format of the input field
		        			button         :    "created_trigger"   // trigger for the calendar (button ID)
        			});	
				  </script> 
				<form:errors path="crmTask.dueDate" cssClass="error"/>	 
	 		<!-- end input field -->		
	 	 </div>
		 </div>
		 
		 <div class="list${classIndex % 2}"><c:set var="classIndex" value="${classIndex+1}" />
		 <div class="listfl"><fmt:message key="dueDate" />:</div>
		 <div class="listp">
		 <!-- input field -->
				<form:input path="crmTask.dueDate" cssClass="textFieldShort" size="10" maxlength="10" />
				  <img src="../graphics/calendarIcon.jpg" class="calendarImage" id="due_date_trigger"/>
				  <script type="text/javascript">
		    		Calendar.setup({
		        			inputField     :    "crmTask.dueDate",   // id of the input field
		        			showsTime      :    false,
		        			ifFormat       :    "%m/%d/%Y",   // format of the input field
		        			button         :    "due_date_trigger",   // trigger for the calendar (button ID)
        					disableFunc    :    disallowDate
		    		});	
				  </script> 
				<form:errors path="crmTask.dueDate" cssClass="error"/>	 
	 		<!-- end input field -->		
	 	 </div>
		 </div>
		
		 <div class="list${classIndex % 2}"><c:set var="classIndex" value="${classIndex+1}" />
		 <div class="listfl"><fmt:message key="description" />:</div>
		 <div class="listp">
		 <!-- input field -->
	    		<form:textarea path="crmTask.description" cols="45" rows="10" cssClass="textfield" htmlEscape="true"/>
		  		<form:errors path="crmTask.description" cssClass="error"/>
	 	 <!-- end input field -->	
	 	 </div>
		 </div>
		 
		 <div class="list${classIndex % 2}"><c:set var="classIndex" value="${classIndex+1}" />
		 <div class="listfl"><fmt:message key="rank" />:</div>
		 <div class="listp">
		 <!-- input field -->
				<form:select path="crmTask.priority">
				<form:option value="" label="Please Select" />
	    		<c:forTokens items="${model.taskRanks}" delims="," var="rank" >
	    		  <form:option value="${rank}" label="${rank}"/>
	    		</c:forTokens>
	    		</form:select>
		 <!-- end input field -->	
	 	 </div>
		 </div>
		 
		 <div class="list${classIndex % 2}"><c:set var="classIndex" value="${classIndex+1}" />
		 <div class="listfl"><fmt:message key="status" />:</div>
		 <div class="listp">
		 <!-- input field -->
				<form:select path="crmTask.status">
					<form:option value="" label="Please Select" />
	    			<form:option value="01"><fmt:message key="taskStatus_01" /></form:option>
					<form:option value="02"><fmt:message key="taskStatus_02" /></form:option>
					<form:option value="03"><fmt:message key="taskStatus_03" /></form:option>
					<form:option value="04"><fmt:message key="taskStatus_04" /></form:option>
					<form:option value="05"><fmt:message key="taskStatus_05" /></form:option>
					<form:option value="06"><fmt:message key="taskStatus_06" /></form:option>
				</form:select>
		 <!-- end input field -->	
	 	 </div>
		 </div>
		 
		 <div class="list${classIndex % 2}"><c:set var="classIndex" value="${classIndex+1}" />
		 <div class="listfl"><fmt:message key="action" />:</div>
		 <div class="listp">
		 <!-- input field -->
				<form:select path="crmTask.action">
				<form:option value="" label="Please Select" />
	    		<c:forTokens items="${model.taskActions}" delims="," var="action" >
	    		  <form:option value="${action}" label="${action}"/>
	    		</c:forTokens>
	    		</form:select>
		 <!-- end input field -->	
	 	 </div>
		 </div>
		 
		 <div class="list${classIndex % 2}"><c:set var="classIndex" value="${classIndex+1}" />
		 <div class="listfl"><fmt:message key="reminder" />:</div>
		 <div class="listp">
		 <!-- input field -->
		        <span class="helpNote"><p><fmt:message key='currentTime' />: <c:out value="${model.currentTime}"></c:out></p></span>
				<form:input path="crmTask.reminder" cssClass="textFieldShort" size="10" maxlength="10" />
				  <img src="../graphics/calendarIcon.jpg" class="calendarImage" id="reminder_trigger"/>
				  <script type="text/javascript">
		    		Calendar.setup({
		        			inputField     :    "crmTask.reminder",   // id of the input field
		        			showsTime      :    false,
		        			ifFormat       :    "%m/%d/%Y",   // format of the input field
		        			button         :    "reminder_trigger",   // trigger for the calendar (button ID)
        					disableFunc    :    disallowDate
		    		});	
				  </script> 
				<form:errors path="crmTask.reminder" cssClass="error"/>	
				  <form:select path="crmTask.reminderTime"><form:option value="0">12:00 AM</form:option><form:option value="30">12:30 AM</form:option><form:option value="60">1:00 AM</form:option><form:option value="90">1:30 AM</form:option><form:option value="120">2:00 AM</form:option><form:option value="150">2:30 AM</form:option><form:option value="180">3:00 AM</form:option><form:option value="210">3:30 AM</form:option><form:option value="240">4:00 AM</form:option><form:option value="270">4:30 AM</form:option><form:option value="300">5:00 AM</form:option><form:option value="330">5:30 AM</form:option><form:option value="360">6:00 AM</form:option><form:option value="390">6:30 AM</form:option><form:option value="420">7:00 AM</form:option><form:option value="450">7:30 AM</form:option><form:option value="480">8:00 AM</form:option><form:option value="510">8:30 AM</form:option><form:option value="540">9:00 AM</form:option><form:option value="570">9:30 AM</form:option><form:option value="600">10:00 AM</form:option><form:option value="630">10:30 AM</form:option><form:option value="660">11:00 AM</form:option><form:option value="690">11:30 AM</form:option><form:option value="720">12:00 PM</form:option><form:option value="750">12:30 PM</form:option><form:option value="780">1:00 PM</form:option><form:option value="810">1:30 PM</form:option><form:option value="840">2:00 PM</form:option><form:option value="870">2:30 PM</form:option><form:option value="900">3:00 PM</form:option><form:option value="930">3:30 PM</form:option><form:option value="960">4:00 PM</form:option><form:option value="990">4:30 PM</form:option><form:option value="1020">5:00 PM</form:option><form:option value="1050">5:30 PM</form:option><form:option value="1080">6:00 PM</form:option><form:option value="1110">6:30 PM</form:option><form:option value="1140">7:00 PM</form:option><form:option value="1170">7:30 PM</form:option><form:option value="1200">8:00 PM</form:option><form:option value="1230">8:30 PM</form:option><form:option value="1260">9:00 PM</form:option><form:option value="1290">9:30 PM</form:option><form:option value="1320">10:00 PM</form:option><form:option value="1350">10:30 PM</form:option><form:option value="1380">11:00 PM</form:option><form:option value="1410">11:30 PM</form:option>
				</form:select>
	 		<!-- end input field -->		
	 	 </div>
		 </div>
		
	<!-- end tab -->        
	</div>    

<!-- end tabs -->			
</div>

<!-- start button -->
	<div align="left" class="button">
	  <c:if test="${crmTaskForm.newCrmTask}">
	    <c:if test="${!scheduleDone}" >
	    <sec:authorize ifAnyGranted="ROLE_AEM,ROLE_ADMIN_${prop_site_id},ROLE_CRM">
          <input type="submit" value="<spring:message code="add"/>" />
        </sec:authorize> 
        </c:if>
	    <c:if test="${scheduleDone}" >
	      <input type="submit" name="__back" value="<fmt:message key="back" />" />
	    </c:if> 
      </c:if>
      <c:if test="${!crmTaskForm.newCrmTask}">
        <sec:authorize ifAnyGranted="ROLE_AEM,ROLE_ADMIN_${prop_site_id},ROLE_CRM">
          <input type="submit" value="<spring:message code="update"/>" />
          <input type="submit" value="Update and Follow Up" name="_followUp" />
        </sec:authorize>
        
        <sec:authorize ifAnyGranted="ROLE_AEM,ROLE_ADMIN_${prop_site_id},ROLE_CRM">  
          <input type="submit" value="<spring:message code="delete"/>" name="_delete" onClick="return confirm('Delete permanently?')" />
        </sec:authorize> 
        
      </c:if>
	</div>
<!-- end button -->
  
  <!-- end table -->
    </td><td class="boxmidr" ></td></tr>
  <tr><td class="botl"></td><td class="botr"></td></tr>
  </table>
  
<!-- end main box -->  
</div>
</form:form>
</sec:authorize> 

  </tiles:putAttribute>    
</tiles:insertDefinition>