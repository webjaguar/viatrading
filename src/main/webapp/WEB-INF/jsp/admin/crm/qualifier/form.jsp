<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ taglib uri="/WEB-INF/tlds/spring.tld" prefix="spring" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ taglib uri="http://www.springframework.org/security/tags" prefix="sec" %>
<script type="text/javascript" src="../javascript/autocompleter1.2.js"></script>
<script type="text/javascript" src="../javascript/autocompleter.Request1.2.js"></script>
<script type="text/javascript" src="../javascript/observer.js"></script>

<tiles:insertDefinition name="admin.crm.qualifier" flush="true">
  <tiles:putAttribute name="content" type="string">

<script language="JavaScript"> 
<!--
window.addEvent('domready', function(){			
	var tabs1 = new SimpleTabs($('tab-block-1'), { selector: 'h4'});
	var Tips1 = new Tips($$('.toolTipImg'));
});
//--> 
</script> 
<form:form commandName="crmQualifierForm" action="crmQualifierForm.jhtm" method="post">
<
<!-- main box -->
  <div id="mboxfull">
  
  <!-- start table -->
  <table cellpadding="0" cellspacing="0" border="0" class="module" >
  <tr>
    <td class="topl_g">
	  
	  <!-- breadcrumb -->
	  <p class="breadcrumb">
	    <a href="../qualifier">CRM</a> &gt;
	    <a href="../qualifier">Qualifier</a> &gt;
	    form 
	  </p>
	  

  </td><td class="topr_g" ></td></tr>
  <tr><td class="boxmidlrg" >

	<h4 title="<fmt:message key='qualifier'/> Form"><fmt:message key="qualifier" /></h4>
	<div>
	<!-- start tab -->
         <c:set var="classIndex" value="0" />
         
	  	<div class="listdivi ln tabdivi"></div>
	  	<div class="listdivi"></div>
			
			<div class="list${classIndex % 2}"><c:set var="classIndex" value="${classIndex+1}" />
		  	<div class="listfl"><div class="helpImg"><img class="toolTipImg" title="Inactive::Checking this, will not allow this crmQualifier to be displayed on crmQualifier dropdown. Only when this crmQualifier doesn't have customer, can check this." src="../graphics/question.gif" /></div><fmt:message key="inactive" />:</div>
		  	<div class="listp">
		  	<!-- input field -->
	   			<form:checkbox path="crmQualifier.inactive" />
		    <!-- end input field -->   	
		  	</div>
		  	</div>
	   
		  	<div class="list${classIndex % 2}"><c:set var="classIndex" value="${classIndex+1}" />
		  	<div class="listfl"><fmt:message key='accountNumber' />:</div>
		  	<div class="listp">
		  	<!-- input field -->
				<form:input path="crmQualifier.accountNumber" htmlEscape="true"/>
				<form:errors path="crmQualifier.accountNumber" cssClass="error"/>
			<!-- end input field -->	
	 		</div>
		  	</div>
		  	
		  	<div class="list${classIndex % 2}"><c:set var="classIndex" value="${classIndex+1}" />
		  	<div class="listfl"><fmt:message key='group' />:</div>
		  	<div class="listp">
		  	<!-- input field -->
				<form:input path="crmQualifier.salesrepGroup" htmlEscape="true"/>
				<form:errors path="crmQualifier.salesrepGroup" cssClass="error"/>
			<!-- end input field -->	
	 		</div>
		  	</div>		
		  	
		  	<div class="list${classIndex % 2}"><c:set var="classIndex" value="${classIndex+1}" />
		  	<div class="listfl"><div class="requiredField"><fmt:message key='Name' />:</div></div>
		  	<div class="listp">
		  	<!-- input field -->
				<form:input path="crmQualifier.name" htmlEscape="true"/>
				<form:errors path="crmQualifier.name" cssClass="error"/>
				<div class="helpNote">e.g. John Smith</div>	 		
			<!-- end input field -->	
	 		</div>
		  	</div>	
		  	
		  	<div class="list${classIndex % 2}"><c:set var="classIndex" value="${classIndex+1}" />
		  	<div class="listfl"><div class="requiredField"><fmt:message key='email' />:</div></div>
		  	<div class="listp">
		  	<!-- input field -->
				<form:input path="crmQualifier.email" htmlEscape="true"/>
				<form:errors path="crmQualifier.email" cssClass="error"/>
				<div class="helpNoteEnd">e.g. johnsmith@webjaguar.com</div>
	 		<!-- end input field -->	
	 		</div>
		  	</div>
		  	
		  	<div class="list${classIndex % 2}"><c:set var="classIndex" value="${classIndex+1}" />
		  	<div class="listfl"><fmt:message key="company" />:</div>
		  	<div class="listp">
		  	<!-- input field -->
         		<form:input path="crmQualifier.address.company"  htmlEscape="true" />
       			<form:errors path="crmQualifier.address.company" cssClass="error" />
		    <!-- end input field -->   	
		  	</div>
		  	</div>
		  	
		  	<div class="list${classIndex % 2}"><c:set var="classIndex" value="${classIndex+1}" />
		  	<div class="listfl"><fmt:message key="address" /> 1:</div>
		  	<div class="listp">
		  	<!-- input field -->
         		<form:input path="crmQualifier.address.addr1" htmlEscape="true" />
       			<form:errors path="crmQualifier.address.addr1" cssClass="error" />
		    <!-- end input field -->   	
		  	</div>
		  	</div>	
		  	
		  	<div class="list${classIndex % 2}"><c:set var="classIndex" value="${classIndex+1}" />
		  	<div class="listfl"><fmt:message key="address" /> 2:</div>
		  	<div class="listp">
		  	<!-- input field -->
         		<form:input path="crmQualifier.address.addr2" htmlEscape="true" />
       			<form:errors path="crmQualifier.address.addr2" cssClass="error" />
		    <!-- end input field -->   	
		  	</div>
		  	</div>	
		  	
		  	<div class="list${classIndex % 2}"><c:set var="classIndex" value="${classIndex+1}" />
		  	<div class="listfl"><fmt:message key='country' />:</div>
		  	<div class="listp">
		  	<!-- input field -->
         		<form:select id="country" path="crmQualifier.address.country" onchange="toggleStateProvince(this)">
		         <form:option value="" label="Please Select"/>
		         <form:options items="${countries}" itemValue="code" itemLabel="name"/>
		        </form:select>
		        <form:errors path="crmQualifier.address.country" cssClass="error" />
		    <!-- end input field -->   	
		  	</div>
		  	</div>	
		  	
		  	<div class="list${classIndex % 2}"><c:set var="classIndex" value="${classIndex+1}" />
		  	<div class="listfl"><fmt:message key="city" />:</div>
		  	<div class="listp">
		  	<!-- input field -->
         		<form:input path="crmQualifier.address.city" htmlEscape="true" />
       			<form:errors path="crmQualifier.address.city" cssClass="error" />
		    <!-- end input field -->   	
		  	</div>
		  	</div>	
		  	
		  	<div class="list${classIndex % 2}"><c:set var="classIndex" value="${classIndex+1}" />
		  	<div class="listfl"><fmt:message key="stateProvince" />:</div>
		  	<div class="listp">
		  	<!-- input field -->
		       <form:select id="state" path="crmQualifier.address.stateProvince">
		         <form:option value="" label="Please Select"/>
		         <form:options items="${states}" itemValue="code" itemLabel="name"/>
		       </form:select>
		       <form:select id="ca_province" path="crmQualifier.address.stateProvince">
		         <form:option value="" label="Please Select"/>
		         <form:options items="${caProvinceList}" itemValue="code" itemLabel="name"/>
		       </form:select>
		       <form:input id="province" path="crmQualifier.address.stateProvince" htmlEscape="true"/>
		       <form:errors path="crmQualifier.address.stateProvince" cssClass="error" />
		    <!-- end input field -->   	
		  	</div>
		  	</div>	
		  	
		  	<div class="list${classIndex % 2}"><c:set var="classIndex" value="${classIndex+1}" />
		  	<div class="listfl"><fmt:message key="zipCode" />:</div>
		  	<div class="listp">
		  	<!-- input field -->
         		<form:input path="crmQualifier.address.zip" htmlEscape="true" />
       			<form:errors path="crmQualifier.address.zip" cssClass="error" />
		    <!-- end input field -->   	
		  	</div>
		  	</div>	
		  	
		  	<div class="list${classIndex % 2}"><c:set var="classIndex" value="${classIndex+1}" />
		  	<div class="listfl"><fmt:message key='phone' />:</div>
		  	<div class="listp">
		  	<!-- input field -->
				<form:input path="crmQualifier.phone" htmlEscape="true"/>
				<form:errors path="crmQualifier.phone" cssClass="error"/>
	 		<!-- end input field -->	
	 		</div>
		  	</div>

		  	<div class="list${classIndex % 2}"><c:set var="classIndex" value="${classIndex+1}" />
		  	<div class="listfl"><fmt:message key="fax" />:</div>
		  	<div class="listp">
		  	<!-- input field -->
         		<form:input path="crmQualifier.address.fax"  htmlEscape="true" />
       			<form:errors path="crmQualifier.address.fax" cssClass="error" />
		    <!-- end input field -->   	
		  	</div>
		  	</div>	
		  	
		  	<div class="list${classIndex % 2}"><c:set var="classIndex" value="${classIndex+1}" />
		  	<div class="listfl"><fmt:message key='cellPhone' />:</div>
		  	<div class="listp">
		  	<!-- input field -->
				<form:input path="crmQualifier.cellPhone" htmlEscape="true"/>
				<form:errors path="crmQualifier.cellPhone" cssClass="error"/>
	 		<!-- end input field -->	
	 		</div>
		  	</div>	
	<!-- end tab -->		
	</div>


  <div align="left" class="button">
      <input type="submit" value="<spring:message code="add"/>" name="_add"/>
      <input type="submit" value="<spring:message code="delete"/>" name="_delete"/>
      <input type="submit" value="<spring:message code="update"/>" name="_update"/>
  </div>

  
  <!-- end table -->
    </td><td class="boxmidr" ></td></tr>
  <tr><td class="botl"></td><td class="botr"></td></tr>
  </table>
  
<!-- end main box -->  
</div>
</form:form>

<script language="JavaScript">
<!--
function toggleStateProvince(el) {
  if (el.value == "US") {
      document.getElementById('state').disabled=false;
      document.getElementById('state').style.display="block";
      document.getElementById('ca_province').disabled=true;
      document.getElementById('ca_province').style.display="none";
      document.getElementById('province').disabled=true;
      document.getElementById('province').style.display="none";
  } else if (el.value == "CA") {
      document.getElementById('state').disabled=true;
      document.getElementById('state').style.display="none";
      document.getElementById('ca_province').disabled=false;
      document.getElementById('ca_province').style.display="block";
      document.getElementById('province').disabled=true;
      document.getElementById('province').style.display="none";
  } else {
      document.getElementById('state').disabled=true;
      document.getElementById('state').style.display="none";
      document.getElementById('ca_province').disabled=true;
      document.getElementById('ca_province').style.display="none";
      document.getElementById('province').disabled=false;
      document.getElementById('province').style.display="block";
  }
}
toggleStateProvince(document.getElementById('country'));
//-->
</script>


  </tiles:putAttribute>    
</tiles:insertDefinition>