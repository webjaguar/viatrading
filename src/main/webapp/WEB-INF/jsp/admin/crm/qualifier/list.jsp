<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/security/tags" prefix="sec" %>

<tiles:insertDefinition name="admin.crm.qualifier" flush="true">
  <tiles:putAttribute name="content" type="string">
  
<sec:authorize ifAnyGranted="ROLE_AEM,ROLE_ADMIN_${prop_site_id},ROLE_CRM"> 
<script type="text/javascript"><!--
	window.addEvent('domready', function(){
		$$('img.action').each(function(img){
			//containers
			var actionList = img.getParent(); 
			var actionHover = actionList.getElements('div.action-hover')[0];
			actionHover.set('opacity',0);
			//show/hide
			img.addEvent('mouseenter',function() {
				actionHover.setStyle('display','block').fade('in');
			});
			actionHover.addEvent('mouseleave',function(){
				actionHover.fade('out');
			});
			actionList.addEvent('mouseleave',function() {
				actionHover.fade('out');
			});
		});
	});	    
function toggleAll(el) {
	var ids = document.getElementsByName("__selected_id");	
  	if (ids.length == 1)
      document.list_form.__selected_id.checked = el.checked;
    else
      for (i = 0; i < ids.length; i++)
        document.list_form.__selected_id[i].checked = el.checked;	
} 
function optionsSelected() {
	var ids = document.getElementsByName("__selected_id");	
  	if (ids.length == 1) {
      if (document.list_form.__selected_id.checked) {
    	return true;
      }
    } else {
      for (i = 0; i < ids.length; i++) {
        if (document.list_form.__selected_id[i].checked) {
    	  return true;
        }
      }
    }

    alert("Please select customer(s).");       
    return false;
} 	
--></script>
<form action="crmQualifierList.jhtm" method="post" id="list" name="list_form">
<input type="hidden" id="sort" name="sort" value="${crmQualifierSearch.sort}" />
<!-- main box -->
  <div id="mboxfull">
  
  <!-- start table -->
  <table cellpadding="0" cellspacing="0" border="0" class="module" >
  <tr>
    <td class="topl_g">
	  
	  <!-- breadcrumb -->
	  <p class="breadcrumb">
	    <a href="../crm">CRM</a> &gt;
	    Qualifiers 
	  </p>
	  
	  <!-- Error Message -->
	  
	  <!-- header image -->
	  <img class="headerImage" src="../graphics/category.gif" />
	  
	  </td><td class="topr_g" ></td></tr>
  <tr><td class="boxmidlrg" >

 <!-- tabs -->
 <div class="tab-wrapper">
	<h4 title="List of affiliates"></h4>
	<div>
	<!-- start tab -->

	  	<div class="listdivi"></div>
	   
		  	<div class="listlight">
		  	<!-- input field --> 
		  	<table border="0" cellpadding="0" cellspacing="1" width="100%">
			  <tr>
			  <c:if test="${model.count > 0}">
			  <td class="pageShowing">
			  <fmt:message key="showing">
				<fmt:param value="${crmQualifierSearch.offset+1}"/>
				<fmt:param value="${model.pageEnd}"/>
				<fmt:param value="${model.count}"/>
			  </fmt:message>
			  </td>
			  </c:if>
			  <td class="pageNavi">
			  Page
			  <select name="page" id="page" onchange="submit()">
			  <c:forEach begin="1" end="${model.pageCount}" var="page">
			  	<option value="${page}" <c:if test="${page == (crmQualifierSearch.page)}">selected</c:if>>${page}</option>
			  </c:forEach>
			  </select>
			  of <c:out value="${model.pageCount}"/>
			  | 
			  <c:if test="${crmQualifierSearch.page == 1}"><span class="pageNaviDead">previous</span></c:if>
			  <c:if test="${crmQualifierSearch.page != 1}"><a href="<c:url value="crmQualifierList.jhtm"><c:param name="page" value="${crmQualifierSearch.page-1}"/></c:url>" class="pageNaviLink">previous</a></c:if>
			  | 
			  <c:if test="${crmQualifierSearch.page == model.pageCount}"><span class="pageNaviDead">next</span></c:if>
			  <c:if test="${crmQualifierSearch.page != model.pageCount}"><a href="<c:url value="crmQualifierList.jhtm"><c:param name="page" value="${crmQualifierSearch.page+1}"/></c:url>" class="pageNaviLink">next</a></c:if>
			  </td>
			  </tr>
			</table>
			
			<table border="0" cellpadding="0" cellspacing="0" width="100%" class="listings">
			  <tr class="listingsHdr2">
			    <td class="indexCol">&nbsp;</td>
			    <td class="listingsHdr3">
			      <table cellspacing="0" cellpadding="1">
			    	  <tr>
			    	    <c:choose>
			    	      <c:when test="${crmQualifierSearch.sort == 'name desc'}">
			    	        <td class="listingsHdr3">
			    	        <a class="hdrLink" href="#" onClick="document.getElementById('sort').value='name';document.getElementById('list').submit()"><fmt:message key="qualifierName" /></a>
			    	        </td>
			    	        <td><img src="../graphics/up.gif" border="0"></td>
			    	      </c:when>
			    	      <c:when test="${crmQualifierSearch.sort == 'name'}">
			    	        <td class="listingsHdr3">
			    	        <a class="hdrLink" href="#" onClick="document.getElementById('sort').value='name desc';document.getElementById('list').submit()"><fmt:message key="qualifierName" /></a>
			    	        </td>
			    	        <td><img src="../graphics/down.gif" border="0"></td>
			    	      </c:when>
			    	      <c:otherwise>
			    	        <td colspan="2" class="listingsHdr3">
			    	        <a class="hdrLink" href="#" onClick="document.getElementById('sort').value='name desc';document.getElementById('list').submit()"><fmt:message key="qualifierName" /></a>
			    	        </td>
			    	      </c:otherwise>
			    	    </c:choose>  
			    	  </tr>
			    	 </table>
			    </td>
			  </tr>    
			<c:forEach items="${model.crmQualifierList}" var="qualifier" varStatus="status">
			  <tr class="row${status.index % 2}" onmouseover="changeStyleClass(this,'rowOver')" onmouseout="changeStyleClass(this,'row${status.index % 2}')">
			    <td align="center">
			    <img class="action" width="23" height="16" alt="Actions" src="../graphics/actions.png"/>
			    <div class="action-hover" style="display: none;">
			        <ul class="round">
			            <li class="action-header">
					      <div class="name"><c:out value="${qualifier.name}"/></div>
					      <div class="menudiv"></div>
					      <div style="position:relative">
					      <%-- <span style="width:16px;float:left;"><a href="customerQuickView.jhtm?cid=${customer.id}" rel="width:900,height:400" id="mb${customer.id}" class="mbCustomer" title="<fmt:message key="customer" />ID : ${customer.id}"><img src="../graphics/magnifier.gif" border="0" /></a></span>--%>
					      </div>  
			            </li>
			            
			              <li class="link"><a href="crmQualifierForm.jhtm?id=${qualifier.id}"><fmt:message key="edit" /></a></li>
				          <li class="link"><a href="crmContactForm.jhtm?qualifierId=${qualifier.id}"><fmt:message key="addContact" /></a></li>
				          <li class="link"><a href="crmContactList.jhtm?crmQualifierId=${qualifier.id}"><fmt:message key="viewContact" /></a></li>
				      
			        </ul>
			    </div>
				</td>
			    <td class="nameCol"><a href="crmQualifierForm.jhtm?id=${qualifier.id}"><c:out value="${qualifier.name}"/></a></td>
			  </tr>
			</c:forEach>
			<c:if test="${model.count == 0}">
			  <tr class="emptyList"><td colspan="3">&nbsp;</td></tr>
			</c:if>
			</table>
			
			<table border="0" cellpadding="0" cellspacing="1" width="100%">
			  <tr>
			  <td class="pageSize">
			  <select name="size" onchange="document.getElementById('page').value=1;submit()">
			  <c:forTokens items="10,25,50" delims="," var="current">
			  	  <option value="${current}" <c:if test="${current == crmQualifierSearch.pageSize}">selected</c:if>>${current} <fmt:message key="perPage" /></option>
			  </c:forTokens>
			  </select>
			  </td>
			  </tr>
			</table>
		  	<!-- end input field -->  	  
		  	
		  	<!-- start button --> 
		  	<div align="left" class="button">
		  	  <sec:authorize ifAnyGranted="ROLE_AEM,ROLE_ADMIN_${prop_site_id},ROLE_CRM">
		  	    <input type="submit" name="__add" value="<fmt:message key="add" />">
		  	  </sec:authorize>
			</div>
			<!-- end button -->
			</div>
	
	<!-- end tab -->        
	</div>
<!-- end tabs -->			
</div>
  
  <!-- end table -->
   </td><td class="boxmidr" ></td></tr>
  <tr><td class="botl"></td><td class="botr"></td></tr>
  </table>
  
<!-- end main box -->  
</div> 
</form> 
</sec:authorize>

  </tiles:putAttribute>    
</tiles:insertDefinition>