<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<c:forEach var="user" items="${users}">
	<option value="${user.id}" <c:if test="${user.id == crmAccountSearchUserId}">selected="selected"</c:if>>
	  <c:forEach begin="1" end="${level}"> &nbsp; </c:forEach>${user.name}
	</option>
	
	<c:if test="${user.subAccounts != null and fn:length(user.subAccounts) gt 0}">
    	<c:set var="level" value="${level + 1}" scope="request"/>
    	<c:set var="users" value="${user.subAccounts}" scope="request"/>
	    <jsp:include page="/WEB-INF/jsp/admin/crm/recursion.jsp"/>
		<c:set var="level" value="${level - 1}" scope="request"/>
    </c:if>
	
</c:forEach>