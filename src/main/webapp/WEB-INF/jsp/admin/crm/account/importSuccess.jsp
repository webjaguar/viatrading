<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="/WEB-INF/tlds/spring.tld" prefix="spring" %>
<%@ taglib uri="http://www.springframework.org/security/tags" prefix="sec" %>

<tiles:insertDefinition name="admin.crm" flush="true">
  <tiles:putAttribute name="content" type="string">
<sec:authorize ifAnyGranted="ROLE_AEM,ROLE_ADMIN_${prop_site_id},ROLE_CRM_IMPORT_EXPORT">

<script language="JavaScript" type="text/JavaScript">
<!--
window.addEvent('domready', function(){			
	var tabs1 = new SimpleTabs($('tab-block-1'), { selector: 'h4'});
});
//-->
</script>	  
<!-- main box -->
  <div id="mboxfull">
  
  <!-- start table -->
  <table cellpadding="0" cellspacing="0" border="0" class="module" >
  <tr>
    <td class="topl_g">
	  
	  <!-- breadcrumb -->
	  <p class="breadcrumb">
	    <a href="../crm">CRM</a> &gt;
	    <a href="../crm/crmAccountList.jhtm">Account</a> &gt;
	    import
	  </p>
  
      <!-- Error Message -->
      <div class="message">Updated Contacts</div>
      
    
  </td><td class="topr_g" ></td></tr>
  <tr><td class="boxmidlrg" >

 <!-- tabs -->
 <div class="tab-wrapper">
	<h4 title=""></h4>
	<div>
	<!-- start tab -->

	  	<div class="listdivi ln tabdivi"></div>
	  	<div class="listdivi"></div>
	   
		  	<div class="listlight">
		  	<!-- input field --> 

			<!-- no update allowed -->
				
			<c:forEach items="${addedItems}" var="item" varStatus="status">
				<c:if test="${status.first}">
				<br><br>
				<div class="message">Added Contacts</div>
				<table border="0" cellpadding="0" cellspacing="0" width="100%" style="padding-left:5px;padding-right:5px;">
				<tr height="20">
				  <td class="importIndexCol importHdr">&nbsp;</td>
				  <td class="importHdr"><spring:message code="crmAccount"/></td>
				  <td class="importHdr">Comments</td>  
				</tr>
				</c:if>
				<tr height="20" class="row${status.index % 2}" onmouseover="changeStyleClass(this,'rowOver')" onmouseout="changeStyleClass(this,'row${status.index % 2}')">
				  <td class="importIndexCol import"><c:out value="${status.count}"/>.</td>
				  <td class="import"><c:out value="${item['accountName']}"/></td>
				  <td class="import" width="100%"><c:out value="${item['comments']}"/></td>
				</tr>
				<c:if test="${status.last}">
				</table>
				</c:if>
			</c:forEach>
			<!-- end input field -->  	  
		  	</div>
		  	
		  	<!-- start button --> 
		  	<!-- end button -->	
		
	<!-- end tab -->        
	</div>
<!-- end tabs -->			
</div>
  
  <!-- end table -->
   </td><td class="boxmidr" ></td></tr>
  <tr><td class="botl"></td><td class="botr"></td></tr>
  </table>
  
<!-- end main box -->  
</div>

</sec:authorize>
  </tiles:putAttribute>    
</tiles:insertDefinition>