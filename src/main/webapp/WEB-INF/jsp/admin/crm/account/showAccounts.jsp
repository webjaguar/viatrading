<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<c:forEach items="${model.crmAccounts}" var="crmAccount">
  <li>
	<span><c:out value="${crmAccount.accountName}" /></span>
	<span style="display:none" id="userid"><c:out value="${crmAccount.id}" /></span>
  </li>
</c:forEach>