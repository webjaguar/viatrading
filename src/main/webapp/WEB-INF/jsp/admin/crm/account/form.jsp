<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ taglib uri="/WEB-INF/tlds/spring.tld" prefix="spring" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ taglib uri="http://www.springframework.org/security/tags" prefix="sec" %>
<script type="text/javascript" src="../javascript/autocompleter1.2.js"></script>
<script type="text/javascript" src="../javascript/autocompleter.Request1.2.js"></script>
<script type="text/javascript" src="../javascript/observer.js"></script>

<tiles:insertDefinition name="admin.crm.account" flush="true">
  <tiles:putAttribute name="content" type="string">
<sec:authorize ifAnyGranted="ROLE_AEM,ROLE_ADMIN_${prop_site_id},ROLE_CRM">
<script language="JavaScript"> 
<!--
window.addEvent('domready', function(){			
	var tabs1 = new SimpleTabs($('tab-block-1'), { selector: 'h4'});
	var Tips1 = new Tips($$('.toolTipImg'));

	var formAccountName = $('formAccountName');
	new Autocompleter.Request.HTML(formAccountName, '../crm/ajax-showCrmAccounts.jhtm', {
		'indicatorClass': 'autocompleter-loading',
		'postData': { 'search': 'crmAccountName' },
		'injectChoice': function(choice) {
			var text = choice.getFirst();
			var value = text.innerHTML;
			choice.inputValue = value;
			text.set('html', this.markQueryValue(value));
			this.addChoiceEvents(choice);
		}
	});
});
//--> 
</script> 
<form:form commandName="crmAccountForm" action="crmAccountForm.jhtm" method="post">
<form:hidden path="newCrmAccount" />

<!-- main box -->
  <div id="mboxfull">
  
  <!-- start table -->
  <table cellpadding="0" cellspacing="0" border="0" class="module" >
  <tr>
    <td class="topl_g">
	  
	  <!-- breadcrumb -->
	  <p class="breadcrumb">
	    <a href="../crm">CRM</a> &gt;
	    <a href="../crm">Accounts</a> &gt;
	    form 
	  </p>
	  
	  <!-- Error Message -->
	  <%--<c:if test="${message != null}">
  		<div class="message"><fmt:message key="${message}"/></div>
  	  </c:if> --%>
  	  <spring:hasBindErrors name="crmAccountForm">
		<div class="message">Please fix all errors!</div>
	  </spring:hasBindErrors>

  </td><td class="topr_g" ></td></tr>
  <tr><td class="boxmidlrg" >

 <!-- tabs -->
 <div id="tab-block-1">
	<h4 title="Account Information">Account Information</h4>
	<div>
	<!-- start tab -->

         <c:set var="classIndex" value="0" />
		<div class="listdivi ln tabdivi"></div>
		
		 <div class="list${classIndex % 2}"><c:set var="classIndex" value="${classIndex+1}" />
		 <div class="listfl"><div class="requiredField"><fmt:message key="crmAccountName" />:</div></div>
		 <div class="listp">
		 <!-- input field -->
	    		<c:choose>
	    		  <c:when test="${crmAccountForm.crmAccount.id == 1}">
	    		    <form:input path="crmAccount.accountName" id="formAccountName" cssClass="textfield" maxlength="255" size="60" htmlEscape="true" disabled="true" />
		  		  </c:when>
	    		  <c:otherwise>
	    		    <form:input path="crmAccount.accountName" id="formAccountName" cssClass="textfield" maxlength="255" size="60" htmlEscape="true"  />
		  		  </c:otherwise>
	    		</c:choose>
	    		<form:errors path="crmAccount.accountName" cssClass="error"/>
	 	 <!-- end input field -->	
	 	 </div>
		 </div>	
		 
		 <div class="list${classIndex % 2}"><c:set var="classIndex" value="${classIndex+1}" />
		 <div class="listfl"><div class="requiredField"><fmt:message key="crmAccountOwner" />:</div></div>
		 <div class="listp">
		 <!-- input field -->
	    		<span class="static"><c:out value="${crmAccountForm.crmAccount.accountOwner}" /></span>
	 	 <!-- end input field -->	
	 	 </div>
		 </div>
		 
		 <div class="list${classIndex % 2}"><c:set var="classIndex" value="${classIndex+1}" />
		 <div class="listfl"><div class="helpImg"><img class="toolTipImg" title="Assigned To::Assign this account to a user. To manage users go to: More-->Access Privilege." src="../graphics/question.gif" /></div><fmt:message key="assignedTo" />:</div>
		 <div class="listp">
		 <!-- input field -->
	    		<form:select path="crmAccount.accessUserId">
	    		    <form:option value="" label="Select"></form:option>
	    		  <c:forEach items="${accessUsers}" var="accessUser">
	    		    <form:option value="${accessUser.id}" label="${accessUser.username}"></form:option>
	    		  </c:forEach>
	    		</form:select>
	     <!-- end input field -->	
	 	 </div>
		 </div>
		 
		 <div class="list${classIndex % 2}"><c:set var="classIndex" value="${classIndex+1}" />
		 <div class="listfl"><fmt:message key="crmAccountNumber" />:</div>
		 <div class="listp">
		 <!-- input field -->
	    		<form:input path="crmAccount.accountNumber" cssClass="textfield" maxlength="255" size="60" htmlEscape="true"/>
		  		<form:errors path="crmAccount.accountNumber" cssClass="error"/>
	 	 <!-- end input field -->	
	 	 </div>
		 </div>
		 
		 <div class="list${classIndex % 2}"><c:set var="classIndex" value="${classIndex+1}" />
		 <div class="listfl"><div class="helpImg"><img class="toolTipImg" title="Account Type::Select the type of account for this account. To manage the available types of account goto: SiteInfo-->Site Configuration-->CRM Configuration." src="../graphics/question.gif" /></div><fmt:message key="crmAccountType" />:</div>
		 <div class="listp">
		 <!-- input field -->
				<form:select path="crmAccount.accountType">
					<form:option value="" label=""></form:option>
					<c:forTokens items="${types}" delims="," var="type" >
	    		  		<form:option value="${type}">${type}</form:option>
	    			</c:forTokens>
	    		</form:select>
		 <!-- end input field -->	
	 	 </div>
		 </div>
		 
		 <div class="list${classIndex % 2}"><c:set var="classIndex" value="${classIndex+1}" />
		 <div class="listfl"><fmt:message key="crmRating" />:</div>
		 <div class="listp">
		 <!-- input field -->
				<form:select path="crmAccount.rating">
				<form:option value="" label=""></form:option>
				<c:forTokens items="${rating}" delims="," var="rate" >
	    		  <form:option value="${rate}">${rate}</form:option>
	    		</c:forTokens>
	    		</form:select>
		 <!-- end input field -->	
	 	 </div>
		 </div>

		 <div class="list${classIndex % 2}"><c:set var="classIndex" value="${classIndex+1}" />
		 <div class="listfl"><fmt:message key="phone" />:</div>
		 <div class="listp">
		 <!-- input field -->
				<form:input path="crmAccount.phone" cssClass="textfield" maxlength="255" size="60" htmlEscape="true"/>
		 <!-- end input field -->	
	 	 </div>
		 </div>
		 
		 <div class="list${classIndex % 2}"><c:set var="classIndex" value="${classIndex+1}" />
		 <div class="listfl"><fmt:message key="fax" />:</div>
		 <div class="listp">
		 <!-- input field -->
				<form:input path="crmAccount.fax" cssClass="textfield" maxlength="255" size="60" htmlEscape="true"/>
		 <!-- end input field -->	
	 	 </div>
		 </div>
		 
		 <div class="list${classIndex % 2}"><c:set var="classIndex" value="${classIndex+1}" />
		 <div class="listfl"><fmt:message key="website" />:</div>
		 <div class="listp">
		 <!-- input field -->
				<form:input path="crmAccount.website" cssClass="textfield" maxlength="255" size="60" htmlEscape="true"/>
		 <!-- end input field -->	
	 	 </div>
		 </div>

		
	<!-- end tab -->        
	</div>
	
	<h4 title="Additional Information">Additional Information</h4>
	<div>
	<!-- start tab -->

         <c:set var="classIndex" value="0" />
		 <div class="listdivi ln tabdivi"></div>
		
		 <div class="list${classIndex % 2}"><c:set var="classIndex" value="${classIndex+1}" />
		 <div class="listfl"><fmt:message key="description" />:</div>
		 <div class="listp">
		 <!-- input field -->
	    		<form:textarea path="crmAccount.description" cols="45" rows="10" cssClass="textfield" htmlEscape="true"/>
	 	 <!-- end input field -->	
	 	 </div>
		 </div>	
		 
		 <div class="list${classIndex % 2}"><c:set var="classIndex" value="${classIndex+1}" />
		 <div class="listfl"><div class="helpImg"><img class="toolTipImg" title="Industry::Select the type of industry for this account. To manage the available types of industry goto: SiteInfo-->Site Configuration-->CRM Configuration." src="../graphics/question.gif" /></div><fmt:message key="industry" />:</div>
		 <div class="listp">
		 <!-- input field -->
	    		<form:select path="crmAccount.industry">
				<form:option value="" label=""></form:option>
				<c:forTokens items="${industry}" delims="," var="ind" >
	    		  <form:option value="${ind}">${ind}</form:option>
	    		</c:forTokens>
	    		</form:select>
	 	 <!-- end input field -->	
	 	 </div>
		 </div>

		 <div class="list${classIndex % 2}"><c:set var="classIndex" value="${classIndex+1}" />
		 <div class="listfl"><div class="helpImg"><img class="toolTipImg" title="Employees::Enter the number of employees in the company." src="../graphics/question.gif" /></div><fmt:message key="employees" />:</div>
		 <div class="listp">
		 <!-- input field -->
				<form:input path="crmAccount.employees" cssClass="textfield50" maxlength="255" size="60" htmlEscape="true"/>
				<form:errors path="crmAccount.employees" cssClass="error"/>
		 <!-- end input field -->	
	 	 </div>
		 </div>
		 
		 <div class="list${classIndex % 2}"><c:set var="classIndex" value="${classIndex+1}" />
		 <div class="listfl"><fmt:message key="annualRevenue" />:</div>
		 <div class="listp">
		 <!-- input field -->
				<form:input path="crmAccount.annualRevenue" cssClass="textfield50" maxlength="255" size="60" htmlEscape="true"/>
				<form:errors path="crmAccount.annualRevenue" cssClass="error"/>
		 <!-- end input field -->	
	 	 </div>
		 </div>
		
	<!-- end tab -->        
	</div>
	
	<h4 title="Address Information">Address Information</h4>
	<div>
	<!-- start tab -->

         <c:set var="classIndex" value="0" />
		 <div class="listdivi ln tabdivi"></div>
		 
		 <div class="list${classIndex % 2}"><c:set var="classIndex" value="${classIndex+1}" />
		 <div style="float:left;width:50%">
		 <div class="listfl"><fmt:message key="billingStreet" />:</div>
		 <div class="listp">
		 <!-- input field -->
				<form:textarea path="crmAccount.billingStreet" cssClass="textArea250x70" htmlEscape="true"/>
		 <!-- end input field -->	
	 	 </div>
	 	 </div>
	 	 
	 	 <div style="float:right;width:50%">
	 	 <div class="listfl"><fmt:message key="shippingStreet" />:</div>
	 	 <div class="listp">
		 <!-- input field -->
				<form:textarea path="crmAccount.shippingStreet" cssClass="textArea250x70" htmlEscape="true"/>
		 <!-- end input field -->	
	 	 </div>
		 </div>
		 </div>
		 
		 <div class="list${classIndex % 2}"><c:set var="classIndex" value="${classIndex+1}" />
		 <div style="float:left;width:50%">
		 <div class="listfl"><fmt:message key="billingCity" />:</div>
		 <div class="listp">
		 <!-- input field -->
				<form:input path="crmAccount.billingCity" cssClass="textfield250" maxlength="255" size="60" htmlEscape="true"/>
		 <!-- end input field -->	
	 	 </div>
	 	 </div>
	 	 
	 	 <div style="float:right;width:50%">
	 	 <div class="listfl"><fmt:message key="shippingCity" />:</div>
	 	 <div class="listp">
		 <!-- input field -->
				<form:input path="crmAccount.shippingCity" cssClass="textfield250" maxlength="255" size="60" htmlEscape="true"/>
		 <!-- end input field -->	
	 	 </div>
		 </div>
		 </div>
		 
		 <div class="list${classIndex % 2}"><c:set var="classIndex" value="${classIndex+1}" />
		 <div style="float:left;width:50%">
		 <div class="listfl"><fmt:message key="billingState" />:</div>
		 <div class="listp">
		 <!-- input field -->
				<form:input path="crmAccount.billingStateProvince" cssClass="textfield250" maxlength="255" size="60" htmlEscape="true"/>
		 <!-- end input field -->	
	 	 </div>
	 	 </div>
	 	 
	 	 <div style="float:right;width:50%">
	 	 <div class="listfl"><fmt:message key="shippingState" />:</div>
	 	 <div class="listp">
		 <!-- input field -->
				<form:input path="crmAccount.shippingStateProvince" cssClass="textfield250" maxlength="255" size="60" htmlEscape="true"/>
		 <!-- end input field -->	
	 	 </div>
		 </div>
		 </div>
		 
		 <div class="list${classIndex % 2}"><c:set var="classIndex" value="${classIndex+1}" />
		 <div style="float:left;width:50%">
		 <div class="listfl"><fmt:message key="billingZip" />:</div>
		 <div class="listp">
		 <!-- input field -->
				<form:input path="crmAccount.billingZip" cssClass="textfield250" maxlength="255" size="60" htmlEscape="true"/>
		 <!-- end input field -->	
	 	 </div>
	 	 </div>
	 	 
	 	 <div style="float:right;width:50%">
	 	 <div class="listfl"><fmt:message key="shippingZip" />:</div>
	 	 <div class="listp">
		 <!-- input field -->
				<form:input path="crmAccount.shippingZip" cssClass="textfield250" maxlength="255" size="60" htmlEscape="true"/>
		 <!-- end input field -->	
	 	 </div>
		 </div>
		 </div>
		 
		 <div class="list${classIndex % 2}"><c:set var="classIndex" value="${classIndex+1}" />
		 <div style="float:left;width:50%">
		 <div class="listfl"><fmt:message key="billingCountry" />:</div>
		 <div class="listp">
		 <!-- input field -->
				<form:select id="country" path="crmAccount.billingCountry" cssClass="textfield250">
                  <form:option value="" label="Please Select"/>
                  <form:options items="${countries}" itemValue="code" itemLabel="name"/>
                </form:select>
                <form:errors path="crmAccount.billingCountry" cssClass="error" />
		 <!-- end input field -->	
	 	 </div>
	 	 </div>
	 	 
	 	 <div style="float:right;width:50%">
	 	 <div class="listfl"><fmt:message key="shippingCountry" />:</div>
	 	 <div class="listp">
		 <!-- input field -->
				<form:select id="country" path="crmAccount.shippingCountry" cssClass="textfield250">
                  <form:option value="" label="Please Select"/>
                  <form:options items="${countries}" itemValue="code" itemLabel="name"/>
                </form:select>
                <form:errors path="crmAccount.shippingCountry" cssClass="error" />
		 <!-- end input field -->	
	 	 </div>
		 </div>
		 </div>

		
	<!-- end tab -->        
	</div>         

<!-- end tabs -->			
</div>

<!-- start button -->
	<div align="left" class="button">
	  <c:if test="${crmAccountForm.newCrmAccount}">
	    <sec:authorize ifAnyGranted="ROLE_AEM,ROLE_ADMIN_${prop_site_id},ROLE_CRM">
          <input type="submit" value="<spring:message code="add"/>" />
        </sec:authorize>  
      </c:if>
      <c:if test="${!crmAccountForm.newCrmAccount}">
        <sec:authorize ifAnyGranted="ROLE_AEM,ROLE_ADMIN_${prop_site_id},ROLE_CRM">
          <input type="submit" value="<spring:message code="update"/>" />
        </sec:authorize>
        <c:if test="${crmAccountForm.crmAccount.id != 1}">
        <sec:authorize ifAnyGranted="ROLE_AEM,ROLE_ADMIN_${prop_site_id},ROLE_CRM">  
          <input type="submit" value="<spring:message code="delete"/>" name="_delete" onClick="return confirm('Delete permanently?')" />
        </sec:authorize>  
        </c:if>
      </c:if>
	</div>
<!-- end button -->
  
  <!-- end table -->
    </td><td class="boxmidr" ></td></tr>
  <tr><td class="botl"></td><td class="botr"></td></tr>
  </table>
  
<!-- end main box -->  
</div>
</form:form>
</sec:authorize> 

  </tiles:putAttribute>    
</tiles:insertDefinition>