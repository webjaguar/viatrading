<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/security/tags" prefix="sec" %>

<tiles:insertDefinition name="admin.customers.affiliate.commissions" flush="true">
  <tiles:putAttribute name="content" type="string">  

<sec:authorize ifAnyGranted="ROLE_AEM,ROLE_ADMIN_${prop_site_id},ROLE_CUSTOMER_AFFILIATE">  
<form action="commissionList.jhtm" id="list" method="post">  
<input type="hidden" name="id" value="${model.owner.id}" /> 
<input type="hidden" id="sort" name="sort" value="${commissionSearch.sort}" />
<c:choose>
<c:when test="${model.level == 2}"><input type="hidden" id="l" name="l" value="2" /></c:when>
<c:otherwise><input type="hidden" id="l" name="l" value="1" /></c:otherwise>
</c:choose>
<!-- main box -->
  <div id="mboxfull">
  
  <!-- start table -->
  <table cellpadding="0" cellspacing="0" border="0" class="module" >
  <tr>
    <td class="topl_g">

	  <!-- breadcrumb -->
	  <p class="breadcrumb">
	    <a href="../customers/affiliateList.jhtm">Affiliate</a> &gt;
	    Commissions &gt;
	    <c:out value="${model.owner.address.firstName}" />&nbsp;<<c:out value="${model.owner.address.lastName}" />
	  </p>
	  
	  <!-- Error Message -->
	  <c:if test="${model.message != null}">
		  <div class="message"><fmt:message key="${model.message}" /></div>
	  </c:if>
	  
	  
  </td><td class="topr_g" ></td></tr>
  <tr><td class="boxmidlrg" >

 <!-- tabs -->
 <div class="tab-wrapper">
	<h4 title="List of commissions"></h4>
	<div>
	<!-- start tab -->

	  	<div class="listdivi"></div>
	    
		  	<div class="listlight">
		  	<!-- input field --> 
		    <table border="0" cellpadding="0" cellspacing="1" width="100%">
			  <tr>
			  <c:if test="${model.commissions.nrOfElements > 0}">
			  <td class="pageShowing">
			  <fmt:message key="showing">
				<fmt:param value="${model.commissions.firstElementOnPage + 1}"/>
				<fmt:param value="${model.commissions.lastElementOnPage + 1}"/>
				<fmt:param value="${model.commissions.nrOfElements}"/>
			  </fmt:message>
			  </td>
			  </c:if>
			  <td class="pageNavi">
			  Page 
			  <select id="page" name="page" onchange="document.getElementById('id').value=${model.owner.id};submit()">
			  <c:forEach begin="1" end="${model.commissions.pageCount}" var="page">
			  	<option value="${page}" <c:if test="${page == (model.commissions.page+1)}">selected</c:if>>${page}</option>
			  </c:forEach>
			  </select>
			  of <c:out value="${model.commissions.pageCount}"/>
			  | 
			  <c:if test="${model.commissions.firstPage}"><span class="pageNaviDead"><fmt:message key="previous" /></span></c:if>
			  <c:if test="${not model.commissions.firstPage}"><a href="<c:url value="commissionList.jhtm"><c:param name="id" value="${model.owner.id}"/><c:param name="page" value="${model.commissions.page}"/></c:url>" class="pageNaviLink"><fmt:message key="previous" /></a></c:if>
			  | 
			  <c:if test="${model.commissions.lastPage}"><span class="pageNaviDead"><fmt:message key="next" /></span></c:if>
			  <c:if test="${not model.commissions.lastPage}"><a href="<c:url value="commissionList.jhtm"><c:param name="id" value="${model.owner.id}"/><c:param name="page" value="${model.commissions.page+2}"/></c:url>" class="pageNaviLink"><fmt:message key="next" /></a></c:if>
			  </td>
			  </tr>
			</table>
			
			<table border="0" cellpadding="0" cellspacing="0" width="100%" class="listings">
			  <tr class="listingsHdr2">
			    <td width="15px">&nbsp;</td>
			    <td width="15px">&nbsp;</td>
			    <td>
			    	<table cellspacing="0" cellpadding="1">
			    	  <tr>
			    	    <c:choose>
			    	      <c:when test="${commissionSearch.sort == 'last_name desc'}">
			    	        <td class="listingsHdr3">
			    	        <a class="hdrLink" href="#" onClick="document.getElementById('sort').value='last_name';document.getElementById('list').submit()"><fmt:message key="lastName" /></a>
			    	        </td>
			    	        <td><img src="../graphics/up.gif" border="0"></td>
			    	      </c:when>
			    	      <c:when test="${commissionSearch.sort == 'last_name'}">
			    	        <td class="listingsHdr3">
			    	        <a class="hdrLink" href="#" onClick="document.getElementById('sort').value='last_name desc';document.getElementById('list').submit()"><fmt:message key="lastName" /></a>
			    	        </td>
			    	        <td><img src="../graphics/down.gif" border="0"></td>
			    	      </c:when>
			    	      <c:otherwise>
			    	        <td colspan="2" class="listingsHdr3">
			    	        <a class="hdrLink" href="#" onClick="document.getElementById('sort').value='last_name desc';document.getElementById('list').submit()"><fmt:message key="lastName" /></a>
			    	        </td>
			    	      </c:otherwise>
			    	    </c:choose>  
			    	  </tr>
			    	  </table>
			    </td>
			    <td>
			    	<table cellspacing="0" cellpadding="1">
			    	  <tr>
			    	    <c:choose>
			    	      <c:when test="${commissionSearch.sort == 'first_name desc'}">
			    	        <td class="listingsHdr3">
			    	        <a class="hdrLink" href="#" onClick="document.getElementById('sort').value='first_name';document.getElementById('list').submit()"><fmt:message key="firstName" /></a>
			    	        </td>
			    	        <td><img src="../graphics/up.gif" border="0"></td>
			    	      </c:when>
			    	      <c:when test="${commissionSearch.sort == 'first_name'}">
			    	        <td class="listingsHdr3">
			    	        <a class="hdrLink" href="#" onClick="document.getElementById('sort').value='first_name desc';document.getElementById('list').submit()"><fmt:message key="firstName" /></a>
			    	        </td>
			    	        <td><img src="../graphics/down.gif" border="0"></td>
			    	      </c:when>
			    	      <c:otherwise>
			    	        <td colspan="2" class="listingsHdr3">
			    	        <a class="hdrLink" href="#" onClick="document.getElementById('sort').value='first_name desc';document.getElementById('list').submit()"><fmt:message key="firstName" /></a>
			    	        </td>
			    	      </c:otherwise>
			    	    </c:choose>  
			    	  </tr>
			    	  </table>
			    </td>
			    <td>
			    	<table cellspacing="0" cellpadding="1">
			    	  <tr>
			    	    <c:choose>
			    	      <c:when test="${commissionSearch.sort == 'order_id desc'}">
			    	        <td class="listingsHdr3">
			    	        <a class="hdrLink" href="#" onClick="document.getElementById('sort').value='order_id';document.getElementById('list').submit()"><fmt:message key="orderNumber" /></a>
			    	        </td>
			    	        <td><img src="../graphics/up.gif" border="0"></td>
			    	      </c:when>
			    	      <c:when test="${commissionSearch.sort == 'order_id'}">
			    	        <td class="listingsHdr3">
			    	        <a class="hdrLink" href="#" onClick="document.getElementById('sort').value='order_id desc';document.getElementById('list').submit()"><fmt:message key="orderNumber" /></a>
			    	        </td>
			    	        <td><img src="../graphics/down.gif" border="0"></td>
			    	      </c:when>
			    	      <c:otherwise>
			    	        <td colspan="2" class="listingsHdr3">
			    	        <a class="hdrLink" href="#" onClick="document.getElementById('sort').value='order_id desc';document.getElementById('list').submit()"><fmt:message key="orderNumber" /></a>
			    	        </td>
			    	      </c:otherwise>
			    	    </c:choose>  
			    	  </tr>
			    	  </table>
			    </td>
			    <td>
			    	<table cellspacing="0" cellpadding="1">
			    	  <tr>
			    	    <c:choose>
			    	      <c:when test="${commissionSearch.sort == 'date_ordered desc'}">
			    	        <td class="listingsHdr3">
			    	        <a class="hdrLink" href="#" onClick="document.getElementById('sort').value='date_ordered';document.getElementById('list').submit()"><fmt:message key="orderDate" /></a>
			    	        </td>
			    	        <td><img src="../graphics/up.gif" border="0"></td>
			    	      </c:when>
			    	      <c:when test="${commissionSearch.sort == 'date_ordered'}">
			    	        <td class="listingsHdr3">
			    	        <a class="hdrLink" href="#" onClick="document.getElementById('sort').value='date_ordered desc';document.getElementById('list').submit()"><fmt:message key="orderDate" /></a>
			    	        </td>
			    	        <td><img src="../graphics/down.gif" border="0"></td>
			    	      </c:when>
			    	      <c:otherwise>
			    	        <td colspan="2" class="listingsHdr3">
			    	        <a class="hdrLink" href="#" onClick="document.getElementById('sort').value='date_ordered desc';document.getElementById('list').submit()"><fmt:message key="orderDate" /></a>
			    	        </td>
			    	      </c:otherwise>
			    	    </c:choose>  
			    	  </tr>
			    	  </table>
			    </td>
			    <td>
			    	<table cellspacing="0" cellpadding="1">
			    	  <tr>
			    	    <c:choose>
			    	      <c:when test="${commissionSearch.sort == 'sub_total desc'}">
			    	        <td class="listingsHdr3">
			    	        <a class="hdrLink" href="#" onClick="document.getElementById('sort').value='sub_total';document.getElementById('list').submit()"><fmt:message key="subTotal" /></a>
			    	        </td>
			    	        <td><img src="../graphics/up.gif" border="0"></td>
			    	      </c:when>
			    	      <c:when test="${commissionSearch.sort == 'sub_total'}">
			    	        <td class="listingsHdr3">
			    	        <a class="hdrLink" href="#" onClick="document.getElementById('sort').value='sub_total desc';document.getElementById('list').submit()"><fmt:message key="subTotal" /></a>
			    	        </td>
			    	        <td><img src="../graphics/down.gif" border="0"></td>
			    	      </c:when>
			    	      <c:otherwise>
			    	        <td colspan="2" class="listingsHdr3">
			    	        <a class="hdrLink" href="#" onClick="document.getElementById('sort').value='sub_total desc';document.getElementById('list').submit()"><fmt:message key="subTotal" /></a>
			    	        </td>
			    	      </c:otherwise>
			    	    </c:choose>  
			    	  </tr>
			    	  </table>
			    </td>
			    <td>
			    	<table cellspacing="0" cellpadding="1">
			    	  <tr>
			    	    <c:choose>
			    	      <c:when test="${commissionSearch.sort == 'status desc'}">
			    	        <td class="listingsHdr3">
			    	        <a class="hdrLink" href="#" onClick="document.getElementById('sort').value='status';document.getElementById('list').submit()"><fmt:message key="order" /> <fmt:message key="status" /></a>
			    	        </td>
			    	        <td><img src="../graphics/up.gif" border="0"></td>
			    	      </c:when>
			    	      <c:when test="${commissionSearch.sort == 'status'}">
			    	        <td class="listingsHdr3">
			    	        <a class="hdrLink" href="#" onClick="document.getElementById('sort').value='status desc';document.getElementById('list').submit()"><fmt:message key="order" /> <fmt:message key="status" /></a>
			    	        </td>
			    	        <td><img src="../graphics/down.gif" border="0"></td>
			    	      </c:when>
			    	      <c:otherwise>
			    	        <td colspan="2" class="listingsHdr3">
			    	        <a class="hdrLink" href="#" onClick="document.getElementById('sort').value='status desc';document.getElementById('list').submit()"><fmt:message key="order" /> <fmt:message key="status" /></a>
			    	        </td>
			    	      </c:otherwise>
			    	    </c:choose>  
			    	  </tr>
			    	  </table>
			    </td>
			    
			    <c:if test="${model.level == 1}">
			    <td>
			    	<table cellspacing="0" cellpadding="1">
			    	  <tr>
			    	    <c:choose>
			    	      <c:when test="${commissionSearch.sort == 'commission_level1 desc'}">
			    	        <td class="listingsHdr3">
			    	        <a class="hdrLink" href="#" onClick="document.getElementById('sort').value='commission_level1';document.getElementById('list').submit()"><fmt:message key="commission" /> Level 1</a>
			    	        </td>
			    	        <td><img src="../graphics/up.gif" border="0"></td>
			    	      </c:when>
			    	      <c:when test="${commissionSearch.sort == 'commission_level1'}">
			    	        <td class="listingsHdr3">
			    	        <a class="hdrLink" href="#" onClick="document.getElementById('sort').value='commission_level1 desc';document.getElementById('list').submit()"><fmt:message key="commission" /> Level 1</a>
			    	        </td>
			    	        <td><img src="../graphics/down.gif" border="0"></td>
			    	      </c:when>
			    	      <c:otherwise>
			    	        <td colspan="2" class="listingsHdr3">
			    	        <a class="hdrLink" href="#" onClick="document.getElementById('sort').value='commission_level1 desc';document.getElementById('list').submit()"><fmt:message key="commission" /> Level 1</a>
			    	        </td>
			    	      </c:otherwise>
			    	    </c:choose>  
			    	  </tr>
			    	  </table>
			    </td>
			    <td>
			    	<table cellspacing="0" cellpadding="1">
			    	  <tr>
			    	    <td rowspan="2" class="listingsHdr3"><fmt:message key="commission" /> <fmt:message key="status" /></td>
			    	  </tr>
			    	</table>
			    </td>
			    </c:if>
			    <c:if test="${model.level == 2}">
			    <td>
			    	<table cellspacing="0" cellpadding="1">
			    	  <tr>
			    	    <c:choose>
			    	      <c:when test="${commissionSearch.sort == 'commission_level2 desc'}">
			    	        <td class="listingsHdr3">
			    	        <a class="hdrLink" href="#" onClick="document.getElementById('sort').value='commission_level2';document.getElementById('list').submit()"><fmt:message key="commission" /> Level 2</a>
			    	        </td>
			    	        <td><img src="../graphics/up.gif" border="0"></td>
			    	      </c:when>
			    	      <c:when test="${commissionSearch.sort == 'commission_level2'}">
			    	        <td class="listingsHdr3">
			    	        <a class="hdrLink" href="#" onClick="document.getElementById('sort').value='commission_level2 desc';document.getElementById('list').submit()"><fmt:message key="commission" /> Level 2</a>
			    	        </td>
			    	        <td><img src="../graphics/down.gif" border="0"></td>
			    	      </c:when>
			    	      <c:otherwise>
			    	        <td colspan="2" class="listingsHdr3">
			    	        <a class="hdrLink" href="#" onClick="document.getElementById('sort').value='commission_level2 desc';document.getElementById('list').submit()"><fmt:message key="commission" /> Level 2</a>
			    	        </td>
			    	      </c:otherwise>
			    	    </c:choose>  
			    	  </tr>
			    	  </table>
			    </td>
			    <td>
			    	<table cellspacing="0" cellpadding="1">
			    	  <tr>
			    	    <td rowspan="2" class="listingsHdr3"><fmt:message key="commission" /> <fmt:message key="status" /></td>
			    	  </tr>
			    	</table>
			    </td>
			    </c:if>
			  </tr>
			  <c:set var="totalCommission" value="0.0" />
			  <c:set var="totalSubTotal" value="0.0" />
			<c:forEach items="${model.commissions.pageList}" var="customer" varStatus="status">
			  <tr class="row${status.index % 2}"  onclick="toggleInfo('${customer.orderId}')">
			    <td width="15px"><img id="info${customer.orderId}image" src="../graphics/expand.gif" border="0"></td>
			    <td class="indexCol"><c:out value="${status.count + model.orders.firstElementOnPage}"/>.</td>
			    <td class="nameCol"><c:out value="${customer.lastName}"/></td>
			    <td class="nameCol"><c:out value="${customer.firstName}"/></td>
			    <td class="nameCol"><a href="../orders/invoice.jhtm?order=${customer.orderId}" class="nameLink"><c:out value="${customer.orderId}"/></a></td>
			    <td class="nameCol"><fmt:formatDate type="date" timeStyle="default" value="${customer.orderDate}"/></td>
			    
			    <td class="numberCol"><fmt:message key="${siteConfig['CURRENCY'].value}" /><fmt:formatNumber value="${customer.subTotal}" pattern="#,##0.00" /></td><c:set var="totalSubTotal" value="${customer.subTotal + totalSubTotal}" />
			    <td class="nameCol"><c:out value="${customer.orderStatusToString}"/></td>
			    <c:if test="${model.level == 1}">
			      <td class="numberCol"><fmt:message key="${siteConfig['CURRENCY'].value}" /><fmt:formatNumber value="${customer.myCommission}" pattern="#,##0.00" /></td><c:set var="totalCommission" value="${customer.myCommission + totalCommission}" />
			      <td class="nameCol"><c:out value="${customer.commissionLevel1StatusToString}"/></td>
			    </c:if>
			    <c:if test="${model.level == 2}">  
			      <td class="numberCol"><fmt:message key="${siteConfig['CURRENCY'].value}" /><fmt:formatNumber value="${customer.myCommission}" pattern="#,##0.00" /></td><c:set var="totalCommission" value="${customer.myCommission + totalCommission}" />
			      <td class="nameCol"><c:out value="${customer.commissionLevel2StatusToString}"/></td>
			    </c:if>
			  </tr>
			  <tr>
			    <td colspan="10">
			      <div id="info${customer.orderId}" class="row${status.index % 2} custInfo">
			        <table class="custdetails">
			        <tr>
			        <c:choose>
			          <c:when test="${customer.orderStatus == 's'}">
			            <td colspan="2"><a href="<c:url value="commissionForm.jhtm"><c:param name="oId" value="${customer.orderId}"/><c:param name="uId" value="${model.owner.id}"/><c:param name="l" value="${model.level}"/></c:url>">update status</a></td>
			          </c:when>
			          <c:otherwise>
			            <td><p>Change order status.</p></td>
			          </c:otherwise>
			        </c:choose>
			        </tr>
			        </table>
			      </div>
			    </td>
			  </tr>
			</c:forEach>
			<tr class="totals">
			  <td style="color:#666666; padding: 5px" colspan="6" align="left"><fmt:message key="total" /></td>
			  <td class="numberCol" align="right"><fmt:formatNumber value="${totalSubTotal}" pattern="#,##0.00" /></td>
			  <td class="numberCol" colspan="2" align="right"><fmt:formatNumber value="${totalCommission}" pattern="#,##0.00" /></td>
			</tr>
			<c:if test="${model.commissions.nrOfElements == 0}">
			  <tr class="emptyList"><td colspan="5">&nbsp;</td></tr>
			</c:if>
			</table>
			<br /><br />
			
			<div>
			 Total Commission: <fmt:message key="${siteConfig['CURRENCY'].value}" /><fmt:formatNumber value="${model.total.commissionTotal}" pattern="#,##0.00" />
			</div>
			
			<table border="0" cellpadding="0" cellspacing="1" width="100%">
			  <tr>
			  <td class="pageSize">
			  <select name="size" onchange="document.getElementById('page').value=1;submit()">
			  <c:forTokens items="10,25,50" delims="," var="current">
			  	  <option value="${current}" <c:if test="${current == model.commissions.pageSize}">selected</c:if>>${current} <fmt:message key="perPage" /></option>
			  </c:forTokens>
			  </select>
			  </td>
			  </tr>
			</table>  
		     
		    <!-- end input field -->  	  
		  	</div>
		  	
		  	<!-- start button --> 
		  	<!-- end button -->	
		  	
	<!-- end tab -->        
	</div>
<!-- end tabs -->			
</div>
  
  <!-- end table -->
   </td><td class="boxmidr" ></td></tr>
  <tr><td class="botl"></td><td class="botr"></td></tr>
  </table>
  
<!-- end main box -->  
</div>
</form>	  	

</sec:authorize>  
  </tiles:putAttribute>    
</tiles:insertDefinition>