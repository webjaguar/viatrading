<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

  <div id="lbox">
   <table cellpadding="0" cellspacing="0" border="0" class="module" style="width: 100%"><tr><td class="topl">
    </td><td class="topr" ></td></tr><tr><td class="boxmid" >
    
    <h2 class="menuleft mfirst"><fmt:message key="customersMenuTitle"/></h2>
    <div class="menudiv"></div>
      <a href="../customers/customerList.jhtm" class="userbutton"><fmt:message key="list"/></a>
      <c:if test="${gSiteConfig['gGROUP_CUSTOMER']}">
	    <a href="../customers/customerGroupList.jhtm" class="userbutton"><fmt:message key="customer"/> <fmt:message key="groups"/></a> 
	  </c:if>
      <a href="../customers/customerImport.jhtm" class="userbutton"><fmt:message key="import"/></a>
      <a href="javascript: submitSearchForm()" class="userbutton"><fmt:message key="export"/></a>
      <c:if test="${gSiteConfig['gCUSTOMER_FIELDS'] > 0}">
	    <a href="../customers/customerFields.jhtm" class="userbutton"><fmt:message key="Fields"/></a>
	  </c:if>
      <c:if test="${gSiteConfig['gPAYMENTS']}">
	    <a href="../customers/payments.jhtm?cid=" class="userbutton"><fmt:message key="incomingPayments"/></a> 
	  </c:if> 
	  <sec:authorize ifAnyGranted="ROLE_AEM,ROLE_ADMIN_${prop_site_id},ROLE_EMAILMANAGER_CREATE">
	  <c:if test="${gSiteConfig['gMASS_EMAIL'] and siteConfig['CUSTOMER_MASS_EMAIL'].value == 'true'}">	
        <a href="../customers/customerMassEmail.jhtm" class="userbutton"><fmt:message key="massEmail"/></a>
      </c:if>
      </sec:authorize>
      <div class="menudiv"></div>
    
    <h2 class="menuleft"><fmt:message key="affiliateMenuTitle"/></h2>   
    <div class="menudiv"></div>
    <a href="affiliateList.jhtm" class="userbutton"><fmt:message key="affiliateMenuTitleList"/></a>
	<div class="menudiv"></div> 
    <c:if test="${param.tab == 'affiliate'}"> 
      <div align="left" class="searchHeader" id="searchHeaderId"><h2><fmt:message key="search" /></h2></div>
	  <table class="searchBoxLeft"><tr><td>
	  <form action="affiliateList.jhtm" method="post" name="searchform">
	  <!-- content area -->
	    <div class="search2">
	      <p><fmt:message key="firstName" />:</p>
	      <input name="firstName" type="text" value="<c:out value='${affiliateSearch.firstName}' />" size="15" />
	    </div> 
	    <div class="search2">
	      <p><fmt:message key="lastName" />:</p>
	      <input name="lastName" type="text" value="<c:out value='${affiliateSearch.lastName}' />" size="15" />
	    </div>  
	    <c:if test="${gSiteConfig['gSALES_REP']}">
	    <div class="search2">
	      <p><fmt:message key="salesRep" />:</p>
	      <select name="sales_rep_id">
	       <option value="-1"></option>
	       <option value="-2" <c:if test="${affiliateSearch.salesRepId == -2}">selected</c:if>><fmt:message key="noSalesRep" /></option>
	       <c:forEach items="${model.salesRepMap}" var="salesRep">
	  	     <option value="${salesRep.value.id}" <c:if test="${affiliateSearch.salesRepId == salesRep.value.id}">selected</c:if>><c:out value="${salesRep.value.name}" /></option>
	       </c:forEach>
	      </select>
	    </c:if>
	    <div class="search2">
	      <p><fmt:message key="accountNumber" />:</p>
          <input name="accountNumber" type="text" value="<c:out value='${affiliateSearch.accountNumber}' />" size="15" />   
	    </div>
	    <div class="search2">
	      <p><fmt:message key="email" />:</p>
	      <input name="email" type="text" value="<c:out value='${affiliateSearch.email}' />" size="15" />
	    </div>
	    <div class="button">
	      <input type="submit" value="Search"/>
		</div>
	  <!-- content area -->
	  </form>
	  </td></tr></table>
	<div class="menudiv"></div>  
	</c:if>
	
	<c:if test="${param.tab == 'commissions'}"> 
    <div class="menudiv"></div>
      <div align="left" class="searchHeader" id="searchHeaderId"><h2><fmt:message key="search" /></h2></div>
	  <table class=searchBoxLeft><tr><td>
	  <form action="commissionList.jhtm" method="post" name="searchform"> 
	  <input type="hidden" name="id" id="id" value="${param.id}"/> 
	  <!-- content area -->
	  	<div class="search2">
	      <p>Level:</p>
	      <select name="commission_level" onChange="document.getElementById('page').value=1;document.getElementById('id').value=${model.owner.id};submit()">
		  <c:forEach begin="1" end="${gSiteConfig['gAFFILIATE']}" var="thisLevel">
		    <option value="${thisLevel}" <c:if test="${model.level == thisLevel}">selected="selected"</c:if>>Level ${thisLevel}</option>
	      </c:forEach>
	      </select>
	    </div> 
	    <div class="search2">
	      <p>Order# :</p>
	      <input name="orderNum" type="text" value="<c:out value='${commissionSearch.orderNum}' />" size="15" />
	    </div>         
	    <div class="button">
	      <input type="submit" value="Search" onClick="document.getElementById('id').value=${model.owner.id};submit()"/>
	    </div>
	  <!-- content area -->
	  </form>
	  </td></tr></table>
	<div class="menudiv"></div>  
	</c:if>

    </td><td class="boxmidr" ></td></tr>
    <tr><td class="botl"></td><td class="botr"></td></tr></table>
  </div>