<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="/WEB-INF/tlds/spring.tld" prefix="spring" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ taglib uri="http://www.springframework.org/security/tags" prefix="sec" %>

<tiles:insertDefinition name="admin.customers.affiliate.commissions" flush="true">
  <tiles:putAttribute name="content" type="string">
  
<sec:authorize ifAnyGranted="ROLE_AEM,ROLE_ADMIN_${prop_site_id},ROLE_CUSTOMER_AFFILIATE">  
<link rel="stylesheet" type="text/css" media="all" href="../../admin/javascript/jscalendar-1.0/skins/aqua/theme.css" title="Aqua" /> <!-- calendar stylesheet -->
<script type="text/javascript" src="../../admin/javascript/jscalendar-1.0/calendar.js"></script> <!-- main calendar program -->
<script type="text/javascript" src="../../admin/javascript/jscalendar-1.0/lang/calendar-en.js"></script>
<script type="text/javascript" src="../../admin/javascript/jscalendar-1.0/calendar-setup.js"></script>  
<script language="JavaScript"> 
<!--
window.addEvent('domready', function(){			
	var tabs1 = new SimpleTabs($('tab-block-1'), { selector: 'h4'});
	var Tips1 = new Tips($$('.toolTipImg'));
});
//--> 
</script>
<form:form commandName="commissionForm" method="post">
<!-- main box -->
  <div id="mboxfull">
  
  <!-- start table -->
  <table cellpadding="0" cellspacing="0" border="0" class="module" >
  <tr>
    <td class="topl_g">
	  
	  <!-- breadcrumb -->
	  <p class="breadcrumb">
	    <a href="../affiliate">Affiliate</a> &gt;
	    <a href="../affiliate/commissionList.jhtm?id=${param.uId}">Commissions</a> &gt;
	    form 
	  </p>
	  
	  <!-- Error Message -->
	  <%--<c:if test="${message != null}">
  		<div class="message"><fmt:message key="${message}"/></div>
  	  </c:if> --%>
  	  <spring:hasBindErrors name="categoryForm">
	    <div class="message">Please fix all errors!</div>
	  </spring:hasBindErrors>
  
  </td><td class="topr_g" ></td></tr>
  <tr><td class="boxmidlrg" >

 <!-- tabs -->
 <div id="tab-block-1">
	<h4 title="Commission/Order Information">Commission Information</h4>
	<div>
	<!-- start tab -->
         <c:set var="classIndex" value="0" />
         
	  	<div class="listdivi ln tabdivi"></div>
	  	<div class="listdivi"></div>
	   
		  	<div class="list${classIndex % 2}"><c:set var="classIndex" value="${classIndex+1}" />
		  	<div class="listfl"><fmt:message key='firstName' />:</div>
		  	<div class="listp">
		  	<!-- input field -->
			  	<c:out value="${commissionForm.commissionReport.firstName}" />
		    <!-- end input field -->   	
		  	</div>
		  	</div>
		  	
		  	<div class="list${classIndex % 2}"><c:set var="classIndex" value="${classIndex+1}" />
		  	<div class="listfl"><fmt:message key='lastName' />:</div>
		  	<div class="listp">
		  	<!-- input field -->
			  	<c:out value="${commissionForm.commissionReport.lastName}" />
		    <!-- end input field -->   	
		  	</div>
		  	</div>
		  	
		  	<div class="list${classIndex % 2}"><c:set var="classIndex" value="${classIndex+1}" />
		  	<div class="listfl">Order Date:</div>
		  	<div class="listp">
		  	<!-- input field -->
			  	<c:out value="${commissionForm.commissionReport.orderDate}" />
		    <!-- end input field -->   	
		  	</div>
		  	</div>

		  	<div class="list${classIndex % 2}"><c:set var="classIndex" value="${classIndex+1}" />
		  	<div class="listfl">SubTotal:</div>
		  	<div class="listp">
		  	<!-- input field -->
			  	<fmt:message key="${siteConfig['CURRENCY'].value}" /><fmt:formatNumber value="${commissionForm.commissionReport.subTotal}" pattern="#,##0.00" />
		    <!-- end input field -->   	
		  	</div>
		  	</div>

		  	<div class="list${classIndex % 2}"><c:set var="classIndex" value="${classIndex+1}" />
		  	<div class="listfl">Order Status:</div>
		  	<div class="listp">
		  	<!-- input field -->
			  	<c:choose>
		   		  <c:when test="${commissionForm.commissionReport.orderStatus == 'x' or commissionForm.commissionReport.orderStatus == 'xp'}">
		     	  <div style="color:red"><c:out value="${commissionForm.commissionReport.orderStatusToString}" /></div>
		   		  </c:when>
		   		  <c:otherwise><c:out value="${commissionForm.commissionReport.orderStatusToString}" /></c:otherwise>
		  		</c:choose>
		    <!-- end input field -->   	
		  	</div>
		  	</div>

	<!-- end tab -->
	</div>  

<c:if test="${commissionForm.level == 1}">	
  <h4 title="Commission Level 1">Commission Level 1</h4>
	<div>
	<!-- start tab -->
         <c:set var="classIndex" value="0" />
         
	  	<div class="listdivi ln tabdivi"></div>
	  	<div class="listdivi"></div>
	   
		  	<div class="list${classIndex % 2}"><c:set var="classIndex" value="${classIndex+1}" />
		  	<div class="listfl">Commission:</div>
		  	<div class="listp">
		  	<!-- input field -->
			  	<fmt:message key="${siteConfig['CURRENCY'].value}" /><form:input path="commissionReport.commissionLevel1" cssClass="textfield" htmlEscape="true" />
		    <!-- end input field -->   	
		  	</div>
		  	</div>

			<div class="list${classIndex % 2}"><c:set var="classIndex" value="${classIndex+1}" />
		  	<div class="listfl">Check Number:</div>
		  	<div class="listp">
		  	<!-- input field -->
			  	<form:input path="commissionReport.checkNumberLevel1" cssClass="textfield" htmlEscape="true" />
		    <!-- end input field -->   	
		  	</div>
		  	</div>

			<div class="list${classIndex % 2}"><c:set var="classIndex" value="${classIndex+1}" />
		  	<div class="listfl"><fmt:message key="date" />:</div>
		  	<div class="listp">
		  	<!-- input field -->
				<form:input path="commissionReport.dateLevel1" size="10" maxlength="10" />
				  <img class="calendarImage"  id="commissionReport_dateLevel1_trigger" src="../graphics/calendarIcon.jpg" />
				  <script type="text/javascript">
		    		Calendar.setup({
		        			inputField     :    "commissionReport.dateLevel1",   // id of the input field
		        			showsTime      :    false,
		        			ifFormat       :    "%m/%d/%Y",   // format of the input field
		        			button         :    "commissionReport_dateLevel1_trigger"   // trigger for the calendar (button ID)
		    		});	
				  </script> 
				<form:errors path="commissionReport.dateLevel1" cssClass="error"/>
		    <!-- end input field -->   	
		  	</div>
		  	</div>
		  	
			<c:if test="${ commissionForm.commissionReport.commissionLevel1 != null and commissionForm.commissionReport.orderStatus != 'x' and commissionForm.commissionReport.orderStatus != 'xp'}">
		  	<div class="list${classIndex % 2}"><c:set var="classIndex" value="${classIndex+1}" />
		  	<div class="listfl">status:</div>
		  	<div class="listp">
		  	<!-- input field -->
			  	<form:select path="commissionReport.commissionLevel1Status">
		          <form:option value="p">Pending</form:option>
		          <form:option value="pd">Paid</form:option>
		        </form:select>
		        <a href="<c:url value="../customers/customer.jhtm"><c:param name="id" value="${commissionForm.commissionReport.affiliateIdLevel1}"/></c:url>">This commission should pay to</a>
		    <!-- end input field -->   	
		  	</div>
		  	</div>
		  	</c:if>
		  	
		  	<div class="list${classIndex % 2}"><c:set var="classIndex" value="${classIndex+1}" />
		  	<div class="listfl">Note:</div>
		  	<div class="listp">
		  	<!-- input field -->
			  	<form:textarea rows="8" cols="60" path="commissionReport.commissionLevel1Note" cssClass="textfield" htmlEscape="true" />
		    <!-- end input field -->   	
		  	</div>
		  	</div>

      </div>
		  		
	<!-- end tab -->
	</div>  
</c:if>

<c:if test="${commissionForm.level == 2}">	
  <h4 title="Commission Level 2">Commission Level 2</h4>
	<div>
	<!-- start tab -->
         <c:set var="classIndex" value="0" />
         
	  	<div class="listdivi ln tabdivi"></div>
	  	<div class="listdivi"></div>
	   
		  	<div class="list${classIndex % 2}"><c:set var="classIndex" value="${classIndex+1}" />
		  	<div class="listfl">Commission:</div>
		  	<div class="listp">
		  	<!-- input field -->
			  	<fmt:message key="${siteConfig['CURRENCY'].value}" /><fmt:formatNumber value="${commissionForm.commissionReport.commissionLevel2}" pattern="#,##0.00" />
		    <!-- end input field -->   	
		  	</div>
		  	</div>

			<div class="list${classIndex % 2}"><c:set var="classIndex" value="${classIndex+1}" />
		  	<div class="listfl">Check Number:</div>
		  	<div class="listp">
		  	<!-- input field -->
			  	<form:input path="commissionReport.checkNumberLevel2" cssClass="textfield" htmlEscape="true" />
		    <!-- end input field -->   	
		  	</div>
		  	</div>

			<div class="list${classIndex % 2}"><c:set var="classIndex" value="${classIndex+1}" />
		  	<div class="listfl"><fmt:message key="date" />:</div>
		  	<div class="listp">
		  	<!-- input field -->
				<form:input path="commissionReport.dateLevel2" size="10" maxlength="10" />
				  <img class="calendarImage"  id="commissionReport_dateLevel2_trigger" src="../graphics/calendarIcon.jpg" />
				  <script type="text/javascript">
		    		Calendar.setup({
		        			inputField     :    "commissionReport.dateLevel2",   // id of the input field
		        			showsTime      :    false,
		        			ifFormat       :    "%m/%d/%Y",   // format of the input field
		        			button         :    "commissionReport_dateLevel2_trigger"   // trigger for the calendar (button ID)
		    		});	
				  </script> 
				<form:errors path="commissionReport.dateLevel2" cssClass="error"/>
		    <!-- end input field -->   	
		  	</div>
		  	</div>

		  	<c:if test="${ commissionForm.commissionReport.commissionLevel2 != null and commissionForm.commissionReport.orderStatus != 'x' and commissionForm.commissionReport.orderStatus != 'xp'}">
		  	<div class="list${classIndex % 2}"><c:set var="classIndex" value="${classIndex+1}" />
		  	<div class="listfl">status:</div>
		  	<div class="listp">
		  	<!-- input field -->
			  	<form:select path="commissionReport.commissionLevel2Status">
		          <form:option value="p">Pending</form:option>
		          <form:option value="pd">Paid</form:option>
		        </form:select>
		        <a href="<c:url value="../customers/customer.jhtm"><c:param name="id" value="${commissionForm.commissionReport.affiliateIdLevel2}"/></c:url>">This commission should pay to</a>
		    <!-- end input field -->   	
		  	</div>
		  	</div>
		  	</c:if>
		  	
		  	<div class="list${classIndex % 2}"><c:set var="classIndex" value="${classIndex+1}" />
		  	<div class="listfl">Note:</div>
		  	<div class="listp">
		  	<!-- input field -->
			  	<form:textarea rows="8" cols="60" path="commissionReport.commissionLevel2Note" cssClass="textfield" htmlEscape="true" />
		    <!-- end input field -->   	
		  	</div>
		  	</div>
		  	
      </div>
		  		
	<!-- end tab -->
	</div>  
</c:if>

<!-- start button -->
<div align="left" class="button">
<input type="submit" value="<fmt:message key="update"/>" />
<input type="submit" value="<fmt:message key="cancel"/>" name="_cancel" />
</div>
<!-- end button -->	
  	              
<!-- end tabs -->			
</div>
  
  <!-- end table -->
    </td><td class="boxmidr" ></td></tr>
  <tr><td class="botl"></td><td class="botr"></td></tr>
  </table>
  
<!-- end main box -->  
 </div>
     
<br />
</form:form>
</sec:authorize>
  </tiles:putAttribute>
</tiles:insertDefinition>