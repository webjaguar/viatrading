<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/security/tags" prefix="sec" %>

<tiles:insertDefinition name="admin.customers.affiliate" flush="true">
  <tiles:putAttribute name="content" type="string">
  
<sec:authorize ifAnyGranted="ROLE_AEM,ROLE_ADMIN_${prop_site_id},ROLE_CUSTOMER_VIEW_LIST,ROLE_CUSTOMER_AFFILIATE">
<c:if test="${model.count > 0}">
<script type="text/javascript" src="../../admin/javascript/multiple.open.accordion1.2.js"></script>
<script type="text/javascript">
<!--
	var box2 = {};
	window.addEvent('domready', function(){
		$$('img.action').each(function(img){
			//containers
			var actionList = img.getParent(); 
			var actionHover = actionList.getElements('div.action-hover')[0];
			actionHover.set('opacity',0);
			//show/hide
			img.addEvent('mouseenter',function() {
				actionHover.setStyle('display','block').fade('in');
			});
			actionHover.addEvent('mouseleave',function(){
				actionHover.fade('out');
			});
			actionList.addEvent('mouseleave',function() {
				actionHover.fade('out');
			});
		});
		var box2 = new multiBox('mbCustomer', {descClassName: 'multiBoxDesc',waitDuration: 5,showNumbers: true,showControls: false,overlay: new overlay()});
		var Tips1 = new Tips('.toolTipImg');
	});
//-->
</script>  
</c:if>
<form action="affiliateList.jhtm" id="list" method="post"> 
<input type="hidden" id="sort" name="sort" value="${affiliateSearch.sort}" /> 
<!-- main box -->
  <div id="mboxfull">
  
  <!-- start table -->
  <table cellpadding="0" cellspacing="0" border="0" class="module" >
  <tr>
    <td class="topl_g">
	  
	  <!-- breadcrumb -->
	  <p class="breadcrumb">
	    <a href="../affiliate">Affiliate</a> &gt;
	    affiliates 
	  </p>
	  
	  <!-- Error Message -->
	  
	  <!-- header image -->
	  <img class="headerImage" src="../graphics/affiliate.gif" />
	  
	  
	  </td><td class="topr_g" ></td></tr>
  <tr><td class="boxmidlrg" >

 <!-- tabs -->
 <div class="tab-wrapper">
	<h4 title=""></h4>
	<div>
	<!-- start tab -->

	  	<div class="listdivi"></div>
	   		    
		  	<div class="listlight">
		  	<!-- input field --> 
		  	<table border="0" cellpadding="0" cellspacing="1" width="100%">
			  <tr>
			  <c:if test="${model.count > 0}">
			  <td class="pageShowing">
			  <fmt:message key="showing">
				<fmt:param value="${affiliateSearch.offset + 1}"/>
				<fmt:param value="${model.pageEnd}"/>
				<fmt:param value="${model.count}"/>
			  </fmt:message>
			  </td>
			  </c:if>
			  <td class="pageNavi">
			  Page 
			  <c:choose>
			  <c:when test="${model.pageCount <= 100 and model.pageCount > 0}">
			  <select id="page" name="page" onchange="submit()">
			  <c:forEach begin="1" end="${model.pageCount}" var="page">
			  	<option value="${page}" <c:if test="${page == (affiliateSearch.page)}">selected</c:if>>${page}</option>
			  </c:forEach>
			  </select>
			  </c:when>
			  <c:otherwise>
			  <input type="text" id="page" name="page" value="${affiliateSearch.page}" size="5" class="textfield50" />
			  <input type="submit" value="go"/>
			  </c:otherwise>
			  </c:choose>			  
			  of <c:out value="${model.pageCount}"/>
			  | 
			  <c:if test="${affiliateSearch.page == 1}"><span class="pageNaviDead"><fmt:message key="previous" /></span></c:if>
			  <c:if test="${affiliateSearch.page != 1}"><a href="<c:url value="affiliateList.jhtm"><c:param name="page" value="${affiliateSearch.page-1}"/></c:url>" class="pageNaviLink"><fmt:message key="previous" /></a></c:if>
			  | 
			  <c:if test="${affiliateSearch.page == model.pageCount}"><span class="pageNaviDead"><fmt:message key="next" /></span></c:if>
			  <c:if test="${affiliateSearch.page != model.pageCount}"><a href="<c:url value="affiliateList.jhtm"><c:param name="page" value="${affiliateSearch.page+1}"/></c:url>" class="pageNaviLink"><fmt:message key="next" /></a></c:if>
			  </td>
			  </tr>
			</table>
		  	
			<table border="0" cellpadding="0" cellspacing="0" width="100%" class="listings">
			  <tr class="listingsHdr2">
			    <c:set var="cols" value="0"/>
			    <td width="15px">&nbsp;</td>
			    <td>
			    	<table cellspacing="0" cellpadding="1">
			    	  <tr>
			    	    <c:choose>
			    	      <c:when test="${affiliateSearch.sort == 'last_name desc, first_name desc'}">
			    	        <td class="listingsHdr3">
			    	        <a class="hdrLink" href="#" onClick="document.getElementById('sort').value='last_name, first_name';document.getElementById('list').submit()"><fmt:message key="lastName" /></a>
			    	        </td>
			    	        <td><img src="../graphics/up.gif" border="0"></td>
			    	      </c:when>
			    	      <c:when test="${affiliateSearch.sort == 'last_name, first_name'}">
			    	        <td class="listingsHdr3">
			    	        <a class="hdrLink" href="#" onClick="document.getElementById('sort').value='last_name desc, first_name desc';document.getElementById('list').submit()"><fmt:message key="lastName" /></a>
			    	        </td>
			    	        <td><img src="../graphics/down.gif" border="0"></td>
			    	      </c:when>
			    	      <c:otherwise>
			    	        <td colspan="2" class="listingsHdr3">
			    	        <a class="hdrLink" href="#" onClick="document.getElementById('sort').value='last_name desc, first_name desc';document.getElementById('list').submit()"><fmt:message key="lastName" /></a>
			    	        </td>
			    	      </c:otherwise>
			    	    </c:choose>  
			    	  </tr>
			    	  </table>
			    </td>
			    <td>
			    	<table cellspacing="0" cellpadding="1">
			    	  <tr>
			    	    <c:choose>
			    	      <c:when test="${affiliateSearch.sort == 'first_name desc, last_name desc'}">
			    	        <td class="listingsHdr3">
			    	        <a class="hdrLink" href="#" onClick="document.getElementById('sort').value='first_name, last_name';document.getElementById('list').submit()"><fmt:message key="firstName" /></a>
			    	        </td>
			    	        <td><img src="../graphics/up.gif" border="0"></td>
			    	      </c:when>
			    	      <c:when test="${affiliateSearch.sort == 'first_name, last_name'}">
			    	        <td class="listingsHdr3">
			    	        <a class="hdrLink" href="#" onClick="document.getElementById('sort').value='first_name desc, last_name desc';document.getElementById('list').submit()"><fmt:message key="firstName" /></a>
			    	        </td>
			    	        <td><img src="../graphics/down.gif" border="0"></td>
			    	      </c:when>
			    	      <c:otherwise>
			    	        <td colspan="2" class="listingsHdr3">
			    	        <a class="hdrLink" href="#" onClick="document.getElementById('sort').value='first_name desc, last_name desc';document.getElementById('list').submit()"><fmt:message key="firstName" /></a>
			    	        </td>
			    	      </c:otherwise>
			    	    </c:choose>  
			    	  </tr>
			    	  </table>
			    </td>
			    <td>
			    	<table cellspacing="0" cellpadding="1">
			    	  <tr>
			    	    <c:choose>
			    	      <c:when test="${affiliateSearch.sort == 'username desc'}">
			    	        <td class="listingsHdr3">
			    	        <a class="hdrLink" href="#" onClick="document.getElementById('sort').value='username';document.getElementById('list').submit()"><fmt:message key="emailAddress" /></a>
			    	        </td>
			    	        <td><img src="../graphics/up.gif" border="0"></td>
			    	      </c:when>
			    	      <c:when test="${affiliateSearch.sort == 'username'}">
			    	        <td class="listingsHdr3">
			    	        <a class="hdrLink" href="#" onClick="document.getElementById('sort').value='username desc';document.getElementById('list').submit()"><fmt:message key="emailAddress" /></a>
			    	        </td>
			    	        <td><img src="../graphics/down.gif" border="0"></td>
			    	      </c:when>
			    	      <c:otherwise>
			    	        <td colspan="2" class="listingsHdr3">
			    	        <a class="hdrLink" href="#" onClick="document.getElementById('sort').value='username desc';document.getElementById('list').submit()"><fmt:message key="emailAddress" /></a>
			    	        </td>
			    	      </c:otherwise>
			    	    </c:choose>  
			    	  </tr>
			    	  </table>
			    </td>
			    <td>
			    	<table cellspacing="0" cellpadding="1">
			    	  <tr>
			    	    <td rowspan="2" class="listingsHdr3"><fmt:message key="accountNumber" /></td>
			          </tr>
			    	</table>
			    </td>
			    <td align="center">
			    	<table cellspacing="0" cellpadding="1">
			    	  <tr>
			    	    <c:choose>
			    	      <c:when test="${affiliateSearch.sort == 'created desc'}">
			    	        <td class="listingsHdr3">
			    	        <a class="hdrLink" href="#" onClick="document.getElementById('sort').value='created';document.getElementById('list').submit()"><fmt:message key="accountCreated" /></a>
			    	        </td>
			    	        <td><img src="../graphics/up.gif" border="0"></td>
			    	      </c:when>
			    	      <c:when test="${affiliateSearch.sort == 'created'}">
			    	        <td class="listingsHdr3">
			    	        <a class="hdrLink" href="#" onClick="document.getElementById('sort').value='created desc';document.getElementById('list').submit()"><fmt:message key="accountCreated" /></a>
			    	        </td>
			    	        <td><img src="../graphics/down.gif" border="0"></td>
			    	      </c:when>
			    	      <c:otherwise>
			    	        <td colspan="2" class="listingsHdr3">
			    	        <a class="hdrLink" href="#" onClick="document.getElementById('sort').value='created desc';document.getElementById('list').submit()"><fmt:message key="accountCreated" /></a>
			    	        </td>
			    	      </c:otherwise>
			    	    </c:choose>  
			    	  </tr>
			    	  </table>
			    </td>
			    <td align="center">
			    	<table cellspacing="0" cellpadding="1">
			    	  <tr>
			    	    <td rowspan="2" class="listingsHdr3"><fmt:message key="imported" /></td>
			          </tr>
			    	</table>
			    </td>
			    <td align="center">
			    	<table cellspacing="0" cellpadding="1">
			    	  <tr>
			    	    <c:choose>
			    	      <c:when test="${affiliateSearch.sort == 'num_of_logins desc'}">
			    	        <td class="listingsHdr3">
			    	        <a class="hdrLink" href="#" onClick="document.getElementById('sort').value='num_of_logins';document.getElementById('list').submit()"><fmt:message key="numOfLogins" /></a>
			    	        </td>
			    	        <td><img src="../graphics/up.gif" border="0"></td>
			    	      </c:when>
			    	      <c:when test="${affiliateSearch.sort == 'num_of_logins'}">
			    	        <td class="listingsHdr3">
			    	        <a class="hdrLink" href="#" onClick="document.getElementById('sort').value='num_of_logins desc';document.getElementById('list').submit()"><fmt:message key="numOfLogins" /></a>
			    	        </td>
			    	        <td><img src="../graphics/down.gif" border="0"></td>
			    	      </c:when>
			    	      <c:otherwise>
			    	        <td colspan="2" class="listingsHdr3">
			    	        <a class="hdrLink" href="#" onClick="document.getElementById('sort').value='num_of_logins desc';document.getElementById('list').submit()"><fmt:message key="numOfLogins" /></a>
			    	        </td>
			    	      </c:otherwise>
			    	    </c:choose>  
			    	  </tr>
			    	  </table>
			    </td>
			    <td align="center">
			    	<table cellspacing="0" cellpadding="1">
			    	  <tr>
			    	    <c:choose>
			    	      <c:when test="${affiliateSearch.sort == 'affiliate_num_child desc'}">
			    	        <td class="listingsHdr3">
			    	        <a class="hdrLink" href="#" onClick="document.getElementById('sort').value='affiliate_num_child';document.getElementById('list').submit()"><fmt:message key="child" /></a>
			    	        </td>
			    	        <td><img src="../graphics/up.gif" border="0"></td>
			    	      </c:when>
			    	      <c:when test="${affiliateSearch.sort == 'affiliate_num_child'}">
			    	        <td class="listingsHdr3">
			    	        <a class="hdrLink" href="#" onClick="document.getElementById('sort').value='affiliate_num_child desc';document.getElementById('list').submit()"><fmt:message key="child" /></a>
			    	        </td>
			    	        <td><img src="../graphics/down.gif" border="0"></td>
			    	      </c:when>
			    	      <c:otherwise>
			    	        <td colspan="2" class="listingsHdr3">
			    	        <a class="hdrLink" href="#" onClick="document.getElementById('sort').value='affiliate_num_child desc';document.getElementById('list').submit()"><fmt:message key="child" /></a>
			    	        </td>
			    	      </c:otherwise>
			    	    </c:choose>  
			    	  </tr>
			    	  </table>
			    </td>
			    <td align="center">
			    	<table cellspacing="0" cellpadding="1">
			    	  <tr>
			    	    <td rowspan="2" class="listingsHdr3">#<fmt:message key="order" /></td>
			          </tr>
			    	</table>
			    </td>
			    <td align="center">
			    	<table cellspacing="0" cellpadding="1">
			    	  <tr>
			    	    <td rowspan="2" class="listingsHdr3"><fmt:message key="commission" /></td>
			          </tr>
			    	</table>
			    </td>
			    <c:if test="${gSiteConfig['gSALES_REP']}">
			    <td align="center">
			    	<table cellspacing="0" cellpadding="1">
			    	  <tr>
			    	    <c:choose>
			    	      <c:when test="${affiliateSearch.sort == 'sales_rep_id desc'}">
			    	        <td class="listingsHdr3">
			    	        <a class="hdrLink" href="#" onClick="document.getElementById('sort').value='sales_rep_id';document.getElementById('list').submit()"><fmt:message key="salesRep" /></a>
			    	        </td>
			    	        <td><img src="../graphics/up.gif" border="0"></td>
			    	      </c:when>
			    	      <c:when test="${affiliateSearch.sort == 'sales_rep_id'}">
			    	        <td class="listingsHdr3">
			    	        <a class="hdrLink" href="#" onClick="document.getElementById('sort').value='sales_rep_id desc';document.getElementById('list').submit()"><fmt:message key="salesRep" /></a>
			    	        </td>
			    	        <td><img src="../graphics/down.gif" border="0"></td>
			    	      </c:when>
			    	      <c:otherwise>
			    	        <td colspan="2" class="listingsHdr3">
			    	        <a class="hdrLink" href="#" onClick="document.getElementById('sort').value='sales_rep_id desc';document.getElementById('list').submit()"><fmt:message key="salesRep" /></a>
			    	        </td>
			    	      </c:otherwise>
			    	    </c:choose>
			    	  </tr>
			    	  </table>
			    </td>			    
			    </c:if>
			    <td align="center">
			    	<table cellspacing="0" cellpadding="1">
			    	  <tr>
			    	    <c:choose>
			    	      <c:when test="${affiliateSearch.sort == 'trackcode desc'}">
			    	        <td class="listingsHdr3">
			    	        <a class="hdrLink" href="#" onClick="document.getElementById('sort').value='trackcode';document.getElementById('list').submit()"><fmt:message key="trackcode" /></a>
			    	        </td>
			    	        <td><img src="../graphics/up.gif" border="0"></td>
			    	      </c:when>
			    	      <c:when test="${affiliateSearch.sort == 'trackcode'}">
			    	        <td class="listingsHdr3">
			    	        <a class="hdrLink" href="#" onClick="document.getElementById('sort').value='trackcode desc';document.getElementById('list').submit()"><fmt:message key="trackcode" /></a>
			    	        </td>
			    	        <td><img src="../graphics/down.gif" border="0"></td>
			    	      </c:when>
			    	      <c:otherwise>
			    	        <td colspan="2" class="listingsHdr3">
			    	        <a class="hdrLink" href="#" onClick="document.getElementById('sort').value='trackcode desc';document.getElementById('list').submit()"><fmt:message key="trackcode" /></a>
			    	        </td>
			    	      </c:otherwise>
			    	    </c:choose>  
			    	  </tr>
			    	  </table>
			    </td>
			  </tr>
			  <c:set var="totalCommission" value="0.0" />
			<c:forEach items="${model.affiliates}" var="customer" varStatus="status">
			  <tr class="row${status.index % 2}"  onclick="toggleInfo('${customer.id}')">
			    <td align="center">
			    <img class="action" width="23" height="16" alt="Actions" src="../graphics/actions.png"/>
			    <div class="action-hover" style="display: none;">
			        <ul class="round">
			            <li class="action-header">
					      <div class="name"><c:out value="${customer.address.firstName}"/>&nbsp;<<c:out value="${customer.address.lastName}"/></div>
					      <div class="id"><c:choose><c:when test="${customer.cardId != null}"><fmt:message key="idCard"/>: <c:out value="${customer.cardId}" /></c:when> <c:otherwise><fmt:message key="id"/>: <c:out value="${customer.id}" /></c:otherwise>  </c:choose></div>
					      <div class="menudiv"></div>
					      <div style="position:relative">
					      <span style="width:16px;float:left;"><a href="customerQuickView.jhtm?cid=${customer.id}" rel="width:900,height:400" id="mb${customer.id}" class="mbCustomer" title="<fmt:message key="customer" />ID : ${customer.id}"><img src="../graphics/magnifier.gif" border="0" /></a></span>
					      <c:if test="${customer.crmContactId != null}">
					        <span style="width:16px;float:left;"><a href="../crm/crmContactQuickView.jhtm?id=${customer.crmContactId}" rel="width:900,height:400" id="mb${customer.crmContactId}" class="mbCustomer" title="<fmt:message key="crmContact" />ID : ${customer.crmContactId}"><img src="../graphics/magnifierCrm.gif" border="0" /></a></span>
					      </c:if>
					        <c:if test="${gSiteConfig['gADD_INVOICE']}">
					          <span style="width:16px;float:left;"><a href="../orders/addInvoice.jhtm?cid=${customer.id}"><img src="../graphics/add.png" alt="Add Order" title="Add Order" border="0"></a></span>
					        </c:if>
					        <c:if test="${gSiteConfig['gTICKET']}">
					          <span style="width:16px;float:left;"><a href="../ticket/ticketForm.jhtm?cid=${customer.id}"><img src="../graphics/ticket_small.gif" alt="Add Ticket" title="Add Ticket" border="0"></a></span>
					        </c:if>
					      </div>  
			            </li>
			            <li class="edit"><a href="../customers/customer.jhtm?id=${customer.id}"><fmt:message key="edit" /></a></li>
			            <li class="edit"><a href="../customers/loginAsCustomer.jhtm?cid=${customer.token}" target="_blank"><fmt:message key="loginAsCustomer" /></a></li>
			            <li class="edit"><a href="commissionList.jhtm?id=${customer.id}">commission</a></li>
			            <c:if test="${customer.lastLogin != null}">
			            <li class="edit"><span><fmt:message key="lastLogin" />: <fmt:formatDate type="both" timeStyle="full" pattern="MM/dd/yyyy hh:mm a" value="${customer.lastLogin}"/></span></li>
			            </c:if>
			            <c:if test="${customer.lastModified != null}">
			            <li class="edit"><span><fmt:message key="lastModified" />: <fmt:formatDate type="both" timeStyle="full" pattern="MM/dd/yyyy hh:mm a" value="${customer.lastModified}"/></span></li>
			            </c:if>
			        </ul>
			    </div>
				</td>
			    <td class="nameCol"><c:out value="${customer.address.lastName}"/></td>
			    <td class="nameCol"><c:out value="${customer.address.firstName}"/></td>
			    <td><c:out value="${customer.username}"/></td>
			    <td><c:out value="${customer.accountNumber}"/></td>
			    <td align="center"><fmt:formatDate type="date" timeStyle="default" value="${customer.created}"/></td>
			    <td align="center">
			      <c:if test="${customer.imported}"><img src="../graphics/checkbox.png" /></c:if>
				</td>
			    <td class="countCol"><c:out value="${customer.numOfLogins}"/></td>
			    <td class="countCol"><a href="../customers/customerList.jhtm?affId=${customer.id}"><c:out value="${customer.affiliateNumChild}"/></a></td>
			    <td class="countCol"><a href="../orders/ordersList.jhtm?affId=${customer.id}"><c:out value="${customer.affiliateNumOrder}"/></a></td>
			    <td class="numberCol"><fmt:formatNumber value="${customer.commissionTotal}" pattern="#,##0.00" /></td><c:set var="totalCommission" value="${customer.commissionTotal + totalCommission}" />
			    <c:if test="${gSiteConfig['gSALES_REP']}">
			    <td align="center"><c:out value="${model.salesRepMap[customer.salesRepId].name}"/></td>
			    </c:if>
			    <td align="center"><c:out value="${customer.trackcode}"/></td>
			  </tr>
			</c:forEach>
			<tr class="totals">
			  <td style="color:#666666; padding: 5px" colspan="10" align="left"><fmt:message key="total" /></td>
			  <td class="numberCol" align="right"><fmt:formatNumber value="${totalCommission}" pattern="#,##0.00" /></td>
			</tr>
			<c:if test="${model.count == 0}">
			  <tr class="emptyList"><td colspan="9">&nbsp;</td></tr>
			</c:if>
			</table>
			
			<table border="0" cellpadding="0" cellspacing="1" width="100%">
			  <tr>
			  <td class="pageSize">
			  <select name="size" onchange="document.getElementById('page').value=1;submit()">
			  <c:forTokens items="10,25,50,100,200,500" delims="," var="current">
			  	  <option value="${current}" <c:if test="${current == affiliateSearch.pageSize}">selected</c:if>>${current} <fmt:message key="perPage" /></option>
			  </c:forTokens>
			  </select>
			  </td>
			  </tr>
			</table>  
		  	<!-- end input field -->  	  
		  	</div>
		  	
		  	<div class="note_list">
			  You can add <c:out value="${gSiteConfig['gNUMBER_AFFILIATE'] - model.count == 0}" /> <fmt:message key="affiliate" />.
			</div>
			
		  	<!-- start button --> 
		  	<!-- end button -->	
	 
	<!-- end tab -->        
	</div>
<!-- end tabs -->			
</div>
  
  <!-- end table -->
   </td><td class="boxmidr" ></td></tr>
  <tr><td class="botl"></td><td class="botr"></td></tr>
  </table>
  
<!-- end main box -->  
</div>
</form>	  
</sec:authorize>

  </tiles:putAttribute>    
</tiles:insertDefinition>