<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ taglib uri="/WEB-INF/tlds/spring.tld" prefix="spring" %>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ taglib uri="http://www.springframework.org/security/tags" prefix="sec" %>

<tiles:insertDefinition name="admin.orders" flush="true">
  <tiles:putAttribute name="tab"  value="orders" />
  <tiles:putAttribute name="content" type="string">

<script type="text/javascript" src="../javascript/autocompleter1.2.js"></script>
<script type="text/javascript" src="../javascript/autocompleter.Request1.2.js"></script>
<script type="text/javascript" src="../javascript/autocompleter.Local1.2.js"></script>
<script type="text/javascript" src="../javascript/observer.js"></script>
<link href="<c:url value="/assets/invoice.css"/>" rel="stylesheet" type="text/css"/>
<script type="text/javascript">
function addOption(lineNumber) {
	var index = $('index_'+lineNumber).value;
	var tr = document.createElement("tr");
	
	var td = document.createElement("td");
	tr.appendChild(td);
	
	var imageTag = document.createElement("img");
	imageTag.setAttribute("src", "../graphics/add.png");
	var aTag = document.createElement("a");
	aTag.setAttribute("onclick", "removeOption('__optionName_"+index+"_"+lineNumber+"')");
	aTag.appendChild(imageTag);
	var td = document.createElement("td");
	td.appendChild(aTag);
	tr.appendChild(td);
	
	
	
	var td2 = document.createElement("td"); 
	td2.setAttribute("style", "padding: 5px 5px 5px 5px;");
	var textfield2 = document.createElement("input");
	textfield2.setAttribute("type", "text"); 
	textfield2.setAttribute("name", "__optionName_"+index+"_"+lineNumber); 
	textfield2.setAttribute("id", "__optionName_"+index+"_"+lineNumber); 
	textfield2.setAttribute("placeholder", "name"); 
	textfield2.setAttribute("value", "");
	textfield2.setAttribute("size", "15");
	td2.appendChild(textfield2);
	tr.appendChild(td2);
	
	var td3 = document.createElement("td"); 
	td3.setAttribute("style", "padding: 5px 5px 5px 5px;");
	var textfield3 = document.createElement("input");
	textfield3.setAttribute("type", "text"); 
	textfield3.setAttribute("name", "__optionValueString_"+index+"_"+lineNumber); 
	textfield3.setAttribute("id", "__optionValueString_"+index+"_"+lineNumber); 
	textfield3.setAttribute("placeholder", "value"); 
	textfield3.setAttribute("value", "");
	textfield3.setAttribute("size", "15");
	td3.appendChild(textfield3);
	tr.appendChild(td3);
	
	document.getElementById('productAttribute_'+lineNumber).appendChild(tr);
	$('index_'+lineNumber).set('value', parseInt(index) + 1);
}
function removeOption(ele) {
	var input = ele;
	var currentColumn = $(input).parentNode;
	var currentRow = currentColumn.parentNode;
	var parentRow = currentRow.parentNode;
	parentRow.removeChild(currentRow);
}

function addOptionNew(lineNumber) {
	var index = $('index_new_'+lineNumber).value;
	
	var tr = document.createElement("tr");
	var imageTag = document.createElement("img");
	imageTag.setAttribute("src", "../graphics/add.png");
	var aTag = document.createElement("a");
	aTag.setAttribute("onclick", "removeOption('__optionName_new_"+index+"_"+lineNumber+"')");
	aTag.appendChild(imageTag);
	var td = document.createElement("td");
	td.appendChild(aTag);
	tr.appendChild(td);
	
	
	var td2 = document.createElement("td"); 
	td2.setAttribute("style", "padding: 5px 5px 5px 5px;");
	var textfield2 = document.createElement("input");
	textfield2.setAttribute("type", "text"); 
	textfield2.setAttribute("name", "__optionName_new_"+index+"_"+lineNumber); 
	textfield2.setAttribute("id", "__optionName_new_"+index+"_"+lineNumber); 
	textfield2.setAttribute("value", "");
	textfield2.setAttribute("size", "15");
	textfield2.setAttribute("placeholder", "name"); 
	td2.appendChild(textfield2);
	tr.appendChild(td2);
	
	var td3 = document.createElement("td"); 
	td3.setAttribute("style", "padding: 5px 5px 5px 5px;");
	var textfield3 = document.createElement("input");
	textfield3.setAttribute("type", "text"); 
	textfield3.setAttribute("name", "__optionValueString_new_"+index+"_"+lineNumber); 
	textfield3.setAttribute("id", "__optionValueString_new_"+index+"_"+lineNumber); 
	textfield3.setAttribute("value", "");
	textfield3.setAttribute("size", "15");
	textfield3.setAttribute("placeholder", "value"); 
	td3.appendChild(textfield3);
	tr.appendChild(td3);
	
	
	document.getElementById('productAttribute_new_'+lineNumber).appendChild(tr);
	$('index_new_'+lineNumber).set('value', parseInt(index) + 1);
}
//-->
</script>  
<script type="text/javascript">
<!--
function buySAFEOnClick(NewWantsBondValue) {
	document.getElementById('WantsBondField').value = NewWantsBondValue;
	document.forms['invoiceForm'].submit();
}
window.addEvent('domready', function(){			
	var tabs1 = new SimpleTabs($('tab-block-1'), { selector: 'h4'});
	var box3 = new multiBox('mbCustomer', {waitDuration: 5,overlay: new overlay()});
	for (var i=1;i<=5;i=i+1) {
		this["el"+i] = $('addSku'+i);
		new Autocompleter.Request.HTML(this["el"+i], '../orders/show-ajax-skus.jhtm', {
		'indicatorClass': 'autocompleter-loading', 'maxChoices': '40' });
	}
});
    function choosePromo(el) {
    	document.getElementById('promoTitle').value=el.value;
    	document.getElementById('promoDiscount').value = document.getElementById('discount_'+el.value).value;
    	<c:if test="${gSiteConfig['gSALES_PROMOTIONS_ADVANCED']}">
    	document.getElementById('promoDiscountType').value = document.getElementById('discount_type_'+el.value).value;
    	</c:if>
    	document.getElementById('promoPercent').value = document.getElementById('percent_'+el.value).value;
	}
	function chooseCustomShippingRate(el) {
	    document.getElementById('shippingTitle').value=el.value;
	    document.getElementById('shippingCost').value = document.getElementById('price_'+el.value).value;
	}	
	function chooseAddress(el) {
		$('order.shipping.code').value = $('code_'+el.value).value;
	    $('order.shipping.firstName').value = $('firstname_'+el.value).value;
	    $('order.shipping.lastName').value = $('lastname_'+el.value).value;
	    $('order.shipping.country').value = $('country_'+el.value).value;
	    $('order.shipping.company').value = $('company_'+el.value).value;
	    $('order.shipping.addr1').value = $('addr1_'+el.value).value;
	    $('order.shipping.addr2').value = $('addr2_'+el.value).value;
	    $('order.shipping.city').value = $('city_'+el.value).value;
	    $('shipping_state').value = $('state_'+el.value).value;
	    $('shipping_ca_province').value = $('state_'+el.value).value;
	    $('order.shipping.zip').value = $('zip_'+el.value).value;
	    $('order.shipping.phone').value = $('phone_'+el.value).value;
	    $('order.shipping.cellPhone').value = $('cellPhone_'+el.value).value;
	    $('order.shipping.fax').value = $('fax_'+el.value).value;
	    if ( $('residential_'+el.value).value == 'true' ) {
	    	$('order.shipping.residential1').checked = true;
	    } else {
	    	$('order.shipping.residential1').checked = false;
	    }
	}	   
<c:if test="${gSiteConfig['gRMA']}">
function addSerialNum(index, suffix) {
  var textfield = document.createElement("input"); 
  textfield.setAttribute("type", "text"); 
  textfield.setAttribute("name", "__serialNum_" + suffix + index); 
  textfield.setAttribute("id", "__serialNum_" + suffix + index); 
  textfield.setAttribute("size", "10");		
  textfield.setAttribute("maxlength", "50");
  document.getElementById('__serialNums_' + suffix + index).appendChild(textfield);
  var comma = document.createTextNode(", ");
  document.getElementById('__serialNums_' + suffix + index).appendChild(comma);
}
</c:if>
<c:if test="${invoiceForm.newInvoice and (gSiteConfig['gADD_INVOICE'] or gSiteConfig['gSERVICE']) and siteConfig['INVOICE_REMOVE_VALIDATION'].value != 'true'}">
function checkRequiredValues() {
 if ($('shippingTitle').value == "") {
 	alert("Please select Shipping Method");
 	return false;
 } 
 if ($('order.paymentMethod').value == "" && $('order.vba').value == "" ) {
 	alert("Please select Payment Method");
 	return false;
 }
 if ($('order.orderType').value == "") {
 	alert("Please select Order Type");
 	return false;
 }
 <c:if test="${siteConfig['ORDER_TRACKCODE_REQUIRED'].value == 'true'}">
 if ($('order.trackcode').value == "") {
 	alert("Please type a trackcode for this order");
 	return false;
 }
 </c:if>
 <c:if test="${siteConfig['ORDER_FLAG1_NAME'].value != ''}">
 if ($('order.flag1').value == "") {
 	alert("Please select <c:out value="${siteConfig['ORDER_FLAG1_NAME'].value}" />");
 	return false;
 }
 </c:if>
 return true;
}
</c:if>
//-->
</script>   
<form:form commandName="invoiceForm" method="post">
<input type="hidden" name="WantsBondField" value="${invoiceForm.order.wantsBond}" id="WantsBondField">
  
<c:if test="${invoiceForm.order != null}">
  <!-- main box -->
  <div id="mboxfull">
  
  <!-- start table -->
  <table cellpadding="0" cellspacing="0" border="0" class="module" >
  <tr>
    <td class="topl_g">
      <spring:bind path="invoiceForm.order.orderId">
		<input type="hidden" name="${status.expression}" value="${status.value}">
	  </spring:bind>
	  <!-- breadcrumb -->
	  <p class="breadcrumb">
	     <c:choose>
	        <c:when test="${(gSiteConfig['gSHIPPING_QUOTE'] or siteConfig['PRODUCT_QUOTE'].value == 'true') and orderName == 'quotes'}">	        	
	    		<a href="../orders/quotesList.jhtm">Quotes</a> &gt;
	    		quote
	        </c:when>
	        <c:otherwise>		       
	    		<a href="../orders">Order</a> &gt;
	    		<fmt:message key="order"/> 
	        </c:otherwise>	        
		 </c:choose>
	    <c:if test="${!invoiceForm.newInvoice}"><a href="invoice.jhtm?order=${invoiceForm.order.orderId}"> #${invoiceForm.order.orderId}</a></c:if>
	  </p>

	  <!-- Error Message -->
	  <c:if test="${message != null}">
		<div class="message"><fmt:message key="${message}" /></div>
	  </c:if>
	  
	  <spring:bind path="invoiceForm.*">
		  <c:forEach var="error" items="${status.errorMessages}">
		    <div class="message"><c:out value="${error}"/></div>
		  </c:forEach>
	  </spring:bind>
	  
	  <div class="paymentMessage">
	  <c:if test="${paymentMessage != null}">
	  	<div class="message"><c:out value="${paymentMessage}"/></div>
	  </c:if>
	  </div>
	  
	  <div align="left">
		<table border="0" class="form">
		  <c:if test="${!invoiceForm.newInvoice}">
			<c:choose>
			  <c:when test="${siteConfig['CHANGE_ORDER_DATE'].value == 'true'}">
			    <tr>
			      <td><fmt:message key="orderDate" /></td><td>:</td> <td><form:input path="orderDate" size="18"  />
			      <img id="time_start_trigger" class="calendarImage" src="../graphics/calendarIcon.jpg" alt="Pick a date" />
			      	<form:errors path="orderDate" type ="hidden" cssClass="error">
			      	  <span style=color:#E00000><b>!Wrong date format</b></span>
			      	  <img class="toolTipImg" title="Format is 01/01/2017[01:00 PM]" src="../graphics/question.gif" />    	
			      	</form:errors>
			      </td>
			    </tr>  
			  </c:when>
			  <c:otherwise>
			    <tr>
			      <td colspan="3"><fmt:message key="orderDate" />: <fmt:formatDate type="both" timeStyle="full" value="${invoiceForm.order.dateOrdered}"/></td>
			    </tr>
			  </c:otherwise>
			</c:choose>
			<c:if test="${siteConfig['DELIVERY_TIME'].value == 'true'}" >
			 <tr>
			   <td>Days to Ship</td><td>:</td>
			   <td>
			    <form:select path="order.shippingPeriod" >
			     <c:forEach begin="0" end="10" step="1" var="value">
			      <form:option value="${value}"><c:out value="${value}" /> Day</form:option>
			     </c:forEach>
			    </form:select>
			   </td>
			 </tr>  
			 <tr>
			   <td>Turn Over Day</td><td>:</td>
			   <td>
			    <form:select path="order.turnOverday">
			     <c:forEach begin="0" end="10" step="1" var="value">
			      <form:option value="${value}"><c:out value="${value}" /> Day</form:option>
			     </c:forEach>
			    </form:select>
			   </td>
			 </tr>  
			</c:if>
			<tr>
			  <td><fmt:message key="lastModified" /></td><td>:</td><td><fmt:formatDate type="both" timeStyle="full" value="${invoiceForm.order.lastModified}"/></td>
			</tr>  
		    <tr>
		      <td colspan="3"><b>Your <fmt:message key="${orderName}Number"/> is #<c:out value="${invoiceForm.order.orderId}"/></b></td>
		    </tr>  
		  </c:if>
		</table>
	  </div>	    

  
  </td><td class="topr_g" ></td></tr>
  <tr><td class="boxmidlrg" >

 <!-- tabs -->
 <div id="tab-block-1">
	<h4 title="Invoice"><fmt:message key="${orderName}"/></h4>
	<div>
	<!-- start tab -->
        <c:set var="classIndex" value="0" />
        
	  	<div class="listdivi ln tabdivi"></div>
	  	<div class="listdivi"></div>
	   
		  	<div class="list${classIndex % 2}"><c:set var="classIndex" value="${classIndex+1}" />
		  	<!-- input field -->
			<table cellpadding="0" cellspacing="0" border="0" width="100%">
			<tr valign="top">
			<td>
			<b><fmt:message key="billingInformation" />:</b>
			<table>
			  <tr>
			    <td><fmt:message key="firstName" />:</td>
			    <td>
			    <spring:bind path="invoiceForm.order.billing.firstName">
			      <input type="text" name="${status.expression}" value="${status.value}">
			    </spring:bind>
			    </td>
			  </tr>
			  <tr>
			    <td><fmt:message key="lastName" />:</td>
			    <td>
			    <spring:bind path="invoiceForm.order.billing.lastName">
			      <input type="text" name="${status.expression}" value="${status.value}">
			    </spring:bind>
			    </td>
			  </tr>
			  <tr>
			    <td><fmt:message key="country" />:</td>
			    <td>
			      <form:select path="order.billing.country"  onchange="toggleStateProvince(this, 'billing')" cssStyle="width:163px;">
			  	    <option value="">Please Select</option>
			  	    <form:options items="${countries}" itemValue="code" itemLabel="name"/>
					<form:errors path="order.billing.country" cssClass="error" />  
				  </form:select>
			    </td>
			  </tr>
			  <tr>
			    <td><fmt:message key="company" />:</td>
			    <td>
			    <spring:bind path="invoiceForm.order.billing.company">
			      <input type="text" name="${status.expression}" value="${status.value}" size="20">
			    </spring:bind>
			    </td>
			  </tr>
			  <tr>
			    <td><fmt:message key="address" /> 1:</td>
			    <td>
			    <spring:bind path="invoiceForm.order.billing.addr1">
			      <input type="text" name="${status.expression}" value="${status.value}" size="20">
			    </spring:bind>
			    </td>
			  </tr>
			  <tr>
			    <td><fmt:message key="address" /> 2:</td>
			    <td>
			    <spring:bind path="invoiceForm.order.billing.addr2">
			      <input type="text" name="${status.expression}" value="${status.value}" size="20">
			    </spring:bind>
			    </td>
			  </tr>
			  <tr>
			    <td><fmt:message key="city" />:</td>
			    <td>
			    <spring:bind path="invoiceForm.order.billing.city">
			      <input type="text" name="${status.expression}" value="${status.value}">
			    </spring:bind>
			    </td>
			  </tr>
			  <tr>
			    <td><fmt:message key="stateProvince" />:</td>
			    <td>
			      <spring:bind path="invoiceForm.order.billing.stateProvince">
					<select id="billing_state" name="<c:out value="${status.expression}"/>">
			  	      <option value="">Please Select</option>
				      <c:forEach items="${states}" var="state">
			  	        <option value="${state.code}" <c:if test="${state.code == status.value}">selected</c:if>>${state.name}</option>
					  </c:forEach>      
			        </select>
					<select id="billing_ca_province" name="<c:out value="${status.expression}"/>">
			  	      <option value="">Please Select</option>
				      <c:forEach items="${caProvinceList}" var="province">
			  	        <option value="${province.code}" <c:if test="${province.code == status.value}">selected</c:if>>${province.name}</option>
					  </c:forEach>      
			        </select> 
			      <input id="billing_province" type="text" name="<c:out value="${status.expression}"/>" value="<c:out value="${status.value}"/>">
			      <c:if test="${status.errorMessage != ''}"><span class="error"><c:out value="${status.errorMessage}"/></span></c:if>
			      </spring:bind>
			    </td>
			  </tr>
			  <tr>
			    <td><fmt:message key="zipPostalCode" />:</td>
			    <td>
			    <spring:bind path="invoiceForm.order.billing.zip">
			      <input type="text" name="${status.expression}" value="${status.value}">
			    </spring:bind>
			    </td>
			  </tr>
			  <tr>
			    <td><fmt:message key="phone" />:</td>
			    <td>
			    <spring:bind path="invoiceForm.order.billing.phone">
			      <input type="text" name="${status.expression}" value="${status.value}" size="20">
			    </spring:bind> 
			    </td>
			  </tr>
			  <tr>
			    <td><fmt:message key="cellPhone" />:</td>
			    <td>
			    <spring:bind path="invoiceForm.order.billing.cellPhone">
			      <input type="text" name="${status.expression}" value="${status.value}" size="20">
			    </spring:bind> 
			    </td>
			  </tr>
			  <tr>
			    <td><fmt:message key="fax" />:</td>
			    <td> 
			    <spring:bind path="invoiceForm.order.billing.fax">
			      <input type="text" name="${status.expression}" value="${status.value}" size="20">
			    </spring:bind>
			    </td>
			  </tr>
			</table>
			</td>
			<td>&nbsp;</td>
			<td>
			<b><c:choose><c:when test="${invoiceForm.order.workOrderNum != null}"><fmt:message key="service" /> <fmt:message key="location" /></c:when><c:otherwise><fmt:message key="shippingInformation" /></c:otherwise></c:choose>:</b>
			<table>
			  <c:if test="${addressList != null and invoiceForm.order.workOrderNum == null}">
			  <tr>
			    <td colspan="2">
			      <select style="font-size:10px;width:270px;" name="addressSelected" id="addressSelected" onChange="chooseAddress(this)">
			  	   <c:forEach items="${addressList}" var="address" varStatus="status">
			  	    <option value="${address.id}"><c:out value="${address.firstName}" /> <c:out value="${address.lastName}" /> <c:out value="${address.addr1}" /> <c:out value="${address.city}" /> <c:out value="${address.zip}" /> <c:out value="${address.stateProvince}" /> <c:out value="${address.country}" /></option>
			       </c:forEach>
			  	  </select>
			      <c:forEach items="${addressList}" var="address" varStatus="status">
			  	   <input type="hidden" id="firstname_${address.id}" value="${address.firstName}" />
			  	   <input type="hidden" id="lastname_${address.id}" value="${address.lastName}" />
			  	   <input type="hidden" id="country_${address.id}" value="${address.country}" />
			  	   <input type="hidden" id="company_${address.id}" value="${address.company}" />
			  	   <input type="hidden" id="addr1_${address.id}" value="${address.addr1}" />
			  	   <input type="hidden" id="addr2_${address.id}" value="${address.addr2}" />
			  	   <input type="hidden" id="city_${address.id}" value="${address.city}" />
			  	   <input type="hidden" id="state_${address.id}" value="${address.stateProvince}" />
			  	   <input type="hidden" id="zip_${address.id}" value="${address.zip}" />
			  	   <input type="hidden" id="phone_${address.id}" value="${address.phone}" />
			  	   <input type="hidden" id="cellPhone_${address.id}" value="${address.cellPhone}" />
			  	   <input type="hidden" id="fax_${address.id}" value="${address.fax}" />
			  	   <input type="hidden" id="residential_${address.id}" value="${address.residential}" />
			  	   <input type="hidden" id="code_${address.id}" value="${address.code}" />
			      </c:forEach>      
			    </td>
			  </tr>
			  </c:if>
			  <tr>
			    <td><fmt:message key="firstName" />:</td>
			    <td>
			    <form:input path="order.shipping.firstName" />
			    </td>
			  </tr>
			  <tr>
			    <td><fmt:message key="lastName" />:</td>
			    <td>
			    <form:input path="order.shipping.lastName" />
			    </td>
			  </tr>
			  <tr>
			    <td><fmt:message key="country" />:</td>
			    <td>
			      <form:select path="order.shipping.country"  onchange="toggleStateProvince(this, 'shipping')" cssStyle="width:163px;">
			  	    <option value="">Please Select</option>
			  	    <form:options items="${countries}" itemValue="code" itemLabel="name"/>
					<form:errors path="order.shipping.country" cssClass="error" />  
				  </form:select>
			    </td>
			  </tr>
			  <tr>
			    <td><fmt:message key="company" />:</td>
			    <td>
			    <form:input path="order.shipping.company" />
			    </td>
			  </tr>
			  <tr>
			    <td><fmt:message key="address" /> 1:</td>
			    <td>
			    <form:input path="order.shipping.addr1" />
			    </td>
			  </tr>
			  <tr>
			    <td><fmt:message key="address" /> 2:</td>
			    <td>
			    <form:input path="order.shipping.addr2" />
			    </td>
			  </tr>
			  <tr>
			    <td><fmt:message key="city" />:</td>
			    <td>
			    <form:input path="order.shipping.city" />
			    </td>
			  </tr>
			  <tr>
			    <td><fmt:message key="stateProvince" />:</td>
			    <td>
			      <spring:bind path="invoiceForm.order.shipping.stateProvince" >
					<select id="shipping_state" name="<c:out value="${status.expression}"/>">
			  	      <option value="">Please Select</option>
				      <c:forEach items="${states}" var="state">
			  	        <option value="${state.code}" <c:if test="${state.code == status.value}">selected</c:if>>${state.name}</option>
					  </c:forEach>      
			        </select>
					<select id="shipping_ca_province" name="<c:out value="${status.expression}"/>">
			  	      <option value="">Please Select</option>
				      <c:forEach items="${caProvinceList}" var="province">
			  	        <option value="${province.code}" <c:if test="${province.code == status.value}">selected</c:if>>${province.name}</option>
					  </c:forEach>      
			        </select>
			      <input id="shipping_province" type="text" name="<c:out value="${status.expression}"/>" value="<c:out value="${status.value}"/>">
			      <c:if test="${status.errorMessage != ''}"><span class="error"><c:out value="${status.errorMessage}"/></span></c:if>
			      </spring:bind>
			    </td>
			  </tr>
			  <tr>
			    <td><fmt:message key="zipPostalCode" />:</td>
			    <td>
			    <form:input path="order.shipping.zip" />
			    </td>
			  </tr>
			  <tr>
			    <td><fmt:message key="phone" />:</td>
			    <td>
			    <form:input path="order.shipping.phone" />
			    </td>
			  </tr>
			  <tr>
			    <td><fmt:message key="cellPhone" />:</td>
			    <td>
			    <form:input path="order.shipping.cellPhone" />
			    </td>
			  </tr>  
			  <tr>
			    <td><fmt:message key="fax" />:</td>
			    <td> 
			    <form:input path="order.shipping.fax" />
			    </td>
			  </tr>
			  <tr>
			    <td><fmt:message key="residential" />:</td>
			    <td> 
			    <form:checkbox path="order.shipping.residential" />
			    </td>
			  </tr>
			  <tr>
			    <td><fmt:message key="liftGateDelivery" />:</td>
			    <td> 
			    <form:checkbox path="order.shipping.liftGate" />
			    </td>
			  </tr>
			  <tr>
			    <td><fmt:message key="code" />:</td>
			    <td> 
			    <form:input path="order.shipping.code" />
			    </td>
			  </tr>
			  <c:if test="${workOrder.service.alternateContact != ''}">	
			  <tr>
			    <td><fmt:message key="alternateContact" />:</td>
			    <td><c:out value="${workOrder.service.alternateContact}"/></td>
			  </tr>
			  </c:if>
			</table>
			</td>
			<td style="width:15%;">&nbsp;</td>
			<td align="left">
			<b><fmt:message key="customerInformation" />:</b><a href="../customers/customerQuickView.jhtm?cid=${customer.id}" rel="width:900,height:400" id="mb${customer.id}" class="mbCustomer" title="<fmt:message key="customer" />ID : ${customer.id}"><img src="../graphics/magnifier.gif" border="0"></a>
			<table cellpadding="0" cellspacing="0">
			  <tr>
			    <td width="70"><fmt:message key="account"/> #</td>
			    <td>:&nbsp;&nbsp;</td>
			    <td><c:out value="${customer.accountNumber}"/></td>
			  </tr>
			  <tr>
			    <td><fmt:message key="taxID"/> #</td>
			    <td>:&nbsp;&nbsp;</td>
			    <td><c:out value="${customer.taxId}"/></td>
			  </tr>
			  <tr>
			    <td><fmt:message key="orderCount"/></td>
			    <td>:&nbsp;&nbsp;</td>
			    <td><a href="ordersList.jhtm?email=<c:out value='${customer.username}'/>"><c:out value="${customerOrderInfo.orderCount}"/></a></td>
			  </tr>
			  <tr>
			    <td><fmt:message key="lastOrder"/></td>
			    <td>:&nbsp;&nbsp;</td>
			    <td><fmt:formatDate pattern="MM/dd/yyyy" value="${customerOrderInfo.lastOrderDate}"/></td>
			  </tr>
			  <c:if test="${languageCodes != null}">
			  <tr>
			    <td><fmt:message key="language"/></td>
			    <td>:&nbsp;&nbsp;</td>
			    <td>
       			  <form:select path="order.languageCode">
        	  	    <form:option value="en"><fmt:message key="language_en"/></form:option>
        	  		<c:forEach items="${languageCodes}" var="i18n">
          			  <form:option value="${i18n.languageCode}"><fmt:message key="language_${i18n.languageCode}"/></form:option>
        	  		</c:forEach>
	  			  </form:select>
			  </tr>
			  
			  </c:if>
			  <tr>
			  <td><fmt:message key="customerPhone"/> #</td>
			    <td>:&nbsp;&nbsp;</td>
			    <td><c:out value="${phoneNumber}"/></td>
			</tr>
			<tr>
			  <td><fmt:message key="customerNote"/></td>
			    <td>:&nbsp;&nbsp;</td>
			    <td><c:out value="${customerNote}"/></td>
			</tr>
			 
			</table>  
			</td>
			<td style="width:20px;">&nbsp;</td>
			<td align="left">
			<b><fmt:message key="${orderName}Information" />:</b>
			<table cellpadding="0" cellspacing="0">
			  <c:if test="${gSiteConfig['gSALES_REP']}">
			   <tr>
			    <td><fmt:message key="qualifier"/>:</td>
			    <td>
			     <form:select path="order.qualifier">
			      <form:option value=""><fmt:message key="none" /></form:option>
			 	  <form:options items="${salesRepList}" itemValue="id" itemLabel="name"/>
			     </form:select>
			    </td>
			  </tr>
			  <tr>
			    <td><fmt:message key="salesRep"/>:</td>
			    <td>
			     <form:select path="order.salesRepId">
			      <form:option value=""><fmt:message key="none" /></form:option>
			 	  <form:options items="${salesRepList}" itemValue="id" itemLabel="name"/>
			 	  <form:options class="nameLinkInactive" items="${salesRepListInactive}" itemValue="id" itemLabel="name"/>
			     </form:select>
			    </td>
			  </tr>
			  <tr>
			    <td><fmt:message key="processedBy" />:</td>
			    <td>
			     <form:select path="order.salesRepProcessedById">
			      <form:option value=""><fmt:message key="none" /></form:option>
			 	  <form:options items="${salesRepList}" itemValue="id" itemLabel="name"/>
			 	  <form:options class="nameLinkInactive" items="${salesRepListInactive}" itemValue="id" itemLabel="name"/>
			     </form:select>
			    </td>
			  </tr>
			  <c:if test="${siteConfig['NO_COMMISSION_FLAG'].value == 'true'}">
			  <tr>
			    <td>Shared Commission:</td>
			    <td>
			     <form:select path="order.nc">
			 	  <form:option value="true"><fmt:message key="yes" /></form:option>
			 	  <form:option value="false"><fmt:message key="no" /></form:option>
			     </form:select>
			    </td>
			  </tr>
			  </c:if>
			  </c:if>
			  <c:if test="${gSiteConfig['gINVOICE_APPROVAL']}"> 
			  <tr>
			    <td>Require Approval:</td>
			    <td>
			      <form:select path="order.approval">
			      <form:option value=""><fmt:message key="none" /></form:option>
			 	  <form:option value="ra"><fmt:message key="approvalStatus_ra" /></form:option>
			 	  <form:option value="ap"><fmt:message key="approvalStatus_ap" /></form:option>
			     </form:select>
			    </td>
			  </tr>  
			  </c:if>
			  <tr>
			    <td><fmt:message key="orderType"/>:</td>
			    <td>
			     <form:select path="order.orderType">
			        <form:option value=""></form:option>
			      <c:forTokens items="${siteConfig['ORDER_TYPE'].value}" delims="," var="type" varStatus="status">
			        <form:option value ="${type}"><c:out value ="${type}" /></form:option>
		  	      </c:forTokens>
			     </form:select>
			     <form:errors path="order.orderType" cssClass="error" />  
			    </td>
			  </tr> 
			  <c:if test="${siteConfig['ORDER_FLAG1_NAME'].value != ''}">
			  <tr>
			    <td><c:out value="${siteConfig['ORDER_FLAG1_NAME'].value}" />:</td>
			    <td>
			     <form:select path="order.flag1">
			      <form:option value=""></form:option>
			 	  <form:option value="1"><fmt:message key="yes" /></form:option>
			 	  <form:option value="0"><fmt:message key="no" /></form:option>
			     </form:select>
			     <form:errors path="order.flag1" cssClass="error" />  
			    </td>
			  </tr>
			  </c:if>
			 
			  <tr>
			    <td><fmt:message key="trackcode"/>:</td>
			    <td>
			     <form:input path="order.trackcode"  cssStyle="width:112px;"/>
			    </td>
			  </tr>    
			  <tr>
			    <td><fmt:message key="scheduledPickUpDueDate" />:</td>
			    <td><form:input path="order.userDueDate"  size="18"  />
			      <img id="time_start_trigger2" class="calendarImage" src="../graphics/calendarIcon.jpg" alt="Pick a date" />
			      	<form:errors path="order.userDueDate" type ="hidden" cssClass="error">
			      	<span style=color:#E00000><b>!Wrong date format</b></span>
			      	  <img class="toolTipImg" title="Format is 01/01/2017[01:00 PM]" src="../graphics/question.gif" />  
			      	</form:errors>
			     </td>
			  </tr>
			 <c:if test="${siteConfig['ORDER_CUSTOM_FIELD1_NAME'].value != ''}">
			  <tr>
			    <td><c:out value="${siteConfig['ORDER_CUSTOM_FIELD1_NAME'].value}" />:</td>
			    <td>
			      <form:select path="order.customField1" value="order.customField1">
			      <form:option value="">Please Select</form:option>
			 	  <c:forTokens items="${siteConfig['ORDER_CUSTOM_FIELD1_VALUE'].value}" delims="," var="value">
				  		      <form:option value="${value}">${value}</form:option>
				  		   </c:forTokens>
			     </form:select>
			     <form:errors path="order.customField1" cssClass="error" />  
			    </td>
			  </tr>
			  </c:if>
			  <c:if test="${siteConfig['ORDER_CUSTOM_FIELD2_NAME'].value != ''}">
			  <tr>
			    <td><c:out value="${siteConfig['ORDER_CUSTOM_FIELD2_NAME'].value}" />:</td>
			    <td>
			       <form:select path="order.customField2" value="order.customField2">
			      <form:option value="">Please Select</form:option>
			 	  <c:forTokens items="${siteConfig['ORDER_CUSTOM_FIELD2_VALUE'].value}" delims="," var="value">
				  		      <form:option value="${value}">${value}</form:option>
				  		   </c:forTokens>
			     </form:select>
			     <form:errors path="order.customField2" cssClass="error" />
			    </td>
			  </tr>
			  </c:if>
			  <c:if test="${siteConfig['ORDER_CUSTOM_FIELD3_NAME'].value != ''}">
			  <tr>
			    <td><c:out value="${siteConfig['ORDER_CUSTOM_FIELD3_NAME'].value}" />:</td>
			    <td>
			     <form:input path="order.customField3"  cssStyle="width:112px;"/>
			     <form:errors path="order.customField3" cssClass="error" />  
			    </td>
			  </tr>
			  </c:if>
			  <sec:authorize ifAnyGranted="ROLE_AEM,ROLE_ADMIN_${prop_site_id},ROLE_OVERRIDE_PAYMENT_ALERT">
				  <c:if test="${gSiteConfig['gPAYMENTS'] and customer.paymentAlert != null}">
				  <tr>
				    <td><fmt:message key="payment"/> <fmt:message key="alert"/>:</td>
				    <td>
				     <form:checkbox path="order.paymentAlert" />
				    </td>
				  </tr>
				  </c:if>
			  </sec:authorize>
			</table>
			</td>
			</tr>
			</table>
			   
			<b><fmt:message key="purchaseOrder" /></b>: <form:input path="order.purchaseOrder" maxlength="50" htmlEscape="true"/>
			<c:if test="${siteConfig['WORLD_SHIP_IMPORT_ORDERS'].value == true }" >
				<b><fmt:message key="purchaseOrder2" /></b>:<input type="text" id="customerPO2" name="customerPO2" value="${upsWorldShip.customerPO2}" >
			    <b><fmt:message key="carrierPhone" /></b>: <input type="text" id="carrierPhone" name="carrierPhone" value="${upsWorldShip.carrierPhone}">
			</c:if>
			<c:if test="${invoiceForm.order.workOrderNum != null}" >
			<br/><b><fmt:message key="workOrder" /> #</b>: <c:out value="${invoiceForm.order.workOrderNum}" />
			</c:if>

			<c:if test="${workOrder != null}">
			<br/><b><c:out value="${siteConfig['SERVICEABLE_ITEM_TITLE'].value}"/> ID</b>: <c:out value="${workOrder.service.item.itemId}" />
			<br/><b>In SN</b>: <c:out value="${workOrder.inSN}" />
			<br/><b><fmt:message key="bwCount"/></b>: <fmt:formatNumber value="${workOrder.bwCount}" pattern="#,##0"/>
			<br/><b><fmt:message key="colorCount"/></b>: <fmt:formatNumber value="${workOrder.colorCount}" pattern="#,##0"/>
			<br/><b><fmt:message key="problem" /></b>: <c:out value="${workOrder.service.problem}" />
			<c:if test="${workOrder.service.trackNumIn != null and workOrder.service.trackNumIn != ''}">
			<br/><b>Inbound <fmt:message key="trackNum" /></b>: <c:out value="${workOrder.service.trackNumInCarrier}" />
				<c:choose>
				 <c:when test="${workOrder.service.trackNumInCarrier == 'UPS'}">
				  <a href="http://wwwapps.ups.com/WebTracking/processInputRequest?HTMLVersion=5.0&sort_by=status&tracknums_displayed=10&TypeOfInquiryNumber=T&loc=en_US&InquiryNumber1=<c:out value='${workOrder.service.trackNumIn}' />&AgreeToTermsAndConditions=yes"><c:out value="${workOrder.service.trackNumIn}" /></a>
				 </c:when>
				 <c:when test="${workOrder.service.trackNumInCarrier == 'FedEx'}">
				  <a href="http://fedex.com/Tracking?action=track&language=english&cntry_code=us&mps=y&tracknumbers=<c:out value='${workOrder.service.trackNumIn}' />&ascend_header=1"><c:out value="${workOrder.service.trackNumIn}" /></a>
				 </c:when>
				 <c:when test="${workOrder.service.trackNumInCarrier == 'USPS'}">
				  <a href="http://trkcnfrm1.smi.usps.com/PTSInternetWeb/InterLabelInquiry.do?strOrigTrackNum=<c:out value='${workOrder.service.trackNumIn}' />"><c:out value="${workOrder.service.trackNumIn}" /></a>
				 </c:when>
				 <c:otherwise><c:out value="${workOrder.service.trackNumIn}" /></c:otherwise>
				</c:choose>	
			</c:if>			
			<c:if test="${workOrder.service.trackNumOut != null and workOrder.service.trackNumOut != ''}">
			<br/><b>Outbound <fmt:message key="trackNum" /></b>: <c:out value="${workOrder.service.trackNumOutCarrier}" />
				<c:choose>
				 <c:when test="${workOrder.service.trackNumOutCarrier == 'UPS'}">
				  <a href="http://wwwapps.ups.com/WebTracking/processInputRequest?HTMLVersion=5.0&sort_by=status&tracknums_displayed=10&TypeOfInquiryNumber=T&loc=en_US&InquiryNumber1=<c:out value='${workOrder.service.trackNumOut}' />&AgreeToTermsAndConditions=yes"><c:out value="${workOrder.service.trackNumOut}" /></a>
				 </c:when>
				 <c:when test="${workOrder.service.trackNumOutCarrier == 'FedEx'}">
				  <a href="http://fedex.com/Tracking?action=track&language=english&cntry_code=us&mps=y&tracknumbers=<c:out value='${workOrder.service.trackNumOut}' />&ascend_header=1"><c:out value="${workOrder.service.trackNumOut}" /></a>
				 </c:when>
				 <c:when test="${workOrder.service.trackNumOutCarrier == 'USPS'}">
				  <a href="http://trkcnfrm1.smi.usps.com/PTSInternetWeb/InterLabelInquiry.do?strOrigTrackNum=<c:out value='${workOrder.service.trackNumOut}' />"><c:out value="${workOrder.service.trackNumOut}" /></a>
				 </c:when>
				 <c:otherwise><c:out value="${workOrder.service.trackNumOut}" /></c:otherwise>
				</c:choose>	
			</c:if>
			</c:if>

			<sec:authorize ifAnyGranted="ROLE_AEM,ROLE_ADMIN_${prop_site_id}">
			  <c:set var="canEdit" value="true"/>
			</sec:authorize>  
						
			<table border="0" cellpadding="2" cellspacing="1" width="100%" class="invoice">
			  <tr>
			    <c:set var="cols" value="0"/>
			    <c:set var="cols2" value="0"/>
			    <c:if test="${invoiceForm.order.workOrderNum == null or canEdit}" >
			      <c:set var="cols" value="${cols+1}"/>
			      <th width="5%" class="invoice"><fmt:message key="remove" /></th>
			    </c:if>
			    <th width="5%" class="invoice"><fmt:message key="line" />#</th>
			    <th class="invoice"><fmt:message key="productId" /></th>
			    <th class="invoice"><fmt:message key="productSku" /></th>
			    <th class="invoice"><fmt:message key="productName" /></th>
			    <th class="invoice" align="center"><fmt:message key="quantity" /></th>
			    <c:if test="${gSiteConfig['gINVENTORY'] && invoiceForm.order.hasLowInventoryMessage}">
			      <c:set var="cols" value="${cols+1}"/>
			      <th width="10%" class="invoice"><fmt:message key="lowInventory" /></th>
			    </c:if>
			    <c:if test="${invoiceForm.order.hasPacking}">
			      <c:set var="cols" value="${cols+1}"/>
			      <th width="10%" class="invoice"><fmt:message key="packing" /></th>
			    </c:if>
			    <c:if test="${invoiceForm.order.hasContent}">
			      <c:set var="cols" value="${cols+1}"/>
			      <th class="invoice"><fmt:message key="content" /></th>
			    </c:if>
			    <c:set var="cols" value="${cols+1}"/>
			    <th class="invoice"><fmt:message key="location" /></th>
			    <c:if test="${invoiceForm.order.hasCustomShipping}">
	  			  <c:set var="cols" value="${cols+1}"/>
      			  <th class="invoice"><fmt:message key="customShipping" /></th>
    			</c:if>
			    <th align="center" class="invoice"><fmt:message key="productPrice" /><c:if test="${order.hasContent}"> / <c:out value="${siteConfig['CASE_UNIT_TITLE'].value}" /></c:if></th>
			    <c:if test="${gSiteConfig['gSPECIAL_PRICING']}" >
			      <c:set var="cols2" value="${cols2+1}"/>
			      <th class="invoice" width="6px"><fmt:message key="specialPricing" /></th>
			    </c:if>
			    <c:if test="${gSiteConfig['gPURCHASE_ORDER'] && siteConfig['SHOW_COST'].value == 'true' && invoiceForm.order.hasCost}" >
			       <c:set var="cols2" value="${cols2+1}"/>
			       <th class="invoice" width="40px"><fmt:message key="cost" /></th>
			    </c:if>
			    <th class="invoice" width="5%"><fmt:message key="taxable" /></th>
			    <c:if test="${invoiceForm.order.lineItemPromos != null}">
			    		<%-- <th width="15%" class="invoice"><fmt:message key="total" /></th> --%>
			    		 <th class="invoice">LineItem Promo</th>
			    		<c:set var="cols2" value="${cols2+1}"/>
			   </c:if>
			    <th width="15%" class="invoice"><fmt:message key="total" /></th>
			  </tr>
		<c:forEach var="lineItem" items="${invoiceForm.order.lineItems}" varStatus="status">
			<input type="hidden" name="index_${lineItem.lineNumber}"  value="1" id="index_${lineItem.lineNumber}">
			
			<c:if test="${status.first and (invoiceForm.order.workOrderNum == null or canEdit)}">
			  <tr bgcolor="#FFFFCC">
			    <td colspan="5">EXISTING</td>
			    <td style="width:150px;">
			     <table border="0" cellpadding="1" cellspacing="0" style="width:150px;" >
			      <tr>
			       <td style="width:75px">Original</td>
			       <td style="width:75px">Quantity</td>
			      </tr>
			     </table>
			    </td>
			    <c:if test="${cols > 1}">
			      <td colspan="${cols - 1}"></td>
			    </c:if>
			    <td style="width:300px;">
			     <table border="0" cellpadding="1" cellspacing="0" style="width:300px;" >
			      <tr>
			       <td style="width:80px">Original</td>
			       <td style="text-align:center;width:140px">Discount</td>
			       <td style="width:80px">Price</td>
			      </tr>
			     </table>
			    </td>   
			    <td colspan="${3+cols+cols2}"></td>
			  </tr>
			</c:if>
			<tr valign="top">
			  <c:if test="${invoiceForm.order.workOrderNum == null or canEdit}" >
			    <td class="invoice" align="center"><input type="checkbox" name="__remove_${lineItem.lineNumber}" <c:if test="${lineItem.processed > 0}" >disabled='true'</c:if><c:if test="${gSiteConfig['gSPECIAL_PRICING']}" >onChange="toggleSpecialPricing(this, '__special_pricing_${lineItem.lineNumber}')"</c:if>></td>
			  </c:if>
			  <td class="invoice" align="center"><c:out value="${status.count}"/></td>
			  <td class="invoice" align="center"><c:out value="${lineItem.productId}"/></td>
			  <td class="invoice"><c:out value="${lineItem.product.sku}"/></td>
			  <td class="invoice"><input type="text" name="__name_${lineItem.lineNumber}" value="<c:out value="${lineItem.product.name}"/>" maxlength="120">
				<table border="0" cellspacing="1" cellpadding="0" name="optionsTable" id="optionsTable">
					<tr class="invoice_lineitem_attributes">
					  <th colspan="3"><a onClick="addOption('${lineItem.lineNumber}')"><img src="../graphics/add.png" alt="Add Option" title="Add Option" border="0">Add New Option</a></th>
					</tr>
					<div id="productAttribute_${lineItem.lineNumber}">
					  <c:forEach items="${lineItem.productAttributes}" var="productAttribute" varStatus="productAttributeStatus">
						<c:if test="${(! empty productAttribute.optionName) or (!empty productAttribute.valueString) or (! empty productAttribute.optionPriceOriginal)}">
							<tr class="invoice_lineitem_attributes${productAttributeStatus.count%2}">
							<td align="right"></td>
							<td class="optionName" style="white-space: nowrap; padding: 5px;" align="right"><input type="text" id="__optionName_update_${productAttribute.id}" placeholder="name"  name="__optionName_update_${productAttribute.id}" value="<c:out value="${productAttribute.optionName}"/>" maxlength="50"> </td>
						    <td class="optionValue" style="padding: 5px;"><input type="text" id="__optionValueString_update_${productAttribute.id}" placeholder="value"  name="__optionValueString_update_${productAttribute.id}" value="<c:out value="${productAttribute.valueString}"/>" maxlength="50"></td>
						    <c:if test="${(! empty productAttribute.optionPriceOriginal) && productAttribute.optionPriceOriginal != 0}">
								<td class="optionPrice"><fmt:formatNumber value="${productAttribute.optionPriceOriginal}" pattern="#,##0.00" /></td>
								<td class="optionNote" style="padding-left:5px;"><fmt:message key="${productAttribute.optionPriceMessageF}"/></td>
							</c:if>
							</tr>
						</c:if>	
					  </c:forEach>
					  <c:forEach items="${lineItem.newProductAttributes}" var="productAttribute" varStatus="productAttributeStatus">
						<tr class="invoice_lineitem_attributes${productAttributeStatus.count%2}">
							<td align="right"></td>
							<td class="optionName" style="white-space: nowrap; padding: 5px;" align="right"><input type="text" id="__optionName_${productAttribute.id}" placeholder="name" name="__optionName_${productAttribute.id}" value="<c:out value="${productAttribute.optionName}"/>" maxlength="50"> </td>
							<td class="optionValue" style="padding: 5px;"><input type="text" id="__optionValueString_${productAttribute.id}" placeholder="value" name="__optionValueString_${productAttribute.id}" value="<c:out value="${productAttribute.valueString}"/>" maxlength="50"></td>
						</tr>
					  </c:forEach>
				  </div>
				</table>
				<c:if test="${lineItem.subscriptionInterval != null}">
				  <c:set var="intervalType" value="${fn:substring(lineItem.subscriptionInterval, 0, 1)}"/>
				  <c:set var="intervalUnit" value="${fn:substring(lineItem.subscriptionInterval, 1, 2)}"/>
				  <div class="invoice_lineitem_subscription">
					<fmt:message key="subscription"/> <fmt:message key="details"/> : <fmt:message key="delivery"/>
					<c:choose>
					  <c:when test="${intervalUnit == '1'}"><fmt:message key="every_${intervalType}"><fmt:param value="${intervalUnit}"/></fmt:message></c:when>
					  <c:otherwise><fmt:message key="every_${intervalType}s"><fmt:param value="${intervalUnit}"/></fmt:message></c:otherwise>
					</c:choose>
				  </div>
				  <div class="invoice_lineitem_subscription"><c:out value="${lineItem.subscriptionCode}"/></div>
				</c:if>
			  </td>
			  <td class="invoice" align="left"> 
			    <table width="100%">
			      <tr>
			        <td width="50%" align="center">
			          <c:out value="${lineItem.originalQuantity}"/>
				    </td>
			        <td width="50%">
				      <c:choose>
				        <c:when test="${invoiceForm.order.workOrderNum != null and !canEdit}">
				     	  <c:out value="${lineItem.quantity}"/>
				        </c:when>
				        <c:when test="${gSiteConfig['gINVENTORY']}" >
				     	  <input style="text-align: right;" type="text" name="__quantity_<c:out value="${lineItem.lineNumber}"/>" value="${lineItem.quantity}" size="5">
				     	  <c:if test="${lineItem.product.id != null}"><td id="container${lineItem.productId}" style="color:#FF0000;" width="100%"></td></c:if>
				    	</c:when>
				    	<c:otherwise>
				      	  <input style="text-align: right;" type="text" name="__quantity_<c:out value='${lineItem.lineNumber}'/>" value="${lineItem.quantity}" size="6">
				    	</c:otherwise>
				      </c:choose>
				      <c:if test="${lineItem.priceCasePackQty != null}"><tr><td colspan="2"><div class="casePackPriceQty">(<c:out value="${lineItem.priceCasePackQty}"/> per <c:out value="${siteConfig['PRICE_CASE_PACK_UNIT_TITLE'].value}" /> )</div></td></tr></c:if>
			    	</td>
			   	  </tr>
			   	</table>
			  </td>
			  <c:if test="${gSiteConfig['gINVENTORY'] && invoiceForm.order.hasLowInventoryMessage}">
			  <td class="invoice" align="center"><input style="text-align: right;" type="text" name="__low_inventory_message_<c:out value='${lineItem.lineNumber}'/>" value="${lineItem.lowInventoryMessage}" size="10"></td>
			  </c:if>
			  <c:if test="${invoiceForm.order.hasPacking}">
			  <td class="invoice" align="center"><c:out value="${lineItem.product.packing}" /></td>
			  </c:if>
			  <c:if test="${invoiceForm.order.hasContent}">
			  <td class="invoice" align="center">
			  <input style="text-align: right;" type="text" name="__caseContent_<c:out value='${lineItem.lineNumber}'/>" value="${lineItem.product.caseContent}" size="6"></td>
			  </c:if>
			  <td class="invoice" align="center">
			  <input style="text-align: right;" type="text" name="__location_<c:out value='${lineItem.lineNumber}'/>" value="${lineItem.location}" size="6"></td>
			  <c:if test="${invoiceForm.order.hasCustomShipping}"><td class="invoice" style="text-align:right;"><fmt:formatNumber value="${lineItem.customShippingCost}" pattern="#,##0.00"/></td></c:if>
			  <td style="width:300px;" class="invoice" align="right">
			    <table border="0" cellpadding="1" cellspacing="0" style="width:300px;" >
			      <tr>
			       <c:choose>
			      <c:when test="${lineItem.product.restrictPriceChange}">
			      
			      		<td style="width:80px;"><input  style="TEXT-ALIGN: right" type="text" name="__originalPrice_<c:out value="${lineItem.lineNumber}"/>" value="<fmt:formatNumber value="${lineItem.originalPrice}" pattern="##0.000"/>" size="8"  readonly/></td>
			      
			       </c:when>
			       <c:otherwise>
			      		<td style="width:80px;"><input  style="TEXT-ALIGN: right" type="text" name="__originalPrice_<c:out value="${lineItem.lineNumber}"/>" value="<fmt:formatNumber value="${lineItem.originalPrice}" pattern="##0.000"/>" size="8" /></td>
			       </c:otherwise>
			       </c:choose>
			       <td style="width:100px;"><input  style="TEXT-ALIGN: right" type="text" name="__discount_<c:out value="${lineItem.lineNumber}"/>" value="<c:out value="${lineItem.discount}"/>" size="4" />
			       <select style="width:55px;" name="__percent_<c:out value="${lineItem.lineNumber}"/>" >
			  	    <option value="true">%</option><option <c:if test="${!lineItem.percent}">selected</c:if> value="false">Fix</option>
			  	   </select>
			  	   </td>
			       <td style="width:80px;"><input  style="TEXT-ALIGN: right" type="text" disabled name="__unitPrice_<c:out value="${lineItem.lineNumber}"/>" value="${lineItem.unitPrice}" size="8" /></td>
			      </tr>
			    </table>
			  </td>
			  <c:if test="${gSiteConfig['gSPECIAL_PRICING']}" >
			  <td class="invoice" align="center"><input type="checkbox" id="__special_pricing_${lineItem.lineNumber}" name="__special_pricing_${lineItem.lineNumber}" <c:if test="${lineItem.setSpecialPricing}">checked</c:if> /></td>
			  </c:if>
			  <c:if test="${gSiteConfig['gPURCHASE_ORDER'] && siteConfig['SHOW_COST'].value == 'true' && invoiceForm.order.hasCost}" >
			  <td class="invoice" style="TEXT-ALIGN: right"><c:out value="${lineItem.unitCost}" /></td>
			  </c:if>
			  <td class="invoice testing" align="center"><input type="checkbox" name="__taxable_${lineItem.lineNumber}" <c:if test="${lineItem.product.taxable}">checked</c:if>></td>
			  
			  <td class="invoice" align="right">			  			
				  	<c:if test = "${lineItem.promo != null && lineItem.promo.title != null}">			  			
				  			Promo: ${lineItem.promo.title}
				  			<br>
				  			Amount: <span style="color:red;"><fmt:formatNumber value="${lineItem.promoAmount}" pattern="#,##0.00"/>	</span>	
			  		</c:if>  			  
			  </td>
			  
			  <td class="invoice" align="right"><fmt:formatNumber value="${lineItem.totalPrice}" pattern="#,##0.00"/></td>
			</tr>
			<c:if test="${gSiteConfig['gRMA']}">
			<tr bgcolor="#FFFFFF">
			  <td colspan="${8+cols+cols2}">
			    <table cellspacing="3">
			      <tr>
			        <td valign="top"><a onClick="addSerialNum(${lineItem.lineNumber}, '')"><img src="../graphics/add.png" width="16" border="0"></a></td>
			        <td valign="top">S/N:</td>
			        <td><div id="__serialNums_${lineItem.lineNumber}">
				      <c:forEach items="${lineItem.serialNums}" var="serialNum">
					    <input type="text" name="__serialNum_${lineItem.lineNumber}" value="<c:out value="${serialNum}"/>" size="10" maxlength="50"
					    	<c:if test="${serialNumsMap[fn:toLowerCase(serialNum)] == 'duplicate'}">style="background-color:#EE0000;"</c:if>>,
					  </c:forEach></div></td>
			      </tr>
			    </table>
			  </td>
			</tr>			
			</c:if>
			</c:forEach>
			<c:forEach var="lineItem" items="${invoiceForm.order.insertedLineItems}" varStatus="status">*
			<input type="hidden" name="index_new_${status.index}"  value="1" id="index_new_${status.index}">
			
			<c:if test="${status.first and (invoiceForm.order.workOrderNum == null or canEdit)}">
			  <tr bgcolor="#FFFFCC">
			    <td colspan="${8+cols+cols2}">NEW</td>
			  </tr>
			</c:if>
			<tr valign="top">
			  <c:if test="${invoiceForm.order.workOrderNum == null or canEdit}" >
			    <td class="invoice" align="center"><input type="checkbox" name="__remove_new_${status.index}" <c:if test="${gSiteConfig['gSPECIAL_PRICING']}" >onChange="toggleSpecialPricing(this, '__special_pricing_new_${status.index}')"</c:if>></td>
			  </c:if>
			  <td class="invoice" align="center"><c:out value="${invoiceForm.order.size + status.count}"/></td>1
			  <td class="invoice" align="center"><c:out value="${lineItem.productId}"/></td>2
			  <td class="invoice"><c:out value="${lineItem.product.sku}"/></td>3
			  <td class="invoice">
			  	<input type="text" name="__name_new_${status.index}" value="<c:out value="${lineItem.product.name}"/>" maxlength="120">
			  	
            	<table border="0" cellspacing="1" cellpadding="0" name="optionsTable" id="optionsTable">
				  <tr class="invoice_lineitem_attributes">
				    <th colspan="3"><a onClick="addOptionNew('${status.index}')"><img src="../graphics/add.png" alt="Add Option" title="Add Option" border="0">Add New Option</a></th>
				  </tr>
				  <div id="productAttribute_new_${status.index}">
					  <c:forEach items="${lineItem.newProductAttributes}" var="productAttribute" varStatus="productAttributeStatus">
						<tr class="invoice_lineitem_attributes${productAttributeStatus.count%2}">
							<td align="right"></td>
							<td class="optionName" style="white-space: nowrap" align="right"><input type="text" id="__optionName_new_${productAttribute.id}" placeholder="name"  name="__optionName_new_${productAttribute.id}" value="<c:out value="${productAttribute.optionName}"/>" maxlength="50"> </td>
							<td class="optionValue"><input type="text" id="__optionValueString_new_${productAttribute.id}" placeholder="value"  name="__optionValueString_new_${productAttribute.id}" value="<c:out value="${productAttribute.valueString}"/>" maxlength="50"></td>
						</tr>
					  </c:forEach>
				  </div>
			    </table>
			  	
			  </td>
			  <td class="invoice" align="left"> 
			  <table width="100%"><tr>
			  <c:choose>
			    <c:when test="${gSiteConfig['gINVENTORY']}" >
			      <td><input style="text-align: right;" type="text"  name="__quantity_new_${status.index}" value="${lineItem.quantity}" <c:if test="${lineItem.product.id != null}">id="__quantity_${lineItem.productId}" onchange="javascript:ajaxRequest(${lineItem.productId},'${lineItem.product.sku}');" </c:if> size="5"></td>
			      <c:if test="${lineItem.product.id != null}"><td style="color:#FF0000;width:100px" id="container${lineItem.productId}" width="100%"></td></c:if>
			    </c:when>
			    <c:otherwise>
			      <td><input style="text-align: right;" type="text" name="__quantity_new_${lineItem.quantity}" value="${lineItem.quantity}" size="5"></td>
			    </c:otherwise>
			  </c:choose>
			  </tr></table>
			  </td>
			  <c:if test="${gSiteConfig['gINVENTORY'] && invoiceForm.order.hasLowInventoryMessage}">
			  <td class="invoice" align="center"><input style="text-align: right;" type="text" name="__low_inventory_message_new_<c:out value='${status.index}'/>" value="${lineItem.lowInventoryMessage}" size="10"></td>
			  </c:if>
			  <c:if test="${invoiceForm.order.hasPacking}">
			  <td class="invoice" align="center"><c:out value="${lineItem.product.packing}" /></td>
			  </c:if>
			  <c:if test="${invoiceForm.order.hasContent}">
			  <td class="invoice" align="center">
			  <input style="text-align: right;" type="text" name="__caseContent_<c:out value='${status.index}'/>" value="${lineItem.product.caseContent}" size="6"></td>
			  </c:if>
			  <td class="invoice" align="center">
			  <input style="text-align: right;" type="text" name="__location_new<c:out value='${status.index}'/>" value="${lineItem.location}" size="6"></td>
			  <c:if test="${invoiceForm.order.hasCustomShipping}"><td class="invoice" style="text-align:right;"><fmt:formatNumber value="${lineItem.customShippingCost}" pattern="#,##0.00"/></td></c:if>
			  <td style="width:300px;" class="invoice" align="right">
			    <table border="0" cellpadding="1" cellspacing="0" style="width:300px;" >
			      <tr>
			      <c:choose>
			      <c:when test="${lineItem.product.restrictPriceChange}">
			      		 <td style="width:80px;"><input  style="TEXT-ALIGN: right" type="text"  name="__originalPrice_new_<c:out value="${status.index}" />" value="<fmt:formatNumber value="${lineItem.originalPrice}" pattern="##0.000"/>" size="8"  readonly/></td>      
			      </c:when>
			      <c:otherwise>
			      		<td style="width:80px;"><input  style="TEXT-ALIGN: right" type="text"  name="__originalPrice_new_<c:out value="${status.index}"/>" value="<fmt:formatNumber value="${lineItem.originalPrice}" pattern="##0.000"/>" size="8" /></td>		      
			      </c:otherwise>
			      </c:choose>
			      
			       <td style="width:100px;"><input  style="TEXT-ALIGN: right" type="text" name="__discount_new_<c:out value="${status.index}"/>" value="<c:out value="${lineItem.discount}"/>" size="4" />
			       <select style="width:55px;" name="__percent_new_<c:out value="${status.index}"/>" >
			  	    <option value="true">%</option><option <c:if test="${!lineItem.percent}">selected</c:if> value="false">Fix</option>
			  	   </select>
			  	   </td>
			       <td style="width:80px;"><input  style="TEXT-ALIGN: right" type="text" disabled name="__unitPrice_new_<c:out value="${status.index}"/>" value="${lineItem.unitPrice}" size="8" /></td>
			      </tr>
			    </table>
			  </td>
			  <c:if test="${gSiteConfig['gSPECIAL_PRICING']}" >
			  <td class="invoice" align="center"><input type="checkbox" id="__special_pricing_new_${status.index}" name="__special_pricing_new_${status.index}" <c:if test="${lineItem.setSpecialPricing}">checked</c:if> /></td>
			  </c:if>
			  <c:if test="${gSiteConfig['gPURCHASE_ORDER'] && siteConfig['SHOW_COST'].value == 'true'  && invoiceForm.order.hasCost}" >
			  <td class="invoice"><c:out value="${lineItem.unitCost}" /></td>
			  </c:if>
			  
			  <td class="invoice" align="center"><input type="checkbox" name="__taxable_new_${status.index}" <c:if test="${lineItem.product.taxable}">checked</c:if>></td>		  			  			  
			  	<td class="invoice" align="right">			  			
				  	<c:if test = "${lineItem.promo != null && lineItem.promo.title != null}">			  			
				  		Promo: ${lineItem.promo.title}
				  			<br>
				  		Amount: <span style="color:red;"> <fmt:formatNumber value="${lineItem.promoAmount}" pattern="#,##0.00"/>	</span>	
			  		</c:if>  			  
			  	</td>			  
			  <td class="invoice" align="right"><fmt:formatNumber value="${lineItem.totalPrice}" pattern="#,##0.00"/></td>
			</tr>
			<c:if test="${gSiteConfig['gRMA']}">
			<tr bgcolor="#FFFFFF">
			  <td colspan="${8+cols+cols2}">
			    <table cellspacing="3">
			      <tr>
			        <td valign="top"><a onClick="addSerialNum(${status.index}, 'new_')"><img src="../graphics/add.png" width="16" border="0"></a></td>
			        <td valign="top">S/N:</td>
			        <td><div id="__serialNums_new_${status.index}">
				      <c:forEach items="${lineItem.serialNums}" var="serialNum">
					    <input type="text" name="__serialNum_new_${status.index}" value="<c:out value="${serialNum}"/>" size="10" maxlength="50"
					    	<c:if test="${serialNumsMap[fn:toLowerCase(serialNum)] == 'duplicate'}">style="background-color:#EE0000;"</c:if>>,
					  </c:forEach></div></td>
			      </tr>
			    </table>
			  </td>
			</tr>			
			</c:if>			
			</c:forEach>
			  <tr bgcolor="#BBBBBB">
			    <td colspan="${8+cols+cols2}">&nbsp;</td>    
			  </tr>
			  <tr>
			    <td class="invoice" colspan="${7+cols+cols2}" align="right"><fmt:message key="subTotal" />:</td>
			    <td class="invoice" align="right"><fmt:formatNumber value="${invoiceForm.order.subTotal}" pattern="#,##0.00"/></td>
			  </tr>
		  	  <c:if test="${invoiceForm.order.budgetEarnedCredits > 0.00 }">
			  	<tr class="customerEarnedCreditsBoxId">
			  		<td class="discount" colspan="${7+cols+cols2}" align="right">Earned Credits: </td>
			  		<td class="discount" align="right"><fmt:formatNumber value="${invoiceForm.order.budgetEarnedCredits}" pattern="-#,##0.00"/></td>
			  	</tr>
		      </c:if>
		      
     		  <c:if test="${invoiceForm.order.promo.discountType eq 'order' }">
			  <tr>
			    <td class="discount" colspan="${7+cols+cols2}" align="right">
			    Discount For Promo Code <c:out value="${invoiceForm.order.promo.title}" />
			    	(<c:choose>
			          <c:when test="${invoiceForm.order.promo.percent}">
			            <fmt:formatNumber value="${invoiceForm.order.promo.discount}" pattern="#,##0.00"/>%
			          </c:when>
			          <c:otherwise>
			            $<fmt:formatNumber value="${invoiceForm.order.promo.discount}" pattern="#,##0.00"/>
			          </c:otherwise>
			      	</c:choose>):
			    </td>
			    <td class="discount" align="right">
			      <c:choose>
			          <c:when test="${invoiceForm.order.promo.percent}">
			            (<fmt:formatNumber value="${invoiceForm.order.subTotal * ( invoiceForm.order.promo.discount / 100.00 )}" pattern="#,##0.00"/>)
			          </c:when>
			          <c:when test="${(!invoiceForm.order.promo.percent) and (invoiceForm.order.promo.discount > invoiceForm.order.subTotal)}">
			        	(<fmt:formatNumber value="${invoiceForm.order.subTotal}" pattern="#,##0.00"/>)
			          </c:when>
			          <c:otherwise>
			            (<fmt:formatNumber value="${invoiceForm.order.promo.discount}" pattern="#,##0.00"/>)
			          </c:otherwise>
			      	</c:choose>	    
			    </td>
			  </tr> 
			  </c:if> 

		<%-- 	  <c:if test="${invoiceForm.order.lineItems != null and fn:length(invoiceForm.order.lineItems) gt 0}">
			   
    			<c:forEach items="${invoiceForm.order.lineItems}" var="item" varStatus="status">
    			  <tr class ="AllamChaeib">
			     	<td> <c:out value="${item.promoAmount}" />   </td>
			     </tr> 
    			</c:forEach>  
  			  </c:if> --%>
                
                
               <!--  2020-11-10 viatrading want promo code with product appplied on lineItem level -->
<%-- 			  <c:if test="${invoiceForm.order.lineItemPromos != null and fn:length(invoiceForm.order.lineItemPromos) gt 0}">
			   
    			<c:forEach items="${invoiceForm.order.lineItemPromos}" var="itemPromo" varStatus="status">
    			<tr>
    			
    			
      			<td class="discount" colspan="${7+cols+cols2}" align="right">
      			<c:choose>
        			<c:when test="${itemPromo.value == 0}"><fmt:message key="promoCode" /></c:when>
        			<c:otherwise><fmt:message key="discountForPromoCode" /></c:otherwise>
      			</c:choose>
      			<c:out value="${itemPromo.key}" />
      			</td>
      			<td class="discount" align="right">
      			<c:choose>
        			<c:when test="${itemPromo.value == 0}">&nbsp;</c:when>
       			 <c:otherwise>($<fmt:formatNumber value="${itemPromo.value}" pattern="#,##0.00"/>)</c:otherwise>
      			</c:choose>
      			</td>
    			</tr>
    			</c:forEach>  
  			  </c:if> --%>
			  
			  <tr>
			    <td class="invoice" colspan="${7+cols+cols2}" align="right">
			      Tax: 
			      <spring:bind path="invoiceForm.order.taxRate">
			       <input style="TEXT-ALIGN: right" type="text" name="${status.expression}" value="${status.value}">
			      </spring:bind> 
			      %
			    </td>
			    <td class="invoice" align="right"><fmt:formatNumber value="${invoiceForm.order.tax}" pattern="#,##0.00"/></td>
			  </tr>
			    <tr>
			    <td class="invoice" colspan="${7+cols+cols2}" align="right">
			    Shipping & Handling (<spring:bind path="invoiceForm.order.shippingMethod">
			              <input type="text" name="${status.expression}" value="${status.value}" size="15" id="shippingTitle" style="width:150px;">
			             </spring:bind>):
			    <select name="customShippingRateSelected" id="customShippingRateSelected" onChange="chooseCustomShippingRate(this)" style="width:250px;">
			  	  <option value="">Choose existing Custom Shipping</option> 
			  	  <c:forEach items="${customShippingRateList}" var="customShippingRate"	varStatus="status">
			  	    <option value="${customShippingRate.title}"><c:out value="${customShippingRate.title}" /></option>
			      </c:forEach>
			  	</select>
			  	<c:forEach items="${customShippingRateList}" var="customShippingRate"	varStatus="status">
			  	  <input type="hidden" id="price_${customShippingRate.title}" value="${customShippingRate.price}" />
			    </c:forEach> <input type="hidden" id="price_" value="" />        
			    </td>
			    <td class="invoice" align="right">
			      <spring:bind path="invoiceForm.order.shippingCost">
			       <input style="TEXT-ALIGN: right"  type="text" name="${status.expression}" value="${status.value}" id="shippingCost">
			      </spring:bind> 
			    </td>
			  </tr>
			  <c:if test="${invoiceForm.order.promo.discountType eq 'shipping'}">
			  <tr>
			    <td class="discount" colspan="${7+cols+cols2}" align="right">
			    Discount For Promo Code <c:out value="${invoiceForm.order.promo.title}" />
			    	(<c:choose>
			          <c:when test="${invoiceForm.order.promo.percent}">
			            <fmt:formatNumber value="${invoiceForm.order.promo.discount}" pattern="#,##0.00"/>%
			          </c:when>
			          <c:otherwise>
			            $<fmt:formatNumber value="${invoiceForm.order.promo.discount}" pattern="#,##0.00"/>
			          </c:otherwise>
			      	</c:choose>):
			    </td>
			    <td class="discount" align="right">
			      <c:choose>
			          <c:when test="${invoiceForm.order.promo.percent}">
			            (<fmt:formatNumber value="${invoiceForm.order.shippingCost * ( invoiceForm.order.promo.discount / 100.00 )}" pattern="#,##0.00"/>)
			          </c:when>
			          <c:when test="${(!invoiceForm.order.promo.percent) and (invoiceForm.order.promo.discount > invoiceForm.order.shippingCost)}">
			        	(<fmt:formatNumber value="${invoiceForm.order.shippingCost}" pattern="#,##0.00"/>)
			          </c:when>
			          <c:otherwise>
			            (<fmt:formatNumber value="${invoiceForm.order.promo.discount}" pattern="#,##0.00"/>)
			          </c:otherwise>
			      	</c:choose>	    
			    </td>
			  </tr> 
			  </c:if>
			  <c:if test="${invoiceForm.order.hasCustomShipping or invoiceForm.order.customShippingCost != null}">
			  <tr>
			  <td class="invoice" colspan="${7+cols+cols2}" align="right">
			    Custom Shipping (<form:input path="order.customShippingTitle"/> )
			  </td>
	  		  <td class="invoice" align="right"><form:input path="order.customShippingCost" cssStyle="TEXT-ALIGN: right"/></td>
	  		  </tr>
    		  </c:if>
    		  <c:choose>
			  <c:when test="${gSiteConfig['gBUDGET'] and invoiceForm.order.requestForCredit != null and invoiceForm.order.creditUsed == null and customer.creditAllowed != null and customer.creditAllowed > 0}">
		    	<tr>
			    	<td class="discount" colspan="${7+cols}" align="right">
		    		<fmt:message key="creditRequestAmount">
						<fmt:param>${customer.creditAllowed}</fmt:param>
						<fmt:param><fmt:formatNumber value="${subTotalByPlan}" pattern="#,##0.00"/></fmt:param>
						<fmt:param><fmt:formatNumber value="${(customer.creditAllowed / 100) * subTotalByPlan}" pattern="#,##0.00"/></fmt:param>
			    	</fmt:message>
			    	<br/> 
			    	<fmt:message key="creditApprovalMessage" />
			    	</td>
			    	<c:choose>
		  				<c:when test="${invoiceForm.order.requestForCredit > 0 }">
			    			<td class="discount" align="right"><fmt:formatNumber value="${invoiceForm.order.requestForCredit}" pattern="-#,##0.00"/></td>
		  				</c:when>
		  				<c:otherwise>
		  					<td class="discount" align="right">0.00</td>
		  				</c:otherwise>
		  			</c:choose>
			    </tr>
			  </c:when>
			  <c:when test="${(gSiteConfig['gGIFTCARD'] or gSiteConfig['gVIRTUAL_BANK_ACCOUNT'] or gSiteConfig['gBUDGET']) and invoiceForm.order.creditUsed != null and invoiceForm.order.creditUsed > 0}">
			  	 <tr>
				    <td class="discount" colspan="${7+cols+cols2}" align="right"><fmt:message key="credit" />:</td>
				    <td class="discount" align="right"><fmt:formatNumber value="${invoiceForm.order.creditUsed}" pattern="-#,##0.00"/></td>
				  </tr>
			  </c:when>
			  </c:choose>
			  <tr>
			    <td class="invoice" colspan="${7+cols+cols2}" align="right">
			      CC Fee: <form:input path="order.ccFeeRate" cssStyle="TEXT-ALIGN: right"/>
			      <form:select path="order.ccFeePercent" cssStyle="width:45px;">
			        <form:option value="true">%</form:option>
			        <form:option value="false">Fix</form:option>
			      </form:select>
			    </td>
			    <td class="invoice" align="right"><fmt:formatNumber value="${invoiceForm.order.ccFee}" pattern="#,##0.00"/></td>
			  </tr>
			  <c:if test="${invoiceForm.order.orderId == null and siteConfig['BUY_SAFE_URL'].value != ''}">
			  <tr>
			    <td class="invoice" colspan="${7+cols+cols2}" align="right">
			    <fmt:message key="buySafeBondGuarntee" />: 
			    <select name="wantsBond" onchange="buySAFEOnClick(this.value);" style="width:60px;">
			      <option value="true" <c:if test="${invoiceForm.order.wantsBond == 'true'}">selected="selected" </c:if>>Yes</option>
			      <option value="false" <c:if test="${invoiceForm.order.wantsBond == 'false'}">selected="selected"</c:if>>No</option>
			    </select>
			   	</td>
			    <td class="invoice" align="right"><fmt:formatNumber value="${invoiceForm.order.bondCost}" pattern="#,##0.00"/></td>
			  </tr>
			  </c:if>			  
			  <c:if test="${invoiceForm.order.bondCost != null and invoiceForm.order.orderId != null}">
			  <tr>
			    <td class="invoice" align="right" colspan="5">
			    <c:if test="${!fn:contains(invoiceForm.order.status,'x')}">
			    	<a style="width: 100px; float: right; text-align: center; background-color:#FF9999; color:WHITE;" href="invoice.jhtm?order=${invoiceForm.order.orderId}&__cancelBuySafe=true"><fmt:message key="cancel" /></a>
			    </c:if>
			    </td>
			    <td class="invoice" colspan="${2+cols+cols2}" align="right"><fmt:message key="buySafeBondGuarntee" />:</td>
			    <td class="invoice" align="right"><fmt:formatNumber value="${invoiceForm.order.bondCost}" pattern="#,##0.00"/></td>
			  </tr>
			  </c:if>			  
			  <tr>
			    <td class="invoice" colspan="${7+cols+cols2}" align="right"><fmt:message key="grandTotal" />:</td>
			    <td align="right" bgcolor="#F8FF27"><fmt:formatNumber value="${invoiceForm.order.grandTotal}" pattern="#,##0.00"/></td>
			  </tr>
			   <c:if test="${gSiteConfig['gPAYMENTS']}">
			  <tr>
			    <td class="invoice" colspan="${7+cols+cols2}" align="right"><fmt:message key="balance" />:</td>
			    <td class="invoice" align="right"><fmt:formatNumber value="${invoiceForm.order.grandTotal - invoiceForm.order.amountPaid}" pattern="#,##0.00"/></td>
			  </tr>
			  </c:if>
			</table>
			<c:if test="${partnerList != null and fn:length(partnerList) gt 0}">
			<div class="partnersBox">
					<b><fmt:message key="partnersList"/></b>
			    	<c:forEach items="${partnerList}" var="partner">
			    		<div class="wrapper">
			    		<c:out value="${partner.partnerName}"/>
			    		<fmt:message key="${siteConfig['CURRENCY'].value}" />
			    		<input type="text" name="__partner_amount_${partner.partnerId}" value="<fmt:formatNumber value="${partner.amount}" pattern="#,##0.00"/>" />
			    		</div>
			    	</c:forEach>
			    	<div> <fmt:message key="total"/>: <fmt:message key="${siteConfig['CURRENCY'].value}" /><fmt:formatNumber value="${invoiceForm.order.budgetEarnedCredits}" pattern="#,##0.00"/></div>
			</div>	
			</c:if>
			<c:if test="${!fn:contains(siteConfig['SITE_URL'].value,'framestoredirect')}">
			<table>
			<tr>
			  <td colspan="3"><b>Promo Discount:</b></td>
			</tr>
			<tr>
			  <td>&nbsp;</td>
			  <td><fmt:message key="code" />:</td>
			  <td>
			    <form:input path="order.promo.title" cssStyle="TEXT-ALIGN: left" disabled="${!gSiteConfig['gSALES_PROMOTIONS']}" id="promoTitle"/>
			    <form:errors path="order.promo.title" cssClass="error" />  
			  	<select name="promoSelected" id="promoSelected" onChange="choosePromo(this)" <c:if test="${!gSiteConfig['gSALES_PROMOTIONS']}" >disabled="disabled"</c:if>>
			  	  <option value="">Choose existing Promo Code</option> 
			  	  <c:forEach items="${promoList}" var="promo"	varStatus="status">
			  	    <option value="${promo.title}"><c:out value="${promo.title}" /></option>
			      </c:forEach>
			  	</select>
			  	
			  	&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
				&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
				&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
				&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
				&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
				&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
				
			    <td>
			    <img class="toolTipImg" title="Populate if this order will not be paid immediately (for Walk-In Orders) or within 24 hours (for all other orders)" src="../graphics/question.gif" />&nbsp;<fmt:message key="agreedPaymentDate" />:</td>
			    <td><form:input path="order.agreedPaymentDate" size="18"  />
			    <img id="time_start_trigger3" class="calendarImage" src="../graphics/calendarIcon.jpg" alt="Pick a date" />
			    <form:errors path="order.agreedPaymentDate" type ="hidden" cssClass="error">
			       <span style=color:#E00000><b>!Wrong date format</b></span>
			       <img class="toolTipImg" title="Format is 01/01/2017[01:00 PM]" src="../graphics/question.gif" />
               </form:errors>
			    
			    </td>
 	
			    <c:forEach items="${promoList}" var="promo"	varStatus="status">
			  	  <input type="hidden" id="discount_${promo.title}" value="${promo.discount}" />
			  	  <input type="hidden" id="discount_type_${promo.title}" value="${promo.discountType}" />
			  	  <input type="hidden" id="percent_${promo.title}" value="${promo.percent}" />
			    </c:forEach>
			    <input type="hidden" id="discount_" value="" />
			    <input type="hidden" id="discount_type_" value="" />
			  	<input type="hidden" id="percent_" value="" />
			  </td>
			</tr>
			<tr>
			  <td>&nbsp;</td>
			  <td><fmt:message key="discount" />:</td>
			  <td><div>
			      <form:input path="order.promo.discount" cssStyle="TEXT-ALIGN: left" disabled="${!gSiteConfig['gSALES_PROMOTIONS']}" id="promoDiscount"/>
			      <form:select path="order.promo.percent" disabled="${!gSiteConfig['gSALES_PROMOTIONS']}" id="promoPercent">
			        <form:option value="false"><fmt:message key="FIXED" />$</form:option>
			  	    <form:option value="true">%</form:option>
			      </form:select>
			      <form:errors path="order.promo.percent" cssClass="error" />
			      <form:errors path="order.promo.discount" cssClass="error" />    
				  </div>
			  </td>
			  
			  <td><img class="toolTipImg" title="Populate if this is a Walk-In Order which will not be picked up immediately " src="../graphics/question.gif" />&nbsp;<fmt:message key="agreedPickUpDate" />:</td>
			    <td><form:input path="order.agreedPickUpDate" size="18"  />
			    <img id="time_start_trigger4" class="calendarImage" src="../graphics/calendarIcon.jpg" alt="Pick a date" />
			    <form:errors path="order.agreedPickUpDate" type ="hidden" cssClass="error">
			        <span style=color:#E00000><b>!Wrong date format</b></span>
			    	<img class="toolTipImg" title="Format is 01/01/2017[01:00 PM]" src="../graphics/question.gif" />
			    </form:errors>
                </td>
			  
			</tr>
			<c:if test="${gSiteConfig['gSALES_PROMOTIONS_ADVANCED']}">
			<tr>
			  <td>&nbsp;</td>
			  <td><fmt:message key="discount" /> <fmt:message key="type" />:</td>
			  <td><div>
			      <form:select path="order.promo.discountType" disabled="${!gSiteConfig['gSALES_PROMOTIONS']}" id="promoDiscountType">
			        <form:option value="order"><fmt:message key="order" /></form:option>
			  	    <form:option value="shipping"><fmt:message key="shipping" /></form:option>
			  	      <form:option value="product"><fmt:message key="product" /></form:option>
			      </form:select>
			      <form:errors path="order.promo.discountType" cssClass="error" />  
				  </div>
			  </td>
			</tr>
			</c:if>
			</table>
			</c:if>
			<c:if test="${orderName != 'quotes'}">
				<c:if test="${creditMessage != null}">
					<font color="red"><fmt:message key="creditMessage" /></font>
				</c:if> <br/>
				<b><fmt:message key="paymentMethod" />:</b> 
				<c:choose>
				  <c:when test="${not invoiceForm.newInvoice}">
				    <%-- <c:out value="${invoiceForm.order.paymentMethod}" /> --%>				    
					    <form:select path="order.paymentMethod" onchange="togglePayment(this)">
						    	<form:option value="" label="Please Select"/>
						     	<form:option value="Credit Card" label="Credit Card"/>
						     	<form:option value="PayPal" label="PayPal"/>
						    <c:forEach items="${customPayments}" var="paymentMethod">
								<c:if test="${paymentMethod.enabled}">					
							    	<option value="${paymentMethod.title}" <c:if test="${(customer.payment == paymentMethod.title) or (invoiceForm.order.paymentMethod == paymentMethod.title)}">selected="selected"</c:if>><c:out value="${paymentMethod.title}" /></option>
							    </c:if>
						    </c:forEach>			    
					    	<c:if test="${(gSiteConfig['gVIRTUAL_BANK_ACCOUNT'] or customer.credit > 0) and invoiceForm.order.grandTotal > 0 and customer.credit >= invoiceForm.order.grandTotal}">
					    		<form:option value="vba"><fmt:message key="vba"/></form:option>
							</c:if>
					  </form:select>				    
				  </c:when>
				  <c:when test="${invoiceForm.order.workOrderNum != null}">
				    <c:out value="${invoiceForm.order.paymentMethod}" />   
				  </c:when>
				  <c:otherwise>
				  <form:select path="order.paymentMethod">
				    <form:option value="" label="Please Select"/>
				    <c:forEach items="${customPayments}" var="paymentMethod">
					<c:if test="${paymentMethod.enabled}">					
				    	<option value="${paymentMethod.title}" <c:if test="${customer.payment == paymentMethod.title}">selected="selected"</c:if>><c:out value="${paymentMethod.title}" /></option>
				    </c:if>
				    </c:forEach>			    
			    	<c:if test="${(gSiteConfig['gVIRTUAL_BANK_ACCOUNT'] or customer.credit > 0) and invoiceForm.order.grandTotal > 0 and customer.credit >= invoiceForm.order.grandTotal}">
			    		<form:option value="vba"><fmt:message key="vba"/></form:option>
					</c:if>
				  </form:select>			  		    
			      <c:if test="${(gSiteConfig['gVIRTUAL_BANK_ACCOUNT'] or customer.credit > 0) and invoiceForm.order.grandTotal > 0 and customer.credit >= invoiceForm.order.grandTotal}">
			      <fmt:message key="creditAmountAvailable" />: 
			      	 <fmt:message key="${siteConfig['CURRENCY'].value}"/> <fmt:formatNumber value="${customer.credit}" pattern="#,##0.00"/>	
				  </c:if>
				  <c:if test="${gSiteConfig['gVIRTUAL_BANK_ACCOUNT'] or customer.credit > 0}">			
					<c:if test="${invoiceForm.order.grandTotal > 0 and customer.credit < invoiceForm.order.grandTotal}">
	      			  <c:if test="${gSiteConfig['gGIFTCARD'] or siteConfig['POINT_TO_CREDIT'].value == 'true' or gSiteConfig['gVIRTUAL_BANK_ACCOUNT']}">
	      			    <form:checkbox path="applyUserPoint" value="true"  />
	      			  </c:if>
				      <fmt:message key="applyOfMyCreditToThisOrder">
					      <c:choose>
					  		<c:when test="${siteConfig['CURRENCY'].value == 'EUR'}">
					  			<fmt:param value="&#8364;" /> 
					  		</c:when>
					  		<c:when test="${siteConfig['CURRENCY'].value == 'GBP'}">
					  			<fmt:param value="&#163;"/> 
					  		</c:when>
					  		<c:when test="${siteConfig['CURRENCY'].value == 'JPY'}">
					  			<fmt:param value="&#165;"/> 
					  		</c:when>
					  		<c:when test="${siteConfig['CURRENCY'].value == 'CAD'}">
					  			<fmt:param value="CAD&#36;"/> 
					  		</c:when>
					  		<c:otherwise>
					  		    <fmt:param value="&#36;" />
					  		</c:otherwise>
						  </c:choose><fmt:param value="${customer.credit}"/>
					  </fmt:message>
					</c:if>	
				  </c:if>
				  </c:otherwise>
				</c:choose>
				<c:if test="${invoiceForm.order.creditCard != null}">
				<c:choose>
				<c:when test="${invoiceForm.order.creditCard.transId != null or invoiceForm.order.status == 's' or fn:contains(invoiceForm.order.status,'x')}">
 					  <c:set value="false" var="showCreditCard"/>
 				  	  <c:set value="${invoiceForm.order.creditCard}" var="creditCard"/>
 				  	  <%@ include file="/WEB-INF/jsp/admin/orders/common/creditcard.jsp" %>
  				</c:when>
				<c:otherwise>
				 <table border="0" cellpadding="0" cellspacing="0">
				  <tr>
				    <td><fmt:message key="cardType" />:</td>
				    <td>
				      <spring:bind path="invoiceForm.order.creditCard.type">
				      <input type="text" name="${status.expression}" value="${status.value}">
				      </spring:bind>
				    </td>
				  </tr>
				  <tr>
				    <td><fmt:message key="cardNumber" />:</td>
				    <td>
				      <spring:bind path="invoiceForm.order.creditCard.number">
				      <input type="text" name="${status.expression}" value="${status.value}" size="20">
				      </spring:bind>
				    </td>
				  </tr>
				   <tr>
				     <td class="formName">Exp Date (MM/YY):</td>
				     <td>
					   <spring:bind path="invoiceForm.order.creditCard.expireMonth">
					   <select name="<c:out value="${status.expression}"/>">
					    <c:set var="monthFound" value="false"/>
					    <c:forTokens items="01,02,03,04,05,06,07,08,09,10,11,12" delims="," var="ccExpireMonth">
				          <option value="${ccExpireMonth}" <c:if test="${status.value==ccExpireMonth}">selected<c:set var="monthFound" value="true"/></c:if>><c:out value="${ccExpireMonth}"/></option>
				        </c:forTokens>
				        <c:if test="${monthFound != true}">
				         <option value="${status.value}" selected><c:out value="${status.value}"/></option>
				        </c:if>
					   </select>
				       </spring:bind> 
				       <spring:bind path="invoiceForm.order.creditCard.expireYear">
					   <select name="<c:out value="${status.expression}"/>">
					    <c:set var="yearFound" value="false"/>
						<c:forEach items="${expireYears}" var="ccExpireYear">
	         			 <option value="${ccExpireYear}" <c:if test="${status.value==ccExpireYear}">selected<c:set var="yearFound" value="true"/></c:if>><c:out value="${ccExpireYear}"/></option>
		    			</c:forEach>
		   			    <c:if test="${yearFound != true}">
				         <option value="${status.value}" selected><c:out value="${status.value}"/></option>
				        </c:if>
					   </select>
				       </spring:bind>  
					 </td>
				   </tr>  
				  
				  <tr>
				    <td><fmt:message key="cardVerificationCode" />:</td>
				    <td>
				      <spring:bind path="invoiceForm.order.creditCard.cardCode">
				      <input type="text" name="${status.expression}" value="${status.value}" maxlength="4" size="4">
				      </spring:bind>
				    </td>
				  </tr>
				  </table>
				</c:otherwise>
				</c:choose>
				</c:if>
			</c:if>
			
			<c:if test="${fn:toLowerCase(invoiceForm.order.paymentMethod) == 'credit card' and siteConfig['CC_BILLING_ADDRESS'].value == 'true'}">
			<div class="ccBillingAddress">
				<lable><b><fmt:message key="ccBillingAddres"/>:</b></lable>
				<p><form:textarea path="order.ccBillingAddress" cssClass="textArea300x400"/></p>
			</div>
			</c:if>

			<c:if test="${invoiceForm.newInvoice and gSiteConfig['gPAYMENTS']}">
			<table border="0">
			<tr>
			  <td colspan="6"><b><fmt:message key="payments" />:</b></td>
			</tr>
			<tr>
			  <td>&nbsp;</td>
			  <td><fmt:message key="date" /></td>
			  <td><fmt:message key="memo" /></td>
			  <td align="center"><fmt:message key="method" /></td>
			  <td align="center"><fmt:message key="amount" /></td>		  
			  <td>&nbsp;</td>
			</tr>
			<c:forEach var="payment" items="${invoiceForm.order.payments}" varStatus="paymentStatus">
			<tr>
			  <td width="20"><c:out value="${paymentStatus.count}"/>.</td>
			  <td>
			    <input type="text" name="__paymentDate_${paymentStatus.index}" id="__paymentDate_${paymentStatus.index}" value="<fmt:formatDate type="date" value="${payment.date}" pattern="MM/dd/yyyy"/>" readonly size="10" maxlength="10" />
				  <img class="calendarImage"  id="__paymentDate_${paymentStatus.index}_trigger" src="../graphics/calendarIcon.jpg" />
				  <script type="text/javascript">
		    		Calendar.setup({
		        			inputField     :    "__paymentDate_${paymentStatus.index}",   // id of the input field
		        			showsTime      :    false,
		        			ifFormat       :    "%m/%d/%Y",   // format of the input field
		        			button         :    "__paymentDate_${paymentStatus.index}_trigger"   // trigger for the calendar (button ID)
		    		});	
				  </script> 
			  </td>
			  <td><input type="text" name="__paymentMemo_${paymentStatus.index}" value="<c:out value="${payment.memo}"/>" maxlength="50" size="40"></td>
			  <td>
           		<select name="__paymentPaymentMethod_${paymentStatus.index}" style="width:150px;">
            	  <option value="">Please Select</option>
			      <c:forEach items="${customPayments}" var="paymentMethod">
					<c:if test="${paymentMethod.enabled}">
		  	        <option value="${paymentMethod.title}" <c:if test="${paymentMethod.title == payment.paymentMethod}">selected</c:if>><c:out value="${paymentMethod.title}"/></option>
		  	        </c:if>
				  </c:forEach>
           		</select>
			  </td>
			  <td><input type="text" name="__paymentAmount_${paymentStatus.index}" value="<c:out value="${payment.amount}"/>" maxlength="10" size="10"></td>
			  <td><c:if test="${payment.message != null}"><span class="error"><c:out value="${payment.message}"/></span></c:if>&nbsp;</td>
			</tr>
			</c:forEach>
		    <tr>
			  <td colspan="4" align="right"><b><fmt:message key="total" /></b></td>
		      <td align="center">
				<input type="text" id="total" value="<c:out value="${invoiceForm.order.totalPayments}"/>" maxlength="10" size="10" readonly="readonly">
		      </td>
		    </tr>
			</table>
			</c:if>	
			
			<c:if test="${fn:contains(siteConfig['CUSTOMIZE_ORDER_LIST'].value,'priority')}">
			<br/><b><fmt:message key="priority" />:</b> 
		  	    <c:forEach begin="0" end="2" var="priority">
	  			<form:radiobutton value="${priority}" path="order.priority" /><img src="../graphics/priority_${priority}.png" border="0">&nbsp;&nbsp;&nbsp;
		  	    </c:forEach>			
			</c:if>
			
			<c:if test="${fn:contains(siteConfig['CUSTOMIZE_ORDER_LIST'].value,'deliveryPerson')}">
			<br/><b><fmt:message key="deliveryPerson" />:</b> <form:input path="order.deliveryPerson" maxlength="50" htmlEscape="true"/>			
			</c:if>
			
			<table style="clear:both;margin-top:10px;" width="100%">
			<tr><td><b><fmt:message key="customerFields"/>:</b></td></tr>
			<c:forEach items="${customerFields}" var="customerField" varStatus="status">
				<tr><td><c:out value="${customerField.name}" />: <c:out value="${customerField.value}" /></td></tr>
			</c:forEach>
			</table>

		    <br>
			<c:if test="${(siteConfig['EDIT_COMMISSION'].value == 'true') && (gSiteConfig['gAFFILIATE'] > 0) && (invoiceForm.order.affiliate.totalCommissionLevel1 != null || invoiceForm.order.affiliate.totalCommissionLevel2 != null)}"> 
			<table border="0" cellpadding="2" cellspacing="1" width="30%">
			<tr>
			  <td><b><fmt:message key="commissionLevel"/></b></td>
			  <td><b><fmt:message key="amount"/></b></td>
			</tr>
			<c:if test="${invoiceForm.order.affiliate.totalCommissionLevel1 != null}">
			<tr>
			  <td><fmt:message key="commissionLevel1"/></td>
			  <td><input type="text" name="__commissionLevel1" value="<c:out value="${invoiceForm.order.affiliate.totalCommissionLevel1}"/>"></input></td>
			</tr>
			</c:if>
			<c:if test="${invoiceForm.order.affiliate.totalCommissionLevel2 != null}">
			<tr>
			  <td><fmt:message key="commissionLevel2"/></td>
			  <td><input type="text" name="__commissionLevel2" value="<c:out value="${invoiceForm.order.affiliate.totalCommissionLevel2}"/>"></input></td>
			</tr>
			</c:if>
			</table>
	        <br>
			</c:if>
		
			<table width="100%" class="specialInst">
				<tr >
				  <td valign="top"><br /> 
				    <b><c:out value="${siteConfig['SPECIAL_INSTRUCTIONS'].value}"/>:</b>  	
				  </td>
				</tr>
				<tr>
				  <td>
				  <spring:bind path="invoiceForm.order.invoiceNote">
				    <textarea name="<c:out value="${status.expression}"/>" rows="8" cols="100" wrap="soft" class="textfield"><c:out value="${status.value}"/></textarea>
			      </spring:bind>
				  </td>
				</tr>
			</table>  
			  
			<table width="100%">
			<tr>
				<td>
					<c:if test="${invoiceForm.order.workOrderNum == null or canEdit}" >
					<c:forEach begin="1" end="5" step="1" var="skuIndex">
					 <b><fmt:message key="add" /> <fmt:message key="productSku" /><fmt:formatNumber value="${skuIndex}" minIntegerDigits="2" pattern="##" /></b>: <input type="text" style="width:75%;" id="addSku${skuIndex}" name="__addProduct${skuIndex}_sku" value=""/> Qty:<fmt:formatNumber value="${skuIndex}" minIntegerDigits="2" pattern="##" /><input type="text" style="width:50px;" id="addQty${skuIndex}" name="__addProduct${skuIndex}_qty" value=""/> <br />
					</c:forEach>
					</c:if>
				</td>
				<td valign="bottom" align="right" style="width:150px;">
					<input type="submit" name="__update" value="<fmt:message key="recalculate"/>">
					<sec:authorize ifAnyGranted="ROLE_AEM,ROLE_ADMIN_${prop_site_id},ROLE_INVOICE_EDIT">
					<c:if test="${!invoiceForm.newInvoice}">
					<input type="submit" name="__save" value="<fmt:message key="saveChanges"/>">
					</c:if>
					</sec:authorize>
					<c:if test="${invoiceForm.newInvoice and (gSiteConfig['gADD_INVOICE'] or gSiteConfig['gSERVICE'])}">
					<input type="submit" name="__save" value="<fmt:message key="addOrder"/>" onclick="return checkRequiredValues();" />
					</c:if>
					<input type="submit" value="<fmt:message key="cancel"/>" name="_cancel">
				</td>
			</tr>
			</table>
		    <!-- end input field -->   	

    <!-- end tab -->        
	</div>     
  </div>    

  <c:if test="${gSiteConfig['gPAYMENTS'] and orderName != 'quotes'}">
  <h4 title="Open Invoices">Open Invoices</h4>
	<div>
	<!-- start tab -->
	  	<div class="listdivi ln tabdivi"></div>
	  	<div class="listdivi"></div>
	   
		  	<div class="list${classIndex % 2}"><c:set var="classIndex" value="${classIndex+1}" />
		  	<div class="listpp">
		  	<!-- input field -->
				<table class="form" width="100%" cellspacing="5">
				  <tr>
				    <td>&nbsp;</td>
				    <td align="center"><b><fmt:message key="orderNumber" /></b></td>
				    <td align="center"><b><fmt:message key="orderDate" /></b></td>
				    <td align="center"><b><fmt:message key="status" /></b></td>
				    <td align="right"><b><fmt:message key="grandTotal" /></b></td>
				    <td align="right"><b><fmt:message key="paid" /></b></td>
				    <td align="right"><b><fmt:message key="due" /></b></td>
				    <td width="20%">&nbsp;</td>
				  </tr>
				<c:set var="totalGrandTotal" value="0.0" />
				<c:set var="totalAmtPaid" value="0.0" />
				<c:set var="totalAmtDue" value="0.0" />
				<c:if test="${openInvoices == null}">
				  <tr><td colspan="8">&nbsp;</td></tr>
				</c:if>
			    <c:forEach items="${openInvoices}" var="openInvoice" varStatus="status">
				  <tr>
				    <td style="width:50px;text-align:right"><c:out value="${status.index + 1}"/>. </td>
				    <td align="center"><a href="../orders/invoice.jhtm?order=${openInvoice.orderId}" class="nameLink"><c:out value="${openInvoice.orderId}"/></a></td>
				    <td align="center"><fmt:formatDate type="date" timeStyle="default" value="${openInvoice.dateOrdered}"/></td>
					<td align="center"><c:choose><c:when test="${openInvoice.status != null}"><fmt:message key="orderStatus_${openInvoice.status}" /></c:when><c:otherwise>&nbsp;</c:otherwise></c:choose></td>
					<td align="right"><fmt:formatNumber value="${openInvoice.grandTotal}" pattern="#,##0.00" /><c:if test="${openInvoice.grandTotal != null}"><c:set var="totalGrandTotal" value="${openInvoice.grandTotal + totalGrandTotal}" /></c:if></td>
					<td align="right"><fmt:formatNumber value="${openInvoice.amountPaid}" pattern="#,##0.00" /><c:if test="${openInvoice.amountPaid != null}"><c:set var="totalAmtPaid" value="${openInvoice.amountPaid + totalAmtPaid}" /></c:if></td>
					<td align="right"><fmt:formatNumber value="${openInvoice.balance}" pattern="#,##0.00" /><c:if test="${openInvoice.balance != null}"><c:set var="totalAmtDue" value="${openInvoice.balance + totalAmtDue}" /></c:if></td>
					<td>&nbsp;</td>
				  </tr>
				</c:forEach>
				  <tr>
					<td colspan="4" align="right"><b><fmt:message key="total" /></b></td>
				    <td align="right"><b><fmt:formatNumber value="${totalGrandTotal}" pattern="#,##0.00" /></b></td>
				    <td align="right"><b><fmt:formatNumber value="${totalAmtPaid}" pattern="#,##0.00" /></b></td>
				    <td align="right"><b><fmt:formatNumber value="${totalAmtDue}" pattern="#,##0.00" /></b></td>
				  </tr>
				  <tr>
					<td colspan="4" align="right"><b><fmt:message key="payment" /> <fmt:message key="alert" /> - <fmt:message key="paid" /></b>: ${order.customer.paymentAlert - totalAmtPaid}</td>
				    <td align="right"><b><fmt:formatNumber value="${totalGrandTotal}" pattern="#,##0.00" /></b></td>
				    <td align="right"><b><fmt:formatNumber value="${totalAmtPaid}" pattern="#,##0.00" /></b></td>
				    <td align="right"><b><fmt:formatNumber value="${totalAmtDue}" pattern="#,##0.00" /></b></td>
				  </tr>
				</table>
		    <!-- end input field -->   	
		  	</div>
		  	</div>

	<!-- end tab -->        
	</div> 
	</c:if>
	
<!-- end tabs -->			
</div>
  
  <!-- end table -->
    </td><td class="boxmidr" ></td></tr>
  <tr><td class="botl"></td><td class="botr"></td></tr>
  </table>
  
<!-- end main box -->  
 </div>

<!-- start button -->
<!-- end button -->	     
</c:if>
</form:form>
<script language="JavaScript">
<!--
function toggleStateProvince(el, type) {
  if (el.value == "US") {
      document.getElementById(type + '_state').disabled=false;
      document.getElementById(type + '_state').style.display="block";
      document.getElementById(type + '_ca_province').disabled=true;
      document.getElementById(type + '_ca_province').style.display="none";
      document.getElementById(type + '_province').disabled=true;
      document.getElementById(type + '_province').style.display="none";
  } else if (el.value == "CA") {
      document.getElementById(type + '_state').disabled=true;
      document.getElementById(type + '_state').style.display="none";
      document.getElementById(type + '_ca_province').disabled=false;
      document.getElementById(type + '_ca_province').style.display="block";
      document.getElementById(type + '_province').disabled=true;
      document.getElementById(type + '_province').style.display="none";
  } else {
      document.getElementById(type + '_state').disabled=true;
      document.getElementById(type + '_state').style.display="none";
      document.getElementById(type + '_ca_province').disabled=true;
      document.getElementById(type + '_ca_province').style.display="none";
      document.getElementById(type + '_province').disabled=false;
      document.getElementById(type + '_province').style.display="block";
  }
}
toggleStateProvince(document.getElementById('order.billing.country'), 'billing');
toggleStateProvince(document.getElementById('order.shipping.country'), 'shipping');
<c:if test="${gSiteConfig['gSPECIAL_PRICING']}" >
function toggleSpecialPricing(el, id) {
  if (el.checked) {
	document.getElementById(id).checked = false;
  }
}
</c:if>
<c:if test="${!invoiceForm.newInvoice and siteConfig['CHANGE_ORDER_DATE'].value == 'true'}">
  Calendar.setup({
      inputField     :    "orderDate",   // id of the input field
      showsTime      :    true,
      ifFormat       :    "%m/%d/%Y[%I:%M %p]",   // format of the input field
      button         :    "time_start_trigger" // trigger for the calendar (button ID)
  });
Calendar.setup({
    inputField     :    "order.userDueDate",   // id of the input field
    showsTime      :    true,
    ifFormat       :    "%m/%d/%Y[%I:%M %p]",   // format of the input field
    button         :    "time_start_trigger2" // trigger for the calendar (button ID)
});
Calendar.setup({
    inputField     :    "order.agreedPaymentDate",   // id of the input field
    showsTime      :    true,
    ifFormat       :    "%m/%d/%Y[%I:%M %p]",   // format of the input field
    button         :    "time_start_trigger3" // trigger for the calendar (button ID)
});
Calendar.setup({
    inputField     :    "order.agreedPickUpDate",   // id of the input field
    showsTime      :    true,
    ifFormat       :    "%m/%d/%Y[%I:%M %p]",   // format of the input field
    button         :    "time_start_trigger4" // trigger for the calendar (button ID)
});
</c:if> 
//-->
</script>

 </tiles:putAttribute>    
</tiles:insertDefinition>
