<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ taglib uri="/WEB-INF/tlds/spring.tld" prefix="spring" %>
<%@ taglib uri="http://www.springframework.org/security/tags" prefix="sec" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>

<tiles:insertDefinition name="admin.orders.export" flush="true">
  <tiles:putAttribute name="content" type="string">

<sec:authorize ifAnyGranted="ROLE_AEM,ROLE_ADMIN_${prop_site_id},ROLE_INVOICE_IMPORT_EXPORT">  

<c:if test="${gSiteConfig['gINVOICE_EXPORT']}">
<script language="JavaScript" type="text/JavaScript">
<!--
window.addEvent('domready', function(){			
	var tabs1 = new SimpleTabs($('tab-block-1'), { selector: 'h4'});
	$$('.highlight').each(function(el) {
		var end = el.getStyle('background-color');
		end = (end == 'transparent') ? '#fff' : end;
		var myFx = new Fx.Tween(el, {duration: 700, wait: false});
		myFx.start('background-color', '#f00', end);
	});
});
//-->
</script>

<form:form commandName="orderSearch" method="post">
<!-- main box -->
  <div id="mboxfull">
  
  <!-- start table -->
  <table cellpadding="0" cellspacing="0" border="0" class="module" >
  <tr>
    <td class="topl_g">
	  
	  <!-- breadcrumb -->
	  <p class="breadcrumb">
	  <c:choose>
	  	<c:when test="${orderName == 'quotes' }">
	  	<c:out value="${orderName}"/>
	  		<a href="../orders/quotesList.jhtm"><fmt:message key="${orderName}"/></a> &gt;
	  	</c:when>
	  	<c:otherwise>
	  		<a href="../orders/"><fmt:message key="${orderName}"/></a> &gt;
	  	</c:otherwise>
	  </c:choose>	  
	  <fmt:message key="export"/> - <c:out value="${orderSearch.fileType}" />
	  </p>
	  
	  <!-- Error Message -->
	  <c:if test="${!empty message}">
		 <div class="highlight error"><spring:message code="${message}" arguments="${arguments}"/></div>
	  </c:if>
	  
  </td><td class="topr_g" ></td></tr>
  <tr><td class="boxmidlrg" >

 <!-- tabs -->
 <div id="tab-block-1">
	<h4 title="export"><fmt:message key="export"/></h4>
	<div>
	<!-- start tab -->

        <c:set var="classIndex" value="0" />
        
	  	<div class="listdivi ln tabdivi"></div>
	  	<div class="listdivi"></div>
	   
	   		<div class="list${classIndex % 2} <c:if test="${arguments != null}">highlight</c:if>"><c:set var="classIndex" value="${classIndex+1}" />
		  	<div class="listfl"><fmt:message key="orderExport"/>: </div>
		  	<div class="listp">
		  	<!-- input field -->
				<table class="form" width="100%">
				  <c:forEach items="${exportedFiles}" var="file">
				  <tr>
				    <td class="formName">&nbsp;</td>
				    <td><a href="../excel/${file.name}"><c:out value="${file.name}"/></a> </td>
				  </tr>
				  </c:forEach>
				</table>
		    <!-- end input field -->   	
		  	</div>
		  	</div>
	  	
	<!-- end tab -->        
	</div> 
	
<!-- end tabs -->			
</div>

<!-- start button -->
<div align="left" class="button"> 
    <input type="submit" name="_export" value="<fmt:message key="fileGenerate" />">
</div>
<!-- end button -->	     
  
  <!-- end table -->
    </td><td class="boxmidr" ></td></tr>
  <tr><td class="botl"></td><td class="botr"></td></tr>
  </table>
  
<!-- end main box -->  
 </div>
</form:form>

</c:if>
</sec:authorize>

  </tiles:putAttribute>    
</tiles:insertDefinition>