<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ taglib uri="http://www.springframework.org/security/tags" prefix="sec" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<link href="<c:url value="/assets/invoice.css"/>" rel="stylesheet" type="text/css"/>

<tiles:insertDefinition name="admin.orders" flush="true">
  <tiles:putAttribute name="tab"  value="orders" />
  <tiles:putAttribute name="content" type="string">
<script language="JavaScript"> 
<!--
window.addEvent('domready', function(){			
	var tabs1 = new SimpleTabs($('tab-block-1'), { selector: 'h4'});
});
function chooseMessage(el) {
   	messageList = document.getElementsByName('message');
	for(var i=0; i<messageList.length; i++) {
		messageList[i].disabled=true;
		messageList[i].style.display="none";
	}
   	subjectList = document.getElementsByName('subject');
	for(var i=0; i<subjectList.length; i++) {
		subjectList[i].disabled=true;
		subjectList[i].style.display="none";
	}
   	document.getElementById('message'+el.value).disabled=false;
       document.getElementById('message'+el.value).style.display="block"; 
   	document.getElementById('subject'+el.value).disabled=false;
       document.getElementById('subject'+el.value).style.display="block";     
    if (document.getElementById('htmlID'+el.value).value == 'true') {
    	$('htmlMessage1').checked = true;
	} else {
	    $('htmlMessage1').checked = false;
	}               
}
function toggleMessage() {
   	if (document.getElementById("userNotified1").checked) {
   	  document.getElementById('messageBox').style.display="block";
   	  document.getElementById('messageSelect').style.display="block";
   	} else {
   	  document.getElementById('messageBox').style.display="none";
   	  document.getElementById('messageSelect').style.display="none";
   	}
}
function toggleMe(id) {
	var subNavs = $$('div.subNav');
	subNavs.each(function(el) {
		$(el).setStyles({'display': 'none'});
	});
	
	$('JumpSubnav'+id).setStyles({
		 display: 'block',
		 visibility: 'visible',
         top: $('img'+id).getTop() + 30 + 'px',
         left: $('img'+id).getLeft() + 5 + 'px'
    });
}

//-->
</script>
<!-- main box -->
<div id="mboxfull">
  
  <!-- start table -->
  <table cellpadding="0" cellspacing="0" border="0" class="module" >
  <tr>
    <td class="topl_g">
	  
	  <!-- breadcrumb -->
	  <p class="breadcrumb">
		<a href="../orders/quotesList.jhtm">Quotes</a> &gt;			    	
	    <fmt:message key="quote"/> # <c:out value="${param.order}"/>
	  </p>
	  
	  <!-- Error Message -->
	  <c:if test="${message != null}">
	    <div class="message"><fmt:message key="${message}" /></div>
	  </c:if>
  
  </td><td class="topr_g" ></td></tr>
  <tr><td class="boxmidlrg" >
<c:if test="${order != null}">
 <!-- tabs -->
 <div id="tab-block-1">
	<h4 title="Invoice">Quote</h4>
	<div>
	<!-- start tab -->

	  	<div class="listdivi ln tabdivi"></div>
	  	<div class="listdivi"></div>
	     
	     
	    <form action="invoice.jhtm" method="post" id="invoiceTools"> 
		  	<input type="hidden" name="order" value="<c:out value="${order.orderId}"/>" >
		  	<div class="toolsArea">
		  	  <table width="100%" cellspacing="0" cellpadding="0" border="0">
		  	   <tr>
		  	    <td>
		  	     <div class="screenButton">
		  	     
		  	       <sec:authorize ifAnyGranted="ROLE_AEM,ROLE_ADMIN_${prop_site_id},ROLE_INVOICE_EDIT">
			        <c:if test="${(siteConfig['ALLOW_EDIT_INVOICE_SHIPPED'].value == 'true' or !(order.status == 's' || order.status == 'x')) and (order.paymentMethod == null || order.paymentMethod != 'Amazon' || (order.paymentMethod == 'Amazon' and siteConfig['AMAZON_ORDER_ALLOW_EDIT'].value == 'true'))}">
		  	        <div class="commonButton">
		  	         <span id="img1" onmouseover="toggleMe(1)"><a class="navbutton" href="<c:url value="invoiceForm.jhtm"><c:param name="orderId" value="${order.orderId}"/><c:param name="orderName" value="${orderName}"/></c:url>" class="nameLink"><img src="../graphics/edit.gif" alt="<fmt:message key='edit' />"  border="0">&nbsp;<fmt:message key="edit" /></a></span>
		  	         <div id="JumpSubnav1" class="subNav"></div>
		  	        </div>
		  	        </c:if>
			       </sec:authorize>
			       
			      <div class="commonButton"> 
		  	       <span id="img3" onmouseover="toggleMe(3)"><img src="../graphics/printer.png" alt="print Purchase Order"  border="0">&nbsp;<fmt:message key="print"/></span>
		  	       <div id="JumpSubnav3" class="subNav">
	           		<ul id="subnavItems">
	               		<li><a class="userbutton" href="invoice.jhtm?order=${order.orderId}&__print=true"><fmt:message key="printerFriendly" /></a></li>
	               		<li><a class="userbutton" href="invoice.jhtm?order=${order.orderId}&__label=true"><fmt:message key="label" /></a></li>
	               		<li><a class="userbutton" href="invoice.jhtm?order=${order.orderId}&__printDiscount=true"><fmt:message key="showDiscount" /></a></li>
	               		<c:if test="${siteConfig['ORDER_OVERVIEW'].value == 'true'}">
	               		 <li><a class="userbutton" href="invoice.jhtm?order=${order.orderId}&__overview=true"><fmt:message key="overview" /></a></li>
	               		</c:if>
	           		</ul>
	       		  </div>
		  	      </div>
		  	      <c:if test="${gSiteConfig['gPDF_INVOICE']}">
		  	      <div class="commonButton">
		  	       <span id="img4" onmouseover="toggleMe(4)"><img src="../graphics/email.gif" alt="Email"  border="0">&nbsp;<fmt:message key="email"/></span>
		  	       <div id="JumpSubnav4" class="subNav">
	           		<ul id="subnavItems">
	               		<li><a class="userbutton" href="email.jhtm?orderId=${order.orderId}" ><fmt:message key="email" /> <fmt:message key="quote"/></a></li>
	               	</ul>
	       		  </div>
		  	      </div>
		  	      </c:if>
		  	     </div>
		  	    </td>
		  	   </tr>
		  	  </table>
		  	</div>
			</form>
			
			<div class="list0">
		  	<!-- input field -->
			<hr>
			
			<table border="0" width="100%" cellspacing="3" cellpadding="1">
			<tr valign="top">
			<td>
			<b><fmt:message key="billingInformation" />: </b>
			<c:set value="${order.billing}" var="address"/>
			<c:set value="true" var="billing" />
			<%@ include file="/WEB-INF/jsp/frontend/common/address.jsp" %>
			<c:set value="false" var="billing" />
			</td>
			<td>&nbsp;</td>
			<td>
			<b><c:choose><c:when test="${order.workOrderNum != null}"><fmt:message key="service" /> <fmt:message key="location" /></c:when><c:otherwise><fmt:message key="shippingInformation" /></c:otherwise></c:choose>:</b>
			<c:set value="${order.shipping}" var="address"/>
			<%@ include file="/WEB-INF/jsp/frontend/common/address.jsp" %>
			<c:if test="${not empty workOrder.service.alternateContact}">	
			  <fmt:message key="alternateContact" />: <c:out value="${workOrder.service.alternateContact}"/>
			</c:if>			
			</td>
			<c:choose>
			  <c:when test="${gSiteConfig['gSEARCH_ENGINE_PROSPECT'] and !empty searchEngineProspect}">
			  	<td>&nbsp;</td>
			  	<td align="left" style="width:250px;">
			  	<b><fmt:message key="quoteReferal" />:</b>
				  <table>
				  <tr>
				    <td><fmt:message key="referrer"/></td>
				    <td>:&nbsp;&nbsp;</td>
				    <td><c:out value="${searchEngineProspect.referrer}" />
				  </td>
				  <tr>
				    <td><fmt:message key="keyPhrase"/></td>
				    <td>:&nbsp;&nbsp;</td>
				    <td><c:out value="${searchEngineProspect.queryString}" />
				  </td>
				  </tr>
				  </table>
			  </td>
			  </c:when>
			  <c:otherwise>
			  <td style="width:20%;">&nbsp;</td>
			  </c:otherwise>
			</c:choose>
			<td align="left">
			<b><fmt:message key="customerInformation" />:</b>
			<table cellpadding="0" cellspacing="0">
			  <tr>
			    <td><fmt:message key="account"/> #</td>
			    <td>:&nbsp;&nbsp;</td>
			    <td><c:out value="${customer.accountNumber}"/></td>
			  </tr>
			  <tr>
			    <td><fmt:message key="taxID"/> #</td>
			    <td>:&nbsp;&nbsp;</td>
			    <td><c:out value="${customer.taxId}"/></td>
			  </tr>
			  <tr>
			    <td><fmt:message key="orderCount"/></td>
			    <td>:&nbsp;&nbsp;</td>
			    <td><a href="ordersList.jhtm?email=<c:out value='${customer.username}'/>"><c:out value="${customerOrderInfo.orderCount}"/></a></td>
			  </tr>
			  <tr>
			    <td><fmt:message key="lastOrder"/></td>
			    <td>:&nbsp;&nbsp;</td>
			    <td><fmt:formatDate pattern="MM/dd/yyyy" value="${customerOrderInfo.lastOrderDate}"/></td>
			  </tr>
			  <%--
  		 	  <c:if test="${languageCodes != null}">
			  <tr>
			    <td><fmt:message key="language"/></td>
			    <td>:&nbsp;&nbsp;</td>
			    <td><fmt:message key="language_${order.languageCode}"/></td>
			  </tr>
			  </c:if>
			  --%>
			</table>  
			</td>
			<td>&nbsp;</td>
			<td align="left">
			<b><fmt:message key="quoteInformation" />:</b>
			<table cellpadding="0" cellspacing="0">
			  <c:if test="${order.host != null}">
			  <tr>
			    <td><fmt:message key="multiStore" /></td>
			    <td>:&nbsp;&nbsp;</td>
			    <td><c:out value="${order.host}"/></td>
			  </tr>
			  </c:if>
			  <tr>
			    <td><fmt:message key="quote" /> #</td>
			    <td>:&nbsp;&nbsp;</td>
			    <td><b><c:out value="${order.orderId}"/></b></td>
			  </tr>
			  <c:if test="${order.externalOrderId != null}">
			  <tr>
			    <td><fmt:message key="externalOrderId" /></td>
			    <td>:&nbsp;&nbsp;</td>
			    <td><b><c:out value="${order.externalOrderId}"/></b></td>
			  </tr>
			  </c:if>
			<c:if test="${order.subscriptionCode != null}">
			  <tr>
			    <td><fmt:message key="subscription" /></td>
			    <td>:&nbsp;&nbsp;</td>
			    <td><a href="subscription.jhtm?code=${order.subscriptionCode}"><c:out value="${order.subscriptionCode}"/></a></td>
			  </tr>
			</c:if>
			  <tr>
			    <td><fmt:message key="quoteDate" /></td>
			    <td>:&nbsp;&nbsp;</td>
			    <td><fmt:formatDate type="both" timeStyle="full" pattern="MM/dd/yyyy hh:mm a" value="${order.dateOrdered}"/></td>
			  </tr>
			<c:if test="${order.lastModified != null}">
			  <tr>
			    <td><fmt:message key="lastModified" /></td>
			    <td>:&nbsp;&nbsp;</td>
			    <td><fmt:formatDate type="both" timeStyle="full" pattern="MM/dd/yyyy hh:mm a" value="${order.lastModified}"/></td>
			  </tr>
			</c:if>
			<c:if test="${siteConfig['DELIVERY_TIME'].value == 'true'}" >
			  <tr>
			    <td><fmt:message key="dueDate" /></td>
			    <td>:&nbsp;&nbsp;</td>
			    <td style="color:#FF0000;font-weight:700;"><fmt:formatDate type="both" timeStyle="full" pattern="MM/dd/yyyy" value="${order.dueDate}"/></td>
			  </tr>
			</c:if>
			<c:if test="${fn:contains(siteConfig['USER_EXPECTED_DELIVERY_TIME'].value, 'true')}" >
			  <tr>
			    <td><fmt:message key="userExpectedDueDate" /></td>
			    <td>:&nbsp;&nbsp;</td>
			    <td style="color:#FF0000;font-weight:700;"><fmt:formatDate type="both" timeStyle="full" pattern="MM/dd/yyyy" value="${order.userDueDate}"/></td>
			  </tr>
			</c:if>
			<c:if test="${siteConfig['REQUESTED_CANCEL_DATE'].value == 'true'}" >
			  <tr>
			    <td><fmt:message key="requestedCancelDate" /></td>
			    <td>:&nbsp;&nbsp;</td>
			    <td style="color:#FF0000;font-weight:700;"><fmt:formatDate type="both" timeStyle="full" pattern="MM/dd/yyyy" value="${order.requestedCancelDate}"/></td>
			  </tr>
			</c:if>
			<c:if test="${order.ipAddress != null}">
			  <tr>
			    <td><fmt:message key="ipAddress" /></td>
			    <td>:&nbsp;&nbsp;</td>
			    <td><c:out value="${order.ipAddress}"/></td>
			  </tr>
			</c:if>
			<c:if test="${gSiteConfig['gSALES_REP']}">
			  <tr>
			    <td><fmt:message key="salesRep"/></td>
			    <td>:&nbsp;&nbsp;</td>
			    <td><c:out value="${salesRep.name}"/></td>
			  </tr>
			  <tr>
			    <td><fmt:message key="processedBy"/></td>
			    <td>:&nbsp;&nbsp;</td>
			    <td><c:out value="${salesRepProcessedBy.name}"/></td>
			  </tr>
			</c:if>
			  <tr>
			    <td><fmt:message key="status"/></td>
			    <td>:&nbsp;&nbsp;</td>
			    <td><fmt:message key="${order.encodeStatus}"/></td>
			  </tr>
			  <c:if test="${gSiteConfig['gADD_INVOICE']}">
			  <tr>
			    <td><fmt:message key="from"/></td>
			    <td>:&nbsp;&nbsp;</td>
			    <td><fmt:message key="${order.backEndOrderString}"/></td>
			  </tr>
			  </c:if>
			  <c:if test="${gSiteConfig['gINVOICE_APPROVAL'] and !empty order.approval}">
			  <tr>
                <td><fmt:message key="approval"/></td>
                <td>:&nbsp;&nbsp;</td>
                <td><fmt:message key="approvalStatus_${order.approval}" /></td>
              </tr>  
              </c:if>
              <c:if test="${!empty order.orderType}">
              <tr>
			    <td><fmt:message key="quoteType"/></td>
			    <td>:&nbsp;&nbsp;</td>
			    <td><c:out value="${order.orderType}"/></td>
			  </tr>
              </c:if>
              <c:if test="${!empty order.trackcode}">
              <tr>
			    <td><fmt:message key="trackcode"/></td>
			    <td>:&nbsp;&nbsp;</td>
			    <td><c:out value="${order.trackcode}"/></td>
			  </tr>
              </c:if>
              <c:if test="${order.flag1 != null}">
              <tr>
			    <td><c:out value="${siteConfig['ORDER_FLAG1_NAME'].value}" /></td>
			    <td>:&nbsp;&nbsp;</td>
			    <td><fmt:message key="flag_${order.flag1}"/></td>
			  </tr>
              </c:if>
			  <c:if test="${zipFile}">
			  <tr>
			    <td><fmt:message key="zipFile"/></td>
			    <td>:&nbsp;&nbsp;</td>
			    <td><a href="<c:url value="/assets/orders_zip/${order.orderId}.zip"/>"><c:out value="${order.orderId}"/>.zip</a></td>
			  </tr>
			  </c:if>
			  <c:if test="${order.linkShare != null}">
			  <tr>
			    <td>LinkShare siteID</td>
			    <td>:&nbsp;&nbsp;</td>
			    <td><c:out value="${order.linkShare.siteID}"/></td>
			  </tr>
			  <tr>
			    <td>LinkShare Entry</td>
			    <td>:&nbsp;&nbsp;</td>
			    <td><fmt:formatDate type="both" timeStyle="full" pattern="MM/dd/yyyy hh:mm a" value="${order.linkShare.dateEntered}"/></td>
			  </tr>
			  </c:if>
			</table>
			</td>
			</tr>
			</table>
			<br/>
			<b><fmt:message key="emailAddress" />:</b> <a href="mailto:${customer.username}"><c:out value="${customer.username}"/></a>

			<c:if test="${order.purchaseOrder != '' and order.purchaseOrder != null}" >
			<br/><b><fmt:message key="purchaseOrder" /></b>: <c:out value="${order.purchaseOrder}" />
			</c:if>

			<table border="0" cellpadding="2" cellspacing="1" width="100%" class="invoice">
			  <tr>
			    <c:set var="cols" value="0"/>
			    <th width="5%" class="invoice"><fmt:message key="line" />#</th>
			    <th class="invoice"><fmt:message key="productSku" /></th>
			    <th class="invoice"><fmt:message key="productName" /></th>
			    <c:forEach items="${productFieldsHeader}" var="productField">
			      <c:if test="${productField.showOnInvoice or productField.showOnInvoiceBackend}">
			      <th class="invoice"><c:out value="${productField.name}" /></th><c:set var="cols" value="${cols+1}"/>
			      </c:if>
			    </c:forEach>
			    <th width="10%" class="invoice"><fmt:message key="quantity" /></th>
			    <c:if test="${siteConfig['GENERATE_PACKING_LIST'].value == 'true'}"> 
			      <c:set var="cols" value="${cols+1}"/>
			      <th width="10%" class="invoice"><fmt:message key="packingList" /></th>
			    </c:if>
			    <c:if test="${gSiteConfig['gINVENTORY'] && order.hasLowInventoryMessage}">
			      <c:set var="cols" value="${cols+1}"/>
			      <th width="10%" class="invoice"><fmt:message key="lowInventory" /></th>
			    </c:if>
			    <c:if test="${order.hasPacking}">
				  <c:set var="cols" value="${cols+1}"/>
			      <th width="10%" class="invoice"><fmt:message key="packing" /></th>
			    </c:if>
			    <c:if test="${order.hasContent}">
				  <c:set var="cols" value="${cols+1}"/>
			      <th class="invoice"><fmt:message key="content" /></th>
			      <c:if test="${!empty siteConfig['EXT_QUANTITY_TITLE'].value}">
			      <c:set var="cols" value="${cols+1}"/>
			      <th class="invoice"><c:out value="${siteConfig['EXT_QUANTITY_TITLE'].value}" /></th>
			      </c:if>
			    </c:if>
			    <c:if test="${order.hasCustomShipping}">
	  			  <c:set var="cols" value="${cols+1}"/>
      			  <th class="invoice"><fmt:message key="customShipping" /></th>
    			</c:if>
    			<c:if test="${order.hasLineItemDiscount}">
	              <c:set var="cols" value="${cols+2}"/>
                  <th class="invoice"><fmt:message key="originalPrice" /></th>
                  <th class="invoice"><fmt:message key="discount" /></th>
                </c:if>
			    <th class="invoice"><fmt:message key="productPrice" /><c:if test="${order.hasContent}"> / <c:out value="${siteConfig['CASE_UNIT_TITLE'].value}" /></c:if></th>
			   	<c:if test="${gSiteConfig['gPURCHASE_ORDER'] && siteConfig['SHOW_COST'].value == 'true' && order.hasCost}">
			      <c:set var="cols" value="${cols+1}"/>
			      <th class="invoice"><fmt:message key="cost" /></th>
			    </c:if> 
			    <th class="invoice"><fmt:message key="total" /></th>
			  </tr>
			<c:forEach var="lineItem" items="${order.lineItems}" varStatus="status">
			<tr valign="top">
			  <td class="invoice" align="center"><c:out value="${status.count}"/></td>
			  <td class="invoice">
				<c:if test="${lineItem.customImageUrl != null}">
			      <a href="customFrame.jhtm?orderId=${order.orderId}&lineNum=${lineItem.lineNumber}" onclick="window.open(this.href,'','width=650,height=800,resizable=yes'); return false;"><img class="invoiceImage" src="../../assets/customImages/${order.orderId}/${lineItem.customImageUrl}" border="0" /></a>
			    </c:if>  			  
			    <c:if test="${siteConfig['SHOW_IMAGE_ON_INVOICE'].value == 'true' and lineItem.product.thumbnail != null}">
		         <img class="invoiceImage" border="0" <c:if test="${!lineItem.product.thumbnail.absolute}"> src="<c:url value="/assets/Image/Product/thumb/${lineItem.product.thumbnail.imageUrl}"/>" </c:if> <c:if test="${lineItem.product.thumbnail.absolute}"> src="<c:url value="${lineItem.product.thumbnail.imageUrl}"/>" </c:if> />
		        </c:if><c:out value="${lineItem.product.sku}"/>
		      </td>
			  <td class="invoice"><c:out value="${lineItem.product.name}"/>
			  <c:if test="${lineItem.customXml != null}"><a href="customFrame.jhtm?orderId=${order.orderId}&lineNum=${lineItem.lineNumber}" onclick="window.open(this.href,'','width=650,height=800,resizable=yes'); return false;">(Custom Frame)</a></c:if>
			   <table border="0" cellspacing="1" cellpadding="0">
				  <c:forEach items="${lineItem.productAttributes}" var="productAttribute" varStatus="productAttributeStatus">
					<tr class="invoice_lineitem_attributes${productAttributeStatus.count%2}">
					<td class="optionName" style="white-space: nowrap" align="right"><c:out value="${productAttribute.optionName}"/>: </td>
					<td class="optionValue" style="padding-left:5px;"><c:out value="${productAttribute.valueString}" escapeXml="false"/></td>
					<c:if test="${(! empty productAttribute.optionPriceOriginal) && productAttribute.optionPriceOriginal != 0}">
					<td class="optionPrice"><fmt:formatNumber value="${productAttribute.optionPriceOriginal}" pattern="#,##0.00" /></td>
					<td class="optionNote" style="padding-left:5px;"><fmt:message key="${productAttribute.optionPriceMessageF}"/></td>
					</c:if>
					</tr>
				  </c:forEach>
	  			  <%@ include file="/WEB-INF/jsp/frontend/common/customLinesInvoice.jsp"%>
			   </table>
				<c:if test="${lineItem.subscriptionInterval != null}">
				  <c:set var="intervalType" value="${fn:substring(lineItem.subscriptionInterval, 0, 1)}"/>
				  <c:set var="intervalUnit" value="${fn:substring(lineItem.subscriptionInterval, 1, 2)}"/>
				  <div class="invoice_lineitem_subscription">
					<fmt:message key="subscription"/> <fmt:message key="details"/> : <fmt:message key="delivery"/>
					<c:choose>
					  <c:when test="${intervalUnit == '1'}"><fmt:message key="every_${intervalType}"><fmt:param value="${intervalUnit}"/></fmt:message></c:when>
					  <c:otherwise><fmt:message key="every_${intervalType}s"><fmt:param value="${intervalUnit}"/></fmt:message></c:otherwise>
					</c:choose>
				  </div>
				  <div class="invoice_lineitem_subscription"><a href="subscription.jhtm?code=${lineItem.subscriptionCode}" class="invoice_lineitem_subscription"><c:out value="${lineItem.subscriptionCode}"/></a></div>
				</c:if>
			  <c:if test="${siteConfig['SHOW_IMAGE_ON_INVOICE'].value == 'true' and lineItem.hasProductAttributesImage}" >
			  <c:forEach items="${lineItem.productAttributes}" var="productAttribute">
			    <c:if test="${!empty productAttribute.imageUrl}" > 
			      <c:if test="${!productAttribute.absolute}"><img src="<c:url value="/assets/Image/Product/options/${productAttribute.imageUrl}"/>" border="0" class="invoiceOptionImage" /></c:if>
			      <c:if test="${productAttribute.absolute}"><img src="<c:url value="${productAttribute.imageUrl}"/>" border="0" class="invoiceOptionImage" /></c:if>
			    </c:if>
			  </c:forEach>
			  </c:if>
			  </td>
			  <c:forEach items="${productFieldsHeader}" var="pFHeader" varStatus="status"><c:set var="check" value="0"/>
			  <c:if test="${pFHeader.showOnInvoice or pFHeader.showOnInvoiceBackend}">
			  <c:forEach items="${lineItem.productFields}" var="productField"> 
			   <c:if test="${pFHeader.id == productField.id}">
			    <td class="invoice"><c:out value="${productField.value}" /></td><c:set var="check" value="1"/>
			   </c:if>       
			  </c:forEach>
			  <c:if test="${check == 0}">
			    <td class="invoice">&nbsp;</td>
			   </c:if>
			  </c:if>
			  </c:forEach>
			  <td class="invoice" align="center" <c:choose>
			  <c:when test="${lineItem.processed == lineItem.quantity }">style="background-color:#bbff99;"</c:when>
			  <c:when test="${(lineItem.processed > lineItem.quantity) or (lineItem.processed < lineItem.quantity) }">style="background-color:#FFAFAF;"</c:when>
			  </c:choose> ><c:out value="${lineItem.quantity}"/></td>
			  <c:if test="${siteConfig['GENERATE_PACKING_LIST'].value == 'true'}"> 
			    <td class="invoice" align="center" <c:choose>
			    <c:when test="${lineItem.processed == lineItem.quantity}">style="background-color:#bbff99;"</c:when>
			    <c:when test="${(lineItem.processed > lineItem.quantity) or (lineItem.processed < lineItem.quantity) }">style="background-color:#FFAFAF;"</c:when>
			    </c:choose> ><c:out value="${lineItem.processed}"/></td>
			  </c:if>
			  <c:if test="${gSiteConfig['gINVENTORY'] && order.hasLowInventoryMessage}">
			   <td class="invoice" align="center"><c:out value="${lineItem.lowInventoryMessage}" /></td>
			  </c:if>
			  <c:if test="${order.hasPacking }">
			  <td class="invoice" align="center"><c:out value="${lineItem.product.packing}" /></td>
			  </c:if>
			  <c:if test="${order.hasContent}">
			    <td class="invoice" align="center"><c:out value="${lineItem.product.caseContent}" /></td>
			    <c:if test="${!empty siteConfig['EXT_QUANTITY_TITLE'].value}">
			    <td class="invoice" align="center">
			     <c:choose>
			      <c:when test="${!empty lineItem.product.caseContent}"><c:out value="${lineItem.product.caseContent * lineItem.quantity}" /></c:when>
			      <c:otherwise><c:out value="${lineItem.quantity}" /></c:otherwise> 
			     </c:choose>
			    </td>
			    </c:if>
			  </c:if>
			  <c:if test="${order.hasCustomShipping}">
			    <td class="invoice" align="center">
			    <c:if test="${!empty lineItem.customShippingCost}">
			     <c:out value="${order.customShippingTitle}" />
			    </c:if>
			    </td>
			  </c:if>
			  <c:if test="${order.hasLineItemDiscount}">
                <td class="invoice" align="right"><fmt:formatNumber value="${lineItem.originalPrice}" pattern="#,##0.00"/></td> 
                <td class="invoice" align="right">
                <c:if test="${lineItem.discount != null}" >
                  <c:choose>
                  <c:when test="${lineItem.percent}"><c:out value="${lineItem.discount}"/>%</c:when>
                  <c:otherwise><fmt:message key="${siteConfig['CURRENCY'].value}" /><fmt:formatNumber value="${lineItem.discount}" pattern="#,##0.00"/></c:otherwise> 
                  </c:choose>
                </c:if> 
                </td> 
              </c:if>
			  <td class="invoice" align="right"><fmt:formatNumber value="${lineItem.unitPrice}" pattern="#,##0.00#" maxFractionDigits="${siteConfig['MAX_FRACTION_DIGIT'].value}"/></td> 
			  <c:if test="${gSiteConfig['gPURCHASE_ORDER'] && siteConfig['SHOW_COST'].value == 'true' && order.hasCost}">
			  <td class="invoice" align="right"><fmt:formatNumber value="${lineItem.unitCost}" pattern="#,##0.00"/></td> 
			  </c:if>
			  <td class="invoice" align="right"><fmt:formatNumber value="${lineItem.totalPrice}" pattern="#,##0.00"/></td>
			</tr> 
			<c:if test="${lineItem.serialNums != null}">
			<tr bgcolor="#FFFFFF">
			  <td colspan="${6+cols}">
			    <table cellspacing="3">
			      <tr>
			        <td valign="top">S/N:</td>
			        <td><c:forEach items="${lineItem.serialNums}" var="serialNum" varStatus="status"><c:if test="${not status.first}">, </c:if><c:out value="${serialNum}"/></c:forEach></td>
			      </tr>
			    </table>
			  </td>
			</tr>			
			</c:if>	
			<c:if test="${not empty lineItem.trackNum or lineItem.dateShipped != null or not empty lineItem.shippingMethod or not empty lineItem.status}">
			<tr bgcolor="#FFFFFF">
			  <td colspan="${6+cols}">
			    <c:if test="${not empty lineItem.status}"><fmt:message key="status"/>: <c:out value="${lineItem.status}"/></c:if>
			    <c:if test="${not empty lineItem.shippingMethod}"><c:out value="${lineItem.shippingMethod}"/></c:if>
			    <c:if test="${lineItem.dateShipped != null}"><fmt:message key="dateShipped"/>: <fmt:formatDate type="date" value="${lineItem.dateShipped}" pattern="MM/dd/yyyy"/></c:if>
			    <c:if test="${not empty lineItem.trackNum}"><fmt:message key="trackNum"/>: <c:out value="${lineItem.trackNum}"/></c:if>
			  </td>
			</tr>			
			</c:if>	
			</c:forEach>
			  <tr bgcolor="#BBBBBB">
			    <td colspan="${6+cols}">&nbsp;</td>
			  </tr>
			  <tr>
			    <td class="invoice" colspan="${5+cols}" align="right"><fmt:message key="subTotal" />:</td>
			    <td class="invoice" align="right"><fmt:formatNumber value="${order.subTotal}" pattern="#,##0.00"/></td>
			  </tr>
			  <c:if test="${order.budgetEarnedCredits > 0.00 }">
			  	<tr class="customerEarnedCreditsBoxId">
			  		<td class="discount" colspan="${5+cols}" align="right">Earned Credits: </td>
			  		<td class="discount" align="right"><fmt:formatNumber value="${order.budgetEarnedCredits}" pattern="-#,##0.00"/></td>
			  	</tr>
		      </c:if>
			  <c:if test="${order.promo.title != null and order.promo.discountType eq 'order'}" >
				  <tr>
				    <td class="discount" colspan="${5+cols}" align="right">
      				<c:choose>
        			  <c:when test="${order.promo.discount == 0}"><fmt:message key="promoCode" /></c:when>
        			  <c:otherwise><fmt:message key="discountForPromoCode" /> </c:otherwise>
      				</c:choose>
      				<c:out value="${order.promo.title}" />
				    <c:choose>
				      <c:when test="${order.promo.discount == 0}"></c:when>
			          <c:when test="${order.promo.percent}">
			            (<fmt:formatNumber value="${order.promo.discount}" pattern="#,##0.00"/>%)
			          </c:when>
			          <c:otherwise>
			            (<fmt:message key="${siteConfig['CURRENCY'].value}" /><fmt:formatNumber value="${order.promo.discount}" pattern="#,##0.00"/>)
			          </c:otherwise>
			      	</c:choose>	    
				    </td>
				    <td class="discount" align="right">
				    <c:choose>
				      <c:when test="${order.promo.discount == 0}">&nbsp;</c:when>
			          <c:when test="${order.promo.percent}">
			            (<fmt:formatNumber value="${order.subTotal * ( order.promo.discount / 100.00 )}" pattern="#,##0.00"/>)
			          </c:when>
			          <c:when test="${(!order.promo.percent) and (order.promo.discount > order.subTotal)}">
			        	(<fmt:formatNumber value="${order.subTotal}" pattern="#,##0.00"/>)
			          </c:when>
			          <c:otherwise>
			            (<fmt:formatNumber value="${order.promo.discount}" pattern="#,##0.00"/>)
			          </c:otherwise>
			      	</c:choose>	    
				    </td>
				  </tr>
			  </c:if>
			  <c:if test="${order.lineItemPromos != null and fn:length(order.lineItemPromos) gt 0}">
    			<c:forEach items="${order.lineItemPromos}" var="itemPromo" varStatus="status">
    			<tr>
      			<td class="discount" colspan="${5+cols}" align="right">
      			<c:choose>
        			<c:when test="${itemPromo.value == 0}"><fmt:message key="promoCode" /></c:when>
        			<c:otherwise><fmt:message key="discountForPromoCode" /></c:otherwise>
      			</c:choose>
      			<c:out value="${itemPromo.key}" />
      			</td>
      			<td class="discount" align="right">
      			<c:choose>
        			<c:when test="${itemPromo.value == 0}">&nbsp;</c:when>
       			 <c:otherwise>($<fmt:formatNumber value="${itemPromo.value}" pattern="#,##0.00"/>)</c:otherwise>
      			</c:choose>
      			</td>
    			</tr>
    			</c:forEach>  
  			  </c:if>
			  <c:choose>
    			<c:when test="${order.taxOnShipping}">
			      <tr>
			    	<td class="invoice" colspan="${5+cols}" align="right"><fmt:message key="shippingHandling" /> (<c:out value="${orderForm.order.shippingMethod}" escapeXml="false"/>):</td>
			    	<td class="invoice" align="right"><fmt:formatNumber value="${order.shippingCost}" pattern="#,##0.00"/></td>
			  	  </tr>
			  	  <tr>
			    	<td class="invoice" colspan="${5+cols}" align="right"><fmt:message key="tax" /></td>
			    	<td class="invoice" align="right"><fmt:formatNumber value="${order.tax}" pattern="#,##0.00"/></td>
			  	  </tr>
  				</c:when>
			    <c:otherwise>
			      <tr>
			    	<td class="invoice" colspan="${5+cols}" align="right"><fmt:message key="tax" /></td>
			    	<td class="invoice" align="right"><fmt:formatNumber value="${order.tax}" pattern="#,##0.00"/></td>
			  	  </tr>
			  	  <tr>
			    	<td class="invoice" colspan="${5+cols}" align="right"><fmt:message key="shippingHandling" /> (<c:out value="${orderForm.order.shippingMethod}" escapeXml="false"/>):</td>
			    	<td class="invoice" align="right"><fmt:formatNumber value="${order.shippingCost}" pattern="#,##0.00"/></td>
			  	  </tr> 
			    </c:otherwise>
  			  </c:choose>
			  <c:if test="${order.promo.title != null and order.promo.discountType eq 'shipping' and order.shippingCost != null}" >
				  <tr>
				    <td class="discount" colspan="${5+cols}" align="right">
      				<c:choose>
        			  <c:when test="${order.promo.discount == 0}"><fmt:message key="promoCode" /></c:when>
        			  <c:otherwise><fmt:message key="discountForPromoCode" /> </c:otherwise>
      				</c:choose>
      				<c:out value="${order.promo.title}" />
				    <c:choose>
				      <c:when test="${order.promo.discount == 0}"></c:when>
			          <c:when test="${order.promo.percent}">
			            (<fmt:formatNumber value="${order.promo.discount}" pattern="#,##0.00"/>%)
			          </c:when>
			          <c:otherwise>
			            (<fmt:message key="${siteConfig['CURRENCY'].value}" /><fmt:formatNumber value="${order.promo.discount}" pattern="#,##0.00"/>)
			          </c:otherwise>
			      	</c:choose>	    
				    </td>
				    <td class="discount" align="right">
				    <c:choose>
				      <c:when test="${order.promo.discount == 0}">&nbsp;</c:when>
			          <c:when test="${order.promo.percent}">
			            (<fmt:formatNumber value="${order.shippingCost * ( order.promo.discount / 100.00 )}" pattern="#,##0.00"/>)
			          </c:when>
			       	  <c:when test="${(!order.promo.percent) and (order.promo.discount > order.shippingCost)}">
			        	(<fmt:formatNumber value="${order.shippingCost}" pattern="#,##0.00"/>)
			          </c:when>
			          <c:otherwise>
			            (<fmt:formatNumber value="${order.promo.discount}" pattern="#,##0.00"/>)
			          </c:otherwise>
			      	</c:choose>	    
				    </td>
				  </tr>
			  </c:if>
			  <c:if test="${order.customShippingCost != null}">
			  <tr>
			    <td class="invoice" colspan="${5+cols}" align="right"><fmt:message key="customShipping" /> (<c:out value="${order.customShippingTitle}" escapeXml="false"/>):</td>
			    <td class="invoice" align="right"><fmt:formatNumber value="${order.customShippingCost}" pattern="#,##0.00"/></td>
			  </tr>
			  </c:if>
			  <c:if test="${order.ccFee != null && order.ccFee != 0}">
			  <tr>
			    <td class="invoice" colspan="${5+cols}" align="right"><fmt:message key="creditCardFee" />:</td>
			    <td class="invoice" align="right"><fmt:formatNumber value="${order.ccFee}" pattern="#,##0.00"/></td>
			  </tr>
			  </c:if>	
			  <c:choose>
			  <c:when test="${gSiteConfig['gBUDGET'] and order.requestForCredit != null and customer.creditAllowed != null and customer.creditAllowed > 0}">
		    	<tr>
			    	<td class="discount" colspan="${5+cols}" align="right">
			    	<fmt:message key="creditRequestAmount">
						<fmt:param>${customer.creditAllowed}</fmt:param>
						<fmt:param><fmt:formatNumber value="${subTotalByPlan}" pattern="#,##0.00"/></fmt:param>
						<fmt:param><fmt:formatNumber value="${(customer.creditAllowed / 100) * subTotalByPlan}" pattern="#,##0.00"/></fmt:param>
				    </fmt:message>
			    	<br/> 
			    	<fmt:message key="creditApprovalMessage" />
			    	</td>
			    	<c:choose>
		  				<c:when test="${order.requestForCredit > 0 }">
			    			<td class="discount" align="right"><fmt:formatNumber value="${order.requestForCredit}" pattern="-#,##0.00"/></td>
		  				</c:when>
		  				<c:otherwise>
		  					<td class="discount" align="right">0.00</td>
		  				</c:otherwise>
		  			</c:choose>
			    </tr>
			  </c:when>
			  <c:when test="${(gSiteConfig['gGIFTCARD'] or gSiteConfig['gVIRTUAL_BANK_ACCOUNT']) and order.creditUsed != null and order.creditUsed > 0}">
				  <tr>
				    <td class="discount" colspan="${5+cols}" align="right"><fmt:message key="credit" />:</td>
				    <td class="discount" align="right"><fmt:formatNumber value="${order.creditUsed}" pattern="-#,##0.00"/></td>
				  </tr>
			  </c:when>
			  </c:choose>			  
			  <c:if test="${order.bondCost != null}">
			  <tr>
			    <td class="invoice" align="right" colspan="2">
			    <c:if test="${!fn:contains(order.status,'x')}">
			      <a style="width: 100px; float: right; text-align: center; background-color:#FF9999; color:WHITE;" href="invoice.jhtm?order=${order.orderId}&__cancelBuySafe=true"><fmt:message key="cancel" /></a>
			    </c:if>
			    </td>
			    <td class="invoice" colspan="${3+cols}" align="right"><fmt:message key="buySafeBondGuarntee" />:</td>
			    <td class="invoice" align="right"><fmt:formatNumber value="${order.bondCost}" pattern="#,##0.00"/></td>
			  </tr>
			  </c:if>			  
			  <tr>
			    <td class="invoice" colspan="${5+cols}" align="right"><fmt:message key="grandTotal" />:</td>
			    <td align="right" bgcolor="#F8FF27"><fmt:message key="${siteConfig['CURRENCY'].value}" /><fmt:formatNumber value="${order.grandTotal}" pattern="#,##0.00"/></td>
			  </tr>
			</table>
			<div>
			<c:out value="${siteConfig['INVOICE_MESSAGE'].value}" escapeXml="false"/>
			</div>				
			<c:if test="${partnerList != null and fn:length(partnerList) gt 0}">
			<div class="partnersBox">
					<b><fmt:message key="partnersList"/></b>
			    	<c:forEach items="${partnerList}" var="partner">
			    		<div class="wrapper">
			    		<c:out value="${partner.partnerName}"/>
			    		<c:out value="${partner.amount}"/>
			    		</div>
			    	</c:forEach>
			    	<div><fmt:message key="total"/>: <fmt:message key="${siteConfig['CURRENCY'].value}" /><fmt:formatNumber value="${order.budgetEarnedCredits}" pattern="#,##0.00"/></div>
			</div>		
			</c:if>	
			
			<c:if test="${gSiteConfig['gPAYMENTS']}">
			<c:set var="totalPayments" value="0"/>
			<c:forEach var="payment" items="${order.paymentHistory}" varStatus="paymentStatus">
			<c:if test="${paymentStatus.first}">
			<table>
			  <tr>
			    <td colspan="5"><b><fmt:message key="paymentHistory"/></b>:</td>
			  </tr>
			</c:if>
			  <tr>
			    <td><fmt:formatDate type="date" timeStyle="default" value="${payment.date}"/></td>
			    <td>&nbsp;</td>
				<td align="right"><fmt:message key="${siteConfig['CURRENCY'].value}" /><fmt:formatNumber value="${payment.amount}" pattern="#,##0.00" /></td>
				<c:set var="totalPayments" value="${totalPayments + payment.amount}"/>
			    <td>&nbsp;</td>
				<td><c:if test="${payment.paymentMethod != ''}"><c:out value="${payment.paymentMethod}"/> </c:if><c:out value="${payment.memo}"/></td>
			  </tr>
			<c:if test="${paymentStatus.last}">
			  <tr>
			    <td class="status"><b><fmt:message key="total" />:</b></td>
			    <td>&nbsp;</td>
				<td align="right" class="status"><b><fmt:message key="${siteConfig['CURRENCY'].value}" /><fmt:formatNumber value="${totalPayments}" pattern="#,##0.00" /></b></td>
			    <td colspan="2">&nbsp;</td>
			  </tr>
			</table>
			</c:if>
			</c:forEach>
			</c:if>
			
			<table style="clear:both;margin-top:10px;" width="100%">
			<c:forEach items="${customerFields}" var="customerField" varStatus="status">
				<tr><td><c:out value="${customerField.name}" />: <c:out value="${customerField.value}" /></td></tr>
			</c:forEach>
			</table>

			<c:if test="${order.invoiceNote != null and order.invoiceNote != ''}">
			<table width="100%">
				<tr >
				  <td valign="top">
				    <b><c:out value="${siteConfig['SPECIAL_INSTRUCTIONS'].value}"/>:</b>  	
				  </td>
				</tr>
				<tr>
				  <td>
				  <c:out value="${order.invoiceNote}" escapeXml="false"/>
				  </td>
				</tr>
			</table>
			</c:if>
			
			<hr>
			<br>

			<c:if test="${(siteConfig['EDIT_COMMISSION'].value == 'true') && (gSiteConfig['gAFFILIATE'] > 0) && (order.affiliate.totalCommissionLevel1 != null || order.affiliate.totalCommissionLevel2 != null)}"> 
			<table border="0" cellpadding="2" cellspacing="1" width="30%" >
			<tr>
			  <td><b><fmt:message key="commissionLevel"/></b></td>
			  <td><b><fmt:message key="amount"/></b></td>
			</tr>
			<c:if test="${order.affiliate.totalCommissionLevel1 != null}">
			<tr>
			  <td><fmt:message key="commissionLevel1"/></td>
			  <td><c:out value="${order.affiliate.totalCommissionLevel1}"/></td>
			</tr>
			</c:if>
			<c:if test="${order.affiliate.totalCommissionLevel2 != null}">
			<tr>
			  <td><fmt:message key="commissionLevel2"/></td>
			  <td><c:out value="${order.affiliate.totalCommissionLevel2}"/></td>
			</tr>
			</c:if>
			</table>
			<hr>
			<br>
			</c:if>			
			
			<table border="0" cellpadding="2" cellspacing="1" width="100%" class="statusHistory">
			<tr>
			  <th class="statusHistory">Date Changed</th>
			  <th class="statusHistory"><fmt:message key="status"/></th>
			  <c:if test="${!empty siteConfig['SUB_STATUS'].value}">
			    <th class="statusHistory"><fmt:message key="subStatus"/></th>
			  </c:if>
			  <th class="statusHistory">Customer Notified</th>
			  <th class="statusHistory"><fmt:message key="trackNum"/></th>
			  <th width="100%" class="statusHistory"><fmt:message key="comments"/></th>
			  <th width="100%" class="statusHistory"><fmt:message key="user"/></th>
			</tr>
			<c:forEach var="orderStatus" items="${order.statusHistory}">
			<c:if test="${orderStatus.status != 'xap' or order.status == 'xap'}">
			<tr valign="top">
			  <td class="statusHistory" style="white-space: nowrap"><fmt:formatDate type="both" timeStyle="full" value="${orderStatus.dateChanged}"/></td>
			  <td class="status" align="center" style="white-space: nowrap"><fmt:message key="orderStatus_${orderStatus.status}"/></td>
			  <c:if test="${!empty siteConfig['SUB_STATUS'].value}">
			    <td class="status" align="center" style="white-space: nowrap"><fmt:message key="orderSubStatus_${orderStatus.subStatus}"/></td>
			  </c:if>
			  <td class="statusHistory" align="center"><c:choose><c:when test="${orderStatus.userNotified}"><img src="../graphics/checkbox.png"></c:when><c:otherwise><img src="../graphics/box.png"></c:otherwise></c:choose></td>
			  <td class="statusHistory">
		        <c:choose>
		         <c:when test="${fn:containsIgnoreCase(order.shippingMethod,'ups')}">
		          <a href="http://wwwapps.ups.com/WebTracking/processInputRequest?HTMLVersion=5.0&sort_by=status&tracknums_displayed=10&TypeOfInquiryNumber=T&loc=en_US&InquiryNumber1=<c:out value='${orderStatus.trackNum}' />&AgreeToTermsAndConditions=yes"><c:out value="${orderStatus.trackNum}" /></a>
		         </c:when>
		         <c:when test="${fn:containsIgnoreCase(order.shippingMethod,'fedex')}">
		          <a href="http://fedex.com/Tracking?action=track&language=english&cntry_code=us&mps=y&tracknumbers=<c:out value='${orderStatus.trackNum}' />&ascend_header=1"><c:out value="${orderStatus.trackNum}" /></a>
		         </c:when>
		         <c:when test="${fn:containsIgnoreCase(order.shippingMethod,'usps')}">
		          <a href="http://trkcnfrm1.smi.usps.com/PTSInternetWeb/InterLabelInquiry.do?strOrigTrackNum=<c:out value='${orderStatus.trackNum}' />"><c:out value="${orderStatus.trackNum}" /></a>
		         </c:when>
		         <c:otherwise><c:out value="${orderStatus.trackNum}" /></c:otherwise>
		        </c:choose>			  
        	  </td>
			  <td style="background-color:#FFFFFF"><c:out value="${orderStatus.comments}"/></td>
			  <td style="background-color:#FFFFFF"><c:out value="${orderStatus.username}"/></td>
			</tr>
			</c:if>
			</c:forEach>
			</table>
			
			<form:form commandName="orderStatus" action="invoice.jhtm" onsubmit="return checkMessage()">
			<input type="hidden" name="order" value="<c:out value="${order.orderId}"/>" >
			<form:hidden path="orderId" />
			<table align="left" border="0" class="updateStatus">
			<c:if test="${showStatusOnApproval}" > <%-- Show status, sub status,tracking only if there is reply from all the managers --%>
				<c:choose>
				  <c:when test="${!((order.status == 's') || (order.status == 'x') || (order.status == 'xap' or order.status == 'ano' or order.status == 'xaf'))}">
				  <tr>
				    <td>
				      <label><fmt:message key="status"/>:</label>
				    </td>
				    <td align="right"> 
				      <form:select path="status" onchange="toggleStatus(this)">
				      <c:choose>
				      <c:when test="${order.status == 'ars'}">
				        <form:option value="ars"><fmt:message key="orderStatus_ars" /></form:option>
				        <form:option value="xaf"><fmt:message key="orderStatus_xaf" /></form:option>
				        <c:if test="${!order.processed or order.fulfilled}" >
				          <form:option value="s"><fmt:message key="orderStatus_s" /></form:option>
				        </c:if>
				      </c:when>
				      <c:otherwise>
				        <form:option value="xq"><fmt:message key="orderStatus_xq" /></form:option>
				        <form:option value="p"><fmt:message key="orderStatus_p" /></form:option>
				        <form:option value="pr"><fmt:message key="orderStatus_pr" /></form:option>
				        <c:if test="${!order.processed or order.fulfilled}" >
				          <form:option value="s"><fmt:message key="orderStatus_s" /></form:option>
				          <form:option value="x"><fmt:message key="orderStatus_x" /></form:option>
				        </c:if>
				        <c:if test="${order.status == 'xp'}">
				          <form:option value="xp"><fmt:message key="orderStatus_xp" /></form:option>
				        </c:if>
				        <c:if test="${order.status == 'xnc'}">
				          <form:option value="xnc"><fmt:message key="orderStatus_xnc" /></form:option>
				        </c:if>
				        <c:if test="${order.status == 'xeb'}">
				          <form:option value="xeb"><fmt:message key="orderStatus_xeb" /></form:option>
				        </c:if>
				        <c:if test="${order.status == 'xg'}">
				          <form:option value="xg"><fmt:message key="orderStatus_xg" /></form:option>
				        </c:if>			        
				        <c:if test="${order.status == 'xge'}">
				          <form:option value="xge"><fmt:message key="orderStatus_xge" /></form:option>
				        </c:if>
				      </c:otherwise>
				      </c:choose>
				      </form:select>
					</td>
				  </tr>
				  </c:when>
				  <c:otherwise>
				    <form:hidden path="status"/>
				  </c:otherwise>
				</c:choose>
				<c:if test="${siteConfig['SUB_STATUS'].value != '0'}">
				  <tr>
					<td>
				      <label><fmt:message key="subStatus"/>:</label>
				    </td>
				    <td align="right">
				      <form:select path="subStatus">
				        <form:option value=""><fmt:message key="orderSubStatus_" /></form:option>
				        <c:forEach begin="0" end="${siteConfig['SUB_STATUS'].value}" var="value">
				          <form:option value="${value}"><fmt:message key="orderSubStatus_${value}" /></form:option>
				        </c:forEach>
				      </form:select>
					</td>
				  </tr>
				</c:if>
				<c:if test="${fn:contains(siteConfig['CUSTOMIZE_ORDER_LIST'].value,'priority')}">
				  <tr>
					<td>
				      <label><fmt:message key="priority"/>:</label>
				    </td>
				    <td align="right">
				      <c:forEach begin="0" end="2" var="priority">
		  			  <form:radiobutton value="${priority}" path="priority" /><img src="../graphics/priority_${priority}.png" border="0">&nbsp;&nbsp;&nbsp;
				      </c:forEach>
					</td>
				  </tr>
				</c:if>
				<c:if test="${fn:contains(siteConfig['CUSTOMIZE_ORDER_LIST'].value,'deliveryPerson')}">
				  <tr>
				    <td>
				      <label><fmt:message key="deliveryPerson"/>:</label>
				    </td>
				    <td align="right">  
				      <form:input path="deliveryPerson" maxlength="50" htmlEscape="true"/>
					</td>
				  </tr>
				</c:if>
				  <tr>
				    <td>
				      <label><fmt:message key="trackNum"/>:</label>
				    </td>
				    <td align="right">  
				      <form:input path="trackNum" />
					</td>
				  </tr>
			</c:if>
			  <tr>
			    <td colspan="2"><fmt:message key="comments"/>:</td>
			  </tr>
			  <tr>
				<td colspan="2"><form:textarea path="comments" rows="5" cols="80" htmlEscape="true"/></td>
			  </tr>
			  <tr>
			    <td>
			      <label>Notify Customer:</label>
			      <form:checkbox path="userNotified" onclick="toggleMessage()"/>
			    </td> 
			    <td height="30" align="right">
			      <div id="messageSelect" style="display:none;">
			      <select id="messageSelected" onChange="chooseMessage(this)">
			        <option value="">choose a message</option>
				  <c:forEach var="message" items="${messages}">
				    <option value="${message.messageId}"><c:out value="${message.messageName}"/></option>
				  </c:forEach>
				  </select>
				  <c:if test="${siteConfig['CUSTOM_SHIPPING_CONTACT_INFO'].value == 'true'}">
				  <select name="contact" onChange="insertContact(this)">
			        <option value="">choose a contact</option>
				  <c:forEach var="contact" items="${contacts}">
				    <option value="${contact.id}"><c:out value="${contact.company}"/></option>
				  </c:forEach>
				  </select>
				  <c:forEach var="contact" items="${contacts}">
			  	   <input type="hidden" id="contact_company_${contact.id}" value="${contact.company}" />
			      </c:forEach>
			      </c:if>
				  </div>
				</td>
			  </tr>
			  <tr>
				<td colspan="2">
				  <div id="messageBox" style="display:none;">
				  <table class="notifyCustomer">
				    <c:if test="${gSiteConfig['gPDF_INVOICE']}">
				    <tr>
				      <td align="right"><fmt:message key="attach"/> <fmt:message key="invoice"/>:&nbsp;&nbsp;</td>
				      <td><input type="checkbox" name="__attach" value="true"></td>
				    </tr>
				    </c:if>
				    <tr>
				      <td align="right"><fmt:message key="to"/>:&nbsp;&nbsp;</td>
				      <td><c:out value="${customer.username}"/></td>
				    </tr>
				    <tr>
				      <td align="right"><fmt:message key="from"/>:&nbsp;&nbsp;</td>
				      <td><form:input path="from" size="50" htmlEscape="true"/></td>
				    </tr>
				    <tr>
				      <td align="right">Cc:&nbsp;&nbsp;</td>
				      <td>
				        <form:input path="cc" size="50" htmlEscape="true"/>
				      </td>
				    </tr>
				    <tr>
				      <td align="right">Bcc:&nbsp;&nbsp;</td>
				      <td><c:choose><c:when test="${multiStore != null}"><c:out value="${multiStore.contactEmail}"/></c:when><c:otherwise><c:out value="${siteConfig['CONTACT_EMAIL'].value}"/></c:otherwise></c:choose></td>
				    </tr>
				    <tr>
				      <td align="right"><fmt:message key="subject"/>:&nbsp;&nbsp;</td>
				      <td>
				        <form:input id="subject" path="subject" size="50" maxlength="50"/>
				  		<c:forEach var="message" items="${messages}">
				    	<input type="text" disabled style="display:none;" id="subject${message.messageId}" name="subject" value="<c:out value="${message.subject}"/>" size="50" maxlength="50">
				  		</c:forEach>
				      </td>
				    </tr>
				    <tr>
				      <td align="right">Html:&nbsp;&nbsp;</td>
				      <td>
				        <form:checkbox path="htmlMessage" />
				  		<c:forEach var="message" items="${messages}">
				    	<input type="hidden" id="htmlID${message.messageId}" value="<c:out value="${message.html}"/>" />
				  		</c:forEach>
				      </td>
				    </tr>
				  </table>
				  <table class="notifyCustomerMessage">  
				    <tr>
				      <td>
				        <form:textarea id="message" path="message" cssClass="notifyMessageBox"/>
				  		<c:forEach var="message" items="${messages}">
				    	<textarea disabled style="display:none;" id="message${message.messageId}" name="message" class="notifyMessageBox"><c:out value="${message.message}"/></textarea>
				  		</c:forEach>
				      </td>
				    </tr>	    
				  </table>
				  </div>
				</td>
			  </tr>
			  <tr>
				<td colspan="2">
				<sec:authorize ifAnyGranted="ROLE_AEM,ROLE_ADMIN_${prop_site_id},ROLE_INVOICE_STATUS">
				<div class="button">
				  <input type="submit" name="__status" value="Update Status" onclick="return checkStatus();">
				  <input type="reset" value="Reset" onclick="document.getElementById('userNotified1').checked=false;toggleMessage()">
				</div>  
				</sec:authorize>  
				</td>
			  </tr>
			</table> 
			</form:form>
		    <!-- end input field -->  	  

		  	<!-- start button -->			  	
            <!-- end button -->	
	        </div>
	  	
	<!-- end tab -->        
	</div>
	
	<c:if test="${gSiteConfig['gBUDGET'] and fn:length(order.actionHistory) gt 0}">
	<h4 title="Approval History">Approval History</h4><div>
	<!-- start tab -->
	  	<div class="listdivi ln tabdivi"></div>
	  	<div class="listdivi"></div>
	   
		  	<div class="list${classIndex % 2}"><c:set var="classIndex" value="${classIndex+1}" />
		  	<div class="listpp">
		  	<!-- input field -->
			<table border="0" cellpadding="2" cellspacing="1" width="100%" class="actionHistory">
			<tr>
			  <th class="actionHistory">Action By</th>
			  <th class="actionHistory">Type</th>
			  <th class="actionHistory">Date</th>
			  <th class="actionHistory">Note</th>
			</tr>
			<c:forEach var="order" items="${order.actionHistory}">
			<tr valign="top">
			  <td class="actionHistory" style="white-space: nowrap"><c:out value="${order.actionByName}"/></td>
			  <td class="actionHistory" style="white-space: nowrap"><c:out value="${order.actionType}"/></td>
			  <td class="status" align="center" style="white-space: nowrap"><fmt:formatDate type="both" timeStyle="full" value="${order.actionDate}"/></td>
			  <td class="actionHistory" align="center"><c:out value="${order.actionNote}"/></td>
			</tr>
			</c:forEach>
			</table>
			<!-- end input field -->   	
		  	</div>
		  	</div>

	<!-- end tab -->        
	</div> 
	</c:if>	

<!-- end tabs -->			
</div>
</c:if>  
  <!-- end table -->
   </td><td class="boxmidr" ></td></tr>
  <tr><td class="botl"></td><td class="botr"></td></tr>
  </table>
  
<!-- end main box -->  
</div>

</tiles:putAttribute>    
</tiles:insertDefinition>