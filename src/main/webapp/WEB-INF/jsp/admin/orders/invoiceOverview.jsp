<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
  <head>
    <title>Invoice</title>
    <link href="<c:url value="/assets/invoice.css"/>" rel="stylesheet" type="text/css"/>
  </head>  
<body class="invoice">
<div style="width:700pt;margin:left;">


<c:if test="${order != null}">
<div align="right"><a href="javascript:window.print()"><img border="0" src="../graphics/printer.png"></a></div>
<!-- <c:out value="${invoiceLayout.headerHtml}" escapeXml="false"/> -->

<table border="0" width="100%" cellspacing="0" cellpadding="0" style="border:1px solid #555555;">
<tr valign="top">
<td colspan="2" width="30%">
  <table width="100%">
   <tr>
    <td style="background-color:#000000;"><b style="color:#ffffff;"><fmt:message key="billingInformation" />:</b></td>
   </tr>
   <tr>
    <td>
	 <c:set value="${order.billing}" var="address"/>
	 <c:set value="true" var="billing" />
	 <div class="billingAddress">
	 <%@ include file="/WEB-INF/jsp/frontend/common/address.jsp" %>
	 </div>
	 <div class="emailAddress">
       <c:out value="${customer.username}" />
     </div>
	 <c:set value="false" var="billing" />
	</td>
   </tr>
   <tr>
    <td>
     <table style="border-top:1px solid #222222;" width="100%" border="0" cellspacing="0" cellpadding="0">
      <tr>
       <td>Order Date</td>
       <td><fmt:formatDate type="both" timeStyle="full" pattern="MM/dd/yyyy" value="${order.dateOrdered}"/></td>
      </tr>
      <tr>
       <td>Payment Type:</td>
       <td><c:out value="${order.paymentMethod}" /></td>
      </tr>
      <c:if test="${order.purchaseOrder != '' and order.purchaseOrder != null}" >
      <tr>
       <td><fmt:message key="purchaseOrder" />:</td>
       <td><c:out value="${order.purchaseOrder}" /></td>
      </tr>
      </c:if>
      <tr>
       <td>SubTotal</td>
       <td><fmt:message key="${siteConfig['CURRENCY'].value}" /><fmt:formatNumber value="${order.subTotal}" pattern="#,##0.00"/></td>
      </tr>
      <tr>
       <td>Taxes</td>
       <td><fmt:message key="${siteConfig['CURRENCY'].value}" /><fmt:formatNumber value="${order.tax}" pattern="#,##0.00"/></td>
      </tr>
      <tr>
        <td>Shipping</td>
        <td><fmt:message key="${siteConfig['CURRENCY'].value}" /><fmt:formatNumber value="${order.shippingCost}" pattern="#,##0.00"/></td>
      </tr>
      <tr>
        <td>GrandTotal</td>
        <td><fmt:message key="${siteConfig['CURRENCY'].value}" /><fmt:formatNumber value="${order.grandTotal}" pattern="#,##0.00"/></td>
      </tr> 
      <tr>
        <td>Previous Orders</td>
        <td><c:out value="${customer.orderCount}"/></td>
      </tr> 
     </table>
	</td>
   </tr>
  </table>		
</td>

<td style="border-left:2px solid #000000; border-right:2px solid #000000;" width="40%">
<table width="100%" border="0" class="overviewInvoice1" cellspacing="0" cellpadding="0">
  <tr>
    <td align="center" colspan="2">
      <table width="100%" cellspacing="0" cellpadding="0">
        <tr valign="top">
          <td>DUE DATE</td>        
        </tr>
        <tr valign="top">
          <td align="center">
           <table width="100%" cellspacing="0" cellpadding="0"><tr align="center">
			<c:choose><c:when test="${order.dueDateDay == 'M'}"><td style="padding-left:10px;"><img style="height:40px;width:40px;float:center" src="../../assets/Image/Layout/mCircle.gif" /></td></c:when><c:otherwise><td style="padding-left:10px;"><img style="height:40px;width:40px;float:center" src="../../assets/Image/Layout/m.gif" /></td></c:otherwise></c:choose>
            <c:choose><c:when test="${order.dueDateDay == 'T'}"><td style=""><img style="height:40px;width:40px;float:center" src="../../assets/Image/Layout/tCircle.gif" /></td></c:when><c:otherwise><td><img style="height:40px;width:40px;float:center" src="../../assets/Image/Layout/t.gif" /></td></c:otherwise></c:choose>
            <c:choose><c:when test="${order.dueDateDay == 'W'}"><td style=""><img style="height:40px;width:40px;float:center" src="../../assets/Image/Layout/wCircle.gif" /></td></c:when><c:otherwise><td><img style="height:40px;width:40px;float:center" src="../../assets/Image/Layout/w.gif" /></td></c:otherwise></c:choose>
            <c:choose><c:when test="${order.dueDateDay == 'Th'}"><td style=""><img style="height:40px;width:40px;float:center" src="../../assets/Image/Layout/tCircle.gif" /></td></c:when><c:otherwise><td><img style="height:40px;width:40px;float:center" src="../../assets/Image/Layout/t.gif" /></td></c:otherwise></c:choose>
            <c:choose><c:when test="${order.dueDateDay == 'F'}"><td style="padding-right:10px;"><img style="height:40px;width:40px;float:center" src="../../assets/Image/Layout/fCircle.gif" /></td></c:when><c:otherwise><td style="padding-right:10px;"><img style="height:40px;width:40px;float:center" src="../../assets/Image/Layout/f.gif" /></td></c:otherwise></c:choose>
			</tr></table>
           </div>
          <div style="font-size:20px;font-weight:700;color:#990000;"><fmt:formatDate type="both" timeStyle="full" pattern="MM/dd/yyyy" value="${order.dueDate}"/></div></td>
        </tr>
      </table>
    </td>
  </tr>
  <tr style="background-color:#cccccc">
    <td align="center" width="%100" style="border-top:1px solid #222222;border-right:1px solid #222222;"><fmt:message key="invoice"/> #</td>
  </tr>
  <tr class="orderNumber">
    <td align="center" style="border-top:1px solid #222222;border-right:1px solid #222222;height:50px;">&nbsp;&nbsp;</td>
  </tr>
  <tr>
    <td align="center" style="border-top:1px solid #222222;" colspan="2">
     <center><b>netTrophy.com</b></center>
     6122 Beach Bl Buena Park CA 90621<br />
     P (888) 707-5111   F (714) 670-0301<br />
     netTrophy@aol.com
    </td>
  </tr>
  <tr>
    <td style="border-top:1px solid #222222;" colspan="2">
      <table cellspacing="0" cellpadding="0">
        <tr>
          <td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
          <td>PARTS ORDERED FROM <span style="margin-left:80px;"> DATE</span></td>
        </tr>
        <tr>
          <td colspan="2">STOCK</td>
        </tr>
        <tr>
          <td colspan="2">PULLED</td>
        </tr>
      </table>
    </td>
  </tr>
  <tr>
    <td style="border-top:1px solid #222222;" colspan="2">
      <table width="100%" cellspacing="0" cellpadding="0">
        <tr>
          <td>BP/NT</td>
          <td>CUSTOMERS</td>
          <td>NONE</td>
        </tr>
      </table>
    </td>
  </tr>
  <tr>
    <td align="center" style="border-top:1px solid #222222;" colspan="2">
      <table width="100%" cellspacing="0" cellpadding="0">
        <tr>
          <td align="center">LASER</td>
          <td align="center">GLASS</td>
          <td align="center">SUBTN</td>
          <td align="center">TROPHY</td>
        </tr>
        <tr>
          <td align="center">VISNS</td>
          <td align="center">VINYL</td>
          <td align="center">BANNER</td>
          <td align="center">OTHER</td>
        </tr>
      </table>
    </td>
  </tr>
  <c:if test="${gSiteConfig['gINVOICE_APPROVAL'] and !empty order.approval}">
    <td><fmt:message key="approval"/></td>
    <td><fmt:message key="approvalStatus_${order.approval}" /></td>
  </c:if>
</table>
</td>

<td colspan="4" width="30%">
	<table width="100%">
    <tr>
      <td colspan="2" style="background-color:#000000;"><b style="color:#ffffff;"><fmt:message key="shippingInformation" /></b></td>
    </tr>
    <tr align="center">
      <td colspan="2" style="border-bottom:1px solid #000000;color:#000000;font-size:22px;font-weight:700;" ><span><c:out value="${order.orderId}"/></span></td>
    </tr>

	<tr>
	  <td colspan="2">
	    <c:set value="${order.shipping}" var="address"/>
	    <div class="shippingAddress">
	    <%@ include file="/WEB-INF/jsp/frontend/common/address.jsp" %>
	    </div>
	  </td>  
	</tr>
	<tr>
	  <td><span style="text-decoration: underline;"> Ship By: </span></td>  
	</tr>
	<tr>
	  <td> <b style="font-size:20px;"><c:out value="${order.shippingMethod}" escapeXml="false"/></b> </td>  
	</tr>
	<tr>
	  <td><span style="text-decoration: underline;"> Address Type: </span></td>  
	</tr>
	<tr>
	  <td> <b style="font-size:20px;"><c:out value="${address.resToString}"/></b> </td>  
	</tr>
	</table>
</td>
</tr>



<tr>
 <td colspan="7" style="white-space:nowrap;border-top:1px solid #555555;">
 <table class="infoBox" width="100%" cellpadding="0" cellspacing="0">
 <tr valign="top">
  <td style="white-space:nowrap;width:50px;border-right:1px solid #555555;padding: 0 10px 15px 0;">TAKEN BY</td>
  <td style="white-space:nowrap;width:50px;border-right:1px solid #555555;padding: 0 10px 15px 0;">ARTWORK BY</td>
  <td colspan="3" style="width:650px;border-right:1px solid #555555;padding: 0 10px 15px 0;">
   <div style="float:left">PROOF</div>
   <div style="float:right">CUSTOMER<br />APPROVED</div>
  </td>
  <td style="white-space:nowrap;width:50px;padding: 0 10px 15px 0;">CHECKED BY</td>
 </tr>
 <tr style="height: 50px;" valign="top">
  <td colspan="2" style="border-right:1px solid #555555;border-top:4px solid #555555;padding: 0 10px 15px 0;">ATTACHMENTS
  <c:if test="${zipFile}"><br />
   <span style="font-size:14px;" ><a href="<c:url value="/assets/orders_zip/${order.orderId}.zip"/>"><c:out value="${order.orderId}"/>.zip</a></span>
  </c:if>
  </td>
  <td style="white-space:nowrap;width:300px;border-right:1px solid #555555;border-top:4px solid #555555;padding: 0 10px 15px 0;">JOB COMPLETED BY</td>
  <td style="white-space:nowrap;width:50px;border-right:1px solid #555555;border-top:4px solid #555555;padding: 0 10px 15px 0;">JOB CHECKED BY</td>
  <td style="white-space:nowrap;width:50px;border-right:1px solid #555555;border-top:4px solid #555555;padding: 0 10px 15px 0;">SHIPPED BY</td>
  <td style="white-space:nowrap;border-top:4px solid #555555;padding: 0 10px 15px 0;">SHIPPED DATE</td>
 </tr>
 </table>
 </td>
</tr> 

<tr valign="top">
<td colspan="7" style="font-size:12px;height:70pt;border-top:1px solid #555555;">
	Comments:<br /><c:out value="${order.invoiceNote}" escapeXml="false"/>
</td>
</tr>
<tr>
<td colspan="7" style="border-top:1px solid #555555;"><h2>Overview</h2></td>
</tr>
<tr>
<td colspan="7" style="border-top:1px solid #555555;">
	<table border="0" cellpadding="2" cellspacing="1" width="100%" style="border:2px solid #666666;">
	<c:forEach var="lineItem" items="${order.lineItems}" varStatus="status">
	<c:if test="${status.index % 4 == 0}" >
	<tr valign="top">
	</c:if>
	  <td width="200" style="border:1px solid #666666;">
	    <table width="200" cellpadding="0" cellspacing="0">
	     <tr>
	       <td valign="top">
	         <c:if test="${siteConfig['SHOW_IMAGE_ON_INVOICE'].value == 'true' and lineItem.product.thumbnail != null}">
		       <img class="invoiceImage" border="0" <c:if test="${!lineItem.product.thumbnail.absolute}"> src="<c:url value="/assets/Image/Product/thumb/${lineItem.product.thumbnail.imageUrl}"/>" </c:if> <c:if test="${lineItem.product.thumbnail.absolute}"> src="<c:url value="${lineItem.product.thumbnail.imageUrl}"/>" </c:if> />
		     </c:if>
	       </td>
	       <td valign="top">
	         <table cellpadding="0" cellspacing="0">
	           <tr><td>Qty</td><td>&nbsp;&nbsp;:&nbsp;&nbsp;</td><td><c:out value="${lineItem.quantity}"/></td></tr>
			  <c:forEach items="${lineItem.productAttributes}" var="productAttribute" varStatus="productAttributeStatus">
			   <c:if test="${fn:toLowerCase(productAttribute.optionName) == 'size'}">
				<tr><td><c:out value="${productAttribute.optionName}"/></td><td>&nbsp;&nbsp;:&nbsp;&nbsp;</td><td><c:out value="${productAttribute.valueString}"/></td></tr>
			   </c:if>	
			  </c:forEach>
			  <%--
	          <c:forEach items="${productFieldsHeader}" var="productField">
	   		   <c:if test="${productField.showOnInvoiceBackend and productField.name == 'size'}">
				<tr><td><c:out value="${productField.name}"/>:</td><td><c:out value="${productField.value}"/></td></tr>
			    </c:if>       
			  </c:forEach>
			  --%>
	           <tr><td>ID</td><td>&nbsp;&nbsp;:&nbsp;&nbsp;</td><td><c:out value="${lineItem.product.sku}"/></td></tr>
	         </table>    
	       </td>
	     </tr>
	    </table>
	  </td>
	<c:if test="${status.index % 4 == 3}" >  
	</tr>  
	</c:if>
	</c:forEach>
	</table>
</td>
</tr>

<tr>
<td colspan="7" style="border-top:1px solid #555555;"><h2>Line Items</h2></td>
</tr>

<tr>
 <td colspan="7" class="lineItems">
  <table border="1" width="100%" cellpadding="0" cellspacing="0" class="invoice">
    <tr>
    <c:set var="cols" value="0"/>
    <th class="invoice"><fmt:message key="productSku" /></th>
    <th class="invoice"><fmt:message key="productName" /></th>
    <c:forEach items="${productFieldsHeader}" var="productField">
      <c:if test="${productField.showOnInvoice or productField.showOnInvoiceBackend}">
      <th class="invoice"><c:out value="${productField.name}" /></th><c:set var="cols" value="${cols+1}"/>
      </c:if>
    </c:forEach>
    <th width="10%" class="invoice"><fmt:message key="quantity" /></th>
    <c:if test="${gSiteConfig['gINVENTORY'] && order.hasLowInventoryMessage}">
	  <c:set var="cols" value="${cols+1}"/>
	  <th width="10%" class="invoice"><fmt:message key="lowInventory" /></th>
	</c:if>
    <c:if test="${order.hasPacking}">
	  <c:set var="cols" value="${cols+1}"/>
      <th width="10%" class="invoice"><fmt:message key="packing" /></th>
    </c:if>
    <c:if test="${order.hasContent}">
	  <c:set var="cols" value="${cols+1}"/>
      <th class="invoice"><fmt:message key="content" /></th>
	  <c:if test="${!empty siteConfig['EXT_QUANTITY_TITLE'].value}">
       <c:set var="cols" value="${cols+1}"/>
       <th class="invoice"><c:out value="${siteConfig['EXT_QUANTITY_TITLE'].value}" /></th>
      </c:if>
    </c:if>
    <th class="invoice"><fmt:message key="productPrice" /><c:if test="${order.hasContent}"> / <c:out value="${siteConfig['CASE_UNIT_TITLE'].value}" /></c:if></th>
    <th class="invoice"><fmt:message key="total" /></th>
   </tr>
   <c:forEach var="lineItem" items="${order.lineItems}" varStatus="status">
	<tr valign="top">
	  <td class="invoice">
	    <c:if test="${siteConfig['SHOW_IMAGE_ON_INVOICE'].value == 'true' and lineItem.product.thumbnail != null}">
		  <img class="invoiceImage" border="0" <c:if test="${!lineItem.product.thumbnail.absolute}"> src="<c:url value="/assets/Image/Product/thumb/${lineItem.product.thumbnail.imageUrl}"/>" </c:if> <c:if test="${lineItem.product.thumbnail.absolute}"> src="<c:url value="${lineItem.product.thumbnail.imageUrl}"/>" </c:if> /><br />
		</c:if><div class="sku"><c:out value="${lineItem.product.sku}"/></div>
	  </td>
	  <td class="invoice"><span class="overview_item_name"><c:out value="${lineItem.product.name}"/></span>
	    <table border="0" cellspacing="1" cellpadding="0">
		  <c:forEach items="${lineItem.productAttributes}" var="productAttribute" varStatus="productAttributeStatus">
			<tr class="invoice_lineitem_attributes${productAttributeStatus.count%2}">
			<td class="optionName" align="right"><c:out value="${productAttribute.optionName}"/>: </td>
			<td class="optionValue" style="padding-left:5px;"><c:out value="${productAttribute.valueString}" escapeXml="false"/></td>
			<c:if test="${(! empty productAttribute.optionPriceOriginal) && productAttribute.optionPriceOriginal != 0}">
			<td class="optionPrice"><fmt:formatNumber value="${productAttribute.optionPriceOriginal}" pattern="#,##0.00" /></td>
			<td class="optionNote" style="padding-left:5px;"><fmt:message key="${productAttribute.optionPriceMessageF}"/></td>
			</c:if>
			</tr>
		  </c:forEach>
		  <%@ include file="/WEB-INF/jsp/frontend/common/customLinesInvoice.jsp"%>
	   </table>
	   <c:if test="${lineItem.subscriptionInterval != null}">
		  <c:set var="intervalType" value="${fn:substring(lineItem.subscriptionInterval, 0, 1)}"/>
		  <c:set var="intervalUnit" value="${fn:substring(lineItem.subscriptionInterval, 1, 2)}"/>
		  <div class="invoice_lineitem_subscription">
			<fmt:message key="subscription"/> <fmt:message key="details"/> : <fmt:message key="delivery"/>
			<c:choose>
			  <c:when test="${intervalUnit == '1'}"><fmt:message key="every_${intervalType}"><fmt:param value="${intervalUnit}"/></fmt:message></c:when>
			  <c:otherwise><fmt:message key="every_${intervalType}s"><fmt:param value="${intervalUnit}"/></fmt:message></c:otherwise>
			</c:choose>
		  </div>
		  <div class="invoice_lineitem_subscription"><c:out value="${lineItem.subscriptionCode}"/></div>
	  </c:if>
	  <c:if test="${siteConfig['SHOW_IMAGE_ON_INVOICE'].value == 'true' and lineItem.hasProductAttributesImage}" >
	  <c:forEach items="${lineItem.productAttributes}" var="productAttribute">
	    <c:if test="${!empty productAttribute.imageUrl}" > 
	      <c:if test="${!productAttribute.absolute}"><img src="<c:url value="/assets/Image/Product/options/${productAttribute.imageUrl}"/>" border="0" class="invoiceOptionImage" /></c:if>
	      <c:if test="${productAttribute.absolute}"><img src="<c:url value="${productAttribute.imageUrl}"/>" border="0" class="invoiceOptionImage" /></c:if>
	    </c:if>
	  </c:forEach>
	  </c:if> 
	  </td>
	  <c:forEach items="${productFieldsHeader}" var="pFHeader" varStatus="status"><c:set var="check" value="0"/>
	  <c:if test="${pFHeader.showOnInvoice or pFHeader.showOnInvoiceBackend}">
	  <c:forEach items="${lineItem.productFields}" var="productField"> 
	   <c:if test="${pFHeader.id == productField.id}">
	    <td class="invoice"><c:out value="${productField.value}" /></td><c:set var="check" value="1"/>
	   </c:if>       
	  </c:forEach>
	  <c:if test="${check == 0}">
	    <td class="invoice">&nbsp;</td>
	   </c:if>
	  </c:if>
	  </c:forEach>
	  <td class="invoice" align="center"><c:out value="${lineItem.quantity}"/></td>
	  <c:if test="${gSiteConfig['gINVENTORY'] && order.hasLowInventoryMessage}">
		<td class="invoice" align="center"><c:out value="${lineItem.lowInventoryMessage}" /></td>
	  </c:if>
	  <c:if test="${order.hasPacking }">
	  <td class="invoice" align="center"><c:out value="${lineItem.product.packing}" /></td>
	  </c:if>
	  <c:if test="${order.hasContent}">
	    <td class="invoice" align="center"><c:out value="${lineItem.product.caseContent}" /></td>
	    <c:if test="${!empty siteConfig['EXT_QUANTITY_TITLE'].value}">
	    <td class="invoice" align="center">
	     <c:choose>
	      <c:when test="${!empty lineItem.product.caseContent}"><c:out value="${lineItem.product.caseContent * lineItem.quantity}" /></c:when>
	      <c:otherwise><c:out value="${lineItem.quantity}" /></c:otherwise> 
	     </c:choose>
	    </td>
	    </c:if>
	  </c:if>
	  <td class="invoice" align="right"><fmt:formatNumber value="${lineItem.unitPrice}" pattern="#,##0.00#" maxFractionDigits="${siteConfig['MAX_FRACTION_DIGIT'].value}"/></td>  
	  <td class="invoice" align="right"><fmt:formatNumber value="${lineItem.totalPrice}" pattern="#,##0.00"/></td>
	</tr>  
	  <c:if test="${lineItem.serialNums != null}">
	  <tr bgcolor="#FFFFFF">
	    <td colspan="${6+cols}">
	      <table cellspacing="3">
	        <tr>
	          <td valign="top">S/N:</td>
	          <td><c:forEach items="${lineItem.serialNums}" var="serialNum" varStatus="status"><c:if test="${not status.first}">, </c:if><c:out value="${serialNum}"/></c:forEach></td>
	        </tr>
	      </table>
	    </td>
	  </tr>			
	  </c:if>
	</c:forEach>
   
  </table>
 </td>
</tr>

</table>

</c:if>
</div>

</body>
</html>