<%@page import="com.webjaguar.model.LineItem,org.jdom.input.SAXBuilder,org.jdom.*,java.io.StringReader,java.util.*"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>

<style type="text/css">
<!--
.style1 {	
	font-family: Verdana, Arial, Helvetica, sans-serif; 
	font-size:9pt;
	font-weight: bold;
}
.style2 {	
	font-family: Verdana, Arial, Helvetica, sans-serif; 
	text-align: center;
	font-size:9pt;
	background-color: #919191;
	color: #FFFFFF;
}
.style3 {	
	font-family: Verdana, Arial, Helvetica, sans-serif; 
	text-align: right;
	font-size:9pt;
}
.style4 {	
	font-family: Verdana, Arial, Helvetica, sans-serif; 
	font-size:9pt;
	white-space: nowrap;
}
-->
</style>
<table width="640" align="center">
<tr>
<td>
<img src="../../assets/customImages/${param.orderId}/${customFrame.customImageUrl}" border="0"/>
</td>
</tr>
<tr>
<td>
<%
LineItem customFrame = (LineItem) request.getAttribute("customFrame");

if (customFrame != null && customFrame.getCustomXml() != null) {

	SAXBuilder builder = new SAXBuilder(false);
					
	Document doc = builder.build(new StringReader(customFrame.getCustomXml()));
		
	List<Element> elements = doc.getRootElement().getChildren();
	
	out.println("<div class='style1'>ID: " + doc.getRootElement().getChild("productID").getValue() + ", ");
	out.println("Type: " + doc.getRootElement().getChild("productType").getValue() + "</div>");
	
	for (Element element: elements) {
		if (element.getName().contains("Details")) {
			out.println("<div style='float:left;'><table cellspacing='1' width='210'>");
			List<Element> children = element.getChildren();
			if (children.size() > 0) {
				for (Element child: children) {
				    out.println("<tr>");
					if (child.getName().equals("title")) {
						out.println("<td class='style2' colspan='2'>" + child.getValue() + "</td>");
					} else {
						out.println("<td class='style3' width='80'>" + child.getName() + ":</td><td class='style4' width='130'>" + child.getValue() + "</td>");
					}
				}
		   		out.println("</tr>");
			}
			out.println("</table></div>");
		}
	}		
}
%>
</td>
</tr>
</table>

