<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
  <head>
    <title>
      <c:choose>
        <c:when test="${order.status == 'xq'}"><fmt:message key="quote" /></c:when>
        <c:otherwise><fmt:message key="invoice" /></c:otherwise>
      </c:choose>
    </title>
    <link href="<c:url value="/assets/invoice.css"/>" rel="stylesheet" type="text/css"/>
  </head>  
<body class="invoice">
<div>


<c:if test="${order != null}">
<div align="right"><a href="javascript:window.print()"><img border="0" src="../graphics/printer.png"></a></div>
<c:choose>
  <c:when test="${order.status == 'xq'}"><h4>  <fmt:message key="quote" />  </h4></c:when>
  <c:otherwise><c:out value="${invoiceLayout.headerHtml}" escapeXml="false"/></c:otherwise>
</c:choose>



<div align="right" id="invoiceInfoId">
<table cellpadding="0" cellspacing="0">
  <tr>
    <td>
      <c:choose>
        <c:when test="${order.status == 'xq'}"><fmt:message key="quote"/></c:when>
        <c:otherwise> <fmt:message key="order"/> </c:otherwise>
      </c:choose> #
    </td>
    <td>:&nbsp;&nbsp;</td>
    <td><b><c:out value="${order.orderId}"/></b></td>
  </tr>
  <tr>
    <td><fmt:message key="order" />&nbsp;<fmt:message key="date" /></td>
    <td>:&nbsp;&nbsp;</td>
    <td><fmt:formatDate type="date" timeStyle="default" value="${order.dateOrdered}"/></td>
  </tr>
  <tr>
    <td><fmt:message key="account" />#</td>
    <td>:&nbsp;&nbsp;</td>
    <td><c:out value="${customer.accountNumber}"/></td>
  </tr>
</table>
</div>

<div id="invoiceAddrWrapperId" >
  <table border="0" width="100%" cellspacing="3" cellpadding="1">
	<tr valign="top">
	  <td>
		<b><fmt:message key="soldTo" />:</b>
		<c:set value="${order.billing}" var="address"/>
		<c:set value="true" var="billing" />
		<%@ include file="/WEB-INF/jsp/frontend/common/address.jsp" %>
		<c:set value="false" var="billing" />
	  </td>
	  <td>&nbsp;</td>
	  <td>
		<b><c:choose><c:when test="${order.workOrderNum != null}"><fmt:message key="service" /> <fmt:message key="location" /></c:when><c:otherwise><fmt:message key="shipTo" /></c:otherwise></c:choose>:</b>
		<c:set value="${order.shipping}" var="address"/>
		<%@ include file="/WEB-INF/jsp/frontend/common/address.jsp" %>
		<c:if test="${not empty workOrder.service.alternateContact}">	
		  <fmt:message key="alternateContact" />: <c:out value="${workOrder.service.alternateContact}"/>
		</c:if>	
	  </td>
	</tr>
</table>
</div>
<br/>
<div class="pickingSheetInfoWrapper">
  
  <div class="pickingSheetInfo">
    <div class="pickingSheetInfoTitle"><fmt:message key="salesRep"/></div>
    <div class="pickingSheetInfoValue"><c:out value="${salesRep.name}"/></div>
  </div>
  
  <div class="pickingSheetInfo">
    <div class="pickingSheetInfoTitle"><fmt:message key="purchaseOrder" /></div>
    <div class="pickingSheetInfoValue"><c:out value="${order.purchaseOrder}" /></div>
  </div>
  
  <div style="clear: both;"></div>
  <hr /> 
  
  <div class="pickingSheetInfo">
    <div class="pickingSheetInfoTitle"><fmt:message key="shipVia" /></div>
    <div class="pickingSheetInfoValue"><c:out value="${order.shippingMethod}" /></div>
  </div>
  
  <div class="pickingSheetInfo" >
    <div class="pickingSheetInfoTitle"><fmt:message key="paymentMethod"/>:</div>
    <div class="pickingSheetInfoValue"><c:out value="${order.paymentMethod}" /></div>
  </div>
  
  <div style="clear: both;"></div>
</div>
<br/>
<p>
<c:if test="${order.purchaseOrder != '' and order.purchaseOrder != null}" >
<b><fmt:message key="purchaseOrder" /></b>: <c:out value="${order.purchaseOrder}" />
</c:if>
<c:if test="${order.workOrderNum != null}" >
<br/><b><fmt:message key="workOrder" /> #</b>: <c:out value="${order.workOrderNum}" />
</c:if>
<c:if test="${workOrder != null}" >
<br/><b><c:out value="${siteConfig['SERVICEABLE_ITEM_TITLE'].value}"/> ID/SN</b>: <c:out value="${workOrder.service.item.itemId}" />  / <c:out value="${workOrder.service.item.serialNum}"/>
<br/><b><c:out value="${siteConfig['SERVICEABLE_ITEM_TITLE'].value}"/> SKU/Description</b>: <c:out value="${workOrder.service.item.sku}"/>  / <c:out value="${product.name}"/>
<br/><b><fmt:message key="bwCount"/></b>: <fmt:formatNumber value="${workOrder.bwCount}" pattern="#,##0"/>
<br/><b><fmt:message key="colorCount"/></b>: <fmt:formatNumber value="${workOrder.colorCount}" pattern="#,##0"/>
<br/><b><fmt:message key="problem" /></b>: <c:out value="${workOrder.service.problem}" />
</c:if>	
<table border="0" cellpadding="2" cellspacing="1" width="100%" class="invoice">
  <tr>
    <c:set var="cols" value="0"/>
    <th class="invoice"><fmt:message key="quantity" />/O</th>
    <th class="invoice"><fmt:message key="quantity" />/S</th>
    <th class="invoice"><fmt:message key="productSku" /></th>
    <th class="invoice"><fmt:message key="productName" /></th>
    <c:forEach items="${productFieldsHeader}" var="productField">
      <c:if test="${productField.showOnInvoice or productField.showOnInvoiceBackend}">
      <th class="invoice"><c:out value="${productField.name}" /></th><c:set var="cols" value="${cols+1}"/>
      </c:if>
    </c:forEach>
    <c:if test="${gSiteConfig['gINVENTORY'] && order.hasLowInventoryMessage}">
	  <c:set var="cols" value="${cols+1}"/>
	  <th width="10%" class="invoice"><fmt:message key="lowInventory" /></th>
	</c:if>
    <c:if test="${order.hasPacking}">
	  <c:set var="cols" value="${cols+1}"/>
      <th width="10%" class="invoice"><fmt:message key="packing" /></th>
    </c:if>
  </tr>
<c:forEach var="lineItem" items="${order.lineItems}" varStatus="status">
<tr valign="top">
  <td class="invoice" align="center">
    <c:choose>
      <c:when test="${!empty lineItem.product.caseContent}"><c:out value="${lineItem.product.caseContent * lineItem.originalQuantity}" /></c:when>
      <c:otherwise><c:out value="${lineItem.originalQuantity}" /></c:otherwise> 
    </c:choose>
  </td>
  <td class="invoice openField" align="center">  </td>
  <td class="invoice">
	<c:if test="${lineItem.customImageUrl != null}">
      <a href="../../assets/customImages/${order.orderId}/${lineItem.customImageUrl}" onclick="window.open(this.href,'','width=640,height=480,resizable=yes'); return false;"><img class="invoiceImage" src="../../assets/customImages/${order.orderId}/${lineItem.customImageUrl}" border="0" /></a>
    </c:if>  
    <c:if test="${siteConfig['SHOW_IMAGE_ON_INVOICE'].value == 'true' and lineItem.product.thumbnail != null}">
	  <img class="invoiceImage" border="0" <c:if test="${!lineItem.product.thumbnail.absolute}"> src="<c:url value="/assets/Image/Product/thumb/${lineItem.product.thumbnail.imageUrl}"/>" </c:if> <c:if test="${lineItem.product.thumbnail.absolute}"> src="<c:url value="${lineItem.product.thumbnail.imageUrl}"/>" </c:if> />
	</c:if><c:out value="${lineItem.product.sku}"/>
  </td>
  <td class="invoice"><c:if test="${lineItem.customXml != null}">Custom Frame - </c:if><c:out value="${lineItem.product.name}"/>
    <table border="0" cellspacing="1" cellpadding="0">
	  <c:forEach items="${lineItem.productAttributes}" var="productAttribute" varStatus="productAttributeStatus">
		<tr class="invoice_lineitem_attributes${productAttributeStatus.count%2}">
		<td class="optionName" style="white-space: nowrap" align="right"><c:out value="${productAttribute.optionName}"/>: </td>
		<td class="optionValue" style="padding-left:5px;"><c:out value="${productAttribute.valueString}" escapeXml="false"/></td>
		<c:if test="${(! empty productAttribute.optionPriceOriginal) && productAttribute.optionPriceOriginal != 0}">
		<td class="optionPrice"><fmt:formatNumber value="${productAttribute.optionPriceOriginal}" pattern="#,##0.00" /></td>
		<td class="optionNote" style="padding-left:5px;"><fmt:message key="${productAttribute.optionPriceMessageF}"/></td>
		</c:if>
		</tr>
	  </c:forEach>
	  <%@ include file="/WEB-INF/jsp/frontend/common/customLinesInvoice.jsp"%>
   </table>
   <c:if test="${lineItem.subscriptionInterval != null}">
	  <c:set var="intervalType" value="${fn:substring(lineItem.subscriptionInterval, 0, 1)}"/>
	  <c:set var="intervalUnit" value="${fn:substring(lineItem.subscriptionInterval, 1, 2)}"/>
	  <div class="invoice_lineitem_subscription">
		<fmt:message key="subscription"/> <fmt:message key="details"/> : <fmt:message key="delivery"/>
		<c:choose>
		  <c:when test="${intervalUnit == '1'}"><fmt:message key="every_${intervalType}"><fmt:param value="${intervalUnit}"/></fmt:message></c:when>
		  <c:otherwise><fmt:message key="every_${intervalType}s"><fmt:param value="${intervalUnit}"/></fmt:message></c:otherwise>
		</c:choose>
	  </div>
	  <div class="invoice_lineitem_subscription"><c:out value="${lineItem.subscriptionCode}"/></div>
  </c:if>
  <c:if test="${siteConfig['SHOW_IMAGE_ON_INVOICE'].value == 'true' and lineItem.hasProductAttributesImage}" >
  <c:forEach items="${lineItem.productAttributes}" var="productAttribute">
    <c:if test="${!empty productAttribute.imageUrl}" > 
      <c:if test="${!productAttribute.absolute}"><img src="<c:url value="/assets/Image/Product/options/${productAttribute.imageUrl}"/>" border="0" class="invoiceOptionImage" /></c:if>
      <c:if test="${productAttribute.absolute}"><img src="<c:url value="${productAttribute.imageUrl}"/>" border="0" class="invoiceOptionImage" /></c:if>
    </c:if>
  </c:forEach>
  </c:if> 
  </td>
  <c:forEach items="${productFieldsHeader}" var="pFHeader" varStatus="status"><c:set var="check" value="0"/>
  <c:if test="${pFHeader.showOnInvoice or pFHeader.showOnInvoiceBackend}">
  <c:forEach items="${lineItem.productFields}" var="productField"> 
   <c:if test="${pFHeader.id == productField.id}">
    <td class="invoice"><c:out value="${productField.value}" /></td><c:set var="check" value="1"/>
   </c:if>       
  </c:forEach>
  <c:if test="${check == 0}">
    <td class="invoice">&nbsp;</td>
   </c:if>
  </c:if>
  </c:forEach>
  <c:if test="${gSiteConfig['gINVENTORY'] && order.hasLowInventoryMessage}">
	<td class="invoice" align="center"><c:out value="${lineItem.lowInventoryMessage}" /></td>
  </c:if>
  <c:if test="${order.hasPacking }">
  <td class="invoice" align="center">
  <c:choose>
      <c:when test="${lineItem.product.caseContent != null}">
	      <c:choose>
	      <c:when test="${fn:startsWith(lineItem.product.sku,'1') }">
	      	<c:out value="Feet" />
	      </c:when>
	      <c:otherwise><c:out value="Pcs" /></c:otherwise>
	      </c:choose>
      </c:when>
      <c:otherwise><c:out value="${lineItem.product.packing}" /></c:otherwise>
    </c:choose>
    </td>
  </c:if>
</tr>  
</c:forEach>
</table>
<c:out value="${siteConfig['INVOICE_MESSAGE'].value}" escapeXml="false"/>
<p>




<c:if test="${order.invoiceNote != null and order.invoiceNote != ''}">
<table width="100%">
	<tr >
	  <td valign="top">
	  <b><c:out value="${siteConfig['SPECIAL_INSTRUCTIONS'].value}"/>:</b> <c:out value="${order.invoiceNote}" escapeXml="false"/>  	 
	  </td>
	</tr>
</table>
</c:if>
<p>
<br>
<c:out value="${invoiceLayout.footerHtml}" escapeXml="false"/>
<p>
</c:if>

</div>

</body>
</html>