<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ taglib uri="/WEB-INF/tlds/spring.tld" prefix="spring" %>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ taglib uri="http://www.springframework.org/security/tags" prefix="sec" %>

<tiles:insertDefinition name="admin.orders" flush="true">
  <tiles:putAttribute name="tab"  value="orders" />
  <tiles:putAttribute name="content" type="string">

<script type="text/javascript" src="../javascript/autocompleter1.2.js"></script>
<script type="text/javascript" src="../javascript/autocompleter.Request1.2.js"></script>
<script type="text/javascript" src="../javascript/autocompleter.Local1.2.js"></script>
<script type="text/javascript" src="../javascript/observer.js"></script>
<link href="<c:url value="/assets/invoice.css"/>" rel="stylesheet" type="text/css"/>
<script type="text/javascript">
<!--
function buySAFEOnClick(NewWantsBondValue) {
	document.getElementById('WantsBondField').value = NewWantsBondValue;
	document.forms['invoiceForm'].submit();
}
window.addEvent('domready', function(){	

	var tabs1 = new SimpleTabs($('tab-block-1'), { selector: 'h4'});
	for (var i=1;i<=20;i=i+1) {
		this["el"+i] = $('addSku'+i);
		new Autocompleter.Request.HTML(this["el"+i], '../orders/show-ajax-skus.jhtm', {
		'indicatorClass': 'autocompleter-loading' });
	}
});
    function choosePromo(el) {
    	document.getElementById('promoTitle').value=el.value;
    	document.getElementById('promoDiscount').value = document.getElementById('discount_'+el.value).value;
    	<c:if test="${gSiteConfig['gSALES_PROMOTIONS_ADVANCED']}">
    	document.getElementById('promoDiscountType').value = document.getElementById('discount_type_'+el.value).value;
    	</c:if>
    	document.getElementById('promoPercent').value = document.getElementById('percent_'+el.value).value;
	}
	function updateShipping(title, value) { 
		$('shippingTitle').value=title;
		$('shippingCost').value=value;
		var newGrandTotal= parseFloat($('grandTotalValue').value);
		if(value != null && value !== '') {
			newGrandTotal= parseFloat($('grandTotalValue').value) + parseFloat(value);
		} 
		$('grandTotal').set('html',newGrandTotal);
	}	
	function chooseCustomShippingRate(el) {
	    document.getElementById('shippingTitle').value=el.value;
	    document.getElementById('shippingCost').value = document.getElementById('price_'+el.value).value;
	}	
	function chooseAddress(el) {
		$('order.shipping.code').value = $('code_'+el.value).value;
	    $('order.shipping.firstName').value = $('firstname_'+el.value).value;
	    $('order.shipping.lastName').value = $('lastname_'+el.value).value;
	    $('order.shipping.country').value = $('country_'+el.value).value;
	    $('order.shipping.company').value = $('company_'+el.value).value;
	    $('order.shipping.addr1').value = $('addr1_'+el.value).value;
	    $('order.shipping.addr2').value = $('addr2_'+el.value).value;
	    $('order.shipping.city').value = $('city_'+el.value).value;
	    $('shipping_state').value = $('state_'+el.value).value;
	    $('shipping_ca_province').value = $('state_'+el.value).value;
	    $('order.shipping.zip').value = $('zip_'+el.value).value;
	    $('order.shipping.phone').value = $('phone_'+el.value).value;
	    $('order.shipping.cellPhone').value = $('cellPhone_'+el.value).value;
	    $('order.shipping.fax').value = $('fax_'+el.value).value;
	    if ( $('residential_'+el.value).value == 'true' ) {
	    	$('order.shipping.residential1').checked = true;
	    } else {
	    	$('order.shipping.residential1').checked = false;
	    }
	    if ( $('liftGate_'+el.value).value == 'true' ) {
	    	$('order.shipping.liftGate').checked = true;
	    } else {
	    	$('order.shipping.liftGate').checked = false;
	    }
	}	   
<c:if test="${gSiteConfig['gRMA']}">
function addSerialNum(index, suffix) {
  var textfield = document.createElement("input"); 
  textfield.setAttribute("type", "text"); 
  textfield.setAttribute("name", "__serialNum_" + suffix + index); 
  textfield.setAttribute("id", "__serialNum_" + suffix + index); 
  textfield.setAttribute("size", "10");		
  textfield.setAttribute("maxlength", "50");
  document.getElementById('__serialNums_' + suffix + index).appendChild(textfield);
  var comma = document.createTextNode(", ");
  document.getElementById('__serialNums_' + suffix + index).appendChild(comma);
}
</c:if>
<c:if test="${invoiceForm.newInvoice and (gSiteConfig['gADD_INVOICE'] or gSiteConfig['gSERVICE']) and siteConfig['INVOICE_REMOVE_VALIDATION'].value != 'true'}">
function checkRequiredValues() {
 if ($('shippingTitle').value == "") {
 	alert("Please select Shipping Method");
 	return false;
 } 
 if ($('order.paymentMethod').value == "") {
 	alert("Please select Payment Method");
 	return false;
 }
 if ($('order.orderType').value == "") {
 	alert("Please select Order Type");
 	return false;
 }
 <c:if test="${siteConfig['ORDER_FLAG1_NAME'].value != ''}">
 if ($('order.flag1').value == "") {
 	alert("Please select <c:out value="${siteConfig['ORDER_FLAG1_NAME'].value}" />");
 	return false;
 }
 </c:if>
 return true;
}
</c:if>


window.addEvent('domready', function(){	
	var firstName = $('order.shipping.firstName');
	new Autocompleter.Request.HTML(firstName, '../customers/show-ajax-customers.jhtm', {
		'indicatorClass': 'autocompleter-loading',
		'postData': { 'search': 'firstname' },
		'injectChoice': function(choice) {
			var text = choice.getFirst();
			var value = text.innerHTML;
			choice.inputValue = value;
			text.set('html', this.markQueryValue(value));
			this.addChoiceEvents(choice);
		},
		'onSelection': function(element, selected, value, input) {
			var userId = selected.getFirst().getNext().innerHTML;
			$('order.shipping.lastName').value = selected.getFirst().getNext().getNext().innerHTML;
			$('order.shipping.company').value = $('company'+userId).innerHTML;
			$('order.shipping.country').value = $('country'+userId).innerHTML;
			toggleStateProvince($('order.shipping.country'), 'shipping');
			$('order.shipping.addr1').value = $('addr1'+userId).innerHTML;
			$('order.shipping.addr2').value = $('addr2'+userId).innerHTML;
			$('order.shipping.city').value = $('city'+userId).innerHTML;
			if($('order.shipping.country').value == 'US') {
				$('shipping_state').value = $('stateProvince'+userId).innerHTML;
			} else if($('order.shipping.country').value == 'CA') {
				$('shipping_ca_province').value = $('stateProvince'+userId).innerHTML;
			} else {
				$('shipping_province').value = $('stateProvince'+userId).innerHTML;
			}
			$('order.shipping.zip').value = $('zip'+userId).innerHTML;
			$('order.shipping.phone').value = $('phone'+userId).innerHTML;
			$('order.shipping.cellPhone').value = $('cellPhone'+userId).innerHTML;
			$('order.shipping.fax').value = $('fax'+userId).innerHTML;
			$('order.shipping.residential1').checked = false;
			if($('residential'+userId).innerHTML == 'true') {
				$('order.shipping.residential1').checked = true;
			}
			$('order.shipping.liftGate1').checked = false;
			if($('liftGate'+userId).innerHTML == 'true') {
				$('order.shipping.liftGate1').checked = true;
			}
			
            $('order.billing.firstName').value = value;
    		$('order.billing.lastName').value = selected.getFirst().getNext().getNext().innerHTML;
			$('order.billing.company').value = $('company'+userId).innerHTML;
			$('order.billing.country').value = $('country'+userId).innerHTML;
			toggleStateProvince($('order.billing.country'), 'billing');
			$('order.billing.addr1').value = $('addr1'+userId).innerHTML;
			$('order.billing.addr2').value = $('addr2'+userId).innerHTML;
			$('order.billing.city').value = $('city'+userId).innerHTML;
			if($('order.billing.country').value == 'US') {
				$('billing_state').value = $('stateProvince'+userId).innerHTML;
			} else if($('order.billing.country').value == 'CA') {
				$('billing_ca_province').value = $('stateProvince'+userId).innerHTML;
			} else {
				$('billing_province').value = $('stateProvince'+userId).innerHTML;
			}
			$('order.billing.zip').value = $('zip'+userId).innerHTML;
			$('order.billing.phone').value = $('phone'+userId).innerHTML;
			$('order.billing.cellPhone').value = $('cellPhone'+userId).innerHTML;
			$('order.billing.fax').value = $('fax'+userId).innerHTML;
			
		}
	});
	var lastName = $('order.shipping.lastName');
	new Autocompleter.Request.HTML(lastName, '../customers/show-ajax-customers.jhtm', {
		'indicatorClass': 'autocompleter-loading',
		'postData': { 'search': 'lastname' },
		'injectChoice': function(choice) {
			var text = choice.getFirst();
			var value = text.innerHTML;
			choice.inputValue = value;
			text.set('html', this.markQueryValue(value));
			this.addChoiceEvents(choice);
		},
		'onSelection': function(element, selected, value, input) {
			var userId = selected.getFirst().getNext().innerHTML;
			$('order.shipping.firstName').value = selected.getFirst().getNext().getNext().innerHTML;
			$('order.shipping.company').value = $('company'+userId).innerHTML;
			$('order.shipping.country').value = $('country'+userId).innerHTML;
			toggleStateProvince($('order.shipping.country'), 'shipping');
			$('order.shipping.addr1').value = $('addr1'+userId).innerHTML;
			$('order.shipping.addr2').value = $('addr2'+userId).innerHTML;
			$('order.shipping.city').value = $('city'+userId).innerHTML;
			if($('order.shipping.country').value == 'US') {
				$('shipping_state').value = $('stateProvince'+userId).innerHTML;
			} else if($('order.shipping.country').value == 'CA') {
				$('shipping_ca_province').value = $('stateProvince'+userId).innerHTML;
			} else {
				$('shipping_province').value = $('stateProvince'+userId).innerHTML;
			}
			$('order.shipping.zip').value = $('zip'+userId).innerHTML;
			$('order.shipping.phone').value = $('phone'+userId).innerHTML;
			$('order.shipping.cellPhone').value = $('cellPhone'+userId).innerHTML;
			$('order.shipping.fax').value = $('fax'+userId).innerHTML;
			$('order.shipping.residential1').checked = false;
			if($('residential'+userId).innerHTML == 'true') {
				$('order.shipping.residential1').checked = true;
			}
			$('order.shipping.liftGate1').checked = false;
			if($('liftGate'+userId).innerHTML == 'true') {
				$('order.shipping.liftGate1').checked = true;
			}

			$('order.billing.firstName').value = selected.getFirst().getNext().getNext().innerHTML;
			$('order.billing.lastName').value = value;
			$('order.billing.company').value = $('company'+userId).innerHTML;
			$('order.billing.country').value = $('country'+userId).innerHTML;
			toggleStateProvince($('order.billing.country'), 'billing');
			$('order.billing.addr1').value = $('addr1'+userId).innerHTML;
			$('order.billing.addr2').value = $('addr2'+userId).innerHTML;
			$('order.billing.city').value = $('city'+userId).innerHTML;
			if($('order.billing.country').value == 'US') {
				$('billing_state').value = $('stateProvince'+userId).innerHTML;
			} else if($('order.billing.country').value == 'CA') {
				$('billing_ca_province').value = $('stateProvince'+userId).innerHTML;
			} else {
				$('billing_province').value = $('stateProvince'+userId).innerHTML;
			}
			$('order.billing.zip').value = $('zip'+userId).innerHTML;
			$('order.billing.phone').value = $('phone'+userId).innerHTML;
			$('order.billing.cellPhone').value = $('cellPhone'+userId).innerHTML;
			$('order.billing.fax').value = $('fax'+userId).innerHTML;
			
		}
	});
});

//-->
</script>   
<form:form commandName="invoiceForm" method="post">
<input type="hidden" name="WantsBondField" value="${invoiceForm.order.wantsBond}" id="WantsBondField">
  
<c:if test="${invoiceForm.order != null}">
  <!-- main box -->
  <div id="mboxfull">
  
  <!-- start table -->
  <table cellpadding="0" cellspacing="0" border="0" class="module" >
  <tr>
    <td class="topl_g">
      <spring:bind path="invoiceForm.order.orderId">
		<input type="hidden" name="${status.expression}" value="${status.value}">
	  </spring:bind>
	  
	  <!-- breadcrumb -->
	  <p class="breadcrumb">
	    <a href="../orders">Quotes</a> &gt;
	    <c:if test="${!invoiceForm.newInvoice}"><a href="invoice.jhtm?order=${invoiceForm.order.orderId}"><fmt:message key="invoice"/> #${invoiceForm.order.orderId}</a></c:if>
	  </p>

	  <!-- Error Message -->
	  <c:if test="${message != null}">
		<div class="message"><fmt:message key="${message}" /></div>
	  </c:if>
	  
	  <spring:bind path="invoiceForm.*">
		  <c:forEach var="error" items="${status.errorMessages}">
		    <div class="message"><c:out value="${error}"/></div>
		  </c:forEach>
	  </spring:bind>
	  
	  <div align="left">
		<table border="0" class="form">
		  <c:if test="${!invoiceForm.newInvoice}">
			<c:choose>
			  <c:when test="${siteConfig['CHANGE_ORDER_DATE'].value == 'true'}">
			    <tr>
			      <td><fmt:message key="orderDate" /></td><td>:</td> <td><form:input path="orderDate" readonly="true" size="18"  />
			      <img id="time_start_trigger" class="calendarImage" src="../graphics/calendarIcon.jpg" alt="Pick a date" /></td>
			    </tr>  
			  </c:when>
			  <c:otherwise>
			    <tr>
			      <td colspan="3"><fmt:message key="orderDate" />: <fmt:formatDate type="both" timeStyle="full" value="${invoiceForm.order.dateOrdered}"/></td>
			    </tr>
			  </c:otherwise>
			</c:choose>
			<c:if test="${siteConfig['DELIVERY_TIME'].value == 'true'}" >
			 <tr>
			   <td>Days to Ship</td><td>:</td>
			   <td>
			    <form:select path="order.shippingPeriod" >
			     <c:forEach begin="0" end="10" step="1" var="value">
			      <form:option value="${value}"><c:out value="${value}" /> Day</form:option>
			     </c:forEach>
			    </form:select>
			   </td>
			 </tr>  
			 <tr>
			   <td>Turn Over Day</td><td>:</td>
			   <td>
			    <form:select path="order.turnOverday">
			     <c:forEach begin="0" end="10" step="1" var="value">
			      <form:option value="${value}"><c:out value="${value}" /> Day</form:option>
			     </c:forEach>
			    </form:select>
			   </td>
			 </tr>  
			</c:if>
			<tr>
			  <td><fmt:message key="lastModified" /></td><td>:</td><td><fmt:formatDate type="both" timeStyle="full" value="${invoiceForm.order.lastModified}"/></td>
			</tr>  
		    <tr>
		      <td colspan="3"><b>Your Order Number is #<c:out value="${invoiceForm.order.orderId}"/></b></td>
		    </tr>  
		  </c:if>
		</table>
	  </div>	    

  
  </td><td class="topr_g" ></td></tr>
  <tr><td class="boxmidlrg" >

 <!-- tabs -->
 <div id="tab-block-1">
	<h4 title="Invoice">Quote</h4>
	<div>
	<!-- start tab -->
        <c:set var="classIndex" value="0" />
        
	  	<div class="listdivi ln tabdivi"></div>
	  	<div class="listdivi"></div>
	   
		  	<div class="list${classIndex % 2}"><c:set var="classIndex" value="${classIndex+1}" />
		  	<!-- input field -->
			<table cellpadding="0" cellspacing="0" border="0" width="100%">
			<tr valign="top">
			<td>
			<b><fmt:message key="billingInformation" />:</b>
			<table>
			  <tr>
			    <td><fmt:message key="firstName" />:</td>
			    <td><form:input path="order.billing.firstName"/></td>
			  </tr>
			  <tr>
			    <td><fmt:message key="lastName" />:</td>
			    <td><form:input path="order.billing.lastName"/></td>
			  </tr>
			  <tr>
			    <td><fmt:message key="country" />:</td>
			    <td>
			      <form:select path="order.billing.country"  onchange="toggleStateProvince(this, 'billing')" cssStyle="width:163px;">
			  	    <option value="">Please Select</option>
			  	    <form:options items="${countries}" itemValue="code" itemLabel="name"/>
					<form:errors path="order.billing.country" cssClass="error" />  
				  </form:select>
			    </td>
			  </tr>
			  <tr>
			    <td><fmt:message key="company" />:</td>
			    <td><form:input path="order.billing.company"/></td>
			  </tr>
			  <tr>
			    <td><fmt:message key="address" /> 1:</td>
			    <td><form:input path="order.billing.addr1"/></td>
			  </tr>
			  <tr>
			    <td><fmt:message key="address" /> 2:</td>
			    <td><form:input path="order.billing.addr2"/></td>
			  </tr>
			  <tr>
			    <td><fmt:message key="city" />:</td>
			    <td><form:input path="order.billing.city"/></td>
			  </tr>
			  <tr>
			    <td><fmt:message key="stateProvince" />:</td>
			    <td>
			      <spring:bind path="invoiceForm.order.billing.stateProvince">
					<select id="billing_state" name="<c:out value="${status.expression}"/>">
			  	      <option value="">Please Select</option>
				      <c:forEach items="${states}" var="state">
			  	        <option value="${state.code}" <c:if test="${state.code == status.value}">selected</c:if>>${state.name}</option>
					  </c:forEach>      
			        </select>
					<select id="billing_ca_province" name="<c:out value="${status.expression}"/>">
			  	      <option value="">Please Select</option>
				      <c:forEach items="${caProvinceList}" var="province">
			  	        <option value="${province.code}" <c:if test="${province.code == status.value}">selected</c:if>>${province.name}</option>
					  </c:forEach>      
			        </select> 
			      <input id="billing_province" type="text" name="<c:out value="${status.expression}"/>" value="<c:out value="${status.value}"/>">
			      <c:if test="${status.errorMessage != ''}"><span class="error"><c:out value="${status.errorMessage}"/></span></c:if>
			      </spring:bind>
			    </td>
			  </tr>
			  <tr>
			    <td><fmt:message key="zipPostalCode" />:</td>
			    <td><form:input path="order.billing.zip"/></td>
			  </tr>
			  <tr>
			    <td><fmt:message key="phone" />:</td>
			    <td><form:input path="order.billing.phone"/></td>
			  </tr>
			  <tr>
			    <td><fmt:message key="cellPhone" />:</td>
			    <td><form:input path="order.billing.cellPhone"/></td>
			  </tr>
			  <tr>
			    <td><fmt:message key="fax" />:</td>
			    <td><form:input path="order.billing.fax"/></td> 
			  </tr>
			</table>
			</td>
			<td>&nbsp;</td>
			<td>
			<b><c:choose><c:when test="${invoiceForm.order.workOrderNum != null}"><fmt:message key="service" /> <fmt:message key="location" /></c:when><c:otherwise><fmt:message key="shippingInformation" /></c:otherwise></c:choose>:</b>
			<table>
			  <tr>
			    <td><fmt:message key="firstName" />:</td>
			    <td>
			    <form:hidden path="order.shipping.code" />
			    <form:input path="order.shipping.firstName" />
			    </td>
			  </tr>
			  <tr>
			    <td><fmt:message key="lastName" />:</td>
			    <td>
			    <form:input path="order.shipping.lastName" />
			    </td>
			  </tr>
			  <tr>
			    <td><fmt:message key="country" />:</td>
			    <td>
			      <form:select path="order.shipping.country"  onchange="toggleStateProvince(this, 'shipping')" cssStyle="width:163px;">
			  	    <option value="">Please Select</option>
			  	    <form:options items="${countries}" itemValue="code" itemLabel="name"/>
					<form:errors path="order.shipping.country" cssClass="error" />  
				  </form:select>
			    </td>
			  </tr>
			  <tr>
			    <td><fmt:message key="company" />:</td>
			    <td>
			    <form:input path="order.shipping.company" />
			    </td>
			  </tr>
			  <tr>
			    <td><fmt:message key="address" /> 1:</td>
			    <td>
			    <form:input path="order.shipping.addr1" />
			    </td>
			  </tr>
			  <tr>
			    <td><fmt:message key="address" /> 2:</td>
			    <td>
			    <form:input path="order.shipping.addr2" />
			    </td>
			  </tr>
			  <tr>
			    <td><fmt:message key="city" />:</td>
			    <td>
			    <form:input path="order.shipping.city" />
			    </td>
			  </tr>
			  <tr>
			    <td><fmt:message key="stateProvince" />:</td>
			    <td>
			      <spring:bind path="invoiceForm.order.shipping.stateProvince" >
					<select id="shipping_state" name="<c:out value="${status.expression}"/>">
			  	      <option value="">Please Select</option>
				      <c:forEach items="${states}" var="state">
			  	        <option value="${state.code}" <c:if test="${state.code == status.value}">selected</c:if>>${state.name}</option>
					  </c:forEach>      
			        </select>
					<select id="shipping_ca_province" name="<c:out value="${status.expression}"/>">
			  	      <option value="">Please Select</option>
				      <c:forEach items="${caProvinceList}" var="province">
			  	        <option value="${province.code}" <c:if test="${province.code == status.value}">selected</c:if>>${province.name}</option>
					  </c:forEach>      
			        </select>
			      <input id="shipping_province" type="text" name="<c:out value="${status.expression}"/>" value="<c:out value="${status.value}"/>">
			      <c:if test="${status.errorMessage != ''}"><span class="error"><c:out value="${status.errorMessage}"/></span></c:if>
			      </spring:bind>
			    </td>
			  </tr>
			  <tr>
			    <td><fmt:message key="zipPostalCode" />:</td>
			    <td>
			    <form:input path="order.shipping.zip" />
			    </td>
			  </tr>
			  <tr>
			    <td><fmt:message key="phone" />:</td>
			    <td>
			    <form:input path="order.shipping.phone" />
			    </td>
			  </tr>
			  <tr>
			    <td><fmt:message key="cellPhone" />:</td>
			    <td>
			    <form:input path="order.shipping.cellPhone" />
			    </td>
			  </tr>  
			  <tr>
			    <td><fmt:message key="fax" />:</td>
			    <td> 
			    <form:input path="order.shipping.fax" />
			    </td>
			  </tr>
			  <tr>
			    <td><fmt:message key="residential" />:</td>
			    <td> 
			    <form:checkbox path="order.shipping.residential" />
			    </td>
			  </tr>
			  <tr>
			    <td><fmt:message key="liftGateDelivery" />:</td>
			    <td> 
			    <form:checkbox path="order.shipping.liftGate" />
			    </td>
			  </tr>
			</table>
			</td>
			<td>&nbsp;</td>
			<td>
			<b><fmt:message key="source" />:</b>
			<table>
			  <tr>
			    <td><fmt:message key="country" />:</td>
			    <td>
			      <form:select path="order.source.country"  onchange="toggleStateProvince(this, 'source')" cssStyle="width:163px;">
			  	    <option value="">Please Select</option>
			  	    <form:options items="${countries}" itemValue="code" itemLabel="name"/>
					<form:errors path="order.source.country" cssClass="error" />  
				  </form:select>
			    </td>
			  </tr>
			  <tr>
			    <td><fmt:message key="stateProvince" />:</td>
			    <td>
			      <spring:bind path="invoiceForm.order.source.stateProvince" >
					<select id="source_state" name="<c:out value="${status.expression}"/>">
			  	      <option value="">Please Select</option>
				      <c:forEach items="${states}" var="state">
			  	        <option value="${state.code}" <c:if test="${state.code == status.value}">selected</c:if>>${state.name}</option>
					  </c:forEach>      
			        </select>
					<select id="source_ca_province" name="<c:out value="${status.expression}"/>">
			  	      <option value="">Please Select</option>
				      <c:forEach items="${caProvinceList}" var="province">
			  	        <option value="${province.code}" <c:if test="${province.code == status.value}">selected</c:if>>${province.name}</option>
					  </c:forEach>      
			        </select>
			      <input id="source_province" type="text" name="<c:out value="${status.expression}"/>" value="<c:out value="${status.value}"/>">
			      <c:if test="${status.errorMessage != ''}"><span class="error"><c:out value="${status.errorMessage}"/></span></c:if>
			      </spring:bind>
			    </td>
			  </tr>
			  <tr>
			    <td><fmt:message key="zipPostalCode" />:</td>
			    <td><form:input path="order.source.zip"/></td>
			  </tr>
			</table>
			</td>
			<%-- 
			<td style="width:15%;">&nbsp;</td>
			<td align="left">
			<b><fmt:message key="customerInformation" />:</b>
			<table cellpadding="0" cellspacing="0">
			  <tr>
			    <td><fmt:message key="account"/> #</td>
			    <td>:&nbsp;&nbsp;</td>
			    <td><c:out value="${customer.accountNumber}"/></td>
			  </tr>
			  <tr>
			    <td><fmt:message key="taxID"/> #</td>
			    <td>:&nbsp;&nbsp;</td>
			    <td><c:out value="${customer.taxId}"/></td>
			  </tr>
			  <tr>
			    <td><fmt:message key="orderCount"/></td>
			    <td>:&nbsp;&nbsp;</td>
			    <td><a href="ordersList.jhtm?email=<c:out value='${customer.username}'/>"><c:out value="${customerOrderInfo.orderCount}"/></a></td>
			  </tr>
			  <tr>
			    <td><fmt:message key="lastOrder"/></td>
			    <td>:&nbsp;&nbsp;</td>
			    <td><fmt:formatDate pattern="MM/dd/yyyy" value="${customerOrderInfo.lastOrderDate}"/></td>
			  </tr>
			 
			  <c:if test="${gSiteConfig['gI18N'] != ''}">
			  <tr>
			    <td><fmt:message key="language"/></td>
			    <td>:&nbsp;&nbsp;</td>
			    <td>
       			  <form:select path="order.language">
        	  	    <form:option value="en"><fmt:message key="language_en"/></form:option>
        	  		<c:forTokens items="${gSiteConfig['gI18N']}" delims="," var="i18n">
          			  <form:option value="${i18n}"><fmt:message key="language_${i18n}"/></form:option>
        	  		</c:forTokens>
	  			  </form:select>
			  </tr>
			  </c:if>
			  
			</table>  
			</td>
			<td style="width:20px;">&nbsp;</td>
			<td align="left">
			<b><fmt:message key="quoteInformation" />:</b>
			<table cellpadding="0" cellspacing="0">
			  <c:if test="${gSiteConfig['gSALES_REP']}">
			  <tr>
			    <td><fmt:message key="salesRep"/>:</td>
			    <td>
			     <form:select path="order.salesRepId">
			      <form:option value=""><fmt:message key="none" /></form:option>
			 	  <form:options items="${salesRepList}" itemValue="id" itemLabel="name"/>
			     </form:select>
			    </td>
			  </tr>
			  <tr>
			    <td><fmt:message key="processedBy" />:</td>
			    <td>
			     <form:select path="order.salesRepProcessedById">
			      <form:option value=""><fmt:message key="none" /></form:option>
			 	  <form:options items="${salesRepList}" itemValue="id" itemLabel="name"/>
			     </form:select>
			    </td>
			  </tr>
			  <c:if test="${siteConfig['NO_COMMISSION_FLAG'].value == 'true'}">
			  <tr>
			    <td>Shared Commission:</td>
			    <td>
			     <form:select path="order.nc">
			 	  <form:option value="true"><fmt:message key="yes" /></form:option>
			 	  <form:option value="false"><fmt:message key="no" /></form:option>
			     </form:select>
			    </td>
			  </tr>
			  </c:if>
			  </c:if>
			  <c:if test="${gSiteConfig['gINVOICE_APPROVAL']}"> 
			  <tr>
			    <td>Require Approval:</td>
			    <td>
			      <form:select path="order.approval">
			      <form:option value=""><fmt:message key="none" /></form:option>
			 	  <form:option value="ra"><fmt:message key="approvalStatus_ra" /></form:option>
			 	  <form:option value="ap"><fmt:message key="approvalStatus_ap" /></form:option>
			     </form:select>
			    </td>
			  </tr>  
			  </c:if>
			  <tr>
			    <td><fmt:message key="quoteType"/>:</td>
			    <td>
			     <form:select path="order.orderType">
			        <form:option value=""></form:option>
			      <c:forTokens items="${siteConfig['ORDER_TYPE'].value}" delims="," var="type" varStatus="status">
			        <form:option value ="${type}"><c:out value ="${type}" /></form:option>
		  	      </c:forTokens>
			     </form:select>
			     <form:errors path="order.orderType" cssClass="error" />  
			    </td>
			  </tr> 
			  <c:if test="${siteConfig['ORDER_FLAG1_NAME'].value != ''}">
			  <tr>
			    <td><c:out value="${siteConfig['ORDER_FLAG1_NAME'].value}" />:</td>
			    <td>
			     <form:select path="order.flag1">
			      <form:option value=""></form:option>
			 	  <form:option value="1"><fmt:message key="yes" /></form:option>
			 	  <form:option value="0"><fmt:message key="no" /></form:option>
			     </form:select>
			     <form:errors path="order.flag1" cssClass="error" />  
			    </td>
			  </tr> 
			  <tr>
			    <td><fmt:message key="trackcode"/>:</td>
			    <td>
			     <form:input path="order.trackcode"  cssStyle="width:112px;"/>
			    </td>
			  </tr> 
			  </c:if>
			</table>
			</td>
			--%>
			</tr>
			</table>
			
			<%--   
			<b><fmt:message key="purchaseOrder" /></b>: <form:input path="order.purchaseOrder" maxlength="50" htmlEscape="true"/>
			
			<c:if test="${invoiceForm.order.workOrderNum != null}" >
			<br/><b><fmt:message key="workOrder" /> #</b>: <c:out value="${invoiceForm.order.workOrderNum}" />
			</c:if>

			<c:if test="${workOrder != null}">
			<br/><b><c:out value="${siteConfig['SERVICEABLE_ITEM_TITLE'].value}"/> ID</b>: <c:out value="${workOrder.service.item.itemId}" />
			<br/><b>In SN</b>: <c:out value="${workOrder.inSN}" />
			<br/><b><fmt:message key="bwCount"/></b>: <fmt:formatNumber value="${workOrder.bwCount}" pattern="#,##0"/>
			<br/><b><fmt:message key="colorCount"/></b>: <fmt:formatNumber value="${workOrder.colorCount}" pattern="#,##0"/>
			<br/><b><fmt:message key="problem" /></b>: <c:out value="${workOrder.service.problem}" />
			<c:if test="${workOrder.service.trackNumIn != null and workOrder.service.trackNumIn != ''}">
			<br/><b>Inbound <fmt:message key="trackNum" /></b>: <c:out value="${workOrder.service.trackNumInCarrier}" />
				<c:choose>
				 <c:when test="${workOrder.service.trackNumInCarrier == 'UPS'}">
				  <a href="http://wwwapps.ups.com/WebTracking/processInputRequest?HTMLVersion=5.0&sort_by=status&tracknums_displayed=10&TypeOfInquiryNumber=T&loc=en_US&InquiryNumber1=<c:out value='${workOrder.service.trackNumIn}' />&AgreeToTermsAndConditions=yes"><c:out value="${workOrder.service.trackNumIn}" /></a>
				 </c:when>
				 <c:when test="${workOrder.service.trackNumInCarrier == 'FedEx'}">
				  <a href="http://fedex.com/Tracking?action=track&language=english&cntry_code=us&mps=y&tracknumbers=<c:out value='${workOrder.service.trackNumIn}' />&ascend_header=1"><c:out value="${workOrder.service.trackNumIn}" /></a>
				 </c:when>
				 <c:when test="${workOrder.service.trackNumInCarrier == 'USPS'}">
				  <a href="http://trkcnfrm1.smi.usps.com/PTSInternetWeb/InterLabelInquiry.do?strOrigTrackNum=<c:out value='${workOrder.service.trackNumIn}' />"><c:out value="${workOrder.service.trackNumIn}" /></a>
				 </c:when>
				 <c:otherwise><c:out value="${workOrder.service.trackNumIn}" /></c:otherwise>
				</c:choose>	
			</c:if>			
			<c:if test="${workOrder.service.trackNumOut != null and workOrder.service.trackNumOut != ''}">
			<br/><b>Outbound <fmt:message key="trackNum" /></b>: <c:out value="${workOrder.service.trackNumOutCarrier}" />
				<c:choose>
				 <c:when test="${workOrder.service.trackNumOutCarrier == 'UPS'}">
				  <a href="http://wwwapps.ups.com/WebTracking/processInputRequest?HTMLVersion=5.0&sort_by=status&tracknums_displayed=10&TypeOfInquiryNumber=T&loc=en_US&InquiryNumber1=<c:out value='${workOrder.service.trackNumOut}' />&AgreeToTermsAndConditions=yes"><c:out value="${workOrder.service.trackNumOut}" /></a>
				 </c:when>
				 <c:when test="${workOrder.service.trackNumOutCarrier == 'FedEx'}">
				  <a href="http://fedex.com/Tracking?action=track&language=english&cntry_code=us&mps=y&tracknumbers=<c:out value='${workOrder.service.trackNumOut}' />&ascend_header=1"><c:out value="${workOrder.service.trackNumOut}" /></a>
				 </c:when>
				 <c:when test="${workOrder.service.trackNumOutCarrier == 'USPS'}">
				  <a href="http://trkcnfrm1.smi.usps.com/PTSInternetWeb/InterLabelInquiry.do?strOrigTrackNum=<c:out value='${workOrder.service.trackNumOut}' />"><c:out value="${workOrder.service.trackNumOut}" /></a>
				 </c:when>
				 <c:otherwise><c:out value="${workOrder.service.trackNumOut}" /></c:otherwise>
				</c:choose>	
			</c:if>
			</c:if>
 --%>
			<sec:authorize ifAnyGranted="ROLE_AEM,ROLE_ADMIN_${prop_site_id}">
			  <c:set var="canEdit" value="true"/>
			</sec:authorize>  
						
			<table border="0" cellpadding="2" cellspacing="1" width="100%" class="invoice">
			  <tr>
			    <c:set var="cols" value="0"/>
			    <c:set var="cols2" value="0"/>
			    <c:if test="${invoiceForm.order.workOrderNum == null or canEdit}" >
			      <c:set var="cols" value="${cols+1}"/>
			      <th width="5%" class="invoice"><fmt:message key="remove" /></th>
			    </c:if>
			    <th width="5%" class="invoice"><fmt:message key="line" />#</th>
			    <th class="invoice"><fmt:message key="productId" /></th>
			    <th class="invoice"><fmt:message key="productSku" /></th>
			    <th class="invoice"><fmt:message key="productName" /></th>
			    <th class="invoice"><fmt:message key="quantity" /></th>
			    <c:if test="${gSiteConfig['gINVENTORY'] && invoiceForm.order.hasLowInventoryMessage}">
			      <c:set var="cols" value="${cols+1}"/>
			      <th width="10%" class="invoice"><fmt:message key="lowInventory" /></th>
			    </c:if>
			    <c:if test="${invoiceForm.order.hasPacking}">
			      <c:set var="cols" value="${cols+1}"/>
			      <th width="10%" class="invoice"><fmt:message key="packing" /></th>
			    </c:if>
			    <c:forEach items="${productFieldsHeader}" var="productField">
			      <c:if test="${productField.showOnInvoice or productField.showOnInvoiceBackend}">
			      <th class="invoice"><c:out value="${productField.name}" /></th><c:set var="cols" value="${cols+1}"/>
			      </c:if>
			    </c:forEach>
			    <c:if test="${invoiceForm.order.hasContent}">
			      <c:set var="cols" value="${cols+1}"/>
			      <th class="invoice"><fmt:message key="content" /></th>
			    </c:if>
			    <c:if test="${invoiceForm.order.hasCustomShipping}">
	  			  <c:set var="cols" value="${cols+1}"/>
      			  <th class="invoice"><fmt:message key="customShipping" /></th>
    			</c:if>
			    <th align="center" class="invoice"><fmt:message key="productPrice" /><c:if test="${order.hasContent}"> / <c:out value="${siteConfig['CASE_UNIT_TITLE'].value}" /></c:if></th>
			    <c:if test="${gSiteConfig['gSPECIAL_PRICING']}" >
			      <c:set var="cols2" value="${cols2+1}"/>
			      <th class="invoice" width="6px"><fmt:message key="specialPricing" /></th>
			    </c:if>
			    <c:if test="${gSiteConfig['gPURCHASE_ORDER'] && siteConfig['SHOW_COST'].value == 'true' && invoiceForm.order.hasCost}" >
			       <c:set var="cols2" value="${cols2+1}"/>
			       <th class="invoice" width="40px"><fmt:message key="cost" /></th>
			    </c:if>
			    <th class="invoice" width="5%"><fmt:message key="taxable" /></th>
			    <th width="15%" class="invoice"><fmt:message key="total" /></th>
			  </tr>
			  
			<c:forEach var="lineItem" items="${invoiceForm.order.lineItems}" varStatus="status">
			<c:if test="${status.first and (invoiceForm.order.workOrderNum == null or canEdit)}">
			  <tr bgcolor="#FFFFCC">
			    <td colspan="${5+cols}">EXISTING</td>
			    <td style="width:300px;">
			     <table border="0" cellpadding="1" cellspacing="0" style="width:300px;" >
			      <tr>
			       <td style="width:80px">Original</td>
			       <td style="text-align:center;width:140px">Discount</td>
			       <td style="width:80px">Price</td>
			      </tr>
			     </table>
			    </td>   
			    <td colspan="${2+cols2}"></td>
			  </tr>
			</c:if>
			<tr valign="top">
			  <c:if test="${invoiceForm.order.workOrderNum == null or canEdit}" >
			    <td class="invoice" align="center"><input type="checkbox" name="__remove_${lineItem.lineNumber}" <c:if test="${lineItem.processed > 0}" >disabled='true'</c:if><c:if test="${gSiteConfig['gSPECIAL_PRICING']}" >onChange="toggleSpecialPricing(this, '__special_pricing_${lineItem.lineNumber}')"</c:if>></td>
			  </c:if>
			  <td class="invoice" align="center"><c:out value="${status.count}"/></td>
			  <td class="invoice" align="center"><c:out value="${lineItem.productId}"/></td>
			  <td class="invoice"><c:out value="${lineItem.product.sku}"/></td>
			  <td class="invoice"><input type="text" name="__name_${lineItem.lineNumber}" value="<c:out value="${lineItem.product.name}"/>" maxlength="120">
				<table border="0" cellspacing="1" cellpadding="0">
				  <c:forEach items="${lineItem.productAttributes}" var="productAttribute" varStatus="productAttributeStatus">
					<tr class="invoice_lineitem_attributes${productAttributeStatus.count%2}">
					<td align="right"> -</td>
					<td class="optionName" style="white-space: nowrap" align="right"><c:out value="${productAttribute.optionName}"/>: </td>
					<td class="optionValue" style="padding-left:5px;"><c:out value="${productAttribute.valueString}" escapeXml="false"/></td>
					<c:if test="${(! empty productAttribute.optionPriceOriginal) && productAttribute.optionPriceOriginal != 0}">
					<td class="optionPrice"><fmt:formatNumber value="${productAttribute.optionPriceOriginal}" pattern="#,##0.00" /></td>
					<td class="optionNote" style="padding-left:5px;"><fmt:message key="${productAttribute.optionPriceMessageF}"/></td>
					</c:if>
					</tr>
				  </c:forEach>
			    </table>
				<c:if test="${lineItem.subscriptionInterval != null}">
				  <c:set var="intervalType" value="${fn:substring(lineItem.subscriptionInterval, 0, 1)}"/>
				  <c:set var="intervalUnit" value="${fn:substring(lineItem.subscriptionInterval, 1, 2)}"/>
				  <div class="invoice_lineitem_subscription">
					<fmt:message key="subscription"/> <fmt:message key="details"/> : <fmt:message key="delivery"/>
					<c:choose>
					  <c:when test="${intervalUnit == '1'}"><fmt:message key="every_${intervalType}"><fmt:param value="${intervalUnit}"/></fmt:message></c:when>
					  <c:otherwise><fmt:message key="every_${intervalType}s"><fmt:param value="${intervalUnit}"/></fmt:message></c:otherwise>
					</c:choose>
				  </div>
				  <div class="invoice_lineitem_subscription"><c:out value="${lineItem.subscriptionCode}"/></div>
				</c:if>
			  </td>
			  <td class="invoice" align="left"> 
			    <table width="100%"><tr>
			    <c:choose>
			    <c:when test="${invoiceForm.order.workOrderNum != null and !canEdit}">
			     <td><c:out value="${lineItem.quantity}"/></td>
			    </c:when>
			    <c:when test="${gSiteConfig['gINVENTORY']}" >
			     <td><input style="text-align: right;" type="text" name="__quantity_<c:out value="${lineItem.lineNumber}"/>" value="${lineItem.quantity}" size="5"></td>
			     <c:if test="${lineItem.product.id != null}"><td id="container${lineItem.productId}" style="color:#FF0000;" width="100%"></td></c:if>
			    </c:when>
			    <c:otherwise>
			      <td><input style="text-align: right;" type="text" name="__quantity_<c:out value='${lineItem.lineNumber}'/>" value="${lineItem.quantity}" size="5"></td>
			    </c:otherwise>
			    </c:choose>
			    </tr></table>
			  </td>
			  <c:if test="${gSiteConfig['gINVENTORY'] && invoiceForm.order.hasLowInventoryMessage}">
			  <td class="invoice" align="center"><input style="text-align: right;" type="text" name="__low_inventory_message_<c:out value='${lineItem.lineNumber}'/>" value="${lineItem.lowInventoryMessage}" size="10"></td>
			  </c:if>
			  <c:if test="${invoiceForm.order.hasPacking}">
			  <td class="invoice" align="center"><c:out value="${lineItem.product.packing}" /></td>
			  </c:if>
			  <c:forEach items="${productFieldsHeader}" var="pFHeader"><c:set var="check" value="0"/>
			  <c:if test="${pFHeader.showOnInvoice or pFHeader.showOnInvoiceBackend}">
			  <c:forEach items="${lineItem.productFields}" var="productField"> 
			   <c:if test="${pFHeader.id == productField.id}">
			    <td class="invoice" align="center"><c:out value="${productField.value}" /></td><c:set var="check" value="1"/>
			   </c:if>       
			  </c:forEach>
			  <c:if test="${check == 0}">
			    <td class="invoice">&nbsp;</td>
			   </c:if>
			  </c:if>
			  </c:forEach>
			  <c:if test="${invoiceForm.order.hasContent}">
			  <td class="invoice" align="center"><c:out value="${lineItem.product.caseContent}" /></td>
			  </c:if>
			  <c:if test="${invoiceForm.order.hasCustomShipping}"><td class="invoice" style="text-align:right;"><fmt:formatNumber value="${lineItem.customShippingCost}" pattern="#,##0.00"/></td></c:if>
			  <td style="width:300px;" class="invoice" align="right">
			    <table border="0" cellpadding="1" cellspacing="0" style="width:300px;" >
			      <tr>
			       <td style="width:80px;"><input  style="TEXT-ALIGN: right" type="text" name="__originalPrice_<c:out value="${lineItem.lineNumber}"/>" value="${lineItem.originalPrice}" size="8" /></td>
			       <td style="width:100px;"><input  style="TEXT-ALIGN: right" type="text" name="__discount_<c:out value="${lineItem.lineNumber}"/>" value="<c:out value="${lineItem.discount}"/>" size="4" />
			       <select style="width:55px;" name="__percent_<c:out value="${lineItem.lineNumber}"/>" >
			  	    <option value="true">%</option><option <c:if test="${!lineItem.percent}">selected</c:if> value="false">Fix</option>
			  	   </select>
			  	   </td>
			       <td style="width:80px;"><input  style="TEXT-ALIGN: right" type="text" disabled name="__unitPrice_<c:out value="${lineItem.lineNumber}"/>" value="${lineItem.unitPrice}" size="8" /></td>
			      </tr>
			    </table>
			  </td>
			  <c:if test="${gSiteConfig['gSPECIAL_PRICING']}" >
			  <td class="invoice" align="center"><input type="checkbox" id="__special_pricing_${lineItem.lineNumber}" name="__special_pricing_${lineItem.lineNumber}" <c:if test="${lineItem.setSpecialPricing}">checked</c:if> /></td>
			  </c:if>
			  <c:if test="${gSiteConfig['gPURCHASE_ORDER'] && siteConfig['SHOW_COST'].value == 'true' && invoiceForm.order.hasCost}" >
			  <td class="invoice" style="TEXT-ALIGN: right"><c:out value="${lineItem.unitCost}" /></td>
			  </c:if>
			  <td class="invoice" align="center"><input type="checkbox" name="__taxable_${lineItem.lineNumber}" <c:if test="${lineItem.product.taxable}">checked</c:if>></td>
			  <td class="invoice" align="right"><fmt:formatNumber value="${lineItem.totalPrice}" pattern="#,##0.00"/></td>
			</tr>
			<c:if test="${gSiteConfig['gRMA']}">
			<tr bgcolor="#FFFFFF">
			  <td colspan="${8+cols+cols2}">
			    <table cellspacing="3">
			      <tr>
			        <td valign="top"><a onClick="addSerialNum(${lineItem.lineNumber}, '')"><img src="../graphics/add.png" width="16" border="0"></a></td>
			        <td valign="top">S/N:</td>
			        <td><div id="__serialNums_${lineItem.lineNumber}">
				      <c:forEach items="${lineItem.serialNums}" var="serialNum">
					    <input type="text" name="__serialNum_${lineItem.lineNumber}" value="<c:out value="${serialNum}"/>" size="10" maxlength="50"
					    	<c:if test="${serialNumsMap[fn:toLowerCase(serialNum)] == 'duplicate'}">style="background-color:#EE0000;"</c:if>>,
					  </c:forEach></div></td>
			      </tr>
			    </table>
			  </td>
			</tr>			
			</c:if>
			</c:forEach>

			<c:forEach var="lineItem" items="${invoiceForm.order.insertedLineItems}" varStatus="status">
			<c:if test="${status.first and (invoiceForm.order.workOrderNum == null or canEdit)}">
			  <tr bgcolor="#FFFFCC">
			    <td colspan="${8+cols+cols2}">NEW</td>
			  </tr>
			</c:if>
			<tr valign="top">
			  <c:if test="${invoiceForm.order.workOrderNum == null or canEdit}" >
			    <td class="invoice" align="center"><input type="checkbox" name="__remove_new_${status.index}" <c:if test="${gSiteConfig['gSPECIAL_PRICING']}" >onChange="toggleSpecialPricing(this, '__special_pricing_new_${status.index}')"</c:if>></td>
			  </c:if>
			  <td class="invoice" align="center"><c:out value="${invoiceForm.order.size + status.count}"/></td>
			  <td class="invoice" align="center"><c:out value="${lineItem.productId}"/></td>
			  <td class="invoice"><c:out value="${lineItem.product.sku}"/></td>
			  <td class="invoice">
			  	<input type="text" name="__name_new_${status.index}" value="<c:out value="${lineItem.product.name}"/>" maxlength="120">
			  	<table border="0" cellspacing="1" cellpadding="0">
				  <c:forEach items="${lineItem.productAttributes}" var="productAttribute" varStatus="productAttributeStatus">
					<tr class="invoice_lineitem_attributes${productAttributeStatus.count%2}">
					<td align="right"> -</td>
					<td class="optionName" style="white-space: nowrap" align="right"><c:out value="${productAttribute.optionName}"/>: </td>
					<td class="optionValue" style="padding-left:5px;"><c:out value="${productAttribute.valueString}" escapeXml="false"/></td>
					<c:if test="${productAttribute.optionPriceOriginal != 0}">
					<td class="optionPrice"><fmt:formatNumber value="${productAttribute.optionPriceOriginal}" pattern="#,##0.00" /></td>
					<td class="optionNote" style="padding-left:5px;"><fmt:message key="${productAttribute.optionPriceMessageF}"/></td>
					</c:if>
					</tr>
				  </c:forEach>
			   </table>
			  </td>
			  <td class="invoice" align="left"> 
			  <table width="100%"><tr>
			  <c:choose>
			    <c:when test="${gSiteConfig['gINVENTORY']}" >
			      <td><input style="text-align: right;" type="text"  name="__quantity_new_${status.index}" value="${lineItem.quantity}" <c:if test="${lineItem.product.id != null}">id="__quantity_${lineItem.productId}" onchange="javascript:ajaxRequest(${lineItem.productId},'${lineItem.product.sku}');" </c:if> size="5"></td>
			      <c:if test="${lineItem.product.id != null}"><td style="color:#FF0000;width:100px" id="container${lineItem.productId}" width="100%"></td></c:if>
			    </c:when>
			    <c:otherwise>
			      <td><input style="text-align: right;" type="text" name="__quantity_new_${status.index}" value="${lineItem.quantity}" size="5"></td>
			    </c:otherwise>
			  </c:choose>
			  </tr></table>
			  </td>
			  <c:if test="${gSiteConfig['gINVENTORY'] && invoiceForm.order.hasLowInventoryMessage}">
			  <td class="invoice" align="center"><input style="text-align: right;" type="text" name="__low_inventory_message_new_<c:out value='${status.index}'/>" value="${lineItem.lowInventoryMessage}" size="10"></td>
			  </c:if>
			  <c:if test="${invoiceForm.order.hasPacking}">
			  <td class="invoice" align="center"><c:out value="${lineItem.product.packing}" /></td>
			  </c:if>
			  <c:forEach items="${productFieldsHeader}" var="pFHeader"><c:set var="check" value="0"/>
			  <c:if test="${pFHeader.showOnInvoice or pFHeader.showOnInvoiceBackend}">
			  <c:forEach items="${lineItem.productFields}" var="productField"> 
			   <c:if test="${pFHeader.id == productField.id}">
			    <td class="invoice" align="center"><c:out value="${productField.value}" /></td><c:set var="check" value="1"/>
			   </c:if>       
			  </c:forEach>
			  <c:if test="${check == 0}">
			    <td class="invoice">&nbsp;</td>
			   </c:if>
			  </c:if>
			  </c:forEach>
			  <c:if test="${invoiceForm.order.hasContent}">
			  <td class="invoice" align="center"><c:out value="${lineItem.product.caseContent}" /></td>
			  </c:if>
			  <c:if test="${invoiceForm.order.hasCustomShipping}"><td class="invoice" style="text-align:right;"><fmt:formatNumber value="${lineItem.customShippingCost}" pattern="#,##0.00"/></td></c:if>
			  <td style="width:300px;" class="invoice" align="right">
			    <table border="0" cellpadding="1" cellspacing="0" style="width:300px;" >
			      <tr>
			       <td style="width:80px;"><input  style="TEXT-ALIGN: right" type="text" name="__originalPrice_new_<c:out value="${status.index}"/>" value="${lineItem.originalPrice}" size="8" /></td>
			       <td style="width:100px;"><input  style="TEXT-ALIGN: right" type="text" name="__discount_new_<c:out value="${status.index}"/>" value="<c:out value="${lineItem.discount}"/>" size="4" />
			       <select style="width:55px;" name="__percent_new_<c:out value="${status.index}"/>" >
			  	    <option value="true">%</option><option <c:if test="${!lineItem.percent}">selected</c:if> value="false">Fix</option>
			  	   </select>
			  	   </td>
			       <td style="width:80px;"><input  style="TEXT-ALIGN: right" type="text" disabled name="__unitPrice_new_<c:out value="${status.index}"/>" value="${lineItem.unitPrice}" size="8" /></td>
			      </tr>
			    </table>
			  </td>
			  <c:if test="${gSiteConfig['gSPECIAL_PRICING']}" >
			  <td class="invoice" align="center"><input type="checkbox" id="__special_pricing_new_${status.index}" name="__special_pricing_new_${status.index}" <c:if test="${lineItem.setSpecialPricing}">checked</c:if> /></td>
			  </c:if>
			  <c:if test="${gSiteConfig['gPURCHASE_ORDER'] && siteConfig['SHOW_COST'].value == 'true'  && invoiceForm.order.hasCost}" >
			  <td class="invoice"><c:out value="${lineItem.unitCost}" /></td>
			  </c:if>
			  <td class="invoice" align="center"><input type="checkbox" name="__taxable_new_${status.index}" <c:if test="${lineItem.product.taxable}">checked</c:if>></td>
			  <td class="invoice" align="right"><fmt:formatNumber value="${lineItem.totalPrice}" pattern="#,##0.00"/></td>
			</tr>
			<c:if test="${gSiteConfig['gRMA']}">
			<tr bgcolor="#FFFFFF">
			  <td colspan="${8+cols+cols2}">
			    <table cellspacing="3">
			      <tr>
			        <td valign="top"><a onClick="addSerialNum(${status.index}, 'new_')"><img src="../graphics/add.png" width="16" border="0"></a></td>
			        <td valign="top">S/N:</td>
			        <td><div id="__serialNums_new_${status.index}">
				      <c:forEach items="${lineItem.serialNums}" var="serialNum">
					    <input type="text" name="__serialNum_new_${status.index}" value="<c:out value="${serialNum}"/>" size="10" maxlength="50"
					    	<c:if test="${serialNumsMap[fn:toLowerCase(serialNum)] == 'duplicate'}">style="background-color:#EE0000;"</c:if>>,
					  </c:forEach></div></td>
			      </tr>
			    </table>
			  </td>
			</tr>			
			</c:if>			
			</c:forEach>
			  <tr bgcolor="#BBBBBB">
			    <td colspan="${8+cols+cols2}">&nbsp;</td>    
			  </tr>
			  <tr>
			    <td class="invoice" colspan="5" align="right"><fmt:message key="totalWeight" />: <form:input path="order.totalWeight"/></td>
			    <td class="invoice" colspan="${2+cols+cols2}" align="right"><fmt:message key="subTotal" />:</td>
			    <td class="invoice" align="right"><fmt:formatNumber value="${invoiceForm.order.subTotal}" pattern="#,##0.00"/></td>
			  </tr>
			  <c:if test="${invoiceForm.order.budgetEarnedCredits > 0.00 }">
			  	<tr class="customerEarnedCreditsBoxId">
			  		<td class="discount" colspan="${7+cols+cols2}" align="right">Earned Credits: </td>
			  		<td class="discount" align="right"><fmt:formatNumber value="${invoiceForm.order.budgetEarnedCredits}" pattern="-#,##0.00"/></td>
			  	</tr>
		      </c:if>
     		  <c:if test="${invoiceForm.order.promo.discountType eq 'order' }">
			  <tr>
			    <td class="discount" colspan="${7+cols+cols2}" align="right">
			    Discount For Promo Code <c:out value="${invoiceForm.order.promo.title}" />
			    	(<c:choose>
			          <c:when test="${invoiceForm.order.promo.percent}">
			            <fmt:formatNumber value="${invoiceForm.order.promo.discount}" pattern="#,##0.00"/>%
			          </c:when>
			          <c:otherwise>
			            $<fmt:formatNumber value="${invoiceForm.order.promo.discount}" pattern="#,##0.00"/>
			          </c:otherwise>
			      	</c:choose>):
			    </td>
			    <td class="discount" align="right">
			      <c:choose>
			          <c:when test="${invoiceForm.order.promo.percent}">
			            (<fmt:formatNumber value="${invoiceForm.order.subTotal * ( invoiceForm.order.promo.discount / 100.00 )}" pattern="#,##0.00"/>)
			          </c:when>
			          <c:when test="${(!invoiceForm.order.promo.percent) and (invoiceForm.order.promo.discount > invoiceForm.order.subTotal)}">
			        	(<fmt:formatNumber value="${invoiceForm.order.subTotal}" pattern="#,##0.00"/>)
			          </c:when>
			          <c:otherwise>
			            (<fmt:formatNumber value="${invoiceForm.order.promo.discount}" pattern="#,##0.00"/>)
			          </c:otherwise>
			      	</c:choose>	    
			    </td>
			  </tr> 
			  </c:if> 
			  <c:if test="${invoiceForm.order.lineItemPromos != null and fn:length(invoiceForm.order.lineItemPromos) gt 0}">
    			<c:forEach items="${invoiceForm.order.lineItemPromos}" var="itemPromo" varStatus="status">
    			<tr>
      			<td class="discount" colspan="${7+cols+cols2}" align="right">
      			<c:choose>
        			<c:when test="${itemPromo.value == 0}"><fmt:message key="promoCode" /></c:when>
        			<c:otherwise><fmt:message key="discountForPromoCode" /></c:otherwise>
      			</c:choose>
      			<c:out value="${itemPromo.key}" />
      			</td>
      			<td class="discount" align="right">
      			<c:choose>
        			<c:when test="${itemPromo.value == 0}">&nbsp;</c:when>
       			 <c:otherwise>($<fmt:formatNumber value="${itemPromo.value}" pattern="#,##0.00"/>)</c:otherwise>
      			</c:choose>
      			</td>
    			</tr>
    			</c:forEach>  
  			  </c:if>
			  
			  <tr>
			    <td class="invoice" colspan="${7+cols+cols2}" align="right">
			      Tax: 
			      <spring:bind path="invoiceForm.order.taxRate">
			       <input style="TEXT-ALIGN: right" type="text" name="${status.expression}" value="${status.value}">
			      </spring:bind> 
			      %
			    </td>
			    <td class="invoice" align="right"><fmt:formatNumber value="${invoiceForm.order.tax}" pattern="#,##0.00"/></td>
			  </tr>
			    <tr>
			    <td class="invoice" colspan="${7+cols+cols2}" align="right">
			    Shipping & Handling(<spring:bind path="invoiceForm.order.shippingMethod">
			              <input type="text" name="${status.expression}" value="${status.value}" size="15" id="shippingTitle" style="width:150px;">
			             </spring:bind>):
			    </td>
			    <td class="invoice" align="right">
			      <spring:bind path="invoiceForm.order.shippingCost">
			       <input style="TEXT-ALIGN: right"  type="text" name="${status.expression}" value="${status.value}" id="shippingCost">
			      </spring:bind> 
			    </td>
			  </tr>
			  <c:if test="${invoiceForm.order.promo.discountType eq 'shipping'}">
			  <tr>
			    <td class="discount" colspan="${7+cols+cols2}" align="right">
			    Discount For Promo Code <c:out value="${invoiceForm.order.promo.title}" />
			    	(<c:choose>
			          <c:when test="${invoiceForm.order.promo.percent}">
			            <fmt:formatNumber value="${invoiceForm.order.promo.discount}" pattern="#,##0.00"/>%
			          </c:when>
			          <c:otherwise>
			            $<fmt:formatNumber value="${invoiceForm.order.promo.discount}" pattern="#,##0.00"/>
			          </c:otherwise>
			      	</c:choose>):
			    </td>
			    <td class="discount" align="right">
			      <c:choose>
			          <c:when test="${invoiceForm.order.promo.percent}">
			            (<fmt:formatNumber value="${invoiceForm.order.shippingCost * ( invoiceForm.order.promo.discount / 100.00 )}" pattern="#,##0.00"/>)
			          </c:when>
			          <c:when test="${(!invoiceForm.order.promo.percent) and (invoiceForm.order.promo.discount > invoiceForm.order.shippingCost)}">
			        	(<fmt:formatNumber value="${invoiceForm.order.shippingCost}" pattern="#,##0.00"/>)
			          </c:when>
			          <c:otherwise>
			            (<fmt:formatNumber value="${invoiceForm.order.promo.discount}" pattern="#,##0.00"/>)
			          </c:otherwise>
			      	</c:choose>	    
			    </td>
			  </tr> 
			  </c:if>
			  <c:if test="${invoiceForm.order.hasCustomShipping or invoiceForm.order.customShippingCost != null}">
			  <tr>
			  <td class="invoice" colspan="${7+cols+cols2}" align="right">
			    Custom Shipping (<form:input path="order.customShippingTitle"/> )
			  </td>
	  		  <td class="invoice" align="right"><form:input path="order.customShippingCost" cssStyle="TEXT-ALIGN: right"/></td>
	  		  </tr>
    		  </c:if>
			  <c:if test="${invoiceForm.order.creditUsed != null}">
			  <tr>
			    <td class="discount" colspan="${7+cols+cols2}" align="right"><fmt:message key="credit" />:</td>
			    <td class="discount" align="right"><fmt:formatNumber value="${invoiceForm.order.creditUsed}" pattern="-#,##0.00"/></td>
			  </tr>
			  </c:if>
			  <tr>
			    <td class="invoice" colspan="${7+cols+cols2}" align="right">
			      CC Fee: <form:input path="order.ccFeeRate" cssStyle="TEXT-ALIGN: right"/>
			      <form:select path="order.ccFeePercent" cssStyle="width:45px;">
			        <form:option value="true">%</form:option>
			        <form:option value="false">Fix</form:option>
			      </form:select>
			    </td>
			    <td class="invoice" align="right"><fmt:formatNumber value="${invoiceForm.order.ccFee}" pattern="#,##0.00"/></td>
			  </tr>
			  <c:if test="${invoiceForm.order.orderId == null and siteConfig['BUY_SAFE_URL'].value != ''}">
			  <tr>
			    <td class="invoice" colspan="${7+cols+cols2}" align="right">
			    <fmt:message key="buySafeBondGuarntee" />: 
			    <select name="wantsBond" onchange="buySAFEOnClick(this.value);" style="width:60px;">
			      <option value="true" <c:if test="${invoiceForm.order.wantsBond == 'true'}">selected="selected" </c:if>>Yes</option>
			      <option value="false" <c:if test="${invoiceForm.order.wantsBond == 'false'}">selected="selected"</c:if>>No</option>
			    </select>
			   	</td>
			    <td class="invoice" align="right"><fmt:formatNumber value="${invoiceForm.order.bondCost}" pattern="#,##0.00"/></td>
			  </tr>
			  </c:if>			  
			  <c:if test="${invoiceForm.order.bondCost != null and invoiceForm.order.orderId != null}">
			  <tr>
			    <td class="invoice" align="right" colspan="5">
			    <c:if test="${!fn:contains(invoiceForm.order.status,'x')}">
			    	<a style="width: 100px; float: right; text-align: center; background-color:#FF9999; color:WHITE;" href="invoice.jhtm?order=${invoiceForm.order.orderId}&__cancelBuySafe=true"><fmt:message key="cancel" /></a>
			    </c:if>
			    </td>
			    <td class="invoice" colspan="${2+cols+cols2}" align="right"><fmt:message key="buySafeBondGuarntee" />:</td>
			    <td class="invoice" align="right"><fmt:formatNumber value="${invoiceForm.order.bondCost}" pattern="#,##0.00"/></td>
			  </tr>
			  </c:if>			  
			  <tr>
			    <td class="invoice" colspan="${7+cols+cols2}" align="right"><fmt:message key="grandTotal" />:</td>
			    <td align="right" bgcolor="#F8FF27" >
			      <div id="grandTotal"> <fmt:formatNumber value="${invoiceForm.order.grandTotal}" pattern="#,##0.00"/> </div>
			      <input id="grandTotalValue" type="hidden" value="${invoiceForm.order.grandTotal - invoiceForm.order.shippingCost - invoiceForm.order.customShippingCost}"/>
			    </td>
			  </tr>
			</table>
		    <c:if test="${partnerList != null and fn:length(partnerList) gt 0}">
			<div class="partnersBox">
			<b><fmt:message key="partnersList"/></b>
			    	<c:forEach items="${partnerList}" var="partner">
			    		<div class="wrapper">
			    		<c:out value="${partner.partnerName}"/>
			    		<c:out value="${partner.amount}"/>
			    		</div>
			    	</c:forEach>
			    	<div><fmt:message key="total"/>: <fmt:message key="${siteConfig['CURRENCY'].value}" /><fmt:formatNumber value="${invoiceForm.order.budgetEarnedCredits}" pattern="#,##0.00"/></div>
			</div>
			</c:if>	
						
			<c:if test="${shippingRates != null}">
			<div class="shippingRatesWrapper" id="shippingRatesWrapperId">
              <div class="shippingRatesTitle"><b><fmt:message key="shipping"/></b></div>
              <c:if test="${invoiceForm.order.shippingMessage != null and !empty invoiceForm.order.shippingMessage}">
				<div class="shippingMessage1" id="shippingMessageId1">
				<c:out value="${invoiceForm.order.shippingMessage}" escapeXml="false"></c:out>
				</div>
			  </c:if>
			  <table width="80%">
                <c:forEach items="${shippingRates}" var="shippingrate" varStatus="loopStatus">
                <tr>
                  <td align="center" height="45px" width="10%">
                    <c:if test="${shippingrate.carrier == 'fedex'}" >
                    <c:set var="fedEx" value="true" />
                    <c:choose>
         		      <c:when test="${fn:contains(shippingrate.code,'EXPRESS') or fn:contains(shippingrate.code,'OVERNIGHT') or fn:contains(shippingrate.code,'2_DAY')}"><img width="80%" src="../../assets/Image/Layout/FedEx_Express_Normal_2Color_Positive_20.gif" /></c:when>
				      <c:when test="${fn:contains(shippingrate.code,'GROUND_HOME')}"><img width="80%" src="../../assets/Image/Layout/FedEx_HomeDelivery_Normal_2Color_Positive_20.gif" /></c:when>
				      <c:when test="${fn:contains(shippingrate.code,'GROUND')}"><img width="80%" src="../../assets/Image/Layout/FedEx_Ground_Normal_2Color_Positive_20.gif" /></c:when>
				      <c:when test="${fn:contains(shippingrate.code,'FREIGHT')}"><img width="80%" src="../../assets/Image/Layout/FedEx_Freight_Normal_2Color_Positive_20.gif" /></c:when>
				      <c:otherwise><img width="80%" src="../../assets/Image/Layout/FedEx_MultipleServices_Normal_2Color_Positive_20.gif" /></c:otherwise>
			        </c:choose>
			        </c:if>
			        <c:if test="${shippingrate.carrier == 'ups'}" >
			          <c:set var="ups" value="true" />
			          <img width="30%" src="../../assets/Image/Layout/UPS_LOGO_S.gif" />
			        </c:if>
			      </td>
			      <td width="5%">
			        <input type="radio" onchange="updateShipping('${shippingrate.title}','${shippingrate.price}');" id="scr_${loopStatus.index}" name="shippingRate" value="${loopStatus.index}" <c:if test="${(status.value == loopStatus.index) || (shippingrate.title == 'Free')}">checked</c:if>>
			      </td>
			      <td width="35%">
			        <c:out value="${shippingrate.title}" escapeXml="false"/>
			      </td>
			      <td align="right" width="15%">
			        <c:if test="${shippingrate.price != null}"><fmt:message key="${siteConfig['CURRENCY'].value}" /></c:if><fmt:formatNumber value="${shippingrate.price}" pattern="#,##0.00"/>
			      </td>
			      <c:if test="${siteConfig['DELIVERY_TIME'].value == 'true' and shippingrate.carrier == 'ups'}">
			      <td style="padding-left:10px;"align="center" width="35%">
			        <c:choose>
			 	      <c:when test="${shippingrate.code == '03' and empty shippingrate.deliveryDate}"><a onclick="window.open(this.href,'','width=740,height=550'); return false;" href="category.jhtm?cid=49">See Map</a></c:when>
			 	      <c:when test="${shippingrate.code == '03' and !empty shippingrate.deliveryDate}">Delivery Date: <fmt:formatDate type="date" timeStyle="default" value="${shippingrate.deliveryDate}"/><div style="font-size:10px;color:#444444;">( Not Guaranteed )</div></c:when>
			 	      <c:otherwise>Delivery Date: <fmt:formatDate type="date" timeStyle="default" value="${shippingrate.deliveryDate}"/></c:otherwise>
			 	    </c:choose>
			      </td>
			      </c:if> 
			      <c:if test="${shippingrate.description != null}">
			      <td>
			        <c:out value="${shippingrate.description}" escapeXml="false"/>
			      </td>
			      </c:if>
			    </tr>
			    </c:forEach>
			  </table>
			</div>
			</c:if>
			<table>
			<tr>
			  <td colspan="3"><b>Promo Discount:</b></td>
			</tr>
			<tr>
			  <td>&nbsp;</td>
			  <td><fmt:message key="code" />:</td>
			  <td>
			    <form:input path="order.promo.title" cssStyle="TEXT-ALIGN: left" disabled="${!gSiteConfig['gSALES_PROMOTIONS']}" id="promoTitle"/>
			    <form:errors path="order.promo.title" cssClass="error" />  
			  	<select name="promoSelected" id="promoSelected" onChange="choosePromo(this)" <c:if test="${!gSiteConfig['gSALES_PROMOTIONS']}" >disabled="disabled"</c:if>>
			  	  <option value="">Choose existing Promo Code</option> 
			  	  <c:forEach items="${promoList}" var="promo"	varStatus="status">
			  	    <option value="${promo.title}"><c:out value="${promo.title}" /></option>
			      </c:forEach>
			  	</select>
			    <c:forEach items="${promoList}" var="promo"	varStatus="status">
			  	  <input type="hidden" id="discount_${promo.title}" value="${promo.discount}" />
			  	  <input type="hidden" id="discount_type_${promo.title}" value="${promo.discountType}" />
			  	  <input type="hidden" id="percent_${promo.title}" value="${promo.percent}" />
			    </c:forEach>
			    <input type="hidden" id="discount_" value="" />
			    <input type="hidden" id="discount_type_" value="" />
			  	<input type="hidden" id="percent_" value="" />
			  </td>
			</tr>
			<tr>
			  <td>&nbsp;</td>
			  <td><fmt:message key="discount" />:</td>
			  <td><div>
			      <form:input path="order.promo.discount" cssStyle="TEXT-ALIGN: left" disabled="${!gSiteConfig['gSALES_PROMOTIONS']}" id="promoDiscount"/>
			      <form:errors path="order.promo.discount" cssClass="error" />  
			      <form:select path="order.promo.percent" disabled="${!gSiteConfig['gSALES_PROMOTIONS']}" id="promoPercent">
			        <form:option value="false"><fmt:message key="FIXED" />$</form:option>
			  	    <form:option value="true">%</form:option>
			      </form:select>
			      <form:errors path="order.promo.percent" cssClass="error" />  
				  </div>
			  </td>
			</tr>
			<c:if test="${gSiteConfig['gSALES_PROMOTIONS_ADVANCED']}">
			<tr>
			  <td>&nbsp;</td>
			  <td><fmt:message key="discount" /> <fmt:message key="type" />:</td>
			  <td><div>
			      <form:select path="order.promo.discountType" disabled="${!gSiteConfig['gSALES_PROMOTIONS']}" id="promoDiscountType">
			        <form:option value="order"><fmt:message key="order" /></form:option>
			  	    <form:option value="shipping"><fmt:message key="shipping" /></form:option>
			      </form:select>
			      <form:errors path="order.promo.discountType" cssClass="error" />  
				  </div>
			  </td>
			</tr>
			</c:if>
			</table>

			<c:if test="${fn:contains(siteConfig['CUSTOMIZE_ORDER_LIST'].value,'priority')}">
			<br/><b><fmt:message key="priority" />:</b> 
		  	    <c:forEach begin="0" end="2" var="priority">
	  			<form:radiobutton value="${priority}" path="order.priority" /><img src="../graphics/priority_${priority}.png" border="0">&nbsp;&nbsp;&nbsp;
		  	    </c:forEach>			
			</c:if>
			
			<c:if test="${fn:contains(siteConfig['CUSTOMIZE_ORDER_LIST'].value,'deliveryPerson')}">
			<br/><b><fmt:message key="deliveryPerson" />:</b> <form:input path="order.deliveryPerson" maxlength="50" htmlEscape="true"/>			
			</c:if>
			
			<table style="clear:both;margin-top:10px;" width="100%">
			<c:forEach items="${customerFields}" var="customerField" varStatus="status">
				<tr><td><c:out value="${customerField.name}" />: <c:out value="${customerField.value}" /></td></tr>
			</c:forEach>
			</table>

		    <br>
			<c:if test="${(siteConfig['EDIT_COMMISSION'].value == 'true') && (gSiteConfig['gAFFILIATE'] > 0) && (invoiceForm.order.affiliate.totalCommissionLevel1 != null || invoiceForm.order.affiliate.totalCommissionLevel2 != null)}"> 
			<table border="0" cellpadding="2" cellspacing="1" width="30%">
			<tr>
			  <td><b><fmt:message key="commissionLevel"/></b></td>
			  <td><b><fmt:message key="amount"/></b></td>
			</tr>
			<c:if test="${invoiceForm.order.affiliate.totalCommissionLevel1 != null}">
			<tr>
			  <td><fmt:message key="commissionLevel1"/></td>
			  <td><input type="text" name="__commissionLevel1" value="<c:out value="${invoiceForm.order.affiliate.totalCommissionLevel1}"/>"></input></td>
			</tr>
			</c:if>
			<c:if test="${invoiceForm.order.affiliate.totalCommissionLevel2 != null}">
			<tr>
			  <td><fmt:message key="commissionLevel2"/></td>
			  <td><input type="text" name="__commissionLevel2" value="<c:out value="${invoiceForm.order.affiliate.totalCommissionLevel2}"/>"></input></td>
			</tr>
			</c:if>
			</table>
	        <br>
			</c:if>
		
			<table width="100%">
				<tr >
				  <td valign="top"><br /> 
				    <b><c:out value="${siteConfig['SPECIAL_INSTRUCTIONS'].value}"/>:</b>  	
				  </td>
				</tr>
				<tr>
				  <td>
				  <spring:bind path="invoiceForm.order.invoiceNote">
				    <textarea name="<c:out value="${status.expression}"/>" rows="8" cols="100" wrap="soft" class="textfield"><c:out value="${status.value}"/></textarea>
			      </spring:bind>
				  </td>
				</tr>
			</table>

			<table width="100%">
			<tr>
				<td>
					<c:if test="${invoiceForm.order.workOrderNum == null or canEdit}" >
						<c:forEach begin="1" end="20" step="1" var="skuIndex">
					 <b><fmt:message key="add" /> <fmt:message key="productSku" /><fmt:formatNumber value="${skuIndex}" minIntegerDigits="2" pattern="##" /></b>: <input type="text" style="width:75%;" id="addSku${skuIndex}" name="__addProduct${skuIndex}_sku" value=""/> Qty:<fmt:formatNumber value="${skuIndex}" minIntegerDigits="2" pattern="##" /><input type="text" style="width:50px;" id="addQty${skuIndex}" name="__addProduct${skuIndex}_qty" value=""/> <br />
					</c:forEach>
					</c:if>
				</td>
				<td valign="bottom" align="right" style="width:150px;">
					<input type="submit" name="__update" value="<fmt:message key="recalculate"/>">
					<sec:authorize ifAnyGranted="ROLE_AEM,ROLE_ADMIN_${prop_site_id},ROLE_INVOICE_EDIT">
					<c:if test="${!invoiceForm.newInvoice}">
					<input type="submit" name="__save" value="<fmt:message key="saveChanges"/>">
					</c:if>
					</sec:authorize>
					<c:if test="${invoiceForm.newInvoice and (gSiteConfig['gADD_INVOICE'] or gSiteConfig['gSERVICE'])}">
					<input type="submit" name="__save" value="<fmt:message key="addQuote"/>" onclick="return checkRequiredValues();" />
					</c:if>
					<input type="submit" value="<fmt:message key="cancel"/>" name="_cancel">
				</td>
			</tr>
			</table>
		    <!-- end input field -->   	

    <!-- end tab -->        
	</div>     
  </div>    
	
<!-- end tabs -->			
</div>
  
  <!-- end table -->
    </td><td class="boxmidr" ></td></tr>
  <tr><td class="botl"></td><td class="botr"></td></tr>
  </table>
  
<!-- end main box -->  
 </div>

<!-- start button -->
<!-- end button -->	     
</c:if>
</form:form>
<script language="JavaScript">
<!--	

function toggleStateProvince(el, type) {
  if (el.value == "US") {
      document.getElementById(type + '_state').disabled=false;
      document.getElementById(type + '_state').style.display="block";
  	  document.getElementById(type + '_ca_province').disabled=true;
      document.getElementById(type + '_ca_province').style.display="none";
      document.getElementById(type + '_province').disabled=true;
      document.getElementById(type + '_province').style.display="none";
  } else if (el.value == "CA") {
      document.getElementById(type + '_state').disabled=true;
      document.getElementById(type + '_state').style.display="none";
      document.getElementById(type + '_ca_province').disabled=false;
      document.getElementById(type + '_ca_province').style.display="block";
      document.getElementById(type + '_province').disabled=true;
      document.getElementById(type + '_province').style.display="none";
  } else {
      document.getElementById(type + '_state').disabled=true;
      document.getElementById(type + '_state').style.display="none";
      document.getElementById(type + '_ca_province').disabled=true;
      document.getElementById(type + '_ca_province').style.display="none";
      document.getElementById(type + '_province').disabled=false;
      document.getElementById(type + '_province').style.display="block";
  }
}
toggleStateProvince(document.getElementById('order.billing.country'), 'billing');
toggleStateProvince(document.getElementById('order.shipping.country'), 'shipping');
toggleStateProvince(document.getElementById('order.source.country'), 'source');
<c:if test="${gSiteConfig['gSPECIAL_PRICING']}" >
function toggleSpecialPricing(el, id) {
  if (el.checked) {
	document.getElementById(id).checked = false;
  }
}
</c:if>
<c:if test="${!invoiceForm.newInvoice and siteConfig['CHANGE_ORDER_DATE'].value == 'true'}">
  Calendar.setup({
      inputField     :    "orderDate",   // id of the input field
      showsTime      :    true,
      ifFormat       :    "%m/%d/%Y[%I:%M %p]",   // format of the input field
      button         :    "time_start_trigger" // trigger for the calendar (button ID)
  });
</c:if>  
//-->
</script>

 </tiles:putAttribute>    
</tiles:insertDefinition>
