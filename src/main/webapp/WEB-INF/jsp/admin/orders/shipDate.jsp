<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<c:if test="${model.shipDate != null}">
<fmt:formatDate type="date" value="${model.shipDate}" pattern="M/dd/yy"/>
</c:if>