<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
  <head>
    <title>Invoice</title>
    <link href="<c:url value="/assets/invoice.css"/>" rel="stylesheet" type="text/css"/>
  </head>  
<body class="invoice">
<div style="width:700px;margin:left;">


<c:if test="${order != null}">

<c:out value="${packingLayout.headerHtml}" escapeXml="false"/>

<table border="0" width="100%" cellspacing="3" cellpadding="1">
<tr valign="top">
<td>
<b><fmt:message key="shippingInformation" />:</b>
<c:set value="${order.shipping}" var="address"/>
<%@ include file="/WEB-INF/jsp/frontend/common/address.jsp" %>
</td>
<td>&nbsp;</td>
<td align="right">
<table>
  <tr>
    <td><fmt:message key="invoice" /> #</td>
    <td>:&nbsp;&nbsp;</td>
    <td><b><c:out value="${order.orderId}"/></b></td>
  </tr>
  <tr>
    <td><fmt:message key="date" /></td>
    <td>:&nbsp;&nbsp;</td>
    <td><fmt:formatDate type="date" timeStyle="default" value="${order.dateOrdered}"/></td>
  </tr>
</table>
</td>
</tr>
</table>
<hr>
<p>
<table border="0" cellpadding="2" cellspacing="1" width="100%" class="invoice">
  <tr>
    <c:set var="cols" value="0"/>
    <th width="5%" class="invoice"><fmt:message key="line" />#</th>
    <th class="invoice"><fmt:message key="productSku" /></th>
    <th class="invoice"><fmt:message key="productName" /></th>
    <c:forEach items="${productFieldsHeader}" var="productField">
      <c:if test="${productField.packingField}">
      <th class="invoice"><c:out value="${productField.name}" /></th><c:set var="cols" value="${cols+1}"/>
      </c:if>
    </c:forEach>
    <th width="10%" class="invoice"><fmt:message key="quantity" /></th>
    <c:if test="${order.hasPacking}">
	  <c:set var="cols" value="${cols+1}"/>
      <th width="10%" class="invoice"><fmt:message key="packing" /></th>
    </c:if>
    <c:if test="${order.hasContent}">
	  <c:set var="cols" value="${cols+1}"/>
      <th class="invoice"><fmt:message key="content" /></th>
	  <c:if test="${!empty siteConfig['EXT_QUANTITY_TITLE'].value}">
       <c:set var="cols" value="${cols+1}"/>
       <th class="invoice"><c:out value="${siteConfig['EXT_QUANTITY_TITLE'].value}" /></th>
      </c:if>
    </c:if>
  </tr>
<c:forEach var="lineItem" items="${order.lineItems}" varStatus="status">
<tr valign="top">
  <td class="invoice" align="center"><c:out value="${status.count}"/></td>
  <td class="invoice"><c:out value="${lineItem.product.sku}"/></td>
  <td class="invoice"><c:out value="${lineItem.product.name}"/>
<c:forEach items="${lineItem.productAttributes}" var="productAttribute">
<div class="invoice_lineitem_attributes">- <c:out value="${productAttribute.optionName}"/>: <c:out value="${productAttribute.valueName}"/>
<c:if test="${(! empty productAttribute.optionPriceOriginal) && productAttribute.optionPriceOriginal != 0}">
	<c:out value="${productAttribute.optionPriceOriginal}"/> <fmt:message key="${productAttribute.optionPriceMessageF}"/>
</c:if>
</div>
</c:forEach>
  </td>
  <c:forEach items="${productFieldsHeader}" var="pFHeader" varStatus="status"><c:set var="check" value="0"/>
  <c:if test="${pFHeader.packingField}">
  <c:forEach items="${lineItem.productFields}" var="productField"> 
   <c:if test="${pFHeader.id == productField.id}">
    <td class="invoice"><c:out value="${productField.value}" /></td><c:set var="check" value="1"/>
   </c:if>       
  </c:forEach>
  <c:if test="${check == 0}">
    <td class="invoice">&nbsp;</td>
   </c:if>
  </c:if>
  </c:forEach>
  <td class="invoice" align="center"><c:out value="${lineItem.quantity}"/></td>
  <c:if test="${order.hasPacking }">
  <td class="invoice" align="center"><c:out value="${lineItem.product.packing}" /></td>
  </c:if>
  <c:if test="${order.hasContent}">
    <td class="invoice" align="center"><c:out value="${lineItem.product.caseContent}" /></td>
    <c:if test="${!empty siteConfig['EXT_QUANTITY_TITLE'].value}">
    <td class="invoice" align="center">
     <c:choose>
      <c:when test="${!empty lineItem.product.caseContent}"><c:out value="${lineItem.product.caseContent * lineItem.quantity}" /></c:when>
      <c:otherwise><c:out value="${lineItem.quantity}" /></c:otherwise> 
     </c:choose>
    </td>
    </c:if>
  </c:if>
</tr>
</c:forEach>
  <tr bgcolor="#BBBBBB">
    <td colspan="${4+cols}">&nbsp;</td>
  </tr>
</table>
<p>
<hr>
<c:out value="${packingLayout.footerHtml}" escapeXml="false"/>
</c:if>

</div>

</body>
</html>