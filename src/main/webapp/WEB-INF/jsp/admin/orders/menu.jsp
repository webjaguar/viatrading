<%@ page import="com.webjaguar.model.*,java.util.*"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ taglib uri="http://www.springframework.org/security/tags" prefix="sec" %>

<link rel="stylesheet" type="text/css" media="all" href="../../admin/javascript/jscalendar-1.0/skins/aqua/theme.css" title="Aqua" /> <!-- calendar stylesheet -->
<script type="text/javascript" src="../../admin/javascript/jscalendar-1.0/calendar.js"></script> <!-- main calendar program -->
<script type="text/javascript" src="../../admin/javascript/jscalendar-1.0/lang/calendar-en.js"></script>
<script type="text/javascript" src="../../admin/javascript/jscalendar-1.0/calendar-setup.js"></script>
<!-- menu -->
<script src="../javascript/MenuMatic_0.68.3.js" type="text/javascript" charset="utf-8"></script>
<script type="text/javascript">
<!--
window.addEvent('domready', function(){
	var Tips1 = new Tips($$('.toolTipImg'));
	var myMenu = new MenuMatic({ orientation:'vertical' });
	var opSearchBox = new multiBox('mbOperator', {showControls : false, useOverlay: false, showNumbers: false });
});
function selectOperator(e) {
	document.getElementById('skuOperator').value = e;
}
function selectParentSkuOperator(e) {
	document.getElementById('product_parent_skuOperator').value = e;
}
function selectShipOperator(e) {
	document.getElementById('shippingOperator').value = e;
}

function UpdateStartEndDate(type){
var myDate = new Date();
$('endDate').value = (myDate.getMonth()+1) + "/" + myDate.getDate() + "/" + myDate.getFullYear();
	if ( type == 0) {
		var day = myDate.getDate()-1;
		if(day == 0) {
			// 1 day = 1 * 24 hours = 24 hours 
			// 24 hours = 24 * 60 min = 1440 min 
			// 1440 min = 1440 * 60 sec = 86400 sec 
			// 86400 sec = 86400 * 1000 ms = 86400000 ms 
			var yesterday = new Date(myDate.getTime() - 86400000);
			$('startDate').value = (yesterday.getMonth()+1) + "/" + yesterday.getDate() + "/" +yesterday.getFullYear();
			$('endDate').value = (yesterday.getMonth()+1) + "/" +yesterday.getDate() + "/" + yesterday.getFullYear();
		} else {
			$('startDate').value = (myDate.getMonth()+1) + "/" + day + "/" + myDate.getFullYear();
			$('endDate').value = (myDate.getMonth()+1) + "/" + day + "/" + myDate.getFullYear();
		}
	} else if ( type == 2 ) {
		myDate.setDate(myDate.getDate() - myDate.getDay());
		$('startDate').value = (myDate.getMonth()+1) + "/" + myDate.getDate() + "/" + myDate.getFullYear();
	} else if ( type == 3 ) {
		var last14thDay = new Date(myDate.getTime() - 86400000*14);
		$('startDate').value = (last14thDay.getMonth()+1) + "/" + last14thDay.getDate() + "/" + last14thDay.getFullYear();
	} else if ( type == 4 ) {
		myDate.setDate(1);
		$('startDate').value = (myDate.getMonth()+1) + "/" + myDate.getDate() + "/" + myDate.getFullYear();
	} else if ( type == 10 ) {
		myDate.setMonth(0, 1);
		$('startDate').value = (myDate.getMonth()+1) + "/" + myDate.getDate() + "/" + myDate.getFullYear();
	} else if ( type == 20 ) {
		$('startDate').value = "";
		$('endDate').value = "";
	} else{
		$('startDate').value = (myDate.getMonth()+1) + "/" + myDate.getDate() + "/" + myDate.getFullYear();
	}
}
function submitSearchForm(fileType) {
	if (typeof document.forms['searchform'] != 'undefined') {
	  document.searchform.action = "invoiceExport.jhtm?fileType2="+fileType;
	  document.searchform.submit();
	} else {
	  myform=document.createElement("form");
	  document.body.appendChild(myform);
      myform.method = "POST";
      myform.action= "invoiceExport.jhtm?fileType2="+fileType;
      myform.submit();
	}
}
//-->
</script>
  <div id="lbox" class="quickMode">
   <table cellpadding="0" cellspacing="0" border="0" class="module" style="width: 100%"><tr><td class="topl">
    </td><td class="topr" ></td></tr><tr><td class="boxmid" >
  
    <h2 class="menuleft mfirst"><fmt:message key="${orderName}"/></h2>
    <div class="menudiv"></div>
    <ul id="nav">
      <li><a href="ordersList.jhtm?orderName=${orderName}" class="userbutton"><fmt:message key="list"/></a></li>
      <c:if test="${gSiteConfig['gSUBSCRIPTION'] and orderName != 'quotes'}">	
        <li><a href="subscriptionsList.jhtm" class="userbutton"><fmt:message key="subscriptions"/></a></li>
      </c:if>
      <sec:authorize ifAnyGranted="ROLE_AEM,ROLE_ADMIN_${prop_site_id},ROLE_INVOICE_IMPORT_EXPORT">
      <c:if test="${gSiteConfig['gINVOICE_EXPORT']}">	
      <li><a href="#" class="userbutton"><fmt:message key="export"/></a>
        <ul>
           <li><a href="javascript: submitSearchForm('xls')" class="userbutton">EXCEL</a></li>
           <c:forTokens items="${siteConfig['INVOICE_EXPORT'].value}" delims="," var="type">
	   		  <li><a href="javascript: submitSearchForm('${type}')" class="userbutton"><c:out value="${fn:toUpperCase(type)}"/></a></li>
    	   </c:forTokens>
    	   <c:if test="${siteConfig['SHOPWORKS'].value == 'true'}">
    	      <li><a href="shopworksList.jhtm" class="userbutton">Shopworks</a></li>
    	   </c:if>     
    	   <li><a href="exportOrders.jhtm" class="userbutton">Watson</a></li>
        </ul>
      </li>
      </c:if>
      </sec:authorize>
      <sec:authorize ifAnyGranted="ROLE_AEM,ROLE_ADMIN_${prop_site_id},ROLE_EMAILMANAGER_CREATE">
      <c:if test="${gSiteConfig['gMASS_EMAIL']}">	
      <li><a href="orderMassEmail.jhtm" class="userbutton" id="massEmailLink"><fmt:message key="massEmail"/></a>
        <c:if test="${siteConfig['MASS_EMAIL_UNIQUEPER_ORDER'].value == 'true'}">	
        <ul>
          <li><a href="orderMassEmail.jhtm?uniquePerOrder=true" class="userbutton">Unique Per Order</a></li>
        </ul>
        </c:if>
      </li>
      </c:if>
      </sec:authorize>
    </ul>
    <div class="menudiv"></div>
    
    <c:if test="${tab == 'orders'}"> 
    <script type="text/javascript" src="../javascript/side-bar.js"></script>
    <script type="text/javascript" >
    window.addEvent('domready', function(){	
		$('searchHeaderId').addEvent('click',function(){
			document.searchform.submit();
		});
    });
    </script>
    <div class="leftbar_searchWrapper">
      <div align="left" class="searchHeader" id="searchHeaderId"><h2><fmt:message key="search" /></h2></div>
	  <div class="searchformBox">
	  <form action="ordersList.jhtm?orderName=${orderName}" name="searchform" method="post" >
	  <table class="searchBoxLeft"><tr><td valign="top" style="width: 110px;">
	  <!-- content area -->
	   <c:if test="${gSiteConfig['gADD_INVOICE']}">
	    <div class="search2">
	      <p><fmt:message key="generatedFrom" />:</p>
	      <select name="backendOrder">
	  	    <option value=""> </option>
	  	    <option value="1" <c:if test="${orderSearch.backendOrder == '1'}">selected</c:if>><fmt:message key="backend" /></option>
	  	    <option value="0" <c:if test="${orderSearch.backendOrder == '0'}">selected</c:if>><fmt:message key="frontend" /></option>
	  	  </select>
	    </div>
	    </c:if>
		<c:if test="${gSiteConfig['gMULTI_STORE'] > 0}">
	    <div class="search2">
	      <p><fmt:message key="multiStore" />:</p>
	      <select name="host">
	  	    <option value=""> </option>
	  	    <option value="main" <c:if test="${orderSearch.host == 'main'}">selected</c:if>><fmt:message key="main" /></option>
	  	    <c:forEach items="${multiStores}" var="multiStore">
	  	    <option value="${multiStore.host}" <c:if test="${orderSearch.host == multiStore.host}">selected</c:if>><c:out value="${multiStore.host}" /></option>
	  	    </c:forEach>
	       </select>
	    </div>
	    </c:if>
	    <div class="search2">
	    <c:choose>
	        <c:when test="${(gSiteConfig['gSHIPPING_QUOTE'] or siteConfig['PRODUCT_QUOTE'].value == 'true') and orderName == 'quotes'}">
	          <input type="hidden" name="status" value="xq" />
	        </c:when>
	        <c:otherwise>
	        	<p><fmt:message key="status" />:</p> 
	        	<select name="status">
			  	    <option value=""> </option>
			  	    <option value="p" <c:if test="${orderSearch.status == 'p'}">selected</c:if>><fmt:message key="orderStatus_p" /></option>
			  	    <option value="pr" <c:if test="${orderSearch.status == 'pr'}">selected</c:if>><fmt:message key="orderStatus_pr" /></option>
			  	    <option value="wp" <c:if test="${orderSearch.status == 'wp'}">selected</c:if>><fmt:message key="orderStatus_wp" /></option>
			  	    <option value="rls" <c:if test="${orderSearch.status == 'rls'}">selected</c:if>><fmt:message key="orderStatus_rls" /></option>
			  	    <%-- <option value="ppr" <c:if test="${orderSearch.status == 'ppr'}">selected</c:if>><fmt:message key="orderStatus_ppr" /></option> --%>
			  	    <c:if test="${siteConfig['AMAZON_URL'].value != ''}">
			  	    <option value="ars" <c:if test="${orderSearch.status == 'ars'}">selected</c:if>><fmt:message key="orderStatus_ars" /></option>
					</c:if>
					<c:if test="${siteConfig['AMAZON_URL'].value != ''}">
			  	    <option value="prars" <c:if test="${orderSearch.status == 'prars'}">selected</c:if>><fmt:message key="orderStatus_prars" /></option>
					</c:if>
			  	    <option value="s" <c:if test="${orderSearch.status == 's'}">selected</c:if>><fmt:message key="orderStatus_s" /></option>
			  	    <option value="x" <c:if test="${orderSearch.status == 'x'}">selected</c:if>><fmt:message key="orderStatus_x" /></option>
					<c:if test="${gSiteConfig['gPAYPAL'] > 0 && gSiteConfig['gPAYPAL'] != 3}">	  	    
			  	    <option value="xp" <c:if test="${orderSearch.status == 'xp'}">selected</c:if>><fmt:message key="orderStatus_xp" /></option>
			  	    </c:if>
			  	    <c:if test="${gSiteConfig['gCREDIT_CARD_PAYMENT'] == 'netcommerce'}">
			  	    <option value="xnc" <c:if test="${orderSearch.status == 'xnc'}">selected</c:if>><fmt:message key="orderStatus_xnc" /></option>
			  	    </c:if>
			  	    <c:if test="${siteConfig['GOOGLE_CHECKOUT_URL'].value != ''}">
			  	    <option value="xg" <c:if test="${orderSearch.status == 'xg'}">selected</c:if>><fmt:message key="orderStatus_xg" /></option>
					</c:if>
					<option value="pprs" <c:if test="${orderSearch.status == 'pprs'}">selected</c:if>><fmt:message key="orderStatus_pprs" /></option>
					<option value="nxs" <c:if test="${orderSearch.status == 'nxs'}">selected</c:if>><fmt:message key="orderStatus_not_x_not_s" /></option>
		  	   </select>
	        </c:otherwise>
	      </c:choose>
	    </div>
	    <c:if test="${siteConfig['SUB_STATUS'].value != '0'}">
	    <div class="search2">
	      <p><fmt:message key="released" />:</p>
	       <select name="sub_status">
	  	    <option value="blank" <c:if test="${orderSearch.subStatus == 'blank'}">selected</c:if>><fmt:message key="orderSubStatus_" /></option>
	  	    <option value='emptyOrNull' <c:if test="${orderSearch.subStatus == 'emptyOrNull'}">selected</c:if>>Empty Or Null</option>
			  <c:forEach begin="0" end="${siteConfig['SUB_STATUS'].value}" var="value">
			    <option value="${value}" <c:if test="${orderSearch.subStatus == fn:trim(value)}">selected</c:if>><fmt:message key="orderSubStatus_${value}" /></option>
			  </c:forEach>
	  	   </select>
	  	</div>    
	    </c:if>
	    <c:if test="${gSiteConfig['gSALES_REP']}">
	    <div class="search2">
	      <p><fmt:message key="salesRep" />:</p>
	      <select name="sales_rep_id">
	       <c:if test="${model.salesRepTree == null}">  
	       <option value="-1"> </option>
	       <option value="-2" <c:if test="${orderSearch.salesRepId == -2}">selected</c:if>><fmt:message key="noSalesRep" /></option>
	       <c:forEach items="${model.salesRepMap}" var="salesRep">
	  	     <option value="${salesRep.value.id}" <c:if test="${orderSearch.salesRepId == salesRep.value.id}">selected</c:if>><c:out value="${salesRep.value.name}" /></option>
	       </c:forEach>
	       <c:forEach items="${model.salesRepMapInactive}" var="salesRep">
	  	     <option class="nameLinkInactive" value="${salesRep.value.id}" <c:if test="${orderSearch.salesRepId == salesRep.value.id}">selected</c:if>><c:out value="${salesRep.value.name}" /></option>
	       </c:forEach>
	       </c:if>
	       <c:if test="${model.salesRepTree != null}">
	         <c:set var="salesRepTree" value="${model.salesRepTree}" scope="request"/>
	         <c:set var="orderSearchSalesRepId" value="${orderSearch.salesRepId}"/>
	         <option value="-1"> </option>
	         <option value="${salesRepTree.id}" <c:if test="${orderSearch.salesRepId == salesRepTree.id}">selected</c:if>><c:out value="${salesRepTree.name}" /></option>
			 <c:set var="recursionPageId" value="${orderSearchSalesRepId}" scope="request"/>
	      	 <c:set var="level" value="1" scope="request"/>
			 <jsp:include page="/WEB-INF/jsp/admin/orders/recursion.jsp"/>
	      	
	       </c:if>
	      </select>
	    </div>  
	    <div class="search2">
	      <p><fmt:message key="processedBy" />:</p>
	      <select name="salesRepProcessedById">
	       <c:if test="${model.salesRepTree == null}">  
	       <option value="-1"> </option>
	       <option value="-2" <c:if test="${orderSearch.salesRepProcessedById == -2}">selected</c:if>><fmt:message key="noOne" /></option>
	       <c:forEach items="${model.salesRepMap}" var="salesRep">
	  	     <option value="${salesRep.value.id}" <c:if test="${orderSearch.salesRepProcessedById == salesRep.value.id}">selected</c:if>><c:out value="${salesRep.value.name}" /></option>
	       </c:forEach>
	       <c:forEach items="${model.salesRepMapInactive}" var="salesRep">
	  	     <option  class="nameLinkInactive" value="${salesRep.value.id}" <c:if test="${orderSearch.salesRepId == salesRep.value.id}">selected</c:if>><c:out value="${salesRep.value.name}" /></option>
	       </c:forEach>
	       </c:if>
	       <c:if test="${model.salesRepTree != null}">
	         <c:set var="orderSearchSalesRepProcessedById" value="${orderSearch.salesRepProcessedById}"/>
	         <option value="-1"> </option>
	         <option value="${salesRepTree.id}" <c:if test="${orderSearch.salesRepProcessedById == salesRepTree.id}">selected</c:if>><c:out value="${salesRepTree.name}" /></option>
			 <c:set var="recursionPageId" value="${orderSearchSalesRepProcessedById}" scope="request"/>
	         <c:set var="level" value="1" scope="request"/>
			 <jsp:include page="/WEB-INF/jsp/admin/orders/recursion.jsp"/>
	       </c:if>
	      </select>
	    </div>
	    </c:if>
	    <div class="search2">
	      <p><fmt:message key="${orderName}"/> Type:</p>
	      <select name="orderType">
	       <option value=""> </option>
	       <c:forTokens items="${siteConfig['ORDER_TYPE'].value}" delims="," var="type" varStatus="status">
			  <option value ="${type}" <c:if test="${type == orderSearch.orderType}">selected</c:if>><c:out value ="${type}" /></option>
		   </c:forTokens>
	      </select>
	    </div>
	    <div class="search2">
	      <p><fmt:message key="paidFull"/>:</p>
	      <select name="paidFull">
		   <option value=""></option>
	         <option value="yes" <c:if test="${orderSearch.paidFull == 'yes'}">selected</c:if>>Yes</option>
	  	     <option value="no" <c:if test="${orderSearch.paidFull == 'no'}">selected</c:if>>No</option>	  	
	      </select>
	    </div>
	    <div class="search2">
	      <p><fmt:message key="firstOrder"/>:</p>
	      <select name="firstOrder">
		   <option value=""></option>
	         <option value="1" <c:if test="${orderSearch.firstOrder == '1'}">selected</c:if>>Yes</option>
	  	     <option value="2" <c:if test="${orderSearch.firstOrder == '2'}">selected</c:if>>No</option>	  	
	      </select>
	    </div>  
	    <c:if test="${gSiteConfig['gACCOUNTING'] == 'TRIPLEFIN'}">
	    <div class="search2">
	      <p>TRIPLEFIN:</p>
	      <select name="triplefin">
	       <option value=""> </option>
		   <option value="true" <c:if test="${orderSearch.triplefin}">selected</c:if>><fmt:message key="sent" /></option>
		   <option value="false" <c:if test="${orderSearch.triplefin == false}">selected</c:if>><fmt:message key="unSent" /></option>
	      </select>
	    </div> 
	    </c:if>
	    <div class="search2">
	      <p><fmt:message key="${orderName}"/> #:</p>
	      <input name="orderNum" type="text" value="<c:out value='${orderSearch.orderNum}' />" size="15" />
	    </div>
	    <c:if test="${orderName != 'quotes'}">
	    <div class="search2">
	      <p>P.O. #:</p>
	      <input name="purchaseOrder" type="text" value="<c:out value='${orderSearch.purchaseOrder}' />" size="15" />
	    </div>
	    </c:if>
	    <div class="search2">
	      <p><fmt:message key="billTo" />:</p>
	      <input name="bill_to" type="text" value="<c:out value='${orderSearch.billToName}' />" size="15" />
	    </div> 
	    <div class="search2">
	      <p><fmt:message key="shipTo" />:</p>
	      <input name="ship_to" type="text" value="<c:out value='${orderSearch.shipToName}' />" size="15" />
	    </div> 
	    <div class="search2">
	      <p><fmt:message key="email" />:</p>
	      <input name="email" type="text" value="<c:out value='${orderSearch.email}' />" size="15" />
	    </div>
	    <div class="search2">
	      <p><fmt:message key="companyName" />:</p>
	      <input name="company_name" type="text" value="<c:out value='${orderSearch.companyName}' />" size="11" />
	    </div>
	    <div class="search2">
	      <p><fmt:message key="qualifier" />:</p>
	      <select name="qualifier">
	       <option value="-1"></option>
	       <option value="-2" <c:if test="${orderSearch.qualifierId == -2}">selected</c:if>><fmt:message key="noQualifier" /></option>
	       <c:forEach items="${model.activeQualifiers}" var="qualifier">
	  	     <option value="${qualifier.id}" <c:if test="${orderSearch.qualifierId == qualifier.id}">selected</c:if>><c:out value="${qualifier.name}" /></option>
	       </c:forEach>
	      </select>
	    </div>  
	    <div class="search2">
	      <p><fmt:message key="multipleSearch" />:</p>
	      <input name="multipleSearch" type="text" value="<c:out value='${orderSearch.multipleSearchString}' />" size="11" />
	    </div>
	    <div class="search2">
	      <p><fmt:message key="rating" /> 1:</p>
	      <select name="rating1">
	         <option value=""></option>
	         <c:forTokens items="${siteConfig['CUSTOMER_RATING_1'].value}" delims="," var="type">
				<option value ="${type}" <c:if test="${orderSearch.rating1 == type}" >selected</c:if>><c:out value ="${type}" /><option>
		  	</c:forTokens>
	      </select>
	    </div>
	    <div class="search2">
	      <p><fmt:message key="rating" /> 2:</p>
	      <select name="rating2">
	      	<option value=""></option>
	        <c:forTokens items="${siteConfig['CUSTOMER_RATING_2'].value}" delims="," var="type">
				<option value ="${type}" <c:if test="${orderSearch.rating2 == type}" >selected</c:if>><c:out value ="${type}" /><option>
		  	</c:forTokens>
	      </select>
	    </div> 
	    <c:if test="${orderName != 'quotes'}">
		    <c:if test="${gSiteConfig['gREGISTRATION_TOUCH_SCREEN']}">
		    <div class="search2">
		      <p><fmt:message key="idCard" />:</p>
		      <input name="cardId" type="text" value="<c:out value='${orderSearch.cardId}' />" size="15" />
		    </div>
		    </c:if>
	    </c:if> 
	    <div class="search2">
	      <p><fmt:message key="trackcode" />:</p>
	      <input name="trackCode" type="text" value="<c:out value='${orderSearch.trackCode}' />" size="15" />
	    </div>
	    <c:if test="${gSiteConfig['gSALES_PROMOTIONS']}">
	    <div class="search2">
	      <p><fmt:message key="promoCode" />:</p>
	      <input name="promoCode" type="text" value="<c:out value='${orderSearch.promoCode}' />" size="15" />
	    </div>
	    </c:if>
	    <c:if test="${orderName != 'quotes'}">
	    <div class="search2">
	      <p><fmt:message key="paymentMethod" />:</p>
	      <input name="paymentMethod" type="text" value="<c:out value='${orderSearch.paymentMethod}' />" size="15" />
	    </div>
	    </c:if>
	    <c:if test="${orderName != 'quotes'}">
	    <div class="search2">
	      <p><fmt:message key="shippingTitle" />: <a href="#operatorShip" rel="type:element" id="mbOperator1" class="mbOperator" style="margin-left: 12px;">
	      <img class="toolTipImg" title="Advanced Search::Click to see more Options" src="../graphics/question.gif" /></a></p>
	      <input name="shippingMethod" type="text" value="<c:out value='${orderSearch.shippingMethod}' />" size="15" />
	       <div id="operatorShip" class="miniWindowWrapper searchOp displayNone">
	    	<div class="header">Advanced Search</div>
	    	  <fieldset class="">
	    	    <label class="AStitle"><fmt:message key="shippingTitle" /></label>
	    	    <input name="shippingMethod" type="text" value="<c:out value='${orderSearch.shippingMethod}' />" size="15" disabled />
	    	  </fieldset>
	    	  <fieldset class="bottom">
	    	    <label class="AStitle"><fmt:message key="operator"/></label>
	    	    <div class="options">
	    	       <label class="radio">
	    	       		<input type="radio" <c:if test="${orderSearch.shippingOperator == 0}" >checked="checked"</c:if> value="0" name="ShippingOP" onchange="selectShipOperator(this.value);" /> <strong>Does not contain</strong>
	    	       </label>
	    	       <label class="radio">
	    	       		<input type="radio" <c:if test="${orderSearch.shippingOperator == 1}" >checked="checked"</c:if> value="1" name="ShippingOP" onchange="selectShipOperator(this.value);" /> <strong>Contains</strong>
	    	       </label>
	    	    </div>
	    	  </fieldset>
	    	  <fieldset>
	    	     <div class="button">
	      			<input type="submit" value="Search" onclick="document.searchform.submit();"/>
      			 </div>
	    	  </fieldset>
		  </div>
		  
		  <input type="hidden" name="shippingOperator" id="shippingOperator"/>
	    </div>
	    </c:if>
	    <div class="search2">
	      <p><fmt:message key="state" />:</p>
	      <input name="state" type="text" value="<c:out value='${orderSearch.state}' />" size="15" />
	    </div>
	    <div class="search2">
	      <p><fmt:message key="sku"/>:   <a href="#operatorSku" rel="type:element" id="mbOperator1" class="mbOperator" style="margin-left: 67px;"><img class="toolTipImg" title="Advanced Search::Click to see more Options" src="../graphics/question.gif" /></a></p>
		  <div id="operatorSku" class="miniWindowWrapper searchOp displayNone">
	    	<div class="header">Advanced Search</div>
	    	  <fieldset class="">
	    	    <label class="AStitle"><fmt:message key="sku"/></label>
	    	    <input name="sku" type="text" value="<c:out value='${orderSearch.sku}' />" size="15" disabled />
	    	  </fieldset>
	    	  <fieldset class="bottom">
	    	    <label class="AStitle"><fmt:message key="operator"/></label>
	    	    <div class="options">
	    	       <label class="radio">
	    	       		<input type="radio" <c:if test="${orderSearch.skuOperator == 0}" >checked="checked"</c:if> value="0" name="skuOp" onchange="selectOperator(this.value);" /> <strong>Contains</strong>
	    	       </label>
	    	       <label class="radio">
	    	       		<input type="radio" <c:if test="${orderSearch.skuOperator == 1}" >checked="checked"</c:if> value="1" name="skuOp" onchange="selectOperator(this.value);" /> <strong>Exact</strong>
	    	       </label>
	    	    </div>
	    	  </fieldset>
	    	  <fieldset>
	    	     <div class="button">
	      			<input type="submit" value="Search" onclick="document.searchform.submit();"/>
      			 </div>
	    	  </fieldset>
		  </div>
		  <input name="sku" type="text" value="<c:out value='${orderSearch.sku}' />" size="15" />
		  <input type="hidden" name="skuOperator" id="skuOperator"/>
	    </div>
		<div class="search2">
	      <p><fmt:message key="masterSku"/>:   <a href="#operatorParentSku" rel="type:element" id="mbparentSkuOperator1" class="mbOperator" style="margin-left: 22px;"><img class="toolTipImg" title="Advanced Search::Click to see more Options" src="../graphics/question.gif" /></a></p>
		  <div id="operatorParentSku" class="miniWindowWrapper searchOp displayNone">
	    	<div class="header">Advanced Search</div>
	    	  <fieldset class="">
	    	    <label class="AStitle"><fmt:message key="parentSku"/></label>
	    	    <input name="product_parent_sku" type="text" value="<c:out value='${orderSearch.parentSku}' />" size="15" disabled />
	    	  </fieldset>
	    	  <fieldset class="bottom">
	    	    <label class="AStitle"><fmt:message key="operator"/></label>
	    	    <div class="options">
	    	       <label class="radio">
	    	       		<input type="radio" <c:if test="${orderSearch.parentSkuOperator == 0}" >checked="checked"</c:if> value="0" name="skuOp" onchange="selectParentSkuOperator(this.value);" /> <strong>Contains</strong>
	    	       </label>
	    	       <label class="radio">
	    	       		<input type="radio" <c:if test="${orderSearch.parentSkuOperator == 1}" >checked="checked"</c:if> value="1" name="skuOp" onchange="selectParentSkuOperator(this.value);" /> <strong>Exact</strong>
	    	       </label>
	    	    </div>
	    	  </fieldset>
	    	  <fieldset>
	    	     <div class="button">
	      			<input type="submit" value="Search" onclick="document.searchform.submit();"/>
      			 </div>
	    	  </fieldset>
		  </div>
		  <input name="product_parent_sku" type="text" value="<c:out value='${orderSearch.parentSku}' />" size="15" />
		  <input type="hidden" name="product_parent_skuOperator" id="product_parent_skuOperator"/>
	    </div>
	    <div class="search2">
	      <p><fmt:message key="country" />:</p>
			<select name="country">
            <option value="" > </option>
            <c:forEach items="${model.countries}" var="country" varStatus="status">
              <option value="${country.code}" <c:if test="${orderSearch.country == country.code}">selected</c:if>>${country.name}</option>
            </c:forEach>
        	</select>
	    </div>
	    <%--
	    <c:if test="${model.languageCodes != null}">
        <div class="search2">
	      <p><fmt:message key="language" />:</p>
			<select name="language">
            <option value="" ></option>
            <option value="en" <c:if test="${orderSearch.languageCode == 'en'}">selected</c:if> ><fmt:message key="language_en"/></option>
         	<c:forEach items="${model.languageCodes}" var="i18n">
          	  <option value="${i18n.languageCode}" <c:if test="${orderSearch.languageCode == i18n.languageCode}">selected</c:if>><fmt:message key="language_${i18n.languageCode}"/></option>
        	</c:forEach>
        	</select>
	    </div> 
	    </c:if>
	    --%>
	    <div class="search2">
	      <p><c:set var="flag" value="false" />
	      <select class="extraFieldSearch" name="productFieldNumber">
	  	    <c:forEach items="${model.productFieldList}" var="productField">
	  	     <c:if test="${!empty productField.name}" ><c:set var="flag" value="true" />
	  	     <option style="font-size:normal;" value="${productField.id}" <c:if test="${orderSearch.productFieldNumber == productField.id}">selected</c:if>><c:out value="${productField.name}" />:</option>
	  	     </c:if>
	  	    </c:forEach>
	      </select>
	      </p>
	      <c:if test="${flag}" >
	       <input name="productField" type="text" value="<c:out value='${orderSearch.productField}' />" size="11" />
	      </c:if>
	    </div>
	    <div class="search2">
	      <p><c:set var="customerFieldFlag" value="false" />
	      <select class="extraFieldSearch" name="customerFieldNumber">
	  	    <c:forEach items="${model.customerFieldList}" var="customerField">
	  	     <c:if test="${!empty customerField.name}" ><c:set var="customerFieldFlag" value="true" />
	  	     <option style="font-size:normal;" value="${customerField.id}" <c:if test="${orderSearch.customerFieldNumber == customerField.id}">selected</c:if>><c:out value="${customerField.name}" />:</option>
	  	     </c:if>
	  	    </c:forEach>
	      </select>
	      </p>
	      <c:if test="${customerFieldFlag}" >
	       <input name="customerField" type="text" value="<c:out value='${orderSearch.customerField}' />" size="11" />
	      </c:if>
	    </div>
	    <div class="button">
	      <input type="submit" value="Search"/>
		</div>
	  <!-- content area -->
	  </td>
	  <c:if test="${orderName != 'quotes'}">
	  <td valign="top">
	  <div id="sideBar">
	  <a name="#tabOrder" ></a>
	  <a href="#tabOrder" id="sideBarTab"><img id="h_toggle"  src="../graphics/tabSliderLeft.png" alt="sideBar" title="sideBar" /></a>
      <div id="sideBarContents" style="width:0px;">
	  <table cellpadding="0" cellspacing="0" border="0" class="module">
      <tr>
      <td class="topl_g"><div align="left" class="searchHeaderAdvanced"><h2><fmt:message key="advancedSearch" /></h2></div></td>
      <td class="topr_g" ></td></tr>
      <tr><td class="boxmidlrg" >
  
		<table class="searchBoxLeftAS"><tr><td>
		<div id="sideBarContentsInner">
			<div class="advancedSearch">
			  <table border="0">
		       <tr> 
		         <td class="advancedSearchDateInterval"><input style="width:12px;margin-right:5px;border: 0px" name="dateType" type="radio" <c:if test="${orderSearch.dateType == 'orderDate'}" >checked</c:if> value="orderDate" />Order Date</td>
		         <td class="advancedSearchDateInterval"><input style="width:12px;margin-right:5px;border: 0px" name="dateType" type="radio" <c:if test="${orderSearch.dateType == 'shipDate'}" >checked</c:if> value="shipDate" />Ship Date</td>
		       </tr>
		      </table>
		      <p>start:</p>
		      <input style="width:90px;margin-right:5px;" name="startDate" id="startDate" type="text" value="<fmt:formatDate type="date" pattern="MM/dd/yyyy" value="${orderSearch.startDate}"/>" readonly size="11" />
			  <img class="calendarImage"  id="startDate_trigger" src="../graphics/calendarIcon.jpg" />
			  <script type="text/javascript">
	    		Calendar.setup({
	        			inputField     :    "startDate",   // id of the input field
	        			showsTime      :    false,
	        			ifFormat       :    "%m/%d/%Y",   // format of the input field
	        			button         :    "startDate_trigger"   // trigger for the calendar (button ID)
	    		});	
			  </script>
		    </div>
		    <div style="clear:both;"></div>    
		    <div class="advancedSearch">
		      <p>end:</p>
		      <input style="width:90px;margin-right:5px;" name="endDate" id="endDate" type="text" value="<fmt:formatDate type="date" pattern="MM/dd/yyyy" value='${orderSearch.endDate}'/>" readonly size="11" />
			  <img class="calendarImage"  id="endDate_trigger" src="../graphics/calendarIcon.jpg" />
			  <script type="text/javascript">
	    		Calendar.setup({
	        			inputField     :    "endDate",   // id of the input field
	        			showsTime      :    false,
	        			ifFormat       :    "%m/%d/%Y",   // format of the input field
	        			button         :    "endDate_trigger"   // trigger for the calendar (button ID)
	    		});	
			  </script>
			</div>  
			<div style="clear:both;"></div>
		      <table border="0">
		       <tr> <td class="advancedSearchDateInterval"><input style="width:12px;margin-right:5px;border: 0px" name="intervaldate" id="today" type="radio" onclick="UpdateStartEndDate(1);" />Today</td> </tr>
		       <tr> <td class="advancedSearchDateInterval"><input style="width:12px;margin-right:5px;border: 0px" name="intervaldate" id="Yesterday" type="radio" onclick="UpdateStartEndDate(0);" />Yesterday</td> </tr>
		       <tr> <td class="advancedSearchDateInterval"><input style="width:12px;margin-right:5px;border: 0px" name="intervaldate" id="weekToDate" type="radio" onclick="UpdateStartEndDate(2);" />Week To Date</td> </tr>
		       <tr> <td class="advancedSearchDateInterval"><input style="width:12px;margin-right:5px;border: 0px" name="intervaldate" id="weekToDate" type="radio" onclick="UpdateStartEndDate(3);" />Last 14 Days</td> </tr>
		       <tr> <td class="advancedSearchDateInterval"><input style="width:12px;margin-right:5px;border: 0px" name="intervaldate" id="monthToDate" type="radio" onclick="UpdateStartEndDate(4);" />Month To Date</td> </tr>
		       <tr> <td class="advancedSearchDateInterval"><input style="width:12px;margin-right:5px;border: 0px" name="intervaldate" id="yearToDate" type="radio" onclick="UpdateStartEndDate(10);" />Year To Date</td> </tr>
		       <tr> <td class="advancedSearchDateInterval"><input style="width:12px;margin-right:5px;border: 0px" name="intervaldate" id="reset" type="radio" onclick="UpdateStartEndDate(20);" />Reset</td> </tr>
		      </table>   
		</div>
		<div class="search2">
	      <p><c:out value="${siteConfig['ORDER_FLAG1_NAME'].value}" />:</p>
	      <select name="flag1">
	       <option value=""> </option>
	       <option value="1" <c:if test="${orderSearch.flag1 == '1'}">selected</c:if>><fmt:message key="yes" /></option>
	       <option value="0" <c:if test="${orderSearch.flag1 == '0'}">selected</c:if>><fmt:message key="no" /></option>
	       <option value="2" <c:if test="${orderSearch.flag1 == '2'}">selected</c:if>><fmt:message key="yes" />+None</option>
	       <option value="3" <c:if test="${orderSearch.flag1 == '3'}">selected</c:if>><fmt:message key="no" />+None</option>
	      </select>
	    </div>
	    <c:if test="${gSiteConfig['gGROUP_CUSTOMER']}">
	    <div class="search2">
	      <p><fmt:message key="group" />:</p>
	      <select name="groupId">
	         <option value=""></option>
	      <c:forEach items="${model.groups}" var="group">
	  	     <option value="${group.id}" <c:if test="${orderSearch.groupId == group.id}">selected</c:if>><c:out value="${group.name}" /></option>
	       </c:forEach>
	      </select>
	    </div>  
	    </c:if>
		<div class="search2">
	      <p><fmt:message key="phone" />:</p>
	      <input name="phone" type="text" value="<c:out value='${orderSearch.phone}' />" size="11" />
	    </div>
	    <div class="search2">
	      <p><fmt:message key="zipcode" />:</p>
	      <input name="zipcode" type="text" value="<c:out value='${orderSearch.zipcode}' />" size="11" />
	    </div>
	    <div class="search2">
	      <div style="width:115px;"><div style="width:100px;float:left;"><p ><fmt:message key="subTotal" />:</p></div><div style="float:right;"><img class="toolTipImg" title="SubTotal::SubTotal >= value" src="../graphics/question.gif" /></div></div>
	      <input name="subTotal" type="text" value="<c:out value='${orderSearch.subTotal}' />" size="11" />
	    </div>
		</td></tr></table>
	  
	  </td><td class="boxmidr" ></td></tr>
      <tr><td class="botl"></td><td class="botr"></td></tr>
      </table></div>
      </div>
      
	  </td>
	  </c:if>
	  </tr></table>
	  </form>
	</div>  
	</div>  
	<div class="menudiv"></div>    
	</c:if>
	
	<c:if test="${param.tab == 'packingList'}"> 
	</c:if>

    </td><td class="boxmidr" ></td></tr>
    <tr><td class="botl"></td><td class="botr"></td></tr></table>
  </div>