<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
  <head>
    <title>Invoice</title>
    <link href="<c:url value="/assets/invoice.css"/>" rel="stylesheet" type="text/css"/>
  </head>  
<body>
<div style="width:100%;margin:auto;">

<c:if test="${order != null}">
<table id="label" border="1">
<tr valign="top">
  <td align="center"><br />
    <h1 style="margin-bottom:1px;"><b>Shipping Information:</b></h1>
    <div style="font-size:11px;text-align:left">OrderId#: <c:out value="${order.orderId}"/></div>
    <br /><br />
    <div id="addressBox" style="font-size:22px ;" >
    <c:set value="${order.shipping}" var="address"/>
    <%@ include file="/WEB-INF/jsp/frontend/common/address.jsp" %>
    </div>
  </td>
</tr>
</table>
</c:if>

</div>

</body>
</html>