<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="/WEB-INF/tlds/spring.tld" prefix="spring" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>

<tiles:insertDefinition name="admin.orders" flush="true">
  <tiles:putAttribute name="tab"  value="orders" />
  <tiles:putAttribute name="content" type="string">

<c:if test="${gSiteConfig['gPDF_INVOICE']}">
<script language="JavaScript" type="text/JavaScript">
<!--
window.addEvent('domready', function(){			
			var tabs1 = new SimpleTabs($('tab-block-1'), { selector: 'h4'});
		});
//-->
</script>
<c:if test="${form.order != null}">	
<form:form commandName="form" onsubmit="return checkMessage()">  
<input type="hidden" id="send" value="true">
<!-- main box -->
  <div id="mboxfull">
  
  <!-- start table -->
  <table cellpadding="0" cellspacing="0" border="0" class="module" >
  <tr>
    <td class="topl_g">
	  
	  <!-- breadcrumb -->
	  <p class="breadcrumb">
	    <a href="../orders">Orders</a> &gt;
	    <a href="../orders/invoice.jhtm?order=${param.orderId}"><fmt:message key="invoice"/> # <c:out value="${param.orderId}"/></a> &gt;
	    <fmt:message key="email"/> <fmt:message key="packingList"/>
	  </p>
	  
	  <!-- Error Message -->
	  <c:if test="${message != null}">
		<div class="message"><fmt:message key="${message}"/></div>
	  </c:if>
	  <spring:hasBindErrors name="form">
		<div class="error"><fmt:message key='form.hasErrors'/></div>
	  </spring:hasBindErrors>
  
  </td><td class="topr_g" ></td></tr>
  <tr><td class="boxmidlrg" >
 <!-- tabs -->
 <div id="tab-block-1">
	<h4 title="<fmt:message key='email'/> <fmt:message key='packingList'/>"><fmt:message key="email"/> <fmt:message key="packingList"/></h4>
	<div>
	<!-- start tab -->

        <c:set var="classIndex" value="0" />
        
	  	<div class="listdivi ln tabdivi"></div>
	  	<div class="listdivi"></div>
            
            <c:if test="${gSiteConfig['gSUPPLIER'] and suppliers != null}">
		  	<div class="list${classIndex % 2}"><c:set var="classIndex" value="${classIndex+1}" />
		  	<div class="listfl"><fmt:message key="supplier"/>:</div>
		  	<div class="listp">
		  	<!-- input field -->
	   			<form:select path="supplierId" onchange="toggleSupplier(this)">
		  		  <form:option value="">Choose a Supplier</form:option>
		  		  <c:forEach items="${suppliers}" var="supplier">
					<form:option value="${supplier.id}" ><c:out value="${supplier.address.company}"/></form:option>
				  </c:forEach>
	            </form:select>
	            <c:forEach items="${suppliers}" var="supplier">
	  	  		  <input type="hidden" id="email_${supplier.id}" value="<c:out value="${supplier.address.email}"/>">
	    		</c:forEach>
	    		<input type="hidden" id="email_" value="" />
		    <!-- end input field -->   	
		  	</div>
		  	</div>  
		  	</c:if>
		  	    
		  	<div class="list${classIndex % 2}"><c:set var="classIndex" value="${classIndex+1}" />
		  	<div class="listfl"><fmt:message key="invoice"/> #:</div>
		  	<div class="listp">
		  	<!-- input field -->
	   			<c:out value="${param.orderId}"/>
		    <!-- end input field -->   	
		  	</div>
		  	</div>  

		  	<div class="list${classIndex % 2}"><c:set var="classIndex" value="${classIndex+1}" />
		  	<div class="listfl"><fmt:message key="from"/>:</div>
		  	<div class="listp">
		  	<!-- input field -->
	   			<c:out value="${form.from}"/>
	            <form:errors path="from" cssClass="error" delimiter=", "/>
		    <!-- end input field -->   	
		  	</div>
		  	</div>  

		  	<div class="list${classIndex % 2}"><c:set var="classIndex" value="${classIndex+1}" />
		  	<div class="listfl"><fmt:message key="to"/>:</div>
		  	<div class="listp">
		  	<!-- input field -->
	   			<form:input path="to" size="50" maxlength="150" htmlEscape="true"/>
	            <form:errors path="to" cssClass="error" delimiter=", "/>
		    <!-- end input field -->   	
		  	</div>
		  	</div>  
		  	
		  	<div class="list${classIndex % 2}"><c:set var="classIndex" value="${classIndex+1}" />
		  	<div class="listfl">Cc:</div>
		  	<div class="listp">
		  	<!-- input field -->
	   			<form:input path="cc" size="50" maxlength="150" htmlEscape="true"/>
	            <form:errors path="cc" cssClass="error" delimiter=", "/>
		    <!-- end input field -->   	
		  	</div>
		  	</div>  
		  	
		  	<div class="list${classIndex % 2}"><c:set var="classIndex" value="${classIndex+1}" />
		  	<div class="listfl">Bcc:</div>
		  	<div class="listp">
		  	<!-- input field -->
	   			<c:out value="${form.bcc}"/>
		    <!-- end input field -->   	
		  	</div>
		  	</div>  
		  	
		  	<div class="list${classIndex % 2}"><c:set var="classIndex" value="${classIndex+1}" />
		  	<div class="listfl"><fmt:message key="subject"/>:</div>
		  	<div class="listp">
		  	<!-- input field -->
	   			<form:input id="subject" path="subject" size="50" maxlength="50" htmlEscape="true"/>
		  		  <c:forEach var="message" items="${messages}">
		    	  <input type="text" disabled style="display:none;" id="subject${message.messageId}" name="subject" value="<c:out value="${message.subject}"/>" size="50" maxlength="50">
		  		  </c:forEach>
		        <form:errors path="subject" cssClass="error"/>
		    <!-- end input field -->   	
		  	</div>
		  	</div> 
		  	
		  	<div class="list${classIndex % 2}"><c:set var="classIndex" value="${classIndex+1}" />
		  	<div class="listfl">Html:</div>
		  	<div class="listp">
		  	<!-- input field -->
	   			<form:checkbox path="html" />
	  			  <c:forEach var="message" items="${messages}">
	    		  <input type="hidden" id="htmlID${message.messageId}" value="<c:out value="${message.html}"/>" />
	  			  </c:forEach>
	            <form:errors path="html" cssClass="error"/>
		    <!-- end input field -->   	
		  	</div>
		  	</div>   
		  	
		  	<div class="list${classIndex % 2}"><c:set var="classIndex" value="${classIndex+1}" />
		  	<div class="listfl"><fmt:message key="message"/>:</div>
		  	<div class="listp">
		  	<!-- input field -->
	   			<form:textarea id="message" path="message" rows="15" cols="60" htmlEscape="true"/>
	  			  <c:forEach var="message" items="${messages}">
	    			<textarea disabled style="display:none;" id="message${message.messageId}" name="message" rows="15" cols="60"><c:out value="${message.message}"/></textarea>
	  			  </c:forEach>
	           <form:errors path="message" cssClass="error"/>
	           <div id="messageSelect">
			      <select id="messageSelected" onChange="chooseMessage(this)">
			        <option value="">choose a message</option>
				  <c:forEach var="message" items="${messages}">
				    <option value="${message.messageId}"><c:out value="${message.messageName}"/></option>
				  </c:forEach>
				  </select>
				</div>
		    <!-- end input field -->   	
		  	</div>
		  	</div>  
	  	
	<!-- end tab -->        
	</div> 
	

<!-- end tabs -->			
</div>

<!-- start button -->
<div align="left" class="button"> 
  <input type="submit" name="__send" value="<fmt:message key="send" />">
  <input type="submit" name="__invoice" value="Back to Invoice" onClick="document.getElementById('send').value='false'">
</div>
<!-- end button -->	     
  
  <!-- end table -->
    </td><td class="boxmidr" ></td></tr>
  <tr><td class="botl"></td><td class="botr"></td></tr>
  </table>
  
<!-- end main box -->  
</div>
</form:form>	
</c:if>

<script language="JavaScript"> 
<!--
   function chooseMessage(el) {
   	messageList = document.getElementsByName('message');
	for(var i=0; i<messageList.length; i++) {
		messageList[i].disabled=true;
		messageList[i].style.display="none";
	}
   	subjectList = document.getElementsByName('subject');
	for(var i=0; i<subjectList.length; i++) {
		subjectList[i].disabled=true;
		subjectList[i].style.display="none";
	}
   	document.getElementById('message'+el.value).disabled=false;
       document.getElementById('message'+el.value).style.display="block"; 
   	document.getElementById('subject'+el.value).disabled=false;
       document.getElementById('subject'+el.value).style.display="block";  
    if (document.getElementById('htmlID'+el.value).value == 'true') {
    	$('html1').checked = true;
	} else {
	    $('html1').checked = false;
	}                     
}	
function trimString(str){
 var returnVal = "";
 for(var i = 0; i < str.length; i++){
   if(str.charAt(i) != ' '){
     returnVal += str.charAt(i)
   }
 }
 return returnVal;
}
function checkMessage() {
    if (document.getElementById('send').value == 'true') {
      var message = document.getElementById('message' + document.getElementById('messageSelected').value);
      var subject = document.getElementById('subject' + document.getElementById('messageSelected').value);
      if (trimString(subject.value) == "") {
        alert("Please put a subject");
        subject.focus();
    	return false;
      }
      if (trimString(message.value) == "") {
        alert("Please put a message");
        message.focus();
    	return false;
      }
    }
    return true;
}

 function toggleSupplier(el) {
    document.getElementById('to').value=document.getElementById('email_'+el.value).value;
    }	
//-->
</script>
</c:if>

  </tiles:putAttribute>    
</tiles:insertDefinition>