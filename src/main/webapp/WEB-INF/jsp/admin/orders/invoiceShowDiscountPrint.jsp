<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
  <head>
    <title>Invoice</title>
    <link href="<c:url value="/assets/invoice.css"/>" rel="stylesheet" type="text/css"/>
  </head>  
<body class="invoice">
<div style="margin:0 auto;">


<c:if test="${order != null}">
<div align="right"><a href="javascript:window.print()"><img border="0" src="../graphics/printer.png"></a></div>
<c:out value="${invoiceLayout.headerHtml}" escapeXml="false"/>

<table border="0" width="100%" cellspacing="3" cellpadding="1">
<tr valign="top">
<td>
<b><fmt:message key="billingInformation" />:</b>
<c:set value="${order.billing}" var="address"/>
<c:set value="true" var="billing" />
<%@ include file="/WEB-INF/jsp/frontend/common/address.jsp" %>
<c:set value="false" var="billing" />
</td>
<td>&nbsp;</td>
<td>
<b><c:choose><c:when test="${order.workOrderNum != null}"><fmt:message key="service" /> <fmt:message key="location" /></c:when><c:otherwise><fmt:message key="shippingInformation" /></c:otherwise></c:choose>:</b>
<c:set value="${order.shipping}" var="address"/>
<%@ include file="/WEB-INF/jsp/frontend/common/address.jsp" %>
</td>
<td>&nbsp;</td>
<td align="right">
<table>
  <tr>
    <td><fmt:message key="invoice"/> #</td>
    <td>:&nbsp;&nbsp;</td>
    <td><b><c:out value="${order.orderId}"/></b></td>
  </tr>
  <c:if test="${siteConfig['TAX_ID_ON_PRINT_INVOICE'].value == 'true'}">
  <tr>
    <td><fmt:message key="taxID"/> #</td>
    <td>:&nbsp;&nbsp;</td>
    <td><c:out value="${customer.taxId}"/></td>
  </tr>
  </c:if>
  <c:if test="${order.externalOrderId != null}">
  <tr>
    <td><fmt:message key="externalOrderId" /></td>
    <td>:&nbsp;&nbsp;</td>
    <td><b><c:out value="${order.externalOrderId}"/></b></td>
  </tr>
  </c:if>
  <tr>
    <td>
    <c:choose>
      <c:when test="${order.status == 'xq'}"><fmt:message key="quoteDate"/></c:when>
      <c:otherwise><fmt:message key="orderDate"/></c:otherwise>
    </c:choose>
	</td>
    <td>:&nbsp;&nbsp;</td>
    <td><fmt:formatDate type="date" timeStyle="default" value="${order.dateOrdered}"/></td>
  </tr>
<c:if test="${gSiteConfig['gSALES_REP']}">
  <tr>
    <td><fmt:message key="salesRep"/></td>
    <td>:&nbsp;&nbsp;</td>
    <td><c:out value="${salesRep.name}"/></td>
  </tr>
  <tr>
    <td><fmt:message key="processedBy"/></td>
    <td>:&nbsp;&nbsp;</td>
    <td><c:out value="${salesRepProcessedBy.name}"/></td>
  </tr>
</c:if>
  <c:if test="${customer.accountNumber != '' and customer.accountNumber != null}" >
  <tr>
    <td><fmt:message key="account"/> #</td>
    <td>:&nbsp;&nbsp;</td>
    <td><c:out value="${customer.accountNumber}"/></td>
  </tr>
  </c:if>
  <tr>
    <td><fmt:message key="status"/></td>
    <td>:&nbsp;&nbsp;</td>
    <td><fmt:message key="${order.encodeStatus}"/></td>
  </tr>
  <c:if test="${gSiteConfig['gADD_INVOICE']}">
  <tr>
    <td><fmt:message key="from"/></td>
    <td>:&nbsp;&nbsp;</td>
    <td><fmt:message key="${order.backEndOrderString}"/></td>
  </tr>
  </c:if>
  <c:if test="${gSiteConfig['gINVOICE_APPROVAL'] and !empty order.approval}">
  <tr>
    <td><fmt:message key="approval"/></td>
    <td>:&nbsp;&nbsp;</td>
    <td><fmt:message key="approvalStatus_${order.approval}" /></td>
  </tr>  
  </c:if>
  <c:if test="${order.orderType != null}">
  <tr>
    <td><fmt:message key="from"/></td>
    <td>:&nbsp;&nbsp;</td>
    <td><c:out value="${order.orderType}"/></td>
  </tr>
  </c:if>
  <c:if test="${order.flag1 != null}">
  <tr>
    <td><c:out value="${siteConfig['ORDER_FLAG1_NAME'].value}" /></td>
    <td>:&nbsp;&nbsp;</td>
    <td><fmt:message key="flag_${order.flag1}"/></td>
  </tr>
  </c:if>
</table>
</td>
</tr>
</table>
<br/>
<b><fmt:message key="emailAddress" />:</b> <a href="mailto:${customer.username}"><c:out value="${customer.username}"/></a>
<hr>
<p>
<c:if test="${order.purchaseOrder != '' and order.purchaseOrder != null}" >
<b><fmt:message key="purchaseOrder" /></b>: <c:out value="${order.purchaseOrder}" />
</c:if>
<c:if test="${order.workOrderNum != null}" >
<br/><b><fmt:message key="workOrder" /> #</b>: <c:out value="${order.workOrderNum}" />
</c:if>
<c:if test="${workOrder != null}" >
<br/><b><c:out value="${siteConfig['SERVICEABLE_ITEM_TITLE'].value}"/> ID/SN</b>: <c:out value="${workOrder.service.item.itemId}" />  / <c:out value="${workOrder.service.item.serialNum}"/>
<br/><b>In SN</b>: <c:out value="${workOrder.inSN}" />
<br/><b><fmt:message key="bwCount"/></b>: <fmt:formatNumber value="${workOrder.bwCount}" pattern="#,##0"/>
<br/><b><fmt:message key="colorCount"/></b>: <fmt:formatNumber value="${workOrder.colorCount}" pattern="#,##0"/>
<br/><b><fmt:message key="problem" /></b>: <c:out value="${workOrder.service.problem}" />
</c:if>	
<table border="0" cellpadding="2" cellspacing="1" width="100%" class="invoice">
  <tr>
    <c:set var="cols" value="0"/>
    <th width="5%" class="invoice"><fmt:message key="line" />#</th>
    <th class="invoice"><fmt:message key="productSku" /></th>
    <th class="invoice"><fmt:message key="productName" /></th>
    <c:forEach items="${productFieldsHeader}" var="productField">
      <c:if test="${productField.showOnInvoice or productField.showOnInvoiceBackend}">
      <th class="invoice"><c:out value="${productField.name}" /></th><c:set var="cols" value="${cols+1}"/>
      </c:if>
    </c:forEach>
    <th width="10%" class="invoice"><fmt:message key="quantity" /></th>
    <c:if test="${gSiteConfig['gINVENTORY'] && order.hasLowInventoryMessage}">
	  <c:set var="cols" value="${cols+1}"/>
	  <th width="10%" class="invoice"><fmt:message key="lowInventory" /></th>
	</c:if>
    <c:if test="${order.hasPacking}">
	  <c:set var="cols" value="${cols+1}"/>
      <th width="10%" class="invoice"><fmt:message key="packing" /></th>
    </c:if>
    <c:if test="${order.hasContent}">
	  <c:set var="cols" value="${cols+1}"/>
      <th class="invoice"><fmt:message key="content" /></th>
	  <c:if test="${!empty siteConfig['EXT_QUANTITY_TITLE'].value}">
       <c:set var="cols" value="${cols+1}"/>
       <th class="invoice"><c:out value="${siteConfig['EXT_QUANTITY_TITLE'].value}" /></th>
      </c:if>
    </c:if>
    <c:if test="${order.hasLineItemDiscount}">
	  <c:set var="cols" value="${cols+2}"/>
      <th class="invoice"><fmt:message key="originalPrice" /></th>
      <th class="invoice"><fmt:message key="discount" /></th>
    </c:if>
    <th class="invoice"><fmt:message key="productPrice" /><c:if test="${order.hasContent}"> / <c:out value="${siteConfig['CASE_UNIT_TITLE'].value}" /></c:if></th>
    <th class="invoice"><fmt:message key="total" /></th>
  </tr>
<c:forEach var="lineItem" items="${order.lineItems}" varStatus="status">
<tr valign="top">
  <td class="invoice" align="center"><c:out value="${status.count}"/></td>
  <td class="invoice">
    <c:if test="${siteConfig['SHOW_IMAGE_ON_INVOICE'].value == 'true' and lineItem.product.thumbnail != null}">
	  <img class="invoiceImage" border="0" <c:if test="${!lineItem.product.thumbnail.absolute}"> src="<c:url value="/assets/Image/Product/thumb/${lineItem.product.thumbnail.imageUrl}"/>" </c:if> <c:if test="${lineItem.product.thumbnail.absolute}"> src="<c:url value="${lineItem.product.thumbnail.imageUrl}"/>" </c:if> />
	</c:if><c:out value="${lineItem.product.sku}"/>
  </td>
  <td class="invoice"><c:out value="${lineItem.product.name}"/>
    <table border="0" cellspacing="1" cellpadding="0">
	  <c:forEach items="${lineItem.productAttributes}" var="productAttribute" varStatus="productAttributeStatus">
		<tr class="invoice_lineitem_attributes${productAttributeStatus.count%2}">
		<td class="optionName" style="white-space: nowrap" align="right"><c:out value="${productAttribute.optionName}"/>: </td>
		<td class="optionValue" style="padding-left:5px;"><c:out value="${productAttribute.valueString}" escapeXml="false"/></td>
		<c:if test="${(! empty productAttribute.optionPriceOriginal) && productAttribute.optionPriceOriginal != 0}">
		<td class="optionPrice"><fmt:formatNumber value="${productAttribute.optionPriceOriginal}" pattern="#,##0.00" /></td>
		<td class="optionNote" style="padding-left:5px;"><fmt:message key="${productAttribute.optionPriceMessageF}"/></td>
		</c:if>
		</tr>
	  </c:forEach>
	  <%@ include file="/WEB-INF/jsp/frontend/common/customLinesInvoice.jsp"%>
   </table>
   <c:if test="${lineItem.subscriptionInterval != null}">
	  <c:set var="intervalType" value="${fn:substring(lineItem.subscriptionInterval, 0, 1)}"/>
	  <c:set var="intervalUnit" value="${fn:substring(lineItem.subscriptionInterval, 1, 2)}"/>
	  <div class="invoice_lineitem_subscription">
		<fmt:message key="subscription"/> <fmt:message key="details"/> : <fmt:message key="delivery"/>
		<c:choose>
		  <c:when test="${intervalUnit == '1'}"><fmt:message key="every_${intervalType}"><fmt:param value="${intervalUnit}"/></fmt:message></c:when>
		  <c:otherwise><fmt:message key="every_${intervalType}s"><fmt:param value="${intervalUnit}"/></fmt:message></c:otherwise>
		</c:choose>
	  </div>
	  <div class="invoice_lineitem_subscription"><c:out value="${lineItem.subscriptionCode}"/></div>
  </c:if>
  <c:if test="${siteConfig['SHOW_IMAGE_ON_INVOICE'].value == 'true' and lineItem.hasProductAttributesImage}" >
  <c:forEach items="${lineItem.productAttributes}" var="productAttribute">
    <c:if test="${!empty productAttribute.imageUrl}" > 
      <c:if test="${!productAttribute.absolute}"><img src="<c:url value="/assets/Image/Product/options/${productAttribute.imageUrl}"/>" border="0" class="invoiceOptionImage" /></c:if>
      <c:if test="${productAttribute.absolute}"><img src="<c:url value="${productAttribute.imageUrl}"/>" border="0" class="invoiceOptionImage" /></c:if>
    </c:if>
  </c:forEach>
  </c:if> 
  </td>
  <c:forEach items="${productFieldsHeader}" var="pFHeader" varStatus="status"><c:set var="check" value="0"/>
  <c:if test="${pFHeader.showOnInvoice or pFHeader.showOnInvoiceBackend}">
  <c:forEach items="${lineItem.productFields}" var="productField"> 
   <c:if test="${pFHeader.id == productField.id}">
    <td class="invoice"><c:out value="${productField.value}" /></td><c:set var="check" value="1"/>
   </c:if>       
  </c:forEach>
  <c:if test="${check == 0}">
    <td class="invoice">&nbsp;</td>
   </c:if>
  </c:if>
  </c:forEach>
  <td class="invoice" align="center"><c:out value="${lineItem.quantity}"/></td>
  <c:if test="${gSiteConfig['gINVENTORY'] && order.hasLowInventoryMessage}">
	<td class="invoice" align="center"><c:out value="${lineItem.lowInventoryMessage}" /></td>
  </c:if>
  <c:if test="${order.hasPacking }">
  <td class="invoice" align="center"><c:out value="${lineItem.product.packing}" /></td>
  </c:if>
  <c:if test="${order.hasContent}">
    <td class="invoice" align="center"><c:out value="${lineItem.product.caseContent}" /></td>
    <c:if test="${!empty siteConfig['EXT_QUANTITY_TITLE'].value}">
    <td class="invoice" align="center">
     <c:choose>
      <c:when test="${!empty lineItem.product.caseContent}"><c:out value="${lineItem.product.caseContent * lineItem.quantity}" /></c:when>
      <c:otherwise><c:out value="${lineItem.quantity}" /></c:otherwise> 
     </c:choose>
    </td>
    </c:if>
  </c:if>
  <c:if test="${order.hasLineItemDiscount}">
    <td class="invoice" align="right"><fmt:formatNumber value="${lineItem.originalPrice}" pattern="#,##0.00"/></td> 
    <td class="invoice" align="right">
    <c:if test="${lineItem.discount != null}" >
     <c:choose>
      <c:when test="${lineItem.percent}"><c:out value="${lineItem.discount}"/>%</c:when>
      <c:otherwise><fmt:message key="${siteConfig['CURRENCY'].value}" /><fmt:formatNumber value="${lineItem.discount}" pattern="#,##0.00"/></c:otherwise> 
     </c:choose>
    </c:if> 
    </td> 
  </c:if>
  <td class="invoice" align="right"><fmt:formatNumber value="${lineItem.unitPrice}" pattern="#,##0.00#" maxFractionDigits="${siteConfig['MAX_FRACTION_DIGIT'].value}"/></td>  
  <td class="invoice" align="right"><fmt:formatNumber value="${lineItem.totalPrice}" pattern="#,##0.00"/></td>
</tr>  
  <c:if test="${lineItem.serialNums != null}">
  <tr bgcolor="#FFFFFF">
    <td colspan="${6+cols}">
      <table cellspacing="3">
        <tr>
          <td valign="top">S/N:</td>
          <td><c:forEach items="${lineItem.serialNums}" var="serialNum" varStatus="status"><c:if test="${not status.first}">, </c:if><c:out value="${serialNum}"/></c:forEach></td>
        </tr>
      </table>
    </td>
  </tr>			
  </c:if>
<c:if test="${not empty lineItem.trackNum or lineItem.dateShipped != null or not empty lineItem.shippingMethod or not empty lineItem.status}">
  <tr bgcolor="#FFFFFF">
    <td colspan="${6+cols}">
      <c:if test="${not empty lineItem.status}"><fmt:message key="status"/>: <c:out value="${lineItem.status}"/></c:if>
      <c:if test="${not empty lineItem.shippingMethod}"><c:out value="${lineItem.shippingMethod}"/></c:if>
      <c:if test="${lineItem.dateShipped != null}"><fmt:message key="dateShipped"/>: <fmt:formatDate type="date" value="${lineItem.dateShipped}" pattern="MM/dd/yyyy"/></c:if>
      <c:if test="${not empty lineItem.trackNum}"><fmt:message key="trackNum"/>: <c:out value="${lineItem.trackNum}"/></c:if>
    </td>
  </tr>			
</c:if>	
</c:forEach>
  <tr bgcolor="#BBBBBB">
    <td colspan="${6+cols}">&nbsp;</td>
  </tr>
  <tr>
    <td width="50px" class="invoice" colspan="${5+cols}" align="right"><fmt:message key="subTotal" />:</td>
    <td width="10px" class="invoice" align="right"><fmt:formatNumber value="${order.subTotal}" pattern="#,##0.00"/></td>
  </tr>
  <c:if test="${order.promo.title != null and order.promo.discountType eq 'order'}" >
	  <tr>
	    <td class="discount" colspan="${5+cols}" align="right">
        <c:choose>
          <c:when test="${order.promo.discount == 0}"><fmt:message key="promoCode" /></c:when>
          <c:otherwise><fmt:message key="discountForPromoCode" /> </c:otherwise>
        </c:choose>
        <c:out value="${order.promo.title}" />
	    <c:choose>
	      <c:when test="${order.promo.discount == 0}"></c:when>
          <c:when test="${order.promo.percent}">
            (<fmt:formatNumber value="${order.promo.discount}" pattern="#,##0.00"/>%)
          </c:when>
          <c:otherwise>
            (<fmt:message key="${siteConfig['CURRENCY'].value}" /><fmt:formatNumber value="${order.promo.discount}" pattern="#,##0.00"/>)
          </c:otherwise>
      	</c:choose>	    
	    </td>
	    <td class="discount" align="right">
	    <c:choose>
	      <c:when test="${order.promo.discount == 0}">&nbsp;</c:when>
          <c:when test="${order.promo.percent}">
            (<fmt:formatNumber value="${order.subTotal * ( order.promo.discount / 100.00 )}" pattern="#,##0.00"/>)
          </c:when>
          <c:when test="${(!order.promo.percent) and (order.promo.discount > order.subTotal)}">
			(<fmt:formatNumber value="${order.subTotal}" pattern="#,##0.00"/>)
		  </c:when>
		  <c:otherwise>
            (<fmt:formatNumber value="${order.promo.discount}" pattern="#,##0.00"/>)
          </c:otherwise>
      	</c:choose>	    
	    </td>
	  </tr>
  </c:if>
  <c:if test="${order.lineItemPromos != null and fn:length(order.lineItemPromos) gt 0}">
    <c:forEach items="${order.lineItemPromos}" var="itemPromo" varStatus="status">
    <tr>
      <td class="discount" colspan="${5+cols}" align="right">
      <c:choose>
        <c:when test="${itemPromo.value == 0}"><fmt:message key="promoCode" /></c:when>
        <c:otherwise><fmt:message key="discountForPromoCode" /></c:otherwise>
      </c:choose>
      <c:out value="${itemPromo.key}" />
      </td>
      <td class="discount" align="right">
      <c:choose>
        <c:when test="${itemPromo.value == 0}">&nbsp;</c:when>
       	<c:otherwise>($<fmt:formatNumber value="${itemPromo.value}" pattern="#,##0.00"/>)</c:otherwise>
      </c:choose>
      </td>
    </tr>
    </c:forEach>  
  </c:if>
  <c:choose>
   	<c:when test="${order.taxOnShipping}">
  	  <tr>
    	<td class="invoice" colspan="${5+cols}" align="right"><fmt:message key="shippingHandling" /> (<c:out value="${order.shippingMethod}" escapeXml="false"/>):</td>
    	<td class="invoice" align="right"><fmt:formatNumber value="${order.shippingCost}" pattern="#,##0.00"/></td>
  	  </tr>
  	  <tr>
    	<td class="invoice" colspan="${5+cols}" align="right"><fmt:message key="tax" />:</td>
    	<td class="invoice" align="right"><fmt:formatNumber value="${order.tax}" pattern="#,##0.00"/></td>
  	  </tr>
   	</c:when>
   	<c:otherwise>
  	  <tr>
    	<td class="invoice" colspan="${5+cols}" align="right"><fmt:message key="tax" />:</td>
    	<td class="invoice" align="right"><fmt:formatNumber value="${order.tax}" pattern="#,##0.00"/></td>
  	  </tr>
  	  <tr>
    	<td class="invoice" colspan="${5+cols}" align="right"><fmt:message key="shippingHandling" /> (<c:out value="${order.shippingMethod}" escapeXml="false"/>):</td>
    	<td class="invoice" align="right"><fmt:formatNumber value="${order.shippingCost}" pattern="#,##0.00"/></td>
  	  </tr>
   	</c:otherwise>		  
  </c:choose>
  
  <c:if test="${order.promo.title != null and order.promo.discountType eq 'shipping' and order.shippingCost != null}" >
	 <tr>
		<td class="discount" colspan="${5+cols}" align="right">
      	  <c:choose>
            <c:when test="${order.promo.discount == 0}"><fmt:message key="promoCode" /></c:when>
            <c:otherwise><fmt:message key="discountForPromoCode" /> </c:otherwise>
      	  </c:choose>
      	  <c:out value="${order.promo.title}" />
		  <c:choose>
			<c:when test="${order.promo.discount == 0}"></c:when>
			<c:when test="${order.promo.percent}">
			  (<fmt:formatNumber value="${order.promo.discount}" pattern="#,##0.00"/>%)
			</c:when>
			<c:otherwise>
			  (<fmt:message key="${siteConfig['CURRENCY'].value}" /><fmt:formatNumber value="${order.promo.discount}" pattern="#,##0.00"/>)
			</c:otherwise>
		  </c:choose>	    
		</td>
		<td class="discount" align="right">
		<c:choose>
		  <c:when test="${order.promo.discount == 0}">&nbsp;</c:when>
		  <c:when test="${order.promo.percent}">
			(<fmt:formatNumber value="${order.shippingCost * ( order.promo.discount / 100.00 )}" pattern="#,##0.00"/>)
		  </c:when>
		  <c:when test="${(!order.promo.percent) and (order.promo.discount > order.shippingCost)}">
		    (<fmt:formatNumber value="${order.shippingCost}" pattern="#,##0.00"/>)
		 </c:when>
		 <c:otherwise>
			(<fmt:formatNumber value="${order.promo.discount}" pattern="#,##0.00"/>)
		  </c:otherwise>
		</c:choose>	    
	  </td>
	</tr>
  </c:if>
  <c:if test="${order.customShippingCost != null}">
  <tr>
    <td class="invoice" colspan="${5+cols}" align="right"><fmt:message key="customShipping" /> (<c:out value="${order.customShippingTitle}" escapeXml="false"/>):</td>
    <td class="invoice" align="right"><fmt:formatNumber value="${order.customShippingCost}" pattern="#,##0.00"/></td>
  </tr>
  </c:if>
  <c:if test="${order.ccFee != null && order.ccFee != 0}">
  <tr>
    <td class="invoice" colspan="${5+cols}" align="right"><fmt:message key="creditCardFee" />:</td>
    <td class="invoice" align="right"><fmt:formatNumber value="${order.ccFee}" pattern="#,##0.00"/></td>
  </tr>
  </c:if>
  <c:if test="${order.creditUsed != null}">
  <tr>
    <td class="discount" colspan="${5+cols}" align="right"><fmt:message key="credit" />:</td>
    <td class="discount" align="right"><fmt:formatNumber value="${order.creditUsed}" pattern="-#,##0.00"/></td>
  </tr>
  </c:if>	
  <c:if test="${order.bondCost != null}">
  <tr>
	<td class="invoice" colspan="${5+cols}" align="right"><fmt:message key="buySafeBondGuarntee" />:</td>
	<td class="invoice" align="right"><fmt:formatNumber value="${order.bondCost}" pattern="#,##0.00"/></td>
  </tr>
  </c:if>			  
  <tr>
    <td class="invoice" colspan="${5+cols}" align="right"><fmt:message key="grandTotal" />:</td>
    <td align="right" bgcolor="#F8FF27"><fmt:message key="${siteConfig['CURRENCY'].value}" /><fmt:formatNumber value="${order.grandTotal}" pattern="#,##0.00"/></td>
  </tr>
</table>
<c:out value="${siteConfig['INVOICE_MESSAGE'].value}" escapeXml="false"/>
<p>
<hr>
<table>
  <tr>
    <td>
    <b><fmt:message key="paymentMethod"/>:</b> <c:out value="${order.paymentMethod}" />
    <c:if test="${siteConfig['SHOW_CC_ON_PRINT_FRIENDLY'].value == 'true'}">
    <p>
	    <c:if test="${order.creditCard != null}">
		<table>
		  <tr>
		    <td align="right" style="white-space: nowrap">Card Type:</td>
		    <td><c:out value="${order.creditCard.type}"/></td>
		  </tr>
		  <tr>
		    <td align="right" style="white-space: nowrap">Card Number:</td>
		    <td><c:out value="${order.creditCard.number}"/></td>  
		  </tr>
		  <tr>
		    <td align="right" style="white-space: nowrap">Expiration Date:</td>
		    <td><c:out value="${order.creditCard.expireMonth}"/>/<c:out value="${order.creditCard.expireYear}"/></td>
		  </tr>
		  <tr>
		    <td align="right" style="white-space: nowrap">Verification Code:</td>
		    <td><c:out value="${order.creditCard.cardCode}"/></td>
		  </tr>
		  <c:if test="${order.creditCard.transId != null}">
		  <tr>
		    <td align="right" style="white-space: nowrap" valign="top">Transaction ID:</td>
		    <td><c:out value="${order.creditCard.transId}" /></td>
		  </tr>
		  </c:if>
		  <c:if test="${order.creditCard.paymentStatus != null}">
		  <tr>
		    <td align="right" style="white-space: nowrap" valign="top">Payment Status:</td>
		    <td><c:out value="${order.creditCard.paymentStatus}" /></td>
		  </tr>
		  </c:if>
		  <c:if test="${order.creditCard.paymentNote != null}">
		  <tr>
		    <td align="right" style="white-space: nowrap" valign="top">Payment Note:</td>
		    <td><c:out value="${order.creditCard.paymentNote}" /></td>
		  </tr>
		  </c:if>
		  <c:if test="${order.creditCard.paymentDate != null}">
		  <tr>
		    <td align="right" style="white-space: nowrap" valign="top">Payment Date:</td>
		    <td><fmt:formatDate type="both" timeStyle="full" value="${order.creditCard.paymentDate}"/></td>
		  </tr>
		  </c:if> 
		</table>
		</c:if>
		<p>
	</c:if>	
    </td>
  </tr>
</table>
<c:if test="${siteConfig['SHOW_PAYMENT_HISTORY_ON_PRINT_FRIENDLY'].value == 'true' and gSiteConfig['gPAYMENTS']}">
<c:set var="totalPayments" value="0"/>
<c:forEach var="payment" items="${order.paymentHistory}" varStatus="paymentStatus">
<c:if test="${paymentStatus.first}">
<table>
  <tr>
    <td colspan="5"><b><fmt:message key="paymentHistory"/></b>:</td>
  </tr>
</c:if>
  <tr>
    <td><fmt:formatDate type="date" timeStyle="default" value="${payment.date}"/></td>
    <td>&nbsp;</td>
	<td align="right"><fmt:message key="${siteConfig['CURRENCY'].value}" /><fmt:formatNumber value="${payment.amount}" pattern="#,##0.00" /></td>
	<c:set var="totalPayments" value="${totalPayments + payment.amount}"/>
    <td>&nbsp;</td>
	<td><c:if test="${payment.paymentMethod != ''}"><c:out value="${payment.paymentMethod}"/> </c:if><c:out value="${payment.memo}"/></td>
  </tr>
<c:if test="${paymentStatus.last}">
  <tr>
    <td class="status"><b><fmt:message key="total" />:</b></td>
    <td>&nbsp;</td>
	<td align="right" class="status"><b><fmt:message key="${siteConfig['CURRENCY'].value}" /><fmt:formatNumber value="${totalPayments}" pattern="#,##0.00" /></b></td>
    <td colspan="2">&nbsp;</td>
  </tr>
</table>
</c:if>
</c:forEach>
</c:if>

<c:forEach items="${customerFields}" var="customerField" varStatus="status">
	<c:out value="${customerField.name}" />: <c:out value="${customerField.value}" /><br />
</c:forEach>

<c:if test="${order.invoiceNote != null and order.invoiceNote != ''}">
<table width="100%">
	<tr >
	  <td valign="top">
	  <b><c:out value="${siteConfig['SPECIAL_INSTRUCTIONS'].value}"/>:</b> <c:out value="${order.invoiceNote}" escapeXml="false"/>  	 
	  </td>
	</tr>
</table>
</c:if>
<p>
<br>
<c:out value="${invoiceLayout.footerHtml}" escapeXml="false"/>
<p>
</c:if>

</div>

</body>
</html>