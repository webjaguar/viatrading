<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ taglib uri="http://www.springframework.org/security/tags" prefix="sec" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ taglib uri="/WEB-INF/tlds/custom.tld" prefix="wj" %>
<%@ taglib uri="/WEB-INF/tlds/spring.tld" prefix="spring" %>

<tiles:insertDefinition name="admin.orders" flush="true">
  <tiles:putAttribute name="tab"  value="orders" />
  <tiles:putAttribute name="content" type="string">  
<sec:authorize ifAnyGranted="ROLE_AEM,ROLE_ADMIN_${prop_site_id},ROLE_INVOICE_VIEW_LIST,ROLE_INVOICE_EDIT,ROLE_INVOICE_STATUS"> 
<script type="text/javascript" src="../javascript/autocompleter1.2.js"></script>
<script type="text/javascript" src="../javascript/autocompleter.Request1.2.js"></script>
<script type="text/javascript" src="../javascript/autocompleter.Local1.2.js"></script>
<script type="text/javascript" src="../javascript/observer.js"></script>

<c:if test="${gSiteConfig['gGROUP_CUSTOMER']}">
<script type="text/javascript" src="../../admin/javascript/multiple.open.accordion1.2.js"></script>
</c:if>
<style>
.topl_g, .topl, .topl_lrg, .boxmidsml, .boxmidlrg {
    overflow-x: visible !important;
}
.module {
    empty-cells: show;
    margin-bottom: 8px;
    table-layout: auto;
}
</style>
<script type="text/javascript">
window.addEvent('domready', function(){
	var box1 = new multiBox('mbOrder', {descClassName: 'descClassName',showNumbers: false,overlay: new overlay()});
	// Create the accordian
	var myAccordion = new Fx.Accordion($$('div.arrow_drop'), $$('div.information'), {
		display: 1, alwaysHide: true,
    	onActive: function() {$('information').removeClass('displayNone');}
	});
	<sec:authorize ifAnyGranted="ROLE_AEM,ROLE_ADMIN_${prop_site_id},ROLE_QUICK_EXPORT">
	$('quickModeTrigger').addEvent('click',function() {
	    $('mboxfull').setStyle('margin', '0');
	    $$('.quickMode').each(function(el){
	    	el.hide();
	    });
	    $$('.quickModeRemove').each(function(el){
	    	el.destroy();
	    });
	    $$('.listingsHdr3').each(function(el){
	    	el.removeProperties('colspan', 'class');
	    });
	    alert('Select the whole page, copy and paste to your excel file.');	   
	  });
	</sec:authorize> 

});	


function SearchCustomers(e) {

	var S = document.getElementsByName("Search")[0]
	
	if(S!=null){
		  document.getElementById('invoice').removeChild(S);

	}
	 
	if(e == "Email"){		  
	 
	  var textfield1 = document.createElement("input");	 
	  textfield1.setAttribute("type", "text"); 	  
	  textfield1.setAttribute("id" , "addEmail");	  
	  textfield1.setAttribute("value", ""); 
	  textfield1.setAttribute("placeholder", "Email Address"); 
	  textfield1.setAttribute("name", "Search"); 
	  textfield1.setAttribute("size", "15"); 
	  
	  
	  document.getElementById('invoice').appendChild(textfield1);
	  
	  var inv = document.getElementById('addinvoice'); 
		
		
		this["el"] = $('addEmail');	
		new Autocompleter.Request.HTML(this["el"], '../orders/show-customer-emails.jhtm', {
		'indicatorClass': 'autocompleter-loading', 'maxChoices': '40',
		
			'onSelection': function(element, selected, value, input) {
				inv.href  = "addInvoice.jhtm?cid="+selected.getNext().innerHTML;
			}
		});
 

	  }
	 
	 if(e == "Phone"){		  	  
	 
	  var textfield1 = document.createElement("input");		 
	  textfield1.setAttribute("type", "text");
	  textfield1.setAttribute("id" , "addPhone");	  
	  textfield1.setAttribute("value", ""); 
	  textfield1.setAttribute("placeholder", "Enter Phone"); 
	  textfield1.setAttribute("name", "Search"); 
	  textfield1.setAttribute("size", "15"); 	  
	  document.getElementById('invoice').appendChild(textfield1);
	  	  
	  var inv = document.getElementById('addinvoice'); 
		
		
		this["el"] = $('addPhone');	
		new Autocompleter.Request.HTML(this["el"], '../orders/show-customer-phonenumbers.jhtm', {
		'indicatorClass': 'autocompleter-loading', 'maxChoices': '40',
		'injectChoice': function(choice) {
			var text = choice.getFirst();
			var value = text.innerHTML;
			choice.inputValue = value;
			text.set('html', this.markQueryValue(value));
			this.addChoiceEvents(choice);
		},
			'onSelection': function(element, selected, value, input) {
				
				inv.href  = "addInvoice.jhtm?cid="+selected.getFirst().getNext().getNext().innerHTML;
			}
		});
	  }
	  
	 if(e == "CellPhone"){		  	  
		 
		  var textfield1 = document.createElement("input");		 
		  textfield1.setAttribute("type", "text");
		  textfield1.setAttribute("id" , "addCellPhone");	  
		  textfield1.setAttribute("value", ""); 
		  textfield1.setAttribute("placeholder", "Enter Cell Phone"); 
		  textfield1.setAttribute("name", "Search"); 
		  textfield1.setAttribute("size", "15"); 	  
		  document.getElementById('invoice').appendChild(textfield1);
		  
		  var inv = document.getElementById('addinvoice'); 
		  
		  this["el"] = $('addCellPhone');	
		
			new Autocompleter.Request.HTML(this["el"], '../orders/show-customer-cellphonenumbers.jhtm', {
			'indicatorClass': 'autocompleter-loading', 'maxChoices': '40',
			'injectChoice': function(choice) {
				var text = choice.getFirst();
				var value = text.innerHTML;
				choice.inputValue = value;
				text.set('html', this.markQueryValue(value));
				this.addChoiceEvents(choice);
			},
				'onSelection': function(element, selected, value, input) {
					
					inv.href  = "addInvoice.jhtm?cid="+selected.getFirst().getNext().getNext().innerHTML;
				}
			});
		  
	 }
	 

}
function showShipDate(orderId) {
    var req = new Request({  
        method: 'get',  
        onRequest: function() { 
        	$('shipDateHelp'+orderId).setStyle('display', 'none'),
           this.box = new Element('img', {src:'../graphics/spinner.gif', id:'syncSpinner'+orderId}).inject($('shipDateBlock'+orderId),'inside');},  
        onComplete: function(response) { $('shipDateBlock'+orderId).innerHTML = "<span style='color:#c00000;'> " + response  + "</span>";
        	$('syncSpinner'+orderId).setStyle('display', 'none'); },
        url: 'show-ajax-shipDate.jhtm?orderId='+orderId}).send();   
}

function toggleAll(el) {
	var ids = document.getElementsByName("__selected_id");	
  	if (ids.length == 1)
      document.list_form.__selected_id.checked = el.checked;
    else
      for (i = 0; i < ids.length; i++)
        document.list_form.__selected_id[i].checked = el.checked;	
}

function batchSelected() {
	var ids = document.getElementsByName("__selected_id");	
  	if (ids.length == 1) {
      if (document.list_form.__selected_id.checked) {
    	return confirm('Batch print orders?');
      }
    } else {
      for (i = 0; i < ids.length; i++) {
        if (document.list_form.__selected_id[i].checked) {
    	  return confirm('Batch print orders?');
        }
      }
    }

    alert("Please select orders(s) to batch.");       
    return false;
}
function batchSubStatusUpdate() {
	var ids = document.getElementsByName("__selected_id");	
  	if (ids.length == 1) {
      if (document.list_form.__selected_id.checked) {
    	return confirm('Batch Sub Status Update?');
      }
    } else {
      for (i = 0; i < ids.length; i++) {
        if (document.list_form.__selected_id[i].checked) {
    	  return confirm('Batch Sub Status Update?');
        }
      }
    }

    alert("Please select orders(s) to batch.");       
    return false;
}

function batchShippingTitleUpdate() {
	var ids = document.getElementsByName("__selected_id");	
  	if (ids.length == 1) {
      if (document.list_form.__selected_id.checked) {
    	return confirm('Batch Shipping Title Update?');
      }
    } else {
      for (i = 0; i < ids.length; i++) {
        if (document.list_form.__selected_id[i].checked) {
    	  return confirm('Batch Shipping Title Update?');
        }
      }
    }

    alert("Please select orders(s) to batch.");       
    return false;
}

function batchDaysToShipUpdate(){
	var ids = document.getElementsByName("__selected_id");	
  	if (ids.length == 1) {
      if (document.list_form.__selected_id.checked) {
    	return confirm('Batch Days to Ship Update?');
      }
    } else {
      for (i = 0; i < ids.length; i++) {
        if (document.list_form.__selected_id[i].checked) {
    	  return confirm('Batch Days to Ship Update?');
        }
      }
    }

    alert("Please select orders(s) to batch.");       
    return false;
}

function batchScheduledDate() {
	var ids = document.getElementsByName("__selected_id");	
  	if (ids.length == 1) {
      if (document.list_form.__selected_id.checked) {
    	return confirm('Batch Scheduled Date Update?');
      }
    } else {
      for (i = 0; i < ids.length; i++) {
        if (document.list_form.__selected_id[i].checked) {
    	  return confirm('Batch Scheduled Date Update?');
        }
      }
    }

    alert("Please select orders(s) to batch.");       
    return false;
}

function batchCustomFieldUpdate() {
	var ids = document.getElementsByName("__selected_id");	
  	if (ids.length == 1) {
      if (document.list_form.__selected_id.checked) {
    	return confirm('Batch Scheduled Update?');
      }
    } else {
      for (i = 0; i < ids.length; i++) {
        if (document.list_form.__selected_id[i].checked) {
    	  return confirm('Batch Scheduled Update?');
        }
      }
    }

    alert("Please select orders(s) to batch.");       
    return false;
}

function optionsSelectedOnlyOneCustomer() {
	var ids = document.getElementsByName("__selected_id");
  	if (ids.length == 1) {
      if (document.list_form.__selected_id.checked) {
    	return true;
      }
    } else if(ids.length > 1) {
    	var count = 0;
    	for (i = 0; i < ids.length; i++) {
            if (document.list_form.__selected_id[i].checked) {
        	  count=count+1;
            }
          }
    	if(count > 1){
      		  alert("Only one customer allowed. ");
      		  return false;
    	}else if(count == 0){
    		alert("Please select one customer.");       
    	    return false;
    	}else{
    		return true;
    	} 
    }
    alert("Please select one customer.");       
    return false;
}
</script>

<script type="text/javascript">
function confirmCustomer(id) {
	if (window.XMLHttpRequest) {
	    xmlhttp = new XMLHttpRequest();
	} else {
	    xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
	}
	xmlhttp.onreadystatechange = function() {
	    if (xmlhttp.readyState == 4 && xmlhttp.status == 200) {
	    	if(xmlhttp.responseText != null && xmlhttp.responseText.length > 0){
	    		var resultText = xmlhttp.responseText;
	    		var r = confirm(resultText);
	    		if (r==true) {
	    			window.location = "../orders/addInvoice.jhtm?cid="+id;
	    		}
	    	}else{
	    		window.location = "../orders/addInvoice.jhtm?cid="+id;
	    	}
	    }
	}
	xmlhttp.open("GET","https://www.viatrading.com/dv/liveapps/customerordershipmentcheck.php?cid="+id,true);
	xmlhttp.send();
}		
</script>

<script type="text/javascript">

function trackOnApp(orderId) {
		if (window.XMLHttpRequest) {
		    xmlhttp = new XMLHttpRequest();
		} else {
		    xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
		}
		
		/* local test */
		/* xmlhttp.open("GET","https://www.viatrading.com/dv/liveapps/generatepayment.php?OrderID="+orderId,true);  */
		/* on server */	
		xmlhttp.open("GET","/dv/liveapps/generatepayment.php?OrderID="+orderId,true); 
		xmlhttp.send();
		
		xmlhttp.onreadystatechange = function() {
			console.log(xmlhttp.readyState, xmlhttp.status, orderId);
			/* alert(xmlhttp.status); */
		    if (xmlhttp.readyState == 4 && xmlhttp.status == 200) {
		    	if(xmlhttp.responseText != null && xmlhttp.responseText.length > 0){
		    		var resultText = xmlhttp.responseText;
		    		console.log(resultText);
		    		/* alert(resultText); */
		    		/* var r = confirm("Track on Apps?"); */
		    		/* if (r==true) { */
		    		/* local test */
		    		/* window.location = "https://www.viatrading.com/dv/liveapps/shiptrack.php?sess_id=" + resultText; */
		    		/* server */
		    		/* window.location = "/dv/liveapps/shiptrack.php?sess_id=" + resultText;  */
		    		window.open("/dv/liveapps/shiptrack.php?sess_id=" + resultText, '_blank');
		    		/* } */
		    	}else {
		    		/* do nothing */
		    		console("request failed.");
		    	}
		    } 
		}
	}	
</script>


<form action="ordersList.jhtm?orderName=${orderName}" id="list" name="list_form" method="post">
<input type="hidden" id="sort" name="sort" value="${orderSearch.sort}" />
<!-- main box -->
  <div id="mboxfull">
  
  <!-- start table -->
  <table cellpadding="0" cellspacing="0" border="0" class="module" >
  <tr class="quickMode">
    <td class="topl_g">
	  
	  <!-- breadcrumb -->
	  <p class="breadcrumb">
	  <c:choose>
	        <c:when test="${(gSiteConfig['gSHIPPING_QUOTE'] or siteConfig['PRODUCT_QUOTE'].value == 'true') and orderName == 'quotes'}">	        	
	    		<a href="../orders/quotesList.jhtm">Quotes</a> &gt;
	    		quotes
	        </c:when>
	        <c:otherwise>		       
	    		<a href="../orders">Order</a> &gt;
	    		orders 
	        </c:otherwise>	        
	  </c:choose>
	  </p>
	  
	  <!-- Error Message -->
	  <div>
	  <div>
	  <!-- header image -->
	  <c:choose>
	        <c:when test="${(gSiteConfig['gSHIPPING_QUOTE'] or siteConfig['PRODUCT_QUOTE'].value == 'true') and orderName == 'quotes'}">	        	
		    		<img class="headerImage" id="quickModeTrigger" src="../graphics/quotes.gif" />
	        </c:when>
	        <c:otherwise>		       
		    		<img class="headerImage" id="quickModeTrigger" src="../graphics/orders.gif" />
	        </c:otherwise>	        
	  </c:choose>
	  </div>
	  </div>
	  <form action="ordersList.jhtm?orderName=${orderName}" name="searchform" method="post" >
	  <div class="search2" style="float : left">
	      <p><fmt:message key="multipleSearch" />:</p>
	      <input name="multipleSearch" type="text" value="<c:out value='${orderSearch.multipleSearchString}' />" size="11" />
	      <input type="submit" value="Search" onclick="document.searchform.submit();"/>
	    </div>
	  </form>
	  
	 <table class="" style="padding-left : 50px"> 
	 <tr><td style="padding-right : 30px"> Pending(<a href="/admin/orders/ordersList.jhtm?status=p">${model.pendingCount}</a>) </td>
	 		<td style="padding-right : 30px"> Being Reviewed(<a href="/admin/orders/ordersList.jhtm?status=pr">${model.beingReviewedCount}</a>) </td>
	 		<td style="padding-right : 30px"> Waiting Payment(<a href="/admin/orders/ordersList.jhtm?status=wp">${model.waitingPaymentCount}</a>) </td>
	 		<td style="padding-right : 30px"> Released(<a href="/admin/orders/ordersList.jhtm?status=rls">${model.releasedCount}</a>)</td>
	 		<td style="padding-right : 30px"> Shipped(<a href="/admin/orders/ordersList.jhtm?status=s">${model.shippedCount}</a>) </td>
	 		<td style="padding-right : 30px"> Cancelled(<a href="/admin/orders/ordersList.jhtm?status=x">${model.cancelledCount}</a>) </td>
	 </tr>
	 </table>
	  
	  <div style="clear: both;"></div>
	  
	 
	  
	  <!-- Options -->  
	  <c:if test="${model.count > 0 and gSiteConfig['gGROUP_CUSTOMER']}">
	    <div class="optionBox"><div class="titlePrivilege"><fmt:message key="options" /></div></div>
		<div class="arrow_drop arrowImageRight"></div>
		<div style="clear: both;"></div>
		<div class="information displayNone" id="information" style="float:left;">  
	
			 <div style="float:left;padding: 5px">
	           <table class="productBatchTool" cellpadding="5" cellspacing="5">
	           <tr>
	            <td valign="top">
	             <table cellpadding="1" cellspacing="1"  width="100%">
	              <tr style="height:30px;white-space: nowrap">
	               <td valign="top" width="20px"><img class="toolTipImg" title="Group Id::Associate group Id to the following order(s) in the list. Can be separated by comma." src="../graphics/question.gif" /></td>
	               <td valign="top"><h3><fmt:message key="groupId" /></h3></td>
	               <td valign="top" align="right"><input type="checkbox" onclick="toggleAll(this)" name="__groupAssignAll" value="true"/> <fmt:message key="all"/></td>
	              </tr>
	             </table>
	            </td>
	           </tr>
	           <tr>
	            <td valign="top">
	             <table cellpadding="1" cellspacing="1">
	              <tr style="">
			       <td>
			        <input name="__group_id" type="text" size="15"/>
			        <div align="left" class="buttonLeft">
		            <input type="submit" name="__groupAssignPacher" value="<fmt:message key="apply" />" onClick="return optionsSelected()">
		            </div>
			       </td>
			       <td>
			        <div><p><input name="__batch_type" type="radio" checked="checked" value="add"/>Add</p></div>
				    <div><p><input name="__batch_type" type="radio" value="remove"/>Remove</p></div>
				    <div><p><input name="__batch_type" type="radio" value="move"/>Move</p></div>
			       </td>
			     </tr>
	             </table>
	            </td>
	           </tr>
	           </table>
	         </div>
	         <c:if test="${model.count > 0 and siteConfig['BATCH_PRINT_INVOICE'].value == 'true'}">
	         <div style="float:left;padding: 5px">
	           <table class="productBatchTool" cellpadding="5" cellspacing="5">
	           <tr>
	            <td valign="top">
	             <table cellpadding="1" cellspacing="1">
	              <tr style="height:30px;white-space: nowrap">
	               <td valign="top" width="20px"><img class="toolTipImg" title="<fmt:message key="batchPrint" />::Print multiple order" src="../graphics/question.gif" /></td>
	               <td valign="top"><h3><fmt:message key="batchPrint" /></h3></td>
	              </tr>
	             </table>
	            </td>
	           </tr>
	           <tr>
	            <td valign="top">
	             <table cellpadding="1" cellspacing="1">
	              <tr style="">
			       <td>
			        <div><p><input type="checkbox" name="__backToBack"/>Back To Back</p></div>
			        <div align="left" class="buttonLeft">
		            <input type="submit" name="__batchPdf" value="<fmt:message key="batchPrint" />" onClick="return batchSelected()"/>
		            </div>
			       </td>
			     </tr>
	             </table>
	            </td>
	           </tr>
	           <tr>
	           
	           </tr>
	           </table>
	         </div>
	         </c:if>
	         <c:if test="${model.count > 0 and siteConfig['SUB_STATUS'].value != '0'}">
	         <div style="float:left;padding: 5px">
	           <table class="productBatchTool" cellpadding="5" cellspacing="5">
	           <tr>
	            <td valign="top">
	             <table cellpadding="1" cellspacing="1">
	              <tr style="height:30px;white-space: nowrap">
	               <td valign="top" width="20px"><img class="toolTipImg" title="<fmt:message key="released" />::Change SubStatus of multiple orders" src="../graphics/question.gif" /></td>
	               <td valign="top"><h3><fmt:message key="released" /></h3></td>
	              </tr>
	             </table>
	            </td>
	           </tr>
	           <tr>
	            <td valign="top">
	             <table cellpadding="1" cellspacing="1">
	              <tr style="">
			       <td>
			        <select name="__sub_status">         
			        	<option value=""></option>			               
				        <c:forEach begin="0" end="${siteConfig['SUB_STATUS'].value}" var="value">
				          <option value="${value}"><fmt:message key="orderSubStatus_${value}" /></option>
				        </c:forEach>
				      </select>
			        <div align="left" class="buttonLeft">
		            <input type="submit" name="__batchsubstatus" value="<fmt:message key="apply" />" onClick="return batchSubStatusUpdate()"/>
		            </div>
			       </td>
			     </tr>
	             </table>
	            </td>
	           </tr>
	           </table>
	         </div>
	         
	         <div style="float:left;padding: 5px">
	           <table class="productBatchTool" cellpadding="5" cellspacing="5">
	           <tr>
	            <td valign="top">
	             <table cellpadding="1" cellspacing="1">
	              <tr style="height:30px;white-space: nowrap">
	               <td valign="top" width="20px"><img class="toolTipImg" title="<fmt:message key="userExpectedDueDate" />::Change SubStatus of multiple orders" src="../graphics/question.gif" /></td>
	               <td valign="top"><h3><fmt:message key="userExpectedDueDate" /></h3></td>
	              </tr>
	             </table>
	            </td>
	           </tr>
	           <tr>
	            <td valign="top">
	             <table cellpadding="1" cellspacing="1">
	              <tr style="">
			       <td>
					
					
					<input id="userDueDate" name="userDueDate" size="18"/>
                                <img id="time_start_trigger2" class="calendarImage" src="../graphics/calendarIcon.jpg" alt="Pick a date" />

                                  <script type="text/javascript">
                                                    Calendar.setup({
                                                                    inputField     :    "userDueDate",   // id of the input field
                                                                    showsTime      :    true,
                                                                    ifFormat       :    "%m/%d/%Y[%I:%M %p]",   // format of the input field
                                                                    button         :    "time_start_trigger2",   // trigger for the calendar (button ID) 
                                                    });
                                   </script>
			        
			        <div align="left" class="buttonLeft">
		            <input type="submit" name="__batchscheduleddate" value="<fmt:message key="apply" />" onClick="return batchScheduledDate()"/>
		            </div>
			       </td>
			     </tr>
	             </table>
	            </td>
	           </tr>
	           </table>
	         </div>
	         
	         </c:if>
	       <c:if test="${gSiteConfig['gREGISTRATION_TOUCH_SCREEN']}">
	    	<div style="float:left;padding: 5px">
	           <table class="productBatchTool" cellpadding="5" cellspacing="5">
	           <tr>
	            <td valign="top">
	             <table cellpadding="1" cellspacing="1">
	              <tr style="height:30px;white-space: nowrap">
	               <td valign="top" width="20px"><img class="toolTipImg" title="Print Sold Label :: Print sold label only allowed by choosing one customer" src="../graphics/question.gif" /></td>
	               <td valign="top"><h3><fmt:message key="printSoldLabel" /></h3></td>
	              </tr>
	             </table>
	             </td>
	           </tr>
	           <tr>
	            <td valign="top">
	             <table cellpadding="1" cellspacing="1">
	              <tr style="">
			       <td>
			        <div align="left" class="buttonLeft">
		            <input type="submit" name="__printSoldLabel" value="Print" onClick="return optionsSelectedOnlyOneCustomer()">
		            </div>
			       </td>
			     </tr>
	             </table>
	            </td>
	           </tr>
	           </table>
	         </div>     
	         </c:if>
	         
	         <!-- Custom field1 -->
	         <c:if test="${siteConfig['ORDER_CUSTOM_FIELD1_NAME'].value != ''}">
	          <div style="float:left;padding: 5px">
	           <table class="productBatchTool" cellpadding="5" cellspacing="5">
	           <tr>
	            <td valign="top">
	             <table cellpadding="1" cellspacing="1">
	              <tr style="height:30px;white-space: nowrap">
	               <td valign="top" width="20px"><img class="toolTipImg" title="${siteConfig['ORDER_CUSTOM_FIELD1_NAME'].value} ::Change ${siteConfig['ORDER_CUSTOM_FIELD1_NAME'].value} value of multiple orders" src="../graphics/question.gif" /></td>
	               <td valign="top"><h3>${siteConfig['ORDER_CUSTOM_FIELD1_NAME'].value}</h3></td>
	              </tr>
	             </table>
	            </td>
	           </tr>
	           <tr>
	            <td valign="top">
	             <table cellpadding="1" cellspacing="1">
	              <tr style="">
			       <td>
			        <select name=__customfield1> 
			        	<option value="">Please Select</option>
			          
			        	<c:forTokens items="${siteConfig['ORDER_CUSTOM_FIELD1_VALUE'].value}" delims="," var="value">
				          <option value="${value}">${value}</option>
				       </c:forTokens>     
				      </select>
			        <div align="left" class="buttonLeft">
		            <input type="submit" name="__batchcustomfield1" value="<fmt:message key="apply" />" onClick="return batchCustomFieldUpdate ()"/>
		            </div>
			       </td>
			     </tr>
	             </table>
	            </td>
	           </tr>
	           </table>
	         </div>
	        </c:if>
	      <!-- Custom field2  -->
	      
	      <c:if test="${siteConfig['ORDER_CUSTOM_FIELD2_NAME'].value != ''}">
	      
	       <div style="float:left;padding: 5px">
	           <table class="productBatchTool" cellpadding="5" cellspacing="5">
	           <tr>
	            <td valign="top">
	             <table cellpadding="1" cellspacing="1">
	              <tr style="height:30px;white-space: nowrap">
	               <td valign="top" width="20px"><img class="toolTipImg" title="${siteConfig['ORDER_CUSTOM_FIELD2_NAME'].value} ::Change ${siteConfig['ORDER_CUSTOM_FIELD2_NAME'].value} value of multiple orders" src="../graphics/question.gif" /></td>
	               <td valign="top"><h3>${siteConfig['ORDER_CUSTOM_FIELD2_NAME'].value}</h3></td>
	              </tr>
	             </table>
	            </td>
	           </tr>
	           <tr>
	            <td valign="top">
	             <table cellpadding="1" cellspacing="1">
	              <tr style="">
			       <td>
			        <select name=__customfield2>   
			        		<option value="">Please Select</option>
			        
			        	<c:forTokens items="${siteConfig['ORDER_CUSTOM_FIELD2_VALUE'].value}" delims="," var="value">
				          <option value="${value}">${value}</option>
				       </c:forTokens>     
				      </select>
			        <div align="left" class="buttonLeft">
		            <input type="submit" name="__batchcustomfield2" value="<fmt:message key="apply" />" onClick="return batchCustomFieldUpdate ()"/>
		            </div>
			       </td>
			     </tr>
	             </table>
	            </td>
	           </tr>
	           </table>
	         </div>
	        </c:if>
	        
	        
	         <div style="float:left;padding: 5px">
	           <table class="productBatchTool" cellpadding="5" cellspacing="5" >
	           <tr>
	            <td valign="top">
	             <table cellpadding="1" cellspacing="1">
	              <tr style="height:30px;white-space: nowrap">
	               <td valign="top" width="20px">
	               <img class="toolTipImg" title=" Add Invoice To customer ::
	               Search by email" src="../graphics/question.gif" /></td>
	               <td valign="top"><h3>Add Invoice To Customer</h3></td>
	              </tr>
	             </table>
	            </td>
	           </tr>
	         
	         <tr>
	         <td valign="top">
	            <table cellpadding="1" cellspacing="1">
	               <tr style="">
			          <td id="invoice">
			           	 <select name=__customfield2 onchange="SearchCustomers(this.value)">   
			        		<option value="">Please Select</option>
			                <option value="Email">Search by Email</option>
			        		<option value="Phone">Search by Phone</option>
			        		<option value="CellPhone">Search by Cell Phone</option>
				         </select>	</br><br>	       				      
	            	 </td>
	               </tr>
	          </table> 
	         </td></tr>
	          
	           <tr>
	            <td valign="top">
	             <table cellpadding="1" cellspacing="1">
	              <tr style="">
			       <td>
			        <div align="left" class="buttonLeft">
			        <a href=""  id ="addinvoice"><input type="button" value="Add invoice"></input></a>
		            </div>
			       </td>
			     </tr>
	             </table>
	            </td>
	           </tr>
	           </table>
	         </div>
	        
	        <!-- batch update shipping title --> 
	        <div style="float:left;padding: 5px">
	           <table class="productBatchTool" cellpadding="5" cellspacing="5" >
	           <tr>
	            <td valign="top">
	             <table cellpadding="1" cellspacing="1">
	              <tr style="height:30px;white-space: nowrap">
	               <td valign="top" width="20px">
	               <img class="toolTipImg" title="Shipping Title ::
	               Batch update shipping title" src="../graphics/question.gif" /></td>
	               <td valign="top"><h3>Shipping Title</h3></td>
	              </tr>
	             </table>
	            </td>
	           </tr>
	         
		       <tr>
		         <td valign="top">
		            <table cellpadding="1" cellspacing="1">
		               <tr style="">
				          <td id="invoice">
				           	 <select name=__shipping_title >   
				        		<option value="">Please Select</option> 
							  	  <c:forEach items="${model.customShippingRateList}" var="customShippingRate"	varStatus="status">
							  	    <option value="${customShippingRate.title}"><c:out value="${customShippingRate.title}" /></option>
							      </c:forEach>
					         </select>	</br><br>	       				      
		            	 </td>
		               </tr>
		          </table> 
		         </td>
		       </tr>	         
	           <tr>
	            <td valign="top">
	             <table cellpadding="1" cellspacing="1">
	              <tr style="">
			       <td>
			        <div align="left" class="buttonLeft">
			         <input type="submit" name="__batchShippingTitle" value="<fmt:message key="apply" />" onClick="return batchShippingTitleUpdate()"/>			        
		            </div>
			       </td>
			     </tr>
	             </table>
	            </td>
	           </tr>
	           </table>
	         </div>
	         
	         <!-- Days to Ship -->
	         <div style="float:left;padding: 5px">
	           <table class="productBatchTool" cellpadding="5" cellspacing="5" >
	           <tr>
	            <td valign="top">
	             <table cellpadding="1" cellspacing="1">
	              <tr style="height:30px;white-space: nowrap">
	               <td valign="top" width="20px">
	               <img class="toolTipImg" title="Days to Ship ::
	               Batch update Days to Ship" src="../graphics/question.gif" /></td>
	               <td valign="top"><h3>Days to Ship</h3></td>
	              </tr>
	             </table>
	            </td>
	           </tr>
	         
		       <tr>
		         <td valign="top">
		            <table cellpadding="1" cellspacing="1">
		               <tr style="">
				          <td id="invoice">
				           	 <select name=__shipping_days >   
				        		<option value="">Please Select</option> 
							      <c:forEach begin="0" end="10" step="1" var="value">
								      <option value="${value}"><c:out value="${value}" /> Day</option>
								  </c:forEach>
					         </select>	
					         </br><br>	       				      
		            	 </td>
		               </tr>
		          </table> 
		         </td>
		       </tr>	         
	           <tr>
	            <td valign="top">
	             <table cellpadding="1" cellspacing="1">
	              <tr style="">
			       <td>
			        <div align="left" class="buttonLeft">
			         <input type="submit" name="__batchDaysToShip" value="<fmt:message key="apply" />" onClick="return batchDaysToShipUpdate()"/>			        
		            </div>
			       </td>
			     </tr>
	             </table>
	            </td>
	           </tr>
	           </table>
	         </div>
	    </div>
	  </c:if>
	  
  
  </td><td class="topr_g quickMode" ></td></tr>
  <tr><td class="boxmidlrg" >

 <!-- tabs -->
 <div class="tab-wrapper">
	<h4 class="quickMode" title=""></h4>
	<div>
	<!-- start tab -->

	  	<%-- <div class="listdivi ln tabdivi"></div> --%>
	  	<div class="listdivi quickMode"></div>
	   
		  	<div class="listlight">
		  	<!-- input field -->
		  	<table border="0" cellpadding="0" cellspacing="1" width="100%" class="quickMode">
			  <tr>
			  <td class="pageShowing">
			  <c:if test="${model.count > 0}">
			  <fmt:message key="showing">
				<fmt:param value="${orderSearch.offset + 1}"/>
				<fmt:param value="${model.pageEnd}"/>
				<fmt:param value="${model.count}"/>
			  </fmt:message>
			  </c:if>
			  </td>
			  <td class="pageNavi">
			  Page 
			  <c:choose>
			  <c:when test="${model.pageCount <= 100 and model.pageCount > 0}">
			  <select name="page" id="page" onchange="submit()">
			  <c:forEach begin="1" end="${model.pageCount}" var="page">
			  	<option value="${page}" <c:if test="${page == (orderSearch.page)}">selected</c:if>>${page}</option>
			  </c:forEach>
			  </select>
			  </c:when>
			  <c:otherwise>
			  <input type="text" id="page" name="page" value="${productSearch.page}" size="5" class="textfield50" />
			  <input type="submit" value="go"/>
			  </c:otherwise>
			  </c:choose>
			  of <c:out value="${model.pageCount}"/>
			  | 
			  <c:if test="${orderSearch.page == 1}"><span class="pageNaviDead"><fmt:message key="previous" /></span></c:if>
			  <c:if test="${orderSearch.page != 1}"><a href="<c:url value="ordersList.jhtm?orderName=${orderName}"><c:param name="page" value="${orderSearch.page-1}"/></c:url>" class="pageNaviLink"><fmt:message key="previous" /></a></c:if>
			  | 
			  <c:if test="${orderSearch.page == model.pageCount}"><span class="pageNaviDead"><fmt:message key="next" /></span></c:if>
			  <c:if test="${orderSearch.page != model.pageCount}"><a href="<c:url value="ordersList.jhtm?orderName=${orderName}"><c:param name="page" value="${orderSearch.page+1}"/></c:url>" class="pageNaviLink"><fmt:message key="next" /></a></c:if>
			  </td>
			  </tr>
			</table>
			
			<c:set var="cols" value="0"/>
			<table border="0" cellpadding="2" cellspacing="0" width="100%" class="listings">
			  <tr class="listingsHdr2">
				<c:if test="${model.count > 0  and (siteConfig['BATCH_PRINT_INVOICE'].value == 'true' or gSiteConfig['gGROUP_CUSTOMER'])}">
			    <td align="center" class="quickMode"><input type="checkbox" onclick="toggleAll(this)"></td>
			    <c:set var="cols" value="${cols+1}"/>
				</c:if>
			    <td class="indexCol quickMode">&nbsp;</td>
			    <c:if test="${gSiteConfig['gADD_INVOICE']}">
			      <td class="indexCol quickMode">&nbsp;</td>
			      <c:set var="cols" value="${cols+1}"/>
			    </c:if>
			    <c:if test="${fn:contains(siteConfig['CUSTOMIZE_ORDER_LIST'].value,'billedTo')}">
			    <c:set var="cols" value="${cols+1}"/>
			    <td class="nameCol">
			    	<table cellspacing="0" cellpadding="1">
			    	  <tr>
			    	    <c:choose>
			    	      <c:when test="${orderSearch.sort == 'bill_to_last_name DESC, bill_to_first_name DESC'}">
			    	        <td class="listingsHdr3">
			    	        <a class="hdrLink" href="#" onClick="document.getElementById('sort').value='bill_to_last_name, bill_to_first_name';document.getElementById('list').submit()"><fmt:message key="billTo" /></a>
			    	        </td>
			    	        <td class="quickModeRemove"><img src="../graphics/up.gif" border="0"></td>
			    	      </c:when>
			    	      <c:when test="${orderSearch.sort == 'bill_to_last_name, bill_to_first_name'}">
			    	        <td class="listingsHdr3">
			    	        <a class="hdrLink" href="#" onClick="document.getElementById('sort').value='bill_to_last_name DESC, bill_to_first_name DESC';document.getElementById('list').submit()"><fmt:message key="billTo" /></a>
			    	        </td>
			    	        <td class="quickModeRemove"><img src="../graphics/down.gif" border="0"></td>
			    	      </c:when>
			    	      <c:otherwise>
			    	        <td colspan="2" class="listingsHdr3">
			    	        <a class="hdrLink" href="#" onClick="document.getElementById('sort').value='bill_to_last_name DESC, bill_to_first_name DESC';document.getElementById('list').submit()"><fmt:message key="billTo" /></a>
			    	        </td>
			    	      </c:otherwise>
			    	    </c:choose>  
			    	  </tr>
			    	  </table>
			    </td>
			    </c:if>
			     <c:set var="cols" value="${cols+1}"/>
			      <td class="nameCol">
			      <table cellspacing="0" cellpadding="1">
			    	  <tr>
			    	   <td class="listingsHdr3">
			    	      TRACK
		    	        </td>
		    	      </tr>
		    	   </table>
		        </td>
			    <c:if test="${fn:contains(siteConfig['CUSTOMIZE_ORDER_LIST'].value,'company')}">
			    <c:set var="cols" value="${cols+1}"/>
			    <td class="nameCol">
			    	<table cellspacing="0" cellpadding="1">
			    	  <tr>
			    	    <c:choose>
			    	      <c:when test="${orderSearch.sort == 'bill_to_company DESC'}">
			    	        <td class="listingsHdr3">
			    	        <a class="hdrLink" href="#" onClick="document.getElementById('sort').value='bill_to_company';document.getElementById('list').submit()"><fmt:message key="company" /></a>
			    	        </td>
			    	        <td class="quickModeRemove"><img src="../graphics/up.gif" border="0"></td>
			    	      </c:when>
			    	      <c:when test="${orderSearch.sort == 'bill_to_company'}">
			    	        <td class="listingsHdr3">
			    	        <a class="hdrLink" href="#" onClick="document.getElementById('sort').value='bill_to_company DESC';document.getElementById('list').submit()"><fmt:message key="company" /></a>
			    	        </td>
			    	        <td class="quickModeRemove"><img src="../graphics/down.gif" border="0"></td>
			    	      </c:when>
			    	      <c:otherwise>
			    	        <td colspan="2" class="listingsHdr3">
			    	        <a class="hdrLink" href="#" onClick="document.getElementById('sort').value='bill_to_company DESC';document.getElementById('list').submit()"><fmt:message key="company" /></a>
			    	        </td>
			    	      </c:otherwise>
			    	    </c:choose>  
			    	  </tr>
			    	</table>
			    </td>
			    </c:if>
			    <c:if test="${fn:contains(siteConfig['CUSTOMIZE_ORDER_LIST'].value,'orderNumber')}">
			    <c:set var="cols" value="${cols+1}"/>
			    <td align="center">
			    	<table cellspacing="0" cellpadding="1">
			    	  <tr>
			    	    <c:choose>
			    	      <c:when test="${orderSearch.sort == 'order_id DESC'}">
			    	        <td class="listingsHdr3">
			    	        <a class="hdrLink" href="#" onClick="document.getElementById('sort').value='order_id';document.getElementById('list').submit()"><fmt:message key="${orderName}Number" /></a>
			    	        </td>
			    	        <td class="quickModeRemove"><img src="../graphics/up.gif" border="0"></td>
			    	      </c:when>
			    	      <c:when test="${orderSearch.sort == 'order_id'}">
			    	        <td class="listingsHdr3">
			    	        <a class="hdrLink" href="#" onClick="document.getElementById('sort').value='order_id DESC';document.getElementById('list').submit()"><fmt:message key="${orderName}Number" /></a>
			    	        </td>
			    	        <td class="quickModeRemove"><img src="../graphics/down.gif" border="0"></td>
			    	      </c:when>
			    	      <c:otherwise>
			    	        <td colspan="2" class="listingsHdr3">
			    	        <a class="hdrLink" href="#" onClick="document.getElementById('sort').value='order_id DESC';document.getElementById('list').submit()"><fmt:message key="${orderName}Number" /></a>
			    	        </td>
			    	      </c:otherwise>
			    	    </c:choose>  
			    	  </tr>
			    	  </table>
			    </td>
			    </c:if>
			    <c:if test="${fn:contains(siteConfig['CUSTOMIZE_ORDER_LIST'].value,'extOrderId')}">
			    <c:set var="cols" value="${cols+1}"/>
			    <td align="center">
			    	<table cellspacing="0" cellpadding="1">
			    	  <tr>
			    	    <c:choose>
			    	      <c:when test="${orderSearch.sort == 'external_order_id DESC'}">
			    	        <td class="listingsHdr3">
			    	        <a class="hdrLink" href="#" onClick="document.getElementById('sort').value='external_order_id';document.getElementById('list').submit()"><fmt:message key="externalOrderId" /></a>
			    	        </td>
			    	        <td class="quickModeRemove"><img src="../graphics/up.gif" border="0"></td>
			    	      </c:when>
			    	      <c:when test="${orderSearch.sort == 'external_order_id'}">
			    	        <td class="listingsHdr3">
			    	        <a class="hdrLink" href="#" onClick="document.getElementById('sort').value='external_order_id DESC';document.getElementById('list').submit()"><fmt:message key="externalOrderId" /></a>
			    	        </td>
			    	        <td class="quickModeRemove"><img src="../graphics/down.gif" border="0"></td>
			    	      </c:when>
			    	      <c:otherwise>
			    	        <td colspan="2" class="listingsHdr3">
			    	        <a class="hdrLink" href="#" onClick="document.getElementById('sort').value='external_order_id DESC';document.getElementById('list').submit()"><fmt:message key="externalOrderId" /></a>
			    	        </td>
			    	      </c:otherwise>
			    	    </c:choose>  
			    	  </tr>
			    	  </table>
			    </td>
			    </c:if>
			    <c:if test="${fn:contains(siteConfig['CUSTOMIZE_ORDER_LIST'].value,'mas200') and orderName != 'quotes'}">
			    <c:set var="cols" value="${cols+1}"/>
			    <td align="center">
			    	<table cellspacing="0" cellpadding="1">
			    	  <tr>
			    	    <c:choose>
			    	      <c:when test="${orderSearch.sort == 'mas200_orderno DESC'}">
			    	        <td class="listingsHdr3">
			    	        <a class="hdrLink" href="#" onClick="document.getElementById('sort').value='mas200_orderno';document.getElementById('list').submit()">Mas200</a>
			    	        </td>
			    	        <td class="quickModeRemove"><img src="../graphics/up.gif" border="0"></td>
			    	      </c:when>
			    	      <c:when test="${orderSearch.sort == 'mas200_orderno'}">
			    	        <td class="listingsHdr3">
			    	        <a class="hdrLink" href="#" onClick="document.getElementById('sort').value='mas200_orderno DESC';document.getElementById('list').submit()">Mas200</a>
			    	        </td>
			    	        <td class="quickModeRemove"><img src="../graphics/down.gif" border="0"></td>
			    	      </c:when>
			    	      <c:otherwise>
			    	        <td colspan="2" class="listingsHdr3">
			    	        <a class="hdrLink" href="#" onClick="document.getElementById('sort').value='mas200_orderno DESC';document.getElementById('list').submit()">Mas200</a>
			    	        </td>
			    	      </c:otherwise>
			    	    </c:choose>  
			    	  </tr>
			    	  </table>
			    </td>
			    </c:if>
				<c:if test="${gSiteConfig['gSERVICE'] and fn:contains(siteConfig['CUSTOMIZE_ORDER_LIST'].value,'workOrder') and orderName != 'quotes'}">
			    <c:set var="cols" value="${cols+1}"/>
			    <td align="center">
			    	<table cellspacing="0" cellpadding="1">
			    	  <tr>
			    	    <c:choose>
			    	      <c:when test="${orderSearch.sort == 'work_order_num DESC'}">
			    	        <td class="listingsHdr3">
			    	        <a class="hdrLink" href="#" onClick="document.getElementById('sort').value='work_order_num';document.getElementById('list').submit()"><fmt:message key="workOrder" /></a>
			    	        </td>
			    	        <td class="quickModeRemove"><img src="../graphics/up.gif" border="0"></td>
			    	      </c:when>
			    	      <c:when test="${orderSearch.sort == 'work_order_num'}">
			    	        <td class="listingsHdr3">
			    	        <a class="hdrLink" href="#" onClick="document.getElementById('sort').value='work_order_num DESC';document.getElementById('list').submit()"><fmt:message key="workOrder" /></a>
			    	        </td>
			    	        <td class="quickModeRemove"><img src="../graphics/down.gif" border="0"></td>
			    	      </c:when>
			    	      <c:otherwise>
			    	        <td colspan="2" class="listingsHdr3">
			    	        <a class="hdrLink" href="#" onClick="document.getElementById('sort').value='work_order_num DESC';document.getElementById('list').submit()"><fmt:message key="workOrder" /></a>
			    	        </td>
			    	      </c:otherwise>
			    	    </c:choose>  
			    	  </tr>
			    	  </table>
			    </td>
			    </c:if>			    
			    <c:if test="${(fn:contains(siteConfig['CUSTOMIZE_ORDER_LIST'].value,'po')) and orderName != 'quotes'}">
			    <c:set var="cols" value="${cols+1}"/>
			    <td align="center">
			    	<table cellspacing="0" cellpadding="1">
			    	  <tr>
			    	    <c:choose>
			    	      <c:when test="${orderSearch.sort == 'purchase_order DESC'}">
			    	        <td class="listingsHdr3">
			    	        <a class="hdrLink" href="#" onClick="document.getElementById('sort').value='purchase_order';document.getElementById('list').submit()">P.O.</a>
			    	        </td>
			    	        <td class="quickModeRemove"><img src="../graphics/up.gif" border="0"></td>
			    	      </c:when>
			    	      <c:when test="${orderSearch.sort == 'purchase_order'}">
			    	        <td class="listingsHdr3">
			    	        <a class="hdrLink" href="#" onClick="document.getElementById('sort').value='purchase_order DESC';document.getElementById('list').submit()">P.O.</a>
			    	        </td>
			    	        <td class="quickModeRemove"><img src="../graphics/down.gif" border="0"></td>
			    	      </c:when>
			    	      <c:otherwise>
			    	        <td colspan="2" class="listingsHdr3">
			    	        <a class="hdrLink" href="#" onClick="document.getElementById('sort').value='purchase_order DESC';document.getElementById('list').submit()">P.O.</a>
			    	        </td>
			    	      </c:otherwise>
			    	    </c:choose>  
			    	  </tr>
			    	  </table>
			    </td>
			    </c:if>
			    <c:if test="${fn:contains(siteConfig['CUSTOMIZE_ORDER_LIST'].value,'dateOrdered')}">
			    <c:set var="cols" value="${cols+1}"/>
			    <td align="center">
			    	<table>
			    	  <tr>
			    	    <c:choose>
			    	      <c:when test="${orderSearch.sort == 'date_ordered DESC'}">
			    	        <td class="listingsHdr3">
			    	        <a class="hdrLink" href="#" onClick="document.getElementById('sort').value='date_ordered';document.getElementById('list').submit()">&nbsp; &nbsp;<fmt:message key="orderDate" />&nbsp; &nbsp;</a>
			    	        </td>
			    	        <td class="quickModeRemove"><img src="../graphics/up.gif" border="0"></td>
			    	      </c:when>
			    	      <c:when test="${orderSearch.sort == 'date_ordered'}">
			    	        <td class="listingsHdr3">
			    	        <a class="hdrLink" href="#" onClick="document.getElementById('sort').value='date_ordered DESC';document.getElementById('list').submit()">&nbsp; &nbsp;<fmt:message key="orderDate" />&nbsp; &nbsp;</a>
			    	        </td>
			    	        <td class="quickModeRemove"><img src="../graphics/down.gif" border="0"></td>
			    	      </c:when>
			    	      <c:otherwise>
			    	        <td class="listingsHdr3">
			    	        <a class="hdrLink" href="#" onClick="document.getElementById('sort').value='date_ordered DESC';document.getElementById('list').submit()">&nbsp; &nbsp; <fmt:message key="orderDate" />&nbsp; &nbsp;</a>
			    	        </td>
			    	      </c:otherwise>
			    	    </c:choose>  
			    	  </tr>
			    	  </table>
			    </td>
			    </c:if>
			    <c:if test="${fn:contains(siteConfig['CUSTOMIZE_ORDER_LIST'].value,'subTotal')}">
			    <td align="right">
			    	<table cellspacing="0" cellpadding="1">
			    	  <tr>
			    	    <c:choose>
			    	      <c:when test="${orderSearch.sort == 'sub_total DESC'}">
			    	        <td class="listingsHdr3">
			    	        <a class="hdrLink" href="#" onClick="document.getElementById('sort').value='sub_total';document.getElementById('list').submit()"><fmt:message key="subTotal" /></a>
			    	        </td>
			    	        <td class="quickModeRemove"><img src="../graphics/up.gif" border="0"></td>
			    	      </c:when>
			    	      <c:when test="${orderSearch.sort == 'sub_total'}">
			    	        <td class="listingsHdr3">
			    	        <a class="hdrLink" href="#" onClick="document.getElementById('sort').value='sub_total DESC';document.getElementById('list').submit()"><fmt:message key="subTotal" /></a>
			    	        </td>
			    	        <td class="quickModeRemove"><img src="../graphics/down.gif" border="0"></td>
			    	      </c:when>
			    	      <c:otherwise>
			    	        <td colspan="2" class="listingsHdr3">
			    	        <a class="hdrLink" href="#" onClick="document.getElementById('sort').value='sub_total DESC';document.getElementById('list').submit()"><fmt:message key="subTotal" /></a>
			    	        </td>
			    	      </c:otherwise>
			    	    </c:choose>  
			    	  </tr>
			    	  </table>
			    </td>
			    </c:if>
			    <c:if test="${fn:contains(siteConfig['CUSTOMIZE_ORDER_LIST'].value,'grandTotal')}">
			    <td align="right">
			    	<table cellspacing="0" cellpadding="1">
			    	  <tr>
			    	    <c:choose>
			    	      <c:when test="${orderSearch.sort == 'grand_total DESC'}">
			    	        <td class="listingsHdr3">
			    	        <a class="hdrLink" href="#" onClick="document.getElementById('sort').value='grand_total';document.getElementById('list').submit()"><fmt:message key="grandTotal" /></a>
			    	        </td>
			    	        <td class="quickModeRemove"><img src="../graphics/up.gif" border="0"></td>
			    	      </c:when>
			    	      <c:when test="${orderSearch.sort == 'sub_total'}">
			    	        <td class="listingsHdr3">
			    	        <a class="hdrLink" href="#" onClick="document.getElementById('sort').value='grand_total DESC';document.getElementById('list').submit()"><fmt:message key="grandTotal" /></a>
			    	        </td>
			    	        <td class="quickModeRemove"><img src="../graphics/down.gif" border="0"></td>
			    	      </c:when>
			    	      <c:otherwise>
			    	        <td colspan="2" class="listingsHdr3">
			    	        <a class="hdrLink" href="#" onClick="document.getElementById('sort').value='grand_total DESC';document.getElementById('list').submit()"><fmt:message key="grandTotal" /></a>
			    	        </td>
			    	      </c:otherwise>
			    	    </c:choose>  
			    	  </tr>
			    	  </table>
			    </td>
			    </c:if>
			    <c:if test="${fn:contains(siteConfig['CUSTOMIZE_ORDER_LIST'].value,'echoSign')}">
			    <td align="center">
			    	<table cellspacing="0" cellpadding="1">
			    	  <tr>
			    	    <td class="listingsHdr3">EchoSign</td>
			          </tr>
			        </table>
			    </td>
			    </c:if>
				<c:if test="${fn:contains(siteConfig['CUSTOMIZE_ORDER_LIST'].value,'linkShare')}">
			    <c:set var="cols" value="${cols+1}"/>
			    <td align="center">
			    	<table cellspacing="0" cellpadding="1">
			    	  <tr>
			    	    <c:choose>
			    	      <c:when test="${orderSearch.sort == 'ls_date_entered DESC'}">
			    	        <td class="listingsHdr3">
			    	        <a class="hdrLink" href="#" onClick="document.getElementById('sort').value='ls_date_entered';document.getElementById('list').submit()">LinkShare</a>
			    	        </td>
			    	        <td class="quickModeRemove"><img src="../graphics/up.gif" border="0"></td>
			    	      </c:when>
			    	      <c:when test="${orderSearch.sort == 'ls_date_entered'}">
			    	        <td class="listingsHdr3">
			    	        <a class="hdrLink" href="#" onClick="document.getElementById('sort').value='ls_date_entered DESC';document.getElementById('list').submit()">LinkShare</a>
			    	        </td>
			    	        <td class="quickModeRemove"><img src="../graphics/down.gif" border="0"></td>
			    	      </c:when>
			    	      <c:otherwise>
			    	        <td colspan="2" class="listingsHdr3">
			    	        <a class="hdrLink" href="#" onClick="document.getElementById('sort').value='ls_date_entered DESC';document.getElementById('list').submit()">LinkShare</a>
			    	        </td>
			    	      </c:otherwise>
			    	    </c:choose>  
			    	  </tr>
			    	  </table>
			    </td>
			    </c:if>	
			    <c:if test="${fn:contains(siteConfig['CUSTOMIZE_ORDER_LIST'].value,'orderType')}">
			    <td align="right">
			    	<table cellspacing="0" cellpadding="1">
			    	  <tr>
			    	    <c:choose>
			    	      <c:when test="${orderSearch.sort == 'order_type DESC'}">
			    	        <td class="listingsHdr3">
			    	        <a class="hdrLink" href="#" onClick="document.getElementById('sort').value='order_type';document.getElementById('list').submit()"><fmt:message key="${orderName}Type" /></a>
			    	        </td>
			    	        <td class="quickModeRemove"><img src="../graphics/up.gif" border="0"></td>
			    	      </c:when>
			    	      <c:when test="${orderSearch.sort == 'order_type'}">
			    	        <td class="listingsHdr3">
			    	        <a class="hdrLink" href="#" onClick="document.getElementById('sort').value='order_type DESC';document.getElementById('list').submit()"><fmt:message key="${orderName}Type" /></a>
			    	        </td>
			    	        <td class="quickModeRemove"><img src="../graphics/down.gif" border="0"></td>
			    	      </c:when>
			    	      <c:otherwise>
			    	        <td colspan="2" class="listingsHdr3">
			    	        <a class="hdrLink" href="#" onClick="document.getElementById('sort').value='order_type DESC';document.getElementById('list').submit()"><fmt:message key="${orderName}Type" /></a>
			    	        </td>
			    	      </c:otherwise>
			    	    </c:choose>  
			    	  </tr>
			    	  </table>
			    </td>
			    </c:if>
			    <c:if test="${fn:contains(siteConfig['CUSTOMIZE_ORDER_LIST'].value,'promoCode')}">
			    <td align="right">
			    	<table cellspacing="0" cellpadding="1">
			    	  <tr>
			    	    <c:choose>
			    	      <c:when test="${orderSearch.sort == 'promo_code DESC'}">
			    	        <td class="listingsHdr3">
			    	        <a class="hdrLink" href="#" onClick="document.getElementById('sort').value='promo_code';document.getElementById('list').submit()"><fmt:message key="promoCode" /></a>
			    	        </td>
			    	        <td class="quickModeRemove"><img src="../graphics/up.gif" border="0"></td>
			    	      </c:when>
			    	      <c:when test="${orderSearch.sort == 'promo_code'}">
			    	        <td class="listingsHdr3">
			    	        <a class="hdrLink" href="#" onClick="document.getElementById('sort').value='promo_code DESC';document.getElementById('list').submit()"><fmt:message key="promoCode" /></a>
			    	        </td>
			    	        <td class="quickModeRemove"><img src="../graphics/down.gif" border="0"></td>
			    	      </c:when>
			    	      <c:otherwise>
			    	        <td colspan="2" class="listingsHdr3">
			    	        <a class="hdrLink" href="#" onClick="document.getElementById('sort').value='promo_code DESC';document.getElementById('list').submit()"><fmt:message key="promoCode" /></a>
			    	        </td>
			    	      </c:otherwise>
			    	    </c:choose>  
			    	  </tr>
			    	  </table>
			    </td>
			    </c:if>
			    <c:if test="${(fn:contains(siteConfig['CUSTOMIZE_ORDER_LIST'].value,'shippingTitle')) and orderName != 'quotes'}">
			    <td align="right">
			    	<table cellspacing="0" cellpadding="1">
			    	  <tr>
			    	    <c:choose>
			    	      <c:when test="${orderSearch.sort == 'shipping_method DESC'}">
			    	        <td class="listingsHdr3">
			    	        <a class="hdrLink" href="#" onClick="document.getElementById('sort').value='shipping_method';document.getElementById('list').submit()"><fmt:message key="shippingTitle" /></a>
			    	        </td>
			    	        <td class="quickModeRemove"><img src="../graphics/up.gif" border="0"></td>
			    	      </c:when>
			    	      <c:when test="${orderSearch.sort == 'shipping_method'}">
			    	        <td class="listingsHdr3">
			    	        <a class="hdrLink" href="#" onClick="document.getElementById('sort').value='shipping_method DESC';document.getElementById('list').submit()"><fmt:message key="shippingTitle" /></a>
			    	        </td>
			    	        <td class="quickModeRemove"><img src="../graphics/down.gif" border="0"></td>
			    	      </c:when>
			    	      <c:otherwise>
			    	        <td colspan="2" class="listingsHdr3">
			    	        <a class="hdrLink" href="#" onClick="document.getElementById('sort').value='shipping_method DESC';document.getElementById('list').submit()"><fmt:message key="shippingTitle" /></a>
			    	        </td>
			    	      </c:otherwise>
			    	    </c:choose>  
			    	  </tr>
			    	  </table>
			    </td>
			    </c:if>
				<c:if test="${gSiteConfig['gPAYMENTS'] and fn:contains(siteConfig['CUSTOMIZE_ORDER_LIST'].value,'amountPaid') and orderName != 'quotes'}">
			    <td align="right" class="listingsHdr3">
			    	<table cellspacing="0" cellpadding="1">
			    	  <tr>
			    	    <c:choose>
			    	      <c:when test="${orderSearch.sort == 'amount_paid DESC'}">
			    	        <td class="listingsHdr3">
			    	        <a class="hdrLink" href="#" onClick="document.getElementById('sort').value='amount_paid';document.getElementById('list').submit()"><fmt:message key="amountPaid" /></a>
			    	        </td>
			    	        <td class="quickModeRemove"><img src="../graphics/up.gif" border="0"></td>
			    	      </c:when>
			    	      <c:when test="${orderSearch.sort == 'amount_paid'}">
			    	        <td class="listingsHdr3">
			    	        <a class="hdrLink" href="#" onClick="document.getElementById('sort').value='amount_paid DESC';document.getElementById('list').submit()"><fmt:message key="amountPaid" /></a>
			    	        </td>
			    	        <td class="quickModeRemove"><img src="../graphics/down.gif" border="0"></td>
			    	      </c:when>
			    	      <c:otherwise>
			    	        <td colspan="2" class="listingsHdr3">
			    	        <a class="hdrLink" href="#" onClick="document.getElementById('sort').value='amount_paid DESC';document.getElementById('list').submit()"><fmt:message key="amountPaid" /></a>
			    	        </td>
			    	      </c:otherwise>
			    	    </c:choose>  
			    	  </tr>
			    	  </table>
			    </td>
				</c:if>
				<c:if test="${fn:contains(siteConfig['CUSTOMIZE_ORDER_LIST'].value,'paymentMethod') and orderName != 'quotes'}">
			    <td align="center">
			    	<table cellspacing="0" cellpadding="1">
			    	  <tr>
			    	    <c:choose>
			    	      <c:when test="${orderSearch.sort == 'payment_method DESC'}">
			    	        <td class="listingsHdr3">
			    	        <a class="hdrLink" href="#" onClick="document.getElementById('sort').value='payment_method';document.getElementById('list').submit()"><fmt:message key="paymentMethod" /></a>
			    	        </td>
			    	        <td class="quickModeRemove"><img src="../graphics/up.gif" border="0"></td>
			    	      </c:when>
			    	      <c:when test="${orderSearch.sort == 'payment_method'}">
			    	        <td class="listingsHdr3">
			    	        <a class="hdrLink" href="#" onClick="document.getElementById('sort').value='payment_method DESC';document.getElementById('list').submit()"><fmt:message key="paymentMethod" /></a>
			    	        </td>
			    	        <td class="quickModeRemove"><img src="../graphics/down.gif" border="0"></td>
			    	      </c:when>
			    	      <c:otherwise>
			    	        <td colspan="2" class="listingsHdr3">
			    	        <a class="hdrLink" href="#" onClick="document.getElementById('sort').value='payment_method DESC';document.getElementById('list').submit()"><fmt:message key="paymentMethod" /></a>
			    	        </td>
			    	      </c:otherwise>
			    	    </c:choose>  
			    	  </tr>
			    	  </table>
			    </td>
			    </c:if>
			    <c:if test="${gSiteConfig['gINVOICE_APPROVAL'] and fn:contains(siteConfig['CUSTOMIZE_ORDER_LIST'].value,'approval')}">
			    <td align="center">
			    	<table cellspacing="0" cellpadding="1">
			    	  <tr>
			    	    <c:choose>
			    	      <c:when test="${orderSearch.sort == 'approval DESC'}">
			    	        <td class="listingsHdr3">
			    	        <a class="hdrLink" href="#" onClick="document.getElementById('sort').value='approval';document.getElementById('list').submit()"><fmt:message key="approval" /></a>
			    	        </td>
			    	        <td class="quickModeRemove"><img src="../graphics/up.gif" border="0"></td>
			    	      </c:when>
			    	      <c:when test="${orderSearch.sort == 'approval'}">
			    	        <td class="listingsHdr3">
			    	        <a class="hdrLink" href="#" onClick="document.getElementById('sort').value='approval DESC';document.getElementById('list').submit()"><fmt:message key="approval" /></a>
			    	        </td>
			    	        <td class="quickModeRemove"><img src="../graphics/down.gif" border="0"></td>
			    	      </c:when>
			    	      <c:otherwise>
			    	        <td colspan="2" class="listingsHdr3">
			    	        <a class="hdrLink" href="#" onClick="document.getElementById('sort').value='approval DESC';document.getElementById('list').submit()"><fmt:message key="approval" /></a>
			    	        </td>
			    	      </c:otherwise>
			    	    </c:choose>  
			    	  </tr>
			    	  </table>
			    </td>
			    </c:if>
			    <c:if test="${fn:contains(siteConfig['CUSTOMIZE_ORDER_LIST'].value,'status')}">
			    <td align="center">
			    	<table cellspacing="0" cellpadding="1">
			    	  <tr>
			    	    <c:choose>
			    	      <c:when test="${orderSearch.sort == 'status DESC'}">
			    	        <td class="listingsHdr3">
			    	        <a class="hdrLink" href="#" onClick="document.getElementById('sort').value='status';document.getElementById('list').submit()"><fmt:message key="status" /></a>
			    	        </td>
			    	        <td class="quickModeRemove"><img src="../graphics/up.gif" border="0"></td>
			    	      </c:when>
			    	      <c:when test="${orderSearch.sort == 'status'}">
			    	        <td class="listingsHdr3">
			    	        <a class="hdrLink" href="#" onClick="document.getElementById('sort').value='status DESC';document.getElementById('list').submit()"><fmt:message key="status" /></a>
			    	        </td>
			    	        <td class="quickModeRemove"><img src="../graphics/down.gif" border="0"></td>
			    	      </c:when>
			    	      <c:otherwise>
			    	        <td colspan="2" class="listingsHdr3">
			    	        <a class="hdrLink" href="#" onClick="document.getElementById('sort').value='status DESC';document.getElementById('list').submit()"><fmt:message key="status" /></a>
			    	        </td>
			    	      </c:otherwise>
			    	    </c:choose>  
			    	  </tr>
			    	  </table>
			    </td>
			    </c:if>
			    <c:if test="${siteConfig['SUB_STATUS'].value != '0' and fn:contains(siteConfig['CUSTOMIZE_ORDER_LIST'].value,'subStatus')}">
			    <td align="center">
			    	<table cellspacing="0" cellpadding="1">
			    	  <tr>
			    	    <c:choose>
			    	      <c:when test="${orderSearch.sort == 'sub_status DESC'}">
			    	        <td class="listingsHdr3">
			    	        <a class="hdrLink" href="#" onClick="document.getElementById('sort').value='sub_status';document.getElementById('list').submit()"><fmt:message key="released" /></a>
			    	        </td>
			    	        <td class="quickModeRemove"><img src="../graphics/up.gif" border="0"></td>
			    	      </c:when>
			    	      <c:when test="${orderSearch.sort == 'sub_status'}">
			    	        <td class="listingsHdr3">
			    	        <a class="hdrLink" href="#" onClick="document.getElementById('sort').value='sub_status DESC';document.getElementById('list').submit()"><fmt:message key="released" /></a>
			    	        </td>
			    	        <td class="quickModeRemove"><img src="../graphics/down.gif" border="0"></td>
			    	      </c:when>
			    	      <c:otherwise>
			    	        <td colspan="2" class="listingsHdr3">
			    	        <a class="hdrLink" href="#" onClick="document.getElementById('sort').value='sub_status DESC';document.getElementById('list').submit()"><fmt:message key="released" /></a>
			    	        </td>
			    	      </c:otherwise>
			    	    </c:choose>  
			    	  </tr>
			    	  </table>
			    </td>
			    </c:if>
			    
			   <td align="center">
			    	<table cellspacing="0" cellpadding="1">
			    	  <tr>
			    	        <td colspan="2" class="listingsHdr3">
			    	        <a class="hdrLink" href="#" >Conditions of Purchase</a>
			    	        </td>
			    	  </tr>
			    	  </table>
			    </td>
			    
			    <c:if test="${siteConfig['SUB_STATUS'].value != '0' and fn:contains(siteConfig['CUSTOMIZE_ORDER_LIST'].value,'scheduledTime')}">
			    <td align="center">
			    	<table cellspacing="0" cellpadding="1">
			    	  <tr>
			    	    <c:choose>
			    	      <c:when test="${orderSearch.sort == 'sub_status DESC'}">
			    	        <td class="listingsHdr3">
			    	        <a class="hdrLink" href="#" onClick="document.getElementById('sort').value='sub_status';document.getElementById('list').submit()"><fmt:message key="userExpectedDueDate" /></a>
			    	        </td>
			    	        <td class="quickModeRemove"><img src="../graphics/up.gif" border="0"></td>
			    	      </c:when>
			    	      <c:when test="${orderSearch.sort == 'sub_status'}">
			    	        <td class="listingsHdr3">
			    	        <a class="hdrLink" href="#" onClick="document.getElementById('sort').value='sub_status DESC';document.getElementById('list').submit()"><fmt:message key="userExpectedDueDate" /></a>
			    	        </td>
			    	        <td class="quickModeRemove"><img src="../graphics/down.gif" border="0"></td>
			    	      </c:when>
			    	      <c:otherwise>
			    	        <td colspan="2" class="listingsHdr3">
			    	        <a class="hdrLink" href="#" onClick="document.getElementById('sort').value='sub_status DESC';document.getElementById('list').submit()"><fmt:message key="userExpectedDueDate" /></a>
			    	        </td>
			    	      </c:otherwise>
			    	    </c:choose>  
			    	  </tr>
			    	  </table>
			    </td>
			    </c:if>
			    <c:if test="${fn:contains(siteConfig['CUSTOMIZE_ORDER_LIST'].value,'deliveryPerson') and orderName != 'quotes'}">
			    <td align="center">
			    	<table cellspacing="0" cellpadding="1">
			    	  <tr>
			    	    <c:choose>
			    	      <c:when test="${orderSearch.sort == 'delivery_person DESC'}">
			    	        <td class="listingsHdr3">
			    	        <a class="hdrLink" href="#" onClick="document.getElementById('sort').value='delivery_person';document.getElementById('list').submit()"><fmt:message key="deliveryPerson" /></a>
			    	        </td>
			    	        <td class="quickModeRemove"><img src="../graphics/up.gif" border="0"></td>
			    	      </c:when>
			    	      <c:when test="${orderSearch.sort == 'delivery_person'}">
			    	        <td class="listingsHdr3">
			    	        <a class="hdrLink" href="#" onClick="document.getElementById('sort').value='delivery_person DESC';document.getElementById('list').submit()"><fmt:message key="deliveryPerson" /></a>
			    	        </td>
			    	        <td class="quickModeRemove"><img src="../graphics/down.gif" border="0"></td>
			    	      </c:when>
			    	      <c:otherwise>
			    	        <td colspan="2" class="listingsHdr3">
			    	        <a class="hdrLink" href="#" onClick="document.getElementById('sort').value='delivery_person DESC';document.getElementById('list').submit()"><fmt:message key="deliveryPerson" /></a>
			    	        </td>
			    	      </c:otherwise>
			    	    </c:choose>  
			    	  </tr>
			    	  </table>
			    </td>
			    </c:if>
			    <c:if test="${fn:contains(siteConfig['CUSTOMIZE_ORDER_LIST'].value,'dateShipped')}">
			    <td class="listingsHdr3">
					<fmt:message key="dateShipped" />
				</td>
				</c:if>
<!--		    <td align="right" class="listingsHdr3">
			    	<table cellspacing="0" cellpadding="1">
			    	  <tr>
			    	    <c:choose>
			    	      <c:when test="${orderSearch.sort == 'shipped DESC'}">
			    	        <td class="listingsHdr3">
			    	        <a class="hdrLink" href="#" onClick="document.getElementById('sort').value='shipped';document.getElementById('list').submit()"><fmt:message key="dateShipped" /></a>
			    	        </td>
			    	        <td class="quickModeRemove"><img src="../graphics/up.gif" border="0"></td>
			    	      </c:when>
			    	      <c:when test="${orderSearch.sort == 'shipped'}">
			    	        <td class="listingsHdr3">
			    	        <a class="hdrLink" href="#" onClick="document.getElementById('sort').value='shipped DESC';document.getElementById('list').submit()"><fmt:message key="dateShipped" /></a>
			    	        </td>
			    	        <td class="quickModeRemove"><img src="../graphics/down.gif" border="0"></td>
			    	      </c:when>
			    	      <c:otherwise>
			    	        <td colspan="2" class="listingsHdr3">
			    	        <a class="hdrLink" href="#" onClick="document.getElementById('sort').value='shipped DESC';document.getElementById('list').submit()"><fmt:message key="dateShipped" /></a>
			    	        </td>
			    	      </c:otherwise>
			    	    </c:choose>  
			    	  </tr>
			    	  </table>
			    </td>
-->				  
                <c:if test="${fn:contains(siteConfig['CUSTOMIZE_ORDER_LIST'].value,'trackCode')}">
			    <td align="center">
			    	<table cellspacing="0" cellpadding="1">
			    	  <tr>
			    	    <c:choose>
			    	      <c:when test="${orderSearch.sort == 'trackcode DESC'}">
			    	        <td class="listingsHdr3">
			    	        <a class="hdrLink" href="#" onClick="document.getElementById('sort').value='trackcode';document.getElementById('list').submit()"><fmt:message key="trackcode" /></a>
			    	        </td>
			    	        <td class="quickModeRemove"><img src="../graphics/up.gif" border="0"></td>
			    	      </c:when>
			    	      <c:when test="${orderSearch.sort == 'trackcode'}">
			    	        <td class="listingsHdr3">
			    	        <a class="hdrLink" href="#" onClick="document.getElementById('sort').value='trackcode DESC';document.getElementById('list').submit()"><fmt:message key="trackcode" /></a>
			    	        </td>
			    	        <td class="quickModeRemove"><img src="../graphics/down.gif" border="0"></td>
			    	      </c:when>
			    	      <c:otherwise>
			    	        <td colspan="2" class="listingsHdr3">
			    	        <a class="hdrLink" href="#" onClick="document.getElementById('sort').value='trackcode DESC';document.getElementById('list').submit()"><fmt:message key="trackcode" /></a>
			    	        </td>
			    	      </c:otherwise>
			    	    </c:choose>  
			    	  </tr>
			    	  </table>
			    </td>
			    </c:if>
			    <c:if test="${fn:contains(siteConfig['CUSTOMIZE_ORDER_LIST'].value,'qualifier')}">
			    <td align="center">
			    	<table cellspacing="0" cellpadding="1">
			    	  <tr>
			    	    <c:choose>
			    	      <c:when test="${orderSearch.sort == 'qualifier DESC'}">
			    	        <td class="listingsHdr3">
			    	        <a class="hdrLink" href="#" onClick="document.getElementById('sort').value='qualifier';document.getElementById('list').submit()"><fmt:message key="qualifier" /></a>
			    	        </td>
			    	        <td class="quickModeRemove"><img src="../graphics/up.gif" border="0"></td>
			    	      </c:when>
			    	      <c:when test="${orderSearch.sort == 'qualifier'}">
			    	        <td class="listingsHdr3">
			    	        <a class="hdrLink" href="#" onClick="document.getElementById('sort').value='qualifier DESC';document.getElementById('list').submit()"><fmt:message key="qualifier" /></a>
			    	        </td>
			    	        <td class="quickModeRemove"><img src="../graphics/down.gif" border="0"></td>
			    	      </c:when>
			    	      <c:otherwise>
			    	        <td colspan="2" class="listingsHdr3">
			    	        <a class="hdrLink" href="#" onClick="document.getElementById('sort').value='qualifier DESC';document.getElementById('list').submit()"><fmt:message key="qualifier" /></a>
			    	        </td>
			    	      </c:otherwise>
			    	    </c:choose>  
			    	  </tr>
			    	</table>
			    </td>
			    </c:if>
			    <c:if test="${gSiteConfig['gSALES_REP'] and fn:contains(siteConfig['CUSTOMIZE_ORDER_LIST'].value,'salesRep')}">
			    <td align="center">
			    	<table cellspacing="0" cellpadding="1">
			    	  <tr>
			    	    <c:choose>
			    	      <c:when test="${orderSearch.sort == 'salesrep_id DESC'}">
			    	        <td class="listingsHdr3">
			    	        <a class="hdrLink" href="#" onClick="document.getElementById('sort').value='salesrep_id';document.getElementById('list').submit()"><fmt:message key="salesRep" /></a>
			    	        </td>
			    	        <td class="quickModeRemove"><img src="../graphics/up.gif" border="0"></td>
			    	      </c:when>
			    	      <c:when test="${orderSearch.sort == 'salesrep_id'}">
			    	        <td class="listingsHdr3">
			    	        <a class="hdrLink" href="#" onClick="document.getElementById('sort').value='salesrep_id DESC';document.getElementById('list').submit()"><fmt:message key="salesRep" /></a>
			    	        </td>
			    	        <td class="quickModeRemove"><img src="../graphics/down.gif" border="0"></td>
			    	      </c:when>
			    	      <c:otherwise>
			    	        <td colspan="2" class="listingsHdr3">
			    	        <a class="hdrLink" href="#" onClick="document.getElementById('sort').value='salesrep_id DESC';document.getElementById('list').submit()"><fmt:message key="salesRep" /></a>
			    	        </td>
			    	      </c:otherwise>
			    	    </c:choose>  
			    	  </tr>
			    	  </table>
			    </td>			    
			    </c:if>
				<c:if test="${gSiteConfig['gMULTI_STORE'] > 0 and fn:contains(siteConfig['CUSTOMIZE_ORDER_LIST'].value,'multiStore')}">
			    <td class="listingsHdr3"><fmt:message key="multiStore" /></td>
				</c:if>
			  </tr>
			  <c:set var="totalSubTotal" value="0.0" />
			  <c:set var="totalGrandTotal" value="0.0" />
			  <c:set var="totalAmountPaid" value="0.0" /> 
			<c:forEach items="${model.orders}" var="order" varStatus="status">
			  <tr class="row${status.index % 2}" onmouseover="changeStyleClass(this,'rowOver')" onmouseout="changeStyleClass(this,'row${status.index % 2}')">
				<c:if test="${siteConfig['BATCH_PRINT_INVOICE'].value == 'true' or gSiteConfig['gGROUP_CUSTOMER']}">			    
				<td align="center" class="quickMode"><input name="__selected_id" value="${status.index}" type="checkbox" />
				 <input name="__selected_customer_${status.index}" value="${order.userId}" type="hidden" />
				 <input name="__selected_order_${status.index}" value="${order.orderId}" type="hidden" />
				 <input name="__selected_order_status_${status.index}" value="${order.status}" type="hidden" />
				</td>
				</c:if>
			    <td class="indexCol quickMode"><c:out value="${status.count + orderSearch.offset}"/>.</td>
			    <c:if test="${gSiteConfig['gADD_INVOICE']}">
			      <td class="nameCol quickMode"><a href="#" onclick="confirmCustomer(${order.userId});"><img src="../graphics/add.png" alt="Add Invoice" title="Add Invoice" border="0"></a></td>
			    </c:if>
			    <c:if test="${fn:contains(siteConfig['CUSTOMIZE_ORDER_LIST'].value,'billedTo')}">
			      <c:choose >
				  <c:when test="${model.firstOrderList[wj:intToLong(order.userId)] eq order.orderId}">
			      <td class="nameCol"><a href="../customers/customerList.jhtm?email=${order.username}" class="nameLinkRed"><c:out value="${order.billing.firstName}"/>&nbsp;<c:out value="${order.billing.lastName}"/></a></td>			
				  </c:when>
				  <c:otherwise>
			      <td class="nameCol">
			      <a href="../customers/customerList.jhtm?email=${order.username}"><c:out value="${order.billing.firstName}"/>&nbsp;<c:out value="${order.billing.lastName}"/></a></td>			
				  </c:otherwise>				  
				  </c:choose>
			    </c:if>
			      <td class="nameCol">
			      		<%-- <a href=" <c:out value="${order.trackUrlApps}"/>" target ="blank"> Track on Apps</a> --%>
			      		<a href="#" onclick="trackOnApp(${order.orderId})"> Track on Apps</a>
			     </td>			
			    <c:if test="${fn:contains(siteConfig['CUSTOMIZE_ORDER_LIST'].value,'company')}">
			      <td class="nameCol"><c:out value="${order.billing.company}"/></td>			
				</c:if>
				<c:if test="${fn:contains(siteConfig['CUSTOMIZE_ORDER_LIST'].value,'orderNumber')}">
				  <td align="center">
				  <c:choose >
				  <c:when test="${model.firstOrderList[wj:intToLong(order.userId)] eq order.orderId}">
				  	<a href="invoice.jhtm?order=${order.orderId}&orderName=${orderName}" class="nameLinkRed"><c:out value="${order.orderId}"/></a>
				  </c:when>
				  <c:otherwise>
				  	<a href="invoice.jhtm?order=${order.orderId}&orderName=${orderName}" class="nameLink"><c:out value="${order.orderId}"/></a>
				  </c:otherwise>				  
				  </c:choose>
				  <span class="sup">
				    <c:choose>
					  <c:when test="${order.subscriptionCode != null}">&sup1;<c:set var="hasSubscription" value="true"/></c:when>
				    </c:choose>
				  </span>
				
				  <c:if test="${siteConfig['INVOICE_PDF_UPLOAD'].value == 'true'}">
					&nbsp;&nbsp;<a href="pdf.jhtm?orderId=${order.orderId}"/><img src="../graphics/pdf<c:if test="${order.pdfUrl == null}">_light</c:if>.gif" border="0"></a>
				  </c:if>
				  <a href="invoice.jhtm?order=${order.orderId}&__printDiscount=true" rel="width:850,height:400" id="mb${order.orderId}" class="mbOrder quickMode" title="<fmt:message key="invoice" /># : ${order.orderId}"><img src="../graphics/magnifier.gif" border="0"></a>
				  <c:choose>
				    <c:when test="${order.printed}"><img src="../graphics/checkbox.png" alt="Printed" title="Printed" border="0" class="quickMode"></c:when>
				    <c:otherwise><img src="../graphics/box.png" alt="Not Printed" title="Not Printed" border="0" class="quickMode"></c:otherwise>
				  </c:choose>
				  <div class="multiBoxDesc mb${order.orderId}"><fmt:message key="grandTotal" />: <fmt:formatNumber value="${order.grandTotal}" pattern="#,##0.00"/></div>
				  </td>
				</c:if> 
				<c:if test="${fn:contains(siteConfig['CUSTOMIZE_ORDER_LIST'].value,'mas200') and orderName != 'quotes'}">
				  <td align="center"><c:if test="${order.mas200orderNo != null}"><c:out value="${order.mas200orderNo}"/> (<c:out value="${order.mas200status}"/>)</c:if></td>
				</c:if>
			    <c:if test="${fn:contains(siteConfig['CUSTOMIZE_ORDER_LIST'].value,'extOrderId')}">
				  <td align="center"><c:if test="${order.externalOrderId != null}"><c:out value="${order.externalOrderId}"/></c:if></td>
			    </c:if>
				<c:if test="${gSiteConfig['gSERVICE'] and fn:contains(siteConfig['CUSTOMIZE_ORDER_LIST'].value,'workOrder') and orderName != 'quotes'}">
				  <td align="center">
				    <a href="../service/workOrder.jhtm?num=${order.workOrderNum}" class="nameLink"><c:out value="${order.workOrderNum}"/></a>
				    <c:if test="${order.workOrderNum != null}">
				    <c:if test="${model.serviceMap[order.workOrderNum] != null}"><a href="<c:url value="/assets/pdf/${model.serviceMap[order.workOrderNum].servicePdfUrl}"/>" target="_blank"><img src="../graphics/pdf.gif" border="0"/></a></c:if>
				    <c:if test="${model.serviceMap[order.workOrderNum] == null}"><a href="../service/pdf.jhtm?num=${order.workOrderNum}"/><img src="../graphics/pdf_light.gif" border="0"></a></c:if>
					</c:if>
				  </td>
				</c:if>				
				<c:if test="${fn:contains(siteConfig['CUSTOMIZE_ORDER_LIST'].value,'po') and orderName != 'quotes'}"> 
				  <td align="center"><c:out value="${order.purchaseOrder}"/></td>
				</c:if>  
				<c:if test="${fn:contains(siteConfig['CUSTOMIZE_ORDER_LIST'].value,'dateOrdered')}">
			    	<td align="center" style="font-size:9px" <c:if test="${model.firstOrderList[wj:intToLong(order.userId)] eq order.orderId}">class="nameLinkRed"</c:if>><fmt:formatDate type="date" timeStyle="full" pattern="MM/dd/yyyy hh:mm a" value="${order.dateOrdered}"/></td>
			    </c:if>
			    <c:if test="${fn:contains(siteConfig['CUSTOMIZE_ORDER_LIST'].value,'subTotal')}">
			      <c:choose>
			      <c:when test="${model.firstOrderList[wj:intToLong(order.userId)] eq order.orderId}"><td class="numberColRed" align="center"><fmt:formatNumber value="${order.subTotal}" pattern="#,##0.00" /></td></c:when>
			      <c:otherwise><td class="numberCol"><fmt:formatNumber value="${order.subTotal}" pattern="#,##0.00" /></td>
			      </c:otherwise>
			      </c:choose>
			      <c:set var="totalSubTotal" value="${order.subTotal + totalSubTotal}" />  			
				</c:if>
				<c:if test="${fn:contains(siteConfig['CUSTOMIZE_ORDER_LIST'].value,'grandTotal')}">  	  
				  <c:choose>
			      <c:when test="${model.firstOrderList[wj:intToLong(order.userId)] eq order.orderId}"><td class="numberColRed" align="center"><fmt:formatNumber value="${order.grandTotal}" pattern="#,##0.00" /></td></c:when>
			      <c:otherwise><td class="numberCol"><fmt:formatNumber value="${order.grandTotal}" pattern="#,##0.00" /></td></c:otherwise>
			      </c:choose> 
				  <c:set var="totalGrandTotal" value="${order.grandTotal + totalGrandTotal}" />
				</c:if>				
			    <c:if test="${fn:contains(siteConfig['CUSTOMIZE_ORDER_LIST'].value,'echoSign')}">
			      <td align="center"<c:if test="${model.firstOrderList[wj:intToLong(order.userId)] eq order.orderId}">class="nameLinkRed"</c:if>><c:if test="${model.echoSign[order.userId] != null}"></c:if><a href="<c:out value="${model.echoSign[order.userId].documentUrl}"/>" target="_blank"><c:out value="${model.echoSign[order.userId].documentName}"/></a>
			    </c:if>
				<c:if test="${fn:contains(siteConfig['CUSTOMIZE_ORDER_LIST'].value,'linkShare')}">
				  <td align="center" >
				  <c:choose>
				    <c:when test="${order.linkShare != null}"><img src="../graphics/checkbox.png" alt="<c:out value="${order.linkShare.siteID}" />" title="<c:out value="${order.linkShare.siteID}" />" border="0"></c:when>
				    <c:otherwise><img src="../graphics/box.png" alt="" title="" border="0"></c:otherwise>
				  </c:choose>		
				  </td>		
			    </c:if>
				<c:if test="${fn:contains(siteConfig['CUSTOMIZE_ORDER_LIST'].value,'orderType')}">
				  <td align="center" <c:if test="${model.firstOrderList[wj:intToLong(order.userId)] eq order.orderId}">class="nameLinkRed"</c:if>><c:out value="${order.orderType}" /></td>
				</c:if>
				<c:if test="${fn:contains(siteConfig['CUSTOMIZE_ORDER_LIST'].value,'promoCode')}">
				  <td align="center" <c:if test="${model.firstOrderList[wj:intToLong(order.userId)] eq order.orderId}">class="nameLinkRed"</c:if>><c:out value="${order.promoCode}" /></td>
				</c:if>
				<c:if test="${fn:contains(siteConfig['CUSTOMIZE_ORDER_LIST'].value,'shippingTitle') and orderName != 'quotes'}">
				  <td align="center" <c:if test="${model.firstOrderList[wj:intToLong(order.userId)] eq order.orderId}">class="nameLinkRed"</c:if>><c:out value="${order.shippingMethod}" escapeXml="false" /></td>
				</c:if>
				<c:if test="${gSiteConfig['gPAYMENTS'] and fn:contains(siteConfig['CUSTOMIZE_ORDER_LIST'].value,'amountPaid') and orderName != 'quotes'}">
				  <td align="right" <c:if test="${model.firstOrderList[wj:intToLong(order.userId)] eq order.orderId}">class="nameLinkRed"</c:if>><c:choose><c:when test="${order.amountPaid != 0 and order.amountPaid != null}"><fmt:formatNumber value="${order.amountPaid}" pattern="#,##0.00" /></c:when><c:otherwise>&nbsp;</c:otherwise></c:choose></td><c:set var="totalAmountPaid" value="${order.amountPaid + totalAmountPaid}" />
				</c:if>
				<c:if test="${fn:contains(siteConfig['CUSTOMIZE_ORDER_LIST'].value,'paymentMethod') and orderName != 'quotes'}">
				  <td align="center" <c:if test="${model.firstOrderList[wj:intToLong(order.userId)] eq order.orderId}">class="nameLinkRed"</c:if>><c:out value="${order.paymentMethod}" /></td>
				</c:if>  
				<c:if test="${gSiteConfig['gINVOICE_APPROVAL'] and fn:contains(siteConfig['CUSTOMIZE_ORDER_LIST'].value,'approval')}">
				  <td align="center" <c:if test="${model.firstOrderList[wj:intToLong(order.userId)] eq order.orderId}">class="nameLinkRed"</c:if>><c:choose><c:when test="${!empty order.approval}"><fmt:message key="approvalStatus_${order.approval}" /></c:when><c:otherwise>&nbsp;</c:otherwise></c:choose></td>
				</c:if>
				<c:if test="${fn:contains(siteConfig['CUSTOMIZE_ORDER_LIST'].value,'status')}">
				  <td align="center" <c:if test="${model.firstOrderList[wj:intToLong(order.userId)] eq order.orderId}">class="nameLinkRed"</c:if>><c:if test="${fn:contains(siteConfig['CUSTOMIZE_ORDER_LIST'].value,'priority') and not (order.status == 's' or fn:contains(order.status,'x'))}"><img src="../graphics/priority_${order.priority}.png" border="0"></c:if><c:choose><c:when test="${order.status != null}"><fmt:message key="orderStatus_${order.status}" /></c:when><c:otherwise>&nbsp;</c:otherwise></c:choose></td>
			    </c:if>
			    <c:if test="${siteConfig['SUB_STATUS'].value != '0' and fn:contains(siteConfig['CUSTOMIZE_ORDER_LIST'].value,'subStatus')}">
				  <td align="center" <c:if test="${model.firstOrderList[wj:intToLong(order.userId)] eq order.orderId}">class="nameLinkRed"</c:if>><c:choose><c:when test="${order.subStatus != null}"><fmt:message key="orderSubStatus_${order.subStatus}" /></c:when><c:otherwise>&nbsp;</c:otherwise></c:choose></td>
			    </c:if>			    
			   <td align="center" >${order.customerField19}</td>			 
				<td align="center" style="font-size:9px"><fmt:formatDate type="both" timeStyle="full"  pattern="MM/dd/yyyy hh:mm a" value="${order.userDueDate}"/></td>		    
			    <c:if test="${fn:contains(siteConfig['CUSTOMIZE_ORDER_LIST'].value,'deliveryPerson') and orderName != 'quotes'}">
				  <td align="center" <c:if test="${model.firstOrderList[wj:intToLong(order.userId)] eq order.orderId}">class="nameLinkRed"</c:if>><c:out value="${order.deliveryPerson}"/>
				</c:if>			    
			    <c:if test="${fn:contains(siteConfig['CUSTOMIZE_ORDER_LIST'].value,'dateShipped')}">
			      <td align="center" <c:if test="${model.firstOrderList[wj:intToLong(order.userId)] eq order.orderId}">class="nameLinkRed"</c:if>>
			      <c:choose>
			       <c:when test="${order.shipped == null or order.shipped == ''}">
			        <img class="quickMode" id="shipDateHelp${order.orderId}" src="../graphics/question.gif" onclick="showShipDate(${order.orderId})"/><div id="shipDateBlock${order.orderId}"></div>
			       </c:when>
			       <c:otherwise>
			        <fmt:formatDate type="date" timeStyle="default" value="${order.shipped}"/>
			       </c:otherwise>
			      </c:choose>
			      </td>	
			    </c:if>
			    <c:if test="${fn:contains(siteConfig['CUSTOMIZE_ORDER_LIST'].value,'trackCode')}">  
			      <td align="center" <c:if test="${model.firstOrderList[wj:intToLong(order.userId)] eq order.orderId}">class="nameLinkRed"</c:if>><c:out value="${order.trackcode}"/></td>
			    </c:if>  
			    <c:if test="${fn:contains(siteConfig['CUSTOMIZE_ORDER_LIST'].value,'qualifier')}">  
			      <td align="center" <c:if test="${model.firstOrderList[wj:intToLong(order.userId)] eq order.orderId}">class="nameLinkRed"</c:if>><c:out value="${model.salesRepMap[order.qualifier].name}"/></td>
			    </c:if> 
				<c:if test="${gSiteConfig['gSALES_REP'] and fn:contains(siteConfig['CUSTOMIZE_ORDER_LIST'].value,'salesRep')}">
			      <td align="center" <c:if test="${model.firstOrderList[wj:intToLong(order.userId)] eq order.orderId}">class="nameLinkRed"</c:if>><c:out value="${model.salesRepMap[order.salesRepId].name}"/></td>
			    </c:if>
				<c:if test="${gSiteConfig['gMULTI_STORE'] > 0 and fn:contains(siteConfig['CUSTOMIZE_ORDER_LIST'].value,'multiStore')}">
				  <td class="nameCol"><c:out value="${order.host}"/></td>
				</c:if>			    
			  </tr>
			</c:forEach>
			<tr class="totals quickMode">
			  <td style="color:#666666; padding: 5px" colspan="${1+cols}" align="left"><fmt:message key="total" /></td>
			  <td class="numberCol"><fmt:formatNumber value="${totalSubTotal}" pattern="#,##0.00" /></td>
			  <td class="numberCol"><fmt:formatNumber value="${totalGrandTotal}" pattern="#,##0.00" /></td>
			  <c:if test="${gSiteConfig['gPAYMENTS'] and fn:contains(siteConfig['CUSTOMIZE_ORDER_LIST'].value,'amountPaid')}">
			    <td class="numberCol"><fmt:formatNumber value="${totalAmountPaid}" pattern="#,##0.00" /></td>
			  </c:if>
			</tr>
			<c:if test="${model.count == 0}">
			  <tr class="emptyList"><td colspan="6">&nbsp;</td></tr>
			</c:if>
			</table>
			
			<c:if test="${hasSubscription}">
				<div class="sup">
					&sup1; <fmt:message key="subscription" />
				</div>
			</c:if>
				  
			<table border="0" cellpadding="0" cellspacing="1" width="100%" class="quickMode">
			  <tr>
			  <td>&nbsp;</td>  
			  <td class="pageSize">
			  <select name="size" onchange="document.getElementById('page').value=1;submit()">
			    <c:forTokens items="10,25,50,100,200,500" delims="," var="current">
			  	  <option value="${current}" <c:if test="${current == orderSearch.pageSize}">selected</c:if>>${current} <fmt:message key="perPage" /></option>
			    </c:forTokens>
			  </select>
			  </td>
			  </tr>
			</table>	  
		    <!-- end input field -->  	  
		  	</div>
		  	
		  	<!-- start button -->
			<!-- end button -->	
	        </div>
	
  	
	<!-- end tab -->        
	</div>
<!-- end tabs -->			
</div>
  
  <!-- end table -->
   </td><td class="boxmidr quickMode" ></td></tr>
  <tr><td class="botl quickMode"></td><td class="botr quickMode"></td></tr>
  </table>
  
<!-- end main box -->  
</div>	  
</form>	

</sec:authorize>

  </tiles:putAttribute>    
</tiles:insertDefinition>