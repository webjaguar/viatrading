<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>

<c:choose>
 <c:when test="${gSiteConfig['gINVENTORY']}">
   <c:forEach items="${model.products}" var="product">
	<li><c:out value="${product.sku}" /></li>
	<c:if test="${product.name != null}">
	<span class="name"><c:out value="${product.name}" /></span></c:if>
	<span class="name" ><c:if test="${product.name != null}"><c:out value="${product.packing}" /></c:if></span>
	<span class="name" ><c:if test="${product.name != null}"><c:out value="${product.field15}" /></c:if></span>
	<c:if test="${product.inventory != null}"><span class="sku">(<c:out value="${product.inventory}" />/<c:out value="${product.inventoryAFS}" />)</span></c:if>
   </c:forEach>
 </c:when>
 <c:otherwise>
   <c:forEach items="${model.products}" var="product">
	<li><c:out value="${product.sku}" /></li>
	
	<span class="name" ><c:if test="${product.name != null}"><c:out value="${product.name}" /></c:if></span>
	<span class="name" ><c:if test="${product.name != null}"><c:out value="${product.packing}" /></c:if></span>
	<span class="name" ><c:if test="${product.name != null}"><c:out value="${product.field15}" /></c:if></span>
	
   </c:forEach>
 </c:otherwise>
</c:choose>
<style>
span.name{visibility: hidden;display:none}
span.sku{visibility: hidden;display:none}
</style>