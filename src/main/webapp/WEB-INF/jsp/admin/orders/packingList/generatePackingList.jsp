<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<link href="<c:url value="/assets/invoice.css"/>" rel="stylesheet" type="text/css"/>

<tiles:insertDefinition name="admin.orders.packingList" flush="true">
  <tiles:putAttribute name="content" type="string">

<script type="text/javascript" src="../javascript/autocompleter1.2.js"></script>
<script type="text/javascript" src="../javascript/autocompleter.Request1.2.js"></script>
<script type="text/javascript" src="../javascript/autocompleter.Local1.2.js"></script>
<script type="text/javascript" src="../javascript/observer.js"></script>
<script type="text/javascript">
<!--	
function chooseMessage(el) {
    	$('emailMessageForm.subject').value = $('subject_'+el.value).value;
    	$('emailMessageForm.message').value = $('message_'+el.value).value;  
    	if ($('htmlID'+el.value).value == 'true') {
    		$('emailMessageForm.html1').checked = true;
		} else {
		    $('emailMessageForm.html1').checked = false;
		}              
}	
window.addEvent('domready', function(){			
			var tabs1 = new SimpleTabs($('tab-block-1'), { selector: 'h4'});
			var Tips1 = new Tips($$('.toolTipImg'));
		});
	 <c:if test="${gSiteConfig['gINVENTORY']}" >	
	 function ajaxRequest(pid,sku){
	 var el = $('container'+pid);
	 new Autocompleter.Request.HTML(el, '../orders/show-ajax-skus.jhtm?value='+sku, {
		'indicatorClass': 'autocompleter-loading'});
	 }	
	 </c:if>
//-->
</script>   
<form:form commandName="packingListForm" method="post">
<!-- main box -->
<div id="mboxfull">
  
  <!-- start table -->
  <table cellpadding="0" cellspacing="0" border="0" class="module" >
  <tr>
    <td class="topl_g">
	  
	  <!-- breadcrumb -->
	  <p class="breadcrumb">
	    <a href="../orders"><fmt:message key="orders" /></a> &gt;
	    <a href="../orders/invoice.jhtm?order=${param.orderId}"><fmt:message key="invoice"/> # <c:out value="${param.orderId}" /></a> &gt;
	    <a href="../orders/packingList.jhtm?orderId=${param.orderId}"><fmt:message key="packingList"/> # <c:out value="${param.orderId}" /></a> &gt;
	    form
	  </p>
	  
	  <!-- Error Message -->
	  <c:if test="${message != null}">
	    <div class="message"><fmt:message key="${message}" /></div>
	  </c:if>
	  <c:if test="${packingListForm.errorMessage != null}">
        <div class="message"><fmt:message key="${packingListForm.errorMessage}"/></div>
      </c:if>
  
  </td><td class="topr_g" ></td></tr>
  <tr><td class="boxmidlrg" >

<c:if test="${order != null}">
 <!-- tabs -->
 <div id="tab-block-1">
	<h4 title="Generate Packing List">Generate Packing List</h4>
	<div>
	<!-- start tab -->

	  	<div class="listdivi ln tabdivi"></div>
	  	<div class="listdivi"></div>
	       
	        <div class="list${classIndex % 2}"><c:set var="classIndex" value="${classIndex+1}" />
		  	<div class="listfl"><div align="left" class="helpImg"><img class="toolTipImg" title="Track Number::Shipping Track Number." src="../graphics/question.gif" /></div><fmt:message key="trackNum" />:</div>
		  	<div class="listp">
		  	<!-- input field -->
			  	<form:input path="packingList.tracking" maxlength="120" size="50" cssClass="textfield" htmlEscape="true" />
		       	<form:errors path="packingList.tracking" cssClass="error" />
		    <!-- end input field -->   	
		  	</div>
		  	</div>
		  	
		  	<div class="list${classIndex % 2}"><c:set var="classIndex" value="${classIndex+1}" />
		  	<div class="listfl"><div align="left" class="helpImg"><img class="toolTipImg" title="Ship Date::Specify the date you ship this packing list." src="../graphics/question.gif" /></div><fmt:message key="dateShipped" />:</div>
		  	<div class="listp">
		  	<!-- input field -->
		       	<form:input path="packingList.shipped" size="10" maxlength="10" />
				  <img class="calendarImage"  id="time_start_trigger" src="../graphics/calendarIcon.jpg" />
				  <script type="text/javascript">
		    		Calendar.setup({
		        			inputField     :    "packingList.shipped",   // id of the input field
		        			showsTime      :    false,
		        			ifFormat       :    "%m/%d/%Y",   // format of the input field
		        			button         :    "time_start_trigger"   // trigger for the calendar (button ID)
		    		});	
				  </script> 
				<form:errors path="packingList.shipped" cssClass="error" />
		    <!-- end input field -->   	
		  	</div>
		  	</div>
		  	
		  	
		  	<div class="list${classIndex % 2}"><c:set var="classIndex" value="${classIndex+1}" />
		  	<!-- input field -->				
				<table border="0" cellpadding="2" cellspacing="1" width="100%" class="invoice">
				  <tr>
				    <c:set var="cols" value="0"/>
				    <th width="5%" class="invoice"><fmt:message key="line" />#</th>
				    <th class="invoice"><fmt:message key="productSku" /></th>
				    <th class="invoice"><fmt:message key="productName" /></th>
				    <c:forEach items="${productFieldsHeader}" var="productField">
				      <c:if test="${productField.packingField}">
				      <th class="invoice"><c:out value="${productField.name}" /></th><c:set var="cols" value="${cols+1}"/>
				      </c:if>
				    </c:forEach>
				    <th width="10%" class="invoice"><fmt:message key="quantity" /></th>
				    <c:choose>
					    <c:when test="${packingListForm.newPackingList}">
					    <th width="10%" class="invoice"><fmt:message key="toShip" /></th>
					    </c:when>
					    <c:otherwise>
					    <th width="10%" class="invoice"><fmt:message key="qtyInThisPacking" /></th>
					    </c:otherwise>
					</c:choose>     
				    <th width="10%" class="invoice"><fmt:message key="shipped" /></th>
				    <th width="10%" class="invoice"><fmt:message key="price" /></th>
				    <c:if test="${gSiteConfig['gINVENTORY'] && order.hasLowInventoryMessage}">
				      <c:set var="cols" value="${cols+1}"/>
				      <th width="10%" class="invoice"><fmt:message key="lowInventory" /></th>
				    </c:if>
				    <c:if test="${order.hasPacking}">
					  <c:set var="cols" value="${cols+1}"/>
				      <th width="10%" class="invoice"><fmt:message key="packing" /></th>
				    </c:if>
				    <c:if test="${order.hasContent}">
					  <c:set var="cols" value="${cols+1}"/>
				      <th class="invoice"><fmt:message key="content" /></th>
				      <c:if test="${!empty siteConfig['EXT_QUANTITY_TITLE'].value}">
				      <c:set var="cols" value="${cols+1}"/>
				      <th class="invoice"><c:out value="${siteConfig['EXT_QUANTITY_TITLE'].value}" /></th>
				      </c:if>
				    </c:if>
				    <c:if test="${gSiteConfig['gINVENTORY']}">
				      <c:set var="cols" value="${cols+1}"/>
				      <th width="10%" class="invoice"><fmt:message key="checkInventory" /></th>
				    </c:if>
				    <th width="10%" class="invoice"><fmt:message key="total" /></th>
				  </tr>
					<c:forEach var="lineItem" items="${order.lineItems}" varStatus="status">
					<tr valign="top">
					  <td class="invoice" align="center"><c:out value="${status.count}"/></td>
					  <td class="invoice"><c:out value="${lineItem.product.sku}"/></td>
					  <td class="invoice"><c:out value="${lineItem.product.name}"/>
					   <table border="0" cellspacing="1" cellpadding="0">
						  <c:forEach items="${lineItem.productAttributes}" var="productAttribute" varStatus="productAttributeStatus">
							<tr class="shoppingCartOption${productAttributeStatus.count%2}">
							<td align="right"> -</td>
							<td style="white-space: nowrap" align="right"><c:out value="${productAttribute.optionName}"/>: </td>
							<td style="width:100%;padding-left:5px;"><c:out value="${productAttribute.valueString}" escapeXml="false"/></td>
							</tr>
						  </c:forEach>
					   </table>
					  </td>
					  <c:forEach items="${productFieldsHeader}" var="pFHeader" varStatus="status"><c:set var="check" value="0"/>
					  <c:if test="${pFHeader.packingField}">
					  <c:forEach items="${lineItem.productFields}" var="productField"> 
					   <c:if test="${pFHeader.id == productField.id}">
					    <td class="invoice"><c:out value="${productField.value}" /></td><c:set var="check" value="1"/>
					   </c:if>       
					  </c:forEach>
					  <c:if test="${check == 0}">
					    <td class="invoice">&nbsp;</td>
					   </c:if>
					  </c:if>
					  </c:forEach>
					  <td class="invoice" align="center"><c:out value="${lineItem.quantity}" /></td>
					  <c:choose>
					    <c:when test="${packingListForm.newPackingList}">
						<td class="invoice" align="center"> 
					      <input name="__quantity_${lineItem.lineNumber}" value="${lineItem.toBeShipQty}"/>
					    </td>
						</c:when>
						<c:otherwise><td class="invoice" align="center"><c:out value="${lineItem.toBeShipQty}" /></td></c:otherwise>
				      </c:choose>  
					  <td class="invoice" align="center"><c:out value="${lineItem.processed}" /></td>
					  <td class="invoice" align="center">
					    <c:out value="${lineItem.unitPrice}" />
					    <input name="__unitPrice_${lineItem.lineNumber}" value="${lineItem.unitPrice}" type="hidden"/>
					  </td>
					  <c:if test="${gSiteConfig['gINVENTORY'] && order.hasLowInventoryMessage}">
					   <td class="invoice" align="center"><c:out value="${lineItem.lowInventoryMessage}" /></td>
					  </c:if>
					  <c:if test="${order.hasPacking }">
					  <td class="invoice" align="center"><c:out value="${lineItem.product.packing}" /></td>
					  </c:if>
					  <c:if test="${order.hasContent}">
					    <td class="invoice" align="center"><c:out value="${lineItem.product.caseContent}" /></td>
					    <c:if test="${!empty siteConfig['EXT_QUANTITY_TITLE'].value}">
					    <td class="invoice" align="center">
					     <c:choose>
					      <c:when test="${!empty lineItem.product.caseContent}"><c:out value="${lineItem.product.caseContent * lineItem.quantity}" /></c:when>
					      <c:otherwise><c:out value="${lineItem.quantity}" /></c:otherwise> 
					     </c:choose>
					    </td>
					    </c:if>
					  </c:if>
					  <c:if test="${gSiteConfig['gINVENTORY']}">
					   <td class="invoice" ><c:if test="${lineItem.productId != null}"><img src="../graphics/up_off.gif" onclick="javascript:ajaxRequest(${lineItem.productId},'${lineItem.product.sku}');" id="__quantity_${lineItem.productId}" /></c:if><span id="container${lineItem.productId}"></span></td>
					  </c:if>
					  <td class="invoice" >
					   <fmt:formatNumber value="${lineItem.packingListTotalPrice}" pattern="#,##0.00"/>
 					  </td>
					</tr>
					</c:forEach>
					<tr bgcolor="#BBBBBB">
				      <td colspan="${8+cols}">&nbsp;</td>
				    </tr>
				    <tr bgcolor="#FFFFFF">
				      <td colspan="${7+cols}" align="right">
					    <fmt:message key="subTotal"/> 
					  </td>
				      <td align="right">
				        <fmt:formatNumber value="${packingListForm.packingList.subTotal}" pattern="#,##0.00"/>
					  </td>
				    </tr>
				    <tr bgcolor="#FFFFFF">
				      <td colspan="${7+cols}" align="right">
					    <fmt:message key="shippingCost"/> 
					  </td>
				      <td align="right">
					    <form:input path="packingList.shippingCost" maxlength="120" size="50" cssClass="textfield120" htmlEscape="true" />
		       			<form:errors path="packingList.shippingCost" cssClass="error" />
					  </td>
				    </tr>
				    <tr bgcolor="#FFFFFF">
				      <td colspan="${7+cols}" align="right">
					    <fmt:message key="grandTotal"/> 
					  </td>
				      <td align="right">
					    <fmt:formatNumber value="${packingListForm.packingList.grandTotal}" pattern="#,##0.00"/>
					  </td>
				    </tr>
				  </table>	
		    <!-- end input field -->   	
		  	</div>
	  	
	<!-- end tab -->        
	</div>         
  
  <h4 title="<fmt:message key="email" />"><fmt:message key="email" /></h4>
	<div>
	<!-- start tab -->
	<p>
	  	<div class="listdivi ln tabdivi"></div>
	  	<div class="listdivi"></div>
		  	
		  	<div class="list${classIndex % 2}"><c:set var="classIndex" value="${classIndex+1}" />
		  	<div class="listfl"><fmt:message key="from"/>:</div>
		  	<div class="listp">
		  	<!-- input field -->
	   			<c:out value="${packingListForm.emailMessageForm.from}"/>
	            <form:errors path="emailMessageForm.from" cssClass="error" delimiter=", "/>
		    <!-- end input field -->   	
		  	</div>
		  	</div>  

		  	<div class="list${classIndex % 2}"><c:set var="classIndex" value="${classIndex+1}" />
		  	<div class="listfl"><div class="helpImg"><img class="toolTipImg" title="Note::Use comma to send to multiple recipient." src="../graphics/question.gif" /></div><fmt:message key="to"/>:</div>
		  	<div class="listp">
		  	<!-- input field -->
	   			<form:input path="emailMessageForm.to" size="50" maxlength="150" htmlEscape="true"/>
	            <form:errors path="emailMessageForm.to" cssClass="error" delimiter=", "/>
		    <!-- end input field -->   	
		  	</div>
		  	</div>  
		  	
		  	<div class="list${classIndex % 2}"><c:set var="classIndex" value="${classIndex+1}" />
		  	<div class="listfl"><div class="helpImg"><img class="toolTipImg" title="Note::Use comma to send to multiple recipient." src="../graphics/question.gif" /></div>Cc:</div>
		  	<div class="listp">
		  	<!-- input field -->
	   			<form:input path="emailMessageForm.cc" size="50" maxlength="150" htmlEscape="true"/>
	            <form:errors path="emailMessageForm.cc" cssClass="error" delimiter=", "/>
		    <!-- end input field -->   	
		  	</div>
		  	</div>  
		  	
		  	<div class="list${classIndex % 2}"><c:set var="classIndex" value="${classIndex+1}" />
		  	<div class="listfl">Bcc:</div>
		  	<div class="listp">
		  	<!-- input field -->
	   			<c:out value="${packingListForm.emailMessageForm.bcc}"/>
		    <!-- end input field -->   	
		  	</div>
		  	</div>  
		  	
		  	<div class="list${classIndex % 2}"><c:set var="classIndex" value="${classIndex+1}" />
		  	<div class="listfl"><fmt:message key="subject"/>:</div>
		  	<div class="listp">
		  	<!-- input field -->
	   			<form:input path="emailMessageForm.subject" size="50" maxlength="50" htmlEscape="true"/>
		        <form:errors path="emailMessageForm.subject" cssClass="error"/>
		    <!-- end input field -->   	
		  	</div>
		  	</div> 
		  	
		  	<div class="list${classIndex % 2}"><c:set var="classIndex" value="${classIndex+1}" />
		  	<div class="listfl">Html:</div>
		  	<div class="listp">
		  	<!-- input field -->
	   			<form:checkbox path="emailMessageForm.html" />
	  			  <c:forEach var="message" items="${messages}">
	    		  <input type="hidden" id="htmlID${message.messageId}" value="<c:out value="${message.html}"/>" />
	  			  </c:forEach>
	            <form:errors path="emailMessageForm.html" cssClass="error"/>
		    <!-- end input field -->   	
		  	</div>
		  	</div> 
		  	
		  	<div class="list${classIndex % 2}"><c:set var="classIndex" value="${classIndex+1}" />
		  	<div class="listfl"><div class="helpImg"><img class="toolTipImg" title="Note::Message is required to send out email." src="../graphics/question.gif" /></div><fmt:message key="message"/>:</div>
		  	<div class="listp">
		  	<!-- input field -->
	   			<form:textarea path="emailMessageForm.message" rows="15" cols="60" htmlEscape="true"/>
	            <form:errors path="emailMessageForm.message" cssClass="error"/>
	            <div id="messageSelect">
			      <select id="messageSelected" onChange="chooseMessage(this)">
			        <option value="">choose a message</option>
				    <c:forEach var="message" items="${messages}">
				      <option value="${message.messageId}"><c:out value="${message.messageName}"/></option>
				    </c:forEach>
				  </select>
				  
				  <c:forEach var="message" items="${messages}">
				    <input type="hidden" id="subject_${message.messageId}" value="<c:out value='${message.subject}'/>" />
				    <input type="hidden" id="message_${message.messageId}" value="<c:out value='${message.message}'/>" />
				  </c:forEach>
			      <input type="hidden" id="subject_" value="" />
			  	  <input type="hidden" id="message_" value="" />
				</div>
		    <!-- end input field -->   	
		  	</div>
		  	</div>  

	<!-- end tab -->        
	</div>
<!-- end tabs -->			
</div>
</c:if> 

<!-- start button -->
<div align="left" class="button">
  <c:choose>
	<c:when test="${packingListForm.newPackingList}">
	  <input type="submit" name="__generate" value="<fmt:message key="generatePackingList"/>">
	  <input type="submit" name="__recalculate" value="<fmt:message key="recalculate"/>">
	</c:when>
	<c:otherwise>
	  <input type="submit" name="__update" value="<fmt:message key="update"/>">
	</c:otherwise>
  </c:choose>
	<input type="submit" value="<fmt:message key="cancel"/>" name="_cancel">
</div>
<!-- end button -->	
 
  <!-- end table -->
   </td><td class="boxmidr" ></td></tr>
  <tr><td class="botl"></td><td class="botr"></td></tr>
  </table>
  
<!-- end main box -->  
</div>
</form:form>
   
</tiles:putAttribute>    
</tiles:insertDefinition> 	  	
  