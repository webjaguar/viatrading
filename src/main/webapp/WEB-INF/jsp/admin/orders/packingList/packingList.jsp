<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ page import="com.webjaguar.model.*, java.text.NumberFormat, java.util.*, java.text.*,java.lang.reflect.Method" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
  <head>
<%
Layout layout = (Layout) request.getAttribute("packingLayout");
Customer customer = (Customer) request.getAttribute("customerLogo");
if(customer != null && customer.getCustomerType() != null && customer.getCustomerType() == 1 && customer.getBrokerImage() != null && customer.getBrokerImage().getImageUrl() != null && !customer.getBrokerImage().getImageUrl().isEmpty()) {
	layout.replace("#customerLogo#", "<img src=\""+request.getContextPath()+"/assets/Image/Customer/"+customer.getBrokerImage().getImageUrl()+"\" alt=\""+customer.getAddress().getFirstName()+"\" border=\"0\" height=\"100\" />");
}else{
	layout.replace("#customerLogo#","");
}
if(customer != null && customer.getAddress() != null){
	layout.replace("#customerfirstName#",customer.getAddress().getFirstName() == null? "" : customer.getAddress().getFirstName());
	layout.replace("#customerlastName#",customer.getAddress().getLastName() == null ? "" : customer.getAddress().getLastName());
	layout.replace("#customerCompany#",customer.getAddress().getCompany() == null ? "" : customer.getAddress().getCompany());
	layout.replace("#customerAddressAddr1#",customer.getAddress().getAddr1() == null? "" : customer.getAddress().getAddr1());
	layout.replace("#customerAddressAddr2#",customer.getAddress().getAddr2() == null? "" : customer.getAddress().getAddr2());
	layout.replace("#customerAddressCity#",customer.getAddress().getCity() == null? "" : customer.getAddress().getCity());
	layout.replace("#customerAddressStateProvince#",customer.getAddress().getStateProvince() == null? "" : customer.getAddress().getStateProvince());
	layout.replace("#customerAddressZip#",customer.getAddress().getZip() == null? "" : customer.getAddress().getZip());
	layout.replace("#customerCountries#",customer.getAddress().getCountry() == null? "" : customer.getAddress().getCountry());
	layout.replace("#customerPhone#",customer.getAddress().getPhone() == null? "" : customer.getAddress().getPhone());
	layout.replace("#customerAddressCellPhone#",customer.getAddress().getCellPhone() == null? "" : customer.getAddress().getCellPhone());
}else{
	layout.replace("#customerfirstName#","");
	layout.replace("#customerlastName#","");
	layout.replace("#customerCompany#","");
	layout.replace("#customerAddressAddr1#","");
	layout.replace("#customerAddressAddr2#","");
	layout.replace("#customerAddressCity#","");
	layout.replace("#customerAddressStateProvince#","");
	layout.replace("#customerAddressZip#","");
	layout.replace("#customerCountries#","");
	layout.replace("#customerPhone#","");
	layout.replace("#customerAddressCellPhone#","");
}

%>
    <title>Invoice</title>
    <link href="<c:url value="/assets/invoice.css"/>" rel="stylesheet" type="text/css"/>
  </head>  
<body class="invoice">
<div style="width:700px;margin:left;">

<c:if test="${order != null}">
<div align="right"><a href="javascript:window.print()"><img border="0" src="../graphics/printer.png"></a></div>
<c:out value="${packingLayout.headerHtml}" escapeXml="false"/>

<table border="0" width="100%" cellspacing="3" cellpadding="1">
<tr valign="top">
<td>
<b><fmt:message key="shippingInformation" />:</b>
<c:set value="${order.shipping}" var="address"/>
<%@ include file="/WEB-INF/jsp/frontend/common/address.jsp" %>
</td>
<td>&nbsp;</td>
<td align="right">
<table>
  <tr id="invoice">
    <td><fmt:message key="invoice" /> #</td>
    <td>:&nbsp;&nbsp;</td>
    <td><b><c:out value="${order.orderId}"/></b></td>
  </tr>
  <tr id="date">
    <td><fmt:message key="date" /></td>
    <td>:&nbsp;&nbsp;</td>
    <td><fmt:formatDate type="date" timeStyle="default" value="${order.dateOrdered}"/></td>
  </tr>
  <tr id="shipping">
    <td><fmt:message key="shipping" /></td>
    <td>:&nbsp;&nbsp;</td>
    <td><c:out value="${order.shippingMethod}"/></td>
  </tr>
  <c:if test="${salesRep != null}">
  <tr id="salesRep">
    <td><fmt:message key="salesRep"/></td>
    <td>:&nbsp;&nbsp;</td>
    <td><c:out value="${salesRep.name}"/></td>
  </tr>
  <tr id="processedBy">
    <td><fmt:message key="processedBy"/></td>
    <td>:&nbsp;&nbsp;</td>
    <td><c:out value="${salesRepProcessedBy.name}"/></td>
  </tr>
  </c:if>
</table>
</td>
</tr>
</table>
<hr>
<p>
<c:if test="${order.purchaseOrder != '' and order.purchaseOrder != null}" >
<b><fmt:message key="purchaseOrder" /></b>: <c:out value="${order.purchaseOrder}" />
</c:if>
<table border="0" cellpadding="2" cellspacing="1" width="100%" class="invoice">
  <tr>
    <c:set var="cols" value="0"/>
    <th width="5%" class="invoice"><fmt:message key="line" />#</th>
    <th class="invoice"><fmt:message key="productSku" /></th>
    <th width="6%" class="invoice"><fmt:message key="location" /></th>
    <th class="invoice"><fmt:message key="productName" /></th>
    <c:forEach items="${productFieldsHeader}" var="productField">
      <c:if test="${productField.packingField}">
      <th class="invoice"><c:out value="${productField.name}" /></th><c:set var="cols" value="${cols+1}"/>
      </c:if>
    </c:forEach>
    <th width="7%" class="invoice"><fmt:message key="quantity" /></th>
    <c:if test="${order.hasPacking}">
	  <c:set var="cols" value="${cols+1}"/>
      <th width="7%" class="invoice"><fmt:message key="packing" /></th>
    </c:if>
    <c:if test="${order.hasContent}">
	  <c:set var="cols" value="${cols+1}"/>
      <th class="invoice"><fmt:message key="content" /></th>
	  <c:if test="${!empty siteConfig['EXT_QUANTITY_TITLE'].value}">
       <c:set var="cols" value="${cols+1}"/>
       <th class="invoice"><c:out value="${siteConfig['EXT_QUANTITY_TITLE'].value}" /></th>
      </c:if>
    </c:if>
  </tr>
<c:forEach var="lineItem" items="${order.lineItems}" varStatus="status">
<tr valign="top">
  <td class="invoice" align="center"><c:out value="${status.count}"/></td>
  <td class="invoice">
    <c:if test="${siteConfig['SHOW_IMAGE_ON_INVOICE'].value == 'true' and lineItem.product.thumbnail != null}">
	  <img class="invoiceImage" border="0" <c:if test="${!lineItem.product.thumbnail.absolute}"> src="<c:url value="/assets/Image/Product/thumb/${lineItem.product.thumbnail.imageUrl}"/>" </c:if> <c:if test="${lineItem.product.thumbnail.absolute}"> src="<c:url value="${lineItem.product.thumbnail.imageUrl}"/>" </c:if> />
	</c:if><c:out value="${lineItem.product.sku}"/>
  </td>
  <td class="invoice" align="center"><c:out value="${lineItem.location}" /></td>
  <td class="invoice"><c:out value="${lineItem.product.name}"/>
    <table border="0" cellspacing="1" cellpadding="0">
	  <c:forEach items="${lineItem.productAttributes}" var="productAttribute" varStatus="productAttributeStatus">
		<tr class="invoice_lineitem_attributes${productAttributeStatus.count%2}">
		<td align="right"> -</td>
		<td class="optionName"style="white-space: nowrap" align="right"><c:out value="${productAttribute.optionName}"/>: </td>
		<td class="optionValue"><c:out value="${productAttribute.valueString}" escapeXml="false"/></td>
		<c:if test="${(! empty productAttribute.optionPriceOriginal) && productAttribute.optionPriceOriginal != 0}">
		<td class="optionPrice"><fmt:formatNumber value="${productAttribute.optionPriceOriginal}" pattern="#,##0.00" /></td>
		<td class="optionNote" style="padding-left:5px;"><fmt:message key="${productAttribute.optionPriceMessageF}"/></td>
		</c:if>
		</tr>
	  </c:forEach>
   </table>
  </td>
  <c:forEach items="${productFieldsHeader}" var="pFHeader" varStatus="status"><c:set var="check" value="0"/>
  <c:if test="${pFHeader.packingField}">
  <c:forEach items="${lineItem.productFields}" var="productField"> 
   <c:if test="${pFHeader.id == productField.id}">
    <td class="invoice"><c:out value="${productField.value}" /></td><c:set var="check" value="1"/>
   </c:if>       
  </c:forEach>
  <c:if test="${check == 0}">
    <td class="invoice">&nbsp;</td>
   </c:if>
  </c:if>
  </c:forEach>
  <td class="invoice" align="center"><c:out value="${lineItem.toBeShipQty}"/>
  <c:if test="${lineItem.priceCasePackQty != null}">
  	<div class="casePackPriceQty">(<c:out value="${lineItem.priceCasePackQty}"/> per <c:out value="${siteConfig['PRICE_CASE_PACK_UNIT_TITLE'].value}" /> )</div>
  </c:if>
  </td>
  <c:if test="${order.hasPacking }">
  <td class="invoice" align="center"><c:out value="${lineItem.product.packing}" /></td>
  </c:if>
  <c:if test="${order.hasContent}">
    <td class="invoice" align="center"><c:out value="${lineItem.product.caseContent}" /></td>
    <c:if test="${!empty siteConfig['EXT_QUANTITY_TITLE'].value}">
    <td class="invoice" align="center">
     <c:choose>
      <c:when test="${!empty lineItem.product.caseContent}"><c:out value="${lineItem.product.caseContent * lineItem.quantity}" /></c:when>
      <c:otherwise><c:out value="${lineItem.quantity}" /></c:otherwise> 
     </c:choose>
    </td>
    </c:if>
  </c:if>
</tr>
</c:forEach>
  <tr bgcolor="#BBBBBB">
    <td colspan="${5+cols}">&nbsp;</td>
  </tr>
</table>
<p>
<hr>
<c:if test="${order.invoiceNote != null and order.invoiceNote != ''}">
<table width="100%">
	<tr>
	 <td valign="top"><b><c:out value="${siteConfig['SPECIAL_INSTRUCTIONS'].value}"/>:</b></td>
    </tr>
	<tr>
	 <td><c:out value="${order.invoiceNote}" escapeXml="false"/></td>
	</tr>
</table>
</c:if>
<c:out value="${packingLayout.footerHtml}" escapeXml="false"/>
</c:if>

</div>

</body>
</html>