<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ taglib uri="http://www.springframework.org/security/tags" prefix="sec" %>

<tiles:insertDefinition name="admin.orders.packingList" flush="true">
  <tiles:putAttribute name="content" type="string">
<c:if test="${siteConfig['GENERATE_PACKING_LIST'].value == 'true'}" >  
<script type="text/javascript">
function toggleAll(el) {
	var ids = document.getElementsByName("__selected_id");	
  	if (ids.length == 1)
      document.list_form.__selected_id.checked = el.checked;
    else
      for (i = 0; i < ids.length; i++)
        document.list_form.__selected_id[i].checked = el.checked;	
}  
function deleteSelected() {
	var ids = document.getElementsByName("__selected_id");	
  	if (ids.length == 1) {
      if (document.list_form.__selected_id.checked) {
    	return confirm('Delete permanently?');
      }
    } else {
      for (i = 0; i < ids.length; i++) {
        if (document.list_form.__selected_id[i].checked) {
    	  return confirm('Delete permanently?');
        }
      }
    }

    alert("Please select packing list(s) to delete.");       
    return false;
}
function toggleMe(id) {
	var subNavs = $$('div.subNav');
	subNavs.each(function(el) {
		$(el).setStyles({'display': 'none'});
	});
	
	$('JumpSubnav'+id).setStyles({
		display: 'block',
		visibility: 'visible',
        top: $('img'+id).getTop() + 5 + 'px',
        left: $('img'+id).getLeft() + 25 + 'px'
    });
} 
</script> 
<form action="packingList.jhtm" id="list" method="post" name="list_form">
<input type="hidden" id="sort" name="sort" value="${packingListSearch.sort}" />
<input type="hidden" name="orderId" value="${model.orderId}" />
<!-- main box -->
  <div id="mboxfull">
  
  <!-- start table -->
  <table cellpadding="0" cellspacing="0" border="0" class="module" >
  <tr>
    <td class="topl_g">
	  
	  <!-- breadcrumb -->
	  <p class="breadcrumb">
	    <a href="../orders"><fmt:message key="orders"/></a> &gt;
	    <a href="../orders/invoice.jhtm?order=${model.orderId}"><fmt:message key="invoice"/> # <c:out value="${model.orderId}" /></a> &gt;
	    <fmt:message key="packingList" />
	  </p>
	  
	  <!-- Error Message -->
	  
	  <!-- header image -->
	  <img class="headerImage" src="../graphics/packingList.gif" />

  
  </td><td class="topr_g" ></td></tr>
  <tr><td class="boxmidlrg" >

 <!-- tabs -->
 <div class="tab-wrapper">
	<h4 title="List of affiliates"></h4>
	<div>
	<!-- start tab -->

	  	<div class="listdivi ln tabdivi"></div>
	  	<div class="listdivi"></div>
	   
		  	<div class="listlight">
		  	<!-- input field -->
		  	<table border="0" cellpadding="0" cellspacing="1" width="100%">
			  <tr>
			  <td class="pageShowing">
			  <c:if test="${model.packingList.nrOfElements > 0}">
			  <fmt:message key="showing">
				<fmt:param value="${model.packingList.firstElementOnPage + 1}"/>
				<fmt:param value="${model.packingList.lastElementOnPage + 1}"/>
				<fmt:param value="${model.packingList.nrOfElements}"/>
			  </fmt:message>
			  </c:if>
			  </td>
			  <td class="pageNavi">
			  Page 
			  <select name="page" id="page" onchange="submit()">
			  <c:forEach begin="1" end="${model.packingList.pageCount}" var="page">
			  	<option value="${page}" <c:if test="${page == (model.packingList.page+1)}">selected</c:if>>${page}</option>
			  </c:forEach>
			  </select>
			  of <c:out value="${model.packingList.pageCount}"/>
			  | 
			  <c:if test="${model.packingList.firstPage}"><span class="pageNaviDead"><fmt:message key="previous" /></span></c:if>
			  <c:if test="${not model.packingList.firstPage}"><a href="<c:url value="packingList.jhtm"><c:param name="page" value="${model.orders.page}"/></c:url>" class="pageNaviLink"><fmt:message key="previous" /></a></c:if>
			  | 
			  <c:if test="${model.packingList.lastPage}"><span class="pageNaviDead"><fmt:message key="next" /></span></c:if>
			  <c:if test="${not model.packingList.lastPage}"><a href="<c:url value="packingList.jhtm"><c:param name="page" value="${model.orders.page+2}"/></c:url>" class="pageNaviLink"><fmt:message key="next" /></a></c:if>
			  </td>
			  </tr>
			</table>
			
			<table border="0" cellpadding="0" cellspacing="0" width="100%" class="listings">
			  <tr class="listingsHdr2">
			  <c:if test="${model.packingList.nrOfElements > 0}">
			    <td align="center"><input type="checkbox" onclick="toggleAll(this)"></td>
			  </c:if>
			    <td class="nameCol" colspan="2">
			    	<table cellspacing="0" cellpadding="1">
			    	  <tr>
			    	    <c:choose>
			    	      <c:when test="${packingListSearch.sort == 'packinglist_num desc'}">
			    	        <td class="listingsHdr3">
			    	        <a class="hdrLink" href="#" onClick="document.getElementById('sort').value='packinglist_num';document.getElementById('list').submit()"><fmt:message key="packingList" /></a>
			    	        </td>
			    	        <td><img src="../graphics/up.gif" border="0"></td>
			    	      </c:when>
			    	      <c:when test="${packingListSearch.sort == 'packinglist_num'}">
			    	        <td class="listingsHdr3">
			    	        <a class="hdrLink" href="#" onClick="document.getElementById('sort').value='packinglist_num desc';document.getElementById('list').submit()"><fmt:message key="packingList" /></a>
			    	        </td>
			    	        <td><img src="../graphics/down.gif" border="0"></td>
			    	      </c:when>
			    	      <c:otherwise>
			    	        <td colspan="2" class="listingsHdr3">
			    	        <a class="hdrLink" href="#" onClick="document.getElementById('sort').value='packinglist_num desc';document.getElementById('list').submit()"><fmt:message key="packingList" /></a>
			    	        </td>
			    	      </c:otherwise>
			    	    </c:choose>  
			    	  </tr>
			    	</table>
			    </td>
			    <td align="center">
			    	<table cellspacing="0" cellpadding="1">
			    	  <tr>
			    	    <c:choose>
			    	      <c:when test="${packingListSearch.sort == 'created DESC'}">
			    	        <td class="listingsHdr3">
			    	        <a class="hdrLink" href="#" onClick="document.getElementById('sort').value='created';document.getElementById('list').submit()"><fmt:message key="dateGenerated" /></a>
			    	        </td>
			    	        <td><img src="../graphics/up.gif" border="0"></td>
			    	      </c:when>
			    	      <c:when test="${packingListSearch.sort == 'created'}">
			    	        <td class="listingsHdr3">
			    	        <a class="hdrLink" href="#" onClick="document.getElementById('sort').value='created DESC';document.getElementById('list').submit()"><fmt:message key="dateGenerated" /></a>
			    	        </td>
			    	        <td><img src="../graphics/down.gif" border="0"></td>
			    	      </c:when>
			    	      <c:otherwise>
			    	        <td colspan="2" class="listingsHdr3">
			    	        <a class="hdrLink" href="#" onClick="document.getElementById('sort').value='created DESC';document.getElementById('list').submit()"><fmt:message key="dateGenerated" /></a>
			    	        </td>
			    	      </c:otherwise>
			    	    </c:choose>  
			    	  </tr>
			    	</table>
			    </td>
			    <td align="center" class="listingsHdr3"><fmt:message key="lastModified" /></td>
			    <td align="center" class="listingsHdr3"><fmt:message key="dateShipped" /></td>
			    <td align="center" class="listingsHdr3"><fmt:message key="customerNotified" /></td>
			    <td align="center">
			    	<table cellspacing="0" cellpadding="1">
			    	  <tr>
			    	    <c:choose>
			    	      <c:when test="${packingListSearch.sort == 'tracking desc'}">
			    	        <td class="listingsHdr3">
			    	        <a class="hdrLink" href="#" onClick="document.getElementById('sort').value='tracking';document.getElementById('list').submit()"><fmt:message key="trackNum" /></a>
			    	        </td>
			    	        <td><img src="../graphics/up.gif" border="0"></td>
			    	      </c:when>
			    	      <c:when test="${packingListSearch.sort == 'tracking'}">
			    	        <td class="listingsHdr3">
			    	        <a class="hdrLink" href="#" onClick="document.getElementById('sort').value='tracking desc';document.getElementById('list').submit()"><fmt:message key="trackNum" /></a>
			    	        </td>
			    	        <td><img src="../graphics/down.gif" border="0"></td>
			    	      </c:when>
			    	      <c:otherwise>
			    	        <td colspan="2" class="listingsHdr3">
			    	        <a class="hdrLink" href="#" onClick="document.getElementById('sort').value='tracking desc';document.getElementById('list').submit()"><fmt:message key="trackNum" /></a>
			    	        </td>
			    	      </c:otherwise>
			    	    </c:choose>  
			    	  </tr>
			    	</table>
			    </td>
			  </tr>
			<c:forEach items="${model.packingList.pageList}" var="packingList" varStatus="status">
			  <div id="navigation">
			  <tr class="row${status.index % 2}" onmouseover="changeStyleClass(this,'rowOver')" onmouseout="changeStyleClass(this,'row${status.index % 2}')">
			    <c:if test="${model.packingList.nrOfElements > 0}">
			      <td align="center"><input name="__selected_id" value="${packingList.packingListNumber}" type="checkbox"></td>
			    </c:if>
			    <td class="nameCol">
			      <a href="<c:url value="generatePackingList.jhtm"><c:param name="orderId" value="${packingList.orderId}"/><c:param name="packing_num" value="${packingList.packingListNumber}"/></c:url>" class="nameLink"><c:out value="${packingList.packingListNumber}"/></a>
			    </td>
			    <td align="center">
				  <span id="img${status.index}" onmouseover="toggleMe(${status.index})"><img src="../graphics/printer.png" alt="print Purchase Order"  border="0"></span>
				  <div id="JumpSubnav${status.index}" class="subNav">
	           		<ul id="subnavItems" style="width: 133px;">
	           			<li style="width: 100%;"><a class="userbutton" style="background-color: #0094df;" href="https://www.viatrading.com/dv/inv/invoice.php?order_id=${model.encodedId}&packing=true" onclick="window.open(this.href,'','width=800,height=300,resizable=yes'); return false;"><fmt:message key="walkin" /></a></li>
	               		<li style="width: 100%;"><a class="userbutton" style="background-color: #0094df;" href="https://www.viatrading.com/dv/inv/invoice.php?order_id=${model.encodedId}&packingother=true" onclick="window.open(this.href,'','width=800,height=300,resizable=yes'); return false;"><fmt:message key="courierLtl" /></a></li>
	               		<li style="width: 100%;"><a class="userbutton" href="packingList.jhtm?orderId=${packingList.orderId}&printPackingList=2&packing_num=${packingList.packingListNumber}" onclick="window.open(this.href,'','width=800,height=300,resizable=yes'); return false;"><fmt:message key="packingList2" /></a></li>
	               		<li style="width: 100%;"><a class="userbutton" href="packingList.jhtm?orderId=${packingList.orderId}&printPackingList=3&packing_num=${packingList.packingListNumber}" onclick="window.open(this.href,'','width=800,height=300,resizable=yes'); return false;"><fmt:message key="packingList3" /></a></li>
	           		</ul>
	       		  </div>
        	    </td>			
			    <td align="center"><fmt:formatDate type="date" timeStyle="default" value="${packingList.created}"/></td>
			    <td align="center"><fmt:formatDate type="date" timeStyle="default" value="${packingList.lastModified}"/></td>
			    <td class="red" align="center"><fmt:formatDate type="date" timeStyle="default" value="${packingList.shipped}"/></td>	
			    <td align="center"><c:if test="${packingList.userNotified}"><img src="../graphics/checkbox.png"></c:if>
			  				<c:if test="${not packingList.userNotified}">&nbsp;</c:if></td>
			    <td align="center"><c:out value="${packingList.tracking}"/></td>
			  </tr>
			  </div>
			</c:forEach>
			<c:if test="${model.packingList.nrOfElements == 0}">
			  <tr class="emptyList"><td colspan="6">&nbsp;</td></tr>
			</c:if>
			</table>
			

        			  
			<table border="0" cellpadding="0" cellspacing="1" width="100%">
			  <tr>
			  <td>&nbsp;</td>  
			  <td class="pageSize">
			  <select name="size" onchange="document.getElementById('page').value=1;submit()">
			    <c:forTokens items="10,25,50,100" delims="," var="current">
			  	  <option value="${current}" <c:if test="${current == model.packingList.pageSize}">selected</c:if>>${current} <fmt:message key="perPage" /></option>
			    </c:forTokens>
			  </select>
			  </td>
			  </tr>
			</table>	  
		    <!-- end input field -->  	  
		  	</div>
		  	
		  	<!-- start button -->
		  	<input type="hidden" name="orderId" value="${model.orderId}">
            <div align="left" class="button">
              <c:if test="${!model.fulfilled}" >
				<input type="submit" name="__generatePackingList" value="<fmt:message key='generatePackingList' />"/>
				<c:if test="${model.packingList.nrOfElements > 0}">
			      <input type="submit" name="__delete" value="Delete Selected Packing List" onClick="return deleteSelected()">
			    </c:if>
			  </c:if>		
	  	    </div>
			<!-- end button -->	

  	
	<!-- end tab -->        
	</div>
<!-- end tabs -->			
</div>
  
  <!-- end table -->
   </td><td class="boxmidr" ></td></tr>
  <tr><td class="botl"></td><td class="botr"></td></tr>
  </table>
  
<!-- end main box -->  
</div>	  
</form>	
</c:if>
  </tiles:putAttribute>    
</tiles:insertDefinition>