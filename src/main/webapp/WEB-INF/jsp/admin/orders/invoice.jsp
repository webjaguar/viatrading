<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ taglib uri="http://www.springframework.org/security/tags" prefix="sec" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<link href="<c:url value="/assets/invoice.css"/>" rel="stylesheet" type="text/css"/>

<tiles:insertDefinition name="admin.orders" flush="true">
  <tiles:putAttribute name="tab"  value="orders" />
  <tiles:putAttribute name="content" type="string">

<!-- customized order page iframe --> 
<iframe id="orderPageFrame" src="" style="height:200px;width:200px;display:none"></iframe> 

<script language="JavaScript"> 
var mbPOBox = {};
<!--
window.addEvent('domready', function(){			
	var tabs1 = new SimpleTabs($('tab-block-1'), { selector: 'h4'});
	mbPOBox = new multiBox('mbPO', {descClassName: 'multiBoxDesc',showNumbers: false,openFromLink: false});
	var addCreditBox = new multiBox('mbAddCredit', {showControls : false, useOverlay: false, showNumbers: false });
	var applyCreditBox = new multiBox('mbApplyCredit', {showControls : false, useOverlay: false, showNumbers: false });
	var customer = new multiBox('mbCustomer', {waitDuration: 5,overlay: new overlay()});
});

function creditADValue(credit,userId) {
	document.getElementById('creditAD_'+userId).value = credit;
}



function sendShippedEmail(orderId) {
	var request = new Request({
		url: "/dv/orderreportnewdb/shippedemails.php?Function=SendEmail" + "&orderid=" + orderId,
		method: 'post',
		onComplete: function(response) { 
			console.log("Email for Invoice " + orderId + " has been sent successfully.");
			location.reload(true);
		}
	}).send();	
} 
		
function addCredit(userId) {
	var addCredit = $('creditAD_'+userId).value;
	var request = new Request({
		url: "customer-ajax-credit.jhtm?userId="+userId+"&addCredit="+addCredit,
		method: 'post',
		onComplete: function(response) { 
			location.reload(true);
		}
	}).send();
	
}
function deductCredit(userId) {
	var deductCredit = $('creditAD_'+userId).value;
	var request = new Request({
		url: "customer-ajax-credit.jhtm?userId="+userId+"&deductCredit="+deductCredit,
		method: 'post',
		onComplete: function(response) { 
			location.reload(true);
		}
	}).send();
}

function creditValue(credit,orderId) {
	document.getElementById('credit_'+orderId).value = credit;
}

function getSiteMessage(groupId, orderId) {
    var req = new Request.HTML({
    url: '../config/show-ajax-groupSiteMessage.jhtm?groupId='+groupId+ '&order='+orderId,
        update: $('siteMessageSelect'),
        onSuccess: function(response){
        	document.getElementById('messageSelect').style.display="block";
        }       
    }).send();
}
		
function applyCredit(orderId) {
	var creditAmount = $('credit_'+orderId).value;
	var customerCredit = $('customerCredit_'+orderId).value;
	var balance= $('balance_'+orderId).value;
	
<%-- TODO: parseFloat returns wierd number if value is grated than 999  --%>
	/*
	alert("Number: " + Math.round(parseFloat($('customerCredit_'+orderId).value)*100)/100);
	
	if(+creditAmount > +customerCredit) {
		alert ("Customer has only $"+customerCredit+" credit available.");
	} else if(creditAmount > balance)  {
		alert ("Balance is only: $"+balance);
	} else { */
		var request = new Request({
			url: "invoice.jhtm?order="+orderId+"&orderName=order&creditAmount="+creditAmount+"&applyCredit="+true,
			method: 'post',
			onComplete: function(response) { 
				location.reload(true);
			}
		}).send();
	/*}*/
}
function toggleMessage() {
   	if (document.getElementById("userNotified1").checked) {
   	  document.getElementById('messageBox').style.display="block";
   	  document.getElementById('messageSelect').style.display="block";
   	  document.getElementById('htmleditor').style.display="inline-block";
   	} else {
   	  document.getElementById('messageBox').style.display="none";
   	  document.getElementById('messageSelect').style.display="none";
   	  document.getElementById('htmleditor').style.display="none";
   	}
}

function chooseMessage(el, orderId) {
	
   	messageList = document.getElementsByName('message');
	for(var i=0; i<messageList.length; i++) {
		messageList[i].disabled=true;
		messageList[i].style.display="none";
	}
   	subjectList = document.getElementsByName('subject');
	for(var i=0; i<subjectList.length; i++) {
		subjectList[i].disabled=true;
		subjectList[i].style.display="none";
	}
   	document.getElementById('message'+el.value).disabled=false;
       document.getElementById('message'+el.value).style.display="block"; 
   	document.getElementById('subject'+el.value).disabled=false;
       document.getElementById('subject'+el.value).style.display="block";     
    if (document.getElementById('htmlID'+el.value).value == 'true') {
    	$('htmlMessage1').checked = true;
	} else {
	    $('htmlMessage1').checked = false;
	} 
    
    //Customized order page logic below
    
	// Encode the orderId
	var base64EncodedOrderId = btoa(orderId);  
	
    // get the customized external order page url
        if (document.getElementById('sourceUrl'+el.value) != null && document.getElementById('sourceUrl'+el.value).value.length != 0) {
        	var customizedExternalOrderPageUrl = document.getElementById('sourceUrl'+el.value).value + "&order_id=" + base64EncodedOrderId; 
        	
        	var xmlhttp=GetXmlHttpObject();
        	if (xmlhttp==null)
        	  {
        	  alert ("Your browser does not support XMLHTTP!");
        	  return;
        	  }
        	      	    		
     		var customizedExternalOrderPageUrl = document.getElementById('sourceUrl'+el.value).value + "&order_id=" + base64EncodedOrderId; 
    		
    		xmlhttp.onreadystatechange = function() {
    		    if (xmlhttp.readyState == 4 && xmlhttp.status == 200) {    		    	
    		    	var responsePageHtml = xmlhttp.responseText; 
    		    	console.log("reponse page successful");
    		    	/* console.log(responsePageHtml); */
                  	document.getElementById('message'+el.value).value = responsePageHtml;
    		    }
     		}
    		xmlhttp.open("GET", customizedExternalOrderPageUrl, true); 
    		/* xmlhttp.open("GET", "http://localhost:8080/admin/message/", true);  */
    		xmlhttp.send();
    		   		  		
    		function GetXmlHttpObject(){
	    		if (window.XMLHttpRequest)
	    		  {
	    		  // code for IE7+, Firefox, Chrome, Opera, Safari
	    		  return new XMLHttpRequest();
	    		  }
	    		if (window.ActiveXObject)
	    		  {
	    		  // code for IE6, IE5
	    		  return new ActiveXObject("Microsoft.XMLHTTP");
	    		  }
	    		return null;
    		}      	
        }    	
}	

function getExternalOrderPage(orderId) {
	// Encode the orderId
	var base64EncodedOrderId = btoa(orderId);
	/* alert(""); */
	
    // get the customized external order page url
/*     if (document.getElementById('sourceUrl'+el.value) != null && document.getElementById('sourceUrl'+el.value).value.length != 0) {
    	var customizedExternalOrderPageUrl = document.getElementById('sourceUrl'+el.value).value + "&order_id=" + base64EncodedOrderId; 
    	document.getElementById("orderPageFrame").src = customizedExternalOrderPageUrl;  
    	document.getElementById("orderPageFrame").onload = function(){
          	document.getElementById('message'+el.value).value = document.getElementById("orderPageFrame").contentWindow.document.body.innerHTML;
    	}       	
    }  */
}

function trimString(str){
 var returnVal = "";
 for(var i = 0; i < str.length; i++){
   if(str.charAt(i) != ' '){
     returnVal += str.charAt(i);
   }
 }
 return returnVal;
}
function checkMessage() {
    if (document.getElementById('userNotified1').checked) {
      var message = document.getElementById('message' + document.getElementById('messageSelected').value);
      var subject = document.getElementById('subject' + document.getElementById('messageSelected').value);
      if (trimString(subject.value) == "") {
        alert("Please put a subject");
        subject.focus();
    	return false;
      }
      if (trimString(message.value) == "") {
        alert("Please put a message");
        message.focus();
    	return false;
      }
    }
    return true;
}
function toggleMe(id) {
	var subNavs = $$('div.subNav');
	subNavs.each(function(el) {
		$(el).setStyles({'display': 'none'});
	});
	
	$('JumpSubnav'+id).setStyles({
		 display: 'block',
		 visibility: 'visible',
         top: $('img'+id).getTop() + 30 + 'px',
         left: $('img'+id).getLeft() + 5 + 'px'
    });
}
function checkStatus() {
	var mx = 0;
	var ind = 0;
	var stat = false;
	if (($('status').value == 's')) {
		mx = $('customShippingMax').value;
		for(ind = 0; ind < mx; ind++){
			if(($('shippingMethod')!=null) && $('shipping_'+ind).value == $('shippingMethod').value){
				stat = true;
			}
		}
	}
	if(($('status').value == 's') && (!stat)){
		$('shippingMessage').style.display='block';
		return false;
	}
	if(($('status').value == 'x') && ($('cancelReason').value == '')){
		$('cancelReasonMessage').style.display='block';
		return false;
	}
	
	if(($('status').value != 'x') && ($('cancelReason').value != '')){
		$('wrongStatus').style.display='block';
		return false;
	}
	
	if (($('status').value == 's') || ($('status').value.contains("x"))) {
		return confirm("Are you sure?");
	}
	return true;
}
<c:if test="${siteConfig['CUSTOM_SHIPPING_CONTACT_INFO'].value == 'true'}">
function insertContact(el) {
	//var request = new Request({
	//	url: "../../admin/catalog/contact-company-ajax.jhtm?contactcompany="+el.value,
	//	method: 'post',
	//	onComplete: function(response) { 
	//		$('contact').value=response;
	//	//}
	//}).send();-->
	//$('carrier').value =$('contact_company_'+el.value).value;
	return true;
}
</c:if>
//-->

function loadPreview() {

	var selectedMsgId = document.getElementById("messageSelected").value;
	var html = document.getElementById("message"+selectedMsgId).value;
	try {
		var oEditor = FCKeditorAPI.GetInstance('siteMessage.message');
		html = oEditor.GetHTML(true);
	} catch(err) {}
	var request = new Request.HTML({
		url: "${_contextpath}/orders/ajaxMessagePreview.jhtm",
		onSuccess: function(response) {
			var w = window.open("","_blank", "toolbar=yes,scrollbars=yes,resizable=yes,top=500,left=500,width=700,height=500");
			w.document.open();
			w.document.write(this.response.text);
			w.document.close();
		}
		}).post({'message': ""+html+""});
}
</script>
<!-- main box -->
<div id="mboxfull">
  
  <!-- start table -->
  <table cellpadding="0" cellspacing="0" border="0" class="module" >
  <tr>
    <td class="topl_g">
	  
	  <!-- breadcrumb -->
	  <p class="breadcrumb">
	    <a href="../orders"><fmt:message key="orders" /></a> &gt;
	    <fmt:message key="order"/> # <c:out value="${param.order}"/>
	  </p>
	  <div class="message" style='display:none;' id="shippingMessage">Shipping title is not valid</div>
	  <div class="message" style='display:none;' id="cancelReasonMessage">Please select a cancel reason</div>
	  <div class="message" style='display:none;' id="wrongStatus">Please select cancel reason only if you wish to cancel the order</div>
	  
	  <!-- Error Message -->
	  <c:if test="${message != null}">
	    <div class="message"><fmt:message key="${message}" /></div>
	  </c:if>
      
  </td><td class="topr_g" ></td></tr>
  <tr><td class="boxmidlrg" >
<c:if test="${order != null}">
 <!-- tabs -->
 <div id="tab-block-1">
	<h4 title="<fmt:message key="order"/>"><fmt:message key="order"/></h4>
	<div>
	<!-- start tab -->

	  	<div class="listdivi ln tabdivi"></div>
	  	<div class="listdivi"></div>
	     
	     
	    <form action="invoice.jhtm" method="post" id="invoiceTools"> 
		  	<input type="hidden" name="order" value="<c:out value="${order.orderId}"/>" >
		  	<input type="hidden" name="balance" id="balance_${order.orderId}" value="<c:out value="${order.grandTotal - order.amountPaid}"/>" >
		  	<div class="toolsArea">
		  	  <table width="100%" cellspacing="0" cellpadding="0" border="0">
		  	   <tr>
		  	    <td>
		  	     <div class="screenButton">		  	    
		  	       <sec:authorize ifAnyGranted="ROLE_AEM,ROLE_ADMIN_${prop_site_id},ROLE_SHIPPED_INVOICE_EDIT">
			        <c:if test="${(siteConfig['ALLOW_EDIT_INVOICE_SHIPPED'].value == 'true' or !(order.status == 's' || order.status == 'x')) and (order.paymentMethod == null || order.paymentMethod != 'Amazon' || (order.paymentMethod == 'Amazon' and siteConfig['AMAZON_ORDER_ALLOW_EDIT'].value == 'true'))}">
		  	        <div class="commonButton">
		  	         <span id="img1" onmouseover="toggleMe(1)"><a class="navbutton" href="<c:url value="invoiceForm.jhtm"><c:param name="orderId" value="${order.orderId}"/></c:url>" class="nameLink"><img src="../graphics/edit.gif" alt="<fmt:message key='edit' />"  border="0">&nbsp;<fmt:message key="edit" /></a></span>
		  	         <div id="JumpSubnav1" class="subNav"></div>
		  	        </div>
		  	        </c:if>		  	       
			       </sec:authorize>
			       <sec:authorize ifAllGranted="ROLE_INVOICE_EDIT">
		  	        <c:if test="${!(order.status == 's' || order.status == 'x') and (order.paymentMethod == null || order.paymentMethod != 'Amazon' || (order.paymentMethod == 'Amazon' and siteConfig['AMAZON_ORDER_ALLOW_EDIT'].value == 'true'))}">
		  	        	<div class="commonButton">
			  	         <span id="img1" onmouseover="toggleMe(1)"><a class="navbutton" href="<c:url value="invoiceForm.jhtm"><c:param name="orderId" value="${order.orderId}"/></c:url>" class="nameLink"><img src="../graphics/edit.gif" alt="<fmt:message key='edit' />"  border="0">&nbsp;<fmt:message key="edit" /></a></span>
			  	         <div id="JumpSubnav1" class="subNav"></div>
			  	        </div>
		  	        </c:if>
		  	       </sec:authorize>
			      <c:if test="${gSiteConfig['gPAYMENTS'] and !(siteConfig['ALLOW_EDIT_PAYMENT_SHIPPED'].value == 'false' || order.status == 'x')}">
		  	      <div class="commonButton"> 
		  	       <span id="img2" onmouseover="toggleMe(2)"><img src="../graphics/payment.png" width="16" alt="<fmt:message key='payment' />"  border="0">&nbsp;<fmt:message key="payment" /></span>
		  	       <div id="JumpSubnav2" class="subNav">
		  	       	<ul id="subnavItems">
		  	       		<sec:authorize ifAnyGranted="ROLE_AEM,ROLE_ADMIN_${prop_site_id},ROLE_INCOMING_PAYMENT_CREATE">
		  	       			<li><a class="userbutton" href="../customers/payment.jhtm?cid=${order.userId}&orderId=${order.orderId}" ><fmt:message key="add" /> <fmt:message key="payment"/></a></li>
		  	       		</sec:authorize>
	               		 <sec:authorize ifAnyGranted="ROLE_AEM,ROLE_ADMIN_${prop_site_id},ROLE_INCOMING_PAYMENT_CANCEL">
	               		 <c:if test="${order.paymentHistory != null and !empty order.paymentHistory and fn:length(order.paymentHistory) gt 0}">
	               		 	<li><a class="userbutton" href="../customers/cancelPayments.jhtm?orderId=${order.orderId}&cid=${order.userId}" ><fmt:message key="cancel" /> <fmt:message key="payment"/></a></li>
	               		 </c:if>   		
		  	       		 </sec:authorize>
		  	       		 <c:if test="${customer.credit > 0.00 and (order.grandTotal - order.amountPaid > 0.00) }">
	               		 <li>
				          	<a class="userbutton mbApplyCredit" href="#${order.orderId}" rel="type:element" id="mbApplyCredit${order.orderId}" ><fmt:message key="f_vba"/></a>
				          	<div id="${order.orderId}" class="miniWindowWrapper applyCreditBox">
					          	<div class="header"><fmt:message key="f_vba"/></div>
					          	<fieldset class="top">
				    	    		<label class="AStitle"><fmt:message key="credit"/> <fmt:message key="available"/></label>
				    	    		<input type="text" name="__customerCredit_" id="customerCredit_${order.orderId}" value="<fmt:formatNumber value="${customer.credit}" pattern="#,##0.00" /> " disabled="disabled"/>
				    	  		</fieldset>
				    	  		<fieldset class="bottom">
				    	  			<c:choose>
				    	  			<c:when test="${gSiteConfig['gBUDGET'] and customer.creditAllowed != null and  customer.creditAllowed > 0}">
				    	  				<label class="AStitle"><fmt:message key="credit"/><span style="color: #FF0000;"> *</span></label>				    	  			
					    	    		<c:set value="${customer.creditAllowed}" var="budgetPercentage"/>
					    	    		<c:set value="${budgetPercentage * (order.balance / 100)}" var="creditOnPercentage"/>
					    	  			<input type="text" value="<fmt:formatNumber value="${creditOnPercentage}" pattern="#,##0.00" />" name="__credit_" id="credit_${order.orderId}" onkeyup="creditValue(this.value, ${order.orderId});"/>
					    	  			<div class="sup" style="">* Credit calculated based on ${customer.creditAllowed}% of balance.</div>
				    	  			</c:when>
				    	  			<c:otherwise>
				    	  				<label class="AStitle"><fmt:message key="credit"/><span style="color: #FF0000;"> </span></label>
										<input type="text" name="__credit_" id="credit_${order.orderId}" onkeyup="creditValue(this.value, ${order.orderId});"/>
				    	  			</c:otherwise>
				    	  			</c:choose>
				    	  			<div class="button">
				    	  				<input type="submit" value="Apply"  name="__apply_credit_" onclick="applyCredit('${order.orderId}');"/>
				    	  			</div>
				    	  		</fieldset>
					        </div>
				          </li>
	               		</c:if>	  	       	
		  	       	</ul>
		  	       </div>
		  	      </div>
		  	      </c:if>
		  	      <c:choose>
				  <c:when test="${!order.paymentAlert}">
					<div class="commonButton"> 
		  	       <span id="img3" onmouseover="toggleMe(3)"><img src="../graphics/printer.png" alt="print Purchase Order"  border="0">&nbsp;<fmt:message key="print"/></span>
		  	       <div id="JumpSubnav3" class="subNav">
	           		<ul id="subnavItems">
	           			<li><a class="userbutton" style="background-color: #0094df;" target="_blank" href="https://www.viatrading.com/dv/inv/invoice.php?order_id=${encodedId}&walkin=true"><fmt:message key="walkin" /></a></li>
	           			<li><a class="userbutton" style="background-color: #0094df;" target="_blank" href=" https://www.viatrading.com/dv/inv/invoice.php?order_id=${encodedId}&walkall=true"> <fmt:message key="walkin" /> ALL</a></li>           			
	           			<li><a class="userbutton" style="background-color: #0094df;"target="_blank"  href="https://www.viatrading.com/dv/inv/invoice.php?order_id=${encodedId}&other=true"><fmt:message key="courierLtl" /></a></li>
	               		<li><a class="userbutton" style="background-color: #0094df;" target="_blank" href="https://www.viatrading.com/dv/inv/invoice.php?order_id=${encodedId}&packing=true">Packing List Walk-In</a></li>
	               		<li><a class="userbutton" style="background-color: #0094df;" target="_blank" href="https://www.viatrading.com/dv/inv/invoice.php?order_id=${encodedId}&packingother=true">Packing List Other</a></li>	               		
	               		<li><a class="userbutton" href="invoice.jhtm?order=${order.orderId}&__print=1"><fmt:message key="printerFriendly" /></a></li>	               	
	               		<li><a class="userbutton" href="invoice.jhtm?order=${order.orderId}&__label=true"><fmt:message key="label" /></a></li>
	               		<li><a class="userbutton" href="invoice.jhtm?order=${order.orderId}&__printDiscount=true"><fmt:message key="showDiscount" /></a></li>
	               		
	               		<c:if test="${siteConfig['ORDER_OVERVIEW'].value == 'true'}">
	               		 <li><a class="userbutton" href="invoice.jhtm?order=${order.orderId}&__overview=true"><fmt:message key="overview" /></a></li>
	               		</c:if>

	               		 
	               		
	           		</ul>
	       		  </div>
		  	      </div>
		  	      </c:when>
				  <c:otherwise>
				  <sec:authorize ifAnyGranted="ROLE_AEM,ROLE_ADMIN_${prop_site_id},ROLE_OVERRIDE_PAYMENT_ALERT">
				   <div class="commonButton"> 
		  	       <span id="img3" onmouseover="toggleMe(3)"><img src="../graphics/printer.png" alt="print Purchase Order"  border="0">&nbsp;<fmt:message key="print"/></span>
		  	       <div id="JumpSubnav3" class="subNav">
	           		<ul id="subnavItems">
	               		<li><a class="userbutton" href="invoice.jhtm?order=${order.orderId}&__print=1"><fmt:message key="printerFriendly" /></a></li>
	               		<c:if test="${gSiteConfig['gSITE_DOMAIN'] == 'tcintlinc.com'}">
	               		<li><a class="userbutton" href="invoice.jhtm?order=${order.orderId}&__print=2"><fmt:message key="printerFriendly" /> 2</a></li>
	               		<li><a class="userbutton" href="invoice.jhtm?order=${order.orderId}&__print=3"><fmt:message key="pickingSheet" /></a></li>
	               		</c:if>
	               		<li><a class="userbutton" href="invoice.jhtm?order=${order.orderId}&__label=true"><fmt:message key="label" /></a></li>
	               		<li><a class="userbutton" href="invoice.jhtm?order=${order.orderId}&__printDiscount=true"><fmt:message key="showDiscount" /></a></li>
	               		<li><a class="userbutton" href="invoice.jhtm?order=${order.orderId}&__walkin=true"><fmt:message key="walkin" /></a></li>
	               		<c:if test="${siteConfig['ORDER_OVERVIEW'].value == 'true'}">
	               		 <li><a class="userbutton" href="invoice.jhtm?order=${order.orderId}&__overview=true"><fmt:message key="overview" /></a></li>
	               		</c:if>
	           		</ul>
	       		  </div>
		  	      </div>
		  	      </sec:authorize>
		  	      </c:otherwise>
			      </c:choose>
		  	      <c:if test="${gSiteConfig['gPDF_INVOICE']}">
		  	      <div class="commonButton">
		  	       <span id="img4" onmouseover="toggleMe(4)"><img src="../graphics/email.gif" alt="Email"  border="0">&nbsp;<fmt:message key="email"/></span>
		  	       <div id="JumpSubnav4" class="subNav">
	           		<ul id="subnavItems">
	               		<li><a class="userbutton" href="email.jhtm?orderId=${order.orderId}" ><fmt:message key="email" />&nbsp;<fmt:message key="order"/></a></li>
	               		<li><a class="userbutton" href="emailPacking.jhtm?orderId=${order.orderId}" ><fmt:message key="email"/>&nbsp;<fmt:message key="packingList"/></a></li>
	           			 <li><a target="blank" class="userbutton"  style="background-color: #0094df;" href="https://www.viatrading.com/dv/emailapps/template/load.php?order_id=${order.orderId}" >
	           			 &nbsp;Email APPS</a></li>
	           		</ul>
	       		  </div>
		  	      </div>
		  	      </c:if>
		  	      <sec:authorize ifAnyGranted="ROLE_AEM,ROLE_ADMIN_${prop_site_id},ROLE_INVOICE_PACKING_LIST,ROLE_DROP_SHIP_CREATE,ROLE_PURCHASE_ORDER_CREATE">
		  	      <c:if test="${(!order.orderCancel) and (siteConfig['GENERATE_PACKING_LIST'].value == 'true' or (gSiteConfig['gINVENTORY'] and gSiteConfig['gPURCHASE_ORDER']))}">
		  	        <div class="commonButton"> 
		  	          <span id="img5" onmouseover="toggleMe(5)"><img src="../graphics/generator.gif" alt="print Purchase Order"  border="0">&nbsp;<fmt:message key="action"/></span>
		  	          <div id="JumpSubnav5" class="subNav">
		           	  <ul id="subnavItems">
		           		<sec:authorize ifAnyGranted="ROLE_AEM,ROLE_ADMIN_${prop_site_id},ROLE_INVOICE_PACKING_LIST">
				          <c:if test="${siteConfig['GENERATE_PACKING_LIST'].value == 'true'}"> 
				            <li><a class="userbutton" href="generatePackingList.jhtm?orderId=${order.orderId}"><fmt:message key="packingList" /></a></li>
				          </c:if>
				         <!--     <li><a class="userbutton" href="generatePackingList.jhtm?orderId=${order.orderId}&&__walkin=true"><fmt:message key="walkin" /></a></li> -->
				        </sec:authorize>
				             <c:if test="${gSiteConfig['gINVENTORY'] and gSiteConfig['gPURCHASE_ORDER']}">
				          <!--      <sec:authorize ifAnyGranted="ROLE_AEM,ROLE_ADMIN_${prop_site_id},ROLE_DROP_SHIP_CREATE">
				               	 <li><a class="userbutton" href="../inventory/purchaseOrderForm.jhtm?orderId=${order.orderId}&dropship=true" ><fmt:message key="dropShip" /></a></li>
				               </sec:authorize> -->
				               <!--  Hide for viatrading.com 
				               <sec:authorize ifAnyGranted="ROLE_AEM,ROLE_ADMIN_${prop_site_id},ROLE_PURCHASE_ORDER_CREATE">
				                 <li><a class="userbutton" href="../inventory/purchaseOrderForm.jhtm?orderId=${order.orderId}" ><fmt:message key="purchaseOrder" /></a></li>
				               	</sec:authorize> 
				               	-->
				             </c:if>
		           		</ul>
	       		        </div>
		  	          </div>
		  	        <c:if test="${gSiteConfig['gINVENTORY'] and gSiteConfig['gPURCHASE_ORDER'] and false}">
		  	          <div class="commonButton">
		  	            <span id="img7" onmouseover="toggleMe(7)"><a class="mbPO navbutton" rel="width:750,height:400" id="mbPOLineItems" href="../inventory/ajaxShowOrderLineItems.jhtm?orderId=${order.orderId}"> <button style="width: 140px; height: 25px;">Create PO</button> </a></span>
		  	            <div id="JumpSubnav7" class="subNav"></div>
		  	          </div>
		  	        </c:if>
		  	      </c:if>
	       		  </sec:authorize>
		  	      <c:if test="${gSiteConfig['gACCOUNTING'] == 'MACS'}">
		  	      <div class="commonButton">
		  	       <span id="img6" onmouseover="toggleMe(6)"><a class="navbutton" href="<c:url value="invoice.jhtm"><c:param name="order" value="${order.orderId}"/><c:param name="__macs" value="__macs"/></c:url>" onclick="document.getElementById('__macs').value='__macs'; document.getElementById('invoiceTools').submit(); return false;" class="nameLink"><img src="../graphics/dataFeed.gif" width="16" alt="MACS"  border="0">&nbsp;MACS</a></span>
		  	       <div id="JumpSubnav6" class="subNav"></div>
		  	      </div>
		  	      </c:if>
		  	      
		  	      <c:if test="${siteConfig['PREMIER_DISTRIBUTOR'].value != ''}">
	           	    <c:if test="${!order.fulfilled and (!order.orderCancel) and (siteConfig['GENERATE_PACKING_LIST'].value == 'true' or (gSiteConfig['gINVENTORY'] and gSiteConfig['gPURCHASE_ORDER']))}">
		  	          <sec:authorize ifAnyGranted="ROLE_AEM,ROLE_ADMIN_${prop_site_id},ROLE_INVOICE_PACKING_LIST,ROLE_DROP_SHIP_CREATE,ROLE_PURCHASE_ORDER_CREATE">
		  	            <div class="commonButton dropShipButton" align="right" style="float:right;"> 
		  	              <c:if test="${gSiteConfig['gINVENTORY'] and gSiteConfig['gPURCHASE_ORDER']}">
			                <sec:authorize ifAnyGranted="ROLE_AEM,ROLE_ADMIN_${prop_site_id},ROLE_DROP_SHIP_CREATE">
			                  <a class="mbPO" rel="width:850,height:400" id="mbPOLineItems" href="../inventory/ajaxShowOrderLineItems.jhtm?orderId=${order.orderId}" >Create Purchase Order(s)</a>
			                </sec:authorize>
			              </c:if>
	           	        </div>
		  	      		<div style="clear:both;"></div>
		  	      	  </sec:authorize>
		  	        </c:if>
	       		  </c:if>
	           	 
	           	 </div>
		  	    </td>
		  	   </tr>
		  	  </table>
		  	</div>
			</form>
			
			<div class="list0">
		  	<!-- input field -->
			<hr>
			
			<table border="0" width="100%" cellspacing="3" cellpadding="1">
			<tr valign="top">
			<td>
			<b><fmt:message key="billingInformation" />:</b>
			<c:set value="${order.billing}" var="address"/>
			<c:set value="true" var="billing" />
			<%@ include file="/WEB-INF/jsp/frontend/common/address.jsp" %>
			<c:set value="false" var="billing" />
			</td>
			<td>&nbsp;</td>
			<td>
			<b><c:choose><c:when test="${order.workOrderNum != null}"><fmt:message key="service" /> <fmt:message key="location" /></c:when><c:otherwise><fmt:message key="shippingInformation" /></c:otherwise></c:choose>:</b>
			<c:set value="${order.shipping}" var="address"/>
			<%@ include file="/WEB-INF/jsp/frontend/common/address.jsp" %>
			 <span style="width:16px;float:left;"><a href="http://maps.google.com/?q=loc:${address.addr1}+${address.city}+${address.stateProvince}+${address.country}" target="new"><img src="../graphics/map_small.gif" alt="Map" title="Map" border="0"></a></span> 
			<c:if test="${not empty workOrder.service.alternateContact}">	
			  <fmt:message key="alternateContact" />: <c:out value="${workOrder.service.alternateContact}"/>
			</c:if>			
			</td>
			<c:choose>
			  <c:when test="${gSiteConfig['gSEARCH_ENGINE_PROSPECT'] and !empty searchEngineProspect}">
			  <td>&nbsp;</td>
			  <td align="left" style="width:250px;">
			  	<b><fmt:message key="orderReferal" />:</b>
				  <table>
				  <tr>
				    <td><fmt:message key="referrer"/></td>
				    <td>:&nbsp;&nbsp;</td>
				    <td><c:out value="${searchEngineProspect.referrer}" />
				  </td>
				  <tr>
				    <td><fmt:message key="keyPhrase"/></td>
				    <td>:&nbsp;&nbsp;</td>
				    <td><c:out value="${searchEngineProspect.queryString}" />
				  </td>
				  </tr>
				  </table>
			  </td>
			  </c:when>
			  <c:otherwise>
			  <td style="width:20%;">&nbsp;</td>
			  </c:otherwise>
			</c:choose>
			<td align="left">
			<b><fmt:message key="customerInformation" />:</b><a href="../customers/customerQuickView.jhtm?cid=${customer.id}" rel="width:900,height:400" id="mb${customer.id}" class="mbCustomer" title="<fmt:message key="customer" />ID : ${customer.id}"><img src="../graphics/magnifier.gif" border="0"></a>
			<table cellpadding="0" cellspacing="0">
			  <tr>
			    <td><fmt:message key="account"/> #</td>
			    <td>:&nbsp;&nbsp;</td>
			    <td><c:out value="${customer.accountNumber}"/></td>
			  </tr>
			  <tr>
			    <td><fmt:message key="companyName"/></td>
			    <td>:&nbsp;&nbsp;</td>
			    <td><c:out value="${customer.address.company}"/></td>
			  </tr>
			  <tr>
			    <td><fmt:message key="taxID"/> #</td>
			    <td>:&nbsp;&nbsp;</td>
			    <td><c:out value="${customer.taxId}"/></td>
			  </tr>
			  <tr>
			    <td><fmt:message key="orderCount"/></td>
			    <td>:&nbsp;&nbsp;</td>
			    <td><a href="ordersList.jhtm?email=<c:out value='${customer.username}'/>"><c:out value="${customerOrderInfo.orderCount}"/></a></td>
			  </tr>
			  <tr>
			    <td><fmt:message key="lastOrder"/></td>
			    <td>:&nbsp;&nbsp;</td>
			    <td><fmt:formatDate pattern="MM/dd/yyyy" value="${customerOrderInfo.lastOrderDate}"/></td>
			  </tr>
			  <c:if test="${gSiteConfig['gGIFTCARD'] or siteConfig['POINT_TO_CREDIT'].value == 'true' or gSiteConfig['gVIRTUAL_BANK_ACCOUNT'] or gSiteConfig['gBUDGET']}">
			  <tr>
			    <td><fmt:message key="credit"/></td>
			    <td>:&nbsp;&nbsp;</td>
			    <td>
			    	<fmt:message key="${siteConfig['CURRENCY'].value}" /><fmt:formatNumber value="${customer.credit}" pattern="#,##0.00" />
			    <sec:authorize ifAnyGranted="ROLE_AEM,ROLE_ADMIN_${prop_site_id},ROLE_CUSTOMER_CREDIT_UPDATE_DELETE">
			    <c:if test="${gSiteConfig['gBUDGET']}">
		          <a href="#${customer.id}" rel="type:element" id="mb${customer.id}" class="mbAddCredit">	<c:out value="${customer.credit}"/></a>
		          <div id="${customer.id}" class="miniWindowWrapper addCreditBox displayNone">
			          <div class="header"><fmt:message key="add"/>&nbsp;<fmt:message key="credit"/></div>
		    	  		<fieldset class="top">
		    	    		<label class="AStitle"><fmt:message key="customer"/>&nbsp;<fmt:message key="name"/></label>
		    	    		<input name="name" type="text" value="<c:out value="${customer.address.firstName}"/>&nbsp;<c:out value="${customer.address.lastName}"/>" disabled="disabled"/>
		    	  		</fieldset>
		    	  		<fieldset class="bottom">
		    	    		<label class="AStitle"><fmt:message key="credit"/>&nbsp;<fmt:message key="available"/></label>
		    	    		<input name="creditAvailable" type="text" value="<fmt:formatNumber value="${customer.credit}" pattern="#,##0.00" />" disabled="disabled"/>
		    	  		</fieldset>
		    	  		<fieldset class="bottom">
		    	    		<label class="AStitle"><fmt:message key="credit"/></label>
		    	    		<input name="__creditAD_" type="text" id="creditAD_${customer.id}" onkeyup="creditADValue(this.value, ${customer.id});"/> 
		    	    		<div class="button">
								<input type="submit" value="Add"  name="__add_credit_" onclick="addCredit('${customer.id}');"/>
								<c:if test="${customer.credit > 0}">
									<input type="submit" value="Deduct"  name="__deduct_credit_" onclick="deductCredit('${customer.id}');"/>
								</c:if>
								<%-- Need to work on Credit History report.
								<div>
									<span style="float: right; padding-top: 10px;">
										<a href="../customers/virtualBankPayment.jhtm?userId=${customer.id}">view history</a>
									</span>
								</div> --%>
							  </div>
		    	  		</fieldset>
	    	  		</div>
	    	  	</c:if>
	    	  	</sec:authorize>
			    </td>
			  </tr>
			  </c:if>
			  <c:if test="${gSiteConfig['gPAYMENTS']}">			  
      	<sec:authorize ifAnyGranted="ROLE_AEM,ROLE_ADMIN_${prop_site_id},ROLE_INCOMING_PAYMENT_VIEW_LIST">
			  <tr>
			    <td><fmt:message key="incomingPayments"/></td>
			    <td>:&nbsp;&nbsp;</td>
			    <td><a href="../customers/payments.jhtm?cid=${customer.id}">view</a></td>
			  </tr>
			  </sec:authorize>
			  <c:if test="${gSiteConfig['gVIRTUAL_BANK_ACCOUNT']}">
			  <sec:authorize ifAnyGranted="ROLE_AEM,ROLE_ADMIN_${prop_site_id},ROLE_OUTGOING_PAYMENT_VIEW_LIST">
			  <tr>
			    <td><fmt:message key="outgoingPayments"/></td>
			    <td>:&nbsp;&nbsp;</td>
			    <td><a href="../customers/virtualBankPayment.jhtm?username=${customer.username}">view</a></td>
			  </tr>
			  </sec:authorize>
			  </c:if>
			  </c:if>
			  <%--
  		 	  <c:if test="${languageCodes != null}">
			  <tr>
			    <td><fmt:message key="language"/></td>
			    <td>:&nbsp;&nbsp;</td>
			    <td><fmt:message key="language_${order.languageCode}"/></td>
			  </tr>
			  </c:if>
			  --%> 
			</table>  
			</td>
			<td>&nbsp;</td>
			<td align="left">
			<b><fmt:message key="orderInformation" />:</b>
			<table cellpadding="0" cellspacing="0">
			  <c:if test="${order.host != null}">
			  <tr>
			    <td><fmt:message key="multiStore" /></td>
			    <td>:&nbsp;&nbsp;</td>
			    <td><c:out value="${order.host}"/></td>
			  </tr>
			  </c:if>
			  <tr>
			    <td><fmt:message key="order" /> #</td>
			    <td>:&nbsp;&nbsp;</td>
			    <td><b><c:out value="${order.orderId}"/></b></td>
			  </tr>
			  <c:if test="${order.externalOrderId != null}">
			  <tr>
			    <td><fmt:message key="externalOrderId" /></td>
			    <td>:&nbsp;&nbsp;</td>
			    <td><b><c:out value="${order.externalOrderId}"/></b></td>
			  </tr>
			  </c:if>
			<c:if test="${order.subscriptionCode != null}">
			  <tr>
			    <td><fmt:message key="subscription" /></td>
			    <td>:&nbsp;&nbsp;</td>
			    <td><a href="subscription.jhtm?code=${order.subscriptionCode}"><c:out value="${order.subscriptionCode}"/></a></td>
			  </tr>
			</c:if>
			  <tr>
			    <td><fmt:message key="orderDate" /></td>
			    <td>:&nbsp;&nbsp;</td>
			    <td><fmt:formatDate type="both" timeStyle="full" pattern="MM/dd/yyyy hh:mm a" value="${order.dateOrdered}"/></td>
			  </tr>
			<c:if test="${order.lastModified != null}">
			  <tr>
			    <td><fmt:message key="lastModified" /></td>
			    <td>:&nbsp;&nbsp;</td>
			    <td><fmt:formatDate type="both" timeStyle="full" pattern="MM/dd/yyyy hh:mm a" value="${order.lastModified}"/></td>
			  </tr>
			</c:if>
			<c:if test="${siteConfig['DELIVERY_TIME'].value == 'true'}" >
			  <tr>
			    <td><fmt:message key="dueDate" /></td>
			    <td>:&nbsp;&nbsp;</td>
			    <td style="color:#FF0000;font-weight:700;"><fmt:formatDate type="both" timeStyle="full" pattern="MM/dd/yyyy" value="${order.dueDate}"/></td>
			  </tr>
			</c:if>
			<c:if test="${siteConfig['USER_EXPECTED_DELIVERY_TIME'].value == 'true'}" >
			  <tr>
			    <td><fmt:message key="userExpectedDueDate" /></td>
			    <td>:&nbsp;&nbsp;</td>
			    <td style="color:#FF0000;font-weight:700;"><fmt:formatDate type="both" timeStyle="full" pattern="MM/dd/yyyy" value="${order.userDueDate}"/></td>
			  </tr>
			</c:if>
			<c:if test="${siteConfig['REQUESTED_CANCEL_DATE'].value == 'true'}" >
			  <tr>
			    <td><fmt:message key="requestedCancelDate" /></td>
			    <td>:&nbsp;&nbsp;</td>
			    <td style="color:#FF0000;font-weight:700;"><fmt:formatDate type="both" timeStyle="full" pattern="MM/dd/yyyy" value="${order.requestedCancelDate}"/></td>
			  </tr>
			</c:if>
			   <c:if test="${order.customField1 != null}">
              <tr>
			     <td><c:out value="${siteConfig['ORDER_CUSTOM_FIELD1_NAME'].value}"/></td>
			    <td>:&nbsp;&nbsp;</td>
			    <td><c:out value="${order.customField1}"/></td>
			  </tr>
              </c:if>
              <c:if test="${order.customField2 != null}">
                 <td><c:out value="${siteConfig['ORDER_CUSTOM_FIELD2_NAME'].value}" /></td>
			    <td>:&nbsp;&nbsp;</td>
			    <td><c:out value="${order.customField2}"/></td>
			  </tr>
              </c:if>
			<c:if test="${order.ipAddress != null}">
			  <tr>
			    <td><fmt:message key="ipAddress" /></td>
			    <td>:&nbsp;&nbsp;</td>
			    <td><c:out value="${order.ipAddress}"/></td>
			  </tr>
			</c:if>
			<c:if test="${gSiteConfig['gSALES_REP']}">
			  <tr>
			    <td><fmt:message key="salesRep"/></td>
			    <td>:&nbsp;&nbsp;</td>
			    <td><c:out value="${salesRep.name}"/></td>
			  </tr>
			  <tr>
			    <td><fmt:message key="processedBy"/></td>
			    <td>:&nbsp;&nbsp;</td>
			    <td><c:out value="${salesRepProcessedBy.name}"/></td>
			  </tr>
			</c:if>
			  <tr>
			    <td><fmt:message key="status"/></td>
			    <td>:&nbsp;&nbsp;</td>
			    <td><fmt:message key="${order.encodeStatus}"/>
			    <c:if test="${order.status == 'x'}">
			    [<c:out value="${orderStatus.cancelReason}"></c:out>]
			    </c:if>
			    </td>
			  </tr>
			  <c:if test="${gSiteConfig['gADD_INVOICE']}">
			  <tr>
			    <td><fmt:message key="from"/></td>
			    <td>:&nbsp;&nbsp;</td>
			    <td>
				<c:choose>
			    <c:when test="${order.orderType=='auction'}">
			    	Apps
			    </c:when>
			    <c:otherwise>
			    	<fmt:message key="${order.backEndOrderString}"/>
				</c:otherwise>
			    </c:choose>
			    </td>
			  </tr>
			  </c:if>
			  <c:if test="${gSiteConfig['gINVOICE_APPROVAL'] and !empty order.approval}">
			  <tr>
                <td><fmt:message key="approval"/></td>
                <td>:&nbsp;&nbsp;</td>
                <td><fmt:message key="approvalStatus_${order.approval}" /></td>
              </tr>  
              </c:if>
              <c:if test="${!empty order.orderType}">
              <tr>
			    <td><fmt:message key="orderType"/></td>
			    <td>:&nbsp;&nbsp;</td>
			    <td><c:out value="${order.orderType}"/></td>
			  </tr>
              </c:if>
              <c:if test="${!empty order.qualifier}">
              <tr>
			    <td><fmt:message key="qualifier"/></td>
			    <td>:&nbsp;&nbsp;</td>
			    <td><c:out value="${salesRepMap[order.qualifier].name}"/></td>
			  </tr>
              </c:if>
              <c:if test="${!empty order.trackcode}">
              <tr>
			    <td><fmt:message key="trackcode"/></td>
			    <td>:&nbsp;&nbsp;</td>
			    <td><c:out value="${order.trackcode}"/></td>
			  </tr>
              </c:if>
              <c:if test="${order.flag1 != null}">
              <tr>
			    <td><c:out value="${siteConfig['ORDER_FLAG1_NAME'].value}" /></td>
			    <td>:&nbsp;&nbsp;</td>
			    <td><fmt:message key="flag_${order.flag1}"/></td>
			  </tr>
              </c:if>
			  <c:if test="${zipFile}">
			  <tr>
			    <td><fmt:message key="zipFile"/></td>
			    <td>:&nbsp;&nbsp;</td>
			    <td><a href="<c:url value="/assets/orders_zip/${order.orderId}.zip"/>"><c:out value="${order.orderId}"/>.zip</a></td>
			  </tr>
			  </c:if>
			  <c:if test="${order.linkShare != null}">
			  <tr>
			    <td>LinkShare siteID</td>
			    <td>:&nbsp;&nbsp;</td>
			    <td><c:out value="${order.linkShare.siteID}"/></td>
			  </tr>
			  <tr>
			    <td>LinkShare Entry</td>
			    <td>:&nbsp;&nbsp;</td>
			    <td><fmt:formatDate type="both" timeStyle="full" pattern="MM/dd/yyyy hh:mm a" value="${order.linkShare.dateEntered}"/></td>
			  </tr>
			  </c:if>
			</table>
			</td>
			</tr>
			</table>
			<br/>
			<b><fmt:message key="emailAddress" />:</b>&nbsp;
			<a href="../customers/customerList.jhtm?email=${customer.username}"><c:out value="${customer.username}"/></a>&nbsp;
			
			

			<c:if test="${order.purchaseOrder != '' and order.purchaseOrder != null}" >
			<br/><b><fmt:message key="purchaseOrder" /></b>: <c:out value="${order.purchaseOrder}" />
			</c:if>

			<c:if test="${order.workOrderNum != null}">
			<br/><b><fmt:message key="workOrder" /> #</b>: <c:out value="${order.workOrderNum}" />
			</c:if>		
			
			<table border="0" cellpadding="2" cellspacing="1" width="100%" class="invoice">
			  <tr>
			    <c:set var="cols" value="0"/>
			    <c:if test="${gSiteConfig['gPURCHASE_ORDER'] and siteConfig['PREMIER_DISTRIBUTOR'].value != ''}">
			      <th width="2%" class="invoice"><fmt:message key="processed" /></th>
			      <c:set var="cols" value="${cols+1}"/>
			    </c:if> 
			    <th width="5%" class="invoice"><fmt:message key="line" />#</th>
			    <th class="invoice"><fmt:message key="productSku" /></th>
			    <th class="invoice"><fmt:message key="productName" /></th>
			    <c:forEach items="${productFieldsHeader}" var="productField">
			      <c:if test="${productField.showOnInvoice or productField.showOnInvoiceBackend}">
			      <th class="invoice"><c:out value="${productField.name}" /></th><c:set var="cols" value="${cols+1}"/>
			      </c:if>
			    </c:forEach>
			    <th width="10%" class="invoice"><fmt:message key="quantity" /></th>
			    <c:if test="${siteConfig['GENERATE_PACKING_LIST'].value == 'true'}"> 
			      <c:set var="cols" value="${cols+1}"/>
			      <th width="10%" class="invoice"><fmt:message key="packingList" /></th>
			    </c:if>
			    <c:if test="${gSiteConfig['gINVENTORY'] && order.hasLowInventoryMessage}">
			      <c:set var="cols" value="${cols+1}"/>
			      <th width="10%" class="invoice"><fmt:message key="lowInventory" /></th>
			    </c:if>
			    <c:if test="${order.hasPacking}">
				  <c:set var="cols" value="${cols+1}"/>
			      <th width="10%" class="invoice"><fmt:message key="packing" /></th>
			    </c:if>
			    <c:if test="${order.hasContent}">
				  <c:set var="cols" value="${cols+1}"/>
			      <th class="invoice"><fmt:message key="content" /></th>
			      <c:if test="${!empty siteConfig['EXT_QUANTITY_TITLE'].value}">
			      <c:set var="cols" value="${cols+1}"/>
			      <th class="invoice"><c:out value="${siteConfig['EXT_QUANTITY_TITLE'].value}" /></th>
			      </c:if>
			    </c:if>
			    <c:if test="${order.hasCustomShipping}">
	  			  <c:set var="cols" value="${cols+1}"/>
      			  <th class="invoice"><fmt:message key="customShipping" /></th>
    			</c:if>
    			<c:if test="${order.hasLineItemDiscount}">
	              <c:set var="cols" value="${cols+2}"/>
                  <th class="invoice"><fmt:message key="originalPrice" /></th>
                  <th class="invoice"><fmt:message key="discount" /></th>
                </c:if>
			    <th class="invoice"><fmt:message key="productPrice" /><c:if test="${order.hasContent}"> / <c:out value="${siteConfig['CASE_UNIT_TITLE'].value}" /></c:if></th>
			   	<c:if test="${gSiteConfig['gPURCHASE_ORDER'] && siteConfig['SHOW_COST'].value == 'true' && order.hasCost}">
			      <c:set var="cols" value="${cols+1}"/>
			      <th class="invoice"><fmt:message key="cost" /></th>
			    </c:if> 
				  	<th class="invoice">LineItem Promo</th>
				  	<c:set var="cols" value="${cols+1}"/>				  	
			    <th class="invoice"><fmt:message key="total" /></th>
			  </tr>
			<c:forEach var="lineItem" items="${order.lineItems}" varStatus="status">
			<tr valign="top" class="invoice0">
			  <c:if test="${gSiteConfig['gPURCHASE_ORDER'] and siteConfig['PREMIER_DISTRIBUTOR'].value != ''}">
			      <td class="invoice" align="center"><input type="checkbox" disabled="disabled" <c:if test="${lineItem.quantity <= lineItem.processed}">checked="checked"</c:if> /></td>
			  </c:if> 
			  <td class="invoice" align="center"><c:out value="${status.count}"/></td>
			  <td class="invoice">
				<c:if test="${lineItem.customImageUrl != null}">
			      <a href="customFrame.jhtm?orderId=${order.orderId}&lineNum=${lineItem.lineNumber}" onclick="window.open(this.href,'','width=650,height=800,resizable=yes'); return false;"><img class="invoiceImage" src="../../assets/customImages/${order.orderId}/${lineItem.customImageUrl}" border="0" /></a>
			    </c:if>  			  
			    <c:if test="${siteConfig['SHOW_IMAGE_ON_INVOICE'].value == 'true' and lineItem.product.thumbnail != null}">
		         <img class="invoiceImage" border="0" <c:if test="${!lineItem.product.thumbnail.absolute}"> src="<c:url value="/assets/Image/Product/thumb/${lineItem.product.thumbnail.imageUrl}"/>" </c:if><c:if test="${lineItem.product.thumbnail.absolute}"> src="<c:url value="${lineItem.product.thumbnail.imageUrl}"/>" </c:if> />
		        </c:if><c:out value="${lineItem.product.sku}"/>
		      </td>
			  <td class="invoice"><c:out value="${lineItem.product.name}"/><br/>
			  <c:if test="${lineItem.includeRetailDisplay}">
				Include Retail Display
			  </c:if>
			  <c:if test="${lineItem.customXml != null}"><a href="customFrame.jhtm?orderId=${order.orderId}&lineNum=${lineItem.lineNumber}" onclick="window.open(this.href,'','width=650,height=800,resizable=yes'); return false;">(Custom Frame)</a></c:if>
			   <table border="0" cellspacing="1" cellpadding="0">
				  <c:forEach items="${lineItem.productAttributes}" var="productAttribute" varStatus="productAttributeStatus">
					<tr class="invoice_lineitem_attributes${productAttributeStatus.count%2}">
					<td class="optionName" style="white-space: nowrap" align="right"><c:out value="${productAttribute.optionName}"/>: </td>
					<td class="optionValue" style="padding-left:5px;"><c:out value="${productAttribute.valueString}" escapeXml="false"/></td>
					<c:if test="${(! empty productAttribute.optionPriceOriginal) && productAttribute.optionPriceOriginal != 0}">
					<td class="optionPrice"><fmt:formatNumber value="${productAttribute.optionPriceOriginal}" pattern="#,##0.00" /></td>
					<td class="optionNote" style="padding-left:5px;"><fmt:message key="${productAttribute.optionPriceMessageF}"/></td>
					</c:if>
					</tr>
				  </c:forEach>
	  			  <%@ include file="/WEB-INF/jsp/frontend/common/customLinesInvoice.jsp"%>
			   </table>
				<c:if test="${lineItem.subscriptionInterval != null}">
				  <c:set var="intervalType" value="${fn:substring(lineItem.subscriptionInterval, 0, 1)}"/>
				  <c:set var="intervalUnit" value="${fn:substring(lineItem.subscriptionInterval, 1, 2)}"/>
				  <div class="invoice_lineitem_subscription">
					<fmt:message key="subscription"/><fmt:message key="details"/>:&nbsp;<fmt:message key="delivery"/>
					<c:choose>
					  <c:when test="${intervalUnit == '1'}"><fmt:message key="every_${intervalType}"><fmt:param value="${intervalUnit}"/></fmt:message></c:when>
					  <c:otherwise><fmt:message key="every_${intervalType}s"><fmt:param value="${intervalUnit}"/></fmt:message></c:otherwise>
					</c:choose>
				  </div>
				  <div class="invoice_lineitem_subscription"><a href="subscription.jhtm?code=${lineItem.subscriptionCode}" class="invoice_lineitem_subscription"><c:out value="${lineItem.subscriptionCode}"/></a></div>
				</c:if>
			  <c:if test="${lineItem.attachment != null}">
      			<a href="../../temp/Order/${order.orderId}/${lineItem.attachment}">
    				<c:out value="${lineItem.attachment}"/>
      			</a>
    		  </c:if>
			  <c:if test="${siteConfig['SHOW_IMAGE_ON_INVOICE'].value == 'true' and lineItem.hasProductAttributesImage}" >
			  <c:forEach items="${lineItem.productAttributes}" var="productAttribute">
			    <c:if test="${!empty productAttribute.imageUrl}" > 
			      <c:if test="${!productAttribute.absolute}"><img src="<c:url value="/assets/Image/Product/options/${productAttribute.imageUrl}"/>" border="0" class="invoiceOptionImage" /></c:if>
			      <c:if test="${productAttribute.absolute}"><img src="<c:url value="${productAttribute.imageUrl}"/>" border="0" class="invoiceOptionImage" /></c:if>
			    </c:if>
			  </c:forEach>
			  </c:if>
			  </td>
			  <c:forEach items="${productFieldsHeader}" var="pFHeader" varStatus="status"><c:set var="check" value="0"/>
			  <c:if test="${pFHeader.showOnInvoice or pFHeader.showOnInvoiceBackend}">
			  <c:forEach items="${lineItem.productFields}" var="productField"> 
			   <c:if test="${pFHeader.id == productField.id}">
			    <td class="invoice"><c:out value="${productField.value}" /></td><c:set var="check" value="1"/>
			   </c:if>       
			  </c:forEach>
			  <c:if test="${check == 0}">
			    <td class="invoice">&nbsp;</td>
			   </c:if>
			  </c:if>
			  </c:forEach>
			  <td class="invoice" align="center" <c:choose>
			  <c:when test="${lineItem.processed == lineItem.quantity }">style="background-color:#bbff99;"</c:when>
			  <c:when test="${(lineItem.processed > lineItem.quantity) or (lineItem.processed < lineItem.quantity) }">style="background-color:#FFAFAF;"</c:when>
			  </c:choose> >
			  <c:choose>
			    <c:when test="${lineItem.priceCasePackQty != null}"><c:out value="${lineItem.quantity}"/><div class="casePackPriceQty">(<c:out value="${lineItem.priceCasePackQty}"/> per <c:out value="${siteConfig['PRICE_CASE_PACK_UNIT_TITLE'].value}" /> )</div></c:when>
			    <c:otherwise><c:out value="${lineItem.quantity}"/></c:otherwise>
			  </c:choose>
			  </td>
			  <c:if test="${siteConfig['GENERATE_PACKING_LIST'].value == 'true'}"> 
			    <td class="invoice" align="center" <c:choose>
			    <c:when test="${lineItem.processed == lineItem.quantity}">style="background-color:#bbff99;"</c:when>
			    <c:when test="${(lineItem.processed > lineItem.quantity) or (lineItem.processed < lineItem.quantity) }">style="background-color:#FFAFAF;"</c:when>
			    </c:choose> ><c:out value="${lineItem.processed}"/></td>
			  </c:if>
			  <c:if test="${gSiteConfig['gINVENTORY'] && order.hasLowInventoryMessage}">
			   <td class="invoice" align="center"><c:out value="${lineItem.lowInventoryMessage}" /></td>
			  </c:if>
			  <c:if test="${order.hasPacking }">
			  <td class="invoice" align="center"><c:out value="${lineItem.product.packing}" /></td>
			  </c:if>
			  <c:if test="${order.hasContent}">
			    <td class="invoice" align="center"><c:out value="${lineItem.product.caseContent}" /></td>
			    <c:if test="${!empty siteConfig['EXT_QUANTITY_TITLE'].value}">
			    <td class="invoice" align="center">
			     <c:choose>
			      <c:when test="${!empty lineItem.product.caseContent}"><c:out value="${lineItem.product.caseContent * lineItem.quantity}" /></c:when>
			      <c:otherwise><c:out value="${lineItem.quantity}" /></c:otherwise> 
			     </c:choose>
			    </td>
			    </c:if>
			  </c:if>
			  <c:if test="${order.hasCustomShipping}">
			    <td class="invoice" align="center">
			    <c:if test="${!empty lineItem.customShippingCost}">
			     <c:out value="${order.customShippingTitle}" />
			    </c:if>
			    </td>
			  </c:if>
			  <c:if test="${order.hasLineItemDiscount}">
                <td class="invoice" align="right"><fmt:message key="${siteConfig['CURRENCY'].value}" /><fmt:formatNumber value="${lineItem.originalPrice}" pattern="#,##0.00"/></td> 
                <td class="invoice" align="right">
                <c:if test="${lineItem.discount != null}" >
                  <c:choose>
                  <c:when test="${lineItem.percent}"><c:out value="${lineItem.discount}"/>%</c:when>
                  <c:otherwise><fmt:message key="${siteConfig['CURRENCY'].value}" /><fmt:formatNumber value="${lineItem.discount}" pattern="#,##0.00"/></c:otherwise> 
                  </c:choose>
                </c:if> 
                </td> 
              </c:if>
			  <td class="invoice" align="right"><fmt:message key="${siteConfig['CURRENCY'].value}" /><fmt:formatNumber value="${lineItem.unitPrice}" pattern="#,##0.00#" maxFractionDigits="${siteConfig['MAX_FRACTION_DIGIT'].value}" /></td> 
			  <c:if test="${gSiteConfig['gPURCHASE_ORDER'] && siteConfig['SHOW_COST'].value == 'true' && order.hasCost}">
			  <td class="invoice" align="right"><fmt:message key="${siteConfig['CURRENCY'].value}" /><fmt:formatNumber value="${lineItem.unitCost}" pattern="#,##0.00"/></td> 
			  </c:if>
			  
			  		<td class="invoice" align="right">			  			
				  		<c:if test = "${ lineItem.promo.title != null}">			  			
				  			  Promo: ${lineItem.promo.title}
				  			  <br>
				  			  Amount: <span style="color:red;"><fmt:formatNumber value="${lineItem.promoAmount}" pattern="#,##0.00"/>	<span>	
			  			 </c:if>  			  
			  		</td>
			  		
			  <td class="invoice" align="right"><fmt:message key="${siteConfig['CURRENCY'].value}" /><fmt:formatNumber value="${lineItem.totalPrice}" pattern="#,##0.00"/></td>
			</tr> 
			<c:if test="${lineItem.serialNums != null}">
			<tr bgcolor="#FFFFFF">
			  <td colspan="${6+cols}">
			    <table cellspacing="3">
			      <tr>
			        <td valign="top">S/N:</td>
			        <td><c:forEach items="${lineItem.serialNums}" var="serialNum" varStatus="status"><c:if test="${not status.first}">, </c:if><c:out value="${serialNum}"/></c:forEach></td>
			      </tr>
			    </table>
			  </td>
			</tr>			
			</c:if>	
			<c:if test="${not empty lineItem.trackNum or lineItem.dateShipped != null or not empty lineItem.shippingMethod or not empty lineItem.status}">
			<tr bgcolor="#FFFFFF">
			  <td colspan="${6+cols}">
			    <c:if test="${not empty lineItem.status}"><fmt:message key="status"/>: <c:out value="${lineItem.status}"/></c:if>
			    <c:if test="${not empty lineItem.shippingMethod}"><c:out value="${lineItem.shippingMethod}"/></c:if>
			    <c:if test="${lineItem.dateShipped != null}"><fmt:message key="dateShipped"/>: <fmt:formatDate type="date" value="${lineItem.dateShipped}" pattern="MM/dd/yyyy"/></c:if>
			    <c:if test="${not empty lineItem.trackNum}"><fmt:message key="trackNum"/>: <c:out value="${lineItem.trackNum}"/></c:if>
			  </td>
			</tr>			
			</c:if>	
			</c:forEach>
			  <tr bgcolor="#BBBBBB">
			    <td colspan="${6+cols}">&nbsp;</td>
			  </tr>
			  <tr>
			    <td class="invoice" colspan="${5+cols}" align="right"><fmt:message key="subTotal" />:</td>
			    <td class="invoice" align="right"><fmt:message key="${siteConfig['CURRENCY'].value}" /><fmt:formatNumber value="${order.subTotal}" pattern="#,##0.00"/></td>
			  </tr>
		  	  <c:if test="${order.budgetEarnedCredits > 0.00 }">
			  	<tr class="customerEarnedCreditsBoxId">
			  		<td class="discount" colspan="${5+cols}" align="right">Earned Credits: </td>
			  		<td class="discount" align="right"><fmt:formatNumber value="${order.budgetEarnedCredits}" pattern="-#,##0.00"/></td>
			  	</tr>
		      </c:if>
			  <c:if test="${order.promo.title != null and order.promo.discountType eq 'order'}" >
				  <tr>
				    <td class="discount" colspan="${5+cols}" align="right">
      				<c:choose>
        			  <c:when test="${order.promo.discount == 0}"><fmt:message key="promoCode" /></c:when>
        			  <c:otherwise><fmt:message key="discountForPromoCode" /></c:otherwise>
      				</c:choose>
      				<c:out value="${order.promo.title}" />
				    <c:choose>
				      <c:when test="${order.promo.discount == 0}"></c:when>
			          <c:when test="${order.promo.percent}">
			            (<fmt:formatNumber value="${order.promo.discount}" pattern="#,##0.00"/>%)
			          </c:when>
			          <c:otherwise>
			            (<fmt:message key="${siteConfig['CURRENCY'].value}" /><fmt:formatNumber value="${order.promo.discount}" pattern="#,##0.00"/>)
			          </c:otherwise>
			      	</c:choose>	    
				    </td>
				    <td class="discount" align="right">
				    <c:choose>
				      <c:when test="${order.promo.discount == 0}">&nbsp;</c:when>
			          <c:when test="${order.promo.percent}">
			            (<fmt:formatNumber value="${order.subTotal * ( order.promo.discount / 100.00 )}" pattern="#,##0.00"/>)
			          </c:when>
			          <c:when test="${(!order.promo.percent) and (order.promo.discount > order.subTotal)}">
			        	(<fmt:message key="${siteConfig['CURRENCY'].value}" /><fmt:formatNumber value="${order.subTotal}" pattern="#,##0.00"/>)
			          </c:when>
			          <c:otherwise>
			            (<fmt:message key="${siteConfig['CURRENCY'].value}" /><fmt:formatNumber value="${order.promo.discount}" pattern="#,##0.00"/>)
			          </c:otherwise>
			      	</c:choose>	    
				    </td>
				  </tr>
			  </c:if>
			  
			  <!-- viaTrading want promo code for produts applied on lineItem -->
<%-- 			  <c:if test="${order.lineItemPromos != null and fn:length(order.lineItemPromos) gt 0}">
    			<c:forEach items="${order.lineItemPromos}" var="itemPromo" varStatus="status">
    			<tr>
      			<td class="discount" colspan="${5+cols}" align="right">
      			<c:choose>
        			<c:when test="${itemPromo.value == 0}"><fmt:message key="promoCode" /></c:when>
        			<c:otherwise><fmt:message key="discountForPromoCode" /></c:otherwise>
      			</c:choose>
      			<c:out value="${itemPromo.key}" />
      			</td>
      			<td class="discount" align="right">
      			<c:choose>
        			<c:when test="${itemPromo.value == 0}">&nbsp;</c:when>
       			 <c:otherwise>(<fmt:message key="${siteConfig['CURRENCY'].value}" /><fmt:formatNumber value="${itemPromo.value}" pattern="#,##0.00"/>)</c:otherwise>
      			</c:choose>
      			</td>
    			</tr>
    			</c:forEach>  
  			  </c:if> --%>
  			  
			  <c:choose>
   			    <c:when test="${order.taxOnShipping}">
   			      <tr>
			    	<td class="invoice" colspan="${5+cols}" align="right"><fmt:message key="shippingHandling" /> (<c:out value="${order.shippingMethod}" escapeXml="false"/>):</td>
			    	<input type="hidden" id="shippingMethod" value="${order.shippingMethod}"/>
			    	<td class="invoice" align="right">
			      	<c:if test="${order.shippingCost != null}">
			          <fmt:message key="${siteConfig['CURRENCY'].value}" /><fmt:formatNumber value="${order.shippingCost}" pattern="#,##0.00"/>
			        </c:if>
			        </td>
			      </tr>
			      <tr>
			    	<td class="invoice" colspan="${5+cols}" align="right"><fmt:message key="tax" />:</td>
			    	<td class="invoice" align="right"><fmt:message key="${siteConfig['CURRENCY'].value}" /><fmt:formatNumber value="${order.tax}" pattern="#,##0.00"/></td>
			  	  </tr>
			  	</c:when>
    			<c:otherwise>
    			  <tr>
			    	<td class="invoice" colspan="${5+cols}" align="right"><fmt:message key="tax" />:</td>
			    	<td class="invoice" align="right"><fmt:message key="${siteConfig['CURRENCY'].value}" /><fmt:formatNumber value="${order.tax}" pattern="#,##0.00"/></td>
			  	  </tr>
			  	  <tr>
			    	<td class="invoice" colspan="${5+cols}" align="right"><fmt:message key="shippingHandling" /> (<c:out value="${order.shippingMethod}" escapeXml="false"/>):</td>
			    	<input type="hidden" id="shippingMethod" value="${order.shippingMethod}"/>
			    	<td class="invoice" align="right">
			      	<c:if test="${order.shippingCost != null}">
			          <fmt:message key="${siteConfig['CURRENCY'].value}" /><fmt:formatNumber value="${order.shippingCost}" pattern="#,##0.00"/>
			        </c:if>
			        </td>
			      </tr>
    			</c:otherwise>
    		  </c:choose>
  			  <c:if test="${order.promo.title != null and order.promo.discountType eq 'shipping' and order.shippingCost != null}" >
				  <tr>
				    <td class="discount" colspan="${5+cols}" align="right">
      				<c:choose>
        			  <c:when test="${order.promo.discount == 0}"><fmt:message key="promoCode" /></c:when>
        			  <c:otherwise><fmt:message key="discountForPromoCode" /></c:otherwise>
      				</c:choose>
      				<c:out value="${order.promo.title}" />
				    <c:choose>
				      <c:when test="${order.promo.discount == 0}"></c:when>
			          <c:when test="${order.promo.percent}">
			            (<fmt:formatNumber value="${order.promo.discount}" pattern="#,##0.00"/>%)
			          </c:when>
			          <c:otherwise>
			            (<fmt:message key="${siteConfig['CURRENCY'].value}" /><fmt:formatNumber value="${order.promo.discount}" pattern="#,##0.00"/>)
			          </c:otherwise>
			      	</c:choose>	    
				    </td>
				    <td class="discount" align="right">
				    <c:choose>
				      <c:when test="${order.promo.discount == 0}">&nbsp;</c:when>
			          <c:when test="${order.promo.percent}">
			            (<fmt:formatNumber value="${order.shippingCost * ( order.promo.discount / 100.00 )}" pattern="#,##0.00"/>)
			          </c:when>
			       	  <c:when test="${(!order.promo.percent) and (order.promo.discount > order.shippingCost)}">
			        	(<fmt:message key="${siteConfig['CURRENCY'].value}" /><fmt:formatNumber value="${order.shippingCost}" pattern="#,##0.00"/>)
			          </c:when>
			          <c:otherwise>
			            (<fmt:message key="${siteConfig['CURRENCY'].value}" /><fmt:formatNumber value="${order.promo.discount}" pattern="#,##0.00"/>)
			          </c:otherwise>
			      	</c:choose>	    
				    </td>
				  </tr>
			  </c:if>
			  <c:if test="${order.customShippingCost != null}">
			  <tr>
			    <td class="invoice" colspan="${5+cols}" align="right"><fmt:message key="customShipping" /> (<c:out value="${order.customShippingTitle}" escapeXml="false"/>):</td>
			    <td class="invoice" align="right"><fmt:message key="${siteConfig['CURRENCY'].value}" /><fmt:formatNumber value="${order.customShippingCost}" pattern="#,##0.00"/></td>
			  </tr>
			  </c:if>
			  <c:if test="${order.ccFee != null && order.ccFee != 0}">
			  <tr>
			    <td class="invoice" colspan="${5+cols}" align="right"><fmt:message key="creditCardFee" />:</td>
			    <td class="invoice" align="right"><fmt:message key="${siteConfig['CURRENCY'].value}" /><fmt:formatNumber value="${order.ccFee}" pattern="#,##0.00"/></td>
			  </tr>
			  </c:if>	
			  <c:choose>
			  <c:when test="${gSiteConfig['gBUDGET'] and order.requestForCredit != null and order.creditUsed == null and customer.creditAllowed != null and customer.creditAllowed > 0}">
		    	<tr>
			    	<td class="discount" colspan="${5+cols}" align="right">
			    	<fmt:message key="creditRequestAmount">
						<fmt:param>${customer.creditAllowed}</fmt:param>
						<fmt:param><fmt:formatNumber value="${subTotalByPlan}" pattern="#,##0.00"/></fmt:param>
						<fmt:param><fmt:formatNumber value="${(customer.creditAllowed / 100) * subTotalByPlan}" pattern="#,##0.00"/></fmt:param>
				    </fmt:message> 	
			    	<br/> 
			    	<fmt:message key="creditApprovalMessage" />
			    	</td>
			    	<c:choose>
		  				<c:when test="${order.requestForCredit > 0 }">
			    			<td class="discount" align="right"><fmt:message key="${siteConfig['CURRENCY'].value}" /><fmt:formatNumber value="${order.requestForCredit}" pattern="-#,##0.00"/></td>
		  				</c:when>
		  				<c:otherwise>
		  					<td class="discount" align="right">0.00</td>
		  				</c:otherwise>
		  			</c:choose>	 
			    </tr>
			  </c:when>
			  <c:when test="${(gSiteConfig['gGIFTCARD'] or gSiteConfig['gVIRTUAL_BANK_ACCOUNT'] or gSiteConfig['gBUDGET']) and order.creditUsed != null and order.creditUsed > 0}">
				  <tr>
				    <td class="discount" colspan="${5+cols}" align="right"><fmt:message key="credit" />:</td>
				    <td class="discount" align="right"><fmt:message key="${siteConfig['CURRENCY'].value}" /><fmt:formatNumber value="${order.creditUsed}" pattern="-#,##0.00"/></td>
				  </tr>
			  </c:when>
			  </c:choose>		  
			  <c:if test="${order.bondCost != null}">
			  <tr>
			    <td class="invoice" align="right" colspan="2">
			    <c:if test="${!fn:contains(order.status,'x')}">
			      <a style="width: 100px; float: right; text-align: center; background-color:#FF9999; color:WHITE;" href="invoice.jhtm?order=${order.orderId}&__cancelBuySafe=true"><fmt:message key="cancel" /></a>
			    </c:if>
			    </td>
			    <td class="invoice" colspan="${3+cols}" align="right"><fmt:message key="buySafeBondGuarntee" />:</td>
			    <td class="invoice" align="right"><fmt:message key="${siteConfig['CURRENCY'].value}" /><fmt:formatNumber value="${order.bondCost}" pattern="#,##0.00"/></td>
			  </tr>
			  </c:if>			  
			  <tr>
			    <td class="invoice" colspan="${5+cols}" align="right"><fmt:message key="grandTotal" />:</td>
			    <td align="right" bgcolor="#F8FF27"><fmt:message key="${siteConfig['CURRENCY'].value}" /><fmt:formatNumber value="${order.grandTotal}" pattern="#,##0.00"/></td>
			  </tr>
			  
      		  <c:if test="${gSiteConfig['gPAYMENTS']}">
			  <tr>
			    <td class="invoice" colspan="${5+cols}" align="right"><fmt:message key="balance" />:</td>
			    <td class="invoice" align="right"><fmt:message key="${siteConfig['CURRENCY'].value}" /><fmt:formatNumber value="${order.grandTotal - order.amountPaid}" pattern="#,##0.00"/></td>
			  </tr>
			  </c:if>
			</table>
			
			<c:if test="${partnerList != null and fn:length(partnerList) gt 0}">
			<div class="partnersBox">
					<b><fmt:message key="partnersList"/></b>
			    	<c:forEach items="${partnerList}" var="partner">
			    		<div>
			    		<c:out value="${partner.partnerName}"/>
			    		<fmt:message key="${siteConfig['CURRENCY'].value}" /><fmt:formatNumber value="${partner.amount}" pattern="#,##0.00"/>
			    		</div>
			    	</c:forEach>
			    	<div> <fmt:message key="total"/>: <fmt:message key="${siteConfig['CURRENCY'].value}" /><fmt:formatNumber value="${order.budgetEarnedCredits}" pattern="#,##0.00"/></div>
			</div>
			</c:if>
						
			<div>
			<c:out value="${siteConfig['INVOICE_MESSAGE'].value}" escapeXml="false"/>
			</div>			
			
			<table align="left">
			  <tr>
			    <td>
			    <b><fmt:message key="paymentMethod" />:</b>&nbsp;<c:out value="${order.paymentMethod}" />
			    <c:if test="${order.creditCard != null and fn:toLowerCase(order.paymentMethod) != 'paypalaccount'}">
				<table>
				 <tr>	
				  <c:choose>
				  <c:when test="${order.creditCard != null and (order.creditCard.transId != null or order.status == 's' or fn:contains(order.status,'x'))}">
 					  <c:set value="false" var="showCreditCard"/>
 				  	  <c:set value="${order.creditCard}" var="creditCard"/>
 				  	  <%@ include file="/WEB-INF/jsp/admin/orders/common/creditcard.jsp" %>
  				  </c:when>
  				  <c:otherwise>
 				  	  <c:set value="true" var="showCreditCard"/>
 				  	  <c:set value="${order.creditCard}" var="creditCard"/>
 				  	  <%@ include file="/WEB-INF/jsp/admin/orders/common/creditcard.jsp" %>
  				  </c:otherwise>
  				  </c:choose>
  				 </tr>
				  <c:if test="${order.creditCard.transId != null}">
				  <tr>
				    <td align="right" style="white-space: nowrap" valign="top">Transaction ID:</td>
				    <td><c:out value="${order.creditCard.transId}" /></td>
				  </tr>
				  </c:if>
				  <c:if test="${order.creditCard.authCode != null}">
				  <tr>
				    <td align="right" style="white-space: nowrap" valign="top">Authorization Code:</td>
				    <td><c:out value="${order.creditCard.authCode}" /></td>
				  </tr>
				  </c:if>
				  <c:if test="${order.creditCard.avsCode != null}">
				  <tr>
				    <td align="right" style="white-space: nowrap" valign="top">AVS Result:</td>
				    <td><c:out value="${order.creditCard.avsCode}" /></td>
				  </tr>
				  </c:if>
				  <c:if test="${order.creditCard.cvv2Code != null}">
				  <tr>
				    <td align="right" style="white-space: nowrap" valign="top">CVV2 Result:</td>
				    <td><c:out value="${order.creditCard.cvv2Code}" /></td>
				  </tr>
				  </c:if>
				  <c:if test="${order.creditCard.paymentStatus != null}">
				  <tr>
				    <td align="right" style="white-space: nowrap" valign="top">Payment Status:</td>
				    <td><c:out value="${order.creditCard.paymentStatus}" /></td>
				  </tr>
				  </c:if>
				  <c:if test="${order.creditCard.paymentNote != null}">
				  <tr>
				    <td align="right" style="white-space: nowrap" valign="top">Payment Note:</td>
				    <td><c:out value="${order.creditCard.paymentNote}" /></td>
				  </tr>
				  </c:if>
				  <c:if test="${order.creditCard.paymentDate != null}">
				  <tr>
				    <td align="right" style="white-space: nowrap" valign="top">Payment Date:</td>
				    <td><fmt:formatDate type="both" timeStyle="full" value="${order.creditCard.paymentDate}"/></td>
				  </tr>
				  </c:if> 
				</table>
				<c:if test="${order.status != 'x' and (gSiteConfig['gCREDIT_CARD_PAYMENT'] == 'authorizenet' or gSiteConfig['gCREDIT_CARD_PAYMENT'] == 'yourpay'
						or gSiteConfig['gCREDIT_CARD_PAYMENT'] == 'payrover'
						or gSiteConfig['gCREDIT_CARD_PAYMENT'] == 'paymentech'
						or gSiteConfig['gCREDIT_CARD_PAYMENT'] == 'ezic'  
						or gSiteConfig['gCREDIT_CARD_PAYMENT'] == 'ebizcharge'  
						or gSiteConfig['gCREDIT_CARD_PAYMENT'] == 'paypalpro')}">
			    <c:if test="${order.creditCard.paymentStatus != 'CAPTURED' && order.creditCard.paymentStatus != 'Sale' }">
			    <form action="invoice.jhtm" method="post">
			    <input type="hidden" name="order" value="<c:out value="${order.orderId}"/>" >
			    <c:if test="${order.creditCard.paymentStatus != 'AUTHORIZED' && order.creditCard.paymentStatus != 'Authorization'}">
			    <input type="submit" name="__auth_only" value="<fmt:message key="authorizeCard"/>">
			    </c:if>
			    <c:choose>
			    <c:when test="${gSiteConfig['gCREDIT_CARD_PAYMENT'] == 'authorizenet' and order.creditCard.paymentStatus == 'AUTHORIZED'}">
			    <input type="submit" name="__charge" value="Capture Current Authorization">
			    <input type="submit" name="__auth_only" value="New Authorization">
			    <input type="submit" name="__charge_new" value="New Charge">
			    </c:when>
			    <c:otherwise>
			    <input type="submit" name="__charge" value="<fmt:message key="chargeCard"/>">
			    </c:otherwise>
			    </c:choose>
			    </form>
			    </c:if>
				<c:if test="${chargeMessage != null}">
			  		<div class="message"><fmt:message key="${chargeMessage}" /></div>
				</c:if>    
				<br/>
			    </c:if>
			
			    </c:if>
			    <c:if test="${order.paypal.txn_id != null}">
			    <table>
			     <tr>
			      <td align="right">Transaction Id:</td>
			      <td><a href="<c:out value="${siteConfig['PAYPAL_URL'].value}"/>vst/id=${order.paypal.txn_id}" target="_new"><c:out value="${order.paypal.txn_id}"/></a></td>
			     </tr>
			     <tr>
			      <td align="right">Payment Status:</td>
			      <td><c:out value="${order.paypal.payment_status}"/></td>
			     </tr>
			     <tr>
			      <td align="right">Payment Date:</td>
			      <td><c:out value="${order.paypal.payment_date}"/></td>
			     </tr>
			    </table>
			    </c:if>
			    <c:if test="${order.netCommerce.txtNumAut != null}">
			    <table>
			     <tr>
			      <td align="right">Payment Status:</td>
			      <td>Transaction is <c:choose><c:when test="${order.netCommerce.respVal == '1'}">Authorized</c:when><c:otherwise>Refused</c:otherwise></c:choose></td>
			     </tr>
			     <tr>
			      <td align="right">Authorization Number:</td>
			      <td><c:out value="${order.netCommerce.txtNumAut}"/></td>
			     </tr>
			     <tr>
			      <td align="right">Description:</td>
			      <td><c:out value="${order.netCommerce.respMsg}"/></td>
			     </tr>
			     <tr>
			      <td align="right">Payment Date:</td>
			      <td><fmt:formatDate type="both" timeStyle="full" value="${order.netCommerce.paymentDate}"/></td>
			     </tr>
			    </table>
			    </c:if>
			    <c:if test="${order.bankAudi != null}">
				  <%@ include file="/WEB-INF/jsp/frontend/common/bankAudi.jsp" %>
			    </c:if>
			    <c:if test="${order.geMoney != null}">
			    <table>
			     <tr>
			      <td align="right"><fmt:message key="accountNumber"/>:</td>
			      <td><c:out value="${order.geMoney.acctNumber}"/></td>
			     </tr>
			     <c:if test="${order.geMoney.transDate != null}">
			     <tr>
			      <td align="right">Transaction Date:</td>
			      <td><c:out value="${order.geMoney.transDate}"/></td>
			     </tr>
			     </c:if>
			     <c:if test="${order.geMoney.status != null}">
			     <tr>
			      <td align="right"><fmt:message key="status"/>:</td>
			      <td><c:out value="${order.geMoney.status}"/></td>
			     </tr>
			     </c:if>
			     <c:if test="${order.geMoney.authCode != null}">
			     <tr>
			      <td align="right">Auth Code:</td>
			      <td><c:out value="${order.geMoney.authCode}"/></td>
			     </tr>
			     </c:if>
			    </table>			    
			    </c:if>
			    <c:if test="${order.eBillme.orderrefid == null
			    				and siteConfig['EBILLME_URL'].value != ''
								and fn:trim(siteConfig['EBILLME_MERCHANTTOKEN'].value) != ''}">
				<br/><br/>
			    <form action="invoice.jhtm" method="post">
			    <input type="hidden" name="order" value="<c:out value="${order.orderId}"/>" >
			    <input type="submit" name="__eBillme" value="eBillme" onClick="return confirm('Send eBillme?')">
			    <c:if test="${eBillmeError != null}"><span class="message"><c:out value="${eBillmeError}"/></span></c:if>
			    </form>
				</c:if>			    
			    <c:if test="${order.eBillme.orderrefid != null}">
			      <div></div>
			      <b>eBillme Payment</b>
			      <%@ include file="/WEB-INF/jsp/frontend/common/eBillme.jsp" %>
			    </c:if>
			    <c:if test="${order.amazonIopn.notificationRefId != null}">
			      <%@ include file="/WEB-INF/jsp/frontend/common/amazonIopn.jsp" %>
			    </c:if>
			    </td>
			  </tr>
			  <c:if test="${fn:toLowerCase(order.paymentMethod) == 'credit card' and siteConfig['CC_BILLING_ADDRESS'].value == 'true'}">
			  <tr>
				  <td><b><fmt:message key="ccBillingAddres"/>:</b>
				  <p><c:out value="${order.ccBillingAddress}"/></p>
				  </td>
			  </tr>
			  </c:if>
			</table>
			<c:if test="${gSiteConfig['gPAYMENTS']}">
			<c:set var="totalPayments" value="0"/>
				<c:forEach var="payment" items="${order.paymentHistory}" varStatus="paymentStatus">
				<c:if test="${paymentStatus.first}">
				<table>
				  <tr>
				    <td colspan="5"><b><fmt:message key="paymentHistory"/></b>:</td>
				  </tr>
				</c:if>
				  <tr>
				    <td><fmt:formatDate type="date" timeStyle="default" value="${payment.date}"/></td>
				    <td>&nbsp;</td>
					<c:choose>					
					<c:when test="${payment.cancelPayment == 'true'}">						
					<td align="right" class="cancel">	
						<fmt:message key="cancelled"/>&nbsp;<fmt:message key="amount"/>:&nbsp;<fmt:message key="${siteConfig['CURRENCY'].value}" /><fmt:formatNumber value="${payment.cancelledAmt}" pattern="#,##0.00" />
					</td>
					</c:when>
					<c:otherwise>						
					<td align="right">
						<fmt:message key="${siteConfig['CURRENCY'].value}" /><fmt:formatNumber value="${payment.amount}" pattern="#,##0.00" />	
					</td>					
					</c:otherwise>
					</c:choose>
					<c:set var="totalPayments" value="${totalPayments + payment.amount}"/>
				    <td>&nbsp;</td>
					<td>
						<a class="nameLink" href="../customers/cancelPayments.jhtm?orderId=${order.orderId}&cid=${order.userId}">
							<c:if test="${payment.paymentMethod != ''}"><c:out value="${payment.paymentMethod}"/></c:if>&nbsp;Note:<c:out value="${payment.memo}"/>
						</a>
					</td>
				  </tr>
				<c:if test="${paymentStatus.last}">
				  <tr>
				    <td class="status"><b><fmt:message key="total" />:</b></td>
				    <td>&nbsp;</td>
					<td align="right" class="status"><b><fmt:message key="${siteConfig['CURRENCY'].value}" /><fmt:formatNumber value="${totalPayments}" pattern="#,##0.00" /></b></td>
				    <td colspan="2">&nbsp;</td>
				  </tr>
				</table>
				</c:if>
				</c:forEach>
			</c:if>
			<!-- Start -->
			<table style="clear:both;margin-top:10px;" width="100%">
				<tr>
				  <td>
					<b><a target="blank" href="https://www.viatrading.com/dv/orderreportnewdb/shippingquotes.php?order_id=${order.orderId}">SQG</a></b>
				  </td>		           
				</tr>
				 <tr>
				 </tr>
			</table>
			<!-- End -->			
			<table style="clear:both;margin-top:10px;" width="100%">
				<tr>
				   <td>
					  <table>
							<tr><td><b><fmt:message key="customerFields"/>:</b></td></tr>
							
							<c:forEach items="${customerFields}" var="customerField" varStatus="status">
								<tr><td><c:out value="${customerField.name}" />: <c:out value="${customerField.value}" /></td></tr>
							</c:forEach>
		               </table>
					</td>
		            <td>
					   <table>
					      <tr><td>
		                      <b><fmt:message key="agreedPaymentDate" />:</b> <fmt:formatDate type="date" value="${order.agreedPaymentDate }"/>
					     </td></tr>
					       <tr><td>
					        <b><fmt:message key="agreedPickUpDate" />:</b> <fmt:formatDate type="date"  value ="${order.agreedPickUpDate}" />
					      </td></tr>
	                   </table>
					</td>
				 </tr>
				 <tr>
				 </tr>
			</table>

			<c:if test="${order.invoiceNote != null and order.invoiceNote != ''}">
			<table width="100%">
				<tr >
				  <td valign="top">
				    <b><c:out value="${siteConfig['SPECIAL_INSTRUCTIONS'].value}"/>:</b>  	
				  </td>
				</tr>
				<tr>
				  <td>
				  <c:out value="${order.invoiceNote}" escapeXml="false"/>
				  </td>
				</tr>
			</table>
			</c:if>
			
			<hr>
			<br>

			<c:if test="${(siteConfig['EDIT_COMMISSION'].value == 'true') && (gSiteConfig['gAFFILIATE'] > 0) && (order.affiliate.totalCommissionLevel1 != null || order.affiliate.totalCommissionLevel2 != null)}"> 
			<table border="0" cellpadding="2" cellspacing="1" width="30%" >
			<tr>
			  <td><b><fmt:message key="commissionLevel"/></b></td>
			  <td><b><fmt:message key="amount"/></b></td>
			</tr>
			<c:if test="${order.affiliate.totalCommissionLevel1 != null}">
			<tr>
			  <td><fmt:message key="commissionLevel1"/></td>
			  <td><c:out value="${order.affiliate.totalCommissionLevel1}"/></td>
			</tr>
			</c:if>
			<c:if test="${order.affiliate.totalCommissionLevel2 != null}">
			<tr>
			  <td><fmt:message key="commissionLevel2"/></td>
			  <td><c:out value="${order.affiliate.totalCommissionLevel2}"/></td>
			</tr>
			</c:if>
			</table>
			<hr>
			<br>
			</c:if>
			
			<table border="0" cellpadding="2" cellspacing="1" width="100%" class="statusHistory">
			<tr>
			  <th class="statusHistory">Date Changed</th>
			  <th class="statusHistory"><fmt:message key="status"/></th>
			  <c:if test="${!empty siteConfig['SUB_STATUS'].value}">
			    <th class="statusHistory"><fmt:message key="subStatus"/></th>
			  </c:if>
			  <th class="statusHistory">Customer Notified</th>
			  <th class="statusHistory"><fmt:message key="trackNum"/></th>
			  <th class="statusHistory"><fmt:message key="carrier"/></th>
			  <th width="100%" class="statusHistory"><fmt:message key="comments"/></th>
			  <th width="100%" class="statusHistory"><fmt:message key="user"/></th>
			</tr>
			<c:forEach var="orderStatus" items="${order.statusHistory}">
			<c:if test="${orderStatus.status != 'xap' or order.status == 'xap'}">
			<tr valign="top">
			  <td class="statusHistory" style="white-space: nowrap"><fmt:formatDate type="both" timeStyle="full" value="${orderStatus.dateChanged}"/></td>
			  <td class="status" align="center" style="white-space: nowrap"><fmt:message key="orderStatus_${orderStatus.status}"/></td>
			  <c:if test="${!empty siteConfig['SUB_STATUS'].value}">
			    <td class="status" align="center" style="white-space: nowrap"><fmt:message key="orderSubStatus_${orderStatus.subStatus}"/></td>
			  </c:if>
			  
			  <td class="statusHistory" align="center"><c:choose><c:when test="${orderStatus.userNotified}"><img src="../graphics/checkbox.png"></c:when><c:otherwise><img src="../graphics/box.png"></c:otherwise></c:choose></td>
			  <td class="statusHistory">
		        <c:choose>
		         <c:when test="${fn:containsIgnoreCase(order.shippingMethod,'ups') or fn:containsIgnoreCase(order.shippingCarrier,'ups')}">
		          <a href="http://wwwapps.ups.com/WebTracking/processInputRequest?HTMLVersion=5.0&sort_by=status&tracknums_displayed=10&TypeOfInquiryNumber=T&loc=en_US&InquiryNumber1=<c:out value='${orderStatus.trackNum}' />&AgreeToTermsAndConditions=yes"><c:out value="${orderStatus.trackNum}" /></a>
		         </c:when>
		         <c:when test="${fn:containsIgnoreCase(order.shippingMethod,'fedex') or fn:containsIgnoreCase(order.shippingCarrier,'fedex')}">
		          <a href="http://fedex.com/Tracking?action=track&language=english&cntry_code=us&mps=y&tracknumbers=<c:out value='${orderStatus.trackNum}' />&ascend_header=1"><c:out value="${orderStatus.trackNum}" /></a>
		         </c:when>
		         <c:when test="${fn:containsIgnoreCase(order.shippingMethod,'usps') or fn:containsIgnoreCase(order.shippingCarrier,'usps')}">
		          <a href="http://trkcnfrm1.smi.usps.com/PTSInternetWeb/InterLabelInquiry.do?strOrigTrackNum=<c:out value='${orderStatus.trackNum}' />"><c:out value="${orderStatus.trackNum}" /></a>
		         </c:when>
		         <c:otherwise><c:out value="${orderStatus.trackNum}" /></c:otherwise>
		        </c:choose>			  
        	  </td>
        	  <td style="background-color:#FFFFFF"><c:out value="${orderStatus.carrier}"/></td>
			  <td style="background-color:#FFFFFF"><c:out value="${orderStatus.comments}"/></td>
			  <td style="background-color:#FFFFFF"><c:out value="${orderStatus.username}"/></td>
			</tr>
			</c:if>
			</c:forEach>
			</table>
					
			<form:form commandName="orderStatus" action="invoice.jhtm" onsubmit="return checkMessage()">
			<input type="hidden" name="order" value="<c:out value="${order.orderId}"/>" >
			<form:hidden path="orderId" />
			<table align="left" border="0" class="updateStatus">
			<c:choose>
			  <c:when test="${!((order.status == 's') || (order.status == 'x') || (order.status == 'xap' or order.status == 'ano' or order.status == 'xaf'))}">
			  <tr>
			    <td>
			      <label><fmt:message key="status"/>:</label>
			    </td>
			    <td align="right"> 
			      <form:select path="status" onchange="toggleStatus(this)">
			      <c:choose>
			      <c:when test="${order.status == 'ars'}">
			        <form:option value="ars"><fmt:message key="orderStatus_ars" /></form:option>
			        <form:option value="xaf"><fmt:message key="orderStatus_xaf" /></form:option>
			        <c:choose>
				        <c:when test="${(!order.processed or order.fulfilled) && !order.paymentAlert}">
				          <form:option value="s"><fmt:message key="orderStatus_s" /></form:option>
				        </c:when>
				        <c:otherwise>
				        <sec:authorize ifAnyGranted="ROLE_AEM">
				          <form:option value="s"><fmt:message key="orderStatus_s" /></form:option>
				        </sec:authorize>
				        </c:otherwise>
			        </c:choose>
			      </c:when>
			      <c:otherwise>
			        <form:option value="p"><fmt:message key="orderStatus_p" /></form:option>
			        <form:option value="pr"><fmt:message key="orderStatus_pr" /></form:option>
			        <form:option value="wp"><fmt:message key="orderStatus_wp" /></form:option>
			        <form:option value="rls"><fmt:message key="orderStatus_rls" /></form:option>
			        <c:if test="${!order.processed or order.fulfilled}" >
			        <c:choose>
				        <c:when test="${(!order.processed or order.fulfilled) && !order.paymentAlert}">
				          <form:option value="s"><fmt:message key="orderStatus_s" /></form:option>
				        </c:when>
				        <c:otherwise>
				        <sec:authorize ifAnyGranted="ROLE_AEM,ROLE_ADMIN_${prop_site_id},ROLE_OVERRIDE_PAYMENT_ALERT">
				          <form:option value="s"><fmt:message key="orderStatus_s" /></form:option>
				        </sec:authorize>
				        </c:otherwise>
			        </c:choose>
			          <form:option value="x"><fmt:message key="orderStatus_x" /></form:option>
			        </c:if>
			        <c:if test="${order.status == 'xp'}">
			          <form:option value="xp"><fmt:message key="orderStatus_xp" /></form:option>
			        </c:if>
			        <c:if test="${order.status == 'xnc'}">
			          <form:option value="xnc"><fmt:message key="orderStatus_xnc" /></form:option>
			        </c:if>
			        <c:if test="${order.status == 'xeb'}">
			          <form:option value="xeb"><fmt:message key="orderStatus_xeb" /></form:option>
			        </c:if>
			        <c:if test="${order.status == 'xg'}">
			          <form:option value="xg"><fmt:message key="orderStatus_xg" /></form:option>
			        </c:if>			        
			        <c:if test="${order.status == 'xge'}">
			          <form:option value="xge"><fmt:message key="orderStatus_xge" /></form:option>
			        </c:if>		
			      </c:otherwise>
			      </c:choose>
			      </form:select>
				</td>
			  </tr>
			  </c:when>
			  <c:otherwise>
			    <form:hidden path="status"/>
			  </c:otherwise>
			</c:choose>
			  <c:if test="${siteConfig['SUB_STATUS'].value != '0'}">
			  <tr><!-- 
				<td>
			      <label><fmt:message key="subStatus"/>:</label>
			    </td>
			    <td align="right">
			      <form:select path="subStatus">
			        <form:option value=""><fmt:message key="orderSubStatus_" /></form:option>
			        <c:forEach begin="0" end="${siteConfig['SUB_STATUS'].value}" var="value">
			          <form:option value="${value}"><fmt:message key="orderSubStatus_${value}" /></form:option>
			        </c:forEach>
			      </form:select>
				</td>  -->
			  </tr>
			  <tr>
			   <c:if test="${orderStatus.cancelReason == null or empty orderStatus.cancelReason }">
			    <td>
			      <label>Cancel Reason:</label>
			    </td>
			    <td align="right">
                   <select name="cancelReason" id="cancelReason">
			        <option value=""></option>
			        <c:forEach items="${siteConfig['ORDER_CANCEL_REASONS'].value}" var="value">
			          <option value="${value}"
			          <c:if test="${value == orderStatus.cancelReason}">
			          	selected
			          </c:if>	
			          >${value}</option>
			          </option>
			        </c:forEach>
			      </select>			     
				</td>
			</c:if>
			  </tr>
			  </c:if>
			  <c:if test="${fn:contains(siteConfig['CUSTOMIZE_ORDER_LIST'].value,'priority')}">
			  <tr>
				<td>
			      <label><fmt:message key="priority"/>:</label>
			    </td>
			    <td align="right">
			      <c:forEach begin="0" end="2" var="priority">
	  			  <form:radiobutton value="${priority}" path="priority" /><img src="../graphics/priority_${priority}.png" border="0">&nbsp;&nbsp;&nbsp;
			      </c:forEach>
				</td>
			  </tr>
			  </c:if>
			  <c:if test="${fn:contains(siteConfig['CUSTOMIZE_ORDER_LIST'].value,'deliveryPerson')}">
			  <tr>
			    <td>
			      <label><fmt:message key="deliveryPerson"/>:</label>
			    </td>
			    <td align="right">  
			      <form:input path="deliveryPerson" maxlength="50" htmlEscape="true"/>
				</td>
			  </tr>
			  </c:if>
			  <tr>
			    <td>
			      <label><fmt:message key="trackNum"/>:</label>
			    </td>
			    <td align="right">  
			      <form:input path="trackNum" />
				</td>
			  </tr>
			  <tr>
			  <c:set var="customShippingAmt" value="0" />
			  <c:forEach var="customeShipping" items="${customeShippingList}" varStatus="status">
				<input type="hidden" id="shipping_${status.index}" name="shipping_${status.index}" value="${customeShipping.title}"/>
				<c:set var="customShippingAmt" value="${customShippingAmt+1}" />    
			  </c:forEach>
			  <input type="hidden" id="customShippingMax" name="customShippingMax" value="${customShippingAmt}"/>
			  	<td colspan="2"><fmt:message key="comments"/>:</td>    
			  </tr>
			  <tr>
				<td colspan="2"><form:textarea path="comments" rows="5" cols="80" htmlEscape="true"/></td>
			  </tr>
			  <tr>
				<td style="width:50px"><fmt:message key="carrier"/>:</td><td><fmt:message key="quote"/>:</td>
			  </tr>
			  <tr>
			  
			  
			  <td>
			  <!--<input type="hidden" id="contact" name="contact"/>-->
			  <select name="contact" id="contact" onChange="insertContact(this)">
			      <option value="">Choose a Contact</option>
				  <c:forEach var="contact" items="${contacts}">
				    <option value="${contact.id}"><c:out value="${contact.company}"/></option>
				  </c:forEach>
			  </select>
				  <input onclick="sendShippedEmail(${order.orderId})" type="button" value="Send Shipped Email">
			  </td>
			  
				<td><input name="quote" value="${quote}" htmlEscape="true"/></td>
			  </tr>
			  <tr>
			    <td>
			      <label>Notify Customer:</label>
			      <form:checkbox path="userNotified" onclick="toggleMessage()"/>
			    </td>
			    <td height="30" align="right">
			      <div id="messageSelect" style="display:none;">
			      <c:if test="${siteConfig['CUSTOM_SHIPPING_CONTACT_INFO'].value == 'true'}">
				  <!--<select name="contact" onChange="insertContact(this)">
			        <option value="">choose a contact</option>
				  <c:forEach var="contact" items="${contacts}">
				    <option value="${contact.id}"><c:out value="${contact.company}"/></option>
				  </c:forEach>
				  </select>-->
				  <c:forEach var="contact" items="${contacts}">
			  	   <input type="hidden" id="contact_company_${contact.id}" value="${contact.company}" />
			      </c:forEach>
			      </c:if>
			      <select id="groupSelected" onChange="getSiteMessage(this.value, ${order.orderId})" >
			        <option value="">choose a group</option>
			         <option value="-1"> All Messages</option>
				  <c:forEach var="group" items="${messageGroups}">
				    <option value="${group.id}"><c:out value="${group.name}"/></option>
				  </c:forEach>
				  </select>		  
			      <div id="siteMessageSelect">
		  	 	  </div>	
				  </div>
				</td>
				<td></td>			
			  </tr>
			  <tr>
				<td colspan="2">
				  <div id="messageBox" style="display:none;">
				  <table class="notifyCustomer">
				    <c:if test="${gSiteConfig['gPDF_INVOICE']}">
				   <!--  <tr>
				      <td align="right"><fmt:message key="attach"/>&nbsp;<fmt:message key="invoice"/>:&nbsp;&nbsp;</td>
				      <td><input type="checkbox" name="__attach" value="true"></td>
				    </tr> -->
				    </c:if>
				    <tr>
				      <td align="right"><fmt:message key="to"/>:&nbsp;&nbsp;</td>
				      <td><c:out value="${customer.username}"/></td>
				    </tr>
				    <tr>
				      <td align="right"><fmt:message key="from"/>:&nbsp;&nbsp;</td>
				      <td><form:input path="from" size="50" htmlEscape="true"/></td>
				    </tr>
				    <tr>
				      <td align="right">Cc:&nbsp;&nbsp;</td>
				      <td>
				        <form:input path="cc" size="50" htmlEscape="true"/>
				      </td>
				    </tr>
				    <tr>
				      <td align="right">Bcc:&nbsp;&nbsp;</td>
				      <td><c:choose><c:when test="${multiStore != null}"><c:out value="${multiStore.contactEmail}"/></c:when><c:otherwise><c:out value="${siteConfig['CONTACT_EMAIL'].value}"/></c:otherwise></c:choose></td>
				    </tr>
				    <tr>
				      <td align="right"><fmt:message key="subject"/>:&nbsp;&nbsp;</td>
				      <td>
				        <form:input id="subject" path="subject" size="50" maxlength="50"/>
				  		<c:forEach var="message" items="${messages}">
				    	<input type="text" disabled style="display:none;" id="subject${message.messageId}" name="subject" value="<c:out value="${message.subject}"/>" size="50" maxlength="50">
				  		</c:forEach>
				      </td>
				    </tr>
				    <tr>
				      <td align="right">Html:&nbsp;&nbsp;</td>
				      <td>
				        <form:checkbox path="htmlMessage" />
				  		<c:forEach var="message" items="${messages}">
				    	<input type="hidden" id="htmlID${message.messageId}" value="<c:out value="${message.html}"/>" />
				  		</c:forEach>
				      </td>
				    </tr>
				  </table>
				  <table class="notifyCustomerMessage">  
				    <tr>
				      <td>
				        <form:textarea id="message" path="message" cssClass="notifyMessageBox"/>
				  		<c:forEach var="message" items="${messages}">
				    		<textarea disabled style="display:none;" id="message${message.messageId}" name="message" class="notifyMessageBox"><c:out value="${message.message}"/></textarea>
				    	 	<input type="hidden" id="sourceUrl${message.messageId}" name="sourceUrl" value="${message.sourceUrl}">
				  		</c:forEach>
				      </td>
				    </tr>	    
				  </table>
				  </div>
				</td>
			  </tr>
			  <tr>
				<td colspan="2">
				<sec:authorize ifAnyGranted="ROLE_AEM,ROLE_ADMIN_${prop_site_id},ROLE_INVOICE_STATUS">
				<div class="button">
					<a style="display:none" href="#" onclick="loadPreview()" class="htmleditor" id="htmleditor"> <img src="../graphics/magnifier.gif" border="0"> Preview </a>
				  <input type="submit" name="__status" value="Update Status" onclick="return checkStatus();">
				  <input type="reset" value="Reset" onclick="document.getElementById('userNotified1').checked=false;toggleMessage()">
				</div>  
				</sec:authorize>  
				</td>
			  </tr>
			  
			</table> 
			</form:form>
			

			
			
            <!-- end input field -->
	        </div>
	  	
	<!-- end tab -->        
	</div>	
	
  <c:if test="${gSiteConfig['gPAYMENTS']}">
  <h4 title="Open <fmt:message key="orders"/>">Open <fmt:message key="orders"/></h4>
	<div>
	<!-- start tab -->
	  	<div class="listdivi ln tabdivi"></div>
	  	<div class="listdivi"></div>
	   
		  	<div class="list${classIndex % 2}"><c:set var="classIndex" value="${classIndex+1}" />
		  	<div class="listpp">
		  	<!-- input field -->
				<table class="form" width="100%" cellspacing="5">
				  <tr>
				    <td>&nbsp;</td>
				    <td align="center"><b><fmt:message key="orderNumber" /></b></td>
				    <td align="center"><b><fmt:message key="orderDate" /></b></td>
				    <td align="center"><b><fmt:message key="status" /></b></td>
				    <td align="right"><b><fmt:message key="grandTotal" /></b></td>
				    <td align="right"><b><fmt:message key="paid" /></b></td>
				    <td align="right"><b><fmt:message key="due" /></b></td>
				    <td width="20%">&nbsp;</td>
				  </tr>
				<c:set var="totalGrandTotal" value="0.0" />
				<c:set var="totalAmtPaid" value="0.0" />
				<c:set var="totalAmtDue" value="0.0" />
				<c:if test="${openInvoices == null}">
				  <tr><td colspan="8">&nbsp;</td></tr>
				</c:if>
			    <c:forEach items="${openInvoices}" var="openInvoice" varStatus="status">
				  <tr>
				    <td style="width:50px;text-align:right"><c:out value="${status.index + 1}"/>. </td>
				    <td align="center"><a href="../orders/invoice.jhtm?order=${openInvoice.orderId}" class="nameLink"><c:out value="${openInvoice.orderId}"/></a></td>
				    <td align="center"><fmt:formatDate type="date" timeStyle="default" value="${openInvoice.dateOrdered}"/></td>
					<td align="center"><c:choose><c:when test="${openInvoice.status != null}"><fmt:message key="orderStatus_${openInvoice.status}" /></c:when><c:otherwise>&nbsp;</c:otherwise></c:choose></td>
					<td align="right"><fmt:message key="${siteConfig['CURRENCY'].value}" /><fmt:formatNumber value="${openInvoice.grandTotal}" pattern="#,##0.00" /><c:if test="${openInvoice.grandTotal != null}"><c:set var="totalGrandTotal" value="${openInvoice.grandTotal + totalGrandTotal}" /></c:if></td>
					<td align="right"><fmt:message key="${siteConfig['CURRENCY'].value}" /><fmt:formatNumber value="${openInvoice.amountPaid}" pattern="#,##0.00" /><c:if test="${openInvoice.amountPaid != null}"><c:set var="totalAmtPaid" value="${openInvoice.amountPaid + totalAmtPaid}" /></c:if></td>
					<td align="right"><fmt:message key="${siteConfig['CURRENCY'].value}" /><fmt:formatNumber value="${openInvoice.balance}" pattern="#,##0.00" /><c:if test="${openInvoice.balance != null}"><c:set var="totalAmtDue" value="${openInvoice.balance + totalAmtDue}" /></c:if></td>
					<td>&nbsp;</td>
				  </tr>
				</c:forEach>
				  <tr>
					<td colspan="4" align="right"><b><fmt:message key="total" /></b></td>
				    <td align="right"><b><fmt:message key="${siteConfig['CURRENCY'].value}" /><fmt:formatNumber value="${totalGrandTotal}" pattern="#,##0.00" /></b></td>
				    <td align="right"><b><fmt:message key="${siteConfig['CURRENCY'].value}" /><fmt:formatNumber value="${totalAmtPaid}" pattern="#,##0.00" /></b></td>
				    <td align="right"><b><fmt:message key="${siteConfig['CURRENCY'].value}" /><fmt:formatNumber value="${totalAmtDue}" pattern="#,##0.00" /></b></td>
				  </tr>
				  <tr>
					<td colspan="4" align="right"><b><fmt:message key="payment" /> <fmt:message key="alert" /> - <fmt:message key="due" /></b></td>
				    <td align="right"><b><fmt:message key="${siteConfig['CURRENCY'].value}" /><fmt:formatNumber value="${customer.paymentAlert - totalAmtDue}" pattern="#,##0.00" /></b></td>
				  </tr>
				</table>
		    <!-- end input field -->   	
		  	</div>
		  	</div>

	<!-- end tab -->        
	</div> 
	</c:if>	
	
	<c:if test="${gSiteConfig['gBUDGET'] and order.actionHistory != null and !empty order.actionHistory and fn:length(order.actionHistory) gt 0}">
	<h4 title="Approval History">Approval History</h4><div>
	<!-- start tab -->
	  	<div class="listdivi ln tabdivi"></div>
	  	<div class="listdivi"></div>
	   
		  	<div class="list${classIndex % 2}"><c:set var="classIndex" value="${classIndex+1}" />
		  	<div class="listpp">
		  	<!-- input field -->
			<table border="0" cellpadding="2" cellspacing="1" width="100%" class="actionHistory">
			<tr>
			  <th class="actionHistory">Action By</th>
			  <th class="actionHistory">Type</th>
			  <th class="actionHistory">Date</th>
			  <th class="actionHistory">Note</th>
			</tr>
			<c:forEach var="order" items="${order.actionHistory}">
			<tr valign="top">
			  <td class="actionHistory" style="white-space: nowrap"><c:out value="${order.actionByName}"/></td>
			  <td class="actionHistory" style="white-space: nowrap"><c:out value="${order.actionType}"/></td>
			  <td class="status" align="center" style="white-space: nowrap"><fmt:formatDate type="both" timeStyle="full" value="${order.actionDate}"/></td>
			  <td class="actionHistory" align="center"><c:out value="${order.actionNote}"/></td>
			</tr>
			</c:forEach>
			</table>
			<!-- end input field -->   	
		  	</div>
		  	</div>

	<!-- end tab -->        
	</div> 
	</c:if>
	
	<sec:authorize ifAnyGranted="ROLE_AEM,ROLE_ADMIN_${prop_site_id},ROLE_PURCHASE_ORDER_UPDATE,ROLE_PURCHASE_ORDER_CREATE">     
	<c:if test="${poList != null and fn:length(poList) > 0}">
    <h4 title="Purchase Orders">Purchase Orders</h4>
	<div>
	<!-- start tab -->
	  	<div class="listdivi ln tabdivi"></div>
	  	<div class="listdivi"></div>
	   
		  	<div class="list${classIndex % 2}"><c:set var="classIndex" value="${classIndex+1}" />
		  	<div class="listpp">
		  	<!-- input field -->
				
			    <table width="100%" class="poList" cellpadding="2" cellspacing="1" border="0">
				 <tr class="listingsHdr2"">
				  <th class="poList"> &nbsp; </th>
				  <th class="poList"><fmt:message key="poNumber" /></th>
				  <th class="poList"><fmt:message key="status" /></th>
				  <th class="poList"><fmt:message key="created" /></th>
				  <th class="poList"><fmt:message key="total" /></th>
				  <th class="poList"><fmt:message key="shipVia" /></th>
				  <th class="poList"><fmt:message key="shippingQuote" /></th>
				  <th class="poList"><fmt:message key="orderBy" /></th>
				  <th class="poList"><fmt:message key="supplier" /></th>
				  <th class="poList"><fmt:message key="supplierInvoiceNumber" /></th>
				  <th class="poList"><fmt:message key="note" /></th>
				</tr>
				<c:forEach items="${poList}" var="po" varStatus="status">
				  <tr class="row${status.index % 2}"	onmouseover="changeStyleClass(this,'rowOver')" 	onmouseout="changeStyleClass(this,'row${status.index % 2}')">
					<td class="poList">
					  <c:out value="${status.index + 1}" />.
					</td>
					<td class="poList"><a href="../inventory/purchaseOrderForm2.jhtm?poId=${po.poId}"><c:out value="${po.poNumber}" /></a></td>
					<td class="poList"><fmt:message key="${po.statusName}" /></td>
					<td class="poList"><fmt:formatDate type="both" timeStyle="full" pattern="MM/dd/yyyy" value="${po.created}"/></td>
					<td class="poList"><fmt:message key="${siteConfig['CURRENCY'].value}" /><fmt:formatNumber value="${po.subTotal}" pattern="#,##0.00"/></td>
					<td class="poList"><c:out value="${po.shipVia}" /></td>
					<td class="poList"><fmt:message key="${siteConfig['CURRENCY'].value}" /><fmt:formatNumber value="${po.shippingQuote}" pattern="#,##0.00"/></td>
					<td class="poList"><c:out value="${po.orderBy}" /></td>
					<td class="poList"><c:out value="${po.company}" /></td>
					<td class="poList"><c:out value="${po.supplierInvoiceNumber }" /></td>
					<td class="poList"><c:out value="${po.noteTrimmed}" /></td>
				  </tr>
				</c:forEach>
				</table>
			  
		    <!-- end input field -->   	
		  	</div>
		  	</div>
			
	<!-- end tab -->        
	</div> 
	</c:if>
	</sec:authorize>	
	
<!-- end tabs -->			
</div>
</c:if>  
  <!-- end table -->
   </td><td class="boxmidr" ></td></tr>
  <tr><td class="botl"></td><td class="botr"></td></tr>
  </table>
  
<!-- end main box -->  
</div>

<script language="JavaScript">
<!--
function toggleStatus(el) {
  if (el.value == "xaf") {
	  document.getElementById('trackNum').disabled=true;
  } else {
	  document.getElementById('trackNum').disabled=false;
  }
}
toggleStatus(document.getElementById('status'));
//-->
</script>
<c:if test="${showPOBox and siteConfig['PREMIER_DISTRIBUTOR'].value != ''}">
<script type="text/javascript">
<!--
window.addEvent('domready', function(){			
	mbPOBox.open($('mbPOLineItems'));
});
//-->
</script>
</c:if>
    
</tiles:putAttribute>    
</tiles:insertDefinition>
