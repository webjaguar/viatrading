<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ taglib uri="/WEB-INF/tlds/spring.tld" prefix="spring" %>
<%@ taglib uri="http://www.springframework.org/security/tags" prefix="sec" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>

<tiles:insertDefinition name="admin.orders.export" flush="true">
  <tiles:putAttribute name="content" type="string">

<sec:authorize ifAnyGranted="ROLE_AEM,ROLE_ADMIN_${prop_site_id},ROLE_INVOICE_IMPORT_EXPORT">  
<c:if test="${gSiteConfig['gINVOICE_EXPORT']}">
<script language="JavaScript" type="text/JavaScript">
<!--
window.addEvent('domready', function(){			
	var tabs1 = new SimpleTabs($('tab-block-1'), { selector: 'h4'});
});
//-->
</script>
<!-- main box -->
  <div id="mboxfull">
  
  <!-- start table -->
  <table cellpadding="0" cellspacing="0" border="0" class="module" >
  <tr>
    <td class="topl_g">
	  
	  <!-- breadcrumb -->
	  <p class="breadcrumb">
	    <a href="../orders/"><fmt:message key="orders"/></a> &gt;
	    <fmt:message key="export"/> - <c:out value="${orderSearch.fileType}" />
	  </p>
	  
	  <!-- Error Message -->
	  <c:if test="${!empty message}">
		 <div class="message"><spring:message code="${message}" arguments="${arguments}"/></div>
	  </c:if>
	  
  </td><td class="topr_g" ></td></tr>
  <tr><td class="boxmidlrg" >

 <!-- tabs -->
 <div id="tab-block-1">
	<h4 title="export"><fmt:message key="export"/></h4>
	<div>
	<!-- start tab -->

        <c:set var="classIndex" value="0" />
        
	  	<div class="listdivi ln tabdivi"></div>
	  	<div class="listdivi"></div>
		   
		   <div class="list${classIndex % 2}"><c:set var="classIndex" value="${classIndex+1}" />
		  	<div class="listfl"><div class="helpImg"><img class="toolTipImg" title="Number of File:: Number of files exist in Shopworks folder." src="../graphics/question.gif" /></div><fmt:message key="file" />:</div>
		  	<div class="listp">
		  	<!-- input field -->
				<span class="static"><c:out value="${totalNumFiles}"></c:out></span>
		    <!-- end input field -->   	
		  	</div>
	  	    </div>
	  	    
		  	<div class="list${classIndex % 2}"><c:set var="classIndex" value="${classIndex+1}" />
		  	<div class="scroll">
		  	<div class="listfl"><fmt:message key="orderExport"/>:</div>
		  	<div class="listp">
		  	<!-- input field -->
				<table class="form" width="100%">
				  <c:forEach items="${exportedFiles}" var="file">
				  <tr>
				    <td class="formName">&nbsp;</td>
				    <td><a href="../export/order/${file['file'].name}"><c:out value="${file['file'].name}"/></a> 
				    	<c:if test="${file['lastModified'] != null}">(<fmt:formatDate type="both" timeStyle="full" value="${file['lastModified']}"/>)</c:if></td>
				  </tr>
				  </c:forEach>
				</table>
		    <!-- end input field -->   	
		  	</div>
		  	</div>
	  	    </div>
	<!-- end tab -->        
	</div> 
	
<!-- end tabs -->			
</div>

<!-- start button -->

<!-- end button -->	     
  
  <!-- end table -->
    </td><td class="boxmidr" ></td></tr>
  <tr><td class="botl"></td><td class="botr"></td></tr>
  </table>
  
<!-- end main box -->  
 </div>

</c:if>
</sec:authorize>

  </tiles:putAttribute>    
</tiles:insertDefinition>

