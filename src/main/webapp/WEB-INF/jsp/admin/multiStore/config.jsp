<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>

<tiles:insertDefinition name="admin.multiStore" flush="true">
  <tiles:putAttribute name="content" type="string">
  
<script language="JavaScript" type="text/JavaScript">
<!--
window.addEvent('domready', function(){			
			var tabs1 = new SimpleTabs($('tab-block-1'), { selector: 'h4'});
			var Tips1 = new Tips($$('.toolTipImg'));
		});
//-->
</script>	
<form action="configuration.jhtm" method="post">
<!-- main box -->
  <div id="mboxfull">
  
  <!-- start table -->
  <table cellpadding="0" cellspacing="0" border="0" class="module" >
  <tr>
    <td class="topl_g">
	  
	  <!-- breadcrumb -->
	  <p class="breadcrumb">
	    <a href="../multiStore/"><fmt:message key="multiStore"/></a> &gt;
	    <fmt:message key="configuration"/> 
	  </p>
	  
	  <!-- Error Message -->
	  <c:if test="${!empty message}">
		<div class="message">
		  <fmt:message key="${message}">
		    <fmt:param value="${multiStoreList}"/>
		  </fmt:message>
		</div>
	  </c:if>
        
  </td><td class="topr_g" ></td></tr>
  <tr><td class="boxmidlrg" >
 <!-- tabs -->
 <div id="tab-block-1">
 
    <c:forEach items="${multiStores}" var="multiStore">
	<h4 title="${multiStore.host}"><c:out value="${multiStore.host}" /> </h4>
	<div>
	<!-- start tab -->

        <c:set var="classIndex" value="0" />
        
	  	<div class="listdivi ln tabdivi"></div>
	  	<div class="listdivi"></div>

	   		<c:if test="${multiStore.storeId != null}">
		  	<div class="list${classIndex % 2}"><c:set var="classIndex" value="${classIndex+1}" />
		  	<div class="listfl"><fmt:message key="storeId"/>:</div>
		  	<div class="listp">
				<c:out value="${multiStore.storeId}"/>
		  	</div>
		  	</div>
		  	</c:if>

	   		<c:if test="${!gSiteConfig['gREGISTRATION_DISABLED']}">
		  	<div class="list${classIndex % 2}"><c:set var="classIndex" value="${classIndex+1}" />
		  	<div class="listfl"><fmt:message key="contactEmail"/>:</div>
		  	<div class="listp">
		  	<!-- input field -->
			  <input type="text" name="__contactEmail_${multiStore.host}" value="<c:out value="${multiStore.contactEmail}"/>" maxlength="80" size="50">
		    <!-- end input field -->   	
		  	</div>
		  	</div>
		  	
	   		<div class="list${classIndex % 2}"><c:set var="classIndex" value="${classIndex+1}" />
		  	<div class="listfl"><fmt:message key="siteMessageNewRegistrants"/>:</div>
		  	<div class="listp">
		  	<!-- input field -->
			  <select name="__midNewRegistration_${multiStore.host}">
		        <option value="0"></option>
			    <c:forEach var="siteMessage" items="${siteMessages}">
			    <option value="${siteMessage.messageId}" <c:if test="${siteMessage.messageId == multiStore.midNewRegistration}">selected</c:if>><c:out value="${siteMessage.messageName}"/></option>
			    </c:forEach>
		 	  </select>
		    <!-- end input field -->   	
		  	</div>
		  	</div>
		  	
		  	<div class="list${classIndex % 2}"><c:set var="classIndex" value="${classIndex+1}" />
		  	<div class="listfl"><fmt:message key="registrationSuccessPage"/>:</div>
		  	<div class="listp">
		  	<!-- input field -->
			  <input type="text" name="__registrationSuccessUrl_${multiStore.host}" value="<c:out value="${multiStore.registrationSuccessUrl}"/>" maxlength="255" size="50">
		    <!-- end input field -->   	
		  	</div>
		  	</div>
		  	</c:if>

			<c:if test="${gSiteConfig['gSHOPPING_CART']}">
			<div class="list${classIndex % 2}"><c:set var="classIndex" value="${classIndex+1}" />
		  	<div class="listfl"><fmt:message key="siteMessageNewOrders"/>:</div>
		  	<div class="listp">
		  	<!-- input field -->
			  <select name="__midNewOrders_${multiStore.host}">
		        <option value="0"></option>
			    <c:forEach var="siteMessage" items="${siteMessages}">
			    <option value="${siteMessage.messageId}" <c:if test="${siteMessage.messageId == multiStore.midNewOrders}">selected</c:if>><c:out value="${siteMessage.messageName}"/></option>
			    </c:forEach>
		 	  </select>
		    <!-- end input field -->   	
		  	</div>
		  	</div>

		    <c:if test="${siteConfig['DSI'].value != ''}">
		  	<div class="list${classIndex % 2}"><c:set var="classIndex" value="${classIndex+1}" />
		  	<div class="listfl"><fmt:message key="siteMessageShippedOrders"/>:</div>
		  	<div class="listp">
		  	<!-- input field -->
			  <select name="__midShippedOrders_${multiStore.host}">
		        <option value="0"></option>
			    <c:forEach var="siteMessage" items="${siteMessages}">
			    <option value="${siteMessage.messageId}" <c:if test="${siteMessage.messageId == multiStore.midShippedOrders}">selected</c:if>><c:out value="${siteMessage.messageName}"/></option>
			    </c:forEach>
		 	  </select>
		    <!-- end input field -->   	
		  	</div>
		  	</div>
		    </c:if>		
		    
		    <c:if test="${siteConfig['MINIMUM_ORDER'].value == 'true'}">
		  	<div class="list${classIndex % 2}"><c:set var="classIndex" value="${classIndex+1}" />
		  	<div class="listfl"><fmt:message key="minimumOrder" /> <fmt:message key="amount" />:</div>
		  	<div class="listp">
		  	<!-- input field -->
			  <input type="text" name="__minOrderAmt_${multiStore.host}" value="<c:out value="${multiStore.minOrderAmt}"/>" size="10" maxlength="10"/>
		    <!-- end input field -->   	
		  	</div>
		  	</div>
		    </c:if>		  	
		  	
		    <c:if test="${gSiteConfig['gPRICE_TABLE'] > 0 and siteConfig['MINIMUM_ORDER'].value == 'true'}">
		  	<div class="list${classIndex % 2}"><c:set var="classIndex" value="${classIndex+1}" />
		  	<div class="listfl"><fmt:message key="minimumOrderForWholeSaler" />:</div>
		  	<div class="listp">
		  	<!-- input field -->
			  <input type="text" name="__minOrderAmtWholesale_${multiStore.host}" value="<c:out value="${multiStore.minOrderAmtWholesale}"/>" size="10" maxlength="10"/>
		    <!-- end input field -->   	
		  	</div>
		  	</div>
		    </c:if>		  			    
		      	
		  	</c:if>
		  	
	<!-- end tab -->        
	</div>         
	</c:forEach>
           	
<!-- end tabs -->			
</div>

<!-- start button -->
<div align="left" class="button"> 
  <input type="submit" name="__update" value="<fmt:message key="Update"/>">
</div>
<!-- end button -->	   

  <!-- end table -->
    </td><td class="boxmidr" ></td></tr>
  <tr><td class="botl"></td><td class="botr"></td></tr>
  </table>
  
<!-- end main box -->  
</div>          	
</form>

  </tiles:putAttribute>    
</tiles:insertDefinition>

