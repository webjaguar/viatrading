<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

  <div id="lbox">
   <table cellpadding="0" cellspacing="0" border="0" class="module" style="width: 100%"><tr><td class="topl">
    </td><td class="topr" ></td></tr><tr><td class="boxmid" >
    
    <h2 class="menuleft mfirst"><fmt:message key="faqTabTitle"/></h2>
    <div class="menudiv"></div> 
    <a href="faqList.jhtm" class="userbutton"><fmt:message key="faqsMenuTitle"/></a>
    <c:if test="${gSiteConfig['gGROUP_FAQ']}">
    <a href="faqGroupList.jhtm" class="userbutton"><fmt:message key="groupsMenuTitle"/></a> 
    </c:if>
    <div class="menudiv"></div>

    </td><td class="boxmidr" ></td></tr>
    <tr><td class="botl"></td><td class="botr"></td></tr></table>
  </div>