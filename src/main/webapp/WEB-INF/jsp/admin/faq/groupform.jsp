<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="/WEB-INF/tlds/spring.tld" prefix="spring" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>

<tiles:insertDefinition name="admin.faq" flush="true">
  <tiles:putAttribute name="content" type="string">
<script language="JavaScript"> 
<!--
window.addEvent('domready', function(){			
			var tabs1 = new SimpleTabs($('tab-block-1'), { selector: 'h4'});
		});
//--> 
</script> 
<form:form commandName="faqGroupForm" action="faqGroup.jhtm" method="post">
<!-- main box -->
  <div id="mboxfull">
  
  <!-- start table -->
  <table cellpadding="0" cellspacing="0" border="0" class="module" >
  <tr>
    <td class="topl_g">
	  
	  <!-- breadcrumb -->
	  <p class="breadcrumb">
	    <a href="../faq">FAQ-Group</a> &gt;
	    form 
	  </p>
	  
	  <!-- Error Message -->
	  <spring:hasBindErrors name="faqGroupForm">
		<div class="message">Please fix all errors!</div>
	  </spring:hasBindErrors>
	  
	  <c:if test="${faqGroupForm.faqGroup.rank != null}">
          <spring:bind path="faqGroupForm.faqGroup.rank"><input type="hidden" name="<c:out value="${status.expression}"/>" value="<c:out value="${status.value}"/>"></spring:bind>
      </c:if>
      <c:if test="${!faqGroupForm.newFaqGroup}">
          <spring:bind path="faqGroupForm.faqGroup.id"><input type="hidden" name="<c:out value="${status.expression}"/>" value="<c:out value="${status.value}"/>"></spring:bind>
      </c:if>
	  
  </td><td class="topr_g" ></td></tr>
  <tr><td class="boxmidlrg" >

 <!-- tabs -->
 <div id="tab-block-1">
	<h4 title="FAQ-Group Form">FAQ-Group</h4>
	<div>
	<!-- start tab -->
	<p>
         <c:set var="classIndex" value="0" />
         
	  	<div class="listdivi ln tabdivi"></div>
	  	<div class="listdivi"></div>
	   
		  	<div class="list${classIndex % 2}"><c:set var="classIndex" value="${classIndex+1}" />
		  	<div class="listfl"><div class="requiredField"><fmt:message key="faqGroupName" />:</div></div>
		  	<div class="listp">
		  	<!-- input field -->
				<form:input path="faqGroup.name" cssClass="textfield" maxlength="100" size="50" htmlEscape="true"/>
				<form:errors path="faqGroup.name" cssClass="error"/>
	 		<!-- end input field -->	
	 		</div>
		  	</div>	
		  	
		  	<c:if test="${gSiteConfig['gPROTECTED'] > 0}">
			<div class="list${classIndex % 2}"><c:set var="classIndex" value="${classIndex+1}" />
		  	<div class="listfl"><fmt:message key="protectedAccess" />:</div>
		  	<div class="listp">
		  	<!-- input field -->
				<form:select path="faqGroup.protectedLevel">
		          <form:option value="0"><fmt:message key="none" /></form:option>      
		          <c:forEach begin="1" end="${gSiteConfig['gPROTECTED']}" var="protected">
			    	<c:set var="key" value="protected${protected}"/>
			    	<c:choose>
			      	  <c:when test="${labels[key] != null and labels[key] != ''}">
			        	<c:set var="label" value="${labels[key]}"/>
			      	  </c:when>
			      	  <c:otherwise><c:set var="label" value="${protected}"/></c:otherwise>
			    	</c:choose>		        
		        	<form:option value="${protectedLevels[protected-1]}"><c:out value="${label}"/></form:option>
		      	  </c:forEach>
		       </form:select>
		        <form:errors path="faqGroup.protectedLevel" cssClass="error" />  
		    <!-- end input field -->	
	 		</div>
		  	</div>	
		    </c:if>   
		    
	</p>	  	        
	<!-- end tab -->		
	</div>
<!-- end tabs -->			
</div>

<!-- start button -->
	<div align="left" class="button">    
      <c:if test="${faqGroupForm.newFaqGroup}">
        <input type="submit" value="<spring:message code="faqGroupAdd"/>" />
      </c:if>
      <c:if test="${!faqGroupForm.newFaqGroup}">
        <input type="submit" value="<spring:message code="faqGroupUpdate"/>">
        <input type="submit" value="<spring:message code="faqGroupDelete"/>" name="_delete" onClick="return confirm('Delete permanently?')" />
      </c:if>
    </div> 
<!-- end button -->	

  <!-- end table -->
    </td><td class="boxmidr" ></td></tr>
  <tr><td class="botl"></td><td class="botr"></td></tr>
  </table>
  
<!-- end main box -->  
</div>
</form:form>
 
  </tiles:putAttribute>    
</tiles:insertDefinition>
