<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ taglib uri="/WEB-INF/tlds/spring.tld" prefix="spring" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ taglib uri="http://www.springframework.org/security/tags" prefix="sec" %>

<tiles:insertDefinition name="admin.faq" flush="true">
  <tiles:putAttribute name="content" type="string">
<sec:authorize ifAnyGranted="ROLE_AEM,ROLE_ADMIN_${prop_site_id},ROLE_FAQ_CREATE,ROLE_FAQ_EDIT,ROLE_FAQ_DELETE">
  
<script type="text/javascript" src="<c:url value="/FCKeditor/fckeditor.js"/>"></script>

<script language="JavaScript"> 
<!--
function loadEditor(el) {
  var oFCKeditor = new FCKeditor(el);
  oFCKeditor.BasePath = "<c:url value="/FCKeditor/"/>";
  oFCKeditor.Config["CustomConfigurationsPath"] = '<c:url value="/admin/javascript/fckconfig.js"/>';
  oFCKeditor.ToolbarSet = 'Html';
  oFCKeditor.Width = 650;
  oFCKeditor.Height = 400;
  oFCKeditor.ReplaceTextarea();
  document.getElementById(el + '_link').style.display="none";
}
//--> 
</script> 
<script language="JavaScript"> 
<!--
window.addEvent('domready', function(){			
			var tabs1 = new SimpleTabs($('tab-block-1'), { selector: 'h4'});
		});
//--> 
</script> 
<form:form commandName="faqForm" action="faq.jhtm" method="post">
<form:hidden path="newFaq" />
<form:hidden path="faq.id" />

<!-- main box -->
  <div id="mboxfull">
  
  <!-- start table -->
  <table cellpadding="0" cellspacing="0" border="0" class="module" >
  <tr>
    <td class="topl_g">
	  
	  <!-- breadcrumb -->
	  <p class="breadcrumb">
	    <a href="../faq">FAQ</a> &gt;
	    form 
	  </p>
	  
	  <!-- Error Message -->
	  <%--<c:if test="${message != null}">
  		<div class="message"><fmt:message key="${message}"/></div>
  	  </c:if> --%>
  	  <spring:hasBindErrors name="faqForm">
		<div class="message">Please fix all errors!</div>
	  </spring:hasBindErrors>

  </td><td class="topr_g" ></td></tr>
  <tr><td class="boxmidlrg" >

 <!-- tabs -->
 <div id="tab-block-1">
	<h4 title="FAQ Form">FAQ</h4>
	<div>
	<!-- start tab -->

         <c:set var="classIndex" value="0" />
         
	  	<div class="listdivi ln tabdivi"></div>
	  	<div class="listdivi"></div>
	   
		  	<div class="list${classIndex % 2}"><c:set var="classIndex" value="${classIndex+1}" />
		  	<div class="listfl"><div class="requiredField"><fmt:message key="faqQuestion" />:</div></div>
		  	<div class="listp">
		  	<!-- input field -->
				<form:input path="faq.question" cssClass="textfield" maxlength="255" size="60" htmlEscape="true"/>
				<form:errors path="faq.question" cssClass="error"/>
	 		<!-- end input field -->	
	 		</div>
		  	</div>	

		  	<div class="list${classIndex % 2}"><c:set var="classIndex" value="${classIndex+1}" />
		  	<div class="listfl"><div class="requiredField"><fmt:message key="faqAnswer" />:</div></div>
		  	<div class="listp">
		  	<!-- input field -->
				<form:textarea path="faq.answer" cssClass="textArea700x400" htmlEscape="true"/>
				<form:errors path="faq.answer" cssClass="error"/>
				<div style="text-align:right" id="faq.answer_link"><a href="#" onClick="loadEditor('faq.answer')" class="htmleditor">- <fmt:message key="htmlEditor" /> -</a></div>
	 		<!-- end input field -->	
	 		</div>
		  	</div>	

		  	<c:if test="${gSiteConfig['gPROTECTED'] > 0}">
			<div class="list${classIndex % 2}"><c:set var="classIndex" value="${classIndex+1}" />
		  	<div class="listfl"><fmt:message key="protectedAccess" />:</div>
		  	<div class="listp">
		  	<!-- input field -->
				<form:select path="faq.protectedLevel">
		          <form:option value="0"><fmt:message key="none" /></form:option>      
		          <c:forEach begin="1" end="${gSiteConfig['gPROTECTED']}" var="protected">
			    	<c:set var="key" value="protected${protected}"/>
			    	<c:choose>
			      	  <c:when test="${labels[key] != null and labels[key] != ''}">
			        	<c:set var="label" value="${labels[key]}"/>
			      	  </c:when>
			      	  <c:otherwise><c:set var="label" value="${protected}"/></c:otherwise>
			    	</c:choose>		        
		        	<form:option value="${protectedLevels[protected-1]}"><c:out value="${label}"/></form:option>
		      	  </c:forEach>
		        </form:select>
		        <form:errors path="faq.protectedLevel" cssClass="error" />  
		    <!-- end input field -->	
	 		</div>
		  	</div>	
		    </c:if> 

			<c:if test="${gSiteConfig['gGROUP_FAQ']}">
		  	<div class="list${classIndex % 2}"><c:set var="classIndex" value="${classIndex+1}" />
		  	<div class="listfl"><fmt:message key="inFaqGroup" />:</div>
		  	<div class="listp">
		  	<!-- input field -->
	    		<form:select path="faq.groupIdList" multiple="true">
		          <form:option value="" label="Please Select"></form:option>
		          <c:forEach items="${groupList}" var="group">
		            <form:option value="${group.id}" label="${group.name}"></form:option>
		          </c:forEach>      
		        </form:select>
		    <!-- end input field -->	
	 		</div>
		  	</div>	
		  	</c:if>
	  	
	<!-- end tab -->        
	</div>         

<!-- end tabs -->			
</div>

<!-- start button -->
	<div align="left" class="button">
	  <c:if test="${faqForm.newFaq}">
	    <sec:authorize ifAnyGranted="ROLE_AEM,ROLE_ADMIN_${prop_site_id},ROLE_FAQ_CREATE">
          <input type="submit" value="<spring:message code="faqAdd"/>" />
        </sec:authorize>  
      </c:if>
      <c:if test="${!faqForm.newFaq}">
        <sec:authorize ifAnyGranted="ROLE_AEM,ROLE_ADMIN_${prop_site_id},ROLE_FAQ_EDIT">
          <input type="submit" value="<spring:message code="faqUpdate"/>" />
        </sec:authorize>
        <sec:authorize ifAnyGranted="ROLE_AEM,ROLE_ADMIN_${prop_site_id},ROLE_FAQ_DELETE">  
          <input type="submit" value="<spring:message code="faqDelete"/>" name="_delete" onClick="return confirm('Delete permanently?')" />
        </sec:authorize>  
      </c:if>
	</div>
<!-- end button -->
  
  <!-- end table -->
    </td><td class="boxmidr" ></td></tr>
  <tr><td class="botl"></td><td class="botr"></td></tr>
  </table>
  
<!-- end main box -->  
</div>
</form:form>
</sec:authorize> 

  </tiles:putAttribute>    
</tiles:insertDefinition>