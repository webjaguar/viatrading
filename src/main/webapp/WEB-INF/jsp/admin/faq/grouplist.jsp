<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<tiles:insertDefinition name="admin.faq" flush="true">
  <tiles:putAttribute name="content" type="string">  
<form action="faqGroupList.jhtm" method="get">
<!-- main box -->
  <div id="mboxfull">
  
  <!-- start table -->
  <table cellpadding="0" cellspacing="0" border="0" class="module" >
  <tr>
    <td class="topl_g">
	  
	  <!-- breadcrumb -->
	  <p class="breadcrumb">
	    <a href="../faq">Groups</a> &gt;
	    groups 
	  </p>
	  
	  <!-- Error Message -->
	  
	  <!-- header image -->
	  <img class="headerImage" src="../graphics/faq.gif" />
	  
	  </td><td class="topr_g" ></td></tr>
  <tr><td class="boxmidlrg" >

 <!-- tabs -->
 <div class="tab-wrapper">
	<h4 title="List of affiliates"></h4>
	<div>
	<!-- start tab -->

	  	<div class="listdivi"></div>
	       
		  	<div class="listlight">
		  	<!-- input field --> 
		  	<table border="0" cellpadding="0" cellspacing="1" width="100%">
			  <tr>
			  <c:if test="${model.faqGroups.nrOfElements > 0}">
			  <td class="pageShowing">
			  <fmt:message key="showing">
				<fmt:param value="${model.faqGroups.firstElementOnPage + 1}"/>
				<fmt:param value="${model.faqGroups.lastElementOnPage + 1}"/>
				<fmt:param value="${model.faqGroups.nrOfElements}"/>
			  </fmt:message>
			  </td>
			  </c:if>
			  <td class="pageNavi">
			  Page 
				<select name="page" onchange="submit()">
				<c:forEach begin="1" end="${model.faqGroups.pageCount}" var="page">
			  	<option value="${page}" <c:if test="${page == (model.faqGroups.page+1)}">selected</c:if>>${page}</option>
				</c:forEach>
				</select>
			  of <c:out value="${model.faqGroups.pageCount}"/>
			  | 
			  <c:if test="${model.faqGroups.firstPage}"><span class="pageNaviDead">previous</span></c:if>
			  <c:if test="${not model.faqGroups.firstPage}"><a href="<c:url value="faqGroupList.jhtm"><c:param name="page" value="${model.faqGroups.page}"/></c:url>" class="pageNaviLink">previous</a></c:if>
			  | 
			  <c:if test="${model.faqGroups.lastPage}"><span class="pageNaviDead">next</span></c:if>
			  <c:if test="${not model.faqGroups.lastPage}"><a href="<c:url value="faqGroupList.jhtm"><c:param name="page" value="${model.faqGroups.page+2}"/></c:url>" class="pageNaviLink">next</a></c:if>
			  </td>
			  </tr>
			</table>
			
			<table border="0" cellpadding="0" cellspacing="0" width="100%" class="listings">
			  <tr class="listingsHdr2">
			    <td class="indexCol">&nbsp;</td>
			    <td class="listingsHdr3"><fmt:message key="faqGroupName" /></td>
			    <td class="listingsHdr3"><fmt:message key="faqCount" /></td>
			    <td class="listingsHdr3"><fmt:message key="rank" /></td>
			  </tr>
			<c:forEach items="${model.faqGroups.pageList}" var="faqGroup" varStatus="status">
			  <tr class="row${status.index % 2}" onmouseover="changeStyleClass(this,'rowOver')" onmouseout="changeStyleClass(this,'row${status.index % 2}')">
			    <td class="indexCol"><c:out value="${status.count + model.faqGroups.firstElementOnPage}"/>.</td>
			    <td class="nameCol"><a href="faqGroup.jhtm?id=${faqGroup.id}" class="nameLink"><c:out value="${faqGroup.name}"/></a>
			      <c:if test="${gSiteConfig['gPROTECTED'] > 0 and faqGroup.protectedLevelAsNumber != '0'}">
			      <img src="../graphics/padlock.gif" border="0"><sup><c:out value="${faqGroup.protectedLevelAsNumber}"/></sup>
			      </c:if>
			    </td>
			    <td class="countCol"><c:out value="${faqGroup.faqCount}"/></td>
			    <td class="rankCol"><input name="__rank_${faqGroup.id}" type="text" value="${faqGroup.rank}" size="5" maxlength="5" class="rankField"></td>
			  </tr>
			</c:forEach>
			<c:if test="${model.faqGroups.nrOfElements == 0}">
			  <tr class="emptyList"><td colspan="4">&nbsp;</td></tr>
			</c:if>
			</table>
		  	<!-- end input field -->  	  
		  	</div>
		  	
		  	<!-- start button --> 
		  	<div align="left" class="button">
		  	  <input type="submit" name="__add" value="<fmt:message key="faqGroupAdd" />"><input type="submit" name="__update_ranking" value="<fmt:message key="updateRanking" />">
		  	</div>
		  	<!-- end button -->	
	
	<!-- end tab -->        
	</div>
<!-- end tabs -->			
</div>
  
  <!-- end table -->
   </td><td class="boxmidr" ></td></tr>
  <tr><td class="botl"></td><td class="botr"></td></tr>
  </table>
  
<!-- end main box -->  
</div> 
</form>  

  </tiles:putAttribute>    
</tiles:insertDefinition>
