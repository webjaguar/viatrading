<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<tiles:insertDefinition name="admin.faq" flush="true">
    <tiles:putAttribute name="content" type="string">
<form action="faqList.jhtm" method="post">
<!-- main box -->
  <div id="mboxfull">
  
  <!-- start table -->
  <table cellpadding="0" cellspacing="0" border="0" class="module" >
  <tr>
    <td class="topl_g">
	  
	  <!-- breadcrumb -->
	  <p class="breadcrumb">
	    <a href="../faq">FAQ / Groups</a> &gt;
	    faqs 
	  </p>
	  
	  <!-- Error Message -->
	  
	  <!-- header image -->
	  <img class="headerImage" src="../graphics/faq.gif" />
	  
	  </td><td class="topr_g" ></td></tr>
  <tr><td class="boxmidlrg" >

 <!-- tabs -->
 <div class="tab-wrapper">
	<h4 title="List of affiliates"></h4>
	<div>
	<!-- start tab -->
	<p>
	  	<div class="listdivi ln tabdivi"></div>
	  	<div class="listdivi"></div>
	   	    
		  	<div class="listlight">
		  	<!-- input field --> 
		  	<table border="0" cellpadding="0" cellspacing="1" width="100%">
			  <tr>
			  <c:if test="${gSiteConfig['gGROUP_FAQ']}">
			  <td>
			    <select name="groupId" onChange="document.getElementById('page').value=1;submit()">
			  	  <option value=""><fmt:message key="allFaqs" /></option>
				  <c:forEach items="${model.groupList}" var="faqGroup">
			  	  <option value="${faqGroup.id}" <c:if test="${faqGroup.id == model.search['groupId']}">selected</c:if>><c:out value="${faqGroup.name}"/></option>
			  	  </c:forEach>  
				</select>
			  </td>
			  </c:if>
			  <c:if test="${model.faqs.nrOfElements > 0}">
			  <td class="pageShowing">
			  <fmt:message key="showing">
				<fmt:param value="${model.faqs.firstElementOnPage + 1}"/>
				<fmt:param value="${model.faqs.lastElementOnPage + 1}"/>
				<fmt:param value="${model.faqs.nrOfElements}"/>
			  </fmt:message>
			  </td>
			  </c:if>
			  <td class="pageNavi">
			  Page 
				<select name="page" id="page" onchange="submit()">  
			  <c:forEach begin="1" end="${model.faqs.pageCount}" var="page">
			  	<option value="${page}" <c:if test="${page == (model.faqs.page+1)}">selected</c:if>>${page}</option>
			  </c:forEach>
				</select>
			  of <c:out value="${model.faqs.pageCount}"/>
			  | 
			  <c:if test="${model.faqs.firstPage}"><span class="pageNaviDead">previous</span></c:if>
			  <c:if test="${not model.faqs.firstPage}"><a href="<c:url value="faqList.jhtm"><c:param name="page" value="${model.faqs.page}"/><c:param name="groupId" value="${model.search['groupId']}"/><c:param name="size" value="${model.faqs.pageSize}"/></c:url>" class="pageNaviLink">previous</a></c:if>
			  | 
			  <c:if test="${model.faqs.lastPage}"><span class="pageNaviDead">next</span></c:if>
			  <c:if test="${not model.faqs.lastPage}"><a href="<c:url value="faqList.jhtm"><c:param name="page" value="${model.faqs.page+2}"/><c:param name="groupId" value="${model.search['groupId']}"/><c:param name="size" value="${model.faqs.pageSize}"/></c:url>" class="pageNaviLink">next</a></c:if>
			  </td>
			  </tr>
			</table>
			
			
			
			<table border="0" cellpadding="0" cellspacing="0" width="100%" class="listings">
			  <tr class="emptyList"><td colspan="2">&nbsp;</td></tr>
			<c:forEach items="${model.faqs.pageList}" var="faq" varStatus="status">
			  <c:if test="${status.first}">
			    <c:set var="groupId" value="${faq.groupId}"/>
			    <tr class="faqGroupHdr">
			      <td height="20"><a href="faqGroup.jhtm?id=${faq.groupId}" class="faqGroupLink"><c:out value="${faq.groupName}"/></a>&nbsp;</td>
			      <td class="rankCol"><fmt:message key="rank" /></td>
			    </tr>
			  </c:if>
			  <c:if test="${groupId != faq.groupId}">
			    <c:set var="groupId" value="${faq.groupId}"/>
			    <tr class="emptyList"><td colspan="2">&nbsp;</td></tr>
			    <tr class="faqGroupHdr">
			      <td height="20"><a href="faqGroup.jhtm?id=${faq.groupId}" class="faqGroupLink"><c:out value="${faq.groupName}"/></a></td>
			      <td class="rankCol"><fmt:message key="rank" /></td>
			    </tr>
			  </c:if>  
			  <tr class="row0" onmouseover="changeStyleClass(this,'rowOver')" onmouseout="changeStyleClass(this,'row0')">
			    <td class="nameCol">&#8226; <a href="faq.jhtm?id=${faq.id}" class="nameLink"><c:out value="${faq.question}"/></a></td>
			    <td class="rankCol"><input name="__rank_${faq.id}" type="text" value="${faq.rank}" size="5" maxlength="5" class="rankField"></td>    
			  </tr>
			</c:forEach>
			<c:if test="${model.faqs.nrOfElements == 0}">
			  <tr class="emptyList"><td colspan="2">&nbsp;</td></tr>
			</c:if>
			</table>
			
			<table border="0" cellpadding="0" cellspacing="1" width="100%">
			  <tr> 
			  <td class="pageSize">
			    <select name="size" onchange="document.getElementById('page').value=1;submit()">
			    <c:forTokens items="10,25,50" delims="," var="current">
			  	  <option value="${current}" <c:if test="${current == model.faqs.pageSize}">selected</c:if>>${current} <fmt:message key="perPage" /></option>
			    </c:forTokens>
			    </select>
			  </td>
			  </tr>
			</table>
			
		  	<!-- end input field -->  	  
		  	</div>
		  	
		  	<!-- start button --> 
		  	<div align="left" class="button">
		  	  <input type="submit" name="__add" value="<fmt:message key="faqAdd" />">
		  	  <input type="submit" name="__update_ranking" value="<fmt:message key="updateRanking" />">  
		  	</div>
		  	<!-- end button -->	
	
	</p>	  	
	<!-- end tab -->        
	</div>
<!-- end tabs -->			
</div>
  
  <!-- end table -->
   </td><td class="boxmidr" ></td></tr>
  <tr><td class="botl"></td><td class="botr"></td></tr>
  </table>
  
<!-- end main box -->  
</div> 
</form>  

  </tiles:putAttribute>    
</tiles:insertDefinition>
