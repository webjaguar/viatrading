<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/security/tags" prefix="sec" %>

<tiles:insertDefinition name="admin.faq" flush="true">
    <tiles:putAttribute name="content" type="string">
    
<sec:authorize ifAnyGranted="ROLE_AEM,ROLE_ADMIN_${prop_site_id},ROLE_FAQ_CREATE">
<form action="faqList.jhtm" method="post">
<!-- main box -->
  <div id="mboxfull">
  
  <!-- start table -->
  <table cellpadding="0" cellspacing="0" border="0" class="module" >
  <tr>
    <td class="topl_g">
	  
	  <!-- breadcrumb -->
	  <p class="breadcrumb">
	    <a href="../faq">FAQ</a> &gt;
	    faqs 
	  </p>
	  
	  <!-- Error Message -->
	  
	  <!-- header image -->
	  <img class="headerImage" src="../graphics/faq.gif" />
	  
	  </td><td class="topr_g" ></td></tr>
  <tr><td class="boxmidlrg" >

 <!-- tabs -->
 <div class="tab-wrapper">
	<h4 title="List of affiliates"></h4>
	<div>
	<!-- start tab -->

	  	<div class="listdivi"></div>
	    
		  	<div class="listlight">
		  	<!-- input field --> 
		  	<table border="0" cellpadding="0" cellspacing="1" width="100%">
			  <tr>
			  <c:if test="${gSiteConfig['gGROUP_FAQ']}">
			  <td>
			    <select name="groupId" onChange="document.getElementById('page').value=1;submit()">
			  	  <option value=""><fmt:message key="allFaqs" /></option>
				  <c:forEach items="${model.groupList}" var="faqGroup">
			  	  <option value="${faqGroup.id}" <c:if test="${faqGroup.id == model.search['groupId']}">selected</c:if>><c:out value="${faqGroup.name}"/></option>
			  	  </c:forEach>  
				</select>
			  </td>
			  </c:if>
			  <c:if test="${model.faqs.nrOfElements > 0}">
			  <td class="pageShowing">
			  <fmt:message key="showing">
				<fmt:param value="${model.faqs.firstElementOnPage + 1}"/>
				<fmt:param value="${model.faqs.lastElementOnPage + 1}"/>
				<fmt:param value="${model.faqs.nrOfElements}"/>
			  </fmt:message>
			  </td>
			  </c:if>
			  <td class="pageNavi">
			  Page 
			  <select name="page" id="page" onchange="submit()">  
			    <c:forEach begin="1" end="${model.faqs.pageCount}" var="page">
			  	<option value="${page}" <c:if test="${page == (model.faqs.page+1)}">selected</c:if>>${page}</option>
			    </c:forEach>
			  </select>
			  of <c:out value="${model.faqs.pageCount}"/>
			  | 
			  <c:if test="${model.faqs.firstPage}"><span class="pageNaviDead">previous</span></c:if>
			  <c:if test="${not model.faqs.firstPage}"><a href="<c:url value="faqList.jhtm"><c:param name="page" value="${model.faqs.page}"/><c:param name="groupId" value="${model.search['groupId']}"/><c:param name="size" value="${model.faqs.pageSize}"/></c:url>" class="pageNaviLink">previous</a></c:if>
			  | 
			  <c:if test="${model.faqs.lastPage}"><span class="pageNaviDead">next</span></c:if>
			  <c:if test="${not model.faqs.lastPage}"><a href="<c:url value="faqList.jhtm"><c:param name="page" value="${model.faqs.page+2}"/><c:param name="groupId" value="${model.search['groupId']}"/><c:param name="size" value="${model.faqs.pageSize}"/></c:url>" class="pageNaviLink">next</a></c:if>
			  </td>
			  </tr>
			</table>
			
			<table border="0" cellpadding="0" cellspacing="0" width="100%" class="listings">
			  <tr class="listingsHdr2">
			    <td class="indexCol">&nbsp;</td>
			    <td class="listingsHdr3"><fmt:message key="faqQuestion" /></td>
			    <td class="listingsHdr3 rankCol"><fmt:message key="rank" /></td>
			  </tr>
			<c:forEach items="${model.faqs.pageList}" var="faq" varStatus="status">
			  <tr class="row${status.index % 2}" onmouseover="changeStyleClass(this,'rowOver')" onmouseout="changeStyleClass(this,'row${status.index % 2}')">
			    <td class="indexCol"><c:out value="${status.count + model.faqs.firstElementOnPage}"/>.</td>
			    <td class="nameCol"><a href="faq.jhtm?id=${faq.id}" class="nameLink"><c:out value="${faq.question}"/></a>
			      <c:if test="${gSiteConfig['gPROTECTED'] > 0 and faq.protectedLevelAsNumber != '0'}">
			      <img src="../graphics/padlock.gif" border="0"><sup><c:out value="${faq.protectedLevelAsNumber}"/></sup>
			      </c:if>
			    </td>
			    <td class="rankCol"><input name="__rank_${faq.id}" type="text" value="${faq.rank}" size="5" maxlength="5" class="rankField"></td>
			  </tr>
			</c:forEach>
			<c:if test="${model.faqs.nrOfElements == 0}">
			  <tr class="emptyList"><td colspan="3">&nbsp;</td></tr>
			</c:if>
			</table>
			
			<table border="0" cellpadding="0" cellspacing="1" width="100%">
			  <tr>
			  <td class="pageSize">
			    <select name="size" onchange="document.getElementById('page').value=1;submit()">
			    <c:forTokens items="10,25,50" delims="," var="current">
			  	  <option value="${current}" <c:if test="${current == model.faqs.pageSize}">selected</c:if>>${current} <fmt:message key="perPage" /></option>
			    </c:forTokens>
			    </select>
			  </td>
			  </tr>
			</table>
		  	<!-- end input field -->  	  
		  	
		  	<!-- start button --> 
		  	<div align="left" class="button">
			  <sec:authorize ifAnyGranted="ROLE_AEM,ROLE_ADMIN_${prop_site_id},ROLE_FAQ_CREATE">
			    <input type="submit" name="__add" value="<fmt:message key='faqAdd' />">
			  </sec:authorize>  
			  <input type="submit" name="__update_ranking" value="<fmt:message key="updateRanking" />">
			</div>    
		  	<!-- end button -->	
	        </div>
	  	
	<!-- end tab -->        
	</div>
<!-- end tabs -->			
</div>
  
  <!-- end table -->
   </td><td class="boxmidr" ></td></tr>
  <tr><td class="botl"></td><td class="botr"></td></tr>
  </table>
  
<!-- end main box -->  
</div> 
</form> 
</sec:authorize>		  	

  </tiles:putAttribute>    
</tiles:insertDefinition>
