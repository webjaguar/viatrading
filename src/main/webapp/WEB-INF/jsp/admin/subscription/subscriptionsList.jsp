<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>

<tiles:insertDefinition name="admin.orders" flush="true">
  <tiles:putAttribute name="content" type="string">

<form action="subscriptionsList.jhtm" id="list" method="post">
<input type="hidden" id="sort" name="sort" value="${subscriptionSearch.sort}" />
<!-- main box -->
  <div id="mboxfull">
  
  <!-- start table -->
  <table cellpadding="0" cellspacing="0" border="0" class="module" >
  <tr>
    <td class="topl_g">
	  
	  <!-- breadcrumb -->
	  <p class="breadcrumb">
	    <a href="../orders/"><fmt:message key="orders"/></a> &gt;
	    <fmt:message key="subscriptions"/>
	  </p>
	  
	  <!-- Error Message -->
	  
	  <!-- header image -->
	  <img class="headerImage" src="../graphics/orders.gif" />

  
  </td><td class="topr_g" ></td></tr>
  <tr><td class="boxmidlrg" >

 <!-- tabs -->
 <div class="tab-wrapper">
	<h4 title="List of affiliates"></h4>
	<div>
	<!-- start tab -->

	  	<div class="listdivi ln tabdivi"></div>
	  	<div class="listdivi"></div>
	   
		  	<div class="listlight">
		  	<!-- input field -->
		  	<table border="0" cellpadding="0" cellspacing="1" width="100%">
			  <tr>
			  <td class="pageShowing">
			  <c:if test="${model.list.nrOfElements > 0}">
			  <fmt:message key="showing">
				<fmt:param value="${model.list.firstElementOnPage + 1}"/>
				<fmt:param value="${model.list.lastElementOnPage + 1}"/>
				<fmt:param value="${model.list.nrOfElements}"/>
			  </fmt:message>
			  </c:if>
			  </td>
			  <td class="pageNavi">
			  Page 
			  <select name="page" id="page" onchange="submit()">
			  <c:forEach begin="1" end="${model.list.pageCount}" var="page">
			  	<option value="${page}" <c:if test="${page == (model.list.page+1)}">selected</c:if>>${page}</option>
			  </c:forEach>
			  </select>
			  of <c:out value="${model.list.pageCount}"/>
			  | 
			  <c:if test="${model.list.firstPage}"><span class="pageNaviDead"><fmt:message key="previous" /></span></c:if>
			  <c:if test="${not model.list.firstPage}"><a href="<c:url value="subscriptionsList.jhtm"><c:param name="page" value="${model.list.page}"/></c:url>" class="pageNaviLink"><fmt:message key="previous" /></a></c:if>
			  | 
			  <c:if test="${model.list.lastPage}"><span class="pageNaviDead"><fmt:message key="next" /></span></c:if>
			  <c:if test="${not model.list.lastPage}"><a href="<c:url value="subscriptionsList.jhtm"><c:param name="page" value="${model.list.page+2}"/></c:url>" class="pageNaviLink"><fmt:message key="next" /></a></c:if>
			  </td>
			  </tr>
			</table>
			
			<table border="0" cellpadding="0" cellspacing="0" width="100%" class="listings">
			  <tr class="listingsHdr2">
			    <td class="indexCol">&nbsp;</td>
			    <td align="center" class="listingsHdr3"><fmt:message key="Enabled" /></td>
			    <td class="nameCol">
			    	<table cellspacing="0" cellpadding="1">
			    	  <tr>
			    	    <c:choose>
			    	      <c:when test="${subscriptionSearch.sort == 'code DESC'}">
			    	        <td class="listingsHdr3">
			    	        <a class="hdrLink" href="#" onClick="document.getElementById('sort').value='code';document.getElementById('list').submit()"><fmt:message key="code" /></a>
			    	        </td>
			    	        <td><img src="../graphics/up.gif" border="0"></td>
			    	      </c:when>
			    	      <c:when test="${subscriptionSearch.sort == 'code'}">
			    	        <td class="listingsHdr3">
			    	        <a class="hdrLink" href="#" onClick="document.getElementById('sort').value='code DESC';document.getElementById('list').submit()"><fmt:message key="code" /></a>
			    	        </td>
			    	        <td><img src="../graphics/down.gif" border="0"></td>
			    	      </c:when>
			    	      <c:otherwise>
			    	        <td colspan="2" class="listingsHdr3">
			    	        <a class="hdrLink" href="#" onClick="document.getElementById('sort').value='code DESC';document.getElementById('list').submit()"><fmt:message key="code" /></a>
			    	        </td>
			    	      </c:otherwise>
			    	    </c:choose>  
			    	  </tr>
			    	</table>
			    </td>
			    <td>
			    	<table cellspacing="0" cellpadding="1">
			    	  <tr>
			    	    <c:choose>
			    	      <c:when test="${subscriptionSearch.sort == 'username DESC'}">
			    	        <td class="listingsHdr3">
			    	        <a class="hdrLink" href="#" onClick="document.getElementById('sort').value='username';document.getElementById('list').submit()"><fmt:message key="emailAddress" /></a>
			    	        </td>
			    	        <td><img src="../graphics/up.gif" border="0"></td>
			    	      </c:when>
			    	      <c:when test="${subscriptionSearch.sort == 'username'}">
			    	        <td class="listingsHdr3">
			    	        <a class="hdrLink" href="#" onClick="document.getElementById('sort').value='username DESC';document.getElementById('list').submit()"><fmt:message key="emailAddress" /></a>
			    	        </td>
			    	        <td><img src="../graphics/down.gif" border="0"></td>
			    	      </c:when>
			    	      <c:otherwise>
			    	        <td colspan="2" class="listingsHdr3">
			    	        <a class="hdrLink" href="#" onClick="document.getElementById('sort').value='username DESC';document.getElementById('list').submit()"><fmt:message key="emailAddress" /></a>
			    	        </td>
			    	      </c:otherwise>
			    	    </c:choose>  
			    	  </tr>
			    	</table>
			    </td>
			    <td align="center">
			    	<table cellspacing="0" cellpadding="1">
			    	  <tr>
			    	    <c:choose>
			    	      <c:when test="${subscriptionSearch.sort == 'created DESC'}">
			    	        <td class="listingsHdr3">
			    	        <a class="hdrLink" href="#" onClick="document.getElementById('sort').value='created';document.getElementById('list').submit()"><fmt:message key="dateAdded" /></a>
			    	        </td>
			    	        <td><img src="../graphics/up.gif" border="0"></td>
			    	      </c:when>
			    	      <c:when test="${subscriptionSearch.sort == 'created'}">
			    	        <td class="listingsHdr3">
			    	        <a class="hdrLink" href="#" onClick="document.getElementById('sort').value='created DESC';document.getElementById('list').submit()"><fmt:message key="dateAdded" /></a>
			    	        </td>
			    	        <td><img src="../graphics/down.gif" border="0"></td>
			    	      </c:when>
			    	      <c:otherwise>
			    	        <td colspan="2" class="listingsHdr3">
			    	        <a class="hdrLink" href="#" onClick="document.getElementById('sort').value='created DESC';document.getElementById('list').submit()"><fmt:message key="dateAdded" /></a>
			    	        </td>
			    	      </c:otherwise>
			    	    </c:choose>  
			    	  </tr>
			    	</table>
			    </td>
			    <td align="center">
			    	<table cellspacing="0" cellpadding="1">
			    	  <tr>
			    	    <c:choose>
			    	      <c:when test="${subscriptionSearch.sort == 'product_sku DESC'}">
			    	        <td class="listingsHdr3">
			    	        <a class="hdrLink" href="#" onClick="document.getElementById('sort').value='product_sku';document.getElementById('list').submit()"><fmt:message key="productSku" /></a>
			    	        </td>
			    	        <td><img src="../graphics/up.gif" border="0"></td>
			    	      </c:when>
			    	      <c:when test="${subscriptionSearch.sort == 'product_sku'}">
			    	        <td class="listingsHdr3">
			    	        <a class="hdrLink" href="#" onClick="document.getElementById('sort').value='product_sku DESC';document.getElementById('list').submit()"><fmt:message key="productSku" /></a>
			    	        </td>
			    	        <td><img src="../graphics/down.gif" border="0"></td>
			    	      </c:when>
			    	      <c:otherwise>
			    	        <td colspan="2" class="listingsHdr3">
			    	        <a class="hdrLink" href="#" onClick="document.getElementById('sort').value='product_sku DESC';document.getElementById('list').submit()"><fmt:message key="productSku" /></a>
			    	        </td>
			    	      </c:otherwise>
			    	    </c:choose>  
			    	  </tr>
			    	</table>
			    </td>
			    <td class="listingsHdr3"><fmt:message key="description" /></td>
			    <td align="center" class="listingsHdr3"><fmt:message key="qty" /></td>
			    <td align="center" class="listingsHdr3"><fmt:message key="frequency" /></td>
			    <td align="center">
			    	<table cellspacing="0" cellpadding="1">
			    	  <tr>
			    	    <c:choose>
			    	      <c:when test="${subscriptionSearch.sort == 'last_order DESC'}">
			    	        <td class="listingsHdr3">
			    	        <a class="hdrLink" href="#" onClick="document.getElementById('sort').value='last_order';document.getElementById('list').submit()"><fmt:message key="lastOrder" /></a>
			    	        </td>
			    	        <td><img src="../graphics/up.gif" border="0"></td>
			    	      </c:when>
			    	      <c:when test="${subscriptionSearch.sort == 'last_order'}">
			    	        <td class="listingsHdr3">
			    	        <a class="hdrLink" href="#" onClick="document.getElementById('sort').value='last_order DESC';document.getElementById('list').submit()"><fmt:message key="lastOrder" /></a>
			    	        </td>
			    	        <td><img src="../graphics/down.gif" border="0"></td>
			    	      </c:when>
			    	      <c:otherwise>
			    	        <td colspan="2" class="listingsHdr3">
			    	        <a class="hdrLink" href="#" onClick="document.getElementById('sort').value='last_order DESC';document.getElementById('list').submit()"><fmt:message key="lastOrder" /></a>
			    	        </td>
			    	      </c:otherwise>
			    	    </c:choose>  
			    	  </tr>
			    	</table>
			    </td>
			    <td align="center">
			    	<table cellspacing="0" cellpadding="1">
			    	  <tr>
			    	    <c:choose>
			    	      <c:when test="${subscriptionSearch.sort == 'next_order DESC, username'}">
			    	        <td class="listingsHdr3">
			    	        <a class="hdrLink" href="#" onClick="document.getElementById('sort').value='next_order, username';document.getElementById('list').submit()"><fmt:message key="nextOrder" /></a>
			    	        </td>
			    	        <td><img src="../graphics/up.gif" border="0"></td>
			    	      </c:when>
			    	      <c:when test="${subscriptionSearch.sort == 'next_order, username'}">
			    	        <td class="listingsHdr3">
			    	        <a class="hdrLink" href="#" onClick="document.getElementById('sort').value='next_order DESC, username';document.getElementById('list').submit()"><fmt:message key="nextOrder" /></a>
			    	        </td>
			    	        <td><img src="../graphics/down.gif" border="0"></td>
			    	      </c:when>
			    	      <c:otherwise>
			    	        <td colspan="2" class="listingsHdr3">
			    	        <a class="hdrLink" href="#" onClick="document.getElementById('sort').value='next_order DESC, username';document.getElementById('list').submit()"><fmt:message key="nextOrder" /></a>
			    	        </td>
			    	      </c:otherwise>
			    	    </c:choose>  
			    	  </tr>
			    	</table>
			    </td>
			  </tr>
			<c:forEach items="${model.list.pageList}" var="item" varStatus="status">
			  <tr class="row${status.index % 2}" onmouseover="changeStyleClass(this,'rowOver')" onmouseout="changeStyleClass(this,'row${status.index % 2}')">
			    <td class="indexCol"><c:out value="${status.count + model.list.firstElementOnPage}"/>.</td>
				<td align="center"><c:if test="${item.enabled}"><img src="../graphics/checkbox.png"></c:if></td>
			    <td class="nameCol"><a href="subscription.jhtm?code=${item.code}" class="nameLink"><c:out value="${item.code}"/></a></td>			
			    <td class="nameCol"><c:out value="${item.customer.username}"/></td>
				<td align="center"><fmt:formatDate type="date" timeStyle="default" value="${item.created}"/></td>
			    <td align="center"><c:out value="${item.product.sku}"/></td>			
			    <td class="nameCol"><c:out value="${item.product.name}"/></td>			
				<td align="center"><c:out value="${item.quantity}"/></td>
				<td align="center">
				  <c:choose>
				    <c:when test="${item.intervalUnit != 1}"><fmt:message key="every_${item.intervalType}s"><fmt:param value="${item.intervalUnit}"/></fmt:message></c:when>
				    <c:otherwise><fmt:message key="every_${item.intervalType}"><fmt:param value="${item.intervalUnit}"/></fmt:message></c:otherwise>
				  </c:choose>
				</td>
				<td align="center"><fmt:formatDate type="date" timeStyle="default" value="${item.lastOrder}"/></td>
				<td align="center"><fmt:formatDate type="date" timeStyle="default" value="${item.nextOrder}"/></td>
			  </tr>
			</c:forEach>
			<c:if test="${model.list.nrOfElements == 0}">
			  <tr class="emptyList"><td colspan="6">&nbsp;</td></tr>
			</c:if>
			</table>
				  
			<table border="0" cellpadding="0" cellspacing="1" width="100%">
			  <tr>
			  <td>&nbsp;</td>  
			  <td class="pageSize">
			  <select name="size" onchange="document.getElementById('page').value=1;submit()">
			    <c:forTokens items="10,25,50,100" delims="," var="current">
			  	  <option value="${current}" <c:if test="${current == model.list.pageSize}">selected</c:if>>${current} <fmt:message key="perPage" /></option>
			    </c:forTokens>
			  </select>
			  </td>
			  </tr>
			</table>	  
		    <!-- end input field -->  	  
		  	</div>
		  	
		  	<!-- start button -->
		  	<!-- end button -->	
	
  	
	<!-- end tab -->        
	</div>
<!-- end tabs -->			
</div>
  
  <!-- end table -->
   </td><td class="boxmidr" ></td></tr>
  <tr><td class="botl"></td><td class="botr"></td></tr>
  </table>
  
<!-- end main box -->  
</div>	  
</form>	

  </tiles:putAttribute>    
</tiles:insertDefinition>