<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="/WEB-INF/tlds/spring.tld" prefix="spring" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>

<c:if test="${gSiteConfig['gSUBSCRIPTION']}">
<tiles:insertDefinition name="admin.orders" flush="true">
  <tiles:putAttribute name="content" type="string">
<script language="JavaScript" type="text/JavaScript">
<!--
window.addEvent('domready', function(){			
			var tabs1 = new SimpleTabs($('tab-block-1'), { selector: 'h4'});
			var Tips1 = new Tips($$('.toolTipImg'));
		});
//-->
</script>	
<link rel="stylesheet" type="text/css" media="all" href="../../admin/javascript/jscalendar-1.0/skins/aqua/theme.css" title="Aqua" /> <!-- calendar stylesheet -->
<script type="text/javascript" src="../../admin/javascript/jscalendar-1.0/calendar.js"></script> <!-- main calendar program -->
<script type="text/javascript" src="../../admin/javascript/jscalendar-1.0/lang/calendar-en.js"></script>
<script type="text/javascript" src="../../admin/javascript/jscalendar-1.0/calendar-setup.js"></script>

<!-- main box -->
  <div id="mboxfull">
<form:form commandName="subscription" method="post">  
  <!-- start table -->
  <table cellpadding="0" cellspacing="0" border="0" class="module" >
  <tr>
    <td class="topl_g">
	  
	  <!-- breadcrumb -->
	  <p class="breadcrumb">
	    <a href="../orders"><fmt:message key="orders" /></a> &gt;
	    <a href="subscriptionsList.jhtm"><fmt:message key="subscriptions" /></a>
	  </p>
	  
	  <!-- Error Message -->
	  <spring:hasBindErrors name="serviceForm">
		<div class="error"><fmt:message key='form.hasErrors'/></div>
	  </spring:hasBindErrors>

	  <div align="right">
		<table class="form">
		  <tr>
		    <td align="right"><fmt:message key="dateAdded" /> : </td>
		    <td><fmt:formatDate type="both" timeStyle="full" value="${subscription.created}"/></td>
		  </tr>
		  <tr>
		    <td align="right"><fmt:message key="lastModified" /> : </td>
		    <td><fmt:formatDate type="both" timeStyle="full" value="${subscription.lastModified}"/></td>
		  </tr>
		</table>
	  </div>
  
  </td><td class="topr_g" ></td></tr>
  <tr><td class="boxmidlrg" >

 <!-- tabs -->
 <div id="tab-block-1">
	<h4 title="<fmt:message key='subscription' />"><fmt:message key='subscription' /></h4>
	<div>
	<!-- start tab -->

        <c:set var="classIndex" value="0" />
        
	  	<div class="listdivi ln tabdivi"></div>
	  	<div class="listdivi"></div>

		  	<div class="list${classIndex % 2}"><c:set var="classIndex" value="${classIndex+1}" />
		  	<div class="listfl"><fmt:message key='code' />:</div>
		  	<div class="listp">
		  	<!-- input field -->
	  			<c:out value="${subscription.code}"/>
		    <!-- end input field -->   	
		  	</div>
		  	</div>

		  	<div class="list${classIndex % 2}"><c:set var="classIndex" value="${classIndex+1}" />
		  	<div class="listfl"><fmt:message key='company' />:</div>
		  	<div class="listp">
		  	<!-- input field -->
	  			<c:out value="${subscription.customer.address.company}"/>
		    <!-- end input field -->   	
		  	</div>
		  	</div>
	
		  	<div class="list${classIndex % 2}"><c:set var="classIndex" value="${classIndex+1}" />
		  	<div class="listfl"><fmt:message key='firstName' />:</div>
		  	<div class="listp">
		  	<!-- input field -->
	  			<c:out value="${subscription.customer.address.firstName}"/>
		    <!-- end input field -->   	
		  	</div>
		  	</div>

		  	<div class="list${classIndex % 2}"><c:set var="classIndex" value="${classIndex+1}" />
		  	<div class="listfl"><fmt:message key='lastName' />:</div>
		  	<div class="listp">
		  	<!-- input field -->
	  			<c:out value="${subscription.customer.address.lastName}"/>
		    <!-- end input field -->   	
		  	</div>
		  	</div>

		  	<div class="list${classIndex % 2}"><c:set var="classIndex" value="${classIndex+1}" />
		  	<div class="listfl"><fmt:message key='emailAddress' />:</div>
		  	<div class="listp">
		  	<!-- input field -->
	  			<c:out value="${subscription.customer.username}"/>
		    <!-- end input field -->   	
		  	</div>
		  	</div>

		  	<div class="list${classIndex % 2}"><c:set var="classIndex" value="${classIndex+1}" />
		  	<div class="listfl"><fmt:message key="sku" />:</div>
		  	<div class="listp">
		  	<!-- input field -->
	  			<c:out value="${subscription.product.sku}"/>
		    <!-- end input field -->   	
		  	</div>
		  	</div>

		  	<div class="list${classIndex % 2}"><c:set var="classIndex" value="${classIndex+1}" />
		  	<div class="listfl"><fmt:message key="description" />:</div>
		  	<div class="listp">
		  	<!-- input field -->
	  			<c:out value="${subscription.product.name}"/>
	  			<c:forEach items="${subscription.productAttributes}" var="productAttribute">
			      <div>- <c:out value="${productAttribute.optionName}"/>: <c:out value="${productAttribute.valueName}"/></div>
			   </c:forEach>
		    <!-- end input field -->   	
		  	</div>
		  	</div>
		  	
		  	<div class="list${classIndex % 2}"><c:set var="classIndex" value="${classIndex+1}" />
		  	<div class="listfl"><fmt:message key="taxable" />:</div>
		  	<div class="listp">
		  	<!-- input field -->
	  			<form:checkbox path="product.taxable"/>
		    <!-- end input field -->   	
		  	</div>
		  	</div>

		  	<div class="list${classIndex % 2}"><c:set var="classIndex" value="${classIndex+1}" />
		  	<div class="listfl"><fmt:message key="unitPrice" />:</div>
		  	<div class="listp">
		  	<!-- input field -->
	  			<form:input path="unitPrice" size="8" maxlength="8"/>
	  			<form:errors path="unitPrice" cssClass="error"/>
		    <!-- end input field -->   	
		  	</div>
		  	</div>	  	
		  	
		  	<div class="list${classIndex % 2}"><c:set var="classIndex" value="${classIndex+1}" />
		  	<div class="listfl"><fmt:message key="quantity" />:</div>
		  	<div class="listp">
		  	<!-- input field -->
	  			<form:input path="quantity" size="5" maxlength="10"/>
	  			<form:errors path="quantity" cssClass="error"/>
		    <!-- end input field -->   	
		  	</div>
		  	</div>
        
		  	<div class="list${classIndex % 2}"><c:set var="classIndex" value="${classIndex+1}" />
		  	<div class="listfl"><fmt:message key="frequency" />:</div>
		  	<div class="listp">
		  	<!-- input field -->
	  			<form:select path="intervalUnit">
		          <form:option value="1"><fmt:message key="every_m"><fmt:param value="1"/></fmt:message></form:option>
		          <form:option value="2"><fmt:message key="every_ms"><fmt:param value="2"/></fmt:message></form:option>
		          <form:option value="3"><fmt:message key="every_ms"><fmt:param value="3"/></fmt:message></form:option>
		        </form:select>
		    <!-- end input field -->
		  	</div>
		  	</div>

		  	<div class="list${classIndex % 2}"><c:set var="classIndex" value="${classIndex+1}" />
		  	<div class="listfl"><fmt:message key="lastOrder" />:</div>
		  	<div class="listp"><fmt:formatDate type="date" timeStyle="default" value="${subscription.lastOrder}"/>
		  	</div>
		  	</div>
		  	
		  	<c:if test="${subscription.orderIdLast != null}">
		  	<div class="list${classIndex % 2}"><c:set var="classIndex" value="${classIndex+1}" />
		  	<div class="listfl"><fmt:message key="orderIdLast"/>:</div>
		  	<div class="listp">
		  	<!-- input field -->
	   			<a href="invoice.jhtm?order=${subscription.orderIdLast}"><c:out value="${subscription.orderIdLast}"/></a>
		    <!-- end input field -->   	
		  	</div>
		  	</div>
		  	</c:if>  	

		  	<div class="list${classIndex % 2}"><c:set var="classIndex" value="${classIndex+1}" />
		  	<div class="listfl"><fmt:message key="nextOrder" />:</div>
		  	<div class="listp">
		  	<!-- input field -->
	  			<form:input path="nextOrder" size="11" maxlength="10" />
				  <img class="calendarImage"  id="next_order_trigger" src="../graphics/calendarIcon.jpg" />
				  <script type="text/javascript">
		    		Calendar.setup({
		        			inputField     :    "nextOrder",   // id of the input field
		        			showsTime      :    false,
		        			ifFormat       :    "%m/%d/%Y",   // format of the input field
		        			button         :    "next_order_trigger"   // trigger for the calendar (button ID)
		    		});	
				  </script> 
				<form:errors path="nextOrder" cssClass="error"/>
		    <!-- end input field -->   	
		  	</div>
		  	</div>
		  	
		  	<div class="list${classIndex % 2}"><c:set var="classIndex" value="${classIndex+1}" />
		  	<div class="listfl"><fmt:message key="Enabled" />:</div>
		  	<div class="listp">
		  	<!-- input field -->
	  			<form:checkbox path="enabled"/>
		    <!-- end input field -->   	
		  	</div>
		  	</div>
		  	
		  	<c:if test="${subscription.orderIdOrig != null}">
		  	<div class="list${classIndex % 2}"><c:set var="classIndex" value="${classIndex+1}" />
		  	<div class="listfl"><fmt:message key="orderIdOrig"/>:</div>
		  	<div class="listp">
		  	<!-- input field -->
	   			<a href="invoice.jhtm?order=${subscription.orderIdOrig}"><c:out value="${subscription.orderIdOrig}"/></a>
		    <!-- end input field -->   	
		  	</div>
		  	</div>
		  	</c:if>  	
	  	
	<!-- end tab -->        
	</div> 
	

<!-- end tabs -->			
</div>

<!-- start button -->
<div align="left" class="button"> 
  <input type="submit" value="<fmt:message key="update"/>">
  <input type="submit" value="<fmt:message key="cancel"/>" name="_cancel">
  <input type="submit" value="<spring:message code="delete"/>" name="_delete" onClick="return confirm('Delete permanently?')">  
</div>
<!-- end button -->	     
  
  <!-- end table -->
    </td><td class="boxmidr" ></td></tr>
  <tr><td class="botl"></td><td class="botr"></td></tr>
  </table>

</form:form>
  
<!-- end main box -->  
</div>
 
  </tiles:putAttribute>    
</tiles:insertDefinition>
</c:if>