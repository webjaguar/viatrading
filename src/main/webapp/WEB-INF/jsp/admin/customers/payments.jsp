<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ taglib uri="http://www.springframework.org/security/tags" prefix="sec" %>

<c:if test="${gSiteConfig['gPAYMENTS']}">
<tiles:insertDefinition name="admin.customers.inComing" flush="true">
  <tiles:putAttribute name="content" type="string">
  <sec:authorize ifAnyGranted="ROLE_AEM,ROLE_ADMIN_${prop_site_id},ROLE_INCOMING_PAYMENT_VIEW_LIST">   
<script language="JavaScript">
<!--
function toggleAll(el) {
	var ids = document.getElementsByName("__selected_paymentId");	
  	if (ids.length == 1)
      document.list.__selected_paymentId.checked = el.checked;
    else
      for (var i = 0; i < ids.length; i++)
        document.list.__selected_paymentId[i].checked = el.checked;	
} 
function cancelSelected() {
	var ids = document.getElementsByName("__selected_paymentId");	
  	if (ids.length == 1) {
      if (document.list.__selected_paymentId.checked) {
    	return confirm('Are you sure you want to cancle selected payments?');
      }
    } else {
      for (var i = 0; i < ids.length; i++) {
        if (document.list.__selected_paymentId[i].checked) {
    	  return confirm('Are you sure you want to cancle selected payments?');
        }
      }
    }

    alert("Please select payment(s) to cancel.");       
    return false;
}

//-->
</script>

<form action="payments.jhtm" id="list" method="post" name="list">
<input type="hidden" id="sort" name="sort" value="${paymentSearch.sort}" />
<!-- main box -->
  <div id="mboxfull">
  
  <!-- start table -->
  <table cellpadding="0" cellspacing="0" border="0" class="module" >
  <tr>
    <td class="topl_g">
	  
	  <!-- breadcrumb -->
	  <p class="breadcrumb">
	    <a href="../customers"><fmt:message key="customer"/></a> &gt;
	    <fmt:message key="incomingPayments"/> 
	  </p>
	  
	  <!-- Error Message -->
	  
	  <!-- header image -->
	  <img class="headerImage" src="../graphics/payment.gif" />
	  
	  </td><td class="topr_g" ></td></tr>
  <tr><td class="boxmidlrg" >

 <!-- tabs -->
 <div class="tab-wrapper">
	<h4 title="List of affiliates"></h4>
	<div>
	<!-- start tab -->
	<p>
	  	<div class="listdivi ln tabdivi"></div>
	  	<div class="listdivi"></div>
	   
		  	<div class="listlight">
		  	<!-- input field --> 
		  	<table border="0" cellpadding="0" cellspacing="1" width="100%">
			  <tr>
			  <td class="pageShowing">
			  <c:if test="${model.payments.nrOfElements > 0}">
			  <fmt:message key="showing">
				<fmt:param value="${model.payments.firstElementOnPage + 1}"/>
				<fmt:param value="${model.payments.lastElementOnPage + 1}"/>
				<fmt:param value="${model.payments.nrOfElements}"/>
			  </fmt:message>
			  </c:if>
			  </td>
			  <td class="pageNavi">
			  Page 
			  <select name="page" id="page" onchange="submit()">
			  <c:forEach begin="1" end="${model.payments.pageCount}" var="page">
			  	<option value="${page}" <c:if test="${page == (model.payments.page+1)}">selected</c:if>>${page}</option>
			  </c:forEach>
			  </select>
			  of <c:out value="${model.payments.pageCount}"/>
			  | 
			  <c:if test="${model.payments.firstPage}"><span class="pageNaviDead"><fmt:message key="previous" /></span></c:if>
			  <c:if test="${not model.payments.firstPage}"><a href="<c:url value="payments.jhtm"><c:param name="page" value="${model.payments.page}"/></c:url>" class="pageNaviLink"><fmt:message key="previous" /></a></c:if>
			  | 
			  <c:if test="${model.payments.lastPage}"><span class="pageNaviDead"><fmt:message key="next" /></span></c:if>
			  <c:if test="${not model.payments.lastPage}"><a href="<c:url value="payments.jhtm"><c:param name="page" value="${model.payments.page+2}"/></c:url>" class="pageNaviLink"><fmt:message key="next" /></a></c:if>
			  </td>
			  </tr>
			</table>
			
			<table border="0" cellpadding="0" cellspacing="0" width="100%" class="listings">
			  <tr class="listingsHdr2">
			    <td class="indexCol">&nbsp;</td>			    
			  	<sec:authorize ifAnyGranted="ROLE_AEM,ROLE_ADMIN_${prop_site_id},ROLE_INCOMING_PAYMENT_CANCEL">
			    <c:if test="${paymentSearch.userId != null}">
			   		<td class="listingsHdr3"><input type="checkbox"  onClick="toggleAll(this)"></td>
			    </c:if>
			    </sec:authorize>
				<td class="listingsHdr3">
					<fmt:message key="memo" />
				</td>
			    <td align="right">
			    	<table cellspacing="0" cellpadding="1">
			    	  <tr>
			    	    <c:choose>
			    	      <c:when test="${paymentSearch.sort == 'payment_amount desc'}">
			    	        <td class="listingsHdr3">
			    	        <a class="hdrLink" href="#" onClick="document.getElementById('sort').value='payment_amount';document.getElementById('list').submit()"><fmt:message key="payment" /></a>
			    	        </td>
			    	        <td><img src="../graphics/up.gif" border="0"></td>
			    	      </c:when>
			    	      <c:when test="${paymentSearch.sort == 'payment_amount'}">
			    	        <td class="listingsHdr3">
			    	        <a class="hdrLink" href="#" onClick="document.getElementById('sort').value='payment_amount desc';document.getElementById('list').submit()"><fmt:message key="payment" /></a>
			    	        </td>
			    	        <td><img src="../graphics/down.gif" border="0"></td>
			    	      </c:when>
			    	      <c:otherwise>
			    	        <td rowspan="2" colspan="2" class="listingsHdr3">
			    	        <a class="hdrLink" href="#" onClick="document.getElementById('sort').value='payment_amount desc';document.getElementById('list').submit()"><fmt:message key="payment" /></a>
			    	        </td>
			    	      </c:otherwise>
			    	    </c:choose>
			    	  </tr>
			    	</table>
			    </td>
			    <td class="listingsHdr3" align="center">
					Added By
				</td>
			    </td>
			    <td align="center">
			    	<table cellspacing="0" cellpadding="1">
			    	  <tr>
			    	    <c:choose>
			    	      <c:when test="${paymentSearch.sort == 'payment_date DESC'}">
			    	        <td class="listingsHdr3">
			    	        <a class="hdrLink" href="#" onClick="document.getElementById('sort').value='payment_date';document.getElementById('list').submit()"><fmt:message key="date" /></a>
			    	        </td>
			    	        <td><img src="../graphics/up.gif" border="0"></td>
			    	      </c:when>
			    	      <c:when test="${paymentSearch.sort == 'payment_date'}">
			    	        <td class="listingsHdr3">
			    	        <a class="hdrLink" href="#" onClick="document.getElementById('sort').value='payment_date DESC';document.getElementById('list').submit()"><fmt:message key="date" /></a>
			    	        </td>
			    	        <td><img src="../graphics/down.gif" border="0"></td>
			    	      </c:when>
			    	      <c:otherwise>
			    	        <td rowspan="2" colspan="2" class="listingsHdr3">
			    	        <a class="hdrLink" href="#" onClick="document.getElementById('sort').value='payment_date DESC';document.getElementById('list').submit()"><fmt:message key="date" /></a>
			    	        </td>
			    	      </c:otherwise>
			    	    </c:choose>
			    	  </tr>
			    	</table>
			    </td>
			    <td>
			    	<table cellspacing="0" cellpadding="1">
			    	  <tr>
			    	    <c:choose>
			    	      <c:when test="${paymentSearch.sort == 'username desc'}">
			    	        <td class="listingsHdr3">
			    	        <a class="hdrLink" href="#" onClick="document.getElementById('sort').value='username';document.getElementById('list').submit()"><fmt:message key="emailAddress" /></a>
			    	        </td>
			    	        <td><img src="../graphics/up.gif" border="0"></td>
			    	      </c:when>
			    	      <c:when test="${paymentSearch.sort == 'username'}">
			    	        <td class="listingsHdr3">
			    	        <a class="hdrLink" href="#" onClick="document.getElementById('sort').value='username desc';document.getElementById('list').submit()"><fmt:message key="emailAddress" /></a>
			    	        </td>
			    	        <td><img src="../graphics/down.gif" border="0"></td>
			    	      </c:when>
			    	      <c:otherwise>
			    	        <td rowspan="2" colspan="2" class="listingsHdr3">
			    	        <a class="hdrLink" href="#" onClick="document.getElementById('sort').value='username';document.getElementById('list').submit()"><fmt:message key="emailAddress" /></a>
			    	        </td>
			    	      </c:otherwise>
			    	    </c:choose>
			    	  </tr>
			    	</table>
			    </td>
			    <td>
			    	<table cellspacing="0" cellpadding="1">
			    	  <tr>
			    	    <c:choose>
			    	      <c:when test="${paymentSearch.sort == 'total_orders_payment_amount desc'}">
			    	        <td class="listingsHdr3">
			    	        <a class="hdrLink" href="#" onClick="document.getElementById('sort').value='total_orders_payment_amount';document.getElementById('list').submit()"><fmt:message key="balance" /></a>
			    	        </td>
			    	        <td><img src="../graphics/up.gif" border="0"></td>
			    	      </c:when>
			    	      <c:when test="${paymentSearch.sort == 'total_orders_payment_amount'}">
			    	        <td class="listingsHdr3">
			    	        <a class="hdrLink" href="#" onClick="document.getElementById('sort').value='total_orders_payment_amount desc';document.getElementById('list').submit()"><fmt:message key="balance" /></a>
			    	        </td>
			    	        <td><img src="../graphics/down.gif" border="0"></td>
			    	      </c:when>
			    	      <c:otherwise>
			    	        <td rowspan="2" colspan="2" class="listingsHdr3">
			    	        <a class="hdrLink" href="#" onClick="document.getElementById('sort').value='total_orders_payment_amount';document.getElementById('list').submit()"><fmt:message key="balance" /></a>
			    	        </td>
			    	      </c:otherwise>
			    	    </c:choose>
			    	  </tr>
			    	</table>
			    </td>
			    <td>
			    	<table cellspacing="0" cellpadding="1">
			    	  <tr>
			    	    <c:choose>
			    	      <c:when test="${paymentSearch.sort == 'company desc'}">
			    	        <td class="listingsHdr3">
			    	        <a class="hdrLink" href="#" onClick="document.getElementById('sort').value='company';document.getElementById('list').submit()"><fmt:message key="company" /></a>
			    	        </td>
			    	        <td><img src="../graphics/up.gif" border="0"></td>
			    	      </c:when>
			    	      <c:when test="${paymentSearch.sort == 'company'}">
			    	        <td class="listingsHdr3">
			    	        <a class="hdrLink" href="#" onClick="document.getElementById('sort').value='company desc';document.getElementById('list').submit()"><fmt:message key="company" /></a>
			    	        </td>
			    	        <td><img src="../graphics/down.gif" border="0"></td>
			    	      </c:when>
			    	      <c:otherwise>
			    	        <td rowspan="2" colspan="2" class="listingsHdr3">
			    	        <a class="hdrLink" href="#" onClick="document.getElementById('sort').value='company';document.getElementById('list').submit()"><fmt:message key="company" /></a>
			    	        </td>
			    	      </c:otherwise>
			    	    </c:choose>
			    	  </tr>
			    	</table>
			    </td>
			    <td>
			    	<table cellspacing="0" cellpadding="1">
			    	  <tr>
			    	    <c:choose>
			    	      <c:when test="${paymentSearch.sort == 'last_name, first_name'}">
			    	        <td class="listingsHdr3">
			    	        <a class="hdrLink" href="#" onClick="document.getElementById('sort').value='last_name, first_name';document.getElementById('list').submit()"><fmt:message key="lastName" /></a>
			    	        </td>
			    	        <td><img src="../graphics/down.gif" border="0"></td>
			    	      </c:when>
			    	      <c:otherwise>
			    	        <td rowspan="2" colspan="2" class="listingsHdr3">
			    	        <a class="hdrLink" href="#" onClick="document.getElementById('sort').value='last_name, first_name';document.getElementById('list').submit()"><fmt:message key="lastName" /></a>
			    	        </td>
			    	      </c:otherwise>
			    	    </c:choose>
			    	  </tr>
			    	</table>
			    </td>
			    <td>
			    	<table cellspacing="0" cellpadding="1">
			    	  <tr>
			    	    <c:choose>
			    	      <c:when test="${paymentSearch.sort == 'first_name, last_name'}">
			    	        <td class="listingsHdr3">
			    	        <a class="hdrLink" href="#" onClick="document.getElementById('sort').value='first_name, last_name';document.getElementById('list').submit()"><fmt:message key="lastName" /></a>
			    	        </td>
			    	        <td><img src="../graphics/down.gif" border="0"></td>
			    	      </c:when>
			    	      <c:otherwise>
			    	        <td rowspan="2" colspan="2" class="listingsHdr3">
			    	        <a class="hdrLink" href="#" onClick="document.getElementById('sort').value='first_name, last_name';document.getElementById('list').submit()"><fmt:message key="firstName" /></a>
			    	        </td>
			    	      </c:otherwise>
			    	    </c:choose>
			    	  </tr>
			    	</table>
			    </td>  
			  </tr>
			<c:forEach items="${model.payments.pageList}" var="payment" varStatus="status">
			  <tr class="row${status.index % 2}" onmouseover="changeStyleClass(this,'rowOver')" onmouseout="changeStyleClass(this,'row${status.index % 2}')">			    
			    <td class="indexCol"><c:out value="${status.count + model.payments.firstElementOnPage}"/>.</td>
			    <sec:authorize ifAnyGranted="ROLE_AEM,ROLE_ADMIN_${prop_site_id},ROLE_INCOMING_PAYMENT_CANCEL">
			    <c:if test="${paymentSearch.userId != null}">
			    <td align="left">
			    	<c:choose>
				    <c:when test="${payment.cancelPayment == 'true'}">
				    	<input value="${payment.id}" type="checkbox" disabled="disabled">
				    </c:when>
				    <c:otherwise>
				    	<input name="__selected_paymentId" value="${payment.id}" type="checkbox">
				    </c:otherwise>
			       </c:choose>
			   	</td>
			   	</c:if>
			   	</sec:authorize>	
			    <c:choose>
				    <c:when test="${!payment.cancelPayment}">
					    <td class="nameCol">
					    	<a href="payment.jhtm?id=${payment.id}&cid=${payment.customer.id}" class="nameLink"><c:if test="${payment.paymentMethod != ''}"><c:out value="${payment.paymentMethod}"/> </c:if><c:out value="${payment.memo}"/></a>			
					    </td>
				    </c:when>
				    <c:otherwise>
					    <td class="nameCol">
					       	<a href="payment.jhtm?id=${payment.id}&cid=${payment.customer.id}" class="cancelLink"><c:if test="${payment.paymentMethod != ''}"><c:out value="${payment.paymentMethod}"/> </c:if><c:out value="${payment.memo}"/></a>			
					    </td>
				    </c:otherwise>
			    </c:choose>
				<td align="right"><fmt:formatNumber value="${payment.amount}" pattern="#,##0.00" /></td>
			    <td align="center"><c:out value="${payment.addedBy}"/></td>
			    <td align="center"><fmt:formatDate type="date" timeStyle="default" value="${payment.date}"/></td>
			    <td class="nameCol"><c:out value="${payment.customer.username}"/></td>
			    <td class="nameCol"><c:out value="${payment.amount - payment.totalOrdersPaymentAmout}"/></td>
			    <td class="nameCol"><c:out value="${payment.customer.address.company}"/></td>
			    <td class="nameCol"><c:out value="${payment.customer.address.lastName}"/></td>
			    <td class="nameCol"><c:out value="${payment.customer.address.firstName}"/></td>  
			  </tr>
			</c:forEach>
			<c:if test="${model.payments.nrOfElements == 0}">
			  <tr class="emptyList"><td colspan="6">&nbsp;</td></tr>
			</c:if>
			</table>
				  
			<table border="0" cellpadding="0" cellspacing="1" width="100%">
			  <tr>
			  <td class="pageSize">
			  <select name="size" onchange="document.getElementById('page').value=1;submit()">
			    <c:forTokens items="10,25,50,100" delims="," var="current">
			  	  <option value="${current}" <c:if test="${current == model.payments.pageSize}">selected</c:if>>${current} <fmt:message key="perPage" /></option>
			    </c:forTokens>
			  </select>
			  </td>
			  </tr>
			</table>	  
		  	<!-- end input field -->  	  
		  	</div>
		  	
		  	<!-- start button -->
		  	<c:if test="${paymentSearch.userId != null}">
			    <input type="hidden" name="cid" value="${paymentSearch.userId}">
			</c:if>     
		  	<div align="left" class="button">
		  	  <c:if test="${paymentSearch.userId != null}">
		  	  <sec:authorize ifAnyGranted="ROLE_AEM,ROLE_ADMIN_${prop_site_id},ROLE_INCOMING_PAYMENT_CREATE">
			    <input type="submit" name="__add" value="<fmt:message key="add" /> <fmt:message key="payment" />">
			  </sec:authorize>
			  <sec:authorize ifAnyGranted="ROLE_AEM,ROLE_ADMIN_${prop_site_id},ROLE_INCOMING_PAYMENT_CANCEL">
			    <input type="submit" name="__cancel" value="<fmt:message key="cancel" /> <fmt:message key="payment" />" onClick="return cancelSelected()">
			  </sec:authorize>
			  </c:if>
			  <c:if test="${paymentSearch.userId == null}">
			    <div class="sup">* To add/cancel a payment go to customer list.</div>
			  </c:if>
		  	</div>
		  	<!-- end button -->	
	  	
	<!-- end tab -->        
	</div>
<!-- end tabs -->			
</div>
  
  <!-- end table -->
   </td><td class="boxmidr" ></td></tr>
  <tr><td class="botl"></td><td class="botr"></td></tr>
  </table>
  
<!-- end main box -->  
</div>
</form>	 
  
  </sec:authorize>
  </tiles:putAttribute>    
</tiles:insertDefinition>
</c:if>
