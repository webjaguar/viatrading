<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<tiles:insertDefinition name="admin.customers" flush="true">
  <tiles:putAttribute name="content" type="string">
<script type="text/javascript" >
<!--
window.addEvent('domready', function(){			
			var tabs1 = new SimpleTabs($('tab-block-1'), { selector: 'h4'});
		});
//-->	
</script>   
  
<form action="partnerCustomerForm.jhtm" id="list" method="post">
<!-- main box -->
  <div id="mboxfull">
  
  <!-- start table -->
  <table cellpadding="0" cellspacing="0" border="0" class="module" >
  <tr>
    <td class="topl_g">
	  
	  <!-- breadcrumb -->
	  <p class="breadcrumb">
	    <a href="../customers">Customers</a> &gt;
	    <a href="../customers/partnerCustomerList.jhtm?cId=${customer.id}">Partner List</a> &gt; <fmt:message key="partnerFor" />
	    <c:out value="${customer.username}"/>
	  </p>
	  
	  <!-- Error -->
	  <c:if test="${!empty message}">
		<div class="message"><fmt:message key="${message}" /></div>
	  </c:if>  
	  
  </td><td class="topr_g" ></td></tr>
  <tr><td class="boxmidlrg" >
  
	
	
<!-- tabs -->
 <div id="tab-block-1">
 
	<h4 title="Add Language">Partner</h4>
	<div>
	<!-- start tab -->

        <c:set var="classIndex" value="0" />
        
	  	<div class="listdivi ln tabdivi"></div>
	  	<div class="listdivi"></div>
   		
		  	<div class="list${classIndex % 2}"><c:set var="classIndex" value="${classIndex+1}" />
			<div class="listfl"><fmt:message key="partnerName"/>:</div>
				<div class="listp">
				<!-- input field -->
			    <select name="partnerId" <c:if test="${update == 'true'}">disabled="disabled"</c:if>>
			        <option value="">Please Select</option>
					<c:forEach items="${partnerList}" var="partner">
						<option value="${partner.id}" <c:if test="${partner.id == pId}">selected</c:if>><c:out value="${partner.partnerName}"/></option>
					</c:forEach>						    
			    </select>
			<!-- end input field -->   	
			</div>
			</div>	
			
			<div class="list${classIndex % 2}"><c:set var="classIndex" value="${classIndex+1}" />
			<div class="listfl"><fmt:message key='amount' />:</div>
			<div class="listp">
			<!-- input field -->
	            <c:if test="${update == 'true'}">
					<input type="hidden" name="partnerId" value="${pId}"/>
			    </c:if>
				<input type="text" name="__partnerAmount" id="__partnerAmountId" value="${partnerAmt}"/>
				<input type="hidden" name="cId" value="${customer.id}"/>
			<!-- end input field -->   	
			</div>
			</div>	   
    </div>
 </div>
	<!-- start button -->
	<div align="left" class="button">
	<c:choose>
	<c:when test="${update == 'true'}">
		<input type="submit" name="__updatePartner" value="<fmt:message key="update" />">
	</c:when>
	<c:otherwise>
		<input type="submit" name="__assignPartner" value="<fmt:message key="add" />">
	</c:otherwise>
	</c:choose>
	</div>
	<!-- end button -->			 
	
	  <!-- end table --> 
	</td><td class="boxmidr" ></td>	</tr>	
	</table>

</div>
</form>
</tiles:putAttribute>
</tiles:insertDefinition>