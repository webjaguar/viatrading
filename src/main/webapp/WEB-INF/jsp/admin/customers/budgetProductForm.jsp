<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="/WEB-INF/tlds/spring.tld" prefix="spring" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>

<c:if test="${gSiteConfig['gBUDGET_PRODUCT']}">
<tiles:insertDefinition name="admin.customers" flush="true">
  <tiles:putAttribute name="content" type="string">
<script language="JavaScript" type="text/JavaScript">
<!--
window.addEvent('domready', function(){			
			var tabs1 = new SimpleTabs($('tab-block-1'), { selector: 'h4'});
			var Tips1 = new Tips($$('.toolTipImg'));
		});
//-->
</script>

<form:form commandName="budgetProduct" method="post">
<form:hidden path="userId" />
<!-- main box -->
  <div id="mboxfull">
  
  <!-- start table -->
  <table cellpadding="0" cellspacing="0" border="0" class="module" >
  <tr>
    <td class="topl_g">
	  
	  <!-- breadcrumb -->
	  <p class="breadcrumb">
	    <a href="../customers/budgetByProducts.jhtm?cid=${budgetProduct.userId}">Customer</a> &gt;
	    Budget &gt; <c:out value="${customer.username}"/>
	  </p>
	  
	  <!-- Error Message -->
	  <spring:hasBindErrors name="budgetProduct">
		<div class="message">Please fix all errors!</div>
	  </spring:hasBindErrors>

  
  </td><td class="topr_g" ></td></tr>
  <tr><td class="boxmidlrg" >

 <!-- tabs -->
 <div id="tab-block-1">
	<h4 title="Description"><fmt:message key="budget" /></h4>
	<div>
	<!-- start tab -->

        <c:set var="classIndex" value="0" />
        
	  	<div class="listdivi ln tabdivi"></div>
	  	<div class="listdivi"></div>

		  	<div class="list${classIndex % 2}"><c:set var="classIndex" value="${classIndex+1}" />
		  	<div class="listfl"><div class="requiredField"><fmt:message key="sku" />:</div></div>
		  	<div class="listp">
		  	<!-- input field -->
				<form:input path="product.sku" maxlength="50" />
				<form:errors path="product.sku" cssClass="error"/>
		    <!-- end input field -->   	
		  	</div>
		  	</div>

		  	<div class="list${classIndex % 2}"><c:set var="classIndex" value="${classIndex+1}" />
		  	<div class="listfl"><div class="requiredField"><fmt:message key="dateEntered" />:</div></div>
		  	<div class="listp">
		  	<!-- input field -->
				<form:input path="entered" size="10" maxlength="10" />
				  <img class="calendarImage"  id="entered_trigger" src="../graphics/calendarIcon.jpg" />
				  <script type="text/javascript">
		    		Calendar.setup({
		        			inputField     :    "entered",   // id of the input field
		        			showsTime      :    false,
		        			ifFormat       :    "%m/%d/%Y",   // format of the input field
		        			button         :    "entered_trigger"   // trigger for the calendar (button ID)
		    		});	
				  </script> 
				<form:errors path="entered" cssClass="error"/>
		    <!-- end input field -->   	
		  	</div>
		  	</div>

		  	<div class="list${classIndex % 2}"><c:set var="classIndex" value="${classIndex+1}" />
		  	<div class="listfl"><div class="requiredField"><fmt:message key="quantity" />:</div></div>
		  	<div class="listp">
		  	<!-- input field -->
				<form:input path="qty" size="5" />
				<form:errors path="qty" cssClass="error"/>
		    <!-- end input field -->   	
		  	</div>
		  	</div>
		  	
	<!-- end tab -->        
	</div>         
	
<!-- end tabs -->			
</div>

<!-- start button -->
<div align="left" class="button"> 
<c:if test="${budgetProduct.id == null}">
  <input type="submit" value="<fmt:message key="add"/>">
</c:if>
<c:if test="${budgetProduct.id != null}">		
  <input type="submit" value="<fmt:message key="update"/>">
  <input type="submit" value="<fmt:message key="delete"/>" name="_delete">
</c:if>
  <input type="submit" value="<fmt:message key="cancel"/>" name="_cancel">
</div>
<!-- end button -->	     
  
  <!-- end table -->
    </td><td class="boxmidr" ></td></tr>
  <tr><td class="botl"></td><td class="botr"></td></tr>
  </table>
  
<!-- end main box -->  
</div>
</form:form>

  </tiles:putAttribute>    
</tiles:insertDefinition>
</c:if>