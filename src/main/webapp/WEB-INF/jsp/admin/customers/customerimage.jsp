<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>	
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
	
	<h4 title="manageShipToAddresses">Manage Shipping Addresses</h4>
	<div>
         <c:set var="classIndex" value="0" />
         
	  	<div class="listdivi ln tabdivi"></div>
	  	<div class="listdivi"></div>
	  	
	  	 <div class="list${classIndex % 2}"><c:set var="classIndex" value="${classIndex+1}" />
         <div class="listfl"><a href="customerAddressList.jhtm?type=add&userid=${customerForm.customer.id}"><fmt:message key="addAddress" /></a></div>
		 <div class="listp">
		 <c:out value="${customer.address.firstName}"/>  
		 </div>
		 </div>
	  	
	  	 <c:forEach items="${addresses}" var="address" varStatus="status">
	  	 <div class="list${classIndex % 2}"><c:set var="classIndex" value="${classIndex+1}" />
		 <div class="listfl"><a href="customerAddressList.jhtm?type=edit&id=${address.id}"><fmt:message key="edit" /></a></div>
		 <div class="listp">
		 <!-- input field -->
                <c:out value="${address.firstName}" />&nbsp;
                <c:out value="${address.lastName}" />&nbsp;
                <c:out value="${address.addr1}" />&nbsp;
                <c:out value="${address.city}" />&nbsp;
                <c:out value="${address.zip}" />&nbsp;
                <c:out value="${address.country}" />&nbsp;
                <c:if test = "${address.primary == 'true'}">
                	<c:out value="(Primary Address)" />
                </c:if>
		 <!-- end input field -->   	
		 </div>
		 </div>
		 </c:forEach>	  	
		 
	</div>
	
	
	<h4 title="picture">Image</h4>
	<div>
         <c:set var="classIndex" value="0" />
         
	  	<div class="listdivi ln tabdivi"></div>
	  	<div class="listdivi"></div>
	  	
	  	 <div class="list1"><c:set var="classIndex" value="${classIndex+1}" />
         <div class="listfl"><a href="customerAddressList.jhtm?type=add&userid=${customerForm.customer.id}"><fmt:message key="addImage" /></a></div>
		 <div class="listp">
		 	
			<div class="listp">
			<input class="textfield" type="file" name="image_file_2" value="browse">
	 	 
		 </div>
		 </div>
	  	 
	</div>