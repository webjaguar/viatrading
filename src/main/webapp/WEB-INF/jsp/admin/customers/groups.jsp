<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/security/tags" prefix="sec" %>

<tiles:insertDefinition name="admin.customers.groups" flush="true">
  <tiles:putAttribute name="content" type="string"> 
<script language="JavaScript">
<!--
function toggleAll(el) {
	var ids = document.getElementsByName("__selected_id");	
  	if (ids.length == 1)
      document.list_form.__selected_id.checked = el.checked;
    else
      for (i = 0; i < ids.length; i++)
        document.list_form.__selected_id[i].checked = el.checked;	
}

function deleteSelected() {
	var ids = document.getElementsByName("__selected_id");	
  	if (ids.length == 1) {
      if (document.list_form.__selected_id.checked) {
    	return confirm('Delete permanently?');
      }
    } else {
      for (i = 0; i < ids.length; i++) {
        if (document.list_form.__selected_id[i].checked) {
    	  return confirm('Delete permanently?');
        }
      }
    }

    alert("Please select group(s) to delete.");       
    return false;
}
//-->
</script> 
<form action="customerGroupList.jhtm" method="post" id="list" name="list_form">
<input type="hidden" id="sort" name="sort" value="${groupCustomerSearch.sort}" />
  <!-- main box -->
  <div id="mboxfull">
  
  <!-- start table -->
  <table cellpadding="0" cellspacing="0" border="0" class="module" >
  <tr>
    <td class="topl_g">
	  
	  <!-- breadcrumb -->
	  <p class="breadcrumb">
	    <a href="../customers"><fmt:message key="customers" /></a> &gt;
	    <fmt:message key="groups" /> 
	  </p>
	  
	  <!-- Error Message -->
	  
	  <!-- header image -->
	  <img class="headerImage" src="../graphics/customer.gif" />
	  
	  </td><td class="topr_g" ></td></tr>
  <tr><td class="boxmidlrg" >

 <!-- tabs -->
 <div class="tab-wrapper">
	<h4 title=""></h4>
	<div>
	<!-- start tab -->
	<p>
	  	<div class="listdivi ln tabdivi"></div>
	  	<div class="listdivi"></div>
	   
		  	<div class="listlight">
		  	<!-- input field --> 
		  	<table border="0" cellpadding="0" cellspacing="1" width="100%">
			  <tr>
			  <c:if test="${model.groups.nrOfElements > 0}">
			  <td class="pageShowing">
			  <fmt:message key="showing">
				<fmt:param value="${model.groups.firstElementOnPage + 1}"/>
				<fmt:param value="${model.groups.lastElementOnPage + 1}"/>
				<fmt:param value="${model.groups.nrOfElements}"/>
			  </fmt:message>
			  </td>
			  </c:if>
			  <td class="pageNavi">
			  Page 
			  <select id="page" name="page" onchange="submit()">
			  <c:forEach begin="1" end="${model.groups.pageCount}" var="page">
			  	<option value="${page}" <c:if test="${page == (model.groups.page+1)}">selected</c:if>>${page}</option>
			  </c:forEach>
			  </select>
			  of <c:out value="${model.groups.pageCount}"/>
			  | 
			  <c:if test="${model.groups.firstPage}"><span class="pageNaviDead">previous</span></c:if>
			  <c:if test="${not model.groups.firstPage}"><a href="<c:url value="customerGroupList.jhtm"><c:param name="page" value="${model.groups.page}"/><c:param name="id" value="${model.id}"/></c:url>" class="pageNaviLink">previous</a></c:if>
			  | 
			  <c:if test="${model.groups.lastPage}"><span class="pageNaviDead">next</span></c:if>
			  <c:if test="${not model.groups.lastPage}"><a href="<c:url value="customerGroupList.jhtm"><c:param name="page" value="${model.groups.page+2}"/><c:param name="id" value="${model.id}"/></c:url>" class="pageNaviLink">next</a></c:if>
			  </td>
			  </tr>
			</table>
			
			<table border="0" cellpadding="0" cellspacing="0" width="100%" class="listings">
			  <tr class="listingsHdr2">
			  <c:if test="${!empty model.groups.pageList}">
			  <td align="center" class="quickMode"><input type="checkbox" onclick="toggleAll(this)"></td>
			  </c:if>
			    <td class="indexCol">&nbsp;</td>
			    <td>
			        <table cellspacing="0" cellpadding="1">
			    	  <tr>
			    	    <c:choose>
			    	      <c:when test="${groupCustomerSearch.sort == 'name desc'}">
			    	        <td class="listingsHdr3">
			    	        <a class="hdrLink" href="#" onClick="document.getElementById('sort').value='name';document.getElementById('list').submit()"><fmt:message key="Name" /></a>
			    	        </td>
			    	        <td><img src="../graphics/up.gif" border="0"></td>
			    	      </c:when>
			    	      <c:when test="${groupCustomerSearch.sort == 'name'}">
			    	        <td class="listingsHdr3">
			    	        <a class="hdrLink" href="#" onClick="document.getElementById('sort').value='name desc';document.getElementById('list').submit()"><fmt:message key="Name" /></a>
			    	        </td>
			    	        <td><img src="../graphics/down.gif" border="0"></td>
			    	      </c:when>
			    	      <c:otherwise>
			    	        <td colspan="2" class="listingsHdr3">
			    	        <a class="hdrLink" href="#" onClick="document.getElementById('sort').value='name desc';document.getElementById('list').submit()"><fmt:message key="Name" /></a>
			    	        </td>
			    	      </c:otherwise>
			    	    </c:choose>  
			    	  </tr>
			    	  </table>
			    </td>
			    <td>
			    	<table cellspacing="0" cellpadding="1">
			    	  <tr>
			    	    <td rowspan="2" class="listingsHdr3"><fmt:message key="id" /></td>
			          </tr>
			    	</table>
			    </td>
			    <td>
			        <table cellspacing="0" cellpadding="1">
			    	  <tr>
			    	    <c:choose>
			    	      <c:when test="${groupCustomerSearch.sort == 'num_customer desc'}">
			    	        <td class="listingsHdr3">
			    	        <a class="hdrLink" href="#" onClick="document.getElementById('sort').value='num_customer';document.getElementById('list').submit()"><fmt:message key="customers" /></a>
			    	        </td>
			    	        <td><img src="../graphics/up.gif" border="0"></td>
			    	      </c:when>
			    	      <c:when test="${groupCustomerSearch.sort == 'num_customer'}">
			    	        <td class="listingsHdr3">
			    	        <a class="hdrLink" href="#" onClick="document.getElementById('sort').value='num_customer desc';document.getElementById('list').submit()"><fmt:message key="customers" /></a>
			    	        </td>
			    	        <td><img src="../graphics/down.gif" border="0"></td>
			    	      </c:when>
			    	      <c:otherwise>
			    	        <td colspan="2" class="listingsHdr3">
			    	        <a class="hdrLink" href="#" onClick="document.getElementById('sort').value='num_customer desc';document.getElementById('list').submit()"><fmt:message key="customers" /></a>
			    	        </td>
			    	      </c:otherwise>
			    	    </c:choose>  
			    	  </tr>
			    	  </table>
			    </td>
			    <td>
			    	<table cellspacing="0" cellpadding="1">
			    	  <tr>
			    	    <td rowspan="2" class="listingsHdr3"><fmt:message key="date" /></td>
			          </tr>
			    	</table>
			    </td>
			    <td>
			        <table cellspacing="0" cellpadding="1">
			    	  <tr>
			    	    <c:choose>
			    	      <c:when test="${groupCustomerSearch.sort == 'created_by desc'}">
			    	        <td class="listingsHdr3">
			    	        <a class="hdrLink" href="#" onClick="document.getElementById('sort').value='created_by';document.getElementById('list').submit()"><fmt:message key="createdBy" /></a>
			    	        </td>
			    	        <td><img src="../graphics/up.gif" border="0"></td>
			    	      </c:when>
			    	      <c:when test="${groupCustomerSearch.sort == 'created_by'}">
			    	        <td class="listingsHdr3">
			    	        <a class="hdrLink" href="#" onClick="document.getElementById('sort').value='created_by desc';document.getElementById('list').submit()"><fmt:message key="createdBy" /></a>
			    	        </td>
			    	        <td><img src="../graphics/down.gif" border="0"></td>
			    	      </c:when>
			    	      <c:otherwise>
			    	        <td colspan="2" class="listingsHdr3">
			    	        <a class="hdrLink" href="#" onClick="document.getElementById('sort').value='created_by desc';document.getElementById('list').submit()"><fmt:message key="createdBy" /></a>
			    	        </td>
			    	      </c:otherwise>
			    	    </c:choose>  
			    	  </tr>
			    	  </table>
			    </td>
			  </tr>
			<c:forEach items="${model.groups.pageList}" var="group" varStatus="status">
			  <tr class="row${status.index % 2}" onmouseover="changeStyleClass(this,'rowOver')" onmouseout="changeStyleClass(this,'row${status.index % 2}')" >
			    <c:if test="${!empty model.groups.pageList}">
			    <td align="center" class="quickMode"><input name="__selected_id" value="${group.id}" type="checkbox"></td>
			    </c:if>
			    <td class="indexCol"><c:out value="${status.count + model.addresses.firstElementOnPage}"/>.</td>
			    <td class="nameCol">
			     <a href="<c:url value="customerGroup.jhtm"><c:param name="id" value="${group.id}"/></c:url>" class="nameLink<c:if test="${!group.active}">Inactive</c:if>">
			     <c:out value="${group.name}"/></a>
			    </td>
			    <td class="nameCol"><c:out value="${group.id}"/></td>
			    <td class="nameCol"><a href="<c:url value="customerList.jhtm"><c:param name="groupId" value="${group.id}"/></c:url>" class="nameLink">
			     <c:out value="${group.numCustomer}"/></a>
			    </td>
			    <td class="nameCol"><fmt:formatDate type="date" timeStyle="default" value="${group.created}"/></td>
			    <td class="nameCol"><c:out value="${group.createdBy}"/></td>
			  </tr>
			</c:forEach>
			<c:if test="${model.groups.nrOfElements == 0}">
			  <tr class="emptyList"><td colspan="3">&nbsp;</td></tr>
			</c:if>
			</table>
			
			<table border="0" cellpadding="0" cellspacing="1" width="100%">
			  <tr>
			  <td class="pageSize">
			  <select name="size" onchange="document.getElementById('page').value=1;submit()">
			  <c:forTokens items="10,25,50,100,200,500" delims="," var="current">
			  	  <option value="${current}" <c:if test="${current == groupCustomerSearch.pageSize}">selected</c:if>>${current} <fmt:message key="perPage" /></option>
			  </c:forTokens>
			  </select>
			  </td>
			  </tr>
			</table>
		  	<!-- end input field -->  	  
		  	</div>
		  	
		  	<!-- start button --> 
		  	<div align="left" class="button">
		  	  <input type="submit" name="__add" value="<fmt:message key="groupAdd" />">
		  	  <input type="submit" name="__delete" value="Delete Selected Groups" onClick="return deleteSelected()">			  
			</div>
		  	<!-- end button -->	
	</p>	  	
	<!-- end tab -->        
	</div>
<!-- end tabs -->			
</div>
  
  <!-- end table -->
   </td><td class="boxmidr" ></td></tr>
  <tr><td class="botl"></td><td class="botr"></td></tr>
  </table>
  
<!-- end main box -->  
</div>
</form>  

  </tiles:putAttribute>    
</tiles:insertDefinition>