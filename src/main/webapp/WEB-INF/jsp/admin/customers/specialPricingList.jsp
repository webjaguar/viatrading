<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/security/tags" prefix="sec" %>
<%@ taglib uri="/WEB-INF/tlds/spring.tld" prefix="spring" %>

<tiles:insertDefinition name="admin.customers.specialPricing" flush="true">
  <tiles:putAttribute name="content" type="string">

<script type="text/javascript" src="../../admin/javascript/multiple.open.accordion1.2.js"></script>  
<script language="JavaScript">
<!--
window.addEvent('domready', function(){	
    var Tips1 = new Tips('.toolTipImg');  
    		
	var myToggler = $$('div.arrow_drop');
	// Create the accordian
	new MultipleOpenAccordion(myToggler, $$('div.information'), {
		transition: Fx.Transitions.sineOut,display:100,
		onActive: function(myToggler){
				myToggler.removeClass('arrowImageRight').addClass('arrowImageDown');
			},
		onBackground: function(myToggler){
		myToggler.removeClass('arrowImageDown').addClass('arrowImageRight');
			}
		});
});
function toggleAll(el) {
	var ids = document.getElementsByName("__selected_id");	
  	if (ids.length == 1)
      document.list_form.__selected_id.checked = el.checked;
    else
      for (i = 0; i < ids.length; i++)
        document.list_form.__selected_id[i].checked = el.checked;	
}  

function deleteSelected() {
	var ids = document.getElementsByName("__selected_id");	
  	if (ids.length == 1) {
      if (document.list_form.__selected_id.checked) {
    	return confirm('Delete permanently?');
      }
    } else {
      for (i = 0; i < ids.length; i++) {
        if (document.list_form.__selected_id[i].checked) {
    	  return confirm('Delete permanently?');
        }
      }
    }

    alert("Please select product(s) to delete.");       
    return false;
}

function optionsSelected() {
	var ids = document.getElementsByName("__selected_id");	
  	if (ids.length == 1) {
      if (document.list_form.__selected_id.checked) {
    	return true;
      }
    } else {
      for (i = 0; i < ids.length; i++) {
        if (document.list_form.__selected_id[i].checked) {
    	  return true;
        }
      }
    }

    alert("Please select product(s).");       
    return false;
}
//-->
</script>

<form action="specialPricing.jhtm" method="post" name="list_form">	
<!-- main box -->
  <div id="mboxfull">
  
  <!-- start table -->
  <table cellpadding="0" cellspacing="0" border="0" class="module" >
  <tr>
    <td class="topl_g">
	  
	  <!-- breadcrumb -->
	  <p class="breadcrumb">
	    <a href="../customers">Customer</a> &gt;
         special Pricing &gt;
         ${model.customer.address.firstName} ${model.customer.address.lastName} (${model.customer.address.company})
      </p>
	  
	  <!-- Error Message -->
	  <c:if test="${!empty model.message}">
		 <div class="message"><spring:message code="${model.message}" arguments="${model.var}"/></div>
	  </c:if>
	  
	  <!-- header image -->
	  <img class="headerImage" src="../graphics/customer.gif" />

	  <!-- Option -->
		<div class="optionBox"><div class="titlePrivilege"><fmt:message key="options" /></div></div>
		<div class="arrow_drop arrowImageRight"></div>
		<div style="clear: both;"></div>
		<div class="information" style="float:left;">  
		     <c:if test="${gSiteConfig['gSPECIAL_PRICING'] == 'true'}">
			 <div style="float:left;padding: 5px">
	           <table class="productBatchTool" cellpadding="5" cellspacing="5">
	           <tr>
	            <td valign="top">
	             <table cellpadding="1" cellspacing="1">
	              <tr style="height:30px;white-space: nowrap">
	               <td valign="top" width="20px"><img class="toolTipImg" title="SpecialPricing::Add Special price for this SKU." src="../graphics/question.gif" /></td>
	               <td valign="top"><h3>Special Pricing</h3></td>
	              </tr>
	             </table>
	            </td>
	           </tr>
	           <tr>
	            <td valign="top">
	             <table cellpadding="1" cellspacing="1">
	              <tr style="">
				   <td>Sku</td>
				   <td><input name="__sku" value="${model.sku}" type="text" size="15" /></td>
				  </tr>
				  <tr>
				   <td>Group</td>
				   <td>
				   <select name="__group_id" style="width:9em;">
				     <option value="" ><c:out value="Select Group"/></option>
				     <c:forEach items="${model.groups}" var="group">
				     <option value="${group.id}" ><c:out value="${group.name}"/></option>
				     </c:forEach>
				   </select>
				   </td>
				   </tr>
				  <tr>
				   <td>Price</td>
				   <td><input name="__price" value="${model.price}" type="text" size="15" /></td>
				  </tr>
				  <tr>
				   <td colspan="2">  
				   <div align="left" class="buttonLeft">
			         <input type="submit" name="__add" value="<fmt:message key="add" /> / <fmt:message key="update" />" />
			       </div>
			       </td>
			      </tr>
	             </table>
	            </td>
	           </tr>
	           </table>
		     </div>
		     <div style="float:left;padding: 5px">
	           <table class="productBatchTool" cellpadding="5" cellspacing="5">
	           <tr>
	            <td valign="top">
	             <table cellpadding="1" cellspacing="1">
	              <tr style="height:30px;white-space: nowrap">
	               <td valign="top" width="20px"><img class="toolTipImg" title="SpecialPricing::Add Special price for the products under this category ID." src="../graphics/question.gif" /></td>
	               <td valign="top"><h3>Special Pricing</h3></td>
	              </tr>
	             </table>
	            </td>
	           </tr>
	           <tr>
	            <td valign="top">
	             <table cellpadding="1" cellspacing="1">
	              <tr style="">
				   <td>category ID</td>
				   <td><input name="__cid" value="${model.cid}" type="text" size="15" /></td>
				  </tr>
				  <tr>
				   <td>Price</td>
				   <td><input name="__price_cid" value="${model.price}" type="text" size="15" /></td>
				  </tr>
				  <tr>
				   <td colspan="2">  
				   <div align="left" class="buttonLeft">
			         <input type="submit" name="__addProductOfCategory" value="<fmt:message key="add" /> / <fmt:message key="update" />" />
			       </div>
			       </td>
			      </tr>
	             </table>
	            </td>
	           </tr>
	           </table>
		     </div>
		     <div style="float:left;padding: 5px">
	           <table class="productBatchTool" cellpadding="5" cellspacing="5">
	           <tr>
	            <td valign="top">
	             <table cellpadding="1" cellspacing="1">
	              <tr style="height:30px;white-space: nowrap">
	               <td valign="top" width="20px"><img class="toolTipImg" title="SpecialPricing::Add Special price for the products under this category ID." src="../graphics/question.gif" /></td>
	               <td valign="top"><h3>Special Pricing</h3></td>
	              </tr>
	             </table>
	            </td>
	           </tr>
	           <tr>
	            <td valign="top">
	             <table cellpadding="1" cellspacing="1">
	              <tr style="">
				   <td>category ID</td>
				   <td><input name="__cid_gid" value="${model.cid}" type="text" size="15" /></td>
				  </tr>
				  <tr>
				   <td>Group</td>
				   <td>
				   <select name="__group_id_cid_gid" style="width:9em;">
				     <option value="" ><c:out value="Select Group"/></option>
				     <c:forEach items="${model.groups}" var="group">
				     <option value="${group.id}" ><c:out value="${group.name}"/></option>
				     </c:forEach>
				   </select>
				   </td>
				   </tr>
				  <tr>
				   <td>Price</td>
				   <td><input name="__price_cid_gid" value="${model.price}" type="text" size="15" /></td>
				  </tr>
				  <tr>
				   <td colspan="2">  
				   <div align="left" class="buttonLeft">
			         <input type="submit" name="__addProductOfCategoryWithCustomerGroupId" value="<fmt:message key="add" /> / <fmt:message key="update" />" />
			       </div>
			       </td>
			      </tr>
	             </table>
	            </td>
	           </tr>
	           </table>
		     </div>
		     </c:if>
		</div>
	
	  </td><td class="topr_g" ></td></tr>
  <tr><td class="boxmidlrg" >

 <!-- tabs -->
 <div class="tab-wrapper">
	<h4 title="List of affiliates"></h4>
	<div>
	<!-- start tab -->

	  	<div class="listdivi"></div>
	   
		  	<div class="listlight">
		  	<!-- input field --> 
			<table border="0" cellpadding="0" cellspacing="1" width="100%">
			  <tr>
			  <c:if test="${model.spList.nrOfElements > 0}">
			  <td class="pageShowing">
			  <fmt:message key="showing">
				<fmt:param value="${model.spList.firstElementOnPage + 1}"/>
				<fmt:param value="${model.spList.lastElementOnPage + 1}"/>
				<fmt:param value="${model.spList.nrOfElements}"/>
			  </fmt:message>
			  </td>
			  </c:if>
			  <td class="pageNavi">
			  Page 
			  <select id="page" name="page" onchange="submit()">
			  <c:forEach begin="1" end="${model.spList.pageCount}" var="page">
			  	<option value="${page}" <c:if test="${page == (model.spList.page+1)}">selected</c:if>>${page}</option>
			  </c:forEach>
			  </select>
			  of <c:out value="${model.spList.pageCount}"/>
			  | 
			  <c:if test="${model.spList.firstPage}"><span class="pageNaviDead"><fmt:message key="previous" /></span></c:if>
			  <c:if test="${not model.spList.firstPage}"><a href="<c:url value="specialPricing.jhtm"><c:param name="page" value="${model.spList.page}"/><c:param name="id" value="${model.customerId}"/></c:url>" class="pageNaviLink"><fmt:message key="previous" /></a></c:if>
			  | 
			  <c:if test="${model.spList.lastPage}"><span class="pageNaviDead"><fmt:message key="next" /></span></c:if>
			  <c:if test="${not model.spList.lastPage}"><a href="<c:url value="specialPricing.jhtm"><c:param name="page" value="${model.spList.page+2}"/><c:param name="id" value="${model.customerId}"/></c:url>" class="pageNaviLink"><fmt:message key="next" /></a></c:if>
			  </td>
			  </tr>
			</table>
			
			<table border="0" cellpadding="0" cellspacing="0" width="100%" class="listings">
			  <input type="hidden" name="id" value="${model.customerId}" />
			  <tr class="listingsHdr2">
			   <c:if test="${model.spList.nrOfElements > 0}">
			    <td width="15px" align="center"><input type="checkbox" onclick="toggleAll(this)"></td>
			   </c:if>     
			    <td width="15px">&nbsp;</td>
			    <td>
			    	<table cellspacing="0" cellpadding="1">
			    	  <tr>
			    	    <td rowspan="2" class="listingsHdr3"><fmt:message key="sku" /></td>
			       	  </tr>
			    	</table>
			    </td>
			    <td>
			    	<table cellspacing="0" cellpadding="1">
			    	  <tr>
			    	    <td rowspan="2" class="listingsHdr3"><fmt:message key="price" /></td>
			       	  </tr>
			    	</table>
			    </td>
			    <td>
			    	<table cellspacing="0" cellpadding="1">
			    	  <tr>
			    	    <td rowspan="2" class="listingsHdr3"><fmt:message key="active" /></td>
			       	  </tr>
			    	</table>
			    </td>
			  </tr>
			<c:forEach items="${model.spList.pageList}" var="sp" varStatus="status">
			  <tr class="row${status.index % 2}" onmouseover="changeStyleClass(this,'rowOver')" onmouseout="changeStyleClass(this,'row${status.index % 2}')">
			  <input type="hidden" name="__id" value="${sp.id}" />
			  <c:if test="${model.spList.nrOfElements > 0}">
			    <td align="center"><input name="__selected_id" value="${sp.id}" type="checkbox"></td>
			  </c:if>
			    <td width="15px">&nbsp;</td>
			    <td class="nameCol"><c:out value="${sp.sku}"/></td>
			    <td class="nameCol"><fmt:formatNumber value="${sp.price}" pattern="#,##0.00" /></td>
			    <td><input type="checkbox" <c:if test="${sp.enable}">checked</c:if> name="__active_<c:out value="${sp.id}"/>" /></td>
			  </tr>
			</c:forEach>
			
			</table>
			
			<table border="0" cellpadding="0" cellspacing="1" width="100%">
			  <tr>
			  <td class="pageSize">
			    <select name="size" onchange="document.getElementById('page').value=1;submit()">
			    <c:forTokens items="10,25,50,100,500" delims="," var="current">
			  	  <option value="${current}" <c:if test="${current == model.spList.pageSize}">selected</c:if>>${current} <fmt:message key="perPage" /></option>
			    </c:forTokens>
			    </select>
			  </td>
			  </tr>
			</table>
		  	<!-- end input field -->  	  
		  	</div>
		  	
		  	<!-- start button --> 
		  	<div align="left" class="button">
			  <c:if test="${model.spList.nrOfElements > 0}">
			  	<input type="submit" name="__delete" value="Delete" onClick="return deleteSelected()">
			  	<input type="submit" name="__update_active" value="Update Active">
			  </c:if>
		  	</div>
		  	<!-- end button -->	
	  	
	<!-- end tab -->        
	</div>
<!-- end tabs -->			
</div>

  <!-- end table -->
   </td><td class="boxmidr" ></td></tr>
  <tr><td class="botl"></td><td class="botr"></td></tr>
  </table>
  
<!-- end main box -->  
</div>		
</form>

  </tiles:putAttribute>    
</tiles:insertDefinition>
