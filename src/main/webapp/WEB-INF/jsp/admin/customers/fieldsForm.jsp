<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="/WEB-INF/tlds/spring.tld" prefix="spring" %>
<%@ taglib uri="http://www.springframework.org/security/tags" prefix="sec" %>

<tiles:insertDefinition name="admin.customers" flush="true">
  <tiles:putAttribute name="content" type="string">
<sec:authorize ifAnyGranted="ROLE_AEM,ROLE_ADMIN_${prop_site_id},ROLE_CUSTOMER_FIELD">  
  
<script language="JavaScript"> 
<!--
window.addEvent('domready', function(){			
			var tabs1 = new SimpleTabs($('tab-block-1'), { selector: 'h4'});
			var Tips1 = new Tips($$('.toolTipImg'));
		});
//--> 
</script> 
<form method="post" name="customerFields">
<!-- main box -->
  <div id="mboxfull">
  
  <!-- start table -->
  <table cellpadding="0" cellspacing="0" border="0" class="module" >
  <tr>
    <td class="topl_g">
	  
	  <!-- breadcrumb -->
	  <p class="breadcrumb">
	     <a href="../customers">Customers</a> &gt;
	    Fields 
	  </p>
	  
	  <!-- Error Message -->
  	  <c:if test="${!empty message}">
		  <div class="message"><spring:message code="${message}" /></div>
	  </c:if>

  </td><td class="topr_g" ></td></tr>
  <tr><td class="boxmidlrg" >

 <!-- tabs -->
 <div id="tab-block-1">
	<h4 title="<fmt:message key="customerFields"/> Form"><fmt:message key="customerFields"/></h4>
	<div>
	<!-- start tab -->
         <c:set var="classIndex" value="0" />
         
	  	<div class="listdivi ln tabdivi"></div>
	  	<div class="listdivi"></div>
	   
		  	<div class="list${classIndex % 2}"><c:set var="classIndex" value="${classIndex+1}" />
		  	<div class="listpp">
		  	<!-- input field -->
				<table class="form">
				  <tr>
				    <td>&nbsp;</td>
				    <td align="center" width="50"><fmt:message key="Enabled" /></td>
				    <td align="center" width="50"><fmt:message key="required" /></td>
				    <td align="center" width="50"><fmt:message key="public" /></td>
				    <td align="center" width="50"><fmt:message key="showOnInvoice" /></td>
				    <c:if test="${siteConfig['CUSTOMER_FIELDS_ON_INVOICE_EXPORT'].value == 'true'}"	>
			    	<td align="center" width="50"><fmt:message key="showOnInvoice" />(Export)</td>
				    </c:if>
				    <td align="center" width="50"><fmt:message key="multiSelecOnAdmin" /></td>
				    <td style="padding-left:20px"><fmt:message key="Name" /></td>
				    <td style="padding-left:20px"><fmt:message key="NameEs" /></td>
				    <td style="padding-left:20px"><fmt:message key="PreValue" /><span style="float:left"><img class="toolTipImg" title="Note::Seperate values with comma. Don't use space before/after comma." src="../graphics/question.gif" /></span></td>
				  </tr>
				  <c:forEach items="${customerFieldsForm.customerFields}" var="customerField" varStatus="status">
				  <tr>
				    <td style="width:150px;text-align:right"><fmt:message key="Field" /> <c:out value="${status.index + 1}"/> : </td>
				    <td align="center"><input name="__enabled_${customerField.id}" type="checkbox" <c:if test="${customerField.enabled}">checked</c:if>></td>
				    <td align="center"><input name="__required_${customerField.id}" type="checkbox" <c:if test="${customerField.required}">checked</c:if>></td>
				    <td align="center"><input name="__publicField_${customerField.id}" type="checkbox" <c:if test="${customerField.publicField}">checked</c:if>></td>
				    <td align="center"><input name="__showOnInvoice_${customerField.id}" type="checkbox" <c:if test="${customerField.showOnInvoice}">checked</c:if>></td>
				    <c:if test="${siteConfig['CUSTOMER_FIELDS_ON_INVOICE_EXPORT'].value == 'true'}"	>
			    	<td align="center"><input name="__showOnInvoiceExport_${customerField.id}" type="checkbox" <c:if test="${customerField.showOnInvoiceExport}">checked</c:if>></td>
				    </c:if>
				    <td align="center"><input name="__multiSelecOnAdmin_${customerField.id}" type="checkbox" <c:if test="${customerField.multiSelecOnAdmin}">checked</c:if>></td>
				    
				    <td><input type="text" name="__name_${customerField.id}" value="<c:out value="${customerField.name}"/>" class="textfield" maxlength="255" size="50"></td>
				    <td><input type="text" name="__nameEs_${customerField.id}" value="<c:out value="${customerField.nameEs}"/>" class="textfield" maxlength="255" size="50"></td>
				    <td><input type="text" name="__preValue_${customerField.id}" value="<c:out value="${customerField.preValue}"/>" class="textfield" maxlength="1024" size="50"></td>
				  </tr>
				  </c:forEach>
				</table>
	 		<!-- end input field -->	
	 		</div>
		  	</div>	
	<!-- end tab -->        	
	</div>
<!-- end tabs -->			
</div>
  
<!-- start button -->
<div align="left" class="button"> 
	<input type="submit" value="<spring:message code="Update"/>">
</div>
<!-- end button -->

  <!-- end table -->
    </td><td class="boxmidr" ></td></tr>
  <tr><td class="botl"></td><td class="botr"></td></tr>
  </table>
  
<!-- end main box -->  
</div>	
</form>

</sec:authorize>
  </tiles:putAttribute>    
</tiles:insertDefinition>


