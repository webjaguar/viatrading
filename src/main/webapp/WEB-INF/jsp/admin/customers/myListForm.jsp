<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="/WEB-INF/tlds/spring.tld" prefix="spring" %>
<%@ taglib uri="http://www.springframework.org/security/tags" prefix="sec" %>

<tiles:insertDefinition name="admin.customers" flush="true">
  <tiles:putAttribute name="content" type="string">
<sec:authorize ifAnyGranted="ROLE_AEM,ROLE_ADMIN_${prop_site_id},ROLE_CUSTOMER_MYLIST_ADD">

<script language="JavaScript" type="text/JavaScript">
<!--
window.addEvent('domready', function(){			
			var tabs1 = new SimpleTabs($('tab-block-1'), { selector: 'h4'});
			var Tips1 = new Tips($$('.toolTipImg'));
		});
//-->
</script>
<form action="addMyList.jhtm" method="post" >
	<input type="hidden" name="id" value="${model.userId}">
<!-- main box -->
  <div id="mboxfull">
  
  <!-- start table -->
  <table cellpadding="0" cellspacing="0" border="0" class="module" >
  <tr>
    <td class="topl_g">
	  
	  <!-- breadcrumb -->
	  <p class="breadcrumb">
	    <a href="../customers">Customers</a> &gt;
	    <a href="../catalog/myList.jhtm">MyList</a> &gt;
	    form 
	  </p>
	  
	  <!-- Error Message -->
	  <c:if test="${!empty model.message}">
		 <div class="message"><spring:message code="${model.message}" arguments="${model.skuToken}"/></div>
	  </c:if>

  
  </td><td class="topr_g" ></td></tr>
  <tr><td class="boxmidlrg" >

 <!-- tabs -->
 <div id="tab-block-1">
	<h4 title="MyList">MyList</h4>
	<div>
	<!-- start tab -->
	<p>
        <c:set var="classIndex" value="0" />
        
	  	<div class="listdivi ln tabdivi"></div>
	  	<div class="listdivi"></div>	

		  	<div class="list${classIndex % 2}"><c:set var="classIndex" value="${classIndex+1}" />
		  	<div class="listfl"><div class="helpImg"><img class="toolTipImg" title="SKU List::Separate Skus with comma or enter" src="../graphics/question.gif" /></div><strong><fmt:message key='enterSku(s)' />:</strong></div>
		  	<div class="listp">
		  	<!-- input field -->
				<textarea cols="40" rows="10" name="skus" ></textarea>
		    <!-- end input field -->   	
		  	</div>
		  	</div>
	</p>	  	
	<!-- end tab -->        
	</div> 

<!-- end tabs -->			
</div>

<!-- start button -->
<div align="left" class="button"> 
	<input type="submit" value="<fmt:message key='add' />" name="__add"/> 
	<input type="submit" value="<spring:message code="cancel"/>" name="_cancel">
</div>
<!-- end button -->	     
  
  <!-- end table -->
    </td><td class="boxmidr" ></td></tr>
  <tr><td class="botl"></td><td class="botr"></td></tr>
  </table>
  
<!-- end main box -->  
</div>
</form>
</sec:authorize>

  </tiles:putAttribute>    
</tiles:insertDefinition>