<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html>
  <head>
    <title>Customer Info</title>
    <link href="<c:url value="../stylesheet${gSiteConfig['gRESELLER']}.css"/>" rel="stylesheet" type="text/css"/>
    <script type="text/javascript" src="../javascript/mootools-1.2.5-core.js"></script>
    <script type="text/javascript" src="../javascript/mootools-1.2.5.1-more.js"></script>
    <script type="text/javascript" src="../javascript/SimpleTabs1.2.js"></script>
    <style type="text/css">
     .userInfo td {font-size:10px;color:#222222;}
     .nameCol {font-size:10px;}
    </style>
  </head>  
<body style="background-color:#ffffff;min-width:700px !important;">
<script type="text/javascript"> 
window.addEvent('domready', function(){			
			var tabs1 = new SimpleTabs($('tab-block-1'), { selector: 'h4'});
		});
</script>
<!-- main box -->
  <div id="mboxfullpopup">
      <div class="title">
        <span>Customer Information</span>
      </div>
      <div class="print">
      	<a href="javascript:window.print()"><img border="0" src="../graphics/printer.png" /></a>
      </div>
  
  <!-- start table -->
  <table cellpadding="0" cellspacing="0" border="0" style="float:left">
  <tr><td class="boxmidlrg" >
      
 <!-- tabs -->
 <div id="tab-block-1">
	<h4 title="<fmt:message key="customerInformation"/>"><fmt:message key="customerInformation"/></h4>
	<div>
	<!-- start tab -->
		<table border="0" width="100%" style="height:340px;width:99%;border:1px solid #999999;background-color:#f3f4f5;clear:both;">
		<tr style="height:150px;">
	      <td valign="top" style="width:100px;"><img src="../graphics/customer.png" width="89" height="137" /></td>
	      <td valign="top" aligh="left">
	        <table border="0" class="userInfo" cellpadding="0" cellspacing="0">
	         <tr>
	          <td><fmt:message key="customer" /></td>
	          <td class="spacer"> : </td>
	          <td><c:out value="${model.customer.address.firstName}" /> <c:out value="${model.customer.address.lastName}" /></td>
	         </tr>
	         <tr>
	          <td><fmt:message key="email" /></td>
	          <td class="spacer"> : </td>
	          <td><c:out value="${model.customer.username}" /></td>
	         </tr>
	         <tr>
	          <td><fmt:message key="address" /></td>
	          <td class="spacer"> : </td>
	          <td><c:out value="${model.customer.address.addr1}" /> <c:out value="${model.customer.address.addr2}" /> <c:out value="${model.customer.address.city}" /><br />
	              <c:out value="${model.customer.address.stateProvince}" /> <c:out value="${model.customer.address.zip}" /> <c:out value="${model.customer.address.country}" /></td>
	         </tr>
	         <tr>
	          <td><fmt:message key="phone" /></td>
	          <td class="spacer"> : </td>
	          <td><c:out value="${model.customer.address.phone}" /></td>
	         </tr>
	         <tr>
	          <td><fmt:message key="cellPhone" /></td>
	          <td class="spacer"> : </td>
	          <td><c:out value="${model.customer.address.cellPhone}" /></td>
	         </tr>
	         <tr>
	          <td><fmt:message key="fax" /></td>
	          <td class="spacer"> : </td>
	          <td><c:out value="${model.customer.address.fax}" /></td>
	         </tr>
	         <tr>
	          <td><fmt:message key="company" /></td>
	          <td class="spacer"> : </td>
	          <td><c:out value="${model.customer.address.company}" /></td>
	         </tr>
	         <tr>
	          <td><fmt:message key="account" /> #</td>
	          <td class="spacer"> : </td>
	          <td><c:out value="${model.customer.accountNumber}" /></td>
	         </tr>
	         </table>
	      </td>
	      <td></td>
	      <td valign="top">
	        <table border="0" class="userInfo" cellpadding="0" cellspacing="0">
	         <tr>
	          <td><fmt:message key="point" /></td>
	          <td class="spacer"> : </td>
	          <td><c:out value="${model.customer.loyaltyPoint}" /></td>
	         </tr>
	         <tr>
	          <td><fmt:message key="credit" /></td>
	          <td class="spacer"> : </td>
	          <td><fmt:message key="${siteConfig['CURRENCY'].value}" /><fmt:formatNumber value="${model.customer.credit}" pattern="#,##0.00" /></td>
	         </tr>
	         <c:if test="${!empty model.customer.registeredBy}">
	         <tr>
	           <td><fmt:message key="registeredBy" /></td>
	            <td class="spacer"> : </td>
	            <td><c:out value="${model.customer.registeredBy}" /></td>
	         </tr>   
	         </c:if>
	         <c:if test="${gSiteConfig['gSALES_REP']}">
	         <tr>
	           <td><fmt:message key="salesRep" /></td>
	            <td class="spacer"> : </td>
	            <td><c:out value="${model.salesRepMap[model.customer.salesRepId].name}" /></td>
	         </tr>   
	         </c:if>
	         </table>
	      </td>
	      </tr>
	      <tr>
	        <td colspan="5" valign="top" style="border-top:1px solid #999999;">
	        <div style="font-size:14px; color:#8C8C8C">Custom Field</div>
	        <c:forEach items="${model.customer.customerFields}" var="customerField" varStatus="status">
	        <div style="padding:3px; margin:3px;">
	        <table class="userInfo" border="0" cellpadding="0" cellspacing="0">
	         <tr>
	          <td><c:out value="${customerField.name}"/></td>
	          <td> : </td>
	          <td><c:out value="${customerField.value}"/></td>
	         </tr>
	        </table>
	        </div>
	        </c:forEach>
	        </td>
	      </tr>
	      <tr>
	        <td colspan="5" valign="top" style="border-top:1px solid #999999;">
	        <div style="font-size:14px; color:#8C8C8C">Note</div>
	        <table class="userInfo" border="0" cellpadding="0" cellspacing="0">
	         <tr>
	          <td><c:out value="${model.customer.note}" escapeXml="false"/></td>
	         </tr>
	        </table>  
	        </td>
	      </tr>
	      </table>
	      <!-- end tab -->        
	      </div>
	      	      
	   
		  <h4 title="Purchase History">Purchase History</h4>
		  <div>
		  <!-- start tab -->
		  <table border="0" style="clear:both;width:100%;border:1px solid #999999;background-color:#eeeeee;">
		    <tr>
			  <td >
			      
				<form>
				<input type="hidden" name="cid" value="${model.customer.id}" />
				<table style="clear:both;" border="0" cellpadding="0" cellspacing="1" width="100%">
				  <tr>
				  <td class="pageShowing">
				  <c:if test="${model.itemsPurchasedList.nrOfElements > 0}">
				  <fmt:message key="showing">
					<fmt:param value="${model.itemsPurchasedList.firstElementOnPage + 1}"/>
					<fmt:param value="${model.itemsPurchasedList.lastElementOnPage + 1}"/>
					<fmt:param value="${model.itemsPurchasedList.nrOfElements}"/>
				  </fmt:message>
				  </c:if>
				  </td>
				  <td class="pageNavi">
				  Page 
				  <select name="page" id="page" onchange="submit()">
				  <c:forEach begin="1" end="${model.itemsPurchasedList.pageCount}" var="page">
				  	<option value="${page}" <c:if test="${page == (model.itemsPurchasedList.page+1)}">selected</c:if>>${page}</option>
				  </c:forEach>
				  </select>
				  of <c:out value="${model.itemsPurchasedList.pageCount}"/>
				  | 
				  <c:if test="${model.itemsPurchasedList.firstPage}"><span class="pageNaviDead"><fmt:message key="previous" /></span></c:if>
				  <c:if test="${not model.itemsPurchasedList.firstPage}"><a href="<c:url value="customerQuickView.jhtm"><c:param name="page" value="${model.itemsPurchasedList.page}"/><c:param name="cid" value="${model.customer.id}"/></c:url>" class="pageNaviLink"><fmt:message key="previous" /></a></c:if>
				  | 
				  <c:if test="${model.itemsPurchasedList.lastPage}"><span class="pageNaviDead"><fmt:message key="next" /></span></c:if>
				  <c:if test="${not model.itemsPurchasedList.lastPage}"><a href="<c:url value="customerQuickView.jhtm"><c:param name="page" value="${model.itemsPurchasedList.page+2}"/><c:param name="cid" value="${model.customer.id}"/></c:url>" class="pageNaviLink"><fmt:message key="next" /></a></c:if>
				  </td>
				  </tr>
				</table>
				</form>
				<table border="0" cellpadding="0" cellspacing="0" width="100%" class="listings">
					<tr class="listingsHdr2">
						<td class="indexCol"> &nbsp; </td>
						<td class="listingsHdr3"> <fmt:message key="date" /> </td>
						<td class="listingsHdr3"> <fmt:message key="invoice" /># </td>
						<td class="listingsHdr3"> <fmt:message key="sku" /> </td>
						<td class="listingsHdr3"> <fmt:message key="productName" /> </td>
						<td class="listingsHdr3"> <fmt:message key="quantity" /> </td>
						<td class="listingsHdr3"> <fmt:message key="unitPrice" /> </td>
						<td class="listingsHdr3"> <fmt:message key="shippingCost" /> </td>
						<td class="listingsHdr3"> <fmt:message key="shippingTitle" /> </td>
						<td class="listingsHdr3"> <fmt:message key="subTotal" /> </td>
						<td class="listingsHdr3"> <fmt:message key="grandTotal" /> </td>
						<td class="listingsHdr3"> <fmt:message key="salesRep" /> </td>
						<td class="listingsHdr3"> <fmt:message key="dateShipped" /> </td>
					</tr>
				<c:forEach items="${model.itemsPurchasedList.pageList}" var="item" varStatus="status">
				  <tr class="row${status.index % 2}" onmouseover="changeStyleClass(this,'rowOver')" onmouseout="changeStyleClass(this,'row${status.index % 2}')">
				    <td class="indexCol"><c:out value="${status.count + model.itemsPurchasedList.firstElementOnPage}"/>.</td>
				    <td class="nameCol"><fmt:formatDate type="date" timeStyle="default" value="${item.dateOrdered}"/></td>		
				    <td class="nameCol"><c:out value="${item.orderId}"/></td>	
				    <td class="nameCol"><c:out value="${item.sku}"/></td>		
				    <td class="nameCol"><c:out value="${item.productName}"/></td>		
				    <td class="nameCol"><c:out value="${item.quantity}"/></td>	
				    <td class="nameCol"><c:out value="${item.unitPrice}"/></td>	
				    <td class="nameCol"><c:out value="${item.shippingCost}"/></td>			
				    <td align="center"><c:out value="${item.shippingMethod}"/></td>			
					<td align="right"><fmt:formatNumber value="${item.subTotal}" pattern="#,##0.00" /></td>
					<td align="right"><fmt:formatNumber value="${item.grandTotal}" pattern="#,##0.00" /></td>   
					<td class="nameCol"><c:out value="${item.salesRepName}"/></td>	
					<td class="nameCol"><c:choose><c:when test="${item.orderStatus == 's'}"><fmt:formatDate type="date" timeStyle="default" value="${item.shipped}"/></c:when><c:otherwise><fmt:message key="${item.encodeStatus}"/></c:otherwise> </c:choose></td>	 
				  </tr>
				</c:forEach>
				<c:if test="${model.itemsPurchasedList.nrOfElements == 0}">
				  <tr class="emptyList"><td colspan="6">&nbsp;</td></tr>
				</c:if>
				</table>
			   </td>
			  </tr>
			</table>	
		    <!-- end tab -->        
			</div>
			
			<h4 title="<c:out value="${model.lastYear}"></c:out>- <fmt:formatDate pattern="yyyy" value="${model.current}"/>Graph"><c:out value="${model.lastYear}"></c:out>- <fmt:formatDate pattern="yyyy" value="${model.current}"/> Graph</h4>
			<div style="clear:both;">
			<!-- start tab -->
			  <table border="0" width="100%" style="border:1px solid #999999;background-color:#eeeeee;">
			    <tr>
			      <td valign="top" width="520px">
					<div align="center" style="">
					  <div style="border:1px solid #999999;background-color:#666666;">
						<h2>
						<span> <c:out value="${model.lastYear}"></c:out>- <fmt:formatDate pattern="yyyy" value="${model.current}"/>Graph  </span>
						</h2>
					  </div>
						<img alt="Blue line chart with alternating gray and white stripes from left to right" 
						src="http://chart.apis.google.com/chart?
						cht=bvs
						&chd=<c:out value="${model.customerPurchase[0]}" />
						&chco=0000FF
						&chls=2.0,1.0,0.0
						&chxt=x,y
						&chtt=<c:out value="${model.lastYear}"></c:out>- <fmt:formatDate pattern="yyyy" value="${model.current}"/> Purchase
						&chg=4.33,25
						&chxl=0:|Jan|Feb|Mar|Apr|May|Jun|Jul|Aug|Sep|Oct|Nov|Des|Jan|Feb|Mar|Apr|May|Jun|Jul|Aug|Sep|Oct|Nov|Des|1:<c:out value="${model.customerPurchase[1]}" />
						&chs=700x200"/>
					</div>
				  </td>				  
				</tr>
				<tr>
				<td valign="top" width="320px">
		  		      <div style="border:1px solid #999999;background-color:#666666;">
						<h2>
						<span> <c:out value="${model.lastYear}"></c:out>- <fmt:formatDate pattern="yyyy" value="${model.current}"/> Shipped  </span>
						</h2>
					  </div>
					  <div class="reportItems">
					    <table>
					     <thead>
					      <tr>
					      	<th class="text">
					          <div class="text_wrapper">
								<div class="text_wrapper">Month</div>
							  </div>
							</th>
							<th class="text">
					          <div class="text_wrapper">
								<div class="text_wrapper">Grand Total</div>
							  </div>
							</th>
					      </tr>
					     </thead>
					     <tbody> 
					     <c:forEach items="${model.customerPurchaseList}" var="customerPurchase" varStatus="status">
					      <tr>
					        <td class="text">
					          <div class="text_wrapper">
								<div class="text_wrapper"><c:out value="${customerPurchase.month}"/></div>
							  </div>
							</td>
							<td class="text">
					          <div class="text_wrapper">
								<div class="text_wrapper"><fmt:message key="${siteConfig['CURRENCY'].value}" /><fmt:formatNumber value="${customerPurchase.grandTotal}" pattern="#,##0.00" /></div>
							  </div>
							</td>
					      </tr>	
					     </c:forEach> 
						 </tbody>
					  </table>  
					</div>
				  </td>
				</tr></table>    
		    <!-- end tab -->        
			</div>	
						
		 </div>
		 <!-- end tabs --> 

	
 
 <!-- end table -->
 </td><td class="boxmidr" ></td></tr>
  </table>
  
<!-- end main box -->  
</div>    

</body>
</html>