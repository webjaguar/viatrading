<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>


	<h4 title="Contacts">Contacts</h4>
	<div>
	<!-- start tab -->

	  	<div class="listdivi ln tabdivi"></div>
	  	<div class="listdivi"></div>
	   
		  	<div class="listlight">
		  	<!-- input field --> 
		  	<table border="0" cellpadding="0" cellspacing="1" width="100%">
			  <tr>
			  <c:if test="${customerExtContacts.nrOfElements > 0}">
			  <td class="pageShowing">
			  <fmt:message key="showing">
				<fmt:param value="${customerExtContacts.firstElementOnPage + 1}"/>
				<fmt:param value="${customerExtContacts.lastElementOnPage + 1}"/>
				<fmt:param value="${customerExtContacts.nrOfElements}"/>
			  </fmt:message>
			  </td>
			  </c:if>
			  <td class="pageNavi">
			  Page 
			  <select name="page" onchange="submit()">
			  <c:forEach begin="1" end="${customerExtContacts.pageCount}" var="page">
			  	<option value="${page}" <c:if test="${page == (customerExtContacts.page+1)}">selected</c:if>>${page}</option>
			  </c:forEach>
			  </select>
			  of <c:out value="${customerExtContacts.pageCount}"/>
			  | 
			  <c:if test="${customerExtContacts.firstPage}"><span class="pageNaviDead">previous</span></c:if>
			  <c:if test="${not customerExtContacts.firstPage}"><a href="<c:url value="customerExtContactList.jhtm"><c:param name="page" value="${customerExtContacts.page}"/><c:param name="id" value="${id}"/></c:url>" class="pageNaviLink">previous</a></c:if>
			  | 
			  <c:if test="${customerExtContacts.lastPage}"><span class="pageNaviDead">next</span></c:if>
			  <c:if test="${not customerExtContacts.lastPage}"><a href="<c:url value="customerExtContactList.jhtm"><c:param name="page" value="${customerExtContacts.page+2}"/><c:param name="id" value="${id}"/></c:url>" class="pageNaviLink">next</a></c:if>
			  </td>
			  </tr>
			</table>
		  <table border="0" cellpadding="0" cellspacing="0" width="100%" class="listings">
					<tr class="listingsHdr2">
					<td class="indexCol">&nbsp;</td>
			    	<td>
			    	<table cellspacing="0" cellpadding="1">
			    	  <tr>
			    	    <td class="listingsHdr3"><fmt:message key="name" /></td>
			    	  </tr>
			    	  </table>
			   		</td>
			   		 	<td align="center">
			          <table cellspacing="0" cellpadding="1">
			    	  <tr>
			    	    <td rowspan="2" class="listingsHdr3"><fmt:message key="position" /></td>
			          </tr>
			    	  </table>
			    	</td>				
			    	<td align="center">
			    	<table cellspacing="0" cellpadding="1">
			    	  <tr>
			    	   <td class="listingsHdr3">
						<fmt:message key="email" />
					</td>
			    	  </tr>
			    	  </table>
			   		</td>
			    	<td align="center">
			    	<table cellspacing="0" cellpadding="1">
			    	  <tr>
			    	    <td class="listingsHdr3">
						<fmt:message key="phone1" />
					</td>
			    	  </tr>
			    	  </table>
			   		</td>
			   	
			    	<td align="center">
			          <table cellspacing="0" cellpadding="1">
			    	  <tr>
			    	    <td rowspan="2" class="listingsHdr3"><fmt:message key="phone2" /></td>
			          </tr>
			    	  </table>
			    	</td>
			</tr>
			

			<c:forEach items="${customerExtContacts.pageList}" var="customerExtContact" varStatus="status">
			  <tr class="row${status.index % 2}" onmouseover="changeStyleClass(this,'rowOver')" onmouseout="changeStyleClass(this,'row${status.index % 2}')" >
			    <td class="indexCol"><c:out value="${status.count + customerExtContacts.firstElementOnPage}"/>.</td>
			    <td class="nameCol"><a href="<c:url value="customerExtContactForm.jhtm"><c:param name="id" value="${customerExtContact.id}"/><c:param name="uid" value="${customerExtContact.userId}"/></c:url>" class="nameLink">
			     <c:out value="${customerExtContact.name}" /></a>
			    </td>
			    <td align="center">
			    <c:out value="${customerExtContact.position}" />
			    </td>
			    <td align="center">
			     <c:out value="${customerExtContact.email}" />
			    </td>
			    <td align="center">
			      <c:out value="${customerExtContact.phone1}" />
			    </td>
			    <td align="center">
			      <c:out value="${customerExtContact.phone2}" />
			    </td>
			  </tr>
			</c:forEach>
			<c:if test="${customerExtContacts.nrOfElements == 0}">
			  <tr class="emptyList"><td colspan="3">&nbsp;</td></tr>
			</c:if>
			</table>
		  	<!-- end input field -->  	  
		  	</div>
		  	
		  	<!-- start button --> 
		  	<div align="left" class="button">
		  	 <c:if test="${siteConfig['MBLOCK'].value != 'true'}"> 
			  <input type="submit" name="__addContact" value="<fmt:message key="addContact"/>">
	   	    </c:if>
			</div>
		  	<!-- end button -->	
	  	
	<!-- end tab -->        
	</div>
	
