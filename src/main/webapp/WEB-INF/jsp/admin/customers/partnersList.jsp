<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<tiles:insertDefinition name="admin.customers" flush="true">
  <tiles:putAttribute name="content" type="string">
 
<form action="partnersList.jhtm" method="post" id="list" name="list_form"> 
  <!-- main box -->
  <div id="mboxfull">
  
  <!-- start table -->
  <table cellpadding="0" cellspacing="0" border="0" class="module" >
  <tr>
    <td class="topl_g">
	  
	  <!-- breadcrumb -->
	  <p class="breadcrumb">
	    <a href="../customers">Customer</a> &gt;
	    Partners list
	  </p>
      
	  </td><td class="topr_g" ></td></tr>
  <tr><td class="boxmidlrg" >

 <!-- tabs -->
 <div class="tab-wrapper">
	<h4 title="List of affiliates"></h4>
	<div>
	<!-- start tab -->
	<p>
	  	<div class="listdivi ln tabdivi"></div>
	  	<div class="listdivi"></div>
	  	
	  	<div class="listlight">
		  	<!-- input field --> 
		  	
		  	<table border="0" cellpadding="0" cellspacing="1" width="100%">
			  <tr>
			  <c:if test="${model.partnersList.nrOfElements > 0}">
			  <td class="pageShowing">
			  <fmt:message key="showing">
				<fmt:param value="${model.partnersList.firstElementOnPage + 1}"/>
				<fmt:param value="${model.partnersList.lastElementOnPage + 1}"/>
				<fmt:param value="${model.partnersList.nrOfElements}"/>
			  </fmt:message>
			  </td>
			  </c:if>
			  <td class="pageNavi">
			  Page 
			  <select name="page" id="page" onchange="submit()">
			  <c:forEach begin="1" end="${model.partnersList.pageCount}" var="page">
			  	<option value="${page}" <c:if test="${page == (model.partnersList.page+1)}">selected</c:if>>${page}</option>
			  </c:forEach>
			  </select>
			  of <c:out value="${model.partnersList.pageCount}"/>
			  | 
			  <c:if test="${model.partnersList.firstPage}"><span class="pageNaviDead">previous</span></c:if>
			  <c:if test="${not model.partnersList.firstPage}"><a href="<c:url value="partnersList.jhtm"><c:param name="page" value="${model.partnersList.page}"/><c:param name="id" value="${model.id}"/></c:url>" class="pageNaviLink">previous</a></c:if>
			  | 
			  <c:if test="${model.partnersList.lastPage}"><span class="pageNaviDead">next</span></c:if>
			  <c:if test="${not model.partnersList.lastPage}"><a href="<c:url value="partnersList.jhtm"><c:param name="page" value="${model.partnersList.page+2}"/><c:param name="id" value="${model.id}"/></c:url>" class="pageNaviLink">next</a></c:if>
			  </td>
			  </tr>
			</table>
			 
			 <table border="0" cellpadding="0" cellspacing="0" width="100%" class="listings">
			  <tr class="listingsHdr2">
			    <td class="listingsHdr3"><fmt:message key="partnerName" /></td>
			    <td class="listingsHdr3"><fmt:message key="created"/> <fmt:message key="date" /></td>
			    <td class="listingsHdr3"><fmt:message key="active"/></td>
			  </tr>
			  <c:forEach items="${model.partnersList.pageList}" var="partner" varStatus="status">
				<tr class="row${status.index % 2}" onmouseover="changeStyleClass(this,'rowOver')" onmouseout="changeStyleClass(this,'row${status.index % 2}')">
			    <td align="left"><a href="addPartners.jhtm?pid=${partner.id}"><c:out value="${partner.partnerName}"/></td>
			    <td align="left"><fmt:formatDate type="date" timeStyle="default" value="${partner.createdDate}"/></td>
			    <td align="left">
			    <c:choose>
					<c:when test="${partner.active}"><img src="../graphics/checkbox.png" alt="" title="" border="0"></c:when>
					<c:otherwise><img src="../graphics/box.png" alt="" title="" border="0"></c:otherwise>
				</c:choose>
			    </td>
			  </tr>
			  </c:forEach>
			  </table>
			  
			<table border="0" cellpadding="0" cellspacing="1" width="100%">
			  <tr>  
			  <td class="pageSize">
			    <select name="size" onchange="document.getElementById('page').value=1;submit()">
			    <c:forTokens items="10,25,50,100,500" delims="," var="current">
			  	  <option value="${current}" <c:if test="${current == model.partnersList.pageSize}">selected</c:if>>${current} <fmt:message key="perPage" /></option>
			    </c:forTokens>
			    </select>
			  </td>
			  </tr>
			</table>
		</div>
		
		<div align="left" class="button quickMode">
			    <input type="submit" name="__add" value="<fmt:message key="addPartnersMenuTitle"/>" />
	    </div>
	    
	    <!-- end tab -->        
	</div>
<!-- end tabs -->			
</div>
  
  <!-- end table -->
   </td><td class="boxmidr" ></td></tr>
  <tr><td class="botl"></td><td class="botr"></td></tr>
  </table>
	  
<!-- end main box -->  
</div>  
</form>		
  
  
  </tiles:putAttribute>
</tiles:insertDefinition>