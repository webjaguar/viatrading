<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="/WEB-INF/tlds/spring.tld" prefix="spring" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib uri="http://www.springframework.org/security/tags" prefix="sec" %>

<c:if test="${gSiteConfig['gPAYMENTS']}">
<tiles:insertDefinition name="admin.customers" flush="true">
  <tiles:putAttribute name="content" type="string">
<script language="JavaScript" type="text/JavaScript">
<!--
window.addEvent('domready', function(){			
			var tabs1 = new SimpleTabs($('tab-block-1'), { selector: 'h4'});
			var Tips1 = new Tips($$('.toolTipImg'));
		});
//-->
</script>

<form:form commandName="paymentForm" method="post">	  
<!-- main box -->
  <div id="mboxfull">
  
  <!-- start table -->
  <table cellpadding="0" cellspacing="0" border="0" class="module" >
  <tr>
    <td class="topl_g">
	  
	  <!-- breadcrumb -->
	  <p class="breadcrumb">
	    <a href="../customers"><fmt:message key="customer" /></a> &gt;
	    <a href="../customers/payments.jhtm"><fmt:message key="payment" /></a> &gt;
	    form 
	  </p>
	  
	  <!-- Error Message -->
	  <spring:hasBindErrors name="paymentForm">
		<div class="message">Please fix all errors!</div>
	  </spring:hasBindErrors>

  
  </td><td class="topr_g" ></td></tr>
  <tr><td class="boxmidlrg" >

 <!-- tabs -->
 <div id="tab-block-1">
	<h4 title="Description"><fmt:message key="payment" /></h4>
	<div>
	<!-- start tab -->
	<p>
        <c:set var="classIndex" value="0" />
        
	  	<div class="listdivi ln tabdivi"></div>
	  	<div class="listdivi"></div>
	   
		  	<div class="list${classIndex % 2}"><c:set var="classIndex" value="${classIndex+1}" />
		  	<div class="listfl"><fmt:message key='company' />:</div>
		  	<div class="listp">
		  	<!-- input field -->
				<c:out value="${paymentForm.payment.customer.address.company}"/>
		    <!-- end input field -->   	
		  	</div>
		  	</div>

		  	<div class="list${classIndex % 2}"><c:set var="classIndex" value="${classIndex+1}" />
		  	<div class="listfl"><fmt:message key='firstName' />:</div>
		  	<div class="listp">
		  	<!-- input field -->
				<c:out value="${paymentForm.payment.customer.address.firstName}"/>
		    <!-- end input field -->   	
		  	</div>
		  	</div>
		  	
		  	<div class="list${classIndex % 2}"><c:set var="classIndex" value="${classIndex+1}" />
		  	<div class="listfl"><fmt:message key='lastName' />:</div>
		  	<div class="listp">
		  	<!-- input field -->
				<c:out value="${paymentForm.payment.customer.address.lastName}"/>
		    <!-- end input field -->   	
		  	</div>
		  	</div>
		  	
		  	<div class="list${classIndex % 2}"><c:set var="classIndex" value="${classIndex+1}" />
		  	<div class="listfl"><fmt:message key='emailAddress' />:</div>
		  	<div class="listp">
		  	<!-- input field -->
				<c:out value="${paymentForm.payment.customer.username}"/>
		    <!-- end input field -->   	
		  	</div>
		  	</div>

		  	<div class="list${classIndex % 2}"><c:set var="classIndex" value="${classIndex+1}" />
		  	<div class="listfl"><div class="requiredField"><fmt:message key="date" />:</div></div>
		  	<div class="listp">
		  	<!-- input field -->
				<form:input path="payment.date" size="10" maxlength="10" />
				  <img class="calendarImage"  id="payment_date_trigger" src="../graphics/calendarIcon.jpg" />
				  <script type="text/javascript">
		    		Calendar.setup({
		        			inputField     :    "payment.date",   // id of the input field
		        			showsTime      :    false,
		        			ifFormat       :    "%m/%d/%Y",   // format of the input field
		        			button         :    "payment_date_trigger"   // trigger for the calendar (button ID)
		    		});	
				  </script> 
				<form:errors path="payment.date" cssClass="error"/>
		    <!-- end input field -->   	
		  	</div>
		  	</div>

		  	<div class="list${classIndex % 2}"><c:set var="classIndex" value="${classIndex+1}" />
		  	<div class="listfl"><div class="requiredField"><fmt:message key="memo" />:</div></div>
		  	<div class="listp">
		  	<!-- input field -->
				<form:input path="payment.memo" size="40" maxlength="50" />
				<form:errors path="payment.memo" cssClass="error"/>
		    <!-- end input field -->   	
		  	</div>
		  	</div>
		  	
		  	<div class="list${classIndex % 2}"><c:set var="classIndex" value="${classIndex+1}" />
		  	<div class="listfl"><fmt:message key="paymentMethod" />:</div>
		  	<div class="listp">
		  	<!-- input field -->
				<form:select path="payment.paymentMethod">
			    <form:option value="" label="Please Select"/>
			    <c:forEach items="${customPayments}" var="paymentMethod">
				  <c:if test="${paymentMethod.enabled}">
			        <form:option value="${paymentMethod.title}"/>
			      </c:if>
			    </c:forEach>
			  </form:select>
		    <!-- end input field -->   	
		  	</div>
		  	</div>

		  	<div class="list${classIndex % 2}"><c:set var="classIndex" value="${classIndex+1}" />
		  	<div class="listfl"><div class="requiredField"><fmt:message key="amount" />:</div></div>
		  	<div class="listp">
		  	<!-- input field -->
		  	<c:choose>
		  	<c:when test="${paymentForm.payment.amount != null and paymentForm.payment.amount !=''}">
		  		<lable><b><c:out value="${paymentForm.payment.amount}"/></b></lable>		
		  	</c:when>
		  	<c:otherwise>
				<form:input path="payment.amount" size="10" maxlength="10" />
				<form:errors path="payment.amount" cssClass="error"/>		  
		  	</c:otherwise>
		  	</c:choose>
		    <!-- end input field -->   	
		  	</div>
		  	</div>
		  	
	<!-- end tab -->        
	</div>         
  
  <h4 title="invoices">Invoices (with this payment)</h4>
	<div>
	<!-- start tab -->
	<p>
	  	<div class="listdivi ln tabdivi"></div>
	  	<div class="listdivi"></div>
	   
		  	<div class="list${classIndex % 2}"><c:set var="classIndex" value="${classIndex+1}" />
		  	<div class="listpp">
		  	<!-- input field -->
			    <c:forEach items="${paymentForm.invoiceList}" var="order" varStatus="status">
				<c:if test="${status.first}">
				<table class="form" width="100%" cellspacing="5">
				  <tr>
				    <td>&nbsp;</td>
				    <td align="center"><b><fmt:message key="orderNumber" /></b></td>
				    <td align="center"><b><fmt:message key="orderDate" /></b></td>
				    <td align="center"><b><fmt:message key="status" /></b></td>
				    <td align="right"><b><fmt:message key="grandTotal" /></b></td>
				    <td align="right"><b><fmt:message key="paid" /></b></td>
				    <td align="right"><b><fmt:message key="balance" /></b></td>
				    <td align="right"><b><fmt:message key="firstPayment" /></b></td>
				    <td align="center"><b><fmt:message key="payment" /></b></td>
				    <td align="center"><b><fmt:message key="cancelled" /> <fmt:message key="amount" /></b></td>
				    <td width="20%">&nbsp;</td>
				  </tr>
				</c:if>
				  <tr>
				    <td style="width:50px;text-align:right"><c:out value="${status.index + 1}"/>. </td>
				    <td align="center"><a href="../orders/invoice.jhtm?order=${order.orderId}" class="nameLink"><c:out value="${order.orderId}"/></a></td>
				    <td align="center"><fmt:formatDate type="date" timeStyle="default" value="${order.dateOrdered}"/></td>
					<td align="center"><c:choose><c:when test="${order.status != null}"><fmt:message key="orderStatus_${order.status}" /></c:when><c:otherwise>&nbsp;</c:otherwise></c:choose></td>
					<td align="right"><fmt:formatNumber value="${order.grandTotal}" pattern="#,##0.00" /></td>
					<c:choose>
					<c:when test="${firstPaymentMap[order.orderId] == false }">
						<td align="right"><fmt:formatNumber value="${order.amountPaid}" pattern="#,##0.00" /></td>	
						<td align="right"><fmt:formatNumber value="${order.balance}" pattern="#,##0.00" /></td>			
					</c:when>
					<c:otherwise>
						<td align="right"><fmt:formatNumber value="0" pattern="#,##0.00" /></td>
						<td align="right"><fmt:formatNumber value="${order.grandTotal}" pattern="#,##0.00" /></td>			
					</c:otherwise>
					</c:choose>
					
					<td align="right">
						<c:if test="${firstPaymentMap[order.orderId] == true }">
							<img border="0" title="primary" alt="primary" src="../graphics/checkbox.png">
						</c:if>
					</td>
				    <td align="center">
				    <c:choose>
				    <c:when test="${order.cancelledPayment}">
				    	<lable><b><fmt:message key="cancelledPayment"/></b></lable>
				    </c:when>
				    <c:otherwise>
				    	<input type="text" name="__payment_${order.orderId}" value="<c:out value="${order.payment}"/>" maxlength="10" size="10">
				    </c:otherwise>
				    </c:choose>
				    </td>
				    <td align="center">
				    	<lable><b><fmt:formatNumber value="${order.paymentCancelledAmt}" pattern="#,##0.00" /></b></lable>
				    </td>
					<c:choose><c:when test="${order.payment > order.balance}"><td class="error"><fmt:message key="form.overPayment" /></td></c:when><c:otherwise><td>&nbsp;</td></c:otherwise></c:choose>		
				  </tr>
				<c:if test="${status.last}">
				  <tr>
					<td colspan="8" align="right"><b><fmt:message key="total" /></b></td>
				    <td align="center">
						<input type="text" id="total" value="<c:out value="${paymentForm.totalPayments}"/>" maxlength="10" size="10" readonly="readonly">
				    </td>
				  </tr>
				</table>
				</c:if>
				</c:forEach>
		    <!-- end input field -->   	
		  	</div>
		  	</div>

	<!-- end tab -->        
	</div> 
	
<!-- end tabs -->			
</div>
<!-- start button -->
<div align="left" class="button"> 
<sec:authorize ifAnyGranted="ROLE_AEM,ROLE_ADMIN_${prop_site_id},ROLE_INCOMING_PAYMENT_CREATE">
<c:if test="${paymentForm.newPayment}">
  <input type="submit" value="<fmt:message key="add"/>">
</c:if>
</sec:authorize>
<c:if test="${!paymentForm.newPayment and paymentUpdate}">		
  <input type="submit" value="<fmt:message key="update"/>">
</c:if>
  <input type="submit" value="<fmt:message key="cancel"/>" name="_cancel">
</div>
<!-- end button -->	     
  
  <!-- end table -->
    </td><td class="boxmidr" ></td></tr>
  <tr><td class="botl"></td><td class="botr"></td></tr>
  </table>
  
<!-- end main box -->  
</div>
</form:form>

  </tiles:putAttribute>    
</tiles:insertDefinition>
</c:if>