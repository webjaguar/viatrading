<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<tiles:insertDefinition name="admin.customers" flush="true">
  <tiles:putAttribute name="content" type="string">
  
<form action="partnerCustomerList.jhtm" id="list" method="post">
<!-- main box -->
  <div id="mboxfull">
  
  <!-- start table -->
  <table cellpadding="0" cellspacing="0" border="0" class="module" >
  <tr>
    <td class="topl_g">
	  
	  <!-- breadcrumb -->
	  <p class="breadcrumb">
	    <a href="../customers">Customers</a> &gt; <fmt:message key="partnerList" /> for <c:out value="${customer.username}"/>
	  </p>
	  
	  <!-- Error -->
	  <div class="message2">
	  </div>
	  <c:if test="${!empty model.message}">
		<div class="message"><fmt:message key="${model.message}" /></div>
	  </c:if>  
	  
  </td><td class="topr_g" ></td></tr>
  <tr><td class="boxmidlrg" >
  
	
	
<!-- tabs -->
 <div class="tab-wrapper">
	<h4 class="quickMode" title=""></h4>
	<div>
	
    <div class="listdivi quickMode"></div>
	   
	<div class="listlight">
	<!-- input field -->  
			<table border="0" cellpadding="0" cellspacing="1" width="100%">
			  <tr>
			  <c:if test="${partnerList.nrOfElements > 0}">
			  <td class="pageShowing">
			  <fmt:message key="showing">
				<fmt:param value="${partnerList.firstElementOnPage + 1}"/>
				<fmt:param value="${partnerList.lastElementOnPage + 1}"/>
				<fmt:param value="${partnerList.nrOfElements}"/>
			  </fmt:message>
			  </td>
			  </c:if>
			  <td class="pageNavi">
			  Page 
			  <select name="page" id="page" onchange="submit()">
			  <c:forEach begin="1" end="${partnerList.pageCount}" var="page">
			  	<option value="${page}" <c:if test="${page == (partnerList.page+1)}">selected</c:if>>${page}</option>
			  </c:forEach>
			  </select>
			  of <c:out value="${partnerList.pageCount}"/>
			  | 
			  <c:if test="${partnerList.firstPage}"><span class="pageNaviDead">previous</span></c:if>
			  <c:if test="${not partnerList.firstPage}"><a href="<c:url value="addressList.jhtm"><c:param name="page" value="${partnerList.page}"/><c:param name="id" value="${model.id}"/></c:url>" class="pageNaviLink">previous</a></c:if>
			  | 
			  <c:if test="${partnerList.lastPage}"><span class="pageNaviDead">next</span></c:if>
			  <c:if test="${not partnerList.lastPage}"><a href="<c:url value="partnerCustomerList.jhtm"><c:param name="page" value="${partnerList.page+2}"/><c:param name="id" value="${model.id}"/></c:url>" class="pageNaviLink">next</a></c:if>
			  </td>
			  </tr>
			</table>
			
	<!-- start tab -->
		<table border="0" cellpadding="0" cellspacing="0" width="100%" class="listings">
		  <tr class="listingsHdr2">
		  	<td align="left" class="listingsHdr3">
		  		<fmt:message key="partnerName"/>
		  	</td>
		  	<td align="left" class="listingsHdr3">
		  		<fmt:message key="amount"/>			  		
		  	</td>
		  </tr>
		  <c:forEach items="${partnerList.pageList}" var="partner">
		  <tr>
		  	<td class="nameCol">
		  		<a href="partnerCustomerForm.jhtm?cId=${customer.id}&pId=${partner.partnerId}"><c:out value="${partner.partnerName}"/></a>
		  	</td>
		  	<td class="nameCol">
		  		<c:out value="${partner.amount}"/>
		  	</td>
		  </tr>
		  </c:forEach>
		  <c:if test="${partnerList.nrOfElements == 0}">
			  <tr class="emptyList"><td colspan="3">&nbsp;</td></tr>
		  </c:if>
		</table>
		
		<table border="0" cellpadding="0" cellspacing="1" width="100%">
		  <tr>  
		  <td class="pageSize">
		    <select name="size" onchange="document.getElementById('page').value=1;submit()">
		    <c:forTokens items="10,25,50,100,500" delims="," var="current">
		  	  <option value="${current}" <c:if test="${current == partnerList.pageSize}">selected</c:if>>${current} <fmt:message key="perPage" /></option>
		    </c:forTokens>
		    </select>
		  </td>
		  </tr>
		</table>
	</div>
	</div>
</div>	

	<!-- start button -->
	<div align="left" class="button">
		<input type="hidden" name="cId" value="${cId}"/>
		<input type="submit" name="__add" value="<fmt:message key="addPartnersMenuTitle"/>" />
	</div>
	<!-- end button -->	
	
<!-- end table -->
    </td><td class="boxmidr" ></td></tr>
  <tr><td class="botl"></td><td class="botr"></td></tr>
  </table>
  
<!-- end main box -->  
</div>
</form>

  </tiles:putAttribute>    
</tiles:insertDefinition>