<%@ page import="com.webjaguar.model.*,java.util.*"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/security/tags" prefix="sec" %>

<link href="<c:url value="/assets/invoice.css"/>" rel="stylesheet" type="text/css"/>
<link rel="stylesheet" type="text/css" media="all" href="../../admin/javascript/jscalendar-1.0/skins/aqua/theme.css" title="Aqua" /> <!-- calendar stylesheet -->
<script type="text/javascript" src="../../admin/javascript/jscalendar-1.0/calendar.js"></script> <!-- main calendar program -->
<script type="text/javascript" src="../../admin/javascript/jscalendar-1.0/lang/calendar-en.js"></script>
<script type="text/javascript" src="../../admin/javascript/jscalendar-1.0/calendar-setup.js"></script>
<script src="../javascript/MenuMatic_0.68.3.js" type="text/javascript" charset="utf-8"></script>
<script type="text/javascript">
<!--
window.addEvent('domready', function(){	
	var Tips1 = new Tips($$('.toolTipImg'));
	var myMenu = new MenuMatic({ orientation:'vertical' });
	var opSearchBox = new multiBox('mbOperator', {showControls : false, useOverlay: false, showNumbers: false });
	var opNumOrderSearchBox = new multiBox('mbNumOrderOperator', {showControls : false, useOverlay: false, showNumbers: false });
	var optotalOrderSearchBox = new multiBox('mbTotalOrderOperator', {showControls : false, useOverlay: false, showNumbers: false });
});
function selectOperator(e, type) {
	 if(type == 'numOrder') {
		document.getElementById('numOrderOperator').value = e;
	} else if(type == 'totalOrder') {
		document.getElementById('totalOrderOperator').value = e;
	}
}
function UpdateStartEndDate(type){
var myDate = new Date();
$('endDate').value = (myDate.getMonth()+1) + "/" + myDate.getDate() + "/" + myDate.getFullYear();
	if ( type == 0) {
		var day = myDate.getDate()-1;
		if(day == 0) {
			// 1 day = 1 * 24 hours = 24 hours 
			// 24 hours = 24 * 60 min = 1440 min 
			// 1440 min = 1440 * 60 sec = 86400 sec 
			// 86400 sec = 86400 * 1000 ms = 86400000 ms 
			var yesterday = new Date(myDate.getTime() - 86400000);
			$('startDate').value = (yesterday.getMonth()+1) + "/" + yesterday.getDate() + "/" +yesterday.getFullYear();
			$('endDate').value = (yesterday.getMonth()+1) + "/" +yesterday.getDate() + "/" + yesterday.getFullYear();
		} else {
			$('startDate').value = (myDate.getMonth()+1) + "/" + day + "/" + myDate.getFullYear();
			$('endDate').value = (myDate.getMonth()+1) + "/" + day + "/" + myDate.getFullYear();
		}
	} else if ( type == 2 ) {
		myDate.setDate(myDate.getDate() - myDate.getDay());
		$('startDate').value = (myDate.getMonth()+1) + "/" + myDate.getDate() + "/" + myDate.getFullYear();
	} else if ( type == 3 ) {
		myDate.setDate(1);
		$('startDate').value = (myDate.getMonth()+1) + "/" + myDate.getDate() + "/" + myDate.getFullYear();
	}else if ( type == 10 ) {
		myDate.setMonth(0, 1);
		$('startDate').value = (myDate.getMonth()+1) + "/" + myDate.getDate() + "/" + myDate.getFullYear();
	}else if ( type == 20 ) {
		$('startDate').value = "";
		$('endDate').value = "";
	}else{
		$('startDate').value = (myDate.getMonth()+1) + "/" + myDate.getDate() + "/" + myDate.getFullYear();
	}
}
function UpdateCartStartEndDate(type){
var myDate = new Date();
$('cartEndDate').value = (myDate.getMonth()+1) + "/" + myDate.getDate() + "/" + myDate.getFullYear();
	if ( type == 2 ) {
		myDate.setDate(myDate.getDate() - myDate.getDay());
		$('cartStartDate').value = (myDate.getMonth()+1) + "/" + myDate.getDate() + "/" + myDate.getFullYear();
	} else if ( type == 3 ) {
		myDate.setDate(1);
		$('cartStartDate').value = (myDate.getMonth()+1) + "/" + myDate.getDate() + "/" + myDate.getFullYear();
	}else if ( type == 10 ) {
		myDate.setMonth(0, 1);
		$('cartStartDate').value = (myDate.getMonth()+1) + "/" + myDate.getDate() + "/" + myDate.getFullYear();
	}else if ( type == 20 ) {
		$('cartStartDate').value = "";
		$('cartEndDate').value = "";
	}else{
		$('cartStartDate').value = (myDate.getMonth()+1) + "/" + myDate.getDate() + "/" + myDate.getFullYear();
	}
}
function UpdateLoginStartEndDate(type){
var myDate = new Date();
$('loginEndDate').value = (myDate.getMonth()+1) + "/" + myDate.getDate() + "/" + myDate.getFullYear();
	if ( type == 2 ) {
		myDate.setDate(myDate.getDate() - myDate.getDay());
		$('loginStartDate').value = (myDate.getMonth()+1) + "/" + myDate.getDate() + "/" + myDate.getFullYear();
	} else if ( type == 3 ) {
		myDate.setDate(1);
		$('loginStartDate').value = (myDate.getMonth()+1) + "/" + myDate.getDate() + "/" + myDate.getFullYear();
	}else if ( type == 10 ) {
		myDate.setMonth(0, 1);
		$('loginStartDate').value = (myDate.getMonth()+1) + "/" + myDate.getDate() + "/" + myDate.getFullYear();
	}else if ( type == 20 ) {
		$('loginStartDate').value = "";
		$('loginEndDate').value = "";
	}else{
		$('loginStartDate').value = (myDate.getMonth()+1) + "/" + myDate.getDate() + "/" + myDate.getFullYear();
	}
}
function submitSearchForm(fileType) {
	if (typeof document.forms['searchform'] != 'undefined') {
	  document.searchform.action = "customerExport.jhtm?fileType2="+fileType;
	  document.searchform.submit();
	} else {
	  myform=document.createElement("form");
	  document.body.appendChild(myform);
      myform.method = "POST";
      myform.action= "customerExport.jhtm?fileType2="+fileType;
      myform.submit();
	}
}
//-->
</script>  
<!-- menu -->
  <div id="lbox" class="quickMode">
   <table cellpadding="0" cellspacing="0" border="0" class="module" style="width: 100%"><tr><td class="topl">
    </td><td class="topr" ></td></tr><tr><td class="boxmid" >
    
  	<c:if test="${param.tab != 'vbaOrders' and param.tab != 'vbaPaymentReport'}">
    <h2 class="menuleft mfirst"><fmt:message key="customersMenuTitle"/></h2>
    <div class="menudiv"></div>
    <ul id="nav">
      <li><a href="customerList.jhtm" class="userbutton"><fmt:message key="list"/></a></li>
      <c:if test="${siteConfig['LUCENE_REINDEX'].value == 'true'}">
      	<li><a href="lCustomerList.jhtm" class="userbutton">Lucene <fmt:message key="list"/></a></li>
      	<li><a href="customerIndex.jhtm" class="userbutton">Lucene Index Now</a></li>
      </c:if>
      <c:if test="${gSiteConfig['gGROUP_CUSTOMER']}">
	    <li><a href="customerGroupList.jhtm" class="userbutton"><fmt:message key="customer"/> <fmt:message key="groups"/></a></li> 
	  </c:if>
      <li><a href="customerImport.jhtm" class="userbutton"><fmt:message key="import"/></a></li>
      <li><a href="#" class="userbutton"><fmt:message key="export"/></a>
        <ul>
           <li><a href="javascript: submitSearchForm('xls')" class="userbutton">EXCEL</a></li>
           <li><a href="javascript: submitSearchForm('csv')" class="userbutton">CSV</a></li>     
        </ul>
      </li>
      <c:if test="${gSiteConfig['gCUSTOMER_FIELDS'] > 0}">
	    <sec:authorize ifAnyGranted="ROLE_AEM,ROLE_ADMIN_${prop_site_id},ROLE_CUSTOMER_FIELD">  
      	  <li><a href="customerFields.jhtm" class="userbutton"><fmt:message key="Fields"/></a></li>
	  	</sec:authorize>
      </c:if>
	  <c:if test="${gSiteConfig['gPAYMENTS']}">
	    <li><a href="#" class="userbutton"><fmt:message key="payments"/></a>
        <ul>
           <sec:authorize ifAnyGranted="ROLE_AEM,ROLE_ADMIN_${prop_site_id},ROLE_INCOMING_PAYMENT_VIEW_LIST"> 
           	<li><a href="payments.jhtm?cid=" class="userbutton"><fmt:message key="incomingPayments"/></a></li>
           </sec:authorize>
           <sec:authorize ifAnyGranted="ROLE_AEM,ROLE_ADMIN_${prop_site_id},ROLE_OUTGOING_PAYMENT_VIEW_LIST">
           <c:if test="${gSiteConfig['gVIRTUAL_BANK_ACCOUNT']}">
           	<li><a href="virtualBankPayment.jhtm" class="userbutton"><fmt:message key="outgoingPayments"/></a></li>            
           </c:if>	
      	   </sec:authorize>   
        </ul>
      </li> 
	  </c:if> 
	  <sec:authorize ifAnyGranted="ROLE_AEM,ROLE_ADMIN_${prop_site_id},ROLE_EMAILMANAGER_CREATE">
	  <c:if test="${gSiteConfig['gMASS_EMAIL'] and siteConfig['CUSTOMER_MASS_EMAIL'].value == 'true'}">
	  <li><a href="#" class="userbutton"><fmt:message key="customerNotify"/></a>
        <ul>
           	<li><a href="customerMassEmail.jhtm" class="userbutton"><fmt:message key="massEmail"/></a></li>
           	<li><a href="customerMassTextMessage.jhtm" class="userbutton"><fmt:message key="massTextMessage"/></a></li>               
        </ul>
      </li> 	
        
      </c:if>
      </sec:authorize>
      <c:if test="${siteConfig['BUDGET_ADD_PARTNER'].value == 'true'}">
    	<li><a href="partnersList.jhtm" class="userbutton"><fmt:message key="partnerList" /></a></li>
      </c:if>
    </ul>  
    <div class="menudiv"></div>
    </c:if>
    
   <c:if test="${gSiteConfig['gAFFILIATE'] > 0 and (param.tab == 'affiliate' or param.tab == 'commissions')}" >
    <h2 class="menuleft"><fmt:message key="affiliateMenuTitle"/></h2>   
    <div class="menudiv"></div>
	   <a href="../customers/affiliateList.jhtm" class="userbutton"><fmt:message key="affiliateMenuTitleList"/></a>
    <div class="menudiv"></div>
    
      <div align="left" class="searchHeader" id="searchHeaderId"><h2><fmt:message key="search" /></h2></div>
	  <table class="searchBoxLeft"><tr><td>
	  <form action="affiliateList.jhtm" name="searchform" method="post">
	  <!-- content area -->
	    <div class="search2">
	      <p><fmt:message key="firstName" />:</p>
	      <input name="firstName" id="firstName" type="text" value="<c:out value='${affiliateSearch.firstName}' />" size="15" />
	    </div> 
	    <div class="search2">
	      <p><fmt:message key="lastName" />:</p>
	      <input name="lastName" id="lastName" type="text" value="<c:out value='${affiliateSearch.lastName}' />" size="15" />
	    </div>  
	    <c:if test="${gSiteConfig['gSALES_REP']}">
	    <div class="search2">
	      <p><fmt:message key="salesRep" />:</p>
	      <select name="sales_rep_id">
	       <option value="-1"></option>
	       <option value="-2" <c:if test="${affiliateSearch.salesRepId == -2}">selected</c:if>><fmt:message key="noSalesRep" /></option>
	       <c:forEach items="${model.salesRepMap}" var="salesRep">
	  	     <option value="${salesRep.value.id}" <c:if test="${affiliateSearch.salesRepId == salesRep.value.id}">selected</c:if>><c:out value="${salesRep.value.name}" /></option>
	       </c:forEach>
	      </select>
	      </div>
	    </c:if>
	    <div class="search2">
	      <p><fmt:message key="accountNumber" />:</p>
          <input name="accountNumber" type="text" value="<c:out value='${affiliateSearch.accountNumber}' />" size="15" />   
	    </div>
	    <div class="search2">
	      <p><fmt:message key="email" />:</p>
	      <input name="email" type="text" value="<c:out value='${affiliateSearch.email}' />" size="15" />
	    </div>
	    <div class="button">
	      <input type="submit" value="Search"/>
		</div>
	  <!-- content area -->
	  </form>
	  </td></tr></table>
	<div class="menudiv"></div>  

	
	<c:if test="${param.tab == 'commissions'}"> 
    <div class="menudiv"></div>
      <div align="left" class="searchHeader" id="searchHeaderId"><h2><fmt:message key="search" /></h2></div>
	  <table class=searchBoxLeft><tr><td>
	  <form action="commissionList.jhtm" method="post"> 
	  <input type="hidden" name="id" id="id" /> 
	  <!-- content area -->
	  	<div class="search2">
	      <p>Level:</p>
	      <select name="commission_level" onChange="document.getElementById('page').value=1;document.getElementById('id').value=${model.owner.id};submit()">
		  <c:forEach begin="1" end="${gSiteConfig['gAFFILIATE']}" var="thisLevel">
		    <option value="${thisLevel}" <c:if test="${model.level == thisLevel}">selected="selected"</c:if>>Level ${thisLevel}</option>
	      </c:forEach>
	      </select>
	    </div> 
	    <div class="search2">
	      <p>Order# :</p>
	      <input name="orderNum" type="text" value="<c:out value='${commissionSearch.orderNum}' />" size="15" />
	    </div>         
	    <div class="button">
	      <input type="submit" value="Search" onClick="document.getElementById('id').value=${model.owner.id};submit()"/>
	    </div>
	  <!-- content area -->
	  </form>
	  </td></tr></table>
	<div class="menudiv"></div>  
	</c:if>
	</c:if>

    <c:if test="${param.tab == 'customers'}"> 
    <script type="text/javascript" src="../../admin/javascript/autocompleter1.2.js"></script>
	<script type="text/javascript" src="../../admin/javascript/autocompleter.Request1.2.js"></script>
	<script type="text/javascript" src="../../admin/javascript/autocompleter.Local1.2.js"></script>
	<script type="text/javascript" src="../../admin/javascript/observer.js"></script>
    <script type="text/javascript" src="../javascript/side-bar.js"></script>
    <script type="text/javascript">
<!--
window.addEvent('domready', function(){
	var firstName = $('firstName');
	var lastName = $('lastName');
	new Autocompleter.Request.HTML(firstName, '../customers/show-ajax-customers.jhtm', {
		'indicatorClass': 'autocompleter-loading',
		'postData': { 'search': 'firstname' },
		'injectChoice': function(choice) {
			var text = choice.getFirst();
			var value = text.innerHTML;
			choice.inputValue = value;
			text.set('html', this.markQueryValue(value));
			this.addChoiceEvents(choice);
		},
		'onSelection': function(element, selected, value, input) {
			lastName.value = selected.getFirst().getNext().getNext().innerHTML;
		}
	});
	
	new Autocompleter.Request.HTML(lastName, '../customers/show-ajax-customers.jhtm', {
		'indicatorClass': 'autocompleter-loading',
		'postData': { 'search': 'lastname' },
		'injectChoice': function(choice) {
			var text = choice.getFirst();
			var value = text.innerHTML;
			choice.inputValue = value;
			text.set('html', this.markQueryValue(value));
			this.addChoiceEvents(choice);
		},
		'onSelection': function(element, selected, value, input) {
			firstName.value = selected.getFirst().getNext().getNext().innerHTML;
		}
	});
});
//-->
</script> 
    <div class="leftbar_searchWrapper">
      <div align="left" class="searchHeader" id="searchHeaderId"><h2><fmt:message key="search" /></h2></div>
      <div class="searchformBox">
	  <form name="searchform" action="customerList.jhtm" method="post" >
	  <table class="searchBoxLeft"><tr><td valign="top" style="width: 110px;">
	  <!-- content area -->
		<c:if test="${gSiteConfig['gMULTI_STORE'] > 0}">
	    <div class="search2">
	      <p><fmt:message key="multiStore" />:</p>
	      <select name="host">
	  	    <option value=""><fmt:message key="all" /> <fmt:message key="accounts" /></option>
	  	    <option value="main" <c:if test="${customerSearch.host == 'main'}">selected</c:if>><fmt:message key="main" /></option>
	  	    <c:forEach items="${multiStores}" var="multiStore">
	  	    <option value="${multiStore.host}" <c:if test="${customerSearch.host == multiStore.host}">selected</c:if>><c:out value="${multiStore.host}" /></option>
	  	    </c:forEach>
	       </select>
	    </div>
	    </c:if>
	    <div class="search2">
	      <p><fmt:message key="firstName" />:</p>
	      <input name="firstName" id="firstName" type="text" value="<c:out value='${customerSearch.firstName}' />" size="15" />
	    </div> 
	    <div class="search2">
	      <p><fmt:message key="lastName" />:</p>
	      <input name="lastName" id="lastName" type="text" value="<c:out value='${customerSearch.lastName}' />" size="15" />
	    </div> 
	    <div class="search2">
	      <p><fmt:message key="email" />:</p>
	      <input name="email" type="text" value="<c:out value='${customerSearch.email}' />" size="15" />
	    </div>
	    <div class="search2">
          <p><fmt:message key="companyName" />:</p>
          <input name="companyName" type="text" value="<c:out value='${customerSearch.companyName}' />" size="15" />
        </div>
        <div class="search2">
          <p><fmt:message key="multipleSearch" />:</p>
          <input name="multipleSearch" type="text" value="<c:out value='${customerSearch.multipleSearchString}' />" size="15" />
        </div>
	    <div class="search2">
	      <p><fmt:message key="accountNumber" />:</p>
          <input name="accountNumber" type="text" value="<c:out value='${customerSearch.accountNumber}' />" size="15" />   
	    </div>
	    <c:if test="${gSiteConfig['gSALES_REP']}">
	    <div class="search2">
	      <p><fmt:message key="salesRep" />:</p>
	      <select name="sales_rep_id">
		   <c:if test="${model.salesRepTree == null}">
	       <option value="-1"></option>
	       <option value="-2" <c:if test="${customerSearch.salesRepId == -2}">selected</c:if>><fmt:message key="noSalesRep" /></option>
	       <c:forEach items="${model.activeSalesReps}" var="salesRep">
	  	     <option value="${salesRep.id}" <c:if test="${customerSearch.salesRepId == salesRep.id}">selected</c:if>><c:out value="${salesRep.name}" /></option>
	       </c:forEach>
	       <c:forEach items="${model.nonActiveSalesReps}" var="salesRep">
	  	     <option class="nameLinkInactive" value="${salesRep.id}" <c:if test="${customerSearch.salesRepId == salesRep.id}">selected</c:if>><c:out value="${salesRep.name}" /></option>
	       </c:forEach>
	       </c:if>
	       <c:if test="${model.salesRepTree != null}">
	         <c:set var="salesRepTree" value="${model.salesRepTree}" scope="request"/>
	         <c:set var="customerSearchSalesRepId" value="${customerSearch.salesRepId}" scope="request"/>
	         <option value="${salesRepTree.id}" <c:if test="${customerSearch.salesRepId == salesRepTree.id}">selected</c:if>><c:out value="${salesRepTree.name}" /></option>
			 <c:set var="level" value="0" scope="request"/>
	      	 <c:set var="recursionPageId" value="${customerSearchSalesRepId}" scope="request"/>
	      	 <jsp:include page="/WEB-INF/jsp/admin/customers/recursion.jsp"/>
	       </c:if>
	      </select>
	    </div>  
	    </c:if>
	    <c:if test="${gSiteConfig['gGROUP_CUSTOMER']}">
	    <div class="search2">
	      <p><fmt:message key="customer"/> <fmt:message key="group" />:</p>
	      <select name="groupId">
	         <option value=""></option>
	      <c:forEach items="${model.groups}" var="group">
	  	     <option value="${group.id}" <c:if test="${customerSearch.groupId == group.id}">selected</c:if>><c:out value="${group.name}" /></option>
	       </c:forEach>
	      </select>
	    </div>  
	    </c:if>	     	    
	    <c:if test="${gSiteConfig['gSHOPPING_CART'] and siteConfig['SHOPPINGCART_SEARCH'].value == 'true'}">
	    <div class="search2">
	      <p><fmt:message key="shoppingCart" />:</p>
	      <select name="cartNotEmpty">
	         <option value=""></option>
	         <option value="true" <c:if test="${customerSearch.cartNotEmpty != null and customerSearch.cartNotEmpty}" >selected</c:if>><fmt:message key="notEmpty" /></option>
             <option value="false" <c:if test="${customerSearch.cartNotEmpty != null and not customerSearch.cartNotEmpty}" >selected</c:if>><fmt:message key="empty" /></option>	         
	      </select>
	    </div>
	    <div class="search2">
	      <p><fmt:message key="shoppingCartTotal" />:</p>
	      <input name="shoppingCartTotal" type="text" value="<fmt:formatNumber value="${customerSearch.shoppingCartTotal}" pattern="###0.00"/>" size="15" />
	    </div> 
	    </c:if>
	    <div class="search2">
	      <p><fmt:message key="loggedIn" />:</p>
	      <select name="loggedInFlag">
	         <option value=""></option>
	         <option value="1" <c:if test="${customerSearch.loggedInFlag == '1'}" >selected</c:if>>Yes</option>       
	      </select>
	    </div>
	    <div class="search2">
	      <p><fmt:message key="rating" /> 1:</p>
	      <select name="rating1">
	         <option value=""></option>
	         <c:forTokens items="${siteConfig['CUSTOMER_RATING_1'].value}" delims="," var="rating1">
				   <option value="${rating1}" <c:if test="${customerSearch.rating1 == rating1}" >selected</c:if>><c:out value="${rating1}" /></option>
		  	   </c:forTokens>
	      </select>
	    </div>
	    <div class="search2">
	      <p><fmt:message key="rating" /> 2:</p>
	      <select name="rating2">
	      	<option value=""></option>
	        <c:forTokens items="${siteConfig['CUSTOMER_RATING_2'].value}" delims="," var="rating2">
				  <option value="${rating2}" <c:if test="${customerSearch.rating2 == rating2}" >selected</c:if>><c:out value="${rating2}" /></option>
		  	  </c:forTokens>
	      </select>
	    </div>
	    <div class="search2">
	      <p><fmt:message key="qualifier" />:</p>
	      <select name="qualifier">
	       <option value="-1"></option>
	       <option value="-2" <c:if test="${customerSearch.qualifierId == -2}">selected</c:if>><fmt:message key="noQualifier" /></option>
	       <c:forEach items="${model.activeQualifiers}" var="qualifier">
	  	     <option value="${qualifier.id}" <c:if test="${customerSearch.qualifierId == qualifier.id}">selected</c:if>><c:out value="${qualifier.name}" /></option>
	       </c:forEach>
	      </select>
	    </div>  
	    <div class="search2">
	      <p><fmt:message key="suspend" />:</p>
	      <select name="suspended">
	         <option value=""></option>
	         <option value="true" <c:if test="${customerSearch.suspended and customerSearch.suspended != null}">selected</c:if>><c:out value="suspended" /></option>
	         <option value="false" <c:if test="${!customerSearch.suspended and customerSearch.suspended != null}">selected</c:if>><c:out value="active" /></option>
	      </select>
	    </div>  
	    <c:if test="${gSiteConfig['gCUSTOMER_SUPPLIER']}">
	    <div class="search2">
	      <p><fmt:message key="supplier"/>:</p>
	      <select name="supplier">	      
	  	     <option value=""></option>
	         <option value="1" <c:if test="${customerSearch.supplier == '1'}">selected</c:if>>Yes</option>
	  	     <option value="2" <c:if test="${customerSearch.supplier == '2'}">selected</c:if>>No</option>	  	     
	      </select>
	    </div>
	    <div class="search2">
	      <p><fmt:message key="supplierPrefix" />:</p>
	      <input name="supplierPrefix" type="text" value="<c:out value='${customerSearch.supplierPrefix}' />" size="15" />
	    </div>
	    </c:if>
	    <c:if test="${gSiteConfig['gMASS_EMAIL']}">
	    <div class="search2">
	      <p><fmt:message key="unsubscribe" /> <fmt:message key="email" />:</p>
	      <select name="unsubscribe">
	         <option value=""></option>
	         <option value="1" <c:if test="${customerSearch.unsubscribe == '1'}" >selected</c:if>>Yes</option>
           <option value="0" <c:if test="${customerSearch.unsubscribe == '0'}" >selected</c:if>>No</option>	         
	      </select>
	    </div>
	    </c:if>
	    <div class="search2">
	      <p><fmt:message key="taxId" />:</p>
	      <select name="taxId">
	         <option value=""></option>
	         <option value="1" <c:if test="${customerSearch.taxId == '1'}" >selected</c:if>>Yes</option>
             <option value="0" <c:if test="${customerSearch.taxId == '0'}" >selected</c:if>>No</option>	         
	      </select>
	    </div>
	    <div class="search2">
	      <p><fmt:message key="trackcode" />:</p>
	      <input name="trackcode" type="text" value="<c:out value='${customerSearch.trackcode}' />" size="15" />
	    </div>
	    <div class="search2">
	      <p><fmt:message key="registeredBy" />:</p>
	      <input name="registeredBy" type="text" value="<c:out value='${customerSearch.registeredBy}' />" size="15" />
	    </div> 
	    <div class="search2">
	      <p><fmt:message key="zipcode" />:</p>
	      <input name="zipcode" type="text" value="<c:out value='${customerSearch.zipcode}' />" size="15" />
	    </div>
	    <div class="search2">
	    <p><fmt:message key="distance" />:</p>
  			<select name="radius">
   			<option value=""></option>
  			<option value="25" <c:if test="${customerSearch.locationSearch.radius == 25}">selected</c:if> >25 miles</option>
   			<option value="50" <c:if test="${customerSearch.locationSearch.radius == 50}">selected</c:if> >50 miles</option>
  			<option value="100" <c:if test="${customerSearch.locationSearch.radius == 100}">selected</c:if> >100 miles</option>
  			<option value="250" <c:if test="${customerSearch.locationSearch.radius == 250}">selected</c:if> >250 miles</option>
   			<option value="500" <c:if test="${customerSearch.locationSearch.radius == 500}">selected</c:if> >500 miles</option>
   			<option value="1000" <c:if test="${customerSearch.locationSearch.radius == 1000}">selected</c:if> >1000 miles</option>
  			</select>
 		</div> 
	    <div class="search2">
	      <p><fmt:message key="phone" />:</p>
	      <input name="phone" type="text" value="<c:out value='${customerSearch.phone}' />" size="15" />
	    </div>
	    <div class="search2">
	      <p><fmt:message key="cellPhone" />:</p>
	      <input name="cellPhone" type="text" value="<c:out value='${customerSearch.cellPhone}' />" size="15" />
	    </div>
	    <div class="search2">
	    <p><fmt:message key="textMessage" />:</p>
  			<select name="textMessage">
   			<option value=""></option>
			<option value="1" <c:if test="${customerSearch.textMessage == 1}">selected</c:if> >enabled text message</option>
			<option value="2" <c:if test="${customerSearch.textMessage == 2}">selected</c:if> >not enabled text message</option>
  			</select>
 		</div> 
	    <div class="search2">
	      <p><fmt:message key="mainSource" />:</p>
	      <input name="mainSource" type="text" value="<c:out value='${customerSearch.mainSource}' />" size="15" />
	    </div>
	    <div class="search2">
	      <p><fmt:message key="paid" />:</p>
	      <input name="paid" type="text" value="<c:out value='${customerSearch.paid}' />" size="15" />
	    </div>
	    <div class="search2">
	      <p><fmt:message key="medium" />:</p>
	      <input name="medium" type="text" value="<c:out value='${customerSearch.medium}' />" size="15" />
	    </div>
	    <div class="search2">
	      <p>Analytics <fmt:message key="language" />:</p>
	      <input name="languageField" type="text" value="<c:out value='${customerSearch.languageField}' />" size="15" />
	    </div>
	    <div class="search2">
	      <p><fmt:message key="idCard" />:</p>
	      <input name="cardId" type="text" value="<c:out value='${customerSearch.cardId}' />" size="15" />
	    </div> 
	    <div class="search2">
	      <p><fmt:message key="city" />:</p>
	      <input name="city" type="text" value="<c:out value='${customerSearch.city}' />" size="15" />
	    </div>
	    <div class="search2">
	      <p><fmt:message key="state" />:</p>
	      <input name="state" type="text" value="<c:out value='${customerSearch.state}' />" size="15" />
	    </div>
	    <div class="search2">
	      <p><fmt:message key="country" />:</p>
			<select name="country">
            <option value="" ></option>
            <c:forEach items="${model.countries}" var="country" varStatus="status">
              <option value="${country.code}" >${country.name}</option>
            </c:forEach>
        	</select>
	    </div> 
	    <div class="search2">
	      <p><fmt:message key="region" />:</p>
			<select name="region">
            <option value="" ></option>
            <option value="non USA"<c:if test="${customerSearch.region == 'non USA'}">selected</c:if>>non USA</option>
            <c:forEach items="${model.region}" var="region" varStatus="status">
              <option value="${region.region}"<c:if test="${customerSearch.region == region.region}">selected</c:if>>${region.region}</option>
            </c:forEach>           
        	</select>
	    </div>
		<div class="search2">
	      <p><fmt:message key="nationalRegion" />:</p>
			<select name="nationalRegion">
            <option value="" ></option>
            <c:forEach items="${model.nationalRegion}" var="nationalRegion" varStatus="status">
              <option value="${nationalRegion.region}"<c:if test="${customerSearch.nationalRegion == nationalRegion.region}">selected</c:if>>${nationalRegion.region}</option>
            </c:forEach>
            
        	</select>
	    </div>
	    <div class="search2">
	      <p><fmt:message key="timeZone" />:</p>
			<select name="timeZone">
            <option value="" ></option>
              <option value="CT"<c:if test="${customerSearch.timeZone == 'CT'}">selected</c:if>>CT</option>
              <option value="PT"<c:if test="${customerSearch.timeZone == 'PT'}">selected</c:if>>PT</option>
              <option value="MT"<c:if test="${customerSearch.timeZone == 'MT'}">selected</c:if>>MT</option>
              <option value="ET"<c:if test="${customerSearch.timeZone == 'ET'}">selected</c:if>>ET</option>
        	</select>
	    </div>
	    <c:if test="${model.languageCodes != null}">
        <div class="search2">
	      <p><fmt:message key="language" />:</p>
			<select name="language">
            <option value="" ></option>
            <option value="en" <c:if test="${customerSearch.languageCode == 'en'}">selected</c:if> ><fmt:message key="language_en"/></option>
            <c:forEach items="${model.languageCodes}" var="i18n">         	
          	  <option value="${i18n.languageCode}" <c:if test="${customerSearch.languageCode == i18n.languageCode}">selected</c:if>><fmt:message key="language_${i18n.languageCode}"/></option>
        	</c:forEach>
        	</select>
	    </div> 
	    </c:if>
	    
	    <div class="search2">
	      <p><fmt:message key="orders"/>:   <a href="#operatorNumOrder" rel="type:element" id="mbNumOrderOperator1" class="mbNumOrderOperator" style="margin-left: 55px;"><img class="toolTipImg" title="Advanced Search::Click to see more Options" src="../graphics/question.gif" /></a></p>
		  <div id="operatorNumOrder" class="miniWindowWrapper searchOp displayNone">
	    	<div class="header">Advanced Search</div>
	    	  <fieldset class="">
	    	    <label class="AStitle"><fmt:message key="orders"/></label>
	    	    <input name="numOrder" type="text" value="<c:out value='${customerSearch.numOrder}' />" size="15" disabled />
	    	  </fieldset>
	    	  <fieldset class="bottom">
	    	    <label class="AStitle"><fmt:message key="operator"/></label>
	    	    <div class="options">
	    	       <label class="radio">
	    	       		<input type="radio" <c:if test="${customerSearch.numOrderOperator == 0}" >checked="checked"</c:if> value="0" name="numOrderOp" onchange="selectOperator(this.value, 'numOrder');" /> <strong> Greater Than (&gt;)</strong>
	    	       </label>
	    	       <label class="radio">
	    	       		<input type="radio" <c:if test="${customerSearch.numOrderOperator == 1}" >checked="checked"</c:if> value="1" name="numOrderOp" onchange="selectOperator(this.value, 'numOrder');" /> <strong> Equal (=) </strong>
	    	       </label>
	    	       <label class="radio">
	    	       		<input type="radio" <c:if test="${customerSearch.numOrderOperator == 2}" >checked="checked"</c:if> value="2" name="numOrderOp" onchange="selectOperator(this.value, 'numOrder');" /> <strong> Less Than (&lt;) </strong>
	    	       </label>
	    	    </div>
	    	  </fieldset>
	    	  <fieldset>
	    	     <div class="button">
	      			<input type="submit" value="Search" onclick="document.searchform.submit();"/>
      			 </div>
	    	  </fieldset>
		  </div>
		  <input name="numOrder" type="text" value="<c:out value='${customerSearch.numOrder}' />" size="15" />
		  <input type="hidden" name="numOrderOperator" id="numOrderOperator"/>
	    </div>
	    
	    <div class="search2">
	      <p><fmt:message key="orderTotal"/>:   <a href="#operatorTotalOrder" rel="type:element" id="mbTotalOrderOperator1" class="mbTotalOrderOperator" style="margin-left: 30px;"><img class="toolTipImg" title="Advanced Search::Click to see more Options" src="../graphics/question.gif" /></a></p>
		  <div id="operatorTotalOrder" class="miniWindowWrapper searchOp displayNone">
	    	<div class="header">Advanced Search</div>
	    	  <fieldset class="">
	    	    <label class="AStitle"><fmt:message key="orderTotal"/></label>
	    	    <input name="totalOrder" type="text" value="<c:out value='${customerSearch.totalOrder}' />" size="15" disabled />
	    	  </fieldset>
	    	  <fieldset class="bottom">
	    	    <label class="AStitle"><fmt:message key="operator"/></label>
	    	    <div class="options">
	    	       <label class="radio">
	    	       		<input type="radio" <c:if test="${customerSearch.totalOrderOperator == 0}" >checked="checked"</c:if> value="0" name="totalOrderOp" onchange="selectOperator(this.value, 'totalOrder');" /> <strong> Greater Than (&gt;)</strong>
	    	       </label>
	    	       <label class="radio">
	    	       		<input type="radio" <c:if test="${customerSearch.totalOrderOperator == 1}" >checked="checked"</c:if> value="1" name="totalOrderOp" onchange="selectOperator(this.value, 'totalOrder');" /> <strong>Equal (=)</strong>
	    	       </label>
	    	       <label class="radio">
	    	       		<input type="radio" <c:if test="${customerSearch.totalOrderOperator == 2}" >checked="checked"</c:if> value="2" name="totalOrderOp" onchange="selectOperator(this.value, 'totalOrder');" /> <strong>Less Than (&lt;) </strong>
	    	       </label>
	    	    </div>
	    	  </fieldset>
	    	  <fieldset>
	    	     <div class="button">
	      			<input type="submit" value="Search" onclick="document.searchform.submit();"/>
      			 </div>
	    	  </fieldset>
		  </div>
		  <input name="totalOrder" type="text" value="<c:out value='${customerSearch.totalOrder}' />" size="15" />
		  <input type="hidden" name="totalOrderOperator" id="totalOrderOperator"/>
	    </div>
	    
	    <div class="search2">
	      <p><c:set var="customerFieldFlag" value="false" />
	      <select class="extraFieldSearch" name="customerFieldNumber">
	  	    <c:forEach items="${model.customerFieldList}" var="customerField">
	  	     <c:if test="${!empty customerField.name}" ><c:set var="customerFieldFlag" value="true" />
	  	     <option style="font-size:normal;" value="${customerField.id}" <c:if test="${customerSearch.customerFieldNumber == customerField.id}">selected</c:if>><c:out value="${customerField.name}" />:</option>
	  	     </c:if>
	  	    </c:forEach>
	      </select>
	      </p>
	      <c:if test="${customerFieldFlag}" >
	       <input name="customerField" type="text" value="<c:out value='${customerSearch.customerField}' />" size="11" />
	      </c:if>
	    </div>   
	    <div class="button">
	      <input type="submit" value="Search"/>
		</div>
	  <!-- content area -->
	  </td>
	  <td valign="top">
	  <div id="sideBar">
	  <a name="#tabCustomer" ></a>
	  <a href="#tabCustomer" id="sideBarTab"><img id="h_toggle"  src="../graphics/tabSliderLeft.png" alt="sideBar" title="sideBar" /></a>
      <div id="sideBarContents" style="width:0px;">
	  <table cellpadding="0" cellspacing="0" border="0" class="module" >
      <tr>
      <td class="topl_g"><div align="left" class="searchHeaderAdvanced"><h2><fmt:message key="advancedSearch" /></h2></div></td>
	  <td class="topr_g" ></td></tr>
      <tr><td class="boxmidlrg" >
  
		<table class="searchBoxLeftAS"><tr><td>
		<div id="sideBarContentsInner">
			<div class="advancedSearch">
		      <p>start:</p>
		      <input style="width:90px;margin-right:5px;" name="startDate" id="startDate" type="text" value="<fmt:formatDate type="date" pattern="MM/dd/yyyy" value="${customerSearch.startDate}"/>" readonly size="11" />
			  <img class="calendarImage"  id="startDate_trigger" src="../graphics/calendarIcon.jpg" />
			  <script type="text/javascript">
	    		Calendar.setup({
	        			inputField     :    "startDate",   // id of the input field
	        			showsTime      :    false,
	        			ifFormat       :    "%m/%d/%Y",   // format of the input field
	        			button         :    "startDate_trigger"   // trigger for the calendar (button ID)
	    		});	
			  </script>
		    </div>
		    <div style="clear:both;"></div>    
		    <div class="advancedSearch">
		      <p>end:</p>
		      <input style="width:90px;margin-right:5px;" name="endDate" id="endDate" type="text" value="<fmt:formatDate type="date" pattern="MM/dd/yyyy" value='${customerSearch.endDate}'/>" readonly size="11" />
			  <img class="calendarImage"  id="endDate_trigger" src="../graphics/calendarIcon.jpg" />
			  <script type="text/javascript">
	    		Calendar.setup({
	        			inputField     :    "endDate",   // id of the input field
	        			showsTime      :    false,
	        			ifFormat       :    "%m/%d/%Y",   // format of the input field
	        			button         :    "endDate_trigger"   // trigger for the calendar (button ID)
	    		});	
			  </script>
			</div>  
			<div style="clear:both;"></div>    
		    <div class="advancedSearch">
		      <table border="0">
		       <tr> <td class="advancedSearchDateInterval"><input style="width:12px;margin-right:5px;border: 0px" name="intervaldate" id="today" type="radio" onclick="UpdateStartEndDate(1);" />Today</td> </tr>
		       <tr> <td class="advancedSearchDateInterval"><input style="width:12px;margin-right:5px;border: 0px" name="intervaldate" id="yesterday" type="radio" onclick="UpdateStartEndDate(0);" />Yesterday</td> </tr>
		       <tr> <td class="advancedSearchDateInterval"><input style="width:12px;margin-right:5px;border: 0px" name="intervaldate" id="weekToDate" type="radio" onclick="UpdateStartEndDate(2);" />Week To Date</td> </tr>
		       <tr> <td class="advancedSearchDateInterval"><input style="width:12px;margin-right:5px;border: 0px" name="intervaldate" id="monthToDate" type="radio" onclick="UpdateStartEndDate(3);" />Month To Date</td> </tr>
		       <tr> <td class="advancedSearchDateInterval"><input style="width:12px;margin-right:5px;border: 0px" name="intervaldate" id="yearToDate" type="radio" onclick="UpdateStartEndDate(10);" />Year To Date</td> </tr>
		       <tr> <td class="advancedSearchDateInterval"><input style="width:12px;margin-right:5px;border: 0px" name="intervaldate" id="reset" type="radio" onclick="UpdateStartEndDate(20);" />Reset</td> </tr>
		      </table>
		    </div>    
		</div>
		</td></tr></table>
	  
	  <c:if test="${gSiteConfig['gSHOPPING_CART'] and siteConfig['SHOPPINGCART_SEARCH'].value == 'true'}">
	  <h4 style="margin-left:11px;">Shopping Cart</h4>
	  <table class="searchBoxLeftAS"><tr><td>
		<div id="sideBarContentsInner">
			<div class="advancedSearch">
		      <p>start:</p>
		      <input style="width:90px;margin-right:5px;" name="cartStartDate" id="cartStartDate" type="text" value="<fmt:formatDate type="date" pattern="MM/dd/yyyy" value="${customerSearch.cartStartDate}"/>" readonly size="11" />
			  <img class="calendarImage"  id="cartStartDate_trigger" src="../graphics/calendarIcon.jpg" />
			  <script type="text/javascript">
	    		Calendar.setup({
	        			inputField     :    "cartStartDate",   // id of the input field
	        			showsTime      :    false,
	        			ifFormat       :    "%m/%d/%Y",   // format of the input field
	        			button         :    "cartStartDate_trigger"   // trigger for the calendar (button ID)
	    		});	
			  </script>
		    </div>
		    <div style="clear:both;"></div>    
		    <div class="advancedSearch">
		      <p>end:</p>
		      <input style="width:90px;margin-right:5px;" name="cartEndDate" id="cartEndDate" type="text" value="<fmt:formatDate type="date" pattern="MM/dd/yyyy" value='${customerSearch.cartEndDate}'/>" readonly size="11" />
			  <img class="calendarImage"  id="cartEndDate_trigger" src="../graphics/calendarIcon.jpg" />
			  <script type="text/javascript">
	    		Calendar.setup({
	        			inputField     :    "cartEndDate",   // id of the input field
	        			showsTime      :    false,
	        			ifFormat       :    "%m/%d/%Y",   // format of the input field
	        			button         :    "cartEndDate_trigger"   // trigger for the calendar (button ID)
	    		});	
			  </script>
			</div>  
			<div style="clear:both;"></div>    
		    <div class="advancedSearch">
		      <table border="0">
		       <tr> <td class="advancedSearchDateInterval"><input style="width:12px;margin-right:5px;border: 0px" name="cartIntervalDate" id="today" type="radio" value="1" onclick="UpdateCartStartEndDate(1);" />Today</td> </tr>
		       <tr> <td class="advancedSearchDateInterval"><input style="width:12px;margin-right:5px;border: 0px" name="cartIntervalDate" id="weekToDate" type="radio" value="2" onclick="UpdateCartStartEndDate(2);" />Week To Date</td> </tr>
		       <tr> <td class="advancedSearchDateInterval"><input style="width:12px;margin-right:5px;border: 0px" name="cartIntervalDate" id="monthToDate" type="radio" value="3" onclick="UpdateCartStartEndDate(3);" />Month To Date</td> </tr>
		       <tr> <td class="advancedSearchDateInterval"><input style="width:12px;margin-right:5px;border: 0px" name="cartIntervalDate" id="yearToDate" type="radio" value="10" onclick="UpdateCartStartEndDate(10);" />Year To Date</td> </tr>
		       <tr> <td class="advancedSearchDateInterval"><input style="width:12px;margin-right:5px;border: 0px" name="cartIntervalDate" id="reset" type="radio" value="20" onclick="UpdateCartStartEndDate(20);" />Reset</td> </tr>
		      </table>
		    </div>    
		</div>
		</td></tr></table></c:if>
		
	  <h4 style="margin-left:11px;">Last Login</h4>
	  <table class="searchBoxLeftAS"><tr><td>
		<div id="sideBarContentsInner">
			<div class="advancedSearch">
		      <p>start:</p>
		      <input style="width:90px;margin-right:5px;" name="loginStartDate" id="loginStartDate" type="text" value="<fmt:formatDate type="date" pattern="MM/dd/yyyy" value="${customerSearch.loginStartDate}"/>" readonly size="11" />
			  <img class="calendarImage"  id="loginStartDate_trigger" src="../graphics/calendarIcon.jpg" />
			  <script type="text/javascript">
	    		Calendar.setup({
	        			inputField     :    "loginStartDate",   // id of the input field
	        			showsTime      :    false,
	        			ifFormat       :    "%m/%d/%Y",   // format of the input field
	        			button         :    "loginStartDate_trigger"   // trigger for the calendar (button ID)
	    		});	
			  </script>
		    </div>
		    <div style="clear:both;"></div>    
		    <div class="advancedSearch">
		      <p>end:</p>
		      <input style="width:90px;margin-right:5px;" name="loginEndDate" id="loginEndDate" type="text" value="<fmt:formatDate type="date" pattern="MM/dd/yyyy" value='${customerSearch.loginEndDate}'/>" readonly size="11" />
			  <img class="calendarImage"  id="loginEndDate_trigger" src="../graphics/calendarIcon.jpg" />
			  <script type="text/javascript">
	    		Calendar.setup({
	        			inputField     :    "loginEndDate",   // id of the input field
	        			showsTime      :    false,
	        			ifFormat       :    "%m/%d/%Y",   // format of the input field
	        			button         :    "loginEndDate_trigger"   // trigger for the calendar (button ID)
	    		});	
			  </script>
			</div>  
			<div style="clear:both;"></div>    
		    <div class="advancedSearch">
		      <table border="0">
		       <tr> <td class="advancedSearchDateInterval"><input style="width:12px;margin-right:5px;border: 0px" name="loginIntervalDate" id="today" type="radio" value="1" onclick="UpdateLoginStartEndDate(1);" />Today</td> </tr>
		       <tr> <td class="advancedSearchDateInterval"><input style="width:12px;margin-right:5px;border: 0px" name="loginIntervalDate" id="weekToDate" type="radio" value="2" onclick="UpdateLoginStartEndDate(2);" />Week To Date</td> </tr>
		       <tr> <td class="advancedSearchDateInterval"><input style="width:12px;margin-right:5px;border: 0px" name="loginIntervalDate" id="monthToDate" type="radio" value="3" onclick="UpdateLoginStartEndDate(3);" />Month To Date</td> </tr>
		       <tr> <td class="advancedSearchDateInterval"><input style="width:12px;margin-right:5px;border: 0px" name="loginIntervalDate" id="yearToDate" type="radio" value="10" onclick="UpdateLoginStartEndDate(10);" />Year To Date</td> </tr>
		       <tr> <td class="advancedSearchDateInterval"><input style="width:12px;margin-right:5px;border: 0px" name="loginIntervalDate" id="reset" type="radio" value="20" onclick="UpdateLoginStartEndDate(20);" />Reset</td> </tr>
		      </table>
		    </div>    
		</div>
		</td></tr></table>
	  </td><td class="boxmidr" ></td></tr>
      <tr><td class="botl"></td><td class="botr"></td></tr>
      </table></div>
    
      </div>
	  </td>
	  </tr></table>
	  </form>
	</div>  
	</div>  
	<div class="menudiv"></div>  
	</c:if>
	  
    <c:if test="${param.tab == 'specialPricing'}"> 
      <div align="left" class="searchHeader" id="searchHeaderId"><h2><fmt:message key="search" /></h2></div>
	  <table class="searchBoxLeft" width="100%"><tr><td valign="top" style="width: 110px;">
	  <form action="specialPricing.jhtm" name="searchform" method="post">	
	  <!-- content area -->
	    <div class="search2">
	      <p>Sku:</p>
	      <input name="_sku_search" type="text" value="<c:out value='${spSearch.sku}' />" size="15" />
	    </div>     
	    <div class="button">
	      <input type="submit" value="Search"/>
	    </div>
	  <!-- content area -->
	  </form>
	  </td></tr></table>
	<div class="menudiv"></div>    
	</c:if> 
	
    <c:if test="${param.tab == 'groups'}"> 
      <div align="left" class="searchHeader" id="searchHeaderId"><h2><fmt:message key="search" /></h2></div>
	  <table class="searchBoxLeft" width="100%"><tr><td valign="top" style="width: 110px;">
	  <form action="customerGroupList.jhtm" name="searchform" method="post">	
	  <!-- content area -->
	    <div class="search2">
	     <p><fmt:message key="group" />:</p>
	     <input name="group" type="text" value="<c:out value='${groupCustomerSearch.group}' />" size="15" />
	   </div>
	   <div class="search2">  
	     <p><fmt:message key="active" />:</p>
	     <select name="active">
	        <option value=""><fmt:message key="all"/></option> 
	        <option value="0" <c:if test="${model.search.active == '0'}">selected</c:if>><fmt:message key="inactive" /></option>
	  	    <option value="1" <c:if test="${model.search.active == '1'}">selected</c:if>><fmt:message key="active" /></option>
	    </select>
	    </div>     
	    <div class="button">
	      <input type="submit" value="Search"/>
	    </div>
	  <!-- content area -->
	  </form>
	  </td></tr></table>
	<div class="menudiv"></div>    
	</c:if> 	
	
	<c:if test="${param.tab == 'vbaOrders'}">
	<h2 class="menuleft mfirst"><fmt:message key="paymentMenuTitle"/></h2>
    <div class="menudiv"></div>
    <ul id="nav">
      <sec:authorize ifAnyGranted="ROLE_AEM,ROLE_ADMIN_${prop_site_id},ROLE_INCOMING_PAYMENT_VIEW_LIST">
        <li><a href="payments.jhtm?" class="userbutton"><fmt:message key="incomingPayments"/></a></li>
      </sec:authorize>      
	  <sec:authorize ifAnyGranted="ROLE_AEM,ROLE_ADMIN_${prop_site_id},ROLE_OUTGOING_PAYMENT_VIEW_LIST">
      <li><a href="virtualBankPayment.jhtm" class="userbutton"><fmt:message key="outgoingPayments"/></a>
      <ul>
      	<li><a href="vbaPaymentReport.jhtm?userName=${vbaOrderSearch.userName}" class="userbutton"><fmt:message key="cancel"/> <fmt:message key="payments"/></a></li>
      </ul>
      </li>
      </sec:authorize>
    </ul>
	<div class="leftbar_searchWrapper">
      <div align="left" class="searchHeader" id="searchHeaderId"><h2><fmt:message key="search" /></h2></div>
	  <div class="searchformBox">
	  <form action="virtualBankPayment.jhtm" name="searchform" method="post" >
	  <table class="searchBoxLeft">
	    <tr>
	    <td valign="top" style="width: 110px;">
	  	<!-- content area -->
	    <div class="search2">
	      <p><fmt:message key="paymentFor"/></p>
	      <select name="paymentFor">
	         <option value="">Select One</option>
	  	     <option value="1" <c:if test="${vbaOrderSearch.paymentFor == 1}">selected</c:if>>Affiliate</option>
	  	     <option value="2" <c:if test="${vbaOrderSearch.paymentFor == 2}">selected</c:if>>Consignment</option>
	      </select>
	    </div>
	    <div class="search2" id="searchStatusId">
       	<p><fmt:message key="status" />:</p> 
       	<select name="orderStatus">
	  	    <option value=""> </option>
	  	    <option value="p" <c:if test="${vbaOrderSearch.orderStatus == 'p'}">selected</c:if>><fmt:message key="orderStatus_p" /></option>
	  	    <option value="pr" <c:if test="${vbaOrderSearch.orderStatus == 'pr'}">selected</c:if>><fmt:message key="orderStatus_pr" /></option>
	  	    <option value="ppr" <c:if test="${vbaOrderSearch.orderStatus == 'ppr'}">selected</c:if>><fmt:message key="orderStatus_ppr" /></option>
	  	    <c:if test="${siteConfig['AMAZON_URL'].value != ''}">
	  	    <option value="ars" <c:if test="${vbaOrderSearch.orderStatus == 'ars'}">selected</c:if>><fmt:message key="orderStatus_ars" /></option>
			</c:if>
			<c:if test="${siteConfig['AMAZON_URL'].value != ''}">
	  	    <option value="prars" <c:if test="${vbaOrderSearch.orderStatus == 'prars'}">selected</c:if>><fmt:message key="orderStatus_prars" /></option>
			</c:if>
	  	    <option value="s" <c:if test="${vbaOrderSearch.orderStatus == 's'}">selected</c:if>><fmt:message key="orderStatus_s" /></option>
	  	    <option value="x" <c:if test="${vbaOrderSearch.orderStatus == 'x'}">selected</c:if>><fmt:message key="orderStatus_x" /></option>
			<c:if test="${gSiteConfig['gPAYPAL'] > 0 && gSiteConfig['gPAYPAL'] != 3}">	  	    
	  	    <option value="xp" <c:if test="${vbaOrderSearch.orderStatus == 'xp'}">selected</c:if>><fmt:message key="orderStatus_xp" /></option>
	  	    </c:if>
	  	    <c:if test="${gSiteConfig['gCREDIT_CARD_PAYMENT'] == 'netcommerce'}">
	  	    <option value="xnc" <c:if test="${vbaOrderSearch.orderStatus == 'xnc'}">selected</c:if>><fmt:message key="orderStatus_xnc" /></option>
	  	    </c:if>
	  	    <c:if test="${siteConfig['GOOGLE_CHECKOUT_URL'].value != ''}">
	  	    <option value="xg" <c:if test="${vbaOrderSearch.orderStatus == 'xg'}">selected</c:if>><fmt:message key="orderStatus_xg" /></option>
			</c:if>
			<option value="pprs" <c:if test="${vbaOrderSearch.orderStatus == 'pprs'}">selected</c:if>><fmt:message key="orderStatus_pprs" /></option>  	   
  	    </select>
	    </div>
	  	<div class="search2">
	      <p><fmt:message key="order"/> #:</p>
	      <input name="orderNum" type="text" value="<c:out value='${vbaOrderSearch.orderId}' />" size="15" />
	    </div>
	    <div class="search2">
	      <p><fmt:message key="productSku"/></p>
	      <input name="productSku" type="text" value="<c:out value='${vbaOrderSearch.productSku}' />" size="15" />
	    </div>
	    <div class="search2">
	      <p><fmt:message key="supplier"/> <fmt:message key="email"/>:</p>
	      <input name="userName" type="text" value="<c:out value='${vbaOrderSearch.userName}' />" size="15" />
	    </div>
		<div class="button">
	      <input type="submit" value="Search"/>
		</div>
		
		</td>
		</tr>
	  </table>
	  </form>
	  </div>
	</div>
	</c:if>
		
	<c:if test="${param.tab == 'vbaPaymentReport'}"> 
	<h2 class="menuleft mfirst"><fmt:message key="paymentMenuTitle"/></h2>
    <div class="menudiv"></div>
    <ul id="nav">
      <sec:authorize ifAnyGranted="ROLE_AEM,ROLE_ADMIN_${prop_site_id},ROLE_INCOMING_PAYMENT_VIEW_LIST">
      	<li><a href="payments.jhtm" class="userbutton"><fmt:message key="incomingPayments"/></a></li>
      </sec:authorize>
      <sec:authorize ifAnyGranted="ROLE_AEM,ROLE_ADMIN_${prop_site_id},ROLE_OUTGOING_PAYMENT_VIEW_LIST">
      <li><a href="virtualBankPayment.jhtm" class="userbutton"><fmt:message key="outgoingPayments"/></a>
      <ul>
      	<li><a href="vbaPaymentReport.jhtm" class="userbutton"><fmt:message key="cancel"/> <fmt:message key="payments"/></a></li>
      </ul>
      </li>
      </sec:authorize>
    </ul>
      <div align="left" class="searchHeader" id="searchHeaderId"><h2><fmt:message key="search" /></h2></div>
	  <form action="vbaPaymentReport.jhtm" method="post" >
	  <table class="searchBoxLeft" width="100%"><tr><td valign="top" style="width:110px;">
	  <!-- content area -->
	    <div class="search2">
	    <p><fmt:message key="paymentMethod" />:</p>
	      <input name="paymentMethod" type="text" value="<c:out value='${vbaPaymentReportFilter.paymentMethod}' />">
	    </div>
	    <div class="search2">
	      <p><fmt:message key="reportView" />:</p>
	      <select name="reportView">
			<option value="consignment" <c:if test="${vbaPaymentReportFilter.reportView == 'consignment'}">selected</c:if>><fmt:message key="consignment" /></option>
			<option value="affiliate" <c:if test="${vbaPaymentReportFilter.reportView == 'affiliate'}">selected</c:if>><fmt:message key="affiliate" /></option>
	      </select>
	  	</div>
	    <div class="search2">
	    <p><fmt:message key="transactionID" />:</p>
	      <input name="transactionId" type="text" value="<c:out value='${vbaPaymentReportFilter.transactionId}' />">
	    </div>
	    <div class="search2">
	    <p><fmt:message key="supplier"/> <fmt:message key="email" />:</p>
	    	<input name="userName" type="text" value="<c:out value='${model.userName}' />">
	    </div>
		<div class="search2">
	      <p><fmt:message key="productSku"/></p>
	      <input name="productSku" type="text" value="<c:out value='${vbaPaymentReportFilter.productSku}' />" size="15" />
	    </div>	  
	  
	    <div class="search2">
	      <p>Payment Date From</p>
	    <input style="width:90px;margin-right:5px;" name="startPayDate" id="startPayDate" type="text" value="<fmt:formatDate type="date" pattern="yyyy/MM/dd" value="${vbaPaymentReportFilter.dateOfPay}"/>" readonly size="11" />
			  <img class="calendarImage"  id="startPayDate_trigger" src="../graphics/calendarIcon.jpg" />
			  <script type="text/javascript">
	    		Calendar.setup({
	        			inputField     :    "startPayDate",   // id of the input field
	        			showsTime      :    false,
	        			ifFormat       :    "%Y/%m/%d",   // format of the input field
	        			button         :    "startPayDate_trigger"   // trigger for the calendar (button ID)
	    		});	
			  </script>
			  
		</div>  
		  <div class="search2">
	      <p>Payment Date TO</p>
	    <input style="width:90px;margin-right:5px;" name="EndPayDate" id="EndPayDate" type="text" value="<fmt:formatDate type="date" pattern="yyyy/MM/dd" value="${vbaPaymentReportFilter.endDateOfPay}"/>" readonly size="11" />
			  <img class="calendarImage"  id="EndPayDate_trigger" src="../graphics/calendarIcon.jpg" />
			  <script type="text/javascript">
	    		Calendar.setup({
	        			inputField     :    "EndPayDate",   // id of the input field
	        			showsTime      :    false,
	        			ifFormat       :    "%Y/%m/%d",   // format of the input field
	        			button         :    "EndPayDate_trigger"   // trigger for the calendar (button ID)
	    		});	
			  </script>
			  
		</div> 
		
	    <div class="button">
	      <input type="submit" value="Search"/>
		</div>
		
	  <!-- content area -->
	  </td>	  	  
	  </tr>
	  </table>
	  </form>
	<div class="menudiv"></div>    
	</c:if>
	
	<c:if test="${param.tab == 'inComing'}">
		<div align="left" class="searchHeader" id="searchHeaderId"><h2><fmt:message key="search" /></h2></div>
	  <table class="searchBoxLeft" width="100%"><tr><td valign="top" style="width: 110px;">
	  <form action="payments.jhtm" name="searchform" method="post">	
	  <!-- content area -->
	    <div class="search2">
	      <p><fmt:message key="email" />:</p>
	      <input name="email" type="text" value="<c:out value='${model.search.email}' />" size="15" />
	    </div>
	    <div class="button">
	      <input type="submit" value="Search"/>
	    </div>
	  <!-- content area -->
	  </form>
	  </td></tr></table>
	<div class="menudiv"></div>    
	</c:if>
	  	
	<c:if test="${param.tab == 'customerList2'}"> 
    <script type="text/javascript" src="../../admin/javascript/autocompleter1.2.js"></script>
	<script type="text/javascript" src="../../admin/javascript/autocompleter.Request1.2.js"></script>
	<script type="text/javascript" src="../../admin/javascript/autocompleter.Local1.2.js"></script>
	<script type="text/javascript" src="../../admin/javascript/observer.js"></script>
    <script type="text/javascript" src="../javascript/side-bar.js"></script>
    <script type="text/javascript">
<!--
window.addEvent('domready', function(){
	var firstName = $('firstName');
	var lastName = $('lastName');
	new Autocompleter.Request.HTML(firstName, '../customers/show-ajax-customers.jhtm', {
		'indicatorClass': 'autocompleter-loading',
		'postData': { 'search': 'firstname' },
		'injectChoice': function(choice) {
			var text = choice.getFirst();
			var value = text.innerHTML;
			choice.inputValue = value;
			text.set('html', this.markQueryValue(value));
			this.addChoiceEvents(choice);
		},
		'onSelection': function(element, selected, value, input) {
			lastName.value = selected.getFirst().getNext().getNext().innerHTML;
		}
	});
	
	new Autocompleter.Request.HTML(lastName, '../customers/show-ajax-customers.jhtm', {
		'indicatorClass': 'autocompleter-loading',
		'postData': { 'search': 'lastname' },
		'injectChoice': function(choice) {
			var text = choice.getFirst();
			var value = text.innerHTML;
			choice.inputValue = value;
			text.set('html', this.markQueryValue(value));
			this.addChoiceEvents(choice);
		},
		'onSelection': function(element, selected, value, input) {
			firstName.value = selected.getFirst().getNext().getNext().innerHTML;
		}
	});
});
//-->
</script> 
    <div class="leftbar_searchWrapper">
      <div align="left" class="searchHeader" id="searchHeaderId"><h2><fmt:message key="search" /></h2></div>
      <div class="searchformBox">
	  <form name="searchform" action="customerList2.jhtm" method="post" >
	  <table class="searchBoxLeft"><tr><td valign="top" style="width: 110px;">
	  <!-- content area -->
		<c:if test="${gSiteConfig['gMULTI_STORE'] > 0}">
	    <div class="search2">
	      <p><fmt:message key="multiStore" />:</p>
	      <select name="host">
	  	    <option value=""><fmt:message key="all" /> <fmt:message key="accounts" /></option>
	  	    <option value="main" <c:if test="${customerSearch.host == 'main'}">selected</c:if>><fmt:message key="main" /></option>
	  	    <c:forEach items="${multiStores}" var="multiStore">
	  	    <option value="${multiStore.host}" <c:if test="${customerSearch.host == multiStore.host}">selected</c:if>><c:out value="${multiStore.host}" /></option>
	  	    </c:forEach>
	       </select>
	    </div>
	    </c:if>
	    <div class="search2">
	      <p><fmt:message key="firstName" />:</p>
	      <input name="firstName" id="firstName" type="text" value="<c:out value='${customerSearch.firstName}' />" size="15" />
	    </div> 
	    <div class="search2">
	      <p><fmt:message key="lastName" />:</p>
	      <input name="lastName" id="lastName" type="text" value="<c:out value='${customerSearch.lastName}' />" size="15" />
	    </div> 
	    <div class="search2">
	      <p><fmt:message key="email" />:</p>
	      <input name="email" type="text" value="<c:out value='${customerSearch.email}' />" size="15" />
	    </div>
	    <div class="search2">
          <p><fmt:message key="companyName" />:</p>
          <input name="companyName" type="text" value="<c:out value='${customerSearch.companyName}' />" size="15" />
        </div>
        <div class="search2">
          <p><fmt:message key="multipleSearch" />:</p>
          <input name="multipleSearch" type="text" value="<c:out value='${customerSearch.multipleSearchString}' />" size="15" />
        </div>
	    <div class="search2">
	      <p><fmt:message key="accountNumber" />:</p>
          <input name="accountNumber" type="text" value="<c:out value='${customerSearch.accountNumber}' />" size="15" />   
	    </div>
	    <c:if test="${gSiteConfig['gSALES_REP']}">
	    <div class="search2">
	      <p><fmt:message key="salesRep" />:</p>
	      <select name="sales_rep_id">
		   <c:if test="${model.salesRepTree == null}">
	       <option value="-1"></option>
	       <option value="-2" <c:if test="${customerSearch.salesRepId == -2}">selected</c:if>><fmt:message key="noSalesRep" /></option>
	       <c:forEach items="${model.activeSalesReps}" var="salesRep">
	  	     <option value="${salesRep.id}" <c:if test="${customerSearch.salesRepId == salesRep.id}">selected</c:if>><c:out value="${salesRep.name}" /></option>
	       </c:forEach>
	       </c:if>
	       <c:if test="${model.salesRepTree != null}">
	         <c:set var="salesRepTree" value="${model.salesRepTree}" scope="request"/>
	         <c:set var="customerSearchSalesRepId" value="${customerSearch.salesRepId}" scope="request"/>
	         <option value="${salesRepTree.id}" <c:if test="${customerSearch.salesRepId == salesRepTree.id}">selected</c:if>><c:out value="${salesRepTree.name}" /></option>
			 <c:set var="level" value="0" scope="request"/>
	      	 <c:set var="recursionPageId" value="${customerSearchSalesRepId}" scope="request"/>
	      	 <jsp:include page="/WEB-INF/jsp/admin/customers/recursion.jsp"/>
	       </c:if>
	      </select>
	    </div>  
	    </c:if>
	    <c:if test="${gSiteConfig['gGROUP_CUSTOMER']}">
	    <div class="search2">
	      <p><fmt:message key="customer"/> <fmt:message key="group" />:</p>
	      <select name="groupId">
	         <option value=""></option>
	      <c:forEach items="${model.groups}" var="group">
	  	     <option value="${group.id}" <c:if test="${customerSearch.groupId == group.id}">selected</c:if>><c:out value="${group.name}" /></option>
	       </c:forEach>
	      </select>
	    </div>  
	    </c:if>	     	    
	    <c:if test="${gSiteConfig['gSHOPPING_CART'] and siteConfig['SHOPPINGCART_SEARCH'].value == 'true'}">
	    <div class="search2">
	      <p><fmt:message key="shoppingCart" />:</p>
	      <select name="cartNotEmpty">
	         <option value=""></option>
	         <option value="true" <c:if test="${customerSearch.cartNotEmpty != null and customerSearch.cartNotEmpty}" >selected</c:if>><fmt:message key="notEmpty" /></option>
             <option value="false" <c:if test="${customerSearch.cartNotEmpty != null and not customerSearch.cartNotEmpty}" >selected</c:if>><fmt:message key="empty" /></option>	         
	      </select>
	    </div>
	    <div class="search2">
	      <p><fmt:message key="shoppingCartTotal" />:</p>
	      <input name="shoppingCartTotal" type="text" value="<fmt:formatNumber value="${customerSearch.shoppingCartTotal}" pattern="###0.00"/>" size="15" />
	    </div> 
	    </c:if>
	    <div class="search2">
	      <p><fmt:message key="loggedIn" />:</p>
	      <select name="loggedInFlag">
	         <option value=""></option>
	         <option value="1" <c:if test="${customerSearch.loggedInFlag == '1'}" >selected</c:if>>Yes</option>       
	      </select>
	    </div>
	    <div class="search2">
	      <p><fmt:message key="rating" /> 1:</p>
	      <select name="rating1">
	         <option value=""></option>
	         <c:forTokens items="${siteConfig['CUSTOMER_RATING_1'].value}" delims="," var="rating1">
				   <option value="${rating1}" <c:if test="${customerSearch.rating1 == rating1}" >selected</c:if>><c:out value="${rating1}" /></option>
		  	   </c:forTokens>
	      </select>
	    </div>
	    <div class="search2">
	      <p><fmt:message key="rating" /> 2:</p>
	      <select name="rating2">
	      	<option value=""></option>
	        <c:forTokens items="${siteConfig['CUSTOMER_RATING_2'].value}" delims="," var="rating2">
				  <option value="${rating2}" <c:if test="${customerSearch.rating2 == rating2}" >selected</c:if>><c:out value="${rating2}" /></option>
		  	  </c:forTokens>
	      </select>
	    </div>
	    <div class="search2">
	      <p><fmt:message key="qualifier" />:</p>
	      <select name="qualifier">
	       <option value="-1"></option>
	       <option value="-2" <c:if test="${customerSearch.qualifierId == -2}">selected</c:if>><fmt:message key="noQualifier" /></option>
	       <c:forEach items="${model.activeQualifiers}" var="qualifier">
	  	     <option value="${qualifier.id}" <c:if test="${customerSearch.qualifierId == qualifier.id}">selected</c:if>><c:out value="${qualifier.name}" /></option>
	       </c:forEach>
	      </select>
	    </div>  
	    <div class="search2">
	      <p><fmt:message key="suspend" />:</p>
	      <select name="suspended">
	         <option value=""></option>
	         <option value="true" <c:if test="${customerSearch.suspended and customerSearch.suspended != null}">selected</c:if>><c:out value="suspended" /></option>
	         <option value="false" <c:if test="${!customerSearch.suspended and customerSearch.suspended != null}">selected</c:if>><c:out value="active" /></option>
	      </select>
	    </div>  
	    <c:if test="${gSiteConfig['gCUSTOMER_SUPPLIER']}">
	    <div class="search2">
	      <p><fmt:message key="supplier"/>:</p>
	      <select name="supplier">	      
	  	     <option value=""></option>
	         <option value="1" <c:if test="${customerSearch.supplier == '1'}">selected</c:if>>Yes</option>
	  	     <option value="2" <c:if test="${customerSearch.supplier == '2'}">selected</c:if>>No</option>	  	     
	      </select>
	    </div>
	    <div class="search2">
	      <p><fmt:message key="supplierPrefix" />:</p>
	      <input name="supplierPrefix" type="text" value="<c:out value='${customerSearch.supplierPrefix}' />" size="15" />
	    </div>
	    </c:if>
	    <c:if test="${gSiteConfig['gMASS_EMAIL']}">
	    <div class="search2">
	      <p><fmt:message key="unsubscribe" /> <fmt:message key="email" />:</p>
	      <select name="unsubscribe">
	         <option value=""></option>
	         <option value="1" <c:if test="${customerSearch.unsubscribe == '1'}" >selected</c:if>>Yes</option>
           <option value="0" <c:if test="${customerSearch.unsubscribe == '0'}" >selected</c:if>>No</option>	         
	      </select>
	    </div>
	    </c:if>
	    <div class="search2">
	      <p><fmt:message key="taxId" />:</p>
	      <select name="taxId">
	         <option value=""></option>
	         <option value="1" <c:if test="${customerSearch.taxId == '1'}" >selected</c:if>>Yes</option>
             <option value="0" <c:if test="${customerSearch.taxId == '0'}" >selected</c:if>>No</option>	         
	      </select>
	    </div>
	    <div class="search2">
	      <p><fmt:message key="trackcode" />:</p>
	      <input name="trackcode" type="text" value="<c:out value='${customerSearch.trackcode}' />" size="15" />
	    </div>
	    <div class="search2">
	      <p><fmt:message key="trackcode" />:</p>
	      <select name="trackcodeFieldDropDown">	      
	  	     <option value=""></option>
	         <option value="1" <c:if test="${customerSearch.trackcodeFieldDropDown == '1'}">selected</c:if>>Empty</option>
	  	     <option value="2" <c:if test="${customerSearch.trackcodeFieldDropDown == '2'}">selected</c:if>>Not Empty</option>	  	     
	      </select>
	    </div>
	    <div class="search2">
	      <p><fmt:message key="registeredBy" />:</p>
	      <input name="registeredBy" type="text" value="<c:out value='${customerSearch.registeredBy}' />" size="15" />
	    </div> 
	    <div class="search2">
	      <p><fmt:message key="zipcode" />:</p>
	      <input name="zipcode" type="text" value="<c:out value='${customerSearch.zipcode}' />" size="15" />
	    </div>
	    <div class="search2">
	    <p><fmt:message key="distance" />:</p>
  			<select name="radius">
   			<option value=""></option>
  			<option value="25" <c:if test="${customerSearch.locationSearch.radius == 25}">selected</c:if> >25 miles</option>
   			<option value="50" <c:if test="${customerSearch.locationSearch.radius == 50}">selected</c:if> >50 miles</option>
  			<option value="100" <c:if test="${customerSearch.locationSearch.radius == 100}">selected</c:if> >100 miles</option>
  			<option value="250" <c:if test="${customerSearch.locationSearch.radius == 250}">selected</c:if> >250 miles</option>
   			<option value="500" <c:if test="${customerSearch.locationSearch.radius == 500}">selected</c:if> >500 miles</option>
   			<option value="1000" <c:if test="${customerSearch.locationSearch.radius == 1000}">selected</c:if> >1000 miles</option>
  			</select>
 		</div> 
	    <div class="search2">
	      <p><fmt:message key="phone" />:</p>
	      <input name="phone" type="text" value="<c:out value='${customerSearch.phone}' />" size="15" />
	    </div>
	    <div class="search2">
	      <p><fmt:message key="cellPhone" />:</p>
	      <input name="cellPhone" type="text" value="<c:out value='${customerSearch.cellPhone}' />" size="15" />
	    </div>
	    <div class="search2">
	      <p><fmt:message key="mainSource" />:</p>
	      <input name="mainSource" type="text" value="<c:out value='${customerSearch.mainSource}' />" size="15" />
	    </div>
	    <div class="search2">
	      <p><fmt:message key="mainSource" />:</p>
	      <select name="mainSourceDropDown">	      
	  	     <option value=""></option>
	         <option value="1" <c:if test="${customerSearch.mainSourceDropDown == '1'}">selected</c:if>>Empty</option>
	  	     <option value="2" <c:if test="${customerSearch.mainSourceDropDown == '2'}">selected</c:if>>Not Empty</option>	  	     
	      </select>
	    </div>
	    <div class="search2">
	      <p><fmt:message key="paid" />:</p>
	      <input name="paid" type="text" value="<c:out value='${customerSearch.paid}' />" size="15" />
	    </div>
	    <div class="search2">
	      <p><fmt:message key="paid" />:</p>
	      <select name="paidDropDown">	      
	  	     <option value=""></option>
	         <option value="1" <c:if test="${customerSearch.paidDropDown == '1'}">selected</c:if>>Empty</option>
	  	     <option value="2" <c:if test="${customerSearch.paidDropDown == '2'}">selected</c:if>>Not Empty</option>	  	     
	      </select>
	    </div>
	    <div class="search2">
	      <p><fmt:message key="medium" />:</p>
	      <input name="medium" type="text" value="<c:out value='${customerSearch.medium}' />" size="15" />
	    </div>
	    <div class="search2">
	      <p><fmt:message key="medium" />:</p>
	      <select name="mediumDropDown">	      
	  	     <option value=""></option>
	         <option value="1" <c:if test="${customerSearch.mediumDropDown == '1'}">selected</c:if>>Empty</option>
	  	     <option value="2" <c:if test="${customerSearch.mediumDropDown == '2'}">selected</c:if>>Not Empty</option>	  	     
	      </select>
	    </div>
	    <div class="search2">
	      <p>Analytics <fmt:message key="language" />:</p>
	      <input name="languageField" type="text" value="<c:out value='${customerSearch.languageField}' />" size="15" />
	    </div>
	    <div class="search2">
	      <p><fmt:message key="language" />:</p>
	      <select name="languageFieldDropDown">	      
	  	     <option value=""></option>
	         <option value="1" <c:if test="${customerSearch.languageFieldDropDown == '1'}">selected</c:if>>Empty</option>
	  	     <option value="2" <c:if test="${customerSearch.languageFieldDropDown == '2'}">selected</c:if>>Not Empty</option>	  	     
	      </select>
	    </div>
	    <div class="search2">
	      <p><fmt:message key="idCard" />:</p>
	      <input name="cardId" type="text" value="<c:out value='${customerSearch.cardId}' />" size="15" />
	    </div> 
	    <div class="search2">
	      <p><fmt:message key="city" />:</p>
	      <input name="city" type="text" value="<c:out value='${customerSearch.city}' />" size="15" />
	    </div>
	    <div class="search2">
	      <p><fmt:message key="state" />:</p>
	      <input name="state" type="text" value="<c:out value='${customerSearch.state}' />" size="15" />
	    </div>
	    <div class="search2">
	      <p><fmt:message key="country" />:</p>
			<select name="country">
            <option value="" ></option>
            <c:forEach items="${model.countries}" var="country" varStatus="status">
              <option value="${country.code}" >${country.name}</option>
            </c:forEach>
        	</select>
	    </div> 
	    <div class="search2">
	      <p><fmt:message key="region" />:</p>
			<select name="region">
            <option value="" ></option>
            <option value="non USA"<c:if test="${customerSearch.region == 'non USA'}">selected</c:if>>non USA</option>
            <c:forEach items="${model.region}" var="region" varStatus="status">
              <option value="${region.region}"<c:if test="${customerSearch.region == region.region}">selected</c:if>>${region.region}</option>
            </c:forEach>           
        	</select>
	    </div>
		<div class="search2">
	      <p><fmt:message key="nationalRegion" />:</p>
			<select name="nationalRegion">
            <option value="" ></option>
            <c:forEach items="${model.nationalRegion}" var="nationalRegion" varStatus="status">
              <option value="${nationalRegion.region}"<c:if test="${customerSearch.nationalRegion == nationalRegion.region}">selected</c:if>>${nationalRegion.region}</option>
            </c:forEach>
            
        	</select>
	    </div>
	    <div class="search2">
	      <p><fmt:message key="timeZone" />:</p>
			<select name="timeZone">
            <option value="" ></option>
              <option value="CT"<c:if test="${customerSearch.timeZone == 'CT'}">selected</c:if>>CT</option>
              <option value="PT"<c:if test="${customerSearch.timeZone == 'PT'}">selected</c:if>>PT</option>
              <option value="MT"<c:if test="${customerSearch.timeZone == 'MT'}">selected</c:if>>MT</option>
              <option value="ET"<c:if test="${customerSearch.timeZone == 'ET'}">selected</c:if>>ET</option>
        	</select>
	    </div>
	    <c:if test="${model.languageCodes != null}">
        <div class="search2">
	      <p><fmt:message key="language" />:</p>
			<select name="language">
            <option value="" ></option>
            <option value="en" <c:if test="${customerSearch.languageCode == 'en'}">selected</c:if> ><fmt:message key="language_en"/></option>
            <c:forEach items="${model.languageCodes}" var="i18n">         	
          	  <option value="${i18n.languageCode}" <c:if test="${customerSearch.languageCode == i18n.languageCode}">selected</c:if>><fmt:message key="language_${i18n.languageCode}"/></option>
        	</c:forEach>
        	</select>
	    </div> 
	    </c:if>
	    
	    <div class="search2">
	      <p><fmt:message key="orders"/>:   <a href="#operatorNumOrder" rel="type:element" id="mbNumOrderOperator1" class="mbNumOrderOperator" style="margin-left: 55px;"><img class="toolTipImg" title="Advanced Search::Click to see more Options" src="../graphics/question.gif" /></a></p>
		  <div id="operatorNumOrder" class="miniWindowWrapper searchOp displayNone">
	    	<div class="header">Advanced Search</div>
	    	  <fieldset class="">
	    	    <label class="AStitle"><fmt:message key="orders"/></label>
	    	    <input name="numOrder" type="text" value="<c:out value='${customerSearch.numOrder}' />" size="15" disabled />
	    	  </fieldset>
	    	  <fieldset class="bottom">
	    	    <label class="AStitle"><fmt:message key="operator"/></label>
	    	    <div class="options">
	    	       <label class="radio">
	    	       		<input type="radio" <c:if test="${customerSearch.numOrderOperator == 0}" >checked="checked"</c:if> value="0" name="numOrderOp" onchange="selectOperator(this.value, 'numOrder');" /> <strong> Greater Than (&gt;)</strong>
	    	       </label>
	    	       <label class="radio">
	    	       		<input type="radio" <c:if test="${customerSearch.numOrderOperator == 1}" >checked="checked"</c:if> value="1" name="numOrderOp" onchange="selectOperator(this.value, 'numOrder');" /> <strong> Equal (=) </strong>
	    	       </label>
	    	       <label class="radio">
	    	       		<input type="radio" <c:if test="${customerSearch.numOrderOperator == 2}" >checked="checked"</c:if> value="2" name="numOrderOp" onchange="selectOperator(this.value, 'numOrder');" /> <strong> Less Than (&lt;) </strong>
	    	       </label>
	    	    </div>
	    	  </fieldset>
	    	  <fieldset>
	    	     <div class="button">
	      			<input type="submit" value="Search" onclick="document.searchform.submit();"/>
      			 </div>
	    	  </fieldset>
		  </div>
		  <input name="numOrder" type="text" value="<c:out value='${customerSearch.numOrder}' />" size="15" />
		  <input type="hidden" name="numOrderOperator" id="numOrderOperator"/>
	    </div>
	    
	    <div class="search2">
	      <p><fmt:message key="orderTotal"/>:   <a href="#operatorTotalOrder" rel="type:element" id="mbTotalOrderOperator1" class="mbTotalOrderOperator" style="margin-left: 30px;"><img class="toolTipImg" title="Advanced Search::Click to see more Options" src="../graphics/question.gif" /></a></p>
		  <div id="operatorTotalOrder" class="miniWindowWrapper searchOp displayNone">
	    	<div class="header">Advanced Search</div>
	    	  <fieldset class="">
	    	    <label class="AStitle"><fmt:message key="orderTotal"/></label>
	    	    <input name="totalOrder" type="text" value="<c:out value='${customerSearch.totalOrder}' />" size="15" disabled />
	    	  </fieldset>
	    	  <fieldset class="bottom">
	    	    <label class="AStitle"><fmt:message key="operator"/></label>
	    	    <div class="options">
	    	       <label class="radio">
	    	       		<input type="radio" <c:if test="${customerSearch.totalOrderOperator == 0}" >checked="checked"</c:if> value="0" name="totalOrderOp" onchange="selectOperator(this.value, 'totalOrder');" /> <strong> Greater Than (&gt;)</strong>
	    	       </label>
	    	       <label class="radio">
	    	       		<input type="radio" <c:if test="${customerSearch.totalOrderOperator == 1}" >checked="checked"</c:if> value="1" name="totalOrderOp" onchange="selectOperator(this.value, 'totalOrder');" /> <strong>Equal (=)</strong>
	    	       </label>
	    	       <label class="radio">
	    	       		<input type="radio" <c:if test="${customerSearch.totalOrderOperator == 2}" >checked="checked"</c:if> value="2" name="totalOrderOp" onchange="selectOperator(this.value, 'totalOrder');" /> <strong>Less Than (&lt;) </strong>
	    	       </label>
	    	    </div>
	    	  </fieldset>
	    	  <fieldset>
	    	     <div class="button">
	      			<input type="submit" value="Search" onclick="document.searchform.submit();"/>
      			 </div>
	    	  </fieldset>
		  </div>
		  <input name="totalOrder" type="text" value="<c:out value='${customerSearch.totalOrder}' />" size="15" />
		  <input type="hidden" name="totalOrderOperator" id="totalOrderOperator"/>
	    </div>
	    
	    <div class="search2">
	      <p><c:set var="customerFieldFlag" value="false" />
	      <select class="extraFieldSearch" name="customerFieldNumber">
	  	    <c:forEach items="${model.customerFieldList}" var="customerField">
	  	     <c:if test="${!empty customerField.name}" ><c:set var="customerFieldFlag" value="true" />
	  	     <option style="font-size:normal;" value="${customerField.id}" <c:if test="${customerSearch.customerFieldNumber == customerField.id}">selected</c:if>><c:out value="${customerField.name}" />:</option>
	  	     </c:if>
	  	    </c:forEach>
	      </select>
	      </p>
	      <c:if test="${customerFieldFlag}" >
	       <input name="customerField" type="text" value="<c:out value='${customerSearch.customerField}' />" size="11" />
	      </c:if>
	    </div>   
	    <div class="button">
	      <input type="submit" value="Search"/>
		</div>
	  <!-- content area -->
	  </td>
	  <td valign="top">
	  <div id="sideBar">
	  <a name="#tabCustomer" ></a>
	  <a href="#tabCustomer" id="sideBarTab"><img id="h_toggle"  src="../graphics/tabSliderLeft.png" alt="sideBar" title="sideBar" /></a>
      <div id="sideBarContents" style="width:0px;">
	  <table cellpadding="0" cellspacing="0" border="0" class="module" >
      <tr>
      <td class="topl_g"><div align="left" class="searchHeaderAdvanced"><h2><fmt:message key="advancedSearch" /></h2></div></td>
	  <td class="topr_g" ></td></tr>
      <tr><td class="boxmidlrg" >
  
		<table class="searchBoxLeftAS"><tr><td>
		<div id="sideBarContentsInner">
			<div class="advancedSearch">
		      <p>start:</p>
		      <input style="width:90px;margin-right:5px;" name="startDate" id="startDate" type="text" value="<fmt:formatDate type="date" pattern="MM/dd/yyyy" value="${customerSearch.startDate}"/>" readonly size="11" />
			  <img class="calendarImage"  id="startDate_trigger" src="../graphics/calendarIcon.jpg" />
			  <script type="text/javascript">
	    		Calendar.setup({
	        			inputField     :    "startDate",   // id of the input field
	        			showsTime      :    false,
	        			ifFormat       :    "%m/%d/%Y",   // format of the input field
	        			button         :    "startDate_trigger"   // trigger for the calendar (button ID)
	    		});	
			  </script>
		    </div>
		    <div style="clear:both;"></div>    
		    <div class="advancedSearch">
		      <p>end:</p>
		      <input style="width:90px;margin-right:5px;" name="endDate" id="endDate" type="text" value="<fmt:formatDate type="date" pattern="MM/dd/yyyy" value='${customerSearch.endDate}'/>" readonly size="11" />
			  <img class="calendarImage"  id="endDate_trigger" src="../graphics/calendarIcon.jpg" />
			  <script type="text/javascript">
	    		Calendar.setup({
	        			inputField     :    "endDate",   // id of the input field
	        			showsTime      :    false,
	        			ifFormat       :    "%m/%d/%Y",   // format of the input field
	        			button         :    "endDate_trigger"   // trigger for the calendar (button ID)
	    		});	
			  </script>
			</div>  
			<div style="clear:both;"></div>    
		    <div class="advancedSearch">
		      <table border="0">
		       <tr> <td class="advancedSearchDateInterval"><input style="width:12px;margin-right:5px;border: 0px" name="intervaldate" id="today" type="radio" onclick="UpdateStartEndDate(1);" />Today</td> </tr>
		       <tr> <td class="advancedSearchDateInterval"><input style="width:12px;margin-right:5px;border: 0px" name="intervaldate" id="yesterday" type="radio" onclick="UpdateStartEndDate(0);" />Yesterday</td> </tr>
		       <tr> <td class="advancedSearchDateInterval"><input style="width:12px;margin-right:5px;border: 0px" name="intervaldate" id="weekToDate" type="radio" onclick="UpdateStartEndDate(2);" />Week To Date</td> </tr>
		       <tr> <td class="advancedSearchDateInterval"><input style="width:12px;margin-right:5px;border: 0px" name="intervaldate" id="monthToDate" type="radio" onclick="UpdateStartEndDate(3);" />Month To Date</td> </tr>
		       <tr> <td class="advancedSearchDateInterval"><input style="width:12px;margin-right:5px;border: 0px" name="intervaldate" id="yearToDate" type="radio" onclick="UpdateStartEndDate(10);" />Year To Date</td> </tr>
		       <tr> <td class="advancedSearchDateInterval"><input style="width:12px;margin-right:5px;border: 0px" name="intervaldate" id="reset" type="radio" onclick="UpdateStartEndDate(20);" />Reset</td> </tr>
		      </table>
		    </div>    
		</div>
		</td></tr></table>
	  
	  <c:if test="${gSiteConfig['gSHOPPING_CART'] and siteConfig['SHOPPINGCART_SEARCH'].value == 'true'}">
	  <h4 style="margin-left:11px;">Shopping Cart</h4>
	  <table class="searchBoxLeftAS"><tr><td>
		<div id="sideBarContentsInner">
			<div class="advancedSearch">
		      <p>start:</p>
		      <input style="width:90px;margin-right:5px;" name="cartStartDate" id="cartStartDate" type="text" value="<fmt:formatDate type="date" pattern="MM/dd/yyyy" value="${customerSearch.cartStartDate}"/>" readonly size="11" />
			  <img class="calendarImage"  id="cartStartDate_trigger" src="../graphics/calendarIcon.jpg" />
			  <script type="text/javascript">
	    		Calendar.setup({
	        			inputField     :    "cartStartDate",   // id of the input field
	        			showsTime      :    false,
	        			ifFormat       :    "%m/%d/%Y",   // format of the input field
	        			button         :    "cartStartDate_trigger"   // trigger for the calendar (button ID)
	    		});	
			  </script>
		    </div>
		    <div style="clear:both;"></div>    
		    <div class="advancedSearch">
		      <p>end:</p>
		      <input style="width:90px;margin-right:5px;" name="cartEndDate" id="cartEndDate" type="text" value="<fmt:formatDate type="date" pattern="MM/dd/yyyy" value='${customerSearch.cartEndDate}'/>" readonly size="11" />
			  <img class="calendarImage"  id="cartEndDate_trigger" src="../graphics/calendarIcon.jpg" />
			  <script type="text/javascript">
	    		Calendar.setup({
	        			inputField     :    "cartEndDate",   // id of the input field
	        			showsTime      :    false,
	        			ifFormat       :    "%m/%d/%Y",   // format of the input field
	        			button         :    "cartEndDate_trigger"   // trigger for the calendar (button ID)
	    		});	
			  </script>
			</div>  
			<div style="clear:both;"></div>    
		    <div class="advancedSearch">
		      <table border="0">
		       <tr> <td class="advancedSearchDateInterval"><input style="width:12px;margin-right:5px;border: 0px" name="cartIntervalDate" id="today" type="radio" value="1" onclick="UpdateCartStartEndDate(1);" />Today</td> </tr>
		       <tr> <td class="advancedSearchDateInterval"><input style="width:12px;margin-right:5px;border: 0px" name="cartIntervalDate" id="weekToDate" type="radio" value="2" onclick="UpdateCartStartEndDate(2);" />Week To Date</td> </tr>
		       <tr> <td class="advancedSearchDateInterval"><input style="width:12px;margin-right:5px;border: 0px" name="cartIntervalDate" id="monthToDate" type="radio" value="3" onclick="UpdateCartStartEndDate(3);" />Month To Date</td> </tr>
		       <tr> <td class="advancedSearchDateInterval"><input style="width:12px;margin-right:5px;border: 0px" name="cartIntervalDate" id="yearToDate" type="radio" value="10" onclick="UpdateCartStartEndDate(10);" />Year To Date</td> </tr>
		       <tr> <td class="advancedSearchDateInterval"><input style="width:12px;margin-right:5px;border: 0px" name="cartIntervalDate" id="reset" type="radio" value="20" onclick="UpdateCartStartEndDate(20);" />Reset</td> </tr>
		      </table>
		    </div>    
		</div>
		</td></tr></table></c:if>
		
	  <h4 style="margin-left:11px;">Last Login</h4>
	  <table class="searchBoxLeftAS"><tr><td>
		<div id="sideBarContentsInner">
			<div class="advancedSearch">
		      <p>start:</p>
		      <input style="width:90px;margin-right:5px;" name="loginStartDate" id="loginStartDate" type="text" value="<fmt:formatDate type="date" pattern="MM/dd/yyyy" value="${customerSearch.loginStartDate}"/>" readonly size="11" />
			  <img class="calendarImage"  id="loginStartDate_trigger" src="../graphics/calendarIcon.jpg" />
			  <script type="text/javascript">
	    		Calendar.setup({
	        			inputField     :    "loginStartDate",   // id of the input field
	        			showsTime      :    false,
	        			ifFormat       :    "%m/%d/%Y",   // format of the input field
	        			button         :    "loginStartDate_trigger"   // trigger for the calendar (button ID)
	    		});	
			  </script>
		    </div>
		    <div style="clear:both;"></div>    
		    <div class="advancedSearch">
		      <p>end:</p>
		      <input style="width:90px;margin-right:5px;" name="loginEndDate" id="loginEndDate" type="text" value="<fmt:formatDate type="date" pattern="MM/dd/yyyy" value='${customerSearch.loginEndDate}'/>" readonly size="11" />
			  <img class="calendarImage"  id="loginEndDate_trigger" src="../graphics/calendarIcon.jpg" />
			  <script type="text/javascript">
	    		Calendar.setup({
	        			inputField     :    "loginEndDate",   // id of the input field
	        			showsTime      :    false,
	        			ifFormat       :    "%m/%d/%Y",   // format of the input field
	        			button         :    "loginEndDate_trigger"   // trigger for the calendar (button ID)
	    		});	
			  </script>
			</div>  
			<div style="clear:both;"></div>    
		    <div class="advancedSearch">
		      <table border="0">
		       <tr> <td class="advancedSearchDateInterval"><input style="width:12px;margin-right:5px;border: 0px" name="loginIntervalDate" id="today" type="radio" value="1" onclick="UpdateLoginStartEndDate(1);" />Today</td> </tr>
		       <tr> <td class="advancedSearchDateInterval"><input style="width:12px;margin-right:5px;border: 0px" name="loginIntervalDate" id="weekToDate" type="radio" value="2" onclick="UpdateLoginStartEndDate(2);" />Week To Date</td> </tr>
		       <tr> <td class="advancedSearchDateInterval"><input style="width:12px;margin-right:5px;border: 0px" name="loginIntervalDate" id="monthToDate" type="radio" value="3" onclick="UpdateLoginStartEndDate(3);" />Month To Date</td> </tr>
		       <tr> <td class="advancedSearchDateInterval"><input style="width:12px;margin-right:5px;border: 0px" name="loginIntervalDate" id="yearToDate" type="radio" value="10" onclick="UpdateLoginStartEndDate(10);" />Year To Date</td> </tr>
		       <tr> <td class="advancedSearchDateInterval"><input style="width:12px;margin-right:5px;border: 0px" name="loginIntervalDate" id="reset" type="radio" value="20" onclick="UpdateLoginStartEndDate(20);" />Reset</td> </tr>
		      </table>
		    </div>    
		</div>
		</td></tr></table>
	  </td><td class="boxmidr" ></td></tr>
      <tr><td class="botl"></td><td class="botr"></td></tr>
      </table></div>
    
      </div>
	  </td>
	  </tr></table>
	  </form>
	</div>  
	</div>  
	<div class="menudiv"></div>  
	</c:if>  	
	  		  
    </td><td class="boxmidr" ></td></tr>
    <tr><td class="botl"></td><td class="botr"></td></tr></table>
  </div>
  
  
  
  