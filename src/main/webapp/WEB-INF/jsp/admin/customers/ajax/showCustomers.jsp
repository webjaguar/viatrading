<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<c:choose>
<c:when test="${(!empty model.search.ajaxSelect) and model.search.ajaxSelect == 'firstname'}">
  <c:forEach items="${model.customers}" var="customer">
	<li>
	  <span><c:out value="${customer.address.firstName}" /></span>
	  <span style="display:none" id="userid"><c:out value="${customer.id}" /></span>
	  <a class="extraInfo"><c:out value="${customer.address.lastName}" /></a>
	  <span style="display:none" id="company${customer.id}"><c:out value="${customer.address.company}" /></span>
	  <span style="display:none" id="country${customer.id}"><c:out value="${customer.address.country}" /></span>
	  <span style="display:none" id="addr1${customer.id}"><c:out value="${customer.address.addr1}" /></span>
	  <span style="display:none" id="addr2${customer.id}"><c:out value="${customer.address.addr2}" /></span>
	  <span style="display:none" id="city${customer.id}"><c:out value="${customer.address.city}" /></span>
	  <span style="display:none" id="stateProvince${customer.id}"><c:out value="${customer.address.stateProvince}" /></span>
	  <span style="display:none" id="zip${customer.id}"><c:out value="${customer.address.zip}" /></span>
	  <span style="display:none" id="phone${customer.id}"><c:out value="${customer.address.phone}" /></span>
	  <span style="display:none" id="cellPhone${customer.id}"><c:out value="${customer.address.cellPhone}" /></span>
	  <span style="display:none" id="fax${customer.id}"><c:out value="${customer.address.fax}" /></span>
	  <span style="display:none" id="residential${customer.id}"><c:out value="${customer.address.residential}" /></span>
	  <span style="display:none" id="liftGate${customer.id}"><c:out value="${customer.address.liftGate}" /></span>
	</li>
  </c:forEach>
</c:when>
<c:when test="${(!empty model.search.ajaxSelect) and model.search.ajaxSelect == 'lastname'}">
  <c:forEach items="${model.customers}" var="customer">
	<li>
	  <span><c:out value="${customer.address.lastName}" /></span>
	  <span style="display:none" id="userid"><c:out value="${customer.id}" /></span>
	  <a class="extraInfo"><c:out value="${customer.address.firstName}" /></a>
	  <span style="display:none" id="company${customer.id}"><c:out value="${customer.address.company}" /></span>
	  <span style="display:none" id="country${customer.id}"><c:out value="${customer.address.country}" /></span>
	  <span style="display:none" id="addr1${customer.id}"><c:out value="${customer.address.addr1}" /></span>
	  <span style="display:none" id="addr2${customer.id}"><c:out value="${customer.address.addr2}" /></span>
	  <span style="display:none" id="city${customer.id}"><c:out value="${customer.address.city}" /></span>
	  <span style="display:none" id="stateProvince${customer.id}"><c:out value="${customer.address.stateProvince}" /></span>
	  <span style="display:none" id="zip${customer.id}"><c:out value="${customer.address.zip}" /></span>
	  <span style="display:none" id="phone${customer.id}"><c:out value="${customer.address.phone}" /></span>
	  <span style="display:none" id="cellPhone${customer.id}"><c:out value="${customer.address.cellPhone}" /></span>
	  <span style="display:none" id="fax${customer.id}"><c:out value="${customer.address.fax}" /></span>
	  <span style="display:none" id="residential${customer.id}"><c:out value="${customer.address.residential}" /></span>
	  <span style="display:none" id="liftGate${customer.id}"><c:out value="${customer.address.liftGate}" /></span>
	</li>
  </c:forEach>
</c:when>
</c:choose>

