<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="/WEB-INF/tlds/spring.tld" prefix="spring" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ taglib uri="http://www.springframework.org/security/tags" prefix="sec" %>

<tiles:insertDefinition name="admin.customers" flush="true">
<tiles:putAttribute name="content" type="string">
<!--  
<script type="text/javascript" src="<c:url value="/FCKeditor/fckeditor.js"/>"></script>
<script type="text/javascript"> 
window.addEvent('domready', function(){			
			var tabs1 = new SimpleTabs($('tab-block-1'), { selector: 'h4'});
			var Tips1 = new Tips($$('.toolTipImg'));
		});
</script>
-->
<form:form commandName="customerExtContactForm" method="post">
<!-- main box -->
  <div id="mboxfull">
  
  <!-- start table -->
  <table cellpadding="0" cellspacing="0" border="0" class="module" >
  <tr>
    <td class="topl_g">
	  
	  <!-- breadcrumb -->
	  <p class="breadcrumb">
	    <a href="../customers">Customers</a> &gt;
	    <a href="../customers/customer.jhtm?id=${param.uid}"><c:out value="${model.firstName}" /> <c:out value="${model.lastName}" /></a> &gt;
	    <a href="../customers/customerExtContactList.jhtm?id=${param.uid}"><fmt:message key="contacts" /></a> &gt;
	    <c:if test="${!customerExtContactForm.newCustomerExtContact}"><fmt:message key="contactEdit" /></c:if><c:if test="${customerExtContactForm.newCustomerExtContact}"><fmt:message key="contactAdd" /></c:if>  
	  </p>
	  
	  <!-- Error Message -->
  	  <c:if test="${!empty message}">
		  <div class="message"><spring:message code="${message}" /></div>
	  </c:if>
	  <spring:hasBindErrors name="customerExtContactForm">
       <span class="error">Please fix all errors!</span>
      </spring:hasBindErrors>

	  </td><td class="topr_g" ></td></tr>
  <tr><td class="boxmidlrg" >

 <!-- tabs -->
 <div id="tab-block-1">

	<h4 title="<fmt:message key='contact' />"><fmt:message key="crmContact" /></h4>
	<div>
	<!-- start tab -->
         <c:set var="classIndex" value="0" />
         
	  	<div class="listdivi ln tabdivi"></div>
	  	<div class="listdivi"></div>
	  	
			  	
	  	<div class="list${classIndex % 2}"><c:set var="classIndex" value="${classIndex+1}" />
		<div class="listfl"><div class="requiredField"><fmt:message key="name" />:</div></div>
		<div class="listp">
		<!-- input field -->
            <form:input path="customerExtContact.name" htmlEscape="true" />
            <form:errors path="customerExtContact.name" cssClass="error" />
		<!-- end input field -->   	
		</div>
		</div>
		

	  	<div class="list${classIndex % 2}"><c:set var="classIndex" value="${classIndex+1}" />
		<div class="listfl"><fmt:message key="position" />:</div>
		<div class="listp">
		<!-- input field -->
            <form:input path="customerExtContact.position" htmlEscape="true" />
            <form:errors path="customerExtContact.position" cssClass="error" />
		<!-- end input field -->   	
		</div>
		</div>
		
         <div class="list${classIndex % 2}"><c:set var="classIndex" value="${classIndex+1}" /> 
         <div class="listfl"><fmt:message key="email" />:</div>
		 <div class="listp">
		 <!-- input field -->
             <form:input path="customerExtContact.email"  htmlEscape="true" />
             <form:errors path="customerExtContact.email" cssClass="error" />
         <!-- end input field -->   	
		 </div>
		 </div>         
         <div class="list${classIndex % 2}"><c:set var="classIndex" value="${classIndex+1}" /> 
         <div class="listfl"><fmt:message key="phone1" />:</div>
		 <div class="listp">
		 <!-- input field -->
             <form:input path="customerExtContact.phone1"  htmlEscape="true" />
             <form:errors path="customerExtContact.phone1" cssClass="error" />
         <!-- end input field -->   	
		 </div>
		 </div>

         <div class="list${classIndex % 2}"><c:set var="classIndex" value="${classIndex+1}" /> 
         <div class="listfl"><fmt:message key="phone2" />:</div>
		 <div class="listp">
		 <!-- input field -->
             <form:input path="customerExtContact.phone2"  htmlEscape="true" />
             <form:errors path="customerExtContact.phone2" cssClass="error" />
         <!-- end input field -->   	
		 </div>
		 </div>

		 

	<!-- end tab -->        
	</div>  
	        	
<!-- end tabs -->			
</div>

<!-- start button -->
<div align="left" class="button"> 
<c:if test="${customerExtContactForm.newCustomerExtContact}">
    <input type="submit" value="<spring:message code="contactAdd"/>" /> 
</c:if>
<c:if test="${!customerExtContactForm.newCustomerExtContact and siteConfig['MBLOCK'].value != 'true'}">
  <input type="submit" value="<spring:message code="contactUpdate"/>" />
  <input type="submit" value="<spring:message code="contactDelete"/>" name="_delete" onClick="return confirm('Delete permanently?')" />

</c:if>
  <input type="submit" value="<spring:message code="cancel"/>" name="_cancel" />
</div>
<!-- end button -->	   

  <!-- end table -->
    </td><td class="boxmidr" ></td></tr>
  <tr><td class="botl"></td><td class="botr"></td></tr>
  </table>
  
<!-- end main box -->  
</div>          	
</form:form>

<script language="JavaScript">

</script>

  </tiles:putAttribute>
</tiles:insertDefinition>


