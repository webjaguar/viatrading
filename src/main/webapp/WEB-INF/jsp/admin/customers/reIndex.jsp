<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<tiles:insertDefinition name="admin.customers" flush="true">
  <tiles:putAttribute name="content" type="string">

<!-- main box -->
  <div id="mboxfull">
    
    <c:if test="${results == null}">
	<div class="message">Pre-calculation/indexing has been scheduled to run at midnight.</div>
	</c:if>
	<pre>
<c:out value="${results}" />
	</pre>
	
  </div>
<!-- end main box -->  

  </tiles:putAttribute>    
</tiles:insertDefinition>