<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="/WEB-INF/tlds/spring.tld" prefix="spring" %>
<%@ taglib uri="http://www.springframework.org/security/tags" prefix="sec" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>

<tiles:insertDefinition name="admin.customers.vba" flush="true">
  <tiles:putAttribute name="content" type="string">
<script type="text/javascript">
<!--
function checkStatus() {
	return confirm("An email will be sent to every supplier. Are you sure about the payments? ");
	return true;
}
function updateSelectedPaymentInfo() {
	var orders = orders + "-1,";
    var orders="";
    for(var j=0;j<document.getElementById("orderCount").value;j++){
            if(document.getElementById("startDate"+j)!=null && document.getElementById("startDate"+j).value!=null && document.getElementById("startDate"+j).value!=""){
                    orders = orders + document.getElementById("orderT"+j).value + ",";          
            }       
    }
	for(var j=0;j<document.getElementById("orderCount").value;j++){
          var orderId = null; 
          if(document.getElementById("orderT"+j)!=null){
                  orderId = document.getElementById("orderT"+j).value; 
          }else{
                  orderId = -1;
          }
          var sku = null;
          if(document.getElementById("productSku_"+orderId)!=null && document.getElementById("productSku_"+orderId).value!=null && document.getElementById("productSku_"+orderId).value!="")
          {
                  sku = document.getElementById("productSku_"+orderId).value;
          }else{
                  sku = "";
          }
          if(document.getElementById("startDate"+j+"_"+sku)!=null && document.getElementById("startDate"+j+"_"+sku).value!=null && document.getElementById("startDate"+j+"_"+sku).value!=""){
                  orders = orders + orderId + ",";
          }
    }
	orders = orders + "-1";
    var res = orders.split(","); 
    subTotal = 0.0;
    for(var k=0;k<res.length;k++){
            subTotal = subTotal + Math.round(((document.getElementById("subTotal"+res[k])!=null&&document.getElementById("subTotal"+res[k]).value!=null)?document.getElementById("subTotal"+res[k]).value:0.0)*1000)/1000;
    }
    
    var amountPay = 0.0;
    for(var k=0;k<res.length;k++){
          total = (document.getElementById("amountPay"+res[k])!=null&&document.getElementById("amountPay"+res[k]).value!=null)?document.getElementById("amountPay"+res[k]).value:0.0;
          amountPay = (amountPay + (Math.round(total*1000)/1000));
    }
    //alert(amountPay);
    document.getElementById("subTotal").innerHTML = formatNumber(subTotal.toFixed(2));
    document.getElementById("amountToPay").innerHTML = formatNumber(amountPay.toFixed(2));
 }
 
 function formatNumber(num){
	return num.toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, "$1,");
 }

//-->
</script>
<sec:authorize ifAnyGranted="ROLE_AEM,ROLE_ADMIN_${prop_site_id},ROLE_OUTGOING_PAYMENT_VIEW_LIST">    
<form id="list" action="virtualBankPayment.jhtm" method="post" name="list_form">
<input type="hidden" id="sort" name="sort" value="${vbaOrderSearch.sort}" />
<!-- main box -->
<div id="mboxfull">
<!-- start table -->
<table cellpadding="0" cellspacing="0" border="0" class="module" >
  <tr>
  	<td class="topl_g">
  	 <!-- breadcrumb -->
	  <p class="breadcrumb">		     
	    <a href="../customers"><fmt:message key="customers"/></a> &gt;
	    <fmt:message key="outgoingPayments"/>
	  </p>
	  
	  <!-- Error Message -->
  	  <c:if test="${!empty model.message}">
		 <div class="highlight error"><spring:message code="${model.message}" arguments="${arguments}"/></div>
	  </c:if>
  	  <!-- header image -->
	  <img class="headerImage" src="../graphics/payment.gif" />
	  
	  	<div class="optionBox"><div class="titlePrivilege"><fmt:message key="options" /></div></div>
		<div class="arrow_drop arrowImageRight"></div>
		<div style="clear: both;"></div>
		<div class="information displayNone" id="information" style="float:left;"> 
			
			
			<div style="float:left;padding: 5px">
	           <table class="productBatchTool" cellpadding="5" cellspacing="5">
	           <tr>
	            <td valign="top">
	             <table cellpadding="1" cellspacing="1">
	              <tr style="height:30px;white-space: nowrap">
	               <td valign="top" width="20px"><img class="toolTipImg" title="<fmt:message key="userExpectedDueDate" />::Change SubStatus of multiple orders" src="../graphics/question.gif" /></td>
	               <td valign="top"><h3>Batch Update</h3></td>
	              </tr>
	             </table>
	            </td>
	           </tr>
	           <tr>
	            <td valign="top">
	             <table cellpadding="1" cellspacing="1">
	              <tr style="">
			       <td>
					<span><fmt:message key="dateOfPay" />: </span>
                                   
	                 <c:choose>
					  	<c:when test="${order.commission != 0 and order.commission != '' }">
					  	<input name="__dateOfPay" id="__dateOfPay" type="text" value="<fmt:formatDate type="date" pattern="MM/dd/yyyy" value="${order.dateOfPay}"/>" readonly size="11" />			      	
					  	<script type="text/javascript">
			    		Calendar.setup({
			        			inputField     :    "__dateOfPay",   // id of the input field
			        			showsTime      :    true,
			        			ifFormat       :    "%m-%d-%Y %I:%M %p",   // format of the input field
			        			button         :    "dateOfPay", // trigger for the calendar (button ID)
			        			timeFormat     :    "12",
			        			onUpdate       :    updateSelectedPaymentInfo
			    		});
					  	</script>
					  	</c:when>
					  	<c:otherwise>
					  	<input name="__dateOfPay" id="__dateOfPay" type="text" value="<fmt:formatDate type="date" pattern="MM/dd/yyyy" value="${order.dateOfPay}"/>" readonly size="11" />			      	
					  	<script type="text/javascript">
			    		Calendar.setup({
			        			inputField     :    "__dateOfPay",   // id of the input field
			        			showsTime      :    true,
			        			ifFormat       :     "%m-%d-%Y %I:%M %p",   // format of the input field
			        			button         :    "dateOfPay", // trigger for the calendar (button ID)
			        			timeFormat     :    "12",
			        			onUpdate       :    updateSelectedPaymentInfo
			    		});
					  	</script>
					  	</c:otherwise>
					  </c:choose> 
			        			        
			       </td>
			     </tr>
			     			     
			     <tr style="">
			       <td>
			        <span><fmt:message key="paymentMethod" />: </span>
				     <c:choose>
					  	<c:when test="${order.commission != 0 and order.commission !=''}">				  		
					  		<select name="__paymentMethod" id="__paymentMethod">
					  			<option value=""></option>
					  			<option value="vba"><fmt:message key="vba"/></option>
							  	<c:forTokens items="${siteConfig['VBA_PAYMENT_OPTIONS'].value}" delims="," var="paymentType">
						          <option value="${paymentType}"> <c:if test="${order.paymentMethod == paymentType}">selected</c:if><c:out value="${paymentType}"/></option>
						  	    </c:forTokens>	
					  		</select>
					  	</c:when>
					  	<c:otherwise>
					  		<select name="__paymentMethod" id="__paymentMethod">
					  			<option value=""></option>
					  			<option value="vba"><fmt:message key="vba"/></option>
							  	<c:forTokens items="${siteConfig['VBA_PAYMENT_OPTIONS'].value}" delims="," var="paymentType">
						          <option value="${paymentType}"> <c:if test="${order.paymentMethod == paymentType}">selected</c:if><c:out value="${paymentType}"/></option>
						  	    </c:forTokens>	
					  		</select>
					  	</c:otherwise>
					  </c:choose>
					</td>
				  </tr>			     
			     			     
			     <tr style="">
			       <td>
			        <span><fmt:message key="note" />: </span>
			        <c:choose>
					  	<c:when test="${order.commission != 0 and order.commission !=''}">				  		
					  		<input name="__note" id="__note" type="text" value="${order.note}" size="20" maxlength="20" class="note">
					  	</c:when>
					  	<c:otherwise>
					  		<input name="__note" id="__note" type="text" value="${order.note}" size="20" maxlength="20" class="note">
					  	</c:otherwise>
					  </c:choose>					  
						<div align="left" class="buttonLeft">
			            	<input type="submit" name="__batchUpdate" value="<fmt:message key="apply" />" onClick="return batchUpdate()"/> 
			            	<%-- <input name="__batchUpdate" value="<fmt:message key="apply" />" onClick="return batchUpdate()"/> --%>
			            </div> 
					</td>
				  </tr>			     		     
				  
	             </table>
	            </td>
	           </tr>
	           </table>
	         </div>			
		</div>	  
    
    </td><td class="topr_g" ></td>	    
  </tr>
  <tr><td class="boxmidlrg" >
	  	
	<!-- tabs -->
	<div class="tab-wrapper">
		<h4 title="List of affiliates"></h4>
		<div>
		<!-- start tab -->
	
		  	<%-- <div class="listdivi ln tabdivi"></div> --%>
		  	<div class="listdivi"></div>
		   
			  	<div class="listlight">
			  	
			  	<table border="0" cellpadding="0" cellspacing="1" width="100%">
				  <tr>
				  <c:if test="${model.orderList.nrOfElements > 0}">
					<td class="pageShowing">
						<fmt:message key="showing">
							<fmt:param value="${model.orderList.firstElementOnPage + 1}" />
							<fmt:param value="${model.orderList.lastElementOnPage + 1}" />
							<fmt:param value="${model.orderList.nrOfElements}" />
						</fmt:message>
					</td>
				  </c:if>
				  <td class="pageNavi">
					<fmt:message key="page" />
					<select name="page" id="page" onchange="submit()">
						<c:forEach begin="1" end="${model.orderList.pageCount}"
							var="page">
							<option value="${page}"
								<c:if test="${page == (model.orderList.page+1)}">selected</c:if>>
								${page}
							</option>
						</c:forEach>
					</select> 
					<fmt:message key="of" />
					<c:out value="${model.orderList.pageCount}" /> 
					|
					<c:if test="${model.orderList.firstPage}">
						<span class="pageNaviDead"><fmt:message key="previous" /></span>
					</c:if>
					<c:if test="${not model.orderList.firstPage}">
						<a
							href="<c:url value="virtualBankPayment.jhtm"><c:param name="page" value="${model.orderList.page}"/><c:param name="size" value="${model.orderList.pageSize}"/></c:url>"
							class="pageNaviLink"><fmt:message key="previous" /></a>
					</c:if>
					|
					<c:if test="${model.orderList.lastPage}">
						<span class="pageNaviDead"><fmt:message key="next" /></span>
					</c:if>
					<c:if test="${not model.orderList.lastPage}">
						<a
							href="<c:url value="virtualBankPayment.jhtm"><c:param name="page" value="${model.orderList.page+2}"/><c:param name="size" value="${model.orderList.pageSize}"/></c:url>"
							class="pageNaviLink"><fmt:message key="next" /></a>
					</c:if>
				</td>
				</tr>
				</table>
			  	
			  	<c:set var="cols" value="0"/>
			  	<!-- start table -->
				<table border="0" cellpadding="2" cellspacing="0" width="100%" class="listings">
					<input type="hidden" name="username" value="${model.username}"/>
				  <tr class="listingsHdr2">
				  <td align="center" class="quickMode"><input type="checkbox" onclick="toggleAll(this)"></td>
				  <td class="indexCol">&nbsp;</td>
				  <td class="listingsHdr3">
						<fmt:message key="orderNumber" />
				  </td>
				  				  <td class="listingsHdr3">
			    	<table cellspacing="0" cellpadding="1">
			    	  <tr>
			    	    <c:choose>
			    	      <c:when test="${vbaOrderSearch.sort == 'date_ordered DESC'}">
			    	        <td class="listingsHdr3">
			    	        <a class="hdrLink" href="#" onClick="document.getElementById('sort').value='date_ordered';document.getElementById('list').submit()"><fmt:message key="orderDate" /></a>
			    	        </td>
			    	        <td class="quickModeRemove"><img src="../graphics/up.gif" border="0"></td>
			    	      </c:when>
			    	      <c:when test="${vbaOrderSearch.sort == 'date_ordered'}">
			    	        <td class="listingsHdr3">
			    	        <a class="hdrLink" href="#" onClick="document.getElementById('sort').value='date_ordered DESC';document.getElementById('list').submit()"><fmt:message key="orderDate" /></a>
			    	        </td>
			    	        <td class="quickModeRemove"><img src="../graphics/down.gif" border="0"></td>
			    	      </c:when>
			    	      <c:otherwise>
			    	        <td colspan="2" class="listingsHdr3">
			    	        <a class="hdrLink" href="#" onClick="document.getElementById('sort').value='date_ordered DESC';document.getElementById('list').submit()"><fmt:message key="orderDate" /></a>
			    	        </td>
			    	      </c:otherwise>
			    	    </c:choose>  
			    	  </tr>
			    	</table>
			      </td>
				  <td class="listingsHdr3">
						<fmt:message key="dateShipped" />
				  </td>
				  <td class="listingsHdr3">
			    	<table cellspacing="0" cellpadding="1">
			    	  <tr>
			    	    <c:choose>
			    	      <c:when test="${vbaOrderSearch.sort == 'product_sku DESC'}">
			    	        <td class="listingsHdr3">
			    	        <a class="hdrLink" href="#" onClick="document.getElementById('sort').value='product_sku';document.getElementById('list').submit()"><fmt:message key="sku" /></a>
			    	        </td>
			    	        <td class="quickModeRemove"><img src="../graphics/up.gif" border="0"></td>
			    	      </c:when>
			    	      <c:when test="${vbaOrderSearch.sort == 'product_sku'}">
			    	        <td class="listingsHdr3">
			    	        <a class="hdrLink" href="#" onClick="document.getElementById('sort').value='product_sku DESC';document.getElementById('list').submit()"><fmt:message key="sku" /></a>
			    	        </td>
			    	        <td class="quickModeRemove"><img src="../graphics/down.gif" border="0"></td>
			    	      </c:when>
			    	      <c:otherwise>
			    	        <td colspan="2" class="listingsHdr3">
			    	        <a class="hdrLink" href="#" onClick="document.getElementById('sort').value='product_sku DESC';document.getElementById('list').submit()"><fmt:message key="sku" /></a>
			    	        </td>
			    	      </c:otherwise>
			    	    </c:choose>  
			    	  </tr>
			    	</table>
			      </td>
				  <td class="listingsHdr3">
						<fmt:message key="companyName" />
				  </td>
				  <td class="listingsHdr3">
						<fmt:message key="cost" />
				  </td>
				  <td  class="listingsHdr3">
						<fmt:message key="quantity" />
				  </td>
				  <td class="listingsHdr3">
						<fmt:message key="subTotal" />
				  </td>
				  <td class="listingsHdr3">
						<fmt:message key="paymentFor" />
				  </td>
				  <td class="listingsHdr3">
						<fmt:message key="amountToPay" />
				  </td>
				  <td class="listingsHdr3">
						<fmt:message key="productConsignmentNote" />
				  </td>
				  <td class="listingsHdr3">
						<fmt:message key="dateOfPay" />
				  </td>
				  <td class="listingsHdr3">
						<fmt:message key="paymentMethod" />
				  </td>
				  <td class="listingsHdr3">
						<fmt:message key="firstOrderDate" />
				  </td>
				  <td class="listingsHdr3">
						<fmt:message key="note" />
				  </td>
				  </tr>
			      <c:set var="totalSubTotal" value="0.0" />
			      <c:set var="totalAmountToPay" value="0.0"  />
			      <input type="hidden" value="${model.orderList.nrOfElements}" id="orderCount"/>
				  <c:forEach items="${model.orderList.pageList}" var="order" varStatus="status">
				  <tr class="row${status.index % 2}" onmouseover="changeStyleClass(this,'rowOver')" onmouseout="changeStyleClass(this,'row${status.index % 2}')">
				  <td align="center" class="quickMode">
				  		 <input name="__selected_id" value="${status.index}" type="checkbox" />
						 <input name="__selected_customer_${status.index}" value="${order.userId}" type="hidden" />
						 <input name="__selected_order_${status.index}" value="${order.orderId}" type="hidden" />
						 <input name="__selected_consignment_${status.index}" value="${order.orderId}" type="hidden" />
					</td>
				  <td class="indexCol">
					<c:out
						value="${status.count }" />
					.
				  </td>
				  <td class="nameCol">				  
				  	<a href="../orders/invoice.jhtm?order=${order.orderId}" class="nameLink"><c:out value="${order.orderId}"/>
				  	<input id="order${status.index}" type="hidden" name="__orderId_" value="${order.orderId}"/>
				  	<input id="orderT${status.index}" type="hidden" name="__orderId_" value="${status.index}"/>
				  	</a>
				  </td>
				  <td align="left">
				  	<fmt:formatDate type="date" timeStyle="default" value="${order.dateOrdered}"/>
				  </td>
				  <td align="left">
				  	<fmt:formatDate type="date" timeStyle="default" value="${order.orderShipDate}" />
				  </td>
				  <td align="left">
				  	<input type="hidden" id="productSku_${order.orderId}" name="productSku_${order.orderId}" value="${order.productSku}"/>
				  	<input type="hidden" id="productSku_${status.index}" name="productSku_${status.index}" value="${order.productSku}"/>
				  	<c:out value="${order.productSku}"/>
				  </td>
				  <td align="left">				  
						<c:out value="${order.companyName}"/>
				  <td align="left">
				  	<c:choose>
				  		<c:when test="${order.cost == null or order.costPercent == null }">
				  			&nbsp;
				  		</c:when>
				  		<c:when test="${order.costPercent == '1' }">
				  			<c:out value="${order.cost}"/>%
				  		</c:when>
				  		<c:otherwise>
				  			<fmt:message key="${siteConfig['CURRENCY'].value}" /><fmt:formatNumber value="${order.cost}" pattern="#,##0.00"/>
				  		</c:otherwise>
				  	</c:choose>
				  </td>
				  <td align="center">
				  	<c:out value="${order.quantity}"/>
				  </td>
				  			  
				  <td align="center">				  
				  	 <fmt:message key="${siteConfig['CURRENCY'].value}" /><fmt:formatNumber value="${order.subTotal}" pattern="#,##0.00" />
				  	 <input id="subTotal${status.index}" type="hidden" value="${order.subTotal}"/>
				  	 <c:set var="totalSubTotal" value="${order.subTotal + totalSubTotal}" />
				  </td>
				  <td align="center">
				  	<c:choose>
				  		<c:when test="${order.commission != 0 and order.commission != '' }">
				  			Affiliate 
				  		</c:when>
				  		<c:otherwise>
				  			Consignment
				  		</c:otherwise>
				  	</c:choose>
				  </td>
				  <td align="center">				  
				  	<c:choose>
				  		<c:when test="${order.commission != 0 and order.commission != '' }">
				  			<fmt:message key="${siteConfig['CURRENCY'].value}" /><fmt:formatNumber value="${order.commission}" pattern="#,##0.00"/><c:set var="totalAmountToPay" value="${order.commission + totalAmountToPay}"  />
				  			<input type="hidden" name="__amountToPay_${status.index}" value="${order.commission}"/>
				  			<input type="hidden" name="__userId_${status.index}" value="${order.userId}"/>
				  			<input id="amountPay${status.index}" type="hidden" value="${order.commission}"/>
				  		</c:when>
				  		<c:otherwise>
				  			<fmt:message key="${siteConfig['CURRENCY'].value}" /><fmt:formatNumber value="${order.consignment}" pattern="#,##0.00"/><c:set var="totalAmountToPay" value="${order.consignment + totalAmountToPay}"  />
				  			<input type="hidden" name="__amountToPay_${status.index}_${order.productSku}" value="${order.consignment}"/>
				  			<input type="hidden" name="__userId_${status.index}_${order.productSku}" value="${order.supplierId}"/>
				  			<input id="amountPay${status.index}" type="hidden" value="${order.consignment}"/>
				  		</c:otherwise>
				  	</c:choose>
				  </td>

				  <td align="left">	
				  	<c:out value="${order.productConsignmentNote}"/>
				  </td>
				  <td align="left">
				  <c:choose>
				  	<c:when test="${order.commission != 0 and order.commission != '' }">
				  	<input style="width:90px;margin-right:5px;" name="__startDate_${status.index}" id="__startDate_${status.index}" type="text" value="<fmt:formatDate type="date" pattern="MM/dd/yyyy" value="${order.dateOfPay}"/>" readonly size="11" />			      	
				  	<script type="text/javascript">
		    		Calendar.setup({
		        			inputField     :    "__startDate_${status.index}",   // id of the input field
		        			showsTime      :    true,
		        			ifFormat       :    "%m-%d-%Y %I:%M %p",   // format of the input field
		        			button         :    "__startDate_${status.index}", // trigger for the calendar (button ID)
		        			timeFormat     :    "12",
		        			onUpdate       :    updateSelectedPaymentInfo
		    		});
				  	</script>
				  	</c:when>
				  	<c:otherwise>
				  	<input style="width:90px;margin-right:5px;" name="__startDate_${status.index}" id="__startDate_${status.index}" type="text" value="<fmt:formatDate type="date" pattern="MM/dd/yyyy" value="${order.dateOfPay}"/>" readonly size="11" />			      	
				  	<script type="text/javascript">
		    		Calendar.setup({
		        			inputField     :    "__startDate_${status.index}",   // id of the input field
		        			showsTime      :    true,
		        			ifFormat       :     "%m-%d-%Y %I:%M %p",   // format of the input field
		        			button         :    "__startDate_${status.index}", // trigger for the calendar (button ID)
		        			timeFormat     :    "12",
		        			onUpdate       :    updateSelectedPaymentInfo
		    		});
				  	</script>
				  	</c:otherwise>
				  </c:choose> 				 
				  </td>
				  <td align="left">
				  <c:choose>
				  	<c:when test="${order.commission != 0 and order.commission !=''}">				  		
				  		<select name="__paymentMethod_${status.index}" id="__paymentMethod_${status.index}">
				  			<option value=""></option>
				  			<option value="vba"><fmt:message key="vba"/></option>
						  	<c:forTokens items="${siteConfig['VBA_PAYMENT_OPTIONS'].value}" delims="," var="paymentType">
					          <option value="${paymentType}"> <c:if test="${order.paymentMethod == paymentType}">selected</c:if><c:out value="${paymentType}"/></option>
					  	    </c:forTokens>	
				  		</select>
				  	</c:when>
				  	<c:otherwise>
				  		<select name="__paymentMethod_${status.index}" id="__paymentMethod_${status.index}">
				  			<option value=""></option>
				  			<option value="vba"><fmt:message key="vba"/></option>
						  	<c:forTokens items="${siteConfig['VBA_PAYMENT_OPTIONS'].value}" delims="," var="paymentType">
					          <option value="${paymentType}"> <c:if test="${order.paymentMethod == paymentType}">selected</c:if><c:out value="${paymentType}"/></option>
					  	    </c:forTokens>	
				  		</select>
				  	</c:otherwise>
				  </c:choose>
				  </td>
				  <td align="left">
				  	<fmt:formatDate type="date" timeStyle="default" value="${order.firstOrderDate}"/>
				  </td>
				  <td align="left">
				  <c:choose>
				  	<c:when test="${order.commission != 0 and order.commission !=''}">				  		
				  		<input name="__note_${status.index}" id="__note_${status.index}" type="text" value="${order.note}" size="20" maxlength="20" class="note">
				  	</c:when>
				  	<c:otherwise>
				  		<input name="__note_${status.index}" id="__note_${status.index}" type="text" value="${order.note}" size="20" maxlength="20" class="note">
				  	</c:otherwise>
				  </c:choose>
				  </td>
				  </tr>
				  </c:forEach>
				  <tr class="totals">
					  <td style="color:#666666;" colspan="9" align="left"><fmt:message key="total" /></td>
					  <td class="numberCol"><fmt:message key="${siteConfig['CURRENCY'].value}" /><fmt:formatNumber value="${totalSubTotal}" pattern="#,##0.00" /></td>
					  <td class="numberCol">&nbsp</td>
				  	  <td class="numberCol"><fmt:message key="${siteConfig['CURRENCY'].value}" /><fmt:formatNumber value="${totalAmountToPay}" pattern="#,##0.00" /></td>
				  	  <td class="numberCol"></td>	
					  <td class="numberCol" id = "subTotal"><fmt:message key="${siteConfig['CURRENCY'].value}" /><fmt:formatNumber value="0.0" pattern="#,##0.00" /></td>
				  	  <td class="numberCol" id = "amountToPay"><fmt:message key="${siteConfig['CURRENCY'].value}" /><fmt:formatNumber value="0.0" pattern="#,##0.00" /></td>	
				  	  <td class="numberCol"></td>	
				  	  <td class="numberCol"></td>	          
				  </tr>				  
				<!-- end table -->  
				</table>
				<table border="0" cellpadding="0" cellspacing="1" width="100%">
				  <tr>
				  <td>&nbsp;</td>  
				  <td class="pageSize">
				  <select name="size" onchange="document.getElementById('page').value=1;submit()">
				    <c:forTokens items="10,25,50,100,200,500" delims="," var="current">
				  	  <option value="${current}" <c:if test="${current == model.orderList.pageSize}">selected</c:if>>${current} <fmt:message key="perPage" /></option>
				    </c:forTokens>
				  </select>
				  </td>
				  </tr>
				</table>					
				<!-- start button -->
			  	<div align="left" class="button"> 
				  <sec:authorize ifAnyGranted="ROLE_AEM,ROLE_ADMIN_${prop_site_id},ROLE_OUTGOING_PAYMENT_CREATE">
					<input type="submit" name="__make_payment" value="<fmt:message key="makePayment" />" onclick="return checkStatus();">			
				  </sec:authorize>
				</div>			  	
	            <!-- end button -->	
			  	
			  	</div>
	  	<!--end tab -->
	  	</div>
	<!-- end tabs -->        
	</div>
	
	  <!-- end table -->
	    </td><td class="boxmidr" ></td>
      </tr>
      <tr><td class="botl"></td><td class="botr"></td></tr>
	     
	   </table>	 
	<!-- end main box -->  
	</div>

</form>
</sec:authorize>  

<script type="text/javascript">
window.addEvent('domready', function(){
	var box1 = new multiBox('mbOrder', {descClassName: 'descClassName',showNumbers: false,overlay: new overlay()});
	// Create the accordian
	var myAccordion = new Fx.Accordion($$('div.arrow_drop'), $$('div.information'), {
		display: 1, alwaysHide: true,
    	onActive: function() {$('information').removeClass('displayNone');}
	});	
});

function toggleAll(el) {
	console.log("toggle all test");
	var ids = document.getElementsByName("__selected_id");	
  	if (ids.length == 1)
      document.list_form.__selected_id.checked = el.checked;
    else
      for (i = 0; i < ids.length; i++)
        document.list_form.__selected_id[i].checked = el.checked;	
}

function batchUpdate(order) {
	var ids = document.getElementsByName("__selected_id");	
  	if (ids.length == 1) {
      if (document.list_form.__selected_id.checked) {
          alert("update");
        document.getElementById('__startDate_0').value = document.getElementById('__dateOfPay').value;
        document.getElementById('__paymentMethod_0').value = document.getElementById('__paymentMethod').value;
        document.getElementById('__note_0').value = document.getElementById('__note').value;
    	return confirm('Batch Update for one?');
      }
    } else {
      for (i = 0; i < ids.length; i++) {
        if (document.list_form.__selected_id[i].checked) {
          document.getElementById('__startDate_' + i).value = document.getElementById('__dateOfPay').value;
          document.getElementById('__paymentMethod_' + i).value = document.getElementById('__paymentMethod').value;
          document.getElementById('__note_' + i).value = document.getElementById('__note').value;    	  
        }           
      }
      return confirm('Batch Update for all selected ones?');   
    }
    alert("Please select orders(s) to batch.");       
    return false;
}

</script>
  
  </tiles:putAttribute>
</tiles:insertDefinition>