<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="/WEB-INF/tlds/spring.tld" prefix="spring" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ taglib uri="http://www.springframework.org/security/tags" prefix="sec" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>

<tiles:insertDefinition name="admin.customers" flush="true">
  <tiles:putAttribute name="content" type="string">
<script type="text/javascript" >
<!--
window.addEvent('domready', function(){			
			var tabs1 = new SimpleTabs($('tab-block-1'), { selector: 'h4'});
		});
//-->	
</script>   
  
<form action="addPartners.jhtm" method="post">
<div id="mboxfull">	
	
	<table cellpadding="0" cellspacing="0" border="0" class="module" >
 
	<tr>
	    <td class="topl_g">
		  
		  <!-- breadcrumb -->
		  <p class="breadcrumb">
		  	<a href="../customers"><fmt:message key="customers"/></a> &gt;
		    <fmt:message key="partner"/> 
		  </p>
		  
		  <!-- Error Message -->
		  <c:if test="${!empty message}">
			<div class="message">
			  <fmt:message key="${message}">		    
			  </fmt:message>			  
			</div>
	  	  </c:if>
	        
	    </td><td class="topr_g" ></td>
	</tr>
	<tr><td class="boxmidlrg">
	
	
<!-- tabs -->
 <div id="tab-block-1">
	<h4 title="Add Language">Partner</h4>
	<div>
	<!-- start tab -->

        <c:set var="classIndex" value="0" />
        
	  	<div class="listdivi ln tabdivi"></div>
	  	<div class="listdivi"></div>
	  	
			<div class="list${classIndex % 2}"><c:set var="classIndex" value="${classIndex+1}" />
			<div class="listfl"><fmt:message key="active"/>:</div>
			<div class="listp">
				<!-- input field --> 
				<input type="checkbox" name="_active" id="__activeId" value="true" <c:if test="${active}">checked</c:if>>
				
			<!-- end input field -->   	
			</div>
			</div>
   		
		  	<div class="list${classIndex % 2}"><c:set var="classIndex" value="${classIndex+1}" />
			<div class="listfl"><fmt:message key="partnerName"/>:</div>
			<div class="listp">
				<!-- input field --> 
				<input type="text" name="__partnerName" id="__partnerNameId" value="${partnerName}"/>
				
			<!-- end input field -->   	
			</div>
			</div>
			
					   
    <!-- end tab -->
    </div>
 <!-- end tab -->
 </div>
 
	<!-- start button -->
	<div align="left" class="button"> 
			<c:choose>
			<c:when test="${update == 'true'}">	
			<input type="hidden" name="pid" value="${pid}"/>			
				<input type="submit" name="update" value="<fmt:message key="update" />">
			</c:when>
			<c:otherwise>
				<input type="submit" name="__addName" value="<fmt:message key="add" />">
			</c:otherwise>
			</c:choose>
	</div>
	<!-- end button -->			 
	
	  <!-- end table --> 
	</td><td class="boxmidr" ></td>	</tr>	
	</table>

</div>
</form>

</tiles:putAttribute>
</tiles:insertDefinition>  
    