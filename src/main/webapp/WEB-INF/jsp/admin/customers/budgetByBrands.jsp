<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="/WEB-INF/tlds/spring.tld" prefix="spring" %>

<c:if test="${gSiteConfig['gBUDGET_BRAND']}">
<tiles:insertDefinition name="admin.customers" flush="true">
  <tiles:putAttribute name="content" type="string">
  
<script language="JavaScript"> 
<!--
window.addEvent('domready', function(){			
			var tabs1 = new SimpleTabs($('tab-block-1'), { selector: 'h4'});
			var Tips1 = new Tips($$('.toolTipImg'));
		});
//--> 
</script> 
<form method="post" name="budgetByBrands">
<!-- main box -->
  <div id="mboxfull">
  
  <!-- start table -->
  <table cellpadding="0" cellspacing="0" border="0" class="module" >
  <tr>
    <td class="topl_g">
	  
	  <!-- breadcrumb -->
	  <p class="breadcrumb">
	     <a href="../customers"><fmt:message key="customer" /></a> &gt;
	     <a href="../customers/customer.jhtm?id=${customer.id}"><c:out value="${customer.username}" /></a> &gt;
	    <fmt:message key="budgetByBrands" /> 
	  </p>
	  
	  <!-- Error Message -->
  	  <c:if test="${!empty message}">
		  <div class="message"><spring:message code="${message}" /></div>
	  </c:if>

  </td><td class="topr_g" ></td></tr>
  <tr><td class="boxmidlrg" >

 <!-- tabs -->
 <div id="tab-block-1">
	<h4 title="<fmt:message key="budgetByBrands"/> Form"><fmt:message key="budgetByBrands"/></h4>
	<div>
	<!-- start tab -->
         <c:set var="classIndex" value="0" />
         
	  	<div class="listdivi ln tabdivi"></div>
	  	<div class="listdivi"></div>
	   
		  	<div class="list${classIndex % 2}"><c:set var="classIndex" value="${classIndex+1}" />
		  	<div class="listpp">
		  	<!-- input field -->
				<table class="form">
				  <tr>
				    <td>&nbsp;</td>
				    <td><fmt:message key="brand" /></td>
				    <td align="center"><fmt:message key="skuPrefix" /></td>
				    <td>&nbsp;</td>				    
				    <td width="20">&nbsp;</td>				    
				    <td align="center"><c:out value="${currentYear}"/><br/><fmt:message key="total" /></td>
				    <td><fmt:message key="yearlyBudget" /></td>				    
				    <td align="center"><fmt:message key="balance" /></td>
				  </tr>
				  <c:forEach items="${brands}" var="brand" varStatus="status">
				  <tr>
				    <td style="width:100px;text-align:right"><c:out value="${status.index + 1}"/> . </td>
				    <td><c:out value="${brand.name}"/></td>
				    <td align="center"><c:out value="${brand.skuPrefix}"/></td>
				    <td><c:if test="${brand.optionName != null}"><c:out value="${brand.optionName}"/>: <c:out value="${brand.valueName}"/></c:if></td>
				    <td>&nbsp;</td>				    
					<td align="right">
				      <c:if test="${brand.total != null}">
				        <fmt:message key="${siteConfig['CURRENCY'].value}" /><fmt:formatNumber value="${brand.total}" pattern="#,##0.00" />
				      </c:if>
				      <c:if test="${brand.total == null}">&nbsp;</c:if>
					</td>
				    <td align="center"><input style="text-align:right" type="text" name="__budget_${brand.id}" value="<c:out value="${brand.budget}"/>" maxlength="10" size="7"></td>
					<td align="right">
				      <fmt:message key="${siteConfig['CURRENCY'].value}" /><fmt:formatNumber value="${brand.balance}" pattern="#,##0.00" />
					</td>
				  </tr>
				  </c:forEach>
				</table>
	 		<!-- end input field -->	
	 		</div>
		  	</div>	
	<!-- end tab -->        	
	</div>
<!-- end tabs -->			
</div>
  
<!-- start button -->
<div align="left" class="button"> 
	<input type="submit" name="__update" value="<fmt:message key="update"/>">
</div>
<!-- end button -->

  <!-- end table -->
    </td><td class="boxmidr" ></td></tr>
  <tr><td class="botl"></td><td class="botr"></td></tr>
  </table>
  
<!-- end main box -->  
</div>	
</form>

  </tiles:putAttribute>    
</tiles:insertDefinition>
</c:if>

