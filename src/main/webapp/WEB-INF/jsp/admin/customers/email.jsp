<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="/WEB-INF/tlds/spring.tld" prefix="spring" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>

<tiles:insertDefinition name="admin.customers" flush="true">
  <tiles:putAttribute name="content" type="string">

<script language="JavaScript" type="text/JavaScript">
<!--
window.addEvent('domready', function(){			
	var tabs1 = new SimpleTabs($('tab-block-1'), { selector: 'h4'});
});
	 
function getSiteMessage(groupId) {
    var req = new Request.HTML({  
    url: '../config/show-ajax-groupSiteMessage.jhtm?groupId='+groupId,
        update: $('siteMessageSelect'),
        onSuccess: function(response){
        	document.getElementById('messageSelect').style.display="block";
        }       
        }).send();
}
//-->
</script>

<form:form commandName="form" onsubmit="return checkMessage()">  
<input type="hidden" id="send" value="true">
<!-- main box -->
  <div id="mboxfull">
  
  <!-- start table -->
  <table cellpadding="0" cellspacing="0" border="0" class="module" >
  <tr>
    <td class="topl_g">
	  
	  <!-- breadcrumb -->
	  <p class="breadcrumb">
	    <a href="../customers/"><fmt:message key="customers"/></a> &gt;
	    <fmt:message key="sendEmail"/>
	  </p>
	  
	  <!-- Error Message -->
	  <c:if test="${message != null}">
		<div class="message"><fmt:message key="${message}"/></div>
	  </c:if>
	  <spring:hasBindErrors name="form">
		<div class="error"><fmt:message key='form.hasErrors'/></div>
	  </spring:hasBindErrors>
  
  </td><td class="topr_g" ></td></tr>
  <tr><td class="boxmidlrg" >
 <!-- tabs -->
 <div id="tab-block-1">
	<h4 title="<fmt:message key='sendEmail'/>"><fmt:message key="sendEmail"/></h4>
	<div>
	<!-- start tab -->

        <c:set var="classIndex" value="0" />
        
	  	<div class="listdivi ln tabdivi"></div>
	  	<div class="listdivi"></div> 

		  	<div class="list${classIndex % 2}"><c:set var="classIndex" value="${classIndex+1}" />
		  	<div class="listfl"><fmt:message key="from"/>:</div>
		  	<div class="listp">
		  	<!-- input field -->
		  	    <form:input path="from" size="50" maxlength="150" htmlEscape="true"/>
	            <form:errors path="from" cssClass="error" delimiter=", "/>
		    <!-- end input field -->   	
		  	</div>
		  	</div>  

		  	<div class="list${classIndex % 2}"><c:set var="classIndex" value="${classIndex+1}" />
		  	<div class="listfl"><fmt:message key="to"/>:</div>
		  	<div class="listp">
		  	<!-- input field -->
	   			<a href="customer.jhtm?id=${param.id}"><c:out value="${form.to}"/></a>
	            <form:errors path="to" cssClass="error" delimiter=", "/>
		    <!-- end input field -->   	
		  	</div>
		  	</div>  
		  	
		  	<div class="list${classIndex % 2}"><c:set var="classIndex" value="${classIndex+1}" />
		  	<div class="listfl">Cc:</div>
		  	<div class="listp">
		  	<!-- input field -->
	   			<form:input path="cc" size="50" maxlength="150" htmlEscape="true"/>
	            <form:errors path="cc" cssClass="error" delimiter=", "/>
		    <!-- end input field -->   	
		  	</div>
		  	</div>  
		  	
		  	<div class="list${classIndex % 2}"><c:set var="classIndex" value="${classIndex+1}" />
		  	<div class="listfl">Bcc:</div>
		  	<div class="listp">
		  	<!-- input field -->
	   			<c:out value="${form.bcc}"/>
		    <!-- end input field -->   	
		  	</div>
		  	</div>  
		  	
		  	<div class="list${classIndex % 2}"><c:set var="classIndex" value="${classIndex+1}" />
		  	<div class="listfl"><div class="requiredField"><fmt:message key="subject"/>:</div></div>
		  	<div class="listp">
		  	<!-- input field -->
	   			<form:input id="subject" path="subject" size="50" maxlength="50" htmlEscape="true"/>
	  			  <c:forEach var="message" items="${messages}">
	    		  <input type="text" disabled style="display:none;" id="subject${message.messageId}" name="subject" value="<c:out value="${message.subject}"/>" size="50" maxlength="50">
	  			  </c:forEach>
	            <form:errors path="subject" cssClass="error"/>
		    <!-- end input field -->   	
		  	</div>
		  	</div>  
		  	
		  	<div class="list${classIndex % 2}"><c:set var="classIndex" value="${classIndex+1}" />
		  	<div class="listfl">Html:</div>
		  	<div class="listp">
		  	<!-- input field -->
	   			<form:checkbox path="html" />
	  			  <c:forEach var="message" items="${messages}">
	    		  <input type="hidden" id="htmlID${message.messageId}" value="<c:out value="${message.html}"/>" />
	  			  </c:forEach>
	            <form:errors path="html" cssClass="error"/>
		    <!-- end input field -->   	
		  	</div>
		  	</div> 
		  	<div class="list${classIndex % 2}"><c:set var="classIndex" value="${classIndex+1}" />
		  	<div class="listfl"><div class="requiredField"><fmt:message key="message"/>:</div></div>
		  	<div class="listp">
		  	<!-- input field -->
	   			<form:textarea id="message" path="message" rows="15" cols="60" htmlEscape="true"/>
	  			  <c:forEach var="message" items="${messages}">
	    			<textarea disabled style="display:none;" id="message${message.messageId}" name="message" rows="15" cols="60"><c:out value="${message.message}"/></textarea>
	  			  </c:forEach>
	           <form:errors path="message" cssClass="error"/>
		    <!-- end input field -->   	
		  	</div>
		  	</div> 
		  	<div class="list${classIndex % 2}"><c:set var="classIndex" value="${classIndex+1}" /> 
		  	<div class="listfl"><div class="requiredField"><fmt:message key="group"/>:</div></div>
		  	<div id="groupSelect">
			      <select id="groupSelected" onChange="getSiteMessage(this.value)" >
			        <option value="">choose a group</option>
			         <option value="-1"> All Messages</option>
				  <c:forEach var="group" items="${messageGroups}">
				    <option value="${group.id}"><c:out value="${group.name}"/></option>
				  </c:forEach>
				  </select>
		  	</div>
		  	</div>
		  	<div class="list${classIndex % 2}"><c:set var="classIndex" value="${classIndex+1}" />
			<div class="listfl" id="messageSelect" style="display:none;"><div class="requiredField"><fmt:message key="message"/> :</div></div>
			<div class="listp">
		  	<!-- input field -->
	         <div id="siteMessageSelect">
		  	 </div>		     		
		    <!-- end input field -->   	
		  	</div>	
		  </div>	
		  	
	  	
	<!-- end tab -->        
	</div> 
	

<!-- end tabs -->			
</div>

<!-- start button -->
<div align="left" class="button"> 
  <input type="submit" name="__send" value="<fmt:message key="send" />">
  <input type="submit" name="_cancel" value="<fmt:message key="cancel" />" onClick="document.getElementById('send').value='false'">
</div>
<!-- end button -->	     
  
  <!-- end table -->
    </td><td class="boxmidr" ></td></tr>
  <tr><td class="botl"></td><td class="botr"></td></tr>
  </table>
  
<!-- end main box -->  
</div>
</form:form>	 			  	

<script language="JavaScript"> 
<!--
   function chooseMessage(el) {
   	messageList = document.getElementsByName('message');
	for(var i=0; i<messageList.length; i++) {
		messageList[i].disabled=true;
		messageList[i].style.display="none";
	}
   	subjectList = document.getElementsByName('subject');
	for(var i=0; i<subjectList.length; i++) {
		subjectList[i].disabled=true;
		subjectList[i].style.display="none";
	}
   	document.getElementById('message'+el.value).disabled=false;
       document.getElementById('message'+el.value).style.display="block"; 
   	document.getElementById('subject'+el.value).disabled=false;
       document.getElementById('subject'+el.value).style.display="block"; 
    if (document.getElementById('htmlID'+el.value).value == 'true') {
    	$('html1').checked = true;
	} else {
	    $('html1').checked = false;
	}                   
}	
function trimString(str){
 var returnVal = "";
 for(var i = 0; i < str.length; i++){
   if(str.charAt(i) != ' '){
     returnVal += str.charAt(i)
   }
 }
 return returnVal;
}
function checkMessage() {
    if (document.getElementById('send').value == 'true') {
      var message = document.getElementById('message' + document.getElementById('messageSelected').value);
      var subject = document.getElementById('subject' + document.getElementById('messageSelected').value);
      if (trimString(subject.value) == "") {
        alert("Please put a subject");
        subject.focus();
    	return false;
      }
      if (trimString(message.value) == "") {
        alert("Please put a message");
        message.focus();
    	return false;
      }
    }
    return true;
}
//-->
</script>
   
  </tiles:putAttribute>    
</tiles:insertDefinition>