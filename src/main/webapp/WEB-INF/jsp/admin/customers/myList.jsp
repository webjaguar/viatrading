<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<tiles:insertDefinition name="admin.customers" flush="true">
  <tiles:putAttribute name="content" type="string">
<sec:authorize ifAnyGranted="ROLE_AEM,ROLE_ADMIN_${prop_site_id},ROLE_CUSTOMER_MYLIST_ADD,ROLE_CUSTOMER_MYLIST_DELETE,ROLE_CUSTOMER_MYLIST_VIEW">
	
<c:if test="${model.products.nrOfElements > 0}">
<script language="JavaScript">
<!--
function toggleAll(el) {
	var ids = document.getElementsByName("__selected_id");	
  	if (ids.length == 1)
      document.list_form.__selected_id.checked = el.checked;
    else
      for (i = 0; i < ids.length; i++)
        document.list_form.__selected_id[i].checked = el.checked;	
}  

function deleteSelected() {
	var ids = document.getElementsByName("__selected_id");	
  	if (ids.length == 1) {
      if (document.list_form.__selected_id.checked) {
    	return confirm('Delete permanently?');
      }
    } else {
      for (i = 0; i < ids.length; i++) {
        if (document.list_form.__selected_id[i].checked) {
    	  return confirm('Delete permanently?');
        }
      }
    }

    alert("Please select product(s) to delete.");       
    return false;
}
//-->
</script>
</c:if>	
<!-- main box -->
  <div id="mboxfull">
  
  <!-- start table -->
  <table cellpadding="0" cellspacing="0" border="0" class="module" >
  <tr>
    <td class="topl_g">
	  
	  <!-- breadcrumb -->
	  <p class="breadcrumb">
	    <a href="../customers">Customer</a> &gt;
	    myList 
	  </p>
	  
	  <!-- Error Message -->
	  <c:if test="${!empty model.message}">
		 <div class="message"><fmt:message key="${model.message}" /></div>
	  </c:if>
	  
	  </td><td class="topr_g" ></td></tr>
  <tr><td class="boxmidlrg" >

 <!-- tabs -->
 <div class="tab-wrapper">
	<h4 title="List of affiliates"></h4>
	<div>
	<!-- start tab -->
	<p>
	  	<div class="listdivi ln tabdivi"></div>
	  	<div class="listdivi"></div>
	    
		  	<div class="listlight">
		  	<!-- input field --> 
			<form action="myList.jhtm" method="post" name="list_form">
			<input type="hidden" name="id" value="${model.userId}">
			<div style="width:100%;">
			
			<table border="0" cellpadding="0" cellspacing="1" width="100%">
			  <tr>
			  <td class="pageShowing">
			  <c:if test="${model.products.nrOfElements > 0}">
			  <fmt:message key="showing">
				<fmt:param value="${model.products.firstElementOnPage + 1}"/>
				<fmt:param value="${model.products.lastElementOnPage + 1}"/>
				<fmt:param value="${model.products.nrOfElements}"/>
			  </fmt:message>
			  </c:if>
			  </td>
			  <td class="pageNavi">
			  Page 
			  <select name="page" id="page" onchange="submit()">
			  <c:forEach begin="1" end="${model.products.pageCount}" var="page">
			  	<option value="${page}" <c:if test="${page == (model.products.page+1)}">selected</c:if>>${page}</option>
			  </c:forEach>
			  </select>
			  of <c:out value="${model.products.pageCount}"/>
			  | 
			  <c:if test="${model.products.firstPage}"><span class="pageNaviDead"><fmt:message key="previous" /></span></c:if>
			  <c:if test="${not model.products.firstPage}"><a href="<c:url value="myList.jhtm"><c:param name="page" value="${model.products.page}"/><c:param name="id" value="${model.userId}"/></c:url>" class="pageNaviLink"><fmt:message key="previous" /></a></c:if>
			  | 
			  <c:if test="${model.products.lastPage}"><span class="pageNaviDead"><fmt:message key="next" /></span></c:if>
			  <c:if test="${not model.products.lastPage}"><a href="<c:url value="myList.jhtm"><c:param name="page" value="${model.products.page+2}"/><c:param name="id" value="${model.userId}"/></c:url>" class="pageNaviLink"><fmt:message key="next" /></a></c:if>
			  </td>
			  </tr>
			</table>
			
			<table border="0" cellpadding="0" cellspacing="0" width="100%" class="listings">
			  <tr class="listingsHdr2">
			<c:if test="${model.products.nrOfElements > 0}">
			    <td align="center"><input type="checkbox" onclick="toggleAll(this)"></td>
			</c:if>
			    <td class="indexCol">&nbsp;</td>
			    <td>
			    	<table cellspacing="0" cellpadding="1">
			    	  <tr>
			    	    <td rowspan="2" class="listingsHdr3"><fmt:message key="productName" /></td>
			    	  </tr>
			    	</table>
			    </td>
			    <td align="center">
			    	<table cellspacing="0" cellpadding="1">
			    	  <tr>
			    	    <td class="listingsHdr3"><fmt:message key="productId" /></td>
			       	  </tr>
			    	</table>
			    </td>
			    <td align="center">
			    	<table cellspacing="0" cellpadding="1">
			    	  <tr>
			    	    <td rowspan="2" class="listingsHdr3"><fmt:message key="productSku" /></td>
			       	  </tr>
			    	</table>
			    </td>
			    <td align="right">
			    	<table cellspacing="0" cellpadding="1">
			    	  <tr>
			    	    <td rowspan="2" class="listingsHdr3"><fmt:message key="productPrice" /></td>
			       	  </tr>
			    	</table>
			    </td>
			    <td align="center">
			    	<table cellspacing="0" cellpadding="1">
			    	  <tr>
			    	    <td rowspan="2" class="listingsHdr3"><fmt:message key="dateAdded" /></td>
			       	  </tr>
			    	</table>
			    </td>
			  </tr>
			<c:forEach items="${model.products.pageList}" var="product" varStatus="status">
			  <tr class="row${status.index % 2}" onmouseover="changeStyleClass(this,'rowOver')" onmouseout="changeStyleClass(this,'row${status.index % 2}')">
			<c:if test="${model.products.nrOfElements > 0}">
			    <td align="center"><input name="__selected_id" value="${product.id}" type="checkbox"></td>
			</c:if>
			    <td class="indexCol"><c:out value="${status.count + model.products.firstElementOnPage}"/>.</td>
			    <td class="nameCol" ><c:out value="${product.name}"/></td>
			    <td class="idCol" ><c:out value="${product.id}"/></td>
			    <td align="center" ><c:out value="${product.sku}"/></td>
			    <td align="right" ><fmt:formatNumber value="${product.price1}" pattern="#,##0.00" /></td>
			    <td align="center" ><fmt:formatDate type="date" timeStyle="default" value="${product.created}"/></td>
			  </tr>
			</c:forEach>
			<c:if test="${model.products.nrOfElements == 0}">
			  <tr class="emptyList"><td colspan="6">&nbsp;</td></tr>
			</c:if>
			</table>
				  
			<table border="0" cellpadding="0" cellspacing="1" width="100%">
			  <tr>  
			  <td class="pageSize">
			    <select name="size" onchange="document.getElementById('page').value=1;submit()">
			    <c:forTokens items="10,25,50,100,500" delims="," var="current">
			  	  <option value="${current}" <c:if test="${current == model.products.pageSize}">selected</c:if>>${current} <fmt:message key="perPage" /></option>
			    </c:forTokens>
			    </select>
			  </td>
			  </tr>
			</table>	  
		  	<!-- end input field -->  	  
		  	</div>
		  	
		  	<!-- start button --> 
		  	<div align="left" class="button">
			   <sec:authorize ifAnyGranted="ROLE_AEM,ROLE_ADMIN_${prop_site_id},ROLE_CUSTOMER_MYLIST_ADD">
			     <input type="submit" name="__add" value="Add to My List">
			   </sec:authorize>    
			   <c:if test="${model.products.nrOfElements > 0}">
			   <sec:authorize ifAnyGranted="ROLE_AEM,ROLE_ADMIN_${prop_site_id},ROLE_CUSTOMER_MYLIST_DELETE">	
			  	 <input type="submit" name="__delete" value="Delete From My List" onClick="return deleteSelected()">
			   </sec:authorize>	  
			  </c:if>
		  	</div>
		  	<!-- end button -->	
	  	
	<!-- end tab -->        
	</div>
<!-- end tabs -->			
</div>
  
  <!-- end table -->
   </td><td class="boxmidr" ></td></tr>
  <tr><td class="botl"></td><td class="botr"></td></tr>
  </table>
  
<!-- end main box -->  
</div>
</form>		  

</sec:authorize>
  </tiles:putAttribute>    
</tiles:insertDefinition>
