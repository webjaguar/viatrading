<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<c:if test="${gSiteConfig['gSUB_ACCOUNTS']}">
<tiles:insertDefinition name="admin.customers" flush="true">
  <tiles:putAttribute name="content" type="string">

<form action="setParent.jhtm?cid=${model.customer.id}" id="list" method="post">
<input type="hidden" id="sort" name="sort" value="${customerParentSearch.sort}" />
<!-- main box -->
  <div id="mboxfull">
  
  <!-- start table -->
  <table cellpadding="0" cellspacing="0" border="0" class="module" >
  <tr>
    <td class="topl_g">
	  
	  <!-- breadcrumb -->
	  <p class="breadcrumb">
	    <a href="../customers">Customers</a> &gt;
	    parent 
	  </p>
	  
	  <!-- Error -->
	  <div class="message2"><fmt:message key="setParentFor" />
	    <c:out value="${model.customer.username}"/>
	  </div>
	  <c:if test="${!empty model.message}">
		<div class="message"><fmt:message key="${model.message}" /></div>
	  </c:if>  
	  
  </td><td class="topr_g" ></td></tr>
  <tr><td class="boxmidlrg" >

 <!-- tabs -->
<div class="tab-wrapper">
	<h4 title="List of products"></h4>
	<div>
	<!-- start tab -->

	  	<div class="listdivi ln tabdivi"></div>
	  	<div class="listdivi"></div>
	   
		    
		  	<div class="listlight">
		  	<!-- input field -->
			<table border="0" cellpadding="0" cellspacing="1" width="100%">
			  <tr>
			  <td>
				<select name="parent"
				  onChange="document.getElementById('page').value=1;submit()">
					<option value=""><fmt:message key="all" /> <fmt:message key="accounts" /></option>
					<option value="-1" <c:if test="${-1 == customerParentSearch['parent']}">selected</c:if>>
						<fmt:message key="main" /> <fmt:message key="accounts" /></option>	
					<c:forEach items="${model.familyTree}" var="customer">
					<option value="${customer.id}"
					<c:if test="${customer.id == customerParentSearch['parent']}">selected</c:if>>
					<fmt:message key="subAccounts" /> <fmt:message key="of" /> <c:out value="${customer.username}" /> 
				  </option>
				</c:forEach>
				</select>
			  </td>
			  <td>
			  <c:if test="${model.count > 0}">
			  <td class="pageShowing">
			  <fmt:message key="showing">
				<fmt:param value="${customerParentSearch.offset + 1}"/>
				<fmt:param value="${model.pageEnd}"/>
				<fmt:param value="${model.count}"/>
			  </fmt:message>
			  </td>
			  </c:if>
			  <td class="pageNavi">
			  Page 
			  <c:choose>
			  <c:when test="${model.pageCount <= 100 and model.pageCount > 0}">
			  <select id="page" name="page" onchange="submit()">
			  <c:forEach begin="1" end="${model.pageCount}" var="page">
			  	<option value="${page}" <c:if test="${page == (customerParentSearch.page)}">selected</c:if>>${page}</option>
			  </c:forEach>
			  </select>
			  </c:when>
			  <c:otherwise>
			  <input type="text" id="page" name="page" value="${customerParentSearch.page}" size="5" class="textfield50" />
			  <input type="submit" value="go"/>
			  </c:otherwise>
			  </c:choose>
			  of <c:out value="${model.pageCount}"/>
			  | 
			  <c:if test="${customerParentSearch.page == 1}"><span class="pageNaviDead"><fmt:message key="previous" /></span></c:if>
			  <c:if test="${customerParentSearch.page != 1}"><a href="#" onClick="document.getElementById('page').value='${customerParentSearch.page-1}';document.getElementById('list').submit()" class="pageNaviLink"><fmt:message key="previous" /></a></c:if>
			  | 
			  <c:if test="${customerParentSearch.page == model.pageCount}"><span class="pageNaviDead"><fmt:message key="next" /></span></c:if>
			  <c:if test="${customerParentSearch.page != model.pageCount}"><a href="#" onClick="document.getElementById('page').value='${customerParentSearch.page+1}';document.getElementById('list').submit()" class="pageNaviLink"><fmt:message key="next" /></a></c:if>
			  </td>
			  </tr>
			</table>
			
			<table border="0" cellpadding="0" cellspacing="0" width="100%" class="listings">
			  <tr class="listingsHdr2">
			    <c:set var="cols" value="0"/>
			    <td width="15px">&nbsp;</td>
			    <td>
			    	<table cellspacing="0" cellpadding="1">
			    	  <tr>
			    	    <c:choose>
			    	      <c:when test="${customerParentSearch.sort == 'last_name desc, first_name'}">
			    	        <td class="listingsHdr3">
			    	        <a class="hdrLink" href="#" onClick="document.getElementById('sort').value='last_name, first_name';document.getElementById('list').submit()"><fmt:message key="lastName" /></a>
			    	        </td>
			    	        <td><img src="../graphics/up.gif" border="0"></td>
			    	      </c:when>
			    	      <c:when test="${customerParentSearch.sort == 'last_name, first_name'}">
			    	        <td class="listingsHdr3">
			    	        <a class="hdrLink" href="#" onClick="document.getElementById('sort').value='last_name desc, first_name';document.getElementById('list').submit()"><fmt:message key="lastName" /></a>
			    	        </td>
			    	        <td><img src="../graphics/down.gif" border="0"></td>
			    	      </c:when>
			    	      <c:otherwise>
			    	        <td colspan="2" class="listingsHdr3">
			    	        <a class="hdrLink" href="#" onClick="document.getElementById('sort').value='last_name, first_name';document.getElementById('list').submit()"><fmt:message key="lastName" /></a>
			    	        </td>
			    	      </c:otherwise>
			    	    </c:choose>  
			    	  </tr>
			    	 </table>
			    </td>
			    <td>
			    	<table cellspacing="0" cellpadding="1">
			    	  <tr>
			    	    <c:choose>
			    	      <c:when test="${customerParentSearch.sort == 'first_name desc, last_name'}">
			    	        <td class="listingsHdr3">
			    	        <a class="hdrLink" href="#" onClick="document.getElementById('sort').value='first_name, last_name';document.getElementById('list').submit()"><fmt:message key="firstName" /></a>
			    	        </td>
			    	        <td><img src="../graphics/up.gif" border="0"></td>
			    	      </c:when>
			    	      <c:when test="${customerParentSearch.sort == 'first_name, last_name'}">
			    	        <td class="listingsHdr3">
			    	        <a class="hdrLink" href="#" onClick="document.getElementById('sort').value='first_name desc, last_name';document.getElementById('list').submit()"><fmt:message key="firstName" /></a>
			    	        </td>
			    	        <td><img src="../graphics/down.gif" border="0"></td>
			    	      </c:when>
			    	      <c:otherwise>
			    	        <td colspan="2" class="listingsHdr3">
			    	        <a class="hdrLink" href="#" onClick="document.getElementById('sort').value='first_name, last_name';document.getElementById('list').submit()"><fmt:message key="firstName" /></a>
			    	        </td>
			    	      </c:otherwise>
			    	    </c:choose>  
			    	  </tr>
			    	  </table>
			    </td>
			    <td>
			    	<table cellspacing="0" cellpadding="1">
			    	  <tr>
			    	    <c:choose>
			    	      <c:when test="${customerParentSearch.sort == 'username desc'}">
			    	        <td class="listingsHdr3">
			    	        <a class="hdrLink" href="#" onClick="document.getElementById('sort').value='username';document.getElementById('list').submit()"><fmt:message key="emailAddress" /></a>
			    	        </td>
			    	        <td><img src="../graphics/up.gif" border="0"></td>
			    	      </c:when>
			    	      <c:when test="${customerParentSearch.sort == 'username'}">
			    	        <td class="listingsHdr3">
			    	        <a class="hdrLink" href="#" onClick="document.getElementById('sort').value='username desc';document.getElementById('list').submit()"><fmt:message key="emailAddress" /></a>
			    	        </td>
			    	        <td><img src="../graphics/down.gif" border="0"></td>
			    	      </c:when>
			    	      <c:otherwise>
			    	        <td colspan="2" class="listingsHdr3">
			    	        <a class="hdrLink" href="#" onClick="document.getElementById('sort').value='username desc';document.getElementById('list').submit()"><fmt:message key="emailAddress" /></a>
			    	        </td>
			    	      </c:otherwise>
			    	    </c:choose>  
			    	  </tr>
			    	  </table>
			    </td>
			    <td>
			    	<table cellspacing="0" cellpadding="1">
			    	  <tr>
			    	    <c:choose>
			    	      <c:when test="${customerParentSearch.sort == 'company desc'}">
			    	        <td class="listingsHdr3">
			    	        <a class="hdrLink" href="#" onClick="document.getElementById('sort').value='company';document.getElementById('list').submit()"><fmt:message key="company" /></a>
			    	        </td>
			    	        <td><img src="../graphics/up.gif" border="0"></td>
			    	      </c:when>
			    	      <c:when test="${customerParentSearch.sort == 'company'}">
			    	        <td class="listingsHdr3">
			    	        <a class="hdrLink" href="#" onClick="document.getElementById('sort').value='company desc';document.getElementById('list').submit()"><fmt:message key="company" /></a>
			    	        </td>
			    	        <td><img src="../graphics/down.gif" border="0"></td>
			    	      </c:when>
			    	      <c:otherwise>
			    	        <td colspan="2" class="listingsHdr3">
			    	        <a class="hdrLink" href="#" onClick="document.getElementById('sort').value='company desc';document.getElementById('list').submit()"><fmt:message key="company" /></a>
			    	        </td>
			    	      </c:otherwise>
			    	    </c:choose>  
			    	  </tr>
			    	  </table>
			    </td>
			    <td>
			    	<table cellspacing="0" cellpadding="1">
			    	  <tr>
			    	    <td rowspan="2" class="listingsHdr3"><fmt:message key="accountNumber" /></td>
			          </tr>
			    	</table>
			    </td>
			    <td align="center">
			    	<table cellspacing="0" cellpadding="1">
			    	  <tr>
			    	    <c:choose>
			    	      <c:when test="${customerParentSearch.sort == 'created desc'}">
			    	        <td class="listingsHdr3">
			    	        <a class="hdrLink" href="#" onClick="document.getElementById('sort').value='created';document.getElementById('list').submit()"><fmt:message key="accountCreated" /></a>
			    	        </td>
			    	        <td><img src="../graphics/up.gif" border="0"></td>
			    	      </c:when>
			    	      <c:when test="${customerParentSearch.sort == 'created'}">
			    	        <td class="listingsHdr3">
			    	        <a class="hdrLink" href="#" onClick="document.getElementById('sort').value='created desc';document.getElementById('list').submit()"><fmt:message key="accountCreated" /></a>
			    	        </td>
			    	        <td><img src="../graphics/down.gif" border="0"></td>
			    	      </c:when>
			    	      <c:otherwise>
			    	        <td colspan="2" class="listingsHdr3">
			    	        <a class="hdrLink" href="#" onClick="document.getElementById('sort').value='created desc';document.getElementById('list').submit()"><fmt:message key="accountCreated" /></a>
			    	        </td>
			    	      </c:otherwise>
			    	    </c:choose>  
			    	  </tr>
			    	 </table>
			    </td>
			    <td>&nbsp;</td>
			  </tr>
			<c:forEach items="${model.customers}" var="customer" varStatus="status">
			  <tr class="row${status.index % 2} <c:if test="${model.customer.id == customer.id}">rowOff</c:if>">
			    <td width="15px"><input type="radio" name="pid" value="${customer.id}" <c:if test="${model.customer.id == customer.id}">disabled</c:if> <c:if test="${model.customer.parent == customer.id}">checked<c:set var="found" value="true"/></c:if>></td>
			    <td class="nameCol"><c:out value="${customer.address.lastName}"/></td>
			    <td class="nameCol"><c:out value="${customer.address.firstName}"/></td>
			    <td><c:out value="${customer.username}"/></td>
			    <td class="nameCol"><c:out value="${customer.address.company}"/></td>
			    <td><c:out value="${customer.accountNumber}"/></td>
			    <td align="center"><fmt:formatDate type="date" timeStyle="default" value="${customer.created}"/></td>
				<td><c:choose>
				 <c:when test="${customer.subCount != 0 and model.customer.id != customer.id}">
				 <a href="<c:url value="setParent.jhtm"><c:param name="cid" value="${param.cid}"/><c:param name="parent" value="${customer.id}"/></c:url>"
												class="countLink"><fmt:message key="subAccounts" /> (<b><c:out
													value="${customer.subCount}" /></b>)</a></c:when>
				 <c:otherwise><div class="countLinkOff"><fmt:message key="subAccounts" /> (<b><c:out
													value="${customer.subCount}" /></b>)</div></c:otherwise>	
				</c:choose></td>
			  </tr>
			</c:forEach>
			<c:if test="${model.count == 0}">
			  <tr class="emptyList"><td colspan="9">&nbsp;</td></tr>
			</c:if>
			</table>
			
			<table border="0" cellpadding="0" cellspacing="0" width="100%">
			  <tr>
			  <td>
				<input type="radio" name="pid" value="0" <c:if test="${model.customer.parent == null}">checked</c:if>> <fmt:message key="none" />
				<c:if test="${found != true and model.customer.parent != null}">
				<input type="radio" name="pid" value="${model.parent.id}" checked> <c:out value="${model.parent.username}"/>
				</c:if>
			  </td>
			  <td class="pageSize">
			  <select name="size" onchange="document.getElementById('page').value=1;submit()">
			  <c:forTokens items="10,25,50" delims="," var="current">
			  	  <option value="${current}" <c:if test="${current == customerParentSearch.pageSize}">selected</c:if>>${current} <fmt:message key="perPage" /></option>
			  </c:forTokens>
			  </select>
			  </td>
			  </tr>
			</table>
			<!-- end input field -->  	  
		  	
		  	<!-- start button -->
            <div align="left" class="button">
			    <input type="submit" name="__update" value="<fmt:message key="update" />">
    			<input type="submit" name="__cancel" value="<fmt:message key="cancel" />">
	  	    </div>
			<!-- end button -->	
	        	  	
	<!-- end tab -->        
	</div>
<!-- end tabs -->			
</div>
  
  <!-- end table -->
   </td><td class="boxmidr" ></td></tr>
  <tr><td class="botl"></td><td class="botr"></td></tr>
  </table>
  
<!-- end main box -->  
</div>
</form>   

  </tiles:putAttribute>    
</tiles:insertDefinition>
</c:if>
