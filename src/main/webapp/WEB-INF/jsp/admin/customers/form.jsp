<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="/WEB-INF/tlds/spring.tld" prefix="spring" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ taglib uri="http://www.springframework.org/security/tags" prefix="sec" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ taglib uri="/WEB-INF/tlds/custom.tld" prefix="wj" %>

<tiles:insertDefinition name="admin.customers" flush="true">
  <tiles:putAttribute name="content" type="string">
  
<sec:authorize ifAnyGranted="ROLE_AEM,ROLE_ADMIN_${prop_site_id},ROLE_CUSTOMER_EDIT,ROLE_CUSTOMER_CREATE,ROLE_CUSTOMER_DELETE">
<script type="text/javascript">
	window.addEvent('domready', function(){
		var box3 = new multiBox('mbCustomer', {waitDuration: 5,overlay: new overlay()});
	});
	function changeHref(id,salesRepId) {
		$('emailSalesRep').set('href', 'email.jhtm?id=' + id + '&salesrepid=' + salesRepId);
	}
</script> 

<style>

.thumbbox {
    background-color: #f3f3ec;
    float: left;
    margin-left: 15px !important;
    overflow: hidden;
}

</style>
 
<script type="text/javascript" src="<c:url value="/FCKeditor/fckeditor.js"/>"></script>
<script type="text/javascript"> 
window.addEvent('domready', function(){			
			var tabs1 = new SimpleTabs($('tab-block-1'), { selector: 'h4'});
			var Tips1 = new Tips($$('.toolTipImg'));
		});
function loadEditor(el) {
  var oFCKeditor = new FCKeditor(el);
  oFCKeditor.BasePath = "<c:url value="/FCKeditor/"/>";
  oFCKeditor.Config["CustomConfigurationsPath"] = '<c:url value="/admin/javascript/fckconfig.js"/>';
  oFCKeditor.ToolbarSet = 'Html';
  oFCKeditor.Width = 650;
  oFCKeditor.Height = 400;
  oFCKeditor.ReplaceTextarea();
  document.getElementById(el + '_link').style.display="none";
}
<c:if test="${gSiteConfig['gAFFILIATE'] > 0}" >
function toggleAffiliate(id) 
{
	if (document.getElementById(id).checked) 
	{
	  document.getElementById('promoCodeID').style.display="block";
	  document.getElementById('commissionID').style.display="block";
	  document.getElementById('categoryID').style.display="block";
	  document.getElementById('domainNameID').style.display="block";
	  document.getElementById('sourceCodeID').style.display="block";
	} else 
	{
	  document.getElementById('promoCodeID').style.display="none";
	  document.getElementById('commissionID').style.display="none";
	  document.getElementById('categoryID').style.display="none";
	  document.getElementById('domainNameID').style.display="none";
	  document.getElementById('sourceCodeID').style.display="none";
	}
}
</c:if>
function automateCityState(zipCode) {
	var request = new Request.JSON({
		url: "${_contextpath}/jsonZipCode.jhtm?zipCode="+zipCode,
		onRequest: function() { 
		},
		onComplete: function(jsonObj) {
			document.getElementById('customer.address.city').value = jsonObj.city;
			document.getElementById('state').value = jsonObj.stateAbbv;
			$$('.highlight').each(function(el) {
				var end = el.getStyle('background-color');
				end = (end == 'transparent') ? '#fff' : end;
				var myFx = new Fx.Tween(el, {duration: 500, wait: false});
				myFx.start('background-color', '#f00', end);
			});
		}
	}).send();
}
</script>
<script type="text/javascript">

var zChar = new Array(' ', '(', ')', '-', '.');
var maxphonelength = 13;
var phonevalue1;
var phonevalue2;
var cursorposition;

function ParseForNumber1(object){
phonevalue1 = ParseChar(object.value, zChar);
}
function ParseForNumber2(object){
phonevalue2 = ParseChar(object.value, zChar);
}

function backspacerUP(object,e) { 
if(e){ 
e = e 
} else {
e = window.event 
} 
if(e.which){ 
var keycode = e.which 
} else {
var keycode = e.keyCode 
}

ParseForNumber1(object)

if(keycode >= 48){
ValidatePhone(object)
}
}

function backspacerDOWN(object,e) { 
if(e){ 
e = e 
} else {
e = window.event 
} 
if(e.which){ 
var keycode = e.which 
} else {
var keycode = e.keyCode 
}
ParseForNumber2(object)
} 

function GetCursorPosition(){

var t1 = phonevalue1;
var t2 = phonevalue2;
var bool = false
for (i=0; i<t1.length; i++)
{
if (t1.substring(i,1) != t2.substring(i,1)) {
if(!bool) {
cursorposition=i
bool=true
}
}
}
}

function ValidatePhone(object){

var p = phonevalue1

p = p.replace(/[^\d]*/gi,"")

if (p.length < 3) {
object.value=p
} else if(p.length==3){
pp=p;
d4=p.indexOf('(')
d5=p.indexOf(')')
if(d4==-1){
pp="("+pp;
}
if(d5==-1){
pp=pp+")";
}
object.value = pp;
} else if(p.length>3 && p.length < 7){
p ="(" + p; 
l30=p.length;
p30=p.substring(0,4);
p30=p30+")"

p31=p.substring(4,l30);
pp=p30+p31;

object.value = pp; 

} else if(p.length >= 7){
p ="(" + p; 
l30=p.length;
p30=p.substring(0,4);
p30=p30+")"

p31=p.substring(4,l30);
pp=p30+p31;

l40 = pp.length;
p40 = pp.substring(0,8);
p40 = p40 + "-"

p41 = pp.substring(8,l40);
ppp = p40 + p41;

object.value = ppp.substring(0, maxphonelength);
}

GetCursorPosition()

if(cursorposition >= 0){
if (cursorposition == 0) {
cursorposition = 2
} else if (cursorposition <= 2) {
cursorposition = cursorposition + 1
} else if (cursorposition <= 5) {
cursorposition = cursorposition + 2
} else if (cursorposition == 6) {
cursorposition = cursorposition + 2
} else if (cursorposition == 7) {
cursorposition = cursorposition + 4
e1=object.value.indexOf(')')
e2=object.value.indexOf('-')
if (e1>-1 && e2>-1){
if (e2-e1 == 4) {
cursorposition = cursorposition - 1
}
}
} else if (cursorposition < 11) {
cursorposition = cursorposition + 3
} else if (cursorposition == 11) {
cursorposition = cursorposition + 1
} else if (cursorposition >= 12) {
cursorposition = cursorposition
}

var txtRange = object.createTextRange();
txtRange.moveStart( "character", cursorposition);
txtRange.moveEnd( "character", cursorposition - object.value.length);
txtRange.select();
}

}

function ParseChar(sStr, sChar)
{
if (sChar.length == null) 
{
zChar = new Array(sChar);
}
else zChar = sChar;

for (i=0; i<zChar.length; i++)
{
sNewStr = "";

var iStart = 0;
var iEnd = sStr.indexOf(sChar[i]);

while (iEnd != -1)
{
sNewStr += sStr.substring(iStart, iEnd);
iStart = iEnd + 1;
iEnd = sStr.indexOf(sChar[i], iStart);
}
sNewStr += sStr.substring(sStr.lastIndexOf(sChar[i]) + 1, sStr.length);

sStr = sNewStr;
}

return sNewStr;
}
</script>
<form:form commandName="customerForm" method="post" enctype="multipart/form-data">  
<!-- main box -->
  <div id="mboxfull">
  
  <!-- start table -->
  <table cellpadding="0" cellspacing="0" border="0" class="module" >
  <tr>
    <td class="topl_g">
    
    
	  
	  <!-- breadcrumb -->
	  <p class="breadcrumb">
	    <a href="../customers">Customers</a> &gt;
	    <c:if test="${!customerForm.newCustomer}">Edit Customer &gt;</c:if><c:if test="${customerForm.newCustomer}">Add Customer</c:if>  
	    ${customerForm.customer.username}
	  </p>
	  
	  <br/>
	  <br/>
	 
	  <div class="thumbbox">
		<p class="thumb">
		  <img src="/assets/Image/Product/Customer_${customerForm.customer.id}.jpg" border="0" width="200" height="130" margin-left="100px">
		</p>
		<p class="uname">
		<c:out value="${image.imageUrl}" />
		</p>
	  </div>
	  
	  
	  <!-- Error Message -->
  	  <c:if test="${!empty message}">
		  <div class="message"><spring:message code="${message}" /></div>
	  </c:if>
	  <spring:hasBindErrors name="customerForm">
       <span class="error">Please fix all errors!</span>
      </spring:hasBindErrors>

	  <div class="info">
	  <c:choose>
		  <c:when test="${gSiteConfig['gSEARCH_ENGINE_PROSPECT'] and !empty searchEngineProspect}">
		  <div align="left" class="left">
		  <table class="form"><tr>
		  <td>&nbsp;</td>
		  <td align="left" style="width:250px;">
		  	<b><fmt:message key="customerReferal" />:</b>
			  <table>
			  <tr>
			    <td><fmt:message key="referrer"/></td>
			    <td>:&nbsp;&nbsp;</td>
			    <td><c:out value="${searchEngineProspect.referrer}" />
			  </td>
			  <tr>
			    <td><fmt:message key="keyPhrase"/></td>
			    <td>:&nbsp;&nbsp;</td>
			    <td><c:out value="${searchEngineProspect.queryString}" />
			  </td>
			  </tr>
			  </table>
		  </td>
		  </tr></table>
		  </div>
		  </c:when>
		  <c:otherwise>
		  <div align="left">
		  <table class="form"><tr><td style="width:20%;">&nbsp;</td></tr></table>
		  </div>
		  </c:otherwise>
	  </c:choose>

	  <c:if test="${!customerForm.newCustomer}">
		<div align="right" class="right">
		<table class="form">
		  <c:if test="${customerForm.customer.host != null}">
		  <tr>
		    <td align="right"><fmt:message key="multiStore" /> : </td>
		    <td><c:out value="${customerForm.customer.host}"/></td>
		  </tr>
		  </c:if>
		  <tr>
		    <td align="right"><fmt:message key="accountCreated" /> : </td>
		    <td><fmt:formatDate type="both" timeStyle="full" value="${customerForm.customer.created}"/></td>
		  </tr>
		  <tr>
		    <td align="right"><fmt:message key="lastModified" /> : </td>
		    <td><fmt:formatDate type="both" timeStyle="full" value="${customerForm.customer.lastModified}"/></td>
		  </tr>
		  <tr>
		    <td align="right"><fmt:message key="lastLogin" /> : </td>
		    <td><fmt:formatDate type="both" timeStyle="full" value="${customerForm.customer.lastLogin}"/></td>
		  </tr>
		  <tr>
		    <td align="right"><fmt:message key="numOfLoginsFull" /> : </td>
		    <td><c:out value="${customerForm.customer.numOfLogins}"/></td>
		  </tr>
		  <tr>
		    <td align="right"><fmt:message key="quickView" /> : </td>
		    <td><a href="customerQuickView.jhtm?cid=${customerForm.customer.id}" rel="width:900,height:400" id="mb${customerForm.customer.id}" class="mbCustomer" title="<fmt:message key="customer" />ID : ${customerForm.customer.id}"><img src="../graphics/magnifier.gif" border="0"></a></td>
		  </tr>
		</table>
		</div>
	  </c:if>
	  </div>
	  
	  </td><td class="topr_g" ></td></tr>
  <tr><td class="boxmidlrg" >

 <!-- tabs -->
 <div id="tab-block-1">
	<h4 title="<fmt:message key="emailAddressAndPassword"/>"><fmt:message key="emailAddressAndPassword"/></h4>
	<div>
	<!-- start tab -->
         <c:set var="classIndex" value="0" />
         
	  	<div class="listdivi ln tabdivi"></div>
	  	<div class="listdivi"></div>
	  	
	  	<div class="list${classIndex % 2}"><c:set var="classIndex" value="${classIndex+1}" />
	  	<div class="listfl"><div class="requiredField"><fmt:message key='emailAddress' />:</div></div>
	  	<div class="listp">
	  	<!-- input field -->
			<form:input path="customer.username" maxlength="80" size="40" htmlEscape="true"/>
			<c:if test="${not customerForm.newCustomer}"><a href="email.jhtm?id=${customerForm.customer.id}"><fmt:message key="sendEmail" /></a></c:if>
			<form:errors path="customer.username" cssClass="error" delimiter=", "/>
	    <!-- end input field -->   	
	  	</div>
	  	</div>  	
		<div class="list${classIndex % 2}"><c:set var="classIndex" value="${classIndex+1}" />
	  	<div class="listfl"><div class="requiredField"><fmt:message key='newPassword' />:</div></div>
	  	<div class="listp">
	  	<!-- input field -->
			<form:password path="customer.password" value="${customerForm.customer.password}"/>
			<form:errors path="customer.password" cssClass="error" />
	    <!-- end input field -->   	
	  	</div>
	  	</div>  	
		<div class="list${classIndex % 2}"><c:set var="classIndex" value="${classIndex+1}" />
	  	<div class="listfl"><div class="requiredField"><fmt:message key='confirmPassword' />:</div></div>
	  	<div class="listp">
	  	<!-- input field -->
			<form:password path="confirmPassword" value="${customerForm.confirmPassword}"/>
			<form:errors path="confirmPassword" cssClass="error" />
	    <!-- end input field -->   	
	  	</div>
	  	</div>  	

        <c:if test="${siteConfig['CUSTOMER_PASSWORD_RESET'].value == 'true'}">
		<div class="list${classIndex % 2}"><c:set var="classIndex" value="${classIndex+1}" />
	  	<div class="listfl"><fmt:message key='passwordValidity' />:</div>
	  	<div class="listp">
	  	<!-- input field -->
			<form:select path="customer.passwordValidity">
			  <form:option value="-1" label="Never"></form:option>
			  <form:option value="1" label="1 Day"></form:option>
			  <form:option value="15" label="15 Days"></form:option>
			  <form:option value="30" label="30 Days"></form:option>
			  <form:option value="45" label="45 Days"></form:option>
			  <form:option value="90" label="90 Days"></form:option>
			  <form:option value="180" label="180 Days"></form:option>
			  <form:option value="365" label="365 Days"></form:option>
			</form:select>
	    <!-- end input field -->   	
	  	</div>
	  	</div> 
	  	</c:if> 
	  		  	
	  	<div class="list${classIndex % 2}"><c:set var="classIndex" value="${classIndex+1}" />
	  	<div class="listfl"><div class="helpImg"><img class="toolTipImg" title="Note::Customer Rating 1" src="../graphics/question.gif" /></div> <fmt:message key="rating" /> 1:</div>
	  	<div class="listp">
	  	<!-- input field -->
	  	<form:select path="customer.rating1">
	  		<form:option value=""></form:option>
	  		<c:forTokens items="${siteConfig['CUSTOMER_RATING_1'].value}" delims="," var="type" varStatus="status">
				<form:option value ="${type}"><c:out value ="${type}" /></form:option>
		  	</c:forTokens>
		</form:select>
	    <!-- end input field -->   	
	  	</div>
	  	</div>	  	
	  	
	  	<div class="list${classIndex % 2}"><c:set var="classIndex" value="${classIndex+1}" />
	  	<div class="listfl"><div class="helpImg"><img class="toolTipImg" title="Note::Customer Rating 2." src="../graphics/question.gif" /></div> <fmt:message key="rating" /> 2:</div>
	  	<div class="listp">
	  	<!-- input field -->
		<form:select path="customer.rating2">
	  		<form:option value=""></form:option>
	  		<c:forTokens items="${siteConfig['CUSTOMER_RATING_2'].value}" delims="," var="type" varStatus="status">
				<form:option value ="${type}"><c:out value ="${type}" /></form:option>
		  	</c:forTokens>
		</form:select>
	    <!-- end input field -->   	
	  	</div>
	  	</div>	

        <c:if test="${gSiteConfig['gREGISTRATION_TOUCH_SCREEN']}">
		<div class="list${classIndex % 2}"><c:set var="classIndex" value="${classIndex+1}" />
	  	<div class="listfl"><fmt:message key='idCardCount' />:</div>
	  	<div class="listp">
	  	<!-- input field -->
			<span class="static"><c:out value="${customerForm.customer.cardIdCount}" /></span>
	    <!-- end input field -->   	
	  	</div>
	  	</div>  
	  	</c:if>	
		<sec:authorize ifAnyGranted="ROLE_AEM,ROLE_ADMIN_${prop_site_id},ROLE_CUSTOMER_CHECKBOX_EDIT">
		<div class="list${classIndex % 2}"><c:set var="classIndex" value="${classIndex+1}" />
	  	<div class="listfl"><fmt:message key="suspended" />:</div>
	  	<div class="listp">
	  	<!-- input field -->
			<form:checkbox path="customer.suspended"/>&nbsp;<fmt:message key="account"/>&nbsp;&nbsp;<form:checkbox path="customer.havingWalkIn"/>&nbsp;Having Walk-In Pallets on Hold Without Payment
	    	<br/>
	    	<form:checkbox path="customer.suspendedEvent"/>&nbsp;<fmt:message key="event"/>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<form:checkbox path="customer.payingWalkIn"/>&nbsp;Paying for Walk-In Pallets without same day Pick-Up
	    	<br/>
	    	<form:checkbox path="customer.holdLoads"/>&nbsp;Holding Loads
            <br>
            <span><a href="/dv/orderreportnewdb/suspendedcustomers.php">suspended customers</a></span>
	    <!-- end input field -->   	
	  	</div>
	  	</div>
	  	</sec:authorize>
	  	 <sec:authorize ifNotGranted="ROLE_CUSTOMER_CHECKBOX_EDIT">
	  	 	  	 <sec:authorize ifNotGranted="ROLE_AEM">
	  	 <sec:authorize ifNotGranted="ROLE_ADMIN_${prop_site_id}">
		<div class="list${classIndex % 2}"><c:set var="classIndex" value="${classIndex+1}" />
	  	<div class="listfl"><fmt:message key="suspended" />:</div>
	  	<div class="listp">
	  	<!-- input field -->
			<form:checkbox path="customer.suspended" disabled = "true"/>&nbsp;<fmt:message key="account"/>&nbsp;&nbsp;<form:checkbox path="customer.havingWalkIn" disabled = "true"/>&nbsp;Having Walk-In Pallets on Hold Without Payment
	    	<br/>
	    	<form:checkbox path="customer.suspendedEvent" disabled = "true"/>&nbsp;<fmt:message key="event"/>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<form:checkbox path="customer.payingWalkIn" disabled = "true"/>&nbsp;Paying for Walk-In Pallets without same day Pick-Up
	    	<br/>
	    	<form:checkbox path="customer.holdLoads" disabled = "true"/>&nbsp;Holding Loads
	    <!-- end input field -->   	
	  	</div>
	  	</div>
	  	</sec:authorize>
	  		</sec:authorize>
	  	</sec:authorize>
	  	<div class="list${classIndex % 2}"><c:set var="classIndex" value="${classIndex+1}" />
	  	<div class="listfl"><div class="helpImg"><img class="toolTipImg" title="Note::Extra Email. Customer will notify by this extra email(s). Seperate by comma." src="../graphics/question.gif" /></div><fmt:message key='extraEmailAddress' />:</div>
	  	<div class="listp">
	  	<!-- input field -->
			<form:input path="customer.extraEmail" maxlength="80" size="40" htmlEscape="true"/>
			<form:errors path="customer.extraEmail" cssClass="error" delimiter=", "/>
	    <!-- end input field -->   	
	  	</div>
	  	</div>
	
		
		<div class="list${classIndex % 2}"><c:set var="classIndex" value="${classIndex+1}" />
		<div class="listfl"><div class="helpImg"><img class="toolTipImg" title="Note::This customer will not be shown on dialing export." src="../graphics/question.gif" /></div><fmt:message key="doNotCall" />:</div>
		<div class="listp">
		<!-- input field -->
            <form:checkbox path="customer.doNotCall" />
        <!-- end input field -->   	
		</div>
		</div>
		
		<c:if test="${gSiteConfig['gBUDGET']}">
        <div class="list${classIndex % 2}"><c:set var="classIndex" value="${classIndex+1}" />
		<div class="listfl"><div class="helpImg"><img class="toolTipImg" title="Note::'Empty': Get value form Site Congifuration. <br/> '-1': No Budget Module. <br/> '0': Only need approvals from managers. <br/>'1-100': Apply this % of customer's credit." src="../graphics/question.gif" /></div><fmt:message key="credit" /> <fmt:message key="allowed" />:</div>
		<div class="listp">
		<!-- input field -->
            <form:input path="customer.creditAllowed" maxlength="80" size="40" htmlEscape="true"/>%
        <!-- end input field -->   	
		</div>
		</div>
		<c:if test="${siteConfig['BUDGET_ADD_PARTNER'].value == 'true'}">
		<div class="list${classIndex % 2}"><c:set var="classIndex" value="${classIndex+1}" />
		<div class="listfl"><div class="helpImg"><img class="toolTipImg" title="Note::'Empty': Get value form Site Congifuration. <br/> '-1': No Budget Module. <br/> '0': Only need approvals from managers. <br/>'1-100': Apply this % of customer's credit." src="../graphics/question.gif" /></div>Budget Plan:</div>
		<div class="listp">
		<!-- input field -->
		 	<form:select id="budgetPlan" path="customer.budgetPlan">
		 	 <form:option value="" label="Please Select"/>
             <form:option value="Plan-A" label="Plan-A"/>
             <form:option value="Plan-B" label="Plan-B"/>
            </form:select>
        <!-- end input field -->   	
		</div>
		</div>
		</c:if>
		</c:if>
		<c:if test="${gSiteConfig['gPAYMENTS']}">
        <div class="list${classIndex % 2}"><c:set var="classIndex" value="${classIndex+1}" />
		<div class="listfl"><div class="helpImg"><img class="toolTipImg" title="Note::Orders can not be processed after this limit is reached." src="../graphics/question.gif" /></div><fmt:message key="payment" /> <fmt:message key="alert" />:</div>
		<div class="listp">
		<!-- input field -->
            <form:input path="customer.paymentAlert" maxlength="80" size="40" htmlEscape="true"/>
        <!-- end input field -->   	
		</div>
		</div>
		</c:if>
		<c:if test="${siteConfig['CUSTOMER_IP_ADDRESS'].value == 'true'}">
		<div class="list${classIndex % 2}"><c:set var="classIndex" value="${classIndex+1}" />
	  	<div class="listfl"><div class="helpImg"><img class="toolTipImg" title="IP Address:: This user can connect only through these IP Address(es). Separate multiple IP Addresses by comma or enter. Leave empty for no limitation." src="../graphics/question.gif" /></div><fmt:message key="ipAddress" />:</div>
	  	<div class="listp">
	  	<!-- input field -->
			<form:textarea path="customer.ipAddress"/>
	        <form:errors path="customer.ipAddress" cssClass="error" />
	    <!-- end input field -->   	
	  	</div>
	  	</div>
	  	</c:if> 

		<c:if test="${gSiteConfig['gSUB_ACCOUNTS'] && hasSubAccounts}">
		<div class="list${classIndex % 2}"><c:set var="classIndex" value="${classIndex+1}" />
	  	<div class="listfl"><fmt:message key="hide" /> <fmt:message key="subAccounts" />:</div>
	  	<div class="listp">
	  	<!-- input field -->
	    	<form:checkbox path="customer.hideSubAccts"/>
	    <!-- end input field -->   	
	  	</div>
	  	</div>
	  	</c:if> 
 		<div class="list${classIndex % 2}"><c:set var="classIndex" value="${classIndex+1}" />
		<div class="listfl"><div class="helpImg"><img class="toolTipImg" title="Note::Check it, customer will go to fulfill new information before place an order." src="../graphics/question.gif" /></div><fmt:message key="updateNewInformation" />:</div>
		<div class="listp">
		<!-- input field -->
            <form:checkbox path="customer.updateNewInformation" />
        <!-- end input field -->   	
		</div>
		</div>  	
	<!-- end tab -->        
	</div>         

	<h4 title="<fmt:message key="customerInformation"/>"><fmt:message key="customerInformation"/></h4>
	<div>
	<!-- start tab -->
         <c:set var="classIndex" value="0" />
         
	  	<div class="listdivi ln tabdivi"></div>
	  	<div class="listdivi"></div>
	  	
	  	<div class="list${classIndex % 2}"><c:set var="classIndex" value="${classIndex+1}" />
	  	<div class="listfl"><div class="requiredField"><fmt:message key='firstName' />:</div></div>
	  	<div class="listp">
	  	<!-- input field -->
            <form:input path="customer.address.firstName"  htmlEscape="true" />
            <form:errors path="customer.address.firstName" cssClass="error" />
	    <!-- end input field -->   	
	  	</div>
	  	</div>
	  	
	  	<div class="list${classIndex % 2}"><c:set var="classIndex" value="${classIndex+1}" />
		<div class="listfl"><div class="requiredField"><fmt:message key='lastName' />:</div></div>
		<div class="listp">
		<!-- input field -->
            <form:input path="customer.address.lastName" htmlEscape="true" />
            <form:errors path="customer.address.lastName" cssClass="error" />
		<!-- end input field -->   	
		</div>
		</div>

	  	<div class="list${classIndex % 2}"><c:set var="classIndex" value="${classIndex+1}" />
		<div class="listfl"><div class="requiredField"><fmt:message key="address" /> 1:</div></div>
		<div class="listp">
		<!-- input field -->
            <form:input path="customer.address.addr1" htmlEscape="true" />
            <form:errors path="customer.address.addr1" cssClass="error" />
		<!-- end input field -->   	
		</div>
		</div>

	  	<div class="list${classIndex % 2}"><c:set var="classIndex" value="${classIndex+1}" />
		<div class="listfl"><fmt:message key="address" /> 2:</div>
		<div class="listp">
		<!-- input field -->
            <form:input path="customer.address.addr2" htmlEscape="true" />
            <form:errors path="customer.address.addr2" cssClass="error" />
		<!-- end input field -->   	
		</div>
		</div>

        <div class="list${classIndex % 2}"><c:set var="classIndex" value="${classIndex+1}" />
	    <div class="listfl"><div class="requiredField"><fmt:message key='country' />:</div></div>
		<div class="listp">
		<!-- input field -->
            <form:select id="country" path="customer.address.country" onchange="toggleStateProvince(this)">
             <form:option value="" label="Please Select"/>
            <form:options items="${countries}" itemValue="code" itemLabel="name"/>
            </form:select>
            <form:errors path="customer.address.country" cssClass="error" />
		 <!-- end input field -->   	
		 </div>
		 </div>
		
		 <div class="list${classIndex % 2}"><c:set var="classIndex" value="${classIndex+1}" />
         <div class="listfl"><div class="requiredField"><fmt:message key="zipCode" />:</div></div>
		 <div class="listp">
		 <!-- input field -->
		 	 <form:input id="automate" path="customer.address.zip" htmlEscape="true"  onkeyup="automateCityState(document.getElementById('automate').value)"/>  
		 	 <form:input id="noautomate" path="customer.address.zip" htmlEscape="true" />  
             <form:errors path="customer.address.zip" cssClass="error" />
         <!-- end input field -->
		 </div>
		 </div>

         <div class="list${classIndex % 2}"><c:set var="classIndex" value="${classIndex+1}" />
         <div class="listfl"><div class="requiredField"><fmt:message key="city" />:</div></div>
		 <div class="listp">
		 <!-- input field -->
             <form:input path="customer.address.city" htmlEscape="true" cssClass="highlight"/>
             <form:errors path="customer.address.city" cssClass="error" />
         <!-- end input field -->   	
		 </div>
		 </div>

         <div class="list${classIndex % 2}"><c:set var="classIndex" value="${classIndex+1}" /> 
         <div class="listfl"><div class="requiredField"><fmt:message key="stateProvince" />:</div></div>
		 <div class="listp">
		 <!-- input field -->
	      <table cellspacing="0" cellpadding="0">
	       <tr>
	       <td>
	         <form:select id="state" path="customer.address.stateProvince" cssClass="highlight">
	           <form:option value="" label="Please Select"/>
	           <form:options items="${states}" itemValue="code" itemLabel="name"/>
	         </form:select>
	         <form:select id="ca_province" path="customer.address.stateProvince">
	           <form:option value="" label="Please Select"/>
	           <form:options items="${caProvinceList}" itemValue="code" itemLabel="name"/>
	         </form:select>
	         <form:input id="province" path="customer.address.stateProvince" htmlEscape="true"/>
	       </td>
	       <td><div id="stateProvinceNA">&nbsp;<form:checkbox path="customer.address.stateProvinceNA" id="addressStateProvinceNA" value="true" /> Not Applicable</div></td>
	       <td>&nbsp;</td>
	       <td>
	         <form:errors path="customer.address.stateProvince" cssClass="error" />
	       </td>
	       </tr>
	      </table>
         <!-- end input field -->   	
		 </div>
		 </div> 

         <div class="list${classIndex % 2}"><c:set var="classIndex" value="${classIndex+1}" />
         <div class="listfl"><div class="requiredField"><fmt:message key="phone" />:</div></div>
		 <div class="listp">
		 <!-- input field -->
             <form:input path="customer.address.phone" htmlEscape="true" cssErrorClass="errorField" id="phone_num"/>
	  		 <form:input path="customer.address.phone"  onkeydown="javascript:backspacerDOWN(this,event);" onkeyup="javascript:backspacerUP(this,event);" id="us_phone_num"/>    
             <form:errors path="customer.address.phone" cssClass="error" />
             <span class="helpNote"><p>US: xxx-xxx-xxxx or (xxx) xxx-xxxx Ext-xxxxx</p></span>
         <!-- end input field -->   	
		 </div>
		 </div>
         
         <div class="list${classIndex % 2}"><c:set var="classIndex" value="${classIndex+1}" /> 
         <div class="listfl"><fmt:message key="cellPhone" />:</div>
		 <div class="listp">
		 <!-- input field -->
             <form:input path="customer.address.cellPhone" htmlEscape="true" id="cell_phone_num"/>
      		 <form:input path="customer.address.cellPhone"  onkeydown="javascript:backspacerDOWN(this,event);" onkeyup="javascript:backspacerUP(this,event);" id="us_cell_phone_num"/>  	  
             <form:errors path="customer.address.cellPhone" cssClass="error" />
         <!-- end input field -->   	
		 </div>
		 </div>
		 
		<div class="list${classIndex % 2}" id="mobileCarrierIdSelect" style="display:none;"><c:set var="classIndex" value="${classIndex+1}" />
	    <div class="listfl" ><fmt:message key='mobileCarrier' />:</div>
		<div class="listp">
		<!-- input field -->
            <form:select id="mobileCarrierId" path="customer.address.mobileCarrierId">
             <form:option value="" label="Please Select"/>
            <form:options items="${mobileCarriers}" itemValue="id" itemLabel="carrier"/>
            </form:select>
            <form:errors path="customer.address.mobileCarrierId" cssClass="error" />
		 <!-- end input field -->   	
		 </div>
		 </div>
		 
		<div class="list${classIndex % 2}"><c:set var="classIndex" value="${classIndex+1}" />
	  	<div class="listfl"><div class="helpImg"><img class="toolTipImg" title="Disclaimer::By checking this box, you authorize Via Trading Corporation to send you alerts regarding Events, New Product Listings and/or promotions by text message.
	  	Standard text messaging rates may apply. Please check with your carrier to see exact costs involved in receiving text messages if any.Notification preferences can be modified at any time by logging in to 'My Account' Section, calling or emailing us" 
	  	src="../graphics/question.gif" /></div><fmt:message key="textMessage" /> <fmt:message key="notification" />:</div>
	  	<div class="listp">
	  	<!-- input field -->
			<form:checkbox path="customer.textMessageNotify"/>
	    <!-- end input field -->   	
	  	</div>
	  	</div>
	  	
	  	<div class="list${classIndex % 2}"><c:set var="classIndex" value="${classIndex+1}" />
	  	<div class="listfl"><div class="helpImg"><img class="toolTipImg" title="Note::Notify customer on whatsapp." src="../graphics/question.gif" /></div><fmt:message key="siteMessageNotification"/>:</div>
	  	<div class="listp">
			<form:checkbox path="customer.whatsappNotify"/>
	  	</div>
	  	</div>
	  	
	  	<div class="list${classIndex % 2}"><c:set var="classIndex" value="${classIndex+1}" />
	  	<div class="listfl"><div class="helpImg"><img class="toolTipImg" title="Note::Notify customer for new orders." src="../graphics/question.gif" /></div><fmt:message key="siteMessageNotification"/>:</div>
	  	<div class="listp">
	  	<!-- input field -->
			<form:checkbox path="customer.emailNotify"/>
	    <!-- end input field -->   	
	  	</div>
	  	</div>

		<c:if test="${gSiteConfig['gMASS_EMAIL']}">
		<div class="list${classIndex % 2}"><c:set var="classIndex" value="${classIndex+1}" />
		<div class="listfl"><div class="helpImg"><img class="toolTipImg" title="Note::The Email address will be excluded from Mass Email Module." src="../graphics/question.gif" /></div><fmt:message key="unsubscribeFromGroupMails" />:</div>
		<div class="listp">
		 	<form:checkbox path="customer.unsubscribe" />
		</div>
		</div>
		</c:if>
		 
         <div class="list${classIndex % 2}"><c:set var="classIndex" value="${classIndex+1}" />
         <div class="listfl"><fmt:message key="fax" />:</div>
		 <div class="listp">
		 <!-- input field -->
              <form:input path="customer.address.fax"  htmlEscape="true" />
              <form:errors path="customer.address.fax" cssClass="error" />
         <!-- end input field -->   	
		 </div>
		 </div>
		 
  		 <c:if test="${languageCodes != null}">
         <div class="list${classIndex % 2}"><c:set var="classIndex" value="${classIndex+1}" />
	     <div class="listfl"><fmt:message key='language' />:</div>
		 <div class="listp">
		 <!-- input field -->
       		<form:select path="customer.languageCode">
        	  <form:option value="en"><fmt:message key="language_en"/></form:option>        	  
        	  <c:forEach items="${languageCodes}" var="i18n">
          		<form:option value="${i18n.languageCode}"><fmt:message key="language_${i18n.languageCode}"/></form:option>
          		</c:forEach>
	  		</form:select>
 		 <!-- end input field -->   	
		 </div>
		 </div>
		 </c:if>
		
         <div class="list${classIndex % 2}"><c:set var="classIndex" value="${classIndex+1}" />
         <div class="listfl"><div <c:if test="${siteConfig['CUSTOMERS_REQUIRED_COMPANY'].value == 'true'}">class="requiredField"</c:if>><fmt:message key="company" />:</div></div>
		 <div class="listp">
		 <!-- input field -->
              <form:input path="customer.address.company"  htmlEscape="true" />
              <form:errors path="customer.address.company" cssClass="error" />
         <!-- end input field -->   	
		 </div>
		 </div>
		 
		 <div class="list${classIndex % 2}"><c:set var="classIndex" value="${classIndex+1}" />
         <div class="listfl"><fmt:message key="websiteUrl" />:</div>
		 <div class="listp">
		 <!-- input field -->
              <form:input path="customer.websiteUrl"  htmlEscape="true" />
              <form:errors path="customer.websiteUrl" cssClass="error" />
         <!-- end input field -->   	
		 </div>
		 </div>
		 
		 <div class="list${classIndex % 2}"><c:set var="classIndex" value="${classIndex+1}" />
         <div class="listfl"><fmt:message key="ebayName" />:</div>
		 <div class="listp">
		 <!-- input field -->
              <form:input path="customer.ebayName"  htmlEscape="true" />
              <form:errors path="customer.ebayName" cssClass="error" />
         <!-- end input field -->   	
		 </div>
		 </div>
		 
		 <div class="list${classIndex % 2}"><c:set var="classIndex" value="${classIndex+1}" />
         <div class="listfl"><fmt:message key="amazonName" />:</div>
		 <div class="listp">
		 <!-- input field -->
              <form:input path="customer.amazonName"  htmlEscape="true" />
              <form:errors path="customer.amazonName" cssClass="error" />
         <!-- end input field -->   	
		 </div>
		 </div>
		 
		 <c:if test="${siteConfig['ADDRESS_RESIDENTIAL_COMMERCIAL'].value >= 2}">
         <div class="list${classIndex % 2}"><c:set var="classIndex" value="${classIndex+1}" />
         <div class="listfl"><fmt:message key="deliveryType" />:</div>
		 <div class="listp">
		 <!-- input field -->
              <form:radiobutton path="customer.address.residential" value="true"/>: <fmt:message key="residential" /><br /><form:radiobutton path="customer.address.residential" value="false"/>: <fmt:message key="commercial" />
         <!-- end input field -->   	
		 </div>
		 </div>
		 
		 <c:if test="${siteConfig['SHIPPING_LTL'].value == 'true'}">
		 <div class="list${classIndex % 2}"><c:set var="classIndex" value="${classIndex+1}" />
		 <div class="listfl"><div class="helpImg"><img class="toolTipImg" title="LTL::" src="../graphics/question.gif" /></div><fmt:message key="liftGateDelivery" />:</div>
		 <div class="listp">
		 <!-- input field -->
              <form:checkbox path="customer.address.liftGate" value="true"/>
         <!-- end input field -->   	
		 </div>
		 </div>
		 </c:if>
		 </c:if> 
		 
		 <div class="list${classIndex % 2}"><c:set var="classIndex" value="${classIndex+1}" />
         <div class="listfl"><fmt:message key="accountNumber" />: </div>
		 <div class="listp">
		 <!-- input field -->
		 	<c:choose>
		 	  <c:when test="${siteConfig['CUSTOMER_AUTOGENERATE_ACCOUNTNUMBER'].value == 'true'}">
		 	    <c:out value="${accountNumber}" ></c:out>
		 	  </c:when>
		 	  <c:otherwise>
		 	    <form:input path="customer.accountNumber" maxlength="20" htmlEscape="true" />
		 	  </c:otherwise>
		 	</c:choose>
         <!-- end input field -->
		 </div>
		 </div>

         <c:if test="${gSiteConfig['gTAX_EXEMPTION'] == true}">
         <div class="list${classIndex % 2}"><c:set var="classIndex" value="${classIndex+1}" />
         <div class="listfl"><fmt:message key="taxId" />:</div>
		 <div class="listp">
		 <!-- input field -->
              <form:input path="customer.taxId" maxlength="20" htmlEscape="true" />
              <form:errors path="customer.taxId" cssClass="error" />
              <span class="helpNote"><p><fmt:message key="toDisableTaxExemptionSimplyEraseTheTaxId" /></p></span>
         <!-- end input field -->   	
		 </div>
		 </div>
         </c:if>
	
		
		<div class="list${classIndex % 2}" id="brokerLogo" style="display:none;"><c:set var="classIndex" value="${classIndex+1}" />
		  <div class="listfl"><div class="helpImg"><img class="toolTipImg" title="Image::Upload the image of the product. Max Image size is 2MB." src="../graphics/question.gif" /></div><fmt:message key="brokerImage" />:</div>
		  <div class="listp">
		  <!-- input field -->
		    <c:if test="${customerForm.customer.brokerImage.imageUrl != null and customerForm.customer.brokerImage.imageUrl != ''}">
			  <c:choose>
			   <c:when test="${customerForm.customer.brokerImage.absolute}">
			     <div class="thumbbox">
				<p class="thumb">
			         <img src="<c:url value="${customerForm.customer.brokerImage.imageUrl}"/>?rnd=${randomNum}" border="0" height="100">
			       </p>
			  </div>  
			   </c:when>
			   <c:otherwise>
			     <div class="thumbbox">
				<p class="thumb">
					 <img src="<c:url value="/assets/Image/Customer/${customerForm.customer.brokerImage.imageUrl}"/>?rnd=${randomNum}" border="0" height="100">
				</p>
				<p class="uname">
				<c:out value="${customerForm.customer.brokerImage.imageUrl}" />
				</p>
				 </div>
			   </c:otherwise>
			  </c:choose>
			 <input name="__clear_image_1" type="checkbox"> <fmt:message key="remove" />
		     </c:if>
		    
			 <input value="browse" class="textfield" type="file" name="image_file_1"/>  
		     
		    <!-- end input field -->   	
		  	</div>
		  	</div>

	<!-- end tab -->        
	</div>  
	
	<c:forEach items="${customerForm.customerFields}" var="customerField" varStatus="status">
	
    <c:if test="${status.first}">
	  <h4 title="<fmt:message key='customerFields' />"><fmt:message key="customerFields" /></h4>
	  <div>
	  <!-- start tab -->
	       <c:set var="classIndex" value="0" />
         
	  	  <div class="listdivi ln tabdivi"></div>
	  	  <div class="listdivi"></div>
    </c:if>
    	  
         <div class="list${classIndex % 2}"><c:set var="classIndex" value="${classIndex+1}" />
         <div class="listfl"><c:if test="${customerField.required}" ><div class="requiredField"></c:if><c:out value="${customerField.name}"/>:<c:if test="${customerField.required}" ></div></c:if></div>
		 <div class="listp">
		 <!-- input field -->
    	
	     <c:choose>
	     
	     <c:when test="${!empty customerField.preValue}">
	       <c:choose>
	       <c:when test="${customerField.multiSelecOnAdmin}">         
		       <form:select path="customer.field${customerField.id}Set" cssClass="customerPreValueField" multiple="true" >
		         <form:option value="" label="Please Select"/>
		         <c:forTokens items="${customerField.preValue}" delims="," var="dropDownValue">
		           <form:option value="${dropDownValue}" />
		         </c:forTokens>
		       </form:select>
	       </c:when>
	       <c:otherwise>
	       	   <form:select path="customer.field${customerField.id}" cssClass="customerPreValueField">
		         <form:option value="" label="Please Select"/>
		         <c:forTokens items="${customerField.preValue}" delims="," var="dropDownValue">
		           <form:option value="${dropDownValue}" />
		         </c:forTokens>
		       </form:select>
	       </c:otherwise>
	       </c:choose>
	     </c:when>
	     <c:otherwise>
	       <form:input path="customer.field${customerField.id}"  htmlEscape="true" maxlength="255" size="70"/>
	     </c:otherwise>
	     </c:choose>
	     (Field <c:out value="${customerField.id}"/>)
	     <form:errors path="customer.field${customerField.id}" cssClass="error" />
         
         <!-- end input field -->   	
		 </div>
		 </div> 
		  
    <c:if test="${status.last}">
	  <!-- end tab -->        
	  </div>  
	</c:if>
	
	</c:forEach>
	
	<h4 title="<fmt:message key='options' />"><fmt:message key="options" /></h4>
		<!-- start tab -->
	
		<jsp:include page="formOptions.jsp" flush="true" />
		<!-- End tab -->
	
	

    <c:if test="${gSiteConfig['gAFFILIATE'] > 0}">
	<h4 title="Affiliate">Affiliate</h4>
	<div>
	<!-- start tab -->
         <c:set var="classIndex" value="0" />
         
	  	<div class="listdivi ln tabdivi"></div>
	  	<div class="listdivi"></div>
	  	
	  	<div class="list${classIndex % 2}"><c:set var="classIndex" value="${classIndex+1}" />
	  	<div class="listfl"><fmt:message key="owner" />:</div>
	  	<div class="listp">
	  	<!-- input field -->
		      <form:select path="customer.affiliateParent" onchange="javascript:document.getElementById('_ownerChanged').value=true">
		        <c:choose>
		          <c:when test="${!customerForm.newCustomer and customerForm.customer.isAffiliate}"><form:option value="-1"><fmt:message key="removeChild" /></form:option></c:when>
		        </c:choose>
		        <c:forEach items="${affiliates}" var="affiliate" >
		          <form:option value="${affiliate.nodeId}"><c:choose><c:when test="${affiliate.nodeId == 0}"><fmt:message key="admin" /></c:when><c:otherwise>${affiliate.firstName} ${affiliate.lastName} </c:otherwise></c:choose> </form:option>
		        </c:forEach>         
		      </form:select>
		      <c:if test="${gSiteConfig['gNUMBER_AFFILIATE'] > numAffiliateInUse}" >
		        <div class="helpNote"><p>Ultimate owner of all customers is the Admin If you want to create an affiliate under Admin simply check Affiliate checkBox.</p></div>
		      </c:if>
    	<!-- end input field -->   
    	</div>
    	</div>

        <c:choose>
        <c:when test="${customerForm.customer.isAffiliate}">
	  	<div class="list${classIndex % 2}"><c:set var="classIndex" value="${classIndex+1}" />
	  	<div class="listfl"><fmt:message key="affiliate" />:</div>
	  	<div class="listp">
	  	<!-- input field -->
		      <form:checkbox path="customer.isAffiliate" id="isAffiliate" onclick="toggleAffiliate(this.id)"  />
		      <c:set var="showAffiliateOption" value="true" />
    	<!-- end input field -->   
    	</div>
    	</div>
        </c:when>
        <c:when test="${!customerForm.customer.isAffiliate and gSiteConfig['gNUMBER_AFFILIATE'] > numAffiliateInUse}">
	  	<div class="list${classIndex % 2}"><c:set var="classIndex" value="${classIndex+1}" />
	  	<div class="listfl"><fmt:message key="affiliate" />:</div>
	  	<div class="listp">
	  	<!-- input field -->
		      <form:checkbox path="customer.isAffiliate" id="isAffiliate" onclick="toggleAffiliate(this.id)"  />
              <c:set var="showAffiliateOption" value="true" />
    	<!-- end input field -->   
    	</div>
    	</div>
        </c:when>
       <c:otherwise>
       	<div class="list${classIndex % 2}"><c:set var="classIndex" value="${classIndex+1}" />
	  	<div class="listfl"><fmt:message key="affiliate" />:</div>
	  	<div class="listp">
	  	<!-- input field -->
		      <div class="helpNote"><p>Number of affiliates is exceeded. Contact webmaster to get more Affiliate account.</p></div>
    	<!-- end input field -->   
    	</div>
    	</div>
        </c:otherwise>
        </c:choose>
 
        <c:if test="${gSiteConfig['gSALES_PROMOTIONS'] and showAffiliateOption}">
	  	<div class="list${classIndex % 2}" id="promoCodeID"><c:set var="classIndex" value="${classIndex+1}" />
	  	<div class="listfl"><fmt:message key="affiliate" /><fmt:message key="id" />/<fmt:message key="promoCode" />:</div>
	  	<div class="listp">
	  	<!-- input field -->
        	<form:input path="customer.affiliate.promoTitle" id="promoCode" />
	        <form:errors path="customer.affiliate.promoTitle" cssClass="error" /><br/>
	        <div class="helpNote"><p>Please create a Promotion code and insert it here.( Make sure this Promo Code exist. )</p></div>
    	<!-- end input field -->   
    	</div>
    	</div>

	  	<div class="list${classIndex % 2}" id="categoryID"><c:set var="classIndex" value="${classIndex+1}" />
	  	<div class="listfl"><fmt:message key="category" /><fmt:message key="id" />:</div>
	  	<div class="listp">
	  	<!-- input field -->
        	<form:input path="customer.affiliate.categoryId"  />
	        <form:errors path="customer.affiliate.categoryId" cssClass="error" /><br/>
	        <div class="helpNote"><p>Please create a category and insert it's ID here.( Make sure this Category ID exist. )</p></div>
    	<!-- end input field -->   
    	</div>
    	</div>

	  	<div class="list${classIndex % 2}" id="domainNameID"><c:set var="classIndex" value="${classIndex+1}" />
	  	<div class="listfl"><fmt:message key="domainName" />:</div>
	  	<div class="listp">
	  	<!-- input field -->
        	<form:input path="customer.affiliate.domainName"  />
	        <form:errors path="customer.affiliate.domainName" cssClass="error" /><br/>
	        <div class="helpNote"><p>Please insert the Domain name here.</p></div>
    	<!-- end input field -->   
    	</div>
    	</div>

	  	<div class="list${classIndex % 2}" id="sourceCodeID"><c:set var="classIndex" value="${classIndex+1}" />
	  	<div class="listfl">Source codes:</div>
	  	<div class="listp">
	  	<!-- input field -->       
         	<c:if test="${customerForm.customer.affiliate.promoTitle != null}">
           		<c:out value="${siteConfig['SITE_URL'].value}"/>home.jhtm?promocode=<c:out value="${customerForm.customer.affiliate.promoTitle}"/><br />
         	</c:if>
         	<c:if test="${customerForm.customer.affiliate.categoryId != null and customerForm.customer.affiliate.promoTitle != null}">
           		<c:out value="${siteConfig['SITE_URL'].value}"/>category.jhtm?cid=<c:out value="${customerForm.customer.affiliate.categoryId}"/>&promocode=<c:out value="${customerForm.customer.affiliate.promoTitle}"/><br />
         	</c:if>
         	<c:if test="${customerForm.customer.affiliate.domainName != null}">
           		<c:out value="${customerForm.customer.affiliate.domainName}"/>
         	</c:if>
    	<!-- end input field -->   
    	</div>
    	</div>
        
		<div class="list${classIndex % 2}" id="commissionID"><c:set var="classIndex" value="${classIndex+1}" />
	  	<div class="listfl">Commission:</div>
	  	<div class="listp">
	  	<!-- input field -->   
		   <table border="0" >
		    <tr>
		     <td width="50%">
		     <div>
		      <label>Product Level1:</label>
		      <form:select path="customer.affiliate.productCommissionLevel1">
		        <form:option value="">None</form:option>
		        <c:if test="${siteConfig['NUMBER_PRODUCT_COMMISSION_TABLE'].value > 0}">
		        <c:forEach begin="1" end="${siteConfig['NUMBER_PRODUCT_COMMISSION_TABLE'].value}" var="commissionTable" >
		         <form:option value="${commissionTable}"><c:out value="${commissionTable}" /></form:option>
		        </c:forEach>
		        </c:if>
		      </form:select>
		     </div>
		     <div >
		      <label>Invoice Level1:</label>
		      <form:input path="customer.affiliate.invoiceCommissionLevel1" maxlength="10" htmlEscape="true" />
		      <form:select path="customer.affiliate.percentLevel1">
			   <form:option value="true" label="%" />
			    <form:option value="false"><fmt:message key="FIXED" /> $</form:option>
		      </form:select>
		      <form:errors path="customer.affiliate.invoiceCommissionLevel1" cssClass="error" />
		     </div>
		     </td>
		     <td></td>
		     <td width="50%">
		     <c:if test="${gSiteConfig['gAFFILIATE'] == 2}">
		     <div >
		      <label>Product Level2:</label>
		      <form:select path="customer.affiliate.productCommissionLevel2">
		        <form:option value="">None</form:option>
		        <c:if test="${siteConfig['NUMBER_PRODUCT_COMMISSION_TABLE'].value > 0}">
		        <c:forEach begin="1" end="${siteConfig['NUMBER_PRODUCT_COMMISSION_TABLE'].value}" var="commissionTable" >
		         <form:option value="${commissionTable}"><c:out value="${commissionTable}" /></form:option>
		        </c:forEach>
		        </c:if>
		      </form:select>
		     </div>     
		     <div>
		      <label>Invoice Level2:</label>
		      <form:input path="customer.affiliate.invoiceCommissionLevel2" maxlength="10" htmlEscape="true" />
		         <form:select path="customer.affiliate.percentLevel2">
			    <form:option value="true" label="%" />
			    <form:option value="false"><fmt:message key="FIXED" /> $</form:option>
		      </form:select>
		      <form:errors path="customer.affiliate.invoiceCommissionLevel2" cssClass="error" />
		     </div>
		     </c:if>
		    </td>
		   </tr>
		   <tr>
		   	 <td width="50%">
		     <div >
		      <label>First Order:</label>
		      <form:input path="customer.affiliate.firstOrderCommission" maxlength="10" htmlEscape="true" />
		      <form:select path="customer.affiliate.firstOrderPercent">
			   <form:option value="true" label="%" />
			    <form:option value="false"><fmt:message key="FIXED" /> $</form:option>
		      </form:select>
		      <form:errors path="customer.affiliate.firstOrderCommission" cssClass="error" />
		     </div>
		     </td>
		   	 <td width="50%">
		     <div >
		      <label>All Other Order:</label>
		      <form:input path="customer.affiliate.otherOrderCommission" maxlength="10" htmlEscape="true" />
		      <form:select path="customer.affiliate.otherOrderPercent">
			   <form:option value="true" label="%" />
			    <form:option value="false"><fmt:message key="FIXED" /> $</form:option>
		      </form:select>
		      <form:errors path="customer.affiliate.otherOrderCommission" cssClass="error" />
		     </div>
		     </td>
		   
		   </tr>
		   </table>
		<!-- end input field -->   
    	</div>
    	</div>
    	 
    </c:if>     
	<!-- end tab -->        
	</div>
	</c:if> 

    <c:if test="${gSiteConfig['gCUSTOMER_SUPPLIER']}">
	<h4 title="<fmt:message key="supplier" />"><fmt:message key="liquidateNow" /></h4>
	<div>
	<!-- start tab -->

	  	<div class="listdivi ln tabdivi"></div>
	  	<div class="listdivi"></div>
		    
		  	<div class="list${classIndex % 2}"><c:set var="classIndex" value="${classIndex+1}" />
		  	<div class="listfl"><fmt:message key="convertToSupplier" />:</div>
		  	<div class="listp">
		  	<!-- input field -->
         		<form:checkbox path="customer.convertToSupplier" disabled="${customerForm.customer.supplierId != null}"/>
		    <!-- end input field -->  	  
		  	</div>
		  	</div>	

			<c:if test="${customerForm.customer.supplierId != null}">
		  	<div class="list${classIndex % 2}"><c:set var="classIndex" value="${classIndex+1}" />
		  	<div class="listfl"><fmt:message key="supplierId" />:</div>
		  	<div class="listp">
		  	<!-- input field -->
         		<a href="../supplier/supplier.jhtm?sid=${customerForm.customer.supplierId}" class="nameLink"><c:out value="${customerForm.customer.supplierId}" /></a>
		    <!-- end input field -->  	  
		  	</div>
		  	</div>	
			</c:if>
			
		  	<div class="list${classIndex % 2}"><c:set var="classIndex" value="${classIndex+1}" />
		  	<div class="listfl"><fmt:message key="supplierPrefix" />:</div>
		  	<div class="listp">
		  	<!-- input field -->
		        <form:input path="customer.supplierPrefix" maxlength="5" size="10" htmlEscape="true" disabled="${customerForm.customer.supplierId != null}"/>
		        <form:errors path="customer.supplierPrefix" cssClass="error" />
		    <!-- end input field -->  	  
		  	</div>
		  	</div>			

		  	<div class="list${classIndex % 2}"><c:set var="classIndex" value="${classIndex+1}" />
		  	<div class="listfl"><fmt:message key="product" /> <fmt:message key="limit" />:</div>
		  	<div class="listp">
		  	<!-- input field -->
			  	<form:select path="customer.supplierProdLimit">
		          <form:option value=""><fmt:message key="unlimited"/></form:option>
		          <c:forTokens items="10,25,50,100,500,1000" delims="," var="num">
		          <form:option value="${num}">${num}</form:option>
		          </c:forTokens>      
		        </form:select>
      			<form:errors path="customer.supplierProdLimit" cssClass="error" />
		    <!-- end input field -->  	  
		  	</div>
		  	</div>
	  	
	<!-- end tab -->        
	</div> 
    </c:if>
	
	<c:if test="${gSiteConfig['gLOYALTY']}">
	<h4 title="<fmt:message key='loyalty' />"><fmt:message key="loyalty" /></h4>
	<div>
	<!-- start tab -->
         <c:set var="classIndex" value="0" />
         
	  	<div class="listdivi ln tabdivi"></div>
	  	<div class="listdivi"></div>
	  	
	  	 <div class="list${classIndex % 2}"><c:set var="classIndex" value="${classIndex+1}" />
		 <div class="listfl"><div class="helpImg"><img class="toolTipImg" title="Note::Points that earned through invoice." src="../graphics/question.gif" /></div><fmt:message key="point" />:</div>
		 <div class="listp">
		 <!-- input field -->
                <c:out value="${customerForm.customer.loyaltyPoint}" />
		 <!-- end input field -->   	
		 </div>
		 </div>	  	
		 
		 <div class="list${classIndex % 2}"><c:set var="classIndex" value="${classIndex+1}" />
		 <div class="listfl"><div class="helpImg"><img class="toolTipImg" title="Note::Specify the value of aech point here. For example 1Point = 1Dollar or 1Point = 0.5Dollar" src="../graphics/question.gif" /></div><fmt:message key="pointValue" />:</div>
		 <div class="listp">
		 <!-- input field -->
                1<fmt:message key="point" /> = <fmt:message key="${siteConfig['CURRENCY'].value}" /><form:input path="customer.pointValue" cssClass="textfield" size="4" maxlength="4" />
                <form:errors path="customer.pointValue" cssClass="error" />
		 <!-- end input field -->   	
		 </div>
		 </div>	  
		 
		 <div class="list${classIndex % 2}"><c:set var="classIndex" value="${classIndex+1}" />
		 <div class="listfl"><div class="helpImg"><img class="toolTipImg" title="Note::Convert points to credit after points reached to this threshold. If threshold is empty or 0, points never convert to credit." src="../graphics/question.gif" /></div><fmt:message key="threshold" />:</div>
		 <div class="listp">
		 <!-- input field -->
                <form:input path="customer.pointThreshold" cssClass="textfield" size="10" />
                <form:errors path="customer.pointThreshold" cssClass="error" />
		 <!-- end input field -->   	
		 </div>
		 </div>	  	
	<!-- end tab -->
	</div>
	</c:if> 
	
	<h4 title="Note">Note</h4>
	<div>
	<!-- start tab -->
         <c:set var="classIndex" value="0" />
         
	  	<div class="listdivi ln tabdivi"></div>
	  	<div class="listdivi"></div>
	  	
	  	 <div class="list${classIndex % 2}"><c:set var="classIndex" value="${classIndex+1}" />
		 <div class="listfl"><fmt:message key="note" />:</div>
		 <div class="listp">
		 <!-- input field -->
             <form:textarea path="customer.note" rows="15" cols="60" cssClass="textfield" htmlEscape="true" />
			 <div style="text-align:left" id="customer.note_link"><a href="#" onClick="loadEditor('customer.note')" class="htmleditor">- <fmt:message key="htmlEditor" /> -</a></div>
		 <!-- end input field -->   	
		 </div>
		 </div>	  	
		 
		 <div class="list${classIndex % 2}"><c:set var="classIndex" value="${classIndex+1}" />
		 <div class="listfl"><fmt:message key="dialingNoteHistory" />:</div>
		 <div class="listp" onClick="document.getElementById('informationHistory').style.display='block';">
		 View Dialing Note History
		 </div>
		 </div>
		 
		 <div class="list${classIndex % 2}" id="informationHistory" style="display: none;"><c:set var="classIndex" value="${classIndex+1}" />
		 <div class="listfl"><fmt:message key="dialingNoteHistory" />:</div>
		 <div class="listp">
		 <!-- input field -->
         		<c:forEach items="${customerForm.customer.dialingNoteHistory}" var="dialingNoteHistory" varStatus="status">
          		<p>
          		Note: <c:out value="${dialingNoteHistory.note}" /> &nbsp; &nbsp; &nbsp; Date: <c:out value="${dialingNoteHistory.created}" /> 
          		</p>
          		</c:forEach>    
         <!-- end input field -->   	
		 </div>
		 </div> 
		         	  
		 
		 <div class="list${classIndex % 2}"><c:set var="classIndex" value="${classIndex+1}" />
		 <div class="listfl"><fmt:message key="dialingNote" />:</div>
		 <div class="listp">
		 <!-- input field -->
             <form:textarea path="customer.newDialingNote.note" rows="15" cols="60" cssClass="textfield" htmlEscape="true" />
			 <div style="text-align:left" id="customer.newDialingNote.note_link"><a href="#" onClick="loadEditor('customer.note')" class="htmleditor">- <fmt:message key="htmlEditor" /> -</a></div>
		 <!-- end input field -->   	
		 </div>
		 </div> 
		 
	<!-- end tab -->
	</div>
	
		
	
	<c:if test="${gSiteConfig['gBUDGET_BRAND']}">
	<h4 title="<fmt:message key='brands' />"><fmt:message key='brands' /></h4>
	<div>
	<!-- start tab -->
         <c:set var="classIndex" value="0" />
         
	  	<div class="listdivi ln tabdivi"></div>
	  	<div class="listdivi"></div>
	  	
	  	<c:forEach items="${brands}" var="brand">
	  	 <div class="list${classIndex % 2}"><c:set var="classIndex" value="${classIndex+1}" />
		 <div class="listfl"><c:out value="${brand.name}"/> <fmt:message key='report' />:</div>
		 <div class="listp">
		 <!-- input field -->
		 	<c:set var="skuPrefix" value=",${brand.skuPrefix},"/>
         	<input type="checkbox" name="BRAND_REPORT" value ="${brand.skuPrefix}" <c:if test="${fn:contains(customerForm.customer.brandReport, skuPrefix)}">checked</c:if>>
		 <!-- end input field -->   	
		 </div>
		 </div>	  	
		 </c:forEach>
		 
	<!-- end tab -->
	</div>
	</c:if>
	
	<c:forTokens items="CUSTOMER_CUSTOM_NOTE_1,CUSTOMER_CUSTOM_NOTE_2,CUSTOMER_CUSTOM_NOTE_3" delims="," var="note" varStatus="noteStatus">
	<c:if test="${siteConfig[note].value != '' && siteConfig[note].value != null}">
	<h4 title="Note"><c:out value="${siteConfig[note].value}"/></h4>
	<div>
	<!-- start tab -->
         <c:set var="classIndex" value="0" />
         
	  	<div class="listdivi ln tabdivi"></div>
	  	<div class="listdivi"></div>
	  	
	  	 <div class="list${classIndex % 2}"><c:set var="classIndex" value="${classIndex+1}" />
		 <div class="listfl"><fmt:message key="htmlCode" />:</div>
		 <div class="listp">
		 <!-- input field -->
             <form:textarea path="customer.customNote${noteStatus.count}" rows="15" cols="60" cssClass="textfield" htmlEscape="true"/>
			 <div style="text-align:left" id="customer.customNote${noteStatus.count}_link"><a href="#" onClick="loadEditor('customer.customNote${noteStatus.count}')" class="htmleditor">- <fmt:message key="htmlEditor" /> -</a></div>
			 <form:errors path="customer.customNote${noteStatus.count}" cssClass="error" />
		 <!-- end input field -->   	
		 </div>
		 </div>	  	
	<!-- end tab -->
	</div> 
	</c:if>       
	</c:forTokens>   
	<c:if test="${not empty echoSigns or siteConfig['ECHOSIGN_API_KEY'].value != ''}">
	<h4 title="EchoSign">EchoSign</h4>
	<div>
	<!-- start tab -->
         <c:set var="classIndex" value="0" />
         
	  	<div class="listdivi ln tabdivi"></div>
	  	<div class="listdivi"></div>
	  	
	  	 <div class="list${classIndex % 2}"><c:set var="classIndex" value="${classIndex+1}" />
		  	<div class="listpp">
		  	<!-- input field -->
		  	<c:if test="${not customerForm.newCustomer and siteConfig['ECHOSIGN_API_KEY'].value != ''}">
		  		<div align="center"><a href="../echoSign/sendDocument.jhtm?cid=${customerForm.customer.id}"><fmt:message key="send" /> EchoSign</a></div>
		  	</c:if>
		  	<c:if test="${not empty echoSigns}">
				<table class="form" width="90%" border="0">
				  <tr>
				    <td width="5%">&nbsp;</td>
				    <td><b><fmt:message key="documentName" /></b></td>
				    <td align="center"><b><fmt:message key="status" /></b></td>
				    <td><b><fmt:message key="history" /></b></td>
				  </tr>
				  <c:forEach items="${echoSigns}" var="echoSign" varStatus="status">
				  <tr>
				    <td valign="top" style="text-align:right"><c:out value="${status.index + 1}"/>.&nbsp;&nbsp;&nbsp;</td>
				    <td valign="top"><a href="<c:out value="${echoSign.documentUrl}"/>" target="_blank"><c:out value="${echoSign.documentName}"/></a></td>				    
				    <td valign="top" align="center" style="<c:if test="${echoSign.agreementStatus == 'SIGNED'}">color:red</c:if>">
				      <c:out value="${fn:replace(echoSign.agreementStatus, '_', ' ')}"/>
				      <c:if test="${echoSign.agreementStatus == 'OUT_FOR_SIGNATURE'}">
				        <br/>(<a href="../echoSign/sendReminder.jhtm?key=<c:out value="${echoSign.documentKey}"/>">remind</a>, 
				        <a href="../echoSign/cancelDocument.jhtm?key=<c:out value="${echoSign.documentKey}"/>">cancel</a>)
				      </c:if>
				    </td>
				    <td><c:out value="${wj:newLineToBreakLine(echoSign.history)}" escapeXml="false"/>    
				    </td>
				  </tr>
				  </c:forEach>
				</table>
		  	</c:if>
	 		<!-- end input field -->	
	 		</div> 	
		 </div>	  	
	<!-- end tab -->
	</div> 
	</c:if>
	<h4 title="Analytics">Analytics</h4>
	<div>
	<!-- start tab -->
         <c:set var="classIndex" value="0" />
         
	  	<div class="listdivi ln tabdivi"></div>
	  	<div class="listdivi"></div>
	  	
	  	<div class="list${classIndex % 2}"><c:set var="classIndex" value="${classIndex+1}" />
         <div class="listfl"><fmt:message key="trackcode" />:</div>
		 <div class="listp">
		 <!-- input field -->
              <form:input path="customer.trackcode"  htmlEscape="true" />
              <form:errors path="customer.trackcode" cssClass="error" />
         <!-- end input field -->   	
		 </div>
		 </div>
	  	
	  	
	  	<c:forEach items="${customerForm.customerFields}" var="customerField" varStatus="status">
	  		<c:if test = "${customerField.id == 1}">
	  		<div class="list${classIndex % 2}"><c:set var="classIndex" value="${classIndex+1}" />
         <div class="listfl"><div class="requiredField"><c:out value="${customerField.name}"/>:</div></div>
		 <div class="listp">
	  		<c:out value="${customerForm.customer.field1}"/>
	        (Field <c:out value="${customerField.id}"/>)
	     <form:errors path="customer.field${customerField.id}" cssClass="error" />
         
         <!-- end input field -->   	
	  		</div>
	  		</c:if>
	  	</c:forEach>
	  	
	  	<div class="list${classIndex % 2}"><c:set var="classIndex" value="${classIndex+1}" />
         <div class="listfl"><fmt:message key="mainSource" />:</div>
		 <div class="listp">
		 <!-- input field -->
              <form:input path="customer.mainSource"  htmlEscape="true" />
              <form:errors path="customer.mainSource" cssClass="error" />
         <!-- end input field -->   	
		 </div>
		 </div>
		 
		 <div class="list${classIndex % 2}"><c:set var="classIndex" value="${classIndex+1}" />
         <div class="listfl"><fmt:message key="paid" />:</div>
		 <div class="listp">
		 <!-- input field -->
              <form:input path="customer.paid"  htmlEscape="true" />
              <form:errors path="customer.paid" cssClass="error" />
         <!-- end input field -->   	
		 </div>
		 </div>
		 
		 <div class="list${classIndex % 2}"><c:set var="classIndex" value="${classIndex+1}" />
         <div class="listfl"><fmt:message key="medium" />:</div>
		 <div class="listp">
		 <!-- input field -->
              <form:input path="customer.medium"  htmlEscape="true" />
              <form:errors path="customer.medium" cssClass="error" />
         <!-- end input field -->   	
		 </div>
		 </div>
		 
		 <div class="list${classIndex % 2}"><c:set var="classIndex" value="${classIndex+1}" />
         <div class="listfl"><fmt:message key="language" />:</div>
		 <div class="listp">
		 <!-- input field -->
              <form:input path="customer.language"  htmlEscape="true" />
              <form:errors path="customer.language" cssClass="error" />
         <!-- end input field -->   	
		 </div>
		 </div>
	  	
	  	
	</div>
	</div>
	
	
	<jsp:include page="customerimage.jsp" flush="true" />
    
    <jsp:include page="customerExtContact.jsp" flush="true" />

	
</div>           	
<!-- end tabs -->


			
</div>

<!-- start button -->
<div align="left" class="button"> 
<c:if test="${customerForm.newCustomer}">
  <sec:authorize ifAnyGranted="ROLE_AEM,ROLE_ADMIN_${prop_site_id},ROLE_CUSTOMER_CREATE">
    <input type="submit" value="<spring:message code="customerAdd"/>" />
  </sec:authorize> 
</c:if>
<c:if test="${!customerForm.newCustomer}">
<sec:authorize ifAnyGranted="ROLE_AEM,ROLE_ADMIN_${prop_site_id},ROLE_CUSTOMER_EDIT">
    <input type="submit" value="<spring:message code="customerUpdate"/>" />
</sec:authorize>  
<sec:authorize ifAnyGranted="ROLE_AEM,ROLE_ADMIN_${prop_site_id},ROLE_CUSTOMER_DELETE">
  <input type="submit" value="<spring:message code="customerDelete"/>" name="_delete" onClick="return confirm('Delete permanently?')" />
</sec:authorize>
</c:if>
  <input type="submit" value="<spring:message code="cancel"/>" name="_cancel" />
</div>
<!-- end button -->	   

  <!-- end table -->
    </td><td class="boxmidr" ></td></tr>
  <tr><td class="botl"></td><td class="botr"></td></tr>
  </table>
  
<!-- end main box -->  
</div>          	
</form:form>

<script language="JavaScript">
<!--
function showLogo(el){
	if(el.value == "1"){
		document.getElementById('brokerLogo').disabled=false;
		document.getElementById('brokerLogo').style.display="block";
	}else{
		document.getElementById('brokerLogo').disabled=true;
		document.getElementById('brokerLogo').style.display="none";
	}
}
showLogo(document.getElementById('customerType'));
function toggleStateProvince(el) {
  if (el.value == "US") {
      document.getElementById('state').disabled=false;
      document.getElementById('state').style.display="block";
      document.getElementById('ca_province').disabled=true;
      document.getElementById('ca_province').style.display="none";
      document.getElementById('province').disabled=true;
      document.getElementById('province').style.display="none";
      document.getElementById('addressStateProvinceNA').disabled=true;
      document.getElementById('stateProvinceNA').style.display="none";
      document.getElementById('mobileCarrierIdSelect').disabled=false;
      document.getElementById('mobileCarrierIdSelect').style.display="block";
      document.getElementById('automate').disabled=false;
      document.getElementById('automate').style.display="block";
      document.getElementById('noautomate').disabled=true;
      document.getElementById('noautomate').style.display="none";
      document.getElementById('phone_num').disabled=true;
      document.getElementById('phone_num').style.display="none";
      document.getElementById('us_phone_num').disabled=false;
      document.getElementById('us_phone_num').style.display="block";
      document.getElementById('cell_phone_num').disabled=true;
      document.getElementById('cell_phone_num').style.display="none";
      document.getElementById('us_cell_phone_num').disabled=false;
      document.getElementById('us_cell_phone_num').style.display="block";
  } else if (el.value == "CA") {
      document.getElementById('state').disabled=true;
      document.getElementById('state').style.display="none";
      document.getElementById('ca_province').disabled=false;
      document.getElementById('ca_province').style.display="block";
      document.getElementById('province').disabled=true;
      document.getElementById('province').style.display="none";
      document.getElementById('addressStateProvinceNA').disabled=true;
      document.getElementById('stateProvinceNA').style.display="none";
      document.getElementById('mobileCarrierIdSelect').disabled=true;
      document.getElementById('mobileCarrierIdSelect').style.display="none";
      document.getElementById('automate').disabled=true;
      document.getElementById('automate').style.display="none";
      document.getElementById('noautomate').disabled=false;
      document.getElementById('noautomate').style.display="block";
      document.getElementById('us_phone_num').disabled=true;
      document.getElementById('us_phone_num').style.display="none";
      document.getElementById('phone_num').disabled=false;
      document.getElementById('phone_num').style.display="block";
      document.getElementById('us_cell_phone_num').disabled=true;
      document.getElementById('us_cell_phone_num').style.display="none";
      document.getElementById('cell_phone_num').disabled=false;
      document.getElementById('cell_phone_num').style.display="block";
  } else {
      document.getElementById('state').disabled=true;
      document.getElementById('state').style.display="none";
      document.getElementById('ca_province').disabled=true;
      document.getElementById('ca_province').style.display="none";
      document.getElementById('province').disabled=false;
      document.getElementById('province').style.display="block";
      document.getElementById('addressStateProvinceNA').disabled=false;
      document.getElementById('stateProvinceNA').style.display="block";
      document.getElementById('mobileCarrierIdSelect').disabled=true;
      document.getElementById('mobileCarrierIdSelect').style.display="none";
      document.getElementById('automate').disabled=true;
      document.getElementById('automate').style.display="none";
      document.getElementById('noautomate').disabled=false;
      document.getElementById('noautomate').style.display="block";
      document.getElementById('us_phone_num').disabled=true;
      document.getElementById('us_phone_num').style.display="none";
      document.getElementById('phone_num').disabled=false;
      document.getElementById('phone_num').style.display="block";
      document.getElementById('us_cell_phone_num').disabled=true;
      document.getElementById('us_cell_phone_num').style.display="none";
      document.getElementById('cell_phone_num').disabled=false;
      document.getElementById('cell_phone_num').style.display="block";
  }
}
toggleStateProvince(document.getElementById('country'));
<c:if test="${gSiteConfig['gAFFILIATE'] > 0}" >
if (document.getElementById('isAffiliate').checked) 
	{
	  document.getElementById('promoCodeID').style.display="block";
	  document.getElementById('commissionID').style.display="block";
	  document.getElementById('categoryID').style.display="block";
	  document.getElementById('domainNameID').style.display="block";
	  document.getElementById('sourceCodeID').style.display="block";
	} else 
	{
	  document.getElementById('promoCodeID').style.display="none";
	  document.getElementById('commissionID').style.display="none";
	  document.getElementById('categoryID').style.display="none";
	  document.getElementById('domainNameID').style.display="none";
	  document.getElementById('sourceCodeID').style.display="none";
	}
</c:if>
//-->
</script>
</sec:authorize>

  </tiles:putAttribute>
</tiles:insertDefinition>


