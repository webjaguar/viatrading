<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="/WEB-INF/tlds/spring.tld" prefix="spring" %>
<%@ taglib uri="http://www.springframework.org/security/tags" prefix="sec" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>

<tiles:insertDefinition name="admin.customers.vbaPaymentReport" flush="true">
  <tiles:putAttribute name="content" type="string">
  <script language="JavaScript">
<!--
<sec:authorize ifAnyGranted="ROLE_AEM,ROLE_ADMIN_${prop_site_id},ROLE_OUTGOING_PAYMENT_CANCEL">  
function cancelSelected() {
	var ids = document.getElementsByName("__selected_transactionId");	
  	if (ids.length == 1) {
      if (document.list.__selected_transactionId.checked) {
    	return confirm('Are you sure you want to cancle selected payment?');
      }
    } else {
      for (i = 0; i < ids.length; i++) {
        if (document.list.__selected_transactionId[i].checked) {
    	  return confirm('Are you sure you want to cancle selected payments?');
        }
      }
    }

    alert("Please select payment(s) to cancel.");       
    return false;
}
</sec:authorize>
//-->
</script>
  <sec:authorize ifAnyGranted="ROLE_AEM,ROLE_ADMIN_${prop_site_id},ROLE_OUTGOING_PAYMENT_VIEW_LIST">
  	<form name="list" id="list" action="vbaPaymentReport.jhtm" >
			<!-- main box -->
			  <div id="mboxfull">
			  
			  <!-- start table -->
			  <table cellpadding="0" cellspacing="0" border="0" class="module" >
			  <tr>
			    <td class="topl_g">
				  
				  <!-- breadcrumb -->
				  <p class="breadcrumb">
				   	<a href="../customers"><fmt:message key="customers"/></a> &gt;
				    <fmt:message key="outgoingPayments" />
				  </p>
				  
				  <!-- Error -->
				  <c:if test="${!empty model.message}">
					<div class="message"><fmt:message key="${model.message}" /></div>
				  </c:if>
				  
				  <!-- header image -->
				  
			  </td><td class="topr_g" ></td></tr>
			  <tr><td class="boxmidlrg" >
			
			 <!-- tabs -->
			 <div class="tab-wrapper">
				<h4 title=""></h4>
				<div>
				<!-- start tab -->
				
				  	<div class="listdivi"></div>
				    
					  	<div class="listlight">
					  	<!-- input field -->
					  	
					  		<table border="0" cellpadding="0" cellspacing="1" width="100%">
							  <tr>
							  <c:if test="${model.vbaPaymentReportList.nrOfElements > 0}">
								<td class="pageShowing">
									<fmt:message key="showing">
										<fmt:param value="${model.vbaPaymentReportList.firstElementOnPage + 1}" />
										<fmt:param value="${model.vbaPaymentReportList.lastElementOnPage + 1}" />
										<fmt:param value="${model.vbaPaymentReportList.nrOfElements}" />
									</fmt:message>
								</td>
							  </c:if>
							  <td class="pageNavi">
								<fmt:message key="page" />
								<select name="page" id="page" onchange="submit()">
									<c:forEach begin="1" end="${model.vbaPaymentReportList.pageCount}"
										var="page">
										<option value="${page}"
											<c:if test="${page == (model.vbaPaymentReportList.page+1)}">selected</c:if>>
											${page}
										</option>
									</c:forEach>
								</select> 
								<fmt:message key="of" />
								<c:out value="${model.vbaPaymentReportList.pageCount}" /> 
								|
								<c:if test="${model.vbaPaymentReportList.firstPage}">
									<span class="pageNaviDead"><fmt:message key="previous" /></span>
								</c:if>
								<c:if test="${not model.vbaPaymentReportList.firstPage}">
									<a
										href="<c:url value="vbaPaymentReport.jhtm"><c:param name="page" value="${model.vbaPaymentReportList.page}"/><c:param name="size" value="${model.vbaPaymentReportList.pageSize}"/></c:url>"
										class="pageNaviLink"><fmt:message key="previous" /></a>
								</c:if>
								|
								<c:if test="${model.vbaPaymentReportList.lastPage}">
									<span class="pageNaviDead"><fmt:message key="next" /></span>
								</c:if>
								<c:if test="${not model.vbaPaymentReportList.lastPage}">
									<a
										href="<c:url value="vbaPaymentReport.jhtm"><c:param name="page" value="${model.vbaPaymentReportList.page+2}"/><c:param name="size" value="${model.vbaPaymentReportList.pageSize}"/></c:url>"
										class="pageNaviLink"><fmt:message key="next" /></a>
								</c:if>
							</td>
							</tr>
							</table>
					  	
					  	<table border="0" cellpadding="2" cellspacing="0" width="100%" class="listings">
						   <tr class="listingsHdr2">
							  <td class="indexCol">&nbsp;</td>
							  <sec:authorize ifAnyGranted="ROLE_AEM,ROLE_ADMIN_${prop_site_id},ROLE_OUTGOING_PAYMENT_CANCEL">  
							  <td class="listingsHdr3">
									<fmt:message key="cancel" />
							  </td>
							  </sec:authorize>
							  <td class="listingsHdr3">
									<fmt:message key="orderNumber" />
							  </td>
							  <td class="listingsHdr3">
									<fmt:message key="orderDate" />
							  </td>
							  <td class="listingsHdr3">
									<fmt:message key="dateShipped" />
							  </td>
							  <c:if test="${model.reportView == 'consignment' }">
							  <td class="listingsHdr3">
									<fmt:message key="sku" />
							  </td>
							  </c:if>							  
							  <td class="listingsHdr3">
									<fmt:message key="companyName" />
							  </td>
							  <c:if test="${model.reportView == 'consignment' }">
							  <td class="listingsHdr3">
									<fmt:message key="cost" />
							  </td>
							  <td  class="listingsHdr3">
									<fmt:message key="quantity" />
							  </td>
							  </c:if>
							  <td class="listingsHdr3">
									<fmt:message key="subTotal" />
							  </td>
							  <td class="listingsHdr3">
									<fmt:message key="amountToPay" />
							  </td>
							  <c:if test="${model.reportView == 'consignment' }">
							  <td class="listingsHdr3">
									<fmt:message key="productConsignmentNote" />
							  </td>
							  </c:if>
							  <td class="listingsHdr3">
									<fmt:message key="dateOfPay" />
							  </td>
							  <td class="listingsHdr3">
									<fmt:message key="paymentMethod" />
							  </td>
							  <td class="listingsHdr3">
									<fmt:message key="note" />
							  </td>
							  <td class="listingsHdr3">
									<fmt:message key="transactionID" />
							  </td>
						   </tr>
					  	  <c:forEach items="${model.vbaPaymentReportList.pageList}" var="vbaPaymentReport" varStatus="status">
						  <tr class="row${status.index % 2}" onmouseover="changeStyleClass(this,'rowOver')" onmouseout="changeStyleClass(this,'row${status.index % 2}')">									    
							  <td class="indexCol"><c:out value="${status.count}"/>.</td>
							  <sec:authorize ifAnyGranted="ROLE_AEM,ROLE_ADMIN_${prop_site_id},ROLE_OUTGOING_PAYMENT_CANCEL">  
							  <td class="nameCol">
							  	<input name="__selected_transactionId" value="${vbaPaymentReport.transactionId}" type="checkbox">
							  </td>
							  </sec:authorize>
							  <td class="nameCol">				  
							  	<a href="../orders/invoice.jhtm?order=${vbaPaymentReport.orderId}" class="nameLink"><c:out value="${vbaPaymentReport.orderId}"/></a>
							  </td>
							  <td align="left">
							  	<fmt:formatDate type="date" timeStyle="default" value="${vbaPaymentReport.dateOrdered}"/>
							  </td>
							  <td align="left">
							  	<fmt:formatDate type="date" timeStyle="default" value="${vbaPaymentReport.orderShipDate}" />
							  </td>
							  <c:if test="${model.reportView == 'consignment' }">
							  <td align="left">
							  	<c:out value="${vbaPaymentReport.productSku}"/>
							  </td>
							  </c:if>
							  <td align="left">
							  	<c:out value="${vbaPaymentReport.companyName}"/>
							  </td>
							  <c:if test="${model.reportView == 'consignment' }">
							  <td align="left">
							  	<c:choose>
							  		<c:when test="${vbaPaymentReport.cost == null or vbaPaymentReport.costPercent == null }">
							  			&nbsp;
							  		</c:when>
							  		<c:when test="${vbaPaymentReport.costPercent == '1' }">
							  			<c:out value="${vbaPaymentReport.cost}"/>%
							  		</c:when>
							  		<c:otherwise>
							  			<fmt:message key="${siteConfig['CURRENCY'].value}" /><fmt:formatNumber value="${vbaPaymentReport.cost}" pattern="#,##0.00"/>
							  		</c:otherwise>
							  	</c:choose>
							  </td>
							  <td align="center">
							  	<c:out value="${vbaPaymentReport.quantity}"/>
							  </td>
							  </c:if>
							  <td align="left">
							  	<fmt:message key="${siteConfig['CURRENCY'].value}" /><fmt:formatNumber value="${vbaPaymentReport.subTotal}" pattern="#,##0.00"/>
							  </td>
							  <td align="left">				  
							  	<c:choose>
							  		<c:when test="${vbaPaymentReport.commission != 0 and vbaPaymentReport.commission != '' }">
							  			<fmt:message key="${siteConfig['CURRENCY'].value}" /><fmt:formatNumber value="${vbaPaymentReport.commission}" pattern="#,##0.00"/>
							  		</c:when>
							  		<c:otherwise>
							  			<fmt:message key="${siteConfig['CURRENCY'].value}" /><fmt:formatNumber value="${vbaPaymentReport.consignment}" pattern="#,##0.00"/>
							  		</c:otherwise>
							  	</c:choose>
							  </td>
							  <c:if test="${model.reportView == 'consignment' }">
							  <td align="left">	
							  	<c:out value="${vbaPaymentReport.productConsignmentNote}"/>
							  </td>
							  </c:if>
							  <td align="left">
							    <fmt:formatDate type="date" pattern="MM/dd/yyyy" value="${vbaPaymentReport.dateOfPay}"/>
							  </td>
							  <td align="left">
							  	<c:out value="${vbaPaymentReport.paymentMethod}"/>
							  </td>
							  <td align="left">	  		
							  		<c:out value="${vbaPaymentReport.note}"/>
							  </td>
							  <td align="left">	  		
							  		<c:out value="${vbaPaymentReport.transactionId}"/>
							  </td>
							  </tr>
						  </c:forEach>
						  
						</table>	  
					    <!-- end input field -->  	  
					    <table border="0" cellpadding="0" cellspacing="1" width="100%">
						  <tr>
						  <td>&nbsp;</td>  
						  <td class="pageSize">
						  <select name="size" onchange="document.getElementById('page').value=1;submit()">
						    <c:forTokens items="10,25,50,100,200,500" delims="," var="current">
						  	  <option value="${current}" <c:if test="${current == model.vbaPaymentReportList.pageSize}">selected</c:if>>${current} <fmt:message key="perPage" /></option>
						    </c:forTokens>
						  </select>
						  </td>
						  </tr>
						</table>
					  </div>       
				</div>
			<!-- end tabs -->			
			</div>
			
			<!-- start button -->
            <div align="left" class="button">
            <sec:authorize ifAnyGranted="ROLE_AEM,ROLE_ADMIN_${prop_site_id},ROLE_OUTGOING_PAYMENT_CANCEL">  
            	<input type="submit" name="__cancel" value="Cancel Selected Payments" onClick="return cancelSelected()">
            </sec:authorize>
            </div>
			  
			  <!-- end table -->
			   </td><td class="boxmidr" ></td></tr>
			  <tr><td class="botl"></td><td class="botr"></td></tr>
			  </table>
			  
			<!-- end main box -->  
			</div>
			</form>
  </sec:authorize>
  </tiles:putAttribute>
</tiles:insertDefinition>