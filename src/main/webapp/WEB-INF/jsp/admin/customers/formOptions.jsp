<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="/WEB-INF/tlds/spring.tld" prefix="spring" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ taglib uri="http://www.springframework.org/security/tags" prefix="sec" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ taglib uri="/WEB-INF/tlds/custom.tld" prefix="wj" %><div>
	<!-- start tab -->
         <c:set var="classIndex" value="0" />
         
	  	<div class="listdivi ln tabdivi"></div>
	  	<div class="listdivi"></div>
		
		<div class="list${classIndex % 2}"><c:set var="classIndex" value="${classIndex+1}" />
		<div class="listfl"><fmt:message key="shipping" /> <fmt:message key="options" />:</div>
		<div class="listp">
		<!-- input field -->
            <form:select path="customer.shippingTitle">
             <form:option value=""><fmt:message key="default" /></form:option>
             <form:option value="No Shipping">No Shipping</form:option>
            </form:select>
        <!-- end input field -->   	
		</div>
		</div>

	  	<div class="list${classIndex % 2}"><c:set var="classIndex" value="${classIndex+1}" />
		<div class="listfl"><fmt:message key="payment" /> <fmt:message key="options" />:</div>
		<div class="listp">
		<!-- input field --> 
		    <form:select path="customer.payment">
		      <form:option value=""><fmt:message key="payment.immediate" /></form:option>
		      <c:forEach items="${paymentList}" var="paymentMethod">
				<c:if test="${paymentMethod.enabled and (not (fn:toLowerCase(fn:trim(paymentMethod.title)) == 'credit card'))}">
		           <form:option value="${paymentMethod.title}">${paymentMethod.title}</form:option>
		        </c:if>
		      </c:forEach>
		    </form:select>
		    <form:errors path="customer.payment" cssClass="error" />
        <!-- end input field -->   	
		</div>
		</div>

        <c:if test="${gSiteConfig['gPRICE_TABLE'] > 0}">
	  	<div class="list${classIndex % 2}"><c:set var="classIndex" value="${classIndex+1}" />
 		<div class="listfl"><fmt:message key="productPriceTable" />:</div>
		<div class="listp">
		<!-- input field -->
		    <form:select path="customer.priceTable">
		      <form:option value="0"><fmt:message key="regularPricing" /></form:option>
		      <c:forEach begin="1" end="${gSiteConfig['gPRICE_TABLE']}" var="priceTable">
			    <c:set var="key" value="priceTable${priceTable}"/>
			    <c:choose>
			      <c:when test="${labels[key] != null and labels[key] != ''}">
			        <c:set var="label" value="${labels[key]}"/>
			      </c:when>
			      <c:otherwise><c:set var="label"><fmt:message key="productPriceTable" /> ${priceTable}</c:set></c:otherwise>
			    </c:choose>		        
		        <form:option value="${priceTable}"><c:out value="${label}"/></form:option>
		      </c:forEach>
		    </form:select>
            <form:errors path="customer.priceTable" cssClass="error" />
        <!-- end input field -->   	
		</div>
		</div>
        </c:if>
        
	  	<div class="list${classIndex % 2}"><c:set var="classIndex" value="${classIndex+1}" />
 		<div class="listfl"><fmt:message key="SeeHiddenPrice" />:</div>
		<div class="listp">
		<!-- input field -->
            <form:checkbox path="customer.seeHiddenPrice" />
        <!-- end input field -->   	
		</div>
		</div>
	
        <c:if test="${customerForm.protectedAccess != null}">
	  	<div class="list${classIndex % 2}"><c:set var="classIndex" value="${classIndex+1}" />
  		<div class="listfl"><fmt:message key="protectedAccess" />:</div>
		<div class="listp">
		<!-- input field -->
			<div class="protectedCheckBoxWrapper">
			<ul>
       		<c:forEach items="${customerForm.protectedAccess}" var="protectedAccess" varStatus="protectedStatus">
  	  	    <c:set var="key" value="protected${protectedStatus.index+1}"/>
			    <c:choose>
			      <c:when test="${labels[key] != null and labels[key] != ''}">
			        <c:set var="label" value="${labels[key]}"/>
			      </c:when>
			      <c:otherwise><c:set var="label"> ${protectedStatus.index+1}</c:set></c:otherwise>
			    </c:choose>		        
		        <li><input name="__enabled_${protectedStatus.index}" type="checkbox" <c:if test="${protectedAccess}">checked</c:if>><span class="protectedLabel">${label}</span></li>
	        </c:forEach>
	        </ul>
	        </div>
        <!-- end input field -->   	
		</div>
		</div>
        </c:if>	
        
	  	<div class="list${classIndex % 2}"><c:set var="classIndex" value="${classIndex+1}" />
   		<div class="listfl"><fmt:message key="loginSuccessPage" />:</div>
		<div class="listp">
		<!-- input field -->
			<form:input path="customer.loginSuccessURL" htmlEscape="true" maxlength="50" size="30" value="${loginSuccessURL}"/>
        <!-- end input field -->   	
		</div>
		</div>
        
        <c:if test="${gSiteConfig['gSHOPPING_CART'] == true and gSiteConfig['gSALES_REP'] == true}">
	  	<div class="list${classIndex % 2}"><c:set var="classIndex" value="${classIndex+1}" />
   		<div class="listfl"><fmt:message key="salesRep" />:</div>
		<div class="listp">
		<!-- input field -->
	        <form:select path="customer.salesRepId" value = "${customer.salesRepId}" onchange="changeHref(${customerForm.customer.id},this.value);">
	          <form:option value=""><fmt:message key="none" /></form:option>
	          <c:forEach items="${salesReps}" var="salesRep">
	  	        <form:option value="${salesRep.id}"><c:out value="${salesRep.name}" /></form:option>
		      </c:forEach>
	        </form:select>
	        <c:if test="${not customerForm.newCustomer}"><a id="emailSalesRep" href="email.jhtm?id=${customerForm.customer.id}&salesrepid=${customerForm.customer.salesRepId}"><fmt:message key="sendEmail" /></a></c:if>
        <!-- end input field -->   	
		</div>
		</div>
		 
        </c:if> 
        
        
        <!-- Access User -->
		<div class="list${classIndex % 2}"><c:set var="classIndex" value="${classIndex+1}" />
   		<div class="listfl">Access user:</div>
		<div class="listp">
         <form:select path="customer.accessUserId">
   		    <form:option value="" label="None"></form:option>
   		    <c:forEach items="${accessUsers}" var="accessUser">
   		    <form:option value="${accessUser.id}" label="${accessUser.username}"></form:option>
   		    </c:forEach>
   		</form:select>
	    </div>
		</div>     
	
        
        <!-- Quaifiers -->
		<div class="list${classIndex % 2}"><c:set var="classIndex" value="${classIndex+1}" />
   		<div class="listfl">Qualifier:</div>
		<div class="listp">
         <form:select path="customer.qualifier">
   		    <form:option value="" label="None"></form:option>
	   		    <c:forEach items="${activeQualifiers}" var="qualifier">
	   		    	<form:option value="${qualifier.id}" label="${qualifier.name}"></form:option>
	   		    </c:forEach>
   			</form:select>
	    </div>
		</div> 
        
        <div class="list${classIndex % 2}"><c:set var="classIndex" value="${classIndex+1}" />
   		<div class="listfl"><fmt:message key="purchaseOrderNumber" />:</div>
		<div class="listp">
		<!-- input field -->
	        <form:select path="customer.poRequired">
          	  <form:option value=""><fmt:message key="normal" /></form:option>
              <form:option value="warning"><fmt:message key="warning" /></form:option>
              <form:option value="required"><fmt:message key="required" /></form:option>
            </form:select>
        <!-- end input field -->   	
		</div>
		</div>
		
		<div class="list${classIndex % 2}"><c:set var="classIndex" value="${classIndex+1}" />
   		<div class="listfl"><fmt:message key="customerType" />:</div>
		<div class="listp">
		<!-- input field -->
	        <form:select id="customerType" path="customer.customerType" onchange="showLogo(this)">
          	  <form:option value="0"><fmt:message key="normal" /></form:option>
              <form:option value="1"><fmt:message key="broker" /></form:option>
            </form:select>
        <!-- end input field -->   	
		</div>
		</div>
	<!-- end tab -->        
	</div>           	