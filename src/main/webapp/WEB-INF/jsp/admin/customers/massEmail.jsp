<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="/WEB-INF/tlds/spring.tld" prefix="spring" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ taglib uri="http://www.springframework.org/security/tags" prefix="sec" %>

<tiles:insertDefinition name="admin.customers" flush="true">
  <tiles:putAttribute name="tab"  value="customer" />
  <tiles:putAttribute name="content" type="string">

<sec:authorize ifAnyGranted="ROLE_AEM,ROLE_ADMIN_${prop_site_id},ROLE_EMAILMANAGER_CREATE">   
<script type="text/javascript" src="<c:url value="/FCKeditor/fckeditor.js"/>"></script>
<script language="JavaScript" type="text/JavaScript">
<!--
window.addEvent('domready', function(){
	var Tips1 = new Tips($$('.toolTipImg'));
});

function getSiteMessage(groupId) {
    var req = new Request.HTML({  
    url: '../config/show-ajax-groupSiteMessage.jhtm?groupId='+groupId,
        update: $('siteMessageSelect'),
        onSuccess: function(response){
        	document.getElementById('messageSelect').style.display="block";
        }       
        }).send();
}
//-->
</script>
	
<form:form commandName="form" >
<input type="hidden" name="_backend_user" value="<sec:authentication property="principal.username"/>" />
<input type="hidden" name="_session_id" value="${pageContext.session.id}" />
<input type="hidden" id="send" value="true">
<!-- main box -->
  <div id="mboxfull">
  
  <!-- start table -->
  <table cellpadding="0" cellspacing="0" border="0" class="module" >
  <tr>
    <td class="topl_g">
	  
	  <!-- breadcrumb -->
	  <p class="breadcrumb">
	    <a href="../customers">Customer</a> &gt;
	    <fmt:message key="massEmail"/>
	  </p>
	  
	  <!-- Error Message -->
	  <c:if test="${message != null}">
		<div class="message"><fmt:message key="${message}"><fmt:param value="${form.numEmails}"/></fmt:message> </div>
	  </c:if>
	  <spring:hasBindErrors name="form">
		<div class="error"><fmt:message key='form.hasErrors'/></div>
	  </spring:hasBindErrors>
	  <c:if test="${pointError != null}">
		<div class="message"><fmt:message key="${pointError}"><fmt:param value="${form.numEmails}" /><fmt:param value="${massEmailPoint}" /></fmt:message></div>
	  </c:if>
	  
	  <!-- header image -->
	  <img class="headerImage" src="../graphics/email40x40.gif" />
  
  </td><td class="topr_g" ></td></tr>
  <tr><td class="boxmidlrg" >
 <!-- tabs -->
 <div id="tab-block-1">
	<div>
	<!-- start tab -->

        <c:set var="classIndex" value="0" />
        
	  	<div class="listdivi ln tabdivi"></div>
	  	<div class="listdivi"></div>
	  	
	  		<form:input type="hidden" id="messageId" path="messageId" size="50" maxlength="50" htmlEscape="true"/>
	   
		  	<div class="list${classIndex % 2}"><c:set var="classIndex" value="${classIndex+1}" />
		  	<div class="listfl">Number of Email to send:</div>
		  	<div class="listp">
		  	<!-- input field -->
	   			<span class="static"><c:out value="${form.numEmails}"/></span>
		    <!-- end input field -->   	
		  	</div>
		  	</div>  

		  	<div class="list${classIndex % 2}"><c:set var="classIndex" value="${classIndex+1}" />
		  	<div class="listfl"><div class="requiredField"><fmt:message key="from"/>:</div></div>
		  	<div class="listp">
		  	<!-- input field -->
		  		<form:input id="from" path="from" size="50" maxlength="50" htmlEscape="true"/>
		  		<input type="text" disabled style="display:none;" id="from" name="from" value="<c:out value="${form.from}"/>" size="50" maxlength="50">
		    	<form:errors path="from" cssClass="error"/>
		    <!-- end input field -->   	
		  	</div>
		  	</div>  
		  	
		  	<div class="list${classIndex % 2}"><c:set var="classIndex" value="${classIndex+1}" />
		  	<div class="listfl"><div class="requiredField"><fmt:message key="subject"/>:</div></div>
		  	<div class="listp">
		  	<!-- input field -->
	   			<form:input id="subject" path="subject" size="50" maxlength="50" htmlEscape="true"/>
	  			  <c:forEach var="message" items="${messages}">
	    		  <input type="text" disabled style="display:none;" id="subject${message.messageId}" name="subject" value="<c:out value="${message.subject}"/>" size="50" maxlength="50">
	  			  </c:forEach>
	            <form:errors path="subject" cssClass="error"/>
		    <!-- end input field -->   	
		  	</div>
		  	</div> 
		  	
		  	<div class="list${classIndex % 2}"><c:set var="classIndex" value="${classIndex+1}" />
		  	<div class="listfl">Html:</div>
		  	<div class="listp">
		  	<!-- input field -->
	   			<form:checkbox path="html" onchange="showHtmlEditor(this)"/>
	  			  <c:forEach var="message" items="${messages}">
	    		  <input type="hidden" id="htmlID${message.messageId}" value="<c:out value="${message.html}"/>" />
	  			  </c:forEach>
	            <form:errors path="html" cssClass="error"/>
		    <!-- end input field -->   	
		  	</div>
		  	</div>  
		  	
		  	<div class="list${classIndex % 2}"><c:set var="classIndex" value="${classIndex+1}" />
		  	<div class="listfl"><div class="requiredField"><fmt:message key="message"/>:</div></div>
		  	<div class="listp" id="preLoadMessage">
		  	<!-- input field -->
	   			<form:textarea id="message" path="message" rows="15" cols="60" htmlEscape="true"/>
	   			<div name="htmlLink" class="htmlEditorLink" style="text-align:right;width:455px;display:none;" id="htmlEditor_link"><a href="#" onClick="loadEditor('message',-1)" class="htmleditor">- <fmt:message key="htmlEditor" /> -</a></div>
	  			  <c:forEach var="message" items="${messages}">
	    			<textarea disabled style="display:none;" id="message${message.messageId}" name="message" rows="15" cols="60"><c:out value="${message.message}"/></textarea>
	    			<div name="htmlLink" class="htmlEditorLink" style="text-align:right;width:455px;display:none;" id="htmlEditor_link${message.messageId}"><a href="#" onClick="loadEditor('message${message.messageId}',${message.messageId})" class="htmleditor">- <fmt:message key="htmlEditor" /> -</a></div>
	  			  </c:forEach>
	           <form:errors path="message" cssClass="error"/>
		    <!-- end input field -->     	
		  	</div>
		  	</div> 
		  	
		  	<div class="list${classIndex % 2}"><c:set var="classIndex" value="${classIndex+1}" /> 
		  	<div class="listfl"><div class="requiredField"><fmt:message key="group"/>:</div></div>
		  	<div id="groupSelect">
			      <select id="groupSelected" onChange="getSiteMessage(this.value)" >
			        <option value="">choose a group</option>
			        <option value="-1"> All Messages</option>
				  <c:forEach var="group" items="${messageGroups}">
				    <option value="${group.id}"><c:out value="${group.name}"/></option>
				  </c:forEach>
				  </select>
		  	</div>
		  	</div>
		  	<div class="list${classIndex % 2}"><c:set var="classIndex" value="${classIndex+1}" />
			<div class="listfl" id="messageSelect" style="display:none;"><div class="requiredField"><fmt:message key="message"/> :</div></div>
			<div class="listp">
		  	<!-- input field -->
	         <div id="siteMessageSelect">
		  	 </div>		     		
		    <!-- end input field -->   	
		  	</div>	
		  </div>	
		  	
		  	<div class="list${classIndex % 2}"><c:set var="classIndex" value="${classIndex+1}" />
		  	<div class="listfl"><div class="helpImg"><img class="toolTipImg" title="Note::Setup a time in future." src="../graphics/question.gif" /></div><fmt:message key="scheduleToSend" /> :</div>
		  	<div class="listp">
		  	<!-- input field -->
	   			<form:input path="scheduleTime" size="18" maxlength="20" readonly="true"/><img id="time_start_trigger" class="calendarImage" src="../graphics/calendarIcon.jpg" alt="Pick a date" />
	   			<div align="left" class="button" >
	   			  <c:if test="${!scheduleDone and form.numEmails > 0}" >
	   			    <input type="submit" name="__send" value="<fmt:message key="schedule" />" onclick="return checkMessage(2)" />
	   			  </c:if>    
	   			</div>
	            <form:errors path="scheduleTime" cssClass="error" delimiter=", "/>
	            <script language="JavaScript"> 
			    <!--
					Calendar.setup({
					      inputField     :    "scheduleTime",   // id of the input field
					      showsTime      :    true,
					      ifFormat       :    "%m/%d/%Y[%I:%M %p]",   // format of the input field
					      button         :    "time_start_trigger" // trigger for the calendar (button ID)
					  });
				//-->
				</script>
		    <!-- end input field -->   	
		  	</div>
		  	</div> 
  	
	<!-- end tab -->        
	</div> 
	

<!-- end tabs -->			
</div>

<!-- start button -->
<div align="left" class="button">
  <c:if test="${!scheduleDone and form.numEmails > 0}" >
    <input type="submit" name="__sendnow" value="<fmt:message key="send" /> Now" onclick="return checkMessage(1)" />
  </c:if>
  <c:if test="${scheduleDone or form.numEmails < 1}" >
    <input type="submit" name="__back" value="<fmt:message key="back" />" />
  </c:if>
</div>
<!-- end button -->	     
  
  <!-- end table -->
    </td><td class="boxmidr" ></td></tr>
  <tr><td class="botl"></td><td class="botr"></td></tr>
  </table>
  
<!-- end main box -->  
</div>
</form:form>	
  			  	

<script language="JavaScript"> 
<!--
function trimString(str){
    var returnVal = "";
    for(var i = 0; i < str.length; i++){
      if(str.charAt(i) != ' '){
        returnVal += str.charAt(i)
      }
    }
    return returnVal;
}
function chooseMessage(el) {
	document.getElementById("messageId").value=el.value;
   	messageList = document.getElementsByName('message');
   	var htmlEditorLinks = $$('#preLoadMessage div.htmlEditorLink');
   	    htmlEditorLinks.each(function(el) {
			$(el).setStyles({'display': 'none'});
	});
   	var iFrames = $$('iframe');
   		iFrames.each(function(el) {
			$(el).setStyles({'display': 'none'});
	});
   	
	for(var i=0; i<messageList.length; i++) {
		messageList[i].disabled=true;
		messageList[i].style.display="none";
	}
   	subjectList = document.getElementsByName('subject');
	for(var i=0; i<subjectList.length; i++) {
		subjectList[i].disabled=true;
		subjectList[i].style.display="none";
	}
   	document.getElementById('message'+el.value).disabled=false;
       document.getElementById('message'+el.value).style.display="block"; 
   	document.getElementById('subject'+el.value).disabled=false;
       document.getElementById('subject'+el.value).style.display="block"; 
    if (document.getElementById('htmlID'+el.value).value == 'true') {
    	$('html1').checked = true;
    	$('htmlEditor_link'+el.value).setStyles({'display': 'block'});
	} else {
	    $('html1').checked = false;
	}              
}		
function checkMessage(type) {
    if (document.getElementById('send').value == 'true') {
      var from = document.getElementById('from');
      var message = document.getElementById('message' + document.getElementById('messageSelected').value);
      var subject = document.getElementById('subject' + document.getElementById('messageSelected').value);
      if (trimString(from.value) == "") {
          alert("Please put a from email address");
            from.focus();
        	return false;
          }
      if (trimString(subject.value) == "") {
        alert("Please put a subject");
        subject.focus();
    	return false;
      }
      if (trimString(message.value) == "") {
        alert("Please put a message");
        message.focus();
    	return false;
      }
      
    }
    if ( type == 1)
    {
      return confirm("Are you sure to send email(s) now?");
    } else {
      return confirm("Are you sure to send email(s) on the provided schedule time?");
    }
    
}
function loadEditor(el,id) {
  var oFCKeditor = new FCKeditor(el);
  oFCKeditor.BasePath = "<c:url value="/FCKeditor/"/>";
  oFCKeditor.Config["CustomConfigurationsPath"] = '<c:url value="/admin/javascript/fckconfig.js"/>';
  oFCKeditor.ToolbarSet = 'Html';
  oFCKeditor.Width = 650;
  oFCKeditor.Height = 400;
  oFCKeditor.ReplaceTextarea();
  document.getElementById('htmlEditor_link' + id).style.display="none";
}  
function showHtmlEditor(el) {
  if ( $('messageSelected').value == '') {
    if ( el.checked) {
    	$('htmlEditor_link').setStyles({'display': 'block'});
    } else {
        $('htmlEditor_link').setStyles({'display': 'none'});
    }
  }  else {
    //
  } 
  
}
//-->
</script>
</sec:authorize>
   
  </tiles:putAttribute>    
</tiles:insertDefinition>