<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="/WEB-INF/tlds/custom.tld" prefix="wj" %>
<%@ taglib uri="http://www.springframework.org/security/tags" prefix="sec" %>

<c:if test="${gSiteConfig['gPAYMENTS']}">
<tiles:insertDefinition name="admin.customers" flush="true">
  <tiles:putAttribute name="content" type="string">

<script type="text/javascript" src="../../admin/javascript/multiple.open.accordion1.2.js"></script>
<script language="JavaScript">
<!--
function toggleAll(el) {
	var ids = document.getElementsByName("__selected_paymentId");	
  	if (ids.length == 1)
      document.list.__selected_paymentId.checked = el.checked;
    else
      for (var i = 0; i < ids.length; i++)
        document.list.__selected_paymentId[i].checked = el.checked;	
} 
function cancelSelected() {
	var ids = document.getElementsByName("__selected_paymentId");	
  	if (ids.length == 1) {
      if (document.list.__selected_paymentId.checked) {
    	return confirm('Are you sure you want to cancle selected payments?');
      }
    } else {
      for (var i = 0; i < ids.length; i++) {
        if (document.list.__selected_paymentId[i].checked) {
    	  return confirm('Are you sure you want to cancle selected payments?');
        }
      }
    }

    alert("Please select payment(s) to cancel.");       
    return false;
}

//-->
</script>

<form action="cancelPayments.jhtm" id="list" method="post" name="list">
<input type="hidden" id="orderId" name="orderId" value="${model.orderId}" />
<c:set value="${model.orderId}" var="orderId"/>
<!-- main box -->
  <div id="mboxfull">
  
  <!-- start table -->
  <table cellpadding="0" cellspacing="0" border="0" class="module" >
  <tr>
    <td class="topl_g">
	  
	  <!-- breadcrumb -->
	  <p class="breadcrumb">
	    <a href="../customers"><fmt:message key="customer"/></a> &gt;
	    <fmt:message key="invoice"/> #
	    <c:if test="${orderId != null}"> <a href="../orders/invoice.jhtm?order=${orderId}" class="nameLink">${orderId}</a></c:if> &gt; 
	    <fmt:message key="payment"/> <fmt:message key="cancel"/>
	  </p>
	  
	  <!-- Error Message -->
	  
	  <!-- header image -->
	  <img class="headerImage" src="../graphics/payment.gif" />
	  
	  </td><td class="topr_g" ></td></tr>
  <tr><td class="boxmidlrg" >

 <!-- tabs -->
 <div class="tab-wrapper">
	<h4 title="List of affiliates"></h4>
	<div>
	<!-- start tab -->
	<p>
	  	<div class="listdivi ln tabdivi"></div>
	  	<div class="listdivi"></div>
	   
		  	<div class="listlight">
		  	<!-- input field --> 
		  	
		  	<table border="0" cellpadding="0" cellspacing="1" width="100%">
			  <tr>
			  <td class="pageShowing">
			  <c:if test="${model.paymentOrdersList.nrOfElements > 0}">
			  <fmt:message key="showing">
				<fmt:param value="${model.paymentOrdersList.firstElementOnPage + 1}"/>
				<fmt:param value="${model.paymentOrdersList.lastElementOnPage + 1}"/>
				<fmt:param value="${model.paymentOrdersList.nrOfElements}"/>
			  </fmt:message>
			  </c:if>
			  </td>
			  <td class="pageNavi">
			  Page 
			  <select name="page" id="page" onchange="submit()">
			  <c:forEach begin="1" end="${model.paymentOrdersList.pageCount}" var="page">
			  	<option value="${page}" <c:if test="${page == (model.paymentOrdersList.page+1)}">selected</c:if>>${page}</option>
			  </c:forEach>
			  </select>
			  of <c:out value="${model.paymentOrdersList.pageCount}"/>
			  | 
			  <c:if test="${model.paymentOrdersList.firstPage}"><span class="pageNaviDead"><fmt:message key="previous" /></span></c:if>
			  <c:if test="${not model.paymentOrdersList.firstPage}"><a href="<c:url value="cancelPayments.jhtm"><c:param name="page" value="${model.paymentOrdersList.page}"/></c:url>" class="pageNaviLink"><fmt:message key="previous" /></a></c:if>
			  | 
			  <c:if test="${model.paymentOrdersList.lastPage}"><span class="pageNaviDead"><fmt:message key="next" /></span></c:if>
			  <c:if test="${not model.paymentOrdersList.lastPage}"><a href="<c:url value="cancelPayments.jhtm"><c:param name="page" value="${model.paymentOrdersList.page+2}"/></c:url>" class="pageNaviLink"><fmt:message key="next" /></a></c:if>
			  </td>
			  </tr>
			</table>
			
			<table border="0" cellpadding="0" cellspacing="0" width="100%" class="listings">
			  <tr class="listingsHdr2">
			  <td class="indexCol">&nbsp;</td>
			    <td class="listingsHdr3"><input type="checkbox"  onClick="toggleAll(this)"></td>
				<td class="listingsHdr3">
					<fmt:message key="memo" />
				</td>
				<td class="listingsHdr3">
					<fmt:message key="payment" /> <fmt:message key="date" />
				</td>
				<td class="listingsHdr3">
					<fmt:message key="order" />
				</td>
				<td class="listingsHdr3">
					<fmt:message key="payment" /> <fmt:message key="amount" />
				</td>
				<td class="listingsHdr3">
					<fmt:message key="cancelled" /> <fmt:message key="amount" />
				</td>
				<td class="listingsHdr3">
					<fmt:message key="cancelled" /> <fmt:message key="by" />
				</td>
			  </tr>
			  <c:forEach items="${model.paymentOrdersList.pageList}" var="payment" varStatus="status">
			  <tr class="row${status.index % 2}" onmouseover="changeStyleClass(this,'rowOver')" onmouseout="changeStyleClass(this,'row${status.index % 2}')">
			    <td class="indexCol"><c:out value="${status.count + model.paymentOrdersList.firstElementOnPage}"/>.</td>
			    <td align="left">			    
			    <c:choose>
				    <c:when test="${payment.cancelPayment == 'true'}">
				    	<input value="${payment.id}" type="checkbox" disabled="disabled">
				    </c:when>
				    <c:otherwise>
				    	<input name="__selected_paymentId" value="${payment.id}" type="checkbox">
				    </c:otherwise>
			    </c:choose>
			    </td>
			    <td class="nameCol"><a href="payment.jhtm?id=${payment.id}" class="nameLink"><c:if test="${payment.paymentMethod != ''}"><c:out value="${payment.paymentMethod}"/> </c:if><c:out value="${payment.memo}"/></a></td>
				<td class="nameCol"><fmt:formatDate type="date" timeStyle="default" value="${payment.date}"/></td>
				<td class="nameCol"><c:out value="${payment.orderId}"/></td>
				<td class="nameCol"><fmt:formatNumber value="${payment.amount}" pattern="#,##0.00" /></td>
				<td class="nameCol"><fmt:formatNumber value="${payment.cancelledAmt}" pattern="#,##0.00" /></td>
				<td class="nameCol"><c:out value="${payment.cancelledBy}"/></td>
			  </tr>
			  </c:forEach>
			<c:if test="${model.paymentOrdersList.nrOfElements == 0}">
			  <tr class="emptyList"><td colspan="6">&nbsp;</td></tr>
			</c:if>
			</table>
			<table border="0" cellpadding="0" cellspacing="1" width="100%">
			  <tr>
			  <td class="pageSize">
			  <select name="size" onchange="document.getElementById('page').value=1;submit()">
			    <c:forTokens items="10,25,50,100" delims="," var="current">
			  	  <option value="${current}" <c:if test="${current == model.paymentOrdersList.pageSize}">selected</c:if>>${current} <fmt:message key="perPage" /></option>
			    </c:forTokens>
			  </select>
			  </td>
			  </tr>
			</table>
		  	<!-- end input field -->  	  
		  	</div>
		  	
		  	<!-- start button -->
		  	<div align="left" class="button">
		  	<sec:authorize ifAnyGranted="ROLE_AEM,ROLE_ADMIN_${prop_site_id},ROLE_INCOMING_PAYMENT_CANCEL">  
			    <input type="submit" name="__cancel" value="Cancel Selected Payments" onClick="return cancelSelected()" />
			</sec:authorize>
		  	</div>
		  	<!-- end button -->	
	  	
	<!-- end tab -->        
	</div>
<!-- end tabs -->			
</div>
  
  <!-- end table -->
   </td><td class="boxmidr" ></td></tr>
  <tr><td class="botl"></td><td class="botr"></td></tr>
  </table>
  
<!-- end main box -->  
</div>
</form>	   

  </tiles:putAttribute>    
</tiles:insertDefinition>
</c:if>