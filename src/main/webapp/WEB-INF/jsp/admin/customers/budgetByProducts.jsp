<%@ page import="java.net.*" %>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="/WEB-INF/tlds/custom.tld" prefix="wj" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>

<c:if test="${gSiteConfig['gBUDGET_PRODUCT']}">
<tiles:insertDefinition name="admin.customers" flush="true">
  <tiles:putAttribute name="content" type="string">
   
<form id="list" method="post">
<input type="hidden" id="sort" name="sort" value="${budgetProductSearch.sort}" />
<!-- main box -->
  <div id="mboxfull">
  
  <!-- start table -->
  <table cellpadding="0" cellspacing="0" border="0" class="module" >
  <tr>
    <td class="topl_g">
	  
	  <!-- breadcrumb -->
	  <p class="breadcrumb">
	    <a href="../customers/customerList.jhtm">Customer</a> &gt;
	    Budget &gt; <c:out value="${model.customer.username}"/>
	  </p>
	  
	  <!-- Error Message -->
	  
	  <!-- header image -->
	  <img class="headerImage" src="../graphics/payment.gif" />
	  
	  </td><td class="topr_g" ></td></tr>
  <tr><td class="boxmidlrg" >

 <!-- tabs -->
 <div class="tab-wrapper">
	<h4 title="List of affiliates"></h4>
	<div>
	<!-- start tab -->

	  	<div class="listdivi ln tabdivi"></div>
	  	<div class="listdivi"></div>
	   
		  	<div class="listlight">
		  	<!-- input field --> 
		  	<table border="0" cellpadding="0" cellspacing="1" width="100%">
			  <tr>
			  <td class="pageShowing">
			  <c:if test="${model.list.nrOfElements > 0}">
			  <fmt:message key="showing">
				<fmt:param value="${model.list.firstElementOnPage + 1}"/>
				<fmt:param value="${model.list.lastElementOnPage + 1}"/>
				<fmt:param value="${model.list.nrOfElements}"/>
			  </fmt:message>
			  </c:if>
			  </td>
			  <td class="pageShowing" align="center">
			   Year: 
			   <select name="year" onchange="document.getElementById('page').value=1;submit()">
				<c:forEach items="${model.years}" var="year">
        		  <option value="${year}" <c:if test="${budgetProductSearch.year == year}">selected</c:if>><c:out value="${year}"/></option>
    			</c:forEach>
			   </select>			  
			  </td>
			  <td class="pageNavi">
			  Page 
			  <select name="page" id="page" onchange="submit()">
			  <c:forEach begin="1" end="${model.list.pageCount}" var="page">
			  	<option value="${page}" <c:if test="${page == (model.list.page+1)}">selected</c:if>>${page}</option>
			  </c:forEach>
			  </select>
			  of <c:out value="${model.list.pageCount}"/>
			  | 
			  <c:if test="${model.list.firstPage}"><span class="pageNaviDead"><fmt:message key="previous" /></span></c:if>
			  <c:if test="${not model.list.firstPage}"><a href="#" onClick="document.getElementById('page').value='${model.list.page}';document.getElementById('list').submit()" class="pageNaviLink"><fmt:message key="previous" /></a></c:if>
			  | 
			  <c:if test="${model.list.lastPage}"><span class="pageNaviDead"><fmt:message key="next" /></span></c:if>
			  <c:if test="${not model.list.lastPage}"><a href="#" onClick="document.getElementById('page').value='${model.list.page+2}';document.getElementById('list').submit()" class="pageNaviLink"><fmt:message key="next" /></a></c:if>
			  </td>
			  </tr>
			</table>
			
			<table border="0" cellpadding="0" cellspacing="0" width="100%" class="listings">
			  <tr class="listingsHdr2">
			    <td class="indexCol">&nbsp;</td>
			    <td class="nameCol">
			    	<table cellspacing="0" cellpadding="1">
			    	  <tr>
			    	    <c:choose>
			    	      <c:when test="${budgetProductSearch.sort == 'sku desc'}">
			    	        <td class="listingsHdr3">
			    	        <a class="hdrLink" href="#" onClick="document.getElementById('sort').value='sku';document.getElementById('list').submit()"><fmt:message key="sku" /></a>
			    	        </td>
			    	        <td><img src="../graphics/up.gif" border="0"></td>
			    	      </c:when>
			    	      <c:when test="${budgetProductSearch.sort == 'sku'}">
			    	        <td class="listingsHdr3">
			    	        <a class="hdrLink" href="#" onClick="document.getElementById('sort').value='sku desc';document.getElementById('list').submit()"><fmt:message key="sku" /></a>
			    	        </td>
			    	        <td><img src="../graphics/down.gif" border="0"></td>
			    	      </c:when>
			    	      <c:otherwise>
			    	        <td rowspan="2" colspan="2" class="listingsHdr3">
			    	        <a class="hdrLink" href="#" onClick="document.getElementById('sort').value='sku';document.getElementById('list').submit()"><fmt:message key="sku" /></a>
			    	        </td>
			    	      </c:otherwise>
			    	    </c:choose>
			    	  </tr>
			    	</table>
			    </td>
			    <td class="nameCol">
			    	<table cellspacing="0" cellpadding="1">
			    	  <tr>
			    	    <c:choose>
			    	      <c:when test="${budgetProductSearch.sort == 'name desc'}">
			    	        <td class="listingsHdr3">
			    	        <a class="hdrLink" href="#" onClick="document.getElementById('sort').value='name';document.getElementById('list').submit()"><fmt:message key="productName" /></a>
			    	        </td>
			    	        <td><img src="../graphics/up.gif" border="0"></td>
			    	      </c:when>
			    	      <c:when test="${budgetProductSearch.sort == 'name'}">
			    	        <td class="listingsHdr3">
			    	        <a class="hdrLink" href="#" onClick="document.getElementById('sort').value='name desc';document.getElementById('list').submit()"><fmt:message key="productName" /></a>
			    	        </td>
			    	        <td><img src="../graphics/down.gif" border="0"></td>
			    	      </c:when>
			    	      <c:otherwise>
			    	        <td rowspan="2" colspan="2" class="listingsHdr3">
			    	        <a class="hdrLink" href="#" onClick="document.getElementById('sort').value='name';document.getElementById('list').submit()"><fmt:message key="productName" /></a>
			    	        </td>
			    	      </c:otherwise>
			    	    </c:choose>
			    	  </tr>
			    	</table>
			    </td>
			    <c:choose>
			    <c:when test="${empty budgetProductSearch.sku}">			    
			    <td align="center">
			    	<table cellspacing="0" cellpadding="1">
			    	  <tr>
			    	    <c:choose>
			    	      <c:when test="${budgetProductSearch.sort == 'qtyTotal DESC'}">
			    	        <td class="listingsHdr3">
			    	        <a class="hdrLink" href="#" onClick="document.getElementById('sort').value='qtyTotal';document.getElementById('list').submit()"><fmt:message key="total" /></a>
			    	        </td>
			    	        <td><img src="../graphics/up.gif" border="0"></td>
			    	      </c:when>
			    	      <c:when test="${budgetProductSearch.sort == 'qtyTotal'}">
			    	        <td class="listingsHdr3">
			    	        <a class="hdrLink" href="#" onClick="document.getElementById('sort').value='qtyTotal DESC';document.getElementById('list').submit()"><fmt:message key="total" /></a>
			    	        </td>
			    	        <td><img src="../graphics/down.gif" border="0"></td>
			    	      </c:when>
			    	      <c:otherwise>
			    	        <td rowspan="2" colspan="2" class="listingsHdr3">
			    	        <a class="hdrLink" href="#" onClick="document.getElementById('sort').value='qtyTotal';document.getElementById('list').submit()"><fmt:message key="total" /></a>
			    	        </td>
			    	      </c:otherwise>
			    	    </c:choose>
			    	  </tr>
			    	</table>
			    </td>
			    <td align="center">
			    	<table cellspacing="0" cellpadding="1">
			    	  <tr>
			    	    <c:choose>
			    	      <c:when test="${budgetProductSearch.sort == 'ordered DESC'}">
			    	        <td class="listingsHdr3">
			    	        <a class="hdrLink" href="#" onClick="document.getElementById('sort').value='qtyTotal';document.getElementById('list').submit()">Ordered</a>
			    	        </td>
			    	        <td><img src="../graphics/up.gif" border="0"></td>
			    	      </c:when>
			    	      <c:when test="${budgetProductSearch.sort == 'ordered'}">
			    	        <td class="listingsHdr3">
			    	        <a class="hdrLink" href="#" onClick="document.getElementById('sort').value='ordered DESC';document.getElementById('list').submit()">Ordered</a>
			    	        </td>
			    	        <td><img src="../graphics/down.gif" border="0"></td>
			    	      </c:when>
			    	      <c:otherwise>
			    	        <td rowspan="2" colspan="2" class="listingsHdr3">
			    	        <a class="hdrLink" href="#" onClick="document.getElementById('sort').value='ordered';document.getElementById('list').submit()">Ordered</a>
			    	        </td>
			    	      </c:otherwise>
			    	    </c:choose>
			    	  </tr>
			    	</table>
			    </td>
				<td class="listingsHdr3" align="center">
					<fmt:message key="balance" />
				</td>			     
			    </c:when>
			    <c:otherwise>
			    <td align="center">
			    	<table cellspacing="0" cellpadding="1">
			    	  <tr>
			    	    <c:choose>
			    	      <c:when test="${budgetProductSearch.sort == 'quantity DESC'}">
			    	        <td class="listingsHdr3">
			    	        <a class="hdrLink" href="#" onClick="document.getElementById('sort').value='quantity';document.getElementById('list').submit()"><fmt:message key="qty" /></a>
			    	        </td>
			    	        <td><img src="../graphics/up.gif" border="0"></td>
			    	      </c:when>
			    	      <c:when test="${budgetProductSearch.sort == 'quantity'}">
			    	        <td class="listingsHdr3">
			    	        <a class="hdrLink" href="#" onClick="document.getElementById('sort').value='quantity DESC';document.getElementById('list').submit()"><fmt:message key="qty" /></a>
			    	        </td>
			    	        <td><img src="../graphics/down.gif" border="0"></td>
			    	      </c:when>
			    	      <c:otherwise>
			    	        <td rowspan="2" colspan="2" class="listingsHdr3">
			    	        <a class="hdrLink" href="#" onClick="document.getElementById('sort').value='quantity';document.getElementById('list').submit()"><fmt:message key="qty" /></a>
			    	        </td>
			    	      </c:otherwise>
			    	    </c:choose>
			    	  </tr>
			    	</table>
			    </td>
			    <td align="center">
			    	<table cellspacing="0" cellpadding="1">
			    	  <tr>
			    	    <c:choose>
			    	      <c:when test="${budgetProductSearch.sort == 'date_entered DESC'}">
			    	        <td class="listingsHdr3">
			    	        <a class="hdrLink" href="#" onClick="document.getElementById('sort').value='date_entered';document.getElementById('list').submit()"><fmt:message key="dateEntered" /></a>
			    	        </td>
			    	        <td><img src="../graphics/up.gif" border="0"></td>
			    	      </c:when>
			    	      <c:when test="${budgetProductSearch.sort == 'date_entered'}">
			    	        <td class="listingsHdr3">
			    	        <a class="hdrLink" href="#" onClick="document.getElementById('sort').value='date_entered DESC';document.getElementById('list').submit()"><fmt:message key="dateEntered" /></a>
			    	        </td>
			    	        <td><img src="../graphics/down.gif" border="0"></td>
			    	      </c:when>
			    	      <c:otherwise>
			    	        <td rowspan="2" colspan="2" class="listingsHdr3">
			    	        <a class="hdrLink" href="#" onClick="document.getElementById('sort').value='date_entered';document.getElementById('list').submit()"><fmt:message key="dateEntered" /></a>
			    	        </td>
			    	      </c:otherwise>
			    	    </c:choose>
			    	  </tr>
			    	</table>
			    </td>
			    </c:otherwise>
			    </c:choose>			    
			  </tr>
			<c:forEach items="${model.list.pageList}" var="budgetProduct" varStatus="status">
			  <tr class="row${status.index % 2}" onmouseover="changeStyleClass(this,'rowOver')" onmouseout="changeStyleClass(this,'row${status.index % 2}')">
			    <td class="indexCol"><c:out value="${status.count + model.list.firstElementOnPage}"/>.</td>
			    <c:if test="${budgetProduct.id == null}">
			    <c:set var="productSKU" value="${budgetProduct.product.sku}" />
			    <%-- <td class="nameCol"><a href="budgetByProducts.jhtm?cid=${budgetProduct.userId}&sku=<%out.print(URLEncoder.encode((String) pageContext.getAttribute("productSKU"), "UTF-8"));%>" class="nameLink"><c:out value="${budgetProduct.product.sku}"/></a></td> --%>
				<td class="nameCol"><a href="budgetByProducts.jhtm?cid=${budgetProduct.userId}&sku=${wj:urlEncode(productSKU)}" class="nameLink"><c:out value="${budgetProduct.product.sku}"/></a></td>
				</c:if>		    
			    <c:if test="${budgetProduct.id != null}">
			    <td class="nameCol"><a href="budgetByProduct.jhtm?cid=${budgetProduct.userId}&id=${budgetProduct.id}" class="nameLink"><c:out value="${budgetProduct.product.sku}"/></a></td>
				</c:if>		    
			    <td class="nameCol"><c:out value="${budgetProduct.product.name}"/></td>
			    <c:choose>
			    <c:when test="${empty budgetProductSearch.sku}">
			    <td align="center"><c:out value="${budgetProduct.qtyTotal}"/></td>
			    <td align="center"><c:out value="${budgetProduct.ordered}"/></td>
			    <td align="center"><c:out value="${budgetProduct.balance}"/></td>
			    </c:when>
			    <c:otherwise>
			    <td align="center"><c:out value="${budgetProduct.qty}"/></td>
			    <td align="center"><fmt:formatDate type="date" timeStyle="default" value="${budgetProduct.entered}"/></td>
			    </c:otherwise>
			    </c:choose>
			  </tr>
			</c:forEach>
			<c:if test="${model.list.nrOfElements == 0}">
			  <tr class="emptyList"><td colspan="6">&nbsp;</td></tr>
			</c:if>
			</table>
				  
			<table border="0" cellpadding="0" cellspacing="1" width="100%">
			  <tr>
			  <td class="pageSize">
			  <select name="size" onchange="document.getElementById('page').value=1;submit()">
			    <c:forTokens items="10,25,50,100" delims="," var="current">
			  	  <option value="${current}" <c:if test="${current == model.list.pageSize}">selected</c:if>>${current} <fmt:message key="perPage" /></option>
			    </c:forTokens>
			  </select>
			  </td>
			  </tr>
			</table>	  
		  	<!-- end input field -->  	  
		  	</div>
		  	
		  	<!-- start button -->
		  	<c:if test="${budgetProductSearch.userId != null}">
			    <input type="hidden" name="cid" value="${budgetProductSearch.userId}">
			</c:if>     
		  	<div align="left" class="button">
		  	  <c:if test="${budgetProductSearch.userId != null}">
			    <input type="submit" name="__add" value="<fmt:message key="add" /> <fmt:message key="budget" />">
			  </c:if>
			  <c:if test="${budgetProductSearch.userId == null}">
			    <div class="sup">* To add budget go to customer list.</div>
			  </c:if>
		  	</div>
		  	<!-- end button -->	
	  	
	<!-- end tab -->        
	</div>
<!-- end tabs -->			
</div>
  
  <!-- end table -->
   </td><td class="boxmidr" ></td></tr>
  <tr><td class="botl"></td><td class="botr"></td></tr>
  </table>
  
<!-- end main box -->  
</div>
</form>	   

  </tiles:putAttribute>    
</tiles:insertDefinition>
</c:if>
