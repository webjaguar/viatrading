<%@ page import="com.webjaguar.listener.SessionListener,java.util.*,com.webjaguar.model.Customer" %>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/security/tags" prefix="sec" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ taglib uri="/WEB-INF/tlds/custom.tld" prefix="wj" %>

<tiles:insertDefinition name="admin.customers" flush="true">
  <tiles:putAttribute name="content" type="string">
  
<sec:authorize ifAnyGranted="ROLE_AEM,ROLE_ADMIN_${prop_site_id},ROLE_CUSTOMER_VIEW_LIST">  
<c:if test="${model.count > 0}">
<script type="text/javascript" src="../../admin/javascript/multiple.open.accordion1.2.js"></script>
<script type="text/javascript">
<!--
	var box2 = {};
	window.addEvent('domready', function(){
		$$('img.action').each(function(img){
			//containers
			var actionList = img.getParent(); 
			var actionHover = actionList.getElements('div.action-hover')[0];
			actionHover.set('opacity',0);
			//show/hide
			img.addEvent('mouseenter',function() {
				actionHover.setStyle('display','block').fade('in');
			});
			actionHover.addEvent('mouseleave',function(){
				actionHover.fade('out');
			});
			actionList.addEvent('mouseleave',function() {
				actionHover.fade('out');
			});
		});
		var addCreditBox = new multiBox('mbAddCredit', {showControls : false, useOverlay: false, showNumbers: false });
		var box2 = new multiBox('mbCustomer', {descClassName: 'multiBoxDesc',waitDuration: 5,showNumbers: true,showControls: false,overlay: new overlay()});
		var box3 = new multiBox('mbRegisterEvent', {descClassName: 'multiBoxDesc',descMaxWidth: 400,openFromLink: false,showNumbers: false,showControls: false,useOverlay: false});
		var Tips1 = new Tips('.toolTipImg');  
		// Create the accordian
		var myAccordion = new Fx.Accordion($$('div.arrow_drop'), $$('div.information'), {
			display: 1, alwaysHide: true,
	    	onActive: function() {$('information').removeClass('displayNone');}
		});
	<sec:authorize ifAnyGranted="ROLE_AEM,ROLE_ADMIN_${prop_site_id},ROLE_QUICK_EXPORT">  
	$('quickModeTrigger').addEvent('click',function() {
	    $('mboxfull').setStyle('margin', '0');
	    $('OverlayContainer').hide();
	    $('subMenusContainer').hide();
	    $$('.quickMode').each(function(el){
	    	el.hide();
	    });
	    $$('.quickModeRemove').each(function(el){
	    	el.destroy();
	    });
	    $$('.listingsHdr3').each(function(el){
	    	el.removeProperties('colspan', 'class');
	    });
	    alert('Select the whole page, copy and paste to your excel file.');
	  });
	  </sec:authorize> 
	});	
	
function creditValue(credit,userId) {
	document.getElementById('credit_'+userId).value = credit;
}
		
function addCredit(userId) {
	var addCredit = $('credit_'+userId).value;
	var request = new Request({
		url: "../orders/customer-ajax-credit.jhtm?userId="+userId+"&addCredit="+addCredit,
		method: 'post',
		onComplete: function(response) { 
			location.reload(true);
		}
	}).send();
	
}
function deductCredit(userId) {
	var deductCredit = $('credit_'+userId).value;
	var request = new Request({
		url: "../orders/customer-ajax-credit.jhtm?userId="+userId+"&deductCredit="+deductCredit,
		method: 'post',
		onComplete: function(response) { 
			location.reload(true);
		}
	}).send();
}
function toggleAll(el) {
	var ids = document.getElementsByName("__selected_id");	
  	if (ids.length == 1)
      document.list_form.__selected_id.checked = el.checked;
    else
      for (i = 0; i < ids.length; i++)
        document.list_form.__selected_id[i].checked = el.checked;	
} 
function optionsSelected() {
	var ids = document.getElementsByName("__selected_id");
  	if (ids.length == 1) {
      if (document.list_form.__selected_id.checked) {
    	return true;
      }
    } else {
      for (i = 0; i < ids.length; i++) {
        if (document.list_form.__selected_id[i].checked) {
    	  return true;
        }
      }
    }
  	if (document.list_form.__groupAssignAll.checked) {
  	   return true;
    }
    alert("Please select customer(s).");       
    return false;
} 
function optionsSelectedOnlyOneCustomer() {
	var ids = document.getElementsByName("__selected_id");
  	if (ids.length == 1) {
      if (document.list_form.__selected_id.checked) {
    	return true;
      }
    } else if(ids.length > 1) {
    	var count = 0;
    	for (i = 0; i < ids.length; i++) {
            if (document.list_form.__selected_id[i].checked) {
        	  count=count+1;
            }
          }
    	if(count > 1){
      		  alert("Only one customer allowed. ");
      		  return false;
    	}else if(count == 0){
    		alert("Please select one customer.");       
    	    return false;
    	}else{
    		return true;
    	} 
    }
    alert("Please select one customer.");       
    return false;
}
function echoSignSelected() {
	var ids = document.getElementsByName("__selected_id");	
  	if (ids.length == 1) {
      if (document.list_form.__selected_id.checked) {
		document.list_form.action = "../echoSign/sendDocumentMegaSign.jhtm";
      	return true;
      }
    } else {
      for (i = 0; i < ids.length; i++) {
        if (document.list_form.__selected_id[i].checked) {
        	document.list_form.action = "../echoSign/sendDocumentMegaSign.jhtm";
          	return true;
        }
      }
    }

    alert("Please select customers(s) to EchoSign.");       
    return false;
}
//-->
</script>  
</c:if>
<script type="text/javascript">
function confirmCustomer(id) {
	if (window.XMLHttpRequest) {
	    xmlhttp = new XMLHttpRequest();
	} else {
	    xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
	}
	xmlhttp.onreadystatechange = function() {
	    if (xmlhttp.readyState == 4 && xmlhttp.status == 200) {
	    	if(xmlhttp.responseText != null && xmlhttp.responseText.length > 0){
	    		var resultText = xmlhttp.responseText;
	    		var r = confirm(resultText);
	    		
	    		if (r==true) {
	    			window.location = "../orders/addInvoice.jhtm?cid="+id;
	    		}
	    	}else{
	    		window.location = "../orders/addInvoice.jhtm?cid="+id;
	    	}
	    }
	}
	xmlhttp.open("GET","https://www.viatrading.com/dv/liveapps/customerordershipmentcheck.php?cid="+id,true);
	xmlhttp.send();
}	
</script>
<form action="customerList.jhtm" method="post" id="list" name="list_form">
<input type="hidden" id="cid" name="cid"> 
<input type="hidden" id="sort" name="sort" value="${customerSearch.sort}" />
<!-- main box -->
  <div id="mboxfull">
  
  <!-- start table -->
  <table cellpadding="0" cellspacing="0" border="0" class="module" >
  <tr class="quickMode">
    <td class="topl_g">
	
	
	  <!-- breadcrumb -->
	  <p class="breadcrumb">
	    <a href="../customers">Customer</a> &gt;
	    customers 
	  </p>
  
      <!-- Error Message -->
      
      <!-- header image -->
      <div>
      <div><img id="quickModeTrigger" class="headerImage" src="../graphics/customer.gif" /></div>
      <form name="searchform" action="customerList.jhtm" method="post" >
	   <div class="search2" style="float : left">
	   
          <p><fmt:message key="multipleSearch" />:</p>
          <input name="multipleSearch" type="text" value="<c:out value='${customerSearch.multipleSearchString}' />" size="15" />
          <input type="submit" value="Search"/>
        </div>
      </form>
        <div style="clear: both;"></div>
    </div>
      <!-- Option -->
      <c:if test="${model.count > 0}">
	    <div class="optionBox"><div class="titlePrivilege"><fmt:message key="options" /></div></div>
		<div class="arrow_drop arrowImageRight"></div>
		<div style="clear: both;"></div>
		<div class="information displayNone" id="information" style="float:left;">  
		     <c:if test="${gSiteConfig['gGROUP_CUSTOMER']}">  
			 <div style="float:left;padding: 5px">
	           <table class="productBatchTool" cellpadding="5" cellspacing="5">
	           <tr>
	            <td valign="top">
	             <table cellpadding="1" cellspacing="1" width="100%">
	              <tr style="height:30px;white-space: nowrap">
	               <td valign="top" width="20px"><img class="toolTipImg" title="Group Id::Associate group Id to the following customer(s) in the list. Can be separated by comma." src="../graphics/question.gif" /></td>
	               <td valign="top"><h3><fmt:message key="groupId" /></h3></td>
	               <td valign="top" align="right"><input type="checkbox" onclick="toggleAll(this)" name="__groupAssignAll" value="true"/> <fmt:message key="all"/></td>
	              </tr>
	             </table>
	            </td>
	           </tr>
	           <tr>
	            <td valign="top">
	             <table cellpadding="1" cellspacing="1">
	              <tr style="">
			       <td>
			        <input name="__group_id" type="text" size="15"/>
			        <div align="left" class="buttonLeft">
		            <input type="submit" name="__groupAssignPacher" value="Apply" onClick="return optionsSelected()">
		            </div>
			       </td>
			       <td >
			        <div><p><input name="__batch_type" type="radio" checked="checked" value="add"/>Add</p></div>
				    <div><p><input name="__batch_type" type="radio" value="remove"/>Remove</p></div>
				    <div><p><input name="__batch_type" type="radio" value="move"/>Move</p></div>
			       </td>
			     </tr>
	             </table>
	            </td>
	           </tr>
	           </table>
	         </div>
	         </c:if>
	         <div style="float:left;padding: 5px">
	           <table class="productBatchTool" cellpadding="5" cellspacing="5">
	           <tr>
	            <td valign="top">
	             <table cellpadding="1" cellspacing="1">
	              <tr style="height:30px;white-space: nowrap">
	               <td valign="top" width="20px"><img class="toolTipImg" title="Action ::Implement action to the following customer(s) in the list" src="../graphics/question.gif" /></td>
	               <td valign="top"><h3><fmt:message key="action" /></h3></td>
	              </tr>
	             </table>
	            </td>
	           </tr>
	           <tr>
	            <td valign="top">
	             <table cellpadding="1" cellspacing="1">
	              <tr style="">
			       <td>
			        <div><p><input name="__batch_action" type="radio" checked="checked" value="1"/>Suspend</p></div>
				    <div><p><input name="__batch_action" type="radio" value="0"/>Resume</p></div>
				   </td>
				   <td>
			        <div align="left" class="buttonLeft">
		            <input type="submit" name="__suspendPacher" value="Apply" onClick="return optionsSelected()">
		            </div>
			       </td>
			     </tr>
	             </table>
	            </td>
	           </tr>
	           </table>		 
			</div>
			<sec:authorize ifAnyGranted="ROLE_AEM,ROLE_ADMIN_${prop_site_id},ROLE_CUSTOMER_UPDATE_TRACKCODE">  
			<c:if test="${fn:contains(siteConfig['CUSTOMIZE_CUSTOMER_LIST'].value,'trackCode')}">
			<div style="float:left;padding: 5px">
	           <table class="productBatchTool" cellpadding="5" cellspacing="5">
	           <tr>
	            <td valign="top">
	             <table cellpadding="1" cellspacing="1">
	              <tr style="height:30px;white-space: nowrap">
	               <td valign="top" width="20px"><img class="toolTipImg" title="Update Track Code ::Update track code to the following customer(s) in the list" src="../graphics/question.gif" /></td>
	               <td valign="top"><h3><fmt:message key="trackcode" /></h3></td>
	              </tr>
	             </table>
	             </td>
	           </tr>
	           <tr>
	            <td valign="top">
	             <table cellpadding="1" cellspacing="1">
	              <tr style="">
			       <td>
			        <input name="__track_code" type="text" size="15"/>
			        <div align="left" class="buttonLeft">
		            <input type="submit" name="__updateTrackCodePacher" value="Update" onClick="return optionsSelected()">
		            </div>
			       </td>
			     </tr>
	             </table>
	            </td>
	           </tr>
	           </table>
	         </div>
	         </c:if>
	         </sec:authorize>
	         <c:if test="${gSiteConfig['gREGISTRATION_TOUCH_SCREEN']}">
	    	<div style="float:left;padding: 5px">
	           <table class="productBatchTool" cellpadding="5" cellspacing="5">
	           <tr>
	            <td valign="top">
	             <table cellpadding="1" cellspacing="1">
	              <tr style="height:30px;white-space: nowrap">
	               <td valign="top" width="20px"><img class="toolTipImg" title="Print Sold Label :: Print sold label only allowed by choosing one customer" src="../graphics/question.gif" /></td>
	               <td valign="top"><h3><fmt:message key="printSoldLabel" /></h3></td>
	              </tr>
	             </table>
	             </td>
	           </tr>
	           <tr>
	            <td valign="top">
	             <table cellpadding="1" cellspacing="1">
	              <tr style="">
			       <td>
			        <div align="left" class="buttonLeft">
		            <input type="submit" name="__printSoldLabel" value="Print" onClick="return optionsSelectedOnlyOneCustomer()">
		            </div>
			       </td>
			     </tr>
	             </table>
	            </td>
	           </tr>
	           </table>
	         </div>     
	         </c:if>
	         
	         
	         <div style="float:left;padding: 5px">
	           <table class="productBatchTool" cellpadding="5" cellspacing="5">
	           <tr>
	            <td valign="top">
	             <table cellpadding="1" cellspacing="1">
	              <tr style="height:30px;white-space: nowrap">
	               <td valign="top" width="20px"><img class="toolTipImg" title="Update Main Source ::Update main source to the following customer(s) in the list" src="../graphics/question.gif" /></td>
	               <td valign="top"><h3><fmt:message key="mainSource" /></h3></td>
	              </tr>
	             </table>
	             </td>
	           </tr>
	           <tr>
	            <td valign="top">
	             <table cellpadding="1" cellspacing="1">
	              <tr style="">
			       <td>
			        <input name="__main_source" type="text" size="15"/>
			        <div align="left" class="buttonLeft">
		            <input type="submit" name="__updateMainSourcePacher" value="Update" onClick="return optionsSelected()">
		            </div>
			       </td>
			     </tr>
	             </table>
	            </td>
	           </tr>
	           </table>
	         </div>
	         
	         <div style="float:left;padding: 5px">
	           <table class="productBatchTool" cellpadding="5" cellspacing="5">
	           <tr>
	            <td valign="top">
	             <table cellpadding="1" cellspacing="1">
	              <tr style="height:30px;white-space: nowrap">
	               <td valign="top" width="20px"><img class="toolTipImg" title="Update Paid ::Update paid to the following customer(s) in the list" src="../graphics/question.gif" /></td>
	               <td valign="top"><h3><fmt:message key="paid" /></h3></td>
	              </tr>
	             </table>
	             </td>
	           </tr>
	           <tr>
	            <td valign="top">
	             <table cellpadding="1" cellspacing="1">
	              <tr style="">
			       <td>
			        <input name="__paid" type="text" size="15"/>
			        <div align="left" class="buttonLeft">
		            <input type="submit" name="__updatePaidPacher" value="Update" onClick="return optionsSelected()">
		            </div>
			       </td>
			     </tr>
	             </table>
	            </td>
	           </tr>
	           </table>
	         </div>
	         
	         <div style="float:left;padding: 5px">
	           <table class="productBatchTool" cellpadding="5" cellspacing="5">
	           <tr>
	            <td valign="top">
	             <table cellpadding="1" cellspacing="1">
	              <tr style="height:30px;white-space: nowrap">
	               <td valign="top" width="20px"><img class="toolTipImg" title="Update Medium ::Update medium to the following customer(s) in the list" src="../graphics/question.gif" /></td>
	               <td valign="top"><h3><fmt:message key="medium" /></h3></td>
	              </tr>
	             </table>
	             </td>
	           </tr>
	           <tr>
	            <td valign="top">
	             <table cellpadding="1" cellspacing="1">
	              <tr style="">
			       <td>
			        <input name="__medium" type="text" size="15"/>
			        <div align="left" class="buttonLeft">
		            <input type="submit" name="__updateMediumPacher" value="Update" onClick="return optionsSelected()">
		            </div>
			       </td>
			     </tr>
	             </table>
	            </td>
	           </tr>
	           </table>
	         </div>
	         
	         <div style="float:left;padding: 5px">
	           <table class="productBatchTool" cellpadding="5" cellspacing="5">
	           <tr>
	            <td valign="top">
	             <table cellpadding="1" cellspacing="1">
	              <tr style="height:30px;white-space: nowrap">
	               <td valign="top" width="20px"><img class="toolTipImg" title="Update language ::Update language to the following customer(s) in the list" src="../graphics/question.gif" /></td>
	               <td valign="top"><h3><fmt:message key="language" /></h3></td>
	              </tr>
	             </table>
	             </td>
	           </tr>
	           <tr>
	            <td valign="top">
	             <table cellpadding="1" cellspacing="1">
	              <tr style="">
			       <td>
			        <input name="__language_field" type="text" size="15"/>
			        <div align="left" class="buttonLeft">
		            <input type="submit" name="__updateLanguageFieldPacher" value="Update" onClick="return optionsSelected()">
		            </div>
			       </td>
			     </tr>
	             </table>
	            </td>
	           </tr>
	           </table>
	         </div>
	         
	         <div style="float:left;padding: 5px">
	           <table class="productBatchTool" cellpadding="5" cellspacing="5">
	           <tr>
	            <td valign="top">
	              <table cellpadding="1" cellspacing="1" width="100%">
	              <tr style="height:30px;white-space: nowrap">
	               <td valign="top" width="20px"><img class="toolTipImg" title="Update metrics ::Updates track code, main source, medium, paid, language to the following customer(s) in the list if the field is not empty" src="../graphics/question.gif" /></td>
	               <td valign="top"><h3><fmt:message key="allMetrics" /></h3></td>
	               <td valign="top" align="right"><input type="checkbox" onclick="toggleAll(this)" name="__metricsAll" value="true"/> <fmt:message key="all"/></td> 
	              </tr>
	             </table>
	             </td>
	           </tr>
	           <tr>
	            <td valign="top">
	             <table cellpadding="1" cellspacing="1">
	              <tr style="">
			       <td>
			        <div align="left" class="buttonLeft">
		            <input type="submit" name="__updateWholeMetrics" value="Update" onClick="return optionsSelected()">
		            </div>
			       </td>
			     </tr>
	             </table>
	            </td>
	           </tr>
	           </table>
	         </div>
	         
	         <div style="float:left;padding: 5px">
	           <table class="productBatchTool" cellpadding="5" cellspacing="5">
	           <tr>
	            <td valign="top">
	              <table cellpadding="1" cellspacing="1" width="100%">
	              <tr style="height:30px;white-space: nowrap">
	               <td valign="top" width="20px"><img class="toolTipImg" title="Update metrics ::Updates qualifier to the following customer(s) in the list if the field is not empty" src="../graphics/question.gif" /></td>
	               <td valign="top"><h3><fmt:message key="qualifier" /></h3></td>
	               <td valign="top" align="right"><input type="checkbox" onclick="toggleAll(this)" name="__qualifierAll" value="true"/> <fmt:message key="all"/></td> 
	              </tr>
	             </table>
	             </td>
	           </tr>
	           <tr>
	            <td valign="top">
	             <table cellpadding="1" cellspacing="1">
	              <tr style="">
			       <td>
			        <select name="__qualifier_field">
	       			<option value="-1"></option>
	      			<option value="-2" <c:if test="${customerSearch.qualifierId == -2}">selected</c:if>><fmt:message key="noQualifier" /></option>
	       			<c:forEach items="${model.activeQualifiers}" var="qualifier">
	  	     			<option value="${qualifier.id}" <c:if test="${customerSearch.qualifierId == qualifier.id}">selected</c:if>><c:out value="${qualifier.name}" /></option>
	       			</c:forEach>
	      			</select>
			        <div align="left" class="buttonLeft">
		            <input type="submit" name="__updateQualifier" value="Update" onClick="return optionsSelected()">
		            </div>
			       </td>
			     </tr>
	             </table>
	            </td>
	           </tr>
	           </table>
	        </div>
	         
	       <!-- For assign User -->  
	          <div style="float:left;padding: 5px">
	           <table class="productBatchTool" cellpadding="5" cellspacing="5">
	           <tr>
	            <td valign="top">
	              <table cellpadding="1" cellspacing="1" width="100%">
	              <tr style="height:30px;white-space: nowrap">
	               <td valign="top" width="20px"><img class="toolTipImg" title="Access User :: Assign user to the contacts in the list" src="../graphics/question.gif" /></td>
	               <td valign="top"><h3><fmt:message key="user" /></h3></td>
	               <td valign="top" align="right"><input type="checkbox" onclick="toggleAll(this)" name="__userAssignAll" value="true"/> <fmt:message key="all"/></td> 
	              </tr>
	             </table>
	             </td>
	           </tr>
	           <tr>
	            <td valign="top">
	             <table cellpadding="1" cellspacing="1">
	              <tr style="">
			       <td>
			       <select name="contact_assignedTo" style="width: 156px"> 
			           <option value="0">Admin</option>
			           <c:forEach items="${model.userList}" var="user">
			             <option value="${user.id}">${user.name}</option>
			           </c:forEach>
			         </select>
			        <div align="left" class="buttonLeft">
		            <input type="submit" name="__userAssignPacher" value="Update" onClick="return optionsSelected()">
		            </div>
			       </td>
			     </tr>
	             </table>
	            </td>
	           </tr>
	           </table>
	        </div>
	        <!-- End -->
	        
	        <!-- For Update New  Information-->  
	          <div style="float:left;padding: 5px">
	           <table class="productBatchTool" cellpadding="5" cellspacing="5">
	           <tr>
	            <td valign="top">
	              <table cellpadding="1" cellspacing="1" width="100%">
	              <tr style="height:30px;white-space: nowrap">
	               <td valign="top" width="20px"><img class="toolTipImg" title="Update new information :: Check it, customer will go to fulfill new information before place an order" src="../graphics/question.gif" /></td>
	               <td valign="top"><h3><fmt:message key="updateNewInformation" /></h3></td>
	               <td valign="top" align="right"><input type="checkbox" onclick="toggleAll(this)" name="__updateNewInfomationAll" value="true"/> <fmt:message key="all"/></td> 
	              </tr>
	             </table>
	             </td>
	           </tr>
	           <tr>
	            <td valign="top">
	             <table cellpadding="1" cellspacing="1">
	              <tr style="">
	              
	              <td>
			        <div><p><input name="__batch_updateNewInformation" type="radio" checked="checked" value="1"/>Check</p></div>
				    <div><p><input name="__batch_updateNewInformation" type="radio" value="0"/>Uncheck</p></div>
				   </td>
	              
			       <td>
<%-- 			       <select name="contact_assignedTo" style="width: 156px"> 
			           <option value="0">Admin</option>
			           <c:forEach items="${model.userList}" var="user">
			             <option value="${user.id}">${user.name}</option>
			           </c:forEach>
			         </select> --%>
			        <div align="left" class="buttonLeft">
		            <input type="submit" name="__updateNewInformation" value="Apply" onClick="return optionsSelected()">
		            </div>
			       </td>
			     </tr>
	             </table>
	            </td>
	           </tr>
	           </table>
	        </div>
	        <!-- End -->
	        
	    </div>
	  </c:if>
    
  </td><td class="topr_g quickMode" ></td></tr>
  <tr><td class="boxmidlrg" >

 <!-- tabs -->
 <div class="tab-wrapper">
	<h4 class="quickMode" title=""></h4>
	<div>
	<!-- start tab -->

	  	<div class="listdivi quickMode"></div>
	   
		  	<div class="listlight">
		  	<!-- input field -->  
		  	<table border="0" cellpadding="0" cellspacing="1" width="100%" class="quickMode">
			  <tr>	   
			  <c:if test="${gSiteConfig['gSUB_ACCOUNTS']}">
			  <td>
				<select name="parent"
				  onChange="document.getElementById('page').value=1;submit()">
					<option value=""><fmt:message key="all" /> <fmt:message key="accounts" /></option>
					<option value="-1" <c:if test="${-1 == customerSearch['parent']}">selected</c:if>>
						<fmt:message key="main" /> <fmt:message key="accounts" /></option>		
					<c:forEach items="${model.familyTree}" var="customer">
					<option value="${customer.id}" <c:if test="${customer.id == customerSearch['parent']}">selected</c:if>>
						<fmt:message key="subAccounts" /> <fmt:message key="of" /> <c:out value="${customer.username}" /></option>
				</c:forEach>
				</select>
			  </td>
			  </c:if>
			  <c:if test="${model.count > 0}">
			  <td class="pageShowing">
			  <fmt:message key="showing">
				<fmt:param value="${customerSearch.offset + 1}"/>
				<fmt:param value="${model.pageEnd}"/>
				<fmt:param value="${model.count}"/>
			  </fmt:message>
			  </td>
			  </c:if>
			  <td class="pageNavi">
			  Page 
			  <c:choose>
			  <c:when test="${model.pageCount <= 100 and model.pageCount > 0}">
			  <select id="page" name="page" onchange="submit()">
			  <c:forEach begin="1" end="${model.pageCount}" var="page">
			  	<option value="${page}" <c:if test="${page == (customerSearch.page)}">selected</c:if>>${page}</option>
			  </c:forEach>
			  </select>
			  </c:when>
			  <c:otherwise>
			  <input type="text" id="page" name="page" value="${customerSearch.page}" size="5" class="textfield50" />
			  <input type="submit" value="go"/>
			  </c:otherwise>
			  </c:choose>			  
			  of <c:out value="${model.pageCount}"/>
			  | 
			  <c:if test="${customerSearch.page == 1}"><span class="pageNaviDead"><fmt:message key="previous" /></span></c:if>
			  <c:if test="${customerSearch.page != 1}"><a href="#" onClick="document.getElementById('page').value='${customerSearch.page-1}';document.getElementById('list').submit()" class="pageNaviLink"><fmt:message key="previous" /></a></c:if>
			  | 
			  <c:if test="${customerSearch.page == model.pageCount}"><span class="pageNaviDead"><fmt:message key="next" /></span></c:if>
			  <c:if test="${customerSearch.page != model.pageCount}"><a href="#" onClick="document.getElementById('page').value='${customerSearch.page+1}';document.getElementById('list').submit()" class="pageNaviLink"><fmt:message key="next" /></a></c:if>
			  </td>
			  </tr>
			</table>
			
			<table border="0" cellpadding="0" cellspacing="0" width="100%" class="listings">
			  <tr class="listingsHdr2">
			    <c:set var="cols" value="0"/>
			    <c:if test="${model.count > 0}">
			    <td align="left" class="quickMode"><input type="checkbox" onclick="toggleAll(this)"></td>
			    </c:if>
			    <td style="min-width:20px;" class="quickMode">&nbsp;</td>
			    <c:if test="${fn:contains(siteConfig['CUSTOMIZE_CUSTOMER_LIST'].value,'lastName')}"> 
			    <td>
			    	<table cellspacing="0" cellpadding="1">
			    	  <tr>
			    	    <c:choose>
			    	      <c:when test="${customerSearch.sort == 'last_name desc, first_name'}">
			    	        <td class="listingsHdr3">
			    	        <a class="hdrLink" href="#" onClick="document.getElementById('sort').value='last_name, first_name';document.getElementById('list').submit()"><fmt:message key="lastName" /></a>
			    	        </td>
			    	        <td><img src="../graphics/up.gif" border="0"></td>
			    	      </c:when>
			    	      <c:when test="${customerSearch.sort == 'last_name, first_name'}">
			    	        <td class="listingsHdr3">
			    	        <a class="hdrLink" href="#" onClick="document.getElementById('sort').value='last_name desc, first_name';document.getElementById('list').submit()"><fmt:message key="lastName" /></a>
			    	        </td>
			    	        <td><img src="../graphics/down.gif" border="0"></td>
			    	      </c:when>
			    	      <c:otherwise>
			    	        <td colspan="2" class="listingsHdr3">
			    	        <a class="hdrLink" href="#" onClick="document.getElementById('sort').value='last_name, first_name';document.getElementById('list').submit()"><fmt:message key="lastName" /></a>
			    	        </td>
			    	      </c:otherwise>
			    	    </c:choose>  
			    	  </tr>
			    	 </table>
			    </td>
			    </c:if>
			    <c:if test="${fn:contains(siteConfig['CUSTOMIZE_CUSTOMER_LIST'].value,'firstName')}"> 
			    <td>
			    	<table cellspacing="0" cellpadding="1">
			    	  <tr>
			    	    <c:choose>
			    	      <c:when test="${customerSearch.sort == 'first_name desc, last_name'}">
			    	        <td class="listingsHdr3">
			    	        <a class="hdrLink" href="#" onClick="document.getElementById('sort').value='first_name, last_name';document.getElementById('list').submit()"><fmt:message key="firstName" /></a>
			    	        </td>
			    	        <td><img src="../graphics/up.gif" border="0"></td>
			    	      </c:when>
			    	      <c:when test="${customerSearch.sort == 'first_name, last_name'}">
			    	        <td class="listingsHdr3">
			    	        <a class="hdrLink" href="#" onClick="document.getElementById('sort').value='first_name desc, last_name';document.getElementById('list').submit()"><fmt:message key="firstName" /></a>
			    	        </td>
			    	        <td><img src="../graphics/down.gif" border="0"></td>
			    	      </c:when>
			    	      <c:otherwise>
			    	        <td colspan="2" class="listingsHdr3">
			    	        <a class="hdrLink" href="#" onClick="document.getElementById('sort').value='first_name, last_name';document.getElementById('list').submit()"><fmt:message key="firstName" /></a>
			    	        </td>
			    	      </c:otherwise>
			    	    </c:choose>  
			    	  </tr>
			    	  </table>
			    </td>
			    </c:if>
			    <c:if test="${fn:contains(siteConfig['CUSTOMIZE_CUSTOMER_LIST'].value,'email')}"> 
			    <td>
			    	<table cellspacing="0" cellpadding="1">
			    	  <tr>
			    	    <c:choose>
			    	      <c:when test="${customerSearch.sort == 'username desc'}">
			    	        <td class="listingsHdr3">
			    	        <a class="hdrLink" href="#" onClick="document.getElementById('sort').value='username';document.getElementById('list').submit()"><fmt:message key="emailAddress" /></a>
			    	        </td>
			    	        <td><img src="../graphics/up.gif" border="0"></td>
			    	      </c:when>
			    	      <c:when test="${customerSearch.sort == 'username'}">
			    	        <td class="listingsHdr3">
			    	        <a class="hdrLink" href="#" onClick="document.getElementById('sort').value='username desc';document.getElementById('list').submit()"><fmt:message key="emailAddress" /></a>
			    	        </td>
			    	        <td><img src="../graphics/down.gif" border="0"></td>
			    	      </c:when>
			    	      <c:otherwise>
			    	        <td colspan="2" class="listingsHdr3">
			    	        <a class="hdrLink" href="#" onClick="document.getElementById('sort').value='username desc';document.getElementById('list').submit()"><fmt:message key="emailAddress" /></a>
			    	        </td>
			    	      </c:otherwise>
			    	    </c:choose>  
			    	  </tr>
			    	  </table>
			    </td>
			    </c:if>
			    <c:if test="${fn:contains(siteConfig['CUSTOMIZE_CUSTOMER_LIST'].value,'company')}"> 
			    <td>
			    	<table cellspacing="0" cellpadding="1">
			    	  <tr>
			    	    <c:choose>
			    	      <c:when test="${customerSearch.sort == 'company desc'}">
			    	        <td class="listingsHdr3">
			    	        <a class="hdrLink" href="#" onClick="document.getElementById('sort').value='company';document.getElementById('list').submit()"><fmt:message key="company" /></a>
			    	        </td>
			    	        <td><img src="../graphics/up.gif" border="0"></td>
			    	      </c:when>
			    	      <c:when test="${customerSearch.sort == 'company'}">
			    	        <td class="listingsHdr3">
			    	        <a class="hdrLink" href="#" onClick="document.getElementById('sort').value='company desc';document.getElementById('list').submit()"><fmt:message key="company" /></a>
			    	        </td>
			    	        <td><img src="../graphics/down.gif" border="0"></td>
			    	      </c:when>
			    	      <c:otherwise>
			    	        <td colspan="2" class="listingsHdr3">
			    	        <a class="hdrLink" href="#" onClick="document.getElementById('sort').value='company desc';document.getElementById('list').submit()"><fmt:message key="company" /></a>
			    	        </td>
			    	      </c:otherwise>
			    	    </c:choose>  
			    	  </tr>
			    	  </table>
			    </td>
			    </c:if>
			    <c:if test="${fn:contains(siteConfig['CUSTOMIZE_CUSTOMER_LIST'].value,'field_12')}"> 
			    <td>
			    	<table cellspacing="0" cellpadding="1">
			    	  <tr>
			    	    <c:choose>
			    	      <c:when test="${customerSearch.sort == 'field_12 desc'}">
			    	        <td class="listingsHdr3">
			    	        <a class="hdrLink" href="#" onClick="document.getElementById('sort').value='field_12';document.getElementById('list').submit()"><fmt:message key="field_12" /></a>
			    	        </td>
			    	        <td><img src="../graphics/up.gif" border="0"></td>
			    	      </c:when>
			    	      <c:when test="${customerSearch.sort == 'field_12'}">
			    	        <td class="listingsHdr3">
			    	        <a class="hdrLink" href="#" onClick="document.getElementById('sort').value='field_12 desc';document.getElementById('list').submit()"><fmt:message key="field_12" /></a>
			    	        </td>
			    	        <td><img src="../graphics/down.gif" border="0"></td>
			    	      </c:when>
			    	      <c:otherwise>
			    	        <td colspan="2" class="listingsHdr3">
			    	        <a class="hdrLink" href="#" onClick="document.getElementById('sort').value='field_12 desc';document.getElementById('list').submit()"><fmt:message key="field_12" /></a>
			    	        </td>
			    	      </c:otherwise>
			    	    </c:choose>  
			    	  </tr>
			    	  </table>
			    </td>
			    </c:if>
			    <c:if test="${fn:contains(siteConfig['CUSTOMIZE_CUSTOMER_LIST'].value,'supplierPrefix')}"> 
			    <td>
			    	<table cellspacing="0" cellpadding="1">
			    	  <tr>
			    	    <c:choose>
			    	      <c:when test="${customerSearch.sort == 'supplier_prefix desc'}">
			    	        <td class="listingsHdr3">
			    	        <a class="hdrLink" href="#" onClick="document.getElementById('sort').value='supplier_prefix';document.getElementById('list').submit()"><fmt:message key="supplierPrefix" /></a>
			    	        </td>
			    	        <td><img src="../graphics/up.gif" border="0"></td>
			    	      </c:when>
			    	      <c:when test="${customerSearch.sort == 'supplier_prefix'}">
			    	        <td class="listingsHdr3">
			    	        <a class="hdrLink" href="#" onClick="document.getElementById('sort').value='supplier_prefix desc';document.getElementById('list').submit()"><fmt:message key="supplierPrefix" /></a>
			    	        </td>
			    	        <td><img src="../graphics/down.gif" border="0"></td>
			    	      </c:when>
			    	      <c:otherwise>
			    	        <td colspan="2" class="listingsHdr3">
			    	        <a class="hdrLink" href="#" onClick="document.getElementById('sort').value='supplier_prefix desc';document.getElementById('list').submit()"><fmt:message key="supplierPrefix" /></a>
			    	        </td>
			    	      </c:otherwise>
			    	    </c:choose>  
			    	  </tr>
			    	  </table>
			    </td>
			    </c:if>
			    <c:if test="${fn:contains(siteConfig['CUSTOMIZE_CUSTOMER_LIST'].value,'trackCode')}">
			    <td align="center">
			    	<table cellspacing="0" cellpadding="1">
			    	  <tr>
			    	    <c:choose>
			    	      <c:when test="${customerSearch.sort == 'trackcode desc'}">
			    	        <td class="listingsHdr3">
			    	        <a class="hdrLink" href="#" onClick="document.getElementById('sort').value='trackcode';document.getElementById('list').submit()"><fmt:message key="trackcode" /></a>
			    	        </td>
			    	        <td><img src="../graphics/up.gif" border="0"></td>
			    	      </c:when>
			    	      <c:when test="${customerSearch.sort == 'trackcode'}">
			    	        <td class="listingsHdr3">
			    	        <a class="hdrLink" href="#" onClick="document.getElementById('sort').value='trackcode desc';document.getElementById('list').submit()"><fmt:message key="trackcode" /></a>
			    	        </td>
			    	        <td><img src="../graphics/down.gif" border="0"></td>
			    	      </c:when>
			    	      <c:otherwise>
			    	        <td colspan="2" class="listingsHdr3">
			    	        <a class="hdrLink" href="#" onClick="document.getElementById('sort').value='trackcode desc';document.getElementById('list').submit()"><fmt:message key="trackcode" /></a>
			    	        </td>
			    	      </c:otherwise>
			    	    </c:choose>  
			    	  </tr>
			    	 </table>
			    </td>
			    </c:if>
			    <c:if test="${fn:contains(siteConfig['CUSTOMIZE_CUSTOMER_LIST'].value,'accountNumber')}">
			    <td>
			    	<table cellspacing="0" cellpadding="1">
			    	  <tr>
			    	    <td rowspan="2" class="listingsHdr3"><fmt:message key="accountNumber" /></td>
			          </tr>
			    	</table>
			    </td>
			    </c:if>
			    <c:if test="${fn:contains(siteConfig['CUSTOMIZE_CUSTOMER_LIST'].value,'phone')}">
			    <td>
			    	<table cellspacing="0" cellpadding="1">
			    	  <tr>
			    	    <td rowspan="2" class="listingsHdr3"><fmt:message key="phone" /></td>
			          </tr>
			    	</table>
			    </td>
			    </c:if>
			    <c:if test="${fn:contains(siteConfig['CUSTOMIZE_CUSTOMER_LIST'].value,'taxId') and gSiteConfig['gTAX_EXEMPTION']}">
			    <td>
			    	<table cellspacing="0" cellpadding="1">
			    	  <tr>
			    	    <td rowspan="2" class="listingsHdr3"><fmt:message key="taxIdShort" /></td>
			          </tr>
			    	</table>
			    </td>
			    </c:if>
			    <c:if test="${fn:contains(siteConfig['CUSTOMIZE_CUSTOMER_LIST'].value,'created')}">
			    <td align="center">
			    	<table cellspacing="0" cellpadding="1">
			    	  <tr>
			    	    <c:choose>
			    	      <c:when test="${customerSearch.sort == 'created desc'}">
			    	        <td class="listingsHdr3">
			    	        <a class="hdrLink" href="#" onClick="document.getElementById('sort').value='created';document.getElementById('list').submit()"><fmt:message key="accountCreated" /></a>
			    	        </td>
			    	        <td><img src="../graphics/up.gif" border="0"></td>
			    	      </c:when>
			    	      <c:when test="${customerSearch.sort == 'created'}">
			    	        <td class="listingsHdr3">
			    	        <a class="hdrLink" href="#" onClick="document.getElementById('sort').value='created desc';document.getElementById('list').submit()"><fmt:message key="accountCreated" /></a>
			    	        </td>
			    	        <td><img src="../graphics/down.gif" border="0"></td>
			    	      </c:when>
			    	      <c:otherwise>
			    	        <td colspan="2" class="listingsHdr3">
			    	        <a class="hdrLink" href="#" onClick="document.getElementById('sort').value='created desc';document.getElementById('list').submit()"><fmt:message key="accountCreated" /></a>
			    	        </td>
			    	      </c:otherwise>
			    	    </c:choose>  
			    	  </tr>
			    	 </table>
			    </td>
			    </c:if>
			    <c:if test="${fn:contains(siteConfig['CUSTOMIZE_CUSTOMER_LIST'].value,'echoSign')}">
			    <td align="center">
			    	<table cellspacing="0" cellpadding="1">
			    	  <tr>
			    	    <td class="listingsHdr3">EchoSign</td>
			          </tr>
			        </table>
			    </td>
			    </c:if>					    
			    <c:if test="${fn:contains(siteConfig['CUSTOMIZE_CUSTOMER_LIST'].value,'imported')}">
			    <td align="center">
			    	<table cellspacing="0" cellpadding="1">
			    	  <tr>
			    	    <td rowspan="2" class="listingsHdr3"><fmt:message key="imported" /></td>
			          </tr>
			        </table>
			    </td>
			    </c:if>
			    <c:if test="${fn:contains(siteConfig['CUSTOMIZE_CUSTOMER_LIST'].value,'registeredBy')}">
			    <td align="center">
			    	<table cellspacing="0" cellpadding="1">
			    	  <tr>
			    	    <td rowspan="2" class="listingsHdr3"><fmt:message key="registeredBy" /></td>
			          </tr>
			        </table>
			    </td>
			    </c:if>
			    <c:if test="${fn:contains(siteConfig['CUSTOMIZE_CUSTOMER_LIST'].value,'logins')}">
			    <td align="center">
			    	<table cellspacing="0" cellpadding="1">
			    	  <tr>
			    	    <c:choose>
			    	      <c:when test="${customerSearch.sort == 'num_of_logins desc'}">
			    	        <td class="listingsHdr3">
			    	        <a class="hdrLink" href="#" onClick="document.getElementById('sort').value='num_of_logins';document.getElementById('list').submit()"><fmt:message key="numOfLogins" /></a>
			    	        </td>
			    	        <td><img src="../graphics/up.gif" border="0"></td>
			    	      </c:when>
			    	      <c:when test="${customerSearch.sort == 'num_of_logins'}">
			    	        <td class="listingsHdr3">
			    	        <a class="hdrLink" href="#" onClick="document.getElementById('sort').value='num_of_logins desc';document.getElementById('list').submit()"><fmt:message key="numOfLogins" /></a>
			    	        </td>
			    	        <td><img src="../graphics/down.gif" border="0"></td>
			    	      </c:when>
			    	      <c:otherwise>
			    	        <td colspan="2" class="listingsHdr3">
			    	        <a class="hdrLink" href="#" onClick="document.getElementById('sort').value='num_of_logins desc';document.getElementById('list').submit()"><fmt:message key="numOfLogins" /></a>
			    	        </td>
			    	      </c:otherwise>
			    	    </c:choose>  
			    	  </tr>
			    	  </table>
			    </td>
			    </c:if>
			    <c:if test="${(gSiteConfig['gSHOPPING_CART']) and (fn:contains(siteConfig['CUSTOMIZE_CUSTOMER_LIST'].value,'orders'))}">
			    <c:set var="cols" value="${cols+2}"/>
			    <td align="right">
			        <table cellspacing="0" cellpadding="1">
			    	  <tr>
			    	    <td rowspan="2" class="listingsHdr3"><fmt:message key="orderCount" /></td>
			          </tr>
			        </table>
			    </td>
			    <td align="right">
			        <table cellspacing="0" cellpadding="1">
			    	  <tr>
			    	    <td rowspan="2" class="listingsHdr3"><fmt:message key="orderTotal" /></td>
			          </tr>
			    	</table>
			    </td>
			    <sec:authorize ifAnyGranted="ROLE_AEM,ROLE_ADMIN_${prop_site_id}">
			    <c:if test="${gSiteConfig['gVIRTUAL_BANK_ACCOUNT'] }">
			    <td align="right">
			        <table cellspacing="0" cellpadding="1">
			    	  <tr>
			    	    <td rowspan="2" class="listingsHdr3"><fmt:message key="outgoingPay" /></td>
			          </tr>
			    	</table>
			    </td>
			    </c:if>
			    </sec:authorize>	
			    </c:if>
			    <c:if test="${(siteConfig['PRODUCT_QUOTE'].value == 'true' or gSiteConfig['gSHIPPING_QUOTE']) and fn:contains(siteConfig['CUSTOMIZE_CUSTOMER_LIST'].value,'quote')}">
			    <td align="right">
			        <table cellspacing="0" cellpadding="1">
			    	  <tr>
			    	    <td rowspan="2" class="listingsHdr3"><fmt:message key="quoteCount" /></td>
			          </tr>
			        </table>
			    </td>
			    </c:if>
			    <c:if test="${fn:contains(siteConfig['CUSTOMIZE_CUSTOMER_LIST'].value,'qualifier')}"> 
			    <td>
			    	<table cellspacing="0" cellpadding="1">
			    	  <tr>
			    	    <c:choose>
			    	      <c:when test="${customerSearch.sort == 'qualifier desc'}">
			    	        <td class="listingsHdr3">
			    	        <a class="hdrLink" href="#" onClick="document.getElementById('sort').value='qualifier';document.getElementById('list').submit()"><fmt:message key="qualifier" /></a>
			    	        </td>
			    	        <td><img src="../graphics/up.gif" border="0"></td>
			    	      </c:when>
			    	      <c:when test="${customerSearch.sort == 'qualifier'}">
			    	        <td class="listingsHdr3">
			    	        <a class="hdrLink" href="#" onClick="document.getElementById('sort').value='qualifier desc';document.getElementById('list').submit()"><fmt:message key="qualifier" /></a>
			    	        </td>
			    	        <td><img src="../graphics/down.gif" border="0"></td>
			    	      </c:when>
			    	      <c:otherwise>
			    	        <td colspan="2" class="listingsHdr3">
			    	        <a class="hdrLink" href="#" onClick="document.getElementById('sort').value='qualifier desc';document.getElementById('list').submit()"><fmt:message key="qualifier" /></a>
			    	        </td>
			    	      </c:otherwise>
			    	    </c:choose>  
			    	  </tr>
			    	  </table>
			    </td>
			    </c:if>
			    <c:if test="${(gSiteConfig['gSALES_REP']) and (fn:contains(siteConfig['CUSTOMIZE_CUSTOMER_LIST'].value,'salesRep'))}">
			    <td align="center">
			    	<table cellspacing="0" cellpadding="1">
			    	  <tr>
			    	    <c:choose>
			    	      <c:when test="${customerSearch.sort == 'sales_rep_id desc'}">
			    	        <td class="listingsHdr3">
			    	        <a class="hdrLink" href="#" onClick="document.getElementById('sort').value='sales_rep_id';document.getElementById('list').submit()"><fmt:message key="salesRep" /></a>
			    	        </td>
			    	        <td><img src="../graphics/up.gif" border="0"></td>
			    	      </c:when>
			    	      <c:when test="${customerSearch.sort == 'sales_rep_id'}">
			    	        <td class="listingsHdr3">
			    	        <a class="hdrLink" href="#" onClick="document.getElementById('sort').value='sales_rep_id desc';document.getElementById('list').submit()"><fmt:message key="salesRep" /></a>
			    	        </td>
			    	        <td><img src="../graphics/down.gif" border="0"></td>
			    	      </c:when>
			    	      <c:otherwise>
			    	        <td colspan="2" class="listingsHdr3">
			    	        <a class="hdrLink" href="#" onClick="document.getElementById('sort').value='sales_rep_id desc';document.getElementById('list').submit()"><fmt:message key="salesRep" /></a>
			    	        </td>
			    	      </c:otherwise>
			    	    </c:choose>  
			    	  </tr>
			    	  </table>
			    </td>			    
			    </c:if>
			    <c:if test="${(gSiteConfig['gMULTI_STORE'] > 0) and (fn:contains(siteConfig['CUSTOMIZE_CUSTOMER_LIST'].value,'multiStore'))}">
			    <c:set var="cols" value="${cols+1}"/>
			    <td class="listingsHdr3"><fmt:message key="multiStore" /></td>
				</c:if>			   
			  </tr>
			<c:forEach items="${model.customers}" var="customer" varStatus="status">
			  <tr class="row${status.index % 2}" onmouseover="changeStyleClass(this,'rowOver')" onmouseout="changeStyleClass(this,'row${status.index % 2}')">
			    <c:if test="${model.count > 0}">
			    <td align="left" class="quickMode"><input name="__selected_id" value="${customer.id}" type="checkbox">
			      <c:if test="${wj:contains(model.loggedIn,customer.id, null)}">
				       <img style="padding-bottom:3px;padding-left:2px;" alt="login" title="login" src="../graphics/online.png" border="0" />
				    </c:if>
				  </td>
			    <td align="center" class="quickMode">
			    <img class="action" width="23" height="16" alt="Actions" src="../graphics/actions.png"/>
			    <div class="action-hover displayNone" style="display: none;">
			        <ul class="round">
			            <li class="action-header">
					      <div class="name"><c:out value="${customer.address.firstName}"/>&nbsp;<c:out value="${customer.address.lastName}"/></div>
					      <div class="id"><c:choose><c:when test="${customer.cardId != null}"><fmt:message key="idCard"/>: <c:out value="${customer.cardId}" /></c:when> <c:otherwise><fmt:message key="id"/>: <c:out value="${customer.id}" /></c:otherwise>  </c:choose></div>
					      <div class="menudiv"></div>
					      <div style="position:relative">
					      <span style="width:16px;float:left;"><a href="customerQuickView.jhtm?cid=${customer.id}" rel="width:900,height:400" id="mb${customer.id}" class="mbCustomer" title="<fmt:message key="customer" />ID : ${customer.id}"><img src="../graphics/magnifier.gif" border="0" /></a></span>
					      <c:if test="${gSiteConfig['gCRM'] and customer.crmContactId != null}">
					        <span style="width:16px;float:left;"><a href="../crm/crmContactQuickView.jhtm?id=${customer.crmContactId}" rel="width:900,height:400" id="mb${customer.crmContactId}" class="mbCustomer" title="<fmt:message key="crmContact" />ID : ${customer.crmContactId}"><img src="../graphics/magnifierCrm.gif" border="0" /></a></span>
					      </c:if>
					        <c:if test="${gSiteConfig['gADD_INVOICE']}">
					          <span style="width:16px;float:left;"><a href="#" onclick="confirmCustomer(${customer.id});"><img src="../graphics/add.png" alt="Add Order" title="Add Order" border="0"></a></span>
					        </c:if>
					        <c:if test="${gSiteConfig['gTICKET']}">
					          <span style="width:16px;float:left;"><a href="../ticket/ticketForm.jhtm?cid=${customer.id}"><img src="../graphics/ticket_small.gif" alt="Add Ticket" title="Add Ticket" border="0"></a></span>
					        </c:if>
					        <span style="width:16px;float:left;"><a href="https://maps.google.com/?q=loc:${customer.address.addr1}+${customer.address.city}+${customer.address.stateProvince}+${customer.address.country}" target="new"><img src="../graphics/map_small.gif" alt="Map" title="Map" border="0"></a></span>
					        <span style="width:16px;float:left;"><a href="https://www.viatrading.com/dv/tsr/viewdocuments.php?id=${customer.id}" target="new">DOCS</a></span>  
					      </div>  
			            </li>
			            <sec:authorize ifAnyGranted="ROLE_AEM,ROLE_ADMIN_${prop_site_id},ROLE_CUSTOMER_EDIT">
				          <li class="link"><a href="customer.jhtm?id=${customer.id}"><fmt:message key="edit" /></a></li>
				        </sec:authorize>
			            <sec:authorize ifAnyGranted="ROLE_AEM,ROLE_ADMIN_${prop_site_id},ROLE_CUSTOMER_LOGINAS">
			              <li class="link" ><a href="loginAsCustomer.jhtm?cid=${customer.token}" target="_blank"><fmt:message key="loginAsCustomer" /></a></li>
			            </sec:authorize>
				        <li class="link"><a href="addressList.jhtm?id=${customer.id}"><fmt:message key="addressList" /></a></li>
				        <li class="link"><a href="email.jhtm?id=${customer.id}"><fmt:message key="sendEmail" /></a></li>
				        <sec:authorize ifAnyGranted="ROLE_AEM,ROLE_ADMIN_${prop_site_id},ROLE_CUSTOMER_ADD_INVOICE">
			            <c:if test="${gSiteConfig['gADD_INVOICE']}">
			              <li class="link"><a href="#" onclick="confirmCustomer(${customer.id});"><fmt:message key="addOrder" /></a></li>
			            </c:if>  
			            <c:if test="${gSiteConfig['gSHIPPING_QUOTE']}">
			              <li class="link"><a href="../orders/addQuote.jhtm?cid=${customer.id}"><fmt:message key="addQuote" /></a></li>
			            </c:if>  
			            </sec:authorize>
			            <c:if test="${gSiteConfig['gSUB_ACCOUNTS']}">
			              <li class="link"><a href="setParent.jhtm?cid=${customer.id}&begin=t"><fmt:message key="parent" /></a></li>
			              <li class="link"><a href="customerList.jhtm?parent=${customer.id}&emptyFilter=true"><fmt:message key="subAccounts" />
			          					(<b><c:out value="${customer.subCount}" /></b>)</a></li>        
					    </c:if>  
					    <c:if test="${siteConfig['gSUB_ACCOUNTS']}">
			              <li class="link"><a href="customerList.jhtm?parent=${customer.id}&emptyFilter=true"><fmt:message key="addPartners" /></a></li>        
					    </c:if>
					    <sec:authorize ifAnyGranted="ROLE_ADMIN_${prop_site_id},ROLE_CUSTOMER_MYLIST_ADD,ROLE_CUSTOMER_MYLIST_DELETE,ROLE_CUSTOMER_MYLIST_VIEW">
			            <c:if test="${gSiteConfig['gMYLIST'] == 'true'}">
			              <li class="link"><a href="myList.jhtm?id=${customer.id}"><fmt:message key="myList" /></a></li>
			            </c:if>
			            </sec:authorize>
			            <c:if test="${gSiteConfig['gSPECIAL_PRICING'] == 'true'}">
			              <li class="link"><a href="specialPricing.jhtm?id=${customer.id}"><fmt:message key="specialPricing" /></a></li>
			            </c:if>
			            <c:if test="${gSiteConfig['gBUDGET_BRAND']}">
			              <li class="link"><a href="budgetByBrands.jhtm?id=${customer.id}"><fmt:message key="budgetByBrands" /></a></li>
			            </c:if>
			            <c:if test="${gSiteConfig['gBUDGET_PRODUCT']}">
			              <li class="link"><a href="budgetByProducts.jhtm?cid=${customer.id}"><fmt:message key="budgetByProducts" /></a></li>
			            </c:if>
			            <c:if test="${gSiteConfig['gAFFILIATE'] > 0 and customer.isAffiliate}" >
			              <li class="link"><a href="../customers/commissionList.jhtm?id=${customer.id}"><fmt:message key="commission" /></a></li>
			            </c:if>
			            <c:if test="${gSiteConfig['gREGISTRATION_TOUCH_SCREEN']}">
			              <li class="link"><a href="customerIdCard_${customer.id}.pdf"><fmt:message key="idCard" />: <c:out value="${customer.cardId}"/></a></li>
			              <li class="link"><a href="../../touchPrintLabel.jhtm?__cardId=${customer.cardId}&__printPdf=true"><fmt:message key="printSoldLabel" /></a></li>
			            </c:if>
			            <c:if test="${gSiteConfig['gEVENT'] and !customer.suspendedEvent}">
			            <li class="link"><a href="../event/registerEventMember.jhtm?id=${customer.id}" rel="width:400,height:380"  id="mb${customer.id}" class="mbRegisterEvent">Register to Event</a></li>
			            </c:if>
			            <c:if test="${customer.lastLogin != null}">
			            <li class="link"><span><fmt:message key="lastLogin" />: <fmt:formatDate type="both" timeStyle="full" pattern="MM/dd/yyyy hh:mm a" value="${customer.lastLogin}"/></span></li>
			            </c:if>
			            <c:if test="${customer.lastModified != null}">
			            <li class="link"><span><fmt:message key="lastModified" />: <fmt:formatDate type="both" timeStyle="full" pattern="MM/dd/yyyy hh:mm a" value="${customer.lastModified}"/></span></li>
			            </c:if>			        
			            <c:if test="${gSiteConfig['gPAYMENTS']}">
      					<sec:authorize ifAnyGranted="ROLE_AEM,ROLE_ADMIN_${prop_site_id},ROLE_INCOMING_PAYMENT_VIEW_LIST">
			              <li class="link"><a href="payments.jhtm?cid=${customer.id}"><fmt:message key="incomingPayments" /></a></li>
			            </sec:authorize>       
			            </c:if>			            
			            <c:if test="${gSiteConfig['gVIRTUAL_BANK_ACCOUNT'] }" >
			            <sec:authorize ifAnyGranted="ROLE_AEM,ROLE_ADMIN_${prop_site_id},ROLE_OUTGOING_PAYMENT_VIEW_LIST">
			              <li class="link"><a href="../customers/virtualBankPayment.jhtm?username=${customer.username}"><fmt:message key="outgoingPayments" /></a></li>
			            </sec:authorize>
			            </c:if>
				        <c:if test="${gSiteConfig['gGIFTCARD'] or siteConfig['POINT_TO_CREDIT'].value == 'true' or gSiteConfig['gVIRTUAL_BANK_ACCOUNT'] or gSiteConfig['gBUDGET']}">
				          <li class="link"><span><fmt:message key="myCredit" />: <fmt:message key="${siteConfig['CURRENCY'].value}" /><fmt:formatNumber value="${customer.credit}" pattern="#,##0.00" /></span></li>
				          <sec:authorize ifAnyGranted="ROLE_AEM,ROLE_ADMIN_${prop_site_id},ROLE_CUSTOMER_CREDIT_UPDATE_DELETE">
				          <c:if test="${ gSiteConfig['gBUDGET']}">			
				          <li>
					            <a href="#${customer.id}" rel="type:element" id="mb${customer.id}" class="mbAddCredit"><fmt:message key="add"/> <fmt:message key="credit"/></a>
					          	<div id="${customer.id}" class="miniWindowWrapper addCreditBox displayNone">
						          	<div class="header"><fmt:message key="add"/>&nbsp;<fmt:message key="credit"/></div>
						    	  		<fieldset class="top">
						    	    		<label class="AStitle"><fmt:message key="customer"/>&nbsp;<fmt:message key="name"/></label>
						    	    		<input name="name" type="text" value="<c:out value="${customer.address.firstName}"/>&nbsp;<c:out value="${customer.address.lastName}"/>" disabled="disabled"/>
						    	  		</fieldset>
						    	  		<fieldset class="bottom">
						    	    		<label class="AStitle"><fmt:message key="credit"/> <fmt:message key="available"/></label>
						    	    		<input name="creditAvailable" type="text" value="<fmt:formatNumber value="${customer.credit}" pattern="#,##0.00" />" disabled="disabled"/>
						    	  		</fieldset>
						    	  		<fieldset class="bottom">
						    	    		<label class="AStitle"><fmt:message key="credit"/></label>
						    	    		<input name="__credit_" type="text" id="credit_${customer.id}" onkeyup="creditValue(this.value, ${customer.id});"/> 
						    	    		<div class="button">
			      								<input type="submit" value="Add"  name="__add_credit_" onclick="addCredit('${customer.id}');"/>
			      								<c:if test="${customer.credit > 0}">
			      									<input type="submit" value="Deduct"  name="__deduct_credit_" onclick="deductCredit('${customer.id}');"/>
			      								</c:if>
			      								<%-- Need to work on Credit History report.
			      								<div>
				      								<span style="float: right; padding-top: 10px;">
				      									<a href="../customers/virtualBankPayment.jhtm?userId=${customer.id}">view history</a>
				      								</span>
			      								</div> --%>
			      							</div>
						    	  	  </fieldset>
				    	  		  </div>
				          </li>
				          </c:if>
				          </sec:authorize>
				        </c:if>	
				        <c:if test="${gSiteConfig['gSHOPPING_CART'] and customer.numOfCartItems > 0}">
				          <li class="link"><span><fmt:message key="shoppingCart" />: <c:out value="${customer.numOfCartItems}"/> <fmt:message key="cartItems(s)" /></span></li>
				        </c:if>
				        <c:if test="${gSiteConfig['gTICKET']}">
					      <li class="link"><a href="../ticket/ticketList.jhtm?user_id=${customer.id}&status=open"><fmt:message key="openTicket" /></a></li>
				        </c:if>
				        <c:if test="${gSiteConfig['gCRM'] && customer.crmContactId != null}">
			              <li class="link"><a href="../crm/crmTaskForm.jhtm?accountId=${customer.crmAccountId}&contactId=${customer.crmContactId}"><fmt:message key="addTask" /></a></li>
			              <li class="link"><a href="../crm/crmTaskList.jhtm?contactId=${customer.crmContactId}"><fmt:message key="viewTask" /></a></li>
			            </c:if>
			            <c:if test="${siteConfig['BUDGET_ADD_PARTNER'].value == 'true'}">
			              <li class="link"><a href="../customers/partnerCustomerList.jhtm?cId=${customer.id}"><fmt:message key="addPartner" /></a></li>
				        </c:if>
			        </ul>
			    </div>
				</td>
			    </c:if>
			    <c:if test="${fn:contains(siteConfig['CUSTOMIZE_CUSTOMER_LIST'].value,'lastName')}">
			    <td class="nameCol">
			    <a style="color:black;" href="../customers/customer.jhtm?id=${customer.id}"><c:out value="${customer.address.lastName}"/></a>			   
			    </td>
			    </c:if>
			    <c:if test="${fn:contains(siteConfig['CUSTOMIZE_CUSTOMER_LIST'].value,'firstName')}">
			    <td class="nameCol"><c:out value="${customer.address.firstName}"/></td>
			    </c:if>
			    <c:if test="${fn:contains(siteConfig['CUSTOMIZE_CUSTOMER_LIST'].value,'email')}">
			    <td style="white-space: nowrap"><c:out value="${customer.username}"/>
			    </c:if>
			    	<c:if test="${gSiteConfig['gCUSTOMER_SUPPLIER'] and customer.supplierId != null}"><span class="sup">&sup1;</span><c:set var="hasSupplier" value="true"/></c:if>
			    	<c:if test="${customer.seeHiddenPrice}"><span class="sup">&sup2;</span><c:set var="seeHiddenPrice" value="true"/></c:if>
			    <c:if test="${fn:contains(siteConfig['CUSTOMIZE_CUSTOMER_LIST'].value,'company')}">
			    <td class="nameCol"><c:out value="${customer.address.company}"/></td>
			    </c:if>
			    <c:if test="${fn:contains(siteConfig['CUSTOMIZE_CUSTOMER_LIST'].value,'field_12')}">
			    <td class="center"><c:out value="${customer.field12}"/></td>
			    </c:if>
			    <c:if test="${fn:contains(siteConfig['CUSTOMIZE_CUSTOMER_LIST'].value,'supplierPrefix')}">
			    <td align="center"><c:out value="${customer.supplierPrefix}"/></td>
			    </c:if>
			    <c:if test="${fn:contains(siteConfig['CUSTOMIZE_CUSTOMER_LIST'].value,'trackCode')}">
			    <td align="center"><c:out value="${customer.trackcode}"/></td>
			    </c:if>
			    <c:if test="${fn:contains(siteConfig['CUSTOMIZE_CUSTOMER_LIST'].value,'accountNumber')}">
			    <td><c:out value="${customer.accountNumber}"/></td>
			    </c:if>
			    <c:if test="${fn:contains(siteConfig['CUSTOMIZE_CUSTOMER_LIST'].value,'phone')}">
			    <td><c:out value="${customer.address.phone}"/></td>
			    </c:if>
			    <c:if test="${fn:contains(siteConfig['CUSTOMIZE_CUSTOMER_LIST'].value,'taxId') and gSiteConfig['gTAX_EXEMPTION']}">
			    <td><c:out value="${customer.taxId}"/></td>
			    </c:if>
			    <c:if test="${fn:contains(siteConfig['CUSTOMIZE_CUSTOMER_LIST'].value,'created')}">
			    <td align="center"><fmt:formatDate type="date" timeStyle="default" value="${customer.created}"/></td>
			    </c:if>
			    <c:if test="${fn:contains(siteConfig['CUSTOMIZE_CUSTOMER_LIST'].value,'echoSign')}">
			    <td align="center"><c:if test="${model.echoSign[customer.id] != null}"></c:if><a href="<c:out value="${model.echoSign[customer.id].documentUrl}"/>" target="_blank"><c:out value="${model.echoSign[customer.id].documentName}"/></a></td>
			    </c:if>			    
			    <c:if test="${fn:contains(siteConfig['CUSTOMIZE_CUSTOMER_LIST'].value,'imported')}">
			    <td align="center">
			      <c:if test="${customer.imported}"><img src="../graphics/checkbox.png" border="0"></c:if>
				</td>
				</c:if>
				<c:if test="${fn:contains(siteConfig['CUSTOMIZE_CUSTOMER_LIST'].value,'registeredBy')}">
			    <td align="center"><c:out value="${customer.registeredBy}"/></td>
				</c:if>
				<c:if test="${fn:contains(siteConfig['CUSTOMIZE_CUSTOMER_LIST'].value,'logins')}">
			    <td class="countCol"><c:out value="${customer.numOfLogins}"/></td>
			    </c:if>
			    <c:if test="${(gSiteConfig['gSHOPPING_CART']) and (fn:contains(siteConfig['CUSTOMIZE_CUSTOMER_LIST'].value,'orders'))}">
			    <td align="center"><a href="../orders/ordersList.jhtm?email=${customer.username}"><c:out value="${customer.orderCount}"/></a></td>
				<td align="right"><fmt:formatNumber value="${customer.ordersGrandTotal}" pattern="#,##0.00" /></td>
				</c:if>
				<sec:authorize ifAnyGranted="ROLE_AEM,ROLE_ADMIN_${prop_site_id}">
				<c:if test="${gSiteConfig['gVIRTUAL_BANK_ACCOUNT']}">
				<td align="right"><a href="../customers/virtualBankPayment.jhtm?username=${customer.username}"><fmt:formatNumber value="${customer.supplierPay}" pattern="#,##0.00" /></a></td>
				</c:if>
				</sec:authorize>
				<c:if test="${(siteConfig['PRODUCT_QUOTE'].value == 'true' or gSiteConfig['gSHIPPING_QUOTE']) and fn:contains(siteConfig['CUSTOMIZE_CUSTOMER_LIST'].value,'quote')}">
			    <td align="center"><c:out value="${model.customerQuoteMap[customer.id].quoteCount}"/></td>
			    </c:if>
			    <c:if test="${fn:contains(siteConfig['CUSTOMIZE_CUSTOMER_LIST'].value,'qualifier')}">
			    <td align="center"><c:out value="${model.qualifierRepMap[customer.qualifier].name}"/></td>
			    </c:if>
			    <c:if test="${(gSiteConfig['gSALES_REP']) and (fn:contains(siteConfig['CUSTOMIZE_CUSTOMER_LIST'].value,'salesRep'))}">
			    <td align="center"><c:out value="${model.salesRepMap[customer.salesRepId].name}"/></td>
			    </c:if>
				<c:if test="${(gSiteConfig['gMULTI_STORE'] > 0) and (fn:contains(siteConfig['CUSTOMIZE_CUSTOMER_LIST'].value,'multiStore'))}">
			    <td class="nameCol"><c:out value="${customer.host}"/></td>
				</c:if>
			  </tr>
			</c:forEach>
			<c:if test="${model.count == 0}">
			  <tr class="emptyList"><td colspan="9">&nbsp;</td></tr>
			</c:if>
			</table>
			
			<c:if test="${hasSupplier}">
				<div class="sup quickMode">
					&sup1; <fmt:message key="supplier" />
				</div>
			</c:if>			
			<c:if test="${seeHiddenPrice}">
				<div class="sup quickMode">
					&sup2; <fmt:message key="SeeHiddenPrice" />
				</div>
			</c:if>	
			
			<table border="0" cellpadding="0" cellspacing="1" width="100%" class="quickMode">
			  <tr>
			  <td class="pageSize">
			  <select name="size" onchange="document.getElementById('page').value=1;submit()">
			  <c:forTokens items="10,25,50,100,200,500" delims="," var="current">
			  	  <option value="${current}" <c:if test="${current == customerSearch.pageSize}">selected</c:if>>${current} <fmt:message key="perPage" /></option>
			  </c:forTokens>
			  </select>
			  </td>
			  </tr>
			</table>
		  	<!-- end input field -->  	  
		  	</div>
		  	
		  	<!-- start button --> 
            <div align="left" class="button quickMode">
              <sec:authorize ifAnyGranted="ROLE_AEM,ROLE_ADMIN_${prop_site_id},ROLE_CUSTOMER_CREATE">
			    <input type="submit" name="__add" value="<fmt:message key="addCustomersMenuTitle"/>" />
			  </sec:authorize>
			  <c:if test="${model.count > 0 and siteConfig['ECHOSIGN_API_KEY'].value != ''}">
			    <input type="submit" name="__echoSign" value="<fmt:message key="send" /> EchoSign" onClick="return echoSignSelected()">
			  </c:if>
	  	    </div>
		  	<!-- end button -->	
		
	<!-- end tab -->        
	</div>
<!-- end tabs -->			
</div>
  
  <!-- end table -->
   </td><td class="boxmidr" ></td></tr>
  <tr><td class="botl"></td><td class="botr"></td></tr>
  </table>
	  
<!-- end main box -->  
</div>  
</form>	
<!--[if IE]>
<style>
.action-hover {
        margin-left:-12px;
        margin-top:1px;
}
</style>
<![EndIf]-->

</sec:authorize>

  </tiles:putAttribute>
</tiles:insertDefinition>
