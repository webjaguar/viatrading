<%@ page import="com.webjaguar.listener.SessionListener,java.util.*" %>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="/WEB-INF/tlds/spring.tld" prefix="spring" %>
<%@ taglib uri="http://www.springframework.org/security/tags" prefix="sec" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>

<%
String protectedLevels[] = new String[10];
for ( int i = 0; i < protectedLevels.length; i++ ) {
	StringBuffer protectedLevel = new StringBuffer( "1" );
	for ( int y = 0; y < i; y++ ) protectedLevel.append( "0" );
	protectedLevels[i] = protectedLevel.toString();
}
pageContext.setAttribute( "protectedLevels", protectedLevels);
%>

<tiles:insertDefinition name="admin.config" flush="true">
  <tiles:putAttribute name="content" type="string">
<script type="text/javascript"> 
<!--
window.addEvent('domready', function(){			
	var tabs1 = new SimpleTabs($('tab-block-1'), { selector: 'h4'});
	var Tips1 = new Tips($$('.toolTipImg'));
	if ($('ccPayment').value != 'authorizenet') {
		$('authTestMode').setStyle('display', 'none');
	}
	if(($('ccPayment').value != 'ebizcharge')) {
		$('pCIfriendlyeBiz').setStyle('display', 'none');
	}
	if (($('ccPayment').value != 'l19')) {
		$('l19TestMode').setStyle('display', 'none');
	}
});
function showAuthTestMode() 
{
	if (($('ccPayment').value == 'authorizenet' )) {
		$('authTestMode').setStyle('display', 'block');
	} else {
		$('authTestMode').setStyle('display', 'none');
	}
	
	if (($('ccPayment').value == 'ebizcharge')) {
		$('pCIfriendlyeBiz').setStyle('display', 'block');
	} else {
		$('pCIfriendlyeBiz').setStyle('display', 'none');
	}
	
	if (($('ccPayment').value == 'l19')) {
		$('l19TestMode').setStyle('display', 'block');
	} else {
		$('l19TestMode').setStyle('display', 'none');
	}
}
function togglePromo(id) 
{
	if (document.getElementById(id).value == 1) 
	{
	  document.getElementById('salesAndPromo1').style.display="block";
	  document.getElementById('salesAndPromo2').style.display="block";
	  document.getElementById('advancedPromotion').style.display="block";
	  document.getElementById('showEligiblePromos').style.display="block";
	} else 
	{
	  document.getElementById('salesAndPromo1').style.display="none";
	  document.getElementById('salesAndPromo2').style.display="none";
	  document.getElementById('advancedPromotion').style.display="none";
	  document.getElementById('showEligiblePromos').style.display="none";
	}
}

function toggleCommissionTable(id) 
{
    if (document.getElementById('gAFFILIATE').value >= 1) 
	{
	  document.getElementById('productAffiliateCommision1').style.display="block";
	  document.getElementById('childInvoice').style.display="block";
	  document.getElementById('numAffiliate').style.display="block";
	  document.getElementById('editCommission').style.display="block";
	} else 
	{
	  document.getElementById('productAffiliateCommision1').style.display="none";
	  document.getElementById('childInvoice').style.display="none";
	  document.getElementById('numAffiliate').style.display="none";
	  document.getElementById('editCommission').style.display="none";
	}
}

function toggleMinimumOrder(id) 
{
    if (document.getElementById(id).value == 'true' ) 
	{
	  document.getElementById('minimumOrderID').style.display="block";
	} else 
	{
	  document.getElementById('minimumOrderID').style.display="none";
	}
}

function toggleSalesRep(id)
{
    if (document.getElementById(id).value == 1 ) 
	{
	  document.getElementById('SalesRepID').style.display="block";
	} else 
	{
	  document.getElementById('SalesRepID').style.display="none";
	}
}

function toggleShoppingCartRecommendedList(id)
{
    if (document.getElementById(id).value == 1 ) 
	{
	  document.getElementById('ShoppingCartRecommendedList').style.display="block";
	} else 
	{
	  document.getElementById('ShoppingCartRecommendedList').style.display="none";
	}
}

function toggleAccessPrivilege(id)
{
    if (document.getElementById(id).checked ) 
	{
	  document.getElementById('accessBody').style.display="block";
	} else 
	{
	  document.getElementById('accessBody').style.display="none";
	}
}

function toggleCountyTax(id) 
{
	if (id == 'gCOUNTY_TAX' & document.getElementById('gCOUNTY_TAX').checked == false) 
	{
	  document.getElementById('gCITY_TAX').checked = false;
	}	
	if (id == 'gCITY_TAX' & document.getElementById('gCITY_TAX').checked) 
	{
	  document.getElementById('gCOUNTY_TAX').checked = true;
	}
}
function toggleInventory(id)
{
    if (document.getElementById(id).checked ) 
	{
	  <c:if test="${gSiteConfig['gINVENTORY']}">
	    $('outOfStockID').style.display="block";
	    $('lowStockID').style.display="block";
	    $('shoppingCartQtyAdjustID').style.display="block";
	    $('inventoryOnProductImExID').style.display="block";
	    $('inventoryUpdateFormID').style.display="block";
	    $('inventoryAFSID').style.display="block";
	  </c:if>
	    $('purchaseOrderID').style.display="block";
	    $('inventoryImportExportID').style.display="block";
	    $('inventoryTypeID').style.display="block";
	    
	} else 
	{
	  <c:if test="${gSiteConfig['gINVENTORY']}">
	    $('outOfStockID').style.display="none";
	    $('lowStockID').style.display="none";
	    $('shoppingCartQtyAdjustID').style.display="none";
	    $('inventoryOnProductImExID').style.display="none";
	    $('inventoryUpdateFormID').style.display="none";
	    $('inventoryAFSID').style.display="none";
	  </c:if>
	    $('purchaseOrderID').style.display="none";
	    $('inventoryImportExportID').style.display="none";
	    $('inventoryTypeID').style.display="none";
	}
}
function toggleGiftCard(id)
{
    if (document.getElementById(id).checked ) 
	{
	    $('minmaxAmount').style.display="block";
	} else 
	{
	    $('minmaxAmount').style.display="none";
	}
}
function toggleReviewType(id)
{	
	if (document.getElementById(id).checked ) 
	{
      document.getElementById('reviewType').style.display="block";
	  document.getElementById('PRODUCT_RATE').checked = false;
	} else 
	{
	  document.getElementById('reviewType').style.display="none";
	}
}
function toggleProductRate(id)
{	
	if (document.getElementById(id).checked ) 
	{
   	  document.getElementById('gPRODUCT_REVIEW').checked = false;
   	  document.getElementById('reviewType').style.display="none";
	}
}

//-->
</script>	

<form  action="configuration.jhtm" method="post">
<!-- main box -->
  <div id="mboxfull">
  
  <!-- start table -->
  <table cellpadding="0" cellspacing="0" border="0" class="module" >
  <tr>
    <td class="topl_g">
	  
	  <!-- breadcrumb -->
	  <p class="breadcrumb">
	    <a href="../config">SiteInfo</a> &gt;
	    <fmt:message key="siteConfigurationTitle"/>
	  </p>
	  
	  <!-- Error Message -->
	  <c:if test="${!empty message}">
		  <div class="message"><spring:message code="${message}" /></div>
	  </c:if>
	  
	  <!-- header image -->
	  <img class="headerImage" src="../graphics/siteInfo.gif" />
	  
		<div align="right">
		<table class="form">
		 <sec:authorize ifAllGranted="ROLE_AEM">
		  <tr>
		    <td align="right">Site Version:</td>
		    <td><c:out value="${gSiteConfig['gSITE_VERSION']}"/> (<fmt:formatDate type="date" value="${gSiteConfig['gSITE_VERSION_DATE']}" pattern="yyyyMMdd"/>)</td>
		  </tr>
		  <tr>
		    <td align="right">Date Added:</td>
		    <td><fmt:formatDate type="date" value="${gSiteConfig['gDATE_CREATED']}"/></td>
		  </tr>
		  <tr>
		    <td align="right">Date Upgraded:</td>
		    <td><fmt:formatDate type="date" value="${gSiteConfig['gDATE_UPGRADED']}"/></td>
		  </tr>
		  <tr>
		    <td align="right">Server Name:</td>
		    <td><c:out value="${gSiteConfig['server_name']}"/></td>
		  </tr>
		  <tr>
		    <td align="right">Tomcat Instance:</td>
		    <td><c:out value="${gSiteConfig['tomcat']}"/></td>
		  </tr>
		  <tr>
		    <td align="right"><input type="hidden" name="__gkey" value="gSITE_ACTIVE">Site <fmt:message key="active" />:</td>
		    <td><input type="checkbox" name="gSITE_ACTIVE" value ="1" <c:if test="${gSiteConfig['gSITE_ACTIVE']}">checked</c:if>></td>
		  </tr>
		  <c:if test="${accessUser != null}">
		  <input type="hidden" name="access_user" value="true"/>
		  <tr>
		    <td align="right">Admin Username:</td>
		    <td><c:out value="${accessUser.username}"/></td>
		  </tr>
		  <tr>
		    <td align="right">Admin Password:</td>
		    <td><c:out value="${accessUser.password}"/></td>
		  </tr>
		  <tr>
		    <td align="right">Admin <fmt:message key="active" />:</td>
		    <td><input type="checkbox" name="access_user_enable" <c:if test="${accessUser.enable}">checked</c:if>/></td>
		  </tr>
		  </c:if>
		  </sec:authorize>
		  <tr>
		    <td align="right"><input type="hidden" name="__gkey" value="gSESSION_TIMEOUT">Session Timeout:</td>
		    <td>
		      <select name="gSESSION_TIMEOUT">
		        <c:forEach items="10,20,30,45,60"  var="minutes">
				<option value="${minutes}" <c:if test="${gSiteConfig['gSESSION_TIMEOUT'] == minutes}">selected</c:if>><c:out value="${minutes}"/> minutes</option>
		        </c:forEach>
		      </select>
			</td>
		  </tr>		  
		  <sec:authorize ifAllGranted="ROLE_AEM">	  
		  <tr>
		    <td align="right">Active Sessions:</td>
		    <td><c:out value="${activeSessions}" /></td>
		  </tr>
		  <tr>
		    <td align="right">Logged in (Front-end):</td>
		    <td><c:out value="${loggedIn}" /></td>
		  </tr>
		  </sec:authorize>
		</table>
		</div>
	  
	  
	  </td><td class="topr_g" ></td></tr>
  <tr><td class="boxmidlrg" >

 <!-- tabs -->
 <div id="tab-block-1">
    <sec:authorize ifAnyGranted="ROLE_AEM,ROLE_ADMIN_${prop_site_id},ROLE_SITE_INFO">
	<h4 title="<fmt:message key="companyInformation" />"><fmt:message key="companyInformation" /></h4>
	<div>
	<!-- start tab -->

	  	<div class="listdivi ln tabdivi"></div>
	  	<div class="listdivi"></div>

			<div class="ConfigNavDialogTitle1">
			   <strong><fmt:message key="companyInformation" /></strong>
			</div>
			<div class="ConfigNavDialogTitle1Container" >
			<div class="title"></div>
		      <div class="form">
		       <label><input type="hidden" name="__key" value="COMPANY_NAME"><fmt:message key="companyName" /><img class="toolTipImg" title="Note::Required by FedEx." src="../graphics/question.gif" />:</label>
		       <input type="text" name="COMPANY_NAME" value="<c:out value="${siteConfig['COMPANY_NAME'].value}"/>" maxlength="255" size="30">
		      </div>  
		      <div class="form">
		       <label><input type="hidden" name="__key" value="COMPANY_CONTACT_PERSON"><fmt:message key="contactPerson" />:</label>
		       <input type="text" name="COMPANY_CONTACT_PERSON" value="<c:out value="${siteConfig['COMPANY_CONTACT_PERSON'].value}"/>" maxlength="255" size="30">
		      </div>  
		      <div class="form">
		       <label><input type="hidden" name="__key" value="COMPANY_ADDRESS1"><fmt:message key="address" />1:</label>
		       <input type="text" name="COMPANY_ADDRESS1" value="<c:out value="${siteConfig['COMPANY_ADDRESS1'].value}"/>" maxlength="255" size="30">
		      </div>  
		      <div class="form">
		       <label><input type="hidden" name="__key" value="COMPANY_ADDRESS2"><fmt:message key="address" />2:</label>
		       <input type="text" name="COMPANY_ADDRESS2" value="<c:out value="${siteConfig['COMPANY_ADDRESS2'].value}"/>" maxlength="255" size="30">
		      </div>  
		      <div class="form">
		       <label><input type="hidden" name="__key" value="COMPANY_CITY"><fmt:message key="city" />:</label>
		       <input type="text" name="COMPANY_CITY" value="<c:out value="${siteConfig['COMPANY_CITY'].value}"/>" maxlength="255" size="30">
		      </div>  
		      <div class="form">
		       <label><input type="hidden" name="__key" value="COMPANY_COUNTRY"><fmt:message key="country" />:</label>
		       <select id="country" name="COMPANY_COUNTRY" onchange="toggleStateProvince(this)">
		  	      <option value="">Please Select</option>
			      <c:forEach items="${countrylist}" var="country">
		  	        <option value="${country.code}" <c:if test="${country.code == siteConfig['COMPANY_COUNTRY'].value}">selected</c:if>>${country.name}</option>
				  </c:forEach>		
				</select>
		      </div>  
		      <div class="form">
		       <label><input type="hidden" name="__key" value="COMPANY_STATE_PROVINCE"><fmt:message key="stateProvince" />:</label>
		       <select id="state" name="COMPANY_STATE_PROVINCE">
		  	      <option value="">Please Select</option>
			      <c:forEach items="${statelist}" var="state">
		  	        <option value="${state.code}" <c:if test="${state.code == siteConfig['COMPANY_STATE_PROVINCE'].value}">selected</c:if>>${state.name}</option>
				  </c:forEach>      
		        </select>
				<select id="ca_province" name="COMPANY_STATE_PROVINCE">
		  	      <option value="">Please Select</option>
			      <c:forEach items="${caProvinceList}" var="province">
		  	        <option value="${province.code}" <c:if test="${province.code == siteConfig['COMPANY_STATE_PROVINCE'].value}">selected</c:if>>${province.name}</option>
				  </c:forEach>      
		        </select>                 
				<input type="text" id="province" name="COMPANY_STATE_PROVINCE" value="<c:out value="${siteConfig['COMPANY_STATE_PROVINCE'].value}"/>" maxlength="255" size="30">
		      </div>  
		      <div class="form">
		       <label><input type="hidden" name="__key" value="COMPANY_ZIPCODE"><fmt:message key="zipCode" />:</label>
		       <input type="text" name="COMPANY_ZIPCODE" value="<c:out value="${siteConfig['COMPANY_ZIPCODE'].value}"/>" maxlength="255" size="30">
		      </div>  
		      <div class="form">
		       <label><input type="hidden" name="__key" value="COMPANY_PHONE"><fmt:message key="phone" /><img class="toolTipImg" title="Note::Required by FedEx." src="../graphics/question.gif" />:</label>
		       <input type="text" name="COMPANY_PHONE" value="<c:out value="${siteConfig['COMPANY_PHONE'].value}"/>" maxlength="255" size="30">
		      </div>  
		      <div class="form">
		       <label><input type="hidden" name="__key" value="COMPANY_FAX"><fmt:message key="fax" />:</label>
		       <input type="text" name="COMPANY_FAX" value="<c:out value="${siteConfig['COMPANY_FAX'].value}"/>" maxlength="255" size="30">
		      </div>
		    </div>   
	<!-- end tab -->        
	</div>  
	</sec:authorize> 
	   
	<sec:authorize ifAnyGranted="ROLE_AEM,ROLE_ADMIN_${prop_site_id},ROLE_SITE_INFO">       	
	<h4 title="Customer Information">Site Configuration</h4>
	<div>
	<!-- start tab -->
	
	  	<div class="listdivi ln tabdivi"></div>
	  	<div class="listdivi"></div>		
	  		
			<div class="ConfigNavDialogTitle1">
			   <strong>Site Configuration</strong>
			</div>
			<div class="ConfigNavDialogTitle1Container" >			
			<div class="title">Common Store Options</div>
		     <div class="form">
		       <label><input type="hidden" name="__key" value="SITE_TITLE">Site Title:</label>
		       <input type="text" name="SITE_TITLE" value="<c:out value="${siteConfig['SITE_TITLE'].value}"/>" maxlength="255" size="50">
		     </div>  
		     <div class="form">
		       <label><input type="hidden" name="__key" value="SITE_NAME">Site Name:</label>
		       <input type="text" name="SITE_NAME" value="<c:out value="${siteConfig['SITE_NAME'].value}"/>" maxlength="255" size="50">
		     </div>  
		     <div class="form">
		       <label><input type="hidden" name="__key" value="CONTACT_EMAIL"><fmt:message key="contactEmail"/><img class="toolTipImg" title="Note::Required by FedEx. Accepts one email only." src="../graphics/question.gif" />:</label>
		       <input type="text" name="CONTACT_EMAIL" value="<c:out value="${siteConfig['CONTACT_EMAIL'].value}"/>" maxlength="255" size="50">
		     </div>
		     <div class="form">
		       <label><input type="hidden" name="__key" value="PRODUCT_REVIEW_EMAIL"><fmt:message key="reviewContactEmail"/><img class="toolTipImg" title="Note::Product review are sent to this email." src="../graphics/question.gif" />:</label>
		       <input type="text" name="PRODUCT_REVIEW_EMAIL" value="<c:out value="${siteConfig['PRODUCT_REVIEW_EMAIL'].value}"/>" maxlength="255" size="50">
		     </div>
		     <div class="form">
		       <label><input type="hidden" name="__key" value="MASS_EMAIL_CONTACT"><fmt:message key="massEmailContact"/><img class="toolTipImg" title="Note::Required for Mass Email. Accepts one email only. If user has email, it will be considered first. If not, this email will be considered. If this email is empty, contact email will be consdered." src="../graphics/question.gif" />:</label>
		       <input type="text" name="MASS_EMAIL_CONTACT" value="<c:out value="${siteConfig['MASS_EMAIL_CONTACT'].value}"/>" maxlength="255" size="50">
		     </div>
		     <div class="form">
		       <label><input type="hidden" name="__key" value="PRODUCT_IMPORT_EMAIL"><fmt:message key="product" /> <fmt:message key="import" /> <fmt:message key="email" /><img class="toolTipImg" title="Note::Accepts multiple emails separated by commas." src="../graphics/question.gif" />:</label>
		       <input type="text" name="PRODUCT_IMPORT_EMAIL" value="<c:out value="${siteConfig['PRODUCT_IMPORT_EMAIL'].value}"/>" maxlength="255" size="65">
		     </div> 
		     <div class="form">
		       <label><input type="hidden" name="__key" value="RFQ_EMAIL"><fmt:message key="quoteEmail"/><img class="toolTipImg" title="Note::Required for receiving Quotes requested by a customer. Accepts one email only." src="../graphics/question.gif" />:</label>
		       <input type="text" name="RFQ_EMAIL" value="<c:out value="${siteConfig['RFQ_EMAIL'].value}"/>" maxlength="255" size="50">
		     </div>
		     <c:if test="${gSiteConfig['gTICKET']}">
		     <div class="form">
		       <label><input type="hidden" name="__key" value="TICKET_CONTACT_EMAIL">Ticket Contact Email:</label>
		       <input type="text" name="TICKET_CONTACT_EMAIL" value="<c:out value="${siteConfig['TICKET_CONTACT_EMAIL'].value}"/>" maxlength="255" size="50">
		     </div> 
		     <div class="form">
		       <label><input type="hidden" name="__key" value="TICKET_MESSAGE_ON_EMAIL"><fmt:message key="ticketMessageOnEmail" />:</label>
		       <input type="checkbox" name="TICKET_MESSAGE_ON_EMAIL" value ="true" <c:if test="${siteConfig['TICKET_MESSAGE_ON_EMAIL'].value == 'true'}">checked</c:if>>
		     </div>
		     <div class="form">
		       <label><input type="hidden" name="__key" value="TICKET_TYPE">Ticket Type(separate with comma):</label>
		       <input type="text" name="TICKET_TYPE" value="<c:out value="${siteConfig['TICKET_TYPE'].value}"/>" maxlength="255" size="50">
		     </div>
		     </c:if>  
		     <div class="form">
		       <label><input type="hidden" name="__key" value="MAX_FRACTION_DIGIT"><fmt:message key="maxFraction" />:</label>
		       <select name="MAX_FRACTION_DIGIT">
		        <option value="2">2 Digit</option>
		        <option value="3" <c:if test="${'3' == siteConfig['MAX_FRACTION_DIGIT'].value}">selected</c:if>>3 Digit</option>
		 	  </select>
		     </div>
		     <div class="form">
		       <label><input type="hidden" name="__key" value="CURRENCY"><fmt:message key="currency" />:</label>
		       <select name="CURRENCY">
		        <option value="XXX"><fmt:message key="none" /></option>
		        <option value="USD" <c:if test="${'USD' == siteConfig['CURRENCY'].value}">selected</c:if>>$ Dollar</option>
		        <option value="EUR" <c:if test="${'EUR' == siteConfig['CURRENCY'].value}">selected</c:if>>&#8364; Euro</option>
		        <option value="GBP" <c:if test="${'GBP' == siteConfig['CURRENCY'].value}">selected</c:if>>&#163; Pound</option>
		        <%-- <option value="JPY" <c:if test="${'JPY' == siteConfig['CURRENCY'].value}">selected</c:if>>&#165; Yen</option> --%>
		 	  </select>
		     </div>
		     <c:if test="${!gSiteConfig['gREGISTRATION_DISABLED']}">
		     <div class="form">
		       <label><input type="hidden" name="__key" value="SITE_MESSAGE_ID_FOR_NEW_REGISTRATION"><fmt:message key="siteMessageNewRegistrants"/>:</label>
			  <select name="SITE_MESSAGE_ID_FOR_NEW_REGISTRATION">
		        <option value="0"></option>
			    <c:forEach var="siteMessage" items="${siteMessages}">
			    <option value="${siteMessage.messageId}" <c:if test="${siteMessage.messageId == siteConfig['SITE_MESSAGE_ID_FOR_NEW_REGISTRATION'].value}">selected</c:if>><c:out value="${siteMessage.messageName}"/></option>
			    </c:forEach>
		 	  </select>
		     </div>
		     </c:if>
		     <c:if test="${siteConfig['PRODUCT_QUOTE'].value == 'true'}">
		     <div class="form">
		       <label><input type="hidden" name="__key" value="SITE_MESSAGE_ID_FOR_NEW_QUOTE"><fmt:message key="siteMessageNewQuotes"/>:</label>
			  <select name="SITE_MESSAGE_ID_FOR_NEW_QUOTE">
		        <option value="0"></option>
			    <c:forEach var="siteMessage" items="${siteMessages}">
			    <option value="${siteMessage.messageId}" <c:if test="${siteMessage.messageId == siteConfig['SITE_MESSAGE_ID_FOR_NEW_QUOTE'].value}">selected</c:if>><c:out value="${siteMessage.messageName}"/></option>
			    </c:forEach>
		 	  </select>
		     </div>
		     <div class="form">
		       <label><input type="hidden" name="__key" value="QUOTE_ADMIN_EMAIL_SUBJECT"><fmt:message key="emailSubjectWhenAdminGetQuote"/>:</label>
			   <input type="text" name="QUOTE_ADMIN_EMAIL_SUBJECT" value="<c:out value="${siteConfig['QUOTE_ADMIN_EMAIL_SUBJECT'].value}"/>" maxlength="25" size="25">
		     </div>
		     </c:if>
		     <c:if test="${gSiteConfig['gSALES_REP']}">
		     <div class="form">
		       <label><input type="hidden" name="__key" value="SITE_MESSAGE_ID_FOR_SALESREP_NOTIFICATION"><fmt:message key="siteMessageSalesRepNotificationCustomer"/>:</label>
			  <select name="SITE_MESSAGE_ID_FOR_SALESREP_NOTIFICATION">
		        <option value="0"></option>
			    <c:forEach var="siteMessage" items="${siteMessages}">
			    <option value="${siteMessage.messageId}" <c:if test="${siteMessage.messageId == siteConfig['SITE_MESSAGE_ID_FOR_SALESREP_NOTIFICATION'].value}">selected</c:if>><c:out value="${siteMessage.messageName}"/></option>
			    </c:forEach>
		 	  </select>
		     </div>
		     </c:if>
		     <c:if test="${gSiteConfig['gSALES_REP']}">
		     <div class="form">
		       <label><input type="hidden" name="__key" value="SITE_MESSAGE_ID_FOR_SALESREP_NOTIFICATION_CONTACT"><fmt:message key="siteMessageSalesRepNotificationContact"/>:</label>
			  <select name="SITE_MESSAGE_ID_FOR_SALESREP_NOTIFICATION_CONTACT">
		        <option value="0"></option>
			    <c:forEach var="siteMessage" items="${siteMessages}">
			    <option value="${siteMessage.messageId}" <c:if test="${siteMessage.messageId == siteConfig['SITE_MESSAGE_ID_FOR_SALESREP_NOTIFICATION_CONTACT'].value}">selected</c:if>><c:out value="${siteMessage.messageName}"/></option>
			    </c:forEach>
		 	  </select>
		     </div>
		     </c:if> 	     		     
		     <c:if test="${gSiteConfig['gSHOPPING_CART']}">
		     <div class="form">
		       <label><input type="hidden" name="__key" value="SITE_MESSAGE_ID_FOR_NEW_ORDERS"><fmt:message key="siteMessageNewOrders"/>:</label>
			  <select name="SITE_MESSAGE_ID_FOR_NEW_ORDERS">
		        <option value="0"></option>
			    <c:forEach var="siteMessage" items="${siteMessages}">
			    <option value="${siteMessage.messageId}" <c:if test="${siteMessage.messageId == siteConfig['SITE_MESSAGE_ID_FOR_NEW_ORDERS'].value}">selected</c:if>><c:out value="${siteMessage.messageName}"/></option>
			    </c:forEach>
		 	  </select>
		     </div>
		     <c:if test="${gSiteConfig['gCUSTOMER_SUPPLIER']}">
		     <div class="form">
		       <label><input type="hidden" name="__key" value="SITE_MESSAGE_ID_FOR_SUPPLIER_ORDERS"><fmt:message key="siteMessageSupplierOrders"/>:</label>
			  <select name="SITE_MESSAGE_ID_FOR_SUPPLIER_ORDERS">
		        <option value="0"></option>
			    <c:forEach var="siteMessage" items="${siteMessages}">
			    <option value="${siteMessage.messageId}" <c:if test="${siteMessage.messageId == siteConfig['SITE_MESSAGE_ID_FOR_SUPPLIER_ORDERS'].value}">selected</c:if>><c:out value="${siteMessage.messageName}"/></option>
			    </c:forEach>
		 	  </select>
		     </div>
		     </c:if>		     
		     <c:if test="${'EVERGREEN' == gSiteConfig['gACCOUNTING']}">
		     <div class="form">
		       <label><input type="hidden" name="__key" value="SITE_MESSAGE_ID_FOR_SALES_REP"><fmt:message key="salesRepMassEmailSiteMessageID"/>:</label>
			   <input type="text" name="SITE_MESSAGE_ID_FOR_SALES_REP" value="<c:out value="${siteConfig['SITE_MESSAGE_ID_FOR_SALES_REP'].value}"/>" maxlength="25" size="25">
		     </div>
		     </c:if>
		     <c:if test="${siteConfig['DSI'].value != ''}">
		     <div class="form">
		       <label><input type="hidden" name="__key" value="SITE_MESSAGE_ID_FOR_SHIPPED_ORDERS"><fmt:message key="siteMessageShippedOrders"/>:</label>
			  <select name="SITE_MESSAGE_ID_FOR_SHIPPED_ORDERS">
		        <option value="0"></option>
			    <c:forEach var="siteMessage" items="${siteMessages}">
			    <option value="${siteMessage.messageId}" <c:if test="${siteMessage.messageId == siteConfig['SITE_MESSAGE_ID_FOR_SHIPPED_ORDERS'].value}">selected</c:if>><c:out value="${siteMessage.messageName}"/></option>
			    </c:forEach>
		 	  </select>
		     </div>
		     </c:if>
		     <c:if test="${gSiteConfig['gWORLDSHIP'] and siteConfig['WORLD_SHIP_EMAIL_NOTIFICATION'].value=='true'}">
		     <div class="form">
		       <label><input type="hidden" name="__key" value="SITE_MESSAGE_ID_FOR_WORLD_SHIP"><fmt:message key="siteMessageWorldShip"/>:</label>
			  <select name="SITE_MESSAGE_ID_FOR_WORLD_SHIP">
		        <option value="0"></option>
			    <c:forEach var="siteMessage" items="${siteMessages}">
			    <option value="${siteMessage.messageId}" <c:if test="${siteMessage.messageId == siteConfig['SITE_MESSAGE_ID_FOR_WORLD_SHIP'].value}">selected</c:if>><c:out value="${siteMessage.messageName}"/></option>
			    </c:forEach>
		 	  </select>
		     </div>
		     </c:if>
		     <c:if test="${gSiteConfig['gFEDEXSHIPMANAAGER'] and siteConfig['FEDEX_SHIPPING_LABEL_EMAIL_NOTIFICATION'].value=='true'}">
		     <div class="form">
		       <label><input type="hidden" name="__key" value="SITE_MESSAGE_ID_FOR_FEDEX_SHIPPING_LABEL"><fmt:message key="siteMessageFedExShipplingLabel"/>:</label>
			  <select name="SITE_MESSAGE_ID_FOR_FEDEX_SHIPPING_LABEL">
		        <option value="0"></option>
			    <c:forEach var="siteMessage" items="${siteMessages}">
			    <option value="${siteMessage.messageId}" <c:if test="${siteMessage.messageId == siteConfig['SITE_MESSAGE_ID_FOR_FEDEX_SHIPPING_LABEL'].value}">selected</c:if>><c:out value="${siteMessage.messageName}"/></option>
			    </c:forEach>
		 	  </select>
		     </div>
		     </c:if>		     
		     </c:if>
		     <div class="form">
		       <label><input type="hidden" name="__key" value="CONTACT_US_ID"><fmt:message key="contactUsCategoryId" />:</label>
		       <input type="text" name="CONTACT_US_ID" value="<c:out value="${siteConfig['CONTACT_US_ID'].value}"/>" maxlength="10" size="10">
		     </div> 
		     <div class="form">
		       <label><input type="hidden" name="__key" value="ABOUT_US_ID"><fmt:message key="aboutUsCategoryId" />:</label>
		       <input type="text" name="ABOUT_US_ID" value="<c:out value="${siteConfig['ABOUT_US_ID'].value}"/>" maxlength="10" size="10">
		     </div>
		     <div class="form">
		       <label><input type="hidden" name="__key" value="CATEGORY_NOT_FOUND_URL"><fmt:message key="categoryNotFoundUrl" />:</label>
		       <input type="text" name="CATEGORY_NOT_FOUND_URL" value="<c:out value="${siteConfig['CATEGORY_NOT_FOUND_URL'].value}"/>" maxlength="255" size="50">
		     </div>  
		     <c:if test="${siteConfig['CUSTOMER_IP_ADDRESS'].value == 'true'}">     
			 <div id="ipAddressErrorMessage" class="form">
			   <label><input type="hidden" name="__key" value="IP_ADDRESS_ERROR_MESSAGE"><fmt:message key="ipAddressErrorMessage" />:</label>
			   <input type="text" name="IP_ADDRESS_ERROR_MESSAGE" value="<c:out value="${siteConfig['IP_ADDRESS_ERROR_MESSAGE'].value}"/>" maxlength="255" size="50">
			 </div> 
			 </c:if>
			 <div class="form">
		       <label><input type="hidden" name="__key" value="DEFAULT_LIST_SIZE"><fmt:message key="listSize" />:</label>
		       <select name="DEFAULT_LIST_SIZE">
			     <c:forTokens items="10,25,50,100,200,500" delims="," var="value">
				  <option value="${value}" <c:if test="${value == siteConfig['DEFAULT_LIST_SIZE'].value}">selected</c:if>>${value}</option>
				 </c:forTokens>
			   </select>
		     </div> 
		     <div class="form">
		       <label><input type="hidden" name="__key" value="LOGIN_SUCCESS_URL"><fmt:message key="loginSuccessPage" />:</label>
		       <input type="text" name="LOGIN_SUCCESS_URL" value="<c:out value="${siteConfig['LOGIN_SUCCESS_URL'].value}"/>" maxlength="255" size="25">
		     </div> 
		     <div class="form">
		       <label><input type="hidden" name="__key" value="REGISTRATION_SUCCESS_URL"><fmt:message key="registrationSuccessPage" />:</label>
		       <input type="text" name="REGISTRATION_SUCCESS_URL" value="<c:out value="${siteConfig['REGISTRATION_SUCCESS_URL'].value}"/>" maxlength="255" size="25">
		     </div> 
		     <div class="form">
		       <label><input type="hidden" name="__key" value="NOTE_ON_REGISTRATION"><fmt:message key="noteOnRegistration" />:</label>
		       <input type="checkbox" name="NOTE_ON_REGISTRATION" value ="true" <c:if test="${siteConfig['NOTE_ON_REGISTRATION'].value == 'true'}">checked</c:if>>
		     </div> 
		     <div class="form">
		       <label><input type="hidden" name="__key" value="COOKIE_AGE">Cookie Age (Tracking<c:if test="${gSiteConfig['gSALES_PROMOTIONS']}"> and Promo</c:if> Code):</label>
		       <select name="COOKIE_AGE">
				  <option value="-1"><fmt:message key="default"/></option>
				  <option value="1" <c:if test="${'1' == siteConfig['COOKIE_AGE'].value}">selected</c:if>>1 day</option>
				  <option value="7" <c:if test="${'7' == siteConfig['COOKIE_AGE'].value}">selected</c:if>>1 week</option>
				  <option value="14" <c:if test="${'14' == siteConfig['COOKIE_AGE'].value}">selected</c:if>>2 weeks</option>
				  <option value="30" <c:if test="${'30' == siteConfig['COOKIE_AGE'].value}">selected</c:if>>1 month</option>
				  <option value="90" <c:if test="${'90' == siteConfig['COOKIE_AGE'].value}">selected</c:if>>3 month</option>
				  <option value="180" <c:if test="${'180' == siteConfig['COOKIE_AGE'].value}">selected</c:if>>6 month</option>
			   </select>	
		     </div>	
		     <div class="form">
		       <label><input type="hidden" name="__key" value="ADMIN_LANDING_PAGE">Admin Landing Page:</label>
		       <select name="ADMIN_LANDING_PAGE">
				  <option value="catalog/categoryList.jhtm"><fmt:message key="catalog"/></option>
				  <option value="catalog/productList.jhtm" <c:if test="${'catalog/productList.jhtm' == siteConfig['ADMIN_LANDING_PAGE'].value}">selected</c:if>><fmt:message key="product"/></option>
				  <option value="customers/customerList.jhtm" <c:if test="${'customers/customerList.jhtm' == siteConfig['ADMIN_LANDING_PAGE'].value}">selected</c:if>><fmt:message key="customer"/></option>
				  <option value="orders/ordersList.jhtm" <c:if test="${'orders/ordersList.jhtm' == siteConfig['ADMIN_LANDING_PAGE'].value}">selected</c:if>><fmt:message key="order"/></option>
				  <c:if test="${gSiteConfig['gREPORT']}">
				    <option value="reports/" <c:if test="${'reports/' == siteConfig['ADMIN_LANDING_PAGE'].value}">selected</c:if>><fmt:message key="reportTabTitle"/></option>
				  </c:if>
				  <c:if test="${!empty siteConfig['ADMIN_MESSAGE'].value}">
				    <option value="message/" <c:if test="${'message/' == siteConfig['ADMIN_LANDING_PAGE'].value}">selected</c:if>><fmt:message key="message"/></option>
				  </c:if>
			   </select>	
		     </div>	
		     <br />
		     <c:if test="${gSiteConfig['gPROTECTED'] > 0}">     
		     <div class="title"><fmt:message key="protectedAccess" /></div> 
			 <div class="form">
			   <label><fmt:message key="anonymous" />:</label>
			   <div class="protectedCheckBoxWrapper"><ul>
       		   <c:forEach items="${protectedAccessAnonymous}" var="protectedAccess" varStatus="protectedStatus">
				 <c:set var="key" value="protected${protectedStatus.count}"/>
		    	 <c:choose>
		      	   <c:when test="${labels[key] != null and labels[key] != ''}">
		        	 <c:set var="label" value="${labels[key]}"/>
		      	   </c:when>
		      	   <c:otherwise><c:set var="label" value="${protectedStatus.count}"/></c:otherwise>
		    	 </c:choose>       		   
  	             <li class="protectedLabelWrapper"><input name="PROTECTED_ACCESS_ANONYMOUS_${protectedStatus.index}" type="checkbox" <c:if test="${protectedAccess}">checked</c:if>><span class="protectedLabel"><c:out value="${label}"/></span></li>
	           </c:forEach></div>
			 </div> 
			 <div class="form">
			   <label><fmt:message key="newRegistrant" />:</label>
			   <div class="protectedCheckBoxWrapper"><ul>
       		   <c:forEach items="${protectedAccessNewRegistrant}" var="protectedAccess" varStatus="protectedStatus">
				 <c:set var="key" value="protected${protectedStatus.count}"/>
		    	 <c:choose>
		      	   <c:when test="${labels[key] != null and labels[key] != ''}">
		        	 <c:set var="label" value="${labels[key]}"/>
		      	   </c:when>
		      	   <c:otherwise><c:set var="label" value="${protectedStatus.count}"/></c:otherwise>
		    	 </c:choose> 
  	             <li class="protectedLabelWrapper"><input name="PROTECTED_ACCESS_NEW_REGISTRANT_${protectedStatus.index}" type="checkbox" <c:if test="${protectedAccess}">checked</c:if>><span class="protectedLabel"><c:out value="${label}"/></span></li>
	           </c:forEach>
	           </ul></div>
			 </div>
			 <div class="form">
			   <label><fmt:message key="newRegistrant" /> w/ <fmt:message key="trackcode" />:</label>
			   <div class="protectedCheckBoxWrapper"><ul>
       		   <c:forEach items="${tcProtectedAccessNewRegistrant}" var="protectedAccess" varStatus="protectedStatus">
				 <c:set var="key" value="protected${protectedStatus.count}"/>
		    	 <c:choose>
		      	   <c:when test="${labels[key] != null and labels[key] != ''}">
		        	 <c:set var="label" value="${labels[key]}"/>
		      	   </c:when>
		      	   <c:otherwise><c:set var="label" value="${protectedStatus.count}"/></c:otherwise>
		    	 </c:choose> 
  	             <li class="protectedLabelWrapper"><input name="TRACK_CODE_PROTECTED_ACCESS_NEW_REGISTRANT_${protectedStatus.index}" type="checkbox" <c:if test="${protectedAccess}">checked</c:if>><span class="protectedLabel"><c:out value="${label}"/></span></li>
	           </c:forEach>
	           </ul></div>
			 </div>
			 <div class="form">
			   <label><input type="hidden" name="__key" value="TRACK_CODE">&nbsp;</label>
	           <input type="text" name="TRACK_CODE" value="<c:out value="${siteConfig['TRACK_CODE'].value}"/>" size="30">
	           (<fmt:message key="trackcode" />)
			 </div>	 
			 </c:if>
			 <div class="title"><fmt:message key="priceProtectedAccess" /></div> 
			 <div class="form">
		       <label><input type="hidden" name="__key" value="PRICE_PROTECTED_ACCESS">Show To Anonymous:</label>
		       <select name="PRICE_PROTECTED_ACCESS">
				  <option value="0"><fmt:message key="regularPricing" /></option>
				  <option value="1" <c:if test="${'1' == siteConfig['PRICE_PROTECTED_ACCESS'].value}">selected</c:if>><fmt:message key="productPriceTable"/> 1</option>
				  <option value="2" <c:if test="${'2' == siteConfig['PRICE_PROTECTED_ACCESS'].value}">selected</c:if>><fmt:message key="productPriceTable"/> 2</option>
				  <option value="3" <c:if test="${'3' == siteConfig['PRICE_PROTECTED_ACCESS'].value}">selected</c:if>><fmt:message key="productPriceTable"/> 3</option>
				  <option value="4" <c:if test="${'4' == siteConfig['PRICE_PROTECTED_ACCESS'].value}">selected</c:if>><fmt:message key="productPriceTable"/> 4</option>
				  <option value="5" <c:if test="${'5' == siteConfig['PRICE_PROTECTED_ACCESS'].value}">selected</c:if>><fmt:message key="productPriceTable"/> 5</option>
				  <option value="6" <c:if test="${'6' == siteConfig['PRICE_PROTECTED_ACCESS'].value}">selected</c:if>><fmt:message key="productPriceTable"/> 6</option>
				  <option value="7" <c:if test="${'7' == siteConfig['PRICE_PROTECTED_ACCESS'].value}">selected</c:if>><fmt:message key="productPriceTable"/> 7</option>
				  <option value="8" <c:if test="${'8' == siteConfig['PRICE_PROTECTED_ACCESS'].value}">selected</c:if>><fmt:message key="productPriceTable"/> 8</option>
				  <option value="9" <c:if test="${'9' == siteConfig['PRICE_PROTECTED_ACCESS'].value}">selected</c:if>><fmt:message key="productPriceTable"/> 9</option>
				  <option value="10" <c:if test="${'10' == siteConfig['PRICE_PROTECTED_ACCESS'].value}">selected</c:if>><fmt:message key="productPriceTable"/> 10</option>
				  </select>
		     </div>
		      <div class="form">
		       <label><input type="hidden" name="__key" value="NEW_REGISTRANT_PRICE_PROTECTED_ACCESS">Assigned To New Registrant:</label>
		       <select name="NEW_REGISTRANT_PRICE_PROTECTED_ACCESS">
				  <option value="0"><fmt:message key="regularPricing" /></option>
				  <option value="1" <c:if test="${'1' == siteConfig['NEW_REGISTRANT_PRICE_PROTECTED_ACCESS'].value}">selected</c:if>><fmt:message key="productPriceTable"/> 1</option>
				  <option value="2" <c:if test="${'2' == siteConfig['NEW_REGISTRANT_PRICE_PROTECTED_ACCESS'].value}">selected</c:if>><fmt:message key="productPriceTable"/> 2</option>
				  <option value="3" <c:if test="${'3' == siteConfig['NEW_REGISTRANT_PRICE_PROTECTED_ACCESS'].value}">selected</c:if>><fmt:message key="productPriceTable"/> 3</option>
				  <option value="4" <c:if test="${'4' == siteConfig['NEW_REGISTRANT_PRICE_PROTECTED_ACCESS'].value}">selected</c:if>><fmt:message key="productPriceTable"/> 4</option>
				  <option value="5" <c:if test="${'5' == siteConfig['NEW_REGISTRANT_PRICE_PROTECTED_ACCESS'].value}">selected</c:if>><fmt:message key="productPriceTable"/> 5</option>
				  <option value="6" <c:if test="${'6' == siteConfig['NEW_REGISTRANT_PRICE_PROTECTED_ACCESS'].value}">selected</c:if>><fmt:message key="productPriceTable"/> 6</option>
				  <option value="7" <c:if test="${'7' == siteConfig['NEW_REGISTRANT_PRICE_PROTECTED_ACCESS'].value}">selected</c:if>><fmt:message key="productPriceTable"/> 7</option>
				  <option value="8" <c:if test="${'8' == siteConfig['NEW_REGISTRANT_PRICE_PROTECTED_ACCESS'].value}">selected</c:if>><fmt:message key="productPriceTable"/> 8</option>
				  <option value="9" <c:if test="${'9' == siteConfig['NEW_REGISTRANT_PRICE_PROTECTED_ACCESS'].value}">selected</c:if>><fmt:message key="productPriceTable"/> 9</option>
				  <option value="10" <c:if test="${'10' == siteConfig['NEW_REGISTRANT_PRICE_PROTECTED_ACCESS'].value}">selected</c:if>><fmt:message key="productPriceTable"/> 10</option>
				  </select>
		     </div> 
			 
		     <div class="title"><fmt:message key="productOptions" /></div> 
		     <div class="form">
		       <label><input type="hidden" name="__key" value="SHOW_SKU">Show SKU:</label>
		       <fmt:message key="yes" /> <input type="radio" name="SHOW_SKU" value="true" <c:if test="${siteConfig['SHOW_SKU'].value == 'true'}">checked</c:if>>
		       <fmt:message key="no" /> <input type="radio" name="SHOW_SKU" value="false" <c:if test="${siteConfig['SHOW_SKU'].value == 'false'}">checked</c:if>>
		     </div>  
		     <div class="form">
		       <label><input type="hidden" name="__key" value="SHOW_ZERO_PRICE">Show 0.00 price:</label>
		       <fmt:message key="yes" /> <input type="radio" name="SHOW_ZERO_PRICE" value="true" <c:if test="${siteConfig['SHOW_ZERO_PRICE'].value == 'true'}">checked</c:if>>
		       <fmt:message key="no" /> <input type="radio" name="SHOW_ZERO_PRICE" value="false" <c:if test="${siteConfig['SHOW_ZERO_PRICE'].value == 'false'}">checked</c:if>>
		     </div>  
		     <c:if test="${gSiteConfig['gSHOPPING_CART']}">
		     <div class="form">
		       <label><input type="hidden" name="__key" value="ZERO_PRICE_HTML">0.00 price HTML<img class="toolTipImg" title="Note::Replaces add to cart if price is zero." src="../graphics/question.gif" />:</label>
		       <input type="text" name="ZERO_PRICE_HTML" value="<c:out value="${siteConfig['ZERO_PRICE_HTML'].value}"/>" maxlength="100" size="25">
		     </div> 
		     </c:if>
		     <div class="form">
		       <label><input type="hidden" name="__key" value="MSRP_TITLE"><fmt:message key="productMsrp" /> Title:</label>
		       <input type="text" name="MSRP_TITLE" value="<c:out value="${siteConfig['MSRP_TITLE'].value}"/>" maxlength="50" size="25">
		     </div>
		     <div class="form">
		       <label><input type="hidden" name="__key" value="PRODUCT_FIELDS_TITLE">Product Fields Title:</label>
		       <input type="text" name="PRODUCT_FIELDS_TITLE" value="<c:out value="${siteConfig['PRODUCT_FIELDS_TITLE'].value}"/>" maxlength="50" size="25">
		     </div>  
		     <c:if test="${gSiteConfig['gRECOMMENDED_LIST']}">
		     <div class="form">
		       <label><input type="hidden" name="__key" value="RECOMMENDED_LIST_TITLE"><fmt:message key="recommendedList" /> <fmt:message key="title" />:</label>
		       <input type="text" name="RECOMMENDED_LIST_TITLE" value="<c:out value="${siteConfig['RECOMMENDED_LIST_TITLE'].value}"/>" maxlength="100" size="25">
		     </div>  
		     <div class="form">
		       <label><input type="hidden" name="__key" value="RECOMMENDED_LIST_DISPLAY"><fmt:message key="recommendedList" /> <fmt:message key="display" />:</label>
		       <select name="RECOMMENDED_LIST_DISPLAY">
		  	    <option value="1">1. <fmt:message key="default" /></option>
		  	    <option value="2" <c:if test="${siteConfig['RECOMMENDED_LIST_DISPLAY'].value == '2'}">selected</c:if>>2. <fmt:message key="default" /> 2</option>
		  	    <option value="3" <c:if test="${siteConfig['RECOMMENDED_LIST_DISPLAY'].value == '3'}">selected</c:if>>3. compact up</option>
		  	    <option value="4" <c:if test="${siteConfig['RECOMMENDED_LIST_DISPLAY'].value == '4'}">selected</c:if>>4. compact short Desc.</option> 
		  	    <option value="5" <c:if test="${siteConfig['RECOMMENDED_LIST_DISPLAY'].value == '5'}">selected</c:if>>5. compact down</option>
		  	    <option value="6" <c:if test="${siteConfig['RECOMMENDED_LIST_DISPLAY'].value == '6'}">selected</c:if>>6. compact popup</option>
		  	    <option value="7" <c:if test="${siteConfig['RECOMMENDED_LIST_DISPLAY'].value == '7'}">selected</c:if>>7. compact</option>
		  	    <option value="8" <c:if test="${siteConfig['RECOMMENDED_LIST_DISPLAY'].value == '8'}">selected</c:if>>8. compact one add to cart</option>
		  	    <option value="9" <c:if test="${siteConfig['RECOMMENDED_LIST_DISPLAY'].value == '9'}">selected</c:if>>9. compact first price</option>
		        <option value="10" <c:if test="${siteConfig['RECOMMENDED_LIST_DISPLAY'].value == '10'}">selected</c:if>>10. divs</option>
		        <option value="11" <c:if test="${siteConfig['RECOMMENDED_LIST_DISPLAY'].value == '11'}">selected</c:if>>11 price break</option>
		        <option value="12" <c:if test="${siteConfig['RECOMMENDED_LIST_DISPLAY'].value == '12'}">selected</c:if>>12. compact one add to cart (MSRP)</option>
		       	<c:if test="${gSiteConfig['gSITE_DOMAIN'] == 'viatrading.com' or gSiteConfig['gSITE_DOMAIN'] == 'test.com'}">		       	
				  <option value="15" <c:if test="${siteConfig['RECOMMENDED_LIST_DISPLAY'].value == '15'}">selected</c:if>>15. Via search Display</option>
		 		</c:if>
		        <option value="quick" <c:if test="${siteConfig['RECOMMENDED_LIST_DISPLAY'].value == 'quick'}">selected</c:if>><fmt:message key="quickMode"/></option>
		        <c:if test="${siteConfig['PRODUCT_QUICK_MODE_2'].value == 'true'}">
		        	<option value="quick2" <c:if test="${siteConfig['RECOMMENDED_LIST_DISPLAY'].value == 'quick2'}">selected</c:if>><fmt:message key="quickMode"/> 2</option>
		  	    </c:if>
		  	   </select>
		     </div> 
		     <div class="form">
		       <label><input type="hidden" name="__key" value="RECOMMENDED_LIST_SHOW_BREADCRUMB"><fmt:message key="showBreadCrumbOnRecommendedItem" />:</label>
		       <fmt:message key="yes" /> <input type="radio" name="RECOMMENDED_LIST_SHOW_BREADCRUMB" value="true" <c:if test="${siteConfig['RECOMMENDED_LIST_SHOW_BREADCRUMB'].value == 'true'}">checked</c:if>>
		       <fmt:message key="no" /> <input type="radio" name="RECOMMENDED_LIST_SHOW_BREADCRUMB" value="false" <c:if test="${siteConfig['RECOMMENDED_LIST_SHOW_BREADCRUMB'].value == 'false'}">checked</c:if>>
		     </div>
		     </c:if>
		     <c:if test="${siteConfig['UPSELL_RECOMMENDED_LIST'].value == 'true'}">
		     <div class="form">
		       <label><input type="hidden" name="__key" value="UPSELL_DEFAULT_RECOMMENDED_LIST"><fmt:message key="upsellDefaultRecommendedList" /><img class="toolTipImg" title="Note::List Recommanded SKUs seperated with comma" src="../graphics/question.gif" />:</label>
		       <input type="text" name="UPSELL_DEFAULT_RECOMMENDED_LIST" value="<c:out value="${siteConfig['UPSELL_DEFAULT_RECOMMENDED_LIST'].value}"/>" maxlength="100" size="25">
		     </div>
		     </c:if>  
		     <c:if test="${gSiteConfig['gALSO_CONSIDER']}"> 
		     <div class="form">
		       <label><input type="hidden" name="__key" value="ALSO_CONSIDER_TITLE"><fmt:message key="alsoConsider" /> <fmt:message key="title" />:</label>
		       <input type="text" name="ALSO_CONSIDER_TITLE" value="<c:out value="${siteConfig['ALSO_CONSIDER_TITLE'].value}"/>" maxlength="100" size="25">
		     </div>  
		     <div class="form">
		       <label><input type="hidden" name="__key" value="ALSO_CONSIDER_THUMB_HEIGHT"><fmt:message key="alsoConsider" /> <fmt:message key="thumbnailHeight" />:</label>
			   <select name="ALSO_CONSIDER_THUMB_HEIGHT">
			      <option value=""><fmt:message key="actual" /></option>  
				<c:forEach begin="50" end="100" step="50" var="value">
				  <option value="${value}" <c:if test="${value == siteConfig['ALSO_CONSIDER_THUMB_HEIGHT'].value}">selected</c:if>>${value}</option>
				</c:forEach>
			   </select>	
		     </div> 
		     <div class="form">
		       <label><input type="hidden" name="__key" value="ALSO_CONSIDER_DETAILS_ONLY"><fmt:message key="alsoConsider" /> <fmt:message key="details" /> <fmt:message key="only" />:</label>
		       <fmt:message key="yes" /> <input type="radio" name="ALSO_CONSIDER_DETAILS_ONLY" value="true" <c:if test="${siteConfig['ALSO_CONSIDER_DETAILS_ONLY'].value == 'true'}">checked</c:if>>
		       <fmt:message key="no" /> <input type="radio" name="ALSO_CONSIDER_DETAILS_ONLY" value="false" <c:if test="${siteConfig['ALSO_CONSIDER_DETAILS_ONLY'].value == 'false'}">checked</c:if>>
		     </div>       
		     </c:if>
		     <c:if test="${gSiteConfig['gCASE_CONTENT']}">  
		     <div class="form">
		       <label><input type="hidden" name="__key" value="CASE_UNIT_TITLE"><fmt:message key="caseUnitTitle" />:</label>
		       <input type="text" name="CASE_UNIT_TITLE" value="<c:out value="${siteConfig['CASE_UNIT_TITLE'].value}"/>" size="30">
		     </div> 
		     </c:if> 
		     <c:if test="${gSiteConfig['gPRICE_CASEPACK']}">
		     <div class="form">
		       <label><input type="hidden" name="__key" value="PRICE_CASE_PACK_UNIT_TITLE"><fmt:message key="priceCasePackUnitTitle" />:</label>
		       <input type="text" name="PRICE_CASE_PACK_UNIT_TITLE" value="<c:out value="${siteConfig['PRICE_CASE_PACK_UNIT_TITLE'].value}"/>" size="30">
		     </div> 
		     </c:if>
		     <c:if test="${gSiteConfig['gSALES_PROMOTIONS']}">
		     <div class="form">
		       <label><input type="hidden" name="__key" value="PRICE_ONSALE_TITLE"><fmt:message key="priceOnsaleTitle" />:</label>
		       <input type="text" name="PRICE_ONSALE_TITLE" value="<c:out value="${siteConfig['PRICE_ONSALE_TITLE'].value}"/>" size="30">
		     </div> 
		     <div class="form">
		       <label><input type="hidden" name="__key" value="PRICE_BEFORE_TITLE"><fmt:message key="priceBeforeSaleTitle" />:</label>
		       <input type="text" name="PRICE_BEFORE_TITLE" value="<c:out value="${siteConfig['PRICE_BEFORE_TITLE'].value}"/>" size="30">
		     </div> 
		     </c:if>
		     <div class="form">
		       <label><input type="hidden" name="__key" value="DETAILS_IMAGE_WIDTH"><fmt:message key="details" /> <fmt:message key="image" /> <fmt:message key="width" />:</label>
			   <select name="DETAILS_IMAGE_WIDTH">
			    <option value=""><fmt:message key="actual" /></option>
			    <c:forEach begin="150" end="500" step="50" var="value">
			     <option value="${value}" <c:if test="${value == siteConfig['DETAILS_IMAGE_WIDTH'].value}">selected</c:if>>${value}</option>
			    </c:forEach>
			   </select>	
		     </div>  
		     <div class="form">
		       <label><input type="hidden" name="__key" value="DETAILS_IMAGE_LOCATION"><fmt:message key="details" /> <fmt:message key="image" /> <fmt:message key="location" />:</label>
			   <select name="DETAILS_IMAGE_LOCATION">
			    <option value="left"><fmt:message key="left" /></option>
			    <option value="top" <c:if test="${'top' == siteConfig['DETAILS_IMAGE_LOCATION'].value}">selected</c:if>><fmt:message key="top" /></option>
			    <option value="middle" <c:if test="${'middle' == siteConfig['DETAILS_IMAGE_LOCATION'].value}">selected</c:if>><fmt:message key="middle" /></option>
			   </select>	
		     </div>
		     <div class="form">
		       <label><input type="hidden" name="__key" value="DETAILS_SHORT_DESC"><fmt:message key="details" /> <fmt:message key="productShortDesc" />:</label>
		       <fmt:message key="yes" /> <input type="radio" name="DETAILS_SHORT_DESC" value="true" <c:if test="${siteConfig['DETAILS_SHORT_DESC'].value == 'true'}">checked</c:if>>
		       <fmt:message key="no" /> <input type="radio" name="DETAILS_SHORT_DESC" value="false" <c:if test="${siteConfig['DETAILS_SHORT_DESC'].value == 'false'}">checked</c:if>>
		     </div>  
		     <div class="form">
		       <label><input type="hidden" name="__key" value="DETAILS_LONG_DESC_LOCATION"><fmt:message key="details" /> <fmt:message key="productLongDesc" /> <fmt:message key="location" />:</label>
			   <select name="DETAILS_LONG_DESC_LOCATION">
			    <option value=""><fmt:message key="below" /></option>
			    <option value="above" <c:if test="${'above' == siteConfig['DETAILS_LONG_DESC_LOCATION'].value}">selected</c:if>><fmt:message key="above" /></option>
			   </select>	
		     </div>		     
		     <div class="form">
		       <label><input type="hidden" name="__key" value="DETAILS_LONG_DESC_HTML"><fmt:message key="details" /> <fmt:message key="productLongDesc" />:</label>
			   <select name="DETAILS_LONG_DESC_HTML">
			    <option value="">HTML</option>
			    <option value="false" <c:if test="${'false' == siteConfig['DETAILS_LONG_DESC_HTML'].value}">selected</c:if>>non HTML</option>
			   </select>	
		     </div>
		     <c:if test="${gSiteConfig['gCOMPARISON']}">
		     <div class="form">
		       <label><input type="hidden" name="__key" value="COMPARISON_TITLE"><fmt:message key="comparison" /> <fmt:message key="title" />:</label>
		       <input type="text" name="COMPARISON_TITLE" value="<c:out value="${siteConfig['COMPARISON_TITLE'].value}"/>" maxlength="100" size="25">
		     </div> 
		     <div class="form">
		       <label><input type="hidden" name="__key" value="COMPARISON_MAX"><fmt:message key="maxComparisons" />:</label>
			   <select name="COMPARISON_MAX">
			    <c:forEach begin="2" end="10" step="1" var="value">
			     <option value="${value}" <c:if test="${value == siteConfig['COMPARISON_MAX'].value}">selected</c:if>>${value}</option>
			    </c:forEach>
			   </select>     
			 </div> 
		     <div class="form">
		       <label><input type="hidden" name="__key" value="COMPARISON_SHORT_DESC_TITLE"><fmt:message key="comparison" /> <fmt:message key="productShortDesc" /> <fmt:message key="title" />:</label>
		       <input type="text" name="COMPARISON_SHORT_DESC_TITLE" value="<c:out value="${siteConfig['COMPARISON_SHORT_DESC_TITLE'].value}"/>" maxlength="100" size="25">
		     </div> 
		     </c:if>
		     <div class="form">
		       <label><input type="hidden" name="__key" value="THUMBNAIL_RESIZE"><fmt:message key="thumbnail" /> <fmt:message key="resize" />:</label>
			   <select name="THUMBNAIL_RESIZE">
			    <c:forTokens items="100,200,300" delims="," var="thumbsize">
			    <option value="${thumbsize}" <c:if test="${thumbsize == siteConfig['THUMBNAIL_RESIZE'].value}">selected</c:if>>${thumbsize} x ${thumbsize}</option>
			    </c:forTokens>
			   </select>	
		     </div>
		     <div class="form">
		       <label><input type="hidden" name="__key" value="PRODUCT_IMAGE_LAYOUT"><fmt:message key="image" /> <fmt:message key="layout" />:</label>
			   <select name="PRODUCT_IMAGE_LAYOUT">
		          <option value=""><fmt:message key="default"/></option> 
		          <c:forTokens items="${siteConfig['PRODUCT_IMAGE_LAYOUTS'].value}" delims="," var="slideshow">
		            <option value="${slideshow}" <c:if test="${slideshow == siteConfig['PRODUCT_IMAGE_LAYOUT'].value}">selected</c:if>><fmt:message key="${slideshow}" /></option>
		          </c:forTokens>
			   </select>	
		     </div>	     
		     <div class="form">
		       <label><input type="hidden" name="__key" value="PRODUCT_REVIEW_DEFAULT"><fmt:message key="productReviewDefault" />:</label>
		       <select name="PRODUCT_REVIEW_DEFAULT">
			     <option value="true" <c:if test="${siteConfig['PRODUCT_REVIEW_DEFAULT'].value == 'true'}">selected</c:if>>true</option>
			     <option value="false" <c:if test="${siteConfig['PRODUCT_REVIEW_DEFAULT'].value == 'false'}">selected</c:if>>false</option>
			   </select>
		     </div>
		     <div class="form">
		       <label><input type="hidden" name="__key" value="PRODUCT_SEARCHABLE_DEFAULT"><fmt:message key="productSearchableDefault" />:</label>
		       <select name="PRODUCT_SEARCHABLE_DEFAULT">
			     <option value="true" <c:if test="${siteConfig['PRODUCT_SEARCHABLE_DEFAULT'].value == 'true'}">selected</c:if>>true</option>
			     <option value="false" <c:if test="${siteConfig['PRODUCT_SEARCHABLE_DEFAULT'].value == 'false'}">selected</c:if>>false</option>
			   </select>
		     </div>
		     <c:if test="${siteConfig['HTML_SCROLLER'].value == 'true'}">
		     <div class="form">
		       <label><input type="hidden" name="__key" value="HTML_SCROLLER_WITH_PRICE">Price on Html Scroller:</label>
		       <input type="checkbox" name="HTML_SCROLLER_WITH_PRICE" value ="true" <c:if test="${siteConfig['HTML_SCROLLER_WITH_PRICE'].value == 'true'}">checked</c:if>>
		     </div>
		     </c:if>
		     <div class="form">
		       <label><input type="hidden" name="__key" value="PRODUCT_SEARCH_NOT_FOUND_URL"><fmt:message key="productSearchNotFoundUrl"/></label>
		       <input type="text" name="PRODUCT_SEARCH_NOT_FOUND_URL" value="<c:out value="${siteConfig['PRODUCT_SEARCH_NOT_FOUND_URL'].value}"/>" maxlength="255" size="50">
		     </div>		     
		     <c:if test="${gSiteConfig['gMASTER_SKU']}">
		     <div class="form">
		       <label><input type="hidden" name="__key" value="MASTER_SKU_PRODUCT_FIELD">Parent Sku Product Field:</label>
		       <input type="text" name="MASTER_SKU_PRODUCT_FIELD" value="<c:out value="${siteConfig['MASTER_SKU_PRODUCT_FIELD'].value}"/>" maxlength="4" size="20">
		     </div>
		     <div class="form">
		       <label><input type="hidden" name="__key" value="APPLY_PARENT_SKU_OPTION"><fmt:message key="applyParentSkuOption" />:</label>
		       <input type="checkbox" name="APPLY_PARENT_SKU_OPTION" value ="true" <c:if test="${siteConfig['APPLY_PARENT_SKU_OPTION'].value == 'true'}">checked</c:if>>
		     </div>
		     <div class="form">
		       <label><input type="hidden" name="__key" value="PARENT_CHILDERN_DISPLAY"><fmt:message key="parentChildrenDisplay" />:</label>
		       <select name="PARENT_CHILDERN_DISPLAY">
			     <option value="0" <c:if test="${siteConfig['PARENT_CHILDERN_DISPLAY'].value == 0}">selected</c:if>>Hide Children</option>
			     <option value="1" <c:if test="${siteConfig['PARENT_CHILDERN_DISPLAY'].value == 1}">selected</c:if>>Show Children</option>
			   </select>
		     </div>
		     <div class="form">
		       <label><input type="hidden" name="__key" value="COLOR_FIELD_ID">Color Field:</label>
		       <input type="text" name="COLOR_FIELD_ID" value="<c:out value="${siteConfig['COLOR_FIELD_ID'].value}"/>" maxlength="4" size="20">
		     </div>
		     <div class="form">
		       <label><input type="hidden" name="__key" value="SIZE_FIELD_ID">Size Field:</label>
		       <input type="text" name="SIZE_FIELD_ID" value="<c:out value="${siteConfig['SIZE_FIELD_ID'].value}"/>" maxlength="4" size="20">
		     </div>
		     </c:if>
		     <div class="form">
		       <label><input type="hidden" name="__key" value="SEARCH_RESULT_DISPALY_FIELD_ID">Search Result Display Field Id:</label>
		       <input type="text" name="SEARCH_RESULT_DISPALY_FIELD_ID" value="<c:out value="${siteConfig['SEARCH_RESULT_DISPALY_FIELD_ID'].value}"/>" maxlength="4" size="20">
		       <div class="helpNote" style="padding-left: 140px;">
			     <p>Field on Search Result: This field will appear on search result and category. Works on view 7 only and products with PPG layout. </p>
			   </div>
		     </div>
		     <div class="form">
		       <label><input type="hidden" name="__key" value="PRODUCT_URL_FIELD_REDIRECT">Product URL REDIRECT FIELD:</label>
		       <input type="text" name="PRODUCT_URL_FIELD_REDIRECT" value="<c:out value="${siteConfig['PRODUCT_URL_FIELD_REDIRECT'].value}"/>" maxlength="4" size="20">
		       <div class="helpNote" style="padding-left: 140px;">
			     <p>Field on Product URL REDIRECT: When show product link, we will check this product field, if it is not empty, we will show this field's content. </p>
			   </div>
		     </div>
		     <c:if test="${gSiteConfig['gGIFTCARD']}">
		     <div class="title"><fmt:message key="giftCardOptions" /></div>
		     <div class="form">
		       <label><input type="hidden" name="__key" value="GIFTCARD_MIN_AMOUNT"><input type="hidden" name="__key" value="GIFTCARD_MAX_AMOUNT"><fmt:message key="giftCardRangeAmount" />:</label>
		       <fmt:message key="min" /><input type="text" name="GIFTCARD_MIN_AMOUNT" value="<c:out value="${siteConfig['GIFTCARD_MIN_AMOUNT'].value}"/>" maxlength="4" size="5">
		       <fmt:message key="max" /><input type="text" name="GIFTCARD_MAX_AMOUNT" value="<c:out value="${siteConfig['GIFTCARD_MAX_AMOUNT'].value}"/>" maxlength="4" size="5">
		     </div>
		     <div class="form">
		       <label><input type="hidden" name="__key" value="SITE_MESSAGE_ID_FOR_GIFT_CARD"><fmt:message key="siteMessageGiftCard"/>:</label>
			  <select name="SITE_MESSAGE_ID_FOR_GIFT_CARD">
		        <option value="0"></option>
			    <c:forEach var="siteMessage" items="${siteMessages}">
			    <option value="${siteMessage.messageId}" <c:if test="${siteMessage.messageId == siteConfig['SITE_MESSAGE_ID_FOR_GIFT_CARD'].value}">selected</c:if>><c:out value="${siteMessage.messageName}"/></option>
			    </c:forEach>
		 	  </select>
		     </div>
		     <div class="form">
		       <label><input type="hidden" name="__key" value="GIFT_CARD_THANK_YOU_ID"><fmt:message key="giftCardThankYouCategoryId" />:</label>
		       <input type="text" name="GIFT_CARD_THANK_YOU_ID" value="<c:out value="${siteConfig['GIFT_CARD_THANK_YOU_ID'].value}"/>" maxlength="10" size="10">
		     </div> 
		     </c:if>
		     <div class="title"><fmt:message key="promoCode" /></div>
		     <div class="form">
		       <label><input type="hidden" name="__key" value="SITE_MESSAGE_ID_FOR_COUPON_CODE"><fmt:message key="siteMessageCouponCode"/>:</label>
			  <select name="SITE_MESSAGE_ID_FOR_COUPON_CODE">
		        <option value="0"></option>
			    <c:forEach var="siteMessage" items="${siteMessages}">
			    <option value="${siteMessage.messageId}" <c:if test="${siteMessage.messageId == siteConfig['SITE_MESSAGE_ID_FOR_COUPON_CODE'].value}">selected</c:if>><c:out value="${siteMessage.messageName}"/></option>
			    </c:forEach>
		 	  </select>
		     </div>
		     
		     <c:if test="${gSiteConfig['gINVENTORY']}">
		     <div class="title"><fmt:message key="inventoryOptions" /></div>
		     <div id="outOfStockID" class="form">
		       <label><input type="hidden" name="__key" value="OUT_OF_STOCK_TITLE"><fmt:message key="outOfStock" /> <fmt:message key="title" />:</label>
		       <input type="text" name="OUT_OF_STOCK_TITLE" value="<c:out value="${siteConfig['OUT_OF_STOCK_TITLE'].value}"/>" maxlength="100" size="25">
		       <input type="hidden" name="__key" value="OUT_OF_STOCK_CONDITION">
		       <select name="OUT_OF_STOCK_CONDITION">
		         <option value="0" <c:if test="${'0' == siteConfig['OUT_OF_STOCK_CONDITION'].value}">selected</c:if>>Show on Negative Inventory AND Dont Allow to Buy Negative Inventory</option>
			     <option value="1" <c:if test="${'1' == siteConfig['OUT_OF_STOCK_CONDITION'].value}">selected</c:if>>Show on Negative Inventory Only</option>
			   </select>
		     </div> 
		     <div id="lowStockID" class="form">
		       <label><input type="hidden" name="__key" value="LOW_STOCK_TITLE"><fmt:message key="lowStock" /> <fmt:message key="title" />:</label>
		       <input type="text" name="LOW_STOCK_TITLE" value="<c:out value="${siteConfig['LOW_STOCK_TITLE'].value}"/>" maxlength="100" size="25">
		     </div> 
		     <div id="inventoryAFSID" class="form">
		       <label><input type="hidden" name="__key" value="INVENTORY_ON_HAND"><fmt:message key="inventoryOnHand" /> <fmt:message key="title" />:</label>
		       <input type="text" name="INVENTORY_ON_HAND" value="<c:out value="${siteConfig['INVENTORY_ON_HAND'].value}"/>" maxlength="100" size="25">
		     </div> 
		     <div id="shoppingCartQtyAdjustID" class="form">
		       <label><input type="hidden" name="__key" value="INVENTORY_ADJUST_QTY_TITLE"><fmt:message key="inventoryAdjustQty" /> <fmt:message key="title" />:</label>
		       <input type="text" name="INVENTORY_ADJUST_QTY_TITLE" value="<c:out value="${siteConfig['INVENTORY_ADJUST_QTY_TITLE'].value}"/>" maxlength="100" size="25">
		     </div> 
		     		     <!-- PO -->
		     
		     <c:if test="${gSiteConfig['gPURCHASE_ORDER']}">
			     <div class="form">
			       <label><input type="hidden" name="__key" value="PO_SPECIAL_INSTRUCTIONS">PO Special Instructions:</label>
			       <input type="text" name="PO_SPECIAL_INSTRUCTIONS" value="<c:out value="${siteConfig['PO_SPECIAL_INSTRUCTIONS'].value}"/>" maxlength="255" size="50">
			     </div>
			     <div class="form">
		           <label><input type="hidden" name="__key" value="PO_REMOVE_CUSTOMER_CONTACT"><fmt:message key="removeCustomerContactOnShipToAddress" />:</label>
		           <input type="checkbox" name="PO_REMOVE_CUSTOMER_CONTACT" value ="true" <c:if test="${siteConfig['PO_REMOVE_CUSTOMER_CONTACT'].value == 'true'}">checked</c:if>>
		           <div class="helpNote" style="padding-left: 140px;">
				      <p>PO: Remove Tel, Fax, email on ShipTo box on Print View and PDF view.</p>
			       </div>
		         </div>
		        <div class="form">
		        	<label><input type="hidden" name="__key" value="PO_STAGING_CHECKLIST">PO Checklist before Staging:</label>	        
		            <input type="text" name="PO_STAGING_CHECKLIST" value="<c:out value="${siteConfig['PO_STAGING_CHECKLIST'].value}"/>" maxlength="255" size="50">
                 </div>
                 <div class="form">
		        	<label><input type="hidden" name="__key" value="PO_LOCATION_AFTERSTAGING">PO Location of Merchandise after staging:</label>	        
		            <input type="text" name="PO_LOCATION_AFTERSTAGING" value="<c:out value="${siteConfig['PO_LOCATION_AFTERSTAGING'].value}"/>" maxlength="255" size="50">
                 </div>
                 <div class="form">
		        	<label><input type="hidden" name="__key" value="PO_PRICING_INSTRUCTIONS">PO Pricing Instructions:</label>	        
		            <input type="text" name="PO_PRICING_INSTRUCTIONS" value="<c:out value="${siteConfig['PO_PRICING_INSTRUCTIONS'].value}"/>" maxlength="255" size="50">
                 </div>
		         
		         
		         
		     </c:if>
		     </c:if>
		     <br /> 
		     <div class="title">Search Options</div>
		     <div class="form">
		       <label><input type="hidden" name="__key" value="SEARCH_DEFAULT_KEYWORDS_CONJUNCTION"><fmt:message key="default" /> <fmt:message key="keywords" /> <fmt:message key="search" /></label>
		       <select name="SEARCH_DEFAULT_KEYWORDS_CONJUNCTION">
		  	    <option value="OR">at least one word must match</option>
		  	    <option value="AND" <c:if test="${siteConfig['SEARCH_DEFAULT_KEYWORDS_CONJUNCTION'].value == 'AND'}">selected</c:if>>all words must match</option>
		 	   </select>
		 	   <input type="hidden" name="__key" value="SEARCH_KEYWORDS_CONJUNCTION">
		       <select name="SEARCH_KEYWORDS_CONJUNCTION">
		  	    <option value="false">Don't Show</option>
		  	    <option value="true" <c:if test="${siteConfig['SEARCH_KEYWORDS_CONJUNCTION'].value == 'true'}">selected</c:if>>Show</option>
		 	   </select>
		     </div>          
		     <div class="form">
		       <label><input type="hidden" name="__key" value="SEARCH_KEYWORDS_DELIMITER"><fmt:message key="keywords" /> Delimiter:</label>
		       <select name="SEARCH_KEYWORDS_DELIMITER">
		  	    <option value=""><fmt:message key="none" /></option>
		  	    <option value=" " <c:if test="${siteConfig['SEARCH_KEYWORDS_DELIMITER'].value == ' '}">selected</c:if>>space</option>
		  	    <option value="," <c:if test="${siteConfig['SEARCH_KEYWORDS_DELIMITER'].value == ','}">selected</c:if>>comma</option>
		 	   </select>
		     </div>         
		     <div class="form">
		       <label><input type="hidden" name="__key" value="SEARCH_KEYWORDS_MINIMUM_CHARACTERS"><fmt:message key="keywordsMinChars" />:</label>
		       <select name="SEARCH_KEYWORDS_MINIMUM_CHARACTERS">
				<c:forEach begin="0" end="5" var="characters">
		  	    <option value="${characters}" <c:if test="${characters == siteConfig['SEARCH_KEYWORDS_MINIMUM_CHARACTERS'].value}">selected</c:if>>${characters}</option>
				</c:forEach>		 	   
			   </select>
		     </div> 
		     <div class="form">
		       <label><input type="hidden" name="__key" value="SEARCH_SORT"><fmt:message key="sortBy" /></label>
		       <select name="SEARCH_SORT">
		        <c:set var="SEARCH_SORT_found" value="false"/>
		  	    <option value="name" <c:if test="${siteConfig['SEARCH_SORT'].value == 'name'}"><c:set var="SEARCH_SORT_found" value="true"/>selected</c:if>><fmt:message key="Name" /></option>
		  	    <option value="sku" <c:if test="${siteConfig['SEARCH_SORT'].value == 'sku'}"><c:set var="SEARCH_SORT_found" value="true"/>selected</c:if>><fmt:message key="sku" /></option>
		  	    <option value="abs(sku)" <c:if test="${siteConfig['SEARCH_SORT'].value == 'abs(sku)'}"><c:set var="SEARCH_SORT_found" value="true"/>selected</c:if>><fmt:message key="sku" /> (<fmt:message key="numeric" />)</option>
			    <option value="rand()" <c:if test="${siteConfig['SEARCH_SORT'].value == 'rand()'}"><c:set var="SEARCH_SORT_found" value="true"/>selected</c:if>><fmt:message key="random" /></option>
			    <c:if test="${SEARCH_SORT_found != true}">
			      <option value="${siteConfig['SEARCH_SORT'].value}" selected><c:out value="${siteConfig['SEARCH_SORT'].value}"/></option>
			    </c:if>
		 	   </select>
		     </div>
		     <div class="form">
		       <label><input type="hidden" name="__key" value="NUMBER_PRODUCT_PER_PAGE_SEARCH"><fmt:message key="listSize" />:</label>
		       <select name="NUMBER_PRODUCT_PER_PAGE_SEARCH">
			     <c:forTokens items="10,25,50,75,100" delims="," var="value">
				  <option value="${value}" <c:if test="${value == siteConfig['NUMBER_PRODUCT_PER_PAGE_SEARCH'].value}">selected</c:if>>${value}</option>
				 </c:forTokens>
			   </select>
		     </div> 
		     <div class="form">
		       <label><input type="hidden" name="__key" value="SEARCH_MINMAXPRICE">Show Min/Max Price<img class="toolTipImg" title="Note::Works if all prices are based on regular pricing." src="../graphics/question.gif" />:</label>
		       <input type="checkbox" name="SEARCH_MINMAXPRICE" value ="true" <c:if test="${siteConfig['SEARCH_MINMAXPRICE'].value == 'true'}">checked</c:if>>
		     </div>
		     <div class="form">
		       <label><input type="hidden" name="__key" value="SEARCH_DISPLAY_MODE"><fmt:message key="searchDisplayMode" /></label>
		       <select name="SEARCH_DISPLAY_MODE">
		  	    <option value="quick"><fmt:message key="quickMode"/><fmt:message key="default" /></option>
		  	    <c:if test="${siteConfig['PRODUCT_QUICK_MODE_2'].value == 'true'}">
		  	     <option value="quick2"><fmt:message key="quickMode"/> 2<fmt:message key="default" /></option>
		  	    </c:if>
		  	    <option value="1" <c:if test="${siteConfig['SEARCH_DISPLAY_MODE'].value == '1'}">selected</c:if>>1. <fmt:message key="default" /> 1</option>
		  	    <option value="2" <c:if test="${siteConfig['SEARCH_DISPLAY_MODE'].value == '2'}">selected</c:if>>2. <fmt:message key="default" /> 2</option>
		  	    <option value="3" <c:if test="${siteConfig['SEARCH_DISPLAY_MODE'].value == '3'}">selected</c:if>>3. compact up</option>
		  	    <option value="4" <c:if test="${siteConfig['SEARCH_DISPLAY_MODE'].value == '4'}">selected</c:if>>4. compact short Desc.</option> 
		  	    <option value="5" <c:if test="${siteConfig['SEARCH_DISPLAY_MODE'].value == '5'}">selected</c:if>>5. compact down</option>
		  	    <option value="6" <c:if test="${siteConfig['SEARCH_DISPLAY_MODE'].value == '6'}">selected</c:if>>6. compact popup</option>
		  	    <option value="7" <c:if test="${siteConfig['SEARCH_DISPLAY_MODE'].value == '7'}">selected</c:if>>7. compact</option>
		  	    <option value="8" <c:if test="${siteConfig['SEARCH_DISPLAY_MODE'].value == '8'}">selected</c:if>>8. compact one add to cart</option>
		  	    <option value="9" <c:if test="${siteConfig['SEARCH_DISPLAY_MODE'].value == '9'}">selected</c:if>>9. compact first price</option>
		        <option value="10" <c:if test="${siteConfig['SEARCH_DISPLAY_MODE'].value == '10'}">selected</c:if>>10. divs</option>
		        <option value="11" <c:if test="${siteConfig['SEARCH_DISPLAY_MODE'].value == '11'}">selected</c:if>>11 price break</option>
		        <option value="12" <c:if test="${siteConfig['SEARCH_DISPLAY_MODE'].value == '12'}">selected</c:if>>12. compact one add to cart (MSRP)</option>
		        <c:if test="${gSiteConfig['gSITE_DOMAIN'] == 'viatrading.com' or gSiteConfig['gSITE_DOMAIN'] == 'test.com'}">
				  <option value="15" <c:if test="${siteConfig['SEARCH_DISPLAY_MODE'].value == '15'}">selected</c:if>>15. Via search Display</option>
		 	    </c:if>
		 	    <c:if test="${siteConfig['GROUP_SEARCH'].value == 'true'}">
		 	      <option disabled="disabled" style="font-weight: bolder; text-align: center;">Group Search</option>
				  <option value="13" <c:if test="${siteConfig['SEARCH_DISPLAY_MODE'].value == '13'}">selected</c:if>>1. Group Search</option>
		 	    </c:if>
		 	   </select>
		     </div> 
		     <c:if test="${siteConfig['LUCENE_REINDEX'].value == 'true'}">
		     <div class="form">
		      <label><input type="hidden" name="__key" value="LUCENE_PROXIMITY_VALUE"><fmt:message key="luceneSearchProximityValue"/>:</label>
		       <select name="LUCENE_PROXIMITY_VALUE">
		       	<c:forEach begin="0" end="100" var="proximity" step="10">
		  	   		<option value="${proximity}" <c:if test="${proximity == siteConfig['LUCENE_PROXIMITY_VALUE'].value}">selected</c:if>>${proximity}</option>
		  	  	</c:forEach>
		  	   </select>
		     </div> 
		     </c:if>
		     <br />
		     <div class="title">Faq Options</div>   
		     <div class="form">
		       <label><input type="hidden" name="__key" value="DEFAULT_FAQ_LIST_SIZE"><fmt:message key="list" /> <fmt:message key="size" />:</label>
		       <select name="DEFAULT_FAQ_LIST_SIZE">
				<c:forEach begin="10" end="50" var="size" step="10">
		  	    <option value="${size}" <c:if test="${size == siteConfig['DEFAULT_FAQ_LIST_SIZE'].value}">selected</c:if>>${size}</option>
				</c:forEach>
			  </select>
		     </div>  
		     <div class="title">Category Options</div>   
		     <div class="form">
		       <label><input type="hidden" name="__key" value="SUBCAT_LEVELS"><fmt:message key="global" /> <fmt:message key="subcatLevels" />:</label>
		       <select name="SUBCAT_LEVELS">
				<c:forEach begin="0" end="2" var="subcatLevels">
		  	    <option value="${subcatLevels}" <c:if test="${subcatLevels == siteConfig['SUBCAT_LEVELS'].value}">selected</c:if>>${subcatLevels}</option>
				</c:forEach>
				<option value="3" <c:if test="${subcatLevels == siteConfig['SUBCAT_LEVELS'].value}">selected</c:if>>acc</option>
		 	   </select>
		     </div>  
		     <div class="form">
		       <label><input type="hidden" name="__key" value="SUBCAT_DISPLAY"><fmt:message key="subcatDisplay" />:</label>
		       <select name="SUBCAT_DISPLAY">
				<option value=""><fmt:message key="column" /></option>
				<option value="row" <c:if test="${siteConfig['SUBCAT_DISPLAY'].value == 'row'}">selected</c:if>><fmt:message key="row" /></option>
				<option value="cell" <c:if test="${siteConfig['SUBCAT_DISPLAY'].value == 'cell'}">selected</c:if>>Cell</option>
				<option value="onePerRow" <c:if test="${siteConfig['SUBCAT_DISPLAY'].value == 'onePerRow'}">selected</c:if>>1 per Row</option>
		 	   </select>
		   	   <input type="hidden" name="__key" value="SUBCAT_PRODUCT_COUNT">
		 	   <select name="SUBCAT_PRODUCT_COUNT">
				<option value="false">&nbsp;</option>
				<option value="true" <c:if test="${siteConfig['SUBCAT_PRODUCT_COUNT'].value == 'true'}">selected</c:if>><fmt:message key="categoryProductCount" /></option>
		 	   </select>
		     </div> 
		     <div class="form">
		       <label><input type="hidden" name="__key" value="SUBCAT_MAX"><fmt:message key="max" /> <fmt:message key="categorySubCount" /><img class="toolTipImg" title="Note::Number of subcats to display before showing More..." src="../graphics/question.gif" />:</label>
		       <select name="SUBCAT_MAX">
				<c:forEach begin="5" end="20" var="subcatMax">
		  	    <option value="${subcatMax}" <c:if test="${subcatMax == siteConfig['SUBCAT_MAX'].value}">selected</c:if>>${subcatMax}</option>
				</c:forEach>
		 	   </select>
		     </div>		     		      
		    <div class="form">
		       <label><input type="hidden" name="__key" value="CATEGORY_DISPLAY_MODE"><fmt:message key="global" /> <fmt:message key="category" /> <fmt:message key="displayMode" />:</label>
		        <select name="CATEGORY_DISPLAY_MODE" id="CATEGORY_DISPLAY_MODE">
		  	     <option value="1">1. <fmt:message key="default" /></option>
		  	     <option value="2" <c:if test="${siteConfig['CATEGORY_DISPLAY_MODE'].value == '2'}">selected</c:if>>2. <fmt:message key="default" /> 2</option>
		  	     <option value="3" <c:if test="${siteConfig['CATEGORY_DISPLAY_MODE'].value == '3'}">selected</c:if>>3. compact up</option>
		  	     <option value="4" <c:if test="${siteConfig['CATEGORY_DISPLAY_MODE'].value == '4'}">selected</c:if>>4. compact short Desc.</option> 
		  	     <option value="5" <c:if test="${siteConfig['CATEGORY_DISPLAY_MODE'].value == '5'}">selected</c:if>>5. compact down</option>
		  	     <option value="6" <c:if test="${siteConfig['CATEGORY_DISPLAY_MODE'].value == '6'}">selected</c:if>>6. compact popup</option>
		  	     <option value="7" <c:if test="${siteConfig['CATEGORY_DISPLAY_MODE'].value == '7'}">selected</c:if>>7. compact</option>
		  	     <option value="7-1" <c:if test="${siteConfig['CATEGORY_DISPLAY_MODE'].value == '7-1'}">selected</c:if>>7-1. compact (Parent Product)</option>
		  	     <option value="8" <c:if test="${siteConfig['CATEGORY_DISPLAY_MODE'].value == '8'}">selected</c:if>>8. compact one add to cart</option>
		  	     <option value="8-1" <c:if test="${siteConfig['CATEGORY_DISPLAY_MODE'].value == '8-1'}">selected</c:if>>8-1. compact one add to cart</option>
		  	     <option value="9" <c:if test="${siteConfig['CATEGORY_DISPLAY_MODE'].value == '9'}">selected</c:if>>9. compact first price</option>
		         <option value="10" <c:if test="${siteConfig['CATEGORY_DISPLAY_MODE'].value == '10'}">selected</c:if>>10. divs</option>
		         <option value="11" <c:if test="${siteConfig['CATEGORY_DISPLAY_MODE'].value == '11'}">selected</c:if>>11 price break</option>
		         <option value="12" <c:if test="${siteConfig['CATEGORY_DISPLAY_MODE'].value == '12'}">selected</c:if>>12</option>
		         <c:if test="${siteConfig['PRODUCT_QUICK_VIEW'].value == 'true'}">
		          <option value="13" <c:if test="${siteConfig['CATEGORY_DISPLAY_MODE'].value == '13'}">selected</c:if>>13. compact down new</option>
		          <option value="14" <c:if test="${siteConfig['CATEGORY_DISPLAY_MODE'].value == '14'}">selected</c:if>>14. compact down new 14</option>
		         <c:if test="${gSiteConfig['gSITE_DOMAIN'] == 'viatrading.com' or gSiteConfig['gSITE_DOMAIN'] == 'wj20test.com'}">
		          <option value="15" <c:if test="${siteConfig['CATEGORY_DISPLAY_MODE'].value == '15'}">selected</c:if>>15. compact down new 15</option>
		         </c:if>
		         <c:if test="${gSiteConfig['gSITE_DOMAIN'] == 'myevergreen.com'}">
		          <option value="16" <c:if test="${siteConfig['CATEGORY_DISPLAY_MODE'].value == '16'}">selected</c:if>>16. compact down new 16</option>
		         </c:if>
		         </c:if>
		         <option value="17" <c:if test="${siteConfig['CATEGORY_DISPLAY_MODE'].value == '17'}">selected</c:if>>17. compact short Desc. One add to cart</option>
		         <c:if test="${siteConfig['PRODUCT_QUICK_VIEW'].value == 'true'}">
		         <option value="18" <c:if test="${siteConfig['CATEGORY_DISPLAY_MODE'].value == '18'}">selected</c:if>>18. compact short Desc.</option>
		         </c:if>
		         <option value="19" <c:if test="${siteConfig['CATEGORY_DISPLAY_MODE'].value == '19'}">selected</c:if>>19. One product per row with LongDesc</option>
		         <option value="quick" <c:if test="${siteConfig['CATEGORY_DISPLAY_MODE'].value == 'quick'}">selected</c:if>><fmt:message key="quickMode"/></option>
		         <c:if test="${siteConfig['PRODUCT_QUICK_MODE_2'].value == 'true'}">
		         	<option value="quick2" <c:if test="${siteConfig['CATEGORY_DISPLAY_MODE'].value == 'quick2'}">selected</c:if>><fmt:message key="quickMode"/> 2</option>
		         	<option value="quick2-1" <c:if test="${siteConfig['CATEGORY_DISPLAY_MODE'].value == 'quick2-1'}">selected</c:if>><fmt:message key="quickMode"/> 2-1</option>
		         	<option value="quick2-2" <c:if test="${siteConfig['CATEGORY_DISPLAY_MODE'].value == 'quick2-2'}">selected</c:if>><fmt:message key="quickMode"/> 2-2</option>
		         </c:if>
		        </select>
		     </div>
		     <div class="form">
		       <label><input type="hidden" name="__key" value="CATEGORY_DISPLAY_MODE_GRID_LIST"><fmt:message key="global" /> <fmt:message key="category" /> <fmt:message key="displayMode" />(Grid-List):</label>
		        <select name="CATEGORY_DISPLAY_MODE_GRID_LIST" id="CATEGORY_DISPLAY_MODE_GRID_LIST">
		         <option value=""></option>
		  	     <option value="1" <c:if test="${siteConfig['CATEGORY_DISPLAY_MODE_GRID_LIST'].value == '1'}">selected</c:if>>1. <fmt:message key="default" /></option>
		  	     <option value="2" <c:if test="${siteConfig['CATEGORY_DISPLAY_MODE_GRID_LIST'].value == '2'}">selected</c:if>>2. <fmt:message key="default" /> 2</option>
		  	     <option value="3" <c:if test="${siteConfig['CATEGORY_DISPLAY_MODE_GRID_LIST'].value == '3'}">selected</c:if>>3. compact up</option>
		  	     <option value="4" <c:if test="${siteConfig['CATEGORY_DISPLAY_MODE_GRID_LIST'].value == '4'}">selected</c:if>>4. compact short Desc.</option> 
		  	     <option value="5" <c:if test="${siteConfig['CATEGORY_DISPLAY_MODE_GRID_LIST'].value == '5'}">selected</c:if>>5. compact down</option>
		  	     <option value="6" <c:if test="${siteConfig['CATEGORY_DISPLAY_MODE_GRID_LIST'].value == '6'}">selected</c:if>>6. compact popup</option>
		  	     <option value="7" <c:if test="${siteConfig['CATEGORY_DISPLAY_MODE_GRID_LIST'].value == '7'}">selected</c:if>>7. compact</option>
		  	     <option value="7-1" <c:if test="${siteConfig['CATEGORY_DISPLAY_MODE_GRID_LIST'].value == '7-1'}">selected</c:if>>7-1. compact (Parent Product)</option>
		  	     <option value="8" <c:if test="${siteConfig['CATEGORY_DISPLAY_MODE_GRID_LIST'].value == '8'}">selected</c:if>>8. compact one add to cart</option>
		  	     <option value="8-1" <c:if test="${siteConfig['CATEGORY_DISPLAY_MODE_GRID_LIST'].value == '8-1'}">selected</c:if>>8-1. compact one add to cart</option>
		  	     <option value="9" <c:if test="${siteConfig['CATEGORY_DISPLAY_MODE_GRID_LIST'].value == '9'}">selected</c:if>>9. compact first price</option>
		         <option value="10" <c:if test="${siteConfig['CATEGORY_DISPLAY_MODE_GRID_LIST'].value == '10'}">selected</c:if>>10. divs</option>
		         <option value="11" <c:if test="${siteConfig['CATEGORY_DISPLAY_MODE_GRID_LIST'].value == '11'}">selected</c:if>>11 price break</option>
		         <option value="12" <c:if test="${siteConfig['CATEGORY_DISPLAY_MODE_GRID_LIST'].value == '12'}">selected</c:if>>12</option>
		         <c:if test="${siteConfig['PRODUCT_QUICK_VIEW'].value == 'true'}">
		          <option value="13" <c:if test="${siteConfig['CATEGORY_DISPLAY_MODE_GRID_LIST'].value == '13'}">selected</c:if>>13. compact down new</option>
		          <option value="14" <c:if test="${siteConfig['CATEGORY_DISPLAY_MODE_GRID_LIST'].value == '14'}">selected</c:if>>14. compact down new 14</option>
		         <c:if test="${gSiteConfig['gSITE_DOMAIN'] == 'viatrading.com' or gSiteConfig['gSITE_DOMAIN'] == 'wj20test.com'}">
		          <option value="15" <c:if test="${siteConfig['CATEGORY_DISPLAY_MODE_GRID_LIST'].value == '15'}">selected</c:if>>15. compact down new 15</option>
		         </c:if>
		         <c:if test="${gSiteConfig['gSITE_DOMAIN'] == 'myevergreen.com'}">
		          <option value="16" <c:if test="${siteConfig['CATEGORY_DISPLAY_MODE_GRID_LIST'].value == '16'}">selected</c:if>>16. compact down new 16</option>
		         </c:if>
		         </c:if>
		         <option value="17" <c:if test="${siteConfig['CATEGORY_DISPLAY_MODE_GRID_LIST'].value == '17'}">selected</c:if>>17. compact short Desc. One add to cart</option>
		         <c:if test="${siteConfig['PRODUCT_QUICK_VIEW'].value == 'true'}">
		         <option value="18" <c:if test="${siteConfig['CATEGORY_DISPLAY_MODE_GRID_LIST'].value == '18'}">selected</c:if>>18. compact short Desc.</option>
		         </c:if>
		         <option value="19" <c:if test="${siteConfig['CATEGORY_DISPLAY_MODE'].value == '19'}">selected</c:if>>19. One product per row with LongDesc</option>
		         <option value="quick" <c:if test="${siteConfig['CATEGORY_DISPLAY_MODE_GRID_LIST'].value == 'quick'}">selected</c:if>><fmt:message key="quickMode"/></option>
		         <c:if test="${siteConfig['PRODUCT_QUICK_MODE_2'].value == 'true'}">
		         	<option value="quick2" <c:if test="${siteConfig['CATEGORY_DISPLAY_MODE_GRID_LIST'].value == 'quick2'}">selected</c:if>><fmt:message key="quickMode"/> 2</option>
		         	<option value="quick2-1" <c:if test="${siteConfig['CATEGORY_DISPLAY_MODE_GRID_LIST'].value == 'quick2-1'}">selected</c:if>><fmt:message key="quickMode"/> 2-1</option>
		         	<option value="quick2-2" <c:if test="${siteConfig['CATEGORY_DISPLAY_MODE_GRID_LIST'].value == 'quick2-2'}">selected</c:if>><fmt:message key="quickMode"/> 2-2</option>
		         </c:if>
		        </select>
		     </div>   
		     <div class="form" id="numberProductPerRow">
		       <label><input type="hidden" name="__key" value="NUMBER_PRODUCT_PER_ROW"><fmt:message key="numProductPerRow" /><img class="toolTipImg" title="Note::This is not applicable for Quick Mode, Default, or Default 2 Category Display Modes" src="../graphics/question.gif" />:</label>
		       <select name="NUMBER_PRODUCT_PER_ROW">
				<c:forEach begin="1" end="5" step="1" var="value">
				  <option value="${value}" <c:if test="${value == siteConfig['NUMBER_PRODUCT_PER_ROW'].value}">selected</c:if>>${value}</option>
				</c:forEach>
			   </select>
		     </div>  
		     <div class="form">
		       <label><input type="hidden" name="__key" value="NUMBER_PRODUCT_PER_PAGE"> <fmt:message key="productPerPage" />:</label>
		       <select name="NUMBER_PRODUCT_PER_PAGE">
				<c:forEach begin="1" end="100" var="products">
		  	    <option value="${products}" <c:if test="${products == siteConfig['NUMBER_PRODUCT_PER_PAGE'].value}">selected</c:if>>${products}</option>
				</c:forEach>
		 	  </select>
		     </div>  
		     <div class="form">
		       <label><input type="hidden" name="__key" value="PRODUCT_SORTING"> <fmt:message key="productSorting" />:</label>
		       <select name="PRODUCT_SORTING">
		        <option value=""><fmt:message key="rank" /></option>
		  	    <option value="name"><fmt:message key="Name" /></option>
		  	    <option value="sku" <c:if test="${siteConfig['PRODUCT_SORTING'].value == 'sku'}">selected</c:if>><fmt:message key="productSku" /></option>
		  	    <option value="created desc" <c:if test="${siteConfig['PRODUCT_SORTING'].value == 'created desc'}">selected</c:if>><fmt:message key="dateAdded" /></option>
		 	  </select>
		     </div> 
		     <div class="form">
		       <label><input type="hidden" name="__key" value="QUICK_MODE_THUMB_HEIGHT"><fmt:message key="quickMode" /> <fmt:message key="thumbnailHeight" />:</label>
		       <select name="QUICK_MODE_THUMB_HEIGHT">
				<c:forEach begin="0" end="200" step="50" var="value">
				  <option value="${value}" <c:if test="${value == siteConfig['QUICK_MODE_THUMB_HEIGHT'].value}">selected</c:if>>${value}</option>
				</c:forEach>
				  <option value="" <c:if test="${'' == siteConfig['QUICK_MODE_THUMB_HEIGHT'].value}">selected</c:if>><fmt:message key="actual" /></option>
			   </select>	
		     </div>  
		     <div class="form">
		       <label><input type="hidden" name="__key" value="QUICK_MODE_SHORT_DESC_TITLE"><fmt:message key="quickMode" /> <fmt:message key="productShortDesc" /> <fmt:message key="title" />:</label>
		       <input type="text" name="QUICK_MODE_SHORT_DESC_TITLE" value="<c:out value="${siteConfig['QUICK_MODE_SHORT_DESC_TITLE'].value}"/>" maxlength="100" size="25">
		     </div>    
		     <div class="form">
		       <label><input type="hidden" name="__key" value="BREADCRUMBS_HOME_TITLE"><fmt:message key="breadCrumbs" /> Home <fmt:message key="title" />:</label>
		       <input type="text" name="BREADCRUMBS_HOME_TITLE" value="<c:out value="${siteConfig['BREADCRUMBS_HOME_TITLE'].value}"/>" size="25">
		     </div>
		     <c:if test="${gSiteConfig['gWATERMARK']}"> 
		     <div class="form">
		       <label><input type="hidden" name="__key" value="WATERMARK_TEXT">Watermark Text:</label>
		       <input type="text" name="WATERMARK_TEXT" value="<c:out value="${siteConfig['WATERMARK_TEXT'].value}"/>" maxlength="255" size="50">
		     </div>  
		     <div class="form">
		       <label><input type="hidden" name="__key" value="WATERMARK_FONT_SIZE">Watermark Font Size:</label>
		       <select name="WATERMARK_FONT_SIZE">
				<c:forEach begin="12" end="24" step="4" var="value">
		  	    <option value="${value}" <c:if test="${value == siteConfig['WATERMARK_FONT_SIZE'].value}">selected</c:if>>${value}</option>
				</c:forEach>
		 	   </select>
		     </div>  
		     <div class="form">
		       <label>Watermark Font Color:</label>
		       R<input type="hidden" name="__key" value="WATERMARK_FONT_COLOR_R">
			   <select name="WATERMARK_FONT_COLOR_R">
				<c:forEach begin="0" end="255" var="value">
		  	    <option value="${value}" <c:if test="${value == siteConfig['WATERMARK_FONT_COLOR_R'].value}">selected</c:if>>${value}</option>
				</c:forEach>
		 	   </select>
		       G<input type="hidden" name="__key" value="WATERMARK_FONT_COLOR_G">
			   <select name="WATERMARK_FONT_COLOR_G">
				<c:forEach begin="0" end="255" var="value">
		  	    <option value="${value}" <c:if test="${value == siteConfig['WATERMARK_FONT_COLOR_G'].value}">selected</c:if>>${value}</option>
				</c:forEach>
		 	   </select>
		       B<input type="hidden" name="__key" value="WATERMARK_FONT_COLOR_B">
			   <select name="WATERMARK_FONT_COLOR_B">
				<c:forEach begin="0" end="255" var="value">
		  	    <option value="${value}" <c:if test="${value == siteConfig['WATERMARK_FONT_COLOR_B'].value}">selected</c:if>>${value}</option>
				</c:forEach>
		 	   </select>
		     </div> 
		     </c:if>
		     <br />
		     <div class="title">Payment options </div>     
		     <c:if test="${gSiteConfig['gCREDIT_CARD_PAYMENT'] != ''}"> 
		     <div class="form">
		       <label><input type="hidden" name="__key" value="CREDIT_ACCOUNT">
		       		  <c:choose>
		       		  	<c:when test="${gSiteConfig['gCREDIT_CARD_PAYMENT'] == 'authorizenet'}">Authorize.Net API Login ID:</c:when>
		       		  	<c:when test="${gSiteConfig['gCREDIT_CARD_PAYMENT'] == 'ebizcharge'}">
	       		  			<c:choose>
	       		  				<c:when test="${siteConfig['PCI_FRIENDLY_EBIZCHARGE'].value == 'true'}">EbizCharge Merchant ID:</c:when>
	       		  				<c:otherwise>EbizCharge API Login ID:</c:otherwise>
	       		  			</c:choose>
	       		  		</c:when>
			       		<c:when test="${gSiteConfig['gCREDIT_CARD_PAYMENT'] == 'netcommerce'}">NetCommerce Merchant Number:</c:when>
		       		  	<c:when test="${gSiteConfig['gCREDIT_CARD_PAYMENT'] == 'yourpay'}">YourPay Store Number:</c:when>
		       		  	<c:when test="${gSiteConfig['gCREDIT_CARD_PAYMENT'] == 'ezic'}">Ezic Account Number:</c:when>
		       		  	<c:when test="${gSiteConfig['gCREDIT_CARD_PAYMENT'] == 'payrover'}">USAePay/PayRover Source Key:</c:when>
		       		  	<c:when test="${gSiteConfig['gCREDIT_CARD_PAYMENT'] == 'bankaudi'}">Bank Audi Access Code:</c:when>
		       		  	<c:when test="${gSiteConfig['gCREDIT_CARD_PAYMENT'] == 'paymentech'}">Paymentech/Orbital Username:</c:when>
		       		  	<c:when test="${gSiteConfig['gCREDIT_CARD_PAYMENT'] == 'paypalpro'}"><fmt:message key="apiUserName"/>:</c:when>		 
		       		  	<c:when test="${gSiteConfig['gCREDIT_CARD_PAYMENT'] == 'l19'}">L19 Account Number,Configuration Id:</c:when>      		  	
		       		  </c:choose>
		       </label>
		       <input type="text" name="CREDIT_ACCOUNT" value="<c:out value="${siteConfig['CREDIT_ACCOUNT'].value}"/>" maxlength="50" size="25">
		       <c:if test="${gSiteConfig['gCREDIT_CARD_PAYMENT'] == 'l19'}">
			     <div class="helpNote" style="padding-left: 140px;">
					<p>Account Number,Configuration Id</p>
				 </div>
		       </c:if>
		     </div>  
			 <c:if test="${not (gSiteConfig['gCREDIT_CARD_PAYMENT'] == 'ezic' or gSiteConfig['gCREDIT_CARD_PAYMENT'] == 'payrover')}">
		     <div class="form">
		       <label><input type="hidden" name="__key" value="CREDIT_PASSWORD">
		       		  <c:choose>
		       		  	<c:when test="${gSiteConfig['gCREDIT_CARD_PAYMENT'] == 'authorizenet'}">Authorize.Net Transaction Key:</c:when>
		       		  	<c:when test="${gSiteConfig['gCREDIT_CARD_PAYMENT'] == 'ebizcharge'}">
		       		  		<c:choose>
			       		  		<c:when test="${siteConfig['PCI_FRIENDLY_EBIZCHARGE'].value == 'true'}">EbizCharge Source Key, Pin:</c:when>
			       		  		<c:otherwise>EbizCharge Transaction Key:</c:otherwise>
			       		  	</c:choose>
			       		 </c:when>
		       		  	<c:when test="${gSiteConfig['gCREDIT_CARD_PAYMENT'] == 'netcommerce'}">NetCommerce MD5 Key:</c:when>
		       		  	<c:when test="${gSiteConfig['gCREDIT_CARD_PAYMENT'] == 'yourpay'}">YourPay Order # Prefix:</c:when>
			       		<c:when test="${gSiteConfig['gCREDIT_CARD_PAYMENT'] == 'bankaudi'}">Bank Audi MD5 Key:</c:when>
		       		  	<c:when test="${gSiteConfig['gCREDIT_CARD_PAYMENT'] == 'paymentech'}">Paymentech/Orbital Password:</c:when>
		       		  	<c:when test="${gSiteConfig['gCREDIT_CARD_PAYMENT'] == 'paypalpro'}"><fmt:message key="apiPassword"/>:</c:when>
		       		  	<c:when test="${gSiteConfig['gCREDIT_CARD_PAYMENT'] == 'l19'}">L19 Secret Key:</c:when>
		       		  </c:choose>
		       </label>
		       <input type="text" name="CREDIT_PASSWORD" value="<c:out value="${siteConfig['CREDIT_PASSWORD'].value}"/>" 
		     		<c:choose><c:when test="${gSiteConfig['gCREDIT_CARD_PAYMENT'] != 'yourpay'}">maxlength="50" size="25"</c:when><c:otherwise>maxlength="5" size="10"</c:otherwise></c:choose>>
		     <c:if test="${gSiteConfig['gCREDIT_CARD_PAYMENT'] == 'l19'}">
			     <div class="helpNote" style="padding-left: 140px;">
					<p>Secret Key</p>
				 </div>
		     </c:if>
		     </div> 
		     </c:if> 
			 <c:if test="${gSiteConfig['gCREDIT_CARD_PAYMENT'] == 'bankaudi' or gSiteConfig['gCREDIT_CARD_PAYMENT'] == 'paymentech' or gSiteConfig['gCREDIT_CARD_PAYMENT'] == 'paypalpro'}">
		     <div class="form">
		       <label><input type="hidden" name="__key" value="CREDIT_MERCHANT_ID">
		       		  <c:choose>
		       		  	<c:when test="${gSiteConfig['gCREDIT_CARD_PAYMENT'] == 'bankaudi'}">Bank Audi Merchant ID:</c:when>
		       		  	<c:when test="${gSiteConfig['gCREDIT_CARD_PAYMENT'] == 'paymentech'}">Paymentech/Orbital Merchant ID:</c:when>
		       		  	<c:when test="${gSiteConfig['gCREDIT_CARD_PAYMENT'] == 'paypalpro'}"><fmt:message key="apiSignature"/>:</c:when>
		       		  </c:choose>
		       </label>
		       <input type="text" name="CREDIT_MERCHANT_ID" value="<c:out value="${siteConfig['CREDIT_MERCHANT_ID'].value}"/>" maxlength="100" size="70">
		     </div> 
		     </c:if> 
			 <c:if test="${gSiteConfig['gCREDIT_CARD_PAYMENT'] == 'paymentech'}">
		     <div class="form">
		       <label><input type="hidden" name="__key" value="CREDIT_BIN">Paymentech/Orbital BIN:</label>
		       <input type="text" name="CREDIT_BIN" value="<c:out value="${siteConfig['CREDIT_BIN'].value}"/>" maxlength="50" size="25">
		     </div> 
		     </c:if>
		     
		     </c:if> 
		     <c:if test="${gSiteConfig['gCREDIT_CARD_PAYMENT'] == 'paypalpro'}">
		     <div class="form">
		       <label><input type="hidden" name="__key" value="PAYPAL_CANCEL_RETURN">Paypal Canceleled URL:</label>
		       <input type="text" name="PAYPAL_CANCEL_RETURN" value="<c:out value="${siteConfig['PAYPAL_CANCEL_RETURN'].value}"/>" maxlength="255" size="50">
		     </div>
		     </c:if>
		     <c:if test="${gSiteConfig['gPAYPAL'] > 0 }">  
		     <div class="form">
		       <label>PayPal Business Account:</label>
		       <input type="text" value="<c:out value="${siteConfig['PAYPAL_BUSINESS'].value}"/>" readonly="readonly"/>
		     </div>
		     <div class="form">
		       <label><input type="hidden" name="__key" value="PAYPAL_RETURN">Paypal Completed URL:</label>
		       <input type="text" name="PAYPAL_RETURN" value="<c:out value="${siteConfig['PAYPAL_RETURN'].value}"/>" maxlength="255" size="50">
		     </div>  
		     <div class="form">
		       <label><input type="hidden" name="__key" value="PAYPAL_CANCEL_RETURN">Paypal Canceleled URL:</label>
		       <input type="text" name="PAYPAL_CANCEL_RETURN" value="<c:out value="${siteConfig['PAYPAL_CANCEL_RETURN'].value}"/>" maxlength="255" size="50">
		     </div>  
		     <div class="form">
		       <label><input type="hidden" name="__key" value="PAYPAL_PAYMENT_METHOD_NOTE">PayPal <fmt:message key="paymentMethod" /> <fmt:message key="note" />:</label>
		       <input type="text" name="PAYPAL_PAYMENT_METHOD_NOTE" value="<c:out value="${siteConfig['PAYPAL_PAYMENT_METHOD_NOTE'].value}"/>" maxlength="255" size="50">
		     </div> 
		     </c:if>
		     <c:if test="${siteConfig['EBILLME_URL'].value != ''}">  
		     <div class="form">
		       <label><input type="hidden" name="__key" value="EBILLME_MERCHANTTOKEN">eBillme Merchant Token:</label>
		       <input type="text" name="EBILLME_MERCHANTTOKEN" value="<c:out value="${siteConfig['EBILLME_MERCHANTTOKEN'].value}"/>" maxlength="255" size="25">
		       (<c:choose><c:when test="${siteConfig['EBILLME_URL'].value == 'https://my.eBillme.com/Auth/'}">LIVE</c:when><c:otherwise>TEST</c:otherwise></c:choose>)
		     </div>  
		     <div class="form">
		       <label><input type="hidden" name="__key" value="EBILLME_PAYEETOKEN">eBillme Payee Token:</label>
		       <input type="text" name="EBILLME_PAYEETOKEN" value="<c:out value="${siteConfig['EBILLME_PAYEETOKEN'].value}"/>" maxlength="255" size="25">
		       (<c:choose><c:when test="${siteConfig['EBILLME_URL'].value == 'https://my.eBillme.com/Auth/'}">LIVE</c:when><c:otherwise>TEST</c:otherwise></c:choose>)
		     </div> 
		     <div class="form">
		       <label><input type="hidden" name="__key" value="EBILLME_UID">eBillme UID:</label>
		       <input type="text" name="EBILLME_UID" value="<c:out value="${siteConfig['EBILLME_UID'].value}"/>" maxlength="255" size="25">
		       (<c:choose><c:when test="${siteConfig['EBILLME_URL'].value == 'https://my.eBillme.com/Auth/'}">LIVE</c:when><c:otherwise>TEST</c:otherwise></c:choose>)
		     </div> 		     
		     <div class="form">
		       <label><input type="hidden" name="__key" value="EBILLME_PWD">eBillme PWD:</label>
		       <input type="text" name="EBILLME_PWD" value="<c:out value="${siteConfig['EBILLME_PWD'].value}"/>" maxlength="255" size="25">
		       (<c:choose><c:when test="${siteConfig['EBILLME_URL'].value == 'https://my.eBillme.com/Auth/'}">LIVE</c:when><c:otherwise>TEST</c:otherwise></c:choose>)
		     </div>  
		     <div class="form">
		       <label><input type="hidden" name="__key" value="EBILLME_CANCEL_URL">eBillme Cancel URL:</label>
		       <input type="text" name="EBILLME_CANCEL_URL" value="<c:out value="${siteConfig['EBILLME_CANCEL_URL'].value}"/>" maxlength="255" size="50">
		     </div> 		     
		     <div class="form">
		       <label><input type="hidden" name="__key" value="EBILLME_ERROR_URL">eBillme Error URL:</label>
		       <input type="text" name="EBILLME_ERROR_URL" value="<c:out value="${siteConfig['EBILLME_ERROR_URL'].value}"/>" maxlength="255" size="50">
		     </div> 		     
		     </c:if>
		     <c:if test="${siteConfig['CENTINEL_URL'].value != ''}">  
		     <div class="form">
		       <label><input type="hidden" name="__key" value="CENTINEL_PROCESSORID"><fmt:message key="cardinalCentinel" /> Processor ID:</label>
		       <input type="text" name="CENTINEL_PROCESSORID" value="<c:out value="${siteConfig['CENTINEL_PROCESSORID'].value}"/>" maxlength="255" size="25">
		       (<c:choose><c:when test="${siteConfig['CENTINEL_URL'].value == 'https://centinel.cardinalcommerce.com/maps/txns.asp'}">LIVE</c:when><c:otherwise>TEST</c:otherwise></c:choose>)
		     </div>  
		     <div class="form">
		       <label><input type="hidden" name="__key" value="CENTINEL_MERCHANTID"><fmt:message key="cardinalCentinel" /> Merchant ID:</label>
		       <input type="text" name="CENTINEL_MERCHANTID" value="<c:out value="${siteConfig['CENTINEL_MERCHANTID'].value}"/>" maxlength="255" size="25">
		       (<c:choose><c:when test="${siteConfig['CENTINEL_URL'].value == 'https://centinel.cardinalcommerce.com/maps/txns.asp'}">LIVE</c:when><c:otherwise>TEST</c:otherwise></c:choose>)
		     </div> 
		     <div class="form">
		       <label><input type="hidden" name="__key" value="CENTINEL_TRANSACTIONPWD"><fmt:message key="cardinalCentinel" /> Transaction Password:</label>
		       <input type="text" name="CENTINEL_TRANSACTIONPWD" value="<c:out value="${siteConfig['CENTINEL_TRANSACTIONPWD'].value}"/>" maxlength="255" size="25">
		       (<c:choose><c:when test="${siteConfig['CENTINEL_URL'].value == 'https://centinel.cardinalcommerce.com/maps/txns.asp'}">LIVE</c:when><c:otherwise>TEST</c:otherwise></c:choose>)
		     </div> 		     	     
		     </c:if>
		     <c:if test="${siteConfig['GEMONEY_DOMAIN'].value != ''}">  
		     <div class="form">
		       <label><input type="hidden" name="__key" value="GEMONEY_MERCHANTID">GE Money Merchant ID:</label>
		       <input type="text" name="GEMONEY_MERCHANTID" value="<c:out value="${siteConfig['GEMONEY_MERCHANTID'].value}"/>" maxlength="255" size="25">
		       (<c:choose><c:when test="${siteConfig['GEMONEY_DOMAIN'].value == 'www.secureb2c.com'}">LIVE</c:when><c:otherwise>TEST</c:otherwise></c:choose>)
		     </div> 
		     <div class="form">
		       <label><input type="hidden" name="__key" value="GEMONEY_PASSWORD">GE Money Password:</label>
		       <input type="text" name="GEMONEY_PASSWORD" value="<c:out value="${siteConfig['GEMONEY_PASSWORD'].value}"/>" maxlength="255" size="25">
		       (<c:choose><c:when test="${siteConfig['GEMONEY_DOMAIN'].value == 'www.secureb2c.com'}">LIVE</c:when><c:otherwise>TEST</c:otherwise></c:choose>)
		     </div> 		     	     
		     <div class="form">
		       <label><input type="hidden" name="__key" value="GEMONEY_PROGRAM_NAME">GE Money Program Name:</label>
		       <input type="text" name="GEMONEY_PROGRAM_NAME" value="<c:out value="${siteConfig['GEMONEY_PROGRAM_NAME'].value}"/>" maxlength="255" size="50">
		     </div>
		     <div class="form">
		       <label><input type="hidden" name="__key" value="GEMONEY_PROMOCODE">GE Money Promo Code:</label>
		       <input type="text" name="GEMONEY_PROMOCODE" value="<c:out value="${siteConfig['GEMONEY_PROMOCODE'].value}"/>" maxlength="3" size="3">
		     </div>
		     </c:if>
		     <sec:authorize ifAnyGranted="ROLE_AEM,ROLE_ADMIN_${prop_site_id}">
		     <c:if test="${siteConfig['AMAZON_URL'].value != ''}">    
		     <div class="form">
		       <label><input type="hidden" name="__key" value="AMAZON_MERCHANT_ID">Amazon Merchant ID:</label>
		       <input type="text" name="AMAZON_MERCHANT_ID" value="<c:out value="${siteConfig['AMAZON_MERCHANT_ID'].value}"/>" maxlength="255" size="25">
		       (<c:choose><c:when test="${siteConfig['AMAZON_URL'].value == 'https://payments.amazon.com/'}">Live</c:when><c:otherwise>Sandbox</c:otherwise></c:choose>)
		     </div> 
		     <div class="form">
		       <label><input type="hidden" name="__key" value="AMAZON_ACCESS_KEY">Amazon Checkout Access Key:</label>
		       <input type="text" name="AMAZON_ACCESS_KEY" value="<c:out value="${siteConfig['AMAZON_ACCESS_KEY'].value}"/>" maxlength="255" size="25">
		       (<c:choose><c:when test="${siteConfig['AMAZON_URL'].value == 'https://payments.amazon.com/'}">Live</c:when><c:otherwise>Sandbox</c:otherwise></c:choose>)
		     </div> 
		     <div class="form">
		       <label><input type="hidden" name="__key" value="AMAZON_SECRET_KEY">Amazon Checkout Secret Key:</label>
		       <input type="text" name="AMAZON_SECRET_KEY" value="<c:out value="${siteConfig['AMAZON_SECRET_KEY'].value}"/>" maxlength="255" size="25">
		       (<c:choose><c:when test="${siteConfig['AMAZON_URL'].value == 'https://payments.amazon.com/'}">Live</c:when><c:otherwise>Sandbox</c:otherwise></c:choose>)
		     </div> 		     
		     <div class="form">
		       <label><input type="hidden" name="__key" value="AMAZON_MERCHANT_TOKEN">Amazon Merchant Token:</label>
		       <input type="text" name="AMAZON_MERCHANT_TOKEN" value="<c:out value="${siteConfig['AMAZON_MERCHANT_TOKEN'].value}"/>" maxlength="255" size="25">
		       (<c:choose><c:when test="${siteConfig['AMAZON_URL'].value == 'https://payments.amazon.com/'}">Live</c:when><c:otherwise>Sandbox</c:otherwise></c:choose>)
		     </div> 
		     <div class="form">
		       <label><input type="hidden" name="__key" value="AMAZON_MERCHANT_NAME">Amazon Merchant Name:</label>
		       <input type="text" name="AMAZON_MERCHANT_NAME" value="<c:out value="${siteConfig['AMAZON_MERCHANT_NAME'].value}"/>" maxlength="255" size="25">
		       (<c:choose><c:when test="${siteConfig['AMAZON_URL'].value == 'https://payments.amazon.com/'}">Live</c:when><c:otherwise>Sandbox</c:otherwise></c:choose>)
		     </div> 
		     <div class="form">
		       <label><input type="hidden" name="__key" value="AMAZON_EMAIL">Amazon Email:</label>
		       <input type="text" name="AMAZON_EMAIL" value="<c:out value="${siteConfig['AMAZON_EMAIL'].value}"/>" maxlength="255" size="25">
		       (<c:choose><c:when test="${siteConfig['AMAZON_URL'].value == 'https://payments.amazon.com/'}">Live</c:when><c:otherwise>Sandbox</c:otherwise></c:choose>)
		     </div> 
		     <div class="form">
		       <label><input type="hidden" name="__key" value="AMAZON_PASSWORD">Amazon Password:</label>
		       <input type="text" name="AMAZON_PASSWORD" value="<c:out value="${siteConfig['AMAZON_PASSWORD'].value}"/>" maxlength="255" size="25">
		       (<c:choose><c:when test="${siteConfig['AMAZON_URL'].value == 'https://payments.amazon.com/'}">Live</c:when><c:otherwise>Sandbox</c:otherwise></c:choose>)
		     </div> 		     		     		     
		     </c:if>
		     </sec:authorize>	
		     <c:if test="${gSiteConfig['gCUSTOMER_SUPPLIER']}">
		     <div class="title"><fmt:message key="vbaPaymentOptions"/></div>
		     <div class="form">
		       <label><input type="hidden" name="__key" value="VBA_PAYMENT_OPTIONS"><fmt:message key="vbaPaymentOptions"/></label>
		       <input type="text" name="VBA_PAYMENT_OPTIONS" value="<c:out value="${siteConfig['VBA_PAYMENT_OPTIONS'].value}"/>" maxlength="255" size="50">
		     </div>
		     <div class="form">
		       <label><input type="hidden" name="__key" value="SITE_MESSAGE_ID_FOR_SUPPLIER_PAYMENT"><fmt:message key="siteMessageToSupplierPayment"/></label>
		       <select name="SITE_MESSAGE_ID_FOR_SUPPLIER_PAYMENT">
		        <option value="0"></option>
			    <c:forEach var="siteMessage" items="${siteMessages}">
			    <option value="${siteMessage.messageId}" <c:if test="${siteMessage.messageId == siteConfig['SITE_MESSAGE_ID_FOR_SUPPLIER_PAYMENT'].value}">selected</c:if>><c:out value="${siteMessage.messageName}"/></option>
			    </c:forEach>
		 	   </select>
		     </div>
		     <div class="form">
		       <label><input type="hidden" name="__key" value="SITE_MESSAGE_ID_FOR_AFFILIATE_PAYMENT"><fmt:message key="siteMessageToAffiliatePayment"/></label>
		       <select name="SITE_MESSAGE_ID_FOR_AFFILIATE_PAYMENT">
		        <option value="0"></option>
			    <c:forEach var="siteMessage" items="${siteMessages}">
			    <option value="${siteMessage.messageId}" <c:if test="${siteMessage.messageId == siteConfig['SITE_MESSAGE_ID_FOR_AFFILIATE_PAYMENT'].value}">selected</c:if>><c:out value="${siteMessage.messageName}"/></option>
			    </c:forEach>
		 	   </select>
		     </div>
		     <div class="form">
		       <label><input type="hidden" name="__key" value="SALESREP_INCOMING_PAYMENT_FILTER"><fmt:message key="salesrepIncomingPaymentFilter" />:</label>
		       <input type="checkbox" name="SALESREP_INCOMING_PAYMENT_FILTER" value ="true" <c:if test="${siteConfig['SALESREP_INCOMING_PAYMENT_FILTER'].value == 'true'}">checked</c:if>>
		     </div>
		     </c:if>
		    
		     <c:if test="${gSiteConfig['gBUDGET']}"> 
		     <div class="title"><fmt:message key="budget"/></div>
		     <div class="form">
		       <label><input type="hidden" name="__key" value="SITE_MESSAGE_ID_FOR_BUDGET_ORDERS"><fmt:message key="siteMessageNewBudgetOrders"/>:</label>
			  <select name="SITE_MESSAGE_ID_FOR_BUDGET_ORDERS">
		        <option value="0"></option>
			    <c:forEach var="siteMessage" items="${siteMessages}">
			    <option value="${siteMessage.messageId}" <c:if test="${siteMessage.messageId == siteConfig['SITE_MESSAGE_ID_FOR_BUDGET_ORDERS'].value}">selected</c:if>><c:out value="${siteMessage.messageName}"/></option>
			    </c:forEach>
		 	  </select>
		     </div>
		      <%-- This is the maximum credit that need to be applied to the order when 'BUDGET' is enabled.--%>  
		     <div class="form">
		     	<label><input type="hidden" name="__key" value="BUDGET_PERCENTAGE"><fmt:message key="budgetPercentage"/><img class="toolTipImg" title="Note::'0' : Need quote approvals form Managers <br/> '1-100' apply this % of customer's credit on order" src="../graphics/question.gif" />:</label>
		        <input type="text" name="BUDGET_PERCENTAGE" value="<c:out value="${siteConfig['BUDGET_PERCENTAGE'].value}"/>" maxlength="255" size="50"> %
		     </div>	  
		     <div class="form">
		     	<label><input type="hidden" name="__key" value="CREDIT_APPROVAL_MESSAGE">Credit Approval Message</label>
		        <input type="text" name="CREDIT_APPROVAL_MESSAGE" value="<c:out value="${siteConfig['CREDIT_APPROVAL_MESSAGE'].value}"/>" maxlength="255" size="50">
		     </div>
		     </c:if> 
		     <c:if test="${siteConfig['GOOGLE_CHECKOUT_URL'].value != ''}"> 
		     <div class="title"><fmt:message key="googleCheckOut" /></div>
		     <div class="form">
		       <label><input type="hidden" name="__key" value="GOOGLE_MERCHANT_ID">Google Merchant Id:</label>
		       <input type="text" name="GOOGLE_MERCHANT_ID" value="<c:out value="${siteConfig['GOOGLE_MERCHANT_ID'].value}"/>" maxlength="255" size="50">
		     </div>
		     <div class="form">
		       <label><input type="hidden" name="__key" value="GOOGLE_MERCHANT_KEY">Google Merchant Key:</label>
		       <input type="text" name="GOOGLE_MERCHANT_KEY" value="<c:out value="${siteConfig['GOOGLE_MERCHANT_KEY'].value}"/>" maxlength="255" size="50">
		     </div>
		     </c:if>
		     <c:if test="${siteConfig['BUY_SAFE_URL'].value != ''}"> 
		     <div class="title"><fmt:message key="buySafe" /></div>
		     <div class="form">
		       <label><input type="hidden" name="__key" value="BUY_SAFE_VERSION">Buy Safe Version:</label>
		       <input type="text" name="BUY_SAFE_VERSION" value="<c:out value="${siteConfig['BUY_SAFE_VERSION'].value}"/>" maxlength="255" size="50">
		     </div>
		     <div class="form">
		       <label><input type="hidden" name="__key" value="BUY_SAFE_USERNAME">Buy Safe Username:</label>
		       <input type="text" name="BUY_SAFE_USERNAME" value="<c:out value="${siteConfig['BUY_SAFE_USERNAME'].value}"/>" maxlength="255" size="50">
		     </div>
		     <div class="form">
		       <label><input type="hidden" name="__key" value="BUY_SAFE_PASSWORD">Buy Safe Password:</label>
		       <input type="text" name="BUY_SAFE_PASSWORD" value="<c:out value="${siteConfig['BUY_SAFE_PASSWORD'].value}"/>" maxlength="255" size="50">
		     </div>
		     <div class="form">
		       <label><input type="hidden" name="__key" value="BUY_SAFE_STORE_TOKEN">Buy Safe Store Token:</label>
		       <input type="text" name="BUY_SAFE_STORE_TOKEN" value="<c:out value="${siteConfig['BUY_SAFE_STORE_TOKEN'].value}"/>" maxlength="255" size="50">
		     </div>
		     </c:if>
		     <c:if test="${gSiteConfig['gACCOUNTING'] == 'MAS90'}">
		     <br /> 
		     <div class="title">Mas90 Configuration</div>     
			 <c:forTokens items="${siteConfig['MAS90'].value}" delims="," var="config" varStatus="status">
		     <div class="form">
		       <label>
		         <c:choose>
		           <c:when test="${status.index == 0}">FTP <fmt:message key="server" />:</c:when>
		           <c:when test="${status.index == 1}">FTP <fmt:message key="username" />:</c:when>
		           <c:when test="${status.index == 2}">FTP <fmt:message key="password" />:</c:when>
		           <c:when test="${status.index == 3}">Inventory <fmt:message key="fileName" />:</c:when>
		           <c:when test="${status.index == 4}">Invoices <fmt:message key="fileName" />:</c:when>
		         </c:choose>
		       </label>
		       <c:out value="${config}"/>
		     </div>  
			 </c:forTokens>
		     </c:if>     

		     <c:if test="${siteConfig['DSI'].value != ''}">
		     <br /> 
		     <div class="title">DSI Configuration</div>     
			 <c:forTokens items="${siteConfig['DSI'].value}" delims="," var="config" varStatus="status">
		     <div class="form">
		       <label>
		         <c:choose>
		           <c:when test="${status.index == 0}">FTP <fmt:message key="server" />:</c:when>
		           <c:when test="${status.index == 1}">FTP <fmt:message key="username" />:</c:when>
		           <c:when test="${status.index == 2}">FTP <fmt:message key="password" />:</c:when>
		           <c:when test="${status.index == 3}">DSI Dealer #:</c:when>
		           <c:when test="${status.index == 4}">Supplier ID:</c:when>
		         </c:choose>
		       </label>
		       <c:out value="${config}"/>
		     </div>  
			 </c:forTokens>
		     </c:if>     

		     <c:if test="${gSiteConfig['gACCOUNTING'] == 'INGRAM'}">
		     <br /> 
		     <div class="title">INGRAM Configuration</div>     
			 <c:forTokens items="${siteConfig['INGRAM'].value}" delims="," var="config" varStatus="status">
		     <div class="form">
		       <label>
		         <c:choose>
		           <c:when test="${status.index == 0}">FTP <fmt:message key="server" />:</c:when>
		           <c:when test="${status.index == 1}">FTP <fmt:message key="username" />:</c:when>
		           <c:when test="${status.index == 2}">FTP <fmt:message key="password" />:</c:when>
		           <c:when test="${status.index == 3}">FTP <fmt:message key="fileName" />:</c:when>
		         </c:choose>
		       </label>
		       <c:out value="${config}"/>
		     </div>  
			 </c:forTokens>
		     </c:if>    

		     <c:if test="${gSiteConfig['gACCOUNTING'] == 'CONCORD'}">
		     <br /> 
		     <div class="title">CONCORD Configuration</div>     
		     <div class="form">
		       <label><input type="hidden" name="__key" value="CONCORD_PRICECHANGE">Price Change:</label>
		       <input type="text" name="CONCORD_PRICECHANGE" value="<c:out value="${siteConfig['CONCORD_PRICECHANGE'].value}"/>" maxlength="5" size="5">%
		     </div> 
		     </c:if> 
		     
		     <c:if test="${siteConfig['FRAGRANCENET'].value != ''}">
		     <br /> 
		     <div class="title">FRAGRANCE Configuration</div>     
		     <div class="form">
		       <label><input type="hidden" name="__key" value="FRAGRANCENET_PRICECHANGE">Price Change:</label>
		       <input type="text" name="FRAGRANCENET_PRICECHANGE" value="<c:out value="${siteConfig['FRAGRANCENET_PRICECHANGE'].value}"/>" maxlength="5" size="5">%
		     </div> 
		     <c:if test="${gSiteConfig['gPRICE_TABLE'] > 0}">
		     <c:forEach begin="1" end="3" step="1" var="value">
		     <c:set value="FRAGRANCENET_PRICETABLE${value}_CHANGE" var="configKey"/>
		     <div class="form">
		       <label><input type="hidden" name="__key" value="${configKey}">Price Table ${value} Change:</label>
		       <input type="text" name="${configKey}" value="<c:out value="${siteConfig[configKey].value}"/>" maxlength="5" size="5">%
		     </div> 
		     </c:forEach>
		     </c:if> 
		     </c:if>
		     
		     <c:if test="${siteConfig['INGRAM'].value != ''}">
		     <br /> 
		     <div class="title">INGRAM Configuration</div>
		     <div class="form">
		       <label><input type="hidden" name="__key" value="INGRAM_CUSTOM_FORMULA_MARGIN">Reg,Min Margin:</label>
		       <input type="text" name="INGRAM_CUSTOM_FORMULA_MARGIN" value="<c:out value="${siteConfig['INGRAM_CUSTOM_FORMULA_MARGIN'].value}"/>" maxlength="20" size="15">
		     </div> 
		     <c:if test="${gSiteConfig['gPRICE_TABLE'] > 0}">
		     <c:forEach begin="1" end="5" step="1" var="value">
		     <c:set value="INGRAM_PRICETABLE${value}_CHANGE" var="configKey"/>
		     <div class="form">
		       <label><input type="hidden" name="__key" value="${configKey}">Price Table ${value} Change:</label>
		       <input type="text" name="${configKey}" value="<c:out value="${siteConfig[configKey].value}"/>" maxlength="5" size="5">%
		     </div> 
		     </c:forEach>
		     </c:if>
		     </c:if>

		     <c:if test="${gSiteConfig['gACCOUNTING'] == 'TRIPLEFIN'}">
		     <br /> 
		     <div class="title">TRIPLEFIN Configuration</div>     
			 <c:forTokens items="${siteConfig['TRIPLEFIN'].value}" delims="," var="config" varStatus="status">
		     <div class="form">
		       <label>
		         <c:choose>
		           <c:when test="${status.index == 0}">FTP <fmt:message key="server" />:</c:when>
		           <c:when test="${status.index == 1}">FTP <fmt:message key="username" />:</c:when>
		           <c:when test="${status.index == 2}">FTP <fmt:message key="password" />:</c:when>
		         </c:choose>
		       </label>
		       <c:out value="${config}"/>
		     </div>  
			 </c:forTokens>
		     </c:if>
		     
		     <c:if test="${gSiteConfig['gSUGAR_SYNC']}">
		     <br /> 
		     <div class="title">Sugar Sync Configuration</div>
		     <div class="form">
		     <label><input type="hidden" name="__key" value="SUGAR_SYNC_CREDENTIALS">Sugar Sync Credentilas:</label>
		     <input type="text" name="SUGAR_SYNC_CREDENTIALS" value="<c:out value="${siteConfig['SUGAR_SYNC_CREDENTIALS'].value}"/>" maxlength="255" size="50">
		     <div class="helpNote" style="padding-left: 140px;">
				<p>Username,Password</p>
			 </div>
		     </div>
		     <div class="form">
		     <label><input type="hidden" name="__key" value="SUGAR_SYNC_AUTHENTICATION">Sugar Sync Authentication:</label>
		     <input type="text" name="SUGAR_SYNC_AUTHENTICATION" value="<c:out value="${siteConfig['SUGAR_SYNC_AUTHENTICATION'].value}"/>" maxlength="255" size="50">
		     <div class="helpNote" style="padding-left: 140px;">
				<p>applicationId,accessKey,privateKey</p>
			 </div>
		     </div>
		     </c:if> 

		     <c:if test="${gSiteConfig['gAFFILIATE'] > 0}">
		     <br />
		     <div class="title">Affiliate Options</div> 
		     </c:if> 
		     <div class="form" id="childInvoice">
		       <c:if test="${gSiteConfig['gAFFILIATE'] > 0}">
		       <label><input type="hidden" name="__key" value="SHOW_CHILD_INVOICE">Show Child Invoice:</label>
		       <fmt:message key="yes" /> <input type="radio" name="SHOW_CHILD_INVOICE" value="true" <c:if test="${siteConfig['SHOW_CHILD_INVOICE'].value == 'true'}">checked</c:if>>
		       <fmt:message key="no" /> <input type="radio" name="SHOW_CHILD_INVOICE" value="false" <c:if test="${siteConfig['SHOW_CHILD_INVOICE'].value == 'false'}">checked</c:if>>
		       </c:if>
		     </div> 
		     <br />
		     <div class="title">Miscellaneous </div>     
		     <c:if test="${gSiteConfig['gSEARCH'] == 'triguide'}">
		     <div class="form">
		       <label><input type="hidden" name="__key" value="SEARCH_ACCOUNT">Triguide Account:</label>
		       <input type="text" name="SEARCH_ACCOUNT" value="<c:out value="${siteConfig['SEARCH_ACCOUNT'].value}"/>" maxlength="10" size="12">
		       <input type="hidden" name="__key" value="TRIGUIDE_URL">
		       <select name="TRIGUIDE_URL">
		  	    <option value="pxml.tgoservices.com">pxml.tgoservices.com</option>
		  	    <option value="md01.tgoservices.com" <c:if test="${'md01.tgoservices.com' == siteConfig['TRIGUIDE_URL'].value}">selected</c:if>>md01.tgoservices.com</option>
		 	   </select>       
		     </div>
		     <div class="form">
		       <label><input type="hidden" name="__key" value="SEARCH_NOTHING_FOUND_MESSAGE">Triguide Nothing Found Message:</label>
		       <input type="text" name="SEARCH_NOTHING_FOUND_MESSAGE" value="<c:out value="${siteConfig['SEARCH_NOTHING_FOUND_MESSAGE'].value}"/>" maxlength="255" size="50">
		     </div>
		     </c:if>   
		     <c:if test="${gSiteConfig['gTAX_EXEMPTION']}">  
		     <div class="form">
		       <label><input type="hidden" name="__key" value="TAX_ID_NOTE"><fmt:message key="taxIdNote" />:</label>
		       <input type="text" name="TAX_ID_NOTE" value="<c:out value="${siteConfig['TAX_ID_NOTE'].value}"/>" maxlength="255" size="50">
		     </div>
		     <div class="form">
		       <label><input type="hidden" name="__key" value="TAX_ID_REQUIRED"><fmt:message key="taxIdRequired" />:</label>
		       <input type="checkbox" name="TAX_ID_REQUIRED" value ="true" <c:if test="${siteConfig['TAX_ID_REQUIRED'].value == 'true'}">checked</c:if>>
		     </div>
		     <div class="form">
		       <label><input type="hidden" name="__key" value="TAX_ID_ON_PRINT_INVOICE"><fmt:message key="taxIdOnPrintInvoice" />:</label>
		       <input type="checkbox" name="TAX_ID_ON_PRINT_INVOICE" value ="true" <c:if test="${siteConfig['TAX_ID_ON_PRINT_INVOICE'].value == 'true'}">checked</c:if>>
		     </div>
		     </c:if>
		     <div class="form">
		       <label><input type="hidden" name="__key" value="SPECIAL_INSTRUCTIONS">Special Instructions:</label>
		       <input type="text" name="SPECIAL_INSTRUCTIONS" value="<c:out value="${siteConfig['SPECIAL_INSTRUCTIONS'].value}"/>" maxlength="255" size="50">
		     </div> 
		     <c:if test="${gSiteConfig['gMYLIST']}">
		     <div class="form">
		       <label><input type="hidden" name="__key" value="MY_LIST_TITLE"><fmt:message key="myList" />:</label>
		       <input type="text" name="MY_LIST_TITLE" value="<c:out value="${siteConfig['MY_LIST_TITLE'].value}"/>" maxlength="255" size="50">
		     </div> 
		     </c:if>
		     <div class="form">
		       <label><input type="hidden" name="__key" value="MY_ORDER_LIST_TITLE"><fmt:message key="myOrderList" />:</label>
		       <input type="text" name="MY_ORDER_LIST_TITLE" value="<c:out value="${siteConfig['MY_ORDER_LIST_TITLE'].value}"/>" maxlength="255" size="50">
		     </div>
		     <div class="form">
		       <label><input type="hidden" name="__key" value="VIEW_MY_ORDER_LIST_TITLE">View <fmt:message key="myOrderList" />:</label>
		       <input type="text" name="VIEW_MY_ORDER_LIST_TITLE" value="<c:out value="${siteConfig['VIEW_MY_ORDER_LIST_TITLE'].value}"/>" maxlength="255" size="50">
		     </div>
		     <div class="form">
		       <label><input type="hidden" name="__key" value="MY_ORDER_HISTORY_STATUS_TITLE"><fmt:message key="myOrderHistoryStatus" />:</label>
		       <input type="text" name="MY_ORDER_HISTORY_STATUS_TITLE" value="<c:out value="${siteConfig['MY_ORDER_HISTORY_STATUS_TITLE'].value}"/>" maxlength="255" size="50">
		     </div>
		     <div class="form">
		       <label><input type="hidden" name="__key" value="VIEW_MY_ORDER_HISTORY_STATUS_TITLE">View <fmt:message key="myOrderHistoryStatus" />:</label>
		       <input type="text" name="VIEW_MY_ORDER_HISTORY_STATUS_TITLE" value="<c:out value="${siteConfig['VIEW_MY_ORDER_HISTORY_STATUS_TITLE'].value}"/>" maxlength="255" size="50">
		     </div>
		     <br />       
		     <div class="title"><fmt:message key="layout" /> <fmt:message key="options" /></div>
		     <div class="form">
		       <label><input type="hidden" name="__key" value="LEFTBAR_LAYOUT"><fmt:message key="LeftBar" />  <fmt:message key="layout" />:</label>
		      <select name="LEFTBAR_LAYOUT">
		  	    <option value="1"><fmt:message key="default" /></option>
		  	    <c:forEach begin="2" end="6" var="LEFTBAR_LAYOUT">
		  	      <option value="${LEFTBAR_LAYOUT}" <c:if test="${siteConfig['LEFTBAR_LAYOUT'].value == LEFTBAR_LAYOUT}">selected</c:if>>Show ${LEFTBAR_LAYOUT-1} Level(s) Sub-Categories</option>
		  	    </c:forEach>
		  	  </select>
		     </div> 
		     <div class="form">
		       <label><input type="hidden" name="__key" value="LEFTBAR_HIDE_ON_SYSTEM_PAGES">Hide Leftbar on System Pages:</label>
		       <fmt:message key="yes" /> <input type="radio" name="LEFTBAR_HIDE_ON_SYSTEM_PAGES" value="true" <c:if test="${siteConfig['LEFTBAR_HIDE_ON_SYSTEM_PAGES'].value == 'true'}">checked</c:if>>
		       <fmt:message key="no" /> <input type="radio" name="LEFTBAR_HIDE_ON_SYSTEM_PAGES" value="false" <c:if test="${siteConfig['LEFTBAR_HIDE_ON_SYSTEM_PAGES'].value == 'false'}">checked</c:if>>
		     </div>
		     <div class="form">
		       <label><input type="hidden" name="__key" value="GOOGLE_CURRENCY_CONVERTER">Google Currency Converter</label>
		       <input type="checkbox" name="GOOGLE_CURRENCY_CONVERTER" value ="true" <c:if test="${siteConfig['GOOGLE_CURRENCY_CONVERTER'].value == 'true'}">checked</c:if>>
		     </div>
		     <div class="form">    
		       <label><input type="hidden" name="__key" value="HORIZONTAL_DYNAMIC_MENU"><fmt:message key="horizontal" /> <fmt:message key="dynamicMenu" /> <fmt:message key="categoryIds" />:</label>
		       <input type="text" name="HORIZONTAL_DYNAMIC_MENU" value="<c:out value="${siteConfig['HORIZONTAL_DYNAMIC_MENU'].value}"/>" maxlength="255" size="50">
		     </div>	
		     <br />
		     <div class="title"><fmt:message key="customer" /> <fmt:message key="options" /></div>
		     <div class="form">
		       <label><input type="hidden" name="__key" value="CUSTOMERS_REQUIRED_COMPANY"><fmt:message key="company" /> Required:</label>
		       <fmt:message key="yes" /> <input type="radio" name="CUSTOMERS_REQUIRED_COMPANY" value="true" <c:if test="${siteConfig['CUSTOMERS_REQUIRED_COMPANY'].value == 'true'}">checked</c:if>>
		       <fmt:message key="no" /> <input type="radio" name="CUSTOMERS_REQUIRED_COMPANY" value="false" <c:if test="${siteConfig['CUSTOMERS_REQUIRED_COMPANY'].value == 'false'}">checked</c:if>>
		     </div>   
		     <div class="form">
		       <label><input type="hidden" name="__key" value="ADDRESS_RESIDENTIAL_COMMERCIAL"><fmt:message key="customersAddressType" />:</label>
			   <select name="ADDRESS_RESIDENTIAL_COMMERCIAL">
		        <c:forEach begin="0" end="3" step="1" var="value">
		          <option value="${value}" <c:if test="${value == siteConfig['ADDRESS_RESIDENTIAL_COMMERCIAL'].value}">selected</c:if> ><fmt:message key="addressType_${value}" /></option>
			    </c:forEach>
			   </select>
		     </div>
		     <div class="form">
		       <label><input type="hidden" name="__key" value="EDIT_EMAIL_ON_FRONTEND"><fmt:message key="editEmailOnFrontEnd" />:</label>
		       <input type="checkbox" name="EDIT_EMAIL_ON_FRONTEND" value ="true" <c:if test="${siteConfig['EDIT_EMAIL_ON_FRONTEND'].value == 'true'}">checked</c:if>>
		     </div>
		     <div class="form">
		       <label><input type="hidden" name="__key" value="EDIT_ADDRESS_ON_FRONTEND"><fmt:message key="editAddressOnFrontEnd" />:</label>
		       <input type="checkbox" name="EDIT_ADDRESS_ON_FRONTEND" value ="true" <c:if test="${siteConfig['EDIT_ADDRESS_ON_FRONTEND'].value == 'true'}">checked</c:if>>
		     </div>
		     <div class="form">
		       <label><input type="hidden" name="__key" value="ADD_ADDRESS_ON_CHECKOUT"><fmt:message key="addAddressOnCheckout" />:</label>
		       <input type="checkbox" name="ADD_ADDRESS_ON_CHECKOUT" value ="true" <c:if test="${siteConfig['ADD_ADDRESS_ON_CHECKOUT'].value == 'true'}">checked</c:if>>
		     </div>
		     <c:if test="${gSiteConfig['gPRICE_TABLE'] > 0 and gSiteConfig['gMINIMUM_INCREMENTAL_QTY']}">
		     <div class="form">
		       <label><input type="hidden" name="__key" value="APPLY_MIN_INC_TO_CUSTOMER"><fmt:message key="applyMinIncTo" />:</label>
		       <select name="APPLY_MIN_INC_TO_CUSTOMER">
		  	    <option value=""><fmt:message key='allCustomers' /></option>
		  	    <option value="0" <c:if test="${siteConfig['APPLY_MIN_INC_TO_CUSTOMER'].value == '0'}">selected</c:if>><fmt:message key='regularCustomers' /></option>
		  	    <option value="1" <c:if test="${siteConfig['APPLY_MIN_INC_TO_CUSTOMER'].value == '1'}">selected</c:if>><fmt:message key='wholesalers' /></option>
		 	  </select>
		     </div>    
		     </c:if> 
		     <div class="form">    
		       <label><input type="hidden" name="__key" value="CUSTOMER_RATING_1"><fmt:message key="customer" /> <fmt:message key="rating" />1 :</label>
		       <input type="text" name="CUSTOMER_RATING_1" value="<c:out value="${siteConfig['CUSTOMER_RATING_1'].value}"/>" maxlength="255" size="50">
		     </div>	
		     <c:if test="${gSiteConfig['gSITE_DOMAIN'] == 'viatrading.com' or gSiteConfig['gSITE_DOMAIN'] == 'wj20test.com'}">
		     <div class="form">    
		       <label><input type="hidden" name="__key" value="CUSTOMER_RATING_2"><fmt:message key="customer" /> <fmt:message key="rating" />2 :</label>
		       <input type="text" name="CUSTOMER_RATING_2" value="<c:out value="${siteConfig['CUSTOMER_RATING_2'].value}"/>" maxlength="255" size="50">
		     </div>
		     </c:if>
		     <br /> 
		     <c:if test="${gSiteConfig['gPAYMENTS']}">
		     <div class="form"> 
		     	<label><input type="hidden" name="__key" value="PAYMENT_ALERT_START_DATE"><fmt:message key="payment" /> <fmt:message key="alert" /> <fmt:message key="startDate" /> :</label>
		     	<input type="text" name="PAYMENT_ALERT_START_DATE" value="<c:out value="${siteConfig['PAYMENT_ALERT_START_DATE'].value}"/>" maxlength="255" size="50">
		     	<div class="helpNote" style="padding-left: 140px;">
					<p>Date should be in MM/dd/yyyy format. Empty will go back to 1 Year.</p>
				</div>
		     </div>
		     </c:if>
		     <c:if test="${gSiteConfig['gCUSTOMER_SUPPLIER']}">
		     <div class="title"><fmt:message key="consignment" /> <fmt:message key="options" /></div>
		     <div class="form">
		       <label><input type="hidden" name="__key" value="ADD_PRODUCT_ON_FRONTEND"><fmt:message key="addProductOnFrontend" />:</label>
		       <input type="checkbox" name="ADD_PRODUCT_ON_FRONTEND" value ="true" <c:if test="${siteConfig['ADD_PRODUCT_ON_FRONTEND'].value == 'true'}">checked</c:if>>
		     </div>
		     </c:if>
		     
		     <c:if test="${gSiteConfig['gSALES_REP']}">
		     <div class="title"><fmt:message key="salesRep" /> <fmt:message key="options" /></div>
		     <div class="form">
		       <label><input type="hidden" name="__key" value="TERRITORY_ZIPCODE"><fmt:message key="territoryZipcode" />:</label>
		       <input type="checkbox" name="TERRITORY_ZIPCODE" value ="true" <c:if test="${siteConfig['TERRITORY_ZIPCODE'].value == 'true'}">checked</c:if>>
		     </div> 
		     <div class="form">
		       <label><input type="hidden" name="__key" value="SALES_REP_SEQUENTIAL_ASSIGNEMENT"><fmt:message key="salesRepSequentialAssignement" />:</label>
		       <input type="checkbox" name="SALES_REP_SEQUENTIAL_ASSIGNEMENT" value ="true" <c:if test="${siteConfig['SALES_REP_SEQUENTIAL_ASSIGNEMENT'].value == 'true'}">checked</c:if>>
		     </div> 
		     <c:if test="${gSiteConfig['gPRESENTATION']}">
		     <div class="form">
		       <label><input type="hidden" name="__key" value="PRESENTATION_NOT_FOUND_LINK"><fmt:message key="template" /> NOT FOUND LINK:</label>
		       <input type="text" name="PRESENTATION_NOT_FOUND_LINK" value="<c:out value="${siteConfig['PRESENTATION_NOT_FOUND_LINK'].value}"/>" maxlength="255" size="50">
		     </div>  
		     </c:if>
		     <br />
		     </c:if>
		     
		     <c:if test="${gSiteConfig['gMANUFACTURER']}">
		     <div class="title"><fmt:message key="manufacturer" /> <fmt:message key="options" /></div>
		     <div class="form">
		       <label><input type="hidden" name="__key" value="BRAND_MIN_ORDER_MESSAGE"><fmt:message key="brandMinimumOrderMessage" />:</label>
		       <input type="text" name="BRAND_MIN_ORDER_MESSAGE" value="<c:out value="${siteConfig['BRAND_MIN_ORDER_MESSAGE'].value}"/>" maxlength="255" size="50">
		     </div>   
		     <br />
		     </c:if>
		     
		     <c:if test="${siteConfig['PROTECTED_HOST'].value != ''}">
		     <div class="title">Protected Host/Domain</div>     
  			 <div class="form">
		       <label>&nbsp;</label>
		       <c:out value="${siteConfig['PROTECTED_HOST'].value}"/>
		       <c:if test="${gSiteConfig['gPROTECTED'] > 0}">
		       --> <fmt:message key="protectedLevel" />
		       <input type="hidden" name="__key" value="PROTECTED_HOST_PROTECTED_LEVEL">
		       <select name="PROTECTED_HOST_PROTECTED_LEVEL">
		         <option value="0"><fmt:message key="none" /></option>
				 <c:forEach items="${protectedLevels}" var="protectedLevel" varStatus="protectedStatus">
				 <c:set var="key" value="protected${protectedStatus.count}"/>
		    	 <c:choose>
		      	   <c:when test="${labels[key] != null and labels[key] != ''}">
		        	 <c:set var="label" value="${labels[key]}"/>
		      	   </c:when>
		      	   <c:otherwise><c:set var="label" value="${protectedStatus.count}"/></c:otherwise>
		    	 </c:choose> 
		          <option value="${protectedLevel}" <c:if test="${protectedLevel == siteConfig['PROTECTED_HOST_PROTECTED_LEVEL'].value}">selected</c:if>><c:out value="${label}"/></option>
                 </c:forEach> 
		 	   </select>		       
		       </c:if>
		     </div>
		     <br/>
		     </c:if> 		     
		     
		     <c:if test="${gSiteConfig['gSERVICE']}">
		     <div class="title"><fmt:message key="service" /> <fmt:message key="options" /></div>
		     <div class="form">
		       <label><input type="hidden" name="__key" value="SERVICEABLE_ITEM_TITLE"><fmt:message key="serviceableItem" /> <fmt:message key="title" />:</label>
		       <input type="text" name="SERVICEABLE_ITEM_TITLE" value="<c:out value="${siteConfig['SERVICEABLE_ITEM_TITLE'].value}"/>" maxlength="255" size="25">
		     </div>
		     <br/>
		     </c:if>
		     <div class="title"><fmt:message key="invoice" /> <fmt:message key="options" /></div>
		     <div class="form">
		       <label><input type="hidden" name="__key" value="EXT_QUANTITY_TITLE"><fmt:message key="extQuantity" /> <fmt:message key="title" />:</label>
		       <input type="text" name="EXT_QUANTITY_TITLE" value="<c:out value="${siteConfig['EXT_QUANTITY_TITLE'].value}"/>" maxlength="255" size="25">
		     </div>
		     <div class="form">
		       <label><input type="hidden" name="__key" value="INVOICE_LINEITEMS_SORTING"><fmt:message key="invoice" /> <fmt:message key="lineItems" /> <fmt:message key="sortBy" />:</label>
		       <select name="INVOICE_LINEITEMS_SORTING">
		  	    <option value=""><fmt:message key="default" /></option>
		  	    <option value="product_sku" <c:if test="${siteConfig['INVOICE_LINEITEMS_SORTING'].value == 'product_sku'}">selected</c:if>><fmt:message key="sku" /></option>
		 	  </select>
		     </div>      
		     <div class="form" id="minimumOrderID">
		       <c:if test="${siteConfig['MINIMUM_ORDER'].value == 'true'}">
		       <label><input type="hidden" name="__key" value="MINIMUM_ORDER_AMOUNT"><fmt:message key="minimumOrder" /> <fmt:message key="amount" /> :</label>
		       <input type="text" name="MINIMUM_ORDER_AMOUNT" value="<c:out value="${siteConfig['MINIMUM_ORDER_AMOUNT'].value}"/>" size="30">
		       </c:if> 
		     </div>
		     <div class="form" id="minimumOrderWholeSaleID">
		       <c:if test="${gSiteConfig['gPRICE_TABLE'] > 0 and siteConfig['MINIMUM_ORDER'].value == 'true'}">
		       <label><input type="hidden" name="__key" value="MINIMUM_ORDER_AMOUNT_WHOLESALE"><fmt:message key="minimumOrderForWholeSaler" /> :</label>
		       <input type="text" name="MINIMUM_ORDER_AMOUNT_WHOLESALE" value="<c:out value="${siteConfig['MINIMUM_ORDER_AMOUNT_WHOLESALE'].value}"/>" size="30">
		       </c:if> 
		     </div>
		     <c:if test="${gSiteConfig['gPRICE_TABLE'] > 0 and gSiteConfig['gMINIMUM_INCREMENTAL_QTY']}">
		     <div class="form">
		       <label><input type="hidden" name="__key" value="MINIMUM_MESSAGE">Minimum Message:</label>
		       <input type="text" name="MINIMUM_MESSAGE" value="<c:out value="${siteConfig['MINIMUM_MESSAGE'].value}"/>" maxlength="255" size="50">
		     </div>
		     <div class="form">
		       <label><input type="hidden" name="__key" value="INCREMENTAL_MESSAGE">Incremental Message:</label>
		       <input type="text" name="INCREMENTAL_MESSAGE" value="<c:out value="${siteConfig['INCREMENTAL_MESSAGE'].value}"/>" maxlength="255" size="50">
		     </div>
		     </c:if>
		     <div class="form">
		       <label><input type="hidden" name="__key" value="INVOICE_MESSAGE">Invoice Message:</label>
		       <input type="text" name="INVOICE_MESSAGE" value="<c:out value="${siteConfig['INVOICE_MESSAGE'].value}"/>" maxlength="255" size="50">
		     </div>
		     <div class="form">
		       <label><input type="hidden" name="__key" value="SHOW_GRANDTOTAL_BILLING_METHOD">Show GrandTotal in Billing Method:</label>
		       <input type="checkbox" name="SHOW_GRANDTOTAL_BILLING_METHOD" value ="true" <c:if test="${siteConfig['SHOW_GRANDTOTAL_BILLING_METHOD'].value == 'true'}">checked</c:if>>
		     </div>
		     <div class="form">
		       <label><input type="hidden" name="__key" value="SHOW_IMAGE_ON_INVOICE"><fmt:message key="showImageOnInvoice" />:</label>
		       <input type="checkbox" name="SHOW_IMAGE_ON_INVOICE" value ="true" <c:if test="${siteConfig['SHOW_IMAGE_ON_INVOICE'].value == 'true'}">checked</c:if>>
		     </div>
		     <div class="form">
		       <label><input type="hidden" name="__key" value="SHOW_CC_ON_PRINT_FRIENDLY"><fmt:message key="showCreditCardInfoOnPrintInvoice" />:</label>
		       <input type="checkbox" name="SHOW_CC_ON_PRINT_FRIENDLY" value ="true" <c:if test="${siteConfig['SHOW_CC_ON_PRINT_FRIENDLY'].value == 'true'}">checked</c:if>>
		     </div>
		     <div class="form">
		       <label><input type="hidden" name="__key" value="SHOW_PAYMENT_HISTORY_ON_PRINT_FRIENDLY"><fmt:message key="showPaymentHistoryOnPrintInvoice" />:</label>
		       <input type="checkbox" name="SHOW_PAYMENT_HISTORY_ON_PRINT_FRIENDLY" value ="true" <c:if test="${siteConfig['SHOW_PAYMENT_HISTORY_ON_PRINT_FRIENDLY'].value == 'true'}">checked</c:if>>
		     </div>
		     <div class="form">
		       <label><input type="hidden" name="__key" value="DEFAULT_CREDITCARD_FEE_RATE"><fmt:message key="creditCardFee" />:</label>
		       <select name="DEFAULT_CREDITCARD_FEE_RATE">
		  	     <c:forTokens items="0,0.5,1.0,1.5,2.0,2.5,3.0" delims="," var="value">
		  	      <option value="${value}" <c:if test="${value == siteConfig['DEFAULT_CREDITCARD_FEE_RATE'].value}">selected</c:if>>${value}%</option>
				 </c:forTokens>
		 	  </select>
		     </div>
		     <div class="form">
		       <label><input type="hidden" name="__key" value="DEFAULT_ORDER_TYPE"><fmt:message key="defaultOrderType" />:</label>
		       <select name="DEFAULT_ORDER_TYPE">
		          <option value="" ></option>
		          <c:forTokens items="${siteConfig['ORDER_TYPE'].value}" delims="," var="type" varStatus="status">
			        <option value ="${type}" <c:if test="${type == siteConfig['DEFAULT_ORDER_TYPE'].value}">selected</c:if>><c:out value ="${type}" /></option>
		  	      </c:forTokens>
		 	  </select>
		     </div>
		     <div class="form">
		       <label><input type="hidden" name="__key" value="ORDER_FLAG1_NAME">Order Flag1 Name:</label>
		       <input type="text" name="ORDER_FLAG1_NAME" value="<c:out value="${siteConfig['ORDER_FLAG1_NAME'].value}"/>" maxlength="255" size="50">
		       <div class="helpNote" style="padding-left: 140px;">
				<p>Custom Field for Order. Yes, No value. Available on Order List filter.</p>
			   </div>
		     </div>
		     <div class="form">
		       <label><input type="hidden" name="__key" value="ORDER_CUSTOM_FIELD1_NAME">Order Custom Field 1 Name:</label>
		       <input type="text" name="ORDER_CUSTOM_FIELD1_NAME" value="<c:out value="${siteConfig['ORDER_CUSTOM_FIELD1_NAME'].value}"/>" maxlength="255" size="50">
		       <div class="helpNote" style="padding-left: 140px;">
				<p>Custom Field for Order. Max 100 characters.</p>
			   </div>
		     </div>
		     <div class="form">
		       <label><input type="hidden" name="__key" value="ORDER_CUSTOM_FIELD1_VALUE">Order Custom Field 1 Value</label>
		       <input type="text" name="ORDER_CUSTOM_FIELD1_VALUE" value="<c:out value="${siteConfig['ORDER_CUSTOM_FIELD1_VALUE'].value}"/>" maxlength="255" size="50">
		     </div>
		     <div class="form">
		       <label><input type="hidden" name="__key" value="ORDER_CUSTOM_FIELD2_NAME">Order Custom Field 2 Name:</label>
		       <input type="text" name="ORDER_CUSTOM_FIELD2_NAME" value="<c:out value="${siteConfig['ORDER_CUSTOM_FIELD2_NAME'].value}"/>" maxlength="255" size="50">
		       <div class="helpNote" style="padding-left: 140px;">
				<p>Custom Field for Order. Max 100 characters.</p>
			   </div>
		     </div>
		       <div class="form">
		       <label><input type="hidden" name="__key" value="ORDER_CUSTOM_FIELD2_VALUE">Order Custom Field 2 Value:</label>
		       <input type="text" name="ORDER_CUSTOM_FIELD2_VALUE" value="<c:out value="${siteConfig['ORDER_CUSTOM_FIELD2_VALUE'].value}"/>" maxlength="255" size="50">
		     </div>
		     <div class="form">
		       <label><input type="hidden" name="__key" value="ORDER_CUSTOM_FIELD3_NAME">Order Custom Field 3 Name:</label>
		       <input type="text" name="ORDER_CUSTOM_FIELD3_NAME" value="<c:out value="${siteConfig['ORDER_CUSTOM_FIELD3_NAME'].value}"/>" maxlength="255" size="50">
		       <div class="helpNote" style="padding-left: 140px;">
				<p>Custom Field for Order. Max 100 characters.</p>
			   </div>
		     </div>
		     <div class="form">
		       <label><input type="hidden" name="__key" value="ORDER_TYPE">Order Type:</label>
		       <input type="text" name="ORDER_TYPE" value="<c:out value="${siteConfig['ORDER_TYPE'].value}"/>" maxlength="255" size="50">
		     </div>
		     <div class="form">
		       <label><input type="hidden" name="__key" value="ORDER_CANCEL_REASONS">Order Cancel Reasons:</label>
		       <input type="text" name="ORDER_CANCEL_REASONS" value="<c:out value="${siteConfig['ORDER_CANCEL_REASONS'].value}"/>" maxlength="255" size="50">
		     </div>
		     <div class="form">
		       <label><input type="hidden" name="__key" value="INVOICE_REMOVE_VALIDATION"><fmt:message key="removeValidationOnInvoice" />:</label>
		       <input type="checkbox" name="INVOICE_REMOVE_VALIDATION" value ="true" <c:if test="${siteConfig['INVOICE_REMOVE_VALIDATION'].value == 'true'}">checked</c:if>>
		     </div>
		     <div class="form">
		       <label><input type="hidden" name="__key" value="ORDER_TRACKCODE_REQUIRED"><fmt:message key="trackcodeRequired" />:</label>
		       <input type="checkbox" name="ORDER_TRACKCODE_REQUIRED" value ="true" <c:if test="${siteConfig['ORDER_TRACKCODE_REQUIRED'].value == 'true'}">checked</c:if>>
		     </div>
		     <div class="form">
		       <label><input type="hidden" name="__key" value="SHOW_BALANCE_ON_FRONTEND"><fmt:message key="balanceOnFrontend" />:</label>
		       <input type="checkbox" name="SHOW_BALANCE_ON_FRONTEND" value ="true" <c:if test="${siteConfig['SHOW_BALANCE_ON_FRONTEND'].value == 'true'}">checked</c:if>>
		     </div>
		     <div class="title"><fmt:message key="quote" /> <fmt:message key="options" /></div>
		     <div class="form">
		       <label><input type="hidden" name="__key" value="QUOTE_VIEW_LIST_THANK_YOU_ID"><fmt:message key="quoteViewThankYouCategoryId" />:</label>
		       <input type="text" name="QUOTE_VIEW_LIST_THANK_YOU_ID" value="<c:out value="${siteConfig['QUOTE_VIEW_LIST_THANK_YOU_ID'].value}"/>" maxlength="10" size="10">
		     </div>
		     <c:if test="${gSiteConfig['gLOYALTY']}">
		     <div class="title"><fmt:message key="loyalty" /> <fmt:message key="options" /></div>
		     <div class="form">
		       <label><input type="hidden" name="__key" value="LOYALTY_SHOW_ON_MYACCOUNT"><fmt:message key="showLoyaltyPointOnMyAccount" />:</label>
		       <input type="checkbox" name="LOYALTY_SHOW_ON_MYACCOUNT" value ="true" <c:if test="${siteConfig['LOYALTY_SHOW_ON_MYACCOUNT'].value == 'true'}">checked</c:if>>
		     </div>
		     </c:if>
		     <c:if test="${gSiteConfig['gLOCATION']}">
		     <div class="title"><fmt:message key="location" /> <fmt:message key="options" /></div>
		     <div class="form">
		       <label><input type="hidden" name="__key" value="GOOGLE_MAP_KEY">Google Map Key:</label>
		       <input type="text" name="GOOGLE_MAP_KEY" value="<c:out value="${siteConfig['GOOGLE_MAP_KEY'].value}"/>" maxlength="255" size="50">
		     </div>
		 	<div class="form">
		       <label><input type="hidden" name="__key" value="LOCATION_SORTING"><fmt:message key="location" /><fmt:message key="sortBy" />:</label>
		       <select name="LOCATION_SORTING">
		  	    <option value="name" <c:if test="${siteConfig['LOCATION_SORTING'].value == 'name'}">selected</c:if>><fmt:message key="location" /></option>
		  	    <option value="state_province" <c:if test="${siteConfig['LOCATION_SORTING'].value == 'state_province'}">selected</c:if>><fmt:message key="state" /></option>
		 	  </select>
		     </div> 
		     </c:if>
		     <c:if test="${gSiteConfig['gEVENT']}">
		     <div class="title"><fmt:message key="event" /> <fmt:message key="options" /></div>
		     <div class="form">
		       <label><input type="hidden" name="__key" value="EVENT_LIST">Event :</label>
		       <input type="text" name="EVENT_LIST" value="<c:out value="${siteConfig['EVENT_LIST'].value}"/>" maxlength="255" size="50">
		     </div>
		     </c:if>
		     <c:if test="${gSiteConfig['gROAD_RUNNER']}">
		     <div class="title"><fmt:message key="roadRunner" /></div>
		     <div class="form">
		       <label><input type="hidden" name="__key" value="RR_USERNAME">Username :</label>
		       <input type="text" name="RR_USERNAME" value="<c:out value="${siteConfig['RR_USERNAME'].value}"/>" maxlength="20" size="50">
		     </div>
		     <div class="form">
		       <label><input type="hidden" name="__key" value="RR_PASSWORD">Password :</label>
		       <input type="text" name="RR_PASSWORD" value="<c:out value="${siteConfig['RR_PASSWORD'].value}"/>" maxlength="20" size="50">
		     </div>
		     <div class="form">
		       <label><input type="hidden" name="__key" value="RR_ORIGIN_ZIP">Origin Zip :</label>
		       <input type="text" name="RR_ORIGIN_ZIP" value="<c:out value="${siteConfig['RR_ORIGIN_ZIP'].value}"/>" maxlength="5" size="50">
		     </div>
		     <div class="form">
		       <label><input type="hidden" name="__key" value="RR_ORIGIN_TYPE">Origin Type :</label>
		       <input type="text" name="RR_ORIGIN_TYPE" value="<c:out value="${siteConfig['RR_ORIGIN_TYPE'].value}"/>" maxlength="1" size="50">
		     </div>
		     <div class="form">
		       <label><input type="hidden" name="__key" value="RR_PAYMENT_TYPE">Payment Type :</label>
		       <input type="text" name="RR_PAYMENT_TYPE" value="<c:out value="${siteConfig['RR_PAYMENT_TYPE'].value}"/>" maxlength="1" size="50">
		     </div>
		     <div class="form">
		       <label><input type="hidden" name="__key" value="RR_ACTUAL_CLASS">Actual Class :</label>
		       <input type="text" name="RR_ACTUAL_CLASS" value="<c:out value="${siteConfig['RR_ACTUAL_CLASS'].value}"/>" maxlength="3" size="50">
		     </div>
		     </c:if>
		     
		     <div class="title"><fmt:message key="externalAccess" /></div>
		     <div class="form">
		       <label><input type="hidden" name="__key" value="PRODUCT1_DIRECTORY">Product Pages :</label>
		       <input type="text" name="PRODUCT1_DIRECTORY" value="<c:out value="${siteConfig['PRODUCT1_DIRECTORY'].value}"/>" maxlength="50" size="50">
		     </div>
		     <div class="form">
		       <label><input type="hidden" name="__key" value="TSR_DIRECTORY">Tsr Pages :</label>
		       <input type="text" name="TSR_DIRECTORY" value="<c:out value="${siteConfig['TSR_DIRECTORY'].value}"/>" maxlength="50" size="50">
		     </div>
		     <div class="form">
		       <label><input type="hidden" name="__key" value="CATEGORY1_DIRECTORY">Category Pages :</label>
		       <input type="text" name="CATEGORY1_DIRECTORY" value="<c:out value="${siteConfig['CATEGORY1_DIRECTORY'].value}"/>" maxlength="50" size="50">
		     </div>
		     <div class="form">
		       <label><input type="hidden" name="__key" value="FRONTEND_REGISTRATION_DIRECTORY">Frontend Registration Page :</label>
		       <input type="text" name="FRONTEND_REGISTRATION_DIRECTORY" value="<c:out value="${siteConfig['FRONTEND_REGISTRATION_DIRECTORY'].value}"/>" maxlength="50" size="50">
		     </div>
		     <div class="form">
		       <label><input type="hidden" name="__key" value="FRONTEND_LOGIN_DIRECTORY">Frontend Login Page :</label>
		       <input type="text" name="FRONTEND_LOGIN_DIRECTORY" value="<c:out value="${siteConfig['FRONTEND_LOGIN_DIRECTORY'].value}"/>" maxlength="50" size="50">
		     </div>
		        
		   </div>
	<!-- end tab -->        
	</div>
	</sec:authorize>

	<c:if test="${gSiteConfig['gMOD_REWRITE'] == '1'}">
    <sec:authorize ifAnyGranted="ROLE_AEM,ROLE_ADMIN_${prop_site_id},ROLE_SITE_INFO">       	
	<h4 title="SEO">SEO</h4>
	<div>
	<!-- start tab -->
	
	  	<div class="listdivi ln tabdivi"></div>
	  	<div class="listdivi"></div>		

			<div class="ConfigNavDialogTitle1">
			   <strong>Site Configuration</strong>
			</div>
			<div class="ConfigNavDialogTitle1Container" >			
			<div class="title">Mod Rewrite Options</div>

		     <div class="form">
		       <label><input type="hidden" name="__key" value="MOD_REWRITE_PRODUCT">Product Label:</label>
		       <input type="text" name="MOD_REWRITE_PRODUCT" value="<c:out value="${siteConfig['MOD_REWRITE_PRODUCT'].value}"/>" maxlength="255" size="50">
		     </div>
		     <div class="form">
		       <label><input type="hidden" name="__key" value="MOD_REWRITE_CATEGORY">Category Label:</label>
		       <input type="text" name="MOD_REWRITE_CATEGORY" value="<c:out value="${siteConfig['MOD_REWRITE_CATEGORY'].value}"/>" maxlength="255" size="50">
		     </div>
		     
		     <div class="form">
		     <table>
		       <tr>
		         <td colspan="2">NOTE:</td>
		       </tr>
		       <tr>
		         <td>1.</td>
		         <td>Label for category and product must be different.</td>
		       </tr>
		       <tr>
		         <td>2.</td>
		         <td>On .htaccess, <span style="font-weight:bold;color:blue;">"product"</span> and <span style="font-weight:bold;color:red;">"category"</span> must match Product and Category Label.</td>
		       </tr>
		       <tr>
		         <td>&nbsp;</td>
		         <td>
		         	RewriteEngine on<br/>
		         	RewriteRule ^<span style="font-weight:bold;color:blue;">product</span>/(.*)/(.*)\.html$ product.jhtm?sku=$1 [QSA]<br/>
					RewriteRule ^<span style="font-weight:bold;color:red;">category</span>/([0-9]+)/(.*)\.html$ category.jhtm?cid=$1 [QSA]
				 </td>
		       </tr>
		     </table>
		     </div>
		     
		     <div class="form">
		       <label><input type="hidden" name="__key" value="CATEGORY_OVERRIDE_CANONICAL">Override Category Canonical using field:</label>
		       <select name="CATEGORY_OVERRIDE_CANONICAL">
		  	    <option value=""></option>
		  	    <option value="1" <c:if test="${siteConfig['CATEGORY_OVERRIDE_CANONICAL'].value == '1'}">selected</c:if>>Field 1</option>
		  	    <option value="2" <c:if test="${siteConfig['CATEGORY_OVERRIDE_CANONICAL'].value == '2'}">selected</c:if>>Field 2</option>
		  	    <option value="3" <c:if test="${siteConfig['CATEGORY_OVERRIDE_CANONICAL'].value == '3'}">selected</c:if>>Field 3</option>
				<option value="4" <c:if test="${siteConfig['CATEGORY_OVERRIDE_CANONICAL'].value == '4'}">selected</c:if>>Field 4</option>
				<option value="5" <c:if test="${siteConfig['CATEGORY_OVERRIDE_CANONICAL'].value == '5'}">selected</c:if>>Field 5</option>
		 	   </select>
		     </div>
		     		     
	        </div>
    <!-- end tab -->        
	</div>
	</sec:authorize>	
	</c:if>
	
	<c:if test="${gSiteConfig['gCRM']}">
    <sec:authorize ifAnyGranted="ROLE_AEM,ROLE_ADMIN_${prop_site_id},ROLE_SITE_INFO">       	
	<h4 title="CRM">CRM Configuration</h4>
	<div>
	<!-- start tab -->
	
	  	<div class="listdivi ln tabdivi"></div>
	  	<div class="listdivi"></div>		
	  		
			<div class="ConfigNavDialogTitle1">
			   <strong>Site Configuration</strong>
			</div>
			<div class="ConfigNavDialogTitle1Container" >			
			<div class="title">CRM Options</div>
		     <div class="form">
		       <label><input type="hidden" name="__key" value="CRM_LEAD_SOURCE">Lead Source:</label>
		       <input type="text" name="CRM_LEAD_SOURCE" value="<c:out value="${siteConfig['CRM_LEAD_SOURCE'].value}"/>" maxlength="255" size="50">
		     </div>  
		     <div class="form">
		       <label><input type="hidden" name="__key" value="CRM_TASK_TYPE">Task Type:</label>
		       <input type="text" name="CRM_TASK_TYPE" value="<c:out value="${siteConfig['CRM_TASK_TYPE'].value}"/>" maxlength="255" size="50">
		     </div>  
		     <div class="form">
		       <label><input type="hidden" name="__key" value="CRM_TASK_RANK">Task Rank:</label>
		       <input type="text" name="CRM_TASK_RANK" value="<c:out value="${siteConfig['CRM_TASK_RANK'].value}"/>" maxlength="255" size="50">
		     </div>  
		     <div class="form">
		       <label><input type="hidden" name="__key" value="CRM_TASK_ACTION">Task Action:</label>
		       <input type="text" name="CRM_TASK_ACTION" value="<c:out value="${siteConfig['CRM_TASK_ACTION'].value}"/>" size="50">
		     </div>
		     <div class="form">
		       <label><input type="hidden" name="__key" value="CRM_RATING"><fmt:message key="crmRating"/>:</label>
		       <input type="text" name="CRM_RATING" value="<c:out value="${siteConfig['CRM_RATING'].value}"/>" maxlength="255" size="50">
		     </div>  
		     <div class="form">
		       <label><input type="hidden" name="__key" value="CRM_ACCOUNT_CONTACT_TYPE">Account/Contact Type:</label>
		       <input type="text" name="CRM_ACCOUNT_CONTACT_TYPE" value="<c:out value="${siteConfig['CRM_ACCOUNT_CONTACT_TYPE'].value}"/>" maxlength="255" size="50">
		     </div>  
		     <div class="form">
		       <label><input type="hidden" name="__key" value="CRM_ACCOUNT_INDUSTRY">Account Industry:</label>
		       <input type="text" name="CRM_ACCOUNT_INDUSTRY" value="<c:out value="${siteConfig['CRM_ACCOUNT_INDUSTRY'].value}"/>" size="50">
		     </div> 
		     <div class="form">
		       <label><input type="hidden" name="__key" value="CREATE_TASK_ON_EMAIL">Create Task On Email:</label>
		       <input type="checkbox" name="CREATE_TASK_ON_EMAIL" value ="true" <c:if test="${siteConfig['CREATE_TASK_ON_EMAIL'].value == 'true'}">checked</c:if>>
		     </div>
		    </div>
    <!-- end tab -->        
	</div>
	</sec:authorize>	
	</c:if>
	       
		
	<sec:authorize ifAllGranted="ROLE_AEM">	
	<h4 title="Customer Information">AEM ONLY</h4>
	<div>
	<!-- start tab -->
	
	  		<div class="listdivi ln tabdivi"></div>
	  		<div class="listdivi"></div>		
	  		 
			<div class="ConfigNavDialogTitle2">
			    <strong>AEM ONLY</strong>
			</div>
			<div class="ConfigNavDialogTitle2Container" >			
			<div class="title">Common Store Options</div>
<%-- 
			 <div class="form">
		       <label><input type="hidden" name="__key" value="SITE_TIME_ZONE">Time Zone:</label>
		       <select name="SITE_TIME_ZONE">
		  	    <option value=""></option>
		  	    <option value="EST" <c:if test="${siteConfig['SITE_TIME_ZONE'].value == 'EST'}">selected</c:if>>EST</option>
		  	    <option value="CST" <c:if test="${siteConfig['SITE_TIME_ZONE'].value == 'CST'}">selected</c:if>>CST</option>
		  	    <option value="MST" <c:if test="${siteConfig['SITE_TIME_ZONE'].value == 'MST'}">selected</c:if>>MST</option>
				<option value="PST" <c:if test="${siteConfig['SITE_TIME_ZONE'].value == 'PST'}">selected</c:if>>PST</option>
		 	   </select>
		     </div> 
--%>
		     <div class="form">
		       <label><input type="hidden" name="__key" value="SITE_URL">URL:</label>
		       <input type="text" name="SITE_URL" value="<c:out value="${siteConfig['SITE_URL'].value}"/>" maxlength="255" size="50">
		     </div>    
		     <div class="form">
		       <label><input type="hidden" name="__key" value="SECURE_URL">Secure URL:</label>
		       <input type="text" name="SECURE_URL" value="<c:out value="${siteConfig['SECURE_URL'].value}"/>" maxlength="255" size="50">
		     </div>  
		     <div class="form">
		       <label><input type="hidden" name="__gkey" value="gRESELLER">Reseller login page:</label>
		       <select name="gRESELLER">
		  	    <option value="aem" <c:if test="${'aem' == gSiteConfig['gRESELLER']}">selected</c:if>>aem</option>
		  	    <option value="magnum" <c:if test="${'magnum' == gSiteConfig['gRESELLER']}">selected</c:if>>magnum</option>
		  	    <option value="888vghost" <c:if test="${'888vghost' == gSiteConfig['gRESELLER']}">selected</c:if>>888vghost</option>
		  	    <option value="engaging" <c:if test="${'engaging' == gSiteConfig['gRESELLER']}">selected</c:if>>engaging</option>
				<option value="top10" <c:if test="${'top10' == gSiteConfig['gRESELLER']}">selected</c:if>>top10</option>
				<option value="jabsol" <c:if test="${'jabsol' == gSiteConfig['gRESELLER']}">selected</c:if>>jabsol</option>
				<option value="ardis" <c:if test="${'ardis' == gSiteConfig['gRESELLER']}">selected</c:if>>ardis</option>
				<option value="demo" <c:if test="${'demo' == gSiteConfig['gRESELLER']}">selected</c:if>>demo</option>
				<option value="roi" <c:if test="${'roi' == gSiteConfig['gRESELLER']}">selected</c:if>>roi</option>
				<option value="nethology" <c:if test="${'nethology' == gSiteConfig['gRESELLER']}">selected</c:if>>nethology</option>
				<option value="via" <c:if test="${'via' == gSiteConfig['gRESELLER']}">selected</c:if>>viatrading</option>
				<option value="ebizstore" <c:if test="${'ebizstore' == gSiteConfig['gRESELLER']}">selected</c:if>>ebizstore</option>
				<option value="datamagic" <c:if test="${'datamagic' == gSiteConfig['gRESELLER']}">selected</c:if>>datamagic</option>
		 	   </select>
		     </div> 
		     <div class="form">
		       <label><input type="hidden" name="__gkey" value="gADMIN_LOGIN_TIPS"><fmt:message key="adminLoginTips" />:</label>
		       <input type="checkbox" name="gADMIN_LOGIN_TIPS" value ="1" <c:if test="${gSiteConfig['gADMIN_LOGIN_TIPS']}">checked</c:if> />
		     </div>      
		     <div class="form">
		       <label><input type="hidden" name="__gkey" value="gSHOPPING_CART">Shopping Cart:</label>
		       <fmt:message key="yes" /> <input type="radio" name="gSHOPPING_CART" id="SHOPPING_CART_YES" value="1" <c:if test="${gSiteConfig['gSHOPPING_CART']}">checked</c:if> onclick="toggleSalesRep(this.id);toggleShoppingCartRecommendedList(this.id)">
		       <fmt:message key="no" /> <input type="radio" name="gSHOPPING_CART" id="SHOPPING_CART_NO" value="0" <c:if test="${not gSiteConfig['gSHOPPING_CART']}">checked</c:if> onclick="toggleSalesRep(this.id);toggleShoppingCartRecommendedList(this.id)">
		       <input type="hidden" name="__key" value="SHOPPINGCART_SEARCH">
		       <select name="SHOPPINGCART_SEARCH">
		  	    <option value="false">&nbsp;</option>
		  	    <option value="true" <c:if test="${siteConfig['SHOPPINGCART_SEARCH'].value == 'true'}">selected</c:if>>Search on Customer List</option>		  	    
		  	   </select>
		  	   <div class="helpNote" style="padding-left: 140px;">
				<p>if enable, ShoppingCart will show on search of customer list</p>
			   </div>
		     </div>  
		     <div class="form" >
		       <label><input type="hidden" name="__key" value="CHECKOUT2_VISIBILITY">Checkout2 Visibility:</label>
		       <select name="CHECKOUT2_VISIBILITY">
		         <option value="-1">Disable Checkout2</option>
		  	    <c:forEach begin="0" end="100" step="10" var="value">
		  	      <option value="${value}" <c:if test="${siteConfig['CHECKOUT2_VISIBILITY'].value == value}">selected</c:if>>${value}</option>
				</c:forEach>
		 	   </select>
		     </div> 
		     <div class="form" id="SalesRepID">
		       <label><input type="hidden" name="__gkey" value="gSALES_REP"><fmt:message key="salesRep" />:</label>
		       <input type="checkbox" name="gSALES_REP" value ="1" <c:if test="${gSiteConfig['gSALES_REP']}">checked</c:if>>
		       <input type="hidden" name="__key" value="SALESREP_LOGIN">
		       <select name="SALESREP_LOGIN">
		  	    <option value="false">&nbsp;</option>
		  	    <option value="true" <c:if test="${siteConfig['SALESREP_LOGIN'].value == 'true'}">selected</c:if>>Login</option>
		  	   </select>
		     </div> 
		     <div class="form">
		       <label><input type="hidden" name="__key" value="SALESREP_PARENT"><fmt:message key="salesRep" /> <fmt:message key="subAccounts" />:</label>
		       <input type="checkbox" name="SALESREP_PARENT" value ="true" <c:if test="${siteConfig['SALESREP_PARENT'].value == 'true'}">checked</c:if>>
		     </div>
		     <div class="form">
		       <label><input type="hidden" name="__key" value="SALESREP_PROTECTED_ACCESS"><fmt:message key="salesRep" /> <fmt:message key="protectedAccess" />:</label>
		       <input type="checkbox" name="SALESREP_PROTECTED_ACCESS" value ="true" <c:if test="${siteConfig['SALESREP_PROTECTED_ACCESS'].value == 'true'}">checked</c:if>>
		     </div>
		     <div class="form" id="ShoppingCartRecommendedList">
		       <label><input type="hidden" name="__key" value="SHOPPING_CART_RECOMMENDED_LIST"><fmt:message key="shoppingCartRecommendedList" />:</label>
		       <input type="checkbox" name="SHOPPING_CART_RECOMMENDED_LIST" value ="true" <c:if test="${siteConfig['SHOPPING_CART_RECOMMENDED_LIST'].value == 'true'}">checked</c:if>>
		     </div>
		     <div class="form">
		       <label><input type="hidden" name="__key" value="UPSELL_RECOMMENDED_LIST"><fmt:message key="upsellRecommendedList" />:</label>
		       <input type="checkbox" name="UPSELL_RECOMMENDED_LIST" value ="true" <c:if test="${siteConfig['UPSELL_RECOMMENDED_LIST'].value == 'true'}">checked</c:if>>
		     </div>
		     <div class="form">
		       <label><input type="hidden" name="__gkey" value="gACCESS_PRIVILEGE"><fmt:message key="accessPrivilege" />:</label>
		       <input type="checkbox" name="gACCESS_PRIVILEGE" id="gACCESS_PRIVILEGE" value ="1" <c:if test="${gSiteConfig['gACCESS_PRIVILEGE']}">checked</c:if> onclick="toggleAccessPrivilege(this.id)">
		     </div>
		     <div class="form">
		       <label><input type="hidden" name="__key" value="ACCESS_PRIVILEGE_GROUP"><fmt:message key="accessPrivilege" /> Group:</label>
		       <input type="checkbox" name="ACCESS_PRIVILEGE_GROUP" value ="true" <c:if test="${siteConfig['ACCESS_PRIVILEGE_GROUP'].value == 'true'}">checked</c:if>>
		     </div>
		     <div class="form" id="accessBody">
		       <label><input type="hidden" name="__gkey" value="gNUMBER_ACCESS_PRIVILEGE"># <fmt:message key="of" /> <fmt:message key="user" /> :</label>
		       <select name="gNUMBER_ACCESS_PRIVILEGE">
		  	    <c:forEach begin="5" end="100" step="5" var="value">
		  	      <option value="${value}" <c:if test="${value == gSiteConfig['gNUMBER_ACCESS_PRIVILEGE']}">selected</c:if>>${value}</option>
				</c:forEach>
		 	   </select>
		     </div>      
		     <div class="form">
		       <label><input type="hidden" name="__gkey" value="gEVENT"><fmt:message key="event" />:</label>
		       <input type="checkbox" name="gEVENT" id="gEVENT" value ="1" <c:if test="${gSiteConfig['gEVENT']}">checked</c:if>>
		     </div>
		     <div class="form">
		       <label><input type="hidden" name="__gkey" value="gSUPPLIER"><fmt:message key="supplier" />:</label>
		       <input type="checkbox" name="gSUPPLIER" value ="1" <c:if test="${gSiteConfig['gSUPPLIER']}">checked</c:if>>
		     </div>
		     <div class="form">
		       <label><input type="hidden" name="__key" value="SUPPLIER_MULTI_ADDRESS"><fmt:message key="supplierMultiAddress" />:</label>
		       <input type="checkbox" name="SUPPLIER_MULTI_ADDRESS" value ="true" <c:if test="${siteConfig['SUPPLIER_MULTI_ADDRESS'].value == 'true'}">checked</c:if>>
		     </div>
		     
		     <div class="form">
		       <label><input type="hidden" name="__gkey" value="gSPECIAL_PRICING"><fmt:message key="specialPricing" />:</label>
		       <input type="checkbox" name="gSPECIAL_PRICING" value ="1" <c:if test="${gSiteConfig['gSPECIAL_PRICING']}">checked</c:if>>
		     </div> 
		     <div class="form">
		       <label><input type="hidden" name="__gkey" value="gCOUNTY_TAX"><fmt:message key="countyTax" />:</label>
		       <input type="checkbox" name="gCOUNTY_TAX" value ="1" <c:if test="${gSiteConfig['gCOUNTY_TAX']}">checked</c:if>>
		     </div>  
		     <div class="form">
		       <label><input type="hidden" name="__gkey" value="gCITY_TAX"><fmt:message key="cityTax" />:</label>
		       <input type="checkbox" id="gCITY_TAX" name="gCITY_TAX" value ="1" <c:if test="${gSiteConfig['gCITY_TAX']}">checked</c:if> onclick="toggleCountyTax(this.id)">
		     </div> 
		     <div class="form">
		       <label><input type="hidden" name="__gkey" value="gTAX_AFTER_DISCOUNT"><fmt:message key="applyTaxAfterDiscount" />:</label>
		       <input type="checkbox" name="gTAX_AFTER_DISCOUNT" value ="1" <c:if test="${gSiteConfig['gTAX_AFTER_DISCOUNT']}">checked</c:if>>
		     </div>             
		     <div class="form">
		       <label><input type="hidden" name="__key" value="NO_SHIPPING"><fmt:message key="noShipping" />:</label>
		    	<fmt:message key="yes" /> <input type="radio" name="NO_SHIPPING" value="true" <c:if test="${siteConfig['NO_SHIPPING'].value == 'true'}">checked</c:if>>
		    	<fmt:message key="no" /> <input type="radio" name="NO_SHIPPING" value="false" <c:if test="${siteConfig['NO_SHIPPING'].value == 'false'}">checked</c:if>>
		     </div>    
		     <div class="form" id="reviewType">    
		       <label><input type="hidden" name="__key" value="SHIPPING_PER_SUPPLIER"><fmt:message key="shippingPerSupplier" />:</label>
		       <select name="SHIPPING_PER_SUPPLIER">
		  	    <option value="false"><fmt:message key="no" /></option>
		  	    <option value="true" <c:if test="${siteConfig['SHIPPING_PER_SUPPLIER'].value == 'true'}">selected</c:if>><fmt:message key="yes" /></option>
		 	   </select>
		     </div>
		     <div class="form">
		       <label><input type="hidden" name="__gkey" value="gMYLIST"><fmt:message key="myList" />:</label>
		       <input type="checkbox" name="gMYLIST" value ="1" <c:if test="${gSiteConfig['gMYLIST']}">checked</c:if>>
		       <input type="hidden" name="__key" value="MYLIST_THUMBNAIL">
		       <select name="MYLIST_THUMBNAIL">
		  	    <option value="false">&nbsp;</option>
		  	    <option value="true" <c:if test="${siteConfig['MYLIST_THUMBNAIL'].value == 'true'}">selected</c:if>><fmt:message key="myList" /> on Default Thumbnail</option>
		  	   </select>  
		     </div>    
		     <div class="form">
		       <label><input type="hidden" name="__gkey" value="gMYORDER_LIST"><fmt:message key="myOrderList" />:</label>
		       <fmt:message key="yes" /> <input type="radio" name="gMYORDER_LIST" value="1" <c:if test="${gSiteConfig['gMYORDER_LIST']}">checked</c:if> >
		       <fmt:message key="no" /> <input type="radio" name="gMYORDER_LIST" value="0" <c:if test="${not gSiteConfig['gMYORDER_LIST']}">checked</c:if> >
		     </div>
		     <div class="form">
		       <label><input type="hidden" name="__gkey" value="gREORDER"><fmt:message key="reorder" />:</label>
		       <input type="checkbox" name="gREORDER" value ="1" <c:if test="${gSiteConfig['gREORDER']}">checked</c:if>>
		     </div>        
		     <div class="form">
		       <label><input type="hidden" name="__gkey" value="gREGISTRATION_DISABLED">Registration Disabled:</label>
		       <fmt:message key="yes" /> <input type="radio" name="gREGISTRATION_DISABLED" value="1" <c:if test="${gSiteConfig['gREGISTRATION_DISABLED']}">checked</c:if> >
		       <fmt:message key="no" /> <input type="radio" name="gREGISTRATION_DISABLED" value="0" <c:if test="${not gSiteConfig['gREGISTRATION_DISABLED']}">checked</c:if> >
		     </div>    
		     <div class="form">
		       <label><input type="hidden" name="__gkey" value="gREGISTRATION_TOUCH_SCREEN">Registration Touch Screen:</label>
		       <input type="checkbox" name="gREGISTRATION_TOUCH_SCREEN" value ="1" <c:if test="${gSiteConfig['gREGISTRATION_TOUCH_SCREEN']}">checked</c:if>>
		     </div>
		     <div class="form">
		       <label><input type="hidden" name="__gkey" value="gWATERMARK">Watermark:</label>
		       <fmt:message key="yes" /> <input type="radio" name="gWATERMARK" value="1" <c:if test="${gSiteConfig['gWATERMARK']}">checked</c:if>>
		       <fmt:message key="no" /> <input type="radio" name="gWATERMARK" value="0" <c:if test="${not gSiteConfig['gWATERMARK']}">checked</c:if>>
		     </div>    
		     <div class="form">
		       <label><input type="hidden" name="__gkey" value="gGROUP_FAQ">Group FAQ:</label>
		       <fmt:message key="yes" /> <input type="radio" name="gGROUP_FAQ" value="1" <c:if test="${gSiteConfig['gGROUP_FAQ']}">checked</c:if>>
		       <fmt:message key="no" /> <input type="radio" name="gGROUP_FAQ" value="0" <c:if test="${not gSiteConfig['gGROUP_FAQ']}">checked</c:if>>
		     </div>    
		     <div class="form">
		       <label><input type="hidden" name="__gkey" value="gSEARCH">3rd Party Search:</label>
		       <select name="gSEARCH">
		  	    <option value=""><fmt:message key="none" /></option>
		  	    <option value="triguide" <c:if test="${'triguide' == gSiteConfig['gSEARCH']}">selected</c:if>>TriGuide</option>
		 	   </select>
		     </div>     
		     <div class="form">
		       <label><input type="hidden" name="__key" value="MINIMUM_ORDER"> <fmt:message key="minimumOrder" />:</label>
		       <fmt:message key="yes" /> <input type="radio" id="MINIMUM_ORDER_YES" name="MINIMUM_ORDER" value="true" <c:if test="${siteConfig['MINIMUM_ORDER'].value == 'true'}">checked</c:if> onclick="toggleMinimumOrder(this.id)">
		       <fmt:message key="no" /> <input type="radio" id="MINIMUM_ORDER_NO" name="MINIMUM_ORDER" value="false" <c:if test="${siteConfig['MINIMUM_ORDER'].value == 'false'}">checked</c:if> onclick="toggleMinimumOrder(this.id)">
		     </div>    
		     <div class="form">
		       <label><input type="hidden" name="__gkey" value="gSERVICE"><fmt:message key="service" />:</label>
		       <input type="checkbox" name="gSERVICE" value ="1" <c:if test="${gSiteConfig['gSERVICE']}">checked</c:if>>
		     </div>
		     <div class="form">
		       <label><input type="hidden" name="__gkey" value="gRMA"><fmt:message key="rma" />:</label>
		       <input type="checkbox" name="gRMA" value ="1" <c:if test="${gSiteConfig['gRMA']}">checked</c:if>>
		     </div>
		     <div class="form">
		       <label><input type="hidden" name="__gkey" value="gTICKET"><fmt:message key="ticket" />:</label>
		       <input type="checkbox" name="gTICKET" value ="1" <c:if test="${gSiteConfig['gTICKET']}">checked</c:if>>
		     </div>
		     <div class="form">
		       <label><input type="hidden" name="__key" value="TICKET_ATTACHMENT">Ticket Attachment:</label>
		       <input type="checkbox" name="TICKET_ATTACHMENT" value ="true" <c:if test="${siteConfig['TICKET_ATTACHMENT'].value == 'true'}">checked</c:if>>
		     </div>
		     <div class="form">
		       <label><input type="hidden" name="__key" value="TICKET_INTERNAL_ACCESS_USER">Internal Ticket:</label>
		       <input type="checkbox" name="TICKET_INTERNAL_ACCESS_USER" value ="true" id="TICKET_INTERNAL_ACCESS_USER" <c:if test="${siteConfig['TICKET_INTERNAL_ACCESS_USER'].value == 'true'}">checked</c:if>>
		     </div>
		     <div class="form">
		       <label><input type="hidden" name="__gkey" value="gGIFTCARD"><fmt:message key="giftCard" />:</label>
		       <input type="checkbox" name="gGIFTCARD" value ="1" <c:if test="${gSiteConfig['gGIFTCARD']}">checked</c:if> onclick="toggleGiftCard(this.id)">
		     </div>
		     <div class="form">
		       <label><input type="hidden" name="__gkey" value="gLOCATION"><fmt:message key="storeLocator" />/<fmt:message key="salesRepLocator" />:</label>
		       <input type="checkbox" name="gLOCATION" value ="1" <c:if test="${gSiteConfig['gLOCATION']}">checked</c:if>>
		     </div>
		     <div class="form">
		       <label><input type="hidden" name="__gkey" value="gPRESENTATION"><fmt:message key="template" />:</label>
		       <input type="checkbox" name="gPRESENTATION" value ="1" <c:if test="${gSiteConfig['gPRESENTATION']}">checked</c:if>>
		     </div>
		     <div class="form">
		       <label><input type="hidden" name="__gkey" value="gMANUFACTURER"><fmt:message key="manufacturer" />:</label>
		       <input type="checkbox" name="gMANUFACTURER" value ="1" <c:if test="${gSiteConfig['gMANUFACTURER']}">checked</c:if>>
		     </div>
		     <div class="form">
		       <label><input type="hidden" name="__gkey" value="gBUY_REQUEST"><fmt:message key="buyRequest" />:</label>
		       <input type="checkbox" name="gBUY_REQUEST" value ="1" <c:if test="${gSiteConfig['gBUY_REQUEST']}">checked</c:if>>
		     </div>
		     <div class="form">
		       <label><input type="hidden" name="__gkey" value="gPRODUCT_REVIEW"><fmt:message key="productReview" />:</label>
		       <input type="checkbox" name="gPRODUCT_REVIEW" id="gPRODUCT_REVIEW" value ="1" <c:if test="${gSiteConfig['gPRODUCT_REVIEW']}">checked</c:if> onclick="toggleReviewType(this.id)">
		     </div>
		     <div class="form" id="reviewType">    
		       <label><input type="hidden" name="__key" value="PRODUCT_REVIEW_TYPE"><fmt:message key="productReview" /> Type:</label>
		       <select name="PRODUCT_REVIEW_TYPE">
		  	    <option value="login"><fmt:message key="login" /></option>
		  	    <option value="anonymous" <c:if test="${siteConfig['PRODUCT_REVIEW_TYPE'].value == 'anonymous'}">selected</c:if>><fmt:message key="anonymous" /></option>
		 	   </select>
		     </div>
		     <div class="form">
		       <label><input type="hidden" name="__key" value="PRODUCT_RATE">Product Rate:</label>
		       <input type="checkbox" name="PRODUCT_RATE" value ="true" id="PRODUCT_RATE" <c:if test="${siteConfig['PRODUCT_RATE'].value == 'true'}">checked</c:if> onclick="toggleProductRate(this.id)">
		     </div>
		     <div class="form">
		       <label><input type="hidden" name="__gkey" value="gCOMPANY_REVIEW"><fmt:message key="companyReview" />:</label>
		       <input type="checkbox" name="gCOMPANY_REVIEW" value ="1" <c:if test="${gSiteConfig['gCOMPANY_REVIEW']}">checked</c:if>>
		     </div>
		     <div class="form">
		       <label><input type="hidden" name="__gkey" value="gRSS">RSS:</label>
		       <input type="checkbox" name="gRSS" value ="1" <c:if test="${gSiteConfig['gRSS']}">checked</c:if>>
		     </div>
		     <div class="form">
		       <label><input type="hidden" name="__key" value="CUSTOM_SHIPPING_CONTACT_INFO">Custom Shipping Contact List:</label>
		       <input type="checkbox" name="CUSTOM_SHIPPING_CONTACT_INFO" value ="true" <c:if test="${siteConfig['CUSTOM_SHIPPING_CONTACT_INFO'].value == 'true'}">checked</c:if>>
		     </div>
		     <div class="form">
		       <label><input type="hidden" name="__gkey" value="gKIT"><fmt:message key="buildKit" />:</label>
		       <input type="checkbox" name="gKIT" value ="1" <c:if test="${gSiteConfig['gKIT']}">checked</c:if>>
		     </div>
		     <div class="form">
		       <label><input type="hidden" name="__key" value="PROTECTED_HOST">Protected Host/Domain:</label>
		       <input type="text" name="PROTECTED_HOST" value="<c:out value="${siteConfig['PROTECTED_HOST'].value}"/>" maxlength="255" size="50">
		     </div>
		     <div class="form">
		       <label><input type="hidden" name="__key" value="ADMIN_MESSAGE">Admin Message:</label>
		       <input type="text" name="ADMIN_MESSAGE" value="<c:out value="${siteConfig['ADMIN_MESSAGE'].value}"/>" maxlength="255" size="50">
		     </div>
		     <div class="form">    
		       <label><input type="hidden" name="__key" value="LEFTBAR_DEFAULT_TYPE"><fmt:message key="default"/> <fmt:message key="leftBar" />:</label>
		       <select name="LEFTBAR_DEFAULT_TYPE">
		  	    <option value="1"></option>
		  	    <option value="3" <c:if test="${siteConfig['LEFTBAR_DEFAULT_TYPE'].value == '3'}">selected</c:if>><fmt:message key="accordionEffect" /></option>
		  	    <option value="4" <c:if test="${siteConfig['LEFTBAR_DEFAULT_TYPE'].value == '4'}">selected</c:if>>Dynamic Menus</option>
		  	    <option value="5" <c:if test="${siteConfig['LEFTBAR_DEFAULT_TYPE'].value == '5'}">selected</c:if>>Dynamic Menus 2</option>
		  	    <option value="6" <c:if test="${siteConfig['LEFTBAR_DEFAULT_TYPE'].value == '6'}">selected</c:if>>Responsive</option>
		 	   </select>
		     </div>	
		     <div class="form">
		       <label><input type="hidden" name="__mkey" value="LEFTBAR_TYPE">LeftBar Types:</label>
		       <c:forTokens items="1,3,4,5" delims="," var="lType" varStatus="status">
		  	     <input type="checkbox" name="LEFTBAR_TYPE" value ="${lType}" <c:if test="${fn:contains(siteConfig['LEFTBAR_TYPE'].value,lType)}">checked</c:if>><fmt:message key="${lType}" />
		  	   </c:forTokens>
		     </div> 	     
		     <div class="form">
		       <label><input type="hidden" name="__gkey" value="gMOD_REWRITE">Mod Rewrite:</label>
		       <input type="checkbox" name="gMOD_REWRITE" value ="1" <c:if test="${gSiteConfig['gMOD_REWRITE'] == '1'}">checked</c:if>>
		     </div>
		     <div class="form">
		       <label><input type="hidden" name="__key" value="MOOTOOLS">Mootools:</label>
		       <input type="checkbox" name="MOOTOOLS" value ="true" <c:if test="${siteConfig['MOOTOOLS'].value == 'true'}">checked</c:if>>
		     </div>	
		     <div class="form">
		       <label><input type="hidden" name="__key" value="PRODUCT_QUICK_VIEW"><fmt:message key="productQuickView" />:</label>
		       <input type="checkbox" name="PRODUCT_QUICK_VIEW" value ="true" <c:if test="${siteConfig['PRODUCT_QUICK_VIEW'].value == 'true'}">checked</c:if>>
		     </div>
		     <div class="form">
		       <label><input type="hidden" name="__key" value="MINI_CART"><fmt:message key="miniCart" />:</label>
		       <input type="checkbox" name="MINI_CART" value ="true" <c:if test="${siteConfig['MINI_CART'].value == 'true'}">checked</c:if>>
		     </div>	
		     <div class="form">
		       <label><input type="hidden" name="__key" value="TEMPLATE">Site Template</label>
		       <select name="TEMPLATE">
		  	    <option value="template1">Table</option>
		  	    <option value="template4" <c:if test="${siteConfig['TEMPLATE'].value == 'template4'}">selected</c:if>>Div</option>
		  	    <option value="template6" <c:if test="${siteConfig['TEMPLATE'].value == 'template6'}">selected</c:if>>Responsive</option>
		  	   </select>
		     </div>	     
		     <div class="title"><fmt:message key="loyalty" /></div>
		     <div class="form">
		       <label><input type="hidden" name="__gkey" value="gLOYALTY"><fmt:message key="loyalty" />:</label>
		       <input type="checkbox" name="gLOYALTY" value ="1" <c:if test="${gSiteConfig['gLOYALTY']}">checked</c:if> >
		     </div>
		     <div class="form">
		       <label><input type="hidden" name="__key" value="POINT_TO_CREDIT"><fmt:message key="convertPointsToCredit" />:</label>
		       <input type="checkbox" name="POINT_TO_CREDIT" value ="true" <c:if test="${siteConfig['POINT_TO_CREDIT'].value == 'true'}">checked</c:if>>
		     </div>
		     <div class="title"><fmt:message key="inventory" /></div>
		     <div class="form">
		       <label><input type="hidden" name="__gkey" value="gINVENTORY"><fmt:message key="inventory" />:</label>
		       <input type="checkbox" id="gINVENTORY" name="gINVENTORY" value ="1" <c:if test="${gSiteConfig['gINVENTORY']}">checked</c:if> onclick="toggleInventory(this.id)">
		     </div>    
		     <div id="inventoryTypeID" class="form">
		       <label><input type="hidden" name="__key" value="INVENTORY_DEDUCT_TYPE"><fmt:message key="deductInventoryOn" />:</label>
		       <select name="INVENTORY_DEDUCT_TYPE">
		  	    <option value="fly"><fmt:message key="fly" /></option>
		  	    <option value="ship" <c:if test="${siteConfig['INVENTORY_DEDUCT_TYPE'].value == 'ship'}">selected</c:if>><fmt:message key="ship" /></option>
		  	   </select>  
		     </div>
		     <div id="inventoryHistory" class="form">
		       <label><input type="hidden" name="__key" value="INVENTORY_HISTORY"><fmt:message key="inventoryHistory" />:</label>
		       <input type="checkbox" name="INVENTORY_HISTORY" value ="true" <c:if test="${siteConfig['INVENTORY_HISTORY'].value == 'true'}">checked</c:if>>
		     </div>
		     <div id="inventoryImportExportID" class="form">
		       <label><input type="hidden" name="__gkey" value="gINVENTORY_IMPORT_EXPORT"><fmt:message key="importExport" /> <fmt:message key="inventory" />:</label>
		       <input type="checkbox" name="gINVENTORY_IMPORT_EXPORT" value ="1" <c:if test="${gSiteConfig['gINVENTORY_IMPORT_EXPORT']}">checked</c:if>>
		     </div> 
		     <div id="inventoryOnProductImExID" class="form">
		       <label><input type="hidden" name="__key" value="INVENTORY_ON_PRODUCT_IMPORT_EXPORT"><fmt:message key="inventoryOnProductImportExport" />:</label>
		       <input type="checkbox" name="INVENTORY_ON_PRODUCT_IMPORT_EXPORT" value ="true" <c:if test="${siteConfig['INVENTORY_ON_PRODUCT_IMPORT_EXPORT'].value == 'true'}">checked</c:if>>
		     </div> 
		     <%-- 
		     <div id="inventoryUpdateFormID" class="form">
		       <label><input type="hidden" name="__key" value="INVENTORY_UPDATE_FORM"><fmt:message key="updateInventoryOnProductForm" />:</label>
		       <input type="checkbox" name="INVENTORY_UPDATE_FORM" value ="true" <c:if test="${siteConfig['INVENTORY_UPDATE_FORM'].value == 'true'}">checked</c:if>>
		     </div> 
		     --%>
		     <div id="inventoryShowOnhandFormID" class="form">
		       <label><input type="hidden" name="__key" value="INVENTORY_SHOW_ONHAND"><fmt:message key="showinventoryAFS" />:</label>
		       <input type="checkbox" name="INVENTORY_SHOW_ONHAND" value ="true" <c:if test="${siteConfig['INVENTORY_SHOW_ONHAND'].value == 'true'}">checked</c:if>>
		     </div>    
		     <div id="purchaseOrderID" class="form">
		       <label><input type="hidden" name="__gkey" value="gPURCHASE_ORDER"><fmt:message key="purchaseOrder" />:</label>
		       <input type="checkbox" name="gPURCHASE_ORDER" value ="1" <c:if test="${gSiteConfig['gPURCHASE_ORDER']}">checked</c:if>>
		     </div>
		     <div class="title"><fmt:message key="reports" /></div>
		     <div class="form">
		       <label><input type="hidden" name="__gkey" value="gREPORT"><fmt:message key="reports" />:</label>
		       <input type="checkbox" name="gREPORT" value ="1" <c:if test="${gSiteConfig['gREPORT']}">checked</c:if>>
		     </div>     
		    <div class="title"><fmt:message key="siteMap" /></div>
		     <div class="form">
		       <label><input type="hidden" name="__key" value="SITEMAP"><fmt:message key="siteMap" />:</label>
		       <input type="checkbox" name="SITEMAP" value ="true" <c:if test="${siteConfig['SITEMAP'].value == 'true'}">checked</c:if>>
		     </div> 
		     <div class="title">Weather Channel</div>
		     <div class="form">
		       <label><input type="hidden" name="__gkey" value="gWEATHER_CHANNEL"><fmt:message key="weather" />:</label>
		       <input type="checkbox" name="gWEATHER_CHANNEL" value ="1" <c:if test="${gSiteConfig['gWEATHER_CHANNEL']}">checked</c:if>>
		     </div>     
		     <div class="title"><fmt:message key="invoiceOptions" /></div>
		     <div class="form">
		       <label><input type="hidden" name="__gkey" value="gADD_INVOICE"><fmt:message key="addOrder" />:</label>
		       <input type="checkbox" name="gADD_INVOICE" value ="1" <c:if test="${gSiteConfig['gADD_INVOICE']}">checked</c:if>>
		     </div>  
		     <div class="form">
		       <label><input type="hidden" name="__gkey" value="gPDF_INVOICE">PDF <fmt:message key="invoice" />:</label>
		       <input type="checkbox" name="gPDF_INVOICE" value ="1" <c:if test="${gSiteConfig['gPDF_INVOICE']}">checked</c:if>>
		     </div>
		     <div class="form">
		       <label><input type="hidden" name="__key" value="ATTACHE_PDF_ON_PLACE_ORDER"><fmt:message key="attach"/> <fmt:message key="invoice"/> on New Orders:</label>
		       <input type="checkbox" name="ATTACHE_PDF_ON_PLACE_ORDER" value ="true" <c:if test="${siteConfig['ATTACHE_PDF_ON_PLACE_ORDER'].value == 'true'}">checked</c:if>>
		     </div> 
		     <div class="form">
		       <label><input type="hidden" name="__key" value="INVOICE_PDF_UPLOAD">PDF <fmt:message key="upload" />:</label>
		       <fmt:message key="yes" /> <input type="radio" name="INVOICE_PDF_UPLOAD" value="true" <c:if test="${siteConfig['INVOICE_PDF_UPLOAD'].value == 'true'}">checked</c:if>>
		       <fmt:message key="no" /> <input type="radio" name="INVOICE_PDF_UPLOAD" value="false" <c:if test="${siteConfig['INVOICE_PDF_UPLOAD'].value == 'false'}">checked</c:if>>
		     </div>  		     
		     <div class="form">
		       <label><input type="hidden" name="__gkey" value="gORDER_FILEUPLOAD"><fmt:message key="fileUpload" />:</label>
		       <select name="gORDER_FILEUPLOAD">
				<c:forEach begin="0" end="3" var="num">
		  	    <option value="${num}" <c:if test="${num == gSiteConfig['gORDER_FILEUPLOAD']}">selected</c:if>>${num}</option>
				</c:forEach>
		 	   </select>
		     </div>  
		     <div class="form">
		       <label><input type="hidden" name="__key" value="CHANGE_ORDER_DATE"><fmt:message key="changeInvoiceDate" />:</label>
		       <input type="checkbox" name="CHANGE_ORDER_DATE" value ="true" <c:if test="${siteConfig['CHANGE_ORDER_DATE'].value == 'true'}">checked</c:if>>
		     </div> 
		     <div class="form">
		       <label><input type="hidden" name="__key" value="CHANGE_PO_DATE"><fmt:message key="changePODate" />:</label>
		       <input type="checkbox" name="CHANGE_PO_DATE" value ="true" <c:if test="${siteConfig['CHANGE_PO_DATE'].value == 'true'}">checked</c:if>>
		     </div>                  
		     <div class="form">
		       <label><input type="hidden" name="__gkey" value="gINVOICE_EXPORT"><fmt:message key="invoice" /> <fmt:message key="export" />:</label>
		       <input type="checkbox" name="gINVOICE_EXPORT" value ="1" <c:if test="${gSiteConfig['gINVOICE_EXPORT']}">checked</c:if>> <fmt:message key="excelFile" />&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
			   <input type="hidden" name="__mkey" value="INVOICE_EXPORT">
		       <c:forTokens items="vaf,csv,thub,packnwood,xml" delims="," var="type" varStatus="status">
		  	     <input type="checkbox" name="INVOICE_EXPORT" value ="${type}" <c:if test="${fn:contains(siteConfig['INVOICE_EXPORT'].value,type)}">checked</c:if>> ${type}&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
		  	   </c:forTokens>
		     </div>
		     <div class="form">
		       <label><input type="hidden" name="__key" value="SHOW_COST"><fmt:message key="showCost" />:</label>
		       <input type="checkbox" name="SHOW_COST" value ="true" <c:if test="${siteConfig['SHOW_COST'].value == 'true'}">checked</c:if>>
		     </div>    
		     <div class="form">
		       <label><input type="hidden" name="__gkey" value="gACCOUNTING">Accounting Integration:</label>
		       <select name="gACCOUNTING">
		  	    <option value=""></option>
		  	    <option value="MACS" <c:if test="${'MACS' == gSiteConfig['gACCOUNTING']}">selected</c:if>>MACS</option>
		  	    <option value="MAS90" <c:if test="${'MAS90' == gSiteConfig['gACCOUNTING']}">selected</c:if>>MAS90</option>
		  	    <option value="CONCORD" <c:if test="${'CONCORD' == gSiteConfig['gACCOUNTING']}">selected</c:if>>CONCORD</option>
		  	    <option value="TRIPLEFIN" <c:if test="${'TRIPLEFIN' == gSiteConfig['gACCOUNTING']}">selected</c:if>>TRIPLEFIN</option>
		  	    <option value="EVERGREEN" <c:if test="${'EVERGREEN' == gSiteConfig['gACCOUNTING']}">selected</c:if>>EVERGREEN</option>
		 	   </select>
		     </div>
		     <div class="form">
		       <label><input type="hidden" name="__key" value="ALLOW_EDIT_INVOICE_SHIPPED"><fmt:message key="allowEditIvoiceOnShipped" /> :</label>
		       <input type="checkbox" name="ALLOW_EDIT_INVOICE_SHIPPED" value ="true" <c:if test="${siteConfig['ALLOW_EDIT_INVOICE_SHIPPED'].value == 'true'}">checked</c:if>>
		     </div>  
		     <div class="form">
		     	<label><input type="hidden" name="__key" value="ALLOW_EDIT_PAYMENT_SHIPPED"><fmt:message key="allowEditPaymentOnShipped" /> :</label>
		       	<input type="checkbox" name="ALLOW_EDIT_PAYMENT_SHIPPED" value ="true" <c:if test="${siteConfig['ALLOW_EDIT_PAYMENT_SHIPPED'].value == 'true'}">checked</c:if>>
		     </div>
		     <div class="form">
		     	<label><input type="hidden" name="__key" value="SHOW_PAYMENT_HISTORY_ON_PRINT_FRIENDLY"><fmt:message key="showPaymentHistoryOnPrint" /> :</label>
		       	<input type="checkbox" name="SHOW_PAYMENT_HISTORY_ON_PRINT_FRIENDLY" value ="true" <c:if test="${siteConfig['SHOW_PAYMENT_HISTORY_ON_PRINT_FRIENDLY'].value == 'true'}">checked</c:if>>
		     	<div class="helpNote" style="padding-left: 140px;">
				<p>Shows payment history on all print and pdf views.</p>
			   </div>
		     </div>
		     <div class="form">
		       <label><input type="hidden" name="__key" value="GENERATE_PACKING_LIST"><fmt:message key="generatePackingList" /> :</label>
		       <input type="checkbox" name="GENERATE_PACKING_LIST" value ="true" <c:if test="${siteConfig['GENERATE_PACKING_LIST'].value == 'true'}">checked</c:if>>
		     </div>  
		     <div class="form">
		       <label><input type="hidden" name="__gkey" value="gSUBSCRIPTION"><fmt:message key="subscription" /> (scheduler required on server):</label>
		       <input type="checkbox" name="gSUBSCRIPTION" value ="1" <c:if test="${gSiteConfig['gSUBSCRIPTION']}">checked</c:if>>
		     </div>
		     <div class="form">
		       <label><input type="hidden" name="__gkey" value="gINVOICE_APPROVAL"><fmt:message key="invoiceApproval" />:</label>
		       <input type="checkbox" name="gINVOICE_APPROVAL" value ="1" <c:if test="${gSiteConfig['gINVOICE_APPROVAL']}">checked</c:if>>
		     </div>
		     <div class="form">
		       <label><input type="hidden" name="__key" value="ORDER_OVERVIEW"><fmt:message key="overview" />:</label>
		       <input type="checkbox" name="ORDER_OVERVIEW" value ="true" <c:if test="${siteConfig['ORDER_OVERVIEW'].value == 'true'}">checked</c:if>>
		     </div>
		     <div class="form">
		       <label><input type="hidden" name="__key" value="DELIVERY_TIME"><fmt:message key="deliveryTime" />:</label>
		       <input type="checkbox" name="DELIVERY_TIME" value ="true" <c:if test="${siteConfig['DELIVERY_TIME'].value == 'true'}">checked</c:if>>
		     </div>
		     <div class="form">
		       <label><input type="hidden" name="__key" value="USER_EXPECTED_DELIVERY_TIME"><fmt:message key="userExpectedDueDate" />:</label>
		       <select name="USER_EXPECTED_DELIVERY_TIME">
				  <option value="false"></option>
				  <option value="true" <c:if test="${'true' == siteConfig['USER_EXPECTED_DELIVERY_TIME'].value}">selected</c:if>>True</option>
				  <option value="true-script" <c:if test="${'true-script' == siteConfig['USER_EXPECTED_DELIVERY_TIME'].value}">selected</c:if>>True-With Custom Script</option>
			   </select>
		     </div>
		     <div class="form">
		       <label><input type="hidden" name="__key" value="REQUESTED_CANCEL_DATE"><fmt:message key="requestedCancelDate" />:</label>
		       <input type="checkbox" name="REQUESTED_CANCEL_DATE" value ="true" <c:if test="${siteConfig['REQUESTED_CANCEL_DATE'].value == 'true'}">checked</c:if>>
		     </div>
		     <div class="form">
		       <label><input type="hidden" name="__gkey" value="gWORLDSHIP">WorldShip:</label>
		       <input type="checkbox" name="gWORLDSHIP" value ="1" <c:if test="${gSiteConfig['gWORLDSHIP']}">checked</c:if>>
		     </div>
		     <div class="form">
		       <label><input type="hidden" name="__key" value="WORLD_SHIP_EMAIL_NOTIFICATION"><fmt:message key="worldshipEmailNotification" />:</label>
		       <input type="checkbox" name="WORLD_SHIP_EMAIL_NOTIFICATION" value ="true" <c:if test="${siteConfig['WORLD_SHIP_EMAIL_NOTIFICATION'].value == 'true'}">checked</c:if>>
		     </div>
		     <div class="form">
		       <label><input type="hidden" name="__key" value="WORLD_SHIP_IMPORT_ORDERS"><fmt:message key="worldshipImportOrder" />:</label>
		       <input type="checkbox" name="WORLD_SHIP_IMPORT_ORDERS" value ="true" <c:if test="${siteConfig['WORLD_SHIP_IMPORT_ORDERS'].value == 'true'}">checked</c:if>>
		     </div>
		     <div class="form">
		       <label><input type="hidden" name="__key" value="ORDER_FULFILMENT"><fmt:message key="orderFulfillment" />:</label>
		       <input type="checkbox" name="ORDER_FULFILMENT" value ="true" <c:if test="${siteConfig['ORDER_FULFILMENT'].value == 'true'}">checked</c:if>>
		     </div>
		     <div class="form">
		       <label><input type="hidden" name="__gkey" value="gFEDEXSHIPMANAAGER">FedEx Shipping Label:</label>
		       <input type="checkbox" name="gFEDEXSHIPMANAAGER" value ="1" <c:if test="${gSiteConfig['gFEDEXSHIPMANAAGER']}">checked</c:if>>
		     </div>
		     <div class="form">
		       <label><input type="hidden" name="__key" value="FEDEX_SHIPPING_LABEL_EMAIL_NOTIFICATION"><fmt:message key="fedExShippingLabelEmailNotification" />:</label>
		       <input type="checkbox" name="FEDEX_SHIPPING_LABEL_EMAIL_NOTIFICATION" value ="true" <c:if test="${siteConfig['FEDEX_SHIPPING_LABEL_EMAIL_NOTIFICATION'].value == 'true'}">checked</c:if>>
		     </div>
		     <div class="form">
		       <label><input type="hidden" name="__key" value="SUB_STATUS"><fmt:message key="subStatus" />:</label>
		       <input type="text" name="SUB_STATUS" value="<c:out value="${siteConfig['SUB_STATUS'].value}"/>" maxlength="5" size="5">
		     </div>
		     <div class="form">
		       <label><input type="hidden" name="__key" value="BATCH_PRINT_INVOICE"><fmt:message key="batchPrint" />:</label>
		       <input type="checkbox" name="BATCH_PRINT_INVOICE" value ="true" <c:if test="${siteConfig['BATCH_PRINT_INVOICE'].value == 'true'}">checked</c:if>>
		     </div>
		     <div class="form">
		       <label><input type="hidden" name="__key" value="BATCH_PRINT_PRODUCT_LABEL"><fmt:message key="batchPrint" /> <fmt:message key="product" /></label>
		       <input type="checkbox" name="BATCH_PRINT_PRODUCT_LABEL" value ="true" <c:if test="${siteConfig['BATCH_PRINT_PRODUCT_LABEL'].value == 'true'}">checked</c:if>>
		     </div>
		     <div class="form">
		       <label><input type="hidden" name="__key" value="ORDER_ADD_VALID_SKU"><fmt:message key="addValidSku" />:</label>
		       <input type="checkbox" name="ORDER_ADD_VALID_SKU" value ="true" <c:if test="${siteConfig['ORDER_ADD_VALID_SKU'].value == 'true'}">checked</c:if>>
		     </div>
		     <div class="title"><fmt:message key="massEmail" /></div>
		     <div class="form">
		       <label><input type="hidden" name="__gkey" value="gMASS_EMAIL"><fmt:message key="massEmail" />:</label>
		       <input type="checkbox" name="gMASS_EMAIL" value ="1" <c:if test="${gSiteConfig['gMASS_EMAIL']}">checked</c:if>>
		     </div>
		     <div class="form">
		       <label><input type="hidden" name="__key" value="CUSTOMER_MASS_EMAIL_SUBSCRIBE_DEFAULT"><fmt:message key="customerMassEmailSubscribeDefault" />:</label>
		       <input type="checkbox" name="CUSTOMER_MASS_EMAIL_SUBSCRIBE_DEFAULT" value ="true" <c:if test="${siteConfig['CUSTOMER_MASS_EMAIL_SUBSCRIBE_DEFAULT'].value == 'true'}">checked</c:if>>
		     </div>
		     <div class="form">
		       <label><input type="hidden" name="__key" value="CUSTOMER_MASS_EMAIL"><fmt:message key="customerMassEmail" />:</label>
		       <input type="checkbox" name="CUSTOMER_MASS_EMAIL" value ="true" <c:if test="${siteConfig['CUSTOMER_MASS_EMAIL'].value == 'true'}">checked</c:if>>
		     </div>
		     <div class="form">
		       <label><input type="hidden" name="__key" value="MASS_EMAIL_JOB_NAME"><fmt:message key="jobNamePrefix" />:</label>
		       <input type="text" name="MASS_EMAIL_JOB_NAME" value="<c:out value="${siteConfig['MASS_EMAIL_JOB_NAME'].value}"/>" maxlength="255" size="25">
		     </div>
		     <div class="form">
		       <label><fmt:message key="point" />:</label>
		       <c:out value="${siteConfig['MASS_EMAIL_POINT'].value}"/>
		     </div>
		     <div class="form">
		       <label><input type="hidden" name="__key" value="MASS_EMAIL_UNIQUEPER_ORDER"><fmt:message key="uniqueEmailPerOrder" />:</label>
		       <input type="checkbox" name="MASS_EMAIL_UNIQUEPER_ORDER" value ="true" <c:if test="${siteConfig['MASS_EMAIL_UNIQUEPER_ORDER'].value == 'true'}">checked</c:if>>
		     </div>
		     <div class="title"><fmt:message key="customer" /> <fmt:message key="options" /></div>
		     <div class="form">
		       <label><input type="hidden" name="__gkey" value="gTAX_EXEMPTION"><fmt:message key="taxExemption" />:</label>
		       <fmt:message key="yes" /> <input type="radio" id="gTax_EXEMPTIONYES" name="gTAX_EXEMPTION" value="1" <c:if test="${gSiteConfig['gTAX_EXEMPTION']}">checked</c:if> >
		       <fmt:message key="no" /> <input type="radio" id="gSALES_EXEMPTIONNO" name="gTAX_EXEMPTION" value="0" <c:if test="${not gSiteConfig['gTAX_EXEMPTION']}">checked</c:if> >
		     </div>    
		     <div class="form">
		       <label><input type="hidden" name="__gkey" value="gCUSTOMER_FIELDS"><fmt:message key="customerFields" />:</label>
			   <select name="gCUSTOMER_FIELDS">
				<c:forTokens items="0,5,10,15,20" delims="," var="value">
		  	    <option value="${value}" <c:if test="${value == gSiteConfig['gCUSTOMER_FIELDS']}">selected</c:if>>${value}</option>
				</c:forTokens>
		 	   </select>
		     </div> 
		     <div class="form">
		       <label><input type="hidden" name="__key" value="CUSTOMER_FIELDS_ON_INVOICE_EXPORT"><fmt:message key="customerFieldsOnInvoiceExport" />:</label>
			    <input type="checkbox" name="CUSTOMER_FIELDS_ON_INVOICE_EXPORT" value ="true" <c:if test="${'true' == siteConfig['CUSTOMER_FIELDS_ON_INVOICE_EXPORT'].value}">checked</c:if>>
		     </div>
		     <div class="form">
		       <label><input type="hidden" name="__gkey" value="gSUB_ACCOUNTS"><fmt:message key="subAccounts" />:</label>
		       <input type="checkbox" name="gSUB_ACCOUNTS" value ="1" <c:if test="${gSiteConfig['gSUB_ACCOUNTS']}">checked</c:if>>
		     </div> 
		     <div class="form">
		       <label><input type="hidden" name="__key" value="SUBACCOUNT_LOGINAS_FRONTEND"><fmt:message key="subAccounts" /> <fmt:message key="loginAsCustomer" />:</label>
		       <input type="checkbox" name="SUBACCOUNT_LOGINAS_FRONTEND" value ="true" <c:if test="${siteConfig['SUBACCOUNT_LOGINAS_FRONTEND'].value == 'true'}">checked</c:if>>
		     </div>
		     <div class="form">
		       <label><input type="hidden" name="__key" value="CUSTOMER_CUSTOM_NOTE_1"><fmt:message key="custom" /> <fmt:message key="note" /> 1:</label>
		       <input type="text" name="CUSTOMER_CUSTOM_NOTE_1" value="<c:out value="${siteConfig['CUSTOMER_CUSTOM_NOTE_1'].value}"/>" maxlength="255" size="50">
		     </div>  
		     <div class="form">
		       <label><input type="hidden" name="__key" value="CUSTOMER_CUSTOM_NOTE_2"><fmt:message key="custom" /> <fmt:message key="note" /> 2:</label>
		       <input type="text" name="CUSTOMER_CUSTOM_NOTE_2" value="<c:out value="${siteConfig['CUSTOMER_CUSTOM_NOTE_2'].value}"/>" maxlength="255" size="50">
		     </div>
		     <div class="form">
		       <label><input type="hidden" name="__key" value="CUSTOMER_CUSTOM_NOTE_3"><fmt:message key="custom" /> <fmt:message key="note" /> 3:</label>
		       <input type="text" name="CUSTOMER_CUSTOM_NOTE_3" value="<c:out value="${siteConfig['CUSTOMER_CUSTOM_NOTE_3'].value}"/>" maxlength="255" size="50">
		     </div>
		     <div class="form">
		       <label><input type="hidden" name="__key" value="CUSTOMER_IP_ADDRESS"><fmt:message key="ipAddress" />:</label>
		       <input type="checkbox" name="CUSTOMER_IP_ADDRESS" value ="true" <c:if test="${siteConfig['CUSTOMER_IP_ADDRESS'].value == 'true'}">checked</c:if>>
		     </div>
		     <div class="form">
		       <label><input type="hidden" name="__key" value="CONFIRM_USERNAME"><fmt:message key="confirmUsernameOnRegistration" />:</label>
		       <input type="checkbox" name="CONFIRM_USERNAME" value ="true" <c:if test="${siteConfig['CONFIRM_USERNAME'].value == 'true'}">checked</c:if>>
		     </div> 
		     <div class="form">
		       <label><input type="hidden" name="__gkey" value="gCUSTOMER_SUPPLIER">Convert To Supplier:</label>
		       <input type="checkbox" name="gCUSTOMER_SUPPLIER" value ="1" <c:if test="${gSiteConfig['gCUSTOMER_SUPPLIER']}">checked</c:if>>
		     </div>
		     <div class="form">
		       <label><input type="hidden" name="__gkey" value="gGROUP_CUSTOMER">Group Customer:</label>
		       <input type="checkbox" name="gGROUP_CUSTOMER" value ="1" <c:if test="${gSiteConfig['gGROUP_CUSTOMER']}">checked</c:if>>
		     </div>
		     <div class="form">
		       <label><input type="hidden" name="__key" value="CURRENT_PASSWORD_CHECK">Current password check:</label>
		       <input type="checkbox" name="CURRENT_PASSWORD_CHECK" value ="true" <c:if test="${siteConfig['CURRENT_PASSWORD_CHECK'].value == 'true'}">checked</c:if>>
		     </div>
		     <div class="form">
		       <label><input type="hidden" name="__key" value="CUSTOMER_PASSWORD_RESET"><fmt:message key="customerPasswordReset" />:</label>
		       <input type="checkbox" name="CUSTOMER_PASSWORD_RESET" value ="true" <c:if test="${siteConfig['CUSTOMER_PASSWORD_RESET'].value == 'true'}">checked</c:if>>
		     </div>
		     <div class="form" >
		       <label><input type="hidden" name="__key" value="CUSTOMER_PASSWORD_LENGTH"><fmt:message key="customerPasswordLength" />:</label>
		       <select name="CUSTOMER_PASSWORD_LENGTH">
				   <c:forEach begin="5" end="12" step="1" var="value">
				    <option value="${value}" <c:if test="${value == siteConfig['CUSTOMER_PASSWORD_LENGTH'].value}">selected</c:if>>${value}</option>
				   </c:forEach>
			     </select>
		     </div> 
		     <div class="form">
		       <label><input type="hidden" name="__key" value="CUSTOMER_PASSWORD_STRONG">Strong Password:</label>
		       <input type="checkbox" name="CUSTOMER_PASSWORD_STRONG" value ="true" <c:if test="${siteConfig['CUSTOMER_PASSWORD_STRONG'].value == 'true'}">checked</c:if>>
		       <div class="helpNote" style="padding-left: 140px;">
		        <p>Force to have at least 1 special character and 1 digit in Customer Password</p>
		       </div> 
		     </div> 
		     <div class="form">
		       <label><input type="hidden" name="__key" value="COOKIE_AGE_KEEP_ME_LOGIN"><fmt:message key="keepMeLoginCookieAge" />:</label>
		       <select name="COOKIE_AGE_KEEP_ME_LOGIN">
				  <option value="-1"></option>
				  <option value="1" <c:if test="${'1' == siteConfig['COOKIE_AGE_KEEP_ME_LOGIN'].value}">selected</c:if>>1 day</option>
				  <option value="7" <c:if test="${'7' == siteConfig['COOKIE_AGE_KEEP_ME_LOGIN'].value}">selected</c:if>>1 week</option>
				  <option value="14" <c:if test="${'14' == siteConfig['COOKIE_AGE_KEEP_ME_LOGIN'].value}">selected</c:if>>2 weeks</option>
				  <option value="30" <c:if test="${'30' == siteConfig['COOKIE_AGE_KEEP_ME_LOGIN'].value}">selected</c:if>>1 month</option>
				  <option value="90" <c:if test="${'90' == siteConfig['COOKIE_AGE_KEEP_ME_LOGIN'].value}">selected</c:if>>3 month</option>
				  <option value="180" <c:if test="${'180' == siteConfig['COOKIE_AGE_KEEP_ME_LOGIN'].value}">selected</c:if>>6 month</option>
			   </select>
		     </div> 
		     <div class="form">
		       <label><input type="hidden" name="__key" value="CUSTOMER_AUTOGENERATE_ACCOUNTNUMBER">Auto Generate Custmoer Account</label>
		       <input type="checkbox" name="CUSTOMER_AUTOGENERATE_ACCOUNTNUMBER" value ="true" <c:if test="${siteConfig['CUSTOMER_AUTOGENERATE_ACCOUNTNUMBER'].value == 'true'}">checked</c:if>> 
		     </div>
		     <%-- UNDER DEVELOPMENT
		     <div class="form">
		       <label><input type="hidden" name="__key" value="CUSTOMER_FACEBOOK_LOGIN">Facebook Login</label>
		       <input type="checkbox" name="CUSTOMER_FACEBOOK_LOGIN" value ="true" <c:if test="${siteConfig['CUSTOMER_FACEBOOK_LOGIN'].value == 'true'}">checked</c:if>> 
		     </div> 
		     --%> 
		     <div class="title">Promotion & Sales Tag</div>
		     <div class="form">
		       <label><input type="hidden" name="__gkey" value="gSALES_PROMOTIONS"><fmt:message key="sales&Promotions" />:</label>
		       <fmt:message key="yes" /> <input type="radio" id="gSALES_PROMOTIONSYES" name="gSALES_PROMOTIONS" value="1" <c:if test="${gSiteConfig['gSALES_PROMOTIONS']}">checked</c:if> onclick="togglePromo(this.id)">
		       <fmt:message key="no" /> <input type="radio" id="gSALES_PROMOTIONSNO" name="gSALES_PROMOTIONS" value="0" <c:if test="${not gSiteConfig['gSALES_PROMOTIONS']}">checked</c:if> onclick="togglePromo(this.id)">
		     </div>    
		     <div class="form" id="salesAndPromo1">
		       <label><input type="hidden" name="__gkey" value="gNUMBER_PROMOS"># <fmt:message key="ofPromotions" />:</label>
		       <select name="gNUMBER_PROMOS">
		        <option value="0">0</option>
		        <option value="1" <c:if test="${value == gSiteConfig['gNUMBER_PROMOS']}">selected</c:if>>1</option>
				<c:forEach begin="10" end="800" step="10" var="value">
				  <option value="${value}" <c:if test="${value == gSiteConfig['gNUMBER_PROMOS']}">selected</c:if>>${value}</option>
				</c:forEach>
				<option value="-1" <c:if test="${-1 == gSiteConfig['gNUMBER_PROMOS']}">selected</c:if>><fmt:message key="unlimited"/></option>
			   </select>
		     </div>    
		     <div class="form">
		       <label><input type="hidden" name="__key" value="PROMO_ON_SHOPPING_CART"><fmt:message key="promoOnShoppingCart"/>:</label>
		       <input type="checkbox" name="PROMO_ON_SHOPPING_CART" value ="true" <c:if test="${siteConfig['PROMO_ON_SHOPPING_CART'].value == 'true'}">checked</c:if>>
		     </div>
		     <div class="form" id="advancedPromotion">
		       <label><input type="hidden" name="__gkey" value="gSALES_PROMOTIONS_ADVANCED">Advanced Promotion:</label>
		       <input type="checkbox" name="gSALES_PROMOTIONS_ADVANCED" value ="1" <c:if test="${gSiteConfig['gSALES_PROMOTIONS_ADVANCED']}">checked</c:if>>
		     </div>
			   <div class="form" id="showEligiblePromos">
		       <label><input type="hidden" name="__gkey" value="gSHOW_ELIGIBLE_PROMOS"><fmt:message key="eligiblePromos" />:</label>
		       <input type="checkbox" name="gSHOW_ELIGIBLE_PROMOS" value ="1" <c:if test="${gSiteConfig['gSHOW_ELIGIBLE_PROMOS']}">checked</c:if>>
		     </div>
			   <div class="form" id="salesAndPromo2">
		       <label><input type="hidden" name="__gkey" value="gNUMBER_SALES_TAG"># <fmt:message key="ofSalesTags" />:</label>
			   <select name="gNUMBER_SALES_TAG">
				  <c:forEach begin="0" end="250" step="5" var="value">
				   <option value="${value}" <c:if test="${value == gSiteConfig['gNUMBER_SALES_TAG']}">selected</c:if>>${value}</option>
				  </c:forEach>
			   </select>
		     </div>    
		     <div class="form">
		       <label><input type="hidden" name="__key" value="SALES_TAG_COUNTDOWN"><fmt:message key="salesTagCountDown" />:</label>
		       <input type="checkbox" name="SALES_TAG_COUNTDOWN" value ="true" <c:if test="${siteConfig['SALES_TAG_COUNTDOWN'].value == 'true'}">checked</c:if>>
		       <div class="helpNote" style="padding-left: 140px;">
		        <p>This Counter shows when SalesTag will be expire and on Complete it refresh the page.</p>
		       </div>
		     </div>
		     <div class="form" id="showDeals">
		       <label><input type="hidden" name="__gkey" value="gDEALS"><fmt:message key="deals" />:</label>
		       <select name="gDEALS">
				 <c:forEach begin="0" end="100" step="10" var="value">
				  <option value="${value}" <c:if test="${value == gSiteConfig['gDEALS']}">selected</c:if>>${value}</option>
				 </c:forEach>
			   </select>
			 </div>
			 <div class="form" id="showRebates">
		       <label><input type="hidden" name="__gkey" value="gMAIL_IN_REBATES"><fmt:message key="mailInRebates" />:</label>
		       <select name="gMAIL_IN_REBATES">
				 <c:forEach begin="0" end="300" step="10" var="value">
				  <option value="${value}" <c:if test="${value == gSiteConfig['gMAIL_IN_REBATES']}">selected</c:if>>${value}</option>
				 </c:forEach>
			   </select>
			 </div>
			 <div class="title">Payment Method</div>
		     <div class="form">
		       <label><input type="hidden" name="__gkey" value="gPAYMENTS"><fmt:message key="payments" />:</label>
		       <input type="checkbox" name="gPAYMENTS" value ="1" <c:if test="${gSiteConfig['gPAYMENTS']}">checked</c:if>>
		     </div>   
		     <div class="form">
		       <label><input type="hidden" name="__gkey" value="gPAYPAL">PayPal:</label>
		       <select name="gPAYPAL">
				<c:forEach begin="0" end="2" var="paypal">
		  	    <option value="${paypal}" <c:if test="${paypal == gSiteConfig['gPAYPAL']}">selected</c:if>><fmt:message key="paypal${paypal}" /></option>
				</c:forEach>
		 	   </select>
		 	   <input type="hidden" name="__key" value="PAYPAL_BUSINESS">
			   <input type="text" name="PAYPAL_BUSINESS" value="<c:out value="${siteConfig['PAYPAL_BUSINESS'].value}"/>" size="30">
		     </div>    
		     <div class="form">
		       <label><input type="hidden" name="__key" value="PAYPAL_URL">PayPal URL:</label>
		       <select name="PAYPAL_URL">
		  	    <option value="https://www.paypal.com/" <c:if test="${'https://www.paypal.com/' == siteConfig['PAYPAL_URL'].value}">selected</c:if>>https://www.paypal.com/</option>
		  	    <option value="https://www.sandbox.paypal.com/us/" <c:if test="${'https://www.sandbox.paypal.com/us/' == siteConfig['PAYPAL_URL'].value}">selected</c:if>>https://www.sandbox.paypal.com/us/</option>
		 	   </select>
		     </div>    
		     <div class="form">
		       <label><input type="hidden" name="__gkey" value="gCREDIT_CARD_PAYMENT">Online Credit Card Billing System:</label>
		       <select name="gCREDIT_CARD_PAYMENT" id="ccPayment" onchange="showAuthTestMode();">
				<option value="">No Online Credit Card Billing System</option>
		  	    <option value="authorizenet" <c:if test="${'authorizenet' == gSiteConfig['gCREDIT_CARD_PAYMENT']}">selected</c:if>>Authorize.Net</option>
		  	    <option value="ebizcharge" <c:if test="${'ebizcharge' == gSiteConfig['gCREDIT_CARD_PAYMENT']}">selected</c:if>>Ebizcharge</option>
		  	    <option value="netcommerce" <c:if test="${'netcommerce' == gSiteConfig['gCREDIT_CARD_PAYMENT']}">selected</c:if>>NetCommerce</option>
		  	    <option value="yourpay" <c:if test="${'yourpay' == gSiteConfig['gCREDIT_CARD_PAYMENT']}">selected</c:if>>YourPay</option>
		  	    <option value="ezic" <c:if test="${'ezic' == gSiteConfig['gCREDIT_CARD_PAYMENT']}">selected</c:if>>Ezic</option>
		  	    <option value="payrover" <c:if test="${'payrover' == gSiteConfig['gCREDIT_CARD_PAYMENT']}">selected</c:if>>PayRover</option>
		  	    <option value="bankaudi" <c:if test="${'bankaudi' == gSiteConfig['gCREDIT_CARD_PAYMENT']}">selected</c:if>>BankAudi</option>
				<option value="paymentech" <c:if test="${'paymentech' == gSiteConfig['gCREDIT_CARD_PAYMENT']}">selected</c:if>>Paymentech</option>
				<option value="paypalpro" <c:if test="${'paypalpro' == gSiteConfig['gCREDIT_CARD_PAYMENT']}">selected</c:if>>PayPalPro</option>
				<option value="l19" <c:if test="${'l19' == gSiteConfig['gCREDIT_CARD_PAYMENT']}">selected</c:if>>L19</option>
		 	   </select>
		     </div>
		     <div class="form" id="authTestMode">
		       <label><input type="hidden" name="__key" value="AUTHORIZE_TEST_MODE">Authorize.net (TEST MODE)</label>
		       <input type="checkbox" name="AUTHORIZE_TEST_MODE" value ="true" <c:if test="${siteConfig['AUTHORIZE_TEST_MODE'].value == 'true'}">checked</c:if>>
		     </div>		     
		     <div class="form" id="pCIfriendlyeBiz">
		       <label><input type="hidden" name="__key" value="PCI_FRIENDLY_EBIZCHARGE">PCI friendly eBizCharge</label>
		       <input type="checkbox" name="PCI_FRIENDLY_EBIZCHARGE" value ="true" <c:if test="${siteConfig['PCI_FRIENDLY_EBIZCHARGE'].value == 'true'}">checked</c:if>>
		     </div>   
		     <div class="form" id="l19TestMode">
		       <label><input type="hidden" name="__key" value="L19_TEST_MODE">L19 (TEST MODE)</label>
		       <input type="checkbox" name="L19_TEST_MODE" value ="true" <c:if test="${siteConfig['L19_TEST_MODE'].value == 'true'}">checked</c:if>>
		     </div>
			 <c:if test="${gSiteConfig['gCREDIT_CARD_PAYMENT'] == 'ebizcharge' or gSiteConfig['gCREDIT_CARD_PAYMENT'] == 'ezic' or gSiteConfig['gCREDIT_CARD_PAYMENT'] == 'authorizenet' or gSiteConfig['gCREDIT_CARD_PAYMENT'] == 'l19' or gSiteConfig['gCREDIT_CARD_PAYMENT'] == 'payrover' or gSiteConfig['gCREDIT_CARD_PAYMENT'] == 'paymentech' or gSiteConfig['gCREDIT_CARD_PAYMENT'] == 'paypalpro'}">
		     <div class="form">
		       <label><input type="hidden" name="__key" value="CREDIT_AUTO_CHARGE"><fmt:message key="chargeInstantly"/>:</label>
		       <select name="CREDIT_AUTO_CHARGE">
				  <option value=""><fmt:message key="no"/></option>
				  <option value="auth" <c:if test="${'auth' == siteConfig['CREDIT_AUTO_CHARGE'].value}">selected</c:if>><fmt:message key="authorizeCard"/></option>
				  <option value="charge" <c:if test="${'charge' == siteConfig['CREDIT_AUTO_CHARGE'].value}">selected</c:if>><fmt:message key="chargeCard"/></option>
			   </select>	
		     </div> 
		     </c:if>		 	   
		     <c:if test="${siteConfig['CREDIT_AUTO_CHARGE'].value != ''}">
		     <div class="form">
		       <label><input type="hidden" name="__key" value="CC_FAILURE_URL">Credit Card Failure URL:</label>
		       <input type="text" name="CC_FAILURE_URL" value="<c:out value="${siteConfig['CC_FAILURE_URL'].value}"/>" maxlength="255" size="50">
		     </div>		     		 	   
		     </c:if>
		     <div class="form">
		       <label><input type="hidden" name="__key" value="PAYPALPRO_ENVIRONMENT">PayPalPro:</label>
		       <select name="PAYPALPRO_ENVIRONMENT">
		        <option value=""><fmt:message key="no"/></option>
		  	    <option value="live" <c:if test="${'live' == siteConfig['PAYPALPRO_ENVIRONMENT'].value}">selected</c:if>>PRODUCTION</option>
		  	    <option value="sandbox" <c:if test="${'sandbox' == siteConfig['PAYPALPRO_ENVIRONMENT'].value}">selected</c:if>>TEST</option>
		 	   </select>
		     </div>
		     <div class="form">
		       <label><input type="hidden" name="__key" value="EBILLME_URL">eBillme:</label>
		       <select name="EBILLME_URL">
		        <option value=""><fmt:message key="no"/></option>
		  	    <option value="https://my.eBillme.com/Auth/" <c:if test="${'https://my.eBillme.com/Auth/' == siteConfig['EBILLME_URL'].value}">selected</c:if>>PRODUCTION</option>
		  	    <option value="https://test.modasolutions.com/Auth/" <c:if test="${'https://test.modasolutions.com/Auth/' == siteConfig['EBILLME_URL'].value}">selected</c:if>>TEST</option>
		 	   </select>
		     </div>
		     <div class="form">
		       <label><input type="hidden" name="__key" value="GOOGLE_CHECKOUT_URL"><fmt:message key="googleCheckOut"/>:</label>
		       <select name="GOOGLE_CHECKOUT_URL">
		  	    <option value="" <c:if test="${'' == siteConfig['GOOGLE_CHECKOUT_URL'].value}">selected</c:if>>NO</option>
		  	    <option value="https://sandbox.google.com/checkout/api/checkout/v2/checkout/Merchant/" <c:if test="${'https://sandbox.google.com/checkout/api/checkout/v2/checkout/Merchant/' == siteConfig['GOOGLE_CHECKOUT_URL'].value}">selected</c:if>>TEST</option>
		  	    <option value="https://checkout.google.com/api/checkout/v2/checkout/Merchant/" <c:if test="${'https://checkout.google.com/api/checkout/v2/checkout/Merchant/' == siteConfig['GOOGLE_CHECKOUT_URL'].value}">selected</c:if>>PRODUCTION</option>
		 	   </select>
		    </div>  		     
		     <div class="form">
		       <label><input type="hidden" name="__key" value="AMAZON_URL">Amazon Check Out:</label>
		       <select name="AMAZON_URL">
		        <option value=""><fmt:message key="no"/></option>
		  	    <option value="https://payments.amazon.com/" <c:if test="${'https://payments.amazon.com/' == siteConfig['AMAZON_URL'].value}">selected</c:if>>Live</option>
		  	    <option value="https://payments-sandbox.amazon.com/" <c:if test="${'https://payments-sandbox.amazon.com/' == siteConfig['AMAZON_URL'].value}">selected</c:if>>Sandbox</option>
		 	   </select>
		     </div> 
		     <div class="form">
		       <label><input type="hidden" name="__key" value="CENTINEL_URL"><fmt:message key="cardinalCentinel" />:</label>
		       <select name="CENTINEL_URL">
		        <option value=""><fmt:message key="no"/></option>
		  	    <option value="https://centinel.cardinalcommerce.com/maps/txns.asp" <c:if test="${'https://centinel.cardinalcommerce.com/maps/txns.asp' == siteConfig['CENTINEL_URL'].value}">selected</c:if>>LIVE</option>
		  	    <option value="https://centineltest.cardinalcommerce.com/maps/txns.asp" <c:if test="${'https://centineltest.cardinalcommerce.com/maps/txns.asp' == siteConfig['CENTINEL_URL'].value}">selected</c:if>>TEST</option>
		 	   </select>
		     </div> 
		     <div class="form">
		       <label><input type="hidden" name="__key" value="GEMONEY_DOMAIN">GE Money:</label>
		       <select name="GEMONEY_DOMAIN">
		        <option value=""><fmt:message key="no"/></option>
		  	    <option value="www.secureb2c.com" <c:if test="${'www.secureb2c.com' == siteConfig['GEMONEY_DOMAIN'].value}">selected</c:if>>LIVE</option>
		  	    <option value="twww.secureb2c.com" <c:if test="${'twww.secureb2c.com' == siteConfig['GEMONEY_DOMAIN'].value}">selected</c:if>>TEST</option>
		 	   </select>
		     </div> 
		     <div class="form">
		       <label><input type="hidden" name="__key" value="INTERNATIONAL_CHECKOUT">International Checkout:</label>
		       <input type="text" name="INTERNATIONAL_CHECKOUT" value="<c:out value="${siteConfig['INTERNATIONAL_CHECKOUT'].value}"/>" size="30">
		     </div> 
		     <div class="form">
		       <label><input type="hidden" name="__key" value="INTERNATIONAL_CHECKOUT_P_VALUE">International Checkout P Value:</label>
		       <input type="text" name="INTERNATIONAL_CHECKOUT_P_VALUE" value="<c:out value="${siteConfig['INTERNATIONAL_CHECKOUT_P_VALUE'].value}"/>" size="30">
		     </div>
		     <div class="form">
		       <label><input type="hidden" name="__key" value="CC_BILLING_ADDRESS">Credit Card Billing Address: </label>
		       <input type="checkbox" name="CC_BILLING_ADDRESS" value ="true" <c:if test="${siteConfig['CC_BILLING_ADDRESS'].value == 'true'}">checked</c:if>>
		       <div class="helpNote" style="padding-left: 140px;">
				    <p>This enables a new text area on checkout near Credit Card to enter different credit card billing address.</p>
			     </div>
		     </div>
		    <div class="title">Buy Safe</div>     
		    <div class="form">
		       <label><input type="hidden" name="__key" value="BUY_SAFE_URL"><fmt:message key="buySafe"/>:</label>
		       <select name="BUY_SAFE_URL">
		  	    <option value="" <c:if test="${'' == siteConfig['BUY_SAFE_URL'].value}">selected</c:if>>NO</option>
		  	    <option value="https://sbws.buysafe.com/BuysafeWS/CheckoutAPI.dll" <c:if test="${'https://sbws.buysafe.com/BuysafeWS/CheckoutAPI.dll' == siteConfig['BUY_SAFE_URL'].value}">selected</c:if>>TEST</option>
		  	    <option value="https://api.buysafe.com/BuysafeWS/CheckoutAPI.dll" <c:if test="${'https://api.buysafe.com/BuysafeWS/CheckoutAPI.dll' == siteConfig['BUY_SAFE_URL'].value}">selected</c:if>>PRODUCTION</option>
		 	   </select>
		    </div>  		     
		    <div class="title">Affiliate</div>
		     <div class="form">
		       <label><input type="hidden" name="__gkey" value="gAFFILIATE"><fmt:message key="affiliate" />:</label>
		       <select name="gAFFILIATE" id="gAFFILIATE" onchange="toggleCommissionTable(this.id)">
				<c:forEach begin="0" end="2" var="affiliate">
		  	    <option value="${affiliate}" <c:if test="${affiliate == gSiteConfig['gAFFILIATE']}">selected</c:if>><fmt:message key="level${affiliate}" /></option>
				</c:forEach>
		 	   </select>
		     </div>    
		     <div class="form" id="productAffiliateCommision1">
		       <label><input type="hidden" name="__key" value="NUMBER_PRODUCT_COMMISSION_TABLE"># <fmt:message key="ofProductCommissionTable" />:</label>
		       <select name="NUMBER_PRODUCT_COMMISSION_TABLE">
			     <c:forEach begin="0" end="10" step="1" var="value">
				  <option value="${value}" <c:if test="${value == siteConfig['NUMBER_PRODUCT_COMMISSION_TABLE'].value}">selected</c:if>>${value}</option>
				 </c:forEach>
			   </select>
		     </div>    
		     <div class="form" id="numAffiliate">
		       <label><input type="hidden" name="__gkey" value="gNUMBER_AFFILIATE"># <fmt:message key="of" /> <fmt:message key="affiliate" />:</label>
		       <select name="gNUMBER_AFFILIATE">
			     <c:forEach begin="10" end="500" step="10" var="value">
				  <option value="${value}" <c:if test="${value == gSiteConfig['gNUMBER_AFFILIATE']}">selected</c:if>>${value}</option>
				 </c:forEach>
			   </select>
		     </div>    
		     <div class="form" id="editCommission">
		       <label><input type="hidden" name="__key" value="EDIT_COMMISSION"><fmt:message key="edit"/>&nbsp;<fmt:message key="commission" />:</label>
		       <input type="checkbox" name="EDIT_COMMISSION" value ="true" <c:if test="${siteConfig['EDIT_COMMISSION'].value == 'true'}">checked</c:if>>
		     </div>   
		     <div class="title"><fmt:message key="virtualPayments"/></div>
		     <div class="form">
		       <label><input type="hidden" name="__gkey" value="gVIRTUAL_BANK_ACCOUNT"><fmt:message key="virtualBankPayments" />:</label>
		       <input type="checkbox" name="gVIRTUAL_BANK_ACCOUNT" value ="1" <c:if test="${gSiteConfig['gVIRTUAL_BANK_ACCOUNT']!=''}">checked</c:if>>
		     </div>
		     <div class="title"><fmt:message key="dataFeed" /></div>
		     <input type="hidden" name="__gkeyArray" value="gDATA_FEED">
		     <c:forTokens items="blueCherry,priceGrabber,mShopper,sortPrice,newEggMall,googleBase,singleFeed,equiteam" delims="," var="feed">
		     <div class="form">
		       <label><fmt:message key="${feed}"/>:</label>
		       <input type="checkbox" name="gDATA_FEED" value ="${feed}" <c:if test="${fn:contains(gSiteConfig['gDATA_FEED'],feed)}">checked</c:if>>
		     </div>
		     </c:forTokens> 
		     <div class="form">
		       <label><input type="hidden" name="__gkey" value="gWEBJAGUAR_DATA_FEED"><fmt:message key="webjaguar" /> <fmt:message key="dataFeed" /> :</label>
		       <select name="gWEBJAGUAR_DATA_FEED">
		  	   <c:forEach begin="0" end="10" step="1" var="element">
		         <option value="${element}" <c:if test="${element == gSiteConfig['gWEBJAGUAR_DATA_FEED']}">selected</c:if>>${element}</option>
		  	   </c:forEach>
		       </select>
		     </div>  	     
		     <c:if test="${gSiteConfig['gWEBJAGUAR_DATA_FEED'] > 0}">
		     <div class="form">
		       <label><input type="hidden" name="__key" value="DATA_FEED_MEMBER_TYPE">Data Feed Member Type:</label>
		       <select name="DATA_FEED_MEMBER_TYPE">
		  	    <option value="">NONE</option>
		  	    <option value="PUBLISHER" <c:if test="${'PUBLISHER' == siteConfig['DATA_FEED_MEMBER_TYPE'].value}">selected</c:if>>PUBLISHER</option>
		  	    <option value="SUBSCRIBER" <c:if test="${'SUBSCRIBER' == siteConfig['DATA_FEED_MEMBER_TYPE'].value}">selected</c:if>>SUBSCRIBER</option>
		  	   </select>
		     </div>      
		     </c:if>
		     <div class="form">
		       <label><input type="hidden" name="__key" value="EXCLUDE_FEEDTYPE_FROM_PRODUCT_EXPORT">Exclude From Export (Feed Type):</label>
			   <input type="text" name="EXCLUDE_FEEDTYPE_FROM_PRODUCT_EXPORT" value="<c:out value="${siteConfig['EXCLUDE_FEEDTYPE_FROM_PRODUCT_EXPORT'].value}"/>" size="50">	
		     </div>
		    <div class="title"><fmt:message key="cdsAndDvds" /></div>
		     <div class="form">
		       <label><input type="hidden" name="__key" value="CDS_AND_DVDS">Cds And Dvds :</label>
		       <input type="text" name="CDS_AND_DVDS" value="<c:out value="${siteConfig['CDS_AND_DVDS'].value}"/>" size="50">
		     </div>
		    <div class="title">i18n</div>
		    <div class="form">
		       <label><input type="hidden" name="__gkey" value="gI18N"><fmt:message key="language" />:</label>
		       <input type="checkbox" name="gI18N" value ="1" <c:if test="${gSiteConfig['gI18N']!=''}">checked</c:if>>
		     </div>  	     
		     <div class="title"><fmt:message key="budget" /></div>
		     <div class="form">
		       <label><input type="hidden" name="__gkey" value="gBUDGET_BRAND"><fmt:message key="budgetByBrands" />:</label>
		       <input type="checkbox" name="gBUDGET_BRAND" value ="1" <c:if test="${gSiteConfig['gBUDGET_BRAND']}">checked</c:if>>
		     </div>  
		     <div class="form">
		       <label><input type="hidden" name="__gkey" value="gBUDGET_PRODUCT"><fmt:message key="budgetByProducts" />:</label>
		       <input type="checkbox" name="gBUDGET_PRODUCT" value ="1" <c:if test="${gSiteConfig['gBUDGET_PRODUCT']}">checked</c:if>>
		     </div> 
		     <div class="form">
		       <label><input type="hidden" name="__gkey" value="gBUDGET"><fmt:message key="budget" />:</label>
		       <input type="checkbox" name="gBUDGET" value ="1" <c:if test="${gSiteConfig['gBUDGET']}">checked</c:if>>
		     </div>
		     <c:if test="${gSiteConfig['gBUDGET']}">
		     <div class="form">
		       <label><input type="hidden" name="__key" value="BUDGET_ADD_PARTNER"><fmt:message key="budget" /> <fmt:message key="partner" />:</label>
		       <input type="checkbox" name="BUDGET_ADD_PARTNER" value ="true" <c:if test="${siteConfig['BUDGET_ADD_PARTNER'].value == 'true'}">checked</c:if>>
		     </div>
		     </c:if>
		     <div class="title"><fmt:message key="categoryOptions" /></div>
		     <div class="form">
		       <label><input type="hidden" name="__key" value="CATEGORY_INTERSECTION"><fmt:message key="categoryIntersection" />:</label>
		       <input type="checkbox" name="CATEGORY_INTERSECTION" value ="true" <c:if test="${siteConfig['CATEGORY_INTERSECTION'].value == 'true'}">checked</c:if>>
		     </div>
		     <div class="form">
		       <label><input type="hidden" name="__key" value="PRODUCT_SORT_BY_FRONTEND">FrontEnd Product Sort By:</label>
		       <input type="checkbox" name="PRODUCT_SORT_BY_FRONTEND" value ="true" <c:if test="${siteConfig['PRODUCT_SORT_BY_FRONTEND'].value == 'true'}">checked</c:if>>
		     </div>
		     <div class="form">
		       <label><input type="hidden" name="__key" value="PRODUCT_QUICK_MODE_2"><fmt:message key="quickMode2" />:</label>
		       <input type="checkbox" name="PRODUCT_QUICK_MODE_2" value ="true" <c:if test="${siteConfig['PRODUCT_QUICK_MODE_2'].value == 'true'}">checked</c:if>>
		       <div class="helpNote" style="padding-left: 140px;">
			     <p>On this mode, a checkbox appear to add to Cart and the Qty would be 1.</p>
			   </div>
		     </div>
		     <div class="form">
		       <label><input type="hidden" name="__key" value="PRODUCT_QUICK_MODE_3"><fmt:message key="quickMode3" />:</label>
		       <input type="checkbox" name="PRODUCT_QUICK_MODE_3" value ="true" <c:if test="${siteConfig['PRODUCT_QUICK_MODE_3'].value == 'true'}">checked</c:if>>
		     </div>
		     <div class="form">
		       <label><input type="hidden" name="__key" value="PRODUCT_PAGE_SIZE">Page Size:</label>
		       <input type="text" name="PRODUCT_PAGE_SIZE" value="<c:out value="${siteConfig['PRODUCT_PAGE_SIZE'].value}"/>" size="25">
		     </div>
		     <div class="form">
		       <label><input type="hidden" name="__key" value="CATEGORY_HTML_TABLE">Remove table around Category Html:</label>
		       <input type="checkbox" name="CATEGORY_HTML_TABLE" value ="true" <c:if test="${siteConfig['CATEGORY_HTML_TABLE'].value == 'true'}">checked</c:if>>
		        <div class="helpNote" style="padding-left: 140px;">
			     <p>This Flag removes the Table around the Category Html.</p>
			   </div>
		     </div>
		     <div class="title"><fmt:message key="productOptions" /></div>
		     <div class="form">
		       <label><input type="hidden" name="__gkey" value="gAUTO_IMPORT"><fmt:message key="autoImport" /> (needs scheduler):</label>
		       <input type="checkbox" name="gAUTO_IMPORT" value ="1" <c:if test="${gSiteConfig['gAUTO_IMPORT']}">checked</c:if>>
		     </div>		     
		     <div class="form">
		       <label>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<input type="hidden" name="__key" value="AUTO_IMPORT_SERVER">FTP <fmt:message key="server" />:</label>
		       <input type="text" name="AUTO_IMPORT_SERVER" value="<c:out value="${siteConfig['AUTO_IMPORT_SERVER'].value}"/>" maxlength="255" size="25">
		     </div>  
		     <div class="form">
		       <label>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<input type="hidden" name="__key" value="AUTO_IMPORT_FILENAME">Excel <fmt:message key="fileName" />:</label>
		       <input type="text" name="AUTO_IMPORT_FILENAME" value="<c:out value="${siteConfig['AUTO_IMPORT_FILENAME'].value}"/>" maxlength="255" size="25">
		     </div>  
		     <div class="form">
		       <label>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<input type="hidden" name="__key" value="AUTO_IMPORT_DELETE">Delete File at Successful Import:</label>
		       <input type="checkbox" name="AUTO_IMPORT_DELETE" value ="true" <c:if test="${siteConfig['AUTO_IMPORT_DELETE'].value == 'true'}">checked</c:if>>
		     </div>  
		     <div class="form">
		       <label>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<input type="hidden" name="__key" value="AUTO_IMPORT_USERNAME">FTP <fmt:message key="username" />:</label>
		       <input type="text" name="AUTO_IMPORT_USERNAME" value="<c:out value="${siteConfig['AUTO_IMPORT_USERNAME'].value}"/>" maxlength="255" size="25">
		     </div>  
		     <div class="form">
		       <label>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<input type="hidden" name="__key" value="AUTO_IMPORT_PASSWORD">FTP <fmt:message key="password" />:</label>
		       <input type="text" name="AUTO_IMPORT_PASSWORD" value="<c:out value="${siteConfig['AUTO_IMPORT_PASSWORD'].value}"/>" maxlength="255" size="25">
		     </div>  
		     <div class="form">
		       <label>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<input type="hidden" name="__key" value="AUTO_IMPORT_EMAIL"><fmt:message key="email" />:</label>
		       <input type="text" name="AUTO_IMPORT_EMAIL" value="<c:out value="${siteConfig['AUTO_IMPORT_EMAIL'].value}"/>" maxlength="255" size="25">
		     </div>
		     <div class="form">
		       <label><input type="hidden" name="__key" value="PRODUCT_IMPORT_FILENAME">Product Import Location (manual FTP):</label>
		       <input type="text" name="PRODUCT_IMPORT_FILENAME" value="<c:out value="${siteConfig['PRODUCT_IMPORT_FILENAME'].value}"/>" maxlength="255" size="25">
		     </div>	     
		     <div class="form">
		       <label><input type="hidden" name="__gkey" value="gBOX"><fmt:message key="box" />:</label>
		       <input type="checkbox" name="gBOX" value ="1" <c:if test="${gSiteConfig['gBOX']}">checked</c:if>>
		     </div>  
		     <div class="form">
		       <label><input type="hidden" name="__gkey" value="gPRICE_CASEPACK">Price Case Pack:</label>
		       <input type="checkbox" name="gPRICE_CASEPACK" value ="1" <c:if test="${gSiteConfig['gPRICE_CASEPACK']}">checked</c:if>>
		     </div>     
		     <div class="form">
		       <label><input type="hidden" name="__gkey" value="gCASE_CONTENT"><fmt:message key="caseContent" /></label>
		    	<fmt:message key="yes" /> <input type="radio"  name="gCASE_CONTENT" value="1" <c:if test="${gSiteConfig['gCASE_CONTENT']}">checked</c:if> >
		    	<fmt:message key="no" /> <input type="radio"  name="gCASE_CONTENT" value="0" <c:if test="${not gSiteConfig['gCASE_CONTENT']}">checked</c:if> >
		         <input type="hidden" name="__key" value="CASE_CONTENT_LAYOUT">
		         <select name="CASE_CONTENT_LAYOUT">
		  	      <option value=""><fmt:message key="default" /></option>
		  	      <option value="1" <c:if test="${siteConfig['CASE_CONTENT_LAYOUT'].value == '1'}">selected</c:if>>Show Multiplied Price</option>
		  	      <option value="2" <c:if test="${siteConfig['CASE_CONTENT_LAYOUT'].value == '2'}">selected</c:if>>Show Case Pack Price</option>
		  	     </select>
		     </div>
		     <div class="form">
		       <label><input type="hidden" name="__gkey" value="gMINIMUM_INCREMENTAL_QTY"><fmt:message key="minimumIncrementalQty" />:</label>
		       <input type="checkbox" name="gMINIMUM_INCREMENTAL_QTY" value ="1" <c:if test="${gSiteConfig['gMINIMUM_INCREMENTAL_QTY']}">checked</c:if>>
		     </div> 
		     <div class="form">
		       <label><input type="hidden" name="__gkey" value="gVARIABLE_PRICE"><fmt:message key="variablePrice" />:</label>
		       <input type="checkbox" name="gVARIABLE_PRICE" value ="1" <c:if test="${gSiteConfig['gVARIABLE_PRICE']}">checked</c:if>>
		     </div>             
		     <div class="form">
		       <label><input type="hidden" name="__gkey" value="gSHARED_CATEGORIES">Shared Categories:</label>
		       <select name="gSHARED_CATEGORIES">
		  	    <option value=""><fmt:message key="none" /></option>
		  	    <option value="http://itwnetwork.wjserver.com/" <c:if test="${'http://itwnetwork.wjserver.com/' == gSiteConfig['gSHARED_CATEGORIES']}">selected</c:if>>ITW Network</option>
		  	    <option value="http://magnumdatanetwork.com/" <c:if test="${'http://magnumdatanetwork.com/' == gSiteConfig['gSHARED_CATEGORIES']}">selected</c:if>>Magnumdata Network</option>
		        <option value="http://binstylenetwork.com/" <c:if test="${'http://binstylenetwork.com/' == gSiteConfig['gSHARED_CATEGORIES']}">selected</c:if>>Binstyle Network</option>
		        <option value="http://engagingworlds.ew-server.com/" <c:if test="${'http://engagingworlds.ew-server.com/' == gSiteConfig['gSHARED_CATEGORIES']}">selected</c:if>>Engaging Worlds</option>
				<option value="http://www.fragranceretailer.com/" <c:if test="${'http://www.fragranceretailer.com/' == gSiteConfig['gSHARED_CATEGORIES']}">selected</c:if>>Fragrance Retailer</option>
				<option value="http://www.jabsol.com/" <c:if test="${'http://www.jabsol.com/' == gSiteConfig['gSHARED_CATEGORIES']}">selected</c:if>>Jabsol</option>
				<option value="http://concord.wjserver10.com/" <c:if test="${'http://concord.wjserver10.com/' == gSiteConfig['gSHARED_CATEGORIES']}">selected</c:if>>Concord</option>
				<option value="http://www.rugtycoon.com/" <c:if test="${'http://www.rugtycoon.com/' == gSiteConfig['gSHARED_CATEGORIES']}">selected</c:if>>Rugtycoon</option>
				<option value="http://www.binstyle.com/" <c:if test="${'http://www.binstyle.com/' == gSiteConfig['gSHARED_CATEGORIES']}">selected</c:if>>Binstyle</option>
				<option value="http://www.chocolateoctopus.com/" <c:if test="${'http://www.chocolateoctopus.com/' == gSiteConfig['gSHARED_CATEGORIES']}">selected</c:if>>Chocolateoctopus</option>
				<option value="http://www.charledswalker.com/" <c:if test="${'http://www.charledswalker.com/' == gSiteConfig['gSHARED_CATEGORIES']}">selected</c:if>>charledswalker</option>
				<option value="http://www.webjaguardemo.com/" <c:if test="${'http://www.webjaguardemo.com/' == gSiteConfig['gSHARED_CATEGORIES']}">selected</c:if>>webjaguardemo.com</option>
				<option value="http://p4i.wjserver170.com/" <c:if test="${'http://p4i.wjserver170.com/' == gSiteConfig['gSHARED_CATEGORIES']}">selected</c:if>>p4i.com</option>
		 	   </select>
		 	   <input type="hidden" name="__key" value="SHARED_CATEGORIES_POSITION">
		       <select name="SHARED_CATEGORIES_POSITION">
		  	    <option value="0">&nbsp;</option>
		  	    <option value="1" <c:if test="${siteConfig['SHARED_CATEGORIES_POSITION'].value == '1'}">selected</c:if>><fmt:message key="LeftBarTop" /></option>
		  	    <option value="2" <c:if test="${siteConfig['SHARED_CATEGORIES_POSITION'].value == '2'}">selected</c:if>><fmt:message key="LeftBarBottom" /></option>
		  	   </select> 
		 	   <input type="hidden" name="__key" value="SHARED_CATEGORIES_LAYOUT">
		       <select name="SHARED_CATEGORIES_LAYOUT">
		  	    <option value="1"><fmt:message key="layout" /> 1</option>
		  	    <option value="2" <c:if test="${siteConfig['SHARED_CATEGORIES_LAYOUT'].value == '2'}">selected</c:if>><fmt:message key="layout" /> 2</option>
		  	   </select> 
		     </div>      
		     <div class="form">
		       <label><input type="hidden" name="__gkey" value="gRECOMMENDED_LIST"><fmt:message key="recommendedList" />:</label>
		       <fmt:message key="yes" /> <input type="radio" name="gRECOMMENDED_LIST" value="1" <c:if test="${gSiteConfig['gRECOMMENDED_LIST']}">checked</c:if> >
		       <fmt:message key="no" /> <input type="radio" name="gRECOMMENDED_LIST" value="0" <c:if test="${not gSiteConfig['gRECOMMENDED_LIST']}">checked</c:if> >
		     </div>    
		     <div class="form">
		       <label><input type="hidden" name="__gkey" value="gALSO_CONSIDER"><fmt:message key="alsoConsider" />:</label>
		    	<fmt:message key="yes" /> <input type="radio" name="gALSO_CONSIDER" value="1" <c:if test="${gSiteConfig['gALSO_CONSIDER']}">checked</c:if> >
		    	<fmt:message key="no" /> <input type="radio" name="gALSO_CONSIDER" value="0" <c:if test="${not gSiteConfig['gALSO_CONSIDER']}">checked</c:if> >
		     </div>    
		     <div class="form">
		       <label><input type="hidden" name="__key" value="CUSTOM_TEXT_PRODUCT"><fmt:message key="customText" />:</label>
		    	<fmt:message key="yes" /> <input type="radio" name="CUSTOM_TEXT_PRODUCT" value="true" <c:if test="${siteConfig['CUSTOM_TEXT_PRODUCT'].value == 'true'}">checked</c:if>>
		    	<fmt:message key="no" /> <input type="radio" name="CUSTOM_TEXT_PRODUCT" value="false" <c:if test="${siteConfig['CUSTOM_TEXT_PRODUCT'].value == 'false'}">checked</c:if>>
		     </div> 
		     <div class="form">
		       <label><input type="hidden" name="__gkey" value="gPRICE_TIERS"><fmt:message key="productPriceTiers" />:</label>
		       <select name="gPRICE_TIERS">
				<c:forEach begin="1" end="10" var="priceTier">
		  	    <option value="${priceTier}" <c:if test="${priceTier == gSiteConfig['gPRICE_TIERS']}">selected</c:if>>${priceTier}</option>
				</c:forEach>
		 	   </select>
		     </div>    
		     <div class="form">
		       <label><input type="hidden" name="__gkey" value="gPRICE_TABLE"><fmt:message key="productPriceTable" />:</label>
		       <select name="gPRICE_TABLE">
				<c:forEach begin="0" end="10" var="priceTable">
		  	    <option value="${priceTable}" <c:if test="${priceTable == gSiteConfig['gPRICE_TABLE']}">selected</c:if>>${priceTable}</option>
				</c:forEach>
		 	   </select>
		     </div>
		     <div class="form">
	       		<label><input type="hidden" name="__key" value="COST_TIERS"><fmt:message key="productCostTiers" />:</label>
			    <input type="checkbox" name="COST_TIERS" value ="true" <c:if test="${'true' == siteConfig['COST_TIERS'].value}">checked</c:if>>
		     </div>  
		     <div class="form">
	       		<label><input type="hidden" name="__key" value="SALES_TAG_ON_PRICE_TABLE"><fmt:message key="salesTagOnPriceTable" />:</label>
			    <input type="checkbox" name="SALES_TAG_ON_PRICE_TABLE" value ="true" <c:if test="${'true' == siteConfig['SALES_TAG_ON_PRICE_TABLE'].value}">checked</c:if>>
		     </div>   
		     <div class="form">
	       		<label><input type="hidden" name="__key" value="PRODUCT_QUOTE"><fmt:message key="productQuote" />:</label>
			    <input type="checkbox" name="PRODUCT_QUOTE" value ="true" <c:if test="${'true' == siteConfig['PRODUCT_QUOTE'].value}">checked</c:if>>
		     </div>
		     <div class="form">
		       <label><input type="hidden" name="__gkey" value="gPROTECTED">Protected Feature:</label>
		       <select name="gPROTECTED">
		       <c:forEach begin="0" end="60" step="10" var="value">
		  	    <option value="${value}" <c:if test="${value == gSiteConfig['gPROTECTED']}">selected</c:if>>${value}</option>
				</c:forEach>
			   </select>	
		     </div>    
		     <div class="form">
		       <label><input type="hidden" name="__gkey" value="gPRODUCT_FIELDS"><fmt:message key="productFields" />:</label>
			   <select name="gPRODUCT_FIELDS">
				<c:forEach begin="10" end="100" step="5" var="value">
		  	    <option value="${value}" <c:if test="${value == gSiteConfig['gPRODUCT_FIELDS']}">selected</c:if>>${value}</option>
				</c:forEach>
		 	   </select>
		     </div>
		     <div class="form">
		       <label><input type="hidden" name="__key" value="PRODUCT_FIELDS_ON_INVOICE_EXPORT"><fmt:message key="productFieldsOnInvoiceExport" />:</label>
			    <input type="checkbox" name="PRODUCT_FIELDS_ON_INVOICE_EXPORT" value ="true" <c:if test="${'true' == siteConfig['PRODUCT_FIELDS_ON_INVOICE_EXPORT'].value}">checked</c:if>>
		     </div>
		     <div class="form">
		       <label><input type="hidden" name="__key" value="CREDIT_CARD_ON_INVOICE_EXPORT"><fmt:message key="creditCardOnInvoiceExport" />:</label>
			    <input type="checkbox" name="CREDIT_CARD_ON_INVOICE_EXPORT" value ="true" <c:if test="${'true' == siteConfig['CREDIT_CARD_ON_INVOICE_EXPORT'].value}">checked</c:if>>
		     </div>
		     <div class="form">
		       <label><input type="hidden" name="__gkey" value="gPRODUCT_IMAGES"># of Product Images:</label>
		 	   <select name="gPRODUCT_IMAGES">
				<c:forEach begin="5" end="20" step="5" var="value">
		  	    <option value="${value}" <c:if test="${value == gSiteConfig['gPRODUCT_IMAGES']}">selected</c:if>>${value}</option>
				</c:forEach>
		 	   </select>
		     </div> 
		     <div class="form">
		       <label><input type="hidden" name="__gkey" value="gCOMPARISON"><fmt:message key="comparison" />:</label>
		       <input type="checkbox" name="gCOMPARISON" value ="1" <c:if test="${gSiteConfig['gCOMPARISON']}">checked</c:if>>
		     </div>
		     <c:if test="${gSiteConfig['gCOMPARISON']}">
		     <div class="form">
		       <label><input type="hidden" name="__key" value="COMPARISON_ON_CATEGORY"><fmt:message key="comparisonOnCategory" />:</label>
		       <input type="checkbox" name="COMPARISON_ON_CATEGORY" value ="true" <c:if test="${siteConfig['COMPARISON_ON_CATEGORY'].value == 'true'}">checked</c:if>>
		     </div> 
		     </c:if>
		       
		     <div class="form">
		       <label><input type="hidden" name="__mkey" value="PRODUCT_IMAGE_LAYOUTS"><fmt:message key="image" /> <fmt:message key="layout" />:</label>
		       <c:forTokens items="ss,mb,s2,qb,mz" delims="," var="slideshow" varStatus="status">
		  	     <input type="checkbox" name="PRODUCT_IMAGE_LAYOUTS" value ="${slideshow}" <c:if test="${fn:contains(siteConfig['PRODUCT_IMAGE_LAYOUTS'].value,slideshow)}">checked</c:if>><fmt:message key="${slideshow}" />
		  	   </c:forTokens>
		     </div> 
		     <div class="form">
		       <label><input type="hidden" name="__key" value="HTML_SCROLLER">Product Scroller (html):</label>
		       <input type="checkbox" name="HTML_SCROLLER" value ="true" <c:if test="${'true' == siteConfig['HTML_SCROLLER'].value}">checked</c:if>>
		     </div> 
		     <div class="form">
		       <label><input type="hidden" name="__gkey" value="gCUSTOM_LINES"><fmt:message key="customLines" />:</label>
		       <input type="checkbox" name="gCUSTOM_LINES" value ="1" <c:if test="${gSiteConfig['gCUSTOM_LINES']}">checked</c:if>>
		     </div>
		     <div class="form">
		       <label><input type="hidden" name="__gkey" value="gTAB"><fmt:message key="productTab" />:</label>
		       <select name="gTAB">
				<c:forEach begin="0" end="10" var="tabNumber">
		  	    <option value="${tabNumber}" <c:if test="${tabNumber == gSiteConfig['gTAB']}">selected</c:if>>${tabNumber}</option>
				</c:forEach>
		 	   </select>
		     </div>   
		     <div class="form">
		       <label><input type="hidden" name="__key" value="PRODUCT_DEFAULT_DETAIL_LAYOUT">Product Detail View Default:</label>
		       <select name="PRODUCT_DEFAULT_DETAIL_LAYOUT">
		  	     <option value="000"><fmt:message key="000" /></option>
		  	     <option value="001" <c:if test="${'001' == siteConfig['PRODUCT_DEFAULT_DETAIL_LAYOUT'].value}">selected</c:if>><fmt:message key="001" /></option>
		  	     <option value="002" <c:if test="${'002' == siteConfig['PRODUCT_DEFAULT_DETAIL_LAYOUT'].value}">selected</c:if>><fmt:message key="002" /></option>
		 	     <option value="003" <c:if test="${'003' == siteConfig['PRODUCT_DEFAULT_DETAIL_LAYOUT'].value}">selected</c:if>><fmt:message key="003" /></option>
		 	     <option value="004" <c:if test="${'004' == siteConfig['PRODUCT_DEFAULT_DETAIL_LAYOUT'].value}">selected</c:if>><fmt:message key="004" /></option>
		 	     <option value="005" <c:if test="${'005' == siteConfig['PRODUCT_DEFAULT_DETAIL_LAYOUT'].value}">selected</c:if>><fmt:message key="005" /></option>
		 	     <option value="006" <c:if test="${'006' == siteConfig['PRODUCT_DEFAULT_DETAIL_LAYOUT'].value}">selected</c:if>><fmt:message key="006" /></option>
		 	     <option value="007" <c:if test="${'007' == siteConfig['PRODUCT_DEFAULT_DETAIL_LAYOUT'].value}">selected</c:if>><fmt:message key="007" /></option>
		 	     <option value="008" <c:if test="${'008' == siteConfig['PRODUCT_DEFAULT_DETAIL_LAYOUT'].value}">selected</c:if>><fmt:message key="008" /></option>
		 	     <option value="009" <c:if test="${'009' == siteConfig['PRODUCT_DEFAULT_DETAIL_LAYOUT'].value}">selected</c:if>><fmt:message key="009" /></option>
		 	     <option value="010" <c:if test="${'010' == siteConfig['PRODUCT_DEFAULT_DETAIL_LAYOUT'].value}">selected</c:if>><fmt:message key="010" /></option>		 	    
		 	     <option value="011" <c:if test="${'011' == siteConfig['PRODUCT_DEFAULT_DETAIL_LAYOUT'].value}">selected</c:if>><fmt:message key="011" /></option>		 	    
		 	     <option value="012" <c:if test="${'012' == siteConfig['PRODUCT_DEFAULT_DETAIL_LAYOUT'].value}">selected</c:if>><fmt:message key="012" /></option>
		 	     <option value="013" <c:if test="${'013' == siteConfig['PRODUCT_DEFAULT_DETAIL_LAYOUT'].value}">selected</c:if>><fmt:message key="013" /></option>
		 	     <option value="014" <c:if test="${'014' == siteConfig['PRODUCT_DEFAULT_DETAIL_LAYOUT'].value}">selected</c:if>><fmt:message key="014" /></option>
		 	     <option value="015" <c:if test="${'015' == siteConfig['PRODUCT_DEFAULT_DETAIL_LAYOUT'].value}">selected</c:if>><fmt:message key="015" /></option>
		 	     <option value="016" <c:if test="${'016' == siteConfig['PRODUCT_DEFAULT_DETAIL_LAYOUT'].value}">selected</c:if>><fmt:message key="016" /></option>	     		 	    
		 	     <option value="017" <c:if test="${'017' == siteConfig['PRODUCT_DEFAULT_DETAIL_LAYOUT'].value}">selected</c:if>><fmt:message key="017" /></option>	     		 	    
		 	     <option value="018" <c:if test="${'018' == siteConfig['PRODUCT_DEFAULT_DETAIL_LAYOUT'].value}">selected</c:if>><fmt:message key="018" /></option>
		 	     <option value="019" <c:if test="${'019' == siteConfig['PRODUCT_DEFAULT_DETAIL_LAYOUT'].value}">selected</c:if>><fmt:message key="019" /></option>
		 	     <option value="020" <c:if test="${'020' == siteConfig['PRODUCT_DEFAULT_DETAIL_LAYOUT'].value}">selected</c:if>><fmt:message key="020" /></option>
		 	     <option value="021" <c:if test="${'021' == siteConfig['PRODUCT_DEFAULT_DETAIL_LAYOUT'].value}">selected</c:if>><fmt:message key="021" /></option>
		 	     <option value="022" <c:if test="${'022' == siteConfig['PRODUCT_DEFAULT_DETAIL_LAYOUT'].value}">selected</c:if>><fmt:message key="022" /></option>
		 	     <option value="023" <c:if test="${'023' == siteConfig['PRODUCT_DEFAULT_DETAIL_LAYOUT'].value}">selected</c:if>><fmt:message key="023" /></option> 
		 	     <option value="041" <c:if test="${'041' == siteConfig['PRODUCT_DEFAULT_DETAIL_LAYOUT'].value}">selected</c:if>><fmt:message key="041" /></option>      		 	    	     		 	    
		 	     <option value="045" <c:if test="${'045' == siteConfig['PRODUCT_DEFAULT_DETAIL_LAYOUT'].value}">selected</c:if>><fmt:message key="045" /></option>
		 	     <option value="046" <c:if test="${'046' == siteConfig['PRODUCT_DEFAULT_DETAIL_LAYOUT'].value}">selected</c:if>><fmt:message key="046" /></option> 
		 	     <option value="047" <c:if test="${'047' == siteConfig['PRODUCT_DEFAULT_DETAIL_LAYOUT'].value}">selected</c:if>><fmt:message key="047" /></option> 
		 	     <option value="048" <c:if test="${'048' == siteConfig['PRODUCT_DEFAULT_DETAIL_LAYOUT'].value}">selected</c:if>><fmt:message key="048" /></option> 
		 	     <option value="049" <c:if test="${'049' == siteConfig['PRODUCT_DEFAULT_DETAIL_LAYOUT'].value}">selected</c:if>><fmt:message key="049" /></option> 
		 	     <option value="050" <c:if test="${'050' == siteConfig['PRODUCT_DEFAULT_DETAIL_LAYOUT'].value}">selected</c:if>><fmt:message key="050" /></option>
		 	     <option value="051" <c:if test="${'051' == siteConfig['PRODUCT_DEFAULT_DETAIL_LAYOUT'].value}">selected</c:if>><fmt:message key="051" /></option>  
		 	   </select>
		     </div> 
		     <div class="form">
		       <label><input type="hidden" name="__mkey" value="PRODUCT_DETAIL_LAYOUT">Product Detail View Layout:</label>
		       <c:forTokens items="000,001,002,003,004,005,006,007,008,009,010,011,012,013,014,015,016,017,018,019,020,021,022,023,041,045,046,047,048,049,050,051" delims="," var="layout" varStatus="status">
		  	     <input type="checkbox" name="PRODUCT_DETAIL_LAYOUT" value ="${layout}" <c:if test="${fn:contains(siteConfig['PRODUCT_DETAIL_LAYOUT'].value,layout)}">checked</c:if>><fmt:message key="${layout}" />
		  	   </c:forTokens>
		     </div> 
		     <div class="form">
		       <label><input type="hidden" name="__gkey" value="gMASTER_SKU"><fmt:message key="parentSku" />:</label>
		       <input type="checkbox" name="gMASTER_SKU" value ="1" <c:if test="${gSiteConfig['gMASTER_SKU']}">checked</c:if>>
		     </div> 
		     <div class="form">
		       <label><input type="hidden" name="__key" value="PRODUCT_CONFIGURATOR"><fmt:message key="productConfigurator" />:</label>
		       <input type="checkbox" name="PRODUCT_CONFIGURATOR" value ="true" <c:if test="${'true' == siteConfig['PRODUCT_CONFIGURATOR'].value}">checked</c:if>>
		     </div> 
		     <div class="form">
		       <label><input type="hidden" name="__key" value="PRODUCT_SYNC"><fmt:message key="productSync" />:</label>
		       <input type="text" name="PRODUCT_SYNC" value="<c:out value="${siteConfig['PRODUCT_SYNC'].value}"/>" maxlength="255" size="65">
		     </div> 
		     <div class="form">
		       <label><input type="hidden" name="__key" value="PRODUCT_INACTIVE_REDIRECT_FIELD">Product Inactive Redirect URL field:</label>
		       <input type="text" name="PRODUCT_INACTIVE_REDIRECT_FIELD" value="<c:out value="${siteConfig['PRODUCT_INACTIVE_REDIRECT_FIELD'].value}"/>" maxlength="2" size="5">
		     </div> 
		     <div class="form">
		       <label><input type="hidden" name="__gkey" value="gPRODUCT_VARIANTS"><fmt:message key="productVariants" />:</label>
		       <input type="checkbox" name="gPRODUCT_VARIANTS" value ="1" <c:if test="${gSiteConfig['gPRODUCT_VARIANTS']}">checked</c:if>>
		     </div> 
		     <div class="form">
		       <label><input type="hidden" name="__key" value="PRODUCT_MULTI_OPTION">Multi Option:</label>
		       <input type="checkbox" name="PRODUCT_MULTI_OPTION" value ="true" <c:if test="${'true' == siteConfig['PRODUCT_MULTI_OPTION'].value}">checked</c:if>>
		     </div>	    
		     <div class="form">
		       <label><input type="hidden" name="__key" value="SMART_DROPDOWN"><fmt:message key="smartDropDown" />:</label>
		       <input type="checkbox" name="SMART_DROPDOWN" value ="true" <c:if test="${siteConfig['SMART_DROPDOWN'].value == 'true'}">checked</c:if>>
		     </div> 
		     <div class="form">
		       <label><input type="hidden" name="__key" value="PRODUCT_QTY_DROPDOWN">Product Qty Dropdown:</label>
		       <input type="checkbox" name="PRODUCT_QTY_DROPDOWN" value ="true" <c:if test="${siteConfig['PRODUCT_QTY_DROPDOWN'].value == 'true'}">checked</c:if>>
		       <%-- Later get max Qty interval from SiteConfig. for Now it is hardcoded to 10
		       <select name="PRODUCT_QTY_DROPDOWN">
		  	     <option value=""></option>
		  	     <c:forTokens items="5,10,15,20" delims="," var="value">
		  	       <option value="${value}" <c:if test="${value == siteConfig['PRODUCT_QTY_DROPDOWN'].value}">selected</c:if>>${value}</option>
		  	     </c:forTokens>
		 	   </select>
		 	   --%> 
		     </div>
		     <div class="title"><fmt:message key="search" /></div>
		     <div class="form">
		       <label><input type="hidden" name="__gkey" value="gPRODUCT_FIELD_SEARCH"><fmt:message key="productFieldSearch" />:</label>
		       <input type="checkbox" name="gPRODUCT_FIELD_SEARCH" value ="1" <c:if test="${gSiteConfig['gPRODUCT_FIELD_SEARCH']}">checked</c:if>>
		       <input type="hidden" name="__key" value="PRODUCT_FIELDS_SEARCH_TYPE">
		       <select name="PRODUCT_FIELDS_SEARCH_TYPE">
		  	     <option value="0"></option>
		  	     <option value="1" <c:if test="${1 == siteConfig['PRODUCT_FIELDS_SEARCH_TYPE'].value}">selected</c:if>>DropDown</option>
		  	     <option value="2" <c:if test="${2 == siteConfig['PRODUCT_FIELDS_SEARCH_TYPE'].value}">selected</c:if>>Checkbox</option>
		 	   </select>
		     </div> 
		     <div class="form">
		       <label><input type="hidden" name="__key" value="SEARCH_FIELDS">Product Search Fields:</label>
		       <input type="text" name="SEARCH_FIELDS" value="<c:out value="${siteConfig['SEARCH_FIELDS'].value}"/>" maxlength="255" size="65">
		       <div class="helpNote" style="padding-left: 140px;">
				<p>Example: name,keywords,short_desc,field_1</p>
				<p>For Lucene: make sure id is also added to list. id,name,keywords,short_desc,field_1</p>
			 </div>
		     </div> 
		     <div class="form">
		       <label><input type="hidden" name="__key" value="SEARCH_FIELDS_MULTIPLE">Product Search Multiple Field Filter :</label>
		       <input type="checkbox" name="SEARCH_FIELDS_MULTIPLE" value ="true" <c:if test="${siteConfig['SEARCH_FIELDS_MULTIPLE'].value == 'true'}">checked</c:if>>
		     </div>
		     <div class="form">
		       <label><input type="hidden" name="__key" value="SEARCH_INFLECTOR">plural->singular Search:</label>
			    <input type="checkbox" name="SEARCH_INFLECTOR" value ="true" <c:if test="${'true' == siteConfig['SEARCH_INFLECTOR'].value}">checked</c:if>>
		     </div> 
		     <div class="form">
		       <label><input type="hidden" name="__key" value="GROUP_SEARCH"><fmt:message key="groupSearch" />:</label>
		       <input type="checkbox" name="GROUP_SEARCH" value ="true" <c:if test="${siteConfig['GROUP_SEARCH'].value == 'true'}">checked</c:if>>
		     </div>
		     <div class="form">
		       <label><input type="hidden" name="__key" value="LUCENE_REINDEX"><fmt:message key="luceneSearch" />:</label>
		       <input type="checkbox" name="LUCENE_REINDEX" value ="true" <c:if test="${siteConfig['LUCENE_REINDEX'].value=='true'}">checked</c:if>>
		     </div>              
		     <div class="form">
		       <label><input type="hidden" name="__key" value="INDEX_CATIDS_ON_LUCENE_PRODUCT"><fmt:message key="indexCatIdsOnLuceneProduct" />:</label>
		       <input type="checkbox" name="INDEX_CATIDS_ON_LUCENE_PRODUCT" value ="true" <c:if test="${siteConfig['INDEX_CATIDS_ON_LUCENE_PRODUCT'].value=='true'}">checked</c:if>>
		     </div>               
		     <div class="title">Shipping Options</div>
		     <div class="form">
		       <label><input type="hidden" name="__key" value="SHIPPING_LTL"><fmt:message key="lessThanTruckload" />:</label>
		       <input type="checkbox" name="SHIPPING_LTL" value ="true" <c:if test="${siteConfig['SHIPPING_LTL'].value == 'true'}">checked</c:if>>
		     </div>                       
		     <div class="form">
		       <label><input type="hidden" name="__gkey" value="gCUSTOM_SHIPPING_ADVANCED">Advanced Custom Shipping:</label>
		       <input type="checkbox" name="gCUSTOM_SHIPPING_ADVANCED" value ="1" <c:if test="${gSiteConfig['gCUSTOM_SHIPPING_ADVANCED']}">checked</c:if>>
		     </div>
		     <div class="form">
		       <label><input type="hidden" name="__key" value="CUSTOM_SHIPPING_LIMIT"><fmt:message key="customShippingLimit" />:</label>
		       <select name="CUSTOM_SHIPPING_LIMIT">
				 <c:forEach begin="5" end="100" step="5" var="value">
				  <option value="${value}" <c:if test="${value == siteConfig['CUSTOM_SHIPPING_LIMIT'].value}">selected</c:if>>${value}</option>
				 </c:forEach>
			   </select>
			 </div>
		     <div class="form">
		       <label><input type="hidden" name="__key" value="UPS_CALCULATOR">UPS Calculator:</label>
		       <input type="checkbox" name="UPS_CALCULATOR" value ="true" <c:if test="${siteConfig['UPS_CALCULATOR'].value == 'true'}">checked</c:if>>
		     </div>
		     <div class="form">
		       <label><input type="hidden" name="__gkey" value="gROAD_RUNNER">Road Runner:</label>
		       <input type="checkbox" name="gROAD_RUNNER" value ="1" <c:if test="${gSiteConfig['gROAD_RUNNER']}">checked</c:if>>
		     </div>
		     <div class="form">
		       <label><input type="hidden" name="__gkey" value="gSHIPPING_QUOTE">Shipping Quote:</label>
		       <input type="checkbox" name="gSHIPPING_QUOTE" value ="1" <c:if test="${gSiteConfig['gSHIPPING_QUOTE']}">checked</c:if>>
		     </div>
			 <div class="title"><fmt:message key="crm" /></div>
		     <div class="form">
		       <label><input type="hidden" name="__gkey" value="gCRM">CRM:</label>
		       <input type="checkbox" name="gCRM" value ="1" <c:if test="${gSiteConfig['gCRM']}">checked</c:if>>
		     </div>
		     <div class="title"><fmt:message key="sugarSync" /></div>
		     <div class="form">
		       <label><input type="hidden" name="__gkey" value="gSUGAR_SYNC">Sugar Sync:</label>
		       <input type="checkbox" name="gSUGAR_SYNC" value ="1" <c:if test="${gSiteConfig['gSUGAR_SYNC']}">checked</c:if>>
		     </div>
			 <div class="title">ETILIZE</div>
		     <div class="form">
		       <label><input type="hidden" name="__key" value="ETILIZE">App ID:</label>
		       <input type="text" name="ETILIZE" value="<c:out value="${siteConfig['ETILIZE'].value}"/>" maxlength="50" size="25">
		     </div>                       
		     <div class="title">SHOPATRON</div>
		     <div class="form">
		       <label><input type="hidden" name="__key" value="SHOPATRON">mfg_id.catalog_id:</label>
		       <input type="text" name="SHOPATRON" value="<c:out value="${siteConfig['SHOPATRON'].value}"/>" maxlength="50" size="25">
		     </div>
		     <div class="form">
		       <label><input type="hidden" name="__key" value="SHOPATRON_REQUIRE_LOGIN">Require Login</label>
		       <input type="checkbox" name="SHOPATRON_REQUIRE_LOGIN" value ="true" <c:if test="${siteConfig['SHOPATRON_REQUIRE_LOGIN'].value == 'true'}">checked</c:if>>
		     </div>		     
			 <div class="title">APPLIANCE SPEC</div>
		     <div class="form">
		       <label><input type="hidden" name="__key" value="APPLIANCE_SPEC_URL">APPLIANCE URL:</label>
		       <input type="text" name="APPLIANCE_SPEC_URL" value="<c:out value="${siteConfig['APPLIANCE_SPEC_URL'].value}"/>" maxlength="255" size="65">
		     </div>
		     <div class="title">EchoSign</div>
		     <div class="form">
		       <label><input type="hidden" name="__key" value="ECHOSIGN_API_KEY">API Key:</label>
		       <input type="text" name="ECHOSIGN_API_KEY" value="<c:out value="${siteConfig['ECHOSIGN_API_KEY'].value}"/>" maxlength="50" size="25">
		     </div>		     
		     <div class="form">
		       <label><input type="hidden" name="__key" value="ECHOSIGN_USER_KEY">User Key:</label>
		       <input type="text" name="ECHOSIGN_USER_KEY" value="<c:out value="${siteConfig['ECHOSIGN_USER_KEY'].value}"/>" maxlength="50" size="25">
		     </div>
		     <div class="title">CartSpan</div>
		     <div class="form">
		       <label><input type="hidden" name="__key" value="CARTSPAN_API_CODE">API Key:</label>
		       <input type="text" name="CARTSPAN_API_CODE" value="<c:out value="${siteConfig['CARTSPAN_API_CODE'].value}"/>" maxlength="50" size="25">
		     </div>			     
		     <div class="title"><fmt:message key="eBridge" /></div>
		     <div class="form">
		       <label><input type="hidden" name="__gkey" value="gEBRIDGE"><fmt:message key="eBridge" />:</label>
		       <input type="checkbox" name="gEBRIDGE" value ="1" <c:if test="${gSiteConfig['gEBRIDGE']}">checked</c:if>>
		     </div>
		     <div class="title"><fmt:message key="shopworks" /></div>
		     <div class="form">
		       <label><input type="hidden" name="__key" value="SHOPWORKS"><fmt:message key="shopworks" />:</label>
		       <input type="checkbox" name="SHOPWORKS" value ="true" <c:if test="${siteConfig['SHOPWORKS'].value == 'true'}">checked</c:if>>
		     </div>
			 <div class="title"><fmt:message key="technologo" /></div>
			 <div class="form">
		       <label><input type="hidden" name="__key" value="TECHNOLOGO"><fmt:message key="technologo" />:</label>
		       <input type="text" name="TECHNOLOGO" value="<c:out value="${siteConfig['TECHNOLOGO'].value}"/>" maxlength="50" size="25">
		     </div>
			 <div class="title">ASI</div>
			 <div class="form">
		       <label><input type="hidden" name="__gkey" value="gASI">ASI Feed</label>
		 	   <select name="gASI">
		  	    <option value="" ></option>
		  	    <%-- <option value="API" <c:if test="${'API' == gSiteConfig['gASI']}">selected</c:if>>API</option> --%>
		  	    <option value="File" <c:if test="${'File' == gSiteConfig['gASI']}">selected</c:if>>File</option>
		 	   </select>
		     </div>
		     <div class="form">
		       <label><input type="hidden" name="__key" value="ASI_FEED_PRODUCT_TOKEN">Product Token:</label>
		       <input type="text" name="ASI_FEED_PRODUCT_TOKEN" value="<c:out value="${siteConfig['ASI_FEED_PRODUCT_TOKEN'].value}"/>" maxlength="50" size="65">
		     </div>
		     <div class="form">
		       <label><input type="hidden" name="__key" value="ASI_FEED_SUPPLIER_TOKEN">Supplier Token:</label>
		       <input type="text" name="ASI_FEED_SUPPLIER_TOKEN" value="<c:out value="${siteConfig['ASI_FEED_SUPPLIER_TOKEN'].value}"/>" maxlength="50" size="65">
		     </div>
			 <div class="form">
		       <label><input type="hidden" name="__key" value="ASI_ENDQTYPRICING"><fmt:message key="endQtyPricing" />:</label>
		       <input type="checkbox" name="ASI_ENDQTYPRICING" value ="true" <c:if test="${siteConfig['ASI_ENDQTYPRICING'].value == 'true'}">checked</c:if>>
		     </div>
			 <div class="form">
		       <label><input type="hidden" name="__key" value="ASI_ENDQTYPRICING_CUSTOMER"><fmt:message key="customer" /> <fmt:message key="endQtyPricing" />:</label>
		       <input type="checkbox" name="ASI_ENDQTYPRICING_CUSTOMER" value ="true" <c:if test="${siteConfig['ASI_ENDQTYPRICING_CUSTOMER'].value == 'true'}">checked</c:if>>
		     </div>
		     <div class="title">ASI New Feed</div>
			 <div class="form">
		       <label><input type="hidden" name="__key" value="ASI_KEY"><fmt:message key="key"/>:</label>
		       <input type="text" name="ASI_KEY" value="<c:out value="${siteConfig['ASI_KEY'].value}"/>" maxlength="50" size="65">
		     </div>
		     <div class="form">
		       <label><input type="hidden" name="__key" value="ASI_SECRET"><fmt:message key="secret"/>:</label>
		       <input type="text" name="ASI_SECRET" value="<c:out value="${siteConfig['ASI_SECRET'].value}"/>" maxlength="50" size="65">
		     </div>
		     <div class="form">
		       <label><input type="hidden" name="__key" value="ASI_POPUP_DISPLAY"><fmt:message key="popupDisplay"/>:</label>
		       <input type="checkbox" name="ASI_POPUP_DISPLAY" value ="true" <c:if test="${siteConfig['ASI_POPUP_DISPLAY'].value == 'true'}">checked</c:if>>
		     </div>
		     <div class="title">THUB</div>
		     <div class="form">
		       <label><input type="hidden" name="__key" value="THUB_AUTHENTICATION">Username,Password:</label>
		       <input type="text" name="THUB_AUTHENTICATION" value="<c:out value="${siteConfig['THUB_AUTHENTICATION'].value}"/>" maxlength="50" size="65">
		     </div>

		     <div class="title">Stone Edge</div>
		     <div class="form">
		       <label><input type="hidden" name="__key" value="STONEEDGE_AUTHENTICATION"><fmt:message key="user" />,<fmt:message key="password" />:</label>
		       <input type="text" name="STONEEDGE_AUTHENTICATION" value="<c:out value="${siteConfig['STONEEDGE_AUTHENTICATION'].value}"/>" maxlength="50" size="65">
		     </div>

		  	<!-- end tab -->        
			</div>    
            </div> 
			</sec:authorize>         
  	
<!-- end tabs -->			
</div>

<!-- start button --> 
<div align="left" class="button"> 
  <sec:authorize ifAnyGranted="ROLE_AEM,ROLE_ADMIN_${prop_site_id},ROLE_SITE_INFO">
	<input type="submit" name="__update" value="<fmt:message key="Update" />">
  </sec:authorize>
</div>	
<!-- end button -->	
  
  <!-- end table -->
   </td><td class="boxmidr" ></td></tr>
  <tr><td class="botl"></td><td class="botr"></td></tr>
  </table>
  
<!-- end main box -->  
 </div>  
</form> 
<script type="text/javascript"> 
<!--
function toggleStateProvince(el) {
  if (el.value == "US") {
      document.getElementById('state').disabled=false;
      document.getElementById('state').style.display="block";
      document.getElementById('ca_province').disabled=true;
      document.getElementById('ca_province').style.display="none";
      document.getElementById('province').disabled=true;
      document.getElementById('province').style.display="none";
  } else if (el.value == "CA") {
      document.getElementById('state').disabled=true;
      document.getElementById('state').style.display="none";
      document.getElementById('ca_province').disabled=false;
      document.getElementById('ca_province').style.display="block";
      document.getElementById('province').disabled=true;
      document.getElementById('province').style.display="none";
  } else {
      document.getElementById('state').disabled=true;
      document.getElementById('state').style.display="none";
      document.getElementById('ca_province').disabled=true;
      document.getElementById('ca_province').style.display="none";
      document.getElementById('province').disabled=false;
      document.getElementById('province').style.display="block";
  }
}
toggleStateProvince(document.getElementById('country'));
	
	if (document.getElementById('gAFFILIATE').value >= 1) 
	{
	  document.getElementById('productAffiliateCommision1').style.display="block";
	  document.getElementById('childInvoice').style.display="block";
	  document.getElementById('numAffiliate').style.display="block";
	} else 
	{
	  document.getElementById('productAffiliateCommision1').style.display="none";
	  document.getElementById('childInvoice').style.display="none";
	  document.getElementById('numAffiliate').style.display="none";
	}

	if (document.getElementById('gSALES_PROMOTIONSYES').checked) 
	{
	  document.getElementById('salesAndPromo1').style.display="block";
	  document.getElementById('salesAndPromo2').style.display="block";
	  document.getElementById('advancedPromotion').style.display="block";
	  document.getElementById('showEligiblePromos').style.display="block";
	} else 
	{
	  document.getElementById('salesAndPromo1').style.display="none";
	  document.getElementById('salesAndPromo2').style.display="none";
	  document.getElementById('advancedPromotion').style.display="none";
	  document.getElementById('showEligiblePromos').style.display="none";
	}	
	
	if (document.getElementById('MINIMUM_ORDER_YES').checked) 
	{
	  document.getElementById('minimumOrderID').style.display="block";
	} else 
	{
	  document.getElementById('minimumOrderID').style.display="none";
	}
	
	if (document.getElementById('SHOPPING_CART_YES').checked) 
	{
	  document.getElementById('SalesRepID').style.display="block";
	} else 
	{
	  document.getElementById('SalesRepID').style.display="none";
	}

	if (document.getElementById('gACCESS_PRIVILEGE').checked) 
	{
	  document.getElementById('accessBody').style.display="block";
	} else 
	{
	  document.getElementById('accessBody').style.display="none";
	}
	
	if (document.getElementById('gINVENTORY').checked) 
	{
	<c:if test="${gSiteConfig['gINVENTORY']}">
	  $('outOfStockID').style.display="block";
	  $('lowStockID').style.display="block";
	  $('shoppingCartQtyAdjustID').style.display="block";
	  </c:if>
	  $('inventoryImportExportID').style.display="block";
	  $('inventoryTypeID').style.display="block";
	  $('purchaseOrderID').style.display="block";
	} else 
	{
	<c:if test="${gSiteConfig['gINVENTORY']}">
	  $('lowStockID').style.display="none";
	  $('outOfStockID').style.display="none";
	  $('shoppingCartQtyAdjustID').style.display="none";
	  </c:if>
	  $('inventoryImportExportID').style.display="none";
	  $('inventoryTypeID').style.display="none";
	  $('purchaseOrderID').style.display="none";
	}
	if (document.getElementById('gPRODUCT_REVIEW').checked) 
	{
	  document.getElementById('reviewType').style.display="block";
	} else 
	{
	  document.getElementById('reviewType').style.display="none";
	}
//-->
</script>	 
  </tiles:putAttribute>    
</tiles:insertDefinition>