<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="/WEB-INF/tlds/spring.tld" prefix="spring" %>
<%@ taglib uri="http://www.springframework.org/security/tags" prefix="sec" %>

<tiles:insertDefinition name="admin.config" flush="true">
  <tiles:putAttribute name="content" type="string">

<form id="siteMessageGroup" action="siteMessageGroup.jhtm" method="post">
<!-- main box -->
  <div id="mboxfull">
  
  <!-- start table -->
  <table cellpadding="0" cellspacing="0" border="0" class="module" >
  <tr>
    <td class="topl_g">
	  
	  <!-- breadcrumb -->
	  <p class="breadcrumb">
	    <a href="../config">SiteInfo</a> &gt;
	    <fmt:message key="siteMessageGroupTitle"/> 
	  </p>
	  
	  <!-- Error -->
	  
	 <!-- header image -->
	  <img class="headerImage" src="../graphics/customer.gif" />
  
  </td><td class="topr_g" ></td></tr>
  <tr><td class="boxmidlrg" >

 <!-- tabs -->
<div class="tab-wrapper">
	<h4 title="List of SiteMessageGroup"></h4>
	<div>
	<!-- start tab -->

	  	<%-- <div class="listdivi ln tabdivi"></div>--%>
	  	<div class="listdivi"></div>
	    
		  	<div class="listlight">
		  	<!-- input field -->
			<table border="0" cellpadding="0" cellspacing="1" width="100%">
				<tr>
					<c:if test="${model.siteMessageGroups.nrOfElements > 0}">
						<td class="pageShowing">
							<fmt:message key="showing">
								<fmt:param value="${model.siteMessageGroups.firstElementOnPage + 1}" />
								<fmt:param value="${model.siteMessageGroups.lastElementOnPage + 1}" />
								<fmt:param value="${model.siteMessageGroups.nrOfElements}" />
							</fmt:message>
						</td>
					</c:if>
					<td class="pageNavi">
						Page
						<select name="page" id="page" onchange="submit()">
							<c:forEach begin="1" end="${model.siteMessageGroups.pageCount}"
								var="page">
								<option value="${page}"
									<c:if test="${page == (model.siteMessageGroups.page+1)}">selected</c:if>>
									${page}
								</option>
							</c:forEach>
						</select> 
						of
						<c:out value="${model.siteMessageGroups.pageCount}" /> 
						|
						<c:if test="${model.siteMessageGroups.firstPage}">
							<span class="pageNaviDead"><fmt:message key="previous" /></span>
						</c:if>
						<c:if test="${not model.siteMessageGroups.firstPage}">
							<a
								href="<c:url value="siteMessage.jhtm"><c:param name="page" value="${model.siteMessageGroups.page}"/><c:if test="${not model.search['homePage']}"><c:param name="parent" value="${model.search['parent']}"/></c:if></c:url>"
								class="pageNaviLink"><fmt:message key="previous" /></a>
						</c:if>
						|
						<c:if test="${model.siteMessageGroups.lastPage}">
							<span class="pageNaviDead"><fmt:message key="next" /></span>
						</c:if>
						<c:if test="${not model.siteMessageGroups.lastPage}">
							<a
								href="<c:url value="siteMessage.jhtm"><c:param name="page" value="${model.siteMessageGroups.page+2}"/><c:param name="size" value="${model.siteMessages.pageSize}"/></c:url>"
								class="pageNaviLink"><fmt:message key="next" /></a>
						</c:if>
					</td>
				</tr>
			</table>

			<table border="0" cellpadding="0" cellspacing="0" width="100%"
				class="listings">
				<tr class="listingsHdr2">
					<td class="indexCol">
						&nbsp;
					</td>
					<td class="listingsHdr3">
						<fmt:message key="Name" />
					</td>
					<td class="listingsHdr3">
						<fmt:message key="id" />
					</td>
					<td  class="listingsHdr3">
						<fmt:message key="siteMessageTitle" />
					</td>
					<td class="listingsHdr3">
						<fmt:message key="date" />
					</td>
									
				</tr>
				<c:forEach items="${model.siteMessageGroups.pageList}" var="siteMessageGroup"
					varStatus="status">
					<tr class="row${status.index % 2}"
						onmouseover="changeStyleClass(this,'rowOver')"
						onmouseout="changeStyleClass(this,'row${status.index % 2}')">
						<td class="indexCol">
							<c:out
								value="${status.count + model.siteMessageGroup.firstElementOnPage}" />
							.
						</td>
						<td class="nameCol">
						 <a href="<c:url value="siteMessageGroupForm.jhtm"><c:param name="id" value="${siteMessageGroup.id}"/></c:url>" class="nameLink<c:if test="${!siteMessageGroup.active}">Inactive</c:if>">
							<c:out
								value="${siteMessageGroup.name }" />
						</td>
						<td class="nameCol">
							<c:out value="${siteMessageGroup.id}" />
						</td>
						<td class="nameCol" ><a href="<c:url value="siteMessage.jhtm"><c:param name="groupId" value="${siteMessageGroup.id}"/></c:url>" class="nameLink">
     							<c:out
								value="${siteMessageGroup.numMessage }" />
						</td>	
						</td>					
						<td class="nameCol"><fmt:formatDate type="date" timeStyle="default" value="${siteMessageGroup.created }"/></td>							
					</tr>
				</c:forEach>
				<c:if test="${model.siteMessages.nrOfElements == 0}">
					<tr class="emptyList">
						<td colspan="8">
							&nbsp;
						</td>
					</tr>
				</c:if>
			</table>

			<table border="0" cellpadding="0" cellspacing="1" width="100%">
				<tr>
				  <td class="pageSize">
					<select name="size"
						onchange="document.getElementById('page').value=1;submit()">
						<c:forTokens items="10,25,50,100" delims="," var="current">
							<option value="${current}"
								<c:if test="${current == model.siteMessageGroups.pageSize}">selected</c:if>>
								${current}
								<fmt:message key="perPage" />
							</option>
						</c:forTokens>
					</select>
				  </td>
				</tr>
			</table>
            <!-- end input field -->  	  
		  	
	        </div>
  	
	<!-- end tab -->        
	</div>
<!-- end tabs -->			
</div>

<!-- start button -->
<div align="left" class="button">
    <input type="submit" name="add_siteMessageGroup" value="<fmt:message key='groupAdd' />"  /> 
</div>	
<!-- end button -->	 
  
  <!-- end table -->
   </td><td class="boxmidr" ></td></tr>
  <tr><td class="botl"></td><td class="botr"></td></tr>
  </table>
  
<!-- end main box -->  
</div>
</form>	

  </tiles:putAttribute>    
</tiles:insertDefinition>