<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="/WEB-INF/tlds/spring.tld" prefix="spring" %>
<%@ taglib uri="http://www.springframework.org/security/tags" prefix="sec" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>

<tiles:insertDefinition name="admin.config" flush="true">
  <tiles:putAttribute name="content" type="string">
<sec:authorize ifAnyGranted="ROLE_AEM,ROLE_ADMIN_${prop_site_id},ROLE_SITE_INFO">
<script language="JavaScript" type="text/JavaScript">
<!--
window.addEvent('domready', function(){			
	var tabs1 = new SimpleTabs($('tab-block-1'), { selector: 'h4'});
	var Tips1 = new Tips($$('.toolTipImg'));
});
//-->
</script>
<form:form commandName="createSitemap" method="post">
<!-- main box -->
  <div id="mboxfull">
  
  <!-- start table -->
  <table cellpadding="0" cellspacing="0" border="0" class="module" >
  <tr>
    <td class="topl_g">
	  
	  <!-- breadcrumb -->
	  <p class="breadcrumb">
	    <a href="../config">SiteInfo</a> &gt;
	    sitemap
	  </p>
	  
	  <!-- Error Message -->
	  <c:if test="${message != null}">
        <div class="message"><c:out value="${message}" /></div>
      </c:if>
	  
  </td><td class="topr_g" ></td></tr>
  <tr><td class="boxmidlrg" >

 <!-- tabs -->
 <div id="tab-block-1">
	<h4 title="sitemap">Sitemap</h4>
	<div>
	<!-- start tab -->

        <c:set var="classIndex" value="0" />
        
	  	<div class="listdivi ln tabdivi"></div>
	  	<div class="listdivi"></div>
		  	<div class="list${classIndex % 2}"><c:set var="classIndex" value="${classIndex+1}" />
		  	<div class="listfl"><div class="helpImg"><img class="toolTipImg" title="Note::Required if you want to include only Active SKUs in Sitemap." src="../graphics/question.gif" /></div><fmt:message key="active" />:</div>
		  	<div class="listp">
		  	<!-- input field -->
		  		<form:select path="active">
		  		    <form:option value="1"><fmt:message key="active" /></form:option>
		  		  	<form:option value=""><fmt:message key="all"/></form:option> 
	        		<form:option value="0"><fmt:message key="inactive" /></form:option>
	      		</form:select>
		  	</div>
		  	</div>
		  	
		  	<c:if test="${gSiteConfig['gMASTER_SKU']}">
		  		<div class="list${classIndex % 2}"><c:set var="classIndex" value="${classIndex+1}" />
		  		<div class="listfl"><div class="helpImg"><img class="toolTipImg" title="Note::Required if you want to include only Master SKUs in Sitemap." src="../graphics/question.gif" /></div><fmt:message key="only" /> <fmt:message key="masterSku" />:</div>
		  		<div class="listp">
		  		<!-- input field -->
					<form:checkbox path="masterSku" disabled="true" />
	 			</div>
		  		</div>
		  	</c:if>
		  	
		  	<div class="list${classIndex % 2}"><c:set var="classIndex" value="${classIndex+1}" />
		  	<div class="listfl"><div class="helpImg"><img class="toolTipImg" title="Note::Required if you want to include only Non Protected SKUs in Sitemap." src="../graphics/question.gif" /></div><fmt:message key="only" /> <fmt:message key="nonProtected" />:</div>
		  	<div class="listp">
		  	<!-- input field -->
		  		<form:checkbox path="protectedAccess" value="0" disabled="true"/>
 			</div>
		  	</div>
		  	
		  	<div class="list${classIndex % 2}"><c:set var="classIndex" value="${classIndex+1}" />
	  		<div class="listfl"><div class="helpImg"><img class="toolTipImg" title="Note::Searchable product." src="../graphics/question.gif" /></div><fmt:message key="searchable" />:</div>
	  		<div class="listp">
	  		<!-- input field -->
				<form:checkbox path="searchable" />
 			</div>
	  		</div>
		  	
		  	<div class="list${classIndex % 2}"><c:set var="classIndex" value="${classIndex+1}" />
		  	<div class="listp">
		  	<!-- input field -->
			    <table class="form" width="100%">
			      <c:forEach items="${exportedFiles}" var="file">
				  <tr>
				    <td class="formName">&nbsp;</td>
				    <td><a href="../../${file['file'].name}"><c:out value="${file['file'].name}"/></a> 
				    	<c:if test="${file['lastModified'] != null}">(<fmt:formatDate type="both" timeStyle="full" value="${file['lastModified']}"/>)</c:if></td>
				  </tr>
				  </c:forEach>
				</table>
		    <!-- end input field -->   	
		  	</div>
		  	</div>
	  	
	<!-- end tab -->        
	</div> 
	
<!-- end tabs -->			
</div>

<!-- start button -->
<div align="left" class="button"> 
    <input type="submit" name="__new" value="<fmt:message key="fileGenerate" />">
</div>
<!-- end button -->	     
  
  <!-- end table -->
    </td><td class="boxmidr" ></td></tr>
  <tr><td class="botl"></td><td class="botr"></td></tr>
  </table>
  
<!-- end main box -->  
 </div>
</form:form>
</sec:authorize>

  </tiles:putAttribute>    
</tiles:insertDefinition>