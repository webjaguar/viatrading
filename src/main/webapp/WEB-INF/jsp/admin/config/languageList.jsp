<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="/WEB-INF/tlds/spring.tld" prefix="spring" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ taglib uri="http://www.springframework.org/security/tags" prefix="sec" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>

<tiles:insertDefinition name="admin.config" flush="true">
  <tiles:putAttribute name="content" type="string">

<script language="JavaScript"> 
<!--
window.addEvent('domready', function(){			
	var tabs1 = new SimpleTabs($('tab-block-1'), { selector: 'h4'});
});
//--> 
</script>
<form action="languageList.jhtm" method="post">

	<div id="mboxfull">
	<!-- start table -->
  	<table cellpadding="0" cellspacing="0" border="0" class="module" >
  	<tr>
	    <td class="topl_g">
		  
		  <!-- breadcrumb -->
		  <p class="breadcrumb">
		    <a href="../config"><fmt:message key="configTabTitle"/></a> &gt;		     
		    <fmt:message key="language"/> 
		  </p>
	
	   </td><td class="topr_g" ></td>
   </tr>
   <tr><td class="boxmidlrg" >
   	 <div class="tab-wrapper">
		<h4 title=""></h4>
		<div>
			<div class="listdivi"></div>	   		    
		  	<div class="listlight">
		  	<!-- input field -->				
				<div style="width:100%;">
					<table border="0" cellpadding="0" cellspacing="0" width="100%" class="listings">
						  <tr class="listingsHdr2">
						    <td class="indexCol">&nbsp;</td>						    
						    <td class="listingsHdr3"><fmt:message key="language" /></td>
						    <td class="listingsHdr3">Action</td>						    						    
						  </tr>
						  <c:forEach items="${model.languagelist}" var="language" varStatus="status">
						  <tr class="row${status.index % 2}" onmouseover="changeStyleClass(this,'rowOver')" onmouseout="changeStyleClass(this,'row${status.index % 2}')">
							<td class="indexCol"></td>	
							<td class="nameCol"><fmt:message key="language_${language.languageCode}"/> (<c:out value="${language.languageCode}"/>)<input type="hidden" name="__code" value="${language.languageCode}"></td>						    	
						    <td class="nameCol"><a href="language.jhtm?languageCode=${language.languageCode}">Setting</a></td>						    	
						  </tr>
						  </c:forEach>
				   </table>
				   
				   <!-- start button -->
				  	<div align="left" class="button"> 
					  <sec:authorize ifAnyGranted="ROLE_AEM">  
						<input type="submit" name="add" value="<fmt:message key="add"/>">						
					  </sec:authorize>
					</div>
				
				</div>		  	
		  	</div>		
		</div>
	 </div>
   </td><td class="boxmidr" ></td></tr>
  <tr><td class="botl"></td><td class="botr"></td></tr>
  	</table>
  	<!-- end table -->
  	</div>

</form>
</tiles:putAttribute>
</tiles:insertDefinition>