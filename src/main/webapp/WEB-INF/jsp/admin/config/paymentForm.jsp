<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="/WEB-INF/tlds/spring.tld" prefix="spring" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ taglib uri="http://www.springframework.org/security/tags" prefix="sec" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>

<tiles:insertDefinition name="admin.config" flush="true">
  <tiles:putAttribute name="content" type="string">

<sec:authorize ifAnyGranted="ROLE_AEM,ROLE_ADMIN_${prop_site_id},ROLE_SITE_INFO">  
<script language="JavaScript"> 
<!--
window.addEvent('domready', function(){			
			var tabs1 = new SimpleTabs($('tab-block-1'), { selector: 'h4'});
		});
//--> 
</script> 
<form:form commandName="paymentMethodForm" method="post" >
<!-- main box -->
  <div id="mboxfull">
  
  <!-- start table -->
  <table cellpadding="0" cellspacing="0" border="0" class="module" >
  <tr>
    <td class="topl_g">
	  
	  <!-- breadcrumb -->
	  <p class="breadcrumb">
	    <a href="../config">SiteInfo</a> &gt;
	    <fmt:message key="paymentMethod"/>
	  </p>
	  
	  <!-- Error Message -->
	  <c:if test="${!empty message}">
		  <div class="message"><spring:message code="${message}" /></div>
	  </c:if>
	  
  </td><td class="topr_g" ></td></tr>
  <tr><td class="boxmidlrg" >

 <!-- tabs -->
 <div id="tab-block-1">
	<h4 title="<fmt:message key="paymentMethod"/> Form"><fmt:message key="paymentMethod"/></h4>
	<div>
	<!-- start tab -->
         <c:set var="classIndex" value="0" />
         
	  	<div class="listdivi ln tabdivi"></div>
	  	<div class="listdivi"></div>

		  	<div class="list${classIndex % 2}"><c:set var="classIndex" value="${classIndex+1}" />

		  	<!-- input field -->
				<table class="form" >
				  <tr>
				    <td>&nbsp;</td>
				    <td align="center" width="50"><fmt:message key="Enabled" /></td>
				    <td style="padding-left:20px"><fmt:message key="title" /></td>
				    <td style="padding-left:20px"><fmt:message key="code" /></td>
				    <td align="center" width="50"><fmt:message key="internal" /></td>
				    <c:if test="${gSiteConfig['gGROUP_CUSTOMER']}">
				    <td align="center"><fmt:message key="group" /></td>
				    </c:if>
				    <td align="center"><fmt:message key="rank" /></td>
				  </tr>
				  <c:forTokens items="VISA,AMEX,MC,DISC,JCB" delims="," var="cc" varStatus="status">
				  <tr>
				    <td style="width:100px;text-align:right">&nbsp;<c:if test="${status.first}"><fmt:message key="creditcard" /> : </c:if></td>
				    <td align="center">
				       <input type="checkbox" name="CREDIT_CARD_PAYMENT" value ="${cc}" <c:if test="${fn:contains(siteConfig['CREDIT_CARD_PAYMENT'].value,cc)}">checked</c:if>>
				    </td>
				    <td><fmt:message key="${cc}"/></td>
				    <td>&nbsp;</td>
				    <td>&nbsp;</td>
				    <c:if test="${gSiteConfig['gGROUP_CUSTOMER']}">
				    <td>&nbsp;</td>    
				    <td>&nbsp;</td>
				    </c:if>
				  </tr>
				  </c:forTokens>
				  <c:forEach items="${paymentMethodForm.customPaymentMethods}" var="paymentMethod" varStatus="status">
				  <tr>
				    <td style="width:100px;text-align:right"><fmt:message key="custom" /> <c:out value="${status.index + 1}"/> : </td>
				    <td align="center"><input name="__customEnabled_${paymentMethod.id}" type="checkbox" <c:if test="${paymentMethod.enabled}">checked</c:if>></td>
				    <td>
						<input type="text" class="textfield" name="__customTitle_${paymentMethod.id}" value="<c:out value="${paymentMethod.title}"/>" maxlength="80" size="50">
				    </td>
				    <td>
						<input type="text" class="textfield75" name="__customCode_${paymentMethod.id}" value="<c:out value="${paymentMethod.code}"/>" maxlength="35" size="30">
				    </td>
				    <td align="center"><input name="__customInternal_${paymentMethod.id}" type="checkbox" <c:if test="${paymentMethod.internal}">checked</c:if>></td>
					<c:if test="${gSiteConfig['gGROUP_CUSTOMER']}">
					<td align="center">
					  <select name="__customGroupId_${paymentMethod.id}">
						<option value=""></option>
						<c:forEach items="${groups}" var="group">
						<option value="${group.id}" <c:if test="${paymentMethod.groupId == group.id}">selected</c:if>><c:out value="${group.name}" /></option>
						</c:forEach>
					  </select>
					</td>
					</c:if>
					<td>
						<input type="text" class="textfield50" name="__customRank_${paymentMethod.id}" value="<c:out value="${paymentMethod.rank}"/>" maxlength="20" size="30">
				   </td>
				  </tr>
				   
				  </c:forEach>
				 
				</table>
			<!-- end input field -->	
			</div>
	<!-- end tab -->		
	</div>	
<!-- end tabs -->			
</div>

<!-- start button -->
<div align="left" class="button"> 
    <input type="submit" name="__update" value="<fmt:message key="Update" />">
</div>	
<!-- end button -->

  <!-- end table -->
    </td><td class="boxmidr" ></td></tr>
  <tr><td class="botl"></td><td class="botr"></td></tr>
  </table>
  
<!-- end main box -->  
 </div>
</form:form>
</sec:authorize>

  </tiles:putAttribute>    
</tiles:insertDefinition>