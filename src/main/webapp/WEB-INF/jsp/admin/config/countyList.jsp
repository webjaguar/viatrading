<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/security/tags" prefix="sec" %>
<%@ taglib uri="/WEB-INF/tlds/spring.tld" prefix="spring" %>

<tiles:insertDefinition name="admin.config" flush="true">
  <tiles:putAttribute name="content" type="string">
  
<sec:authorize ifAnyGranted="ROLE_AEM,ROLE_ADMIN_${prop_site_id},ROLE_SITE_INFO">
<c:if test="${gSiteConfig['gSHOPPING_CART']}">
 
<form action="countyList.jhtm" method="post">
<!-- main box -->
  <div id="mboxfull">
  
  <!-- start table -->
  <table cellpadding="0" cellspacing="0" border="0" class="module" >
  <tr>
    <td class="topl_g">
	  
	  <!-- breadcrumb -->
	  <p class="breadcrumb">
	    <a href="../config">SiteInfo</a> &gt;
	    <a href="../config/stateList.jhtm">States</a> &gt;
	    <c:out value="${model.state}"/>
	  </p>
	  
	  <!-- Error Message -->
	  <c:if test="${!empty message}">
		  <div class="message"><spring:message code="${message}" /></div>
	  </c:if>
  
  </td><td class="topr_g" ></td></tr>
  <tr><td class="boxmidlrg" >

 <!-- tabs -->
<div class="tab-wrapper">
	<h4 title="List of affiliates"></h4>
	<div>
	<!-- start tab -->

	  	<div class="listdivi ln tabdivi"></div>
	  	<div class="listdivi"></div>
	   
		  	<div class="listlight">
		  	<!-- input field -->

				<div style="width:100%;">
				<table border="0" cellpadding="0" cellspacing="0" width="100%" class="listings">
				  <input type="hidden" name="country" value="${model.country}" />
				  <input type="hidden" name="state" value="${model.state}" />
				  <tr class="listingsHdr2">
				    <td class="indexCol">&nbsp;</td>   
				    <td class="listingsHdr3"><c:out value="${model.state}"/> Counties</td>  
				    <c:if test="${gSiteConfig['gCITY_TAX']}">
				      <td class="listingsHdr3">City <fmt:message key="taxRate" /></td>
				    </c:if>   
				    <td class="listingsHdr3"><fmt:message key="taxRate" /></td>
				  </tr>
				<c:forEach items="${model.countyList}" var="county" varStatus="status">
				  <tr class="row${status.index % 2}" onmouseover="changeStyleClass(this,'rowOver')" onmouseout="changeStyleClass(this,'row${status.index % 2}')">
				    <td class="indexCol"><c:out value="${status.count}"/>.</td>
				    <td class="nameCol"><c:out value="${county.county}"/><input type="hidden" name="__counties" value="${county.county}" /></td>
				    <c:if test="${gSiteConfig['gCITY_TAX']}">
				      <td class="nameCol" valign="middle"><a href="<c:url value="cityList.jhtm"><c:param name="county" value="${county.county}"/><c:param name="state" value="${model.state}"/></c:url>" ><img border="0" src="../graphics/edit.gif" align="middle"/></a></td>
				    </c:if>                      
				    <td class="rankCol"><input name="__taxrate_${county.county}" type="text" value="${county.taxRate}" size="5" maxlength="5" class="rankField"></td>
				  </tr>
				</c:forEach>
				</table>
            <!-- end input field -->  	  
		  	</div>

		  	
		  	<!-- start button -->
			<div align="left" class="button"> 
			  <input type="submit" name="__update" value="<fmt:message key="Update" />">
			</div>
	  		<!-- end button -->	
		</div>
		<!-- end tab -->        
	</div>
<!-- end tabs -->			
</div>
  
  <!-- end table -->
   </td><td class="boxmidr" ></td></tr>
  <tr><td class="botl"></td><td class="botr"></td></tr>
  </table>
  
<!-- end main box -->  
</div>
</div>
</form>
</c:if>
</sec:authorize>
 
  </tiles:putAttribute>    
</tiles:insertDefinition>


