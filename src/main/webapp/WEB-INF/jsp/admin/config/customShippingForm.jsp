<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="/WEB-INF/tlds/spring.tld" prefix="spring" %>
<%@ taglib uri="http://www.springframework.org/security/tags" prefix="sec" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>

<tiles:insertDefinition name="admin.config.shipping" flush="true">
  <tiles:putAttribute name="content" type="string">
  
<sec:authorize ifAnyGranted="ROLE_AEM,ROLE_ADMIN_${prop_site_id},ROLE_SITE_INFO"> 
<script type="text/javascript"> 
<!--
window.addEvent('domready', function(){			
	var tabs1 = new SimpleTabs($('tab-block-1'), { selector: 'h4'});
	var Tips1 = new Tips($$('.toolTipImg'));
	var count = 0;
	$$('div.mainContainer').each(function(el) {
		el.addClass(count++ % 2 == 0 ? 'list0' : 'list1');
	});
	$('shippingRate.ignoreCarriers1').addEvent('change', function() {
		if ($('shippingRate.ignoreCarriers1').checked) {
			$('carrierList').setStyle('display', 'none');
		} else {
			$('carrierList').setStyle('display', 'block');
		}	
	});
});
//-->
</script>
<style>
#container { display: table;  }
.row  { display: table-row; }
.cell { display: table-cell;  width:50px; }
</style>
<form:form id="customShippingForm" commandName="customShippingForm" method="post" >
<!-- main box -->
  <div id="mboxfull">
  
  <!-- start table -->
  <table cellpadding="0" cellspacing="0" border="0" class="module" >
  <tr>
    <td class="topl_g">
	  
	  <!-- breadcrumb -->
	  <p class="breadcrumb">
	    <a href="../config">SiteInfo</a> &gt;
	    <fmt:message key="customShipping"/>
	  </p>
	  
	  <!-- Error Message -->
  	  <spring:hasBindErrors name="customShippingForm">
		<div class="message">Please fix all errors!</div>
	  </spring:hasBindErrors>
	  
  </td><td class="topr_g" ></td></tr>
  <tr><td class="boxmidlrg" >

 <!-- tabs -->
 <div id="tab-block-1">
	<h4 title="Custom Shipping Form"><fmt:message key="customShipping"/></h4>
	<div>
	<!-- start tab -->
         <c:set var="classIndex" value="0" />
         
	  	<div class="listdivi ln tabdivi"></div>
	  	<div class="listdivi"></div>
	   
		  	<div class="list${classIndex % 2}"><c:set var="classIndex" value="${classIndex+1}" />
		  	<div class="listfl"><fmt:message key='internal' />:</div>
		  	<div class="listp">
		  	<!-- input field -->
				<form:checkbox path="shippingRate.internal"/>
	 		<!-- end input field -->	
	 		</div>
		  	</div>			

		  	<div class="list${classIndex % 2}"><c:set var="classIndex" value="${classIndex+1}" />
		  	<div class="listfl"><div class="helpImg"><img class="toolTipImg" title="Ingore Carriers::If enabled, it will not return rates from UPS, USPS and FedEx. Even one custom shipping has this flag enabled, than it will not return rates for UPS, USPS and FedEx. If other custom shipping is assigned to any of these carriers, that custom shipping option will not be displayed." src="../graphics/question.gif" /></div> <fmt:message key='ignoreCarriers' />:</div>
		  	<div class="listp">
		  	<!-- input field -->
				<form:checkbox path="shippingRate.ignoreCarriers"/>
	 		<!-- end input field -->	
	 		</div>
		  	</div>			

		  	<div class="list${classIndex % 2}"><c:set var="classIndex" value="${classIndex+1}" />
		  	<div class="listfl"><div class="helpImg"><img class="toolTipImg" title="Custom Shipping::Please make sure that custom shipping is enabled on products for which you want this options to be available during checkout." src="../graphics/question.gif" /></div> <fmt:message key="title" />:</div>
		  	<div class="listp">
		  	<!-- input field -->
				<form:input path="shippingRate.title" cssClass="textfield" />
				<form:errors path="shippingRate.title" cssClass="error"/>	
	 		<!-- end input field -->	
	 		</div>
		  	</div>			
	
		  	<div class="list${classIndex % 2}"><c:set var="classIndex" value="${classIndex+1}" />
		  	<div class="listfl"><div class="helpImg"><img class="toolTipImg" title="Note::Select Price. If '% of Shipping Carrier' is selected, discount will be given equivalent to Price % from Cost of the carrier selected. If 'fixed' is selected, cost of the carrier will be replaced by Price. If '% of Subtotal' is selected, discount will be given equivalent to Price % from Cost of the Subtotal. " src="../graphics/question.gif" /></div><fmt:message key='price' />:</div>
		  	<div class="listp">
		  	<!-- input field -->
				<form:input path="shippingRate.price" cssClass="textfield" />
				<form:select path="shippingRate.percent" cssStyle="width:100px;">
					<form:option value="0" label="Fixed $"></form:option>
					<form:option value="1" label="% of Shipping Carrier" />
					<form:option value="2" label="% of Subtotal" />
					<form:option value="3" label="per Unit of Weight" />
				</form:select>
			<!-- end input field -->	
	 		</div>
		  	</div>			

		  	<div class="list${classIndex % 2}"><c:set var="classIndex" value="${classIndex+1}" />
		  	<div class="listfl"><fmt:message key='minPrice' />:</div>
		  	<div class="listp">
		  	<!-- input field -->
				<form:input path="shippingRate.minPrice" cssClass="textfield"/>
				<form:errors path="shippingRate.minPrice" cssClass="error"/>
			<!-- end input field -->	
	 		</div>
		  	</div>			

		  	<div class="list${classIndex % 2}"><c:set var="classIndex" value="${classIndex+1}" />
		  	<div class="listfl"><fmt:message key='maxPrice' />:</div>
		  	<div class="listp">
		  	<!-- input field -->
				<form:input path="shippingRate.maxPrice" cssClass="textfield"/>
				<form:errors path="shippingRate.maxPrice" cssClass="error"/>
			<!-- end input field -->	
	 		</div>
		  	</div>			

		  	<div class="list${classIndex % 2}"><c:set var="classIndex" value="${classIndex+1}" />
		  	<div class="listfl"><fmt:message key='minWeight' />:</div>
		  	<div class="listp">
		  	<!-- input field -->
				<form:input path="shippingRate.minWeight" cssClass="textfield"/>
				<form:errors path="shippingRate.minWeight" cssClass="error"/>
			<!-- end input field -->	
	 		</div>
		  	</div>			

		  	<div class="list${classIndex % 2}"><c:set var="classIndex" value="${classIndex+1}" />
		  	<div class="listfl"><fmt:message key='maxWeight' />:</div>
		  	<div class="listp">
		  	<!-- input field -->
				<form:input path="shippingRate.maxWeight" cssClass="textfield"/>
				<form:errors path="shippingRate.maxWeight" cssClass="error"/>
			<!-- end input field -->	
	 		</div>
		  	</div>			

		  	<div class="list${classIndex % 2}"><c:set var="classIndex" value="${classIndex+1}" />
		  	<div class="listfl"><fmt:message key='handling' />:</div>
		  	<div class="listp">
		  	<!-- input field -->
				<form:input path="shippingRate.handling" cssClass="textfield"/>
				<form:errors path="shippingRate.handling" cssClass="error"/>
			<!-- end input field -->	
	 		</div>
		  	</div>			

		  	<div class="list${classIndex % 2}"><c:set var="classIndex" value="${classIndex+1}" />
		  	<div class="listfl"><fmt:message key='country' />:</div>
		  	<div class="listp">
		  	<!-- input field -->
		  		<form:select path="shippingRate.country">
				<option value="" <c:if test="${'' == customShippingForm.shippingRate.country}">selected</c:if>><fmt:message key='allCountries' /></option>
				<c:forEach items="${countrylist}" var="country">
			  	  <option value="${country.code}" <c:if test="${country.code == customShippingForm.shippingRate.country}">selected</c:if>>${country.name}</option>
				</c:forEach>
				</form:select>
            <!-- end input field -->	
	 		</div>
		  	</div>	
		  	
		  	<c:if test="${gSiteConfig['gPRICE_TABLE'] > 0}">
		  	<div class="list${classIndex % 2}"><c:set var="classIndex" value="${classIndex+1}" />
		  	<div class="listfl"><fmt:message key='customerType' />:</div>
		  	<div class="listp">
		  	<!-- input field -->
		  		<form:select path="shippingRate.customerType">
				  <option value=""><fmt:message key='allCustomers' /></option>
			  	  <option value="0" <c:if test="${0 == customShippingForm.shippingRate.customerType}">selected</c:if>><fmt:message key='regularCustomers' /></option>
			  	  <option value="1" <c:if test="${1 == customShippingForm.shippingRate.customerType}">selected</c:if>><fmt:message key='wholesalers' /></option>
            	</form:select>
            <!-- end input field -->	
	 		</div>
		  	</div>
		  	</c:if>
		  	
		  	<c:if test="${gSiteConfig['gMULTI_STORE'] > 0}">
		  	<div class="list${classIndex % 2}"><c:set var="classIndex" value="${classIndex+1}" />
		  	<div class="listfl"><fmt:message key='multiStore' />:</div>
		  	<div class="listp">
		  	<!-- input field -->
		  		<form:select path="shippingRate.host">
				<c:forEach items="${multiStores}" var="multiStore">
			  	    <option value="${multiStore.host}" <c:if test="${multiStore.host == customShippingForm.shippingRate.host}">selected<c:set var="hostFound" value="true"/></c:if>>${multiStore.host}</option>
				</c:forEach>
			    <c:if test="${hostFound != true and shippingRate.host != ''}">
				    <option value="${shippingRate.host}" selected><c:out value="${shippingRate.host}"/></option>
				</c:if>					  
		       	</form:select>
            <!-- end input field -->	
	 		</div>
		  	</div>
		  	</c:if>
		  	
		  	<div class="list${classIndex % 2}" id="carrierList"><c:set var="classIndex" value="${classIndex+1}" />
		  	<div class="listfl"><fmt:message key='shippingCarrier' />:</div>
		  	<div class="listp">
		  	<!-- input field -->
		  		<form:select path="shippingRate.carrierId">
		  		<option value="" <c:if test="${'' == customShippingForm.shippingRate.carrierId}">selected</c:if>></option>
				<c:forEach items="${carriers}" var="carrier">
			  	  <option value="${carrier.id}" <c:if test="${carrier.id == customShippingForm.shippingRate.carrierId}">selected</c:if>>${carrier.shippingTitle}</option>
				</c:forEach>
				</form:select>
            	<form:errors path="shippingRate.carrierId" cssClass="error"/>
	 	 	<!-- end input field -->	
	 		</div>
		  	</div>
		  	
		  	<div class="list${classIndex % 2}"><c:set var="classIndex" value="${classIndex+1}" />
		  	<div class="listfl"><div class="helpImg"><img class="toolTipImg" title="Note::Description. The following description will show up on frontend next to the custom shipping. " src="../graphics/question.gif" /></div><fmt:message key='description' />:</div>
		  	<div class="listp">
		  	<!-- input field --> 
		  		<form:textarea path="shippingRate.description" cssClass="textfield" rows="15"/>
		  		<form:errors path="shippingRate.description" cssClass="error"/>	
	 		<!-- end input field -->	
	 		</div>
	 		</div>

	<!-- end tab -->        
	</div>
	         
<c:if test="${gSiteConfig['gCUSTOM_SHIPPING_ADVANCED']}">	  
    <h4 title="<fmt:message key="Custom Shipping Form" />"><fmt:message key="advanced" /></h4>
	<div>
	<!-- start tab -->

	  	<div class="listdivi ln tabdivi"></div>
	  	<div class="listdivi"></div>  	
		  	
		  	<c:if test="${gSiteConfig['gGROUP_CUSTOMER']}">
		  	<div class="list${classIndex % 2}"><c:set var="classIndex" value="${classIndex+1}" />
		  	<div class="listfl"><div class="helpImg"><img class="toolTipImg" title="Note::Add Group Id. Separate multiple groups with comma.  Include(OR): If customer belongs to any of these groups, Custom Shipping is available. Exclude(NOR): If customer belongs to any of these groups, Custom Shipping is not available. " src="../graphics/question.gif" /></div><fmt:message key='groupId' />:</div>
		  	<div class="listp">
		  	<!-- input field --> 
		  		<form:textarea path="shippingRate.groupIds" cssClass="textfield" rows="15"/>
		  		<div ><fmt:message key='include' /><form:radiobutton value="include" path="shippingRate.groupType" />
				<fmt:message key='exclude' /><form:radiobutton value="exclude" path="shippingRate.groupType"/></div>
		  		<form:errors path="shippingRate.groupIds" cssClass="error"/>	
	 		<!-- end input field -->	
	 		</div>
	 		</div>
			</c:if>
			
			<div class="list${classIndex % 2}"><c:set var="classIndex" value="${classIndex+1}" />
		  	<div class="listfl"><div class="helpImg"><img class="toolTipImg" title="Note::Add State Codes. Separate multiple States with comma. Include(OR): If customer belongs to any of these states, Custom Shipping is available. Exclude(NOR): If customer belongs to any of these states, Custom Shipping is not available. " src="../graphics/question.gif" /></div><fmt:message key='state' />:</div>
		  	<div class="listp">
		  	<!-- input field --> 
		  		<form:textarea path="shippingRate.states" cssClass="textfield" rows="15"/>
		  		<div ><fmt:message key='include' /><form:radiobutton value="include" path="shippingRate.stateType" />
				<fmt:message key='exclude' /><form:radiobutton value="exclude" path="shippingRate.stateType"/></div>
		  		<form:errors path="shippingRate.states" cssClass="error"/>	
	 		<!-- end input field -->	
	 		</div>
	 		</div>
	 		
	 		<div class="list${classIndex % 2}"><c:set var="classIndex" value="${classIndex+1}" />
		  	<div class="listfl"><fmt:message key='category' />:</div>
		  	<div class="listp">
		  	<!-- input field -->
				<form:input path="shippingRate.categoryId" cssClass="textfield"/>
				<form:errors path="shippingRate.categoryId" cssClass="error"/>
			<!-- end input field -->	
	 		</div>
		  	</div>			

	</div>
<!-- end tabs -->
</c:if> 			
</div>
<!-- start button -->
	<div align="left" class="button">
	<c:choose>
		<c:when test="${customShippingForm.newCustomShipping}">
		  <sec:authorize ifAnyGranted="ROLE_AEM,ROLE_ADMIN_${prop_site_id}">
			<input type="submit" value="<fmt:message key='add' />" /> 
		  </sec:authorize>	
		</c:when>
		<c:otherwise>
		  <sec:authorize ifAnyGranted="ROLE_AEM,ROLE_ADMIN_${prop_site_id},ROLE_PROMOTION_EDIT">
			<input type="submit" value="<fmt:message key='Update' />" /> 
		  </sec:authorize>
		</c:otherwise>
	</c:choose>
	<input type="submit" value="<spring:message code="cancel"/>" name="_cancel">
	</div>	
<!-- end button -->  
  <!-- end table -->
    </td><td class="boxmidr" ></td></tr>
  <tr><td class="botl"></td><td class="botr"></td></tr>
  </table>
  
<!-- end main box -->  
</div>
</form:form>

<script type="text/javascript">
<!--
if ($('shippingRate.ignoreCarriers1').checked) {
	$('carrierList').setStyle('display', 'none');
} else {
	$('carrierList').setStyle('display', 'block');
}	
//-->
</script>

</sec:authorize>
 
  </tiles:putAttribute>    
</tiles:insertDefinition>
