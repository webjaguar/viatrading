<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="/WEB-INF/tlds/spring.tld" prefix="spring" %>
<%@ taglib uri="http://www.springframework.org/security/tags" prefix="sec" %>

<tiles:insertDefinition name="admin.config" flush="true">
  <tiles:putAttribute name="content" type="string">

<sec:authorize ifAnyGranted="ROLE_AEM,ROLE_ADMIN_${prop_site_id},ROLE_SITE_INFO">
<script type="text/javascript" >
<!--
window.addEvent('domready', function(){			
			var tabs1 = new SimpleTabs($('tab-block-1'), { selector: 'h4'});
		});
//-->	
</script> 
<form method="post" action="labels.jhtm" >
<!-- main box -->
  <div id="mboxfull">
  
  <!-- start table -->
  <table cellpadding="0" cellspacing="0" border="0" class="module" >
  <tr>
    <td class="topl_g">
	  
	  <!-- breadcrumb -->
	  <p class="breadcrumb">
	    <a href="../config">SiteInfo</a> &gt;
	    <fmt:message key="labels"/> 
	  </p>
	  
	  <!-- Error Message -->
	  <c:if test="${!empty message}">
		  <div class="message"><spring:message code="${message}" /></div>
	  </c:if>
	  
  </td><td class="topr_g" ></td></tr>
  <tr><td class="boxmidlrg" >

 <!-- tabs -->
 <div id="tab-block-1">
	<h4 title="<fmt:message key="labels"/> "><fmt:message key="labels"/> </h4>
	<div>
	<!-- start tab -->
         <c:set var="classIndex" value="0" />
         
	  	<div class="listdivi ln tabdivi"></div>
	  	<div class="listdivi"></div>
	  	
		    <c:if test="${gSiteConfig['gPRICE_TABLE'] > 0}">

	  		<c:set var="classIndex" value="${classIndex+1}" />
		  			  	
		  	<div class="list${classIndex % 2}">
		  	<div class="listfl"><fmt:message key="productPriceTable" />:</div>
		  	<div class="listp">&nbsp;</div>
		  	</div>
            <c:forEach begin="1" end="${gSiteConfig['gPRICE_TABLE']}" var="priceTable">
            <c:set var="key" value="priceTable${priceTable}" />
            <input type="hidden" name="__labels" value="${key}">
		  	<div class="list${classIndex % 2}">
		  	<div class="listfl">${priceTable}:</div>
		  	<div class="listp">
		  	<!-- input field -->
				<input type="text" class="textfield" name="priceTable${priceTable}" value="<c:out value="${labels[key]}"/>" size="30" maxlength="100"/>
			<!-- end input field -->	
	 		</div>
		  	</div>
		  	</c:forEach>
		  	</c:if>
		  	
		    <c:if test="${gSiteConfig['gPROTECTED'] > 0}">
		    
		    <c:set var="classIndex" value="${classIndex+1}" />
		    
		  	<div class="list${classIndex % 2}">
		  	<div class="listfl"><fmt:message key="protectedLevel" />:</div>
		  	<div class="listp">&nbsp;</div>
		  	</div>
            <c:forEach begin="1" end="${gSiteConfig['gPROTECTED']}" var="protectedL">
            <c:set var="key" value="protected${protectedL}" />
            <input type="hidden" name="__labels" value="${key}">
		  	<div class="list${classIndex % 2}">
		  	<div class="listfl">${protectedL}:</div>
		  	<div class="listp">
		  	<!-- input field -->
				<input type="text" class="textfield" name="protected${protectedL}" value="<c:out value="${labels[key]}"/>" size="30" maxlength="100"/>
			<!-- end input field -->	
	 		</div>
		  	</div>
		  	</c:forEach>
		  	</c:if>		  	

	<!-- end tab -->		
	</div>
<!-- end tabs -->			
</div>
<!-- start button -->
<div align="left" class="button">
    <input type="submit" name="__update" value="<fmt:message key='update' />"  /> 
</div>	
<!-- end button -->	  
  <!-- end table -->
    </td><td class="boxmidr" ></td></tr>
  <tr><td class="botl"></td><td class="botr"></td></tr>
  </table>
  
<!-- end main box -->  
 </div>
</form> 
</sec:authorize>

  </tiles:putAttribute>    
</tiles:insertDefinition>