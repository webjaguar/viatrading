<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="/WEB-INF/tlds/spring.tld" prefix="spring" %>
<%@ taglib uri="http://www.springframework.org/security/tags" prefix="sec" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>

<tiles:insertDefinition name="admin.config" flush="true">
  <tiles:putAttribute name="content" type="string">

<sec:authorize ifAnyGranted="ROLE_AEM,ROLE_ADMIN_${prop_site_id},ROLE_SITE_INFO"> 

<script type="text/javascript" src="<c:url value="/FCKeditor/fckeditor.js"/>"></script>

<script type="text/javascript" >
<!--
window.addEvent('domready', function(){			
			var tabs1 = new SimpleTabs($('tab-block-1'), { selector: 'h4'});
		});
function loadEditor(el) {
  var oFCKeditor = new FCKeditor(el);
  oFCKeditor.BasePath = "<c:url value="/FCKeditor/"/>";
  oFCKeditor.Config["CustomConfigurationsPath"] = '<c:url value="/admin/javascript/fckconfig.js"/>';
  oFCKeditor.ToolbarSet = 'Html';
  oFCKeditor.Width = 650;
  oFCKeditor.Height = 400;
  oFCKeditor.ReplaceTextarea();
  document.getElementById(el + '_link').style.display="none";
}
//-->	
</script> 
<form:form commandName="adminMessage" method="post">
<!-- main box -->
  <div id="mboxfull">
  
  <!-- start table -->
  <table cellpadding="0" cellspacing="0" border="0" class="module" >
  <tr>
    <td class="topl_g">
	  
	  <!-- breadcrumb -->
	  <p class="breadcrumb">
	    <a href="../config"><fmt:message key="configTabTitle"/></a> &gt;
	    <a href="../config/adminMessage.jhtm"><c:out value="${siteConfig['ADMIN_MESSAGE'].value}"/></a> &gt;
	    Form 
	  </p>
	  
	  <!-- Error Message -->
	  <c:if test="${!empty message}">
		  <div class="message"><spring:message code="${message}" /></div>
	  </c:if>
	  
  </td><td class="topr_g" ></td></tr>
  <tr><td class="boxmidlrg" >

 <!-- tabs -->
 <div id="tab-block-1">
	<h4 title="<c:out value="${siteConfig['ADMIN_MESSAGE'].value}"/> Form"><c:out value="${siteConfig['ADMIN_MESSAGE'].value}"/></h4>
	<div>
	<!-- start tab -->
         <c:set var="classIndex" value="0" />
         
	  	<div class="listdivi ln tabdivi"></div>
	  	<div class="listdivi"></div>

		  	<div class="list${classIndex % 2}"><c:set var="classIndex" value="${classIndex+1}" />
		  	<div class="listfl"><div class="requiredField"><fmt:message key='message' />:</div></div>
		  	<div class="listp">
		  	<!-- input field -->
				<table class="form" border="0" cellpadding="0" cellspacing="0"><tr><td>
				<form:textarea id="message" path="message" rows="8" cols="60" htmlEscape="true"/>
				<form:errors path="message" cssClass="error"/>
				<div style="text-align:right" id="message_link"><a href="#" onClick="loadEditor('message')" class="htmleditor">- <fmt:message key="htmlEditor" /> -</a></div>
				</td></tr> </table>
			<!-- end input field -->	
	 		</div>
		  	</div>
		  	
	<!-- end tab -->		
	</div>
<!-- end tabs -->			
</div>
<!-- start button -->
<div align="left" class="button">
	<input type="submit" value="<fmt:message key='Update' />"> 
</div>	
<!-- end button -->	  
  <!-- end table -->
    </td><td class="boxmidr" ></td></tr>
  <tr><td class="botl"></td><td class="botr"></td></tr>
  </table>
  
<!-- end main box -->  
 </div>
</form:form>
</sec:authorize>

  </tiles:putAttribute>    
</tiles:insertDefinition>