<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="/WEB-INF/tlds/spring.tld" prefix="spring" %>
<%@ taglib uri="http://www.springframework.org/security/tags" prefix="sec" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>

<tiles:insertDefinition name="admin.config" flush="true">
  <tiles:putAttribute name="content" type="string">
<sec:authorize ifAnyGranted="ROLE_AEM,ROLE_ADMIN_${prop_site_id}">
<script language="JavaScript"> 
<!--
window.addEvent('domready', function(){			
			var tabs1 = new SimpleTabs($('tab-block-1'), { selector: 'h4'});
		});
//--> 
</script> 
<form:form commandName="changePasswordForm" method="post" >
<!-- main box -->
  <div id="mboxfull">
  
  <!-- start table -->
  <table cellpadding="0" cellspacing="0" border="0" class="module" >
  <tr>
    <td class="topl_g">
	  
	  <!-- breadcrumb -->
	  <p class="breadcrumb">
	    <a href="../config"><fmt:message key="configTabTitle" /></a> &gt;
	    <fmt:message key="change"/> <fmt:message key="password" />
	  </p>
	  
	  <!-- Error Message -->
	  <c:if test="${!empty message}">
	    <div class="message"><spring:message code="${message}" /></div>
	  </c:if>

  </td><td class="topr_g" ></td></tr>
  <tr><td class="boxmidlrg" >

 <!-- tabs -->
 <div id="tab-block-1">
	<h4 title="<fmt:message key="change"/> <fmt:message key="password" /> Form"><fmt:message key="change"/> <fmt:message key="password" /></h4>
	<div>
	<!-- start tab -->
         <c:set var="classIndex" value="0" />
         
	  	<div class="listdivi ln tabdivi"></div>
	  	<div class="listdivi"></div>
	   
		  	<div class="list${classIndex % 2}"><c:set var="classIndex" value="${classIndex+1}" />
		  	<div class="listfl"><fmt:message key='username' />:</div>
		  	<div class="listp">
		  	<!-- input field -->
				<c:out value="${changePasswordForm.user.username}"/>
			<!-- end input field -->	
	 		</div>
		  	</div>	

		  	<div class="list${classIndex % 2}"><c:set var="classIndex" value="${classIndex+1}" />
		  	<div class="listfl"><strong><fmt:message key='currentPassword' />:</strong></div>
		  	<div class="listp">
		  	<!-- input field -->
				<form:password cssClass="textfield" path="currentPassword"/>
				<form:errors path="currentPassword" cssClass="error" />
			<!-- end input field -->	
	 		</div>
		  	</div>	
		  	
		  	<div class="list${classIndex % 2}"><c:set var="classIndex" value="${classIndex+1}" />
		  	<div class="listfl"><strong><fmt:message key='password' />:</strong></div>
		  	<div class="listp">
		  	<!-- input field -->
				<form:password cssClass="textfield" path="user.password"/>
				<form:errors path="user.password" cssClass="error" />
			<!-- end input field -->	
	 		</div>
		  	</div>	
	
		  	<div class="list${classIndex % 2}"><c:set var="classIndex" value="${classIndex+1}" />
		  	<div class="listfl"><strong><fmt:message key='confirmPassword' />:</strong></div>
		  	<div class="listp">
		  	<!-- input field -->
				<form:password cssClass="textfield" path="confirmPassword"/>
				<form:errors path="confirmPassword" cssClass="error" />
			<!-- end input field -->	
	 		</div>
		  	</div>	
	<!-- end tab -->		
	</div>
<!-- end tabs -->			
</div>
<!-- start button -->
<div align="left" class="button">
    <input type="submit" class="red" name="_update" value="<fmt:message key="Update" />">
    <input type="submit" name="_cancel" value="<fmt:message key="cancel" />">
</div>	
<!-- end button -->	  
  <!-- end table -->
    </td><td class="boxmidr" ></td></tr>
  <tr><td class="botl"></td><td class="botr"></td></tr>
  </table>
  
<!-- end main box -->  
 </div>
</form:form>	

</sec:authorize> 
  </tiles:putAttribute>    
</tiles:insertDefinition>
