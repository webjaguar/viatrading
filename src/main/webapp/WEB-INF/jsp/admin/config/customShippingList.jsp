<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ taglib uri="http://www.springframework.org/security/tags" prefix="sec" %>
<%@ taglib uri="/WEB-INF/tlds/spring.tld" prefix="spring" %>

<tiles:insertDefinition name="admin.config.shipping" flush="true">
  <tiles:putAttribute name="content" type="string">

<sec:authorize ifAnyGranted="ROLE_AEM,ROLE_ADMIN_${prop_site_id},ROLE_SITE_INFO"> 
<script type="text/javascript">
<!--
window.addEvent('domready', function(){			
	var tabs1 = new SimpleTabs($('tab-block-1'), { selector: 'h4'});
	var Tips1 = new Tips($$('.toolTipImg'));
});
//-->
</script>   
<form action="customShippingList.jhtm" method="post">
<!-- main box -->
  <div id="mboxfull">
  
  <!-- start table -->
  <table cellpadding="0" cellspacing="0" border="0" class="module" >
  <tr>
    <td class="topl_g">
	  
	  <!-- breadcrumb -->
	  <p class="breadcrumb">
	    <a href="../config">SiteInfo</a> &gt;
	    Custom Shipping
	  </p>
	  
	  <!-- Error Message -->
	  <c:if test="${!empty model.message}">
		  <div class="message"><spring:message code="${model.message}" /></div>
	  </c:if>
	  
	  </td><td class="topr_g" ></td></tr>
  <tr><td class="boxmidlrg" >

<!-- tabs -->
 <div class="tab-wrapper">
	<h4 title=""></h4>
	<div>
	<!-- start tab -->

	  	<div class="listdivi ln tabdivi"></div>
	  	<div class="listdivi"></div>
	   
		  	<div class="listlight">
		  	<!-- input field --> 
		  	<table border="0" cellpadding="0" cellspacing="1" width="100%">
				<tr>
					<td>&nbsp;
					</td>
					<c:if test="${model.customShipping.nrOfElements > 0}">
						<td class="pageShowing">
							<fmt:message key="showing">
								<fmt:param value="${model.customShipping.firstElementOnPage + 1}" />
								<fmt:param value="${model.customShipping.lastElementOnPage + 1}" />
								<fmt:param value="${model.customShipping.nrOfElements}" />
							</fmt:message>
						</td>
					</c:if>
					<td class="pageNavi">
						Page
						<select name="page" id="page" onchange="submit()">
							<c:forEach begin="1" end="${model.customShipping.pageCount}"
								var="page">
								<option value="${page}"
									<c:if test="${page == (model.customShipping.page+1)}">selected</c:if>>
									${page}
								</option>
							</c:forEach>
						</select> 
						of
						<c:out value="${model.customShipping.pageCount}" /> 
						|
						<c:if test="${model.customShipping.firstPage}">
							<span class="pageNaviDead"><fmt:message key="previous" /></span>
						</c:if>
						<c:if test="${not model.customShipping.firstPage}">
							<a
								href="<c:url value="promoList.jhtm"><c:param name="page" value="${model.customShipping.page}"/><c:param name="parent" value="${model.search['parent']}"/><c:param name="size" value="${model.customShipping.pageSize}"/></c:url>"
								class="pageNaviLink"><fmt:message key="previous" /></a>
						</c:if>
						|
						<c:if test="${model.customShipping.lastPage}">
							<span class="pageNaviDead"><fmt:message key="next" /></span>
						</c:if>
						<c:if test="${not model.customShipping.lastPage}">
							<a
								href="<c:url value="promoList.jhtm"><c:param name="page" value="${model.customShipping.page+2}"/><c:param name="parent" value="${model.search['parent']}"/><c:param name="size" value="${model.customShipping.pageSize}"/></c:url>"
								class="pageNaviLink"><fmt:message key="next" /></a>
						</c:if>
					</td>
				</tr>
			</table>

			<table border="0" cellpadding="0" cellspacing="0" width="100%"
				class="listings">
				<tr class="listingsHdr2">

					<td class="indexCol">&nbsp;</td>
					<td class="listingsHdr3">
						<fmt:message key="internal" />
					</td>
					<td class="listingsHdr3">
						<fmt:message key="shippingTitle" />
					</td>
					<td class="listingsHdr3">
						<fmt:message key="cost" />
					</td>
					<td class="listingsHdr3">
						<fmt:message key="minPrice" />
					</td>
					<td class="listingsHdr3">
						<fmt:message key="maxPrice" />
					</td>
					<c:if test="${gSiteConfig['gMULTI_STORE'] > 0}">
		  			<td class="listingsHdr3">
						<fmt:message key="multiStore" />
					</td>
					</c:if>
				</tr>
			 	<c:forEach items="${model.customShipping.pageList}" var="customShipping"	varStatus="status">
					<tr class="row${status.index % 2}"	onmouseover="changeStyleClass(this,'rowOver')" 	onmouseout="changeStyleClass(this,'row${status.index % 2}')">
						<td class="indexCol"><c:out value="${status.count + model.customShipping.firstElementOnPage}" />.</td>
						<td class="nameCol">
							<c:choose>
								<c:when test="${customShipping.internal == true}"><input type="checkbox" checked="checked" readonly="readonly" disabled="disabled"/></c:when>
								<c:when test="${customShipping.internal == false}"><input type="checkbox" readonly="readonly" disabled="disabled"/></c:when>
							</c:choose>
						</td>
						<td class="nameCol">
							<a href="customShipping.jhtm?id=${customShipping.id}"><img border="0" src="../graphics/edit.gif" align="left"/></a>
							<a href="customShipping.jhtm?id=${customShipping.id}" class="nameLink"><c:out value="${customShipping.title}" escapeXml="false" /></a>
						</td>
						<td class="nameCol">
							<c:if test="${customShipping.price != null}">
								<c:choose>
									<c:when test="${customShipping.percent == 0}">$<c:out value="${customShipping.price}" /></c:when>
									<c:when test="${customShipping.percent == 1}"><c:out value="${customShipping.price}" />% Off Shipping Carrier</c:when>
									<c:when test="${customShipping.percent == 2}">$<c:out value="${customShipping.price}" />% Off Subtotal</c:when>
								</c:choose>
							</c:if>
						</td>
						<td class="nameCol">
							<c:if test="${customShipping.minPrice != null}">$</c:if><c:out value="${customShipping.minPrice}" />
						</td>
						<td class="nameCol">
							<c:if test="${customShipping.maxPrice != null}">$</c:if><c:out value="${customShipping.maxPrice}" />
						</td>
						<c:if test="${gSiteConfig['gMULTI_STORE'] > 0}">
		  				<td class="nameCol">
							<c:if test="${customShipping.host != null}"></c:if><c:out value="${customShipping.host}" />
						</td>
						</c:if>
					</tr>
				</c:forEach> 
				<c:if test="${model.customShipping.nrOfElements == 0}">
					<tr class="emptyList">
						<td colspan="8">
							&nbsp;
						</td>
					</tr>
				</c:if>
			</table>
			<table border="0" cellpadding="0" cellspacing="1" width="100%">
				<tr>
				  <td class="pageSize">
					<select name="size"
						onchange="document.getElementById('page').value=1;submit()">
						<c:forTokens items="10,25,50" delims="," var="current">
							<option value="${current}"
								<c:if test="${current == customShippingSearch.pageSize}">selected</c:if>>
								${current}
								<fmt:message key="perPage" />
							</option>
						</c:forTokens>
					</select>
				  </td>
				</tr>
			</table>
		  	
		  	<!-- end input field -->  	  
		  	</div>
		  	
		  	<!-- start button -->
		  	<div align="left" class="button">
 			  <c:choose>
				<c:when test="${siteConfig['CUSTOM_SHIPPING_LIMIT'].value > model.nOfcustomShipping}">
				  <sec:authorize ifAnyGranted="ROLE_AEM,ROLE_ADMIN_${prop_site_id}">
					<input type="submit" name="__add" value="<fmt:message key="add" />"/>
				  </sec:authorize>	
				</c:when>
				<c:otherwise>
				  <div class="note_list">
					<fmt:message key="toAddMoreCustomShippingContactWebMaster" />
				  </div>	
				</c:otherwise>
			</c:choose>
			</div>
		  	<!-- end button -->	
	
	  	
	<!-- end tab -->        
	</div>
<!-- end tabs -->			
</div>
  
  <!-- end table -->
   </td><td class="boxmidr" ></td></tr>
  <tr><td class="botl"></td><td class="botr"></td></tr>
  </table>
  
<!-- end main box -->  
 </div>  
</form>

</sec:authorize>

</tiles:putAttribute>    
</tiles:insertDefinition>
