<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="/WEB-INF/tlds/spring.tld" prefix="spring" %>
<%@ taglib uri="http://www.springframework.org/security/tags" prefix="sec" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>

<tiles:insertDefinition name="admin.config" flush="true">
  <tiles:putAttribute name="content" type="string">

<sec:authorize ifAnyGranted="ROLE_AEM,ROLE_ADMIN_${prop_site_id},ROLE_SITE_INFO"> 
<script type="text/javascript" src="<c:url value="/FCKeditor/fckeditor.js"/>"></script>
<script type="text/javascript" >
<!--
window.addEvent('domready', function(){			
	var tabs1 = new SimpleTabs($('tab-block-1'), { selector: 'h4'});
});
function insertElement() {
	var insert = document.getElementById("insert").value;
	if (insert == "") {
	  alert("Please choose an element");
	} else {
	  document.getElementById("message").value=document.getElementById("message").value+insert;
	}
}
function loadEditor(el) {
  var oFCKeditor = new FCKeditor(el);
  oFCKeditor.BasePath = "<c:url value="/FCKeditor/"/>";
  oFCKeditor.Config["CustomConfigurationsPath"] = '<c:url value="/admin/javascript/fckconfig.js"/>';
  oFCKeditor.ToolbarSet = 'Html';
  oFCKeditor.Width = 650;
  oFCKeditor.Height = 400;
  oFCKeditor.ReplaceTextarea();
  document.getElementById(el + '_link').style.display="none";
}
//-->	
</script> 
<form:form id="siteMessageForm" commandName="siteMessageForm" method="post" >
<!-- main box -->
  <div id="mboxfull">
  
  <!-- start table -->
  <table cellpadding="0" cellspacing="0" border="0" class="module" >
  <tr>
    <td class="topl_g">
	  
	  <!-- breadcrumb -->
	  <p class="breadcrumb">
	    <a href="../config"><fmt:message key="configTabTitle"/></a> &gt;
	    <a href="../config/siteMessage.jhtm"><fmt:message key="siteMessageTitle"/></a> &gt;
	    form 
	  </p>
	  
	  <!-- Error Message -->
	  <c:if test="${!empty message}">
		  <div class="message"><spring:message code="${message}" /></div>
	  </c:if>
	  
  </td><td class="topr_g" ></td></tr>
  <tr><td class="boxmidlrg" >

 <!-- tabs -->
 <div id="tab-block-1">
	<h4 title="<fmt:message key="siteMessageTitle"/> Form"><fmt:message key="siteMessageTitle"/></h4>
	<div>
	<!-- start tab -->
         <c:set var="classIndex" value="0" />
         
	  	<div class="listdivi ln tabdivi"></div>
	  	<div class="listdivi"></div>

		  	<div class="list${classIndex % 2}"><c:set var="classIndex" value="${classIndex+1}" />
		  	<div class="listfl">Html:</div>
		  	<div class="listp">
		  	<!-- input field -->
				<form:checkbox path="siteMessage.html"/>
				<form:errors path="siteMessage.html" cssClass="error"/>
			<!-- end input field -->	
	 		</div>
		  	</div>
		  	
		  	<div class="list${classIndex % 2}"><c:set var="classIndex" value="${classIndex+1}" />
		  	<div class="listfl"><div class="requiredField"><fmt:message key='messageName' />:</div></div>
		  	<div class="listp">
		  	<!-- input field -->
				<form:input path="siteMessage.messageName" cssClass="textfield" size="30" maxlength="30" htmlEscape="true"/>
				<form:errors path="siteMessage.messageName" cssClass="error"/>
			<!-- end input field -->	
	 		</div>
		  	</div>
		  	
		  	<div class="list${classIndex % 2}"><c:set var="classIndex" value="${classIndex+1}" />
		  	<div class="listfl"><div class="requiredField"><fmt:message key='subject' />:</div></div>
		  	<div class="listp">
		  	<!-- input field -->
				<form:input path="siteMessage.subject" cssClass="textfield" size="50" maxlength="80" htmlEscape="true"/>
				<form:errors path="siteMessage.subject" cssClass="error"/>
			<!-- end input field -->	
	 		</div>
		  	</div>
		  	
		  	<div class="list${classIndex % 2}"><c:set var="classIndex" value="${classIndex+1}" />
		  	<div class="listfl"><fmt:message key='sourceURL' />:</div>
		  	<div class="listp">
		  	<!-- input field -->
				<form:input path="siteMessage.sourceUrl" cssClass="textfield" size="50" maxlength="80" htmlEscape="true"/>
				<form:errors path="siteMessage.sourceUrl" cssClass="error"/>
			<!-- end input field -->	
	 		</div>
		  	</div>

		  	<div class="list${classIndex % 2}"><c:set var="classIndex" value="${classIndex+1}" />
		  	<div class="listfl">Insert into Message:</div>
		  	<div class="listp">
		  	<!-- input field -->
				<select id="insert" size="1">
				  <option value=""><fmt:message key='selectElement' /></option>
				  <option value="#firstname#"><fmt:message key='firstName' /></option>
				  <option value="#lastname#"><fmt:message key="lastName" /></option>
				  <option value="#email#"><fmt:message key="emailAddress" /></option>
				  <option value="#password#"><fmt:message key="password" /></option>
				  <option value="#order#"><fmt:message key="orderNumber" /></option>
				  <option value="#status#"><fmt:message key="status" /></option>
				  <option value="#tracknum#"><fmt:message key="trackNum" /></option>
				  <option value="#orderlink#"><fmt:message key="orderLink" /></option>
				  <option value="#packinglistnumber#"><fmt:message key="packinglistnumber" /></option>
				  <option value="#itemDesc#"><fmt:message key="quote" /> <fmt:message key="item" /> <fmt:message key="description" /></option>
				  <c:if test="${siteConfig['DELIVERY_TIME'].value == 'true'}" >
				    <option value="#orderduedate#"><fmt:message key="orderDueDate" /></option>
				  </c:if>
				  <c:if test="${fn:contains(siteConfig['USER_EXPECTED_DELIVERY_TIME'].value, 'true')}" >
				    <option value="#orderuserduedate#"><fmt:message key="userExpectedDueDate" /></option>
				  </c:if>
				  <c:if test="${gSiteConfig['gINVENTORY']}">
				    <option value="#ponumber#"><fmt:message key="poNumber" /></option>
				  </c:if>
				  <c:if test="${gSiteConfig['gSALES_REP']}">
				    <option value="#salesRepName#"><fmt:message key="salesRep" /> <fmt:message key="Name" /></option>
				    <option value="#salesRepEmail#"><fmt:message key="salesRep" /> <fmt:message key="emailAddress" /></option>
				    <option value="#salesRepPhone#"><fmt:message key="salesRep" /> <fmt:message key="phone" /></option>
				    <option value="#salesRepCellPhone#"><fmt:message key="salesRep" /> <fmt:message key="cellPhone" /></option>
				  </c:if>
				  <c:if test="${siteConfig['CUSTOM_SHIPPING_CONTACT_INFO'].value == 'true'}">
				    <option value="#contactName#"><fmt:message key="customShippingContactName" /></option>
				    <option value="#contactCompany#"><fmt:message key="customShippingContactCompany" /></option>
				    <option value="#contactEmail#"><fmt:message key="customShippingContactEmail" /></option>
				    <option value="#contactPhone1#"><fmt:message key="customShippingContactPhone1" /></option>
				    <option value="#contactPhone2#"><fmt:message key="customShippingContactPhone2" /></option>
				    <option value="#contactWebsite#"><fmt:message key="customShippingContactWebSite" /></option>
				  </c:if>
				  <c:if test="${gSiteConfig['gGIFTCARD']}">
				  	<option value="#giftCard#"><fmt:message key="giftCard" /></option>
				    <option value="#giftCardAmount#"><fmt:message key="giftCardAmount" /></option>
				    <option value="#giftCardRecipient#"><fmt:message key="giftCardRecipientName" /></option>
				    <option value="#giftCardSender#"><fmt:message key="giftCardSenderName" /></option>
				    <option value="#giftCardMessage#"><fmt:message key="giftCardMessage" /></option>
				    <option value="#giftCardCode#"><fmt:message key="giftCardCode" /></option>
				  </c:if>
			  	  <c:if test="${gSiteConfig['gCUSTOMER_SUPPLIER']}">
			  	  	<option value="#sku#"><fmt:message key="supplierSku" /></option>
			  	  	<option value="#name#"><fmt:message key="supplierSku" /><fmt:message key="name"/></option>
			  	    <option value="#qty#"><fmt:message key="supplierQuantity" /></option>
			  	    <option value="#consignmentAmount#"><fmt:message key="supplierCommission" /></option>
			  	    <option value="#netRecoveryPerUnit#"><fmt:message key="netRecoveryPerUnit" /></option>
				  </c:if>
				  <option value="#dynamicPromo#"><fmt:message key="dynamicPromo"/></option>
				  <option value="#percentage,1#"><fmt:message key="percentage"/></option>
				  <option value="#fixed#"><fmt:message key="fixed"/></option>
				  <option value="#amount,1#"><fmt:message key="amount"/></option>
				  <option value="#delta,1#"><fmt:message key="delta"/></option>
				  <option value="#hours,1#"><fmt:message key="hours"/></option>
				  <option value="#paymentUrl#"><fmt:message key="paymentUrl"/></option>
				  <option value="#DOC_BSUSRC#"><fmt:message key="DOC_BSUSRC"/></option>
				  <option value="#DOC_BSUSRC_SPA#"><fmt:message key="DOC_BSUSRC_SPA"/></option>
				  <option value="#DOC_CRC#"><fmt:message key="DOC_CRC"/></option>
				  <option value="#DOC_CRC_SPA#"><fmt:message key="DOC_CRC_SPA"/></option>
				  <option value="#DOC_MPA#"><fmt:message key="DOC_MPA"/></option>
				  <option value="#DOC_MAP_SPA#"><fmt:message key="DOC_MAP_SPA"/></option>
				  <option value="#crmcontactid#">Crm Contact Id</option>
				</select>
				<input type="button" value="insert" onclick="insertElement()"/>
			<!-- end input field -->	
	 		</div>
		  	</div>

		  	<div class="list${classIndex % 2}"><c:set var="classIndex" value="${classIndex+1}" />
		  	<div class="listfl"><div class="requiredField"><fmt:message key='message' />:</div></div>
		  	<div class="listp">
		  	<!-- input field -->
				<form:textarea id="message" path="siteMessage.message" cssClass="textArea700x400" rows="8" cols="60" htmlEscape="true"/>
				<div style="text-align:left" id="siteMessage.message_link"><a href="#" onClick="loadEditor('siteMessage.message')" class="htmleditor">- <fmt:message key="htmlEditor" /> -</a></div>
				<form:errors path="siteMessage.message" cssClass="error"/>
			<!-- end input field -->	
	 		</div>
		  	</div>
		  	<div class="list${classIndex % 2}"><c:set var="classIndex" value="${classIndex+1}" />
		  	<div class="listfl"><fmt:message key="inGroup" />:</div>
		  	<div class="listp">
		  	<!-- input field -->
	    		<form:select path="siteMessage.groupIdList" multiple="true">
		          <form:option value="" label="Please Select"></form:option>
		          <c:forEach items="${groupList}" var="group">
		            <form:option value="${group.id}" label="${group.name}"></form:option>
		          </c:forEach>      
		        </form:select>
		    <!-- end input field -->	
	 		</div>
		  	</div>
		  	
	<!-- end tab -->		
	</div>
<!-- end tabs -->			
</div>
<!-- start button -->
<div align="left" class="button">
	<c:choose>
		<c:when test="${siteMessageForm.newSiteMessage}">
			<input type="submit" value="<fmt:message key='add' />" /> 
		</c:when>
		<c:otherwise>
			<input type="submit" name="update_siteMessage" value="<fmt:message key='Update' />" /> 
			<input type="submit" name="delete_siteMessage" value="<fmt:message key='delete' />" /> 
		</c:otherwise>
	</c:choose>
	<input type="submit" value="<spring:message code="cancel"/>" name="_cancel">
</div>	
<!-- end button -->	  
  <!-- end table -->
    </td><td class="boxmidr" ></td></tr>
  <tr><td class="botl"></td><td class="botr"></td></tr>
  </table>
  
<!-- end main box -->  
 </div>
</form:form>
</sec:authorize>

  </tiles:putAttribute>    
</tiles:insertDefinition>