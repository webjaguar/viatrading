<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="/WEB-INF/tlds/spring.tld" prefix="spring" %>
<%@ taglib uri="http://www.springframework.org/security/tags" prefix="sec" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>

<tiles:insertDefinition name="admin.config.shipping" flush="true">
  <tiles:putAttribute name="content" type="string">
  
<sec:authorize ifAnyGranted="ROLE_AEM,ROLE_ADMIN_${prop_site_id},ROLE_SITE_INFO">  
<script type="text/javascript"> 
<!--
window.addEvent('domready', function(){			
			var tabs1 = new SimpleTabs($('tab-block-1'), { selector: 'h4'});
			var Tips1 = new Tips($$('.toolTipImg'));
		});
<c:if test="${empty view}">
function toggleNoneFlat(id)
{
      if (document.getElementById(id).value == 'flt') 
	  {
	    document.getElementById('shippingTitle').style.display="block";
	    document.getElementById('handlingBox').style.display="none";
	    document.getElementById('shippingBase').style.display="block";
	    document.getElementById('shippingInc').style.display="none";
	    document.getElementById('shippingHandling.priceLimit').style.display="block";
	    document.getElementById('zeroWeight').style.display="none";
	    <c:forEach items="${shippingHandlingForm.customShippingRates}" var="shipRate">
	    document.getElementById('__customCarriedId_${shipRate.id}').style.display="none";
	    </c:forEach>
	  }
	  else if ( document.getElementById(id).value == 'wgt')
	  {
	    document.getElementById('shippingTitle').style.display="block";
	    document.getElementById('handlingBox').style.display="block";
	    document.getElementById('shippingBase').style.display="block";
	    document.getElementById('shippingInc').style.display="block";
	    document.getElementById('shippingHandling.priceLimit').style.display="block";
	    document.getElementById('zeroWeight').style.display="none";
	    <c:forEach items="${shippingHandlingForm.customShippingRates}" var="shipRate">
	    document.getElementById('__customCarriedId_${shipRate.id}').style.display="none";
	    </c:forEach>
	  }
	  else if ( document.getElementById(id).value == 'prc')
	  {
	    document.getElementById('shippingTitle').style.display="block";
	    document.getElementById('handlingBox').style.display="block";
	    document.getElementById('shippingBase').style.display="block";
	    document.getElementById('shippingInc').style.display="block";
	    document.getElementById('shippingHandling.priceLimit').style.display="block";
	    document.getElementById('zeroWeight').style.display="none";
	    <c:forEach items="${shippingHandlingForm.customShippingRates}" var="shipRate">
	    document.getElementById('__customCarriedId_${shipRate.id}').style.display="none";
	    </c:forEach>
	  }
	  else if ( document.getElementById(id).value == 'crr')
	  {
	    document.getElementById('shippingTitle').style.display="none";	    
	    document.getElementById('handlingBox').style.display="block";
	    document.getElementById('shippingBase').style.display="none";
	    document.getElementById('shippingInc').style.display="none";
	    document.getElementById('shippingHandling.priceLimit').style.display="block";
	    document.getElementById('zeroWeight').style.display="block";
	    <c:forEach items="${shippingHandlingForm.customShippingRates}" var="shipRate">
	    document.getElementById('__customCarriedId_${shipRate.id}').style.display="block";
	    </c:forEach>
	  }
}
</c:if>
//-->
</script>
<form:form id="shippingHandlingForm" commandName="shippingHandlingForm" method="post" >
<!-- main box -->
  <div id="mboxfull">
  
  <!-- start table -->
  <table cellpadding="0" cellspacing="0" border="0" class="module" >
  <tr>
    <td class="topl_g">
	  
	  <!-- breadcrumb -->
	  <p class="breadcrumb">
	    <a href="../config">SiteInfo</a> &gt;
	    <c:choose>
	     <c:when test="${empty view}"><a href="../config/siteMessage.jhtm"><fmt:message key="shippingHandling"/></a> &gt;</c:when>
	     <c:otherwise><a href="../config/siteMessage.jhtm">TBD</a> &gt;</c:otherwise>
	    </c:choose>
	    form 
	  </p>
	  
	  <!-- Error Message -->
	  <c:if test="${!empty message}">
		  <div class="message"><spring:message code="${message}" /></div>
	  </c:if>

	  
  </td><td class="topr_g" ></td></tr>
  <tr><td class="boxmidlrg" >

 <!-- tabs -->
 <div id="tab-block-1">
    <c:choose>
	 <c:when test="${empty view}"><h4 title="<fmt:message key="shippingHandling"/> Form"><fmt:message key="shippingHandling"/></h4></c:when>
	 <c:otherwise><h4 title="TBD Form">TBD</h4></c:otherwise>
	</c:choose>
	
	<div>
	<!-- start tab -->
         <c:set var="classIndex" value="0" />
         
	  	<div class="listdivi ln tabdivi"></div>
	  	<div class="listdivi"></div>
		<c:choose>
		 <c:when test="${empty view}">
		  	<div class="list${classIndex % 2}"><c:set var="classIndex" value="${classIndex+1}" />
		  	<div class="listfl"><fmt:message key='priceLimit' />:</div>
		  	<div class="listp">
		  	<!-- input field -->
				<form:input path="shippingHandling.priceLimit" cssClass="textfield"/>
				<form:errors path="shippingHandling.priceLimit" cssClass="error"/> <br />
			<!-- end input field -->	
	 		</div>
		  	</div>

		  	<div class="list${classIndex % 2}"><c:set var="classIndex" value="${classIndex+1}" />
		  	<div class="listfl"><fmt:message key='newPriceCost'/>:</div>
		  	<div class="listp">
		  	<!-- input field -->
				<form:input path="shippingHandling.newPriceCost" cssClass="textfield"/>
				<form:errors path="shippingHandling.newPriceCost" cssClass="error"/><br>
			<!-- end input field -->	
	 		</div>
		  	</div>

		  	<div class="list${classIndex % 2}"><c:set var="classIndex" value="${classIndex+1}" />
		  	<div class="listfl"><fmt:message key='newShippingTitle'/>:</div>
		  	<div class="listp">
		  	<!-- input field -->
				<form:input path="shippingHandling.newShippingTitle" cssClass="textfield"/>
				<span class="helpNote"><p>If SubTotal &gt; Price Limit Then Shipping = New price Cost</p></span>
			<!-- end input field -->	
	 		</div>
		  	</div>

		  	<div class="list${classIndex % 2}"><c:set var="classIndex" value="${classIndex+1}" />
		  	<div class="listfl">Select:</div>
		  	<div class="listp">
		  	<!-- input field -->
			    <hr>
				<form:radiobutton path="shippingHandling.shippingMethod" value="flt" id="flat"  onclick="toggleNoneFlat(this.id)"/><fmt:message key='flatprice' />
			    <form:radiobutton path="shippingHandling.shippingMethod" value="wgt" id="baseonweight" onclick="toggleNoneFlat(this.id)"/><fmt:message key='baseOnWeight' />
			    <form:radiobutton path="shippingHandling.shippingMethod" value="prc" id="beseonprice"  onclick="toggleNoneFlat(this.id)"/><fmt:message key='baseOnPrice' />
			    <form:radiobutton path="shippingHandling.shippingMethod" value="crr" id="baseoncarrier"   onclick="toggleNoneFlat(this.id)"/><fmt:message key='baseOnCarrier'   />
			<!-- end input field -->	
	 		</div>
		  	</div>

		  	<div id="zeroWeight"  class="list${classIndex % 2}"><c:set var="classIndex" value="${classIndex+1}" />
		  	<div class="listfl">If total weight is zero or less:</div>
		  	<div class="listp">
		  	<!-- input field -->
			  <form:input path="shippingHandling.zeroWeightNewShippingTitle" cssClass="textfield"/> (<fmt:message key='newShippingTitle'/>)
			  <form:errors path="shippingHandling.zeroWeightNewShippingTitle" cssClass="error"/>
			  <br/>
			  <form:input path="shippingHandling.zeroWeightNewPriceCost" cssClass="textfield"/> (<fmt:message key='newPriceCost'/>)
			  <form:errors path="shippingHandling.zeroWeightNewPriceCost" cssClass="error"/>
			  <span class="helpNote"><p>If <fmt:message key='newShippingTitle'/> and <fmt:message key='newPriceCost'/> are both empty then total weight adjusts to 1.</p></span>
			<!-- end input field -->	
	 		</div>
		  	</div>

		  	<div id="shippingTitle"  class="list${classIndex % 2}"><c:set var="classIndex" value="${classIndex+1}" />
		  	<div class="listfl"><fmt:message key='shippingTitle' />:</div>
		  	<div class="listp">
		  	<!-- input field -->
			  <form:input path="shippingHandling.shippingTitle" cssClass="textfield"/>
			  <form:errors path="shippingHandling.shippingTitle" cssClass="error"/>
			<!-- end input field -->	
	 		</div>
		  	</div>
	
		  	<div id="shippingBase" class="list${classIndex % 2}"><c:set var="classIndex" value="${classIndex+1}" />
		  	<div class="listfl"><fmt:message key='shippingBase' />:</div>
		  	<div class="listp">
		  	<!-- input field -->
			  <form:input path="shippingHandling.shippingBase" cssClass="textfield"/>
			  <form:errors path="shippingHandling.shippingBase" cssClass="error"/>
			<!-- end input field -->	
	 		</div>
		  	</div>

		  	<div id="shippingInc" class="list${classIndex % 2}"><c:set var="classIndex" value="${classIndex+1}" />
		  	<div class="listfl"><fmt:message key='shippingIncremental' />:</div>
		  	<div class="listp">
		  	<!-- input field -->
			    <form:input path="shippingHandling.shippingIncremental" cssClass="textfield"/>
			    <form:errors path="shippingHandling.shippingIncremental" cssClass="error"/>
			<!-- end input field -->	
	 		</div>
		  	</div>

			<div id="handlingBox" >
		  	<div class="list${classIndex % 2}"><c:set var="classIndex" value="${classIndex+1}" />
		  	<div class="listfl"><fmt:message key='handling' />:</div>
		  	<div class="listp">
		  	<!-- input field -->
			   <form:radiobutton path="shippingHandling.handlingType" value="weight"  onclick="toggleNoneFlat(this.id)"/>base on weight<br />
			   <form:radiobutton path="shippingHandling.handlingType" value="price"  onclick="toggleNoneFlat(this.id)"/>base on price
			<!-- end input field -->	
	 		</div>
		  	</div>

		  	<div class="list${classIndex % 2}"><c:set var="classIndex" value="${classIndex+1}" />
		  	<div class="listfl"><fmt:message key='handlingBase' />:</div>
		  	<div class="listp">
		  	<!-- input field -->
			   <form:input path="shippingHandling.handlingBase" cssClass="textfield"/>
			   <form:errors path="shippingHandling.handlingBase" cssClass="error"/>
			<!-- end input field -->	
	 		</div>
		  	</div>

		  	<div class="list${classIndex % 2}"><c:set var="classIndex" value="${classIndex+1}" />
		  	<div class="listfl"><fmt:message key='handlingIncremental' />:</div>
		  	<div class="listp">
		  	<!-- input field -->
			   <form:input path="shippingHandling.handlingIncremental" cssClass="textfield"/>
			   <form:errors path="shippingHandling.handlingIncremental" cssClass="error"/>
			<!-- end input field -->	
	 		</div>
		  	</div>
		  	</div>
		 </c:when>
		 <c:otherwise>
 		  	<div class="list${classIndex % 2}"><c:set var="classIndex" value="${classIndex+1}" />
		  	<div class="listfl"><div class="helpImg"><img class="toolTipImg" title="Notice:: Enabling TBD would disable Carriers and Shipping & Handling." src="../graphics/question.gif" /></div><fmt:message key='tbdEnable' />:</div>
		  	<div class="listp">
		  	<!-- input field -->
			    <form:checkbox path="shippingHandling.tbdEnable"/>
			    <form:errors path="shippingHandling.tbdEnable" cssClass="error"/>
			<!-- end input field -->	
	 		</div>
		  	</div>
 
 		  	<div class="list${classIndex % 2}"><c:set var="classIndex" value="${classIndex+1}" />
		  	<div class="listfl"><fmt:message key='tbdTitle' />:</div>
		  	<div class="listp">
		  	<!-- input field -->
			    <form:input path="shippingHandling.tbdTitle" cssClass="textfield"/>
			    <form:errors path="shippingHandling.tbdTitle" cssClass="error"/>
			<!-- end input field -->	
	 		</div>
		  	</div>
 </c:otherwise>
</c:choose>	 	  	
	<!-- end tab -->		
	</div>
<!-- end tabs -->			
</div>
<!-- start button -->
<div align="left" class="button">
   <input type="submit" name="__update" value="<fmt:message key="Update" />">
</div>	
<!-- end button -->  
  <!-- end table -->
    </td><td class="boxmidr" ></td></tr>
  <tr><td class="botl"></td><td class="botr"></td></tr>
  </table>
  
<!-- end main box -->  
</div>
</form:form>

<c:if test="${empty view}">
<script type="text/javascript"> 
<!--
	  if (document.getElementById('flat').checked) 
	  {
	    document.getElementById('shippingTitle').style.display="block";
	    document.getElementById('shippingBase').style.display="block";
	    document.getElementById('handlingBox').style.display="none";
	    document.getElementById('shippingHandling.priceLimit').style.display="block";
	    document.getElementById('shippingInc').style.display="none";
	    document.getElementById('zeroWeight').style.display="none";
	    <c:forEach items="${shippingHandlingForm.customShippingRates}" var="shipRate">
	    document.getElementById('__customCarriedId_${shipRate.id}').style.display="none";
	    </c:forEach>	    
	  } 
	  else if ( document.getElementById('baseonweight').checked)
	  {
	    document.getElementById('shippingTitle').style.display="block";
	    document.getElementById('handlingBox').style.display="block";
	    document.getElementById('shippingBase').style.display="block";
	    document.getElementById('shippingInc').style.display="block";
	    document.getElementById('shippingHandling.priceLimit').style.display="block";
	    document.getElementById('zeroWeight').style.display="none";
	    <c:forEach items="${shippingHandlingForm.customShippingRates}" var="shipRate">
	    document.getElementById('__customCarriedId_${shipRate.id}').style.display="none";
	    </c:forEach>
	  }
	  else if ( document.getElementById('beseonprice').checked)
	  { 
	    document.getElementById('shippingTitle').style.display="block";
	    document.getElementById('handlingBox').style.display="block";
	    document.getElementById('shippingBase').style.display="block";
	    document.getElementById('shippingInc').style.display="block";
	    document.getElementById('shippingHandling.priceLimit').style.display="block";
	    document.getElementById('zeroWeight').style.display="none";
	    <c:forEach items="${shippingHandlingForm.customShippingRates}" var="shipRate">
	    document.getElementById('__customCarriedId_${shipRate.id}').style.display="none";
	    </c:forEach>
	  }
	  else if ( document.getElementById('baseoncarrier').checked)
	  {
	    document.getElementById('shippingTitle').style.display="none";	 
	    document.getElementById('shippingHandling.priceLimit').style.display="block";
	    document.getElementById('handlingBox').style.display="block";
	    document.getElementById('shippingBase').style.display="none";
	    document.getElementById('shippingInc').style.display="none";
	    document.getElementById('zeroWeight').style.display="block";
	    <c:forEach items="${shippingHandlingForm.customShippingRates}" var="shipRate">
	    document.getElementById('__customCarriedId_${shipRate.id}').style.display="block";
	    </c:forEach>
	  }

//-->
</script>
</c:if>	
</sec:authorize>
 
  </tiles:putAttribute>    
</tiles:insertDefinition>
