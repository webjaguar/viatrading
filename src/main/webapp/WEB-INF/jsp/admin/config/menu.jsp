<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/security/tags" prefix="sec" %>

<c:if test="${gSiteConfig['gSHOPPING_CART']}">
<script src="../javascript/MenuMatic_0.68.3.js" type="text/javascript" charset="utf-8"></script>
<script type="text/javascript" >
window.addEvent('domready', function() {			
	var myMenu = new MenuMatic({ orientation:'vertical' });			
});	

function submitSearchForm(fileType) {
	if (typeof document.forms['searchform'] != 'undefined') {
	  document.searchform.action = "customerExport.jhtm?fileType2="+fileType;
	  document.searchform.submit();
	} else {
	  myform=document.createElement("form");
	  document.body.appendChild(myform);
      myform.method = "POST";
      myform.action= "customerExport.jhtm?fileType2="+fileType;
      myform.submit();
	}
}
</script>
</c:if>


  <div id="lbox">
   <table cellpadding="0" cellspacing="0" border="0" class="module" style="width: 100%"><tr><td class="topl">
    </td><td class="topr" ></td></tr><tr><td class="boxmid" >
    <h2 class="menuleft mfirst"><fmt:message key="siteConfigurationTitle"/></h2>
    <div class="menudiv"></div> 
    <sec:authorize ifAnyGranted="ROLE_AEM,ROLE_ADMIN_${prop_site_id}">
    <a href="changePassword.jhtm" class="userbutton"><fmt:message key="changePasswordTitle"/></a>
    </sec:authorize>
    <sec:authorize ifAnyGranted="ROLE_AEM,ROLE_ADMIN_${prop_site_id},ROLE_SITE_INFO">
    <a href="configuration.jhtm" class="userbutton"><fmt:message key="siteConfigurationTitle"/></a>
    <a href="countryList.jhtm" class="userbutton"><fmt:message key="countryMenuTitle"/></a>
    <c:if test="${gSiteConfig['gSHOPPING_CART']}">	  
    <a href="stateList.jhtm" class="userbutton"><fmt:message key="stateMenuTitle"/></a>
    <c:if test="${gSiteConfig['gPAYPAL'] < 2}">
    <a href="paymentMethod.jhtm" class="userbutton"><fmt:message key="paymentMethod"/></a>
    </c:if>
    </c:if>
    <a href="siteMessage.jhtm" class="userbutton"><fmt:message key="siteMessageTitle"/></a>
    <a href="siteMessageGroup.jhtm" class="userbutton"><fmt:message key="siteMessageGroupTitle"/></a>
    <c:if test="${siteConfig['ADMIN_MESSAGE'].value != ''}">
    <a href="adminMessage.jhtm" class="userbutton"><c:out value="${siteConfig['ADMIN_MESSAGE'].value}"/></a>
    </c:if>
    <c:if test="${gSiteConfig['gPRICE_TABLE'] > 0 or gSiteConfig['gPROTECTED'] > 0}">
    <a href="labels.jhtm" class="userbutton"><fmt:message key="labels"/></a>
    </c:if>
    <c:if test="${siteConfig['SITEMAP'].value == 'true'}">
    <a href="createSitemap.jhtm" class="userbutton"><fmt:message key="siteMap"/></a>
    </c:if>
    <c:if test="${gSiteConfig['gI18N']!=''}">
    <a href="languageList.jhtm" class="userbutton"><fmt:message key="language"/></a>
    </c:if>
   <c:if test="${gSiteConfig['gSHOPPING_CART']}">	
   <div class="menudiv"></div>
   <h2 class="menuleft"><fmt:message key="shippingMenuTitle"/></h2>
   <div class="menudiv"></div>    
   <ul id="nav">
     <li><a href="shippingMethodList.jhtm" class="userbutton"><fmt:message key="carriers"/></a></li>
     <li><a href="customShippingList.jhtm" class="userbutton"><fmt:message key="customShipping"/></a>
     <c:if test="${siteConfig['CUSTOM_SHIPPING_CONTACT_INFO'].value == 'true'}">
      <ul>
        <li><a href="customShippingContactInfoList.jhtm" class="userbutton"><fmt:message key="contactList"/></a></li>
      </ul>
     </c:if>
     </li>
     <li><a href="shippingHandling.jhtm" class="userbutton"><fmt:message key="shippingHandling"/></a></li>
     <li><a href="tbd.jhtm" class="userbutton"><fmt:message key="tobeDetermine"/></a></li>
    </ul>   
    </c:if>
     <c:if test="${param.tab == 'siteMessage'}"> 
    <div align="left" class="searchHeader" id="searchHeaderId"><h2><fmt:message key="search" /></h2></div>
	  <table class="searchBoxLeft"><tr><td>
	  <form action="siteMessage.jhtm" name="searchform" method="post">
	  <!-- content area -->
	     <div class="search2">
	      <p><fmt:message key="siteMessageTitle" />:</p>
	      <select name="messageId">
	         <option value=""></option>
	      <c:forEach items="${model.siteMessagesList}" var="siteMessage">
	  	     <option value="${siteMessage.messageId}" <c:if test="${siteMessageSearch.messageId == siteMessage.messageId}">selected</c:if>><c:out value="${siteMessage.messageName}" /></option>
	       </c:forEach>
	      </select>
	    </div> 
	    <div class="search2">
	      <p><fmt:message key="siteMessageGroupTitle" />:</p>
	      <select name="groupId">
	      <option value="-1"></option>
	      <c:forEach items="${model.groups}" var="group">
	  	     <option value="${group.id}" <c:if test="${siteMessageSearch.groupId == group.id}">selected</c:if>><c:out value="${group.name}" /></option>
	       </c:forEach>
	      </select>
	    </div>  
	    <div class="button">
	      <input type="submit" value="Search" onclick="document.searchform.submit();"/>
		</div>
	  <!-- content area -->
	  </form>
	  </td></tr></table>
	  </c:if>
    </sec:authorize>
    <div class="menudiv"></div> 
    </td><td class="boxmidr" ></td></tr>
    <tr><td class="botl"></td>
    <td class="botr"></td>
	<div class="menudiv"></div></tr>
    </table>
  </div>
  

  
