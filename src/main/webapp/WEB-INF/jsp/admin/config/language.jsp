<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="/WEB-INF/tlds/spring.tld" prefix="spring" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ taglib uri="http://www.springframework.org/security/tags" prefix="sec" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>

<tiles:insertDefinition name="admin.config" flush="true">
  <tiles:putAttribute name="content" type="string">
  
<script language="JavaScript" type="text/JavaScript">
<!--
window.addEvent('domready', function(){			
	var tabs1 = new SimpleTabs($('tab-block-1'), { selector: 'h4'});
});
//-->
</script>	
<form action="language.jhtm" method="post">
<input type="hidden" value="${language.languageCode}" name="languageCode"/>

<div id="mboxfull">	
	
	<table cellpadding="0" cellspacing="0" border="0" class="module" >
 
	<tr>
	    <td class="topl_g">
		  
		  <!-- breadcrumb -->
		  <p class="breadcrumb">
		  	<a href="../config"><fmt:message key="configTabTitle"/></a> &gt;
		    <a href="../config/languageList.jhtm"><fmt:message key="languageList"/></a> &gt;
		    <fmt:message key="configuration"/> 
		  </p>
		  
		  <!-- Error Message -->
		  <c:if test="${!empty message}">
			<div class="message">
			  <fmt:message key="${message}">		    
			  </fmt:message>			  
			</div>
	  	  </c:if>
	        
	    </td><td class="topr_g" ></td>
	</tr>
	<tr><td class="boxmidlrg">
	<div id="tab-block-1">  

    <h4 title="<fmt:message key="language_${language.languageCode}"/>"><img src="../graphics/${language.languageCode}.gif" border="0" title="<fmt:message key="language_${language.languageCode}"/>"> <fmt:message key="language_${language.languageCode}"/></h4> 
	<div>
	<!-- start tab -->

        <c:set var="classIndex" value="0" />
        
	  	<div class="listdivi ln tabdivi"></div>
	  	<div class="listdivi"></div>
   		
		  	<div class="list${classIndex % 2}"><c:set var="classIndex" value="${classIndex+1}" />
		  	<div class="listfl"><fmt:message key="siteMessageNewRegistrants"/>:</div>
		  	<div class="listp">
		  	<!-- input field --> 
		  	  <select name="newRegistration">
		        <option value=""></option>
			    <c:forEach var="siteMessage" items="${siteMessages}">
			    <option value="${siteMessage.messageId}"  <c:if test="${siteMessage.messageId == language.newRegistrationId}">selected="selected"</c:if>><c:out value="${siteMessage.messageName}"/></option>
			    </c:forEach>
		 	  </select>
		    <!-- end input field -->   	
		  	</div>
		  	</div>
		  	<div class="list${classIndex % 2}"><c:set var="classIndex" value="${classIndex+1}" />
		  	<div class="listfl"><fmt:message key="siteMessageNewOrders"/>:</div>
		  	<div class="listp">
		  	<!-- input field -->
			  <select name="newOrders">
		        <option value=""></option>
			    <c:forEach var="siteMessage" items="${siteMessages}">
			    <option value="${siteMessage.messageId}" <c:if test="${siteMessage.messageId == language.newOrderId}">selected="selected"</c:if>><c:out value="${siteMessage.messageName}"/></option>		   
			    </c:forEach>
		 	  </select>
		    <!-- end input field -->   	
		  	</div>
		  	</div>
		  	
		   
    </div>
	
	</div>

	<!-- start button -->
	<div align="left" class="button"> 
	  <input type="submit" name="_update" value="<fmt:message key="Update"/>">
	
	<sec:authorize ifAnyGranted="ROLE_AEM"> 
	  <input type="submit" name="delete" value="<fmt:message key="delete" />">
	</sec:authorize>
	<!-- end button -->			 
	</div>	
	  <!-- end table --> 
	</td><td class="boxmidr" ></td>	</tr>	
	</table>

</div>
</form>

</tiles:putAttribute>
</tiles:insertDefinition>