<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/security/tags" prefix="sec" %>
<%@ taglib uri="/WEB-INF/tlds/spring.tld" prefix="spring" %>

<tiles:insertDefinition name="admin.config" flush="true">
  <tiles:putAttribute name="content" type="string">  

<sec:authorize ifAnyGranted="ROLE_AEM,ROLE_ADMIN_${prop_site_id},ROLE_SITE_INFO">
<c:if test="${gSiteConfig['gSHOPPING_CART']}">
<form action="stateList.jhtm" method="post">
<!-- main box -->
  <div id="mboxfull">
  
  <!-- start table -->
  <table cellpadding="0" cellspacing="0" border="0" class="module" >
  <tr>
    <td class="topl_g">
	  
	  <!-- breadcrumb -->
	  <p class="breadcrumb">
	    <a href="../config">SiteInfo</a> &gt;
	    <a href="../config/stateList.jhtm">States</a> &gt;
	    <c:out value="${model.state}"/>
	  </p>
	  
	  <!-- Error Message -->
	  <c:if test="${!empty message}">
		  <div class="message"><spring:message code="${message}" /></div>
	  </c:if>
  
  </td><td class="topr_g" ></td></tr>
  <tr><td class="boxmidlrg" >

 <!-- tabs -->
<div class="tab-wrapper">
	<h4 title="List of affiliates"></h4>
	<div>
	<!-- start tab -->

	  	<div class="listdivi"></div>
	   
		  	<div class="listlight">
		  	<!-- input field -->
				<div style="width:100%;">
				<c:set var="cols" value="1"/>
				<table border="0" cellpadding="0" cellspacing="0" width="100%" class="listings">
				  <tr class="listingsHdr2">
				    <td class="indexCol">&nbsp;</td>
				    <td class="listingsHdr3">U.S. States</td>   
				    <c:if test="${gSiteConfig['gCOUNTY_TAX']}">
				      <c:set var="cols" value="${cols+1}"/>
				      <td class="listingsHdr3">County <fmt:message key="taxRate" /></td>
				    </c:if>   
				    <td class="listingsHdr3">State <fmt:message key="taxRate" /></td>
				    <td class="listingsHdr3"><fmt:message key="applyTaxOnShipping" /></td>
				    <td class="listingsHdr3"><fmt:message key="region" /></td>
				  </tr>
				<c:forEach items="${model.statelist}" var="state" varStatus="status">
				  <tr class="row${status.index % 2}" onmouseover="changeStyleClass(this,'rowOver')" onmouseout="changeStyleClass(this,'row${status.index % 2}')">
				    <td class="indexCol"><c:out value="${status.count}"/>.</td>
				    <td class="nameCol"><c:out value="${state.name}"/> (<c:out value="${state.code}"/>)<input type="hidden" name="__code" value="${state.code}"></td>
				    <c:if test="${gSiteConfig['gCOUNTY_TAX']}">
				      <td class="nameCol" valign="middle"><a href="countyList.jhtm?state=${state.code}" ><img border="0" src="../graphics/edit.gif" align="middle"/></a></td>
				    </c:if>
				    <td class="rankCol"><input name="__taxrate_${state.code}" type="text" value="${state.taxRate}" size="5" maxlength="5" class="rankField"></td>
				    <td class="rankCol"><input name="__applyShippingTax_${state.code}" type="checkbox" <c:if test="${state.applyShippingTax}">checked="checked"</c:if>  class="rankField"></td>
				    <td class="rankCol"><input name="__region_${state.code}" type="text" value="${state.region}" size="25" maxlength="25" class="regionField"></td>
				  </tr>
				</c:forEach>
				  <tr><td colspan="4">&nbsp;</td></tr>
				  <tr class="listingsHdr2">
				    <td class="indexCol">&nbsp;</td>
				    <td colspan="${cols}" class="listingsHdr3">Canadian Provinces/Territories</td>    
				    <td class="listingsHdr3"><fmt:message key="taxRate" /></td>
				    <td class="listingsHdr3"><fmt:message key="applyOnShipping" /></td>
				    <td class="listingsHdr3"><fmt:message key="region" /></td>
				  </tr>
				<c:forEach items="${model.caStateList}" var="state" varStatus="status">
				  <tr class="row${status.index % 2}" onmouseover="changeStyleClass(this,'rowOver')" onmouseout="changeStyleClass(this,'row${status.index % 2}')">
				    <td class="indexCol"><c:out value="${status.count}"/>.</td>
				    <td colspan="${cols}" class="nameCol"><c:out value="${state.name}"/> (<c:out value="${state.code}"/>)<input type="hidden" name="__code" value="${state.code}"></td>
				    <td class="rankCol"><input name="__taxrate_${state.code}" type="text" value="${state.taxRate}" size="5" maxlength="5" class="rankField"></td>
				    <td class="rankCol"><input name="__applyShippingTax_${state.code}" type="checkbox" <c:if test="${state.applyShippingTax}">checked="checked"</c:if>  class="rankField"></td>
				    <td class="rankCol"><input name="__region_${state.code}" type="text" value="${state.region}" size="25" maxlength="25" class="regionField"></td>
				  </tr>
				</c:forEach>
				</table>
			<!-- end input field -->  	  
		  	</div>

		  	<!-- start button -->
				<div align="left" class="button"> 
				  <input type="submit" name="__update" value="<fmt:message key="Update" />">
				</div>
	  		<!-- end button -->	
		</div>
		<!-- end tab -->        
	</div>
<!-- end tabs -->			
</div>
  
  <!-- end table -->
   </td><td class="boxmidr" ></td></tr>
  <tr><td class="botl"></td><td class="botr"></td></tr>
  </table>
  
<!-- end main box -->  
</div>
</div>
</form>
</c:if>
</sec:authorize>
 
  </tiles:putAttribute>    
</tiles:insertDefinition>