<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="/WEB-INF/tlds/spring.tld" prefix="spring" %>
<%@ taglib uri="http://www.springframework.org/security/tags" prefix="sec" %>

<tiles:insertDefinition name="admin.config" flush="true">
  <tiles:putAttribute name="content" type="string">

<sec:authorize ifAnyGranted="ROLE_AEM,ROLE_ADMIN_${prop_site_id},ROLE_SITE_INFO"> 
<script language="JavaScript"> 
<!--
var countryArray = new Array();
<c:forEach items="${model.countrylist}" var="country" varStatus="status">
countryArray.push("__enabled_${country.code}");</c:forEach>
function selectAll() {
  for (i=0; i<countryArray.length; i++) {
    document.getElementById(countryArray[i]).checked = true;
  }
}
function invertAll() {
  for (i=0; i<countryArray.length; i++) {
    if (document.getElementById(countryArray[i]).checked) {
      document.getElementById(countryArray[i]).checked = false; 
    } else {
      document.getElementById(countryArray[i]).checked = true; 
    }
  }
}
//--> 
</script>  
  
<form action="countryList.jhtm" method="post">
<!-- main box -->
  <div id="mboxfull">
  
  <!-- start table -->
  <table cellpadding="0" cellspacing="0" border="0" class="module" >
  <tr>
    <td class="topl_g">
	  
	  <!-- breadcrumb -->
	  <p class="breadcrumb">
	    <a href="../config"><fmt:message key="configTabTitle"/></a> &gt;
	    Countries
	  </p>
	  
	  <!-- Error Message -->
      <c:if test="${!empty message}">
		  <div class="message"><spring:message code="${message}" /></div>
	  </c:if>

  </td><td class="topr_g" ></td></tr>
  <tr><td class="boxmidlrg" >

 <!-- tabs -->
<div class="tab-wrapper">
	<h4 title="List of affiliates"></h4>
	<div>
	<!-- start tab -->
<a href='' onClick="selectAll();return false">Select all</a>&nbsp;
<a href='' onClick="invertAll();return false">Invert selection</a>

	  	<div class="listdivi"></div>
	   		    
		  	<div class="listlight">
		  	<!-- input field -->
				
				<div style="width:100%;">
				
				<table border="0" cellpadding="0" cellspacing="0" width="100%" class="listings">
				  <tr class="listingsHdr2">
				    <td class="indexCol">&nbsp;</td>
				    <td class="enabledCol" style="padding-right:10px;"><fmt:message key="Enabled" /></td>
				    <td class="listingsHdr3"><fmt:message key="country" /></td>    
				    <td class="listingsHdr3" align="center"><fmt:message key="customers" /></td>
			    	<c:if test="${gSiteConfig['gSHOPPING_CART']}">
			    	<td class="listingsHdr3" align="center"><fmt:message key="orderCount" /></td>
			    	</c:if>
			    	<td class="listingsHdr3"><fmt:message key="taxRate" /></td>
				    <td align="center" class="listingsHdr3"><fmt:message key="rank" /></td>
				    <td align="center" class="listingsHdr3"><fmt:message key="region" /></td>
				  </tr>
				<c:forEach items="${model.countrylist}" var="country" varStatus="status">
				  <tr class="row${status.index % 2}" onmouseover="changeStyleClass(this,'rowOver')" onmouseout="changeStyleClass(this,'row${status.index % 2}')">
				    <td class="indexCol"><c:out value="${status.count}"/>.</td>
				    <td align="center"><input name="__enabled_${country.code}" id="__enabled_${country.code}" type="checkbox" <c:if test="${country.enabled}">checked</c:if>></td>    
				    <td class="nameCol"><c:out value="${country.name}"/> (<c:out value="${country.code}"/>)<input type="hidden" name="__code" value="${country.code}"></td>
				    <td align="center"><c:choose><c:when test="${country.customerCount > 0}"><a href="../customers/customerList.jhtm?country=${country.code}"><c:out value="${country.customerCount}"/></a></c:when><c:otherwise>&nbsp;</c:otherwise></c:choose></td>
			    	<c:if test="${gSiteConfig['gSHOPPING_CART']}">
				    <td align="center"><c:choose><c:when test="${country.orderCount > 0}"><c:out value="${country.orderCount}"/></c:when><c:otherwise>&nbsp;</c:otherwise></c:choose></td>
			    	</c:if>
			    	<c:choose>
			    	  <c:when test="${country.code == 'US' or country.code == 'CA'}">
			    	    <td class="rankCol"><a href="stateList.jhtm">Setup Tax</a></td>
			    	  </c:when>
			    	  <c:otherwise>
			    	    <td class="rankCol"><input name="__taxrate_${country.code}" type="text" value="${country.taxRate}" size="5" maxlength="5" class="rankField"></td>
			    	  </c:otherwise>
			    	</c:choose>
			    	
				    <td class="rankCol"><input name="__rank_${country.code}" type="text" value="${country.rank}" size="5" maxlength="5" class="rankField"></td>
				    <td class="rankCol"><input name="__region_${country.code}" type="text" value="${country.region}" size="25" maxlength="25"  class="regionField"></td>
				  </tr>
				</c:forEach>
				</table>
            <!-- end input field -->  	  
		  	</div>
		  	
		  	<!-- start button -->
				<div align="left" class="button"> 
				  <input type="submit" name="__update" value="<fmt:message key="Update" />">
				</div>
	  		<!-- end button -->	
		</div>
		<!-- end tab -->        
	</div>
<!-- end tabs -->			
</div>
  
  <!-- end table -->
   </td><td class="boxmidr" ></td></tr>
  <tr><td class="botl"></td><td class="botr"></td></tr>
  </table>
  
<!-- end main box -->  
</div>
</form>
</sec:authorize>
 
  </tiles:putAttribute>    
</tiles:insertDefinition>


