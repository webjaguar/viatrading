<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="/WEB-INF/tlds/spring.tld" prefix="spring" %>
<%@ taglib uri="http://www.springframework.org/security/tags" prefix="sec" %>

<tiles:insertDefinition name="admin.config.siteMessage" flush="true">
  <tiles:putAttribute name="content" type="string">

<form id="siteMessageForm" action="siteMessage.jhtm" method="post">
<input type="hidden" id="sort" name="sort" value="${siteMessageSearch.sort}" />
<!-- main box -->
  <div id="mboxfull">
  
  <!-- start table -->
  <table cellpadding="0" cellspacing="0" border="0" class="module" >
  <tr>
    <td class="topl_g">
	  
	  <!-- breadcrumb -->
	  <p class="breadcrumb">
	    <a href="../config">SiteInfo</a> &gt;
	    <fmt:message key="siteMessageTitle"/> 
	  </p>
	  
	  <!-- Error -->
	  
	<!-- header image -->
	  <img class="headerImage" src="../graphics/customer.gif" />
  
  </td><td class="topr_g" ></td></tr>
  <tr><td class="boxmidlrg" >

 <!-- tabs -->
<div class="tab-wrapper">
	<h4 title="List of SiteMessage"></h4>
	<div>
	<!-- start tab -->

	  	<%-- <div class="listdivi ln tabdivi"></div>--%>
	  	<div class="listdivi"></div>
	    
		  	<div class="listlight">
		  	<!-- input field -->
			<table border="0" cellpadding="0" cellspacing="1" width="100%">
				<tr>
					<c:if test="${model.siteMessages.nrOfElements > 0}">
						<td class="pageShowing">
							<fmt:message key="showing">
								<fmt:param value="${model.siteMessages.firstElementOnPage + 1}" />
								<fmt:param value="${model.siteMessages.lastElementOnPage + 1}" />
								<fmt:param value="${model.siteMessages.nrOfElements}" />
							</fmt:message>
						</td>
					</c:if>
					<td class="pageNavi">
						Page
						<select name="page" id="page" onchange="submit()">
							<c:forEach begin="1" end="${model.siteMessages.pageCount}"
								var="page">
								<option value="${page}"
									<c:if test="${page == (model.siteMessages.page+1)}">selected</c:if>>
									${page}
								</option>
							</c:forEach>
						</select> 
						of
						<c:out value="${model.siteMessages.pageCount}" /> 
						|
						<c:if test="${model.siteMessages.firstPage}">
							<span class="pageNaviDead"><fmt:message key="previous" /></span>
						</c:if>
						<c:if test="${not model.siteMessages.firstPage}">
							<a
								href="<c:url value="siteMessage.jhtm"><c:param name="page" value="${model.siteMessages.page}"/><c:if test="${not model.search['homePage']}"><c:param name="parent" value="${model.search['parent']}"/></c:if></c:url>"
								class="pageNaviLink"><fmt:message key="previous" /></a>
						</c:if>
						|
						<c:if test="${model.siteMessages.lastPage}">
							<span class="pageNaviDead"><fmt:message key="next" /></span>
						</c:if>
						<c:if test="${not model.siteMessages.lastPage}">
							<a
								href="<c:url value="siteMessage.jhtm"><c:param name="page" value="${model.siteMessages.page+2}"/><c:param name="size" value="${model.siteMessages.pageSize}"/></c:url>"
								class="pageNaviLink"><fmt:message key="next" /></a>
						</c:if>
					</td>
				</tr>
			</table>

			<table border="0" cellpadding="0" cellspacing="0" width="100%"
				class="listings">
				<tr class="listingsHdr2">
					<td class="indexCol">
						&nbsp;
					</td>
					<td>
					<table cellspacing="0" cellpadding="1">
			    	  <tr>
			    	    <c:choose>
			    	      <c:when test="${siteMessageSearch.sort == 'message_name desc'}">
			    	        <td class="listingsHdr3">
			    	        <a class="hdrLink" href="#" onClick="document.getElementById('sort').value='message_name';document.getElementById('siteMessageForm').submit()"><fmt:message key="messageName" /></a>
			    	        </td>
			    	        <td><img src="../graphics/up.gif" border="0"></td>
			    	      </c:when>
			    	      <c:when test="${siteMessageSearch.sort == 'message_name'}">
			    	        <td class="listingsHdr3">
			    	        <a class="hdrLink" href="#" onClick="document.getElementById('sort').value='message_name desc';document.getElementById('siteMessageForm').submit()"><fmt:message key="messageName" /></a>
			    	        </td>
			    	        <td><img src="../graphics/down.gif" border="0"></td>
			    	      </c:when>
			    	      <c:otherwise>
			    	        <td colspan="2" class="listingsHdr3">
			    	        <a class="hdrLink" href="#" onClick="document.getElementById('sort').value='message_name desc';document.getElementById('siteMessageForm').submit()"><fmt:message key="messageName" /></a>
			    	        </td>
			    	      </c:otherwise>
			    	    </c:choose>  
			    	  </tr>
			    	  </table>
			    	  </td>	
					<td>
					<table cellspacing="0" cellpadding="1">
			    	  <tr>
			    	    <c:choose>
			    	      <c:when test="${siteMessageSearch.sort == 'message_id desc'}">
			    	        <td class="listingsHdr3">
			    	        <a class="hdrLink" href="#" onClick="document.getElementById('sort').value='message_id';document.getElementById('siteMessageForm').submit()"><fmt:message key="siteMessageId" /></a>
			    	        </td>
			    	        <td><img src="../graphics/up.gif" border="0"></td>
			    	      </c:when>
			    	      <c:when test="${siteMessageSearch.sort == 'message_id'}">
			    	        <td class="listingsHdr3">
			    	        <a class="hdrLink" href="#" onClick="document.getElementById('sort').value='message_id desc';document.getElementById('siteMessageForm').submit()"><fmt:message key="siteMessageId" /></a>
			    	        </td>
			    	        <td><img src="../graphics/down.gif" border="0"></td>
			    	      </c:when>
			    	      <c:otherwise>
			    	        <td colspan="2" class="listingsHdr3">
			    	        <a class="hdrLink" href="#" onClick="document.getElementById('sort').value='message_id desc';document.getElementById('siteMessageForm').submit()"><fmt:message key="siteMessageId" /></a>
			    	        </td>
			    	      </c:otherwise>
			    	    </c:choose>  
			    	  </tr>
			    	  </table>
			    	  </td>	
					<td>
					<table cellspacing="0" cellpadding="1">
			    	  <tr>
			    	    <c:choose>
			    	      <c:when test="${siteMessageSearch.sort == 'subject desc'}">
			    	        <td class="listingsHdr3">
			    	        <a class="hdrLink" href="#" onClick="document.getElementById('sort').value='subject';document.getElementById('siteMessageForm').submit()"><fmt:message key="subject" /></a>
			    	        </td>
			    	        <td><img src="../graphics/up.gif" border="0"></td>
			    	      </c:when>
			    	      <c:when test="${siteMessageSearch.sort == 'subject'}">
			    	        <td class="listingsHdr3">
			    	        <a class="hdrLink" href="#" onClick="document.getElementById('sort').value='subject desc';document.getElementById('siteMessageForm').submit()"><fmt:message key="subject" /></a>
			    	        </td>
			    	        <td><img src="../graphics/down.gif" border="0"></td>
			    	      </c:when>
			    	      <c:otherwise>
			    	        <td colspan="2" class="listingsHdr3">
			    	        <a class="hdrLink" href="#" onClick="document.getElementById('sort').value='subject desc';document.getElementById('siteMessageForm').submit()"><fmt:message key="subject" /></a>
			    	        </td>
			    	      </c:otherwise>
			    	    </c:choose>  
			    	  </tr>
			    	  </table>
			    	  </td>
					<td align="center" class="listingsHdr3">
						HTML
					</td>				
				</tr>
				<c:forEach items="${model.siteMessages.pageList}" var="siteMessage"
					varStatus="status">
					<tr class="row${status.index % 2}"
						onmouseover="changeStyleClass(this,'rowOver')"
						onmouseout="changeStyleClass(this,'row${status.index % 2}')">
						<td class="indexCol">
							<c:out
								value="${status.count + model.siteMessages.firstElementOnPage}" />
							.
						</td>
						<td class="nameCol">
						<a href="siteMessageForm.jhtm?id=${siteMessage.messageId}" class="nameLink"><c:out value="${siteMessage.messageName }" /> </a>							
						</td>
						<td class="nameCol">
							<a href="siteMessageForm.jhtm?id=${siteMessage.messageId}" class="nameLink"><c:out value="${siteMessage.messageId}" /> </a>
						</td>						
						<td class="nameCol">
							<c:out
								value="${siteMessage.subject }" />
						</td>		
						<td align="center" >
     							<c:if test="${siteMessage.html}"><img src="../graphics/checkbox.png" border="0" /></c:if>
						</td>
					</tr>
				</c:forEach>
				<c:if test="${model.siteMessages.nrOfElements == 0}">
					<tr class="emptyList">
						<td colspan="8">
							&nbsp;
						</td>
					</tr>
				</c:if>
			</table>

			<table border="0" cellpadding="0" cellspacing="1" width="100%">
				<tr>
				  <td class="pageSize">
					<select name="size"
						onchange="document.getElementById('page').value=1;submit()">
						<c:forTokens items="10,25,50,100" delims="," var="current">
							<option value="${current}"
								<c:if test="${current == model.siteMessages.pageSize}">selected</c:if>>
								${current}
								<fmt:message key="perPage" />
							</option>
						</c:forTokens>
					</select>
				  </td>
				</tr>
			</table>
            <!-- end input field -->  	  
		  	
	        </div>
  	
	<!-- end tab -->        
	</div>
<!-- end tabs -->			
</div>
<!-- start button -->
<div align="left" class="button">
    <input type="submit" name="add_siteMessage" value="<fmt:message key='addsiteMessage' />"  /> 
</div>	
<!-- end button -->	 
  
  <!-- end table -->
   </td><td class="boxmidr" ></td></tr>
  <tr><td class="botl"></td><td class="botr"></td></tr>
  </table>
  
<!-- end main box -->  
</div>
</form>	

  </tiles:putAttribute>    
</tiles:insertDefinition>