<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/security/tags" prefix="sec" %>
<%@ taglib uri="/WEB-INF/tlds/spring.tld" prefix="spring" %>

<tiles:insertDefinition name="admin.config.shipping" flush="true">
  <tiles:putAttribute name="content" type="string">

<sec:authorize ifAnyGranted="ROLE_AEM,ROLE_ADMIN_${prop_site_id},ROLE_SITE_INFO"> 
<script type="text/javascript">
<!--
window.addEvent('domready', function(){			
	var tabs1 = new SimpleTabs($('tab-block-1'), { selector: 'h4'});
	var Tips1 = new Tips($$('.toolTipImg'));
});
//-->
</script>   
<form action="shippingMethodList.jhtm" method="post">
<!-- main box -->
  <div id="mboxfull">
  
  <!-- start table -->
  <table cellpadding="0" cellspacing="0" border="0" class="module" >
  <tr>
    <td class="topl_g">
	  
	  <!-- breadcrumb -->
	  <p class="breadcrumb">
	    <a href="../config">SiteInfo</a> &gt;
	    Shipping Carrier
	  </p>
	  
	  <!-- Error Message -->
	  <c:if test="${!empty model.message}">
		  <div class="message"><spring:message code="${model.message}" /></div>
	  </c:if>
	  
	  </td><td class="topr_g" ></td></tr>
  <tr><td class="boxmidlrg" >

 <!-- tabs -->
 <div class="tab-wrapper">
	<h4 title="List of affiliates"></h4>
	<div>
	<!-- start tab -->
	   
		  	<div class="listlight">
		  	<!-- input field --> 
			
			
			<!-- tabs -->
			 <div id="tab-block-1">
			    <h4 title="Origin Address">Origin Address</h4>
				<div>
				<!-- start tab -->
	  				<div class="listdivi ln tabdivi"></div>
	  				<div class="listdivi"></div>
					<div style="clear:both;">
					<table class="form" align="center">
				      <tr>
					    <td><input type="hidden" name="__key" value="SOURCE_ADDRESS">Source Address:</td>
					    <td><input type="text" class="textfield" name="SOURCE_ADDRESS" value="<c:out value="${model.siteConfig['SOURCE_ADDRESS'].value}"/>" maxlength="80" style="width: 250px;"></td>
					  </tr>   
				      <tr>
					    <td><input type="hidden" name="__key" value="SOURCE_CITY">Source City:</td>
					    <td><input type="text" class="textfield" name="SOURCE_CITY" value="<c:out value="${model.siteConfig['SOURCE_CITY'].value}"/>" maxlength="80" style="width: 250px;"></td>
					  </tr>      
					  <tr>
					    <td><input type="hidden" name="__key" value="SOURCE_ZIP">Source Zip:</td>
					    <td><input type="text" class="textfield" name="SOURCE_ZIP" value="<c:out value="${model.siteConfig['SOURCE_ZIP'].value}"/>" maxlength="10" style="width: 250px;"></td>
					  </tr>
					  <tr>
					    <td><input type="hidden" name="__key" value="SOURCE_COUNTRY">Source Country:</td>
					    <td>
					       <select id="country" name="SOURCE_COUNTRY" onchange="toggleStateProvince(this)" style="width: 250px;">
					  	      <option value="">Please Select</option>
						      <c:forEach items="${model.countrylist}" var="country">
					  	        <option value="${country.code}" <c:if test="${country.code == model.siteConfig['SOURCE_COUNTRY'].value}">selected</c:if>>${country.name}</option>
							  </c:forEach>		
							</select>
						</td>
					  </tr>     
					  <tr>
					    <td><input type="hidden" name="__key" value="SOURCE_STATEPROVINCE">Source State/Province:</td>
					    <td>
					       <select id="state" name="SOURCE_STATEPROVINCE" style="width: 250px;">
					  	      <option value="">Please Select</option>
						      <c:forEach items="${model.statelist}" var="state">
					  	        <option value="${state.code}" <c:if test="${state.code == model.siteConfig['SOURCE_STATEPROVINCE'].value}">selected</c:if>>${state.name}</option>
							  </c:forEach>      
					        </select>
							<select id="ca_province" name="SOURCE_STATEPROVINCE" style="width: 250px;">
					  	      <option value="">Please Select</option>
						      <c:forEach items="${model.caProvinceList}" var="province">
					  	        <option value="${province.code}" <c:if test="${province.code == model.siteConfig['SOURCE_STATEPROVINCE'].value}">selected</c:if>>${province.name}</option>
							  </c:forEach>      
					        </select>      
							<select id="province" name="SOURCE_STATEPROVINCE" style="width: 250px;">
					  	      <option value="">N/A</option>
					        </select> 
					    </td>
					  </tr>
					  <c:if test="${model.siteConfig['DELIVERY_TIME'].value == 'true'}">
					  <tr>
					    <td><input type="hidden" name="__key" value="SHIPPING_TURNOVER_TIME">Shipping Turn Over Time:</td>
					    <td>
						  <select id="country" name="SHIPPING_TURNOVER_TIME" style="width: 250px;">
					  	      <option value="0">Please Select</option>
					  	        <option value="1" <c:if test="${1 == model.siteConfig['SHIPPING_TURNOVER_TIME'].value}">selected</c:if>>1 Day</option>	
					  	        <option value="2" <c:if test="${2 == model.siteConfig['SHIPPING_TURNOVER_TIME'].value}">selected</c:if>>2 Day</option>
					  	        <option value="3" <c:if test="${3 == model.siteConfig['SHIPPING_TURNOVER_TIME'].value}">selected</c:if>>3 Day</option>		
					  	        <option value="4" <c:if test="${4 == model.siteConfig['SHIPPING_TURNOVER_TIME'].value}">selected</c:if>>4 Day</option>	
					  	        <option value="5" <c:if test="${5 == model.siteConfig['SHIPPING_TURNOVER_TIME'].value}">selected</c:if>>5 Day</option>
					  	        <option value="6" <c:if test="${6 == model.siteConfig['SHIPPING_TURNOVER_TIME'].value}">selected</c:if>>6 Day</option>
							</select>
						</td>
					  </tr>
					  </c:if> 
					  <tr>
					    <td>
					    <!-- start button --> 
					  	<div align="left" class="button"> 
							<input type="submit" name="__update" value="<fmt:message key="Update" />">
						</div>	
					  	<!-- end button -->		
					    </td>
					  </tr>  
					</table>
				    </div>	 
				<!-- end tab -->        
				</div>  
				
				
				<h4 title="USPS">USPS</h4>
				<div>
				<!-- start tab --> 
					<div class="listdivi ln tabdivi"></div>
	  				<div class="listdivi"></div>
					<div style="clear:both;">
					<table border="0" cellspacing="10">
					 <tr>
						<td><input type="hidden" name="__key" value="USPS_USERID">USPS UserID:</td>
				 		<td><input type="text" name="USPS_USERID" value="<c:out value="${model.siteConfig['USPS_USERID'].value}"/>" maxlength="50" size="20">
						  <c:if test="${empty model.siteConfig['USPS_USERID'].value}"><a href="https://secure.shippingapis.com/registration/" target="_blank">Register for user ID</a></c:if>
						</td>
					 </tr>
					 <tr>
						<td colspan="2" >
						 <ol>
						   <li style="font-size:9px;color:#7b7b7b;">Register for User ID</li>
						   <li style="font-size:9px;color:#7b7b7b;">Email or call USPS and activate your user ID (Mention webjaguar Ecommerce solution and your user ID)</li>
						 </ol>
						</td>
					  </tr>
					  <tr>
					    <td>
					    <!-- start button --> 
					  	<div align="left" class="button"> 
							<input type="submit" name="__update" value="<fmt:message key="Update" />">
						</div>	
					  	<!-- end button -->		
					    </td>
					  </tr>
				    </table>
				    </div>	 
				<!-- end tab -->        
				</div>
				
				<h4 title="UPS">UPS</h4>
				<div>
				<!-- start tab --> 
					<div class="listdivi ln tabdivi"></div>
	  				<div class="listdivi"></div>
					<div style="clear:both;">
					<table border="0" cellspacing="10">
					 <tr>
						<td><input type="hidden" name="__key" value="UPS_USERID">UPS UserID:</td>
				 		<td><input type="text" name="UPS_USERID" value="<c:out value="${model.siteConfig['UPS_USERID'].value}"/>" maxlength="50" size="20"></td>
					 </tr>
					 <tr>
						<td><input type="hidden" name="__key" value="UPS_PASSWORD">UPS Password:</td>
				 		<td><input type="text" name="UPS_PASSWORD" value="<c:out value="${model.siteConfig['UPS_PASSWORD'].value}"/>" maxlength="50" size="20"></td>
					 </tr>
					 <tr>
						<td><input type="hidden" name="__key" value="UPS_ACCESS_LICENSE_NUMBER">UPS Access License Number:</td>
				 		<td><input type="text" name="UPS_ACCESS_LICENSE_NUMBER" value="<c:out value="${model.siteConfig['UPS_ACCESS_LICENSE_NUMBER'].value}"/>" maxlength="50" size="20"></td>
					 </tr>
					 <tr>
						<td><input type="hidden" name="__key" value="UPS_SHIPPER_NUMBER">UPS Shipper Number:</td>
				 		<td><input type="text" name="UPS_SHIPPER_NUMBER" value="<c:out value="${model.siteConfig['UPS_SHIPPER_NUMBER'].value}"/>" maxlength="50" size="20">
				 		</td>
					 </tr> 
					 <tr>
						<td><input type="hidden" name="__key" value="UPS_PICKUPTYPE">UPS Pickup Type:</td>
				 		<td>
					         <select style="font-size:10px;width:150px" name="UPS_PICKUPTYPE">
					           <option value="01" <c:if test="${model.siteConfig['UPS_PICKUPTYPE'].value == '01'}">selected</c:if>>Daily Pickup</option>
					           <option value="03" <c:if test="${model.siteConfig['UPS_PICKUPTYPE'].value == '03'}">selected</c:if>>Customer Counter/ Retail Rate</option>
					         </select>
				 		</td>
					 </tr>
					 <tr>
						<td colspan="2" >
						 <ol>
						   <li style="font-size:9px;color:#7b7b7b;">www.UPS.com</li>
						   <li style="font-size:9px;color:#7b7b7b;">Click on Support Tab on top</li>
						   <li style="font-size:9px;color:#7b7b7b;">Scroll down all the way down and click on E-mail UPS</li>
						   <li style="font-size:9px;color:#7b7b7b;">Fill out Name</li>
						   <li style="font-size:9px;color:#7b7b7b;">Fill out E-Mail Address</li>
						   <li style="font-size:9px;color:#7b7b7b;">Select Category	"Technical Support"</li>
						   <li style="font-size:9px;color:#7b7b7b;">Select Topic  "Online Tools" - Click on Next</li>
						   <li style="font-size:9px;color:#7b7b7b;">Fill out Phone Number</li>
						   <li style="font-size:9px;color:#7b7b7b;">Select Stage of Development "Pre-integration"</li>
						   <li style="font-size:9px;color:#7b7b7b;">Tool "Rates and Service Selection XML"</li>
						   <li style="font-size:9px;color:#7b7b7b;">XPCI Version Date "1.0001"</li>
						   <li style="font-size:9px;color:#7b7b7b;">Please explain that you want the AccessLicenceNumber to get the shipping rate via the UPS webservice.</li>
						   <li style="font-size:9px;color:#7b7b7b;">Click Send E-Mail</li>
						   <li style="font-size:9px;color:#7b7b7b;">You will be contacted by a UPS representative.</li>
						 </ol>
						 <dl style="margin-left: 40px;">
						   <li style="font-size:9px;color:#7b7b7b;float:none;list-style: none;">Test UserID: bachir</li>
						   <li style="font-size:9px;color:#7b7b7b;float:none;list-style: none;">Test Password: aemsol</li>
						   <li style="font-size:9px;color:#7b7b7b;float:none;list-style: none;">Test Access License Number: EBEE3E934BADAE08</li>
						 </dl> 
						</td>
					  </tr>
					  <tr>
					    <td>
					    <!-- start button --> 
					  	<div align="left" class="button"> 
							<input type="submit" name="__update" value="<fmt:message key="Update" />">
						</div>	
					  	<!-- end button -->		
					    </td>
					  </tr>
				    </table>
				    </div>	 
				<!-- end tab -->        
				</div>          
			
				<h4 title="FedEx�">FedEx�</h4>
				<div>
				<!-- start tab -->
					<div class="listdivi ln tabdivi"></div>
	  				<div class="listdivi"></div>
					<div style="clear:both;">
					<div>
					<table>
					  <tr>
					    <td>
					      <table border="0" cellspacing="10">
						  <tr>
						    <td><input type="hidden" name="__key" value="COMPANY_CONTACT_FIRSTNAME">Contact First Name:</td>
						    <td>
						      <c:choose>
						       <c:when test="${empty model.siteConfig['COMPANY_CONTACT_FIRSTNAME'].value}">
						         <input type="text" name="COMPANY_CONTACT_FIRSTNAME" value="<c:out value="${model.siteConfig['COMPANY_CONTACT_FIRSTNAME'].value}"/>" maxlength="50" size="20">
						       </c:when>
						       <c:otherwise>
						         <input type="hidden" name="COMPANY_CONTACT_FIRSTNAME" value="${model.siteConfig['COMPANY_CONTACT_FIRSTNAME'].value}" />
						         <c:out value="${model.siteConfig['COMPANY_CONTACT_FIRSTNAME'].value}" />
						       </c:otherwise>
						      </c:choose>
						    </td>
						  </tr>
						  <tr>  
						    <td><input type="hidden" name="__key" value="COMPANY_CONTACT_LASTNAME">Contact Last Name:</td>
						    <td>
						      <c:choose>
						       <c:when test="${empty model.siteConfig['COMPANY_CONTACT_LASTNAME'].value}">
						         <input type="text" name="COMPANY_CONTACT_LASTNAME" value="<c:out value="${model.siteConfig['COMPANY_CONTACT_LASTNAME'].value}"/>" maxlength="50" size="20">
						       </c:when>
						       <c:otherwise>
						         <input type="hidden" name="COMPANY_CONTACT_LASTNAME" value="${model.siteConfig['COMPANY_CONTACT_LASTNAME'].value}" />
						         <c:out value="${model.siteConfig['COMPANY_CONTACT_LASTNAME'].value}" />
						       </c:otherwise>
						      </c:choose>
						    </td>
						  </tr> 
						  <tr>
						    <c:choose>
						       <c:when test="${empty model.siteConfig['FEDEX_USER_KEY'].value or empty model.siteConfig['FEDEX_USER_PASSWORD'].value}">
						    		<td>Register:</td>
						    		<td>
						      		 <a href="fedExSubscriber.jhtm?__getkeyPassword=true" />Get Key password register Step 1</a>
						   		    </td>
						       </c:when>
						       <c:otherwise>
						    		<td>Key/Password:</td>
						    		<td>
						      		  registered
						   		    </td>
						       </c:otherwise>
						    </c:choose>
						  </tr> 
						  <tr>
						    <c:choose>
						       <c:when test="${empty model.siteConfig['FEDEX_USER_METERNUMBER'].value}">
						    		<td>Subscribe:</td>
						    		<td>
						      		  <a href="fedExSubscriber.jhtm?__getMeterNumber=true" />Get Meter number subscription step 2</a>
						   		    </td>
						       </c:when>
						       <c:otherwise>
						    		<td>Meter #:</td>
						    		<td>
						      		  registered
						   		    </td>
						       </c:otherwise>
						    </c:choose>
						  </tr> 
					    </table>
					    </td>
					    <td valign="top">
					      <table border="0" cellspacing="10" class="shippingCarrierForm">
					      <tr>
					        <td>SignatureOptionType<input type="hidden" name="__key" value="FEDEX_SIGNATURE_TYPE"></td>
					        <td colspan"2">
					         <select style="font-size:10px;width:150px" name="FEDEX_SIGNATURE_TYPE">
					           <option value="NO_SIGNATURE_REQUIRED">NO SIGNATURE REQUIRED</option>
					           <option value="DIRECT" <c:if test="${model.siteConfig['FEDEX_SIGNATURE_TYPE'].value == 'DIRECT'}">selected</c:if>>DIRECT</option>
					           <option value="INDIRECT" <c:if test="${model.siteConfig['FEDEX_SIGNATURE_TYPE'].value == 'INDIRECT'}">selected</c:if>>INDIRECT</option>
					           <option value="ADULT" <c:if test="${model.siteConfig['FEDEX_SIGNATURE_TYPE'].value == 'ADULT'}">selected</c:if>>ADULT</option>
					         </select>
					        </td>
					      </tr>
					      <tr>
					        <td>PackagingType<input type="hidden" name="__key" value="FEDEX_PACKAGING_TYPE"></td>
					        <td>
					         <select style="font-size:10px;width:150px" name="FEDEX_PACKAGING_TYPE">
					           <option value="YOUR_PACKAGING">YOUR PACKAGING</option>
					         </select>
					        </td>
					        <td>
					         <table>
					          <tr>
					           <td colspan="3">Dimensions (per package) </td>
					          </tr>
					          <tr>
					           <td colspan="3">
					            <input type="hidden" name="__key" value="PACKAGING_DIMENSION_LENGTH">
					            <input type="hidden" name="__key" value="PACKAGING_DIMENSION_WIDTH">
					            <input type="hidden" name="__key" value="PACKAGING_DIMENSION_HEIGHT">
					            <input type="hidden" name="__key" value="PACKAGING_DIMENSION_UNIT">
					            <input type="text" style="width: 32px; font-size: 11px;"  onblur="if(this.value==''){this.value='L';}" onfocus="if(this.value=='L'){this.value='';}" value="<c:choose><c:when test="${empty model.siteConfig['PACKAGING_DIMENSION_LENGTH'].value}">L</c:when><c:otherwise><c:out value="${model.siteConfig['PACKAGING_DIMENSION_LENGTH'].value}"/></c:otherwise></c:choose>" maxlength="3" name="PACKAGING_DIMENSION_LENGTH"/>
							    <input type="text" style="width: 32px; font-size: 11px;"  onblur="if(this.value==''){this.value='W';}" onfocus="if(this.value=='W'){this.value='';}" value="<c:choose><c:when test="${empty model.siteConfig['PACKAGING_DIMENSION_WIDTH'].value}">W</c:when><c:otherwise><c:out value="${model.siteConfig['PACKAGING_DIMENSION_WIDTH'].value}"/></c:otherwise></c:choose>"  maxlength="3" name="PACKAGING_DIMENSION_WIDTH"/>
							    <input type="text" style="width: 32px; font-size: 11px;"  onblur="if(this.value==''){this.value='H';}" onfocus="if(this.value=='H'){this.value='';}" value="<c:choose><c:when test="${empty model.siteConfig['PACKAGING_DIMENSION_HEIGHT'].value}">H</c:when><c:otherwise><c:out value="${model.siteConfig['PACKAGING_DIMENSION_HEIGHT'].value}"/></c:otherwise></c:choose>" maxlength="3" name="PACKAGING_DIMENSION_HEIGHT"/>
							    <select name="PACKAGING_DIMENSION_UNIT">
							     <option value="cm">cm</option>
							     <option value="in" <c:if test="${model.siteConfig['PACKAGING_DIMENSION_UNIT'].value == 'in'}">selected</c:if>>in</option>
							    </select>
							   </td>
					          </tr>
					         </table>
					        </td>
					      </tr>
					      <tr>
					        <td>DropoffType<input type="hidden" name="__key" value="FEDEX_DROP_OFF_TYPE"></td>
					        <td colspan"2">
					         <select style="font-size:10px;width:150px" name="FEDEX_DROP_OFF_TYPE">
					           <option value="REGULAR_PICKUP" <c:if test="${model.siteConfig['FEDEX_DROP_OFF_TYPE'].value == 'REGULAR_PICKUP'}">selected</c:if>>REGULAR PICKUP</option>
					           <option value="BUSINESS_SERVICE_CENTER" <c:if test="${model.siteConfig['FEDEX_DROP_OFF_TYPE'].value == 'BUSINESS_SERVICE_CENTER'}">selected</c:if>>BUSINESS SERVICE CENTER</option>
					           <option value="DROP_BOX" <c:if test="${model.siteConfig['FEDEX_DROP_OFF_TYPE'].value == 'DROP_BOX'}">selected</c:if>>DROP BOX</option>
					           <option value="REQUEST_COURIER" <c:if test="${model.siteConfig['FEDEX_DROP_OFF_TYPE'].value == 'REQUEST_COURIER'}">selected</c:if>>REQUEST COURIER</option>
					           <option value="STATION" <c:if test="${model.siteConfig['FEDEX_DROP_OFF_TYPE'].value == 'STATION'}">selected</c:if>>STATION</option>
					         </select>
					        </td>
					      </tr>
					      <tr>
					        <td>Saturday Delivery<input type="hidden" name="__key" value="FEDEX_SATURDAY_DELIVERY"></td>
					        <td colspan"2">
					         <select style="font-size:10px;width:150px" name="FEDEX_SATURDAY_DELIVERY">
					           <option value="false">No</option>
					           <option value="true" <c:if test="${model.siteConfig['FEDEX_SATURDAY_DELIVERY'].value == 'true'}">selected</c:if>>Yes</option>
					         </select>
					        </td>
					      </tr>
					    </table>
					    </td>
					  </tr>
					  <tr>
						<td colspan="2" >
						 <ol>
						   <li style="font-size:9px;color:#7b7b7b;">Make sure you have entered the following information on SiteInfo->SiteConfiguration</li>
						   <li style="font-size:9px;color:#7b7b7b;">Company Information->Company Name and Phone</li>
						   <li style="font-size:9px;color:#7b7b7b;">Site Configuration->Contact Email</li>
						   <li style="font-size:9px;color:#7b7b7b;">Fill out carriers->FedEx->Contact First Name</li>
						   <li style="font-size:9px;color:#7b7b7b;">Fill out carriers->FedEx->Contact Last Name</li>
						   <li style="font-size:9px;color:#7b7b7b;">Click on Update Button</li>
						   <li style="font-size:9px;color:#7b7b7b;">Click on "Get Key password register Step 1"</li>
						   <li style="font-size:9px;color:#7b7b7b;">You should see Registered</li>
						   <li style="font-size:9px;color:#7b7b7b;">Then click on "Get Meter number subscription step 2"</li>
						   <li style="font-size:9px;color:#7b7b7b;">You should see Registered</li>
						   <li style="font-size:9px;color:#7b7b7b;">Done.</li>
						   <li style="font-size:9px;color:#7b7b7b;">Now select The FedEx shipping type from the list below and click update.</li>
						   <li style="font-size:9px;color:#7b7b7b;">Finally goto frontend and check if you get FedEx shipping result.</li>
						 </ol>
						 <div class="helpNote">
						 	NOTE: If destination address type is "Residential", FEDEX returns "FedEx Home Delivery".<br/>
						 		  If destination address type is "Commercial" FEDEX returns "FedEx Ground".
						 </div>
						</td>
					  </tr>
					  <tr>
					    <td>
					    <!-- start button --> 
					  	<div align="left" class="button"> 
							<input type="submit" name="__update" value="<fmt:message key="Update" />">
						</div>	
					  	<!-- end button -->		
					    </td>
					  </tr>
					</table>
				    </div>
				    </div>
				<!-- end tab -->        
				</div>
			</div>	

			<br>     
			<div style="clear:both;width:100%;">
			
			<table border="0" cellpadding="0" cellspacing="0" width="100%" class="listings">
			  <tr class="listingsHdr2">
			    <td class="indexCol">&nbsp;</td>
			    <td class="enabledCol"><fmt:message key="Enabled" /></td>
			    <td class="listingsHdr3" style="padding-left:10px;"><fmt:message key="shippingCarrier" /></td> 
			    <td class="listingsHdr3"><fmt:message key="shippingCode" /></td>
			    <td class="listingsHdr3"><fmt:message key="shippingTitle" /></td> 
			    <td class="listingsHdr3"><fmt:message key="minPrice" /></td> 
			    <td class="listingsHdr3"><fmt:message key="maxPrice" /></td> 
			    <c:if test="${gSiteConfig['gWEATHER_CHANNEL']}" >  
			     <td class="listingsHdr3">
			       <img class="toolTipImg" title="Temperature::Faster shipping method can deliver product with low temperature." src="../graphics/question.gif" />
			       <fmt:message key="temperature" />
			     </td>   
			    </c:if>
			    <td class="listingsHdr3"><fmt:message key="rank" /></td>
			  </tr>
			<c:forEach items="${model.shippingmethodlist}" var="shippingmethod" varStatus="status">
			  <tr class="row${status.index % 2}" onmouseover="changeStyleClass(this,'rowOver')" onmouseout="changeStyleClass(this,'row${status.index % 2}')">
			    <td class="indexCol"><c:out value="${status.count}"/>.<input type="hidden" name="__id" value="${shippingmethod.id}"></td>
			    <td align="center"><input type="checkbox" <c:if test="${shippingmethod.shippingActive==true}">checked</c:if> name=__active_<c:out value="${shippingmethod.id}"/>></td>
			    <td class="nameCol"><c:choose><c:when test="${shippingmethod.carrier == 'fedex'}">FedEx<sup>&#174;</sup></c:when><c:otherwise><c:out value="${shippingmethod.carrier}"/></c:otherwise></c:choose></td>
			    <td class="nameCol"><c:out value="${shippingmethod.shippingCode}"/></td>
			    <td class="nameCol">
			     <c:choose>
			      <c:when test="${shippingmethod.carrier == 'fedex'}">
			        <input type="hidden" value="<c:out value="${shippingmethod.shippingTitle}" escapeXml="false"/>" name=__title_<c:out value="${shippingmethod.id}"/> size="30" class="contentField">
			        <c:out value="${shippingmethod.shippingTitle}" escapeXml="false"/></td>
			      </c:when>
			      <c:otherwise>
			        <input type="text" value="<c:out value="${shippingmethod.shippingTitle}" escapeXml="false"/>" name=__title_<c:out value="${shippingmethod.id}"/> size="30" class="contentField">
			      </c:otherwise>
			     </c:choose>
			     <td class="rankCol"><input type="text" value="<c:out value="${shippingmethod.minPrice}"/>" name=__minPrice_<c:out value="${shippingmethod.id}"/> size="7" maxlength="10" class="rankField"></td>
			     <td class="rankCol"><input type="text" value="<c:out value="${shippingmethod.maxPrice}"/>" name=__maxPrice_<c:out value="${shippingmethod.id}"/> size="7" maxlength="10" class="rankField"></td>
			    <c:if test="${gSiteConfig['gWEATHER_CHANNEL']}" >
			     <td class="rankCol"><input type="text" value="<c:out value="${shippingmethod.weatherTemp}"/>" name=__weather_temp_<c:out value="${shippingmethod.id}"/> size="5" maxlength="5" class="rankField">&deg;F</td>
			    </c:if>
			    <td class="rankCol"><input type="text" value="<c:out value="${shippingmethod.shippingRank}"/>" name=__rank_<c:out value="${shippingmethod.id}"/> size="5" maxlength="5" class="rankField"></td>
			  </tr>
			</c:forEach>
			</table>
		  	<!-- end input field -->  	  
		  	</div>
		  	
		  	<!-- start button --> 
		  	<div align="left" class="button"> 
				<input type="submit" name="__update" value="<fmt:message key="Update" />">
			</div>	
		  	<!-- end button -->	
		  	
	<!-- end tab -->        
	</div>
<!-- end tabs -->			
</div>
  
  <!-- end table -->
   </td><td class="boxmidr" ></td></tr>
  <tr><td class="botl"></td><td class="botr"></td></tr>
  </table>
  
<!-- end main box -->  
 </div>  
</form>

<script type="text/javascript"> 
<!--
function toggleStateProvince(el) {
  if (el.value == "US") {
      document.getElementById('state').disabled=false;
      document.getElementById('state').style.display="block";
      document.getElementById('ca_province').disabled=true;
      document.getElementById('ca_province').style.display="none";
      document.getElementById('province').disabled=true;
      document.getElementById('province').style.display="none";
  } else if (el.value == "CA") {
      document.getElementById('state').disabled=true;
      document.getElementById('state').style.display="none";
      document.getElementById('ca_province').disabled=false;
      document.getElementById('ca_province').style.display="block";
      document.getElementById('province').disabled=true;
      document.getElementById('province').style.display="none";
  } else {
      document.getElementById('state').disabled=true;
      document.getElementById('state').style.display="none";
      document.getElementById('ca_province').disabled=true;
      document.getElementById('ca_province').style.display="none";
      document.getElementById('province').disabled=false;
      document.getElementById('province').style.display="block";
  }
}
toggleStateProvince(document.getElementById('country'));
//-->
</script>
</sec:authorize>

</tiles:putAttribute>    
</tiles:insertDefinition>
