<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="/WEB-INF/tlds/spring.tld" prefix="spring" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ taglib uri="http://www.springframework.org/security/tags" prefix="sec" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>

<tiles:insertDefinition name="admin.config" flush="true">
  <tiles:putAttribute name="content" type="string">
  
<script language="JavaScript" type="text/JavaScript">
<!--
window.addEvent('domready', function(){			
	var tabs1 = new SimpleTabs($('tab-block-1'), { selector: 'h4'});
});
//-->
</script>	
<form action="addLanguage.jhtm" method="post">

<div id="mboxfull">	
	
	<table cellpadding="0" cellspacing="0" border="0" class="module" >
 
	<tr>
	    <td class="topl_g">
		  
		  <!-- breadcrumb -->
		  <p class="breadcrumb">
		  	<a href="../config"><fmt:message key="configTabTitle"/></a> &gt;
		    <a href="../config/languageList.jhtm"><fmt:message key="languageList"/></a> &gt;
		    <fmt:message key="configuration"/> 
		  </p>
		  
		  <!-- Error Message -->
		  <c:if test="${!empty message}">
			<div class="message">
			  <fmt:message key="${message}">		    
			  </fmt:message>			  
			</div>
	  	  </c:if>
	        
	    </td><td class="topr_g" ></td>
	</tr>
	<tr><td class="boxmidlrg">
	
<!-- tabs -->
 <div id="tab-block-1">
	<h4 title="Add Language">Add Language</h4>
	<div>
	<!-- start tab -->

        <c:set var="classIndex" value="0" />
        
	  	<div class="listdivi ln tabdivi"></div>
	  	<div class="listdivi"></div>
   		
		  	<div class="list${classIndex % 2}"><c:set var="classIndex" value="${classIndex+1}" />
			<div class="listfl"><fmt:message key="language"/>:</div>
				<div class="listp">
				<!-- input field --> 
				<select name="language">
				<option value="0">Select Language</option>
				<option value="ar">Arabic</option>						        
				<option value="fr">French</option>
				<option value="de">German</option>	
				<option value="es">Spanish</option>						    
			    </select>
			<!-- end input field -->   	
			</div>
			</div>		   
    </div>
	
	</div>

	<!-- start button -->
	<div align="left" class="button">
		<sec:authorize ifAnyGranted="ROLE_AEM"> 
			<input type="submit" name="add" value="<fmt:message key="add" />">
		</sec:authorize>
	</div>
	<!-- end button -->			 
	
	  <!-- end table --> 
	</td><td class="boxmidr" ></td>	</tr>	
	</table>

</div>
</form>

</tiles:putAttribute>
</tiles:insertDefinition>