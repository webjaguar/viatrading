<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/security/tags" prefix="sec" %>

<tiles:insertDefinition name="admin.config.shipping" flush="true">
  <tiles:putAttribute name="content" type="string">
  
<sec:authorize ifAnyGranted="ROLE_AEM,ROLE_ADMIN_${prop_site_id},ROLE_SITE_INFO">  	   
<form id="list" action="customShippingContactInfoList.jhtm" method="post">
<input type="hidden" id="sort" name="sort" value="${contactInfoSearch.sort}" />
<!-- main box -->
  <div id="mboxfull">
  
  <!-- start table -->
  <table cellpadding="0" cellspacing="0" border="0" class="module" >
  <tr>
    <td class="topl_g">
	  
	  <!-- breadcrumb -->
	  <p class="breadcrumb">
	    <a href="../config"><fmt:message key="configTabTitle"/></a> &gt;
	    <a href="customShipping.jhtm"><fmt:message key="customShipping"/></a> &gt;
	    <fmt:message key="contactList"/>
	  </p>
	  
	  <!-- Error -->
	  <c:if test="${!empty model.message}">
		<div class="message"><fmt:message key="${model.message}" /></div>
	  </c:if> 
	  
	  <!-- header image -->
	  <img class="headerImage" src="../graphics/supplier.gif" />
	  
  </td><td class="topr_g" ></td></tr>
  <tr><td class="boxmidlrg" >

 <!-- tabs -->
 <div class="tab-wrapper">
	<h4 title=""></h4>
	<div>
	<!-- start tab -->

	  	<div class="listdivi ln tabdivi"></div>
	  	<div class="listdivi"></div>
	   
		  	<div class="listlight">
		  	<!-- input field -->
			<table border="0" cellpadding="0" cellspacing="1" width="100%">
			  <tr>
			  <c:if test="${model.tickets.nrOfElements > 0}">
			  <td class="pageShowing">
			  <fmt:message key="showing">
				<fmt:param value="${model.list.firstElementOnPage + 1}"/>
				<fmt:param value="${model.list.lastElementOnPage + 1}"/>
				<fmt:param value="${model.list.nrOfElements}"/>
			  </fmt:message>
			  </td>
			  </c:if>
			  <td class="pageNavi">
			  <fmt:message key="page" />
			  <select id="page" name="page" onchange="submit()">
			  <c:forEach begin="1" end="${model.list.pageCount}" var="page">
			  	<option value="${page}" <c:if test="${page == (model.list.page+1)}">selected</c:if>>${page}</option>
			  </c:forEach>
			  </select>
			  <fmt:message key="of" />&nbsp;<c:out value="${model.list.pageCount}"/>
			  | 
			  <c:if test="${model.list.firstPage}"><span class="pageNaviDead">previous</span></c:if>
			  <c:if test="${not model.list.firstPage}"><a href="<c:url value="ticketList.jhtm"><c:param name="page" value="${model.list.page}"/></c:url>" class="pageNaviLink">previous</a></c:if>
			  | 
			  <c:if test="${model.list.lastPage}"><span class="pageNaviDead">next</span></c:if>
			  <c:if test="${not model.list.lastPage}"><a href="<c:url value="ticketList.jhtm"><c:param name="page" value="${model.list.page+2}"/></c:url>" class="pageNaviLink">next</a></c:if>
			  </td>
			  </tr>
			</table>
			
			<table border="0" cellpadding="0" cellspacing="0" width="100%" class="listings">
			  <tr class="listingsHdr2">
			    <td class="indexCol">&nbsp;</td>
			    <td align="left">
			    	<table cellspacing="0" cellpadding="1">
			    	  <tr>
			    	    <c:choose>
			    	      <c:when test="${contactInfoSearch.sort == 'company DESC'}">
			    	        <td class="listingsHdr3">
			    	        <a class="hdrLink" href="#" onClick="document.getElementById('sort').value='company';document.getElementById('list').submit()"><fmt:message key="company" /></a>
			    	        </td>
			    	        <td><img src="../graphics/up.gif" border="0"></td>
			    	      </c:when>
			    	      <c:when test="${contactInfoSearch.sort == 'company'}">
			    	        <td class="listingsHdr3">
			    	        <a class="hdrLink" href="#" onClick="document.getElementById('sort').value='company DESC';document.getElementById('list').submit()"><fmt:message key="company" /></a>
			    	        </td>
			    	        <td><img src="../graphics/down.gif" border="0"></td>
			    	      </c:when>
			    	      <c:otherwise>
			    	        <td colspan="2" class="listingsHdr3">
			    	        <a class="hdrLink" href="#" onClick="document.getElementById('sort').value='company DESC';document.getElementById('list').submit()"><fmt:message key="company" /></a>
			    	        </td>
			    	      </c:otherwise>
			    	    </c:choose>  
			    	  </tr>
			    	  </table>
			    </td>
			    <td align="left">
			    	<table cellspacing="0" cellpadding="1">
			    	  <tr>
			    	    <c:choose>
			    	      <c:when test="${contactInfoSearch.sort == 'contact_name DESC'}">
			    	        <td class="listingsHdr3">
			    	        <a class="hdrLink" href="#" onClick="document.getElementById('sort').value='contact_name';document.getElementById('list').submit()"><fmt:message key="contactPerson" /></a>
			    	        </td>
			    	        <td><img src="../graphics/up.gif" border="0"></td>
			    	      </c:when>
			    	      <c:when test="${contactInfoSearch.sort == 'contact_name'}">
			    	        <td class="listingsHdr3">
			    	        <a class="hdrLink" href="#" onClick="document.getElementById('sort').value='contact_name DESC';document.getElementById('list').submit()"><fmt:message key="contactPerson" /></a>
			    	        </td>
			    	        <td><img src="../graphics/down.gif" border="0"></td>
			    	      </c:when>
			    	      <c:otherwise>
			    	        <td colspan="2" class="listingsHdr3">
			    	        <a class="hdrLink" href="#" onClick="document.getElementById('sort').value='contact_name DESC';document.getElementById('list').submit()"><fmt:message key="contactPerson" /></a>
			    	        </td>
			    	      </c:otherwise>
			    	    </c:choose>  
			    	  </tr>
			    	  </table>
			    </td>
			    <td class="listingsHdr3"><fmt:message key="email" /></td>
			    <td class="listingsHdr3"><fmt:message key="phone" />1</td>
			    <td class="listingsHdr3"><fmt:message key="phone" />2</td>
			    <td class="listingsHdr3"><fmt:message key="website" /></td>
			    <td class="listingsHdr3"><fmt:message key="trackUrl" /></td>
			  </tr>
			<c:forEach items="${model.list.pageList}" var="contact" varStatus="status">
			  <tr class="row${status.index % 2}" onmouseover="changeStyleClass(this,'rowOver')" onmouseout="changeStyleClass(this,'row${status.index % 2}')">
			    <td class="indexCol"><c:out value="${status.count + model.list.firstElementOnPage}" /> .</td>
			    <td class="nameCol" align="left"><c:out value="${contact.company}"/></td>
			    <td class="nameCol" align="left"><a href="customShippingContactInfo.jhtm?id=${contact.id}"><c:out value="${contact.contactName}"/></a></td>
			    <td class="nameCol"><c:out value="${contact.email}"/></td>
			    <td class="nameCol"><c:out value="${contact.tel1}"/></td>
			    <td class="nameCol"><c:out value="${contact.tel2}"/></td>
			    <td class="nameCol"><c:out value="${contact.website}"/></td>
			    <td class="nameCol"><a href="<c:out value="${contact.trackUrl}"/>" target="_blank"><c:out value="${contact.trackUrl}"/></a></td>
			  </tr>
			</c:forEach>
			<c:if test="${model.list.nrOfElements == 0}">
			  <tr class="emptyList"><td colspan="4">&nbsp;</td></tr>
			</c:if>
			</table>
			
			<table border="0" cellpadding="0" cellspacing="1" width="100%">
			  <tr>
			  <td class="pageSize">
			  <select name="size" onchange="document.getElementById('page').value=1;submit()">
			  <c:forTokens items="10,25,50,500" delims="," var="current">
			  	  <option value="${current}" <c:if test="${current == model.list.pageSize}">selected</c:if>>${current} <fmt:message key="perPage" /></option>
			  </c:forTokens>
			  </select>
			  </td>
			  </tr>
			</table>
		    <!-- end input field -->  	  
		  	
		  	<!-- start button -->
		  	<div align="left" class="button">
				<input type="submit" name="_add" value="<fmt:message key="add" />"/>
			</div>
			<!-- end button -->	
	        	  	
	<!-- end tab -->        
	</div>
<!-- end tabs -->			
</div></div>
  
  <!-- end table -->
   </td><td class="boxmidr" ></td></tr>
  <tr><td class="botl"></td><td class="botr"></td></tr>
  </table>
  
<!-- end main box -->  
</div>
</form>
</sec:authorize>

  </tiles:putAttribute>    
</tiles:insertDefinition>
