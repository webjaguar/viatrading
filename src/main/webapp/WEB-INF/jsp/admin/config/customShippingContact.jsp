<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="/WEB-INF/tlds/spring.tld" prefix="spring" %>
<%@ taglib uri="http://www.springframework.org/security/tags" prefix="sec" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>

<tiles:insertDefinition name="admin.config.shipping" flush="true">
  <tiles:putAttribute name="content" type="string">

<sec:authorize ifAnyGranted="ROLE_AEM,ROLE_ADMIN_${prop_site_id},ROLE_SITE_INFO"> 
<script type="text/javascript" >
<!--
window.addEvent('domready', function(){			
	var tabs1 = new SimpleTabs($('tab-block-1'), { selector: 'h4'});
});
//-->	
</script> 
<form:form commandName="contactForm" method="post">
<!-- main box -->
  <div id="mboxfull">
  
  <!-- start table -->
  <table cellpadding="0" cellspacing="0" border="0" class="module" >
  <tr>
    <td class="topl_g">
	  
	  <!-- breadcrumb -->
	  <p class="breadcrumb">
	    <a href="../config"><fmt:message key="configTabTitle"/></a> &gt;
	    <a href="customShipping.jhtm"><fmt:message key="customShipping"/></a> &gt;
	    <fmt:message key="contactList"/> &gt;
	    Form
	  </p>
	  
	  <!-- Error Message -->
	  <c:if test="${!empty message}">
		  <div class="message"><spring:message code="${message}" /></div>
	  </c:if>
	  
  </td><td class="topr_g" ></td></tr>
  <tr><td class="boxmidlrg" >

 <!-- tabs -->
 <div id="tab-block-1">
	<h4 title="">Contact Info</h4>
	<div>
	<!-- start tab -->
         <c:set var="classIndex" value="0" />
         
	  	<div class="listdivi ln tabdivi"></div>
	  	<div class="listdivi"></div>

  	       <div class="list${classIndex % 2}"><c:set var="classIndex" value="${classIndex+1}" />
		  	<div class="listfl"><fmt:message key='active' />:</div>
		  	<div class="listp">
		  	<!-- input field -->
				<form:checkbox path="contact.active" cssClass="textfield" /> 
			    <form:errors path="contact.active" cssClass="error" />
			<!-- end input field -->	
	 		</div>
		  	</div>
		  	
		  	
		  	<div class="list${classIndex % 2}"><c:set var="classIndex" value="${classIndex+1}" />
		  	<div class="listfl"><fmt:message key='company' />:</div>
		  	<div class="listp">
		  	<!-- input field -->
				<form:input path="contact.company" size="5" cssClass="textfield" /> 
			    <form:errors path="contact.company" cssClass="error" />
			<!-- end input field -->	
	 		</div>
		  	</div>
		  	
		  	<div class="list${classIndex % 2}"><c:set var="classIndex" value="${classIndex+1}" />
		  	<div class="listfl"><div class="requiredField"><fmt:message key='contactPerson' />:</div></div>
		  	<div class="listp">
		  	<!-- input field -->
				<form:input path="contact.contactName" size="5" cssClass="textfield" /> 
			    <form:errors path="contact.contactName" cssClass="error" />
			<!-- end input field -->	
	 		</div>
		  	</div>
		  	
		  	<div class="list${classIndex % 2}"><c:set var="classIndex" value="${classIndex+1}" />
		  	<div class="listfl"><fmt:message key='email' />:</div>
		  	<div class="listp">
		  	<!-- input field -->
				<form:input path="contact.email" size="5" cssClass="textfield" /> 
			    <form:errors path="contact.email" cssClass="error" />
			<!-- end input field -->	
	 		</div>
		  	</div>
		  	
		  	<div class="list${classIndex % 2}"><c:set var="classIndex" value="${classIndex+1}" />
		  	<div class="listfl">tel 1:</div>
		  	<div class="listp">
		  	<!-- input field -->
				<form:input path="contact.tel1" size="5" cssClass="textfield" /> 
			    <form:errors path="contact.tel1" cssClass="error" />
			<!-- end input field -->	
	 		</div>
		  	</div>
		  	
		  	<div class="list${classIndex % 2}"><c:set var="classIndex" value="${classIndex+1}" />
		  	<div class="listfl">Tel 2:</div>
		  	<div class="listp">
		  	<!-- input field -->
				<form:input path="contact.tel2" size="5" cssClass="textfield" /> 
			    <form:errors path="contact.tel2" cssClass="error" />
			<!-- end input field -->	
	 		</div>
		  	</div>
		  	
		  	<div class="list${classIndex % 2}"><c:set var="classIndex" value="${classIndex+1}" />
		  	<div class="listfl"><fmt:message key='website' />:</div>
		  	<div class="listp">
		  	<!-- input field -->
				<form:input path="contact.website" size="5" cssClass="textfield" /> 
			    <form:errors path="contact.website" cssClass="error" />
			<!-- end input field -->	
	 		</div>
		  	</div>
		  	
		  	<div class="list${classIndex % 2}"><c:set var="classIndex" value="${classIndex+1}" />
		  	<div class="listfl"><fmt:message key='trackUrl' />:</div>
		  	<div class="listp">
		  	<!-- input field -->
				<form:input path="contact.trackUrl" size="5" cssClass="textfield" /> 
			    <form:errors path="contact.trackUrl" cssClass="error" />
			<!-- end input field -->	
	 		</div>
		  	</div>
	<!-- end tab -->		
	</div>
<!-- end tabs -->			
</div>
<!-- start button -->
<div align="left" class="button">
<c:choose>
 <c:when test="${contactForm.newContact}">
   <input type="submit" value="<fmt:message key='add' />">
 </c:when>
 <c:otherwise>
   <input type="submit" value="<fmt:message key='Update' />">
   <input type="submit" value="<fmt:message key='delete' />" name="_delete" onClick="return confirm('Delete permanently?')" />
 </c:otherwise>
</c:choose>
  <input type="submit" value="<fmt:message key='cancel' />" name="_cancel">
	 
</div>	
<!-- end button -->	  
  <!-- end table -->
    </td><td class="boxmidr" ></td></tr>
  <tr><td class="botl"></td><td class="botr"></td></tr>
  </table>
  
<!-- end main box -->  
 </div>
</form:form>
</sec:authorize>

  </tiles:putAttribute>    
</tiles:insertDefinition>