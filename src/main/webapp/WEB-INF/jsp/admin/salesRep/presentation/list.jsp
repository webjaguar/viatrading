<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/security/tags" prefix="sec" %>

<tiles:insertDefinition name="admin.salesRep.presentation" flush="true">
	<tiles:putAttribute name="content" type="string">
<form action="templateList.jhtm" method="post" id="list">
<input type="hidden" id="sort" name="sort" value="${templateSearch.sort}" />
<!-- main box -->
  <div id="mboxfull">
  
  <!-- start table -->
  <table cellpadding="0" cellspacing="0" border="0" class="module" >
  <tr>
    <td class="topl_g">
	  
	  <!-- breadcrumb -->
	  <p class="breadcrumb">
	    <a href="../salesRep/templateList.jhtm"><fmt:message key="template" /></a> &gt;
	    <fmt:message key="template" />s
	  </p>
	  
	  <!-- Error Message -->
	  <c:if test="${model.message != null}">
        <div class="message"><c:out value="${model.message}"/></div>
      </c:if>	
      
      <!-- header image -->
	  <img class="headerImage" src="../graphics/salesRep.gif" />
      
	  </td><td class="topr_g" ></td></tr>
  <tr><td class="boxmidlrg" >

 <!-- tabs -->
 <div class="tab-wrapper">
	<h4 title="SalesRep"></h4>
	<div>
	<!-- start tab -->

	  	<div class="listdivi"></div>
	     
		  	<div class="listlight">
		  	<!-- input field --> 
		  	<table border="0" cellpadding="0" cellspacing="1" width="100%">
		    <tr>
			<c:if test="${model.templateList.nrOfElements > 0}">
				<td class="pageShowing">
					<fmt:message key="showing">
						<fmt:param value="${model.templateList.firstElementOnPage + 1}" />
						<fmt:param value="${model.templateList.lastElementOnPage + 1}" />
						<fmt:param value="${model.templateList.nrOfElements}" />
					</fmt:message>
				</td>
			</c:if>
			<td class="pageNavi">
				Page
				<select name="page" id="page" onchange="submit()">
					<c:forEach begin="1" end="${model.templateList.pageCount}"
						var="page">
						<option value="${page}"
							<c:if test="${page == (model.templateList.page+1)}">selected</c:if>>
							${page}
						</option>
					</c:forEach>
				</select> 
				of
				<c:out value="${model.templateList.pageCount}" /> 
				|
				<c:if test="${model.templateList.firstPage}">
					<span class="pageNaviDead"><fmt:message key="previous" /></span>
				</c:if>
				<c:if test="${not model.templateList.firstPage}">
					<a
						href="<c:url value="templateList.jhtm"><c:param name="page" value="${model.templateList.page}"/><c:param name="size" value="${model.templateList.pageSize}"/></c:url>"
						class="pageNaviLink"><fmt:message key="previous" /></a>
				</c:if>
				|
				<c:if test="${model.templateList.lastPage}">
					<span class="pageNaviDead"><fmt:message key="next" /></span>
				</c:if>
				<c:if test="${not model.templateList.lastPage}">
					<a
						href="<c:url value="templateList.jhtm"><c:param name="page" value="${model.templateList.page+2}"/><c:param name="size" value="${model.templateList.pageSize}"/></c:url>"
						class="pageNaviLink"><fmt:message key="next" /></a>
				</c:if>
				</td>
				</tr>
				</table>
			
				<table border="0" cellpadding="0" cellspacing="0" width="100%" class="listings">
					<tr class="listingsHdr2">
					<td class="indexCol">&nbsp;</td>
			    	<td>
			    	<table cellspacing="0" cellpadding="1">
			    	  <tr>
			    	    <c:choose>
			    	      <c:when test="${templateSearch.sort == 'name desc'}">
			    	        <td class="listingsHdr3">
			    	        <a class="hdrLink" href="#" onClick="document.getElementById('sort').value='name';document.getElementById('list').submit()"><fmt:message key="Name" /></a>
			    	        </td>
			    	        <td><img src="../graphics/up.gif" border="0"></td>
			    	      </c:when>
			    	      <c:when test="${templateSearch.sort == 'name'}">
			    	        <td class="listingsHdr3">
			    	        <a class="hdrLink" href="#" onClick="document.getElementById('sort').value='name desc';document.getElementById('list').submit()"><fmt:message key="Name" /></a>
			    	        </td>
			    	        <td><img src="../graphics/down.gif" border="0"></td>
			    	      </c:when>
			    	      <c:otherwise>
			    	        <td colspan="2" class="listingsHdr3">
			    	        <a class="hdrLink" href="#" onClick="document.getElementById('sort').value='name';document.getElementById('list').submit()"><fmt:message key="Name" /></a>
			    	        </td>
			    	      </c:otherwise>
			    	    </c:choose>  
			    	  </tr>
			    	  </table>
			   		</td>
			   		<td align="center" class="listingsHdr3">
						<fmt:message key="id" />
					</td>	
			    	<td align="center">
			          <table cellspacing="0" cellpadding="1">
			    	  <tr>
			    	    <td rowspan="2" class="listingsHdr3">Product Per Page</td>
			          </tr>
			    	  </table>
			    	</td>	
					</tr>
					<c:forEach items="${model.templateList.pageList}" var="template"	varStatus="status">
						<tr class="row${status.index % 2}"	onmouseover="changeStyleClass(this,'rowOver')" 	onmouseout="changeStyleClass(this,'row${status.index % 2}')">
							<td class="indexCol">
								<c:out value="${status.count + model.templateList.firstElementOnPage}" />.
							</td>
							<td class="nameCol">
								<a href="templateForm.jhtm?id=${template.id}" class="nameLink"><c:out value="${template.name}" /></a>
							</td>
							<td align="center">
								<c:out value="${template.id}" />
							</td>
							<td align="center">
								<c:out value="${template.noOfProduct}" />
							</td>
						</tr>
					</c:forEach>
					<c:if test="${model.templateList.nrOfElements == 0}">
						<tr class="emptyList">
							<td colspan="5">
								&nbsp;
							</td>
						</tr>
					</c:if>
				</table>
	        
	        <table border="0" cellpadding="0" cellspacing="1" width="100%">
				<tr>
				  <td class="pageSize">
					<select name="size"
						onchange="document.getElementById('page').value=1;submit()">
						<c:forTokens items="10,25,50,100" delims="," var="current">
							<option value="${current}"
								<c:if test="${current == model.templateList.pageSize}">selected</c:if>>
								${current}
								<fmt:message key="perPage" />
							</option>
						</c:forTokens>
					</select>
				  </td>
				</tr>
			</table>		  	
	        
		  	<!-- end input field -->  	  
		  	</div>
		  	
		  	<!-- start button --> 
		  	<div align="left" class="button">
			  <sec:authorize ifAnyGranted="ROLE_AEM,ROLE_ADMIN_${prop_site_id},ROLE_SALES_REP_CREATE">
				<input type="submit" name="__add" value="<fmt:message key="add" /> <fmt:message key="template" />"/>
			  </sec:authorize>	
			</div>    	
			<!-- end button -->	
 	
	<!-- end tab -->        
	</div>
<!-- end tabs -->			
</div>
  
  <!-- end table -->
   </td><td class="boxmidr" ></td></tr>
  <tr><td class="botl"></td><td class="botr"></td></tr>
  </table>
  
<!-- end main box -->  
</div> 
</form>	

	</tiles:putAttribute>
</tiles:insertDefinition>