<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="/WEB-INF/tlds/spring.tld" prefix="spring" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib uri="http://www.springframework.org/security/tags" prefix="sec" %>

<tiles:insertDefinition name="admin.salesRep.presentation" flush="true">
  <tiles:putAttribute name="content" type="string"> 

<c:if test="${gSiteConfig['gSALES_REP'] == true}">
<script type="text/javascript" src="<c:url value="/FCKeditor/fckeditor.js"/>"></script>
<script language="JavaScript"> 
<!--
window.addEvent('domready', function(){			
			var tabs1 = new SimpleTabs($('tab-block-1'), { selector: 'h4'});
			var Tips1 = new Tips($$('.toolTipImg'));
		});
//--> 
function loadEditor(el) {
  var oFCKeditor = new FCKeditor(el);
  oFCKeditor.BasePath = "<c:url value="/FCKeditor/"/>";
  oFCKeditor.Config["CustomConfigurationsPath"] = '<c:url value="/admin/javascript/fckconfig.js"/>';
  oFCKeditor.ToolbarSet = 'Html';
  oFCKeditor.Width = 650;
  oFCKeditor.Height = 400;
  oFCKeditor.ReplaceTextarea();
  document.getElementById(el + '_link').style.display="none";
}

</script>
<form:form commandName="templateForm" method="post" >
<!-- main box -->
  <div id="mboxfull">
  
  <!-- start table -->
  <table cellpadding="0" cellspacing="0" border="0" class="module" >
  <tr>
    <td class="topl_g">
	  
	  <!-- breadcrumb -->
	  <p class="breadcrumb">
	    <a href="../salesRep/templateList.jhtm"><fmt:message key="template" /></a> &gt;
	    <c:if test="${templateForm.newTemplate}">form</c:if><c:if test="${!templateForm.newTemplate}">${templateForm.template.name}</c:if>
	  </p>
	  
	  <!-- Error Message -->
	  <c:if test="${message != null}">
	    <div class="message"><spring:message code="${message}"/></div>
	  </c:if>
	  <spring:hasBindErrors name="templateForm">
		<div class="message">Please fix all errors!</div>
	  </spring:hasBindErrors>
	  
  <div align="right">	
    <table class="form">
		<c:if test="${not templateForm.newTemplate}">
		  <c:if test="${templateForm.template.created != null}">
		  <tr>
		    <td align="right"><fmt:message key="dateAdded" /> : </td>
		    <td><fmt:formatDate type="both" timeStyle="full" value="${templateForm.template.created}"/></td>
		  </tr>
		  </c:if>
		  <c:if test="${templateForm.template.modified != null}">
		  <tr>
		    <td align="right"><fmt:message key="lastModified" /> : </td>
		    <td><fmt:formatDate type="both" timeStyle="full" value="${templateForm.template.modified}"/></td>
		  </tr>
		  </c:if>		  
		</c:if>
	</table>
  </div>

	  
  </td><td class="topr_g" ></td></tr>
  <tr><td class="boxmidlrg" >

 <!-- tabs -->
 <div id="tab-block-1">
	<h4 title="<fmt:message key='template'/> Form"><fmt:message key="template" /></h4>
	<div>
	<!-- start tab -->
         <c:set var="classIndex" value="0" />
         
	  	<div class="listdivi"></div>
	  	
		  	<div class="list${classIndex % 2}"><c:set var="classIndex" value="${classIndex+1}" />
		  	<div class="listfl"><div class="requiredField"><fmt:message key='Name' />:</div></div>
		  	<div class="listp">
		  	<!-- input field -->
				<form:input path="template.name" htmlEscape="true"/>
				<form:errors path="template.name" cssClass="error"/> 		
			<!-- end input field -->	
	 		</div>
		  	</div>	
		  	
		  	<div class="list${classIndex % 2}"><c:set var="classIndex" value="${classIndex+1}" />
		  	<div class="listfl"><div class="requiredField">Product Per Page:</div></div>
		  	<div class="listp">
		  	<!-- input field -->
		    <form:select path="template.noOfProduct">
			    <c:forEach begin="1" end="20" var="index">
			    	<form:option value="${index}">${index}</form:option>
			    </c:forEach>
		        </form:select>
	 		<!-- end input field -->	
	 		</div>
		  	</div>	
		  	
		  	
	  	 <div class="list${classIndex % 2}"><c:set var="classIndex" value="${classIndex+1}" />
		 <div class="listfl"><fmt:message key="htmlCode" />:</div>
		 <div class="listp">
		 <!-- input field -->
                <form:textarea rows="16" cols="70" cssClass="textfield" path="template.design" htmlEscape="true" />
                <div style="text-align:left"  id="template.design"><a href="#" onClick="loadEditor('template.design')" class="htmleditor">- <fmt:message key="htmlEditor" /> -</a></div>
		 <!-- end input field -->   	
		 </div>
		 </div>
		 <div class="list${classIndex % 2}"><c:set var="classIndex" value="${classIndex+1}" />
		 <div class="listfl"><fmt:message key="footer"/> <fmt:message key="htmlCode" />:</div>
		 <div class="listp">
		 <!-- input field -->
                <form:textarea rows="16" cols="70" cssClass="textfield" path="template.footerHtmlCode" htmlEscape="true" />
                <div style="text-align:left"  id="template.footerHtmlCode"><a href="#" onClick="loadEditor('template.footerHtmlCode')" class="htmleditor">- <fmt:message key="htmlEditor" /> -</a></div>
		 <!-- end input field -->   	
		 </div>
		 </div>
		 <div class="list${classIndex % 2}"><c:set var="classIndex" value="${classIndex+1}" />
		  	<div class="listfl"><div class="helpImg"><img class="toolTipImg" title="Note::#sku# on the HTML Code or Head Tag will be dynamically replaced by the sku of the product" src="../graphics/question.gif" /></div>Dynamic Element:</div>
		  	<div class="listp">
		  	<!-- input field -->
		  	     <span class="static">
		  	     #presentationTitle# --&gt; Presentation Title<br/>
		  	     #image1# --&gt; Product Image<br/>
		  		 #sku1# --&gt; <fmt:message key="productSku" /><br/>
		  		 #productName# --&gt; <fmt:message key="Name" /><br/>
		  		 #shortDesc# --&gt; <fmt:message key="productShortDesc" /><br/>
		  		 #salesRepName# --&gt; <fmt:message key="salesRep" /> <fmt:message key="name"/><br/>
		  		 #created# --&gt; <fmt:message key="created" /><br/>
		  		 #note# --&gt; <fmt:message key="note" /><br/>
		  		 
		  		 </span>
	 		<!-- end input field -->	
	 		</div>
		  </div>

	<!-- end tab -->		
	</div>
	</div>	
	
	<div>
	  		  	
	
<!-- end tabs -->			


<!-- start button -->
<div align="left" class="button">
	<c:choose>
		<c:when test="${templateForm.newTemplate}">
		  <sec:authorize ifAnyGranted="ROLE_AEM,ROLE_ADMIN_${prop_site_id},ROLE_SALES_REP_CREATE">
			<input type="submit" value="<fmt:message key='add' />" /> 
		  </sec:authorize>	
		</c:when>
		<c:otherwise>
		  <sec:authorize ifAnyGranted="ROLE_AEM,ROLE_ADMIN_${prop_site_id},ROLE_SALES_REP_EDIT">
			<input type="submit" value="<fmt:message key='Update' />" />
		  </sec:authorize>	 
		  <sec:authorize ifAnyGranted="ROLE_AEM,ROLE_ADMIN_${prop_site_id},ROLE_SALES_REP_DELETE">	
			<input type="submit" name="delete" value="<fmt:message key='delete' />" onclick="return confirm('Are you sure you wish to delete this <fmt:message key="template" />?')"/>
		  </sec:authorize>	  
		</c:otherwise>
	</c:choose>
	<input type="submit" value="<spring:message code="cancel"/>" name="_cancel">
</div>
<!-- end button -->	
</div>
  <!-- end table -->
    </td><td class="boxmidr" ></td></tr>
  <tr><td class="botl"></td><td class="botr"></td></tr>
  </table>
  
<!-- end main box -->  
 </div>
</form:form>
</c:if>

  </tiles:putAttribute>    
</tiles:insertDefinition>