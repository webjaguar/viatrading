<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>


  <div id="lbox">
   <table cellpadding="0" cellspacing="0" border="0" class="module" style="width: 100%"><tr><td class="topl">
    </td><td class="topr" ></td></tr><tr><td class="boxmid" >
  
    <h2 class="menuleft mfirst"><fmt:message key="salesRepMenuTitle"/></h2>
    <div class="menudiv"></div>
    <a href="salesRepList.jhtm" class="userbutton"><fmt:message key="list"/></a> 
    <div class="menudiv"></div>
    <c:if test="${gSiteConfig['gPRESENTATION']}">
	   <h2 class="menuleft"><fmt:message key="template"/></h2>
	   <div class="menudiv"></div>
	   <a href="templateList.jhtm" class="userbutton"><fmt:message key="list"/></a> 
	   <div class="menudiv"></div>
	</c:if>
		<div class="leftbar_searchWrapper">
		
	<c:if test="${param.tab == 'salesRep'}">
	    <div class="menudiv"></div>
	      <div align="left" class="searchHeader" id="searchHeaderId"><h2><fmt:message key="search" /></h2></div>
		  <table class="searchBoxLeft"  width="100%"><tr><td>
		  <form action="salesRepList.jhtm" name="searchform" method="post">
		  <!-- content area --> 
		    <div class="search2">
		      <p><fmt:message key="Name" />:</p>
	          <input name="name" type="text" value="<c:out value='${salesRepSearch.name}' />" size="15" />
		    </div>    
		    <div class="search2">
	          <p><fmt:message key="accountNumber" />:</p>
	          <input name="accountNumber" type="text" value="<c:out value='${salesRepSearch.accountNumber}' />" size="15" />
	        </div> 
		    <div class="search2">
	          <p><fmt:message key="email" />:</p>
	          <input name="email" type="text" value="<c:out value='${salesRepSearch.email}' />" size="15" />
	        </div>
	        <div class="search2">
	      	<p><fmt:message key="group" />:</p>
			<select name="salesrepGroup">
			<option value="" ></option>
            <option value="blank"<c:if test="${salesRepSearch.salesrepGroup == 'blank' }">selected</c:if>>Blank</option>
            <option value="nonblank"<c:if test="${salesRepSearch.salesrepGroup == 'nonblank' }">selected</c:if>>Non blank</option>
            <c:forEach items="${model.groupList}" var="salesrepGroup" varStatus="status">
              <option value="${salesrepGroup}"<c:if test="${salesRepSearch.salesrepGroup == salesrepGroup}">selected</c:if>>${salesrepGroup}</option>
            </c:forEach>           
        	</select>
	    	</div>  
		    <div class="button">
		      <input type="submit" value="Search"/>
		    </div>
		  <!-- content area -->
		  </form>
		  </td></tr></table>
		  
	</c:if>
	<c:if test="${gSiteConfig['gPRESENTATION']}">
	<c:if test="${param.tab == 'presentation'}">
	    <div class="menudiv"></div>
	      <div align="left" class="searchHeader" id="searchHeaderId"><h2><fmt:message key="search" /></h2></div>
		  <table class="searchBoxLeft"  width="100%"><tr><td>
		  <form action="templateList.jhtm" name="searchform" method="post">
		  <!-- content area --> 
		    <div class="search2">
		      <p><fmt:message key="Name" />:</p>
	          <input name="name" type="text" value="<c:out value='${templateSearch.name}' />" size="15" />
		    </div> 
		    <div class="button">
		      <input type="submit" value="Search"/>
		    </div>
		  <!-- content area -->
		  </form>
		  </td></tr></table>
	</c:if>
	</c:if>
		<div class="menudiv"></div>  
		</div>
    </td><td class="boxmidr" ></td></tr>
    <tr><td class="botl"></td><td class="botr"></td></tr></table>
  </div>