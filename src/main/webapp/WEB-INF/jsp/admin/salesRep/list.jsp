<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/security/tags" prefix="sec" %>

<tiles:insertDefinition name="admin.salesRep" flush="true">
	<tiles:putAttribute name="content" type="string">

<sec:authorize ifAnyGranted="ROLE_AEM,ROLE_ADMIN_${prop_site_id},ROLE_SALES_REP_CREATE,ROLE_SALES_REP_VIEW_LIST">
<c:if test="${gSiteConfig['gSALES_REP'] == true}">
<form action="salesRepList.jhtm" method="post" id="list">
<input type="hidden" id="sort" name="sort" value="${salesRepSearch.sort}" />
<!-- main box -->
  <div id="mboxfull">
  
  <!-- start table -->
  <table cellpadding="0" cellspacing="0" border="0" class="module" >
  <tr>
    <td class="topl_g">
	  
	  <!-- breadcrumb -->
	  <p class="breadcrumb">
	    <a href="../salesRep"><fmt:message key="salesRep" /></a> &gt;
	    <fmt:message key="salesReps" />
	  </p>
	  
	  <!-- Error Message -->
	  <c:if test="${model.message != null}">
        <div class="message"><c:out value="${model.message}"/></div>
      </c:if>	
      
      <!-- header image -->
	  <img class="headerImage" src="../graphics/salesRep.gif" />
      
	  </td><td class="topr_g" ></td></tr>
  <tr><td class="boxmidlrg" >

 <!-- tabs -->
 <div class="tab-wrapper">
	<h4 title="SalesRep"></h4>
	<div>
	<!-- start tab -->

	  	<div class="listdivi"></div>
	     
		  	<div class="listlight">
		  	<!-- input field --> 
		  	<table border="0" cellpadding="0" cellspacing="1" width="100%">
		    <tr>
			  <c:if test="${siteConfig['SALESREP_PARENT'].value == 'true'}">
			  <td>
				<select name="parent"
				  onChange="document.getElementById('page').value=1;submit()" style="width:200px">
					<option value=""><fmt:message key="all" /> <fmt:message key="accounts" /></option>
					<option value="-1" <c:if test="${-1 == salesRepSearch['parent']}">selected</c:if>>
						<fmt:message key="main" /> <fmt:message key="accounts" /></option>		
					<c:forEach items="${model.salesRepTree}" var="salesRep">
					<option value="${salesRep.id}" <c:if test="${salesRep.id == salesRepSearch['parent']}">selected</c:if>>
						<fmt:message key="subAccounts" /> <fmt:message key="of" /> <c:out value="${salesRep.name}" /></option>
				</c:forEach>
				</select>
			  </td>
			  </c:if>
			<c:if test="${model.salesReps.nrOfElements > 0}">
				<td class="pageShowing">
					<fmt:message key="showing">
						<fmt:param value="${model.salesReps.firstElementOnPage + 1}" />
						<fmt:param value="${model.salesReps.lastElementOnPage + 1}" />
						<fmt:param value="${model.salesReps.nrOfElements}" />
					</fmt:message>
				</td>
			</c:if>
			<td class="pageNavi">
				Page
				<select name="page" id="page" onchange="submit()">
					<c:forEach begin="1" end="${model.salesReps.pageCount}"
						var="page">
						<option value="${page}"
							<c:if test="${page == (model.salesReps.page+1)}">selected</c:if>>
							${page}
						</option>
					</c:forEach>
				</select> 
				of
				<c:out value="${model.salesReps.pageCount}" /> 
				|
				<c:if test="${model.salesReps.firstPage}">
					<span class="pageNaviDead"><fmt:message key="previous" /></span>
				</c:if>
				<c:if test="${not model.salesReps.firstPage}">
					<a
						href="<c:url value="salesRepList.jhtm"><c:param name="page" value="${model.salesReps.page}"/><c:param name="size" value="${model.salesReps.pageSize}"/></c:url>"
						class="pageNaviLink"><fmt:message key="previous" /></a>
				</c:if>
				|
				<c:if test="${model.salesReps.lastPage}">
					<span class="pageNaviDead"><fmt:message key="next" /></span>
				</c:if>
				<c:if test="${not model.salesReps.lastPage}">
					<a
						href="<c:url value="salesRepList.jhtm"><c:param name="page" value="${model.salesReps.page+2}"/><c:param name="size" value="${model.salesReps.pageSize}"/></c:url>"
						class="pageNaviLink"><fmt:message key="next" /></a>
				</c:if>
				</td>
				</tr>
				</table>
			
				<table border="0" cellpadding="0" cellspacing="0" width="100%" class="listings">
					<tr class="listingsHdr2">
					<td class="indexCol">&nbsp;</td>
			    	<td>
			    	<table cellspacing="0" cellpadding="1">
			    	  <tr>
			    	    <c:choose>
			    	      <c:when test="${salesRepSearch.sort == 'name desc'}">
			    	        <td class="listingsHdr3">
			    	        <a class="hdrLink" href="#" onClick="document.getElementById('sort').value='name';document.getElementById('list').submit()"><fmt:message key="Name" /></a>
			    	        </td>
			    	        <td><img src="../graphics/up.gif" border="0"></td>
			    	      </c:when>
			    	      <c:when test="${salesRepSearch.sort == 'name'}">
			    	        <td class="listingsHdr3">
			    	        <a class="hdrLink" href="#" onClick="document.getElementById('sort').value='name desc';document.getElementById('list').submit()"><fmt:message key="Name" /></a>
			    	        </td>
			    	        <td><img src="../graphics/down.gif" border="0"></td>
			    	      </c:when>
			    	      <c:otherwise>
			    	        <td colspan="2" class="listingsHdr3">
			    	        <a class="hdrLink" href="#" onClick="document.getElementById('sort').value='name';document.getElementById('list').submit()"><fmt:message key="Name" /></a>
			    	        </td>
			    	      </c:otherwise>
			    	    </c:choose>  
			    	  </tr>
			    	  </table>
			   		</td>
			   		<td class="listingsHdr3">
						<fmt:message key="salesRepId" />
					</td>					
			    	<td align="center">
			    	<table cellspacing="0" cellpadding="1">
			    	  <tr>
			    	    <c:choose>
			    	      <c:when test="${salesRepSearch.sort == 'email desc'}">
			    	        <td class="listingsHdr3">
			    	        <a class="hdrLink" href="#" onClick="document.getElementById('sort').value='email';document.getElementById('list').submit()"><fmt:message key="email" /></a>
			    	        </td>
			    	        <td><img src="../graphics/up.gif" border="0"></td>
			    	      </c:when>
			    	      <c:when test="${salesRepSearch.sort == 'email'}">
			    	        <td class="listingsHdr3">
			    	        <a class="hdrLink" href="#" onClick="document.getElementById('sort').value='email desc';document.getElementById('list').submit()"><fmt:message key="email" /></a>
			    	        </td>
			    	        <td><img src="../graphics/down.gif" border="0"></td>
			    	      </c:when>
			    	      <c:otherwise>
			    	        <td colspan="2" class="listingsHdr3">
			    	        <a class="hdrLink" href="#" onClick="document.getElementById('sort').value='email';document.getElementById('list').submit()"><fmt:message key="email" /></a>
			    	        </td>
			    	      </c:otherwise>
			    	    </c:choose>  
			    	  </tr>
			    	  </table>
			   		</td>
			    	<td align="center">
			    	<table cellspacing="0" cellpadding="1">
			    	  <tr>
			    	    <c:choose>
			    	      <c:when test="${salesRepSearch.sort == 'sales_rep_account_num desc'}">
			    	        <td class="listingsHdr3">
			    	        <a class="hdrLink" href="#" onClick="document.getElementById('sort').value='sales_rep_account_num';document.getElementById('list').submit()"><fmt:message key="accountNumber" /></a>
			    	        </td>
			    	        <td><img src="../graphics/up.gif" border="0"></td>
			    	      </c:when>
			    	      <c:when test="${salesRepSearch.sort == 'sales_rep_account_num'}">
			    	        <td class="listingsHdr3">
			    	        <a class="hdrLink" href="#" onClick="document.getElementById('sort').value='sales_rep_account_num desc';document.getElementById('list').submit()"><fmt:message key="accountNumber" /></a>
			    	        </td>
			    	        <td><img src="../graphics/down.gif" border="0"></td>
			    	      </c:when>
			    	      <c:otherwise>
			    	        <td colspan="2" class="listingsHdr3">
			    	        <a class="hdrLink" href="#" onClick="document.getElementById('sort').value='sales_rep_account_num';document.getElementById('list').submit()"><fmt:message key="accountNumber" /></a>
			    	        </td>
			    	      </c:otherwise>
			    	    </c:choose>  
			    	  </tr>
			    	  </table>
			   		</td>
			   		<td align="center"><fmt:message key="group" /></td>
			    	<c:if test="${siteConfig['SALESREP_PARENT'].value == 'true'}">
			    	<td align="center">
			          <table cellspacing="0" cellpadding="1">
			    	  <tr>
			    	    <td rowspan="2" class="listingsHdr3"><fmt:message key="subAccounts" /></td>
			          </tr>
			    	  </table>
			    	</td>
			    	</c:if>
			    	<td align="center">
			          <table cellspacing="0" cellpadding="1">
			    	  <tr>
			    	    <td rowspan="2" class="listingsHdr3"><fmt:message key="phone" /></td>
			          </tr>
			    	  </table>
			    	</td>
			    	<c:if test="${siteConfig['SALESREP_LOGIN'].value == 'true'}">
				    	<td align="center">
				    	<table cellspacing="0" cellpadding="1">
				    	  <tr>
				    	    <c:choose>
				    	      <c:when test="${salesRepSearch.sort == 'salesrep_num_of_logins desc'}">
				    	        <td class="listingsHdr3">
				    	        <a class="hdrLink" href="#" onClick="document.getElementById('sort').value='salesrep_num_of_logins';document.getElementById('list').submit()"><fmt:message key="numOfLogins" /></a>
				    	        </td>
				    	        <td><img src="../graphics/up.gif" border="0"></td>
				    	      </c:when>
				    	      <c:when test="${salesRepSearch.sort == 'salesrep_num_of_logins'}">
				    	        <td class="listingsHdr3">
				    	        <a class="hdrLink" href="#" onClick="document.getElementById('sort').value='salesrep_num_of_logins desc';document.getElementById('list').submit()"><fmt:message key="numOfLogins" /></a>
				    	        </td>
				    	        <td><img src="../graphics/down.gif" border="0"></td>
				    	      </c:when>
				    	      <c:otherwise>
				    	        <td colspan="2" class="listingsHdr3">
				    	        <a class="hdrLink" href="#" onClick="document.getElementById('sort').value='salesrep_num_of_logins';document.getElementById('list').submit()"><fmt:message key="numOfLogins" /></a>
				    	        </td>
				    	      </c:otherwise>
				    	    </c:choose>  
				    	  </tr>
				    	  </table>
				   		</td>
			    	</c:if>	
			    	<c:if test="${siteConfig['SALESREP_LOGIN'].value == 'true'}">
				    	<td align="center">
				    	<table cellspacing="0" cellpadding="1">
				    	  <tr>
				    	    <c:choose>
				    	      <c:when test="${salesRepSearch.sort == 'salesrep_last_login desc'}">
				    	        <td class="listingsHdr3">
				    	        <a class="hdrLink" href="#" onClick="document.getElementById('sort').value='salesrep_last_login';document.getElementById('list').submit()"><fmt:message key="lastLogin" /></a>
				    	        </td>
				    	        <td><img src="../graphics/up.gif" border="0"></td>
				    	      </c:when>
				    	      <c:when test="${salesRepSearch.sort == 'salesrep_last_login'}">
				    	        <td class="listingsHdr3">
				    	        <a class="hdrLink" href="#" onClick="document.getElementById('sort').value='salesrep_last_login desc';document.getElementById('list').submit()"><fmt:message key="lastLogin" /></a>
				    	        </td>
				    	        <td><img src="../graphics/down.gif" border="0"></td>
				    	      </c:when>
				    	      <c:otherwise>
				    	        <td colspan="2" class="listingsHdr3">
				    	        <a class="hdrLink" href="#" onClick="document.getElementById('sort').value='salesrep_last_login';document.getElementById('list').submit()"><fmt:message key="lastLogin" /></a>
				    	        </td>
				    	      </c:otherwise>
				    	    </c:choose>  
				    	  </tr>
				    	  </table>
				   		</td>
			    	</c:if>	
					</tr>
					<c:forEach items="${model.salesReps.pageList}" var="salesRep"	varStatus="status">
						<tr class="row${status.index % 2}"	onmouseover="changeStyleClass(this,'rowOver')" 	onmouseout="changeStyleClass(this,'row${status.index % 2}')">
							<td class="indexCol">
								<c:out value="${status.count + model.salesReps.firstElementOnPage}" />.
							</td>
							<td class="nameCol">
								<a href="salesRep.jhtm?id=${salesRep.id}" class="nameLink<c:if test="${salesRep.inactive}">Inactive</c:if>"><c:out value="${salesRep.name}" /></a>
							</td>
							<td align="center">
								<c:out value="${salesRep.id}" />
							</td>
							<td align="center">
								<c:out value="${salesRep.email}" />
							</td>
							<td align="center">
								<c:out value="${salesRep.accountNumber}" />
							</td>
							<td align="center">
								<c:out value="${salesRep.salesrepGroup}" />
							</td>
							<c:if test="${siteConfig['SALESREP_PARENT'].value == 'true'}">
							<td align="center">
								<c:if test="${salesRep.subCount > 0}">
								<a href="salesRepList.jhtm?parent=${salesRep.id}" class="nameLink"><c:out value="${salesRep.subCount}" /></a>
								</c:if>
							</td>
							</c:if>
							<td align="center">
								<c:out value="${salesRep.phone}" />
							</td>
							<c:if test="${siteConfig['SALESREP_LOGIN'].value == 'true'}">
								<td align="center">
									<c:out value="${salesRep.numOfLogins}" />
								</td>
								<td align="center">
									<fmt:formatDate type="date" timeStyle="default" value="${salesRep.lastLogin}"/>
								</td>
							</c:if>
						</tr>
					</c:forEach>
					<c:if test="${model.salesReps.nrOfElements == 0}">
						<tr class="emptyList">
							<td colspan="5">
								&nbsp;
							</td>
						</tr>
					</c:if>
				</table>
				
				<c:if test="${model.salesReps.nrOfElements > 0}">
					<div class="sup">
						&sup1; <fmt:message key="toEditClickSalesRepName" /> 
					</div>
				</c:if>
	        
	        <table border="0" cellpadding="0" cellspacing="1" width="100%">
				<tr>
				  <td class="pageSize">
					<select name="size"
						onchange="document.getElementById('page').value=1;submit()">
						<c:forTokens items="10,25,50,100" delims="," var="current">
							<option value="${current}"
								<c:if test="${current == model.salesReps.pageSize}">selected</c:if>>
								${current}
								<fmt:message key="perPage" />
							</option>
						</c:forTokens>
					</select>
				  </td>
				</tr>
			</table>		  	
	        
		  	<!-- end input field -->  	  
		  	</div>
		  	
		  	<!-- start button --> 
		  	<div align="left" class="button">
			  <sec:authorize ifAnyGranted="ROLE_AEM,ROLE_ADMIN_${prop_site_id},ROLE_SALES_REP_CREATE">
				<input type="submit" name="__add" value="<fmt:message key="add" /> <fmt:message key="salesRep" />"/>
			  </sec:authorize>	
			</div>    	
			<!-- end button -->	
 	
	<!-- end tab -->        
	</div>
<!-- end tabs -->			
</div>
  
  <!-- end table -->
   </td><td class="boxmidr" ></td></tr>
  <tr><td class="botl"></td><td class="botr"></td></tr>
  </table>
  
<!-- end main box -->  
</div> 
</form>	 
</c:if> 
</sec:authorize>

	</tiles:putAttribute>
</tiles:insertDefinition>