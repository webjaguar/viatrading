<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="/WEB-INF/tlds/spring.tld" prefix="spring" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib uri="http://www.springframework.org/security/tags" prefix="sec" %>

<tiles:insertDefinition name="admin.salesRep" flush="true">
  <tiles:putAttribute name="content" type="string"> 

<c:if test="${gSiteConfig['gSALES_REP'] == true}">
<script language="JavaScript"> 
<!--
window.addEvent('domready', function(){			
	var tabs1 = new SimpleTabs($('tab-block-1'), { selector: 'h4'});
	var Tips1 = new Tips($$('.toolTipImg'));
});
//--> 
</script>
<form:form commandName="salesRepForm" method="post" >
<!-- main box -->
  <div id="mboxfull">
  
  <!-- start table -->
  <table cellpadding="0" cellspacing="0" border="0" class="module" >
  <tr>
    <td class="topl_g">
	  
	  <!-- breadcrumb -->
	  <p class="breadcrumb">
	    <a href="../salesRep"><fmt:message key="salesRep" /></a> &gt;
	    <c:if test="${salesRepForm.newSalesRep}">form</c:if><c:if test="${!salesRepForm.newSalesRep}">${salesRepForm.salesRep.name}</c:if>
	  </p>
	  
	  <!-- Error Message -->
	  <c:if test="${message != null}">
	    <div class="message"><spring:message code="${message}"/></div>
	  </c:if>
	  <spring:hasBindErrors name="salesRepForm">
		<div class="message">Please fix all errors!</div>
	  </spring:hasBindErrors>
	  
  <div align="right">	
    <table class="form">
		<c:if test="${not salesRepForm.newSalesRep}">
		  <c:if test="${salesRepForm.salesRep.created != null}">
		  <tr>
		    <td align="right"><fmt:message key="dateAdded" /> : </td>
		    <td><fmt:formatDate type="both" timeStyle="full" value="${salesRepForm.salesRep.created}"/></td>
		  </tr>
		  </c:if>
		  <c:if test="${salesRepForm.salesRep.lastModified != null}">
		  <tr>
		    <td align="right"><fmt:message key="lastModified" /> : </td>
		    <td><fmt:formatDate type="both" timeStyle="full" value="${salesRepForm.salesRep.lastModified}"/></td>
		  </tr>
		  </c:if>
		  <c:if test="${siteConfig['SALESREP_LOGIN'].value == 'true'}">		  
		  <tr>
		    <td align="right"><fmt:message key="lastLogin" /> : </td>
		    <td><fmt:formatDate type="both" timeStyle="full" value="${salesRepForm.salesRep.lastLogin}"/></td>
		  </tr>
		  <tr>
		    <td align="right"><fmt:message key="numOfLoginsFull" /> : </td>
		    <td><c:out value="${salesRepForm.salesRep.numOfLogins}"/></td>
		  </tr>
		  </c:if>		  
		</c:if>
	</table>
  </div>

	  
  </td><td class="topr_g" ></td></tr>
  <tr><td class="boxmidlrg" >

 <!-- tabs -->
 <div id="tab-block-1">
	<h4 title="<fmt:message key='salesRep'/> Form"><fmt:message key="salesRep" /></h4>
	<div>
	<!-- start tab -->
         <c:set var="classIndex" value="0" />
         
	  	<div class="listdivi ln tabdivi"></div>
	  	<div class="listdivi"></div>
			
			<div class="list${classIndex % 2}"><c:set var="classIndex" value="${classIndex+1}" />
		  	<div class="listfl"><div class="helpImg"><img class="toolTipImg" title="Inactive::Checking this, will not allow this SalesRep to be displayed on salesRep dropdown. Only when this salesRep doesn't have customer, can check this." src="../graphics/question.gif" /></div><fmt:message key="inactive" />:</div>
		  	<div class="listp">
		  	<!-- input field -->
	   			<form:checkbox path="salesRep.inactive" />
		    <!-- end input field -->   	
		  	</div>
		  	</div>
			
			<c:if test="${siteConfig['SALESREP_PARENT'].value == 'true'}">
		  	<div class="list${classIndex % 2}"><c:set var="classIndex" value="${classIndex+1}" />
		  	<div class="listfl"><fmt:message key='parent' /> <fmt:message key='salesRep' />:</div>
		  	<div class="listp">
		  	<!-- input field -->
		  	    <form:select path="salesRep.parent">
				  <form:option value=""><fmt:message key="none"/></form:option>
        		  <c:forEach items="${salesReps}" var="salesRep">
        		  <form:option value="${salesRep.id}">${salesRep.name}</form:option>
        		  </c:forEach> 
      			</form:select>
				<form:errors path="salesRep.parent" cssClass="error"/>
			<!-- end input field -->	
	 		</div>
		  	</div>	
			</c:if>
			
			<c:if test="${siteConfig['SALESREP_LOGIN'].value == 'true'}">	
		  	<div class="list${classIndex % 2}"><c:set var="classIndex" value="${classIndex+1}" />
		  	<div class="listfl"><fmt:message key='password' />:</div>
		  	<div class="listp">
		  	<!-- input field -->
				<form:input path="salesRep.password" htmlEscape="true"/>
				<form:errors path="salesRep.password" cssClass="error"/>
				<div class="helpNote"><p>Min 5 Character or BLANK to restrict access</p></div>
			<!-- end input field -->	
	 		</div>
		  	</div>			  
			</c:if>
	   
		  	<div class="list${classIndex % 2}"><c:set var="classIndex" value="${classIndex+1}" />
		  	<div class="listfl"><fmt:message key='accountNumber' />:</div>
		  	<div class="listp">
		  	<!-- input field -->
				<form:input path="salesRep.accountNumber" htmlEscape="true"/>
				<form:errors path="salesRep.accountNumber" cssClass="error"/>
			<!-- end input field -->	
	 		</div>
		  	</div>
		  	
		  	<div class="list${classIndex % 2}"><c:set var="classIndex" value="${classIndex+1}" />
		  	<div class="listfl"><fmt:message key='group' />:</div>
		  	<div class="listp">
		  	<!-- input field -->
				<form:input path="salesRep.salesrepGroup" htmlEscape="true"/>
				<form:errors path="salesRep.salesrepGroup" cssClass="error"/>
			<!-- end input field -->	
	 		</div>
		  	</div>		
		  	
		  	<div class="list${classIndex % 2}"><c:set var="classIndex" value="${classIndex+1}" />
		  	<div class="listfl"><div class="requiredField"><fmt:message key='Name' />:</div></div>
		  	<div class="listp">
		  	<!-- input field -->
				<form:input path="salesRep.name" htmlEscape="true"/>
				<form:errors path="salesRep.name" cssClass="error"/>
				<div class="helpNote">e.g. John Smith</div>	 		
			<!-- end input field -->	
	 		</div>
		  	</div>	
		  	
		  	<div class="list${classIndex % 2}"><c:set var="classIndex" value="${classIndex+1}" />
		  	<div class="listfl"><div class="requiredField"><fmt:message key='email' />:</div></div>
		  	<div class="listp">
		  	<!-- input field -->
				<form:input path="salesRep.email" htmlEscape="true"/>
				<form:errors path="salesRep.email" cssClass="error"/>
				<div class="helpNoteEnd">e.g. johnsmith@webjaguar.com</div>
	 		<!-- end input field -->	
	 		</div>
		  	</div>
		  	
		  	<div class="list${classIndex % 2}"><c:set var="classIndex" value="${classIndex+1}" />
		  	<div class="listfl"><fmt:message key="company" />:</div>
		  	<div class="listp">
		  	<!-- input field -->
         		<form:input path="salesRep.address.company"  htmlEscape="true" />
       			<form:errors path="salesRep.address.company" cssClass="error" />
		    <!-- end input field -->   	
		  	</div>
		  	</div>
		  	
		  	<div class="list${classIndex % 2}"><c:set var="classIndex" value="${classIndex+1}" />
		  	<div class="listfl"><fmt:message key="address" /> 1:</div>
		  	<div class="listp">
		  	<!-- input field -->
         		<form:input path="salesRep.address.addr1" htmlEscape="true" />
       			<form:errors path="salesRep.address.addr1" cssClass="error" />
		    <!-- end input field -->   	
		  	</div>
		  	</div>	
		  	
		  	<div class="list${classIndex % 2}"><c:set var="classIndex" value="${classIndex+1}" />
		  	<div class="listfl"><fmt:message key="address" /> 2:</div>
		  	<div class="listp">
		  	<!-- input field -->
         		<form:input path="salesRep.address.addr2" htmlEscape="true" />
       			<form:errors path="salesRep.address.addr2" cssClass="error" />
		    <!-- end input field -->   	
		  	</div>
		  	</div>	
		  	
		  	<div class="list${classIndex % 2}"><c:set var="classIndex" value="${classIndex+1}" />
		  	<div class="listfl"><fmt:message key='country' />:</div>
		  	<div class="listp">
		  	<!-- input field -->
         		<form:select id="country" path="salesRep.address.country" onchange="toggleStateProvince(this)">
		         <form:option value="" label="Please Select"/>
		         <form:options items="${countries}" itemValue="code" itemLabel="name"/>
		        </form:select>
		        <form:errors path="salesRep.address.country" cssClass="error" />
		    <!-- end input field -->   	
		  	</div>
		  	</div>	
		  	
		  	<div class="list${classIndex % 2}"><c:set var="classIndex" value="${classIndex+1}" />
		  	<div class="listfl"><fmt:message key="city" />:</div>
		  	<div class="listp">
		  	<!-- input field -->
         		<form:input path="salesRep.address.city" htmlEscape="true" />
       			<form:errors path="salesRep.address.city" cssClass="error" />
		    <!-- end input field -->   	
		  	</div>
		  	</div>	
		  	
		  	<div class="list${classIndex % 2}"><c:set var="classIndex" value="${classIndex+1}" />
		  	<div class="listfl"><fmt:message key="stateProvince" />:</div>
		  	<div class="listp">
		  	<!-- input field -->
		       <form:select id="state" path="salesRep.address.stateProvince">
		         <form:option value="" label="Please Select"/>
		         <form:options items="${states}" itemValue="code" itemLabel="name"/>
		       </form:select>
		       <form:select id="ca_province" path="salesRep.address.stateProvince">
		         <form:option value="" label="Please Select"/>
		         <form:options items="${caProvinceList}" itemValue="code" itemLabel="name"/>
		       </form:select>
		       <form:input id="province" path="salesRep.address.stateProvince" htmlEscape="true"/>
		       <form:errors path="salesRep.address.stateProvince" cssClass="error" />
		    <!-- end input field -->   	
		  	</div>
		  	</div>	
		  	
		  	<div class="list${classIndex % 2}"><c:set var="classIndex" value="${classIndex+1}" />
		  	<div class="listfl"><fmt:message key="zipCode" />:</div>
		  	<div class="listp">
		  	<!-- input field -->
         		<form:input path="salesRep.address.zip" htmlEscape="true" />
       			<form:errors path="salesRep.address.zip" cssClass="error" />
		    <!-- end input field -->   	
		  	</div>
		  	</div>	
		  	
		  	<div class="list${classIndex % 2}"><c:set var="classIndex" value="${classIndex+1}" />
		  	<div class="listfl"><fmt:message key='phone' />:</div>
		  	<div class="listp">
		  	<!-- input field -->
				<form:input path="salesRep.phone" htmlEscape="true"/>
				<form:errors path="salesRep.phone" cssClass="error"/>
	 		<!-- end input field -->	
	 		</div>
		  	</div>

		  	<div class="list${classIndex % 2}"><c:set var="classIndex" value="${classIndex+1}" />
		  	<div class="listfl"><fmt:message key="fax" />:</div>
		  	<div class="listp">
		  	<!-- input field -->
         		<form:input path="salesRep.address.fax"  htmlEscape="true" />
       			<form:errors path="salesRep.address.fax" cssClass="error" />
		    <!-- end input field -->   	
		  	</div>
		  	</div>	
		  	
		  	<div class="list${classIndex % 2}"><c:set var="classIndex" value="${classIndex+1}" />
		  	<div class="listfl"><fmt:message key='cellPhone' />:</div>
		  	<div class="listp">
		  	<!-- input field -->
				<form:input path="salesRep.cellPhone" htmlEscape="true"/>
				<form:errors path="salesRep.cellPhone" cssClass="error"/>
	 		<!-- end input field -->	
	 		</div>
		  	</div>	
	<!-- end tab -->		
	</div>
	
	<c:if test="${(salesRepForm.protectedAccess != null and siteConfig['SALESREP_PROTECTED_ACCESS'].value) or gSiteConfig['gPRESENTATION']}">
	<h4 title="Note">Options</h4>
	<div>
	<!-- start tab -->
         <c:set var="classIndex" value="0" />
         
	  	<div class="listdivi ln tabdivi"></div>
	  	<div class="listdivi"></div>
	  	
	  	<c:if test="${salesRepForm.protectedAccess != null and siteConfig['SALESREP_PROTECTED_ACCESS'].value}">
			  	<div class="list${classIndex % 2}"><c:set var="classIndex" value="${classIndex+1}" />
		  		<div class="listfl"><fmt:message key="protectedAccess" />:</div>
				<div class="listp">
				<!-- input field -->
				    <div class="protectedCheckBoxWrapper">
				    <ul>
		       		<c:forEach items="${salesRepForm.protectedAccess}" var="protectedAccess" varStatus="protectedStatus">
		  	  	    <c:set var="key" value="protected${protectedStatus.index+1}"/>
					    <c:choose>
					      <c:when test="${labels[key] != null and labels[key] != ''}">
					        <c:set var="label" value="${labels[key]}"/>
					      </c:when>
					      <c:otherwise><c:set var="label"> ${protectedStatus.index+1}</c:set></c:otherwise>
					    </c:choose>		        
				        <li><input name="__enabled_${protectedStatus.index}" type="checkbox" <c:if test="${protectedAccess}">checked</c:if>><span class="protectedLabel">${label}</span></li>
			        </c:forEach>
			        </ul>
			        </div>
		        <!-- end input field -->   	
				</div>
				</div>
		     </c:if>	
		   
		  	<c:if test="${gSiteConfig['gPRESENTATION']}">
			  	<div class="list${classIndex % 2}"><c:set var="classIndex" value="${classIndex+1}" />
			  	<div class="listfl"><div class="helpImg"><img class="toolTipImg" title="Edit Price On presentation::Unchecking this, will not allow to choose price table on presentation." src="../graphics/question.gif" /></div><fmt:message key="editPriceOnPresentation" />:</div>
			  	<div class="listp">
			  	<!-- input field -->
		   			<form:checkbox path="salesRep.editPrice" />
			    <!-- end input field -->   	
			  	</div>
			  	</div>
	  		</c:if> 
	  	 	  	
	<!-- end tab -->
	</div>	
	</c:if>
	
	<c:if test="${siteConfig['TERRITORY_ZIPCODE'].value}">
	<h4 title="Note">Territory Zipcode</h4>
	<div>
	<!-- start tab -->
         <c:set var="classIndex" value="0" />
         
	  	<div class="listdivi ln tabdivi"></div>
	  	<div class="listdivi"></div>
	  	
	  	 <div class="list${classIndex % 2}"><c:set var="classIndex" value="${classIndex+1}" />
		 <div class="listfl"><div class="helpImg"><img class="toolTipImg" title="Note::Seperate zipcodes with comma before and after. Do not use space." src="../graphics/question.gif" /></div><fmt:message key="territoryZipcode" />:</div>
		 <div class="listp">
		 <!-- input field -->
                <form:textarea rows="8" cols="60" path="salesRep.territoryZip" htmlEscape="true" />
		 <!-- end input field -->   	
		 </div>
		 </div>	  	
	<!-- end tab -->
	</div>	
	</c:if>
	
	
	<h4 title="Note">Note</h4>
	<div>
	<!-- start tab -->
         <c:set var="classIndex" value="0" />
         
	  	<div class="listdivi ln tabdivi"></div>
	  	<div class="listdivi"></div>
	  	
	  	 <div class="list${classIndex % 2}"><c:set var="classIndex" value="${classIndex+1}" />
		 <div class="listfl"><fmt:message key="note" />:</div>
		 <div class="listp">
		 <!-- input field -->
                <form:textarea rows="8" cols="60" path="salesRep.note" htmlEscape="true" />
		 <!-- end input field -->   	
		 </div>
		 </div>	  	
	<!-- end tab -->
	</div>	
	
<!-- end tabs -->			
</div>

<!-- start button -->
<div align="left" class="button">
	<c:choose>
		<c:when test="${salesRepForm.newSalesRep}">
		  <sec:authorize ifAnyGranted="ROLE_AEM,ROLE_ADMIN_${prop_site_id},ROLE_SALES_REP_CREATE">
			<input type="submit" value="<fmt:message key='add' />" /> 
		  </sec:authorize>	
		</c:when>
		<c:otherwise>
		  <sec:authorize ifAnyGranted="ROLE_AEM,ROLE_ADMIN_${prop_site_id},ROLE_SALES_REP_EDIT">
			<input type="submit" value="<fmt:message key='Update' />" />
		  </sec:authorize>	 
		  <sec:authorize ifAnyGranted="ROLE_AEM,ROLE_ADMIN_${prop_site_id},ROLE_SALES_REP_DELETE">	
			<input type="submit" name="delete" value="<fmt:message key='delete' />" onclick="return confirm('Are you sure you wish to delete this <fmt:message key="salesRep" />?')"/>
		  </sec:authorize>	  
		</c:otherwise>
	</c:choose>
	<input type="submit" value="<spring:message code="cancel"/>" name="_cancel">
</div>
<!-- end button -->	

  <!-- end table -->
    </td><td class="boxmidr" ></td></tr>
  <tr><td class="botl"></td><td class="botr"></td></tr>
  </table>
  
<!-- end main box -->  
 </div>
</form:form>

<script language="JavaScript">
<!--
function toggleStateProvince(el) {
  if (el.value == "US") {
      document.getElementById('state').disabled=false;
      document.getElementById('state').style.display="block";
      document.getElementById('ca_province').disabled=true;
      document.getElementById('ca_province').style.display="none";
      document.getElementById('province').disabled=true;
      document.getElementById('province').style.display="none";
  } else if (el.value == "CA") {
      document.getElementById('state').disabled=true;
      document.getElementById('state').style.display="none";
      document.getElementById('ca_province').disabled=false;
      document.getElementById('ca_province').style.display="block";
      document.getElementById('province').disabled=true;
      document.getElementById('province').style.display="none";
  } else {
      document.getElementById('state').disabled=true;
      document.getElementById('state').style.display="none";
      document.getElementById('ca_province').disabled=true;
      document.getElementById('ca_province').style.display="none";
      document.getElementById('province').disabled=false;
      document.getElementById('province').style.display="block";
  }
}
toggleStateProvince(document.getElementById('country'));
//-->
</script>

</c:if>

  </tiles:putAttribute>    
</tiles:insertDefinition>