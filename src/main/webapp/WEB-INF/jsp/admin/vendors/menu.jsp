<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>

  <div id="lbox">
   <table cellpadding="0" cellspacing="0" border="0" class="module" style="width: 100%"><tr><td class="topl">
    </td><td class="topr" ></td></tr><tr><td class="boxmid" >
    
    <c:if test="${siteConfig['TECHDATA'].value != ''}">
    <h2 class="menuleft mfirst">TechData</h2>    
    <div class="menudiv"></div>
    <a href="techdataManuf.jhtm" class="userbutton"><fmt:message key="manufacturers"/></a>
    <a href="techdataCategory.jhtm" class="userbutton"><fmt:message key="categories"/></a>
    <div class="menudiv"></div>
    </c:if>

    <c:if test="${siteConfig['INGRAM'].value != ''}">
    <h2 class="menuleft">IngramMicro</h2>    
    <div class="menudiv"></div>
    <a href="ingrammicroCategory.jhtm" class="userbutton"><fmt:message key="categories"/></a>
    <div class="menudiv"></div>
    </c:if>
    
    <c:if test="${siteConfig['SYNNEX'].value != ''}">
    <h2 class="menuleft">Synnex</h2>    
    <div class="menudiv"></div>
    <a href="synnexCategory.jhtm" class="userbutton"><fmt:message key="categories"/></a>
    <div class="menudiv"></div>
    </c:if>
    
    <c:if test="${siteConfig['DSI'].value != ''}">
    <h2 class="menuleft">DSI</h2>    
    <div class="menudiv"></div>
    <a href="dsiCategory.jhtm" class="userbutton"><fmt:message key="categories"/></a>
    <div class="menudiv"></div>
    </c:if>
    
    <c:if test="${siteConfig['DSDI_FILE_LINK'].value != ''}">
    <h2 class="menuleft">DSDI </h2>    
    <div class="menudiv"></div>
    <a href="dsdiCategory.jhtm" class="userbutton"><fmt:message key="categories"/></a>
    <div class="menudiv"></div>
    </c:if>  
    
    <c:if test="${siteConfig['KOLE_IMPORTS'].value != ''}">
    <h2 class="menuleft">Kole Imports</h2>    
    <div class="menudiv"></div>
    <a href="koleImportsCategory.jhtm" class="userbutton"><fmt:message key="categories"/></a>
    <div class="menudiv"></div>
    </c:if>    
    
    <c:if test="${gSiteConfig['gASI'] != ''}">
    <h2 class="menuleft">ASI</h2>    
    <div class="menudiv"></div>
    <%-- 
    <c:if test="${gSiteConfig['gASI'] == 'API'}">
    <a href="asiCategory.jhtm" class="userbutton">Category Map</a>
    <a href="asiSupplierTree.jhtm" class="userbutton"><fmt:message key="supplierTree"/></a>
    </c:if>
    --%>
    <c:if test="${gSiteConfig['gASI'] == 'File'}">
    <a href="asiConfig.jhtm" class="userbutton">Configuration</a>
    </c:if>
    <div class="menudiv"></div>
    </c:if>    

    <c:if test="${siteConfig['ETILIZE'].value != ''}">
    <h2 class="menuleft">ETILIZE</h2>    
    <div class="menudiv"></div>
    <a href="etilizeManuf.jhtm" class="userbutton"><fmt:message key="manufacturers"/></a>
    <a href="etilizeCategory.jhtm" class="userbutton"><fmt:message key="categories"/></a>
    <div class="menudiv"></div>
    </c:if>
    
    <c:if test="${gSiteConfig['gWEBJAGUAR_DATA_FEED'] > 0 and siteConfig['DATA_FEED_MEMBER_TYPE'].value == 'SUBSCRIBER'}">
    <h2 class="menuleft">Webjaguar</h2>    
    <div class="menudiv"></div>
    <a href="webjaguarCategory.jhtm" class="userbutton"><fmt:message key="category"/></a>
    <div class="menudiv"></div>
    </c:if>
    
    </td><td class="boxmidr" ></td></tr>
    <tr><td class="botl"></td><td class="botr"></td></tr></table>
  </div>