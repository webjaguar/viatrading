<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="/WEB-INF/tlds/spring.tld" prefix="spring" %>

<tiles:insertDefinition name="admin.vendors" flush="true">
  <tiles:putAttribute name="title"  value="Etilize" />
  <tiles:putAttribute name="content" type="string">
<script language="JavaScript" type="text/JavaScript">
<!--
window.addEvent('domready', function(){			
			var tabs1 = new SimpleTabs($('tab-block-1'), { selector: 'h4'});
		});
//-->
</script>

<!-- main box -->
  <div id="mboxfull">
  
  <!-- start table -->
  <table cellpadding="0" cellspacing="0" border="0" class="module" >
  <tr>
    <td class="topl_g">
	  
	  <!-- Error Message -->
	  <c:if test="${!empty message}">
		 <div class="error"><spring:message code="${message}" arguments="${arguments}"/></div>
	  </c:if>
	  
  </td><td class="topr_g" ></td></tr>
  <tr><td class="boxmidlrg" >

 <!-- tabs -->
 <div id="tab-block-1">
	<h4 title="Etilize <fmt:message key="manufacturers" />">Etilize <fmt:message key="manufacturers" /></h4>
	<div>
	<!-- start tab -->

        <c:set var="classIndex" value="0" />
        
	  	<div class="listdivi ln tabdivi"></div>
	  	<div class="listdivi"></div>
	   
		  	<div class="listlight">
		  	<!-- input field -->

			<table border="0" cellpadding="0" cellspacing="0" width="100%" class="listings">
			  <tr class="listingsHdr2">
				<td class="indexCol">&nbsp;</td>
			    <td class="listingsHdr3">Manufacturer Name</td>
				<td class="listingsHdr3">Manufacturer ID</td>
				<td>&nbsp;</td>
			  </tr>
			<c:forEach items="${manufacturers.manufacturer}" var="manufacturer" varStatus="status">
			  <tr class="row${status.index % 2}">
				<td height="20" class="indexCol"><c:out value="${status.count}"/>.</td>
			    <td class="nameCol"><c:out value="${manufacturer.name}"/></td>
			    <td class="nameCol"><c:out value="${manufacturer.id}"/></td>
			    <td>&nbsp;</td>
			  </tr>		  
			</c:forEach>
			</table>

		    <!-- end input field -->   	
		  	</div>
	  	
	<!-- end tab -->        
	</div> 
	
<!-- end tabs -->			
</div>  
  
  <!-- end table -->
    </td><td class="boxmidr" ></td></tr>
  <tr><td class="botl"></td><td class="botr"></td></tr>
  </table>
  
<!-- end main box -->  
 </div>

  </tiles:putAttribute>    
</tiles:insertDefinition>