<%@ page import="spexlive.etilize.com.*,java.util.*,java.io.*"%>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="/WEB-INF/tlds/spring.tld" prefix="spring" %>

<tiles:insertDefinition name="admin.vendors" flush="true">
  <tiles:putAttribute name="title"  value="Etilize" />
  <tiles:putAttribute name="content" type="string">
<script language="JavaScript" type="text/JavaScript">
<!--
window.addEvent('domready', function(){			
	var tabs1 = new SimpleTabs($('tab-block-1'), { selector: 'h4'});
});
//-->
</script>
<!-- main box -->
  <div id="mboxfull">
  
  <!-- start table -->
  <table cellpadding="0" cellspacing="0" border="0" class="module" >
  <tr>
    <td class="topl_g">
  
  </td><td class="topr_g" ></td></tr>
  <tr><td class="boxmidlrg" >

 <!-- tabs -->
 <div id="tab-block-1">
	<h4 title="Etilize <fmt:message key="categories" />">Etilize <fmt:message key="categories" /></h4>
	<div>
	<!-- start tab -->

        <c:set var="classIndex" value="0" />
        
	  	<div class="listdivi ln tabdivi"></div>
	  	<div class="listdivi"></div>
	   
		  	<div class="listlight">
		  	<!-- input field -->
		

				<table border="0" cellpadding="1" cellspacing="0" width="100%" style="padding-left:5px;padding-right:5px;background-color:#EEEEEE">			
					<c:set var="level" value="0" scope="request"/>
		  			<jsp:include page="/WEB-INF/jsp/admin/vendors/etilize/recursion.jsp"/>
				</table>				
		    <!-- end input field -->   	
		  	</div>

	<!-- end tab -->        
	</div> 
	
<!-- end tabs -->			
</div>

<!-- start button -->
<!-- end button -->	     
  
  <!-- end table -->
    </td><td class="boxmidr" ></td></tr>
  <tr><td class="botl"></td><td class="botr"></td></tr>
  </table>
  
<!-- end main box -->  
 </div>

  </tiles:putAttribute>    
</tiles:insertDefinition>

<%-- 
save to excel
PrintWriter pw = new PrintWriter(new FileWriter("/Users/easprec/Desktop/etilize.csv"));

for (Category category: (ArrayList<Category>) request.getAttribute("categories")) {
  pw.write(category.getId() + "," + category.getName());
  pw.write("\n");
  printTree(category, 1, pw);
}
pw.close();
--%> 