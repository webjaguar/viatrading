<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ taglib uri="/WEB-INF/tlds/spring.tld" prefix="spring" %>

<tiles:insertDefinition name="admin.vendors" flush="true">
  <tiles:putAttribute name="title"  value="Vendors - ASI" />
  <tiles:putAttribute name="content" type="string">
<script language="JavaScript" type="text/JavaScript">
<!--
window.addEvent('domready', function(){
	var Tips1 = new Tips($$('.toolTipImg'));
});	
//-->
</script>
<form method="post" action="asiConfig.jhtm" >
<!-- main box -->
  <div id="mboxfull">
  
  <!-- start table -->
  <table cellpadding="0" cellspacing="0" border="0" class="module" >
  <tr>
    <td class="topl_g">
    
	  <!-- breadcrumb -->
	  <p class="breadcrumb">
	    <a href="../vendors/asiConfig.jhtm">ASI</a> &gt;
	    <fmt:message key="configuration" />
	  </p>
	  
	  <!-- Error Message -->
	  <c:if test="${!empty message}">
		 <div class="error"><spring:message code="${message}" arguments="${arguments}"/></div>
	  </c:if>
	  
	  <!-- header image -->
	  <img class="headerImage" src="../graphics/siteInfo.gif" />
	  
  </td><td class="topr_g" ></td></tr>
  <tr><td class="boxmidlrg" >

 <!-- tabs -->
 <div class="tab-wrapper">
	<h4 title=""></h4>
	<div>
	<!-- start tab -->

	  	<div class="listdivi"></div>
	   
		  	<div class="listlight">
		  	<!-- input field -->

			<table border="0" cellpadding="0" cellspacing="0" width="100%" class="listings">
			   <tr class="listingsHdr2">
			     <td class="indexCol">&nbsp;</td>
			     <td class="listingsHdr3"><fmt:message key="title" /></td>
			     <td class="listingsHdr3"><fmt:message key="value" /></td>
			  </tr>
			  <%--
			  <tr class="row1" onmouseover="changeStyleClass(this,'rowOver')" onmouseout="changeStyleClass(this,'row1')">
			    <td class="indexCol">&nbsp;</td>
			    <td class="nameCol"><input type="hidden" name="__key" value="ASI_FREEZE_DEFAULT"><fmt:message key="defaultFreezeASIFromFeed" /><img class="toolTipImg" title="Note::If checked, mark up will apply on End Qt Price." src="../graphics/question.gif" /></td>
			    <td class="nameCol">
			    <select name="ASI_FREEZE_DEFAULT">
		        	<option value="0" <c:if test="${'0' == siteConfig['ASI_FREEZE_DEFAULT'].value}">selected</c:if>></option>
		        	<option value="1" <c:if test="${'1' == siteConfig['ASI_FREEZE_DEFAULT'].value}">selected</c:if>>Freeze</option>
		 	  	</select>
		 	  	</td>
		  	  </tr>
		  	   --%>
		  	  <c:if test="${gSiteConfig['gASI'] != ''}">
			  <tr class="row1" onmouseover="changeStyleClass(this,'rowOver')" onmouseout="changeStyleClass(this,'row1')">
			    <td class="indexCol">&nbsp;</td>
			    <td class="nameCol"><input type="hidden" name="__key" value="ASI_PRICE_MARK_UP_CHANGE_BASE_PRICE"><fmt:message key="changeBasePrice" /><img class="toolTipImg" title="Note::If checked, mark up will change the base price. If it is not checked, markup will be used to replace sales tag." src="../graphics/question.gif" /></td>
			    <td class="nameCol">
			      <input type="checkbox" name="ASI_PRICE_MARK_UP_CHANGE_BASE_PRICE" value ="true" <c:if test="${siteConfig['ASI_PRICE_MARK_UP_CHANGE_BASE_PRICE'].value == 'true'}">checked</c:if>>
		     	</td>
			  </tr>
		  	  <tr class="row0" onmouseover="changeStyleClass(this,'rowOver')" onmouseout="changeStyleClass(this,'row0')">
			    <td class="indexCol">&nbsp;</td>
			    <td class="nameCol"><input type="hidden" name="__key" value="ASI_PRICE_MARK_UP_TYPE"><fmt:message key="markUpBasedOn" /><img class="toolTipImg" title="Note::If checked, mark up will apply on End Qt Price." src="../graphics/question.gif" /></td>
			    <td class="nameCol">
			    	<select name="ASI_PRICE_MARK_UP_TYPE">
				      <option value="0" <c:if test="${siteConfig['ASI_PRICE_MARK_UP_TYPE'].value == '0'}">selected="selected"</c:if>>Regular Prices</option>
					  <option value="1" <c:if test="${siteConfig['ASI_PRICE_MARK_UP_TYPE'].value == '1'}">selected="selected"</c:if>>End Qty Price</option>
					  <option value="2" <c:if test="${siteConfig['ASI_PRICE_MARK_UP_TYPE'].value == '2'}">selected="selected"</c:if>>Regualr Cost</option>
					  <option value="3" <c:if test="${siteConfig['ASI_PRICE_MARK_UP_TYPE'].value == '3'}">selected="selected"</c:if>>End Qty Cost</option>
					</select>
				</td>
			  </tr>
		  	  <tr class="row1" onmouseover="changeStyleClass(this,'rowOver')" onmouseout="changeStyleClass(this,'row1')">
			    <td class="indexCol">&nbsp;</td>
			    <td class="nameCol"><input type="hidden" name="__key" value="ASI_PRICE_MARK_UP_FORMULA"><fmt:message key="markUp" />/<fmt:message key="margin" /></td>
			    <td class="nameCol">
			    	<select name="ASI_PRICE_MARK_UP_FORMULA">
				      <option value="0" <c:if test="${siteConfig['ASI_PRICE_MARK_UP_FORMULA'].value == '0'}">selected="selected"</c:if>>Markup</option>
					  <option value="1" <c:if test="${siteConfig['ASI_PRICE_MARK_UP_FORMULA'].value == '1'}">selected="selected"</c:if>>Margin</option>
					</select>
				</td>
			  </tr>
		  	  </c:if>
			  <c:set var="counter" value="0"/>
			  <c:forEach items="${fn:split(siteConfig['ASI_PRICE_MARK_UP_FOR_ALL_PRODUCTS'].value, ',')}" var="discount" varStatus="status">
		  	  <c:set var="counter" value="${status.index+1}"/>
			  <tr class="row${status.index % 2}" onmouseover="changeStyleClass(this,'rowOver')" onmouseout="changeStyleClass(this,'row${status.index % 2}')">
			    <td class="indexCol">&nbsp;</td>
			    <td class="nameCol"><input type="hidden" name="__mkey" value="ASI_PRICE_MARK_UP_FOR_ALL_PRODUCTS"><fmt:message key="markUp"/> ${status.index+1}<img class="toolTipImg" title="Note::Add/Deduct mark up percentage to/from the price. Add negative value to deduct markup value from price." src="../graphics/question.gif" /></td>
			    <td class="nameCol"><input type="text" name="ASI_PRICE_MARK_UP_FOR_ALL_PRODUCTS" value ="${discount}"></input>
		  	  </td>
		  	  
			  </tr>
			  </c:forEach>
			  <c:forTokens items="0,1,2,3,4,5,6,7,8,9" delims="," begin="${counter}" varStatus="status1">
		  	  <tr class="row${status1.index % 2}" onmouseover="changeStyleClass(this,'rowOver')" onmouseout="changeStyleClass(this,'row${status1.index % 2}')">
			    <td class="indexCol">&nbsp;</td>
			    <td class="nameCol"><input type="hidden" name="__mkey" value="ASI_PRICE_MARK_UP_FOR_ALL_PRODUCTS"><fmt:message key="markUp"/> ${status1.index+1}<img class="toolTipImg" title="Note::Add/Deduct mark up percentage to/from the price. Add negative value to deduct markup value from price." src="../graphics/question.gif" /></td>
			    <td class="nameCol"><input type="text" name="ASI_PRICE_MARK_UP_FOR_ALL_PRODUCTS"></input>
		  	  </td>		    
			  </tr>
			  </c:forTokens>
			</table>

		    <!-- end input field -->   	
		  	</div>
	  	
	<!-- end tab -->        
	</div> 
	
<!-- end tabs -->			
</div>

<!-- start button -->
<div align="left" class="button"> 
    <input type="submit"  name="__update" value="<fmt:message key="update" />">
</div>
<!-- end button -->	     
  
  <!-- end table -->
    </td><td class="boxmidr" ></td></tr>
  <tr><td class="botl"></td><td class="botr"></td></tr>
  </table>
  
<!-- end main box -->  
 </div>
</form>

  </tiles:putAttribute>    
</tiles:insertDefinition>