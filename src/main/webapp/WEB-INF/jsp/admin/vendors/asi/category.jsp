<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="/WEB-INF/tlds/spring.tld" prefix="spring" %>

<tiles:insertDefinition name="admin.vendors" flush="true">
  <tiles:putAttribute name="title"  value="Vendors - ASI" />
  <tiles:putAttribute name="content" type="string">
<script language="JavaScript" type="text/JavaScript">
<!--

//-->
</script>
<form method="post">
<!-- main box -->
  <div id="mboxfull">
  
  <!-- start table -->
  <table cellpadding="0" cellspacing="0" border="0" class="module" >
  <tr>
    <td class="topl_g">
    
	  <!-- breadcrumb -->
	  <p class="breadcrumb">
	    <a href="../vendors/asiCategory.jhtm">ASI</a> &gt;
	    <fmt:message key="list" />
	  </p>
	  
	  <!-- Error Message -->
	  <c:if test="${!empty message}">
		 <div class="error"><spring:message code="${message}" arguments="${arguments}"/></div>
	  </c:if>
	  
	  <!-- header image -->
	  <img class="headerImage" src="../graphics/supplier.gif" />
	  
  </td><td class="topr_g" ></td></tr>
  <tr><td class="boxmidlrg" >

 <!-- tabs -->
 <div class="tab-wrapper">
	<h4 title=""></h4>
	<div>
	<!-- start tab -->
	  	<div class="listdivi ln tabdivi"></div>
	  	<div class="listdivi"></div>
	   
		  	<div class="listlight">
		  	<!-- input field -->
		  	<table border="0" cellpadding="0" cellspacing="1" width="100%">
			  <tr>	   
			  <td>
				<select name="_supplier" onChange="submit();" style="width:200px">	
				  <option value="" >Select Supplier</option>
				  <c:forEach items="${supplierList}" var="supplier">
					<option value="${supplier}" <c:if test="${supplier == selectedSupplier}">selected</c:if>><c:out value="${supplier}" /></option>
				  </c:forEach>
				</select>
			  </td>
			  </tr>
			</table>

			<table border="0" cellpadding="0" cellspacing="0" width="100%" class="listings" style="border:0px">
			  <tr class="listingsHdr2">
			    <td width="5%" align="center" class="listingsHdr3">Level 1</td>
				<td width="5%" align="center" class="listingsHdr3">Level 2</td>
				<td width="5%" align="center" class="listingsHdr3">Level 3</td>
				<td width="10%" align="center" class="listingsHdr3">Category ID</td>
				<td width="14%">&nbsp;</td>		    
			  </tr>
			<c:set var="status" value="1"/>
			<c:forEach items="${categories}" var="level1" varStatus="status">
			  <input type="hidden" name="code" value="<c:out value="${level1.code}"/>">
			  <tr class="row${status.index % 2}" onmouseover="changeStyleClass(this,'rowOver')" onmouseout="changeStyleClass(this,'row${status.index % 2}')">
			    <td width="5%" align="center"><c:out value="${level1.supplier}"/></td>
			    <td width="5%" >&nbsp;</td>		    
			    <td width="5%" >&nbsp;</td>
			    <td width="10%" align="center"></td>
			    <td width="14%">&nbsp;</td>	
			  </tr>
			  <tr>
			  <td colspan="10">
			  <div id="info${level1.supplier}" class="row${status.index % 2}">
			  <table border="0" cellpadding="0" cellspacing="0" width="100%" class="listings" style="border:0px">
			  <c:forEach items="${level1.subs}" var="level2" varStatus="level2Status">
			  <input type="hidden" name="code" value="<c:out value="${level2.code}"/>">
			  <tr class="row${level2Status.index % 2}" onmouseover="changeStyleClass(this,'rowOver')" onmouseout="changeStyleClass(this,'row${level2Status.index % 2}')">
			    <td <c:if test="${level2Status.first}">width="1%"</c:if> height="25">&nbsp;</td>
			    <td <c:if test="${level2Status.first}">width="5%"</c:if>>&nbsp;</td>
			    <td <c:if test="${level2Status.first}">width="5%"</c:if> align="center"><c:out value="${level2.cat}"/></td>		    
			    <td <c:if test="${level2Status.first}">width="5%"</c:if>>&nbsp;</td>
			    <td <c:if test="${level2Status.first}">width="10%"</c:if> align="center"><input type="text" name="<c:out value="${level2.code}"/>_catId" value="${level2.catId}" class="textfield50" disabled="disabled"></td>
			    <td <c:if test="${level2Status.first}">width="14%"</c:if>>&nbsp;</td>
			  </tr>
			  <c:forEach items="${level2.subs}" var="level3" varStatus="level3Status">
			  <input type="hidden" name="code" value="<c:out value="${level3.code}"/>">
			  <tr class="row${level2Status.index % 2}" onmouseover="changeStyleClass(this,'rowOver')" onmouseout="changeStyleClass(this,'row${level2Status.index % 2}')">
			    <td <c:if test="${level3Status.first}">width="1%"</c:if> height="25">&nbsp;</td>
			    <td <c:if test="${level3Status.first}">width="5%"</c:if>height="25">&nbsp;</td>
			    <td <c:if test="${level3Status.first}">width="5%"</c:if>>&nbsp;</td>
			    <td <c:if test="${level3Status.first}">width="5%"</c:if> align="center"><c:out value="${level3.sub}"/></td>
			    <td <c:if test="${level3Status.first}">width="10%"</c:if> align="center"><input type="text" name="<c:out value="${level3.code}"/>_catId" value="${level3.catId}" class="textfield50" disabled="disabled"></td>
			    <td <c:if test="${level3Status.first}">width="14%"</c:if>>&nbsp;</td>
			  </tr>
			  </c:forEach>	
			  </c:forEach>
			  </table>
			  </div>
			  </td>
			  </tr>
			</c:forEach>
			</table>

		    <!-- end input field -->   	
		  	</div>
	  	
	<!-- end tab -->        
	</div> 
	
<!-- end tabs -->			
</div>

<!-- start button -->
<div align="left" class="button"> 
    <input type="submit" value="<fmt:message key="update" />">
</div>
<!-- end button -->	     
  
  <!-- end table -->
    </td><td class="boxmidr" ></td></tr>
  <tr><td class="botl"></td><td class="botr"></td></tr>
  </table>
  
<!-- end main box -->  
 </div>
</form>

  </tiles:putAttribute>    
</tiles:insertDefinition>