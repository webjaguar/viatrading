<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="/WEB-INF/tlds/spring.tld" prefix="spring" %>

<tiles:insertDefinition name="admin.vendors" flush="true">
  <tiles:putAttribute name="content" type="string">
<script language="JavaScript" type="text/JavaScript">
<!--
window.addEvent('domready', function(){			
			var tabs1 = new SimpleTabs($('tab-block-1'), { selector: 'h4'});
		});
//-->
</script>
<form method="post">
<!-- main box -->
  <div id="mboxfull">
  
  <!-- start table -->
  <table cellpadding="0" cellspacing="0" border="0" class="module" >
  <tr>
    <td class="topl_g">
	  
	  <!-- Error Message -->
	  <c:if test="${!empty message}">
		 <div class="error"><spring:message code="${message}" arguments="${arguments}"/></div>
	  </c:if>
	  
  </td><td class="topr_g" ></td></tr>
  <tr><td class="boxmidlrg" >

 <!-- tabs -->
 <div id="tab-block-1">
	<h4 title="Kole Imports <fmt:message key="categories" />">Webjaguar <fmt:message key="categories" /></h4>
	<div>
	<!-- start tab -->

        <c:set var="classIndex" value="0" />
        
	  	<div class="listdivi ln tabdivi"></div>
	  	<div class="listdivi"></div>
		  	<div class="listlight">
		  	<!-- input field -->

			<table border="0" cellpadding="0" cellspacing="0" width="100%" class="listings" style="border:0px">
			  <tr class="listingsHdr2">
			    <td width="1%" onclick="toggleAll()"><img id="allImage" src="../graphics/expand.gif" border="0" /></td>
				<c:forEach begin="1" end="${level}" var="index" varStatus="status">
				  <td width="5%" align="center" class="listingsHdr3">Level ${index}</td>
				</c:forEach>
				<td width="10%" align="center" class="listingsHdr3">Category ID</td>	
				<td width="9%">&nbsp;</td>		    
			  </tr>
			<c:set var="status" value="1"/>
		<%--	<c:forEach items="${categories}" var="level1">
			  <c:set var="status" value="${status + 1}"/>
			  <input type="hidden" name="breadCrumb" value="<c:out value="${level1.breadCrumb}"/>">
			  <tr class="row${status % 2}" onmouseover="changeStyleClass(this,'rowOver')" onmouseout="changeStyleClass(this,'row${status % 2}')">
			    <td align="center" height="25" onclick="toggleInfo('${level1.breadCrumb}')"><img id="info${level1.breadCrumb}image" src="../graphics/expand.gif" border="0" /></td>
			    <td align="center">.</td>		    
			    <td>&nbsp;</td>
			    <td><c:out value="${level1.cat}"/></td>
			    <td align="center" ><input type="text" name="<c:out value="${level1.breadCrumb}"/>_catId" value="${level1.breadCrumb}" class="textfield50"></td>
			    <td>&nbsp;</td>
			  </tr>
			  <tr>
			  
			  </tr>
			</c:forEach>
			 --%>
			
			<c:forEach items="${categories}" var="category">
			  <input type="hidden" name="breadCrumb" value="<c:out value="${category.breadCrumb}"/>">
			  <c:set var="maxLevel" value="${fn:length(fn:split(category.breadCrumb, '-->'))}"></c:set>
			  <tr class="row${status % 2}" onmouseover="changeStyleClass(this,'rowOver')" onmouseout="changeStyleClass(this,'row${status % 2}')">
			  	 <td align="center" height="25" onclick="toggleInfo('${category.breadCrumb}')"><img id="info${category.breadCrumb}image" src="../graphics/expand.gif" border="0" /></td>
			     <c:forEach items="${fn:split(category.breadCrumb, '-->')}" var="cat"  varStatus="status"> 
			       <td width="5%" align="center"><c:if test="${maxLevel == status.index+1}"> <c:out value="${cat}"></c:out></c:if></td>
				 </c:forEach>
				 <c:forEach begin="${maxLevel}" end="${level-1}">
				   <td width="5%" align="center"></td>
				 </c:forEach> 
				 <td align="center" ><input type="text" name="<c:out value="${category.breadCrumb}"/>_catId" value="${category.catId}" class="textfield50"></td>
			     <td>&nbsp;</td>
			  </tr>
			  <tr>
			  
			  </tr>
			</c:forEach>
			
			
			</table>

		    <!-- end input field -->   	
		  	</div>
	  	
	<!-- end tab -->        
	</div> 
	
<!-- end tabs -->			
</div>

<!-- start button -->
<div align="right" class="button"> 
    <input type="submit" value="<fmt:message key="update" />">
</div>
<!-- end button -->	     
  
  <!-- end table -->
    </td><td class="boxmidr" ></td></tr>
  <tr><td class="botl"></td><td class="botr"></td></tr>
  </table>
  
<!-- end main box -->  
 </div>
</form>

  </tiles:putAttribute>    
</tiles:insertDefinition>