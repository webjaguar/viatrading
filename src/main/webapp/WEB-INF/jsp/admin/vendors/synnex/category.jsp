<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="/WEB-INF/tlds/spring.tld" prefix="spring" %>

<tiles:insertDefinition name="admin.vendors" flush="true">
  <tiles:putAttribute name="title"  value="Vendors - Synnex" />
  <tiles:putAttribute name="content" type="string">
<script language="JavaScript" type="text/JavaScript">
<!--
window.addEvent('domready', function(){			
			var tabs1 = new SimpleTabs($('tab-block-1'), { selector: 'h4'});
		});
var cat_array = new Array(<c:forEach items="${categories}" var="category" varStatus="status">'${category.cat}'<c:if test="${not status.last}">, </c:if></c:forEach>);
function toggleAll() {
  if (document.getElementById('allImage').src.indexOf("expand.gif")>0) {
    document.getElementById('allImage').src="../graphics/collapse.gif";
    for (x=0; x<cat_array.length; x++) {
      document.getElementById('info'+cat_array[x]).style.display="block";
      document.getElementById('info'+cat_array[x]+'image').src="../graphics/collapse.gif";
    }
  } else {
    document.getElementById('allImage').src="../graphics/expand.gif";
    for (x=0; x<cat_array.length; x++) {
      document.getElementById('info'+cat_array[x]).style.display="none";
      document.getElementById('info'+cat_array[x]+'image').src="../graphics/expand.gif";    
    }
  }
}
//-->
</script>
<form method="post">
<!-- main box -->
  <div id="mboxfull">
  
  <!-- start table -->
  <table cellpadding="0" cellspacing="0" border="0" class="module" >
  <tr>
    <td class="topl_g">
	  
	  <!-- Error Message -->
	  <c:if test="${!empty message}">
		 <div class="error"><spring:message code="${message}" arguments="${arguments}"/></div>
	  </c:if>
	  
  </td><td class="topr_g" ></td></tr>
  <tr><td class="boxmidlrg" >

 <!-- tabs -->
 <div id="tab-block-1">
	<h4 title="Synnex <fmt:message key="categories" />">Synnex <fmt:message key="categories" /></h4>
	<div>
	<!-- start tab -->

        <c:set var="classIndex" value="0" />
        
	  	<div class="listdivi ln tabdivi"></div>
	  	<div class="listdivi"></div>
	   
		  	<div class="listlight">
		  	<!-- input field -->

			<table border="0" cellpadding="0" cellspacing="0" width="100%" class="listings" style="border:0px">
			  <tr class="listingsHdr2">
			    <td width="1%" onclick="toggleAll()"><img id="allImage" src="../graphics/expand.gif" border="0" /></td>
				<td width="5%" align="center" class="listingsHdr3">Level 1</td>
				<td width="5%" align="center" class="listingsHdr3">Level 2</td>
				<td width="5%" align="center" class="listingsHdr3">Level 3</td>
				<td width="50%" align="center" colspan="3" class="listingsHdr3">Descr</td>
				<td width="10%" align="center" class="listingsHdr3">Category ID</td>	
				<td width="10%" align="center" class="listingsHdr3">Price Change</td>	
				<td width="14%">&nbsp;</td>		    
			  </tr>
			<c:set var="status" value="1"/>
			<c:forEach items="${categories}" var="level1">
			  <c:set var="status" value="${status + 1}"/>
			  <input type="hidden" name="code" value="<c:out value="${level1.code}"/>">
			  <tr class="row${status % 2}" onmouseover="changeStyleClass(this,'rowOver')" onmouseout="changeStyleClass(this,'row${status % 2}')">
			    <td align="center" height="25" onclick="toggleInfo('${level1.cat}')"><img id="info${level1.cat}image" src="../graphics/expand.gif" border="0" /></td>
			    <td align="center"><c:out value="${level1.cat}"/></td>		    
			    <td>&nbsp;</td>
			    <td>&nbsp;</td>
			    <td colspan="3"><c:out value="${level1.descr}"/></td>
			    <td width="10%" align="center"><input type="text" name="<c:out value="${level1.code}"/>_catId" value="${level1.catId}" class="textfield50"></td>
			    <td width="10%" align="center"><input type="text" name="<c:out value="${level1.code}"/>_priceChange" value="${level1.priceChange}" class="textfield50">%</td>
			    <td>&nbsp;</td>
			  </tr>
			  <tr>
			  <td colspan="10">
			  <div id="info${level1.cat}" class="row${status % 2}" style="display:none">
			  <table border="0" cellpadding="0" cellspacing="0" width="100%" class="listings" style="border:0px">
			  <c:forEach items="${level1.subs}" var="level2">
			  <input type="hidden" name="code" value="<c:out value="${level2.code}"/>">
			  <tr class="row${status % 2}" onmouseover="changeStyleClass(this,'rowOver')" onmouseout="changeStyleClass(this,'row${status % 2}')">
			    <td width="1%" height="25">&nbsp;</td>
			    <td width="5%">&nbsp;</td>		    
			    <td width="5%" align="center"><c:out value="${level2.sub}"/></td>
			    <td width="5%">&nbsp;</td>
			    <td width="10%">&nbsp;</td>
			    <td width="40%" colspan="2"><c:out value="${level2.descr}"/></td>
			    <td width="10%" align="center"><input type="text" name="<c:out value="${level2.code}"/>_catId" value="${level2.catId}" class="textfield50"></td>
			    <td width="10%" align="center"><input type="text" name="<c:out value="${level2.code}"/>_priceChange" value="${level2.priceChange}" class="textfield50">%</td>
			    <td width="14%">&nbsp;</td>
			  </tr>
			  <c:forEach items="${level2.subs}" var="level3">
			  <input type="hidden" name="code" value="<c:out value="${level3.code}"/>">
			  <tr class="row${status % 2}" onmouseover="changeStyleClass(this,'rowOver')" onmouseout="changeStyleClass(this,'row${status % 2}')">
			    <td width="1%" height="25">&nbsp;</td>
			    <td width="5%">&nbsp;</td>		    
			    <td width="5%">&nbsp;</td>
			    <td width="5%" align="center"><c:out value="${level3.subSub}"/></td>
			    <td width="20%" colspan="2">&nbsp;</td>
			    <td width="30%"><c:out value="${level3.descr}"/></td>
			    <td width="10%" align="center"><input type="text" name="<c:out value="${level3.code}"/>_catId" value="${level3.catId}" class="textfield50"></td>
			    <td width="10%" align="center"><input type="text" name="<c:out value="${level3.code}"/>_priceChange" value="${level3.priceChange}" class="textfield50">%</td>
			    <td width="14%">&nbsp;</td>
			  </tr>
			  </c:forEach>	
			  </c:forEach>	
			  </table>
			  </div>
			  </td>
			  </tr>
			</c:forEach>
			</table>

		    <!-- end input field -->   	
		  	</div>
	  	
	<!-- end tab -->        
	</div> 
	
<!-- end tabs -->			
</div>

<!-- start button -->
<div align="right" class="button"> 
    <input type="submit" value="<fmt:message key="update" />">
</div>
<!-- end button -->	     
  
  <!-- end table -->
    </td><td class="boxmidr" ></td></tr>
  <tr><td class="botl"></td><td class="botr"></td></tr>
  </table>
  
<!-- end main box -->  
 </div>
</form>

  </tiles:putAttribute>    
</tiles:insertDefinition>