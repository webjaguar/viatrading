<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ taglib uri="/WEB-INF/tlds/spring.tld" prefix="spring" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ taglib uri="http://www.springframework.org/security/tags" prefix="sec" %>

<tiles:insertDefinition name="admin.event" flush="true">
  <tiles:putAttribute name="content" type="string">
<sec:authorize ifAnyGranted="ROLE_AEM,ROLE_ADMIN_${prop_site_id},ROLE_EVENT_CREATE,ROLE_EVENT_UPDATE">

<script language="JavaScript"> 
<!--
window.addEvent('domready', function(){			
	var tabs1 = new SimpleTabs($('tab-block-1'), { selector: 'h4'});
	var Tips1 = new Tips($$('.toolTipImg'));
});
//--> 
</script> 
<form:form commandName="eventMemberForm" action="eventMemberForm.jhtm" method="post">
<form:hidden path="eventMember.eventId" />

<!-- main box -->
  <div id="mboxfull">
  
  <!-- start table -->
  <table cellpadding="0" cellspacing="0" border="0" class="module" >
  <tr>
    <td class="topl_g">
	  
	  <!-- breadcrumb -->
	  <p class="breadcrumb">
	    <a href="../event">EventMember</a> &gt;
	    form 
	  </p>
	  
	  <!-- Error Message -->
	  <%--<c:if test="${message != null}">
  		<div class="message"><fmt:message key="${message}"/></div>
  	  </c:if> --%>
  	  <spring:hasBindErrors name="eventForm">
		<div class="message">Please fix all errors!</div>
	  </spring:hasBindErrors>

  </td><td class="topr_g" ></td></tr>
  <tr><td class="boxmidlrg" >

 <!-- tabs -->
 <div id="tab-block-1">
	<h4 title="Event Member Form">EventMember</h4>
	<div>
	<!-- start tab -->

         <c:set var="classIndex" value="0" />
         
	  	 <div class="listdivi ln tabdivi"></div>
	  	 <div class="list${classIndex % 2}"><c:set var="classIndex" value="${classIndex+1}" />
		 <div class="listfl"><fmt:message key="eventField1" />:</div>
		 <div class="listp">
		 <!-- input field -->
			<form:input path="eventMember.field1" maxlength="255" size="60" htmlEscape="true"/>
			<form:errors path="eventMember.field1" cssClass="error"/>
	 	 <!-- end input field -->	
	 	 </div>
		 </div>		

		 <div class="list${classIndex % 2}"><c:set var="classIndex" value="${classIndex+1}" />
		 <div class="listfl"><fmt:message key="comment" />:</div>
		 <div class="listp">
		 <!-- input field -->
      	  		<form:textarea rows="15" cols="60" path="eventMember.comment" cssClass="textfield" htmlEscape="true" />
      	  		<form:errors path="eventMember.comment" cssClass="error" /> 
		 <!-- end input field -->
		 </div>
		 </div>	
		
	<!-- end tab -->        
	</div>         

<!-- end tabs -->			
</div>

<!-- start button -->
	<div align="left" class="button">
	  <c:if test="${eventMemberForm.newEventMember}">
	    <sec:authorize ifAnyGranted="ROLE_AEM,ROLE_ADMIN_${prop_site_id},ROLE_EVENT_CREATE">
          <input type="submit" value="<spring:message code="eventAdd"/>" />
        </sec:authorize>  
      </c:if>
      <c:if test="${!eventMemberForm.newEventMember}">
        <sec:authorize ifAnyGranted="ROLE_AEM,ROLE_ADMIN_${prop_site_id},ROLE_EVENT_UPDATE">
          <input type="submit" value="<spring:message code="eventUpdate"/>" />
        </sec:authorize>  
      </c:if>
	</div>
<!-- end button -->
  
  <!-- end table -->
    </td><td class="boxmidr" ></td></tr>
  <tr><td class="botl"></td><td class="botr"></td></tr>
  </table>
  
<!-- end main box -->  
</div>
</form:form>
</sec:authorize> 

  </tiles:putAttribute>    
</tiles:insertDefinition>