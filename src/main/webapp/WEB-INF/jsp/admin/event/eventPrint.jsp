<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ taglib uri="/WEB-INF/tlds/custom.tld" prefix="wj" %>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>

  <head>
    <title>Event</title>
  </head>
<body class="invoice">

<c:if test="${model.eventMember != null}">
<div align="right"><a href="javascript:window.print()"><img border="0" src="../graphics/printer.png" /></a></div>
<c:out value="${model.eventMember.htmlCode}" escapeXml="false"/>
</c:if>

<c:if test="${model.eventMemberList != null}">
        <div align="right"><a href="javascript:window.print()"><img border="0" src="../graphics/printer.png" /></a></div>
        <c:forEach items="${model.eventMemberList}" var="eventMember" varStatus="status">
        
            <c:set var = "string1" value = "${model.event.htmlCode}"/>
            <c:set var = "userIndex" value = "${eventMember.userIndex}"/>
            <c:set var = "result" value='${fn:replace(string1,"#queue#",userIndex)}' />
            <c:set var = "eventName" value = "${model.event.eventName}"/>
            <c:set var = "result1" value='${fn:replace(result,"#eventname#",eventName)}' />
            <c:set var = "startDate" value = "${model.event.startDate}"/>
            <c:set var = "result2" value='${fn:replace(result1,"#eventdate#",startDate)}' />
            <c:set var = "firstName" value = "${eventMember.firstName}"/>
            <c:set var = "result3" value='${fn:replace(result2,"#firstname#",firstName)}' />
            <c:set var = "lastName" value = "${eventMember.lastName}"/>
            <c:set var = "result4" value='${fn:replace(result3,"#lastname#",lastName)}' />
            <c:set var = "cardId" value = "${eventMember.cardId}"/>
            <c:set var = "result5" value='${fn:replace(result4,"#cardid#",cardId)}' />
            <c:set var = "userId" value = "${model.userMap[eventMember.userId]}"/>
            <c:set var = "result6" value='${fn:replace(result5,"#customerfield20#",userId)}' />

           ${result6} 

</c:forEach>
</c:if>

</div>
</body>
</html>