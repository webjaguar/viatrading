<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/security/tags" prefix="sec" %>


<tiles:insertDefinition name="admin.event.eventMember" flush="true">
  <tiles:putAttribute name="content" type="string">
  
<sec:authorize ifAnyGranted="ROLE_AEM,ROLE_ADMIN_${prop_site_id},ROLE_EVENT_MEMBER_VIEW_LIST"> 

<script type="text/javascript"><!--
window.addEvent('domready', function(){
	var box2 = new multiBox('mbCustomer', {descClassName: 'multiBoxDesc',waitDuration: 5,showNumbers: true,overlay: new overlay()});
	var myAccordion = new Fx.Accordion($$('div.arrow_drop'), $$('div.information'), {
		display: 1, alwaysHide: true,
    	onActive: function() {$('information').removeClass('displayNone');}
	});
	<sec:authorize ifAnyGranted="ROLE_AEM,ROLE_ADMIN_${prop_site_id},ROLE_QUICK_EXPORT">  
	$('quickModeTrigger').addEvent('click',function() {
	    $('mboxfull').setStyle('margin', '0');
	    $('OverlayContainer').hide();
	    $$('.quickMode').each(function(el){
	    	el.hide();
	    });
	    $$('.quickModeRemove').each(function(el){
	    	el.destroy();
	    });
	    $$('.listingsHdr3').each(function(el){
	    	el.removeProperties('colspan', 'class');
	    });
	    alert('Select the whole page, copy and paste to your excel file.');
	  });
	  </sec:authorize>
}); 
function toggleAll(el) {
	var ids = document.getElementsByName("__selected_id");	
  	if (ids.length == 1)
      document.list_form.__selected_id.checked = el.checked;
    else
      for (i = 0; i < ids.length; i++)
        document.list_form.__selected_id[i].checked = el.checked;	
} 
function deleteSelected() {
	var ids = document.getElementsByName("__selected_id");	
  	if (ids.length == 1) {
      if (document.list_form.__selected_id.checked) {
    	return confirm('Delete permanently?');
      }
    } else {
      for (i = 0; i < ids.length; i++) {
        if (document.list_form.__selected_id[i].checked) {
    	  return confirm('Delete permanently?');
        }
      }
    }

    alert("Please select member(s) to delete.");       
    return false;
}
function optionsSelected() {
	var ids = document.getElementsByName("__selected_id");
  	if (ids.length == 1) {
      if (document.list_form.__selected_id.checked) {
    	return true;
      }
    } else {
      for (i = 0; i < ids.length; i++) {
        if (document.list_form.__selected_id[i].checked) {
    	  return true;
        }
      }
    }
  	if (document.list_form.__groupAssignAll.checked) {
  	   return true;
    }
    alert("Please select customer(s).");       
    return false;
} 
--></script>
<form action="eventMemberList.jhtm" method="post" id="list" name="list_form">
<input type="hidden" id="id" name="id" value="${eventMemberSearch.eventId}" />
<input type="hidden" id="sort" name="sort" value="${eventMemberSearch.sort}" />
<!-- main box -->
  <div id="mboxfull">
  
  <!-- start table -->
  <table cellpadding="0" cellspacing="0" border="0" class="module" >
  <tr class="quickMode">
    <td class="topl_g">
	  
	  <!-- breadcrumb -->
	  <p class="breadcrumb">
	    <a href="../event">Event</a> &gt;
	    <a href="../event">Event Member</a> &gt;
	    <c:out value="${model.event.eventName}" />
	  </p>
	  
	  <!-- Error Message -->
	  
	  <!-- header image -->
	  <img id="quickModeTrigger" class="headerImage" src="../graphics/event.gif" />
	   <!-- Option -->
      <c:if test="${model.count > 0}">
      	<div class="optionBox"><div class="titlePrivilege"><fmt:message key="options" /></div></div>
		<div class="arrow_drop arrowImageRight"></div>
		<div style="clear: both;"></div>
		<div class="information displayNone" id="information" style="float:left;">
			<div style="float:left;padding: 5px">
	           <table class="productBatchTool" cellpadding="5" cellspacing="5">
	           <tr>
	            <td valign="top">
	             <table cellpadding="1" cellspacing="1">
	              <tr style="height:30px;white-space: nowrap">
	               <td valign="top" width="20px"><img class="toolTipImg" title="Print :: Print multiple event member" src="../graphics/question.gif" /></td>
	               <td valign="top"><h3><fmt:message key="printEventMember" /></h3></td>
	              </tr>
	             </table>
	             </td>
	           </tr>
	           <tr>
	            <td valign="top">
	             <table cellpadding="1" cellspacing="1">
	              <tr style="">
			       <td>
			        <div align="left" class="buttonLeft">
		            <input type="submit" name="__printEventMember" value="Print" onClick="return optionsSelected()">
		            </div>
			       </td>
			     </tr>
	             </table>
	            </td>
	           </tr>
	           </table>
	       </div>
	       <c:if test="${gSiteConfig['gGROUP_CUSTOMER']}">  
			 			<div style="float:left;padding: 5px">
	           			<table class="productBatchTool" cellpadding="5" cellspacing="5">
	           				<tr>
	            				<td valign="top">
	             				<table cellpadding="1" cellspacing="1" width="100%">
	              				<tr style="height:30px;white-space: nowrap">
	               				<td valign="top" width="20px"><img class="toolTipImg" title="Group Id::Associate group Id to the following customer(s) in the list. Can be separated by comma." src="../graphics/question.gif" /></td>
	               				<td valign="top"><h3><fmt:message key="groupId" /></h3></td>
	               				<td valign="top" align="right"><input type="checkbox" onclick="toggleAll(this)" name="__groupAssignAll" value="true"/> <fmt:message key="all"/></td>
	              				</tr>
	             				</table>
	            				</td>
	           				</tr>
	           				<tr>
	            				<td valign="top">
	             				<table cellpadding="1" cellspacing="1">
	              				<tr style="">
			       				<td>
			        				<input name="__group_id" type="text" size="15"/>
			        				<div align="left" class="buttonLeft">
		            				<input type="submit" name="__groupAssignPacher" value="Apply" onClick="return optionsSelected()">
		            				</div>
			       				</td>
			       				<td >
			        				<div><p><input name="__batch_type" type="radio" checked="checked" value="add"/>Add</p></div>
				    				<div><p><input name="__batch_type" type="radio" value="remove"/>Remove</p></div>
				    				<div><p><input name="__batch_type" type="radio" value="move"/>Move</p></div>
			       				</td>
			     				</tr>
	             				</table>
	            				</td>
	           				</tr>
	           			</table>
	         			</div>
	         		</c:if>
		</div>
      </c:if>
	  </td><td class="topr_g" ></td></tr>
  <tr><td class="boxmidlrg" >

 <!-- tabs -->
 <div class="tab-wrapper">
	<h4 class="quickMode" title="List of affiliates"></h4>
	<div>
	<!-- start tab -->
	<p>
	  	<div class="listdivi ln tabdivi quickMode"></div>
	  	<div class="listdivi"></div>
	   
		  	<div class="listlight">
		  	<!-- input field --> 
		  	<table border="0" cellpadding="0" cellspacing="1" width="100%" class="quickMode">
			  <tr>
			  <c:if test="${model.count > 0}">
			  <td class="pageShowing">
			  <fmt:message key="showing">
				<fmt:param value="${eventMemberSearch.offset+1}"/>
				<fmt:param value="${model.pageEnd}"/>
				<fmt:param value="${model.count}"/>
			  </fmt:message>
			  </td>
			  </c:if>
			  <td class="pageNavi">
			  Page
			  <select name="page" id="page" onchange="submit()">
			  <c:forEach begin="1" end="${model.pageCount}" var="page">
			  	<option value="${page}" <c:if test="${page == (eventMemberSearch.page)}">selected</c:if>>${page}</option>
			  </c:forEach>
			  </select>
			  of <c:out value="${model.pageCount}"/>
			  | 
			  <c:if test="${eventMemberSearch.page == 1}"><span class="pageNaviDead">previous</span></c:if>
			  <c:if test="${eventMemberSearch.page != 1}"><a href="<c:url value="eventMemberList.jhtm"><c:param name="page" value="${eventMemberSearch.page-1}"/><c:param name="id" value="${eventMemberSearch.eventId}"/></c:url>" class="pageNaviLink">previous</a></c:if>
			  | 
			  <c:if test="${eventMemberSearch.page == model.pageCount}"><span class="pageNaviDead">next</span></c:if>
			  <c:if test="${eventMemberSearch.page != model.pageCount}"><a href="<c:url value="eventMemberList.jhtm"><c:param name="page" value="${eventMemberSearch.page+1}"/><c:param name="id" value="${eventMemberSearch.eventId}"/></c:url>" class="pageNaviLink">next</a></c:if>
			  </td>
			  </tr>
			</table>
			
			<table border="0" cellpadding="0" cellspacing="0" width="100%" class="listings">
			  <tr class="listingsHdr2NoImage">
			    <c:if test="${model.count > 0}">
			    <td align="left" style="min-width:50px;" class="quickMode"><input type="checkbox" onclick="toggleAll(this)"></td>
			    </c:if>
			    <%-- 
			    <td>
			    	<table cellspacing="0" cellpadding="1">
			    	  <tr>
			    	    <c:choose>
			    	      <c:when test="${eventMemberSearch.sort == 'last_name, first_name desc'}">
			    	        <td class="listingsHdr3">
			    	        <a class="hdrLink" href="#" onClick="document.getElementById('sort').value='last_name, first_name';document.getElementById('list').submit()"><fmt:message key="lastName" /></a>
			    	        </td>
			    	        <td><img src="../graphics/up.gif" border="0"></td>
			    	      </c:when>
			    	      <c:when test="${eventMemberSearch.sort == 'last_name, first_name'}">
			    	        <td class="listingsHdr3">
			    	        <a class="hdrLink" href="#" onClick="document.getElementById('sort').value='last_name, first_name desc';document.getElementById('list').submit()"><fmt:message key="lastName" /></a>
			    	        </td>
			    	        <td><img src="../graphics/down.gif" border="0"></td>
			    	      </c:when>
			    	      <c:otherwise>
			    	        <td colspan="2" class="listingsHdr3">
			    	        <a class="hdrLink" href="#" onClick="document.getElementById('sort').value='last_name, first_name desc';document.getElementById('list').submit()"><fmt:message key="lastName" /></a>
			    	        </td>
			    	      </c:otherwise>
			    	    </c:choose>  
			    	  </tr>
			    	 </table>
			    </td>
			    <td>
			    	<table cellspacing="0" cellpadding="1">
			    	  <tr>
			    	    <c:choose>
			    	      <c:when test="${eventMemberSearch.sort == 'first_name, last_name desc'}">
			    	        <td class="listingsHdr3">
			    	        <a class="hdrLink" href="#" onClick="document.getElementById('sort').value='first_name, last_name';document.getElementById('list').submit()"><fmt:message key="firstName" /></a>
			    	        </td>
			    	        <td><img src="../graphics/up.gif" border="0"></td>
			    	      </c:when>
			    	      <c:when test="${eventMemberSearch.sort == 'first_name, last_name'}">
			    	        <td class="listingsHdr3">
			    	        <a class="hdrLink" href="#" onClick="document.getElementById('sort').value='first_name, last_name desc';document.getElementById('list').submit()"><fmt:message key="firstName" /></a>
			    	        </td>
			    	        <td><img src="../graphics/down.gif" border="0"></td>
			    	      </c:when>
			    	      <c:otherwise>
			    	        <td colspan="2" class="listingsHdr3">
			    	        <a class="hdrLink" href="#" onClick="document.getElementById('sort').value='first_name, last_name desc';document.getElementById('list').submit()"><fmt:message key="firstName" /></a>
			    	        </td>
			    	      </c:otherwise>
			    	    </c:choose>  
			    	  </tr>
			    	  </table>
			    </td>
			    <td>
			    	<table cellspacing="0" cellpadding="1">
			    	  <tr>
			    	    <c:choose>
			    	      <c:when test="${eventMemberSearch.sort == 'username desc'}">
			    	        <td class="listingsHdr3">
			    	        <a class="hdrLink" href="#" onClick="document.getElementById('sort').value='username';document.getElementById('list').submit()"><fmt:message key="emailAddress" /></a>
			    	        </td>
			    	        <td><img src="../graphics/up.gif" border="0"></td>
			    	      </c:when>
			    	      <c:when test="${eventMemberSearch.sort == 'username'}">
			    	        <td class="listingsHdr3">
			    	        <a class="hdrLink" href="#" onClick="document.getElementById('sort').value='username desc';document.getElementById('list').submit()"><fmt:message key="emailAddress" /></a>
			    	        </td>
			    	        <td><img src="../graphics/down.gif" border="0"></td>
			    	      </c:when>
			    	      <c:otherwise>
			    	        <td colspan="2" class="listingsHdr3">
			    	        <a class="hdrLink" href="#" onClick="document.getElementById('sort').value='username desc';document.getElementById('list').submit()"><fmt:message key="emailAddress" /></a>
			    	        </td>
			    	      </c:otherwise>
			    	    </c:choose>  
			    	  </tr>
			    	  </table>
			    </td>
			    --%>
			    <td class="listingsHdr3WithWrap"><fmt:message key="lastName" /></td>
			    <td class="listingsHdr3WithWrap"><fmt:message key="firstName" /></td>
			    <td class="listingsHdr3WithWrap"><fmt:message key="emailAddress" /></td>
			    <td class="listingsHdr3WithWrap"><fmt:message key="idCard" /></td>
			    <%-- <td class="listingsHdr3"><fmt:message key="phone" /></td> 
			    <td class="listingsHdr3WithWrap"><fmt:message key="accountNumber" /></td>--%>
			    <td class="listingsHdr3WithWrap"><fmt:message key="index" /></td>
			    <td class="listingsHdr3WithWrap" style="width : 20px"><fmt:message key="numOf300PalRegister" /></td>
			    <td class="listingsHdr3WithWrap" style="width : 20px"><fmt:message key="numOf300PalOrders" /></td>
			    <td class="listingsHdr3WithWrap" style="width : 20px"><fmt:message key="sales300Pal" /></td>
			    <td class="listingsHdr3WithWrap" style="width : 20px"><fmt:message key="numOfAuctionRegister" /></td>			    
			    <td class="listingsHdr3WithWrap" style="width : 20px"><fmt:message key="numOfAuctionOrders" /></td>
			    <td class="listingsHdr3WithWrap" style="width : 20px"><fmt:message key="salesAuction" /></td>
			    <td class="listingsHdr3WithWrap"><fmt:message key="print" /></td>
			    <td class="listingsHdr3WithWrap"><fmt:message key="comment" /></td>
			    <td class="listingsHdr3WithWrap"><fmt:message key="eventField1" /></td>
			    <td class="listingsHdr3WithWrap" style="width : 20px"><fmt:message key="taxId" /></td>
			    <td class="listingsHdr3WithWrap">Tax Status</td>
			    <td class="listingsHdr3WithWrap"><fmt:message key="registerDate" /></td>
			    <td align="right">
			        <table cellspacing="0" cellpadding="1">
			    	  <tr>
			    	    <td rowspan="2" class="listingsHdr3"><fmt:message key="lastOrderDate" /></td>
			          </tr>
			    	</table>
			    </td>
			    <c:if test="${(gSiteConfig['gSALES_REP'])}">
			    <td align="right">
			        <table cellspacing="0" cellpadding="1">
			    	  <tr>
			    	    <td rowspan="2" class="listingsHdr3"><fmt:message key="salesRep" /></td>
			          </tr>
			    	</table>
			    </td>
			    </c:if>
			  </tr>
			<c:forEach items="${model.eventMemberList}" var="eventMember" varStatus="status">
			  <tr class="row${status.index % 2}" onmouseover="changeStyleClass(this,'rowOver')" onmouseout="changeStyleClass(this,'row${status.index % 2}')">
			    <td width="50px" class="quickMode">
			      <table border="0" cellspacing="0" cellpadding="0">
			      <tr>
			        <td align="left"><input name="__selected_id" value="${eventMember.userId}" type="checkbox"></td>
			        <td class="indexCol"><c:out value="${status.count}"/>.</td>
			        <td><span style="width:16px;float:left;"><a href="../customers/customerQuickView.jhtm?cid=${eventMember.userId}" rel="width:900,height:400" id="mb${eventMember.userId}" class="mbCustomer" title="<fmt:message key="customer" />ID : ${eventMember.userId}"><img src="../graphics/magnifier.gif" border="0" /></a></span></td>
			        <c:if test="${gSiteConfig['gADD_INVOICE']}">
			          <td><a href="../orders/addInvoice.jhtm?cid=${eventMember.userId}"><img src="../graphics/add.png" alt="Add Order" title="Add Order" border="0"></a></td>
			        </c:if>
			      </tr>
			      </table>
			    </td>
			    <td class="nameCol"><a href="../customers/customer.jhtm?id=${eventMember.userId}"><c:out value="${eventMember.lastName}"/></a></td>
			    <td class="nameCol"><a href="../customers/customer.jhtm?id=${eventMember.userId}"><c:out value="${eventMember.firstName}"/></a></td>
			    <td class="nameCol"><c:out value="${eventMember.username}"/></td>
			    <td class="nameCol"><c:out value="${eventMember.cardId}"/></td>
			    <%-- <td class="nameCol"><c:out value="${eventMember.phone}"/></td> 
			    <td class="nameCol"><c:out value="${eventMember.accountNumber}"/></td>  --%>
			    <td class="nameCol"><c:out value="${eventMember.userIndex}"/></td>
			    <td class="nameCol"><c:out value="${eventMember.numOf300PalRegister}"/></td>
			    <td class="nameCol"><c:out value="${eventMember.numOf300PalOrders}"/></td>
			    <td align="center"><fmt:formatNumber value="${eventMember.sales300Pal}" pattern="$##,##0.00" /></td>	
			    <td class="nameCol"><c:out value="${eventMember.numOfAuctionRegister}"/></td>
			    <td class="nameCol"><c:out value="${eventMember.numOfAuctionOrders}"/></td>
			    <td align="center"><fmt:formatNumber value="${eventMember.salesAuction}" pattern="$##,##0.00" /></td>		    
			    <td class="nameCol"><a href="eventMemberList.jhtm?id=${param.id}&cid=${eventMember.userId}&__print=true"><fmt:message key="print" /></a></td>
			    <td class="nameCol" style="width:100px;"><a href="eventMemberForm.jhtm?id=${eventMember.eventId}&cid=${eventMember.userId}"><img border="0" src="../graphics/edit.gif" align="left"/></a><c:out value="${eventMember.comment}"/></td>
			    <td class="nameCol"><c:out value="${eventMember.field1}"/></td>
			    <td class="nameCol"><c:out value="${eventMember.taxId}"/></td>
			    <td class="nameCol"><c:out value="${eventMember.field20}"/></td>
			    <td class="nameCol"><fmt:formatDate type="both" timeStyle="full" pattern="MM/dd/yyyy hh:mm a" value="${eventMember.registerDate}"/></td>
				<td align="right"><fmt:formatDate type="both" timeStyle="full" pattern="MM/dd/yyyy" value="${eventMember.lastOrderDate}"/></td>
				<c:if test="${(gSiteConfig['gSALES_REP'])}">
			    <td align="center"><c:out value="${model.salesRepMap[eventMember.salesRepId].name}"/></td>
			    </c:if>
			  </tr>
			</c:forEach>
			<c:if test="${model.count == 0}">
			  <tr class="emptyList"><td colspan="3">&nbsp;</td></tr>
			</c:if>
			</table>
			
			<table border="0" cellpadding="0" cellspacing="1" width="100%" class="quickMode">
			  <tr>
			  <td class="pageSize">
			  <select name="size" onchange="document.getElementById('page').value=1;submit()">
			  <c:forTokens items="10,25,50,100,200,500" delims="," var="current">
			  	  <option value="${current}" <c:if test="${current == eventMemberSearch.pageSize}">selected</c:if>>${current} <fmt:message key="perPage" /></option>
			  </c:forTokens>
			  </select>
			  </td>
			  </tr>
			</table>
		  	<!-- end input field -->  	  
		  	
		  	<!-- start button -->
            <div align="left" class="button quickMode">
			    <sec:authorize ifAnyGranted="ROLE_AEM,ROLE_ADMIN_${prop_site_id},ROLE_EVENT_CREATE">
			      <%-- <input type="submit" name="__add" value="<fmt:message key="eventAdd" />"> --%>
			    </sec:authorize>   
			  	<c:if test="${model.count > 0}">
			  	<sec:authorize ifAnyGranted="ROLE_AEM,ROLE_ADMIN_${prop_site_id},ROLE_EVENT_DELETE">  
			  	  <input type="submit" name="__delete" value="Delete Member" onClick="return deleteSelected()">
			  	</sec:authorize>   
			  </c:if>
	  	    </div>
			<!-- end button -->	
			</div>
	
	<!-- end tab -->        
	</div>
<!-- end tabs -->			
</div>
  
  <!-- end table -->
   </td><td class="boxmidr" ></td></tr>
  <tr><td class="botl"></td><td class="botr"></td></tr>
  </table>
  
<!-- end main box -->  
</div> 
</form> 
</sec:authorize>

  </tiles:putAttribute>    
</tiles:insertDefinition>