<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
  <head>
    <title>Customer Info</title>
    <link href="<c:url value="../stylesheet${gSiteConfig['gRESELLER']}.css"/>" rel="stylesheet" type="text/css"/>
    <link href="<c:url value="../stylesheet2.css"/>" rel="stylesheet" type="text/css"/>
    <script type="text/javascript" src="../javascript/mootools-1.2.5-core.js"></script>
    <script type="text/javascript" src="../javascript/mootools-1.2.5.1-more.js"></script>
  </head> 
<style type="text/css">
html {
overflow-x: hidden;
}
</style>   
<body style="background-color:#ffffff">
<script type="text/javascript"> 
window.addEvent('domready', function(){
			
});
</script>
<!-- main box -->
  <div id="mboxfullpopup">
      <div class="title">
        <span>Register to Event</span>
      </div>
      <div id="main">
      <form action="../event/registerEventMember.jhtm" method="post">
      <input type="hidden" name="_page" value="0">
      <input type="hidden" id="finish" name="_finish">
      <c:set var="classIndex" value="0" />
      
      <c:forEach items="${model.events}" var="event">
      <div class="list${classIndex % 2}"><c:set var="classIndex" value="${classIndex+1}" />
        <table>
        <tr>
         <td style="width:30px;"><input type="checkbox" name="eventId" value="${event.id}" ></td>
         <td><div class="eventName"><c:out value="${event.eventName}" /></div>
         <div class="eventDate"><fmt:formatDate type="both" timeStyle="full" pattern="MM/dd/yyyy hh:mm a" value="${event.startDate}"/></div>
         </td>
        </tr>
        </table>
      </div>
      </c:forEach>
      <div align="left" class="button"> 
        <input type="submit" name="Register"/>
      </div>
      </form>    
      </div>

<!-- end main box -->  
  </div>    

</body>
</html>