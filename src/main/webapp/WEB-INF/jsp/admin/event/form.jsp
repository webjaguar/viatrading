<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ taglib uri="/WEB-INF/tlds/spring.tld" prefix="spring" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ taglib uri="http://www.springframework.org/security/tags" prefix="sec" %>

<tiles:insertDefinition name="admin.event" flush="true">
  <tiles:putAttribute name="content" type="string">
<sec:authorize ifAnyGranted="ROLE_AEM,ROLE_ADMIN_${prop_site_id},ROLE_EVENT_CREATE,ROLE_EVENT_UPDATE,ROLE_EVENT_DELETE">
  
<script type="text/javascript" src="<c:url value="/FCKeditor/fckeditor.js"/>"></script>
<link rel="stylesheet" type="text/css" media="all" href="./../../admin/javascript/jscalendar-1.0/skins/aqua/theme.css" title="Aqua" /> <!-- calendar stylesheet -->
<script type="text/javascript" src="./../../admin/javascript/jscalendar-1.0/calendar.js"></script> <!-- main calendar program -->
<script type="text/javascript" src="./../../admin/javascript/jscalendar-1.0/lang/calendar-en.js"></script>
<script type="text/javascript" src="./../../admin/javascript/jscalendar-1.0/calendar-setup.js"></script>
<script language="JavaScript"> 
<!--
window.addEvent('domready', function(){			
	var tabs1 = new SimpleTabs($('tab-block-1'), { selector: 'h4'});
	var Tips1 = new Tips($$('.toolTipImg'));
});
function loadEditor(el) {
	var oFCKeditor = new FCKeditor(el);
	oFCKeditor.BasePath = "<c:url value="/FCKeditor/"/>";
	oFCKeditor.Config["CustomConfigurationsPath"] = '<c:url value="/admin/javascript/fckconfig.js"/>';
	oFCKeditor.ToolbarSet = 'Html';
	oFCKeditor.Width = 650;
	oFCKeditor.Height = 400;
	oFCKeditor.ReplaceTextarea();
	document.getElementById(el + '_link').style.display="none";
}
//--> 
</script> 
<form:form commandName="eventForm" action="event.jhtm" method="post">
<form:hidden path="newEvent" />
<form:hidden path="event.id" />

<!-- main box -->
  <div id="mboxfull">
  
  <!-- start table -->
  <table cellpadding="0" cellspacing="0" border="0" class="module" >
  <tr>
    <td class="topl_g">
	  
	  <!-- breadcrumb -->
	  <p class="breadcrumb">
	    <a href="../event">Event</a> &gt;
	    form 
	  </p>
	  
	  <!-- Error Message -->
	  <%--<c:if test="${message != null}">
  		<div class="message"><fmt:message key="${message}"/></div>
  	  </c:if> --%>
  	  <spring:hasBindErrors name="eventForm">
		<div class="message">Please fix all errors!</div>
	  </spring:hasBindErrors>

  </td><td class="topr_g" ></td></tr>
  <tr><td class="boxmidlrg" >

 <!-- tabs -->
 <div id="tab-block-1">
	<h4 title="Event Form">Event</h4>
	<div>
	<!-- start tab -->

         <c:set var="classIndex" value="0" />
         
	  	 <div class="listdivi ln tabdivi"></div>
	  	 <div class="list${classIndex % 2}"><c:set var="classIndex" value="${classIndex+1}" />
		 <div class="listfl"><fmt:message key="active" />:</div>
		 <div class="listp">
		 <!-- input field -->
			<form:checkbox path="event.active" htmlEscape="true"/>
			<form:errors path="event.active" cssClass="error"/>
	 	 <!-- end input field -->	
	 	 </div>
		 </div>	

		 <div class="list${classIndex % 2}"><c:set var="classIndex" value="${classIndex+1}" />
		 <div class="listfl"><div class="requiredField"><fmt:message key="eventName" />:</div></div>
		 <div class="listp">
		 <!-- input field -->
		  		<c:choose>
		  		  <c:when test="${fn:length(siteConfig['EVENT_LIST'].value)>0 and eventForm.newEvent}">
		  		    <select name="event.eventName">
	       			  <c:forTokens items="${siteConfig['EVENT_LIST'].value}" delims="," var="type" varStatus="status">
			            <option value ="${type}"><c:out value ="${type}" /></option>
		              </c:forTokens>
	              </select>
	    		  </c:when>
	    		  <c:otherwise>
	    		  	<form:input path="event.eventName" maxlength="255" size="60" htmlEscape="true"/>
				  </c:otherwise>
		  		</c:choose>
		  		<form:errors path="event.eventName" cssClass="error"/>
	 	 <!-- end input field -->	
	 	 </div>
		 </div>	

		 <div class="list${classIndex % 2}"><c:set var="classIndex" value="${classIndex+1}" />
		 <div class="listfl"><div class="requiredField"><fmt:message key='startDate' />:</div></div>
		 <div class="listp">
		 <!-- input field -->
				<form:input path="event.startDate" cssClass="textfield" size="19" maxlength="19" />
				  <img src="../graphics/calendarIcon.jpg" class="calendarImage" id="time_start_trigger"/>
				  <script type="text/javascript">
		    		Calendar.setup({
		        			inputField     :    "event.startDate",   // id of the input field
		        			showsTime      :    true,
		        			ifFormat       :    "%m-%d-%Y %I:%M %p",   // format of the input field
		        			button         :    "time_start_trigger",   // trigger for the calendar (button ID)
		        			timeFormat     :    "12"
		    		});	
				  </script> 
				<form:errors path="event.startDate" cssClass="error"/>	
		 <!-- end input field -->	
	 	 </div>
		 </div>
		  	
		  	
		 <div class="list${classIndex % 2}"><c:set var="classIndex" value="${classIndex+1}" />
		 <div class="listfl"><div class="requiredField"><fmt:message key='endDate' />:</div></div>
		 <div class="listp">
		 <!-- input field -->
				<form:input path="event.endDate" cssClass="textfield" size="19" maxlength="19" />
				  <img src="../graphics/calendarIcon.jpg" class="calendarImage" id="time_end_trigger"/>
				  <script type="text/javascript">
		    		Calendar.setup({
		        			inputField     :    "event.endDate",   // id of the input field
		        			showsTime      :    true,
		        			ifFormat       :    "%m-%d-%Y %I:%M %p",   // format of the input field
		        			button         :    "time_end_trigger",   // trigger for the calendar (button ID)
		        			timeFormat     :    "12"
		    		});	
				  </script> 
				<form:errors path="event.endDate" cssClass="error"/>		
	 	 <!-- end input field -->	
	 	 </div>
		 </div>			
		  	
		 <div class="list${classIndex % 2}"><c:set var="classIndex" value="${classIndex+1}" />
		 <div class="listfl"><div class="requiredField"><fmt:message key="active" />&nbsp;<fmt:message key="customer" />&nbsp;<fmt:message key="only" />:</div></div>
		 <div class="listp">
		 <!-- input field -->
				<form:checkbox path="event.activeUserOnly"  disabled="true" htmlEscape="true"/>
				<form:errors path="event.activeUserOnly" cssClass="error"/>
	 	 <!-- end input field -->	
	     </div>
		 </div>	
			
		 <div class="list${classIndex % 2}"><c:set var="classIndex" value="${classIndex+1}" />
		 <div class="listfl"><fmt:message key="categoryHtml" />:</div>
		 <div class="listp">
	   			<table border="0" cellpadding="0" cellspacing="0"><tr><td>
      	  		<form:textarea rows="15" cols="60" path="event.htmlCode" cssClass="textfield" htmlEscape="true" />
      	  		<form:errors path="event.htmlCode" cssClass="error" /> 
	  	  		<div style="text-align:right" id="event.htmlCode_link"><a href="#" onClick="loadEditor('event.htmlCode')" class="htmleditor">- <fmt:message key="htmlEditor" /> -</a></div>
		  		</td></tr></table>
		 </div>
		 </div>	
		 <div class="list${classIndex % 2}"><c:set var="classIndex" value="${classIndex+1}" />
		 <div class="listfl"><fmt:message key="categoryHtmlEs" />:</div>
		 <div class="listp">
		  		<table border="0" cellpadding="0" cellspacing="0"><tr><td>
      	  		<form:textarea rows="15" cols="60" path="event.htmlCodeEs" cssClass="textfield" htmlEscape="true" />
      	  		<form:errors path="event.htmlCodeEs" cssClass="error" /> 
	  	  		<div style="text-align:right" id="event.htmlCode_link"><a href="#" onClick="loadEditor('event.htmlCodeEs')" class="htmleditor">- <fmt:message key="htmlEditor" /> -</a></div>
		  		</td></tr></table>
		  		#firstname# --&gt; <fmt:message key="firstName" /><br/>	
		  	#lastname# --&gt; <fmt:message key="lastName" /><br/>	
		  	#cardid# --&gt; <fmt:message key="idCard" /><br/>	
		  	#salesRepName# --&gt; <fmt:message key="salesRepName" /><br/>	
		  	#accountNumber# --&gt; <fmt:message key="accountNumber" /><br/>
		  	#eventname# --&gt; <fmt:message key="eventName" /><br/>
		  	#eventdate# --&gt; <fmt:message key="eventDate" /><br/>
		  	#queue# --&gt; <fmt:message key="queue" /><br/>
		  	#customerfield1# --&gt; <fmt:message key="customer" /> <fmt:message key="Field" />1<br/>
		 </div>
		 </div>	
		 
		
	<!-- end tab -->        
	</div>         

<!-- end tabs -->			
</div>

<!-- start button -->
	<div align="left" class="button">
	  <c:if test="${eventForm.newEvent}">
	    <sec:authorize ifAnyGranted="ROLE_AEM,ROLE_ADMIN_${prop_site_id},ROLE_EVENT_CREATE">
          <input type="submit" value="<spring:message code="eventAdd"/>" />
        </sec:authorize>  
      </c:if>
      <c:if test="${!eventForm.newEvent}">
        <sec:authorize ifAnyGranted="ROLE_AEM,ROLE_ADMIN_${prop_site_id},ROLE_EVENT_UPDATE">
          <input type="submit" value="<spring:message code="eventUpdate"/>" />
        </sec:authorize>
        <sec:authorize ifAnyGranted="ROLE_AEM,ROLE_ADMIN_${prop_site_id},ROLE_EVENT_DELETE">  
          <input type="submit" value="<spring:message code="eventDelete"/>" name="_delete" onClick="return confirm('Delete permanently?')" />
        </sec:authorize>  
      </c:if>
	</div>
<!-- end button -->
  
  <!-- end table -->
    </td><td class="boxmidr" ></td></tr>
  <tr><td class="botl"></td><td class="botr"></td></tr>
  </table>
  
<!-- end main box -->  
</div>
</form:form>
</sec:authorize> 

  </tiles:putAttribute>    
</tiles:insertDefinition>