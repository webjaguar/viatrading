<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<!-- menu -->
  <div id="lbox" class="quickMode">
   <table cellpadding="0" cellspacing="0" border="0" class="module" style="width: 100%"><tr><td class="topl">
    </td><td class="topr" ></td></tr><tr><td class="boxmid" >
  
    <h2 class="menuleft mfirst"><fmt:message key="eventMenuTitle"/></h2>
    <div class="menudiv"></div>
      <a href="eventList.jhtm" class="userbutton"><fmt:message key="list"/></a>
    <div class="menudiv"></div>
	  	  
	  <c:if test="${param.tab == 'event'}">
	  <div align="left" class="searchHeader" id="searchHeaderId"><h2><fmt:message key="search" /></h2></div>
	  <table class="searchBoxLeft"><tr><td valign="top" style="width: 110px;">
	  <form name="searchform" action="eventList.jhtm" method="post" >
	  <!-- content area -->
	    <div class="search2">
	      <p><fmt:message key="eventName" />:</p>
	      <input name="eventName" id="eventName" type="text" value="<c:out value='${eventSearch.eventName}' />" size="15" />
	    </div> 
	    <div class="search2">
	      <p><fmt:message key="active" />:</p>
	      <select name="_active">
	        <option value=""><fmt:message key="all"/></option> 
	        <option value="0" <c:if test="${eventSearch.active == '0'}">selected</c:if>><fmt:message key="inactive" /></option>
	  	    <option value="1" <c:if test="${eventSearch.active == '1'}">selected</c:if>><fmt:message key="active" /></option>
	      </select>
	    </div>
	    <div class="search2">
	      <p><fmt:message key="inEffect" />:</p>
	      <select name="_inEffect">
	        <option value=""><fmt:message key="all"/></option> 
	  	    <option value="1" <c:if test="${eventSearch.inEffect == '1'}">selected</c:if>><fmt:message key="inEffect" /></option>
	      </select>
	    </div>
	   <div class="button">
	      <input type="submit" value="Search"/>
	   </div>
	  <!-- content area -->
	  </form>
	  </td></tr></table>
	  </c:if>
	  
	  <c:if test="${param.tab == 'eventMember'}"> 
      <div align="left" class="searchHeader" id="searchHeaderId"><h2><fmt:message key="search" /></h2></div>
	  <table class="searchBoxLeft" width="100%"><tr><td>
	  <form action="eventMemberList.jhtm" method="post">
	  <input type="hidden" name="id" value="<c:out value='${eventMemberSearch.eventId}' />" />
      <!-- content area -->
      <c:if test="${gSiteConfig['gSALES_REP']}">
	    <div class="search2">
	      <p><fmt:message key="salesRep" />:</p>
	      <select name="salesRepId">
	       <option value="-1"></option>
	       <option value="-2" <c:if test="${eventMemberSearch.salesRepId == -2}">selected</c:if>><fmt:message key="noSalesRep" /></option>
	       <c:forEach items="${model.salesReps}" var="salesRep">
	  	     <option value="${salesRep.id}" <c:if test="${eventMemberSearch.salesRepId == salesRep.id}">selected</c:if>><c:out value="${salesRep.name}" /></option>
	       </c:forEach>
	      </select>
	  </div>  
	  </c:if>
	  <div class="search2">
	      <p><fmt:message key="firstName" />:</p>
	      <input name="firstName" id="firstName" type="text" value="<c:out value='${eventMemberSearch.firstName}' />" size="15" />
	  </div> 
	  <div class="search2">
	      <p><fmt:message key="lastName" />:</p>
	      <input name="lastName" id="lastName" type="text" value="<c:out value='${eventMemberSearch.lastName}' />" size="15" />
	  </div>  
	  <div class="search2">
	      <p><fmt:message key="email" />:</p>
	      <input name="email" type="text" value="<c:out value='${eventMemberSearch.email}' />" size="15" />
	  </div>
      <c:if test="${gSiteConfig['gREGISTRATION_TOUCH_SCREEN']}">
	    <div class="search2">
	      <p><fmt:message key="idCard" />:</p>
	      <input name="cardId" type="text" value="<c:out value='${eventMemberSearch.cardId}' />" size="15" />
	    </div>
	  </c:if>    
      <div class="button">
	      <input type="submit" value="Search"/>
      </div>
      <!-- content area -->
	  </form>
	  </td></tr></table>
	  </c:if>

	<div class="menudiv"></div>  
		  
     		  
    </td><td class="boxmidr" ></td></tr>
    <tr><td class="botl"></td><td class="botr"></td></tr></table>
  </div>