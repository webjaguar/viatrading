<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/security/tags" prefix="sec" %>
	<style>#lbox{display:none;} #mboxfull{margin:0 !important;}</style>
<tiles:insertDefinition name="admin.report" flush="true">
  <tiles:putAttribute name="content" type="string">
<sec:authorize ifAnyGranted="ROLE_AEM,ROLE_ADMIN_${prop_site_id},ROLE_REPORT_LAY">   
<!-- main box -->
  <div id="mboxfull">
  
  <!-- start table -->
  <table cellpadding="0" cellspacing="0" border="0" class="module" >
  <tr>
    <td class="topl_g">
 <!-- Error -->
	  <c:if test="${!empty model.message}">
		<div class="message"><fmt:message key="${model.message}" /></div>
	  </c:if>

	  
  </td><td class="topr_g" ></td></tr>
  <tr><td class="boxmidlrg" >  
  <!-- start table -->

	<table style="margin-top: 10px; width: 99%; padding-top: 37px;clear: left;" cellpadding="5" cellspacing="5">
           <tr>
            <td style="line-height:8px; height:20px;" valign="top">
              <iframe style="width:100%; min-height:700px; border:2px solid #9a9a9a; " src="https://www.viatrading.com/dv/orderreport/salesreport.php"></iframe>
            </td>
           </tr>
        </table>			
</div>
  
  <!-- end table -->
  </td><td class="boxmidr" ></td></tr>
  <tr><td class="botl"></td><td class="botr"></td></tr>
  </table>
  
<!-- end main box -->  
</div>

 </sec:authorize>
  </tiles:putAttribute>
</tiles:insertDefinition>