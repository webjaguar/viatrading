<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/security/tags" prefix="sec" %>

<tiles:insertDefinition name="admin.report.product" flush="true">
  <tiles:putAttribute name="content" type="string">
  
<sec:authorize ifAnyGranted="ROLE_AEM,ROLE_ADMIN_${prop_site_id},ROLE_REPORT_PRODUCT">
<script type="text/javascript">
<!--
window.addEvent('domready', function(){
	var box1 = new multiBox('mbOrder', {descClassName: 'descClassName',showNumbers: false,overlay: new overlay()});
	// Create the accordian
	var myAccordion = new Fx.Accordion($$('div.arrow_drop'), $$('div.information'), {
		display: 1, alwaysHide: true,
    	onActive: function() {$('information').removeClass('displayNone');}
	});
	<sec:authorize ifAnyGranted="ROLE_AEM,ROLE_ADMIN_${prop_site_id},ROLE_QUICK_EXPORT">
	$('quickModeTrigger').addEvent('click',function() {
	    $('mboxfull').setStyle('margin', '0');
	    $$('.quickMode').each(function(el){
	    	el.hide();
	    });
	    $$('.quickModeRemove').each(function(el){
	    	el.destroy();
	    });
	    $$('.totals').each(function(el){
	    	el.destroy();
	    });
	    $$('.listingsHdr3').each(function(el){
	    	el.removeProperties('colspan', 'class');
	    });
	    alert('Select the whole page, copy and paste to your excel file.');	   
	});
	</sec:authorize> 
});

function showShipDate(orderId, userId) {
     var req = new Request({  
         method: 'get',  
         onRequest: function() { 
         	$('shipDateHelp'+orderId+userId).setStyle('display', 'none'),
            this.box = new Element('img', {src:'../graphics/spinner.gif', id:'syncSpinner'+orderId+userId}).inject($('shipDateBlock'+orderId+userId),'inside');},  
         onComplete: function(response) { $('shipDateBlock'+orderId+userId).innerHTML = "<span style='color:#c00000;'> " + response  + "</span>";
         	$('syncSpinner'+orderId+userId).setStyle('display', 'none'); },
         url: '../orders/show-ajax-shipDate.jhtm?orderId='+orderId}).send();   
}
//-->
</script>
<form name="list" id="list" action="">
<input type="hidden" id="sort" name="sort" value="${productListFilter.sort}" />
<!-- main box -->
  <div id="mboxfull">
  
  <!-- start table -->
  <table cellpadding="0" cellspacing="0" border="0" class="module" >
  <tr class="quickMode">
    <td class="topl_g">
	  
	  <!-- breadcrumb -->
	  <p class="breadcrumb">
	    <a href="../reports"><fmt:message key="reports" /></a> &gt;
	    <fmt:message key="sku" />
	  </p>
	  
	  <!-- Error -->
	  <c:if test="${!empty model.message}">
		<div class="message"><fmt:message key="${model.message}" /></div>
	  </c:if>
	  
	  <!-- header image -->
	  <img id="quickModeTrigger" class="headerImage" src="../graphics/reports.gif" alt=""/> 
	  
  </td><td class="topr_g" ></td></tr>
  <tr><td class="boxmidlrg" >

 <!-- tabs -->
 <div class="tab-wrapper">
	<h4 title=""></h4>
	<div>
	<!-- start tab -->
	
	  	<div class="listdivi"></div>
	    
		  	<div class="listlight">
		  	<!-- input field -->
		  	
		  	<table border="0" cellpadding="0" cellspacing="1" width="100%" class="quickMode">
			  <tr>
			  <td>
			  <c:if test="${model.count > 0}">
			  <fmt:message key="showing">
				<fmt:param value="${productListFilter.offset + 1}"/>
				<fmt:param value="${model.pageEnd}"/>
				<fmt:param value="${model.count}"/>
			  </fmt:message>
			  </c:if>  
			  </td>
			  <td class="pageNavi">
			  Page 
			  <c:choose>
			  <c:when test="${model.pageCount <= 100 and model.pageCount > 0}">
			  <select name="page" id="page" onchange="submit()">
			  <c:forEach begin="1" end="${model.pageCount}" var="page">
			  	<option value="${page}" <c:if test="${page == (productListFilter.page)}">selected</c:if>>${page}</option>
			  </c:forEach>
			  </select>
			  </c:when>
			  <c:otherwise>
			  <input type="text" id="page" name="page" value="${productListFilter.page}" size="5" class="textfield50" />
			  <input type="submit" value="go"/>
			  </c:otherwise>
			  </c:choose>
			  of <c:out value="${model.pageCount}"/>
			  | 
			  <c:if test="${productListFilter.page == 1}"><span class="pageNaviDead"><fmt:message key="previous" /></span></c:if>
			  <c:if test="${productListFilter.page != 1}"><a href="<c:url value="productListReport.jhtm"><c:param name="page" value="${productSearch.page-1}"/></c:url>" class="pageNaviLink"><fmt:message key="previous" /></a></c:if>
			  | 
			  <c:if test="${productListFilter.page == model.pageCount}"><span class="pageNaviDead"><fmt:message key="next" /></span></c:if>
			  <c:if test="${productListFilter.page != model.pageCount}"><a href="<c:url value="productListReport.jhtm"><c:param name="page" value="${productSearch.page+1}"/></c:url>" class="pageNaviLink"><fmt:message key="next" /></a></c:if>
			  </td>
			  </tr>
			</table>
		  	
		  	
		  	<table border="0" cellpadding="2" cellspacing="0" width="100%" class="listings">
			  <tr class="listingsHdr2">
			    <td class="indexCol">&nbsp;</td>
			      <td class="listingsHdr3">
			    	<table cellspacing="0" cellpadding="1">
			    	  <tr>
			    	    <c:choose>
			    	      <c:when test="${productListFilter.sort == 'product_sku DESC'}">
			    	        <td class="listingsHdr3">
			    	        <a class="hdrLink" href="#" onClick="document.getElementById('sort').value='product_sku';document.getElementById('list').submit()"><fmt:message key="sku" /></a>
			    	        </td>
			    	        <td class="quickModeRemove"><img src="../graphics/up.gif" border="0"></td>
			    	      </c:when>
			    	      <c:when test="${productListFilter.sort == 'product_sku'}">
			    	        <td class="listingsHdr3">
			    	        <a class="hdrLink" href="#" onClick="document.getElementById('sort').value='product_sku DESC';document.getElementById('list').submit()"><fmt:message key="sku" /></a>
			    	        </td>
			    	        <td class="quickModeRemove"><img src="../graphics/down.gif" border="0"></td>
			    	      </c:when>
			    	      <c:otherwise>
			    	        <td colspan="2" class="listingsHdr3">
			    	        <a class="hdrLink" href="#" onClick="document.getElementById('sort').value='product_sku DESC';document.getElementById('list').submit()"><fmt:message key="sku" /></a>
			    	        </td>
			    	      </c:otherwise>
			    	    </c:choose>  
			    	  </tr>
			    	</table>
			      </td>
			      <td class="listingsHdr3">
			    	<table cellspacing="0" cellpadding="1">
			    	  <tr>
			    	    <c:choose>
			    	      <c:when test="${productListFilter.sort == 'product_name DESC'}">
			    	        <td class="listingsHdr3">
			    	        <a class="hdrLink" href="#" onClick="document.getElementById('sort').value='product_name';document.getElementById('list').submit()"><fmt:message key="productName" /></a>
			    	        </td>
			    	        <td class="quickModeRemove"><img src="../graphics/up.gif" border="0"></td>
			    	      </c:when>
			    	      <c:when test="${productListFilter.sort == 'product_name'}">
			    	        <td class="listingsHdr3">
			    	        <a class="hdrLink" href="#" onClick="document.getElementById('sort').value='product_name DESC';document.getElementById('list').submit()"><fmt:message key="productName" /></a>
			    	        </td>
			    	        <td class="quickModeRemove"><img src="../graphics/down.gif" border="0"></td>
			    	      </c:when>
			    	      <c:otherwise>
			    	        <td colspan="2" class="listingsHdr3">
			    	        <a class="hdrLink" href="#" onClick="document.getElementById('sort').value='product_name DESC';document.getElementById('list').submit()"><fmt:message key="productName" /></a>
			    	        </td>
			    	      </c:otherwise>
			    	    </c:choose>  
			    	  </tr>
			    	</table>
			      </td>
			      <td class="listingsHdr3">
			    	<table cellspacing="0" cellpadding="1">
			    	  <tr>
			    	    <c:choose>
			    	      <c:when test="${productListFilter.sort == 'order_id DESC'}">
			    	        <td class="listingsHdr3">
			    	        <a class="hdrLink" href="#" onClick="document.getElementById('sort').value='order_id';document.getElementById('list').submit()"><fmt:message key="orderNumber" /></a>
			    	        </td>
			    	        <td class="quickModeRemove"><img src="../graphics/up.gif" border="0"></td>
			    	      </c:when>
			    	      <c:when test="${productListFilter.sort == 'order_id'}">
			    	        <td class="listingsHdr3">
			    	        <a class="hdrLink" href="#" onClick="document.getElementById('sort').value='order_id DESC';document.getElementById('list').submit()"><fmt:message key="orderNumber" /></a>
			    	        </td>
			    	        <td class="quickModeRemove"><img src="../graphics/down.gif" border="0"></td>
			    	      </c:when>
			    	      <c:otherwise>
			    	        <td colspan="2" class="listingsHdr3">
			    	        <a class="hdrLink" href="#" onClick="document.getElementById('sort').value='order_id DESC';document.getElementById('list').submit()"><fmt:message key="orderNumber" /></a>
			    	        </td>
			    	      </c:otherwise>
			    	    </c:choose>  
			    	  </tr>
			    	</table>
			    </td>
			    <td class="listingsHdr3">
			    	<table cellspacing="0" cellpadding="1">
			    	  <tr>
		    	        <td class="listingsHdr3">
		    	        <fmt:message key="paid"/>   
		    	        </td>
			    	  </tr>
			    	</table>
			    </td>
			    <td class="listingsHdr3">
			    	<table cellspacing="0" cellpadding="1">
			    	  <tr>
		    	        <td class="listingsHdr3">
		    	        <fmt:message key="orderType"/>   
		    	        </td>
			    	  </tr>
			    	</table>
			    </td>
			    <td class="listingsHdr3">
			    	<table cellspacing="0" cellpadding="1">
			    	  <tr>
			    	    <c:choose>
			    	      <c:when test="${productListFilter.sort == 'userid DESC'}">
			    	        <td class="listingsHdr3">
			    	        <a class="hdrLink" href="#" onClick="document.getElementById('sort').value='userid';document.getElementById('list').submit()"><fmt:message key="billTo" /></a>
			    	        </td>
			    	        <td class="quickModeRemove"><img src="../graphics/up.gif" border="0"></td>
			    	      </c:when>
			    	      <c:when test="${productListFilter.sort == 'userid'}">
			    	        <td class="listingsHdr3">
			    	        <a class="hdrLink" href="#" onClick="document.getElementById('sort').value='userid DESC';document.getElementById('list').submit()"><fmt:message key="billTo" /></a>
			    	        </td>
			    	        <td class="quickModeRemove"><img src="../graphics/down.gif" border="0"></td>
			    	      </c:when>
			    	      <c:otherwise>
			    	        <td colspan="2" class="listingsHdr3">
			    	        <a class="hdrLink" href="#" onClick="document.getElementById('sort').value='userid DESC';document.getElementById('list').submit()"><fmt:message key="billTo" /></a>
			    	        </td>
			    	      </c:otherwise>
			    	    </c:choose>  
			    	  </tr>
			    	</table>
			    </td>
			    <td class="listingsHdr3">
			    	<table cellspacing="0" cellpadding="1">
			    	  <tr>
			    	    <c:choose>
			    	      <c:when test="${productListFilter.sort == 'date_ordered DESC'}">
			    	        <td class="listingsHdr3">
			    	        <a class="hdrLink" href="#" onClick="document.getElementById('sort').value='date_ordered';document.getElementById('list').submit()"><fmt:message key="orderDate" /></a>
			    	        </td>
			    	        <td class="quickModeRemove"><img src="../graphics/up.gif" border="0"></td>
			    	      </c:when>
			    	      <c:when test="${productListFilter.sort == 'date_ordered'}">
			    	        <td class="listingsHdr3">
			    	        <a class="hdrLink" href="#" onClick="document.getElementById('sort').value='date_ordered DESC';document.getElementById('list').submit()"><fmt:message key="orderDate" /></a>
			    	        </td>
			    	        <td class="quickModeRemove"><img src="../graphics/down.gif" border="0"></td>
			    	      </c:when>
			    	      <c:otherwise>
			    	        <td colspan="2" class="listingsHdr3">
			    	        <a class="hdrLink" href="#" onClick="document.getElementById('sort').value='date_ordered DESC';document.getElementById('list').submit()"><fmt:message key="orderDate" /></a>
			    	        </td>
			    	      </c:otherwise>
			    	    </c:choose>  
			    	  </tr>
			    	</table>
			    </td>
			    <td class="listingsHdr3">
			    	<table cellspacing="0" cellpadding="1">
			    	  <tr>
			    	    <c:choose>
			    	      <c:when test="${productListFilter.sort == 'status DESC'}">
			    	        <td class="listingsHdr3">
			    	        <a class="hdrLink" href="#" onClick="document.getElementById('sort').value='status';document.getElementById('list').submit()"><fmt:message key="status" /></a>
			    	        </td>
			    	        <td class="quickModeRemove"><img src="../graphics/up.gif" border="0"></td>
			    	      </c:when>
			    	      <c:when test="${productListFilter.sort == 'status'}">
			    	        <td class="listingsHdr3">
			    	        <a class="hdrLink" href="#" onClick="document.getElementById('sort').value='status DESC';document.getElementById('list').submit()"><fmt:message key="status" /></a>
			    	        </td>
			    	        <td class="quickModeRemove"><img src="../graphics/down.gif" border="0"></td>
			    	      </c:when>
			    	      <c:otherwise>
			    	        <td colspan="2" class="listingsHdr3">
			    	        <a class="hdrLink" href="#" onClick="document.getElementById('sort').value='status DESC';document.getElementById('list').submit()"><fmt:message key="status" /></a>
			    	        </td>
			    	      </c:otherwise>
			    	    </c:choose>  
			    	  </tr>
			    	</table>
			    </td>
			    <td class="listingsHdr3">
			    	<table cellspacing="0" cellpadding="1">
			    	  <tr>
			    	    <c:choose>
			    	      <c:when test="${productListFilter.sort == 'salesrep_id DESC'}">
			    	        <td class="listingsHdr3">
			    	        <a class="hdrLink" href="#" onClick="document.getElementById('sort').value='salesrep_id';document.getElementById('list').submit()"><fmt:message key="salesRep" /></a>
			    	        </td>
			    	        <td class="quickModeRemove"><img src="../graphics/up.gif" border="0"></td>
			    	      </c:when>
			    	      <c:when test="${productListFilter.sort == 'salesrep_id'}">
			    	        <td class="listingsHdr3">
			    	        <a class="hdrLink" href="#" onClick="document.getElementById('sort').value='salesrep_id DESC';document.getElementById('list').submit()"><fmt:message key="salesRep" /></a>
			    	        </td>
			    	        <td class="quickModeRemove"><img src="../graphics/down.gif" border="0"></td>
			    	      </c:when>
			    	      <c:otherwise>
			    	        <td colspan="2" class="listingsHdr3">
			    	        <a class="hdrLink" href="#" onClick="document.getElementById('sort').value='salesrep_id DESC';document.getElementById('list').submit()"><fmt:message key="salesRep" /></a>
			    	        </td>
			    	      </c:otherwise>
			    	    </c:choose>  
			    	  </tr>
			    	</table>
			    </td>
			    <td class="listingsHdr3">
					<fmt:message key="dateShipped" />
				</td>
			    <td class="listingsHdr3">
			    	<table cellspacing="0" cellpadding="1">
			    	  <tr>
			    	    <c:choose>
			    	      <c:when test="${productListFilter.sort == 'orders.shipping_method DESC'}">
			    	        <td class="listingsHdr3">
			    	        <a class="hdrLink" href="#" onClick="document.getElementById('sort').value='orders.shipping_method';document.getElementById('list').submit()"><fmt:message key="shippingMethod" /></a>
			    	        </td>
			    	        <td class="quickModeRemove"><img src="../graphics/up.gif" border="0"></td>
			    	      </c:when>
			    	      <c:when test="${productListFilter.sort == 'orders.shipping_method'}">
			    	        <td class="listingsHdr3">
			    	        <a class="hdrLink" href="#" onClick="document.getElementById('sort').value='orders.shipping_method DESC';document.getElementById('list').submit()"><fmt:message key="shippingMethod" /></a>
			    	        </td>
			    	        <td class="quickModeRemove"><img src="../graphics/down.gif" border="0"></td>
			    	      </c:when>
			    	      <c:otherwise>
			    	        <td colspan="2" class="listingsHdr3">
			    	        <a class="hdrLink" href="#" onClick="document.getElementById('sort').value='orders.shipping_method DESC';document.getElementById('list').submit()"><fmt:message key="shippingMethod" /></a>
			    	        </td>
			    	      </c:otherwise>
			    	    </c:choose>  
			    	  </tr>
			    	</table>
			    </td>
			    <td class="listingsHdr3">
			    	<table cellspacing="0" cellpadding="1">
			    	  <tr>
			    	    <c:choose>
			    	      <c:when test="${productListFilter.sort == 'bill_to_company DESC'}">
			    	        <td class="listingsHdr3">
			    	        <a class="hdrLink" href="#" onClick="document.getElementById('sort').value='bill_to_company';document.getElementById('list').submit()"><fmt:message key="company" /></a>
			    	        </td>
			    	        <td class="quickModeRemove"><img src="../graphics/up.gif" border="0"></td>
			    	      </c:when>
			    	      <c:when test="${productListFilter.sort == 'bill_to_company'}">
			    	        <td class="listingsHdr3">
			    	        <a class="hdrLink" href="#" onClick="document.getElementById('sort').value='bill_to_company DESC';document.getElementById('list').submit()"><fmt:message key="company" /></a>
			    	        </td>
			    	        <td class="quickModeRemove"><img src="../graphics/down.gif" border="0"></td>
			    	      </c:when>
			    	      <c:otherwise>
			    	        <td colspan="2" class="listingsHdr3">
			    	        <a class="hdrLink" href="#" onClick="document.getElementById('sort').value='bill_to_company DESC';document.getElementById('list').submit()"><fmt:message key="company" /></a>
			    	        </td>
			    	      </c:otherwise>
			    	    </c:choose>  
			    	  </tr>
			    	</table>
			    </td>
			    <td class="listingsHdr3">
			    	<table cellspacing="0" cellpadding="1">
			    	  <tr>
			    	    <c:choose>
			    	      <c:when test="${productListFilter.sort == 'quantity DESC'}">
			    	        <td class="listingsHdr3">
			    	        <a class="hdrLink" href="#" onClick="document.getElementById('sort').value='quantity';document.getElementById('list').submit()"><fmt:message key="qty" /></a>
			    	        </td>
			    	        <td class="quickModeRemove"><img src="../graphics/up.gif" border="0"></td>
			    	      </c:when>
			    	      <c:when test="${productListFilter.sort == 'quantity'}">
			    	        <td class="listingsHdr3">
			    	        <a class="hdrLink" href="#" onClick="document.getElementById('sort').value='quantity DESC';document.getElementById('list').submit()"><fmt:message key="qty" /></a>
			    	        </td>
			    	        <td class="quickModeRemove"><img src="../graphics/down.gif" border="0"></td>
			    	      </c:when>
			    	      <c:otherwise>
			    	        <td colspan="2" class="listingsHdr3">
			    	        <a class="hdrLink" href="#" onClick="document.getElementById('sort').value='quantity DESC';document.getElementById('list').submit()"><fmt:message key="qty" /></a>
			    	        </td>
			    	      </c:otherwise>
			    	    </c:choose>  
			    	  </tr>
			    	</table>
			    </td>
			    <c:if test="${gSiteConfig['gCASE_CONTENT']}">  
			    <td class="listingsHdr3">
					<fmt:message key="content" />
				</td>
				</c:if>
			    <td class="listingsHdr3">
			    	<table cellspacing="0" cellpadding="1">
			    	  <tr>
			    	    <c:choose>
			    	      <c:when test="${productListFilter.sort == 'unit_price DESC'}">
			    	        <td class="listingsHdr3">
			    	        <a class="hdrLink" href="#" onClick="document.getElementById('sort').value='unit_price';document.getElementById('list').submit()"><fmt:message key="unitPrice" /></a>
			    	        </td>
			    	        <td class="quickModeRemove"><img src="../graphics/up.gif" border="0"></td>
			    	      </c:when>
			    	      <c:when test="${productListFilter.sort == 'unit_price'}">
			    	        <td class="listingsHdr3">
			    	        <a class="hdrLink" href="#" onClick="document.getElementById('sort').value='unit_price DESC';document.getElementById('list').submit()"><fmt:message key="unitPrice" /></a>
			    	        </td>
			    	        <td class="quickModeRemove"><img src="../graphics/down.gif" border="0"></td>
			    	      </c:when>
			    	      <c:otherwise>
			    	        <td colspan="2" class="listingsHdr3">
			    	        <a class="hdrLink" href="#" onClick="document.getElementById('sort').value='unit_price DESC';document.getElementById('list').submit()"><fmt:message key="unitPrice" /></a>
			    	        </td>
			    	      </c:otherwise>
			    	    </c:choose>  
			    	  </tr>
			    	</table>
			    </td>
			    <td class="listingsHdr3">
			    	<table cellspacing="0" cellpadding="1">
			    	  <tr>
			    	    <td rowspan="2" class="listingsHdr3"><fmt:message key="subTotal" /></td>
			    	  </tr>
			    	  <tr>
			    	    <td></td>
			    	  </tr>
			    	</table>
			    </td>
			  </tr>
			  <c:set var="totalSubTotal" value="0.0" />
			  <c:set var="totalQtu" value="0.0" />
		  	  <c:forEach items="${model.productList}" var="productReport" varStatus="status">
			  <tr class="row${status.index % 2}" onmouseover="changeStyleClass(this,'rowOver')" onmouseout="changeStyleClass(this,'row${status.index % 2}')">
			    <td class="indexCol"><c:out value="${status.count + productListFilter.offset}"/>.</td>
			    <td class="nameCol"><a href="../catalog/productList.jhtm?product_sku=${productReport.sku}" class="nameLink"><c:out value="${productReport.sku}"/></a></td>
			    <td class="nameCol"><c:out value="${productReport.name}"/></td>				
			    <td class="nameColCenter"><a href="../orders/invoice.jhtm?order=<c:out value="${productReport.orderId}"/>" ><c:out value="${productReport.orderId}"/></a></td>
			    <td class="nameColCenter"><c:out value="${productReport.percentage}"/>%&nbsp;</td>
			    <td class="nameColCenter"><c:out value="${productReport.orderType}"/>&nbsp;</td>
			    <td class="nameColCenter"><a href="../customers/customer.jhtm?id=<c:out value="${productReport.customer.id}"/>" ><c:out value="${productReport.customer.address.firstName}"/> <c:out value="${productReport.customer.address.lastName}"/></a></td>	
			    <td class="nameColCenter"><fmt:formatDate type="date" timeStyle="default" value="${productReport.orderDate}"/></td>			
			    <td class="nameColCenter"><c:choose><c:when test="${productReport.status != null}"><fmt:message key="orderStatus_${productReport.status}" /></c:when><c:otherwise>&nbsp;</c:otherwise></c:choose></td>	
			    <td class="nameColCenter"><c:out value="${productReport.salesRep.name}"/></td>	
			    <c:choose>
			     <c:when test="${productListFilter.dateType == 'orderDate'}">
			     <td class="nameColCenter"><img id="shipDateHelp${productReport.orderId}<c:out value="${productReport.lineItemNum}"/>" src="../graphics/question.gif" class="quickMode" onclick="showShipDate(${productReport.orderId}, <c:out value="${productReport.lineItemNum}"/>)"/><div id="shipDateBlock${productReport.orderId}<c:out value="${productReport.lineItemNum}"/>"></div></td>
			     </c:when>
			     <c:otherwise>
			      <td class="nameColCenter"><fmt:formatDate type="date" timeStyle="default" value="${productReport.shipped}"/></td>
			     </c:otherwise>
			    </c:choose>	
			    <td class="nameColCenter"><c:out value="${productReport.shippingMethod}" escapeXml="false" /></td>
			    <td class="nameColCenter"><c:out value="${productReport.company}"/></td>
			    <td class="numberCol"><c:out value="${productReport.quantity}"/></td>	<c:set var="totalQtu" value="${productReport.quantity + totalQtu}" />
			    <c:if test="${gSiteConfig['gCASE_CONTENT']}">  
			      <td class="nameCol" align="center"><c:out value="${productReport.caseContent}"/></td>
			    </c:if>
			    <td class="numberCol"><fmt:message key="${siteConfig['CURRENCY'].value}" /><fmt:formatNumber value="${productReport.unitPrice}" pattern="#,##0.00" /></td>			
			    <td class="numberCol"><fmt:message key="${siteConfig['CURRENCY'].value}" /><fmt:formatNumber value="${productReport.subTotal}" pattern="#,##0.00" /></td><c:set var="totalSubTotal" value="${productReport.subTotal + totalSubTotal}" />
			  </tr>
			  </c:forEach>
			  <tr class="totals" >
			  <td style="color:#666666; padding: 5px" colspan="10" align="left"><fmt:message key="total" /></td>
			  <td class="numberCol"><fmt:formatNumber value="${totalQtu}" pattern="#,##0" /></td>
			  <td class="numberCol" colspan="2"><fmt:formatNumber value="${totalSubTotal}" pattern="#,##0.00" /></td>
			</tr>
			</table>
			
			<table border="0" cellpadding="0" cellspacing="1" width="100%" class="quickMode">
			  <tr>
			  <td>&nbsp;</td>  
			  <td class="pageSize">
			  <select name="size" onchange="document.getElementById('page').value=1;submit()">
			    <c:forTokens items="10,25,50,100,200,500" delims="," var="current">
			  	  <option value="${current}" <c:if test="${current == productListFilter.pageSize}">selected</c:if>>${current} <fmt:message key="perPage" /></option>
			    </c:forTokens>
			  </select>
			  </td>
			  </tr>
			</table>	  
		    <!-- end input field -->  	  
		  	</div>
		  	
		  	<!-- start button -->
			<!-- end button -->	
       	  	
	<!-- end tab -->        
	</div>
<!-- end tabs -->			
</div>
  
  <!-- end table -->
   </td><td class="boxmidr" ></td></tr>
  <tr><td class="botl"></td><td class="botr"></td></tr>
  </table>
  
<!-- end main box -->  
</div>
</form>
</sec:authorize>

  </tiles:putAttribute>
</tiles:insertDefinition>
