<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/security/tags" prefix="sec" %>

<link rel="stylesheet" type="text/css" media="all" href="../../admin/javascript/jscalendar-1.0/skins/aqua/theme.css" title="Aqua" /> <!-- calendar stylesheet -->
<script type="text/javascript" src="../../admin/javascript/jscalendar-1.0/calendar.js"></script> <!-- main calendar program -->
<script type="text/javascript" src="../../admin/javascript/jscalendar-1.0/lang/calendar-en.js"></script>
<script type="text/javascript" src="../../admin/javascript/jscalendar-1.0/calendar-setup.js"></script>
<!-- menu -->
<script type="text/javascript">
<!--
window.addEvent('domready', function(){
	var Tips1 = new Tips($$('.toolTipImg'));
	var opSearchBox = new multiBox('mbOperator', {showControls : false, useOverlay: false, showNumbers: false });
	var opNumOrderSearchBox = new multiBox('mbNumOrderOperator', {showControls : false, useOverlay: false, showNumbers: false });
	var optotalOrderSearchBox = new multiBox('mbTotalOrderOperator', {showControls : false, useOverlay: false, showNumbers: false });
	var opaverageOrderSearchBox = new multiBox('mbaverageOrderOperator', {showControls : false, useOverlay: false, showNumbers: false });
});
function selectOperator(e, type) {
	if(type == 'sku') {
		document.getElementById('skuOperator').value = e;
	} else if(type == 'numOrder') {
		document.getElementById('numOrderOperator').value = e;
	} else if(type == 'totalOrder') {
		document.getElementById('totalOrderOperator').value = e;
	} else if(type == 'averageOrder') {
		document.getElementById('averageOrderOperator').value = e;
	}
}
function UpdateStartEndDate2(type){
	var myDate = new Date();
	$('endDate').value = (myDate.getMonth()+1) + "/" + myDate.getDate() + "/" + myDate.getFullYear();
	if ( type == 2 ) {
		myDate.setDate(myDate.getDate() - 30);
		$('startDate').value = (myDate.getMonth()+1) + "/" + myDate.getDate() + "/" + myDate.getFullYear();
	} else if ( type == 3 ) {
		myDate.setDate(myDate.getDate() - 60);
		$('startDate').value = (myDate.getMonth()+1) + "/" + myDate.getDate() + "/" + myDate.getFullYear();
	}else if ( type == 4 ) {
		myDate.setDate(myDate.getDate() - 90);
		$('startDate').value = (myDate.getMonth()+1) + "/" + myDate.getDate() + "/" + myDate.getFullYear();
	}else if ( type == 5 ) {
		myDate.setDate(myDate.getDate() - 180);
		$('startDate').value = (myDate.getMonth()+1) + "/" + myDate.getDate() + "/" + myDate.getFullYear();
	}else if ( type == 10 ) {
		myDate.setYear(myDate.getFullYear() - 1);
		$('startDate').value = (myDate.getMonth()+1) + "/" + myDate.getDate() + "/" + myDate.getFullYear();
	}else if ( type == 20 ) {
		$('startDate').value = "";
		$('endDate').value = "";
	}else{
		$('startDate').value = (myDate.getMonth()+1) + "/" + myDate.getDate() + "/" + myDate.getFullYear();
	}
}
function UpdateCreatedStartEndDate2(type){
	var myDate = new Date();
	$('createdEndDate').value = (myDate.getMonth()+1) + "/" + myDate.getDate() + "/" + myDate.getFullYear();
	if ( type == 2 ) {
		myDate.setDate(myDate.getDate() - myDate.getDay());
		$('createdStartDate').value = (myDate.getMonth()+1) + "/" + myDate.getDate() + "/" + myDate.getFullYear();
	} else if ( type == 3 ) {
		myDate.setDate(1);
		$('createdStartDate').value = (myDate.getMonth()+1) + "/" + myDate.getDate() + "/" + myDate.getFullYear();
	}else if ( type == 10 ) {
		myDate.setMonth(0, 1);
		$('createdStartDate').value = (myDate.getMonth()+1) + "/" + myDate.getDate() + "/" + myDate.getFullYear();
	}else if ( type == 20 ) {
		$('createdStartDate').value = "";
		$('createdEndDate').value = "";
	}else{
		$('createdStartDate').value = (myDate.getMonth()+1) + "/" + myDate.getDate() + "/" + myDate.getFullYear();
	}
}
function UpdateStartEndDate(type){
var myDate = new Date();
$('endDate').value = (myDate.getMonth()+1) + "/" + myDate.getDate() + "/" + myDate.getFullYear();
	if ( type == 2 ) {
		myDate.setDate(myDate.getDate() - myDate.getDay());
		$('startDate').value = (myDate.getMonth()+1) + "/" + myDate.getDate() + "/" + myDate.getFullYear();
	} else if ( type == 3 ) {
		myDate.setDate(1);
		$('startDate').value = (myDate.getMonth()+1) + "/" + myDate.getDate() + "/" + myDate.getFullYear();
	}else if ( type == 10 ) {
		myDate.setMonth(0, 1);
		$('startDate').value = (myDate.getMonth()+1) + "/" + myDate.getDate() + "/" + myDate.getFullYear();
	}else if ( type == 20 ) {
		$('startDate').value = "";
		$('endDate').value = "";
	}else{
		$('startDate').value = (myDate.getMonth()+1) + "/" + myDate.getDate() + "/" + myDate.getFullYear();
	}
}

function submitSearchForm(fileType) {
	if (typeof document.forms['searchform'] != 'undefined') {
	  document.searchform.action = "customerExport.jhtm?fileType2="+fileType;
	  document.searchform.submit();
	} else {
	  myform=document.createElement("form");
	  document.body.appendChild(myform);
      myform.method = "POST";
      myform.action= "customerExport.jhtm?fileType2="+fileType;
      myform.submit();
	}
}
//-->
</script>
  <div id="lbox" class="quickMode">
   <table cellpadding="0" cellspacing="0" border="0" class="module" style="width: 100%"><tr><td class="topl">
    </td><td class="topr" ></td></tr><tr><td class="boxmid" >
    <c:if test="${gSiteConfig['gREPORT']}">
    <h2 class="menuleft mfirst"><fmt:message key="reports"/></h2>
    <div class="menudiv"></div>
    <sec:authorize ifAnyGranted="ROLE_AEM,ROLE_ADMIN_${prop_site_id},ROLE_REPORT_SHIPPED_ORDER,ROLE_REPORT_ORDER_NEED_ATTENTION,ROLE_REPORT_SHIPPED_OVERVIEW,ROLE_REPORT_ORDER_PEN_PROC,ROLE_REPORT_SHIPPED_DAILY,ROLE_REPORT_AFFILIATE_COMMISSION">
    <a href="orderReport.jhtm" class="userbutton"><fmt:message key="orders"/></a>
    </sec:authorize>
    <sec:authorize ifAnyGranted="ROLE_AEM,ROLE_ADMIN_${prop_site_id},ROLE_REPORT_ORDERS_DAILY">
    <a href="dailyOrderReport.jhtm" class="userbutton"><fmt:message key="ordersDaily"/></a>
    </sec:authorize>
    <sec:authorize ifAnyGranted="ROLE_AEM,ROLE_ADMIN_${prop_site_id},ROLE_REPORT_ORDER_OVERVIEW">
    <a href="orderDetailReport.jhtm" class="userbutton"><fmt:message key="ordersOverview"/></a>
    </sec:authorize>
    <sec:authorize ifAnyGranted="ROLE_AEM,ROLE_ADMIN_${prop_site_id},ROLE_REPORT_CUSTOMERS"> 
    <a href="customerReport.jhtm" class="userbutton"><fmt:message key="customersOverview"/></a>
    <a href="customerInactiveReport.jhtm" class="userbutton"><fmt:message key="customersInactive"/></a>
    </sec:authorize>
    <sec:authorize ifAnyGranted="ROLE_AEM,ROLE_ADMIN_${prop_site_id},ROLE_REPORT_SALESREP">   
    <a href="salesRepReport.jhtm" class="userbutton"><fmt:message key="salesRep"/></a>  
    </sec:authorize>
    <sec:authorize ifAnyGranted="ROLE_AEM,ROLE_ADMIN_${prop_site_id},ROLE_REPORT_SALESREP_DAILY">   
    <a href="salesRepReportDaily.jhtm" class="userbutton"><fmt:message key="salesRep"/><fmt:message key="daily"/></a>
    </sec:authorize>
    <sec:authorize ifAnyGranted="ROLE_AEM,ROLE_ADMIN_${prop_site_id},ROLE_REPORT_SALESREP_DAILY">   
    <a href="salesRepDetailReport.jhtm" class="userbutton"><fmt:message key="salesRepOverview"/></a>
    </sec:authorize>
     <sec:authorize ifAnyGranted="ROLE_AEM,ROLE_ADMIN_${prop_site_id},ROLE_REPORT_SALESREP_OVERVIEWII">   
    <a href="salesRepDetailReport2.jhtm" class="userbutton"><fmt:message key="salesRepOverview"/> II</a>
    </sec:authorize>
    <sec:authorize ifAnyGranted="ROLE_AEM,ROLE_ADMIN_${prop_site_id},ROLE_REPORT_CUSTOMER_METRIC">   
    <a href="metricOverviewReport.jhtm" class="userbutton"><fmt:message key="metricOverviewReport"/></a>
    </sec:authorize>
    <sec:authorize ifAnyGranted="ROLE_AEM,ROLE_ADMIN_${prop_site_id},ROLE_REPORT_TRACKCODE">   
    <a href="trackcodeReport.jhtm" class="userbutton"><fmt:message key="trackcode"/></a>    
    </sec:authorize>
    <c:if test="${gSiteConfig['gPROCESSING'] }" >
    <sec:authorize ifAnyGranted="ROLE_AEM,ROLE_ADMIN_${prop_site_id},ROLE_REPORT_PROCESSING">   
    <a href="truckLoadProcessReport.jhtm" class="userbutton"><fmt:message key="truckLoadProcess" /></a>    
    </sec:authorize>
    </c:if>
    <sec:authorize ifAnyGranted="ROLE_AEM,ROLE_ADMIN_${prop_site_id}">   
    <a href="promoCodeReport.jhtm" class="userbutton"><fmt:message key="promoCode"/></a>    
    </sec:authorize>
    <sec:authorize ifAnyGranted="ROLE_AEM,ROLE_ADMIN_${prop_site_id},ROLE_REPORT_IMP_EXP">   
    <a href="importExportHistory.jhtm" class="userbutton"><fmt:message key="importExport"/></a>
    </sec:authorize>
    <sec:authorize ifAnyGranted="ROLE_AEM,ROLE_ADMIN_${prop_site_id},ROLE_EMAILMANAGER_VIEW_HISTORY">   
    <a href="massEmailReport.jhtm" class="userbutton"><fmt:message key="massEmail"/></a>
    </sec:authorize>
    <sec:authorize ifAnyGranted="ROLE_AEM,ROLE_ADMIN_${prop_site_id},ROLE_REPORT_PRODUCT">   
    <a href="productListReport.jhtm" class="userbutton"><fmt:message key="productReport"/></a>
    </sec:authorize>
    <c:if test="${gSiteConfig['gINVENTORY'] and siteConfig['INVENTORY_HISTORY'].value == 'true'}">
	<sec:authorize ifAnyGranted="ROLE_AEM,ROLE_ADMIN_${prop_site_id},ROLE_REPORT_INVENTORY">   
    <a href="inventoryActivityReport.jhtm" class="userbutton"><fmt:message key="inventoryActivity"/></a>    
    <a href="inventoryReport.jhtm" class="userbutton"><fmt:message key="inventorySummary"/></a>    
    </sec:authorize>
    </c:if>
    <sec:authorize ifAnyGranted="ROLE_AEM,ROLE_ADMIN_${prop_site_id},ROLE_REPORT_LAY"> 
    <a href="layReport.jhtm" class="userbutton"><fmt:message key="layReport"/></a>
    </sec:authorize>
    <sec:authorize ifAnyGranted="ROLE_AEM,ROLE_ADMIN_${prop_site_id},ROLE_REPORT_ABANDONED_SHOPPING_CART">
    <a href="abandonedShoppingCartController.jhtm" class="userbutton"><fmt:message key="abandonedShoppingCart"/></a>
    </sec:authorize>
    <sec:authorize ifAnyGranted="ROLE_AEM,ROLE_ADMIN_${prop_site_id},ROLE_REPORT_EVENT_MEMBER_LIST"> 
    <a href="eventMemberListReport.jhtm" class="userbutton"><fmt:message key="eventMemberList"/></a>
    </sec:authorize>
    <div class="menudiv"></div>
    
    <sec:authorize ifAnyGranted="ROLE_AEM,ROLE_ADMIN_${prop_site_id},ROLE_REPORT_IMP_EXP"> 
    <c:if test="${param.tab == 'importExportReport'}"> 
    <div class="menudiv"></div>
      <div align="left" class="searchHeader" id="searchHeaderId"><h2><fmt:message key="search" /></h2></div>
	  <table class="searchBoxLeft" width="100%"><tr><td>
	  <form action="importExportHistory.jhtm" method="post">
      <!-- content area -->
      <div class="search2">
        <p><fmt:message key="type" />:</p>
	    <select name="_type">
	       <option value="" <c:if test="${importExportHistorySearch.imported == ''}">selected</c:if>><fmt:message key="all"/></option> 
	      <option value="product" <c:if test="${importExportHistorySearch.type == 'product'}">selected</c:if>><fmt:message key="product"/></option>
	      <option value="customer" <c:if test="${importExportHistorySearch.type == 'customer'}">selected</c:if>><fmt:message key="customer"/></option> 
	      <option value="invoice" <c:if test="${importExportHistorySearch.type == 'invoice'}">selected</c:if>><fmt:message key="invoice"/></option>
	      <option value="prodsupp" <c:if test="${importExportHistorySearch.type == 'prodsupp'}">selected</c:if>><fmt:message key="product'sSupplier"/></option> 
	      <option value="inventory" <c:if test="${importExportHistorySearch.type == 'inventory'}">selected</c:if>><fmt:message key="inventory"/></option>
	      <option value="crmcontact" <c:if test="${importExportHistorySearch.type == 'crmcontact'}">selected</c:if>><fmt:message key="crmContact"/></option>  
	    </select>
      </div>
      <div class="search2">
        <p><fmt:message key="import" /> / <fmt:message key="export" />:</p>
	    <select name="_import">
	      <option value="1" <c:if test="${importExportHistorySearch.imported == '1'}">selected</c:if>><fmt:message key="import"/></option>
	      <option value="0" <c:if test="${importExportHistorySearch.imported == '0'}">selected</c:if>><fmt:message key="export"/></option> 
	      <option value="" <c:if test="${importExportHistorySearch.imported == ''}">selected</c:if>><fmt:message key="all"/></option> 
	    </select>
      </div>         
      <div class="search2">
	      <p><fmt:message key="username"/>:</p>
	      <input name="_username" type="text" value="<c:out value='${importExportHistorySearch.username}' />" size="15" />
	    </div>  
      <div class="button">
	      <input type="submit" value="Search"/>
      </div>
      <!-- content area -->
	  </form>
	  </td></tr></table>
	<div class="menudiv"></div>
	</c:if>
	</sec:authorize>
	
	<sec:authorize ifAnyGranted="ROLE_AEM,ROLE_ADMIN_${prop_site_id},ROLE_REPORT_ORDER_OVERVIEW"> 
	<c:if test="${param.tab == 'orderDetailReport'}"> 
    <div class="menudiv"></div>
      <div align="left" class="searchHeader" id="searchHeaderId"><h2><fmt:message key="search" /></h2></div>
	  <table class="searchBoxLeft" width="100%"><tr><td>
	  <form action="orderDetailReport.jhtm" method="post">
      <!-- content area -->
      <div class="search2">
	      <p><fmt:message key="year"/>:</p>
	      <select name="year">
	       <c:forEach items="${model.years}" var="year"	varStatus="status">
	         <option <c:if test="${orderDetailFilter.year == year}">selected</c:if> value="${year}">${year}</option>
	       </c:forEach>
	      </select>
	  </div>
	  <c:if test="${gSiteConfig['gADD_INVOICE']}">
	    <div class="search2">
	      <p><fmt:message key="generatedFrom" />:</p>
	      <select name="backendOrder">
	  	    <option value=""><fmt:message key="allOrders" /></option>
	  	    <option value="1" <c:if test="${orderDetailFilter.backendOrder == '1'}">selected</c:if>><fmt:message key="backend" /></option>
	  	    <option value="0" <c:if test="${orderDetailFilter.backendOrder == '0'}">selected</c:if>><fmt:message key="frontend" /></option>
	  	  </select>
	    </div>
	  </c:if> 
	  <c:if test="${gSiteConfig['gSALES_REP']}">
	    <div class="search2">
	      <p><fmt:message key="salesRep" />:</p>
	      <select name="salesRepId">
		   <c:if test="${model.salesRepTree == null}">
	       <option value="-1"></option>
	       <option value="-2" <c:if test="${orderDetailFilter.salesRepId == -2}">selected</c:if>>No Sales Rep</option>
	       <c:forEach items="${model.salesReps}" var="salesRep">
	  	     <option value="${salesRep.id}" <c:if test="${orderDetailFilter.salesRepId == salesRep.id}">selected</c:if>><c:out value="${salesRep.name}" /></option>
	       </c:forEach>
	       </c:if>
	       <c:if test="${model.salesRepTree != null}">
	         <c:set var="salesRepTree" value="${model.salesRepTree}" scope="request"/>
	         <c:set var="orderDetailFilterSalesRepId" value="${orderDetailFilter.salesRepId}"/>
	         <option value="${salesRepTree.id}" <c:if test="${orderDetailFilter.salesRepId == salesRepTree.id}">selected</c:if>><c:out value="${salesRepTree.name}" /></option>	         
			 <c:set var="recursionPageId" value="${orderDetailFilterSalesRepId}" scope="request"/>
	      	 <c:set var="level" value="1" scope="request"/>
			 <jsp:include page="/WEB-INF/jsp/admin/report/recursion.jsp"/>	       
	       </c:if>
	      </select>
	    </div>  
	  </c:if>
	  <div class="search2">
	      <p><fmt:message key="dateType" />:</p>
	      <select name="dateType">
			<option value="orderDate">Order Date</option>
			<option value="shipDate" <c:if test="${orderDetailFilter.dateType == 'shipDate'}">selected</c:if>>Ship Date</option>
		  </select>
	  </div>
	  <div class="search2">
	      <p><fmt:message key="orderType" />:</p>
	      <select name="orderType">
	       <option value=""><fmt:message key="allOrders" /></option>
	       <c:forTokens items="${siteConfig['ORDER_TYPE'].value}" delims="," var="type" varStatus="status">
			  <option value ="${type}" <c:if test="${type == orderDetailFilter.orderType}">selected</c:if>><c:out value ="${type}" /></option>
		   </c:forTokens>
	      </select>
	  </div>
	  <div class="search2">
	      <p><fmt:message key="shippingTitle" />:</p>
	      <input name="shippingMethod" type="text" value="<c:out value='${orderDetailFilter.shippingMethod}' />" size="15" />
	  </div> 
	  <div class="search2">
	      <p><c:set var="flag" value="false" />
	      <select class="extraFieldSearch" name="productFieldNumber">
	  	    <c:forEach items="${model.productFieldList}" var="productField">
	  	     <c:if test="${!empty productField.name}" ><c:set var="flag" value="true" />
	  	     <option style="font-size:normal;" value="${productField.id}" <c:if test="${orderDetailFilter.productFieldNumber == productField.id}">selected</c:if>><c:out value="${productField.name}" />:</option>
	  	     </c:if>
	  	    </c:forEach>
	      </select>
	      </p>
	      <c:if test="${flag}" >
	       <input name="productField" type="text" value="<c:out value='${orderDetailFilter.productField}' />" size="11" />
	      </c:if>
	    </div>
	    <div class="search2">
	      <p><c:set var="customerFieldFlag" value="false" />
	      <select class="extraFieldSearch" name="customerFieldNumber">
	  	    <c:forEach items="${model.customerFieldList}" var="customerField">
	  	     <c:if test="${!empty customerField.name}" ><c:set var="customerFieldFlag" value="true" />
	  	     <option style="font-size:normal;" value="${customerField.id}" <c:if test="${orderDetailFilter.customerFieldNumber == customerField.id}">selected</c:if>><c:out value="${customerField.name}" />:</option>
	  	     </c:if>
	  	    </c:forEach>
	      </select>
	      </p>
	      <c:if test="${customerFieldFlag}" >
	       <input name="customerField" type="text" value="<c:out value='${orderDetailFilter.customerField}' />" size="11" />
	      </c:if>
	  </div>  
      <div class="button">
	      <input type="submit" value="Search"/>
      </div>
      <!-- content area -->
	  </form>
	  </td></tr></table>
	<div class="menudiv"></div>
	</c:if>
	<c:if test="${param.tab == 'ordersDaily'}"> 
    <div class="menudiv"></div>
      <div align="left" class="searchHeader" id="searchHeaderId"><h2><fmt:message key="search" /></h2></div>
	  <table class="searchBoxLeft" width="100%"><tr><td>
	  <form action="dailyOrderReport.jhtm" method="post">
      <!-- content area -->
      <div class="search2">
	      <table class="searchBoxLeftAS" style="border: 0px;"><tr><td>
			<div class="advancedSearch">
		      <p>start:</p>
		      <input style="width:90px;margin-right:5px;" name="startDate" id="startDate" type="text" value="<fmt:formatDate type="date" pattern="MM/dd/yyyy" value="${shipDailyOrderReportFilter.startDate}"/>" readonly size="11" />
			  <img class="calendarImage"  id="startDate_trigger" src="../graphics/calendarIcon.jpg" />
			  <script type="text/javascript">
	    		Calendar.setup({
	        			inputField     :    "startDate",   // id of the input field
	        			showsTime      :    false,
	        			ifFormat       :    "%m/%d/%Y",   // format of the input field
	        			button         :    "startDate_trigger"   // trigger for the calendar (button ID)
	    		});	
			  </script>
		    </div>	
		    <div style="clear:both;"></div>    
		    <div class="advancedSearch">
		      <p>end:</p>
		      <input style="width:90px;margin-right:5px;" name="endDate" id="endDate" type="text" value="<fmt:formatDate type="date" pattern="MM/dd/yyyy" value='${shipDailyOrderReportFilter.endDate}'/>" readonly size="11" />
			  <img class="calendarImage"  id="endDate_trigger" src="../graphics/calendarIcon.jpg" />
			  <script type="text/javascript">
	    		Calendar.setup({
	        			inputField     :    "endDate",   // id of the input field
	        			showsTime      :    false,
	        			ifFormat       :    "%m/%d/%Y",   // format of the input field
	        			button         :    "endDate_trigger"   // trigger for the calendar (button ID)
	    		});	
			  </script>
			</div>  
			<div style="clear:both;"></div>    
		    <div class="advancedSearch">
		      <table border="0">
		       <tr> <td class="advancedSearchDateInterval"><input style="width:12px;margin-right:5px;border: 0px" name="intervaldate" id="today" type="radio" onclick="UpdateStartEndDate(1);" />Today</td> </tr>
		       <tr> <td class="advancedSearchDateInterval"><input style="width:12px;margin-right:5px;border: 0px" name="intervaldate" id="weekToDate" type="radio" onclick="UpdateStartEndDate(2);" />Week To Date</td> </tr>
		       <tr> <td class="advancedSearchDateInterval"><input style="width:12px;margin-right:5px;border: 0px" name="intervaldate" id="monthToDate" type="radio" onclick="UpdateStartEndDate(3);" />Month To Date</td> </tr>
		       <tr> <td class="advancedSearchDateInterval"><input style="width:12px;margin-right:5px;border: 0px" name="intervaldate" id="yearToDate" type="radio" onclick="UpdateStartEndDate(10);" />Year To Date</td> </tr>
		       <tr> <td class="advancedSearchDateInterval"><input style="width:12px;margin-right:5px;border: 0px" name="intervaldate" id="reset" type="radio" onclick="UpdateStartEndDate(20);" />Reset</td> </tr>
		      </table>
		    </div>   
		</td></tr></table>
	  </div>
	  <div class="search2">
	      <p><fmt:message key="dateType" />:</p>
	      <select name="dateType">
			<option value="orderDate">Order Date</option>
			<option value="shipDate" <c:if test="${shipDailyOrderReportFilter.dateType == 'shipDate'}">selected</c:if>>Ship Date</option>
		  </select>
	  </div> 
      <div class="button">
	      <input type="submit" value="Search"/>
      </div>
      <!-- content area -->
	  </form>
	  </td></tr></table>
	<div class="menudiv"></div>
	</c:if>
	</sec:authorize>
	
	<sec:authorize ifAnyGranted="ROLE_AEM,ROLE_ADMIN_${prop_site_id},ROLE_REPORT_CUSTOMERS"> 
	<c:if test="${param.tab == 'customersInactive'}"> 
	<script type="text/javascript" src="../javascript/side-bar.js"></script>
    <div class="menudiv"></div> 
      <div align="left" class="searchHeader" id="searchHeaderId"><h2><fmt:message key="search" /></h2></div>
	  <form action="customerInactiveReport.jhtm" method="post" name="list_inactive_search" id="list_inactive_search">
	  <table class="searchBoxLeft" width="100%"><tr><td valign="top" style="width: 110px;">
	  <!-- content area -->
	    <c:if test="${gSiteConfig['gSALES_REP']}">
	    <div class="search2">
	      <p><fmt:message key="salesRep" />:</p>
	      <select name="salesRepId">
		   <c:if test="${model.salesRepTree == null}">
	       <option value="-1"><fmt:message key="allOrders" /></option>
	       <option value="-2" <c:if test="${inActiveCustomerFilter.salesRepId == -2}">selected</c:if>><fmt:message key="noSalesRep" /></option>
	       <c:forEach items="${model.salesReps}" var="salesRep">
	  	     <option value="${salesRep.id}" <c:if test="${inActiveCustomerFilter.salesRepId == salesRep.id}">selected</c:if>><c:out value="${salesRep.name}" /></option>
	       </c:forEach>
	       </c:if>
	       <c:if test="${model.salesRepTree != null}">
	         <c:set var="salesRepTree" value="${model.salesRepTree}" scope="request"/>
	         <c:set var="inActiveCustomerFilterSalesRepId" value="${inActiveCustomerFilter.salesRepId}"/>
	         <option value="${salesRepTree.id}" <c:if test="${inActiveCustomerFilter.salesRepId == salesRepTree.id}">selected</c:if>><c:out value="${salesRepTree.name}" /></option>
			 <c:set var="recursionPageId" value="${inActiveCustomerFilterSalesRepId}" scope="request"/>
	      	 <c:set var="level" value="1" scope="request"/>
			 <jsp:include page="/WEB-INF/jsp/admin/report/recursion.jsp"/>		       
	       </c:if>
	      </select>
	    </div>  
	    </c:if>
	    <c:if test="${gSiteConfig['gGROUP_CUSTOMER']}">
	    <div class="search2">
	      <p><fmt:message key="group" />:</p>
	      <select name="groupId">
	         <option value=""></option>
	      <c:forEach items="${model.groupList}" var="group">
	  	     <option value="${group.id}" <c:if test="${inActiveCustomerFilter.groupId == group.id}">selected</c:if>><c:out value="${group.name}" /></option>
	       </c:forEach>
	      </select>
	    </div>
	    </c:if> 
	    <div class="search2">
	      <p><fmt:message key="firstName"/>:</p>
	      <input name="firstname" type="text" value="<c:out value='${inActiveCustomerFilter.firstName}' />" size="15" />
	    </div>
	    <div class="search2">
	      <p><fmt:message key="lastName"/>:</p>
	      <input name="lastname" type="text" value="<c:out value='${inActiveCustomerFilter.lastName}' />" size="15" />
	    </div>
	    <div class="search2">
	      <p><fmt:message key="rating" /> 1:</p>
	      <select name="rating1">
	         <option value=""></option>
	         <c:forTokens items="${siteConfig['CUSTOMER_RATING_1'].value}" delims="," var="type">
				<option value ="${type}" <c:if test="${inActiveCustomerFilter.rating1 == type}" >selected</c:if>><c:out value ="${type}" /><option>
		  	</c:forTokens>
	      </select>
	    </div>
	    <div class="search2">
	      <p><fmt:message key="rating" /> 2:</p>
	      <select name="rating2">
	      	<option value=""></option>
	        <c:forTokens items="${siteConfig['CUSTOMER_RATING_2'].value}" delims="," var="type">
				<option value ="${type}" <c:if test="${inActiveCustomerFilter.rating2 == type}" >selected</c:if>><c:out value ="${type}" /><option>
		  	</c:forTokens>
	      </select>
	    </div>
	    <div class="search2">
	      <p><fmt:message key="email"/>:</p>
	      <input name="email" type="text" value="<c:out value='${inActiveCustomerFilter.username}' />" size="15" />
	    </div> 
	    <div class="search2">
	      <p><fmt:message key="country"/>:</p>
	      <input name="country" type="text" value="<c:out value='${inActiveCustomerFilter.country}' />" size="15" />
	    </div> 
	    <div class="search2">
	      <p><fmt:message key="state"/>:</p>
	      <input name="state" type="text" value="<c:out value='${inActiveCustomerFilter.state}' />" size="15" />
	    </div> 
	    <div class="search2">
	      <p><fmt:message key="phone"/>:</p>
	      <input name="phone" type="text" value="<c:out value='${inActiveCustomerFilter.phone}' />" size="15" />
	    </div> 
	    <div class="search2">
	      <p><fmt:message key="orders"/>:   <a href="#operatorNumOrder" rel="type:element" id="mbNumOrderOperator1" class="mbNumOrderOperator" style="margin-left: 55px;"><img class="toolTipImg" title="Advanced Search::Click to see more Options" src="../graphics/question.gif" /></a></p>
		  <div id="operatorNumOrder" class="miniWindowWrapper searchOp displayNone">
	    	<div class="header">Advanced Search</div>
	    	  <fieldset class="">
	    	    <label class="AStitle"><fmt:message key="orders"/></label>
	    	    <input name="numOrder" type="text" value="<c:out value='${inActiveCustomerFilter.numOrder}' />" size="15" disabled />
	    	  </fieldset>
	    	  <fieldset class="bottom">
	    	    <label class="AStitle"><fmt:message key="operator"/></label>
	    	    <div class="options">
	    	       <label class="radio">
	    	       		<input type="radio" <c:if test="${inActiveCustomerFilter.numOrderOperator == 0}" >checked="checked"</c:if> value="0" name="numOrderOp" onchange="selectOperator(this.value, 'numOrder');" /> <strong> Greater Than (&gt;)</strong>
	    	       </label>
	    	       <label class="radio">
	    	       		<input type="radio" <c:if test="${inActiveCustomerFilter.numOrderOperator == 1}" >checked="checked"</c:if> value="1" name="numOrderOp" onchange="selectOperator(this.value, 'numOrder');" /> <strong> Equal (=) </strong>
	    	       </label>
	    	       <label class="radio">
	    	       		<input type="radio" <c:if test="${inActiveCustomerFilter.numOrderOperator == 2}" >checked="checked"</c:if> value="2" name="numOrderOp" onchange="selectOperator(this.value, 'numOrder');" /> <strong> Less Than (&lt;) </strong>
	    	       </label>
	    	    </div>
	    	  </fieldset>
	    	  <fieldset>
	    	     <div class="button">
	      			<input type="submit" value="Search" onclick="document.list_inactive_search.submit();"/>
      			 </div>
	    	  </fieldset>
		  </div>
		  <input name="numOrder" type="text" value="<c:out value='${inActiveCustomerFilter.numOrder}' />" size="15" />
		  <input type="hidden" name="numOrderOperator" id="numOrderOperator"/>
	    </div>
	  <div class="search2">
	      <p><fmt:message key="orderTotal"/>:   <a href="#operatorTotalOrder" rel="type:element" id="mbTotalOrderOperator1" class="mbTotalOrderOperator" style="margin-left: 30px;"><img class="toolTipImg" title="Advanced Search::Click to see more Options" src="../graphics/question.gif" /></a></p>
		  <div id="operatorTotalOrder" class="miniWindowWrapper searchOp displayNone">
	    	<div class="header">Advanced Search</div>
	    	  <fieldset class="">
	    	    <label class="AStitle"><fmt:message key="orderTotal"/></label>
	    	    <input name="totalOrder" type="text" value="<c:out value='${inActiveCustomerFilter.totalOrder}' />" size="15" disabled />
	    	  </fieldset>
	    	  <fieldset class="bottom">
	    	    <label class="AStitle"><fmt:message key="operator"/></label>
	    	    <div class="options">
	    	       <label class="radio">
	    	       		<input type="radio" <c:if test="${inActiveCustomerFilter.totalOrderOperator == 0}" >checked="checked"</c:if> value="0" name="totalOrderOp" onchange="selectOperator(this.value, 'totalOrder');" /> <strong> Greater Than (&gt;)</strong>
	    	       </label>
	    	       <label class="radio">
	    	       		<input type="radio" <c:if test="${inActiveCustomerFilter.totalOrderOperator == 1}" >checked="checked"</c:if> value="1" name="totalOrderOp" onchange="selectOperator(this.value, 'totalOrder');" /> <strong>Equal (=)</strong>
	    	       </label>
	    	       <label class="radio">
	    	       		<input type="radio" <c:if test="${inActiveCustomerFilter.totalOrderOperator == 2}" >checked="checked"</c:if> value="2" name="totalOrderOp" onchange="selectOperator(this.value, 'totalOrder');" /> <strong>Less Than (&lt;) </strong>
	    	       </label>
	    	    </div>
	    	  </fieldset>
	    	  <fieldset>
	    	     <div class="button">
	      			<input type="submit" value="Search" onclick="document.list_inactive_search.submit();"/>
      			 </div>
	    	  </fieldset>
		  </div>
		  <input name="totalOrder" type="text" value="<c:out value='${inActiveCustomerFilter.totalOrder}' />" size="15" />
		  <input type="hidden" name="totalOrderOperator" id="totalOrderOperator"/>
	    </div>	    
	      <div class="search2">
	      <p><fmt:message key="averageOrder"/>:   <a href="#operatoraverageOrder" rel="type:element" id="mbaverageOrderOperator" class="mbaverageOrderOperator" style="margin-left: 10px;"><img class="toolTipImg" title="Advanced Search::Click to see more Options" src="../graphics/question.gif" /></a></p>
		   <div id="operatoraverageOrder" class="miniWindowWrapper searchOp displayNone">
	    	<div class="header">Advanced Search</div>
	    	 <fieldset class="">
	    	    <label class="AStitle"><fmt:message key="averageOrder"/></label>
	    	    <input name="averageOrder" type="text" value="<c:out value='${inActiveCustomerFilter.averageOrder}' />" size="15" disabled />
	    	  </fieldset>
	    	<fieldset class="bottom">
	    	    <label class="AStitle"><fmt:message key="operator"/></label>
	    	    <div class="options">
	    	       <label class="radio">
	    	       		<input type="radio" <c:if test="${inActiveCustomerFilter.averageOrderOperator == 0}" >checked="checked"</c:if> value="0" name="averageOrderOp" onchange="selectOperator(this.value, 'averageOrder');" /> <strong> Greater Than (&gt;)</strong>
	    	       </label>
	    	       <label class="radio">
	    	       		<input type="radio" <c:if test="${inActiveCustomerFilter.averageOrderOperator == 1}" >checked="checked"</c:if> value="1" name="averageOrderOp" onchange="selectOperator(this.value, 'averageOrder');" /> <strong>Equal (=)</strong>
	    	       </label>
	    	       <label class="radio">
	    	       		<input type="radio" <c:if test="${inActiveCustomerFilter.averageOrderOperator == 2}" >checked="checked"</c:if> value="2" name="averageOrderOp" onchange="selectOperator(this.value, 'averageOrder');" /> <strong>Less Than (&lt;) </strong>
	    	       </label>
	    	    </div>
	    	  </fieldset>
	    	  <fieldset>
	    	     <div class="button">
	      			<input type="submit" value="Search" onclick="document.list_inactive_search.submit();"/>
      			 </div>
	    	  </fieldset>
		  </div>  
		  <input name="averageOrder" type="text" value="<c:out value='${inActiveCustomerFilter.averageOrder}' />" size="15" />
		  <input type="hidden" name="averageOrderOperator" id="averageOrderOperator"/>
	    </div>	    
	   <div class="search2">
	      <p><c:set var="customerFieldFlag" value="false" />
	      <select class="extraFieldSearch" name="customerFieldNumber">
	  	    <c:forEach items="${model.customerFieldList}" var="customerField">
	  	     <c:if test="${!empty customerField.name}" ><c:set var="customerFieldFlag" value="true" />
	  	     <option style="font-size:normal;" value="${customerField.id}" <c:if test="${inActiveCustomerFilter.customerFieldNumber == customerField.id}">selected</c:if>><c:out value="${customerField.name}" />:</option>
	  	     </c:if>
	  	    </c:forEach>
	      </select>
	      </p>
	      <c:if test="${customerFieldFlag}" >
	       <input name="customerField" type="text" value="<c:out value='${inActiveCustomerFilter.customerField}' />" size="11" />
	      </c:if>
	    </div>   
	    <div class="button">
	      <input type="submit" value="Search"/>
		</div>
	  <!-- content area -->
	  </td>
	  <td valign="top">
	  <div id="sideBar">
	
	  <a href="#" id="sideBarTab"><img src="../graphics/tabSliderLeft.png" alt="sideBar" title="sideBar" /></a>
	  <div id="sideBarContents" style="width:0px;">
	  <table cellpadding="0" cellspacing="0" border="0" class="module" >
      <tr>
      <td class="topl_g"><div align="left" class="searchHeaderAdvanced"><h2><fmt:message key="advancedSearch" /></h2></div></td>
      <td class="topr_g" ></td></tr>
      <tr><td class="boxmidlrg" >
  
		<table class="searchBoxLeftAS"><tr><td>
		<div id="sideBarContentsInner">
			<div class="advancedSearch">
		      <p>start:</p>
		      <input style="width:90px;margin-right:5px;" name="startDate" id="startDate" type="text" value="<fmt:formatDate type="date" pattern="MM/dd/yyyy" value='${inActiveCustomerFilter.startDate}'/>"  size="11" />
			  <img class="calendarImage"  id="startDate_trigger" src="../graphics/calendarIcon.jpg" alt="" />
			  <script type="text/javascript">
			   Calendar.setup({
					inputField     :    "startDate",   // id of the input field
					showsTime      :    false,
					ifFormat       :    "%m/%d/%Y",   // format of the input field
					button         :    "startDate_trigger"   // trigger for the calendar (button ID)
			   });	
			  </script>
			</div>  
		    <div style="clear:both;"></div>    
		    <div class="advancedSearch">
		      <p>end:</p>
		      <input style="width:90px;margin-right:5px;" name="endDate" id="endDate" type="text" value="<fmt:formatDate type="date" pattern="MM/dd/yyyy" value='${inActiveCustomerFilter.endDate}'/>"  size="11" />
			  <img class="calendarImage"  id="endDate_trigger" src="../graphics/calendarIcon.jpg" alt="" />
			  <script type="text/javascript">
			   Calendar.setup({
					inputField     :    "endDate",   // id of the input field
					showsTime      :    false,
					ifFormat       :    "%m/%d/%Y",   // format of the input field
					button         :    "endDate_trigger"   // trigger for the calendar (button ID)
			   });	
			  </script>
			</div>  
			<div style="clear:both;"></div>    
		    <div class="advancedSearch">
		      <table border="0">
		       <tr> <td class="advancedSearchDateInterval"><input style="width:12px;margin-right:5px;border: 0px" name="intervaldate" id="month" type="radio" value="30" onclick="UpdateStartEndDate2(2);" />30 days</td> </tr>
		       <tr> <td class="advancedSearchDateInterval"><input style="width:12px;margin-right:5px;border: 0px" name="intervaldate" id="month" type="radio" value="60" onclick="UpdateStartEndDate2(3);" />60 days</td> </tr>
		       <tr> <td class="advancedSearchDateInterval"><input style="width:12px;margin-right:5px;border: 0px" name="intervaldate" id="month" type="radio" value="90" onclick="UpdateStartEndDate2(4);" />90 days</td> </tr>
		       <tr> <td class="advancedSearchDateInterval"><input style="width:12px;margin-right:5px;border: 0px" name="intervaldate" id="month" type="radio" value="180" onclick="UpdateStartEndDate2(5);" />180 days</td> </tr>
		       <tr> <td class="advancedSearchDateInterval"><input style="width:12px;margin-right:5px;border: 0px" name="intervaldate" id="month" type="radio" value="365" onclick="UpdateStartEndDate2(10);" />1 year</td> </tr>
		       <tr> <td class="advancedSearchDateInterval"><input style="width:12px;margin-right:5px;border: 0px" name="intervaldate" id="reset" type="radio" value="-1" onclick="UpdateStartEndDate2(20);" />Reset</td> </tr>
		      </table>  
		    </div>
		</div>
		</td></tr></table>
		
		<h4 style="margin-left:11px;">Date Created</h4>
	  	<table class="searchBoxLeftAS"><tr><td>
		<div id="sideBarContentsInner">
			<div class="advancedSearch">
		      <p>start:</p>
		      <input style="width:90px;margin-right:5px;" name="createdStartDate" id="createdStartDate" type="text" value="<fmt:formatDate type="date" pattern="MM/dd/yyyy" value='${inActiveCustomerFilter.createdStartDate}'/>"  size="11" />
			  <img class="calendarImage"  id="createdStartDate_trigger" src="../graphics/calendarIcon.jpg" alt="" />
			  <script type="text/javascript">
			   Calendar.setup({
					inputField     :    "createdStartDate",   // id of the input field
					showsTime      :    false,
					ifFormat       :    "%m/%d/%Y",   // format of the input field
					button         :    "createdStartDate_trigger"   // trigger for the calendar (button ID)
			   });	
			  </script>
			</div>  
		    <div style="clear:both;"></div>    
		    <div class="advancedSearch">
		      <p>end:</p>
		      <input style="width:90px;margin-right:5px;" name="createdEndDate" id="createdEndDate" type="text" value="<fmt:formatDate type="date" pattern="MM/dd/yyyy" value='${inActiveCustomerFilter.createdEndDate}'/>"  size="11" />
			  <img class="calendarImage"  id="createdEndDate_trigger" src="../graphics/calendarIcon.jpg" alt="" />
			  <script type="text/javascript">
			   Calendar.setup({
					inputField     :    "createdEndDate",   // id of the input field
					showsTime      :    false,
					ifFormat       :    "%m/%d/%Y",   // format of the input field
					button         :    "createdEndDate_trigger"   // trigger for the calendar (button ID)
			   });	
			  </script>
			</div>  
			<div style="clear:both;"></div>    
		    <div class="advancedSearch">
		      <table border="0">
		       <tr> <td class="advancedSearchDateInterval"><input style="width:12px;margin-right:5px;border: 0px" name="intervaldate" id="month" type="radio" onclick="UpdateCreatedStartEndDate2(1);" />Today</td> </tr>
		       <tr> <td class="advancedSearchDateInterval"><input style="width:12px;margin-right:5px;border: 0px" name="intervaldate" id="month" type="radio" onclick="UpdateCreatedStartEndDate2(2);" />Week To Date</td> </tr>
		       <tr> <td class="advancedSearchDateInterval"><input style="width:12px;margin-right:5px;border: 0px" name="intervaldate" id="month" type="radio" onclick="UpdateCreatedStartEndDate2(3);" />Month To Date</td> </tr>
		       <tr> <td class="advancedSearchDateInterval"><input style="width:12px;margin-right:5px;border: 0px" name="intervaldate" id="month" type="radio" onclick="UpdateCreatedStartEndDate2(10);" />Year To Date</td> </tr>
		       <tr> <td class="advancedSearchDateInterval"><input style="width:12px;margin-right:5px;border: 0px" name="intervaldate" id="reset" type="radio" onclick="UpdateCreatedStartEndDate2(20);" />Reset</td> </tr>
		      </table>  
		    </div>
		</div>
		</td></tr></table>
		
	  </td><td class="boxmidr" ></td></tr>
      <tr><td class="botl"></td><td class="botr"></td></tr>
      </table></div>
    
      </div>
      
	  </td>
	  </tr></table>
	  </form>
	<div class="menudiv"></div>    
	</c:if>
	
    <c:if test="${param.tab == 'customersReport'}"> 
	<script type="text/javascript" src="../javascript/side-bar.js"></script>
    <div class="menudiv"></div> 
      <div align="left" class="searchHeader" id="searchHeaderId"><h2><fmt:message key="search" /></h2></div>
	  <form action="customerReport.jhtm" method="post">
	  <table class="searchBoxLeft" width="100%"><tr><td valign="top" style="width: 110px;">
	  <!-- content area -->
	    <c:if test="${gSiteConfig['gADD_INVOICE']}">
	    <div class="search2">
	      <p><fmt:message key="generatedFrom" />:</p>
	      <select name="backendOrder">
	  	    <option value=""><fmt:message key="allOrders" /></option>
	  	    <option value="1" <c:if test="${customerFilter.backendOrder == '1'}">selected</c:if>><fmt:message key="backend" /></option>
	  	    <option value="0" <c:if test="${customerFilter.backendOrder == '0'}">selected</c:if>><fmt:message key="frontend" /></option>
	  	  </select>
	    </div>
	    </c:if>
	    <c:if test="${gSiteConfig['gSALES_REP']}">
	    <div class="search2">
	      <p><fmt:message key="salesRep" />:</p>
	      <select name="salesRepId">
		   <c:if test="${model.salesRepTree == null}">
	       <option value="-1"></option>
	       <option value="-2" <c:if test="${customerFilter.salesRepId == -2}">selected</c:if>><fmt:message key="noSalesRep" /></option>
	       <c:forEach items="${model.salesReps}" var="salesRep">
	  	     <option value="${salesRep.id}" <c:if test="${customerFilter.salesRepId == salesRep.id}">selected</c:if>><c:out value="${salesRep.name}" /></option>
	       </c:forEach>
	       </c:if>
	       <c:if test="${model.salesRepTree != null}">
	         <c:set var="salesRepTree" value="${model.salesRepTree}" scope="request"/>
	         <c:set var="customerFilterSalesRepId" value="${customerFilter.salesRepId}"/>
	         <option value="${salesRepTree.id}" <c:if test="${customerFilter.salesRepId == salesRepTree.id}">selected</c:if>><c:out value="${salesRepTree.name}" /></option>
			 <c:set var="recursionPageId" value="${customerFilterSalesRepId}" scope="request"/>
	      	 <c:set var="level" value="1" scope="request"/>
			 <jsp:include page="/WEB-INF/jsp/admin/report/recursion.jsp"/>	       
	       </c:if>
	      </select>
	    </div>  
	    </c:if>
	    <c:if test="${gSiteConfig['gGROUP_CUSTOMER']}">
	    <div class="search2">
	      <p><fmt:message key="group" />:</p>
	      <select name="groupId">
	         <option value=""></option>
	      <c:forEach items="${model.groupList}" var="group">
	  	     <option value="${group.id}" <c:if test="${customerFilter.groupId == group.id}">selected</c:if>><c:out value="${group.name}" /></option>
	       </c:forEach>
	      </select>
	    </div>
	    </c:if> 
	    <div class="search2">
	      <p><fmt:message key="orderType" />:</p>
	      <select name="orderType">
	       <option value=""><fmt:message key="allOrders" /></option>
	       <c:forTokens items="${siteConfig['ORDER_TYPE'].value}" delims="," var="type" varStatus="status">
			  <option value ="${type}" <c:if test="${type == customerFilter.orderType}">selected</c:if>><c:out value ="${type}" /></option>
		   </c:forTokens>
	      </select>
	    </div>
	    <div class="search2">
	      <p><fmt:message key="firstName"/>:</p>
	      <input name="firstname" type="text" value="<c:out value='${customerFilter.firstName}' />" size="15" />
	    </div>
	    <div class="search2">
	      <p><fmt:message key="lastName"/>:</p>
	      <input name="lastname" type="text" value="<c:out value='${customerFilter.lastName}' />" size="15" />
	    </div>
	    <div class="search2">
	      <p><fmt:message key="shippingTitle" />:</p>
	      <input name="shippingMethod" type="text" value="<c:out value='${customerFilter.shippingMethod}' />" size="15" />
	    </div>
	    <div class="search2">
	      <div style="width:115px;"><div style="width:100px;float:left;"><p><fmt:message key="subTotal" />:</p></div><div style="float:right;"><img class="toolTipImg" title="SubTotal::SubTotal >= value" src="../graphics/question.gif" /></div></div>
	      <input name="totalOrder" type="text" value="<c:out value='${customerFilter.totalOrder}' />" size="15" />
	    </div>  
	    <div class="search2">
	      <p><c:set var="flag" value="false" />
	      <select class="extraFieldSearch" name="productFieldNumber">
	  	    <c:forEach items="${model.productFieldList}" var="productField">
	  	     <c:if test="${!empty productField.name}" ><c:set var="flag" value="true" />
	  	     <option style="font-size:normal;" value="${productField.id}" <c:if test="${customerFilter.productFieldNumber == productField.id}">selected</c:if>><c:out value="${productField.name}" />:</option>
	  	     </c:if>
	  	    </c:forEach>
	      </select>
	      </p>
	      <c:if test="${flag}" >
	       <input name="productField" type="text" value="<c:out value='${customerFilter.productField}' />" size="11" />
	      </c:if>
	    </div>
	    <div class="search2">
	      <p><c:set var="customerFieldFlag" value="false" />
	      <select class="extraFieldSearch" name="customerFieldNumber">
	  	    <c:forEach items="${model.customerFieldList}" var="customerField">
	  	     <c:if test="${!empty customerField.name}" ><c:set var="customerFieldFlag" value="true" />
	  	     <option style="font-size:normal;" value="${customerField.id}" <c:if test="${customerFilter.customerFieldNumber == customerField.id}">selected</c:if>><c:out value="${customerField.name}" />:</option>
	  	     </c:if>
	  	    </c:forEach>
	      </select>
	      </p>
	      <c:if test="${customerFieldFlag}" >
	       <input name="customerField" type="text" value="<c:out value='${customerFilter.customerField}' />" size="11" />
	      </c:if>
	    </div>
	    <div class="button">
	      <input type="submit" value="Search"/>
		</div>
	  <!-- content area -->
	  </td>
	  <td valign="top">
	  <div id="sideBar">
	
	  <a href="#" id="sideBarTab"><img src="../graphics/tabSliderLeft.png" alt="sideBar" title="sideBar" /></a>
	  <div id="sideBarContents" style="width:0px;">
	  <table cellpadding="0" cellspacing="0" border="0" class="module" >
      <tr>
      <td class="topl_g"><div align="left" class="searchHeaderAdvanced"><h2><fmt:message key="advancedSearch" /></h2></div></td>
      <td class="topr_g" ></td></tr>
      <tr><td class="boxmidlrg" >
  
		<table class="searchBoxLeftAS"><tr><td>
		<div id="sideBarContentsInner">
			<div class="advancedSearch">
		      <p>start:</p>
		      <input style="width:90px;margin-right:5px;" name="startDate" id="startDate" type="text" value="<fmt:formatDate type="date" pattern="MM/dd/yyyy" value='${customerFilter.startDate}'/>"  size="11" />
			  <img class="calendarImage"  id="startDate_trigger" src="../graphics/calendarIcon.jpg" alt="" />
			  <script type="text/javascript">
			   Calendar.setup({
					inputField     :    "startDate",   // id of the input field
					showsTime      :    false,
					ifFormat       :    "%m/%d/%Y",   // format of the input field
					button         :    "startDate_trigger"   // trigger for the calendar (button ID)
			   });	
			  </script>
			</div>  
		    <div style="clear:both;"></div>    
		    <div class="advancedSearch">
		      <p>end:</p>
		      <input style="width:90px;margin-right:5px;" name="endDate" id="endDate" type="text" value="<fmt:formatDate type="date" pattern="MM/dd/yyyy" value='${customerFilter.endDate}'/>"  size="11" />
			  <img class="calendarImage"  id="endDate_trigger" src="../graphics/calendarIcon.jpg" alt="" />
			  <script type="text/javascript">
			   Calendar.setup({
					inputField     :    "endDate",   // id of the input field
					showsTime      :    false,
					ifFormat       :    "%m/%d/%Y",   // format of the input field
					button         :    "endDate_trigger"   // trigger for the calendar (button ID)
			   });	
			  </script>
			</div>  
			<div style="clear:both;"></div>    
		    <div class="advancedSearch">
		      <table border="0">
		       <tr> <td class="advancedSearchDateInterval"><input style="width:12px;margin-right:5px;border: 0px" name="intervaldate" id="today" type="radio" onclick="UpdateStartEndDate(1);" />Today</td> </tr>
		       <tr> <td class="advancedSearchDateInterval"><input style="width:12px;margin-right:5px;border: 0px" name="intervaldate" id="weekToDate" type="radio" onclick="UpdateStartEndDate(2);" />Week To Date</td> </tr>
		       <tr> <td class="advancedSearchDateInterval"><input style="width:12px;margin-right:5px;border: 0px" name="intervaldate" id="monthToDate" type="radio" onclick="UpdateStartEndDate(3);" />Month To Date</td> </tr>
		       <tr> <td class="advancedSearchDateInterval"><input style="width:12px;margin-right:5px;border: 0px" name="intervaldate" id="yearToDate" type="radio" onclick="UpdateStartEndDate(10);" />Year To Date</td> </tr>
		       <tr> <td class="advancedSearchDateInterval"><input style="width:12px;margin-right:5px;border: 0px" name="intervaldate" id="reset" type="radio" onclick="UpdateStartEndDate(20);" />Reset</td> </tr>
		      </table>  
		    </div>
		</div>
		</td></tr></table>
	  
	  </td><td class="boxmidr" ></td></tr>
      <tr><td class="botl"></td><td class="botr"></td></tr>
      </table></div>
    
      </div>
      
	  </td>
	  </tr></table>
	  </form>
	<div class="menudiv"></div>    
	</c:if>
	</sec:authorize>
	
	<sec:authorize ifAnyGranted="ROLE_AEM,ROLE_ADMIN_${prop_site_id},ROLE_REPORT_SALESREP"> 
	<c:if test="${param.tab == 'salesRepsReport'}"> 
    <div class="menudiv"></div>
      <div align="left" class="searchHeader" id="searchHeaderId"><h2><fmt:message key="search" /></h2></div>
	  <table class="searchBoxLeft" width="100%"><tr><td>
	  <form action="salesRepReport.jhtm" method="post">
      <!-- content area -->
      <div class="search2">
	      <p><fmt:message key="year"/>:</p>
	      <select name="year">
	       <c:forEach items="${model.yearList}" var="year"	varStatus="status">
	         <option <c:if test="${salesRepFilter.year == year}">selected</c:if> value="${year}">${year}</option>
	       </c:forEach>
	      </select>
	  </div>    
	  <c:if test="${gSiteConfig['gSALES_REP']}">
	    <div class="search2">
	      <p><fmt:message key="salesRep" />:</p>
	      <select name="salesRepId">
		   <c:if test="${model.salesRepTree == null}">
	       <option value="-1"></option>
	       <c:forEach items="${model.salesReps}" var="salesRep">
	  	     <option value="${salesRep.id}" <c:if test="${salesRepFilter.salesRepId == salesRep.id}">selected</c:if>><c:out value="${salesRep.name}" /></option>
	       </c:forEach>
	       </c:if>
	       <c:if test="${model.salesRepTree != null}">
	         <c:set var="salesRepTree" value="${model.salesRepTree}" scope="request"/>
	         <c:set var="salesRepFilterSalesRepId" value="${salesRepFilter.salesRepId}"/>
	         <option value="${salesRepTree.id}" <c:if test="${salesRepFilter.salesRepId == salesRepTree.id}">selected</c:if>><c:out value="${salesRepTree.name}" /></option>
			 <c:set var="recursionPageId" value="${salesRepFilterSalesRepId}" scope="request"/>
	      	 <c:set var="level" value="1" scope="request"/>
			 <jsp:include page="/WEB-INF/jsp/admin/report/recursion.jsp"/>	       
	       </c:if>
	      </select>
	    </div>  
	  </c:if>
      <div class="search2">
	      <p><fmt:message key="quarter"/>:</p>
	      <select name="quarter">
	       <option value="1"><fmt:message key="quarter" /> 1</option>
	       <option value="2" <c:if test="${salesRepFilter.quarter == '2'}">selected</c:if>><fmt:message key="quarter" /> 2</option>
	       <option value="3" <c:if test="${salesRepFilter.quarter == '3'}">selected</c:if>><fmt:message key="quarter" /> 3</option>
	       <option value="4" <c:if test="${salesRepFilter.quarter == '4'}">selected</c:if>><fmt:message key="quarter" /> 4</option>
	       <option value="0" <c:if test="${salesRepFilter.quarter == '0'}">selected</c:if>>yearly</option>
	      </select>
	  </div>
	  <div class="search2">
	      <p><fmt:message key="dateType" />:</p>
	      <select name="dateType">
			<option value="orderDate">Order Date</option>
			<option value="shipDate" <c:if test="${salesRepFilter.dateType == 'shipDate'}">selected</c:if>>Ship Date</option>
		  </select>
	  </div>
      <div class="button">
	      <input type="submit" value="Search"/>
      </div>
      <!-- content area -->
	  </form>
	  </td></tr></table>
	<div class="menudiv"></div>
	</c:if>
	</sec:authorize>
	
	<sec:authorize ifAnyGranted="ROLE_AEM,ROLE_ADMIN_${prop_site_id},ROLE_REPORT_SALESREP_DAILY"> 
	<c:if test="${param.tab == 'salesRepsDailyReport'}"> 
    <div class="menudiv"></div>
      <div align="left" class="searchHeader" id="searchHeaderId"><h2><fmt:message key="search" /></h2></div>
	  <table class="searchBoxLeft" width="100%"><tr><td>
	  <form action="salesRepReportDaily.jhtm" method="post">
      <!-- content area -->
      <div class="search2">
	      <p><fmt:message key="year"/>:</p>
	      <select name="year">
	       <c:forEach items="${model.yearList}" var="year"	varStatus="status">
	         <option <c:if test="${salesRepDailyFilter.year == year}">selected</c:if> value="${year}">${year}</option>
	       </c:forEach>
	      </select>
	  </div>
	  <div class="search2">
	      <p><fmt:message key="dateType" />:</p>
	      <select name="dateType">
			<option value="orderDate">Order Date</option>
			<option value="shipDate" <c:if test="${salesRepDailyFilter.dateType == 'shipDate'}">selected</c:if>>Ship Date</option>
		  </select>
	  </div>    
      <c:if test="${gSiteConfig['gSALES_REP']}">
	    <div class="search2">
	      <p><fmt:message key="salesRep" />:</p>
	      <select name="salesRepId">
		   <c:if test="${model.salesRepTree == null}">
	       <option value="-1"></option>
	       <c:forEach items="${model.salesReps}" var="salesRep">
	  	     <option value="${salesRep.id}" <c:if test="${salesRepDailyFilter.salesRepId == salesRep.id}">selected</c:if>><c:out value="${salesRep.name}" /></option>
	       </c:forEach>
	       </c:if>
	       <c:if test="${model.salesRepTree != null}">
	         <c:set var="salesRepTree" value="${model.salesRepTree}" scope="request"/>
	         <c:set var="salesRepDailyFilterSalesRepId" value="${salesRepDailyFilter.salesRepId}"/>
	         <option value="${salesRepTree.id}" <c:if test="${salesRepDailyFilter.salesRepId == salesRepTree.id}">selected</c:if>><c:out value="${salesRepTree.name}" /></option>
			 <c:set var="recursionPageId" value="${salesRepDailyFilterSalesRepId}" scope="request"/>
	      	 <c:set var="level" value="1" scope="request"/>
			 <jsp:include page="/WEB-INF/jsp/admin/report/recursion.jsp"/>	       
	       </c:if>
	      </select>
	    </div>  
	  </c:if>
      <div class="button">
	      <input type="submit" value="Search"/>
      </div>
      <!-- content area -->
	  </form>
	  </td></tr></table>
	<div class="menudiv"></div>
	</c:if>
	</sec:authorize>
	
	<sec:authorize ifAnyGranted="ROLE_AEM,ROLE_ADMIN_${prop_site_id},ROLE_REPORT_PRODUCT"> 
	<c:if test="${param.tab == 'productList'}">
	<script type="text/javascript" src="../javascript/side-bar.js"></script>
	<div class="menudiv"></div> 
      <div align="left" class="searchHeader" id="searchHeaderId"><h2><fmt:message key="search" /></h2></div>
	  <form action="productListReport.jhtm" method="post" name="list_product_search">
	  <table class=searchBoxLeft><tr><td valign="top" style="width: 110px;">
	  <!-- content area -->
	    <c:if test="${gSiteConfig['gADD_INVOICE']}">
	    <div class="search2">
	      <p><fmt:message key="generatedFrom" />:</p>
	      <select name="backendOrder">
	  	    <option value=""><fmt:message key="allOrders" /></option>
	  	    <option value="1" <c:if test="${productListFilter.backendOrder == '1'}">selected</c:if>><fmt:message key="backend" /></option>
	  	    <option value="0" <c:if test="${productListFilter.backendOrder == '0'}">selected</c:if>><fmt:message key="frontend" /></option>
	  	  </select>
	    </div>
	    </c:if>
	    <div class="search2">
	      <p><fmt:message key="status" />:</p>
	      <select name="status">
	  	    <option value=""><fmt:message key="allOrders" /></option>
	  	    <option value="p" <c:if test="${productListFilter.status == 'p'}">selected</c:if>><fmt:message key="orderStatus_p" /></option>
	  	    <option value="pr" <c:if test="${productListFilter.status == 'pr'}">selected</c:if>><fmt:message key="orderStatus_pr" /></option>
	  	     <option value="wp" <c:if test="${productListFilter.status == 'wp'}">selected</c:if>><fmt:message key="orderStatus_wp" /></option>
	  	     <option value="rls" <c:if test="${productListFilter.status == 'rls'}">selected</c:if>><fmt:message key="orderStatus_rls" /></option>
	  	    <%-- <option value="ppr" <c:if test="${productListFilter.status == 'ppr'}">selected</c:if>><fmt:message key="orderStatus_ppr" /></option> --%>
	  	    <option value="s" <c:if test="${productListFilter.status == 's'}">selected</c:if>><fmt:message key="orderStatus_s" /></option>
	  	    <option value="x" <c:if test="${productListFilter.status == 'x'}">selected</c:if>><fmt:message key="orderStatus_x" /></option>
			<c:if test="${gSiteConfig['gPAYPAL'] > 0}">	  	    
	  	    <option value="xp" <c:if test="${productListFilter.status == 'xp'}">selected</c:if>><fmt:message key="orderStatus_xp" /></option>
	  	    </c:if>
	  	    <c:if test="${gSiteConfig['gCREDIT_CARD_PAYMENT'] == 'netcommerce'}">
	  	    <option value="xnc" <c:if test="${productListFilter.status == 'xnc'}">selected</c:if>><fmt:message key="orderStatus_xnc" /></option>
	  	    </c:if>
	  	    <c:if test="${siteConfig['GOOGLE_CHECKOUT_URL'].value != ''}">
	  	    <option value="xg" <c:if test="${productListFilter.status == 'xg'}">selected</c:if>><fmt:message key="orderStatus_xg" /></option>
			</c:if>
			<option value="pprs" <c:if test="${productListFilter.status == 'pprs'}">selected</c:if>><fmt:message key="orderStatus_pprs" /></option>
			<option value="nxs" <c:if test="${productListFilter.status == 'nxs'}">selected</c:if>><fmt:message key="orderStatus_not_x_not_s" /></option>
	      </select>
	    </div>  
        <c:if test="${gSiteConfig['gSALES_REP']}">
	    <div class="search2">
	      <p><fmt:message key="salesRep" />:</p>
	      <select name="salesRepId">
		   <c:if test="${model.salesRepTree == null}">
	       <option value="-1"></option>
	       <option value="-2" <c:if test="${productListFilter.salesRepId == -2}">selected</c:if>><fmt:message key="noSalesRep" /></option>
	       <c:forEach items="${model.salesReps}" var="salesRep">
	  	     <option value="${salesRep.id}" <c:if test="${productListFilter.salesRepId == salesRep.id}">selected</c:if>><c:out value="${salesRep.name}" /></option>
	       </c:forEach>
	       </c:if>
	       <c:if test="${model.salesRepTree != null}">
	         <c:set var="salesRepTree" value="${model.salesRepTree}" scope="request"/>
	         <c:set var="productListFilterSalesRepId" value="${productListFilter.salesRepId}"/>
	         <option value="${salesRepTree.id}" <c:if test="${productListFilter.salesRepId == salesRepTree.id}">selected</c:if>><c:out value="${salesRepTree.name}" /></option>
			 <c:set var="recursionPageId" value="${productListFilterSalesRepId}" scope="request"/>
	      	 <c:set var="level" value="1" scope="request"/>
			 <jsp:include page="/WEB-INF/jsp/admin/report/recursion.jsp"/>	       
	       </c:if>
		  </select>
		</div>
		</c:if>
	    <div class="search2">
	      <p><fmt:message key="orderType" />:</p>
	      <select name="orderType">
	       <option value=""><fmt:message key="allOrders" /></option>
	       <c:forTokens items="${siteConfig['ORDER_TYPE'].value}" delims="," var="type" varStatus="status">
			  <option value ="${type}" <c:if test="${type == productListFilter.orderType}">selected</c:if>><c:out value ="${type}" /></option>
		   </c:forTokens>
	      </select>
	    </div>  
	    <div class="search2">
	      <p><fmt:message key="email" />:</p>
	      <input name="username" type="text" value="<c:out value='${productListFilter.username}' />" size="15" />
	    </div>
	    <div class="search2">
	      <p><fmt:message key="billTo" />:</p>
	      <input name="billTo" type="text" value="<c:out value='${productListFilter.billToName}' />" size="15" />
	    </div>
	    <div class="search2">
	      <p><fmt:message key="shippingTitle" />:</p>
	      <input name="shippingMethod" type="text" value="<c:out value='${productListFilter.shippingMethod}' />" size="15" />
	    </div>
	    <div class="search2">
	      <p><fmt:message key="sku"/>:   <a href="#operatorSku" rel="type:element" id="mbOperator1" class="mbOperator" style="margin-left: 67px;"><img class="toolTipImg" title="Advanced Search::Click to see more Options" src="../graphics/question.gif" /></a></p>
		  <div id="operatorSku" class="miniWindowWrapper searchOp displayNone">
	    	<div class="header">Advanced Search</div>
	    	  <fieldset class="">
	    	    <label class="AStitle"><fmt:message key="sku"/></label>
	    	    <input name="sku" type="text" value="<c:out value='${productListFilter.sku}' />" size="15" disabled />
	    	  </fieldset>
	    	  <fieldset class="bottom">
	    	    <label class="AStitle"><fmt:message key="operator"/></label>
	    	    <div class="options">
	    	       <label class="radio">
	    	       		<input type="radio" <c:if test="${productListFilter.skuOperator == 0}" >checked="checked"</c:if> value="0" name="skuOp" onchange="selectOperator(this.value, 'sku');" /> <strong>Contains</strong>
	    	       </label>
	    	       <label class="radio">
	    	       		<input type="radio" <c:if test="${productListFilter.skuOperator == 1}" >checked="checked"</c:if> value="1" name="skuOp" onchange="selectOperator(this.value, 'sku');" /> <strong>Exact</strong>
	    	       </label>
	    	    </div>
	    	  </fieldset>
	    	  <fieldset>
	    	     <div class="button">
	      			<input type="submit" value="Search" onclick="document.list_product_search.submit();"/>
      			 </div>
	    	  </fieldset>
		  </div>
		   <input name="sku" type="text" value="<c:out value='${productListFilter.sku}' />" size="15" />
		  <div class="search2">
	      	<p><fmt:message key="parentSku" />:</p>
	      	<input name="parentSku" type="text" value="<c:out value='${productListFilter.parentSku}' />" size="15" />
	      </div> 
		  <input type="hidden" name="skuOperator" id="skuOperator"/>
		  <div class="search2">
	      	<p><fmt:message key="productName" />:</p>
	      	<input name="productName" type="text" value="<c:out value='${productListFilter.productName}' />" size="15" />
	      </div> 
	    </div>
	    <div class="search2">
	      <p><c:set var="flag" value="false" />
	      <select class="extraFieldSearch" name="productFieldNumber">
	  	    <c:forEach items="${model.productFieldList}" var="productField">
	  	     <c:if test="${!empty productField.name}" ><c:set var="flag" value="true" />
	  	     <option style="font-size:normal;" value="${productField.id}" <c:if test="${productListFilter.productFieldNumber == productField.id}">selected</c:if>><c:out value="${productField.name}" />:</option>
	  	     </c:if>
	  	    </c:forEach>
	      </select>
	      </p>
	      <c:if test="${flag}" >
	       <input name="productField" type="text" value="<c:out value='${productListFilter.productField}' />" size="11" />
	      </c:if>
	    </div> 
	    <div class="button">
	      <input type="submit" value="Search"/>
		</div>
	  <!-- content area -->
	  </td>
	  <td valign="top">
	  <div id="sideBar">
	
	  <a href="#" id="sideBarTab"><img src="../graphics/tabSliderLeft.png" alt="sideBar" title="sideBar" /></a>
	  <div id="sideBarContents" style="width:0px;">
	  <table cellpadding="0" cellspacing="0" border="0" class="module" >
      <tr>
      <td class="topl_g"><div align="left" class="searchHeaderAdvanced"><h2><fmt:message key="advancedSearch" /></h2></div></td>
      <td class="topr_g" ></td></tr>
      <tr><td class="boxmidlrg" >
  
		<table class="searchBoxLeftAS"><tr><td>
		<div id="sideBarContentsInner">
			<div class="advancedSearch">
			  <table border="0">
		       <tr> 
		         <td class="advancedSearchDateInterval"><input style="width:12px;margin-right:5px;border: 0px" name="dateType" type="radio" <c:if test="${productListFilter.dateType == 'orderDate'}" >checked</c:if> value="orderDate" />Order Date</td>
		         <td class="advancedSearchDateInterval"><input style="width:12px;margin-right:5px;border: 0px" name="dateType" type="radio" <c:if test="${productListFilter.dateType == 'shipDate'}" >checked</c:if> value="shipDate" />Ship Date</td>
		       </tr>
		      </table>
		      <p>start:</p>
		      <input style="width:90px;margin-right:5px;" name="startDate" id="startDate" type="text" value="<fmt:formatDate type="date" pattern="MM/dd/yyyy" value="${productListFilter.startDate}"/>" readonly size="11" />
			  <img class="calendarImage"  id="startDate_trigger" src="../graphics/calendarIcon.jpg" />
			  <script type="text/javascript">
	    		Calendar.setup({
	        			inputField     :    "startDate",   // id of the input field
	        			showsTime      :    false,
	        			ifFormat       :    "%m/%d/%Y",   // format of the input field
	        			button         :    "startDate_trigger"   // trigger for the calendar (button ID)
	    		});	
			  </script>
		    </div>
		    <div style="clear:both;"></div>    
		    <div class="advancedSearch">
		      <p>end:</p>
		      <input style="width:90px;margin-right:5px;" name="endDate" id="endDate" type="text" value="<fmt:formatDate type="date" pattern="MM/dd/yyyy" value='${productListFilter.endDate}'/>" readonly size="11" />
			  <img class="calendarImage"  id="endDate_trigger" src="../graphics/calendarIcon.jpg" />
			  <script type="text/javascript">
	    		Calendar.setup({
	        			inputField     :    "endDate",   // id of the input field
	        			showsTime      :    false,
	        			ifFormat       :    "%m/%d/%Y",   // format of the input field
	        			button         :    "endDate_trigger"   // trigger for the calendar (button ID)
	    		});	
			  </script>
			</div>  
			<div style="clear:both;"></div>    
		    <div class="advancedSearch">
		      <table border="0">
		       <tr> <td class="advancedSearchDateInterval"><input style="width:12px;margin-right:5px;border: 0px" name="intervaldate" id="today" type="radio" onclick="UpdateStartEndDate(1);" />Today</td> </tr>
		       <tr> <td class="advancedSearchDateInterval"><input style="width:12px;margin-right:5px;border: 0px" name="intervaldate" id="weekToDate" type="radio" onclick="UpdateStartEndDate(2);" />Week To Date</td> </tr>
		       <tr> <td class="advancedSearchDateInterval"><input style="width:12px;margin-right:5px;border: 0px" name="intervaldate" id="monthToDate" type="radio" onclick="UpdateStartEndDate(3);" />Month To Date</td> </tr>
		       <tr> <td class="advancedSearchDateInterval"><input style="width:12px;margin-right:5px;border: 0px" name="intervaldate" id="yearToDate" type="radio" onclick="UpdateStartEndDate(10);" />Year To Date</td> </tr>
		       <tr> <td class="advancedSearchDateInterval"><input style="width:12px;margin-right:5px;border: 0px" name="intervaldate" id="reset" type="radio" onclick="UpdateStartEndDate(20);" />Reset</td> </tr>
		      </table>  
		    </div>
		</div>    
		</td></tr></table>
	  
	  </td><td class="boxmidr" ></td></tr>
      <tr><td class="botl"></td><td class="botr"></td></tr>
      </table></div>
    
      </div>
      
	  </td>
	  </tr></table>
	  </form>
	<div class="menudiv"></div>	
	</c:if>
	</sec:authorize>

	
    <sec:authorize ifAnyGranted="ROLE_AEM,ROLE_ADMIN_${prop_site_id},ROLE_REPORT_SALESREP_DAILY"> 
	<c:if test="${param.tab == 'salesRepsDetailReport'}"> 
	<script type="text/javascript" src="../javascript/side-bar.js"></script>
    <div class="menudiv"></div> 
      <div align="left" class="searchHeader" id="searchHeaderId"><h2><fmt:message key="search" /></h2></div>
	  <form action="salesRepDetailReport.jhtm" method="post">
	  <table class="searchBoxLeft" width="100%"><tr><td valign="top" style="width:110px;">
	  <!-- content area -->
        <c:if test="${gSiteConfig['gADD_INVOICE']}">
	    <div class="search2">
	      <p><fmt:message key="generatedFrom" />:</p>
	      <select name="backendOrder">
	  	    <option value=""><fmt:message key="allOrders" /></option>
	  	    <option value="1" <c:if test="${salesRepDetailFilter.backendOrder == '1'}">selected</c:if>><fmt:message key="backend" /></option>
	  	    <option value="0" <c:if test="${salesRepDetailFilter.backendOrder == '0'}">selected</c:if>><fmt:message key="frontend" /></option>
	  	  </select>
	    </div>
	    </c:if>
	    <div class="search2">
	      <p><fmt:message key="salesRep" />:</p>
	      <select name="salesRepId">
		   <c:if test="${model.salesRepTree == null}">
	       <option value="-1"></option>
	       <c:forEach items="${model.salesReps}" var="salesRep">
	  	     <option value="${salesRep.id}" <c:if test="${salesRepDetailFilter.salesRepId == salesRep.id}">selected</c:if>><c:out value="${salesRep.name}" /></option>
	       </c:forEach>
	       </c:if>
	       <c:if test="${model.salesRepTree != null}">
	         <c:set var="salesRepTree" value="${model.salesRepTree}" scope="request"/>
	         <c:set var="salesRepDetailFilterSalesRepId" value="${salesRepDetailFilter.salesRepId}"/>
	         <option value="${salesRepTree.id}" <c:if test="${salesRepDetailFilter.salesRepId == salesRepTree.id}">selected</c:if>><c:out value="${salesRepTree.name}" /></option>
			 <c:set var="recursionPageId" value="${salesRepDetailFilterSalesRepId}" scope="request"/>
	      	 <c:set var="level" value="1" scope="request"/>
			 <jsp:include page="/WEB-INF/jsp/admin/report/recursion.jsp"/>	       
	       </c:if>
	      </select>
	    </div> 
	    <div class="search2">
	      <p><fmt:message key="orderType" />:</p>
	      <select name="orderType">
	       <option value=""><fmt:message key="allOrders" /></option>
	       <c:forTokens items="${siteConfig['ORDER_TYPE'].value}" delims="," var="type" varStatus="status">
			  <option value ="${type}" <c:if test="${type == salesRepDetailFilter.orderType}">selected</c:if>><c:out value ="${type}" /></option>
		   </c:forTokens>
	      </select>
	    </div>
	    <div class="search2">
	      <p><fmt:message key="shippingTitle" />:</p>
	      <input name="shippingMethod" type="text" value="<c:out value='${salesRepDetailFilter.shippingMethod}' />" size="15" />
	    </div>  
	    <div class="search2">
	      <p><c:set var="flag" value="false" />
	      <select class="extraFieldSearch" name="productFieldNumber">
	  	    <c:forEach items="${model.productFieldList}" var="productField">
	  	     <c:if test="${!empty productField.name}" ><c:set var="flag" value="true" />
	  	     <option style="font-size:normal;" value="${productField.id}" <c:if test="${salesRepDetailFilter.productFieldNumber == productField.id}">selected</c:if>><c:out value="${productField.name}" />:</option>
	  	     </c:if>
	  	    </c:forEach>
	      </select>
	      </p>
	      <c:if test="${flag}" >
	       <input name="productField" type="text" value="<c:out value='${salesRepDetailFilter.productField}' />" size="11" />
	      </c:if>
	    </div>
	    <div class="search2">
	      <p><c:set var="customerFieldFlag" value="false" />
	      <select class="extraFieldSearch" name="customerFieldNumber">
	  	    <c:forEach items="${model.customerFieldList}" var="customerField">
	  	     <c:if test="${!empty customerField.name}" ><c:set var="customerFieldFlag" value="true" />
	  	     <option style="font-size:normal;" value="${customerField.id}" <c:if test="${salesRepDetailFilter.customerFieldNumber == customerField.id}">selected</c:if>><c:out value="${customerField.name}" />:</option>
	  	     </c:if>
	  	    </c:forEach>
	      </select>
	      </p>
	      <c:if test="${customerFieldFlag}" >
	       <input name="customerField" type="text" value="<c:out value='${salesRepDetailFilter.customerField}' />" size="11" />
	      </c:if>
	    </div>   
	    <div class="button">
	      <input type="submit" value="Search"/>
		</div>
	  <!-- content area -->
	  </td>
	  <td valign="top">
	  <div id="sideBar">
	
	  <a href="#" id="sideBarTab"><img src="../graphics/tabSliderLeft.png" alt="sideBar" title="sideBar" /></a>
	  <div id="sideBarContents" style="width:0px;">
	  <table cellpadding="0" cellspacing="0" border="0" class="module" >
      <tr>
      <td class="topl_g"><div align="left" class="searchHeaderAdvanced"><h2><fmt:message key="advancedSearch" /></h2></div></td>
      <td class="topr_g" ></td></tr>
      <tr><td class="boxmidlrg" >
  
		<table class="searchBoxLeftAS"><tr><td>
		<div id="sideBarContentsInner">
			<div class="advancedSearch">
			  <table border="0">
		       <tr> 
		         <td class="advancedSearchDateInterval"><input style="width:12px;margin-right:5px;border: 0px" name="dateType" type="radio" <c:if test="${salesRepDetailFilter.dateType == 'orderDate'}" >checked</c:if> value="orderDate" />Order Date</td>
		         <td class="advancedSearchDateInterval"><input style="width:12px;margin-right:5px;border: 0px" name="dateType" type="radio" <c:if test="${salesRepDetailFilter.dateType == 'shipDate'}" >checked</c:if> value="shipDate" />Ship Date</td>
		       </tr>
		      </table>
		      <p>start:</p>
		      <input style="width:90px;margin-right:5px;" name="startDate" id="startDate" type="text" value="<fmt:formatDate type="date" pattern="MM/dd/yyyy" value='${salesRepDetailFilter.startDate}'/>"  size="11" />
			  <img class="calendarImage"  id="startDate_trigger" src="../graphics/calendarIcon.jpg" alt="" />
			  <script type="text/javascript">
			   Calendar.setup({
					inputField     :    "startDate",   // id of the input field
					showsTime      :    false,
					ifFormat       :    "%m/%d/%Y",   // format of the input field
					button         :    "startDate_trigger"   // trigger for the calendar (button ID)
			   });	
			  </script>
			</div>  
		    <div style="clear:both;"></div>    
		    <div class="advancedSearch">
		      <p>end:</p>
		      <input style="width:90px;margin-right:5px;" name="endDate" id="endDate" type="text" value="<fmt:formatDate type="date" pattern="MM/dd/yyyy" value='${salesRepDetailFilter.endDate}'/>"  size="11" />
			  <img class="calendarImage"  id="endDate_trigger" src="../graphics/calendarIcon.jpg" alt="" />
			  <script type="text/javascript">
			   Calendar.setup({
					inputField     :    "endDate",   // id of the input field
					showsTime      :    false,
					ifFormat       :    "%m/%d/%Y",   // format of the input field
					button         :    "endDate_trigger"   // trigger for the calendar (button ID)
			   });	
			  </script>
			</div>  
			<div style="clear:both;"></div>    
		    <div class="advancedSearch">
		      <table border="0">
		       <tr> <td class="advancedSearchDateInterval"><input style="width:12px;margin-right:5px;border: 0px" name="intervaldate" id="today" type="radio" onclick="UpdateStartEndDate(1);" />Today</td> </tr>
		       <tr> <td class="advancedSearchDateInterval"><input style="width:12px;margin-right:5px;border: 0px" name="intervaldate" id="weekToDate" type="radio" onclick="UpdateStartEndDate(2);" />Week To Date</td> </tr>
		       <tr> <td class="advancedSearchDateInterval"><input style="width:12px;margin-right:5px;border: 0px" name="intervaldate" id="monthToDate" type="radio" onclick="UpdateStartEndDate(3);" />Month To Date</td> </tr>
		       <tr> <td class="advancedSearchDateInterval"><input style="width:12px;margin-right:5px;border: 0px" name="intervaldate" id="yearToDate" type="radio" onclick="UpdateStartEndDate(10);" />Year To Date</td> </tr>
		       <tr> <td class="advancedSearchDateInterval"><input style="width:12px;margin-right:5px;border: 0px" name="intervaldate" id="reset" type="radio" onclick="UpdateStartEndDate(20);" />Reset</td> </tr>
		      </table>  
		    </div>
		</div>
		</td></tr></table>
	  
	  </td><td class="boxmidr" ></td></tr>
      <tr><td class="botl"></td><td class="botr"></td></tr>
      </table></div>
    
      </div>
      
	  </td>
	  </tr></table>
	  </form>
	<div class="menudiv"></div>    
	</c:if>
	</sec:authorize> 	
	
	 <sec:authorize ifAnyGranted="ROLE_AEM,ROLE_ADMIN_${prop_site_id},ROLE_REPORT_SALESREP_OVERVIEWII"> 
	<c:if test="${param.tab == 'salesRepsDetailReport2'}">
	<script type="text/javascript" src="../javascript/side-bar.js"></script>
    <div class="menudiv"></div> 
      <div align="left" class="searchHeader" id="searchHeaderId"><h2><fmt:message key="search" /></h2></div>
	  <form action="salesRepDetailReport2.jhtm" method="post">
	  <table class="searchBoxLeft" width="100%"><tr><td valign="top" style="width:110px;">
	  <!-- content area -->
        <c:if test="${gSiteConfig['gADD_INVOICE']}">
	    <div class="search2">
	      <p><fmt:message key="generatedFrom" />:</p>
	      <select name="backendOrder">
	  	    <option value=""><fmt:message key="allOrders" /></option>
	  	    <option value="1" <c:if test="${salesRepDetailFilter2.backendOrder == '1'}">selected</c:if>><fmt:message key="backend" /></option>
	  	    <option value="0" <c:if test="${salesRepDetailFilter2.backendOrder == '0'}">selected</c:if>><fmt:message key="frontend" /></option>
	  	  </select>
	    </div>
	    </c:if>
	    <div class="search2">
	      <p><fmt:message key="salesRep" />:</p>
	      <select name="salesRepId">
		   <c:if test="${model.salesRepTree == null}">
	       <option value="-1"></option>
	       <c:forEach items="${model.salesReps}" var="salesRep">
	  	     <option value="${salesRep.id}" <c:if test="${salesRepDetailFilter2.salesRepId == salesRep.id}">selected</c:if>><c:out value="${salesRep.name}" /></option>
	       </c:forEach>
	       </c:if>
	       <c:if test="${model.salesRepTree != null}">
	         <c:set var="salesRepTree" value="${model.salesRepTree}" scope="request"/>
	         <c:set var="salesRepDetailFilterSalesRepId" value="${salesRepDetailFilter2.salesRepId}"/>
	         <option value="${salesRepTree.id}" <c:if test="${salesRepDetailFilter2.salesRepId == salesRepTree.id}">selected</c:if>><c:out value="${salesRepTree.name}" /></option>
			 <c:set var="recursionPageId" value="${salesRepDetailFilterSalesRepId}" scope="request"/>
	      	 <c:set var="level" value="1" scope="request"/>
			 <jsp:include page="/WEB-INF/jsp/admin/report/recursion.jsp"/>	       
	       </c:if>
	      </select>
	    </div> 
	    <div class="search2">
	      <p><fmt:message key="orderType" />:</p>
	      <select name="orderType">
	       <option value=""><fmt:message key="allOrders" /></option>
	       <c:forTokens items="${siteConfig['ORDER_TYPE'].value}" delims="," var="type" varStatus="status">
			  <option value ="${type}" <c:if test="${type == salesRepDetailFilter2.orderType}">selected</c:if>><c:out value ="${type}" /></option>
		   </c:forTokens>
	      </select>
	    </div>
	    <div class="search2">
	      <p><fmt:message key="shippingTitle" />:</p>
	      <input name="shippingMethod" type="text" value="<c:out value='${salesRepDetailFilter2.shippingMethod}' />" size="15" />
	    </div>  	    
	     <div class="button">
	      <input type="submit" value="Search"/>
		</div>
	  <!-- content area -->
	  </td>
	  <td valign="top">
	  <div id="sideBar">
	
	  <a href="#" id="sideBarTab"><img src="../graphics/tabSliderLeft.png" alt="sideBar" title="sideBar" /></a>
	  <div id="sideBarContents" style="width:0px;">
	  <table cellpadding="0" cellspacing="0" border="0" class="module" >
      <tr>
      <td class="topl_g"><div align="left" class="searchHeaderAdvanced"><h2><fmt:message key="advancedSearch" /></h2></div></td>
      <td class="topr_g" ></td></tr>
      <tr><td class="boxmidlrg" >
  
		<table class="searchBoxLeftAS"><tr><td>
		<div id="sideBarContentsInner">
			<div class="advancedSearch">
			  <table border="0">
		       <tr> 
		         <td class="advancedSearchDateInterval"><input style="width:12px;margin-right:5px;border: 0px" name="dateType" type="radio" <c:if test="${salesRepDetailFilter2.dateType == 'orderDate'}" >checked</c:if> value="orderDate" />Order Date</td>
		         <td class="advancedSearchDateInterval"><input style="width:12px;margin-right:5px;border: 0px" name="dateType" type="radio" <c:if test="${salesRepDetailFilter2.dateType == 'shipDate'}" >checked</c:if> value="shipDate" />Ship Date</td>
		       </tr>
		      </table>
		      <p>start:</p>
		      <input style="width:90px;margin-right:5px;" name="startDate" id="startDate" type="text" value="<fmt:formatDate type="date" pattern="MM/dd/yyyy" value='${salesRepDetailFilter2.startDate}'/>"  size="11" />
			  <img class="calendarImage"  id="startDate_trigger" src="../graphics/calendarIcon.jpg" alt="" />
			  <script type="text/javascript">
			   Calendar.setup({
					inputField     :    "startDate",   // id of the input field
					showsTime      :    false,
					ifFormat       :    "%m/%d/%Y",   // format of the input field
					button         :    "startDate_trigger"   // trigger for the calendar (button ID)
			   });	
			  </script>
			</div>  
		    <div style="clear:both;"></div>    
		    <div class="advancedSearch">
		      <p>end:</p>
		      <input style="width:90px;margin-right:5px;" name="endDate" id="endDate" type="text" value="<fmt:formatDate type="date" pattern="MM/dd/yyyy" value='${salesRepDetailFilter2.endDate}'/>"  size="11" />
			  <img class="calendarImage"  id="endDate_trigger" src="../graphics/calendarIcon.jpg" alt="" />
			  <script type="text/javascript">
			   Calendar.setup({
					inputField     :    "endDate",   // id of the input field
					showsTime      :    false,
					ifFormat       :    "%m/%d/%Y",   // format of the input field
					button         :    "endDate_trigger"   // trigger for the calendar (button ID)
			   });	
			  </script>
			</div>  
			<div style="clear:both;"></div>    
		    <div class="advancedSearch">
		      <table border="0">
		       <tr> <td class="advancedSearchDateInterval"><input style="width:12px;margin-right:5px;border: 0px" name="intervaldate" id="today" type="radio" onclick="UpdateStartEndDate(1);" />Today</td> </tr>
		       <tr> <td class="advancedSearchDateInterval"><input style="width:12px;margin-right:5px;border: 0px" name="intervaldate" id="weekToDate" type="radio" onclick="UpdateStartEndDate(2);" />Week To Date</td> </tr>
		       <tr> <td class="advancedSearchDateInterval"><input style="width:12px;margin-right:5px;border: 0px" name="intervaldate" id="monthToDate" type="radio" onclick="UpdateStartEndDate(3);" />Month To Date</td> </tr>
		       <tr> <td class="advancedSearchDateInterval"><input style="width:12px;margin-right:5px;border: 0px" name="intervaldate" id="yearToDate" type="radio" onclick="UpdateStartEndDate(10);" />Year To Date</td> </tr>
		       <tr> <td class="advancedSearchDateInterval"><input style="width:12px;margin-right:5px;border: 0px" name="intervaldate" id="reset" type="radio" onclick="UpdateStartEndDate(20);" />Reset</td> </tr>
		      </table>  
		    </div>
		</div>
		</td></tr></table>
	  
	  </td><td class="boxmidr" ></td></tr>
      <tr><td class="botl"></td><td class="botr"></td></tr>
      </table></div>
    
      </div>
      
	  </td>
	  </tr></table>
	  </form>
	<div class="menudiv"></div>    
	</c:if>
	</sec:authorize>
	
	<sec:authorize ifAnyGranted="ROLE_AEM,ROLE_ADMIN_${prop_site_id},ROLE_REPORT_CUSTOMER_METRIC"> 
    <c:if test="${param.tab == 'metricOverviewReport'}"> 
    <div class="menudiv"></div>
      <div align="left" class="searchHeader" id="searchHeaderId"><h2><fmt:message key="search" /></h2></div>
	  <table class="searchBoxLeft" width="100%"><tr><td>
	  <form action="metricOverviewReport.jhtm" method="post">
      <!-- content area -->
      <div class="search2">
	      <p><fmt:message key="quarter"/>:</p>
	      <select name="quarter">
	       <option value="1" <c:if test="${metricOverviewFilter.quarter == '1'}">selected</c:if>><fmt:message key="quarter" /></option>
	       <option value="0" <c:if test="${metricOverviewFilter.quarter == '0'}">selected</c:if>><fmt:message key="year" /></option>
	      </select>
	  </div>
      <div class="search2">
      	<p><fmt:message key="year"/>:</p>
	      <select name="year">
	       <c:forEach items="${model.years}" var="year"	varStatus="status">
	         <option <c:if test="${metricOverviewFilter.year == year}">selected</c:if> value="${year}">${year}</option>
	       </c:forEach>
	      </select>
      </div><c:if test="${gSiteConfig['gADD_INVOICE']}">
	    <div class="search2">
	      <p><fmt:message key="generatedFrom" />:</p>
	      <select name="backendOrder">
	  	    <option value=""><fmt:message key="allOrders" /></option>
	  	    <option value="1" <c:if test="${metricOverviewFilter.backendOrder == '1'}">selected</c:if>><fmt:message key="backend" /></option>
	  	    <option value="0" <c:if test="${metricOverviewFilter.backendOrder == '0'}">selected</c:if>><fmt:message key="frontend" /></option>
	  	  </select>
	    </div>
	  </c:if>
	  <div class="search2">
	      <p><fmt:message key="dateType" />:</p>
	      <select name="dateType">
			<option value="orderDate">Order Date</option>
			<option value="shipDate" <c:if test="${metricOverviewFilter.dateType == 'shipDate'}">selected</c:if>>Ship Date</option>
		  </select>
	  </div>   
      <div class="search2">
	      <p><fmt:message key="salesRep" />:</p>
	      <select name="salesRepId">
		   <c:if test="${model.salesRepTree == null}">
	       <option value="-1"></option>
	       <c:forEach items="${model.salesReps}" var="salesRep">
	  	     <option value="${salesRep.id}" <c:if test="${metricOverviewFilter.salesRepId == salesRep.id}">selected</c:if>><c:out value="${salesRep.name}" /></option>
	       </c:forEach>
	       </c:if>
	       <c:if test="${model.salesRepTree != null}">
	         <c:set var="salesRepTree" value="${model.salesRepTree}" scope="request"/>
	         <c:set var="metricOverviewFilterSalesRepId" value="${metricOverviewFilter.salesRepId}"/>
	         <option value="${salesRepTree.id}" <c:if test="${metricOverviewFilter.salesRepId == salesRepTree.id}">selected</c:if>><c:out value="${salesRepTree.name}" /></option>
			 <c:set var="recursionPageId" value="${metricOverviewFilterSalesRepId}" scope="request"/>
	      	 <c:set var="level" value="1" scope="request"/>
			 <jsp:include page="/WEB-INF/jsp/admin/report/recursion.jsp"/>	       
	       </c:if>
	      </select>
	    </div>
      <div class="search2">
	      <p><fmt:message key="orderType" />:</p>
	      <select name="orderType">
	       <option value=""><fmt:message key="allOrders" /></option>
	       <c:forTokens items="${siteConfig['ORDER_TYPE'].value}" delims="," var="type" varStatus="status">
			  <option value ="${type}" <c:if test="${type == metricOverviewFilter.orderType}">selected</c:if>><c:out value ="${type}" /></option>
		   </c:forTokens>
	      </select>
	    </div>
      <div class="button">
	      <input type="submit" value="Search"/>
      </div>
      <!-- content area -->
	  </form>
	  </td></tr></table>
	<div class="menudiv"></div>
	</c:if>
	</sec:authorize>
	
	<sec:authorize ifAnyGranted="ROLE_AEM,ROLE_ADMIN_${prop_site_id}"> 
	<c:if test="${param.tab == 'inventory'}"> 
	<div class="menudiv"></div> 
      <div align="left" class="searchHeader" id="searchHeaderId"><h2><fmt:message key="search" /></h2></div>
	  <form action="inventoryReport.jhtm" method="post" name="list_inventory_search">
	  <table class="searchBoxLeft" width="100%"><tr><td valign="top" style="width:110px;">
	  <!-- content area -->
        <c:if test="${gSiteConfig['gINVENTORY']}">
	    <div class="search2">
	      <p><fmt:message key="sku"/>:   <a href="#operatorSku" rel="type:element" id="mbOperator1" class="mbOperator" style="margin-left: 67px;"><img class="toolTipImg" title="Advanced Search::Click to see more Options" src="../graphics/question.gif" /></a></p>
		  <div id="operatorSku" class="miniWindowWrapper searchOp displayNone">
	    	<div class="header">Advanced Search</div>
	    	  <fieldset class="">
	    	    <label class="AStitle"><fmt:message key="sku"/></label>
	    	    <input name="sku" type="text" value="<c:out value='${inventoryReportFilter.sku}' />" size="15" disabled />
	    	  </fieldset>
	    	  <fieldset class="bottom">
	    	    <label class="AStitle"><fmt:message key="operator"/></label>
	    	    <div class="options">
	    	       <label class="radio">
	    	       		<input type="radio" <c:if test="${inventoryReportFilter.skuOperator == 0}" >checked="checked"</c:if> value="0" name="skuOp" onchange="selectOperator(this.value, 'sku');" /> <strong>Contains</strong>
	    	       </label>
	    	       <label class="radio">
	    	       		<input type="radio" <c:if test="${inventoryReportFilter.skuOperator == 1}" >checked="checked"</c:if> value="1" name="skuOp" onchange="selectOperator(this.value, 'sku');" /> <strong>Exact</strong>
	    	       </label>
	    	    </div>
	    	  </fieldset>
	    	  <fieldset>
	    	     <div class="button">
	      			<input type="submit" value="Search" onclick="document.list_inventory_search.submit();"/>
      			 </div>
	    	  </fieldset>
		  </div>
		  <input name="sku" type="text" value="<c:out value='${inventoryReportFilter.sku}' />">
	  	  <input type="hidden" name="skuOperator" id="skuOperator"/>
	    </div>
	    <div class="search2">
	      <p><fmt:message key="year" />:</p>
	      <select name="year">
	        	<option value="">Please Select</option>
	        <c:forEach items="${model.years}" var="yr">
	        	<option value="${yr}" <c:if test="${(inventoryReportFilter.startDate != null) and ((inventoryReportFilter.startDate.year+1900) == yr)}">selected</c:if>>${yr}</option>
	        </c:forEach>
	      </select>
	    </div>
	    </c:if>
	    <div class="button">
	      <input type="submit" value="Search"/>
		</div>
	  <!-- content area -->
	  </td>
	  </tr></table>
	  </form>
	<div class="menudiv"></div>    
	</c:if>
	</sec:authorize>

	<c:if test="${siteConfig['INVENTORY_HISTORY'].value == 'true'}">
	<sec:authorize ifAnyGranted="ROLE_AEM,ROLE_ADMIN_${prop_site_id}"> 
	<c:if test="${param.tab == 'inventoryActivity'}"> 
	<div class="menudiv"></div> 
      <div align="left" class="searchHeader" id="searchHeaderId"><h2><fmt:message key="search" /></h2></div>
	  <form action="inventoryActivityReport.jhtm" method="post" name="list_inventory_activity_search">
	  <table class="searchBoxLeft" width="100%"><tr><td valign="top" style="width:110px;">
	  <!-- content area -->
        <c:if test="${gSiteConfig['gINVENTORY']}">
	    <div class="search2">
	      <p><fmt:message key="sku" />:<a href="#operatorSku" rel="type:element" id="mbOperator1" class="mbOperator" style="margin-left: 67px;"><img class="toolTipImg" title="Advanced Search::Click to see more Options" src="../graphics/question.gif" /></a></p>
	      <div id="operatorSku" class="miniWindowWrapper searchOp displayNone">
	    	<div class="header">Advanced Search</div>
	    	  <fieldset class="">
	    	    <label class="AStitle"><fmt:message key="sku"/></label>
	    	    <input name="sku" type="text" value="<c:out value='${inventoryActivityFilter.sku}' />" size="15" disabled />
	    	  </fieldset>
	    	  <fieldset class="bottom">
	    	    <label class="AStitle"><fmt:message key="operator"/></label>
	    	    <div class="options">
	    	       <label class="radio">
	    	       		<input type="radio" <c:if test="${inventoryActivityFilter.skuOperator == 0}" >checked="checked"</c:if> value="0" name="skuOp" onchange="selectOperator(this.value, 'sku');" /> <strong>Contains</strong>
	    	       </label>
	    	       <label class="radio">
	    	       		<input type="radio" <c:if test="${inventoryActivityFilter.skuOperator == 1}" >checked="checked"</c:if> value="1" name="skuOp" onchange="selectOperator(this.value, 'sku');" /> <strong>Exact</strong>
	    	       </label>
	    	    </div>
	    	  </fieldset>
	    	  <fieldset>
	    	     <div class="button">
	      			<input type="submit" value="Search" onclick="document.list_inventory_activity_search.submit();"/>
      			 </div>
	    	  </fieldset>
		  </div>
	      <input name="sku" type="text" value="<c:out value='${inventoryActivityFilter.sku}' />">
	      <input type="hidden" name="skuOperator" id="skuOperator"/>
	    </div>
	    <div class="search2">
	      <p><fmt:message key="type" />:</p>
	      <input name="type" type="text" value="<c:out value='${inventoryActivityFilter.type}' />">
	  	</div>
	    <div class="search2">
	      <p><fmt:message key="year" />:</p>
	      <select name="year">
	        	<option value="">Please Select</option>
	        <c:forEach items="${model.years}" var="yr">
	        	<option value="${yr}" <c:if test="${(inventoryActivityFilter.startDate != null) and ((inventoryActivityFilter.startDate.year+1900) == yr)}">selected</c:if>>${yr}</option>
	        </c:forEach>
	      </select>
	    </div>
	    </c:if>
	    <div class="button">
	      <input type="submit" value="Search"/>
		</div>
	  <!-- content area -->
	  </td>
	  </tr></table>
	  </form>
	<div class="menudiv"></div>    
	</c:if>
	</sec:authorize>
	</c:if>
	
	<c:if test="${gSiteConfig['gPROCESSING']}">
	<sec:authorize ifAnyGranted="ROLE_AEM,ROLE_ADMIN_${prop_site_id},ROLE_REPORT_PROCESSING"> 
	<c:if test="${param.tab == 'truckLoad'}"> 
	<script type="text/javascript" src="../javascript/side-bar.js"></script>
	<div class="menudiv"></div> 
      <div align="left" class="searchHeader" id="searchHeaderId"><h2><fmt:message key="search" /></h2></div>
	  <form action="truckLoadProcessReport.jhtm" method="post" name="list_inventory_activity_search">
	  <table class="searchBoxLeft" width="100%"><tr><td valign="top" style="width:110px;">
	  <!-- content area -->
        <div class="search2">
	    <p><fmt:message key="truckLoadProcess" />:</p>
	      <input name="name" type="text" value="<c:out value='${truckLoadProcessSearch.name}' />">
	    </div>
	  	<div class="search2">
	    <p><fmt:message key="originalSku" />:</p>
	      <input name="originalSku" type="text" value="<c:out value='${truckLoadProcessSearch.originalSku}' />">
	    </div>
	    <div class="producedSku">
	    <p><fmt:message key="producedSku" />:</p>
	      <input name="producedSku" type="text" value="<c:out value='${truckLoadProcessSearch.producedSku}' />">
	    </div>
	  	<div class="search2">
	      <p><fmt:message key="truckLoadCustom1" />:</p>
	      <input name="custom1" type="text" value="<c:out value='${truckLoadProcessSearch.custom1}' />">
	  	</div>
	  	<div class="search2">
	      <p><fmt:message key="truckLoadCustom2" />:</p>
	      <input name="custom2" type="text" value="<c:out value='${truckLoadProcessSearch.custom2}' />">
	  	</div>
	  	<div class="search2">
	      <p><fmt:message key="truckLoadCustom3" />:</p>
	      <input name="custom3" type="text" value="<c:out value='${truckLoadProcessSearch.custom3}' />">
	  	</div>
	  	<div class="search2">
	      <p><fmt:message key="truckLoadCustom4" />:</p>
	      <input name="custom4" type="text" value="<c:out value='${truckLoadProcessSearch.custom4}' />">
	  	</div>
	  	<div class="search2">
	      <p><fmt:message key="status" />:</p>
	      <select name="status">
	        <option value="">Please Select</option> 
			<option value="x" <c:if test="${truckLoadProcessSearch.status == 'x'}">selected</c:if>><fmt:message key="processStatus_x" /></option>
			<option value="c" <c:if test="${truckLoadProcessSearch.status == 'c'}">selected</c:if>><fmt:message key="processStatus_c" /></option>
	        <option value="pr" <c:if test="${truckLoadProcessSearch.status == 'pr'}">selected</c:if>><fmt:message key="processStatus_pr" /></option>
	        <option value="xpr" <c:if test="${truckLoadProcessSearch.status == 'xpr'}">selected</c:if>><fmt:message key="processStatus_xpr" /></option>
			<option value="cpr" <c:if test="${truckLoadProcessSearch.status == 'cpr'}">selected</c:if>><fmt:message key="processStatus_cpr" /></option>
	        <option value="xc" <c:if test="${truckLoadProcessSearch.status == 'xc'}">selected</c:if>><fmt:message key="processStatus_xc" /></option>
	      </select>
	  	</div>
	    <div class="button">
	      <input type="submit" value="Search"/>
		</div>
	  <!-- content area -->
	  </td>
	  <td valign="top">
	  <div id="sideBar">
	
	  <a href="#" id="sideBarTab"><img src="../graphics/tabSliderLeft.png" alt="sideBar" title="sideBar" /></a>
	  <div id="sideBarContents" style="width:0px;">
	  <table cellpadding="0" cellspacing="0" border="0" class="module" >
      <tr>
      <td class="topl_g"><div align="left" class="searchHeaderAdvanced"><h2><fmt:message key="advancedSearch" /></h2></div></td>
      <td class="topr_g" ></td></tr>
      <tr><td class="boxmidlrg" >
  
		<table class="searchBoxLeftAS"><tr><td>
		<div id="sideBarContentsInner">
			<div class="advancedSearch">
		      <p>start:</p>
		      <input style="width:90px;margin-right:5px;" name="startDate" id="startDate" type="text" value="<fmt:formatDate type="date" pattern="MM/dd/yyyy" value='${truckLoadProcessSearch.startDate}'/>"  size="11" />
			  <img class="calendarImage"  id="startDate_trigger" src="../graphics/calendarIcon.jpg" alt="" />
			  <script type="text/javascript">
			   Calendar.setup({
					inputField     :    "startDate",   // id of the input field
					showsTime      :    false,
					ifFormat       :    "%m/%d/%Y",   // format of the input field
					button         :    "startDate_trigger"   // trigger for the calendar (button ID)
			   });	
			  </script>
			</div>  
		    <div style="clear:both;"></div>    
		    <div class="advancedSearch">
		      <p>end:</p>
		      <input style="width:90px;margin-right:5px;" name="endDate" id="endDate" type="text" value="<fmt:formatDate type="date" pattern="MM/dd/yyyy" value='${truckLoadProcessSearch.endDate}'/>"  size="11" />
			  <img class="calendarImage"  id="endDate_trigger" src="../graphics/calendarIcon.jpg" alt="" />
			  <script type="text/javascript">
			   Calendar.setup({
					inputField     :    "endDate",   // id of the input field
					showsTime      :    false,
					ifFormat       :    "%m/%d/%Y",   // format of the input field
					button         :    "endDate_trigger"   // trigger for the calendar (button ID)
			   });	
			  </script>
			</div>  
			<div style="clear:both;"></div>    
		    <div class="advancedSearch">
		      <table border="0">
		       <tr> <td class="advancedSearchDateInterval"><input style="width:12px;margin-right:5px;border: 0px" name="intervaldate" id="today" type="radio" onclick="UpdateStartEndDate(1);" />Today</td> </tr>
		       <tr> <td class="advancedSearchDateInterval"><input style="width:12px;margin-right:5px;border: 0px" name="intervaldate" id="weekToDate" type="radio" onclick="UpdateStartEndDate(2);" />Week To Date</td> </tr>
		       <tr> <td class="advancedSearchDateInterval"><input style="width:12px;margin-right:5px;border: 0px" name="intervaldate" id="monthToDate" type="radio" onclick="UpdateStartEndDate(3);" />Month To Date</td> </tr>
		       <tr> <td class="advancedSearchDateInterval"><input style="width:12px;margin-right:5px;border: 0px" name="intervaldate" id="yearToDate" type="radio" onclick="UpdateStartEndDate(10);" />Year To Date</td> </tr>
		       <tr> <td class="advancedSearchDateInterval"><input style="width:12px;margin-right:5px;border: 0px" name="intervaldate" id="reset" type="radio" onclick="UpdateStartEndDate(20);" />Reset</td> </tr>
		      </table>  
		    </div>
		</div>
		</td></tr></table>
	  
	  </td><td class="boxmidr" ></td></tr>
      <tr><td class="botl"></td><td class="botr"></td></tr>
      </table></div>
    
      </div>
      
	  </td>
	  </tr></table>
	  </form>
	<div class="menudiv"></div>    
	</c:if>     
	</sec:authorize>
	</c:if>
	</c:if>
	<c:if test="${param.tab == 'abandonedShoppingCart'}"> 
	<c:if test="${gSiteConfig['gSALES_REP']}">
	   <div align="left" class="searchHeader" id="searchHeaderId"><h2><fmt:message key="search" /></h2></div>
	    <form action="abandonedShoppingCartController.jhtm" name="searchform" method="post">
	  <table class="searchBoxLeft"><tr><td>
	 
	  <!-- content area -->
    <div class="menudiv"></div>
	 <div class="search2">
	      <p><fmt:message key="salesRep" />:</p>
	      <select name="sales_rep_id">
		   <c:if test="${model.salesRepTree == null}">
	       <option value="-1"></option>
	       <option value="-2" <c:if test="${abandonedShoppingCartSearch.salesRepId == -2}">selected</c:if>><fmt:message key="noSalesRep" /></option>
	       <c:forEach items="${model.salesReps}" var="salesRep">
	  	     <option value="${salesRep.id}" <c:if test="${abandonedShoppingCartSearch.salesRepId == salesRep.id}">selected</c:if>><c:out value="${salesRep.name}" /></option>
	       </c:forEach>
	       </c:if>
	       <c:if test="${model.salesRepTree != null}">
	         <c:set var="salesRepTree" value="${model.salesRepTree}" scope="request"/>
	         <c:set var="abandonedShoppingCartSearchSalesRepId" value="${abandonedShoppingCartSearch.salesRepId}" scope="request"/>
	         <option value="${salesRepTree.id}" <c:if test="${abandonedShoppingCartSearch.salesRepId == salesRepTree.id}">selected</c:if>><c:out value="${salesRepTree.name}" /></option>
			 <c:set var="level" value="0" scope="request"/>
	      	 <c:set var="recursionPageId" value="${abandonedShoppingCartSearchSalesRepId}" scope="request"/>
	      	 <jsp:include page="/WEB-INF/jsp/admin/customers/recursion.jsp"/>
	       </c:if>
	      </select>
	   </div>   
		
		<div id="sideBarContentsInner">
			<div class="advancedSearch">
		      <p>start:</p>
		      <input style="width:90px;margin-right:5px;" name="startDate" id="startDate" type="text" value="<fmt:formatDate type="date" pattern="MM/dd/yyyy" value="${abandonedShoppingCartSearch.startDate}"/>" size="11" />
			  <img class="calendarImage"  id="startDate_trigger" src="../graphics/calendarIcon.jpg" />
			  <script type="text/javascript">
	    		Calendar.setup({
	        			inputField     :    "startDate",   // id of the input field
	        			showsTime      :    false,
	        			ifFormat       :    "%m/%d/%Y",   // format of the input field
	        			button         :    "startDate_trigger"   // trigger for the calendar (button ID)
	    		});	
			  </script>
		    </div>
		    <div style="clear:both;"></div>    
		    <div class="advancedSearch">
		      <p>end:</p>
		      <input style="width:90px;margin-right:5px;" name="endDate" id="endDate" type="text" value="<fmt:formatDate type="date" pattern="MM/dd/yyyy" value='${abandonedShoppingCartSearch.endDate}'/>" size="11" />
			  <img class="calendarImage"  id="endDate_trigger" src="../graphics/calendarIcon.jpg" />
			  <script type="text/javascript">
	    		Calendar.setup({
	        			inputField     :    "endDate",   // id of the input field
	        			showsTime      :    false,
	        			ifFormat       :    "%m/%d/%Y",   // format of the input field
	        			button         :    "endDate_trigger"   // trigger for the calendar (button ID)
	    		});	
			  </script>
			</div>  
			<div style="clear:both;"></div>    
		    <div class="advancedSearch">
		      <table border="0">
		       <tr> <td class="advancedSearchDateInterval"><input style="width:12px;margin-right:5px;border: 0px" name="intervaldate" id="today" type="radio" onclick="UpdateStartEndDate(1);" />Today</td> </tr>
		       <tr> <td class="advancedSearchDateInterval"><input style="width:12px;margin-right:5px;border: 0px" name="intervaldate" id="weekToDate" type="radio" onclick="UpdateStartEndDate(2);" />Week To Date</td> </tr>
		       <tr> <td class="advancedSearchDateInterval"><input style="width:12px;margin-right:5px;border: 0px" name="intervaldate" id="monthToDate" type="radio" onclick="UpdateStartEndDate(3);" />Month To Date</td> </tr>
		       <tr> <td class="advancedSearchDateInterval"><input style="width:12px;margin-right:5px;border: 0px" name="intervaldate" id="yearToDate" type="radio" onclick="UpdateStartEndDate(10);" />Year To Date</td> </tr>
		       <tr> <td class="advancedSearchDateInterval"><input style="width:12px;margin-right:5px;border: 0px" name="intervaldate" id="reset" type="radio" onclick="UpdateStartEndDate(20);" />Reset</td> </tr>
		      </table>  
		    </div>
		</div>    
	<div class="button">
	 	  <input type="submit" value="Search"/>
		</div>
		 </td><td class="boxmidr" ></td></tr></table>
	  </form>
	<div class="menudiv"></div>	
    	  </c:if>
	  </c:if>
	  	  
	  	  
	  <c:if test="${param.tab == 'eventMemberList'}"> 
      <div align="left" class="searchHeader" id="searchHeaderId"><h2><fmt:message key="search" /></h2></div>
	  <table class="searchBoxLeft" width="100%"><tr><td>
	  <form action="eventMemberListReport.jhtm" method="post">
	  <input type="hidden" name="id" value="<c:out value='${eventMemberReportSearch.eventId}' />" />
      <!-- content area -->
      <c:if test="${gSiteConfig['gSALES_REP']}">
	    <div class="search2">
	      <p><fmt:message key="salesRep" />:</p>
	      <select name="salesRepId">
	       <option value="-1"></option>
	       <option value="-2" <c:if test="${eventMemberReportSearch.salesRepId == -2}">selected</c:if>><fmt:message key="noSalesRep" /></option>
	       <c:forEach items="${model.salesReps}" var="salesRep">
	  	     <option value="${salesRep.id}" <c:if test="${eventMemberReportSearch.salesRepId == salesRep.id}">selected</c:if>><c:out value="${salesRep.name}" /></option>
	       </c:forEach>
	      </select>
	  </div>  
	  </c:if>
	  <div class="search2">
	      <p><fmt:message key="firstName" />:</p>
	      <input name="firstName" id="firstName" type="text" value="<c:out value='${eventMemberReportSearch.firstName}' />" size="15" />
	  </div> 
	  <div class="search2">
	      <p><fmt:message key="lastName" />:</p>
	      <input name="lastName" id="lastName" type="text" value="<c:out value='${eventMemberReportSearch.lastName}' />" size="15" />
	  </div>  
	  <div class="search2">
	      <p><fmt:message key="email" />:</p>
	      <input name="email" type="text" value="<c:out value='${eventMemberReportSearch.email}' />" size="15" />
	  </div>
      <c:if test="${gSiteConfig['gREGISTRATION_TOUCH_SCREEN']}">
	    <div class="search2">
	      <p><fmt:message key="idCard" />:</p>
	      <input name="cardId" type="text" value="<c:out value='${eventMemberReportSearch.cardId}' />" size="15" />
	    </div>
	  </c:if>    
      <div class="button">
	      <input type="submit" value="Search"/>
      </div>
      <!-- content area -->
	  </form>
	  </td></tr></table>
	  </c:if>
	  
	  
	  </td><td class="boxmidr" ></td></tr>
    <tr><td class="botl"></td><td class="botr"></td></tr></table>
  </div>