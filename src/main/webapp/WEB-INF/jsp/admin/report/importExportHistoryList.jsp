<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/security/tags" prefix="sec" %>

<tiles:insertDefinition name="admin.report.importExport" flush="true">
  <tiles:putAttribute name="content" type="string">
  
<sec:authorize ifAnyGranted="ROLE_AEM,ROLE_ADMIN_${prop_site_id},ROLE_REPORT_IMP_EXP">
<form id="list" action="importExportHistory.jhtm" method="post">
<input type="hidden" id="sort" name="sort" value="${importExportHistorySearch.sort}" />
<!-- main box -->
  <div id="mboxfull">
  
  <!-- start table -->
  <table cellpadding="0" cellspacing="0" border="0" class="module" >
  <tr>
    <td class="topl_g">
	  
	  <!-- breadcrumb -->
	  <p class="breadcrumb">
	    <a href="../reports"><fmt:message key="reports" /></a> &gt;
	    <fmt:message key="importExport" />
	  </p>
	  
	  <!-- Error -->
	  <c:if test="${!empty model.message}">
		<div class="message"><fmt:message key="${model.message}" /></div>
	  </c:if>
	  
	  <!-- header image -->
	  <img class="headerImage" src="../graphics/reports.gif" /> 
	  
  </td><td class="topr_g" ></td></tr>
  <tr><td class="boxmidlrg" >

 <!-- tabs -->
 <div class="tab-wrapper">
	<h4 title=""></h4>
	<div>
	<!-- start tab -->

	  	<div class="listdivi"></div>
	    
		  	<div class="listlight">
		  	<!-- input field -->
			<table border="0" cellpadding="0" cellspacing="1" width="100%">
				<tr>
					<td>&nbsp;
					</td>
					<c:if test="${model.importExportHistoryList.nrOfElements > 0}">
						<td class="pageShowing">
							<fmt:message key="showing">
								<fmt:param value="${model.importExportHistoryList.firstElementOnPage + 1}" />
								<fmt:param value="${model.importExportHistoryList.lastElementOnPage + 1}" />
								<fmt:param value="${model.importExportHistoryList.nrOfElements}" />
							</fmt:message>
						</td>
					</c:if>
					<td class="pageNavi">
						<fmt:message key="page" />
						<select name="page" id="page" onchange="submit()">
							<c:forEach begin="1" end="${model.importExportHistoryList.pageCount}"
								var="page">
								<option value="${page}"
									<c:if test="${page == (model.importExportHistoryList.page+1)}">selected</c:if>>
									${page}
								</option>
							</c:forEach>
						</select> 
						<fmt:message key="of" />
						<c:out value="${model.importExportHistoryList.pageCount}" /> 
						|
						<c:if test="${model.importExportHistoryList.firstPage}">
							<span class="pageNaviDead"><fmt:message key="previous" /></span>
						</c:if>
						<c:if test="${not model.importExportHistoryList.firstPage}">
							<a
								href="<c:url value="importExportHistory.jhtm"><c:param name="page" value="${model.importExportHistoryList.page}"/><c:param name="size" value="${model.purchaseOrders.pageSize}"/></c:url>"
								class="pageNaviLink"><fmt:message key="previous" /></a>
						</c:if>
						|
						<c:if test="${model.importExportHistoryList.lastPage}">
							<span class="pageNaviDead"><fmt:message key="next" /></span>
						</c:if>
						<c:if test="${not model.importExportHistoryList.lastPage}">
							<a
								href="<c:url value="importExportHistory.jhtm"><c:param name="page" value="${model.importExportHistoryList.page+2}"/><c:param name="size" value="${model.purchaseOrders.pageSize}"/></c:url>"
								class="pageNaviLink"><fmt:message key="next" /></a>
						</c:if>
					</td>
				</tr>
			</table>
		
			<table border="0" cellpadding="0" cellspacing="0" width="100%"
				class="listings">
				<tr class="listingsHdr2">
				    <td class="indexCol">&nbsp;</td>
					<td class="listingsHdr3"><fmt:message key="type" /></td>
					<td class="listingsHdr3">
			    	  <table cellspacing="0" cellpadding="1">
			    	  <tr>
			    	    <c:choose>
			    	      <c:when test="${importExportHistorySearch.sort == 'created DESC'}">
			    	        <td class="listingsHdr3">
			    	        <a class="hdrLink" href="#" onClick="document.getElementById('sort').value='created';document.getElementById('list').submit()"><fmt:message key="created" /></a>
			    	        </td>
			    	        <td><img src="../graphics/up.gif" border="0"></td>
			    	      </c:when>
			    	      <c:when test="${importExportHistorySearch.sort == 'created'}">
			    	        <td class="listingsHdr3">
			    	        <a class="hdrLink" href="#" onClick="document.getElementById('sort').value='created DESC';document.getElementById('list').submit()"><fmt:message key="created" /></a>
			    	        </td>
			    	        <td><img src="../graphics/down.gif" border="0"></td>
			    	      </c:when>
			    	      <c:otherwise>
			    	        <td colspan="2" class="listingsHdr3">
			    	        <a class="hdrLink" href="#" onClick="document.getElementById('sort').value='created DESC';document.getElementById('list').submit()"><fmt:message key="created" /></a>
			    	        </td>
			    	      </c:otherwise>
			    	    </c:choose>  
			    	  </tr>
			    	  </table>
					</td>
					<td class="listingsHdr3">
			    	  <table cellspacing="0" cellpadding="1">
			    	  <tr>
			    	    <c:choose>
			    	      <c:when test="${importExportHistorySearch.sort == 'username DESC'}">
			    	        <td class="listingsHdr3">
			    	        <a class="hdrLink" href="#" onClick="document.getElementById('sort').value='username';document.getElementById('list').submit()"><fmt:message key="username" /></a>
			    	        </td>
			    	        <td><img src="../graphics/up.gif" border="0"></td>
			    	      </c:when>
			    	      <c:when test="${importExportHistorySearch.sort == 'username'}">
			    	        <td class="listingsHdr3">
			    	        <a class="hdrLink" href="#" onClick="document.getElementById('sort').value='username DESC';document.getElementById('list').submit()"><fmt:message key="username" /></a>
			    	        </td>
			    	        <td><img src="../graphics/down.gif" border="0"></td>
			    	      </c:when>
			    	      <c:otherwise>
			    	        <td colspan="2" class="listingsHdr3">
			    	        <a class="hdrLink" href="#" onClick="document.getElementById('sort').value='username DESC';document.getElementById('list').submit()"><fmt:message key="username" /></a>
			    	        </td>
			    	      </c:otherwise>
			    	    </c:choose>  
			    	  </tr>
			    	  </table>
					</td>
					<td class="listingsHdr3"><fmt:message key="import" />/<fmt:message key="export" /></td>
				</tr>
				<c:forEach items="${model.importExportHistoryList.pageList}" var="ieHistory" varStatus="status">
					<tr class="row${status.index % 2}"	onmouseover="changeStyleClass(this,'rowOver')" 	onmouseout="changeStyleClass(this,'row${status.index % 2}')">
						<td class="indexCol">
							<c:out value="${status.count + model.importExportHistoryList.firstElementOnPage}" />.
						</td>
						<td class="nameCol"><c:out value="${ieHistory.type}" /></td>
						<td align="left"><fmt:formatDate type="both" timeStyle="full" pattern="MM/dd/yyyy hh:mm a" value="${ieHistory.created}"/></td>
						<td align="left"><c:out value="${ieHistory.username}" /></td>
						<td align="left"><c:out value='${ieHistory.importedString}' /></td>
					</tr>
				</c:forEach>
				<c:if test="${model.purchaseOrders.nrOfElements == 0}">
					<tr class="emptyList">
						<td colspan="8">
							&nbsp;
						</td>
					</tr>
				</c:if>
			</table>
			
			<c:if test="${model.importExportHistoryList.nrOfElements > 0}">
			</c:if>
			<table border="0" cellpadding="0" cellspacing="1" width="100%">
				<tr>
					<td class="pageSize">
						<select name="size"
							onchange="document.getElementById('page').value=1;submit()">
							<c:forTokens items="10,25,50,100,200,500" delims="," var="current">
								<option value="${current}"
									<c:if test="${current == model.importExportHistoryList.pageSize}">selected</c:if>>
									${current}
									<fmt:message key="perPage" />
								</option>
							</c:forTokens>
						</select>
					</td>
				</tr>
			</table>
		    <!-- end input field -->  	  
		  	</div>
		  	
		  	<!-- start button -->
			<!-- end button -->	
	        	  	
	<!-- end tab -->        
	</div>
<!-- end tabs -->			
</div>
  
  <!-- end table -->
   </td><td class="boxmidr" ></td></tr>
  <tr><td class="botl"></td><td class="botr"></td></tr>
  </table>
  
<!-- end main box -->  
</div>
</form>
</sec:authorize>

  </tiles:putAttribute>
</tiles:insertDefinition>