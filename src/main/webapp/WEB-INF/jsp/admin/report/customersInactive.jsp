<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/security/tags" prefix="sec" %>

<tiles:insertDefinition name="admin.report.customersInactive" flush="true">
  <tiles:putAttribute name="content" type="string">  
  
<sec:authorize ifAnyGranted="ROLE_AEM,ROLE_ADMIN_${prop_site_id},ROLE_REPORT_CUSTOMERS">
<c:if test="${gSiteConfig['gGROUP_CUSTOMER']}">
<script type="text/javascript" src="../../admin/javascript/multiple.open.accordion1.2.js"></script>
</c:if>
<script type="text/javascript">
<!--
window.addEvent('domready', function(){
	$$('img.action').each(function(img){
		//containers
		var actionList = img.getParent(); 
		var actionHover = actionList.getElements('div.action-hover')[0];
		actionHover.set('opacity',0);
		//show/hide
		img.addEvent('mouseenter',function() {
			actionHover.setStyle('display','block').fade('in');
		});
		actionHover.addEvent('mouseleave',function(){
			actionHover.fade('out');
		});
		actionList.addEvent('mouseleave',function() {
			actionHover.fade('out');
		});
	});
	var box2 = new multiBox('mbCustomer', {descClassName: 'multiBoxDesc',waitDuration: 5,showNumbers: false,showControls: false,overlay: new overlay()});
	var box3 = new multiBox('mbRegisterEvent', {descClassName: 'multiBoxDesc',descMaxWidth: 400,openFromLink: false,showNumbers: false,showControls: false,useOverlay: false});
	var Tips1 = new Tips('.toolTipImg'); 
	var myToggler = $$('div.arrow_drop');
	// Create the accordian
	var myAccordion = new Fx.Accordion($$('div.arrow_drop'), $$('div.information'), {
		display: 1, alwaysHide: true,
    	onActive: function() {$('information').removeClass('displayNone');}
	});
});
function toggleAll(el) {
	var ids = document.getElementsByName("__selected_id");	
  	if (ids.length == 1)
      document.list_form.__selected_id.checked = el.checked;
    else
      for (i = 0; i < ids.length; i++)
        document.list_form.__selected_id[i].checked = el.checked;	
} 
function optionsSelected() {
	var ids = document.getElementsByName("__selected_id");	
  	if (ids.length == 1) {
      if (document.list_form.__selected_id.checked) {
    	return true;
      }
    } else {
      for (i = 0; i < ids.length; i++) {
        if (document.list_form.__selected_id[i].checked) {
    	  return true;
        }
      }
    }

    alert("Please select customer(s).");       
    return false;
} 	
//-->
</script>
<form  id="list" action="" name="list_form" method="post">
<input type="hidden" id="sort" name="sort" value="${inActiveCustomerFilter.sort}" />
<!-- main box -->
  <div id="mboxfull">
  
  <!-- start table -->
  <table cellpadding="0" cellspacing="0" border="0" class="module" >
  <tr>
    <td class="topl_g">
	  
	  <!-- breadcrumb -->
	  <p class="breadcrumb">
	    <a href="../reports"><fmt:message key="reports" /></a> &gt;
	    <fmt:message key="inactiveCustomers" />
	  </p>
	  
	  <!-- Error -->
	  <c:if test="${!empty model.message}">
		<div class="message"><fmt:message key="${model.message}" /></div>
	  </c:if>
	  
	  <!-- header image -->
	  <img class="headerImage" src="../graphics/reports.gif" alt=""/> 
	  
	  <!-- option -->
	  <c:if test="${model.count > 0 and gSiteConfig['gGROUP_CUSTOMER']}">
	    <div class="optionBox"><div class="titlePrivilege"><fmt:message key="options" /></div></div>
		<div class="arrow_drop arrowImageRight"></div>
		<div style="clear: both;"></div>
		<div class="information displayNone" id="information" style="float:left;">  
	 
			 <div style="float:left;padding: 5px">
	           <table class="productBatchTool" cellpadding="5" cellspacing="5">
	           <tr>
	            <td valign="top">
	             <table cellpadding="1" cellspacing="1">
	              <tr style="height:30px;white-space: nowrap">
	               <td valign="top" width="20px"><img class="toolTipImg" title="Group Id::Associate group Id to the following customer(s) in the list" src="../graphics/question.gif" /></td>
	               <td valign="top"><h3><fmt:message key="groupId" /></h3></td>
	              </tr>
	             </table>
	            </td>
	           </tr>
	           <tr>
	            <td valign="top">
	             <table cellpadding="1" cellspacing="1">
	              <tr><td colspan="2"> <input type="checkbox" onclick="toggleAll(this)" name="__groupAssignAll" value="true" /><fmt:message key="selectAll"/></td> </tr>
	              <tr style="">
			       <td>
			        <input name="__group_id" type="text" size="15"/>
			        <div align="left" class="buttonLeft">
		            <input type="submit" name="__groupAssignPacher" value="Apply" onClick="return optionsSelected()">
		            </div>
			       </td>
			       <td >
			        <div><p><input name="__batch_type" type="radio" checked="checked" value="add"/>Add</p></div>
				    <div><p><input name="__batch_type" type="radio" value="remove"/>Remove</p></div>
				    <div><p><input name="__batch_type" type="radio" value="move"/>Move</p></div>
			       </td>
			     </tr>
	             </table>
	            </td>
	           </tr>
	           </table>
	         </div>
	    </div>
	  </c:if>
	  
  </td><td class="topr_g" ></td></tr>
  <tr><td class="boxmidlrg" >

 <!-- tabs -->
 <div class="tab-wrapper">
	<h4 title=""></h4>
	<div>
	<!-- start tab -->

	  	<div class="listdivi"></div>
	    
		  	<div class="listlight">
		  	<!-- input field -->
		  	
		  	<table border="0" cellpadding="0" cellspacing="1" width="100%">
				<tr>
					<td>&nbsp;
					</td>
					<c:if test="${model.count > 0}">
						<td class="pageShowing">
							<fmt:message key="showing">
								<fmt:param value="${model.inActiveCustomerFilter.offset + 1}" />
								<fmt:param value="${model.pageEnd}" />
								<fmt:param value="${model.count}" />
							</fmt:message>
						</td>
					</c:if>
					<td class="pageNavi">
						<fmt:message key="page" />
						
						<c:choose>
			  			  <c:when test="${model.pageCount <= 100 and model.pageCount > 0}">
			  				<select id="page" name="page" onchange="submit()">
			  				  <c:forEach begin="1" end="${model.pageCount}" var="page">
			  				    <option value="${page}" <c:if test="${page == (model.inActiveCustomerFilter.page)}">selected</c:if>>${page}</option>
			  				  </c:forEach>
			  				</select>
			  			  </c:when>
			  			  <c:otherwise>
			  			    <input type="text" id="page" name="page" value="${model.inActiveCustomerFilter.page}" size="5" class="textfield50" />
			  				<input type="submit" value="go"/>
			  			  </c:otherwise>
			  			</c:choose>			  
			 			<fmt:message key="of" /><c:out value="${model.pageCount}"/>
			  			| 
			  			<c:if test="${model.inActiveCustomerFilter.page == 1}"><span class="pageNaviDead"><fmt:message key="previous" /></span></c:if>
						<c:if test="${model.inActiveCustomerFilter.page != 1}"><a href="#" onClick="document.getElementById('page').value='${model.inActiveCustomerFilter.page-1}';document.getElementById('list').submit()" class="pageNaviLink"><fmt:message key="previous" /></a></c:if>
						| 
						<c:if test="${model.inActiveCustomerFilter.page == model.pageCount}"><span class="pageNaviDead"><fmt:message key="next" /></span></c:if>
						<c:if test="${model.inActiveCustomerFilter.page != model.pageCount}"><a href="#" onClick="document.getElementById('page').value='${model.inActiveCustomerFilter.page+1}';document.getElementById('list').submit()" class="pageNaviLink"><fmt:message key="next" /></a></c:if>
			  		</td>
				</tr>
			</table>
		  	
		  	<table border="0" cellpadding="2" cellspacing="0" width="100%" class="listings">
			  <tr class="listingsHdr2">
			    <c:if test="${model.count > 0 and gSiteConfig['gGROUP_CUSTOMER']}">
			    <td align="left"><input type="checkbox" onclick="toggleAll(this)"></td>
			    </c:if>
			    <td class="indexCol">&nbsp;</td>
			    <td class="listingsHdr3">
			    	<table cellspacing="0" cellpadding="1">
			    	  <tr>
			    	    <c:choose>
			    	      <c:when test="${inActiveCustomerFilter.sort == 'last_name DESC, first_name DESC'}">
			    	        <td class="listingsHdr3">
			    	        <a class="hdrLink" href="#" onClick="document.getElementById('sort').value='last_name, first_name';document.getElementById('list').submit()"><fmt:message key="customer" /></a>
			    	        </td>
			    	        <td><img src="../graphics/up.gif" border="0"></td>
			    	      </c:when>
			    	      <c:when test="${inActiveCustomerFilter.sort == 'last_name, first_name'}">
			    	        <td class="listingsHdr3">
			    	        <a class="hdrLink" href="#" onClick="document.getElementById('sort').value='last_name DESC, first_name DESC';document.getElementById('list').submit()"><fmt:message key="customer" /></a>
			    	        </td>
			    	        <td><img src="../graphics/down.gif" border="0"></td>
			    	      </c:when>
			    	      <c:otherwise>
			    	        <td colspan="2" class="listingsHdr3">
			    	        <a class="hdrLink" href="#" onClick="document.getElementById('sort').value='last_name DESC, first_name DESC';document.getElementById('list').submit()"><fmt:message key="customer" /></a>
			    	        </td>
			    	      </c:otherwise>
			    	    </c:choose>  
			    	  </tr>
			    	</table>
			    </td>
			    <td align="left">
			    	<table cellspacing="0" cellpadding="1">
			    	  <tr>
			    	    <c:choose>
			    	      <c:when test="${inActiveCustomerFilter.sort == 'username DESC'}">
			    	        <td class="listingsHdr3">
			    	        <a class="hdrLink" href="#" onClick="document.getElementById('sort').value='username';document.getElementById('list').submit()"><fmt:message key="email" /></a>
			    	        </td>
			    	        <td><img src="../graphics/up.gif" border="0"></td>
			    	      </c:when>
			    	      <c:when test="${inActiveCustomerFilter.sort == 'username'}">
			    	        <td class="listingsHdr3">
			    	        <a class="hdrLink" href="#" onClick="document.getElementById('sort').value='username DESC';document.getElementById('list').submit()"><fmt:message key="email" /></a>
			    	        </td>
			    	        <td><img src="../graphics/down.gif" border="0"></td>
			    	      </c:when>
			    	      <c:otherwise>
			    	        <td colspan="2" class="listingsHdr3">
			    	        <a class="hdrLink" href="#" onClick="document.getElementById('sort').value='username DESC';document.getElementById('list').submit()"><fmt:message key="email" /></a>
			    	        </td>
			    	      </c:otherwise>
			    	    </c:choose>  
			    	  </tr>
			    	</table>
			    </td>
			    <td align="center">
			    	<table cellspacing="0" cellpadding="1">
			    	  <tr>
			    	    <c:choose>
			    	      <c:when test="${inActiveCustomerFilter.sort == 'created DESC'}">
			    	        <td class="listingsHdr3">
			    	        <a class="hdrLink" href="#" onClick="document.getElementById('sort').value='created';document.getElementById('list').submit()"><fmt:message key="date" /></a>
			    	        </td>
			    	        <td><img src="../graphics/up.gif" border="0"></td>
			    	      </c:when>
			    	      <c:when test="${inActiveCustomerFilter.sort == 'created'}">
			    	        <td class="listingsHdr3">
			    	        <a class="hdrLink" href="#" onClick="document.getElementById('sort').value='created DESC';document.getElementById('list').submit()"><fmt:message key="date" /></a>
			    	        </td>
			    	        <td><img src="../graphics/down.gif" border="0"></td>
			    	      </c:when>
			    	      <c:otherwise>
			    	        <td colspan="2" class="listingsHdr3">
			    	        <a class="hdrLink" href="#" onClick="document.getElementById('sort').value='created DESC';document.getElementById('list').submit()"><fmt:message key="date" /></a>
			    	        </td>
			    	      </c:otherwise>
			    	    </c:choose>  
			    	  </tr>
			    	</table>
			    </td>
				<td align="center">
			    	<table cellspacing="0" cellpadding="1">
			    	  <tr>
			    	    <c:choose>
			    	      <c:when test="${inActiveCustomerFilter.sort == 'num_of_logins DESC'}">
			    	        <td class="listingsHdr3">
			    	        <a class="hdrLink" href="#" onClick="document.getElementById('sort').value='num_of_logins';document.getElementById('list').submit()"><fmt:message key="numOfLogins" /></a>
			    	        </td>
			    	        <td><img src="../graphics/up.gif" border="0"></td>
			    	      </c:when>
			    	      <c:when test="${inActiveCustomerFilter.sort == 'num_of_logins'}">
			    	        <td class="listingsHdr3">
			    	        <a class="hdrLink" href="#" onClick="document.getElementById('sort').value='num_of_logins DESC';document.getElementById('list').submit()"><fmt:message key="numOfLogins" /></a>
			    	        </td>
			    	        <td><img src="../graphics/down.gif" border="0"></td>
			    	      </c:when>
			    	      <c:otherwise>
			    	        <td colspan="2" class="listingsHdr3">
			    	        <a class="hdrLink" href="#" onClick="document.getElementById('sort').value='num_of_logins DESC';document.getElementById('list').submit()"><fmt:message key="numOfLogins" /></a>
			    	        </td>
			    	      </c:otherwise>
			    	    </c:choose>  
			    	  </tr>
			    	</table>
			    </td>
			    <td align="center">
			    	<table cellspacing="0" cellpadding="1">
			    	  <tr>
			    	    <c:choose>
			    	      <c:when test="${inActiveCustomerFilter.sort == 'order_count DESC'}">
			    	        <td class="listingsHdr3">
			    	        <a class="hdrLink" href="#" onClick="document.getElementById('sort').value='order_count';document.getElementById('list').submit()"><fmt:message key="orderCount" /></a>
			    	        </td>
			    	        <td><img src="../graphics/up.gif" border="0"></td>
			    	      </c:when>
			    	      <c:when test="${inActiveCustomerFilter.sort == 'order_count'}">
			    	        <td class="listingsHdr3">
			    	        <a class="hdrLink" href="#" onClick="document.getElementById('sort').value='order_count DESC';document.getElementById('list').submit()"><fmt:message key="orderCount" /></a>
			    	        </td>
			    	        <td><img src="../graphics/down.gif" border="0"></td>
			    	      </c:when>
			    	      <c:otherwise>
			    	        <td colspan="2" class="listingsHdr3">
			    	        <a class="hdrLink" href="#" onClick="document.getElementById('sort').value='order_count DESC';document.getElementById('list').submit()"><fmt:message key="orderCount" /></a>
			    	        </td>
			    	      </c:otherwise>
			    	    </c:choose>  
			    	  </tr>
			    	</table>
			    </td>
			  <td align="center">
			    	<table cellspacing="0" cellpadding="1">
			    	  <tr>
			    	    <c:choose>
			    	      <c:when test="${inActiveCustomerFilter.sort == 'last_ordered_date DESC'}">
			    	        <td class="listingsHdr3">
			    	        <a class="hdrLink" href="#" onClick="document.getElementById('sort').value='last_ordered_date';document.getElementById('list').submit()"><fmt:message key="lastOrderedDate" /></a>
			    	        </td>
			    	        <td><img src="../graphics/up.gif" border="0"></td>
			    	      </c:when>
			    	      <c:when test="${inActiveCustomerFilter.sort == 'last_ordered_date'}">
			    	        <td class="listingsHdr3">
			    	        <a class="hdrLink" href="#" onClick="document.getElementById('sort').value='last_ordered_date DESC';document.getElementById('list').submit()"><fmt:message key="lastOrderedDate" /></a>
			    	        </td>
			    	        <td><img src="../graphics/down.gif" border="0"></td>
			    	      </c:when>
			    	      <c:otherwise>
			    	        <td colspan="2" class="listingsHdr3">
			    	        <a class="hdrLink" href="#" onClick="document.getElementById('sort').value='last_ordered_date DESC';document.getElementById('list').submit()"><fmt:message key="lastOrderedDate" /></a>
			    	        </td>
			    	      </c:otherwise>
			    	    </c:choose>  
			    	  </tr>
			    	</table>
			    </td> 
			    <td align="center">
			    	<table cellspacing="0" cellpadding="1">
			    	  <tr>
			    	    <c:choose>
			    	      <c:when test="${inActiveCustomerFilter.sort == 'average_order DESC'}">
			    	        <td class="listingsHdr3">
			    	        <a class="hdrLink" href="#" onClick="document.getElementById('sort').value='average_order';document.getElementById('list').submit()"><fmt:message key="averageOrder" /></a>
			    	        </td>
			    	        <td><img src="../graphics/up.gif" border="0"></td>
			    	      </c:when>
			    	      <c:when test="${inActiveCustomerFilter.sort == 'average_order'}">
			    	        <td class="listingsHdr3">
			    	        <a class="hdrLink" href="#" onClick="document.getElementById('sort').value='average_order DESC';document.getElementById('list').submit()"><fmt:message key="averageOrder" /></a>
			    	        </td>
			    	        <td><img src="../graphics/down.gif" border="0"></td>
			    	      </c:when>
			    	      <c:otherwise>
			    	        <td colspan="2" class="listingsHdr3">
			    	        <a class="hdrLink" href="#" onClick="document.getElementById('sort').value='average_order DESC';document.getElementById('list').submit()"><fmt:message key="averageOrder" /></a>
			    	        </td>
			    	      </c:otherwise>
			    	    </c:choose>  
			    	  </tr>
			    	</table>
			    </td>
			    <td align="center">
			    	<table cellspacing="0" cellpadding="1">
			    	  <tr>
			    	    <c:choose>
			    	      <c:when test="${inActiveCustomerFilter.sort == 'grand_total DESC'}">
			    	        <td class="listingsHdr3">
			    	        <a class="hdrLink" href="#" onClick="document.getElementById('sort').value='grand_total';document.getElementById('list').submit()"><fmt:message key="orderTotal" /></a>
			    	        </td>
			    	        <td><img src="../graphics/up.gif" border="0"></td>
			    	      </c:when>
			    	      <c:when test="${inActiveCustomerFilter.sort == 'grand_total'}">
			    	        <td class="listingsHdr3">
			    	        <a class="hdrLink" href="#" onClick="document.getElementById('sort').value='grand_total DESC';document.getElementById('list').submit()"><fmt:message key="orderTotal" /></a>
			    	        </td>
			    	        <td><img src="../graphics/down.gif" border="0"></td>
			    	      </c:when>
			    	      <c:otherwise>
			    	        <td colspan="2" class="listingsHdr3">
			    	        <a class="hdrLink" href="#" onClick="document.getElementById('sort').value='grand_total DESC';document.getElementById('list').submit()"><fmt:message key="orderTotal" /></a>
			    	        </td>
			    	      </c:otherwise>
			    	    </c:choose>  
			    	  </tr>
			    	</table>
			    </td>
			    <td align="left" class="listingsHdr3"><fmt:message key="address" /></td>
			    <td align="center" class="listingsHdr3"><fmt:message key="phone" /></td>
			    <td align="left" class="listingsHdr3"><fmt:message key="salesRep" /></td>
			  </tr>
		  	  <c:forEach items="${model.inactiveCustomerReports}" var="inactiveCustomer"	varStatus="status">
			  <tr class="row${status.index % 2}" onmouseover="changeStyleClass(this,'rowOver')" onmouseout="changeStyleClass(this,'row${status.index % 2}')">
			    <c:if test="${model.count > 0 and gSiteConfig['gGROUP_CUSTOMER']}">
			    <td align="left"><input name="__selected_id" value="${inactiveCustomer.userId}" type="checkbox"></td>
			    </c:if>
			    <td align="center">
			    <img class="action" width="23" height="16" alt="Actions" src="../graphics/actions.png"/>
			    <div class="action-hover" style="display: none;">
			        <ul class="round">
			            <li class="action-header">
					      <div class="name"><c:out value="${inactiveCustomer.address.firstName}"/> <c:out value="${inactiveCustomer.address.lastName}"/></div>
					      <div class="menudiv"></div>
					      <div style="position:relative">
					      <span style="width:16px;float:left;"><a href="../customers/customerQuickView.jhtm?cid=${inactiveCustomer.userId}" rel="width:900,height:400" id="mb${inactiveCustomer.userId}" class="mbCustomer" title="<fmt:message key="customer" />ID : ${inactiveCustomer.userId}"><img src="../graphics/magnifier.gif" border="0" /></a></span>
					      <c:if test="${gSiteConfig['gCRM'] and inactiveCustomer.crmContactId != null}">
					        <span style="width:16px;float:left;"><a href="../crm/crmContactQuickView.jhtm?id=${inactiveCustomer.crmContactId}" rel="width:900,height:400" id="mb${inactiveCustomer.crmContactId}" class="mbCustomer" title="<fmt:message key="crmContact" />ID : ${inactiveCustomer.crmContactId}"><img src="../graphics/magnifierCrm.gif" border="0" /></a></span>
					      </c:if>
					        <c:if test="${gSiteConfig['gADD_INVOICE']}">
					          <span style="width:16px;float:left;"><a href="../orders/addInvoice.jhtm?cid=${inactiveCustomer.userId}"><img src="../graphics/add.png" alt="Add Invoice" title="Add Invoice" border="0"></a></span>
					        </c:if>
					        <c:if test="${gSiteConfig['gTICKET']}">
					          <span style="width:16px;float:left;"><a href="../ticket/ticketForm.jhtm?cid=${inactiveCustomer.userId}"><img src="../graphics/ticket_small.gif" alt="Add Ticket" title="Add Ticket" border="0"></a></span>
					        </c:if>
					      </div>  
			            </li>
			            <sec:authorize ifAnyGranted="ROLE_AEM,ROLE_ADMIN_${prop_site_id},ROLE_CUSTOMER_EDIT">
				          <li class="edit"><a href="../customers/customer.jhtm?id=${inactiveCustomer.userId}"><fmt:message key="edit" /></a></li>
				        </sec:authorize>
			            <li class="edit"><a href="../customers/addressList.jhtm?id=${inactiveCustomer.userId}"><fmt:message key="addressList" /></a></li>
				        <li class="edit"><a href="../customers/email.jhtm?id=${inactiveCustomer.userId}"><fmt:message key="sendEmail" /></a></li>
				        <sec:authorize ifAnyGranted="ROLE_AEM,ROLE_ADMIN_${prop_site_id},ROLE_CUSTOMER_ADD_INVOICE">
			            <c:if test="${gSiteConfig['gADD_INVOICE']}">
			              <li class="edit"><a href="../orders/addInvoice.jhtm?cid=${inactiveCustomer.userId}"><fmt:message key="addOrder" /></a></li>
			            </c:if>  
			            </sec:authorize>
			            <c:if test="${gSiteConfig['gPAYMENTS']}">
			              <li class="edit"><a href="../customers/payments.jhtm?cid=${inactiveCustomer.userId}"><fmt:message key="payments" /></a></li>       
			            </c:if>
			            <sec:authorize ifAnyGranted="ROLE_AEM,ROLE_ADMIN_${prop_site_userId},ROLE_CUSTOMER_MYLIST_ADD,ROLE_CUSTOMER_MYLIST_DELETE,ROLE_CUSTOMER_MYLIST_VIEW">
			            <c:if test="${gSiteConfig['gMYLIST'] == 'true'}">
			              <li class="edit"><a href="../customers/myList.jhtm?id=${inactiveCustomer.userId}"><fmt:message key="myList" /></a></li>
			            </c:if>
			            </sec:authorize>
			            <c:if test="${gSiteConfig['gSPECIAL_PRICING'] == 'true'}">
			              <li class="edit"><a href="../customers/specialPricing.jhtm?id=${inactiveCustomer.userId}"><fmt:message key="specialPricing" /></a></li>
			            </c:if>
			            <c:if test="${gSiteConfig['gBUDGET_BRAND']}">
			              <li class="edit"><a href="../customers/budgetByBrands.jhtm?id=${inactiveCustomer.userId}"><fmt:message key="budgetByBrands" /></a></li>
			            </c:if>
			            <c:if test="${gSiteConfig['gBUDGET_PRODUCT']}">
			              <li class="edit"><a href="budgetByProducts.jhtm?cid=${inactiveCustomer.userId}"><fmt:message key="budgetByProducts" /></a></li>
			            </c:if>
			            <c:if test="${gSiteConfig['gEVENT'] and !inactiveCustomer.suspendedEvent}">
			              <li class="edit"><a href="../event/registerEventMember.jhtm?id=${inactiveCustomer.userId}" rel="width:400,height:380"  id="mb${inactiveCustomer.userId}" class="mbRegisterEvent">Register to Event</a></li>
			            </c:if>
			            <c:if test="${gSiteConfig['gCRM'] && inactiveCustomer.crmContactId != null}">
			              <li class="edit"><a href="../crm/crmTaskForm.jhtm?accountId=${inactiveCustomer.crmAccountId}&contactId=${inactiveCustomer.crmContactId}"><fmt:message key="addTask" /></a></li>
			            </c:if>
			        </ul>
			    </div>
				</td>
				<td class="nameCol"><a href="../customers/customer.jhtm?id=<c:out value="${inactiveCustomer.userId}" />"><c:out value="${inactiveCustomer.firstName}" /> <c:out value="${inactiveCustomer.lastName}" /></a></td>			
			    <td class="nameCol"><c:out value="${inactiveCustomer.username}" /></td>
			    <td style="white-space: nowrap;" align="center"><fmt:formatDate type="date" timeStyle="default" value="${inactiveCustomer.customerCreated}"/></td>				
				<td align="center"><c:out value="${inactiveCustomer.numOfLogins}" /></td>
				<td align="center"><c:out value="${inactiveCustomer.orderCount}" /></td>
				<td style="white-space: nowrap;" align="center"><fmt:formatDate type="date" timeStyle="default" value="${inactiveCustomer.lastOrderedDate}"/></td>				
				<td align="right" style="padding-right: 10px"><fmt:message key="${siteConfig['CURRENCY'].value}" /><fmt:formatNumber value="${inactiveCustomer.averageOrder}" pattern="#,##0.00" /></td>
			   	<td align="right" style="padding-right: 10px"><fmt:message key="${siteConfig['CURRENCY'].value}" /><fmt:formatNumber value="${inactiveCustomer.ordersGrandTotal}" pattern="#,##0.00" /></td><c:set var="totalSubTotal" value="${order.subTotal + totalSubTotal}" />
				<td align="left"><c:out value="${inactiveCustomer.addressToString}" /></td>
				<td align="left"><c:out value="${inactiveCustomer.address.phone}"/></td>    
				<td align="left"><c:out value="${model.salesRepMap[inactiveCustomer.salesRepId].name}"/></td>
			  </tr>
			</c:forEach>
			</table>
			
			<table border="0" cellpadding="0" cellspacing="1" width="100%">
			  <tr> 
			  <td class="pageSize">
			    <select name="size" onchange="document.getElementById('page').value=1;submit()">
			    <c:forTokens items="10,25,50,100,500" delims="," var="current">
			  	  <option value="${current}" <c:if test="${current == model.inActiveCustomerFilter.pageSize}">selected</c:if>>${current} <fmt:message key="perPage" /></option>
			    </c:forTokens>
			    </select>
			  </td>
			  </tr>
			</table>   		  
		    <!-- end input field -->
		  	</div>
		  	
		  	<!-- start button -->
		  	<div align="left" class="button">
		  	</div>
			<!-- end button -->	
      	  	
	<!-- end tab -->        
	</div>
<!-- end tabs -->			
</div>
  
  <!-- end table -->
   </td><td class="boxmidr" ></td></tr>
  <tr><td class="botl"></td><td class="botr"></td></tr>
  </table>
  
<!-- end main box -->  
</div>
	 </form> 
</sec:authorize>

  </tiles:putAttribute>
</tiles:insertDefinition>
