<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/security/tags" prefix="sec" %>

<tiles:insertDefinition name="admin.report.orders" flush="true">
  <tiles:putAttribute name="content" type="string">
<c:if test="${gSiteConfig['gREPORT']}">
<sec:authorize ifAnyGranted="ROLE_AEM,ROLE_ADMIN_${prop_site_id},ROLE_REPORT_SHIPPED_ORDER,ROLE_REPORT_ORDER_NEED_ATTENTION,ROLE_REPORT_SHIPPED_OVERVIEW,ROLE_REPORT_ORDER_PEN_PROC,ROLE_REPORT_SHIPPED_DAILY,ROLE_REPORT_AFFILIATE_COMMISSION">
<script type="text/javascript">
<!--

//-->
</script>   
<!-- main box -->
  <div id="mboxfull">
  
  <!-- start table -->
  <table cellpadding="0" cellspacing="0" border="0" class="module" >
  <tr>
    <td class="topl_g">
	  
	  <!-- breadcrumb -->
	  <p class="breadcrumb">
	    <a href="../reports"><fmt:message key="reports" /></a> &gt;
	    <fmt:message key="orders" />
	  </p>
	  
	  <!-- Error -->
	  <c:if test="${!empty model.message}">
		<div class="message"><fmt:message key="${model.message}" /></div>
	  </c:if>
	  
	  <!-- header image -->
	  <img class="headerImage" src="../graphics/reports.gif" /> 
	  
  </td><td class="topr_g" ></td></tr>
  <tr><td class="boxmidlrg" >

 <!-- tabs -->
 <div class="tab-wrapper">
	<h4 title=""></h4>
	<div>
	<!-- start tab -->
        <form action="" method="post" >

	  	<div class="listdivi"></div>
	    
		  	<div class="listlight">
		  	<!-- input field -->
		  	    
		  		<table border="0" width="100%">
		  		  <tr>
		  		   <td valign="middle">
		  		    <div align="center" style="width: 48%;float: left; height: auto;">
		  		     <div style="width: 85%;">
		  		      <div style="border:1px solid #999999;background-color:#666666;float:left;width: 100%;height:30px;">
						<h2>
						<span> <c:out value="${model.shipOrderReportFilter.year}"/><fmt:message key="ordersOverview"/></span>
						</h2>
						<div style="position:relative;float:left;top:-22px;">
						 <select name="_dateType_shipped_order" onchange="submit();">
						  <option value="orderDate">Order Date</option>
						  <option value="shipDate" <c:if test="${model.shipOrderReportFilter.dateType == 'shipDate'}">selected="selected"</c:if>>Ship Date</option>
						 </select>
						</div>
						<div style="position:relative;float:right;top:-22px;">
						 <input type="text" name="_year_shipped_order" maxlength="4" value="${model.shipOrderReportFilter.year}" style="width:50px"/>
						</div>
					  </div>
					  <div style="border:1px solid #dddddd;height: 340px;">
					  <sec:authorize ifAnyGranted="ROLE_AEM,ROLE_ADMIN_${prop_site_id},ROLE_REPORT_SHIPPED_ORDER">
						<img alt="Blue line chart with alternating gray and white stripes from left to right" 
						src="http://chart.apis.google.com/chart?
						cht=bvs
						&chd=<c:out value="${model.saleReportsGraph[0]}" />
						&chco=0000FF
						&chls=2.0,1.0,0.0
						&chxt=x,y
						&chg=8.33,25
						&chxl=0:|Jan|Feb|Mar|Apr|May|Jun|Jul|Aug|Sep|Oct|Nov|Dec|1:<c:out value="${model.saleReportsGraph[1]}" />
						&chs=400x200"/>
					  </sec:authorize>  
					  </div>
					  <div style="height: 30px;border:1px solid #bbbbbb;background-color:#e5e5e5;padding:5px 0 5px 0; text-align:left;">
						<span style="padding:0 0 0 10px;"> <!--  view report --> </span>
					  </div>
					 </div>
		  		    </div>
		  		    
		  		    <div align="center" style="width: 48%;float: right">
		  		     <div style="width: 85%;">
		  		      <div style="border:1px solid #999999;background-color:#666666;">
						<h2>
						<span> <c:out value="${model.shipOrderReportFilter.year}"/> Orders Overview</span>
						</h2>
					  </div>
					  <div class="reportItems">
					  <sec:authorize ifAnyGranted="ROLE_AEM,ROLE_ADMIN_${prop_site_id},ROLE_REPORT_SHIPPED_OVERVIEW">
					    <table height="400px">
					     <thead>
					      <tr>
					      	<th class="text">
					          <div class="text_wrapper">
								<div class="text_wrapper"><fmt:message key="month" /></div>
							  </div>
							</th>
							<th class="text">
					          <div class="text_wrapper">
								<div class="text_wrapper"><fmt:message key="grandTotal" /></div>
							  </div>
							</th>
							<th class="text">
					          <div class="text_wrapper">
								<div class="text_wrapper"># Orders</div>
							  </div>
							</th>
							<th class="text">
					          <div class="text_wrapper">
								<div class="text_wrapper">Unique User</div>
							  </div>
							</th>
					      </tr>
					     </thead>
					     <tbody> 
					     <c:set var="quarterIndex" value="0"/>
					     <c:forEach items="${model.salesReportsList}" var="saleReport" varStatus="status">
					     <c:if test="${status.index % 3 == 0}">
					       <c:set var="quarter" value="0"/>
					       <c:set var="overviewNumOrder" value="0"/>
					       <c:set var="overviewUniqUser" value="0"/>
					     </c:if>
					      <tr>
					        <td class="text">
					          <div class="text_wrapper">
								<div class="text_wrapper"><c:out value="${saleReport.month}"/></div>
							  </div>
							</td>
							<td class="text">
					          <div class="text_wrapper">
								<div class="text_wrapper"><fmt:message key="${siteConfig['CURRENCY'].value}" /><fmt:formatNumber value="${saleReport.grandTotal}" pattern="#,##0.00" /></div>
							  </div><c:set var="quarter" value="${saleReport.grandTotal + quarter}" />
							</td>
							<td class="text">
					          <div class="text_wrapper">
								<div class="text_wrapper"><c:out value="${saleReport.numOrder}"/></div><c:set var="overviewNumOrder" value="${saleReport.numOrder + overviewNumOrder}" />
							  </div>
							</td>
							<td class="text">
					          <div class="text_wrapper">
								<div class="text_wrapper"><c:out value="${saleReport.uniqueUser}"/></div><c:set var="overviewUniqUser" value="${saleReport.uniqueUser + overviewUniqUser}" />
							  </div>
							</td>
					      </tr>
					      <c:if test="${status.index % 3 == 2}">
					      <c:set var="quarterIndex" value="${quarterIndex + 1}" />
					      <tr style="font-weight:700;">
					        <td class="text">
					          <div class="text_wrapper">
								<div class="text_wrapper">Q<c:out value="${quarterIndex}" /></div>
							  </div>
							</td>
							<td class="text">
					          <div class="text_wrapper">
								<div class="text_wrapper"><fmt:message key="${siteConfig['CURRENCY'].value}" /><fmt:formatNumber value="${quarter}" pattern="#,##0.00" /></div>
							  </div>
							</td>
							<td class="text">
					          <div class="text_wrapper">
								<div class="text_wrapper"><c:out value="${overviewNumOrder}" /></div>
							  </div>
							</td>
							<td class="text">
					          <div class="text_wrapper">
								<div class="text_wrapper"><c:out value="${overviewUniqUser}" /></div>
							  </div>
							</td>
					      </tr>
					      </c:if>
					     </c:forEach> 
						 </tbody>
					  </table> 
					 </sec:authorize>  
					 </div>
					 <div style="height: 30px;border:1px solid #bbbbbb;background-color:#e5e5e5;padding:5px 0 5px 0; text-align:left;">
					  <span style="padding:0 0 0 10px;"> <!--  view report --> </span>
					 </div>
					</div>
		  		    </div>
		  		   </td>
		  		  </tr>
		  		</table>
		  		  
		  		<table border="0" width="100%">
		  		  <tr>
		  		   <td>
		  		    <div align="center" style="width: 48%;float: left;">
					  <div style="width: 85%;">
					   <div style="border:1px solid #999999;background-color:#666666;">
						<h2>
						<span> Pending &amp; Processing orders </span>
						</h2>
					   </div>
					   <div style="border:1px solid #dddddd;height: 395px;">
					   <sec:authorize ifAnyGranted="ROLE_AEM,ROLE_ADMIN_${prop_site_id},ROLE_REPORT_ORDER_PEN_PROC">
						<img alt="Google-o-meter with default red to green coloring" 
						src="http://chart.apis.google.com/chart?
						chs=285x165
						&cht=gom
						&chd=t:<c:out value="${model.openOrder}" />
						&chl=<c:out value="${model.openOrder}" />
						&chco=00ff00,FFFF00,ff0000"/>
					   </sec:authorize>	
					   </div>
					   <div style="height: 30px;border:1px solid #bbbbbb;background-color:#e5e5e5;padding:5px 0 5px 0; text-align:left;">
						<span style="padding:0 0 0 10px;"> <!--  view report --> </span>
					   </div>
					  </div> 
		  		    </div>
		  		    <div align="center" style="width: 48%;float: right">
		  		      <div style="width: 85%;">
		  		       <div style="border:1px solid #999999;background-color:#666666;">
						<h2>
						<span> ORDERS NEED ATTENTION </span><a style="font-size:10px; color:#EEEEEE; float:right" href="../orders/ordersList.jhtm?status=ppr">view more</a>
						</h2>
					   </div>
					   <div id="tableContainer" class="tableContainer">
					   <sec:authorize ifAnyGranted="ROLE_AEM,ROLE_ADMIN_${prop_site_id},ROLE_REPORT_ORDER_NEED_ATTENTION">
						 <table>
						     <thead class="fixedHeader">
						      <tr>
						      	<th class="text">
						          <div class="text_wrapper">
									<div class="text_wrapper">Date</div>
								  </div>
								</th>
								<th class="text">
						          <div class="text_wrapper">
									<div class="text_wrapper"><fmt:message key="grandTotal" /></div>
								  </div>
								</th>
								<th class="text">
						          <div class="text_wrapper">
									<div class="text_wrapper"><fmt:message key="orderNumber" /></div>
								  </div>
								</th>
								<c:if test="${gSiteConfig['gSALES_REP']}">
								<th class="text">
						          <div class="text_wrapper">
									<div class="text_wrapper"><fmt:message key="salesRep" /></div>
								  </div>
								</th>
								</c:if>
								<th class="text">
						          <div class="text_wrapper">
									<div class="text_wrapper"><fmt:message key="status" /></div>
								  </div>
								</th>
						      </tr>
						     </thead>
						     <tbody class="scrollContent">
						     <c:set var="totalGrandTotal1" value="0.0" />
						     <c:forEach items="${model.penProcOrderReport}" var="penProcOrderReport" varStatus="status">
						      <tr>
						        <td class="text">
						          <div class="text_wrapper">
									<div class="text_wrapper"><fmt:formatDate type="date" value="${penProcOrderReport.dateOrdered}"/></div>
								  </div>
								</td>
								<td class="text">
						          <div class="text_wrapper">
									<div class="text_wrapper"><fmt:message key="${siteConfig['CURRENCY'].value}" /><fmt:formatNumber value="${penProcOrderReport.grandTotal}" pattern="#,##0.00" /></div>
								  </div><c:set var="totalGrandTotal1" value="${penProcOrderReport.grandTotal + totalGrandTotal1}" />
								</td>
								<td class="text">
						          <div class="text_wrapper">
									<div class="text_wrapper"><a href="../orders/invoice.jhtm?order=${penProcOrderReport.orderId}" class="nameLink"><c:out value="${penProcOrderReport.orderId}"/></a></div>
								  </div>
								</td>
								<c:if test="${gSiteConfig['gSALES_REP']}">
								<td class="text">
						          <div class="text_wrapper">
									<div class="text_wrapper"><c:out value="${model.salesRepMap[penProcOrderReport.salesRepId].name}"/></div>
								  </div>
								</td>
								</c:if>
								<td class="text">
						          <div class="text_wrapper">
									<div class="text_wrapper"><c:choose><c:when test="${penProcOrderReport.status != null}"><fmt:message key="orderStatus_${penProcOrderReport.status}" /></c:when><c:otherwise>&nbsp;</c:otherwise></c:choose></div>
								  </div>
								</td>
						      </tr>	
						     </c:forEach> 
						     <tr class="totals">
								 <td style="color:#666666; padding: 5px" align="left"><fmt:message key="total" /></td>
								 <td class="numberCol"><fmt:formatNumber value="${totalGrandTotal1}" pattern="#,##0.00" /></td>
							  </tr> 
						     </tbody>
						   </table>
				       </sec:authorize>		   
				       </div>
					   <div style="height: 30px;border:1px solid #bbbbbb;background-color:#e5e5e5;padding:5px 0 5px 0; text-align:left;">
						  <span style="padding:0 0 0 10px;"> <!--  view report --> </span>
				       </div>
				      </div> 
		  		    </div>
		  		   </td>
		  		  </tr>
		  		</table>

		  		<table border="0" width="100%">
		  		  <tr>
		  		   <td>
		  		    <div align="center" style="width: 48%;float: left;">
					  <div style="width: 85%;">
					  <c:if test="${gSiteConfig['gAFFILIATE'] > 0}">
					   <div style="border:1px solid #999999;background-color:#666666;">
						<h2>
						<span> Affiliate Commission Overview </span>
						</h2>
					   </div>
					   <div id="tableContainer" class="tableContainer">
					   <sec:authorize ifAnyGranted="ROLE_AEM,ROLE_ADMIN_${prop_site_id},ROLE_REPORT_AFFILIATE_COMMISSION">
						   <table>
						     <thead class="fixedHeader">
						      <tr>
						      	<th class="text">
						          <div class="text_wrapper">
									<div class="text_wrapper"><fmt:message key="Name" /></div>
								  </div>
								</th>
								<th class="text">
						          <div class="text_wrapper">
									<div class="text_wrapper"><fmt:message key="total" /></div>
								  </div>
								</th>
								<th class="text">
						          <div class="text_wrapper">
									<div class="text_wrapper">Due Total</div>
								  </div>
								</th>
								<th class="text">
						          <div class="text_wrapper">
									<div class="text_wrapper">Paid Total</div>
								  </div>
								</th>
								<th class="text">
						          <div class="text_wrapper">
									<div class="text_wrapper"># Orders</div>
								  </div>
								</th>
						      </tr>
						     </thead>
						     <tbody class="scrollContent">
						     <c:forEach items="${model.affiliateReport}" var="affiliateReport" varStatus="status">
						      <tr>
						        <td class="text">
						          <div class="text_wrapper">
									<div class="text_wrapper"><a href="../customers/customer.jhtm?id=${affiliateReport.userId}"><c:out value="${affiliateReport.firstName}"/>&nbsp;<c:out value="${affiliateReport.lastName}"/></a></div>
								  </div>
								</td>
								<td class="text">
						          <div class="text_wrapper">
									<div class="text_wrapper"><fmt:message key="${siteConfig['CURRENCY'].value}" /><fmt:formatNumber value="${affiliateReport.commissionTotal}" pattern="#,##0.00" /></div>
								  </div>
								</td>
								<td class="text">
						          <div class="text_wrapper">
									<div class="text_wrapper"><fmt:message key="${siteConfig['CURRENCY'].value}" /><fmt:formatNumber value="${affiliateReport.commissionDueTotal}" pattern="#,##0.00" /></div>
								  </div>
								</td>
								<td class="text">
						          <div class="text_wrapper">
									<div class="text_wrapper"><fmt:message key="${siteConfig['CURRENCY'].value}" /><fmt:formatNumber value="${affiliateReport.commissionPaidTotal}" pattern="#,##0.00" /></div>
								  </div>
								</td>
								<td class="text">
						          <div class="text_wrapper">
									<div class="text_wrapper"><c:out value="${affiliateReport.numOrder}"/></div>
								  </div>
								</td>
						      </tr>	
						     </c:forEach> 
						     </tbody>
						   </table>
					   </sec:authorize>	
					   </div>
					   <div style="height: 30px;border:1px solid #bbbbbb;background-color:#e5e5e5;padding:5px 0 5px 0; text-align:left;">
						<span style="padding:0 0 0 10px;"> <!--  view report --> </span>
					   </div>
					   </c:if>
					  </div> 
		  		    </div>
		  		    
		  		    <div align="center" style="width: 48%;float: right">
		  		     <div style="width: 85%;">
		  		      <div style="border:1px solid #999999;background-color:#666666;float:left;width: 100%;height:30px;">
						<h2>
						<span> <c:out value="${model.shipOrderReportFilter.year}"/> Orders daily </span>
						</h2>
						<div style="position:relative;float:left;top:-22px;">
						 <select name="_dateType_shippedDaily_order" onchange="submit();">
						  <option value="orderDate">Order Date</option>
						  <option value="shipDate" <c:if test="${model.shipDailyOrderReportFilter.dateType == 'shipDate'}">selected="selected"</c:if>>Ship Date</option>
						 </select>
						</div>
					  </div>
					  <div id="tableContainer" class="tableContainer">
					  <sec:authorize ifAnyGranted="ROLE_AEM,ROLE_ADMIN_${prop_site_id},ROLE_REPORT_SHIPPED_DAILY">
						   <table>
						     <thead class="fixedHeader">
						      <tr>
						      	<th class="text">
						          <div class="text_wrapper">
									<div class="text_wrapper"><fmt:message key="date" /></div>
								  </div>
								</th>
								<th class="text">
						          <div class="text_wrapper">
									<div class="text_wrapper"><fmt:message key="grandTotal" /></div>
								  </div>
								</th>
								<th class="text">
						          <div class="text_wrapper">
									<div class="text_wrapper"># Orders</div>
								  </div>
								</th>
								<th class="text">
						          <div class="text_wrapper">
									<div class="text_wrapper">Unique User</div>
								  </div>
								</th>
						      </tr>
						     </thead>
						     <tbody class="scrollContent">
						     <c:set var="totalGrandTotal2" value="0.0" />
			  				 <c:set var="totalOrders" value="0" />
			  				 <c:set var="totalUniqueUser" value="0" />
						     <c:forEach items="${model.salesReportsListDaily}" var="dayReport" varStatus="status">
						      <tr>
						        <td class="text">
						          <div class="text_wrapper">
									<div class="text_wrapper"><fmt:formatDate type="date" value="${dayReport.date}"/></div>
								  </div>
								</td>
								<td class="text">
						          <div class="text_wrapper">
									<div class="text_wrapper"><fmt:message key="${siteConfig['CURRENCY'].value}" /><fmt:formatNumber value="${dayReport.grandTotal}" pattern="#,##0.00" /></div>
								  </div><c:set var="totalGrandTotal2" value="${dayReport.grandTotal + totalGrandTotal2}" />
								</td>
								<td class="text">
						          <div class="text_wrapper">
									<div class="text_wrapper"><c:out value="${dayReport.numOrder}"/></div>
								  </div><c:set var="totalOrders" value="${dayReport.numOrder + totalOrders}" />
								</td>
								<td class="text">
						          <div class="text_wrapper">
									<div class="text_wrapper"><c:out value="${dayReport.uniqueUser}"/></div>
								  </div><c:set var="totalUniqueUser" value="${dayReport.uniqueUser + totalUniqueUser}" />
								</td>
						      </tr>	
						     </c:forEach>
						      <tr class="totals">
								 <td style="color:#666666; padding: 5px" align="left"><fmt:message key="total" /></td>
								 <td style="color:#666666; padding: 5px" align="right"><fmt:formatNumber value="${totalGrandTotal2}" pattern="#,##0.00" /></td>
								 <td style="color:#666666; padding: 5px" align="right"><c:out value="${totalOrders}" /></td>
								 <td style="color:#666666; padding: 5px" align="right"><c:out value="${totalUniqueUser}" /></td>
							  </tr> 
						     </tbody>
						   </table>
						</sec:authorize>    
						</div>
						<div style="height: 30px;border:1px solid #bbbbbb;background-color:#e5e5e5;padding:5px 0 5px 0; text-align:left;">
						  <span style="padding:0 0 0 10px;"> <!--  view report --> </span>
						</div>
					  </div>	
		  		    </div>
		  		   </td>
		  		  </tr>
		  		</table>

		    <!-- end input field -->  	  
		  	</div>
		  	
		  	<!-- start button -->
			<!-- end button -->	
       	  	</form>
	<!-- end tab -->        
	</div>
<!-- end tabs -->			
</div>
  
  <!-- end table -->
   </td><td class="boxmidr" ></td></tr>
  <tr><td class="botl"></td><td class="botr"></td></tr>
  </table>
  
<!-- end main box -->  
</div>
</sec:authorize>
</c:if>
  </tiles:putAttribute>
</tiles:insertDefinition>