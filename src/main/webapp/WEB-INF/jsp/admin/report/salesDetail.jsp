<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/security/tags" prefix="sec" %>

<tiles:insertDefinition name="admin.report.orderDetail" flush="true">
  <tiles:putAttribute name="content" type="string">
<c:if test="${gSiteConfig['gREPORT']}">
<sec:authorize ifAnyGranted="ROLE_AEM,ROLE_ADMIN_${prop_site_id},ROLE_REPORT_ORDER_OVERVIEW">
<script type="text/javascript">
<!--
window.addEvent('domready', function(){			
	$$('span.toolTip').each(function(element,index) {  
	         var content = element.get('title').split('::');  
	         element.store('tip:title', content[0]);  
	         element.store('tip:text', content[1]);  
	     	});
	var Tips1 = new Tips($$('.toolTip'));
	<sec:authorize ifAnyGranted="ROLE_AEM,ROLE_ADMIN_${prop_site_id},ROLE_QUICK_EXPORT">
	$('quickModeTrigger').addEvent('click',function() {
	    $('mboxfull').setStyle('margin', '0');
	    $$('.quickMode').each(function(el){
	    	el.hide();
	    });
	    $$('.quickModeRemove').each(function(el){
	    	el.destroy();
	    });
	    $$('.totals').each(function(el){
	    	el.destroy();
	    });
	    $$('.listingsHdr3').each(function(el){
	    	el.removeProperties('colspan', 'class');
	    });
	    alert('Select the whole page, copy and paste to your excel file.');	   
	 });
	 </sec:authorize> 
});
//-->
</script> 
<form name="customerList" id="list" action="">
<input type="hidden" id="sort" name="sort" value="${salesRepDetailFilter.sort}" />  
<!-- main box -->
  <div id="mboxfull">
  
  <!-- start table -->
  <table cellpadding="0" cellspacing="0" border="0" class="module" >
  <tr class="quickMode">
    <td class="topl_g">
	  
	  <!-- breadcrumb -->
	  <p class="breadcrumb">
	    <a href="../reports"><fmt:message key="reports" /></a> &gt;
	    <fmt:message key="ordersOverview"/>
	  </p>
	  
	  <!-- Error -->
	  <c:if test="${!empty model.message}">
		<div class="message"><fmt:message key="${model.message}" /></div>
	  </c:if>
	  
	  <!-- header image -->
	  <img class="headerImage" id="quickModeTrigger" src="../graphics/reports.gif" /> 
	  
  </td><td class="topr_g" ></td></tr>
  <tr><td class="boxmidlrg" >

 <!-- tabs -->
 <div class="tab-wrapper">
	<h4 title=""></h4>
	<div>
	<!-- start tab -->

	  	<div class="listdivi"></div>
	    
		  	<div class="listlight">
		  	<!-- input field -->
		  	    
		  	<table border="0" cellpadding="2" cellspacing="0" width="100%" class="listings">
			  <tr class="listingsHdr2">
			      <td class="listingsHdr3"><fmt:message key="month" /></td>
			      <td class="listingsHdr3"><span class="toolTip" title="<fmt:message key="merchandise" />::GrandTotal - ( ShippingCost + Tax + Credit Card Fee )"><fmt:message key="merchandize" /></span></td>
			      <td class="listingsHdr3"><fmt:message key="shipping" /></td>
			      <td class="listingsHdr3"><fmt:message key="ccFee" /></td>
			      <td class="listingsHdr3"><fmt:message key="tax" /></td>
			      <td class="listingsHdr3"><fmt:message key="grandTotal" /></td>
			      <td class="listingsHdr3"><fmt:message key="orders" /></td>
			      <td class="listingsHdr3"><fmt:message key="uniqueUser" /></td>
			      <td class="listingsHdr3"><fmt:message key="newUser" /></td>
			      <td class="listingsHdr3"><fmt:message key="newUserActive" /></td>
			      <td class="listingsHdr3"><fmt:message key="averagePerOrder" /></td>
			      <td class="listingsHdr3"><fmt:message key="averagePerCustomer" /></td>
			      <td class="listingsHdr3"><span class="toolTip" title="Activated::# of customers who purchased again this period who had not purchased for the previous 30 days">Activated</span></td>
			      <td class="listingsHdr3"><span class="toolTip" title="Retained::# of customers who purchased in the preceding 30 days who purchased again in the current period">Retained</span></td>
			  </tr>
			    <c:set var="merchandizeTotal" value="0"/>
			    <c:set var="shippingCostTotal" value="0"/>
			    <c:set var="ccFeeTotal" value="0"/>
			    <c:set var="taxTotal" value="0"/>
			    <c:set var="grandTotalTotal" value="0"/>
			    <c:set var="numOrderTotal" value="0"/>
			    <c:set var="newUserTotal" value="0"/>
			    <c:set var="newUserActiveTotal" value="0"/>
			    <c:set var="avgPerOrderTotal" value="0"/>
			    <c:set var="avgPerCustomerTotal" value="0"/>
			    <c:set var="numCustomerActivatedTotal" value="0"/>
			    <c:set var="numCustomerRetainedTotal" value="0"/>
		  	  <c:forEach items="${model.salesDetailReportsList}" var="saleDetailReport" varStatus="status">
		  	  <c:if test="${status.index % 3 == 0}">
		  	    <c:set var="merchandize" value="0"/>
			    <c:set var="shippingCost" value="0"/>
			    <c:set var="ccFee" value="0"/>
			    <c:set var="tax" value="0"/>
			    <c:set var="grandTotal" value="0"/>
			    <c:set var="numOrder" value="0"/>
			    <c:set var="newUser" value="0"/>
			    <c:set var="newUserActive" value="0"/>
			    <c:set var="avgPerOrder" value="0"/>
			    <c:set var="avgPerCustomer" value="0"/>
			    <c:set var="numCustomerActivated" value="0"/>
			    <c:set var="numCustomerRetained" value="0"/>
			  </c:if>
			  <tr class="row${status.index % 2}" onmouseover="changeStyleClass(this,'rowOver')" onmouseout="changeStyleClass(this,'row${status.index % 2}')">
			    <td class="numberCol small"><c:out value="${saleDetailReport.month}"/></td>			
			    <td class="numberCol small"><fmt:message key="${siteConfig['CURRENCY'].value}" /><fmt:formatNumber value="${saleDetailReport.merchandize}" pattern="#,##0.00" /></td><c:set var="merchandize" value="${saleDetailReport.merchandize + merchandize}" /><c:set var="merchandizeTotal" value="${saleDetailReport.merchandize + merchandizeTotal}" />
			    <td class="numberCol small"><fmt:message key="${siteConfig['CURRENCY'].value}" /><fmt:formatNumber value="${saleDetailReport.shippingCost}" pattern="#,##0.00" /></td><c:set var="shippingCost" value="${saleDetailReport.shippingCost + shippingCost}" /><c:set var="shippingCostTotal" value="${saleDetailReport.shippingCost + shippingCostTotal}" />
			    <td class="numberCol small"><fmt:message key="${siteConfig['CURRENCY'].value}" /><fmt:formatNumber value="${saleDetailReport.ccFee}" pattern="#,##0.00" /></td><c:set var="ccFee" value="${saleDetailReport.ccFee + ccFee}" /><c:set var="ccFeeTotal" value="${saleDetailReport.ccFee + ccFeeTotal}" />
			    <td class="numberCol small"><fmt:message key="${siteConfig['CURRENCY'].value}" /><fmt:formatNumber value="${saleDetailReport.tax}" pattern="#,##0.00" /></td><c:set var="tax" value="${saleDetailReport.tax + tax}" /><c:set var="taxTotal" value="${saleDetailReport.tax + taxTotal}" />
			    <td class="numberCol small"><fmt:message key="${siteConfig['CURRENCY'].value}" /><fmt:formatNumber value="${saleDetailReport.grandTotal}" pattern="#,##0.00" /></td><c:set var="grandTotal" value="${saleDetailReport.grandTotal + grandTotal}" /><c:set var="grandTotalTotal" value="${saleDetailReport.grandTotal + grandTotalTotal}" />
			    <td class="numberCol small"><fmt:formatNumber value="${saleDetailReport.numOrder}" pattern="#,##0" /></td><c:set var="numOrder" value="${saleDetailReport.numOrder + numOrder}" /><c:set var="numOrderTotal" value="${saleDetailReport.numOrder + numOrderTotal}" />
			    <td class="numberCol small"><fmt:formatNumber value="${saleDetailReport.uniqueUser}" pattern="#,##0" /></td>	
			    <td class="numberCol small"><fmt:formatNumber value="${saleDetailReport.newUser}" pattern="#,##0" /></td><c:set var="newUser" value="${saleDetailReport.newUser + newUser}" /><c:set var="newUserTotal" value="${saleDetailReport.newUser + newUserTotal}" />	
			    <td class="numberCol small"><fmt:formatNumber value="${saleDetailReport.newUserActive}" pattern="#,##0" /></td><c:set var="newUserActive" value="${saleDetailReport.newUserActive + newUserActive}" /><c:set var="newUserActiveTotal" value="${saleDetailReport.newUserActive + newUserActiveTotal}" />
			    <td class="numberCol small"><fmt:message key="${siteConfig['CURRENCY'].value}" /><fmt:formatNumber value="${saleDetailReport.avgGrandTotal}" pattern="#,##0.00" /></td><c:set var="avgPerOrder" value="${saleDetailReport.avgGrandTotal + avgPerOrder}" /><c:set var="avgPerOrderTotal" value="${saleDetailReport.avgGrandTotal + avgPerOrderTotal}" />				
			    <td class="numberCol small"><fmt:message key="${siteConfig['CURRENCY'].value}" /><fmt:formatNumber value="${saleDetailReport.avgPerCustomer}" pattern="#,##0.00" /></td><c:set var="avgPerCustomer" value="${saleDetailReport.avgPerCustomer + avgPerCustomer}" /><c:set var="avgPerCustomerTotal" value="${saleDetailReport.avgPerCustomer + avgPerCustomerTotal}" />
			    <td class="numberCol small"><fmt:formatNumber value="${saleDetailReport.numCustomerActivated}" pattern="#,##0" /></td><c:set var="numCustomerActivated" value="${saleDetailReport.numCustomerActivated + numCustomerActivated}" /><c:set var="numCustomerActivatedTotal" value="${saleDetailReport.numCustomerActivated + numCustomerActivatedTotal}" />		
			    <td class="numberCol small"><fmt:formatNumber value="${saleDetailReport.numCustomerRetained}" pattern="#,##0" /></td><c:set var="numCustomerRetained" value="${saleDetailReport.numCustomerRetained + numCustomerRetained}" /><c:set var="numCustomerRetainedTotal" value="${saleDetailReport.numCustomerRetained + numCustomerRetainedTotal}" />			
			  </tr>
			  <c:if test="${status.index % 3 == 2}">
			  <c:set var="quarterIndex" value="${quarterIndex + 1}" />
			  <tr style="font-weight:700;">
			    <td class="numberCol quarter">Q<c:out value="${quarterIndex}" /></td>
			    <td class="numberCol quarter"><fmt:message key="${siteConfig['CURRENCY'].value}" /><fmt:formatNumber value="${merchandize}" pattern="#,##0.00" /></td>
			    <td class="numberCol quarter"><fmt:message key="${siteConfig['CURRENCY'].value}" /><fmt:formatNumber value="${shippingCost}" pattern="#,##0.00" /></td>
			    <td class="numberCol quarter"><fmt:message key="${siteConfig['CURRENCY'].value}" /><fmt:formatNumber value="${ccFee}" pattern="#,##0.00" /></td>
			    <td class="numberCol quarter"><fmt:message key="${siteConfig['CURRENCY'].value}" /><fmt:formatNumber value="${tax}" pattern="#,##0.00" /></td>
			    <td class="numberCol quarter"><fmt:message key="${siteConfig['CURRENCY'].value}" /><fmt:formatNumber value="${grandTotal}" pattern="#,##0.00" /></td>
			    <td class="numberCol quarter"><fmt:formatNumber value="${numOrder}" pattern="#,##0" /></td>
			    <td class="numberCol quarter"></td>
			    <td class="numberCol quarter"><fmt:formatNumber value="${newUser}" pattern="#,##0" /></td>
			    <td class="numberCol quarter"><fmt:formatNumber value="${newUserActive}" pattern="#,##0" /></td>
			    <td class="numberCol quarter"><fmt:message key="${siteConfig['CURRENCY'].value}" />[<fmt:formatNumber value="${grandTotal/numOrder}" pattern="#,##0.00" />]</td>
			    <td class="numberCol quarter"><fmt:message key="${siteConfig['CURRENCY'].value}" /><fmt:formatNumber value="${avgPerCustomer}" pattern="#,##0.00" /></td>
			    <td class="numberCol quarter"><fmt:formatNumber value="${numCustomerActivated}" pattern="#,##0" /></td>
			    <td class="numberCol quarter"><fmt:formatNumber value="${numCustomerRetained}" pattern="#,##0" /></td>
			 </tr>
			 </c:if>
			 </c:forEach>
			 <tr class="totals">
			    <td style="color:#666666; padding: 5px" colspan="1" align="left"><fmt:message key="total" /></td>
			    <td class="numberCol"><fmt:message key="${siteConfig['CURRENCY'].value}" /><fmt:formatNumber value="${merchandizeTotal}" pattern="#,##0.00" /></td>
			    <td class="numberCol"><fmt:message key="${siteConfig['CURRENCY'].value}" /><fmt:formatNumber value="${shippingCostTotal}" pattern="#,##0.00" /></td>
			    <td class="numberCol"><fmt:message key="${siteConfig['CURRENCY'].value}" /><fmt:formatNumber value="${ccFeeTotal}" pattern="#,##0.00" /></td>
			    <td class="numberCol"><fmt:message key="${siteConfig['CURRENCY'].value}" /><fmt:formatNumber value="${taxTotal}" pattern="#,##0.00" /></td>
			    <td class="numberCol"><fmt:message key="${siteConfig['CURRENCY'].value}" /><fmt:formatNumber value="${grandTotalTotal}" pattern="#,##0.00" /></td>
			    <td class="numberCol"><fmt:formatNumber value="${numOrderTotal}" pattern="#,##0" /></td>
			    <td class="numberCol"></td>
			    <td class="numberCol"><fmt:formatNumber value="${newUserTotal}" pattern="#,##0" /></td>
			    <td class="numberCol"><fmt:formatNumber value="${newUserActiveTotal}" pattern="#,##0" /></td>
			    <td class="numberCol"><fmt:message key="${siteConfig['CURRENCY'].value}" />[<fmt:formatNumber value="${grandTotalTotal/numOrderTotal}" pattern="#,##0.00" />]</td>
			    <td class="numberCol"><fmt:message key="${siteConfig['CURRENCY'].value}" /><fmt:formatNumber value="${avgPerCustomerTotal}" pattern="#,##0.00" /></td>
			    <td class="numberCol"><fmt:formatNumber value="${numCustomerActivatedTotal}" pattern="#,##0" /></td>
			    <td class="numberCol"><fmt:formatNumber value="${numCustomerRetainedTotal}" pattern="#,##0" /></td>
			  </tr> 
			</table>


		    <!-- end input field -->  	  
		  	</div>
		  	
		  	<!-- start button -->
			<!-- end button -->	

	<!-- end tab -->        
	</div>
<!-- end tabs -->			
</div>
  
  <!-- end table -->
   </td><td class="boxmidr" ></td></tr>
  <tr><td class="botl"></td><td class="botr"></td></tr>
  </table>
  
<!-- end main box -->  
</div>
</form>  
</sec:authorize>

</c:if>
 </tiles:putAttribute>    
</tiles:insertDefinition>