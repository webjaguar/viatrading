<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/security/tags" prefix="sec" %>

<tiles:insertDefinition name="admin.report" flush="true">
  <tiles:putAttribute name="content" type="string">
  
<!-- main box -->
  <div id="mboxfull">
  
  <!-- start table -->
  <table cellpadding="0" cellspacing="0" border="0" class="module" >
  <tr>
    <td class="topl_g">
	  
	  <!-- breadcrumb -->
	  <p class="breadcrumb">
	    <fmt:message key="reports" />
	  </p>
	  
	  <!-- Error -->
	  <c:if test="${!empty model.message}">
		<div class="message"><fmt:message key="${model.message}" /></div>
	  </c:if>
	  
	  <!-- header image -->
	  <img class="headerImage" src="../graphics/reports.gif" /> 
	  
  </td><td class="topr_g" ></td></tr>
  <tr><td class="boxmidlrg" >  
  <!-- start table -->

 <!-- tabs -->
 <div class="tab-wrapper">
	<h4 title=""></h4>
	<div>
	<!-- start tab -->
	<table border="0" width="100%"><tr><td>

	  	<div class="listdivi"></div>
	 
	    <sec:authorize ifAnyGranted="ROLE_AEM,ROLE_ADMIN_${prop_site_id},ROLE_REPORT_SHIPPED_ORDER,ROLE_REPORT_ORDER_NEED_ATTENTION,ROLE_REPORT_SHIPPED_OVERVIEW,ROLE_REPORT_ORDER_PEN_PROC,ROLE_REPORT_SHIPPED_DAILY,ROLE_REPORT_AFFILIATE_COMMISSION,ROLE_REPORT_TRACKCODE,ROLE_REPORT_ORDER_OVERVIEW">
    	<div style="float:left;padding: 5px">
           <table class="reportBox" cellpadding="5" cellspacing="5">
           <tr>
            <td style="line-height:8px; height:20px;" valign="top">
             <table cellpadding="1" cellspacing="1">
              <tr style="height:15px;white-space: nowrap">
               <td valign="top"><h3><fmt:message key="saleReport" /></h3></td>
              </tr>
             </table>
            </td>
           </tr>
           <tr>
            <td valign="top">
                <sec:authorize ifAnyGranted="ROLE_AEM,ROLE_ADMIN_${prop_site_id},ROLE_REPORT_SHIPPED_ORDER,ROLE_REPORT_ORDER_NEED_ATTENTION,ROLE_REPORT_SHIPPED_OVERVIEW,ROLE_REPORT_ORDER_PEN_PROC,ROLE_REPORT_SHIPPED_DAILY,ROLE_REPORT_AFFILIATE_COMMISSION">
			    <a href="orderReport.jhtm" class="reportLink"><fmt:message key="orders"/></a>
			    </sec:authorize>
			    <sec:authorize ifAnyGranted="ROLE_AEM,ROLE_ADMIN_${prop_site_id},ROLE_REPORT_ORDER_PEN_PROC,ROLE_REPORT_SHIPPED_DAILY">
			    <a href="dailyOrderReport.jhtm" class="reportLink"><fmt:message key="ordersDaily"/></a>
			    </sec:authorize>
			    <sec:authorize ifAnyGranted="ROLE_AEM,ROLE_ADMIN_${prop_site_id},ROLE_REPORT_ORDER_OVERVIEW">
			    <a href="orderDetailReport.jhtm" class="reportLink"><fmt:message key="ordersOverview"/></a>
			    </sec:authorize>
			    <sec:authorize ifAnyGranted="ROLE_AEM,ROLE_ADMIN_${prop_site_id},ROLE_REPORT_TRACKCODE">   
			    <a href="trackcodeReport.jhtm" class="reportLink"><fmt:message key="trackcode"/></a>    
			    </sec:authorize>
			    <sec:authorize ifAnyGranted="ROLE_AEM,ROLE_ADMIN_${prop_site_id}">   
    			<a href="promoCodeReport.jhtm" class="reportLink"><fmt:message key="promoCode"/></a>    
    			</sec:authorize>
            </td>
           </tr>
           </table>		 
		</div>
		</sec:authorize>
		
		<sec:authorize ifAnyGranted="ROLE_AEM,ROLE_ADMIN_${prop_site_id},ROLE_REPORT_CUSTOMERS,ROLE_REPORT_SALESREP,ROLE_REPORT_SALESREP_DAILY,ROLE_REPORT_SALESREP_OVERVIEWII,ROLE_REPORT_CUSTOMER_METRIC">
		<div style="float:left;padding: 5px">
           <table class="reportBox" cellpadding="5" cellspacing="5">
           <tr>
            <td style="line-height:8px; height:20px;" valign="top">
             <table cellpadding="1" cellspacing="1">
              <tr style="height:15px;white-space: nowrap">
               <td valign="top"><h3><fmt:message key="customerSalesRepReport" /></h3></td>
              </tr>
             </table>
            </td>
           </tr>
           <tr>
            <td valign="top">
			    <sec:authorize ifAnyGranted="ROLE_AEM,ROLE_ADMIN_${prop_site_id},ROLE_REPORT_CUSTOMERS"> 
			    <a href="customerReport.jhtm" class="reportLink"><fmt:message key="customersOverview"/></a>
			    <a href="customerInactiveReport.jhtm" class="reportLink"><fmt:message key="customersInactive"/></a>
			    </sec:authorize>
			    <sec:authorize ifAnyGranted="ROLE_AEM,ROLE_ADMIN_${prop_site_id},ROLE_REPORT_SALESREP">   
			    <a href="salesRepReport.jhtm" class="reportLink"><fmt:message key="salesRep"/></a>  
			    </sec:authorize>
			    <sec:authorize ifAnyGranted="ROLE_AEM,ROLE_ADMIN_${prop_site_id},ROLE_REPORT_SALESREP_DAILY">   
			    <a href="salesRepReportDaily.jhtm" class="reportLink"><fmt:message key="salesRep"/><fmt:message key="daily"/></a>
			    </sec:authorize>
			    <sec:authorize ifAnyGranted="ROLE_AEM,ROLE_ADMIN_${prop_site_id},ROLE_REPORT_SALESREP_DAILY">
			    <a href="salesRepDetailReport.jhtm" class="reportLink"><fmt:message key="salesRepOverview"/></a>
			    </sec:authorize>			    
			    <sec:authorize ifAnyGranted="ROLE_AEM,ROLE_ADMIN_${prop_site_id},ROLE_REPORT_SALESREP_OVERVIEWII">
			    <a href="salesRepDetailReport2.jhtm" class="reportLink"><fmt:message key="salesRepOverview"/> II</a>
			    </sec:authorize>
			    <sec:authorize ifAnyGranted="ROLE_AEM,ROLE_ADMIN_${prop_site_id},ROLE_REPORT_CUSTOMER_METRIC">   
			    <a href="metricOverviewReport.jhtm" class="userbutton"><fmt:message key="metricOverviewReport"/></a>
			    </sec:authorize>
            </td>
           </tr>
           </table>		 
		</div>
		</sec:authorize>
    
        <sec:authorize ifAnyGranted="ROLE_AEM,ROLE_ADMIN_${prop_site_id},ROLE_EMAILMANAGER_VIEW_HISTORY">
        <div style="float:left;padding: 5px">
           <table class="reportBox" cellpadding="5" cellspacing="5">
           <tr>
            <td style="line-height:8px; height:20px;" valign="top">
             <table cellpadding="1" cellspacing="1">
              <tr style="height:15px;white-space: nowrap">
               <td valign="top"><h3><fmt:message key="massEmailReport" /></h3></td>
              </tr>
             </table>
            </td>
           </tr>
           <tr>
            <td valign="top">
			    <sec:authorize ifAnyGranted="ROLE_AEM,ROLE_ADMIN_${prop_site_id},ROLE_EMAILMANAGER_VIEW_HISTORY">   
			    <a href="massEmailReport.jhtm" class="reportLink"><fmt:message key="massEmail"/></a>
			    </sec:authorize>
            </td>
           </tr>
           </table>		 
		</div>
		</sec:authorize>
  
        <sec:authorize ifAnyGranted="ROLE_AEM,ROLE_ADMIN_${prop_site_id},ROLE_REPORT_IMP_EXP,ROLE_REPORT_PRODUCT">
        <div style="float:left;padding: 5px">
           <table class="reportBox" cellpadding="5" cellspacing="5">
           <tr>
            <td style="line-height:8px; height:20px;" valign="top">
             <table cellpadding="1" cellspacing="1">
              <tr style="height:15px;white-space: nowrap">
               <td valign="top"><h3><fmt:message key="productReport" /></h3></td>
              </tr>
             </table>
            </td>
           </tr>
           <tr>
            <td valign="top">
			    <sec:authorize ifAnyGranted="ROLE_AEM,ROLE_ADMIN_${prop_site_id},ROLE_REPORT_IMP_EXP">   
			    <a href="importExportHistory.jhtm" class="reportLink"><fmt:message key="importExport"/></a>
			    </sec:authorize>
			    <sec:authorize ifAnyGranted="ROLE_AEM,ROLE_ADMIN_${prop_site_id},ROLE_REPORT_PRODUCT">   
			    <a href="productListReport.jhtm" class="reportLink"><fmt:message key="productReport"/></a>
			    </sec:authorize>
			    <sec:authorize ifAnyGranted="ROLE_AEM,ROLE_ADMIN_${prop_site_id},ROLE_REPORT_PRODUCT">   
			    <a href="inventoryActivityReport.jhtm" class="reportLink"><fmt:message key="inventoryActivity"/></a>
			    </sec:authorize>
			    <sec:authorize ifAnyGranted="ROLE_AEM,ROLE_ADMIN_${prop_site_id},ROLE_REPORT_PRODUCT">   
			    <a href="inventoryReport.jhtm" class="reportLink"><fmt:message key="inventorySummary"/></a>
			    </sec:authorize>
            </td>
           </tr>
           </table>		 
		</div> 
		</sec:authorize>
		
  
		 
    </td></tr></table>
	<!-- end tab -->        
	</div>
<!-- end tabs -->			
</div>
  
  <!-- end table -->
  </td><td class="boxmidr" ></td></tr>
  <tr><td class="botl"></td><td class="botr"></td></tr>
  </table>
  
<!-- end main box -->  
</div>

  
  </tiles:putAttribute>
</tiles:insertDefinition>