<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/security/tags" prefix="sec" %>

<tiles:insertDefinition name="admin.report.inventory" flush="true">
  <tiles:putAttribute name="content" type="string">  
  
<c:if test="${siteConfig['INVENTORY_HISTORY'].value == 'true'}">
<sec:authorize ifAnyGranted="ROLE_AEM,ROLE_ADMIN_${prop_site_id},ROLE_REPORT_PRODUCT,ROLE_REPORT_INVENTORY">

<form  id="list" action="" name="list_form">
<input type="hidden" id="sort" name="sort" value="${inventoryReportFilter.sort}" />
<!-- main box -->
  <div id="mboxfull">
  
  <!-- start table -->
  <table cellpadding="0" cellspacing="0" border="0" class="module" >
  <tr>
    <td class="topl_g">
	  
	  <!-- breadcrumb -->
	  <p class="breadcrumb">
	    <a href="../reports"><fmt:message key="reports" /></a> &gt;
	    <fmt:message key="inventorySummary" />
	  </p>
	  
	  <!-- Error -->
	  <c:if test="${!empty model.message}">
		<div class="message"><fmt:message key="${model.message}" /></div>
	  </c:if>
	  
	  <!-- header image -->
	  <img class="headerImage" src="../graphics/reports.gif" alt=""/> 
	  
  </td><td class="topr_g" ></td></tr>
  <tr><td class="boxmidlrg" >

 <!-- tabs -->
 <div class="tab-wrapper">
	<h4 title=""></h4>
	<div>
	<!-- start tab -->
	
	  	<div class="listdivi"></div>
	    
		  	<div class="listlight">
		  	<!-- input field -->
		  	
		  	<table border="0" cellpadding="0" cellspacing="1" width="100%">
			  <tr>
			  <td class="pageShowing">
			  <c:if test="${model.count > 0}">
			  <fmt:message key="showing">
				<fmt:param value="${inventoryReportFilter.offset + 1}"/>
				<fmt:param value="${model.pageEnd}"/>
				<fmt:param value="${model.count}"/>
			  </fmt:message>
			  </c:if>
			  </td>
			  <td class="pageNavi">
			  Page 
			  <c:choose>
			  <c:when test="${model.pageCount <= 100 and model.pageCount > 0}">
			  <select name="page" id="page" onchange="submit()">
			  <c:forEach begin="1" end="${model.pageCount}" var="page">
			  	<option value="${page}" <c:if test="${page == (inventoryReportFilter.page)}">selected</c:if>>${page}</option>
			  </c:forEach>
			  </select>
			  </c:when>
			  <c:otherwise>
			  <input type="text" id="page" name="page" value="${productSearch.page}" size="5" class="textfield50" />
			  <input type="submit" value="go"/>
			  </c:otherwise>
			  </c:choose>
			  of <c:out value="${model.pageCount}"/>
			  | 
			  <c:if test="${inventoryReportFilter.page == 1}"><span class="pageNaviDead"><fmt:message key="previous" /></span></c:if>
			  <c:if test="${inventoryReportFilter.page != 1}"><a href="<c:url value="inventoryReport.jhtm"><c:param name="page" value="${inventoryReportFilter.page-1}"/></c:url>" class="pageNaviLink"><fmt:message key="previous" /></a></c:if>
			  | 
			  <c:if test="${inventoryReportFilter.page == model.pageCount}"><span class="pageNaviDead"><fmt:message key="next" /></span></c:if>
			  <c:if test="${inventoryReportFilter.page != model.pageCount}"><a href="<c:url value="inventoryReport.jhtm"><c:param name="page" value="${inventoryReportFilter.page+1}"/></c:url>" class="pageNaviLink"><fmt:message key="next" /></a></c:if>
			  </td>
			  </tr>
			</table>
			
		  	<table border="0" cellpadding="2" cellspacing="0" width="100%" class="listings">
			  <tr class="listingsHdr2">
			    <td class="indexCol">&nbsp;</td>
			    <td class="listingsHdr3">
			    	<table cellspacing="0" cellpadding="1">
			    	  <tr>
			    	    <c:choose>
			    	      <c:when test="${inventoryReportFilter.sort == 'sku DESC'}">
			    	        <td class="listingsHdr3">
			    	        <a class="hdrLink" href="#" onClick="document.getElementById('sort').value='sku';document.getElementById('list').submit()"><fmt:message key="sku" /></a>
			    	        </td>
			    	        <td><img src="../graphics/up.gif" border="0"></td>
			    	      </c:when>
			    	      <c:when test="${inventoryReportFilter.sort == 'sku'}">
			    	        <td class="listingsHdr3">
			    	        <a class="hdrLink" href="#" onClick="document.getElementById('sort').value='sku DESC';document.getElementById('list').submit()"><fmt:message key="sku" /></a>
			    	        </td>
			    	        <td><img src="../graphics/down.gif" border="0"></td>
			    	      </c:when>
			    	      <c:otherwise>
			    	        <td colspan="2" class="listingsHdr3">
			    	        <a class="hdrLink" href="#" onClick="document.getElementById('sort').value='sku';document.getElementById('list').submit()"><fmt:message key="sku" /></a>
			    	        </td>
			    	      </c:otherwise>
			    	    </c:choose>  
			    	  </tr>
			    	</table>
			    </td>
			    <td class="listingsHdr3">
			    	<table cellspacing="0" cellpadding="1">
			    	  <tr>
			    	    <c:choose>
			    	      <c:when test="${inventoryReportFilter.sort == 'name DESC'}">
			    	        <td class="listingsHdr3">
			    	        <a class="hdrLink" href="#" onClick="document.getElementById('sort').value='name';document.getElementById('list').submit()"><fmt:message key="item" /></a>
			    	        </td>
			    	        <td><img src="../graphics/up.gif" border="0"></td>
			    	      </c:when>
			    	      <c:when test="${inventoryReportFilter.sort == 'name'}">
			    	        <td class="listingsHdr3">
			    	        <a class="hdrLink" href="#" onClick="document.getElementById('sort').value='name DESC';document.getElementById('list').submit()"><fmt:message key="item" /></a>
			    	        </td>
			    	        <td><img src="../graphics/down.gif" border="0"></td>
			    	      </c:when>
			    	      <c:otherwise>
			    	        <td colspan="2" class="listingsHdr3">
			    	        <a class="hdrLink" href="#" onClick="document.getElementById('sort').value='name';document.getElementById('list').submit()"><fmt:message key="item" /></a>
			    	        </td>
			    	      </c:otherwise>
			    	    </c:choose>  
			    	  </tr>
			    	</table>
			    </td>
			    <td class="listingsHdr3">
			    	<table cellspacing="0" cellpadding="1">
			    	  <tr>
			    	    <c:choose>
			    	      <c:when test="${inventoryReportFilter.sort == 'units_purchased_in_transit DESC'}">
			    	        <td class="listingsHdr3">
			    	        <a class="hdrLink" href="#" onClick="document.getElementById('sort').value='units_purchased_in_transit';document.getElementById('list').submit()"><fmt:message key="unitsPurchasedInTransit" /></a>
			    	        </td>
			    	        <td><img src="../graphics/up.gif" border="0"></td>
			    	      </c:when>
			    	      <c:when test="${inventoryReportFilter.sort == 'units_purchased_in_transit'}">
			    	        <td class="listingsHdr3">
			    	        <a class="hdrLink" href="#" onClick="document.getElementById('sort').value='units_purchased_in_transit DESC';document.getElementById('list').submit()"><fmt:message key="unitsPurchasedInTransit" /></a>
			    	        </td>
			    	        <td><img src="../graphics/down.gif" border="0"></td>
			    	      </c:when>
			    	      <c:otherwise>
			    	        <td colspan="2" class="listingsHdr3">
			    	        <a class="hdrLink" href="#" onClick="document.getElementById('sort').value='units_purchased_in_transit';document.getElementById('list').submit()"><fmt:message key="unitsPurchasedInTransit" /></a>
			    	        </td>
			    	      </c:otherwise>
			    	    </c:choose>  
			    	  </tr>
			    	</table>
			    </td>
			    <td class="listingsHdr3">
			    	<table cellspacing="0" cellpadding="1">
			    	  <tr>
			    	    <c:choose>
			    	      <c:when test="${inventoryReportFilter.sort == 'units_purchased_delivered DESC'}">
			    	        <td class="listingsHdr3">
			    	        <a class="hdrLink" href="#" onClick="document.getElementById('sort').value='units_purchased_delivered';document.getElementById('list').submit()"><fmt:message key="unitsPurchasedDelivered" /></a>
			    	        </td>
			    	        <td><img src="../graphics/up.gif" border="0"></td>
			    	      </c:when>
			    	      <c:when test="${inventoryReportFilter.sort == 'units_purchased_delivered'}">
			    	        <td class="listingsHdr3">
			    	        <a class="hdrLink" href="#" onClick="document.getElementById('sort').value='units_purchased_delivered DESC';document.getElementById('list').submit()"><fmt:message key="unitsPurchasedDelivered" /></a>
			    	        </td>
			    	        <td><img src="../graphics/down.gif" border="0"></td>
			    	      </c:when>
			    	      <c:otherwise>
			    	        <td colspan="2" class="listingsHdr3">
			    	        <a class="hdrLink" href="#" onClick="document.getElementById('sort').value='units_purchased_delivered';document.getElementById('list').submit()"><fmt:message key="unitsPurchasedDelivered" /></a>
			    	        </td>
			    	      </c:otherwise>
			    	    </c:choose>  
			    	  </tr>
			    	</table>
			    </td>
			    <td class="listingsHdr3">
			    	<table cellspacing="0" cellpadding="1">
			    	  <tr>
			    	    <c:choose>
			    	      <c:when test="${inventoryReportFilter.sort == 'units_sold_in_process DESC'}">
			    	        <td class="listingsHdr3">
			    	        <a class="hdrLink" href="#" onClick="document.getElementById('sort').value='units_sold_in_process';document.getElementById('list').submit()"><fmt:message key="unitsSoldInProcess" /></a>
			    	        </td>
			    	        <td><img src="../graphics/up.gif" border="0"></td>
			    	      </c:when>
			    	      <c:when test="${inventoryReportFilter.sort == 'units_sold_in_process'}">
			    	        <td class="listingsHdr3">
			    	        <a class="hdrLink" href="#" onClick="document.getElementById('sort').value='units_sold_in_process DESC';document.getElementById('list').submit()"><fmt:message key="unitsSoldInProcess" /></a>
			    	        </td>
			    	        <td><img src="../graphics/down.gif" border="0"></td>
			    	      </c:when>
			    	      <c:otherwise>
			    	        <td colspan="2" class="listingsHdr3">
			    	        <a class="hdrLink" href="#" onClick="document.getElementById('sort').value='units_sold_in_process';document.getElementById('list').submit()"><fmt:message key="unitsSoldInProcess" /></a>
			    	        </td>
			    	      </c:otherwise>
			    	    </c:choose>  
			    	  </tr>
			    	</table>
			    </td>
			    <td class="listingsHdr3">
			    	<table cellspacing="0" cellpadding="1">
			    	  <tr>
			    	    <c:choose>
			    	      <c:when test="${inventoryReportFilter.sort == 'units_sold_shipped DESC'}">
			    	        <td class="listingsHdr3">
			    	        <a class="hdrLink" href="#" onClick="document.getElementById('sort').value='units_sold_shipped';document.getElementById('list').submit()"><fmt:message key="unitsSoldShipped" /></a>
			    	        </td>
			    	        <td><img src="../graphics/up.gif" border="0"></td>
			    	      </c:when>
			    	      <c:when test="${inventoryReportFilter.sort == 'units_sold_shipped'}">
			    	        <td class="listingsHdr3">
			    	        <a class="hdrLink" href="#" onClick="document.getElementById('sort').value='units_sold_shipped DESC';document.getElementById('list').submit()"><fmt:message key="unitsSoldShipped" /></a>
			    	        </td>
			    	        <td><img src="../graphics/down.gif" border="0"></td>
			    	      </c:when>
			    	      <c:otherwise>
			    	        <td colspan="2" class="listingsHdr3">
			    	        <a class="hdrLink" href="#" onClick="document.getElementById('sort').value='units_sold_shipped';document.getElementById('list').submit()"><fmt:message key="unitsSoldShipped" /></a>
			    	        </td>
			    	      </c:otherwise>
			    	    </c:choose>  
			    	  </tr>
			    	</table>
			    </td>
			    <td class="listingsHdr3">
			    	<table cellspacing="0" cellpadding="1">
			    	  <tr>
			    	    <c:choose>
			    	      <c:when test="${inventoryReportFilter.sort == 'units_adjusted DESC'}">
			    	        <td class="listingsHdr3">
			    	        <a class="hdrLink" href="#" onClick="document.getElementById('sort').value='units_adjusted';document.getElementById('list').submit()"><fmt:message key="unitsAdjusted" /></a>
			    	        </td>
			    	        <td><img src="../graphics/up.gif" border="0"></td>
			    	      </c:when>
			    	      <c:when test="${inventoryReportFilter.sort == 'units_adjusted'}">
			    	        <td class="listingsHdr3">
			    	        <a class="hdrLink" href="#" onClick="document.getElementById('sort').value='units_adjusted DESC';document.getElementById('list').submit()"><fmt:message key="unitsAdjusted" /></a>
			    	        </td>
			    	        <td><img src="../graphics/down.gif" border="0"></td>
			    	      </c:when>
			    	      <c:otherwise>
			    	        <td colspan="2" class="listingsHdr3">
			    	        <a class="hdrLink" href="#" onClick="document.getElementById('sort').value='units_adjusted';document.getElementById('list').submit()"><fmt:message key="unitsAdjusted" /></a>
			    	        </td>
			    	      </c:otherwise>
			    	    </c:choose>  
			    	  </tr>
			    	</table>
			    </td>
			    <td class="listingsHdr3">
			    	<table cellspacing="0" cellpadding="1">
			    	  <tr>
			    	    <c:choose>
			    	      <c:when test="${inventoryReportFilter.sort == 'inventory_afs DESC'}">
			    	        <td class="listingsHdr3">
			    	        <a class="hdrLink" href="#" onClick="document.getElementById('sort').value='inventory_afs';document.getElementById('list').submit()"><fmt:message key="available_for_sale" /></a>
			    	        </td>
			    	        <td><img src="../graphics/up.gif" border="0"></td>
			    	      </c:when>
			    	      <c:when test="${inventoryReportFilter.sort == 'inventory_afs'}">
			    	        <td class="listingsHdr3">
			    	        <a class="hdrLink" href="#" onClick="document.getElementById('sort').value='inventory_afs DESC';document.getElementById('list').submit()"><fmt:message key="available_for_sale" /></a>
			    	        </td>
			    	        <td><img src="../graphics/down.gif" border="0"></td>
			    	      </c:when>
			    	      <c:otherwise>
			    	        <td colspan="2" class="listingsHdr3">
			    	        <a class="hdrLink" href="#" onClick="document.getElementById('sort').value='inventory_afs';document.getElementById('list').submit()"><fmt:message key="available_for_sale" /></a>
			    	        </td>
			    	      </c:otherwise>
			    	    </c:choose>  
			    	  </tr>
			    	</table>
			    </td>
			  </tr>
		  	  <c:forEach items="${model.inventoryReportList}" var="inventoryReport"	varStatus="status">
			  <tr class="row${status.index % 2}" onmouseover="changeStyleClass(this,'rowOver')" onmouseout="changeStyleClass(this,'row${status.index % 2}')">
			    <td class="indexCol">${status.index + 1}</td>
			    <td class="nameCol"><c:out value="${inventoryReport.sku}" /></td>			
			    <td class="nameCol"><c:out value="${inventoryReport.name}"/></td>
			    <td class="nameCol"><a href="inventoryActivityReport.jhtm?type=purchaseOrder&sku=${inventoryReport.sku}"><c:out value="${inventoryReport.unitsPurchasedInTransit}" /></a></td>			
			    <td class="nameCol"><a href="inventoryActivityReport.jhtm?type=purchaseOrder&sku=${inventoryReport.sku}"><c:out value="${inventoryReport.unitsPurchasedDelivered}" /></a></td>			
			    <td class="nameCol"><c:if test="${inventoryReport.unitsSoldInProcess != null}"><a href="inventoryActivityReport.jhtm?type=packingList&sku=${inventoryReport.sku}"><c:out value="${inventoryReport.unitsSoldInProcess * (-1)}" /></a></c:if></td>
			    <td class="nameCol"><c:if test="${inventoryReport.unitsSoldShipped != null}"><a href="inventoryActivityReport.jhtm?type=packingList&sku=${inventoryReport.sku}"><c:out value="${inventoryReport.unitsSoldShipped * (-1)}" /></a></c:if></td>
			    <td class="nameCol"><a href="inventoryActivityReport.jhtm?type=adjustment&sku=${inventoryReport.sku}"><c:out value="${inventoryReport.unitsAdjusted}" /></a></td>			
			    <td class="nameCol"><c:out value="${inventoryReport.inventoryAFS}" /></td>
			  </tr>
			</c:forEach>
			</table>
			
			<table border="0" cellpadding="0" cellspacing="1" width="100%">
			  <tr> 
			  <td class="pageSize">
			  <select name="size" onchange="document.getElementById('page').value=1;submit()">
			    <c:forTokens items="10,25,50,100,200,500" delims="," var="current">
			  	  <option value="${current}" <c:if test="${current == inventoryReportFilter.pageSize}">selected</c:if>>${current} <fmt:message key="perPage" /></option>
			    </c:forTokens>
			  </select>
			  </td>
			  </tr>
			</table>   		  
		    <!-- end input field -->
		  	</div>
		  	
		  	<!-- start button -->
		  	<div align="left" class="button">
		  	</div>
			<!-- end button -->	
      	  	
	<!-- end tab -->        
	</div>
<!-- end tabs -->			
</div>
  
  <!-- end table -->
   </td><td class="boxmidr" ></td></tr>
  <tr><td class="botl"></td><td class="botr"></td></tr>
  </table>
  
<!-- end main box -->  
</div>
	 </form> 
</sec:authorize>
</c:if>
  </tiles:putAttribute>
</tiles:insertDefinition>
