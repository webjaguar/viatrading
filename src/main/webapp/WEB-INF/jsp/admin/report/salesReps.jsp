<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/security/tags" prefix="sec" %>

<tiles:insertDefinition name="admin.report.salesReps" flush="true">
  <tiles:putAttribute name="content" type="string">
  
<sec:authorize ifAnyGranted="ROLE_AEM,ROLE_ADMIN_${prop_site_id},ROLE_REPORT_SALESREP">
<script type="text/javascript">
<!--

//-->
</script>
<form name="salesRepList" id="salesRepList" action="">
<!-- main box -->
  <div id="mboxfull">
  
  <!-- start table -->
  <table cellpadding="0" cellspacing="0" border="0" class="module" >
  <tr>
    <td class="topl_g">
	  
	  <!-- breadcrumb -->
	  <p class="breadcrumb">
	    <a href="../reports"><fmt:message key="reports" /></a> &gt;
	    <fmt:message key="salesRep" />
	  </p>
	  
	  <!-- Error -->
	  <c:if test="${!empty model.message}">
		<div class="message"><fmt:message key="${model.message}" /></div>
	  </c:if>
	  
	  <!-- header image -->
	  <img class="headerImage" src="../graphics/reports.gif" alt=""/> 
	  <!--  
	  <div>
	  <select name="_dateType_salesRep" onchange="submit();">
	    <option value="0">Order Date</option>
	    <option value="1" <c:if test="${salesRepFilter.dateType == 1}">selected="selected"</c:if>>Ship Date</option>
	  </select>
	  </div>
	  -->
	  
  </td><td class="topr_g" ></td></tr>
  <tr><td class="boxmidlrg" >

 <!-- tabs -->
 <div class="tab-wrapper">
	<h4 title=""></h4>
	<div>
	<!-- start tab -->
	
	  	<div class="listdivi"></div>
	    
		  	<div class="listlight">
		  	<!-- input field -->
		  	
		  	<table border="0" cellpadding="2" cellspacing="0" width="100%" class="listings">
			  <tr class="listingsHdr2">
			    <td align="left" class="listingsHdr3"><fmt:message key="Name" /></td>
			    <c:choose>
			     <c:when test="${salesRepFilter.quarter == '1'}">
			       <td colspan="2" align="center" class="salesRepReportHead">Jan</td>
			       <td colspan="2" align="center" class="salesRepReportHead">Feb</td>
			       <td colspan="2" align="center" class="salesRepReportHead">Mar</td>
			     </c:when>
			     <c:when test="${salesRepFilter.quarter == '2'}">
			       <td colspan="2" align="center" class="salesRepReportHead">Apr</td>
			       <td colspan="2" align="center" class="salesRepReportHead">May</td>
			       <td colspan="2" align="center" class="salesRepReportHead">Jun</td>
			     </c:when>
			     <c:when test="${salesRepFilter.quarter == '3'}">
			       <td colspan="2" align="center" class="salesRepReportHead">Jul</td>
			       <td colspan="2" align="center" class="salesRepReportHead">Aug</td>
			       <td colspan="2" align="center" class="salesRepReportHead">Sep</td>
			     </c:when>
			     <c:when test="${salesRepFilter.quarter == '4'}">
			       <td colspan="2" align="center" class="salesRepReportHead">Oct</td>
			       <td colspan="2" align="center" class="salesRepReportHead">Nov</td>
			       <td colspan="2" align="center" class="salesRepReportHead">Dec</td>
			     </c:when>
			     <c:when test="${salesRepFilter.quarter == '0'}">
			       <td colspan="2" align="center" class="salesRepReportHead">${salesRepFilter.year}</td>
			     </c:when>
			    </c:choose>

			  </tr>
			  <tr>
			    <td class="indexCol">&nbsp;</td>
			    <c:set var="totalGrandTotalShipped0" value="0.0" />
			    <c:set var="totalGrandTotalShipped1" value="0.0" />
			    <c:set var="totalGrandTotalShipped2" value="0.0" />
			    <c:set var="totalGrandTotalPenProc0" value="0.0" />
			    <c:set var="totalGrandTotalPenProc1" value="0.0" />
			    <c:set var="totalGrandTotalPenProc2" value="0.0" />
			    <c:choose>
			     <c:when test="${salesRepFilter.quarter == '1'}">
			       <td align="center" class="salesRepReport"><fmt:message key="shipped" /></td><td align="center" class="salesRepReport"><fmt:message key="pendingProcessing" /></td>
			       <td align="center" class="salesRepReport"><fmt:message key="shipped" /></td><td align="center" class="salesRepReport"><fmt:message key="pendingProcessing" /></td>
			       <td align="center" class="salesRepReport"><fmt:message key="shipped" /></td><td align="center" class="salesRepReport"><fmt:message key="pendingProcessing" /></td>
			     </c:when>
			     <c:when test="${salesRepFilter.quarter == '2'}">
			       <td align="center" class="salesRepReport"><fmt:message key="shipped" /></td><td align="center" class="salesRepReport"><fmt:message key="pendingProcessing" /></td>
			       <td align="center" class="salesRepReport"><fmt:message key="shipped" /></td><td align="center" class="salesRepReport"><fmt:message key="pendingProcessing" /></td>
			       <td align="center" class="salesRepReport"><fmt:message key="shipped" /></td><td align="center" class="salesRepReport"><fmt:message key="pendingProcessing" /></td>
			     </c:when>
			     <c:when test="${salesRepFilter.quarter == '3'}">
			       <td align="center" class="salesRepReport"><fmt:message key="shipped" /></td><td align="center" class="salesRepReport"><fmt:message key="pendingProcessing" /></td>
			       <td align="center" class="salesRepReport"><fmt:message key="shipped" /></td><td align="center" class="salesRepReport"><fmt:message key="pendingProcessing" /></td>
			       <td align="center" class="salesRepReport"><fmt:message key="shipped" /></td><td align="center" class="salesRepReport"><fmt:message key="pendingProcessing" /></td>
			     </c:when>
			     <c:when test="${salesRepFilter.quarter == '4'}">
			       <td align="center" class="salesRepReport"><fmt:message key="shipped" /></td><td align="center" class="salesRepReport"><fmt:message key="pendingProcessing" /></td>
			       <td align="center" class="salesRepReport"><fmt:message key="shipped" /></td><td align="center" class="salesRepReport"><fmt:message key="pendingProcessing" /></td>
			       <td align="center" class="salesRepReport"><fmt:message key="shipped" /></td><td align="center" class="salesRepReport"><fmt:message key="pendingProcessing" /></td>
			     </c:when>
			     <c:when test="${salesRepFilter.quarter == '0'}">
			       <td align="center" class="salesRepReport"><fmt:message key="shipped" /></td><td align="center" class="salesRepReport"><fmt:message key="pendingProcessing" /></td>
			     </c:when>
			    </c:choose>
			  </tr>
			  
		  	  <c:forEach items="${model.salesRepReports}" var="salesReps" varStatus="outStatus">
			  <tr class="row${outStatus.index % 2}" onmouseover="changeStyleClass(this,'rowOver')" onmouseout="changeStyleClass(this,'row${outStatus.index % 2}')">
			    <c:forEach items="${salesReps}" var="salesRep" varStatus="status">
			      <c:if test="${status.first}">
					<td style="white-space: nowrap"><c:out value="${model.salesRepMap[salesRep.salesRepId].name}"/></td>
				  </c:if>
			      <td align="center" style="white-space: nowrap">
			        <c:if test="${!empty salesRep.grandTotalShipped}">
			          <fmt:message key="${siteConfig['CURRENCY'].value}" /><fmt:formatNumber value="${salesRep.grandTotalShipped}" pattern="#,##0.00" />
			          <c:if test="${status.index == 0}" ><c:set var="totalGrandTotalShipped0" value="${salesRep.grandTotalShipped + totalGrandTotalShipped0}" /></c:if>
			          <c:if test="${status.index == 1}" ><c:set var="totalGrandTotalShipped1" value="${salesRep.grandTotalShipped + totalGrandTotalShipped1}" /></c:if>
			          <c:if test="${status.index == 2}" ><c:set var="totalGrandTotalShipped2" value="${salesRep.grandTotalShipped + totalGrandTotalShipped2}" /></c:if>
			        </c:if>
			      </td>
			      <td align="center" style="white-space: nowrap">
			        <c:if test="${!empty salesRep.grandTotalPenProc}">
			          <fmt:message key="${siteConfig['CURRENCY'].value}" /><fmt:formatNumber value="${salesRep.grandTotalPenProc}" pattern="#,##0.00" />
			          <c:if test="${status.index == 0}" ><c:set var="totalGrandTotalPenProc0" value="${salesRep.grandTotalPenProc + totalGrandTotalPenProc0}" /></c:if>
			          <c:if test="${status.index == 1}" ><c:set var="totalGrandTotalPenProc1" value="${salesRep.grandTotalPenProc + totalGrandTotalPenProc1}" /></c:if>
			          <c:if test="${status.index == 2}" ><c:set var="totalGrandTotalPenProc2" value="${salesRep.grandTotalPenProc + totalGrandTotalPenProc2}" /></c:if>
			        </c:if>
			      </td>
			    </c:forEach>	
			  </tr>
			  </c:forEach>
			  <tr class="totals">
			  <td style="color:#666666; padding: 5px" align="left"><fmt:message key="total" /></td>
			  <td style="color:#666666; padding: 5px" align="center"><fmt:formatNumber value="${totalGrandTotalShipped0}" pattern="#,##0.00" /></td>
			  <td style="color:#666666; padding: 5px" align="center"><fmt:formatNumber value="${totalGrandTotalPenProc0}" pattern="#,##0.00" /></td>
			  <c:if test="${salesRepFilter.quarter == '1' or salesRepFilter.quarter == '2' or salesRepFilter.quarter == '3' or salesRepFilter.quarter == '4'}">
			    <td style="color:#666666; padding: 5px" align="center"><fmt:formatNumber value="${totalGrandTotalShipped1}" pattern="#,##0.00" /></td>
			    <td style="color:#666666; padding: 5px" align="center"><fmt:formatNumber value="${totalGrandTotalPenProc1}" pattern="#,##0.00" /></td>
			    <td style="color:#666666; padding: 5px" align="center"><fmt:formatNumber value="${totalGrandTotalShipped2}" pattern="#,##0.00" /></td>
			    <td style="color:#666666; padding: 5px" align="center"><fmt:formatNumber value="${totalGrandTotalPenProc2}" pattern="#,##0.00" /></td>
			  </c:if>	
			</table>
				  
		    <!-- end input field -->  	  
		  	</div>
		  	
		  	<!-- start button -->
			<!-- end button -->	
       	  	
	<!-- end tab -->        
	</div>
<!-- end tabs -->			
</div>
  
  <!-- end table -->
   </td><td class="boxmidr" ></td></tr>
  <tr><td class="botl"></td><td class="botr"></td></tr>
  </table>
  
<!-- end main box -->  
</div>
</form>
</sec:authorize>

  </tiles:putAttribute>
</tiles:insertDefinition>
