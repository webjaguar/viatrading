<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/security/tags" prefix="sec" %>

<tiles:insertDefinition name="admin.report.salesRepDetail2" flush="true">
  <tiles:putAttribute name="content" type="string">
    
<c:if test="${gSiteConfig['gREPORT']}">
<sec:authorize ifAnyGranted="ROLE_AEM,ROLE_ADMIN_${prop_site_id},ROLE_REPORT_SALESREP_OVERVIEWII">
<script type="text/javascript">
<!--
window.addEvent('domready', function(){			
	$$('span.toolTip').each(function(element,index) {  
         var content = element.get('title').split('::');  
         element.store('tip:title', content[0]);  
         element.store('tip:text', content[1]);  
	});
	var Tips1 = new Tips($$('.toolTip'));
});
//-->
</script> 
<form action="salesRepDetailReport2.jhtm" id="list" method="post">
<input type="hidden" id="sort" name="sort" value="${salesRepDetailFilter2.sort}" />  
<!-- main box -->
  <div id="mboxfull">
  
  <!-- start table -->
  <table cellpadding="0" cellspacing="0" border="0" class="module" >
  <tr>
    <td class="topl_g">
	  
	  <!-- breadcrumb -->
	  <p class="breadcrumb">
	    <a href="../reports"><fmt:message key="reports" /></a> &gt;
	    <fmt:message key="salesRepOverview"/> II
	  </p>
	  
	  <!-- Error -->
	  <c:if test="${!empty model.message}">
		<div class="message"><fmt:message key="${model.message}" /></div>
	  </c:if>
	  
	  <!-- header image -->
	  <img class="headerImage" src="../graphics/reports.gif" /> 
	  
  </td><td class="topr_g" ></td></tr>
  <tr><td class="boxmidlrg" >

 <!-- tabs -->
 <div class="tab-wrapper">
	<h4 title=""></h4>
	<div>
	<!-- start tab -->

	  	<div class="listdivi"></div>
	    
		  	<div class="listlight">
		  	<!-- input field -->
		  	    
		  	<table border="0" cellpadding="1" cellspacing="0" width="100%" class="listings">
			  <tr class="listingsHdr2">
			      <td class="listingsHdr3"><fmt:message key="salesRep" /></td>
			      <td class="listingsHdr3">
			        <table cellspacing="0" cellpadding="1">
			    	  <tr>
			    	    <c:choose>
			    	      <c:when test="${salesRepDetailFilter2.sort == 'merchandize DESC'}">
			    	        <td class="listingsHdr3">
			    	        <a class="hdrLink" href="#" onClick="document.getElementById('sort').value='merchandize';document.getElementById('list').submit()"><span class="toolTip" title="<fmt:message key="merchandise" />::GrandTotal - ( ShippingCost + Tax + Credit Card Fee )"><fmt:message key="merchandise" /></span></a>
			    	        </td>
			    	        <td><img src="../graphics/up.gif" border="0"></td>
			    	      </c:when>
			    	      <c:when test="${salesRepDetailFilter2.sort == 'merchandize'}">
			    	        <td class="listingsHdr3">
			    	        <a class="hdrLink" href="#" onClick="document.getElementById('sort').value='merchandize DESC';document.getElementById('list').submit()"><span class="toolTip" title="<fmt:message key="merchandise" />::GrandTotal - ( ShippingCost + Tax + Credit Card Fee )"><fmt:message key="merchandise" /></span></a>
			    	        </td>
			    	        <td><img src="../graphics/down.gif" border="0"></td>
			    	      </c:when>
			    	      <c:otherwise>
			    	        <td colspan="2" class="listingsHdr3">
			    	        <a class="hdrLink" href="#" onClick="document.getElementById('sort').value='merchandize DESC';document.getElementById('list').submit()"><span class="toolTip" title="<fmt:message key="merchandise" />::GrandTotal - ( ShippingCost + Tax + Credit Card Fee )"><fmt:message key="merchandise" /></span></a>
			    	        </td>
			    	      </c:otherwise>
			    	    </c:choose>  
			    	  </tr>
			    	</table>
			      </td>
			      <td class="listingsHdr3">
			        <table cellspacing="0" cellpadding="1">
			    	  <tr>
			    	    <c:choose>
			    	      <c:when test="${salesRepDetailFilter2.sort == 'grand_total DESC'}">
			    	        <td class="listingsHdr3">
			    	        <a class="hdrLink" href="#" onClick="document.getElementById('sort').value='grand_total';document.getElementById('list').submit()"><fmt:message key="grandTotal" /></a>
			    	        </td>
			    	        <td><img src="../graphics/up.gif" border="0"></td>
			    	      </c:when>
			    	      <c:when test="${salesRepDetailFilter2.sort == 'grand_total'}">
			    	        <td class="listingsHdr3">
			    	        <a class="hdrLink" href="#" onClick="document.getElementById('sort').value='grand_total DESC';document.getElementById('list').submit()"><fmt:message key="grandTotal" /></a>
			    	        </td>
			    	        <td><img src="../graphics/down.gif" border="0"></td>
			    	      </c:when>
			    	      <c:otherwise>
			    	        <td colspan="2" class="listingsHdr3">
			    	        <a class="hdrLink" href="#" onClick="document.getElementById('sort').value='grand_total DESC';document.getElementById('list').submit()"><fmt:message key="grandTotal" /></a>
			    	        </td>
			    	      </c:otherwise>
			    	    </c:choose>  
			    	  </tr>
			    	</table>
			      </td>
			      <td class="listingsHdr3">
			        <table cellspacing="0" cellpadding="1">
			    	  <tr>
			    	    <c:choose>
			    	      <c:when test="${salesRepDetailFilter2.sort == 'pp_order DESC'}">
			    	        <td class="listingsHdr3">
			    	        <a class="hdrLink" href="#" onClick="document.getElementById('sort').value='pp_order';document.getElementById('list').submit()"><fmt:message key="ppOrder" /></a>
			    	        </td>
			    	        <td><img src="../graphics/up.gif" border="0"></td>
			    	      </c:when>
			    	      <c:when test="${salesRepDetailFilter2.sort == 'pp_order'}">
			    	        <td class="listingsHdr3">
			    	        <a class="hdrLink" href="#" onClick="document.getElementById('sort').value='pp_order DESC';document.getElementById('list').submit()"><fmt:message key="ppOrder" /></a>
			    	        </td>
			    	        <td><img src="../graphics/down.gif" border="0"></td>
			    	      </c:when>
			    	      <c:otherwise>
			    	        <td colspan="2" class="listingsHdr3">
			    	        <a class="hdrLink" href="#" onClick="document.getElementById('sort').value='pp_order DESC';document.getElementById('list').submit()"><fmt:message key="ppOrder" /></a>
			    	        </td>
			    	      </c:otherwise>
			    	    </c:choose>  
			    	  </tr>
			    	</table>
                  </td>
			      <td class="listingsHdr3"><fmt:message key="shipAndPP" /></td>
			    </tr>
			    <c:set var="merchandize" value="0"/>
			    <c:set var="grandTotal" value="0"/>
			    <c:set var="ppOrder" value="0"/>
			    <c:set var="shipAndPP" value="0"/>
			   <c:forEach items="${model.salesRepDetailReportsList}" var="salesRepDetailReport" varStatus="status">
			  <tr class="row${status.index % 2}" onmouseover="changeStyleClass(this,'rowOver')" onmouseout="changeStyleClass(this,'row${status.index % 2}')">
			    <td class="nameCol"><c:out value="${model.salesRepMap[salesRepDetailReport.salesRepId].name}"/></td>			
			    <td class="nameCol"><fmt:message key="${siteConfig['CURRENCY'].value}" /><fmt:formatNumber value="${salesRepDetailReport.merchandize}" pattern="#,##0.00" /></td><c:set var="merchandize" value="${salesRepDetailReport.merchandize + merchandize}" />
			    <td class="nameCol"><fmt:message key="${siteConfig['CURRENCY'].value}" /><fmt:formatNumber value="${salesRepDetailReport.grandTotal}" pattern="#,##0.00" /></td><c:set var="grandTotal" value="${salesRepDetailReport.grandTotal + grandTotal}" />
			    <td class="nameCol"><fmt:message key="${siteConfig['CURRENCY'].value}" /><fmt:formatNumber value="${salesRepDetailReport.ppOrder}" pattern="#,##0.00" /></td><c:set var="ppOrder" value="${salesRepDetailReport.ppOrder + ppOrder}" />	
			    <td class="nameCol"><fmt:message key="${siteConfig['CURRENCY'].value}" /><fmt:formatNumber value="${salesRepDetailReport.grandTotal+salesRepDetailReport.ppOrder}" pattern="#,##0.00" /></td><c:set var="shipAndPP" value="${salesRepDetailReport.grandTotal+salesRepDetailReport.ppOrder +shipAndPP}" />			   
			  </tr>
			  </c:forEach>
			  <tr class="totals">
			    <td style="color:#666666; padding: 5px" colspan="1" align="left"><fmt:message key="total" /></td>
			    <td class="nameCol"><fmt:message key="${siteConfig['CURRENCY'].value}" /><fmt:formatNumber value="${merchandize}" pattern="#,##0.00" /></td>
			    <td class="nameCol"><fmt:message key="${siteConfig['CURRENCY'].value}" /><fmt:formatNumber value="${grandTotal}" pattern="#,##0.00" /></td>
			    <td class="nameCol"><fmt:message key="${siteConfig['CURRENCY'].value}" /><fmt:formatNumber value="${ppOrder}" pattern="#,##0.00" /></td>
			    <td class="nameCol"><fmt:message key="${siteConfig['CURRENCY'].value}" /><fmt:formatNumber value="${shipAndPP}" pattern="#,##0.00" /></td>
			  </tr>
			</table>
		    <!-- end input field -->  	   
		  	</div>
		  	
		  	<!-- start button -->
			<!-- end button -->	

	<!-- end tab -->        
	</div>
<!-- end tabs -->			
</div>
  
  <!-- end table -->
   </td><td class="boxmidr" ></td></tr>
  <tr><td class="botl"></td><td class="botr"></td></tr>
  </table>
  
<!-- end main box -->  
</div>
</form>

</sec:authorize>
</c:if>
  </tiles:putAttribute>
</tiles:insertDefinition>