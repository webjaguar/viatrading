<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib uri="http://www.springframework.org/security/tags" prefix="sec" %>

<tiles:insertDefinition name="admin.report.inventoryActivity" flush="true">
  <tiles:putAttribute name="content" type="string">  

<c:if test="${siteConfig['INVENTORY_HISTORY'].value == 'true'}">  
<sec:authorize ifAnyGranted="ROLE_AEM,ROLE_ADMIN_${prop_site_id},ROLE_REPORT_PRODUCT,ROLE_REPORT_INVENTORY">
<script language="JavaScript">
<!--
window.addEvent('domready', function(){			 
	 new multiBox('mbInventoryNote', {showControls : false, useOverlay: false, showNumbers: false });
});
</script>

<form  id="list" action="" name="list_form">
<input type="hidden" id="sort" name="sort" value="${inventoryActivityFilter.sort}" />
<!-- main box -->
  <div id="mboxfull">
  
  <!-- start table -->
  <table cellpadding="0" cellspacing="0" border="0" class="module" >
  <tr>
    <td class="topl_g">
	  
	  <!-- breadcrumb -->
	  <p class="breadcrumb">
	    <a href="../reports"><fmt:message key="reports" /></a> &gt;
	    <fmt:message key="inventoryActivity" />
	  </p>
	  
	  <!-- Error -->
	  <c:if test="${!empty model.message}">
		<div class="message"><fmt:message key="${model.message}" /></div>
	  </c:if>
	  
	  <!-- header image -->
	  <img class="headerImage" src="../graphics/reports.gif" alt=""/> 
	  
  </td><td class="topr_g" ></td></tr>
  <tr><td class="boxmidlrg" >

 <!-- tabs -->
 <div class="tab-wrapper">
	<h4 title=""></h4>
	<div>
	<!-- start tab -->
	
	  	<div class="listdivi"></div>
	    
		  	<div class="listlight">
		  	<!-- input field -->
		  	
		  	<table border="0" cellpadding="0" cellspacing="1" width="100%">
			  <tr>
			  <td class="pageShowing">
			  <c:if test="${model.count > 0}">
			  <fmt:message key="showing">
				<fmt:param value="${inventoryActivityFilter.offset + 1}"/>
				<fmt:param value="${model.pageEnd}"/>
				<fmt:param value="${model.count}"/>
			  </fmt:message>
			  </c:if>
			  </td>
			  <td class="pageNavi">
			  Page 
			  <c:choose>
			  <c:when test="${model.pageCount <= 100 and model.pageCount > 0}">
			  <select name="page" id="page" onchange="submit()">
			  <c:forEach begin="1" end="${model.pageCount}" var="page">
			  	<option value="${page}" <c:if test="${page == (inventoryActivityFilter.page)}">selected</c:if>>${page}</option>
			  </c:forEach>
			  </select>
			  </c:when>
			  <c:otherwise>
			  <input type="text" id="page" name="page" value="${inventoryActivityFilter.page}" size="5" class="textfield50" />
			  <input type="submit" value="go"/>
			  </c:otherwise>
			  </c:choose>
			  of <c:out value="${model.pageCount}"/>
			  | 
			  <c:if test="${inventoryActivityFilter.page == 1}"><span class="pageNaviDead"><fmt:message key="previous" /></span></c:if>
			  <c:if test="${inventoryActivityFilter.page != 1}"><a href="<c:url value="inventoryActivityReport.jhtm"><c:param name="page" value="${inventoryActivityFilter.page-1}"/></c:url>" class="pageNaviLink"><fmt:message key="previous" /></a></c:if>
			  | 
			  <c:if test="${inventoryActivityFilter.page == model.pageCount}"><span class="pageNaviDead"><fmt:message key="next" /></span></c:if>
			  <c:if test="${inventoryActivityFilter.page != model.pageCount}"><a href="<c:url value="inventoryActivityReport.jhtm"><c:param name="page" value="${inventoryActivityFilter.page+1}"/></c:url>" class="pageNaviLink"><fmt:message key="next" /></a></c:if>
			  </td>
			  </tr>
			</table>
			
		  	<table border="0" cellpadding="2" cellspacing="0" width="100%" class="listings">
			  <tr class="listingsHdr2">
			    <td class="indexCol">&nbsp;</td>
			    <td class="listingsHdr3">
			    	<table cellspacing="0" cellpadding="1">
			    	  <tr>
			    	    <c:choose>
			    	      <c:when test="${inventoryActivityFilter.sort == 'sku DESC'}">
			    	        <td class="listingsHdr3">
			    	        <a class="hdrLink" href="#" onClick="document.getElementById('sort').value='sku';document.getElementById('list').submit()"><fmt:message key="sku" /></a>
			    	        </td>
			    	        <td><img src="../graphics/up.gif" border="0"></td>
			    	      </c:when>
			    	      <c:when test="${inventoryActivityFilter.sort == 'sku'}">
			    	        <td class="listingsHdr3">
			    	        <a class="hdrLink" href="#" onClick="document.getElementById('sort').value='sku DESC';document.getElementById('list').submit()"><fmt:message key="sku" /></a>
			    	        </td>
			    	        <td><img src="../graphics/down.gif" border="0"></td>
			    	      </c:when>
			    	      <c:otherwise>
			    	        <td colspan="2" class="listingsHdr3">
			    	        <a class="hdrLink" href="#" onClick="document.getElementById('sort').value='sku';document.getElementById('list').submit()"><fmt:message key="sku" /></a>
			    	        </td>
			    	      </c:otherwise>
			    	    </c:choose>  
			    	  </tr>
			    	</table>
			    </td>
			    <td class="listingsHdr3">
			    	<table cellspacing="0" cellpadding="1">
			    	  <tr>
			    	    <c:choose>
			    	      <c:when test="${inventoryActivityFilter.sort == 'date DESC'}">
			    	        <td class="listingsHdr3">
			    	        <a class="hdrLink" href="#" onClick="document.getElementById('sort').value='date';document.getElementById('list').submit()"><fmt:message key="date" /></a>
			    	        </td>
			    	        <td><img src="../graphics/up.gif" border="0"></td>
			    	      </c:when>
			    	      <c:when test="${inventoryActivityFilter.sort == 'date'}">
			    	        <td class="listingsHdr3">
			    	        <a class="hdrLink" href="#" onClick="document.getElementById('sort').value='date DESC';document.getElementById('list').submit()"><fmt:message key="date" /></a>
			    	        </td>
			    	        <td><img src="../graphics/down.gif" border="0"></td>
			    	      </c:when>
			    	      <c:otherwise>
			    	        <td colspan="2" class="listingsHdr3">
			    	        <a class="hdrLink" href="#" onClick="document.getElementById('sort').value='date';document.getElementById('list').submit()"><fmt:message key="date" /></a>
			    	        </td>
			    	      </c:otherwise>
			    	    </c:choose>  
			    	  </tr>
			    	</table>
			     </td>
			    <td class="listingsHdr3">
			    	<table cellspacing="0" cellpadding="1">
			    	  <tr>
			    	    <c:choose>
			    	      <c:when test="${inventoryActivityFilter.sort == 'type DESC'}">
			    	        <td class="listingsHdr3">
			    	        <a class="hdrLink" href="#" onClick="document.getElementById('sort').value='type';document.getElementById('list').submit()"><fmt:message key="type" /></a>
			    	        </td>
			    	        <td><img src="../graphics/up.gif" border="0"></td>
			    	      </c:when>
			    	      <c:when test="${inventoryActivityFilter.sort == 'type'}">
			    	        <td class="listingsHdr3">
			    	        <a class="hdrLink" href="#" onClick="document.getElementById('sort').value='type DESC';document.getElementById('list').submit()"><fmt:message key="type" /></a>
			    	        </td>
			    	        <td><img src="../graphics/down.gif" border="0"></td>
			    	      </c:when>
			    	      <c:otherwise>
			    	        <td colspan="2" class="listingsHdr3">
			    	        <a class="hdrLink" href="#" onClick="document.getElementById('sort').value='type';document.getElementById('list').submit()"><fmt:message key="type" /></a>
			    	        </td>
			    	      </c:otherwise>
			    	    </c:choose>  
			    	  </tr>
			    	</table>
			    </td>
			    <td class="listingsHdr3">
			    	<table cellspacing="0" cellpadding="1">
			    	  <tr>
			    	    <td colspan="2" class="listingsHdr3">
			    	      <fmt:message key="reference" />
			    	    </td>
			    	  </tr>
			    	</table>
			    </td>
			    <td class="listingsHdr3">
			    	<table cellspacing="0" cellpadding="1">
			    	  <tr>
			    	    <c:choose>
			    	      <c:when test="${inventoryActivityFilter.sort == 'quantity DESC'}">
			    	        <td class="listingsHdr3">
			    	        <a class="hdrLink" href="#" onClick="document.getElementById('sort').value='quantity';document.getElementById('list').submit()"><fmt:message key="qty" /></a>
			    	        </td>
			    	        <td><img src="../graphics/up.gif" border="0"></td>
			    	      </c:when>
			    	      <c:when test="${inventoryActivityFilter.sort == 'quantity'}">
			    	        <td class="listingsHdr3">
			    	        <a class="hdrLink" href="#" onClick="document.getElementById('sort').value='quantity DESC';document.getElementById('list').submit()"><fmt:message key="qty" /></a>
			    	        </td>
			    	        <td><img src="../graphics/down.gif" border="0"></td>
			    	      </c:when>
			    	      <c:otherwise>
			    	        <td colspan="2" class="listingsHdr3">
			    	        <a class="hdrLink" href="#" onClick="document.getElementById('sort').value='quantity';document.getElementById('list').submit()"><fmt:message key="qty" /></a>
			    	        </td>
			    	      </c:otherwise>
			    	    </c:choose>  
			    	  </tr>
			    	</table>
			    </td>
			    <td class="listingsHdr3">
			    	<table cellspacing="0" cellpadding="1">
			    	  <tr>
			    	    <c:choose>
			    	      <c:when test="${inventoryActivityFilter.sort == 'inventory DESC'}">
			    	        <td class="listingsHdr3">
			    	        <a class="hdrLink" href="#" onClick="document.getElementById('sort').value='inventory';document.getElementById('list').submit()"><fmt:message key="total" /></a>
			    	        </td>
			    	        <td><img src="../graphics/up.gif" border="0"></td>
			    	      </c:when>
			    	      <c:when test="${inventoryActivityFilter.sort == 'inventory'}">
			    	        <td class="listingsHdr3">
			    	        <a class="hdrLink" href="#" onClick="document.getElementById('sort').value='inventory DESC';document.getElementById('list').submit()"><fmt:message key="total" /></a>
			    	        </td>
			    	        <td><img src="../graphics/down.gif" border="0"></td>
			    	      </c:when>
			    	      <c:otherwise>
			    	        <td colspan="2" class="listingsHdr3">
			    	        <a class="hdrLink" href="#" onClick="document.getElementById('sort').value='inventory';document.getElementById('list').submit()"><fmt:message key="total" /></a>
			    	        </td>
			    	      </c:otherwise>
			    	    </c:choose>  
			    	  </tr>
			    	</table>
			    </td>
			     <td class="listingsHdr3">
			    	<table cellspacing="0" cellpadding="1">
			    	  <tr>
			    	    <c:choose>
			    	      <c:when test="${inventoryActivityFilter.sort == 'inv_afs_temp DESC'}">
			    	        <td class="listingsHdr3">
			    	        <a class="hdrLink" href="#" onClick="document.getElementById('sort').value='inv_afs_temp';document.getElementById('list').submit()"><fmt:message key="inventoryAFS" /></a>
			    	        </td>
			    	        <td><img src="../graphics/up.gif" border="0"></td>
			    	      </c:when>
			    	      <c:when test="${inventoryActivityFilter.sort == 'inv_afs_temp'}">
			    	        <td class="listingsHdr3">
			    	        <a class="hdrLink" href="#" onClick="document.getElementById('sort').value='inv_afs_temp DESC';document.getElementById('list').submit()"><fmt:message key="inventoryAFS" /></a>
			    	        </td>
			    	        <td><img src="../graphics/down.gif" border="0"></td>
			    	      </c:when>
			    	      <c:otherwise>
			    	        <td colspan="2" class="listingsHdr3">
			    	        <a class="hdrLink" href="#" onClick="document.getElementById('sort').value='inv_afs_temp';document.getElementById('list').submit()"><fmt:message key="inventoryAFS" /></a>
			    	        </td>
			    	      </c:otherwise>
			    	    </c:choose>  
			    	  </tr>
			    	</table>
			    </td>
			    <td class="listingsHdr3">
			    	<fmt:message key="user" />
			    </td>
			    <td class="listingsHdr3">
			    	<fmt:message key="detail" />
			    </td>
			  </tr>
		  	  <c:forEach items="${model.inventoryActivityList}" var="inventoryActivity"	varStatus="status">
			  <tr class="row${status.index % 2}" onmouseover="changeStyleClass(this,'rowOver')" onmouseout="changeStyleClass(this,'row${status.index % 2}')">
			    <td class="indexCol">${status.index + 1}</td>
			    <td class="nameCol"><c:out value="${inventoryActivity.sku}" /></td>			
			    <td class="nameCol"><fmt:formatDate type="both" timeStyle="full" pattern="MM/dd/yyyy hh:mm a" value="${inventoryActivity.date}"/></td>
			    <td class="nameCol">
			    <c:out value="${inventoryActivity.type}" />
			    </td>			
			    <td class="nameCol">
			    	<c:if test="${inventoryActivity.type == 'packingList'}"><a href="../orders/invoice.jhtm?order=${fn:split(inventoryActivity.reference, '-')[0]}"><c:out value="${inventoryActivity.reference}" /></a></c:if>
			    	<c:if test="${inventoryActivity.type == 'purchaseOrder'}"><a href="../inventory/purchaseOrderForm.jhtm?poId=${inventoryActivity.reference}"><c:out value="${inventoryActivity.reference}" /></a></c:if>
			    	<c:if test="${inventoryActivity.type == 'process'}"><a href="../catalog/truckLoadProcess.jhtm?id=${inventoryActivity.reference}"><c:out value="${inventoryActivity.reference}" /></a></c:if>
			    </td>
			    <td class="nameCol"><c:out value="${inventoryActivity.quantity}" /></td>			
			    <td class="nameCol"><c:out value="${inventoryActivity.inventory}" /></td>
			   <td class="nameCol"><c:out value="${inventoryActivity.invAFS}" /></td>
			    
			    <td class="nameCol">
			    <c:choose>
			      <c:when test="${inventoryActivity.accessUserId == 0}">Admin</c:when>
			      <c:when test="${inventoryActivity.accessUserId == null}">Front End</c:when>
			      <c:otherwise><c:out value="${inventoryActivity.accessUser}" /></c:otherwise>
			    </c:choose>
			    </td>
			    <td class="nameCol">
			    <a href="#${inventoryActivity.id}" rel="type:element" id="mbInventoryNote${inventoryActivity.id}" class="mbInventoryNote"><img src="../graphics/question.gif"/></a>
			    <div id="${inventoryActivity.id}" class="miniWindowWrapper inventoryAdj">
		  			<div class="header">Invenotory</div>
	    	  		<fieldset class="top">
	    	    		<label class="AStitle"><fmt:message key="sku"/></label>
	    	    		<input name="__sku_" type="text" value="<c:out value='${inventoryActivity.sku}'/>" size="15"  disabled="disabled"/>
	    	  		</fieldset>
	    	  		<fieldset class="bottom">
	    	    		<label class="AStitle"><fmt:message key="inventory"/></label>
	    	    		<input name="__inventory_" type="text" value="<c:out value='${inventoryActivity.inventory}'/>" size="15"  disabled="disabled"/>
	    	  		</fieldset>
	    	  		<fieldset class="bottom">
	    	    		<label class="AStitle"><fmt:message key="adjust"/></label>
	    	    		<input name="__adjust_qty_" type="text" size="15" value="<c:out value='${inventoryActivity.quantity}'/>" disabled="disabled" />
	    	  		</fieldset>
	    	  		<fieldset class="bottom">
	    	    		<label class="AStitle"><fmt:message key="note"/></label>
	    	    		<textarea name="__adjust_note_" disabled="disabled">${inventoryActivity.note}</textarea>
	    	    	</fieldset>
	    	  	</div>
			    </td>
			  </tr>
			</c:forEach>
			</table>
			
			<table border="0" cellpadding="0" cellspacing="1" width="100%">
			  <tr> 
			  <td class="pageSize">
			  <select name="size" onchange="document.getElementById('page').value=1;submit()">
			    <c:forTokens items="10,25,50,100,200,500" delims="," var="current">
			  	  <option value="${current}" <c:if test="${current == inventoryActivityFilter.pageSize}">selected</c:if>>${current} <fmt:message key="perPage" /></option>
			    </c:forTokens>
			  </select>
			  </td>
			  </tr>
			</table>   		  
		    <!-- end input field -->
		  	</div>
		  	
		  	<!-- start button -->
		  	<div align="left" class="button">
		  	</div>
			<!-- end button -->	
      	  	
	<!-- end tab -->        
	</div>
<!-- end tabs -->			
</div>
  
  <!-- end table -->
   </td><td class="boxmidr" ></td></tr>
  <tr><td class="botl"></td><td class="botr"></td></tr>
  </table>
  
<!-- end main box -->  
</div>
	 </form> 
</sec:authorize>
</c:if>
  </tiles:putAttribute>
</tiles:insertDefinition>
