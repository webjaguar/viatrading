<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/security/tags" prefix="sec" %>

<tiles:insertDefinition name="admin.report.truckLoad" flush="true">
  <tiles:putAttribute name="content" type="string">
  
<sec:authorize ifAnyGranted="ROLE_AEM,ROLE_ADMIN_${prop_site_id},ROLE_REPORT_PROCESSING">
<script type="text/javascript">
<!--

//-->
</script>
<form name="list" id="list" action="">
<input type="hidden" id="sort" name="sort" value="${truckLoadProcessSearch.sort}" />
<!-- main box -->
  <div id="mboxfull">
  
  <!-- start table -->
  <table cellpadding="0" cellspacing="0" border="0" class="module" >
  <tr>
    <td class="topl_g">
	  
	  <!-- breadcrumb -->
	  <p class="breadcrumb">
	    <a href="../reports"><fmt:message key="reports" /></a> &gt;
	    <fmt:message key="truckLoadProcess" />
	  </p>
	  
	  <!-- Error -->
	  <c:if test="${!empty model.message}">
		<div class="message"><fmt:message key="${model.message}" /></div>
	  </c:if>
	  
	  <!-- header image -->
	  <img class="headerImage" src="../graphics/reports.gif" alt=""/> 
	  
  </td><td class="topr_g" ></td></tr>
  <tr><td class="boxmidlrg" >

 <!-- tabs -->
 <div class="tab-wrapper">
	<h4 title=""></h4>
	<div>
	<!-- start tab -->
	
	  	<div class="listdivi"></div>
	    
		  	<div class="listlight">
		  	<!-- input field -->
		  	
		  	<table border="0" cellpadding="0" cellspacing="1" width="100%">
				<tr>
					<td>&nbsp;
					</td>
					<c:if test="${model.truckLoadProcessList.nrOfElements > 0}">
						<td class="pageShowing">
							<fmt:message key="showing">
								<fmt:param value="${model.truckLoadProcessList.firstElementOnPage + 1}" />
								<fmt:param value="${model.truckLoadProcessList.lastElementOnPage + 1}" />
								<fmt:param value="${model.truckLoadProcessList.nrOfElements}" />
							</fmt:message>
						</td>
					</c:if>
					<td class="pageNavi">
						<fmt:message key="page" />
						<select name="page" id="page" onchange="submit()">
							<c:forEach begin="1" end="${model.truckLoadProcessList.pageCount}"
								var="page">
								<option value="${page}"
									<c:if test="${page == (model.truckLoadProcessList.page+1)}">selected</c:if>>
									${page}
								</option>
							</c:forEach>
						</select> 
						<fmt:message key="of" />
						<c:out value="${model.truckLoadProcessList.pageCount}" /> 
						|
						<c:if test="${model.truckLoadProcessList.firstPage}">
							<span class="pageNaviDead"><fmt:message key="previous" /></span>
						</c:if>
						<c:if test="${not model.truckLoadProcessList.firstPage}">
							<a
								href="<c:url value="truckLoadProcessReport.jhtm"><c:param name="page" value="${model.truckLoadProcess.page}"/><c:param name="size" value="${model.truckLoadProcess.pageSize}"/></c:url>"
								class="pageNaviLink"><fmt:message key="previous" /></a>
						</c:if>
						|
						<c:if test="${model.truckLoadProcessList.lastPage}">
							<span class="pageNaviDead"><fmt:message key="next" /></span>
						</c:if>
						<c:if test="${not model.truckLoadProcessList.lastPage}">
							<a
								href="<c:url value="truckLoadProcessReport.jhtm"><c:param name="page" value="${model.truckLoadProcess.page+2}"/><c:param name="size" value="${model.truckLoadProcess.pageSize}"/></c:url>"
								class="pageNaviLink"><fmt:message key="next" /></a>
						</c:if>
					</td>
				</tr>
			</table>
		  	
		  	
		  	<table border="0" cellpadding="2" cellspacing="0" width="100%" class="listings">
			  <tr class="listingsHdr2">
			    <td class="listingsHdr3">
			    	<table cellspacing="0" cellpadding="1">
			    	  <tr>
			    	    <c:choose>
			    	      <c:when test="${truckLoadProcessSearch.sort == 'id DESC'}">
			    	        <td class="listingsHdr3">
			    	        <a class="hdrLink" href="#" onClick="document.getElementById('sort').value='id';document.getElementById('list').submit()"><fmt:message key="truckLoadProcess" />ID</a>
			    	        </td>
			    	        <td><img src="../graphics/up.gif" border="0"></td>
			    	      </c:when>
			    	      <c:when test="${truckLoadProcessSearch.sort == 'id'}">
			    	        <td class="listingsHdr3">
			    	        <a class="hdrLink" href="#" onClick="document.getElementById('sort').value='id DESC';document.getElementById('list').submit()"><fmt:message key="truckLoadProcess" />ID</a>
			    	        </td>
			    	        <td><img src="../graphics/down.gif" border="0"></td>
			    	      </c:when>
			    	      <c:otherwise>
			    	        <td colspan="2" class="listingsHdr3">
			    	        <a class="hdrLink" href="#" onClick="document.getElementById('sort').value='id DESC';document.getElementById('list').submit()"><fmt:message key="truckLoadProcess" />ID</a>
			    	        </td>
			    	      </c:otherwise>
			    	    </c:choose>  
			    	  </tr>
			    	</table>
			      </td>
			    
			    
			      <td class="listingsHdr3">
			    	<table cellspacing="0" cellpadding="1">
			    	  <tr>
			    	    <c:choose>
			    	      <c:when test="${truckLoadProcessSearch.sort == 'name DESC'}">
			    	        <td class="listingsHdr3">
			    	        <a class="hdrLink" href="#" onClick="document.getElementById('sort').value='name';document.getElementById('list').submit()"><fmt:message key="truckLoadProcess" /></a>
			    	        </td>
			    	        <td><img src="../graphics/up.gif" border="0"></td>
			    	      </c:when>
			    	      <c:when test="${truckLoadProcessSearch.sort == 'name'}">
			    	        <td class="listingsHdr3">
			    	        <a class="hdrLink" href="#" onClick="document.getElementById('sort').value='name DESC';document.getElementById('list').submit()"><fmt:message key="truckLoadProcess" /></a>
			    	        </td>
			    	        <td><img src="../graphics/down.gif" border="0"></td>
			    	      </c:when>
			    	      <c:otherwise>
			    	        <td colspan="2" class="listingsHdr3">
			    	        <a class="hdrLink" href="#" onClick="document.getElementById('sort').value='name DESC';document.getElementById('list').submit()"><fmt:message key="truckLoadProcess" /></a>
			    	        </td>
			    	      </c:otherwise>
			    	    </c:choose>  
			    	  </tr>
			    	</table>
			      </td>
			      
			      
			    <td class="listingsHdr3"><fmt:message key="dateAdded" /></td>
			    <td class="listingsHdr3"><fmt:message key="lastModified" /></td>
			    <td class="listingsHdr3"><fmt:message key="status" /></td>
			      
			      <td class="listingsHdr3">
			    	<table cellspacing="0" cellpadding="1">
			    	  <tr>
			    	    <c:choose>
			    	      <c:when test="${truckLoadProcessSearch.sort == 'custom1 DESC'}">
			    	        <td class="listingsHdr3">
			    	        <a class="hdrLink" href="#" onClick="document.getElementById('sort').value='custom1';document.getElementById('list').submit()"><fmt:message key="truckLoadCustom1" /></a>
			    	        </td>
			    	        <td><img src="../graphics/up.gif" border="0"></td>
			    	      </c:when>
			    	      <c:when test="${truckLoadProcessSearch.sort == 'custom1'}">
			    	        <td class="listingsHdr3">
			    	        <a class="hdrLink" href="#" onClick="document.getElementById('sort').value='custom1 DESC';document.getElementById('list').submit()"><fmt:message key="truckLoadCustom1" /></a>
			    	        </td>
			    	        <td><img src="../graphics/down.gif" border="0"></td>
			    	      </c:when>
			    	      <c:otherwise>
			    	        <td colspan="2" class="listingsHdr3">
			    	        <a class="hdrLink" href="#" onClick="document.getElementById('sort').value='custom1 DESC';document.getElementById('list').submit()"><fmt:message key="truckLoadCustom1" /></a>
			    	        </td>
			    	      </c:otherwise>
			    	    </c:choose>  
			    	  </tr>
			    	</table>
			      </td>
			      
			      <td class="listingsHdr3">
			    	<table cellspacing="0" cellpadding="1">
			    	  <tr>
			    	    <c:choose>
			    	      <c:when test="${truckLoadProcessSearch.sort == 'custom2 DESC'}">
			    	        <td class="listingsHdr3">
			    	        <a class="hdrLink" href="#" onClick="document.getElementById('sort').value='custom2';document.getElementById('list').submit()"><fmt:message key="truckLoadCustom2" /></a>
			    	        </td>
			    	        <td><img src="../graphics/up.gif" border="0"></td>
			    	      </c:when>
			    	      <c:when test="${truckLoadProcessSearch.sort == 'custom2'}">
			    	        <td class="listingsHdr3">
			    	        <a class="hdrLink" href="#" onClick="document.getElementById('sort').value='custom2 DESC';document.getElementById('list').submit()"><fmt:message key="truckLoadCustom2" /></a>
			    	        </td>
			    	        <td><img src="../graphics/down.gif" border="0"></td>
			    	      </c:when>
			    	      <c:otherwise>
			    	        <td colspan="2" class="listingsHdr3">
			    	        <a class="hdrLink" href="#" onClick="document.getElementById('sort').value='custom2 DESC';document.getElementById('list').submit()"><fmt:message key="truckLoadCustom2" /></a>
			    	        </td>
			    	      </c:otherwise>
			    	    </c:choose>  
			    	  </tr>
			    	</table>
			      </td>
			      
			      <td class="listingsHdr3">
			    	<table cellspacing="0" cellpadding="1">
			    	  <tr>
			    	    <c:choose>
			    	      <c:when test="${truckLoadProcessSearch.sort == 'custom3 DESC'}">
			    	        <td class="listingsHdr3">
			    	        <a class="hdrLink" href="#" onClick="document.getElementById('sort').value='custom3';document.getElementById('list').submit()"><fmt:message key="truckLoadCustom3" /></a>
			    	        </td>
			    	        <td><img src="../graphics/up.gif" border="0"></td>
			    	      </c:when>
			    	      <c:when test="${truckLoadProcessSearch.sort == 'custom3'}">
			    	        <td class="listingsHdr3">
			    	        <a class="hdrLink" href="#" onClick="document.getElementById('sort').value='custom3 DESC';document.getElementById('list').submit()"><fmt:message key="truckLoadCustom3" /></a>
			    	        </td>
			    	        <td><img src="../graphics/down.gif" border="0"></td>
			    	      </c:when>
			    	      <c:otherwise>
			    	        <td colspan="2" class="listingsHdr3">
			    	        <a class="hdrLink" href="#" onClick="document.getElementById('sort').value='custom3 DESC';document.getElementById('list').submit()"><fmt:message key="truckLoadCustom3" /></a>
			    	        </td>
			    	      </c:otherwise>
			    	    </c:choose>  
			    	  </tr>
			    	</table>
			      </td>
			      
			      <td class="listingsHdr3">
			    	<table cellspacing="0" cellpadding="1">
			    	  <tr>
			    	    <c:choose>
			    	      <c:when test="${truckLoadProcessSearch.sort == 'custom4 DESC'}">
			    	        <td class="listingsHdr3">
			    	        <a class="hdrLink" href="#" onClick="document.getElementById('sort').value='custom4';document.getElementById('list').submit()"><fmt:message key="truckLoadCustom4" /></a>
			    	        </td>
			    	        <td><img src="../graphics/up.gif" border="0"></td>
			    	      </c:when>
			    	      <c:when test="${truckLoadProcessSearch.sort == 'custom4'}">
			    	        <td class="listingsHdr3">
			    	        <a class="hdrLink" href="#" onClick="document.getElementById('sort').value='custom4 DESC';document.getElementById('list').submit()"><fmt:message key="truckLoadCustom4" /></a>
			    	        </td>
			    	        <td><img src="../graphics/down.gif" border="0"></td>
			    	      </c:when>
			    	      <c:otherwise>
			    	        <td colspan="2" class="listingsHdr3">
			    	        <a class="hdrLink" href="#" onClick="document.getElementById('sort').value='custom4 DESC';document.getElementById('list').submit()"><fmt:message key="truckLoadCustom4" /></a>
			    	        </td>
			    	      </c:otherwise>
			    	    </c:choose>  
			    	  </tr>
			    	</table>
			      </td>
			      
			      <td class="listingsHdr3">Original Sku</td>
			    <td class="listingsHdr3">Produced Sku</td>
			    <td class="listingsHdr3"><fmt:message key="quantity" /></td>
			    <td class="listingsHdr3">UNIT MRSP</td>
			    <td class="listingsHdr3">EXT MRSP </td>
			    <td class="listingsHdr3"><fmt:message key="user" /></td>
			      
			  </tr>
		  	  <c:forEach items="${model.truckLoadProcessList.pageList}" var="truckLoadProcess" varStatus="status">
			  	<c:forEach items="${truckLoadProcess.originalProduct}" var="truckLoadProcessOriginal" varStatus="status">
			  <tr class="row${status.index % 2}" onmouseover="changeStyleClass(this,'rowOver')" onmouseout="changeStyleClass(this,'row${status.index % 2}')">
			    <td class="nameCol"><a href="../catalog/truckLoadProcess.jhtm?id=${truckLoadProcess.id}" class="nameLink"><c:out value="${truckLoadProcess.id}"/></a></td>
			    <td class="nameCol"><a href="../catalog/truckLoadProcess.jhtm?id=${truckLoadProcess.id}" class="nameLink"><c:out value="${truckLoadProcess.name}"/></a></td>
			    <td class="nameCol"><fmt:formatDate type="both" timeStyle="full" pattern="MM/dd/yyyy hh:mm a" value="${truckLoadProcess.startDate}"/></td>
			    <td class="nameCol"><fmt:formatDate type="both" timeStyle="full" pattern="MM/dd/yyyy hh:mm a" value="${truckLoadProcess.lastDate}"/></td>			    
				<td class="nameCol"><fmt:message key="processStatus_${truckLoadProcess.status}" /></td>	
				<td class="nameCol"><c:out value="${truckLoadProcess.custom1}"/></td>	
			  	<td class="nameCol"><c:out value="${truckLoadProcess.custom2}"/></td>	
			  	<td class="nameCol"><c:out value="${truckLoadProcess.custom3}"/></td>	
			  	<td class="nameCol"><c:out value="${truckLoadProcess.custom4}"/></td>
			  	<td class="nameCol"><c:out value="${truckLoadProcessOriginal.sku}"/></td>
			  	<td></td>
			  	<td class="nameCol"><c:out value="${truckLoadProcessOriginal.inventory}"/></td>
			  	<td></td>
			  	<td></td>
			  	<td class="nameCol"><c:out value="${truckLoadProcess.createdByName}"/></td>
			  </tr>
			  </c:forEach>
			  <c:forEach items="${truckLoadProcess.derivedProduct}" var="truckLoadProcessDerived" varStatus="status">
			  <tr class="row${status.index % 2}" onmouseover="changeStyleClass(this,'rowOver')" onmouseout="changeStyleClass(this,'row${status.index % 2}')">
			    <td class="nameCol"><a href="../catalog/truckLoadProcess.jhtm?id=${truckLoadProcess.id}" class="nameLink"><c:out value="${truckLoadProcess.id}"/></a></td>
			    <td class="nameCol"><a href="../catalog/truckLoadProcess.jhtm?id=${truckLoadProcess.id}" class="nameLink"><c:out value="${truckLoadProcess.name}"/></a></td>
			    <td class="nameCol"><fmt:formatDate type="both" timeStyle="full" pattern="MM/dd/yyyy hh:mm a" value="${truckLoadProcess.startDate}"/></td>
			    <td class="nameCol"><fmt:formatDate type="both" timeStyle="full" pattern="MM/dd/yyyy hh:mm a" value="${truckLoadProcess.lastDate}"/></td>			    
				<td class="nameCol"><fmt:message key="processStatus_${truckLoadProcess.status}" /></td>	
				<td class="nameCol"><c:out value="${truckLoadProcess.custom1}"/></td>	
			  	<td class="nameCol"><c:out value="${truckLoadProcess.custom2}"/></td>	
			  	<td class="nameCol"><c:out value="${truckLoadProcess.custom3}"/></td>	
			  	<td class="nameCol"><c:out value="${truckLoadProcess.custom4}"/></td>	
			  	<td></td>
			  	<td class="nameCol"><c:out value="${truckLoadProcessDerived.product.sku}"/></td>
			  	<td class="nameCol"><c:out value="${truckLoadProcessDerived.quantity}"/></td>
			  	<td class="nameCol"><fmt:message key="${siteConfig['CURRENCY'].value}" /><fmt:formatNumber value="${truckLoadProcessDerived.unitPrice}" pattern="#,##0.00#" maxFractionDigits="${siteConfig['MAX_FRACTION_DIGIT'].value}" /></td>
                <td class="nameCol"><fmt:message key="${siteConfig['CURRENCY'].value}" /><fmt:formatNumber value="${truckLoadProcessDerived.unitPrice * truckLoadProcessDerived.quantity}" pattern="#,##0.00#" maxFractionDigits="${siteConfig['MAX_FRACTION_DIGIT'].value}" /></td>
                <td class="nameCol"><c:out value="${truckLoadProcess.createdByName}"/></td>
			  </tr>
			  </c:forEach>
			</c:forEach>
			</table>
			
			<table border="0" cellpadding="0" cellspacing="1" width="100%">
			  <tr>
			  <td>&nbsp;</td>  
			  <td class="pageSize">
			  <select name="size" onchange="document.getElementById('page').value=1;submit()">
			    <c:forTokens items="10,25,50,100,200,500" delims="," var="current">
			  	  <option value="${current}" <c:if test="${current == truckLoadProcessSearch.pageSize}">selected</c:if>>${current} <fmt:message key="perPage" /></option>
			    </c:forTokens>
			  </select>
			  </td>
			  </tr>
			</table>	  
		    <!-- end input field -->  	  
		  	</div>
		  	
		  	<!-- start button -->
			<!-- end button -->	
       	  	
	<!-- end tab -->        
	</div>
<!-- end tabs -->			
</div>
  
  <!-- end table -->
   </td><td class="boxmidr" ></td></tr>
  <tr><td class="botl"></td><td class="botr"></td></tr>
  </table>
  
<!-- end main box -->  
</div>
</form>
</sec:authorize>

  </tiles:putAttribute>
</tiles:insertDefinition>
