<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/security/tags" prefix="sec" %>

<tiles:insertDefinition name="admin.report.ordersDaily" flush="true">
  <tiles:putAttribute name="content" type="string">
  <script language="JavaScript">
  window.addEvent('domready', function(){
		var box1 = new multiBox('mbOrder', {descClassName: 'descClassName',showNumbers: false,overlay: new overlay()});
		// Create the accordian
		var myAccordion = new Fx.Accordion($$('div.arrow_drop'), $$('div.information'), {
			display: 1, alwaysHide: true,
	    	onActive: function() {$('information').removeClass('displayNone');}
		});
		<sec:authorize ifAnyGranted="ROLE_AEM,ROLE_ADMIN_${prop_site_id},ROLE_QUICK_EXPORT">
		$('quickModeTrigger').addEvent('click',function() {
		    $('mboxfull').setStyle('margin', '0');
		    $$('.quickMode').each(function(el){
		    	el.hide();
		    });
		    $$('.quickModeRemove').each(function(el){
		    	el.destroy();
		    });
		    $$('.totals').each(function(el){
		    	el.destroy();
		    });
		    $$('.listingsHdr3').each(function(el){
		    	el.removeProperties('colspan', 'class');
		    });
		    alert('Select the whole page, copy and paste to your excel file.');		    
		 });
		 </sec:authorize> 
	});
  </script>
	<c:if test="${gSiteConfig['gREPORT']}">
		<sec:authorize ifAnyGranted="ROLE_AEM,ROLE_ADMIN_${prop_site_id},ROLE_REPORT_ORDERS_DAILY">
			<form name="list" id="list" action="">
			<!-- main box -->
			  <div id="mboxfull">
			  
			  <!-- start table -->
			  <table cellpadding="0" cellspacing="0" border="0" class="module" >
			  <tr class="quickMode">
			    <td class="topl_g">
				  
				  <!-- breadcrumb -->
				  <p class="breadcrumb">
				    <a href="../reports"><fmt:message key="reports" /></a> &gt;
				    <fmt:message key="ordersDaily" />
				  </p>
				  
				  <!-- Error -->
				  <c:if test="${!empty model.message}">
					<div class="message"><fmt:message key="${model.message}" /></div>
				  </c:if>
				  
				  <!-- header image -->
				  <img class="headerImage" id="quickModeTrigger" src="../graphics/reports.gif" alt=""/> 
				  
			  </td><td class="topr_g" ></td></tr>
			  <tr><td class="boxmidlrg" >
			
			 <!-- tabs -->
			 <div class="tab-wrapper">
				<h4 title=""></h4>
				<div>
				<!-- start tab -->
				
				  	<div class="listdivi"></div>
				    
					  	<div class="listlight">
					  	<!-- input field -->
					  	
					  		<table border="0" cellpadding="0" cellspacing="1" width="100%" class="quickMode">
							  <tr>
							  <td>
							  <c:if test="${model.count > 0}">
							  <fmt:message key="showing">
								<fmt:param value="${shipDailyOrderReportFilter.offset + 1}"/>
								<fmt:param value="${model.pageEnd}"/>
								<fmt:param value="${model.count}"/>
							  </fmt:message>
							  </c:if>  
							  </td>
							  <td class="pageNavi">
							  Page 
							  <c:choose>
							  <c:when test="${model.pageCount <= 100 and model.pageCount > 0}">
							  <select name="page" id="page" onchange="submit()">
							  <c:forEach begin="1" end="${model.pageCount}" var="page">
							  	<option value="${page}" <c:if test="${page == (shipDailyOrderReportFilter.page)}">selected</c:if>>${page}</option>
							  </c:forEach>
							  </select>
							  </c:when>
							  <c:otherwise>
							  <input type="text" id="page" name="page" value="${shipDailyOrderReportFilter.page}" size="5" class="textfield50" />
							  <input type="submit" value="go"/>
							  </c:otherwise>
							  </c:choose>
							  of <c:out value="${model.pageCount}"/>
							  | 
							  <c:if test="${shipDailyOrderReportFilter.page == 1}"><span class="pageNaviDead"><fmt:message key="previous" /></span></c:if>
							  <c:if test="${shipDailyOrderReportFilter.page != 1}"><a href="<c:url value="dailyOrderReport.jhtm"><c:param name="page" value="${shipDailyOrderReportFilter.page-1}"/></c:url>" class="pageNaviLink"><fmt:message key="previous" /></a></c:if>
							  | 
							  <c:if test="${shipDailyOrderReportFilter.page == model.pageCount}"><span class="pageNaviDead"><fmt:message key="next" /></span></c:if>
							  <c:if test="${shipDailyOrderReportFilter.page != model.pageCount}"><a href="<c:url value="dailyOrderReport.jhtm"><c:param name="page" value="${shipDailyOrderReportFilter.page+1}"/></c:url>" class="pageNaviLink"><fmt:message key="next" /></a></c:if>
							  </td>
							  </tr>
							</table>
					  	
					  	<table border="0" cellpadding="2" cellspacing="0" width="100%" class="listings">
						  <tr class="listingsHdr2">
						  	<td class="indexCol">&nbsp;</td>
						  	<td class="listingsHdr3">
						  		<a class="hdrLink" ><fmt:message key="date" /></a>
						  	</td>
						  	<td class="listingsHdr3">
						  		<a class="hdrLink" ><fmt:message key="grandTotal" /></a>
						  	</td>
						  	<td class="listingsHdr3">
						  		<a class="hdrLink" ><fmt:message key="merchandise" /></a>
						  	</td>
						  	<td class="listingsHdr3">
						  		<a class="hdrLink" ># Order</a>
						  	</td>
						  	<td class="listingsHdr3">
						  		<a class="hdrLink" >Unique User</a>
						  	</td>
						  </tr>
						  <c:set var="totalGrandTotal2" value="0.0" />
						  <c:set var="merchandizeTotal" value="0.0" />
			  			  <c:set var="totalOrders" value="0" />
			  			  <c:set var="totalUniqueUser" value="0" />
					  	  <c:forEach items="${model.salesReportsListDaily}" var="dayReport" varStatus="status">
						  <tr class="row${status.index % 2}" onmouseover="changeStyleClass(this,'rowOver')" onmouseout="changeStyleClass(this,'row${status.index % 2}')">
						    <td class="indexCol"><c:out value="${status.count}"/>.</td>
						    <td class="nameCol"><fmt:formatDate type="date" value="${dayReport.date}"/></td>			
						    <td class="nameCol"><fmt:message key="${siteConfig['CURRENCY'].value}" /><fmt:formatNumber value="${dayReport.grandTotal}" pattern="#,##0.00" /></td>
						    <td class="nameCol"><fmt:message key="${siteConfig['CURRENCY'].value}" /><fmt:formatNumber value="${dayReport.merchandize}" pattern="#,##0.00" /></td>
						    <td class="nameCol"><c:out value="${dayReport.numOrder}"/></td>	
						    <td class="nameCol"><c:out value="${dayReport.uniqueUser}"/></td>	
						    <c:set var="totalGrandTotal2" value="${dayReport.grandTotal + totalGrandTotal2}" />
						    <c:set var="merchandizeTotal" value="${dayReport.merchandize + merchandizeTotal}" />
						    <c:set var="totalOrders" value="${dayReport.numOrder + totalOrders}" />		
						    <c:set var="totalUniqueUser" value="${dayReport.uniqueUser + totalUniqueUser}" />
						  </tr>
						  </c:forEach>
						  <tr class="totals">
						             <td class="indexCol"></td>
									 <td class="nameCol" style="color:#666666; padding: 5px" align="left"><fmt:message key="total" /></td>
									 <td class="nameCol" style="color:#666666; padding: 5px" align="right"><fmt:formatNumber value="${totalGrandTotal2}" pattern="#,##0.00" /></td>
									 <td class="nameCol" style="color:#666666; padding: 5px" align="right"><fmt:formatNumber value="${merchandizeTotal}" pattern="#,##0.00" /></td>
									 <td class="nameCol" style="color:#666666; padding: 5px" align="right"><c:out value="${totalOrders}" /></td>
									 <td class="nameCol" style="color:#666666; padding: 5px" align="right"><c:out value="${totalUniqueUser}" /></td>
						</tr>
						</table>	  
					    <!-- end input field -->  	  
					    <table border="0" cellpadding="0" cellspacing="1" width="100%" class="quickMode">
						  <tr>
						  <td>&nbsp;</td>  
						  <td class="pageSize">
						  <select name="size" onchange="document.getElementById('page').value=1;submit()">
						    <c:forTokens items="10,25,50,100,200,500" delims="," var="current">
						  	  <option value="${current}" <c:if test="${current == shipDailyOrderReportFilter.pageSize}">selected</c:if>>${current} <fmt:message key="perPage" /></option>
						    </c:forTokens>
						  </select>
						  </td>
						  </tr>
						</table>
					  </div>       
				</div>
			<!-- end tabs -->			
			</div>
			  
			  <!-- end table -->
			   </td><td class="boxmidr" ></td></tr>
			  <tr><td class="botl"></td><td class="botr"></td></tr>
			  </table>
			  
			<!-- end main box -->  
			</div>
			</form>
		</sec:authorize>
	</c:if>
  </tiles:putAttribute>
</tiles:insertDefinition>




