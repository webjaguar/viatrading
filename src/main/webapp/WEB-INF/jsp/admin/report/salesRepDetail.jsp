<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/security/tags" prefix="sec" %>

<tiles:insertDefinition name="admin.report.salesRepDetail" flush="true">
  <tiles:putAttribute name="content" type="string">
    
<c:if test="${gSiteConfig['gREPORT']}">
<sec:authorize ifAnyGranted="ROLE_AEM,ROLE_ADMIN_${prop_site_id},ROLE_REPORT_SALESREP_DAILY">
<script type="text/javascript">
<!--
window.addEvent('domready', function(){			
	$$('span.toolTip').each(function(element,index) {  
	         var content = element.get('title').split('::');  
	         element.store('tip:title', content[0]);  
	         element.store('tip:text', content[1]);  
	     	});
	var Tips1 = new Tips($$('.toolTip'));
});
//-->
</script>  
<form action="salesRepDetailReport.jhtm" id="list" method="post">
<input type="hidden" id="sort" name="sort" value="${salesRepDetailFilter.sort}" />  
<!-- main box -->
  <div id="mboxfull">
  
  <!-- start table -->
  <table cellpadding="0" cellspacing="0" border="0" class="module" >
  <tr>
    <td class="topl_g">
	  
	  <!-- breadcrumb -->
	  <p class="breadcrumb">
	    <a href="../reports"><fmt:message key="reports" /></a> &gt;
	    <fmt:message key="salesRepOverview"/>
	  </p>
	  
	  <!-- Error -->
	  <c:if test="${!empty model.message}">
		<div class="message"><fmt:message key="${model.message}" /></div>
	  </c:if>
	  
	  <!-- header image -->
	  <img class="headerImage" src="../graphics/reports.gif" /> 
	  
  </td><td class="topr_g" ></td></tr>
  <tr><td class="boxmidlrg" >

 <!-- tabs -->
 <div class="tab-wrapper">
	<h4 title=""></h4>
	<div>
	<!-- start tab -->

	  	<div class="listdivi"></div>
	    
		  	<div class="listlight">
		  	<!-- input field -->
		  	    
		  	<table border="0" cellpadding="2" cellspacing="0" width="100%" class="listings">
			  <tr class="listingsHdr2">
			      <td class="listingsHdr3"><fmt:message key="salesRep" /></td>
			      <td class="listingsHdr3">
			        <table cellspacing="0" cellpadding="1">
			    	  <tr>
			    	    <c:choose>
			    	      <c:when test="${salesRepDetailFilter.sort == 'merchandize DESC'}">
			    	        <td class="listingsHdr3">
			    	        <a class="hdrLink" href="#" onClick="document.getElementById('sort').value='merchandize';document.getElementById('list').submit()"><span class="toolTip" title="<fmt:message key="merchandise" />::GrandTotal - ( ShippingCost + Tax + Credit Card Fee )"><fmt:message key="merchandise" /></span></a>
			    	        </td>
			    	        <td><img src="../graphics/up.gif" border="0"></td>
			    	      </c:when>
			    	      <c:when test="${salesRepDetailFilter.sort == 'merchandize'}">
			    	        <td class="listingsHdr3">
			    	        <a class="hdrLink" href="#" onClick="document.getElementById('sort').value='merchandize DESC';document.getElementById('list').submit()"><span class="toolTip" title="<fmt:message key="merchandise" />::GrandTotal - ( ShippingCost + Tax + Credit Card Fee )"><fmt:message key="merchandise" /></span></a>
			    	        </td>
			    	        <td><img src="../graphics/down.gif" border="0"></td>
			    	      </c:when>
			    	      <c:otherwise>
			    	        <td colspan="2" class="listingsHdr3">
			    	        <a class="hdrLink" href="#" onClick="document.getElementById('sort').value='merchandize DESC';document.getElementById('list').submit()"><span class="toolTip" title="<fmt:message key="merchandise" />::GrandTotal - ( ShippingCost + Tax + Credit Card Fee )"><fmt:message key="merchandise" /></span></a>
			    	        </td>
			    	      </c:otherwise>
			    	    </c:choose>  
			    	  </tr>
			    	</table>
			      </td>
			      <td class="listingsHdr3">
			        <table cellspacing="0" cellpadding="1">
			    	  <tr>
			    	    <c:choose>
			    	      <c:when test="${salesRepDetailFilter.sort == 'shipping_cost DESC'}">
			    	        <td class="listingsHdr3">
			    	        <a class="hdrLink" href="#" onClick="document.getElementById('sort').value='shipping_cost';document.getElementById('list').submit()"><fmt:message key="shipping" /></a>
			    	        </td>
			    	        <td><img src="../graphics/up.gif" border="0"></td>
			    	      </c:when>
			    	      <c:when test="${salesRepDetailFilter.sort == 'shipping_cost'}">
			    	        <td class="listingsHdr3">
			    	        <a class="hdrLink" href="#" onClick="document.getElementById('sort').value='shipping_cost DESC';document.getElementById('list').submit()"><fmt:message key="shipping" /></a>
			    	        </td>
			    	        <td><img src="../graphics/down.gif" border="0"></td>
			    	      </c:when>
			    	      <c:otherwise>
			    	        <td colspan="2" class="listingsHdr3">
			    	        <a class="hdrLink" href="#" onClick="document.getElementById('sort').value='shipping_cost DESC';document.getElementById('list').submit()"><fmt:message key="shipping" /></a>
			    	        </td>
			    	      </c:otherwise>
			    	    </c:choose>  
			    	  </tr>
			    	</table>
			      </td>
			      <td class="listingsHdr3">
			        <table cellspacing="0" cellpadding="1">
			    	  <tr>
			    	    <c:choose>
			    	      <c:when test="${salesRepDetailFilter.sort == 'cc_fee DESC'}">
			    	        <td class="listingsHdr3">
			    	        <a class="hdrLink" href="#" onClick="document.getElementById('sort').value='cc_fee';document.getElementById('list').submit()"><fmt:message key="ccFee" /></a>
			    	        </td>
			    	        <td><img src="../graphics/up.gif" border="0"></td>
			    	      </c:when>
			    	      <c:when test="${salesRepDetailFilter.sort == 'cc_fee'}">
			    	        <td class="listingsHdr3">
			    	        <a class="hdrLink" href="#" onClick="document.getElementById('sort').value='cc_fee DESC';document.getElementById('list').submit()"><fmt:message key="ccFee" /></a>
			    	        </td>
			    	        <td><img src="../graphics/down.gif" border="0"></td>
			    	      </c:when>
			    	      <c:otherwise>
			    	        <td colspan="2" class="listingsHdr3">
			    	        <a class="hdrLink" href="#" onClick="document.getElementById('sort').value='cc_fee DESC';document.getElementById('list').submit()"><fmt:message key="ccFee" /></a>
			    	        </td>
			    	      </c:otherwise>
			    	    </c:choose>  
			    	  </tr>
			    	</table>
			      </td>
			      <td class="listingsHdr3">
			        <table cellspacing="0" cellpadding="1">
			    	  <tr>
			    	    <c:choose>
			    	      <c:when test="${salesRepDetailFilter.sort == 'tax DESC'}">
			    	        <td class="listingsHdr3">
			    	        <a class="hdrLink" href="#" onClick="document.getElementById('sort').value='tax';document.getElementById('list').submit()"><fmt:message key="tax" /></a>
			    	        </td>
			    	        <td><img src="../graphics/up.gif" border="0"></td>
			    	      </c:when>
			    	      <c:when test="${salesRepDetailFilter.sort == 'tax'}">
			    	        <td class="listingsHdr3">
			    	        <a class="hdrLink" href="#" onClick="document.getElementById('sort').value='tax DESC';document.getElementById('list').submit()"><fmt:message key="tax" /></a>
			    	        </td>
			    	        <td><img src="../graphics/down.gif" border="0"></td>
			    	      </c:when>
			    	      <c:otherwise>
			    	        <td colspan="2" class="listingsHdr3">
			    	        <a class="hdrLink" href="#" onClick="document.getElementById('sort').value='tax DESC';document.getElementById('list').submit()"><fmt:message key="tax" /></a>
			    	        </td>
			    	      </c:otherwise>
			    	    </c:choose>  
			    	  </tr>
			    	</table>
			      </td>
			      <td class="listingsHdr3">
			        <table cellspacing="0" cellpadding="1">
			    	  <tr>
			    	    <c:choose>
			    	      <c:when test="${salesRepDetailFilter.sort == 'grand_total DESC'}">
			    	        <td class="listingsHdr3">
			    	        <a class="hdrLink" href="#" onClick="document.getElementById('sort').value='grand_total';document.getElementById('list').submit()"><fmt:message key="grandTotal" /></a>
			    	        </td>
			    	        <td><img src="../graphics/up.gif" border="0"></td>
			    	      </c:when>
			    	      <c:when test="${salesRepDetailFilter.sort == 'grand_total'}">
			    	        <td class="listingsHdr3">
			    	        <a class="hdrLink" href="#" onClick="document.getElementById('sort').value='grand_total DESC';document.getElementById('list').submit()"><fmt:message key="grandTotal" /></a>
			    	        </td>
			    	        <td><img src="../graphics/down.gif" border="0"></td>
			    	      </c:when>
			    	      <c:otherwise>
			    	        <td colspan="2" class="listingsHdr3">
			    	        <a class="hdrLink" href="#" onClick="document.getElementById('sort').value='grand_total DESC';document.getElementById('list').submit()"><fmt:message key="grandTotal" /></a>
			    	        </td>
			    	      </c:otherwise>
			    	    </c:choose>  
			    	  </tr>
			    	</table>
			      </td>
			      <td class="listingsHdr3">
			        <table cellspacing="0" cellpadding="1">
			    	  <tr>
			    	    <c:choose>
			    	      <c:when test="${salesRepDetailFilter.sort == 'num_of_order DESC'}">
			    	        <td class="listingsHdr3">
			    	        <a class="hdrLink" href="#" onClick="document.getElementById('sort').value='num_of_order';document.getElementById('list').submit()"><fmt:message key="orders" /></a>
			    	        </td>
			    	        <td><img src="../graphics/up.gif" border="0"></td>
			    	      </c:when>
			    	      <c:when test="${salesRepDetailFilter.sort == 'num_of_order'}">
			    	        <td class="listingsHdr3">
			    	        <a class="hdrLink" href="#" onClick="document.getElementById('sort').value='num_of_order DESC';document.getElementById('list').submit()"><fmt:message key="orders" /></a>
			    	        </td>
			    	        <td><img src="../graphics/down.gif" border="0"></td>
			    	      </c:when>
			    	      <c:otherwise>
			    	        <td colspan="2" class="listingsHdr3">
			    	        <a class="hdrLink" href="#" onClick="document.getElementById('sort').value='num_of_order DESC';document.getElementById('list').submit()"><fmt:message key="orders" /></a>
			    	        </td>
			    	      </c:otherwise>
			    	    </c:choose>  
			    	  </tr>
			    	</table>
                  </td>
			      <td class="listingsHdr3">
			        <table cellspacing="0" cellpadding="1">
			    	  <tr>
			    	    <c:choose>
			    	      <c:when test="${salesRepDetailFilter.sort == 'unique_user DESC'}">
			    	        <td class="listingsHdr3">
			    	        <a class="hdrLink" href="#" onClick="document.getElementById('sort').value='unique_user';document.getElementById('list').submit()"><fmt:message key="uniqueUser" /></a>
			    	        </td>
			    	        <td><img src="../graphics/up.gif" border="0"></td>
			    	      </c:when>
			    	      <c:when test="${salesRepDetailFilter.sort == 'unique_user'}">
			    	        <td class="listingsHdr3">
			    	        <a class="hdrLink" href="#" onClick="document.getElementById('sort').value='unique_user DESC';document.getElementById('list').submit()"><fmt:message key="uniqueUser" /></a>
			    	        </td>
			    	        <td><img src="../graphics/down.gif" border="0"></td>
			    	      </c:when>
			    	      <c:otherwise>
			    	        <td colspan="2" class="listingsHdr3">
			    	        <a class="hdrLink" href="#" onClick="document.getElementById('sort').value='unique_user DESC';document.getElementById('list').submit()"><fmt:message key="uniqueUser" /></a>
			    	        </td>
			    	      </c:otherwise>
			    	    </c:choose>  
			    	  </tr>
			    	</table>
			      </td>
			      <td class="listingsHdr3"><fmt:message key="newUser" /></td>
			      <td class="listingsHdr3"><fmt:message key="newUserActive" /></td>
			      <td class="listingsHdr3"><fmt:message key="averagePerOrder" /></td>
			      <td class="listingsHdr3"><fmt:message key="averagePerCustomer" /></td>
			  </tr>
			    <c:set var="merchandize" value="0"/>
			    <c:set var="shippingCost" value="0"/>
			    <c:set var="ccFee" value="0"/>
			    <c:set var="tax" value="0"/>
			    <c:set var="grandTotal" value="0"/>
			    <c:set var="numOrder" value="0"/>
			    <c:set var="uniqueUser" value="0"/>
			    <c:set var="newUser" value="0"/>
			    <c:set var="newUserActive" value="0"/>
			    <c:set var="avgPerOrder" value="0"/>
			    <c:set var="avgPerCustomer" value="0"/>
		  	  <c:forEach items="${model.salesRepDetailReportsList}" var="salesRepDetailReport" varStatus="status">
			  <tr class="row${status.index % 2}" onmouseover="changeStyleClass(this,'rowOver')" onmouseout="changeStyleClass(this,'row${status.index % 2}')">
			    <td class="nameCol"><c:out value="${model.salesRepMap[salesRepDetailReport.salesRepId].name}"/></td>			
			    <td class="numberCol"><fmt:message key="${siteConfig['CURRENCY'].value}" /><fmt:formatNumber value="${salesRepDetailReport.merchandize}" pattern="#,##0.00" /></td><c:set var="merchandize" value="${salesRepDetailReport.merchandize + merchandize}" />
			    <td class="numberCol"><fmt:message key="${siteConfig['CURRENCY'].value}" /><fmt:formatNumber value="${salesRepDetailReport.shippingCost}" pattern="#,##0.00" /></td><c:set var="shippingCost" value="${salesRepDetailReport.shippingCost + shippingCost}" />
			    <td class="numberCol"><fmt:message key="${siteConfig['CURRENCY'].value}" /><fmt:formatNumber value="${salesRepDetailReport.ccFee}" pattern="#,##0.00" /></td><c:set var="ccFee" value="${salesRepDetailReport.ccFee + ccFee}" />
			    <td class="numberCol"><fmt:message key="${siteConfig['CURRENCY'].value}" /><fmt:formatNumber value="${salesRepDetailReport.tax}" pattern="#,##0.00" /></td><c:set var="tax" value="${salesRepDetailReport.tax + tax}" />
			    <td class="numberCol"><fmt:message key="${siteConfig['CURRENCY'].value}" /><fmt:formatNumber value="${salesRepDetailReport.grandTotal}" pattern="#,##0.00" /></td><c:set var="grandTotal" value="${salesRepDetailReport.grandTotal + grandTotal}" />
			    <td class="numberCol"><c:out value="${salesRepDetailReport.numOrder}"/></td><c:set var="numOrder" value="${salesRepDetailReport.numOrder + numOrder}" />	
			    <td class="numberCol"><c:out value="${salesRepDetailReport.uniqueUser}"/></td><c:set var="uniqueUser" value="${salesRepDetailReport.uniqueUser + uniqueUser}" />		
			    <td class="numberCol"><c:out value="${salesRepDetailReport.newUser}"/></td><c:set var="newUser" value="${salesRepDetailReport.newUser + newUser}" />	
			    <td class="numberCol"><c:out value="${salesRepDetailReport.newUserActive}"/></td><c:set var="newUserActive" value="${salesRepDetailReport.newUserActive + newUserActive}" />	
			    <td class="numberCol"><fmt:message key="${siteConfig['CURRENCY'].value}" /><fmt:formatNumber value="${salesRepDetailReport.avgGrandTotal}" pattern="#,##0.00" /></td><c:set var="avgPerOrder" value="${salesRepDetailReport.avgGrandTotal + avgPerOrder}" />		
			    <td class="numberCol"><fmt:message key="${siteConfig['CURRENCY'].value}" /><fmt:formatNumber value="${salesRepDetailReport.avgPerCustomer}" pattern="#,##0.00" /></td><c:set var="avgPerCustomer" value="${salesRepDetailReport.avgPerCustomer + avgPerCustomer}" />
			  </tr>
			  </c:forEach>
			  <tr class="totals">
			    <td style="color:#666666; padding: 5px" colspan="1" align="left"><fmt:message key="total" /></td>
			    <td class="numberCol"><fmt:message key="${siteConfig['CURRENCY'].value}" /><fmt:formatNumber value="${merchandize}" pattern="#,##0.00" /></td>
			    <td class="numberCol"><fmt:message key="${siteConfig['CURRENCY'].value}" /><fmt:formatNumber value="${shippingCost}" pattern="#,##0.00" /></td>
			    <td class="numberCol"><fmt:message key="${siteConfig['CURRENCY'].value}" /><fmt:formatNumber value="${ccFee}" pattern="#,##0.00" /></td>
			    <td class="numberCol"><fmt:message key="${siteConfig['CURRENCY'].value}" /><fmt:formatNumber value="${tax}" pattern="#,##0.00" /></td>
			    <td class="numberCol"><fmt:message key="${siteConfig['CURRENCY'].value}" /><fmt:formatNumber value="${grandTotal}" pattern="#,##0.00" /></td>
			    <td class="numberCol"><c:out value="${numOrder}"/></td>
			    <td class="numberCol"><c:out value="${uniqueUser}"/></td>
			    <td class="numberCol"><c:out value="${newUser}"/></td>
			    <td class="numberCol"><c:out value="${newUserActive}"/></td>
			    <td class="numberCol"><fmt:message key="${siteConfig['CURRENCY'].value}" />[<fmt:formatNumber value="${grandTotal/numOrder}" pattern="#,##0.00" />]</td>
			    <td class="numberCol"><fmt:message key="${siteConfig['CURRENCY'].value}" /><fmt:formatNumber value="${grandTotal/uniqueUser}" pattern="#,##0.00" /></td>
			  </tr>
			</table>
		    <!-- end input field -->  	  
		  	</div>
		  	
		  	<!-- start button -->
			<!-- end button -->	

	<!-- end tab -->        
	</div>
<!-- end tabs -->			
</div>
  
  <!-- end table -->
   </td><td class="boxmidr" ></td></tr>
  <tr><td class="botl"></td><td class="botr"></td></tr>
  </table>
  
<!-- end main box -->  
</div>
</form>

</sec:authorize>
</c:if>
  </tiles:putAttribute>
</tiles:insertDefinition>