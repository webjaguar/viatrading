<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/security/tags" prefix="sec" %>

<tiles:insertDefinition name="admin.report.salesRepsDaily" flush="true">
  <tiles:putAttribute name="content" type="string">
  
<sec:authorize ifAnyGranted="ROLE_AEM,ROLE_ADMIN_${prop_site_id},ROLE_REPORT_SALESREP_DAILY">
<script type="text/javascript">
<!--

//-->
</script>
<form name="salesRepList" id="salesRepList" action="">
<!-- main box -->
  <div id="mboxfull">
  
  <!-- start table -->
  <table cellpadding="0" cellspacing="0" border="0" class="module" >
  <tr>
    <td class="topl_g">
	  
	  <!-- breadcrumb -->
	  <p class="breadcrumb">
	    <a href="../reports"><fmt:message key="reports" /></a> &gt;
	    <fmt:message key="salesRep" /> <fmt:message key="daily" />&gt;
	    <c:out value="${model.salesRep.name}" />
	  </p>
	  
	  <!-- Error -->
	  <c:if test="${!empty model.message}">
		<div class="message"><fmt:message key="${model.message}" /></div>
	  </c:if>
	  
	  <!-- header image -->
	  <img class="headerImage" src="../graphics/reports.gif" alt=""/> 

  </td><td class="topr_g" ></td></tr>
  <tr><td class="boxmidlrg" >

 <!-- tabs -->
 <div class="tab-wrapper">
	<h4 title=""></h4>
	<div>
	<!-- start tab -->
	
	  	<div class="listdivi"></div>
	    
		  	<div class="listlight">
		  	<!-- input field -->
		  	
		  	<table border="0" cellpadding="2" cellspacing="0" width="100%" class="listings">
			  <tr class="listingsHdr2">
				  <td class="indexCol">&nbsp;</td>
			      <td align="center" class="listingsHdr3"><fmt:message key="date" /></td>
				  <td align="center" class="listingsHdr3"><fmt:message key="shipped" /></td>
				  <td align="center" class="listingsHdr3"># Orders</td>
				  <td align="center" class="listingsHdr3"># Unique User</td>
				  <td align="center" class="listingsHdr3"><fmt:message key="pendingProcessing" /></td>
				  <td align="center" class="listingsHdr3"># Orders</td>
				  <td align="center" class="listingsHdr3"># Unique User</td>
			  </tr>
			  <c:set var="totalGrandTotalShipped" value="0.0" />
			  <c:set var="totalGrandTotalPenProc" value="0.0" />
			  <c:set var="totalNumOrderShipped" value="0" />
			  <c:set var="totalUniqueUserShipped" value="0" />
			  <c:set var="totalNumOrderPenProc" value="0" />
			  <c:set var="totalUniqueUserPenProc" value="0" />
		  	  <c:forEach items="${model.salesRepReport}" var="salesRep" varStatus="status">
			  <tr class="row${status.index % 2}" onmouseover="changeStyleClass(this,'rowOver')" onmouseout="changeStyleClass(this,'row${status.index % 2}')">
				  <td class="indexCol">
					<c:out value="${status.count}" />.
				  </td>
				  <td align="center"><fmt:formatDate type="date" timeStyle="default" value="${salesRep.orderDate}"/></td>
			      <td align="right" style="white-space: nowrap">
			        <c:if test="${!empty salesRep.grandTotalShipped}">
			          <fmt:message key="${siteConfig['CURRENCY'].value}" /><fmt:formatNumber value="${salesRep.grandTotalShipped}" pattern="#,##0.00" />
			          <c:set var="totalGrandTotalShipped" value="${salesRep.grandTotalShipped + totalGrandTotalShipped}" />
			        </c:if>
			      </td>
			      <td class="numberCol" style="white-space: nowrap">
			          <c:out value="${salesRep.numOrderShipped}" />
			          <c:set var="totalNumOrderShipped" value="${salesRep.numOrderShipped + totalNumOrderShipped}" />
			      </td>
			      <td class="numberCol" style="white-space: nowrap">
			          <c:out value="${salesRep.uniqueUserShipped}" />
			          <c:set var="totalUniqueUserShipped" value="${salesRep.uniqueUserShipped + totalUniqueUserShipped}" />
			      </td>
			      <td class="numberCol" style="white-space: nowrap">
			        <c:if test="${!empty salesRep.grandTotalPenProc}">
			          <fmt:message key="${siteConfig['CURRENCY'].value}" /><fmt:formatNumber value="${salesRep.grandTotalPenProc}" pattern="#,##0.00" />
			          <c:set var="totalGrandTotalPenProc" value="${salesRep.grandTotalPenProc + totalGrandTotalPenProc}" />
			        </c:if>
			      </td>
			      <td class="numberCol" style="white-space: nowrap">
			          <c:out value="${salesRep.numOrderPenProc}" />
			          <c:set var="totalNumOrderPenProc" value="${salesRep.numOrderPenProc + totalNumOrderPenProc}" />
			      </td>
			      <td class="numberCol" style="white-space: nowrap">
			          <c:out value="${salesRep.uniqueUserPenProc}" />
			          <c:set var="totalUniqueUserPenProc" value="${salesRep.uniqueUserPenProc + totalUniqueUserPenProc}" />
			      </td>
			  </tr>
			  </c:forEach>	
			  <tr class="totals">
			  <td style="color:#666666; padding: 5px" colspan="2" align="left"><fmt:message key="total" /></td>
			  <td class="numberCol"><fmt:formatNumber value="${totalGrandTotalShipped}" pattern="#,##0.00" /></td>
			  <td class="numberCol"><fmt:formatNumber value="${totalNumOrderShipped}" pattern="#,##0" /></td>
			  <td class="numberCol"><fmt:formatNumber value="${totalUniqueUserShipped}" pattern="#,##0" /></td>
			  <td class="numberCol"><fmt:formatNumber value="${totalGrandTotalPenProc}" pattern="#,##0.00" /></td>
			  <td class="numberCol"><fmt:formatNumber value="${totalNumOrderPenProc}" pattern="#,##0" /></td>
			  <td class="numberCol"><fmt:formatNumber value="${totalUniqueUserPenProc}" pattern="#,##0" /></td>
			</tr>
			</table>
				  
		    <!-- end input field -->  	  
		  	</div>
		  	
		  	<!-- start button -->
			<!-- end button -->	
       	  	
	<!-- end tab -->        
	</div>
<!-- end tabs -->			
</div>
  
  <!-- end table -->
   </td><td class="boxmidr" ></td></tr>
  <tr><td class="botl"></td><td class="botr"></td></tr>
  </table>
  
<!-- end main box -->  
</div>
</form>
</sec:authorize>

  </tiles:putAttribute>
</tiles:insertDefinition>
