<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/security/tags" prefix="sec" %>

<tiles:insertDefinition name="admin.report.customers" flush="true">
  <tiles:putAttribute name="content" type="string">
  
<sec:authorize ifAnyGranted="ROLE_AEM,ROLE_ADMIN_${prop_site_id},ROLE_REPORT_CUSTOMERS">
<script type="text/javascript">
<!--

//-->
</script>
<form name="customerList" id="list" action="">
<input type="hidden" id="sort" name="sort" value="${customerFilter.sort}" />
<!-- main box -->
  <div id="mboxfull">
  
  <!-- start table -->
  <table cellpadding="0" cellspacing="0" border="0" class="module" >
  <tr>
    <td class="topl_g">
	  
	  <!-- breadcrumb -->
	  <p class="breadcrumb">
	    <a href="../reports"><fmt:message key="reports" /></a> &gt;
	    <fmt:message key="customersOverview" />
	  </p>
	  
	  <!-- Error -->
	  <c:if test="${!empty model.message}">
		<div class="message"><fmt:message key="${model.message}" /></div>
	  </c:if>
	  
	  <!-- header image -->
	  <img class="headerImage" src="../graphics/reports.gif" alt=""/> 
	  
  </td><td class="topr_g" ></td></tr>
  <tr><td class="boxmidlrg" >

 <!-- tabs -->
 <div class="tab-wrapper">
	<h4 title=""></h4>
	<div>
	<!-- start tab -->
	
	  	<div class="listdivi"></div>
	    
		  	<div class="listlight">
		  	<!-- input field -->
		  	
		  	<table border="0" cellpadding="0" cellspacing="1" width="100%">
				<tr>
					<td>&nbsp;
					</td>
					<c:if test="${model.customerReports.nrOfElements > 0}">
						<td class="pageShowing">
							<fmt:message key="showing">
								<fmt:param value="${model.customerReports.firstElementOnPage + 1}" />
								<fmt:param value="${model.customerReports.lastElementOnPage + 1}" />
								<fmt:param value="${model.customerReports.nrOfElements}" />
							</fmt:message>
						</td>
					</c:if>
					<td class="pageNavi">
						<fmt:message key="page" />
						<select name="page" id="page" onchange="submit()">
							<c:forEach begin="1" end="${model.customerReports.pageCount}"
								var="page">
								<option value="${page}"
									<c:if test="${page == (model.customerReports.page+1)}">selected</c:if>>
									${page}
								</option>
							</c:forEach>
						</select> 
						<fmt:message key="of" />
						<c:out value="${model.customerReports.pageCount}" /> 
						|
						<c:if test="${model.customerReports.firstPage}">
							<span class="pageNaviDead"><fmt:message key="previous" /></span>
						</c:if>
						<c:if test="${not model.customerReports.firstPage}">
							<a
								href="<c:url value="customerReport.jhtm"><c:param name="page" value="${model.customerReports.page}"/><c:param name="size" value="${model.inactiveCustomerReports.pageSize}"/></c:url>"
								class="pageNaviLink"><fmt:message key="previous" /></a>
						</c:if>
						|
						<c:if test="${model.customerReports.lastPage}">
							<span class="pageNaviDead"><fmt:message key="next" /></span>
						</c:if>
						<c:if test="${not model.customerReports.lastPage}">
							<a
								href="<c:url value="customerReport.jhtm"><c:param name="page" value="${model.customerReports.page+2}"/><c:param name="size" value="${model.customerReports.pageSize}"/></c:url>"
								class="pageNaviLink"><fmt:message key="next" /></a>
						</c:if>
					</td>
				</tr>
			</table>
		  	
		  	<table border="0" cellpadding="2" cellspacing="0" width="100%" class="listings">
			  <tr class="listingsHdr2">
			    <td class="indexCol">&nbsp;</td>
			    <td class="listingsHdr3">
			    	<table cellspacing="0" cellpadding="1">
			    	  <tr>
			    	    <c:choose>
			    	      <c:when test="${customerFilter.sort == 'last_name DESC, first_name DESC'}">
			    	        <td class="listingsHdr3">
			    	        <a class="hdrLink" href="#" onClick="document.getElementById('sort').value='last_name, first_name';document.getElementById('list').submit()"><fmt:message key="customer" /></a>
			    	        </td>
			    	        <td><img src="../graphics/up.gif" border="0"></td>
			    	      </c:when>
			    	      <c:when test="${customerFilter.sort == 'last_name, first_name'}">
			    	        <td class="listingsHdr3">
			    	        <a class="hdrLink" href="#" onClick="document.getElementById('sort').value='last_name DESC, first_name DESC';document.getElementById('list').submit()"><fmt:message key="customer" /></a>
			    	        </td>
			    	        <td><img src="../graphics/down.gif" border="0"></td>
			    	      </c:when>
			    	      <c:otherwise>
			    	        <td colspan="2" class="listingsHdr3">
			    	        <a class="hdrLink" href="#" onClick="document.getElementById('sort').value='last_name DESC, first_name DESC';document.getElementById('list').submit()"><fmt:message key="customer" /></a>
			    	        </td>
			    	      </c:otherwise>
			    	    </c:choose>  
			    	  </tr>
			    	</table>
			    </td>
			    <td align="left">
			    	<table cellspacing="0" cellpadding="1">
			    	  <tr>
			    	    <c:choose>
			    	      <c:when test="${customerFilter.sort == 'company DESC'}">
			    	        <td class="listingsHdr3">
			    	        <a class="hdrLink" href="#" onClick="document.getElementById('sort').value='company';document.getElementById('list').submit()"><fmt:message key="company" /></a>
			    	        </td>
			    	        <td><img src="../graphics/up.gif" border="0"></td>
			    	      </c:when>
			    	      <c:when test="${customerFilter.sort == 'company'}">
			    	        <td class="listingsHdr3">
			    	        <a class="hdrLink" href="#" onClick="document.getElementById('sort').value='company DESC';document.getElementById('list').submit()"><fmt:message key="company" /></a>
			    	        </td>
			    	        <td><img src="../graphics/down.gif" border="0"></td>
			    	      </c:when>
			    	      <c:otherwise>
			    	        <td colspan="2" class="listingsHdr3">
			    	        <a class="hdrLink" href="#" onClick="document.getElementById('sort').value='company DESC';document.getElementById('list').submit()"><fmt:message key="company" /></a>
			    	        </td>
			    	      </c:otherwise>
			    	    </c:choose>  
			    	  </tr>
			    	</table>
			    </td>
			    <td align="left" class="listingsHdr3"><fmt:message key="salesRep" /></td>
			    <td align="center">
			    	<table cellspacing="0" cellpadding="1">
			    	  <tr>
			    	    <c:choose>
			    	      <c:when test="${customerFilter.sort == 'order_count DESC'}">
			    	        <td class="listingsHdr3">
			    	        <a class="hdrLink" href="#" onClick="document.getElementById('sort').value='order_count';document.getElementById('list').submit()"><fmt:message key="orderCount" /></a>
			    	        </td>
			    	        <td><img src="../graphics/up.gif" border="0"></td>
			    	      </c:when>
			    	      <c:when test="${customerFilter.sort == 'order_count'}">
			    	        <td class="listingsHdr3">
			    	        <a class="hdrLink" href="#" onClick="document.getElementById('sort').value='order_count DESC';document.getElementById('list').submit()"><fmt:message key="orderCount" /></a>
			    	        </td>
			    	        <td><img src="../graphics/down.gif" border="0"></td>
			    	      </c:when>
			    	      <c:otherwise>
			    	        <td colspan="2" class="listingsHdr3">
			    	        <a class="hdrLink" href="#" onClick="document.getElementById('sort').value='order_count DESC';document.getElementById('list').submit()"><fmt:message key="orderCount" /></a>
			    	        </td>
			    	      </c:otherwise>
			    	    </c:choose>  
			    	  </tr>
			    	</table>
			    </td>
			    <td align="center">
			    	<table cellspacing="0" cellpadding="1">
			    	  <tr>
			    	    <c:choose>
			    	      <c:when test="${customerFilter.sort == 'grand_total DESC'}">
			    	        <td class="listingsHdr3">
			    	        <a class="hdrLink" href="#" onClick="document.getElementById('sort').value='grand_total';document.getElementById('list').submit()"><fmt:message key="orderTotal" /></a>
			    	        </td>
			    	        <td><img src="../graphics/up.gif" border="0"></td>
			    	      </c:when>
			    	      <c:when test="${customerFilter.sort == 'grand_total'}">
			    	        <td class="listingsHdr3">
			    	        <a class="hdrLink" href="#" onClick="document.getElementById('sort').value='grand_total DESC';document.getElementById('list').submit()"><fmt:message key="orderTotal" /></a>
			    	        </td>
			    	        <td><img src="../graphics/down.gif" border="0"></td>
			    	      </c:when>
			    	      <c:otherwise>
			    	        <td colspan="2" class="listingsHdr3">
			    	        <a class="hdrLink" href="#" onClick="document.getElementById('sort').value='grand_total DESC';document.getElementById('list').submit()"><fmt:message key="orderTotal" /></a>
			    	        </td>
			    	      </c:otherwise>
			    	    </c:choose>  
			    	  </tr>
			    	</table>
			    </td>
			  </tr>
			  <c:set var="totalCountTotal" value="0"/>
			  <c:set var="totalSubTotal" value="0"/>
		  	  <c:forEach items="${model.customerReports.pageList}" var="customer"	varStatus="status">
			  <tr class="row${status.index % 2}" onmouseover="changeStyleClass(this,'rowOver')" onmouseout="changeStyleClass(this,'row${status.index % 2}')">
			    <td class="indexCol"><c:out value="${status.count + model.orders.firstElementOnPage}"/>.</td>
			    <td class="nameCol"><a href="../customers/customer.jhtm?id=<c:out value="${customer.userId}" />"><c:out value="${customer.firstName}" /> <c:out value="${customer.lastName}" /></a></td>			
			    <td align="left"><c:out value="${customer.address.company}"/></td>
			    <td align="left"><c:out value="${model.salesRepMap[customer.salesRepId].name}"/></td>
			    <td class="numberCol"><c:out value="${customer.orderCount}" /></td><c:set var="totalCountTotal" value="${customer.orderCount + totalCountTotal}" />			
				<td class="numberCol" style="padding-right: 10px"><fmt:message key="${siteConfig['CURRENCY'].value}" /><fmt:formatNumber value="${customer.ordersGrandTotal}" pattern="#,##0.00" /></td><c:set var="totalSubTotal" value="${customer.ordersGrandTotal + totalSubTotal}" />
			  </tr>
			</c:forEach>
			  <tr class="totals">
			    <td style="color:#666666; padding: 5px" colspan="4" align="left"><fmt:message key="total" /></td>
			    <td class="numberCol"><fmt:formatNumber value="${totalCountTotal}" pattern="#,##0" /></td>
			    <td class="numberCol"><fmt:formatNumber value="${totalSubTotal}" pattern="#,##0" /></td>
			  </tr>	
              <tr>
              	<td colspan="10">
					<table border="0" cellpadding="0" cellspacing="1" width="100%">
					  <tr>
					  <td>&nbsp;</td>  
					  <td class="pageSize">
					  <select name="size" onchange="document.getElementById('page').value=1;submit()">
					    <c:forTokens items="10,25,50,100,200,500" delims="," var="current">
					  	  <option value="${current}" <c:if test="${current == model.customerReports.pageSize}">selected</c:if>>${current} <fmt:message key="perPage" /></option>
					    </c:forTokens>
					  </select>
					  </td>
					  </tr>
					</table>
				</td>
			  </tr>
			</table>  			  
		    <!-- end input field -->  	  
		  	</div>
		  	
		  	<!-- start button -->
			<!-- end button -->	
       	  	
	<!-- end tab -->        
	</div>
<!-- end tabs -->			
</div>
       
  <!-- end table -->
   </td><td class="boxmidr" ></td></tr>
  <tr><td class="botl"></td><td class="botr"></td></tr>
  </table>
 
<!-- end main box -->  
</div>
</form>  
</sec:authorize>

  </tiles:putAttribute>
</tiles:insertDefinition>
