<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/security/tags" prefix="sec" %>

<tiles:insertDefinition name="admin.report.metricOverview" flush="true">
  <tiles:putAttribute name="content" type="string">
<c:if test="${gSiteConfig['gREPORT']}">
<sec:authorize ifAnyGranted="ROLE_AEM,ROLE_ADMIN_${prop_site_id},ROLE_REPORT_CUSTOMER_METRIC">
<script type="text/javascript">
<!--
window.addEvent('domready', function(){			
	$$('span.toolTip').each(function(element,index) {  
	         var content = element.get('title').split('::');  
	         element.store('tip:title', content[0]);  
	         element.store('tip:text', content[1]);  
	     	});
	var Tips1 = new Tips($$('.toolTip'));
});
//-->
</script>  
<!-- main box -->
  <div id="mboxfull">
 
  <!-- start table -->
  <table cellpadding="0" cellspacing="0" border="0" class="module" >
  <tr>
    <td class="topl_g">
	  
	  <!-- breadcrumb -->
	  <p class="breadcrumb">
	    <a href="../reports"><fmt:message key="reports" /></a> &gt;
	    <fmt:message key="metricOverviewReport"/>
	  </p>
	  
	  <!-- Error -->
	  <c:if test="${!empty model.message}">
		<div class="message"><fmt:message key="${model.message}" /></div>
	  </c:if>
	  
	  <!-- header image -->
	  <img class="headerImage" src="../graphics/reports.gif" /> 
	  
  </td><td class="topr_g" ></td></tr>
  <tr><td class="boxmidlrg" >

 <!-- tabs -->
 <div class="tab-wrapper">
	<h4 title=""></h4>
	<div>
	<!-- start tab -->

	  	<div class="listdivi"></div>
	    
		  	<div class="listlight">
		  	<!-- input field -->
		  	    
		  	<table border="0" cellpadding="2" cellspacing="0" width="100%" class="listings">
			  <tr class="listingsHdr2 hdr2BackgroundColor align">			  
			     	 <td class="listingsHdr3" align="left">CUSTOMER METRICS</td>
			     <c:forEach items="${model.fiveYears}"  var="year">
			     	  <td class="listingsHdr3" ><c:out value="${year}"/></td>
			     </c:forEach>
			     	<td class="listingsHdr3" ><fmt:message key="total" /></td>
			  </tr>
			    
			 <tr class="row${status.index % 2}" onmouseover="changeStyleClass(this,'rowOver')" onmouseout="changeStyleClass(this,'row${status.index % 2}')">
			  <td class="nameCol small bold" colspan="1" align="left"><fmt:message key="newRegistration" /></td>
			 
			  <c:forEach items="${model.reports}" var="reports" varStatus="status">
			    <td class="numberCol small align"><fmt:formatNumber value="${reports.newRegistrations}" pattern="#,##0"/><c:set var="totalNewRegistrations" value="${reports.newRegistrations+totalNewRegistrations}"/></td>
			  </c:forEach>
			   <td class="numberCol small align bold"><fmt:formatNumber  value="${totalNewRegistrations}" pattern="#,##0"/></td>
			 </tr> 
			 
			 <tr class="row${status.index % 2}" onmouseover="changeStyleClass(this,'rowOver')" onmouseout="changeStyleClass(this,'row${status.index % 2}')">
			  <td class="nameCol small bold" colspan="1" align="left"><fmt:message key="newActivation" /></td>
			  <c:forEach items="${model.reports}" var="reports" varStatus="status">
			    <td class="numberCol small align"><fmt:formatNumber value="${reports.newActivations}" pattern="#,##0"/><c:set var="totalNewActivations" value="${reports.newActivations+totalNewActivations}"/></td>
			   </c:forEach>
			   <td class="numberCol small align bold"><fmt:formatNumber value="${totalNewActivations}" pattern="#,##0"/></td>
			 </tr>
			  <tr>
			  <td  class="nameCol small colorStyle bold" align="left"><fmt:message key="closeRate" /></td>
			  <c:forEach items="${model.reports}" var="reports" varStatus="status">			  
			    <td class="numberCol small align colorStyle">
			    	<c:choose>
			    		<c:when test="${reports.newRegistrations!=0}">
			    			<fmt:formatNumber value="${(reports.newActivations/reports.newRegistrations)*100}" pattern="#,##0.00" />%
			    		</c:when>
			    		<c:otherwise>0%</c:otherwise>
			    	</c:choose>			    	
			    </td>
			   </c:forEach>
			   <c:choose>
		    		<c:when test="${totalNewRegistrations!=0}">
		    			<td class="numberCol small align colorStyle bold"><fmt:formatNumber value="${(totalNewActivations/totalNewRegistrations)*100}" pattern="#,##0.00"/>%</td>
		    		</c:when>
		    		<c:otherwise><td class="numberCol small align colorStyle bold" >0% </td></c:otherwise>
			   </c:choose>	
			 </tr>	 
			 <tr>
			 <td class="nameCol small bold colorRed" colspan="1" align="left"><fmt:message key="totalSpend" /></td>
			 <c:forEach items="${model.reports}" var="reports" varStatus="status">
			 	<td class="numberCol small align colorRed">$<fmt:formatNumber value="${reports.totalSpend}" pattern="#,##0"/><c:set var="totalTotalSpend" value="${reports.totalSpend+totalTotalSpend}"/></td>
			 </c:forEach>
			 	<td class="numberCol small bold align colorRed">$<fmt:formatNumber value="${totalTotalSpend}" pattern="#,##0"/></td>			 
			 </tr>
			  
			 <tr class="row${status.index % 2}" onmouseover="changeStyleClass(this,'rowOver')" onmouseout="changeStyleClass(this,'row${status.index % 2}')">
			  <td class="nameCol small bold" colspan="1" align="left"><fmt:message key="totalRegistration" /></td>
			  <c:forEach items="${model.reports}" var="reports" varStatus="status">
			    <td class="numberCol small align"><fmt:formatNumber value="${reports.totalRegistration}"pattern="#,##0"/><c:set var="totalRegistrations" value="${reports.totalRegistration+totalRegistrations}"/></td>			  
			   </c:forEach>
			   <c:choose>
			   	<c:when test="${model.totalRegistrations == 0}">
			   		<td class="numberCol small bold align"><fmt:formatNumber value="${totalRegistrations}" pattern="#,##0"/></td>
			   	</c:when>
			   	<c:otherwise>
			   		<td class="numberCol small bold align"><fmt:formatNumber value="${model.totalRegistrations}" pattern="#,##0"/></td>
			   	</c:otherwise>
			   </c:choose>			   	
			 </tr> 
			 
			 <tr class="row${status.index % 2}" onmouseover="changeStyleClass(this,'rowOver')" onmouseout="changeStyleClass(this,'row${status.index % 2}')">
			  <td class="nameCol small bold" colspan="1" align="left"><fmt:message key="totalActivation" /></td>
			  <c:forEach items="${model.reports}" var="reports" varStatus="status">
			    <td class="numberCol small align">
			    	<fmt:formatNumber value="${reports.totalActivations}" pattern="#,##0"/><c:set var="totalActivations" value="${reports.totalActivations+totalActivations}"/></td>
			   </c:forEach>
			   <c:choose>
			   	<c:when test="${model.totalRegistrations == 0}">
			   		<td class="numberCol small align bold"><fmt:formatNumber value="${totalActivations}" pattern="#,##0"/></td>
			   	</c:when>
			   	<c:otherwise>
			   		<td class="numberCol small align bold"><fmt:formatNumber value="${model.totalActivations}" pattern="#,##0"/></td>
			   	</c:otherwise>
			   	</c:choose>
			 </tr>
			  <tr>
			  <td class="nameCol small colorStyle bold" align="left"><fmt:message key="allTimeCloseRate" /></td>
			  <c:forEach items="${model.reports}" var="reports" varStatus="status">			  
			    <td class="numberCol small colorStyle align">
			    	<c:choose>
			    		<c:when test="${reports.totalRegistration!=0}">
			    			<fmt:formatNumber value="${(reports.totalActivations/reports.totalRegistration)*100}"pattern="#,##0.00" />%
			    		</c:when>
			    		<c:otherwise>0%</c:otherwise>
			    	</c:choose>				    		    	
			    </td>
			   </c:forEach>
			   <c:choose>
			   		<c:when test="${model.totalRegistrations!=0 or totalRegistrations!=0 }" >			  			   
			    		<c:if test="${model.totalRegistrations!=0}">
			    			<td class="numberCol small align bold colorStyle"><fmt:formatNumber value="${(model.totalActivations/model.totalRegistrations)*100}" pattern="#,##0.00" />%</td>
			 			</c:if>
			    		<c:if test="${totalRegistrations!=0}">
			    			<td class="numberCol small align bold colorStyle"><fmt:formatNumber value="${(totalActivations/totalRegistrations)*100}" pattern="#,##0.00" />%</td>
			    		</c:if>	
		    		</c:when>	    	
		    		<c:otherwise><td class="numberCol small align bold colorStyle" align="left">0% </td></c:otherwise>
		    	</c:choose>			  	
			 </tr>			 
			 
			 <tr class="row${status.index % 2}" onmouseover="changeStyleClass(this,'rowOver')" onmouseout="changeStyleClass(this,'row${status.index % 2}')">
			  <td class="nameCol small bold" colspan="1" align="left"><fmt:message key="tsRegistration" /></td>
			  <c:forEach items="${model.reports}" var="reports" varStatus="status">
			    <td class="numberCol small align"><fmt:formatNumber value="${reports.touchScreenRegistrations}"pattern="#,##0" /><c:set var="totaltouchScreenRegistrations" value="${reports.touchScreenRegistrations+totaltouchScreenRegistrations}"/></td>
			   </c:forEach>
			   <td class="numberCol small align bold"><fmt:formatNumber value="${totaltouchScreenRegistrations}"pattern="#,##0" /></td>
			 </tr> 
			 
			 <tr class="row${status.index % 2}" onmouseover="changeStyleClass(this,'rowOver')" onmouseout="changeStyleClass(this,'row${status.index % 2}')">
			  <td class="nameCol small bold" colspan="1" align="left"><fmt:message key="tsActivation" /></td>
			  <c:forEach items="${model.reports}" var="reports" varStatus="status">
			    <td class="numberCol small align"><fmt:formatNumber value="${reports.touchScreenActivations}"pattern="#,##0" /><c:set var="totaltouchScreenActivations" value="${reports.touchScreenActivations+totaltouchScreenActivations}"/></td>
			   </c:forEach>
			   <td class="numberCol small align bold"><fmt:formatNumber value="${totaltouchScreenActivations}"pattern="#,##0" /></td>
			 </tr>
			  <tr>
			  <td class="nameCol small colorStyle bold" align="left"><fmt:message key="closeRate" /></td>
			  <c:forEach items="${model.reports}" var="reports" varStatus="status">			  
			    <td class="numberCol small colorStyle align">
			    	<c:choose>
			    		<c:when test="${reports.touchScreenRegistrations!=0}">
			    			<fmt:formatNumber value="${(reports.touchScreenActivations/reports.touchScreenRegistrations)*100}"pattern="#,##0.00" />%
			    		</c:when>
			    		<c:otherwise>0%</c:otherwise>
			    	</c:choose>			    	
			    </td>
			   </c:forEach>
			   <c:choose>
		    		<c:when test="${totaltouchScreenRegistrations!=0}">
		    			<td class="numberCol small colorStyle align bold"><fmt:formatNumber value="${(totaltouchScreenActivations/totaltouchScreenRegistrations)*100}" pattern="#,##0.00" />%</td>
		    		</c:when>
		    		<c:otherwise><td class="numberCol small colorStyle align bold">0% </td></c:otherwise>
			   </c:choose>	
			 </tr>			 
			 
			 <tr class="row${status.index % 2}" onmouseover="changeStyleClass(this,'rowOver')" onmouseout="changeStyleClass(this,'row${status.index % 2}')">
			  <td class="nameCol small bold" colspan="1" align="left"><fmt:message key="feRegistration" /></td>
			  <c:forEach items="${model.reports}" var="reports" varStatus="status">
			    <td class="numberCol small align"><fmt:formatNumber value="${reports.frontendRegistrations}" pattern="#,##0" /><c:set var="totalfrontendRegistrations" value="${reports.frontendRegistrations+totalfrontendRegistrations}"/></td>
			   </c:forEach>
			   <td class="numberCol small align bold"><fmt:formatNumber value="${totalfrontendRegistrations}"pattern="#,##0" /></td>
			 </tr> 
			 
			 <tr class="row${status.index % 2}" onmouseover="changeStyleClass(this,'rowOver')" onmouseout="changeStyleClass(this,'row${status.index % 2}')">
			  <td class="nameCol small bold" colspan="1" align="left"><fmt:message key="feActivation" /></td>
			  <c:forEach items="${model.reports}" var="reports" varStatus="status">
			    <td class="numberCol small align"><fmt:formatNumber value="${reports.frontendActivations}"pattern="#,##0" /><c:set var="totalfrontendActivations" value="${reports.frontendActivations+totalfrontendActivations}"/></td>
			   </c:forEach>
			   <td class="numberCol small align bold"><fmt:formatNumber value="${totalfrontendActivations}"pattern="#,##0" /></td>
			 </tr>
			 
			  <tr>
			  <td class="nameCol small colorStyle bold" align="left"><fmt:message key="closeRate" /></td>
			  <c:forEach items="${model.reports}" var="reports" varStatus="status">			  
			    <td class="numberCol small align colorStyle">
			    	<c:choose>
			    		<c:when test="${reports.frontendRegistrations!=0}">
			    			<fmt:formatNumber value="${(reports.frontendActivations/reports.frontendRegistrations)*100}"pattern="#,##0.00" />%
			    		</c:when>
			    		<c:otherwise>0%</c:otherwise>
			    	</c:choose>			    	
			    </td>
			   </c:forEach>
			   <c:choose>
		    		<c:when test="${totalfrontendRegistrations!=0}">
		    			<td class="numberCol small align bold colorStyle"><fmt:formatNumber value="${(totalfrontendActivations/totalfrontendRegistrations)*100}"pattern="#,##0.00" />%</td>
		    		</c:when>
		    		<c:otherwise><td class="numberCol small align bold colorStyle">0% </td></c:otherwise>
			   </c:choose>
			 </tr>		
			 
			 
			 <tr class="row${status.index % 2}" onmouseover="changeStyleClass(this,'rowOver')" onmouseout="changeStyleClass(this,'row${status.index % 2}')">
			  <td class="nameCol small bold" colspan="1" align="left"><fmt:message key="lpLeads" /></td>
			  <c:forEach items="${model.reports}" var="reports" varStatus="status">
			    <td class="numberCol small align"><fmt:formatNumber value="${reports.landingPageLeads}"pattern="#,##0" /><c:set var="totallandingPageLeads" value="${reports.landingPageLeads+totallandingPageLeads}"/></td>
			  </c:forEach>
			    <td class="numberCol small align bold"><fmt:formatNumber value="${totallandingPageLeads}"pattern="#,##0" /></td>
			 </tr>		 
			 
			 <tr class="row${status.index % 2}" onmouseover="changeStyleClass(this,'rowOver')" onmouseout="changeStyleClass(this,'row${status.index % 2}')">
			  <td class="nameCol small bold" colspan="1" align="left"><fmt:message key="lpRegistration" /></td>
			  <c:forEach items="${model.reports}" var="reports" varStatus="status">
			    <td class="numberCol small align"><fmt:formatNumber value="${reports.landingPageRegistrations}"pattern="#,##0" /><c:set var="totallandingPageRegistrations" value="${reports.landingPageRegistrations+totallandingPageRegistrations}"/></td>
			   </c:forEach>
			    <td class="numberCol small align bold"><fmt:formatNumber value="${totallandingPageRegistrations}"pattern="#,##0" /></td>
			 </tr> 
			 
			 <tr class="row${status.index % 2}" onmouseover="changeStyleClass(this,'rowOver')" onmouseout="changeStyleClass(this,'row${status.index % 2}')">
			  <td class="nameCol small bold" colspan="1" align="left"><fmt:message key="lpActivation" /></td>
			  <c:forEach items="${model.reports}" var="reports" varStatus="status">
			    <td class="numberCol small align"><fmt:formatNumber value="${reports.landingPageActivations}"pattern="#,##0" /><c:set var="totallandingPageActivations" value="${reports.landingPageActivations+totallandingPageActivations}"/></td>
			   </c:forEach>
			   <td class="numberCol small align bold"><fmt:formatNumber value="${totallandingPageActivations}"pattern="#,##0" /></td>
			 </tr>
			 
			  <tr>
			  <td class="nameCol small colorRed bold" align="left"><fmt:message key="registrationCloseRate" /></td>
			  <c:forEach items="${model.reports}" var="reports" varStatus="status">			  
			    <td class="numberCol small align colorRed">
			    	<c:choose>
			    		<c:when test="${reports.landingPageRegistrations!=0}">
			    			<fmt:formatNumber value="${(reports.landingPageActivations/reports.landingPageRegistrations)*100}"pattern="#,##0.00" />%
			    		</c:when>
			    		<c:otherwise>0%</c:otherwise>
			    	</c:choose>			    	
			    </td>
			   </c:forEach>
			   <c:choose>
		    		<c:when test="${totallandingPageRegistrations!=0}">
		    			<td class="numberCol small align colorRed bold" align="left"><fmt:formatNumber value="${(totallandingPageActivations/totallandingPageRegistrations)*100}"pattern="#,##0.00" />%</td>
		    		</c:when>
		    		<c:otherwise><td class="numberCol small align colorRed bold" align="left">0% </td></c:otherwise>
			   </c:choose>
			 </tr>
			 
			 <tr>
			  <td  class="nameCol small bold colorStyle" colspan="1" align="left"><fmt:message key="activationCloseRate" /></td>
			  <c:forEach items="${model.reports}" var="reports" varStatus="status">			  
			    <td class="numberCol small align colorStyle">
			    	<c:choose>
			    		<c:when test="${reports.landingPageRegistrations!=0}">
			    			<fmt:formatNumber value="${(reports.landingPageActivations/reports.landingPageRegistrations)*100}"pattern="#,##0" />%
			    		</c:when>
			    		<c:otherwise>0%</c:otherwise>
			    	</c:choose>			    	
			    </td>
			   </c:forEach>
			   <c:choose>
		    		<c:when test="${totallandingPageRegistrations!=0}">
		    			<td class="numberCol small align bold colorStyle"><fmt:formatNumber value="${(totallandingPageActivations/totallandingPageRegistrations)*100}"pattern="#,##0" />%</td>
		    		</c:when>
		    		<c:otherwise><td class="numberCol small align bold colorStyle">0% </td></c:otherwise>
			   </c:choose>
			 </tr>	
			 
			 <tr class="row${status.index % 2}" onmouseover="changeStyleClass(this,'rowOver')" onmouseout="changeStyleClass(this,'row${status.index % 2}')">
			  <td class="nameCol small bold" colspan="1" align="left"><fmt:message key="allOtherRegistration" /></td>
			  <c:forEach items="${model.reports}" var="reports" varStatus="status">
			    <td class="numberCol small align"><fmt:formatNumber value="${reports.allOtherRegistrations}"pattern="#,##0" /><c:set var="totalallOtherRegistrations" value="${reports.allOtherRegistrations+totalallOtherRegistrations}"/></td>
			   </c:forEach>
			   <td class="numberCol small align bold"><fmt:formatNumber value="${totalallOtherRegistrations}"pattern="#,##0" /></td>
			 </tr>
			  
			 <tr class="row${status.index % 2}" onmouseover="changeStyleClass(this,'rowOver')" onmouseout="changeStyleClass(this,'row${status.index % 2}')">
			  <td class="nameCol small bold" colspan="1" align="left"><fmt:message key="allOtherActivation" /></td>
			  <c:forEach items="${model.reports}" var="reports" varStatus="status">
			    <td class="numberCol small align"><fmt:formatNumber value="${reports.allOtherActivations}"pattern="#,##0" /><c:set var="totalallOtherActivations" value="${reports.allOtherActivations+totalallOtherActivations}"/></td>
			   </c:forEach>
			   <td class="numberCol small align bold"><fmt:formatNumber value="${totalallOtherActivations}"pattern="#,##0" /></td>
			 </tr>
			 
			  <tr>
			  <td  class="nameCol small colorStyle bold" align="left"><fmt:message key="closeRate" /></td>
			  <c:forEach items="${model.reports}" var="reports" varStatus="status">			  
			    <td class="numberCol small colorStyle align">
			    	<c:choose>
			    		<c:when test="${reports.allOtherRegistrations!=0}">
			    			<fmt:formatNumber value="${(reports.allOtherActivations/reports.allOtherRegistrations)*100}"pattern="#,##0.00" />%
			    		</c:when>
			    		<c:otherwise>0%</c:otherwise>
			    	</c:choose>			    	
			    </td>
			   </c:forEach>
			   <c:choose>
		    		<c:when test="${totalallOtherRegistrations!=0}">
		    			<td class="numberCol small colorStyle align"><fmt:formatNumber value="${(totalallOtherActivations/totalallOtherRegistrations)*100}"pattern="#,##0.00" />%</td>
		    		</c:when>
		    		<c:otherwise><td class="numberCol small colorStyle align bold">0%</td></c:otherwise>
			   </c:choose>
			 </tr>
			 
			 <tr class="row${status.index % 2}" onmouseover="changeStyleClass(this,'rowOver')" onmouseout="changeStyleClass(this,'row${status.index % 2}')">
			  <td style="border-top: 1px solid black" class="nameCol small colorRed bold" colspan="1" align="left"><span class="toolTip" title="<fmt:message key="merchandise" />::GrandTotal - ( ShippingCost + Tax + Credit Card Fee )"><fmt:message key="merchandize" /></span></td>
			  <c:forEach items="${model.reports}" var="reports" varStatus="status">
			    <td style="border-top: 1px solid black" class="numberCol small colorRed align">$<fmt:formatNumber value="${reports.merchandize}"pattern="#,##0" /><c:set var="totalmerchandize" value="${reports.merchandize+totalmerchandize}"/></td>
			   </c:forEach>			    
		    	<td style="border-top: 1px solid black" class="numberCol small colorRed bold align">$<fmt:formatNumber value="${totalmerchandize}"pattern="#,##0" /></td>		    		
			 </tr> 
			 
			 <tr class="row${status.index % 2}" onmouseover="changeStyleClass(this,'rowOver')" onmouseout="changeStyleClass(this,'row${status.index % 2}')">
			  <td class="nameCol small bold" colspan="1" align="left"><fmt:message key="shipping" /></td>
			  <c:forEach items="${model.reports}" var="reports" varStatus="status">
			    <td class="numberCol small align">$<fmt:formatNumber value="${reports.shipping}"pattern="#,##0" /><c:set var="totalshipping" value="${reports.shipping+totalshipping}"/></td>
			   </c:forEach>
			   <td class="numberCol small align bold">$<fmt:formatNumber value="${totalshipping}"pattern="#,##0" /></td>	
			 </tr>
			 
			 <tr class="row${status.index % 2}" onmouseover="changeStyleClass(this,'rowOver')" onmouseout="changeStyleClass(this,'row${status.index % 2}')">
			  <td class="nameCol small bold" colspan="1" align="left"><fmt:message key="ccFee" /></td>
			  <c:forEach items="${model.reports}" var="reports" varStatus="status">
			    <td class="numberCol small align">$<fmt:formatNumber value="${reports.ccFee}"pattern="#,##0" /><c:set var="totalccFee" value="${reports.ccFee+totalccFee}"/></td>
			   </c:forEach>
			   <td class="numberCol small align bold">$<fmt:formatNumber value="${totalccFee}"pattern="#,##0" /></td>
			 </tr>
			 
			 <tr class="row${status.index % 2}" onmouseover="changeStyleClass(this,'rowOver')" onmouseout="changeStyleClass(this,'row${status.index % 2}')">
			  <td class="nameCol small bold colorStyle" colspan="1" align="left"><fmt:message key="grandTotal" /></td>
			  <c:forEach items="${model.reports}" var="reports" varStatus="status">
			    <td class="numberCol small align colorStyle">$<fmt:formatNumber value="${reports.grandTotal}"pattern="#,##0" /><c:set var="totalgrandTotal" value="${reports.grandTotal+totalgrandTotal}"/></td>
			   </c:forEach>
			   <td class="numberCol small align bold colorStyle">$<fmt:formatNumber value="${totalgrandTotal}"pattern="#,##0" /></td>
			 </tr>
			 
			 <tr class="row${status.index % 2}" onmouseover="changeStyleClass(this,'rowOver')" onmouseout="changeStyleClass(this,'row${status.index % 2}')">
			  <td class="nameCol small bold" colspan="1" align="left">#<fmt:message key="customers" /></td>
			  <c:forEach items="${model.reports}" var="reports" varStatus="status">
			    <td class="numberCol small align"><fmt:formatNumber value="${reports.customersPurchased}"pattern="#,###" /><c:set var="totalcustomersPurchased" value="${reports.customersPurchased+totalcustomersPurchased}"/></td>
			   </c:forEach>
			   <td class="numberCol small align bold"><fmt:formatNumber value="${totalcustomersPurchased}"pattern="#,###" /></td>
			 </tr>
			 
			 <tr class="row${status.index % 2}" onmouseover="changeStyleClass(this,'rowOver')" onmouseout="changeStyleClass(this,'row${status.index % 2}')">
			  <td class="nameCol small bold" colspan="1" align="left">#<fmt:message key="orders" /></td>
			  <c:forEach items="${model.reports}" var="reports" varStatus="status">
			    <td class="numberCol small align"><fmt:formatNumber value="${reports.totalOrders}"pattern="#,###" /><c:set var="totalOrder" value="${reports.totalOrders+totalOrder}"/></td>
			   </c:forEach>
			   <td class="numberCol small align bold"><fmt:formatNumber value="${totalOrder}"pattern="#,###" /></td>
			 </tr>
			 
			 <tr class="row${status.index % 2}" onmouseover="changeStyleClass(this,'rowOver')" onmouseout="changeStyleClass(this,'row${status.index % 2}')">
			  <td class="nameCol small bold" colspan="1" align="left"><span class="toolTip" title="Avg Orders::GrandTotal / # Orders"><fmt:message key="avgOrders" /></span></td>
			  <c:forEach items="${model.reports}" var="reports" varStatus="status">
			    <td class="numberCol small align">$<fmt:formatNumber value="${reports.avgOrders}"pattern="#,###" /><c:set var="totalavgOrders" value="${reports.avgOrders+totalavgOrders}"/></td>
			   </c:forEach>
			    <td class="numberCol small align bold">$<fmt:formatNumber value="${totalavgOrders}"pattern="#,###" /></td>
			 </tr>
			 
			  <tr class="row${status.index % 2}" onmouseover="changeStyleClass(this,'rowOver')" onmouseout="changeStyleClass(this,'row${status.index % 2}')">
			  <td class="nameCol small bold" colspan="1" align="left"><span class="toolTip" title="Avg Customers::GrandTotal / # Customers"><fmt:message key="avgCustomers" /></span></td>
			  <c:forEach items="${model.reports}" var="reports" varStatus="status">
			    <td class="numberCol small align">$<fmt:formatNumber value="${reports.avgCustomers}"pattern="#,###" /><c:set var="totalavgCustomers" value="${reports.avgCustomers+totalavgCustomers}"/></td>
			   </c:forEach>
			   <td class="numberCol small align bold">$<fmt:formatNumber value="${totalavgCustomers}"pattern="#,###" /></td>
			 </tr>
			 
			 
			 	  
			</table>


		    <!-- end input field -->  	  
		  	</div>
		  	
		  	<!-- start button -->
			<!-- end button -->	

	<!-- end tab -->        
	</div>
<!-- end tabs -->			
</div>
  
  <!-- end table -->
   </td><td class="boxmidr" ></td></tr>
  <tr><td class="botl"></td><td class="botr"></td></tr>
  </table>
  
<!-- end main box -->  
</div>
</sec:authorize>

</c:if>
 </tiles:putAttribute>    
</tiles:insertDefinition>