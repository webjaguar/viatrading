<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="/WEB-INF/tlds/spring.tld" prefix="spring" %>
<%@ taglib uri="http://www.springframework.org/security/tags" prefix="sec" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>

<tiles:insertDefinition name="admin.report.abandonedShoppingCart" flush="true">
  <tiles:putAttribute name="content" type="string">
  
  
<sec:authorize ifAnyGranted="ROLE_AEM,ROLE_ADMIN_${prop_site_id},ROLE_REPORT_ABANDONED_SHOPPING_CART">
<c:if test="true">
<script type="text/javascript" src="../../admin/javascript/multiple.open.accordion1.2.js"></script>
<script type="text/javascript">
<!--
	var box2 = {};
	window.addEvent('domready', function(){
		$$('img.action').each(function(img){
			//containers
			var actionList = img.getParent(); 
			var actionHover = actionList.getElements('div.action-hover')[0];
			actionHover.set('opacity',0);
			//show/hide
			img.addEvent('mouseenter',function() {
				actionHover.setStyle('display','block').fade('in');
			});
			actionHover.addEvent('mouseleave',function(){
				actionHover.fade('out');
			});
			actionList.addEvent('mouseleave',function() {
				actionHover.fade('out');
			});
		});
		var addCreditBox = new multiBox('mbAddCredit', {showControls : false, useOverlay: false, showNumbers: false });
		var box2 = new multiBox('mbCustomer', {descClassName: 'multiBoxDesc',waitDuration: 5,showNumbers: true,showControls: false,overlay: new overlay()});
		var box3 = new multiBox('mbRegisterEvent', {descClassName: 'multiBoxDesc',descMaxWidth: 400,openFromLink: false,showNumbers: false,showControls: false,useOverlay: false});
		var Tips1 = new Tips('.toolTipImg');  
		// Create the accordian
		var myAccordion = new Fx.Accordion($$('div.arrow_drop'), $$('div.information'), {
			display: 1, alwaysHide: true,
	    	onActive: function() {$('information').removeClass('displayNone');}
		});
	<sec:authorize ifAnyGranted="ROLE_AEM,ROLE_ADMIN_${prop_site_id},ROLE_QUICK_EXPORT">  
	$('quickModeTrigger').addEvent('click',function() {
	    $('mboxfull').setStyle('margin', '0');
	    $('OverlayContainer').hide();
	    $('subMenusContainer').hide();
	    $$('.quickMode').each(function(el){
	    	el.hide();
	    });
	    $$('.quickModeRemove').each(function(el){
	    	el.destroy();
	    });
	    $$('.listingsHdr3').each(function(el){
	    	el.removeProperties('colspan', 'class');
	    });
	    alert('Select the whole page, copy and paste to your excel file.');
	  });
	  </sec:authorize> 
	});	
	
function creditValue(credit,userId) {
	document.getElementById('credit_'+userId).value = credit;
}
		
function addCredit(userId) {
	var addCredit = $('credit_'+userId).value;
	var request = new Request({
		url: "../orders/customer-ajax-credit.jhtm?userId="+userId+"&addCredit="+addCredit,
		method: 'post',
		onComplete: function(response) { 
			location.reload(true);
		}
	}).send();
	
}
function deductCredit(userId) {
	var deductCredit = $('credit_'+userId).value;
	var request = new Request({
		url: "../orders/customer-ajax-credit.jhtm?userId="+userId+"&deductCredit="+deductCredit,
		method: 'post',
		onComplete: function(response) { 
			location.reload(true);
		}
	}).send();
}
function toggleAll(el) {
	var ids = document.getElementsByName("__selected_id");	
	if (ids.length == 1)
      document.list_form.__selected_id.checked = el.checked;
    else
      for (i = 0; i < ids.length; i++)
        document.list_form.__selected_id[i].checked = el.checked;	
} 
function optionsSelected() {
	var ids = document.getElementsByName("__selected_id");
  	if (ids.length == 1) {
      if (document.list_form.__selected_id.checked) {
    	return true;
      }
    } else {
      for (i = 0; i < ids.length; i++) {
        if (document.list_form.__selected_id[i].checked) {
    	  return true;
        }
      }
    }
  	if (document.list_form.__groupAssignAll.checked) {
  	   return true;
    }
    alert("Please select customer(s).");       
    return false;
} 
function optionsSelectedOnlyOneCustomer() {
	var ids = document.getElementsByName("__selected_id");
  	if (ids.length == 1) {
      if (document.list_form.__selected_id.checked) {
    	return true;
      }
    } else if(ids.length > 1) {
    	var count = 0;
    	for (i = 0; i < ids.length; i++) {
            if (document.list_form.__selected_id[i].checked) {
        	  count=count+1;
            }
          }
    	if(count > 1){
      		  alert("Only one customer allowed. ");
      		  return false;
    	}else if(count == 0){
    		alert("Please select one customer.");       
    	    return false;
    	}else{
    		return true;
    	} 
    }
    alert("Please select one customer.");       
    return false;
}
function echoSignSelected() {
	var ids = document.getElementsByName("__selected_id");	
  	if (ids.length == 1) {
      if (document.list_form.__selected_id.checked) {
		document.list_form.action = "../echoSign/sendDocumentMegaSign.jhtm";
      	return true;
      }
    } else {
      for (i = 0; i < ids.length; i++) {
        if (document.list_form.__selected_id[i].checked) {
        	document.list_form.action = "../echoSign/sendDocumentMegaSign.jhtm";
          	return true;
        }
      }
    }

    alert("Please select customers(s) to EchoSign.");       
    return false;
}
//-->
</script>    
</c:if>  
  

<form id="abandonedShoppingCart" action="abandonedShoppingCartController.jhtm" method="post" name="list_form">
<input type="hidden" id="sort" name="sort" value="${abandonedShoppingCartSearch.sort}" />
<!-- main box -->
  <div id="mboxfull">
  
  <!-- start table -->
  <table cellpadding="0" cellspacing="0" border="0" class="module" >
  <tr>
    <td class="topl_g">
	  
	  <!-- breadcrumb -->
	  <p class="breadcrumb">
	    <a href="../report">Report</a> &gt;
	    <fmt:message key="abandonedShoppingCart"/> 
	  </p>
	  
	  <!-- Error -->
	  
	 <!-- header image -->
	  <img class="headerImage" src="../graphics/customer.gif" />
  
  </td><td class="topr_g" ></td></tr>
  <tr><td class="boxmidlrg" >


      <!-- Option -->
      <c:if test="true">
	    <div class="optionBoxAbandonCart"><div class="titlePrivilege"><fmt:message key="options" /></div></div>
		<div class="arrow_drop arrowImageRight"></div>
		<div style="clear: both;"></div>
		<div class="information displayNone" id="information" style="float:left;">  
		     <c:if test="${gSiteConfig['gGROUP_CUSTOMER']}">  
			 <div style="float:left;padding: 5px">
	           <table class="productBatchTool" cellpadding="5" cellspacing="5">
	           <tr>
	            <td valign="top">
	             <table cellpadding="1" cellspacing="1" width="100%">
	              <tr style="height:30px;white-space: nowrap">
	               <td valign="top" width="20px"><img class="toolTipImg" title="Group Id::Associate group Id to the following customer(s) in the list. Can be separated by comma." src="../graphics/question.gif" /></td>
	               <td valign="top"><h3><fmt:message key="groupId" /></h3></td>
	               	<td valign="top" align="right"><input type="checkbox" onclick="toggleAll(this)" name="__groupAssignAll" value="true"/> <fmt:message key="all"/></td>
	              </tr>
	             </table>
	            </td>
	           </tr>
	           <tr>
	            <td valign="top">
	             <table cellpadding="1" cellspacing="1">
	              <tr style="">
			       <td>
			        <input name="__group_id" type="text" size="15"/>
			        <div align="left" class="buttonLeft">
		            <input type="submit" name="__groupAssignPacher" value="Apply" onClick="return optionsSelected()">
		            </div>
			       </td>
			       <td >
			        <div><p><input name="__batch_type" type="radio" checked="checked" value="add"/>Add</p></div>
				    <div><p><input name="__batch_type" type="radio" value="remove"/>Remove</p></div>
				    <div><p><input name="__batch_type" type="radio" value="move"/>Move</p></div>
			       </td>
			     </tr>
	             </table>
	            </td>
	           </tr>
	           </table>
	         </div>
	         </c:if>
	   
	    </div>
	  </c:if>



 <!-- tabs -->
<div class="tab-wrapper">
	<h4 title="List of abandonedShoppingCarts"></h4>
	<div>
	<!-- start tab -->

	  	<%-- <div class="listdivi ln tabdivi"></div>--%>
	  	<div class="listdivi"></div>
	    
		  	<div class="listlight">
		  	<!-- input field -->
			<table border="0" cellpadding="0" cellspacing="1" width="100%">
				<tr>
					
					<c:if test="${model.abandonedShoppingCarts.nrOfElements > 0}">
						<td class="pageShowing">
							<fmt:message key="showing">
								<fmt:param value="${model.abandonedShoppingCarts.firstElementOnPage + 1}" />
								<fmt:param value="${model.abandonedShoppingCarts.lastElementOnPage + 1}" />
								<fmt:param value="${model.abandonedShoppingCarts.nrOfElements}" />
							</fmt:message>
						</td>
					</c:if>
					<td class="pageNavi">
						Page
						<select name="page" id="page" onchange="submit()">
							<c:forEach begin="1" end="${model.abandonedShoppingCarts.pageCount}"
								var="page">
								<option value="${page}"
									<c:if test="${page == (model.abandonedShoppingCarts.page+1)}">selected</c:if>>
									${page}
								</option>
							</c:forEach>
						</select> 
						of
						<c:out value="${model.abandonedShoppingCarts.pageCount}" /> 
						|
						<c:if test="${model.abandonedShoppingCarts.firstPage}">
							<span class="pageNaviDead"><fmt:message key="previous" /></span>
						</c:if>
						<c:if test="${not model.abandonedShoppingCarts.firstPage}">
							<a
								href="<c:url value="abandonedShoppingCartController.jhtm"><c:param name="page" value="${model.abandonedShoppingCarts.page}"/><c:if test="${not model.search['homePage']}"><c:param name="parent" value="${model.search['parent']}"/></c:if></c:url>"
								class="pageNaviLink"><fmt:message key="previous" /></a>
						</c:if>
						|
						<c:if test="${model.abandonedShoppingCarts.lastPage}">
							<span class="pageNaviDead"><fmt:message key="next" /></span>
						</c:if>
						<c:if test="${not model.abandonedShoppingCarts.lastPage}">
							<a
								href="<c:url value="abandonedShoppingCartController.jhtm"><c:param name="page" value="${model.abandonedShoppingCarts.page+2}"/><c:param name="size" value="${model.abandonedShoppingCarts.pageSize}"/></c:url>"
								class="pageNaviLink"><fmt:message key="next" /></a>
						</c:if>
					</td>
				</tr>
			</table>

			<table border="0" cellpadding="0" cellspacing="0" width="100%"
				class="listings">
				<tr class="listingsHdr2">					
					<c:if test="true">
			        	<td align="left" class="quickMode"><input type="checkbox" onclick="toggleAll(this)"></td>
			    	</c:if>					
					<td class="indexCol">
						&nbsp;
					</td>
					<td class="listingsHdr3" align="left">
						<fmt:message key="email" />
					</td>
					<td class="listingsHdr3" align="left">
						<fmt:message key="lastName" />
					</td>
					<td class="listingsHdr3" align="left">
						<fmt:message key="firstName"  />
					</td>
					<td class="listingsHdr3" align="left">
						<fmt:message key="action"  />
					</td>
					<td class="listingsHdr3" align="left">
						<fmt:message key="cellPhone" />
					</td>
					<td class="listingsHdr3" align="left">
						<fmt:message key="date" />
					</td>
					<td>
					<table cellspacing="0" cellpadding="1">
			    	  <tr>
			    	    <c:choose>
			    	      <c:when test="${abandonedShoppingCartSearch.sort == 'temp_cart_total desc'}">
			    	        <td class="listingsHdr3">
			    	        <a class="hdrLink" href="#" onClick="document.getElementById('sort').value='temp_cart_total';document.getElementById('abandonedShoppingCart').submit()"><fmt:message key="abandonedShoppingCartAmount" /></a>
			    	        </td>
			    	        <td><img src="../graphics/up.gif" border="0"></td>
			    	      </c:when>
			    	      <c:when test="${abandonedShoppingCartSearch.sort == 'temp_cart_total'}">
			    	        <td class="listingsHdr3">
			    	        <a class="hdrLink" href="#" onClick="document.getElementById('sort').value='temp_cart_total desc';document.getElementById('abandonedShoppingCart').submit()"><fmt:message key="abandonedShoppingCartAmount" /></a>
			    	        </td>
			    	        <td><img src="../graphics/down.gif" border="0"></td>
			    	      </c:when>
			    	      <c:otherwise>
			    	        <td colspan="2" class="listingsHdr3">
			    	        <a class="hdrLink" href="#" onClick="document.getElementById('sort').value='temp_cart_total desc';document.getElementById('abandonedShoppingCart').submit()"><fmt:message key="abandonedShoppingCartAmount" /></a>
			    	        </td>
			    	      </c:otherwise>
			    	    </c:choose>  
			    	  </tr>
			    	  </table>
			    	  </td>								
				</tr>
				<c:forEach items="${model.abandonedShoppingCarts.pageList}" var="abandonedShoppingCart"
					varStatus="status">
					<tr class="row${status.index % 2}"
						onmouseover="changeStyleClass(this,'rowOver')"
						onmouseout="changeStyleClass(this,'row${status.index % 2}')">
						<td align="left" class="quickMode"><input name="__selected_id" value="${abandonedShoppingCart.userId}" type="checkbox">			      		
						</td>
						<td class="nameColAbandonCart"  align="left">
							<c:out
								value="${status.count + model.abandonedShoppingCarts.firstElementOnPage}" />.
						</td>
						<td class="nameColAbandonCart">
						<a href="<c:url value="${_contextpath}/admin/customers/customer.jhtm?id=${abandonedShoppingCart.userId}"></c:url>" class="nameLink">
							<c:out value="${abandonedShoppingCart.customerEmail }" />
						</td>
						<td class="nameColAbandonCart">
							<c:out value="${abandonedShoppingCart.customerLastName}" />
						</td>
						<td class="nameColAbandonCart">
							<c:out value="${abandonedShoppingCart.customerFirstName}" />
						</td>
						<td class="nameColAbandonCart">
						<a href="<c:url value="${_contextpath}/admin/customers/loginAsCustomer.jhtm?cid=${abandonedShoppingCart.token}&type=abandonedShoppingCart"></c:url>" class="nameLink">
							<fmt:message key="seeCart" />
						</td>
						<td class="nameColAbandonCart">
							<c:out value="${abandonedShoppingCart.customerPhone}" />
						</td>
						</td>					
						<td class="nameColAbandonCart">
							<fmt:formatDate type="date" timeStyle="default" value="${abandonedShoppingCart.createdDate }"/>
						</td>
						<td class="nameColAbandonCart">
						<fmt:formatNumber value="${abandonedShoppingCart.totalAmount}" pattern="$##,###.00" />
						</td>							
					</tr>
				</c:forEach>
				<c:if test="${model.abandonedShoppingCarts.nrOfElements == 0}">
					<tr class="emptyList">
						<td colspan="8">
							&nbsp;
						</td>
					</tr>
				</c:if>
			</table>

			<table border="0" cellpadding="0" cellspacing="1" width="100%">
				<tr>
				  <td class="pageSize">
					<select name="size"
						onchange="document.getElementById('page').value=1;submit()">
						<c:forTokens items="10,25,50,100" delims="," var="current">
							<option value="${current}"
								<c:if test="${current == model.abandonedShoppingCarts.pageSize}">selected</c:if>>
								${current}
								<fmt:message key="perPage" />
							</option>
						</c:forTokens>
					</select>
				  </td>
				</tr>
			</table>
            <!-- end input field -->  	  
		  	
	        </div>
  	
	<!-- end tab -->        
	</div>
<!-- end tabs -->			
</div>

<!-- start button -->
<!-- end button -->	 
  
  <!-- end table -->
   </td><td class="boxmidr" ></td></tr>
  <tr><td class="botl"></td><td class="botr"></td></tr>
  </table>
  
<!-- end main box -->  
</div>
</form>	
</sec:authorize>
  </tiles:putAttribute>    
</tiles:insertDefinition>