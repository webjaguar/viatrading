<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/security/tags" prefix="sec" %>

<tiles:insertDefinition name="admin.report.metricOverview" flush="true">
  <tiles:putAttribute name="content" type="string"> 
  
<c:if test="${gSiteConfig['gREPORT']}">
<sec:authorize ifAnyGranted="ROLE_AEM,ROLE_ADMIN_${prop_site_id},ROLE_REPORT_CUSTOMER_METRIC">
<script type="text/javascript">
<!--
window.addEvent('domready', function(){			
	$$('span.toolTip').each(function(element,index) {  
	         var content = element.get('title').split('::');  
	         element.store('tip:title', content[0]);  
	         element.store('tip:text', content[1]);  
	     	});
	dCreditBox = new multiBox('mbAddCredit', {showControls : false, useOverlay: false, showNumbers: false });
	var box2 = new multiBox('mbCustomer', {descClassName: 'multiBoxDesc',waitDuration: 5,showNumbers: true,showControls: false,overlay: new overlay()});
	var box3 = new multiBox('mbRegisterEvent', {descClassName: 'multiBoxDesc',descMaxWidth: 400,openFromLink: false,showNumbers: false,showControls: false,useOverlay: false});
	// Create the accordian
	var myAccordion = new Fx.Accordion($$('div.arrow_drop'), $$('div.information'), {
		display: 1, alwaysHide: true,
    	onActive: function() {$('information').removeClass('displayNone');}
	});
	<sec:authorize ifAnyGranted="ROLE_AEM,ROLE_ADMIN_${prop_site_id},ROLE_QUICK_EXPORT">
	$('quickModeTrigger').addEvent('click',function() {
		$('mboxfull').setStyle('margin', '0');
	    $('OverlayContainer').hide();
	    $$('.quickMode').each(function(el){
	    	el.hide();
	    });
	    alert('Select the whole page, copy and paste to your excel file.');	 
	  });
	</sec:authorize> 
});
//-->
</script>  
<!-- main box -->
  <div id="mboxfull">
 
  <!-- start table -->
  <table cellpadding="0" cellspacing="0" border="0" class="module" >
  <tr class="quickMode">
    <td class="topl_g">
	  
	  <!-- breadcrumb -->
	  <p class="breadcrumb">
	    <a href="../reports"><fmt:message key="reports" /></a> &gt;
	    <fmt:message key="metricOverviewReport"/>
	  </p>
	  
	  <!-- Error -->
	  <c:if test="${!empty model.message}">
		<div class="message"><fmt:message key="${model.message}" /></div>
	  </c:if>
	  
	  <!-- header image -->
	  <img id="quickModeTrigger" class="headerImage" src="../graphics/reports.gif" /> 
	  
  </td><td class="topr_g" ></td></tr>
  <tr><td class="boxmidlrg" >

 <!-- tabs -->
 <div class="tab-wrapper">
	<h4 title=""></h4>
	<div>
	<!-- start tab -->

	  	<div class="listdivi"></div>
	    
		  	<div class="listlight">
		  	<!-- input field -->
		  	    
		  	<table border="0" cellpadding="0" cellspacing="0" width="100%" class="listings">
			  <tr class="listingsHdr2 hdr2BackgroundColor align ">
			      <td class="listingsHdr3" align="left">CUSTOMER METRICS</td>
			      <td class="listingsHdr3">JAN</td>
			      <td class="listingsHdr3">FEB</td>
			      <td class="listingsHdr3">MAR</td>
			      <td class="listingsHdr3">Q1</td>
			      <td class="listingsHdr3">APR</td>
			      <td class="listingsHdr3">MAY</td>
			      <td class="listingsHdr3">JUNE</td>
			      <td class="listingsHdr3">Q2</td>
			      <td class="listingsHdr3">JULY</td>
			      <td class="listingsHdr3">AUG</td>
			      <td class="listingsHdr3 ">SEP</td>
			      <td class="listingsHdr3">Q3</td>
			      <td class="listingsHdr3">OCT</td>
			      <td class="listingsHdr3">NOV</td>
			      <td class="listingsHdr3">DEC</td>
			      <td class="listingsHdr3">Q4</td>
			      <td class="listingsHdr3"><fmt:message key="total"/></td>
			  </tr>
			  			  
			 <tr class="row${status.index % 2} " onmouseover="changeStyleClass(this,'rowOver')" onmouseout="changeStyleClass(this,'row${status.index % 2}')">
			  <td class="nameCol small bold" colspan="1" align="left"><fmt:message key="newRegistration" /></td>			 
			  <c:forEach items="${model.reports}" var="reports" varStatus="status">
			  <c:if test="${status.index % 3 == 0}">
		  	    <c:set var="qtrNewRegistrations" value="0"/>
		  	  </c:if>  
			    <td class="numberCol small align"><fmt:formatNumber value="${reports.newRegistrations}" pattern="#,##0"/><c:set var="qtrNewRegistrations" value="${reports.newRegistrations+qtrNewRegistrations}"/><c:set var="totalNewRegistrations" value="${reports.newRegistrations+totalNewRegistrations}"/></td>
			     <c:if test="${status.index % 3 == 2}">
				  <c:set var="quarterIndex" value="${quarterIndex + 1}" />				 
				  <td class="numberCol small align bold" ><fmt:formatNumber value="${qtrNewRegistrations}" pattern="#,##0" /></td>
				  <c:if test="${status.index == 2}" ><c:set value="${qtrNewRegistrations}" var="q1NewRegistrations" /></c:if>
				  <c:if test="${status.index == 5}" ><c:set value="${qtrNewRegistrations}" var="q2NewRegistrations" /></c:if>	
				  <c:if test="${status.index == 8}" ><c:set value="${qtrNewRegistrations}" var="q3NewRegistrations" /></c:if>	
				  <c:if test="${status.index == 11}" ><c:set value="${qtrNewRegistrations}" var="q4NewRegistrations" /></c:if>			
			  	 </c:if>
			  </c:forEach>			 
			   <td class="numberCol small align bold"><fmt:formatNumber  value="${totalNewRegistrations}" pattern="#,##0"/></td>
			 </tr> 

			 <tr class="row${status.index % 2}" onmouseover="changeStyleClass(this,'rowOver')" onmouseout="changeStyleClass(this,'row${status.index % 2}')">
			  <td class="nameCol small bold" colspan="1" align="left"><fmt:message key="newActivation" /></td>
			  <c:forEach items="${model.reports}" var="reports" varStatus="status">
			  <c:if test="${status.index % 3 == 0}">
		  	    <c:set var="qtrNewActivations" value="0"/>
		  	  </c:if>  
			    <td class="numberCol small align"><fmt:formatNumber value="${reports.newActivations}" pattern="#,##0"/><c:set var="qtrNewActivations" value="${reports.newActivations+qtrNewActivations}"/><c:set var="totalNewActivations" value="${reports.newActivations+totalNewActivations}"/></td>
			    <c:if test="${status.index % 3 == 2}">
				  <c:set var="quarterIndex" value="${quarterIndex + 1}" />				 
				  <td class="numberCol small align bold"><fmt:formatNumber value="${qtrNewActivations}" pattern="#,##0" /></td>				  
				  <c:if test="${status.index == 2}" >	<c:set value="${qtrNewActivations}" var="q1NewActivations" /></c:if>
				  <c:if test="${status.index == 5}" >	<c:set value="${qtrNewActivations}" var="q2NewActivations" /></c:if>	
				  <c:if test="${status.index == 8}" >	<c:set value="${qtrNewActivations}" var="q3NewActivations" /></c:if>	
				  <c:if test="${status.index == 11}" ><c:set value="${qtrNewActivations}" var="q4NewActivations" /></c:if>				
			  	 </c:if>
			   </c:forEach>
			   <td class="numberCol small align bold"><fmt:formatNumber value="${totalNewActivations}" pattern="#,##0"/></td>
			 </tr>

			  <tr>
			  <td class="nameCol small colorStyle bold" align="left"><fmt:message key="closeRate" /></td>
			  <c:forEach items="${model.reports}" var="reports" varStatus="status">	
			    <td class="numberCol small align colorStyle">
			    	<c:choose>
			    		<c:when test="${reports.newRegistrations!=0}"><fmt:formatNumber value="${(reports.newActivations/reports.newRegistrations)*100}" pattern="#,##0.00" />%</c:when>			    						    		
			    		<c:otherwise>0%</c:otherwise>
			    	</c:choose>			    	
			    </td>
			    <c:if test="${status.index % 3 == 2}">
				  <c:set var="quarterIndex" value="${quarterIndex + 1}" />				 
				  <td class="numberCol small align colorStyle bold" >				  
					  <c:if test="${status.index == 2}" >
					  	<c:if test="${q1NewRegistrations !=0}"><fmt:formatNumber value="${(q1NewActivations/q1NewRegistrations)*100 }" pattern="#,##0.00" />%</c:if>
					  	<c:if test="${q1NewRegistrations ==0}">0%</c:if>
					  </c:if>				 
					  <c:if test="${status.index == 5}" >
					  	 <c:if test="${q2NewRegistrations !=0}"><fmt:formatNumber value="${(q2NewActivations/q2NewRegistrations)*100}" pattern="#,##0.00"/>%</c:if>
					  	 <c:if test="${q2NewRegistrations ==0}">0%</c:if>
					  </c:if>				  
					  <c:if test="${status.index == 8}" >
					  	<c:if test="${q3NewRegistrations !=0}"><fmt:formatNumber value="${(q3NewActivations/q3NewRegistrations)*100}" pattern="#,##0.00"/>%</c:if>
					  	<c:if test="${q3NewRegistrations ==0}">0%</c:if>
					  </c:if>				  
					  	<c:if test="${status.index == 11}" >
					  	<c:if test="${q4NewRegistrations !=0}"><fmt:formatNumber value="${(q4NewActivations/q4NewRegistrations)*100}" pattern="#,##0.00"/>%</c:if>
					  	<c:if test="${q4NewRegistrations ==0}">0%</c:if>
					  </c:if>				 
				  </td>				
			  	 </c:if>
			   </c:forEach>
			   <c:choose>
		    		<c:when test="${totalNewRegistrations!=0}">
		    			<td class="numberCol small align colorStyle bold"><fmt:formatNumber value="${(totalNewActivations/totalNewRegistrations)*100}" pattern="#,##0.00"/>%</td>
		    		</c:when>
		    		<c:otherwise><td class="numberCol small align colorStyle bold" align="left">0% </td></c:otherwise>
			   </c:choose>	
			 </tr>
			 
			 <tr >
			 <td class="nameCol small colorRed bold" colspan="1" align="left"><fmt:message key="totalSpend" /></td>
			 <c:forEach items="${model.reports}" var="reports" varStatus="status">
			 <c:if test="${status.index % 3 == 0}">
		  	    <c:set var="qtrTotalSpend" value="0"/>
		  	  </c:if> 
			 	<td class="numberCol small align colorRed" align="left">$<fmt:formatNumber value="${reports.totalSpend}" pattern="#,##0"/><c:set var="qtrTotalSpend" value="${reports.totalSpend+qtrTotalSpend}"/><c:set var="totalTotalSpend" value="${reports.totalSpend+totalTotalSpend}"/></td>
			 	<c:if test="${status.index % 3 == 2}">
				  <c:set var="quarterIndex" value="${quarterIndex + 1}" />				 
				  <td class="numberCol small align colorRed bold">$<fmt:formatNumber value="${qtrTotalSpend}" pattern="#,##0" /></td>				  
				  <c:if test="${status.index == 2}" >	<c:set value="${qtrTotalSpend}" var="q1TotalSpend" /></c:if>
				  <c:if test="${status.index == 5}" >	<c:set value="${qtrTotalSpend}" var="q2TotalSpend" /></c:if>	
				  <c:if test="${status.index == 8}" >	<c:set value="${qtrTotalSpend}" var="q3TotalSpend" /></c:if>	
				  <c:if test="${status.index == 11}" ><c:set value="${qtrTotalSpend}" var="q4TotalSpend" /></c:if>				
			  	 </c:if>
			 </c:forEach>
			 	<td class="numberCol small align colorRed bold" align="left">$<fmt:formatNumber value="${totalTotalSpend}" pattern="#,##0"/></td>			 
			 </tr>
			 	

			 <tr class="row${status.index % 2}" onmouseover="changeStyleClass(this,'rowOver')" onmouseout="changeStyleClass(this,'row${status.index % 2}')">
			  <td class="nameCol small bold" colspan="1" align="left"><fmt:message key="totalRegistration" /></td>
			  <c:forEach items="${model.reports}" var="reports" varStatus="status">
			  <c:if test="${status.index % 3 == 0}">
		  	    <c:set var="totalRegistrations" value="0"/>
		  	  </c:if> 
			    <td class="numberCol small align"><fmt:formatNumber value="${reports.totalRegistration}" pattern="#,##0"/><c:set var="totalRegistrations" value="${reports.totalRegistration}"/></td>
			    <c:if test="${status.index % 3 == 2}">
				  <c:set var="quarterIndex" value="${quarterIndex + 1}" />				 
				  <td class="numberCol small align bold"><fmt:formatNumber value="${totalRegistrations}" pattern="#,##0" /></td>				  
				  <c:if test="${status.index == 2}" >	<c:set value="${totalRegistrations}" var="q1totalRegistrations" /></c:if>
				  <c:if test="${status.index == 5}" >	<c:set value="${totalRegistrations}" var="q2totalRegistrations" /></c:if>	
				  <c:if test="${status.index == 8}" >	<c:set value="${totalRegistrations}" var="q3totalRegistrations" /></c:if>	
				  <c:if test="${status.index == 11}" ><c:set value="${totalRegistrations}" var="q4totalRegistrations" /></c:if>				
			  	 </c:if>			  
			   </c:forEach>
			   <c:choose>
			   	<c:when test="${model.totalRegistrations == 0}">
			   		<td class="numberCol small align bold"><fmt:formatNumber value="${totalRegistrations}" pattern="#,##0"/></td>
			   	</c:when>
			   	<c:otherwise>
			   		<td class="numberCol small align bold"><fmt:formatNumber value="${model.totalRegistrations}" pattern="#,##0"/></td>
			   	</c:otherwise>
			   </c:choose>			   	
			 </tr> 
			 
			 
			 
			 <tr class="row${status.index % 2}" onmouseover="changeStyleClass(this,'rowOver')" onmouseout="changeStyleClass(this,'row${status.index % 2}')">
			  <td class="nameCol small bold" colspan="1" align="left"><fmt:message key="totalActivation" /></td>
			  <c:forEach items="${model.reports}" var="reports" varStatus="status">			  
			  <c:if test="${status.index % 3 == 0}">
		  	    <c:set var="totalActivations" value="0"/>
		  	  </c:if> 
			    <td class="numberCol small align"><fmt:formatNumber value="${reports.totalActivations}" pattern="#,##0"/><c:set var="totalActivations" value="${reports.totalActivations}"/></td>
			    <c:if test="${status.index % 3 == 2}">
				  <c:set var="quarterIndex" value="${quarterIndex + 1}" />				 
				  <td class="numberCol small align bold"><fmt:formatNumber value="${totalActivations}" pattern="#,##0" /></td>				  			  
				  <c:if test="${status.index == 2}" >	<c:set value="${totalActivations}" var="q1totalActivations" /></c:if>
				  <c:if test="${status.index == 5}" >	<c:set value="${totalActivations}" var="q2totalActivations" /></c:if>	
				  <c:if test="${status.index == 8}" >	<c:set value="${totalActivations}" var="q3totalActivations" /></c:if>	
				  <c:if test="${status.index == 11}" ><c:set value="${totalActivations}" var="q4totalActivations" /></c:if>					
			  	 </c:if>
			   </c:forEach>
			   <c:choose>
			   	<c:when test="${model.totalRegistrations == 0}">
			   		<td class="numberCol small align bold"><fmt:formatNumber value="${totalActivations}" pattern="#,##0"/></td>
			   	</c:when>
			   	<c:otherwise>
			   		<td class="numberCol small align bold"><fmt:formatNumber value="${model.totalActivations}" pattern="#,##0"/></td>
			   	</c:otherwise>
			   	</c:choose>
			 </tr>
			 
			  <tr>
			  <td class="nameCol small colorStyle bold" align="left"><fmt:message key="allTimeCloseRate" /></td>
			  <c:forEach items="${model.reports}" var="reports" varStatus="status">	
			    <td class="numberCol small align colorStyle">
			    	<c:choose>
			    		<c:when test="${reports.totalRegistration!=0}">
			    			<fmt:formatNumber value="${(reports.totalActivations/reports.totalRegistration)*100}" pattern="#,##0.00" />%
			    		</c:when>
			    		<c:otherwise>0%</c:otherwise>
			    	</c:choose>				    		    	
			    </td>
			    <c:if test="${status.index % 3 == 2}">
				  <c:set var="quarterIndex" value="${quarterIndex + 1}" />				 
				  <td class="numberCol small align colorStyle bold">
				  	<c:if test="${status.index == 2}" >
					  	<c:if test="${q1totalRegistrations !=0}"><fmt:formatNumber value="${(q1totalActivations/q1totalRegistrations)*100 }" pattern="#,##0.00" />%</c:if>
					  	<c:if test="${q1totalRegistrations ==0}">0%</c:if>
					  </c:if>				 
					  <c:if test="${status.index == 5}" >
					  	 <c:if test="${q2totalRegistrations !=0}"><fmt:formatNumber value="${(q2totalActivations/q2totalRegistrations)*100}" pattern="#,##0.00"/>%</c:if>
					  	 <c:if test="${q2totalRegistrations ==0}">0%</c:if>
					  </c:if>				  
					  <c:if test="${status.index == 8}" >
					  	<c:if test="${q3totalRegistrations !=0}"><fmt:formatNumber value="${(q3totalActivations/q3totalRegistrations)*100}" pattern="#,##0.00"/>%</c:if>
					  	<c:if test="${q3totalRegistrations ==0}">0%</c:if>
					  </c:if>				  
					  	<c:if test="${status.index == 11}" >
					  	<c:if test="${q4totalRegistrations !=0}"><fmt:formatNumber value="${(q4totalActivations/q4totalRegistrations)*100}" pattern="#,##0.00"/>%</c:if>
					  	<c:if test="${q4totalRegistrations ==0}">0%</c:if>
					  </c:if>
				  </td>				
			  	 </c:if>
			   </c:forEach>
			   <c:choose>
			   		<c:when test="${model.totalRegistrations!=0 or totalRegistrations!=0 }" >			  			   
			    		<c:if test="${model.totalRegistrations!=0}">
			    			<td class="numberCol small align colorStyle"><fmt:formatNumber value="${(model.totalActivations/model.totalRegistrations)*100}" pattern="#,##0.00" />%</td>
			 			</c:if>
			    		<c:if test="${totalRegistrations!=0}">
			    			<td class="numberCol small align bold colorStyle"><fmt:formatNumber value="${(totalActivations/totalRegistrations)*100}" pattern="#,##0.00" />%</td>
			    		</c:if>	
		    		</c:when>	    	
		    		<c:otherwise><td class="numberCol small align bold colorStyle">0% </td></c:otherwise>
		    	</c:choose>			  	
			 </tr>			 
			<%-- <c:if test="${gSiteConfig['gSITE_DOMAIN'] == 'viatrading.com'}">--%> 
			 <tr class="row${status.index % 2}" onmouseover="changeStyleClass(this,'rowOver')" onmouseout="changeStyleClass(this,'row${status.index % 2}')">
			  <td class="nameCol small bold" colspan="1" align="left"><fmt:message key="tsRegistration" /></td>
			  <c:forEach items="${model.reports}" var="reports" varStatus="status">
			  <c:if test="${status.index % 3 == 0}">
		  	    <c:set var="qtrtouchScreenRegistrations" value="0"/>
		  	  </c:if> 
			    <td class="numberCol small align"><fmt:formatNumber value="${reports.touchScreenRegistrations}" pattern="#,##0" /><c:set var="qtrtouchScreenRegistrations" value="${reports.touchScreenRegistrations+qtrtouchScreenRegistrations}"/><c:set var="totaltouchScreenRegistrations" value="${reports.touchScreenRegistrations+totaltouchScreenRegistrations}"/></td>
			    <c:if test="${status.index % 3 == 2}">
				  <c:set var="quarterIndex" value="${quarterIndex + 1}" />				 
				  <td class="numberCol small align bold"><fmt:formatNumber value="${qtrtouchScreenRegistrations}" pattern="#,##0" /></td>				  
				  <c:if test="${status.index == 2}" >	<c:set value="${qtrtouchScreenRegistrations}" var="q1touchScreenRegistrations" /></c:if>
				  <c:if test="${status.index == 5}" >	<c:set value="${qtrtouchScreenRegistrations}" var="q2touchScreenRegistrations" /></c:if>	
				  <c:if test="${status.index == 8}" >	<c:set value="${qtrtouchScreenRegistrations}" var="q3touchScreenRegistrations" /></c:if>	
				  <c:if test="${status.index == 11}" ><c:set value="${qtrtouchScreenRegistrations}" var="q4touchScreenRegistrations" /></c:if>				
			  	</c:if>
			   </c:forEach>
			   <td class="numberCol small align bold" ><fmt:formatNumber value="${totaltouchScreenRegistrations}" pattern="#,##0" /></td>
			 </tr>
			 
			 <tr class="row${status.index % 2}" onmouseover="changeStyleClass(this,'rowOver')" onmouseout="changeStyleClass(this,'row${status.index % 2}')">
			  <td class="nameCol small bold" colspan="1" align="left"><fmt:message key="tsActivation" /></td>
			  <c:forEach items="${model.reports}" var="reports" varStatus="status">
			  <c:if test="${status.index % 3 == 0}">
		  	    <c:set var="qtrtouchScreenActivations" value="0"/>
		  	  </c:if> 
			    <td class="numberCol small align"><fmt:formatNumber value="${reports.touchScreenActivations}" pattern="#,##0" /><c:set var="qtrtouchScreenActivations" value="${reports.touchScreenActivations+qtrtouchScreenActivations}"/><c:set var="totaltouchScreenActivations" value="${reports.touchScreenActivations+totaltouchScreenActivations}"/></td>
			    <c:if test="${status.index % 3 == 2}">
				  <c:set var="quarterIndex" value="${quarterIndex + 1}" />				 
				  <td class="numberCol small align bold"><fmt:formatNumber value="${qtrtouchScreenActivations}" pattern="#,##0" /></td>					  
				  <c:if test="${status.index == 2}" >	<c:set value="${qtrtouchScreenActivations}" var="q1touchScreenActivations" /></c:if>
				  <c:if test="${status.index == 5}" >	<c:set value="${qtrtouchScreenActivations}" var="q2touchScreenActivations" /></c:if>	
				  <c:if test="${status.index == 8}" >	<c:set value="${qtrtouchScreenActivations}" var="q3touchScreenActivations" /></c:if>	
				  <c:if test="${status.index == 11}" ><c:set value="${qtrtouchScreenActivations}" var="q4touchScreenActivations" /></c:if>			
			  	</c:if>
			   </c:forEach>
			   <td class="numberCol small align bold"><fmt:formatNumber value="${totaltouchScreenActivations}" pattern="#,##0" /></td>
			 </tr>
			 
			  <tr>
			  <td class="nameCol small colorStyle bold" align="left"><fmt:message key="closeRate" /></td>
			  <c:forEach items="${model.reports}" var="reports" varStatus="status">			  
			    <td class="numberCol small align colorStyle">
			    	<c:choose>
			    		<c:when test="${reports.touchScreenRegistrations!=0}">
			    			<fmt:formatNumber value="${(reports.touchScreenActivations/reports.touchScreenRegistrations)*100}" pattern="#,##0.00" />%
			    		</c:when>
			    		<c:otherwise>0%</c:otherwise>
			    	</c:choose>			    	
			    </td>
			    <c:if test="${status.index % 3 == 2}">
				  <c:set var="quarterIndex" value="${quarterIndex + 1}" />				 
				  <td class="numberCol small align colorStyle bold" >
				  	<c:if test="${status.index == 2}" >
					  	<c:if test="${q1touchScreenRegistrations !=0}"><fmt:formatNumber value="${(q1touchScreenActivations/q1touchScreenRegistrations)*100 }" pattern="#,##0.00" />%</c:if>
					  	<c:if test="${q1touchScreenRegistrations ==0}">0%</c:if>
					  </c:if>				 
					  <c:if test="${status.index == 5}" >
					  	 <c:if test="${q2touchScreenRegistrations !=0}"><fmt:formatNumber value="${(q2touchScreenActivations/q2touchScreenRegistrations)*100}" pattern="#,##0.00"/>%</c:if>
					  	 <c:if test="${q2touchScreenRegistrations ==0}">0%</c:if>
					  </c:if>				  
					  <c:if test="${status.index == 8}" >
					  	<c:if test="${q3touchScreenRegistrations !=0}"><fmt:formatNumber value="${(q3touchScreenActivations/q3touchScreenRegistrations)*100}" pattern="#,##0.00"/>%</c:if>
					  	<c:if test="${q3touchScreenRegistrations ==0}">0%</c:if>
					  </c:if>				  
					  	<c:if test="${status.index == 11}" >
					  	<c:if test="${q4touchScreenRegistrations !=0}"><fmt:formatNumber value="${(q4touchScreenActivations/q4touchScreenRegistrations)*100}" pattern="#,##0.00"/>%</c:if>
					  	<c:if test="${q4touchScreenRegistrations ==0}">0%</c:if>
					  </c:if>
				  </td>				
			  	 </c:if>
			   </c:forEach>
			   <c:choose>
		    		<c:when test="${totaltouchScreenRegistrations!=0}">
		    			<td class="numberCol small align colorStyle bold"><fmt:formatNumber value="${(totaltouchScreenActivations/totaltouchScreenRegistrations)*100}" pattern="#,##0.00" />%</td>
		    		</c:when>
		    		<c:otherwise><td class="numberCol small align colorStyle bold">0% </td></c:otherwise>
			   </c:choose>	
			 </tr>	
			 <%--</c:if>--%>		 
			 
			 <tr class="row${status.index % 2}" onmouseover="changeStyleClass(this,'rowOver')" onmouseout="changeStyleClass(this,'row${status.index % 2}')">
			  <td class="nameCol small bold" colspan="1" align="left"><fmt:message key="feRegistration" /></td>
			  <c:forEach items="${model.reports}" var="reports" varStatus="status">
			  <c:if test="${status.index % 3 == 0}">
		  	    <c:set var="qtrfrontendRegistrations" value="0"/>
		  	  </c:if>
			    <td class="numberCol small align"><fmt:formatNumber value="${reports.frontendRegistrations}" pattern="#,##0" /><c:set var="qtrfrontendRegistrations" value="${reports.frontendRegistrations+qtrfrontendRegistrations}"/><c:set var="totalfrontendRegistrations" value="${reports.frontendRegistrations+totalfrontendRegistrations}"/></td>
			    <c:if test="${status.index % 3 == 2}">
				  <c:set var="quarterIndex" value="${quarterIndex + 1}" />				 
				  <td class="numberCol small align bold"><fmt:formatNumber value="${qtrfrontendRegistrations}" pattern="#,##0" /></td>				  					  
				  <c:if test="${status.index == 2}" >	<c:set value="${qtrfrontendRegistrations}" var="q1frontendRegistrations" /></c:if>
				  <c:if test="${status.index == 5}" >	<c:set value="${qtrfrontendRegistrations}" var="q2frontendRegistrations" /></c:if>	
				  <c:if test="${status.index == 8}" >	<c:set value="${qtrfrontendRegistrations}" var="q3frontendRegistrations" /></c:if>	
				  <c:if test="${status.index == 11}" ><c:set value="${qtrfrontendRegistrations}" var="q4frontendRegistrations" /></c:if>				
			  	</c:if>
			   </c:forEach>
			   <td class="numberCol small align bold"><fmt:formatNumber value="${totalfrontendRegistrations}" pattern="#,##0" /></td>
			 </tr> 
			 
			 <tr class="row${status.index % 2}" onmouseover="changeStyleClass(this,'rowOver')" onmouseout="changeStyleClass(this,'row${status.index % 2}')">
			  <td class="nameCol small bold" colspan="1" align="left"><fmt:message key="feActivation" /></td>
			  <c:forEach items="${model.reports}" var="reports" varStatus="status">			  
			  <c:if test="${status.index % 3 == 0}">
		  	    <c:set var="qtrfrontendActivations" value="0"/>
		  	  </c:if>
			    <td class="numberCol small align"><fmt:formatNumber value="${reports.frontendActivations}" pattern="#,##0" /><c:set var="qtrfrontendActivations" value="${reports.frontendActivations+qtrfrontendActivations}"/><c:set var="totalfrontendActivations" value="${reports.frontendActivations+totalfrontendActivations}"/></td>
			    <c:if test="${status.index % 3 == 2}">
				  <c:set var="quarterIndex" value="${quarterIndex + 1}" />				 
				  <td class="numberCol small align bold"><fmt:formatNumber value="${qtrfrontendActivations}" pattern="#,##0" /></td>					  				  					  
				  <c:if test="${status.index == 2}" >	<c:set value="${qtrfrontendActivations}" var="q1frontendActivations" /></c:if>
				  <c:if test="${status.index == 5}" >	<c:set value="${qtrfrontendActivations}" var="q2frontendActivations" /></c:if>	
				  <c:if test="${status.index == 8}" >	<c:set value="${qtrfrontendActivations}" var="q3frontendActivations" /></c:if>	
				  <c:if test="${status.index == 11}" ><c:set value="${qtrfrontendActivations}" var="q4frontendActivations" /></c:if>				
			  	</c:if>
			   </c:forEach>
			   <td class="numberCol small align bold"><fmt:formatNumber value="${totalfrontendActivations}" pattern="#,##0" /></td>
			 </tr>
			 
			  <tr>
			  <td  class="nameCol small colorStyle bold" align="left"><fmt:message key="closeRate" /></td>
			  <c:forEach items="${model.reports}" var="reports" varStatus="status">			  
			    <td class="numberCol small align colorStyle">
			    	<c:choose>
			    		<c:when test="${reports.frontendRegistrations!=0}">
			    			<fmt:formatNumber value="${(reports.frontendActivations/reports.frontendRegistrations)*100}" pattern="#,##0.00" />%
			    		</c:when>
			    		<c:otherwise>0%</c:otherwise>
			    	</c:choose>			    	
			    </td>
			    <c:if test="${status.index % 3 == 2}">
				  <c:set var="quarterIndex" value="${quarterIndex + 1}" />				 
				  <td class="numberCol small align colorStyle bold">
				  	<c:if test="${status.index == 2}" >
					  	<c:if test="${q1frontendRegistrations !=0}"><fmt:formatNumber value="${(q1frontendActivations/q1frontendRegistrations)*100 }" pattern="#,##0.00" />%</c:if>
					  	<c:if test="${q1frontendRegistrations ==0}">0%</c:if>
					  </c:if>				 
					  <c:if test="${status.index == 5}" >
					  	 <c:if test="${q2frontendRegistrations !=0}"><fmt:formatNumber value="${(q2frontendActivations/q2frontendRegistrations)*100}" pattern="#,##0.00"/>%</c:if>
					  	 <c:if test="${q2frontendRegistrations ==0}">0%</c:if>
					  </c:if>				  
					  <c:if test="${status.index == 8}" >
					  	<c:if test="${q3frontendRegistrations !=0}"><fmt:formatNumber value="${(q3frontendActivations/q3frontendRegistrations)*100}" pattern="#,##0.00"/>%</c:if>
					  	<c:if test="${q3frontendRegistrations ==0}">0%</c:if>
					  </c:if>				  
					  	<c:if test="${status.index == 11}" >
					  	<c:if test="${q4frontendRegistrations !=0}"><fmt:formatNumber value="${(q4frontendActivations/q4frontendRegistrations)*100}" pattern="#,##0.00"/>%</c:if>
					  	<c:if test="${q4frontendRegistrations ==0}">0%</c:if>
					  </c:if>
				  </td>				
			  	</c:if>
			   </c:forEach>
			   <c:choose>
		    		<c:when test="${totalfrontendRegistrations!=0}">
		    			<td class="numberCol small align colorStyle bold"><fmt:formatNumber value="${(totalfrontendActivations/totalfrontendRegistrations)*100}" pattern="#,##0.00" />%</td>
		    		</c:when>
		    		<c:otherwise><td class="numberCol small align colorStyle bold">0% </td></c:otherwise>
			   </c:choose>
			 </tr>	
			 
			 <%-- <c:if test="${gSiteConfig['gSITE_DOMAIN'] == 'viatrading.com'}">--%> 
			 <tr class="row${status.index % 2}" onmouseover="changeStyleClass(this,'rowOver')" onmouseout="changeStyleClass(this,'row${status.index % 2}')">
			  <td class="nameCol small bold" colspan="1" align="left"><fmt:message key="lpLeads" /></td>
			  <c:forEach items="${model.reports}" var="reports" varStatus="status">
			  <c:if test="${status.index % 3 == 0}">
		  	    <c:set var="qtrlandingPageLeads" value="0"/>
		  	  </c:if>
			    <td class="numberCol small align"><fmt:formatNumber value="${reports.landingPageLeads}" pattern="#,##0" /><c:set var="qtrlandingPageLeads" value="${reports.landingPageLeads+qtrlandingPageLeads}"/><c:set var="totallandingPageLeads" value="${reports.landingPageLeads+totallandingPageLeads}"/></td>
			    <c:if test="${status.index % 3 == 2}">
				  <c:set var="quarterIndex" value="${quarterIndex + 1}" />				 
				  <td class="numberCol small align bold"><fmt:formatNumber value="${qtrlandingPageLeads}" pattern="#,##0" /></td>				  				  				  					  
				  <c:if test="${status.index == 2}" >	<c:set value="${qtrlandingPageLeads}" var="q1landingPageLeads" /></c:if>
				  <c:if test="${status.index == 5}" >	<c:set value="${qtrlandingPageLeads}" var="q2landingPageLeads" /></c:if>	
				  <c:if test="${status.index == 8}" >	<c:set value="${qtrlandingPageLeads}" var="q3landingPageLeads" /></c:if>	
				  <c:if test="${status.index == 11}" ><c:set value="${qtrlandingPageLeads}" var="q4landingPageLeads" /></c:if>				
			  	</c:if>
			   </c:forEach>
			    <td class="numberCol small align bold"><fmt:formatNumber value="${totallandingPageLeads}" pattern="#,##0" /></td>
			 </tr>		 
			 
			 <tr class="row${status.index % 2}" onmouseover="changeStyleClass(this,'rowOver')" onmouseout="changeStyleClass(this,'row${status.index % 2}')">
			  <td class="nameCol small bold" colspan="1" align="left"><fmt:message key="lpRegistration" /></td>
			  <c:forEach items="${model.reports}" var="reports" varStatus="status">
			  <c:if test="${status.index % 3 == 0}">
		  	    <c:set var="qtrlandingPageRegistrations" value="0"/>
		  	  </c:if>
			    <td class="numberCol small align"><fmt:formatNumber value="${reports.landingPageRegistrations}" pattern="#,##0" /><c:set var="qtrlandingPageRegistrations" value="${reports.landingPageRegistrations+qtrlandingPageRegistrations}"/><c:set var="totallandingPageRegistrations" value="${reports.landingPageRegistrations+totallandingPageRegistrations}"/></td>
			    <c:if test="${status.index % 3 == 2}">
				  <c:set var="quarterIndex" value="${quarterIndex + 1}" />				 
				  <td class="numberCol small align bold"><fmt:formatNumber value="${qtrlandingPageRegistrations}" pattern="#,##0" /></td>				  				  				  					  
				  <c:if test="${status.index == 2}" >	<c:set value="${qtrlandingPageRegistrations}" var="q1landingPageRegistrations" /></c:if>
				  <c:if test="${status.index == 5}" >	<c:set value="${qtrlandingPageRegistrations}" var="q2landingPageRegistrations" /></c:if>	
				  <c:if test="${status.index == 8}" >	<c:set value="${qtrlandingPageRegistrations}" var="q3landingPageRegistrations" /></c:if>	
				  <c:if test="${status.index == 11}" ><c:set value="${qtrlandingPageRegistrations}" var="q4landingPageRegistrations" /></c:if>				
			  	</c:if>
			   </c:forEach>
			    <td class="numberCol small align bold"><fmt:formatNumber value="${totallandingPageRegistrations}" pattern="#,##0" /></td>
			 </tr>
			  
			 <tr class="row${status.index % 2}" onmouseover="changeStyleClass(this,'rowOver')" onmouseout="changeStyleClass(this,'row${status.index % 2}')">
			  <td class="nameCol small bold" colspan="1" align="left"><fmt:message key="lpActivation" /></td>
			  <c:forEach items="${model.reports}" var="reports" varStatus="status">
			  <c:if test="${status.index % 3 == 0}">
		  	    <c:set var="qtrlandingPageActivations" value="0"/>
		  	  </c:if>
			    <td class="numberCol small align"><fmt:formatNumber value="${reports.landingPageActivations}" pattern="#,##0" /><c:set var="qtrlandingPageActivations" value="${reports.landingPageActivations+qtrlandingPageActivations}"/><c:set var="totallandingPageActivations" value="${reports.landingPageActivations+totallandingPageActivations}"/></td>
			    <c:if test="${status.index % 3 == 2}">
				  <c:set var="quarterIndex" value="${quarterIndex + 1}" />				 
				  <td class="numberCol small align bold"><fmt:formatNumber value="${qtrlandingPageActivations}" pattern="#,##0" /></td>				  				  				  				  					  
				  <c:if test="${status.index == 2}" >	<c:set value="${qtrlandingPageActivations}" var="q1landingPageActivations" /></c:if>
				  <c:if test="${status.index == 5}" >	<c:set value="${qtrlandingPageActivations}" var="q2landingPageActivations" /></c:if>	
				  <c:if test="${status.index == 8}" >	<c:set value="${qtrlandingPageActivations}" var="q3landingPageActivations" /></c:if>	
				  <c:if test="${status.index == 11}" ><c:set value="${qtrlandingPageActivations}" var="q4landingPageActivations" /></c:if>				
			  	</c:if>
			   </c:forEach>
			   <td class="numberCol small align bold"><fmt:formatNumber value="${totallandingPageActivations}" pattern="#,##0" /></td>
			 </tr>
			 
			  <tr>
			  <td class="nameCol small colorRed bold"><fmt:message key="registrationCloseRate" /></td>
			  <c:forEach items="${model.reports}" var="reports" varStatus="status">			  
			    <td class="numberCol small align colorRed">
			    	<c:choose>
			    		<c:when test="${reports.landingPageLeads!=0}">
			    			<fmt:formatNumber value="${(reports.landingPageRegistrations/reports.landingPageLeads)*100}" pattern="#,##0.00" />%
			    		</c:when>
			    		<c:otherwise>0%</c:otherwise>
			    	</c:choose>			    	
			    </td>
			    <c:if test="${status.index % 3 == 2}">
				  <c:set var="quarterIndex" value="${quarterIndex + 1}" />				 
				  <td class="numberCol small align colorRed bold">
				  	<c:if test="${status.index == 2}" >
					  	<c:if test="${q1landingPageLeads !=0}"><fmt:formatNumber value="${(q1landingPageRegistrations/q1landingPageLeads)*100 }" pattern="#,##0.00" />%</c:if>
					  	<c:if test="${q1landingPageLeads ==0}">0%</c:if>
					  </c:if>				 
					  <c:if test="${status.index == 5}" >
					  	 <c:if test="${q2landingPageLeads !=0}"><fmt:formatNumber value="${(q2landingPageRegistrations/q2landingPageLeads)*100}" pattern="#,##0.00"/>%</c:if>
					  	 <c:if test="${q2landingPageLeads ==0}">0%</c:if>
					  </c:if>				  
					  <c:if test="${status.index == 8}" >
					  	<c:if test="${q3landingPageLeads !=0}"><fmt:formatNumber value="${(q3landingPageRegistrations/q3landingPageLeads)*100}" pattern="#,##0.00"/>%</c:if>
					  	<c:if test="${q3landingPageLeads ==0}">0%</c:if>
					  </c:if>				  
					  	<c:if test="${status.index == 11}" >
					  	<c:if test="${q4landingPageLeads !=0}"><fmt:formatNumber value="${(q4landingPageRegistrations/q4landingPageLeads)*100}" pattern="#,##0.00"/>%</c:if>
					  	<c:if test="${q4landingPageLeads ==0}">0%</c:if>
					  </c:if>
				  </td>				
			  	</c:if>
			   </c:forEach>
			   <c:choose>
		    		<c:when test="${totallandingPageLeads!=0}">
		    			<td class="numberCol small align colorRed bold"><fmt:formatNumber value="${(totallandingPageRegistrations/totallandingPageLeads)*100}" pattern="#,##0.00" />%</td>
		    		</c:when>
		    		<c:otherwise><td class="numberCol small align colorRed bold">0% </td></c:otherwise>
			   </c:choose>
			 </tr>
			 
			 <tr>
			  <td class="nameCol small colorStyle bold " colspan="1" align="left"><fmt:message key="activationCloseRate" /></td>
			  <c:forEach items="${model.reports}" var="reports" varStatus="status">			  
			    <td class="numberCol small colorStyle align">
			    	<c:choose>
			    		<c:when test="${reports.landingPageRegistrations!=0}">
			    			<fmt:formatNumber value="${(reports.landingPageActivations/reports.landingPageRegistrations)*100}" pattern="#,##0" />%
			    		</c:when>
			    		<c:otherwise>0%</c:otherwise>
			    	</c:choose>			    	
			    </td>
			    <c:if test="${status.index % 3 == 2}">
				  <c:set var="quarterIndex" value="${quarterIndex + 1}" />				 
				  <td class="numberCol small colorStyle align bold">
				  	<c:if test="${status.index == 2}" >
					  	<c:if test="${q1landingPageRegistrations !=0}"><fmt:formatNumber value="${(q1landingPageActivations/q1landingPageRegistrations)*100 }" pattern="#,##0" />%</c:if>
					  	<c:if test="${q1landingPageRegistrations ==0}">0%</c:if>
					  </c:if>				 
					  <c:if test="${status.index == 5}" >
					  	 <c:if test="${q2landingPageRegistrations !=0}"><fmt:formatNumber value="${(q2landingPageActivations/q2landingPageRegistrations)*100}" pattern="#,##0"/>%</c:if>
					  	 <c:if test="${q2landingPageRegistrations ==0}">0%</c:if>
					  </c:if>				  
					  <c:if test="${status.index == 8}" >
					  	<c:if test="${q3landingPageRegistrations !=0}"><fmt:formatNumber value="${(q3landingPageActivations/q3landingPageRegistrations)*100}" pattern="#,##0"/>%</c:if>
					  	<c:if test="${q3landingPageRegistrations ==0}">0%</c:if>
					  </c:if>				  
					  	<c:if test="${status.index == 11}" >
					  	<c:if test="${q4landingPageRegistrations !=0}"><fmt:formatNumber value="${(q4landingPageActivations/q4landingPageRegistrations)*100}" pattern="#,##0"/>%</c:if>
					  	<c:if test="${q4landingPageRegistrations ==0}">0%</c:if>
					  </c:if>
				  </td>				
			  	</c:if>
			   </c:forEach>
			   <c:choose>
		    		<c:when test="${totallandingPageRegistrations!=0}">
		    			<td class="numberCol small colorStyle align bold"><fmt:formatNumber value="${(totallandingPageActivations/totallandingPageRegistrations)*100}" pattern="#,##0" />%</td>
		    		</c:when>
		    		<c:otherwise><td class="numberCol small colorStyle align bold">0% </td></c:otherwise>
			   </c:choose>
			 </tr>
			 <%--</c:if> --%>
			 
			 <tr class="row${status.index % 2}" onmouseover="changeStyleClass(this,'rowOver')" onmouseout="changeStyleClass(this,'row${status.index % 2}')">
			  <td class="nameCol small bold" colspan="1" align="left"><fmt:message key="allOtherRegistration" /></td>
			  <c:forEach items="${model.reports}" var="reports" varStatus="status">
			  <c:if test="${status.index % 3 == 0}">
		  	    <c:set var="qtrallOtherRegistrations" value="0"/>
		  	  </c:if>			  
			    <td class="numberCol small align "><fmt:formatNumber value="${reports.allOtherRegistrations}" pattern="#,##0" /><c:set var="qtrallOtherRegistrations" value="${reports.allOtherRegistrations+qtrallOtherRegistrations}"/><c:set var="totalallOtherRegistrations" value="${reports.allOtherRegistrations+totalallOtherRegistrations}"/></td>
			    <c:if test="${status.index % 3 == 2}">
				  <c:set var="quarterIndex" value="${quarterIndex + 1}" />				 
				  <td class="numberCol small align bold"><fmt:formatNumber value="${qtrallOtherRegistrations}" pattern="#,##0" /></td>				  			  				  				  				  					  
				  <c:if test="${status.index == 2}" >	<c:set value="${qtrallOtherRegistrations}" var="q1allOtherRegistrations" /></c:if>
				  <c:if test="${status.index == 5}" >	<c:set value="${qtrallOtherRegistrations}" var="q2allOtherRegistrations" /></c:if>	
				  <c:if test="${status.index == 8}" >	<c:set value="${qtrallOtherRegistrations}" var="q3allOtherRegistrations" /></c:if>	
				  <c:if test="${status.index == 11}" ><c:set value="${qtrallOtherRegistrations}" var="q4allOtherRegistrations" /></c:if>					
			  	</c:if>
			   </c:forEach>
			   <td class="numberCol small align bold"><fmt:formatNumber value="${totalallOtherRegistrations}" pattern="#,##0" /></td>
			 </tr>
			  
			 <tr class="row${status.index % 2}" onmouseover="changeStyleClass(this,'rowOver')" onmouseout="changeStyleClass(this,'row${status.index % 2}')">
			  <td class="nameCol small bold" colspan="1" align="left"><fmt:message key="allOtherActivation" /></td>
			  <c:forEach items="${model.reports}" var="reports" varStatus="status">
			  <c:if test="${status.index % 3 == 0}">
		  	    <c:set var="qtrallOtherActivations" value="0"/>
		  	  </c:if>			  
			    <td class="numberCol small align"><fmt:formatNumber value="${reports.allOtherActivations}" pattern="#,##0" /><c:set var="qtrallOtherActivations" value="${reports.allOtherActivations+qtrallOtherActivations}"/><c:set var="totalallOtherActivations" value="${reports.allOtherActivations+totalallOtherActivations}"/></td>
			    <c:if test="${status.index % 3 == 2}">
				  <c:set var="quarterIndex" value="${quarterIndex + 1}" />				 
				  <td class="numberCol small align bold"><fmt:formatNumber value="${qtrallOtherActivations}" pattern="#,##0" /></td>				  			  				  				  				  					  
				  <c:if test="${status.index == 2}" >	<c:set value="${qtrallOtherActivations}" var="q1allOtherActivations" /></c:if>
				  <c:if test="${status.index == 5}" >	<c:set value="${qtrallOtherActivations}" var="q2allOtherActivations" /></c:if>	
				  <c:if test="${status.index == 8}" >	<c:set value="${qtrallOtherActivations}" var="q3allOtherActivations" /></c:if>	
				  <c:if test="${status.index == 11}" ><c:set value="${qtrallOtherActivations}" var="q4allOtherActivations" /></c:if>				
			  	</c:if>
			   </c:forEach>
			   <td class="numberCol small align bold"><fmt:formatNumber value="${totalallOtherActivations}" pattern="#,##0" /></td>
			 </tr>
			 
			  <tr style="border-bottom:thick dotted #000000">
			  <td  class="nameCol small colorStyle bold" align="left"><fmt:message key="closeRate" /></td>
			  <c:forEach items="${model.reports}" var="reports" varStatus="status">			  
			    <td class="numberCol small align colorStyle">
			    	<c:choose>
			    		<c:when test="${reports.allOtherRegistrations!=0}">
			    			<fmt:formatNumber value="${(reports.allOtherActivations/reports.allOtherRegistrations)*100}" pattern="#,##0.00" />%
			    		</c:when>
			    		<c:otherwise>0%</c:otherwise>
			    	</c:choose>			    	
			    </td>
			    <c:if test="${status.index % 3 == 2}">
				  <c:set var="quarterIndex" value="${quarterIndex + 1}" />				 
				  <td class="numberCol small align colorStyle bold">
				  	 <c:if test="${status.index == 2}" >
				  	  <c:if test="${q1allOtherRegistrations !=0}"><fmt:formatNumber value="${(q1allOtherActivations/q1allOtherRegistrations)*100 }" pattern="#,##0.00" />%</c:if>
					  	<c:if test="${q1allOtherRegistrations ==0}">0%</c:if>
					  </c:if>				 
					  <c:if test="${status.index == 5}" >
					  	 <c:if test="${q2allOtherRegistrations !=0}"><fmt:formatNumber value="${(q2allOtherActivations/q2allOtherRegistrations)*100}" pattern="#,##0.00"/>%</c:if>
					  	 <c:if test="${q2allOtherRegistrations ==0}">0%</c:if>
					  </c:if>				  
					  <c:if test="${status.index == 8}" >
					  	<c:if test="${q3allOtherRegistrations !=0}"><fmt:formatNumber value="${(q3allOtherActivations/q3allOtherRegistrations)*100}" pattern="#,##0.00"/>%</c:if>
					  	<c:if test="${q3allOtherRegistrations ==0}">0%</c:if>
					  </c:if>				  
					  	<c:if test="${status.index == 11}" >
					  	<c:if test="${q4allOtherRegistrations !=0}"><fmt:formatNumber value="${(q4allOtherActivations/q4allOtherRegistrations)*100}" pattern="#,##0.00"/>%</c:if>
					  	<c:if test="${q4allOtherRegistrations ==0}">0%</c:if>
					  </c:if>
				  </td>				
			  	</c:if>
			   </c:forEach>
			   <c:choose>
		    		<c:when test="${totalallOtherRegistrations!=0}">
		    			<td class="numberCol small align colorStyle bold"><fmt:formatNumber value="${(totalallOtherActivations/totalallOtherRegistrations)*100}" pattern="#,##0.00" />%</td>
		    		</c:when>
		    		<c:otherwise>
		    			<td class="numberCol small align colorStyle bold">0% </td>
		    		</c:otherwise>
			   </c:choose>	   
			 </tr>		
			 
			 <tr class="row${status.index % 2}" onmouseover="changeStyleClass(this,'rowOver')" onmouseout="changeStyleClass(this,'row${status.index % 2}')">
			  <td style="border-top: 1px solid black" class="nameCol small colorRed bold" colspan="1" align="left"><span class="toolTip" title="<fmt:message key="merchandise" />::GrandTotal - ( ShippingCost + Tax + Credit Card Fee )"><fmt:message key="merchandize" /></span></td>
			  <c:forEach items="${model.reports}" var="reports" varStatus="status">
			  <c:if test="${status.index % 3 == 0}">
		  	    <c:set var="qtrmerchandize" value="0"/>
		  	  </c:if>			  			  
			    <td style="border-top: 1px solid black" class="numberCol small align colorRed">$<fmt:formatNumber value="${reports.merchandize}" pattern="#,##0" /><c:set var="qtrmerchandize" value="${reports.merchandize+qtrmerchandize}"/><c:set var="totalmerchandize" value="${reports.merchandize+totalmerchandize}"/></td>
			    <c:if test="${status.index % 3 == 2}">
				  <c:set var="quarterIndex" value="${quarterIndex + 1}" />				 
				  <td style="border-top: 1px solid black" class="numberCol small align colorRed bold">$<fmt:formatNumber value="${qtrmerchandize}" pattern="#,##0" /></td>				
			  	</c:if>
			   </c:forEach>			    
		    	<td style="border-top: 1px solid black" class="numberCol small align colorRed bold">$<fmt:formatNumber value="${totalmerchandize}" pattern="#,##0" /></td>		    		
			 </tr> 
			 
			 <tr class="row${status.index % 2}" onmouseover="changeStyleClass(this,'rowOver')" onmouseout="changeStyleClass(this,'row${status.index % 2}')">
			  <td class="nameCol small bold" colspan="1" align="left"><fmt:message key="shipping" /></td>
			  <c:forEach items="${model.reports}" var="reports" varStatus="status">
			  <c:if test="${status.index % 3 == 0}">
		  	    <c:set var="qtrshipping" value="0"/>
		  	  </c:if>			  			  
			    <td class="numberCol small align ">$<fmt:formatNumber value="${reports.shipping}" pattern="#,##0" /><c:set var="qtrshipping" value="${reports.shipping+qtrshipping}"/><c:set var="totalshipping" value="${reports.shipping+totalshipping}"/></td>
			     <c:if test="${status.index % 3 == 2}">
				  <c:set var="quarterIndex" value="${quarterIndex + 1}" />
				  <td class="numberCol small align bold">$<fmt:formatNumber value="${qtrshipping}" pattern="#,##0" /></td>			
			  	</c:if>
			   </c:forEach>
			   <td class="numberCol small align bold">$<fmt:formatNumber value="${totalshipping}" pattern="#,##0" /></td>	
			 </tr>
			 
			 <tr class="row${status.index % 2}" onmouseover="changeStyleClass(this,'rowOver')" onmouseout="changeStyleClass(this,'row${status.index % 2}')">
			  <td class="nameCol small bold" colspan="1" align="left"><fmt:message key="ccFee" /></td>
			  <c:forEach items="${model.reports}" var="reports" varStatus="status">
			  <c:if test="${status.index % 3 == 0}">
		  	    <c:set var="qtrccFee" value="0"/>
		  	  </c:if>
			    <td class="numberCol small align">$<fmt:formatNumber value="${reports.ccFee}" pattern="#,##0" /><c:set var="qtrccFee" value="${reports.ccFee+qtrccFee}"/><c:set var="totalccFee" value="${reports.ccFee+totalccFee}"/></td>
			    <c:if test="${status.index % 3 == 2}">
				  <c:set var="quarterIndex" value="${quarterIndex + 1}" />				 
				  <td class="numberCol small align bold">$<fmt:formatNumber value="${qtrccFee}" pattern="#,##0" /></td>				
			  	</c:if>
			   </c:forEach>
			   <td class="numberCol small align bold">$<fmt:formatNumber value="${totalccFee}" pattern="#,##0" /></td>
			 </tr>
						 
			 <tr class="row${status.index % 2}" onmouseover="changeStyleClass(this,'rowOver')" onmouseout="changeStyleClass(this,'row${status.index % 2}')">
			  <td class="nameCol small bold colorStyle" colspan="1" align="left"><fmt:message key="grandTotal" /></td>
			  <c:forEach items="${model.reports}" var="reports" varStatus="status">
			  <c:if test="${status.index % 3 == 0}">
		  	    <c:set var="qtrgrandTotal" value="0"/>
		  	  </c:if>
			    <td class="numberCol small align colorStyle">$<fmt:formatNumber value="${reports.grandTotal}" pattern="#,##0" /><c:set var="qtrgrandTotal" value="${reports.grandTotal+qtrgrandTotal}"/><c:set var="totalgrandTotal" value="${reports.grandTotal+totalgrandTotal}"/></td>
			     <c:if test="${status.index % 3 == 2}">
				  <c:set var="quarterIndex" value="${quarterIndex + 1}" />				 
				  <td class="numberCol small align colorStyle bold">$<fmt:formatNumber value="${qtrgrandTotal}" pattern="#,##0" />
				  <c:if test="${quarterIndex == 97}">
				  	<c:set var="qtrgrandTotal_97" value="${qtrgrandTotal}"/>
				  </c:if>
				  <c:if test="${quarterIndex == 98}">
				  	<c:set var="qtrgrandTotal_98" value="${qtrgrandTotal}"/>
				  </c:if>
				  <c:if test="${quarterIndex == 99}">
				  	<c:set var="qtrgrandTotal_99" value="${qtrgrandTotal}"/>
				  </c:if>
				  <c:if test="${quarterIndex == 100}">
				  	<c:set var="qtrgrandTotal_100" value="${qtrgrandTotal}"/>
				  </c:if>
				  </td>				
			  	</c:if>
			   </c:forEach>
			   <td class="numberCol small align colorStyle bold">$<fmt:formatNumber value="${totalgrandTotal}" pattern="#,##0" /></td>
			 </tr>
			 
			 <tr class="row${status.index % 2}" onmouseover="changeStyleClass(this,'rowOver')" onmouseout="changeStyleClass(this,'row${status.index % 2}')">
			  <td class="nameCol small bold" colspan="1" align="left">#<fmt:message key="customers" /></td>
			  <c:forEach items="${model.reports}" var="reports" varStatus="status">
			  <c:if test="${status.index % 3 == 0}">
		  	    <c:set var="qtrcustomersPurchased" value="0"/>
		  	  </c:if>
			    <td class="numberCol small align"><fmt:formatNumber value="${reports.customersPurchased}" pattern="#,###" /><c:set var="qtrcustomersPurchased" value="${reports.customersPurchased+qtrcustomersPurchased}"/><c:set var="totalcustomersPurchased" value="${reports.customersPurchased+totalcustomersPurchased}"/></td>
			     <c:if test="${status.index % 3 == 2}">
				  <c:set var="quarterIndex" value="${quarterIndex + 1}" />				 
				  <td class="numberCol small align bold"><fmt:formatNumber value="${qtrcustomersPurchased}" pattern="#,##0" /></td>				
			  	</c:if>
			   </c:forEach>
			   <td class="numberCol small align bold"><fmt:formatNumber value="${totalcustomersPurchased}" pattern="#,###" /></td>
			 </tr>
			 
			 <tr class="row${status.index % 2}" onmouseover="changeStyleClass(this,'rowOver')" onmouseout="changeStyleClass(this,'row${status.index % 2}')">
			  <td class="nameCol small bold" colspan="1" align="left">#<fmt:message key="orders" /></td>
			  <c:forEach items="${model.reports}" var="reports" varStatus="status">
			  <c:if test="${status.index % 3 == 0}">
		  	    <c:set var="qtrOrder" value="0"/>
		  	  </c:if>
			    <td class="numberCol small align"><fmt:formatNumber value="${reports.totalOrders}" pattern="#,###" /><c:set var="qtrOrder" value="${reports.totalOrders+qtrOrder}"/><c:set var="totalOrder" value="${reports.totalOrders+totalOrder}"/></td>
			    <c:if test="${status.index % 3 == 2}">
				  <c:set var="quarterIndex" value="${quarterIndex + 1}" />				 
				  <td class="numberCol small align bold"><fmt:formatNumber value="${qtrOrder}" pattern="#,##0" />
				  <c:if test="${quarterIndex == 105}">
				  	<c:set var="qtrOrder_105" value="${qtrOrder}"/>
				  </c:if>
				  <c:if test="${quarterIndex == 106}">
				  	<c:set var="qtrOrder_106" value="${qtrOrder}"/>
				  </c:if>
				  <c:if test="${quarterIndex == 107}">
				  	<c:set var="qtrOrder_107" value="${qtrOrder}"/>
				  </c:if>
				  <c:if test="${quarterIndex == 108}">
				  	<c:set var="qtrOrder_108" value="${qtrOrder}"/>
				  </c:if></td>				
			  	</c:if>
			   </c:forEach>
			   <c:set var="qtrOrderValueQ1" value="${qtrOrder}"/>
			   <td class="numberCol small align bold"><fmt:formatNumber value="${totalOrder}" pattern="#,###" /></td>
			 </tr>
			 <tr class="row${status.index % 2}" onmouseover="changeStyleClass(this,'rowOver')" onmouseout="changeStyleClass(this,'row${status.index % 2}')">
			  <td class="nameCol small bold" colspan="1" align="left"><span class="toolTip" title="Avg Orders::GrandTotal / # Orders"><fmt:message key="avgOrders" /></span></td>
			  <c:forEach items="${model.reports}" var="reports" varStatus="status">
			  <c:if test="${status.index % 3 == 0}">
		  	    <c:set var="qtravgOrders" value="0"/>
		  	  </c:if>
			    <td class="numberCol small align">$<fmt:formatNumber value="${reports.avgOrders}" pattern="#,###" /><c:set var="qtravgOrders" value="${reports.avgOrders+qtravgOrders}"/><c:set var="totalavgOrders" value="${reports.avgOrders+totalavgOrders}"/></td>
			    <c:if test="${status.index % 3 == 2}">
				  <c:set var="quarterIndex" value="${quarterIndex + 1}" />				 
				  <td class="numberCol small align bold">
				  <c:if test="${quarterIndex == 109}">
				  	$<fmt:formatNumber value="${qtrgrandTotal_97 / qtrOrder_105}" pattern="#,##0" />
				  </c:if>
				  <c:if test="${quarterIndex == 110}">
				  	$<fmt:formatNumber value="${qtrgrandTotal_98 / qtrOrder_106}" pattern="#,##0" />
				  </c:if>
				  <c:if test="${quarterIndex == 111}">
				  	$<fmt:formatNumber value="${qtrgrandTotal_99 / qtrOrder_107}" pattern="#,##0" />
				  </c:if>
				  <c:if test="${quarterIndex == 112}">
				  	$<fmt:formatNumber value="${qtrgrandTotal_100 / qtrOrder_108}" pattern="#,##0" />
				  </c:if>
				  </td>				
			  	</c:if>
			   </c:forEach>
			    <td class="numberCol small align bold">$<fmt:formatNumber value="${totalavgOrders}" pattern="#,###" /></td>
			 </tr>
			 
			  <tr class="row${status.index % 2}" onmouseover="changeStyleClass(this,'rowOver')" onmouseout="changeStyleClass(this,'row${status.index % 2}')">
			  <td class="nameCol small bold" colspan="1" align="left"><span class="toolTip" title="Avg Customers::GrandTotal / # Customers"><fmt:message key="avgCustomers" /></span></td>
			  <c:forEach items="${model.reports}" var="reports" varStatus="status">
			  <c:if test="${status.index % 3 == 0}">
		  	    <c:set var="qtravgCustomers" value="0"/>
		  	  </c:if>
			    <td class="numberCol small align">$<fmt:formatNumber value="${reports.avgCustomers}" pattern="#,###" /><c:set var="qtravgCustomers" value="${reports.avgCustomers+qtravgCustomers}"/><c:set var="totalavgCustomers" value="${reports.avgCustomers+totalavgCustomers}"/></td>
			    <c:if test="${status.index % 3 == 2}">
				  <c:set var="quarterIndex" value="${quarterIndex + 1}" />				 
				  <td class="numberCol small align bold">$<fmt:formatNumber value="${qtravgCustomers}" pattern="#,##0" /></td>				
			  	</c:if>
			   </c:forEach>
			   <td class="numberCol small align bold">$<fmt:formatNumber value="${totalavgCustomers}" pattern="#,###" /></td>
			 </tr>
			 
			 
			 	  
			</table>


		    <!-- end input field -->  	  
		  	</div>
		  	
		  	<!-- start button -->
			<!-- end button -->	

	<!-- end tab -->        
	</div>
<!-- end tabs -->			
</div>
  
  <!-- end table -->
   </td><td class="boxmidr" ></td></tr>
  <tr><td class="botl"></td><td class="botr"></td></tr>
  </table>
  
<!-- end main box -->  
</div>

</sec:authorize>


</c:if>
 </tiles:putAttribute>    
</tiles:insertDefinition>