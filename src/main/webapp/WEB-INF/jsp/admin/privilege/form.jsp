<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="/WEB-INF/tlds/spring.tld" prefix="spring" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ taglib uri="http://www.springframework.org/security/tags" prefix="sec" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>

<tiles:insertDefinition name="admin.privilege" flush="true">
  <tiles:putAttribute name="content" type="string">
<sec:authorize ifAnyGranted="ROLE_AEM,ROLE_ADMIN_${prop_site_id}">
  <script type="text/javascript" src="../../admin/javascript/clientcide.2.2.0.js"></script>
<script type="text/javascript">
<!--
window.addEvent('domready', function(){	
	var tabs1 = new SimpleTabs($('tab-block-1'), { selector: 'h4'});
	var mymultiOpen;		
	myMultiOpen = new MultipleOpenAccordion($('AccordionMulti'), {
		elements: $$('#AccordionMulti div.stretcher'),
		togglers: $$('#AccordionMulti div.stretchtoggle'),
		openAll: false
	});
	$('checkAll').addEvent('click',function (e) {
		var toggle = $('checkAll').checked;
		$$('#accessPrivilegeForm input[type=checkbox]').each(function(check) {
			check.checked = toggle;
		});
	});
});
function toggleAll() {
	myMultiOpen.toggleAll(new Fx());
}
//-->
</script>
<form:form name="myform" commandName="accessPrivilegeForm" action="privilege.jhtm" method="post">
<!-- main box -->
  <div id="mboxfull">
  
  <!-- start table -->
  <table cellpadding="0" cellspacing="0" border="0" class="module" >
  <tr>
    <td class="topl_g">
	  
	  <!-- breadcrumb -->
	  <p class="breadcrumb">
	    <a href="../privilege">Privilege</a> &gt;
	    <a href="../privilege">users</a> &gt;
	    ${accessPrivilegeForm.privilege.user.username} 
	  </p>
	  
	  <!-- Error Message -->
	  <c:if test="${message != null}">
		 <div class="message"><c:out value="${message}" /></div>
	  </c:if>
	  <spring:hasBindErrors name="accessPrivilegeForm">
		<div class="message">Please fix all errors!</div>
	  </spring:hasBindErrors>
	  <form:errors path="*" cssClass="error"/>
	  
	  
  </td><td class="topr_g" ></td></tr>
  <tr><td class="boxmidlrg" >

 <!-- tabs -->
 <div id="tab-block-1">
	<h4 title="User Form">User</h4>
	<div>
	<!-- start tab -->
         <c:set var="classIndex" value="0" />
         
	  	<div class="listdivi ln tabdivi"></div>
	  	<div class="listdivi"></div>
	   
		  	<div class="list${classIndex % 2}"><c:set var="classIndex" value="${classIndex+1}" />
		  	<div class="listfl"><fmt:message key='active' />:</div>
		  	<div class="listp">
		  	<!-- input field -->
			    <form:checkbox path="privilege.user.enable"/>
			    <form:errors path="privilege.user.enable" cssClass="error"/>
			<!-- end input field -->	
	 		</div>
		  	</div>	
		  	
		  	<div class="list${classIndex % 2}"><c:set var="classIndex" value="${classIndex+1}" />
		  	<div class="listfl"><div class="requiredField"><fmt:message key='username' />:</div></div>
		  	<div class="listp">
		  	<!-- input field -->
			    <form:input path="privilege.user.username" cssClass="textfield" maxlength="50" size="30" htmlEscape="true"/>
			    <form:errors path="privilege.user.username" cssClass="error"/>
	 		<!-- end input field -->	
	 		</div>
		  	</div>
	
		  	<div class="list${classIndex % 2}"><c:set var="classIndex" value="${classIndex+1}" />
		  	<div class="listfl"><div class="requiredField"><fmt:message key='password' />:</div></div>
		  	<div class="listp">
		  	<!-- input field -->
				<form:password path="privilege.user.password" cssClass="textfield" maxlength="50" size="30"/>
				<form:errors path="privilege.user.password" cssClass="error"/>
				<div class="helpNote"><p>Min 5 Character</p></div>
	 		<!-- end input field -->	
	 		</div>
		  	</div>

		  	<div class="list${classIndex % 2}"><c:set var="classIndex" value="${classIndex+1}" />
		  	<div class="listfl"><div class="requiredField"><fmt:message key='confirmPassword' />:</div></div>
		  	<div class="listp">
		  	<!-- input field -->
				<form:password path="confirmPassword" cssClass="textfield" maxlength="50" size="30" />
				<form:errors path="confirmPassword" cssClass="error"/>
	 		<!-- end input field -->	
	 		</div>
		  	</div>
		  	
		  	<div class="list${classIndex % 2}"><c:set var="classIndex" value="${classIndex+1}" />
		  	<div class="listfl"><div class="requiredField"><fmt:message key="firstName" />:</div></div>
		  	<div class="listp">
		  	<!-- input field -->
	 			<form:input path="privilege.user.firstName" cssClass="textfield" maxlength="50" htmlEscape="true"/>
	 			<form:errors path="privilege.user.firstName" cssClass="error" />
		    <!-- end input field -->  	  
		  	</div>
		  	</div>
		  	
		  	<div class="list${classIndex % 2}"><c:set var="classIndex" value="${classIndex+1}" />
		  	<div class="listfl"><div class="requiredField"><fmt:message key="lastName" />:</div></div>
		  	<div class="listp">
		  	<!-- input field -->
	 			<form:input path="privilege.user.lastName" cssClass="textfield" maxlength="50" htmlEscape="true"/>
	 			<form:errors path="privilege.user.lastName" cssClass="error" />
		    <!-- end input field -->  	  
		  	</div>
		  	</div>
		  	
		  	<div class="list${classIndex % 2}"><c:set var="classIndex" value="${classIndex+1}" />
		  	<div class="listfl"><div class="requiredField"><fmt:message key="email" />:</div></div>
		  	<div class="listp">
		  	<!-- input field -->
	 			<form:input path="privilege.user.email" cssClass="textfield" maxlength="50" htmlEscape="true"/>
	 			<form:errors path="privilege.user.email" cssClass="error" />
		    <!-- end input field -->  	  
		  	</div>
		  	</div>
			
		  	<div class="list${classIndex % 2}"><c:set var="classIndex" value="${classIndex+1}" />
		  	<div class="listfl"><fmt:message key='ipAddress' />:</div>
		  	<div class="listp">
		  	<!-- input field -->
				<form:textarea path="privilege.user.ipAddress" cssClass="textArea300x400" />
				<form:errors path="privilege.user.ipAddress"  cssClass="error"/>
				<div class="helpNote"><p>This user can connect only through these IP Address(es).<br /> Separate multiple IP Addresses by comma or enter.</p></div>
	 		<!-- end input field -->	
	 		</div>
		  	</div>

			<c:if test="${gSiteConfig['gSALES_REP'] == true}">
	  		<div class="list${classIndex % 2}"><c:set var="classIndex" value="${classIndex+1}" />
		  	<div class="listfl"><fmt:message key="salesRep" />:</div>
		  	<div class="listp">
		  	<!-- input field -->
		  		<form:select path="privilege.user.salesRepId">
		  		<form:option value="" label="Please Select">Please Select</form:option>
		  		<c:forEach items="${salesReps}" var="salesRep" >
		  		  <form:option value="${salesRep.id}" label="${salesRep.name}">${salesRep.name}</form:option>
		  		</c:forEach>
		  		<form:errors path="privilege.user.salesRepId"  cssClass="error"/>
				</form:select>
	 		<!-- end input field -->  	  
		  	</div>
		  	</div>

	  		<div class="list${classIndex % 2}"><c:set var="classIndex" value="${classIndex+1}" />
		  	<div class="listfl"><fmt:message key="salesRep" /> <fmt:message key="filter" />:</div>
		  	<div class="listp">
		  	<!-- input field -->
			    <form:checkbox path="privilege.user.salesRepFilter"/>
			    <form:errors path="privilege.user.salesRepFilter" cssClass="error"/>
	 		<!-- end input field -->  	  
		  	</div>
		  	</div>
			</c:if>
	<!-- end tab -->        
	</div>         

	<h4 title="<fmt:message key="privilege"/>"><fmt:message key="privilege"/></h4>
	<div>
	<!-- start tab -->
         <c:set var="classIndex" value="0" />	  	
		
		<div class="listdivi ln tabdivi"></div>
	  	<div class="listdivi"></div>  	
	
	    <c:if test="${siteConfig['ACCESS_PRIVILEGE_GROUP'].value == 'true'}">
			<div class="list${classIndex % 2}"><c:set var="classIndex" value="${classIndex+1}" />
		  	<div class="listfl">Group Association:</div>
		  	<div class="listp">
		  	<!-- input field -->
                <form:textarea path="privilege.user.groupIds" cssClass="textfield" rows="10" cols="45"/>
	            <form:errors path="privilege.user.groupIds" cssClass="error"/>
			    <div class="helpNote">
			      <p>Enter Group Ids here. Seperate them with comma or press enter.</p>
			    </div>
		    <!-- end input field -->   	
		  	</div>
		  	</div>
		 </c:if>
		  	
		  	<div class="list${classIndex % 2}"><c:set var="classIndex" value="${classIndex+1}" />
		  	<div class="listp" style="width:100%">
		  	<!-- input field -->
			<div class="AccordionMulti" id="AccordionMulti">    
			    	<table border="0"  width="100%">
			    	<tr>
			    	  <td><a href="#" onclick="javascript:toggleAll();">Open/Close</a></td>
			    	  <td>Check/Uncheck<input id="checkAll" type="checkbox" name=""  <c:if test="${accessPrivilegeForm.privilege.user.groupIds != ''}">disabled="disabled"</c:if> ></td>
			    	</tr>
					<tr>
					<td colspan="2"width="100%">
					<div class="stretchtoggle">
					    <div style="">
						  <span class="titlePrivilege"><fmt:message key="note" /></span>
						</div>	
				    </div>
					<div class="stretcher">
						 <div class="list${classIndex % 2}"><c:set var="classIndex" value="${classIndex+1}" />
						 <div class="listfl"></div>
						 <!-- input field -->
						     <div class="helpNote">
						        <p><strong>Please select the User Access Privilege on the following forms.<br />If No privileges selected the user can not login.</strong></p>
						     </div>   
					     <!-- end input field -->	
					 	 </div>		  	
					  	 </div>
					</td>
					</tr>
				    <tr valign="top">
				    <td width="50%" >
				    <div class="stretchtoggle">
					    <div style="">
						  <span class="titlePrivilege"><fmt:message key="customer" /></span>
						</div>	
				    </div>
				    <div class="stretcher">
					     <div  class="checkAll">
					       <a style="text-decoration:none;color:red;font-weight:700" href="javascript:;" onclick="SetChecked('CUSTOMER',5,13);return false;">All</a>&nbsp;/&nbsp;
					       <a style="text-decoration:none;color:red;font-weight:700" href="javascript:;" onclick="UnSetChecked('CUSTOMER',5,13);return false;">None</a>
					     </div>
					     <div class="list${classIndex % 2}"><c:set var="classIndex" value="${classIndex+1}" />
					  	 <div class="listfl"><input type="hidden" name="__key" value="ROLE_CUSTOMER_CREATE">CREATE:</div>
					  	 <div class="listp">
					  	 <!-- input field -->
					     	 <input type="checkbox" name="ROLE_CUSTOMER_CREATE" value ="ROLE_CUSTOMER_CREATE" <c:if test="${accessPrivilegeForm.privilege.roleCustomerCreate}">checked</c:if> <c:if test="${accessPrivilegeForm.privilege.user.groupIds != ''}">disabled="disabled"</c:if>>
					   	 <!-- end input field -->	
						 </div>
					  	 </div>
					  	 <div class="list${classIndex % 2}"><c:set var="classIndex" value="${classIndex+1}" />
					  	 <div class="listfl"><input type="hidden" name="__key" value="ROLE_CUSTOMER_DELETE">DELETE:</div>
					  	 <div class="listp">
					  	 <!-- input field -->
					     	 <input type="checkbox" name="ROLE_CUSTOMER_DELETE" value ="ROLE_CUSTOMER_DELETE" <c:if test="${accessPrivilegeForm.privilege.roleCustomerDelete}">checked</c:if> <c:if test="${accessPrivilegeForm.privilege.user.groupIds != ''}">disabled="disabled"</c:if>>
					   	 <!-- end input field -->	
						 </div>
					  	 </div>	
					  	 <div class="list${classIndex % 2}"><c:set var="classIndex" value="${classIndex+1}" />
					  	 <div class="listfl"><input type="hidden" name="__key" value="ROLE_CUSTOMER_EDIT">EDIT:</div>
					  	 <div class="listp">
					  	 <!-- input field -->
							<input type="checkbox" name="ROLE_CUSTOMER_EDIT" value ="ROLE_CUSTOMER_EDIT" <c:if test="${accessPrivilegeForm.privilege.roleCustomerEdit}">checked</c:if> <c:if test="${accessPrivilegeForm.privilege.user.groupIds != ''}">disabled="disabled"</c:if>>
					   	 <!-- end input field -->	
						 </div>
					  	 </div>		
					  	 <div class="list${classIndex % 2}"><c:set var="classIndex" value="${classIndex+1}" />
					  	 <div class="listfl"><input type="hidden" name="__key" value="ROLE_CUSTOMER_LOGINAS">LOGIN AS:</div>
					  	 <div class="listp">
					  	 <!-- input field -->
					     	 <input type="checkbox" name="ROLE_CUSTOMER_LOGINAS" value ="ROLE_CUSTOMER_LOGINAS" <c:if test="${accessPrivilegeForm.privilege.roleCustomerLoginAs}">checked</c:if> <c:if test="${accessPrivilegeForm.privilege.user.groupIds != ''}">disabled="disabled"</c:if>>
					   	 <!-- end input field -->	
						 </div>
					  	 </div>		
					  	 <div class="list${classIndex % 2}"><c:set var="classIndex" value="${classIndex+1}" />
					  	 <div class="listfl"><input type="hidden" name="__key" value="ROLE_CUSTOMER_MYLIST_ADD">MY LIST ADD:</div>
					  	 <div class="listp">
					  	 <!-- input field -->
					     	 <input type="checkbox" name="ROLE_CUSTOMER_MYLIST_ADD" value ="ROLE_CUSTOMER_MYLIST_ADD" <c:if test="${accessPrivilegeForm.privilege.roleCustomerMyListAdd}">checked</c:if>  <c:if test="${accessPrivilegeForm.privilege.user.groupIds != ''}">disabled="disabled"</c:if>>
					   	 <!-- end input field -->	
						 </div>
					  	 </div>		
					  	 <div class="list${classIndex % 2}"><c:set var="classIndex" value="${classIndex+1}" />
					  	 <div class="listfl"><input type="hidden" name="__key" value="ROLE_CUSTOMER_MYLIST_DELETE">MY LIST DELETE:</div>
					  	 <div class="listp">
					  	 <!-- input field -->
				       		<input type="checkbox" name="ROLE_CUSTOMER_MYLIST_DELETE" value ="ROLE_CUSTOMER_MYLIST_DELETE" <c:if test="${accessPrivilegeForm.privilege.roleCustomerMyListDelete}">checked</c:if>  <c:if test="${accessPrivilegeForm.privilege.user.groupIds != ''}">disabled="disabled"</c:if>>
					   	 <!-- end input field -->	
						 </div>
					  	 </div>		
					  	 <div class="list${classIndex % 2}"><c:set var="classIndex" value="${classIndex+1}" />
					  	 <div class="listfl"><input type="hidden" name="__key" value="ROLE_CUSTOMER_MYLIST_VIEW">MY LIST VIEW:</div>
					  	 <div class="listp">
					  	 <!-- input field -->
					     	 <input type="checkbox" name="ROLE_CUSTOMER_MYLIST_VIEW" value ="ROLE_CUSTOMER_MYLIST_VIEW" <c:if test="${accessPrivilegeForm.privilege.roleCustomerMyListView}">checked</c:if>  <c:if test="${accessPrivilegeForm.privilege.user.groupIds != ''}">disabled="disabled"</c:if>>
					   	 <!-- end input field -->	
						 </div>
					  	 </div>	
					  	 <div class="list${classIndex % 2}"><c:set var="classIndex" value="${classIndex+1}" />
					  	 <div class="listfl"><input type="hidden" name="__key" value="ROLE_CUSTOMER_IMPORT_EXPORT">IMPORT/EXPORT:</div>
					  	 <div class="listp">
					  	 <!-- input field -->
					     	 <input type="checkbox" name="ROLE_CUSTOMER_IMPORT_EXPORT" value ="ROLE_CUSTOMER_IMPORT_EXPORT" <c:if test="${accessPrivilegeForm.privilege.roleCustomerImportExport}">checked</c:if>  <c:if test="${accessPrivilegeForm.privilege.user.groupIds != ''}">disabled="disabled"</c:if>>
					   	 <!-- end input field -->	
						 </div>
					  	 </div>	
					  	 <div class="list${classIndex % 2}"><c:set var="classIndex" value="${classIndex+1}" />
					  	 <div class="listfl"><input type="hidden" name="__key" value="ROLE_CUSTOMER_ADD_INVOICE">ADD ORDER:</div>
					  	 <div class="listp">
					  	 <!-- input field -->
					     	 <input type="checkbox" name="ROLE_CUSTOMER_ADD_INVOICE" value ="ROLE_CUSTOMER_ADD_INVOICE" <c:if test="${accessPrivilegeForm.privilege.roleCustomerAddInvoice}">checked</c:if>  <c:if test="${accessPrivilegeForm.privilege.user.groupIds != ''}">disabled="disabled"</c:if>>
					   	 <!-- end input field -->	
						 </div>
					  	 </div>
					  	 <div class="list${classIndex % 2}"><c:set var="classIndex" value="${classIndex+1}" />
					  	 <div class="listfl"><input type="hidden" name="__key" value="ROLE_CUSTOMER_VIEW_LIST">VIEW LIST:</div>
					  	 <div class="listp">
					  	 <!-- input field -->
					     	 <input type="checkbox" name="ROLE_CUSTOMER_VIEW_LIST" value ="ROLE_CUSTOMER_VIEW_LIST" <c:if test="${accessPrivilegeForm.privilege.roleCustomerViewList}">checked</c:if>  <c:if test="${accessPrivilegeForm.privilege.user.groupIds != ''}">disabled="disabled"</c:if>>
					   	 <!-- end input field -->	
						 </div>
					  	 </div> 
					  	 <div class="list${classIndex % 2}"><c:set var="classIndex" value="${classIndex+1}" />
					  	 <div class="listfl"><input type="hidden" name="__key" value="ROLE_CUSTOMER_FIELD">FIELLD:</div>
					  	 <div class="listp">
					  	 <!-- input field -->
					     	 <input type="checkbox" name="ROLE_CUSTOMER_FIELD" value ="ROLE_CUSTOMER_FIELD" <c:if test="${accessPrivilegeForm.privilege.roleCustomerField}">checked</c:if>  <c:if test="${accessPrivilegeForm.privilege.user.groupIds != ''}">disabled="disabled"</c:if>>
					   	 <!-- end input field -->	
						 </div>
					  	 </div> 
					  	 <div class="list${classIndex % 2}"><c:set var="classIndex" value="${classIndex+1}" />
					  	 <div class="listfl"><input type="hidden" name="__key" value="ROLE_CUSTOMER_AFFILIATE">AFFILIATE:</div>
					  	 <div class="listp">
					  	 <!-- input field -->
					     	 <input type="checkbox" name="ROLE_CUSTOMER_AFFILIATE" value ="ROLE_CUSTOMER_AFFILIATE" <c:if test="${accessPrivilegeForm.privilege.roleCustomerAffiliate}">checked</c:if>  <c:if test="${accessPrivilegeForm.privilege.user.groupIds != ''}">disabled="disabled"</c:if>>
					   	 <!-- end input field -->	
						 </div>
					  	 </div>
					  	 <div class="list${classIndex % 2}"><c:set var="classIndex" value="${classIndex+1}" />
					  	 <div class="listfl"><input type="hidden" name="__key" value="ROLE_CUSTOMER_CHECKBOX_EDIT">CHECKBOX EDIT:</div>
					  	 <div class="listp">
					  	 <!-- input field -->
					     	 <input type="checkbox" name="ROLE_CUSTOMER_CHECKBOX_EDIT" value ="ROLE_CUSTOMER_CHECKBOX_EDIT" <c:if test="${accessPrivilegeForm.privilege.roleCustomerCheckboxEdit}">checked</c:if>  <c:if test="${accessPrivilegeForm.privilege.user.groupIds != ''}">disabled="disabled"</c:if>>
					   	 <!-- end input field -->	
						 </div>
					  	 </div>
					  	 <c:if test="${ gSiteConfig['gBUDGET']}">
					  	 <div class="list${classIndex % 2}"><c:set var="classIndex" value="${classIndex+1}" />
					  	 <div class="listfl"><input type="hidden" name="__key" value="ROLE_CUSTOMER_CREDIT_UPDATE_DELETE">ADD/DEDUCT CREDIT:</div>
					  	 <div class="listp">
					  	 <!-- input field -->
					     	 <input type="checkbox" name="ROLE_CUSTOMER_CREDIT_UPDATE_DELETE" value ="ROLE_CUSTOMER_CREDIT_UPDATE_DELETE" <c:if test="${groupPrivilegeForm.privilege.roleCustomerCreditUpdateDelete}">checked</c:if>>
					   	 <!-- end input field -->	
						 </div>
					  	 </div>
					  	 </c:if>
					  	 <c:if test="${fn:contains(siteConfig['CUSTOMIZE_CUSTOMER_LIST'].value,'trackCode')}">				  	  
					  	 <div class="list${classIndex % 2}"><c:set var="classIndex" value="${classIndex+1}" />
					  	 <div class="listfl"><input type="hidden" name="__key" value="ROLE_CUSTOMER_UPDATE_TRACKCODE">UPDATE TRACK CODE:</div>
					  	 <div class="listp">
					  	 <!-- input field -->
					     	 <input type="checkbox" name="ROLE_CUSTOMER_UPDATE_TRACKCODE" value ="ROLE_CUSTOMER_UPDATE_TRACKCODE" <c:if test="${accessPrivilegeForm.privilege.roleCustomerUpdateTrackCode}">checked</c:if> <c:if test="${accessPrivilegeForm.privilege.user.groupIds != ''}">disabled="disabled"</c:if>>
					   	 <!-- end input field -->	
						 </div>
					  	 </div>
					  	 </c:if>
				    </div>           
					</td>
					<td width="50%" >
					<div class="stretchtoggle">
					    <div style="">
						  <span class="titlePrivilege"><fmt:message key="invoice" /></span>
						</div>	
				    </div>
				    <div class="stretcher">
					     <div  class="checkAll">
					       <a style="text-decoration:none;color:red;font-weight:700" href="javascript:;" onclick="SetChecked('INVOICE',5,12);return false;">All</a>&nbsp;/&nbsp;
					       <a style="text-decoration:none;color:red;font-weight:700" href="javascript:;" onclick="UnSetChecked('INVOICE',5,12);return false;">None</a>
					     </div>
				     	 <div class="list${classIndex % 2}"><c:set var="classIndex" value="${classIndex+1}" />
					  	 <div class="listfl"><input type="hidden" name="__key" value="ROLE_INVOICE_EDIT">EDIT:</div>
					  	 <div class="listp">
					  	 <!-- input field -->
					     	 <input type="checkbox" name="ROLE_INVOICE_EDIT" value ="ROLE_INVOICE_EDIT" <c:if test="${accessPrivilegeForm.privilege.roleInvoiceEdit}">checked</c:if>  <c:if test="${accessPrivilegeForm.privilege.user.groupIds != ''}">disabled="disabled"</c:if>>
					   	 <!-- end input field -->	
						 </div>
					  	 </div>
					  	 <div class="list${classIndex % 2}"><c:set var="classIndex" value="${classIndex+1}" />
					  	 <div class="listfl"><input type="hidden" name="__key" value="ROLE_SHIPPED_INVOICE_EDIT">EDIT SHIPPED INVOICE:</div>
					  	 <div class="listp">
					  	 <!-- input field -->
					     	 <input type="checkbox" name="ROLE_SHIPPED_INVOICE_EDIT" value ="ROLE_SHIPPED_INVOICE_EDIT" <c:if test="${accessPrivilegeForm.privilege.roleShippedInvoiceEdit}">checked</c:if>  <c:if test="${accessPrivilegeForm.privilege.user.groupIds != ''}">disabled="disabled"</c:if>>
					   	 <!-- end input field -->	
						 </div>
					  	 </div>
				     	 <div class="list${classIndex % 2}"><c:set var="classIndex" value="${classIndex+1}" />
					  	 <div class="listfl"><input type="hidden" name="__key" value="ROLE_INVOICE_STATUS">UPDATE STATUS:</div>
					  	 <div class="listp">
					  	 <!-- input field -->
					     	 <input type="checkbox" name="ROLE_INVOICE_STATUS" value ="ROLE_INVOICE_STATUS" <c:if test="${accessPrivilegeForm.privilege.roleInvoiceStatus}">checked</c:if>  <c:if test="${accessPrivilegeForm.privilege.user.groupIds != ''}">disabled="disabled"</c:if>>
					   	 <!-- end input field -->	
						 </div>
					  	 </div>
				     	 <div class="list${classIndex % 2}"><c:set var="classIndex" value="${classIndex+1}" />
					  	 <div class="listfl"><input type="hidden" name="__key" value="ROLE_INVOICE_VIEW_LIST">VIEW LIST:</div>
					  	 <div class="listp">
					  	 <!-- input field -->
					     	 <input type="checkbox" name="ROLE_INVOICE_VIEW_LIST" value ="ROLE_INVOICE_VIEW_LIST" <c:if test="${accessPrivilegeForm.privilege.roleInvoiceViewList}">checked</c:if>  <c:if test="${accessPrivilegeForm.privilege.user.groupIds != ''}">disabled="disabled"</c:if>>
					   	 <!-- end input field -->	
						 </div>
					  	 </div>
					  	 <div class="list${classIndex % 2}"><c:set var="classIndex" value="${classIndex+1}" />
					  	 <div class="listfl"><input type="hidden" name="__key" value="ROLE_INVOICE_PACKING_LIST">PACKING LIST:</div>
					  	 <div class="listp">
					  	 <!-- input field -->
					     	 <input type="checkbox" name="ROLE_INVOICE_PACKING_LIST" value ="ROLE_INVOICE_PACKING_LIST" <c:if test="${accessPrivilegeForm.privilege.roleInvoicePackingList}">checked</c:if>  <c:if test="${accessPrivilegeForm.privilege.user.groupIds != ''}">disabled="disabled"</c:if>>
					   	 <!-- end input field -->	
						 </div>
					  	 </div> 
					  	 <div class="list${classIndex % 2}"><c:set var="classIndex" value="${classIndex+1}" />
					  	 <div class="listfl"><input type="hidden" name="__key" value="ROLE_INVOICE_IMPORT_EXPORT">IMPORT/EXPORT:</div>
					  	 <div class="listp">
					  	 <!-- input field -->
					     	 <input type="checkbox" name="ROLE_INVOICE_IMPORT_EXPORT" value ="ROLE_INVOICE_IMPORT_EXPORT" <c:if test="${accessPrivilegeForm.privilege.roleInvoiceImportExport}">checked</c:if>  <c:if test="${accessPrivilegeForm.privilege.user.groupIds != ''}">disabled="disabled"</c:if>>
					   	 <!-- end input field -->	
						 </div>
					  	 </div>
					  	 <div class="list${classIndex % 2}"><c:set var="classIndex" value="${classIndex+1}" />
					  	 <div class="listfl"><input type="hidden" name="__key" value="ROLE_OVERRIDE_PAYMENT_ALERT">Override Payment Alert:</div>
					  	 <div class="listp">
					  	 <!-- input field -->
					     	 <input type="checkbox" name="ROLE_OVERRIDE_PAYMENT_ALERT" value ="ROLE_OVERRIDE_PAYMENT_ALERT" <c:if test="${accessPrivilegeForm.privilege.roleOverridePaymentAlert}">checked</c:if>  <c:if test="${accessPrivilegeForm.privilege.user.groupIds != ''}">disabled="disabled"</c:if>>
					   	 <!-- end input field -->	
						 </div>
					  	 </div> 
				    </div>           
					</td>
					</tr>
					
					<tr valign="top">
				    <td width="50%" >
				    <div class="stretchtoggle">
					    <div style="">
						  <span class="titlePrivilege"><fmt:message key="incomingPayments" /></span>
						</div>	
				    </div>
				    <div class="stretcher">
				    	<div  class="checkAll">
					       <a style="text-decoration:none;color:red;font-weight:700" href="javascript:;" onclick="SetChecked('INCOMING_PAYMENT',5,21);return false;">All</a>&nbsp;/&nbsp;
					       <a style="text-decoration:none;color:red;font-weight:700" href="javascript:;" onclick="UnSetChecked('INCOMING_PAYMENT',5,21);return false;">None</a>
					    </div>
					    <div class="list${classIndex % 2}"><c:set var="classIndex" value="${classIndex+1}" />
					  	 <div class="listfl"><input type="hidden" name="__key" value="ROLE_INCOMING_PAYMENT_VIEW_LIST">VIEW LIST:</div>
					  	 <div class="listp">
					  	 <!-- input field -->
					     	 <input type="checkbox" name="ROLE_INCOMING_PAYMENT_VIEW_LIST" value ="ROLE_INCOMING_PAYMENT_VIEW_LIST" <c:if test="${accessPrivilegeForm.privilege.roleIncomingPaymentViewList}">checked</c:if>  <c:if test="${accessPrivilegeForm.privilege.user.groupIds != ''}">disabled="disabled"</c:if>>
					   	 <!-- end input field -->	
						 </div>
					  	 </div>
					  	 <div class="list${classIndex % 2}"><c:set var="classIndex" value="${classIndex+1}" />
					  	 <div class="listfl"><input type="hidden" name="__key" value="ROLE_INCOMING_PAYMENT_CREATE">CREATE:</div>
					  	 <div class="listp">
					  	 <!-- input field -->
					     	 <input type="checkbox" name="ROLE_INCOMING_PAYMENT_CREATE" value ="ROLE_INCOMING_PAYMENT_CREATE" <c:if test="${accessPrivilegeForm.privilege.roleIncomingPaymentCreate}">checked</c:if>  <c:if test="${accessPrivilegeForm.privilege.user.groupIds != ''}">disabled="disabled"</c:if>>
					   	 <!-- end input field -->	
						 </div>
					  	 </div>
					  	 <div class="list${classIndex % 2}"><c:set var="classIndex" value="${classIndex+1}" />
					  	 <div class="listfl"><input type="hidden" name="__key" value="ROLE_INCOMING_PAYMENT_CANCEL">CANCEL:</div>
					  	 <div class="listp">
					  	 <!-- input field -->
					     	 <input type="checkbox" name="ROLE_INCOMING_PAYMENT_CANCEL" value ="ROLE_INCOMING_PAYMENT_CANCEL" <c:if test="${accessPrivilegeForm.privilege.roleIncomingPaymentCancel}">checked</c:if>  <c:if test="${accessPrivilegeForm.privilege.user.groupIds != ''}">disabled="disabled"</c:if>>
					   	 <!-- end input field -->	
						 </div>
					  	 </div>
				    </div>
				    </td>
				    
				    <td width="50%" >
					<div class="stretchtoggle">
					    <div style="">
						  <span class="titlePrivilege"><fmt:message key="outgoingPayments" /></span>
						</div>	
				    </div>
				    <div class="stretcher">
				    	<div  class="checkAll">
					       <a style="text-decoration:none;color:red;font-weight:700" href="javascript:;" onclick="SetChecked('OUTGOING_PAYMENT',5,21);return false;">All</a>&nbsp;/&nbsp;
					       <a style="text-decoration:none;color:red;font-weight:700" href="javascript:;" onclick="UnSetChecked('OUTGOING_PAYMENT',5,21);return false;">None</a>
					    </div>
					    <div class="list${classIndex % 2}"><c:set var="classIndex" value="${classIndex+1}" />
					  	 <div class="listfl"><input type="hidden" name="__key" value="ROLE_OUTGOING_PAYMENT_VIEW_LIST">VIEW LIST:</div>
					  	 <div class="listp">
					  	 <!-- input field -->
					     	 <input type="checkbox" name="ROLE_OUTGOING_PAYMENT_VIEW_LIST" value ="ROLE_OUTGOING_PAYMENT_VIEW_LIST" <c:if test="${accessPrivilegeForm.privilege.roleOutgoingPaymentViewList}">checked</c:if>  <c:if test="${accessPrivilegeForm.privilege.user.groupIds != ''}">disabled="disabled"</c:if>>
					   	 <!-- end input field -->	
						 </div>
					  	 </div>
					  	 <div class="list${classIndex % 2}"><c:set var="classIndex" value="${classIndex+1}" />
					  	 <div class="listfl"><input type="hidden" name="__key" value="ROLE_OUTGOING_PAYMENT_CREATE">CREATE:</div>
					  	 <div class="listp">
					  	 <!-- input field -->
					     	 <input type="checkbox" name="ROLE_OUTGOING_PAYMENT_CREATE" value ="ROLE_OUTGOING_PAYMENT_CREATE" <c:if test="${accessPrivilegeForm.privilege.roleOutgoingPaymentCreate}">checked</c:if>  <c:if test="${accessPrivilegeForm.privilege.user.groupIds != ''}">disabled="disabled"</c:if>>
					   	 <!-- end input field -->	
						 </div>
					  	 </div>
					  	 <div class="list${classIndex % 2}"><c:set var="classIndex" value="${classIndex+1}" />
					  	 <div class="listfl"><input type="hidden" name="__key" value="ROLE_OUTGOING_PAYMENT_CANCEL">CANCEL:</div>
					  	 <div class="listp">
					  	 <!-- input field -->
					     	 <input type="checkbox" name="ROLE_OUTGOING_PAYMENT_CANCEL" value ="ROLE_OUTGOING_PAYMENT_CANCEL" <c:if test="${accessPrivilegeForm.privilege.roleOutgoingPaymentCancel}">checked</c:if>  <c:if test="${accessPrivilegeForm.privilege.user.groupIds != ''}">disabled="disabled"</c:if>>
					   	 <!-- end input field -->	
						 </div>
					  	 </div>
				    </div>
				    </td>
				    </tr>
					
					<tr valign="top">
				    <td width="50%" >
				    <div class="stretchtoggle">
					    <div style="">
						  <span class="titlePrivilege"><fmt:message key="product" /></span>
						</div>	
				    </div>
				    <div class="stretcher">
					     <div  class="checkAll">
					       <a style="text-decoration:none;color:red;font-weight:700" href="javascript:;" onclick="SetChecked('PRODUCT',5,12);return false;">All</a>&nbsp;/&nbsp;
					       <a style="text-decoration:none;color:red;font-weight:700" href="javascript:;" onclick="UnSetChecked('PRODUCT',5,12);return false;">None</a>
					     </div>
				     	 <div class="list${classIndex % 2}"><c:set var="classIndex" value="${classIndex+1}" />
					  	 <div class="listfl"><input type="hidden" name="__key" value="ROLE_PRODUCT_CREATE">CREATE:</div>
					  	 <div class="listp">
					  	 <!-- input field -->
					     	 <input type="checkbox" name="ROLE_PRODUCT_CREATE" value ="ROLE_PRODUCT_CREATE" <c:if test="${accessPrivilegeForm.privilege.roleProductCreate}">checked</c:if>  <c:if test="${accessPrivilegeForm.privilege.user.groupIds != ''}">disabled="disabled"</c:if>>
					   	 <!-- end input field -->	
						 </div>
					  	 </div> 
				     	 <div class="list${classIndex % 2}"><c:set var="classIndex" value="${classIndex+1}" />
					  	 <div class="listfl"><input type="hidden" name="__key" value="ROLE_PRODUCT_EDIT">EDIT:</div>
					  	 <div class="listp">
					  	 <!-- input field -->
					     	 <input type="checkbox" name="ROLE_PRODUCT_EDIT" value ="ROLE_PRODUCT_EDIT" <c:if test="${accessPrivilegeForm.privilege.roleProductEdit}">checked</c:if>  <c:if test="${accessPrivilegeForm.privilege.user.groupIds != ''}">disabled="disabled"</c:if>>
					   	 <!-- end input field -->	
						 </div>
					  	 </div> 
				     	 <div class="list${classIndex % 2}"><c:set var="classIndex" value="${classIndex+1}" />
					  	 <div class="listfl"><input type="hidden" name="__key" value="ROLE_PRODUCT_DELETE">DELETE:</div>
					  	 <div class="listp">
					  	 <!-- input field -->
					     	 <input type="checkbox" name="ROLE_PRODUCT_DELETE" value ="ROLE_PRODUCT_DELETE" <c:if test="${accessPrivilegeForm.privilege.roleProductDelete}">checked</c:if>  <c:if test="${accessPrivilegeForm.privilege.user.groupIds != ''}">disabled="disabled"</c:if>>
					   	 <!-- end input field -->	
						 </div>
					  	 </div> 
				     	 <div class="list${classIndex % 2}"><c:set var="classIndex" value="${classIndex+1}" />
					  	 <div class="listfl"><input type="hidden" name="__key" value="ROLE_PRODUCT_VIEW_LIST">VIEW LIST:</div>
					  	 <div class="listp">
					  	 <!-- input field -->
					     	 <input type="checkbox" name="ROLE_PRODUCT_VIEW_LIST" value ="ROLE_PRODUCT_VIEW_LIST" <c:if test="${accessPrivilegeForm.privilege.roleProductViewList}">checked</c:if>  <c:if test="${accessPrivilegeForm.privilege.user.groupIds != ''}">disabled="disabled"</c:if>>
					   	 <!-- end input field -->	
						 </div>
					  	 </div>
					  	 <div class="list${classIndex % 2}"><c:set var="classIndex" value="${classIndex+1}" />
					  	 <div class="listfl"><input type="hidden" name="__key" value="ROLE_PRODUCT_VIEW_LISTXX">VIEW LISTXX:</div>
					  	 <div class="listp">
					  	 <!-- input field -->
					     	 <input type="checkbox" name="ROLE_PRODUCT_VIEW_LISTXX" value ="ROLE_PRODUCT_VIEW_LISTXX" <c:if test="${accessPrivilegeForm.privilege.roleProductViewListXX}">checked</c:if>  <c:if test="${accessPrivilegeForm.privilege.user.groupIds != ''}">disabled="disabled"</c:if>>
					   	 <!-- end input field -->	
						 </div>
					  	 </div>  
				     	 <div class="list${classIndex % 2}"><c:set var="classIndex" value="${classIndex+1}" />
					  	 <div class="listfl"><input type="hidden" name="__key" value="ROLE_PRODUCT_IMPORT_EXPORT">IMPORT/EXPORT:</div>
					  	 <div class="listp">
					  	 <!-- input field -->
					     	 <input type="checkbox" name="ROLE_PRODUCT_IMPORT_EXPORT" value ="ROLE_PRODUCT_IMPORT_EXPORT" <c:if test="${accessPrivilegeForm.privilege.roleProductImportExport}">checked</c:if>  <c:if test="${accessPrivilegeForm.privilege.user.groupIds != ''}">disabled="disabled"</c:if>>
					   	 <!-- end input field -->	
						 </div>
					  	 </div> 
				     	 <div class="list${classIndex % 2}"><c:set var="classIndex" value="${classIndex+1}" />
					  	 <div class="listfl"><input type="hidden" name="__key" value="ROLE_PRODUCT_FIELD_EDIT">FIELD EDIT:</div>
					  	 <div class="listp">
					  	 <!-- input field -->
					     	 <input type="checkbox" name="ROLE_PRODUCT_FIELD_EDIT" value ="ROLE_PRODUCT_FIELD_EDIT" <c:if test="${accessPrivilegeForm.privilege.roleProductFieldEdit}">checked</c:if>  <c:if test="${accessPrivilegeForm.privilege.user.groupIds != ''}">disabled="disabled"</c:if>>
					   	 <!-- end input field -->	
						 </div>
					  	 </div> 
				    </div>
				    </td>
					<td width="50%" >
				    <div class="stretchtoggle">
					    <div style="">
						  <span class="titlePrivilege"><fmt:message key="category" /></span>
						</div>	
				    </div>
				    <div class="stretcher">
					    <div  class="checkAll">
					       <a style="text-decoration:none;color:red;font-weight:700" href="javascript:;" onclick="SetChecked('CATEGORY',5,13);return false;">All</a>&nbsp;/&nbsp;
					       <a style="text-decoration:none;color:red;font-weight:700" href="javascript:;" onclick="UnSetChecked('CATEGORY',5,13);return false;">None</a>
					    </div>
					    <div class="list${classIndex % 2}"><c:set var="classIndex" value="${classIndex+1}" />
					  	<div class="listfl"><input type="hidden" name="__key" value="ROLE_CATEGORY_CREATE">CREATE:</div>
					  	<div class="listp">
					  	<!-- input field -->
					     	<input type="checkbox" name="ROLE_CATEGORY_CREATE" value ="ROLE_CATEGORY_CREATE" <c:if test="${accessPrivilegeForm.privilege.roleCategoryCreate}">checked</c:if>  <c:if test="${accessPrivilegeForm.privilege.user.groupIds != ''}">disabled="disabled"</c:if>>
					   	<!-- end input field -->	
						</div>
					  	</div> 
					    <div class="list${classIndex % 2}"><c:set var="classIndex" value="${classIndex+1}" />
					  	<div class="listfl"><input type="hidden" name="__key" value="ROLE_CATEGORY_EDIT">EDIT:</div>
					  	<div class="listp">
					  	<!-- input field -->
					     	<input type="checkbox" name="ROLE_CATEGORY_EDIT" value ="ROLE_CATEGORY_EDIT" <c:if test="${accessPrivilegeForm.privilege.roleCategoryEdit}">checked</c:if>  <c:if test="${accessPrivilegeForm.privilege.user.groupIds != ''}">disabled="disabled"</c:if>>
					   	<!-- end input field -->	
						</div>
					  	</div> 
					    <div class="list${classIndex % 2}"><c:set var="classIndex" value="${classIndex+1}" />
					  	<div class="listfl"><input type="hidden" name="__key" value="ROLE_CATEGORY_DELETE">DELETE:</div>
					  	<div class="listp">
					  	<!-- input field -->
					     	<input type="checkbox" name="ROLE_CATEGORY_DELETE" value ="ROLE_CATEGORY_DELETE" <c:if test="${accessPrivilegeForm.privilege.roleCategoryDelete}">checked</c:if>  <c:if test="${accessPrivilegeForm.privilege.user.groupIds != ''}">disabled="disabled"</c:if>>
					   	<!-- end input field -->	
						</div>
					  	</div> 
					    <div class="list${classIndex % 2}"><c:set var="classIndex" value="${classIndex+1}" />
					  	<div class="listfl"><input type="hidden" name="__key" value="ROLE_CATEGORY_UPDATE_RANK">UPDATE RANK:</div>
					  	<div class="listp">
					  	<!-- input field -->
					     	<input type="checkbox" name="ROLE_CATEGORY_UPDATE_RANK" value ="ROLE_CATEGORY_UPDATE_RANK" <c:if test="${accessPrivilegeForm.privilege.roleCategoryUpdateRank}">checked</c:if>  <c:if test="${accessPrivilegeForm.privilege.user.groupIds != ''}">disabled="disabled"</c:if>>
					   	<!-- end input field -->	
						</div>
					  	</div> 
					  	<div class="list${classIndex % 2}"><c:set var="classIndex" value="${classIndex+1}" />
					  	<div class="listfl"><input type="hidden" name="__key" value="ROLE_CATEGORY_UPDATE_HEAD_TAG">UPDATE HEAD TAG:</div>
					  	<div class="listp">
					  	<!-- input field -->
					     	<input type="checkbox" name="ROLE_CATEGORY_UPDATE_HEAD_TAG" value ="ROLE_CATEGORY_UPDATE_HEAD_TAG" <c:if test="${accessPrivilegeForm.privilege.roleCategoryUpdateHeadTag}">checked</c:if>  <c:if test="${accessPrivilegeForm.privilege.user.groupIds != ''}">disabled="disabled"</c:if>>
					   	<!-- end input field -->	
						</div>
					  	</div> 
				    </div>           
					</td>
					</tr>  
					<tr valign="top">
				    <td width="50%" >
				    <div class="stretchtoggle">
					    <div style="">
						  <span class="titlePrivilege"><fmt:message key="salesRep" /></span>
						</div>	
				    </div>
				    <div class="stretcher">
				     	<div  class="checkAll">
				       		<a style="text-decoration:none;color:red;font-weight:700" href="javascript:;" onclick="SetChecked('SALES_REP',5,14);return false;">All</a>&nbsp;/&nbsp;
				       		<a style="text-decoration:none;color:red;font-weight:700" href="javascript:;" onclick="UnSetChecked('SALES_REP',5,14);return false;">None</a>
				     	</div>
				     	<div class="list${classIndex % 2}"><c:set var="classIndex" value="${classIndex+1}" />
					  	<div class="listfl"><input type="hidden" name="__key" value="ROLE_SALES_REP_CREATE">CREATE:</div>
					  	<div class="listp">
					  	<!-- input field -->
					     	<input type="checkbox" name="ROLE_SALES_REP_CREATE" value ="ROLE_SALES_REP_CREATE" <c:if test="${accessPrivilegeForm.privilege.roleSalesRepCreate}">checked</c:if>  <c:if test="${accessPrivilegeForm.privilege.user.groupIds != ''}">disabled="disabled"</c:if>>
					   	<!-- end input field -->	
						</div>
					  	</div> 
				     	<div class="list${classIndex % 2}"><c:set var="classIndex" value="${classIndex+1}" />
					  	<div class="listfl"><input type="hidden" name="__key" value="ROLE_SALES_REP_EDIT">EDIT:</div>
					  	<div class="listp">
					  	<!-- input field -->
					     	<input type="checkbox" name="ROLE_SALES_REP_EDIT" value ="ROLE_SALES_REP_EDIT" <c:if test="${accessPrivilegeForm.privilege.roleSalesRepEdit}">checked</c:if>  <c:if test="${accessPrivilegeForm.privilege.user.groupIds != ''}">disabled="disabled"</c:if>>
					   	<!-- end input field -->	
						</div>
					  	</div> 
				     	<div class="list${classIndex % 2}"><c:set var="classIndex" value="${classIndex+1}" />
					  	<div class="listfl"><input type="hidden" name="__key" value="ROLE_SALES_REP_DELETE">DELETE:</div>
					  	<div class="listp">
					  	<!-- input field -->
					     	<input type="checkbox" name="ROLE_SALES_REP_DELETE" value ="ROLE_SALES_REP_DELETE" <c:if test="${accessPrivilegeForm.privilege.roleSalesRepDelete}">checked</c:if>  <c:if test="${accessPrivilegeForm.privilege.user.groupIds != ''}">disabled="disabled"</c:if>>
					   	<!-- end input field -->	
						</div>
					  	</div> 
					  	<div class="list${classIndex % 2}"><c:set var="classIndex" value="${classIndex+1}" />
					  	<div class="listfl"><input type="hidden" name="__key" value="ROLE_SALES_REP_VIEW_LIST">VIEW LIST:</div>
					  	<div class="listp">
					  	<!-- input field -->
					     	<input type="checkbox" name="ROLE_SALES_REP_VIEW_LIST" value ="ROLE_SALES_REP_VIEW_LIST" <c:if test="${accessPrivilegeForm.privilege.roleSalesRepViewList}">checked</c:if>  <c:if test="${accessPrivilegeForm.privilege.user.groupIds != ''}">disabled="disabled"</c:if>>
					   	<!-- end input field -->	
						</div>
					  	</div>
				    </div>
				    </td>
					<td width="50%" >
					<div class="stretchtoggle">
					    <div style="">
						  <span class="titlePrivilege"><fmt:message key="layout" /></span>
						</div>	
				    </div>
				    <div class="stretcher">
					     <div  class="checkAll">
					       <a style="text-decoration:none;color:red;font-weight:700" href="javascript:;" onclick="SetChecked('LAYOUT',5,11);return false;">All</a>&nbsp;/&nbsp;
					       <a style="text-decoration:none;color:red;font-weight:700" href="javascript:;" onclick="UnSetChecked('LAYOUT',5,11);return false;">None</a>
					     </div>
				         <div class="list${classIndex % 2}"><c:set var="classIndex" value="${classIndex+1}" />
					  	 <div class="listfl"><input type="hidden" name="__key" value="ROLE_LAYOUT_EDIT">EDIT:</div>
					  	 <div class="listp">
					  	 <!-- input field -->
					     	 <input type="checkbox" name="ROLE_LAYOUT_EDIT" value ="ROLE_LAYOUT_EDIT" <c:if test="${accessPrivilegeForm.privilege.roleLayoutEdit}">checked</c:if>  <c:if test="${accessPrivilegeForm.privilege.user.groupIds != ''}">disabled="disabled"</c:if>>
					   	 <!-- end input field -->	
						 </div>
					  	 </div> 
					  	 <div class="list${classIndex % 2}"><c:set var="classIndex" value="${classIndex+1}" />
					  	 <div class="listfl"><input type="hidden" name="__key" value="ROLE_LAYOUT_HEAD_TAG">EDIT HEAD TAG:</div>
					  	 <div class="listp">
					  	 <!-- input field -->
					     	 <input type="checkbox" name="ROLE_LAYOUT_HEAD_TAG" value ="ROLE_LAYOUT_HEAD_TAG" <c:if test="${accessPrivilegeForm.privilege.roleLayoutHeadTag}">checked</c:if>  <c:if test="${accessPrivilegeForm.privilege.user.groupIds != ''}">disabled="disabled"</c:if>>
					   	 <!-- end input field -->	
						 </div>
					  	 </div> 
				    </div>           
					</td>
					</tr>
					<tr valign="top">
				    <td width="50%" >
				    <div class="stretchtoggle">
					    <div style="">
						  <span class="titlePrivilege"><fmt:message key="faq" /></span>
						</div>	
				    </div>
				    <div class="stretcher">
					     <div  class="checkAll">
					       <a style="text-decoration:none;color:red;font-weight:700" href="javascript:;" onclick="SetChecked('FAQ',5,8);return false;">All</a>&nbsp;/&nbsp;
					       <a style="text-decoration:none;color:red;font-weight:700" href="javascript:;" onclick="UnSetChecked('FAQ',5,8);return false;">None</a>
					     </div>
				         <div class="list${classIndex % 2}"><c:set var="classIndex" value="${classIndex+1}" />
					  	 <div class="listfl"><input type="hidden" name="__key" value="ROLE_FAQ_CREATE">CREATE:</div>
					  	 <div class="listp">
					  	 <!-- input field -->
					     	 <input type="checkbox" name="ROLE_FAQ_CREATE" value ="ROLE_FAQ_CREATE" <c:if test="${accessPrivilegeForm.privilege.roleFAQCreate}">checked</c:if>  <c:if test="${accessPrivilegeForm.privilege.user.groupIds != ''}">disabled="disabled"</c:if>>
					   	 <!-- end input field -->	
						 </div>
					  	 </div> 
				         <div class="list${classIndex % 2}"><c:set var="classIndex" value="${classIndex+1}" />
					  	 <div class="listfl"><input type="hidden" name="__key" value="ROLE_FAQ_EDIT">EDIT:</div>
					  	 <div class="listp">
					  	 <!-- input field -->
					     	 <input type="checkbox" name="ROLE_FAQ_EDIT" value ="ROLE_FAQ_EDIT" <c:if test="${accessPrivilegeForm.privilege.roleFAQEdit}">checked</c:if>  <c:if test="${accessPrivilegeForm.privilege.user.groupIds != ''}">disabled="disabled"</c:if>>
					   	 <!-- end input field -->	
						 </div>
					  	 </div> 
				         <div class="list${classIndex % 2}"><c:set var="classIndex" value="${classIndex+1}" />
					  	 <div class="listfl"><input type="hidden" name="__key" value="ROLE_FAQ_DELETE">DELETE:</div>
					  	 <div class="listp">
					  	 <!-- input field -->
					     	 <input type="checkbox" name="ROLE_FAQ_DELETE" value ="ROLE_FAQ_DELETE" <c:if test="${accessPrivilegeForm.privilege.roleFAQDelete}">checked</c:if>  <c:if test="${accessPrivilegeForm.privilege.user.groupIds != ''}">disabled="disabled"</c:if>>
					   	 <!-- end input field -->	
						 </div>
					  	 </div> 
				    </div>
				    </td>
					<td width="50%" >
					<div class="stretchtoggle">
					    <div style="">
						  <span class="titlePrivilege"><fmt:message key="policy" /></span>
						</div>	
				    </div>
				    <div class="stretcher">
					     <div  class="checkAll">
					       <a style="text-decoration:none;color:red;font-weight:700" href="javascript:;" onclick="SetChecked('POLICY',5,11);return false;">All</a>&nbsp;/&nbsp;
					       <a style="text-decoration:none;color:red;font-weight:700" href="javascript:;" onclick="UnSetChecked('POLICY',5,11);return false;">None</a>
					     </div>
				         <div class="list${classIndex % 2}"><c:set var="classIndex" value="${classIndex+1}" />
					  	 <div class="listfl"><input type="hidden" name="__key" value="ROLE_POLICY_CREATE">CREATE:</div>
					  	 <div class="listp">
					  	 <!-- input field -->
					     	 <input type="checkbox" name="ROLE_POLICY_CREATE" value ="ROLE_POLICY_CREATE" <c:if test="${accessPrivilegeForm.privilege.rolePolicyCreate}">checked</c:if>  <c:if test="${accessPrivilegeForm.privilege.user.groupIds != ''}">disabled="disabled"</c:if>>
					   	 <!-- end input field -->	
						 </div>
					  	 </div> 
				         <div class="list${classIndex % 2}"><c:set var="classIndex" value="${classIndex+1}" />
					  	 <div class="listfl"><input type="hidden" name="__key" value="ROLE_POLICY_EDIT">EDIT:</div>
					  	 <div class="listp">
					  	 <!-- input field -->
					     	 <input type="checkbox" name="ROLE_POLICY_EDIT" value ="ROLE_POLICY_EDIT" <c:if test="${accessPrivilegeForm.privilege.rolePolicyEdit}">checked</c:if>  <c:if test="${accessPrivilegeForm.privilege.user.groupIds != ''}">disabled="disabled"</c:if>>
					   	 <!-- end input field -->	
						 </div>
					  	 </div> 
				         <div class="list${classIndex % 2}"><c:set var="classIndex" value="${classIndex+1}" />
					  	 <div class="listfl"><input type="hidden" name="__key" value="ROLE_POLICY_DELETE">DELETE:</div>
					  	 <div class="listp">
					  	 <!-- input field -->
					     	 <input type="checkbox" name="ROLE_POLICY_DELETE" value ="ROLE_POLICY_DELETE" <c:if test="${accessPrivilegeForm.privilege.rolePolicyDelete}">checked</c:if>  <c:if test="${accessPrivilegeForm.privilege.user.groupIds != ''}">disabled="disabled"</c:if>>
					   	 <!-- end input field -->	
						 </div>
					  	 </div> 
				    </div>           
					</td>
					</tr>  
					<tr valign="top">
				    <td width="50%" >
				    <div class="stretchtoggle">
					    <div style="">
						  <span class="titlePrivilege"><fmt:message key="promotionsTitle" /></span>
						</div>	
				    </div>
				    <div class="stretcher">
					     <div  class="checkAll">
					       <a style="text-decoration:none;color:red;font-weight:700" href="javascript:;" onclick="SetChecked('PROMOTION',5,14);return false;">All</a>&nbsp;/&nbsp;
					       <a style="text-decoration:none;color:red;font-weight:700" href="javascript:;" onclick="UnSetChecked('PROMOTION',5,14);return false;">None</a>
					     </div>
					     <div class="list${classIndex % 2}"><c:set var="classIndex" value="${classIndex+1}" />
					  	 <div class="listfl"><input type="hidden" name="__key" value="ROLE_PROMOTION_CREATE">CREATE:</div>
					  	 <div class="listp">
					  	 <!-- input field -->
					     	 <input type="checkbox" name="ROLE_PROMOTION_CREATE" value ="ROLE_PROMOTION_CREATE" <c:if test="${accessPrivilegeForm.privilege.rolePromotionCreate}">checked</c:if>  <c:if test="${accessPrivilegeForm.privilege.user.groupIds != ''}">disabled="disabled"</c:if>>
					   	 <!-- end input field -->	
						 </div>
					  	 </div> 
					     <div class="list${classIndex % 2}"><c:set var="classIndex" value="${classIndex+1}" />
					  	 <div class="listfl"><input type="hidden" name="__key" value="ROLE_PROMOTION_EDIT">EDIT:</div>
					  	 <div class="listp">
					  	 <!-- input field -->
					     	 <input type="checkbox" name="ROLE_PROMOTION_EDIT" value ="ROLE_PROMOTION_EDIT" <c:if test="${accessPrivilegeForm.privilege.rolePromotionEdit}">checked</c:if>  <c:if test="${accessPrivilegeForm.privilege.user.groupIds != ''}">disabled="disabled"</c:if>>
					   	 <!-- end input field -->	
						 </div>
					  	 </div> 
					     <div class="list${classIndex % 2}"><c:set var="classIndex" value="${classIndex+1}" />
					  	 <div class="listfl"><input type="hidden" name="__key" value="ROLE_PROMOTION_DELETE">DELETE:</div>
					  	 <div class="listp">
					  	 <!-- input field -->
					     	 <input type="checkbox" name="ROLE_PROMOTION_DELETE" value ="ROLE_PROMOTION_DELETE" <c:if test="${accessPrivilegeForm.privilege.rolePromotionDelete}">checked</c:if>  <c:if test="${accessPrivilegeForm.privilege.user.groupIds != ''}">disabled="disabled"</c:if>>
					   	 <!-- end input field -->	
						 </div>
					  	 </div> 
				    </div>
				    </td>
					<td width="50%" >
					<div class="stretchtoggle">
					    <div style="">
						  <span class="titlePrivilege"><fmt:message key="salesTag" /></span>
						</div>	
				    </div>
				    <div class="stretcher">
					     <div  class="checkAll">
					       <a style="text-decoration:none;color:red;font-weight:700" href="javascript:;" onclick="SetChecked('SALES_TAG',5,14);return false;">All</a>&nbsp;/&nbsp;
					       <a style="text-decoration:none;color:red;font-weight:700" href="javascript:;" onclick="UnSetChecked('SALES_TAG',5,14);return false;">None</a>
					     </div>
					     <div class="list${classIndex % 2}"><c:set var="classIndex" value="${classIndex+1}" />
					  	 <div class="listfl"><input type="hidden" name="__key" value="ROLE_SALES_TAG_CREATE">CREATE:</div>
					  	 <div class="listp">
					  	 <!-- input field -->
					     	 <input type="checkbox" name="ROLE_SALES_TAG_CREATE" value ="ROLE_SALES_TAG_CREATE" <c:if test="${accessPrivilegeForm.privilege.roleSalesTagCreate}">checked</c:if>  <c:if test="${accessPrivilegeForm.privilege.user.groupIds != ''}">disabled="disabled"</c:if>>
					   	 <!-- end input field -->	
						 </div>
					  	 </div> 
					     <div class="list${classIndex % 2}"><c:set var="classIndex" value="${classIndex+1}" />
					  	 <div class="listfl"><input type="hidden" name="__key" value="ROLE_SALES_TAG_EDIT">EDIT:</div>
					  	 <div class="listp">
					  	 <!-- input field -->
					     	 <input type="checkbox" name="ROLE_SALES_TAG_EDIT" value ="ROLE_SALES_TAG_EDIT" <c:if test="${accessPrivilegeForm.privilege.roleSalesTagEdit}">checked</c:if>  <c:if test="${accessPrivilegeForm.privilege.user.groupIds != ''}">disabled="disabled"</c:if>>
					   	 <!-- end input field -->	
						 </div>
					  	 </div> 
					     <div class="list${classIndex % 2}"><c:set var="classIndex" value="${classIndex+1}" />
					  	 <div class="listfl"><input type="hidden" name="__key" value="ROLE_SALES_TAG_DELETE">DELETE:</div>
					  	 <div class="listp">
					  	 <!-- input field -->
					     	 <input type="checkbox" name="ROLE_SALES_TAG_DELETE" value ="ROLE_SALES_TAG_DELETE" <c:if test="${accessPrivilegeForm.privilege.roleSalesTagDelete}">checked</c:if>  <c:if test="${accessPrivilegeForm.privilege.user.groupIds != ''}">disabled="disabled"</c:if>>
					   	 <!-- end input field -->	
						 </div>
					  	 </div> 
				    </div>           
					</td>
					</tr>
					<tr valign="top">
				    <td width="50%" >
				    <div class="stretchtoggle">
					    <div style="">
						  <span class="titlePrivilege"><fmt:message key="mailInRebates" /></span>
						</div>	
				    </div>
				    <div class="stretcher">
				        <div  class="checkAll">
					       <a style="text-decoration:none;color:red;font-weight:700" href="javascript:;" onclick="SetChecked('MAILINREBATE',5,17);return false;">All</a>&nbsp;/&nbsp;
					       <a style="text-decoration:none;color:red;font-weight:700" href="javascript:;" onclick="UnSetChecked('MAILINREBATE',5,17);return false;">None</a>
					     </div>
					  	 <div class="list${classIndex % 2}"><c:set var="classIndex" value="${classIndex+1}" />
					  	 <div class="listfl"><input type="hidden" name="__key" value="ROLE_MAILINREBATE_CREATE">Create:</div>
					  	 <div class="listp">
					  	 <!-- input field -->
					     	 <input type="checkbox" name="ROLE_MAILINREBATE_CREATE" value ="ROLE_MAILINREBATE_CREATE" <c:if test="${accessPrivilegeForm.privilege.roleMailInRebateCreate}">checked</c:if>  <c:if test="${accessPrivilegeForm.privilege.user.groupIds != ''}">disabled="disabled"</c:if>>
					   	 <!-- end input field -->	
						 </div>
					  	 </div>
					  	 
					  	 <div class="list${classIndex % 2}"><c:set var="classIndex" value="${classIndex+1}" />
					  	 <div class="listfl"><input type="hidden" name="__key" value="ROLE_MAILINREBATE_DELETE">Delete:</div>
					  	 <div class="listp">
					  	 <!-- input field -->
					     	 <input type="checkbox" name="ROLE_MAILINREBATE_DELETE" value ="ROLE_MAILINREBATE_DELETE" <c:if test="${accessPrivilegeForm.privilege.roleMailInRebateDelete}">checked</c:if>  <c:if test="${accessPrivilegeForm.privilege.user.groupIds != ''}">disabled="disabled"</c:if>>
					   	 <!-- end input field -->	
						 </div>
					  	 </div>
				    </div>
				    </td>
					<td width="50%" >
					<div class="stretchtoggle">
					    <div style="">
						  <span class="titlePrivilege"><fmt:message key="deals" /></span>
						</div>	
				    </div>
				    <div class="stretcher">
						<div  class="checkAll">
					       <a style="text-decoration:none;color:red;font-weight:700" href="javascript:;" onclick="SetChecked('DEAL',5,9);return false;">All</a>&nbsp;/&nbsp;
					       <a style="text-decoration:none;color:red;font-weight:700" href="javascript:;" onclick="UnSetChecked('DEAL',5,9);return false;">None</a>
					     </div>
					  	 <div class="list${classIndex % 2}"><c:set var="classIndex" value="${classIndex+1}" />
					  	 <div class="listfl"><input type="hidden" name="__key" value="ROLE_DEAL_CREATE">Create:</div>
					  	 <div class="listp">
					  	 <!-- input field -->
					     	 <input type="checkbox" name="ROLE_DEAL_CREATE" value ="ROLE_DEAL_CREATE" <c:if test="${accessPrivilegeForm.privilege.roleDealCreate}">checked</c:if>  <c:if test="${accessPrivilegeForm.privilege.user.groupIds != ''}">disabled="disabled"</c:if>>
					   	 <!-- end input field -->	
						 </div>
					  	 </div>
					  	 
					  	 <div class="list${classIndex % 2}"><c:set var="classIndex" value="${classIndex+1}" />
					  	 <div class="listfl"><input type="hidden" name="__key" value="ROLE_DEAL_DELETE">Delete:</div>
					  	 <div class="listp">
					  	 <!-- input field -->
					     	 <input type="checkbox" name="ROLE_DEAL_DELETE" value ="ROLE_DEAL_DELETE" <c:if test="${accessPrivilegeForm.privilege.roleDealDelete}">checked</c:if>  <c:if test="${accessPrivilegeForm.privilege.user.groupIds != ''}">disabled="disabled"</c:if>>
					   	 <!-- end input field -->	
						 </div>
					  	 </div>
				    </div>           
					</td>
					</tr>
					<tr valign="top">
				    <td width="50%" >
				    <div class="stretchtoggle">
					    <div style="">
						  <span class="titlePrivilege"><fmt:message key="tickets" /></span>
						</div>	
				    </div>
				    <div class="stretcher">
					     <div  class="checkAll">
					       <a style="text-decoration:none;color:red;font-weight:700" href="javascript:;" onclick="SetChecked('TICKET',5,11);return false;">All</a>&nbsp;/&nbsp;
					       <a style="text-decoration:none;color:red;font-weight:700" href="javascript:;" onclick="UnSetChecked('TICKET',5,11);return false;">None</a>
					     </div>
					     <div class="list${classIndex % 2}"><c:set var="classIndex" value="${classIndex+1}" />
					  	 <div class="listfl"><input type="hidden" name="__key" value="ROLE_TICKET_UPDATE">UPDATE:</div>
					  	 <div class="listp">
					  	 <!-- input field -->
					     	 <input type="checkbox" name="ROLE_TICKET_UPDATE" value ="ROLE_TICKET_UPDATE" <c:if test="${accessPrivilegeForm.privilege.roleTicketUpdate}">checked</c:if>  <c:if test="${accessPrivilegeForm.privilege.user.groupIds != ''}">disabled="disabled"</c:if>>
					   	 <!-- end input field -->	
						 </div>
					  	 </div> 
					     <div class="list${classIndex % 2}"><c:set var="classIndex" value="${classIndex+1}" />
					  	 <div class="listfl"><input type="hidden" name="__key" value="ROLE_TICKET_VIEW_LIST">VIEW LIST:</div>
					  	 <div class="listp">
					  	 <!-- input field -->
					     	 <input type="checkbox" name="ROLE_TICKET_VIEW_LIST" value ="ROLE_TICKET_VIEW_LIST" <c:if test="${accessPrivilegeForm.privilege.roleTicketViewList}">checked</c:if>  <c:if test="${accessPrivilegeForm.privilege.user.groupIds != ''}">disabled="disabled"</c:if>>
					   	 <!-- end input field -->	
						 </div>
					  	 </div>
					  	 <div class="list${classIndex % 2}"><c:set var="classIndex" value="${classIndex+1}" />
					  	 <div class="listfl"><input type="hidden" name="__key" value="ROLE_TICKET_CREATE">CREATE:</div>
					  	 <div class="listp">
					  	 <!-- input field -->
					     	 <input type="checkbox" name="ROLE_TICKET_CREATE" value ="ROLE_TICKET_CREATE" <c:if test="${accessPrivilegeForm.privilege.roleTicketCreate}">checked</c:if>  <c:if test="${accessPrivilegeForm.privilege.user.groupIds != ''}">disabled="disabled"</c:if>>
					   	 <!-- end input field -->	
						 </div>
					  	 </div> 
				    </div>
				    </td>
					<td width="50%" >
					<div class="stretchtoggle">
					    <div style="">
						  <span class="titlePrivilege"><fmt:message key="purchaseOrder" /></span>
						</div>	
				    </div>
				    <div class="stretcher">
					     <div  class="checkAll">
					       <a style="text-decoration:none;color:red;font-weight:700" href="javascript:;" onclick="SetChecked('PURCHASE_ORDER',5,19);return false;">All</a>&nbsp;/&nbsp;
					       <a style="text-decoration:none;color:red;font-weight:700" href="javascript:;" onclick="UnSetChecked('PURCHASE_ORDER',5,19);return false;">None</a>
					     </div>
					     <div class="list${classIndex % 2}"><c:set var="classIndex" value="${classIndex+1}" />
					  	 <div class="listfl"><input type="hidden" name="__key" value="ROLE_PURCHASE_ORDER_CREATE">CREATE:</div>
					  	 <div class="listp">
					  	 <!-- input field -->
					     	 <input type="checkbox" name="ROLE_PURCHASE_ORDER_CREATE" value ="ROLE_PURCHASE_ORDER_CREATE" <c:if test="${accessPrivilegeForm.privilege.rolePurchaseOrderCreate}">checked</c:if>  <c:if test="${accessPrivilegeForm.privilege.user.groupIds != ''}">disabled="disabled"</c:if>>
					   	 <!-- end input field -->	
						 </div>
					  	 </div> 
					     <div class="list${classIndex % 2}"><c:set var="classIndex" value="${classIndex+1}" />
					  	 <div class="listfl"><input type="hidden" name="__key" value="ROLE_PURCHASE_ORDER_VIEW_LIST">VIEW LIST:</div>
					  	 <div class="listp"> 
					  	 <!-- input field -->
					     	 <input type="checkbox" name="ROLE_PURCHASE_ORDER_VIEW_LIST" value ="ROLE_PURCHASE_ORDER_VIEW_LIST" <c:if test="${accessPrivilegeForm.privilege.rolePurchaseOrderViewList}">checked</c:if>  <c:if test="${accessPrivilegeForm.privilege.user.groupIds != ''}">disabled="disabled"</c:if>>
					   	 <!-- end input field -->	
						 </div>
					  	 </div>
					  	 <div class="list${classIndex % 2}"><c:set var="classIndex" value="${classIndex+1}" />
					  	 <div class="listfl"><input type="hidden" name="__key" value="ROLE_PURCHASE_ORDER_UPDATE">UPDATE:</div>
					  	 <div class="listp"> 
					  	 <!-- input field -->
					     	 <input type="checkbox" name="ROLE_PURCHASE_ORDER_UPDATE" value ="ROLE_PURCHASE_ORDER_UPDATE" <c:if test="${accessPrivilegeForm.privilege.rolePurchaseOrderUpdate}">checked</c:if>  <c:if test="${accessPrivilegeForm.privilege.user.groupIds != ''}">disabled="disabled"</c:if>>
					   	 <!-- end input field -->	
						 </div>
					  	 </div>
					  	 <div class="list${classIndex % 2}"><c:set var="classIndex" value="${classIndex+1}" />
					  	 <div class="listfl"><input type="hidden" name="__key" value="ROLE_INVENTORY_IMPORT_EXPORT">INVENTORY IMPORT/EXPORT:</div>
					  	 <div class="listp"> 
					  	 <!-- input field -->
					     	 <input type="checkbox" name="ROLE_INVENTORY_IMPORT_EXPORT" value ="ROLE_INVENTORY_IMPORT_EXPORT" <c:if test="${accessPrivilegeForm.privilege.roleInventoryImportExport}">checked</c:if>  <c:if test="${accessPrivilegeForm.privilege.user.groupIds != ''}">disabled="disabled"</c:if>>
					   	 <!-- end input field -->	
						 </div>
					  	 </div>
				    </div>           
					</td>
					</tr>  
					<tr valign="top">
				    <td width="50%" >
				    <div class="stretchtoggle">
					    <div style="">
						  <span class="titlePrivilege"><fmt:message key="supplier" /></span>
						</div>	
				    </div>
				    <div class="stretcher">
					     <div  class="checkAll">
					       <a style="text-decoration:none;color:red;font-weight:700" href="javascript:;" onclick="SetChecked('SUPPLIER',5,13);return false;">All</a>&nbsp;/&nbsp;
					       <a style="text-decoration:none;color:red;font-weight:700" href="javascript:;" onclick="UnSetChecked('SUPPLIER',5,13);return false;">None</a>
					     </div>
					     <div class="list${classIndex % 2}"><c:set var="classIndex" value="${classIndex+1}" />
					  	 <div class="listfl"><input type="hidden" name="__key" value="ROLE_SUPPLIER_CREATE">CREATE:</div>
					  	 <div class="listp">
					  	 <!-- input field -->
					     	 <input type="checkbox" name="ROLE_SUPPLIER_CREATE" value ="ROLE_SUPPLIER_CREATE" <c:if test="${accessPrivilegeForm.privilege.roleSupplierCreate}">checked</c:if>  <c:if test="${accessPrivilegeForm.privilege.user.groupIds != ''}">disabled="disabled"</c:if>>
					   	 <!-- end input field -->	
						 </div>
					  	 </div>
					     <div class="list${classIndex % 2}"><c:set var="classIndex" value="${classIndex+1}" />
					  	 <div class="listfl"><input type="hidden" name="__key" value="ROLE_SUPPLIER_UPDATE">UPDATE:</div>
					  	 <div class="listp">
					  	 <!-- input field -->
					     	 <input type="checkbox" name="ROLE_SUPPLIER_UPDATE" value ="ROLE_SUPPLIER_UPDATE" <c:if test="${accessPrivilegeForm.privilege.roleSupplierUpdate}">checked</c:if>  <c:if test="${accessPrivilegeForm.privilege.user.groupIds != ''}">disabled="disabled"</c:if>>
					   	 <!-- end input field -->	
						 </div>
					  	 </div> 
					  	 <div class="list${classIndex % 2}"><c:set var="classIndex" value="${classIndex+1}" />
					  	 <div class="listfl"><input type="hidden" name="__key" value="ROLE_SUPPLIER_DELETE">DELETE:</div>
					  	 <div class="listp">
					  	 <!-- input field -->
					     	 <input type="checkbox" name="ROLE_SUPPLIER_DELETE" value ="ROLE_SUPPLIER_DELETE" <c:if test="${accessPrivilegeForm.privilege.roleSupplierDelete}">checked</c:if> <c:if test="${accessPrivilegeForm.privilege.user.groupIds != ''}">disabled="disabled"</c:if>>
					   	 <!-- end input field -->	
						 </div>
					  	 </div> 
					  	 <div class="list${classIndex % 2}"><c:set var="classIndex" value="${classIndex+1}" />
					  	 <div class="listfl"><input type="hidden" name="__key" value="ROLE_SUPPLIER_VIEW_LIST">VIEW LIST:</div>
					  	 <div class="listp">
					  	 <!-- input field -->
					     	 <input type="checkbox" name="ROLE_SUPPLIER_VIEW_LIST" value ="ROLE_SUPPLIER_VIEW_LIST" <c:if test="${accessPrivilegeForm.privilege.roleSupplierViewList}">checked</c:if>  <c:if test="${accessPrivilegeForm.privilege.user.groupIds != ''}">disabled="disabled"</c:if>>
					   	 <!-- end input field -->	
						 </div>
					  	 </div>
				    </div>
				    </td>
					<td width="50%" >
					<div class="stretchtoggle">
					    <div style="">
						  <span class="titlePrivilege"><fmt:message key="dropShip" /></span>
						</div>	
				    </div>
				    <div class="stretcher">
					     <div  class="checkAll">
					       <a style="text-decoration:none;color:red;font-weight:700" href="javascript:;" onclick="SetChecked('DROP_SHIP',5,14);return false;">All</a>&nbsp;/&nbsp;
					       <a style="text-decoration:none;color:red;font-weight:700" href="javascript:;" onclick="UnSetChecked('DROP_SHIP',5,14);return false;">None</a>
					     </div>
					  	 <div class="list${classIndex % 2}"><c:set var="classIndex" value="${classIndex+1}" />
					  	 <div class="listfl"><input type="hidden" name="__key" value="ROLE_DROP_SHIP_CREATE">DROP SHIP:</div>
					  	 <div class="listp"> 
					  	 <!-- input field -->
					     	 <input type="checkbox" name="ROLE_DROP_SHIP_CREATE" value ="ROLE_DROP_SHIP_CREATE" <c:if test="${accessPrivilegeForm.privilege.roleDropShipCreate}">checked</c:if>  <c:if test="${accessPrivilegeForm.privilege.user.groupIds != ''}">disabled="disabled"</c:if>>
					   	 <!-- end input field -->	
						 </div>
					  	 </div>
				    </div>           
					</td>
					</tr>
					<tr valign="top">
				    <td width="50%" >
				    <div class="stretchtoggle">
					    <div style="">
						  <span class="titlePrivilege"><fmt:message key="service" /></span>
						</div>	
				    </div>
				    <div class="stretcher">
					     <div  class="checkAll">
					       <a style="text-decoration:none;color:red;font-weight:700" href="javascript:;" onclick="SetChecked('SERVICE',5,12);return false;">All</a>&nbsp;/&nbsp;
					       <a style="text-decoration:none;color:red;font-weight:700" href="javascript:;" onclick="UnSetChecked('SERVICE',5,12);return false;">None</a>
					     </div>
					  	 <div class="list${classIndex % 2}"><c:set var="classIndex" value="${classIndex+1}" />
					  	 <div class="listfl"><input type="hidden" name="__key" value="ROLE_SERVICE_VIEW_LIST">VIEW LIST:</div>
					  	 <div class="listp">
					  	 <!-- input field -->
					     	 <input type="checkbox" name="ROLE_SERVICE_VIEW_LIST" value ="ROLE_SERVICE_VIEW_LIST" <c:if test="${accessPrivilegeForm.privilege.roleServiceViewList}">checked</c:if>  <c:if test="${accessPrivilegeForm.privilege.user.groupIds != ''}">disabled="disabled"</c:if>>
					   	 <!-- end input field -->	
						 </div>
					  	 </div>
				    </div>
				    </td>
					<td width="50%" >
					<div class="stretchtoggle">
					    <div style="">
						  <span class="titlePrivilege"><fmt:message key="reports" /></span>
						</div>	
				    </div>
				    <div class="stretcher">
					     <div  class="checkAll">
					       <a style="text-decoration:none;color:red;font-weight:700" href="javascript:;" onclick="SetChecked('REPORT',5,11);return false;">All</a>&nbsp;/&nbsp;
					       <a style="text-decoration:none;color:red;font-weight:700" href="javascript:;" onclick="UnSetChecked('REPORT',5,11);return false;">None</a>
					     </div>
					  	 <div class="list${classIndex % 2}"><c:set var="classIndex" value="${classIndex+1}" />
					  	 <div class="listfl"><input type="hidden" name="__key" value="ROLE_REPORT_CUSTOMERS">Customers Inactive/Overview:</div>
					  	 <div class="listp">
					  	 <!-- input field -->
					     	 <input type="checkbox" name="ROLE_REPORT_CUSTOMERS" value ="ROLE_REPORT_CUSTOMERS" <c:if test="${accessPrivilegeForm.privilege.roleReportCustomers}">checked</c:if>  <c:if test="${accessPrivilegeForm.privilege.user.groupIds != ''}">disabled="disabled"</c:if>>
					   	 <!-- end input field -->	
						 </div>
					  	 </div>	
					  	 <div class="list${classIndex % 2}"><c:set var="classIndex" value="${classIndex+1}" />
					  	 <div class="listfl"><input type="hidden" name="__key" value="ROLE_REPORT_ORDERS_DAILY">Orders Daily:</div>
					  	 <div class="listp">
					  	 <!-- input field -->
					     	 <input type="checkbox" name="ROLE_REPORT_ORDERS_DAILY" value ="ROLE_REPORT_ORDERS_DAILY" <c:if test="${accessPrivilegeForm.privilege.roleReportOrdersDaily}">checked</c:if>  <c:if test="${accessPrivilegeForm.privilege.user.groupIds != ''}">disabled="disabled"</c:if>>
					   	 <!-- end input field -->	
						 </div>
					  	 </div>	
					  	 <div class="list${classIndex % 2}"><c:set var="classIndex" value="${classIndex+1}" />
					  	 <div class="listfl"><input type="hidden" name="__key" value="ROLE_REPORT_SALESREP">SalesRep:</div>
					  	 <div class="listp">
					  	 <!-- input field -->
					     	 <input type="checkbox" name="ROLE_REPORT_SALESREP" value ="ROLE_REPORT_SALESREP" <c:if test="${accessPrivilegeForm.privilege.roleReportSalesRep}">checked</c:if>  <c:if test="${accessPrivilegeForm.privilege.user.groupIds != ''}">disabled="disabled"</c:if>>
					   	 <!-- end input field -->	
						 </div>
					  	 </div>	
					  	 <div class="list${classIndex % 2}"><c:set var="classIndex" value="${classIndex+1}" />
					  	 <div class="listfl"><input type="hidden" name="__key" value="ROLE_REPORT_SALESREP_DAILY">SalesRep Daily/Overview:</div>
					  	 <div class="listp">
					  	 <!-- input field -->
					     	 <input type="checkbox" name="ROLE_REPORT_SALESREP_DAILY" value ="ROLE_REPORT_SALESREP_DAILY" <c:if test="${accessPrivilegeForm.privilege.rolereportSalesRepDaily}">checked</c:if>  <c:if test="${accessPrivilegeForm.privilege.user.groupIds != ''}">disabled="disabled"</c:if>>
					   	 <!-- end input field -->	
						 </div>
					  	 </div>	
					  	 <div class="list${classIndex % 2}"><c:set var="classIndex" value="${classIndex+1}" />
					  	 <div class="listfl"><input type="hidden" name="__key" value="ROLE_REPORT_SALESREP_OVERVIEWII">SalesRep Overview II:</div>
					  	 <div class="listp">
					  	 <!-- input field -->
					     	 <input type="checkbox" name="ROLE_REPORT_SALESREP_OVERVIEWII" value ="ROLE_REPORT_SALESREP_OVERVIEWII" <c:if test="${accessPrivilegeForm.privilege.roleReportSalesRepOverviewII}">checked</c:if>  <c:if test="${accessPrivilegeForm.privilege.user.groupIds != ''}">disabled="disabled"</c:if>>
					   	 <!-- end input field -->	
						 </div>
					  	 </div>
					  	 <div class="list${classIndex % 2}"><c:set var="classIndex" value="${classIndex+1}" />
					  	 <div class="listfl"><input type="hidden" name="__key" value="ROLE_REPORT_CUSTOMER_METRIC"><fmt:message key="metricOverviewReport"/></div>
					  	 <div class="listp">
					  	 <!-- input field -->
					     	 <input type="checkbox" name="ROLE_REPORT_CUSTOMER_METRIC" value ="ROLE_REPORT_CUSTOMER_METRIC" <c:if test="${accessPrivilegeForm.privilege.roleReportCustomerMetric}">checked</c:if>  <c:if test="${accessPrivilegeForm.privilege.user.groupIds != ''}">disabled="disabled"</c:if>>
					   	 <!-- end input field -->	
						 </div>
					  	 </div>		
					  	 <div class="list${classIndex % 2}"><c:set var="classIndex" value="${classIndex+1}" />
					  	 <div class="listfl"><input type="hidden" name="__key" value="ROLE_REPORT_TRACKCODE">Track Code:</div>
					  	 <div class="listp">
					  	 <!-- input field -->
					     	 <input type="checkbox" name="ROLE_REPORT_TRACKCODE" value ="ROLE_REPORT_TRACKCODE" <c:if test="${accessPrivilegeForm.privilege.roleReportTrackCode}">checked</c:if>  <c:if test="${accessPrivilegeForm.privilege.user.groupIds != ''}">disabled="disabled"</c:if>>
					   	 <!-- end input field -->	
						 </div>
					  	 </div>	
					  	 <div class="list${classIndex % 2}"><c:set var="classIndex" value="${classIndex+1}" />
					  	 <div class="listfl"><input type="hidden" name="__key" value="ROLE_REPORT_PROCESSING">Product Processing:</div>
					  	 <div class="listp">
					  	 <!-- input field -->
					     	 <input type="checkbox" name="ROLE_REPORT_PROCESSING" value ="ROLE_REPORT_PROCESSING" <c:if test="${accessPrivilegeForm.privilege.roleReportProcessing}">checked</c:if>  <c:if test="${accessPrivilegeForm.privilege.user.groupIds != ''}">disabled="disabled"</c:if>>
					   	 <!-- end input field -->	
						 </div>
					  	 </div>
					  	 <div class="list${classIndex % 2}"><c:set var="classIndex" value="${classIndex+1}" />
					  	 <div class="listfl"><input type="hidden" name="__key" value="ROLE_REPORT_IMP_EXP">Imp/Exp:</div>
					  	 <div class="listp">
					  	 <!-- input field -->
					     	 <input type="checkbox" name="ROLE_REPORT_IMP_EXP" value ="ROLE_REPORT_IMP_EXP" <c:if test="${accessPrivilegeForm.privilege.roleReportImpExp}">checked</c:if>  <c:if test="${accessPrivilegeForm.privilege.user.groupIds != ''}">disabled="disabled"</c:if>>
					   	 <!-- end input field -->	
						 </div>
					  	 </div>	 
					  	 <div class="list${classIndex % 2}"><c:set var="classIndex" value="${classIndex+1}" />
					  	 <div class="listfl"><input type="hidden" name="__key" value="ROLE_REPORT_SHIPPED_ORDER">Shipped Order:</div>
					  	 <div class="listp">
					  	 <!-- input field -->
					     	 <input type="checkbox" name="ROLE_REPORT_SHIPPED_ORDER" value ="ROLE_REPORT_SHIPPED_ORDER" <c:if test="${accessPrivilegeForm.privilege.roleReportShippedOrder}">checked</c:if>  <c:if test="${accessPrivilegeForm.privilege.user.groupIds != ''}">disabled="disabled"</c:if>>
					   	 <!-- end input field -->	
						 </div>
					  	 </div>	
					  	 <div class="list${classIndex % 2}"><c:set var="classIndex" value="${classIndex+1}" />
					  	 <div class="listfl"><input type="hidden" name="__key" value="ROLE_REPORT_ORDER_NEED_ATTENTION">Order Need Attention:</div>
					  	 <div class="listp">
					  	 <!-- input field -->
					     	 <input type="checkbox" name="ROLE_REPORT_ORDER_NEED_ATTENTION" value ="ROLE_REPORT_ORDER_NEED_ATTENTION" <c:if test="${accessPrivilegeForm.privilege.roleReportOrdersNeedAttention}">checked</c:if>  <c:if test="${accessPrivilegeForm.privilege.user.groupIds != ''}">disabled="disabled"</c:if>>
					   	 <!-- end input field -->	
						 </div>
					  	 </div>	
					  	 <div class="list${classIndex % 2}"><c:set var="classIndex" value="${classIndex+1}" />
					  	 <div class="listfl"><input type="hidden" name="__key" value="ROLE_REPORT_ORDER_OVERVIEW">Order Overview:</div>
					  	 <div class="listp">
					  	 <!-- input field -->
					     	 <input type="checkbox" name="ROLE_REPORT_ORDER_OVERVIEW" value ="ROLE_REPORT_ORDER_OVERVIEW" <c:if test="${accessPrivilegeForm.privilege.roleReportOrdersOverview}">checked</c:if>  <c:if test="${accessPrivilegeForm.privilege.user.groupIds != ''}">disabled="disabled"</c:if>>
					   	 <!-- end input field -->	
						 </div>
					  	 </div>	
					  	 <div class="list${classIndex % 2}"><c:set var="classIndex" value="${classIndex+1}" />
					  	 <div class="listfl"><input type="hidden" name="__key" value="ROLE_REPORT_SHIPPED_OVERVIEW">Shipped Overview:</div>
					  	 <div class="listp">
					  	 <!-- input field -->
					     	 <input type="checkbox" name="ROLE_REPORT_SHIPPED_OVERVIEW" value ="ROLE_REPORT_SHIPPED_OVERVIEW" <c:if test="${accessPrivilegeForm.privilege.roleReportShippedOverview}">checked</c:if>  <c:if test="${accessPrivilegeForm.privilege.user.groupIds != ''}">disabled="disabled"</c:if>>
					   	 <!-- end input field -->	
						 </div>
					  	 </div>	
					  	 <div class="list${classIndex % 2}"><c:set var="classIndex" value="${classIndex+1}" />
					  	 <div class="listfl"><input type="hidden" name="__key" value="ROLE_REPORT_ORDER_PEN_PROC">Pending/Processing Orders:</div>
					  	 <div class="listp">
					  	 <!-- input field -->
					     	 <input type="checkbox" name="ROLE_REPORT_ORDER_PEN_PROC" value ="ROLE_REPORT_ORDER_PEN_PROC" <c:if test="${accessPrivilegeForm.privilege.roleReportOrderPenProc}">checked</c:if>  <c:if test="${accessPrivilegeForm.privilege.user.groupIds != ''}">disabled="disabled"</c:if>>
					   	 <!-- end input field -->	
						 </div>
					  	 </div>	
					  	 <div class="list${classIndex % 2}"><c:set var="classIndex" value="${classIndex+1}" />
					  	 <div class="listfl"><input type="hidden" name="__key" value="ROLE_REPORT_SHIPPED_DAILY">Shipped Daily:</div>
					  	 <div class="listp">
					  	 <!-- input field -->
					     	 <input type="checkbox" name="ROLE_REPORT_SHIPPED_DAILY" value ="ROLE_REPORT_SHIPPED_DAILY" <c:if test="${accessPrivilegeForm.privilege.roleReportShippedDaily}">checked</c:if>  <c:if test="${accessPrivilegeForm.privilege.user.groupIds != ''}">disabled="disabled"</c:if>>
					   	 <!-- end input field -->	
						 </div>
					  	 </div>
					  	 <div class="list${classIndex % 2}"><c:set var="classIndex" value="${classIndex+1}" />
					  	 <div class="listfl"><input type="hidden" name="__key" value="ROLE_REPORT_AFFILIATE_COMMISSION">Affiliate Commission:</div>
					  	 <div class="listp">
					  	 <!-- input field -->
					     	 <input type="checkbox" name="ROLE_REPORT_AFFILIATE_COMMISSION" value ="ROLE_REPORT_AFFILIATE_COMMISSION" <c:if test="${accessPrivilegeForm.privilege.roleReportAffiliateCommission}">checked</c:if>  <c:if test="${accessPrivilegeForm.privilege.user.groupIds != ''}">disabled="disabled"</c:if>>
					   	 <!-- end input field -->	
						 </div>
					  	 </div>
					  	 <div class="list${classIndex % 2}"><c:set var="classIndex" value="${classIndex+1}" />
					  	 <div class="listfl"><input type="hidden" name="__key" value="ROLE_REPORT_PRODUCT">Product:</div>
					  	 <div class="listp">
					  	 <!-- input field -->
					     	 <input type="checkbox" name="ROLE_REPORT_PRODUCT" value ="ROLE_REPORT_PRODUCT" <c:if test="${accessPrivilegeForm.privilege.roleReportProduct}">checked</c:if>  <c:if test="${accessPrivilegeForm.privilege.user.groupIds != ''}">disabled="disabled"</c:if>>
					   	 <!-- end input field -->	
						 </div>
					  	 </div>
					  	 <div class="list${classIndex % 2}"><c:set var="classIndex" value="${classIndex+1}" />
					  	 <div class="listfl"><input type="hidden" name="__key" value="ROLE_REPORT_INVENTORY">Inventory:</div>
					  	 <div class="listp">
					  	 <!-- input field -->
					     	 <input type="checkbox" name="ROLE_REPORT_INVENTORY" value ="ROLE_REPORT_INVENTORY" <c:if test="${accessPrivilegeForm.privilege.roleReportInventory}">checked</c:if>  <c:if test="${accessPrivilegeForm.privilege.user.groupIds != ''}">disabled="disabled"</c:if>>
					   	 <!-- end input field -->	
						 </div>
					  	 </div>
					  	 <div class="list${classIndex % 2}"><c:set var="classIndex" value="${classIndex+1}" />
					  	 <div class="listfl"><input type="hidden" name="__key" value="ROLE_REPORT_ABANDONED_SHOPPING_CART">Abandoned Shopping Cart:</div>
					  	 <div class="listp">
					  	 <!-- input field -->
					     	 <input type="checkbox" name="ROLE_REPORT_ABANDONED_SHOPPING_CART" value ="ROLE_REPORT_ABANDONED_SHOPPING_CART" <c:if test="${accessPrivilegeForm.privilege.roleReportAbandonedShoppingCart}">checked</c:if>  <c:if test="${accessPrivilegeForm.privilege.user.groupIds != ''}">disabled="disabled"</c:if>>
					   	 <!-- end input field -->	
						 </div>
					  	 </div>
					  	 <div class="list${classIndex % 2}"><c:set var="classIndex" value="${classIndex+1}" />
					  	 <div class="listfl"><input type="hidden" name="__key" value="ROLE_REPORT_LAY">LAY Report:</div>
					  	 <div class="listp">
					  	 <!-- input field -->
					     	 <input type="checkbox" name="ROLE_REPORT_LAY" value ="ROLE_REPORT_LAY" <c:if test="${accessPrivilegeForm.privilege.roleReportLAY}">checked</c:if>  <c:if test="${accessPrivilegeForm.privilege.user.groupIds != ''}">disabled="disabled"</c:if>>
					   	 <!-- end input field -->	
						 </div>
					  	 </div>
					  	 <div class="list${classIndex % 2}"><c:set var="classIndex" value="${classIndex+1}" />
					  	 <div class="listfl"><input type="hidden" name="__key" value="ROLE_REPORT_EVENT_MEMBER_LIST">Event Member List:</div>
					  	 <div class="listp">
					  	 <!-- input field -->
					     	 <input type="checkbox" name="ROLE_REPORT_EVENT_MEMBER_LIST" value ="ROLE_REPORT_EVENT_MEMBER_LIST" <c:if test="${accessPrivilegeForm.privilege.roleReportEventMemberList}">checked</c:if>  <c:if test="${accessPrivilegeForm.privilege.user.groupIds != ''}">disabled="disabled"</c:if>>
					   	 <!-- end input field -->	
						 </div>
					  	 </div>
				    </div>           
					</td>
					</tr>   
					<tr valign="top">
				    <td width="50%" >
				    <div class="stretchtoggle">
					    <div style="">
						  <span class="titlePrivilege"><fmt:message key="event" /></span>
						</div>	
				    </div>
				    <div class="stretcher">
						<div  class="checkAll">
					       <a style="text-decoration:none;color:red;font-weight:700" href="javascript:;" onclick="SetChecked('EVENT',5,10);return false;">All</a>&nbsp;/&nbsp;
					       <a style="text-decoration:none;color:red;font-weight:700" href="javascript:;" onclick="UnSetChecked('EVENT',5,10);return false;">None</a>
					     </div>
					  	 <div class="list${classIndex % 2}"><c:set var="classIndex" value="${classIndex+1}" />
					  	 <div class="listfl"><input type="hidden" name="__key" value="ROLE_EVENT_CREATE">Create Event:</div>
					  	 <div class="listp">
					  	 <!-- input field -->
					     	 <input type="checkbox" name="ROLE_EVENT_CREATE" value ="ROLE_EVENT_CREATE" <c:if test="${accessPrivilegeForm.privilege.roleEventCreate}">checked</c:if>  <c:if test="${accessPrivilegeForm.privilege.user.groupIds != ''}">disabled="disabled"</c:if>>
					   	 <!-- end input field -->	
						 </div>
					  	 </div>	
					  	 <div class="list${classIndex % 2}"><c:set var="classIndex" value="${classIndex+1}" />
					  	 <div class="listfl"><input type="hidden" name="__key" value="ROLE_EVENT_UPDATE">Update Event:</div>
					  	 <div class="listp">
					  	 <!-- input field -->
					     	 <input type="checkbox" name="ROLE_EVENT_UPDATE" value ="ROLE_EVENT_UPDATE" <c:if test="${accessPrivilegeForm.privilege.roleEventUpdate}">checked</c:if>  <c:if test="${accessPrivilegeForm.privilege.user.groupIds != ''}">disabled="disabled"</c:if>>
					   	 <!-- end input field -->	
						 </div>
					  	 </div>	
					  	 <div class="list${classIndex % 2}"><c:set var="classIndex" value="${classIndex+1}" />
					  	 <div class="listfl"><input type="hidden" name="__key" value="ROLE_EVENT_DELETE">Event Delete:</div>
					  	 <div class="listp">
					  	 <!-- input field -->
					     	 <input type="checkbox" name="ROLE_EVENT_DELETE" value ="ROLE_EVENT_DELETE" <c:if test="${accessPrivilegeForm.privilege.roleEventDelete}">checked</c:if>  <c:if test="${accessPrivilegeForm.privilege.user.groupIds != ''}">disabled="disabled"</c:if>>
					   	 <!-- end input field -->	
						 </div>
					  	 </div>
					  	 <div class="list${classIndex % 2}"><c:set var="classIndex" value="${classIndex+1}" />
					  	 <div class="listfl"><input type="hidden" name="__key" value="ROLE_EVENT_VIEW_LIST">Event View List:</div>
					  	 <div class="listp">
					  	 <!-- input field -->
					     	 <input type="checkbox" name="ROLE_EVENT_VIEW_LIST" value ="ROLE_EVENT_VIEW_LIST" <c:if test="${accessPrivilegeForm.privilege.roleEventViewList}">checked</c:if>  <c:if test="${accessPrivilegeForm.privilege.user.groupIds != ''}">disabled="disabled"</c:if>>
					   	 <!-- end input field -->	
						 </div>
					  	 </div>
					  	 <div class="list${classIndex % 2}"><c:set var="classIndex" value="${classIndex+1}" />
					  	 <div class="listfl"><input type="hidden" name="__key" value="ROLE_EVENT_MEMBER_VIEW_LIST">Event Member View List:</div>
					  	 <div class="listp">
					  	 <!-- input field -->
					     	 <input type="checkbox" name="ROLE_EVENT_MEMBER_VIEW_LIST" value ="ROLE_EVENT_MEMBER_VIEW_LIST" <c:if test="${accessPrivilegeForm.privilege.roleEventMemberViewList}">checked</c:if>  <c:if test="${accessPrivilegeForm.privilege.user.groupIds != ''}">disabled="disabled"</c:if>>
					   	 <!-- end input field -->	
						 </div>
					  	 </div>	
				    </div>
				    </td>
					<td width="50%" >
					<div class="stretchtoggle">
					    <div style="">
						  <span class="titlePrivilege"><fmt:message key="emailManager" /></span>
						</div>	
				    </div>
				    <div class="stretcher">
						<div  class="checkAll">
					       <a style="text-decoration:none;color:red;font-weight:700" href="javascript:;" onclick="SetChecked('EMAILMANAGER',5,17);return false;">All</a>&nbsp;/&nbsp;
					       <a style="text-decoration:none;color:red;font-weight:700" href="javascript:;" onclick="UnSetChecked('EMAILMANAGER',5,17);return false;">None</a>
					     </div>
					  	 <div class="list${classIndex % 2}"><c:set var="classIndex" value="${classIndex+1}" />
					  	 <div class="listfl"><input type="hidden" name="__key" value="ROLE_EMAILMANAGER_CREATE">Create Mass Email:</div>
					  	 <div class="listp">
					  	 <!-- input field -->
					     	 <input type="checkbox" name="ROLE_EMAILMANAGER_CREATE" value ="ROLE_EMAILMANAGER_CREATE" <c:if test="${accessPrivilegeForm.privilege.roleEmailManagerCreate}">checked</c:if>  <c:if test="${accessPrivilegeForm.privilege.user.groupIds != ''}">disabled="disabled"</c:if>>
					   	 <!-- end input field -->	
						 </div>
					  	 </div>	
					  	 <div class="list${classIndex % 2}"><c:set var="classIndex" value="${classIndex+1}" />
					  	 <div class="listfl"><input type="hidden" name="__key" value="ROLE_EMAILMANAGER_VIEW_HISTORY">View History:</div>
					  	 <div class="listp">
					  	 <!-- input field -->
					     	 <input type="checkbox" name="ROLE_EMAILMANAGER_VIEW_HISTORY" value ="ROLE_EMAILMANAGER_VIEW_HISTORY" <c:if test="${accessPrivilegeForm.privilege.roleEmailManagerViewHistory}">checked</c:if>  <c:if test="${accessPrivilegeForm.privilege.user.groupIds != ''}">disabled="disabled"</c:if>>
					   	 <!-- end input field -->	
						 </div>
					  	 </div>	
					  	 <div class="list${classIndex % 2}"><c:set var="classIndex" value="${classIndex+1}" />
					  	 <div class="listfl"><input type="hidden" name="__key" value="ROLE_EMAILMANAGER_BUY_CREDIT">Buy Credit:</div>
					  	 <div class="listp">
					  	 <!-- input field -->
					     	 <input type="checkbox" name="ROLE_EMAILMANAGER_BUY_CREDIT" value ="ROLE_EMAILMANAGER_BUY_CREDIT" <c:if test="${accessPrivilegeForm.privilege.roleEmailManagerBuyCredit}">checked</c:if>  <c:if test="${accessPrivilegeForm.privilege.user.groupIds != ''}">disabled="disabled"</c:if>>
					   	 <!-- end input field -->	
						 </div>
					  	 </div>	
				    </div>           
					</td>
					</tr>
				    <tr valign="top">
				    <td width="50%" >
				    <div class="stretchtoggle">
					    <div style="">
						  <span class="titlePrivilege"><fmt:message key="crm" /></span>
						</div>	
				    </div>
				    <div class="stretcher">
				        <div  class="checkAll">
					       <a style="text-decoration:none;color:red;font-weight:700" href="javascript:;" onclick="SetChecked('CRM',5,8);return false;">All</a>&nbsp;/&nbsp;
					       <a style="text-decoration:none;color:red;font-weight:700" href="javascript:;" onclick="UnSetChecked('CRM',5,8);return false;">None</a>
					     </div>
					  	 <div class="list${classIndex % 2}"><c:set var="classIndex" value="${classIndex+1}" />
					  	 <div class="listfl"><input type="hidden" name="__key" value="ROLE_CRM">CRM:</div>
					  	 <div class="listp">
					  	 <!-- input field -->
					     	 <input type="checkbox" name="ROLE_CRM" value ="ROLE_CRM" <c:if test="${accessPrivilegeForm.privilege.roleCRM}">checked</c:if>  <c:if test="${accessPrivilegeForm.privilege.user.groupIds != ''}">disabled="disabled"</c:if>>
					   	 <!-- end input field -->	
						 </div>
					  	 </div>	
					  	 <div class="list${classIndex % 2}"><c:set var="classIndex" value="${classIndex+1}" />
					  	 <div class="listfl"><input type="hidden" name="__key" value="ROLE_CRM_FIELD">CRM Field:</div>
					  	 <div class="listp">
					  	 <!-- input field -->
					     	 <input type="checkbox" name="ROLE_CRM_FIELD" value ="ROLE_CRM_FIELD" <c:if test="${accessPrivilegeForm.privilege.roleCRMField}">checked</c:if>  <c:if test="${accessPrivilegeForm.privilege.user.groupIds != ''}">disabled="disabled"</c:if>>
					   	 <!-- end input field -->	
						 </div>
					  	 </div>	
					  	 <div class="list${classIndex % 2}"><c:set var="classIndex" value="${classIndex+1}" />
					  	 <div class="listfl"><input type="hidden" name="__key" value="ROLE_CRM_GROUP">CRM Group:</div>
					  	 <div class="listp">
					  	 <!-- input field -->
					     	 <input type="checkbox" name="ROLE_CRM_GROUP" value ="ROLE_CRM_GROUP" <c:if test="${accessPrivilegeForm.privilege.roleCRMGroup}">checked</c:if>  <c:if test="${accessPrivilegeForm.privilege.user.groupIds != ''}">disabled="disabled"</c:if>>
					   	 <!-- end input field -->	
						 </div>
					  	 </div>	
					  	 <div class="list${classIndex % 2}"><c:set var="classIndex" value="${classIndex+1}" />
					  	 <div class="listfl"><input type="hidden" name="__key" value="ROLE_CRM_FORM">CRM Form:</div>
					  	 <div class="listp">
					  	 <!-- input field -->
					     	 <input type="checkbox" name="ROLE_CRM_FORM" value ="ROLE_CRM_FORM" <c:if test="${accessPrivilegeForm.privilege.roleCRMForm}">checked</c:if>  <c:if test="${accessPrivilegeForm.privilege.user.groupIds != ''}">disabled="disabled"</c:if>>
					   	 <!-- end input field -->	
						 </div>
					  	 </div>
					  	 <div class="list${classIndex % 2}"><c:set var="classIndex" value="${classIndex+1}" />
					  	 <div class="listfl"><input type="hidden" name="__key" value="ROLE_CRM_IMPORT_EXPORT">IMPORT/EXPORT:</div>
					  	 <div class="listp">
					  	 <!-- input field -->
					     	 <input type="checkbox" name="ROLE_CRM_IMPORT_EXPORT" value ="ROLE_CRM_IMPORT_EXPORT" <c:if test="${accessPrivilegeForm.privilege.roleCRMImportExport}">checked</c:if>  <c:if test="${accessPrivilegeForm.privilege.user.groupIds != ''}">disabled="disabled"</c:if>>
					   	 <!-- end input field -->	
						 </div>
					  	 </div>	
					</div>
				    </td>
					<td width="50%" >
					<div class="stretchtoggle">
					    <div style="">
						  <span class="titlePrivilege"><fmt:message key="configTabTitle" /></span>
						</div>	
				    </div>
				    <div class="stretcher">
						<div  class="checkAll">
					       <a style="text-decoration:none;color:red;font-weight:700" href="javascript:;" onclick="SetChecked('SITE_INFO',5,14);return false;">All</a>&nbsp;/&nbsp;
					       <a style="text-decoration:none;color:red;font-weight:700" href="javascript:;" onclick="UnSetChecked('SITE_INFO',5,14);return false;">None</a>
					     </div>
					  	 <div class="list${classIndex % 2}"><c:set var="classIndex" value="${classIndex+1}" />
					  	 <div class="listfl"><input type="hidden" name="__key" value="ROLE_SITE_INFO">Site Info:</div>
					  	 <div class="listp">
					  	 <!-- input field -->
					     	 <input type="checkbox" name="ROLE_SITE_INFO" value ="ROLE_SITE_INFO" <c:if test="${accessPrivilegeForm.privilege.roleSiteInfo}">checked</c:if>  <c:if test="${accessPrivilegeForm.privilege.user.groupIds != ''}">disabled="disabled"</c:if>>
					   	 <!-- end input field -->	
						 </div>
					  	 </div>	
				    </div>           
					</td>
					</tr>
					<tr valign="top">
				    <td width="50%" >
				    <div class="stretchtoggle">
					    <div style="">
						  <span class="titlePrivilege"><fmt:message key="dataFeed" /></span>
						</div>	
				    </div>
				    <div class="stretcher">
				        <div  class="checkAll">
					       <a style="text-decoration:none;color:red;font-weight:700" href="javascript:;" onclick="SetChecked('DATA_FEED',5,14);return false;">All</a>&nbsp;/&nbsp;
					       <a style="text-decoration:none;color:red;font-weight:700" href="javascript:;" onclick="UnSetChecked('DATA_FEED',5,14);return false;">None</a>
					     </div>
					     <div class="list${classIndex % 2}"><c:set var="classIndex" value="${classIndex+1}" />
					  	 <div class="listfl"><input type="hidden" name="__key" value="ROLE_DATA_FEED">Generate Data Feed:</div>
					  	 <div class="listp">
					  	 <!-- input field -->
					     	 <input type="checkbox" name="ROLE_DATA_FEED" value ="ROLE_DATA_FEED" <c:if test="${accessPrivilegeForm.privilege.roleDataFeed}">checked</c:if>  <c:if test="${accessPrivilegeForm.privilege.user.groupIds != ''}">disabled="disabled"</c:if>>
					   	 <!-- end input field -->	
						 </div>
					  	 </div>
				    </div>
				    </td>
					<td width="50%" >
				    <div class="stretchtoggle">
					    <div style="">
						  <span class="titlePrivilege"><fmt:message key="truckLoadProcess" /></span>
						</div>	
				    </div>
				    <div class="stretcher">
				        <div  class="checkAll">
					       <a style="text-decoration:none;color:red;font-weight:700" href="javascript:;" onclick="SetChecked('PROCESSING',5,15);return false;">All</a>&nbsp;/&nbsp;
					       <a style="text-decoration:none;color:red;font-weight:700" href="javascript:;" onclick="UnSetChecked('PROCESSING',5,15);return false;">None</a>
					     </div>
					      <div class="list${classIndex % 2}"><c:set var="classIndex" value="${classIndex+1}" />
					  	 <div class="listfl"><input type="hidden" name="__key" value="ROLE_PROCESSING_VIEW_LIST">View List:</div>
					  	 <div class="listp">
					  	 <!-- input field -->
					     	 <input type="checkbox" name="ROLE_PROCESSING_VIEW_LIST" value ="ROLE_PROCESSING_VIEW_LIST" <c:if test="${accessPrivilegeForm.privilege.roleProcessingViewList}">checked</c:if>  <c:if test="${accessPrivilegeForm.privilege.user.groupIds != ''}">disabled="disabled"</c:if>>
					   	 <!-- end input field -->	
						 </div>
					  	 </div>
					  	 <div class="list${classIndex % 2}"><c:set var="classIndex" value="${classIndex+1}" />
					  	 <div class="listfl"><input type="hidden" name="__key" value="ROLE_PROCESSING_CREATE">Create:</div>
					  	 <div class="listp">
					  	 <!-- input field -->
					     	 <input type="checkbox" name="ROLE_PROCESSING_CREATE" value ="ROLE_PROCESSING_CREATE" <c:if test="${accessPrivilegeForm.privilege.roleProcessingCreate}">checked</c:if>  <c:if test="${accessPrivilegeForm.privilege.user.groupIds != ''}">disabled="disabled"</c:if>>
					   	 <!-- end input field -->	
						 </div>
					  	 </div>
					  	 <div class="list${classIndex % 2}"><c:set var="classIndex" value="${classIndex+1}" />
					  	 <div class="listfl"><input type="hidden" name="__key" value="ROLE_PROCESSING_UPDATE">Update:</div>
					  	 <div class="listp">
					  	 <!-- input field -->
					     	 <input type="checkbox" name="ROLE_PROCESSING_UPDATE" value ="ROLE_PROCESSING_UPDATE" <c:if test="${accessPrivilegeForm.privilege.roleProcessingUpdate}">checked</c:if>  <c:if test="${accessPrivilegeForm.privilege.user.groupIds != ''}">disabled="disabled"</c:if>>
					   	 <!-- end input field -->	
						 </div>
					  	 </div>
					  	 <div class="list${classIndex % 2}"><c:set var="classIndex" value="${classIndex+1}" />
					  	 <div class="listfl"><input type="hidden" name="__key" value="ROLE_PROCESSING_DELETE">Cancel:</div>
					  	 <div class="listp">
					  	 <!-- input field -->
					     	 <input type="checkbox" name="ROLE_PROCESSING_DELETE" value ="ROLE_PROCESSING_DELETE" <c:if test="${accessPrivilegeForm.privilege.roleProcessingDelete}">checked</c:if>  <c:if test="${accessPrivilegeForm.privilege.user.groupIds != ''}">disabled="disabled"</c:if>>
					   	 <!-- end input field -->	
						 </div>
					  	 </div>
				    </div>
				    </td>
					</tr>
					<tr>
						<td width="50%" >
					    <div class="stretchtoggle">
						    <div style="">
							  <span class="titlePrivilege"><fmt:message key="storeLocator" /></span>
							</div>	
					    </div>
					    <div class="stretcher">
					        <div  class="checkAll">
						       <a style="text-decoration:none;color:red;font-weight:700" href="javascript:;" onclick="SetChecked('STORE_LOCATER',5,18);return false;">All</a>&nbsp;/&nbsp;
						       <a style="text-decoration:none;color:red;font-weight:700" href="javascript:;" onclick="UnSetChecked('STORE_LOCATER',5,18);return false;">None</a>
						     </div>
						      <div class="list${classIndex % 2}"><c:set var="classIndex" value="${classIndex+1}" />
						  	 <div class="listfl"><input type="hidden" name="__key" value="ROLE_STORE_LOCATER_VIEW_LIST">View List:</div>
						  	 <div class="listp">
						  	 <!-- input field -->
						     	 <input type="checkbox" name="ROLE_STORE_LOCATER_VIEW_LIST" value ="ROLE_STORE_LOCATER_VIEW_LIST" <c:if test="${accessPrivilegeForm.privilege.roleStoreLocatorViewList}">checked</c:if>  <c:if test="${accessPrivilegeForm.privilege.user.groupIds != ''}">disabled="disabled"</c:if>>
						   	 <!-- end input field -->	
							 </div>
						  	 </div>
						  	 <div class="list${classIndex % 2}"><c:set var="classIndex" value="${classIndex+1}" />
						  	 <div class="listfl"><input type="hidden" name="__key" value="ROLE_STORE_LOCATER_CREATE">Create:</div>
						  	 <div class="listp">
						  	 <!-- input field -->
						     	 <input type="checkbox" name="ROLE_STORE_LOCATER_CREATE" value ="ROLE_STORE_LOCATER_CREATE" <c:if test="${accessPrivilegeForm.privilege.roleStoreLocatorCreate}">checked</c:if>  <c:if test="${accessPrivilegeForm.privilege.user.groupIds != ''}">disabled="disabled"</c:if>>
						   	 <!-- end input field -->	
							 </div>
						  	 </div>
						  	 <div class="list${classIndex % 2}"><c:set var="classIndex" value="${classIndex+1}" />
						  	 <div class="listfl"><input type="hidden" name="__key" value="ROLE_STORE_LOCATER_IMPORT_EXPORT">Import/Export:</div>
						  	 <div class="listp">
						  	 <!-- input field -->
						     	 <input type="checkbox" name="ROLE_STORE_LOCATER_IMPORT_EXPORT" value ="ROLE_STORE_LOCATER_IMPORT_EXPORT" <c:if test="${accessPrivilegeForm.privilege.roleStoreLocatorImportExport}">checked</c:if>  <c:if test="${accessPrivilegeForm.privilege.user.groupIds != ''}">disabled="disabled"</c:if>>
						   	 <!-- end input field -->	
							 </div>
						  	 </div>
						  	  <div class="list${classIndex % 2}"><c:set var="classIndex" value="${classIndex+1}" />
						  	 <div class="listfl"><input type="hidden" name="__key" value="ROLE_STORE_LOCATER_UPDATE">Update:</div>
						  	 <div class="listp">
						  	 <!-- input field -->
						     	 <input type="checkbox" name="ROLE_STORE_LOCATER_UPDATE" value ="ROLE_STORE_LOCATER_UPDATE" <c:if test="${accessPrivilegeForm.privilege.roleStoreLocatorUpdate}">checked</c:if>  <c:if test="${accessPrivilegeForm.privilege.user.groupIds != ''}">disabled="disabled"</c:if>>
						   	 <!-- end input field -->	
							 </div>
						  	 </div>
						  	 <div class="list${classIndex % 2}"><c:set var="classIndex" value="${classIndex+1}" />
						  	 <div class="listfl"><input type="hidden" name="__key" value="ROLE_STORE_LOCATER_DELETE">Delete:</div>
						  	 <div class="listp">
						  	 <!-- input field -->
						     	 <input type="checkbox" name="ROLE_STORE_LOCATER_DELETE" value ="ROLE_STORE_LOCATER_DELETE" <c:if test="${accessPrivilegeForm.privilege.roleStoreLocatorDelete}">checked</c:if>  <c:if test="${accessPrivilegeForm.privilege.user.groupIds != ''}">disabled="disabled"</c:if>>
						   	 <!-- end input field -->	
							 </div>
						  	 </div>
					    </div>
					    </td>
					    <td width="50%" >
							<div class="stretchtoggle">
							    <div style="">
							    	<span class="titlePrivilege"><fmt:message key="echosign" /></span>
								</div>	
						    </div>
						    <div class="stretcher">
						  	 <div class="list${classIndex % 2}"><c:set var="classIndex" value="${classIndex+1}" />
						  	 <div class="listfl"><input type="hidden" name="__key" value="ROLE_ECHOSIGN_VIEW">EchoSign:</div>
						  	 <div class="listp">
						  	 <!-- input field -->
						     	 <input type="checkbox" name="ROLE_ECHOSIGN_VIEW" value ="ROLE_ECHOSIGN_VIEW" <c:if test="${accessPrivilegeForm.privilege.roleEchoSignView}">checked</c:if>  <c:if test="${accessPrivilegeForm.privilege.user.groupIds != ''}">disabled="disabled"</c:if>>
						   	 <!-- end input field -->	
							 </div>
						  	 </div>	
						    </div>           
						</td>
					</tr>
					<tr valign="top">
				    <td width="50%" >
				    <div class="stretchtoggle">
					    <div style="">
						  <span class="titlePrivilege"><fmt:message key="quickExport" /></span>
						</div>	
				    </div>
				    <div class="stretcher">
				        <div  class="checkAll">
					       <a style="text-decoration:none;color:red;font-weight:700" href="javascript:;" onclick="SetChecked('QUICK_EXPORT',5,17);return false;">All</a>&nbsp;/&nbsp;
					       <a style="text-decoration:none;color:red;font-weight:700" href="javascript:;" onclick="UnSetChecked('QUICK_EXPORT',5,17);return false;">None</a>
					     </div>
					     <div class="list${classIndex % 2}"><c:set var="classIndex" value="${classIndex+1}" />
					  	 <div class="listfl"><input type="hidden" name="__key" value="ROLE_QUICK_EXPORT"><fmt:message key="quickExport" />:</div>
					  	 <div class="listp">
					  	 <!-- input field -->
					     	 <input type="checkbox" name="ROLE_QUICK_EXPORT" value ="ROLE_QUICK_EXPORT" <c:if test="${accessPrivilegeForm.privilege.roleQuickExport}">checked</c:if>  <c:if test="${accessPrivilegeForm.privilege.user.groupIds != ''}">disabled="disabled"</c:if>>
					   	 <!-- end input field -->	
						 </div>
					  	 </div>
				    </div>
				    </td>
				    </tr>
					</table> 
			</div>    
			<!-- end input field -->	
	 		</div>
		  	</div>
		  	

	<!-- end tab -->		
	</div>
<!-- end tabs -->			
</div>
<!-- start button -->
	<div align="left" class="button">
	  <c:if test="${accessPrivilegeForm.newPrivilege}">
        <input type="submit" value="<fmt:message key="add" />">
      </c:if>
      <c:if test="${!accessPrivilegeForm.newPrivilege}">
        <input type="submit" value="<fmt:message key="update" />">
        <input type="submit" value="<fmt:message key="delete" />" name="_delete" onClick="return confirm('Delete permanently?')">
      </c:if>
	</div>
<!-- end button -->	
  
  <!-- end table -->
    </td><td class="boxmidr" ></td></tr>
  <tr><td class="botl"></td><td class="botr"></td></tr>
  </table>
  
<!-- end main box -->  
</div>
</form:form>

<script language="JavaScript">
<!--
function SetChecked(boxname,start,end) {
	f=document.myform;
	for(i=0 ; i<f.elements.length ; i++) {
		if (f.elements[i].name.substring(start,end)==boxname) {
		f.elements[i].checked=true;
		}
	}
}
function UnSetChecked(boxname,start,end) {
	f=document.myform;

	for( i=0 ; i<f.elements.length ; i++) {
		if (f.elements[i].name.substring(start,end)==boxname) {
		f.elements[i].checked=false;
		}
	}
}
//-->
</script>

</sec:authorize> 
  </tiles:putAttribute>    
</tiles:insertDefinition>