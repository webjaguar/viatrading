<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/security/tags" prefix="sec" %>

  <div id="lbox">
   <table cellpadding="0" cellspacing="0" border="0" class="module" style="width: 100%"><tr><td class="topl">
    </td><td class="topr" ></td></tr><tr><td class="boxmid" >
  
    <h2 class="menuleft mfirst"><fmt:message key="accessPrivilege"/></h2>
    <div class="menudiv"></div> 
    <a href="privilegeList.jhtm" class="userbutton"><fmt:message key="users"/></a>
    <c:if test="${siteConfig['ACCESS_PRIVILEGE_GROUP'].value == 'true'}">
    <a href="privilegeGroupList.jhtm" class="userbutton"><fmt:message key="groups"/></a>
    </c:if>
    <a href="privilegeAuditList.jhtm" class="userbutton"><fmt:message key="audit"/></a>
    <div class="menudiv"></div>

    </td><td class="boxmidr" ></td></tr>
    <tr><td class="botl"></td><td class="botr"></td></tr></table>
  </div>