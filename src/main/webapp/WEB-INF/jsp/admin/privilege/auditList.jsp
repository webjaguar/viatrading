<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/security/tags" prefix="sec" %>


<tiles:insertDefinition name="admin.privilege.audit" flush="true">
  <tiles:putAttribute name="content" type="string">
  
<sec:authorize ifAnyGranted="ROLE_AEM,ROLE_ADMIN_${prop_site_id}"> 
<script language="JavaScript">
<!--
function toggleAll(el) {
	var ids = document.getElementsByName("__selected_id");	
  	if (ids.length == 1)
      document.list_form.__selected_id.checked = el.checked;
    else
      for (i = 0; i < ids.length; i++)
        document.list_form.__selected_id[i].checked = el.checked;	
}  
function deleteSelected() {
	var ids = document.getElementsByName("__selected_id");	
  	if (ids.length == 1) {
      if (document.list_form.__selected_id.checked) {
    	return confirm('Delete permanently?');
      }
    } else {
      for (i = 0; i < ids.length; i++) {
        if (document.list_form.__selected_id[i].checked) {
    	  return confirm('Delete permanently?');
        }
      }
    }

    alert("Please select audit trial to delete.");       
    return false;
}
//-->
</script>
<form action="privilegeAuditList.jhtm" method="get" name="list_form">
<input type="hidden" name="username" value="${model.username}" />
<!-- main box -->
  <div id="mboxfull">
  
  <!-- start table -->
  <table cellpadding="0" cellspacing="0" border="0" class="module" >
  <tr>
    <td class="topl_g">
	  
	  <!-- breadcrumb -->
	  <p class="breadcrumb">
	    <a href="../privilege">Privilege</a> &gt;
	    <a href="../privilege">users</a> 
	    <c:if test="${!empty model.username}" >
	      &gt;
	      <c:out value="${model.username}" />
	    </c:if>
	  </p>
	  
	  <!-- Error Message -->
	  
	  <!-- header image -->
	  <img class="headerImage" src="../graphics/Privilege.gif" />
	  
	  </td><td class="topr_g" ></td></tr>
  <tr><td class="boxmidlrg" >

 <!-- tabs -->
 <div class="tab-wrapper">
	<h4 title="List of affiliates"></h4>
	<div>
	<!-- start tab -->
	<p>
	  	<div class="listdivi ln tabdivi"></div>
	  	<div class="listdivi"></div>
	   
		  	<div class="listlight">
		  	<!-- input field --> 
		  	<table border="0" cellpadding="0" cellspacing="1" width="100%">
			  <tr>
			  <c:if test="${model.users.nrOfElements > 0}">
			  <td class="pageShowing">
			  <fmt:message key="showing">
				<fmt:param value="${model.users.firstElementOnPage + 1}"/>
				<fmt:param value="${model.users.lastElementOnPage + 1}"/>
				<fmt:param value="${model.users.nrOfElements}"/>
			  </fmt:message>
			  </td>
			  </c:if>
			  <td class="pageNavi">
			  Page 
			  <select name="page" id="page" onchange="submit()">
			    <c:forEach begin="1" end="${model.users.pageCount}" var="page">
			  	<option value="${page}" <c:if test="${page == (model.users.page+1)}">selected</c:if>>${page}</option>
			    </c:forEach>
			  </select>
			  of <c:out value="${model.users.pageCount}"/>
			  | 
			  <c:if test="${model.users.firstPage}"><span class="pageNaviDead">previous</span></c:if>
			  <c:if test="${not model.users.firstPage}"><a href="<c:url value="privilegeAuditList.jhtm"><c:param name="page" value="${model.users.page}"/><c:param name="username" value="${model.username}"/></c:url>" class="pageNaviLink">previous</a></c:if>
			  | 
			  <c:if test="${model.users.lastPage}"><span class="pageNaviDead">next</span></c:if>
			  <c:if test="${not model.users.lastPage}"><a href="<c:url value="privilegeAuditList.jhtm"><c:param name="page" value="${model.users.page+2}"/><c:param name="username" value="${model.username}"/></c:url>" class="pageNaviLink">next</a></c:if>
			  </td>
			  </tr>
			</table>
			
			<table border="0" cellpadding="0" cellspacing="0" width="100%" class="listings">
			  <tr class="listingsHdr2">
			  <c:if test="${model.users.nrOfElements > 0}">
			    <td align="center"><input type="checkbox" onclick="toggleAll(this)"></td>
			  </c:if>
			    <td class="indexCol">&nbsp;</td>
			    <td class="listingsHdr3"><fmt:message key="username" /></td>
			    <td class="listingsHdr3"><fmt:message key="page" /> <fmt:message key="visited" /></td>
			    <td class="listingsHdr3"><fmt:message key="login" /></td>
			    <td class="listingsHdr3"><fmt:message key="time" /></td>
			  </tr>
			<c:forEach items="${model.users.pageList}" var="user" varStatus="status">
			  <tr class="row${status.index % 2}" onmouseover="changeStyleClass(this,'rowOver')" onmouseout="changeStyleClass(this,'row${status.index % 2}')">
			  <c:if test="${model.users.nrOfElements > 0}">
			    <td align="center"><input name="__selected_id" value="${user.id}" type="checkbox"></td>
			  </c:if>
			    <td class="indexCol"><c:out value="${status.count + model.users.firstElementOnPage}"/>.</td>
			    <td class="nameCol"><c:out value="${user.username}"/></td>
			    <td class="nameCol"><c:out value="${user.pageSeen}"/></td>
			    <td class="nameCol"><fmt:formatDate type="both" timeStyle="full" value="${user.pageVisitDate}"/></td>
			    <td class="nameCol"><c:out value="${user.pageWorkTime}"/></td>
			  </tr>
			</c:forEach>
			<c:if test="${model.users.nrOfElements == 0}">
			  <tr class="emptyList"><td colspan="3">&nbsp;</td></tr>
			</c:if>
			</table>
			
			<table border="0" cellpadding="0" cellspacing="1" width="100%">
			  <tr> 
			  <td class="pageSize">
			    <select name="size" onchange="document.getElementById('page').value=1;submit()">
			    <c:forTokens items="10,25,50,100,500" delims="," var="current">
			  	  <option value="${current}" <c:if test="${current == model.users.pageSize}">selected</c:if>>${current} <fmt:message key="perPage" /></option>
			    </c:forTokens>
			    </select>
			  </td>
			  </tr>
			</table>
		  	<!-- end input field -->  	  
		  	
		  	<!-- start button --> 
		  	<div align="left" class="button">
		  	  <input type="submit" name="__delete" value="Delete Selected Audit" onClick="return deleteSelected()">
			</div>
			<!-- end button -->	
			</div>
	
	</p>		 	
	<!-- end tab -->        
	</div>
<!-- end tabs -->			
</div>
  
  <!-- end table -->
   </td><td class="boxmidr" ></td></tr>
  <tr><td class="botl"></td><td class="botr"></td></tr>
  </table>
  
<!-- end main box -->  
</div> 
</form> 
</sec:authorize>

  </tiles:putAttribute>    
</tiles:insertDefinition>