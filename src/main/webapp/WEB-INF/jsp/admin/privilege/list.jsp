<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ taglib uri="http://www.springframework.org/security/tags" prefix="sec" %>

<tiles:insertDefinition name="admin.privilege" flush="true">
  <tiles:putAttribute name="content" type="string">
  
<sec:authorize ifAnyGranted="ROLE_AEM,ROLE_ADMIN_${prop_site_id}"> 

<script type="text/javascript">
	window.addEvent('domready', function(){
		var box2 = new multiBox('mbContact', {descClassName: 'multiBoxDesc',waitDuration: 5,showControls: false,overlay: new overlay()});
		$$('a.action').each(function(img){
			//containers
			var actionList = img.getParent(); 
			var actionHover = actionList.getElements('div.action-hover')[0];
			actionHover.set('opacity',0);
			//show/hide
			img.addEvent('mouseenter',function() {
				actionHover.setStyle('margin-left','20px').fade('in');
				actionHover.setStyle('padding-left','5px').fade('in');
				actionHover.setStyle('display','block').fade('in');
			});
			actionHover.addEvent('mouseleave',function(){
				actionHover.fade('out');
			});
			actionList.addEvent('mouseleave',function() {
				actionHover.fade('out');
			});
		});
	});	    
</script>

<form action="privilegeList.jhtm" method="get">
<!-- main box -->
  <div id="mboxfull">
  
  <!-- start table -->
  <table cellpadding="0" cellspacing="0" border="0" class="module" >
  <tr>
    <td class="topl_g">
	  
	  <!-- breadcrumb -->
	  <p class="breadcrumb">
	    <a href="../privilege">Privilege</a> &gt;
	    users 
	  </p>
	  
	  <!-- Error Message -->
	  
	  <!-- header image -->
	  <img class="headerImage" src="../graphics/Privilege.gif" />
	  
	  </td><td class="topr_g" ></td></tr>
  <tr><td class="boxmidlrg" >

 <!-- tabs -->
 <div class="tab-wrapper">
	<h4 title="List of affiliates"></h4>
	<div>
	<!-- start tab -->
	
	  	<div class="listdivi ln tabdivi"></div>
	  	<div class="listdivi"></div>
	   
		  	<div class="listlight">
		  	<!-- input field --> 
		  	<table border="0" cellpadding="0" cellspacing="1" width="100%">
			  <tr>
			  <c:if test="${model.count > 0}">
			  <td class="pageShowing">
			  <fmt:message key="showing">
				<fmt:param value="${accessPrivilegeSearch.offset+1}"/>
				<fmt:param value="${model.pageEnd}"/>
				<fmt:param value="${model.count}"/>
			  </fmt:message>
			  </td>
			  </c:if>
			  <td class="pageNavi">
			  Page 
			  <select name="page" id="page" onchange="submit()">
			  <c:forEach begin="1" end="${model.pageCount}" var="page">
			  	<option value="${page}" <c:if test="${page == (accessPrivilegeSearch.page)}">selected</c:if>>${page}</option>
			  </c:forEach>
			  </select>
			  of <c:out value="${model.pageCount}"/>
			  | 
			  <c:if test="${accessPrivilegeSearch.page == 1}"><span class="pageNaviDead">previous</span></c:if>
			  <c:if test="${accessPrivilegeSearch.page != 1}"><a href="<c:url value="privilegeList.jhtm"><c:param name="page" value="${accessPrivilegeSearch.page-1}"/></c:url>" class="pageNaviLink">previous</a></c:if>
			  | 
			  <c:if test="${accessPrivilegeSearch.page == model.pageCount}"><span class="pageNaviDead">next</span></c:if>
			  <c:if test="${accessPrivilegeSearch.page != model.pageCount}"><a href="<c:url value="privilegeList.jhtm"><c:param name="page" value="${accessPrivilegeSearch.page+1}"/></c:url>" class="pageNaviLink">next</a></c:if>
			  </td>
			  </tr>
			</table>
			
			<table border="0" cellpadding="0" cellspacing="0" width="100%" class="listings">
			  <tr class="listingsHdr2">
			    <td class="indexCol">&nbsp;</td>
			    <td class="listingsHdr3"><fmt:message key="username" /></td>
			    <td class="listingsHdr3"><fmt:message key="active" /></td>
			    <td class="listingsHdr3"><fmt:message key="action" /></td>
			    <c:if test="${siteConfig['ACCESS_PRIVILEGE_GROUP'].value == 'true'}">
				  <td class="listingsHdr3"><fmt:message key="noOfGroups" /></td>
				</c:if>
			  </tr>
			<c:forEach items="${model.accessUserList}" var="user" varStatus="status">
			  <tr class="row${status.index % 2}" onmouseover="changeStyleClass(this,'rowOver')" onmouseout="changeStyleClass(this,'row${status.index % 2}')">
			    <td class="indexCol"><c:out value="${status.count}"/>.</td>
			    <td class="nameCol"><a href="privilege.jhtm?id=${user.id}" class="nameLink"><c:out value="${user.username}"/></a></td>
			    <td class="nameCol"><c:if test="${user.enable}"><div align="left"><img src="../graphics/checkbox.png" /></div></c:if></td>
			    <td class="nameCol"><a href="privilegeAuditList.jhtm?username=${user.username}" class="nameLink"><fmt:message key="audit"/></a></td>
			    <c:if test="${siteConfig['ACCESS_PRIVILEGE_GROUP'].value == 'true'}">
				  <td class="nameCol">
				  <c:choose>
				    <c:when test="${fn:length(user.groupSetNames) > 0}">
				      <a href="#" class="action"><c:out value="${fn:length(user.groupSetNames)}"/> </a>
				  	  <div class="action-hover" style="display: none;">
			          <ul class="round">
			            <li class="action-header">
					      <div class="name">Groups:</div>
					      <div class="menudiv"></div>
					      <div class="name">
					        <c:forEach items="${user.groupSetNames}" var="groupName">
					          <c:out value="${groupName}"/><br/>
					        </c:forEach>
					      </div>
			            </li>
			          </ul>
			          </div>
				    </c:when>
				    <c:otherwise>
				      <a href="#"><c:out value="${fn:length(user.groupSetNames)}"/></a>
				    </c:otherwise>
				  </c:choose>
				  </td>
			    </c:if>
			  </tr>
			</c:forEach>
			<c:if test="${model.count == 0}">
			  <tr class="emptyList"><td colspan="3">&nbsp;</td></tr>
			</c:if>
			</table>
			
			<table border="0" cellpadding="0" cellspacing="1" width="100%">
			  <tr>
			  <td class="pageSize">
			  <select name="size" onchange="document.getElementById('page').value=1;submit()">
			  <c:forTokens items="10,25,50" delims="," var="current">
			  	  <option value="${current}" <c:if test="${current == accessPrivilegeSearch.pageSize}">selected</c:if>>${current} <fmt:message key="perPage" /></option>
			  </c:forTokens>
			  </select>
			  </td>
			  </tr>
			</table>
		  	<!-- end input field -->  	  
		  	
		  	<!-- start button --> 
		  	<div align="left" class="button">
		  	  <input type="submit" name="__add" value="<fmt:message key="userAdd" />">
			</div>
			<!-- end button -->	
			</div>
					 	
	<!-- end tab -->        
	</div>
<!-- end tabs -->			
</div>
  
  <!-- end table -->
   </td><td class="boxmidr" ></td></tr>
  <tr><td class="botl"></td><td class="botr"></td></tr>
  </table>
  
<!-- end main box -->  
</div> 
</form> 
</sec:authorize>

  </tiles:putAttribute>    
</tiles:insertDefinition>