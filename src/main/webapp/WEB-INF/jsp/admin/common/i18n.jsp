<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>

<c:if test="${url == null}">
&nbsp;
</c:if>

<c:if test="${url != null}">
<ul class="language">
<c:forEach items="${adminLanguages}" var="language">
<li><a href="<c:out value="${url}" />language=${language}"><img src="../graphics/${language}.gif" border="0" alt="" title="<fmt:message key="language_${language}"/>" /></a></li>
</c:forEach>
</ul>
</c:if>
