<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/security/tags" prefix="sec" %>
<c:if test="${!empty siteConfig['SITE_TIME_ZONE'].value}" >
<fmt:setTimeZone value="${siteConfig['SITE_TIME_ZONE'].value}" scope="application"/>
</c:if>
<script language="javascript" type="text/javascript">
/* <![CDATA[ */
var lTime=<%= session.getMaxInactiveInterval() %>;
function resetTimer() {
lTime=<%= session.getMaxInactiveInterval() %>;
box.close($('mb1'));
}
function LogoutTimer() {
lTime=lTime-1;
var lMins=(lTime-(lTime%60))/60;
var lSecs=lTime%60;
window.status="Auto Logout: " + lMins + " Min " + lSecs + " Sec" ;
//5 min warning
if (lMins==5 && lSecs==0) {
box.open($('mb1'));
}
//timeout warning
if (lMins==0 && lSecs==0) {
///alert('Your session has timed out. You will need to login again. Please make a copy of your work before you do anything else as it may be lost.');
window.location = "../logout.jsp";
}
}
window.setInterval("LogoutTimer()",1000);
LogoutTimer();
/* ]]> */
</script>
<!-- header -->
<table border="0" cellpadding="0" cellspacing="0" width="100%" class="listings quickMode" >
<tr class="listingsHdr4">
	<td align="left" style="height:50px !important;padding:0;margin-left:2px">
	 <c:choose>
	 <c:when test="${'datamagic' == gSiteConfig['gRESELLER']}"><a href="${siteConfig['SITE_URL'].value}" target="_blank"><span class="headerWebImage"><img src="https://www.webjaguar.net/logo/datamagiclogo.png" border="0" /></span></a></c:when>
     <c:when test="${'roi' == gSiteConfig['gRESELLER']}"><a href="${siteConfig['SITE_URL'].value}" target="_blank"><span class="headerWebImage"><img src="https://www.webjaguar.net/logo/roilogo.jpg" border="0" /></span></a></c:when>
     <c:when test="${'via' == gSiteConfig['gRESELLER']}"><a href="${siteConfig['SITE_URL'].value}" target="_blank"><span class="headerWebImage"><img src="https://www.webjaguar.com/logo/vialogo.png" border="0" /></span></a></c:when>
     <c:when test="${'aem' == gSiteConfig['gRESELLER']}"><a href="${siteConfig['SITE_URL'].value}" target="_blank"><span class="headerWebImage"><img src="https://www.webjaguar.net/logo/aemlogo.png" border="0" /></span></a></c:when>
     <c:otherwise><div class="headerWebName"><a href="${siteConfig['SITE_URL'].value}" target="_blank"><c:out value="${siteConfig['SITE_URL'].value}"/></a></div></c:otherwise>
     </c:choose>
	</td>
	<td valign="top" style="height:50px !important;font-size:.9em;padding:0;">
	<div style="float:right; display:block; width:500px;">
	 <div style="float:right;">
	 <div style="float:left;text-align:left">
	 <span class="username"><sec:authentication property="principal.username"/></span>
     <span class="role">
      <sec:authorize ifNotGranted="ROLE_AEM,ROLE_ADMIN_${prop_site_id}"></sec:authorize>
      <sec:authorize ifAllGranted="ROLE_ADMIN_${prop_site_id}">(Administrator)</sec:authorize>
      <sec:authorize ifAllGranted="ROLE_AEM">(AEM)</sec:authorize>
     </span>
     <span style="color:#dddddd;">&nbsp;|&nbsp;</span>
     <span style="color:#dddddd;font-weight:600px;"><jsp:useBean id="now" class="java.util.Date" scope="request" />
        <span><fmt:formatDate value="${now}" pattern="EEEE, MMMM d, yyyy" /></span>
     </span>
     <span style="color:#dddddd;">&nbsp;|&nbsp;</span>
     <span><a class="logout" href="../logout.jsp"><fmt:message key='logout' /></a>&nbsp;</span>
     </div>
     <div>
     <div align="right" class="flag"></div>
     <div align="left" class="frontend"><a href="${siteConfig['SITE_URL'].value}" target="_blank"><c:out value="${siteConfig['SITE_URL'].value}"/></a></div>
     </div>
     </div>
    </div>
    <a href="#htmlElement" rel="type:element" id="mb1" class="mbxwz" title="<fmt:message key='session' />"></a>
    <div class="multiBoxDesc mb1"></div><br />
    <div id="htmlElement" style="width:350px;">
      <div class="header">
        <span><fmt:message key='sessionTimeoutWarning' /></span>
      </div>
    <fmt:message key="sessionAlert" /><br /><br /><a href="../config/resetSession.jhtm?reset=true" target="iframe" onclick="resetTimer();"><fmt:message key="giveMeMoreTime" /></a><br />
    <a href="../logout.jsp"><fmt:message key='logout' /></a></div>
    <iframe style="display:none;" name="iframe"></iframe>
    
	</td>
</tr>
</table>
<script type="text/javascript">
	var box = {};
	window.addEvent('domready', function(){
		box = new multiBox('mbxwz', {descClassName: 'multiBoxDesc',showNumbers: false,openFromLink: false});
	   	var menu = new DropMenu('navmenu-h');
	}); 
</script>

<div class="quickMode" id="header">
 <div id="header_l">
 <ul id="navmenu-h" class="mainmenu">
  <c:if test="${!empty siteConfig['ADMIN_MESSAGE'].value}">
    <li class="main"><a <c:choose><c:when test="${param.tab == 'message'}"> class="headerSelectedLink"</c:when><c:otherwise>class="headerOffLink"</c:otherwise></c:choose> href="../message/" ><strong><c:out value="${siteConfig['ADMIN_MESSAGE'].value}"/></strong></a></li>
  </c:if>
  <li class="main"><a <c:choose><c:when test="${param.tab == 'catalog'}"> class="headerSelectedLink"</c:when><c:otherwise>class="headerOffLink"</c:otherwise></c:choose> href="../catalog/" ><strong><fmt:message key="catalogTabTitle"/></strong></a>
    <ul class="submenu">
      <li><a href="../catalog/categoryList.jhtm" class="headerOffLink"><fmt:message key="categories"/></a></li>
      <li><a href="../catalog/productList.jhtm" class="headerOffLink"><fmt:message key="products"/></a></li>
      <li><a href="../catalog/productFields.jhtm" class="headerOffLink"><fmt:message key="fields"/></a></li>
      <li><a href="../catalog/options.jhtm" class="headerOffLink"><fmt:message key="options"/></a></li>
      <li><a href="../catalog/truckLoadProcessList.jhtm" class="headerOffLink"><fmt:message key="processingList"/></a></li>
      <c:if test="${gSiteConfig['gPRODUCT_REVIEW'] or siteConfig['PRODUCT_RATE'].value == 'true'}">
        <li><a href="../catalog/productReviewList.jhtm" class="headerOffLink"><fmt:message key="reviews"/></a></li>
      </c:if>
    </ul>
  </li>
  <c:if test="${gSiteConfig['gPURCHASE_ORDER']}">
    <li class="main"><a <c:choose><c:when test="${param.tab == 'inventory'}"> class="headerSelectedLink"</c:when><c:otherwise>class="headerOffLink"</c:otherwise></c:choose> href="../inventory/" ><strong><fmt:message key="purchaseOrderTabTitle"/></strong></a></li>
  </c:if>
  <li class="main"><a <c:choose><c:when test="${param.tab == 'customers'}"> class="headerSelectedLink"</c:when><c:otherwise>class="headerOffLink"</c:otherwise></c:choose> href="../customers/" ><strong><fmt:message key="customersTabTitle"/></strong></a>
    <ul class="submenu">
      <li><a href="../customers/customerList.jhtm" class="headerOffLink"><fmt:message key="customers"/></a></li>
      <li><a href="../customers/customerList2.jhtm" class="headerOffLink"><fmt:message key="customers"/> List2 </a></li>
      <c:if test="${siteConfig['LUCENE_REINDEX'].value == 'true'}">
        <li><a href="../customers/lCustomerList.jhtm" class="headerOffLink">Lucene <fmt:message key="customers"/> <fmt:message key="list"/></a></li>
      	<li><a href="../customers/customerIndex.jhtm" class="headerOffLink">Lucene Index Now</a></li>
      </c:if>
      <li><a href="../customers/addCustomer.jhtm" class="headerOffLink"><fmt:message key="addCustomersMenuTitle"/></a></li>
      <c:if test="${gSiteConfig['gGROUP_CUSTOMER']}">
        <li><a href="../customers/customerGroupList.jhtm" class="headerOffLink"><fmt:message key="groups"/></a></li>
      </c:if>
      <sec:authorize ifAnyGranted="ROLE_AEM,ROLE_ADMIN_${prop_site_id},ROLE_CUSTOMER_FIELD">
      <c:if test="${gSiteConfig['gCUSTOMER_FIELDS'] > 0}">
        <li><a href="../customers/customerFields.jhtm" class="headerOffLink"><fmt:message key="Fields"/></a></li>
      </c:if>
      </sec:authorize>
      <c:if test="${gSiteConfig['gPAYMENTS']}">
      <sec:authorize ifAnyGranted="ROLE_AEM,ROLE_ADMIN_${prop_site_id},ROLE_INCOMING_PAYMENT_VIEW_LIST">
        <li><a href="../customers/payments.jhtm?cid=" class="headerOffLink"><fmt:message key="incomingPayments"/></a></li>
      </sec:authorize>
      </c:if>
      <c:if test="${gSiteConfig['gVIRTUAL_BANK_ACCOUNT'] }" >
      <sec:authorize ifAnyGranted="ROLE_AEM,ROLE_ADMIN_${prop_site_id},ROLE_OUTGOING_PAYMENT_VIEW_LIST">
        <li><a href="../customers/virtualBankPayment.jhtm" class="headerOffLink"><fmt:message key="outgoingPayments"/></a></li>
      </sec:authorize>
      </c:if>
      <c:if test="${gSiteConfig['gAFFILIATE'] > 0}">
      <sec:authorize ifAnyGranted="ROLE_AEM,ROLE_ADMIN_${prop_site_id},ROLE_CUSTOMER_AFFILIATE">
        <li><a <c:choose><c:when test="${param.tab == 'affiliate'}"> class="headerSelectedLink"</c:when><c:otherwise>class="headerOffLink"</c:otherwise></c:choose> href="../customers/affiliateList.jhtm" ><fmt:message key="affiliateTabTitle"/></a></li>
      </sec:authorize>  
  </c:if>
    </ul>
  </li>
  <sec:authorize ifAnyGranted="ROLE_AEM,ROLE_ADMIN_${prop_site_id},ROLE_PROMOTION_CREATE,ROLE_PROMOTION_EDIT,ROLE_PROMOTION_DELETE,ROLE_SALES_TAG_CREATE,ROLE_SALES_TAG_EDIT,ROLE_SALES_TAG_DELETE">  
  <c:if test="${gSiteConfig['gSALES_PROMOTIONS'] or gSiteConfig['gGIFTCARD'] or gSiteConfig['gDEALS'] > 0}">
    <li class="main"><a <c:choose><c:when test="${param.tab == 'promos'}"> class="headerSelectedLink"</c:when><c:otherwise>class="headerOffLink"</c:otherwise></c:choose> href="../promos/" ><strong><fmt:message key="promosTabTitle"/></strong></a>
      <ul class="submenu">
      <sec:authorize ifAnyGranted="ROLE_AEM,ROLE_ADMIN_${prop_site_id},ROLE_PROMOTION_CREATE,ROLE_PROMOTION_EDIT,ROLE_PROMOTION_DELETE">
        <li><a href="../promos/promoList.jhtm" class="headerOffLink"><fmt:message key="promotionsTitle"/></a></li>
      </sec:authorize>
      <sec:authorize ifAnyGranted="ROLE_AEM,ROLE_ADMIN_${prop_site_id},ROLE_PROMOTION_CREATE,ROLE_PROMOTION_EDIT,ROLE_PROMOTION_DELETE">
        <li><a href="../promos/promoDynamicList.jhtm" class="headerOffLink"><fmt:message key="promotionsDynamicTitle"/></a></li>
        <li><a href="../promos/campaignList.jhtm" class="headerOffLink"><fmt:message key="Campaigns"/></a></li>
      </sec:authorize>
      <sec:authorize ifAnyGranted="ROLE_AEM,ROLE_ADMIN_${prop_site_id},ROLE_PROMOTION_CREATE,ROLE_PROMOTION_EDIT,ROLE_PROMOTION_DELETE">
      <c:if test="${empty siteConfig['PRODUCT_SYNC'].value}" >
        <li><a href="../promos/salesTagList.jhtm" class="headerOffLink"><fmt:message key="salesTagsTitle"/></a></li>
      </c:if>
      </sec:authorize>
      <sec:authorize ifAnyGranted="ROLE_AEM,ROLE_ADMIN_${prop_site_id}">
      <c:if test="${gSiteConfig['gGIFTCARD']}">
        <li><a href="../promos/giftCardList.jhtm" class="headerOffLink"><fmt:message key="giftCard"/></a></li>
      </c:if>
      </sec:authorize>
      <sec:authorize ifAnyGranted="ROLE_AEM,ROLE_ADMIN_${prop_site_id}">
      <c:if test="${gSiteConfig['gDEALS'] > 0}">
        <li><a href="../promos/dealList.jhtm" class="headerOffLink"><fmt:message key="deals"/></a></li>
         <li><a href="../promos/viaDealList.jhtm" class="headerOffLink">Via Deals</a></li>
      </c:if>
      </sec:authorize>
      <sec:authorize ifAnyGranted="ROLE_AEM,ROLE_ADMIN_${prop_site_id},ROLE_MAILINREBATE_CREATE,ROLE_MAILINREBATE_DELETE">
      <c:if test="${gSiteConfig['gMAIL_IN_REBATES'] > 0}">
        <li><a href="../promos/mailInRebateList.jhtm" class="headerOffLink"><fmt:message key="mailInRebates"/></a></li>
      </c:if>
      </sec:authorize>
      </ul>
    </li>
  </c:if>
  </sec:authorize>
  <li class="main"><a <c:choose><c:when test="${param.tab == 'faq' or param.tab == 'policy' or param.tab == 'ticket'}">class="headerSelectedLink"</c:when><c:otherwise>class="headerOffLink"</c:otherwise></c:choose> href="#" ><strong><span>Support</span></strong></a>
   <ul class="submenu">
   <sec:authorize ifAnyGranted="ROLE_AEM,ROLE_ADMIN_${prop_site_id},ROLE_FAQ_CREATE,ROLE_FAQ_EDIT,ROLE_FAQ_DELETE">   
    <li><a <c:choose><c:when test="${param.tab == 'faq'}"> class="headerSelectedLink"</c:when><c:otherwise>class="headerOffLink"</c:otherwise></c:choose> href="../faq/" ><fmt:message key="faqTabTitle"/></a></li>
  </sec:authorize>
  <sec:authorize ifAnyGranted="ROLE_AEM,ROLE_ADMIN_${prop_site_id},ROLE_POLICY_CREATE,ROLE_POLICY_EDIT,ROLE_POLICY_DELETE">   
    <li><a <c:choose><c:when test="${param.tab == 'policy'}"> class="headerSelectedLink"</c:when><c:otherwise>class="headerOffLink"</c:otherwise></c:choose> href="../policy/" ><fmt:message key="policyTabTitle"/></a></li>
  </sec:authorize>
   <sec:authorize ifAnyGranted="ROLE_AEM,ROLE_ADMIN_${prop_site_id},ROLE_TICKET_UPDATE,ROLE_TICKET_VIEW_LIST">   
  <c:if test="${gSiteConfig['gTICKET']}">
    <li><a <c:choose><c:when test="${param.tab == 'ticket'}"> class="headerSelectedLink"</c:when><c:otherwise>class="headerOffLink"</c:otherwise></c:choose> href="../ticket/" ><fmt:message key="ticketTabTitle"/></a></li>
  </c:if>
  </sec:authorize>
   </ul>
   </li>
  <sec:authorize ifAnyGranted="ROLE_AEM,ROLE_ADMIN_${prop_site_id},ROLE_LAYOUT_EDIT,ROLE_LAYOUT_HEAD_TAG">   
    <li class="main"><a <c:choose><c:when test="${param.tab == 'layout'}"> class="headerSelectedLink"</c:when><c:otherwise>class="headerOffLink"</c:otherwise></c:choose> href="../layout/" ><strong><fmt:message key="layoutTabTitle"/></strong></a></li>
  </sec:authorize>
  <c:if test="${siteConfig['PRODUCT_QUOTE'].value == 'true' or gSiteConfig['gSHIPPING_QUOTE']}">
  	<li class="main"><a class="headerOffLink" href="../orders/quotesList.jhtm" ><strong><fmt:message key="quotesTabTitle"/></strong></a></li>
  </c:if>
  <c:if test="${gSiteConfig['gSHOPPING_CART']}">
    <li class="main"><a <c:choose><c:when test="${param.tab == 'orders'}"> class="headerSelectedLink"</c:when><c:otherwise>class="headerOffLink"</c:otherwise></c:choose> href="../orders/" ><strong><fmt:message key="ordersTabTitle"/></strong></a></li>
    <sec:authorize ifAnyGranted="ROLE_AEM,ROLE_ADMIN_${prop_site_id},ROLE_SALES_REP_VIEW_LIST"> 
    <c:if test="${gSiteConfig['gSALES_REP']}">
      <li class="main"><a <c:choose><c:when test="${param.tab == 'salesRep'}"> class="headerSelectedLink"</c:when><c:otherwise>class="headerOffLink"</c:otherwise></c:choose> href="../salesRep/" ><strong><fmt:message key="salesRepTabTitle"/></strong></a></li>
    </c:if>
    </sec:authorize>
  </c:if>
  <sec:authorize ifAnyGranted="ROLE_AEM,ROLE_ADMIN_${prop_site_id},ROLE_SUPPLIER_VIEW_LIST">  
  <c:if test="${gSiteConfig['gSUPPLIER']}">
    <li class="main"><a <c:choose><c:when test="${param.tab == 'supplier'}"> class="headerSelectedLink"</c:when><c:otherwise>class="headerOffLink"</c:otherwise></c:choose> href="../supplier/" ><strong><fmt:message key="supplierTabTitle"/></strong></a></li>
  </c:if>
  </sec:authorize>
  <sec:authorize ifAnyGranted="ROLE_AEM,ROLE_ADMIN_${prop_site_id},ROLE_SERVICE_VIEW_LIST">   
  <c:if test="${gSiteConfig['gSERVICE']}">
    <li class="main"><a <c:choose><c:when test="${param.tab == 'service'}"> class="headerSelectedLink"</c:when><c:otherwise>class="headerOffLink"</c:otherwise></c:choose> href="../service/" ><strong><fmt:message key="serviceTabTitle"/></strong></a></li>
  </c:if>
  </sec:authorize>
  <c:if test="${gSiteConfig['gRMA']}">
    <li class="main"><a <c:choose><c:when test="${param.tab == 'rma'}"> class="headerSelectedLink"</c:when><c:otherwise>class="headerOffLink"</c:otherwise></c:choose> href="../rma/" ><strong><fmt:message key="rma"/></strong></a></li>
  </c:if>
  <sec:authorize ifAnyGranted="ROLE_AEM,ROLE_ADMIN_${prop_site_id},ROLE_DATA_FEED">   
  <c:if test="${!empty gSiteConfig['gDATA_FEED'] or gSiteConfig['gWEBJAGUAR_DATA_FEED'] > 0}">
    <li class="main"><a <c:choose><c:when test="${param.tab == 'dataFeed'}"> class="headerSelectedLink"</c:when><c:otherwise>class="headerOffLink"</c:otherwise></c:choose> href="../dataFeed/" ><strong><fmt:message key="dataFeedTabTitle"/></strong></a>
      <ul class="submenu">
      <c:forTokens items="${gSiteConfig['gDATA_FEED']}" delims="," var="feed">   
  		  <li><a href="../dataFeed/dataFeed.jhtm?feed=${feed}" class="headerOffLink"><fmt:message key="${feed}"/></a></li>  
      </c:forTokens>
      <c:if test="${gSiteConfig['gWEBJAGUAR_DATA_FEED'] > 0}">
          <li><a href="../dataFeed/webjaguarDataFeedList.jhtm" class="headerOffLink"><c:out value="${siteConfig['FEED_NAME'].value}"></c:out></a></li>  
      </c:if>
    </ul>
    </li>
  </c:if>
  </sec:authorize>
  <sec:authorize ifAnyGranted="ROLE_AEM,ROLE_ADMIN_${prop_site_id},ROLE_REPORT_CUSTOMERS,ROLE_REPORT_SALESREP,ROLE_REPORT_SALESREP_DAILY,ROLE_REPORT_TRACKCODE,ROLE_REPORT_IMP_EXP,ROLE_REPORT_SHIPPED_ORDER,ROLE_REPORT_ORDER_NEED_ATTENTION,ROLE_REPORT_SHIPPED_OVERVIEW,ROLE_REPORT_ORDER_PEN_PROC,ROLE_REPORT_ORDER_OVERVIEW,ROLE_REPORT_SHIPPED_DAILY,ROLE_REPORT_AFFILIATE_COMMISSION,ROLE_REPORT_PRODUCT,ROLE_REPORT_SALESREP_OVERVIEWII,ROLE_REPORT_CUSTOMER_METRIC">   
  <c:if test="${gSiteConfig['gREPORT']}">
    <li class="main"><a <c:choose><c:when test="${param.tab == 'reports'}"> class="headerSelectedLink"</c:when><c:otherwise>class="headerOffLink"</c:otherwise></c:choose> href="../reports/" ><strong><fmt:message key="reportTabTitle"/></strong></a>
    <ul class="submenu">
      <sec:authorize ifAnyGranted="ROLE_AEM,ROLE_ADMIN_${prop_site_id},ROLE_REPORT_SHIPPED_ORDER,ROLE_REPORT_ORDER_NEED_ATTENTION,ROLE_REPORT_SHIPPED_OVERVIEW,ROLE_REPORT_ORDER_PEN_PROC,ROLE_REPORT_SHIPPED_DAILY,ROLE_REPORT_AFFILIATE_COMMISSION">
        <li><a href="../reports/orderReport.jhtm" class="headerOffLink"><fmt:message key="orders"/></a></li>
      </sec:authorize>
      <sec:authorize ifAnyGranted="ROLE_AEM,ROLE_ADMIN_${prop_site_id},ROLE_REPORT_ORDER_PEN_PROC,ROLE_REPORT_SHIPPED_DAILY">
        <li><a href="../reports/dailyOrderReport.jhtm" class="headerOffLink"><fmt:message key="ordersDaily"/></a></li>
      </sec:authorize>
      <sec:authorize ifAnyGranted="ROLE_AEM,ROLE_ADMIN_${prop_site_id},ROLE_REPORT_ORDER_OVERVIEW">
        <li><a href="../reports/orderDetailReport.jhtm" class="headerOffLink"><fmt:message key="ordersOverview"/></a></li>
      </sec:authorize>
      <sec:authorize ifAnyGranted="ROLE_AEM,ROLE_ADMIN_${prop_site_id},ROLE_REPORT_CUSTOMERS"> 
        <li><a href="../reports/customerReport.jhtm" class="headerOffLink"><fmt:message key="customersOverview"/></a></li>
        <li><a href="../reports/customerInactiveReport.jhtm" class="headerOffLink"><fmt:message key="customersInactive"/></a></li>
      </sec:authorize>
      <sec:authorize ifAnyGranted="ROLE_AEM,ROLE_ADMIN_${prop_site_id},ROLE_REPORT_SALESREP">   
        <li><a href="../reports/salesRepReport.jhtm" class="headerOffLink"><fmt:message key="salesRep"/></a></li>  
      </sec:authorize>
      <sec:authorize ifAnyGranted="ROLE_AEM,ROLE_ADMIN_${prop_site_id},ROLE_REPORT_SALESREP_DAILY">   
        <li><a href="../reports/salesRepReportDaily.jhtm" class="headerOffLink"><fmt:message key="salesRep"/><fmt:message key="daily"/></a></li>
      </sec:authorize>
      <sec:authorize ifAnyGranted="ROLE_AEM,ROLE_ADMIN_${prop_site_id},ROLE_REPORT_SALESREP_DAILY">   
        <li><a href="../reports/salesRepDetailReport.jhtm" class="headerOffLink"><fmt:message key="salesRepOverview"/></a></li>
      </sec:authorize>      
      <sec:authorize ifAnyGranted="ROLE_AEM,ROLE_ADMIN_${prop_site_id},ROLE_REPORT_SALESREP_OVERVIEWII">   
        <li><a href="../reports/salesRepDetailReport2.jhtm" class="headerOffLink"><fmt:message key="salesRepOverview"/> II</a></li>
      </sec:authorize>  
      <sec:authorize ifAnyGranted="ROLE_AEM,ROLE_ADMIN_${prop_site_id},ROLE_REPORT_CUSTOMER_METRIC">
      	<li><a href="../reports/metricOverviewReport.jhtm" class="headerOffLink"><fmt:message key="metricOverviewReport"/></a></li>
	  </sec:authorize>
      <sec:authorize ifAnyGranted="ROLE_AEM,ROLE_ADMIN_${prop_site_id},ROLE_REPORT_TRACKCODE">   
        <li><a href="../reports/trackcodeReport.jhtm" class="headerOffLink"><fmt:message key="trackcode"/></a></li>    
      </sec:authorize>
      <c:if test="${gSiteConfig['gPROCESSING']}">
      <sec:authorize ifAnyGranted="ROLE_AEM,ROLE_ADMIN_${prop_site_id},ROLE_REPORT_PROCESSING">   
        <li><a href="../reports/truckLoadProcessReport.jhtm" class="headerOffLink"><fmt:message key="truckLoadProcess"/></a></li>    
      </sec:authorize>
      </c:if>
      <sec:authorize ifAnyGranted="ROLE_AEM,ROLE_ADMIN_${prop_site_id},ROLE_REPORT_IMP_EXP">   
        <li><a href="../reports/importExportHistory.jhtm" class="headerOffLink"><fmt:message key="importExport"/></a></li>
      </sec:authorize>
      <sec:authorize ifAnyGranted="ROLE_AEM,ROLE_ADMIN_${prop_site_id},ROLE_EMAILMANAGER_VIEW_HISTORY">   
        <li><a href="../reports/massEmailReport.jhtm" class="headerOffLink"><fmt:message key="massEmail"/></a></li>
      </sec:authorize>
      <sec:authorize ifAnyGranted="ROLE_AEM,ROLE_ADMIN_${prop_site_id},ROLE_REPORT_PRODUCT">   
        <li><a href="../reports/productListReport.jhtm" class="headerOffLink"><fmt:message key="productReport"/></a></li>
      </sec:authorize>
      <c:if test="${siteConfig['INVENTORY_HISTORY'].value == 'true'}">
      <sec:authorize ifAnyGranted="ROLE_AEM,ROLE_ADMIN_${prop_site_id},ROLE_REPORT_INVENTORY">   
        <li><a href="../reports/inventoryActivityReport.jhtm" class="headerOffLink"><fmt:message key="inventoryActivity"/></a></li>
      </sec:authorize>
      <sec:authorize ifAnyGranted="ROLE_AEM,ROLE_ADMIN_${prop_site_id},ROLE_REPORT_INVENTORY">   
        <li><a href="../reports/inventoryReport.jhtm" class="headerOffLink"><fmt:message key="inventorySummary"/></a></li>
      </sec:authorize> 
      </c:if>
      <sec:authorize ifAnyGranted="ROLE_AEM,ROLE_ADMIN_${prop_site_id},ROLE_REPORT_LAY">   
        <li><a href="../reports/layReport.jhtm" class="headerOffLink"><fmt:message key="layReport"/></a></li>
      </sec:authorize> 
      <sec:authorize ifAnyGranted="ROLE_AEM,ROLE_ADMIN_${prop_site_id},ROLE_REPORT_ABANDONED_SHOPPING_CART">   
      	<li><a href="../reports/abandonedShoppingCartController.jhtm" class="headerOffLink"><fmt:message key="abandonedShoppingCart"/></a></li>
      </sec:authorize>
      <sec:authorize ifAnyGranted="ROLE_AEM,ROLE_ADMIN_${prop_site_id},ROLE_REPORT_EVENT_MEMBER_LIST"> 
      <li><a href="../reports/eventMemberListReport.jhtm" class="headerOffLink"><fmt:message key="eventMemberList"/></a></li>
      </sec:authorize>
    </ul>
    </li>
  </c:if>
  </sec:authorize>
  <sec:authorize ifAnyGranted="ROLE_AEM,ROLE_ADMIN_${prop_site_id},ROLE_CRM,ROLE_CRM_FIELD,ROLE_CRM_GROUP,ROLE_CRM_FORM,ROLE_CRM_IMPORT_EXPORT">   
  <c:if test="${gSiteConfig['gCRM']}">
    <li class="main"><a <c:choose><c:when test="${param.tab == 'crm'}"> class="headerSelectedLink"</c:when><c:otherwise>class="headerOffLink"</c:otherwise></c:choose> href="../crm/" ><strong><fmt:message key="crmTabTitle"/></strong></a>
    <ul class="submenu">
      <sec:authorize ifAnyGranted="ROLE_AEM,ROLE_ADMIN_${prop_site_id},ROLE_CRM">
      <li><a href="../crm/crmAccountList.jhtm" class="headerOffLink"><fmt:message key="accounts"/></a></li>
      <li><a href="../crm/crmContactList.jhtm" class="headerOffLink"><fmt:message key="contacts"/></a></li>
      <li><a href="../crm/crmTaskList.jhtm" class="headerOffLink"><fmt:message key="tasks"/></a></li>
      <li><a href="../crm/crmQualifierList.jhtm" class="headerOffLink"><fmt:message key="qualifier"/></a></li>
      </sec:authorize>
      <sec:authorize ifAnyGranted="ROLE_AEM,ROLE_ADMIN_${prop_site_id},ROLE_CRM_FORM">
      <li><a href="../crm/crmFormList.jhtm" class="headerOffLink"><fmt:message key="forms"/></a></li>
      </sec:authorize>
      <sec:authorize ifAnyGranted="ROLE_AEM,ROLE_ADMIN_${prop_site_id},ROLE_CRM_FIELD">
      <li><a href="../crm/crmContactFieldList.jhtm" class="headerOffLink"><fmt:message key="fields"/></a></li>
      </sec:authorize>
      <sec:authorize ifAnyGranted="ROLE_AEM,ROLE_ADMIN_${prop_site_id},ROLE_CRM_GROUP">
      <li><a href="../crm/crmContactGroupList.jhtm" class="headerOffLink"><fmt:message key="groups"/></a></li>
      </sec:authorize>
    </ul>
    </li>
  </c:if>
  </sec:authorize>
  <sec:authorize ifAnyGranted="ROLE_AEM,ROLE_ADMIN_${prop_site_id},ROLE_SITE_INFO">
  <li class="main"><a <c:choose><c:when test="${param.tab == 'config'}"> class="headerSelectedLink"</c:when><c:otherwise>class="headerOffLink"</c:otherwise></c:choose> href="../config/configuration.jhtm" ><strong><fmt:message key="configTabTitle"/></strong></a>
    <ul class="submenu">
      <li><a href="../config/configuration.jhtm" class="headerOffLink"><fmt:message key="siteConfigurationTitle"/></a></li>
      <li><a href="../config/siteMessage.jhtm" class="headerOffLink"><fmt:message key="siteMessageTitle"/></a></li>
      <li><a href="../config/siteMessageGroup.jhtm" class="headerOffLink"><fmt:message key="siteMessageGroupTitle"/></a></li>
      <c:if test="${gSiteConfig['gSHOPPING_CART']}">	
      <li><a href="../config/shippingMethodList.jhtm" class="headerOffLink"><fmt:message key="carriers"/></a></li>
      <li><a href="../config/customShippingList.jhtm" class="headerOffLink"><fmt:message key="customShipping"/></a></li>
      </c:if>
      <c:if test="${gSiteConfig['gI18N']!=''}">
       <li><a href="../config/languageList.jhtm" class="headerOffLink"><fmt:message key="language"/></a></li>
      </c:if>
    </ul>
  </li>
  </sec:authorize>
  <li class="main"><a href="#" class="headerOffLink"><strong><span>More</span><small>&#9660;</small></strong></a>
   <ul class="submenu">
    <sec:authorize ifAnyGranted="ROLE_AEM,ROLE_ADMIN_${prop_site_id},ROLE_EMAILMANAGER_VIEW_HISTORY,ROLE_EMAILMANAGER_BUY_CREDIT">
     <c:if test="${gSiteConfig['gMASS_EMAIL']}">	
      <li><a <c:choose><c:when test="${param.tab == 'massEmail'}"> class="headerSelectedLink"</c:when><c:otherwise>class="headerOffLink"</c:otherwise></c:choose> href="../massEmail/" ><fmt:message key="massEmailTabTitle"/></a></li>
     </c:if>
    </sec:authorize>
    <sec:authorize ifAnyGranted="ROLE_AEM,ROLE_ADMIN_${prop_site_id}">   
     <c:if test="${gSiteConfig['gACCESS_PRIVILEGE']}">
      <li><a <c:choose><c:when test="${param.tab == 'privilege'}"> class="headerSelectedLink"</c:when><c:otherwise>class="headerOffLink"</c:otherwise></c:choose> href="../privilege/" ><fmt:message key="privilegeTabTitle"/></a></li>
     </c:if>
    </sec:authorize>
    <sec:authorize ifAnyGranted="ROLE_AEM,ROLE_ADMIN_${prop_site_id}">   
     <c:if test="${gSiteConfig['gMULTI_STORE'] > 0}">
      <li><a <c:choose><c:when test="${param.tab == 'multiStore'}"> class="headerSelectedLink"</c:when><c:otherwise>class="headerOffLink"</c:otherwise></c:choose> href="../multiStore/" ><fmt:message key="multiStore"/></a></li>
     </c:if>
    </sec:authorize>
    <sec:authorize ifAnyGranted="ROLE_AEM,ROLE_ADMIN_${prop_site_id},ROLE_STORE_LOCATER_VIEW_LIST">  
     <c:if test="${gSiteConfig['gLOCATION']}">
      <li><a href="../location/locationList.jhtm" class="headerOffLink"><fmt:message key="storeLocator"/></a></li>
     </c:if>
    </sec:authorize>
    <sec:authorize ifAnyGranted="ROLE_AEM,ROLE_ADMIN_${prop_site_id}">  
     <c:if test="${siteConfig['TECHDATA'].value != '' or 
     			   siteConfig['SYNNEX'].value != '' or 
     			   siteConfig['DSI'].value != '' or 
     			   siteConfig['INGRAM'].value != '' or
     			   siteConfig['ETILIZE'].value != '' or
     			   siteConfig['KOLE_IMPORTS'].value != '' or
     			   siteConfig['DSDI_FILE_LINK'].value != '' or   
     			   siteConfig['DATA_FEED_MEMBER_TYPE'].value == 'SUBSCRIBER' or  			   
     			   gSiteConfig['gASI'] != ''}">
      <li><a href="../vendors/" class="headerOffLink"><fmt:message key="vendors"/></a></li>
     </c:if>
    </sec:authorize>    
    <sec:authorize ifAnyGranted="ROLE_AEM,ROLE_ADMIN_${prop_site_id},ROLE_EVENT_VIEW_LIST,ROLE_EVENT_MEMBER_VIEW_LIST">   
     <c:if test="${gSiteConfig['gEVENT']}">
      <li><a href="../event/" class="headerOffLink"><fmt:message key="event"/></a></li>
     </c:if>
    </sec:authorize>
    <c:if test="${siteConfig['ECHOSIGN_API_KEY'].value != ''}">
      <li><a href="../echoSign/" class="headerOffLink">EchoSign</a></li>
    </c:if>
    </ul>
  </li>
  <li class="main last"><a href="#" class="headerOffLink"><strong><span>Language</span><small>&#9660;</small></strong></a>
  <%@ include file="/WEB-INF/jsp/admin/common/i18n.jsp" %>
  </li>
  </ul>
 
 </div>
</div>

<div id="headerbot"></div>