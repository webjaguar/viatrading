<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="/WEB-INF/tlds/spring.tld" prefix="spring" %>

<tiles:insertDefinition name="admin.service" flush="true">
  <tiles:putAttribute name="tab"  value="services" />
  <tiles:putAttribute name="content" type="string">

<script language="JavaScript"> 
<!--
window.addEvent('domready', function(){			
			var tabs1 = new SimpleTabs($('tab-block-1'), { selector: 'h4'});
		});
//--> 
</script>

<form method="post" enctype="multipart/form-data">  
<!-- main box -->
  <div id="mboxfull">
  
  <!-- start table -->
  <table cellpadding="0" cellspacing="0" border="0" class="module" >
  <tr>
    <td class="topl_g">
	  
	  <!-- breadcrumb -->
	  <p class="breadcrumb">
	    <a href="../service"><spring:message code="service"/></a> &gt;
		<a href="service.jhtm?num=${param.num}"><spring:message code="serviceRequest"/> # ${param.num}</a> &gt;
        <fmt:message key="3rdParty"/> <fmt:message key="workOrder"/> PDF 
	  </p>
	  
	  <!-- Error Message -->
	  <c:if test="${message != null}">
		  <div class="message"><spring:message code="${message}"/></div>
	  </c:if>
  
  </td><td class="topr_g" ></td></tr>
  <tr><td class="boxmidlrg" >

 <!-- tabs -->
 <div id="tab-block-1">
	<h4 title="<fmt:message key="3rdParty"/> <fmt:message key="workOrder"/> PDF"><fmt:message key="3rdParty"/> <fmt:message key="workOrder"/> PDF</h4>
	<div>
	<!-- start tab -->

        <c:set var="classIndex" value="0" />
        
	  	<div class="listdivi ln tabdivi"></div>
	  	<div class="listdivi"></div>
	   
		  	<div class="list${classIndex % 2}"><c:set var="classIndex" value="${classIndex+1}" />
		  	<div class="listfl"><fmt:message key="serviceRequest"/> #:</div>
		  	<div class="listp">
		  	<!-- input field -->
	   			<c:out value="${param.num}"/>
		    <!-- end input field -->   	
		  	</div>
		  	</div> 	   
	   
		  	<div class="list${classIndex % 2}"><c:set var="classIndex" value="${classIndex+1}" />
		  	<div class="listfl"><fmt:message key="3rdParty"/> <fmt:message key="workOrder"/> <fmt:message key="pdfFile"/>:</div>
		  	<div class="listp">
		  	<!-- input field -->
			    <c:if test="${service.wo3rdPdfUrl != null}">
				<a href="<c:url value="/assets/pdf/${service.wo3rdPdfUrl}"/>" target="_blank"><c:out value="${service.wo3rdPdfUrl}"/></a>
		    	</c:if>
		    	<c:if test="${service.wo3rdPdfUrl == null}">
				<input value="browse" type="file" name="_file"/>
				</c:if>
		    <!-- end input field -->   	
		  	</div>
		  	</div>
		  	
	<!-- end tab -->		
	</div>
<!-- end tabs -->			
</div>

<!-- start button -->
	<div align="left" class="button">
<c:choose>
  <c:when test="${service.wo3rdPdfUrl != null}">
        <input type="submit" value="<spring:message code="delete"/>" name="_delete">
  </c:when>
  <c:otherwise>
        <input type="submit" value="<spring:message code="upload"/>" name="_upload">
  </c:otherwise>
</c:choose>	
	</div>
<!-- end button -->	
    
  <!-- end table -->
    </td><td class="boxmidr" ></td></tr>
  <tr><td class="botl"></td><td class="botr"></td></tr>
  </table>
  
<!-- end main box -->  
 </div>		
</form>

  </tiles:putAttribute>    
</tiles:insertDefinition>
