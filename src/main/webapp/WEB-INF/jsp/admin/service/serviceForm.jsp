<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="/WEB-INF/tlds/spring.tld" prefix="spring" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ taglib uri="/WEB-INF/tlds/custom.tld" prefix="wj" %>
<%@ taglib uri="http://www.springframework.org/security/tags" prefix="sec" %>

<sec:authorize ifAnyGranted="ROLE_AEM,ROLE_ADMIN_${prop_site_id}">
  <c:set var="canEdit" value="true"/>
</sec:authorize>

<c:if test="${gSiteConfig['gSERVICE']}">
<tiles:insertDefinition name="admin.service" flush="true">
  <tiles:putAttribute name="tab"  value="services" />
  <tiles:putAttribute name="content" type="string">
<script language="JavaScript" type="text/JavaScript">
<!--
window.addEvent('domready', function(){			
	var tabs1 = new SimpleTabs($('tab-block-1'), { selector: 'h4'});
	var Tips1 = new Tips($$('.toolTipImg'));
});
//-->
</script>	
<link rel="stylesheet" type="text/css" media="all" href="../../admin/javascript/jscalendar-1.0/skins/aqua/theme.css" title="Aqua" /> <!-- calendar stylesheet -->
<script type="text/javascript" src="../../admin/javascript/jscalendar-1.0/calendar.js"></script> <!-- main calendar program -->
<script type="text/javascript" src="../../admin/javascript/jscalendar-1.0/lang/calendar-en.js"></script>
<script type="text/javascript" src="../../admin/javascript/jscalendar-1.0/calendar-setup.js"></script>

<!-- main box -->
  <div id="mboxfull">
<form:form commandName="serviceForm" method="post">  
  <!-- start table -->
  <table cellpadding="0" cellspacing="0" border="0" class="module" >
  <tr>
    <td class="topl_g">
	  
	  <!-- breadcrumb -->
	  <p class="breadcrumb">
	    <a href="../service">Service</a> &gt;
	     <fmt:message key='serviceRequest' /> # <c:out value="${serviceForm.service.serviceNum}"/> 
	  </p>
	  
	  <!-- Error Message -->
	  <spring:hasBindErrors name="serviceForm">
		<div class="error"><fmt:message key='form.hasErrors'/></div>
	  </spring:hasBindErrors>

	  <div align="right">
		<table class="form">
		  <tr>
		    <td align="right"><fmt:message key="reportDate" /> : </td>
		    <td><fmt:formatDate type="both" timeStyle="full" value="${serviceForm.service.reportDate}"/></td>
		  </tr>
		  <tr>
		    <td align="right"><fmt:message key="lastModified" /> : </td>
		    <td><fmt:formatDate type="both" timeStyle="full" value="${serviceForm.service.lastModified}"/></td>
		  </tr>
		</table>
	  </div>
  
  </td><td class="topr_g" ></td></tr>
  <tr><td class="boxmidlrg" >

 <!-- tabs -->
 <div id="tab-block-1">
	<h4 title="<fmt:message key='serviceRequest' />"><fmt:message key='serviceRequest' /></h4>
	<div>
	<!-- start tab -->

        <c:set var="classIndex" value="0" />
        
	  	<div class="listdivi ln tabdivi"></div>
	  	<div class="listdivi"></div>
	   
		  	<div class="list${classIndex % 2}"><c:set var="classIndex" value="${classIndex+1}" />
		  	<div class="listfl"><fmt:message key="serviceRequest" /> #:</div>
		  	<div class="listp">
		  	<!-- input field -->
	  			<b><c:out value="${serviceForm.service.serviceNum}"/></b>
		    <!-- end input field -->   	
		  	</div>
		  	</div>

			<c:if test="${serviceForm.service.item.customer.id != null}">
		  	<div class="list${classIndex % 2}"><c:set var="classIndex" value="${classIndex+1}" />
		  	<div class="listfl"><fmt:message key='company' />:</div>
		  	<div class="listp">
		  	<!-- input field -->
	  			<c:out value="${serviceForm.service.item.customer.address.company}"/>
		    <!-- end input field -->   	
		  	</div>
		  	</div>
	
		  	<div class="list${classIndex % 2}"><c:set var="classIndex" value="${classIndex+1}" />
		  	<div class="listfl"><fmt:message key='firstName' />:</div>
		  	<div class="listp">
		  	<!-- input field -->
	  			<c:out value="${serviceForm.service.item.customer.address.firstName}"/>
		    <!-- end input field -->   	
		  	</div>
		  	</div>

		  	<div class="list${classIndex % 2}"><c:set var="classIndex" value="${classIndex+1}" />
		  	<div class="listfl"><fmt:message key='lastName' />:</div>
		  	<div class="listp">
		  	<!-- input field -->
	  			<c:out value="${serviceForm.service.item.customer.address.lastName}"/>
		    <!-- end input field -->   	
		  	</div>
		  	</div>

		  	<div class="list${classIndex % 2}"><c:set var="classIndex" value="${classIndex+1}" />
		  	<div class="listfl"><fmt:message key='emailAddress' />:</div>
		  	<div class="listp">
		  	<!-- input field -->
	  			<c:out value="${serviceForm.service.item.customer.username}"/>
		    <!-- end input field -->   	
		  	</div>
		  	</div>
		  	</c:if>

		  	<div class="list${classIndex % 2}"><c:set var="classIndex" value="${classIndex+1}" />
		  	<div class="listfl"><c:out value="${siteConfig['SERVICEABLE_ITEM_TITLE'].value}"/> ID:</div>
		  	<div class="listp">
		  	<!-- input field -->
		  	  <c:choose>
		  	   <c:when test="${serviceItems == null}">
	  			<c:out value="${serviceForm.service.item.itemId}"/>
		  	   </c:when>
		  	   <c:otherwise>
	 		    <form:select path="service.item.id">
	  		      <c:forEach items="${serviceItems}" var="item">
	   			  <form:option value="${item.id}" ><c:out value="${item.itemId}"/></form:option>
	  		      </c:forEach>	    
	 		    </form:select>
		  	   </c:otherwise>
		  	  </c:choose>
		    <!-- end input field -->   	
		  	</div>
		  	</div>

		  	<div class="list${classIndex % 2}"><c:set var="classIndex" value="${classIndex+1}" />
		  	<div class="listfl"><fmt:message key="S/N" /> #:</div>
		  	<div class="listp">
		  	<!-- input field -->
	  			<c:out value="${serviceForm.service.item.serialNum}"/>&nbsp;&nbsp;[ <a href="item.jhtm?id=${serviceForm.service.item.id}"><fmt:message key='edit' /></a> ]
		    <!-- end input field -->   	
		  	</div>
		  	</div>

		  	<div class="list${classIndex % 2}"><c:set var="classIndex" value="${classIndex+1}" />
		  	<div class="listfl"><c:out value="${siteConfig['SERVICEABLE_ITEM_TITLE'].value}"/> <fmt:message key="location" />:</div>
		  	<div class="listp">
		  	<!-- input field -->
	  				<c:if test="${serviceForm.service.item.address.company != ''}">
					<c:out value="${serviceForm.service.item.address.company}"/><br/>
					</c:if>
					<c:out value="${serviceForm.service.item.address.firstName}"/> <c:out value="${serviceForm.service.item.address.lastName}"/><br/>
					<c:out value="${serviceForm.service.item.address.addr1}"/><br/> 
					<c:if test="${serviceForm.service.item.address.addr2 != ''}">
					<c:out value="${serviceForm.service.item.address.addr2}"/><br/>
					</c:if>
					<c:out value="${serviceForm.service.item.address.city}"/>, <c:out value="${serviceForm.service.item.address.stateProvince}"/> <c:out value="${serviceForm.service.item.address.zip}"/><br/>
					<c:out value="${countries[serviceForm.service.item.address.country]}"/><br/>
					<c:if test="${serviceForm.service.item.address.phone != ''}">
					Tel: <c:out value="${serviceForm.service.item.address.phone}"/><br/>
					</c:if>
					<c:if test="${serviceForm.service.item.address.fax != ''}">
					Fax: <c:out value="${serviceForm.service.item.address.fax}"/><br/>
					</c:if>
					<c:if test="${serviceForm.service.item.address.cellPhone != ''}">
					Cell: <c:out value="${serviceForm.service.item.address.cellPhone}"/><br/>
					</c:if>
					<c:if test="${serviceForm.service.item.address.email != ''}">
					<fmt:message key="email" />: <c:out value="${serviceForm.service.item.address.email}"/><br/>
					</c:if>
		    <!-- end input field -->   	
		  	</div>
		  	</div>

			<c:if test="${alternateContacts != null or not empty serviceForm.service.alternateContact}">
		  	<div class="list${classIndex % 2}"><c:set var="classIndex" value="${classIndex+1}" />
		  	<div class="listfl"><fmt:message key="alternateContact" />:</div>
		  	<div class="listp">
		  	<!-- input field -->
			    <c:if test="${alternateContacts != null}">
			      <form:select path="service.alternateContact">
			       <form:option value="" label="Please Select"/>
			       <form:options items="${alternateContacts}"/>
			      </form:select>
			      <form:errors path="service.alternateContact" cssClass="error" />     
			    </c:if>		  	
			    <c:if test="${alternateContacts == null}">
			  	<c:choose>
			  	<c:when test="${fn:toLowerCase(serviceForm.service.status) != 'completed' or canEdit}">
	  			  <form:input path="service.alternateContact" size="50" maxlength="255" htmlEscape="true"/>
			  	</c:when>
			  	<c:otherwise>
			  	  <c:out value="${serviceForm.service.alternateContact}"/>
			    </c:otherwise>
			    </c:choose>			    
	  			</c:if>
		    <!-- end input field -->   	
		  	</div>
		  	</div>
			</c:if>
				
		  	<div class="list${classIndex % 2}"><c:set var="classIndex" value="${classIndex+1}" />
		  	<div class="listfl"><fmt:message key="sku" />:</div>
		  	<div class="listp">
		  	<!-- input field -->
	  			<c:out value="${serviceForm.service.item.sku}"/>
		    <!-- end input field -->   	
		  	</div>
		  	</div>

		  	<div class="list${classIndex % 2}"><c:set var="classIndex" value="${classIndex+1}" />
		  	<div class="listfl"><fmt:message key="description" />:</div>
		  	<div class="listp">
		  	<!-- input field -->
	  			<c:out value="${product.name}"/>
		    <!-- end input field -->   	
		  	</div>
		  	</div>

			<c:forEach items="${product.productFields}" var="productField" varStatus="status">
		  	<div class="list${classIndex % 2}"><c:set var="classIndex" value="${classIndex+1}" />
		  	<div class="listfl"><c:out value="${productField.name}"/>:</div>
		  	<div class="listp">
		  	<!-- input field -->
	  			<c:out value="${productField.value}"/>
		    <!-- end input field -->   	
		  	</div>
		  	</div>
			</c:forEach>
		
		  	<div class="list${classIndex % 2}"><c:set var="classIndex" value="${classIndex+1}" />
		  	<div class="listfl"><fmt:message key="status" />:</div>
		  	<div class="listp"><c:out value="${serviceForm.service.status}"/>
		  	<!-- input field -->
		  	<%--  this should be changed on the work order form
	  			<form:select path="service.status">
		          <form:option value="pending">pending</form:option>
		          <form:option value="processing">processing</form:option>
		          <form:option value="parts ordered">parts ordered</form:option>
		          <form:option value="completed">completed</form:option>
		        </form:select>
		  	--%>
		    <!-- end input field -->   	
		  	</div>
		  	</div>

		  	<div class="list${classIndex % 2}"><c:set var="classIndex" value="${classIndex+1}" />
		  	<div class="listfl"><fmt:message key="completeDate" />:</div>
		  	<div class="listp"><fmt:formatDate type="date" timeStyle="default" value="${serviceForm.service.completeDate}"/>
		  	<!-- input field -->
		  	<%--  this should be dynamic upon invoice generation
	  			<form:input path="service.completeDate" size="11" maxlength="10" />
				  <img class="calendarImage"  id="complete_date_trigger" src="../graphics/calendarIcon.jpg" />
				  <script type="text/javascript">
		    		Calendar.setup({
		        			inputField     :    "service.completeDate",   // id of the input field
		        			showsTime      :    false,
		        			ifFormat       :    "%m/%d/%Y",   // format of the input field
		        			button         :    "complete_date_trigger"   // trigger for the calendar (button ID)
		    		});	
				  </script> 
				<form:errors path="service.completeDate" cssClass="error"/>
		  	--%>
		    <!-- end input field -->   	
		  	</div>
		  	</div>
        
		  	<div class="list${classIndex % 2}"><c:set var="classIndex" value="${classIndex+1}" />
		  	<div class="listfl"><fmt:message key="service"/> <fmt:message key="type" />:</div>
		  	<div class="listp">
		  	<c:choose>
		  	<c:when test="${fn:toLowerCase(serviceForm.service.status) == 'completed'}">
		  	  <c:out value="${serviceForm.service.type}"/>
		  	</c:when>
		  	<c:otherwise>
		  	<!-- input field -->
	  			<form:select path="service.type">
		          <form:option value="on site">on site</form:option>
		          <form:option value="in house">in house</form:option>
		          <form:option value="CPP">CPP</form:option>
		          <form:option value="Warranty">Warranty</form:option>
		        </form:select>
		    <!-- end input field -->
		    </c:otherwise>
		    </c:choose>   	
		  	</div>
		  	</div>

		  	<div class="list${classIndex % 2}"><c:set var="classIndex" value="${classIndex+1}" />
		  	<div class="listfl"><fmt:message key="problem" />:</div>
		  	<div class="listp">
		  	<c:choose>
		  	<c:when test="${fn:toLowerCase(serviceForm.service.status) == 'completed'}">
		  	  <c:out value="${serviceForm.service.problem}"/>
		  	</c:when>
		  	<c:otherwise>
		  	<!-- input field -->
	  			<form:input path="service.problem" size="50" maxlength="255" htmlEscape="true"/>
		    <!-- end input field -->   	
		    </c:otherwise>
		    </c:choose>
		  	</div>
		  	</div>

		  	<div class="list${classIndex % 2}"><c:set var="classIndex" value="${classIndex+1}" />
		  	<div class="listfl"><fmt:message key="comments" />:</div>
		  	<div class="listp">
		  	<c:choose>
		  	<c:when test="${fn:toLowerCase(serviceForm.service.status) == 'completed'}">
		  	    <c:out value="${wj:newLineToBreakLine(serviceForm.service.comments)}" escapeXml="false"/>
		  	</c:when>
		  	<c:otherwise>
		  	<!-- input field -->
	  			<form:textarea rows="8" cols="60" path="service.comments" htmlEscape="true" />
		    <!-- end input field -->  
		    </c:otherwise>
		    </c:choose> 	
		  	</div>
		  	</div>
		  	
		  	<div class="list${classIndex % 2}"><c:set var="classIndex" value="${classIndex+1}" />
		  	<div class="listfl"><fmt:message key="notes" /> (<fmt:message key="internal" />):</div>
		  	<div class="listp">
		  	<c:choose>
		  	<c:when test="${fn:toLowerCase(serviceForm.service.status) == 'completed'}">
		  	     <c:out value="${wj:newLineToBreakLine(serviceForm.service.notes)}" escapeXml="false"/>
		  	</c:when>
		  	<c:otherwise>
		  	<!-- input field -->
	  			<form:textarea rows="8" cols="60" path="service.notes" htmlEscape="true"  readonly="${not canEdit}"/>
	  			<br/>
	  			<form:input path="service.notesTemp" size="50" maxlength="255" htmlEscape="true"/>
		    <!-- end input field -->  
		    </c:otherwise>
		    </c:choose> 	
		  	</div>
		  	</div>		  	
		  	
		  	<div class="list${classIndex % 2}"><c:set var="classIndex" value="${classIndex+1}" />
		  	<div class="listfl"><fmt:message key='priority' />:</div>
		  	<div class="listp">
		  	<!-- input field -->
		  	    <c:forEach begin="0" end="2" var="priority">
	  			<form:radiobutton value="${priority}" path="service.priority" /><img src="../graphics/priority_${priority}.png" border="0">&nbsp;&nbsp;&nbsp;
		  	    </c:forEach>
		    <!-- end input field -->   	
		  	</div>
		  	</div>		  
  	
 		  	<div class="list${classIndex % 2}"><c:set var="classIndex" value="${classIndex+1}" />
		  	<div class="listfl">Inbound <fmt:message key="trackNum" />:</div>
		  	<div class="listp">
		  	<!-- input field -->
				<form:select path="service.trackNumInCarrier">
				  <form:option value="">Choose a carrier</form:option>
				  <form:option value="UPS">UPS</form:option>
				  <form:option value="FedEx">FedEx</form:option>
				  <form:option value="USPS">USPS</form:option>
				  <form:option value="Trucks">Trucks</form:option>
				  <form:option value="Others">Others</form:option>
				</form:select>
			  	<c:choose>
			  	<c:when test="${fn:toLowerCase(serviceForm.service.status) != 'completed' or canEdit}">
			  	  <form:input path="service.trackNumIn" maxlength="50" htmlEscape="true"/>
			  	</c:when>
			  	<c:otherwise>
			  	  <c:out value="${serviceForm.service.trackNumIn}"/>
			    </c:otherwise>
			    </c:choose>				
		    <!-- end input field -->
		  	</div>
		  	</div> 	
  	
  		  	<div class="list${classIndex % 2}"><c:set var="classIndex" value="${classIndex+1}" />
		  	<div class="listfl"><fmt:message key="purchaseOrder" />:</div>
		  	<div class="listp">
		  	<!-- input field -->
			  	<c:choose>
			  	<c:when test="${fn:toLowerCase(serviceForm.service.status) != 'completed' or canEdit}">
			  	  <form:input path="service.purchaseOrder" maxlength="50" htmlEscape="true"/>
			  	</c:when>
			  	<c:otherwise>
			  	  <c:out value="${serviceForm.service.purchaseOrder}"/>
			    </c:otherwise>
			    </c:choose>				
			    &nbsp;&nbsp;<a href="pdfPO.jhtm?num=${serviceForm.service.serviceNum}"/><img src="../graphics/pdf<c:if test="${serviceForm.service.purchaseOrderPdfUrl == null}">_light</c:if>.gif" border="0"></a>	
		    <!-- end input field -->
		  	</div>
		  	</div>  	
  	
  		  	<div class="list${classIndex % 2}"><c:set var="classIndex" value="${classIndex+1}" />
		  	<div class="listfl"><fmt:message key="3rdParty"/> <fmt:message key="workOrder"/>:</div>
		  	<div class="listp">
		  	<!-- input field -->
			  	<c:choose>
			  	<c:when test="${fn:toLowerCase(serviceForm.service.status) != 'completed' or canEdit}">
			  	  <form:input path="service.wo3rd" maxlength="50" htmlEscape="true"/>
			  	</c:when>
			  	<c:otherwise>
			  	  <c:out value="${serviceForm.service.wo3rd}"/>
			    </c:otherwise>
			    </c:choose>				
			    &nbsp;&nbsp;<a href="pdfWO3rd.jhtm?num=${serviceForm.service.serviceNum}"/><img src="../graphics/pdf<c:if test="${serviceForm.service.wo3rdPdfUrl == null}">_light</c:if>.gif" border="0"></a>	
		    <!-- end input field -->
		  	</div>
		  	</div>  	

  		  	<div class="list${classIndex % 2}"><c:set var="classIndex" value="${classIndex+1}" />
		  	<div class="listfl"><fmt:message key="supplier" /> <fmt:message key="purchaseOrder" />:</div>
		  	<div class="listp">
		  	<!-- input field -->
			  	<c:choose>
			  	<c:when test="${fn:toLowerCase(serviceForm.service.status) != 'completed' or canEdit}">
			  	  <form:input path="service.poSupplier" maxlength="50" htmlEscape="true"/>
			  	</c:when>
			  	<c:otherwise>
			  	  <c:out value="${serviceForm.service.poSupplier}"/>
			    </c:otherwise>
			    </c:choose>				
		    <!-- end input field -->
		  	</div>
		  	</div>		

  		  	<div class="list${classIndex % 2}"><c:set var="classIndex" value="${classIndex+1}" />
		  	<div class="listfl">ETA/Notes/SN:</div>
		  	<div class="listp">
		  	<!-- input field -->
			  	<c:choose>
			  	<c:when test="${fn:toLowerCase(serviceForm.service.status) != 'completed' or canEdit}">
			  	  <form:input path="service.notes2" maxlength="50" htmlEscape="true"/>
			  	</c:when>
			  	<c:otherwise>
			  	  <c:out value="${serviceForm.service.notes2}"/>
			    </c:otherwise>
			    </c:choose>				
		    <!-- end input field -->
		  	</div>
		  	</div>

  		  	<div class="list${classIndex % 2}"><c:set var="classIndex" value="${classIndex+1}" />
		  	<div class="listfl">Reference #:</div>
		  	<div class="listp">
		  	<!-- input field -->
			  	<c:choose>
			  	<c:when test="${fn:toLowerCase(serviceForm.service.status) != 'completed' or canEdit}">
			  	  <form:input path="service.refNum" maxlength="50" htmlEscape="true"/>
			  	</c:when>
			  	<c:otherwise>
			  	  <c:out value="${serviceForm.service.refNum}"/>
			    </c:otherwise>
			    </c:choose>				
		    <!-- end input field -->
		  	</div>
		  	</div>  		  		  	

  		  	<div class="list${classIndex % 2}"><c:set var="classIndex" value="${classIndex+1}" />
		  	<div class="listfl">Mfg. Warranty Ticket #:</div>
		  	<div class="listp">
		  	<!-- input field -->
			  	<c:choose>
			  	<c:when test="${fn:toLowerCase(serviceForm.service.status) != 'completed' or canEdit}">
			  	  <form:input path="service.manufWarranty" maxlength="50" htmlEscape="true"/>
			  	</c:when>
			  	<c:otherwise>
			  	  <c:out value="${serviceForm.service.manufWarranty}"/>
			    </c:otherwise>
			    </c:choose>				
		    <!-- end input field -->
		  	</div>
		  	</div>  
  	
	<!-- end tab -->        
	</div> 
	
  <h4 title="<fmt:message key="serviceHistory" />"><fmt:message key="serviceHistory" /></h4>
	<div>
	<!-- start tab -->
	  	<div class="listdivi ln tabdivi"></div>
	  	<div class="listdivi"></div>
	   
		  	<div class="list${classIndex % 2}"><c:set var="classIndex" value="${classIndex+1}" />
		  	<div class="listpp">

<iframe src="serviceHistory.jsp?id=${serviceForm.service.item.id}" width="800" height="500" frameborder="0"></iframe>

		  	</div>
		  	</div>

	<!-- end tab -->        
	</div>	
	
<!-- end tabs -->			
</div>

<!-- start button -->
<c:if test="${fn:toLowerCase(serviceForm.service.status) != 'completed' or canEdit}">
<div align="left" class="button"> 
  <input type="submit" value="<fmt:message key="update"/>">
  <c:if test="${not serviceForm.service.hasWorkOrder}"><input type="submit" value="<fmt:message key="updateAndGenerate"/> <fmt:message key="workOrder"/>" name="_workorder"></c:if>
  <input type="submit" value="<fmt:message key="cancel"/>" name="_cancel">
</div>
</c:if>
<!-- end button -->	     
  
  <!-- end table -->
    </td><td class="boxmidr" ></td></tr>
  <tr><td class="botl"></td><td class="botr"></td></tr>
  </table>
  
</form:form>
<!-- end main box -->  
</div>

 
  </tiles:putAttribute>    
</tiles:insertDefinition>
</c:if>