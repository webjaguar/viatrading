<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>

<c:if test="${gSiteConfig['gSERVICE']}">
<tiles:insertDefinition name="admin.service" flush="true">
  <tiles:putAttribute name="tab"  value="items" />
  <tiles:putAttribute name="content" type="string">
    
<form action="items.jhtm" id="list" method="post">
<input type="hidden" id="sort" name="sort" value="${serviceableItemSearch.sort}">
  <!-- main box -->
  <div id="mboxfull">
  
  <!-- start table -->
  <table cellpadding="0" cellspacing="0" border="0" class="module" >
  <tr>
    <td class="topl_g">
	  
	  <!-- breadcrumb -->
	  <p class="breadcrumb">
	    <a href="../service">Service</a> &gt;
	    <c:out value="${siteConfig['SERVICEABLE_ITEM_TITLE'].value}"/>
	  </p>
	  
	  <c:if test="${not empty model.message}">
		 <div class="message"><fmt:message key="${model.message}"/></div>
	  </c:if>
	  	  
	  </td><td class="topr_g" ></td></tr>
  <tr><td class="boxmidlrg" >

 <!-- tabs -->
 <div class="tab-wrapper">
	<h4 title=""></h4>
	<div>
	<!-- start tab -->

	  	<div class="listdivi"></div>
	   
		  	<div class="listlight">
		  	<!-- input field --> 
			<table border="0" cellpadding="0" cellspacing="1" width="100%">
			  <tr>
			  <td class="pageShowing">
			  <c:if test="${model.list.nrOfElements > 0}">
			  <fmt:message key="showing">
				<fmt:param value="${model.list.firstElementOnPage + 1}"/>
				<fmt:param value="${model.list.lastElementOnPage + 1}"/>
				<fmt:param value="${model.list.nrOfElements}"/>
			  </fmt:message>
			  </c:if>
			  </td>
			  <td class="pageNavi">
			  Page 
			  <select name="page" id="page" onchange="submit()">
			  <c:forEach begin="1" end="${model.list.pageCount}" var="page">
			  	<option value="${page}" <c:if test="${page == (model.list.page+1)}">selected</c:if>>${page}</option>
			  </c:forEach>
			  </select>
			  of <c:out value="${model.list.pageCount}"/>
			  | 
			  <c:if test="${model.list.firstPage}"><span class="pageNaviDead"><fmt:message key="previous" /></span></c:if>
			  <c:if test="${not model.list.firstPage}"><a href="<c:url value="items.jhtm"><c:param name="page" value="${model.list.page}"/></c:url>" class="pageNaviLink"><fmt:message key="previous" /></a></c:if>
			  | 
			  <c:if test="${model.list.lastPage}"><span class="pageNaviDead"><fmt:message key="next" /></span></c:if>
			  <c:if test="${not model.list.lastPage}"><a href="<c:url value="items.jhtm"><c:param name="page" value="${model.list.page+2}"/></c:url>" class="pageNaviLink"><fmt:message key="next" /></a></c:if>
			  </td>
			  </tr>
			</table>
			
			<table border="0" cellpadding="0" cellspacing="0" width="100%" class="listings">
			  <tr class="listingsHdr2">
			    <td class="indexCol">&nbsp;</td>
				<td class="nameCol">
			    	<table cellspacing="0" cellpadding="1">
			    	  <tr>
			    	    <c:choose>
			    	      <c:when test="${serviceableItemSearch.sort == 'item_id DESC'}">
			    	        <td class="listingsHdr3">
			    	        <a class="hdrLink" href="#" onClick="document.getElementById('sort').value='item_id';document.getElementById('list').submit()"><c:out value="${siteConfig['SERVICEABLE_ITEM_TITLE'].value}"/> ID</a>
			    	        </td>
			    	        <td><img src="../graphics/up.gif" border="0"></td>
			    	      </c:when>
			    	      <c:when test="${serviceableItemSearch.sort == 'item_id'}">
			    	        <td class="listingsHdr3">
			    	        <a class="hdrLink" href="#" onClick="document.getElementById('sort').value='item_id DESC';document.getElementById('list').submit()"><c:out value="${siteConfig['SERVICEABLE_ITEM_TITLE'].value}"/> ID</a>
			    	        </td>
			    	        <td><img src="../graphics/down.gif" border="0"></td>
			    	      </c:when>
			    	      <c:otherwise>
			    	        <td colspan="2" class="listingsHdr3">
			    	        <a class="hdrLink" href="#" onClick="document.getElementById('sort').value='item_id DESC';document.getElementById('list').submit()"><c:out value="${siteConfig['SERVICEABLE_ITEM_TITLE'].value}"/> ID</a>
			    	        </td>
			    	      </c:otherwise>
			    	    </c:choose>  
			    	  </tr>
			    	  </table>
				</td>
				<td align="center">
			    	<table cellspacing="0" cellpadding="1">
			    	  <tr>
			    	    <c:choose>
			    	      <c:when test="${serviceableItemSearch.sort == 'serial_num DESC'}">
			    	        <td class="listingsHdr3">
			    	        <a class="hdrLink" href="#" onClick="document.getElementById('sort').value='serial_num';document.getElementById('list').submit()"><fmt:message key="S/N" /></a>
			    	        </td>
			    	        <td><img src="../graphics/up.gif" border="0"></td>
			    	      </c:when>
			    	      <c:when test="${serviceableItemSearch.sort == 'serial_num'}">
			    	        <td class="listingsHdr3">
			    	        <a class="hdrLink" href="#" onClick="document.getElementById('sort').value='serial_num DESC';document.getElementById('list').submit()"><fmt:message key="S/N" /></a>
			    	        </td>
			    	        <td><img src="../graphics/down.gif" border="0"></td>
			    	      </c:when>
			    	      <c:otherwise>
			    	        <td colspan="2" class="listingsHdr3">
			    	        <a class="hdrLink" href="#" onClick="document.getElementById('sort').value='serial_num DESC';document.getElementById('list').submit()"><fmt:message key="S/N" /></a>
			    	        </td>
			    	      </c:otherwise>
			    	    </c:choose>  
			    	  </tr>
			    	  </table>
				</td>
			    <td align="center">
			    	<table cellspacing="0" cellpadding="1">
			    	  <tr>
			    	    <c:choose>
			    	      <c:when test="${serviceableItemSearch.sort == 'sku DESC'}">
			    	        <td class="listingsHdr3">
			    	        <a class="hdrLink" href="#" onClick="document.getElementById('sort').value='sku';document.getElementById('list').submit()"><fmt:message key="sku" /></a>
			    	        </td>
			    	        <td><img src="../graphics/up.gif" border="0"></td>
			    	      </c:when>
			    	      <c:when test="${serviceableItemSearch.sort == 'sku'}">
			    	        <td class="listingsHdr3">
			    	        <a class="hdrLink" href="#" onClick="document.getElementById('sort').value='sku DESC';document.getElementById('list').submit()"><fmt:message key="sku" /></a>
			    	        </td>
			    	        <td><img src="../graphics/down.gif" border="0"></td>
			    	      </c:when>
			    	      <c:otherwise>
			    	        <td colspan="2" class="listingsHdr3">
			    	        <a class="hdrLink" href="#" onClick="document.getElementById('sort').value='sku DESC';document.getElementById('list').submit()"><fmt:message key="sku" /></a>
			    	        </td>
			    	      </c:otherwise>
			    	    </c:choose>  
			    	  </tr>
			    	  </table>
			    </td>
			    <td class="listingsHdr3" align="center"><fmt:message key="active" /></td>				
			    <td>
			    	<table cellspacing="0" cellpadding="1">
			    	  <tr>
			    	    <c:choose>
			    	      <c:when test="${serviceableItemSearch.sort == 'username DESC'}">
			    	        <td class="listingsHdr3">
			    	        <a class="hdrLink" href="#" onClick="document.getElementById('sort').value='username';document.getElementById('list').submit()"><fmt:message key="emailAddress" /></a>
			    	        </td>
			    	        <td><img src="../graphics/up.gif" border="0"></td>
			    	      </c:when>
			    	      <c:when test="${serviceableItemSearch.sort == 'username'}">
			    	        <td class="listingsHdr3">
			    	        <a class="hdrLink" href="#" onClick="document.getElementById('sort').value='username DESC';document.getElementById('list').submit()"><fmt:message key="emailAddress" /></a>
			    	        </td>
			    	        <td><img src="../graphics/down.gif" border="0"></td>
			    	      </c:when>
			    	      <c:otherwise>
			    	        <td colspan="2" class="listingsHdr3">
			    	        <a class="hdrLink" href="#" onClick="document.getElementById('sort').value='username DESC';document.getElementById('list').submit()"><fmt:message key="emailAddress" /></a>
			    	        </td>
			    	      </c:otherwise>
			    	    </c:choose>  
			    	  </tr>
			    	  </table>
			    </td>    
			    <td>
			    	<table cellspacing="0" cellpadding="1">
			    	  <tr>
			    	    <c:choose>
			    	      <c:when test="${serviceableItemSearch.sort == 'company DESC'}">
			    	        <td class="listingsHdr3">
			    	        <a class="hdrLink" href="#" onClick="document.getElementById('sort').value='company';document.getElementById('list').submit()"><fmt:message key="company" /></a>
			    	        </td>
			    	        <td><img src="../graphics/up.gif" border="0"></td>
			    	      </c:when>
			    	      <c:when test="${serviceableItemSearch.sort == 'company'}">
			    	        <td class="listingsHdr3">
			    	        <a class="hdrLink" href="#" onClick="document.getElementById('sort').value='company DESC';document.getElementById('list').submit()"><fmt:message key="company" /></a>
			    	        </td>
			    	        <td><img src="../graphics/down.gif" border="0"></td>
			    	      </c:when>
			    	      <c:otherwise>
			    	        <td colspan="2" class="listingsHdr3">
			    	        <a class="hdrLink" href="#" onClick="document.getElementById('sort').value='company DESC';document.getElementById('list').submit()"><fmt:message key="company" /></a>
			    	        </td>
			    	      </c:otherwise>
			    	    </c:choose>  
			    	  </tr>
			    	  </table>
			    </td>
			    <td>
			    	<table cellspacing="0" cellpadding="1">
			    	  <tr>
			    	    <c:choose>
			    	      <c:when test="${serviceableItemSearch.sort == 'last_name DESC, first_name DESC'}">
			    	        <td class="listingsHdr3">
			    	        <a class="hdrLink" href="#" onClick="document.getElementById('sort').value='last_name, first_name';document.getElementById('list').submit()"><fmt:message key="lastName" /></a>
			    	        </td>
			    	        <td><img src="../graphics/up.gif" border="0"></td>
			    	      </c:when>
			    	      <c:when test="${serviceableItemSearch.sort == 'last_name, first_name'}">
			    	        <td class="listingsHdr3">
			    	        <a class="hdrLink" href="#" onClick="document.getElementById('sort').value='last_name DESC, first_name DESC';document.getElementById('list').submit()"><fmt:message key="lastName" /></a>
			    	        </td>
			    	        <td><img src="../graphics/down.gif" border="0"></td>
			    	      </c:when>
			    	      <c:otherwise>
			    	        <td colspan="2" class="listingsHdr3">
			    	        <a class="hdrLink" href="#" onClick="document.getElementById('sort').value='last_name DESC, first_name DESC';document.getElementById('list').submit()"><fmt:message key="lastName" /></a>
			    	        </td>
			    	      </c:otherwise>
			    	    </c:choose>  
			    	  </tr>
			    	  </table>
			    </td>
			    <td>
			    	<table cellspacing="0" cellpadding="1">
			    	  <tr>
			    	    <c:choose>
			    	      <c:when test="${serviceableItemSearch.sort == 'first_name DESC, last_name DESC'}">
			    	        <td class="listingsHdr3">
			    	        <a class="hdrLink" href="#" onClick="document.getElementById('sort').value='first_name, last_name';document.getElementById('list').submit()"><fmt:message key="firstName" /></a>
			    	        </td>
			    	        <td><img src="../graphics/up.gif" border="0"></td>
			    	      </c:when>
			    	      <c:when test="${serviceableItemSearch.sort == 'first_name, last_name'}">
			    	        <td class="listingsHdr3">
			    	        <a class="hdrLink" href="#" onClick="document.getElementById('sort').value='first_name DESC, last_name DESC';document.getElementById('list').submit()"><fmt:message key="firstName" /></a>
			    	        </td>
			    	        <td><img src="../graphics/down.gif" border="0"></td>
			    	      </c:when>
			    	      <c:otherwise>
			    	        <td colspan="2" class="listingsHdr3">
			    	        <a class="hdrLink" href="#" onClick="document.getElementById('sort').value='first_name DESC, last_name DESC';document.getElementById('list').submit()"><fmt:message key="firstName" /></a>
			    	        </td>
			    	      </c:otherwise>
			    	    </c:choose>  
			    	  </tr>
			    	  </table>
			    </td>
			  </tr>
			<c:forEach items="${model.list.pageList}" var="item" varStatus="status">
			  <tr class="row${status.index % 2}" onmouseover="changeStyleClass(this,'rowOver')" onmouseout="changeStyleClass(this,'row${status.index % 2}')">
			    <td class="indexCol"><c:out value="${status.count + model.list.firstElementOnPage}"/>.</td>
			    <td class="nameCol"><a href="item.jhtm?id=${item.id}" class="nameLink"><c:out value="${item.itemId}"/></a></td>
			    <td align="center"><a href="item.jhtm?id=${item.id}" class="nameLink"><c:out value="${item.serialNum}"/></a></td>
			    <td align="center"><c:out value="${item.sku}"/></td>
			    <td align="center"><input type="hidden" name="item_ids" value="<c:out value="${item.id}"/>"><input name="__active_${item.id}" type="checkbox" <c:if test="${item.active}">checked</c:if>></td>
			    <td class="nameCol"><c:out value="${item.customer.username}"/></td>
			    <td class="nameCol"><c:out value="${item.address.company}"/></td>
			    <td class="nameCol"><c:out value="${item.address.lastName}"/></td>
			    <td class="nameCol"><c:out value="${item.address.firstName}"/></td>  
			  </tr>
			</c:forEach>
			<c:if test="${model.list.nrOfElements == 0}">
			  <tr class="emptyList"><td colspan="9">&nbsp;</td></tr>
			</c:if>
			</table>
				  
			<table border="0" cellpadding="0" cellspacing="1" width="100%">
			  <tr>
			  <td class="pageSize">
			  <select name="size" onchange="document.getElementById('page').value=1;submit()">
			    <c:forTokens items="10,25,50,100" delims="," var="current">
			  	  <option value="${current}" <c:if test="${current == model.list.pageSize}">selected</c:if>>${current} <fmt:message key="perPage" /></option>
			    </c:forTokens>
			  </select>
			  </td>
			  </tr>
			</table>	  
		  	<!-- end input field -->  	  
		  	</div>
		  	
		  	<!-- start button --> 
		  	<div align="left" class="button"> 
  			  <input type="submit" name="__add" value="<fmt:message key="add" /> <c:out value="${siteConfig['SERVICEABLE_ITEM_TITLE'].value}"/>">
  			  <c:if test="${model.list.nrOfElements > 0}">
  			  <input type="submit" name="__update" value="<fmt:message key="update" /> <c:out value="${siteConfig['SERVICEABLE_ITEM_TITLE'].value}"/>">
  			  </c:if>
			</div>
		  	<!-- end button -->	
	  	
	<!-- end tab -->        
	</div>
<!-- end tabs -->			
</div>
  
  <!-- end table -->
   </td><td class="boxmidr" ></td></tr>
  <tr><td class="botl"></td><td class="botr"></td></tr>
  </table>
  
<!-- end main box -->  
</div>
</form>  	  

  </tiles:putAttribute>    
</tiles:insertDefinition>
</c:if>
