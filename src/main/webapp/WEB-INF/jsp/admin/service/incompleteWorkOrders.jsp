<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="/WEB-INF/tlds/custom.tld" prefix="wj" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>

<html>
<head>
<title><fmt:message key="incompleteWorkOrders" /></title>
<script type="text/javascript" src="../javascript/javascript.js"></script>
<style type="text/css">
<!--
.pageShowing {
	color: #666666;
	font-size:10px;
	font-weight: bold;
	font-family:'Verdana','Geneva',sans-serif;	
}
.pageNavi {
	text-align: right;	
	color: #666666;
	font-size:10px;
	font-weight: bold;
	padding-bottom: 5px;	
	font-family:'Verdana','Geneva',sans-serif;
}
.pageNaviDead {
	color: #999999;
}
.pageNaviLink {
    color: #FF6633;
	text-decoration: underline
}
.pageSize {
	padding-top: 5px;
	text-align: right;	
}
.nameLink {
	color: #003366;
	text-decoration: none;
}	
.nameLink:hover {
	text-decoration: underline;
}
.listings {
	font-size: 11px;
	margin:0;
	padding:0;
	font-family: Verdana, Arial, Helvetica, sans-serif;
	color: #000000;
	height:5px;
}
.indexCol {
	color: #666666;
	text-align: right;
	margin-left: 5px;
	margin-right: 5px;
}
.listingsHdr2 {
	color: #000000;
	font-weight: bold;
	background: url(../graphics/list_bg.gif) repeat-x ;
	line-height:20px;
	height:25px;
}
.listingsHdr3 {
	font-size: 11px;
	color: #FFFFFF;
	font-weight: bold;
	padding-right: 5px;
	line-height:11px;
	white-space:nowrap !important;
}
.nameCol {
	padding-left: 10px;
}
.row0 {
	background: #FFFFFF}
.row1 {
	background: #fff9f9;}
.rowOver {
	background: #FFFFCC}
//-->
</style>
</head> 
<body>

<form action="incompleteWorkOrders.jhtm" id="list" method="post">
<input type="hidden" id="sort" name="sort" value="${serviceSearch.sort}">
	   
			<table border="0" cellpadding="0" cellspacing="1" width="800">
			  <tr>
			  <td class="pageShowing">
			  <c:if test="${model.list.nrOfElements > 0}">
			  <fmt:message key="showing">
				<fmt:param value="${model.list.firstElementOnPage + 1}"/>
				<fmt:param value="${model.list.lastElementOnPage + 1}"/>
				<fmt:param value="${model.list.nrOfElements}"/>
			  </fmt:message>
			  </c:if>
			  </td>
			  <td class="pageNavi">
			  Page 
			  <select name="page" id="page" onchange="submit()">
			  <c:forEach begin="1" end="${model.list.pageCount}" var="page">
			  	<option value="${page}" <c:if test="${page == (model.list.page+1)}">selected</c:if>>${page}</option>
			  </c:forEach>
			  </select>
			  of <c:out value="${model.list.pageCount}"/>
			  | 
			  <c:if test="${model.list.firstPage}"><span class="pageNaviDead"><fmt:message key="previous" /></span></c:if>
			  <c:if test="${not model.list.firstPage}"><a href="<c:url value="incompleteWorkOrders.jhtm"><c:param name="page" value="${model.list.page}"/></c:url>" class="pageNaviLink"><fmt:message key="previous" /></a></c:if>
			  | 
			  <c:if test="${model.list.lastPage}"><span class="pageNaviDead"><fmt:message key="next" /></span></c:if>
			  <c:if test="${not model.list.lastPage}"><a href="<c:url value="incompleteWorkOrders.jhtm"><c:param name="page" value="${model.list.page+2}"/></c:url>" class="pageNaviLink"><fmt:message key="next" /></a></c:if>
			  </td>
			  </tr>
			</table>
			
			<table border="0" cellpadding="0" cellspacing="0" class="listings" width="800">
			  <tr class="listingsHdr2">
			    <td class="indexCol" width="25">&nbsp;</td>
				<td class="listingsHdr3 nameCol" width="50">WO #</td>
				<td class="listingsHdr3" align="center" width="80">WO <fmt:message key="date" /></td>
			    <td class="listingsHdr3" align="center" width="200"><c:out value="${siteConfig['SERVICEABLE_ITEM_TITLE'].value}"/> <fmt:message key="location" /></td>
				<td class="listingsHdr3" align="center" width="70"><fmt:message key="status" /></td>
				<td class="listingsHdr3 nameCol" width="*"><fmt:message key="notes" /> (<fmt:message key="internal" />)</td>
			  </tr>
			<c:forEach items="${model.list.pageList}" var="workOrder" varStatus="status">
			  <tr class="row${status.index % 2}" onmouseover="changeStyleClass(this,'rowOver')" onmouseout="changeStyleClass(this,'row${status.index % 2}')">
			    <td class="indexCol" valign="top"><c:out value="${status.count + model.list.firstElementOnPage}"/>.</td>
			    <td class="nameCol" valign="top"><a href="workOrder.jhtm?num=${workOrder.service.serviceNum}" class="nameLink" target="_parent"><c:out value="${workOrder.service.serviceNum}"/></a></td>
			    <td align="center" valign="top"><fmt:formatDate type="date" timeStyle="default" value="${workOrder.date}"/></td>
			    <td valign="top" class="nameCol">
<c:if test="${workOrder.service.item.address.company != ''}">
<c:out value="${workOrder.service.item.address.company}"/><br/>
</c:if>
<c:out value="${workOrder.service.item.address.firstName}"/> <c:out value="${workOrder.service.item.address.lastName}"/><br/>
<c:out value="${workOrder.service.item.address.addr1}"/> <c:out value="${workOrder.service.item.address.addr2}"/><br/>
<c:out value="${workOrder.service.item.address.city}"/>, <c:out value="${workOrder.service.item.address.stateProvince}"/> <c:out value="${workOrder.service.item.address.zip}"/><br/>
<c:out value="${model.countries[workOrder.service.item.address.country]}"/><br/>
<c:if test="${workOrder.service.item.address.phone != ''}">
Tel: <c:out value="${workOrder.service.item.address.phone}"/><br/>
</c:if>
Fax: <c:out value="${workOrder.service.item.address.fax}"/>			    
				</td>	
			    <td align="center" valign="top"><img src="../graphics/priority_${workOrder.service.priority}.png" border="0"><br/><c:out value="${workOrder.service.status}"/></td>
				<td valign="top" class="nameCol">
				  <c:out value="${wj:newLineToBreakLine(workOrder.service.notes)}" escapeXml="false"/>
				</td>
			  </tr>
			</c:forEach>
			<c:if test="${model.list.nrOfElements == 0}">
			  <tr class="emptyList"><td colspan="6">&nbsp;</td></tr>
			</c:if>
			</table>
				  
			<table border="0" cellpadding="0" cellspacing="1" width="800">
			  <tr>
			  <td class="pageSize">
			  <select name="size" onchange="document.getElementById('page').value=1;submit()">
			    <c:forTokens items="10,25,50,100" delims="," var="current">
			  	  <option value="${current}" <c:if test="${current == model.list.pageSize}">selected</c:if>>${current} <fmt:message key="perPage" /></option>
			    </c:forTokens>
			  </select>
			  </td>
			  </tr>
			</table>	  

</form>  

</body>
</html>