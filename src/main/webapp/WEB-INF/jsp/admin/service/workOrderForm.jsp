<%@ page import="java.util.*" %>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ taglib uri="http://www.springframework.org/security/tags" prefix="sec" %>

<tiles:insertDefinition name="admin.service" flush="true">
  <tiles:putAttribute name="tab"  value="services" />
  <tiles:putAttribute name="content" type="string">

<script language="JavaScript" type="text/JavaScript">
<!--
window.addEvent('domready', function(){			
			var tabs1 = new SimpleTabs($('tab-block-1'), { selector: 'h4'});
			var Tips1 = new Tips($$('.toolTipImg'));
		});
//-->
</script>
<link rel="stylesheet" type="text/css" media="all" href="../../admin/javascript/jscalendar-1.0/skins/aqua/theme.css" title="Aqua" /> <!-- calendar stylesheet -->
<script type="text/javascript" src="../../admin/javascript/jscalendar-1.0/calendar.js"></script> <!-- main calendar program -->
<script type="text/javascript" src="../../admin/javascript/jscalendar-1.0/lang/calendar-en.js"></script>
<script type="text/javascript" src="../../admin/javascript/jscalendar-1.0/calendar-setup.js"></script>

<style type="text/css">
<!--
.woHdr {
	border-bottom: 2px solid #000000;
	}
.woFtr {
	border-bottom: 2px solid #000000;
	}
-->
</style>

<form:form commandName="workOrder" method="post"> 
<!-- main box -->
<div id="mboxfull">
  
  <!-- start table -->
  <table cellpadding="0" cellspacing="0" border="0" class="module" >
  <tr>
    <td class="topl_g">
	  
	  <!-- breadcrumb -->
	  <p class="breadcrumb">
	    <a href="../service">Service</a> &gt;
	    <fmt:message key="workOrder"/> # <c:out value="${workOrder.service.serviceNum}"/>
	  </p>
	  
	  <!-- Error Message -->
	  <c:if test="${message != null}">
	    <div class="message"><fmt:message key="${message}" /></div>
	  </c:if>

	  <c:if test="${not workOrder.newWorkOrder}">	  
	  <div align="right">
		<table class="form">
		  <tr>
		    <td align="right"><fmt:message key="dateAdded" /> : </td>
		    <td><fmt:formatDate type="both" timeStyle="full" value="${workOrder.created}"/></td>
		  </tr>
		  <tr>
		    <td align="right"><fmt:message key="lastModified" /> : </td>
		    <td><fmt:formatDate type="both" timeStyle="full" value="${workOrder.lastModified}"/></td>
		  </tr>
		</table>
	  </div>
	  </c:if>
  
  </td><td class="topr_g" ></td></tr>
  <tr><td class="boxmidlrg" >
<c:if test="${workOrder != null}">

 <!-- tabs -->
 <div id="tab-block-1">
	<h4 title="<fmt:message key='workOrder' />"><fmt:message key='workOrder' /></h4>
	<div>
	<!-- start tab -->

	  	<div class="listdivi ln tabdivi"></div>
	  	<div class="listdivi"></div>
	       
		  	<div class="list0">
		  	<!-- input field -->
		  	
			<table border="0" align="center" cellspacing="3" cellpadding="1">
			<tr valign="top">
			<td align="right"><b><fmt:message key="service" /> <fmt:message key="location" />:</b></td>
		  	<td>
	  				<c:if test="${workOrder.service.item.address.company != ''}">
					<c:out value="${workOrder.service.item.address.company}"/><br/>
					</c:if>
					<c:out value="${workOrder.service.item.address.firstName}"/> <c:out value="${workOrder.service.item.address.lastName}"/><br/>
					<c:out value="${workOrder.service.item.address.addr1}"/><br/> 
					<c:if test="${workOrder.service.item.address.addr2 != ''}">
					<c:out value="${workOrder.service.item.address.addr2}"/><br/>
					</c:if>
					<c:out value="${workOrder.service.item.address.city}"/>, <c:out value="${workOrder.service.item.address.stateProvince}"/> <c:out value="${workOrder.service.item.address.zip}"/><br/>
					<c:out value="${countries[workOrder.service.item.address.country]}"/><br/>
					<c:if test="${workOrder.service.item.address.phone != ''}">
					Tel: <c:out value="${workOrder.service.item.address.phone}"/><br/>
					</c:if>
					<c:if test="${workOrder.service.item.address.fax != ''}">
					Fax: <c:out value="${workOrder.service.item.address.fax}"/><br/>
					</c:if>
					<c:if test="${workOrder.service.item.address.cellPhone != ''}">
					Cell: <c:out value="${workOrder.service.item.address.cellPhone}"/><br/>
					</c:if>
					<c:if test="${workOrder.service.item.address.email != ''}">
					<fmt:message key="email" />: <c:out value="${workOrder.service.item.address.email}"/><br/>
					</c:if>  	
		  	</td>
		  	</tr>
		  	<c:if test="${not empty workOrder.service.alternateContact}">
			<tr>
			<td align="right"><fmt:message key="alternateContact" />:</td>
		  	<td><c:out value="${workOrder.service.alternateContact}"/></td>
		  	</tr>
		  	</c:if>
		  	</table>		  	
			
			<br/>			
			
			<table border="0" width="100%" cellspacing="3" cellpadding="1">
			<tr valign="top">
			<td>
			<table border="0">
			  <c:if test="${subContractors != null}">
			  <tr>
			    <td><b><fmt:message key="subContractor" /></b></td>
			    <td>&nbsp;&nbsp;</td>
			    <td>
				  <select name="service.host" style="width:250px;">
				    <c:forEach items="${subContractors}" var="option">
					  	<option value="${option.host}" <c:if test="${option.host == workOrder.service.host}">selected</c:if>>
						  <c:if test="${option.host == ''}"><fmt:message key="default" /></c:if><c:out value="${option.host}"/> <c:if test="${option.name != ''}"> - <c:out value="${option.name}"/></c:if>
					  	</option>						  
					</c:forEach>
				  </select>
			    </td>
			  </tr>
			  </c:if>
			  <tr>
			    <td><b><fmt:message key="workOrder"/> <fmt:message key="type" /></b></td>
			    <td>&nbsp;&nbsp;</td>
			    <td>
	  			<form:select path="type" cssStyle="width:250px;">
	  			  <form:option value=""></form:option>
				  <c:forEach var="serviceType" items="${serviceTypes}">
		          <form:option value="${serviceType}"/>
				  </c:forEach>
		        </form:select>
			    </td>
			  </tr>			  
			  <tr>
			    <td><b><fmt:message key="purchaseOrder" /></b></td>
			    <td>&nbsp;&nbsp;</td>
			    <td><form:input path="service.purchaseOrder" maxlength="50" htmlEscape="true"/></td>
			  </tr>
			  <tr>
			    <td><b><fmt:message key="account"/> #</b></td>
			    <td>&nbsp;&nbsp;</td>
			    <td><c:out value="${workOrder.service.item.customer.accountNumber}"/></td>
			  </tr>
			  <tr>
			    <td><b><c:out value="${siteConfig['SERVICEABLE_ITEM_TITLE'].value}"/> ID/SN</b></td>
			    <td>&nbsp;&nbsp;</td>
			    <td><c:out value="${workOrder.service.item.itemId}"/> / <c:out value="${workOrder.service.item.serialNum}"/></td>
			  </tr>
			  <tr>
			    <td><b><fmt:message key="sku" /></b></td>
			    <td>&nbsp;&nbsp;</td>
			    <td><c:out value="${workOrder.service.item.sku}"/></td>
			  </tr>
			  <tr>
			    <td><b><fmt:message key="description" /></b></td>
			    <td>&nbsp;&nbsp;</td>
			    <td><c:out value="${product.name}"/></td>
			  </tr>
			  <c:forEach items="${product.productFields}" var="productField">
			  <tr>
			    <td><b><c:out value="${productField.name}"/></b></td>
			    <td>&nbsp;&nbsp;</td>
			    <td><c:out value="${productField.value}"/></td>
			  </tr>
			  </c:forEach>			  
			  <tr>
			    <td><b><fmt:message key="technician" /></b></td>
			    <td>&nbsp;&nbsp;</td>
			    <td>
				   <select name="technician"/>
				    <c:set var="technicianFound" value="false"/>
				    <option value=""></option>
				    <c:forTokens items="Chris Valera,Eric Allgood,John Aubert" delims="," var="technician">
			          <option value="${technician}" <c:if test="${workOrder.technician == technician}">selected<c:set var="technicianFound" value="true"/></c:if>><c:out value="${technician}"/></option>
			        </c:forTokens>
			        <c:if test="${technicianFound != true and not empty workOrder.technician}">
			         <option value="${workOrder.technician}" selected><c:out value="${workOrder.technician}"/></option>
			        </c:if>
				   </select>
			    </td>
			  </tr>
			  <tr>
			    <td valign="top"><b><fmt:message key="problem" /></b></td>
			    <td>&nbsp;&nbsp;</td>
			    <td><form:textarea rows="2" cols="50" path="service.problem" htmlEscape="true" /></td>
			  </tr>			  
			  <tr>
			    <td><b><fmt:message key="trackNum" /> (In)</b></td>
			    <td>&nbsp;&nbsp;</td>
			    <td><c:out value="${workOrder.service.trackNumInCarrier}" />
				<c:choose>
				 <c:when test="${workOrder.service.trackNumInCarrier == 'UPS'}">
				  <a href="http://wwwapps.ups.com/WebTracking/processInputRequest?HTMLVersion=5.0&sort_by=status&tracknums_displayed=10&TypeOfInquiryNumber=T&loc=en_US&InquiryNumber1=<c:out value='${workOrder.service.trackNumIn}' />&AgreeToTermsAndConditions=yes"><c:out value="${workOrder.service.trackNumIn}" /></a>
				 </c:when>
				 <c:when test="${workOrder.service.trackNumInCarrier == 'FedEx'}">
				  <a href="http://fedex.com/Tracking?action=track&language=english&cntry_code=us&mps=y&tracknumbers=<c:out value='${workOrder.service.trackNumIn}' />&ascend_header=1"><c:out value="${workOrder.service.trackNumIn}" /></a>
				 </c:when>
				 <c:when test="${workOrder.service.trackNumInCarrier == 'USPS'}">
				  <a href="http://trkcnfrm1.smi.usps.com/PTSInternetWeb/InterLabelInquiry.do?strOrigTrackNum=<c:out value='${workOrder.service.trackNumIn}' />"><c:out value="${workOrder.service.trackNumIn}" /></a>
				 </c:when>
				 <c:otherwise><c:out value="${workOrder.service.trackNumIn}" /></c:otherwise>
				</c:choose>
			    </td>
			  </tr>	
			  <tr>
			    <td><b><fmt:message key="trackNum" /> (Out)</b></td>
			    <td>&nbsp;&nbsp;</td>
			    <td>
				<form:select path="service.trackNumOutCarrier">
				  <form:option value="">Choose a carrier</form:option>
				  <form:option value="UPS">UPS</form:option>
				  <form:option value="FedEx">FedEx</form:option>
				  <form:option value="USPS">USPS</form:option>
				  <form:option value="Trucks">Trucks</form:option>
				  <form:option value="Others">Others</form:option>
				</form:select>
				<form:input path="service.trackNumOut" maxlength="50" htmlEscape="true"/>
			    </td>
			  </tr>
			</table>
			</td>
			<td>&nbsp;</td>
			<td align="right">
			<table>
			  <tr>
			    <td><b><fmt:message key="workOrder" /> #</b></td>
			    <td>&nbsp;&nbsp;</td>
			    <td><c:out value="${workOrder.service.serviceNum}"/></td>
			  </tr>			
			  <tr>
			    <td><b><fmt:message key="reportDate" /></b></td>
			    <td>&nbsp;&nbsp;</td>
			    <td><fmt:formatDate type="both" timeStyle="full" value="${workOrder.service.reportDate}"/></td>
			  </tr>
			  <tr>
			    <td><b><fmt:message key="workOrder" /> <fmt:message key="date" /></b></td>
			    <td>&nbsp;&nbsp;</td>
			    <td>
	  			<form:input path="date" size="11" maxlength="10" />
				  <img class="calendarImage"  id="date_trigger" src="../graphics/calendarIcon.jpg" />
				  <script type="text/javascript">
		    		Calendar.setup({
		        			inputField     :    "date",   // id of the input field
		        			showsTime      :    false,
		        			ifFormat       :    "%m/%d/%Y",   // format of the input field
		        			button         :    "date_trigger"   // trigger for the calendar (button ID)
		    		});	
				  </script> 
				<form:errors path="date" cssClass="error"/>
				</td>
			  </tr>
			  <tr>
			    <td><b><fmt:message key="service" /> <fmt:message key="date" /></b></td>
			    <td>&nbsp;&nbsp;</td>
			    <td>
	  			<form:input path="serviceDate" size="11" maxlength="10" />
				  <img class="calendarImage"  id="service_date_trigger" src="../graphics/calendarIcon.jpg" />
				  <script type="text/javascript">
		    		Calendar.setup({
		        			inputField     :    "serviceDate",   // id of the input field
		        			showsTime      :    false,
		        			ifFormat       :    "%m/%d/%Y",   // format of the input field
		        			button         :    "service_date_trigger"   // trigger for the calendar (button ID)
		    		});	
				  </script> 
				<form:errors path="serviceDate" cssClass="error"/>
				</td>
			  </tr>
			  <tr>
			    <td><b><fmt:message key="startTime" /></b></td>
			    <td>&nbsp;&nbsp;</td>
			    <td>
			  	  <form:select path="startHour" cssStyle="width:50px">
				    <form:option value="" />
	    		    <c:forEach begin="0" end="9" var="hour">
				    <form:option value="0${hour}" />
        		    </c:forEach>
	    		    <c:forEach begin="10" end="23" var="hour">
				    <form:option value="${hour}" />
        		    </c:forEach>
				  </form:select> : 
			      <form:select path="startMin" cssStyle="width:50px">
				    <form:option value="" />
	    		    <c:forEach begin="0" end="9" var="min">
				    <form:option value="0${min}" />
        		    </c:forEach>
	    		    <c:forEach begin="10" end="59" var="min">
				    <form:option value="${min}" />
        		    </c:forEach>
				  </form:select>
				  <%-- 
				  <fmt:formatDate type="time" timeStyle="short" value="${workOrder.startTime}"/>
				  --%>
				  <form:errors path="startTime" cssClass="error"/>
				</td>
			  </tr>
			  <tr>
			    <td><b><fmt:message key="stopTime" /></b></td>
			    <td>&nbsp;&nbsp;</td>
			    <td>
			  	  <form:select path="stopHour" cssStyle="width:50px">
				    <form:option value="" />
	    		    <c:forEach begin="0" end="9" var="hour">
				    <form:option value="0${hour}" />
        		    </c:forEach>
	    		    <c:forEach begin="10" end="23" var="hour">
				    <form:option value="${hour}" />
        		    </c:forEach>
				  </form:select> : 
			      <form:select path="stopMin" cssStyle="width:50px">
				    <form:option value="" />
	    		    <c:forEach begin="0" end="9" var="min">
				    <form:option value="0${min}" />
        		    </c:forEach>
	    		    <c:forEach begin="10" end="59" var="min">
				    <form:option value="${min}" />
        		    </c:forEach>
				  </form:select>
				  <form:errors path="stopTime" cssClass="error"/>
				</td>
			  </tr>
			  <tr>
			    <td><b>Install %</b></td>
			    <td>&nbsp;&nbsp;</td>
			    <td><form:input path="installPercentage" size="5" maxlength="3"/> <form:errors path="installPercentage" cssClass="error"/></td>
			  </tr>
			  <c:if test="${workOrder.outSN != null and fn:trim(workOrder.outSN) != ''}">
			  <tr>
			    <td><b>Out SN</b></td>
			    <td>&nbsp;&nbsp;</td>
			    <td><form:input path="outSN" size="10" maxlength="50" htmlEscape="true"/></td>
			  </tr>
			  </c:if>
			  <c:if test="${workOrder.inSN != null and fn:trim(workOrder.inSN) != ''}">
			  <tr>
			    <td><b>In SN</b></td>
			    <td>&nbsp;&nbsp;</td>
			    <td><form:input path="inSN" size="10" maxlength="50" htmlEscape="true"/></td>
			  </tr>
			  </c:if>
			  <tr>
			    <td><b><fmt:message key="bwCount"/></b></td>
			    <td>&nbsp;&nbsp;</td>
			    <td><form:input path="bwCount" size="10" maxlength="10"/> <form:errors path="bwCount" cssClass="error"/></td>
			  </tr>
			  <tr>
			    <td><b><fmt:message key="colorCount"/></b></td>
			    <td>&nbsp;&nbsp;</td>
			    <td><form:input path="colorCount" size="10" maxlength="10"/> <form:errors path="colorCount" cssClass="error"/></td>
			  </tr>
			  <tr>
			    <td><b>Printer From City, State, Zip</b></td>
			    <td>&nbsp;&nbsp;</td>
			    <td><form:input path="service.itemFrom" size="20" maxlength="100"/> <form:errors path="service.itemFrom" cssClass="error"/></td>
			  </tr>
			  <tr>
			    <td><b>Printer To City, State, Zip</b></td>
			    <td>&nbsp;&nbsp;</td>
			    <td><form:input path="service.itemTo" size="20" maxlength="100"/> <form:errors path="service.itemTo" cssClass="error"/></td>
			  </tr>
			  <tr>
			    <td><b><fmt:message key="shipDate" /></b></td>
			    <td>&nbsp;&nbsp;</td>
			    <td>
	  			<form:input path="service.shipDate" size="11" maxlength="10" />
				  <img class="calendarImage"  id="ship_date_trigger" src="../graphics/calendarIcon.jpg" />
				  <script type="text/javascript">
		    		Calendar.setup({
		        			inputField     :    "service.shipDate",   // id of the input field
		        			showsTime      :    false,
		        			ifFormat       :    "%m/%d/%Y",   // format of the input field
		        			button         :    "ship_date_trigger"   // trigger for the calendar (button ID)
		    		});	
				  </script> 
				<form:errors path="service.shipDate" cssClass="error"/>
				</td>
			  </tr>
			</table>
			</td>
			</tr>
			</table>

			<br/>
						
			<table border="0" cellpadding="2" cellspacing="0" width="100%">
			  <tr>
			    <c:set var="cols" value="0"/>
			    <td width="5%" class="woHdr"><fmt:message key="remove" /></td>
			    <td width="5%" class="woHdr">&nbsp;</td>
			    <td width="15%" class="woHdr"><fmt:message key="itemCode" /></td>
			    <td class="woHdr"><fmt:message key="description" /></td>
			    <td width="10%" class="woHdr">In SN</td>
			    <td width="10%" class="woHdr">Out SN</td>
			    <td width="10%" class="woHdr"><fmt:message key="quantity" /></td>
			    <c:if test="${workOrder.hasPacking}">
				  <c:set var="cols" value="${cols+1}"/>
			      <td width="10%" class="woHdr"><fmt:message key="packing" /></td>
			    </c:if>
			    <c:if test="${workOrder.hasContent}">
				  <c:set var="cols" value="${cols+1}"/>
			      <td class="woHdr"><fmt:message key="content" /></td>
			    </c:if>
			  </tr>
			<c:forEach var="lineItem" items="${workOrder.lineItems}" varStatus="status">
			<c:if test="${status.first}">
			  <tr bgcolor="#FFFFCC">
			    <td colspan="${7+cols}">EXISTING</td>
			  </tr>
			</c:if>			
			<tr valign="top">
			  <td align="center"><input type="checkbox" name="__remove_${lineItem.lineNumber}"></td>
			  <td align="center">&nbsp;</td>
			  <td><c:out value="${lineItem.product.sku}"/></td>
			  <td><c:out value="${lineItem.product.name}"/>
			  </td>
			  <td>
			    <input type="text" name="__inSN_${lineItem.lineNumber}" value="${lineItem.inSN}" size="10" maxlength="50">
			  </td>			  
			  <td>
			    <input type="text" name="__outSN_${lineItem.lineNumber}" value="${lineItem.outSN}" size="10" maxlength="50">
			  </td>			  
			  <td align="left"> 
			    <input style="TEXT-ALIGN: right" type="text" name="__quantity_${lineItem.lineNumber}" value="${lineItem.quantity}" size="5">
			  </td>
			  <c:if test="${workOrder.hasPacking}">
			  <td align="center"><c:out value="${lineItem.product.packing}" /></td>
			  </c:if>
			  <c:if test="${workOrder.hasContent}">
			    <td align="center"><c:out value="${lineItem.product.caseContent}" /></td>
			  </c:if>
			</tr>
			<tr>
			  <td colspan="3" align="right"><b><fmt:message key="workNotes" />:</b>&nbsp;&nbsp;&nbsp;</td>
			  <td colspan="${4+cols}"><input type="text" name="__notes_<c:out value="${lineItem.lineNumber}"/>" value="<c:out value="${lineItem.notes}"/>" size="50" maxlength="255"></td>
			</tr>
			<tr><td colspan="${7+cols}">&nbsp;</td></tr>
			</c:forEach>
			<c:forEach var="lineItem" items="${workOrder.insertedLineItems}" varStatus="status">
			<c:if test="${status.first}">
			  <tr bgcolor="#FFFFCC">
			    <td colspan="${7+cols}">NEW</td>
			  </tr>
			</c:if>
			<tr valign="top">
			  <td align="center"><input type="checkbox" name="__remove_new_${status.index}"></td>
			  <td align="center">&nbsp;</td>
			  <td><c:out value="${lineItem.product.sku}"/></td>
			  <td><c:if test="${lineItem.product.id != null}"><c:out value="${lineItem.product.name}"/></c:if>
			  					  <c:if test="${lineItem.product.id == null}"><input type="text" name="__name_new_${status.index}" value="<c:out value="${lineItem.product.name}"/>" size="50" maxlength="120"></c:if>  
			  </td>
			  <td>
			    <input type="text" name="__inSN_new_${status.index}" value="${lineItem.inSN}" size="10" maxlength="50">
			  </td>			  
			  <td>
			    <input type="text" name="__outSN_new_${status.index}" value="${lineItem.outSN}" size="10" maxlength="50">
			  </td>	
			  <td align="left"> 
			    <input style="TEXT-ALIGN: right" type="text" name="__quantity_new_${status.index}" value="${lineItem.quantity}" size="5">
			  </td>
			  <c:if test="${workOrder.hasPacking}">
			  <td align="center"><c:out value="${lineItem.product.packing}" /></td>
			  </c:if>
			  <c:if test="${workOrder.hasContent}">
			  <td align="center"><c:out value="${lineItem.product.caseContent}" /></td>
			  </c:if>
			</tr>
			<tr>
			  <td colspan="3" align="right"><b><fmt:message key="workNotes" />:</b>&nbsp;&nbsp;&nbsp;</td>
			  <td colspan="${4+cols}"><input type="text" name="__notes_new_${status.index}" value="<c:out value="${lineItem.notes}"/>" size="50" maxlength="255"></td>
			</tr>
			<tr><td colspan="${7+cols}">&nbsp;</td></tr>
			</c:forEach>
			  <tr>
			    <td colspan="${7+cols}" class="woFtr">&nbsp;</td>
			  </tr>
			</table>	

			<br/>
			<table border="0" cellspacing="3" cellpadding="1">
			  <tr>
			    <td><b><fmt:message key="status" />:</b></td>
			    <td>
				<c:choose>
				<c:when test="${fn:toLowerCase(workOrder.service.status) == 'completed'}">Completed</c:when>
		        <c:otherwise>
	  			<form:select path="service.status" cssStyle="width:200px;">
				  <c:forEach var="serviceStatus" items="${statuses}">
		          <form:option value="${serviceStatus}"/>
				  </c:forEach>
		        </form:select>
		        </c:otherwise>
		        </c:choose>
			    </td>
			  </tr>
			  <c:if test="${not (workOrder.service.status == 'Completed' or workOrder.service.status == 'Canceled')}">
			  <tr>
			    <td><b><fmt:message key="priority" />:</b></td>
			    <td><img src="../graphics/priority_${workOrder.service.priority}.png" border="0"></td>
			  </tr>
			  </c:if>
			</table>
			
			<sec:authorize ifAnyGranted="ROLE_AEM,ROLE_ADMIN_${prop_site_id}">
			  <c:set var="canEdit" value="true"/>
			</sec:authorize>
			
			<c:if test="${fn:toLowerCase(workOrder.service.status) != 'completed' or canEdit}">
			<br/><br/>
			<table width="100%">
			<tr>
			<td>
			<b><fmt:message key="add" /> <fmt:message key="productSku" /></b>: <input type="text" name="__sku" value="">
			<input type="submit" name="__update" value="<fmt:message key="update"/>">
			</td>
			<td align="right">
			<c:if test="${not workOrder.newWorkOrder}">
			<input type="submit" name="__save" value="<fmt:message key="saveChanges"/>">
			<c:if test="${!(fn:toLowerCase(workOrder.service.status) == 'canceled' or fn:toLowerCase(workOrder.service.status) == 'completed')}">
			<input type="submit" name="__complete" value="<fmt:message key="saveAndGenerate"/> <fmt:message key="invoice"/>">
			</c:if>
			</c:if>
			<c:if test="${workOrder.newWorkOrder}">
			<input type="submit" name="__save" value="<fmt:message key="add"/> <fmt:message key="workOrder"/>">
			</c:if>
			<input type="submit" value="<fmt:message key="cancel"/>" name="_cancel">
			</td>
			</tr>
			</table>
			</c:if>	
						 
            <!-- end input field -->
	  		</div>
	<!-- end tab -->        
	</div>
	
  <h4 title="<fmt:message key="serviceHistory" />"><fmt:message key="serviceHistory" /></h4>
	<div>
	<!-- start tab -->
	  	<div class="listdivi ln tabdivi"></div>
	  	<div class="listdivi"></div>
	   
		  	<div class="list${classIndex % 2}"><c:set var="classIndex" value="${classIndex+1}" />
		  	<div class="listpp">

<iframe src="serviceHistory.jsp?id=${workOrder.service.item.id}" width="800" height="500" frameborder="0"></iframe>
	
		  	</div>
		  	</div>

	<!-- end tab -->        
	</div>		
	
<!-- end tabs -->			
</div>

</c:if>  
  <!-- end table -->
   </td><td class="boxmidr" ></td></tr>
  <tr><td class="botl"></td><td class="botr"></td></tr>
  </table>
  
<!-- end main box -->  
</div>
</form:form>
    
</tiles:putAttribute>    
</tiles:insertDefinition> 	  	
