<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="/WEB-INF/tlds/spring.tld" prefix="spring" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>

<c:if test="${gSiteConfig['gSERVICE']}">
<tiles:insertDefinition name="admin.service" flush="true">
  <tiles:putAttribute name="tab"  value="items" />
  <tiles:putAttribute name="content" type="string">

<script language="JavaScript" type="text/JavaScript">
<!--
window.addEvent('domready', function(){			
			var tabs1 = new SimpleTabs($('tab-block-1'), { selector: 'h4'});
			var Tips1 = new Tips($$('.toolTipImg'));
		});
//-->
</script>	
<link rel="stylesheet" type="text/css" media="all" href="../../admin/javascript/jscalendar-1.0/skins/aqua/theme.css" title="Aqua" /> <!-- calendar stylesheet -->
<script type="text/javascript" src="../../admin/javascript/jscalendar-1.0/calendar.js"></script> <!-- main calendar program -->
<script type="text/javascript" src="../../admin/javascript/jscalendar-1.0/lang/calendar-en.js"></script>
<script type="text/javascript" src="../../admin/javascript/jscalendar-1.0/calendar-setup.js"></script>

<!-- main box -->
  <div id="mboxfull">
<form:form commandName="serviceableItemForm" method="post">  
  <!-- start table -->
  <table cellpadding="0" cellspacing="0" border="0" class="module" >
  <tr>
    <td class="topl_g">
	  
	  <!-- breadcrumb -->
	  <p class="breadcrumb">
	    <a href="../service">Service</a> &gt;
	    <a href="../service/items.jhtm"><c:out value="${siteConfig['SERVICEABLE_ITEM_TITLE'].value}"/>s</a> &gt;
	    <c:out value="${siteConfig['SERVICEABLE_ITEM_TITLE'].value}"/> <c:out value="${serviceableItemForm.item.itemId}" /> <c:out value="${serviceableItemForm.item.serialNum}" />
	  </p>
	  
	  <!-- Error Message -->
	  <c:if test="${!empty message}">
		<div class="message"><spring:message code="${message}" /></div>
	  </c:if>
	  <spring:hasBindErrors name="serviceableItemForm">
		<div class="error"><fmt:message key='form.hasErrors'/></div>
	  </spring:hasBindErrors>
  
  </td><td class="topr_g" ></td></tr>
  <tr><td class="boxmidlrg" >

 <!-- tabs -->
 <div id="tab-block-1">
	<h4 title="<c:out value="${siteConfig['SERVICEABLE_ITEM_TITLE'].value}"/>"><c:out value="${siteConfig['SERVICEABLE_ITEM_TITLE'].value}"/></h4>
	<div>
	<!-- start tab -->

        <c:set var="classIndex" value="0" />
        
	  	<div class="listdivi ln tabdivi"></div>
	  	<div class="listdivi"></div>

		  	<div class="list${classIndex % 2}"><c:set var="classIndex" value="${classIndex+1}" />
		  	<div class="listfl"><fmt:message key="active" />:</div>
		  	<div class="listp">
		  	<!-- input field -->
	   			<form:checkbox path="item.active" />
		    <!-- end input field -->   	
		  	</div>
		  	</div>
	   
	        <c:if test="${serviceableItemForm.item.customer != null}">
		  	<div class="list${classIndex % 2}"><c:set var="classIndex" value="${classIndex+1}" />
		  	<div class="listfl"><fmt:message key='company' />:</div>
		  	<div class="listp">
		  	<!-- input field -->
                <c:out value="${serviceableItemForm.item.customer.address.company}"/>
		    <!-- end input field -->   	
		  	</div>
		  	</div>
		  	
		  	<div class="list${classIndex % 2}"><c:set var="classIndex" value="${classIndex+1}" />
		  	<div class="listfl"><fmt:message key='firstName' />:</div>
		  	<div class="listp">
		  	<!-- input field -->
                <c:out value="${serviceableItemForm.item.customer.address.firstName}"/>
		    <!-- end input field -->   	
		  	</div>
		  	</div>

		  	<div class="list${classIndex % 2}"><c:set var="classIndex" value="${classIndex+1}" />
		  	<div class="listfl"><fmt:message key='lastName' />:</div>
		  	<div class="listp">
		  	<!-- input field -->
                <c:out value="${serviceableItemForm.item.customer.address.lastName}"/>
		    <!-- end input field -->   	
		  	</div>
		  	</div>
		  	
		  	<div class="list${classIndex % 2}"><c:set var="classIndex" value="${classIndex+1}" />
		  	<div class="listfl"><fmt:message key='emailAddress' />:</div>
		  	<div class="listp">
		  	<!-- input field -->
                <c:out value="${serviceableItemForm.item.customer.username}"/> &nbsp;&nbsp;[ <a href="setOwner.jhtm?id=${serviceableItemForm.item.id}&begin=t"><fmt:message key='change' /></a> ]
		    <!-- end input field -->   	
		  	</div>
		  	</div>
		  	</c:if>
		  	
		  	<c:if test="${serviceableItemForm.item.customer == null and not serviceableItemForm.newItem}">
		  	<div class="list${classIndex % 2}"><c:set var="classIndex" value="${classIndex+1}" />
		  	<div class="listfl">&nbsp;</div>
		  	<div class="listp">
		  	<!-- input field -->
                <a href="setOwner.jhtm?id=${serviceableItemForm.item.id}&begin=t"><fmt:message key='setOwner' /></a>
		    <!-- end input field -->   	
		  	</div>
		  	</div>
		  	</c:if>
		  	
		  	<div class="list${classIndex % 2}"><c:set var="classIndex" value="${classIndex+1}" />
		  	<div class="listfl"><div class="requiredField"><c:out value="${siteConfig['SERVICEABLE_ITEM_TITLE'].value}"/> ID:</div></div>
		  	<div class="listp">
		  	<!-- input field -->
                <form:input path="item.itemId" size="40" maxlength="50" />
				<form:errors path="item.itemId" cssClass="error"/>
		    <!-- end input field -->   	
		  	</div>
		  	</div>
		  	
		  	<div class="list${classIndex % 2}"><c:set var="classIndex" value="${classIndex+1}" />
		  	<div class="listfl"><div class="requiredField"><fmt:message key="S/N" />:</div></div>
		  	<div class="listp">
		  	<!-- input field -->
                <form:input path="item.serialNum" size="40" maxlength="50" />
				<form:errors path="item.serialNum" cssClass="error"/>
		    <!-- end input field -->   	
		  	</div>
		  	</div>
		  	
		  	<div class="list${classIndex % 2}"><c:set var="classIndex" value="${classIndex+1}" />
		  	<div class="listfl"><fmt:message key="sku" />:</div>
		  	<div class="listp">
		  	<!-- input field -->
                <form:input path="item.sku" size="40" maxlength="50" />
				<form:errors path="item.sku" cssClass="error"/>
		    <!-- end input field -->   	
		  	</div>
		  	</div>

		  	<div class="list${classIndex % 2}"><c:set var="classIndex" value="${classIndex+1}" />
		  	<div class="listfl"><fmt:message key="description" />:</div>
		  	<div class="listp">
		  	<!-- input field -->
                <c:out value="${product.name}"/>    
		    <!-- end input field -->   	
		  	</div>
		  	</div>	
		  	
		  	<c:forEach items="${product.productFields}" var="productField" varStatus="status">
		  	<div class="list${classIndex % 2}"><c:set var="classIndex" value="${classIndex+1}" />
		  	<div class="listfl"><c:out value="${productField.name}"/>:</div>
		  	<div class="listp">
		  	<!-- input field -->
                <c:out value="${productField.value}"/>
		    <!-- end input field -->   	
		  	</div>
		  	</div>				  		  	
			</c:forEach>
	  	
	<!-- end tab -->        
	</div> 
	
	<h4 title="<fmt:message key='location' />"><fmt:message key='location' /></h4>
	<div>
	<!-- start tab -->

	  	<div class="listdivi ln tabdivi"></div>
	  	<div class="listdivi"></div>
	   
		  	<div class="list${classIndex % 2}"><c:set var="classIndex" value="${classIndex+1}" />
		  	<div class="listfl"><fmt:message key="company" />:</div>
		  	<div class="listp">
		  	<!-- input field -->
         		<form:input path="item.address.company"  htmlEscape="true" />
       			<form:errors path="item.address.company" cssClass="error" />
		    <!-- end input field -->   	
		  	</div>
		  	</div>

		  	<div class="list${classIndex % 2}"><c:set var="classIndex" value="${classIndex+1}" />
		  	<div class="listfl"><fmt:message key='firstName' />:</div>
		  	<div class="listp">
		  	<!-- input field -->
         		<form:input path="item.address.firstName"  htmlEscape="true" />
       			<form:errors path="item.address.firstName" cssClass="error" />
		    <!-- end input field -->   	
		  	</div>
		  	</div>
		  	
		  	<div class="list${classIndex % 2}"><c:set var="classIndex" value="${classIndex+1}" />
		  	<div class="listfl"><fmt:message key='lastName' />:</div>
		  	<div class="listp">
		  	<!-- input field -->
				<form:input path="item.address.lastName" htmlEscape="true" />
       			<form:errors path="item.address.lastName" cssClass="error" />
		    <!-- end input field -->   	
		  	</div>
		  	</div>	
		  	
		  	<div class="list${classIndex % 2}"><c:set var="classIndex" value="${classIndex+1}" />
		  	<div class="listfl"><fmt:message key="address" /> 1:</div>
		  	<div class="listp">
		  	<!-- input field -->
         		<form:input path="item.address.addr1" htmlEscape="true" />
       			<form:errors path="item.address.addr1" cssClass="error" />
		    <!-- end input field -->   	
		  	</div>
		  	</div>	
		  	
		  	<div class="list${classIndex % 2}"><c:set var="classIndex" value="${classIndex+1}" />
		  	<div class="listfl"><fmt:message key="address" /> 2:</div>
		  	<div class="listp">
		  	<!-- input field -->
         		<form:input path="item.address.addr2" htmlEscape="true" />
       			<form:errors path="item.address.addr2" cssClass="error" />
		    <!-- end input field -->   	
		  	</div>
		  	</div>	
		  	
		  	<div class="list${classIndex % 2}"><c:set var="classIndex" value="${classIndex+1}" />
		  	<div class="listfl"><fmt:message key='country' />:</div>
		  	<div class="listp">
		  	<!-- input field -->
         		<form:select id="country" path="item.address.country" onchange="toggleStateProvince(this)">
		         <form:option value="" label="Please Select"/>
		         <form:options items="${countries}" itemValue="code" itemLabel="name"/>
		        </form:select>
		        <form:errors path="item.address.country" cssClass="error" />
		    <!-- end input field -->   	
		  	</div>
		  	</div>	
		  	
		  	<div class="list${classIndex % 2}"><c:set var="classIndex" value="${classIndex+1}" />
		  	<div class="listfl"><fmt:message key="city" />:</div>
		  	<div class="listp">
		  	<!-- input field -->
         		<form:input path="item.address.city" htmlEscape="true" />
       			<form:errors path="item.address.city" cssClass="error" />
		    <!-- end input field -->   	
		  	</div>
		  	</div>	
		  	
		  	<div class="list${classIndex % 2}"><c:set var="classIndex" value="${classIndex+1}" />
		  	<div class="listfl"><fmt:message key="stateProvince" />:</div>
		  	<div class="listp">
		  	<!-- input field -->
		       <form:select id="state" path="item.address.stateProvince">
		         <form:option value="" label="Please Select"/>
		         <form:options items="${states}" itemValue="code" itemLabel="name"/>
		       </form:select>
		       <form:select id="ca_province" path="item.address.stateProvince">
		         <form:option value="" label="Please Select"/>
		         <form:options items="${caProvinceList}" itemValue="code" itemLabel="name"/>
		       </form:select>
		       <form:input id="province" path="item.address.stateProvince" htmlEscape="true"/>
		       <form:errors path="item.address.stateProvince" cssClass="error" />
		    <!-- end input field -->   	
		  	</div>
		  	</div>	
		  	
		  	<div class="list${classIndex % 2}"><c:set var="classIndex" value="${classIndex+1}" />
		  	<div class="listfl"><fmt:message key="zipCode" />:</div>
		  	<div class="listp">
		  	<!-- input field -->
         		<form:input path="item.address.zip" htmlEscape="true" />
       			<form:errors path="item.address.zip" cssClass="error" />
		    <!-- end input field -->   	
		  	</div>
		  	</div>	
		  	
		  	<div class="list${classIndex % 2}"><c:set var="classIndex" value="${classIndex+1}" />
		  	<div class="listfl"><fmt:message key="phone" />:</div>
		  	<div class="listp">
		  	<!-- input field -->
         		<form:input path="item.address.phone"  htmlEscape="true" />
       			<form:errors path="item.address.phone" cssClass="error" />
		    <!-- end input field -->   	
		  	</div>
		  	</div>	
		  	
		  	<div class="list${classIndex % 2}"><c:set var="classIndex" value="${classIndex+1}" />
		  	<div class="listfl"><fmt:message key="cellPhone" />:</div>
		  	<div class="listp">
		  	<!-- input field -->
         		<form:input path="item.address.cellPhone"  htmlEscape="true" />
       			<form:errors path="item.address.cellPhone" cssClass="error" />
		    <!-- end input field -->   	
		  	</div>
		  	</div>			  	

		  	<div class="list${classIndex % 2}"><c:set var="classIndex" value="${classIndex+1}" />
		  	<div class="listfl"><fmt:message key="fax" />:</div>
		  	<div class="listp">
		  	<!-- input field -->
         		<form:input path="item.address.fax"  htmlEscape="true" />
       			<form:errors path="item.address.fax" cssClass="error" />
		    <!-- end input field -->   	
		  	</div>
		  	</div>	
		  	
		  	<div class="list${classIndex % 2}"><c:set var="classIndex" value="${classIndex+1}" />
		  	<div class="listfl"><fmt:message key="email" />:</div>
		  	<div class="listp">
		  	<!-- input field -->
         		<form:input path="item.address.email" maxlength="255" size="60" htmlEscape="true" />
       			<form:errors path="item.address.email" cssClass="error" />
		    <!-- end input field -->   	
		  	</div>
		  	</div>	

		  	<div class="list${classIndex % 2}"><c:set var="classIndex" value="${classIndex+1}" />
		  	<div class="listfl"><div class="helpImg"><img class="toolTipImg" title="Note::Separate contacts by line breaks." src="../graphics/question.gif" /></div><fmt:message key="alternateContacts" />:</div>
		  	<div class="listp">
		  	<!-- input field -->
	  			<form:textarea rows="8" cols="60" path="item.alternateContacts" htmlEscape="true" />
		    <!-- end input field -->  
		  	</div>
		  	</div>
	  	
	<!-- end tab -->        
	</div> 
	
  <c:if test="${not serviceableItemForm.newItem}">
  <h4 title="<fmt:message key="serviceHistory" />"><fmt:message key="serviceHistory" /></h4>
	<div>
	<!-- start tab -->
	  	<div class="listdivi ln tabdivi"></div>
	  	<div class="listdivi"></div>
	   
		  	<div class="list${classIndex % 2}"><c:set var="classIndex" value="${classIndex+1}" />
		  	<div class="listpp">

<iframe src="serviceHistory.jsp?id=${serviceableItemForm.item.id}" width="800" height="500" frameborder="0"></iframe>
 	
		  	</div>
		  	</div>

	<!-- end tab -->        
	</div> 
	</c:if>
	
<!-- end tabs -->			
</div>	

<!-- start button -->
<div align="left" class="button"> 
<c:if test="${serviceableItemForm.newItem}">
  <input type="submit" value="<fmt:message key="add"/> <c:out value="${siteConfig['SERVICEABLE_ITEM_TITLE'].value}"/>">
</c:if>
<c:if test="${!serviceableItemForm.newItem}">		
  <input type="submit" value="<fmt:message key="update"/>">
  <input type="submit" value="<spring:message code="delete"/>" name="_delete" onClick="return confirm('Delete permanently?')">
</c:if>
  <input type="submit" value="<fmt:message key="cancel"/>" name="_cancel">
</div>
<!-- end button -->	     
  
  <!-- end table -->
    </td><td class="boxmidr" ></td></tr>
  <tr><td class="botl"></td><td class="botr"></td></tr>
  </table>

</form:form>  
<!-- end main box -->  
</div>

<script language="JavaScript">
<!--
function toggleStateProvince(el) {
  if (el.value == "US") {
      document.getElementById('state').disabled=false;
      document.getElementById('state').style.display="block";
      document.getElementById('ca_province').disabled=true;
      document.getElementById('ca_province').style.display="none";
      document.getElementById('province').disabled=true;
      document.getElementById('province').style.display="none";
  } else if (el.value == "CA") {
      document.getElementById('state').disabled=true;
      document.getElementById('state').style.display="none";
      document.getElementById('ca_province').disabled=false;
      document.getElementById('ca_province').style.display="block";
      document.getElementById('province').disabled=true;
      document.getElementById('province').style.display="none";
  } else {
      document.getElementById('state').disabled=true;
      document.getElementById('state').style.display="none";
      document.getElementById('ca_province').disabled=true;
      document.getElementById('ca_province').style.display="none";
      document.getElementById('province').disabled=false;
      document.getElementById('province').style.display="block";
  }
}
toggleStateProvince(document.getElementById('country'));
//-->
</script>
  </tiles:putAttribute>    
</tiles:insertDefinition>
</c:if>