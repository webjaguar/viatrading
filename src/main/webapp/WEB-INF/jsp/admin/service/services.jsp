<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>

<c:if test="${gSiteConfig['gSERVICE']}">
<tiles:insertDefinition name="admin.service" flush="true">
  <tiles:putAttribute name="tab"  value="services" />
  <tiles:putAttribute name="content" type="string">
    
<form action="services.jhtm" id="list" method="post">
<input type="hidden" id="sort" name="sort" value="${serviceSearch.sort}">
  <!-- main box -->
  <div id="mboxfull">
  
  <!-- start table -->
  <table cellpadding="0" cellspacing="0" border="0" class="module" >
  <tr>
    <td class="topl_g">
	  
	  <!-- breadcrumb -->
	  <p class="breadcrumb">
	    <a href="../service">Service</a> &gt;
	    requests 
	  </p>
	  
	  <!-- Error Message -->
	  
	  </td><td class="topr_g" ></td></tr>
  <tr><td class="boxmidlrg" >

 <!-- tabs -->
 <div class="tab-wrapper">
	<h4 title="List of affiliates"></h4>
	<div>
	<!-- start tab -->

	  	<div class="listdivi"></div>
	   
		  	<div class="listlight">
		  	<!-- input field --> 
			<table border="0" cellpadding="0" cellspacing="1" width="100%">
			  <tr>
			  <td class="pageShowing">
			  <c:if test="${list.nrOfElements > 0}">
			  <fmt:message key="showing">
				<fmt:param value="${list.firstElementOnPage + 1}"/>
				<fmt:param value="${list.lastElementOnPage + 1}"/>
				<fmt:param value="${list.nrOfElements}"/>
			  </fmt:message>
			  </c:if>
			  </td>
			  <td class="pageNavi">
			  Page 
			  <select name="page" id="page" onchange="submit()">
			  <c:forEach begin="1" end="${list.pageCount}" var="page">
			  	<option value="${page}" <c:if test="${page == (list.page+1)}">selected</c:if>>${page}</option>
			  </c:forEach>
			  </select>
			  of <c:out value="${list.pageCount}"/>
			  | 
			  <c:if test="${list.firstPage}"><span class="pageNaviDead"><fmt:message key="previous" /></span></c:if>
			  <c:if test="${not list.firstPage}"><a href="<c:url value="services.jhtm"><c:param name="page" value="${list.page}"/></c:url>" class="pageNaviLink"><fmt:message key="previous" /></a></c:if>
			  | 
			  <c:if test="${list.lastPage}"><span class="pageNaviDead"><fmt:message key="next" /></span></c:if>
			  <c:if test="${not list.lastPage}"><a href="<c:url value="services.jhtm"><c:param name="page" value="${list.page+2}"/></c:url>" class="pageNaviLink"><fmt:message key="next" /></a></c:if>
			  </td>
			  </tr>
			</table>
			
			<table border="0" cellpadding="0" cellspacing="0" width="100%" class="listings">
			  <tr class="listingsHdr2">
			    <td class="indexCol">&nbsp;</td>
				<td class="nameCol">
			    	<table cellspacing="0" cellpadding="1">
			    	  <tr>
			    	    <c:choose>
			    	      <c:when test="${serviceSearch.sort == 'service_num DESC'}">
			    	        <td class="listingsHdr3">
			    	        <a class="hdrLink" href="#" onClick="document.getElementById('sort').value='service_num';document.getElementById('list').submit()"><fmt:message key="serviceRequest" /> #</a>
			    	        </td>
			    	        <td><img src="../graphics/up.gif" border="0"></td>
			    	      </c:when>
			    	      <c:when test="${serviceSearch.sort == 'service_num'}">
			    	        <td class="listingsHdr3">
			    	        <a class="hdrLink" href="#" onClick="document.getElementById('sort').value='service_num DESC';document.getElementById('list').submit()"><fmt:message key="serviceRequest" /> #</a>
			    	        </td>
			    	        <td><img src="../graphics/down.gif" border="0"></td>
			    	      </c:when>
			    	      <c:otherwise>
			    	        <td colspan="2" class="listingsHdr3">
			    	        <a class="hdrLink" href="#" onClick="document.getElementById('sort').value='service_num DESC';document.getElementById('list').submit()"><fmt:message key="serviceRequest" /> #</a>
			    	        </td>
			    	      </c:otherwise>
			    	    </c:choose>  
			    	  </tr>
			    	  </table>
				</td>
				<td class="listingsHdr3"><fmt:message key="workOrder" /> #</td>
				<td align="center" class="listingsHdr3"><fmt:message key="orderNumber" /></td>
				<td align="center">
			    	<table cellspacing="0" cellpadding="1">
			    	  <tr>
			    	    <c:choose>
			    	      <c:when test="${serviceSearch.sort == 'company DESC'}">
			    	        <td class="listingsHdr3">
			    	        <a class="hdrLink" href="#" onClick="document.getElementById('sort').value='company';document.getElementById('list').submit()"><fmt:message key="company" /></a>
			    	        </td>
			    	        <td><img src="../graphics/up.gif" border="0"></td>
			    	      </c:when>
			    	      <c:when test="${serviceSearch.sort == 'company'}">
			    	        <td class="listingsHdr3">
			    	        <a class="hdrLink" href="#" onClick="document.getElementById('sort').value='company DESC';document.getElementById('list').submit()"><fmt:message key="company" /></a>
			    	        </td>
			    	        <td><img src="../graphics/down.gif" border="0"></td>
			    	      </c:when>
			    	      <c:otherwise>
			    	        <td colspan="2" class="listingsHdr3">
			    	        <a class="hdrLink" href="#" onClick="document.getElementById('sort').value='company DESC';document.getElementById('list').submit()"><fmt:message key="company" /></a>
			    	        </td>
			    	      </c:otherwise>
			    	    </c:choose>  
			    	  </tr>
			    	  </table>
				</td>
				<td align="center">
			    	<table cellspacing="0" cellpadding="1">
			    	  <tr>
			    	    <c:choose>
			    	      <c:when test="${serviceSearch.sort == 'item_id DESC'}">
			    	        <td class="listingsHdr3">
			    	        <a class="hdrLink" href="#" onClick="document.getElementById('sort').value='item_id';document.getElementById('list').submit()"><c:out value="${siteConfig['SERVICEABLE_ITEM_TITLE'].value}"/> ID</a>
			    	        </td>
			    	        <td><img src="../graphics/up.gif" border="0"></td>
			    	      </c:when>
			    	      <c:when test="${serviceSearch.sort == 'item_id'}">
			    	        <td class="listingsHdr3">
			    	        <a class="hdrLink" href="#" onClick="document.getElementById('sort').value='item_id DESC';document.getElementById('list').submit()"><c:out value="${siteConfig['SERVICEABLE_ITEM_TITLE'].value}"/> ID</a>
			    	        </td>
			    	        <td><img src="../graphics/down.gif" border="0"></td>
			    	      </c:when>
			    	      <c:otherwise>
			    	        <td colspan="2" class="listingsHdr3">
			    	        <a class="hdrLink" href="#" onClick="document.getElementById('sort').value='item_id DESC';document.getElementById('list').submit()"><c:out value="${siteConfig['SERVICEABLE_ITEM_TITLE'].value}"/> ID</a>
			    	        </td>
			    	      </c:otherwise>
			    	    </c:choose>  
			    	  </tr>
			    	  </table>
				</td>
			    <td align="center">
			    	<table cellspacing="0" cellpadding="1">
			    	  <tr>
			    	    <c:choose>
			    	      <c:when test="${serviceSearch.sort == 'report_date DESC'}">
			    	        <td class="listingsHdr3">
			    	        <a class="hdrLink" href="#" onClick="document.getElementById('sort').value='report_date';document.getElementById('list').submit()"><fmt:message key="reportDate" /></a>
			    	        </td>
			    	        <td><img src="../graphics/up.gif" border="0"></td>
			    	      </c:when>
			    	      <c:when test="${serviceSearch.sort == 'report_date'}">
			    	        <td class="listingsHdr3">
			    	        <a class="hdrLink" href="#" onClick="document.getElementById('sort').value='report_date DESC';document.getElementById('list').submit()"><fmt:message key="reportDate" /></a>
			    	        </td>
			    	        <td><img src="../graphics/down.gif" border="0"></td>
			    	      </c:when>
			    	      <c:otherwise>
			    	        <td colspan="2" class="listingsHdr3">
			    	        <a class="hdrLink" href="#" onClick="document.getElementById('sort').value='report_date DESC';document.getElementById('list').submit()"><fmt:message key="reportDate" /></a>
			    	        </td>
			    	      </c:otherwise>
			    	    </c:choose>  
			    	  </tr>
			    	  </table>
			    </td>
			    <td align="center">
			    	<table cellspacing="0" cellpadding="1">
			    	  <tr>
			    	    <c:choose>
			    	      <c:when test="${serviceSearch.sort == 'complete_date DESC'}">
			    	        <td class="listingsHdr3">
			    	        <a class="hdrLink" href="#" onClick="document.getElementById('sort').value='complete_date';document.getElementById('list').submit()"><fmt:message key="completeDate" /></a>
			    	        </td>
			    	        <td><img src="../graphics/up.gif" border="0"></td>
			    	      </c:when>
			    	      <c:when test="${serviceSearch.sort == 'complete_date'}">
			    	        <td class="listingsHdr3">
			    	        <a class="hdrLink" href="#" onClick="document.getElementById('sort').value='complete_date DESC';document.getElementById('list').submit()"><fmt:message key="completeDate" /></a>
			    	        </td>
			    	        <td><img src="../graphics/down.gif" border="0"></td>
			    	      </c:when>
			    	      <c:otherwise>
			    	        <td colspan="2" class="listingsHdr3">
			    	        <a class="hdrLink" href="#" onClick="document.getElementById('sort').value='complete_date DESC';document.getElementById('list').submit()"><fmt:message key="completeDate" /></a>
			    	        </td>
			    	      </c:otherwise>
			    	    </c:choose>  
			    	  </tr>
			    	  </table>
			    </td>
				<td align="center">
			    	<table cellspacing="0" cellpadding="1">
			    	  <tr>
			    	    <c:choose>
			    	      <c:when test="${serviceSearch.sort == 'status DESC'}">
			    	        <td class="listingsHdr3">
			    	        <a class="hdrLink" href="#" onClick="document.getElementById('sort').value='status';document.getElementById('list').submit()"><fmt:message key="status" /></a>
			    	        </td>
			    	        <td><img src="../graphics/up.gif" border="0"></td>
			    	      </c:when>
			    	      <c:when test="${serviceSearch.sort == 'status'}">
			    	        <td class="listingsHdr3">
			    	        <a class="hdrLink" href="#" onClick="document.getElementById('sort').value='status DESC';document.getElementById('list').submit()"><fmt:message key="status" /></a>
			    	        </td>
			    	        <td><img src="../graphics/down.gif" border="0"></td>
			    	      </c:when>
			    	      <c:otherwise>
			    	        <td colspan="2" class="listingsHdr3">
			    	        <a class="hdrLink" href="#" onClick="document.getElementById('sort').value='status DESC';document.getElementById('list').submit()"><fmt:message key="status" /></a>
			    	        </td>
			    	      </c:otherwise>
			    	    </c:choose>  
			    	  </tr>
			    	  </table>
				</td>
			    <td align="center">
			    	<table cellspacing="0" cellpadding="1">
			    	  <tr>
			    	    <c:choose>
			    	      <c:when test="${serviceSearch.sort == 'technician DESC'}">
			    	        <td class="listingsHdr3">
			    	        <a class="hdrLink" href="#" onClick="document.getElementById('sort').value='technician';document.getElementById('list').submit()"><fmt:message key="technician" /></a>
			    	        </td>
			    	        <td><img src="../graphics/up.gif" border="0"></td>
			    	      </c:when>
			    	      <c:when test="${serviceSearch.sort == 'technician'}">
			    	        <td class="listingsHdr3">
			    	        <a class="hdrLink" href="#" onClick="document.getElementById('sort').value='technician DESC';document.getElementById('list').submit()"><fmt:message key="technician" /></a>
			    	        </td>
			    	        <td><img src="../graphics/down.gif" border="0"></td>
			    	      </c:when>
			    	      <c:otherwise>
			    	        <td colspan="2" class="listingsHdr3">
			    	        <a class="hdrLink" href="#" onClick="document.getElementById('sort').value='technician DESC';document.getElementById('list').submit()"><fmt:message key="technician" /></a>
			    	        </td>
			    	      </c:otherwise>
			    	    </c:choose>  
			    	  </tr>
			    	  </table>
			    </td>
			    <td align="center">
			    	<table cellspacing="0" cellpadding="1">
			    	  <tr>
			    	    <c:choose>
			    	      <c:when test="${serviceSearch.sort == 'purchase_order DESC'}">
			    	        <td class="listingsHdr3">
			    	        <a class="hdrLink" href="#" onClick="document.getElementById('sort').value='purchase_order';document.getElementById('list').submit()">P.O.</a>
			    	        </td>
			    	        <td><img src="../graphics/up.gif" border="0"></td>
			    	      </c:when>
			    	      <c:when test="${serviceSearch.sort == 'purchase_order'}">
			    	        <td class="listingsHdr3">
			    	        <a class="hdrLink" href="#" onClick="document.getElementById('sort').value='purchase_order DESC';document.getElementById('list').submit()">P.O.</a>
			    	        </td>
			    	        <td><img src="../graphics/down.gif" border="0"></td>
			    	      </c:when>
			    	      <c:otherwise>
			    	        <td colspan="2" class="listingsHdr3">
			    	        <a class="hdrLink" href="#" onClick="document.getElementById('sort').value='purchase_order DESC';document.getElementById('list').submit()">P.O.</a>
			    	        </td>
			    	      </c:otherwise>
			    	    </c:choose>  
			    	  </tr>
			    	  </table>
			    </td>
			    <td align="center">
			    	<table cellspacing="0" cellpadding="1">
			    	  <tr>
			    	    <c:choose>
			    	      <c:when test="${serviceSearch.sort == 'wo_3rd DESC'}">
			    	        <td class="listingsHdr3">
			    	        <a class="hdrLink" href="#" onClick="document.getElementById('sort').value='wo_3rd';document.getElementById('list').submit()"><fmt:message key="3rdParty" /> W.O.</a>
			    	        </td>
			    	        <td><img src="../graphics/up.gif" border="0"></td>
			    	      </c:when>
			    	      <c:when test="${serviceSearch.sort == 'wo_3rd'}">
			    	        <td class="listingsHdr3">
			    	        <a class="hdrLink" href="#" onClick="document.getElementById('sort').value='wo_3rd DESC';document.getElementById('list').submit()"><fmt:message key="3rdParty" /> W.O.</a>
			    	        </td>
			    	        <td><img src="../graphics/down.gif" border="0"></td>
			    	      </c:when>
			    	      <c:otherwise>
			    	        <td colspan="2" class="listingsHdr3">
			    	        <a class="hdrLink" href="#" onClick="document.getElementById('sort').value='wo_3rd DESC';document.getElementById('list').submit()"><fmt:message key="3rdParty" /> W.O.</a>
			    	        </td>
			    	      </c:otherwise>
			    	    </c:choose>  
			    	  </tr>
			    	  </table>
			    </td>
			    <td align="center">
			    	<table cellspacing="0" cellpadding="1">
			    	  <tr>
			    	    <c:choose>
			    	      <c:when test="${serviceSearch.sort == 'manuf_warranty DESC'}">
			    	        <td class="listingsHdr3">
			    	        <a class="hdrLink" href="#" onClick="document.getElementById('sort').value='manuf_warranty';document.getElementById('list').submit()">Mfg. Warranty Ticket #</a>
			    	        </td>
			    	        <td><img src="../graphics/up.gif" border="0"></td>
			    	      </c:when>
			    	      <c:when test="${serviceSearch.sort == 'manuf_warranty'}">
			    	        <td class="listingsHdr3">
			    	        <a class="hdrLink" href="#" onClick="document.getElementById('sort').value='manuf_warranty DESC';document.getElementById('list').submit()">Mfg. Warranty Ticket #</a>
			    	        </td>
			    	        <td><img src="../graphics/down.gif" border="0"></td>
			    	      </c:when>
			    	      <c:otherwise>
			    	        <td colspan="2" class="listingsHdr3">
			    	        <a class="hdrLink" href="#" onClick="document.getElementById('sort').value='manuf_warranty DESC';document.getElementById('list').submit()">Mfg. Warranty Ticket #</a>
			    	        </td>
			    	      </c:otherwise>
			    	    </c:choose>  
			    	  </tr>
			    	  </table>
			    </td>
			    <td align="center">
			    	<table cellspacing="0" cellpadding="1">
			    	  <tr>
			    	    <c:choose>
			    	      <c:when test="${serviceSearch.sort == 'work_order_type DESC'}">
			    	        <td class="listingsHdr3">
			    	        <a class="hdrLink" href="#" onClick="document.getElementById('sort').value='work_order_type';document.getElementById('list').submit()"><fmt:message key="type" /></a>
			    	        </td>
			    	        <td><img src="../graphics/up.gif" border="0"></td>
			    	      </c:when>
			    	      <c:when test="${serviceSearch.sort == 'work_order_type'}">
			    	        <td class="listingsHdr3">
			    	        <a class="hdrLink" href="#" onClick="document.getElementById('sort').value='work_order_type DESC';document.getElementById('list').submit()"><fmt:message key="type" /></a>
			    	        </td>
			    	        <td><img src="../graphics/down.gif" border="0"></td>
			    	      </c:when>
			    	      <c:otherwise>
			    	        <td colspan="2" class="listingsHdr3">
			    	        <a class="hdrLink" href="#" onClick="document.getElementById('sort').value='work_order_type DESC';document.getElementById('list').submit()"><fmt:message key="type" /></a>
			    	        </td>
			    	      </c:otherwise>
			    	    </c:choose>  
			    	  </tr>
			    	  </table>
			    </td>
			  </tr>
			<c:forEach items="${list.pageList}" var="service" varStatus="status">
			  <tr class="row${status.index % 2}" onmouseover="changeStyleClass(this,'rowOver')" onmouseout="changeStyleClass(this,'row${status.index % 2}')">
			    <td class="indexCol"><c:out value="${status.count + list.firstElementOnPage}"/>.</td>
			    <td class="nameCol"><a href="service.jhtm?num=${service.serviceNum}" class="nameLink"><c:out value="${service.serviceNum}"/></a>
					<a href="pdf.jhtm?num=${service.serviceNum}"/><img src="../graphics/pdf<c:if test="${service.servicePdfUrl == null}">_light</c:if>.gif" border="0"></a></td>
			    <td class="nameCol">
			    	<c:if test="${service.hasWorkOrder}"><a href="workOrder.jhtm?num=${service.serviceNum}" class="nameLink"><c:out value="${service.serviceNum}"/></a> <a href="workOrderPrint.jhtm?num=${service.serviceNum}"><img src="../graphics/printer.png" border="0"></a>
				    <c:choose>
				    <c:when test="${service.printed}"><img src="../graphics/checkbox.png" alt="Printed" title="Printed" border="0"></c:when>
				    <c:otherwise><img src="../graphics/box.png" alt="Not Printed" title="" border="0"></c:otherwise>
				    </c:choose>			    	
			    	</c:if>
			    	<c:if test="${not service.hasWorkOrder}">&nbsp;</c:if>
			    </td>
			    <td align="center">
			    	<c:if test="${service.orderId != null}"><a href="../orders/invoice.jhtm?order=${service.orderId}" class="nameLink"><c:out value="${service.orderId}"/></a>
			    		<c:if test="${service.pdfUrl != null}"><a href="<c:url value="/assets/pdf/${service.pdfUrl}"/>" target="_blank"><img src="../graphics/pdf.gif" border="0"/></a></c:if>
						<c:if test="${siteConfig['INVOICE_PDF_UPLOAD'].value == 'true' and service.pdfUrl == null}">
							<a href="../orders/pdf.jhtm?orderId=${service.orderId}"/><img src="../graphics/pdf_light.gif" border="0"></a>
						</c:if>			    		
			    	</c:if>
			    	<c:if test="${service.orderId == null}">&nbsp;</c:if>
			    </td>
			    <td align="center"><c:out value="${service.item.address.company}"/></td>
			    <td align="center"><a href="item.jhtm?id=${service.item.id}" class="nameLink"><c:out value="${service.item.itemId}"/></a></td>
			    <td align="center"><fmt:formatDate type="date" timeStyle="default" value="${service.reportDate}"/></td>	
			    <td align="center"><fmt:formatDate type="date" timeStyle="default" value="${service.completeDate}"/></td>	
			    <td align="center"><c:if test="${not (service.status == 'Completed' or service.status == 'Canceled')}"><img src="../graphics/priority_${service.priority}.png" border="0"></c:if><c:out value="${service.status}"/></td>
			    <td align="center"><c:out value="${service.technician}"/></td>			    
			    <td align="center"><c:out value="${service.purchaseOrder}"/></td>
			    <td align="center">
			    	<c:out value="${service.wo3rd}"/>
			    	<c:if test="${service.wo3rdPdfUrl != null}">
			    	<a href="<c:url value="/assets/pdf/${service.wo3rdPdfUrl}"/>" target="_blank"/><img src="../graphics/pdf.gif" border="0"></a>
			    	</c:if>
				</td>
			    <td align="center"><c:out value="${service.manufWarranty}"/></td>			    
			    <td align="center"><c:out value="${service.type}"/></td>
			  </tr>
			</c:forEach>
			<c:if test="${list.nrOfElements == 0}">
			  <tr class="emptyList"><td colspan="2">&nbsp;</td></tr>
			</c:if>
			</table>
				  
			<table border="0" cellpadding="0" cellspacing="1" width="100%">
			  <tr>
			  <td class="pageSize">
			  <select name="size" onchange="document.getElementById('page').value=1;submit()">
			    <c:forTokens items="10,25,50,100" delims="," var="current">
			  	  <option value="${current}" <c:if test="${current == list.pageSize}">selected</c:if>>${current} <fmt:message key="perPage" /></option>
			    </c:forTokens>
			  </select>
			  </td>
			  </tr>
			</table>	  
		  	<!-- end input field -->  	  
		  	</div>
		  	
		  	<!-- start button --> 
		  	<!-- end button -->	
	  	
	<!-- end tab -->        
	</div>
<!-- end tabs -->			
</div>
  
  <!-- end table -->
   </td><td class="boxmidr" ></td></tr>
  <tr><td class="botl"></td><td class="botr"></td></tr>
  </table>
  
<!-- end main box -->  
</div>
</form>  
 
  </tiles:putAttribute>    
</tiles:insertDefinition>
</c:if>
