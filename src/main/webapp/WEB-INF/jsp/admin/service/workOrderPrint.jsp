<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
  <head>
    <title><fmt:message key="workOrder" /></title>

<style type="text/css">
<!--
body {
	font-size: 8pt;
	font-family: Verdana, Arial, Helvetica, sans-serif;
	color: #000000;
	background: #FFFFFF;
	margin:20px;
	}
.woHdr {
	border-bottom: 2px solid #000000;
	}
.woFtr {
	border-bottom: 2px solid #000000;
	}
-->
</style>    
    
  </head>  
<body>
<div style="width:700px;margin:left;">

<c:if test="${message != null}">
  <div class="message"><fmt:message key="${message}" /></div>
</c:if>

<c:if test="${workOrder != null}">

<c:set value="${workOrderLayout.headerHtml}" var="headerHtml"/>
<c:set value="${fn:replace(headerHtml, '#type#', workOrder.type)}" var="headerHtml"/>
<c:out value="${headerHtml}" escapeXml="false"/>
		  	
			<table border="0" align="center" cellspacing="3" cellpadding="1">
			<tr valign="top">
			<td align="right"><b><fmt:message key="service" /> <fmt:message key="location" />:</b></td>
		  	<td>
	  				<c:if test="${workOrder.service.item.address.company != ''}">
					<c:out value="${workOrder.service.item.address.company}"/><br/>
					</c:if>
					<c:out value="${workOrder.service.item.address.firstName}"/> <c:out value="${workOrder.service.item.address.lastName}"/><br/>
					<c:out value="${workOrder.service.item.address.addr1}"/><br/> 
					<c:if test="${workOrder.service.item.address.addr2 != ''}">
					<c:out value="${workOrder.service.item.address.addr2}"/><br/>
					</c:if>
					<c:out value="${workOrder.service.item.address.city}"/>, <c:out value="${workOrder.service.item.address.stateProvince}"/> <c:out value="${workOrder.service.item.address.zip}"/><br/>
					<c:out value="${countries[workOrder.service.item.address.country]}"/><br/>
					<c:if test="${workOrder.service.item.address.phone != ''}">
					Tel: <c:out value="${workOrder.service.item.address.phone}"/><br/>
					</c:if>
					<c:if test="${workOrder.service.item.address.fax != ''}">
					Fax: <c:out value="${workOrder.service.item.address.fax}"/><br/>
					</c:if>
					<c:if test="${workOrder.service.item.address.cellPhone != ''}">
					Cell: <c:out value="${workOrder.service.item.address.cellPhone}"/><br/>
					</c:if>
					<c:if test="${workOrder.service.item.address.email != ''}">
					<fmt:message key="email" />: <c:out value="${workOrder.service.item.address.email}"/><br/>
					</c:if>  	
		  	</td>
		  	</tr>
		  	<c:if test="${not empty workOrder.service.alternateContact}">
			<tr>
			<td align="right"><fmt:message key="alternateContact" />:</td>
		  	<td><c:out value="${workOrder.service.alternateContact}"/></td>
		  	</tr>
		  	</c:if>		  	
		  	</table>		  	
			
			<br/>			
			
			<table border="0" width="100%" cellspacing="3" cellpadding="1">
			<tr valign="top">
			<td>
			<table border="0">
			  <tr>
			    <td><b><fmt:message key="purchaseOrder" /></b></td>
			    <td>&nbsp;&nbsp;</td>
			    <td><c:out value="${workOrder.service.purchaseOrder}"/></td>
			  </tr>
			  <tr>
			    <td><b><fmt:message key="account"/> #</b></td>
			    <td>&nbsp;&nbsp;</td>
			    <td><c:out value="${workOrder.service.item.customer.accountNumber}"/></td>
			  </tr>
			  <tr>
			    <td><b><c:out value="${siteConfig['SERVICEABLE_ITEM_TITLE'].value}"/> ID/SN</b></td>
			    <td>&nbsp;&nbsp;</td>
			    <td><c:out value="${workOrder.service.item.itemId}"/> / <c:out value="${workOrder.service.item.serialNum}"/></td>
			  </tr>
			  <tr>
			    <td><b><fmt:message key="sku" /></b></td>
			    <td>&nbsp;&nbsp;</td>
			    <td><c:out value="${workOrder.service.item.sku}"/></td>
			  </tr>
			  <tr>
			    <td><b><fmt:message key="description" /></b></td>
			    <td>&nbsp;&nbsp;</td>
			    <td><c:out value="${product.name}"/></td>
			  </tr>
			  <c:forEach items="${product.productFields}" var="productField">
			  <tr>
			    <td><b><c:out value="${productField.name}"/></b></td>
			    <td>&nbsp;&nbsp;</td>
			    <td><c:out value="${productField.value}"/></td>
			  </tr>
			  </c:forEach>			  
			  <tr>
			    <td><b><fmt:message key="technician" /></b></td>
			    <td>&nbsp;&nbsp;</td>
			    <td><c:out value="${workOrder.technician}"/></td>
			  </tr>
			  <tr>
			    <td><b><fmt:message key="problem" /></b></td>
			    <td>&nbsp;&nbsp;</td>
			    <td><c:out value="${workOrder.service.problem}"/></td>
			  </tr>			  
			</table>
			</td>
			<td>&nbsp;</td>
			<td align="right">
			<table>
			  <tr>
			    <td><b><fmt:message key="workOrder" /> #</b></td>
			    <td>&nbsp;&nbsp;</td>
			    <td><c:out value="${workOrder.service.serviceNum}"/></td>
			  </tr>			
			  <tr>
			    <td><b><fmt:message key="reportDate" /></b></td>
			    <td>&nbsp;&nbsp;</td>
			    <td><fmt:formatDate type="both" timeStyle="full" value="${workOrder.service.reportDate}"/></td>
			  </tr>
			  <tr>
			    <td><b><fmt:message key="workOrder" /> <fmt:message key="date" /></b></td>
			    <td>&nbsp;&nbsp;</td>
			    <td><fmt:formatDate type="date" timeStyle="default" value="${workOrder.date}"/></td>
			  </tr>
			  <tr>
			    <td><b><fmt:message key="service" /> <fmt:message key="date" /></b></td>
			    <td>&nbsp;&nbsp;</td>
			    <td><fmt:formatDate type="date" timeStyle="default" value="${workOrder.serviceDate}"/></td>
			  </tr>
			  <tr>
			    <td><b><fmt:message key="startTime" /></b></td>
			    <td>&nbsp;&nbsp;</td>
			    <td><fmt:formatDate type="time" timeStyle="short" value="${workOrder.startTime}"/></td>
			  </tr>
			  <tr>
			    <td><b><fmt:message key="stopTime" /></b></td>
			    <td>&nbsp;&nbsp;</td>
			    <td><fmt:formatDate type="time" timeStyle="short" value="${workOrder.stopTime}"/></td>
			  </tr>
			  <tr>
			    <td><b>Install %</b></td>
			    <td>&nbsp;&nbsp;</td>
			    <td><c:out value="${workOrder.installPercentage}"/></td>
			  </tr>
			  <c:if test="${workOrder.outSN != null and fn:trim(workOrder.outSN) != ''}">
			  <tr>
			    <td><b>Out SN</b></td>
			    <td>&nbsp;&nbsp;</td>
			    <td><c:out value="${workOrder.outSN}"/></td>
			  </tr>
			  </c:if>
			  <c:if test="${workOrder.inSN != null and fn:trim(workOrder.inSN) != ''}">
			  <tr>
			    <td><b>In SN</b></td>
			    <td>&nbsp;&nbsp;</td>
			    <td><c:out value="${workOrder.inSN}"/></td>
			  </tr>
			  </c:if>
			  <tr>
			    <td><b><fmt:message key="bwCount"/></b></td>
			    <td>&nbsp;&nbsp;</td>
			    <td><fmt:formatNumber value="${workOrder.bwCount}" pattern="#,##0"/></td>
			  </tr>
			  <tr>
			    <td><b><fmt:message key="colorCount"/></b></td>
			    <td>&nbsp;&nbsp;</td>
			    <td><fmt:formatNumber value="${workOrder.colorCount}" pattern="#,##0"/></td>
			  </tr>
			</table>
			</td>
			</tr>
			</table>

			<br/>
						
			<table border="0" cellpadding="2" cellspacing="0" width="100%">
			  <tr>
			    <c:set var="cols" value="0"/>
			    <td width="15%" class="woHdr"><fmt:message key="itemCode" /></td>
			    <td class="woHdr"><fmt:message key="description" /></td>
			    <td width="10%" class="woHdr" align="center">In SN</td>
			    <td width="10%" class="woHdr" align="center">Out SN</td>
			    <td width="5%" class="woHdr" align="center"><fmt:message key="qty" /></td>
			    <c:if test="${workOrder.hasPacking}">
				  <c:set var="cols" value="${cols+1}"/>
			      <td width="10%" class="woHdr"><fmt:message key="packing" /></td>
			    </c:if>
			    <c:if test="${workOrder.hasContent}">
				  <c:set var="cols" value="${cols+1}"/>
			      <td class="woHdr"><fmt:message key="content" /></td>
			    </c:if>
			  </tr>
			<c:forEach var="lineItem" items="${workOrder.lineItems}" varStatus="status">
			<tr valign="top">
			  <td><c:out value="${lineItem.product.sku}"/></td>
			  <td><c:out value="${lineItem.product.name}"/></td>
			  <td align="center"><c:out value="${lineItem.inSN}" /></td>
			  <td align="center"><c:out value="${lineItem.outSN}" /></td>
			  <td align="center"><c:out value="${lineItem.quantity}" /></td>
			  <c:if test="${workOrder.hasPacking}">
			  <td align="center"><c:out value="${lineItem.product.packing}" /></td>
			  </c:if>
			  <c:if test="${workOrder.hasContent}">
			    <td align="center"><c:out value="${lineItem.product.caseContent}" /></td>
			  </c:if>
			</tr>
			<tr>
			  <td align="right"><b><fmt:message key="workNotes" />:</b>&nbsp;&nbsp;&nbsp;</td>
			  <td colspan="${cols}"><c:out value="${lineItem.notes}" /></td>
			</tr>
			<tr><td colspan="${5+cols}">&nbsp;</td></tr>
			</c:forEach>
			<c:if test="${fn:toLowerCase(workOrder.service.status) != 'completed'}">
			<tr><td colspan="${5+cols}">&nbsp;</td></tr>
			<tr><td colspan="${5+cols}">&nbsp;</td></tr>
			<tr><td colspan="${5+cols}">&nbsp;</td></tr>			
			</c:if>
			  <tr>
			    <td colspan="${5+cols}" class="woFtr">&nbsp;</td>
			  </tr>
			</table>	

			<br/>
			
			<c:if test="${fn:trim(workOrder.service.comments) != ''}">
			<table border="0" cellspacing="3" cellpadding="1">
			  <tr>
			    <td valign="top"><b><fmt:message key="comments" />:</b></td>
			    <td><c:out value="${workOrder.service.comments}"/></td>
			  </tr>
			</table>			
			</c:if>
			
			<table border="0" cellspacing="3" cellpadding="1">
			  <tr>
			    <td><b><fmt:message key="status" />:</b></td>
			    <td><c:out value="${workOrder.service.status}"/></td>
			  </tr>
			  <c:if test="${not (workOrder.service.status == 'Completed' or workOrder.service.status == 'Canceled')}">
			  <tr>
			    <td><b><fmt:message key="priority" />:</b></td>
			    <td><img src="../graphics/priority_${workOrder.service.priority}.png" border="0"></td>
			  </tr>
			  </c:if>
			</table>			
						
<c:set value="${workOrderLayout.footerHtml}" var="footerHtml"/>
<c:set value="${fn:replace(footerHtml, '#type#', workOrder.type)}" var="footerHtml"/>
<c:out value="${footerHtml}" escapeXml="false"/>
 
</c:if>

</div>

</body>
</html>