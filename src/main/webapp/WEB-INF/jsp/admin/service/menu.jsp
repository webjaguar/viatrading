<%@ page import="java.util.*" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<script type="text/javascript">
<!--
window.addEvent('domready', function(){
	mbWO = new multiBox('mbWorkOrder', {descClassName: 'descClassName',showNumbers: false,overlay: new overlay()});
	<c:if test="${model.incompleteWO}">
	mbWO.open($('mbWorkOrder'));
	</c:if>
});
//-->
</script>

  <div id="lbox">
   <table cellpadding="0" cellspacing="0" border="0" class="module" style="width: 100%"><tr><td class="topl">
    </td><td class="topr" ></td></tr><tr><td class="boxmid" >

    <h2 class="menuleft mfirst"><fmt:message key="service"/></h2>
    <div class="menudiv"></div>
      <a href="services.jhtm" class="userbutton"><fmt:message key="requests"/></a>
      <a href="items.jhtm" class="userbutton"><c:out value="${siteConfig['SERVICEABLE_ITEM_TITLE'].value}"/>s</a>
      <a href="incompleteWorkOrders.jhtm" rel="width:850,height:400" class="mbWorkOrder userbutton" id="mbWorkOrder" title="<fmt:message key="incompleteWorkOrders" />"><fmt:message key="incomplete" /> WO</a>
	<div class="menudiv"></div> 
	
    <c:if test="${tab == 'items'}"> 
    <div class="menudiv"></div>
      <div align="left" class="searchHeader" id="searchHeaderId"><h2><fmt:message key="search" /></h2></div>
	  <table class="searchBoxLeft"  width="100%"><tr><td>
	  <form action="items.jhtm" name="searchform" method="post">
	  <!-- content area --> 
	    <div class="search2">
	      <p><c:out value="${siteConfig['SERVICEABLE_ITEM_TITLE'].value}"/> ID:</p>
          <input name="item_id" type="text" value="<c:out value='${serviceableItemSearch.itemId}' />" size="15" />
	    </div>     
	    <div class="search2">
	      <p><fmt:message key="S/N" />:</p>
          <input name="s/n" type="text" value="<c:out value='${serviceableItemSearch.serialNum}' />" size="15" />
	    </div>
	    <div class="search2">
	      <p><fmt:message key="sku" />:</p>
          <input name="sku" type="text" value="<c:out value='${serviceableItemSearch.sku}' />" size="15" />
	    </div>
	    <div class="search2">
          <p><fmt:message key="companyName" />:</p>
          <input name="companyName" type="text" value="<c:out value='${serviceableItemSearch.companyName}' />" size="15" />
        </div>      
	    <div class="button">
	      <input type="submit" value="Search"/>
	    </div>
	  <!-- content area -->
	  </form>
	  </td></tr></table>
	<div class="menudiv"></div>  
    </c:if>
    
    <c:if test="${tab == 'services'}"> 
    <div class="menudiv"></div>
      <div align="left" class="searchHeader" id="searchHeaderId"><h2><fmt:message key="search" /></h2></div>
	  <table class="searchBoxLeft"  width="100%"><tr><td>
	  <form action="services.jhtm" name="searchform" method="post">
	  <!-- content area --> 
	    <div class="search2">
	      <p><fmt:message key="serviceRequest" />:</p>
          <input name="serviceNum" type="text" value="<c:out value='${serviceSearch.serviceNum}' />" size="15" />
	    </div>
	    <div class="search2">
	      <p><c:out value="${siteConfig['SERVICEABLE_ITEM_TITLE'].value}"/> ID:</p>
          <input name="item_id_like" type="text" value="<c:out value='${serviceSearch.itemIdLike}' />" size="15" />
	    </div>     
	    <div class="search2">
	      <p><fmt:message key="status"/>:</p>
		  <select name="status">
          <option value=""><fmt:message key="all" /></option>
		  <c:forEach var="status" items="${statuses}">
            <option value="${status}" <c:if test="${serviceSearch.status == status}">selected</c:if>><c:out value="${status}"/></option>
          </c:forEach>  
          </select>
	    </div>    
	    <div class="search2">
	      <p>P.O. #:</p>
	      <input name="purchaseOrder" type="text" value="<c:out value='${serviceSearch.purchaseOrder}' />" size="15" />
	    </div>
	    <div class="search2">
          <p><fmt:message key="companyName" />:</p>
          <input name="companyName" type="text" value="<c:out value='${serviceSearch.companyName}' />" size="15" />
        </div> 
	    <div class="search2">
          <p><fmt:message key="workOrder" /> <fmt:message key="type" />:</p>
          <input name="workOrderType" type="text" value="<c:out value='${serviceSearch.workOrderType}' />" size="15" />
        </div>
	    <div class="search2">
          <p><fmt:message key="technician" />:</p>
          <input name="technician" type="text" value="<c:out value='${serviceSearch.technician}' />" size="15" />
        </div>
	    <div class="search2">
          <p><fmt:message key="3rdParty"/> W.O. #:</p>
          <input name="wo3rd" type="text" value="<c:out value='${serviceSearch.wo3rd}' />" size="15" />
        </div>        
	    <div class="search2">
          <p><fmt:message key="supplier" /> P.O. #:</p>
          <input name="poSupplier" type="text" value="<c:out value='${serviceSearch.poSupplier}' />" size="15" />
        </div>
	    <div class="search2">
          <p>Reference #:</p>
          <input name="refNum" type="text" value="<c:out value='${serviceSearch.refNum}' />" size="15" />
        </div>        
	    <div class="search2">
          <p>Mfg. Warranty Ticket #:</p>
          <input name="manufWarranty" type="text" value="<c:out value='${serviceSearch.manufWarranty}' />" size="15" />
        </div>
        
	    <div class="button">
	      <input type="submit" value="Search"/>
	    </div>
	  <!-- content area -->
	  </form>
	  </td></tr></table>
	<div class="menudiv"></div>  
    </c:if>

    </td><td class="boxmidr" ></td></tr>
    <tr><td class="botl"></td><td class="botr"></td></tr></table>
  </div>