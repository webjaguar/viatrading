<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<c:if test="${gSiteConfig['gSERVICE']}">
<tiles:insertDefinition name="admin.service" flush="true">
  <tiles:putAttribute name="content" type="string">
<script language="JavaScript"> 
<!--
window.addEvent('domready', function(){			
			var tabs1 = new SimpleTabs($('tab-block-1'), { selector: 'h4'});
		});
//--> 
</script>   
<form action="setOwner.jhtm?id=${model.item.id}" id="list" method="post">
<input type="hidden" id="sort" name="sort" value="${serviceableItemOwnerSearch.sort}">
  <!-- main box -->
  <div id="mboxfull">
  
  <!-- start table -->
  <table cellpadding="0" cellspacing="0" border="0" class="module" >
  <tr>
    <td class="topl_g">
	  
	  <!-- breadcrumb -->
	  <p class="breadcrumb">
	    <a href="../service">Service</a> &gt;
	    <c:out value="${siteConfig['SERVICEABLE_ITEM_TITLE'].value}"/>
	  </p>
	  
	  <!-- header -->
	  <div class="message2"><fmt:message key="setOwnerOf" /> <c:out value="${siteConfig['SERVICEABLE_ITEM_TITLE'].value}"/> <c:out value="${model.item.itemId}"/> <c:out value="${model.item.serialNum}"/></div>
	  
	  <!-- Error Message -->
	  
	  </td><td class="topr_g" ></td></tr>
  <tr><td class="boxmidlrg" >

 <!-- tabs -->
 <div id="tab-block-1">
	<h4 title="List of affiliates">List</h4>
	<div>
	<!-- start tab -->

	  	<div class="listdivi ln tabdivi"></div>
	  	<div class="listdivi"></div>
	   
		  	<div class="listlight">
		  	<!-- input field --> 
			<table border="0" cellpadding="0" cellspacing="1" width="100%">
			  <tr>
			  <td>
				<select name="parent"
				  onChange="document.getElementById('page').value=1;submit()">
					<option value=""><fmt:message key="all" /> <fmt:message key="accounts" /></option>
					<option value="-1" <c:if test="${-1 == serviceableItemOwnerSearch['parent']}">selected</c:if>>
						<fmt:message key="main" /> <fmt:message key="accounts" /></option>	
					<c:forEach items="${model.familyTree}" var="customer">
					<option value="${customer.id}"
					<c:if test="${customer.id == serviceableItemOwnerSearch['parent']}">selected</c:if>>
					<fmt:message key="subAccounts" /> <fmt:message key="of" /> <c:out value="${customer.username}" /> 
				  </option>
				</c:forEach>
				</select>
			  </td>
			  <td>
			  <c:if test="${model.customers.nrOfElements > 0}">
			  <td class="pageShowing">
			  <fmt:message key="showing">
				<fmt:param value="${model.customers.firstElementOnPage + 1}"/>
				<fmt:param value="${model.customers.lastElementOnPage + 1}"/>
				<fmt:param value="${model.customers.nrOfElements}"/>
			  </fmt:message>
			  </td>
			  </c:if>
			  <td class="pageNavi">
			  Page 
			  <select id="page" name="page" onchange="submit()">
			  <c:forEach begin="1" end="${model.customers.pageCount}" var="page">
			  	<option value="${page}" <c:if test="${page == (model.customers.page+1)}">selected</c:if>>${page}</option>
			  </c:forEach>
			  </select>
			  of <c:out value="${model.customers.pageCount}"/>
			  | 
			  <c:if test="${model.customers.firstPage}"><span class="pageNaviDead"><fmt:message key="previous" /></span></c:if>
			  <c:if test="${not model.customers.firstPage}"><a href="#" onClick="document.getElementById('page').value='${model.customers.page}';document.getElementById('list').submit()" class="pageNaviLink"><fmt:message key="previous" /></a></c:if>
			  | 
			  <c:if test="${model.customers.lastPage}"><span class="pageNaviDead"><fmt:message key="next" /></span></c:if>
			  <c:if test="${not model.customers.lastPage}"><a href="#" onClick="document.getElementById('page').value='${model.customers.page+2}';document.getElementById('list').submit()" class="pageNaviLink"><fmt:message key="next" /></a></c:if>
			  </td>
			  </tr>
			</table>
			
			<table border="0" cellpadding="0" cellspacing="0" width="100%" class="listings">
			  <tr class="listingsHdr2">
			    <c:set var="cols" value="0"/>
			    <td width="15px">&nbsp;</td>
			    <td>
			    	<table cellspacing="0" cellpadding="1">
			    	  <tr>
			    	    <td rowspan="2" class="listingsHdr3"><fmt:message key="lastName" /></td>
			    	    <td><c:choose><c:when test="${serviceableItemOwnerSearch.sort == 'last_name, first_name'}"><img src="../graphics/down_off.gif" border="0"></c:when><c:otherwise><a href="#" onClick="document.getElementById('sort').value='last_name, first_name';document.getElementById('list').submit()"><img src="../graphics/down.gif" border="0"></a></c:otherwise></c:choose></td>
			    	  </tr>
			    	</table>
			    </td>
			    <td>
			    	<table cellspacing="0" cellpadding="1">
			    	  <tr>
			    	    <td rowspan="2" class="listingsHdr3"><fmt:message key="firstName" /></td>
			    	    <td><c:choose><c:when test="${serviceableItemOwnerSearch.sort == 'first_name, last_name'}"><img src="../graphics/down_off.gif" border="0"></c:when><c:otherwise><a href="#" onClick="document.getElementById('sort').value='first_name, last_name';document.getElementById('list').submit()"><img src="../graphics/down.gif" border="0"></a></c:otherwise></c:choose></td>
			    	  </tr>
			    	</table>
			    </td>
			    <td>
			    	<table cellspacing="0" cellpadding="1">
			    	  <tr>
			    	    <td rowspan="2" class="listingsHdr3"><fmt:message key="emailAddress" /></td>
			    	    <td><c:choose><c:when test="${serviceableItemOwnerSearch.sort == 'username'}"><img src="../graphics/down_off.gif" border="0"></c:when><c:otherwise><a href="#" onClick="document.getElementById('sort').value='username';document.getElementById('list').submit()"><img src="../graphics/down.gif" border="0"></a></c:otherwise></c:choose></td>
			    	  </tr>
			    	</table>
			    </td>
			    <td>
			    	<table cellspacing="0" cellpadding="1">
			    	  <tr>
			    	    <td rowspan="2" class="listingsHdr3"><fmt:message key="company" /></td>
			    	    <td><c:choose><c:when test="${serviceableItemOwnerSearch.sort == 'company'}"><img src="../graphics/down_off.gif" border="0"></c:when><c:otherwise><a href="#" onClick="document.getElementById('sort').value='company';document.getElementById('list').submit()"><img src="../graphics/down.gif" border="0"></a></c:otherwise></c:choose></td>
			    	  </tr>
			    	</table>
			    </td>
			    <td>
			    	<table cellspacing="0" cellpadding="1">
			    	  <tr>
			    	    <td rowspan="2" class="listingsHdr3"><fmt:message key="accountNumber" /></td>
			          </tr>
			    	</table>
			    </td>
			    <td align="center">
			    	<table cellspacing="0" cellpadding="1">
			    	  <tr>
			    	    <td rowspan="2" class="listingsHdr3"><fmt:message key="accountCreated" /></td>
			    	    <td><c:choose><c:when test="${serviceableItemOwnerSearch.sort == 'created DESC'}"><img src="../graphics/up_off.gif" border="0"></c:when><c:otherwise><a href="#" onClick="document.getElementById('sort').value='created DESC';document.getElementById('list').submit()"><img src="../graphics/up.gif" border="0"></a></c:otherwise></c:choose></td>
			    	  </tr>
			    	  <tr>
			    	    <td><c:choose><c:when test="${serviceableItemOwnerSearch.sort == 'created'}"><img src="../graphics/down_off.gif" border="0"></c:when><c:otherwise><a href="#" onClick="document.getElementById('sort').value='created';document.getElementById('list').submit()"><img src="../graphics/down.gif" border="0"></a></c:otherwise></c:choose></td>
			    	  </tr>
			    	</table>
			    </td>
			    <td>&nbsp;</td>
			  </tr>
			<c:forEach items="${model.customers.pageList}" var="customer" varStatus="status">
			  <tr class="row${status.index % 2}">
			    <td width="15px"><input type="radio" name="uid" value="${customer.id}" <c:if test="${model.item.customer.id == customer.id}">checked<c:set var="found" value="true"/></c:if>></td>
			    <td class="nameCol"><c:out value="${customer.address.lastName}"/></td>
			    <td class="nameCol"><c:out value="${customer.address.firstName}"/></td>
			    <td><c:out value="${customer.username}"/></td>
			    <td class="nameCol"><c:out value="${customer.address.company}"/></td>
			    <td><c:out value="${customer.accountNumber}"/></td>
			    <td align="center"><fmt:formatDate type="date" timeStyle="default" value="${customer.created}"/></td>
				<td><c:choose>
				 <c:when test="${customer.subCount != 0}">
				 <a href="<c:url value="setOwner.jhtm"><c:param name="id" value="${param.id}"/><c:param name="parent" value="${customer.id}"/></c:url>"
												class="countLink"><fmt:message key="subAccounts" /> (<b><c:out
													value="${customer.subCount}" /></b>)</a></c:when>
				 <c:otherwise><div class="countLinkOff"><fmt:message key="subAccounts" /> (<b><c:out
													value="${customer.subCount}" /></b>)</div></c:otherwise>	
				</c:choose></td>
			  </tr>
			</c:forEach>
			<c:if test="${model.customers.nrOfElements == 0}">
			  <tr class="emptyList"><td colspan="9">&nbsp;</td></tr>
			</c:if>
			</table>
			
			<table border="0" cellpadding="0" cellspacing="0" width="100%">
			  <tr>
			  <td>
				<input type="radio" name="uid" value="0" <c:if test="${model.item.customer.id == null}">checked</c:if>> <fmt:message key="none" />
				<c:if test="${found != true and model.item.customer.id != null}">
				<input type="radio" name="uid" value="${model.item.customer.id}" checked> <c:out value="${model.customer.username}"/>
				</c:if>
			  </td>
			  <td class="pageSize">
			  <select name="size" onchange="document.getElementById('page').value=1;submit()">
			  <c:forTokens items="10,25,50" delims="," var="current">
			  	  <option value="${current}" <c:if test="${current == model.customers.pageSize}">selected</c:if>>${current} <fmt:message key="perPage" /></option>
			  </c:forTokens>
			  </select>
			  </td>
			  </tr>
			</table>  		  	
		  	<!-- end input field -->  	  
		  	</div>
		  	
		  	<!-- start button -->
		  	<div align="left" class="button"> 
		  	  <input type="submit" name="__update" value="<fmt:message key="update" />">
    		  <input type="submit" name="__cancel" value="<fmt:message key="cancel" />"> 
		  	</div>
		  	<!-- end button -->	
	  	
	<!-- end tab -->        
	</div>
<!-- end tabs -->			
</div>
  
  <!-- end table -->
   </td><td class="boxmidr" ></td></tr>
  <tr><td class="botl"></td><td class="botr"></td></tr>
  </table>
  
<!-- end main box -->  
</div>
</form>  	  

  </tiles:putAttribute>    
</tiles:insertDefinition>
</c:if>
