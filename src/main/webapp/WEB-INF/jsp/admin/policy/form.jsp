<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="/WEB-INF/tlds/spring.tld" prefix="spring" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ taglib uri="http://www.springframework.org/security/tags" prefix="sec" %>


<tiles:insertDefinition name="admin.policy" flush="true">
  <tiles:putAttribute name="content" type="string">

<script type="text/javascript" src="<c:url value="/FCKeditor/fckeditor.js"/>"></script>

<script language="JavaScript"> 
<!--
function loadEditor(el) {
  var oFCKeditor = new FCKeditor(el);
  oFCKeditor.BasePath = "<c:url value="/FCKeditor/"/>";
  oFCKeditor.Config["CustomConfigurationsPath"] = '<c:url value="/admin/javascript/fckconfig.js"/>';
  oFCKeditor.ToolbarSet = 'Html';
  oFCKeditor.Width = 650;
  oFCKeditor.Height = 400;
  oFCKeditor.ReplaceTextarea();
  document.getElementById(el + '_link').style.display="none";
}
//--> 
</script> 
<script language="JavaScript"> 
<!--
window.addEvent('domready', function(){			
			var tabs1 = new SimpleTabs($('tab-block-1'), { selector: 'h4'});
		});
//--> 
</script>  
<form:form commandName="policyForm" action="policy.jhtm" method="post">  
<!-- main box -->
  <div id="mboxfull">
  
  <!-- start table -->
  <table cellpadding="0" cellspacing="0" border="0" class="module" >
  <tr>
    <td class="topl_g">
	  
	  <!-- breadcrumb -->
	  <p class="breadcrumb">
	    <a href="../policy">Policy</a> &gt;
	    ${policyForm.policy.name} 
	  </p>
	  
	  <!-- Error Message -->
	  <%--<c:if test="${message != null}">
  		<div class="message"><fmt:message key="${message}"/></div>
  	  </c:if> --%>
	  <spring:hasBindErrors name="policyForm">
		<div class="message">Please fix all errors!</div>
	  </spring:hasBindErrors>
		
		<c:if test="${policyForm.policy.rank != null}">
		<spring:bind path="policyForm.policy.rank"><input type="hidden" name="<c:out value="${status.expression}"/>" value="<c:out value="${status.value}"/>"></spring:bind>
		</c:if>
		<c:if test="${!policyForm.newPolicy}">
		<spring:bind path="policyForm.policy.id"><input type="hidden" name="<c:out value="${status.expression}"/>" value="<c:out value="${status.value}"/>"></spring:bind>
		</c:if>
		
  </td><td class="topr_g" ></td></tr>
  <tr><td class="boxmidlrg" >

 <!-- tabs -->
 <div id="tab-block-1">
	<h4 title="Policy">Policy</h4>
	<div>
	<!-- start tab -->
	<p>
         <c:set var="classIndex" value="0" />
         
	  	<div class="listdivi ln tabdivi"></div>
	  	<div class="listdivi"></div>
	   
		  	<div class="list${classIndex % 2}"><c:set var="classIndex" value="${classIndex+1}" />
		  	<div class="listfl"><div class="requiredField"><fmt:message key="policyName" />:</div></div>
		  	<div class="listp">
		  	<!-- input field -->
				<form:input path="policy.name" maxlength="100" size="60" htmlEscape="true"/>
				<form:errors path="policy.name" cssClass="error"/>
	 		<!-- end input field -->	
	 		</div>
		  	</div>			

		  	<div class="list${classIndex % 2}"><c:set var="classIndex" value="${classIndex+1}" />
		  	<div class="listfl"><fmt:message key="policyContent" />:</div>
		  	<div class="listp">
		  	<!-- input field -->
				<table class="form" border="0" cellpadding="0" cellspacing="0"><tr><td>
				<form:textarea rows="8" cols="60" path="policy.content" htmlEscape="true"/>
				<form:errors path="policy.content" cssClass="error"/>
				<div style="text-align:right" id="policy.content_link"><a href="#" onClick="loadEditor('policy.content')" class="htmleditor">- <fmt:message key="htmlEditor" /> -</a></div>
				</td></tr> </table>
	 		<!-- end input field -->	
	 		</div>
		  	</div>
	</p>	  		
	<!-- end tab -->        
	</div>         
<!-- end tabs -->			
</div>

<!-- start button -->
	<div align="left" class="button"> 
	  <c:if test="${policyForm.newPolicy}">
	    <sec:authorize ifAnyGranted="ROLE_AEM,ROLE_ADMIN_${prop_site_id},ROLE_POLICY_CREATE">
          <input type="submit" value="<spring:message code="policyAdd"/>" />
        </sec:authorize>  
      </c:if>
      <c:if test="${!policyForm.newPolicy}">
        <sec:authorize ifAnyGranted="ROLE_AEM,ROLE_ADMIN_${prop_site_id},ROLE_POLICY_EDIT">
          <input type="submit" value="<spring:message code="policyUpdate"/>" />
        </sec:authorize>
        <sec:authorize ifAnyGranted="ROLE_AEM,ROLE_ADMIN_${prop_site_id},ROLE_POLICY_DELETE">
          <input type="submit" value="<spring:message code="policyDelete"/>" name="_delete" onClick="return confirm('Delete permanently?')" />
        </sec:authorize>  
      </c:if>	
	</div>
<!-- end button -->	  

  <!-- end table -->
    </td><td class="boxmidr" ></td></tr>
  <tr><td class="botl"></td><td class="botr"></td></tr>
  </table>
  
<!-- end main box -->  
 </div>
</form:form>  

  </tiles:putAttribute>    
</tiles:insertDefinition>