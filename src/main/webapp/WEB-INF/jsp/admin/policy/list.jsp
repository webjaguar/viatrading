<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/security/tags" prefix="sec" %>


<tiles:insertDefinition name="admin.policy" flush="true">
  <tiles:putAttribute name="content" type="string"> 
  
<form action="policyList.jhtm" method="get">
  <!-- main box -->
  <div id="mboxfull">
  
  <!-- start table -->
  <table cellpadding="0" cellspacing="0" border="0" class="module" >
  <tr>
    <td class="topl_g">
	  
	  <!-- breadcrumb -->
	  <p class="breadcrumb">
	    <a href="../policy">Policy</a> &gt;
	    policies 
	  </p>
	  
	  <!-- Error Message -->
	  
	  <!-- header image -->
	  <img class="headerImage" src="../graphics/policy.gif" />
	  
	  </td><td class="topr_g" ></td></tr>
  <tr><td class="boxmidlrg" >

 <!-- tabs -->
 <div class="tab-wrapper">
	<h4 title="List of affiliates"></h4>
	<div>
	<!-- start tab -->
	<p>
	  	<div class="listdivi ln tabdivi"></div>
	  	<div class="listdivi"></div>
	   
		  	<div class="listlight">
		  	<!-- input field --> 
		  	<table border="0" cellpadding="0" cellspacing="1" width="100%">
			  <tr>
			  <c:if test="${model.policies.nrOfElements > 0}">
			  <td class="pageShowing">
			  <fmt:message key="showing">
				<fmt:param value="${model.policies.firstElementOnPage + 1}"/>
				<fmt:param value="${model.policies.lastElementOnPage + 1}"/>
				<fmt:param value="${model.policies.nrOfElements}"/>
			  </fmt:message>
			  </td>
			  </c:if>
			  <td class="pageNavi">
			  Page 
			  <c:forEach begin="1" end="${model.policies.pageCount}" var="page">
				<select name="page" onchange="submit()">
			  	<option value="${page}" <c:if test="${page == (model.policies.page+1)}">selected</c:if>>${page}</option>
				</select>
			  </c:forEach>
			  of <c:out value="${model.policies.pageCount}"/>
			  | 
			  <c:if test="${model.policies.firstPage}"><span class="pageNaviDead">previous</span></c:if>
			  <c:if test="${not model.policies.firstPage}"><a href="<c:url value="policyList.jhtm"><c:param name="page" value="${model.policies.page}"/></c:url>" class="pageNaviLink">previous</a></c:if>
			  | 
			  <c:if test="${model.policies.lastPage}"><span class="pageNaviDead">next</span></c:if>
			  <c:if test="${not model.policies.lastPage}"><a href="<c:url value="policyList.jhtm"><c:param name="page" value="${model.policies.page+2}"/></c:url>" class="pageNaviLink">next</a></c:if>
			  </td>
			  </tr>
			</table>
			
			<table border="0" cellpadding="0" cellspacing="0" width="100%" class="listings">
			  <tr class="listingsHdr2">
			    <td class="indexCol">&nbsp;</td>
			    <td class="listingsHdr3"><fmt:message key="policyName" /></td>
			    <td class="listingsHdr3" align="center"><fmt:message key="rank" /></td>
			  </tr>
			<c:forEach items="${model.policies.pageList}" var="policy" varStatus="status">
			  <tr class="row${status.index % 2}" onmouseover="changeStyleClass(this,'rowOver')" onmouseout="changeStyleClass(this,'row${status.index % 2}')" >
			    <td class="indexCol"><c:out value="${status.count + model.policies.firstElementOnPage}"/>.</td>
			    <td class="nameCol"><a href="policy.jhtm?id=${policy.id}" class="nameLink"><c:out value="${policy.name}"/></a></td>
			    <td class="rankCol"><input name="__rank_${policy.id}" type="text" value="${policy.rank}" size="5" maxlength="5" class="rankField"></td>
			  </tr>
			</c:forEach>
			<c:if test="${model.policies.nrOfElements == 0}">
			  <tr class="emptyList"><td colspan="3">&nbsp;</td></tr>
			</c:if>
			</table>
		  	<!-- end input field -->  	  
		  	</div>
		  	
		  	<!-- start button --> 
		  	<div align="left" class="button">
			  <sec:authorize ifAnyGranted="ROLE_AEM,ROLE_ADMIN_${prop_site_id},ROLE_POLICY_CREATE">
			    <input type="submit" name="__add" value="<fmt:message key="policyAdd" />">
			  </sec:authorize>
			  <input type="submit" name="__update_ranking" value="<fmt:message key="updateRanking" />">
			</div>
		  	<!-- end button -->	
	</p>	  	
	<!-- end tab -->        
	</div>
<!-- end tabs -->			
</div>
  
  <!-- end table -->
   </td><td class="boxmidr" ></td></tr>
  <tr><td class="botl"></td><td class="botr"></td></tr>
  </table>
  
<!-- end main box -->  
</div>
</form>  

  </tiles:putAttribute>    
</tiles:insertDefinition>