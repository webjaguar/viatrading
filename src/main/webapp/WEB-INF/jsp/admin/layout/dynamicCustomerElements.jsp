<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>

		  	<div class="list${classIndex % 2}"><c:set var="classIndex" value="${classIndex+1}" />
		  	<div class="listfl">Dynamic Elements:</div>
		  	<div class="listp">
		  	<!-- input field -->
		  		 <table  width="100%">
		  		 <tr><td valign="top" width="33%">
		  	     <table>
		  	     <tr><td>#firstname# </td><td>--&gt;</td><td><fmt:message key="customer's" /> <fmt:message key="firstName" /></td></tr>
		  	     <tr><td>#lastname# </td><td>--&gt;</td><td><fmt:message key="customer's" /> <fmt:message key="lastName" /></td></tr>
		  	     <tr><td>#email# </td><td>--&gt;</td><td><fmt:message key="customer's" /> <fmt:message key="email" /></td></tr>
		  	     <tr><td>#accountnumber# </td><td>--&gt;</td><td><fmt:message key="customer's" /> <fmt:message key="accountNumber" /></td></tr>
		  	     <tr><td>#company# </td><td>--&gt;</td><td><fmt:message key="customer's" /> <fmt:message key="company" /></td></tr>
		  	     <tr><td>#addr1# </td><td>--&gt;</td><td><fmt:message key="customer's" /> <fmt:message key="address" /> 1</td></tr>
		  	     <tr><td>#addr2# </td><td>--&gt;</td><td><fmt:message key="customer's" /> <fmt:message key="address" /> 2</td></tr>
		  	     <tr><td>#city# </td><td>--&gt;</td><td><fmt:message key="customer's" /> <fmt:message key="city" /></td></tr>
		  	     <tr><td>#state# </td><td>--&gt;</td><td><fmt:message key="customer's" /> <fmt:message key="stateProvince" /></td></tr>
		  	     <tr><td>#zip# </td><td>--&gt;</td><td><fmt:message key="customer's" /> <fmt:message key="zipCode" /></td></tr>
		  	     <tr><td>#country# </td><td>--&gt;</td><td><fmt:message key="customer's" /> <fmt:message key="country" /></td></tr>
		  	     <tr><td>#phone# </td><td>--&gt;</td><td><fmt:message key="customer's" /> <fmt:message key="phone" /></td></tr>
		  	     <tr><td>#fax# </td><td>--&gt;</td><td><fmt:message key="customer's" /> <fmt:message key="fax" /></td></tr>
		  	     <tr><td>#cellphone# </td><td>--&gt;</td><td><fmt:message key="customer's" /> <fmt:message key="cellPhone" /></td></tr>
		  	     </table>
		  		 </td>
		  		 <td valign="top" width="33%">
		  	     <table>		  		 
		  		 <c:if test="${gSiteConfig['gCUSTOMER_FIELDS'] > 0}">
            	   <c:forEach begin="1" end="${gSiteConfig['gCUSTOMER_FIELDS']}" var="current">
            	     <tr><td>#customerfield${current}# </td><td>--&gt;</td><td><fmt:message key="customer" /> <fmt:message key="Field" /> <c:out value="${current}"/></td></tr>
			       </c:forEach>		  		 
		  		 </c:if>
		  	     </table>
		  		 </td>
		  		 <td valign="top" width="33%">
		  	     <table>		  		 
		  		   <tr><td>#id# </td><td>--&gt;</td><td><fmt:message key="product" /> <fmt:message key="id" /></td></tr>
		  		   <tr><td>#cartItems# </td><td>--&gt;</td><td>Number items in Cart</td></tr>
		  		   <tr><td>#cartSubTotal# </td><td>--&gt;</td><td>Cart SubTotal</td></tr>
		  		   <tr><td>#salesRepName# </td><td>--&gt;</td><td>RalesRep Name(HTML Code Login)</td></tr>
		  		   <tr><td>#loginSuccessPage# </td><td>--&gt;</td><td>Login Success Page(HTML Code Login)</td></tr>
			     </table>
		  		 </td></tr>
		  		 </table> 
	 		<!-- end input field -->	
	 		</div>
		  	</div>  