<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="/WEB-INF/tlds/spring.tld" prefix="spring" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib uri="http://www.springframework.org/security/tags" prefix="sec" %>

<tiles:insertDefinition name="admin.layout" flush="true">
  <tiles:putAttribute name="menu" value="/WEB-INF/jsp/admin/layout/menu.jsp?tab=site" />
  <tiles:putAttribute name="content" type="string">
  
<sec:authorize ifAnyGranted="ROLE_AEM,ROLE_ADMIN_${prop_site_id},ROLE_LAYOUT_EDIT,ROLE_LAYOUT_HEAD_TAG">
<script language="JavaScript"> 
<!--
window.addEvent('domready', function(){			
	var tabs1 = new SimpleTabs($('tab-block-1'), { selector: 'h4'});
});
//--> 
</script> 
<!-- main box -->
  <div id="mboxfull">
  
  <!-- start table -->
  <table cellpadding="0" cellspacing="0" border="0" class="module" >
  <tr>
    <td class="topl_g">
	  
	  <!-- breadcrumb -->
	  <p class="breadcrumb">
	    <a href="../layout">Layout</a> &gt;
	    <c:if test="${param.css == 'true'}">css</c:if><c:if test="${param.css != 'true'}">site</c:if> 
	  </p>
	  
	  <!-- Error Message -->
	  
	  <!-- header image -->
	  <img class="headerImage" src="../graphics/layout.gif" />
	  
	  </td><td class="topr_g" ></td></tr>
  <tr><td class="boxmidlrg" >

 <!-- tabs -->
 <div id="tab-block-1">
	<h4 title="<c:if test="${param.css == 'true'}">css</c:if><c:if test="${param.css != 'true'}">site</c:if> "><c:if test="${param.css == 'true'}">css</c:if><c:if test="${param.css != 'true'}">site</c:if> </h4>
	<div>
	<!-- start tab -->
	<c:set var="classIndex" value="0" />

	  	<div class="listdivi ln tabdivi"></div>
	  	<div class="listdivi"></div>
	    
		  	<div class="list${classIndex % 2}"><c:set var="classIndex" value="${classIndex+1}" />
		  	<div class="listp">
		  	<!-- input field -->
		  		<c:if test="${param.css != 'true'}">
				<div align="center">
				<form:form  commandName="layout" action="siteLayout.jhtm" method="get">
				  <select name="layout" onchange="submit()" style="width:250px !important">
					<c:forEach items="${layoutList}" var="option">
				  	<option value="${option.id}<c:if test="${option.host != ''}">,${option.host}</c:if>" <c:if test="${option.id == layout.id and option.host == layout.host}">selected</c:if>>
					<c:choose>
					  <c:when test="${option.id == 1}">
				  	    <fmt:message key="default" /><c:if test="${option.host != ''}"> - <c:out value="${option.host}"/></c:if>
					  </c:when>
					  <c:otherwise>
				  	    ${option.id} - <c:out value="${option.name}"/>
					  </c:otherwise>
					</c:choose>
				  	</option>						  
					</c:forEach>
				  </select>
				</form:form>
				</div>
				
				<table width="50%" align="center" border="0">
				  <sec:authorize ifAnyGranted="ROLE_AEM,ROLE_ADMIN_${prop_site_id},ROLE_LAYOUT_HEAD_TAG">
				  <tr>
				    <td height="30" colspan="3"><a href="siteHeadTag.jhtm?id=${layout.id}<c:if test="${layout.host != ''}">&host=${layout.host}</c:if>" class="layoutCell<c:if test="${layout.headTag == ''}">Empty</c:if>"><fmt:message key="headTag" /><c:if test="${updated == 'HeadTag'}"><br><span class="layoutUpdated"><fmt:message key="layout.updated" /></span></c:if></a>
				    </td>
				  </tr>
				  <tr>
				    <td height="10" colspan="3">&nbsp;</td>
				  </tr>
				  </sec:authorize>				
				  <tr>
				    <td height="50" colspan="3"><a href="siteHeader.jhtm?id=${layout.id}<c:if test="${layout.host != ''}">&host=${layout.host}</c:if>" class="layoutCell<c:if test="${layout.headerHtml == ''}">Empty</c:if>"><fmt:message key="Header" /><c:if test="${updated == 'Header'}"><br><span class="layoutUpdated"><fmt:message key="layout.updated" /></span></c:if></a>
				    </td>
				  </tr>
				  <tr>
				    <td height="30" colspan="3"><a href="siteTopBar.jhtm?id=${layout.id}<c:if test="${layout.host != ''}">&host=${layout.host}</c:if>" class="layoutCell<c:if test="${layout.topBarHtml == ''}">Empty</c:if>"><fmt:message key="TopBar" /><c:if test="${updated == 'TopBar'}"><br><span class="layoutUpdated"><fmt:message key="layout.updated" /></span></c:if></a></td>
				  </tr>
				  <tr>
				    <td>
				      <table cellpadding="0" cellspacing="0">
				        <tr>
				          <td height="64"><a href="siteLeftBarTop.jhtm?id=${layout.id}<c:if test="${layout.host != ''}">&host=${layout.host}</c:if>" class="layoutCell<c:if test="${layout.leftBarTopHtml == ''}">Empty</c:if>"><fmt:message key="LeftBarTop" /><c:if test="${updated == 'LeftBarTop'}"><br><span class="layoutUpdated"><fmt:message key="layout.updated" /></span></c:if></a></td>
				        </tr>
				        <tr>
				          <td height="4"><img src="../graphics/transparent.gif"></td>
				        </tr>
				        <tr>
				          <td height="64" class="layoutCellOff">Category<br>Links</td>
				        </tr>
				        <tr>
				          <td height="4"><img src="../graphics/transparent.gif"></td>
				        </tr>
				        <tr>
				          <td height="64"><a href="siteLeftBarBottom.jhtm?id=${layout.id}<c:if test="${layout.host != ''}">&host=${layout.host}</c:if>" class="layoutCell<c:if test="${layout.leftBarBottomHtml == ''}">Empty</c:if>"><fmt:message key="LeftBarBottom" /><c:if test="${updated == 'LeftBarBottom'}"><br><span class="layoutUpdated"><fmt:message key="layout.updated" /></span></c:if></a></td>
				        </tr>
				      </table>
				    </td>
				    <td width="100%">&nbsp;</td>
				    <td align="right">
				      <table cellpadding="0" cellspacing="0">
				        <tr>
				          <td height="98"><a href="siteRightBarTop.jhtm?id=${layout.id}<c:if test="${layout.host != ''}">&host=${layout.host}</c:if>" class="layoutCell<c:if test="${layout.rightBarTopHtml == ''}">Empty</c:if>"><fmt:message key="RightBarTop" /><c:if test="${updated == 'RightBarTop'}"><br><span class="layoutUpdated"><fmt:message key="layout.updated" /></span></c:if></a></td>
				        </tr>
				        <tr>
				          <td height="4"><img src="../graphics/transparent.gif"></td>
				        </tr>
				        <tr>
				          <td height="98"><a href="siteRightBarBottom.jhtm?id=${layout.id}<c:if test="${layout.host != ''}">&host=${layout.host}</c:if>" class="layoutCell<c:if test="${layout.rightBarBottomHtml == ''}">Empty</c:if>"><fmt:message key="RightBarBottom" /><c:if test="${updated == 'RightBarBottom'}"><br><span class="layoutUpdated"><fmt:message key="layout.updated" /></span></c:if></a></td>
				        </tr>
				      </table>
				    </td>    
				  </tr>
				  <tr>
				    <td height="50" colspan="3"><a href="siteFooter.jhtm?id=${layout.id}<c:if test="${layout.host != ''}">&host=${layout.host}</c:if>" class="layoutCell<c:if test="${layout.footerHtml == ''}">Empty</c:if>"><fmt:message key="Footer" /><c:if test="${updated == 'Footer'}"><br><span class="layoutUpdated"><fmt:message key="layout.updated" /></span></c:if></a></td>
				  </tr>
				  <sec:authorize ifAnyGranted="ROLE_AEM">
				  <c:if test="${layout.id == 1}">
				  <tr>
				    <td height="35" colspan="3"><a style="background-color:#FFDA9E !important;" href="siteAemFooter.jhtm?id=${layout.id}<c:if test="${layout.host != ''}">&host=${layout.host}</c:if>" class="layoutCell<c:if test="${layout.aemFooter == ''}">Empty</c:if>"><fmt:message key="AemFooter" /><c:if test="${updated == 'AemFooter'}"><br><span class="layoutUpdated"><fmt:message key="layout.updated" /></span></c:if></a></td>
				  </tr>
				  </c:if>
				  </sec:authorize>
				  <tr>
				    <td colspan="3">   
				      <form:form  commandName="layout" action="siteLayout.jhtm" method="post">
				      <form:hidden path="id" />
				      <form:hidden path="lang" />
				      <form:hidden path="host" />
				      <table class="form">
				        <c:if test="${layout.id != 1}">
				        <tr>
				          <td align="right">Layout Name : </td>
				          <td>            
				            <spring:bind path="layout.name">
				    		<input type="text" name="<c:out value="${status.expression}"/>" value="<c:out value="${status.value}"/>" maxlength="20">
				    		</spring:bind>
				    	  </td>
				        </tr>
				        </c:if>
				        <tr>
				          <td align="right">Left Pane Width : </td>
				          <td>            
				            <spring:bind path="layout.leftPaneWidth">
				    		<input type="text" name="<c:out value="${status.expression}"/>" value="<c:out value="${status.value}"/>" size="5" maxlength="4">
				    		</spring:bind>
				    	  </td>
				        </tr>
				      </table>
				      <sec:authorize ifAnyGranted="ROLE_AEM,ROLE_ADMIN_${prop_site_id},ROLE_LAYOUT_EDIT,ROLE_LAYOUT_HEAD_TAG">
				      <div align="left" class="button">
				        <input type="submit" value="<spring:message code="Update"/>" name="__update">
				  	  </div>
				  	  </sec:authorize>
				  	  </form:form>
					</td>
				  </tr>  
				</table>
				</c:if>
				
				<c:if test="${param.css == 'true'}">
				<style type="text/css">
				<!--
				.body {
					background: #EEEEEE;
				}
				.site {
					border: 5px solid #00FF00;
					background: #FFFFFF;
				}
				.main {
					border: 5px solid #666666;
				}
				.leftbar {
					border: 5px solid #FFFF00;
					background: #FFFFFF;
				}
				.content {
					border: 5px solid #FF0000;
					background: #FFFFFF;
				}
				-->
				</style>
				<table  border="0" align="center" cellpadding="20" cellspacing="0" class="body">
				  <tr>
				    <td align="center"><table width="500" height="300" border="0" cellpadding="0" cellspacing="0" class="site">
				      <tr>
				        <td align="center" valign="middle">
							header &amp; topbar	area
							  <table width="100%" height="200" border="0" cellpadding="0" cellspacing="0" class="main">
				            <tr>
				              <td class="leftbar"><div align="center">leftbar area </div></td>
				              <td width="75%"  class="content"><div align="center">content area </div></td>
				            </tr>
				        </table>
				          <div align="center">footer area</div></td>
				      </tr>
				    </table></td>
				  </tr>
				</table>
				<table width="200"  border="0" align="center" cellpadding="5" cellspacing="5">
				  <tr>
				    <td colspan="2">&nbsp;</td>
				  </tr>
				  <tr>
				    <td colspan="2">LEGEND: </td>
				  </tr>
				  <tr>
				    <td bgcolor="#EEEEEE">&nbsp;</td>
				    <td>body class</td>
				  </tr>
				  <tr>
				    <td bgcolor="#00FF00">&nbsp;</td>
				    <td>site class </td>
				  </tr>
				  <tr>
				    <td bgcolor="#666666">&nbsp;</td>
				    <td>main class </td>
				  </tr>
				  <tr>
				    <td bgcolor="#FFFF00">&nbsp;</td>
				    <td>leftbar class </td>
				  </tr>
				  <tr>
				    <td bgcolor="#FF0000">&nbsp;</td>
				    <td>content class </td>
				  </tr>
				</table>
				</c:if>
			  	<!-- end input field -->  	  

	 		<!-- end input field -->	
	 		</div>
		  	</div>	
	    
		  	<div class="listlight">
		  	<!-- input field --> 
		  	
		  	<!-- start button --> 
		  	<div align="left" class="button">

			</div>    
		  	<!-- end button -->	
	        </div>

	<!-- end tab -->        
	</div>
<!-- end tabs -->			
</div>
  
  <!-- end table -->
   </td><td class="boxmidr" ></td></tr>
  <tr><td class="botl"></td><td class="botr"></td></tr>
  </table>
  
<!-- end main box -->  
</div> 

</sec:authorize>
  </tiles:putAttribute>    
</tiles:insertDefinition>

