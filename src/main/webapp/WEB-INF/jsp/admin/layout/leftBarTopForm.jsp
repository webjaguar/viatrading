<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="/WEB-INF/tlds/spring.tld" prefix="spring" %>
<%@ taglib uri="http://www.springframework.org/security/tags" prefix="sec" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>

<tiles:insertDefinition name="admin.layout" flush="true">
  <tiles:putAttribute name="menu" value="/WEB-INF/jsp/admin/layout/menu.jsp?tab=site" />
  <tiles:putAttribute name="content" type="string">
  
<sec:authorize ifAnyGranted="ROLE_AEM,ROLE_ADMIN_${prop_site_id},ROLE_LAYOUT_EDIT">    
<script type="text/javascript" src="<c:url value="/FCKeditor/fckeditor.js"/>"></script>
<script language="JavaScript"> 
<!--
window.addEvent('domready', function(){			
			var tabs1 = new SimpleTabs($('tab-block-1'), { selector: 'h4'});
		});
function loadEditor(el) {
  var oFCKeditor = new FCKeditor(el);
  oFCKeditor.BasePath = "<c:url value="/FCKeditor/"/>";
  oFCKeditor.Config["CustomConfigurationsPath"] = '<c:url value="/admin/javascript/fckconfig.js"/>';
  oFCKeditor.ToolbarSet = 'Html';
  oFCKeditor.Width = 650;
  oFCKeditor.Height = 400;
  oFCKeditor.ReplaceTextarea();
  document.getElementById(el + '_link').style.display="none";
}
//--> 
</script>

<c:if test="${lastModified != null}">
<div align="right">
<table class="form">
  <tr>
    <td align="right"><fmt:message key="lastModified" /> : </td>
    <td><fmt:formatDate type="both" timeStyle="full" value="${lastModified}"/></td>
  </tr>
</table>
</div>
</c:if>


<form:form method="post" commandName="layout">
<!-- main box -->
  <div id="mboxfull">
  
  <!-- start table -->
  <table cellpadding="0" cellspacing="0" border="0" class="module" >
  <tr>
    <td class="topl_g">
	  
	  <!-- breadcrumb -->
	  <p class="breadcrumb">
	    <a href="../layout"><fmt:message key="layout"/></a> &gt;
	    <fmt:message key="LeftBarTop"/> 
	  </p>
	  
	  <!-- Error Message -->
	  <c:if test="${message != null}">
		<div class="message"><spring:message code="${message}"/></div>
	  </c:if>
	  
  </td><td class="topr_g" ></td></tr>
  <tr><td class="boxmidlrg" >

 <!-- tabs -->
 <div id="tab-block-1">
	<h4 title="<fmt:message key='LeftBarTop'/> Form"><fmt:message key="LeftBarTop"/></h4>
	<div>
	<!-- start tab -->
         <c:set var="classIndex" value="0" />
         
	  	<div class="listdivi ln tabdivi"></div>
	  	<div class="listdivi"></div>

			<c:if test="${layout.host != ''}">
		  	<div class="list${classIndex % 2}"><c:set var="classIndex" value="${classIndex+1}" />
		  	<div class="listfl"><fmt:message key="multiStore" />:</div>
		  	<div class="listp">
		  	<!-- input field -->
	  			<c:out value="${layout.host}"/>
	 		<!-- end input field -->	
	 		</div>
		  	</div>
		  	</c:if>

		  	<div class="list${classIndex % 2}"><c:set var="classIndex" value="${classIndex+1}" />
		  	<div class="listfl"><fmt:message key="htmlCode" />:</div>
		  	<div class="listp">
		  	<!-- input field -->
	  			<form:textarea path="leftBarTopHtml" rows="15" cols="80" cssClass="textArea700x400" htmlEscape="true"/>
	  			<div id="leftBarTopHtml_link"><a href="#" onClick="loadEditor('leftBarTopHtml')" class="htmleditor">- <fmt:message key="htmlEditor" /> -</a></div>
	 		<!-- end input field -->	
	 		</div>
		  	</div>	
		  	
		  	<div class="list${classIndex % 2}"><c:set var="classIndex" value="${classIndex+1}" />
		  	<div class="listfl"><fmt:message key="htmlCode" /> <fmt:message key="login" />:</div>
		  	<div class="listp">
		  	<!-- input field -->
				<form:textarea path="leftBarTopHtmlLogin" rows="15" cols="80" cssClass="textArea700x400" htmlEscape="true"/>
				<div id="leftBarTopHtmlLogin_link"><a href="#" onClick="loadEditor('leftBarTopHtmlLogin')" class="htmleditor">- <fmt:message key="htmlEditor" /> -</a></div>
	 		<!-- end input field -->	
	 		</div>
		  	</div>
	  		<%-- For Mobile layout --%>
        	<c:if test="${!gSiteConfig['gMOBILE_LAYOUT']}">
        	<form:hidden path="leftBarTopHtmlMobile"/>
        	<form:hidden path="leftBarTopHtmlLoginMobile"/>
        	</c:if>
        	<c:if test="${gSiteConfig['gMOBILE_LAYOUT']}">
        		<div class="list${classIndex % 2}"><c:set var="classIndex" value="${classIndex+1}" />
		  		<div class="listfl"><fmt:message key="htmlCode" /> <fmt:message key="mobile"/>:</div>
		  		<div class="listp">
		  		<!-- input field -->
	  				<form:textarea path="leftBarTopHtmlMobile" rows="15" cols="80" cssClass="textArea700x400" htmlEscape="true"/>
	  				<div id="leftBarTopHtmlMobile_link"><a href="#" onClick="loadEditor('leftBarTopHtmlMobile')" class="htmleditor">- <fmt:message key="htmlEditor" /> -</a></div>
	 			<!-- end input field -->	
	 			</div>
		  		</div>	
		  	
		  		<div class="list${classIndex % 2}"><c:set var="classIndex" value="${classIndex+1}" />
		  		<div class="listfl"><fmt:message key="htmlCode" /> <fmt:message key="login" /> <fmt:message key="mobile"/>:</div>
		  		<div class="listp">
		  		<!-- input field -->
					<form:textarea path="leftBarTopHtmlLoginMobile" rows="15" cols="80" cssClass="textArea700x400" htmlEscape="true"/>
					<div id="leftBarTopHtmlLoginMobile_link"><a href="#" onClick="loadEditor('leftBarTopHtmlLoginMobile')" class="htmleditor">- <fmt:message key="htmlEditor" /> -</a></div>
	 			<!-- end input field -->	
	 			</div>
		  		</div>
        		</c:if> 
	  		<%@ include file="/WEB-INF/jsp/admin/layout/dynamicCustomerElements.jsp" %>  	
	  		  	
	<!-- end tab -->        
	</div>   
	
	<c:forEach items="${languageCodes }" var="i18n">
	<h4 title="<fmt:message key="language_${i18n.languageCode}"/>"><img src="../graphics/${i18n.languageCode}.gif" border="0" title="<fmt:message key="language_${i18n.languageCode}"/>"> <fmt:message key="language_${i18n.languageCode}"/></h4>
	<div>
	<!-- start tab -->

	  	<div class="listdivi ln tabdivi"></div>
	  	<div class="listdivi"></div>
		  	
		  	<div class="list${classIndex % 2}"><c:set var="classIndex" value="${classIndex+1}" />
		  	<div class="listfl"><fmt:message key="htmlCode" />:</div>
		  	<div class="listp">
		  	<!-- input field -->
      	  		<textarea name="__i18nLeftBarTopHtml_${i18n.languageCode}" rows="15" cols="80" class="textfield"><c:out value="${i18nMap[i18n.languageCode].leftBarTopHtml}"/></textarea> 
	  	  		<div id="__i18nLeftBarTopHtml_${i18n.languageCode}_link"><a href="#" onClick="loadEditor('__i18nLeftBarTopHtml_${i18n.languageCode}')" class="htmleditor">- <fmt:message key="htmlEditor" /> -</a></div>
		  	<!-- end input field --> 
		  	</div>
		  	</div>

		  	<div class="list${classIndex % 2}"><c:set var="classIndex" value="${classIndex+1}" />
		  	<div class="listfl"><fmt:message key="htmlCode" /> <fmt:message key="login" />:</div>
		  	<div class="listp">
		  	<!-- input field -->
      	  		<textarea name="__i18nLeftBarTopHtmlLogin_${i18n.languageCode}" rows="15" cols="80" class="textfield"><c:out value="${i18nMap[i18n.languageCode].leftBarTopHtmlLogin}"/></textarea> 
	  	  		<div id="__i18nLeftBarTopHtmlLogin_${i18n.languageCode}_link"><a href="#" onClick="loadEditor('__i18nLeftBarTopHtmlLogin_${i18n.languageCode}')" class="htmleditor">- <fmt:message key="htmlEditor" /> -</a></div>
		  	<!-- end input field --> 
		  	</div>
		  	</div>
		  	<c:if test="${gSiteConfig['gMOBILE_LAYOUT']}">
        		<div class="list${classIndex % 2}"><c:set var="classIndex" value="${classIndex+1}" />
		  		<div class="listfl"><fmt:message key="htmlCode" /> <fmt:message key="mobile"/>:</div>
		  		<div class="listp">
		  		<!-- input field -->
      	  			<textarea name="__i18nLeftBarTopHtmlMobile_${i18n.languageCode}" rows="15" cols="80" class="textfield"><c:out value="${i18nMap[i18n.languageCode].leftBarTopHtmlMobile}"/></textarea> 
	  	  			<div id="__i18nLeftBarTopHtmlMobile_${i18n.languageCode}_link"><a href="#" onClick="loadEditor('__i18nLeftBarTopHtmlMobile_${i18n.languageCode}')" class="htmleditor">- <fmt:message key="htmlEditor" /> -</a></div>
		  		<!-- end input field --> 	
	 			</div>
		  		</div>	
		  	
		  		<div class="list${classIndex % 2}"><c:set var="classIndex" value="${classIndex+1}" />
		  		<div class="listfl"><fmt:message key="htmlCode" /> <fmt:message key="login" /> <fmt:message key="mobile"/>:</div>
		  		<div class="listp">
		  		<!-- input field -->
      	  			<textarea name="__i18nLeftBarTopHtmlLoginMobile_${i18n.languageCode}" rows="15" cols="80" class="textfield"><c:out value="${i18nMap[i18n.languageCode].leftBarTopHtmlLoginMobile}"/></textarea> 
	  	  			<div id="__i18nLeftBarTopHtmlLoginMobile_${i18n.languageCode}_link"><a href="#" onClick="loadEditor('__i18nLeftBarTopHtmlLoginMobile_${i18n.languageCode}')" class="htmleditor">- <fmt:message key="htmlEditor" /> -</a></div>
		  		<!-- end input field --> 	
	 			</div>
		  		</div>
        	</c:if>
		  	<%@ include file="/WEB-INF/jsp/admin/layout/dynamicCustomerElements.jsp" %>
		  		    
	<!-- end tab -->		
	</div>	
	</c:forEach>
		
<!-- end tabs -->			
</div>

<!-- start button -->
	<div align="left" class="button">
        <input type="submit" value="<spring:message code="Update"/>" name="_update">
     	<input type="submit" value="<spring:message code="cancel"/>" name="_cancel">
	</div>
<!-- end button -->	
    
  <!-- end table -->
    </td><td class="boxmidr" ></td></tr>
  <tr><td class="botl"></td><td class="botr"></td></tr>
  </table>
  
<!-- end main box -->  
</div>
</form:form>
</sec:authorize>

  </tiles:putAttribute>    
</tiles:insertDefinition>

