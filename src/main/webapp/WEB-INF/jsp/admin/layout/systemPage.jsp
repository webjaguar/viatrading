<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ taglib uri="http://www.springframework.org/security/tags" prefix="sec" %>
<%@ taglib uri="/WEB-INF/tlds/spring.tld" prefix="spring" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>

<tiles:insertDefinition name="admin.layout" flush="true">
  <tiles:putAttribute name="menu" value="/WEB-INF/jsp/admin/layout/menu.jsp" />
  <tiles:putAttribute name="content" type="string">

<sec:authorize ifAnyGranted="ROLE_AEM,ROLE_ADMIN_${prop_site_id},ROLE_LAYOUT_EDIT">   
<script type="text/javascript" src="<c:url value="/FCKeditor/fckeditor.js"/>"></script>
<script language="JavaScript"> 
<!--
function loadEditor(el) {
  var oFCKeditor = new FCKeditor(el);
  oFCKeditor.BasePath = "<c:url value="/FCKeditor/"/>";
  oFCKeditor.Config["CustomConfigurationsPath"] = '<c:url value="/admin/javascript/fckconfig.js"/>';
  oFCKeditor.ToolbarSet = 'Html';
  oFCKeditor.Width = 650;
  oFCKeditor.Height = 400;
  oFCKeditor.ReplaceTextarea();
  document.getElementById(el + '_link').style.display="none";
}
//--> 
</script>
<script language="JavaScript"> 
<!--
window.addEvent('domready', function(){			
	var tabs1 = new SimpleTabs($('tab-block-1'), { selector: 'h4'});
	var Tips1 = new Tips($$('.toolTipImg'));
});
//--> 
</script> 
<form:form method="post" commandName="layout">
<!-- main box -->
  <div id="mboxfull">
  
  <!-- start table -->
  <table cellpadding="0" cellspacing="0" border="0" class="module" >
  <tr>
    <td class="topl_g">
	  
	  <!-- breadcrumb -->
	  <p class="breadcrumb">
	    <a href="../layout">Layout</a> &gt;
        <c:choose>
          <c:when test="${fn:contains(param.type,'packing')}">
        	<fmt:message key="packingList"/> <c:out value="${fn:substringAfter(param.type,'packing')}"/>
          </c:when>
          <c:otherwise>
      		<fmt:message key="${param.type}"/>
          </c:otherwise>
        </c:choose>
	  </p>
	  
	  <!-- Error Message -->
	  <c:if test="${message != null}">
	    <div class="message"><spring:message code="${message}"/></div>
	  </c:if>
	  
	  <c:if test="${lastModified != null}">
		<div align="right">
		<table class="form">
		  <tr>
		    <td align="right"><fmt:message key="lastModified" /> : </td>
		    <td><fmt:formatDate type="both" timeStyle="full" value="${lastModified}"/></td>
		  </tr>
		</table>
		</div>
	  </c:if>

  </td><td class="topr_g" ></td></tr>
  <tr><td class="boxmidlrg" >

 <!-- tabs -->
 <div id="tab-block-1">
	<h4 title="layout">
        <c:choose>
          <c:when test="${fn:contains(param.type,'packing')}">
        	<fmt:message key="packingList"/> <c:out value="${fn:substringAfter(param.type,'packing')}"/>
          </c:when>
          <c:otherwise>
      		<fmt:message key="${param.type}"/>
          </c:otherwise>
        </c:choose>
	</h4>
	<div>
	<!-- start tab -->
         <c:set var="classIndex" value="0" />
         
	  	<div class="listdivi ln tabdivi"></div>
	  	<div class="listdivi"></div>
				
			<c:if test="${layoutList != null}">
		  	<div class="list${classIndex % 2}"><c:set var="classIndex" value="${classIndex+1}" />
		  	<div class="listfl">
		  		  <c:if test="${not(param.type == 'workOrder' or param.type == 'workorder')}"><fmt:message key="multiStore" />:</c:if>
		  		  <c:if test="${param.type == 'workOrder' or param.type == 'workorder'}"><fmt:message key="subContractor" />:</c:if>
		  	</div>
		  	<div class="listp">
		  	<!-- input field -->
			  <select name="layout" onchange="document.location='systemPage.jhtm?type=${layout.type}&host=' + this.value">
				<c:forEach items="${layoutList}" var="option">
			  	<option value="${option.host}" <c:if test="${option.host == param.host}">selected</c:if>>
				  <c:if test="${option.host == ''}"><fmt:message key="default" /></c:if><c:out value="${option.host}"/> <c:if test="${option.name != ''}"> - <c:out value="${option.name}"/></c:if>
			  	</option>						  
				</c:forEach>
			  </select>
	 		<!-- end input field -->	
	 		</div>
		  	</div>
			<c:if test="${(param.type == 'workOrder' or param.type == 'workorder') and (param.host != null && param.host != '')}">
		  	<div class="list${classIndex % 2}"><c:set var="classIndex" value="${classIndex+1}" />
		  	<div class="listfl">Name:</div>
		  	<div class="listp">
		  	<!-- input field -->
			    <form:input path="name" cssClass="textfield" size="50" maxlength="50" />
	 		<!-- end input field -->	
	 		</div>
		  	</div>
		  	</c:if>
		  	</c:if>
		  		 
		  	<c:if test="${param.type != 'forgetPassword' }">  
		  	<div class="list${classIndex % 2}"><c:set var="classIndex" value="${classIndex+1}" />
		  	<div class="listfl"><fmt:message key="Header" /> <fmt:message key="htmlCode" />:</div>
		  	<div class="listp">
		  	<!-- input field -->
				<form:textarea path="headerHtml" rows="15" cols="80" cssClass="textArea700x400" htmlEscape="true"/>
				<div id="headerHtml_link"><a href="#" onClick="loadEditor('headerHtml')" class="htmleditor">- <fmt:message key="htmlEditor" /> -</a></div>
	 		<!-- end input field -->	
	 		</div>
		  	</div>	
			</c:if>
			
			<c:if test="${param.type != 'forgetPassword' }"> 
		  	<div class="list${classIndex % 2}"><c:set var="classIndex" value="${classIndex+1}" />
		  	<div class="listfl"><fmt:message key="Footer" /> <fmt:message key="htmlCode" />:</div>
		  	<div class="listp">
		  	<!-- input field -->
				<form:textarea path="footerHtml" rows="15" cols="80" cssClass="textArea700x400" htmlEscape="true"/>
	  			<div id="footerHtml_link"><a href="#" onClick="loadEditor('footerHtml')" class="htmleditor">- <fmt:message key="htmlEditor" /> -</a></div>
	 		<!-- end input field -->	
	 		</div>
		  	</div>
		  	</c:if>
		  	
		  	<c:choose>
		  	  <c:when test="${(fn:contains(param.type,'product') or  param.type == 'cart')}">
		  	    <div class="list${classIndex % 2}"><c:set var="classIndex" value="${classIndex+1}" />
			  	<div class="listfl"><fmt:message key="LeftBarTop" /> <fmt:message key="htmlCode" />:</div>
			  	<div class="listp">
			  	<!-- input field -->
					<form:textarea path="leftBarTopHtml" rows="15" cols="80" cssClass="textArea700x400" htmlEscape="true"/>
		  			<div id="footerHtml_link"><a href="#" onClick="loadEditor('leftBarTopHtml')" class="htmleditor">- <fmt:message key="htmlEditor" /> -</a></div>
		 		<!-- end input field -->	
		 		</div>
			  	</div>
			  	
			  	<div class="list${classIndex % 2}"><c:set var="classIndex" value="${classIndex+1}" />
			  	<div class="listfl"><fmt:message key="RightBarTop" /> <fmt:message key="htmlCode" />:</div>
			  	<div class="listp">
			  	<!-- input field -->
					<form:textarea path="rightBarTopHtml" rows="15" cols="80" cssClass="textArea700x400" htmlEscape="true"/>
		  			<div id="footerHtml_link"><a href="#" onClick="loadEditor('rightBarTopHtml')" class="htmleditor">- <fmt:message key="htmlEditor" /> -</a></div>
		 		<!-- end input field -->	
		 		</div>
			  	</div>
		  	  </c:when>
		  	  <c:otherwise>
		  	  		<form:hidden path="leftBarTopHtml"/>
					<form:hidden path="rightBarTopHtml"/>
		  	  </c:otherwise>
		  	</c:choose>
		  	  	

            <div class="list${classIndex % 2}"><c:set var="classIndex" value="${classIndex+1}" />
		  	<div class="listfl"><div class="helpImg"><img class="toolTipImg" title="Note::This Head Tag will be replaced by Global Head Tag. So You need to have your Title tag and stylesheet and every global values needded on the front end." src="../graphics/question.gif" /></div><fmt:message key="headTag" />:</div>
		  	
		  	<div class="listp">
		  	<!-- input field -->
				<form:textarea path="headTag" rows="15" cols="80" cssClass="textArea700x400" htmlEscape="true"/>
	 		<!-- end input field -->	
	 		</div>
		  	</div>
		  	
			<c:if test="${param.type == 'finvoice'}">
		  	<div class="list${classIndex % 2}"><c:set var="classIndex" value="${classIndex+1}" />
		  	<div class="listfl"><div class="helpImg"><img class="toolTipImg" title="Note::#order# on the HTML Code will be dynamically replaced by the order number" src="../graphics/question.gif" /></div>Dynamic Element:</div>
		  	<div class="listp">
		  	<!-- input field -->
		  	     <span class="static">
		  		 #order# --&gt; <fmt:message key="orderNumber" /><br/>
		  		 #ordergrandtotal# --&gt; <fmt:message key="grandTotal" /><br/>
		  		 #ordersubtotal# --&gt; <fmt:message key="subTotal" /><br/>
		  		 #ordertotalquantity# --&gt; <fmt:message key="orderTotalQuantity" /><br/>
		  		 #orderdiscount# --&gt; <fmt:message key="discount" /><br/>
		  		 </span>
	 		<!-- end input field -->	
	 		</div>
		  	</div>
		  	</c:if>
		  	
			<c:if test="${param.type == 'pinvoice'}">
		  	<div class="list${classIndex % 2}"><c:set var="classIndex" value="${classIndex+1}" />
		  	<div class="listfl"><div class="helpImg"><img class="toolTipImg" title="Note::#email# on the HTML Code will be dynamically replaced by the email" src="../graphics/question.gif" /></div>Dynamic Element:</div>
		  	<div class="listp">
		  	<!-- input field -->
		  	     <span class="static">
	 				#email# --&gt; <fmt:message key="email" /> (Not on Head Tag)<br/>
		  		 </span>
	 		<!-- end input field -->	
	 		</div>
		  	</div>
		  	</c:if>
		  	
		  	<c:if test="${param.type == 'myOrderHistory'}">
		  	<div class="list${classIndex % 2}"><c:set var="classIndex" value="${classIndex+1}" />
		  	<div class="listfl"><div class="helpImg"><img class="toolTipImg" title="Note::#firstname# on the HTML Code will be dynamically replaced by the order number" src="../graphics/question.gif" /></div>Dynamic Element:</div>
		  	<div class="listp">
		  	<!-- input field -->
		  	     <span class="static">
		  	        #firstname# --&gt; <fmt:message key="firstname"/><br/>
		  	        #lastname# --&gt; <fmt:message key="lastname"/><br/>
		  	        #email# --&gt; <fmt:message key="email"/><br/>
					#lastOrderId# --&gt; <fmt:message key="lastOrderId"/><br/>
					#lastOrderStatus# --&gt; <fmt:message key="lastOrderStatus"/><br/>
					#lastOrderSubStatus# --&gt; <fmt:message key="lastOrderSubStatus"/><br/>
		  		 </span>
	 		<!-- end input field -->	
	 		</div>
		  	</div>
		  	</c:if>
		  	
			<c:if test="${param.type == 'myList'}">
		  	<div class="list${classIndex % 2}"><c:set var="classIndex" value="${classIndex+1}" />
		  	<div class="listfl"><div class="helpImg"><img class="toolTipImg" title="Note::#email# on the HTML Code will be dynamically replaced by the email" src="../graphics/question.gif" /></div>Dynamic Element:</div>
		  	<div class="listp">
		  	<!-- input field -->
		  	     <span class="static">
	 				#email# --&gt; <fmt:message key="email" /> (Not on Head Tag)<br/>
		  		 </span>
	 		<!-- end input field -->	
	 		</div>
		  	</div>
		  	</c:if>
		  	
			<c:if test="${param.type == 'shipping'}">
		  	<div class="list${classIndex % 2}"><c:set var="classIndex" value="${classIndex+1}" />
		  	<div class="listfl"><div class="helpImg"><img class="toolTipImg" title="Note::#email# on the HTML Code will be dynamically replaced by the email" src="../graphics/question.gif" /></div>Dynamic Element:</div>
		  	<div class="listp">
		  	<!-- input field -->
		  	     <span class="static">
	 				#email# --&gt; <fmt:message key="email" /> (Not on Head Tag)<br/>
		  		 </span>
	 		<!-- end input field -->	
	 		</div>
		  	</div>
		  	</c:if>
		  	
			<c:if test="${param.type == 'workOrder' or param.type == 'workorder'}">
		  	<div class="list${classIndex % 2}"><c:set var="classIndex" value="${classIndex+1}" />
		  	<div class="listfl"><div class="helpImg"><img class="toolTipImg" title="Note::#type# on the HTML Code will be dynamically replaced by the work order type" src="../graphics/question.gif" /></div>Dynamic Element:</div>
		  	<div class="listp">
		  	<!-- input field -->
		  		 <span class="static">
		  		 #type# --&gt; <fmt:message key="workOrder" /> <fmt:message key="type" /><br/>
		  		 </span>
	 		<!-- end input field -->	
	 		</div>
		  	</div>
		  	</c:if>

			<c:if test="${fn:contains(param.type,'product')}">
		  	<div class="list${classIndex % 2}"><c:set var="classIndex" value="${classIndex+1}" />
		  	<div class="listfl"><div class="helpImg"><img class="toolTipImg" title="Note::#sku# on the HTML Code or Head Tag will be dynamically replaced by the sku of the product" src="../graphics/question.gif" /></div>Dynamic Element:</div>
		  	<div class="listp">
		  	<!-- input field -->
		  	     <span class="static">
		  		 #sku# --&gt; <fmt:message key="productSku" /><br/>
		  		 #id# --&gt; <fmt:message key="productId" /><br/>
		  		 #name# --&gt; <fmt:message key="Name" /><br/>
		  		 #namewithoutquotes# --&gt; <fmt:message key="Name" /><br/>
		  		 #msrp# --&gt; MSRP<br/>
		  		 #price1# --&gt; Price1<br/>
		  		 #weight# --&gt; Weight<br/>
		  		 #shortdesc# --&gt; <fmt:message key="productShortDesc" /> (Only on Head Tag)<br/>
		  		 #keyword# --&gt; <fmt:message key="keywords" /> (Only on Head Tag)<br/>
		  		 #breadcrumb# --&gt; <fmt:message key="breadCrumbs" /> (Only on Head Tag)<br/>
		  		 #field1# --&gt; <fmt:message key="Field" />1 (Only on Head Tag)<br/>
		  		 #email# --&gt; <fmt:message key="email" /> (Not on Head Tag)<br/>
		  		 #firstname# --&gt; <fmt:message key="firstName" /> (Not on Head Tag)<br/>
		  		 #lastname# --&gt; <fmt:message key="lastName" /> (Not on Head Tag)<br/>
		  		 </span>
	 		<!-- end input field -->	
	 		</div>
		  	</div>
		  	</c:if>
		  	
		  	<c:if test="${param.type == 'category'}">
		  	<div class="list${classIndex % 2}"><c:set var="classIndex" value="${classIndex+1}" />
		  	<div class="listfl"><div class="helpImg"><img class="toolTipImg" title="Note::#name# on the Head Tag will be dynamically replaced by the category name." src="../graphics/question.gif" /></div>Dynamic Element:</div>
		  	<div class="listp">
		  	<!-- input field -->
		  	     <span class="static">
		  		 #name# --&gt; <fmt:message key="Name" /> (Only on Head Tag and Header HTML Code)<br/>
		  		 #breadcrumb# --&gt; <fmt:message key="breadCrumbs" /> (Only on Head Tag)<br/>
		  		 #field1# --&gt; <fmt:message key="categorField1" /> (Only on Head Tag)<br/>
		  		 #field2# --&gt; <fmt:message key="categorField2" /> (Only on Head Tag)<br/>
		  		 #field3# --&gt; <fmt:message key="categorField3" /> (Only on Head Tag)<br/>
		  		 #field4# --&gt; <fmt:message key="categorField4" /> (Only on Head Tag)<br/>
		  		 #field5# --&gt; <fmt:message key="categorField5" /> (Only on Head Tag)<br/>
		  		 #email# --&gt; <fmt:message key="email" /> (Not on Head Tag)<br/>
		  		 </span>
	 		<!-- end input field -->	
	 		</div>
		  	</div>
		  	</c:if>

			<c:if test="${param.type == 'cart'}">
		  	<div class="list${classIndex % 2}"><c:set var="classIndex" value="${classIndex+1}" />
		  	<div class="listfl"><div class="helpImg"><img class="toolTipImg" title="Note::#cartsubtotal# on the HTML Code will be dynamically replaced by the cart's subtotal" src="../graphics/question.gif" /></div>Dynamic Element:</div>
		  	<div class="listp">
		  	<!-- input field -->
		  	     <span class="static">
		  		 #cartsubtotal# --&gt; <fmt:message key="subTotal" /><br/>
		  		 </span>
	 		<!-- end input field -->	
	 		</div>
		  	</div>
		  	</c:if>
		  	
			<c:if test="${param.type == 'salesRep'}">
		  	<div class="list${classIndex % 2}"><c:set var="classIndex" value="${classIndex+1}" />
		  	<div class="listfl">Dynamic Element:</div>
		  	<div class="listp">
		  	<!-- input field -->
		  	     <span class="static">
		  		 #salesRepName# --&gt; <fmt:message key="Name" /><br/>
		  		 #salesRepEmail# --&gt; <fmt:message key="email" /><br/>
		  		 #salesRepPhone# --&gt; <fmt:message key="phone" /><br/>
		  		 #salesRepCellPhone# --&gt; <fmt:message key="cellPhone" /><br/>
		  		 #salesRepAddress1# --&gt; <fmt:message key="address" /> 1<br/>
		  		 #salesRepAddress2# --&gt; <fmt:message key="address" /> 2<br/>
		  		 #salesRepCity# --&gt; <fmt:message key="city" /><br/>
		  		 #salesRepCountry# --&gt; <fmt:message key="country" /><br/>
		  		 #salesRepStateProvince# --&gt; <fmt:message key="stateProvince" /><br/>
		  		 #salesRepZip# --&gt; <fmt:message key="zipCode" /><br/>
		  		 #salesRepFax# --&gt; <fmt:message key="fax" /><br/>
		  		 #salesRepCompany# --&gt; <fmt:message key="company" /><br/>
		  		 </span>
	 		<!-- end input field -->	
	 		</div>
		  	</div>
		  	</c:if>
		  	
		  	<c:if test="${param.type == 'productSearch'}">
		  	<div class="list${classIndex % 2}"><c:set var="classIndex" value="${classIndex+1}" />
		  	<div class="listfl"><div class="helpImg"><img class="toolTipImg" title="Note::#name# on the Head Tag will be dynamically replaced by the category name." src="../graphics/question.gif" /></div>Dynamic Element:</div>
		  	<div class="listp">
		  	<!-- input field -->
		  	     <span class="static">
		  		 #name# --&gt; <fmt:message key="Name" /> (Only on Head Tag and Header HTML Code)<br/>
		  		 #breadcrumb# --&gt; <fmt:message key="breadCrumbs" /> (Only on Head Tag) if cid exist on URL breadcrum automatically shows on Header Html<br/>
		  		 #field1# --&gt; <fmt:message key="categorField1" /> (Only on Head Tag)<br/>
		  		 #field2# --&gt; <fmt:message key="categorField2" /> (Only on Head Tag)<br/>
		  		 #field3# --&gt; <fmt:message key="categorField3" /> (Only on Head Tag)<br/>
		  		 #field4# --&gt; <fmt:message key="categorField4" /> (Only on Head Tag)<br/>
		  		 #field5# --&gt; <fmt:message key="categorField5" /> (Only on Head Tag)<br/>
		  		 #email# --&gt; <fmt:message key="email" /> (Not on Head Tag)<br/>
		  		 </span>
	 		<!-- end input field -->	
	 		</div>
		  	</div>
		  	</c:if>
		  	
		  	<c:if test="${param.type == 'subAccount'}">
		  	<div class="list${classIndex % 2}"><c:set var="classIndex" value="${classIndex+1}" />
		  	<div class="listfl"><div class="helpImg"><img class="toolTipImg" title="Note::#credit# on the Head Tag will be dynamically replaced by the credit available." src="../graphics/question.gif" /></div>Dynamic Element:</div>
		  	<div class="listp">
		  	<!-- input field -->
		  	     <span class="static">
		  		 #credit# --&gt; <fmt:message key="credit" /><br/>
		  		 </span>
	 		<!-- end input field -->	
	 		</div>
		  	</div>
		  	</c:if>
	<%--For Mobile Layout --%>
	<c:if test="${! gSiteConfig['gMOBILE_LAYOUT']}">
	    <form:hidden path="headerHtmlMobile"/>
		<form:hidden path="footerHtmlMobile"/>
		<form:hidden path="leftBarTopHtmlMobile"/>
		<form:hidden path="rightBarTopHtmlMobile"/>
		<form:hidden path="headTagMobile"/> 
	</c:if>
	<c:if test="${gSiteConfig['gMOBILE_LAYOUT'] and param.type != 'forgetPassword'}">
		<div class="list${classIndex % 2}"><c:set var="classIndex" value="${classIndex+1}" />
		  	<div class="listfl"><fmt:message key="Header" /> <fmt:message key="htmlCode" /> <fmt:message key="mobile"/>:</div>
		  	<div class="listp">
		  	<!-- input field -->
				<form:textarea path="headerHtmlMobile" rows="15" cols="80" cssClass="textArea700x400" htmlEscape="true"/>
				<div id="headerHtmlMobile_link"><a href="#" onClick="loadEditor('headerHtmlMobile')" class="htmleditor">- <fmt:message key="htmlEditor" /> -</a></div>
	 		<!-- end input field -->	
	 		</div>
		 </div>
		 <div class="list${classIndex % 2}"><c:set var="classIndex" value="${classIndex+1}" />
		  	<div class="listfl"><fmt:message key="Footer" /> <fmt:message key="htmlCode" /> <fmt:message key="mobile"/>:</div>
		  	<div class="listp">
		  	<!-- input field -->
				<form:textarea path="footerHtmlMobile" rows="15" cols="80" cssClass="textArea700x400" htmlEscape="true"/>
	  			<div id="footerHtmlMobile_link"><a href="#" onClick="loadEditor('footerHtmlMobile')" class="htmleditor">- <fmt:message key="htmlEditor" /> -</a></div>
	 		<!-- end input field -->	
	 		</div>
		 </div>
		 <!-- TODO: Category controller code should be chaged to read this layout. -->
		  	<c:choose>
		  	  <c:when test="${!(fn:contains(param.type,'packing')) and !(param.type =='category')}">
		  	    <div class="list${classIndex % 2}"><c:set var="classIndex" value="${classIndex+1}" />
			  	<div class="listfl"><fmt:message key="LeftBarTop" /> <fmt:message key="htmlCode" />  <fmt:message key="mobile"/>:</div>
			  	<div class="listp">
			  	<!-- input field -->
					<form:textarea path="leftBarTopHtmlMobile" rows="15" cols="80" cssClass="textArea700x400" htmlEscape="true"/>
		  			<div id="leftBarTopHtmlMobile_link"><a href="#" onClick="loadEditor('leftBarTopHtmlMobile')" class="htmleditor">- <fmt:message key="htmlEditor" /> -</a></div>
		 		<!-- end input field -->	
		 		</div>
			  	</div>
			  	<c:choose>
			  	<c:when test="${!(param.type == 'systemPage') }">
			  	<div class="list${classIndex % 2}"><c:set var="classIndex" value="${classIndex+1}" />
			  	<div class="listfl"><fmt:message key="RightBarTop" /> <fmt:message key="htmlCode" />  <fmt:message key="mobile"/>:</div>
			  	<div class="listp">
			  	<!-- input field -->
					<form:textarea path="rightBarTopHtmlMobile" rows="15" cols="80" cssClass="textArea700x400" htmlEscape="true"/>
		  			<div id="rightBarTopHtmlMobile"><a href="#" onClick="loadEditor('rightBarTopHtmlMobile')" class="htmleditor">- <fmt:message key="htmlEditor" /> -</a></div>
		 		<!-- end input field -->	
		 		</div>
			  	</div>
			  	</c:when>
			  	<c:otherwise>
			  		<form:hidden path="rightBarTopHtmlMobile"/>
		  	    </c:otherwise>
			  	</c:choose>
		  	  </c:when>
		  	  <c:otherwise>
		  	  		<form:hidden path="leftBarTopHtmlMobile"/>
					<form:hidden path="rightBarTopHtmlMobile"/>
		  	  </c:otherwise>
		  	</c:choose>
		  	<div class="list${classIndex % 2}"><c:set var="classIndex" value="${classIndex+1}" />
		  	<div class="listfl"><div class="helpImg"><img class="toolTipImg" title="Note::This Head Tag will be replaced by Global Head Tag. So You need to have your Title tag and stylesheet and every global values needded on the front end." src="../graphics/question.gif" /></div><fmt:message key="headTag" />  <fmt:message key="mobile"/>:</div>
		  	
		  	<div class="listp">
		  	<!-- input field -->
				<form:textarea path="headTagMobile" rows="15" cols="80" cssClass="textArea700x400" htmlEscape="true"/>
	 		<!-- end input field -->	
	 		</div>
		  	</div>
	</c:if>
	<!-- end tab -->   
	</div>         

	<c:forEach items="${languageCodes}" var="i18n">
	<h4 title="<fmt:message key="language_${i18n.languageCode}"/>"><img src="../graphics/${i18n.languageCode}.gif" border="0" title="<fmt:message key="language_${i18n.languageCode}"/>"> <fmt:message key="language_${i18n.languageCode}"/></h4>
	<div>
	<!-- start tab -->

	  	<div class="listdivi ln tabdivi"></div>
	  	<div class="listdivi"></div>
		  	
		  	<div class="list${classIndex % 2}"><c:set var="classIndex" value="${classIndex+1}" />
		  	<div class="listfl"><fmt:message key="Header" /> <fmt:message key="htmlCode" />:</div>
		  	<div class="listp">
		  	<!-- input field -->
      	  		<textarea name="__i18nHeaderHtml_${i18n.languageCode}" rows="15" cols="80" class="textArea700x400"><c:out value="${i18nMap[i18n.languageCode].headerHtml}"/></textarea> 
	  	  		<div id="__i18nHeaderHtml_${i18n.languageCode}_link"><a href="#" onClick="loadEditor('__i18nHeaderHtml_${i18n.languageCode}')" class="htmleditor">- <fmt:message key="htmlEditor" /> -</a></div>
		  	<!-- end input field --> 
		  	</div>
		  	</div>

		  	<div class="list${classIndex % 2}"><c:set var="classIndex" value="${classIndex+1}" />
		  	<div class="listfl"><fmt:message key="Footer" /> <fmt:message key="htmlCode" />:</div>
		  	<div class="listp">
		  	<!-- input field -->
      	  		<textarea name="__i18nFooterHtml_${i18n.languageCode}" rows="15" cols="80" class="textArea700x400"><c:out value="${i18nMap[i18n.languageCode].footerHtml}"/></textarea> 
	  	  		<div id="__i18nFooterHtml_${i18n.languageCode}_link"><a href="#" onClick="loadEditor('__i18nFooterHtml_${i18n.languageCode}')" class="htmleditor">- <fmt:message key="htmlEditor" /> -</a></div>
		  	<!-- end input field --> 
		  	</div>
		  	</div>
		  	
            <div class="list${classIndex % 2}"><c:set var="classIndex" value="${classIndex+1}" />
		  	<div class="listfl"><div class="helpImg"><img class="toolTipImg" title="Note::This Head Tag will be replaced by Global Head Tag. So You need to have your Title tag and stylesheet and every global values needded on the front end." src="../graphics/question.gif" /></div><fmt:message key="headTag" />:</div>
		  	<div class="listp">
		  	<!-- input field -->
      	  		<textarea name="__i18nHeadTag_${i18n.languageCode}" rows="15" cols="80" class="textArea700x400"><c:out value="${i18nMap[i18n.languageCode].headTag}"/></textarea> 
		  	<!-- end input field --> 
		  	</div>
		  	</div>	  	
		  	
		  	<c:if test="${gSiteConfig['gMOBILE_LAYOUT']}">
				<div class="list${classIndex % 2}"><c:set var="classIndex" value="${classIndex+1}" />
		  			<div class="listfl"><fmt:message key="Header" /> <fmt:message key="htmlCode" /> <fmt:message key="mobile"/>:</div>
		  			<div class="listp">
		  			<!-- input field -->
      	  				<textarea name="__i18nHeaderHtmlMobile_${i18n.languageCode}" rows="15" cols="80" class="textArea700x400"><c:out value="${i18nMap[i18n.languageCode].headerHtmlMobile}"/></textarea> 
	  	  				<div id="__i18nHeaderHtmlMobile_${i18n.languageCode}_link"><a href="#" onClick="loadEditor('__i18nHeaderHtmlMobile_${i18n.languageCode}')" class="htmleditor">- <fmt:message key="htmlEditor" /> -</a></div>
		  			<!-- end input field -->	
	 				</div>
		 		</div>
		 		<div class="list${classIndex % 2}"><c:set var="classIndex" value="${classIndex+1}" />
		  			<div class="listfl"><fmt:message key="Footer" /> <fmt:message key="htmlCode" /> <fmt:message key="mobile"/>:</div>
		  			<div class="listp">
		  			<!-- input field -->
      	  				<textarea name="__i18nFooterHtmlMobile_${i18n.languageCode}" rows="15" cols="80" class="textArea700x400"><c:out value="${i18nMap[i18n.languageCode].footerHtmlMobile}"/></textarea> 
	  	  				<div id="__i18nFooterHtmlMobile_${i18n.languageCode}_link"><a href="#" onClick="loadEditor('__i18nFooterHtmlMobile_${i18n.languageCode}')" class="htmleditor">- <fmt:message key="htmlEditor" /> -</a></div>
		  			<!-- end input field --> 	
	 				</div>
		 		</div>
		  			<div class="list${classIndex % 2}"><c:set var="classIndex" value="${classIndex+1}" />
		  			<div class="listfl"><div class="helpImg"><img class="toolTipImg" title="Note::This Head Tag will be replaced by Global Head Tag. So You need to have your Title tag and stylesheet and every global values needded on the front end." src="../graphics/question.gif" /></div><fmt:message key="headTag" />  <fmt:message key="mobile"/>:</div>
		  	
		  			<div class="listp">
		  			<!-- input field -->
      	  				<textarea name="__i18nHeadTagMobile_${i18n.languageCode}" rows="15" cols="80" class="textArea700x400"><c:out value="${i18nMap[i18n.languageCode].headTagMobile}"/></textarea> 
		  			<!-- end input field --> 	
	 				</div>
		  			</div>
			</c:if>	    
	<!-- end tab -->		
	</div>	
	</c:forEach>

<!-- end tabs -->			
</div>

<!-- start button -->
	<div align="left" class="button">
      <input type="submit" value="<spring:message code="Update"/>" name="_update">
      <input type="submit" value="<spring:message code="cancel"/>" name="_cancel">
	</div>
<!-- end button -->
  
  <!-- end table -->
    </td><td class="boxmidr" ></td></tr>
  <tr><td class="botl"></td><td class="botr"></td></tr>
  </table>
  
<!-- end main box -->  
</div>
</form:form>
</sec:authorize>

  </tiles:putAttribute>    
</tiles:insertDefinition>
