<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>


  <div id="lbox">
   <table cellpadding="0" cellspacing="0" border="0" class="module" style="width: 100%"><tr><td class="topl">
    </td><td class="topr" ></td></tr><tr><td class="boxmid" >
  
    <h2 class="menuleft mfirst"><fmt:message key="layout"/></h2>
    <div class="menudiv"></div> 
      <a href="siteLayout.jhtm" class="userbutton"><fmt:message key="Site" /></a>
	  <%-- <a href="siteLayout.jhtm?css=true" class="userbutton">CSS</a> --%>
      <a href="systemPage.jhtm?type=login" class="userbutton"><fmt:message key="login" /></a>
      <a href="systemPage.jhtm?type=forgetPassword" class="userbutton"><fmt:message key="forgetPassword" /></a>
      <a href="systemPage.jhtm?type=createAccount" class="userbutton"><fmt:message key="createAccount"/></a>
      <a href="systemPage.jhtm?type=verifyAccount" class="userbutton"><fmt:message key="verifyAccount"/></a>
      <a href="systemPage.jhtm?type=faq" class="userbutton"><fmt:message key="faq" /></a>
      <a href="systemPage.jhtm?type=policy" class="userbutton"><fmt:message key="policy" /></a>
      <a href="systemPage.jhtm?type=myAccount" class="userbutton"><fmt:message key="myAccount" /></a>
      <a href="systemPage.jhtm?type=myAddress" class="userbutton"><fmt:message key="myAddress" /></a>
      <a href="systemPage.jhtm?type=changeEmailPassword" class="userbutton"><fmt:message key="changeEmailPassword" /></a>
      <a href="systemPage.jhtm?type=manageStoredAddress" class="userbutton"><fmt:message key="manageStoredAddress" /></a>
      <a href="systemPage.jhtm?type=viewEditMyAccountPage" class="userbutton"><fmt:message key="viewEditMyAccountPage" /></a>
      <a href="systemPage.jhtm?type=eventRegister" class="userbutton"><fmt:message key="eventRegister" /></a>
    <c:if test="${gSiteConfig['gSHOPPING_CART']}">
      <a href="systemPage.jhtm?type=myOrderHistory" class="userbutton"><fmt:message key="myOrderHistory" /></a>
      <a href="systemPage.jhtm?type=myOrderList" class="userbutton"><fmt:message key="myOrderList" /></a>
    </c:if>
    <c:if test="${gSiteConfig['gSUGAR_SYNC']}">
      <a href="systemPage.jhtm?type=sugarSync" class="userbutton"><fmt:message key="sugarSync" /></a>
    </c:if>
    <c:if test="${gSiteConfig['gMYLIST']}">
      <a href="systemPage.jhtm?type=myList" class="userbutton"><fmt:message key="myList" /></a>
    </c:if>
    <c:if test="${gSiteConfig['gLOCATION']}">
      <a href="systemPage.jhtm?type=location" class="userbutton"><fmt:message key="location" /></a>
    </c:if>
	<c:if test="${siteConfig['DELIVERY_TIME'].value == 'true'}">
      <a href="systemPage.jhtm?type=deliveryTime" class="userbutton"><fmt:message key="deliveryTime" /></a>
    </c:if>

    <h2 class="menuleft"><fmt:message key="catalog"/></h2>
    <div class="menudiv"></div> 
      <a href="systemPage.jhtm?type=product" class="userbutton"><fmt:message key="product" /></a>
      <c:if test="${siteConfig['PRODUCT1_DIRECTORY'].value != ''}">
         <a href="systemPage.jhtm?type=product1" class="userbutton"><fmt:message key="product" />1</a>
          <a href="systemPage.jhtm?type=product2" class="userbutton"><fmt:message key="product" />2</a>
      </c:if>
      <a href="systemPage.jhtm?type=productSearch" class="userbutton"><fmt:message key="productSearch" /></a>
      <a href="systemPage.jhtm?type=productNotFound" class="userbutton"><fmt:message key="productNotFound" /></a>
    <c:if test="${gSiteConfig['gPRODUCT_REVIEW']}">
      <a href="systemPage.jhtm?type=productReview" class="userbutton"><fmt:message key="productReview" /></a>
    </c:if>
    <a href="systemPage.jhtm?type=category" class="userbutton"><fmt:message key="category" /></a>
    
    <c:if test="${siteConfig['PRODUCT_QUOTE'].value == 'true'}">
    <h2 class="menuleft"><fmt:message key="quote"/></h2>
    <div class="menudiv"></div>
    	<a href="systemPage.jhtm?type=myQuoteHistory" class="userbutton"><fmt:message key="myQuoteHistory" /></a>
    	<a href="systemPage.jhtm?type=quoteLayout" class="userbutton"><fmt:message key="quoteLayout" /></a>
    	<a href="systemPage.jhtm?type=quoteList" class="userbutton"><fmt:message key="quoteList" /></a>
    </c:if>
    <c:if test="${gSiteConfig['gSUB_ACCOUNTS']}">    
    <h2 class="menuleft"><fmt:message key="subAccount"/></h2>
    <div class="menudiv"></div>
    <a href="systemPage.jhtm?type=subAccount" class="userbutton"><fmt:message key="subAccount" /></a>
    </c:if>
    <c:if test="${gSiteConfig['gGIFTCARD'] or gSiteConfig['gVIRTUAL_BANK_ACCOUNT']}">     
    <h2 class="menuleft"><fmt:message key="vba"/></h2>
    <div class="menudiv"></div>
    <a href="systemPage.jhtm?type=viewMyBalanceLayout" class="userbutton"><fmt:message key="viewMyBalance" /></a>
    <c:if test="${gSiteConfig['gVIRTUAL_BANK_ACCOUNT']}">
    	<a href="systemPage.jhtm?type=viewMyProductsLayout" class="userbutton"><fmt:message key="viewMyProducts" /></a>
    	<a href="systemPage.jhtm?type=viewMySalesLayout" class="userbutton"><fmt:message key="viewMySales" /></a>
    	<a href="systemPage.jhtm?type=viewMyReportLayout" class="userbutton"><fmt:message key="viewMyReport" /></a>
    </c:if>
    </c:if>
    <c:if test="${gSiteConfig['gSHOPPING_CART']}">
    <h2 class="menuleft"><fmt:message key="shoppingCart"/></h2>
    <div class="menudiv"></div> 
        <a href="systemPage.jhtm?type=cart" class="userbutton"><fmt:message key="cart" /></a>
        <a href="systemPage.jhtm?type=shipping" class="userbutton"><fmt:message key="shipping"/></a>
        <a href="systemPage.jhtm?type=billing" class="userbutton"><fmt:message key="billing"/></a>
        <a href="systemPage.jhtm?type=pinvoice" class="userbutton"><fmt:message key="pinvoice"/></a>
        <a href="systemPage.jhtm?type=finvoice" class="userbutton"><fmt:message key="finvoice"/></a>
        <a href="systemPage.jhtm?type=invoice" class="userbutton"><fmt:message key="invoice"/></a>
        <c:if test="${gSiteConfig['gSITE_DOMAIN'] == 'tcintlinc.com'}">
          <a href="systemPage.jhtm?type=invoice2" class="userbutton"><fmt:message key="invoice2"/></a>
          <a href="systemPage.jhtm?type=pickingSheet" class="userbutton"><fmt:message key="pickingSheet"/></a>
        </c:if>
        <%-- MTZ
        <c:if test="${gSiteConfig['gCUSTOMER_SUPPLIER']}">
        <a href="systemPage.jhtm?type=suppliersalelayout" class="userbutton"><fmt:message key="supplierSale"/></a>
		</c:if>--%>
        <c:if test="${siteConfig['CREDIT_AUTO_CHARGE'].value != ''}">
        <a href="systemPage.jhtm?type=ccfailed" class="userbutton"><fmt:message key="ccfailed"/></a>
		</c:if>
        <c:if test="${siteConfig['CENTINEL_URL'].value != ''}">
        <a href="systemPage.jhtm?type=cardinalCentinel" class="userbutton"><fmt:message key="cardinalCentinel"/></a>
        <a href="systemPage.jhtm?type=cardinalCentinelFailed" class="userbutton">C.Centinel Failure</a>
		</c:if>
        <c:if test="${gSiteConfig['gORDER_FILEUPLOAD'] > 0}">
        <a href="systemPage.jhtm?type=attachFiles" class="userbutton"><fmt:message key="attachFiles"/></a>        
        </c:if>
        <c:if test="${gSiteConfig['gGIFTCARD']}">
        	<a href="systemPage.jhtm?type=giftCard" class="userbutton"><fmt:message key="giftCard"/></a>
        </c:if>
        <c:if test="${gSiteConfig['gSHOPPING_CART_1']}">
        <h2 class="menuleft"><fmt:message key="shoppingCart"/> 1</h2>
        <div class="menudiv"></div>
            <a href="systemPage.jhtm?type=billingShipping" class="userbutton"><fmt:message key="billingShipping"/></a>
            <a href="systemPage.jhtm?type=shipping1" class="userbutton"><fmt:message key="shipping"/> 1</a>
	        <a href="systemPage.jhtm?type=billing1" class="userbutton"><fmt:message key="billing"/> 1</a>
	        <a href="systemPage.jhtm?type=pinvoice1" class="userbutton"><fmt:message key="pinvoice"/> 1</a>
	        <a href="systemPage.jhtm?type=finvoice1" class="userbutton"><fmt:message key="finvoice"/> 1</a>
        </c:if>        
        <c:if test="${siteConfig['CHECKOUT2_VISIBILITY'].value != '-1'}">
        <h2 class="menuleft"><fmt:message key="shoppingCart"/> 2</h2>
        <div class="menudiv"></div>
            <a href="systemPage.jhtm?type=shipping2" class="userbutton"><fmt:message key="shipping"/> 2</a>
	        <a href="systemPage.jhtm?type=billing2" class="userbutton"><fmt:message key="billing"/> 2</a>
	        <a href="systemPage.jhtm?type=pinvoice2" class="userbutton"><fmt:message key="pinvoice"/> 2</a>
	        <a href="systemPage.jhtm?type=finvoice2" class="userbutton"><fmt:message key="finvoice"/> 2</a>
        </c:if>
        
        <h2 class="menuleft"><fmt:message key="admin"/></h2>
        <div class="menudiv"></div>
        <c:if test="${gSiteConfig['gPDF_INVOICE']}">
          <a href="pdf.jhtm?type=invoice" class="userbutton"><fmt:message key="invoice"/> PDF</a>
        </c:if>
        <c:if test="${gSiteConfig['gPURCHASE_ORDER'] }">
          <a href="pdf.jhtm?type=po" class="userbutton"><fmt:message key="purchaseOrder"/> PDF</a>
        </c:if>
        <c:if test="${gSiteConfig['gPDF_INVOICE']}">
          <a href="pdf.jhtm?type=packing" class="userbutton"><fmt:message key="packingList"/> PDF</a>
        </c:if>
        <c:if test="${gSiteConfig['gPRESENTATION']}">
          <a href="pdf.jhtm?type=presentation" class="userbutton"><fmt:message key="presentation"/> PDF</a>
        </c:if>
        <a href="systemPage.jhtm?type=purchaseOrder" class="userbutton"><fmt:message key="purchaseOrder"/></a>
        <a href="systemPage.jhtm?type=packing" class="userbutton"><fmt:message key="packingList1"/></a>
        <a href="systemPage.jhtm?type=packing2" class="userbutton"><fmt:message key="packingList2"/></a>
        <a href="systemPage.jhtm?type=packing3" class="userbutton"><fmt:message key="packingList3"/></a>
        <a href="systemPage.jhtm?type=packing4" class="userbutton"><fmt:message key="packingList4"/></a>
        <c:if test="${gSiteConfig['gSUBSCRIPTION']}">
        <a href="systemPage.jhtm?type=subscription" class="userbutton"><fmt:message key="subscription"/></a>
        </c:if>  
    </c:if>
    
        <c:if test="${gSiteConfig['gSERVICE']}">
        <h2 class="menuleft"><fmt:message key="service"/></h2>
        <div class="menudiv"></div> 
        <a href="systemPage.jhtm?type=workOrder" class="userbutton"><fmt:message key="workOrder"/></a>
        </c:if>
        <c:if test="${gSiteConfig['gACCOUNTING'] == 'EVERGREEN'}">
        <h2 class="menuleft">MyEverGreen</h2>
        <div class="menudiv"></div> 
        <a href="systemPage.jhtm?type=evergreen-orders" class="userbutton"><fmt:message key="evergreen-orders"/></a>
        <a href="systemPage.jhtm?type=evergreen-order" class="userbutton"><fmt:message key="evergreen-order"/></a>
        <a href="systemPage.jhtm?type=evergreen-orderFulfillmentSchedule" class="userbutton" style="height:33px;"><fmt:message key="evergreen-orderFulfillmentSchedule"/></a>
        <a href="systemPage.jhtm?type=evergreen-orderFulfillmentDetails" class="userbutton"  style="height:33px;"><fmt:message key="evergreen-orderFulfillmentDetails"/></a>
        <a href="systemPage.jhtm?type=evergreen-myCreditsDamagedReportsList" class="userbutton"  style="height:48px;"><fmt:message key="evergreen-myCreditsDamagedReportsList"/></a>
        <a href="systemPage.jhtm?type=evergreen-myCreditsDamagedReport" class="userbutton"  style="height:33px;"><fmt:message key="evergreen-myCreditsDamagedReport"/></a>
        </c:if>  
        <c:if test="${siteConfig['MAS200'].value != ''}">
        <h2 class="menuleft">MAS200</h2>
        <div class="menudiv"></div> 
        <a href="systemPage.jhtm?type=jbd-orders" class="userbutton"><fmt:message key="jbd-orders"/></a>
        <a href="systemPage.jhtm?type=jbd-order" class="userbutton"><fmt:message key="jbd-order"/></a>
        </c:if> 
      	<c:if test="${gSiteConfig['gSALES_REP'] and siteConfig['SALESREP_LOGIN'].value == 'true'}">
      	<h2 class="menuleft"><fmt:message key="salesRep"/></h2>
        <div class="menudiv"></div> 
      	<a href="systemPage.jhtm?type=salesRep" class="userbutton"><fmt:message key="salesRep"/></a>
      	<a href="systemPage.jhtm?type=srLogin" class="userbutton"><fmt:message key="login" /> (<fmt:message key="salesRep"/>)</a>
      	</c:if>
        <c:if test="${gSiteConfig['gTICKET']}">
        <h2 class="menuleft"><fmt:message key="ticket"/></h2>
        <div class="menudiv"></div> 
        <a href="systemPage.jhtm?type=tickets" class="userbutton"><fmt:message key="tickets" /></a>
        <a href="systemPage.jhtm?type=newTicket" class="userbutton"><fmt:message key="newTicket" /></a>
        <a href="systemPage.jhtm?type=updateTicket" class="userbutton"><fmt:message key="updateTicket" /></a>
        </c:if>
    <div class="menudiv"></div>

    </td><td class="boxmidr" ></td></tr>
    <tr><td class="botl"></td><td class="botr"></td></tr></table>
  </div>