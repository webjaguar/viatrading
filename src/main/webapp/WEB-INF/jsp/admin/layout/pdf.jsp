<%@ page import="java.util.Random" %>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ taglib uri="/WEB-INF/tlds/spring.tld" prefix="spring" %>

<% request.setAttribute("randomNum", new Random().nextInt( 1000 )); %>
<tiles:insertDefinition name="admin.layout" flush="true">
  <tiles:putAttribute name="menu" value="/WEB-INF/jsp/admin/layout/menu.jsp" />
  <tiles:putAttribute name="content" type="string">

<script language="JavaScript"> 
<!--
window.addEvent('domready', function(){			
	var tabs1 = new SimpleTabs($('tab-block-1'), { selector: 'h4'});
});
//--> 
</script>
<form method="post" enctype="multipart/form-data">  
<!-- main box -->
  <div id="mboxfull">
  
  <!-- start table -->
  <table cellpadding="0" cellspacing="0" border="0" class="module" >
  <tr>
    <td class="topl_g">
	  
	  <!-- breadcrumb -->
	  <p class="breadcrumb">
	    <a href="../layout"><fmt:message key="layout"/></a> &gt;
        <c:choose>
          <c:when test="${fn:contains(param.type,'packing')}">
        	<fmt:message key="packingList"/> <c:out value="${fn:substringAfter(param.type,'packing')}"/>
          </c:when>
          <c:otherwise>
      		<fmt:message key="${param.type}"/>
          </c:otherwise>
        </c:choose>
		PDF 
	  </p>
	  
	  <!-- Error Message -->
	  <c:if test="${message != null}">
		  <div class="message"><spring:message code="${message}"/></div>
	  </c:if>
  
  </td><td class="topr_g" ></td></tr>
  <tr><td class="boxmidlrg" >

 <!-- tabs -->
 <div id="tab-block-1">
	<h4 title="${param.type} PDF">
      <c:choose>
        <c:when test="${fn:contains(param.type,'packing')}">
        	<fmt:message key="packingList"/> <c:out value="${fn:substringAfter(param.type,'packing')}"/>
        </c:when>
        <c:otherwise>
      		<fmt:message key="${param.type}"/>
        </c:otherwise>
      </c:choose>
      PDF
	</h4>
	<div>
	<!-- start tab -->
	<p>
        <c:set var="classIndex" value="0" />
        
	  	<div class="listdivi ln tabdivi"></div>
	  	<div class="listdivi"></div>
	   
		  	<div class="list${classIndex % 2}"><c:set var="classIndex" value="${classIndex+1}" />
		  	<div class="listfl"><fmt:message key="Header" /> <fmt:message key="image" />:</div>
		  	<div class="listp">
		  	<!-- input field -->
			   <c:if test="${hasHeaderImage}">
		    	<div class="thumbbox">
					<p class="thumb">
		    		  <img src="<c:url value="/assets/Image/Layout/pdf_${param.type}_header.gif"/>?rnd=${randomNum}" border="0">
					</p>
					<p class="uname">
					  <input name="__delete_header" type="checkbox" <c:if test="${param.__delete_header != null}">checked</c:if>> <fmt:message key="delete" />
					</p>
				</div>
		    	</c:if>
		    	<c:if test="${not hasHeaderImage}">
				<input value="browse" type="file" name="_image_header"/>
				</c:if>
		    <!-- end input field -->   	
		  	</div>
		  	</div>

		  	<div class="list${classIndex % 2}"><c:set var="classIndex" value="${classIndex+1}" />
		  		<c:if test="${not fn:contains(param.type,'presentation')}">
		  			<div class="listfl"><fmt:message key="Footer" /> <fmt:message key="image" /> 1:</div>
		  		</c:if>
		  	<div class="listp">
		  	<!-- input field -->
			   <c:if test="${hasFooterImage1}">
		    	<div class="thumbbox">
					<p class="thumb">
		    		  <img src="<c:url value="/assets/Image/Layout/pdf_${param.type}_footer_1.gif"/>?rnd=${randomNum}" border="0">
					</p>
					<p class="uname">
		    		  <input name="__delete_footer_1" type="checkbox" <c:if test="${param.__delete_footer_1 != null}">checked</c:if>> <fmt:message key="delete" />
					</p>
				</div>
		    	</c:if>
		    	<c:if test="${not hasFooterImage1}">
		    		<c:if test="${not fn:contains(param.type,'presentation')}">
						<input value="browse" type="file" name="_image_footer_1"/>
					</c:if>
				</c:if>
		    <!-- end input field -->   	
		  	</div>
		  	</div>
			
		  	<div class="list${classIndex % 2}"><c:set var="classIndex" value="${classIndex+1}" />
		  		<c:if test="${not fn:contains(param.type,'presentation')}">
		  			<div class="listfl"><fmt:message key="Footer" /> <fmt:message key="image" /> 2:</div>
		  		</c:if>
		  	<div class="listp">
		  	<!-- input field -->
			   <c:if test="${hasFooterImage2}">
		    	<div class="thumbbox">
					<p class="thumb">
					  <img src="<c:url value="/assets/Image/Layout/pdf_${param.type}_footer_2.gif"/>?rnd=${randomNum}" border="0" height="100">
					</p>
					<p class="uname">
					  <input name="__delete_footer_2" type="checkbox" <c:if test="${param.__delete_footer_2 != null}">checked</c:if>> <fmt:message key="delete" />
					</p>
				</div>
		    	</c:if>
		    	<c:if test="${not hasFooterImage2}">
		    		<c:if test="${not fn:contains(param.type,'presentation')}">
						<input value="browse" type="file" name="_image_footer_2"/>
					</c:if>   
				</c:if>          
		    <!-- end input field -->   	
		  	</div>
		  	</div>
		  	
		  		<div class="sup">
					to fit PDF make sure width of image is not more than 530 pixels
				</div>
		  	
	<!-- end tab -->		
	</div>
<!-- end tabs -->			
</div>

<!-- start button -->
	<div align="left" class="button">
        <input type="submit" value="<spring:message code="Update"/>" name="_update">
        <input type="submit" value="<spring:message code="cancel"/>" name="_cancel">
	</div>
<!-- end button -->	
    
  <!-- end table -->
    </td><td class="boxmidr" ></td></tr>
  <tr><td class="botl"></td><td class="botr"></td></tr>
  </table>
  
<!-- end main box -->  
 </div>		
 
</form>

  </tiles:putAttribute>    
</tiles:insertDefinition>
