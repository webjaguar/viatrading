<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/security/tags" prefix="sec" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>

<tiles:insertDefinition name="admin.supplier" flush="true">
	<tiles:putAttribute name="content" type="string">

<sec:authorize ifAnyGranted="ROLE_AEM,ROLE_ADMIN_${prop_site_id},ROLE_SUPPLIER_VIEW_LIST">	
<c:if test="${gSiteConfig['gSUPPLIER'] == true}">
<script type="text/javascript" src="../../admin/javascript/multiple.open.accordion1.2.js"></script>
<script type="text/javascript">
<!--
	var box2 = {};
	window.addEvent('domready', function(){
		$$('img.action').each(function(img){
			//containers
			var actionList = img.getParent(); 
			var actionHover = actionList.getElements('div.action-hover')[0];
			actionHover.set('opacity',0);
			//show/hide
			img.addEvent('mouseenter',function() {
				actionHover.setStyle('display','block').fade('in');
			});
			actionHover.addEvent('mouseleave',function(){
				actionHover.fade('out');
			});
			actionList.addEvent('mouseleave',function() {
				actionHover.fade('out');
			});
		});
		// Create the accordian
		var myAccordion = new Fx.Accordion($$('div.arrow_drop'), $$('div.information'), {
			display: 1, alwaysHide: true,
	    	onActive: function() {$('information').removeClass('displayNone');}
		});
		$('quickModeTrigger').addEvent('click',function() {
		    $('mboxfull').setStyle('margin', '0');
		    $$('.quickMode').each(function(el){
		    	el.hide();
		    });
		    $$('.quickModeRemove').each(function(el){
		    	el.destroy();
		    });
		    $$('.listingsHdr3').each(function(el){
		    	el.removeProperties('colspan', 'class');
		    });
		    alert('Select the whole page, copy and paste to your excel file.');
		  });
	});
	function toggleAll(el) {
		var ids = document.getElementsByName("__selected_id");	
	  	if (ids.length == 1)
	      document.list_form.__selected_id.checked = el.checked;
	    else
	      for (i = 0; i < ids.length; i++)
	        document.list_form.__selected_id[i].checked = el.checked;	
	} 
	function optionsSelected() {
		var ids = document.getElementsByName("__selected_id");	
	  	if (ids.length == 1) {
	      if (document.list_form.__selected_id.checked) {
	    	return true;
	      }
	    } else {
	      for (i = 0; i < ids.length; i++) {
	        if (document.list_form.__selected_id[i].checked) {
	    	  return true;
	        }
	      }
	    }

	    alert("Please select supplier(s).");       
	    return false;
	}
-->		
</script>

<form action="supplierList.jhtm" method="post" name="list_form" id="list">
<!-- main box -->
  <div id="mboxfull">
  
  <!-- start table -->
  <table cellpadding="0" cellspacing="0" border="0" class="module" >
  <tr class="quickMode">
    <td class="topl_g">
	  
	  <!-- breadcrumb -->
	  <p class="breadcrumb">
	    <a href="../supplier"><fmt:message key="supplier" /></a> &gt;
	    <fmt:message key="suppliers" />
	  </p>
	  
	  <!-- Error Message -->
	  <c:if test="${model.message != null}">
	    <div class="message"><c:out value="${model.message}"/></div>
	  </c:if>
	  
	  <!-- header image -->
	  <img class="headerImage" id="quickModeTrigger" src="../graphics/supplier.gif" />
	    
	  <c:if test="${model.suppliers.nrOfElements > 0}">
	    <div class="optionBox"><div class="titlePrivilege"><fmt:message key="options" /></div></div>
		<div class="arrow_drop arrowImageRight"></div>
		<div style="clear: both;"></div>
		<div class="information displayNone" id="information" style="float:left;padding: 5px">
	      <div style="float:left;padding: 5px">
	        <table class="productBatchTool" cellpadding="5" cellspacing="5">
	           <tr>
	            <td valign="top">
	             <table cellpadding="1" cellspacing="1">
	              <tr style="height:30px;white-space: nowrap">
	               <td valign="top" width="20px"><img class="toolTipImg" title="Active::Activate or Deactivate following supplier(s) in the list." src="../graphics/question.gif" /></td>
	               <td valign="top"><h3><fmt:message key="active" /></h3></td>
	              </tr>
	             </table>
	            </td>
	           </tr>
	           <tr>
	            <td valign="top">
	             <table cellpadding="1" cellspacing="1">
	              <tr style="">
			       <td>
			       <select name="__active_mode">
				    <option value="1" ><c:out value="active"/></option>
				    <option value="0" ><c:out value="inActive"/></option>
				   </select>
				   <div align="left" class="buttonLeft">
		            <input type="submit" name="__activeBacher" value="Apply" onClick="return optionsSelected()">
		           </div>
			       </td>
			      </tr>
	             </table>
	            </td>
	           </tr>
	           </table>
			</div>
			
			<c:if test="${siteConfig['LUCENE_REINDEX'].value == 'true'}">
     		<div style="float:left;padding: 5px">
	        <table class="productBatchTool" cellpadding="5" cellspacing="5">
	            <tr>
	              <td valign="top">
	                <table cellpadding="1" cellspacing="1">
	                  <tr style="height:30px;white-space: nowrap">
	                    <td valign="top" width="20px"><img class="toolTipImg" title="Rank::Order the products in search result based on this rank. Rank 1 is for highest ranking and 100 is for lowset ranking." src="../graphics/question.gif" /></td>
	               		<td valign="top"><h3><fmt:message key="rank" /></h3></td>
	              	  </tr>
	             	</table>
	              </td>
	            </tr>
	            <tr>
	              <td valign="top">
	             	<table cellpadding="1" cellspacing="1">
	              	  <tr style="">
			       		<td>
			       		  <select name="__rank">
			       		    <c:forEach begin="1" end="100" var="rank">
			       		      <option value="${rank}" ><c:out value="${rank}"/></option>
				    		</c:forEach>
				    	  </select>
				   		  <div align="left" class="buttonLeft">
		            		<input type="submit" name="__updateRankingBacher" value="Apply" onClick="return optionsSelected()">
		         		  </div>
			       		</td>
			      	  </tr>
	                </table>
	              </td>
	            </tr>
	          </table>
			</div>
			</c:if>
	  		</div>
	  </c:if>
	  
	  </td><td class="topr_g quickMode" ></td></tr>
  <tr><td class="boxmidlrg" >

 <!-- tabs -->
 <div class="tab-wrapper">
	<h4 class="quickMode" title=""></h4>
	<div>
	<!-- start tab -->

	  	<div class="listdivi quickMode"></div>
	   
		  	<div class="listlight">
		  	<!-- input field --> 
		  	<table border="0" cellpadding="0" cellspacing="1" width="100%" class="quickMode">
		    <tr>
			<td>&nbsp;
			</td>
			<c:if test="${model.suppliers.nrOfElements > 0}">
				<td class="pageShowing">
					<fmt:message key="showing">
						<fmt:param value="${model.suppliers.firstElementOnPage + 1}" />
						<fmt:param value="${model.suppliers.lastElementOnPage + 1}" />
						<fmt:param value="${model.suppliers.nrOfElements}" />
					</fmt:message>
				</td>
			</c:if>
			<td class="pageNavi">
				Page
				<select name="page" id="page" onchange="submit()">
					<c:forEach begin="1" end="${model.suppliers.pageCount}"
						var="page">
						<option value="${page}"
							<c:if test="${page == (model.suppliers.page+1)}">selected</c:if>>
							${page}
						</option>
					</c:forEach>
				</select> 
				of
				<c:out value="${model.suppliers.pageCount}" /> 
				|
				<c:if test="${model.suppliers.firstPage}">
					<span class="pageNaviDead"><fmt:message key="previous" /></span>
				</c:if>
				<c:if test="${not model.suppliers.firstPage}">
					<a
						href="<c:url value="supplierList.jhtm"><c:param name="page" value="${model.suppliers.page}"/>supplierList.jhtm<c:param name="size" value="${model.suppliers.pageSize}"/></c:url>"
						class="pageNaviLink"><fmt:message key="previous" /></a>
				</c:if>
				|
				<c:if test="${model.suppliers.lastPage}">
					<span class="pageNaviDead"><fmt:message key="next" /></span>
				</c:if>
				<c:if test="${not model.suppliers.lastPage}">
					<a
						href="<c:url value="supplierList.jhtm"><c:param name="page" value="${model.suppliers.page+2}"/><c:param name="size" value="${model.suppliers.pageSize}"/></c:url>"
						class="pageNaviLink"><fmt:message key="next" /></a>
				</c:if>
				</td>
				</tr>
				</table>
			
				<table border="0" cellpadding="0" cellspacing="0" width="100%"
					class="listings">
					<tr class="listingsHdr2">
						<td align="center" class="quickMode"><input type="checkbox" onclick="toggleAll(this)"></td>
						<td class="indexCol quickMode">&nbsp;</td>
						<td class="listingsHdr3"><fmt:message key="Name" /></td>
						<td class="listingsHdr3"><fmt:message key="supplierId" /></td>
						<c:if test="${model.prefix == true}">
							<td class="listingsHdr3"><fmt:message key="supplierPrefix" /></td>
						</c:if>
						<td class="listingsHdr3"><fmt:message key="city" /></td>
						<td class="listingsHdr3"><fmt:message key="state" /></td>
						<td class="listingsHdr3"><fmt:message key="zipCode" /></td>
						<td class="listingsHdr3"><fmt:message key="phone" /></td>
						<td class="listingsHdr3"><fmt:message key="fax" /></td>
						<c:if test="${siteConfig['SUPPLIER_MULTI_ADDRESS'].value == 'true'}">
						<td class="listingsHdr3"><fmt:message key="action" /></td>
						</c:if>
						<td class="listingsHdr3 quickMode"><fmt:message key="active" /></td>
						<td class="listingsHdr3"><fmt:message key="products" /></td>
						
					</tr>
					<c:forEach items="${model.suppliers.pageList}" var="supplier"	varStatus="status">
						<tr class="row${status.index % 2}"	onmouseover="changeStyleClass(this,'rowOver')" 	onmouseout="changeStyleClass(this,'row${status.index % 2}')">
							<td class="idCol quickMode">
							    <input type="checkbox" name="__selected_id" value="${supplier.id}">
							</td>							
							<td align="center" class="quickMode">
								<c:if test="${supplier.supplierPrefix != null}">
							    <img class="action" width="23" height="16" alt="Actions" src="../graphics/actions.png"/>
							    <div class="action-hover" style="display: none;">
							        <ul class="round">
							            <li class="action-header">
									      <div class="name"><c:out value="${supplier.address.firstName}"/> <c:out value="${supplier.address.lastName}"/></div>
									      <div class="id"><c:out value="${supplier.id}"/></div>
									      <div class="menudiv"></div>								       
							            </li>						            
							            <sec:authorize ifAnyGranted="ROLE_AEM,ROLE_ADMIN_${prop_site_id}">
								          <li class="productAdd link"><a href="../catalog/product.jhtm?supId=${supplier.id}&prefix=${supplier.supplierPrefix}"><fmt:message key="productAdd" /></a></li>
								        </sec:authorize> 
							        </ul>
							    </div>
							    </c:if>
							</td>							
							<td class="nameCol">
								<a href="supplier.jhtm?sid=${supplier.id}" class="nameLink<c:if test="${!supplier.active}">Inactive</c:if>"><c:out value="${supplier.address.company}" /></a>
							</td>							
							<td class="idCol">
							    <c:out value="${supplier.id}" />
						    </td>
						    <c:if test="${model.prefix == true}">
								<td class="">
									<c:out value="${supplier.supplierPrefix}"/>
								</td>								
							</c:if>
							<td class="nameCol">
								<c:out value="${supplier.address.city}" />
							</td>
							<td class="nameCol">
								<c:out value="${supplier.address.stateProvince}" />
							</td>
							<td class="nameCol">
								<c:out value="${supplier.address.zip}" />
							</td>
							<td class="nameCol">
								<c:out value="${supplier.address.phone}" />
							</td>
							<td class="nameCol">
								<c:out value="${supplier.address.fax}" />
							</td>
							<c:if test="${siteConfig['SUPPLIER_MULTI_ADDRESS'].value == 'true'}">
							<td class="nameCol">
								<a href="addressList.jhtm?sid=${supplier.id}" class="nameLink"><fmt:message key="address" /></a>
							</td>
							</c:if>
							<td class="nameCol quickMode">
								<c:if test="${supplier.active}"><div align="left"><img src="../graphics/checkbox.png" /></div></c:if>
							</td>						
							<td class="nameCol">
								<c:forEach items="${model.supplierProductCount}" var="entry">
								  <c:if test="${entry.key == supplier.id}">
								     <a href="../catalog/productList.jhtm?supplier_company=${supplier.address.company}">
								     <c:out value="${entry.value}"></c:out>
								     </a>
								  </c:if>
								</c:forEach>
							</td>						
						</tr>
					</c:forEach>
					<c:if test="${model.suppliers.nrOfElements == 0}">
						<tr class="emptyList">
							<td colspan="8">
								&nbsp;
							</td>
						</tr>
					</c:if>
				</table>
				
				<c:if test="${model.suppliers.nrOfElements > 0}">
					<div class="sup quickMode">
						&sup1; <fmt:message key="toEditClickSupplierName" /> 
					</div>
				</c:if>
				
				<table border="0" cellpadding="0" cellspacing="1" width="100%" class="quickMode">
				  <tr>
				  <td class="pageSize">
					<select name="size"
						onchange="document.getElementById('page').value=1;submit()">
						<c:forTokens items="10,25,50,100,500" delims="," var="current">
							<option value="${current}"
								<c:if test="${current == model.suppliers.pageSize}">selected</c:if>>
								${current}
								<fmt:message key="perPage" />
							</option>
						</c:forTokens>
					</select>
				  </td>
				  </tr>
			    </table> 	
		  	<!-- end input field -->  	  
		  	
		  	<!-- start button --> 
            <div align="left" class="button quickMode">
			  <sec:authorize ifAnyGranted="ROLE_AEM,ROLE_ADMIN_${prop_site_id},ROLE_SUPPLIER_CREATE">
				<input type="submit" name="__add" value="<fmt:message key="add" /> <fmt:message key="supplier" />"/>
			  </sec:authorize>	
			</div>		
			<!-- end button -->	 
			</div>
			
	<!-- end tab -->        
	</div>
<!-- end tabs -->			
</div>
  
  <!-- end table -->
   </td><td class="boxmidr quickMode" ></td></tr>
  <tr><td class="botl quickMode"></td><td class="botr quickMode"></td></tr>
  </table>
  
<!-- end main box -->  
</div> 
</form>
</c:if>
</sec:authorize>

	</tiles:putAttribute>
</tiles:insertDefinition>