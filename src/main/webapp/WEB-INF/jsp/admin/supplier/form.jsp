<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="/WEB-INF/tlds/spring.tld" prefix="spring" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib uri="http://www.springframework.org/security/tags" prefix="sec" %>


<tiles:insertDefinition name="admin.supplier" flush="true">
  <tiles:putAttribute name="content" type="string"> 
<sec:authorize ifAnyGranted="ROLE_AEM,ROLE_ADMIN_${prop_site_id},ROLE_SUPPLIER_CREATE,ROLE_SUPPLIER_UPDATE,ROLE_SUPPLIER_DELETE">

<c:if test="${gSiteConfig['gSUPPLIER'] == true}">
<script type="text/javascript"> 
<!--
window.addEvent('domready', function(){			
	var tabs1 = new SimpleTabs($('tab-block-1'), { selector: 'h4'});
	var Tips1 = new Tips($$('.toolTipImg'));
});
//--> 
</script>
<form:form commandName="supplierForm" method="post" >
<!-- main box -->
  <div id="mboxfull">
  
  <!-- start table -->
  <table cellpadding="0" cellspacing="0" border="0" class="module" >
  <tr>
    <td class="topl_g">
	  
	  <!-- breadcrumb -->
	  <p class="breadcrumb">
	    <a href="../supplier"><fmt:message key='supplier' /></a> &gt;
	    form 
	  </p>
	  
	  <!-- Error Message -->
	  <c:if test="${message != null}">
	    <div class="message"><spring:message code="${message}"/></div>
	  </c:if>
	  <spring:hasBindErrors name="supplierForm">
		<div class="message">Please fix all errors!</div>
	  </spring:hasBindErrors>
	  
	  
  </td><td class="topr_g" ></td></tr>
  <tr><td class="boxmidlrg" >

 <!-- tabs -->
 <div id="tab-block-1">
	<h4 title="<fmt:message key='supplier'/> Form"><fmt:message key="supplier" /></h4>
	<div>
	<!-- start tab -->

        <c:set var="classIndex" value="0" />
	  	<div class="listdivi"></div>
	  	
		  	<div class="list${classIndex % 2}"><c:set var="classIndex" value="${classIndex+1}" />
		  	<div class="listfl"><div class="helpImg"><img class="toolTipImg" title="Note::If supplier is inactive, all products associated to this supplier will become inactive." src="../graphics/question.gif" /></div><div class="requiredField"><fmt:message key="active" />:</div></div>
		  	<div class="listp">
		  	<!-- input field -->
				<form:checkbox path="supplier.active" />
			<!-- end input field -->	
	 		</div>
		  	</div>

		  	<div class="list${classIndex % 2}"><c:set var="classIndex" value="${classIndex+1}" />
		  	<div class="listfl"><div class="requiredField"><fmt:message key='company' /> <fmt:message key='Name' />:</div></div>
		  	<div class="listp">
		  	<!-- input field -->
		  		<c:if test="${supplierForm.supplier.userId != null}">
				<c:out value="${supplierForm.supplier.address.company}"/>
				</c:if>
		  		<c:if test="${supplierForm.supplier.userId == null}">
				<form:input path="supplier.address.company" htmlEscape="true" />
				</c:if>
				<form:errors path="supplier.address.company" cssClass="error"/>
			<!-- end input field -->	
	 		</div>
		  	</div>	
		  	
		  	<div class="list${classIndex % 2}"><c:set var="classIndex" value="${classIndex+1}" />
		  	<div class="listfl"><div class="helpImg"><img class="toolTipImg" title="Note::Your Account Number with this supplier. This Account Number is needed when you want to create Purchase Order." src="../graphics/question.gif" /></div><div class="requiredField"><fmt:message key="accountNumber" />:</div></div>
		  	<div class="listp">
		  	<!-- input field -->
				<form:input path="supplier.accountNumber"/>
				<form:errors path="supplier.accountNumber" cssClass="error"/>
			<!-- end input field -->	
	 		</div>
		  	</div>	
		  	
		  	<div class="list${classIndex % 2}"><c:set var="classIndex" value="${classIndex+1}" />
		  	<div class="listfl"><div class="helpImg"><img class="toolTipImg" title="Note::Supplier Prefix" src="../graphics/question.gif" /></div><div>Supplier Prefix:</div></div>
		  	<div class="listp">
		  	<!-- input field -->
				<c:out value="${supplierForm.supplier.supplierPrefix}"/>
			<!-- end input field -->	
	 		</div>
		  	</div>
		  	
		  	<div class="list${classIndex % 2}"><c:set var="classIndex" value="${classIndex+1}" />
		  	<div class="listfl"><div class="">Vendor Group:</div></div>
		  	<div class="listp">
		  	<!-- input field -->
				<form:input path="supplier.group" htmlEscape="true" />
				<form:errors path="supplier.group" cssClass="error"/>
			<!-- end input field -->	
	 		</div>
		  	</div>	
		  	
		   <div class="list${classIndex % 2}"><c:set var="classIndex" value="${classIndex+1}" />
		  	<div class="listfl"><div class="">Vendor DBA:</div></div>
		  	<div class="listp">
		  	<!-- input field -->
				<form:input path="supplier.dba" htmlEscape="true" />
				<form:errors path="supplier.dba" cssClass="error"/>
			<!-- end input field -->	
	 		</div>
		  	</div>	
		  	
		  	<div class="list${classIndex % 2}"><c:set var="classIndex" value="${classIndex+1}" />
		  	<div class="listfl"><div class="helpImg"><img class="toolTipImg" title="Note::contact person first name." src="../graphics/question.gif" /></div><fmt:message key="firstName" />:</div>
		  	<div class="listp">
		  	<!-- input field -->
		  		<c:if test="${supplierForm.supplier.userId != null}">
				<c:out value="${supplierForm.supplier.address.firstName}"/>
				</c:if>
		  		<c:if test="${supplierForm.supplier.userId == null}">
		        <form:input path="supplier.address.firstName" htmlEscape="true" />
		        </c:if>
		        <form:errors path="supplier.address.firstName" cssClass="error" />
	 		<!-- end input field -->	
	 		</div>
		  	</div>
		  	
		  	<div class="list${classIndex % 2}"><c:set var="classIndex" value="${classIndex+1}" />
		  	<div class="listfl"><div class="helpImg"><img class="toolTipImg" title="Note::Contact person last name." src="../graphics/question.gif" /></div><fmt:message key="lastName" />:</div>
		  	<div class="listp">
		  	<!-- input field -->
		  		<c:if test="${supplierForm.supplier.userId != null}">
				<c:out value="${supplierForm.supplier.address.lastName}"/>
				</c:if>
		  		<c:if test="${supplierForm.supplier.userId == null}">
		        <form:input path="supplier.address.lastName" htmlEscape="true" />
		        </c:if>
		        <form:errors path="supplier.address.lastName" cssClass="error" />
	 		<!-- end input field -->	
	 		</div>
		  	</div>
		  	
		  	<div class="list${classIndex % 2}"><c:set var="classIndex" value="${classIndex+1}" />
		  	<div class="listfl"><div class="requiredField"><fmt:message key='address' /> 1:</div></div>
		  	<div class="listp">
		  	<!-- input field -->
		  		<c:if test="${supplierForm.supplier.userId != null}">
				<c:out value="${supplierForm.supplier.address.addr1}"/>
				</c:if>
		  		<c:if test="${supplierForm.supplier.userId == null}">
		        <form:input path="supplier.address.addr1" htmlEscape="true" />
				</c:if>		       
		        <form:errors path="supplier.address.addr1" cssClass="error" />
	 		<!-- end input field -->	
	 		</div>
		  	</div>
	  		  	
		  	<div class="list${classIndex % 2}"><c:set var="classIndex" value="${classIndex+1}" />
		  	<div class="listfl"><fmt:message key='address' /> 2:</div>
		  	<div class="listp">
		  	<!-- input field -->
		  		<c:if test="${supplierForm.supplier.userId != null}">
				<c:out value="${supplierForm.supplier.address.addr2}"/>
				</c:if>
		  		<c:if test="${supplierForm.supplier.userId == null}">
 		        <form:input path="supplier.address.addr2" htmlEscape="true" />
 		        </c:if>
		        <form:errors path="supplier.address.addr2" cssClass="error" />
	 		<!-- end input field -->	
	 		</div>
		  	</div>

		  	<div class="list${classIndex % 2}"><c:set var="classIndex" value="${classIndex+1}" />
		  	<div class="listfl"><div class="requiredField"><fmt:message key='country' />:</div></div>
		  	<div class="listp">
		  	<!-- input field -->
		  		<c:if test="${supplierForm.supplier.userId != null}">
				<c:out value="${countryMap[supplierForm.supplier.address.country]}"/>
				</c:if>
		  		<c:if test="${supplierForm.supplier.userId == null}">		  	
		        <form:select id="country" path="supplier.address.country" onchange="toggleStateProvince(this)">
		         <form:option value="" label="Please Select"/>
		         <form:options items="${countries}" itemValue="code" itemLabel="name"/>
		        </form:select>
		        </c:if>
		        <form:errors path="supplier.address.country" cssClass="error" />
	 		<!-- end input field -->	
	 		</div>
		  	</div>

		  	<div class="list${classIndex % 2}"><c:set var="classIndex" value="${classIndex+1}" />
		  	<div class="listfl"><div class="requiredField"><fmt:message key="city" />:</div></div>
		  	<div class="listp">
		  	<!-- input field -->
		  		<c:if test="${supplierForm.supplier.userId != null}">
				<c:out value="${supplierForm.supplier.address.city}"/>
				</c:if>
		  		<c:if test="${supplierForm.supplier.userId == null}">		  	
				<form:input path="supplier.address.city" htmlEscape="true" />
				</c:if>
				<form:errors path="supplier.address.city" cssClass="error" />
	 		<!-- end input field -->	
	 		</div>
		  	</div>

		  	<div class="list${classIndex % 2}"><c:set var="classIndex" value="${classIndex+1}" />
		  	<div class="listfl"><div class="requiredField"><fmt:message key="stateProvince" />:</div></div>
		  	<div class="listp">
		  	<!-- input field -->
		  		<c:if test="${supplierForm.supplier.userId != null}">
				<c:out value="${supplierForm.supplier.address.stateProvince}"/>
				</c:if>
		  		<c:if test="${supplierForm.supplier.userId == null}">
		        <form:select id="state" path="supplier.address.stateProvince">
		         <form:option value="" label="Please Select"/>
		         <form:options items="${states}" itemValue="code" itemLabel="name"/>
		        </form:select>
		        <form:select id="ca_province" path="supplier.address.stateProvince">
		         <form:option value="" label="Please Select"/>
		         <form:options items="${caProvinceList}" itemValue="code" itemLabel="name"/>
		        </form:select>
		        <form:input id="province" path="supplier.address.stateProvince"/>
		        </c:if>
		        <form:errors path="supplier.address.stateProvince" cssClass="error" />
	 		<!-- end input field -->	
	 		</div>
		  	</div>

		  	<div class="list${classIndex % 2}"><c:set var="classIndex" value="${classIndex+1}" />
		  	<div class="listfl"><div class="requiredField"><fmt:message key="zipCode" />:</div></div>
		  	<div class="listp">
		  	<!-- input field -->   
		  		<c:if test="${supplierForm.supplier.userId != null}">
				<c:out value="${supplierForm.supplier.address.zip}"/>
				</c:if>
		  		<c:if test="${supplierForm.supplier.userId == null}">
		        <form:input path="supplier.address.zip" htmlEscape="true" />
		        </c:if>
		        <form:errors path="supplier.address.zip" cssClass="error" />
	 		<!-- end input field -->	
	 		</div>
		  	</div>

		  	<div class="list${classIndex % 2}"><c:set var="classIndex" value="${classIndex+1}" />
		  	<div class="listfl"><fmt:message key="phone" />:</div>
		  	<div class="listp">
		  	<!-- input field -->
		  		<c:if test="${supplierForm.supplier.userId != null}">
				<c:out value="${supplierForm.supplier.address.phone}"/>
				</c:if>
		  		<c:if test="${supplierForm.supplier.userId == null}">
		        <form:input path="supplier.address.phone"  htmlEscape="true" />
		        </c:if>
		        <form:errors path="supplier.address.phone" cssClass="error" />
	 		<!-- end input field -->	
	 		</div>
		  	</div>
		  	
		  	<div class="list${classIndex % 2}"><c:set var="classIndex" value="${classIndex+1}" />
		  	<div class="listfl"><fmt:message key="fax" />:</div>
		  	<div class="listp">
		  	<!-- input field -->
		  		<c:if test="${supplierForm.supplier.userId != null}">
				<c:out value="${supplierForm.supplier.address.fax}"/>
				</c:if>
		  		<c:if test="${supplierForm.supplier.userId == null}">
		        <form:input path="supplier.address.fax"  htmlEscape="true" />
		        </c:if>
		        <form:errors path="supplier.address.fax" cssClass="error" />
	 		<!-- end input field -->	
	 		</div>
		  	</div>
		   
		  	<div class="list${classIndex % 2}"><c:set var="classIndex" value="${classIndex+1}" />
		  	<div class="listfl"><div class="requiredField"><fmt:message key="email" />:</div></div>
		  	<div class="listp">
		  	<!-- input field -->
		  		<c:if test="${supplierForm.supplier.userId != null}">
				<a href="../customers/customer.jhtm?id=${supplierForm.supplier.userId}"><c:out value="${supplierForm.supplier.address.email}"/></a>
				</c:if>
		  		<c:if test="${supplierForm.supplier.userId == null}">
		        <form:input path="supplier.address.email" maxlength="80" size="40" htmlEscape="true" />
		        </c:if>
		        <form:errors path="supplier.address.email" cssClass="error" />
	 		<!-- end input field -->	
	 		</div>
		  	</div>
		  	
		  	<div class="list${classIndex % 2}"><c:set var="classIndex" value="${classIndex+1}" />
		  	<div class="listfl"><fmt:message key="note" />:</div>
		  	<div class="listp">
		  	<!-- input field -->
		       <form:textarea path="supplier.note" rows="15" cols="60" cssClass="textfield" htmlEscape="true"/>
		       <form:errors path="supplier.note" cssClass="error" />
	 		<!-- end input field -->	
	 		</div>
		  	</div>
          
	<!-- end tab -->		
	</div>
	
	<h4 title="<fmt:message key='supplier'/> Form">Payment Info</h4>
		<div>
	<!-- start tab -->
        <c:set var="classIndex" value="0" />
        <div class="listdivi ln tabdivi"></div>
	  	<div class="listdivi"></div>
		  	<div class="list${classIndex % 2}"><c:set var="classIndex" value="${classIndex+1}" />
		  	<div class="listfl"><div class="helpImg"><img class="toolTipImg" title="Payment Preference::Select payment preference." src="../graphics/question.gif" /></div>Payment Preference:</div>
		  	<div class="listp">
				<form:radiobutton path="supplier.paymentPreference" cssStyle="width: 80px;" value="check" />
				Check <br>
				<form:radiobutton path="supplier.paymentPreference" cssStyle="width: 80px;" value="transfer" />
				Transfer <br>
				<form:radiobutton path="supplier.paymentPreference" cssStyle="width: 80px;" value="other" />
				Other
	 		</div>
		  	</div>
			<div class="list${classIndex % 2}"><c:set var="classIndex" value="${classIndex+1}" />
			  	<div class="listfl"><div class="helpImg"><img class="toolTipImg" title="Note::Mail check to" src="../graphics/question.gif" /></div>Mail Check to:</div>
			  	<div class="listp input-parent">
			  	    <form:input path="supplier.mailCheckTo"/>
		 		</div>
		  	</div>
			<div class="list${classIndex % 2}"><c:set var="classIndex" value="${classIndex+1}" />
			  	<div class="listfl"><div class="helpImg"><img class="toolTipImg" title="Note::Transfer money to" src="../graphics/question.gif" /></div>Transfer Money to:</div>
			  	<div class="listp input-parent">
			  	    <form:input path="supplier.transferMoneyTo"/>
		 		</div>
		  	</div>
	<!-- end tab -->		
	</div>

	<c:if test="${gSiteConfig['gASI'] != '' or siteConfig['PREMIER_DISTRIBUTOR'].value != ''}">
	<h4 title="<fmt:message key='supplier'/> Form"><fmt:message key="markUp" /></h4>
	<div>
	<!-- start tab -->

        <c:set var="classIndex" value="0" />
        <div class="listdivi ln tabdivi"></div>
	  	<div class="listdivi"></div>
	  	
	  	    <c:if test="${siteConfig['ASI_ENDQTYPRICING'].value == 'true'}">
		  	<div class="list${classIndex % 2}"><c:set var="classIndex" value="${classIndex+1}" />
		  	<div class="listfl"><div class="helpImg"><img class="toolTipImg" title="EQP::Use to generate Purchase Order based on EQP." src="../graphics/question.gif" /></div><fmt:message key="endQtyPricing" />:</div>
		  	<div class="listp">
		  	<!-- input field -->
				<form:checkbox path="supplier.endQtyPricing" />
			<!-- end input field -->	
	 		</div>
		  	</div>
		  	</c:if>		  	
	   	
       	    <c:if test="${gSiteConfig['gASI'] != ''}">
			<div class="list${classIndex % 2}"><c:set var="classIndex" value="${classIndex+1}" />
		  	<div class="listfl"><div class="helpImg"></div><fmt:message key="markUp" />/<fmt:message key="margin" />:</div>
		  	<div class="listp">
		  	<!-- input field -->
				<form:select path="supplier.markupFormula" cssStyle="width: 145px;" >
				  <form:option value="0">Markup</form:option>
				  <form:option value="1">Margin</form:option>
				</form:select>
			<!-- end input field -->	
	 		</div>
		  	</div>
		  	
		  	<div class="list${classIndex % 2}"><c:set var="classIndex" value="${classIndex+1}" />
		  	<div class="listfl"><div class="helpImg"><img class="toolTipImg" title="Markup based on EQP::Markup will be added based on EQP." src="../graphics/question.gif" /></div><fmt:message key="markUpBasedOn" />:</div>
		  	<div class="listp">
		  	<!-- input field -->
				<form:select path="supplier.markupType" cssStyle="width: 145px;" >
				  <form:option value="0">Regular Prices</form:option>
				  <form:option value="1">End Qty Price</form:option>
				  <form:option value="2">Regualr Cost</form:option>
				  <form:option value="3">End Qty Cost</form:option>
				</form:select>
			<!-- end input field -->	
	 		</div>
		  	</div>
		  	
			<c:forTokens items="1,2,3,4,5,6,7,8,9,10" delims="," var="index" varStatus="status">
			<div class="list${classIndex % 2}"><c:set var="classIndex" value="${classIndex+1}" />
		  	<div class="listfl"><div class="helpImg"><img class="toolTipImg" title="Note::Add/Deduct mark up percentage to/from the price. Add negative value to deduct markup value from price." src="../graphics/question.gif" /></div><fmt:message key="markUp"/> ${index}:</div>
		  	<div class="listp">
		  	<!-- input field -->
		  	    <form:input path="supplier.markup${index}"/>%
			<!-- end input field -->	
	 		</div>
		  	</div>
		  	</c:forTokens>
		  	</c:if>
			  
	<!-- end tab -->		
	</div>
	</c:if>
	
<!-- end tabs -->			
</div>

<!-- start button -->
<div align="left" class="button">
	<c:choose>
		<c:when test="${supplierForm.newSupplier}">
		  <sec:authorize ifAnyGranted="ROLE_AEM,ROLE_ADMIN_${prop_site_id},ROLE_SUPPLIER_CREATE">
			<input type="submit" value="<fmt:message key='add' />" />
		  </sec:authorize>	 
		</c:when>
		<c:otherwise>
		  <sec:authorize ifAnyGranted="ROLE_AEM,ROLE_ADMIN_${prop_site_id},ROLE_SUPPLIER_UPDATE">
			<input type="submit" value="<fmt:message key='Update' />" /> 
		  </sec:authorize>	
		  <sec:authorize ifAnyGranted="ROLE_AEM,ROLE_ADMIN_${prop_site_id},ROLE_SUPPLIER_DELETE">	
		    <c:if test="${supplierForm.supplier.userId == null}">
			<input type="submit" name="delete" value="<fmt:message key='delete' />" onClick="return confirm('Delete permanently?')"/>
			</c:if>
		  </sec:authorize>	  
		</c:otherwise>
	</c:choose>
	<input type="submit" value="<spring:message code="cancel"/>" name="_cancel">
</div>
<!-- end button -->	
 
  <!-- end table -->
    </td><td class="boxmidr" ></td></tr>
  <tr><td class="botl"></td><td class="botr"></td></tr>
  </table>
  
<!-- end main box -->  
</div>
</form:form>

<script type="text/javascript">
<!--
function toggleStateProvince(el) {
  if (el.value == "US") {
      document.getElementById('state').disabled=false;
      document.getElementById('state').style.display="block";
      document.getElementById('ca_province').disabled=true;
      document.getElementById('ca_province').style.display="none";
      document.getElementById('province').disabled=true;
      document.getElementById('province').style.display="none";
  } else if (el.value == "CA") {
      document.getElementById('state').disabled=true;
      document.getElementById('state').style.display="none";
      document.getElementById('ca_province').disabled=false;
      document.getElementById('ca_province').style.display="block";
      document.getElementById('province').disabled=true;
      document.getElementById('province').style.display="none";
  } else {
      document.getElementById('state').disabled=true;
      document.getElementById('state').style.display="none";
      document.getElementById('ca_province').disabled=true;
      document.getElementById('ca_province').style.display="none";
      document.getElementById('province').disabled=false;
      document.getElementById('province').style.display="block";
  }
}
toggleStateProvince(document.getElementById('country'));
//-->
</script>
</c:if>
<style>
.input-parent input {width: 50%;}
</style>
</sec:authorize>

  </tiles:putAttribute>    
</tiles:insertDefinition>