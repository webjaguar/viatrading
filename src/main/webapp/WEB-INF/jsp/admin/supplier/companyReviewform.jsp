<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="/WEB-INF/tlds/spring.tld" prefix="spring" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>

<tiles:insertDefinition name="admin.supplier.review" flush="true">
  <tiles:putAttribute name="content" type="string">

<script language="JavaScript" type="text/JavaScript">
<!--
window.addEvent('domready', function(){			
	var tabs1 = new SimpleTabs($('tab-block-1'), { selector: 'h4'});  
   	var Tips1 = new Tips($$('.toolTipImg'));
	
});
//-->
</script>	
 
<!-- main box -->
  <div id="mboxfull">
  
  <!-- start table -->
  <table cellpadding="0" cellspacing="0" border="0" class="module" >
  <tr>
    <td class="topl_g">
	  
	  <!-- breadcrumb -->
	  <p class="breadcrumb">
	    <a href="../supplier"><fmt:message key='supplier' /></a> &gt;
	    <a href="../supplier/companyReviewList.jhtm"><fmt:message key="review" /></a>
	  </p>
	  
	  <!-- Error Message -->
	  <c:if test="${message != null}">
        <div class="message"><spring:message code="${message}"/></div>
      </c:if>
	  <spring:hasBindErrors name="productReviewForm">
          <div class="message">Please fix all errors!</div>
      </spring:hasBindErrors> 
      
  </td><td class="topr_g" ></td></tr>
  <tr><td class="boxmidlrg" >

	  
<form:form commandName="companyReviewForm" method="post" name="productReviewForm">  
 <input type="hidden" name="id" value="${companyReviewForm.productReview.id}"/>
  <input type="hidden" name="pid" value="${companyReviewForm.productReview.productId}"/>
 <!-- tabs -->
 <div id="tab-block-1">
	<h4 title="Product Info">Review</h4>
	<div>
	<!-- start tab -->

        <c:set var="classIndex" value="0" />
        
	  	<div class="listdivi ln tabdivi"></div>
	  	<div class="listdivi"></div>
	   
		  	<div class="list${classIndex % 2}"><c:set var="classIndex" value="${classIndex+1}" />
		  	<div class="listfl"><fmt:message key="active" />:</div>
		  	<div class="listp">
		  	<!-- input field -->
	   			<form:checkbox path="companyReview.active" />
		    <!-- end input field -->   	
		  	</div>
		  	</div>

		  	<div class="list${classIndex % 2}"><c:set var="classIndex" value="${classIndex+1}" />
		  	<div class="listfl"><div class="requiredField"><fmt:message key="rating" />:</div></div>
		  	<div class="listp">
		  	<!-- input field -->
	   			<form:input path="companyReview.rate" disabled="true" cssClass="textfield" size="50" maxlength="120" htmlEscape="true"/>
	   			<form:errors path="companyReview.rate" cssClass="error" />
		    <!-- end input field -->   	
		  	</div>
		  	</div>
		  	
		  	<div class="list${classIndex % 2}"><c:set var="classIndex" value="${classIndex+1}" />
		  	<div class="listfl"><div class="requiredField"><fmt:message key="title" />:</div></div>
		  	<div class="listp">
		  	<!-- input field -->
	   			<form:input path="companyReview.title" disabled="true" cssClass="textfield" size="50" maxlength="120" htmlEscape="true"/>
	   			<form:errors path="companyReview.title" cssClass="error" />
		    <!-- end input field -->   	
		  	</div>
		  	</div>
		  	
		  	<div class="list${classIndex % 2}"><c:set var="classIndex" value="${classIndex+1}" />
		  	<div class="listfl"><div class="requiredField"><fmt:message key="review" />:</div></div>
		  	<div class="listp">
		  	<!-- input field -->
	   			<form:textarea path="companyReview.text" disabled="true" cssClass="textArea300x400" htmlEscape="true"/>
	   			<form:errors path="companyReview.text" cssClass="error" />
		    <!-- end input field -->   	
		  	</div>
		  	</div>
		   
	<!-- end tab -->        
	</div> 

<!-- end tabs -->			
</div>

<!-- start button -->
<div align="left" class="button"> 
  <input type="submit" value="<fmt:message key="update" />" name="_update"/>
  <input type="submit" value="<fmt:message key="delete" />" name="_delete"/>
  <input type="submit" value="<fmt:message key="cancel" />" name="_cancel"/>
</div>
<!-- end button -->	     
</form:form>  
  <!-- end table -->
    </td><td class="boxmidr" ></td></tr>
  <tr><td class="botl"></td><td class="botr"></td></tr>
  </table>
  
<!-- end main box -->  
</div>

 </tiles:putAttribute>    
</tiles:insertDefinition>