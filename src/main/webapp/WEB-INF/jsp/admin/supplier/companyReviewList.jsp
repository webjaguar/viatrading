<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/security/tags" prefix="sec" %>


<tiles:insertDefinition name="admin.supplier.review" flush="true">
  <tiles:putAttribute name="content" type="string">
  
<sec:authorize ifAnyGranted="ROLE_AEM,ROLE_ADMIN_${prop_site_id}"> 
<c:if test="${model.count > 0}">
<script language="JavaScript">
<!--
function deleteSelected() {
	var ids = document.getElementsByName("__selected_id");	
  	if (ids.length == 1) {
      if (document.list_form.__selected_id.checked) {
    	return confirm('Delete permanently?');
      }
    } else {
      for (i = 0; i < ids.length; i++) {
        if (document.list_form.__selected_id[i].checked) {
    	  return confirm('Delete permanently?');
        }
      }
    }

    alert("Please select Company Review(s) to delete.");       
    return false;
}
function toggleAll(el) {
	var ids = document.getElementsByName("__selected_id");	
  	if (ids.length == 1)
      document.list_form.__selected_id.checked = el.checked;
    else
      for (i = 0; i < ids.length; i++)
        document.list_form.__selected_id[i].checked = el.checked;	
}  
//-->
</script>
</c:if>

<form action="companyReviewList.jhtm" method="post" name="list_form" id="list">
<input type="hidden" name="id" value="${model.companyId}"/> 
<!-- main box -->
  <div id="mboxfull">
  
  <!-- start table -->
  <table cellpadding="0" cellspacing="0" border="0" class="module" >
  <tr>
    <td class="topl_g">
	  <!-- breadcrumb -->
	  <p class="breadcrumb">
	    <a href="../supplier"><fmt:message key='supplier' /></a> &gt;
	    <fmt:message key="review" />
	  </p>
	  
	  <!-- Error -->
	  <c:if test="${!empty model.message}">
		<div class="message"><fmt:message key="${model.message}" /></div>
	  </c:if>  
	  
  </td><td class="topr_g" ></td></tr>
  <tr><td class="boxmidlrg" >

 <!-- tabs -->
<div class="tab-wrapper">
	<h4 title=""></h4>
	<div>
	<!-- start tab -->
	  	
	  	<div class="listdivi ln tabdivi"></div>
	  	<div class="listdivi"></div>
	   
		    
		  	<div class="listlight">
		  	<!-- input field -->
			<table border="0" cellpadding="0" cellspacing="1" width="100%">
			  <tr>
			  <td>
			  <c:if test="${model.count > 0}">
			  <fmt:message key="showing">
				<fmt:param value="${compnayReviewSearch.offset + 1}"/>
				<fmt:param value="${model.pageEnd}"/>
				<fmt:param value="${model.count}"/>
			  </fmt:message>
			  </c:if>
			  <c:if test="${model.categoryName != null}">
			    (Category - <c:out value="${model.categoryName}"/>)
			  </c:if>  
			  </td>
			  <td class="pageNavi">
			  Page 
			  <c:choose>
			  <c:when test="${model.pageCount <= 100 and model.pageCount > 0}">
			  <select name="page" id="page" onchange="submit()">
			  <c:forEach begin="1" end="${model.pageCount}" var="page">
			  	<option value="${page}" <c:if test="${page == (compnayReviewSearch.page)}">selected</c:if>>${page}</option>
			  </c:forEach>
			  </select>
			  </c:when>
			  <c:otherwise>
			  <input type="text" id="page" name="page" value="${compnayReviewSearch.page}" size="5" class="textfield50" />
			  <input type="submit" value="go"/>
			  </c:otherwise>
			  </c:choose>
			  of <c:out value="${model.pageCount}"/>
			  | 
			  <c:if test="${compnayReviewSearch.page == 1}"><span class="pageNaviDead"><fmt:message key="previous" /></span></c:if>
			  <c:if test="${compnayReviewSearch.page != 1}"><a href="<c:url value="companyReviewList.jhtm"><c:param name="page" value="${compnayReviewSearch.page-1}"/></c:url>" class="pageNaviLink"><fmt:message key="previous" /></a></c:if>
			  | 
			  <c:if test="${compnayReviewSearch.page == model.pageCount}"><span class="pageNaviDead"><fmt:message key="next" /></span></c:if>
			  <c:if test="${compnayReviewSearch.page != model.pageCount}"><a href="<c:url value="companyReviewList.jhtm"><c:param name="page" value="${compnayReviewSearch.page+1}"/></c:url>" class="pageNaviLink"><fmt:message key="next" /></a></c:if>
			  </td>
			  </tr>
			</table>
			
			
			<table border="0" cellpadding="0" cellspacing="0" width="100%" class="listings">
			  <tr class="listingsHdr2">
			    <td class="indexCol" align="center"><input type="checkbox" onclick="toggleAll(this)"></td>
			    <td class="indexCol">&nbsp;</td>
			    <td class="listingsHdr3"><fmt:message key="rating" /></td>
			    <td class="listingsHdr3"><fmt:message key="title" /></td>
			    <td class="listingsHdr3"><fmt:message key="review" /></td>
			    <td class="listingsHdr3"><fmt:message key="active" /></td>
			    <td class="listingsHdr3"><fmt:message key="dateAdded" /></td>
			    <td>&nbsp;</td>

			  </tr>
			<c:forEach items="${model.companyReviewList}" var="companyReview" varStatus="status">
			  <tr class="row${status.index % 2}" onmouseover="changeStyleClass(this,'rowOver')" onmouseout="changeStyleClass(this,'row${status.index % 2}')">
			    <td class="indexCol" align="center"><input name="__selected_id" value="${companyReview.id}" type="checkbox"></td>
			    <td class="indexCol"><c:out value="${status.count + compnayReviewSearch.offset}"/>.</td>
			    <td class="nameCol">
			    <c:choose>
			      <c:when test="${companyReview.rate == 0}"><img src="../graphics/star_0.gif" alt=<c:out value="${companyReview.rate}"/> title="<c:out value="${companyReview.rate}"/>" border="0" /></c:when>
			      <c:when test="${companyReview.rate >= 1 and companyReview.rate < 2}"><img src="../graphics/star_1.gif" alt=<c:out value="${companyReview.rate}"/> title="<c:out value="${companyReview.rate}"/>" border="0" /></c:when>
			      <c:when test="${companyReview.rate >= 2 and companyReview.rate < 3}"><img src="../graphics/star_2.gif" alt=<c:out value="${companyReview.rate}"/> title="<c:out value="${companyReview.rate}"/>" border="0" /></c:when>
			      <c:when test="${companyReview.rate >= 3 and companyReview.rate < 4}"><img src="../graphics/star_3.gif" alt=<c:out value="${companyReview.rate}"/> title="<c:out value="${companyReview.rate}"/>" border="0" /></c:when>
			      <c:when test="${companyReview.rate >= 4 and companyReview.rate < 5}"><img src="../graphics/star_4.gif" alt=<c:out value="${companyReview.rate}"/> title="<c:out value="${companyReview.rate}"/>" border="0" /></c:when>
			      <c:when test="${companyReview.rate == 5}"><img src="../graphics/star_5.gif" alt=<c:out value="${companyReview.rate}"/> title="<c:out value="${companyReview.rate}"/>" border="0" /></c:when>
			    </c:choose>
			    </td>
			    <td class="nameCol"><a href="<c:url value="companyReviewForm.jhtm"><c:param name="id" value="${companyReview.id}"/><c:param name="companyId" value="${companyReview.companyId}"/></c:url>"> <c:out value="${companyReview.title}"/></a></td>
			    <td class="nameCol"><c:out value="${companyReview.shortText}"/></td>
			    <td class="nameCol"><c:if test="${companyReview.active}"><img src="../graphics/checkbox.png" alt="Printed" title="Printed" border="0" /></c:if></td>
			    <td class="nameCol"><fmt:formatDate type="date" value="${companyReview.created}" pattern="M/dd/yy"/></td>
			    
			  </tr>
			</c:forEach>
			<c:if test="${model.count == 0}">
			  <tr class="emptyList"><td colspan="6">&nbsp;</td></tr>
			</c:if>
			</table>

			
			<table border="0" cellpadding="0" cellspacing="1" width="100%">
			  <tr> 
			  <td class="pageSize">
			    <select name="size" onchange="document.getElementById('page').value=1;submit()">
			    <c:forTokens items="10,25,50,100,500" delims="," var="current">
			  	  <option value="${current}" <c:if test="${current == companyReviewList.pageSize}">selected</c:if>>${current} <fmt:message key="perPage" /></option>
			    </c:forTokens>
			    </select>
			  </td>
			  </tr>
			</table>
		    <!-- end input field -->  	  
		  	
		  	<!-- start button -->
            <div align="left" class="button"> 
				<input type="submit" name="__delete" value="<fmt:message key="delete" />" onClick="return deleteSelected()">
				<input type="submit" name="__update" value="<fmt:message key="update" />" onClick="return deleteSelected()">
	  	    </div>
			<!-- end button -->	
	        </div>
	        	        	  	
	<!-- end tab -->        
	</div>
<!-- end tabs -->			
</div>
  
  <!-- end table -->
   </td><td class="boxmidr" ></td></tr>
  <tr><td class="botl"></td><td class="botr"></td></tr>
  </table>
  
<!-- end main box -->  
</div>
</form> 
</sec:authorize>

  </tiles:putAttribute>    
</tiles:insertDefinition>