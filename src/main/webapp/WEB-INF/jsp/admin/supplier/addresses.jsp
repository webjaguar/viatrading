<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/security/tags" prefix="sec" %>

<tiles:insertDefinition name="admin.supplier" flush="true">
  <tiles:putAttribute name="content" type="string"> 
  
<form action="addressList.jhtm" method="get">
<input type="hidden" name="sid" value="${model.sid}"/>
  <!-- main box -->
  <div id="mboxfull">
  
  <!-- start table -->
  <table cellpadding="0" cellspacing="0" border="0" class="module" >
  <tr>
    <td class="topl_g">
	  
	  <!-- breadcrumb -->
	  <p class="breadcrumb">
	    <a href="../supplier"><fmt:message key="supplier" /></a> &gt;
	    <a href="../supplier/supplier.jhtm?sid=${model.sid}"><c:out value="${model.company}" /></a> &gt;
	    Addresses 
	  </p>
	  
	  <!-- Error Message -->
	  
	  <!-- header image -->
	  <img class="headerImage" src="../graphics/supplier.gif" />
	  
	  </td><td class="topr_g" ></td></tr>
  <tr><td class="boxmidlrg" >

 <!-- tabs -->
 <div class="tab-wrapper">
	<h4 title=""></h4>
	<div>
	<!-- start tab -->

	  	<div class="listdivi ln tabdivi"></div>
	  	<div class="listdivi"></div>
	   
		  	<div class="listlight">
		  	<!-- input field --> 
		  	<table border="0" cellpadding="0" cellspacing="1" width="100%">
			  <tr>
			  <c:if test="${model.addresses.nrOfElements > 0}">
			  <td class="pageShowing">
			  <fmt:message key="showing">
				<fmt:param value="${model.addresses.firstElementOnPage + 1}"/>
				<fmt:param value="${model.addresses.lastElementOnPage + 1}"/>
				<fmt:param value="${model.addresses.nrOfElements}"/>
			  </fmt:message>
			  </td>
			  </c:if>
			  <td class="pageNavi">
			  Page 
			  <select name="page" onchange="submit()">
			  <c:forEach begin="1" end="${model.addresses.pageCount}" var="page">
			  	<option value="${page}" <c:if test="${page == (model.addresses.page+1)}">selected</c:if>>${page}</option>
			  </c:forEach>
			  </select>
			  of <c:out value="${model.addresses.pageCount}"/>
			  | 
			  <c:if test="${model.addresses.firstPage}"><span class="pageNaviDead">previous</span></c:if>
			  <c:if test="${not model.addresses.firstPage}"><a href="<c:url value="addressList.jhtm"><c:param name="page" value="${model.addresses.page}"/><c:param name="id" value="${model.id}"/></c:url>" class="pageNaviLink">previous</a></c:if>
			  | 
			  <c:if test="${model.addresses.lastPage}"><span class="pageNaviDead">next</span></c:if>
			  <c:if test="${not model.addresses.lastPage}"><a href="<c:url value="addressList.jhtm"><c:param name="page" value="${model.addresses.page+2}"/><c:param name="id" value="${model.id}"/></c:url>" class="pageNaviLink">next</a></c:if>
			  </td>
			  </tr>
			</table>
			
			<table border="0" cellpadding="0" cellspacing="0" width="100%" class="listings">
			  <tr class="listingsHdr2">
			    <td class="indexCol">&nbsp;</td>
			    <td>
			    	<table cellspacing="0" cellpadding="1">
			    	  <tr>
			    	    <td rowspan="2" class="listingsHdr3"><fmt:message key="addressBook" /></td>
			          </tr>
			    	</table>
			    </td>
			    <td>
			    	<table cellspacing="0" cellpadding="1">
			    	  <tr>
			    	    <td rowspan="2" class="listingsHdr3"><fmt:message key="primaryAddress" /></td>
			          </tr>
			    	</table>
			    </td>
			  </tr>
			<c:forEach items="${model.addresses.pageList}" var="address" varStatus="status">
			  <tr class="row${status.index % 2}" onmouseover="changeStyleClass(this,'rowOver')" onmouseout="changeStyleClass(this,'row${status.index % 2}')" >
			    <td class="indexCol"><c:out value="${status.count + model.addresses.firstElementOnPage}"/>.</td>
			    <td class="nameCol"><a href="<c:url value="addressForm.jhtm"><c:param name="aid" value="${address.id}"/><c:param name="sid" value="${model.sid}"/></c:url>" class="nameLink">
			     <c:out value="${address.addr1}"/> <c:out value="${address.addr2}"/>, <c:out value="${address.city}"/>, <c:out value="${address.stateProvince}"/> <c:out value="${address.zip}"/>, <c:out value="${address.country}"/></a>
			    </td>
			    <td class="nameCol">
			      <c:choose>
				    <c:when test="${address.id == model.defaultAddress.id}"><img src="../graphics/checkbox.png" alt="primary" title="primary" border="0"></c:when>
				    <c:otherwise><img src="../graphics/box.png" alt="Not Printed" title="" border="0"></c:otherwise>
				  </c:choose>
			    </td>
			  </tr>
			</c:forEach>
			<c:if test="${model.addresses.nrOfElements == 0}">
			  <tr class="emptyList"><td colspan="3">&nbsp;</td></tr>
			</c:if>
			</table>
		  	<!-- end input field -->  	  
		  	</div>
		  	
		  	<!-- start button --> 
		  	<div align="left" class="button">
		  	  
			  <input type="submit" name="__add" value="<fmt:message key="addressAdd" />">
			</div>
		  	<!-- end button -->	
		  		  	
	<!-- end tab -->        
	</div>
<!-- end tabs -->			
</div>
  
  <!-- end table -->
   </td><td class="boxmidr" ></td></tr>
  <tr><td class="botl"></td><td class="botr"></td></tr>
  </table>
  
<!-- end main box -->  
</div>
</form>  

  </tiles:putAttribute>    
</tiles:insertDefinition>