<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="/WEB-INF/tlds/spring.tld" prefix="spring" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ taglib uri="http://www.springframework.org/security/tags" prefix="sec" %>

<tiles:insertDefinition name="admin.supplier" flush="true">
  <tiles:putAttribute name="content" type="string">
  
<script type="text/javascript" src="<c:url value="/FCKeditor/fckeditor.js"/>"></script>
<script type="text/javascript"> 
window.addEvent('domready', function(){			
			var tabs1 = new SimpleTabs($('tab-block-1'), { selector: 'h4'});
			var Tips1 = new Tips($$('.toolTipImg'));
		});
</script>
<form:form commandName="addressForm" method="post">
<!-- main box -->
  <div id="mboxfull">
  
  <!-- start table -->
  <table cellpadding="0" cellspacing="0" border="0" class="module" >
  <tr>
    <td class="topl_g">
	  
	  <!-- breadcrumb -->
	  <p class="breadcrumb">
	    <a href="../supplier"><fmt:message key="supplier" /></a> &gt;
	    <a href="../supplier/supplier.jhtm?sid=${param.sid}"><c:out value="${model.company}" /></a> &gt;
	    <c:if test="${!addressForm.newAddress}"><fmt:message key="addressEdit" /></c:if><c:if test="${addressForm.newAddress}"><fmt:message key="addressAdd" /></c:if>  
	  </p>
	  
	  <!-- Error Message -->
  	  <c:if test="${!empty message}">
		  <div class="message"><spring:message code="${message}" /></div>
	  </c:if>
	  <spring:hasBindErrors name="addressForm">
       <span class="error">Please fix all errors!</span>
      </spring:hasBindErrors>

	  </td><td class="topr_g" ></td></tr>
  <tr><td class="boxmidlrg" >

 <!-- tabs -->
 <div id="tab-block-1">

	<h4 title="<fmt:message key='address' />"><fmt:message key="address" /></h4>
	<div>
	<!-- start tab -->
         <c:set var="classIndex" value="0" />
         
	  	<div class="listdivi ln tabdivi"></div>
	  	<div class="listdivi"></div>
	  	
	  	<div class="list${classIndex % 2}"><c:set var="classIndex" value="${classIndex+1}" />
		<div class="listfl"><div class="requiredField"><fmt:message key="firstName" />:</div></div>
		<div class="listp">
		<!-- input field -->
            <form:input path="address.firstName" htmlEscape="true" />
            <form:errors path="address.firstName" cssClass="error" />
		<!-- end input field -->   	
		</div>
		</div>
		
		<div class="list${classIndex % 2}"><c:set var="classIndex" value="${classIndex+1}" />
		<div class="listfl"><div class="requiredField"><fmt:message key="lastName" />:</div></div>
		<div class="listp">
		<!-- input field -->
            <form:input path="address.lastName" htmlEscape="true" />
            <form:errors path="address.lastName" cssClass="error" />
		<!-- end input field -->   	
		</div>
		</div>
		<div class="list${classIndex % 2}"><c:set var="classIndex" value="${classIndex+1}" />
		<div class="listfl"><div class="requiredField"><fmt:message key="address" /> 1:</div></div>
		<div class="listp">
		<!-- input field -->
            <form:input path="address.addr1" htmlEscape="true" />
            <form:errors path="address.addr1" cssClass="error" />
		<!-- end input field -->   	
		</div>
		</div>

	  	<div class="list${classIndex % 2}"><c:set var="classIndex" value="${classIndex+1}" />
		<div class="listfl"><fmt:message key="address" /> 2:</div>
		<div class="listp">
		<!-- input field -->
            <form:input path="address.addr2" htmlEscape="true" />
            <form:errors path="address.addr2" cssClass="error" />
		<!-- end input field -->   	
		</div>
		</div>

        <div class="list${classIndex % 2}"><c:set var="classIndex" value="${classIndex+1}" />
	    <div class="listfl"><div class="requiredField"><fmt:message key='country' />:</div></div>
		<div class="listp">
		<!-- input field -->
            <form:select id="country" path="address.country" onchange="toggleStateProvince(this)">
             <form:option value="" label="Please Select"/>
            <form:options items="${model.countries}" itemValue="code" itemLabel="name"/>
            </form:select>
            <form:errors path="address.country" cssClass="error" />
		 <!-- end input field -->   	
		 </div>
		 </div>

         <div class="list${classIndex % 2}"><c:set var="classIndex" value="${classIndex+1}" />
         <div class="listfl"><div class="requiredField"><fmt:message key="city" />:</div></div>
		 <div class="listp">
		 <!-- input field -->
             <form:input path="address.city" htmlEscape="true" />
             <form:errors path="address.city" cssClass="error" />
         <!-- end input field -->   	
		 </div>
		 </div>

         <div class="list${classIndex % 2}"><c:set var="classIndex" value="${classIndex+1}" /> 
         <div class="listfl"><div class="requiredField"><fmt:message key="stateProvince" />:</div></div>
		 <div class="listp">
		 <!-- input field -->
	      <table cellspacing="0" cellpadding="0">
	       <tr>
	       <td>
	         <form:select id="state" path="address.stateProvince">
	           <form:option value="" label="Please Select"/>
	           <form:options items="${model.states}" itemValue="code" itemLabel="name"/>
	         </form:select>
	         <form:select id="ca_province" path="address.stateProvince">
	           <form:option value="" label="Please Select"/>
	           <form:options items="${model.caProvinceList}" itemValue="code" itemLabel="name"/>
	         </form:select>
	         <form:input id="province" path="address.stateProvince" htmlEscape="true"/>
	       </td>
	       <td><div id="stateProvinceNA">&nbsp;<form:checkbox path="address.stateProvinceNA" id="addressStateProvinceNA" value="true" /> Not Applicable</div></td>
	       <td>&nbsp;</td>
	       <td>
	         <form:errors path="address.stateProvince" cssClass="error" />
	       </td>
	       </tr>
	      </table>
         <!-- end input field -->   	
		 </div>
		 </div>

         <div class="list${classIndex % 2}"><c:set var="classIndex" value="${classIndex+1}" />
         <div class="listfl"><div class="requiredField"><fmt:message key="zipCode" />:</div></div>
		 <div class="listp">
		 <!-- input field -->
             <form:input path="address.zip" htmlEscape="true" />
             <form:errors path="address.zip" cssClass="error" />
         <!-- end input field -->   	
		 </div>
		 </div>

         <div class="list${classIndex % 2}"><c:set var="classIndex" value="${classIndex+1}" />
         <div class="listfl"><div class="requiredField"><fmt:message key="phone" />:</div></div>
		 <div class="listp">
		 <!-- input field -->
             <form:input path="address.phone"  htmlEscape="true" />
             <form:errors path="address.phone" cssClass="error" />
         <!-- end input field -->   	
		 </div>
		 </div>
         
         <div class="list${classIndex % 2}"><c:set var="classIndex" value="${classIndex+1}" /> 
         <div class="listfl"><fmt:message key="cellPhone" />:</div>
		 <div class="listp">
		 <!-- input field -->
             <form:input path="address.cellPhone"  htmlEscape="true" />
             <form:errors path="address.cellPhone" cssClass="error" />
         <!-- end input field -->   	
		 </div>
		 </div>
		 
         <div class="list${classIndex % 2}"><c:set var="classIndex" value="${classIndex+1}" />
         <div class="listfl"><fmt:message key="fax" />:</div>
		 <div class="listp">
		 <!-- input field -->
              <form:input path="address.fax"  htmlEscape="true" />
              <form:errors path="address.fax" cssClass="error" />
         <!-- end input field -->   	
		 </div>
		 </div>
		 
         <div class="list${classIndex % 2}"><c:set var="classIndex" value="${classIndex+1}" />
         <div class="listfl"><div <c:if test="${siteConfig['CUSTOMERS_REQUIRED_COMPANY'].value == 'true'}">class="requiredField"</c:if>><fmt:message key="company" />:</div></div>
		 <div class="listp">
		 <!-- input field -->
              <form:input path="address.company"  htmlEscape="true" />
              <form:errors path="address.company" cssClass="error" />
         <!-- end input field -->   	
		 </div>
		 </div>
		 
		 <c:if test="${siteConfig['ADDRESS_RESIDENTIAL_COMMERCIAL'].value >= 2}">
         <div class="list${classIndex % 2}"><c:set var="classIndex" value="${classIndex+1}" />
         <div class="listfl"><fmt:message key="deliveryType" />:</div>
		 <div class="listp">
		 <!-- input field -->
              <form:radiobutton path="address.residential" value="true"/>: <fmt:message key="residential" /><br /><form:radiobutton path="address.residential" value="false"/>: <fmt:message key="commercial" />
         <!-- end input field -->   	
		 </div>
		 </div>
		 
		 <c:if test="${siteConfig['SHIPPING_LTL'].value == 'true'}">
		 <div class="list${classIndex % 2}"><c:set var="classIndex" value="${classIndex+1}" />
		 <div class="listfl"><div class="helpImg"><img class="toolTipImg" title="LTL::" src="../graphics/question.gif" /></div><fmt:message key="liftGateDelivery" />:</div>
		 <div class="listp">
		 <!-- input field -->
              <form:checkbox path="address.liftGate" value="true"/>
         <!-- end input field -->   	
		 </div>
		 </div>
		 </c:if>
		 </c:if> 
		 
		 <c:if test="${not addressForm.address.primary}">
		 <div class="list${classIndex % 2}"><c:set var="classIndex" value="${classIndex+1}" />
		 <div class="listfl"><fmt:message key="setAsPrimaryAddress" />:</div>
		 <div class="listp">
		 <!-- input field -->
		 	<form:checkbox path="setAsPrimary" value="true"/>
		 <!-- end input field -->   	
		 </div>
		 </div>	
		 </c:if>

	<!-- end tab -->        
	</div>  
	        	
<!-- end tabs -->			
</div>

<!-- start button -->
<div align="left" class="button"> 
<c:if test="${addressForm.newAddress}">
    <input type="submit" value="<spring:message code="addressAdd"/>" /> 
</c:if>
<c:if test="${!addressForm.newAddress}">
  <input type="submit" value="<spring:message code="addressUpdate"/>" />
  <c:if test="${not addressForm.address.primary}">
  <input type="submit" value="<spring:message code="addressDelete"/>" name="_delete" onClick="return confirm('Delete permanently?')" />
  </c:if>
</c:if>
  <input type="submit" value="<spring:message code="cancel"/>" name="_cancel" />
</div>
<!-- end button -->	   

  <!-- end table -->
    </td><td class="boxmidr" ></td></tr>
  <tr><td class="botl"></td><td class="botr"></td></tr>
  </table>
  
<!-- end main box -->  
</div>          	
</form:form>

<script language="JavaScript">
<!--
function toggleStateProvince(el) {
  if (el.value == "US") {
      document.getElementById('state').disabled=false;
      document.getElementById('state').style.display="block";
      document.getElementById('ca_province').disabled=true;
      document.getElementById('ca_province').style.display="none";
      document.getElementById('province').disabled=true;
      document.getElementById('province').style.display="none";
      document.getElementById('addressStateProvinceNA').disabled=true;
      document.getElementById('stateProvinceNA').style.display="none";
  } else if (el.value == "CA") {
      document.getElementById('state').disabled=true;
      document.getElementById('state').style.display="none";
      document.getElementById('ca_province').disabled=false;
      document.getElementById('ca_province').style.display="block";
      document.getElementById('province').disabled=true;
      document.getElementById('province').style.display="none";
      document.getElementById('addressStateProvinceNA').disabled=true;
      document.getElementById('stateProvinceNA').style.display="none";
  } else {
      document.getElementById('state').disabled=true;
      document.getElementById('state').style.display="none";
      document.getElementById('ca_province').disabled=true;
      document.getElementById('ca_province').style.display="none";
      document.getElementById('province').disabled=false;
      document.getElementById('province').style.display="block";
      document.getElementById('addressStateProvinceNA').disabled=false;
      document.getElementById('stateProvinceNA').style.display="block";
  }
}
toggleStateProvince(document.getElementById('country'));
//-->
</script>

  </tiles:putAttribute>
</tiles:insertDefinition>


