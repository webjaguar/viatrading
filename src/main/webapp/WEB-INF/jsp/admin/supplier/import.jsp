<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="/WEB-INF/tlds/spring.tld" prefix="spring" %>
<%@ taglib uri="http://www.springframework.org/security/tags" prefix="sec" %>

<tiles:insertDefinition name="admin.supplier" flush="true">
 <tiles:putAttribute name="content" type="string">

<script type="text/javascript" src="../../admin/javascript/multiple.open.accordion1.2.js"></script>
<script language="JavaScript" type="text/JavaScript">
</script>
<form method="post" name="uploadForm" enctype="multipart/form-data">
<!-- main box -->
  <div id="mboxfull">
  
  <!-- start table -->
  <table cellpadding="0" cellspacing="0" border="0" class="module" >
  <tr>
    <td class="topl_g">
	  
	  <!-- breadcrumb -->
	  <p class="breadcrumb">
	    <a href="../supplier">supplier</a> &gt;
	    <fmt:message key="import" />
	  </p>
	  
	  <!-- Error Message -->
	  <c:if test="${!empty message}">
  		<div class="message"><spring:message code="${message}" arguments="${arguments}"/></div>
	  </c:if>  

  
  </td><td class="topr_g" ></td></tr>
  <tr><td class="boxmidlrg" >

 <!-- tabs -->
 <div id="tab-block-1">
	<h4 title="<fmt:message key='import' />"><fmt:message key="import" /></h4>
	<div>
	<!-- start tab -->

        <c:set var="classIndex" value="0" />
        
	  	<div class="listdivi ln tabdivi"></div>
	  	<div class="listdivi"></div>
	   
		  	<div class="list${classIndex % 2}"><c:set var="classIndex" value="${classIndex+1}" />
		  	<div class="listfl"><fmt:message key="supplierImport" />:</div>
		  	<div class="listp">
		  	<!-- input field -->
	   			<c:forEach items="${invalidItems}" var="item" varStatus="status">
				<c:if test="${status.first}">
				<div class="message"><fmt:message key="importFailedTitle" /></div>
				<table border="0" class="listings" cellpadding="0" cellspacing="0" width="100%">
				<tr height="20">
				  <td class="importIndexCol importHdr">&nbsp;</td>
				  <td class="importHdr"><fmt:message key="excelLine" />#</td>
				  <td class="importHdr" align="center"><fmt:message key="supplierId" /></td>
<%-- 				  <td class="importHdr" align="center"><fmt:message key="productSku" /></td>
				  <td class="importHdr" width="100%"><fmt:message key="reason" /></td> --%>
				</tr>
				</c:if>
				<tr height="20" class="row${status.index % 2}" onmouseover="changeStyleClass(this,'rowOver')" onmouseout="changeStyleClass(this,'row${status.index % 2}')">
				  <td class="importIndexCol import"><c:out value="${status.count}"/>.</td>
				  <td class="import" align="center"><c:out value="${item['rowNum']}"/></td>
				  <td class="import" align="center"><c:out value="${item['id']}"/></td>
<%-- 				  <td class="import" align="center"><c:out value="${item['sku']}"/></td>
				  <td class="import"><c:out value="${item['reason']}"/></td> --%>
				</tr>
				<c:if test="${status.last}">
				</table>
				</c:if>
				</c:forEach>
				
				<table class="form" width="100%">
				  <tr>
				    <td class="formName"><spring:message code="excelFile" text="Excel File"/>:</td>
				    <td>
						<input value="browse" type="file" name="file"/>
				    </td>
				  </tr>
				</table>

		    <!-- end input field -->   	
		  	</div>
		  	</div>
		  	  	
	<!-- end tab -->        
	</div> 
	
<!-- end tabs -->			
</div>

<!-- start button -->
<div align="left" class="button"> 
    <input type="submit" name="__upload" value="<spring:message code="fileUpload"/>">
</div>
<!-- end button -->	     
  
  <!-- end table -->
    </td><td class="boxmidr" ></td></tr>
  <tr><td class="botl"></td><td class="botr"></td></tr>
  </table>
  
<!-- end main box -->  
 </div>
</form>

  </tiles:putAttribute>    
</tiles:insertDefinition>