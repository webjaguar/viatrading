<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>


  <div id="lbox" class="quickMode">
   <table cellpadding="0" cellspacing="0" border="0" class="module" style="width: 100%"><tr><td class="topl">
    </td><td class="topr" ></td></tr><tr><td class="boxmid" >
  
    <h2 class="menuleft mfirst"><fmt:message key="supplierMenuTitle"/></h2>
    <div class="menudiv"></div>
    <a href="supplierList.jhtm" class="userbutton"><fmt:message key="list"/></a>
    <a href="ImportSupplier.jhtm" class="userbutton">Import Supplier</a>
    <a href="ExportSupplier.jhtm" class="userbutton">Export Supplier</a>
    <c:if test="${gSiteConfig['gCOMPANY_REVIEW']}">
      <a href="companyReviewList.jhtm" class="userbutton"><fmt:message key="reviews"/></a>
    </c:if>
    <div class="menudiv"></div>
  
  <div align="left" class="searchHeader" id="searchHeaderId"><h2><fmt:message key="search" /></h2></div>
	  <table class="searchBoxLeft"  width="100%"><tr><td>
	  <form action="supplierList.jhtm" name="searchform" method="post">
	  <!-- content area --> 
	    <div class="search2">
	      <p><fmt:message key="Name" />:</p>
	      <input name="company" type="text" value="<c:out value='${supplierSearch.company}' />" size="15" />
	    </div>
	       
	    <div class="button">
	      <input type="submit" value="Search"/>
	    </div>
	  <!-- content area -->
	  </form>
	  </td></tr></table>
  
  
    <c:if test="${gSiteConfig['gCOMPANY_REVIEW']}">
	<c:if test="${param.tab == 'companyReview'}"> 
    <div class="menudiv"></div>
      <div align="left" class="searchHeader" id="searchHeaderId"><h2><fmt:message key="search" /></h2></div>
	  <table class="searchBoxLeft" width="100%"><tr><td>
	  <form action="companyReviewList.jhtm" method="post">
      <!-- content area -->
	  <div class="search2">
	      <p><fmt:message key="active" />:</p>
	      <select name="active">
	  	    <option value=""><fmt:message key="all" /></option>
	  	    <option value="0" <c:if test="${companyReviewSearch.active == '0'}">selected</c:if>><fmt:message key="inactive" /></option>
	  	    <option value="1" <c:if test="${companyReviewSearch.active == '1'}">selected</c:if>><fmt:message key="active" /></option>
	       </select>
	    </div>
      <div class="search2">
        <p><fmt:message key="supplierId" />:</p>
        <input name="id" type="text" value="<c:out value='${companyReviewSearch.companyId}' />" size="15" />
      </div>     
      <div class="button">
	      <input type="submit" value="Search"/>
      </div>
      <!-- content area -->
	  </form>
	  </td></tr></table>
	<div class="menudiv"></div>
	</c:if>
	</c:if>

    </td><td class="boxmidr" ></td></tr>
    <tr><td class="botl"></td><td class="botr"></td></tr></table>
  </div>