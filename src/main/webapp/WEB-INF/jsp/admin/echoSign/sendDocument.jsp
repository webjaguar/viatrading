<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ taglib uri="http://www.springframework.org/security/tags" prefix="sec" %>

<tiles:insertDefinition name="admin.echoSign" flush="true">
  <tiles:putAttribute name="tab"  value="documents" />
  <tiles:putAttribute name="content" type="string">
<sec:authorize ifAnyGranted="ROLE_AEM,ROLE_ADMIN_${prop_site_id},ROLE_ECHOSIGN_VIEW">
<script language="JavaScript"> 
<!--
window.addEvent('domready', function(){			
			var tabs1 = new SimpleTabs($('tab-block-1'), { selector: 'h4'});
		});

function trimString(str) {
  var returnVal = "";
  for (var i = 0; i < str.length; i++) {
	if(str.charAt(i) != ' ') {
	  returnVal += str.charAt(i);
	}
  }
  return returnVal;
}

function checkForm() {
  var el = document.getElementById("documentName");
  if (trimString(el.value) == "") {
	  el.focus();
	  return confirm('Use file name as document name?');
  } else {
    return true;
  }	   
}
//--> 
</script>

<form:form commandName="echoSign" method="post" name="uploadForm" enctype="multipart/form-data">
<!-- main box -->
  <div id="mboxfull">
  
  <!-- start table -->
  <table cellpadding="0" cellspacing="0" border="0" class="module" >
  <tr>
    <td class="topl_g">
	  
	  <!-- breadcrumb -->
	  <p class="breadcrumb">
	    <a href="../echoSign/">EchoSign</a> &gt;
	    <a href="../customers/customer.jhtm?id=${echoSign.userId}">${echoSign.recipient}</a>
	  </p>
	  
	  <!-- Error Message -->
	  <c:if test="${message != null}">
		  <div class="message"><fmt:message key="${message}"/></div>
	  </c:if>
	  <c:if test="${errorMessage != null}">
		  <div class="message"><c:out value="${errorMessage}"/></div>
	  </c:if>
  
  </td><td class="topr_g" ></td></tr>
  <tr><td class="boxmidlrg" >

 <!-- tabs -->
 <div id="tab-block-1">
	<h4 title="EchoSign">EchoSign</h4>
	<div>
	<!-- start tab -->

        <c:set var="classIndex" value="0" />
        
	  	<div class="listdivi ln tabdivi"></div>
	  	<div class="listdivi"></div>	   

		  	<div class="list${classIndex % 2}"><c:set var="classIndex" value="${classIndex+1}" />
		  	<div class="listfl"><fmt:message key="documentName" />:</div>
		  	<div class="listp">
		  	<!-- input field -->
				<form:input path="documentName" cssClass="textfield" maxlength="255" htmlEscape="true"/>
				<form:errors path="documentName" cssClass="error"/>
	 		</div>
		  	</div>	

		  	<div class="list${classIndex % 2}"><c:set var="classIndex" value="${classIndex+1}" />
		  	<div class="listfl"><fmt:message key="message" />:</div>
		  	<div class="listp">
		  	<!-- input field -->
				<form:textarea path="message" rows="15" cols="60" cssClass="textfield" htmlEscape="true"/>
				<form:errors path="message" cssClass="error"/>
	            <div id="messageSelect">
			      <select id="messageSelected" onChange="chooseMessage(this)">
			        <option value="">choose a message</option>
				  <c:forEach var="message" items="${messages}">
				    <option value="${message.messageId}"><c:out value="${message.messageName}"/></option>
				  </c:forEach>
				  </select>
				  <c:forEach var="message" items="${messages}">
					<input type="text" disabled style="display:none;" id="subject${message.messageId}" value="<c:out value="${message.subject}"/>">
	    			<textarea disabled style="display:none;" id="message${message.messageId}"><c:out value="${message.message}"/></textarea>
				  </c:forEach>				  
			    </div>
	 		</div>
		  	</div>

		  	<div class="list${classIndex % 2}"><c:set var="classIndex" value="${classIndex+1}" />
		  	<div class="listfl"><div class="helpImg"><img class="toolTipImg" title="Note::Optional parameter which sets how often you'd like the recipient(s) to be reminded to sign this document." src="../graphics/question.gif" /></div><fmt:message key="reminderFrequency"/>:</div>
		  	<div class="listp">
		  	<!-- input field -->
			  	<form:select path="reminderFrequency">
		          <form:option value=""><fmt:message key="none"/></form:option>
		          <form:option value="DAILY_UNTIL_SIGNED">daily until signed</form:option>
		          <form:option value="WEEKLY_UNTIL_SIGNED">weekly until signed</form:option>
		        </form:select>
		    <!-- end input field -->   	
		  	</div>
		  	</div>	
	   
		  	<div class="list${classIndex % 2}"><c:set var="classIndex" value="${classIndex+1}" />
		  	<div class="listfl"><fmt:message key="pdfFile"/>:</div>
		  	<div class="listp">
		  	<!-- input field -->
				<input value="browse" type="file" name="_file"/>
		    <!-- end input field -->   	
		  	</div>
		  	</div>
		  	
	<!-- end tab -->		
	</div>
<!-- end tabs -->			
</div>

<!-- start button -->
	<div align="left" class="button">
        <input type="submit" value="<fmt:message key="send"/>" name="_send" onClick="return checkForm()">
	</div>
<!-- end button -->	
    
  <!-- end table -->
    </td><td class="boxmidr" ></td></tr>
  <tr><td class="botl"></td><td class="botr"></td></tr>
  </table>
  
<!-- end main box -->  
 </div>		
</form:form>

<script language="JavaScript"> 
<!--
function chooseMessage(el) {
 if (el.value != '') {
	 document.getElementById('documentName').value = document.getElementById('subject'+el.value).value;
	 document.getElementById('message').value = document.getElementById('message'+el.value).value;
 } else {
	 document.getElementById('documentName').value = '';
	 document.getElementById('message').value = '';
 }
}	
//-->
</script>
</sec:authorize>
  </tiles:putAttribute>    
</tiles:insertDefinition>
