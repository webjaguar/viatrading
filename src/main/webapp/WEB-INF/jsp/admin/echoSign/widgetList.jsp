<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ taglib uri="http://www.springframework.org/security/tags" prefix="sec" %>

<tiles:insertDefinition name="admin.echoSign" flush="true">
  <tiles:putAttribute name="tab"  value="widgets" />
  <tiles:putAttribute name="content" type="string">    
    <sec:authorize ifAnyGranted="ROLE_AEM,ROLE_ADMIN_${prop_site_id},ROLE_ECHOSIGN_VIEW">
<form action="widgets.jhtm" id="list" method="post">
<input type="hidden" id="sort" name="sort" value="${echoSignWidgetSearch.sort}">
  <!-- main box -->
  <div id="mboxfull">
  
  <!-- start table -->
  <table cellpadding="0" cellspacing="0" border="0" class="module" >
  <tr>
    <td class="topl_g">
	  
	  <!-- breadcrumb -->
	  <p class="breadcrumb">
	    EchoSign &gt;
	    <a href="widgets.jhtm">Widgets</a>
	  </p>
	  
	  <!-- Error Message -->
	  
	  </td><td class="topr_g" ></td></tr>
  <tr><td class="boxmidlrg" >

 <!-- tabs -->
 <div class="tab-wrapper">
	<h4 title="List of affiliates"></h4>
	<div>
	<!-- start tab -->

	  	<div class="listdivi ln tabdivi"></div>
	  	<div class="listdivi"></div>
	   
		  	<div class="listlight">
		  	<!-- input field --> 
			<table border="0" cellpadding="0" cellspacing="1" width="100%">
			  <tr>
			  <td class="pageShowing">
			  <c:if test="${model.list.nrOfElements > 0}">
			  <fmt:message key="showing">
				<fmt:param value="${model.list.firstElementOnPage + 1}"/>
				<fmt:param value="${model.list.lastElementOnPage + 1}"/>
				<fmt:param value="${model.list.nrOfElements}"/>
			  </fmt:message>
			  </c:if>
			  </td>
			  <td class="pageNavi">
			  Page 
			  <select name="page" id="page" onchange="submit()">
			  <c:forEach begin="1" end="${model.list.pageCount}" var="page">
			  	<option value="${page}" <c:if test="${page == (model.list.page+1)}">selected</c:if>>${page}</option>
			  </c:forEach>
			  </select>
			  of <c:out value="${model.list.pageCount}"/>
			  | 
			  <c:if test="${model.list.firstPage}"><span class="pageNaviDead"><fmt:message key="previous" /></span></c:if>
			  <c:if test="${not model.list.firstPage}"><a href="<c:url value="widgets.jhtm"><c:param name="page" value="${model.list.page}"/></c:url>" class="pageNaviLink"><fmt:message key="previous" /></a></c:if>
			  | 
			  <c:if test="${model.list.lastPage}"><span class="pageNaviDead"><fmt:message key="next" /></span></c:if>
			  <c:if test="${not model.list.lastPage}"><a href="<c:url value="widgets.jhtm"><c:param name="page" value="${model.list.page+2}"/></c:url>" class="pageNaviLink"><fmt:message key="next" /></a></c:if>
			  </td>
			  </tr>
			</table>
			
			<table border="0" cellpadding="0" cellspacing="0" width="100%" class="listings">
			  <tr class="listingsHdr2">
			    <td class="indexCol">&nbsp;</td>
				<td class="nameCol">
			    	<table cellspacing="0" cellpadding="1">
			    	  <tr>
			    	    <c:choose>
			    	      <c:when test="${echoSignWidgetSearch.sort == 'document_name DESC'}">
			    	        <td class="listingsHdr3">
			    	        <a class="hdrLink" href="#" onClick="document.getElementById('sort').value='document_name';document.getElementById('list').submit()"><fmt:message key="documentName" /></a>
			    	        </td>
			    	        <td><img src="../graphics/up.gif" border="0"></td>
			    	      </c:when>
			    	      <c:when test="${echoSignWidgetSearch.sort == 'document_name'}">
			    	        <td class="listingsHdr3">
			    	        <a class="hdrLink" href="#" onClick="document.getElementById('sort').value='document_name DESC';document.getElementById('list').submit()"><fmt:message key="documentName" /></a>
			    	        </td>
			    	        <td><img src="../graphics/down.gif" border="0"></td>
			    	      </c:when>
			    	      <c:otherwise>
			    	        <td colspan="2" class="listingsHdr3">
			    	        <a class="hdrLink" href="#" onClick="document.getElementById('sort').value='document_name DESC';document.getElementById('list').submit()"><fmt:message key="documentName" /></a>
			    	        </td>
			    	      </c:otherwise>
			    	    </c:choose>  
			    	  </tr>
			    	  </table>
				</td>						
			    <td align="center">
			    	<table cellspacing="0" cellpadding="1">
			    	  <tr>
			    	    <c:choose>
			    	      <c:when test="${echoSignWidgetSearch.sort == 'date_created DESC'}">
			    	        <td class="listingsHdr3">
			    	        <a class="hdrLink" href="#" onClick="document.getElementById('sort').value='date_created';document.getElementById('list').submit()"><fmt:message key="dateCreated" /></a>
			    	        </td>
			    	        <td><img src="../graphics/up.gif" border="0"></td>
			    	      </c:when>
			    	      <c:when test="${echoSignWidgetSearch.sort == 'date_created'}">
			    	        <td class="listingsHdr3">
			    	        <a class="hdrLink" href="#" onClick="document.getElementById('sort').value='date_created DESC';document.getElementById('list').submit()"><fmt:message key="dateCreated" /></a>
			    	        </td>
			    	        <td><img src="../graphics/down.gif" border="0"></td>
			    	      </c:when>
			    	      <c:otherwise>
			    	        <td colspan="2" class="listingsHdr3">
			    	        <a class="hdrLink" href="#" onClick="document.getElementById('sort').value='date_created DESC';document.getElementById('list').submit()"><fmt:message key="dateCreated" /></a>
			    	        </td>
			    	      </c:otherwise>
			    	    </c:choose>  
			    	  </tr>
			    	  </table>
			    </td>
			    <td align="center" class="listingsHdr3"><fmt:message key="documents" /></td>
			    <td align="center" class="listingsHdr3">Widget</td>
			  </tr>
			<c:forEach items="${model.list.pageList}" var="widget" varStatus="status">
			  <tr class="row${status.index % 2}">
			    <td class="indexCol"><c:out value="${status.count + model.list.firstElementOnPage}"/>.</td>
			    <td class="nameCol"><c:out value="${widget.documentName}"/></td>
			    <td align="center"><fmt:formatDate type="date" timeStyle="default" value="${widget.created}"/></td>							    					    
			    <td align="center"><c:if test="${widget.numOfDocuments > 0}"><a href="list.jhtm?widget=${widget.documentKey}"><c:out value="${widget.numOfDocuments}"/></a></c:if></td>							    					    
			    <td align="center"><c:out value="${widget.documentKey}"/></td>							    					    
			  </tr>
			  <tr class="row${status.index % 2}">
			    <td class="indexCol">&nbsp;</td>
			    <td align="center" colspan="5">
				  <c:if test="${widget.url != null}"><a href="<c:out value="${widget.url}"/>" target="_blank"><c:out value="${widget.url}"/></a></c:if>
				  <c:if test="${widget.javascript != null}"><c:out value="${widget.javascript}"/></c:if>
			    </td>						    					    
			  </tr>
			  <c:if test="${widget.completionUrl != null && widget.completionUrl != ''}">
			  <tr class="row${status.index % 2}">
			    <td class="indexCol">&nbsp;</td>
			    <td align="center" colspan="5">Completion URL: <c:out value="${widget.completionUrl}"/></td>						    					    
			  </tr>	
			  </c:if>		  
			  <tr class="row${status.index % 2}">
			    <td colspan="6">&nbsp;</td>						    					    
			  </tr>	
			</c:forEach>
			<c:if test="${model.list.nrOfElements == 0}">
			  <tr class="emptyList"><td colspan="2">&nbsp;</td></tr>
			</c:if>
			</table>
				  
			<table border="0" cellpadding="0" cellspacing="1" width="100%">
			  <tr>
			  <td class="pageSize">
			  <select name="size" onchange="document.getElementById('page').value=1;submit()">
			    <c:forTokens items="10,25,50,100" delims="," var="current">
			  	  <option value="${current}" <c:if test="${current == model.list.pageSize}">selected</c:if>>${current} <fmt:message key="perPage" /></option>
			    </c:forTokens>
			  </select>
			  </td>
			  </tr>
			</table>	  
		  	<!-- end input field -->  	  
		  	</div>
		  	
		  	<!-- start button -->
            <div align="left" class="button">
			      <input type="button" value="<fmt:message key="add" /> Widget" onClick="window.location='widget.jhtm'">
	  	    </div>
			<!-- end button -->			  	
	  	
	<!-- end tab -->        
	</div>
<!-- end tabs -->			
</div>
  
  <!-- end table -->
   </td><td class="boxmidr" ></td></tr>
  <tr><td class="botl"></td><td class="botr"></td></tr>
  </table>
  
<!-- end main box -->  
</div>
</form>  
 </sec:authorize>
  </tiles:putAttribute>    
</tiles:insertDefinition>
