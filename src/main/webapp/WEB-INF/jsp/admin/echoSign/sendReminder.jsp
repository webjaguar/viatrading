<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ taglib uri="http://www.springframework.org/security/tags" prefix="sec" %>
<%@ taglib uri="/WEB-INF/tlds/custom.tld" prefix="wj" %>

<tiles:insertDefinition name="admin.echoSign" flush="true">
  <tiles:putAttribute name="tab"  value="documents" />
  <tiles:putAttribute name="content" type="string">
<sec:authorize ifAnyGranted="ROLE_AEM,ROLE_ADMIN_${prop_site_id},ROLE_ECHOSIGN_VIEW">
<script language="JavaScript"> 
<!--
window.addEvent('domready', function(){			
			var tabs1 = new SimpleTabs($('tab-block-1'), { selector: 'h4'});
		});

function trimString(str) {
  var returnVal = "";
  for (var i = 0; i < str.length; i++) {
	if(str.charAt(i) != ' ') {
	  returnVal += str.charAt(i);
	}
  }
  return returnVal;
}
//--> 
</script>

<form:form commandName="echoSign" method="post">
<!-- main box -->
  <div id="mboxfull">
  
  <!-- start table -->
  <table cellpadding="0" cellspacing="0" border="0" class="module" >
  <tr>
    <td class="topl_g">
	  
	  <!-- breadcrumb -->
	  <p class="breadcrumb">
	    <a href="../echoSign/">EchoSign</a> &gt;
	    <a href="../customers/customer.jhtm?id=${echoSign.userId}">BACK TO CUSTOMER</a>
	  </p>
	  
	  <!-- Error Message -->
	  <c:if test="${message != null}">
		  <div class="message"><fmt:message key="${message}"/></div>
	  </c:if>
	  <c:if test="${errorMessage != null}">
		  <div class="message"><c:out value="${errorMessage}"/></div>
	  </c:if>
  
  </td><td class="topr_g" ></td></tr>
  <tr><td class="boxmidlrg" >

 <!-- tabs -->
 <div id="tab-block-1">
	<h4 title="EchoSign">EchoSign</h4>
	<div>
	<!-- start tab -->

        <c:set var="classIndex" value="0" />
        
	  	<div class="listdivi ln tabdivi"></div>
	  	<div class="listdivi"></div>	   

		  	<div class="list${classIndex % 2}"><c:set var="classIndex" value="${classIndex+1}" />
		  	<div class="listfl"><fmt:message key="document" />:</div>
		  	<div class="listp">
		  		<a href="<c:out value="${echoSign.documentUrl}"/>" target="_blank"><c:out value="${echoSign.documentName}"/></a>
	 		  </div>
		  	</div>	

		  	<div class="list${classIndex % 2}"><c:set var="classIndex" value="${classIndex+1}" />
		  	<div class="listfl"><fmt:message key="status" />:</div>
		  	<div class="listp">
		  		<c:out value="${fn:replace(echoSign.agreementStatus, '_', ' ')}"/>
	 		  </div>
		  	</div>	
		  	
		  	<div class="list${classIndex % 2}"><c:set var="classIndex" value="${classIndex+1}" />
		  	<div class="listfl"><fmt:message key="history" />:</div>
		  	<div class="listp">
		  		<c:out value="${wj:newLineToBreakLine(echoSign.history)}" escapeXml="false"/>    			    
	 		  </div>
		  	</div>

		  	<div class="list${classIndex % 2}"><c:set var="classIndex" value="${classIndex+1}" />
		  	<div class="listfl"><fmt:message key="note" />:</div>
		  	<div class="listp">
		  	<!-- input field -->
				<form:textarea path="message" rows="15" cols="60" cssClass="textfield" htmlEscape="true"/>
				<form:errors path="message" cssClass="error"/>
	            <div id="messageSelect">
			      <select id="messageSelected" onChange="chooseMessage(this)">
			        <option value="">choose a message</option>
				  <c:forEach var="message" items="${messages}">
				    <option value="${message.messageId}"><c:out value="${message.messageName}"/></option>
				  </c:forEach>
				  </select>
				  <c:forEach var="message" items="${messages}">
	    			<textarea disabled style="display:none;" id="message${message.messageId}"><c:out value="${message.message}"/></textarea>
				  </c:forEach>				  
			    </div>
	 		</div>
		  	</div>
		  	
	<!-- end tab -->		
	</div>
<!-- end tabs -->			
</div>

<!-- start button -->
	<div align="left" class="button">
        <input type="submit" value="Send Reminder" name="_send">
	</div>
<!-- end button -->	
    
  <!-- end table -->
    </td><td class="boxmidr" ></td></tr>
  <tr><td class="botl"></td><td class="botr"></td></tr>
  </table>
  
<!-- end main box -->  
 </div>		
</form:form>

<script language="JavaScript"> 
<!--
function chooseMessage(el) {
 if (el.value != '') {
	 document.getElementById('message').value = document.getElementById('message'+el.value).value;
 } else {
	 document.getElementById('message').value = '';
 }
}	
//-->
</script>
</sec:authorize>
  </tiles:putAttribute>    
</tiles:insertDefinition>
