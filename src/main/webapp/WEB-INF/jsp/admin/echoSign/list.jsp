<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ taglib uri="http://www.springframework.org/security/tags" prefix="sec" %>

<tiles:insertDefinition name="admin.echoSign" flush="true">
  <tiles:putAttribute name="tab"  value="documents" />
  <tiles:putAttribute name="content" type="string">
    <sec:authorize ifAnyGranted="ROLE_AEM,ROLE_ADMIN_${prop_site_id},ROLE_ECHOSIGN_VIEW">
<form action="list.jhtm" id="list" method="post">
<input type="hidden" id="sort" name="sort" value="${echoSignSearch.sort}">
  <!-- main box -->
  <div id="mboxfull">
  
  <!-- start table -->
  <table cellpadding="0" cellspacing="0" border="0" class="module" >
  <tr>
    <td class="topl_g">
	  
	  <!-- breadcrumb -->
	  <p class="breadcrumb">
	    <a href="../echoSign/">EchoSign</a> &gt;
	    <fmt:message key="documents" />
	  </p>
	  
	  <!-- Error Message -->
	  
	  </td><td class="topr_g" ></td></tr>
  <tr><td class="boxmidlrg" >

 <!-- tabs -->
 <div class="tab-wrapper">
	<h4 title="List of affiliates"></h4>
	<div>
	<!-- start tab -->

	  	<div class="listdivi ln tabdivi"></div>
	  	<div class="listdivi"></div>
	   
		  	<div class="listlight">
		  	<!-- input field --> 
			<table border="0" cellpadding="0" cellspacing="1" width="100%">
			  <tr>
			  <td class="pageShowing">
			  <c:if test="${model.list.nrOfElements > 0}">
			  <fmt:message key="showing">
				<fmt:param value="${model.list.firstElementOnPage + 1}"/>
				<fmt:param value="${model.list.lastElementOnPage + 1}"/>
				<fmt:param value="${model.list.nrOfElements}"/>
			  </fmt:message>
			  </c:if>
			  </td>
			  <td class="pageNavi">
			  Page 
			  <select name="page" id="page" onchange="submit()">
			  <c:forEach begin="1" end="${model.list.pageCount}" var="page">
			  	<option value="${page}" <c:if test="${page == (model.list.page+1)}">selected</c:if>>${page}</option>
			  </c:forEach>
			  </select>
			  of <c:out value="${model.list.pageCount}"/>
			  | 
			  <c:if test="${model.list.firstPage}"><span class="pageNaviDead"><fmt:message key="previous" /></span></c:if>
			  <c:if test="${not model.list.firstPage}"><a href="<c:url value="list.jhtm"><c:param name="page" value="${model.list.page}"/></c:url>" class="pageNaviLink"><fmt:message key="previous" /></a></c:if>
			  | 
			  <c:if test="${model.list.lastPage}"><span class="pageNaviDead"><fmt:message key="next" /></span></c:if>
			  <c:if test="${not model.list.lastPage}"><a href="<c:url value="list.jhtm"><c:param name="page" value="${model.list.page+2}"/></c:url>" class="pageNaviLink"><fmt:message key="next" /></a></c:if>
			  </td>
			  </tr>
			</table>
			
			<table border="0" cellpadding="0" cellspacing="0" width="100%" class="listings">
			  <tr class="listingsHdr2">
			    <td class="indexCol">&nbsp;</td>
				<td class="nameCol">
			    	<table cellspacing="0" cellpadding="1">
			    	  <tr>
			    	    <c:choose>
			    	      <c:when test="${echoSignSearch.sort == 'document_name DESC'}">
			    	        <td class="listingsHdr3">
			    	        <a class="hdrLink" href="#" onClick="document.getElementById('sort').value='document_name';document.getElementById('list').submit()"><fmt:message key="documentName" /></a>
			    	        </td>
			    	        <td><img src="../graphics/up.gif" border="0"></td>
			    	      </c:when>
			    	      <c:when test="${echoSignSearch.sort == 'document_name'}">
			    	        <td class="listingsHdr3">
			    	        <a class="hdrLink" href="#" onClick="document.getElementById('sort').value='document_name DESC';document.getElementById('list').submit()"><fmt:message key="documentName" /></a>
			    	        </td>
			    	        <td><img src="../graphics/down.gif" border="0"></td>
			    	      </c:when>
			    	      <c:otherwise>
			    	        <td colspan="2" class="listingsHdr3">
			    	        <a class="hdrLink" href="#" onClick="document.getElementById('sort').value='document_name DESC';document.getElementById('list').submit()"><fmt:message key="documentName" /></a>
			    	        </td>
			    	      </c:otherwise>
			    	    </c:choose>  
			    	  </tr>
			    	  </table>
				</td>			
			    <td>
			    	<table cellspacing="0" cellpadding="1">
			    	  <tr>
			    	    <c:choose>
			    	      <c:when test="${echoSignSearch.sort == 'username DESC'}">
			    	        <td class="listingsHdr3">
			    	        <a class="hdrLink" href="#" onClick="document.getElementById('sort').value='username';document.getElementById('list').submit()"><fmt:message key="emailAddress" /></a>
			    	        </td>
			    	        <td><img src="../graphics/up.gif" border="0"></td>
			    	      </c:when>
			    	      <c:when test="${echoSignSearch.sort == 'username'}">
			    	        <td class="listingsHdr3">
			    	        <a class="hdrLink" href="#" onClick="document.getElementById('sort').value='username DESC';document.getElementById('list').submit()"><fmt:message key="emailAddress" /></a>
			    	        </td>
			    	        <td><img src="../graphics/down.gif" border="0"></td>
			    	      </c:when>
			    	      <c:otherwise>
			    	        <td colspan="2" class="listingsHdr3">
			    	        <a class="hdrLink" href="#" onClick="document.getElementById('sort').value='username DESC';document.getElementById('list').submit()"><fmt:message key="emailAddress" /></a>
			    	        </td>
			    	      </c:otherwise>
			    	    </c:choose>  
			    	  </tr>
			    	  </table>
			    </td> 				
			    <td align="center">
			    	<table cellspacing="0" cellpadding="1">
			    	  <tr>
			    	    <c:choose>
			    	      <c:when test="${echoSignSearch.sort == 'date_sent DESC'}">
			    	        <td class="listingsHdr3">
			    	        <a class="hdrLink" href="#" onClick="document.getElementById('sort').value='date_sent';document.getElementById('list').submit()"><fmt:message key="dateSent" />/<fmt:message key="created" /></a>
			    	        </td>
			    	        <td><img src="../graphics/up.gif" border="0"></td>
			    	      </c:when>
			    	      <c:when test="${echoSignSearch.sort == 'date_sent'}">
			    	        <td class="listingsHdr3">
			    	        <a class="hdrLink" href="#" onClick="document.getElementById('sort').value='date_sent DESC';document.getElementById('list').submit()"><fmt:message key="dateSent" />/<fmt:message key="created" /></a>
			    	        </td>
			    	        <td><img src="../graphics/down.gif" border="0"></td>
			    	      </c:when>
			    	      <c:otherwise>
			    	        <td colspan="2" class="listingsHdr3">
			    	        <a class="hdrLink" href="#" onClick="document.getElementById('sort').value='date_sent DESC';document.getElementById('list').submit()"><fmt:message key="dateSent" />/<fmt:message key="created" /></a>
			    	        </td>
			    	      </c:otherwise>
			    	    </c:choose>  
			    	  </tr>
			    	  </table>
			    </td>
			    <td align="center" class="listingsHdr3">Widget</td>
			    <td align="center">
			    	<table cellspacing="0" cellpadding="1">
			    	  <tr>
			    	    <c:choose>
			    	      <c:when test="${echoSignSearch.sort == 'last_update DESC'}">
			    	        <td class="listingsHdr3">
			    	        <a class="hdrLink" href="#" onClick="document.getElementById('sort').value='last_update';document.getElementById('list').submit()"><fmt:message key="lastUpdate" /></a>
			    	        </td>
			    	        <td><img src="../graphics/up.gif" border="0"></td>
			    	      </c:when>
			    	      <c:when test="${echoSignSearch.sort == 'last_update'}">
			    	        <td class="listingsHdr3">
			    	        <a class="hdrLink" href="#" onClick="document.getElementById('sort').value='last_update DESC';document.getElementById('list').submit()"><fmt:message key="lastUpdate" /></a>
			    	        </td>
			    	        <td><img src="../graphics/down.gif" border="0"></td>
			    	      </c:when>
			    	      <c:otherwise>
			    	        <td colspan="2" class="listingsHdr3">
			    	        <a class="hdrLink" href="#" onClick="document.getElementById('sort').value='last_update DESC';document.getElementById('list').submit()"><fmt:message key="lastUpdate" /></a>
			    	        </td>
			    	      </c:otherwise>
			    	    </c:choose>  
			    	  </tr>
			    	  </table>
			    </td>
				<td align="center">
			    	<table cellspacing="0" cellpadding="1">
			    	  <tr>
			    	    <c:choose>
			    	      <c:when test="${echoSignSearch.sort == 'agreement_status DESC'}">
			    	        <td class="listingsHdr3">
			    	        <a class="hdrLink" href="#" onClick="document.getElementById('sort').value='agreement_status';document.getElementById('list').submit()"><fmt:message key="status" /></a>
			    	        </td>
			    	        <td><img src="../graphics/up.gif" border="0"></td>
			    	      </c:when>
			    	      <c:when test="${echoSignSearch.sort == 'agreement_status'}">
			    	        <td class="listingsHdr3">
			    	        <a class="hdrLink" href="#" onClick="document.getElementById('sort').value='agreement_status DESC';document.getElementById('list').submit()"><fmt:message key="status" /></a>
			    	        </td>
			    	        <td><img src="../graphics/down.gif" border="0"></td>
			    	      </c:when>
			    	      <c:otherwise>
			    	        <td colspan="2" class="listingsHdr3">
			    	        <a class="hdrLink" href="#" onClick="document.getElementById('sort').value='agreement_status DESC';document.getElementById('list').submit()"><fmt:message key="status" /></a>
			    	        </td>
			    	      </c:otherwise>
			    	    </c:choose>  
			    	  </tr>
			    	  </table>
				</td>
			    <td align="center" class="listingsHdr3"><fmt:message key="action" /></td>
			  </tr>
			<c:forEach items="${model.list.pageList}" var="echoSign" varStatus="status">
			  <tr class="row${status.index % 2}" onmouseover="changeStyleClass(this,'rowOver')" onmouseout="changeStyleClass(this,'row${status.index % 2}')">
			    <td class="indexCol"><c:out value="${status.count + model.list.firstElementOnPage}"/>.</td>
			    <c:choose>
			    <c:when test="${echoSign.documentUrl != null}">
			    <td class="nameCol"><a href="<c:out value="${echoSign.documentUrl}"/>" target="_blank"><c:out value="${echoSign.documentName}"/></a></td>
			    </c:when>
			    <c:otherwise>
			    <td class="nameCol"><c:out value="${echoSign.documentName}"/></td>
			    </c:otherwise>
			    </c:choose>
			    <td class="nameCol"><a href="../customers/customer.jhtm?id=${echoSign.userId}"><c:out value="${echoSign.customer.username}"/></a>
			    <c:if test="${echoSign.agreementStatus == null or echoSign.agreementStatus == 'OUT_FOR_SIGNATURE'}">
			    <span class="sup">*<c:set var="hasUpdates" value="true"/></span>
			    </c:if>
			    </td>
			    <td align="center"><fmt:formatDate type="date" timeStyle="default" value="${echoSign.dateSent}"/></td>	
			    <td align="center"><a href="<c:out value="widgets.jhtm?widget=${echoSign.widgetDocumentKey}"/>"><c:out value="${echoSign.widgetDocumentKey}"/></a></td>	
			    <td align="center"><fmt:formatDate type="date" timeStyle="default" value="${echoSign.lastUpdate}"/></td>	
			    <td align="center" style="<c:if test="${echoSign.agreementStatus == 'SIGNED'}">color:red</c:if>">
			      <c:out value="${fn:replace(echoSign.agreementStatus, '_', ' ')}"/>
				</td>		
			    <td align="center">
			      <c:if test="${echoSign.agreementStatus == 'OUT_FOR_SIGNATURE'}">
			        <a href="../echoSign/sendReminder.jhtm?key=<c:out value="${echoSign.documentKey}"/>">remind</a> |  
			        <a href="../echoSign/cancelDocument.jhtm?key=<c:out value="${echoSign.documentKey}"/>">cancel</a>
			      </c:if>			    
				</td>					    
			  </tr>
			</c:forEach>
			<c:if test="${model.list.nrOfElements == 0}">
			  <tr class="emptyList"><td colspan="2">&nbsp;</td></tr>
			</c:if>
			</table>
				  
			<table border="0" cellpadding="0" cellspacing="1" width="100%">
			  <tr>
			  <td class="pageSize">
			  <select name="size" onchange="document.getElementById('page').value=1;submit()">
			    <c:forTokens items="10,25,50,100" delims="," var="current">
			  	  <option value="${current}" <c:if test="${current == model.list.pageSize}">selected</c:if>>${current} <fmt:message key="perPage" /></option>
			    </c:forTokens>
			  </select>
			  </td>
			  </tr>
			</table>	  
		  	<!-- end input field -->  	  
		  	</div>
		  	
			<c:if test="${hasUpdates}">
				<div class="sup">
					* Click to get latest status
				</div>
			</c:if>		  	
		  	
	  	
	<!-- end tab -->        
	</div>
<!-- end tabs -->			
</div>
  
  <!-- end table -->
   </td><td class="boxmidr" ></td></tr>
  <tr><td class="botl"></td><td class="botr"></td></tr>
  </table>
  
<!-- end main box -->  
</div>
</form>  
 </sec:authorize>
  </tiles:putAttribute>    
</tiles:insertDefinition>
