<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ taglib uri="http://www.springframework.org/security/tags" prefix="sec" %>

<tiles:insertDefinition name="admin.echoSign" flush="true">
  <tiles:putAttribute name="tab"  value="widgets" />
  <tiles:putAttribute name="content" type="string">
<sec:authorize ifAnyGranted="ROLE_AEM,ROLE_ADMIN_${prop_site_id},ROLE_ECHOSIGN_VIEW">
<script language="JavaScript"> 
<!--
window.addEvent('domready', function(){			
			var tabs1 = new SimpleTabs($('tab-block-1'), { selector: 'h4'});
	    	var Tips1 = new Tips($$('.toolTipImg'));			
		});

function trimString(str) {
  var returnVal = "";
  for (var i = 0; i < str.length; i++) {
	if(str.charAt(i) != ' ') {
	  returnVal += str.charAt(i);
	}
  }
  return returnVal;
}

function checkForm() {
  var el = document.getElementById("documentName");
  if (trimString(el.value) == "") {
	  el.focus();
	  return confirm('Use file name as document name?');
  } else {
    return true;
  }	   
}
//--> 
</script>

<form:form commandName="echoSign" method="post" name="uploadForm" enctype="multipart/form-data">
<!-- main box -->
  <div id="mboxfull">
  
  <!-- start table -->
  <table cellpadding="0" cellspacing="0" border="0" class="module" >
  <tr>
    <td class="topl_g">
	  
	  <!-- breadcrumb -->
	  <p class="breadcrumb">
	    EchoSign &gt;
	    <a href="widgets.jhtm">Widgets</a> &gt;
	    Widget
	  </p>
	  
	  <!-- Error Message -->
	  <c:if test="${message != null}">
		  <div class="message"><fmt:message key="${message}"/></div>
	  </c:if>
	  <c:if test="${errorMessage != null}">
		  <div class="message"><c:out value="${errorMessage}"/></div>
	  </c:if>
  
  </td><td class="topr_g" ></td></tr>
  <tr><td class="boxmidlrg" >

 <!-- tabs -->
 <div id="tab-block-1">
	<h4 title="EchoSign">EchoSign</h4>
	<div>
	<!-- start tab -->

        <c:set var="classIndex" value="0" />
        
	  	<div class="listdivi ln tabdivi"></div>
	  	<div class="listdivi"></div>	   

		  	<div class="list${classIndex % 2}"><c:set var="classIndex" value="${classIndex+1}" />
		  	<div class="listfl">Widget <fmt:message key="type" />:</div>
		  	<div class="listp">
			 <form:select path="embedded" cssStyle="width:100px;">
			   <form:option value="false">URL</form:option>
			   <form:option value="true">Javascript</form:option>
			 </form:select>
	 		</div>
		  	</div>	

		  	<div class="list${classIndex % 2}"><c:set var="classIndex" value="${classIndex+1}" />
		  	<div class="listfl"><fmt:message key="documentName" />:</div>
		  	<div class="listp">
		  	<!-- input field -->
				<form:input path="documentName" cssClass="textfield" maxlength="255" htmlEscape="true"/>
				<form:errors path="documentName" cssClass="error"/>
	 		</div>
		  	</div>	

		  	<div class="list${classIndex % 2}"><c:set var="classIndex" value="${classIndex+1}" />
		  	<div class="listfl"><div class="helpImg"><img class="toolTipImg" title="NOTE::The url to which the user will be sent after successfully completing the widget. If left blank then page will stay on echosign." src="../graphics/question.gif" /></div>Completion URL:</div>
		  	<div class="listp">
		  	<!-- input field -->
				<form:input path="completionUrl" cssClass="textfield" maxlength="255" htmlEscape="true"/>
				<form:errors path="completionUrl" cssClass="error"/>
	 		</div>
		  	</div>	
	   
		  	<div class="list${classIndex % 2}"><c:set var="classIndex" value="${classIndex+1}" />
		  	<div class="listfl"><fmt:message key="pdfFile"/>:</div>
		  	<div class="listp">
		  	<!-- input field -->
				<input value="browse" type="file" name="_file"/>
		    <!-- end input field -->   	
		  	</div>
		  	</div>
		  	
	<!-- end tab -->		
	</div>
<!-- end tabs -->			
</div>

<!-- start button -->
	<div align="left" class="button">
        <input type="submit" value="<fmt:message key="create"/> Widget" name="_send" onClick="return checkForm()">
	</div>
<!-- end button -->	
    
  <!-- end table -->
    </td><td class="boxmidr" ></td></tr>
  <tr><td class="botl"></td><td class="botr"></td></tr>
  </table>
  
<!-- end main box -->  
 </div>		
</form:form>
</sec:authorize>
  </tiles:putAttribute>    
</tiles:insertDefinition>
