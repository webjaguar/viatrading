<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<link rel="stylesheet" type="text/css" media="all" href="../../admin/javascript/jscalendar-1.0/skins/aqua/theme.css" title="Aqua" /> <!-- calendar stylesheet -->
<script type="text/javascript" src="../../admin/javascript/jscalendar-1.0/calendar.js"></script> <!-- main calendar program -->
<script type="text/javascript" src="../../admin/javascript/jscalendar-1.0/lang/calendar-en.js"></script>
<script type="text/javascript" src="../../admin/javascript/jscalendar-1.0/calendar-setup.js"></script>

  <div id="lbox">
   <table cellpadding="0" cellspacing="0" border="0" class="module" style="width: 100%"><tr><td class="topl">
    </td><td class="topr" ></td></tr><tr><td class="boxmid" >
    
    <h2 class="menuleft mfirst">EchoSign</h2>
    <div class="menudiv"></div>
    <a href="list.jhtm" class="userbutton"><fmt:message key="documents"/></a>
    <a href="widgets.jhtm" class="userbutton">Widgets</a>
    <div class="menudiv"></div>    
    
    <h2 class="menuleft"><fmt:message key="sent"/></h2>    
    <div class="menudiv"></div>    
    <c:set var="totalSent" value="0" />
    <table width="100%" border="0" cellspacing="2" cellpadding="2" style="background-color:#EFF7FF;border:1px solid #D9E6F7;color:#314973;">
    <c:forTokens items="Jan,Feb,Mar,Apr,May,Jun,Jul,Aug,Sep,Oct,Nov,Dec" delims="," var="month" varStatus="status">
      <tr>
        <td width="99%"><div style="margin-left:5px;"><c:out value="${month}"/></div></td>
        <td width="*" align="right" <c:if test="${status.last}">style="border-bottom: #000000 1px solid"</c:if>>
          <div style="margin-right:5px;">
        	<c:if test="${echoSignSentMap[status.count] != null}">
        	  <fmt:formatNumber value="${echoSignSentMap[status.count]}" pattern="#,##0"/>
        	  <c:set var="totalSent" value="${totalSent + echoSignSentMap[status.count]}" />
        	</c:if>
        	<c:if test="${echoSignSentMap[status.count] == null}">-</c:if>
         </div>
        </td>
      </tr>
    </c:forTokens>
      <tr>
    	<td>&nbsp;</td>
    	<td align="right"><div style="margin-right:5px;"><fmt:formatNumber value="${totalSent}" pattern="#,##0"/></div></td>
      </tr>
	</table>
    <div class="menudiv"></div>
    
    <c:if test="${tab == 'documents'}">
    <div class="menudiv"></div>
      <div align="left" class="searchHeader" id="searchHeaderId"><h2><fmt:message key="search" /></h2></div>
	  <table class="searchBoxLeft"  width="100%"><tr><td>
	  <form action="list.jhtm" name="searchform" method="post">
	  <!-- content area -->     
	    <div class="search2">
	      <p><fmt:message key="status" />:</p>
	      <select name="status">
	  	    <option value=""><fmt:message key="all" /></option>
	  	    <option value="SIGNED" <c:if test="${echoSignSearch.agreementStatus == 'SIGNED'}">selected</c:if>>SIGNED</option>
	  	    <option value="OUT_FOR_SIGNATURE" <c:if test="${echoSignSearch.agreementStatus == 'OUT_FOR_SIGNATURE'}">selected</c:if>>OUT FOR SIGNATURE</option>
	  	    <option value="ABORTED" <c:if test="${echoSignSearch.agreementStatus == 'ABORTED'}">selected</c:if>>ABORTED</option>
	       </select>
	    </div>
	    <div class="search2">
	      <p><fmt:message key="documentName" />:</p>
          <input name="documentName" type="text" value="<c:out value='${echoSignSearch.documentName}' />" size="15" />
	    </div>
	    <div class="search2">
	      <p><fmt:message key="email" />:</p>
          <input name="username" type="text" value="<c:out value='${echoSignSearch.username}' />" size="15" />
	    </div>
	    <div class="search2">
	      <p>start:</p>
	      <input style="width:90px;margin-right:5px;" name="startDate" id="startDate" type="text" value="<fmt:formatDate type="date" pattern="MM/dd/yyyy" value='${echoSignSearch.startDate}'/>" readonly size="11" />
		  <img class="calendarImage"  id="startDate_trigger" src="../graphics/calendarIcon.jpg" />
		  <script type="text/javascript">
    		Calendar.setup({
        			inputField     :    "startDate",   // id of the input field
        			showsTime      :    false,
        			ifFormat       :    "%m/%d/%Y",   // format of the input field
        			button         :    "startDate_trigger"   // trigger for the calendar (button ID)
    		});	
		  </script>	    
	    </div>
	    <div class="search2">
	      <p>end:</p>
	      <input style="width:90px;margin-right:5px;" name="endDate" id="endDate" type="text" value="<fmt:formatDate type="date" pattern="MM/dd/yyyy" value='${echoSignSearch.endDate}'/>" readonly size="11" />
		  <img class="calendarImage"  id="endDate_trigger" src="../graphics/calendarIcon.jpg" />
		  <script type="text/javascript">
    		Calendar.setup({
        			inputField     :    "endDate",   // id of the input field
        			showsTime      :    false,
        			ifFormat       :    "%m/%d/%Y",   // format of the input field
        			button         :    "endDate_trigger"   // trigger for the calendar (button ID)
    		});	
		  </script>	    
	    </div>
	    <div class="search2">
	      <p>Widget:</p>
          <input name="widget" type="text" value="<c:out value='${echoSignSearch.widget}' />" size="15" />
	    </div>
	    <div class="button">
	      <input type="submit" value="Search"/>
	    </div>
	  <!-- content area -->
	  </form>
	  </td></tr></table>
	<div class="menudiv"></div> 
	</c:if> 
    
    <c:if test="${tab == 'widgets'}">
    <div class="menudiv"></div>
      <div align="left" class="searchHeader" id="searchHeaderId"><h2><fmt:message key="search" /></h2></div>
	  <table class="searchBoxLeft"  width="100%"><tr><td>
	  <form action="widgets.jhtm" name="searchform" method="post">
	  <!-- content area -->
	    <div class="search2">
	      <p>Widget:</p>
          <input name="widget" type="text" value="<c:out value='${echoSignWidgetSearch.widget}' />" size="15" />
	    </div>
	    <div class="button">
	      <input type="submit" value="Search"/>
	    </div>
	  <!-- content area -->
	  </form>
	  </td></tr></table>
	<div class="menudiv"></div> 
	</c:if>     
    
    </td><td class="boxmidr" ></td></tr>
    <tr><td class="botl"></td><td class="botr"></td></tr></table>
  </div>