<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/security/tags" prefix="sec" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>

<tiles:insertDefinition name="admin.dataFeed" flush="true">
  <tiles:putAttribute name="content" type="string">
  
<sec:authorize ifAnyGranted="ROLE_AEM,ROLE_ADMIN_${prop_site_id},ROLE_DATA_FEED">  
<script language="JavaScript" type="text/JavaScript">
<!--
window.addEvent('domready', function(){			
			var tabs1 = new SimpleTabs($('tab-block-1'), { selector: 'h4'});
			var Tips1 = new Tips($$('.toolTipImg'));
		});
//-->
</script>	
<form action="dataFeed.jhtm?feed=${param.feed}" method="post">
<!-- main box -->
  <div id="mboxfull">
  
  <!-- start table -->
  <table cellpadding="0" cellspacing="0" border="0" class="module" >
  <tr>
    <td class="topl_g">
	  
	  <!-- breadcrumb -->
	  <p class="breadcrumb">
	    <a href="../dataFeed.jhtm">Data Feed</a> &gt;
	    ${param.feed} 
	  </p>
	  
	  <!-- Error Message -->
	  <c:if test="${!empty message1}">
		<div class="message"><fmt:message key="${message1}"/></div>
	  </c:if>
	  <c:if test="${!empty message2}">
		<div class="message"><fmt:message key="${message2}"/></div>
	  </c:if>
	  <c:if test="${!empty ftpMessage}">
		<div class="message"><c:out value="${ftpMessage}" escapeXml="false"/></div>
      </c:if>
      
      <!-- header image -->
	  <img class="headerImage" src="../graphics/dataFeed.gif" />

  
  </td><td class="topr_g" ></td></tr>
  <tr><td class="boxmidlrg" >
 <!-- tabs -->
 <div id="tab-block-1">
	<h4 title="<fmt:message key='${param.feed}' /> <fmt:message key='dataFeed' />"><fmt:message key="${param.feed}" /> <fmt:message key="dataFeed" /></h4>
	<div>
	<!-- start tab -->
	<p>
        <c:set var="classIndex" value="0" />
        
	  	<div class="listdivi ln tabdivi"></div>
	  	<div class="listdivi"></div>
	   
		  	<div class="list${classIndex % 2}"><c:set var="classIndex" value="${classIndex+1}" />
		  	<div class="listfl">&nbsp;</div>
		  	<div class="listp">
		  	<!-- input field -->
	   			<a href="../feed/${generatedFile['file'].name}"><c:out value="${generatedFile['file'].name}"/></a> 
    	        <c:if test="${generatedFile['lastModified'] != null}">(<fmt:formatDate type="both" timeStyle="full" value="${generatedFile['lastModified']}"/>)</c:if>
		    <!-- end input field -->   	
		  	</div>
		  	</div>
		  	
		  	<div class="list${classIndex % 2}"><c:set var="classIndex" value="${classIndex+1}" />
		  	<div class="listfl"></div>
		  	<div class="listp">
		  	<!-- input field -->
		  	    <div align="right" class="button"> 
	   			<input type="submit" name="__new" value="<fmt:message key="dataFeedGenerate" />">
  				<input type="submit" name="__ftp" value="<fmt:message key="dataFeedFtp" />">
  				</div>		    
  			<!-- end input field -->   	
		  	</div>
		  	</div>
		  	
	<!-- end tab -->        
	</div>         

	<h4 title="<fmt:message key='${param.feed}' /> <fmt:message key='configuration' />"><fmt:message key="${param.feed}" /> <fmt:message key="configuration" /></h4>
	<div>
	<!-- start tab -->
         <c:set var="classIndex" value="0" />
         
	  	<div class="listdivi ln tabdivi"></div>
	  	<div class="listdivi"></div>
	  	
	  	<div class="list${classIndex % 2}"><c:set var="classIndex" value="${classIndex+1}" />
	  	<div class="listfl">FTP <fmt:message key="server" />:</div>
	  	<div class="listp">
	  	<!-- input field -->
            <input type="text" name="server" value="<c:out value="${dataFeedConfig['server']}"/>" maxlength="30" size="30">
	    <!-- end input field -->   	
	  	</div>
	  	</div>
	  	
	  	<div class="list${classIndex % 2}"><c:set var="classIndex" value="${classIndex+1}" />
	  	<div class="listfl">FTP <fmt:message key="username" />:</div>
	  	<div class="listp">
	  	<!-- input field -->
            <input type="text" name="username" value="<c:out value="${dataFeedConfig['username']}"/>" maxlength="30" size="30">
	    <!-- end input field -->   	
	  	</div>
	  	</div>
	  	
	  	<div class="list${classIndex % 2}"><c:set var="classIndex" value="${classIndex+1}" />
	  	<div class="listfl">FTP <fmt:message key="password" />:</div>
	  	<div class="listp">
	  	<!-- input field -->
            <input type="text" name="password" value="<c:out value="${dataFeedConfig['password']}"/>" maxlength="30" size="30">
	    <!-- end input field -->   	
	  	</div>
	  	</div>
	  	
	  	<div class="list${classIndex % 2}"><c:set var="classIndex" value="${classIndex+1}" />
	  	<div class="listfl">FTP <fmt:message key="fileName" />:</div>
	  	<div class="listp">
	  	<!-- input field -->
            <input type="text" name="filename" value="<c:out value="${dataFeedConfig['filename']}"/>" maxlength="30" size="30">
	    <!-- end input field -->   	
	  	</div>
	  	</div>	  	
	  	
	  	<hr />

	  	<div class="list${classIndex % 2}"><c:set var="classIndex" value="${classIndex+1}" />
	  	<div class="listp">
	  	<!-- input field -->
            <table cellspacing="5" class="form">
		  <tr>
		    <td width="50">&nbsp;</td>
		    <td align="center" width="50"><fmt:message key="Enabled" /></td>
		    <td><fmt:message key="fieldName" /></td>
		    <td><fmt:message key="product" /></td>
		    <td><fmt:message key="defaultValue" /></td>
		  </tr>
		  <c:forEach items="${dataFeedList}" var="dataFeed">
		  <tr>
		    <td>&nbsp;</td>
		    <td align="center"><input name="__enabled_${dataFeed.fieldName}" type="checkbox" <c:if test="${dataFeed.enabled or dataFeed.required}">checked</c:if> <c:if test="${dataFeed.required}">disabled</c:if>></td>
		    <td><c:out value="${dataFeed.fieldName}"/><img class="toolTipImg" title="<c:out value="${dataFeed.fieldName}"/>::<c:out value="${dataFeed.description}"/>" src="../graphics/question.gif" /></td>
		    <td>
		      <c:choose>
		       <c:when test="${fn:toUpperCase(dataFeed.fieldName) == 'NAME' or dataFeed.fieldName == 'Product Name' or dataFeed.fieldName == 'TitleDescription'}">
			    <select name="__methodName_${dataFeed.fieldName}">
		  	      <option value="getName"><fmt:message key="productName" /></option>
				  <c:forEach items="${productFields}" var="productField">
				  <c:if test="${productField.enabled}">
		  	      <option value="getField${productField.id}"
		  	      		<c:if test="${productField.methodName == dataFeed.methodName}">selected</c:if>>Field <c:out value="${productField.id}"/>: ${productField.name}</option>
		  	      </c:if>
				  </c:forEach>
				</select>
			   </c:when>
		       <c:when test="${fn:toUpperCase(dataFeed.fieldName) == 'DESCRIPTION' or dataFeed.fieldName == 'Detailed Description' or dataFeed.fieldName == 'LineDescription' or dataFeed.fieldName == 'DetailDescription'}">
			    <select name="__methodName_${dataFeed.fieldName}">
		  	      <option value="getName"><fmt:message key="productName" /></option>			    
		  	      <option value="getShortDesc" <c:if test="${'getShortDesc' == dataFeed.methodName}">selected</c:if>><fmt:message key="productShortDesc" /></option>
		  	      <option value="getLongDesc" <c:if test="${'getLongDesc' == dataFeed.methodName}">selected</c:if>><fmt:message key="productLongDesc" /></option>
				  <c:forEach items="${productFields}" var="productField">
				  <c:if test="${productField.enabled}">
		  	      <option value="getField${productField.id}"
		  	      		<c:if test="${productField.methodName == dataFeed.methodName}">selected</c:if>>Field <c:out value="${productField.id}"/>: ${productField.name}</option>
		  	      </c:if>
				  </c:forEach>
				</select>
			   </c:when>
			   <c:when test="${fn:toUpperCase(dataFeed.fieldName) == 'AVAILABILITY'}">
			    <select name="__methodName_${dataFeed.fieldName}">
			      <option value=""></option>
		  	      <option value="getAvailability" <c:if test="${dataFeed.methodName == 'getAvailability'}">selected</c:if>>DYNAMIC: Based On System's Inventory Settings.</option>			    
		  	      <c:forEach items="${productFields}" var="productField">
				  <c:if test="${productField.enabled}">
		  	      <option value="getField${productField.id}"
		  	      		<c:if test="${productField.methodName == dataFeed.methodName}">selected</c:if>>Field <c:out value="${productField.id}"/>: ${productField.name}</option>
		  	      </c:if>
				  </c:forEach>
				</select>
			   </c:when>
		       <c:when test="${dataFeed.fieldName == 'Keywords'}">
			    <select name="__methodName_${dataFeed.fieldName}">
		  	      <option value="getKeywords"><fmt:message key="keywords" /></option>
				  <c:forEach items="${productFields}" var="productField">
				  <c:if test="${productField.enabled}">
		  	      <option value="getField${productField.id}"
		  	      		<c:if test="${productField.methodName == dataFeed.methodName}">selected</c:if>>Field <c:out value="${productField.id}"/>: ${productField.name}</option>
		  	      </c:if>
				  </c:forEach>
				</select>
			   </c:when>
		       <c:when test="${fn:contains(dataFeed.methodName,'getField') or dataFeed.methodName == '' or dataFeed.fieldName == 'ManufacturerPartsNumber'}">
			    <select name="__methodName_${dataFeed.fieldName}">
		  	      <option value=""></option>
		  	      <c:if test="${dataFeed.fieldName == 'ManufacturerPartsNumber'}">
		  	      <option value="getSku" <c:if test="${dataFeed.methodName == 'getSku'}">selected</c:if>><fmt:message key="productSku" /></option>
		  	      </c:if>
				  <c:forEach items="${productFields}" var="productField">
				  <c:if test="${productField.enabled}">
		  	      <option value="getField${productField.id}"
		  	      		<c:if test="${productField.methodName == dataFeed.methodName}">selected</c:if>>Field <c:out value="${productField.id}"/>: ${productField.name}</option>
		  	      </c:if>
				  </c:forEach>
				</select>
			   </c:when>
			   <c:otherwise>
			    <c:choose>
			      <c:when test="${dataFeed.methodName == 'getSku'}"><fmt:message key="productSku" /></c:when>
			      <c:when test="${dataFeed.methodName == 'getShortDesc'}"><fmt:message key="productShortDesc" /></c:when>
			      <c:when test="${dataFeed.methodName == 'getName'}"><fmt:message key="productName" /></c:when>
			      <c:when test="${dataFeed.methodName == 'getPrice1'}"><fmt:message key="productPrice" /><c:if test="${gSiteConfig['gPRICE_TIERS'] > 1}"> 1</c:if></c:when>
			      <c:when test="${dataFeed.methodName == 'getPrice2'}"><fmt:message key="productPrice" /> 2</c:when>
			      <c:when test="${dataFeed.methodName == 'getPrice3'}"><fmt:message key="productPrice" /> 3</c:when>
			      <c:when test="${dataFeed.methodName == 'getPrice4'}"><fmt:message key="productPrice" /> 4</c:when>
			      <c:when test="${dataFeed.methodName == 'getQtyBreak1'}">QtyBreak 1</c:when>
			      <c:when test="${dataFeed.methodName == 'getQtyBreak2'}">QtyBreak 2</c:when>			      
			      <c:when test="${dataFeed.methodName == 'getQtyBreak3'}">QtyBreak 3</c:when>
			      <c:when test="${dataFeed.methodName == 'getPriceTable5'}">PriceTable 5</c:when>
			      <c:when test="${dataFeed.methodName == 'getWeight'}"><fmt:message key="productWeight" /></c:when>
			      <c:when test="${dataFeed.methodName == 'isTaxable'}"><fmt:message key="taxable" /></c:when>
			      <c:when test="${dataFeed.methodName == 'url'}">
			      	<c:out value="${siteConfig['SITE_URL'].value}"/>product.jhtm?id=__&trackcode=<c:out value="${param.feed}"/>
			      </c:when>
			      <c:when test="${dataFeed.methodName == 'detailsbig' or dataFeed.methodName == 'thumb' or dataFeed.methodName == 'nontaxable'}">
			      	DYNAMIC
			      </c:when>
			      <c:when test="${dataFeed.methodName == 'getInventory'}"><fmt:message key="inventory" /></c:when>			      
			      <c:when test="${dataFeed.methodName == 'getLongDesc'}"><fmt:message key="productLongDesc" /></c:when>
				  <c:when test="${dataFeed.methodName == 'getBreadCrumb'}">
			      	DYNAMIC
			      </c:when>
			      <c:when test="${dataFeed.methodName == 'getMsrp'}"><fmt:message key="productMsrp" /></c:when>
			      <c:when test="${dataFeed.methodName == 'getVarient'}">#20*PRICE</c:when>
				 </c:choose>
				 <input type="hidden" name="__methodName_${dataFeed.fieldName}" value="${dataFeed.methodName}">  
			   </c:otherwise>
			  </c:choose>
		    </td>
		    <td>
			  <input type="text" name="__defaultValue_${dataFeed.fieldName}" value="<c:out value="${dataFeed.defaultValue}"/>" maxlength="255" size="10">
		    </td>
		  </tr>
		  </c:forEach>
		</table>
	    <!-- end input field -->   	
	  	</div>
	  	</div>
	
	<!-- end tab --> 
	</div>  

	<h4 title="<fmt:message key='${param.feed}' /> <fmt:message key='filter' />"><fmt:message key="${param.feed}" /> <fmt:message key="filter" /></h4>
	<div>
	<!-- start tab -->
         <c:set var="classIndex" value="0" />
         
	  	<div class="listdivi ln tabdivi"></div>
	  	<div class="listdivi"></div>
	  	
	  	<div class="list${classIndex % 2}"><c:set var="classIndex" value="${classIndex+1}" />
	  	<div class="listfl"><div class="helpImg"><img class="toolTipImg" title="Note::Leave this field blank to get all products. Get all products that price1 is <b>grater than or equal</b> this value." src="../graphics/question.gif" /></div><fmt:message key="price" />:</div>
	  	<div class="listp">
	  	<!-- input field -->
            <input type="text" name="price" value="<c:out value="${search.price}"/>" maxlength="30" size="30">
	    <!-- end input field -->   	
	  	</div>
	  	</div>
	  	
	  	<div class="list${classIndex % 2}"><c:set var="classIndex" value="${classIndex+1}" />
	  	<div class="listfl"><div class="helpImg"><img class="toolTipImg" title="Note::Leave this field blank to get all products. Get all products that inventory is <b>grater than or equal</b> this value." src="../graphics/question.gif" /></div><fmt:message key="inventory" />:</div>
	  	<div class="listp">
	  	<!-- input field -->
            <input type="text" name="inventory" value="<c:out value="${search.inventory}"/>" maxlength="30" size="30">
	    <!-- end input field -->   	
	  	</div>
	  	</div>
	  	
	 <!-- end tab --> 
	</div>    
           	
<!-- end tabs -->			
</div>

<!-- start button -->
<div align="left" class="button"> 
  <input type="submit" name="__update" value="<fmt:message key="Update"/>">
</div>
<!-- end button -->	   

  <!-- end table -->
    </td><td class="boxmidr" ></td></tr>
  <tr><td class="botl"></td><td class="botr"></td></tr>
  </table>
  
<!-- end main box -->  
</div>          	
</form>
</sec:authorize>

  </tiles:putAttribute>    
</tiles:insertDefinition>

