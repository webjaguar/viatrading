<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/security/tags" prefix="sec" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>

<tiles:insertDefinition name="admin.dataFeed" flush="true">
  <tiles:putAttribute name="content" type="string">
  
<sec:authorize ifAnyGranted="ROLE_AEM,ROLE_ADMIN_${prop_site_id},ROLE_DATA_FEED">  
<script language="JavaScript" type="text/JavaScript">
<!--
window.addEvent('domready', function(){			
	var tabs1 = new SimpleTabs($('tab-block-1'), { selector: 'h4'});
	var Tips1 = new Tips($$('.toolTipImg'));
});
//-->
</script>	
<form action="myselfDataFeed.jhtm" method="post">
<!-- main box -->
  <div id="mboxfull">
  
  <!-- start table -->
  <table cellpadding="0" cellspacing="0" border="0" class="module" >
  <tr>
    <td class="topl_g">
	  
	  <!-- breadcrumb -->
	  <p class="breadcrumb">
	    <a href="../dataFeed.jhtm">Data Feed</a> &gt;
	    <c:out value="${siteConfig['FEED_NAME'].value}"></c:out> 
	  </p>
	  
	  <!-- Error Message -->
	  <c:if test="${!empty message1}">
		<div class="message"><fmt:message key="${message1}"/></div>
	  </c:if>
	  <c:if test="${!empty message2}">
		<div class="message"><fmt:message key="${message2}"/></div>
	  </c:if>
	  <c:if test="${!empty ftpMessage}">
		<div class="message"><c:out value="${ftpMessage}" escapeXml="false"/></div>
      </c:if>
      
      <!-- header image -->
	  <img class="headerImage" src="../graphics/dataFeed.gif" />

  
  </td><td class="topr_g" ></td></tr>
  <tr><td class="boxmidlrg" >
 <!-- tabs -->
 <div id="tab-block-1">
	<h4 title="<c:out value="${siteConfig['FEED_NAME'].value}"></c:out> <fmt:message key='dataFeed' />"><c:out value="${siteConfig['FEED_NAME'].value}"></c:out> <fmt:message key="dataFeed" /></h4>
	<div>
	<!-- start tab -->
        <c:set var="classIndex" value="0" />
        
	  	<div class="listdivi ln tabdivi"></div>
	  	<div class="listdivi"></div>
		  	<div class="list${classIndex % 2}"><c:set var="classIndex" value="${classIndex+1}" />
		  	<div class="listfl">&nbsp;</div>
		  	<div class="listp">
		  	<!-- input field -->
	   		<c:forEach items="${dataFeedConfigList}" var="dataFeedConfig"   varStatus="status">
		  	<c:set var="fileName" value="file${dataFeedConfig.clientId}"></c:set>
				<a href="../feed/${generatedFile[fileName].name}"><c:out value="${generatedFile[fileName].name}"/></a> 
    	        <c:if test="${generatedFile['lastModified'] != null}">(<fmt:formatDate type="both" timeStyle="full" value="${generatedFile['lastModified']}"/>)</c:if>
		    </c:forEach>
		    <!-- end input field -->   	
		  	</div>
		  	</div>
		  	
		  	<div class="list${classIndex % 2}"><c:set var="classIndex" value="${classIndex+1}" />
		  	<div class="listfl"></div>
		  	<div class="listp">
		  	<!-- input field -->
		  	    
		  	    <div align="right" class="button">
		  	    <c:choose>
		  	      <c:when test="${siteConfig['DATA_FEED_MEMBER_TYPE'].value == 'PUBLISHER'}">
		  	      <%--  <input type="submit" name="__new" value="<fmt:message key="dataFeedGenerate" />"> --%>
  				    <input type="submit" name="__ftp" value="<fmt:message key="dataFeedFtp" />">
		  	      </c:when>
		  	      <c:when test="${siteConfig['DATA_FEED_MEMBER_TYPE'].value == 'SUBSCRIBER'}">
        	   	  <%--	<input type="submit" name="__download" value="<fmt:message key="dataFeedDownload" />"> --%>
  				    <input type="submit" name="__ftpDownload" value="<fmt:message key="dataFeedFtpDownload" />">
		  	      </c:when>
		  	      <c:otherwise>
		  	        <fmt:message key="toUploadDownloadFeedContactWebMaster"/>
		  	      </c:otherwise>
		  	    </c:choose>
  				</div>		    
  			<!-- end input field -->   	
		  	</div>
		  	</div>
		  	
	<!-- end tab -->        
	</div>         
<%--
	<h4 title="<c:out value="${siteConfig['FEED_NAME'].value}"></c:out> <fmt:message key='configuration' />"><c:out value="${siteConfig['FEED_NAME'].value}"></c:out> <fmt:message key="configuration" /></h4>
	<div>
	<!-- start tab -->
         <c:set var="classIndex" value="0" />
         
	  	<div class="listdivi ln tabdivi"></div>
	  	<div class="listdivi"></div>
	  	
	  	<div class="list${classIndex % 2}"><c:set var="classIndex" value="${classIndex+1}" />
	  	<div class="listfl">FTP <fmt:message key="server" />:</div>
	  	<div class="listp">
	  	<!-- input field -->
            <input type="text" name="server" value="<c:out value="${dataFeedConfig.server}"/>" maxlength="30" size="30">
	    <!-- end input field -->   	
	  	</div>
	  	</div>
	  	
	  	<div class="list${classIndex % 2}"><c:set var="classIndex" value="${classIndex+1}" />
	  	<div class="listfl">FTP <fmt:message key="username" />:</div>
	  	<div class="listp">
	  	<!-- input field -->
            <input type="text" name="username" value="<c:out value="${dataFeedConfig.username}"/>" maxlength="30" size="30">
	    <!-- end input field -->   	
	  	</div>
	  	</div>
	  	
	  	<div class="list${classIndex % 2}"><c:set var="classIndex" value="${classIndex+1}" />
	  	<div class="listfl">FTP <fmt:message key="password" />:</div>
	  	<div class="listp">
	  	<!-- input field -->
            <input type="text" name="password" value="<c:out value="${dataFeedConfig.password}"/>" maxlength="30" size="30">
	    <!-- end input field -->   	
	  	</div>
	  	</div>
	  	
	  	<div class="list${classIndex % 2}"><c:set var="classIndex" value="${classIndex+1}" />
	  	<div class="listfl">FTP <fmt:message key="fileName" />:</div>
	  	<div class="listp">
	  	<!-- input field -->
            <input type="text" name="filename" value="<c:out value="${dataFeedConfig.filename}"/>" maxlength="30" size="30">
	    <!-- end input field -->   	
	  	</div>
	  	</div>	 
	  	
	  	<c:if test="${siteConfig['DATA_FEED_MEMBER_TYPE'].value == 'PUBLISHER'}">
	  	<div class="list${classIndex % 2}"><c:set var="classIndex" value="${classIndex+1}" />
	  	<div class="listfl"><div class="helpImg"><img class="toolTipImg" title="Note::Add prefix. Leave blank if you want to upload products with same skus." src="../graphics/question.gif" /></div><fmt:message key="prefix" />:</div>
	  	<div class="listp">
	  	<!-- input field -->
            <input type="text" name="prefix" value="<c:out value="${dataFeedConfig.prefix}"/>" maxlength="30" size="30">
	    <!-- end input field -->   	
	  	</div>
	  	</div>
   	    
   	    <div class="list${classIndex % 2}"><c:set var="classIndex" value="${classIndex+1}" />
	  	<div class="listfl"><div class="helpImg"><img class="toolTipImg" title="Note::Add Caegory Id. Leave blank if you want to upload all products." src="../graphics/question.gif" /></div><fmt:message key="category" />:</div>
	  	<div class="listp">
	  	<!-- input field -->
            <input type="text" name="category" value="<c:out value="${dataFeedConfig.category}"/>" maxlength="30" size="30">
	    <!-- end input field -->   	
	  	</div>
	  	</div>
   	    </c:if>
	
	<!-- end tab --> 
	</div>  
 --%>
	<h4 title="<c:out value="${siteConfig['FEED_NAME'].value}"></c:out> <fmt:message key='configuration' />"><c:out value="${siteConfig['FEED_NAME'].value}"></c:out> <fmt:message key="configuration" /></h4>
	<div>
	<!-- start tab -->
         <c:set var="classIndex" value="0" />
         
	  	<div class="listdivi ln tabdivi"></div>
	  	<div class="listdivi"></div>
	  	
	  	<div class="list${classIndex % 2}"><c:set var="classIndex" value="${classIndex+1}" />

			<table class="form" border="0" cellspacing="2" cellpadding="0" id="feedConfig">
			  <tr>
			    <td></td>
			    <td align="center"><div class="feedConfig"><fmt:message key="active" /></div></td>
			    <td align="center"><div class="feedConfig">FTP <fmt:message key="server" /></div></td>
			    <td align="center"><div class="feedConfig">FTP <fmt:message key="username" /></div></td>
			    <td align="center"><div class="feedConfig">FTP <fmt:message key="password" /></div></td>
			    <td align="center"><div class="feedConfig">FTP <fmt:message key="fileName" /></div></td>
			    <c:if test="${siteConfig['DATA_FEED_MEMBER_TYPE'].value == 'PUBLISHER'}">
	  			<td align="center"><div class="feedConfig"><fmt:message key="prefix" /></div></td>
			    <td align="center"><div class="feedConfig"><fmt:message key="category" /></div></td>
			    </c:if>
			  </tr>
			  <c:forEach items="${dataFeedConfigList}" var="dataFeedConfig"   varStatus="status">
			  <tr>
			    <td><input type="hidden" name="clientId_${status.index}" value="${status.index}"> </td>
			    <td><input type="checkbox"  <c:if test="${dataFeedConfig.active}">checked="checked"</c:if>  name="active_${status.index}"> </td>
			    <td><input type="text" name="server_${status.index}" value="<c:out value="${dataFeedConfig.server}"/>" maxlength="30" size="20"></td>
			    <td><input type="text" name="username_${status.index}" value="<c:out value="${dataFeedConfig.username}"/>" maxlength="30" size="20"></td>
			    <td><input type="password" name="password_${status.index}" value="<c:out value="${dataFeedConfig.password}"/>" maxlength="30" size="20"></td>
			    <td><input type="text" name="filename_${status.index}" value="<c:out value="${dataFeedConfig.filename}"/>" maxlength="30" size="20"></td>
	  			<c:if test="${siteConfig['DATA_FEED_MEMBER_TYPE'].value == 'PUBLISHER'}">
	  			<td><input type="text" name="prefix_${status.index}" value="<c:out value="${dataFeedConfig.prefix}"/>" maxlength="10" size="10"></td>
	  			<td><input type="text" name="category_${status.index}" value="<c:out value="${dataFeedConfig.category}"/>" maxlength="5" size="5"></td>
	  			</c:if>
			  </tr>
			  </c:forEach>
			  <%--
			  <c:forEach items="${productForm.product.kitParts}" var="kitPart" varStatus="status">
			  <tr>
			    <td>&nbsp;</td>
			    <td><input type="text" name="__kitParts_sku" value="<c:out value="${kitPart.kitPartsSku}"/>" class="textfield250" maxlength="50"></td>
			    <td><input type="text" name="__kitParts_quantity" value="<c:out value="${kitPart.quantity}"/>" class="textfield50" maxlength="10"></td>
			  </tr>
			  </c:forEach>
			    --%>
			</table>	

	  	</div>

	<!-- end tab --> 
	</div> 
  <%--
	<h4 title="<c:out value="${siteConfig['FEED_NAME'].value}"></c:out> <fmt:message key='filter' />"><c:out value="${siteConfig['FEED_NAME'].value}"></c:out> <fmt:message key="filter" /></h4>
	<div>
	<!-- start tab -->
         <c:set var="classIndex" value="0" />
        
	  	<div class="listdivi ln tabdivi"></div>
	  	<div class="listdivi"></div>
	  	
	  	<div class="list${classIndex % 2}"><c:set var="classIndex" value="${classIndex+1}" />
	  	<div class="listfl"><div class="helpImg"><img class="toolTipImg" title="Note::Leave this field blank to get all products. Get all products that price1 is <b>grater than or equal</b> this value." src="../graphics/question.gif" /></div><fmt:message key="price" />:</div>
	  	<div class="listp">
	  	<!-- input field -->
            <input type="text" name="price" value="<c:out value="${search.price}"/>" maxlength="30" size="30">
	    <!-- end input field -->   	
	  	</div>
	  	</div>
	  	
	  	<div class="list${classIndex % 2}"><c:set var="classIndex" value="${classIndex+1}" />
	  	<div class="listfl"><div class="helpImg"><img class="toolTipImg" title="Note::Leave this field blank to get all products. Get all products that inventory is <b>grater than or equal</b> this value." src="../graphics/question.gif" /></div><fmt:message key="inventory" />:</div>
	  	<div class="listp">
	  	<!-- input field -->
            <input type="text" name="inventory" value="<c:out value="${search.inventory}"/>" maxlength="30" size="30">
	    <!-- end input field -->   	
	  	</div>
	  	</div>
   	  
	 <!-- end tab --> 
	</div>    
       	--%> 
<!-- end tabs -->			
</div>

<!-- start button -->
<div align="left" class="button"> 
  <input type="submit" name="__update" value="<fmt:message key="Update"/>">
</div>
<!-- end button -->	   

  <!-- end table -->
    </td><td class="boxmidr" ></td></tr>
  <tr><td class="botl"></td><td class="botr"></td></tr>
  </table>
  
<!-- end main box -->  
</div>          	
</form>
</sec:authorize>

  </tiles:putAttribute>    
</tiles:insertDefinition>