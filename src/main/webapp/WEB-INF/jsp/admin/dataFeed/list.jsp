<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/security/tags" prefix="sec" %>


<tiles:insertDefinition name="admin.dataFeed" flush="true">
  <tiles:putAttribute name="content" type="string">
  
<sec:authorize ifAnyGranted="ROLE_AEM,ROLE_ADMIN_${prop_site_id},ROLE_DATA_FEED">
<form action="webjaguarDataFeedList.jhtm" method="post" id="list" name="list_form">
<!-- main box -->
  <div id="mboxfull">
  
  <!-- start table -->
  <table cellpadding="0" cellspacing="0" border="0" class="module" >
  <tr>
    <td class="topl_g">
	  
	  <!-- breadcrumb -->
	  <p class="breadcrumb">
	    <a href="../dataFeed/webjaguarDataFeedList.jhtm">Data Feed</a> &gt;
	    <c:out value="${siteConfig['FEED_NAME'].value}"></c:out> 
	  </p>
	  
	  <!-- Error Message -->
	  
	  <!-- header image -->
	  <img class="headerImage" src="../graphics/dataFeed.gif" />
	  
	  	    
	  </td><td class="topr_g" ></td></tr>
  <tr><td class="boxmidlrg" >

 <!-- tabs -->
 <div class="tab-wrapper">
	<h4 title="List of affiliates"></h4>
	<div>
	<!-- start tab -->

	  	<div class="listdivi"></div>
	   
		  	<div class="listlight">
		  	<!-- input field --> 
		  	<table border="0" cellpadding="0" cellspacing="1" width="100%">
			  <tr>
			  <c:if test="${model.count > 0}">
			  <td class="pageShowing">
			  <fmt:message key="showing">
				<fmt:param value="${dataFeedSearch.offset+1}"/>
				<fmt:param value="${model.pageEnd}"/>
				<fmt:param value="${model.count}"/>
			  </fmt:message>
			  </td>
			  </c:if>
			  <td class="pageNavi">
			  Page
			  <select name="page" id="page" onchange="submit()">
			  <c:forEach begin="1" end="${model.pageCount}" var="page">
			  	<option value="${page}" <c:if test="${page == (dataFeedSearch.page)}">selected</c:if>>${page}</option>
			  </c:forEach>
			  </select>
			  of <c:out value="${model.pageCount}"/>
			  | 
			  <c:if test="${dataFeedSearch.page == 1}"><span class="pageNaviDead">previous</span></c:if>
			  <c:if test="${dataFeedSearch.page != 1}"><a href="<c:url value="webjaguarDataFeedList.jhtm"><c:param name="page" value="${dataFeedSearch.page-1}"/></c:url>" class="pageNaviLink">previous</a></c:if>
			  | 
			  <c:if test="${dataFeedSearch.page == model.pageCount}"><span class="pageNaviDead">next</span></c:if>
			  <c:if test="${dataFeedSearch.page != model.pageCount}"><a href="<c:url value="webjaguarDataFeedList.jhtm"><c:param name="page" value="${dataFeedSearch.page+1}"/></c:url>" class="pageNaviLink">next</a></c:if>
			  </td>
			  </tr>
			</table>
			
			<table border="0" cellpadding="0" cellspacing="0" width="100%" class="listings">
			  <tr class="listingsHdr2">
			    <td class="listingsHdr3"> No. </td>
			    <td class="listingsHdr3">
			      <table cellspacing="0" cellpadding="1">
			    	  <tr>
			    	    <c:choose>
			    	      <c:when test="${dataFeedSearch.sort == 'name desc'}">
			    	        <td class="listingsHdr3">
			    	        <a class="hdrLink" href="#" onClick="document.getElementById('sort').value='name';document.getElementById('list').submit()"><fmt:message key="name" /></a>
			    	        </td>
			    	        <td><img src="../graphics/up.gif" border="0"></td>
			    	      </c:when>
			    	      <c:when test="${dataFeedSearch.sort == 'name'}">
			    	        <td class="listingsHdr3">
			    	        <a class="hdrLink" href="#" onClick="document.getElementById('sort').value='name desc';document.getElementById('list').submit()"><fmt:message key="name" /></a>
			    	        </td>
			    	        <td><img src="../graphics/down.gif" border="0"></td>
			    	      </c:when>
			    	      <c:otherwise>
			    	        <td colspan="2" class="listingsHdr3">
			    	        <a class="hdrLink" href="#" onClick="document.getElementById('sort').value='name desc';document.getElementById('list').submit()"><fmt:message key="name" /></a>
			    	        </td>
			    	      </c:otherwise>
			    	    </c:choose>  
			    	  </tr>
			      </table>
			    </td>
			    <td class="listingsHdr3">
			      <table cellspacing="0" cellpadding="1">
			    	  <tr>
			    	    <c:choose>
			    	      <c:when test="${dataFeedSearch.sort == 'server desc'}">
			    	        <td class="listingsHdr3">
			    	        <a class="hdrLink" href="#" onClick="document.getElementById('sort').value='server';document.getElementById('list').submit()"><fmt:message key="server" /></a>
			    	        </td>
			    	        <td><img src="../graphics/up.gif" border="0"></td>
			    	      </c:when>
			    	      <c:when test="${dataFeedSearch.sort == 'server'}">
			    	        <td class="listingsHdr3">
			    	        <a class="hdrLink" href="#" onClick="document.getElementById('sort').value='server desc';document.getElementById('list').submit()"><fmt:message key="server" /></a>
			    	        </td>
			    	        <td><img src="../graphics/down.gif" border="0"></td>
			    	      </c:when>
			    	      <c:otherwise>
			    	        <td colspan="2" class="listingsHdr3">
			    	        <a class="hdrLink" href="#" onClick="document.getElementById('sort').value='server desc';document.getElementById('list').submit()"><fmt:message key="server" /></a>
			    	        </td>
			    	      </c:otherwise>
			    	    </c:choose>  
			    	  </tr>
			      </table>
			    </td>
			    <td class="listingsHdr3">
			      <table cellspacing="0" cellpadding="1">
			    	  <tr>
			    	    <c:choose>
			    	      <c:when test="${dataFeedSearch.sort == 'username desc'}">
			    	        <td class="listingsHdr3">
			    	        <a class="hdrLink" href="#" onClick="document.getElementById('sort').value='username';document.getElementById('list').submit()"><fmt:message key="username" /></a>
			    	        </td>
			    	        <td><img src="../graphics/up.gif" border="0"></td>
			    	      </c:when>
			    	      <c:when test="${dataFeedSearch.sort == 'username'}">
			    	        <td class="listingsHdr3">
			    	        <a class="hdrLink" href="#" onClick="document.getElementById('sort').value='username desc';document.getElementById('list').submit()"><fmt:message key="username" /></a>
			    	        </td>
			    	        <td><img src="../graphics/down.gif" border="0"></td>
			    	      </c:when>
			    	      <c:otherwise>
			    	        <td colspan="2" class="listingsHdr3">
			    	        <a class="hdrLink" href="#" onClick="document.getElementById('sort').value='username desc';document.getElementById('list').submit()"><fmt:message key="username" /></a>
			    	        </td>
			    	      </c:otherwise>
			    	    </c:choose>  
			    	  </tr>
			      </table>
			    </td>
			    <td class="listingsHdr3">
			      <table cellspacing="0" cellpadding="1">
			    	  <tr>
			    	    <c:choose>
			    	      <c:when test="${dataFeedSearch.sort == 'filename desc'}">
			    	        <td class="listingsHdr3">
			    	        <a class="hdrLink" href="#" onClick="document.getElementById('sort').value='filename';document.getElementById('list').submit()"><fmt:message key="fileName" /></a>
			    	        </td>
			    	        <td><img src="../graphics/up.gif" border="0"></td>
			    	      </c:when>
			    	      <c:when test="${dataFeedSearch.sort == 'filename'}">
			    	        <td class="listingsHdr3">
			    	        <a class="hdrLink" href="#" onClick="document.getElementById('sort').value='filename desc';document.getElementById('list').submit()"><fmt:message key="fileName" /></a>
			    	        </td>
			    	        <td><img src="../graphics/down.gif" border="0"></td>
			    	      </c:when>
			    	      <c:otherwise>
			    	        <td colspan="2" class="listingsHdr3">
			    	        <a class="hdrLink" href="#" onClick="document.getElementById('sort').value='filename desc';document.getElementById('list').submit()"><fmt:message key="fileName" /></a>
			    	        </td>
			    	      </c:otherwise>
			    	    </c:choose>  
			    	  </tr>
			      </table>
			    </td>
			    <td class="listingsHdr3">
			      <table cellspacing="0" cellpadding="1">
			    	  <tr>
			    	    <c:choose>
			    	      <c:when test="${dataFeedSearch.sort == 'prefix desc'}">
			    	        <td class="listingsHdr3">
			    	        <a class="hdrLink" href="#" onClick="document.getElementById('sort').value='prefix';document.getElementById('list').submit()"><fmt:message key="prefix" /></a>
			    	        </td>
			    	        <td><img src="../graphics/up.gif" border="0"></td>
			    	      </c:when>
			    	      <c:when test="${dataFeedSearch.sort == 'prefix'}">
			    	        <td class="listingsHdr3">
			    	        <a class="hdrLink" href="#" onClick="document.getElementById('sort').value='prefix desc';document.getElementById('list').submit()"><fmt:message key="prefix" /></a>
			    	        </td>
			    	        <td><img src="../graphics/down.gif" border="0"></td>
			    	      </c:when>
			    	      <c:otherwise>
			    	        <td colspan="2" class="listingsHdr3">
			    	        <a class="hdrLink" href="#" onClick="document.getElementById('sort').value='prefix desc';document.getElementById('list').submit()"><fmt:message key="prefix" /></a>
			    	        </td>
			    	      </c:otherwise>
			    	    </c:choose>  
			    	  </tr>
			      </table>
			    </td>
			    <td class="listingsHdr3"><fmt:message key="category" /></td>
			  </tr>
			<c:forEach items="${model.webjagaurDataFeedList}" var="dataFeed" varStatus="status">
			  <tr class="row${status.index % 2}" onmouseover="changeStyleClass(this,'rowOver')" onmouseout="changeStyleClass(this,'row${status.index % 2}')">
			    <td align="left"> ${status.index + 1} </td>
			    <td class="nameCol"><a href="webjaguarDataFeedForm.jhtm?id=${dataFeed.id}"><c:out value="${dataFeed.name}"/></a></td>
			    <td class="nameCol"><c:out value="${dataFeed.server}"/></td>
				<td class="nameCol"><c:out value="${dataFeed.username}"/></td>
				<td class="nameCol"><c:out value="${dataFeed.filename}"/></td>
			    <td class="nameCol"><c:out value="${dataFeed.prefix}"/></td>
			    <td class="nameCol"><c:out value="${dataFeed.category}"/></td>
			  </tr>
			</c:forEach>
			<c:if test="${model.count == 0}">
			  <tr class="emptyList"><td colspan="3">&nbsp;</td></tr>
			</c:if>
			</table>
			
			<table border="0" cellpadding="0" cellspacing="1" width="100%">
			  <tr>
			  <td class="pageSize">
			  <select name="size" onchange="document.getElementById('page').value=1;submit()">
			  <c:forTokens items="10,25,50,100,200,500" delims="," var="current">
			  	  <option value="${current}" <c:if test="${current == dataFeedSearch.pageSize}">selected</c:if>>${current} <fmt:message key="perPage" /></option>
			  </c:forTokens>
			  </select>
			  </td>
			  </tr>
			</table>
		  	<!-- end input field -->  	  
		  	
		  	<!-- start button --> 
		  	<div align="left" class="button">
		  	  <sec:authorize ifAnyGranted="ROLE_AEM,ROLE_ADMIN_${prop_site_id},ROLE_DATA_FEED">
		  	    <c:if test="${model.count < gSiteConfig['gWEBJAGUAR_DATA_FEED']}">
		  	      <input type="submit" name="__add" value="<fmt:message key="add" />">
		  	    </c:if>
		  	  </sec:authorize>
			</div>
			<!-- end button -->
		  	</div>
	
	<!-- end tab -->        
	</div>
<!-- end tabs -->			
</div>
  
  <!-- end table -->
   </td><td class="boxmidr" ></td></tr>
  <tr><td class="botl"></td><td class="botr"></td></tr>
  </table>
  
<!-- end main box -->  
</div> 
</form> 
</sec:authorize>

  </tiles:putAttribute>    
</tiles:insertDefinition>