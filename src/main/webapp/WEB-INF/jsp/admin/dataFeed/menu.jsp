<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/security/tags" prefix="sec" %>

  <div id="lbox">
   <c:if test="${!empty gSiteConfig['gDATA_FEED']}">
   <table cellpadding="0" cellspacing="0" border="0" class="module" style="width: 100%">
   <tr>
     <td class="topl"></td>
     <td class="topr" ></td>
   </tr>
   <tr>
     <td class="boxmid" >
       <h2 class="menuleft mfirst"><fmt:message key="dataFeed"/></h2>
       <div class="menudiv"></div>
       <c:forTokens items="${gSiteConfig['gDATA_FEED']}" delims="," var="feed">
         <a href="dataFeed.jhtm?feed=${feed}" class="userbutton"><fmt:message key="${feed}"/></a>
  	   </c:forTokens> 
  	   <div class="menudiv"></div>
    </td>
    <td class="boxmidr" ></td>
  </tr>
  <tr>
    <td class="botl"></td>
    <td class="botr"></td>
  </tr>
  </table>
  </c:if>  
  <c:if test="${gSiteConfig['gWEBJAGUAR_DATA_FEED'] > 0}">
  <table cellpadding="0" cellspacing="0" border="0" class="module" style="width: 100%">
  <tr>
    <td class="topl"></td>
    <td class="topr" ></td>
  </tr>
  <tr>
    <td class="boxmid" >
      <h2 class="menuleft mfirst"><fmt:message key="webjaguar"/></h2>
      <div class="menudiv"></div>
        <a href="webjaguarDataFeedList.jhtm" class="userbutton"><fmt:message key="dataFeed"/></a>
        <a href="webjaguarDataFeedForm.jhtm?mtz=true" class="userbutton"><fmt:message key="mytradezone"/></a>
      <div class="menudiv"></div>
    </td>
    <td class="boxmidr" ></td>
  </tr>
  <tr>
    <td class="botl"></td>
    <td class="botr"></td>
  </tr>
  </table>
  </c:if>
  </div>
