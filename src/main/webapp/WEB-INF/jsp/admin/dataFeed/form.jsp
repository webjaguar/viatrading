<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ taglib uri="/WEB-INF/tlds/spring.tld" prefix="spring" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ taglib uri="http://www.springframework.org/security/tags" prefix="sec" %>

<tiles:insertDefinition name="admin.dataFeed" flush="true">
  <tiles:putAttribute name="content" type="string">
<sec:authorize ifAnyGranted="ROLE_AEM,ROLE_ADMIN_${prop_site_id},ROLE_DATA_FEED">
<script language="JavaScript"> 
<!--
window.addEvent('domready', function(){			
	var tabs1 = new SimpleTabs($('tab-block-1'), { selector: 'h4'});
	var Tips1 = new Tips($$('.toolTipImg'));
});
//--> 
</script> 
<form:form commandName="webjaguarDataFeedForm" action="webjaguarDataFeedForm.jhtm" method="post">
<form:hidden path="newWebjaguarDataFeed" />

<!-- main box -->
  <div id="mboxfull">
  
  <!-- start table -->
  <table cellpadding="0" cellspacing="0" border="0" class="module" >
  <tr>
    <td class="topl_g">
	  
	  <!-- breadcrumb -->
	  <p class="breadcrumb">
	    <a href="../dataFeed/webjaguarDataFeedList.jhtm">Data Feed</a> &gt;
	    <c:out value="${siteConfig['FEED_NAME'].value}"></c:out> 
	  </p>
	  
	  <!-- Error Message -->
	  <%--<c:if test="${message != null}">
  		<div class="message"><fmt:message key="${message}"/></div>
  	  </c:if> --%>
  	  <spring:hasBindErrors name="webjaguarDataFeedForm">
		<div class="message">Please fix all errors!</div>
	  </spring:hasBindErrors>
	  <!-- Error Message -->
	  <c:if test="${!empty message1}">
		<div class="message"><fmt:message key="${message1}"/></div>
	  </c:if>
	  <c:if test="${!empty message2}">
		<div class="message"><fmt:message key="${message2}"/></div>
	  </c:if>
	  <c:if test="${!empty ftpMessage}">
		<div class="message"><c:out value="${ftpMessage}" escapeXml="false"/></div>
      </c:if>
      

  </td><td class="topr_g" ></td></tr>
  <tr><td class="boxmidlrg" >

 <!-- tabs -->
 <div id="tab-block-1">
	<h4 title="Configuration">Configuration</h4>
	<div>
	<!-- start tab -->

         <c:set var="classIndex" value="0" />
		 <div class="listdivi ln tabdivi"></div>
		
		 <div class="list${classIndex % 2}"><c:set var="classIndex" value="${classIndex+1}" />
		 <div class="listfl"><div class="helpImg"><img class="toolTipImg" title="Active::Automatic feed work only if this feed is active." src="../graphics/question.gif" /></div><fmt:message key="active" />:</div>
		 <div class="listp">
		 <!-- input field -->
		        <form:checkbox path="webjaguarDataFeed.active"/>
		        <form:errors path="webjaguarDataFeed.active"></form:errors>
		 <!-- end input field -->	
	 	 </div>
		 </div>
		 
		 <div class="list${classIndex % 2}"><c:set var="classIndex" value="${classIndex+1}" />
		 <div class="listfl"><div class="helpImg"><img class="toolTipImg"/></div><fmt:message key="type" />:</div>
		 <div class="listp">
		 <!-- input field -->
		        <form:select path="webjaguarDataFeed.feedType">
		          <form:option value="webjaguar">webjaguar</form:option>
		          <form:option value="mytradezone">mytradezone</form:option>
		          <form:option value="premier">premier</form:option>
		        </form:select>
		        <form:errors path="webjaguarDataFeed.feedType"></form:errors>
		 <!-- end input field -->	
	 	 </div>
		 </div>
		
		 <div class="list${classIndex % 2}"><c:set var="classIndex" value="${classIndex+1}" />
		 <div class="listfl"><div class="requiredField"><div class="helpImg"><img class="toolTipImg" title="Token::This number varifies the authentication. If you are a publisher, this token is auto generated and provide it to all your subscriber. If you are a subscriber, ask your feed provider to get the token and save it here." src="../graphics/question.gif" /></div><fmt:message key="token" />:</div></div>
		 <div class="listp">
		 <!-- input field -->
		        <c:choose>
		        <c:when test="${siteConfig['DATA_FEED_MEMBER_TYPE'].value == 'PUBLISHER' and webjaguarDataFeedForm.webjaguarDataFeed.subscriber}">
		 		  <c:out value="${webjaguarDataFeedForm.webjaguarDataFeed.token}"></c:out>
		 	    </c:when>
		        <c:otherwise>
		          <form:input path="webjaguarDataFeed.token" cssClass="textfield" maxlength="255" size="60" htmlEscape="true"/>
		        </c:otherwise>
		        </c:choose>
		          <form:errors path="webjaguarDataFeed.token"></form:errors>
		 <!-- end input field -->	
	 	 </div>
		 </div>
		 
		 <div class="list${classIndex % 2}"><c:set var="classIndex" value="${classIndex+1}" />
		 <div class="listfl"><div class="helpImg"><img class="toolTipImg" title="Name::Provide name that identifies this feed." src="../graphics/question.gif" /></div><div class="requiredField"><fmt:message key="name" />:</div></div>
		 <div class="listp">
		 <!-- input field -->
		 		<form:input path="webjaguarDataFeed.name" cssClass="textfield" maxlength="255" size="60" htmlEscape="true"/>
		  		<form:errors path="webjaguarDataFeed.name" cssClass="error"/>
	 	 <!-- end input field -->	
	 	 </div>
		 </div>
		 
		 <div class="list${classIndex % 2}"><c:set var="classIndex" value="${classIndex+1}" />
		 <div class="listfl"><div class="helpImg"><img class="toolTipImg" title="Server::Provide ftp location to save/get data feed." src="../graphics/question.gif" /></div><div class="requiredField"><fmt:message key="server" />:</div></div>
		 <div class="listp">
		 <!-- input field -->
		 		<form:input path="webjaguarDataFeed.server" cssClass="textfield" maxlength="255" size="60" htmlEscape="true"/>
		  		<form:errors path="webjaguarDataFeed.server" cssClass="error"/>
	 	 <!-- end input field -->	
	 	 </div>
		 </div>
		 
		 <div class="list${classIndex % 2}"><c:set var="classIndex" value="${classIndex+1}" />
		 <div class="listfl"><div class="helpImg"><img class="toolTipImg" title="Username ::Provide username to login ftp server." src="../graphics/question.gif" /></div><div class="requiredField"><fmt:message key="username" />:</div></div>
		 <div class="listp">
		 <!-- input field -->
		 		<form:input path="webjaguarDataFeed.username" cssClass="textfield" maxlength="255" size="60" htmlEscape="true"/>
		  		<form:errors path="webjaguarDataFeed.username" cssClass="error"/>
	 	 <!-- end input field -->	
	 	 </div>
		 </div>
		 
		 <div class="list${classIndex % 2}"><c:set var="classIndex" value="${classIndex+1}" />
		 <div class="listfl"><div class="helpImg"><img class="toolTipImg" title="Password ::Password to login ftp server." src="../graphics/question.gif" /></div><fmt:message key="password" />:</div>
		 <div class="listp">
		 <!-- input field -->
		 		<form:password path="webjaguarDataFeed.password" cssClass="textfield" maxlength="255" size="60" htmlEscape="true" showPassword="true"/>
		  		<form:errors path="webjaguarDataFeed.password" cssClass="error"/>
	 	 <!-- end input field -->	
	 	 </div>
		 </div>
		 
		 <div class="list${classIndex % 2}"><c:set var="classIndex" value="${classIndex+1}" />
		 <div class="listfl"><div class="helpImg"><img class="toolTipImg" title="File Name::File name to save/get data feed from ftp server." src="../graphics/question.gif" /></div><div class="requiredField"><fmt:message key="fileName" />:</div></div>
		 <div class="listp">
		 <!-- input field -->
		 		<form:input path="webjaguarDataFeed.filename" cssClass="textfield" maxlength="255" size="60" htmlEscape="true"/>
		  		<form:errors path="webjaguarDataFeed.filename" cssClass="error"/>
	 	 <!-- end input field -->	
	 	 </div>
		 </div>
		 
		 <div class="list${classIndex % 2}"><c:set var="classIndex" value="${classIndex+1}" />
		 <div class="listfl"><div class="helpImg"><img class="toolTipImg" title="File Type::File type to save/get data feed from ftp server." src="../graphics/question.gif" /></div><div class="requiredField"><fmt:message key="fileType" />:</div></div>
		 <div class="listp">
		 <!-- input field -->
		 		<form:select path="webjaguarDataFeed.filetype">
		 		  <form:option value="xml" label="XML"></form:option>
		 		  <form:option value="txt" label="TEXT"></form:option>
		 		</form:select>
		  		<form:errors path="webjaguarDataFeed.filetype" cssClass="error"/>
	 	 <!-- end input field -->	
	 	 </div>
		 </div>
		 
		 <c:if test="${siteConfig['DATA_FEED_MEMBER_TYPE'].value == 'PUBLISHER' and webjaguarDataFeedForm.webjaguarDataFeed.subscriber}">
		 <div class="list${classIndex % 2}"><c:set var="classIndex" value="${classIndex+1}" />
		 <div class="listfl"><div class="helpImg"><img class="toolTipImg" title="Prefix ::Prefix will be appended to eack sku on the feed." src="../graphics/question.gif" /></div><fmt:message key="prefix" />:</div>
		 <div class="listp">
		 <!-- input field -->
		 		<form:input path="webjaguarDataFeed.prefix" cssClass="textfield" maxlength="255" size="60" htmlEscape="true"/>
		  		<form:errors path="webjaguarDataFeed.prefix" cssClass="error"/>
	 	 <!-- end input field -->	
	 	 </div>
		 </div>
		 
		 <div class="list${classIndex % 2}"><c:set var="classIndex" value="${classIndex+1}" />
		 <div class="listfl"><div class="helpImg"><img class="toolTipImg" title="Category ::Provide category id here, if you want to restrict data feed to get only those prodcuts in that category." src="../graphics/question.gif" /></div><fmt:message key="category" />:</div>
		 <div class="listp">
		 <!-- input field -->
		 		<form:input path="webjaguarDataFeed.category" cssClass="textfield" maxlength="255" size="60" htmlEscape="true"/>
		  		<form:errors path="webjaguarDataFeed.category" cssClass="error"/>
	 	 <!-- end input field -->	
	 	 </div>
		 </div>
		 </c:if>
		
	<!-- end tab -->        
	</div>
	<c:if test="${webjaguarDataFeedForm.webjaguarDataFeed.feedType == 'mytradezone'}">
	<h4 title="FieldMapping">Field Mapping</h4>
	<div>
	<!-- start tab -->

         <c:set var="classIndex" value="0" />
		 <div class="listdivi ln tabdivi"></div>
		
		 <div class="list${classIndex % 2}"><c:set var="classIndex" value="${classIndex+1}" />
		 <div class="listp">
		 <!-- input field -->
           <table cellspacing="5" class="form">
		  <tr>
		    <td width="50">&nbsp;</td>
		    <td align="center" width="50"><fmt:message key="Enabled" /></td>
		    <td><fmt:message key="fieldName" /></td>
		    <td><fmt:message key="product" /></td>
		    <td><fmt:message key="defaultValue" /></td>
		  </tr>
		  <c:forEach items="${dataFeedList}" var="dataFeed">
		  <tr>
		    <td>&nbsp;</td>
		    <td align="center"><input name="__enabled_${dataFeed.fieldName}" type="checkbox" <c:if test="${dataFeed.enabled or dataFeed.required}">checked</c:if> <c:if test="${dataFeed.required}">disabled</c:if>></td>
		    <td><c:out value="${dataFeed.fieldName}"/><img class="toolTipImg" title="<c:out value="${dataFeed.fieldName}"/>::<c:out value="${dataFeed.description}"/>" src="../graphics/question.gif" /></td>
		    <td>
		      <select name="__methodName_${dataFeed.fieldName}">
		  	  <option value=""></option>
		  	  <c:forEach items="${productFields}" var="productField">
			  <c:if test="${productField.enabled}">
		  	    <option value="getField${productField.id}"
		  	      <c:if test="${productField.methodName == dataFeed.methodName}">selected</c:if>>Field <c:out value="${productField.id}"/>: ${productField.name}</option>
		  	  </c:if>
			  </c:forEach>
			  </select>
			</td>
		    <td>
			  <input type="text" name="__defaultValue_${dataFeed.fieldName}" value="<c:out value="${dataFeed.defaultValue}"/>" maxlength="255" size="10">
		    </td>
		  </tr>
		  </c:forEach>
		</table>
		 <!-- end input field -->	
	 	 </div>
		 </div>
	<!-- end tab -->        
	</div>
	</c:if>
<!-- end tabs -->			
</div>

<!-- start button -->
	<div align="left" class="button">
	  <c:if test="${webjaguarDataFeedForm.newWebjaguarDataFeed and count < gSiteConfig['gWEBJAGUAR_DATA_FEED']}">
	    <sec:authorize ifAnyGranted="ROLE_AEM,ROLE_ADMIN_${prop_site_id},ROLE_DATA_FEED">
          <input type="submit" value="<spring:message code="save"/>" />
        </sec:authorize>  
      </c:if>
      <c:if test="${!webjaguarDataFeedForm.newWebjaguarDataFeed}">
        <sec:authorize ifAnyGranted="ROLE_AEM,ROLE_ADMIN_${prop_site_id},ROLE_DATA_FEED">
          <input type="submit" value="<spring:message code="update"/>" />
          <input type="submit" value="<spring:message code="delete"/>" name="_delete" onClick="return confirm('Delete permanently?')" />
          
          <c:choose>
		    <c:when test="${siteConfig['DATA_FEED_MEMBER_TYPE'].value == 'PUBLISHER' and webjaguarDataFeedForm.webjaguarDataFeed.subscriber}">
		  	  <input type="submit" name="_ftp" value="<fmt:message key="dataFeedFtp" />">
		  	</c:when>
		  	<c:when test="${siteConfig['DATA_FEED_MEMBER_TYPE'].value == 'SUBSCRIBER' and !webjaguarDataFeedForm.webjaguarDataFeed.subscriber}">
        	  <input type="submit" name="_ftpDownload" value="<fmt:message key="dataFeedFtpDownload" />">
		  	</c:when>
		  </c:choose>
        </sec:authorize>  
      </c:if>
	</div>
<!-- end button -->
  
  <!-- end table -->
    </td><td class="boxmidr" ></td></tr>
  <tr><td class="botl"></td><td class="botr"></td></tr>
  </table>
  
<!-- end main box -->  
</div>
</form:form>
</sec:authorize> 

  </tiles:putAttribute>    
</tiles:insertDefinition>