<%@ page import="java.net.*" %>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="/WEB-INF/tlds/custom.tld" prefix="wj" %>
<%@ taglib uri="http://www.springframework.org/security/tags" prefix="sec" %>
<%@ page contentType="text/html;charset=UTF-8" pageEncoding="UTF-8" %>


<tiles:insertDefinition name="admin.catalog.product" flush="true">
  <tiles:putAttribute name="content" type="string">
  
<sec:authorize ifAnyGranted="ROLE_AEM,ROLE_ADMIN_${prop_site_id},ROLE_PRODUCT_VIEW_LIST,ROLE_PRODUCT_VIEW_LISTXX"> 
<c:if test="${model.count > 0}">
<script type="text/javascript" src="../../admin/javascript/multiple.open.accordion1.2.js"></script>
<script language="JavaScript">
<!--
<c:if test="${model.count > 0}">
window.addEvent('domready', function(){
	var Tips1 = new Tips('.toolTipImg');  
	var inventoryAdjustBox = new multiBox('mbInventory', {showControls : false, useOverlay: false, showNumbers: false });
	new SmoothScroll({ duration:300 }, window);
	// Create the accordian
	var myAccordion = new Fx.Accordion($$('div.arrow_drop'), $$('div.information'), {
		display: 1, alwaysHide: true,
    	onActive: function() {$('information').removeClass('displayNone');}
	});
	<sec:authorize ifAnyGranted="ROLE_AEM,ROLE_ADMIN_${prop_site_id},ROLE_QUICK_EXPORT">
	$('quickModeTrigger').addEvent('click',function() {
	    $('mboxfull').setStyle('margin', '0');
	    $$('.quickMode').each(function(el){
	    	el.hide();
	    });
	    $$('.quickModeRemove').each(function(el){
	    	el.destroy();
	    });
	    
	    $$('.mbInventoryAFS').each(function(el){
	    	el.getParent().getParent().show();
	    	var onHand_AFS = el.text.split('/');   	
	    	el.text = onHand_AFS[0].trim();
	    });

	    $$('.mbInventory').each(function(el){
	    	var onHand_AFS = el.text.split('/') ;   	
	    	el.text = onHand_AFS[1].trim();
	    });
	    
	    $$('.listingsHdr3WithWrap').each(function(el){
	    	el.removeProperties('colspan', 'class');
	    });
	    alert('Select the whole page, copy and paste to your excel file.');
	  });
    </sec:authorize>
});
</c:if>
<c:if test="${!empty siteConfig['PRODUCT_SYNC'].value}">
function syncProduct() {
     var req = new Request({  
         method: 'get',  
         onRequest: function() { 
            this.box = new Element('img', {src:'../graphics/spinner.gif'}, {id:'syncSpinner'}).inject($('syncProductResp'),'inside');},  
         onComplete: function(response) { $('syncProductResp').innerHTML = "<span style='color:#c00000;'> Done!" + response  + " Product Synchronized.</span>";
         	$('startSync1').setStyle('display', 'none'); $('startSync2').setStyle('display', 'none'); $('syncSpinner').setStyle('display', 'none');},
         url: 'syncProductController.jhtm'}).send();   
}
</c:if>
function toggleAll(el) {
	var ids = document.getElementsByName("__selected_id");	
  	if (ids.length == 1)
      document.list_form.__selected_id.checked = el.checked;
    else
      for (i = 0; i < ids.length; i++)
        document.list_form.__selected_id[i].checked = el.checked;	
}  
<sec:authorize ifAnyGranted="ROLE_AEM,ROLE_ADMIN_${prop_site_id},ROLE_PRODUCT_DELETE">  
function deleteSelected() {
	var ids = document.getElementsByName("__selected_id");	
  	if (ids.length == 1) {
      if (document.list_form.__selected_id.checked) {
    	return confirm('Delete permanently?');
      }
    } else {
      for (i = 0; i < ids.length; i++) {
        if (document.list_form.__selected_id[i].checked) {
    	  return confirm('Delete permanently?');
        }
      }
    }

    alert("Please select product(s) to delete.");       
    return false;
}
</sec:authorize>
function optionsSelected() {
	var ids = document.getElementsByName("__selected_id");	
  	if (ids.length == 1) {
      if (document.list_form.__selected_id.checked) {
    	return true;
      }
    } else {
      for (i = 0; i < ids.length; i++) {
        if (document.list_form.__selected_id[i].checked) {
    	  return true;
        }
      }
    }

    alert("Please select product(s).");       
    return false;
}
function duplicateSelected() {
	var ids = document.getElementsByName("__selected_id");	
  	if (ids.length == 1) {
      if (document.list_form.__selected_id.checked) {
    	  if ( document.list_form.__duplicate_sku_prefix.value != "") {
    		  return true;
    	  } else {
    		  alert("Please add prefix."); 
    		  return false;
    	  }
       }
    } else {
      for (i = 0; i < ids.length; i++) {
        if (document.list_form.__selected_id[i].checked) {
        	  if (document.list_form.__duplicate_sku_prefix.value != "") {
        		  return true;
        	  } else {
        		  alert("Please add prefix."); 
        		  return false;
        	  }
        }
      }
    }

    alert("Please select product(s).");       
    return false;
}

function fobSelected() {
	var ids = document.getElementsByName("__selected_id");	
  	if (ids.length == 1) {
      if (document.list_form.__selected_id.checked) {
    	  if ( document.list_form.__fob.value != "") {
    		  return true;
    	  } else {
    		  alert("Please add a fob value."); 
    		  return false;
    	  }
       }
    } else {
      for (i = 0; i < ids.length; i++) {
        if (document.list_form.__selected_id[i].checked) {
        	if (document.list_form.__fob.value != "") {
        		return true;
        	} else {
        		alert("Please add a fob value."); 
        		return false;
        	}
        }
      }
    }

    alert("Please select product(s).");       
    return false;
}

function toggleAll(el) {
	var ids = document.getElementsByName("__selected_id");	
  	if (ids.length == 1)
      document.list_form.__selected_id.checked = el.checked;
    else
      for (i = 0; i < ids.length; i++)
        document.list_form.__selected_id[i].checked = el.checked;	
}

function APPS_Labels() {
	
	var ids = document.getElementsByName("__selected_id");	
	var url = "https://www.viatrading.com/dv/label/multiple.php?sku=";
	for (i = 0; i < ids.length; i++) {
        if (document.list_form.__selected_id[i].checked) {
        	var sku = document.getElementsByName("__selected_sku_"+ids[i].value);
        	url = url + sku[0].value + ",";     			   		
	    }
	}
	window.open(url);
}

function batchSelected() {
	var ids = document.getElementsByName("__selected_id");	
  	if (ids.length == 1) {
      if (document.list_form.__selected_id.checked) {
    	return confirm('Batch print lables?');
      }
    } else {
      for (i = 0; i < ids.length; i++) {
        if (document.list_form.__selected_id[i].checked) {
    	  return confirm('Batch print lables?');
        }
      }
    }
  	if(document.list_form.checkAll.checked) {
  	  return confirm('Batch print lables?');  		
  	}

    alert("Please select product(s) to batch.");       
    return false;
}
//-->
</script>
</c:if>
<script language="JavaScript">
<!--
function allCategory() {
	document.getElementById('category').value='';
	document.list_form.submit();
}
function assignValue(e, ele, id) {
	if(ele.substring(0,9) == "adjustQty") {
		document.getElementById('adjustQty_'+id).value = e;
	} else if(ele.substring(0,9) == "adjustNot") {
		document.getElementById('adjustNote_'+id).value = e;
	} else if(ele.substring(0,9) == "adjustAvF") {
		document.getElementById('adjustAvForSale_'+id).checked = true;		
	}
}
function adjustInventory(id) {
	var sku = $('sku_'+id).value;
	var inventory = $('inventory_'+id).value;
	var adjustQty = $('adjustQty_'+id).value;
	var adjustNote = $('adjustNote_'+id).value;
	var adjustAvForSale = false;
	if($('adjustAvForSale_'+id) != null && $('adjustAvForSale_'+id).checked) {
		adjustAvForSale= true;
	}
	var request = new Request({
		url: "adjust-ajax-Inventory.jhtm?sku="+sku+"&inventory="+inventory+"&adjustQty="+adjustQty+"&adjustNote="+adjustNote+"&adjustAvForSale="+adjustAvForSale,
		method: 'post',
		onComplete: function(response) { 
			location.reload(true);
		}
	}).send();
}

//-->
</script>
<a id="top" name="top"></a>
<form action="productList.jhtm" method="post" name="list_form" id="list">
<input type="hidden" id="sort" name="sort" value="${productSearch.sort}" />
<!-- main box -->
  <div id="mboxfull">
  
  <!-- start table -->
  <table cellpadding="0" cellspacing="0" border="0" class="module" >
  <tr class="quickMode">
    <td class="topl_g">
    
    <!-- breadcrumb -->
	  <p class="breadcrumb">
	    <a href="../catalog"><fmt:message key="catalog" /></a> &gt;
	    <c:if test="${orderForm.tempPromoErrorMessage != null}"><fmt:message key="${model.lowInventory}"/></c:if> <fmt:message key="products" />
	  </p>
    
   	<!-- Error -->
    
    <!-- header image -->
	<img id="quickModeTrigger" class="headerImage" src="../graphics/category.gif" />
    
    <%-- <div class="breadcrumbBox">
	  <!-- header image -->
	  <div class="headerImage"><img  id="quickModeTrigger" src="../graphics/category.gif" /></div>
	  
	  <!-- breadcrumb -->
	  <ul class="breadcrumb" id="breadcrumbId"> 	
	    <li class="first"><a href="../catalog">Catalog</a></li>
	    <li>&raquo;</li>
	    <li class="last"><c:if test="${orderForm.tempPromoErrorMessage != null}"><fmt:message key="${model.lowInventory}"/></c:if> <fmt:message key="products" /></li>
	  </ul>
	  
	  <!-- Error Message -->
	  <c:if test="${!empty model.message}">
		<div class="message"><fmt:message key="${model.message}" /></div>
	  </c:if>
	</div> --%>
	  
	  
	  <!-- Options -->  
	  <c:if test="${model.count > 0}">
	    <div class="optionBox"><div class="titlePrivilege"><fmt:message key="options" /></div></div>
		<div class="arrow_drop arrowImageRight"></div>
		<div style="clear: both;"></div>
		<div class="information displayNone" id="information" style="float:left;">
			 <div style="float:left;padding: 5px">
	         <c:if test="${gSiteConfig['gSALES_PROMOTIONS']}">
	           <table class="productBatchTool" cellpadding="5" cellspacing="5">
	           <tr>
	            <td valign="top">
	             <table cellpadding="1" cellspacing="1">
	              <tr style="height:30px;white-space: nowrap">
	               <td valign="top" width="20px"><img class="toolTipImg" title="SalesTag::<fmt:message key="applySalesTagToTheFollowingSelectedProduct" />" src="../graphics/question.gif" /></td>
	               <td valign="top"><h3><fmt:message key="salesTag" /></h3></td>
	              </tr>
	             </table>
	            </td>
	           </tr>
	           <tr>
	            <td valign="top">
	             <table cellpadding="1" cellspacing="1">
	              <tr style="">
			       <td>
			       <select name="__salesTag_id">
				    <option value="-999" ><c:out value="No Sales Tag"/></option>
				    <c:forEach items="${model.salesTagCodes}" var="option">
				     <option value="${option.tagId}" ><c:out value="${option.title}"/></option>
			        </c:forEach>	
				   </select>
				   <div align="left" class="buttonLeft">
		            <input type="submit" name="__salesTagBatcher" value="Apply" onClick="return optionsSelected()">
		           </div>
			       </td>
			      </tr>
	             </table>
	            </td>
	           </tr>
	           </table>
			 </c:if>
			 </div>    
			 <div style="float:left;padding: 5px">
	           <table class="productBatchTool" cellpadding="5" cellspacing="5">
	           <tr>
	            <td valign="top">
	             <table cellpadding="1" cellspacing="1">
	              <tr style="height:30px;white-space: nowrap">
	               <td valign="top" width="20px"><img class="toolTipImg" title="Category Id::Associate category Id to the following product(s) in the list.Seperate multiple Ids with a comma" src="../graphics/question.gif" /></td>
	               <td valign="top"><h3><fmt:message key="categoryId" /></h3></td>
	              </tr>
	             </table>
	            </td>
	           </tr>
	           <tr>
	            <td valign="top">
	             <table cellpadding="1" cellspacing="1">
	              <tr style="">
			       <td>
			        <input name="__category_id" type="text" size="15"/>
			        <div align="left" class="buttonLeft">
		            <input type="submit" name="__categoryAssignBacher" value="Apply" onClick="return optionsSelected()">
		            </div>
			       </td>
			       <td >
			        <div><p><input name="__batch_type" type="radio" checked="checked" value="add"/>Add</p></div>
				    <div><p><input name="__batch_type" type="radio" value="remove"/>Remove</p></div>
				    <div><p><input name="__batch_type" type="radio" value="move"/>Move</p></div>
			       </td>
			     </tr>
	             </table>
	            </td>
	           </tr>
	           </table>		 
			</div>
			<div style="float:left;padding: 5px">
	           <table class="productBatchTool" cellpadding="5" cellspacing="5">
	           <tr>
	            <td valign="top">
	             <table cellpadding="1" cellspacing="1">
	              <tr style="height:30px;white-space: nowrap">
	               <td valign="top" width="20px"><img class="toolTipImg" title="Active::Activate or Deactivate following product(s) in the list." src="../graphics/question.gif" /></td>
	               <td valign="top"><h3><fmt:message key="active" /></h3></td>
	              </tr>
	             </table>
	            </td>
	           </tr>
	           <tr>
	            <td valign="top">
	             <table cellpadding="1" cellspacing="1">
	              <tr style="">
			       <td>
			       <select name="__active_mode">
				    <option value="1" ><c:out value="active"/></option>
				    <option value="0" ><c:out value="inActive"/></option>
				   </select>
				   <div align="left" class="buttonLeft">
		            <input type="submit" name="__activeBacher" value="Apply" onClick="return optionsSelected()">
		           </div>
			       </td>
			      </tr>
	             </table>
	            </td>
	           </tr>
	           </table>
			</div>
			<c:if test="${gSiteConfig['gMANUFACTURER']}">
			 <div style="float:left;padding: 5px">
	           <table class="productBatchTool" cellpadding="5" cellspacing="5">
	           <tr>
	            <td valign="top">
	             <table cellpadding="1" cellspacing="1">
	              <tr style="height:30px;white-space: nowrap">
	               <td valign="top" width="20px"><img class="toolTipImg" title="Manufacturer::Assign Manufacturer to the following product(s) in the list." src="../graphics/question.gif" /></td>
	               <td valign="top"><h3><fmt:message key="manufacturer" /></h3></td>
	              </tr>
	             </table>
	            </td>
	           </tr>
	           <tr>
	            <td valign="top">
	             <table cellpadding="1" cellspacing="1">
	              <tr style="">
			       <td>
			       <select name="__manufacturer_id">
				   <option value="-999" ><c:out value="No Manufacturer"/></option>
				    <c:forEach items="${model.manufactuereCodes}" var="option">
				     <option value="${option.id}" ><c:out value="${option.name}"/></option>
			        </c:forEach>
			       </select> 
				   <div align="left" class="buttonLeft">
		            <input type="submit" name="__manufacturerBacher" value="Apply" onClick="return optionsSelected()">
		           </div>
			       </td>
			      </tr>
	             </table>
	            </td>
	           </tr>
	           </table>
			 </div>
			</c:if>
			<div style="float:left;padding: 5px">
	           <table class="productBatchTool" cellpadding="5" cellspacing="5">
	           <tr>
	            <td valign="top">
	             <table cellpadding="1" cellspacing="1">
	              <tr style="height:30px;white-space: nowrap">
	               <td valign="top" width="20px"><img class="toolTipImg" title="FOB::Changing fob value of products." src="../graphics/question.gif" /></td>
	               <td valign="top"><h3><fmt:message key="fob" /></h3></td>
	              </tr>
	             </table>
	            </td>
	           </tr>
	           <tr>
	            <td valign="top">
	             <table cellpadding="1" cellspacing="1" style="width:90%">
	              <tr>
			       <td>
			       Fob Zip Code:<input name="__fob" type="text" size="15" style="width: 116px;float:right"/>
			       </td></tr>
			     </table>
				 <div align="left" class="buttonLeft">
		            <input type="submit" name="__fobBacher" value="Apply" onClick="return fobSelected()">
		         </div>
			    </td>
			   </tr>
	           </table>
			</div>
			<div style="float:left;padding: 5px">
	           <table class="productBatchTool" cellpadding="5" cellspacing="5">
	           <tr>
	            <td valign="top">
	             <table cellpadding="1" cellspacing="1">
	              <tr style="height:30px;white-space: nowrap">
	               <td valign="top" width="20px"><img class="toolTipImg" title="Duplicate Sku::Create duplicate sku of following product(s) in the list." src="../graphics/question.gif" /></td>
	               <td valign="top"><h3><fmt:message key="duplicateSku" /></h3></td>
	              </tr>
	             </table>
	            </td>
	           </tr>
	           <tr>
	            <td valign="top">
	             <table cellpadding="1" cellspacing="1" style="width:90%">
	              <tr>
			       <td>
			       Prefix:<input name="__duplicate_sku_prefix" type="text" size="15" style="width: 116px;float:right"/>
			       </td></tr>
			       <tr><td>
			       Category:<input name="__category_association" type="text" size="10" style="width: 116px;float:right"/>
			       </td>
			      </tr>
			     </table>
				 <div align="left" class="buttonLeft">
		            <input type="submit" name="__duplicateSkuBacher" value="Apply" onClick="return duplicateSelected()">
		         </div>
			    </td>
			   </tr>
	           </table>
			</div>
			<c:if test="${siteConfig['BATCH_PRINT_PRODUCT_LABEL'].value == 'true'}">
	         <div style="float:left;padding: 5px">
	           <table class="productBatchTool" cellpadding="5" cellspacing="5">
	           <tr>
	            <td valign="top">
	             <table cellpadding="1" cellspacing="1" width="100%">
	              <tr style="height:30px;white-space: nowrap">
	               <td valign="top" width="20px"><img class="toolTipImg" title="<fmt:message key="batchPrint" />::Print Product Label" src="../graphics/question.gif" /></td>
	               <td valign="top"><h3><fmt:message key="label" /></h3></td>
	               <td valign="top" align="right"><input type="checkbox" onclick="toggleAll(this)" name="checkAll"/> <fmt:message key="all"/></td>
	              </tr>
	             </table>
	            </td>
	           </tr>
	           <tr>
	            <td valign="top">
	             <table cellpadding="1" cellspacing="1">
	              <tr>
			       <td>
			        <select name="__qty_sku">
				  	 <option value="1" ><fmt:message key="quantity"/></option>
			         <c:forEach var="i" begin="1" end="10">
				  	  <option value="${i}" >${i}</option>
					 </c:forEach>
			       </select>
			       </td>
			      </tr>
			      <tr>
			       <td>
			       <select name="__label_layout_id">
					  <c:forEach items="${model.labelTemplateList}" var="productLabelTemplate">
					   <option value="${productLabelTemplate.id}"><c:out value="${productLabelTemplate.name}"/></option>
					  </c:forEach>	    
					</select>
			       </td>
			      </tr>
			      <tr>
			       <td>
			        <div align="left" class="buttonLeft">
		            <input type="submit" name="__batchPdf" value="<fmt:message key="batchPrint" />" onClick="return batchSelected()"/>
		            </div>
			       </td>
			     </tr>
	             </table>
	            </td>
	           </tr>
	           </table>
	         </div>
	         
	         	<div style="float:left;padding: 5px">
	           <table class="productBatchTool" cellpadding="5" cellspacing="5">
	           <tr>
	            <td valign="top">
	             <table cellpadding="1" cellspacing="1">
	              <tr style="height:30px;white-space: nowrap">
	               <td valign="top"><h3>Print Labels from APPS</h3></td>
	              </tr>
	               <tr>
	               <td valign="top">Select below skus to print</td>
	              </tr>
	             </table>
	            </td>
	           </tr>
	           <tr>
	            <td valign="top">
	             <table cellpadding="1" cellspacing="1">
	              <tr style="">
			       <td>
			        <div align="left" class="buttonLeft">
		            <input type="button" name="__batchPdf" value="Print" onClick="return APPS_Labels();"/>
		            </div>
			       </td>
			       
			     </tr>
	             </table>
	            </td>
	           </tr>
	           </table>		 
			</div>
	       </c:if>
			<c:if test="${!empty siteConfig['PRODUCT_SYNC'].value}">
			<div style="float:left;padding: 5px">
	           <table class="productBatchTool" cellpadding="5" cellspacing="5">
	           <tr>
	            <td valign="top">
	             <table cellpadding="1" cellspacing="1">
	              <tr style="height:30px;white-space: nowrap">
	               <td valign="top" width="20px"><img class="toolTipImg" title="Synchronize Product::Click on Sync Image will automatically synchronize products with the master website product." src="../graphics/question.gif" /></td>
	               <td valign="top"><h3>Synchronize Product</h3></td>
	              </tr>
	             </table>
	            </td>
	           </tr>
	           <tr>
	            <td valign="top">
	             <table cellpadding="1" cellspacing="1">
	              <tr style="">
			       <td>
			        <span id="startSync1">Click Sync Button to Start</span>
			       </td>
			     </tr>
	              <tr style="">
			       <td>
			        <img style="cursor: pointer;" src="../graphics/sync.jpg" id="startSync2" onclick="syncProduct()" /><span style="display: block !important;" id="syncProductResp" />
			       </td>
			     </tr>
	             </table>
	            </td>
	           </tr>
	           </table>		 
			</div>
			</c:if>
			<div style="float:left;padding: 5px">
	           <table class="productBatchTool" cellpadding="5" cellspacing="5">
	           <tr>
	            <td valign="top">
	             <table cellpadding="1" cellspacing="1" width="100%">
	              <tr style="height:30px;white-space: nowrap">
	               <td valign="top" width="20px"><img class="toolTipImg" title="Edit Field::Edit field of selected product(s) in the list." src="../graphics/question.gif" /></td>
	               <td valign="top"><h3><fmt:message key="field" /></h3></td>
	               <td valign="top" align="right"><input type="checkbox" onclick="toggleAll(this)" name="checkAll"/> <fmt:message key="all"/></td>
	              </tr>
	             </table>
	            </td>
	           </tr>
	           <tr>
	            <td valign="top">
	              <table cellpadding="1" cellspacing="1" style="width:90%">
	                <tr>
			          <td>
			       		<select name="__field_id">
				    	  <option value="" ><c:out value="Select Field"/></option>
				    	  <c:forEach items="${model.productFieldList}" var="productField">
	  	     				<c:if test="${!empty productField.name and productField.enabled and productField.showOnDropDown}" >
	  	     				  <option value="${productField.id}" ><c:out value="${productField.name}"/></option>
			        	    </c:if>	
	  	    	    	  </c:forEach>	
				   		</select>
				      </td>
			        </tr>
			        <tr>
			          <td>
			            <fmt:message key="value" />: <input name="__field_value" type="text" size="15" style="width: 159px; margin-top: 5px;"/>
			          </td>
			        </tr>
			        
			       </table>
				   <div align="left" class="buttonLeft">
		             <input type="submit" name="__fieldEditBatcher" value="Apply" onClick="return optionsSelected()">
		           </div>
			     </td>
			   </tr>
	           </table>
			</div>
			<c:if test="${gSiteConfig['gPRESENTATION']}">
			  <div style="float:left;padding: 5px">
	           <table class="productBatchTool" cellpadding="5" cellspacing="5">
	           <tr>
	            <td valign="top">
	             <table cellpadding="1" cellspacing="1" width="100%">
	              <tr style="height:30px;white-space: nowrap">
	               <td valign="top" width="20px"><img class="toolTipImg" title="Presentation::Print the Presentation of product(s) in PDF format" src="../graphics/question.gif" /></td>
	               <td valign="top"><h3><fmt:message key="printPresentation" /></h3></td>
	               <td valign="top" align="right"><input type="checkbox" onclick="toggleAll(this)" name="checkAll"/> <fmt:message key="all"/></td>
	              </tr>
	             </table>
	            </td>
	           </tr>
	           <tr>
	            <td valign="top">
	              <table cellpadding="1" cellspacing="1" style="width:90%">
	                <tr>
			          <td>
			          <select name="__productPerPage">
					  <option value="1">1 product per page</option>
					   <option value="2">2 products per page</option>
					   <option value="3">3 products per page</option>
					   <option value="4">4 products per page</option>
					   <option value="5">5 products per page</option>
					   <option value="6">6 products per page</option>
					</select>
			          </td>
			        </tr>
			       </table>
				  <div align="left" class="buttonLeft">
		            <input type="submit" name="__presentationPdf" value="Batch Print"/>
		            </div>
			     </td>
			   </tr>
	           </table>
			</div>
			</c:if>
			<c:if test="${siteConfig['LUCENE_REINDEX'].value == 'true'}">
     		<div style="float:left;padding: 5px">
	        <table class="productBatchTool" cellpadding="5" cellspacing="5">
	            <tr>
	              <td valign="top">
	                <table cellpadding="1" cellspacing="1">
	                  <tr style="height:30px;white-space: nowrap">
	                    <td valign="top" width="20px"><img class="toolTipImg" title="Rank::Order the products in search result based on this rank. Rank 0 is for lowest ranking and 100 is for lowset ranking." src="../graphics/question.gif" /></td>
	               		<td valign="top"><h3><fmt:message key="rank" /></h3></td>
	              	    <td valign="top" align="right"><input type="checkbox" onclick="toggleAll(this)" name="checkAll"/> <fmt:message key="all"/></td>
	              	  </tr>
	             	</table>
	              </td>
	            </tr>
	            <tr>
	              <td valign="top">
	             	<table cellpadding="1" cellspacing="1">
	              	  <tr style="">
			       		<td>
			       		  <select name="__rank">
			       		    <c:forEach begin="1" end="100" var="rank">
			       		      <option value="${rank}" ><c:out value="${rank}"/></option>
				    		</c:forEach>
				    	  </select>
				   		  <div align="left" class="buttonLeft">
		            		<input type="submit" name="__updateRankingBacher" value="Apply" onClick="return optionsSelected()">
		         		  </div>
			       		</td>
			      	  </tr>
	                </table>
	              </td>
	            </tr>
	          </table>
			</div>
			</c:if>
			<c:if test="${gSiteConfig['gCUSTOMER_SUPPLIER']}">
			<div style="float:left;padding: 5px">
	          <table class="productBatchTool" cellpadding="5" cellspacing="5">
	           <tr>
	            <td valign="top">
	             <table cellpadding="1" cellspacing="1" width="100%">
	              <tr style="height:30px;white-space: nowrap">
	               <td valign="top" width="20px"><img class="toolTipImg" title="Assign Primary Supplier::Assign primary supplier for selected product(s) in the list." src="../graphics/question.gif" /></td>
	               <td valign="top"><h3><fmt:message key="primarySupplier" /></h3></td>
	               <td valign="top" align="right"><input type="checkbox" onclick="toggleAll(this)" name="checkAll"/> <fmt:message key="all"/></td>
	              </tr>
	             </table>
	            </td>
	           </tr>
	           <tr>
	            <td valign="top">
	              <table cellpadding="1" cellspacing="1" style="width:90%">
	                <tr>
			        <td>
			       		<select name="__supplier_id">
				    	  <option value="" ><c:out value="Select Supplier"/></option>
				    	  <c:forEach items="${model.suppliersList}" var="supplier">
	  	     				  <option value="${supplier.id}" ><c:out value="${supplier.address.company}"/></option>
	  	    	    	  </c:forEach>	
				   		</select>
				      </td>
			        </tr>
			        <tr>
			          <td><table>
			          <tr>
			          	<td>
			            <strong><fmt:message key="cost" />:</strong> <input name="__cost_value" type="text" size="15" style="width: 100px; margin-top: 0px;"/>
			          </td>
			          <td>
			          	<select name="__cost_percent" style="width : 50px; margin-top: 5px;">
			          		 <option value="true" >%</option>
			          		 <option value="false" ><fmt:message key="FIXED" /> $</option>
				   		</select>
			          </td>
			          </tr>
			          </table></td> 
			        </tr>
			        <tr>
			        	<td>
			            <fmt:message key="supplier" /> <fmt:message key="sku" />: <input name="__supplier_sku" type="text" size="15" style="width: 113px; margin-top: 0px;"/>
			          </td>
			        </tr>
			        
			       </table>
				   <div align="left" class="buttonLeft">
		             <input type="submit" name="__supplierEditBatcher" value="Apply" onClick="return optionsSelected()">
		           </div>
			     </td>
			   </tr>
	           </table>
			</div>
			</c:if>
			<div style="float:left;padding: 5px">
	        <table class="productBatchTool" cellpadding="5" cellspacing="5">
	            <tr>
	              <td valign="top">
	                <table cellpadding="1" cellspacing="1">
	                   <table cellpadding="1" cellspacing="1" width="100%">
	                    <td valign="top" width="20px"><img class="toolTipImg" title="Parent SKU::you can assign the product's parent by input sku." src="../graphics/question.gif" /></td>
	               		<td valign="top"><h3><fmt:message key="masterSku" /></h3></td>
	              	    <td valign="top" align="right"><input type="checkbox" onclick="toggleAll(this)" name="checkAll"/> <fmt:message key="all"/></td>
	              	  </tr>
	             	</table>
	              </td>
	            </tr>
	            <tr>
	              <td valign="top">
	             	<table cellpadding="1" cellspacing="1">
	              	  <tr style="">
			       		<td>
							<fmt:message key="masterSku" />: <input name="__parent_sku" type="text" size="15" style="width: 159px; margin-top: 0px;"/>
				   		  <div align="left" class="buttonLeft">
		            		<input type="submit" name="__updateParentSKUBacher" value="Apply" onClick="return optionsSelected()">
		         		  </div>
			       		</td>
			      	  </tr>
	                </table>
	              </td>
	            </tr>
	          </table>
			</div>
			
			<!-- Weight START -->
			<div style="float:left;padding: 5px">
	        <table class="productBatchTool" cellpadding="5" cellspacing="5">
	            <tr>
	              <td valign="top">
	                <table cellpadding="1" cellspacing="1">
	                   <table cellpadding="1" cellspacing="1" width="100%">
	                    <td valign="top" width="20px"><img class="toolTipImg" title="Weight::you can assign weight to selected products." src="../graphics/question.gif" /></td>
	               		<td valign="top"><h3><fmt:message key="weight" /></h3></td>
	              	    <td valign="top" align="right"><input type="checkbox" onclick="toggleAll(this)" name="checkAll"/> <fmt:message key="all"/></td>
	              	  </tr>
	             	</table>
	              </td>
	            </tr>
	            <tr>
	              <td valign="top">
	             	<table cellpadding="1" cellspacing="1">
	              	  <tr style="">
			       		<td>
							<fmt:message key="weight" />: <input name="__product_weight" type="text" size="15" style="width: 159px; margin-top: 0px;"/>
				   		  <div align="left" class="buttonLeft">
		            		<input type="submit" name="__updateWeightBacher" value="Apply" onClick="return optionsSelected()">
		         		  </div>
			       		</td>
			      	  </tr>
	                </table>
	              </td>
	            </tr>
	          </table>
			</div>
		<!-- Weight END-->
		
		<!-- Program START -->
			<div style="float:left;padding: 5px">
	        <table class="productBatchTool" cellpadding="5" cellspacing="5">
	            <tr>
	              <td valign="top">
	                <table cellpadding="1" cellspacing="1">
	                   <table cellpadding="1" cellspacing="1" width="100%">
	                    <td valign="top" width="20px"><img class="toolTipImg" title="Program::you can assign program to selected products." src="../graphics/question.gif" /></td>
	               		<td valign="top"><h3>Program</h3></td>
	              	    <td valign="top" align="right"><input type="checkbox" onclick="toggleAll(this)" name="checkAll"/> <fmt:message key="all"/></td>
	              	  </tr>
	             	</table>
	              </td>
	            </tr>
	            <tr>
	              <td valign="top">
	             	<table cellpadding="1" cellspacing="1">
	              	  <tr style="">
			       		<td>
							Program: <input name="__product_program" type="text" size="15" style="width: 159px; margin-top: 0px;"/>
				   		  <div align="left" class="buttonLeft">
		            		<input type="submit" name="__updateProgramBacher" value="Apply" onClick="return optionsSelected()">
		         		  </div>
			       		</td>
			      	  </tr>
	                </table>
	              </td>
	            </tr>
	          </table>
			</div>
		</div>
		<!-- Program END-->
		</div>
      </c:if>
    
  </td><td class="topr_g quickMode" ></td></tr>
  <tr><td class="boxmidlrg" >

 <!-- tabs -->
<div class="tab-wrapper">
	<h4 class="quickMode" title=""></h4>
	<div>
	<!-- start tab -->
	  	
	  	<%-- <div class="listdivi ln tabdivi"></div> --%>
	  	<div class="listdivi quickMode"></div>
	   
		    
		  	<div class="listlight">
		  	<!-- input field -->
			<table border="0" cellpadding="0" cellspacing="1" width="100%" class="quickMode">
			  <tr>
			  <td class="pageShowing">
			  <c:if test="${model.count > 0}">
			  <fmt:message key="showing">
				<fmt:param value="${productSearch.offset + 1}"/>
				<fmt:param value="${model.pageEnd}"/>
				<fmt:param value="${model.count}"/>
			  </fmt:message>
			  </c:if>
			  <c:if test="${model.categoryName != null}">
			    (Category - <c:out value="${model.categoryName}"/>)
			  </c:if>  
			  </td>
			  <td class="pageNavi">
			  Page 
			  <c:choose>
			  <c:when test="${model.pageCount <= 100 and model.pageCount > 0}">
			  <select name="page" id="page" onchange="submit()">
			  <c:forEach begin="1" end="${model.pageCount}" var="page">
			  	<option value="${page}" <c:if test="${page == (productSearch.page)}">selected</c:if>>${page}</option>
			  </c:forEach>
			  </select>
			  </c:when>
			  <c:otherwise>
			  <input type="text" id="page" name="page" value="${productSearch.page}" size="5" class="textfield50" />
			  <input type="submit" value="go"/>
			  </c:otherwise>
			  </c:choose>
			  of <c:out value="${model.pageCount}"/>
			  | 
			  <c:if test="${productSearch.page == 1}"><span class="pageNaviDead"><fmt:message key="previous" /></span></c:if>
			  <c:if test="${productSearch.page != 1}"><a href="<c:url value="productList.jhtm"><c:param name="page" value="${productSearch.page-1}"/><c:param name="category" value="${model.search['category']}"/><c:param name="type" value="${param.type}"/></c:url>" class="pageNaviLink"><fmt:message key="previous" /></a></c:if>
			  | 
			  <c:if test="${productSearch.page == model.pageCount}"><span class="pageNaviDead"><fmt:message key="next" /></span></c:if>
			  <c:if test="${productSearch.page != model.pageCount}"><a href="<c:url value="productList.jhtm"><c:param name="page" value="${productSearch.page+1}"/><c:param name="category" value="${model.search['category']}"/><c:param name="type" value="${param.type}"/></c:url>" class="pageNaviLink"><fmt:message key="next" /></a></c:if>
			  </td>
			  </tr>
			</table>
			<input type="hidden" name="type" value="${param.type}" />
			
			
			<table border="0" cellpadding="0" cellspacing="0" width="100%" class="listings" id="stickyList">
			  <tr class="listingsHdr2NoImage">
			<c:if test="${model.count > 0}">
			    <td align="center" class="quickMode"><input type="checkbox" onclick="toggleAll(this)"></td>
			</c:if>
			    <td class="indexCol quickMode">&nbsp;</td>
			     <td align="center">
			    	<table cellspacing="0" cellpadding="1">
			    	  <tr>
			    	    <c:choose>
			    	      <c:when test="${productSearch.sort == 'sku desc'}">
			    	        <td class="listingsHdr3WithWrap">
			    	        <a class="hdrLink" href="#" onClick="document.getElementById('sort').value='sku';document.getElementById('list').submit()"><fmt:message key="productSku" /></a>
			    	        </td>
			    	        <td class="quickModeRemove"><img src="../graphics/up.gif" border="0"></td>
			    	      </c:when>
			    	      <c:when test="${productSearch.sort == 'sku'}">
			    	        <td class="listingsHdr3WithWrap">
			    	        <a class="hdrLink" href="#" onClick="document.getElementById('sort').value='sku desc';document.getElementById('list').submit()"><fmt:message key="productSku" /></a>
			    	        </td>
			    	        <td class="quickModeRemove"><img src="../graphics/down.gif" border="0"></td>
			    	      </c:when>
			    	      <c:otherwise>
			    	        <td colspan="2" class="listingsHdr3WithWrap">
			    	        <a class="hdrLink" href="#" onClick="document.getElementById('sort').value='sku desc';document.getElementById('list').submit()"><fmt:message key="productSku" /></a>
			    	        </td>
			    	      </c:otherwise>
			    	    </c:choose>  
			    	  </tr>
			    	</table>
			    </td>
			    <td align="center">&nbsp</td>
			    <td>
			    	<table cellspacing="0" cellpadding="1">
			    	  <tr>
			    	    <c:choose>
			    	      <c:when test="${productSearch.sort == 'name desc'}">
			    	        <td class="listingsHdr3WithWrap">
			    	        <a class="hdrLink" href="#" onClick="document.getElementById('sort').value='name';document.getElementById('list').submit()"><fmt:message key="productName" /></a>
			    	        </td>
			    	        <td class="quickModeRemove"><img src="../graphics/up.gif" border="0"></td>
			    	      </c:when>
			    	      <c:when test="${productSearch.sort == 'name'}">
			    	        <td class="listingsHdr3WithWrap">
			    	        <a class="hdrLink" href="#" onClick="document.getElementById('sort').value='name desc';document.getElementById('list').submit()"><fmt:message key="productName" /></a>
			    	        </td>
			    	        <td class="quickModeRemove"><img src="../graphics/down.gif" border="0"></td>
			    	      </c:when>
			    	      <c:otherwise>
			    	        <td rowspan="2" colspan="2" class="listingsHdr3WithWrap">
			    	        <a class="hdrLink" href="#" onClick="document.getElementById('sort').value='name desc';document.getElementById('list').submit()"><fmt:message key="productName" /></a>
			    	        </td>
			    	      </c:otherwise>
			    	    </c:choose>
			    	  </tr>
			    	</table>     
			    </td>
			    <td class="quickMode">&nbsp;</td>
			    <td align="center">
			    	<table cellspacing="0" cellpadding="1">
			    	  <tr>
			    	    <c:choose>
			    	      <c:when test="${productSearch.sort == 'master_sku desc'}">
			    	        <td class="listingsHdr3WithWrap">
			    	        <a class="hdrLink" href="#" onClick="document.getElementById('sort').value='master_sku';document.getElementById('list').submit()"><fmt:message key="masterSku" /></a>
			    	        </td>
			    	        <td class="quickModeRemove"><img src="../graphics/up.gif" border="0"></td>
			    	      </c:when>
			    	      <c:when test="${productSearch.sort == 'master_sku'}">
			    	        <td class="listingsHdr3WithWrap">
			    	        <a class="hdrLink" href="#" onClick="document.getElementById('sort').value='master_sku desc';document.getElementById('list').submit()"><fmt:message key="masterSku" /></a>
			    	        </td>
			    	        <td class="quickModeRemove"><img src="../graphics/down.gif" border="0"></td>
			    	      </c:when>
			    	      <c:otherwise>
			    	        <td colspan="2" class="listingsHdr3WithWrap">
			    	        <a class="hdrLink" href="#" onClick="document.getElementById('sort').value='master_sku desc';document.getElementById('list').submit()"><fmt:message key="masterSku" /></a>
			    	        </td>
			    	      </c:otherwise>
			    	    </c:choose>  
			    	  </tr>
			    	</table>
			    </td>
			    <td class="quickMode">Report</td>
			    <td align="right">
			    	<table cellspacing="0" cellpadding="1">
			    	  <tr>
			    	    <c:choose>
			    	      <c:when test="${productSearch.sort == 'weight desc'}">
			    	        <td class="listingsHdr3WithWrapWithWrap">
			    	        <a class="hdrLink" href="#" onClick="document.getElementById('sort').value='weight';document.getElementById('list').submit()"><fmt:message key="productWeight" /></a>
			    	        </td>
			    	        <td class="quickModeRemove"><img src="../graphics/up.gif" border="0"></td>
			    	      </c:when>
			    	      <c:when test="${productSearch.sort == 'weight'}">
			    	        <td class="listingsHdr3WithWrapWithWrap">
			    	        <a class="hdrLink" href="#" onClick="document.getElementById('sort').value='weight desc';document.getElementById('list').submit()"><fmt:message key="productWeight" /></a>
			    	        </td>
			    	        <td class="quickModeRemove"><img src="../graphics/down.gif" border="0"></td>
			    	      </c:when>
			    	      <c:otherwise>
			    	        <td colspan="2" class="listingsHdr3WithWrapWithWrap">
			    	        <a class="hdrLink" href="#" onClick="document.getElementById('sort').value='weight desc';document.getElementById('list').submit()"><fmt:message key="productWeight" /></a>
			    	        </td>
			    	      </c:otherwise>
			    	    </c:choose>  
			    	  </tr>
			    	</table>
			    </td>
			    <td align="center" class="quickMode">
			    	<table cellspacing="0" cellpadding="1">
			    	  <tr>
			    	    <td rowspan="2" class="listingsHdr3WithWrap"># Units</td>
			       	  </tr>
			    	</table>
			    </td>
			    <td align="center" class="quickMode" style="width : 20px">
			    	<table cellspacing="0" cellpadding="1">
			    	  <tr>
			    	    <td rowspan="2" class="listingsHdr3WithWrap" >#1: Packing</td>
			       	  </tr>
			    	</table>
			    </td>
			    <td align="center" class="quickMode" style="width : 20px">
			    	<table cellspacing="0" cellpadding="1">
			    	  <tr>
			    	    <td rowspan="2" class="listingsHdr3WithWrap" >#24: Condition</td>
			       	  </tr>
			    	</table>
			    </td>
			    <td align="center" class="quickMode" style="width : 20px">
			    	<table cellspacing="0" cellpadding="1">
			    	  <tr>
			    	    <td rowspan="2" class="listingsHdr3WithWrap" >#14: Manifested</td>
			       	  </tr>
			    	</table>
			    </td>
			    <td align="center" class="quickMode" style="width : 20px"> 
			    	<table cellspacing="0" cellpadding="1">
			    	  <tr>
			    	    <td rowspan="2" class="listingsHdr3WithWrap" >#15: # of Pallets</td>
			       	  </tr>
			    	</table>
			    </td>
			    <td align="center" class="quickMode" style="width : 20px"> 
			    	<table cellspacing="0" cellpadding="1">
			    	  <tr>
			    	    <td rowspan="2" class="listingsHdr3WithWrap" >#13: FOB</td>
			       	  </tr>
			    	</table>
			    </td>
			    <td align="center" class="quickMode" style="width : 20px">
			    	<table cellspacing="0" cellpadding="1">
			    	  <tr>
			    	    <td rowspan="2" class="listingsHdr3WithWrap" >#19: PT Code</td>
			       	  </tr>
			    	</table>
			    </td>
   			    <td align="center" class="quickMode" style="width : 20px">
			    	<table cellspacing="0" cellpadding="1">
			    	  <tr>
			    	    <td rowspan="2" class="listingsHdr3WithWrap" >#32: Ribbons & Tags</td>
			       	  </tr>
			    	</table>
			    </td>
			    <td align="center">
			    	<table cellspacing="0" cellpadding="1">
			    	  <tr>
			    	    <c:choose>
			    	      <c:when test="${productSearch.sort == 'packing desc'}">
			    	        <td class="listingsHdr3WithWrap">
			    	        <a class="hdrLink" href="#" onClick="document.getElementById('sort').value='packing';document.getElementById('list').submit()"><fmt:message key="packing" /></a>
			    	        </td>
			    	        <td class="quickModeRemove"><img src="../graphics/up.gif" border="0"></td>
			    	      </c:when>
			    	      <c:when test="${productSearch.sort == 'packing'}">
			    	        <td class="listingsHdr3WithWrap">
			    	        <a class="hdrLink" href="#" onClick="document.getElementById('sort').value='packing desc';document.getElementById('list').submit()"><fmt:message key="packing" /></a>
			    	        </td>
			    	        <td class="quickModeRemove"><img src="../graphics/down.gif" border="0"></td>
			    	      </c:when>
			    	      <c:otherwise>
			    	        <td colspan="2" class="listingsHdr3WithWrap">
			    	        <a class="hdrLink" href="#" onClick="document.getElementById('sort').value='packing desc';document.getElementById('list').submit()"><fmt:message key="packing" /></a>
			    	        </td>
			    	      </c:otherwise>
			    	    </c:choose>  
			    	  </tr>
			    	</table>
			    </td>
			    <td align="center">
			    	<table cellspacing="0" cellpadding="1">
			    	  <tr>
			    	    <c:choose>
			    	      <c:when test="${productSearch.sort == 'case_content desc'}">
			    	        <td class="listingsHdr3WithWrap">
			    	        <a class="hdrLink" href="#" onClick="document.getElementById('case_content').value='price_1';document.getElementById('list').submit()"><fmt:message key="caseContent" /></a>
			    	        </td>
			    	        <td class="quickModeRemove"><img src="../graphics/up.gif" border="0"></td>
			    	      </c:when>
			    	      <c:when test="${productSearch.sort == 'case_content'}">
			    	        <td class="listingsHdr3WithWrap">
			    	        <a class="hdrLink" href="#" onClick="document.getElementById('case_content').value='price_1 desc';document.getElementById('list').submit()"><fmt:message key="caseContent" /></a>
			    	        </td>
			    	        <td class="quickModeRemove"><img src="../graphics/down.gif" border="0"></td>
			    	      </c:when>
			    	      <c:otherwise>
			    	        <td colspan="2" class="listingsHdr3WithWrap">
			    	        <a class="hdrLink" href="#" onClick="document.getElementById('case_content').value='price_1 desc';document.getElementById('list').submit()"><fmt:message key="caseContent" /></a>
			    	        </td>
			    	      </c:otherwise>
			    	    </c:choose>  
			    	  </tr>
			    	</table>
			    </td>
			    <td align="center">
			    	<table cellspacing="0" cellpadding="1">
			    	  <tr>
			    	    <c:choose>
			    	      <c:when test="${productSearch.sort == 'price_1 desc'}">
			    	        <td class="listingsHdr3WithWrap">
			    	        <a class="hdrLink" href="#" onClick="document.getElementById('sort').value='price_1';document.getElementById('list').submit()"><fmt:message key="productPrice" /></a>
			    	        </td>
			    	        <td class="quickModeRemove"><img src="../graphics/up.gif" border="0"></td>
			    	      </c:when>
			    	      <c:when test="${productSearch.sort == 'price_1'}">
			    	        <td class="listingsHdr3WithWrap">
			    	        <a class="hdrLink" href="#" onClick="document.getElementById('sort').value='price_1 desc';document.getElementById('list').submit()"><fmt:message key="productPrice" /></a>
			    	        </td>
			    	        <td class="quickModeRemove"><img src="../graphics/down.gif" border="0"></td>
			    	      </c:when>
			    	      <c:otherwise>
			    	        <td colspan="2" class="listingsHdr3WithWrap">
			    	        <a class="hdrLink" href="#" onClick="document.getElementById('sort').value='price_1 desc';document.getElementById('list').submit()"><fmt:message key="productPrice" /></a>
			    	        </td>
			    	      </c:otherwise>
			    	    </c:choose>  
			    	  </tr>
			    	</table>
			    </td>
			    <c:if test="${gSiteConfig['gINVENTORY']}">
			    <td align="center">
			    	<table cellspacing="0" cellpadding="1">
			    	  <tr>
			    	    <c:choose>
			    	      <c:when test="${productSearch.sort == 'inventory desc'}">
			    	        <td class="listingsHdr3WithWrap">
			    	        <a class="hdrLink" href="#" onClick="document.getElementById('sort').value='inventory';document.getElementById('list').submit()"><c:if test="${siteConfig['INVENTORY_SHOW_ONHAND'].value=='true'}"><fmt:message key="inventory_onhand" />/</c:if><fmt:message key="available_for_sale" /></a>
			    	        </td>
			    	        <td class="quickModeRemove"><img src="../graphics/up.gif" border="0"></td>
			    	      </c:when>
			    	      <c:when test="${productSearch.sort == 'inventory'}">
			    	        <td class="listingsHdr3WithWrap">
			    	        <a class="hdrLink" href="#" onClick="document.getElementById('sort').value='inventory desc';document.getElementById('list').submit()"><c:if test="${siteConfig['INVENTORY_SHOW_ONHAND'].value=='true'}"><fmt:message key="inventory_onhand" />/</c:if><fmt:message key="available_for_sale" /></a>
			    	        </td>
			    	        <td class="quickModeRemove"><img src="../graphics/down.gif" border="0"></td>
			    	      </c:when>
			    	      <c:otherwise>
			    	        <td colspan="2" class="listingsHdr3WithWrap">
			    	        <a class="hdrLink" href="#" onClick="document.getElementById('sort').value='inventory desc';document.getElementById('list').submit()"><c:if test="${siteConfig['INVENTORY_SHOW_ONHAND'].value=='true'}"><fmt:message key="inventory_onhand" />/</c:if><fmt:message key="available_for_sale" /></a>
			    	        </td>
			    	      </c:otherwise>
			    	    </c:choose>  
			    	  </tr>
			    	</table>
			    </td>
			    </c:if>
			    <td align="center">
			    	<table cellspacing="0" cellpadding="1">
			    	  <tr>
			    	    <c:choose>
			    	      <c:when test="${productSearch.sort == 'created_by desc'}">
			    	        <td class="listingsHdr3WithWrap">
			    	        <a class="hdrLink" href="#" onClick="document.getElementById('sort').value='created_by';document.getElementById('list').submit()"><fmt:message key="createdBy" /></a>
			    	        </td>
			    	        <td class="quickModeRemove"><img src="../graphics/up.gif" border="0"></td>
			    	      </c:when>
			    	      <c:when test="${productSearch.sort == 'created_by'}">
			    	        <td class="listingsHdr3WithWrap">
			    	        <a class="hdrLink" href="#" onClick="document.getElementById('sort').value='created_by desc';document.getElementById('list').submit()"><fmt:message key="createdBy" /></a>
			    	        </td>
			    	        <td class="quickModeRemove"><img src="../graphics/down.gif" border="0"></td>
			    	      </c:when>
			    	      <c:otherwise>
			    	        <td colspan="2" class="listingsHdr3WithWrap">
			    	        <a class="hdrLink" href="#" onClick="document.getElementById('sort').value='created_by desc';document.getElementById('list').submit()"><fmt:message key="createdBy" /></a>
			    	        </td>
			    	      </c:otherwise>
			    	    </c:choose>  
			    	  </tr>
			    	</table>
			    </td>
			    <td align="center">
			    	<table cellspacing="0" cellpadding="1">
			    	  <tr>
			    	    <c:choose>
			    	      <c:when test="${productSearch.sort == 'created desc'}">
			    	        <td class="listingsHdr3WithWrap">
			    	        <a class="hdrLink" href="#" onClick="document.getElementById('sort').value='created';document.getElementById('list').submit()"><fmt:message key="dateAdded" /></a>
			    	        </td>
			    	        <td class="quickModeRemove"><img src="../graphics/up.gif" border="0"></td>
			    	      </c:when>
			    	      <c:when test="${productSearch.sort == 'created'}">
			    	        <td class="listingsHdr3WithWrap">
			    	        <a class="hdrLink" href="#" onClick="document.getElementById('sort').value='created desc';document.getElementById('list').submit()"><fmt:message key="dateAdded" /></a>
			    	        </td>
			    	        <td class="quickModeRemove"><img src="../graphics/down.gif" border="0"></td>
			    	      </c:when>
			    	      <c:otherwise>
			    	        <td colspan="2" class="listingsHdr3WithWrap">
			    	        <a class="hdrLink" href="#" onClick="document.getElementById('sort').value='created desc';document.getElementById('list').submit()"><fmt:message key="dateAdded" /></a>
			    	        </td>
			    	      </c:otherwise>
			    	    </c:choose>  
			    	  </tr>
			    	</table>
			    </td>
			    <sec:authorize ifAnyGranted="ROLE_AEM,ROLE_ADMIN_${prop_site_id},ROLE_PRODUCT_VIEW_LISTXX">
			    <td align="center">
			    	<table cellspacing="0" cellpadding="1">
			    	  <tr>
			    	    <c:choose>
			    	      <c:when test="${productSearch.sort == 'last_modified desc'}">
			    	        <td class="listingsHdr3WithWrap">
			    	        <a class="hdrLink" href="#" onClick="document.getElementById('sort').value='last_modified';document.getElementById('list').submit()"><fmt:message key="lastModified" /></a>
			    	        </td>
			    	        <td class="quickModeRemove"><img src="../graphics/up.gif" border="0"></td>
			    	      </c:when>
			    	      <c:when test="${productSearch.sort == 'last_modified'}">
			    	        <td class="listingsHdr3WithWrap">
			    	        <a class="hdrLink" href="#" onClick="document.getElementById('sort').value='last_modified desc';document.getElementById('list').submit()"><fmt:message key="lastModified" /></a>
			    	        </td>
			    	        <td class="quickModeRemove"><img src="../graphics/down.gif" border="0"></td>
			    	      </c:when>
			    	      <c:otherwise>
			    	        <td colspan="2" class="listingsHdr3WithWrap">
			    	        <a class="hdrLink" href="#" onClick="document.getElementById('sort').value='last_modified desc';document.getElementById('list').submit()"><fmt:message key="lastModified" /></a>
			    	        </td>
			    	      </c:otherwise>
			    	    </c:choose>  
			    	  </tr>
			    	</table>
			    </td>			   
			    <c:if test="${gSiteConfig['gSUPPLIER']}">
			     <td align="center">
			    	<table cellspacing="0" cellpadding="1">
			    	  <tr>
			    	    <c:choose>
			    	      <c:when test="${productSearch.sort == 'default_supplier_id desc'}">
			    	        <td class="listingsHdr3WithWrap">
			    	        <a class="hdrLink" href="#" onClick="document.getElementById('sort').value='default_supplier_id';document.getElementById('list').submit()"><fmt:message key="supplier" /></a>
			    	        </td>
			    	        <td class="quickModeRemove"><img src="../graphics/up.gif" border="0"></td>
			    	      </c:when>
			    	      <c:when test="${productSearch.sort == 'default_supplier_id'}">
			    	        <td class="listingsHdr3WithWrap">
			    	        <a class="hdrLink" href="#" onClick="document.getElementById('sort').value='default_supplier_id desc';document.getElementById('list').submit()"><fmt:message key="supplier" /></a>
			    	        </td>
			    	        <td class="quickModeRemove"><img src="../graphics/down.gif" border="0"></td>
			    	      </c:when>
			    	      <c:otherwise>
			    	        <td colspan="2" class="listingsHdr3WithWrap">
			    	        <a class="hdrLink" href="#" onClick="document.getElementById('sort').value='default_supplier_id desc';document.getElementById('list').submit()"><fmt:message key="supplier" /></a>
			    	        </td>
			    	      </c:otherwise>
			    	    </c:choose>  
			    	  </tr>
			    	</table>
			      </td>
			      <td align="center">
			    	<table cellspacing="0" cellpadding="1">
			    	  <tr>
			    	    <c:choose>
			    	      <c:when test="${productSearch.sort == 'cost desc'}">
			    	        <td class="listingsHdr3WithWrap">
			    	        <a class="hdrLink" href="#" onClick="document.getElementById('sort').value='cost';document.getElementById('list').submit()"><fmt:message key="cost" /></a>
			    	        </td>
			    	        <td class="quickModeRemove"><img src="../graphics/up.gif" border="0"></td>
			    	      </c:when>
			    	      <c:when test="${productSearch.sort == 'cost'}">
			    	        <td class="listingsHdr3WithWrap">
			    	        <a class="hdrLink" href="#" onClick="document.getElementById('sort').value='cost desc';document.getElementById('list').submit()"><fmt:message key="cost" /></a>
			    	        </td>
			    	        <td class="quickModeRemove"><img src="../graphics/down.gif" border="0"></td>
			    	      </c:when>
			    	      <c:otherwise>
			    	        <td colspan="2" class="listingsHdr3WithWrap">
			    	        <a class="hdrLink" href="#" onClick="document.getElementById('sort').value='cost desc';document.getElementById('list').submit()"><fmt:message key="cost" /></a>
			    	        </td>
			    	      </c:otherwise>
			    	    </c:choose>  
			    	  </tr>
			    	</table>
			      </td>
			   		<td align="center" class="quickMode">
			    	<table cellspacing="0" cellpadding="1">
			    	  <tr>
			    	    <td rowspan="2" class="listingsHdr3WithWrap"><fmt:message key="supplier" /></td>
			       	  </tr>
			    	</table>
			     	</td> 
			    </c:if>
			    </sec:authorize>
			    <c:if test="${model.categoryName != null}">
			      <td align="center" class="quickMode">
			    	<table cellspacing="0" cellpadding="1">
			    	  <tr>
			    	    <c:choose>
			    	      <c:when test="${productSearch.sort == 'rank desc'}">
			    	        <td class="listingsHdr3WithWrap">
			    	        <a class="hdrLink" href="#" onClick="document.getElementById('sort').value='rank';document.getElementById('list').submit()"><fmt:message key="rank" /></a>
			    	        </td>
			    	        <td class="quickModeRemove"><img src="../graphics/up.gif" border="0"></td>
			    	      </c:when>
			    	      <c:when test="${productSearch.sort == 'rank'}">
			    	        <td class="listingsHdr3WithWrap">
			    	        <a class="hdrLink" href="#" onClick="document.getElementById('sort').value='rank desc';document.getElementById('list').submit()"><fmt:message key="rank" /></a>
			    	        </td>
			    	        <td class="quickModeRemove"><img src="../graphics/down.gif" border="0"></td>
			    	      </c:when>
			    	      <c:otherwise>
			    	        <td colspan="2" class="listingsHdr3WithWrap">
			    	        <a class="hdrLink" href="#" onClick="document.getElementById('sort').value='rank desc';document.getElementById('list').submit()"><fmt:message key="rank" /></a>
			    	        </td>
			    	      </c:otherwise>
			    	    </c:choose>  
			    	  </tr>
			    	</table>
			    </td>
			    </c:if>
			  </tr>
			<c:forEach items="${model.products}" var="product" varStatus="status">
			  <tr class="row${status.index % 2}" onmouseover="changeStyleClass(this,'rowOver')" onmouseout="changeStyleClass(this,'row${status.index % 2}')">
			<c:if test="${model.count > 0}">
			    <td align="center" class="quickMode"><input name="__selected_id" value="${product.id}" type="checkbox">
			    <input type="hidden" value="${product.sku}" name="__selected_sku_${product.id}"/>
			    </td>
			</c:if>
			    <td class="indexCol quickMode"><c:out value="${status.count + productSearch.offset}"/>.</td>
			    <td align="center"><c:out value="${product.sku}" escapeXml="false"/></td>
			    <td align="center"><a href="${product.redirectUrlEn}" target="_blank"><img class="quickMode" style="padding-left:2px;" src="../graphics/eye.png" border="0"/></a></td>
			    <td class="nameCol"><a href="product.jhtm?id=${product.id}&category=${model.search['category']}" class="nameLink<c:if test="${!product.active}">Inactive</c:if>"><c:out value="${product.name}" escapeXml="false" /></a>
					<c:if test="${gSiteConfig['gSUBSCRIPTION'] and product.subscription}"><span class="sup">&sup1;</span><c:set var="hasSubscription" value="true"/></c:if>
			    </td>
			    <td class="quickMode">
			      <c:if test="${gSiteConfig['gPRODUCT_REVIEW'] or siteConfig['PRODUCT_RATE'].value == 'true'}">
				    <a href="productReviewList.jhtm?sku=${product.sku}" ><img src="../graphics/rate.gif" border="0"></a>
				  </c:if>
			      <c:if test="${product.thumbnail != null}">
				    <div class="productThumbWrapper"><a href="<c:if test="${not product.thumbnail.absolute}">../../assets/Image/Product/detailsbig/</c:if><c:out value="${product.thumbnail.imageUrl}"/>" onclick="window.open(this.href,'','width=600,height=300,resizable=yes'); return false;">
				      <img class="productThumb" src="<c:if test="${not product.thumbnail.absolute}">../../assets/Image/Product/detailsbig/</c:if><c:out value="${product.thumbnail.imageUrl}"/>" border="0">
			        </a></div>
				  </c:if>
			      <c:if test="${gSiteConfig['gPROTECTED'] > 0 and product.protectedLevelAsNumber != '0'}">
			      <img src="../graphics/padlock.gif" border="0"><sup><c:out value="${product.protectedLevelAsNumber}"/></sup>
			      </c:if>
			    </td>
			    <td class="idCol" align="center"><c:out value="${product.masterSku}"/></td>
			    <td align="center"><a href="../../admin/reports/productListReport.jhtm?sku=${product.sku}">Report</a></td>
			    <td align="center"><fmt:formatNumber value="${product.weight}" pattern="#,##0.##" /></td>
				<td align="center"><c:out value="${product.field7}" escapeXml="false"/></td>
			    <td align="center"><c:out value="${product.field1}" escapeXml="false"/></td>
			    <td align="center"><c:out value="${product.field24}" escapeXml="false"/></td>
			    <td align="center"><c:out value="${product.field14}" escapeXml="false"/></td>
			    <td align="center"><c:out value="${product.field15}" escapeXml="false"/></td>
			    <td align="center"><c:out value="${product.field13}" escapeXml="false"/></td>
			    <td align="center"><c:out value="${product.field19}" escapeXml="false"/></td>
			    <td align="center"><c:out value="${product.field32}" escapeXml="false"/></td>
			    <td align="center"><c:out value="${product.packing}" escapeXml="false"/></td>
			    <td align="center"><c:out value="${product.caseContent}" escapeXml="false"/></td>
			    <td align="center"><fmt:formatNumber value="${product.price1}" pattern="$##,###.##" /></td>
			    <c:if test="${gSiteConfig['gINVENTORY']}">
			      <c:choose>
			      <c:when test="${product.inventory != null}">
			      <td align="center"><b class="red"><a href="#${product.id}" rel="type:element" id="mbInventory${product.id}" class="mbInventory"><c:if test="${siteConfig['INVENTORY_SHOW_ONHAND'].value=='true'}"><c:out value="${product.inventory}/ " /></c:if><c:out value="${product.inventoryAFS}" /></a></b></td>
			      <td align="center" style="display:none"><b class="red"><a href="#${product.id}" rel="type:element" id="mbInventory${product.id}AFS" class="mbInventoryAFS"><c:if test="${siteConfig['INVENTORY_SHOW_ONHAND'].value=='true'}"><c:out value="${product.inventory}/ " /></c:if><c:out value="${product.inventoryAFS}" /></a></b></td>
			      	
			      	
			      	<input type="hidden" value="<c:out value='${product.inventory}'/>" id="inventory_${product.id}"/>
	    	  		<input type="hidden" value="<c:out value='${product.sku}'/>" id="sku_${product.id}"/>
	    	  		<div id="${product.id}" class="miniWindowWrapper inventoryAdj displayNone quickMode">
		  			
	    			  <div class="header"><fmt:message key="adjustInventory"/></div>
	    	  		<fieldset class="top">
	    	    		<label class="AStitle"><fmt:message key="sku"/></label>
	    	    		<input name="__sku_" type="text" value="<c:out value='${product.sku}'/>" size="15"  disabled="disabled"/>
	    	  		</fieldset>
	    	  		<c:if test="${siteConfig['INVENTORY_SHOW_ONHAND'].value=='true'}">
	    	  		<fieldset class="bottom">
	    	    		<label class="AStitle"><fmt:message key="inventory_onhand"/></label>
	    	    		<input name="__inventory_" type="text" value="<c:out value='${product.inventory}'/>" size="15"  disabled="disabled"/>
	    	  		</fieldset>
	    	  		</c:if>
	    	  		<fieldset class="bottom">
	    	    		<label class="AStitle"><fmt:message key="available_for_sale"/></label>
	    	    		<input name="__inventory_" type="text" value="<c:out value='${product.inventoryAFS}'/>" size="15"  disabled="disabled"/>
	    	  		</fieldset>
	    	  		<fieldset class="bottom">
	    	    		<label class="AStitle"><fmt:message key="adjust"/></label>
	    	    		<input name="__adjust_qty_" type="text" size="15" id="adjustQty_${product.id}" onkeyup="assignValue(this.value, 'adjustQty', ${product.id});"/>
	    	  		</fieldset>
	    	  		<fieldset class="bottom">
	    	    		<label class="AStitle"><fmt:message key="note"/></label>
	    	    		<textarea name="__adjust_note_" id="adjustNote_${product.id}" onkeyup="assignValue(this.value, 'adjustNote', ${product.id});"></textarea>
	    	    	</fieldset>	
	    	    	<c:if test="${gSiteConfig['gSITE_DOMAIN'] == 'viatrading.com' or gSiteConfig['gSITE_DOMAIN'] == 'test.com'}">
	    	  		<fieldset class="bottom">
	    	    		<label class="AStitleLong">Adjust Available For Sale Only</label>
	    	    		<input name="__adjust_AvForSale_" id="adjustAvForSale_${product.id}" type="checkbox"  onclick="assignValue(this.value, 'adjustAvForSale', ${product.id});"/> 
	    	    	</fieldset>
	    	    	</c:if>    
	    	  		<fieldset>
	    	     		<div class="button">
	      					<input type="submit" value="Adjust"  name="__adjust_inventory_" onclick="adjustInventory(${product.id});"/>
	      					<span style="float: right; padding-top: 10px;"><c:if test="${siteConfig['INVENTORY_HISTORY'].value == 'true'}"><a href="../reports/inventoryActivityReport.jhtm?sku=${product.sku}">view history</a></c:if></span>
      			 		</div>
	    	  		</fieldset>
		  			</div>
			     </c:when>
			     <c:otherwise>
			     	<td align="center"><b class="red"><a href="#${product.id}" rel="type:element" id="mbInventory${product.id}" class="mbInventory"><img border="0" src="../graphics/edit.gif" align="center"/></b></td>
			    	<input type="hidden" value="<c:out value='${product.inventory}'/>" id="inventory_${product.id}"/>
	    	  		<input type="hidden" value="<c:out value='${product.sku}'/>" id="sku_${product.id}"/>
	    	  		<div id="${product.id}" class="miniWindowWrapper inventoryAdj displayNone quickMode">
		  			
	    			<div class="header"><fmt:message key="initializeInventory"/></div>
	    	  		<fieldset class="top">
	    	    		<label class="AStitle"><fmt:message key="sku"/></label>
	    	    		<input name="__sku_" type="text" value="<c:out value='${product.sku}'/>" size="15"  disabled="disabled"/>
	    	  		</fieldset>
	    	  		<fieldset class="bottom">
	    	    		<label class="AStitle"><fmt:message key="initialInventory"/></label>
	    	    		<input name="__adjust_qty_" type="text" size="15" id="adjustQty_${product.id}" onkeyup="assignValue(this.value, 'adjustQty', ${product.id});"/>
	    	  		</fieldset>
	    	  		<fieldset class="bottom">
	    	    		<label class="AStitle"><fmt:message key="note"/></label>
	    	    		<textarea name="__adjust_note_" id="adjustNote_${product.id}" onkeyup="assignValue(this.value, 'adjustNote', ${product.id});"></textarea>
	    	    	</fieldset>
	    	  		<fieldset>
	    	     		<div class="button">
	      					<input type="submit" value="<fmt:message key="initialize"/>"  name="__adjust_inventory_" onclick="adjustInventory(${product.id});"/>
      			 		</div>
	    	  		</fieldset>
		  			</div>
			     </c:otherwise>
			     </c:choose>
			    </c:if>
			    <td align="center"><c:out value="${product.createdBy}" escapeXml="false"/></td>
			    <td align="center"><fmt:formatDate type="date" value="${product.created}" pattern="M/dd/yy"/></td>
			    <sec:authorize ifAnyGranted="ROLE_AEM,ROLE_ADMIN_${prop_site_id},ROLE_PRODUCT_VIEW_LISTXX">
			    <td align="center"><fmt:formatDate type="date" value="${product.lastModified}" pattern="M/dd/yy"/></td>
			    
			    <c:if test="${gSiteConfig['gSUPPLIER']}">
			    	<td align="center"><c:out value="${product.defaultSupplierName}" escapeXml="false"/></td>
			    	<c:choose>
			    	<c:when test="${product.cost != null}">
			    		<c:choose>
							<c:when test ="${product.costPercent == true}">
								<td align="center"><fmt:formatNumber value="${product.cost}" pattern="#,##0.00" />%</td>
							</c:when>
							<c:otherwise>
								<td align="center"><fmt:formatNumber value="${product.cost}" pattern="$##,###.##" /></td>							
							</c:otherwise>
						</c:choose>		
					</c:when>
					<c:otherwise>
					<td align="center">&nbsp</td>
					</c:otherwise>
					</c:choose>
			    	<c:set var="productSKU" value="${product.sku}" />
			     	 <%--<td align="center"><c:if test="${product.sku != null}"><a href="productSupplierList.jhtm?sku=<%out.print(URLEncoder.encode((String) pageContext.getAttribute("productSKU"), "UTF-8"));%>" class="nameLink"><fmt:message key="supplier" /></a></c:if></td> --%>
			     	 <td align="center" class="quickMode"><c:if test="${product.sku != null}"><a href="productSupplierList.jhtm?sku=${wj:urlEncode(productSKU)}" class="nameLink"><fmt:message key="supplier" /></a></c:if></td> 	    
			    </c:if>
			    </sec:authorize>
			    <c:if test="${model.categoryName != null}">
			      <td align="center" class="quickMode"><input name="__rank_${product.id}" type="text" value="${product.rank}" size="5" maxlength="5" class="rankField"></td>
			    </c:if>
			  </tr>
			</c:forEach>
			<c:if test="${model.count == 0}">
			  <tr class="emptyList"><td colspan="6">&nbsp;</td></tr>
			</c:if>
			</table>
			
			<c:if test="${hasSubscription}">
				<div class="sup quickMode">
					&sup1; <fmt:message key="subscription" />
				</div>
			</c:if>
			
			<!-- pagination -->
			<table border="0" cellpadding="0" cellspacing="1" width="100%" class="quickMode">
			  <tr>
			  <td class="pageShowing">
			  <c:if test="${model.count > 0}">
			  <fmt:message key="showing">
				<fmt:param value="${productSearch.offset + 1}"/>
				<fmt:param value="${model.pageEnd}"/>
				<fmt:param value="${model.count}"/>
			  </fmt:message>
			  </c:if>
			  <c:if test="${model.categoryName != null}">
			    (Category - <c:out value="${model.categoryName}"/>)
			  </c:if>  
			  </td>
			  <td class="pageNavi">
			  <a href="#top">Top</a>
			  |  
			  <c:if test="${productSearch.page == 1}"><span class="pageNaviDead"><fmt:message key="previous" /></span></c:if>
			  <c:if test="${productSearch.page != 1}"><a href="<c:url value="productList.jhtm"><c:param name="page" value="${productSearch.page-1}"/><c:param name="category" value="${model.search['category']}"/><c:param name="type" value="${param.type}"/></c:url>" class="pageNaviLink"><fmt:message key="previous" /></a></c:if>
			  | 
			  <c:if test="${productSearch.page == model.pageCount}"><span class="pageNaviDead"><fmt:message key="next" /></span></c:if>
			  <c:if test="${productSearch.page != model.pageCount}"><a href="<c:url value="productList.jhtm"><c:param name="page" value="${productSearch.page+1}"/><c:param name="category" value="${model.search['category']}"/><c:param name="type" value="${param.type}"/></c:url>" class="pageNaviLink"><fmt:message key="next" /></a></c:if>
			  </td>
			  </tr>
			</table>
			<!-- pagination -->
			
			
			<table border="0" cellpadding="0" cellspacing="1" width="100%" class="quickMode">
			  <tr> 
			  <td class="pageSize">
			    <select name="size" onchange="document.getElementById('page').value=1;submit()">
			    <c:forTokens items="10,25,50,100,500" delims="," var="current">
			  	  <option value="${current}" <c:if test="${current == productSearch.pageSize}">selected</c:if>>${current} <fmt:message key="perPage" /></option>
			    </c:forTokens>
			    </select>
			  </td>
			  </tr>
			</table>
		    <!-- end input field -->  	  
		  	
		  	<!-- start button -->
            <div align="left" class="button quickMode">
			    <sec:authorize ifAnyGranted="ROLE_AEM,ROLE_ADMIN_${prop_site_id},ROLE_PRODUCT_CREATE">
			      <input type="submit" name="__add" value="<fmt:message key="productAdd" />">
			    </sec:authorize>   
			  	<c:if test="${model.count > 0}">
			  	<sec:authorize ifAnyGranted="ROLE_AEM,ROLE_ADMIN_${prop_site_id},ROLE_PRODUCT_DELETE">  
			  	  <input type="submit" name="__delete" value="Delete Selected Products" onClick="return deleteSelected()">
			  	</sec:authorize>  
				<c:if test="${model.categoryName != null}">
				  <input type="submit" name="__update_ranking" value="<fmt:message key="updateRanking" />">
				</c:if>  
			  </c:if>
	  	    </div>
			<!-- end button -->	
	        </div>
	        	        	  	
	<!-- end tab -->        
	</div>
<!-- end tabs -->			
</div>
  
  <!-- end table -->
   </td><td class="boxmidr" ></td></tr>
  <tr><td class="botl"></td><td class="botr"></td></tr>
  </table>
  
<!-- end main box -->  
</div>
</form> 
</sec:authorize>

  </tiles:putAttribute>    
</tiles:insertDefinition>