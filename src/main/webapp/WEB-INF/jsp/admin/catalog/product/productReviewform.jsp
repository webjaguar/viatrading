<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="/WEB-INF/tlds/spring.tld" prefix="spring" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>

<tiles:insertDefinition name="admin.catalog.product.review" flush="true">
  <tiles:putAttribute name="content" type="string">

<script language="JavaScript" type="text/JavaScript">
<!--
window.addEvent('domready', function(){			
	var tabs1 = new SimpleTabs($('tab-block-1'), { selector: 'h4'});  
   	var Tips1 = new Tips($$('.toolTipImg'));
	
});
//-->
</script>	
 
<!-- main box -->
  <div id="mboxfull">
  
  <!-- start table -->
  <table cellpadding="0" cellspacing="0" border="0" class="module" >
  <tr>
    <td class="topl_g">
	  
	  <!-- breadcrumb -->
	  <p class="breadcrumb">
	    <a href="../catalog">Catalog</a> &gt;
	    <a href="../catalog/productList.jhtm"><fmt:message key="products" /></a> &gt;
	    <a href="../catalog/productReviewList.jhtm"><fmt:message key="review" /></a>
	  </p>
	  
	  <!-- Error Message -->
	  <c:if test="${message != null}">
        <div class="message"><spring:message code="${message}"/></div>
      </c:if>
	  <spring:hasBindErrors name="productReviewForm">
          <div class="message">Please fix all errors!</div>
      </spring:hasBindErrors> 
      
  </td><td class="topr_g" ></td></tr>
  <tr><td class="boxmidlrg" >

	  
<form:form commandName="productReviewForm" method="post" name="productReviewForm">  
 <input type="hidden" name="id" value="${productReviewForm.productReview.id}"/>
  <input type="hidden" name="pid" value="${productReviewForm.productReview.productId}"/>
 <!-- tabs -->
 <div id="tab-block-1">
	<h4 title="Product Info">Review</h4>
	<div>
	<!-- start tab -->

        <c:set var="classIndex" value="0" />
        
	  	<div class="listdivi ln tabdivi"></div>
	  	<div class="listdivi"></div>
	   
		  	<div class="list${classIndex % 2}"><c:set var="classIndex" value="${classIndex+1}" />
		  	<div class="listfl"><fmt:message key="active" />:</div>
		  	<div class="listp">
		  	<!-- input field -->
	   			<form:checkbox path="productReview.active" />
		    <!-- end input field -->   	
		  	</div>
		  	</div>

		  	<div class="list${classIndex % 2}"><c:set var="classIndex" value="${classIndex+1}" />
		  	<div class="listfl"><div class="requiredField"><fmt:message key="rating" />:</div></div>
		  	<div class="listp">
		  	<!-- input field -->
	   			<form:input path="productReview.rate" disabled="true" cssClass="textfield" size="50" maxlength="120" htmlEscape="true"/>
	   			<form:errors path="productReview.rate" cssClass="error" />
		    <!-- end input field -->   	
		  	</div>
		  	</div>
		  	
		  	<div class="list${classIndex % 2}"><c:set var="classIndex" value="${classIndex+1}" />
		  	<div class="listfl"><div class="requiredField"><fmt:message key="title" />:</div></div>
		  	<div class="listp">
		  	<!-- input field -->
	   			<form:input path="productReview.title" disabled="true" cssClass="textfield" size="50" maxlength="120" htmlEscape="true"/>
	   			<form:errors path="productReview.title" cssClass="error" />
		    <!-- end input field -->   	
		  	</div>
		  	</div>
		  	
		  	<div class="list${classIndex % 2}"><c:set var="classIndex" value="${classIndex+1}" />
		  	<div class="listfl"><div class="requiredField"><fmt:message key="review" />:</div></div>
		  	<div class="listp">
		  	<!-- input field -->
	   			<form:textarea path="productReview.text" disabled="true" cssClass="textArea300x400" htmlEscape="true"/>
	   			<form:errors path="productReview.text" cssClass="error" />
		    <!-- end input field -->   	
		  	</div>
		  	</div>
		   
	<!-- end tab -->        
	</div> 

<!-- end tabs -->			
</div>

<!-- start button -->
<div align="left" class="button"> 
  <input type="submit" value="<fmt:message key="update" />" name="_update"/>
  <input type="submit" value="<fmt:message key="delete" />" name="_delete"/>
  <input type="submit" value="<fmt:message key="cancel" />" name="_cancel"/>
</div>
<!-- end button -->	     
</form:form>  
  <!-- end table -->
    </td><td class="boxmidr" ></td></tr>
  <tr><td class="botl"></td><td class="botr"></td></tr>
  </table>
  
<!-- end main box -->  
</div>

 </tiles:putAttribute>    
</tiles:insertDefinition>