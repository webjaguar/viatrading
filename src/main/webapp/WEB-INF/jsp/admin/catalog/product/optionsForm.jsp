<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="/WEB-INF/tlds/spring.tld" prefix="spring" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>

<tiles:insertDefinition name="admin.catalog.product.option" flush="true">
  <tiles:putAttribute name="content" type="string">
  <script type="text/javascript" src="../../admin/javascript/multiple.open.accordion1.2.js"></script>
<script language="JavaScript" type="text/JavaScript">
<!--
window.addEvent('domready', function(){			
			var tabs1 = new SimpleTabs($('tab-block-1'), { selector: 'h4'});
			var Tips1 = new Tips($$('.toolTipImg'));
			var myToggler = $$('div.arrow_drop');
			// Create the accordian
			new MultipleOpenAccordion(myToggler, $$('div.information'), {
				openAll: false, firstElementsOpen: [1],transition: Fx.Transitions.sineOut, display: 0,
				onActive: function(myToggler){
						myToggler.removeClass('tabsliderUp').addClass('tabsliderDown');
					},
				onBackground: function(myToggler){
				myToggler.removeClass('tabsliderDown').addClass('tabsliderUp');
					}
		});
});
function checkOptionCode() {
  if (document.getElementById('optionCode').value == '') {
     alert("Please type an option code");
     document.getElementById('optionCode').focus();
     return false;
  }   
  else {
     submit();   
  }
}
//-->
</script>
<style type="text/css">
 .hdr1 {font-size:12px;color:#676767}
</style>

<!-- main box -->
  <div id="mboxfull">
  
  <!-- start table -->
  <table cellpadding="0" cellspacing="0" border="0" class="module" >
  <tr>
    <td class="topl_g">
	  
	  <!-- breadcrumb -->
	  <p class="breadcrumb">
	    <a href="../catalog">Catalog</a> &gt;
	    <a href="../catalog/productList.jhtm">Products</a> &gt;
	    <a href="../catalog/options.jhtm">options</a> &gt;
	    ${form.option.code}
	  </p>
	  
	  <!-- Error Message -->
	  <c:if test="${!empty message}">
		  <div class="message"><spring:message code="${message}" arguments="${var}" /></div>
	  </c:if>
	  <spring:hasBindErrors name="productForm">
          <div class="message">Please fix all errors!</div>
      </spring:hasBindErrors>

      
		<c:if test="${!form.newOption}">
		<div align="right">
		<table class="form">
		  <tr>
		    <td align="right"><fmt:message key="dateAdded" /> : </td>
		    <td><fmt:formatDate type="both" timeStyle="full" value="${form.option.created}"/></td>
		  </tr>
		  <tr>
		    <td align="right"><fmt:message key="lastModified" /> : </td>
		    <td><fmt:formatDate type="both" timeStyle="full" value="${form.option.lastModified}"/></td>
		  </tr>
		</table>
		</div>
		</c:if>
  
  </td><td class="topr_g" ></td></tr>
  <tr><td class="boxmidlrg" >

  <c:if test="${form.newOption}" >
      <div class="titleBox"><div class="titlePrivilege"><fmt:message key="options" /></div></div>
	  <div class="arrow_drop arrowImageRight"></div>
	  <div style="clear: both;"></div>
	  <div class="information">
        <div style="float:left;padding: 5px">
        <form:form commandName="productForm" method="get">
        <table class="productBatchTool" cellpadding="5" cellspacing="5">
           <tr>
            <td valign="top">
             <table cellpadding="1" cellspacing="1">
              <tr style="height:30px;white-space: nowrap">
               <td valign="top" width="20px"><img class="toolTipImg" title="Option Code::Load Sku. Type an existing Option Code here.." src="../graphics/question.gif" /></td>
               <td valign="top"><h3><fmt:message key='option' /> <fmt:message key='code' /></h3></td>
              </tr>
             </table>
            </td>
           </tr>
           <tr>
            <td valign="top">
             <table cellpadding="1" cellspacing="1">
              <tr style="">
		       <td>
		        <input type="text" name="optionCode" id="optionCode" />
			    <div align="left" class="buttonLeft">
	             <input type="submit" value="Load Option" onClick="return checkOptionCode()" />
	            </div>
		       </td>
		      </tr>
             </table>
             </td>
           </tr>
        </table>
		</form:form>
		</div>
      </div>
  </c:if>

<form:form commandName="form" method="post" id="form">
<input type="hidden" name="delete" id="delete">
<input type="hidden" name="up" id="up">
<input type="hidden" name="down" id="down">
 <!-- tabs -->
 <div id="tab-block-1">
	<h4 title="Option">option</h4>
	<div>
	<!-- start tab -->
        <c:set var="classIndex" value="0" />
        
	  	<div class="listdivi ln tabdivi"></div>
	  	<div class="listdivi"></div>
	   
		  	<div class="list${classIndex % 2}"><c:set var="classIndex" value="${classIndex+1}" />
		  	<div class="listfl"><fmt:message key='option' /> <fmt:message key='code' />:</div>
		  	<div class="listp">
		  	<!-- input field -->
	   			<form:input path="option.code" cssClass="textfield" maxlength="50" size="50" disabled="${gSiteConfig['gSITE_DOMAIN'] == 'doors2gostore.com' and form.option.code == 'GLOBAL'}"/>
	   			<form:errors path="option.code" cssClass="error" />
		    	<div class="helpNote">
			      <p>Do not use special characters like '-', '_', '%', space, '^', '@'</p>
			    </div>
		    <!-- end input field -->   	
		  	</div>
		  	<c:if test="${gSiteConfig['gPROTECTED'] > 0}">
		  	<div class="listfl"><fmt:message key='protectedLevel' />:</div>
			<div class="listp">
			<!-- input field -->
		    	<form:select path="option.protectedLevel">
		      	  <form:option value="0"><fmt:message key="none"/></form:option>
			      <c:forEach begin="1" end="${gSiteConfig['gPROTECTED']}" var="protected">
			    	<c:set var="key" value="protected${protected}"/>
			    	<c:choose>
			      	  <c:when test="${labels[key] != null and labels[key] != ''}">
			        	<c:set var="label" value="${labels[key]}"/>
			      	  </c:when>
			      	  <c:otherwise><c:set var="label" value="${protected}"/></c:otherwise>
			    	</c:choose>		        
		        	<form:option value="${protectedLevels[protected-1]}"><c:out value="${label}"/></form:option>
		      	  </c:forEach>
		    	</form:select>
            	<form:errors path="option.protectedLevel" cssClass="error" />
        	<!-- end input field -->   	
			</div>
        	</c:if>
		  	</div>
		  	
		  	<div class="list${classIndex % 2}"><c:set var="classIndex" value="${classIndex+1}" />
		  	<!-- input field -->
		     <div class="form" align="right">
		       <input type="submit" name="__add" value="Add">
		       <c:if test="${siteConfig['PREMIER_DISTRIBUTOR'].value != '' or true}">
	           	 <input type="submit" name="__add_color_swatch" value="Add Color Swatch">
		       </c:if>
		       <c:if test="${siteConfig['CUSTOM_TEXT_PRODUCT'].value == 'true'}">
		         <input type="submit" name="__add_custom_text" value="Add Customer Message">
		       </c:if>
		       <c:if test="${gSiteConfig['gBOX']}">
		         <input type="submit" name="__add_box" value="Add Box Content">
		       </c:if>
		     </div>     
		     
			<table class="form" cellspacing="4" cellpadding="0" border="0">
		     <c:forEach items="${form.option.productOptions}" var="productOption" varStatus="status">
		       <tr valign="top">
		        <td width="125" rowspan="3">&nbsp;</td>
		        <td width="18" rowspan="3"><a href="#" onClick="document.getElementById('delete').value='${status.index}';document.getElementById('form').submit()"><img src="../graphics/trash.gif" border="0" title="delete"></a></td>
		        <td width="16" align="right" valign="bottom"><c:if test="${not status.first}"><a href="#" onClick="document.getElementById('up').value='${status.index}';document.getElementById('form').submit()"><img src="../graphics/arrowUp.gif" border="0" title="up"></a></c:if></td>		       
		        <td width="100" class="hdr1">
			   <c:choose>
		          <c:when test="${productOption.type == 'box'}">
		            <fmt:message key="sku" />:
		            <img class="toolTipImg" title="Note::Please provide a valid sku here." src="../graphics/question.gif" />
		          </c:when>
		          <c:when test="${productOption.type == 'colorSwatch'}">
		            <fmt:message key="Name" />:
		            <img class="toolTipImg" title="Note::Customer will select color." src="../graphics/question.gif" />
		          </c:when>
		          <c:when test="${productOption.type == 'cus'}">
		            <fmt:message key="Name" />:
		            <img class="toolTipImg" title="Note::Customer will fill out a value." src="../graphics/question.gif" />
		          </c:when>
		          <c:otherwise>
		            <fmt:message key="Name" />:
		            <img class="toolTipImg" title="Note::Please provide product option name here. ( size, color, etc...).  Also, Do not use special characters for option name like '-', '_', '%', space, '^', '@'" src="../graphics/question.gif" />
		          </c:otherwise>
		       </c:choose>
		        </td>			   
		        <td>		        
			   <input type="text" name="optionName_${productOption.index}" maxlength="100" size="50" class="textfield" value="<c:out value="${productOption.name}"/>">
		        </td>			   
			    <td><c:if test="${productOption.message != null}"><span class="error"><fmt:message key="${productOption.message}"/></span></c:if>&nbsp;</td>
			   </tr>
		       <tr valign="top">
		        <td align="right"></td>		       
		        <td class="hdr1">
			      <fmt:message key="helpText" />:
		          <img class="toolTipImg" title="Note::Please provide help message here." src="../graphics/question.gif" />
		        </td>			   
		        <td>		        
			      <textarea rows="2" cols="80" name="optionHelpText_${productOption.index}"><c:out value="${productOption.helpText}"/></textarea>
		        </td>			   
			    <td></td>
			   </tr>
		       <tr valign="top">
		        <td align="right"><c:if test="${not status.last}"><a href="#" onClick="document.getElementById('down').value='${status.index}';document.getElementById('form').submit()"><img src="../graphics/arrowDown.gif" border="0" title="down"></a></c:if></td>			       
		        <c:choose>
		          <c:when test="${productOption.type == 'box'}">
				    <td>&nbsp;</td>
		            <td>
		              note: no values for box
		              <c:out value="${productOption.valuesToTextArea}"/> 
		            </td>
		          </c:when>
		          <c:when test="${productOption.type == 'colorSwatch'}">
				    <td class="hdr1">
				      <fmt:message key="numberOfColors" />:
		              <img class="toolTipImg" title="Note::Please provide number of colors to be selected." src="../graphics/question.gif" />
		            </td>
		            <td>
		              <select name="optionNumberOfColors_${productOption.index}">
		                <c:forEach begin="0" end="10" step="1" var="number">
		                  <option value="${number}" <c:if test="${productOption.numberOfColors == number}">selected="selected"</c:if> >${number}</option>
		                </c:forEach>
		              </select>
		            </td>
		          </c:when>
		          <c:when test="${productOption.type == 'cus'}">
				    <td>&nbsp;</td>
		            <td>
		              note: no values for custom text
		              <c:out value="${productOption.valuesToTextArea}"/>
		            </td>
		          </c:when>
		          <c:otherwise>
		            <td class="hdr1">          
		              <fmt:message key="Values" />:
		              <img class="toolTipImg" title="Note::Please provide product option values here. ( small, large, etc...) Separate them by enter." src="../graphics/question.gif" />
		            </td>		            
		            <td>          
		              <textarea rows="10" cols="80" name="optionValue_${productOption.index}"><c:out value="${productOption.valuesToTextArea}"/></textarea>
		            </td>          
		          </c:otherwise>
		        </c:choose>
			    <td><c:if test="${productOption.valueMessage != null}"><span class="error"><fmt:message key="${productOption.valueMessage}"/></span></c:if>&nbsp;</td>        
			   </tr>
			   <tr>
			     <td colspan="1">&nbsp;</td>
			     <td colspan="5"><hr></td>
			   </tr>
		     </c:forEach>
		     </table>
		    <!-- end input field -->   	
		  	</div>

	<!-- end tab -->        
	</div> 
	
 
<!-- end tabs -->			
</div>

<!-- start button -->
<div align="right" class="button"> 
<c:if test="${form.newOption}">
  <input type="submit" value="<spring:message code="add"/> <fmt:message key="option"/>" name="_add">
</c:if>
<c:if test="${not form.newOption}">
  <input type="submit" value="<fmt:message key="update"/> <fmt:message key="option"/>" name="_update">
  <c:if test="${gSiteConfig['gSITE_DOMAIN'] != 'doors2gostore.com' || form.option.code != 'GLOBAL'}">
  <input type="submit" value="<spring:message code="delete"/>" name="_delete" onClick="return confirm('Delete permanently?')">
  </c:if>
</c:if>
  <input type="submit" value="<fmt:message key="cancel"/>" name="_cancel">  
</div>
<!-- end button -->	     
</form:form>
  <!-- end table -->
    </td><td class="boxmidr" ></td></tr>
  <tr><td class="botl"></td><td class="botr"></td></tr>
  </table>
  
<!-- end main box -->  
</div>
 
  </tiles:putAttribute>    
</tiles:insertDefinition>


