<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/security/tags" prefix="sec" %>

<tiles:insertDefinition name="admin.catalog.product" flush="true">
  <tiles:putAttribute name="content" type="string">
  <sec:authorize ifAnyGranted="ROLE_AEM,ROLE_ADMIN_${prop_site_id},ROLE_SUPPLIER_VIEW_LIST">

<c:if test="${gSiteConfig['gSUPPLIER'] == true}">
<form action="productSupplierList.jhtm" method="post">
<!-- main box -->
  <div id="mboxfull">
  
  <!-- start table -->
  <table cellpadding="0" cellspacing="0" border="0" class="module" >
  <tr>
    <td class="topl_g">
	  
	  <!-- breadcrumb -->
	  <p class="breadcrumb">
	    <a href="../catalog"><fmt:message key="catalog" /></a> &gt;
	    <a href="../catalog/productList.jhtm"><fmt:message key="products" /></a> &gt;
	    <fmt:message key="suppliers" />
	  </p>
	  
	  <!-- Error -->
	  <c:if test="${!empty model.message}">
		<div class="message"><fmt:message key="${model.message}" /></div>
	  </c:if>  
	  
  </td><td class="topr_g" ></td></tr>
  <tr><td class="boxmidlrg" >

 <!-- tabs -->
 <div class="tab-wrapper">
	<h4 title="List of suppliers"></h4>
	<div>
	<!-- start tab -->

	  	<div class="listdivi ln tabdivi"></div>
	  	<div class="listdivi"></div>
	    
		  	<div class="listlight">
		  	<!-- input field -->
			<table border="0" cellpadding="0" cellspacing="1" width="100%">
				<tr>
					<td>&nbsp;
					</td>
					<c:if test="${model.suppliers.nrOfElements > 0}">
						<td class="pageShowing">
							<fmt:message key="showing">
								<fmt:param value="${model.suppliers.firstElementOnPage + 1}" />
								<fmt:param value="${model.suppliers.lastElementOnPage + 1}" />
								<fmt:param value="${model.suppliers.nrOfElements}" />
							</fmt:message>
						</td>
					</c:if>
					<td class="pageNavi">
						Page
						<select name="page" id="page" onchange="submit()">
							<c:forEach begin="1" end="${model.suppliers.pageCount}"
								var="page">
								<option value="${page}"
									<c:if test="${page == (model.suppliers.page+1)}">selected</c:if>>
									${page}
								</option>
							</c:forEach>
						</select> 
						of
						<c:out value="${model.suppliers.pageCount}" /> 
						|
						<c:if test="${model.suppliers.firstPage}">
							<span class="pageNaviDead"><fmt:message key="previous" /></span>
						</c:if>
						<c:if test="${not model.suppliers.firstPage}">
							<a
								href="<c:url value="productSupplierList.jhtm"><c:param name="sku" value="${model.sku}"/><c:param name="page" value="${model.suppliers.page}"/><c:param name="size" value="${model.suppliers.pageSize}"/></c:url>"
								class="pageNaviLink"><fmt:message key="previous" /></a>
						</c:if>
						|
						<c:if test="${model.suppliers.lastPage}">
							<span class="pageNaviDead"><fmt:message key="next" /></span>
						</c:if>
						<c:if test="${not model.suppliers.lastPage}">
							<a
								href="<c:url value="productSupplierList.jhtm"><c:param name="sku" value="${model.sku}"/><c:param name="page" value="${model.suppliers.page+2}"/><c:param name="size" value="${model.suppliers.pageSize}"/></c:url>"
								class="pageNaviLink"><fmt:message key="next" /></a>
						</c:if>
					</td>
				</tr>
			</table>
		
			<table border="0" cellpadding="0" cellspacing="0" width="100%"
				class="listings">
				<tr class="listingsHdr2">
		
					<td class="indexCol">&nbsp;</td>
					<td class="listingsHdr3"><fmt:message key="Name" /></td>
					<td class="listingsHdr3"><fmt:message key="cost" /></td>
					<td class="listingsHdr3"><fmt:message key="lastModified" /></td>
					<td class="listingsHdr3"><fmt:message key="city" /></td>
					<td class="listingsHdr3"><fmt:message key="state" /></td>
					<td class="listingsHdr3"><fmt:message key="zipCode" /></td>
					<td class="listingsHdr3"><fmt:message key="phone" /></td>
					<td class="listingsHdr3"><fmt:message key="fax" /></td>
					<td class="listingsHdr3"><fmt:message key="primarySupplier" /></td>
				</tr>
				<c:forEach items="${model.suppliers.pageList}" var="supplier"	varStatus="status">
					<tr class="row${status.index % 2}"	onmouseover="changeStyleClass(this,'rowOver')" 	onmouseout="changeStyleClass(this,'row${status.index % 2}')">
						<td class="indexCol">
							<c:out value="${status.count + model.suppliers.firstElementOnPage}" />.
						</td>
						<td class="nameCol">
							<a href="productSupplierForm.jhtm?sku=${model.sku}&sid=${supplier.id}" class="nameLink"><c:out value="${supplier.address.company}" /></a>
						</td>
						<td class="nameCol">
						<c:choose>
						   <c:when test="${supplier.price != null}">
							<c:if test ="${supplier.percent == true}">
								<a href="productSupplierForm.jhtm?sku=${model.sku}&sid=${supplier.id}" class="nameLink"><fmt:formatNumber value="${supplier.price}" pattern="#,##0.00" /></a>%
							</c:if>
							<c:if test ="${supplier.percent == false}">
								<a href="productSupplierForm.jhtm?sku=${model.sku}&sid=${supplier.id}" class="nameLink"><c:if test="${supplier.price != null}">$</c:if><fmt:formatNumber value="${supplier.price}" pattern="#,##0.00" /></a>
							</c:if>	
						   </c:when>
						   <c:otherwise>
						      <a href="productSupplierForm.jhtm?sku=${model.sku}&sid=${supplier.id}" class="nameLink"><img border="0" src="../graphics/edit.gif" align="center"/></a>
						   </c:otherwise>
						</c:choose>		
						</td>
						<td class="nameCol">
							<fmt:formatDate type="date" dateStyle="full" value="${supplier.lastModified}" pattern="MM/dd/yyyy (hh:mm)a zz "/>
						</td>
						<td class="nameCol">
							<c:out value="${supplier.address.city}" />
						</td>
						<td class="nameCol">
							<c:out value="${supplier.address.stateProvince}" />
						</td>
						<td class="nameCol">
							<c:out value="${supplier.address.zip}" />
						</td>
						<td class="nameCol">
							<c:out value="${supplier.address.phone}" />
						</td>
						<td class="nameCol">
							<c:out value="${supplier.address.fax}" />
						</td>
						<td class="nameCol">
					      <c:choose>
						    <c:when test="${supplier.primary == true}"><img src="../graphics/checkbox.png" alt="primary" title="primary" border="0"></c:when>
						    <c:otherwise><img src="../graphics/box.png" alt="Not Printed" title="" border="0"></c:otherwise>
						  </c:choose>
					    </td>
					</tr>
				</c:forEach>
				<c:if test="${model.suppliers.nrOfElements == 0}">
					<tr class="emptyList">
						<td colspan="8">
							&nbsp;
						</td>
					</tr>
				</c:if>
			</table>
			
			<c:if test="${model.suppliers.nrOfElements > 0}">
			</c:if>
			<table border="0" cellpadding="0" cellspacing="1" width="100%">
				<tr>
					<td class="pageSize">
						<select name="size"
							onchange="document.getElementById('page').value=1;submit()">
							<c:forTokens items="10,25,50" delims="," var="current">
								<option value="${current}"
									<c:if test="${current == model.suppliers.pageSize}">selected</c:if>>
									${current}
									<fmt:message key="perPage" />
								</option>
							</c:forTokens>
						</select>
					</td>
				</tr>
			</table>
		    <!-- end input field -->  	  
		  	
		  	<!-- start button -->
		  	<input type="hidden" name="sku" value="${model.sku}">
		  	<%-- When product added form front-end we shouldn't allow ADMIN to add supplier to that Product. --%>
            <div align="left" class="button">
				<input type="submit" name="__add" value="<fmt:message key="add" /> <fmt:message key="supplier" />"/>
	  	    </div>
			<!-- end button -->	
	        	  	
	<!-- end tab -->        
	</div>
<!-- end tabs -->			
</div>
  
  <!-- end table -->
   </td><td class="boxmidr" ></td></tr>
  <tr><td class="botl"></td><td class="botr"></td></tr>
  </table>
  
<!-- end main box -->  
</div>
</form>
</c:if>

  </sec:authorize>
  </tiles:putAttribute>
</tiles:insertDefinition>