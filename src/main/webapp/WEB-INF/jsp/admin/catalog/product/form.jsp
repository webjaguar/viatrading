<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="/WEB-INF/tlds/spring.tld" prefix="spring" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ taglib uri="http://www.springframework.org/security/tags" prefix="sec" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ page contentType="text/html;charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib uri="/WEB-INF/tlds/custom.tld" prefix="wj" %>

<tiles:insertDefinition name="admin.catalog.product" flush="true">
  <tiles:putAttribute name="content" type="string">

<script type="text/javascript" src="<c:url value="/FCKeditor/fckeditor.js"/>"></script>
<script type="text/javascript" src="../../admin/javascript/multiple.open.accordion1.2.js"></script>
<script language="JavaScript" type="text/JavaScript">
<!--
window.onload = function() {
  for (i=${gSiteConfig['gPRICE_TIERS']}; i>1; i--) {
    updateQtyBreak(i);
  }
}

function updateQtyBreak(i) {
  var priceFrom = document.getElementById('priceFrom_'+i).value;
  if (priceFrom > 0) {
    if ((i<${gSiteConfig['gPRICE_TIERS']}) && (document.getElementById('priceFrom_'+(i+1)).value > 0)) {
      document.getElementById('priceTo_'+i).value = document.getElementById('priceFrom_'+(i+1)).value-1;    
    } else {
      document.getElementById('priceTo_'+i).value = "";
    }
    document.getElementById('priceTo_'+(i-1)).value = priceFrom-1;    
  } else {
    document.getElementById('priceTo_'+i).value = "";
  }
}


function addKitParts() {
	  var tr = document.createElement("tr");

	  tr.appendChild(document.createElement("td"));
	  
	  var td1 = document.createElement("td");  
	  var textfield1 = document.createElement("input");
	  textfield1.setAttribute("type", "text"); 
	  textfield1.setAttribute("name", "__kitParts_sku"); 
	  textfield1.setAttribute("class", "textfield250"); 
	  textfield1.setAttribute("maxlength", "50");
	  td1.appendChild(textfield1);
	  tr.appendChild(td1);

	  var td2 = document.createElement("td");  
	  var textfield2 = document.createElement("input");
	  textfield2.setAttribute("type", "text"); 
	  textfield2.setAttribute("name", "__kitParts_quantity"); 
	  textfield2.setAttribute("class", "textfield50"); 
	  textfield2.setAttribute("maxlength", "10");
	  td2.appendChild(textfield2);
	  tr.appendChild(td2);
	  
	  document.getElementById('kitParts').appendChild(tr);
}

function loadEditor(el) {
  var oFCKeditor = new FCKeditor(el);
  oFCKeditor.BasePath = "<c:url value="/FCKeditor/"/>";
  oFCKeditor.Config["CustomConfigurationsPath"] = '<c:url value="/admin/javascript/fckconfig.js"/>';
  oFCKeditor.ToolbarSet = 'Html';
  oFCKeditor.Width = 650;
  oFCKeditor.Height = 400;
  oFCKeditor.ReplaceTextarea();
  document.getElementById(el + '_link').style.display="none";
}

function ckeckSKU() {
  if ( document.getElementById('loadSKU').value == '' ) {
     alert("Please type a SKU");
     return false;
  }   
  else
     submit();   
}
<c:if test="${gSiteConfig['gVARIABLE_PRICE']}">
function togglePrice(id) {
if (document.getElementById(id).checked) 
	{
	  document.getElementById('fixPrice').style.display="none";
	} else 
	{
	  document.getElementById('fixPrice').style.display="block";
	}
}
</c:if>

<c:if test="${gSiteConfig['gPRODUCT_VARIANTS']}">
function addVariant() {
	  var tr = document.createElement("tr");

	  tr.appendChild(document.createElement("td"));
	  
	  var td1 = document.createElement("td");  
	  var textfield1 = document.createElement("input");
	  textfield1.setAttribute("type", "text"); 
	  textfield1.setAttribute("name", "__variant_sku"); 
	  textfield1.setAttribute("class", "textfield250"); 
	  textfield1.setAttribute("maxlength", "50");
	  td1.appendChild(textfield1);
	  tr.appendChild(td1);

	  var td2 = document.createElement("td");  
	  var textfield2 = document.createElement("input");
	  textfield2.setAttribute("type", "text"); 
	  textfield2.setAttribute("name", "__variant_name"); 
	  textfield2.setAttribute("class", "textfield250"); 
	  textfield2.setAttribute("maxlength", "120");
	  td2.appendChild(textfield2);
	  tr.appendChild(td2);

	  var td3 = document.createElement("td");  
	  var textfield3 = document.createElement("input");
	  textfield3.setAttribute("type", "text"); 
	  textfield3.setAttribute("name", "__variant_shortDesc"); 
	  textfield3.setAttribute("class", "textfield250"); 
	  textfield3.setAttribute("maxlength", "512");
	  td3.appendChild(textfield3);
	  tr.appendChild(td3);

	  var td4 = document.createElement("td");  
	  var textfield4 = document.createElement("input");
	  textfield4.setAttribute("type", "text"); 
	  textfield4.setAttribute("name", "__variant_price"); 
	  textfield4.setAttribute("class", "textfield50"); 
	  textfield4.setAttribute("maxlength", "10");
	  td4.appendChild(textfield4);
	  tr.appendChild(td4);
	  
	  document.getElementById('variants').appendChild(tr);
}
</c:if>
function showSelect(id){
	var fld = document.getElementById('opt'+ id);
	var values = "";
	for (var i = 0; i < fld.options.length; i++) {
	  if (fld.options[i].selected) {
	    values = values + fld.options[i].value + ",";
	  }
	}
	   var existing = document.getElementById(id).value;
	   if(existing == ""){
			  document.getElementById("text"+id).value = "," + values;

	   } else {
			  document.getElementById("text"+id).value=existing + values;
	   }
}
window.addEvent('domready', function(){			
			var tabs1 = new SimpleTabs($('tab-block-1'), { selector: 'h4'});  
	    	var Tips1 = new Tips($$('.toolTipImg'));
	     	 
			<c:if test="${productForm.newProduct}" >
			var myToggler = $$('div.arrow_drop');
			// Create the accordian
			new MultipleOpenAccordion(myToggler, $$('div.information'), {
				transition: Fx.Transitions.sineOut,display:100,
				onActive: function(myToggler){
						myToggler.removeClass('arrowImageRight').addClass('arrowImageDown');
					},
				onBackground: function(myToggler){
				myToggler.removeClass('arrowImageDown').addClass('arrowImageRight');
					}
			});
			</c:if>
			<c:if test="${gSiteConfig['gPURCHASE_ORDER'] && siteConfig['SHOW_COST'].value == 'true'}">
			var boxShowCost = new multiBox('mbCost', {showNumbers: false});
			</c:if>
});
//-->
</script>	
 
<!-- main box -->
  <div id="mboxfull">
  
  <!-- start table -->
  <table cellpadding="0" cellspacing="0" border="0" class="module" >
  <tr>
    <td class="topl_g">
	  
	  <!-- breadcrumb -->
	  <p class="breadcrumb">
	    <a href="../catalog">Catalog</a> &gt;
	    <a href="../catalog/productList.jhtm">Products</a> &gt;
	    ${productForm.product.sku} 
	  </p>
	  
	  <!-- Error Message -->
	  <c:if test="${message != null}">
        <div class="message"><spring:message code="${message}"/></div>
      </c:if>
      <c:if test="${!empty productForm.message}">
		<div class="message"><c:out value="${productForm.message}"></c:out></div>
	  </c:if>
	  <spring:hasBindErrors name="productForm">
          <div class="message">Please fix all errors!</div>
      </spring:hasBindErrors>

      
	  <c:if test="${!productForm.newProduct}">
		<div style="float: left;">
		<table class="form">
		  <tr>
		    <td align="right"><fmt:message key="lastModified" /> : </td>
		    <td><fmt:formatDate type="both" timeStyle="full" value="${productForm.product.lastModified}"/></td>
		  </tr>
		  <tr>
		    <td align="right"><fmt:message key="lastModifiedBy" /> : </td>
		    <td><c:out value="${productForm.product.lastModifiedBy}"/></td>
		  </tr>
		</table>
		</div>
		<div style="float: right;">
		<table class="form">
		  <tr>
		    <td align="right"><fmt:message key="productId" /> : </td>
		    <td>
			  <spring:bind path="productForm.product.id">
			    <c:out value="${status.value}"/>
		        <input type="hidden" name="<c:out value="${status.expression}"/>" value="<c:out value="${status.value}"/>">
			  </spring:bind>
		    </td>
		  </tr>
		  <tr>
		    <td align="right"><fmt:message key="dateAdded" /> : </td>
		    <td><fmt:formatDate type="both" timeStyle="full" value="${productForm.product.created}"/></td>
		  </tr>
		  <tr>
		    <td align="right"><fmt:message key="createdBy" /> : </td>
		    <td><c:out value="${productForm.product.createdBy}"/></td>
		  </tr>
		</table>
		</div>
	
      </c:if>  
      
      <jsp:include page="/WEB-INF/jsp/admin/catalog/product/vickiHelp.jsp" /> 
      
		
		<div style="clear: both;"></div> 
      
  </td><td class="topr_g" ></td></tr>
  <tr><td class="boxmidlrg" >
  
  <c:if test="${productForm.newProduct}" >
  <div class="optionBox"><div class="titlePrivilege"><fmt:message key="options" /></div></div>
	  <div class="arrow_drop arrowImageRight"></div>
	  <div style="clear: both;"></div>
	  <div class="information">
		 <div style="float:left;padding: 5px">
           <form:form commandName="productForm" method="get">
           <table class="productBatchTool" cellpadding="5" cellspacing="5">
           <tr>
            <td valign="top">
             <table cellpadding="1" cellspacing="1">
              <tr style="height:30px;white-space: nowrap">
               <td valign="top" width="20px"><img class="toolTipImg" title="Load Sku::Use for duplication." src="../graphics/question.gif" /></td>
               <td valign="top"><h3><fmt:message key="productSku" /></h3></td>
              </tr>
             </table>
            </td>
           </tr>
           <tr>
            <td valign="top">
             <table cellpadding="1" cellspacing="1">
              <tr style="">
		       <td>
		        <input type="text" name="sku" id="loadSKU"/>
			    <div align="left" class="buttonLeft">
	             <input type="submit" name="__loadSku" value="Load SKU" onClick="return ckeckSKU()" />
	            </div>
		       </td>
		      </tr>
             </table>
            </td>
           </tr>
           </table>
           </form:form>
		 </div>
	  </div>
  </c:if>
	  
<form:form commandName="productForm" method="post" name="productForm" enctype="multipart/form-data">  
 <form:hidden path="newProduct" />
 <form:hidden path="product.id" />
 <!-- tabs -->
 <div id="tab-block-1">
	<h4 title="Product Info">Product Info</h4>
       
   <jsp:include page="/WEB-INF/jsp/admin/catalog/product/productInfo.jsp" /> 
  
  <h4 title="<fmt:message key="layout" />"><fmt:message key="layout" /></h4>
	<div>
	<!-- start tab -->

	  	<div class="listdivi ln tabdivi"></div>
	  	<div class="listdivi"></div>
	   
		  	<div class="list${classIndex % 2}"><c:set var="classIndex" value="${classIndex+1}" />
		  	<div class="listfl"><fmt:message key="hide" /> <fmt:message key="Header" />:</div>
		  	<div class="listp">
		  	<!-- input field -->
			    <fmt:message key="yes" /> <form:radiobutton path="product.hideHeader" value="true" />
			    <fmt:message key="no" /> <form:radiobutton path="product.hideHeader" value="false" />
		    <!-- end input field -->   	
		  	</div>
		  	</div>
		    
		  	<div class="list${classIndex % 2}"><c:set var="classIndex" value="${classIndex+1}" />
		  	<div class="listfl"><fmt:message key="hide" /> <fmt:message key="TopBar" />:</div>
		  	<div class="listp">
		  	<!-- input field -->
			    <fmt:message key="yes" /> <form:radiobutton path="product.hideTopBar" value="true" />
			    <fmt:message key="no" /> <form:radiobutton path="product.hideTopBar" value="false" />
		    <!-- end input field -->  	  
		  	</div>
		  	</div>
		  
		  	<div class="list${classIndex % 2}"><c:set var="classIndex" value="${classIndex+1}" />
		  	<div class="listfl"><fmt:message key="hide" /> <fmt:message key="LeftBar" />:</div>
		  	<div class="listp">
		  	<!-- input field -->
		        <fmt:message key="yes" /> <form:radiobutton path="product.hideLeftBar" value="true" />
		        <fmt:message key="no" /> <form:radiobutton path="product.hideLeftBar" value="false" />
		  	<!-- end input field -->	
		  	</div>
		  	</div>	
		  	
		  	<div class="list${classIndex % 2}"><c:set var="classIndex" value="${classIndex+1}" />
		  	<div class="listfl"><fmt:message key="hide" /> <fmt:message key="RightBar" />:</div>
		  	<div class="listp">
		  	<!-- input field -->
			    <fmt:message key="yes" /> <form:radiobutton path="product.hideRightBar" value="true" />
			    <fmt:message key="no" /> <form:radiobutton path="product.hideRightBar" value="false" />
		    <!-- end input field -->  	  
		  	</div>
		  	</div>
		  
		  	<div class="list${classIndex % 2}"><c:set var="classIndex" value="${classIndex+1}" />
		  	<div class="listfl"><fmt:message key="hide" /> <fmt:message key="Footer" />:</div>
		  	<div class="listp">
		  	<!-- input field -->
		        <fmt:message key="yes" /> <form:radiobutton path="product.hideFooter" value="true" />
		        <fmt:message key="no" /> <form:radiobutton path="product.hideFooter" value="false" />
		  	<!-- end input field -->	
		  	</div>
		  	</div>	
		  	
		  	<div class="list${classIndex % 2}"><c:set var="classIndex" value="${classIndex+1}" />
		  	<div class="listfl"><fmt:message key="hide" /> <fmt:message key="breadCrumbs" />:</div>
		  	<div class="listp">
		  	<!-- input field -->
		        <fmt:message key="yes" /> <form:radiobutton path="product.hideBreadCrumbs" value="true" />
		        <fmt:message key="no" /> <form:radiobutton path="product.hideBreadCrumbs" value="false" />
		    <!-- end input field -->  	  
		  	</div>
		  	</div>
		  
		  	<div class="list${classIndex % 2}"><c:set var="classIndex" value="${classIndex+1}" />
		  	<div class="listfl"><div class="helpImg"><img class="toolTipImg" title="Product Layout::Select the front end or the webpage layout." src="../graphics/question.gif" /></div><fmt:message key="productLayout" />:</div>
		  	<div class="listp">
		  	<!-- input field -->
		        <form:select path="product.productLayout">
			      <form:option value=""><fmt:message key="global"/></form:option>
		          <c:forTokens items="${siteConfig['PRODUCT_DETAIL_LAYOUT'].value}" delims="," var="option" varStatus="status">
		  	        <form:option value="${option}"><fmt:message key="${option}"/></form:option>
		  	      </c:forTokens>
		       </form:select>
		    <!-- end input field -->  	  
		  	</div>
		  	</div>
		  
		  	<div class="list${classIndex % 2}"><c:set var="classIndex" value="${classIndex+1}" />
		  	<div class="listfl"><div class="helpImg"><img class="toolTipImg" title="Head Tag::Add your custom Head Tag HTML code here. Ths will override your Default Head Tag." src="../graphics/question.gif" /></div><fmt:message key="headTag" />:</div>
		  	<div class="listp">
		  	<!-- input field -->
      			<form:textarea path="product.headTag" cssClass="textfield" rows="15" cols="60" htmlEscape="true"/>
		  	<!-- end input field -->	
		  	</div>
		  	</div>	
		  	
		  	<div class="list${classIndex % 2}"><c:set var="classIndex" value="${classIndex+1}" />
		  	<div class="listfl"><div class="helpImg"><img class="toolTipImg" title="HTML Add To Cart::Please enter code for custom or different, add to cart form, here." src="../graphics/question.gif" /></div><fmt:message key="htmlAddToCart" />:</div>
		  	<div class="listp">
		  	<!-- input field -->
      			<form:textarea path="product.htmlAddToCart" cssClass="textfield" rows="15" cols="60" htmlEscape="true"/>
		  	    <div style="text-align:left" id="product.htmlAddToCart_link"><a href="#" onClick="loadEditor('product.htmlAddToCart')" class="htmleditor">- <fmt:message key="htmlEditor" /> -</a></div>
			<!-- end input field -->	
		  	</div>
		  	</div>
  	
	<!-- end tab -->        
	</div>         
	
  <c:if test="${not empty productForm.productFields}">	
  <h4 title="ProductField">ProductField</h4>
	<div>
	<!-- start tab -->

	  	<div class="listdivi ln tabdivi"></div>
	  	<div class="listdivi"></div>
  <c:forEach items="${productForm.productFields}" var="productField" varStatus="status"> 
		  	<div class="list${classIndex % 2}"><c:set var="classIndex" value="${classIndex+1}" />
		  	<div class="listfl">
		       <c:if test="${siteConfig['DSI'].value != '' and siteConfig['CUSTOM_SHIPPING_FLAG'].value == productField.id}">
		         <div class="helpImg"><img class="toolTipImg" title="Custom Shipping Flag::A value of '0' means this item has custom shipping." src="../graphics/question.gif" /></div>
		       </c:if>		
		       <c:if test="${siteConfig['DSI'].value != '' and siteConfig['CUSTOM_SHIPPING_FIELD1'].value == productField.id}">
		         <div class="helpImg"><img class="toolTipImg" title="<c:out value="${siteConfig['CUSTOM_SHIPPING_TITLE1'].value}" />::shipping cost" src="../graphics/question.gif" /></div>
		       </c:if>
		       <c:if test="${siteConfig['DSI'].value != '' and siteConfig['CUSTOM_SHIPPING_FIELD2'].value == productField.id}">
		         <div class="helpImg"><img class="toolTipImg" title="<c:out value="${siteConfig['CUSTOM_SHIPPING_TITLE2'].value}" />::shipping cost" src="../graphics/question.gif" /></div>
		       </c:if>
		       <c:if test="${siteConfig['PRODUCT_INACTIVE_REDIRECT_FIELD'].value == productField.id}">
		         <div class="helpImg"><img class="toolTipImg" title="Product Inactive Redirect URL::If this item is inactive then request will redirect to this URL." src="../graphics/question.gif" /></div>
		       </c:if>
		  		<c:out value="${productField.name}"/>:
		  	</div>
		  	<div class="listp">
		  	<!-- input field -->
		  	    <c:choose>
			     <c:when test="${!empty productField.preValue and siteConfig['SEARCH_FIELDS_MULTIPLE'].value == 'true' and (productField.search == 'true' or productField.search2 == 'true')}">
			       <input type="hidden" id="${productField.id}" value="<c:out value="${wj:getProductFieldValue(productForm.product,productField.id)}" escapeXml="false"/>">
                   
                   <form:input path="product.field${productField.id}" id="text${productField.id}" cssClass="textfield" maxlength="255" size="70"  htmlEscape="true" />
			     	<div id=""></div>	     	
			     	<br></br>
			     	<form:select path="" cssClass="productPreValueField"  id="opt${productField.id}" multiple="multiple" onChange="showSelect(${productField.id})">
			        <c:forTokens items="${productField.preValue}" delims="," var="dropDownValue" >
			           <form:option value="${dropDownValue}" ><c:out value="${dropDownValue}"></c:out></form:option>
			         </c:forTokens>
			       </form:select>
			     </c:when>
			     <c:when test="${!empty productField.preValue and productField.search == 'false'}">
			       <form:select path="product.field${productField.id}" cssClass="productPreValueField">
			         <form:option value="" label="Please Select"/>
			         <c:forTokens items="${productField.preValue}" delims="," var="dropDownValue">
			           <form:option value="${dropDownValue}" />
			         </c:forTokens>
			       </form:select>
			     </c:when>
			     <c:when test="${!empty productField.preValue}">
			       <form:select path="product.field${productField.id}" cssClass="productPreValueField">
			         <form:option value="" label="Please Select"/>
			         <c:forTokens items="${productField.preValue}" delims="," var="dropDownValue">
			           <form:option value="${dropDownValue}" />
			         </c:forTokens>
			       </form:select>
			     </c:when>
			     <c:otherwise>
			       <form:input  path="product.field${productField.id}" cssClass="textfield" maxlength="255" size="70" htmlEscape="true"/>
			     </c:otherwise>
			     </c:choose>
			     (Field <c:out value="${productField.id}"/>)
		    <!-- end input field -->   	
		  	</div>
		  	</div>
  </c:forEach>	        
	<!-- end tab -->        
	</div> 
  </c:if>	

  <c:if test="${gSiteConfig['gLOYALTY']}">
  <h4 title="<fmt:message key='loyalty' />"><fmt:message key="loyalty" /></h4>
	<div>
	<!-- start tab -->

	  	<div class="listdivi ln tabdivi"></div>
	  	<div class="listdivi"></div>
	   
		  	<div class="list${classIndex % 2}"><c:set var="classIndex" value="${classIndex+1}" />
		  	<div class="listfl"><div class="helpImg"><img class="toolTipImg" title="Note::Customers will receive this amount of points if an order takes place." src="../graphics/question.gif" /></div><fmt:message key="loyalty" /> <fmt:message key="point" />:</div>
		  	<div class="listp">
		  	<!-- input field -->
		        <form:input path="product.loyaltyPoint" size="10" cssClass="textfield50" maxlength="10" />
			    <form:errors path="product.loyaltyPoint" cssClass="error"/>
		    <!-- end input field -->   	
		  	</div>
		  	</div>
	  	
	<!-- end tab -->        
	</div> 
	</c:if> 
	
		    
  <h4 title="Price">Price</h4>
	<div>
	<!-- start tab -->

	  	<div class="listdivi ln tabdivi"></div>
	  	<div class="listdivi"></div>
	   		<c:if test="${gSiteConfig['gVARIABLE_PRICE']}">
		  	<div class="list${classIndex % 2}"><c:set var="classIndex" value="${classIndex+1}" />
		  	<div class="listfl"><div class="helpImg"><img class="toolTipImg" title="Note::Customer can enter the price of this product. Recommended usage: Donation" src="../graphics/question.gif" /></div><fmt:message key="price" /> <fmt:message key="enterByCustomer" />:</div>
		  	<div class="listp">
		  	<!-- input field -->
		        <form:checkbox id="product.priceByCustomer" path="product.priceByCustomer" onclick="togglePrice(this.id)" />
		    <!-- end input field -->   	
		  	</div>
		  	</div>
		  	</c:if>
		    
		  	<div class="list${classIndex % 2}"><c:set var="classIndex" value="${classIndex+1}" />
		  	<div class="listfl"><div class="helpImg"><img class="toolTipImg" title="Taxable::Check if the product is taxable." src="../graphics/question.gif" /></div><fmt:message key="taxable" />:</div>
		  	<div class="listp">
		  	<!-- input field -->
     			<form:checkbox path="product.taxable" />
		    <!-- end input field -->  	  
		  	</div>
		  	</div>
		  
		    <div id="fixPrice">
		  	<div class="list${classIndex % 2}"><c:set var="classIndex" value="${classIndex+1}" />
		  	<div class="listfl"><div class="helpImg"><img class="toolTipImg" title="Login Required::Check if price display needs login." src="../graphics/question.gif" /></div><fmt:message key="loginRequire" />:</div>
		  	<div class="listp">
		  	<!-- input field -->
     			<form:checkbox path="product.loginRequire" />
		  	<!-- end input field -->	
		  	</div>
		  	</div>
		  	
		  	<div class="list${classIndex % 2}"><c:set var="classIndex" value="${classIndex+1}" />
		  	<div class="listfl"><div class="helpImg"><img class="toolTipImg" title="Hide price::Check if want to hide price." src="../graphics/question.gif" /></div><fmt:message key="hidePrice" />:</div>
		  	<div class="listp">
		  	<!-- input field -->
     			<form:checkbox path="product.hidePrice" />
		    <!-- end input field -->  	  
		  	</div>
		  	</div>	
		  	
		  	<c:if test="${siteConfig['PRODUCT_QUOTE'].value == 'true'}">
		  	<div class="list${classIndex % 2}"><c:set var="classIndex" value="${classIndex+1}" />
		  	<div class="listfl"><fmt:message key="quote" />:</div>
		  	<div class="listp">
		  	<!-- input field -->
     			<form:checkbox path="product.quote" />
		    <!-- end input field -->  	  
		  	</div>
		  	</div>	
		  	</c:if>
		  	
		  	<div class="list${classIndex % 2}"><c:set var="classIndex" value="${classIndex+1}" />
		  	<div class="listfl"><fmt:message key="packing" />:</div>
		  	<div class="listp">
		  	<!-- input field -->
	 			<form:input path="product.packing" cssClass="textfield" maxlength="50" htmlEscape="true"/>
	 			<form:errors path="product.packing" cssClass="error" />
		    <!-- end input field -->  	  
		  	</div>
		  	</div>
		  	
		  	<c:if test="${gSiteConfig['gCASE_CONTENT']}">
		  	<div class="list${classIndex % 2}"><c:set var="classIndex" value="${classIndex+1}" />
		  	<div class="listfl"><fmt:message key="caseContent" />:</div>
		  	<div class="listp">
		  	<!-- input field -->
	 			<form:input path="product.caseContent"  cssClass="textfield" maxlength="8" htmlEscape="true" />
	 			<form:errors path="product.caseContent" cssClass="error"/>
		  	<!-- end input field -->	
		  	</div>
		  	</div>	
		  	</c:if>
		  	
		  	<c:if test="${gSiteConfig['gASI'] != ''}">
		  	<div class="list${classIndex % 2}"><c:set var="classIndex" value="${classIndex+1}" />
		  	<div class="listfl"><div class="helpImg"><img class="toolTipImg" title="Ignore ASI Price::Check this flag to ignore Price provided by ASI." src="../graphics/question.gif" /></div><fmt:message key="ignoreASIPrice" />:</div>
		  	<div class="listp">
		  	<!-- input field -->
	 			<form:checkbox path="product.asiIgnorePrice" />
		    <!-- end input field -->  	  
		  	</div>
		  	</div>		  	
		  	</c:if>


  	        <div class="list${classIndex % 2}"><c:set var="classIndex" value="${classIndex+1}" />
		  	<div class="listfl"><div class="helpImg"><img class="toolTipImg" title="NO Price Override:: Do not allow price over riding while adding invoice" src="../graphics/question.gif" /></div>Don't over ride price:</div>
		  	<div class="listp">
		  	<!-- input field -->
	 			<form:checkbox path="product.restrictPriceChange" />
		    <!-- end input field -->  	  
		  	</div>
		  	</div>	
		  	
		  	<div class="list${classIndex % 2}"><c:set var="classIndex" value="${classIndex+1}" />
		  	<div class="listfl"><fmt:message key="productPrice" /><c:if test="${gSiteConfig['gPRICE_TIERS'] > 1}"> 1</c:if>:</div>
		  	<div class="listp">
		  	<!-- input field -->
		        <div class="listfl0"><form:input path="product.price1" cssClass="textfield75" size="10" maxlength="10"/> ${siteConfig['CURRENCY'].value}</div>
		        <c:if test="${gSiteConfig['gPRICE_TIERS'] > 1}">
		        <div class="listfl0">
		        <c:choose>
		          <c:when test="${gSiteConfig['gMINIMUM_INCREMENTAL_QTY'] and productForm.product.minimumQty != null}">
		            <input type="text" id="priceFrom_1" size="5" value="${productForm.product.minimumQty}" readonly="readonly" class="qtyBreak">
		          </c:when>
		          <c:otherwise>
		            <input type="text" id="priceFrom_1" size="5" value="1" readonly="readonly" class="qtyBreak">
		          </c:otherwise>
		        </c:choose>
		        to <input type="text" id="priceTo_1" size="5" readonly="readonly" class="textfield50" class="qtyBreak">
		        </div>
		        <c:if test="${gSiteConfig['gSUPPLIER'] and  siteConfig['COST_TIERS'].value == 'true'}">
		        	<div class="listfl0"><fmt:message key="productCost" /><c:if test="${gSiteConfig['gPRICE_TIERS'] > 1}"> 1</c:if>:</div>
					<div class="listfl0"><form:input path="product.cost1" cssClass="textfield75" size="10" maxlength="10"/></div>
					<div class="listfl0"><fmt:message key="productMargin" />:</div>	  
					<div class="listfl0"><form:input path="product.marginPercent1" size="10" maxlength="10" disabled="true"/>%</div>  
					<div class="listfl0"><c:out value="((Price1-Cost1)/Price1)*100"/></div> 
				</c:if>
		        </c:if>
		        <form:errors path="product.price1" cssClass="error" delimiter=", "/>
		        <c:if test="${gSiteConfig['gPURCHASE_ORDER'] && siteConfig['SHOW_COST'].value == 'true'}">
		          <a id="mbCost" class="mbCost" title="Cost" rel="width:350,height:260,ajax:true" href="show-ajax-cost.jhtm?id=${productForm.product.id}"><img src="../graphics/magnifier.gif" border="0"></a>
		        </c:if>
		  	<!-- end input field -->	
		  	</div>
		  	</div>
		  	
		  	<c:if test="${gSiteConfig['gPRICE_TIERS'] > 1}">
            <c:forEach begin="2" end="${gSiteConfig['gPRICE_TIERS']}" var="priceTier">
		  	<div class="list${classIndex % 2}"><c:set var="classIndex" value="${classIndex+1}" />
		  	<div class="listfl"><div class="helpImg"><img class="toolTipImg" title="Price::It accept 7 integers and 3 decimals, For emaple,1234567.999." src="../graphics/question.gif" /></div><fmt:message key="productPrice" /> ${priceTier}:</div>
		  	<div class="listp">
		  	<!-- input field -->
		  	<c:set var="tempCost" value=""></c:set>
			    <div class="listfl0"><form:input path="product.price${priceTier}" cssClass="textfield75" size="10" maxlength="10"/></div>
			    <div class="listfl0"><form:input path="product.qtyBreak${priceTier-1}" size="5" maxlength="10" onblur="updateQtyBreak(${priceTier})" id="priceFrom_${priceTier}" cssClass="qtyBreak"/>
			    to <input type="text" id="priceTo_${priceTier}" class="textfield50" size="5" readonly="readonly" class="qtyBreak"></div>
		        <c:if test="${gSiteConfig['gSUPPLIER'] and siteConfig['COST_TIERS'].value == 'true'}">
		        	<div class="listfl0"><fmt:message key="productCost" /><c:if test="${gSiteConfig['gPRICE_TIERS'] > 1}"> ${priceTier}</c:if>:</div>
					<div class="listfl0"><form:input path="product.cost${priceTier}" cssClass="textfield75" size="10" maxlength="10"/></div>
					<div class="listfl0"><fmt:message key="productMargin" />:</div>
					<div class="listfl0"><form:input path="product.marginPercent${priceTier}" size="10" maxlength="10" disabled="true"/> %</div>	        
					<div class="listfl0"><c:out value="((Price${priceTier}-Cost${priceTier})/Price${priceTier})*100"/></div> 
				</c:if>
			    <form:errors path="product.price${priceTier}" cssClass="error" delimiter=", "/>
		    <!-- end input field -->  	  
		  	</div>
		  	</div>	
		  	</c:forEach>
		  	</c:if>
		  	
		  	<div class="list${classIndex % 2}"><c:set var="classIndex" value="${classIndex+1}" />
		  	<div class="listfl"><fmt:message key="hideMsrp" />:</div>
		  	<div class="listp">
		  	<!-- input field -->
     			<form:checkbox path="product.hideMsrp" />
		    <!-- end input field -->  	  
		  	</div>
		  	</div>	
		  	
		  	<div class="list${classIndex % 2}"><c:set var="classIndex" value="${classIndex+1}" />
		  	<div class="listfl"><c:choose><c:when test="${siteConfig['MSRP_TITLE'].value != ''}"><c:out value="${siteConfig['MSRP_TITLE'].value}" /></c:when><c:otherwise><div class="helpImg"><img class="toolTipImg" title="MSRP::It accept 7 integers and 3 decimals, For emaple,1234567.999." src="../graphics/question.gif" /></div><fmt:message key="productMsrp" /></c:otherwise></c:choose>:</div>
		  	<div class="listp">
		  	<!-- input field -->
			    <form:input path="product.msrp" cssClass="textfield75" size="10" maxlength="10" />
			    <form:errors path="product.msrp" cssClass="error" />  
		  	<!-- end input field -->	
		  	</div>
		  	</div>
		  	
		  	<c:if test="${gSiteConfig['gPRICE_TABLE'] > 0}">
            <c:forEach begin="1" end="${gSiteConfig['gPRICE_TABLE']}" var="priceTable">
		  	<div class="list${classIndex % 2}"><c:set var="classIndex" value="${classIndex+1}" />
			    <c:set var="key" value="priceTable${priceTable}"/>
			    <c:choose>
			      <c:when test="${labels[key] != null and labels[key] != ''}">
			        <c:set var="label" value="${labels[key]}"/>
			      </c:when>
			      <c:otherwise><c:set var="label"><fmt:message key="productPriceTable" /> ${priceTable}</c:set></c:otherwise>
			    </c:choose>		        
		  	<div class="listfl"><div class="helpImg"><img class="toolTipImg" title="Price Table::It accept 7 integers and 3 decimals, For emaple,1234567.999." src="../graphics/question.gif" /></div><c:out value="${label}"/>:</div>
		  	<div class="listp">
		  	<!-- input field -->
			    <form:input path="product.priceTable${priceTable}" cssClass="textfield75" size="10" maxlength="10"/>
			    <form:errors path="product.priceTable${priceTable}" cssClass="error" />
		    <!-- end input field -->  	  
		  	</div>
		  	</div>
		  	</c:forEach>
		  	</c:if>	
		  	
		  	<c:if test="${gSiteConfig['gPRICE_CASEPACK']}">
		  	
		  	<div class="list${classIndex % 2}"><c:set var="classIndex" value="${classIndex+1}" />
		  	<div class="listfl"><fmt:message key="casePackQty" />:</div>
		  	<div class="listp">
		  	<!-- input field -->
	 			<form:input path="product.priceCasePackQty"  cssClass="textfield" maxlength="8" htmlEscape="true" />
	 			<form:errors path="product.priceCasePackQty" cssClass="error"/>
		  	<!-- end input field -->	
		  	</div>
		  	</div>
		  	
            <c:forEach begin="1" end="${gSiteConfig['gPRICE_TABLE']}" var="priceTable">
		  	<div class="list${classIndex % 2}"><c:set var="classIndex" value="${classIndex+1}" />       
		  	<div class="listfl"><div class="helpImg"><img class="toolTipImg" title="Price Case Pack::It accept 7 integers and 3 decimals, For emaple,1234567.999." src="../graphics/question.gif" /></div><fmt:message key="priceCasePack" /> ${priceTable}:</div>
		  	<div class="listp">
		  	<!-- input field -->
			    <form:input path="product.priceCasePack${priceTable}" cssClass="textfield75" size="10" maxlength="10"/>
			    <form:errors path="product.priceCasePack${priceTable}" cssClass="error" />
		    <!-- end input field -->  	  
		  	</div>
		  	</div>
		  	</c:forEach>
		  	</c:if>
		  	</div>
	  	
	<!-- end tab -->        
	</div>  

  <c:if test="${gSiteConfig['gINVENTORY']}">
  <h4 title="<fmt:message key='inventory' />"><fmt:message key="inventory" /></h4>
	<div>
	<!-- start tab -->

	  	<div class="listdivi ln tabdivi"></div>
	  	<div class="listdivi"></div>
	   
	  	    	<div class="list${classIndex % 2}"><c:set var="classIndex" value="${classIndex+1}" />
	  			<div class="listfl"><div class="helpImg"><img class="toolTipImg" title="Note::Add more product inventory through Purchase Order" src="../graphics/question.gif" /></div><fmt:message key="available_for_sale" />:</div>
	  			<div class="listp">
	  			<!-- input field -->
	  				<c:choose>
	  					<c:when test="${productForm.newProduct or (siteConfig['INVENTORY_UPDATE_FORM'].value == 'true' && siteConfig['INVENTORY_HISTORY'].value != 'true')}">
	  						<form:input path="product.inventoryAFS" cssClass="textfield50" size="10" maxlength="10" />
			    			<form:errors path="product.inventoryAFS" cssClass="error" />
	  					</c:when>
	  					<c:otherwise>
			    			<c:out value="${productForm.product.inventoryAFS}" />
	  					</c:otherwise>
	  				</c:choose>
			    <!-- end input field -->  	  
	  			</div>
	  			</div> 
		  	    <div class="list${classIndex % 2}"><c:set var="classIndex" value="${classIndex+1}" />
		  		<div class="listfl"><div class="helpImg"><img class="toolTipImg" title="Note::Add more product inventory through Purchase Order" src="../graphics/question.gif" /></div><fmt:message key="inventory_onhand" />:</div>
		  		<div class="listp">
		  		<!-- input field -->
				    	<c:choose>
	  				<c:when test="${productForm.newProduct or (siteConfig['INVENTORY_UPDATE_FORM'].value == 'true' && siteConfig['INVENTORY_HISTORY'].value != 'true')}">
	  					<form:input path="product.inventory" cssClass="textfield50" size="10" maxlength="10" />
			    		<form:errors path="product.inventory" cssClass="error" />
	  				</c:when>
	  				<c:otherwise>
			    		<c:out value="${productForm.product.inventory}" />
	  				</c:otherwise>
	  			</c:choose>
		        	<!-- end input field -->  	  
		  		</div>
		  		</div>
		  	<div class="list${classIndex % 2}"><c:set var="classIndex" value="${classIndex+1}" />
		  	<div class="listfl"><div class="helpImg"><img class="toolTipImg" title="Note::Allows customer to buy this product even if it is out of stock. Always product appears on fronEnd." src="../graphics/question.gif" /></div><fmt:message key="allowToBuyNegInventory" />:</div>
		  	<div class="listp">
		  	<!-- input field -->
		        <form:checkbox path="product.negInventory" />
		        <form:errors path="product.negInventory" cssClass="error" />  
		    <!-- end input field -->   	
		  	</div>
		  	</div>
		  	
		  	<div class="list${classIndex % 2}"><c:set var="classIndex" value="${classIndex+1}" />
		  	<div class="listfl"><div class="helpImg"><img class="toolTipImg" title="Note::Product will be displayed on front end even if inventory is negative. You can use Out of Stock message. SiteInfo -> Inventory Options." src="../graphics/question.gif" /></div><fmt:message key="showNegInventory" />:</div>
		  	<div class="listp">
		  	<!-- input field -->
		        <form:checkbox path="product.showNegInventory" />
		        <form:errors path="product.showNegInventory" cssClass="error" />  
		    <!-- end input field -->   	
		  	</div>
		  	</div>
		  	
		  	<div class="list${classIndex % 2}"><c:set var="classIndex" value="${classIndex+1}" />
		  	<div class="listfl"><div class="helpImg"><img class="toolTipImg" title="Note::If the Inventory of this product falls bellow this number then it will be shown on Low Inventory List. Add Inventory using Purchase Order." src="../graphics/question.gif" /></div><fmt:message key="lowInventoryList" />:</div>
		  	<div class="listp">
		  	<!-- input field -->
		        <form:input path="product.lowInventory" cssClass="textfield50" size="10" maxlength="10" />
			    <form:errors path="product.lowInventory" cssClass="error" />  
		    <!-- end input field -->   	
		  	</div>
		  	</div>
		  	
		  	
	  	
	<!-- end tab -->        
	</div>
	</c:if>  
	
	 <c:if test="${gSiteConfig['gKIT']}">
   <h4 title="seo">Build A Kit</h4>
	<div>
	<!-- start tab -->
		<div class="listdivi ln tabdivi"></div>
	  	<div class="listdivi"></div>
			<div class="list${classIndex % 2}"><c:set var="classIndex" value="${classIndex+1}" />

			<table class="form" border="0" cellspacing="2" cellpadding="0" id="kitParts">
			  <tr>
			    <td><a onClick="addKitParts()"><img src="../graphics/add.png" width="16" border="0"></a></td>
			    <td align="center"><div class="kitPartSku"><fmt:message key="kitPartSku" /></div></td>
			    <td align="center"><div class="kitPartSku"><fmt:message key="quantity" /></div></td>
			  </tr>
			  <c:if test="${empty productForm.product.kitParts}">
			  <tr>
			    <td>&nbsp;</td>
			    <td><input type="text" name="__kitParts_sku" value="" class="textfield250" maxlength="50"></td>
			    <td><input type="text" name="__kitParts_quantity" value="" class="textfield50" maxlength="10"></td>
			  </tr>
			  </c:if>
			  <c:forEach items="${productForm.product.kitParts}" var="kitPart" varStatus="status">
			  <tr>
			    <td>&nbsp;</td>
			    <td><input type="text" name="__kitParts_sku" value="<c:out value="${kitPart.kitPartsSku}"/>" class="textfield250" maxlength="50"></td>
			    <td><input type="text" name="__kitParts_quantity" value="<c:out value="${kitPart.quantity}"/>" class="textfield50" maxlength="10"></td>
			  </tr>
			  </c:forEach>
			</table>	
					  
				<div class="kitPartsInfo"><br>
					- All SKU needs to be unique in this list .<br>
					- To remove a SKU, put the quantity to 0.<br>
				</div>
				
			</div>
			<div class="kitpartError">
				<form:errors path="product.kitParts" cssClass="error"/>
			</div>
	<!-- end tab -->        
	</div>
  </c:if>
	
    <c:if test="${gSiteConfig['gAFFILIATE'] > 0 and siteConfig['NUMBER_PRODUCT_COMMISSION_TABLE'].value > 0}">
	<h4 title="<fmt:message key="affiliate" />"><fmt:message key="affiliate" /></h4>
	<div>
	<!-- start tab -->

	  	<div class="listdivi ln tabdivi"></div>
	  	<div class="listdivi"></div>
	        
	        <c:forEach begin="1" end="${siteConfig['NUMBER_PRODUCT_COMMISSION_TABLE'].value}" var="commissionTable">
		  	<div class="list${classIndex % 2}"><c:set var="classIndex" value="${classIndex+1}" />
		  	<div class="listfl"><fmt:message key="commisionTable" /> ${commissionTable}:</div>
		  	<div class="listp">
		  	<!-- input field -->
                <fmt:message key="${siteConfig['CURRENCY'].value}" /><form:input path="product.commissionTables.commissionTable${commissionTable}" cssClass="textfield" size="10" maxlength="10"/>
                <form:errors path="product.commissionTables.commissionTable${commissionTable}" cssClass="error" />
		    <!-- end input field -->   	
		  	</div>
		  	</div>
		  	</c:forEach>
	  	
	<!-- end tab -->        
	</div> 
	</c:if>
	
	<c:if test="${gSiteConfig['gRECOMMENDED_LIST']}">
	<h4 title="<fmt:message key="recommendedList" />"><fmt:message key="recommendedList" /></h4>
	<div>
	<!-- start tab -->

	  	<div class="listdivi ln tabdivi"></div>
	  	<div class="listdivi"></div>
	   
		  	<div class="list${classIndex % 2}"><c:set var="classIndex" value="${classIndex+1}" />
		  	<div class="listfl"><div class="helpImg"><img class="toolTipImg" title="Recommended List::Mention multiple SKUs comma seperated. These Skus will appear on frontend with this product." src="../graphics/question.gif" /></div><fmt:message key="productSkus" />:</div>
		  	<div class="listp">
		  	<!-- input field -->
         		<form:textarea path="product.recommendedList" cssClass="textfield" rows="5" cols="35" htmlEscape="true" />
		    <!-- end input field -->   	
		  	</div>
		  	</div>
		    
		  	<div class="list${classIndex % 2}"><c:set var="classIndex" value="${classIndex+1}" />
		  	<div class="listfl"><fmt:message key="recommendedList" /> <fmt:message key="title" />:</div>
		  	<div class="listp">
		  	<!-- input field -->
         		<form:input path="product.recommendedListTitle" cssClass="textfield" htmlEscape="true" />
		    <!-- end input field -->  	  
		  	</div>
		  	</div>
		  
		  	<div class="list${classIndex % 2}"><c:set var="classIndex" value="${classIndex+1}" />
		  	<div class="listfl"><fmt:message key="recommendedList" /> <fmt:message key="display" />:</div>
		  	<div class="listp">
		  	<!-- input field -->
		        <form:select path="product.recommendedListDisplay" cssClass="textfield" >
		          <form:option value=""><fmt:message key="global"/></form:option> 
		          <form:option value="1">1. <fmt:message key="default" /></form:option>
		          <form:option value="2">2. <fmt:message key="default" /> 2</form:option>
		          <form:option value="3">3. compact up</form:option>
		          <form:option value="4">4. compact short Desc.</form:option>
		          <form:option value="5">5. compact down</form:option>
		          <form:option value="6">6. compact popup</form:option>
		          <form:option value="7">7. compact</form:option>
		          <form:option value="8">8. compact one add to cart</form:option>
		          <form:option value="9">9. compact first price</form:option>
		          <form:option value="10">10. divs</form:option>
		          <form:option value="11">11. price break</form:option>
		          <form:option value="12">12. compact one add to cart (MSRP)</form:option>
		          <form:option value="quick"><fmt:message key="quickMode"/></form:option>
		          <c:if test="${siteConfig['PRODUCT_QUICK_MODE_2'].value == 'true'}">
		          	<form:option value="quick2"><fmt:message key="quickMode"/></form:option>
		          </c:if>		          
				  <c:forEach items="${productForm.productFields}" var="productField" varStatus="status">
		          <form:option value="field${productField.id}">Drop Down -- <c:out value="${productField.name}"/> (Field <c:out value="${productField.id}"/>)</form:option>		          
				  </c:forEach>
		        </form:select>
		  	<!-- end input field -->	
		  	</div>
		  	</div>	
	  	
	<!-- end tab -->        
	</div> 
	</c:if>
	
	<c:if test="${gSiteConfig['gPRODUCT_VARIANTS']}">
    <h4 title="<fmt:message key="productVariants" />"><fmt:message key="variants" /></h4>
	<div>
	<!-- start tab -->
		<div class="listdivi ln tabdivi"></div>
	  	<div class="listdivi"></div>
			<div class="list${classIndex % 2}"><c:set var="classIndex" value="${classIndex+1}" />

			<table class="form" border="0" cellspacing="2" cellpadding="0" id="variants">
			  <tr>
			    <td><a onClick="addVariant()"><img src="../graphics/add.png" width="16" border="0"></a></td>
			    <td align="center"><div class="requiredField"><fmt:message key="sku" /></div></td>
			    <td align="center"><div class="requiredField"><fmt:message key="name" /></div></td>
			    <td align="center" style="color:#676767;"><"<fmt:message key="shortDesc" /></td>
			    <td align="center"><div class="requiredField"><fmt:message key="price" /></div></td>
			  </tr>
			  <c:if test="${empty productForm.product.variants}">
			  <tr>
			    <td>&nbsp;</td>
			    <td><input type="text" name="__variant_sku" value="" class="textfield250" maxlength="50"></td>
			    <td><input type="text" name="__variant_name" value="" class="textfield250" maxlength="120"></td>
			    <td><input type="text" name="__variant_shortDesc" value="" class="textfield250" maxlength="512"></td>
			    <td><input type="text" name="__variant_price" value="" class="textfield50" maxlength="10"></td>
			  </tr>
			  </c:if>
			  <c:forEach items="${productForm.product.variants}" var="variant" varStatus="status">
			  <tr>
			    <td>&nbsp;</td>
			    <td><input type="text" name="__variant_sku" value="<c:out value="${variant.sku}"/>" class="textfield250" maxlength="50"></td>
			    <td><input type="text" name="__variant_name" value="<c:out value="${variant.name}"/>" class="textfield250" maxlength="120"></td>
			    <td><input type="text" name="__variant_shortDesc" value="<c:out value="${variant.shortDesc}"/>" class="textfield250" maxlength="512"></td>
			    <td><input type="text" name="__variant_price" value="<c:out value="${variant.price1}"/>" class="textfield50" maxlength="10"></td>
			  </tr>
			  </c:forEach>
			</table>	
					  
				<div class="sup">
					- SKU needs to be unique per product.<br/>
					- SKU is required to save variant in database.<br/>
					- SKU, Name, and Price are required to show on front-end.<br/>
				</div>
				
			</div>

	<!-- end tab -->        
	</div> 
    </c:if>	
	
	<c:if test="${gSiteConfig['gSUBSCRIPTION']}">
	<h4 title="<fmt:message key="subscription" />"><fmt:message key="subscription" /></h4>
	<div>
	<!-- start tab -->

	  	<div class="listdivi ln tabdivi"></div>
	  	<div class="listdivi"></div>
		    
		  	<div class="list${classIndex % 2}"><c:set var="classIndex" value="${classIndex+1}" />
		  	<div class="listfl"><fmt:message key="Enabled" />:</div>
		  	<div class="listp">
		  	<!-- input field -->
         		<form:checkbox path="product.subscription" />
		    <!-- end input field -->  	  
		  	</div>
		  	</div>	

		  	<div class="list${classIndex % 2}"><c:set var="classIndex" value="${classIndex+1}" />
		  	<div class="listfl"><div class="helpImg"><img class="toolTipImg" title="Note::Discount will only apply if price of item didn't come from price table or special pricing. If item has a valid sales tag, discount will be applied to the discounted price." src="../graphics/question.gif" /></div><fmt:message key='discount' />:</div>
		  	<div class="listp">
		  	<!-- input field -->
				<form:input path="product.subscriptionDiscount" size="5" maxlength="5"/>% 
				<form:errors path="product.subscriptionDiscount" cssClass="error"/>
				<span class="helpNote"><p><fmt:message key='maxis100' /></p></span>
	 		<!-- end input field -->	
	 		</div>
		  	</div>	
	  	
	<!-- end tab -->        
	</div> 
	</c:if>

    <c:if test="${gSiteConfig['gTAB'] > 0}">
	<h4 title="<fmt:message key="tab" />"><fmt:message key="tab" /></h4>
	<div>
	<!-- start tab -->

	  	<div class="listdivi ln tabdivi"></div>
	  	<div class="listdivi"></div>

            <c:forEach begin="1" end="${gSiteConfig['gTAB']}" var="tabNumber">
		  	<div class="list${classIndex % 2}"><c:set var="classIndex" value="${classIndex+1}" />
		  	<div class="listfl">Tab${tabNumber} Name:</div>
		  	<div class="listp">
		  	<!-- input field -->
			    <form:input path="product.tab${tabNumber}" cssClass="textfield" htmlEscape="true" />
			    <form:errors path="product.tab${tabNumber}" cssClass="error" />
		    <!-- end input field -->  	  
		  	</div>
		  	<br />
		  	<div class="listfl">Tab${tabNumber} Content:</div>
		  	<div class="listp">
		  	<!-- input field -->
	           <form:textarea path="product.tab${tabNumber}Content" cssClass="textfield" rows="15" cols="35" htmlEscape="true" />
	             <c:if test="${siteConfig['DETAILS_LONG_DESC_HTML'].value != 'false'}">
			    <div style="text-align:left" id="product.tab${tabNumber}Content_link"><a href="#" onClick="loadEditor('product.tab${tabNumber}Content')" class="htmleditor">- <fmt:message key="htmlEditor" /> -</a></div>
			    </c:if>
		    <!-- end input field -->   	
		  	</div>
		  	</div>
		  	</c:forEach>
		  	
	<!-- end tab -->        
	</div> 
	</c:if>	
	
	<h4 title="<fmt:message key="image" />"><fmt:message key="image" /></h4>
	<div>
	<!-- start tab -->

	  	<div class="listdivi ln tabdivi"></div>
	  	<div class="listdivi"></div>
	  	
	        <c:forEach items="${productForm.productImages}" var="image" varStatus="status">
		  	<div class="list${classIndex % 2}"><c:set var="classIndex" value="${classIndex+1}" />
		  	<div class="listfl"><div class="helpImg"><img class="toolTipImg" title="Image::Upload the image of the product. Max Image size is 2MB." src="../graphics/question.gif" /></div><fmt:message key="categoryName" />:</div>
		  	<div class="listp">
		  	<!-- input field -->

		      <c:if test="${image.imageUrl != null}">
			   <c:choose>
			    <c:when test="${image.absolute}">
			      <div class="thumbbox">
					<p class="thumb">
			          <img src="<c:url value="${image.imageUrl}"/>?rnd=${randomNum}" border="0" height="100">
			        </p>
				  </div>  
			    </c:when>
			    <c:otherwise>
			      <div class="thumbbox">
					<p class="thumb">
					  <img src="<c:url value="/assets/Image/Product/detailsbig/${image.imageUrl}"/>?rnd=${randomNum}" border="0" height="100">
					</p>
					<p class="uname">
					<c:out value="${image.imageUrl}" />
					</p>
				  </div>
			    </c:otherwise>
			   </c:choose>
			  <input name="__clear_image_${image.index}" type="checkbox"> <fmt:message key="remove" />
		      </c:if>
		      <c:if test="${image.imageUrl == null}">
			    <input value="browse" class="textfield" type="file" name="image_file_${image.index}"/>  
		      </c:if>
     
		    <!-- end input field -->   	
		  	</div>
		  	</div>
		  	</c:forEach>
		    
		  	<div class="list${classIndex % 2}"><c:set var="classIndex" value="${classIndex+1}" />
		  	<div class="listfl"><div class="helpImg"><img class="toolTipImg" title="Folder Name::All the images under this folder will be displayed with this product, at the product's front end." src="../graphics/question.gif" /></div><fmt:message key="folder" /> <fmt:message key="Name" />:</div>
		  	<div class="listp">
		  	<!-- input field -->
       			<form:input id="imageFolder" path="product.imageFolder" cssClass="textfield" htmlEscape="true"/>
		    <!-- end input field -->  	  
		  	</div>
		  	</div>
		  
		    <c:if test="${siteConfig['PRODUCT_IMAGE_LAYOUTS'].value != ''}">   
		  	<div class="list${classIndex % 2}"><c:set var="classIndex" value="${classIndex+1}" />
		  	<div class="listfl"><fmt:message key="layout" />:</div>
		  	<div class="listp">
		  	<!-- input field -->
		        <form:select path="product.imageLayout" id="imageLayout" onchange="toggleImageFolder()">
		          <form:option value=""><fmt:message key="global"/></form:option> 
		          <c:forTokens items="${siteConfig['PRODUCT_IMAGE_LAYOUTS'].value}" delims="," var="slideshow">
		            <form:option value="${slideshow}"><fmt:message key="${slideshow}" /></form:option>
		          </c:forTokens>
		       </form:select>
		  	<!-- end input field -->	
		  	</div>
		  	</div>	
		  	</c:if>
		  	
	  	
	<!-- end tab -->        
	</div> 

	<h4 title="<fmt:message key="note" />"><fmt:message key="note" /></h4>
	<div>
	<!-- start tab -->

	  	<div class="listdivi ln tabdivi"></div>
	  	<div class="listdivi"></div>

		  	<div class="list${classIndex % 2}"><c:set var="classIndex" value="${classIndex+1}" />
		  	<div class="listfl"><div class="helpImg"><img class="toolTipImg" title="Note:: Enter the notes related to product here. Admin only." src="../graphics/question.gif" /></div>Note:</div>
		  	<div class="listp">
		  	<!-- input field -->
			    <form:textarea path="product.note" cssClass="textArea700x400" htmlEscape="true" />
			    <form:errors path="product.note" cssClass="error" />
		    <!-- end input field -->  	  
		  	</div>
		  	</div>
		  	
		  	<c:if test="${gSiteConfig['gASI'] != ''}">
		  	<sec:authorize ifAnyGranted="ROLE_AEM,ROLE_ADMIN_${prop_site_id}">
		  	<div class="list${classIndex % 2}"><c:set var="classIndex" value="${classIndex+1}" />
		  	<div class="listfl">ASI XML:</div>
		  	<div class="listp">
		  	<!-- input field -->
			    <form:textarea path="product.asiXML" cssClass="textArea700x400" htmlEscape="true" disabled="true"/>
			    <form:errors path="product.asiXML" cssClass="error" />
		    <!-- end input field -->  	  
		  	</div>
		  	</div>
		  	</sec:authorize>
		  	</c:if>
		  	
	<!-- end tab -->        
	</div>

	<c:if test="${siteConfig['SITEMAP'].value == 'true'}">
    <h4 title="seo">SEO</h4>
	<div>
	<!-- start tab -->
		<div class="listdivi ln tabdivi"></div>
	  	<div class="listdivi"></div>
  		  	<div class="list${classIndex % 2}"><c:set var="classIndex" value="${classIndex+1}" />
		  	<div class="listfl"><div class="helpImg"><img class="toolTipImg" title="Site Map Priority::Select product priority for Site Map. Default priority is 0.5" src="../graphics/question.gif" /></div><fmt:message key="priority" />:</div>
		  	<div class="listp">
		  	<!-- input field -->
		  	   <form:select path="product.siteMapPriority">
		  	     <c:forEach begin="0" end="10" step="1" var="priority">
		  	       <form:option value="${priority/10}" label="${priority/10}"></form:option>
		  	     </c:forEach>
		  	   </form:select>
			<!-- end input field -->	
		  	</div>
		 </div>
	<!-- end tab -->        
	</div> 
    </c:if>
  
    
    <c:forEach items="${languageCodes}" var="i18n">
	<h4 title="<fmt:message key="language_${i18n.languageCode}"/>"><img src="../graphics/${i18n.languageCode}.gif" border="0" title="<fmt:message key="language_${i18n.languageCode}"/>"> <fmt:message key="language_${i18n.languageCode}"/></h4>
	<div>
	<!-- start tab -->

	  	<div class="listdivi ln tabdivi"></div>
	  	<div class="listdivi"></div>
	   
		  	<div class="list${classIndex % 2}"><c:set var="classIndex" value="${classIndex+1}" />
		  	<div class="listfl"><fmt:message key="productName" />:</div>
		  	<div class="listp">
		  	<!-- input field -->
			  	<input type="text" name="__i18nName_${i18n.languageCode}" value="<c:out value="${productForm.product.i18nProduct[i18n.languageCode].i18nName}"/>" class="textfield" maxlength="120" size="50">
		    <!-- end input field -->   	
		  	</div>
		  	</div>

		  	<div class="list${classIndex % 2}"><c:set var="classIndex" value="${classIndex+1}" />
		  	<div class="listfl"><fmt:message key="productShortDesc" />:</div>
		  	<div class="listp">
		  	<!-- input field -->
			  	<input type="text" name="__i18nShortDesc_${i18n.languageCode}" value="<c:out value="${productForm.product.i18nProduct[i18n.languageCode].i18nShortDesc}"/>" class="textfield" maxlength="512" size="50">
		    <!-- end input field -->	
		  	</div>
		  	</div>
		  	
		  	<div class="list${classIndex % 2}"><c:set var="classIndex" value="${classIndex+1}" />
		  	<div class="listfl"><fmt:message key="productLongDesc" />:</div>
		  	<div class="listp">
		  	<!-- input field -->
	            <textarea name="__i18nLongDesc_${i18n.languageCode}" id="__i18nLongDesc_${i18n.languageCode}" rows="15" cols="60" class="textfield"><c:out value="${productForm.product.i18nProduct[i18n.languageCode].i18nLongDesc}"/></textarea>		  	
	   			<c:if test="${siteConfig['DETAILS_LONG_DESC_HTML'].value != 'false'}">
			    <div style="text-align:left" id="__i18nLongDesc_${i18n.languageCode}_link"><a href="#" onClick="loadEditor('__i18nLongDesc_${i18n.languageCode}')" class="htmleditor">- <fmt:message key="htmlEditor" /> -</a></div>
			    </c:if>
		  	<!-- end input field --> 
		  	</div>
		  	</div>	

  			<c:forEach items="${productForm.productFields}" var="productField" varStatus="status"> 
		  	<div class="list${classIndex % 2}"><c:set var="classIndex" value="${classIndex+1}" />
		  	<div class="listfl">
		  		<c:out value="${i18nProductFields[i18n.languageCode][productField.id].name}"/>:
		  	</div>
		  	<div class="listp">
		  	<!-- input field -->
		  	    <c:set var="i18nField" value="field${productField.id}"/>
		  	    <%-- pageContext.setAttribute("i18nField", "field" + pageContext.getAttribute("productFieldId")); --%>
		  	    <c:choose>
			     <c:when test="${!empty i18nProductFields[i18n.languageCode][productField.id].preValue}">
			       <select name="__i18nProductField${productField.id}_${i18n.languageCode}" class="productPreValueField">
			         <option value="">Please Select</option>
			         <c:forTokens items="${i18nProductFields[i18n.languageCode][productField.id].preValue}" delims="," var="dropDownValue">
			           <option value="${dropDownValue}" <c:if test="${dropDownValue == productForm.product.i18nProduct[i18n.languageCode][i18nField]}">selected</c:if>>${dropDownValue}</option>
			         </c:forTokens>
			       </select>
			     </c:when>
			     <c:otherwise>
			  	  <input type="text" name="__i18nProductField${productField.id}_${i18n.languageCode}" value="<c:out value="${productForm.product.i18nProduct[i18n.languageCode][i18nField]}"/>" class="textfield" maxlength="255" size="70">
			     </c:otherwise>
			     </c:choose>
			     (Field <c:out value="${productField.id}"/>)
		    <!-- end input field -->   	
		  	</div>
		  	</div>
  </c:forEach>	
		  		    
	<!-- end tab -->		
	</div>	
	</c:forEach>
	
		
	

<!-- end tabs -->			
</div>

<!-- start button -->
<div align="left" class="button"> 
<c:if test="${productForm.newProduct}">
  <sec:authorize ifAnyGranted="ROLE_AEM,ROLE_ADMIN_${prop_site_id},ROLE_PRODUCT_CREATE">
    <input type="submit" value="<spring:message code="productAdd"/>">
  </sec:authorize>  
</c:if>
<c:if test="${!productForm.newProduct}">
  <sec:authorize ifAnyGranted="ROLE_AEM,ROLE_ADMIN_${prop_site_id},ROLE_PRODUCT_EDIT">
    <input type="submit" value="<spring:message code="productUpdate"/>" <c:if test="${gSiteConfig['gSHOPPING_CART']}">onClick="document.getElementById('retainOnCart').value='true'"</c:if>>
    <c:if test="${gSiteConfig['gSHOPPING_CART']}">
      <input type="submit" value="<spring:message code="productUpdate"/> (<fmt:message key="removeFromCart" />)" onClick="document.getElementById('retainOnCart').value='false'">
	  <form:hidden id="retainOnCart" path="product.retainOnCart" />
	</c:if>    
  </sec:authorize>  
  <sec:authorize ifAnyGranted="ROLE_AEM,ROLE_ADMIN_${prop_site_id},ROLE_PRODUCT_DELETE">
    <input type="submit" value="<spring:message code="productDelete"/>" name="_delete" onClick="return confirm('Delete permanently?')" />
  </sec:authorize>
</c:if>
  <input type="submit" value="<spring:message code="cancel"/>" name="_cancel">
</div>
<!-- end button -->	     
</form:form>  
  <!-- end table -->
    </td><td class="boxmidr" ></td></tr>
  <tr><td class="botl"></td><td class="botr"></td></tr>
  </table>
  
<!-- end main box -->  
</div>


<script type="text/javascript"> 
<!--	
<c:if test="${gSiteConfig['gVARIABLE_PRICE']}">
if (document.getElementById('product.priceByCustomer').checked) 
	{
	  document.getElementById('fixPrice').style.display="none";
	} else 
	{
	  document.getElementById('fixPrice').style.display="block";
	}
</c:if>	
<c:if test="${fn:contains(siteConfig['PRODUCT_IMAGE_LAYOUTS'].value,'ss')}" >
function toggleImageFolder() {
  if (document.getElementById('imageLayout').value == "ss") {
      document.getElementById('imageFolder').disabled=true;
  } else {
      document.getElementById('imageFolder').disabled=false;
  }
}
toggleImageFolder();
</c:if>
//-->
</script>	

 </tiles:putAttribute>    
</tiles:insertDefinition>