<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="/WEB-INF/tlds/spring.tld" prefix="spring" %>

<tiles:insertDefinition name="admin.catalog.product" flush="true">
  <tiles:putAttribute name="content" type="string">
<script language="JavaScript" type="text/JavaScript">
<!--
window.addEvent('domready', function(){			
			var tabs1 = new SimpleTabs($('tab-block-1'), { selector: 'h4'});
		});
//-->
</script>
<!-- main box -->
  <div id="mboxfull">
  
  <!-- start table -->
  <table cellpadding="0" cellspacing="0" border="0" class="module" >
  <tr>
    <td class="topl_g">
	  
	  <!-- breadcrumb -->
	  <p class="breadcrumb">
	    <a href="../catalog">Catalog</a> &gt;
	    <a href="../catalog/productList.jhtm">Products</a> &gt;
	    import
	  </p>
	  
	  <!-- Error Message -->
	  <c:if test="${!empty message}">
  		<div class="error"><spring:message code="${message}" arguments="${arguments}"/></div>
	  </c:if>  

  
  </td><td class="topr_g" ></td></tr>
  <tr><td class="boxmidlrg" >

 <!-- tabs -->
 <div id="tab-block-1">
	<h4 title="Import">Import</h4>
	<div>
	<!-- start tab -->

        <c:set var="classIndex" value="0" />
        
	  	<div class="listdivi ln tabdivi"></div>
	  	<div class="listdivi"></div>
	   
		  	<div class="listlight">
		  	<!-- input field -->
			
				<c:if test="${updatedItems != null}">
				<div class="message">Updated Items</div>
				<table border="0" cellpadding="0" cellspacing="0" width="100%" style="padding-left:5px;padding-right:5px;">
				<tr height="20">
				  <td class="importIndexCol importHdr">&nbsp;</td>
				  <td class="importHdr"><spring:message code="productId"/></td>
				  <td class="importHdr" align="center"><spring:message code="productSku"/></td>
				  <td class="importHdr"><c:if test="${hasNameCol}"><spring:message code="productName"/></c:if></td>
				  <td class="importHdr">Comments</td>  
				</tr>
				<c:forEach items="${updatedItems}" var="item" varStatus="status">
				<tr height="20" class="row${status.index % 2}" onmouseover="changeStyleClass(this,'rowOver')" onmouseout="changeStyleClass(this,'row${status.index % 2}')">
				  <td class="importIndexCol import"><c:out value="${status.count}"/>.</td>
				  <td class="import" align="center"><c:out value="${item['id']}"/></td>
				  <td class="import" align="center"><c:out value="${item['sku']}"/></td>
				  <td class="import"><c:if test="${hasNameCol}"><c:out value="${item['name']}"/></c:if></td>
				  <td class="import" width="100%"><c:out value="${item['comments']}"/></td>
				</tr>
				</c:forEach>
				</table>
				</c:if>
				
				<c:if test="${addedItems != null}">
				<br><br>
				<div class="message">Added Items</div>
				<table border="0" cellpadding="0" cellspacing="0" width="100%" style="padding-left:5px;padding-right:5px;">
				<tr height="20">
				  <td class="importIndexCol importHdr">&nbsp;</td>
				  <td class="importHdr"><spring:message code="productId"/></td>
				  <td class="importHdr" align="center"><spring:message code="productSku"/></td>
				  <td class="importHdr"><c:if test="${hasNameCol}"><spring:message code="productName"/></c:if></td>
				  <td class="importHdr">Comments</td>  
				</tr>
				<c:forEach items="${addedItems}" var="item" varStatus="status">
				<tr height="20" class="row${status.index % 2}" onmouseover="changeStyleClass(this,'rowOver')" onmouseout="changeStyleClass(this,'row${status.index % 2}')">
				  <td class="importIndexCol import"><c:out value="${status.count}"/>.</td>
				  <td class="import" align="center"><c:out value="${item['id']}"/></td>
				  <td class="import" align="center"><c:out value="${item['sku']}"/></td>
				  <td class="import"><c:if test="${hasNameCol}"><c:out value="${item['name']}"/></c:if></td>
				  <td class="import" width="100%"><c:out value="${item['comments']}"/></td>
				</tr>
				</c:forEach>
				</table>
				</c:if>
				
		    <!-- end input field -->   	
	  	
	<!-- end tab -->        
	</div> 
	
<!-- end tabs -->			
</div>

<!-- start button -->
<!-- end button -->	     
  
  <!-- end table -->
    </td><td class="boxmidr" ></td></tr>
  <tr><td class="botl"></td><td class="botr"></td></tr>
  </table>
  
<!-- end main box -->  
 </div>

  </tiles:putAttribute>    
</tiles:insertDefinition>