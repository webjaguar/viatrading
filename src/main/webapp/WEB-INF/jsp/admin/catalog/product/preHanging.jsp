<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>

<tiles:insertDefinition name="admin.catalog.product" flush="true">
  <tiles:putAttribute name="content" type="string">
  
<script language="JavaScript" type="text/JavaScript">
<!--
window.addEvent('domready', function(){			
			var tabs1 = new SimpleTabs($('tab-block-1'), { selector: 'h4'});
			var Tips1 = new Tips($$('.toolTipImg'));
		});
function deleteSelected() {
	var ids = document.getElementsByName("__selected_id");	
  	if (ids.length == 1) {
      if (document.list_form.__selected_id.checked) {
    	return confirm('Delete permanently?');
      }
    } else {
      for (i = 0; i < ids.length; i++) {
        if (document.list_form.__selected_id[i].checked) {
    	  return confirm('Delete permanently?');
        }
      }
    }

    alert("Please select item(s) to delete.");       
    return false;
} 
function addPreHanging() {
	  var tr = document.createElement("tr");

	  tr.appendChild(document.createElement("td"));
	  tr.appendChild(document.createElement("td"));
		  
	  var td1 = document.createElement("td");  
	  var textfield1 = document.createElement("input");
	  textfield1.setAttribute("type", "text"); 
	  textfield1.setAttribute("name", "__prehang_doorConfig"); 
	  textfield1.setAttribute("maxlength", "50");
	  textfield1.setAttribute("size", "20");
	  td1.appendChild(textfield1);
	  tr.appendChild(td1);

	  var td2 = document.createElement("td");  
	  td2.setAttribute("align", "center");
	  var textfield2 = document.createElement("input");
	  textfield2.setAttribute("type", "text"); 
	  textfield2.setAttribute("name", "__prehang_species"); 
	  textfield2.setAttribute("maxlength", "50");
	  textfield2.setAttribute("size", "20");
	  td2.appendChild(textfield2);
	  tr.appendChild(td2);

	  var td3 = document.createElement("td");  
	  td3.setAttribute("align", "center");
	  var textfield3 = document.createElement("input");
	  textfield3.setAttribute("type", "text"); 
	  textfield3.setAttribute("name", "__prehang_widthHeight"); 
	  textfield3.setAttribute("maxlength", "50");
	  textfield3.setAttribute("size", "20");
	  td3.appendChild(textfield3);
	  tr.appendChild(td3);

	  var td4 = document.createElement("td");  
	  td4.setAttribute("align", "center");
	  var textfield4 = document.createElement("input");
	  textfield4.setAttribute("type", "text"); 
	  textfield4.setAttribute("name", "__prehang_price"); 
	  textfield4.setAttribute("maxlength", "10");
	  textfield4.setAttribute("size", "10");
	  td4.appendChild(textfield4);
	  tr.appendChild(td4);

	  var td5 = document.createElement("td");  
	  td5.setAttribute("align", "center");
	  var textfield4 = document.createElement("input");
	  textfield4.setAttribute("type", "text"); 
	  textfield4.setAttribute("name", "__prehang_dimensions"); 
	  textfield4.setAttribute("maxlength", "50");
	  textfield4.setAttribute("size", "20");
	  td5.appendChild(textfield4);
	  tr.appendChild(td5);
	  
	  document.getElementById('listings').appendChild(tr);
}
//-->
</script>	

<form action="preHanging.jhtm" name="list_form" method="post">
<!-- main box -->
  <div id="mboxfull">
  
  <!-- start table -->
  <table cellpadding="0" cellspacing="0" border="0" class="module" >
  <tr>
    <td class="topl_g">
	  
	  <!-- breadcrumb -->
	  <p class="breadcrumb">
	     <a href="../catalog">Catalog</a> &gt;
	     <a href="../product">Product</a> &gt;
	    Pre-Hanging 
	  </p>
        
	  <!-- Error -->
	  <c:if test="${!empty message}">
		 <div class="message"><fmt:message key="${message}" /></div>
	  </c:if>
	  
  </td><td class="topr_g" ></td></tr>
  <tr><td class="boxmidlrg" >

 <!-- tabs -->
 <div id="tab-block-1">
	<h4 title="Pre-Hanging">Pre-Hanging</h4>
	<div>
	<!-- start tab -->

         <c:set var="classIndex" value="0" />
         <c:set var="cols" value="0"/>
         
	  	<div class="listdivi ln tabdivi"></div>
	  	<div class="listdivi"></div>
	   
		  	<div class="list${classIndex % 2}"><c:set var="classIndex" value="${classIndex+1}" />
		  	<div class="listpp">

			<table border="0" cellpadding="0" cellspacing="0" width="100%" id="listings" class="listings">
			  <tr class="listingsHdr2">
			    <td width="1%" align="center">&nbsp;</td>
			    <td width="5%" class="indexCol">&nbsp;</td>
			    <td class="listingsHdr3" width="*">Door Configuration</td>
			    <td class="listingsHdr3" width="18%" align="center">SPECIES</td>
			    <td class="listingsHdr3" width="18%" align="center">WIDTH &amp; HEIGHT</td>
			    <td class="listingsHdr3" width="15%" align="center">Price</td>
			    <td class="listingsHdr3" width="18%" align="center">Net Frame Dimension</td>
			  </tr>
			  <c:forEach items="${preHanging}" var="preHanging" varStatus="status">
			  <tr class="row${status.index % 2}" >
			    <td align="center"><input name="__selected_id" value="${preHanging['id']}" type="checkbox"></td>
			    <td class="indexCol"><input name="_id" value="${preHanging['id']}" type="hidden"><c:out value="${status.index + 1}"/>.</td>
			    <td class="nameCol"><c:out value="${preHanging['door_config']}"/></td>
			    <td align="center"><c:out value="${preHanging['species']}"/></td>
			    <td align="center"><c:out value="${preHanging['width_height']}"/></td>
			    <td align="center"><input name="__price_${preHanging['id']}" value="<c:out value="${preHanging['price']}"/>" maxlength="10" size="10"></td>
			    <td align="center"><input name="__dimensions_${preHanging['id']}" value="<c:out value="${preHanging['dimensions']}"/>" maxlength="50" size="20"></td>
			  </tr>
			  </c:forEach>
			  <tr class="listingsHdr2">
			    <td align="center"><a onClick="addPreHanging()"><img src="../graphics/add.png" width="16" border="0"></a></td>
			    <td class="indexCol">&nbsp;</td>
			    <td class="listingsHdr3">Door Configuration</td>
			    <td class="listingsHdr3" align="center">SPECIES</td>
			    <td class="listingsHdr3" align="center">WIDTH &amp; HEIGHT</td>
			    <td class="listingsHdr3" align="center">Price</td>
			    <td class="listingsHdr3" align="center">Net Frame Dimension</td>
			  </tr>
			</table>
	
	 		</div>
		  	</div>	
	  	
	<!-- end tab -->        
	</div>         

<!-- end tabs -->			
</div>

<!-- start button -->
  <div align="left" class="button"> 
      <input type="submit" name="__update" value="<fmt:message key="Update"/>">
	  <c:if test="${not empty preHanging}">
		<input type="submit" name="__delete" value="Delete Selected Items" onClick="return deleteSelected()">
	  </c:if>  
  </div>
<!-- end button -->	
  
  <!-- end table -->
    </td><td class="boxmidr" ></td></tr>
  <tr><td class="botl"></td><td class="botr"></td></tr>
  </table>
  
<!-- end main box -->  
</div>
</form>

  </tiles:putAttribute>    
</tiles:insertDefinition>

