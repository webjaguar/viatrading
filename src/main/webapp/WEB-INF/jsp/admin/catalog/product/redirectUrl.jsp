<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="/WEB-INF/tlds/spring.tld" prefix="spring" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ taglib uri="http://www.springframework.org/security/tags" prefix="sec" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ page contentType="text/html;charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib uri="/WEB-INF/tlds/custom.tld" prefix="wj" %>
<div class="list${classIndex % 2}"><c:set var="classIndex" value="${classIndex+1}" />
  	<div class="listfl">
     Redirect URL English:</div>
  	<div class="listp">
  	<!-- input field -->
  			<form:input path="product.redirectUrlEn" cssClass="textfield" htmlEscape="true"/>
  			<form:errors path="product.redirectUrlEn" cssClass="error" />
  	<!-- end input field -->	
   </div>
</div>

<div class="list${classIndex % 2}"><c:set var="classIndex" value="${classIndex+1}" />
  	<div class="listfl">
     Redirect URL Spanish:</div>
  	<div class="listp">
  	<!-- input field -->
  			<form:input path="product.redirectUrlEs" cssClass="textfield" htmlEscape="true"/>
  			<form:errors path="product.redirectUrlEs" cssClass="error" />
  	<!-- end input field -->	
   </div>
</div>