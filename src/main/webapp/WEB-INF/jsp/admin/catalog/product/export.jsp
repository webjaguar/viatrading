<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="/WEB-INF/tlds/spring.tld" prefix="spring" %>
<%@ taglib uri="http://www.springframework.org/security/tags" prefix="sec" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>

<tiles:insertDefinition name="admin.catalog.product" flush="true">
  <tiles:putAttribute name="content" type="string">
<sec:authorize ifAnyGranted="ROLE_AEM,ROLE_ADMIN_${prop_site_id},ROLE_PRODUCT_IMPORT_EXPORT">
<script language="JavaScript" type="text/JavaScript">
<!--
window.addEvent('domready', function(){			
			var tabs1 = new SimpleTabs($('tab-block-1'), { selector: 'h4'});
			var Tips1 = new Tips($$('.toolTipImg'));
			$$('.highlight').each(function(el) {
				var end = el.getStyle('background-color');
				end = (end == 'transparent') ? '#fff' : end;
				var myFx = new Fx.Tween(el, {duration: 700, wait: false});
				myFx.start('background-color', '#f00', end);
			});
		});
//-->
</script>
<form:form commandName="exportProductSearch" method="post">
<!-- main box -->
  <div id="mboxfull">
  
  <!-- start table -->
  <table cellpadding="0" cellspacing="0" border="0" class="module" >
  <tr>
    <td class="topl_g">
	  
	  <!-- breadcrumb -->
	  <p class="breadcrumb">
	    <a href="../catalog">Catalog</a> &gt;
	    <a href="../catalog/productList.jhtm">Products</a> &gt;
	    export
	  </p>
	  
	  <!-- Error Message -->
	  <c:if test="${!empty message}">
		 <div class="highlight error"><spring:message code="${message}" arguments="${arguments}"/></div>
	  </c:if>
	  
  </td><td class="topr_g" ></td></tr>
  <tr><td class="boxmidlrg" >

 <!-- tabs -->
 <div id="tab-block-1">
	<h4 title="export">Export</h4>
	<div>
	<!-- start tab -->

        <c:set var="classIndex" value="0" />
        
	  	<div class="listdivi ln tabdivi"></div>
	  	<div class="listdivi"></div>
		  	
		  	<div class="list${classIndex % 2}"><c:set var="classIndex" value="${classIndex+1}" />
		  	<div class="listfl"><fmt:message key="type" />:</div>
		  	<div class="listp">
		  	<!-- input field -->
		  	    <form:select path="fileType">
		  	      <option value="none"></option>
		  	      <form:option value="xls">Excel</form:option>
		  	      <form:option value="csv">CSV</form:option>
		  	    </form:select>   
		    <!-- end input field -->	
	 		</div>
		  	</div>
		  	
		  	<div class="list${classIndex % 2}"><c:set var="classIndex" value="${classIndex+1}" />
		  	<div class="listfl"><fmt:message key="active" />:</div>
		  	<div class="listp">
		  	<!-- input field -->
				<form:select path="active">
	        		<form:option value=""><fmt:message key="all"/></form:option> 
	        		<form:option value="0"><fmt:message key="inactive" /></form:option>
	  	   			<form:option value="1"><fmt:message key="active" /></form:option>
	      		</form:select>
	 		</div>
		  	</div>
	      
	      <c:choose>
	        <c:when test="${exportProductSearch.fileType == 'csv'}">
			  	<div class="list${classIndex % 2}"><c:set var="classIndex" value="${classIndex+1}" />
			  	<div class="listfl"><div class="helpImg"><img class="toolTipImg" title="Note::Required if you are changing the SKU's of existing items." src="../graphics/question.gif" /></div><fmt:message key="include" /> <fmt:message key="productId" />:</div>
			  	<div class="listp">
			  	<!-- input field -->
			  	    <form:checkbox path="withProductId" />
		 		</div>
			  	</div>
	        </c:when>
	        <c:otherwise>
		        <sec:authorize ifAnyGranted="ROLE_AEM">
			  	<div class="list${classIndex % 2}"><c:set var="classIndex" value="${classIndex+1}" />
			  	<div class="listfl"><div class="helpImg"><img class="toolTipImg" title="Note::Required if you are changing the SKU's of existing items." src="../graphics/question.gif" /></div><fmt:message key="include" /> <fmt:message key="productId" />:</div>
			  	<div class="listp">
			  	<!-- input field -->
			  	    <form:checkbox path="withProductId" />
		 		</div>
			  	</div>
			  	</sec:authorize>
	        </c:otherwise>
	      </c:choose>
	        

		  	<div class="list${classIndex % 2}"><c:set var="classIndex" value="${classIndex+1}" />
		  	<div class="listfl"><div class="helpImg"><img class="toolTipImg" title="Note::Uncheck if you are having trouble opening the excel file due to HTML Codes." src="../graphics/question.gif" /></div><fmt:message key="include" /> <fmt:message key="productLongDesc" />:</div>
		  	<div class="listp">
		  	<!-- input field -->
				<form:checkbox path="withLongDesc" />
	 		</div>
		  	</div>
		  	
		  	<div class="list${classIndex % 2}"><c:set var="classIndex" value="${classIndex+1}" />
		  	<div class="listfl"><div class="helpImg"><img class="toolTipImg" title="Note::Uncheck if you are having trouble opening the excel file due to Recommended List." src="../graphics/question.gif" /></div><fmt:message key="include" /> <fmt:message key="recommendedList" />:</div>
		  	<div class="listp">
		  	<!-- input field -->
				<form:checkbox path="withRecommendedList" />
	 		</div>
		  	</div>	  	

			<div class="list${classIndex % 2}"><c:set var="classIndex" value="${classIndex+1}" />
		  	<div class="listfl"><div class="helpImg"><img class="toolTipImg" title="Note::Uncheck if you are having trouble opening the excel file due to HTML Codes." src="../graphics/question.gif" /></div><fmt:message key="include" /> <fmt:message key="productName" />:</div>
		  	<div class="listp">
		  	<!-- input field -->
				<form:checkbox path="withProductName" />
	 		</div>
		  	</div>	  	

			<c:if test="${languageCodes != null}">
		  	<div class="list${classIndex % 2}"><c:set var="classIndex" value="${classIndex+1}" />
		  	<div class="listfl"><fmt:message key="withI18n" />:</div>
		  	<div class="listp">
		  	<!-- input field -->
				<form:checkbox path="withI18n" />
	 		</div>
		  	</div>		
		  	</c:if>

		  	<div class="list${classIndex % 2}"><c:set var="classIndex" value="${classIndex+1}" />
		  	<div class="listfl"><div class="helpImg"><img class="toolTipImg" title="Note::Useful for data feeds." src="../graphics/question.gif" /></div><fmt:message key="include" /> <fmt:message key="productandImageURL" />:</div>
		  	<div class="listp">
		  	<!-- input field -->
				<form:checkbox path="withProductAndImageUrl" />
	 		</div>
		  	</div>		

		  	<div class="list${classIndex % 2}"><c:set var="classIndex" value="${classIndex+1}" />
		  	<div class="listfl"><fmt:message key="productSku" />:</div>
		  	<div class="listp">
		  	<!-- input field -->
				<form:input path="sku" cssClass="textfield" maxlength="50" htmlEscape="true"/>
				(starts with)
	 		</div>
		  	</div>	
		  			  	
		  	<div class="list${classIndex % 2}"><c:set var="classIndex" value="${classIndex+1}" />
		  	<div class="listfl"><div class="helpImg"><img class="toolTipImg" title="Note::Includes sub-categories." src="../graphics/question.gif" /></div><fmt:message key="categoryId" />:</div>
		  	<div class="listp">
		  	<!-- input field -->
				<form:input path="category" cssClass="textfield50" maxlength="10" htmlEscape="true"/>
				<form:errors path="category" cssClass="error"/>
	 		</div>
		  	</div>	

			<%--
			<c:if test="${siteConfig['ETILIZE'].value != ''}">
		  	<div class="list${classIndex % 2}"><c:set var="classIndex" value="${classIndex+1}" />
		  	<div class="listfl"><div class="helpImg"><img class="toolTipImg" title="Note::Put Etilize Category ID's, seprated by commas." src="../graphics/question.gif" /></div>Etilize Category Filter:</div>
		  	<div class="listp">
		  	<!-- input field -->
				<input type="text" class="textfield" name="etilizeCategoryFilter" maxlength="255"
				value="<c:forEach items="${exportProductSearch.etilizeSearchCriteria.categoryFilter}" var="categoryFilter" varStatus="status"><c:if test="${not status.first}">, </c:if><c:out value="${categoryFilter.id}"/></c:forEach>"
				/>
	 		</div>
		  	</div>	
			</c:if>
			--%>

		  	<div class="list${classIndex % 2} <c:if test="${arguments != null}">highlight</c:if>"><c:set var="classIndex" value="${classIndex+1}" />
		  	<div class="listp">
		  	<!-- input field -->
			    <table class="form" width="100%">
				  <c:forEach items="${exportedFiles}" var="file">
				  <tr>
				    <td class="formName">&nbsp;</td>
				    <td><a href="../excel/${file['file'].name}"><c:out value="${file['file'].name}"/></a> 
				    	<c:if test="${file['lastModified'] != null}">(<fmt:formatDate type="both" timeStyle="full" value="${file['lastModified']}"/>)</c:if></td>
				  </tr>
				  </c:forEach>
				</table>
		    <!-- end input field -->   	
		  	</div>
		  	</div>
	  	
	<!-- end tab -->        
	</div> 
	
<!-- end tabs -->			
</div>

<!-- start button -->
<div align="left" class="button"> 
    <input type="submit" name="__new" value="<fmt:message key="fileGenerate" />">
</div>
<!-- end button -->	     
  
  <!-- end table -->
    </td><td class="boxmidr" ></td></tr>
  <tr><td class="botl"></td><td class="botr"></td></tr>
  </table>
  
<!-- end main box -->  
 </div>
</form:form>
</sec:authorize>

  </tiles:putAttribute>    
</tiles:insertDefinition>