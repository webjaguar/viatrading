<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="/WEB-INF/tlds/spring.tld" prefix="spring" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib uri="http://www.springframework.org/security/tags" prefix="sec" %>


<tiles:insertDefinition name="admin.catalog.product" flush="true">
  <tiles:putAttribute name="content" type="string"> 
  <sec:authorize ifAnyGranted="ROLE_AEM,ROLE_ADMIN_${prop_site_id},ROLE_SUPPLIER_UPDATE,ROLE_SUPPLIER_DELETE">
<script type="text/javascript" src="../../admin/javascript/autocompleter1.2.js"></script>
<script type="text/javascript" src="../../admin/javascript/autocompleter.Request1.2.js"></script>
<script type="text/javascript" src="../../admin/javascript/autocompleter.Local1.2.js"></script>
<script type="text/javascript" src="../../admin/javascript/observer.js"></script>
  
<c:if test="${gSiteConfig['gSUPPLIER'] == true}">
<script language="JavaScript" type="text/JavaScript">
<!--
window.addEvent('domready', function(){			
			var tabs1 = new SimpleTabs($('tab-block-1'), { selector: 'h4'});
			var Tips1 = new Tips($$('.toolTipImg'));			
			var supplier_company = $('supplier_company');	//Ajax Autocompleter 	
			var sDropDown = $('supplier.id');
			
			new Autocompleter.Request.HTML(supplier_company, '../supplier/show-ajax-suppliers.jhtm', {
				'indicatorClass': 'autocompleter-loading', 'onSelection': function(element, selected, value, input) {
					for ( var i = 0; i < sDropDown.options.length; i++ ) {
				        if ( sDropDown.options[i].text.toLowerCase() == value.toLowerCase() ) {
				        	sDropDown.options[i].selected = true;
				            return;
				        }
				    }
				} });
		});
//-->
</script>
<form:form commandName="productSupplierForm" method="post" >
<!-- main box -->
  <div id="mboxfull">
  
  <!-- start table -->
  <table cellpadding="0" cellspacing="0" border="0" class="module" >
  <tr>
    <td class="topl_g">
	  
	  <!-- breadcrumb -->
	  <p class="breadcrumb">
	    <a href="../catalog"><fmt:message key="catalog" /></a> &gt;
	    <a href="../catalog/productList.jhtm"><fmt:message key="products" /></a> &gt;
	    <a href="../catalog/productSupplierList.jhtm?sku=<c:out value="${param.sku}" />"><fmt:message key="supplier" /></a> &gt;
	    ${productSupplierForm.supplier.address.company}
	  </p>
	  
	  <!-- Error Message -->
	  <c:if test="${message != null}">
        <div class="message"><spring:message code="${message}"/></div>
      </c:if>
	  <spring:hasBindErrors name="supplierForm">
		<div class="message">Please fix all errors!</div>
	  </spring:hasBindErrors>

  
  </td><td class="topr_g" ></td></tr>
  <tr><td class="boxmidlrg" >

 <!-- tabs -->
 <div id="tab-block-1">
	<h4 title="<fmt:message key='supplier' />"><fmt:message key="supplier" /></h4>
	<div>
	<!-- start tab -->
			 <c:set var="classIndex" value="0" />
			
				 
			<div class="listdivi ln tabdivi"></div>
	  		<div class="listdivi"></div>
	  		<c:choose>
	  		<c:when test="${productSupplierForm.newSupplier}">  		
	 		<div class="list${classIndex % 2}"><c:set var="classIndex" value="${classIndex+1}" />	 		
	  		<div class="listfl"><fmt:message key='search'/> <fmt:message key='supplierCompanyName' />:</div>
	 		<div class="listp">				
				<input  id="supplier_company" name="" type="text" class="textfield" value="" />
			</div>
			</div>
			<div class="list${classIndex % 2}"><c:set var="classIndex" value="${classIndex+1}" />			
			<div class="listfl"><fmt:message key='supplierCompanyName' />:</div>			
			<div class="listp">
			<!-- input field -->
			 	<form:select path="supplier.id" >
			         <form:options items="${suppliers}" itemValue="id" itemLabel="address.company"/>
				</form:select>				 	 		
	   		<!-- end input field -->
		  	</div>
		  	</div>
			</c:when>
			</c:choose>
		  	<div class="list${classIndex % 2}"><c:set var="classIndex" value="${classIndex+1}" />
		  	<c:if test="${siteConfig['COST_TIERS'].value == 'true'}">
		  		<div class="listfl"><fmt:message key="cost" />:</div>
		  	</c:if>
		  	<c:if test="${! siteConfig['COST_TIERS'].value == 'true'}">
		  		<div class="listfl"><strong><fmt:message key="cost" />:</strong></div>
		  	</c:if>
		  	<div class="listp">
		  	<!-- input field -->
				<form:input path="supplier.price" cssClass="textfield" htmlEscape="true" />
				<c:if test="${gSiteConfig['gCUSTOMER_SUPPLIER']}">
				<form:select path="supplier.percent" cssClass="second">
					<form:option value="true" label="%" />
					<form:option value="false"><fmt:message key="FIXED" /> $</form:option>
				</form:select>
				</c:if>
       			<form:errors path="supplier.price" cssClass="error" />
		    <!-- end input field -->   	
		  	</div>
		  	</div>
		  	
		  	<c:if test="${not productSupplierForm.supplier.primary}">
			 <div class="list${classIndex % 2}"><c:set var="classIndex" value="${classIndex+1}" />
			 <div class="listfl"><fmt:message key="setAsPrimarySupplier" />:</div>
			 <div class="listp">
			 <!-- input field -->
			 	<form:checkbox path="setAsPrimary" value="true"/>
			 <!-- end input field -->   	
			 </div>
			 </div>	
			</c:if>

		  	<div class="list${classIndex % 2}"><c:set var="classIndex" value="${classIndex+1}" />
		  	<div class="listfl"><fmt:message key="supplier" /> <fmt:message key="sku" />:</div>
		  	<div class="listp">
		  	<!-- input field -->
				<form:input path="supplier.supplierSku" cssClass="textfield" htmlEscape="true" />
       			<form:errors path="supplier.supplierSku" cssClass="error" />
		    <!-- end input field -->   	
		  	</div>
		  	</div>
	  	
	<!-- end tab -->        
	</div> 
	
 
<!-- end tabs -->			
</div>

<!-- start button -->
<div align="left" class="button"> 
	<c:choose>
		<c:when test="${productSupplierForm.newSupplier}">
			<input type="submit" value="<fmt:message key='add' />" /> 
		</c:when>
		<c:otherwise>
			<sec:authorize ifAnyGranted="ROLE_AEM,ROLE_ADMIN_${prop_site_id},ROLE_SUPPLIER_UPDATE">
				<input type="submit" value="<fmt:message key='Update' />" /> 
			</sec:authorize>
			<sec:authorize ifAnyGranted="ROLE_AEM,ROLE_ADMIN_${prop_site_id},ROLE_SUPPLIER_DELETE">
				<c:if test="${productSupplierForm.canDelete}">
				<input type="submit" name="delete" value="<fmt:message key='delete' />" onClick="return confirm('Delete permanently?')"/>  
				</c:if>
			</sec:authorize>
		</c:otherwise>
	</c:choose>
	<input type="submit" value="<spring:message code="cancel"/>" name="_cancel">
</div>
<!-- end button -->	     
  
  <!-- end table -->
    </td><td class="boxmidr" ></td></tr>
  <tr><td class="botl"></td><td class="botr"></td></tr>
  </table>
  
<!-- end main box -->  
</div>
</form:form>
</c:if>
  </sec:authorize>
  </tiles:putAttribute>    
</tiles:insertDefinition>