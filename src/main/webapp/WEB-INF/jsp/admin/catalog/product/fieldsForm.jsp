<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="/WEB-INF/tlds/spring.tld" prefix="spring" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ taglib uri="http://www.springframework.org/security/tags" prefix="sec" %>

<tiles:insertDefinition name="admin.catalog.product" flush="true">
  <tiles:putAttribute name="content" type="string">
<script type="text/JavaScript">
<!--
window.addEvent('domready', function(){			
	var tabs1 = new SimpleTabs($('tab-block-1'), { selector: 'h4'});
	var Tips1 = new Tips($$('.toolTipImg'));
});
//-->
</script>
<form method="post" name="productFields">
<!-- main box -->
  <div id="mboxfull">
  
  <!-- start table -->
  <table cellpadding="0" cellspacing="0" border="0" class="module" >
  <tr>
    <td class="topl_g">
	  
	  <!-- breadcrumb -->
	  <p class="breadcrumb">
	     <a href="../catalog">Catalog</a> &gt;
	     <a href="../catalog/productList.jhtm">Product</a> &gt;
	    Fields 
	  </p>
	  
	  <!-- Error -->
	  <c:if test="${!empty message}">
		 <div class="message"><spring:message code="${message}" /></div>
	  </c:if>
	  
  </td><td class="topr_g" ></td></tr>
  <tr><td class="boxmidlrg" >

 <!-- tabs -->
 <div id="tab-block-1">
	<h4 title="<fmt:message key="productFields"/> Form"><fmt:message key="productFields"/></h4>
	<div>
	<!-- start tab -->

         <c:set var="classIndex" value="0" />
         <c:set var="cols" value="0"/>
         
	  	<div class="listdivi ln tabdivi"></div>
	  	<div class="listdivi"></div>
	   
		  	<div class="list${classIndex % 2}"><c:set var="classIndex" value="${classIndex+1}" />
		  	<div class="listpp" style="overflow-x: scroll;">
		  	<!-- input field -->
			<table class="form" cellspacing="2" cellpadding="0">
			  <tr>
			    <td>&nbsp;</td>
			    <td align="center" class="productFieldTitle"><fmt:message key="Enabled" /></td>
			    <td align="center" class="productFieldTitle"><fmt:message key="details" /></td>
			    <td align="center" class="productFieldTitle"><fmt:message key="quickMode" /></td>
			    <c:if test="${siteConfig['PRODUCT_QUICK_MODE_2'].value == 'true'}"	>
			    	<td align="center" class="productFieldTitle"><fmt:message key="quickMode2" /></td>
			    </c:if>
			    <td align="center" class="productFieldTitle"><fmt:message key="showOnInvoice" /></td>
			    <td align="center" class="productFieldTitle"><fmt:message key="showOnInvoice" /> (<fmt:message key="backend" />)</td>
			    <c:if test="${siteConfig['PRODUCT_FIELDS_ON_INVOICE_EXPORT'].value == 'true'}"	>
			    <td align="center" class="productFieldTitle"><fmt:message key="showOnInvoice" /> (<fmt:message key="export" />)</td>
			    </c:if>
			    <c:if test="${gSiteConfig['gPRESENTATION']}">
			    <td align="center" class="productFieldTitle"><fmt:message key="showOnPresentation" /></td>
			    </c:if>
			    <c:if test="${siteConfig['ADD_PRODUCT_ON_FRONTEND'].value == 'true' and gSiteConfig['gCUSTOMER_SUPPLIER']}">
			    <td align="center" class="productFieldTitle"><fmt:message key="showOnProductFormFrontend" /></td>
			    </c:if>
			    <td align="center" class="productFieldTitle"><fmt:message key="showOnDropDown" /></td>
			    <td align="center" class="productFieldTitle"><fmt:message key="showOnPacking" /></td>
			    <c:if test="${gSiteConfig['gMYLIST']}">
			    <td align="center" class="productFieldTitle"><fmt:message key="showOnMyList" /></td>
			    </c:if>
			    <c:if test="${siteConfig['PRODUCT_FIELDS_SEARCH_TYPE'].value != '0'}"	>
				    <c:set var="cols" value="${cols+1}"/>
				    <td align="center" class="productFieldTitle"><fmt:message key="search" /></td>
				    <c:if test="${siteConfig['PRODUCT_QUICK_MODE_2'].value == 'true'}"	>
					    <c:set var="cols" value="${cols+1}"/>
					    <td align="center" class="productFieldTitle"><fmt:message key="search" /> 2</td>
				    </c:if>
			    </c:if>
			    <c:if test="${gSiteConfig['gCOMPARISON']}">
			    <c:set var="cols" value="${cols+1}"/>
			    <td align="center" class="productFieldTitle"><fmt:message key="compare" /></td>
			    </c:if>
			    <c:if test="${gSiteConfig['gSERVICE']}">
			    <c:set var="cols" value="${cols+1}"/>
			    <td align="center" class="productFieldTitle"><fmt:message key="service" /></td>
			    </c:if>
			    <td align="center" class="productFieldTitle"><fmt:message key="productExport" /></td>
			    
			    <c:if test="${gSiteConfig['gPROTECTED'] > 0}">
			    <c:set var="cols" value="${cols+1}"/>
			    <td align="center" class="productFieldTitle"><fmt:message key="protectedLevel" /></td>
			    </c:if>
			    <td align="left" class="productFieldTitle"><fmt:message key="rank" /></td>
			    <td align="center" class="productFieldTitle"><fmt:message key="type" /></td>					   
			    <td align="center" class="productFieldTitle"><fmt:message key="indexForSearch" /></td>
			    <td align="center" class="productFieldTitle"><fmt:message key="indexForFilter" /></td>
			    <td align="center" class="productFieldTitle"><fmt:message key="indexForPredictiveSearch" /></td>
			    <td align="center" class="productFieldTitle"><fmt:message key="rangeFilter" /></td>
			    <td align="center" class="productFieldTitle"><fmt:message key="checkboxFilter" /></td>
			    <td class="productFieldTitle" style="padding-left:20px"><fmt:message key="Name" /></td>
			    <td class="productFieldTitle" style="padding-left:20px"><fmt:message key="groupName" /></td>
			    <td class="productFieldTitle" style="padding-left:20px"><fmt:message key="PreValue" /><span style="float:left"><img class="toolTipImg" title="Note::Seperate values with comma. Don't use space before/after comma." src="../graphics/question.gif" /></span></td>
				<td class="productFieldTitle" style="padding-left:20px"><fmt:message key="FormatType" /></td>
			    
			  
			  </tr>
			  <c:forEach items="${productFieldsForm.productFields}" var="productField" varStatus="status">
			  <tr>
			    <td style="font-size:10px;color:#333333;width:100px;text-align:right"><fmt:message key="Field" />&nbsp;<c:out value="${status.index + 1}"/> : </td>			    
			    <td align="center"><input name="__enabled_${productField.id}" type="checkbox" <c:if test="${productField.enabled}">checked</c:if>></td>
			    <td align="center"><input name="__detailsField_${productField.id}" type="checkbox"  <c:if test="${productField.detailsField}">checked</c:if>></td>
			    <td align="center"><input name="__quickModeField_${productField.id}" type="checkbox"  <c:if test="${productField.quickModeField}">checked</c:if>></td>
			    <c:if test="${siteConfig['PRODUCT_QUICK_MODE_2'].value == 'true'}"	>
			    	<td align="center"><input name="__quickMode2Field_${productField.id}" type="checkbox"  <c:if test="${productField.quickMode2Field}">checked</c:if>></td>
			    </c:if>
			    <td align="center"><input name="__showOnInvoice_${productField.id}" type="checkbox"  <c:if test="${productField.showOnInvoice}">checked</c:if>></td>
			    <td align="center"><input name="__showOnInvoice_backend_${productField.id}" type="checkbox"  <c:if test="${productField.showOnInvoiceBackend}">checked</c:if>></td>
			    <c:if test="${siteConfig['PRODUCT_FIELDS_ON_INVOICE_EXPORT'].value == 'true'}"	>
			    <td align="center"><input name="__showOnInvoice_export_${productField.id}" type="checkbox"  <c:if test="${productField.showOnInvoiceExport}">checked</c:if>></td>
			    </c:if>
			    <c:if test="${gSiteConfig['gPRESENTATION']}">
			    <td align="center"><input name="__showOnPresentation_${productField.id}" type="checkbox"  <c:if test="${productField.showOnPresentation}">checked</c:if>></td>
			    </c:if>
			    <c:if test="${siteConfig['ADD_PRODUCT_ON_FRONTEND'].value == 'true' and gSiteConfig['gCUSTOMER_SUPPLIER']}">
			    <td align="center"><input name="__showOnProductForm_frontend_${productField.id}" type="checkbox"  <c:if test="${productField.showOnProductFormFrontend}">checked</c:if>></td>
			    </c:if>
			    <td align="center"><input name="__showOnDropDown_${productField.id}" type="checkbox"  <c:if test="${productField.showOnDropDown}">checked</c:if>></td>
			    <td align="center"><input name="__packingField_${productField.id}" type="checkbox"  <c:if test="${productField.packingField}">checked</c:if>></td>
			    <c:if test="${gSiteConfig['gMYLIST']}">
			    <td align="center"><input name="__showOnMyList_${productField.id}" type="checkbox"  <c:if test="${productField.showOnMyList}">checked</c:if>></td>
			    </c:if>
			    <c:if test="${siteConfig['PRODUCT_FIELDS_SEARCH_TYPE'].value != '0'}"	>
			    <td align="center"><input name="__search_${productField.id}" type="checkbox" <c:if test="${productField.search}">checked</c:if>></td>
			    <c:if test="${siteConfig['PRODUCT_QUICK_MODE_2'].value == 'true'}"	>
			    	<td align="center"><input name="__search2_${productField.id}" type="checkbox" <c:if test="${productField.search2}">checked</c:if>></td>
			    </c:if>
			    </c:if>
			    <c:if test="${gSiteConfig['gCOMPARISON']}">
			    <td align="center"><input name="__comparisonField_${productField.id}" type="checkbox" <c:if test="${productField.comparisonField}">checked</c:if>></td>
			    </c:if>    
			    <c:if test="${gSiteConfig['gSERVICE']}">
			    <td align="center"><input name="__serviceField_${productField.id}" type="checkbox" <c:if test="${productField.serviceField}">checked</c:if>></td>
			    </c:if>
			    <td align="center"><input name="__productExport_${productField.id}" type="checkbox"  <c:if test="${productField.productExport}">checked</c:if>></td>
			    
			    <c:if test="${gSiteConfig['gPROTECTED'] > 0}">
			    <td align="center">
					<select  class="textfield50" name="__protected_${productField.id}">
			  	      <option value="0"></option>
					  <c:forEach begin="1" end="${gSiteConfig['gPROTECTED']}" var="prot">
				  	      <c:set var="key" value="protected${prot}"/>
				    	<c:choose>
				      	  <c:when test="${labels[key] != null and labels[key] != ''}">
				        	<c:set var="label" value="${labels[key]}"/>
				      	  </c:when>
				      	  <c:otherwise><c:set var="label" value="${prot}"/></c:otherwise>
				    	</c:choose>		        
			        	<option value="${protectedLevels[prot-1]}" <c:if test="${protectedLevels[prot-1] == productField.protectedLevel}">selected</c:if>><c:out value="${label}"/></option>
					  </c:forEach>
					</select>			
			    </td>
			    </c:if>
			    <td>
					<input type="text" name="__rank_${productField.id}" value="<c:out value="${productField.rank}"/>" class="textfield20" maxlength="5" size="10">
			    </td>

			    <td align="center">
				    <select class="textfield50" name="__fieldType_${productField.id}">
			  	      <option value="string" <c:if test="${'string' == productField.fieldType}">selected</c:if>><fmt:message key="text" /></option>
			  	      <option value="number" <c:if test="${'number' == productField.fieldType}">selected</c:if>><fmt:message key="number" /></option>
					</select>
			    </td>	
			    <td align="center"><input name="__indexForSearch_${productField.id}" type="checkbox"  <c:if test="${productField.indexForSearch}">checked</c:if>></td>
			    <td align="center"><input name="__indexForFilter_${productField.id}" type="checkbox"  <c:if test="${productField.indexForFilter}">checked</c:if>></td>
			    <td align="center"><input name="__indexForPredictiveSearch_${productField.id}" type="checkbox"  <c:if test="${productField.indexForPredictiveSearch}">checked</c:if>></td>			    
			    <td align="center"><input name="__rangeFilter_${productField.id}" type="checkbox"  <c:if test="${productField.rangeFilter}">checked</c:if>></td>
			    <td align="center"><input name="__checkboxFilter_${productField.id}" type="checkbox"  <c:if test="${productField.checkboxFilter}">checked</c:if>></td>
			    <td>
					<input type="text" name="__name_${productField.id}" value="<c:out value="${productField.name}"/>" class="textfield250" maxlength="255" size="50">
			    </td>
			    <td>
					<input type="text" name="__group_name_${productField.id}" value="<c:out value="${productField.groupName}"/>" class="textfield250" maxlength="255" size="50">
			    </td>
			    <td>
			        <input type="text" name="__preValue_${productField.id}" value="<c:out value="${productField.preValue}"/>" class="textfield250" maxlength="512" size="50">
			    </td>
			    <td>
					<input type="text" name="__formatType_${productField.id}" value="<c:out value="${productField.formatType}"/>" class="textfield100" maxlength="100" size="10">
			    </td>
			  </tr>
			  <%-- for now it is TURNED OFF --%>
			  <c:if test="${false and gSiteConfig['gCUSTOMER_SUPPLIER']}">
			  <tr>
			    <td align="right" colspan="${11+cols}" class="productFieldTitle">
			        <fmt:message key="categoryIds" />:
				</td>
			    <td colspan="4">
			    	<input type="text" name="__catIds_${productField.id}" value="<c:out value="${productField.categoryIds}"/>" class="textfield500" size="50">
				</td>
			  </tr>
			  <tr>
			    <td colspan="${11+cols}" align="right">&nbsp;</td>
			  </tr>
			  </c:if>
			  </c:forEach>
			</table>
	 		<!-- end input field -->	
	 		</div>
		  	</div>	
	  	
	<!-- end tab -->        
	</div>         

    <c:forEach items="${languageCodes}" var="i18n">
	<h4 title="<fmt:message key="language_${i18n.languageCode}"/>"><img src="../graphics/${i18n.languageCode}.gif" border="0" title="<fmt:message key="language_${i18n.languageCode}"/>"> <fmt:message key="language_${i18n.languageCode}"/></h4>
	<div>
	<!-- start tab -->
	
         <c:set var="classIndex" value="0" />
         <c:set var="cols" value="0"/>	
         
	  	<div class="listdivi ln tabdivi"></div>
	  	<div class="listdivi"></div>
	   
		  	<div class="list${classIndex % 2}"><c:set var="classIndex" value="${classIndex+1}" />
		  	<div class="listpp">
		  	<!-- input field -->
			<table class="form" cellspacing="2" cellpadding="0">
			  <tr>
			    <td>&nbsp;</td>
			    <td class="productFieldTitle" style="padding-left:20px"><fmt:message key="Name" /></td>
			    <td class="productFieldTitle" style="padding-left:20px"><fmt:message key="PreValue" /><span style="float:left"><img class="toolTipImg" title="Note::Seperate values with comma. Don't use space before/after comma." src="../graphics/question.gif" /></span></td>
			  </tr>
			  <c:forEach items="${productFieldsForm.productFields}" var="productField" varStatus="status">
			  <tr>
			    <td style="font-size:10px;color:#333333;width:100px;text-align:right"><fmt:message key="Field" />&nbsp;<c:out value="${status.index + 1}"/> : </td>
			    <td>
					<input type="text" name="__i18n_name_${productField.id}_${i18n.languageCode}" value="<c:out value="${i18nProductFields[i18n.languageCode][productField.id].name}"/>" class="textfield250" maxlength="255" size="50">
			    </td>
			    <td>
			        <input type="text" name="__i18n_preValue_${productField.id}_${i18n.languageCode}" value="<c:out value="${i18nProductFields[i18n.languageCode][productField.id].preValue}"/>" class="textfield250" maxlength="512" size="50">
			    </td>
			  </tr>
			  </c:forEach>
			</table>
	 		<!-- end input field -->	
	 		</div>
		  	</div>	
		  	
	<!-- end tab -->		
	</div>	
	</c:forEach>


<!-- end tabs -->			
</div>

<!-- start button -->
  <div align="left" class="button"> 
    <sec:authorize ifAnyGranted="ROLE_AEM,ROLE_ADMIN_${prop_site_id},ROLE_PRODUCT_FIELD_EDIT">
      <input type="submit" value="<spring:message code="productFieldsUpdate"/>">
    </sec:authorize>
  </div>
<!-- end button -->	
  
  <!-- end table -->
    </td><td class="boxmidr" ></td></tr>
  <tr><td class="botl"></td><td class="botr"></td></tr>
  </table>
  
<!-- end main box -->  
</div>
</form>

  </tiles:putAttribute>    
</tiles:insertDefinition>