<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="/WEB-INF/tlds/spring.tld" prefix="spring" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ taglib uri="http://www.springframework.org/security/tags" prefix="sec" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ page contentType="text/html;charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib uri="/WEB-INF/tlds/custom.tld" prefix="wj" %>
<div>
	<!-- start tab -->

        <c:set var="classIndex" value="0" />
        
	  	<div class="listdivi ln tabdivi"></div>
	  	<div class="listdivi"></div>
	   
		  	<div class="list${classIndex % 2}"><c:set var="classIndex" value="${classIndex+1}" />
		  	<div class="listfl"><div class="helpImg"><img class="toolTipImg" title="Active::Unchecking this, will not allow this product to be displayed on front end." src="../graphics/question.gif" /></div><fmt:message key="active" />:</div>
		  	<div class="listp">
		  	<!-- input field -->
	   			<form:checkbox path="product.active" />
		    <!-- end input field -->   	
		  	</div>
		  	</div>
		  	
		  	<div class="list${classIndex % 2}"><c:set var="classIndex" value="${classIndex+1}" />
		  	<div class="listfl"><div class="helpImg"><img class="toolTipImg" title="Active::Unchecking this, will not allow this product to be displayed on front end." src="../graphics/question.gif" /></div><fmt:message key="softLink" />:</div>
		  	<div class="listp">
		  	<!-- input field -->
	   			<form:checkbox path="product.softLink" />
		    <!-- end input field -->   	
		  	</div>
		  	</div>
		  	
		  	<div class="list${classIndex % 2}"><c:set var="classIndex" value="${classIndex+1}" />
		  	<div class="listfl"><div class="requiredField"><div class="helpImg"><img class="toolTipImg" title="Product Name::The Maximum character is 120.For example: Test_Example." src="../graphics/question.gif" /></div><fmt:message key="productName" />:</div></div>
		  	<div class="listp">
		  	<!-- input field -->
	   			<form:input path="product.name" cssClass="textfield" size="50" maxlength="120" htmlEscape="true"/>
	   			<form:errors path="product.name" cssClass="error" />
	   			<div class="helpNote">
			      <c:if test="${gSiteConfig['gMOD_REWRITE'] == '1'}">
			      <p> The name will be on URL. The special characters are not recommended( "/","%","@","&").</p>
			      </c:if>
			    </div>
		    <!-- end input field -->   	
		  	</div>
		  	</div>
		  	
		  	<div class="list${classIndex % 2}"><c:set var="classIndex" value="${classIndex+1}" />
		  	<div class="listfl"><fmt:message key="fobZipCode" />:</div>
		  	<div class="listp">
		  	<!-- input field -->
	   			<form:input path="product.fobZipCode" cssClass="textfield" size="70" maxlength="512" htmlEscape="true"/>
	  			<form:errors path="product.fobZipCode" cssClass="error" />
	  			
		    <!-- end input field -->  	  
		  	</div>
		  	</div>
		  			   
		  	<div class="list${classIndex % 2}"><c:set var="classIndex" value="${classIndex+1}" />
		  	<div class="listfl"><div class="helpImg"><img class="toolTipImg" title="Stock Keeping Unit::Required. Alphanumeric number. The maximum character is 50." src="../graphics/question.gif" /></div><div class="requiredField"><fmt:message key="productSku" />:</div></div>	    
		    <div class="listp">
		    <c:choose>
		    <c:when test="${productForm.newProduct}">
		  	<!-- input field -->
		  	<c:if test="${supplierPrefix != null}">
		  		<label><c:out value="${supplierPrefix}"/></label>
		  	</c:if>		  	
	   			<form:input path="product.sku" cssClass="textfield" maxlength="50" htmlEscape="true"/>
	   			<form:errors path="product.sku" cssClass="error" />
			    <div class="helpNote">			   
			      <p>Changing SKU will change behaviour of some modules. It is recommended not to change SKU.</p>
			       <p>  The special characters are not recommended( "/","%","@","&"," ").</p>
			    </div>
		    <!-- end input field -->  	  		  	
		  	</c:when>
		  	<c:otherwise>
		  	<c:if test="${supplierPrefix != null}">
		  		<label><c:out value="${supplierPrefix}"/></label>
		  	</c:if>
		  		<c:out value="${productForm.product.sku}"/>		
		  	</c:otherwise>
		  	</c:choose>
		  	</div>
		  	</div>
		  	
		  	<!-- Program field -->		  	
		  	<div class="list${classIndex % 2}"><c:set var="classIndex" value="${classIndex+1}" />
		  	<div class="listfl"><fmt:message key="program" />:</div>
		  	<div class="listp">
	   			<form:input path="product.program" cssClass="textfield" size="70" maxlength="512" htmlEscape="true"/>
	  			<form:errors path="product.program" cssClass="error" />
		  	</div>
		  	</div>
		  	<!-- Program field -->	
		  	
		  	<div class="list${classIndex % 2}"><c:set var="classIndex" value="${classIndex+1}" />
		  	<div class="listfl"><fmt:message key="p_class" />:</div>
		  	<div class="listp">
		  	<!-- input field -->
	   			<form:input path="product.classField" cssClass="textfield" size="70" maxlength="512" htmlEscape="true"/>
	  			<form:errors path="product.classField" cssClass="error" />
	  			
		    <!-- end input field -->  	  
		  	</div>
		  	</div>
		  	
		  		<div class="list${classIndex % 2}"><c:set var="classIndex" value="${classIndex+1}" />
		  	<div class="listfl"><fmt:message key="upc" />:</div>
		  	<div class="listp">
		  	<!-- input field -->
	   			<form:input path="product.upc" cssClass="textfield" size="70" maxlength="512" htmlEscape="true"/>
	  			<form:errors path="product.upc" cssClass="error" />
	  			
		    <!-- end input field -->  	  
		  	</div>
		  	</div>

		    <c:if test="${gSiteConfig['gMASTER_SKU']}">
		  	<div class="list${classIndex % 2}"><c:set var="classIndex" value="${classIndex+1}" />
		  	<div class="listfl"><div class="helpImg"><img class="toolTipImg" title="Parent Sku::Required. Alphanumeric number. The maximum character is 50." src="../graphics/question.gif" /></div><fmt:message key="masterSku" />:</div>
		  	<div class="listp">
		  	<!-- input field -->
	   			<form:input path="product.masterSku" cssClass="textfield" htmlEscape="true"/>
	   			<form:errors path="product.masterSku" cssClass="error" />
		  	<!-- end input field -->	
		  	</div>
		  	</div>	
		  	</c:if>

			<c:if test="${siteConfig['ETILIZE'].value != ''}">
		  	<div class="list${classIndex % 2}"><c:set var="classIndex" value="${classIndex+1}" />
		  	<div class="listfl"><fmt:message key="upc" />:</div>
		  	<div class="listp">
		  	<!-- input field -->
	   			<form:input path="product.upc" cssClass="textfield" maxlength="50" htmlEscape="true"/>
	   			<form:errors path="product.upc" cssClass="error" />
		    <!-- end input field -->  	  
		  	</div>
		  	</div>
		  	
		  	<div class="list${classIndex % 2}"><c:set var="classIndex" value="${classIndex+1}" />
		  	<div class="listfl">Etilize ID:</div>
		  	<div class="listp">
		  	<!-- input field -->
	   			<form:input path="product.etilizeId" cssClass="textfield" maxlength="50" htmlEscape="true"/>
	   			<form:errors path="product.etilizeId" cssClass="error" />
		    <!-- end input field -->  	  
		  	</div>
		  	</div>		  	
		  	</c:if>
		  
		    <c:if test="${gSiteConfig['gALSO_CONSIDER']}">
		  	<div class="list${classIndex % 2}"><c:set var="classIndex" value="${classIndex+1}" />
		  	<div class="listfl"><div class="helpImg"><img class="toolTipImg" title="Also Consider::Mention only one SKU that will appear on frontend with this product." src="../graphics/question.gif" /></div><fmt:message key="alsoConsider" /><fmt:message key="productSku" />:</div>
		  	<div class="listp">
		  	<!-- input field -->
	   			<form:input path="product.alsoConsider" cssClass="textfield" htmlEscape="true"/>
	   			<form:errors path="product.alsoConsider" cssClass="error" />
		  	<!-- end input field -->	
		  	</div>
		  	</div>	
		  	</c:if>
		  	<jsp:include page="/WEB-INF/jsp/admin/catalog/product/redirectUrl.jsp" /> 
		  	
		 			  	
		  	<div class="list${classIndex % 2}"><c:set var="classIndex" value="${classIndex+1}" />
		  	<div class="listfl"><div class="helpImg"><img class="toolTipImg" title="::The Maximum character is 512." src="../graphics/question.gif" /></div><fmt:message key="productShortDesc" />:</div>
		  	<div class="listp">
		  	<!-- input field -->
	   			<form:textarea path="product.shortDesc" rows="8" cols="60" cssClass="textfield" htmlEscape="true"/>
	  			<form:errors path="product.shortDesc" cssClass="error" />
	  			
		    <!-- end input field -->  	  
		  	</div>
		  	</div>
		  	
		  	<div class="list${classIndex % 2}"><c:set var="classIndex" value="${classIndex+1}" />
		  	<div class="listfl"><fmt:message key="productLongDesc" />:</div>
		  	<div class="listp">
		  	<!-- input field -->
	   			<form:textarea path="product.longDesc" rows="15" cols="60" cssClass="textfield" htmlEscape="true"/>
	   			<c:if test="${siteConfig['DETAILS_LONG_DESC_HTML'].value != 'false'}">
			    <div style="text-align:left" id="product.longDesc_link"><a href="#" onClick="loadEditor('product.longDesc')" class="htmleditor">- <fmt:message key="htmlEditor" /> -</a></div>
			    </c:if>
			    <form:errors path="product.longDesc" cssClass="error" />
		  	<!-- end input field -->	
		  	</div>
		  	</div>
		  	
		  	<div class="list${classIndex % 2}"><c:set var="classIndex" value="${classIndex+1}" />
		  	<div class="listfl"><div class="helpImg"><img class="toolTipImg" title="Weight::It accept 8 integers and 2 decimals, For emaple,12345678.99." src="../graphics/question.gif" /></div><fmt:message key="productWeight" />:</div>
		  	<div class="listp">
		  	<!-- input field -->
		  	    <form:input path="product.weight" cssClass="textfield" size="8" htmlEscape="true"/>(lbs.)
	 			<form:errors path="product.weight" cssClass="error" />  
	 			
		  	<!-- end input field -->	
		  	</div>
		  	</div>

		  	<div class="list${classIndex % 2}"><c:set var="classIndex" value="${classIndex+1}" />
		  	<div class="listfl"><div class="helpImg"><img class="toolTipImg" title="Package Length::Used to calculate dimensional weight for UPS.Package Length will be allowed 8 integers and 2 decimals, For example,12345678.99." src="../graphics/question.gif" /></div><fmt:message key="package" /> <fmt:message key="length" />:</div>
		  	<div class="listp">
		  	<!-- input field -->
		  	    <form:input path="product.packageL" cssClass="textfield" size="8" htmlEscape="true"/>(inch)
	 			<form:errors path="product.packageL" cssClass="error" /> 
	 			
		  	<!-- end input field -->	
		  	</div>
		  	</div>
		  	
		  	<div class="list${classIndex % 2}"><c:set var="classIndex" value="${classIndex+1}" />
		  	<div class="listfl"><div class="helpImg"><img class="toolTipImg" title="Package Width::Used to calculate dimensional weight for UPS.Package Width will be allowed 8 integers and 2 decimals, For emaple,12345678.99." src="../graphics/question.gif" /></div><fmt:message key="package" /> <fmt:message key="width" />:</div>
		  	<div class="listp">
		  	<!-- input field -->
		  	    <form:input path="product.packageW" cssClass="textfield" size="8" htmlEscape="true"/>(inch)
	 			<form:errors path="product.packageW" cssClass="error" />  
	 		 
		  	<!-- end input field -->	
		  	</div>
		  	</div>		  	
		  	
		  	<div class="list${classIndex % 2}"><c:set var="classIndex" value="${classIndex+1}" />
		  	<div class="listfl"><div class="helpImg"><img class="toolTipImg" title="Package Height::Used to calculate dimensional weight for UPS.Package Height will be allowed 8 integers and 2 decimals, For example,12345678.99." src="../graphics/question.gif" /></div><fmt:message key="package" /> <fmt:message key="height" />:</div>
		  	<div class="listp">
		  	<!-- input field -->
		  	    <form:input path="product.packageH" cssClass="textfield" size="8" htmlEscape="true"/>(inch)
	 			<form:errors path="product.packageH" cssClass="error" />  
	 			
		  	<!-- end input field -->	
		  	</div>
		  	</div>		  	
		  	
		  	<div class="list${classIndex % 2}"><c:set var="classIndex" value="${classIndex+1}" />
		  	<div class="listfl"><div class="helpImg"><img class="toolTipImg" title="Max Items in Package::Used to calculate number of packages for UPS.Items will be allowed 6 integers and 2 decimals, For example,1234.99" src="../graphics/question.gif" /></div><fmt:message key="upsMaxItemsInPackage" />:</div>
		  	<div class="listp">
		  	<!-- input field -->
		  	    <form:input path="product.upsMaxItemsInPackage" cssClass="textfield" size="8" htmlEscape="true"/>
	 			<form:errors path="product.upsMaxItemsInPackage" cssClass="error" />  
	 		 
		  	<!-- end input field -->	
		  	</div>
		  	</div>		  	
		  	
		  	<div class="list${classIndex % 2}"><c:set var="classIndex" value="${classIndex+1}" />
		  	<div class="listfl"><div class="helpImg"><img class="toolTipImg" title="Max Items in Package::Used to calculate number of packages for USPS.Items will be allowed 6 integers and 2 decimals, For emaple,1234.99." src="../graphics/question.gif" /></div><fmt:message key="uspsMaxItemsInPackage" />:</div>
		  	<div class="listp">
		  	<!-- input field -->
		  	    <form:input path="product.uspsMaxItemsInPackage" cssClass="textfield" size="8" htmlEscape="true"/>
	 			<form:errors path="product.uspsMaxItemsInPackage" cssClass="error" />  
	 			
		  	<!-- end input field -->	
		  	</div>
		  	</div>		  	
		  	
		  	<c:if test="${gSiteConfig['gSALES_PROMOTIONS']}">
		  	<div class="list${classIndex % 2}"><c:set var="classIndex" value="${classIndex+1}" />
		  	<div class="listfl"><fmt:message key="salesTag" />:</div>
		  	<div class="listp">
		  	<!-- input field -->
				<form:select path="product.salesTagId" cssClass="textfield"  >
				  <form:option value="${null}"> <fmt:message key='NoSalesTag'/></form:option>
				  <c:forEach items="${salesTagCodes}" var="option">
				   <form:option value="${option.tagId}" ><c:out value="${option.title}"/></form:option>
				  </c:forEach>	    
				</form:select>		
		  	<!-- end input field -->	
		  	</div>
		  	</div>	
		  	</c:if>
		  	
		  	<c:if test="${gSiteConfig['gMAIL_IN_REBATES'] > 0}">
		  	<div class="list${classIndex % 2}"><c:set var="classIndex" value="${classIndex+1}" />
		  	<div class="listfl"><fmt:message key="mailInRebates" />:</div>
		  	<div class="listp">
		  	<!-- input field -->
				<form:select path="product.rebateId" cssClass="textfield"  >
				  <form:option value="${null}"> <fmt:message key='NoRebate'/></form:option>
				  <c:forEach items="${mailInRebates}" var="option">
				   <form:option value="${option.rebateId}" ><c:out value="${option.title}"/></form:option>
				  </c:forEach>	    
				</form:select>		
		  	<!-- end input field -->	
		  	</div>
		  	</div>	
		  	</c:if>
		  	
		  	<c:if test="${gSiteConfig['gMANUFACTURER']}">
		  	<div class="list${classIndex % 2}"><c:set var="classIndex" value="${classIndex+1}" />
		  	<div class="listfl"><fmt:message key="manufacturer" />:</div>
		  	<div class="listp">
		  	<!-- input field -->
	   			<form:select path="product.manufactureName" cssClass="textfield"  >
				  <form:option value="${null}"></form:option>
				  <c:forEach items="${manufacturers}" var="manufacturer">
				   <form:option value="${manufacturer.name}" ><c:out value="${manufacturer.name}"/></form:option>
				  </c:forEach>	    
				</form:select>	
		  	<!-- end input field -->	
		  	</div>
		  	</div>	
		  	</c:if>
		  	
		  	<c:if test="${gSiteConfig['gMINIMUM_INCREMENTAL_QTY']}">
		  	<div class="list${classIndex % 2}"><c:set var="classIndex" value="${classIndex+1}" />
		  	<div class="listfl"><fmt:message key="minimumToOrder" /> / <fmt:message key="incremental" />:</div>
		  	<div class="listp">
		  	<!-- input field -->
	 			<form:input path="product.minimumQty"  cssClass="textfield50" maxlength="8" htmlEscape="true" /> / <form:input path="product.incrementalQty"  cssClass="textfield50" maxlength="8" htmlEscape="true"/>
	 			<form:errors path="product.minimumQty" cssClass="error"/>
	 			<form:errors path="product.incrementalQty" cssClass="error"/>
		  	<!-- end input field -->	
		  	</div>
		  	</div>	
		  	</c:if>
		  	
		  	<div class="list${classIndex % 2}"><c:set var="classIndex" value="${classIndex+1}" />
		  	<div class="listfl"><div class="helpImg"><img class="toolTipImg" title="List::check it to add to list." src="../graphics/question.gif" /></div><fmt:message key="includeRetailDisplay" />:</div>
		  	<div class="listp">
		  	<!-- input field -->
	 			<form:checkbox path="product.includeRetailEnabled" />
		    <!-- end input field -->  	  
		  	</div>
		  	</div>
		  	
			<c:if test="${gSiteConfig['gMYLIST']}">
		  	<div class="list${classIndex % 2}"><c:set var="classIndex" value="${classIndex+1}" />
		  	<div class="listfl"><div class="helpImg"><img class="toolTipImg" title="List::check it to add to list." src="../graphics/question.gif" /></div><fmt:message key="addToList" />:</div>
		  	<div class="listp">
		  	<!-- input field -->
	 			<form:checkbox path="product.addToList" />
		    <!-- end input field -->  	  
		  	</div>
		  	</div>
		  	</c:if>
		  	
		  	<c:if test="${gSiteConfig['gPRESENTATION']}">
		  	<div class="list${classIndex % 2}"><c:set var="classIndex" value="${classIndex+1}" />
		  	<div class="listfl"><div class="helpImg"><img class="toolTipImg" title="Presentation::check it to add to Presentation." src="../graphics/question.gif" /></div><fmt:message key="addToPresentationList" />:</div>
		  	<div class="listp">
		  	<!-- input field -->
	 			<form:checkbox path="product.addToPresentation" />
		    <!-- end input field -->  	  
		  	</div>
		  	</div>
		  	</c:if>
		  	
		  	<c:if test="${gSiteConfig['gCOMPARISON']}">
		  	<div class="list${classIndex % 2}"><c:set var="classIndex" value="${classIndex+1}" />
		  	<div class="listfl"><fmt:message key="compare" />:</div>
		  	<div class="listp">
		  	<!-- input field -->
	 			<form:checkbox path="product.compare"/>
		  	<!-- end input field -->	
		  	</div>
		  	</div>
		  	</c:if>	
		  	
		  	<div class="list${classIndex % 2}"><c:set var="classIndex" value="${classIndex+1}" />
		  	<div class="listfl"><div class="helpImg"><img class="toolTipImg" title="Searchable::check it to be searched." src="../graphics/question.gif" /></div><fmt:message key="searchable" />:</div>
		  	<div class="listp">
		  	<!-- input field -->
	 			<form:checkbox path="product.searchable" />
		    <!-- end input field -->  	  
		  	</div>
		  	</div>
		  	
		  	<c:if test="${siteConfig['LUCENE_REINDEX'].value == 'true'}">
     		<div class="list${classIndex % 2}"><c:set var="classIndex" value="${classIndex+1}" />
		  	<div class="listfl"><fmt:message key="searchRank" />:</div>
		  	<div class="listp">
		  	<!-- input field -->
		  	   <form:errors path="product.searchRank" cssClass="error"/>
		       <form:select path="product.searchRank" >
		  	      <c:forEach begin="1" end="100" var="rank">
	 			    <form:option value="${rank}"><c:out value="${rank}" /></form:option>
		  	      </c:forEach>
	 			</form:select>
	 		<!-- end input field -->  	  
		  	</div>
		  	</div>
		  	</c:if>
    
		  	<c:if test="${gSiteConfig['gASI'] != '' or siteConfig['INGRAM'].value != ''}">
		  	<div class="list${classIndex % 2}"><c:set var="classIndex" value="${classIndex+1}" />
		  	<div class="listfl"><div class="helpImg"><img class="toolTipImg" title="Freeze From Feed::Check this flag to freeze this product for future Feed update." src="../graphics/question.gif" /></div><fmt:message key="feedFreeze" />:</div>
		  	<div class="listp">
		  	<!-- input field -->
	 			<form:checkbox path="product.feedFreeze" />
		    <!-- end input field -->  	  
		  	</div>
		  	</div>		  	
		  	</c:if>
		  	
		  	<c:if test="${gSiteConfig['gSHOPPING_CART']}">
		  	<div class="list${classIndex % 2}"><c:set var="classIndex" value="${classIndex+1}" />
		  	<div class="listfl"><div class="helpImg"><img class="toolTipImg" title="Custom Shipping::Check this flag to enable custom shipping for this product." src="../graphics/question.gif" /></div><fmt:message key="custom" /> <fmt:message key="shipping" />:</div>
		  	<div class="listp">
		  	<!-- input field -->
	 			<form:checkbox path="product.customShippingEnabled" />
		    <!-- end input field -->  	  
		  	</div>
		  	</div>		  	
		  	</c:if>
		  	
		  	<c:if test="${gSiteConfig['gWEATHER_CHANNEL']}">	
		  	<div class="list${classIndex % 2}"><c:set var="classIndex" value="${classIndex+1}" />  	
		  	<div class="listfl"><div class="helpImg"><img class="toolTipImg" title="Temperature::Maximum temperature ideal for this item." src="../graphics/question.gif" /></div><fmt:message key="temperature" />:</div>
		  	<div class="listp">
		  	<!-- input field -->
				<form:input path="product.temperature" cssClass="textfield" size="8" maxlength="8" htmlEscape="true" />&deg;F
				<form:errors path="product.temperature" cssClass="error"/>
		    <!-- end input field -->  	  
		  	</div>
		  	</div>
		  	</c:if>
		  	
		  	<div class="list${classIndex % 2}"><c:set var="classIndex" value="${classIndex+1}" />
            <div class="listfl"><div class="helpImg"><img class="toolTipImg" title="Category Association::Enter Category Ids here. Seperate them with comma or press enter. It helps in searching." src="../graphics/question.gif" /></div>Category Association:</div>
            <div class="listp">
            <!-- input field -->
            <form:textarea path="product.categoryIds" cssClass="textfield" rows="10" cols="20"/>
            <form:errors path="product.categoryIds" cssClass="error"/>
            <img height="180" width="300" border="0" src="../../assets/Image/Product/categories-back.png">
            <div class="helpNote">
            	<p>Enter Category Ids here. Seperate them with comma or press enter. It helps in searching. </p>
            </div>
            <!-- end input field -->
            </div>
            </div>
		  	
		  	<div class="list${classIndex % 2}"><c:set var="classIndex" value="${classIndex+1}" />
		  	<div class="listfl"><div class="helpImg"><img class="toolTipImg" title="Keyword::Seperate them with comma or press enter. It helps in searching." src="../graphics/question.gif" /></div><fmt:message key="keywords" />:</div>
		  	<div class="listp">
		  	<!-- input field -->
		        <form:textarea path="product.keywords" cssClass="textfield" rows="10" cols="45"/>
		        <form:errors path="product.keywords" cssClass="error"/>
		        
		    <!-- end input field -->  	  
		  	</div>
		  	</div>

			<c:choose>
			<c:when test="${siteConfig['PRODUCT_MULTI_OPTION'].value != 'true'}">
				<div class="list${classIndex % 2}"><c:set var="classIndex" value="${classIndex+1}" />
			  	<div class="listfl"><div class="helpImg"><img class="toolTipImg" title="Option code::Add the available option code for the product. To manage option code go to Procucts --> Options. MAX 300 character" src="../graphics/question.gif" /></div><fmt:message key="option" /> <fmt:message key="code" />:</div>
			  	<div class="listp">
			  	<!-- input field -->
			        <form:input path="product.optionCode" cssClass="textfield" maxlength="300" htmlEscape="true"/>
		   			<form:errors path="product.optionCode" cssClass="error" />
		   			
			    <!-- end input field -->  	  
			  	</div>
			  	</div>
			</c:when>
			<c:otherwise>
				<div class="list${classIndex % 2}"><c:set var="classIndex" value="${classIndex+1}" />
			  	<div class="listfl"><div class="helpImg"><img class="toolTipImg" title="Option code::Add the available option code for the product. Use multiple option code seperated with comma. To manage option code go to Procucts --> Options." src="../graphics/question.gif" /></div><fmt:message key="option" /> <fmt:message key="code" />:</div>
			  	<div class="listp">
			  	<!-- input field -->
			        <form:textarea path="product.optionCode" cssClass="textfield" rows="10" cols="45"/>
		   			<form:errors path="product.optionCode" cssClass="error" />
			    <!-- end input field -->  	  
		  	</div>
		  	</div>
			</c:otherwise>
			</c:choose>
		  	
		  	<c:if test="${gSiteConfig['gPRODUCT_REVIEW'] or siteConfig['PRODUCT_RATE'].value == 'true'}">
		  	<div class="list${classIndex % 2}"><c:set var="classIndex" value="${classIndex+1}" />
		  	<div class="listfl"><fmt:message key="review" />:</div>
		  	<div class="listp">
		  	<!-- input field -->
	   			<form:checkbox path="product.enableRate" />
		    <!-- end input field -->   	
		  	</div>
		  	</div>
		  	</c:if>
		  	
		  	<c:if test="${gSiteConfig['gPRICE_TIERS'] > 1}">
		  	<div class="list${classIndex % 2}"><c:set var="classIndex" value="${classIndex+1}" />
		  	<div class="listfl"><div class="helpImg"><img class="toolTipImg" title="Note::This code will allow qty break discounting on multiple items in a shopping cart." src="../graphics/question.gif" /></div><fmt:message key="crossItems" /> <fmt:message key="code" />:</div>
		  	<div class="listp">
		  	<!-- input field -->
		        <form:input path="product.crossItemsCode" cssClass="textfield" maxlength="50" htmlEscape="true"/>
	   			<form:errors path="product.crossItemsCode" cssClass="error" />
	   			<div class="helpNote">
			      <p>This code will allow qty break discounting on multiple items in a shopping cart. </p>
			     
			    </div>
		    <!-- end input field -->  	  
		  	</div>
		  	</div>
		  	</c:if>			  	
		  	
	  		<c:if test="${gSiteConfig['gPROTECTED'] > 0}">
		  	<div class="list${classIndex % 2}"><c:set var="classIndex" value="${classIndex+1}" />
 			<div class="listfl"><fmt:message key="protectedLevel" />:</div>
			<div class="listp">
			<!-- input field -->
		    	<form:select path="product.protectedLevel">
		      	  <form:option value="0"><fmt:message key="none"/></form:option>
			      <c:forEach begin="1" end="${gSiteConfig['gPROTECTED']}" var="protected1">
			    	<c:set var="key" value="protected${protected1}"/>
			    	<c:choose>
			      	  <c:when test="${labels[key] != null and labels[key] != ''}">
			        	<c:set var="label" value="${labels[key]}"/>
			      	  </c:when>
			      	  <c:otherwise><c:set var="label" value="${protected1}"/></c:otherwise>
			    	</c:choose>		        
		        	<form:option value="${protectedLevels[protected1-1]}"><c:out value="${label}"/></form:option>
		      	  </c:forEach>
		    	</form:select>
            	<form:errors path="product.protectedLevel" cssClass="error" />
        	<!-- end input field -->   	
			</div>
			</div>
        	</c:if>
        	
        	<c:if test="${gSiteConfig['gBOX']}">
		  	<div class="list${classIndex % 2}"><c:set var="classIndex" value="${classIndex+1}" />
		  	<div class="listfl"><div class="helpImg"><img class="toolTipImg" title="Note::If no value enter, User can not add this item to shopping cart." src="../graphics/question.gif" /></div><fmt:message key="box"/> <fmt:message key="size"/>:</div>
		  	<div class="listp">
		  	<!-- input field -->
			    <form:input path="product.boxSize" size="10" cssClass="textfield" maxlength="10" />
			    <form:errors path="product.boxSize" cssClass="error"/>
			     <div class="helpNote">
				   <p>User must add this amount of item to the box or more. To add price for each extra look at the following field.</p>
				 </div>
	        <!-- end input field -->  	  
		  	</div>
		  	</div>

		  	<div class="list${classIndex % 2}"><c:set var="classIndex" value="${classIndex+1}" />
		  	<div class="listfl"><div class="helpImg"><img class="toolTipImg" title="Note::Customers will charge this amount if they add more than box size." src="../graphics/question.gif" /></div><fmt:message key="extraAmount"/>:</div>
		  	<div class="listp">
		  	<!-- input field -->
			    <fmt:message key="${siteConfig['CURRENCY'].value}" /><form:input path="product.boxExtraAmt" cssClass="textfield" size="10" maxlength="10" />
			    <form:errors path="product.boxExtraAmt" cssClass="error"/>
	        <!-- end input field -->  	  
		  	</div>
		  	</div>
		  	</c:if>  
		  	
		  	<c:if test="${gSiteConfig['gCUSTOM_LINES']}">
		  	<div class="list${classIndex % 2}"><c:set var="classIndex" value="${classIndex+1}" />
		  	<div class="listfl"><div class="helpImg"><img class="toolTipImg" title="Note::Customers will provide more attribute for this product on front end." src="../graphics/question.gif" /></div><fmt:message key="numberCustomLine"/>:</div>
		  	<div class="listp">
		  	<!-- input field -->
			    <form:select path="product.numCustomLines">
			    <option value=""><fmt:message key="none"/></option>
            	<c:forTokens items="1,2,3,4,5,6,7,8,9,10" delims="," var="current">
			  	  <option value="${current}" <c:if test="${current == productForm.product.numCustomLines}">selected</c:if>>${current}</option>
			    </c:forTokens>
        		</form:select>
	        <!-- end input field -->  	  
		  	</div>
		  	</div>	
		  	
		  	<div class="list${classIndex % 2}"><c:set var="classIndex" value="${classIndex+1}" />
		  	<div class="listfl"><div class="helpImg"><img class="toolTipImg" title="Note::Maximum character in custom line." src="../graphics/question.gif" /></div><fmt:message key="numberCharacter"/>:</div>
		  	<div class="listp">
		  	<!-- input field -->
			    <form:select path="product.customLineCharacter">
            	<c:forEach begin="5" end="80" step="5" var="current">
			  	  <option value="${current}" <c:if test="${current == productForm.product.customLineCharacter}">selected</c:if>>${current}</option>
			    </c:forEach>
        		</form:select>
	        <!-- end input field -->  	  
		  	</div>
		  	</div>
		  	</c:if>  

	<!-- end tab -->        
	</div>  