<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<c:if test="${model.costList != null}">
<c:set var="classIndex" value="0" />
<table border="0" style="padding:3px;width:100%">
<c:forEach items="${model.costList}" var="cost">
 <c:set var="classIndex" value="${classIndex+1}" />
 <tr class="list${classIndex % 2}">
  <td style="padding:5px;"><c:out value="${cost['company']}"/></td>
  <td style="padding:5px;"><c:out value="${cost['po_number']}"/></td>
  <td style="padding:5px;"><fmt:message key="${siteConfig['CURRENCY'].value}" /><c:out value="${cost['unit_cost']}"/></td>
  <td style="padding:5px;"><fmt:formatDate type="date" value="${cost['created']}" pattern="M/dd/yy"/></td>
 </tr>
</c:forEach>
</table>
</c:if>