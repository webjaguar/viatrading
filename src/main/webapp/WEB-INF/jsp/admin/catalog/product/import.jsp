<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="/WEB-INF/tlds/spring.tld" prefix="spring" %>
<%@ taglib uri="http://www.springframework.org/security/tags" prefix="sec" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>

<tiles:insertDefinition name="admin.catalog.product" flush="true">
  <tiles:putAttribute name="content" type="string">
<sec:authorize ifAnyGranted="ROLE_AEM,ROLE_ADMIN_${prop_site_id},ROLE_PRODUCT_IMPORT_EXPORT">
<script language="JavaScript" type="text/JavaScript">
<!--
window.addEvent('domready', function(){			
			var tabs1 = new SimpleTabs($('tab-block-1'), { selector: 'h4'});
			var Tips1 = new Tips($$('.toolTipImg'));
		});
<c:if test="${gSiteConfig['gCUSTOMER_SUPPLIER']}">
function checkSupplierId() {
	if (document.getElementById("defaultSupplierId").value == '') {
		return confirm('Are you sure you want to upload items not associated to any supplier?')	
	} else {
		return true;
	}
}
</c:if>
//-->
</script>
<form:form commandName="importProductSearch" method="post" name="uploadForm" enctype="multipart/form-data">
<!-- main box -->
  <div id="mboxfull">
  
  <!-- start table -->
  <table cellpadding="0" cellspacing="0" border="0" class="module" >
  <tr>
    <td class="topl_g">
	  
	  <!-- breadcrumb -->
	  <p class="breadcrumb">
	    <a href="../catalog">Catalog</a> &gt;
	    <a href="../catalog/productList.jhtm">Products</a> &gt;
	    import
	  </p>
	  
	  <!-- Error Message -->
	  <c:if test="${!empty message}">
  		<div class="message"><spring:message code="${message}" arguments="${arguments}"/></div>
	  </c:if>  

  
  </td><td class="topr_g" ></td></tr>
  <tr><td class="boxmidlrg" >

 <!-- tabs -->
 <div id="tab-block-1">
	<h4 title="Import">Import</h4>
	<div>
	<!-- start tab -->

        <c:set var="classIndex" value="0" />
        
	  	<div class="listdivi ln tabdivi"></div>
	  	<div class="listdivi"></div>
	  	
	  		<c:if test="${gSiteConfig['gCUSTOMER_SUPPLIER']}">
		  	<div class="list${classIndex % 2}"><c:set var="classIndex" value="${classIndex+1}" />
		  	<div class="listfl"><div class="helpImg"><img class="toolTipImg" title="Note::This should be a valid customer's supplier id." src="../graphics/question.gif" /></div><fmt:message key="customer" /> <fmt:message key="supplierId" />:</div>
		  	<div class="listp">
		  	<!-- input field -->
				<form:input path="defaultSupplierId" cssClass="textfield50" maxlength="10" htmlEscape="true"/>
				<form:errors path="defaultSupplierId" cssClass="error"/>
	 		</div>
		  	</div>		 
		  	</c:if> 	
	   
			<c:if test="${invalidItems != null}">
		  	<div class="list${classIndex % 2}"><c:set var="classIndex" value="${classIndex+1}" />
		  	<div class="listfl"></div>
		  	<div class="listp">
		  	<!-- input field -->
				<table border="0" class="listings" cellpadding="0" cellspacing="0" width="100%">
				<tr height="20">
				  <td class="importIndexCol importHdr">&nbsp;</td>
				  <td class="importHdr">Excel line #</td>
				  <td class="importHdr"><spring:message code="productId"/></td>
				  <td class="importHdr" align="center"><spring:message code="productSku"/></td>
				  <td class="importHdr" width="100%">Reason</td>
				</tr>
	   			<c:forEach items="${invalidItems}" var="item" varStatus="status">
				<tr height="20" class="row${status.index % 2}" onmouseover="changeStyleClass(this,'rowOver')" onmouseout="changeStyleClass(this,'row${status.index % 2}')">
				  <td class="importIndexCol import"><c:out value="${status.count}"/>.</td>
				  <td class="import" align="center"><c:out value="${item['rowNum']}"/></td>
				  <td class="import" align="center"><c:out value="${item['id']}"/></td>
				  <td class="import" align="center"><c:out value="${item['sku']}"/></td>
				  <td class="import"><c:out value="${item['reason']}"/></td>
				</tr>
				</c:forEach>
				</table>								

		    <!-- end input field -->   	
		  	</div>
		  	</div>
		  	</c:if>
		  	
		  	<div class="list${classIndex % 2}"><c:set var="classIndex" value="${classIndex+1}" />
		  	<div class="listfl"><spring:message code="excelFile" text="Excel File"/>:</div>
		  	<div class="listp">
		  	<!-- input field -->
		  		<table>
		  		<c:if test="${siteConfig['PRODUCT_IMPORT_FILENAME'].value != ''}">
		  		  <tr>
		  		    <td>
		  		      <c:choose>
		  		        <c:when test="${importProductSearch.importFileMap != null}">
		  		        Use <span style="color:red;"><c:out value="${siteConfig['PRODUCT_IMPORT_FILENAME'].value}"/></span> 
		  		        <c:choose>
				        <c:when test="${file['size'] > (1024*1024)}">
				    	  (<fmt:formatNumber value="${file['size']/1024/1024}" pattern="#,##0.0"/> MB
				    	</c:when>
				        <c:when test="${file['size'] > 1024}">
				    	  (<fmt:formatNumber value="${file['size']/1024}" pattern="#,##0"/> KB
				    	</c:when>
				    	<c:otherwise>
				    	  (1 KB
				    	</c:otherwise>
						</c:choose>
						-
		  		        <fmt:formatDate type="both" timeStyle="full" value="${importProductSearch.importFileMap['lastModified']}"/>)
						found in FTP
		  		        </c:when>
		  		        <c:otherwise>
		  		        Upload <span style="color:red;"><c:out value="${siteConfig['PRODUCT_IMPORT_FILENAME'].value}"/></span> 
		  		      	via FTP
		  		        </c:otherwise>
		  		      </c:choose>
		  		    </td>
		  		  </tr>	
		  		  <tr><td>&nbsp;&nbsp;&nbsp;- OR -</td></tr>	
		  		</c:if>
		  		  <tr><td><input value="browse" type="file" name="file"/></td></tr>	
		  		</table>
	 		</div>
		  	</div>			  	

		  	<div class="list${classIndex % 2}"><c:set var="classIndex" value="${classIndex+1}" />
		  	<div class="listfl"><fmt:message key="instructions/tips" />:</div>
		  	<div class="listp">
		  		<table width="50%" cellspacing="5">
		  		  <tr>
		  		  	<td valign="top"> - </td>
		  			<td>The maximum file upload size is 2.00 MB.</td>
		  		  </tr>
		  		  <tr>
		  		  	<td valign="top"> - </td>
		  			<td>Column "SKU" is required.</td>
		  		  </tr>
		  		  <tr>
		  		  	<td valign="top"> - </td>
		  			<td>Including the "ID" column will allow you to change the SKU of an item.</td>
		  		  </tr>		  		  
		  		  <tr>
		  		  	<td valign="top"> - </td>
		  			<td>All updated items will be kept from customers' saved shopping cart.
		  				To retain/remove any item, just add a header column titled "RetainOnCart" and 
		  				put "1" to retain or "0" to keep. 
		  			</td>
		  		  </tr>
		  		  <tr>
		  		  	<td valign="top"> - </td>
		  			<td>Do not import inventory, if inventory history is important for you. Use Purchase Order Import for importing inventory.</td>
		  		  </tr>
		  		  
		  		</table>
	 		</div>
		  	</div>		 
	  	
	<!-- end tab -->        
	</div> 
	
<!-- end tabs -->			
</div>

<!-- start button -->
<div align="left" class="button"> 
    <input type="submit" name="__upload" value="<spring:message code="fileUpload"/>" <c:if test="${gSiteConfig['gCUSTOMER_SUPPLIER']}">onClick="return checkSupplierId()"</c:if>>
</div>
<!-- end button -->	     
  
  <!-- end table -->
    </td><td class="boxmidr" ></td></tr>
  <tr><td class="botl"></td><td class="botr"></td></tr>
  </table>
  
<!-- end main box -->  
 </div>
</form:form>
</sec:authorize>

  </tiles:putAttribute>    
</tiles:insertDefinition>