<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ taglib uri="/WEB-INF/tlds/spring.tld" prefix="spring" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ taglib uri="http://www.springframework.org/security/tags" prefix="sec" %>

<tiles:insertDefinition name="admin.catalog.manufacturer" flush="true">
	<tiles:putAttribute name="content" type="string">
<sec:authorize ifAnyGranted="ROLE_AEM,ROLE_ADMIN_${prop_site_id},ROLE_MANUFACTURER_CREATE,ROLE_MANUFACTURER_UPDATE,ROLE_MANUFACTURER_DELETE">
<script type="text/javascript">
<!--
window.addEvent('domready', function(){			
	var tabs1 = new SimpleTabs($('tab-block-1'), { selector: 'h4'});	
});
//-->
</script>
<form:form commandName="manufacturerForm" method="post">
<!-- main box -->
  <div id="mboxfull">
  
  <table cellpadding="0" cellspacing="0" border="0" class="module" >
  <tr>
    <td class="topl_g">
	  
	  <!-- breadcrumb -->
	  <p class="breadcrumb">
	  <a href="../catalog">Catalog</a> &gt;
	  <a href="../catalog/manufacturerList.jhtm">Manufacturer</a> &gt;
	    form 
	  </p>
	  
	  <!-- Error Message -->
	  <c:if test="${!empty message}">
		  <div class="message"><fmt:message key="${message}" /></div>
	  </c:if>	
  	  <spring:hasBindErrors name="manufacturerForm">
		<div class="message">Please fix all errors!</div>
	  </spring:hasBindErrors>

   </td><td class="topr_g" ></td>
  </tr>
  <tr><td class="boxmidlrg" >
						  
	<div id="tab-block-1">
		<h4 title="Manufacturer Form">Manufacturer</h4>
			<div>
			<!-- start tab -->
			<c:set var="classIndex" value="0" />
													         
			<div class="listdivi ln tabdivi"></div>  
			<div class="list${classIndex % 2}"><c:set var="classIndex" value="${classIndex+1}" />
			<div class="listfl"><div class="requiredField"><fmt:message key="name" />:</div></div>
			<div class="listp">
			<!-- input field -->
				<form:input path="manufacturer.name" cssClass="textfield" size="50" maxlength="120" htmlEscape="true"/>
				<form:errors path="manufacturer.name" cssClass="error" />
			<!-- end input field -->   	
			</div>
			</div>	
			<!-- end tab -->	 
			</div>
															 
	<!-- end tabs -->	
	</div>
	
	<!-- start button -->
	<div align="left" class="button">
		<c:if test="${manufacturerForm.newManufacturer}">
			<sec:authorize ifAnyGranted="ROLE_AEM,ROLE_ADMIN_${prop_site_id},ROLE_MANUFACTURER_CREATE">
				<input type="submit" value="<spring:message code="add"/>" />
			</sec:authorize>  
		</c:if>
		<c:if test="${!manufacturerForm.newManufacturer}">     
			<sec:authorize ifAnyGranted="ROLE_AEM,ROLE_ADMIN_${prop_site_id},ROLE_MANUFACTURER_UPDATE">
				<input type="submit" value="<spring:message code="update"/>" />
			</sec:authorize>
			<sec:authorize ifAnyGranted="ROLE_AEM,ROLE_ADMIN_${prop_site_id},ROLE_MANUFACTURER_DELETE">  
				<input type="submit" value="<spring:message code="delete"/>" name="_delete" onClick="return confirm('Delete permanently?')" />
			</sec:authorize>  
		</c:if>
	</div>
	<!-- end button -->
			
  <!-- end table -->
  </td><td class="boxmidr" ></td></tr>  
  <tr><td class="botl"></td><td class="botr"></td>
  </table> 
  
  
  </div>
</form:form>
</sec:authorize>

</tiles:putAttribute>
</tiles:insertDefinition>