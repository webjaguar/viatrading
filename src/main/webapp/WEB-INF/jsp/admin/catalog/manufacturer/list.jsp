<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/security/tags" prefix="sec" %>

<tiles:insertDefinition name="admin.catalog.manufacturer" flush="true">
	<tiles:putAttribute name="content" type="string">
	
<form action="manufacturerList.jhtm" method="post">
<div id="mboxfull">

<!-- start table -->
  <table cellpadding="0" cellspacing="0" border="0" class="module" >
  <tr>
    <td class="topl_g">
	  
	  <!-- breadcrumb -->
	  <p class="breadcrumb">
	    <a href="../catalog"><fmt:message key="catalog" /></a> &gt;
	    <fmt:message key="manufacturer" />
	  </p>
	  
	  <!-- Error -->
	  
	  <!-- header image -->
	  <img class="headerImage" src="../graphics/category.gif" />
  
  </td><td class="topr_g" ></td></tr>
  <tr><td class="boxmidlrg" >
   
  <!-- tabs -->
  <div class="tab-wrapper">
  <h4 title="List of Manufacturers"></h4>
  <div> 
   <div class="listdivi"></div>
	    	<div class="listlight">
	    	<!-- input field --> 
		  	<table border="0" cellpadding="0" cellspacing="1" width="100%">
			  <tr>
			  <c:if test="${model.count > 0}">
			  <td class="pageShowing">
			  <fmt:message key="showing">
				<fmt:param value="${manufacturerSearch.offset+1}"/>
				<fmt:param value="${model.pageEnd}"/>
				<fmt:param value="${model.count}"/>
			  </fmt:message>
			  </td>
			  </c:if>
			  <td class="pageNavi">
			  Page
			  <select name="page" id="page" onchange="submit()">
			  <c:forEach begin="1" end="${model.pageCount}" var="page">
			  	<option value="${page}" <c:if test="${page == (manufacturerSearch.page)}">selected</c:if>>${page}</option>
			  </c:forEach>
			  </select>
			  of <c:out value="${model.pageCount}"/>
			  | 
			  <c:if test="${manufacturerSearch.page == 1}"><span class="pageNaviDead">previous</span></c:if>
			  <c:if test="${manufacturerSearch.page != 1}"><a href="<c:url value="manufacturerList.jhtm"><c:param name="page" value="${manufacturerSearch.page-1}"/></c:url>" class="pageNaviLink">previous</a></c:if>
			  | 
			  <c:if test="${manufacturerSearch.page == model.pageCount}"><span class="pageNaviDead">next</span></c:if>
			  <c:if test="${manufacturerSearch.page != model.pageCount}"><a href="<c:url value="manufacturerList.jhtm"><c:param name="page" value="${manufacturerSearch.page+1}"/></c:url>" class="pageNaviLink">next</a></c:if>
			  </td>
			  </tr>
			</table>
	    		    			
	    	<table border="0" cellpadding="0" cellspacing="0" width="100%" class="listings">
			  <tr class="listingsHdr2">
			    <td class="indexCol">&nbsp;</td>
			    <td class="listingsHdr3"><fmt:message key="manufacturerName" /></td>
			    <td class="listingsHdr3"><fmt:message key="dateAdded" /></td>
			    <td class="listingsHdr3"><fmt:message key="lastModified" /></td>			    
			  </tr>
			<c:forEach items="${model.manufacturerlist}" var="manufacturer" varStatus="status">
			  <tr class="row${status.index % 2}" onmouseover="changeStyleClass(this,'rowOver')" onmouseout="changeStyleClass(this,'row${status.index % 2}')">
			    <td class="indexCol"><c:out value="${status.count}"/>.</td>
			    <td class="nameCol"><a href="manufacturer.jhtm?id=${manufacturer.id}" class="nameLink"><c:out value="${manufacturer.name}"/></a></td>
			    <td class="nameCol"><fmt:formatDate type="both" timeStyle="full" pattern="MM/dd/yyyy hh:mm a" value="${manufacturer.dateAdded}"/></td>
			    <td class="nameCol"><fmt:formatDate type="both" timeStyle="full" pattern="MM/dd/yyyy hh:mm a" value="${manufacturer.lastModified}"/></td>			    
			  </tr>
			</c:forEach>
			<c:if test="${model.count == 0}">
			  <tr class="emptyList"><td colspan="3">&nbsp;</td></tr>
			</c:if>
			</table>
			
			<table border="0" cellpadding="0" cellspacing="1" width="100%">
			  <tr>
			  <td class="pageSize">
			  <select name="size" onchange="document.getElementById('page').value=1;submit()">
			  <c:forTokens items="10,25,50,100" delims="," var="current">
			  	  <option value="${current}" <c:if test="${current == manufacturerSearch.pageSize}">selected</c:if>>${current} <fmt:message key="perPage" /></option>
			  </c:forTokens>
			  </select>
			  </td>
			  </tr>
			</table>
	    	
		  	<!-- start button --> 
		 	<div align="left" class="button">
		  	  <sec:authorize ifAnyGranted="ROLE_AEM,ROLE_ADMIN_${prop_site_id}">
		  	    <input type="submit" name="__add" value="<fmt:message key="add" />">
		  	  </sec:authorize>
			</div> 
			<!-- end button -->	
		  	</div>    
  </div>  
  </div>  
  </td><td class="boxmidr" ></td></tr>  
  </table>
</div>
</form>
	
	</tiles:putAttribute>
</tiles:insertDefinition>