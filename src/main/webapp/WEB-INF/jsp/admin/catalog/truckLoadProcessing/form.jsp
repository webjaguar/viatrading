<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ taglib uri="/WEB-INF/tlds/spring.tld" prefix="spring" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ taglib uri="http://www.springframework.org/security/tags" prefix="sec" %>

<tiles:insertDefinition name="admin.catalog.truckLoadProcess" flush="true">
	<tiles:putAttribute name="content" type="string">

<sec:authorize ifAnyGranted="ROLE_AEM,ROLE_ADMIN_${prop_site_id},ROLE_PROCESSING_CREATE,ROLE_PROCESSING_UPDATE,ROLE_PROCESSING_DELETE"> 
<script type="text/javascript" src="../javascript/autocompleter1.2.js"></script>
<script type="text/javascript" src="../javascript/autocompleter.Request1.2.js"></script>
<script type="text/javascript" src="../javascript/autocompleter.Local1.2.js"></script>
<script type="text/javascript" src="../javascript/observer.js"></script>
<style type="text/css">
ul.autocompleter-choices {min-height: 180px;}
</style>
	<c:set var="classIndex" value="${classIndex+1}" />
<script type="text/javascript">
		window.addEvent('domready', function(){			
			var tabs1 = new SimpleTabs($('tab-block-1'), { selector: 'h4'});
				this["el"+1] = $('addSku'+1);
				new Autocompleter.Request.HTML(this["el"+1], '../orders/show-ajax-skus.jhtm', {
				'indicatorClass': 'autocompleter-loading',
				'postData': {
					'inactive': '1' // send additional POST data,
				 },'onSelection': function(element, selected, value, input) {
						
					addPackingV1.value=selected.getNext().getNext().innerHTML;
					addNumOfPalletsV1.value=selected.getNext().getNext().getNext().innerHTML;
					}
				
				});
				this["el"+1] = $('addSkuP'+1);
				new Autocompleter.Request.HTML(this["el"+1], '../orders/show-ajax-skus.jhtm', {
				
				'indicatorClass': 'autocompleter-loading',
				'postData': {
					'inactive': '1' // send additional POST data,
				 },
				'onSelection': function(element, selected, value, input) {
					addDesc1.value = selected.getNext().innerHTML;
				 	addPacking1.value = selected.getNext().getNext().innerHTML;
					addNumOfPallets1.value = selected.getNext().getNext().getNext().innerHTML;
					
				}});
		});
		
		
		function toggleAll(ele){
		
			var checkboxes =  document.getElementsByName('__selected_id');
		     for( var i=0; i < checkboxes.length; i++ ) { 

				if(ele.checked) {
                     	checkboxes[i].checked = "checked"
				} else {
                 	checkboxes[i].checked = ""
				} 
		     }
		}

		function truckLoadOriginal() {
			  var tr = document.createElement("tr");

			  tr.appendChild(document.createElement("td"));

			  var childNodeArray = document.getElementById('truckLoadOriginal').childNodes;
			  var td1 = document.createElement("td");  
			  var textfield1 = document.createElement("input");
			  textfield1.setAttribute("type", "text"); 
			  textfield1.setAttribute("id" , "addSku" + childNodeArray.length); 
			  textfield1.setAttribute("name", "__truckLoadOriginal"); 
			  textfield1.setAttribute("class", "textfield250"); 
			  textfield1.setAttribute("maxlength", "50");
			  td1.appendChild(textfield1);
			  tr.appendChild(td1);

			  var td2 = document.createElement("td");  
			  var textfield2 = document.createElement("input");
			  textfield2.setAttribute("type", "text"); 
			  textfield2.setAttribute("name", "__truckLoadOriginal_qty"); 
			  textfield2.setAttribute("class", "textfield50"); 
			  textfield2.setAttribute("maxlength", "10");
			  td2.appendChild(textfield2);
			  tr.appendChild(td2);
			  
			  var td2 = document.createElement("td");  
			  var textfield2 = document.createElement("input");
			  textfield2.setAttribute("type", "text"); 
			  textfield2.setAttribute("name", "__truckLoadOriginal_value"); 
			  textfield2.setAttribute("class", "textfield70"); 
			  td2.appendChild(textfield2);
			  tr.appendChild(td2);
			  
			  var td2 = document.createElement("td");  
			  var textfield2 = document.createElement("input");
			  textfield2.setAttribute("type", "text"); 
			  textfield2.setAttribute("name", "__truckLoadOriginal_ValCG"); 
			  textfield2.setAttribute("class", "textfield70"); 
			  td2.appendChild(textfield2);
			  tr.appendChild(td2);
			  
			  var td5 = document.createElement("td");  
			  var textfield5 = document.createElement("input");
			  textfield5.setAttribute("type", "text"); 
			  textfield5.setAttribute("id", "addPackingV"+ childNodeArray.length); 
			  textfield5.setAttribute("name", "__truckLoadOriginal_packing"); 
			  textfield5.setAttribute("class", "textfield70"); 
			  td5.appendChild(textfield5);
			  tr.appendChild(td5);
			  
			  var td6 = document.createElement("td");  
			  var textfield6 = document.createElement("input");
			  textfield6.setAttribute("type", "text");
			  textfield6.setAttribute("id", "addNumOfPalletsV" + childNodeArray.length); 
			  textfield6.setAttribute("name", "__truckLoadOriginal_numOfPallets"); 
			  textfield6.setAttribute("class", "textfield70"); 
			  td6.appendChild(textfield6);
			  tr.appendChild(td6);
			  
			  
			  document.getElementById('truckLoadOriginal').appendChild(tr);
			  var e =  childNodeArray.length -1;
			  this["el"+e] = $('addSku'+e);
				new Autocompleter.Request.HTML(this["el"+e], '../orders/show-ajax-skus.jhtm', {
				'indicatorClass': 'autocompleter-loading',
				'postData': {
					'inactive': '1' // send additional POST data,
				 }, 
				'onSelection': function(element, selected, value, input) {
					console.log(selected.getNext().innerHTML + "selected.getNext().innerHTML");
						/* $('addDesc'+e).value = selected.getNext().innerHTML; */
					 	$('addPackingV'+e).value = selected.getNext().getNext().innerHTML;
						$('addNumOfPalletsV'+e).value = selected.getNext().getNext().getNext().innerHTML;

					} 
				});
		}

		function truckLoadProduced() {
			  var tr = document.createElement("tr");

			  tr.appendChild(document.createElement("td"));
			  var childNodeArray = document.getElementById('truckLoadProduced').childNodes;
			  console.log(childNodeArray.length + "------childNodeArray");
			  var td1 = document.createElement("td");  
			  var textfield1 = document.createElement("input");
			  textfield1.setAttribute("type", "text"); 
			  textfield1.setAttribute("id" , "addSkuP" + childNodeArray.length); 
			  textfield1.setAttribute("name", "__truckLoadProduced_sku"); 
			  textfield1.setAttribute("class", "textfield200"); 
			  textfield1.setAttribute("maxlength", "30");
			  td1.appendChild(textfield1);
			  tr.appendChild(td1);
			  
			  var td2 = document.createElement("td");  
			  var textfield2 = document.createElement("input");
			  textfield2.setAttribute("type", "text"); 
			  textfield2.setAttribute("id", "addDesc"+ childNodeArray.length); 
			  textfield2.setAttribute("name", "__truckLoadProduced_Desc"); 
			  textfield2.setAttribute("class", "textfield250"); 
			  textfield2.setAttribute("maxlength", "200");
			  td2.appendChild(textfield2);
			  tr.appendChild(td2);
			  
			  var td2 = document.createElement("td");  
			  var textfield2 = document.createElement("input");
			  textfield2.setAttribute("type", "text"); 
			  textfield2.setAttribute("name", "__truckLoadProduced_qty"); 
			  textfield2.setAttribute("class", "textfield50"); 
			  textfield2.setAttribute("maxlength", "30");
			  td2.appendChild(textfield2);
			  tr.appendChild(td2);

			  var td2 = document.createElement("td");  
			  var textfield2 = document.createElement("input");
			  textfield2.setAttribute("type", "text"); 
			  textfield2.setAttribute("name", "__truckLoadProduced_unitPrice"); 
			  textfield2.setAttribute("class", "textfield50"); 
			  textfield2.setAttribute("maxlength", "30");
			  td2.appendChild(textfield2);
			  tr.appendChild(td2);
			  
			  var td2 = document.createElement("td");  
			  var textfield2 = document.createElement("input");
			  textfield2.setAttribute("type", "text"); 
			  textfield2.setAttribute("name", "__truckLoadProduced_condition"); 
			  textfield2.setAttribute("class", "textfield70"); 
			  textfield2.setAttribute("maxlength", "20");
			  td2.appendChild(textfield2);
			  tr.appendChild(td2);
			  
			  var td2 = document.createElement("td");  
			  var textfield2 = document.createElement("input");
			  textfield2.setAttribute("type", "text"); 
			  textfield2.setAttribute("name", "__truckLoadProduced_#units"); 
			  textfield2.setAttribute("class", "textfield70"); 
			  textfield2.setAttribute("maxlength", "20");
			  td2.appendChild(textfield2);
			  tr.appendChild(td2);
			  
			  var td7 = document.createElement("td");  
			  var textfield7 = document.createElement("input");
			  textfield7.setAttribute("type", "text"); 
			  textfield7.setAttribute("name", "__truckLoadProduced_msrp"); 
			  textfield7.setAttribute("class", "textfield40"); 
			  textfield7.setAttribute("maxlength", "20");
			  td7.appendChild(textfield7);
			  tr.appendChild(td7);
			  
			  var td8 = document.createElement("td");  
			  var textfield8 = document.createElement("input");
			  textfield8.setAttribute("type", "checkbox"); 
			  textfield8.setAttribute("name", "__truckLoadProduced_printLabel"); 
			  textfield8.setAttribute("class", "textfield40"); 
			  textfield8.setAttribute("maxlength", "20");
			  td8.appendChild(textfield8);
			  td8.setAttribute("align", "center"); 
			  tr.appendChild(td8);
			  
			  var td9 = document.createElement("td");  
			  var textfield9 = document.createElement("input");
			  textfield9.setAttribute("type", "text");
			  textfield9.setAttribute("id", "addPacking"+ childNodeArray.length); 
			  textfield9.setAttribute("name", "__truckLoadProduced_packing"); 
			  textfield9.setAttribute("class", "textfield40"); 
			  textfield9.setAttribute("maxlength", "20");
			  td9.appendChild(textfield9);
			  tr.appendChild(td9);
			  
			  var td10 = document.createElement("td");  
			  var textfield10 = document.createElement("input");
			  textfield10.setAttribute("type", "text"); 
			  textfield10.setAttribute("id", "addNumOfPallets"+ childNodeArray.length); 
			  textfield10.setAttribute("name", "__truckLoadProduced_numOfPallets"); 
			  textfield10.setAttribute("class", "textfield40"); 
			  textfield10.setAttribute("maxlength", "20");
			  td10.appendChild(textfield10);
			  tr.appendChild(td10);
			  
			  document.getElementById('truckLoadProduced').appendChild(tr);
			  
			  var e =  childNodeArray.length -1;
			  this["el"+e] = $('addSkuP'+e);
			  new Autocompleter.Request.HTML(this["el"+e], '../orders/show-ajax-skus.jhtm', {
				'indicatorClass': 'autocompleter-loading',
				'postData': {
					'inactive': '1' // send additional POST data,
				 },
				'onSelection': function(element, selected, value, input) {
					$('addDesc'+e).value = selected.getNext().innerHTML;
				 	$('addPacking'+e).value = selected.getNext().getNext().innerHTML;
					$('addNumOfPallets'+e).value = selected.getNext().getNext().getNext().innerHTML;

				}
				});
				
		}
	</script>
	
	<form:form commandName="truckLoadProcessForm" method="post">
		<div id="mboxfull">
		<table cellpadding="0" cellspacing="0" border="0" class="module" >
		  <tr>
		    <td class="topl_g">
			  
			  <!-- breadcrumb -->
			  <p class="breadcrumb">
			  <a href="../catalog">Catalog</a> &gt;
			  <a href="../catalog/truckLoadProcessList.jhtm"><fmt:message key="truckLoadProcess" /></a> &gt;
			    form 
			  </p>
			  
			  <!-- Error Message -->
			  <c:if test="${!empty message}">
				  <div class="message"><fmt:message key="${message}" /></div>
			  </c:if>	
		  	  <spring:hasBindErrors name="truckLoadProcessForm">
				<div class="message">Please fix all errors! SKU enter may not be valid.</div>
			  </spring:hasBindErrors>
		
		   </td><td class="topr_g" ></td>
		  </tr>
		  <tr><td class="boxmidlrg" >
		  	<div id="tab-block-1">
				<h4 title="Truck Load Process Form"><fmt:message key="truckLoadProcess" /></h4> 
				<div>
					<!-- start tab -->
					<c:set var="classIndex" value="0" />
					<c:set var="originalQty" value="0" />
					<c:set var="producedQty" value="0" />							         
					<div class="listdivi ln tabdivi"></div> 
					
 					<div class="list${classIndex % 2}"><c:set var="classIndex" value="${classIndex+1}" />					
						<div class="listfl"><strong>Date Started:</strong></div>
						<div class="listp">
							<form:input path="truckLoadProcess.dateStarted" size="50" cssClass="textfield" htmlEscape="true"/>
					   		<img id="start_date_trigger" class="calendarImage" src="../graphics/calendarIcon.jpg" alt="Pick a date" />
						</div>
					</div>
					
				    <div class="list${classIndex % 2}"><c:set var="classIndex" value="${classIndex+1}" />					
						<div class="listfl"><strong>Date Ended:</strong></div>
						<div class="listp">
							<form:input path="truckLoadProcess.dateEnded" size="50" cssClass="textfield" htmlEscape="true"/>
					   		<img id="end_date_trigger" class="calendarImage" src="../graphics/calendarIcon.jpg" alt="Pick a date" />
						</div>
					</div>

       				<div class="list${classIndex % 2}"><c:set var="classIndex" value="${classIndex+1}" />
						<div class="listfl"><strong>Type:</strong></div>						
						<div class="listp">
							<form:select path="truckLoadProcess.type">
								<form:option value="">Please Select</form:option>
								<form:option value="Processing">Processing</form:option>
								<form:option value="PricingOnly">Pricing Only</form:option>
								<form:option value="ConvertingOnly">Converting Only</form:option>
							</form:select>
						</div>						
					</div>	
					<div class="list${classIndex % 2}"><c:set var="classIndex" value="${classIndex+1}" />
					<div class="listfl"><strong><fmt:message key='name' />:</strong></div>
					<div class="listp">
						<form:input path="truckLoadProcess.name" cssClass="textfield" size="50" maxlength="120" htmlEscape="true"/>
					</div>
					</div>
					<div class="list${classIndex % 2}"><c:set var="classIndex" value="${classIndex+1}" />
					<div class="listfl"><strong><fmt:message key='id' />:</strong></div>
					<div class="listp">
						<c:out value="${truckLoadProcessForm.truckLoadProcess.id}"/>
					</div>	
					</div>
					<c:if test="${! truckLoadProcessForm.newTruckLoadProcess}">
						<div class="list${classIndex % 2}"><c:set var="classIndex" value="${classIndex+1}" />
						<div class="listfl"><strong>Created By:</strong></div>
							<c:out value="${truckLoadProcessForm.truckLoadProcess.createdByName}"/>
						<div class="listp">
						</div>	
						</div>
					</c:if>
					<div class="list${classIndex % 2}"><c:set var="classIndex" value="${classIndex+1}" />
		
					<table class="form" border="0" cellspacing="2" cellpadding="0" id="truckLoadOriginal">
					  <tr>
					    <td><a onClick="truckLoadOriginal()"><img src="../graphics/add.png" width="16" border="0"></a></td>
					    <td align="center"><div class="kitPartSku">Add the SKU to be Processed</div></td>
					    <td align="center"><div class="kitPartSku"><fmt:message key="quantity" /></div></td>
					    <td align="center"><div class="kitPartSku"><fmt:message key="value" /></div></td>
					    <td align="center"><div class="kitPartSku">ValCG</div></td>
 					    <td align="center"><div class="kitPartSku" style="width:100px">Packing</div></td>
					    <td align="center"><div class="kitPartSku" style="width:100px">#of Pallets</div></td> 
					    
					    
					  </tr>
					  <tr>
						  <c:if test="${empty truckLoadProcessForm.truckLoadProcess.originalProduct}">
						    <td>&nbsp;</td>
						    <td><input type="text" name="__truckLoadOriginal"  id="addSku1" value="" class="textfield250" maxlength="50"></td>
						    <td><input type="text" name="__truckLoadOriginal_qty" value="" class="textfield50" maxlength="10"></td>
						    <td><input type="text" name="__truckLoadOriginal_value" value="" class="textfield70"></td>					    
						    <td><input type="text" name="__truckLoadOriginal_ValCG" value="" class="textfield70"></td>	
						    <td><input type="text" name="__truckLoadOriginal_packing" value="" id="addPackingV1" class="textfield70"></td>					    
						    <td><input type="text" name="__truckLoadOriginal_numOfPallets" id="addNumOfPalletsV1" value="" class="textfield70"></td>					    
						    				    
						    	    
						  </c:if>
					  </tr>
					  <c:forEach items="${truckLoadProcessForm.truckLoadProcess.originalProduct}" var="originalProduct" varStatus="status">
					  <tr>
					    <td>&nbsp;</td>
					    <td><c:out value="${originalProduct.sku}"/></td>
					    <td align="center" style="width:100px";><c:out value="${originalProduct.inventory}"/></td>
					    <c:set var="originalQty" value="${originalQty + originalProduct.inventory}" />
					    <td align="center" style="width:100px";><fmt:formatNumber value="${originalProduct.price1}" pattern="$#,###.00"/></td>
					    <td align="center" style="width:100px";><fmt:formatNumber value="${originalProduct.price2}" pattern="$#,###.00"/></td>
    				    <td align="center" style="width:100px";>${originalProduct.packing}</td>
					    <td align="center" style="width:100px";>${originalProduct.field15}</td> 
					    
					    
					  </tr>
					  </c:forEach>
					</table>
						
					</div>
				
					<div class="list${classIndex % 2}"><c:set var="classIndex" value="${classIndex+1}" />
						Item Created	
							</div>
					<div class="list${classIndex % 2}"><c:set var="classIndex" value="${classIndex+1}" />
		
					<table class="form" border="0" cellspacing="5" cellpadding="10" id="truckLoadProduced">
					  <tr>
					    <td><a onClick="truckLoadProduced()"><img src="../graphics/add.png" width="16" border="0"></a></td>
					    <td><div class="kitPartSku">SKU Produced</div></td>&nbsp;
					    <td><div class="kitPartSku">Product Description(Unique)</div></td>
					    <td align="center"><div class="kitPartSku"><fmt:message key="quantity" /></div></td>
					    <td align="center"><div class="kitPartSku">Unit Price</div></td>
					    <td align="center"><div class="kitPartSku"> Condition</div></td>
					    <td align="center"><div class="kitPartSku"># of Units</div></td>
					    <td align="center"><div class="kitPartSku">Ext MSRP</div></td>
						<td align="center" style="width:120px"><div class="kitPartSku">Print labels All </div>
 						<td align="center"><div class="kitPartSku" style="width:100px">Packing</div></td>
						<td align="center"><div class="kitPartSku" style="width:100px">#of Pallets</div></td> 
					   						    			    
					  </tr>
					  <c:if test="${empty truckLoadProcessForm.truckLoadProcess.derivedProduct}">
					  <tr>
					    <td>&nbsp;</td>
					    <td align="center"><input type="text" name="__truckLoadProduced_sku" id="addSkuP1" value="" class="textfield250" maxlength="50"></td>
					    <td ><input type="text" name="__truckLoadProduced_Desc" id="addDesc1" value="" class="textfield250" maxlength="200"></td>
					   <td align="center"><input type="text" name="__truckLoadProduced_qty" value="" class="textfield70" maxlength="30"></td>
					    <td align="center"><input type="text" name="__truckLoadProduced_unitPrice" value="" class="textfield70" maxlength="30"></td>
					    <td align="center"><input type="text" name="__truckLoadProduced_condition" value="" class="textfield70" maxlength="30"></td>
					    <td align="center"><input type="text" name="__truckLoadProduced_#units" value="" class="textfield70" maxlength="30"></td>
					    
					    <td align="center"><input type="text" name="__truckLoadProduced_msrp" value="" class="textfield70" maxlength="20"></td>
					     <td align="center"><input type="checkbox" onclick="toggleAll(this)"></td>
					    <td align="center"><input type="text" name="__truckLoadProduced_packing" id="addPacking1" value="" class="textfield70" maxlength="30"></td>				    
					   	<td align="center"><input type="text" name="__truckLoadProduced_numOfPallets" id="addNumOfPallets1" value="" class="textfield70" maxlength="30"></td>

					  </tr>
					  </c:if>
					  <c:forEach items="${truckLoadProcessForm.truckLoadProcess.derivedProduct}" var="derivedProduct" varStatus="status">
					  <tr>
					    <td align="center">&nbsp;</td>
					    <td style="width:120px"><c:out value="${derivedProduct.product.sku}"/></td>
					    <td style="width:250px"><c:out value="${derivedProduct.description}"/></td>
					    <td align="center" style="width:80px"><c:out value="${derivedProduct.quantity}"/>&nbsp;&nbsp;</td>
					    <td align="center" style="width:80px"><fmt:formatNumber value="${derivedProduct.unitPrice}" pattern="$#,###.00"/></td>
					    <td align="center" style="width:150px"><c:out value="${derivedProduct.condition}"/>&nbsp;&nbsp;</td>
					    <td align="center" style="width:80px"><c:out value="${derivedProduct.no_Of_Units}"/>&nbsp;&nbsp;</td>
					    <td align="center" style="width:100px"><fmt:formatNumber value="${derivedProduct.unitPrice * derivedProduct.quantity}" pattern="$#,###.00"/></td>
					    <td align="center" style="width:80px"><input type="checkbox" name="__selected_id" value="${derivedProduct.id}"></td>
   					    <td align="center" style="width:100px">${derivedProduct.packing}</td>
					    <td align="center" style="width:100px">${derivedProduct.numOfPallets}</td> 
			
					    
					  	<c:set var="producedQty" value="${producedQty + derivedProduct.quantity}" />
					  </tr>
					  </c:forEach>
					</table>
					</div>
					<!-- start tab -->
															  
					<div>
						<div class="list${classIndex % 2}"><c:set var="classIndex" value="${classIndex+1}" />
					<div class="listfl"><strong><fmt:message key="truckLoadCustom1" /> :</strong></div>
					<div class="listp">
						<form:input path="truckLoadProcess.custom1" cssClass="textfield" size="50" maxlength="120" htmlEscape="true"/>
					</div>	
					</div>
					<div class="list${classIndex % 2}"><c:set var="classIndex" value="${classIndex+1}" />
					<div class="listfl"><strong><fmt:message key="truckLoadCustom2" /> :</strong></div>
					<div class="listp">
						<form:input path="truckLoadProcess.custom2" cssClass="textfield" size="50" maxlength="120" htmlEscape="true"/>
					</div>	
					</div>
					<div class="list${classIndex % 2}"><c:set var="classIndex" value="${classIndex+1}" />
					<div class="listfl"><strong><fmt:message key="truckLoadCustom3" /> :</strong></div>
					<div class="listp">
						<form:input path="truckLoadProcess.custom3" size="50" cssClass="textfield" htmlEscape="true"/>
					   <img id="time_start_trigger4" class="calendarImage" src="../graphics/calendarIcon.jpg" alt="Pick a date" />			
					</div>	
					</div>
					<div class="list${classIndex % 2}"><c:set var="classIndex" value="${classIndex+1}" />
					<div class="listfl"><strong>PT :</strong></div>
					<div class="listp">
						<form:input path="truckLoadProcess.pt" cssClass="textfield" size="50" maxlength="120" htmlEscape="true"/>
					</div>	
					</div>							
					<div class="list${classIndex % 2}"><c:set var="classIndex" value="${classIndex+1}" />
					<div class="listfl"><strong><fmt:message key="truckLoadCustom4" /> :</strong></div>
					<div class="listp">
						<form:textarea path="truckLoadProcess.custom4" cssClass="textfield" rows="10" cols="45"/>
					</div>
					
  					<div class="list${classIndex % 2}"><c:set var="classIndex" value="${classIndex+1}" />
						<div class="listfl"><strong>Team Hours:</strong></div>
						<div class="listp">
							<form:input path="truckLoadProcess.teamHours" size="50" cssClass="textfield" htmlEscape="true"/>
						</div>		
					</div> 
 					<div class="list${classIndex % 2}"><c:set var="classIndex" value="${classIndex+1}" />
						<div class="listfl"><strong>Team Cost:</strong></div>
						<div class="listp">
							<form:input path="truckLoadProcess.teamCost" size="50" cssClass="textfield" htmlEscape="true"/>		
						</div>
					</div>
					<div class="list${classIndex % 2}"><c:set var="classIndex" value="${classIndex+1}" />
						<div class="listfl"><strong>#Team Members:</strong></div>
						<div class="listp">
							<form:input path="truckLoadProcess.numOfTeamMembers" size="50" cssClass="textfield" htmlEscape="true"/>		
						</div>
					</div>
					
					</div>
					<div class="list${classIndex % 2}"><c:set var="classIndex" value="${classIndex+1}" />
					<div class="listfl"><strong>Total ExtMsrp : </strong><fmt:message key="${siteConfig['CURRENCY'].value}" /> <fmt:formatNumber value="${truckLoadProcessForm.truckLoadProcess.extMsrp}" pattern="#,##0.00" /> </div>
					</div>
					<c:if test="${! truckLoadProcessForm.newTruckLoadProcess}">
						<div class="list${classIndex % 2}"><c:set var="classIndex" value="${classIndex+1}" />
						<div class="listfl"><strong>Total Original QTY : </strong><c:out value="${originalQty}"/></div>
						</div>
						<div class="list${classIndex % 2}"><c:set var="classIndex" value="${classIndex+1}" />
						<div class="listfl"><strong>Total Produced QTY : </strong><c:out value="${producedQty}"/></div>
						</div>
					</c:if>
					<div class="list${classIndex % 2}"><c:set var="classIndex" value="${classIndex+1}" />
					
					<div class="listfl"><strong>Print Labels : </strong>Enter qty</div><input type="text" name="labelQty" value="" size="10" maxlength="10">    
					<input type="submit" class="button" name="__PmPdf" value="Print"/></div>
			</div>
					<!-- start button --> 
					<div align="left" class="button"> 
					<c:choose>
					<c:when test="${truckLoadProcessForm.truckLoadProcess.status=='pr' or truckLoadProcessForm.newTruckLoadProcess}">
						<c:if test="${truckLoadProcessForm.newTruckLoadProcess}">
							<sec:authorize ifAnyGranted="ROLE_AEM,ROLE_ADMIN_${prop_site_id},ROLE_PROCESSING_CREATE">
								<input type="submit" value="<spring:message code="add"/>" name="add"/>
							</sec:authorize>  
						</c:if>
						<c:if test="${!truckLoadProcessForm.newTruckLoadProcess}">     
							<sec:authorize ifAnyGranted="ROLE_AEM,ROLE_ADMIN_${prop_site_id},ROLE_PROCESSING_UPDATE">
								<input type="submit" value="<spring:message code="update"/>" name="update"/>
							</sec:authorize>
							<sec:authorize ifAnyGranted="ROLE_AEM,ROLE_ADMIN_${prop_site_id},ROLE_PROCESSING_UPDATE">  
								<input type="submit" value="complete" name="complete" />
							</sec:authorize>  
							<sec:authorize ifAnyGranted="ROLE_AEM,ROLE_ADMIN_${prop_site_id},ROLE_PROCESSING_DELETE">  
								<input type="submit" value="cancel" name="cancel" />
							</sec:authorize>
						</c:if>
					</c:when>
					<c:otherwise>
						<sec:authorize ifAnyGranted="ROLE_AEM,ROLE_ADMIN_${prop_site_id},ROLE_PROCESSING_UPDATE">
							<input type="submit" value="<spring:message code="updatePT"/>" name="updatePT"/>
						</sec:authorize>
					</c:otherwise>	
					</c:choose>
					</div>
					<!-- end button -->
		  </td><td class="boxmidr" ></td>
		  </tr>
		  <tr><td class="botl"></td><td class="botr"></td></tr> 
		 
	
		</table>
					 
		</div>

		
	</form:form>
<script>
Calendar.setup({
    inputField     :    "truckLoadProcess.custom3",   // id of the input field
    showsTime      :    true,
    ifFormat       :    "%m/%d/%Y",   // format of the input field
    button         :    "time_start_trigger4" // trigger for the calendar (button ID)
});
</script>

<script>
Calendar.setup({
    inputField     :    "truckLoadProcess.dateStarted",   // id of the input field
    showsTime      :    true,
    ifFormat       :    "%m/%d/%Y",   // format of the input field
    button         :    "start_date_trigger" // trigger for the calendar (button ID)
});
</script>
	
<script>
Calendar.setup({
    inputField     :    "truckLoadProcess.dateEnded",   // id of the input field
    showsTime      :    true,
    ifFormat       :    "%m/%d/%Y",   // format of the input field
    button         :    "end_date_trigger" // trigger for the calendar (button ID)
});
</script>


	
	</sec:authorize>
	</tiles:putAttribute>
</tiles:insertDefinition>