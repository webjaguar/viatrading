<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/security/tags" prefix="sec" %>

<tiles:insertDefinition name="admin.catalog.truckLoadProcess" flush="true">
	<tiles:putAttribute name="content" type="string">
	
<sec:authorize ifAnyGranted="ROLE_AEM,ROLE_ADMIN_${prop_site_id},ROLE_PROCESSING_VIEW_LIST">
<form action="truckLoadProcessList.jhtm" method="post" id="list">
<input type="hidden" id="sort" name="sort" value="${truckLoadProcessSearch.sort}" />
<div id="mboxfull">

<!-- start table -->
  <table cellpadding="0" cellspacing="0" border="0" class="module" >
  <tr>
    <td class="topl_g">
	  
	  <!-- breadcrumb -->
	  <p class="breadcrumb">
	    <a href="../catalog"><fmt:message key="catalog" /></a> &gt;
	    <a href="../catalog/truckLoadProcessList.jhtm">Processing List</a>
	  </p>
	  
	  <!-- Error -->
	  
	  <!-- header image -->
	  <img class="headerImage" src="../graphics/category.gif" />
  
  </td><td class="topr_g" ></td></tr>
  <tr><td class="boxmidlrg" >
   
  <!-- tabs -->
  <div class="tab-wrapper">
  <h4 title="List of truckLoadProcesss"></h4>
  <div>
	<!-- start tab -->
	
	  	<div class="listdivi"></div>
	    
		  	<div class="listlight">
		  	<!-- input field -->
		  	
		  	<table border="0" cellpadding="0" cellspacing="1" width="100%">
				<tr>
					<td>&nbsp;
					</td>
					<c:if test="${model.truckLoadProcessList.nrOfElements > 0}">
						<td class="pageShowing">
							<fmt:message key="showing">
								<fmt:param value="${model.truckLoadProcessList.firstElementOnPage + 1}" />
								<fmt:param value="${model.truckLoadProcessList.lastElementOnPage + 1}" />
								<fmt:param value="${model.truckLoadProcessList.nrOfElements}" />
							</fmt:message>
						</td>
					</c:if>
					<td class="pageNavi">
						<fmt:message key="page" />
						<select name="page" id="page" onchange="submit()">
							<c:forEach begin="1" end="${model.truckLoadProcessList.pageCount}"
								var="page">
								<option value="${page}"
									<c:if test="${page == (model.truckLoadProcessList.page+1)}">selected</c:if>>
									${page}
								</option>
							</c:forEach>
						</select> 
						<fmt:message key="of" />
						<c:out value="${model.truckLoadProcessList.pageCount}" /> 
						|
						<c:if test="${model.truckLoadProcessList.firstPage}">
							<span class="pageNaviDead"><fmt:message key="previous" /></span>
						</c:if>
						<c:if test="${not model.truckLoadProcessList.firstPage}">
							<a
								href="<c:url value="truckLoadProcessList.jhtm"><c:param name="page" value="${model.truckLoadProcess.page}"/><c:param name="size" value="${model.truckLoadProcess.pageSize}"/></c:url>"
								class="pageNaviLink"><fmt:message key="previous" /></a>
						</c:if>
						|
						<c:if test="${model.truckLoadProcessList.lastPage}">
							<span class="pageNaviDead"><fmt:message key="next" /></span>
						</c:if>
						<c:if test="${not model.truckLoadProcessList.lastPage}">
							<a
								href="<c:url value="truckLoadProcessList.jhtm"><c:param name="page" value="${model.truckLoadProcess.page+2}"/><c:param name="size" value="${model.truckLoadProcess.pageSize}"/></c:url>"
								class="pageNaviLink"><fmt:message key="next" /></a>
						</c:if>
					</td>
				</tr>
			</table>
		  	
		  	
		  	<table border="0" cellpadding="2" cellspacing="0" width="100%" class="listings">
			  <tr class="listingsHdr2">
			    <td class="listingsHdr3">
			    	<table cellspacing="0" cellpadding="1">
			    	  <tr>
			    	    <c:choose>
			    	      <c:when test="${truckLoadProcessSearch.sort == 'id DESC'}">
			    	        <td class="listingsHdr3">
			    	        <a class="hdrLink" href="#" onClick="document.getElementById('sort').value='id';document.getElementById('list').submit()"><fmt:message key="truckLoadProcess" />ID</a>
			    	        </td>
			    	        <td><img src="../graphics/up.gif" border="0"></td>
			    	      </c:when>
			    	      <c:when test="${truckLoadProcessSearch.sort == 'id'}">
			    	        <td class="listingsHdr3">
			    	        <a class="hdrLink" href="#" onClick="document.getElementById('sort').value='id DESC';document.getElementById('list').submit()"><fmt:message key="truckLoadProcess" />ID</a>
			    	        </td>
			    	        <td><img src="../graphics/down.gif" border="0"></td>
			    	      </c:when>
			    	      <c:otherwise>
			    	        <td colspan="2" class="listingsHdr3">
			    	        <a class="hdrLink" href="#" onClick="document.getElementById('sort').value='id DESC';document.getElementById('list').submit()"><fmt:message key="truckLoadProcess" />ID</a>
			    	        </td>
			    	      </c:otherwise>
			    	    </c:choose>  
			    	  </tr>
			    	</table>
			      </td>
			    
			    
			      <td class="listingsHdr3">
			    	<table cellspacing="0" cellpadding="1">
			    	  <tr>
			    	    <c:choose>
			    	      <c:when test="${truckLoadProcessSearch.sort == 'name DESC'}">
			    	        <td class="listingsHdr3">
			    	        <a class="hdrLink" href="#" onClick="document.getElementById('sort').value='name';document.getElementById('list').submit()"><fmt:message key="truckLoadProcess" /></a>
			    	        </td>
			    	        <td><img src="../graphics/up.gif" border="0"></td>
			    	      </c:when>
			    	      <c:when test="${truckLoadProcessSearch.sort == 'name'}">
			    	        <td class="listingsHdr3">
			    	        <a class="hdrLink" href="#" onClick="document.getElementById('sort').value='name DESC';document.getElementById('list').submit()"><fmt:message key="truckLoadProcess" /></a>
			    	        </td>
			    	        <td><img src="../graphics/down.gif" border="0"></td>
			    	      </c:when>
			    	      <c:otherwise>
			    	        <td colspan="2" class="listingsHdr3">
			    	        <a class="hdrLink" href="#" onClick="document.getElementById('sort').value='name DESC';document.getElementById('list').submit()"><fmt:message key="truckLoadProcess" /></a>
			    	        </td>
			    	      </c:otherwise>
			    	    </c:choose>  
			    	  </tr>
			    	</table>
			      </td>
			       <td class="listingsHdr3">
			    	<table cellspacing="0" cellpadding="1">
			    	  <tr>
			    	    <c:choose>
			    	      <c:when test="${truckLoadProcessSearch.sort == 'date_created DESC'}">
			    	        <td class="listingsHdr3">
			    	        <a class="hdrLink" href="#" onClick="document.getElementById('sort').value='date_created';document.getElementById('list').submit()"><fmt:message key="dateAdded" /></a>
			    	        </td>
			    	        <td><img src="../graphics/up.gif" border="0"></td>
			    	      </c:when>
			    	      <c:when test="${truckLoadProcessSearch.sort == 'date_created'}">
			    	        <td class="listingsHdr3">
			    	        <a class="hdrLink" href="#" onClick="document.getElementById('sort').value='date_created DESC';document.getElementById('list').submit()"><fmt:message key="dateAdded" /></a>
			    	        </td>
			    	        <td><img src="../graphics/down.gif" border="0"></td>
			    	      </c:when>
			    	      <c:otherwise>
			    	        <td colspan="2" class="listingsHdr3">
			    	        <a class="hdrLink" href="#" onClick="document.getElementById('sort').value='date_created DESC';document.getElementById('list').submit()"><fmt:message key="dateAdded" /></a>
			    	        </td>
			    	      </c:otherwise>
			    	    </c:choose>  
			    	  </tr>
			    	</table>
			      </td>
			      
			    <td class="listingsHdr3"><fmt:message key="lastModified" /></td>
			    <td class="listingsHdr3"><fmt:message key="status" /></td>
			    <td class="listingsHdr3">PT</td>
			      <td class="listingsHdr3">
			    	<table cellspacing="0" cellpadding="1">
			    	  <tr>
			    	    <c:choose>
			    	      <c:when test="${truckLoadProcessSearch.sort == 'custom1 DESC'}">
			    	        <td class="listingsHdr3">
			    	        <a class="hdrLink" href="#" onClick="document.getElementById('sort').value='custom1';document.getElementById('list').submit()"><fmt:message key="truckLoadCustom1" /></a>
			    	        </td>
			    	        <td><img src="../graphics/up.gif" border="0"></td>
			    	      </c:when>
			    	      <c:when test="${truckLoadProcessSearch.sort == 'custom1'}">
			    	        <td class="listingsHdr3">
			    	        <a class="hdrLink" href="#" onClick="document.getElementById('sort').value='custom1 DESC';document.getElementById('list').submit()"><fmt:message key="truckLoadCustom1" /></a>
			    	        </td>
			    	        <td><img src="../graphics/down.gif" border="0"></td>
			    	      </c:when>
			    	      <c:otherwise>
			    	        <td colspan="2" class="listingsHdr3">
			    	        <a class="hdrLink" href="#" onClick="document.getElementById('sort').value='custom1 DESC';document.getElementById('list').submit()"><fmt:message key="truckLoadCustom1" /></a>
			    	        </td>
			    	      </c:otherwise>
			    	    </c:choose>  
			    	  </tr>
			    	</table>
			      </td>
			      
			      <td class="listingsHdr3">
			    	<table cellspacing="0" cellpadding="1">
			    	  <tr>
			    	    <c:choose>
			    	      <c:when test="${truckLoadProcessSearch.sort == 'custom2 DESC'}">
			    	        <td class="listingsHdr3">
			    	        <a class="hdrLink" href="#" onClick="document.getElementById('sort').value='custom2';document.getElementById('list').submit()"><fmt:message key="truckLoadCustom2" /></a>
			    	        </td>
			    	        <td><img src="../graphics/up.gif" border="0"></td>
			    	      </c:when>
			    	      <c:when test="${truckLoadProcessSearch.sort == 'custom2'}">
			    	        <td class="listingsHdr3">
			    	        <a class="hdrLink" href="#" onClick="document.getElementById('sort').value='custom2 DESC';document.getElementById('list').submit()"><fmt:message key="truckLoadCustom2" /></a>
			    	        </td>
			    	        <td><img src="../graphics/down.gif" border="0"></td>
			    	      </c:when>
			    	      <c:otherwise>
			    	        <td colspan="2" class="listingsHdr3">
			    	        <a class="hdrLink" href="#" onClick="document.getElementById('sort').value='custom2 DESC';document.getElementById('list').submit()"><fmt:message key="truckLoadCustom2" /></a>
			    	        </td>
			    	      </c:otherwise>
			    	    </c:choose>  
			    	  </tr>
			    	</table>
			      </td>
			      
			      <td class="listingsHdr3">
			    	<table cellspacing="0" cellpadding="1">
			    	  <tr>
			    	    <c:choose>
			    	      <c:when test="${truckLoadProcessSearch.sort == 'custom3 DESC'}">
			    	        <td class="listingsHdr3">
			    	        <a class="hdrLink" href="#" onClick="document.getElementById('sort').value='custom3';document.getElementById('list').submit()"><fmt:message key="truckLoadCustom3" /></a>
			    	        </td>
			    	        <td><img src="../graphics/up.gif" border="0"></td>
			    	      </c:when>
			    	      <c:when test="${truckLoadProcessSearch.sort == 'custom3'}">
			    	        <td class="listingsHdr3">
			    	        <a class="hdrLink" href="#" onClick="document.getElementById('sort').value='custom3 DESC';document.getElementById('list').submit()"><fmt:message key="truckLoadCustom3" /></a>
			    	        </td>
			    	        <td><img src="../graphics/down.gif" border="0"></td>
			    	      </c:when>
			    	      <c:otherwise>
			    	        <td colspan="2" class="listingsHdr3">
			    	        <a class="hdrLink" href="#" onClick="document.getElementById('sort').value='custom3 DESC';document.getElementById('list').submit()"><fmt:message key="truckLoadCustom3" /></a>
			    	        </td>
			    	      </c:otherwise>
			    	    </c:choose>  
			    	  </tr>
			    	</table>
			      </td>
			      
			      <td class="listingsHdr3">
			    	<table cellspacing="0" cellpadding="1">
			    	  <tr>
			    	    <c:choose>
			    	      <c:when test="${truckLoadProcessSearch.sort == 'custom4 DESC'}">
			    	        <td class="listingsHdr3">
			    	        <a class="hdrLink" href="#" onClick="document.getElementById('sort').value='custom4';document.getElementById('list').submit()"><fmt:message key="truckLoadCustom4" /></a>
			    	        </td>
			    	        <td><img src="../graphics/up.gif" border="0"></td>
			    	      </c:when>
			    	      <c:when test="${truckLoadProcessSearch.sort == 'custom4'}">
			    	        <td class="listingsHdr3">
			    	        <a class="hdrLink" href="#" onClick="document.getElementById('sort').value='custom4 DESC';document.getElementById('list').submit()"><fmt:message key="truckLoadCustom4" /></a>
			    	        </td>
			    	        <td><img src="../graphics/down.gif" border="0"></td>
			    	      </c:when>
			    	      <c:otherwise>
			    	        <td colspan="2" class="listingsHdr3">
			    	        <a class="hdrLink" href="#" onClick="document.getElementById('sort').value='custom4 DESC';document.getElementById('list').submit()"><fmt:message key="truckLoadCustom4" /></a>
			    	        </td>
			    	      </c:otherwise>
			    	    </c:choose>  
			    	  </tr>
			    	</table>
			      </td>
			      
			      <td class="listingsHdr3">Ext Msrp</td>
			      <td class="listingsHdr3">Created By</td>
			      
			  </tr>
		  	 <c:forEach items="${model.truckLoadProcessList.pageList}" var="truckLoadProcess" varStatus="status">
		  	  <tr>
			  	<td class="nameCol"><a href="../catalog/truckLoadProcess.jhtm?id=${truckLoadProcess.id}" class="nameLink"><c:out value="${truckLoadProcess.id}"/></a></td>
			    <td class="nameCol"><a href="../catalog/truckLoadProcess.jhtm?id=${truckLoadProcess.id}" class="nameLink"><c:out value="${truckLoadProcess.name}"/></a></td>
			    <td class="nameCol"><fmt:formatDate type="both" timeStyle="full" pattern="MM/dd/yyyy hh:mm a" value="${truckLoadProcess.startDate}"/></td>
			    <td class="nameCol"><fmt:formatDate type="both" timeStyle="full" pattern="MM/dd/yyyy hh:mm a" value="${truckLoadProcess.lastDate}"/></td>	
				<td class="nameCol"><fmt:message key="processStatus_${truckLoadProcess.status}" /></td>
				<td class="nameCol"><c:out value="${truckLoadProcess.pt}" /></td>	
				<td class="nameCol"><c:out value="${truckLoadProcess.custom1}" /></td>
				<td class="nameCol"><c:out value="${truckLoadProcess.custom2}" /></td>
				<td class="nameCol"><c:out value="${truckLoadProcess.custom3}" /></td>
				<td class="nameCol"><c:out value="${truckLoadProcess.custom4}" /></td>
				<td class="nameCol"><c:out value="${truckLoadProcess.extMsrp}" /></td>	
				<td class="nameCol"><c:out value="${truckLoadProcess.createdByName}" /></td>
			 </tr>
			</c:forEach>
			</table>
			
			<table border="0" cellpadding="0" cellspacing="1" width="100%">
			  <tr>
			  <td>&nbsp;</td>  
			  <td class="pageSize">
			  <select name="size" onchange="document.getElementById('page').value=1;submit()">
			    <c:forTokens items="10,25,50,100,200,500" delims="," var="current">
			  	  <option value="${current}" <c:if test="${current == truckLoadProcessSearch.pageSize}">selected</c:if>>${current} <fmt:message key="perPage" /></option>
			    </c:forTokens>
			  </select>
			  </td>
			  </tr>
			</table>	  
		    <!-- end input field -->  	  
		  	</div>
		  	
		  	<!-- start button -->
		 	<div align="left" class="button">
		  	  <sec:authorize ifAnyGranted="ROLE_AEM,ROLE_ADMIN_${prop_site_id},ROLE_PROCESSING_CREATE">
		  	    <input type="submit" name="__add" value="<fmt:message key="add" />">
		  	  </sec:authorize>
			</div> 
			<!-- end button -->	
       	  	
	<!-- end tab -->        
	</div>
  </div>  
  </td><td class="boxmidr" ></td></tr> 
  <tr><td class="botl"></td><td class="botr"></td></tr> 
  </table>
</div>
</form>
	</sec:authorize>
	</tiles:putAttribute>
</tiles:insertDefinition>