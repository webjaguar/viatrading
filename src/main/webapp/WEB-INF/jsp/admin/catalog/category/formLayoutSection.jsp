<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="/WEB-INF/tlds/spring.tld" prefix="spring" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ taglib uri="http://www.springframework.org/security/tags" prefix="sec" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<script type="text/javascript">
function showMobileCode(id){
	 document.getElementById(id).disabled=false;
    document.getElementById(id).style.display="block"; 
}	
</script>
<h4 title="Layout">Layout</h4>
	<div>
	<!-- start tab -->

	  	<div class="listdivi ln tabdivi"></div>
	  	<div class="listdivi"></div>
	  	<sec:authorize ifAnyGranted="ROLE_AEM,ROLE_ADMIN_${prop_site_id},ROLE_CATEGORY_EDIT,ROLE_CATEGORY_CREATE">
	  	
	  		<div class="list${classIndex % 2}"><c:set var="classIndex" value="${classIndex+1}" />
		  	<div class="listfl"><fmt:message key="productPerPage" />:</div>
		  	<div class="listp">
		  	<!-- input field -->
			  	<form:select path="category.productPerPage">
		          <form:option value=""><fmt:message key="global"/></form:option>
		          <c:forEach begin="1" end="50" var="products">
		          <form:option value="${products}">${products}</form:option>
		          </c:forEach>      
		        </form:select>
      			<form:errors path="category.productPerPage" cssClass="error" /> 
		    <!-- end input field -->	
		  	</div>
		  	</div>			  		
		  	
		  	<div class="list${classIndex % 2}"><c:set var="classIndex" value="${classIndex+1}" />
		  	<div class="listfl"><div class="helpImg"><img class="toolTipImg" title="<fmt:message key="layout" />::For Parent, if category doesn't have a parent then layout will be the default layout" src="../graphics/question.gif" /></div><fmt:message key="layout" />:</div>
		  	<div class="listp">
		  	<!-- input field -->
			    <form:select path="category.layoutId">
			      <form:option value="0"><fmt:message key="parent"/></form:option>
			      <form:option value="1"><fmt:message key="default"/></form:option>
			      <c:forEach items="${layoutList}" var="option">
			      <c:if test="${option.id > 1}">
			      <form:option value="${option.id}">${option.id} - <c:out value="${option.name}"/></form:option>
				  </c:if>
			      </c:forEach>      
			    </form:select>
			    <form:errors path="category.layoutId" cssClass="error" />  
	 		<!-- end input field -->	
	 		</div>
		  	</div>
		  	
		  	<div class="list${classIndex % 2}"><c:set var="classIndex" value="${classIndex+1}" />
		  	<div class="listfl"><fmt:message key="subcatLevels" />:</div>
		  	<div class="listp">
		  	<!-- input field -->
	   			<form:select path="category.subcatLevels">
        		<form:option value=""><fmt:message key="global"/></form:option>
        		<c:forEach begin="0" end="2" var="subcatLevels">
        		<form:option value="${subcatLevels}">${subcatLevels}</form:option>
        		</c:forEach> 
      			</form:select>
      			<form:errors path="category.subcatLevels" cssClass="error" />  
		    <!-- end input field -->	
		  	</div>
		  	</div>	
		  	
		  	<div class="list${classIndex % 2}"><c:set var="classIndex" value="${classIndex+1}" />
		  	<div class="listfl"><div class="helpImg"><img class="toolTipImg" title="NOTE::Applies if Sub Category Display is Row or Column" src="../graphics/question.gif" /></div><fmt:message key="subcatCols" />:</div>
		  	<div class="listp">
		  	<!-- input field -->
       			<form:select path="category.subcatCols">
        		<c:forEach begin="1" end="5" var="subcatCols">
        		<form:option value="${subcatCols}">${subcatCols}</form:option>
        		</c:forEach> 
       			</form:select>
       			<form:errors path="category.subcatCols" cssClass="error" />  
	 		<!-- end input field -->	
	 		</div>
		  	</div>	
		  	
		  	<div class="list${classIndex % 2}"><c:set var="classIndex" value="${classIndex+1}" />
		  	<div class="listfl"><fmt:message key="subcat" /> <fmt:message key="location" />:</div>
		  	<div class="listp">
		  	<!-- input field -->
       			<form:select path="category.subcatLocation">
        		<form:option value=""><fmt:message key="below" /> <fmt:message key="categoryHtml" /></form:option>
        		<form:option value="1"><fmt:message key="above" /> <fmt:message key="categoryHtml" /></form:option>
       			</form:select>
       			<form:errors path="category.subcatCols" cssClass="error" />  
		    <!-- end input field -->	
		  	</div>
		  	</div>	
		  	
		  	<div class="list${classIndex % 2}"><c:set var="classIndex" value="${classIndex+1}" />
		  	<div class="listfl"><fmt:message key="displayMode" />:</div>
		  	<div class="listp">
		  	<!-- input field -->
			    <form:select path="category.displayMode">
		          <form:option value=""><fmt:message key="global"/></form:option>
		          <c:choose>
		            <c:when test="${siteConfig['TEMPLATE'].value == 'template6'}">
		              <form:option value="22">22. Slider Layout</form:option>
			          <form:option value="28-R">28. ADI-R</form:option>
		          	  <form:option value="29-R">29. Compact-R</form:option>
		            </c:when>
		            <c:otherwise>
		              <form:option value="1">1. <fmt:message key="default" /></form:option>
			          <form:option value="2">2. <fmt:message key="default" /> 2</form:option>
			          <form:option value="3">3. compact up</form:option>
			          <form:option value="4">4. compact short Desc.</form:option>
			          <form:option value="5">5. compact down</form:option>
			          <form:option value="6">6. compact popup</form:option>
			          <form:option value="7">7. compact</form:option>
			          <form:option value="7-1">7-1. compact</form:option>
			          <form:option value="8">8. compact one add to cart</form:option>
			          <form:option value="8-1">8-1. compact one add to cart</form:option>
			          <form:option value="9">9. compact first price</form:option>
			          <form:option value="10">10. divs</form:option>
			          <form:option value="11">11. price break</form:option>
			          <form:option value="12">12. compact one add to cart (MSRP)</form:option>
			          <c:if test="${siteConfig['PRODUCT_QUICK_VIEW'].value == 'true'}">
			          <form:option value="13">13. compact down new</form:option>
			          <form:option value="14">14. compact down new - 14</form:option>
			          <c:if test="${gSiteConfig['gSITE_DOMAIN'] == 'viatrading.com' or gSiteConfig['gSITE_DOMAIN'] == 'test.com'}">
			          <form:option value="15">15. compact down new - 15</form:option>
			          </c:if>
			          <c:if test="${gSiteConfig['gSITE_DOMAIN'] == 'myevergreen.com'}">
			          <form:option value="16">16. compact down new - 16</form:option>
			          </c:if>
			          </c:if>
			          <form:option value="17">17. compact short Desc. One add to cart</form:option>
			          <c:if test="${siteConfig['PRODUCT_QUICK_VIEW'].value == 'true'}">
			          <form:option value="18">18. compact short Desc.</form:option>
			          </c:if>
			          <form:option value="19">19. One product per row with LongDesc</form:option>
			          <c:if test="${gSiteConfig['gSITE_DOMAIN'] == 'bnoticed.com'}">
			          <form:option value="20">20. Bnoticed Search Display</form:option>
			          </c:if>
			          <form:option value="21">21. Responsive Layout</form:option>
			          <form:option value="quick"><fmt:message key="quickMode"/></form:option>
			          <c:if test="${siteConfig['PRODUCT_QUICK_MODE_2'].value == 'true'}">
			          	<form:option value="quick2"><fmt:message key="quickMode2"/></form:option>
			          	<form:option value="quick2-1"><fmt:message key="quickMode2"/>-1</form:option>
			          	<form:option value="quick2-2"><fmt:message key="quickMode2"/>-2</form:option>
			          </c:if>
			          <form:option value="quick3"><fmt:message key="quickMode"/>3</form:option>
			          <form:option value="22">22. Slider Layout</form:option>
			          <form:option value="23">23. Fluid Layout</form:option>
			          <form:option value="24">24. Holden Layout</form:option>
			          <form:option value="25">25. Packnwood Layout</form:option>
			          <form:option value="26">26. MSConcepts Layout</form:option>
			          <form:option value="27">27. WholesaleGoodz</form:option>
			        </c:otherwise>
		          </c:choose>
		        </form:select>
      			<form:errors path="category.displayMode" cssClass="error" />  
	 		  <!-- end input field -->	
	 		  </div>
		  	</div>
		  	
		  	<c:if test="${siteConfig['TEMPLATE'].value != 'template6'}">
		    <div class="list${classIndex % 2}"><c:set var="classIndex" value="${classIndex+1}" />
		  	<div class="listfl"><fmt:message key="displayModeGrid" />:</div>
		  	<div class="listp">
		  	<!-- input field -->
			    <form:select path="category.displayModeGrid">
		          <form:option value=""><fmt:message key="global"/></form:option>
		          <form:option value="1">1. <fmt:message key="default" /></form:option>
		          <form:option value="2">2. <fmt:message key="default" /> 2</form:option>
		          <form:option value="3">3. compact up</form:option>
		          <form:option value="4">4. compact short Desc.</form:option>
		          <form:option value="5">5. compact down</form:option>
		          <form:option value="6">6. compact popup</form:option>
		          <form:option value="7">7. compact</form:option>
		          <form:option value="7-1">7-1. compact</form:option>
		          <form:option value="8">8. compact one add to cart</form:option>
		          <form:option value="8-1">8-1. compact one add to cart</form:option>
		          <form:option value="9">9. compact first price</form:option>
		          <form:option value="10">10. divs</form:option>
		          <form:option value="11">11. price break</form:option>
		          <form:option value="12">12. compact one add to cart (MSRP)</form:option>
		          <c:if test="${siteConfig['PRODUCT_QUICK_VIEW'].value == 'true'}">
		          <form:option value="13">13. compact down new</form:option>
		          <form:option value="14">14. compact down new - 14</form:option>
		          <c:if test="${gSiteConfig['gSITE_DOMAIN'] == 'viatrading.com' or gSiteConfig['gSITE_DOMAIN'] == 'test.com'}">
		          <form:option value="15">15. compact down new - 15</form:option>
		          </c:if>
		          <c:if test="${gSiteConfig['gSITE_DOMAIN'] == 'myevergreen.com'}">
		          <form:option value="16">16. compact down new - 16</form:option>
		          </c:if>
		          </c:if>
		          <form:option value="17">17. compact short Desc. One add to cart</form:option>
		          <c:if test="${siteConfig['PRODUCT_QUICK_VIEW'].value == 'true'}">
		          <form:option value="18">18. compact short Desc.</form:option>
		          </c:if>
		          <form:option value="19">19. One product per row with LongDesc</form:option>
		          <c:if test="${gSiteConfig['gSITE_DOMAIN'] == 'bnoticed.com'}">
		          <form:option value="20">20. Bnoticed Search Display</form:option>
		          </c:if>
		          <form:option value="quick"><fmt:message key="quickMode"/></form:option>
		          <c:if test="${siteConfig['PRODUCT_QUICK_MODE_2'].value == 'true'}">
		          	<form:option value="quick2"><fmt:message key="quickMode2"/></form:option>
		          	<form:option value="quick2-1"><fmt:message key="quickMode2"/>-1</form:option>
		          	<form:option value="quick2-2"><fmt:message key="quickMode2"/>-2</form:option>
		          </c:if>
		          <form:option value="quick3"><fmt:message key="quickMode"/>3</form:option>
		          <form:option value="23">23. Fluid Layout</form:option>
		          <form:option value="26">26. MSConcepts Layout</form:option>
		          <form:option value="27">27. WholesaleGoodz</form:option>
		        </form:select>
      			<form:errors path="category.displayModeGrid" cssClass="error" />  
	 		  <!-- end input field -->	
	 		  </div>
		  	</div>
		  	</c:if>
		  	
		  	<div class="list${classIndex % 2}"><c:set var="classIndex" value="${classIndex+1}" />
		  	<div class="listfl"><fmt:message key="LeftBar" />:</div>
		  	<div class="listp">
		  	<!-- input field -->
		  		<form:select path="category.leftbarType">
		  			<form:option value=""><fmt:message key="global"/></form:option>
			        <c:forTokens items="${siteConfig['LEFTBAR_TYPE'].value}" delims="," var="lType" varStatus="status">
			  	        <form:option value="${lType}"><fmt:message key="${lType}"/></form:option>
			  	    </c:forTokens>
		  	    </form:select>
		  	    <br />
	   			<form:checkbox path="category.hideLeftBar" /> <fmt:message key="hide" /> <br>
      			<form:checkbox path="category.showSubcats" /> <fmt:message key="showSubcats" /> 
		    <!-- end input field -->	
		  	</div>
		  	</div>
		  	
		  	<div class="list${classIndex % 2}"><c:set var="classIndex" value="${classIndex+1}" />
		  	<div class="listfl"><div class="helpImg"><img class="toolTipImg" title="NOTE::Applies if want to use full width" src="../graphics/question.gif" /></div><fmt:message key="showFullWidth" />:</div>
		  	<div class="listp">
		  	<!-- input field -->
	   			<fmt:message key="yes" /> <form:radiobutton path="category.showFullWidth" value="true" />
                <fmt:message key="no" /> <form:radiobutton path="category.showFullWidth" value="false" /> 
	 		<!-- end input field -->	
	 		</div>
		  	</div>
		  	
		  	<div class="list${classIndex % 2}"><c:set var="classIndex" value="${classIndex+1}" />
		  	<div class="listfl"><fmt:message key="hide" /><div class="helpImg"><img class="toolTipImg" title="NOTE::Applies if want to hide header" src="../graphics/question.gif" /></div><fmt:message key="Header" />:</div>
		  	<div class="listp">
		  	<!-- input field -->
	   			<fmt:message key="yes" /> <form:radiobutton path="category.hideHeader" value="true" />
                <fmt:message key="no" /> <form:radiobutton path="category.hideHeader" value="false" /> 
	 		<!-- end input field -->	
	 		</div>
		  	</div>	
		  	
		  	<div class="list${classIndex % 2}"><c:set var="classIndex" value="${classIndex+1}" />
		  	<div class="listfl"><fmt:message key="hide" /><div class="helpImg"><img class="toolTipImg" title="NOTE::Applies if want to hide TopBar" src="../graphics/question.gif" /></div> <fmt:message key="TopBar" />:</div>
		  	<div class="listp">
		  	<!-- input field -->
	            <fmt:message key="yes" /> <form:radiobutton path="category.hideTopBar" value="true" />
                <fmt:message key="no" /> <form:radiobutton path="category.hideTopBar" value="false" />
		    <!-- end input field -->	
		  	</div>
		  	</div>				  

            <div class="list${classIndex % 2}"><c:set var="classIndex" value="${classIndex+1}" />
		  	<div class="listfl"><fmt:message key="hide" /><div class="helpImg"><img class="toolTipImg" title="NOTE::Applies if want to hide RightBar" src="../graphics/question.gif" /></div><fmt:message key="RightBar" />:</div>
		  	<div class="listp">
		  	<!-- input field -->
	            <fmt:message key="yes" /> <form:radiobutton path="category.hideRightBar" value="true" />
                <fmt:message key="no" /> <form:radiobutton path="category.hideRightBar" value="false" />
	 		<!-- end input field -->	
	 		</div>
		  	</div>	
		  	
		  	<div class="list${classIndex % 2}"><c:set var="classIndex" value="${classIndex+1}" />
		  	<div class="listfl"><fmt:message key="hide" /><div class="helpImg"><img class="toolTipImg" title="NOTE::Applies if want to hide Footer" src="../graphics/question.gif" /></div> <fmt:message key="Footer" />:</div>
		  	<div class="listp">
		  	<!-- input field -->
	            <fmt:message key="yes" /> <form:radiobutton path="category.hideFooter" value="true" />
                <fmt:message key="no" /> <form:radiobutton path="category.hideFooter" value="false" />
		    <!-- end input field -->	
		  	</div>
		  	</div>	
		  	
		  	<div class="list${classIndex % 2}"><c:set var="classIndex" value="${classIndex+1}" />
		  	<div class="listfl"><fmt:message key="hide" /><div class="helpImg"><img class="toolTipImg" title="NOTE::Applies if want to hide BreadCrumbs" src="../graphics/question.gif" /></div><fmt:message key="breadCrumbs" />:</div>
		  	<div class="listp">
		  	<!-- input field -->
	            <fmt:message key="yes" /> <form:radiobutton path="category.hideBreadCrumbs" value="true" />
                <fmt:message key="no" /> <form:radiobutton path="category.hideBreadCrumbs" value="false" />
	 		<!-- end input field -->	
	 		</div>
		  	</div> 
		  	</sec:authorize>
		  	
		  	<sec:authorize ifAnyGranted="ROLE_AEM,ROLE_ADMIN_${prop_site_id},ROLE_CATEGORY_EDIT,ROLE_CATEGORY_CREATE,ROLE_CATEGORY_UPDATE_HEAD_TAG">
		  	<div class="list${classIndex % 2}"><c:set var="classIndex" value="${classIndex+1}" />
		  	<div class="listfl"><fmt:message key="headTag" />:</div>
		  	<div class="listp">
		  	<!-- input field -->
	            <form:textarea path="category.headTag" rows="15" cols="60" htmlEscape="true"/>
		    <!-- end input field -->
		    <div style="text-align:right" id="showMobileHtmlCode"><a href="javascript:showMobileCode('headTagMobile');">- <fmt:message key="showMobileCode" /> -</a></div>	
		  	</div>
		  	</div>
		  	
		  	<div class="list${classIndex % 2}" id="headTagMobile" style="display:none;"><c:set var="classIndex" value="${classIndex+1}" />
		  	<div class="listfl"><fmt:message key="headTag" /> <fmt:message key="mobile" />:</div>
		  	<div class="listp">
		  	<!-- input field -->
	            <form:textarea path="category.headTagMobile" rows="15" cols="60" htmlEscape="true"/>
		    <!-- end input field -->	
		  	</div>
		  	</div>	
		  	</sec:authorize>
	   
      
	<!-- end tab -->		
	</div>