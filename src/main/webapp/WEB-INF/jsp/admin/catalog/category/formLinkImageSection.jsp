<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="/WEB-INF/tlds/spring.tld" prefix="spring" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ taglib uri="http://www.springframework.org/security/tags" prefix="sec" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
    
    
    <h4 title="Category Link/Image">Category Link/Image</h4>
	<div>
	<!-- start tab -->

	  <div class="listdivi ln tabdivi"></div>
	  <div class="listdivi"></div>
	  <sec:authorize ifAnyGranted="ROLE_AEM,ROLE_ADMIN_${prop_site_id},ROLE_CATEGORY_EDIT">
	   
		    <div class="list${classIndex % 2}"><c:set var="classIndex" value="${classIndex+1}" />
		    <div class="listfl"><div class="helpImg"><img class="toolTipImg" title="Image::Upload the image of the product. Max Image size is 2MB." src="../graphics/question.gif" /></div><fmt:message key="type" />:</div>
		    <div class="listp">
		    <!-- input field -->
	          	<form:select path="category.linkType">     
	            <c:forEach items="${linkTypes}" var="linkType">
	            <form:option value="${linkType}"><fmt:message key="${linkType}" /></form:option>
	            </c:forEach> 
	            </form:select>
	            <form:errors path="category.linkType" cssClass="error" />      
            <!-- end input field -->
		    </div>
		    </div>
		  
		    <div class="list${classIndex % 2}"><c:set var="classIndex" value="${classIndex+1}" />
		  	<div class="listfl"><div class="helpImg"><img class="toolTipImg" title="Image::Upload the image of the product. Max Image size is 2MB." src="../graphics/question.gif" /></div><fmt:message key="linkImage" />:</div>
		  	<div class="listp">
		  	<!-- input field -->
			    <c:if test="${categoryForm.category.hasLinkImage}">
		    	<table class="form">
		    	  <tr>
		    	    <td>
		    		  <img src="<c:url value="/assets/Image/Category/catlink_${categoryForm.category.id}.gif"/>" border="0" width="200">
		    		</td>
		    	  </tr>
		    	  <tr>
		    	    <td class="error"><input name="__delete_linkImage" type="checkbox" <c:if test="${param.__delete_linkImage != null}">checked</c:if>> <fmt:message key="delete" /></td>
		    	  </tr>
		    	</table>
		    	</c:if>
		    	<c:if test="${not categoryForm.category.hasLinkImage}">
				<input value="browse" type="file" name="link_image"/>
		    	</c:if>
	 		<!-- end input field -->	
	 		</div>
		  	</div> 	
		  	
		  	<div class="list${classIndex % 2}"><c:set var="classIndex" value="${classIndex+1}" />
		    <div class="listfl"><div class="helpImg"><img class="toolTipImg" title="Image::Upload the image of the product. Max Image size is 2MB." src="../graphics/question.gif" /></div><fmt:message key="linkImage" /> (<fmt:message key="rollover" />):</div>
		    <div class="listp">
		    <!-- input field -->
			    <c:if test="${categoryForm.category.hasLinkImageOver}">
		    	<table class="form">
		    	  <tr>
		    	    <td>
		    		  <img src="<c:url value="/assets/Image/Category/catlink_over_${categoryForm.category.id}.gif"/>" border="0" width="200">
		    		</td>
		    	  </tr>
		    	  <tr>
		    	    <td class="error"><input name="__delete_linkImageOver" type="checkbox" <c:if test="${param.__delete_linkImageOver != null}">checked</c:if>> <fmt:message key="delete" /></td>
		    	  </tr>
		    	</table>
		    	</c:if>
		    	<c:if test="${not categoryForm.category.hasLinkImageOver}">
				<input value="browse" type="file" name="link_image_over"/>
		    	</c:if>   
            <!-- end input field -->
		    </div>
		    </div>	
		    
		    <div class="list${classIndex % 2}"><c:set var="classIndex" value="${classIndex+1}" />
		  	<div class="listfl"><div class="helpImg"><img class="toolTipImg" title="Image::Upload the image of the product. Max Image size is 2MB." src="../graphics/question.gif" /></div><fmt:message key="thumbnailViewImage" />:</div>
		  	<div class="listp">
		  	<!-- input field -->
			    <c:if test="${categoryForm.category.hasImage or (not empty (categoryForm.category.imageUrl) and categoryForm.category.imageUrl != null)}">
		    	<table class="form">
		    	  <tr>
		    	    <td>
		    	    <c:choose>
		    	    <c:when test="${ not empty (categoryForm.category.imageUrl) and categoryForm.category.imageUrl != null}">
		    	    	<c:choose>
		    	    	<c:when test="${categoryForm.category.absolute}">
		    	    		<img src="<c:url value="${categoryForm.category.imageUrl}"/>" border="0" width="200">
		    	    	</c:when>
		    	    	<c:otherwise>
		    	    		<img src="<c:url value="/assets/Image/Category/${categoryForm.category.imageUrl}"/>" border="0" width="200">
		    	    	</c:otherwise>
		    	    	</c:choose>
		    	    </c:when>
		    	    <c:otherwise>
		    		  	<img src="<c:url value="/assets/Image/Category/cat_${categoryForm.category.id}.gif"/>" border="0" width="200">
		    		</c:otherwise>
		    		</c:choose>
		    		</td>
		    	  </tr>
		    	  <tr>
		    	    <td class="error"><input name="__delete_image" type="checkbox" <c:if test="${param.__delete_image != null}">checked</c:if>> <fmt:message key="delete" /></td>
		    	  </tr>
		    	</table>
		    	</c:if>
		    	<c:if test="${(not (categoryForm.category.hasImage)) and (empty (categoryForm.category.imageUrl))}">
					<input value="browse" type="file" name="_image"/>
				</c:if>	
				<!-- end input field -->	
	 		</div>
	 		<br>
	 		<div class="listfl"><div class="helpImg"><img class="toolTipImg" title="Image Name::The Maximum character is 255." src="../graphics/question.gif" /></div><fmt:message key="thumbnailViewImage" /> <fmt:message key="name" />:</div>
	 		<br>
	 		<div class="listp">
				<form:input  path="category.imageUrlAltName" size="20" />
			<input type="checkbox" <c:if test="${categoryForm.category.hasImage || (not empty (categoryForm.category.imageUrl))}">disabled</c:if> <c:if test="${not empty (categoryForm.category.imageUrl) and categoryForm.category.imageUrl != null}">checked="checked" </c:if> name="_imageWithName" /> <fmt:message key="imageWithName"/>
			<span class="helpNote">
				<p>To change the old CAT_ID image name to more SEO friendly name, please delete the image if exisitng and upload it with the checbox selected.</p>
			</span>
			</div>
	 		</div> 	
		      
		    <div class="list${classIndex % 2}"><c:set var="classIndex" value="${classIndex+1}" />
		    <div class="listfl"><div class="helpImg"><img class="toolTipImg" title="Image::Upload the image of the product. Max Image size is 2MB." src="../graphics/question.gif" /></div><fmt:message key="detailViewImage" />:</div>
		    <div class="listp">
		    <!-- input field -->
			    <c:if test="${imageBigExist}">
		    	<table class="form">
		    	  <tr>
		    	    <td>
		    		  <img src="<c:url value="/assets/Image/Category/cat_${categoryForm.category.id}_big.gif"/>" border="0" width="200">
		    		</td>
		    	  </tr>
		    	  <tr>
		    	    <td class="error"><input name="__delete_image_big" type="checkbox" <c:if test="${param.__delete_image_big != null}">checked</c:if>> <fmt:message key="delete" /></td>
		    	  </tr>
		    	</table>
		    	</c:if>
		    	<c:if test="${not  imageBigExist}">
				<input value="browse" type="file" name="_image_big"/>
				</c:if>
            <!-- end input field -->
		    </div>
		    </div>	
	</sec:authorize>	      
	<!-- end tab -->		
	</div>	