<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="/WEB-INF/tlds/spring.tld" prefix="spring" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ taglib uri="http://www.springframework.org/security/tags" prefix="sec" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>

<c:if test="${gSiteConfig['gPROTECTED'] > 0}">
<script language="JavaScript">
<!--
function toggleProtectedHtmlCode(el) {
  if (el.value == "0") {
      document.getElementById('protectedHtmlCode').style.display="none";
      <c:forEach items="${languageCodes}" var="i18n">
      document.getElementById('protectedHtmlCode_${i18n}').style.display="none";
      </c:forEach>
  } else {
      document.getElementById('protectedHtmlCode').style.display="block";
      <c:forEach items="${languageCodes}" var="i18n">
      document.getElementById('protectedHtmlCode_${i18n}').style.display="block";
      </c:forEach>      
  }
}
//-->
</script>
</c:if>

	<h4 title="Options">Options</h4>
	<div>
	<!-- start tab -->

	  	<div class="listdivi ln tabdivi"></div>
	  	<div class="listdivi"></div>
	   <sec:authorize ifAnyGranted="ROLE_AEM,ROLE_ADMIN_${prop_site_id},ROLE_CATEGORY_EDIT">
	   
		  	<div class="list${classIndex % 2}"><c:set var="classIndex" value="${classIndex+1}" />
		  	<div class="listfl"><fmt:message key="homePage" />:</div>
		  	<div class="listp">
		  	<c:choose>
		  	<c:when test="${categoryForm.category.host == null}">
		  	<!-- input field -->
	            <form:select path="category.homePage">
                <form:option value="false"><fmt:message key="no"/></form:option>
                <form:option value="true"><fmt:message key="yes"/></form:option>
                </form:select>
                <form:errors path="category.homePage" cssClass="error" />   
          	<!-- end input field -->
          	</c:when>
          	<c:otherwise><c:out value="${categoryForm.category.host}"/></c:otherwise>
          	</c:choose>
		  	</div>
		  	</div>	
		  	
		  	<c:if test="${gSiteConfig['gPROTECTED'] > 0}">
		  	<div class="list${classIndex % 2}"><c:set var="classIndex" value="${classIndex+1}" />
		  	<div class="listfl"><fmt:message key="protectedLevel" />:</div>

		  	<div class="listp">
		  	<!-- input field -->
	            <form:select path="category.protectedLevel" onchange="toggleProtectedHtmlCode(this)">
                <form:option value="0"><fmt:message key="none" /></form:option>      
                  <c:forEach begin="1" end="${gSiteConfig['gPROTECTED']}" var="protectedL">
			    	<c:set var="key" value="protected${protectedL}"/>
			    	<c:choose>
			      	  <c:when test="${labels[key] != null and labels[key] != ''}">
			        	<c:set var="label" value="${labels[key]}"/>
			      	  </c:when>
			      	  <c:otherwise><c:set var="label" value="${protectedL}"/></c:otherwise>
			    	</c:choose>		        
		        	<form:option value="${protectedLevels[protectedL-1]}"><c:out value="${label}"/></form:option>
		      	  </c:forEach>
		    </form:select>
                <form:errors path="category.protectedLevel" cssClass="error" />   
	 		<!-- end input field -->	
	 		</div>
		  	</div> 
		  	</c:if>
		  		  
		  	<div class="list${classIndex % 2}"><c:set var="classIndex" value="${classIndex+1}" />
		  	<div class="listfl"><div class="helpImg"><img class="toolTipImg" title="NOTE::Hide this category on search page." src="../graphics/question.gif" /></div><fmt:message key="showOnSearch" />:</div>
		  	<div class="listp">
		  	<!-- input field -->
		  	    <form:checkbox path="category.showOnSearch" />
            </div>
            </div>     
		  		  
    </sec:authorize>
	<!-- end tab -->		
	</div>
    