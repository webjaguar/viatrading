<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="/WEB-INF/tlds/spring.tld" prefix="spring" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ taglib uri="http://www.springframework.org/security/tags" prefix="sec" %>

<tiles:insertDefinition name="admin.catalog.category" flush="true">
  <tiles:putAttribute name="content" type="string">

<c:choose>
 <c:when test="${siteConfig['TINYMCE'].value == 'true'}">
 <script type="text/javascript" src="<c:url value="/tinymce/jscripts/tiny_mce/tiny_mce.js"/>"></script>
<script type="text/javascript">
function loadEditor(el) {
	tinyMCE.init({
        // General options
        mode : "textareas",
        theme : "advanced",
        editor_selector : el,
        plugins : "safari,spellchecker,pagebreak,style,layer,table,save,advhr,advimage,advlink,emotions,iespell,inlinepopups,insertdatetime,preview,media,searchreplace,print,contextmenu,paste,directionality,fullscreen,noneditable,visualchars,nonbreaking,xhtmlxtras,template,imagemanager",

        // Theme options
        theme_advanced_buttons1 : "bold,italic,underline,strikethrough,|,justifyleft,justifycenter,justifyright,justifyfull,|,styleselect,formatselect,fontselect,fontsizeselect",
        theme_advanced_buttons2 : "cut,copy,paste,|,bullist,numlist,|,undo,redo,|,link,unlink,anchor,cleanup,code,|mpreview,|,forecolor,backcolor,|,removeformat,fullscreen",
        theme_advanced_buttons3 : "",
        theme_advanced_buttons4 : "",
        theme_advanced_toolbar_location : "top",
        theme_advanced_toolbar_align : "left",
        theme_advanced_statusbar_location : "bottom",
        theme_advanced_resizing : true,

        // Example content CSS (should be your site CSS)
        content_css : "css/example.css",

        // Drop lists for link/image/media/template dialogs
        template_external_list_url : "js/template_list.js",
        external_link_list_url : "js/link_list.js",
        external_image_list_url : "js/image_list.js",
        media_external_list_url : "js/media_list.js",

        imagemanager_rootpath : "${prop_site_root}"
	});
}
</script>
 </c:when>
 <c:otherwise>
 <script type="text/javascript" src="<c:url value="/FCKeditor/fckeditor.js"/>"></script>
 <script type="text/javascript">
 function loadEditor(el) {
  var oFCKeditor = new FCKeditor(el);
  oFCKeditor.BasePath = "<c:url value="/FCKeditor/"/>";
  oFCKeditor.Config["CustomConfigurationsPath"] = '<c:url value="/admin/javascript/fckconfig.js"/>';
  oFCKeditor.ToolbarSet = 'Html';
  oFCKeditor.Width = 650;
  oFCKeditor.Height = 400;
  oFCKeditor.ReplaceTextarea();
  document.getElementById(el + '_link').style.display="none";
 }
 </script>
 </c:otherwise>
</c:choose>
<c:if test="${siteConfig['CATEGORY_INTERSECTION'].value == 'true'}">
<link rel="stylesheet" type="text/css" media="all" href="../../admin/javascript/jscalendar-1.0/skins/aqua/theme.css" title="Aqua" /> <!-- calendar stylesheet -->
<script type="text/javascript" src="../../admin/javascript/jscalendar-1.0/calendar.js"></script> <!-- main calendar program -->
<script type="text/javascript" src="../../admin/javascript/jscalendar-1.0/lang/calendar-en.js"></script>
<script type="text/javascript" src="../../admin/javascript/jscalendar-1.0/calendar-setup.js"></script>
</c:if>

<script language="JavaScript"> 
<!--
window.addEvent('domready', function(){			
	var tabs1 = new SimpleTabs($('tab-block-1'), { selector: 'h4'});  
	var Tips1 = new Tips($$('.toolTipImg'));
	<c:if test="${gSiteConfig['gPRODUCT_FIELD_SEARCH']}">
	if ($('productFieldSearch_id').checked == false ) {
		document.getElementById('productFieldSearchType').style.display="none";
		document.getElementById('productFieldSearchPosition').style.display="none";
	}else if($('productFieldSearchType_id').value == '2' || ($('productFieldSearchType_id').value == '0' &&  ${siteConfig['PRODUCT_FIELDS_SEARCH_TYPE'].value == '2'})) {
		document.getElementById('productFieldSearchPosition').style.display="block";
	} else {
		document.getElementById('productFieldSearchPosition').style.display="none";
	}
	</c:if>
});
<c:if test="${gSiteConfig['gPRODUCT_FIELD_SEARCH']}" >
function showProductFieldSearchType() 
{
	if ($('productFieldSearch_id').checked == true) {
		document.getElementById('productFieldSearchType').style.display="block";
		if($('productFieldSearchType_id').value == '2' || ( $('productFieldSearchType_id').value == '0' &&  ${siteConfig['PRODUCT_FIELDS_SEARCH_TYPE'].value == '2'})) {
			document.getElementById('productFieldSearchPosition').style.display="block";
		}
	} else {
		document.getElementById('productFieldSearchType').style.display="none";
		document.getElementById('productFieldSearchPosition').style.display="none";
	}
}
function showProductFieldSearchPosition() 
{
	if (($('productFieldSearch_id').checked == true && $('productFieldSearchType_id').value == '2') || ($('productFieldSearchType_id').value == '0' && ${siteConfig['PRODUCT_FIELDS_SEARCH_TYPE'].value == '2'})) {
		document.getElementById('productFieldSearchPosition').style.display="block";
	} else {
		document.getElementById('productFieldSearchPosition').style.display="none";
	}
}
</c:if>

function hideDate() {
  if (document.getElementById('sortBySelect').value == "viewed") {
      document.getElementById('startDate').style.display="none";
      document.getElementById('endDate').style.display="none";
  } else {
      document.getElementById('startDate').style.display="block";
      document.getElementById('endDate').style.display="block";
  }
}
//--> 
</script>

<form:form commandName="categoryForm" method="post" enctype="multipart/form-data" name="myform">
<form:hidden path="newCategory" />
<c:if test="${categoryForm.category.rank != null}">
<form:hidden path="category.rank" />
</c:if>
<!-- main box -->
  <div id="mboxfull">
  
  <!-- start table -->
  <table cellpadding="0" cellspacing="0" border="0" class="module" >
  <tr>
    <td class="topl_g">
	  
	  <!-- breadcrumb -->
	  <p class="breadcrumb">
	    <a href="../catalog">Catalog</a> &gt;
	    <a href="../catalog/categoryList.jhtm">Categories</a>  &gt;
	    ${categoryForm.category.name}
	  </p>
	  
	  <!-- Error Message -->
  	  <spring:hasBindErrors name="categoryForm">
	    <div class="message">Please fix all errors!</div>
	  </spring:hasBindErrors>
	  
  <sec:authorize ifAnyGranted="ROLE_AEM,ROLE_ADMIN_${prop_site_id},ROLE_CATEGORY_EDIT,ROLE_CATEGORY_CREATE">
  <div align="right">	
    <table class="form">
		<c:if test="${!categoryForm.newCategory}">
		  <c:if test="${categoryForm.category.host == null}">
		  <tr>
		    <td align="right"><fmt:message key="categoryParent" /> ID : </td>
		    <td>
			  <form:input path="category.parent" size="5" cssClass="textfield50" /> 
			  <form:errors path="category.parent" cssClass="error" />
		    </td>
		  </tr>
		  </c:if>
		  <tr>
		    <td align="right"><fmt:message key="categoryId" /> : </td>
		    <td>
		      <c:out value="${categoryForm.category.id}"/>
			  <form:hidden path="category.id" />
		    </td>
		  </tr>
		  <tr>
		    <td align="right"><fmt:message key="dateAdded" /> : </td>
		    <td><fmt:formatDate type="both" timeStyle="full" value="${categoryForm.category.created}"/></td>
		  </tr>
		  <tr>
		    <td align="right"><fmt:message key="lastModified" /> : </td>
		    <td><fmt:formatDate type="both" timeStyle="full" value="${categoryForm.category.lastModified}"/></td>
		  </tr>
		  <tr>
		    <td align="right"><fmt:message key="numOfClicksFull" /> : </td>
		    <td><c:out value="${categoryForm.category.numOfClicks}"/></td>
		  </tr>
		</c:if>
	</table>
  </div>
  </sec:authorize>
  
  </td><td class="topr_g" ></td></tr>
  <tr><td class="boxmidlrg" >

 <!-- tabs -->
 <div id="tab-block-1">
	<h4 title="Category Info">Category Info</h4>
	<div>
	<!-- start tab -->
         <c:set var="classIndex" value="0" />
         
	  	<div class="listdivi ln tabdivi"></div>
	  	<div class="listdivi"></div>
	    <sec:authorize ifAnyGranted="ROLE_AEM,ROLE_ADMIN_${prop_site_id},ROLE_CATEGORY_EDIT,ROLE_CATEGORY_CREATE">
	   
		  	<div class="list${classIndex % 2}"><c:set var="classIndex" value="${classIndex+1}" />
		  	<div class="listfl"><div class="requiredField"><div class="helpImg"><img class="toolTipImg" title="Name::The Maximum character is 150.For example: Test_Example." src="../graphics/question.gif" /></div><fmt:message key="categoryName" />:</div></div>
		  	<div class="listp">
		  	<!-- input field -->
			  	<form:input path="category.name" maxlength="150" size="50" cssClass="textfield" htmlEscape="true" />
		       	<form:errors path="category.name" cssClass="error" />
		       	<div class="helpNote">
			     
			      <c:if test="${gSiteConfig['gMOD_REWRITE'] == '1'}">
			      <p> The name will exist on URL so that the special(For instance "/","%","@","&") characters are not recommended.</p>
			      </c:if>
			    </div>
		    <!-- end input field -->   	
		  	</div>
		  	</div>
		    
		  	<div class="list${classIndex % 2}"><c:set var="classIndex" value="${classIndex+1}" />
		  	<div class="listfl"><div class="helpImg"><img class="toolTipImg" title="Redirected URL::Url link of the new Category webpage(The maximum character is 255).For example: Http://www.google.com/webhp" src="../graphics/question.gif" /></div><fmt:message key="categoryUrl" />:</div>
		  	<div class="listp">
		  	<!-- input field -->
			  	<form:input path="category.url" maxlength="255" size="50" cssClass="textfield" htmlEscape="true" />
		        <form:errors path="category.url" cssClass="error" />
		        <form:select path="category.urlTarget" cssClass="second">
		          <form:option value="" label="Same window"/>
		          <form:option value="_blank" label="New window"/>
		        </form:select>
		      	<form:errors path="category.urlTarget" cssClass="error" /> 
		      	<form:checkbox path="category.redirect301" /> 301 Moved Permanently
		      	<div class="helpNote">
		      	 
			      <p> The special(For instance "/","%","@","&") characters are not recommended.</p>
			      
			    </div>
		    <!-- end input field -->  	  
		  	</div>
		  	</div>
		  	
		  	<!-- Spanish Urls -->
		  	<div class="list${classIndex % 2}"><c:set var="classIndex" value="${classIndex+1}" />
		  	<div class="listfl"><div class="helpImg"><img class="toolTipImg" title="Redirected URL::Url link of the new Spanish Category webpage(The maximum character is 255).For example: Http://www.google.com/webhp" src="../graphics/question.gif" /></div><fmt:message key="categoryUrl" /> Spanish:</div>
		  	<div class="listp">
		  	<!-- input field -->
			  	<form:input path="category.urlEs" maxlength="255" size="50" cssClass="textfield" htmlEscape="true" />
		        <form:errors path="category.urlEs" cssClass="error" />		      
		      	<div class="helpNote">	      	 
			      <p> The special(For instance "/","%","@","&") characters are not recommended.</p>		      
			    </div>
		    <!-- end input field -->  	  
		  	</div>
		  	</div>
		  
		  	<div class="list${classIndex % 2}"><c:set var="classIndex" value="${classIndex+1}" />
		  	<div class="listfl"><div class="helpImg"><img class="toolTipImg" title="HTML Code::Enter here your HTML code for category web page.And you can use Html Editor to make it easy." src="../graphics/question.gif" /></div><fmt:message key="categoryHtml" />:</div>
		  	<div class="listp">
		  	<!-- input field -->
		  		<table border="0" cellpadding="0" cellspacing="0"><tr><td>
      	  		<form:textarea rows="15" cols="60" path="category.htmlCode" cssClass="textfield category.htmlCode" htmlEscape="true" />
      	  		<form:errors path="category.htmlCode" cssClass="error" /> 
	  	  		<div style="text-align:right" id="category.htmlCode_link"><a href="javascript:loadEditor('category.htmlCode');" class="htmleditor">- <fmt:message key="htmlEditor" /> -</a></div>
	  	  		</td></tr></table>
		  	<!-- end input field -->	
		  	</div>
		  	</div>	
		  	     
		  	<c:if test="${gSiteConfig['gPROTECTED'] > 0}">
		  	<div class="list${classIndex % 2}" id="protectedHtmlCode" <c:if test="${categoryForm.category.protectedLevel == 0}">style="display:none"</c:if>><c:set var="classIndex" value="${classIndex+1}" />
		  	<div class="listfl"><fmt:message key="protectedHtmlCode" />:</div>
		  	<div class="listp">
		  	<!-- input field -->
		  	    <table border="0" cellpadding="0" cellspacing="0"><tr><td>
          		<form:textarea path="category.protectedHtmlCode" rows="15" cols="60"  cssClass="textfield category.protectedHtmlCode" htmlEscape="true" />
	      		<form:errors path="category.protectedHtmlCode" cssClass="error" />
	      		<div style="text-align:right" id="category.protectedHtmlCode_link"><a href="javascript:loadEditor('category.protectedHtmlCode')" class="htmleditor">- <fmt:message key="htmlEditor" /> -</a></div>
	      		</td></tr></table>
	 		<!-- end input field -->	
	 		</div>
		  	</div>			
     		</c:if>

		  	<div class="list${classIndex % 2}"><c:set var="classIndex" value="${classIndex+1}" />
		  	<div class="listfl"><div class="helpImg"><img class="toolTipImg" title="Footer::Enter the HTML Code for custom footer." src="../graphics/question.gif" /></div><fmt:message key="Footer" /> <fmt:message key="categoryHtml" />:</div>
		  	<div class="listp">
		  	<!-- input field -->
		  		<table border="0" cellpadding="0" cellspacing="0"><tr><td>
      	  		<form:textarea rows="15" cols="60" path="category.footerHtmlCode" cssClass="textfield category.footerHtmlCode" htmlEscape="true" />
      	  		<form:errors path="category.footerHtmlCode" cssClass="error" /> 
	  	  		<div style="text-align:right" id="category.footerHtmlCode_link"><a href="javascript:loadEditor('category.footerHtmlCode')" class="htmleditor">- <fmt:message key="htmlEditor" /> -</a></div>
	  	  		</td></tr></table>
		  	<!-- end input field -->	
		  	</div>
		  	</div>

		  	<div class="list${classIndex % 2}"><c:set var="classIndex" value="${classIndex+1}" />
		  	<div class="listfl"><div class="helpImg"><img class="toolTipImg" title="Right Bar(top):: Enter the HTML Code for custom Right Bar(top)." src="../graphics/question.gif" /></div><fmt:message key="RightBarTop" /> <fmt:message key="categoryHtml" />:</div>
		  	<div class="listp">
		  	<!-- input field -->
		  		<table border="0" cellpadding="0" cellspacing="0"><tr><td>
      	  		<form:textarea rows="15" cols="60" path="category.rightBarTopHtmlCode" cssClass="textfield category.rightBarTopHtmlCode" htmlEscape="true" />
      	  		<form:errors path="category.rightBarTopHtmlCode" cssClass="error" /> 
	  	  		<div style="text-align:right" id="category.rightBarTopHtmlCode_link"><a href="javascript:loadEditor('category.rightBarTopHtmlCode')" class="htmleditor">- <fmt:message key="htmlEditor" /> -</a></div>
	  	  		</td></tr></table>
		  	<!-- end input field -->	
		  	</div>
		  	</div>

		  	<div class="list${classIndex % 2}"><c:set var="classIndex" value="${classIndex+1}" />
		  	<div class="listfl"><div class="helpImg"><img class="toolTipImg" title="Right Bar(bottom):: Enter the HTML Code for custom Right Bar(bottom)." src="../graphics/question.gif" /></div><fmt:message key="RightBarBottom" /> <fmt:message key="categoryHtml" />:</div>
		  	<div class="listp">
		  	<!-- input field -->
		  		<table border="0" cellpadding="0" cellspacing="0"><tr><td>
      	  		<form:textarea rows="15" cols="60" path="category.rightBarBottomHtmlCode" cssClass="textfield category.rightBarBottomHtmlCode" htmlEscape="true" />
      	  		<form:errors path="category.rightBarBottomHtmlCode" cssClass="error" /> 
	  	  		<div style="text-align:right" id="category.rightBarBottomHtmlCode_link"><a href="javascript:loadEditor('category.rightBarBottomHtmlCode')" class="htmleditor">- <fmt:message key="htmlEditor" /> -</a></div>
	  	  		</td></tr></table>
		  	<!-- end input field -->	
		  	</div>
		  	</div>

			<div class="list${classIndex % 2}"><c:set var="classIndex" value="${classIndex+1}" />
		  	<div class="listfl"><div class="helpImg"><img class="toolTipImg" title="Left Bar(top):: Enter the HTML Code for custom Left Bar(top)." src="../graphics/question.gif" /></div><fmt:message key="LeftBarTop" /> <fmt:message key="categoryHtml" />:</div>
		  	<div class="listp">
		  	<!-- input field -->
		  		<table border="0" cellpadding="0" cellspacing="0"><tr><td>
      	  		<form:textarea rows="15" cols="60" path="category.leftBarTopHtmlCode" cssClass="textfield category.leftBarTopHtmlCode" htmlEscape="true" />
      	  		<form:errors path="category.leftBarTopHtmlCode" cssClass="error" /> 
	  	  		<div style="text-align:right" id="category.leftBarTopHtmlCode_link"><a href="javascript:loadEditor('category.leftBarTopHtmlCode')" class="htmleditor">- <fmt:message key="htmlEditor" /> -</a></div>
	  	  		</td></tr></table>
		  	<!-- end input field -->	
		  	</div>
		  	</div>

			<div class="list${classIndex % 2}"><c:set var="classIndex" value="${classIndex+1}" />
		  	<div class="listfl"><div class="helpImg"><img class="toolTipImg" title="Left Bar(bottom):: Enter the HTML Code for custom Left Bar(bottom)." src="../graphics/question.gif" /></div><fmt:message key="LeftBarBottom" /> <fmt:message key="categoryHtml" />:</div>
		  	<div class="listp">
		  	<!-- input field -->
		  		<table border="0" cellpadding="0" cellspacing="0"><tr><td>
      	  		<form:textarea rows="15" cols="60" path="category.leftBarBottomHtmlCode" cssClass="textfield category.leftBarBottomHtmlCode" htmlEscape="true" />
      	  		<form:errors path="category.leftBarBottomHtmlCode" cssClass="error" /> 
	  	  		<div style="text-align:right" id="category.leftBarBottomHtmlCode_link"><a href="javascript:loadEditor('category.leftBarBottomHtmlCode')" class="htmleditor">- <fmt:message key="htmlEditor" /> -</a></div>
	  	  		</td></tr></table>
		  	<!-- end input field -->	
		  	</div>
		  	</div>

			<c:if test="${siteConfig['CATEGORY_INTERSECTION'].value == 'true'}">
		  	<div class="list${classIndex % 2}"><c:set var="classIndex" value="${classIndex+1}" />
		  	<div class="listfl"><div align="left" class="helpImg"><img class="toolTipImg" title="Intersection/Union::Enter category ID of other categories that you want to get the intersection/union with this category. Use comma to seperate them." src="../graphics/question.gif" /></div><fmt:message key="categoryIds" />:</div>
		  	<div class="listp">
		  	<!-- input field -->
			   <form:input path="category.categoryIds" maxlength="120" size="50" cssClass="textfield" htmlEscape="true" />
		       Intersection: <form:radiobutton value="intersection" path="category.setType" /> Union: <form:radiobutton value="union" path="category.setType" />
		       <div class="helpNote">
		       <p> For Example: 10,12</p>
		       </div>
		       <form:errors path="category.categoryIds" cssClass="error" />
	 		<!-- end input field -->	
	 		</div>
		  	</div>			
		  	</c:if>

		  	<div class="list${classIndex % 2}"><c:set var="classIndex" value="${classIndex+1}" />
		  	<div class="listfl"><fmt:message key="sortBy" />:</div>
		  	<div class="listp">
		  	<!-- input field -->
			   <form:select path="category.sortBy" id="sortBySelect" onchange="hideDate()">
			     <form:option value=""><fmt:message key="global"/></form:option>
			     <c:if test="${siteConfig['CATEGORY_INTERSECTION'].value == 'true'}">
			     <form:option value="best_seller"><fmt:message key="bestSeller"/></form:option>
			     <form:option value="best_seller_sold"><fmt:message key="bestSellerSold"/></form:option>
			     <form:option value="viewed desc"><fmt:message key="mostViewed"/></form:option>
			     <form:option value="created desc"><fmt:message key="newArrival"/></form:option>
			     <form:option value="rand()"><fmt:message key="random"/></form:option>
			     </c:if>
			   </form:select>
		       <form:errors path="category.sortBy" cssClass="error" />	 		
		    <!-- end input field -->	
	 		</div>
		  	</div>			

			<c:if test="${siteConfig['CATEGORY_INTERSECTION'].value == 'true'}">
		  	<div class="list${classIndex % 2}"><c:set var="classIndex" value="${classIndex+1}" />
		  	<div class="listfl"><div class="helpImg"><img class="toolTipImg" title="Limit::You can limit the number of products to be shown on frontend." src="../graphics/question.gif" /></div><fmt:message key="limit" />:</div>
		  	<div class="listp">
		  	<!-- input field -->
			   <form:input path="category.limit" maxlength="5" size="10" cssClass="textfield" htmlEscape="false" />
		       <form:errors path="category.limit" cssClass="error" />
		    <!-- end input field -->	
	 		</div>
		  	</div>

		  	<div id="startDate" class="list${classIndex % 2}"><c:set var="classIndex" value="${classIndex+1}" />
		  	<div class="listfl"><fmt:message key='startDate' />:</div>
		  	<div class="listp">
		  	<!-- input field -->
				<form:input  path="category.startDate" cssClass="textfield" size="10" />
				<img class="calendarImage"  id="time_start_trigger" src="../graphics/calendarIcon.jpg" />
				<form:errors path="category.startDate" cssClass="error" /> 
				<script type="text/javascript">	
				    Calendar.setup({
				        inputField     :    "category.startDate",   // id of the input field
				        showsTime      :    false,
				        ifFormat       :    "%m/%d/%Y",   // format of the input field
				        button         :    "time_start_trigger",   // trigger for the calendar (button ID)
				        timeFormat     :    "12"
				    });
                </script> 
		    <!-- end input field -->	
	 		</div>
		  	</div>			

		  	<div id="endDate" class="list${classIndex % 2}"><c:set var="classIndex" value="${classIndex+1}" />
		  	<div class="listfl"><fmt:message key='endDate' />:</div>
		  	<div class="listp">
		  	<!-- input field -->
		  	    <form:input  path="category.endDate" cssClass="textfield" size="10" />
				<img class="calendarImage"  id="time_end_trigger" src="../graphics/calendarIcon.jpg" />
				<form:errors path="category.endDate" cssClass="error" />	
				<script type="text/javascript">
			    Calendar.setup({
			        inputField     :    "category.endDate",   // id of the input field
			        showsTime      :    false,
			        ifFormat       :    "%m/%d/%Y",   // format of the input field
			        button         :    "time_end_trigger",   // trigger for the calendar (button ID)
			        timeFormat     :    "12"
			    });
				</script> 
		    <!-- end input field -->	
	 		</div>
		  	</div>
		  	</c:if>
		  	
		  	<c:if test="${gSiteConfig['gPRODUCT_FIELD_SEARCH']  }"	>
		  	<div class="list${classIndex % 2}"><c:set var="classIndex" value="${classIndex+1}" />
		  	<div class="listfl"><div class="helpImg"><img class="toolTipImg" title="<fmt:message key="productFieldSearch" />::DropDowns or checkbox will showup to filter the products on this category. (If products have Product field.)" src="../graphics/question.gif" /></div><fmt:message key="productFieldSearch" />:</div>
		  	<div class="listp">
		  	<!-- input field -->
		  	    <form:checkbox id="productFieldSearch_id" path="category.productFieldSearch"  onchange="showProductFieldSearchType();"/>
            </div>
            </div>
              
     		<div id="productFieldSearchType" class="list${classIndex % 2}"><c:set var="classIndex" value="${classIndex+1}" />
     		<div class="listfl"><fmt:message key="productFieldSearchType" />:</div>
     		<div class="listp" >
     		<!-- input field -->
       			<form:select id="productFieldSearchType_id" path="category.productFieldSearchType" onchange="showProductFieldSearchPosition();">
				 <form:option value="0">Global</form:option>
				 <form:option value="1">Dropdown</form:option>	
				 <form:option value="2">Checkbox</form:option>	
				 <form:option value="3">Custom Form</form:option>	
				</form:select>
	 		<!-- end input field -->     		
		    </div>
		    </div>
		    
     		<div id="productFieldSearchPosition" class="list${classIndex % 2}"><c:set var="classIndex" value="${classIndex+1}" />
     		<div class="listfl"><fmt:message key="productFieldSearchPosition" />:</div>
     		<div class="listp" >
     		<!-- input field -->
       			<form:select path="category.productFieldSearchPosition">
				 <form:option value=""/>	
				 <form:option value="left">Left</form:option>
				 <form:option value="top">Top</form:option>	
				</form:select>
	 		<!-- end input field -->     		
		    </div>
		    </div>
		    </c:if>
     		
     		<c:if test="${gSiteConfig['gSALES_PROMOTIONS']}">
		  	<div class="list${classIndex % 2}"><c:set var="classIndex" value="${classIndex+1}" />
		  	<div class="listfl"><div class="helpImg"><img class="toolTipImg" title="<fmt:message key="salesTag" />::Products should be associated to this category." src="../graphics/question.gif" /></div><fmt:message key="showProductWithSalesTag" />:</div>
		  	<div class="listp">
		  	<!-- input field -->
       			<form:select path="category.salesTagTitle">
				 <form:option value=""/>
				 <form:option value="all">All</form:option>	
				 <form:options items="${salesTagCodes}" itemValue="title" itemLabel="title"/>	
				</form:select>
	 		<!-- end input field -->	
	 		</div>
		  	</div>			
     		</c:if>
     		
     		<c:if test="${gSiteConfig['gMASTER_SKU']}">
		  	<div class="list${classIndex % 2}"><c:set var="classIndex" value="${classIndex+1}" />
		  	<div class="listfl"><div class="helpImg"><img class="toolTipImg" title="Parent-Child::Hide/Show children products on this Category" src="../graphics/question.gif" /></div><fmt:message key="displayChildren" />:</div>
		  	<div class="listp">
		  	<!-- input field -->
       			<form:select path="category.parentChildDisplay">
				 <form:option value="0" label="Global"/>	
				 <form:option value="1" label="Show Children"/>	
				 <form:option value="2" label="Hide Children"/>	
				</form:select>
	 		<!-- end input field -->	
	 		</div>
		  	</div>			
     		</c:if>	
     		
     		<c:if test="false">
		  	<div class="list${classIndex % 2}"><c:set var="classIndex" value="${classIndex+1}" />
		  	<div class="listfl"><div class="helpImg"><img class="toolTipImg" title="Shared Category::Show this category on shared web site." src="../graphics/question.gif" /></div><fmt:message key="showOnShareCategory" />:</div>
		  	<div class="listp">
		  	<!-- input field -->
       			<form:checkbox path="category.shared" />
	 		<!-- end input field -->	
	 		</div>
		  	</div>			
     		</c:if>	
     				
	</sec:authorize>
	<!-- end tab -->        
	</div>         

    
	<h4 title="Layout">Layout</h4>
	<div>
	<!-- start tab -->

	  	<div class="listdivi ln tabdivi"></div>
	  	<div class="listdivi"></div>
	  	<sec:authorize ifAnyGranted="ROLE_AEM,ROLE_ADMIN_${prop_site_id},ROLE_CATEGORY_EDIT,ROLE_CATEGORY_CREATE">
	  	
	  		<div class="list${classIndex % 2}"><c:set var="classIndex" value="${classIndex+1}" />
		  	<div class="listfl"><fmt:message key="productPerPage" />:</div>
		  	<div class="listp">
		  	<!-- input field -->
			  	<form:select path="category.productPerPage">
		          <form:option value=""><fmt:message key="global"/></form:option>
		          <c:forEach begin="1" end="50" var="products">
		          <form:option value="${products}">${products}</form:option>
		          </c:forEach>      
		        </form:select>
      			<form:errors path="category.productPerPage" cssClass="error" /> 
		    <!-- end input field -->	
		  	</div>
		  	</div>			  		
		  	
		  	<div class="list${classIndex % 2}"><c:set var="classIndex" value="${classIndex+1}" />
		  	<div class="listfl"><div class="helpImg"><img class="toolTipImg" title="<fmt:message key="layout" />::For Parent, if category doesn't have a parent then layout will be the default layout" src="../graphics/question.gif" /></div><fmt:message key="layout" />:</div>
		  	<div class="listp">
		  	<!-- input field -->
			    <form:select path="category.layoutId">
			      <form:option value="0"><fmt:message key="parent"/></form:option>
			      <form:option value="1"><fmt:message key="default"/></form:option>
			      <c:forEach items="${layoutList}" var="option">
			      <c:if test="${option.id > 1}">
			      <form:option value="${option.id}">${option.id} - <c:out value="${option.name}"/></form:option>
				  </c:if>
			      </c:forEach>      
			    </form:select>
			    <form:errors path="category.layoutId" cssClass="error" />  
	 		<!-- end input field -->	
	 		</div>
		  	</div>
		  	
		  	<div class="list${classIndex % 2}"><c:set var="classIndex" value="${classIndex+1}" />
		  	<div class="listfl"><fmt:message key="subcatLevels" />:</div>
		  	<div class="listp">
		  	<!-- input field -->
	   			<form:select path="category.subcatLevels">
        		<form:option value=""><fmt:message key="global"/></form:option>
        		<c:forEach begin="0" end="2" var="subcatLevels">
        		<form:option value="${subcatLevels}">${subcatLevels}</form:option>
        		</c:forEach> 
      			</form:select>
      			<form:errors path="category.subcatLevels" cssClass="error" />  
		    <!-- end input field -->	
		  	</div>
		  	</div>	
		  	
		  	<div class="list${classIndex % 2}"><c:set var="classIndex" value="${classIndex+1}" />
		  	<div class="listfl"><div class="helpImg"><img class="toolTipImg" title="NOTE::Applies if Sub Category Display is Row or Column" src="../graphics/question.gif" /></div><fmt:message key="subcatCols" />:</div>
		  	<div class="listp">
		  	<!-- input field -->
       			<form:select path="category.subcatCols">
        		<c:forEach begin="1" end="5" var="subcatCols">
        		<form:option value="${subcatCols}">${subcatCols}</form:option>
        		</c:forEach> 
       			</form:select>
       			<form:errors path="category.subcatCols" cssClass="error" />  
	 		<!-- end input field -->	
	 		</div>
		  	</div>	
		  	
		  	<div class="list${classIndex % 2}"><c:set var="classIndex" value="${classIndex+1}" />
		  	<div class="listfl"><fmt:message key="subcat" /> <fmt:message key="location" />:</div>
		  	<div class="listp">
		  	<!-- input field -->
       			<form:select path="category.subcatLocation">
        		<form:option value=""><fmt:message key="below" /> <fmt:message key="categoryHtml" /></form:option>
        		<form:option value="1"><fmt:message key="above" /> <fmt:message key="categoryHtml" /></form:option>
       			</form:select>
       			<form:errors path="category.subcatCols" cssClass="error" />  
		    <!-- end input field -->	
		  	</div>
		  	</div>	
		  	
		  	<div class="list${classIndex % 2}"><c:set var="classIndex" value="${classIndex+1}" />
		  	<div class="listfl"><fmt:message key="displayMode" />:</div>
		  	<div class="listp">
		  	<!-- input field -->
			    <form:select path="category.displayMode">
		          <form:option value=""><fmt:message key="global"/></form:option>
		          <form:option value="1">1. <fmt:message key="default" /></form:option>
		          <form:option value="2">2. <fmt:message key="default" /> 2</form:option>
		          <form:option value="3">3. compact up</form:option>
		          <form:option value="4">4. compact short Desc.</form:option>
		          <form:option value="5">5. compact down</form:option>
		          <form:option value="6">6. compact popup</form:option>
		          <form:option value="7">7. compact</form:option>
		          <form:option value="7-1">7-1. compact</form:option>
		          <form:option value="8">8. compact one add to cart</form:option>
		          <form:option value="8-1">8-1. compact one add to cart</form:option>
		          <form:option value="9">9. compact first price</form:option>
		          <form:option value="10">10. divs</form:option>
		          <form:option value="11">11. price break</form:option>
		          <form:option value="12">12. compact one add to cart (MSRP)</form:option>
		          <c:if test="${siteConfig['PRODUCT_QUICK_VIEW'].value == 'true'}">
		          <form:option value="13">13. compact down new</form:option>
		          <form:option value="14">14. compact down new - 14</form:option>
		          <c:if test="${gSiteConfig['gSITE_DOMAIN'] == 'viatrading.com' or gSiteConfig['gSITE_DOMAIN'] == 'test.com'}">
		          <form:option value="15">15. compact down new - 15</form:option>
		          </c:if>
		          <c:if test="${gSiteConfig['gSITE_DOMAIN'] == 'myevergreen.com'}">
		          <form:option value="16">16. compact down new - 16</form:option>
		          </c:if>
		          </c:if>
		          <form:option value="17">17. compact short Desc. One add to cart</form:option>
		          <c:if test="${siteConfig['PRODUCT_QUICK_VIEW'].value == 'true'}">
		          <form:option value="18">18. compact short Desc.</form:option>
		          </c:if>
		          <form:option value="quick"><fmt:message key="quickMode"/></form:option>
		          <c:if test="${siteConfig['PRODUCT_QUICK_MODE_2'].value == 'true'}">
		          	<form:option value="quick2"><fmt:message key="quickMode2"/></form:option>
		          	<form:option value="quick2-1"><fmt:message key="quickMode2"/>-1</form:option>
		          	<form:option value="quick2-2"><fmt:message key="quickMode2"/>-2</form:option>
		          </c:if>
		          <c:if test="${siteConfig['PRODUCT_QUICK_MODE_3'].value == 'true'}">
		          	<form:option value="quick3"><fmt:message key="quickMode3"/></form:option>
		          </c:if>
		        </form:select>
      			<form:errors path="category.displayMode" cssClass="error" />  
	 		  <!-- end input field -->	
	 		  </div>
		  	</div>
		  	
		  	<div class="list${classIndex % 2}"><c:set var="classIndex" value="${classIndex+1}" />
		  	<div class="listfl"><fmt:message key="displayModeGrid" />:</div>
		  	<div class="listp">
		  	<!-- input field -->
			    <form:select path="category.displayModeGrid">
		          <form:option value=""><fmt:message key="global"/></form:option>
		          <form:option value="1">1. <fmt:message key="default" /></form:option>
		          <form:option value="2">2. <fmt:message key="default" /> 2</form:option>
		          <form:option value="3">3. compact up</form:option>
		          <form:option value="4">4. compact short Desc.</form:option>
		          <form:option value="5">5. compact down</form:option>
		          <form:option value="6">6. compact popup</form:option>
		          <form:option value="7">7. compact</form:option>
		          <form:option value="7-1">7-1. compact</form:option>
		          <form:option value="8">8. compact one add to cart</form:option>
		          <form:option value="8-1">8-1. compact one add to cart</form:option>
		          <form:option value="9">9. compact first price</form:option>
		          <form:option value="10">10. divs</form:option>
		          <form:option value="11">11. price break</form:option>
		          <form:option value="12">12. compact one add to cart (MSRP)</form:option>
		          <c:if test="${siteConfig['PRODUCT_QUICK_VIEW'].value == 'true'}">
		          <form:option value="13">13. compact down new</form:option>
		          <form:option value="14">14. compact down new - 14</form:option>
		          <c:if test="${gSiteConfig['gSITE_DOMAIN'] == 'viatrading.com' or gSiteConfig['gSITE_DOMAIN'] == 'test.com'}">
		          <form:option value="15">15. compact down new - 15</form:option>
		          </c:if>
		          <c:if test="${gSiteConfig['gSITE_DOMAIN'] == 'myevergreen.com'}">
		          <form:option value="16">16. compact down new - 16</form:option>
		          </c:if>
		          </c:if>
		          <form:option value="17">17. compact short Desc. One add to cart</form:option>
		          <c:if test="${siteConfig['PRODUCT_QUICK_VIEW'].value == 'true'}">
		          <form:option value="18">18. compact short Desc.</form:option>
		          </c:if>
		          <form:option value="quick"><fmt:message key="quickMode"/></form:option>
		          <c:if test="${siteConfig['PRODUCT_QUICK_MODE_2'].value == 'true'}">
		          	<form:option value="quick2"><fmt:message key="quickMode2"/></form:option>
		          	<form:option value="quick2-1"><fmt:message key="quickMode2"/>-1</form:option>
		          	<form:option value="quick2-2"><fmt:message key="quickMode2"/>-2</form:option>
		          </c:if>
		        </form:select>
      			<form:errors path="category.displayModeGrid" cssClass="error" />  
	 		  <!-- end input field -->	
	 		  </div>
		  	</div>
		  	
		  	
		  	<div class="list${classIndex % 2}"><c:set var="classIndex" value="${classIndex+1}" />
		  	<div class="listfl"><fmt:message key="LeftBar" />:</div>
		  	<div class="listp">
		  	<!-- input field -->
		  		<form:select path="category.leftbarType">
		  			<form:option value=""><fmt:message key="global"/></form:option>
			        <c:forTokens items="${siteConfig['LEFTBAR_TYPE'].value}" delims="," var="lType" varStatus="status">
			  	        <form:option value="${lType}"><fmt:message key="${lType}"/></form:option>
			  	    </c:forTokens>
		  	    </form:select>
		  	    <br />
	   			<form:checkbox path="category.hideLeftBar" /> <fmt:message key="hide" /> <br>
      			<form:checkbox path="category.showSubcats" /> <fmt:message key="showSubcats" /> 
		    <!-- end input field -->	
		  	</div>
		  	</div>
		  	
		  	<div class="list${classIndex % 2}"><c:set var="classIndex" value="${classIndex+1}" />
		  	<div class="listfl"><div class="helpImg"><img class="toolTipImg" title="NOTE::Applies if want to show full width" src="../graphics/question.gif" /></div><fmt:message key="showFullWidth" />:</div>
		  	<div class="listp">
		  	<!-- input field -->
	   			<fmt:message key="yes" /> <form:radiobutton path="category.showFullWidth" value="true" />
                <fmt:message key="no" /> <form:radiobutton path="category.showFullWidth" value="false" /> 
	 		<!-- end input field -->	
	 		</div>
		  	</div>	
		  	
		  	<div class="list${classIndex % 2}"><c:set var="classIndex" value="${classIndex+1}" />
		  	<div class="listfl"><fmt:message key="hide" /><div class="helpImg"><img class="toolTipImg" title="NOTE::Applies if want to hide header" src="../graphics/question.gif" /></div><fmt:message key="Header" />:</div>
		  	<div class="listp">
		  	<!-- input field -->
	   			<fmt:message key="yes" /> <form:radiobutton path="category.hideHeader" value="true" />
                <fmt:message key="no" /> <form:radiobutton path="category.hideHeader" value="false" /> 
	 		<!-- end input field -->	
	 		</div>
		  	</div>	
		  	
		  	<div class="list${classIndex % 2}"><c:set var="classIndex" value="${classIndex+1}" />
		  	<div class="listfl"><fmt:message key="hide" /><div class="helpImg"><img class="toolTipImg" title="NOTE::Applies if want to hide TopBar" src="../graphics/question.gif" /></div> <fmt:message key="TopBar" />:</div>
		  	<div class="listp">
		  	<!-- input field -->
	            <fmt:message key="yes" /> <form:radiobutton path="category.hideTopBar" value="true" />
                <fmt:message key="no" /> <form:radiobutton path="category.hideTopBar" value="false" />
		    <!-- end input field -->	
		  	</div>
		  	</div>				  

            <div class="list${classIndex % 2}"><c:set var="classIndex" value="${classIndex+1}" />
		  	<div class="listfl"><fmt:message key="hide" /><div class="helpImg"><img class="toolTipImg" title="NOTE::Applies if want to hide RightBar" src="../graphics/question.gif" /></div><fmt:message key="RightBar" />:</div>
		  	<div class="listp">
		  	<!-- input field -->
	            <fmt:message key="yes" /> <form:radiobutton path="category.hideRightBar" value="true" />
                <fmt:message key="no" /> <form:radiobutton path="category.hideRightBar" value="false" />
	 		<!-- end input field -->	
	 		</div>
		  	</div>	
		  	
		  	<div class="list${classIndex % 2}"><c:set var="classIndex" value="${classIndex+1}" />
		  	<div class="listfl"><fmt:message key="hide" /><div class="helpImg"><img class="toolTipImg" title="NOTE::Applies if want to hide Footer" src="../graphics/question.gif" /></div> <fmt:message key="Footer" />:</div>
		  	<div class="listp">
		  	<!-- input field -->
	            <fmt:message key="yes" /> <form:radiobutton path="category.hideFooter" value="true" />
                <fmt:message key="no" /> <form:radiobutton path="category.hideFooter" value="false" />
		    <!-- end input field -->	
		  	</div>
		  	</div>	
		  	
		  	<div class="list${classIndex % 2}"><c:set var="classIndex" value="${classIndex+1}" />
		  	<div class="listfl"><fmt:message key="hide" /><div class="helpImg"><img class="toolTipImg" title="NOTE::Applies if want to hide BreadCrumbs" src="../graphics/question.gif" /></div><fmt:message key="breadCrumbs" />:</div>
		  	<div class="listp">
		  	<!-- input field -->
	            <fmt:message key="yes" /> <form:radiobutton path="category.hideBreadCrumbs" value="true" />
                <fmt:message key="no" /> <form:radiobutton path="category.hideBreadCrumbs" value="false" />
	 		<!-- end input field -->	
	 		</div>
		  	</div> 
		  	</sec:authorize>
		  	
		  	<sec:authorize ifAnyGranted="ROLE_AEM,ROLE_ADMIN_${prop_site_id},ROLE_CATEGORY_EDIT,ROLE_CATEGORY_CREATE,ROLE_CATEGORY_UPDATE_HEAD_TAG">
		  	<div class="list${classIndex % 2}"><c:set var="classIndex" value="${classIndex+1}" />
		  	<div class="listfl"><fmt:message key="headTag" />:</div>
		  	<div class="listp">
		  	<!-- input field -->
	            <form:textarea path="category.headTag" rows="15" cols="60" htmlEscape="true"/>
		    <!-- end input field -->	
		  	</div>
		  	</div>	
		  	</sec:authorize>
	   
      
	<!-- end tab -->		
	</div>
	
	<c:import url="/WEB-INF/jsp/admin/catalog/category/formOptionSection.jsp" />
	
	<c:import url="/WEB-INF/jsp/admin/catalog/category/formLinkImageSection.jsp" />
	
    <h4 title="seo">SEO</h4>
	<div>
	<!-- start tab -->
		<div class="listdivi ln tabdivi"></div>
	  	<div class="listdivi"></div>
	  	<c:if test="${siteConfig['SITEMAP'].value == 'true'}">
  		  	<div class="list${classIndex % 2}"><c:set var="classIndex" value="${classIndex+1}" />
		  	<div class="listfl"><div class="helpImg"><img class="toolTipImg" title="Site Map Priority::Select category priority for Site Map. Default priority is 0.5" src="../graphics/question.gif" /></div><fmt:message key="priority" />:</div>
		  	<div class="listp">
		  	<!-- input field -->
		  	   <form:select path="category.siteMapPriority">
		  	     <c:forEach begin="0" end="10" step="1" var="priority">
		  	       <form:option value="${priority/10}" label="${priority/10}"></form:option>
		  	     </c:forEach>
		  	   </form:select>
			<!-- end input field -->	
		  	</div>
		 </div>
		 </c:if>
		  	<div class="list${classIndex % 2}"><c:set var="classIndex" value="${classIndex+1}" />
		  	<div class="listfl"><div class="helpImg"><img class="toolTipImg" title="Field 1:: The Maximum character is 512" src="../graphics/question.gif" /></div><fmt:message key="categorField1" />:</div>
		  	<div class="listp">
		  	<!-- input field -->
	   			<form:input path="category.field1" cssClass="textfield" maxlength="255" htmlEscape="true"/>
	   			<form:errors path="category.field1" cssClass="error" />
		    <!-- end input field -->  	  
		  	</div></div>
		  	<div class="list${classIndex % 2}"><c:set var="classIndex" value="${classIndex+1}" />
		  	<div class="listfl"><div class="helpImg"><img class="toolTipImg" title="Field 2::The Maximum character is 512" src="../graphics/question.gif" /></div><fmt:message key="categorField2" />:</div>
		  	<div class="listp">
		  	<!-- input field -->
	   			<form:input path="category.field2" cssClass="textfield" maxlength="255" htmlEscape="true"/>
	   			<form:errors path="category.field2" cssClass="error" />
		    <!-- end input field -->  	  
		  	</div>
		  	</div>
		  	<div class="list${classIndex % 2}"><c:set var="classIndex" value="${classIndex+1}" />
		  	<div class="listfl"><div class="helpImg"><img class="toolTipImg" title="Field 3::The Maximum character is 512" src="../graphics/question.gif" /></div><fmt:message key="categorField3" />:</div>
		  	<div class="listp">
		  	<!-- input field -->
	   			<form:input path="category.field3" cssClass="textfield" maxlength="255" htmlEscape="true"/>
	   			<form:errors path="category.field3" cssClass="error" />
		    <!-- end input field -->  	  
		  	</div>
		  	</div>
		  	<div class="list${classIndex % 2}"><c:set var="classIndex" value="${classIndex+1}" />
		  	<div class="listfl"><div class="helpImg"><img class="toolTipImg" title="Field 4::The Maximum character is 512" src="../graphics/question.gif" /></div><fmt:message key="categorField4" />:</div>
		  	<div class="listp">
		  	<!-- input field -->
	   			<form:input path="category.field4" cssClass="textfield" maxlength="255" htmlEscape="true"/>
	   			<form:errors path="category.field4" cssClass="error" />
		    <!-- end input field -->  	  
		  	</div>
		  	</div>
		  	<div class="list${classIndex % 2}"><c:set var="classIndex" value="${classIndex+1}" />
		  	<div class="listfl"><div class="helpImg"><img class="toolTipImg" title="Field 5::The Maximum character is 512" src="../graphics/question.gif" /></div><fmt:message key="categorField5" />:</div>
		  	<div class="listp">
		  	<!-- input field -->
	   			<form:input path="category.field5" cssClass="textfield" maxlength="255" htmlEscape="true"/>
	   			<form:errors path="category.field5" cssClass="error" />
		    <!-- end input field -->  	  
		  	</div>
		  	</div>		 
	<!-- end tab -->        
	</div>   
  
    <c:forEach items="${languageCodes}" var="i18n">
	<h4 title="<fmt:message key="language_${i18n.languageCode}"/>"><img src="../graphics/${i18n.languageCode}.gif" border="0" title="<fmt:message key="language_${i18n.languageCode}"/>"> <fmt:message key="language_${i18n.languageCode}"/></h4>
	<div>
	<!-- start tab -->

	  	<div class="listdivi ln tabdivi"></div>
	  	<div class="listdivi"></div>
		  	<div class="list${classIndex % 2}"><c:set var="classIndex" value="${classIndex+1}" />
		  	<div class="listfl"><fmt:message key="categoryName" />:</div>
		  	<div class="listp">
		  	<!-- input field -->
			  	<input type="text" name="__i18nName_${i18n.languageCode}" value="<c:out value="${categoryForm.i18nCategory[i18n.languageCode].i18nName}"/>" class="textfield" maxlength="150" size="50">
		    <!-- end input field -->   	
		  	</div>
		  	</div>

		  	<div class="list${classIndex % 2}"><c:set var="classIndex" value="${classIndex+1}" />
		  	<div class="listfl"><fmt:message key="headTag" />:</div>
		  	<div class="listp">
		  	<!-- input field -->
	            <textarea name="__i18nHeadTag_${i18n.languageCode}" rows="15" cols="60" class="textfield"><c:out value="${categoryForm.i18nCategory[i18n.languageCode].i18nHeadTag}"/></textarea>
		    <!-- end input field -->	
		  	</div>
		  	</div>
		  	
		  	<div class="list${classIndex % 2}"><c:set var="classIndex" value="${classIndex+1}" />
		  	<div class="listfl"><fmt:message key="categoryHtml" />:</div>
		  	<div class="listp">
		  	<!-- input field -->
	   			<table class="form" border="0" cellpadding="0" cellspacing="0"><tr><td>
      	  		<textarea name="__i18nHtmlCode_${i18n.languageCode}" rows="15" cols="60" class="textfield __i18nHtmlCode_${i18n.languageCode}"><c:out value="${categoryForm.i18nCategory[i18n.languageCode].i18nHtmlCode}"/></textarea> 
	  	  		<div style="text-align:right" id="__i18nHtmlCode_${i18n.languageCode}_link"><a href="javascript:loadEditor('__i18nHtmlCode_${i18n.languageCode}')" class="htmleditor">- <fmt:message key="htmlEditor" /> -</a></div>
		  		</td></tr></table>
		  	<!-- end input field --> 
		  	</div>
		  	</div>	
		  	     
		  	<c:if test="${gSiteConfig['gPROTECTED'] > 0}">
		  	<div class="list${classIndex % 2}" id="protectedHtmlCode_${i18n.languageCode}" <c:if test="${categoryForm.category.protectedLevel == 0}">style="display:none"</c:if>><c:set var="classIndex" value="${classIndex+1}" />
		  	<div class="listfl"><fmt:message key="protectedHtmlCode" />:</div>
		  	<div class="listp">
		  	<!-- input field -->
       			<table class="form" border="0" cellpadding="0" cellspacing="0"><tr><td>
      	  		<textarea name="__i18nProtectedHtmlCode_${i18n.languageCode}" rows="15" cols="60" class="textfield __i18nProtectedHtmlCode_${i18n.languageCode}"><c:out value="${categoryForm.i18nCategory[i18n.languageCode].i18nProtectedHtmlCode}"/></textarea> 
	  	  		<div style="text-align:right" id="__i18nProtectedHtmlCode_${i18n.languageCode}_link"><a href="javascript:loadEditor('__i18nProtectedHtmlCode_${i18n.languageCode}')" class="htmleditor">- <fmt:message key="htmlEditor" /> -</a></div>
	       		</td></tr></table>
	 		<!-- end input field -->	
	 		</div>
		  	</div>			
     		</c:if>
		  		    
	<!-- end tab -->		
	</div>	
	</c:forEach>
	
<!-- end tabs -->			
</div>

<!-- start button -->
<div align="left" class="button">
<c:if test="${categoryForm.newCategory}">
  <sec:authorize ifAnyGranted="ROLE_AEM,ROLE_ADMIN_${prop_site_id},ROLE_CATEGORY_CREATE">
  <input type="submit" value="<fmt:message key="categoryAdd"/>">
  </sec:authorize>
</c:if>
<c:if test="${!categoryForm.newCategory}">
 <sec:authorize ifAnyGranted="ROLE_AEM,ROLE_ADMIN_${prop_site_id},ROLE_CATEGORY_EDIT,ROLE_CATEGORY_UPDATE_HEAD_TAG">
   <input type="submit" value="<fmt:message key="update"/>" name="_update">
 </sec:authorize> 
 <sec:authorize ifAnyGranted="ROLE_AEM,ROLE_ADMIN_${prop_site_id},ROLE_CATEGORY_DELETE">
   <c:if test="${categoryForm.category.host == null and categoryForm.category.feed == null}">
   <input type="submit" value="<fmt:message key="categoryDelete"/>" name="_delete" onClick="return confirm('Delete permanently?')">
   </c:if>
 </sec:authorize>  
  <input type="submit" value="<fmt:message key="cancel"/>" name="_cancel">
</c:if>
</div>
<!-- end button -->	
  
  <!-- end table -->
    </td><td class="boxmidr" ></td></tr>
  <tr><td class="botl"></td><td class="botr"></td></tr>
  </table>
  
<!-- end main box -->  
 </div>
</form:form>

<c:if test="${gSiteConfig['gPROTECTED'] > 0}">
<script language="JavaScript">
<!--
function toggleProtectedHtmlCode(el) {
  if (el.value == "0") {
      document.getElementById('protectedHtmlCode').style.display="none";
      <c:forEach items="${languageCodes}" var="i18n">
      document.getElementById('protectedHtmlCode_${i18n}').style.display="none";
      </c:forEach>
  } else {
      document.getElementById('protectedHtmlCode').style.display="block";
      <c:forEach items="${languageCodes}" var="i18n">
      document.getElementById('protectedHtmlCode_${i18n}').style.display="block";
      </c:forEach>      
  }
}
//-->
</script>
</c:if>
<c:if test="${siteConfig['CATEGORY_INTERSECTION'].value == 'true'}">
<script type="text/javascript">	
  if (document.getElementById('sortBySelect').value == "viewed") {
      document.getElementById('startDate').style.display="none";
      document.getElementById('endDate').style.display="none";
  } else {
      document.getElementById('startDate').style.display="block";
      document.getElementById('endDate').style.display="block";
  }
</script> 
</c:if>
  </tiles:putAttribute>    
</tiles:insertDefinition>