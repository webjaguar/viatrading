<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<c:forEach var="category" items="${categories}">
	
	<tr>
	  <c:forEach begin="1" end="${level}">
	    <td>&nbsp;</td>
	  </c:forEach>
	  <td style="background-color:#FFFFFF;border: #919191 1px solid;padding:5px;">
		<c:out value="${category.name}"></c:out>
	  </td>
	</tr>
	
	<c:if test="${category.subCategories != null and fn:length(category.subCategories) gt 0}">
    	<c:set var="level" value="${level + 1}" scope="request"/>
    	<c:set var="categories" value="${category.subCategories}" scope="request"/>
	    <jsp:include page="/WEB-INF/jsp/admin/catalog/category/recursion.jsp"/>
		<c:set var="level" value="${level - 1}" scope="request"/>
    </c:if>
	
</c:forEach>