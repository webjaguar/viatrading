<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/security/tags" prefix="sec" %>

<tiles:insertDefinition name="admin.catalog.category" flush="true">
	<tiles:putAttribute name="content" type="string">
<form action="categoryList.jhtm" method="post">
<!-- main box -->
  <div id="mboxfull">
  
  <!-- start table -->
  <table cellpadding="0" cellspacing="0" border="0" class="module" >
  <tr>
    <td class="topl_g">
	  
	  <!-- breadcrumb -->
	  <p class="breadcrumb">
	    <a href="../catalog/categoryList.jhtm?parent=">Category</a>&nbsp;&gt;
	    <c:forEach items="${model.categoryTree}" var="category" varStatus="status">
					<a href="../catalog/categoryList.jhtm?parent=${category.id}"><c:out value="${category.name}" /></a><c:if test="${!status.last}">&nbsp;&gt;</c:if>
			</c:forEach>		
	  </p>
	  
	  <!-- Error -->
	  
	  <!-- header image -->
	  <img class="headerImage" src="../graphics/category.gif" />
  
  </td><td class="topr_g" ></td></tr>
  <tr><td class="boxmidlrg" >

 <!-- tabs -->
<div class="tab-wrapper">
	<h4 title="List of Categories"></h4>
	<div>
	<!-- start tab -->

	  	<%-- <div class="listdivi ln tabdivi"></div>--%>
	  	<div class="listdivi"></div>
	    
		  	<div class="listlight">
		  	<!-- input field -->
			<table border="0" cellpadding="0" cellspacing="1" width="100%">
				<tr>
					<td>
						<select name="parent"
							onChange="document.getElementById('page').value=1;submit()">
							<option value=""><fmt:message key="categoryMain" /></option>
							<c:if test="${gSiteConfig['gMULTI_STORE'] > 0}">
							<option value="multi" <c:if test="${model.search['multiStore']}"> selected</c:if>><fmt:message key="multiStore" /></option>		
							</c:if>
							<option value="home" <c:if test="${model.search['homePage']}"> selected</c:if>><fmt:message key="homePages" /></option>		
							<c:forEach items="${model.categoryTree}" var="category">
								<option value="${category.id}"
									<c:if test="${category.id == model.search['parent']}">selected</c:if>>
									<fmt:message key="categorySub" />
									<c:out value="${category.name}" />
								</option>
							</c:forEach>
						</select>
					</td>
					<c:if test="${model.categories.nrOfElements > 0}">
						<td class="pageShowing">
							<fmt:message key="showing">
								<fmt:param value="${model.categories.firstElementOnPage + 1}" />
								<fmt:param value="${model.categories.lastElementOnPage + 1}" />
								<fmt:param value="${model.categories.nrOfElements}" />
							</fmt:message>
						</td>
					</c:if>
					<td class="pageNavi">
						Page
						<select name="page" id="page" onchange="submit()">
							<c:forEach begin="1" end="${model.categories.pageCount}"
								var="page">
								<option value="${page}"
									<c:if test="${page == (model.categories.page+1)}">selected</c:if>>
									${page}
								</option>
							</c:forEach>
						</select> 
						of
						<c:out value="${model.categories.pageCount}" /> 
						|
						<c:if test="${model.categories.firstPage}">
							<span class="pageNaviDead"><fmt:message key="previous" /></span>
						</c:if>
						<c:if test="${not model.categories.firstPage}">
							<a
								href="<c:url value="categoryList.jhtm"><c:param name="page" value="${model.categories.page}"/><c:if test="${not model.search['homePage']}"><c:param name="parent" value="${model.search['parent']}"/></c:if></c:url>"
								class="pageNaviLink"><fmt:message key="previous" /></a>
						</c:if>
						|
						<c:if test="${model.categories.lastPage}">
							<span class="pageNaviDead"><fmt:message key="next" /></span>
						</c:if>
						<c:if test="${not model.categories.lastPage}">
							<a
								href="<c:url value="categoryList.jhtm"><c:param name="page" value="${model.categories.page+2}"/><c:if test="${not model.search['homePage']}"><c:param name="parent" value="${model.search['parent']}"/></c:if></c:url>"
								class="pageNaviLink"><fmt:message key="next" /></a>
						</c:if>
					</td>
				</tr>
			</table>

			<table border="0" cellpadding="0" cellspacing="0" width="100%"
				class="listings">
				<tr class="listingsHdr2">
					<td class="indexCol">
						&nbsp;
					</td>
					<c:if test="${model.search['multiStore']}">
					<td class="listingsHdr3"><fmt:message key="multiStore" /></td>
					</c:if>
					<td class="listingsHdr3">
						<fmt:message key="categoryName" />
					</td>
					<td>&nbsp;</td>
					<td class="listingsHdr3">
						<fmt:message key="categoryId" />
					</td>
					<td class="listingsHdr3">
						&nbsp;
					</td>
					<td class="listingsHdr3">
						&nbsp;
					</td>
					<td class="listingsHdr3">
						<fmt:message key="numOfClicks" />
					</td>
					<td align="center" class="listingsHdr3">HTML</td>
					<td align="center" class="listingsHdr3">
						<fmt:message key="rank" />
					</td>
				</tr>
				<c:forEach items="${model.categories.pageList}" var="category"
					varStatus="status">
					<tr class="row${status.index % 2}"
						onmouseover="changeStyleClass(this,'rowOver')"
						onmouseout="changeStyleClass(this,'row${status.index % 2}')">
						<td class="indexCol">
							<c:out
								value="${status.count + model.categories.firstElementOnPage}" />
							.
						</td>
						<c:if test="${model.search['multiStore']}">
						<td class="nameCol"><c:out value="${category.host}" /></td>
						</c:if>						
						<td class="nameCol">
							<a href="category.jhtm?id=${category.id}" class="nameLink"><c:out value="${category.name}" /> </a>
							<span class="sup">
								<c:choose>
									<c:when test="${category.linkType == 'NONLINK'}">&sup1;</c:when>
									<c:when test="${category.linkType == 'HIDDEN'}">&sup2;</c:when>
								</c:choose>
							</span>
						</td>
						<td>
			      			<c:if test="${gSiteConfig['gPROTECTED'] > 0 and category.protectedLevelAsNumber != '0'}">
			      			  <img src="../graphics/padlock.gif" border="0"><sup><c:out value="${category.protectedLevelAsNumber}"/></sup>
			      			</c:if>							
							<c:if test="${category.homePage}">
								<img src="../graphics/homePage.gif" border="0"
									title="<fmt:message key="homePage" />">
							</c:if>
						</td>
						<td class="idCol">
							<c:out value="${category.id}" /><a href="${siteConfig['SITE_URL'].value}category.jhtm?cid=${category.id}" target="_blank"><img style="padding-left:2px;" src="../graphics/eye.png" border="0"/></a>
						</td>
						<td class="categorySubCountCol">
							<a
								href="<c:url value="categoryList.jhtm"><c:param name="parent" value="${category.id}"/></c:url>"
								class="categorySubCountLink"><fmt:message key="viewSubCategories" /> (<b><c:out
									value="${category.subCount}" /></b>)
							</a>
						</td>
						<td class="categoryProductCountCol">
							<a
								href="<c:url value="productList.jhtm"><c:param name="category" value="${category.id}"/></c:url>"
								class="categoryProductCountLink"><fmt:message key="viewProducts" /> (<b><c:out
									value="${category.productCount}" /></b>)
							</a>
						</td>
						<td class="countCol">
							<c:out value="${category.numOfClicks}" />
						</td>
   						<td align="center">
     							<c:if test="${category.hasHtmlCode}"><img src="../graphics/checkbox.png" border="0" /></c:if>
						</td>
						<td class="rankCol">
							<input name="__rank_${category.id}" type="text"
								value="${category.rank}" size="5" maxlength="5"
								class="rankField">
						</td>
					</tr>
				</c:forEach>
				<c:if test="${model.categories.nrOfElements == 0}">
					<tr class="emptyList">
						<td colspan="8">
							&nbsp;
						</td>
					</tr>
				</c:if>
			</table>
			
			<c:if test="${model.categories.nrOfElements > 0}">
				<div class="sup">
					&sup1; non linkable on storefront
				</div>
				<div class="sup">
					&sup2; hidden on storefront
				</div>
			</c:if>

			<table border="0" cellpadding="0" cellspacing="1" width="100%">
				<tr>
				  <td class="pageSize">
					<select name="size"
						onchange="document.getElementById('page').value=1;submit()">
						<c:forTokens items="10,25,50,100" delims="," var="current">
							<option value="${current}"
								<c:if test="${current == model.categories.pageSize}">selected</c:if>>
								${current}
								<fmt:message key="perPage" />
							</option>
						</c:forTokens>
					</select>
				  </td>
				</tr>
			</table>
            <!-- end input field -->  	  
		  	
		  	<!-- start button -->
		  	<div align="left" class="button"> 
			  <sec:authorize ifAnyGranted="ROLE_AEM,ROLE_ADMIN_${prop_site_id},ROLE_CATEGORY_CREATE">  
				<input type="submit" name="__add" value="<fmt:message key="categoryAdd" />">
			  </sec:authorize>
			  <sec:authorize ifAnyGranted="ROLE_AEM,ROLE_ADMIN_${prop_site_id},ROLE_CATEGORY_UPDATE_RANK">  	
				<input type="submit" name="__update_ranking" value="<fmt:message key="updateRanking" />">
			  </sec:authorize>
			</div>			  	
            <!-- end button -->	
	        </div>
	        
	       <div id="footer"  align="center" class="quickMode">
  <div class="footer_r"></div>
  <div class="footer_l">
  <div class="foottxt">
  <div><a href="../config/configuration.jhtm"></a>

  <a href="http://advancedemedia.com/faq.jhtm" target="_blank"></a>

  <a href="../catalog/dashboard.jhtm?newAdmin=true" target="_blank">Responsive (Beta) version</a>
  </div></div>
  </div>
</div>
  	
	<!-- end tab -->        
	</div>
<!-- end tabs -->			
</div>
  
  <!-- end table -->
   </td><td class="boxmidr" ></td></tr>
  <tr><td class="botl"></td><td class="botr"></td></tr>
  </table>
  
<!-- end main box -->  
</div>
</form>	

	</tiles:putAttribute>
</tiles:insertDefinition>