<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/security/tags" prefix="sec" %>

<script type="text/javascript">
<!--
window.addEvent('domready', function(){
	var Tips1 = new Tips($$('.toolTipImg'));
	var myMenu = new MenuMatic({ orientation:'vertical' });
	var opSearchBox = new multiBox('mbOperator', {showControls : false, useOverlay: false, showNumbers: false });
});
function selectOperator(e) {
	document.getElementById('product_skuOperator').value = e;
}
function selectParentSkuOperator(e) {
	document.getElementById('product_parent_skuOperator').value = e;
}
function submitSearchForm(fileType) {
	if (typeof document.forms['searchform'] != 'undefined') {
	  document.searchform.action = "productExport.jhtm?fileType2="+fileType;
	  document.searchform.submit();
	} else {
	  myform=document.createElement("form");
	  document.body.appendChild(myform);
      myform.method = "POST";
      myform.action= "productExport.jhtm?fileType2="+fileType;
      myform.submit();
	}
}
//-->
</script>		
<script src="../javascript/MenuMatic_0.68.3.js" type="text/javascript" charset="utf-8"></script>
  <div id="lbox" class="quickMode">
   <table cellpadding="0" cellspacing="0" border="0" class="module" style="width: 100%"><tr><td class="topl">
    </td><td class="topr" ></td></tr><tr><td class="boxmid" >
  
    <h2 class="menuleft mfirst"><fmt:message key="categoriesMenuTitle"/></h2>
    <div class="menudiv"></div>
    <a href="categoryList.jhtm" class="userbutton"><fmt:message key="list"/></a>
    <sec:authorize ifAnyGranted="ROLE_AEM,ROLE_ADMIN_${prop_site_id}">
    <a href="categoryExport.jhtm" class="userbutton"><fmt:message key="export"/></a>
    </sec:authorize>
    <c:choose>
    <c:when test="${gSiteConfig['gRESELLER'] == 'roi'}">
      <a href="categoryTreeImport.jhtm" class="userbutton"><fmt:message key="import"/></a>    
    </c:when>
    <c:otherwise>
      <sec:authorize ifAnyGranted="ROLE_AEM">
      <a href="categoryTreeImport.jhtm" class="userbutton"><fmt:message key="import"/> (AEM Only)</a>   
      </sec:authorize> 
    </c:otherwise>
    </c:choose>
    <div class="menudiv"></div>
    <h2 class="menuleft"><fmt:message key="productsMenuTitle"/></h2>
    <div class="menudiv"></div>
    <ul id="nav">
    <li><a href="productList.jhtm" class="userbutton"><fmt:message key="list"/></a></li>
    <li><a href="productImport.jhtm" class="userbutton"><fmt:message key="import"/></a></li>   
  	<li><a href="#" class="userbutton"><fmt:message key="export"/></a>
      <ul>
        <li><a href="javascript: submitSearchForm('xls')" class="userbutton">EXCEL</a></li>
        <li><a href="javascript: submitSearchForm('csv')" class="userbutton">CSV</a></li>
      </ul>
    </li>
    <li><a href="productFields.jhtm" class="userbutton"><fmt:message key="Fields"/></a></li>
    <li><a href="options.jhtm" class="userbutton"><fmt:message key="options"/></a></li>
    <c:if test="${gSiteConfig['gPRODUCT_REVIEW'] or siteConfig['PRODUCT_RATE'].value == 'true'}">
      <li><a href="productReviewList.jhtm" class="userbutton"><fmt:message key="reviews"/></a></li>
    </c:if>
    <c:if test="${siteConfig['LUCENE_REINDEX'].value == 'true'}">
      <li><a href="reIndex.jhtm?now" class="userbutton">Re-index Now</a></li>
    </c:if>
    </ul>
    <div class="menudiv"></div>
    
    <c:if test="${gSiteConfig['gSITE_DOMAIN'] == 'doors2gostore.com'}">
      <h2 class="menuleft">Pre-Hanging</h2>
      <div class="menudiv"></div>
      <a href="productOptions.jhtm?id=1236" class="userbutton">Options</a>
      <a href="preHanging.jhtm" class="userbutton">Price</a>
      <div class="menudiv"></div>
    </c:if>    
    
    <c:if test="${gSiteConfig['gSUPPLIER']}">
      <h2 class="menuleft"><fmt:message key="supplierMenuTitle"/></h2>
      <div class="menudiv"></div>
      <a href="productSupplierImport.jhtm" class="userbutton"><fmt:message key="import"/></a>
      <a href="productSupplierExport.jhtm" class="userbutton"><fmt:message key="export"/></a>
    </c:if> 
    
    <div class="menudiv"></div>
    <c:if test="${gSiteConfig['gMANUFACTURER']}">
      <h2 class="menuleft"><fmt:message key="manufacturerMenuTitle"/></h2>
      <div class="menudiv"></div>
      <a href="manufacturerList.jhtm" class="userbutton"><fmt:message key="list"/></a>
    </c:if>
    
    <sec:authorize ifAnyGranted="ROLE_AEM,ROLE_ADMIN_${prop_site_id},ROLE_PROCESSING_VIEW_LIST">
    <c:if test="${gSiteConfig['gPROCESSING']}">
      <div class="menudiv"></div>
      <h2 class="menuleft"><fmt:message key="truckLoadProcessMenuTitle"/></h2>
      <div class="menudiv"></div>
      <a href="truckLoadProcessList.jhtm" class="userbutton"><fmt:message key="list"/></a>
    </c:if>
    </sec:authorize>
    
    <c:if test="${param.tab == 'product'}">
    <link rel="stylesheet" type="text/css" media="all" href="../../admin/javascript/jscalendar-1.0/skins/aqua/theme.css" title="Aqua" /> <!-- calendar stylesheet -->
	<script type="text/javascript" src="../../admin/javascript/jscalendar-1.0/calendar.js"></script> <!-- main calendar program -->
	<script type="text/javascript" src="../../admin/javascript/jscalendar-1.0/lang/calendar-en.js"></script>
	<script type="text/javascript" src="../../admin/javascript/jscalendar-1.0/calendar-setup.js"></script>
	<script type="text/javascript" src="../javascript/side-bar.js"></script>
    <script type="text/javascript" >
    window.addEvent('domready', function(){	
		$('searchHeaderId').addEvent('click',function(){
			document.searchform.submit();
		});
    });
    </script>
    <div class="leftbar_searchWrapper">
      <div align="left" class="searchHeader" id="searchHeaderId"><h2><fmt:message key="search" /></h2></div>
	  <div class="searchformBox">
	  <form action="productList.jhtm" name="searchform" method="post" >
	  <table class="searchBoxLeft"><tr><td valign="top" style="width: 110px;">
	  <!-- content area -->
	    <div class="search2">
	      <p><fmt:message key="categoryId" />:</p>
	      <input id="category" name="category" type="text" value="<c:out value='${productSearch.category}' />" size="15" />
	    </div>
	    <div class="search2">
	      <p><fmt:message key="productId" />:</p>
	      <input name="product_id" type="text" value="<c:out value='${productSearch.productId}' />" size="15" />
	    </div>     
	    <div class="search2">
	      <p><fmt:message key="productName" />:</p>
	      <input name="product_name" type="text" value="<c:out value='${productSearch.productName}' />" size="15" />
	    </div>
	    <div class="search2">
	      <p><fmt:message key="shortDesc" />:</p>
	      <input name="short_desc" type="text" value="<c:out value='${productSearch.shortDesc}' />" size="15" />
	    </div>
	    <div class="search2">
	      <p><fmt:message key="sku"/>:   <a href="#operatorSku" rel="type:element" id="mbOperator1" class="mbOperator" style="margin-left: 67px;"><img class="toolTipImg" title="Advanced Search::Click to see more Options" src="../graphics/question.gif" /></a></p>
		  <div id="operatorSku" class="miniWindowWrapper searchOp displayNone">
	    	<div class="header">Advanced Search</div>
	    	  <fieldset class="">
	    	    <label class="AStitle"><fmt:message key="sku"/></label>
	    	    <input name="product_sku" type="text" value="<c:out value='${productSearch.sku}' />" size="15" disabled />
	    	  </fieldset>
	    	  <fieldset class="bottom">
	    	    <label class="AStitle"><fmt:message key="operator"/></label>
	    	    <div class="options">
	    	       <label class="radio">
	    	       		<input type="radio" <c:if test="${productSearch.skuOperator == 0}" >checked="checked"</c:if> value="0" name="skuOp" onchange="selectOperator(this.value);" /> <strong>Contains</strong>
	    	       </label>
	    	       <label class="radio">
	    	       		<input type="radio" <c:if test="${productSearch.skuOperator == 1}" >checked="checked"</c:if> value="1" name="skuOp" onchange="selectOperator(this.value);" /> <strong>Exact</strong>
	    	       </label>
	    	    </div>
	    	  </fieldset>
	    	  <fieldset>
	    	     <div class="button">
	      			<input type="submit" value="Search" onclick="document.searchform.submit();"/>
      			 </div>
	    	  </fieldset>
		  </div>
		  <input name="product_sku" type="text" value="<c:out value='${productSearch.sku}' />" size="15" />
		  <input type="hidden" name="product_skuOperator" id="product_skuOperator"/>
	    </div>
	    <div class="search2">
	      <p><fmt:message key="parentSku"/>:   <a href="#operatorParentSku" rel="type:element" id="mbparentSkuOperator1" class="mbOperator" style="margin-left: 22px;"><img class="toolTipImg" title="Advanced Search::Click to see more Options" src="../graphics/question.gif" /></a></p>
		  <div id="operatorParentSku" class="miniWindowWrapper searchOp displayNone">
	    	<div class="header">Advanced Search</div>
	    	  <fieldset class="">
	    	    <label class="AStitle"><fmt:message key="parentSku"/></label>
	    	    <input name="product_parent_sku" type="text" value="<c:out value='${productSearch.parentSku}' />" size="15" disabled />
	    	  </fieldset>
	    	  <fieldset class="bottom">
	    	    <label class="AStitle"><fmt:message key="operator"/></label>
	    	    <div class="options">
	    	       <label class="radio">
	    	       		<input type="radio" <c:if test="${productSearch.parentSkuOperator == 0}" >checked="checked"</c:if> value="0" name="skuOp" onchange="selectParentSkuOperator(this.value);" /> <strong>Contains</strong>
	    	       </label>
	    	       <label class="radio">
	    	       		<input type="radio" <c:if test="${productSearch.parentSkuOperator == 1}" >checked="checked"</c:if> value="1" name="skuOp" onchange="selectParentSkuOperator(this.value);" /> <strong>Exact</strong>
	    	       </label>
	    	    </div>
	    	  </fieldset>
	    	  <fieldset>
	    	     <div class="button">
	      			<input type="submit" value="Search" onclick="document.searchform.submit();"/>
      			 </div>
	    	  </fieldset>
		  </div>
		  <input name="product_parent_sku" type="text" value="<c:out value='${productSearch.parentSku}' />" size="15" />
		  <input type="hidden" name="product_parent_skuOperator" id="product_parent_skuOperator"/>
	    </div>
	    <div class="search2">
	      <p><fmt:message key="hasParentSku" />:</p>
	      <select name="_has_parent_sku">
	        <option value=""><fmt:message key="all"/></option> 
	        <option value="0" <c:if test="${productSearch.hasParentSku == '0'}">selected</c:if>>No Parent Sku</option>
	  	    <option value="1" <c:if test="${productSearch.hasParentSku == '1'}">selected</c:if>>Has Parent Sku</option>
	      </select>
	    </div>
	    <div class="search2">
	      <p><fmt:message key="salesTag" />:</p>
	      <input name="salesTag_title" type="text" value="<c:out value='${productSearch.salesTagTitle}' />" size="15" />
	    </div>
	    <c:if test="${gSiteConfig['gKIT']}">
	    <div class="search2">
	      <p><fmt:message key="typeSearch" />:</p>
	      <select name="_type">
	        <option value=""><fmt:message key="all"/></option> 
	        <option value="1" <c:if test="${productSearch.type == '1'}">selected</c:if>><fmt:message key="kit" /></option>
	      </select>
	    </div>
	    </c:if>
	    <c:if test="${gSiteConfig['gINVENTORY']}">
	    <div class="search2">
	      <p><fmt:message key="inventory_onhand" />:</p>
	      <select name="_inventory">
	        <option value="" <c:if test="${productSearch.inventory == ''}">selected</c:if>><fmt:message key="all"/></option> 
	        <option value="1" <c:if test="${productSearch.inventory == '1'}">selected</c:if>><fmt:message key="low"/></option> 
	        <option value="2" <c:if test="${productSearch.inventory == '2'}">selected</c:if>><fmt:message key="negative"/></option> 
	      </select>
	    </div>
        </c:if>
        <div class="search2">
	      <p><fmt:message key="active" />:</p>
	      <select name="_active">
	        <option value=""><fmt:message key="all"/></option> 
	        <option value="0" <c:if test="${productSearch.active == '0'}">selected</c:if>><fmt:message key="inactive" /></option>
	  	    <option value="1" <c:if test="${productSearch.active == '1'}">selected</c:if>><fmt:message key="active" /></option>
	      </select>
	    </div>
	     <div class="search2">
	      <p><fmt:message key="hasPrimarySupplier" />:</p>
	      <select name="_hasPrimarySupplier">
	        <option value=""><fmt:message key="all"/></option> 
	        <option value="1" <c:if test="${productSearch.hasPrimarySupplier == '1'}">selected</c:if>>Yes</option>
	  	    <option value="0" <c:if test="${productSearch.hasPrimarySupplier == '0'}">selected</c:if>>No</option>
	      </select>
	    </div>
	    <c:if test="${gSiteConfig['gCUSTOMER_SUPPLIER']}">
	     <div class="search2">	
		  	<p><fmt:message key="consignmentProducts" />:</p>
	   			<select name="_consignmentProducts" >
				  <option value=""></option>
				  <option value="1" <c:if test="${productSearch.consignment == '1'}" >selected</c:if>>Yes</option>
             	  <option value="0" <c:if test="${productSearch.consignment == '0'}" >selected</c:if>>No</option>	   
				</select>	
		  </div>	    	
	    </c:if>     
	    <c:if test="${gSiteConfig['gSUPPLIER']}">
		<div class="search2">
	     <p><fmt:message key="supplierCompany" />:</p>
	     <input name="supplier_company" type="text" value="<c:out value='${productSearch.supplierCompany}' />" size="15" />
	    </div>
        <div class="search2">
	     <p><fmt:message key="supplierAccountNumber" />:</p>
	     <input name="supplier_account_number" type="text" value="<c:out value='${productSearch.supplierAccountNumber}' />" size="15" />
	    </div>
        </c:if>
        <c:if test="${gSiteConfig['gMANUFACTURER']}">
		  <div class="search2">	
		  	<p><fmt:message key="manufacturer" />:</p>
	   			<select  Class="manufacturerSearch" name="manufacturer" >
				  <option value=""></option>
				  <option value="-1" <c:if test="${productSearch.manufacturer == -1}">selected</c:if>><fmt:message key="noManufacturer" /></option>
				  <c:forEach items="${model.manufacturers}" var="manufacturer">
				   <option value="${manufacturer.name}" <c:if test="${productSearch.manufacturer == manufacturer.name}">selected</c:if>><c:out value="${manufacturer.name}"/></option>
				  </c:forEach>	    
				</select>	
		  </div>
	    </c:if>	    
	     <div class="search2">
	      <p><c:set var="flag" value="false" />
	       <select class="extraFieldSearch" name="productFieldNumber">
	  	    <c:forEach items="${model.productFieldList}" var="productField">
	  	     <c:if test="${!empty productField.name}" ><c:set var="flag" value="true" />
	  	     <option style="font-size:normal;" value="${productField.id}" <c:if test="${productSearch.productField[0].id == productField.id}">selected</c:if>><c:out value="${productField.name}" />:</option>
	  	     </c:if>
	  	    </c:forEach>
	      </select>
	      </p>
	      <c:if test="${flag}" >
	       <input name="productField" type="text" value="<c:out value='${productSearch.productField[0].value}' />" size="11" />
	      </c:if>
	    </div> 
	    <div class="button">
	      <input type="submit" value="Search"/>
	    </div>
	  <!-- content area -->
	  </td>
	  <td valign="top">
	  <div id="sideBar">
	  <a href="#" id="sideBarTab"><img src="../graphics/tabSliderLeft.png" alt="sideBar" title="sideBar" /></a>
	  <div id="sideBarContents" style="width:0px;">
	  	<table cellpadding="0" cellspacing="0" border="0" class="module" >
      		<tr>
      			<td class="topl_g"><div align="left" class="searchHeaderAdvanced"><h2><fmt:message key="advancedSearch" /></h2></div></td>
      			<td class="topr_g" ></td>
      		</tr>
      		<tr><td class="boxmidlrg" >
  
		<table class="searchBoxLeftAS"><tr><td>
		<div id="sideBarContentsInner">
			<div class="advancedSearch">
		      <p>start:</p>
		      <input style="width:90px;margin-right:5px;" name="startDate" id="startDate" type="text" value="<fmt:formatDate type="date" pattern="MM/dd/yyyy" value='${productSearch.startDate}'/>"  size="11" />
			  <img class="calendarImage"  id="startDate_trigger" src="../graphics/calendarIcon.jpg" alt="" />
			  <script type="text/javascript">
			   Calendar.setup({
					inputField     :    "startDate",   // id of the input field
					showsTime      :    false,
					ifFormat       :    "%m/%d/%Y",   // format of the input field
					button         :    "startDate_trigger"   // trigger for the calendar (button ID)
			   });	
			  </script>
			</div>  
		    <div style="clear:both;"></div>    
		    <div class="advancedSearch">
		      <p>end:</p>
		      <input style="width:90px;margin-right:5px;" name="endDate" id="endDate" type="text" value="<fmt:formatDate type="date" pattern="MM/dd/yyyy" value='${productSearch.endDate}'/>"  size="11" />
			  <img class="calendarImage"  id="endDate_trigger" src="../graphics/calendarIcon.jpg" alt="" />
			  <script type="text/javascript">
			   Calendar.setup({
					inputField     :    "endDate",   // id of the input field
					showsTime      :    false,
					ifFormat       :    "%m/%d/%Y",   // format of the input field
					button         :    "endDate_trigger"   // trigger for the calendar (button ID)
			   });	
			  </script>
			</div>  
			<div style="clear:both;"></div>    
		    <div class="advancedSearch">
		      <table border="0">
		       <tr> <td class="advancedSearchDateInterval"><input style="width:12px;margin-right:5px;border: 0px" name="intervaldate" id="today" type="radio" onclick="UpdateStartEndDate(1);" />Today</td> </tr>
		       <tr> <td class="advancedSearchDateInterval"><input style="width:12px;margin-right:5px;border: 0px" name="intervaldate" id="weekToDate" type="radio" onclick="UpdateStartEndDate(2);" />Week To Date</td> </tr>
		       <tr> <td class="advancedSearchDateInterval"><input style="width:12px;margin-right:5px;border: 0px" name="intervaldate" id="monthToDate" type="radio" onclick="UpdateStartEndDate(3);" />Month To Date</td> </tr>
		       <tr> <td class="advancedSearchDateInterval"><input style="width:12px;margin-right:5px;border: 0px" name="intervaldate" id="yearToDate" type="radio" onclick="UpdateStartEndDate(10);" />Year To Date</td> </tr>
		       <tr> <td class="advancedSearchDateInterval"><input style="width:12px;margin-right:5px;border: 0px" name="intervaldate" id="reset" type="radio" onclick="UpdateStartEndDate(20);" />Reset</td> </tr>
		      </table>  
		    </div>
		</div>
		</td></tr></table>
	  
	  </td><td class="boxmidr" ></td></tr>
	  
	  	</table>
	  </div>
	  
	  
	  </div>
	  </td></tr></table>
	  </form>
	</div>  
	</div>  
	<div class="menudiv"></div>  
	</c:if>

    <c:if test="${param.tab == 'option'}"> 
    <div class="menudiv"></div>
      <div align="left" class="searchHeader" id="searchHeaderId"><h2><fmt:message key="search" /></h2></div>
	  <table class="searchBoxLeft" width="100%"><tr><td>
	  <form action="options.jhtm" name="searchform" method="post">
      <!-- content area -->
      <div class="search2">
        <p><fmt:message key="option" /> <fmt:message key="code" />:</p>
        <input name="option_code" type="text" value="<c:out value='${optionSearch.optionCode}' />" size="15" />
      </div>     
      <div class="button">
	      <input type="submit" value="Search"/>
      </div>
      <!-- content area -->
	  </form>
	  </td></tr></table>
	<div class="menudiv"></div>
	</c:if>
	
	<c:if test="${gSiteConfig['gPRODUCT_REVIEW'] or siteConfig['PRODUCT_RATE'].value == 'true'}">
	<c:if test="${param.tab == 'productReview'}"> 
    <div class="menudiv"></div>
      <div align="left" class="searchHeader" id="searchHeaderId"><h2><fmt:message key="search" /></h2></div>
	  <table class="searchBoxLeft" width="100%"><tr><td>
	  <form action="productReviewList.jhtm" name="searchform" method="post">
      <!-- content area -->
	  <div class="search2">
	      <p><fmt:message key="active" />:</p>
	      <select name="active">
	  	    <option value=""><fmt:message key="all" /></option>
	  	    <option value="0" <c:if test="${productReviewSearch.active == '0'}">selected</c:if>><fmt:message key="inactive" /></option>
	  	    <option value="1" <c:if test="${productReviewSearch.active == '1'}">selected</c:if>><fmt:message key="active" /></option>
	       </select>
	    </div>
      <div class="search2">
        <p><fmt:message key="sku" />:</p>
        <input name="sku" type="text" value="<c:out value='${productReviewSearch.productSku}' />" size="15" />
      </div>     
      <div class="button">
	      <input type="submit" value="Search"/>
      </div>
      <!-- content area -->
	  </form>
	  </td></tr></table>
	<div class="menudiv"></div>
	</c:if>
	</c:if>
	
	<c:if test="${gSiteConfig['gPROCESSING']}">
	<sec:authorize ifAnyGranted="ROLE_AEM,ROLE_ADMIN_${prop_site_id},ROLE_REPORT_PROCESSING"> 
	<script>
	   function UpdateStartEndDate(type){
		var myDate = new Date();
		$('endDate').value = (myDate.getMonth()+1) + "/" + myDate.getDate() + "/" + myDate.getFullYear();
			if ( type == 2 ) {
				myDate.setDate(myDate.getDate() - myDate.getDay());
				$('startDate').value = (myDate.getMonth()+1) + "/" + myDate.getDate() + "/" + myDate.getFullYear();
			} else if ( type == 3 ) {
				myDate.setDate(1);
				$('startDate').value = (myDate.getMonth()+1) + "/" + myDate.getDate() + "/" + myDate.getFullYear();
			}else if ( type == 10 ) {
				myDate.setMonth(0, 1);
				$('startDate').value = (myDate.getMonth()+1) + "/" + myDate.getDate() + "/" + myDate.getFullYear();
			}else if ( type == 20 ) {
				$('startDate').value = "";
				$('endDate').value = "";
			}else{
				$('startDate').value = (myDate.getMonth()+1) + "/" + myDate.getDate() + "/" + myDate.getFullYear();
			}
		}
	</script>
	<c:if test="${param.tab == 'truckLoadProcess'}"> 
	<link rel="stylesheet" type="text/css" media="all" href="../../admin/javascript/jscalendar-1.0/skins/aqua/theme.css" title="Aqua" /> <!-- calendar stylesheet -->
	<script type="text/javascript" src="../../admin/javascript/jscalendar-1.0/calendar.js"></script> <!-- main calendar program -->
	<script type="text/javascript" src="../../admin/javascript/jscalendar-1.0/lang/calendar-en.js"></script>
	<script type="text/javascript" src="../../admin/javascript/jscalendar-1.0/calendar-setup.js"></script>
	<script type="text/javascript" src="../javascript/side-bar.js"></script>
	<div class="menudiv"></div> 
      <div align="left" class="searchHeader" id="searchHeaderId"><h2><fmt:message key="search" /></h2></div>
	  <form action="truckLoadProcessList.jhtm" name="searchform" method="post" >
	  <table class="searchBoxLeft" width="100%"><tr><td valign="top" style="width:110px;">
	  <!-- content area -->
        <div class="search2">
	    <p><fmt:message key="truckLoadProcess" />:</p>
	      <input name="name" type="text" value="<c:out value='${truckLoadProcessSearch.name}' />">
	    </div>
	  	<div class="search2">
	      <p><fmt:message key="truckLoadCustom1" />:</p>
	      <input name="custom1" type="text" value="<c:out value='${truckLoadProcessSearch.custom1}' />">
	  	</div>
	  	<div class="search2">
	    <p><fmt:message key="originalSku" />:</p>
	      <input name="originalSku" type="text" value="<c:out value='${truckLoadProcessSearch.originalSku}' />">
	    </div>
	    <div class="producedSku">
	    <p><fmt:message key="producedSku" />:</p>
	      <input name="producedSku" type="text" value="<c:out value='${truckLoadProcessSearch.producedSku}' />">
	    </div>
	  	<div class="search2">
	      <p><fmt:message key="truckLoadCustom2" />:</p>
	      <input name="custom2" type="text" value="<c:out value='${truckLoadProcessSearch.custom2}' />">
	  	</div>
	  	<div class="search2">
	      <p><fmt:message key="truckLoadCustom3" />:</p>
	      <input name="custom3" type="text" value="<c:out value='${truckLoadProcessSearch.custom3}' />">
	  	</div>
	  	<div class="search2">
	      <p><fmt:message key="truckLoadCustom4" />:</p>
	      <input name="custom4" type="text" value="<c:out value='${truckLoadProcessSearch.custom4}' />">
	  	</div>
	  	<div class="search2">
	      <p>PT:</p>
			<select name="pt">
			<option value="" ></option>
            <option value="blank">Blank</option>
            <option value="nonblank">Non blank</option>
            <c:forEach items="${model.truckLoadProcessPTList}" var="pt" varStatus="status">
              <option value="${pt}"<c:if test="${truckLoadProcessSearch.pt == pt}">selected</c:if>>${pt}</option>
            </c:forEach>           
        	</select>
	    </div>
	  	<div class="search2">
	      <p><fmt:message key="status" />:</p>
	      <select name="status">
	        <option value="">Please Select</option> 
			<option value="x" <c:if test="${truckLoadProcessSearch.status == 'x'}">selected</c:if>><fmt:message key="processStatus_x" /></option>
			<option value="c" <c:if test="${truckLoadProcessSearch.status == 'c'}">selected</c:if>><fmt:message key="processStatus_c" /></option>
	        <option value="pr" <c:if test="${truckLoadProcessSearch.status == 'pr'}">selected</c:if>><fmt:message key="processStatus_pr" /></option>
	        <option value="xpr" <c:if test="${truckLoadProcessSearch.status == 'xpr'}">selected</c:if>><fmt:message key="processStatus_xpr" /></option>
			<option value="cpr" <c:if test="${truckLoadProcessSearch.status == 'cpr'}">selected</c:if>><fmt:message key="processStatus_cpr" /></option>
	        <option value="xc" <c:if test="${truckLoadProcessSearch.status == 'xc'}">selected</c:if>><fmt:message key="processStatus_xc" /></option>
	      </select>
	  	</div>
	    <div class="button">
	      <input type="submit" value="Search"/>
		</div>
	  <!-- content area -->
	  </td>
	  <td valign="top">
	  <div id="sideBar">
	
	  <a href="#" id="sideBarTab"><img src="../graphics/tabSliderLeft.png" alt="sideBar" title="sideBar" /></a>
	  <div id="sideBarContents" style="width:0px;">
	  <table cellpadding="0" cellspacing="0" border="0" class="module" >
      <tr>
      <td class="topl_g"><div align="left" class="searchHeaderAdvanced"><h2><fmt:message key="advancedSearch" /></h2></div></td>
      <td class="topr_g" ></td></tr>
      <tr><td class="boxmidlrg" >
  
		<table class="searchBoxLeftAS"><tr><td>
		<div id="sideBarContentsInner">
			<div class="advancedSearch">
		      <p>start:</p>
		      <input style="width:90px;margin-right:5px;" name="startDate" id="startDate" type="text" value="<fmt:formatDate type="date" pattern="MM/dd/yyyy" value='${truckLoadProcessSearch.startDate}'/>"  size="11" />
			  <img class="calendarImage"  id="startDate_trigger" src="../graphics/calendarIcon.jpg" alt="" />
			  <script type="text/javascript">
			   Calendar.setup({
					inputField     :    "startDate",   // id of the input field
					showsTime      :    false,
					ifFormat       :    "%m/%d/%Y",   // format of the input field
					button         :    "startDate_trigger"   // trigger for the calendar (button ID)
			   });	
			  </script>
			</div>  
		    <div style="clear:both;"></div>    
		    <div class="advancedSearch">
		      <p>end:</p>
		      <input style="width:90px;margin-right:5px;" name="endDate" id="endDate" type="text" value="<fmt:formatDate type="date" pattern="MM/dd/yyyy" value='${truckLoadProcessSearch.endDate}'/>"  size="11" />
			  <img class="calendarImage"  id="endDate_trigger" src="../graphics/calendarIcon.jpg" alt="" />
			  <script type="text/javascript">
			   Calendar.setup({
					inputField     :    "endDate",   // id of the input field
					showsTime      :    false,
					ifFormat       :    "%m/%d/%Y",   // format of the input field
					button         :    "endDate_trigger"   // trigger for the calendar (button ID)
			   });	
			  </script>
			</div>  
			<div style="clear:both;"></div>    
		    <div class="advancedSearch">
		      <table border="0">
		       <tr> <td class="advancedSearchDateInterval"><input style="width:12px;margin-right:5px;border: 0px" name="intervaldate" id="today" type="radio" onclick="UpdateStartEndDate(1);" />Today</td> </tr>
		       <tr> <td class="advancedSearchDateInterval"><input style="width:12px;margin-right:5px;border: 0px" name="intervaldate" id="weekToDate" type="radio" onclick="UpdateStartEndDate(2);" />Week To Date</td> </tr>
		       <tr> <td class="advancedSearchDateInterval"><input style="width:12px;margin-right:5px;border: 0px" name="intervaldate" id="monthToDate" type="radio" onclick="UpdateStartEndDate(3);" />Month To Date</td> </tr>
		       <tr> <td class="advancedSearchDateInterval"><input style="width:12px;margin-right:5px;border: 0px" name="intervaldate" id="yearToDate" type="radio" onclick="UpdateStartEndDate(10);" />Year To Date</td> </tr>
		       <tr> <td class="advancedSearchDateInterval"><input style="width:12px;margin-right:5px;border: 0px" name="intervaldate" id="reset" type="radio" onclick="UpdateStartEndDate(20);" />Reset</td> </tr>
		      </table>  
		    </div>
		</div>
		</td></tr></table>
	  
	  </td><td class="boxmidr" ></td></tr>
      <tr><td class="botl"></td><td class="botr"></td></tr>
      </table>
      </div>
    
      </div>
      
	  </td>
	  </tr></table>
	  </form>
	<div class="menudiv"></div>    
	</c:if>
	</sec:authorize>
	</c:if>
	
	

    </td><td class="boxmidr" ></td></tr>
    <tr><td class="botl"></td><td class="botr"></td></tr></table>
  </div>