<%@ page import="java.net.*" %>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/security/tags" prefix="sec" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ taglib uri="/WEB-INF/tlds/custom.tld" prefix="wj" %>

<tiles:insertDefinition name="admin.responsive-template.supplier" flush="true">
	<tiles:putAttribute name="css">
		<style>
			#grid .field-name{
			    width: 200px;
			}
			#grid .field-city{
			    width: 200px;
			}
			#grid .field-zip{
			    width: 120px;
			}
			#grid .field-phone{
				width: 120px;
			}
			#grid .field-fax{
				width: 120px;
			}
			#grid .field-active{
			    text-align: center;
			}
		</style>
	</tiles:putAttribute>
	<tiles:putAttribute name="content" type="string">
		<sec:authorize ifAnyGranted="ROLE_AEM,ROLE_ADMIN_${prop_site_id},ROLE_SUPPLIER_VIEW_LIST">	
			<c:if test="${gSiteConfig['gSUPPLIER'] == true}">
				<form action="supplierList.jhtm" method="post" name="list_form" id="list">
					<div class="page-banner">
						<div class="breadcrumb"><span><a href="${_contextpath}/admin/catalog/dashboard.jhtm">Home</a></span><i class="sli-arrow-right"></i><span><fmt:message key="suppliers" /></span></div>
					</div>
					<div class="page-head">
						<div class="row justify-content-between">
							<div class="col-sm-8 title">
								<h3><fmt:message key="suppliers" /></h3>
							</div>
							<div class="col-sm-4 actions">
								<div class="dropdown dropdown-actions">
									<button type="button" class="btn-outline dropdown-toggle active btn btn-primary" data-toggle="dropdown">
			                             <i class="sli-settings"></i> Actions
									</button>
									<div class="dropdown-menu dropdown-menu-right">
		                   				<sec:authorize ifAnyGranted="ROLE_AEM,ROLE_ADMIN_${prop_site_id},ROLE_SUPPLIER_CREATE">
		                   					<a class="dropdown-item"><fmt:message key="add" /> <fmt:message key="supplier" /></a>
		                   				</sec:authorize>
										<c:if test="${gSiteConfig['gSUPPLIER'] and !gSiteConfig['gADI'] and siteConfig['TRAVERS'].value == ''}">
											<div tabindex="-1" class="dropdown-divider"></div>
											<a href="productSupplierImport.jhtm" class="dropdown-item"><fmt:message key="import"/></a>
											<a href="productSupplierExport.jhtm" class="dropdown-item"><fmt:message key="export"/></a>
										</c:if>
			                         </div>
			                    </div>
			                    <div class="dropdown dropdown-setting">
									<button type="button" class="btn-outline dropdown-toggle active btn btn-primary" data-toggle="dropdown">
			                             <i class="sli-screen-desktop"></i> Display
			                         </button>	                         
									<div class="dropdown-menu dropdown-menu-right">
										<div class="form-group">
					                    		<div class="option-header">Rows</div>
											<select class="custom-select form-control" name="size" onchange="document.getElementById('page').value=1;submit();">
												<c:forTokens items="10,25,50,100,200,500" delims="," var="current">
										  	  		<option value="${current}" <c:if test="${current == model.suppliers.pageSize}">selected</c:if>>${current}</option>
									  			</c:forTokens>
											</select>
										</div>
										<div class="form-group">
					                    		<div class="option-header">Columns</div>
					                    		<div id="column-hider"></div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
					<div class="page-body">
						<div class="grid-stage"> 
							<c:if test="${model.suppliers.nrOfElements > 0}">
								<div class="page-stat">
									<fmt:message key="showing">
										<fmt:param value="${model.suppliers.firstElementOnPage + 1}" />
										<fmt:param value="${model.suppliers.lastElementOnPage + 1}" />
										<fmt:param value="${model.suppliers.nrOfElements}" />
									</fmt:message>
								</div>
							</c:if>
							<table id="grid"></table>
							<div class="footer">
				                <div class="float-right">
									<c:if test="${model.suppliers.firstPage}">
								 		<span class="previous"><i class="sli-arrow-left" style="font-size: 0.7rem"></i> Previous</span>
									</c:if>
									<c:if test="${not model.suppliers.firstPage}">
										<a href="<c:url value="supplierList.jhtm"><c:param name="page" value="${model.suppliers.page}"/>supplierList.jhtm<c:param name="size" value="${model.suppliers.pageSize}"/></c:url>">
											<span class="previous"><i class="sli-arrow-left" style="font-size: 0.7rem"></i> Previous</span>
										</a>
									</c:if>
									<span class="status"><c:out value="${model.suppliers.page+1}" /> of <c:out value="${model.suppliers.pageCount}" /></span>
									<c:if test="${model.suppliers.lastPage}">
									 	<span class="next">Next <i class="sli-arrow-right" style="font-size: 0.7rem"></i></span>
									</c:if>
									<c:if test="${not model.suppliers.lastPage}">
										<a href="<c:url value="supplierList.jhtm"><c:param name="page" value="${model.suppliers.page+2}"/><c:param name="size" value="${model.suppliers.pageSize}"/></c:url>">
											<span class="next">Next <i class="sli-arrow-right" style="font-size: 0.7rem"></i></span>
										</a>
									</c:if>
								</div>
							</div>
						</div>
					</div>
				</form>
			</c:if>
		</sec:authorize>
	</tiles:putAttribute>
	<tiles:putAttribute name="js">
		<!-- DGRID TABLE SCRIPT MUST BE AT BOTTOM OF THE PAGE -->
		<script src="${_contextpath}/admin-responsive-static/assets/plugins/dojo/dojo.js" data-dojo-config="async: true"></script>
		<script>
			//Table Data Sanitization
			var data = [];
			
			<c:forEach items="${model.suppliers.pageList}" var="supplier" varStatus="status">
		    		data.push({
		    			id: '${supplier.id}',
					name: '${supplier.address.company}',
					city: '${wj:escapeJS(supplier.address.city)}',
					state: '${wj:escapeJS(supplier.address.stateProvince)}',
					zip: '${wj:escapeJS(supplier.address.zip)}',
					phone: '${wj:escapeJS(supplier.address.phone)}',
					fax: '${wj:escapeJS(supplier.address.fax)}',
					<c:if test="${supplier.active}">
						active: true,
					</c:if>
				});
			</c:forEach>

			//Columns Definitions
			require(['dojo/_base/lang',
					'dojo/_base/declare',
					'dstore/Memory',
					'dstore/Trackable',
					'dgrid/OnDemandGrid',
					'dgrid/Editor',
					'dgrid/extensions/ColumnHider',
					'dgrid/extensions/ColumnReorder',
					'dgrid/extensions/ColumnResizer',
					'dgrid/Selector',
					'dijit/form/DropDownButton',
					'dijit/DropDownMenu',
					'dijit/MenuItem',
					'summary/SummaryRow',
				], function (lang, declare, Memory, Trackable, OnDemandGrid, Editor, ColumnHider, ColumnReorder, ColumnResizer, Selector, DropDownButton, DropDownMenu, MenuItem, SummaryRow) {
				var columns = {
					batch: {
						selector: 'checkbox',
						unhidable: true
					},
					name: {
						label: '<fmt:message key="Name" />',
						renderCell: function(object){
							var link = document.createElement('a');
							link.className = 'plain';
							link.href = 'supplier.jhtm?sid='+object.id;
							link.textContent = object.name;
							return link;
						},
						sortable: false,
					},
					id: {
						label: '<fmt:message key="supplierId" />',
						get: function(object){
							return object.id;
						},
						sortable: false,
					},
					city: {
						label: '<fmt:message key="city" />',
						get: function(object){
							return object.city;
						},
						sortable: false,
					},
					state: {
						label: '<fmt:message key="state" />',
						get: function(object){
							return object.state;
						},
						sortable: false,
					},
					zip: {
						label: '<fmt:message key="zipCode" />',
						get: function(object){
							return object.zip;
						},
						sortable: false,
					},
					phone: {
						label: '<fmt:message key="phone" />',
						get: function(object){
							return object.phone;
						},
						sortable: false,
					},
					fax: {
						label: '<fmt:message key="fax" />',
						get: function(object){
							return object.fax;
						},
						sortable: false,
					},
					active: {
						label: '<fmt:message key="active" />',
						renderCell: function(object){
							var div = document.createElement("div");
							if(object.active){
								var i = document.createElement("i");
								i.className = 'sli-check';
								div.appendChild(i);
							}
							return div;
						},
						sortable: false,
					},
				};
				var store = new (declare([Memory, Trackable]))({
					data: data
				});
				
				var grid = new (declare([OnDemandGrid, Editor, ColumnHider, ColumnReorder, ColumnResizer, Selector, SummaryRow]))({
					collection: store,
					selectionMode: "none",
					allowSelectAll: true,
					columns: columns
				},'grid');
				
				//sorting columns
				grid.on("dgrid-sort", lang.hitch(this, function(e){
					e.preventDefault();						
					var property = e.sort[0].property;
				    var order = e.sort[0].descending ? "DESC" : "ASC";
				    document.getElementById('sort').value = property + ' ' + order;
				    document.getElementById('list').submit();
				}));
				
				grid.on('dgrid-refresh-complete', function(event) {
					$('#grid-hider-menu').appendTo('#column-hider');
				});
				
				grid.startup();
			});
		</script>
	</tiles:putAttribute>
</tiles:insertDefinition>