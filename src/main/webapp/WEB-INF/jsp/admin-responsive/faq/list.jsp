<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="/WEB-INF/tlds/spring.tld" prefix="spring" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ taglib uri="http://www.springframework.org/security/tags" prefix="sec" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ taglib uri="/WEB-INF/tlds/custom.tld" prefix="wj" %>

<tiles:insertDefinition name="admin.responsive-template.faq" flush="true">
	<tiles:putAttribute name="css">
		<style>
			#grid .field-question{
			    width: 250px;
			}
			#grid .field-rank{
			    text-align: right;
			}
		</style>
	</tiles:putAttribute>
	<tiles:putAttribute name="content" type="string">
		<sec:authorize ifAnyGranted="ROLE_AEM,ROLE_ADMIN_${prop_site_id},ROLE_FAQ_CREATE">
			<form action="faqList.jhtm" method="post" id="list">
				<c:set var="search" value="${model.search}"></c:set>
				<div class="page-banner">
					<div class="breadcrumb"><span><a href="${_contextpath}/admin/catalog/dashboard.jhtm">Home</a></span><i class="sli-arrow-right"></i><span>FAQs</span></div>
				</div>
		  		<div class="page-head">
					<div class="row justify-content-between">
						<div class="col-sm-8 title">
							<h3>FAQs</h3>
						</div>
						<div class="col-sm-4 actions">
							<div class="dropdown dropdown-actions">
								<button type="button" class="btn-outline dropdown-toggle active btn btn-primary" data-toggle="dropdown">
		                             <i class="sli-settings"></i> Actions
		                         </button>
		                         <div class="dropdown-menu dropdown-menu-right">
									<sec:authorize ifAnyGranted="ROLE_AEM,ROLE_ADMIN_${prop_site_id},ROLE_FAQ_CREATE">
										<a class="dropdown-item"><fmt:message key='faqAdd' /></a>
									</sec:authorize>  
									<a class="dropdown-item"><fmt:message key="updateRanking" /></a>
		                         </div>
		                    </div>
		                    <div class="dropdown dropdown-setting">
								<button type="button" class="btn-outline dropdown-toggle active btn btn-primary" data-toggle="dropdown">
		                             <i class="sli-screen-desktop"></i> Display
		                         </button>
								<div class="dropdown-menu dropdown-menu-right">
									<div class="form-group">
				                    		<div class="option-header">Rows</div>
										<select class="custom-select form-control" name="size" onchange="document.getElementById('page').value=1;submit();">
											<c:forTokens items="10,25,50" delims="," var="current">
												<option value="${current}" <c:if test="${current == model.faqs.pageSize}">selected</c:if>>${current}</option>
											</c:forTokens>
										</select>
									</div>
									<div class="form-group">
				                    		<div class="option-header">Columns</div>
				                    		<div id="column-hider"></div>
									</div>
								</div>
		                    </div>
						</div>
					</div>
				</div>
				<div class="page-body">
					<div class="grid-stage">
						<c:if test="${model.faqs.nrOfElements > 0}">
							<div class="page-stat">
								<fmt:message key="showing">
									<fmt:param value="${model.faqs.firstElementOnPage + 1}"/>
									<fmt:param value="${model.faqs.lastElementOnPage + 1}"/>
									<fmt:param value="${model.faqs.nrOfElements}"/>
								</fmt:message>
							</div>
						</c:if>
			            <table id="grid"></table>
						<div class="footer">
			                <div class="float-right">
								<c:if test="${model.faqs.firstPage}">
							 		<span class="previous"><i class="sli-arrow-left" style="font-size: 0.7rem"></i> Previous</span>
								</c:if>
								<c:if test="${not model.faqs.firstPage}">
									<a href="<c:url value="faqList.jhtm"><c:param name="page" value="${model.faqs.page}"/><c:param name="groupId" value="${model.search['groupId']}"/><c:param name="size" value="${model.faqs.pageSize}"/></c:url>" >
										<span class="previous"><i class="sli-arrow-left" style="font-size: 0.7rem"></i> Previous</span>
									</a>
								</c:if>
								<span class="status"><c:out value="${model.faqs.page+1}" /> of <c:out value="${model.faqs.pageCount}"/></span>
								<c:if test="${model.faqs.lastPage}">
								 	<span class="next">Next <i class="sli-arrow-right" style="font-size: 0.7rem"></i></span>
								</c:if>
								<c:if test="${not model.faqs.lastPage}">
									<a href="<c:url value="faqList.jhtm"><c:param name="page" value="${model.faqs.page+2}"/><c:param name="groupId" value="${model.search['groupId']}"/><c:param name="size" value="${model.faqs.pageSize}"/></c:url>">
										<span class="next">Next <i class="sli-arrow-right" style="font-size: 0.7rem"></i></span>
									</a>
								</c:if>
							</div>
						</div>
					</div>
				</div>
				<input type="hidden" id="page" name="page" value="${search.page}"/>
			</form> 
		</sec:authorize>
	</tiles:putAttribute>
	<tiles:putAttribute name="js">
		<!-- DGRID TABLE SCRIPT MUST BE AT BOTTOM OF THE PAGE -->
		<script src="${_contextpath}/admin-responsive-static/assets/plugins/dojo/dojo.js" data-dojo-config="async: true"></script>
		<script>
			function generateMainGrid(){
				//Table Data Sanitization
				var data = [];
				<c:forEach items="${model.faqs.pageList}" var="faq" varStatus="status">
					data.push({
						id: '${faq.id}',
						offset: '<c:out value="${status.count + model.faqs.firstElementOnPage}"/>.',
						faqQuestion: '${wj:escapeJS(faq.question)}',
						rank: '${faq.rank}',
						<c:if test="${gSiteConfig['gPROTECTED'] > 0}">
							protectedLevelAsNumber: '${faq.protectedLevelAsNumber}',
						</c:if>
					});
				</c:forEach>
				
				require(["dgrid/List", "dgrid/OnDemandGrid", "dgrid/Selection", "dgrid/Keyboard", "dgrid/Editor", "dijit/form/CheckBox", 
						"dijit/form/TimeTextBox", "dijit/form/Select", "dojo/data/ObjectStore", "dijit/form/FilteringSelect",
						"dojo/_base/declare", "dojo/_base/lang", "dojo/store/Memory", "dojo/store/Observable", "dgrid/test/data/createSyncStore", 
						'dgrid/extensions/ColumnHider', 'dgrid/extensions/ColumnReorder', 'dgrid/extensions/ColumnResizer',
					], function(List, OnDemandGrid, Selection, Keyboard, Editor, CheckBox, TimeTextBox, Select, ObjectStore, FilteringSelect,
							declare, lang, LegacyMemory, LegacyObservable, createSyncStore, ColumnHider, ColumnReorder, ColumnResizer){

					//Column Definitions
					var columns = {
						offset: {
							label: '',
							get: function(object){ 
								return object.offset;
							},
							sortable: false,
							unhidable: true
						},
						question: {
							label: '<fmt:message key="faqQuestion" />',
							renderCell: function(object){
								var link = document.createElement('a');
								link.text = object.faqQuestion;
								link.href = 'faq.jhtm?id='+object.id;
								return link;
							},
							sortable: false,
						},
						rank: {
							editor: 'text',
							label: '<fmt:message key="rank" />',
							sortable: false,
						},
						flags: {
							label: 'Flags',
							renderCell: function(object){
								var div = document.createElement("div");
								<c:if test="${gSiteConfig['gPROTECTED'] > 0}">
									if(object.protectedLevelAsNumber != 0){
										var i = document.createElement("i");
										i.className = 'sli-lock';
										var sup = document.createElement("sup");
										sup.innerHTML = object.protectedLevelAsNumber;
										div.appendChild(i);
										div.appendChild(sup);
									}
								</c:if>
								return div;
							},
							sortable: false
						}
					};
					
					var grid = new (declare([OnDemandGrid, Selection, Keyboard, Editor, ColumnHider, ColumnReorder, ColumnResizer]))({
						collection: createSyncStore({ data: data }),
						columns: columns,
						selectionMode: "none",
					},'grid');
					
					grid.on('dgrid-refresh-complete', function(event) {
						$('#grid-hider-menu').appendTo('#column-hider');
					});
					
					grid.startup();
				});
			}
			generateMainGrid();
		</script>
	</tiles:putAttribute>
</tiles:insertDefinition>