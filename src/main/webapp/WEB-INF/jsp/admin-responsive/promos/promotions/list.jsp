<%@ page import="java.net.*" %>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/security/tags" prefix="sec" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ taglib uri="/WEB-INF/tlds/custom.tld" prefix="wj" %>

<tiles:insertDefinition name="admin.responsive-template.promos" flush="true"> 
	<tiles:putAttribute name="css">
		<style>
			#grid .field-discount{
				text-align: right;
			}
			#grid .field-minOrder{
				width: 120px;
				text-align: right;
			}
			#grid .field-promoDate{
				width: 350px;
			}
			#grid .field-inEffect{
				text-align: center;
			}
			#grid .field-rank{
				text-align: right;
			}
			#grid .field-rank input{
				text-align: right;
			}
		</style>
	</tiles:putAttribute>
	<tiles:putAttribute name="content" type="string">
  		<form action="promoList.jhtm" method="post" name="list_form" id="list">
 			<div class="page-banner">
				<div class="breadcrumb"><span><a href="${_contextpath}/admin/catalog/dashboard.jhtm">Home</a></span><i class="sli-arrow-right"></i><span><a href="${_contextpath}/admin/promos">Sales/Promos</a></span><i class="sli-arrow-right"></i><span>Promotions</span></div>
			</div>
			<div class="page-head">
				<div class="row justify-content-between">
					<div class="col-sm-8 title">
						<h3>Promotions</h3>
					</div>
					<div class="col-sm-4 actions">
						<div class="dropdown dropdown-actions">
							<button type="button" class="btn-outline dropdown-toggle active btn btn-primary" data-toggle="dropdown">
	                             <i class="sli-settings"></i> Actions
							</button>
							<div class="dropdown-menu dropdown-menu-right">
								<c:if test="${gSiteConfig['gNUMBER_PROMOS'] == '-1' or gSiteConfig['gNUMBER_PROMOS'] > model.nOfpromotions}">
									<sec:authorize ifAnyGranted="ROLE_AEM,ROLE_ADMIN_${prop_site_id},ROLE_PROMOTION_CREATE">
										<a class="dropdown-item"><fmt:message key="promoAdd" /></a>
								  	</sec:authorize>	
								  	<sec:authorize ifAnyGranted="ROLE_AEM,ROLE_ADMIN_${prop_site_id}">  	
										<a class="dropdown-item"><fmt:message key="updateRanking" /></a>
									</sec:authorize>
								</c:if>
							</div>
	                    </div>	                
	                    <div class="dropdown dropdown-setting">
							<button type="button" class="btn-outline dropdown-toggle active btn btn-primary" data-toggle="dropdown">
	                             <i class="sli-screen-desktop"></i> Display
	                         </button>	                         
							<div class="dropdown-menu dropdown-menu-right">
								<div class="form-group">
			                    		<div class="option-header">Rows</div>
									<select class="custom-select form-control" name="size" onchange="document.getElementById('page').value=1;submit();">
										<c:forTokens items="10,25,50,100,200,500" delims="," var="current">
								  	  		<option value="${current}" <c:if test="${current == promoSearch.pageSize}">selected</c:if>>${current}</option>
							  			</c:forTokens>
									</select>
								</div>
								<div class="form-group">
			                    		<div class="option-header">Columns</div>
			                    		<div id="column-hider"></div>
								</div>
							</div>
	                    </div>
					</div>
				</div>
			</div>
		    <div class="page-body">
				<div class="grid-stage">
					<c:if test="${model.promotions.nrOfElements > 0}">
						<div class="page-stat">
							<fmt:message key="showing">
								<fmt:param value="${model.promotions.firstElementOnPage + 1}" />
								<fmt:param value="${model.promotions.lastElementOnPage + 1}" />
								<fmt:param value="${model.promotions.nrOfElements}" />
							</fmt:message>
						</div> 
					</c:if>
				    <table id="grid"></table>
				    <div class="footer">
						<div class="float-left">
							<c:if test="${model.nOfpromotions > 0}">
								<p class="mb-0"><sup>1</sup> <fmt:message key="toEditClickPromoCode" /></p>
							</c:if> 
						</div>
		                <div class="float-right">
							<c:if test="${model.promotions.firstPage}">
						 		<span class="previous"><i class="sli-arrow-left" style="font-size: 0.7rem"></i> Previous</span>
							</c:if>
							<c:if test="${not model.promotions.firstPage}">
								<a href="<c:url value="promoList.jhtm"><c:param name="page" value="${model.promotions.page}"/><c:param name="parent" value="${model.search['parent']}"/><c:param name="size" value="${model.promotions.pageSize}"/></c:url>">
									<span class="previous"><i class="sli-arrow-left" style="font-size: 0.7rem"></i> Previous</span>
								</a>
							</c:if>	
							<span class="status"><c:out value="${model.promotions.page+1}" /> of <c:out value="${model.promotions.pageCount}" /></span>
							<c:if test="${model.promotions.lastPage}">
								<span class="next">Next <i class="sli-arrow-right" style="font-size: 0.7rem"></i></span>
							</c:if>
							<c:if test="${not model.promotions.lastPage}">
								<a href="<c:url value="promoList.jhtm"><c:param name="page" value="${model.promotions.page+2}"/><c:param name="parent" value="${model.search['parent']}"/><c:param name="size" value="${model.promotions.pageSize}"/></c:url>">
									<span class="next">Next <i class="sli-arrow-right" style="font-size: 0.7rem"></i></span>
								</a>
							</c:if>
						</div>
					</div>
				</div>
			</div>
		    <input type="hidden" id="sort" name="sort" value="${promoSearch.sort}" />
	    </form>
	</tiles:putAttribute>
	<tiles:putAttribute name="js">
		<!-- DGRID TABLE SCRIPT MUST BE AT BOTTOM OF THE PAGE -->
		<script src="${_contextpath}/admin-responsive-static/assets/plugins/dojo/dojo.js" data-dojo-config="async: true"></script>
		<script>
			//Table Data Sanitization
			var data = [];
			<c:forEach items="${model.promotions.pageList}" var="promo" varStatus="status">
				data.push({
					id: '${promo.promoId}',
					offset: '${status.count + model.promotions.firstElementOnPage}.',
					<c:choose>
						<c:when test="${promo.percent == true}">
							discount: '${promo.discount}%',
						</c:when>
						<c:when test="${promo.percent == false}">
							discount: '${promo.discount}',
						</c:when>
					</c:choose>
					minOrder: decodeHTML('<fmt:message key="${siteConfig[\'CURRENCY\'].value}" />') + '${promo.minOrder}',
					promoDate: '<fmt:formatDate type="date" timeZone="${siteConfig[\'SITE_TIME_ZONE\'].value}" dateStyle="full" value="${promo.startDate}" pattern="MM/dd/yyyy (hh:mm)a zz "/> - <fmt:formatDate type="date" timeZone="${siteConfig[\'SITE_TIME_ZONE\'].value}" dateStyle="full" value="${promo.endDate}" pattern="MM/dd/yyyy (hh:mm)a zz"/>',
					<c:if test="${promo.inEffect}">
						inEffect: true,
					</c:if>
					rank: '${promo.rank}',
				});
			</c:forEach>

			//Columns Definitions
			require(['dojo/_base/lang',
					'dojo/_base/declare',
					'dstore/Memory',
					'dstore/Trackable',
					'dgrid/OnDemandGrid',
					'dgrid/Editor',
					'dgrid/extensions/ColumnHider',
					'dgrid/extensions/ColumnReorder',
					'dgrid/extensions/ColumnResizer',
					'dgrid/Selector',
					'dijit/form/DropDownButton',
					'dijit/DropDownMenu',
					'dijit/MenuItem'
				], function (lang, declare, Memory, Trackable, OnDemandGrid, Editor, ColumnHider, ColumnReorder, ColumnResizer, Selector, DropDownButton, DropDownMenu, MenuItem) {
				var columns = {
					batch: {
						selector: 'checkbox',
						unhidable: true
					},
					offset: {
						label: '#',
						get: function(object){
							return object.offset;
						},
						sortable: false,
						unhidable: true
					},
					discount: {
						label: '<fmt:message key="discount" />',
						get: function(object){
							return object.discount;
						},
						sortable: false,
					},
					minOrder: {
						label: '<fmt:message key="minOrder" />',
						get: function(object){
							return object.minOrder;
						},
						sortable: false,
					},
					promoDate: {
						label: '<fmt:message key="promoDate" />',
						get: function(object){
							return object.promoDate;
						},
						sortable: false,
					},
					inEffect: {
						label: '<fmt:message key="inEffect" />',
						renderCell: function(object){
							var div = document.createElement("div");
							if(object.inEffect){
								var i = document.createElement("i");
								i.className = 'sli-check';
								div.appendChild(i);
							}
							return div;
						},
						sortable: false,
					},
					rank: {
						label: '<fmt:message key="rank" />',
						editor: "text",
					},
				};
				var store = new (declare([Memory, Trackable]))({
					data: data,
					idProperty:"id"
				});
				
				var grid = new (declare([OnDemandGrid, Editor, ColumnHider, ColumnReorder, ColumnResizer, Selector]))({
					collection: store,
					selectionMode: "none",
					allowSelectAll: true,
					columns: columns
				},'grid');

				//sorting columns
				grid.on("dgrid-sort", lang.hitch(this, function(e){
					e.preventDefault();						
					var property = e.sort[0].property;
				    var order = e.sort[0].descending ? "desc" : "asc";
				    document.getElementById('sort').value = property + ' ' + order;
				    document.getElementById('list').submit();
				}));
				
				<c:if test="${promoSearch.sort == 'rank desc'}">
					grid.updateSortArrow([{property: 'rank', descending: true}],true);
				</c:if>
				<c:if test="${promoSearch.sort == 'rank asc'}">
					grid.updateSortArrow([{property: 'rank', ascending: true}],true);
				</c:if>

				grid.on('dgrid-refresh-complete', function(event) {
					$('#grid-hider-menu').appendTo('#column-hider');
				});
				
				grid.startup();
			});
		</script>
	</tiles:putAttribute>
</tiles:insertDefinition>
 