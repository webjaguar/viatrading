<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ taglib uri="http://www.springframework.org/security/tags" prefix="sec" %>
<%@ taglib uri="/WEB-INF/tlds/spring.tld" prefix="spring" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>

<tiles:insertDefinition name="admin.responsive-template.promos.giftCard" flush="true"> 
	<tiles:putAttribute name="title"  value="Sales Tags Manager" />
	<tiles:putAttribute name="content" type="string">
  		<!-- Begin: Content -->
		<section id="content" class="animated fadeIn"> 
		    <!-- Begin: Orders Table -->
		    <form action="giftCardList.jhtm" method="post">
				<c:choose>
			  		<c:when test="${!empty model.giftCards}">
					    <div class="panel panel-default">  
			                <div class="panel-menu">
			                    <table class="fluid-width">
			                        <tbody>
			                        <tr> 
			                            <td class="pageShowing pr-15"> 
											<c:if test="${model.giftCards.nrOfElements > 0}">
												<fmt:message key="showing">
													<fmt:param value="${model.giftCards.firstElementOnPage + 1}" />
													<fmt:param value="${model.giftCards.lastElementOnPage + 1}" />
													<fmt:param value="${model.giftCards.nrOfElements}" />
												</fmt:message> 
											</c:if>
			                            </td>
			                            <td>
			                                <table class="pull-right">
			                                    <tbody>
			                                    <tr>
			                                        <td class="pageNumber pr-15 hidden-xs">
			                                            <table>
			                                                <tbody>
			                                                <tr>
			                                                    <td class="pr-5">Page</td>
			                                                    <td class="w-75">
			                                                    	<select name="page" id="page" onchange="submit()" class="form-control input-sm" title="Page Number">
																		<c:forEach begin="1" end="${model.giftCards.pageCount}"
																			var="page">
																			<option value="${page}"
																				<c:if test="${page == (model.giftCards.page+1)}">selected</c:if>>
																				${page}
																			</option>
																		</c:forEach>
																	</select>  
			                                                    </td>
			                                                    <td class="pl-5">of <c:out value="${model.giftCards.pageCount}" />  </td>
			                                                </tr>
			                                                </tbody>
			                                            </table>
			                                        </td> 
			                                        <td class="pageSize pr-15 w-150 hidden-xs"> 
			                                            <select name="size" onchange="document.getElementById('page').value=1;submit()" class="form-control input-sm" title="Page Size">
															<c:forTokens items="10,25,50" delims="," var="current">
																<option value="${current}"
																	<c:if test="${current == model.giftCards.pageSize}">selected</c:if>>
																	${current}
																	<fmt:message key="perPage" />
																</option>
															</c:forTokens>
														</select>
			                                        </td>
			                                        <td class="pageNav"> 
			                                            <div class="btn-group btn-group-sm pull-right">
				                                            <c:if test="${model.giftCards.firstPage}">
														 		<a href="javascript:void(0);" title="Previous" class="btn btn-dark">
			                                                    	<span class="fa fa-chevron-left"></span>
			                                                	</a>
															</c:if>
															<c:if test="${not model.giftCards.firstPage}">
																<a class="btn btn-dark" href="<c:url value="giftCardList.jhtm"><c:param name="page" value="${model.giftCards.page}"/><c:param name="size" value="${model.giftCards.pageSize}"/></c:url>" >
																	 <span class="fa fa-chevron-left"></span>
																</a>
															</c:if> 
															<c:if test="${model.giftCards.lastPage}">
															 	<a href="javascript:void(0);" title="Next" class="btn btn-dark">
															 		<span class="fa fa-chevron-right"></span>
																</a>
															</c:if>
															<c:if test="${not model.giftCards.lastPage}">
																<a class="btn btn-dark"href="<c:url value="giftCardList.jhtm"><c:param name="page" value="${model.giftCards.page+2}"/><c:param name="size" value="${model.giftCards.pageSize}"/></c:url>">
																	<span class="fa fa-chevron-right"></span>
																</a>
															</c:if>
														</div>
			                                        </td>
			                                    </tr>
			                                    </tbody>
			                                </table>
			                            </td>
			                        </tr>
			                        </tbody>
			                    </table>
			                </div> 
			                <!-- Table Content -->
		                    <table class="responsiveBootstrapFooTable table table-hover" data-sorting="true">
		                        <thead>
		                        <tr class="bg-light">
		                            <th class="text-nowrap w-50" data-type="html" data-sortable="false">#</th>
		                            <th class="text-nowrap w-75" data-type="html" data-sortable="false"> 
		                            	<fmt:message key="claimCode" />
		                           	</th> 
		                            <th class="text-nowrap " data-type="html" data-breakpoints="xs">
		                               <fmt:message key="amount" />
		                            </th> 
		                            <th class="text-nowrap " data-type="html" data-breakpoints="xs sm">
		                                <fmt:message key="dateCreated" />
		                            </th>
		                            <th class="text-nowrap " data-type="html" data-breakpoints="xs sm">
		                               <fmt:message key="active" />
		                            </th> 	
		                             <th class="text-nowrap " data-type="html" data-breakpoints="xs sm">
		                              <fmt:message key="amountRedeemed" />
		                            </th> 
		                             <th class="text-nowrap " data-type="html" data-breakpoints="xs sm">
		                               <fmt:message key="dateRedeemed" />
		                            </th> 
		                             <th class="text-nowrap " data-type="html" data-breakpoints="xs sm">
		                               <fmt:message key="redeemedBy" />
		                            </th>  
		                        </tr>
		                        </thead>
		                         
		                        <tbody>
			                        <c:forEach items="${model.giftCards.pageList}" var="giftCard"	varStatus="status">
				                        <tr>
				                            <td ><c:set var="claimCode" scope="page" value="${model.giftCard.nrOfElements}" />
												<c:out value="${status.count + model.giftCard.firstElementOnPage}" />.
											</td>
				                           
				                            <td>
				                            	<a href="giftCardForm.jhtm?id=${giftCard.code}"><c:out value="${giftCard.code}" /></a>
				                            </td>  
				                            
				                            <td>
												<fmt:message key="${siteConfig['CURRENCY'].value}" /><fmt:formatNumber value="${giftCard.amount}" pattern="#,##0.00" />
				                            </td>
				                            
				                            <td>
				                                <fmt:formatDate type="date" dateStyle="full" value="${giftCard.dateCreated}" pattern="MM/dd/yyyy"/>
			                                </td> 
											
				                            <td>
			                              		<c:choose>
												  <c:when test="${giftCard.active}"><img src="../graphics/checkbox.png" alt="" title="" border="0"></c:when>
												  <c:otherwise><img src="../graphics/box.png" alt="" title="" border="0"></c:otherwise>
											 	</c:choose>
				                            </td> 
				                            
				                            <td>
				                                <fmt:message key="${siteConfig['CURRENCY'].value}" /><fmt:formatNumber value="${giftCard.amountRedeemed}" pattern="#,##0.00" />
			                                </td> 
			                                
			                                <td>
				                                <fmt:formatDate type="date" dateStyle="full" value="${giftCard.dateRedeemed}" pattern="MM/dd/yyyy"/>
			                                </td> 
			                                
			                                <td>
				                                <a href="../customers/customer.jhtm?id=${giftCard.redeemedById}" ><c:out value="${giftCard.redeemedByFirstName}" /> <c:out value="${giftCard.redeemedByLastName}" /></a>
			                                </td> 
				                        </tr>
			                        </c:forEach> 
		                        </tbody>
		                    </table> 
		                    
		                    <div class="panel-footer">
				                <div class="clearfix">  
				                   <div class="pull-right"> 
					                </div>
				                </div>
				            </div>  
					    </div>
				    </c:when>
				    <c:otherwise>
						<div class="panel panel-default">  
			                <div class="panel-menu">
			                    <table class="fluid-width">
			                        <tbody>
			                        <tr> 
			                            <td class="pageShowing pr-15"> 
											<c:if test="${model.giftCardOrders.nrOfElements > 0}">
												<fmt:message key="showing">
													<fmt:param value="${model.giftCardOrders.firstElementOnPage + 1}" />
													<fmt:param value="${model.giftCardOrders.lastElementOnPage + 1}" />
													<fmt:param value="${model.giftCardOrders.nrOfElements}" />
												</fmt:message> 
											</c:if>
			                            </td>
			                            <td>
			                                <table class="pull-right">
			                                    <tbody>
			                                    <tr>
			                                        <td class="pageNumber pr-15 hidden-xs">
			                                            <table>
			                                                <tbody>
			                                                <tr>
			                                                    <td class="pr-5">Page</td>
			                                                    <td class="w-75">
			                                                    	<select name="page" id="page" onchange="submit()" class="form-control input-sm" title="Page Number">
																		<c:forEach begin="1" end="${model.giftCardOrders.pageCount}"
																			var="page">
																			<option value="${page}"
																				<c:if test="${page == (model.giftCardOrders.page+1)}">selected</c:if>>
																				${page}
																			</option>
																		</c:forEach>
																	</select>  
			                                                    </td>
			                                                    <td class="pl-5">of <c:out value="${model.giftCardOrders.pageCount}" />  </td>
			                                                </tr>
			                                                </tbody>
			                                            </table>
			                                        </td> 
			                                        <td class="pageSize pr-15 w-150 hidden-xs"> 
			                                            <select name="size" onchange="document.getElementById('page').value=1;submit()" class="form-control input-sm" title="Page Size">
															<c:forTokens items="10,25,50" delims="," var="current">
																<option value="${current}"
																	<c:if test="${current == model.giftCardOrders.pageSize}">selected</c:if>>
																	${current}
																	<fmt:message key="perPage" />
																</option>
															</c:forTokens>
														</select>
			                                        </td>
			                                        <td class="pageNav"> 
			                                            <div class="btn-group btn-group-sm pull-right">
				                                            <c:if test="${model.giftCardOrders.firstPage}">
														 		<a href="javascript:void(0);" title="Previous" class="btn btn-dark">
			                                                    	<span class="fa fa-chevron-left"></span>
			                                                	</a>
															</c:if>
															<c:if test="${not model.giftCardOrders.firstPage}">
																<a class="btn btn-dark" href="<c:url value="giftCardList.jhtm"><c:param name="page" value="${model.giftCardOrders.page}"/><c:param name="size" value="${model.giftCardOrders.pageSize}"/></c:url>" >
																	 <span class="fa fa-chevron-left"></span>
																</a>
															</c:if> 
															<c:if test="${model.giftCardOrders.lastPage}">
															 	<a href="javascript:void(0);" title="Next" class="btn btn-dark">
															 		<span class="fa fa-chevron-right"></span>
																</a>
															</c:if>
															<c:if test="${not model.giftCardOrders.lastPage}">
																<a class="btn btn-dark"href="<c:url value="giftCardList.jhtm"><c:param name="page" value="${model.giftCardOrders.page+2}"/><c:param name="size" value="${model.giftCardOrders.pageSize}"/></c:url>">
																	<span class="fa fa-chevron-right"></span>
																</a>
															</c:if>
														</div>
			                                        </td>
			                                    </tr>
			                                    </tbody>
			                                </table>
			                            </td>
			                        </tr>
			                        </tbody>
			                    </table>
			                </div> 
			                <!-- Table Content -->
		                    <table class="responsiveBootstrapFooTable table table-hover" data-sorting="true">
		                        <thead>
		                        <tr class="bg-light">
		                            <th class="text-nowrap w-50" data-type="html" data-sortable="false">#</th>
		                            <th class="text-nowrap w-75" data-type="html" data-sortable="false"> 
		                            	<fmt:message key="orderNumber" />
		                           	</th> 
		                            <th class="text-nowrap " data-type="html" data-breakpoints="xs">
		                               <fmt:message key="orderDate" />
		                            </th> 
		                            <th class="text-nowrap " data-type="html" data-breakpoints="xs sm">
		                               <fmt:message key="subTotal" />
		                            </th>
		                            <th class="text-nowrap " data-type="html" data-breakpoints="xs sm">
		                               <fmt:message key="numberOfGiftCard" />
		                            </th> 	
		                             <th class="text-nowrap " data-type="html" data-breakpoints="xs sm">
		                              <fmt:message key="paymentMethod" />
		                            </th>  
		                        </tr>
		                        </thead>
		                         
		                        <tbody>
			                        <c:forEach items="${model.giftCardOrders.pageList}" var="giftCardOrder"	varStatus="status">
				                        <tr>
				                            <td ><c:set var="claimCode" scope="page" value="${model.giftCardOrders.nrOfElements}" />
												<c:out value="${status.count + model.giftCardOrders.firstElementOnPage}" />.
											</td>
				                           
				                            <td>
				                            	<a href="giftCardList.jhtm?giftcardOrderId=${giftCardOrder.giftCardOrderId}"><c:out value="${giftCardOrder.giftCardOrderId}" /></a>
				                            </td>  
				                            
				                            <td>
												<fmt:formatDate type="date" timeStyle="default" value="${giftCardOrder.dateCreated}"/>
				                            </td>
				                            
				                            <td>
				                                <fmt:message key="${siteConfig['CURRENCY'].value}" /><fmt:formatNumber value="${giftCardOrder.subTotal}" pattern="#,##0.00" />
			                                </td> 
											
				                            <td>
			                              		<c:out value="${giftCardOrder.totalQuantity}" />
				                            </td> 
				                            
				                            <td>
				                                <c:choose>
													<c:when test="${giftCardOrder.paymentMethod != 'Credit Card'}">
														<c:out value="${giftCardOrder.paymentMethod}"/>
													</c:when>
													<c:otherwise>								
														<fmt:message key='${giftCardOrder.creditCard.type}' />
													</c:otherwise>
												</c:choose>
			                                </td>  
				                        </tr>
			                        </c:forEach> 
		                        </tbody>
		                    </table> 
		                    
		                    <div class="panel-footer">
				                <div class="clearfix"> 
				                    
				                   <div class="pull-right">
					                   
					                </div>
				                </div>
				            </div>  
					    </div> 
				    </c:otherwise>
			    </c:choose>
		    </form>
		    <!-- End: Orders Table -->
		</section>
		<!-- End: Content -->
  
	</tiles:putAttribute>
</tiles:insertDefinition>
 