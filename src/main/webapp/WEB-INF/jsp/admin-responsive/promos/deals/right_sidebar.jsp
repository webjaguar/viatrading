<%@ page import="com.webjaguar.model.*,java.util.*"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/security/tags" prefix="sec" %>



<!-- Start: Right Sidebar -->
<aside id="sidebar_right" class="nano affix">
    <!-- Start: Sidebar Right Content -->
    <div class="sidebar-right-content nano-content">
        <div class="tab-block sidebar-block br-n">
            <ul class="nav nav-tabs tabs-border nav-justified">
                <li class="active">
                    <a href="javascript:void(0);" data-toggle="tab" data-target="#sidebar-right-tab1">Search</a>
                </li>
                
                <c:if test="${gSiteConfig['gDEALS'] > 0 and param.tab == 'dealsTab'}">	
                	 <form action="dealList.jhtm" method="post">
                	 	<div class="tab-content br-n">
		                    <div id="sidebar-right-tab1" class="tab-pane active">
		                    	<div class="form-group">
		                            <label for="firstName"><fmt:message key="view" />:</label>  
							      	 <select name="_viewtype" class="form-control input-sm" >
								        <option value="" <c:if test="${dealSearch.viewType == ''}">selected</c:if>><fmt:message key="all"/></option>
								        <option value="sku" <c:if test="${dealSearch.viewType == 'sku'}">selected</c:if>><fmt:message key="sku"/></option>
								        <option value="subTotal" <c:if test="${dealSearch.viewType == 'subTotal'}">selected</c:if>><fmt:message key="subTotal"/></option>  
								    </select>
		                        </div> 
		                        
		                        <input type="submit" value="Search" class="btn btn-sm btn-primary btn-block"/>  
	                        </div>
                     	</div>            
                	 </form> 
                </c:if>
      		</ul>
  		</div>	
	</div>
</aside>
                