<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ taglib uri="http://www.springframework.org/security/tags" prefix="sec" %>
<%@ taglib uri="/WEB-INF/tlds/spring.tld" prefix="spring" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>

<tiles:insertDefinition name="admin.responsive-template.promos.deals" flush="true">  
	<tiles:putAttribute name="content" type="string">
  		<!-- Begin: Content -->
		<section id="content" class="animated fadeIn"> 
		    <!-- Begin: Orders Table -->
		    <form action="dealList.jhtm" method="post"> 
			    <div class="panel panel-default">  
	                <div class="panel-menu">
	                    <table class="fluid-width">
	                        <tbody>
	                        <tr> 
	                            <td class="pageShowing pr-15"> 
									<c:if test="${model.deals.nrOfElements > 0}">
										<fmt:message key="showing">
											<fmt:param value="${model.deals.firstElementOnPage + 1}" />
											<fmt:param value="${model.deals.lastElementOnPage + 1}" />
											<fmt:param value="${model.deals.nrOfElements}" />
										</fmt:message> 
									</c:if>
	                            </td>
	                            <td>
	                                <table class="pull-right">
	                                    <tbody>
	                                    <tr>
	                                        <td class="pageNumber pr-15 hidden-xs">
	                                            <table>
	                                                <tbody>
	                                                <tr>
	                                                    <td class="pr-5">Page</td>
	                                                    <td class="w-75">
	                                                    	<select name="page" id="page" onchange="submit()" class="form-control input-sm" title="Page Number">
																<c:forEach begin="1" end="${model.deals.pageCount}"
																	var="page">
																	<option value="${page}"
																		<c:if test="${page == (model.deals.page+1)}">selected</c:if>>
																		${page}
																	</option>
																</c:forEach>
															</select>  
	                                                    </td>
	                                                    <td class="pl-5">of <c:out value="${model.deals.pageCount}" />  </td>
	                                                </tr>
	                                                </tbody>
	                                            </table>
	                                        </td> 
	                                        <td class="pageSize pr-15 w-150 hidden-xs"> 
	                                            <select name="size" onchange="document.getElementById('page').value=1;submit()" class="form-control input-sm" title="Page Size">
													<c:forTokens items="10,25,50" delims="," var="current">
														<option value="${current}"
															<c:if test="${current == dealSearch.pageSize}">selected</c:if>>
															${current}
															<fmt:message key="perPage" />
														</option>
													</c:forTokens>
												</select>
	                                        </td>
	                                        <td class="pageNav"> 
	                                            <div class="btn-group btn-group-sm pull-right">
		                                            <c:if test="${model.deals.firstPage}">
												 		<a href="javascript:void(0);" title="Previous" class="btn btn-dark">
	                                                    	<span class="fa fa-chevron-left"></span>
	                                                	</a>
													</c:if>
													<c:if test="${not model.deals.firstPage}">
														<a class="btn btn-dark" href="<c:url value="dealList.jhtm"><c:param name="page" value="${model.deals.page}"/><c:param name="size" value="${model.deals.pageSize}"/></c:url>" >
															 <span class="fa fa-chevron-left"></span>
														</a>
													</c:if> 
													<c:if test="${model.deals.lastPage}">
													 	<a href="javascript:void(0);" title="Next" class="btn btn-dark">
													 		<span class="fa fa-chevron-right"></span>
														</a>
													</c:if>
													<c:if test="${not model.deals.lastPage}">
														<a class="btn btn-dark"href="<c:url value="dealList.jhtm"><c:param name="page" value="${model.deals.page+2}"/><c:param name="size" value="${model.deals.pageSize}"/></c:url>">
															<span class="fa fa-chevron-right"></span>
														</a>
													</c:if>
												</div>
	                                        </td>
	                                    </tr>
	                                    </tbody>
	                                </table>
	                            </td>
	                        </tr>
	                        </tbody>
	                    </table>
	                </div> 
	                <!-- Table Content -->
                    <table class="responsiveBootstrapFooTable table table-hover" data-sorting="true">
                        <thead>
                        <tr class="bg-light">
                            <th class="text-nowrap w-50" data-type="html" data-sortable="false">#</th>
                            <th class="text-nowrap w-75" data-type="html" data-sortable="false"> 
                            	<fmt:message key="title" />
                           	</th> 
                            <th class="text-nowrap " data-type="html" data-breakpoints="xs">
                               <fmt:message key="buy" />
                            </th> 
                            <th class="text-nowrap " data-type="html" data-breakpoints="xs sm">
                               <fmt:message key="get" />
                            </th>
                            <th class="text-nowrap " data-type="html" data-breakpoints="xs sm">
                               <fmt:message key="discount" />
                            </th> 	
                            <th class="text-nowrap " data-type="html" data-breakpoints="xs sm">
                               <fmt:message key="dealDate" />
                            </th> 
                            <th class="text-nowrap " data-type="html" data-breakpoints="xs sm">
                               <fmt:message key="inEffect" />
                            </th>   
                        </tr>
                        </thead>
                         
                        <tbody>
	                        <c:forEach items="${model.deals.pageList}" var="deal"	varStatus="status">
		                        <tr>
		                            <td >
		                            	<c:out value="${status.count + model.deals.firstElementOnPage}" />.
									</td>
		                           
		                            <td>
		                            	<a href="dealOnSku.jhtm?id=${deal.dealId}">
		                            		<c:out value="${deal.title}" />
										</a>
		                            </td>  
		                            
		                            <td>
										<c:choose><c:when test="${deal.buySku != null}">${deal.buySku}</c:when><c:otherwise><fmt:message key="dollar"/>${deal.buyAmount}</c:otherwise> </c:choose> 
		                            </td>
		                            
		                            <td>
		                                <c:out value="${deal.getSku}"></c:out>
	                                </td> 
									
		                            <td>
	                             		<c:out value="${deal.discount}"></c:out> <c:choose><c:when test="${deal.discountType}">%</c:when><c:otherwise>USD</c:otherwise> </c:choose>
		                            </td>
		                            
		                            <td>
		                              <fmt:formatDate type="date" dateStyle="full" value="${deal.startDate}" pattern="MM/dd/yyyy (hh:mm)a zz "/> - <fmt:formatDate type="date" dateStyle="full" value="${deal.endDate}" pattern="MM/dd/yyyy (hh:mm)a zz"/> 
	                                </td> 
	                                
	                                <td>
		                                <c:if test="${deal.inEffect}"><div align="left"><img src="../graphics/checkbox.png" /></div></c:if>
	                                </td> 
		                        </tr>
	                        </c:forEach> 
                        </tbody>
                    </table> 
                    
                    <div class="panel-footer">
		                <div class="clearfix">
		                    <div class="pull-left">
		                    	<c:if test="${model.nOfdeals > 0}">
									<div class="pull-left mb-xs-15">
										<p class="text-danger mb-0">	
										<sup>1</sup> <fmt:message key="toEditClickDealTitle" /> 
										</p>
									</div>
								</c:if> 
		                    </div>
		                    
		                   <div class="pull-right">
			                   	<c:choose>
									<c:when test="${gSiteConfig['gDEALS'] > model.nOfdeals}">
									  <sec:authorize ifAnyGranted="ROLE_AEM,ROLE_ADMIN_${prop_site_id},ROLE_PROMOTION_CREATE">
										<input class="btn btn-primary mr-10" type="submit" name="__addOnSku" value="<fmt:message key="dealAddOnSku" />"/> &nbsp;
										<input class="btn btn-primary mr-10" type="submit" name="__addOnSubTotal" value="<fmt:message key="dealAddOnSubTotal" />"/>
									  </sec:authorize>	
									</c:when>
									<c:otherwise>
									  <div class="note_list">
										<fmt:message key="toAddMoreDealContactWebMaster" />
									  </div>	
									</c:otherwise>
								</c:choose>	 
			                </div>
		                </div>
		            </div>  
			    </div>
		    </form>
		    <!-- End: Orders Table -->
		</section>
		<!-- End: Content -->
  
	</tiles:putAttribute>
</tiles:insertDefinition>
 