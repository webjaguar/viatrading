<%@ page import="java.net.*" %>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/security/tags" prefix="sec" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ taglib uri="/WEB-INF/tlds/custom.tld" prefix="wj" %>

<tiles:insertDefinition name="admin.responsive-template.promos.salestags" flush="true"> 
	<tiles:putAttribute name="css">
		<style>
			#grid .field-discount{
				text-align: right;
			}
			#grid .field-salesTagDate{
				width: 350px;
			}
			#grid .field-inEffect{
				text-align: center;
			}
		</style>
	</tiles:putAttribute>
	<tiles:putAttribute name="content" type="string">
	    <form action="salesTagList.jhtm" method="post">
			<div class="page-banner">
				<div class="breadcrumb"><span><a href="${_contextpath}/admin/catalog/dashboard.jhtm">Home</a></span><i class="sli-arrow-right"></i><span><a href="${_contextpath}/admin/promos">Sales/Promos</a></span><i class="sli-arrow-right"></i><span>Sales Tags</span></div>
			</div>
			<div class="page-head">
				<div class="row justify-content-between">
					<div class="col-sm-8 title">
						<h3>Sales Tags</h3>
					</div>
					<div class="col-sm-4 actions">
						<div class="dropdown dropdown-actions">
							<button type="button" class="btn-outline dropdown-toggle active btn btn-primary" data-toggle="dropdown">
	                             <i class="sli-settings"></i> Actions
							</button>
							<div class="dropdown-menu dropdown-menu-right">
								<c:if test="${gSiteConfig['gNUMBER_SALES_TAG'] > model.nOfsalesTags}">
									<sec:authorize ifAnyGranted="ROLE_AEM,ROLE_ADMIN_${prop_site_id},ROLE_SALES_TAG_CREATE">
										<a class="dropdown-item"><fmt:message key="salesTagAdd" /></a>
									</sec:authorize>	
								</c:if>
							</div>
	                    </div>   
	                    <div class="dropdown dropdown-setting">
							<button type="button" class="btn-outline dropdown-toggle active btn btn-primary" data-toggle="dropdown">
	                             <i class="sli-screen-desktop"></i> Display
	                         </button>	                         
							<div class="dropdown-menu dropdown-menu-right">
								<div class="form-group">
			                    		<div class="option-header">Rows</div>
									<select class="custom-select form-control" name="size" onchange="document.getElementById('page').value=1;submit();">
										<c:forTokens items="10,25,50,100,200,500" delims="," var="current">
								  	  		<option value="${current}" <c:if test="${current == salesTagSearch.pageSize}">selected</c:if>>${current}</option>
							  			</c:forTokens>
									</select>
								</div>
								<div class="form-group">
			                    		<div class="option-header">Columns</div>
			                    		<div id="column-hider"></div>
								</div>
							</div>
	                    </div>
					</div>
				</div>
			</div>
			<div class="page-body">
				<div class="grid-stage">
					<c:if test="${model.salesTags.nrOfElements > 0}">
						<div class="page-stat">
							<fmt:message key="showing">
								<fmt:param value="${model.salesTags.firstElementOnPage + 1}" />
								<fmt:param value="${model.salesTags.lastElementOnPage + 1}" />
								<fmt:param value="${model.salesTags.nrOfElements}" />
							</fmt:message>
						</div>
					</c:if>
				    <table id="grid"></table>
				    <div class="footer">
						<div class="float-left">
							<c:if test="${model.nOfsalesTags > 0}">
								<div class="pull-left mb-xs-15">
									<p class="mb-0"><sup>1</sup> <fmt:message key="toEditClickSalesTagCode" /></p>
								</div>
							</c:if> 
						</div>
		                <div class="float-right">
							<c:if test="${model.salesTags.firstPage}">
						 		<span class="previous"><i class="sli-arrow-left" style="font-size: 0.7rem"></i> Previous</span>
							</c:if>
							<c:if test="${not model.salesTags.firstPage}">
								<a href="<c:url value="salesTagList.jhtm"><c:param name="page" value="${model.salesTags.page}"/><c:param name="size" value="${model.salesTags.pageSize}"/></c:url>">
									<span class="previous"><i class="sli-arrow-left" style="font-size: 0.7rem"></i> Previous</span>
								</a>
							</c:if>	
							<span class="status"><c:out value="${model.salesTags.page+1}" /> of <c:out value="${model.salesTags.pageCount}" /></span>
							<c:if test="${model.salesTags.lastPage}">
								<span class="next">Next <i class="sli-arrow-right" style="font-size: 0.7rem"></i></span>
							</c:if>
							<c:if test="${not model.salesTags.lastPage}">
								<a href="<c:url value="salesTagList.jhtm"><c:param name="page" value="${model.salesTags.page+2}"/><c:param name="size" value="${model.salesTags.pageSize}"/></c:url>">
									<span class="next">Next <i class="sli-arrow-right" style="font-size: 0.7rem"></i></span>
								</a>
							</c:if>
						</div>
					</div>
				</div>
			</div>
		</form>
	</tiles:putAttribute>
	<tiles:putAttribute name="js">
		<!-- DGRID TABLE SCRIPT MUST BE AT BOTTOM OF THE PAGE -->
		<script src="${_contextpath}/admin-responsive-static/assets/plugins/dojo/dojo.js" data-dojo-config="async: true"></script>
		<script>
			//Table Data Sanitization
			var data = [];
			<c:forEach items="${model.salesTags.pageList}" var="salesTag"	varStatus="status">
				data.push({
					id: '${salesTag.tagId}',
					offset: '${status.count + model.salesTags.firstElementOnPage}.',
					code: '${wj:escapeJS(salesTag.title)}',
					<c:choose>
						<c:when test="${salesTag.percent == true}">
							discount: '${salesTag.discount}%',
						</c:when>
						<c:when test="${salesTag.percent == false}">
							discount: '${salesTag.discount}',
						</c:when>
					</c:choose>
					salesTagDate: '<fmt:formatDate type="date" dateStyle="full" value="${salesTag.startDate}" pattern="MM/dd/yyyy (hh:mm)a zz"/> - <fmt:formatDate type="date" dateStyle="full" value="${salesTag.endDate}" pattern="MM/dd/yyyy (hh:mm)a zz"/>',
					<c:if test="${salesTag.inEffect}">
						inEffect: true,
					</c:if>
				});
			</c:forEach>

			//Columns Definitions
			require(['dojo/_base/lang',
					'dojo/_base/declare',
					'dstore/Memory',
					'dstore/Trackable',
					'dgrid/OnDemandGrid',
					'dgrid/Editor',
					'dgrid/extensions/ColumnHider',
					'dgrid/extensions/ColumnReorder',
					'dgrid/extensions/ColumnResizer',
					'dgrid/Selector',
					'dijit/form/DropDownButton',
					'dijit/DropDownMenu',
					'dijit/MenuItem'
				], function (lang, declare, Memory, Trackable, OnDemandGrid, Editor, ColumnHider, ColumnReorder, ColumnResizer, Selector, DropDownButton, DropDownMenu, MenuItem) {
				var columns = {
					batch: {
						selector: 'checkbox',
						unhidable: true
					},
					offset: {
						label: '#',
						get: function(object){
							return object.offset;
						},
						sortable: false,
						unhidable: true
					},
					code: {
						label: '<fmt:message key="salesTag" />',
						renderCell: function(object){
							var link = document.createElement('a');
							link.className = "plain";
							link.href = "salesTag.jhtm?id="+object.id;
							link.textContent = object.code;
							return link;
						},
						sortable: false,
					},
					discount: {
						label: '<fmt:message key="discount" />',
						get: function(object){
							return object.discount;
						},
						sortable: false,
					},
					salesTagDate: {
						label: '<fmt:message key="salesTagDate" />',
						get: function(object){
							return object.salesTagDate;
						},
						sortable: false,
					},
					salesTagDate: {
						label: '<fmt:message key="salesTagDate" />',
						get: function(object){
							return object.salesTagDate;
						},
						sortable: false,
					},
					inEffect: {
						label: '<fmt:message key="inEffect" />',
						renderCell: function(object){
							var div = document.createElement("div");
							if(object.inEffect){
								var i = document.createElement("i");
								i.className = 'sli-check';
								div.appendChild(i);
							}
							return div;
						},
						sortable: false,
					},
				};
				var store = new (declare([Memory, Trackable]))({
					data: data,
					idProperty:"id",
					code: 'salesTag.title'
				});
				
				var grid = new (declare([OnDemandGrid, Editor, ColumnHider, ColumnReorder, ColumnResizer, Selector]))({
					collection: store,
					selectionMode: "none",
					allowSelectAll: true,
					columns: columns
				},'grid');
				
				//sorting columns
				grid.on("dgrid-sort", lang.hitch(this, function(e){
					e.preventDefault();						
					var property = e.sort[0].property;
				    var order = e.sort[0].descending ? "desc" : "asc";
				    document.getElementById('sort').value = property + ' ' + order;
				    document.getElementById('list').submit();
				}));
				
				grid.on('dgrid-refresh-complete', function(event) {
					$('#grid-hider-menu').appendTo('#column-hider');
				});
				
				grid.startup();
			});
		</script>
	</tiles:putAttribute>
</tiles:insertDefinition>
 