<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>

<select class="custom-select" id="messageSelected" onChange="chooseMessage(this)">
	<option value="">Choose a message</option>
	<c:forEach var="message" items="${model.siteMessagesList}">
		<option value="${message.messageId}"><c:out value="${message.messageName}"/></option>
	</c:forEach>
</select>
			   	