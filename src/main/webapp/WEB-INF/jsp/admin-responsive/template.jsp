<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>

<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width,initial-scale=1,shrink-to-fit=no">
	<link rel="shortcut icon" href="${_contextpath}/admin-responsive-static/assets/favicon/favicon-32x32.png">
	<link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Inconsolata:400,700%7COpen+Sans:400,400i,600,700">
	<link rel="stylesheet" href="${_contextpath}/admin-responsive-static/assets/plugins/animate.css/animate.min.css">
	<link rel="stylesheet" href="${_contextpath}/admin-responsive-static/assets/plugins/font-awesome/css/font-awesome.min.css">
	<link rel="stylesheet" href="${_contextpath}/admin-responsive-static/assets/plugins/weather-icons/css/weather-icons.min.css">
	<link rel="stylesheet" href="${_contextpath}/admin-responsive-static/assets/plugins/weather-icons/css/weather-icons-wind.min.css">
	<link rel="stylesheet" href="${_contextpath}/admin-responsive-static/assets/plugins/ionicons/css/ionicons.min.css">
	<link rel="stylesheet" href="${_contextpath}/admin-responsive-static/assets/plugins/dijit/themes/claro/claro.css">
	<link rel="stylesheet" href="${_contextpath}/admin-responsive-static/assets/plugins/dgrid/css/dgrid.css">
	<link rel="stylesheet" href="${_contextpath}/admin-responsive-static/assets/plugins/dgrid/css/skins/claro.css">
	<link rel="stylesheet" href="${_contextpath}/admin-responsive-static/assets/plugins/bootstrap-datetimepicker/css/bootstrap-datetimepicker.css">
	<link rel="stylesheet" href="${_contextpath}/admin-responsive-static/assets/plugins/evol-colorpicker/css/evol-colorpicker.min.css" />
	<link rel="stylesheet" href="${_contextpath}/admin-responsive-static/assets/css/default-form.css">
	<link rel="stylesheet" href="${_contextpath}/admin-responsive-static/assets/css/navbar-1.css">
	<link rel="stylesheet" href="${_contextpath}/admin-responsive-static/assets/css/main.css">
	<link rel="stylesheet" href="${_contextpath}/admin-responsive-static/assets/css/custom.css">
    <title><tiles:insertAttribute name="title" flush="false" /></title>
    <tiles:insertAttribute name="css" flush="false" />
</head>
<body>
	<div id="root" data-reactroot="" data-layout="default-sidebar-1" data-background="light" data-navbar="danger" data-logo="info" data-left-sidebar="light" data-top-navigation="info" data-collapsed="false">
		<nav class="navbar navbar-1 d-flex justify-content-around align-items-center flex-nowrap">
			<a class="logo" href="${_contextpath}/admin/catalog/dashboard.jhtm">
				<img src="${_contextpath}/admin-responsive-static/assets/img/logos/logo-white.png"></img>
			</a>
			<ul class="nav nav-inline hidden-sm-down">
				<li class="nav-item">
					<a class="nav-link toggle-layout"> 
						<i class="sli-menu"></i>
					</a>
				</li>
			</ul>
			<div class="separator"></div>
			<ul class="nav nav-inline hidden-md-down">
				<div class="dropdown-flags dropdown">
					<button type="button" data-toggle="dropdown" aria-haspopup="true"
						aria-expanded="false" class="btn btn-default">
						<span class="flag flag-icon-background flag-icon flag-icon-gb"></span>
					</button>
					<div tabindex="-1" aria-hidden="true" role="menu" class="dropdown-menu-right dropdown-menu">
						<h6 tabindex="-1" class="dropdown-title dropdown-header">Countries</h6>
						<div class="dropdown-inner">
							<div class="dropdown-item">
								<a href="#">
									<img src="${_contextpath}/admin-responsive-static/assets/flags/br.png" class="mb-1 mr-2">
									<span class="country-name">Brasil</span>
								</a>
							</div>
							<div class="dropdown-item">
								<a href="#">
									<img src="${_contextpath}/admin-responsive-static/assets/flags/us.png" class="mb-1 mr-2">
									<span class="country-name">USA</span>
								</a>
							</div>
							<div class="dropdown-item">
								<a href="#">
									<img src="${_contextpath}/admin-responsive-static/assets/flags/es.png" class="mb-1 mr-2">
									<span class="country-name">Espa�a</span>
								</a>
							</div>
							<div class="dropdown-item">
								<a href="#">
									<img src="${_contextpath}/admin-responsive-static/assets/flags/cn.png" class="mb-1 mr-2">
									<span class="country-name">China</span>
								</a>
							</div>
							<div class="dropdown-item">
								<a href="#">
									<img src="${_contextpath}/admin-responsive-static/assets/flags/in.png" class="mb-1 mr-2">
									<span class="country-name">India</span>
								</a>
							</div>
							<div class="dropdown-item">
								<a href="#">
									<img src="${_contextpath}/admin-responsive-static/assets/flags/ca.png" class="mb-1 mr-2">
									<span class="country-name">Canada</span>
								</a>
							</div>
							<div class="dropdown-item">
								<a href="#">
									<img src="${_contextpath}/admin-responsive-static/assets/flags/au.png" class="mb-1 mr-2">
									<span class="country-name">Australia</span>
								</a>
							</div>
							<div class="dropdown-item">
								<a href="#">
									<img src="${_contextpath}/admin-responsive-static/assets/flags/jp.png" class="mb-1 mr-2">
									<span class="country-name">Japan</span>
								</a>
							</div>
							<div class="dropdown-item">
								<a href="#">
									<img src="${_contextpath}/admin-responsive-static/assets/flags/kr.png" class="mb-1 mr-2">
									<span class="country-name">Korea</span>
								</a>
							</div>
						</div>
					</div>
				</div>
				<div class="dropdown-flags dropdown">
					<button type="button" data-toggle="dropdown" aria-haspopup="true"
						aria-expanded="false" class="btn btn-default">
						<i class="sli-user"></i>
					</button>
					<div tabindex="-1" aria-hidden="true" role="menu" class="dropdown-menu-right animated fadeInUp dropdown-menu">
						<h6 tabindex="-1" class="dropdown-title dropdown-header">My account</h6>
						<a class="dropdown-item">
							<i class="sli-settings"></i>
							<span class="title"> Profile</span>
						</a>
						<a class="dropdown-item">
							<i class="sli-clock"></i><span class="title"> Lock screen</span>
						</a>
						<a class="dropdown-item" href="${_contextpath}/admin/logout.jsp">
							<i class="sli-power"></i><span class="title"> Logout</span>
						</a>
					</div>
				</div>
			</ul>
			<ul class="nav nav-inline">
				<li class="nav-item hidden-md-up">
					<a class="nav-link toggle-layout"><i class="sli-menu"></i></a>
				</li>
			</ul>
		</nav>
		<div class="container-fluid">
			<tiles:insertAttribute name="sidebar" flush="false" />
			<div class="main">
				<tiles:insertAttribute name="content" flush="false" />
			</div>
		</div>
		<span id="legacy">
			<a href="${_contextpath}/admin/catalog/categoryList.jhtm?newAdmin=false">[Legacy Version]</a>
		</span>
	</div>
	<script src="${_contextpath}/admin-responsive-static/assets/js/jquery/jquery.min.js"></script>
	<script src="${_contextpath}/admin-responsive-static/assets/js/jquery-ui.min.js"></script>
	<script src="${_contextpath}/admin-responsive-static/assets/js/tether.min.js"></script>
	<script src="${_contextpath}/admin-responsive-static/assets/plugins/moment/moment.min.js"></script>
	<script src="${_contextpath}/admin-responsive-static/assets/js/bootstrap.min.js"></script>
	<script src="${_contextpath}/admin-responsive-static/assets/plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js"></script>
	<script src="${_contextpath}/admin-responsive-static/assets/plugins/bootstrap-datetimepicker/js/bootstrap-datetimepicker.js"></script>
	<script src="${_contextpath}/admin-responsive-static/assets/plugins/evol-colorpicker/js/evol-colorpicker.min.js"></script>
	<script src="${_contextpath}/admin-responsive-static/assets/js/application.js"></script>
	<tiles:insertAttribute name="js" flush="false" />
</body>
</html>
