<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="/WEB-INF/tlds/spring.tld" prefix="spring" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ taglib uri="http://www.springframework.org/security/tags" prefix="sec" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ taglib uri="/WEB-INF/tlds/custom.tld" prefix="wj" %>

<tiles:insertDefinition name="admin.responsive-template.customers.groups" flush="true">
	<tiles:putAttribute name="topbar" value="/WEB-INF/jsp/admin-responsive/common/topbar.jsp?tab=customerGroupForm">
	</tiles:putAttribute>
	<tiles:putAttribute name="content" type="string">
		<sec:authorize ifAnyGranted="ROLE_AEM,ROLE_ADMIN_${prop_site_id},ROLE_CRM_GROUP">
			<!-- Begin: Content -->
			<section id="content" class="animated fadeIn"> 
			    <form:form commandName="customerGroupForm" method="post" enctype="multipart/form-data">
				 	
				  	<!-- Error Message -->
			  	  	<c:if test="${!empty message}">
						  <div class="message"><fmt:message key="${message}" /></div>
				  	</c:if>
				  	<spring:hasBindErrors name="customerGroupForm">
				       <span class="error">Please fix all errors!</span>
			      	</spring:hasBindErrors>
				 	
				 	<div class="panel panel-default">
			         	<ul class="nav panel-tabs panel-tabs-border"> 
                               <li class="active">
			                    <a href="javascript:void(0);" data-toggle="tab" data-target="#tab_1"><fmt:message key="group" /></a>
			                </li> 
			                <c:if test="${siteConfig['CUSTOMER_GROUP_GALLERY'].value == 'true' and model.customerGroup.id != null and  model.customerGroup.id > 0}">
								 <li>
				                    <a href="javascript:void(0);" data-toggle="tab" data-target="#tab_2"><fmt:message key="group" /> <fmt:message key='image' /></a>
				                </li>  
							</c:if>
							
							<c:if test="${!customerGroupForm.newCustomerGroup and gSiteConfig['gIMPRINT_OPTIONS'] > 0 and siteConfig['CUSTOMER_GROUP_IMPRINT_OPTIONS'].value == 'true'}">
								<li>
				                    <a href="javascript:void(0);" data-toggle="tab" data-target="#tab_3"><fmt:message key="imprint" /></a>
				                </li> 
							</c:if>
							
							<c:if test="${!customerGroupForm.newCustomerGroup}">
								<c:if test="${siteConfig['CUSTOMER_GROUP_MARKUP'].value == 'true'}">
									<li>
					                    <a href="javascript:void(0);" data-toggle="tab" data-target="#tab_4"><fmt:message key="markup" /></a>
					                </li> 
								</c:if>
								<c:if test="${siteConfig['CUSTOMER_GROUP_PRICE_TABLE_MARKUP'].value == 'true'}">
									<li>
					                    <a href="javascript:void(0);" data-toggle="tab" data-target="#tab_5"><fmt:message key="priceTableMarkup" /></a>
					                </li>
								</c:if>
								<c:if test="${gSiteConfig['gWILDMAN_GROUP']}">
									<li>
					                    <a href="javascript:void(0);" data-toggle="tab" data-target="#tab_6"><fmt:message key="charges" /></a>
					                </li>
								</c:if>
								<c:if test="${gSiteConfig['gWILDMAN_GROUP'] || siteConfig['GROUP_ORDER_LIMIT'].value == 'true'}">
									<li>
					                    <a href="javascript:void(0);" data-toggle="tab" data-target="#tab_7"><fmt:message key="configuration" /></a>
					                </li>
								</c:if>
								<c:if test="${siteConfig['GROUP_REWARDS'].value == 'true'}">
									<li>
					                    <a href="javascript:void(0);" data-toggle="tab" data-target="#tab_8"><fmt:message key="reward" /></a>
					                </li>
								</c:if>
								<c:if test="${siteConfig['GROUP_ORDER_LIMIT'].value == 'true'}">
									<li>
					                    <a href="javascript:void(0);" data-toggle="tab" data-target="#tab_9"><fmt:message key="orderLimit" /></a>
					                </li>
								</c:if>
								<c:if test="${gSiteConfig['gBUDGET_FREQUENCY'] > 0 and siteConfig['BUDGET_FREQUENCY_GROUP_LABELS'].value == 'true'}">
									<li>
					                    <a href="javascript:void(0);" data-toggle="tab" data-target="#tab_10"><fmt:message key="budgetFrequency" /> <fmt:message key="labels" />s</a>
					                </li>
								</c:if>
							</c:if>
							
							<c:if test="${!customerGroupForm.newCustomerGroup}">
							<c:if test="${siteConfig['CLICK_DIALING'].value == 'true'}">
								<li>
			                    	<a href="javascript:void(0);" data-toggle="tab" data-target="#tab_11">Dialing Information</a>
				                </li>
							</c:if>
							</c:if> 
			            </ul>
			            
			            <!-- Body -->
	                    <div class="panel-body">
			                <div class="tab-content">
			                    <div id="tab_1" class="tab-pane active"> 
			                        <div class="form-horizontal"> 
			                            <div class="form-group">
			                                <label for="customerGroup_active" class="col-sm-3 col-md-3 col-lg-3 control-label">    
			                                	<fmt:message key="active" />:
			                                </label>
			                               <div class="col-sm-9 col-md-8 col-lg-8">
												<p class="form-control-static">
													<form:checkbox path="customerGroup.active" id="customerGroup_active" value="true"/>
												</p>
			                                </div>
			                            </div>  
			                            
			                            <c:set var="nameErrors"><form:errors path="customerGroup.name"/></c:set> 
			                       		<div class="form-group <c:if test="${not empty nameErrors}"> has-error</c:if>">
			                                <label for="customerGroup_name" class="col-sm-3 col-md-3 col-lg-3 control-label">   
			                                	<span class="requiredField"><fmt:message key="Name" />:</span>
			                                </label>
			                               <div class="col-sm-9 col-md-8 col-lg-8">
												<form:input path="customerGroup.name" class="form-control" id="customerGroup_name" htmlEscape="true" />
										 		<form:errors path="customerGroup.name" cssClass="help-block" /> 
			                                </div> 
			                            </div>   
			                            
			                            <div class="form-group">
			                                <label for="customerGroup_companyStore" class="col-sm-3 col-md-3 col-lg-3 control-label">   
		                                	 	<fmt:message key="companyStore" />:
			                                </label>
			                               	<div class="col-sm-9 col-md-8 col-lg-8">
			                               		<p class="form-control-static">
		                               				<form:checkbox path="customerGroup.companyStore" id="customerGroup_companyStore" value="true"/>
	                               				</p>  
			                                </div>
			                            </div> 
			                            
			                            <div class="form-group">
			                                <label for="customerGroup_checkoutPurchaseOrder" class="col-sm-3 col-md-3 col-lg-3 control-label">   
		                                	 	<fmt:message key="checkoutPurchaseOrder" />:
			                                </label>
			                               	<div class="col-sm-9 col-md-8 col-lg-8">
			                               		<p class="form-control-static">
		                               				<form:checkbox path="customerGroup.checkoutPurchaseOrder"  id="customerGroup_checkoutPurchaseOrder" value="true"/>
	                               				</p>  
			                                </div>
			                            </div>  
			                            
			                            <c:if test="${fn:contains(siteConfig['USER_EXPECTED_DELIVERY_TIME'].value, 'true')}" >
				                            <div class="form-group">
				                                <label for="customerGroup_checkoutShipDateEnabled" class="col-sm-3 col-md-3 col-lg-3 control-label">   
			                                	 	<fmt:message key="checkoutShipDateEnabled" />:
				                                </label>
				                               	<div class="col-sm-9 col-md-8 col-lg-8">
				                               		<p class="form-control-static">
			                               				<form:checkbox path="customerGroup.checkoutShipDateEnabled" id="customerGroup_checkoutShipDateEnabled" value="true"/>
		                               				</p>  
				                                </div>
				                            </div>  
			                            </c:if>
			                            
			                            <c:if test="${siteConfig['REQUESTED_CANCEL_DATE'].value == 'true'}">
				                            <div class="form-group">
				                                <label for="customerGroup_checkoutCancelDateEnabled" class="col-sm-3 col-md-3 col-lg-3 control-label">   
			                                	 	<fmt:message key="checkoutCancelDateEnabled" />:
				                                </label>
				                               	<div class="col-sm-9 col-md-8 col-lg-8">
				                               		<p class="form-control-static">
				                               			<form:checkbox path="customerGroup.checkoutCancelDateEnabled"  id="customerGroup_checkoutCancelDateEnabled" value="true"/> 
		                               				</p>  
				                                </div>
				                            </div>   
			                            </c:if>
			                            
			                            <c:if test="${gSiteConfig['gWILDMAN_GROUP']}">
			                            	<div class="form-group">
				                                <label for="customerGroup_checkoutGiftcard" class="col-sm-3 col-md-3 col-lg-3 control-label">   
			                                	 	<fmt:message key="disableCheckoutGiftcard" />:
				                                </label>
				                               	<div class="col-sm-9 col-md-8 col-lg-8">
				                               		<p class="form-control-static">
				                               			<form:checkbox path="customerGroup.checkoutGiftcard" id="customerGroup_checkoutGiftcard" value="true"/> 
		                               				</p>  
				                                </div>
				                            </div>
				                            
				                            <div class="form-group">
				                                <label for="customerGroup_checkoutPromocode" class="col-sm-3 col-md-3 col-lg-3 control-label">   
			                                	 	<fmt:message key="disableCheckoutPromocode" />:
				                                </label>
				                               	<div class="col-sm-9 col-md-8 col-lg-8">
				                               		<p class="form-control-static">
				                               			<form:checkbox path="customerGroup.checkoutPromocode" id="customerGroup_checkoutPromocode" value="true"/>
		                               				</p>  
				                                </div>
				                            </div>
			                            </c:if>
			                            
			                            <div class="form-group">
			                                <label for="customerGroup_returnUrl" class="col-sm-3 col-md-3 col-lg-3 control-label">   
		                                	 	<fmt:message key="f_returnUrl" />:
			                                </label>
			                               	<div class="col-sm-9 col-md-8 col-lg-8">
			                               		<form:input path="customerGroup.returnUrl" class="form-control" id="customerGroup_returnUrl" htmlEscape="true" />
			                                </div>
			                            </div>
			                            
			                            <div class="form-group">
			                                <label for="customerGroup_showPartiallyShipOk" class="col-sm-3 col-md-3 col-lg-3 control-label">   
		                                	 	<fmt:message key="showPartiallyShipOk" />:
			                                </label>
			                               	<div class="col-sm-9 col-md-8 col-lg-8">
			                               		<p class="form-control-static">
			                               			<form:checkbox path="customerGroup.showPartiallyShipOk" id="customerGroup_showPartiallyShipOk" value="true"/>
	                               				</p>  
			                                </div>
			                            </div>
			                            
			                            <div class="form-group">
			                                <label for="customerGroup_showFrontendReport" class="col-sm-3 col-md-3 col-lg-3 control-label">   
		                                	 	<fmt:message key="showFrontendReport" />:
			                                </label>
			                               	<div class="col-sm-9 col-md-8 col-lg-8">
			                               		<p class="form-control-static">
			                               			<form:checkbox path="customerGroup.showFrontendReport" id="customerGroup_showFrontendReport" value="true"/>
	                               				</p>  
			                                </div>
			                            </div>
			                            
			                            <div class="form-group">
			                                <label for="customerGroup_priceCasePack" class="col-sm-3 col-md-3 col-lg-3 control-label">   
		                                	 	<fmt:message key="cstGrpPriceCasePack" />:
			                                </label>
			                               	<div class="col-sm-9 col-md-8 col-lg-8">
			                               		<form:select path="customerGroup.priceCasePack" class="form-control" id="customerGroup_priceCasePack" >
										          <option value="0" <c:if test="${model.customerGroup.priceCasePack == '0'}">selected='selected'</c:if>>Select Location</option>
												  <option value='1' <c:if test="${model.customerGroup.priceCasePack == '1'}">selected='selected'</c:if>>Price Case Pack 1</option>
												  <option value='2' <c:if test="${model.customerGroup.priceCasePack == '2'}">selected='selected'</c:if>>Price Case Pack 2</option>
												  <option value='3' <c:if test="${model.customerGroup.priceCasePack == '3'}">selected='selected'</c:if>>Price Case Pack 3</option>
												  <option value='4' <c:if test="${model.customerGroup.priceCasePack == '4'}">selected='selected'</c:if>>Price Case Pack 4</option>
												  <option value='5' <c:if test="${model.customerGroup.priceCasePack == '5'}">selected='selected'</c:if>>Price Case Pack 5</option>
												</form:select>	
			                                </div>
			                            </div>
			                            
			                        </div> 
			                   	</div> 
                                
                                <c:if test="${siteConfig['CUSTOMER_GROUP_GALLERY'].value == 'true' and model.customerGroup.id != null and  model.customerGroup.id > 0}"> 
									<!-- Tab2 -->
									<div id="tab_2" class="tab-pane"> 
				                        <div class="form-horizontal"> 
				                        	<c:if test="${fn:length(model.galleryFiles) gt 0}">
		 						 				<c:forEach items="${model.galleryFiles}" var="logo" varStatus="status">
		 						 					<div class="form-group">
						                                <label for="" class="col-sm-3 col-md-3 col-lg-3 control-label">   
					                                	 	<fmt:message key="image" />:
						                                </label>
						                               	<div class="col-sm-9 col-md-8 col-lg-8"> 
						                               		<c:out value="${logo.name}" /> <br>
						                               		<c:choose>
															  <c:when test="${logo.userId != null}">
																<img src="<c:url value="/assets/Image/Customer/${logo.userId}/${logo.name}"/>" border="0" height="100" alt="product image">
															  </c:when>
															  <c:when test="${logo.groupId != null}">
																<img src="<c:url value="/assets/Image/Customer/Group/${logo.groupId}/${logo.name}"/>" border="0" height="100" alt="product image">
															  </c:when>
															</c:choose>
						                                </div>
						                            </div>
						                            <div class="form-group">
						                                <label for="__logo_name_${logo.id}" class="col-sm-3 col-md-3 col-lg-3 control-label">   
					                                	 	<fmt:message key="name" />:
						                                </label>
						                               	<div class="col-sm-9 col-md-8 col-lg-8"> 
						                               		<input name="__logo_name_${logo.id}" value="${logo.name}" class="form_control" id="__logo_name_${logo.id}"  htmlEscape="true" />
						                                </div>
						                            </div>
						                            <div class="form-group">
						                                <label for="__logo_price_${logo.id}" class="col-sm-3 col-md-3 col-lg-3 control-label">   
					                                	 	<fmt:message key="price" />:
						                                </label>
						                               	<div class="col-sm-9 col-md-8 col-lg-8"> 
						                               		<input name="__logo_price_${logo.id}" value="${logo.price}" class="form_control" id="__logo_price_${logo.id}" htmlEscape="true" />
						                                </div>
						                            </div>
						                            <div class="form-group">
						                                <label for="__remove_logo_${logo.id}" class="col-sm-3 col-md-3 col-lg-3 control-label">   
					                                	 	<fmt:message key="remove" />:
						                                </label>
						                               	<div class="col-sm-9 col-md-8 col-lg-8"> 
						                               		 <p class="form-control-static"><input name="__remove_logo_${logo.id}" id="__remove_logo_${logo.id}" type="checkbox"></p>
						                                </div>
						                            </div>
		 						 				</c:forEach>
	 						 				</c:if> 
				                            
				                            <div class="form-group">
				                                <label for="image_file_${customerGroupForm.customerGroup.id}" class="col-sm-3 col-md-3 col-lg-3 control-label">
				                                    <fmt:message key="upload" />:
				                                </label>
				                                <div class="col-sm-9 col-md-8 col-lg-8">
				                                	<input value="browse" class="form-control" type="file" name="image_file_${customerGroupForm.customerGroup.id}" id="image_file_${customerGroupForm.customerGroup.id}"/> 
				                                </div>
				                            </div>
											 
		                       				<div class="form-group">
				                                <label for="customerGroup_disableCustomerUpload" class="col-sm-3 col-md-3 col-lg-3 control-label">
				                                   <fmt:message key="disableCustomerUpload" />:
				                                </label>
				                                <div class="col-sm-9 col-md-8 col-lg-8">
				                                	<p class="form-control-static"><form:checkbox path="customerGroup.disableCustomerUpload" id="customerGroup_disableCustomerUpload"/></p>
				                                </div>
				                            </div> 
				                        </div> 
				                   	</div>  
								</c:if>
								
								<c:if test="${!customerGroupForm.newCustomerGroup and gSiteConfig['gIMPRINT_OPTIONS'] > 0 and siteConfig['CUSTOMER_GROUP_IMPRINT_OPTIONS'].value == 'true'}">
									<!-- Tab3 -->
									<div id="tab_3" class="tab-pane"> 
				                        <div class="form-horizontal">  
	 						 				<c:forEach begin="1" end="5" var="id">
	 						 					<c:set var="locationId" value="locationId_${id}"/>
	 						 					<div class="form-group">
					                                <label for="" class="col-sm-3 col-md-3 col-lg-3 control-label">   
				                                	 	<fmt:message key="location" /> <fmt:message key="name" /> ${id}:
					                                </label>
					                               	<div class="col-sm-9 col-md-8 col-lg-8"> 
					                               		<select name="locationId_${id}" id="locationId_${id}" class="form-control">
											          		<option value="0">Select Location</option>
															<c:forEach items="${model.imprintList}" var="imprint">
																<option value="${imprint.id}" <c:if test="${imprint.id == model[locationId]}">selected</c:if>>${imprint.name}</option>
															</c:forEach>
														</select>
					                                </div>
					                            </div>
				                            </c:forEach> 
			                            </div>
		                            </div>
								</c:if>
								
								<c:if test="${!customerGroupForm.newCustomerGroup}">
									<c:if test="${siteConfig['CUSTOMER_GROUP_MARKUP'].value == 'true'}">
										<!-- Tab4 -->
										<div id="tab_4" class="tab-pane"> 
											<div class="table-responsive">
					                            <table class="table table-hover table-fixed-layout">
					                            	<tbody>
					                            		<c:forEach begin="1" end="10" var="id">
															<c:set var="markupRule" value="markupRule_${id}"/>
															<tr>
																<td><fmt:message key="markup" /> <fmt:message key="rule" /> ${id}:</td>
																<td>
																	<select name="markupRule_${id}" onchange="" class="form-control">
															          <option value="0">Select Markup Rule</option>
																		<c:forEach items="${model.markupList}" var="markup">
																			<option value="${markup.id}" <c:if test="${markup.id == model[markupRule]}">selected</c:if>>${markup.name}</option>
																		</c:forEach>
																	</select>
																</td>
																<c:set var="markupSkus" value="markupSkus_${id}"/>
																<td>
																	<textarea name="markupSkus_${id}" id="markupSkus_${id}" rows="4" class="form-control"><c:out value="${model[markupSkus]}"/></textarea> 
																</td>
																<c:set var="markupType" value="markupType_${id}"/>     
																<td>   		
													        		<select name="markupType_${id}" onchange="" class="form-control">
																		<option value="Sku" <c:if test="${model[markupType] == 'Sku'}">selected</c:if>>Sku</option>
																		<option value="Manufacture" <c:if test="${model[markupType] == 'Manufacture'}">selected</c:if>>Manufacture</option>
																	</select>
																</td>
															</tr>
														</c:forEach>
					                            	</tbody>
					                            </table>
				                            </div> 
			                            </div>
									</c:if>
									
									<c:if test="${siteConfig['CUSTOMER_GROUP_PRICE_TABLE_MARKUP'].value == 'true'}">
										<!-- Tab5 -->
										<div id="tab_5" class="tab-pane"> 
					                        <div class="form-horizontal">   
	 						 					<div class="form-group">
					                                <label for="customerGroup_priceTableMarkup" class="col-sm-3 col-md-3 col-lg-3 control-label">   
				                                	 	<fmt:message key="priceTableMarkup" /> :
					                                </label>
					                               	<div class="col-sm-9 col-md-8 col-lg-8"> 
					                               		 <form:input path="customerGroup.priceTableMarkup" id="customerGroup_priceTableMarkup" class="form-control" htmlEscape="true"/>%
					                                </div>
					                            </div> 
				                            </div>
			                            </div>
									</c:if>
									
									<c:if test="${gSiteConfig['gWILDMAN_GROUP']}">
										<!-- Tab6 -->
										<div id="tab_6" class="tab-pane"> 
					                        <div class="form-horizontal">   
	 						 					<div class="form-group">
					                                <label for="customerGroup_rushCharge" class="col-sm-3 col-md-3 col-lg-3 control-label">   
				                                	 	<fmt:message key="rushCharge" /> :
					                                </label>
					                               	<div class="col-sm-9 col-md-8 col-lg-8"> 
					                               		 <form:input path="customerGroup.rushCharge" id="customerGroup_rushCharge" class="form-control" htmlEscape="true" />${siteConfig['CURRENCY'].value}
					                                </div>
					                            </div> 
					                            
					                            <div class="form-group">
					                                <label for="customerGroup_rushServiceDays" class="col-sm-3 col-md-3 col-lg-3 control-label">   
				                                	 	<fmt:message key="rushServiceDays" /> :
					                                </label>
					                               	<div class="col-sm-9 col-md-8 col-lg-8"> 
					                               		 <form:input path="customerGroup.rushServiceDays" id="customerGroup_rushServiceDays" class="form-control" htmlEscape="true" />
			                               		  	</div>
					                            </div> 
					                            
					                            <div class="form-group">
					                                <label for="customerGroup_baggingCharge" class="col-sm-3 col-md-3 col-lg-3 control-label">  
					                                	<span class="fa fa-fw fa-question-circle" data-toggle="tooltip" title="Multiply by quantity of items in the cart that ."></span> 
				                                	 	<fmt:message key="baggingCharge" /> :
					                                </label>
					                               	<div class="col-sm-9 col-md-8 col-lg-8"> 
					                               		 <form:input path="customerGroup.baggingCharge" class="form-control" id="customerGroup_baggingCharge" htmlEscape="true" />${siteConfig['CURRENCY'].value}
			                               		 	</div>
					                            </div> 
					                            
					                            <div class="form-group">
					                                <label for="customerGroup_baggingSupplierIds" class="col-sm-3 col-md-3 col-lg-3 control-label">  
					                                	<span class="fa fa-fw fa-question-circle" data-toggle="tooltip" title="Provide list of comma separated supplier ids."></span> 
				                                	 	<fmt:message key="baggingServiceSupplierIds" /> :
					                                </label>
					                               	<div class="col-sm-9 col-md-8 col-lg-8"> 
					                               		<form:textarea path="customerGroup.baggingSupplierIds" class="form-control" id="customerGroup_baggingSupplierIds" rows="3"/>
					                                </div>
					                            </div> 
					                            
					                            <div class="form-group">
					                                <label for="customerGroup_lessThanMinQty" class="col-sm-3 col-md-3 col-lg-3 control-label">  
					                                	<span class="fa fa-fw fa-question-circle" data-toggle="tooltip" title="Minimum Quantity per supplier to avoid any extra charge.""></span> 
				                                	 	<fmt:message key="lessThanMinQty" /> :
					                                </label>
					                               	<div class="col-sm-9 col-md-8 col-lg-8"> 
					                               		<form:input path="customerGroup.lessThanMinQty" class="form-control" id="customerGroup_lessThanMinQty" htmlEscape="true" />
					                                </div>
					                            </div> 
					                            
					                            <div class="form-group">
					                                <label for="customerGroup_lessThanMinCharge" class="col-sm-3 col-md-3 col-lg-3 control-label">  
					                                	<span class="fa fa-fw fa-question-circle" data-toggle="tooltip" title="Multiply by quantity of items for specified suppliers in the cart."></span> 
				                                	 	<fmt:message key="lessThanMinCharge" /> :
					                                </label>
					                               	<div class="col-sm-9 col-md-8 col-lg-8">  
			      										<form:input path="customerGroup.lessThanMinCharge" class="form-control" id="customerGroup_lessThanMinCharge" htmlEscape="true" />${siteConfig['CURRENCY'].value}
				                               	 	</div>
					                            </div> 
					                            
					                            <div class="form-group">
					                                <label for="customerGroup_lessThanMinSupplierIds" class="col-sm-3 col-md-3 col-lg-3 control-label">  
					                                	<span class="fa fa-fw fa-question-circle" data-toggle="tooltip" title="Provide list of comma separated supplier ids."></span> 
				                                	 	<fmt:message key="lessThanMinCharge" /> :
					                                </label>
					                               	<div class="col-sm-9 col-md-8 col-lg-8">  
			      										 <form:textarea path="customerGroup.lessThanMinSupplierIds" class="form-control" id="customerGroup_lessThanMinSupplierIds" rows="3"/>
		      										 </div>
					                            </div>  
				                            </div>
			                            </div>
									</c:if>
									
									<c:if test="${gSiteConfig['gWILDMAN_GROUP'] || siteConfig['GROUP_ORDER_LIMIT'].value == 'true'}">
										<!-- Tab7 -->
										<div id="tab_7" class="tab-pane"> 
					                        <div class="form-horizontal">   
	 						 					<div class="form-group">
					                                <label for="customerGroup_contactEmail" class="col-sm-3 col-md-3 col-lg-3 control-label">   
				                                	 	<fmt:message key="contactEmail" /> :
					                                </label>
					                               	<div class="col-sm-9 col-md-8 col-lg-8"> 
					                               		<form:textarea path="customerGroup.contactEmail" id="customerGroup_contactEmail" rows="2" class="form-control" htmlEscape="true"/> 
			                               		 	</div>
					                            </div>  
					                            
					                            <c:if test="${gSiteConfig['gWILDMAN_GROUP']}">
					                            	<div class="form-group">
						                                <label for="customerGroup_paymentEmail" class="col-sm-3 col-md-3 col-lg-3 control-label">   
					                                	 	<fmt:message key="paymentEmail" /> :
						                                </label>
						                               	<div class="col-sm-9 col-md-8 col-lg-8">
						                               		<form:input path="customerGroup.paymentEmail" class="form-control" id="customerGroup_paymentEmail" htmlEscape="true"/>  
				                               		 	</div>
						                            </div>  
					                            </c:if>
				                            </div>
			                            </div>  
									</c:if>
									
									<c:if test="${siteConfig['GROUP_REWARDS'].value == 'true'}">
										<!-- Tab8 -->
										<div id="tab_8" class="tab-pane"> 
					                        <div class="form-horizontal">   
	 						 					<div class="form-group">
					                                <label for="customerGroup_rewardPoints" class="col-sm-3 col-md-3 col-lg-3 control-label">   
					                                	<span class="fa fa-fw fa-question-circle" data-toggle="tooltip" title="Conversion of 1 $ to xxx rewards"></span>
				                                	 	<fmt:message key="reward" /> :
					                                </label>
					                               	<div class="col-sm-9 col-md-8 col-lg-8"> 
					                               		1 <fmt:message key="${siteConfig['CURRENCY'].value}" /> = <form:input path="customerGroup.rewardPoints" class="form-control" id="customerGroup_rewardPoints" htmlEscape="true" /> points
			                               		 	</div>
					                            </div>   
				                            </div>
			                            </div>  
									</c:if>
									
									<c:if test="${siteConfig['GROUP_ORDER_LIMIT'].value == 'true'}">
										<!-- Tab9 -->
										<div id="tab_9" class="tab-pane"> 
					                        <div class="form-horizontal">   
	 						 					<div class="form-group">
					                                <label for="customerGroup_groupOrderLimit" class="col-sm-3 col-md-3 col-lg-3 control-label">   
					                                	<span class="fa fa-fw fa-question-circle" data-toggle="tooltip" title="Customers under this group has a limit on their order total. If exceeded, order is placed as an QUOTE and waits for approvals."></span>
				                                	 	<fmt:message key="orderLimit" /> :
					                                </label>
					                               	<div class="col-sm-9 col-md-8 col-lg-8"> 
					                               		 <fmt:formatNumber value="${customerGroupForm.customerGroup.groupOrderLimit}" pattern="###0.00" var="decimalPattern" />
			    										 <form:input path="customerGroup.groupOrderLimit"  value="${decimalPattern}" class="form-control" id="customerGroup_groupOrderLimit" htmlEscape="true" /> USD
			                               		 	</div>
					                            </div> 
					                            
					                            <div class="form-group">
					                                <label for="" class="col-sm-3 col-md-3 col-lg-3 control-label">   
					                                	<span class="fa fa-fw fa-question-circle" data-toggle="tooltip" title="When Order is place as QUOTE, approval email notice is sent to this Approval Manager"></span>
				                                	 	<fmt:message key="approvingManager" /> :
					                                </label>
					                               	<div class="col-sm-9 col-md-8 col-lg-8"> 
					                               		<div class="radio"> 
					                               			<label> 
						                               		 <form:radiobutton path="customerGroup.orderLimitManager" value="1" />&nbsp; Immediate Manager <br>
															 <form:radiobutton path="customerGroup.orderLimitManager" value="2" />&nbsp; All Managers  <br>
															 <form:radiobutton path="customerGroup.orderLimitManager" value="3" />&nbsp; Check Customer Setting
														 	</label>
														 </div>
													 </div>
					                            </div>  
					                              
				                            </div>
			                            </div>  
									</c:if>
									
									<c:if test="${gSiteConfig['gBUDGET_FREQUENCY'] > 0 and siteConfig['BUDGET_FREQUENCY_GROUP_LABELS'].value == 'true'}">
										<!-- Tab10 -->
										<div id="tab_10" class="tab-pane"> 
					                        <div class="form-horizontal">   
	 						 					<div class="form-group">
					                                <label for="" class="col-sm-3 col-md-3 col-lg-3 control-label">   
					                                	<span class="fa fa-fw fa-question-circle" data-toggle="tooltip" title="When Order is place as QUOTE, approval email notice is sent to this Approval Manager"></span>
				                                	 	<fmt:message key="approvingManager" /> :
					                                </label>
					                               	<div class="col-sm-9 col-md-8 col-lg-8"> 
		                               		 	 		<div class="radio"> 
					                               			<label> 
				                               					<form:radiobutton path="customerGroup.budgetFrequencyManager" value="1" />&nbsp; Immediate Manager <br>
															 	<form:radiobutton path="customerGroup.budgetFrequencyManager" value="2" />&nbsp; All Managers <br>
															 	<form:radiobutton path="customerGroup.budgetFrequencyManager" value="3" />&nbsp; Check Customer Setting
					                               			</label>
				                               			</div>
			                               		 	</div>
					                            </div> 
					                            
					                            <c:forEach begin="1" end="${gSiteConfig['gBUDGET_FREQUENCY']}" var="type">
													<c:set var="key" value="BFRTY_${type}" />
													<div class="form-group">
						                                <label for="BFRTY_${type}" class="col-sm-3 col-md-3 col-lg-3 control-label">   
				                                	 		<fmt:message key="budgetFrequency" /> ${type}:
						                                </label>
						                               	<div class="col-sm-9 col-md-8 col-lg-8"> 
			                               		 	 		<input type="text" class="form-control" id="BFRTY_${type}" name="BFRTY_${type}" value="<c:out value="${model[key]}"/>" maxlength="100"/>
				                               		 	</div>
						                            </div> 
												</c:forEach>
				                            </div>
			                            </div>
									</c:if> 
								</c:if>
								
								<c:if test="${!customerGroupForm.newCustomerGroup}">
								<c:if test="${siteConfig['CLICK_DIALING'].value == 'true'}">
									<!-- Tab11 -->
									<div id="tab_11" class="tab-pane"> 
				                        <div class="form-horizontal">   
				 					 		<c:set var="closerIdsErrors"><form:errors path="customerGroup.closerIds"/></c:set> 
			                       			<div class="form-group <c:if test="${not empty closerIdsErrors}"> has-error</c:if>">
				                                <label for="customerGroup_closerIds" class="col-sm-3 col-md-3 col-lg-3 control-label">   
				                                	Closer Ids:
				                                </label>
				                               	<div class="col-sm-9 col-md-8 col-lg-8"> 
                       		 	 		        	<form:textarea path="customerGroup.closerIds" id="customerGroup_closerIds" rows="10" cssClass="form-control" htmlEscape="true"/>
	 	    										<form:errors path="customerGroup.closerIds" cssClass="help-block" />
		                               		 	</div>
				                            </div> 
				                            
				                            <c:set var="salesRepIdsErrors"><form:errors path="customerGroup.salesRepIds"/></c:set> 
			                       			<div class="form-group <c:if test="${not empty salesRepIdsErrors}"> has-error</c:if>">
				                                <label for="customerGroup_salesRepIds" class="col-sm-3 col-md-3 col-lg-3 control-label">   
				                                	<fmt:message key="salesRep"/> Ids:
				                                </label>
				                               	<div class="col-sm-9 col-md-8 col-lg-8"> 
                       		 	 		        	<form:textarea path="customerGroup.salesRepIds" id="customerGroup_salesRepIds" rows="10" cssClass="form-control" htmlEscape="true"/>
	 	   										 	<form:errors path="customerGroup.salesRepIds" cssClass="help-block" />
		                               		 	</div>
				                            </div> 
				                            
				                            <div class="form-group">
				                                <label for="" class="col-sm-3 col-md-3 col-lg-3 control-label">   
				                                	<fmt:message key="dialingGroup"/>:
				                                </label>
				                               	<div class="col-sm-9 col-md-8 col-lg-8"> 
				                               		<p class="form-control-static">
                       		 	 		        	<form:checkbox path="customerGroup.dialingGroup" />
                       		 	 		        	</p>
		                               		 	</div>
				                            </div>
				                            
				                            <c:set var="dialingMessageErrors"><form:errors path="customerGroup.dialingMessage"/></c:set> 
			                       			<div class="form-group <c:if test="${not empty dialingMessageErrors}"> has-error</c:if>">
				                                <label for="customerGroup_dialingMessage" class="col-sm-3 col-md-3 col-lg-3 control-label">   
				                                	<fmt:message key="dialingMessage"/>:
				                                </label>
				                               	<div class="col-sm-9 col-md-8 col-lg-8"> 
                       		 	 		        	<form:textarea path="customerGroup.dialingMessage" rows="10" cssClass="form-control" id="customerGroup_dialingMessage"  htmlEscape="true"/>
	 	   										 	<form:errors path="customerGroup.dialingMessage" cssClass="help-block" />
		                               		 	</div>
				                            </div>
				                            
				                            <div class="form-group">
				                                <label for="" class="col-sm-3 col-md-3 col-lg-3 control-label">   
				                                	Timezones:
				                                </label>
				                               	<div class="col-sm-9 col-md-8 col-lg-8"> 
				                               		<p class="form-control-static">
                       		 	 		        	<input type="checkbox" class="timezoneCheckbox" id="HSTcheckbox"/> HST
										            <input type="checkbox" class="timezoneCheckbox" id="AKSTcheckbox"/> AKST
										            <input type="checkbox" class="timezoneCheckbox" id="PSTcheckbox"/> PST
										            <input type="checkbox" class="timezoneCheckbox" id="MSTcheckbox"/> MST
										            <input type="checkbox" class="timezoneCheckbox" id="CSTcheckbox"/> CST
										            <input type="checkbox" class="timezoneCheckbox" id="ESTcheckbox"/> EST
										            <form:input path="customerGroup.timezones" type="hidden" name="timezoneList" id="timezoneList"/>
                       		 	 		        	</p>
		                               		 	</div>
				                            </div>
				                            
			                            </div>
		                            </div>
								</c:if>
								</c:if>
								  
				            </div>
			            </div> 
			            
			            <!-- Footer -->
			            <div class="panel-footer">
			                <div class="clearfix">
			                    <div class="pull-right">
			                    	<c:if test="${customerGroupForm.newCustomerGroup}"> 
									    <button type="submit" class="btn btn-primary" value="<spring:message code="add"/>">
			                            	<span class="fa fa-plus mr-5"></span> Add
		                        		</button>
									</c:if>
									<c:if test="${!customerGroupForm.newCustomerGroup}">
										<button type="submit" class="btn btn-primary" value="<spring:message code="update"/>"  onclick="setTimezones()">
				                            <span class="fa fa-refresh mr-5"></span> Update
				                        </button>  
				                         
								  		<input type="submit" value="<fmt:message key="delete"/>" name="_delete" class="btn btn-danger" onClick="return confirm('Delete permanently?')">
									</c:if>
							 	 	<input type="submit" value="<fmt:message key="cancel"/>" name="_cancel" class="btn btn-default" /> 
								</div>
							</div>
						</div>
			   		</div>
			     </form:form>
			</section>
		</sec:authorize>
	</tiles:putAttribute>
</tiles:insertDefinition>

<script type="text/javascript">
<!--
window.addEvent('domready', function(){			
			var tabs1 = new SimpleTabs($('tab-block-1'), { selector: 'h4'});
		});
window.addEvent('domready', function(){			
	var timezoneList = document.getElementById('timezoneList').value;
	if(timezoneList.indexOf("HST") > -1){
		var hst = document.getElementById('HSTcheckbox');
		hst.checked = true;
	}
	if(timezoneList.indexOf("AKST") > -1){
		var akst = document.getElementById('AKSTcheckbox');
		akst.checked = true;
	}
	if(timezoneList.indexOf("PST") > -1){
		var pst = document.getElementById('PSTcheckbox');
		pst.checked = true;
	}
	if(timezoneList.indexOf("MST") > -1){
		var mst = document.getElementById('MSTcheckbox');
		mst.checked = true;
	}
	if(timezoneList.indexOf("CST") > -1){
		var cst = document.getElementById('CSTcheckbox');
		cst.checked = true;
	}
	if(timezoneList.indexOf("EST") > -1){
		var est = document.getElementById('ESTcheckbox');
		est.checked = true;
	}
	
});

function setTimezones() {
	var timezones = "";
	var hst = document.getElementById('HSTcheckbox');
	if(hst.checked){
		timezones += "HST";
	}	
	var akst = document.getElementById('AKSTcheckbox');
	if(akst.checked){
		if(timezones != ""){
			timezones += ",";
		}
		timezones += "AKST";
	}
	var pst = document.getElementById('PSTcheckbox');
	if(pst.checked){
		if(timezones != ""){
			timezones += ",";
		}
		timezones += "PST";
	}
	var mst = document.getElementById('MSTcheckbox');
	if(mst.checked){
		if(timezones != ""){
			timezones += ",";
		}
		timezones += "MST";
	}
	var cst = document.getElementById('CSTcheckbox');
	if(cst.checked){
		if(timezones != ""){
			timezones += ",";
		}
		timezones += "CST";
	}
	var est = document.getElementById('ESTcheckbox');
	if(est.checked){
		if(timezones != ""){
			timezones += ",";
		}
		timezones += "EST";
	}
	var timezoneListInput = document.getElementById('timezoneList');
	timezoneListInput.value = timezones;
}
//-->
</script>