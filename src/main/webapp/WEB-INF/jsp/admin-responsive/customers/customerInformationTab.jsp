<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="/WEB-INF/tlds/spring.tld" prefix="spring" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ taglib uri="http://www.springframework.org/security/tags" prefix="sec" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ taglib uri="/WEB-INF/tlds/custom.tld" prefix="wj" %>

<div id="tab_2" class="tab-pane" role="tabpanel">
	<div class="line">
		<div class="form-row">
			<div class="col-label">
				<label for="customer_address_firstName" class="col-form-label">
					<fmt:message key='firstName' />
				</label>
			</div>
			<div class="col-lg-4">
	            <form:input path="customer.address.firstName" id="customer_address_firstName" class="form-control"/>
			</div>
		</div>
	</div> 
	<div class="line">
		<div class="form-row">
			<div class="col-label">
				<label for="customer_address_lastName" class="col-form-label">
					<fmt:message key='lastName' />
				</label>
			</div>                 
			<div class="col-lg-4"> 
				<form:input path="customer.address.lastName" id="customer_address_lastName" class="form-control"/>   	
			</div>
		</div>
	</div>
	<div class="line">
		<div class="form-row">
			<div class="col-label">
				<label for="customer_address_addr1" class="col-form-label">
					<fmt:message key="address" /> <fmt:message key="f_one" />
				</label>
			</div>                 
			<div class="col-lg-4"> 
				<form:input path="customer.address.addr1" id="customer_address_addr1" class="form-control"/>  	
			</div>
		</div>
	</div>
	<div class="line">
		<div class="form-row">
			<div class="col-label">
				<label for="customer_address_addr2" class="col-form-label">
					<fmt:message key="address" /> <fmt:message key="f_two" />
				</label>
			</div>
			<div class="col-lg-4"> 
				<form:input path="customer.address.addr2" id="customer_address_addr2" class="form-control"/>
			</div>
		</div>
	</div>
	<div class="line">
		<div class="form-row">
			<div class="col-label">
				<label for="customer_address_country" class="col-form-label">
					<fmt:message key='country' />
				</label>
			</div>
			<div class="col-lg-4"> 
				<form:select id="country" path="customer.address.country" onchange="toggleStateProvince(this)" class="custom-select">
					<form:option value="" label="Please Select"/>
					<form:options items="${countries}" itemValue="code" itemLabel="name"/>
				</form:select>
			</div>
		</div>
	</div>
	<div class="line">
		<div class="form-row">
			<div class="col-label">
				<label for="customer_address_city" class="col-form-label">
					<fmt:message key="city" />
				</label>
			</div>
			<div class="col-lg-4">
				<form:input path="customer.address.city" id="customer_address_city" class="form-control" />
			</div>
		</div>
	</div>
	<div class="line">
		<div class="form-row">
			<div class="col-label">
				<label class="col-form-label">
					<fmt:message key="stateProvince" />
				</label>
			</div>
			<div class="col-lg-4">
				<div class="form-group">
					<form:select id="state" path="customer.address.stateProvince" class="custom-select">
						<form:option value="" label="Please Select"/>
						<form:options items="${states}" itemValue="code" itemLabel="name"/>
					</form:select>
				</div>
				<div class="form-group">
					<form:select id="ca_province" path="customer.address.stateProvince" class="custom-select">
						<form:option value="" label="Please Select"/>
						<form:options items="${caProvinceList}" itemValue="code" itemLabel="name"/>
					</form:select>
				</div>
				<div class="form-group">
					<form:input class="form-control" id="province" path="customer.address.stateProvince"/>
				</div>
				<div id="stateProvinceNA">
					<div class="form-group">
						<label class="custom-control custom-checkbox">
							<form:checkbox path="customer.address.stateProvinceNA" id="addressStateProvinceNA" value="true"  class="custom-control-input"/>
							<span class="custom-control-indicator"></span>
							<span class="custom-control-description">Not Applicable</span>
						</label>
					</div>
				</div> 
			</div>
		</div>
	</div>
	<div class="line">
		<div class="form-row">
			<div class="col-label">
				<label for="customer_address_zip" class="col-form-label">
					<fmt:message key="zipCode" />
				</label>
			</div>
			<div class="col-lg-4">
				<form:input path="customer.address.zip" id="customer_address_zip" class="form-control"/> 
			</div>
		</div>
	</div>
	<div class="line">
		<div class="form-row">
			 <div class="col-label">
				<label for="customer_address_phone" class="col-form-label">
					<fmt:message key="phone" />
				</label>
			</div>
			<div class="col-lg-4">
				<div class="form-group">
					<form:input path="customer.address.phone"  id="customer_address_phone" class="form-control"/>
				</div>
				<small class="help-text">
					US: xxx-xxx-xxxx or (xxx) xxx-xxxx Ext-xxxxx
				</small>
			</div>
		</div>
	</div>
	<div class="line">
		<div class="form-row">
			<div class="col-label">
				<label for="customer_address_cellPhone" class="col-form-label"><fmt:message key="cellPhone" /></label>
			</div>
			<div class="col-lg-4"> 
				<form:input path="customer.address.cellPhone" id="customer_address_cellPhone" class="form-control"/>
			</div>
		</div>
	</div>
	<c:if test="${siteConfig['CUSTOMER_MASS_TEXT'].value == 'true'}">
		<div class="line">
			<div class="form-row">
				<div class="col-label">
					<label for="customer_address_cellPhone" class="col-form-label">
						<fmt:message key='mobileCarrier' />
					</label>
				</div>
				<div class="col-lg-4"> 
					<form:select id="mobileCarrierId" path="customer.address.mobileCarrierId"  class="form-control">
						<form:option value="" label="Please Select"/>
						<form:options items="${mobileCarriers}" itemValue="id" itemLabel="carrier"/>
					</form:select> 
	 			</div>
 			</div>
		</div>
		<div class="line">
			<div class="form-row">
				<div class="col-label">
					<label for="customer_address_cellPhone" class="col-form-label">
						<fmt:message key="textMessage" /> <fmt:message key="notification" />
					</label>
				</div>
                 <div class="col-lg-4">
                 	<label class="custom-control custom-checkbox">
						<form:checkbox path="customer.textMessageNotify" id="customer_textMessageNotify" class="custom-control-input"/>
						<span class="custom-control-indicator"></span>
					</label>
                 </div>
             </div>
		</div>
	</c:if>
	<div class="line">
		<div class="form-row">
			<div class="col-label">
				<label for="customer_address_fax" class="col-form-label"><fmt:message key="fax" /></label>
			</div>
			<div class="col-lg-4"> 
				<form:input path="customer.address.fax" id="customer_address_fax" class="form-control"/>
			</div>
		</div>
	</div>
	<c:if test="${languageCodes != null}">
		<div class="line">
			<div class="form-row">
				<div class="col-label">
					<label for="customer_languageCode" class="col-form-label"><fmt:message key='language' /></label>
				</div>
				<div class="col-lg-4"> 
					<form:select path="customer.languageCode" id="customer_languageCode" class="custom-select">
						<form:option value="en"><fmt:message key="language_en"/></form:option>        	  
						<c:forEach items="${languageCodes}" var="i18n">
							<form:option value="${i18n.languageCode}"><fmt:message key="language_${i18n.languageCode}"/></form:option>
						</c:forEach>
					</form:select>
				</div>
			</div>
		</div>
	</c:if>
	<div class="line">
		<div class="form-row">
			<div class="col-label">
				<label for="customer_address_company" class="col-form-label">
					<fmt:message key="company" />
				</label>
			</div>
			<div class="col-lg-4"> 
				<form:input path="customer.address.company" id="customer_address_company" class="form-control"/>
			</div>
		</div>
	</div>
	<div class="line">
		<div class="form-row">
			<div class="col-label">
				<label for="customer_websiteUrl" class="col-form-label">
					<fmt:message key="websiteUrl" />
				</label>
			</div>
			<div class="col-lg-4"> 
				<form:input path="customer.websiteUrl" id="customer_websiteUrl" class="form-control"/>
			</div>
		</div>
	</div>
	<div class="line">
		<div class="form-row">
			<div class="col-label">
				<label for="customer_ebayName" class="col-form-label">
					<fmt:message key="ebayName" />
				</label>
			</div>
			<div class="col-lg-4"> 
				<form:input path="customer.ebayName" id="customer_ebayName" class="form-control" />
			</div>
		</div>
	</div>
	<div class="line">
		<div class="form-row">
			<div class="col-label">
				<label for="customer_amazonName" class="col-form-label">
					<fmt:message key="amazonName" />
				</label>
			</div>
			<div class="col-lg-4">
				<form:input path="customer.amazonName" id="customer_amazonName" class="form-control"/>
			</div>
		</div>
	</div>
	<c:if test="${siteConfig['ADDRESS_RESIDENTIAL_COMMERCIAL'].value >= 2}">
		<div class="line">
			<div class="form-row">
				<div class="col-label">
					<label for="customer_amazonName" class="col-form-label"><fmt:message key="deliveryType" /></label>
				</div>
				<div class="col-lg-4">
					<div class="form-group">
	                    <label class="custom-control custom-checkbox">
	                        <form:radiobutton path="customer.address.residential" class="custom-control-input" id="customer_address_residential" value="true"/>
	                        <span class="custom-control-indicator"></span>
	                        <span class="custom-control-description"><fmt:message key="residential" /></span>
	                    </label>
	                    <label class="custom-control custom-checkbox">
	                        <form:radiobutton path="customer.address.residential" class="custom-control-input" id="customer_address_residential" value="false"/>  
	                        <span class="custom-control-indicator"></span>
	                        <span class="custom-control-description"><fmt:message key="commercial" /></span>
	                    </label>
                    </div>
				</div>
			</div>
		</div>
		<c:if test="${siteConfig['SHIPPING_LTL'].value == 'true'}">
			<div class="line">
				<div class="form-row">
					<div class="col-label">
						<label for="customer_amazonName" class="col-form-label">
							<span class="fa fa-question-circle" data-toggle="tooltip" data-placement="top" title="LTL::"></span>
							<fmt:message key="liftGateDelivery" />
						</label>
					</div>
					<div class="col-lg-4">
						<div class="form-group">
							<label class="custom-control custom-checkbox">
								<form:checkbox path="customer.address.liftGate" id="customer_address_liftGate" class="custom-control-input" value="true"/>
			      				<span class="custom-control-indicator"></span>
			      			</label>
						</div> 
					</div>
				</div>
			</div>
		</c:if>
	</c:if>
                           
	<div class="line">
		<div class="form-row">
			<div class="col-label">
				<label for="customer_accountNumber" class="col-form-label">
					<fmt:message key="accountNumber" />
				</label>
			</div>
			<div class="col-lg-2">
				<form:input path="customer.accountNumberPrefix" id="customer_accountNumberPrefix" placeholder="Prefix" class="form-control"/>
			</div>
			<c:choose>
				<c:when test="${siteConfig['CUSTOMER_AUTOGENERATE_ACCOUNTNUMBER'].value == 'true'}">
					<div class="col-lg-2">
						<form:input path="customer.accountNumber" id="customer_accountNumber" class="form-control"/>
					</div>
				</c:when>
				<c:otherwise>
					<div class="col-lg-2">
						<form:input path="customer.accountNumber" id="customer_accountNumber" maxlength="50" class="form-control"/>
					</div>
					<c:if test="${gSiteConfig['gWILDMAN_GROUP']}">
						<div class="col-lg-3">
							<div class="form-group">
								<label class="custom-control custom-checkbox">
				      				<form:checkbox path="customer.defaultLocation" class="custom-control-input" id="customer_defaultLocation" />
				      				<span class="custom-control-indicator"></span>
									<span class="custom-control-description">Default Location</span>
				      			</label>
		      				</div>
						</div>
					</c:if>
				</c:otherwise>
			</c:choose>
		</div>
	</div>
                           
	<c:if test="${siteConfig['AHI_FLAG'].value == 'true'}">
		<div class="line">
			<div class="form-row">
				<div class="col-label">
					<label for="customer_taxId" class="col-form-label">HIN Number</label>
				</div>
				<div class="col-lg-4">
					<form:input path="customer.hinNumber" id="customer_hinNumber" maxlength="50" class="form-control"/>
				</div>
			</div>
		</div>
	</c:if>
	<c:if test="${gSiteConfig['gTAX_EXEMPTION'] == true}">
		<div class="line">
			<div class="form-row">
				<div class="col-label">
					<label for="customer_taxId" class="col-form-label">
						<fmt:message key="taxId" />
					</label>
				</div>
				<div class="col-lg-4"> 
					<div class="form-group">
						<form:input path="customer.taxId" id="customer_taxId" maxlength="20" class="form-control" />
					</div>
					<small class="help-text">
						To Disable Tax Exemption Simply Erase The Tax ID
					</small>
				</div>
			</div>
		</div>
	</c:if>
	<div class="line">
		<div class="form-row">
			<div class="col-label">
				<label for="customer_taxRate" class="col-form-label"><fmt:message key="taxRate" /></label>
			</div>
			<div class="col-lg-4"> 
				<form:input path="customer.taxRate" id="customer_taxRate" maxlength="20" class="form-control" />
			</div>
		</div>
	</div>
	<c:if test="${siteConfig['QUICKBOOKS_INTEGRATION'].value == 'true'}">
		<div class="line">
			<div class="form-row">
				<div class="col-label">
					<label class="col-form-label">
						<fmt:message key="qbTaxCode" />
					</label>
				</div>
				<div class="col-lg-4"> 
					<form:input path="customer.qbTaxCode" id="customer_qbTaxCode" maxlength="20" class="form-control"/>
				</div>
			</div>
		</div>
	</c:if>
	<div class="line">
		<div class="form-row">
			<div class="col-label">
				<label for="customer_allowEditTax" class="col-form-label">
					<fmt:message key="allowEditTax" />
				</label>
			</div>
			<div class="col-lg-4">
				<div class="checkbox">
					 <label class="custom-control custom-checkbox">
						<form:checkbox path="customer.allowEditTax" class="custom-control-input" id="customer_allowEditTax"/>  
						<span class="custom-control-indicator"></span>
						<span class="custom-control-description">Yes</span>
					</label>
				</div>
			</div>
		</div>
	</div>
</div>