<%@ page import="com.webjaguar.model.*,java.util.*"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/security/tags" prefix="sec" %>

<!-- Start: Right Sidebar -->
<aside id="sidebar_right" class="nano affix">
    <!-- Start: Sidebar Right Content -->
    <div class="sidebar-right-content nano-content">
        <div class="tab-block sidebar-block br-n">
            <ul class="nav nav-tabs tabs-border nav-justified">
                <li class="active">
                    <a href="javascript:void(0);" data-toggle="tab" data-target="#sidebar-right-tab1">Search</a>
                </li>
                <c:if test="${param.tab == 'customers'}"> 
	                <li>
	                    <a href="javascript:void(0);" data-toggle="tab" data-target="#sidebar-right-tab2">Advanced Search</a>
	                </li>
                </c:if>
            </ul>
            <c:if test="${param.tab == 'vbaOrders'}"> 
            	 <form action="virtualBankPayment.jhtm" name="searchform" method="post" > 
            		<div class="tab-content br-n">
	                    <div id="sidebar-right-tab1" class="tab-pane active">
	                    	<div class="form-group">
	                            <label for="paymentFor"><fmt:message key="paymentFor"/></label>
	                            <select name="paymentFor" id="paymentFor" class="form-control input-sm">
							         <option value="">Select One</option>
							  	     <option value="1" <c:if test="${vbaOrderSearch.paymentFor == 1}">selected</c:if>>Affiliate</option>
							  	     <option value="2" <c:if test="${vbaOrderSearch.paymentFor == 2}">selected</c:if>>Consignment</option>
						      	</select> 
	                        </div>
	                        <div class="form-group">
	                            <label for="orderStatus"><fmt:message key="status" /></label>
	                            <select name="orderStatus" id="orderStatus" class="form-control input-sm">
							  	    <option value=""> </option>
							  	    <option value="p" <c:if test="${vbaOrderSearch.orderStatus == 'p'}">selected</c:if>><fmt:message key="orderStatus_p" /></option>
							  	    <option value="pr" <c:if test="${vbaOrderSearch.orderStatus == 'pr'}">selected</c:if>><fmt:message key="orderStatus_pr" /></option>
							  	    <option value="ppr" <c:if test="${vbaOrderSearch.orderStatus == 'ppr'}">selected</c:if>><fmt:message key="orderStatus_ppr" /></option>
							  	    <option value="s" <c:if test="${vbaOrderSearch.orderStatus == 's'}">selected</c:if>><fmt:message key="orderStatus_s" /></option>
							  	   	<option value="pprs" <c:if test="${vbaOrderSearch.orderStatus == 'pprs'}">selected</c:if>><fmt:message key="orderStatus_pprs" /></option>  	   
						  	    </select>
	                        </div>
	                        <div class="form-group">
	                            <label for="orderNum"><fmt:message key="order" /></label>
	                            <input name="orderNum" type="text" id="orderNum" value="<c:out value='${vbaOrderSearch.orderId}' />" class="form-control input-sm" />
	                        </div>
	                        <div class="form-group">
	                            <label for="productSku"><fmt:message key="productSku" /></label>
	                            <input name="productSku" id="productSku" type="text" value="<c:out value='${vbaOrderSearch.productSku}' />" class="form-control input-sm"  />
	                        </div>
	                        <div class="form-group">
	                            <label for="host"><fmt:message key="supplier"/> <fmt:message key="email"/></label>
	                            <input name="userName" type="text" value="<c:out value='${vbaOrderSearch.userName}' />" class="form-control input-sm" />
	                        </div>
	                        
	                        <input type="submit" value="Search" class="btn btn-sm btn-primary btn-block"/>  
	                    </div>
                  	</div> 
			  	</form> 
	        </c:if> 
            <c:if test="${param.tab == 'groups'}"> 
            	<form action="customerGroupList.jhtm" name="searchform" method="post">	
            		<div class="tab-content br-n">
	                    <div id="sidebar-right-tab1" class="tab-pane active">
	                    	<div class="form-group">
	                            <label for="host"><fmt:message key="group" /></label>
	                            <input name="group" type="text" value="<c:out value='${groupCustomerSearch.group}' />" class="form-control input-sm"/>
	                        </div>
	                        <div class="form-group">
	                            <label for="host"><fmt:message key="active" /></label>
	                            <select name="active" class="form-control input-sm">
							        <option value=""><fmt:message key="all"/></option> 
							        <option value="0" <c:if test="${model.search.active == '0'}">selected</c:if>><fmt:message key="inactive" /></option>
							  	    <option value="1" <c:if test="${model.search.active == '1'}">selected</c:if>><fmt:message key="active" /></option>
							    </select>
	                        </div>
	                        
	                        <input type="submit" value="Search" class="btn btn-sm btn-primary btn-block"/>  
	                    </div>
                  	</div> 
			  	</form> 
	        </c:if>

            <c:if test="${param.tab == 'customers'}"> 
	            <form name="searchform" action="customerList.jhtm" method="post" >
	                <div class="tab-content br-n">
	                    <div id="sidebar-right-tab1" class="tab-pane active">
	                    	<c:if test="${gSiteConfig['gMULTI_STORE'] > 0}">
		                        <div class="form-group">
		                            <label for="host"><fmt:message key="multiStore" />:</label>
		                            <select class="form-control input-sm" id="host" name="host"> 
	                                 	<option value=""><fmt:message key="all" /> <fmt:message key="accounts" /></option>
								  	    <option value="main" <c:if test="${customerSearch.host == 'main'}">selected</c:if>><fmt:message key="main" /></option>
								  	    <c:forEach items="${multiStores}" var="multiStore">
								  	    	<option value="${multiStore.host}" <c:if test="${customerSearch.host == multiStore.host}">selected</c:if>><c:out value="${multiStore.host}" /></option>
								  	    </c:forEach>
		                            </select>
		                        </div>
	                        </c:if> 
	                        <div class="form-group">
	                            <label for="firstName"><fmt:message key="firstName" />:</label> 
	                            <input name="firstName" id="firstName" type="text" value="<c:out value='${customerSearch.firstName}' />" class="form-control input-sm" />
	                        </div>
	
	                        <div class="form-group">
	                            <label for="lastName"><fmt:message key="lastName" />:</label> 
	                            <input name="lastName" id="lastName" type="text" value="<c:out value='${customerSearch.lastName}' />" class="form-control input-sm" />
	                        </div>
	
	                        <div class="form-group">
	                            <label for="email"><fmt:message key="email" />:</label> 
	                            <input name="email" type="text" value="<c:out value='${customerSearch.email}' />" class="form-control input-sm" />
	                        </div>
	
	                        <div class="form-group">
	                            <label for="companyName"><fmt:message key="companyName" />:</label> 
	                            <input name="companyName" type="text" value="<c:out value='${customerSearch.companyName}' />" class="form-control input-sm" />
	                        </div>
	
	                        <div class="form-group">
	                            <label for="accountNumber"><fmt:message key="accountNumber" />:</label>
	                            <input name="accountNumber" type="text" value="<c:out value='${customerSearch.accountNumber}' />" class="form-control input-sm"/>   
	                        </div>
							
							<c:if test="${gSiteConfig['gSALES_REP']}">
								<div class="form-group">
		                            <label for="sales_rep_id"><fmt:message key="salesRep" />:</label>
		                            <select class="form-control input-sm" id="sales_rep_id" name="sales_rep_id">
		                            	<c:if test="${model.salesRepTree == null}">
								       		<option value="-1"></option>
								       		<option value="-2" <c:if test="${customerSearch.salesRepId == -2}">selected</c:if>><fmt:message key="noSalesRep" /></option>
								       		<option value="-3" <c:if test="${customerSearch.salesRepId == -3}">selected</c:if>><fmt:message key="allSalesRep" /></option>
								       		<c:forEach items="${model.salesReps}" var="salesRep">
								  	     		<option value="${salesRep.id}" <c:if test="${customerSearch.salesRepId == salesRep.id}">selected</c:if>><c:out value="${salesRep.name}" /></option>
								       		</c:forEach>
								       	</c:if>
		                                <c:if test="${model.salesRepTree != null}">
							         		<c:set var="salesRepTree" value="${model.salesRepTree}" scope="request"/>
								         	<c:set var="customerSearchSalesRepId" value="${customerSearch.salesRepId}" scope="request"/>
								         	<option value="${salesRepTree.id}" <c:if test="${customerSearch.salesRepId == salesRepTree.id}">selected</c:if>><c:out value="${salesRepTree.name}" /></option>
										 	<c:set var="level" value="0" scope="request"/>
							      		 	<c:set var="recursionPageId" value="${customerSearchSalesRepId}" scope="request"/>
								      	 	<jsp:include page="/WEB-INF/jsp/admin/customers/recursion.jsp"/>
								       </c:if>
		                            </select>
		                        </div> 
						    </c:if> 
	
							<c:if test="${gSiteConfig['gGROUP_CUSTOMER']}">
								<div class="form-group">
		                            <label for="groupId"><fmt:message key="customer"/> <fmt:message key="group" />:</label>
		                            <select class="form-control input-sm" id="groupId" name="groupId">
		                               <option value=""></option>
							      		<c:forEach items="${model.groups}" var="group">
							  	     		<option value="${group.id}" <c:if test="${customerSearch.groupId == group.id}">selected</c:if>><c:out value="${group.name}" /></option>
								      	</c:forEach>
		                            </select>
		                        </div> 
						    </c:if> 
							
							<c:if test="${gSiteConfig['gSHOPPING_CART'] and siteConfig['SHOPPINGCART_SEARCH'].value == 'true'}">
								<div class="form-group">
		                            <label for="cartNotEmpty"><fmt:message key="shoppingCart" />:</label>
		                            <select class="form-control input-sm" id="cartNotEmpty" name="cartNotEmpty">
	                                	<option value=""></option>
							         	<option value="true" <c:if test="${customerSearch.cartNotEmpty != null and customerSearch.cartNotEmpty}" >selected</c:if>><fmt:message key="notEmpty" /></option>
						             	<option value="false" <c:if test="${customerSearch.cartNotEmpty != null and not customerSearch.cartNotEmpty}" >selected</c:if>><fmt:message key="empty" /></option>	  
		                            </select>
		                        </div>
		                        
		                        <div class="form-group">
		                            <label for="shoppingCartTotal"><fmt:message key="shoppingCartTotal" />:</label> 
		                            <input name="shoppingCartTotal" type="text" value="<fmt:formatNumber value="${customerSearch.shoppingCartTotal}" pattern="###0.00"/>" class="form-control input-sm" />
		                        </div> 
						    </c:if>
	
	                        <div class="form-group">
	                            <label for="loggedInFlag"><fmt:message key="loggedIn" />:</label>
	                            <select class="form-control input-sm" id="loggedInFlag" name="loggedInFlag"> 
	                                <option value=""></option>
		         					<option value="1" <c:if test="${customerSearch.loggedInFlag == '1'}" >selected</c:if>>Yes</option>  
	                            </select>
	                        </div>
	
	                        <div class="form-group">
	                            <label for="rating1"><fmt:message key="rating" /> 1:</label>
	                            <select class="form-control input-sm" id="rating1" name="rating1">
	                            	<option value=""></option>
						         	<c:forTokens items="${siteConfig['CUSTOMER_RATING_1'].value}" delims="," var="rating1">
									   <option value="${rating1}" <c:if test="${customerSearch.rating1 == rating1}" >selected</c:if>><c:out value="${rating1}" /></option>
							  	   	</c:forTokens>
	                            </select>
	                        </div>
	
	                        <div class="form-group">
	                            <label for="rating2"><fmt:message key="rating" /> 2:</label>
	                            <select class="form-control input-sm" id="rating2" name="rating2">
	                                <option value=""></option>
		        					<c:forTokens items="${siteConfig['CUSTOMER_RATING_2'].value}" delims="," var="rating2">
					  					<option value="${rating2}" <c:if test="${customerSearch.rating2 == rating2}" >selected</c:if>><c:out value="${rating2}" /></option>
			  	 					</c:forTokens>
	                            </select>
	                        </div>
	
	                        <div class="form-group">
	                            <label for="suspended"><fmt:message key="suspend" />:</label>
	                            <select class="form-control input-sm" id="suspended" name="suspended">
	                                <option value=""></option>
		         					<option value="true" <c:if test="${customerSearch.suspended and customerSearch.suspended != null}">selected</c:if>><c:out value="suspended" /></option>
		         					<option value="false" <c:if test="${!customerSearch.suspended and customerSearch.suspended != null}">selected</c:if>><c:out value="active" /></option>
	                            </select>
	                        </div>
							
							<c:if test="${gSiteConfig['gCUSTOMER_SUPPLIER']}">
								<div class="form-group">
		                            <label for="supplier"><fmt:message key="supplier"/>:</label>
		                            <select class="form-control input-sm" id="supplier" name="supplier">
	                                	<option value=""></option>
							         	<option value="1" <c:if test="${customerSearch.supplier == '1'}">selected</c:if>>Yes</option>
							  	     	<option value="2" <c:if test="${customerSearch.supplier == '2'}">selected</c:if>>No</option>	
		                            </select>
		                        </div> 
						    </c:if>
		    
		    				<c:if test="${gSiteConfig['gMASS_EMAIL']}">
		                        <div class="form-group">
		                            <label for="unsubscribe"><fmt:message key="unsubscribe" /> <fmt:message key="email" />:</label>
		                            <select class="form-control input-sm" id="unsubscribe" name="unsubscribe">
		                                <option value=""></option>
		         						<option value="1" <c:if test="${customerSearch.unsubscribe == '1'}" >selected</c:if>>Yes</option>
	           							<option value="0" <c:if test="${customerSearch.unsubscribe == '0'}" >selected</c:if>>No</option>	       
		                            </select>
		                        </div>
	                        </c:if>
	
	                        <div class="form-group">
	                            <label for="taxId"><fmt:message key="taxId" />:</label>
	                            <select class="form-control input-sm" id="taxId" name="taxId">
	                                <option value=""></option>
		         					<option value="1" <c:if test="${customerSearch.taxId == '1'}" >selected</c:if>>Yes</option>
	             					<option value="0" <c:if test="${customerSearch.taxId == '0'}" >selected</c:if>>No</option>	     
	                            </select>
	                        </div>
	
	                        <div class="form-group">
	                            <label for="trackcode"><fmt:message key="trackcode" />:</label> 
	                            <input name="trackcode" id="trackcode" type="text" value="<c:out value='${customerSearch.trackcode}' />" class="form-control input-sm" />
	                        </div>
	
	                        <div class="form-group">
	                            <label for="registeredBy"><fmt:message key="registeredBy" />:</label> 
	                            <input name="registeredBy" id="registeredBy" type="text" value="<c:out value='${customerSearch.registeredBy}' />" class="form-control input-sm" />
	                        </div>
	
	                        <div class="form-group">
	                            <label for="zipcode"><fmt:message key="zipcode" />:</label> 
	                            <input name="zipcode" id="zipcode" type="text" value="<c:out value='${customerSearch.zipcode}' />" class="form-control input-sm" />
	                        </div>
	
	                        <div class="form-group">
	                            <label for="phone"><fmt:message key="phone" />:</label> 
	                            <input name="phone" type="text" value="<c:out value='${customerSearch.phone}' />" class="form-control input-sm" id="phone"/>
	                        </div>
							
	                        <div class="form-group">
	                            <label for="cellPhone"><fmt:message key="cellPhone" />:</label> 
	                            <input name="cellPhone" type="text" value="<c:out value='${customerSearch.cellPhone}' />"class="form-control input-sm" id="cellPhone"/>
	                        </div>
	
	                        <div class="form-group">
	                            <label for="cardId"><fmt:message key="idCard" />:</label> 
	                            <input name="cardId" type="text" value="<c:out value='${customerSearch.cardId}' />" class="form-control input-sm" id="cardId" />
	                        </div>
	
	                        <div class="form-group">
	                            <label for="city"><fmt:message key="city" />:</label> 
	                            <input name="city" type="text" value="<c:out value='${customerSearch.city}' />" class="form-control input-sm" id="city" />
	                        </div>
	
	                        <div class="form-group">
	                            <label for="state"><fmt:message key="state" />:</label> 
	                            <input name="state" type="text" id="state" value="<c:out value='${customerSearch.state}' />" class="form-control input-sm" />
	                        </div>
	
	                        <div class="form-group">
	                            <label for="country"><fmt:message key="country" />:</label>
	                            <select class="form-control input-sm" id="country" name="country">
	                                <option value="" ></option>
						            <c:forEach items="${model.countries}" var="country" varStatus="status">
						              <option value="${country.code}" <c:if test="${customerSearch.country == country.code}">selected</c:if>>${country.name}</option>
						            </c:forEach>
	                            </select>
	                        </div>
	
	                        <div class="form-group">
	                            <label for="region"><fmt:message key="region" />:</label>
	                            <select class="form-control input-sm" id="region" name="region">
	                               	<option value="" ></option>
					             	<option value="non USA"<c:if test="${customerSearch.region == 'non USA'}">selected</c:if>>non USA</option>
					            	<c:forEach items="${model.region}" var="region" varStatus="status">
					              		<option value="${region.region}"<c:if test="${customerSearch.region == region.region}">selected</c:if>>${region.region}</option>
						            </c:forEach>
	                            </select>
	                        </div>
	
	                        <div class="form-group">
	                            <label for="nationalRegion"><fmt:message key="nationalRegion" />:</label>
	                            <select class="form-control input-sm" id="nationalRegion" name="nationalRegion">
	                               	<option value="" ></option>
						            <c:forEach items="${model.nationalRegion}" var="nationalRegion" varStatus="status">
						              <option value="${nationalRegion.region}"<c:if test="${customerSearch.nationalRegion == nationalRegion.region}">selected</c:if>>${nationalRegion.region}</option>
						            </c:forEach>
	                            </select>
	                        </div>
						
						 	<c:if test="${model.languageCodes != null}">
		                        <div class="form-group">
		                            <label for="language"><fmt:message key="language" />:</label>
		                            <select class="form-control input-sm" id="language" name="language">
		                                <option value="" ></option>
							            <option value="en" <c:if test="${customerSearch.languageCode == 'en'}">selected</c:if> ><fmt:message key="language_en"/></option>
							            <c:forEach items="${model.languageCodes}" var="i18n">         	
							          	  <option value="${i18n.languageCode}" <c:if test="${customerSearch.languageCode == i18n.languageCode}">selected</c:if>><fmt:message key="language_${i18n.languageCode}"/></option>
							        	</c:forEach>
		                            </select>
		                        </div>
	                        </c:if>
	
	                        <div class="form-group">
	                            <label for="numOrder"><fmt:message key="orders"/>:<a href="#operatorNumOrder" rel="type:element" id="mbNumOrderOperator1" class="mbNumOrderOperator" style="margin-left: 55px;"><img class="toolTipImg" title="Advanced Search::Click to see more Options" src="../graphics/question.gif" /></a></label>
	                          	<div id="operatorNumOrder" class="miniWindowWrapper searchOp displayNone" style="display:none;">
							    	<div class="header">Advanced Search</div>
					    	  		<fieldset class="">
						    	    	<label class="AStitle"><fmt:message key="orders"/></label>
						    	    	<input name="numOrder" type="text" value="<c:out value='${customerSearch.numOrder}' />" size="15" disabled />
					    	 	 	</fieldset>
					    	  		<fieldset class="bottom">
				    	    			<label class="AStitle"><fmt:message key="operator"/></label>
						    	    	<div class="options">
						    	       		<label class="radio">
						    	       			<input type="radio" <c:if test="${customerSearch.numOrderOperator == 0}" >checked="checked"</c:if> value="0" name="numOrderOp" onchange="selectOperator(this.value, 'numOrder');" /> <strong> Greater Than (&gt;)</strong>
						    	       		</label>
						    	       		<label class="radio">
						    	       			<input type="radio" <c:if test="${customerSearch.numOrderOperator == 1}" >checked="checked"</c:if> value="1" name="numOrderOp" onchange="selectOperator(this.value, 'numOrder');" /> <strong> Equal (=) </strong>
						    	       		</label>
						    	       		<label class="radio">
						    	       			<input type="radio" <c:if test="${customerSearch.numOrderOperator == 2}" >checked="checked"</c:if> value="2" name="numOrderOp" onchange="selectOperator(this.value, 'numOrder');" /> <strong> Less Than (&lt;) </strong>
						    	       		</label>
						    	    	</div>
				    	  			</fieldset>
					    	  		<fieldset>
				    	     			<div class="button">
					      					<input type="submit" value="Search" onclick="document.searchform.submit();"/>
				      			 		</div>
					    	  		</fieldset>
						  		</div> 
	                       		<input name="numOrder" type="text" value="<c:out value='${customerSearch.numOrder}' />" class="form-control input-sm" id="numOrder" />
			  					<input type="hidden" name="numOrderOperator" id="numOrderOperator"/>
	                        </div>
	
	                        <div class="form-group">
	                            <label for="totalOrder">Order Total:<a href="#operatorTotalOrder" rel="type:element" id="mbTotalOrderOperator1" class="mbTotalOrderOperator" style="margin-left: 30px;"><img class="toolTipImg" title="Advanced Search::Click to see more Options" src="../graphics/question.gif" /></a></label> 
	                            <div id="operatorTotalOrder" class="miniWindowWrapper searchOp displayNone" style="display:none;">
							    	<div class="header">Advanced Search</div>
							    	  <fieldset class="">
							    	    <label class="AStitle"><fmt:message key="orderTotal"/></label>
							    	    <input name="totalOrder" type="text" value="<c:out value='${customerSearch.totalOrder}' />" size="15" disabled />
							    	  </fieldset>
							    	  <fieldset class="bottom">
							    	    <label class="AStitle"><fmt:message key="operator"/></label>
							    	    <div class="options">
							    	       <label class="radio">
							    	       		<input type="radio" <c:if test="${customerSearch.totalOrderOperator == 0}" >checked="checked"</c:if> value="0" name="totalOrderOp" onchange="selectOperator(this.value, 'totalOrder');" /> <strong> Greater Than (&gt;)</strong>
							    	       </label>
							    	       <label class="radio">
							    	       		<input type="radio" <c:if test="${customerSearch.totalOrderOperator == 1}" >checked="checked"</c:if> value="1" name="totalOrderOp" onchange="selectOperator(this.value, 'totalOrder');" /> <strong>Equal (=)</strong>
							    	       </label>
							    	       <label class="radio">
							    	       		<input type="radio" <c:if test="${customerSearch.totalOrderOperator == 2}" >checked="checked"</c:if> value="2" name="totalOrderOp" onchange="selectOperator(this.value, 'totalOrder');" /> <strong>Less Than (&lt;) </strong>
							    	       </label>
							    	    </div>
							    	  </fieldset>
							    	  <fieldset>
							    	     <div class="button">
							      			<input type="submit" value="Search" onclick="document.searchform.submit();"/>
						      			 </div>
							    	  </fieldset>
							  	</div>
	                           	<input name="totalOrder" type="text" value="<c:out value='${customerSearch.totalOrder}' />" class="form-control input-sm" id="totalOrder" />
		 					 	<input type="hidden" name="totalOrderOperator" id="totalOrderOperator"/>
	                        </div>
	
	                        <div class="form-group">
	                        	<c:set var="customerFieldFlag" value="false" />
	                            <select class="form-control input-sm" id="customerFieldNumber" name="customerFieldNumber">
	                              	 <c:forEach items="${model.customerFieldList}" var="customerField">
							  	     	<c:if test="${!empty customerField.name}" ><c:set var="customerFieldFlag" value="true" />
					  	    		 		<option style="font-size:normal;" value="${customerField.id}" <c:if test="${customerSearch.customerFieldNumber == customerField.id}">selected</c:if>><c:out value="${customerField.name}" />:</option>
							  	     	</c:if>
						  	    	</c:forEach>
	                            </select>
	                        </div>
						 	<c:if test="${customerFieldFlag}" >  
		                        <div class="form-group">
		                            <input type="text" class="form-control input-sm" id="customerField" name="customerField" value="<c:out value='${customerSearch.customerField}' />">
		                        </div>
	                        </c:if>
							<input type="submit" value="Search" class="btn btn-sm btn-primary btn-block"/>  
	
	                    </div>
	                    <div id="sidebar-right-tab2" class="tab-pane">
	                        <div class="form-group">
	                            <label for="start_datepicker">Start:</label>
	                            <div class="input-group">
	                                <span class="input-group-addon">
	                                    <span class="fa fa-calendar"></span>
	                                </span> 
	                                <input type="text" class="form-control" name="startDate" id="start_datepicker" 
											value="<fmt:formatDate type="date" pattern="${siteConfig['SITE_DATE_FORMAT'].value}" 
											timeZone="${siteConfig['SITE_TIME_ZONE'].value}" value="${customerSearch.startDate}"/>" 
											data-provide="datetimepicker" data-date-format="MM/DD/YYYY"  />  
	                            </div>
	                        </div>
	
	                        <div class="form-group">
	                            <label for="end_datepicker">End:</label>
	                            <div class="input-group">
	                                <span class="input-group-addon">
	                                    <span class="fa fa-calendar"></span>
	                                </span> 
	                                <input type="text" class="form-control" name="endDate" id="end_datepicker" 
											value="<fmt:formatDate type="date" pattern="${siteConfig['SITE_DATE_FORMAT'].value}" 
											timeZone="${siteConfig['SITE_TIME_ZONE'].value}" value="${customerSearch.endDate}"/>" 
											data-provide="datetimepicker" data-date-format="MM/DD/YYYY"  />  
	                            </div>
	                        </div>
	
	                        <div class="form-group">
	                            <div class="radio">
	                                <label> 
	                                    <input name="intervaldate" id="today" type="radio" onclick="UpdateStartEndDate(1);" />
	                                    Today
	                                </label>
	                            </div>
	                            <div class="radio">
	                                <label>
	                                	<input name="intervaldate" id="yesterday" type="radio" onclick="UpdateStartEndDate(0);" /> 
	                                    Yesterday
	                                </label>
	                            </div>
	                            <div class="radio">
	                                <label>
	                                	<input name="intervaldate" id="weekToDate" type="radio" onclick="UpdateStartEndDate(2);" /> 
	                                    Week To Date
	                                </label>
	                            </div>
	                            <div class="radio">
	                                <label>
	                                	<input name="intervaldate" id="last14days" type="radio" onclick="UpdateStartEndDate(3);" /> 
	                                    Last 14 Days
	                                </label>
	                            </div>
	                            <div class="radio">
	                                <label>
	                                	<input name="intervaldate" id="monthToDate" type="radio" onclick="UpdateStartEndDate(3);" /> 
	                                    Month To Date
	                                </label>
	                            </div>
	                            <div class="radio">
	                                <label>
	                                	<input name="intervaldate" id="yearToDate" type="radio" onclick="UpdateStartEndDate(10);" />  
	                                    Year To Date
	                                </label>
	                            </div>
	                            <div class="radio">
	                                <label>
	                                	<input name="intervaldate" id="reset" type="radio" onclick="UpdateStartEndDate(20);" /> 
	                                    Reset
	                                </label>
	                            </div>
	                        </div>
							
							<c:if test="${gSiteConfig['gSHOPPING_CART'] and siteConfig['SHOPPINGCART_SEARCH'].value == 'true'}">
		                        <h4>Shopping Cart</h4>
		
		                        <div class="form-group">
		                            <label for="start_datepicker">Start:</label>
		                            <div class="input-group">
		                                <span class="input-group-addon">
		                                    <span class="fa fa-calendar"></span>
		                                </span>
		                                <input type="text" class="form-control" name="cartStartDate" id="cartStartDate" 
											value="<fmt:formatDate type="date" pattern="${siteConfig['SITE_DATE_FORMAT'].value}" 
											timeZone="${siteConfig['SITE_TIME_ZONE'].value}" value="${customerSearch.cartStartDate}"/>" 
											data-provide="datetimepicker" data-date-format="MM/DD/YYYY"  />  
		                            </div>
		                        </div>
		
		                        <div class="form-group">
		                            <label for="end_datepicker">End:</label>
		                            <div class="input-group">
		                                <span class="input-group-addon">
		                                    <span class="fa fa-calendar"></span>
		                                </span>
	                                 	<input type="text" class="form-control" name="cartEndDate" id="cartEndDate" 
												value="<fmt:formatDate type="date" pattern="${siteConfig['SITE_DATE_FORMAT'].value}" 
												timeZone="${siteConfig['SITE_TIME_ZONE'].value}" value="${customerSearch.cartEndDate}"/>" 
												data-provide="datetimepicker" data-date-format="MM/DD/YYYY"  />  
		                             </div>
		                        </div>
		
		                        <div class="form-group">
		                            <div class="radio">
		                                <label> 
		                                    <input name="cartIntervalDate" id="today" type="radio" value="1" onclick="UpdateCartStartEndDate(1);" />
		                                    Today
		                                </label>
		                            </div>
		                            <div class="radio">
		                                <label>
		                                	<input name="cartIntervalDate" id="Yesterday" type="radio" value="2" onclick="UpdateCartStartEndDate(2);" /> 
		                                    Yesterday
		                                </label>
		                            </div>
		                            <div class="radio">
		                                <label> 
		                                    <input name="cartIntervalDate" id="weekToDate" type="radio" value="2" onclick="UpdateCartStartEndDate(2);" />
		                                    Week To Date
		                                </label>
		                            </div>
		                            <div class="radio">
		                                <label> 
		                                    <input name="cartIntervalDate" id="last14days" type="radio" value="3" onclick="UpdateCartStartEndDate(3);" />
		                                    Last 14 Days
		                                </label>
		                            </div>
		                            <div class="radio">
		                                <label> 
		                                    <input name="cartIntervalDate" id="monthToDate" type="radio" value="3" onclick="UpdateCartStartEndDate(3);" />
		                                    Month To Date
		                                </label>
		                            </div>
		                            <div class="radio">
		                                <label> 
		                                    <input name="cartIntervalDate" id="yearToDate" type="radio" value="10" onclick="UpdateCartStartEndDate(10);" />
		                                    Year To Date
		                                </label>
		                            </div>
		                            <div class="radio">
		                                <label> 
		                                    <input name="cartIntervalDate" id="reset" type="radio" value="20" onclick="UpdateCartStartEndDate(20);" />
		                                    Reset
		                                </label>
		                            </div>
		                        </div>
							</c:if>
	                        
	                        <h4>Last Login</h4>
	
	                        <div class="form-group">
	                            <label for="start_datepicker">Start:</label>
	                            <div class="input-group">
	                                <span class="input-group-addon">
	                                    <span class="fa fa-calendar"></span>
	                                </span>
	                                <input type="text" class="form-control" name="loginStartDate" id="loginStartDate" 
												value="<fmt:formatDate type="date" pattern="${siteConfig['SITE_DATE_FORMAT'].value}" 
												timeZone="${siteConfig['SITE_TIME_ZONE'].value}" value="${customerSearch.loginStartDate}"/>" 
												data-provide="datetimepicker" data-date-format="MM/DD/YYYY"  />
	                             </div>
	                        </div>
	
	                        <div class="form-group">
	                            <label for="end_datepicker">End:</label>
	                            <div class="input-group">
	                                <span class="input-group-addon">
	                                    <span class="fa fa-calendar"></span>
	                                </span>
	                                <input type="text" class="form-control" name="loginEndDate" id="loginEndDate" 
												value="<fmt:formatDate type="date" pattern="${siteConfig['SITE_DATE_FORMAT'].value}" 
												timeZone="${siteConfig['SITE_TIME_ZONE'].value}" value="${customerSearch.loginEndDate}"/>" 
												data-provide="datetimepicker" data-date-format="MM/DD/YYYY"  />
	                             </div>
	                        </div>
	
	                        <div class="form-group">
	                            <div class="radio">
	                                <label> 
	                                    <input  name="loginIntervalDate" id="today" type="radio" value="1" onclick="UpdateLoginStartEndDate(1);" />
	                                    Today
	                                </label>
	                            </div>
	                            <div class="radio">
	                                <label>
	                                    <input name="loginIntervalDate" id="yesterday" type="radio" value="2" onclick="UpdateLoginStartEndDate(2);" />
	                                    Yesterday
	                                </label>
	                            </div>
	                            <div class="radio">
	                                <label>
	                                    <input name="loginIntervalDate" id="weekToDate" type="radio" value="2" onclick="UpdateLoginStartEndDate(2);" />
	                                    Week To Date
	                                </label>
	                            </div>
	                            <div class="radio">
	                                <label>
	                                	<input name="loginIntervalDate" id="last14Days" type="radio" value="3" onclick="UpdateLoginStartEndDate(3);" /> 
	                                    Last 14 Days
	                                </label>
	                            </div>
	                            <div class="radio">
	                                <label>
	                                	<input name="loginIntervalDate" id="monthToDate" type="radio" value="3" onclick="UpdateLoginStartEndDate(3);" /> 
	                                    Month To Date
	                                </label>
	                            </div>
	                            <div class="radio">
	                                <label>
	                                	<input name="loginIntervalDate" id="yearToDate" type="radio" value="10" onclick="UpdateLoginStartEndDate(10);" /> 
	                                    Year To Date
	                                </label>
	                            </div>
	                            <div class="radio">
	                                <label>
	                                	<input name="loginIntervalDate" id="reset" type="radio" value="20" onclick="UpdateLoginStartEndDate(20);" /> 
	                                    Reset
	                                </label>
	                            </div>
	                            <!-- Another Search Button  -->
	                            <input type="submit" value="Search" class="btn btn-sm btn-primary btn-block"/>
	                        </div>
	                    </div>
	                </div>
	            </form>
            </c:if>
            <!-- Below Needs to be IMPLMENTED... -->
            <c:if test="${param.tab == 'specialPricing'}"> 
		      <div align="left" class="searchHeader" id="searchHeaderId"><h2><fmt:message key="search" /></h2></div>
			  <table class="searchBoxLeft" width="100%"><tr><td valign="top" style="width: 110px;">
			  <form action="specialPricing.jhtm" name="searchform" method="post">	
			  <!-- content area -->
			    <div class="search2">
			      <p>Sku:</p>
			      <input name="_sku_search" type="text" value="<c:out value='${spSearch.sku}' />" size="15" />
			    </div>     
			    <div class="button">
			      <input type="submit" value="Search"/>
			    </div>
			  <!-- content area -->
			  </form>
			  </td></tr></table>
			<div class="menudiv"></div>    
			</c:if> 
			
		     	
			
			<c:if test="${param.tab == 'groupLogoImport'}"> 
				<%--No Search --%>
			</c:if> 
				
			<c:if test="${param.tab == 'vbaPaymentReport'}"> 
			<h2 class="menuleft mfirst"><fmt:message key="paymentMenuTitle"/></h2>
		    <div class="menudiv"></div>
		    <ul id="nav">
		      <sec:authorize ifAnyGranted="ROLE_AEM,ROLE_ADMIN_${prop_site_id},ROLE_INCOMING_PAYMENT_VIEW_LIST">
		      	<li><a href="payments.jhtm" class="userbutton"><fmt:message key="incomingPayments"/></a></li>
		      </sec:authorize>
		      <sec:authorize ifAnyGranted="ROLE_AEM,ROLE_ADMIN_${prop_site_id},ROLE_OUTGOING_PAYMENT_VIEW_LIST">
		      <li><a href="virtualBankPayment.jhtm" class="userbutton"><fmt:message key="outgoingPayments"/></a>
		      <ul>
		      	<li><a href="vbaPaymentReport.jhtm" class="userbutton"><fmt:message key="cancel"/> <fmt:message key="payments"/></a></li>
		      </ul>
		      </li>
		      </sec:authorize>
		    </ul>
		      <div align="left" class="searchHeader" id="searchHeaderId"><h2><fmt:message key="search" /></h2></div>
			  <form action="vbaPaymentReport.jhtm" method="post" >
			  <table class="searchBoxLeft" width="100%"><tr><td valign="top" style="width:110px;">
			  <!-- content area -->
			    <div class="search2">
			    <p><fmt:message key="paymentMethod" />:</p>
			      <input name="paymentMethod" type="text" value="<c:out value='${vbaPaymentReportFilter.paymentMethod}' />">
			    </div>
			    <div class="search2">
			      <p><fmt:message key="reportView" />:</p>
			      <select name="reportView">
					<option value="consignment" <c:if test="${vbaPaymentReportFilter.reportView == 'consignment'}">selected</c:if>><fmt:message key="consignment" /></option>
					<option value="affiliate" <c:if test="${vbaPaymentReportFilter.reportView == 'affiliate'}">selected</c:if>><fmt:message key="affiliate" /></option>
			      </select>
			  	</div>
			    <div class="search2">
			    <p><fmt:message key="transactionID" />:</p>
			      <input name="transactionId" type="text" value="<c:out value='${vbaPaymentReportFilter.transactionId}' />">
			    </div>
			    <div class="search2">
			    <p><fmt:message key="supplier"/> <fmt:message key="email" />:</p>
			    	<input name="userName" type="text" value="<c:out value='${model.userName}' />">
			    </div>
				<div class="search2">
			      <p><fmt:message key="productSku"/></p>
			      <input name="productSku" type="text" value="<c:out value='${vbaPaymentReportFilter.productSku}' />" size="15" />
			    </div>	   
			    <div class="button">
			      <input type="submit" value="Search"/>
				</div>
			  <!-- content area -->
			  </td>	  	  
			  </tr>
			  </table>
			  </form>
			<div class="menudiv"></div>    
			</c:if>
        </div>
    </div>
</aside>
<!-- End: Right Sidebar -->
