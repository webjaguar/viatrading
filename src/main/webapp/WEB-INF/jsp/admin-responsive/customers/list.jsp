<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="/WEB-INF/tlds/spring.tld" prefix="spring" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ taglib uri="http://www.springframework.org/security/tags" prefix="sec" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ taglib uri="/WEB-INF/tlds/custom.tld" prefix="wj" %>

<tiles:insertDefinition name="admin.responsive-template.customers" flush="true">
 	<tiles:putAttribute name="css">
	 	<style>
	 		#grid .field-username{
			    width: 200px;
			}
			#grid .field-company{
			    width: 250px;
			}
			#grid .field-created{
			    width: 150px;
			}
			#grid .field-imported{
			    text-align: center;
			}
			#grid .field-registeredBy{
			    width: 120px;
			}
			#grid .field-num_of_logins{
			    text-align: right;
			}
			#grid .field-orderCount{
			    text-align: right;
			}
			#grid .field-ordersGrandTotal{
			    text-align: right;
			}
			#grid .field-trackcode{
			    width: 120px;
			}
		</style>
	</tiles:putAttribute>
	<tiles:putAttribute name="content" type="string">
		<sec:authorize ifAnyGranted="ROLE_AEM,ROLE_ADMIN_${prop_site_id},ROLE_CUSTOMER_VIEW_LIST">
			<form action="customerList.jhtm" method="post" id="list" name="list_form">
				<div class="page-banner">
					<div class="breadcrumb"><span><a href="${_contextpath}/admin/catalog/dashboard.jhtm">Home</a></span><i class="sli-arrow-right"></i><span><a href="${_contextpath}/admin/customers/customerList.jhtm">Customers</a></span><i class="sli-arrow-right"></i><span>Customers</span></div>
				</div>
				<div class="page-head">
					<div class="row justify-content-between">
						<div class="col-sm-8 title">
							<h3>Customers</h3>
						</div>
						<div class="col-sm-4 actions">
							<div class="dropdown dropdown-actions">
								<button type="button" class="btn-outline dropdown-toggle active btn btn-primary" data-toggle="dropdown">
		                             <i class="sli-settings"></i> Actions
		                         </button>
		                         <div class="dropdown-menu dropdown-menu-right">
			                        <sec:authorize ifAnyGranted="ROLE_AEM,ROLE_ADMIN_${prop_site_id},ROLE_CUSTOMER_CREATE">
									    	<a class="dropdown-item"><fmt:message key="addCustomersMenuTitle"/></a>
								  	</sec:authorize>
								  	<c:if test="${model.count > 0 and siteConfig['ECHOSIGN_API_KEY'].value != ''}">
								  		<a class="dropdown-item"><fmt:message key="send" /> EchoSign</a>
								  	</c:if>
		                         </div>
		                    </div>
		                    <div class="dropdown dropdown-setting">
								<button type="button" class="btn-outline dropdown-toggle active btn btn-primary" data-toggle="dropdown">
		                             <i class="sli-screen-desktop"></i> Display
		                         </button>	                         
								<div class="dropdown-menu dropdown-menu-right">
									<div class="form-group">
				                    		<div class="option-header">Rows</div>
										<select class="custom-select form-control" name="size" onchange="document.getElementById('page').value=1;submit();">
											<c:forTokens items="10,25,50,100,200,500" delims="," var="current">
									  	  		<option value="${current}" <c:if test="${current == customerSearch.pageSize}">selected</c:if>>${current}</option>
								  			</c:forTokens>
										</select>
									</div>
									<div class="form-group">
				                    		<div class="option-header">Columns</div>
				                    		<div id="column-hider"></div>
									</div>
								</div>
		                    </div>
						</div>
					</div>
				</div>
				<div class="page-body">
					<div class="grid-stage">
						<c:if test="${model.count > 0}">
							<div class="page-stat">
								<fmt:message key="showing">
									<fmt:param value="${customerSearch.offset + 1}"/>
									<fmt:param value="${model.pageEnd}"/>
									<fmt:param value="${model.count}"/>
								</fmt:message>
							</div>
					  	</c:if>
					    <table id="grid"></table>
					    <div class="footer">
			                <div class="float-right">
								<c:if test="${customerSearch.page == 1}">
							 		<span class="previous"><i class="sli-arrow-left" style="font-size: 0.7rem"></i> Previous</span>
								</c:if>
								<c:if test="${customerSearch.page != 1}">
									<a href="#" onClick="document.getElementById('page').value='${customerSearch.page-1}';document.getElementById('list').submit()">
										<span class="previous"><i class="sli-arrow-left" style="font-size: 0.7rem"></i> Previous</span>
									</a>
								</c:if>	
								<span class="status"><c:out value="${customerSearch.page}" /> of <c:out value="${model.pageCount}" /></span>
								<c:if test="${customerSearch.page == model.pageCount}">
  									<span class="next">Next <i class="sli-arrow-right" style="font-size: 0.7rem"></i></span>
 								</c:if>
								<c:if test="${customerSearch.page != model.pageCount}">
  									<a href="#" onClick="document.getElementById('page').value='${customerSearch.page+1}';document.getElementById('list').submit()">
										<span class="next">Next <i class="sli-arrow-right" style="font-size: 0.7rem"></i></span>
									</a>
 								</c:if>
							</div>
						</div>
					</div>
				</div>
				<input type="hidden" id="cid" name="cid"> 
				<input type="hidden" id="sort" name="sort" value="${customerSearch.sort}" />
				<input type="hidden" id="page" name="page" value="${customerSearch.page}"/>
			</form>		
		</sec:authorize>
	</tiles:putAttribute>
	<tiles:putAttribute name="js">
		<!-- DGRID TABLE SCRIPT MUST BE AT BOTTOM OF THE PAGE -->
		<script src="${_contextpath}/admin-responsive-static/assets/plugins/dojo/dojo.js" data-dojo-config="async: true"></script>
		<script>
			//Table Data Sanitization
			var data = [];
			<c:forEach items="${model.customers}" var="customer" varStatus="status">
				data.push({
					id: '${customer.id}',
					<c:if test="${customer.cardId != null}">
						cardId: "${customer.cardId}",
					</c:if>
					<c:if test="${wj:contains(model.loggedIn,customer.id, null)}">
			       		online: true,
			    		</c:if>
					<c:if test="${fn:contains(siteConfig['CUSTOMIZE_CUSTOMER_LIST'].value,'lastName')}">
				    		last_name: '${wj:escapeJS(customer.address.lastName)}',
			    		</c:if>
					<c:if test="${fn:contains(siteConfig['CUSTOMIZE_CUSTOMER_LIST'].value,'firstName')}">
						first_name: '${wj:escapeJS(customer.address.firstName)}',
			    		</c:if>
					<c:if test="${fn:contains(siteConfig['CUSTOMIZE_CUSTOMER_LIST'].value,'email')}">
		  				username: '${wj:escapeJS(customer.username)}',
		  				<c:if test="${gSiteConfig['gCUSTOMER_SUPPLIER'] and customer.supplierId != null}">
				    			hasSupplier: true,
				    		</c:if>
				    		<c:if test="${customer.seeHiddenPrice}">
    							seeHiddenPrice: true,
 						</c:if>
					</c:if>
					<c:if test="${fn:contains(siteConfig['CUSTOMIZE_CUSTOMER_LIST'].value,'company')}">
	    					company: "${wj:escapeJS(customer.address.company)}",
	    				</c:if>
					<c:if test="${fn:contains(siteConfig['CUSTOMIZE_CUSTOMER_LIST'].value,'accountNumber')}">
			    			accountNumber: "${wj:escapeJS(customer.accountNumber)}",
			  		</c:if> 
		    			<c:if test="${fn:contains(siteConfig['CUSTOMIZE_CUSTOMER_LIST'].value,'phone')}">
					    phone: '${customer.address.phone}',
				    </c:if>
				    <c:if test="${fn:contains(siteConfig['CUSTOMIZE_CUSTOMER_LIST'].value,'taxId') and gSiteConfig['gTAX_EXEMPTION']}">
						taxId: "${customer.taxId}",
				    </c:if>
					<c:if test="${fn:contains(siteConfig['CUSTOMIZE_CUSTOMER_LIST'].value,'credit')}">
						credit: '<fmt:formatNumber value="${customer.credit}" pattern="#,##0.00" />',
					</c:if>
					<c:if test="${fn:contains(siteConfig['CUSTOMIZE_CUSTOMER_LIST'].value,'created')}">
			    			created: '<fmt:formatDate type="date" timeStyle="default" timeZone="${siteConfig[\'SITE_TIME_ZONE\'].value}" value="${customer.created}"/>',
			    		</c:if>
		    			<c:if test="${fn:contains(siteConfig['CUSTOMIZE_CUSTOMER_LIST'].value,'cellPhone')}">
			    			cellPhone: "${customer.address.cellPhone}"
	    				</c:if>
					<c:if test="${fn:contains(siteConfig['CUSTOMIZE_CUSTOMER_LIST'].value,'imported')}">
						<c:if test="${customer.imported}">
							imported: true,
						</c:if>
					</c:if>
					<c:if test="${fn:contains(siteConfig['CUSTOMIZE_CUSTOMER_LIST'].value,'registeredBy')}">
						registeredBy: '${wj:escapeJS(customer.registeredBy)}',
					</c:if>
					<c:if test="${fn:contains(siteConfig['CUSTOMIZE_CUSTOMER_LIST'].value,'logins')}">
						num_of_logins: '${customer.numOfLogins}',
					</c:if>
					<c:if test="${(gSiteConfig['gSHOPPING_CART']) and (fn:contains(siteConfig['CUSTOMIZE_CUSTOMER_LIST'].value,'orders'))}">
						ordersURL: "../orders/ordersList.jhtm?email=${customer.username}",
						orderCount: '${customer.orderCount}',
						ordersGrandTotal: '<fmt:formatNumber value="${customer.ordersGrandTotal}" pattern="#,##0.00" />',
				    </c:if>
					<c:if test="${(siteConfig['PRODUCT_QUOTE'].value == 'true' or gSiteConfig['gSHIPPING_QUOTE']) and fn:contains(siteConfig['CUSTOMIZE_CUSTOMER_LIST'].value,'quote')}">
						quotesURL: "../orders/quotesList.jhtm?email=${customer.username}",
			    			quoteCount: "${model.customerQuoteMap[customer.id].quoteCount}",
					</c:if>
		    			<c:if test="${(gSiteConfig['gSALES_REP']) and (fn:contains(siteConfig['CUSTOMIZE_CUSTOMER_LIST'].value,'salesRep'))}">
		    				sales_rep_id: "${model.salesRepMap[customer.salesRepId].name}",
					</c:if>
		    			<c:if test="${(gSiteConfig['gMULTI_STORE'] > 0) and (fn:contains(siteConfig['CUSTOMIZE_CUSTOMER_LIST'].value,'multiStore'))}">
		    				host: '${wj:escapeJS(customer.host)}',
		    			</c:if>
		    			<sec:authorize ifAnyGranted="ROLE_AEM,ROLE_ADMIN_${prop_site_id},ROLE_CUSTOMER_LOGINAS">
						<c:choose>
							<c:when test="${(gSiteConfig['gMULTI_STORE'] > 0) and customer.host != null and customer.host != '' and (fn:contains(siteConfig['CUSTOMIZE_CUSTOMER_LIST'].value,'multiStore'))}">
								loginAsCustomerURL: "http://${customer.host}/admin/customers/loginAsCustomer.jhtm?cid=${customer.token}",
							</c:when>
							<c:otherwise>
								loginAsCustomerURL: "loginAsCustomer.jhtm?cid=${customer.token}",
							</c:otherwise>
						</c:choose>
		            </sec:authorize>
		            
		            <c:if test="${gSiteConfig['gSUB_ACCOUNTS']}">
						subCount: "${customer.subCount}",        
				    </c:if>
						
		            <c:if test="${gSiteConfig['gCRM'] && customer.crmContactId != null}">
			            crmAccountId: '${customer.crmAccountId}',
			            crmContactId: '${customer.crmContactId}',
		            </c:if>
			        addressURL: 'http://maps.google.com/?q=loc:${customer.address.addr1}+${customer.address.city}+${customer.address.stateProvince}+${customer.address.country}',
				});
			</c:forEach>

			//Columns Definitions
			require(['dojo/_base/lang',
					'dojo/_base/declare',
					'dstore/Memory',
					'dstore/Trackable',
					'dgrid/OnDemandGrid',
					'dgrid/Editor',
					'dgrid/extensions/ColumnHider',
					'dgrid/extensions/ColumnReorder',
					'dgrid/extensions/ColumnResizer',
					'dgrid/Selector',
					'dijit/form/DropDownButton',
					'dijit/DropDownMenu',
					'dijit/MenuItem'
				], function (lang, declare, Memory, Trackable, OnDemandGrid, Editor, ColumnHider, ColumnReorder, ColumnResizer, Selector, DropDownButton, DropDownMenu, MenuItem) {
				var columns = {
					batch: {
						selector: 'checkbox',
						unhidable: true
					},
					actions:{
						label: 'Actions',
						renderCell: function(object, data, td, options){
							var div = document.createElement("div");
							var menu = new DropDownMenu({
								style: "display: none;"
							});
							/*
							** SEARCH & MAP
							*/
							var menuItem;
							
							menuItem = new MenuItem({
						        label: 'View',
						        onClick: function(){
						        		
						        	}
						    });
						    menu.addChild(menuItem);
							
							<sec:authorize ifAnyGranted="ROLE_AEM,ROLE_ADMIN_${prop_site_id},ROLE_CUSTOMER_EDIT">
							    menuItem = new MenuItem({
							        label: '<fmt:message key="edit" />',
							        onClick: function(){
							        		location.href = 'customer.jhtm?id='+object.id;
							        }
							    });
							    menu.addChild(menuItem);
						    </sec:authorize>
						    
						    menuItem = new MenuItem({
						        label: '<fmt:message key="loginAsCustomer" />',
						        onClick: function(){
						        		window.open(object.loginAsCustomerURL,"_blank");
						        	}
						    });
						    menu.addChild(menuItem);
						    
						    menuItem = new MenuItem({
						        label: '<fmt:message key="addressList" />',
						        onClick: function(){
						        		location.href = 'addressList.jhtm?id='+object.id;
						        	}
						    });
						    menu.addChild(menuItem);
						    
						    menuItem = new MenuItem({
						        label: '<fmt:message key="sendEmail" />',
						        onClick: function(){
						        		location.href = 'email.jhtm?id='+object.id;
						        	}
						    });
						    menu.addChild(menuItem);
						    
						    <sec:authorize ifAnyGranted="ROLE_AEM,ROLE_ADMIN_${prop_site_id},ROLE_CUSTOMER_ADD_INVOICE">
								<c:if test="${gSiteConfig['gADD_INVOICE']}">
						            menuItem = new MenuItem({
								        label: '<fmt:message key="addOrder" />',
								        onClick: function(){
								        		location.href = '../orders/addInvoice.jhtm?cid='+object.id;
								        }
								    });
									menu.addChild(menuItem);
								</c:if>  
								<c:if test="${siteConfig['PRODUCT_QUOTE'].value == 'true' or gSiteConfig['gSHIPPING_QUOTE']}">
									menuItem = new MenuItem({
								        label: '<fmt:message key="addQuote" />',
								        onClick: function(){
								        		location.href = '../orders/addQuote.jhtm?cid='+object.id;
								        }
								    });
									menu.addChild(menuItem);
								</c:if>  
				            </sec:authorize>
				            
				            <c:if test="${gSiteConfig['gSUB_ACCOUNTS']}">
					            menuItem = new MenuItem({
							        label: '<fmt:message key="parent" />',
							        onClick: function(){
							        		location.href ='setParent.jhtm?cid=' + object.id + '&begin=t';
							        }
							    });
								menu.addChild(menuItem);
								
								menuItem = new MenuItem({
							        label: '<fmt:message key="subAccounts" /> (<b>'+object.subCount+'</b>)',
							        onClick: function(){
							        		location.href = 'customerList.jhtm?parent=' + object.id + '&emptyFilter=true';
							        }
							    });
								menu.addChild(menuItem);
						    </c:if>
						    
						    <c:if test="${siteConfig['gSUB_ACCOUNTS']}">
							    menuItem = new MenuItem({
							        label: '<fmt:message key="addPartners" />',
							        onClick: function(){
							        		location.href = 'customerList.jhtm?parent='+object.id+'&emptyFilter=true';
							        }
							    });
								menu.addChild(menuItem);
						    </c:if>
						    
						    <sec:authorize ifAnyGranted="ROLE_AEM,ROLE_ADMIN_${prop_site_id},ROLE_CUSTOMER_MYLIST_ADD,ROLE_CUSTOMER_MYLIST_DELETE,ROLE_CUSTOMER_MYLIST_VIEW">
					            <c:if test="${gSiteConfig['gMYLIST'] == 'true'}">
						            menuItem = new MenuItem({
								        label: '<fmt:message key="myList" />',
								        onClick: function(){
								        		location.href = 'myList.jhtm?id='+object.id;
								        }
								    });
									menu.addChild(menuItem);
					            </c:if>
				            </sec:authorize>
						    
				            <c:if test="${gSiteConfig['gSPECIAL_PRICING'] == 'true'}">
					            menuItem = new MenuItem({
							        label: '<fmt:message key="specialPricing" />',
							        onClick: function(){
							        		location.href = 'specialPricing.jhtm?id='+object.id;
							        }
							    });
								menu.addChild(menuItem);
				            </c:if>
				            
				            <c:if test="${gSiteConfig['gBUDGET_BRAND']}">
					            menuItem = new MenuItem({
							        label: '<fmt:message key="budgetByBrands" />',
							        onClick: function(){
							        		location.href = 'budgetByBrands.jhtm?id='+object.id;
							        }
							    });
								menu.addChild(menuItem);
				            </c:if>
				            
				            <c:if test="${gSiteConfig['gBUDGET_PRODUCT']}">
					            menuItem = new MenuItem({
							        label: '<fmt:message key="budgetByProducts" />',
							        onClick: function(){
							        		location.href = 'budgetByProducts.jhtm?cid='+object.id;
							        }
							    });
								menu.addChild(menuItem);
				            </c:if>
				            
				            <c:if test="${gSiteConfig['gAFFILIATE'] > 0 and customer.isAffiliate}" >
					            menuItem = new MenuItem({
							        label: '<fmt:message key="commission" />',
							        onClick: function(){
							        		location.href = '../customers/commissionList.jhtm?id='+object.id;
							        }
							    });
								menu.addChild(menuItem);
				            </c:if>
				            
				            <c:if test="${gSiteConfig['gREGISTRATION_TOUCH_SCREEN']}">
					            menuItem = new MenuItem({
							        label: '<fmt:message key="idCard" />: '+object.cardId,
							        onClick: function(){
							        		location.href = 'customerIdCard_'+object.id+'.pdf';
							        }
							    });
								menu.addChild(menuItem);
				              
								menuItem = new MenuItem({
							        label: '<fmt:message key="printSoldLabel" />',
							        onClick: function(){
							        		location.href = '../../touchPrintLabel.jhtm?__cardId='+object.cardId+'&__printPdf=true';
							        }
							    });
								menu.addChild(menuItem);
				            </c:if>
				            
						    /* Register to Event */
				            /* Last Login */
				            /* Last Modified */
				            
				            <c:if test="${gSiteConfig['gPAYMENTS']}">
								<sec:authorize ifAnyGranted="ROLE_AEM,ROLE_ADMIN_${prop_site_id},ROLE_INCOMING_PAYMENT_VIEW_LIST">
									menuItem = new MenuItem({
								        label: '<fmt:message key="incomingPayments" />',
								        onClick: function(){
								        		location.href = 'payments.jhtm?cid='+object.id;
								        }
								    });
									menu.addChild(menuItem);
								</sec:authorize>       
				            </c:if>
				            
				            /* Customer Credit */
				            /* Shopping Cart */
				            
				            <c:if test="${gSiteConfig['gTICKET']}">
					            menuItem = new MenuItem({
							        label: '<fmt:message key="openTicket" />',
							        onClick: function(){
							        		location.href = '../ticket/ticketList.jhtm?user_id='+object.id+'&status=open';
							        }
							    });
								menu.addChild(menuItem);
					        </c:if>
					        
					        <c:if test="${gSiteConfig['gCRM'] && customer.crmContactId != null}">
						        menuItem = new MenuItem({
							        label: '<fmt:message key="addTask" />',
							        onClick: function(){
							        		location.href = '../crm/crmTaskForm.jhtm?accountId='+object.crmAccountId+'&contactId='+object.crmContactId;
							        }
							    });
								menu.addChild(menuItem);
								
								menuItem = new MenuItem({
							        label: '<fmt:message key="viewTask" />',
							        onClick: function(){
							        		location.href = '../crm/crmTaskList.jhtm?contactId='+object.crmContactId;
							        }
							    });
								menu.addChild(menuItem);
				            </c:if>
				            <c:if test="${siteConfig['BUDGET_ADD_PARTNER'].value == 'true'}">
					            menuItem = new MenuItem({
							        label: '<fmt:message key="addPartner" />',
							        onClick: function(){
							        		location.href = '../customers/partnerCustomerList.jhtm?cId='+object.crmContactId;
							        }
							    });
								menu.addChild(menuItem);
					        </c:if>
						    menu.startup();

						    var dropdown = new DropDownButton({
						        iconClass: "sli-settings",
						        dropDown: menu,
						    },'button');
						    
						    div.appendChild(dropdown.domNode);
		                    return div;
						},
						sortable: false
					},
					<c:if test="${fn:contains(siteConfig['CUSTOMIZE_CUSTOMER_LIST'].value,'lastName')}">
						last_name: {
							label: '<fmt:message key="lastName" />',
							get: function(object){
								return object.last_name;
							}
						},
					</c:if>
					<c:if test="${fn:contains(siteConfig['CUSTOMIZE_CUSTOMER_LIST'].value,'firstName')}">
						first_name: {
							label: '<fmt:message key="firstName" />',
							get: function(object){
								return object.first_name;
							}
						},
					</c:if>
					<c:if test="${fn:contains(siteConfig['CUSTOMIZE_CUSTOMER_LIST'].value,'email')}">
						username: {
							label: 'Email Address',
							renderCell: function(object, data, td, options){
								var span = document.createElement("span");
								span.innerHTML = object.username;
								if(object.hasSupplier){
									var sup = document.createElement("sup");
									sup.innerHTML = '1';
									span.appendChild(sup);
								}
								if(object.seeHiddenPrice){
									var sup = document.createElement("sup");
									sup.innerHTML = '2';
									span.appendChild(sup);
								}
								return span;
							}
						},
					</c:if>
					<c:if test="${fn:contains(siteConfig['CUSTOMIZE_CUSTOMER_LIST'].value,'company')}">
						company: {
							label: '<fmt:message key="company" />',
							get: function(object){
								return object.company;
							}
						},
					</c:if>
					<c:if test="${fn:contains(siteConfig['CUSTOMIZE_CUSTOMER_LIST'].value,'accountNumber')}">
						accountNumber:{
							label: 'Account #',
							get: function(object){
								return object.accountNumber;
							},
							sortable: false
						},
					</c:if>
					<c:if test="${fn:contains(siteConfig['CUSTOMIZE_CUSTOMER_LIST'].value,'phone')}">
						phone:{
							label: '<fmt:message key="phone" />',
							get: function(object){
								return object.phone;
							},
							sortable: false
						},
				    </c:if>
					<c:if test="${fn:contains(siteConfig['CUSTOMIZE_CUSTOMER_LIST'].value,'taxId') and gSiteConfig['gTAX_EXEMPTION']}">
						taxId:{
							label: '<fmt:message key="taxIdShort" />',
							get: function(object){
								return object.taxId;
							},
							sortable: false
						},
					</c:if>
					<c:if test="${fn:contains(siteConfig['CUSTOMIZE_CUSTOMER_LIST'].value,'credit')}">
						credit:{
							label: '<fmt:message key="taxIdShort" />',
							get: function(object){
								return object.credit;
							},
							sortable: false
						},
					</c:if>
					
					<c:if test="${fn:contains(siteConfig['CUSTOMIZE_CUSTOMER_LIST'].value,'created')}">
						created:{
							label: '<fmt:message key="accountCreated" />',
							get: function(object){
								return object.created;
							}
						},
					</c:if>
					<c:if test="${fn:contains(siteConfig['CUSTOMIZE_CUSTOMER_LIST'].value,'cellPhone')}">
						cellPhone:{
							label: '<fmt:message key="cellPhone" />',
							get: function(object){
								return object.cellPhone;
							},
							sortable: false
						},
				    </c:if>
						
					/*
					    <c:if test="${fn:contains(siteConfig['CUSTOMIZE_CUSTOMER_LIST'].value,'zipcode')}">
					    <td align="center"><c:out value="${customer.address.zip}"/></td>
					    </c:if>
					    <c:if test="${fn:contains(siteConfig['CUSTOMIZE_CUSTOMER_LIST'].value,'address')}">
					    <td align="center"><c:out value="${customer.address.addr1}"/></td>
					    </c:if>
					    <c:if test="${fn:contains(siteConfig['CUSTOMIZE_CUSTOMER_LIST'].value,'city')}">
					    <td align="center"><c:out value="${customer.address.city}"/></td>
					    </c:if>
					    <c:if test="${fn:contains(siteConfig['CUSTOMIZE_CUSTOMER_LIST'].value,'stateProvince')}">
					    <td align="center"><c:out value="${customer.address.stateProvince}"/></td>
					    </c:if>
					    <c:if test="${fn:contains(siteConfig['CUSTOMIZE_CUSTOMER_LIST'].value,'echoSign')}">
					    <td align="center"><c:if test="${model.echoSign[customer.id] != null}"></c:if><a href="<c:out value="${model.echoSign[customer.id].documentUrl}"/>" target="_blank"><c:out value="${model.echoSign[customer.id].documentName}"/></a></td>
					    </c:if>
					*/
					
					<c:if test="${fn:contains(siteConfig['CUSTOMIZE_CUSTOMER_LIST'].value,'imported')}">
						imported:{
							label: '<fmt:message key="imported" />',
							renderCell: function(object, data, td, options){
								var div = document.createElement("div");
								if(object.imported){
									var i = document.createElement("i");
									i.className = 'sli-check';
									div.appendChild(i);
								}
								return div;
							},
							sortable: false
						},
					</c:if>
					<c:if test="${fn:contains(siteConfig['CUSTOMIZE_CUSTOMER_LIST'].value,'registeredBy')}">
						registeredBy:{
							label: 'Registered By',
							get: function(object){
								return object.registeredBy;
							},
							sortable: false
						},
					</c:if>
					<c:if test="${fn:contains(siteConfig['CUSTOMIZE_CUSTOMER_LIST'].value,'logins')}">
						num_of_logins:{
							label: '<fmt:message key="numOfLogins" />',
							get: function(object){
								return object.num_of_logins;
							}
						},
					</c:if>
					<c:if test="${(gSiteConfig['gSHOPPING_CART']) and (fn:contains(siteConfig['CUSTOMIZE_CUSTOMER_LIST'].value,'orders'))}">
					    orderCount:{
							label: '<fmt:message key="orderCount" />',
							renderCell: function(object, data, td, options){
								var link = document.createElement("a");
								link.innerHTML = object.orderCount;
								link.href = object.ordersURL;
								return link;
							},
							sortable: false
						},
						ordersGrandTotal:{
							label: '<fmt:message key="orderTotal" />',
							get: function(object){
								return object.ordersGrandTotal;
							},
							sortable: false
						},
					</c:if>
					<c:if test="${(siteConfig['PRODUCT_QUOTE'].value == 'true' or gSiteConfig['gSHIPPING_QUOTE']) and fn:contains(siteConfig['CUSTOMIZE_CUSTOMER_LIST'].value,'quote')}">
						quoteCount:{
							label: '<fmt:message key="quoteCount" />',
							renderCell: function(object, data, td, options){
								var link = document.createElement("a");
								link.innerHTML = object.quoteCount;
								link.href = object.quotesURL;
								return link;
							},
							sortable: false
						},
					</c:if>
					<c:if test="${fn:contains(siteConfig['CUSTOMIZE_CUSTOMER_LIST'].value,'trackCode')}">
						trackcode:{
							label: '<fmt:message key="trackcode" />',
							get: function(object){
								return object.trackcode;
							}
						},
					</c:if>
					<c:if test="${(gSiteConfig['gSALES_REP']) and (fn:contains(siteConfig['CUSTOMIZE_CUSTOMER_LIST'].value,'salesRep'))}">
						sales_rep_id:{
							label: '<fmt:message key="salesRep" />',
							get: function(object){
								return object.sales_rep_id;
							}
						},
					</c:if>
					<c:if test="${(gSiteConfig['gMULTI_STORE'] > 0) and (fn:contains(siteConfig['CUSTOMIZE_CUSTOMER_LIST'].value,'multiStore'))}">
						host:{
							label: '<fmt:message key="multiStore" />',
							get: function(object){
								return object.host;
							},
							sortable: false
						},
					</c:if>
				};
				var store = new (declare([Memory, Trackable]))({
					data: data,
					idProperty:"id"
				});
				
				var grid = new (declare([OnDemandGrid, Editor, ColumnHider, ColumnReorder, ColumnResizer, Selector]))({
					collection: store,
					selectionMode: "none",
					allowSelectAll: true,
					columns: columns
				},'grid');
				
				grid.startup();
				//sorting columns
				grid.on("dgrid-sort", lang.hitch(this, function(e){
					e.preventDefault();						
					var property = e.sort[0].property;
				    var order = e.sort[0].descending ? "desc" : "asc";
				    document.getElementById('sort').value = property + ' ' + order;
				    document.getElementById('list').submit();
				}));
				
				<c:if test="${customerSearch.sort == 'last_name desc'}">
					grid.updateSortArrow([{property: 'last_name', descending: true}],true);
				</c:if>
				<c:if test="${customerSearch.sort == 'last_name asc'}">
					grid.updateSortArrow([{property: 'last_name', ascending: true}],true);
				</c:if>
				
				<c:if test="${customerSearch.sort == 'first_name desc'}">
					grid.updateSortArrow([{property: 'first_name', descending: true}],true);
				</c:if>
				<c:if test="${customerSearch.sort == 'first_name asc'}">
					grid.updateSortArrow([{property: 'first_name', ascending: true}],true);
				</c:if>
				
				<c:if test="${customerSearch.sort == 'username desc'}">
					grid.updateSortArrow([{property: 'username', descending: true}],true);
				</c:if>
				<c:if test="${customerSearch.sort == 'username asc'}">
					grid.updateSortArrow([{property: 'username', ascending: true}],true);
				</c:if>
				
				<c:if test="${customerSearch.sort == 'company desc'}">
					grid.updateSortArrow([{property: 'company', descending: true}],true);
				</c:if>
				<c:if test="${customerSearch.sort == 'company asc'}">
					grid.updateSortArrow([{property: 'company', ascending: true}],true);
				</c:if>
				
				<c:if test="${customerSearch.sort == 'created desc'}">
					grid.updateSortArrow([{property: 'created', descending: true}],true);
				</c:if>
				<c:if test="${customerSearch.sort == 'created asc'}">
					grid.updateSortArrow([{property: 'created', ascending: true}],true);
				</c:if>
				
				<c:if test="${customerSearch.sort == 'num_of_logins desc'}">
					grid.updateSortArrow([{property: 'num_of_logins', descending: true}],true);
				</c:if>
				<c:if test="${customerSearch.sort == 'num_of_logins asc'}">
					grid.updateSortArrow([{property: 'num_of_logins', ascending: true}],true);
				</c:if>
				
				<c:if test="${customerSearch.sort == 'trackcode desc'}">
					grid.updateSortArrow([{property: 'trackcode', descending: true}],true);
				</c:if>
				<c:if test="${customerSearch.sort == 'trackcode asc'}">
					grid.updateSortArrow([{property: 'trackcode', ascending: true}],true);
				</c:if>
				
				<c:if test="${customerSearch.sort == 'sales_rep_id desc'}">
					grid.updateSortArrow([{property: 'sales_rep_id', descending: true}],true);
				</c:if>
				<c:if test="${customerSearch.sort == 'sales_rep_id asc'}">
					grid.updateSortArrow([{property: 'sales_rep_id', ascending: true}],true);
				</c:if>
				
				grid.on('dgrid-refresh-complete', function(event) {
					$('#grid-hider-menu').appendTo('#column-hider');
				});
				
				grid.startup();
			});
		</script>
	</tiles:putAttribute>
</tiles:insertDefinition>