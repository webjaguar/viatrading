<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ taglib uri="http://www.springframework.org/security/tags" prefix="sec" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ taglib uri="/WEB-INF/tlds/spring.tld" prefix="spring" %>
<%@ taglib uri="/WEB-INF/tlds/custom.tld" prefix="wj" %>

<tiles:insertDefinition name="admin.responsive-template.customersForm" flush="true">
	<tiles:putAttribute name="css">
	</tiles:putAttribute>
	<tiles:putAttribute name="content" type="string">
		<sec:authorize ifAnyGranted="ROLE_AEM,ROLE_ADMIN_${prop_site_id},ROLE_CUSTOMER_EDIT,ROLE_CUSTOMER_CREATE,ROLE_CUSTOMER_DELETE">
			<form:form commandName="customerForm" method="post" enctype="multipart/form-data" class="page-form">
				<div class="page-banner">
					<div class="breadcrumb">
						<span><a href="${_contextpath}/admin/catalog/dashboard.jhtm">Home</a></span>
						<i class="sli-arrow-right"></i><span><a href="${_contextpath}/admin/customers/customerList.jhtm">Customers</a></span>
						<i class="sli-arrow-right"></i><span>${customerForm.customer.username}</span>
					</div>
				</div>
				<div class="page-head">
					<div class="row justify-content-between">
						<div class="col-sm-8 title">
							${customerForm.customer.address.firstName}&nbsp;${customerForm.customer.address.lastName}
						</div>
						<div class="col-sm-4 actions">
							<div class="dropdown">
								<button type="button" class="btn-outline dropdown-toggle active btn btn-primary" data-toggle="dropdown">
		                             <i class="sli-settings"></i> Actions
		                         </button>
		                         <div class="dropdown-menu dropdown-menu-right">
									<c:if test="${!customerForm.newCustomer}">
										<sec:authorize ifAnyGranted="ROLE_AEM,ROLE_ADMIN_${prop_site_id},ROLE_CUSTOMER_DELETE">
											<a class="dropdown-item" onClick="return confirm('Delete permanently?')"><spring:message code="customerDelete"/></a>
										</sec:authorize>
									</c:if> 
		                         </div>
							</div>
						</div>
					</div>
				</div>
				<div class="page-body">
					<div class="main-content">
						<div class="form-section">
							<div class="content">
							 	<ul class="nav nav-tabs" role="tablist">
					                <li class="nav-item">
					                    <a class="nav-link active" data-toggle="tab" role="tab" href="#tab_1"><fmt:message key="emailAddressAndPassword"/></a>
					                </li>
					                <li class="nav-item">
					                    <a class="nav-link" data-toggle="tab" role="tab" href="#tab_2"><fmt:message key="customerInformation"/></a>
					                </li>
					                <li class="nav-item">
					                		<div class="dropdown">
						                    <a class="nav-link" data-toggle="dropdown">Advanced <i class="fa fa-caret-down"></i></a>
											<div class="dropdown-menu dropdown-menu-right">
												<a class="dropdown-item" data-toggle="tab" role="tab" href="#tab_4"><fmt:message key="options" /></a>
												<c:if test="${gSiteConfig['gAFFILIATE'] > 0}">
													<a class="dropdown-item" data-toggle="tab" role="tab" data-target="#tab_5">Affiliate</a>
								                </c:if>									
												<c:if test="${gSiteConfig['gCUSTOMER_SUPPLIER']}">
													<a class="dropdown-item" data-toggle="tab" role="tab" data-target="#tab_supplier"><fmt:message key="supplier" /></a> 
												</c:if>
												<c:if test="${gSiteConfig['gLOYALTY']}">
								                    <a class="dropdown-item" data-toggle="tab" role="tab" data-target="#tab_6"><fmt:message key="loyalty" /></a>
								                </c:if>
												<a class="dropdown-item" data-toggle="tab" role="tab" data-target="#tab_7">Note</a>
												<c:if test="${gSiteConfig['gASI'] != ''}">
													<a class="dropdown-item" data-toggle="tab" role="tab" data-target="#tab_8"><fmt:message key="markUp" /></a>
												</c:if> 
												<c:if test="${gSiteConfig['gBUDGET_BRAND']}">
													<a class="dropdown-item" data-toggle="tab" role="tab" data-target="#tab_brands"><fmt:message key='brands' /></a> 
												</c:if>
												<c:if test="${siteConfig['GROUP_REWARDS'].value == 'true'}">
								                    <a class="dropdown-item" data-toggle="tab" data-target="#tab_10"><fmt:message key="reward" />s</a>
								                </c:if>
												<c:if test="${siteConfig['GROUP_ORDER_LIMIT'].value == 'true'}">
								                    <a class="dropdown-item" data-toggle="tab" data-target="#tab_11"><fmt:message key="orderLimit" /></a>
												</c:if>
												<c:if test="${gSiteConfig['gBUDGET_FREQUENCY'] > 0}">
													<a class="dropdown-item" data-toggle="tab" data-target="#tab_12"><fmt:message key="budgetFrequency" /></a>
												</c:if>
											</div>
										</div>
					                </li>
				                </ul>
				                <div class="tab-content">
				                    <jsp:include page="/WEB-INF/jsp/admin-responsive/customers/formEmailPasswordTab.jsp" />
				                    <jsp:include page="/WEB-INF/jsp/admin-responsive/customers/customerInformationTab.jsp" />
				                    <jsp:include page="/WEB-INF/jsp/admin-responsive/customers/formOptionTab.jsp" />
				                    <jsp:include page="/WEB-INF/jsp/admin-responsive/customers/formAffiliateTab.jsp" />
				                    <c:if test="${gSiteConfig['gCUSTOMER_SUPPLIER']}">
								 		<jsp:include page="/WEB-INF/jsp/admin-responsive/customers/formSupplierTab.jsp" />
								    </c:if>
								    <c:if test="${gSiteConfig['gLOYALTY']}">
								 		<jsp:include page="/WEB-INF/jsp/admin-responsive/customers/formLoyaltyTab.jsp" />
									</c:if>
									<jsp:include page="/WEB-INF/jsp/admin-responsive/customers/formNoteTab.jsp" />
								    <c:if test="${gSiteConfig['gASI'] != ''}">
										<jsp:include page="/WEB-INF/jsp/admin-responsive/customers/asiTab.jsp" />
								    </c:if>
								    <c:if test="${gSiteConfig['gBUDGET_BRAND']}">
										<div id="tab_brands" class="tab-pane">
											<c:forEach items="${brands}" var="brand">
												<div class="line">
													<div class="form-row">
														<div class="col-label">
															<label for="customer_customNote1" class="col-form-label">
							                                    	<c:out value="${brand.name}"/> <fmt:message key='report' />
															</label>
														</div>
														<div class="col-lg-4">
															<label class="custom-control custom-checkbox">
																<c:set var="skuPrefix" value=",${brand.skuPrefix},"/>
																<input type="checkbox" name="BRAND_REPORT" id="BRAND_REPORT" value ="${brand.skuPrefix}" <c:if test="${fn:contains(customerForm.customer.brandReport, skuPrefix)}">checked</c:if>  class="custom-control-input"/>
																<span class="custom-control-indicator"></span>
															</label>
														</div>
													</div>
												</div>
				                            	</c:forEach> 
										</div>
									</c:if>
									<c:if test="${siteConfig['GROUP_REWARDS'].value == 'true'}">
										<div id="tab_10" class="tab-pane">
					                        <div class="line">
					                            <div class="form-row">
													<div class="col-label">
														<label for="customer_rewardPoints" class="col-form-label"><fmt:message key="reward" /> <fmt:message key="points" /></label>
													</div>
					                                <div class="col-lg-4"> 
														<form:input path="customer.rewardPoints" id="customer_rewardPoints" class="form-control"/> 
					                                </div>
					                            </div>
					                        </div>
										</div>
									</c:if>
					                <c:if test="${siteConfig['GROUP_ORDER_LIMIT'].value == 'true'}">
                    						<div id="tab_11" class="tab-pane">
											<div class="line">
												<div class="form-row">
													<div class="col-label">
														<label for="customer_rewardPoints" class="col-form-label">
															<span class="fa fa-question-circle" data-toggle="tooltip" data-placement="top" title="" data-original-title="Note::Budget Manager: All the order approvals should be sent to selected 'Order Limit Manager' email address"></span>
															<fmt:message key="orderLimit" /> Manager
														</label>
													</div>
													<div class="col-lg-4">
														<form:select path="customer.orderLimitManager" id="customer_orderLimitManager" class="custom-select">
															<form:option value=""></form:option>
															<c:forEach items="${managersList}" var="parent">
																<c:if test="${customerForm.customer.username != parent.username}">
																	<form:option value="${parent.username}">${parent.username}</form:option>
																</c:if>
															</c:forEach>
														</form:select>
													</div>
												</div>
											</div>
										</div>
									</c:if>
									<c:if test="${gSiteConfig['gBUDGET_FREQUENCY'] > 0}">
										<jsp:include page="budgetFrequencyForm.jsp" />
									</c:if>
								</div>
							</div>
							<div class="footer">
								<div class="form-row">
									<div class="col-12">
										<c:if test="${customerForm.newCustomer}">
											<sec:authorize ifAnyGranted="ROLE_AEM,ROLE_ADMIN_${prop_site_id},ROLE_CUSTOMER_CREATE">
												<button class="btn btn-default">Add</button>
											</sec:authorize> 
										</c:if>
										<sec:authorize ifAnyGranted="ROLE_AEM,ROLE_ADMIN_${prop_site_id},ROLE_CUSTOMER_EDIT">
											<button class="btn btn-default">Update</button>
										</sec:authorize>
										<button class="btn btn-default">Cancel</button>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</form:form>
		</sec:authorize>
	</tiles:putAttribute>
	<tiles:putAttribute name="js">
	    <script src="${_contextpath}/admin-responsive-static/assets/plugins/tinymce/tinymce.min.js"></script>
	    <script>
			tinymce.init({
				selector: '.code-editor',
				toolbar_items_size : 'small',
				height: 250,
				theme: 'modern',
				plugins: 'code print preview fullpage searchreplace autolink directionality visualblocks visualchars fullscreen image link media template codesample table charmap hr pagebreak nonbreaking anchor toc insertdatetime advlist lists textcolor wordcount imagetools contextmenu colorpicker textpattern help',
				toolbar1: 'formatselect | bold italic strikethrough forecolor backcolor | link | alignleft aligncenter alignright alignjustify  | numlist bullist outdent indent  | removeformat',
				image_advtab: true,
				templates: [
					{ title: 'Test template 1', content: 'Test 1' },
					{ title: 'Test template 2', content: 'Test 2' }
				],
				content_css: [
					'//fonts.googleapis.com/css?family=Lato:300,300i,400,400i',
					'//www.tinymce.com/css/codepen.min.css',
					"${_contextpath}/admin-responsive-static/assets/css/mce-custom.css",
				]
			});
		</script>
	</tiles:putAttribute>
</tiles:insertDefinition>