<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="/WEB-INF/tlds/spring.tld" prefix="spring" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ taglib uri="http://www.springframework.org/security/tags" prefix="sec" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ taglib uri="/WEB-INF/tlds/custom.tld" prefix="wj" %>
    	
<div class="line">
	<div class="form-row">
	  	<div class="col-label">
	  		<div class="col-form-label">
	  			<fmt:message key="facebook"/>
	  		</div>
	  	</div>
	  	<div class="col-lg-4">
	  		<div class="form-group">
	        		<form:input path="customer.affiliate.affiliateFacebook" maxlength="300" class="form-control"/>
	        	</div>
	        	<small class="help-text">
				Should be an absolute path. Example: http://www.domainName.com/ContactUs.html<br/>
				Use #affFacebook# on Layout to see the information on front end
			</small>   
    		</div>
    	</div>
</div>
<div class="line">
	<div class="form-row">
	  	<div class="col-label">
	  		<div class="col-form-label"><fmt:message key="google"/></div>
	  	</div>
	  	<div class="col-lg-4">
	  		<div class="form-group">
	        		<form:input path="customer.affiliate.affiliateGoogle" maxlength="300" class="form-control"/>
        		</div>
	        	<small class="help-text">
				Should be an absolute path. Example: http://www.domainName.com/ContactUs.html<br/>
	        		Use #affGoogle# on Layout to see the information on front end
	        	</small>   
		</div>
	</div>
</div>
<div class="line">
	<div class="form-row">
	  	<div class="col-label">
	  		<div class="col-form-label"><fmt:message key="twitter"/></div>
	  	</div>
	  	<div class="col-lg-4">
	  		<div class="form-group">  
        			<form:input path="customer.affiliate.affiliateTwitter" maxlength="300" class="form-control"/>
			</div>
        		<small class="help-text">
	        		Should be an absolute path. Example: http://www.domainName.com/ContactUs.htm<br/>
				Use #affTwitter# on Layout to see the information on front end
			</small>
    		</div>
	</div>	
</div>