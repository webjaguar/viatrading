<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="/WEB-INF/tlds/spring.tld" prefix="spring" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ taglib uri="http://www.springframework.org/security/tags" prefix="sec" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ taglib uri="/WEB-INF/tlds/custom.tld" prefix="wj" %>

<c:if test="${gSiteConfig['gPAYMENTS']}">
	<tiles:insertDefinition name="admin.responsive-template.customers.incomingPayments" flush="true"> 
		<tiles:putAttribute name="css">
			<style>
				#grid .field-memo{
					width: 200px;
				}
				#grid .field-paymentAmt{
					text-align: right;
				}
				#grid .field-username{
					width: 180px;
				}
				#grid .field-company{
					width: 200px;
				}
			</style>
		</tiles:putAttribute>
		<tiles:putAttribute name="content" type="string">
			<sec:authorize ifAnyGranted="ROLE_AEM,ROLE_ADMIN_${prop_site_id},ROLE_CRM">
				<form action="payments.jhtm" id="list" method="post" name="list">
				    <div class="page-banner">
						<div class="breadcrumb"><span><a href="${_contextpath}/admin/catalog/dashboard.jhtm">Home</a></span><i class="sli-arrow-right"></i><span><a href="${_contextpath}/admin/customers/">Customers</a></span><i class="sli-arrow-right"></i><span>Incoming Payments</span></div>
					</div>
			  		<div class="page-head">
						<div class="row justify-content-between">
							<div class="col-sm-8 title">
								<h3>Incoming Payments</h3>
							</div>
							<div class="col-sm-4 actions">
								<div class="dropdown dropdown-actions">
									<button type="button" class="btn-outline dropdown-toggle active btn btn-primary" data-toggle="dropdown">
			                             <i class="sli-settings"></i> Actions
			                         </button>
			                         <div class="dropdown-menu dropdown-menu-right">
										<c:if test="${paymentSearch.userId != null}">
											<sec:authorize ifAnyGranted="ROLE_AEM,ROLE_ADMIN_${prop_site_id},ROLE_INCOMING_PAYMENT_CREATE">
												<a class="dropdown-item"><fmt:message key="add" /> <fmt:message key="payment" /></a>
											</sec:authorize>
											<sec:authorize ifAnyGranted="ROLE_AEM,ROLE_ADMIN_${prop_site_id},ROLE_INCOMING_PAYMENT_CANCEL">
												<a class="dropdown-item"><fmt:message key="cancel" /> <fmt:message key="payment" /></a>
											</sec:authorize>
										</c:if> 
			                         </div>
			                    </div>
								<div class="dropdown dropdown-setting">
									<button type="button" class="btn-outline dropdown-toggle active btn btn-primary" data-toggle="dropdown">
			                             <i class="sli-screen-desktop"></i> Display
			                         </button>
									<div class="dropdown-menu dropdown-menu-right">
										<div class="form-group">
					                    		<div class="option-header">Rows</div>
											<select class="custom-select form-control" name="size" onchange="document.getElementById('page').value=1;submit();">
												<c:forTokens items="10,25,50,100" delims="," var="current">
													<option value="${current}" <c:if test="${current == model.payments.pageSize}">selected</c:if>>${current}</option>
												</c:forTokens>
											</select>
										</div>
										<div class="form-group">
					                    		<div class="option-header">Columns</div>
					                    		<div id="column-hider"></div>
										</div>
									</div>
			                    </div>
							</div>
						</div>
					</div>
					<div class="page-body">
						<div class="grid-stage">
							<c:if test="${model.payments.nrOfElements > 0}">
								<div class="page-stat">
									<fmt:message key="showing">
										<fmt:param value="${model.payments.firstElementOnPage + 1}"/>
										<fmt:param value="${model.payments.lastElementOnPage + 1}"/>
										<fmt:param value="${model.payments.nrOfElements}"/>
									</fmt:message>
								</div>
							</c:if>
				            <table id="grid"></table>
						    <div class="footer">
				                <div class="float-right">
									<c:if test="${model.payments.firstPage}">
								 		<span class="previous"><i class="sli-arrow-left" style="font-size: 0.7rem"></i> Previous</span>
									</c:if>
									<c:if test="${not model.payments.firstPage}">
										<a href="<c:url value="payments.jhtm"><c:param name="page" value="${model.payments.page}"/></c:url>" >
											<span class="previous"><i class="sli-arrow-left" style="font-size: 0.7rem"></i> Previous</span>
										</a>
									</c:if>
									<span class="status"><c:out value="${model.payments.page+1}" /> of <c:out value="${model.payments.pageCount}"/></span>
									<c:if test="${model.payments.lastPage}">
									 	<span class="next">Next <i class="sli-arrow-right" style="font-size: 0.7rem"></i></span>
									</c:if>
									<c:if test="${not model.payments.lastPage}">
										<a href="<c:url value="payments.jhtm"><c:param name="page" value="${model.payments.page+2}"/></c:url>">
											<span class="next">Next <i class="sli-arrow-right" style="font-size: 0.7rem"></i></span>
										</a>
									</c:if>
								</div>
							</div>
						</div>
					</div>
					<c:if test="${paymentSearch.userId != null}">
					    <input type="hidden" name="cid" value="${paymentSearch.userId}">
					</c:if>
					<input type="hidden" id="sort" name="sort" value="${paymentSearch.sort}" />
				</form>
			</sec:authorize>
		</tiles:putAttribute>
		<tiles:putAttribute name="js">
			<!-- DGRID TABLE SCRIPT MUST BE AT BOTTOM OF THE PAGE -->
			<script src="${_contextpath}/admin-responsive-static/assets/plugins/dojo/dojo.js" data-dojo-config="async: true"></script>
			<script>
				function generateMainGrid(){
					//Table Data Sanitization
					var data = [];
					<c:forEach items="${model.payments.pageList}" var="payment" varStatus="status">
						data.push({
							id: '${payment.id}',
							offset: '<c:out value="${status.count + model.payments.firstElementOnPage}"/>.',
							paymentURL: 'payment.jhtm?id=${payment.id}&cid=${payment.customer.id}',
							memo: '<c:if test="${payment.paymentMethod != ''}"><c:out value="${payment.paymentMethod}"/> </c:if><c:out value="${payment.memo}"/>',
							paymentAmt: '<fmt:formatNumber value="${payment.amount}" pattern="#,##0.00" />',
							date: '<fmt:formatDate type="date" timeStyle="default" value="${payment.date}"/>',
							username: '${wj:escapeJS(payment.customer.username)}',
							company: '${wj:escapeJS(payment.customer.address.company)}',
							lastName: '${wj:escapeJS(payment.customer.address.lastName)}',
							firstName: '${wj:escapeJS(payment.customer.address.firstName)}',
						});
					</c:forEach>
					
					require(["dgrid/List", "dgrid/OnDemandGrid", "dgrid/Selection", "dgrid/Keyboard", "dgrid/Editor", "dijit/form/CheckBox", 
							"dijit/form/TimeTextBox", "dijit/form/Select", "dojo/data/ObjectStore", "dijit/form/FilteringSelect",
							"dojo/_base/declare", "dojo/_base/lang", "dojo/store/Memory", "dojo/store/Observable", "dgrid/test/data/createSyncStore", 
							'dgrid/extensions/ColumnHider', 'dgrid/extensions/ColumnReorder', 'dgrid/extensions/ColumnResizer',
						], function(List, OnDemandGrid, Selection, Keyboard, Editor, CheckBox, TimeTextBox, Select, ObjectStore, FilteringSelect,
								declare, lang, LegacyMemory, LegacyObservable, createSyncStore, ColumnHider, ColumnReorder, ColumnResizer){
	
						//Column Definitions
						var columns = {
							offset: {
								label: '',
								get: function(object){ 
									return object.offset;
								},
								sortable: false,
								unhidable: true
							},
							memo: {
								label: '<fmt:message key="memo" />',
								renderCell: function(object){
									var link = document.createElement('a');
									link.text = object.memo;
									link.href = object.paymentURL;
									return link;
								},
								sortable: false,
							},
							paymentAmt: {
								label: '<fmt:message key="payment" />',
								get: function(object){
									return object.paymentAmt;
								},
								sortable: false,
							},
							date: {
								label: '<fmt:message key="date" />',
								get: function(object){
									return object.date;
								},
								sortable: false,
							},
							username: {
								label: '<fmt:message key="emailAddress" />',
								get: function(object){
									return object.username;
								},
								sortable: false,
							},
							company: {
								label: '<fmt:message key="company" />',
								get: function(object){
									return object.company;
								},
								sortable: false,
							},
							lastName: {
								label: '<fmt:message key="lastName" />',
								get: function(object){
									return object.lastName;
								},
								sortable: false,
							},
							firstName: {
								label: '<fmt:message key="firstName" />',
								get: function(object){
									return object.firstName;
								},
								sortable: false,
							}
						};
						
						var grid = new (declare([OnDemandGrid, Selection, Keyboard, Editor, ColumnHider, ColumnReorder, ColumnResizer]))({
							collection: createSyncStore({ data: data }),
							columns: columns,
							selectionMode: "none",
						},'grid');
						
						grid.on('dgrid-refresh-complete', function(event) {
							$('#grid-hider-menu').appendTo('#column-hider');
						});
						
						grid.startup();
					});
				}
			
				generateMainGrid();
			</script>
		</tiles:putAttribute>
	</tiles:insertDefinition>
</c:if>
 