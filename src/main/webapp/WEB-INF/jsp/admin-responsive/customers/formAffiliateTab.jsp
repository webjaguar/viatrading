<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="/WEB-INF/tlds/spring.tld" prefix="spring" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ taglib uri="http://www.springframework.org/security/tags" prefix="sec" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ taglib uri="/WEB-INF/tlds/custom.tld" prefix="wj" %>

<c:if test="${gSiteConfig['gAFFILIATE'] > 0}">
	<div id="tab_5" class="tab-pane" role="tabpanel">
		<div class="line">
			<div class="form-row">
				<div class="col-label">
					<label for="customer_affiliateParent" class="col-form-label">
						<fmt:message key="owner" />
					</label>
				</div>
				<div class="col-lg-4">
					<div class="form-group">
						<form:select path="customer.affiliateParent" id="customer_affiliateParent" onchange="javascript:document.getElementById('_ownerChanged').value=true" class="custom-select">
							<c:choose>
								<c:when test="${!customerForm.newCustomer and customerForm.customer.isAffiliate}"><form:option value="-1"><fmt:message key="removeChild" /></form:option></c:when>
							</c:choose>
							<c:forEach items="${affiliates}" var="affiliate" >
								<form:option value="${affiliate.nodeId}"><c:choose><c:when test="${affiliate.nodeId == 0}"><fmt:message key="admin" /></c:when><c:otherwise>${affiliate.firstName} ${affiliate.lastName} </c:otherwise></c:choose> </form:option>
							</c:forEach>         
						</form:select>
					</div>
					<c:if test="${gSiteConfig['gNUMBER_AFFILIATE'] > numAffiliateInUse}" >
						<small class="help-text">
							Ultimate owner of all customers is the Admin If you want to create an affiliate under Admin simply check Affiliate checkBox.
						</small>
					</c:if>
				</div>
			</div>
		</div>
		<c:choose>
			<c:when test="${!customerForm.customer.isAffiliate and gSiteConfig['gNUMBER_AFFILIATE'] > numAffiliateInUse}">
				<div class="line">
					<div class="form-row">
						<div class="col-label">
							<label for="customer_affiliateParent" class="col-form-label">
								<fmt:message key="affiliate" />
							</label>
						</div>
						<div class="col-lg-4">
							<label class="custom-control custom-checkbox">
								<form:checkbox path="customer.isAffiliate" id="isAffiliate" class="custom-control-input" />
								<span class="custom-control-indicator"></span>
								<span class="custom-control-description">
									<c:set var="showAffiliateOption" value="true" /> Yes
								</span>
							</label>
						</div>
					</div> 
				</div>
			</c:when>
			<c:otherwise>
				<div class="line">
					<div class="form-row">
						<div class="col-label">
							<label for="customer_affiliateParent" class="col-form-label">
								<fmt:message key="affiliate" />
							</label>
						</div>
						<div class="col-lg-4">
							<label class="text">Number of affiliates is exceeded. Contact webmaster to get more Affiliate account.</label>
						</div>
					</div>
				</div>
			</c:otherwise>
		</c:choose> 
		<c:if test="${gSiteConfig['gSALES_PROMOTIONS'] and showAffiliateOption}">
			<div class="line">
				<div class="form-row">
					<div class="col-label">
						<label for="customer_affiliateParent" class="col-form-label">
							<fmt:message key="id" />/<fmt:message key="promoCode" />
						</label>
					</div>
					<div class="col-lg-4">  
						<div class="form-group">
							<form:input path="customer.affiliate.promoTitle" id="promoCode" class="form-control"/>
						</div>
						<small class="help-text">
							Please create a Promotion code and insert it here. (Make sure this Promo Code exist.)<br/>
							Use #promocode# on Layout to see the Promo Code on front end
						</small>
					</div>
				</div>   
			</div>
			<div class="line">
				<div class="form-row">
					<div class="col-label">
						<label for="customer_affiliateParent" class="col-form-label">
							<fmt:message key="id" />/<fmt:message key="id" />
						</label>
					</div>
					<div class="col-lg-4">  
						<div class="form-group">
							<form:input path="customer.affiliate.categoryId" id="customer_affiliate_categoryId" class="form-control"/>
						</div>
						<small class="help-text">Please create a category and insert it's ID here. (Make sure this Category ID exist.)</small>
					</div>
				</div>   
			</div>
			<div class="line">
				<div class="form-row">
					<div class="col-label">
						<label for="customer_affiliateParent" class="col-form-label">
							<fmt:message key="domainName" />
						</label>
					</div>
					<div class="col-lg-4">
						<div class="form-group">
							<form:input path="customer.affiliate.domainName"  id="customer_affiliate_domainName" class="form-control"/>
						</div>
						<small class="help-text">Please insert the Domain name here.</small>
					</div>
				</div>   
			</div>
			<div class="line">
				<div class="form-row">
					<div class="col-label">
						<label for="customer_affiliateParent" class="col-form-label">Source codes</label>
					</div>
					<div class="col-lg-4">
						<label class="text">
				         	<c:if test="${customerForm.customer.affiliate.promoTitle != null}">
				           		<c:out value="${siteConfig['SITE_URL'].value}"/>home.jhtm?promocode=<c:out value="${customerForm.customer.affiliate.promoTitle}"/><br />
				         	</c:if>
				         	<c:if test="${customerForm.customer.affiliate.categoryId != null and customerForm.customer.affiliate.promoTitle != null}">
				           		<c:out value="${siteConfig['SITE_URL'].value}"/>category.jhtm?cid=<c:out value="${customerForm.customer.affiliate.categoryId}"/>&promocode=<c:out value="${customerForm.customer.affiliate.promoTitle}"/><br />
				         	</c:if>
				         	<c:if test="${customerForm.customer.affiliate.domainName != null}">
				           		<c:out value="${customerForm.customer.affiliate.domainName}"/>
				         	</c:if>
			         	</label>
					</div>
				</div>   
			</div>
          	<div class="line">
				<div class="form-row" id="commissionID">
					<div class="col-label"> 
						<label for="customer_affiliateParent" class="col-form-label">Commission</label>
					</div>
					<div class="col-lg-8">
						<div class="row">
							<div class="col-lg-4">
								<label>Product Level1:</label>
								<form:select path="customer.affiliate.productCommissionLevel1" id="customer_affiliate_productCommissionLevel1" class="custom-select">
									<form:option value="">None</form:option>
									<c:if test="${siteConfig['NUMBER_PRODUCT_COMMISSION_TABLE'].value > 0}">
										<c:forEach begin="1" end="${siteConfig['NUMBER_PRODUCT_COMMISSION_TABLE'].value}" var="commissionTable" >
											<form:option value="${commissionTable}"><c:out value="${commissionTable}" /></form:option>
										</c:forEach>
									</c:if>
								</form:select>
							</div>
							<div class="col-lg-4">
								<label>Invoice Level1:</label>
								<form:input path="customer.affiliate.invoiceCommissionLevel1" id="customer_affiliate_invoiceCommissionLevel1" maxlength="10" class="form-control"/>
							</div>
							<div class="col-lg-2">
								<label>&nbsp;</label>
								<form:select path="customer.affiliate.percentLevel1" class="custom-select">
									<form:option value="true" label="%" />
									<form:option value="false"><fmt:message key="FIXED" /> $</form:option>
								</form:select>
							</div>
						</div>
						<c:if test="${gSiteConfig['gAFFILIATE'] == 2}">
							<div class="row">
								<div class="col-lg-4">
									<label>Product Level2:</label>
									<form:select path="customer.affiliate.productCommissionLevel2" id="customer_affiliate_productCommissionLevel2" class="custom-select">
										<form:option value="">None</form:option>
										<c:if test="${siteConfig['NUMBER_PRODUCT_COMMISSION_TABLE'].value > 0}">
											<c:forEach begin="1" end="${siteConfig['NUMBER_PRODUCT_COMMISSION_TABLE'].value}" var="commissionTable" >
										         <form:option value="${commissionTable}"><c:out value="${commissionTable}" /></form:option>
									        </c:forEach>
								        </c:if>
									</form:select>
								</div>
								<div class="col-lg-4">
									<label>Invoice Level2:</label>
									<form:input path="customer.affiliate.invoiceCommissionLevel2" id="customer_affiliate_invoiceCommissionLevel2" maxlength="10" class="form-control"/>
								</div>
								<div class="col-lg-2">
									<label>&nbsp;</label>
									<form:select path="customer.affiliate.percentLevel2" class="custom-select">
										<form:option value="true" label="%" />
										<form:option value="false"><fmt:message key="FIXED" /> $</form:option>
									</form:select>
								</div>
							</div>
						</c:if>
					</div>
				</div>   
			</div>
			<div class="line">
				<div class="form-row" id="logoID">
					<div class="col-label">
						<label for="customer_affiliateParent" class="col-form-label"><fmt:message key="logo"/></label>
					</div>
					<div class="col-lg-4">
						<c:if test="${customerForm.customer.affiliate.affiliateLogo == null or customerForm.customer.affiliate.affiliateLogo == ''}">
							<input value="browse" class="textfield" type="file" name="upload_affilLogo" id="upload_affilLogo"/>  
						</c:if>
					</div>
				</div>   
			</div>
			<div class="line">
				<div class="form-row" id="aboutUsID">
					<div class="col-label">
						<label for="customer_affiliateParent" class="col-form-label"><fmt:message key="aboutUs"/></label>
					</div>
					<div class="col-lg-4">
						<div class="form-group">
							<form:input path="customer.affiliate.affiliateAboutUs" id="customer_affiliate_affiliateAboutUs" maxlength="300" class="form-control"/>
						</div>
						<small class="help-text">
							Should be an absolute path. Example: http://www.domainName.com/AboutUs.html<br/>
							Use #affAboutus# on Layout to see the information on front end
						</small>
					</div>
				</div>
			</div>
			<div class="line">				    	 
				<div class="form-row" id="aboutUsID">
					<div class="col-label">
						<label for="customer_affiliateParent" class="col-form-label">
							<fmt:message key="contactUs"/>
						</label>
					</div>
					<div class="col-lg-4">
						<div class="form-group">   
							<form:input path="customer.affiliate.affiliateContactUs" id="customer_affiliate_affiliateContactUs" maxlength="300" class="form-control"/>
						</div>
						<small class="help-text">
							Should be an absolute path. Example: http://www.domainName.com/ContactUs.html<br/>
							Use #affContactus# on Layout to see the information on front end
						</small>									    
					</div>
				</div>
			</div>
			<jsp:include page="socialMedia.jsp" />
		</c:if>
	</div>
</c:if>