<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ taglib uri="http://www.springframework.org/security/tags" prefix="sec" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ taglib uri="/WEB-INF/tlds/spring.tld" prefix="spring" %>
<%@ taglib uri="/WEB-INF/tlds/custom.tld" prefix="wj" %>

<div id="tab_8" class="tab-pane">
	<c:if test="${siteConfig['ASI_ENDQTYPRICING'].value == 'true'}">
		<div class="line">
			<div class="form-row">
				<div class="col-label">
					<label for="customer_endQtyPricing" class="col-form-label">
						<img class="toolTipImg" title="EQP::Use to generate Purchase Order based on EQP." src="../graphics/question.gif" />&nbsp;<fmt:message key="endQtyPricing" />:
					</label>
				</div>
				<div class="col-lg-4">
					<label class="custom-control custom-checkbox">
						<form:checkbox path="customer.endQtyPricing" id="customer_endQtyPricing" class="custom-control-input"/>
						<span class="custom-control-indicator"></span>
						<span class="custom-control-description">
							Yes
						</span>
					</label>
				</div>
			</div>
		</div>
	</c:if>				                            
	<div class="line">
		<div class="form-row">
			<div class="col-label">
				<label for="customer_markupFormula" class="col-form-label"><fmt:message key="markUp" />/<fmt:message key="margin" /></label>
			</div>
			<div class="col-lg-4"> 
				<form:select path="customer.markupFormula"  id="customer_markupFormula" class="custom-select" >
					<form:option value="0">Markup</form:option>
					<form:option value="1">Margin</form:option>
				</form:select>
			</div>
		</div>
	</div>
	<div class="line">			                            
		<div class="form-row">
			<div class="col-label">
				<label for="customer_markupType" class="col-form-label"> 
					<span class="fa fa-question-circle" data-toggle="tooltip" data-placement="top" title="" data-original-title="Markup based on EQP::Markup will be added based on EQP."></span>
					<fmt:message key="markUpBasedOn" />
				</label>
			</div>
			<div class="col-lg-4"> 
				<form:select path="customer.markupType" id="customer_markupType" class="custom-select" >
					<form:option value="0">Regular Prices</form:option>
					<form:option value="1">End Qty Price</form:option>
					<form:option value="2">Regular Cost</form:option>
					<form:option value="3">End Qty Cost</form:option>
				</form:select>
			</div>
		</div>
	</div>
	<c:forTokens items="1,2,3,4,5,6,7,8,9,10" delims="," var="index" varStatus="status">
		<div class="line">
			<div class="form-row">
				<div class="col-label">
					<label for="customer_markup1" class="col-form-label">
						<span class="fa fa-question-circle" data-toggle="tooltip" data-placement="top" title="" data-original-title="Note::Add/Deduct mark up percentage to/from the price. Add negative value to deduct markup value from price."></span>
						<fmt:message key="markUp"/> ${index}
					</label>
				</div>
				<div class="col-lg-4">
					<div class="input-group">
						<form:input path="customer.markup${index}" id="customer_markup${index}" class="form-control"/>
						<span class="input-group-addon">%</span>
					</div>
				</div>
			</div>
		</div>
	</c:forTokens> 
</div>