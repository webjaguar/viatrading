<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="/WEB-INF/tlds/spring.tld" prefix="spring" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ taglib uri="http://www.springframework.org/security/tags" prefix="sec" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ taglib uri="/WEB-INF/tlds/custom.tld" prefix="wj" %>

<div id="tab_1" class="tab-pane active" role="tabpanel">
	<div class="line">
		<div class="form-row">
			<div class="col-label">
				<label class="col-form-label">
					<fmt:message key='emailAddress' />
				</label>
			</div>
			<div class="col-lg-4">
				<form:input path="customer.username" id="customer_username" class="form-control"/>
		  	</div>
		  	<div class="col-lg-2">
	  			<a class="btn btn-default btn-flat" href="email.jhtm?id=${customerForm.customer.id}"><fmt:message key="sendEmail" /></a>
	  		</div>
      	</div>
	</div>
	<div class="line">
		<div class="form-row">
			<div class="col-label">
				<label class="col-form-label">
					<fmt:message key='newPassword' />
				</label>
			</div>
			<div class="col-lg-4">
				<form:password path="customer.password" id="customer_password" class="form-control"/>
			</div>
		</div>
	</div>
	<div class="line">
		<div class="form-row">
			<div class="col-label">
				<label class="col-form-label">
					<fmt:message key='confirmPassword' />
				</label>
			</div>
			<div class="col-lg-4">
				<form:password path="confirmPassword" id="confirmPassword" class="form-control"/>
			</div>
		</div>
	</div>
                            
	<c:if test="${siteConfig['CUSTOMER_PASSWORD_RESET'].value == 'true'}">
		<div class="line">
			<div class="form-row">
				<div class="col-label">
					<label class="col-form-label">
						<fmt:message key='passwordValidity' />
					</label>
				</div>
				<div class="col-lg-4">
					<form:select path="customer.passwordValidity" id="customer_passwordValidity" class="custom-select">
						<form:option value="-1" label="Never"></form:option>
						<form:option value="1" label="1 Day"></form:option>
						<form:option value="15" label="15 Days"></form:option>
						<form:option value="30" label="30 Days"></form:option>
						<form:option value="45" label="45 Days"></form:option>
						<form:option value="90" label="90 Days"></form:option>
						<form:option value="180" label="180 Days"></form:option>
						<form:option value="365" label="365 Days"></form:option>
					</form:select>
				</div>
			</div>
		</div>
	</c:if> 
	<div class="line">
		<div class="form-row">
			<div class="col-label">
				<label class="col-form-label">
					<span class="fa fa-question-circle" data-toggle="tooltip" data-placement="top" title="Note::Customer Rating 1"></span><fmt:message key="rating" /> 1
				</label>
			</div>
			<div class="col-lg-4">
				<form:select path="customer.rating1" class="custom-select" id="customer_rating1">
					<form:option value=""></form:option>
					<c:forTokens items="${siteConfig['CUSTOMER_RATING_1'].value}" delims="," var="type" varStatus="status">
						<form:option value ="${type}"><c:out value ="${type}" /></form:option>
					</c:forTokens>
				</form:select> 
			</div>
		</div>
	</div>
	<div class="line">
		<div class="form-row">
			<div class="col-label">
				<label class="col-form-label">
					<span class="fa fa-question-circle" data-toggle="tooltip" data-placement="top" title="Note::Customer Rating 2."></span><fmt:message key="rating" /> 2
				</label>
			</div>
			<div class="col-lg-4">
				<form:select path="customer.rating2" class="custom-select" id="customer_rating2">
					<form:option value=""></form:option>
			  		<c:forTokens items="${siteConfig['CUSTOMER_RATING_2'].value}" delims="," var="type" varStatus="status">
						<form:option value ="${type}"><c:out value ="${type}" /></form:option>
				  	</c:forTokens>
				</form:select> 
			</div>
		</div>
	</div>
	<c:if test="${gSiteConfig['gREGISTRATION_TOUCH_SCREEN']}">
		<div class="line">
			<div class="form-row">
				<div class="col-label">
					<label for="customer_suspended" class="control-label"><fmt:message key='idCardCount' /></label>
				</div>
				<div class="col-lg-4">
					<label class="text"><c:out value="${customerForm.customer.cardIdCount}" /></label>
				</div>
			</div>
		</div>   
	</c:if>
	<div class="line">
		<div class="form-row">
			<div class="col-label">
				<label for="customer_suspended" class="col-form-label">
					<fmt:message key="suspended" />
				</label>
			</div>
			<div class="col-lg-4">
				<div class="form-group">
					<label class="custom-control custom-checkbox">
						<form:checkbox path="customer.suspended" id="customer_suspended" class="custom-control-input"/>
						<span class="custom-control-indicator"></span>
						<span class="custom-control-description"><fmt:message key="account"/></span>
					</label>
				</div>
				<c:if test="${gSiteConfig['gEVENT']}">
			    		<div class="form-group">
				    		<label class="custom-control custom-checkbox">
					    		<form:checkbox path="customer.suspendedEvent" class="custom-control-input"/>
					    		<span class="custom-control-indicator"></span>	
					    		<span class="custom-control-description"><fmt:message key="event"/></span>
				    		</label>
			    		</div>
				</c:if>
			</div>
		</div>
	</div>
	<div class="line">
		<div class="form-row">
			<div class="col-label">
				<label for="customer_emailNotify" class="col-form-label">
					<span class="fa fa-question-circle" data-toggle="tooltip" data-placement="top" title="Note::Notify customer for new orders."></span>
					<fmt:message key="email" /> <fmt:message key="notification" />
				</label>
			</div>
			<div class="col-lg-4">
				<div class="form-group">
					<label class="custom-control custom-checkbox">
				    		<form:checkbox path="customer.emailNotify" id="customer_emailNotify" class="custom-control-input"/>
				    		<span class="custom-control-indicator"></span>	
				    		<span class="custom-control-description">Yes</span>
			    		</label>
				</div>
			</div>
		</div>
	</div>
	<div class="line">
		<div class="form-row">
			<div class="col-label">
				<label for="customer_extraEmail" class="col-form-label">
					<span class="fa fa-question-circle" data-toggle="tooltip" data-placement="top" title="Note::Extra Email. Customer will notify by this extra email(s). Seperate by comma."></span>
					<fmt:message key='extraEmailAddress' />
				</label>
			</div>
			<div class="col-lg-4">
				<div class="form-group">
					<form:textarea path="customer.extraEmail" id="customer_extraEmail" class="form-control" rows="5"  />
				</div>
			</div>
		</div>
	</div>
	<c:if test="${gSiteConfig['gMASS_EMAIL']}">
		<div class="line">
			<div class="form-row">
				<div class="col-label">
					<label for="customer_unsubscribe" class="col-form-label">
						<span class="fa fa-question-circle" data-toggle="tooltip" data-placement="top" title="Note::The Email address will be excluded from Mass Email Module."></span>
						<fmt:message key="unsubscribe" />
					</label>
				</div>
				<div class="col-lg-4">
					<div class="form-group">
						<label class="custom-control custom-checkbox">
					    		<form:checkbox path="customer.unsubscribe" id="customer_unsubscribe" class="custom-control-input"/>
					    		<span class="custom-control-indicator"></span>	
					    		<span class="custom-control-description">Yes</span>
				    		</label>
					</div>
				</div>
			</div>
		</div>
	</c:if>
	<c:if test="${gSiteConfig['gBUDGET']}">
		<div class="line">
			<div class="form-row">
				<div class="col-label">
					<label for="customer_creditAllowed" class="col-form-label">
						<span data-html="true" class="fa fa-question-circle" data-toggle="tooltip" data-placement="top" title="Note::'Empty': Get value form Site Congifuration.<br/>'-1': No Budget Module.<br/>'0': Only need approvals from managers.<br/>'1-100': Apply this % of customer's credit."></span>
						<fmt:message key="credit" /> <fmt:message key="allowed" />
					</label>
				</div>
				<div class="col-lg-4">
					<div class="input-group">
						<form:input path="customer.creditAllowed" maxlength="80" id="customer_creditAllowed" class="form-control"/>
						<span class="input-group-addon">%</span>
					</div>
				</div>
			</div>
		</div>
		<c:if test="${siteConfig['BUDGET_ADD_PARTNER'].value == 'true'}">
			<div class="line">
				<div class="form-row">
					<div class="col-label">
						<label for="customer_budgetPlan" class="col-form-label">
							<span data-html="true" class="fa fa-question-circle" data-toggle="tooltip" data-placement="top" title="Note::'Empty': Get value form Site Congifuration.<br/>'-1': No Budget Module.<br/>'0': Only need approvals from managers.<br/>'1-100': Apply this % of customer's credit."></span>
							Budget Plan
						</label>
					</div>
					<div class="col-lg-4">
						<form:select id="budgetPlan" path="customer.budgetPlan" class="custom-select">
							<form:option value="" label="Please Select"/>
							<form:option value="Plan-A" label="Plan-A"/>
							<form:option value="Plan-B" label="Plan-B"/>
						</form:select>
					</div>
				</div>
			</div>
		</c:if>
	</c:if>
	<c:if test="${gSiteConfig['gPAYMENTS']}">
		<div class="line">
			<div class="form-row">
				<div class="col-label">
					<label for="customer_budgetPlan" class="col-form-label">
						<span class="fa fa-question-circle" data-toggle="tooltip" data-placement="top" title="Note::Orders can not be processed after this limit is reached."></span>
						<fmt:message key="payment" /> <fmt:message key="alert" />
					</label>
				</div>
				<div class="col-lg-4">
					<form:input path="customer.paymentAlert" maxlength="80" id="customer_paymentAlert" class="form-control"/>
				</div>
			</div>
		</div>
	</c:if>
	<c:if test="${siteConfig['CUSTOMER_IP_ADDRESS'].value == 'true'}">
		<div class="line">
			<div class="form-row">
				<div class="col-label">
					<label for="customer_budgetPlan" class="col-form-label">
						<span class="fa fa-question-circle" data-toggle="tooltip" data-placement="top" title="IP Address:: This user can connect only through these IP Address(es). Separate multiple IP Addresses by comma or enter. Leave empty for no limitation."></span>
						<fmt:message key="ipAddress" />
					</label>
				</div>
				<div class="col-lg-4">
					<form:textarea path="customer.ipAddress" id="customer_ipAddress" class="form-control"/>
				</div>
			</div>
		</div>
	</c:if>                 
	<c:if test="${gSiteConfig['gSUB_ACCOUNTS'] && hasSubAccounts}">
		<div class="line">
			<div class="form-row">
				<div class="col-label">
					<label for="customer_budgetPlan" class="col-form-label">
						<fmt:message key="subAccounts" />
					</label>
				</div>
				<div class="col-lg-4">
					<label class="custom-control custom-checkbox">
				    		<form:checkbox path="customer.hideSubAccts" id="customer_hideSubAccts" class="custom-control-input"/>
				    		<span class="custom-control-indicator"></span>
			    		</label>
					  	
				</div>
			</div>
		</div>
	</c:if>
</div>