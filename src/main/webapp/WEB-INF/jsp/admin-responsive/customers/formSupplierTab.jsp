<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="/WEB-INF/tlds/spring.tld" prefix="spring" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ taglib uri="http://www.springframework.org/security/tags" prefix="sec" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ taglib uri="/WEB-INF/tlds/custom.tld" prefix="wj" %>

<div id="tab_supplier" class="tab-pane">
	<div class="line">
		<div class="form-row">
			<div class="col-label">
				<label class="col-form-label"><fmt:message key="convertToSupplier" /></label>
            </div>
            <div class="col-lg-4">
				<label class="custom-control custom-checkbox">
					<form:checkbox path="customer.convertToSupplier" id="customer_convertToSupplier" disabled="${customerForm.customer.supplierId != null}" class="custom-control-input" />
					<span class="custom-control-indicator"></span>
				</label>
            </div>
		</div>
	</div>    
	<c:if test="${customerForm.customer.supplierId != null}">
		<div class="line">
			<div class="form-row">
				<div class="col-label">
					<label class="col-form-label"><fmt:message key="supplierId" /></label>
				</div>
				<div class="col-lg-4">
					<label class="text">
						<a href="../supplier/supplier.jhtm?sid=${customerForm.customer.supplierId}"><c:out value="${customerForm.customer.supplierId}" /></a>
					</label>
				</div>
			</div>
		</div>
	</c:if>
	<div class="line">
       	<div class="form-row">
       		<div class="col-label">
				<label for="customer_pointValue" class="col-form-label"><fmt:message key="supplierPrefix" /></label>
            </div>
            <div class="col-lg-4">
				<form:input path="customer.supplierPrefix" id="customer_supplierPrefix" maxlength="5" class="form-control" disabled="${customerForm.customer.supplierId != null}"/>
            </div>
        </div>
	</div>
	<div class="line"> 
       	<div class="form-row">
       		<div class="col-label">
				<label for="customer_pointThreshold" class="col-form-label"><fmt:message key="product" /> <fmt:message key="limit" /></label>
            </div>
            <div class="col-lg-4">
				<form:select path="customer.supplierProdLimit" id="customer_supplierProdLimit" class="custom-select">
					<form:option value=""><fmt:message key="unlimited"/></form:option>
					<c:forTokens items="10,25,50,100,500,1000" delims="," var="num">
						<form:option value="${num}">${num}</form:option>
					</c:forTokens>      
				</form:select>
            </div>
        </div>
	</div>
</div>