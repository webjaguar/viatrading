<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="/WEB-INF/tlds/spring.tld" prefix="spring" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ taglib uri="http://www.springframework.org/security/tags" prefix="sec" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ taglib uri="/WEB-INF/tlds/custom.tld" prefix="wj" %>
 
<div class="line">
	<div class="form-row">
		<div class="col-label">
			<label for="customer_priceTable" class="col-form-label">
				<fmt:message key="productPriceTable" />
			</label>
		</div>
		<div class="col-lg-4"> 
			<form:select path="customer.priceTable" id="customer_priceTable" class="custom-select">
				<form:option value="0"><fmt:message key="regularPricing" /></form:option>
				<c:forEach items="${unlimitedPriceTables}" var="priceTable">
					<form:option value="${priceTable.tableNumber}">${priceTable.tableName}</form:option>
				</c:forEach>
			</form:select>
		</div>
	</div>
</div>