<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/security/tags" prefix="sec" %> 
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ taglib uri="/WEB-INF/tlds/custom.tld" prefix="wj" %>
 
<tiles:insertDefinition name="admin.responsive-template.customers.vba" flush="true">  
	<tiles:putAttribute name="content" type="string">
  
<sec:authorize ifAnyGranted="ROLE_AEM,ROLE_ADMIN_${prop_site_id},ROLE_OUTGOING_PAYMENT_VIEW_LIST"> 	
<!-- Begin: Content -->
<section id="content" class="animated fadeIn">
	<form action="virtualBankPayment.jhtm" method="post"> 
		   
 	  	<!-- Error Message -->
  	  	<c:if test="${!empty model.message}">
		 <div class="highlight error"><spring:message code="${model.message}" arguments="${arguments}"/></div>
	  	</c:if>
	  	
	    <!-- Begin: Payment List Table -->
	    <div class="panel panel-default">
	        <div class="panel-menu">
	            <table class="fluid-width">
	                <tbody>
	                <tr> 
                    	 
                     	<c:if test="${model.orderList.nrOfElements > 0}">
						  <td class="pageShowing pr-15">
							  <fmt:message key="showing">
								<fmt:param value="${model.orderList.firstElementOnPage + 1}"/>
								<fmt:param value="${model.orderList.lastElementOnPage + 1}"/>
								<fmt:param value="${model.orderList.nrOfElements}"/>
							  </fmt:message>
						  </td>
					  	</c:if> 
	                    <td>
	                        <table class="pull-right">
	                            <tbody>
	                            <tr>
	                               <td class="pageNumber pr-15 hidden-xs"> 
	                                    <table>
	                                        <tbody>
	                                        <tr>
	                                            <td class="pr-5">Page</td>
	                                            <td class="w-75"> 
		                                            <select name="page" id="page" onchange="submit()" class="form-control input-sm">
												  		<c:forEach begin="1" end="${model.orderList.pageCount}" var="page">
															<option value="${page}"
																<c:if test="${page == (model.orderList.page+1)}">selected</c:if>>
																${page}
															</option>
														</c:forEach>
												  	</select> 
											  	</td>
	                                             
	                                            <td class="pl-5">of <c:out value="${model.orderList.pageCount}" /></td>
	                                        </tr>
	                                        </tbody>
	                                    </table>
	                                </td> 
	                                <td class="pageSize pr-15 w-150 hidden-xs">
	                                	<select name="size" onchange="document.getElementById('page').value=1;submit()"  class="form-control input-sm" >
										    <c:forTokens items="10,25,50,100,200,500" delims="," var="current">
										  	  <option value="${current}" <c:if test="${current == model.orderList.pageSize}">selected</c:if>>${current} <fmt:message key="perPage" /></option>
										    </c:forTokens>
									  	</select> 
								  	</td>
	                                <td class="pageNav">
	                                    <div class="btn-group btn-group-sm pull-right"> 
	                                        <c:if test="${model.orderList.firstPage}">
	                                        	 <a href="javascript:void(0);" title="Previous" class="btn btn-dark">
		                                            <span class="fa fa-chevron-left"></span>
		                                        </a>
                                        	</c:if>
			 								<c:if test="${not model.orderList.firstPage}">
		 										<a href="<c:url value="payments.jhtm"><c:param name="page" value="${model.orderList.page}"/><c:param name="size" value="${model.orderList.pageSize}"/></c:url>" title="Previous" class="btn btn-dark">
		 											 <span class="fa fa-chevron-left"></span>
	 											</a>
	 										</c:if>
			  							   	<c:if test="${model.orderList.lastPage}">
			  							    	 <a href="javascript:void(0);" title="Next" class="btn btn-dark">
		                                            <span class="fa fa-chevron-right"></span>
		                                        </a>
		  							    	</c:if>
			  								<c:if test="${not model.orderList.lastPage}">
			  									<a href="<c:url value="payments.jhtm"><c:param name="page" value="${model.orderList.page+2}"/><c:param name="size" value="${model.orderList.pageSize}"/></c:url>" title="Next" class="btn btn-dark">
													<span class="fa fa-chevron-right"></span>
												</a>
		  									</c:if> 
	                                    </div>
	                                </td>
	                            </tr>
	                            </tbody>
	                        </table>
	                    </td>
	                </tr>
	                </tbody>
	            </table>
	        </div> 
             <table class="responsiveBootstrapFooTable table table-hover" data-sorting="true">
                <thead>
                <c:set var="cols" value="0"/> 
                <input type="hidden" name="username" value="${model.username}"/>
                <tr class="bg-light"> 
                	
               	 	<th class="text-nowrap w-50"" data-type="" data-breakpoints="" data-sortable="false"></th>
               	 	
                    <th class="text-nowrap" data-type="" data-breakpoints="" data-sortable="false">
                    	<fmt:message key="orderNumber" />
                   	</th>
                   	 
                    <th class="text-nowrap" data-type="html"> 
                    	<fmt:message key="orderDate" />
                    </th>  
                     
                    <th class="text-nowrap" data-type="html" data-breakpoints="xs sm">   
			    	    <fmt:message key="dateShipped" />
		    	    </th> 
			    	 
			    	 <th class="text-nowrap" data-type="html" data-breakpoints="xs sm">
			    	 	<fmt:message key="sku" />
		    	 	</th>
		    	 	
                    <th class="text-nowrap" data-type="html" data-breakpoints="xs sm md">    
			    	    <fmt:message key="companyName" />
                    </th>
                    
                    <th class="text-nowrap" data-type="html" data-breakpoints="xs sm md"> 
			    	    <fmt:message key="cost" />
                    </th>
                     
                    <th class="text-nowrap" data-type="html" data-breakpoints="xs sm md lg">
                       Qty Ordered
                    </th> 
                    
                    <th class="text-nowrap" data-type="html" data-breakpoints="xs sm md lg">
                       Qty Processed
                    </th> 
                      <th class="text-nowrap" data-type="html" data-breakpoints="xs sm md"> 
			    	    <fmt:message key="subTotal" />
                    </th>
                     
                    <th class="text-nowrap" data-type="html" data-breakpoints="xs sm md lg">
                      	<fmt:message key="paymentFor" />
                    </th> 
                    
                    <th class="text-nowrap" data-type="html" data-breakpoints="xs sm md">
                       	<fmt:message key="amountToPay" />
                    </th>
                      
                    <th class="text-nowrap" data-type="html" data-breakpoints="xs sm md"> 
			    	    <fmt:message key="status" />
                    </th>
                     
                    <th class="text-nowrap" data-type="html" data-breakpoints="xs sm md lg">
                       	<fmt:message key="productConsignmentNote" />
                    </th>
                     
                    <th class="text-nowrap" data-type="html" data-breakpoints="xs sm md lg">
                     	<fmt:message key="dateOfPay" />
                    </th>
                    
                    <th class="text-nowrap" data-type="html" data-breakpoints="xs sm md">
                       	<fmt:message key="paymentMethod" />
                    </th> 
                    <th class="text-nowrap" data-type="html" data-breakpoints="xs sm md lg">
                     	<fmt:message key="note" />
                    </th>
                </tr>
                </thead>
                <tbody>
                	<c:set var="totalSubTotal" value="0.0" />
			  		<c:forEach items="${model.orderList.pageList}" var="order" varStatus="status">
		                <tr>
		                    <td>
		                        <c:out value="${status.count }" />.
		                    </td>  
		                     
					    	<td>
						    	<a href="../orders/invoice.jhtm?order=${order.orderId}" class="nameLink"><c:out value="${order.orderId}"/><input type="hidden" name="__orderId_" value="${order.orderId}"/></a>
						    </td>  
						    
					    	<td>
					    		<fmt:formatDate type="date" timeStyle="default" value="${order.dateOrdered}"/>
					        </td> 
					         
						  	<td><fmt:formatDate type="date" timeStyle="default" value="${order.orderShipDate}" /></td> 
						  	
					     	<td>
								<input type="hidden" name="productSku_${order.orderId}" value="${order.productSku}"/>
				  				<c:out value="${order.productSku}"/>
							</td>
							
						    <td><c:out value="${order.companyName}"/></td>
						    
						    <td>
						    	<c:choose>
							  		<c:when test="${order.cost == null or order.costPercent == null }">
							  			&nbsp;
							  		</c:when>
							  		<c:when test="${order.costPercent == '1' }">
							  			<c:out value="${order.cost}"/>%
							  		</c:when>
							  		<c:otherwise>
							  			<fmt:message key="${siteConfig['CURRENCY'].value}" /><fmt:formatNumber value="${order.cost}" pattern="#,##0.00"/>
							  		</c:otherwise>
							  	</c:choose>
						    </td>
						    
					     	<td><c:out value="${order.quantityOrdered}"/></td>
						     
				    	 	<td><c:out value="${order.quantity}"/></td>
						     
					     	<td>
						     	<fmt:message key="${siteConfig['CURRENCY'].value}" /><fmt:formatNumber value="${order.subTotal}" pattern="#,##0.00"/><c:set var="totalSubTotal" value="${order.subTotal + totalSubTotal}" />
		  				 	</td>
						     
					     	<td>
								<c:choose>
							  		<c:when test="${order.commission != 0 and order.commission != '' }">
							  			Affiliate 
							  		</c:when>
							  		<c:otherwise>
							  			Consignment
							  		</c:otherwise>
							  	</c:choose>
							</td>
							
							<td>
								<c:choose>
							  		<c:when test="${order.commission != 0 and order.commission != '' }">
							  			<fmt:message key="${siteConfig['CURRENCY'].value}" /><fmt:formatNumber value="${order.commission}" pattern="#,##0.00"/>
							  			<input type="hidden" name="__amountToPay_${status.index}" value="${order.commission}"/>
							  			<input type="hidden" name="__userId_${status.index}" value="${order.userId}"/>
							  		</c:when>
							  		<c:otherwise>
							  			<fmt:message key="${siteConfig['CURRENCY'].value}" /><fmt:formatNumber value="${order.consignment}" pattern="#,##0.00"/>
							  			<input type="hidden" name="__amountToPay_${status.index}_${order.productSku}" value="${order.consignment}"/>
							  			<input type="hidden" name="__userId_${status.index}_${order.productSku}" value="${order.supplierId}"/>
							  		</c:otherwise>
							  	</c:choose>
						  	</td>
						  	
						    <td>	
						  		<c:out value="${order.status}"/>
						  	</td>
						  	
						  	<td>	
							  	<c:out value="${order.productConsignmentNote}"/>
						  	</td>
						  	
						  	<td>
								  <c:choose>
								  	<c:when test="${order.commission != 0 and order.commission != '' }">
									  	<span class="input-group-addon">
		                                    <span class="fa fa-calendar"></span>
		                                </span> 
		                                <input type="text" class="form-control" name="__startDate_${status.index}" id="startDate${status.index}" 
												value="<fmt:formatDate type="date" pattern="${siteConfig['SITE_DATE_FORMAT'].value}" 
												timeZone="${siteConfig['SITE_TIME_ZONE'].value}" value="${order.dateOfPay}" />" 
												data-provide="datetimepicker" data-date-format="MM/DD/YYYY"/>  
									  	 
								  	</c:when>
								  	<c:otherwise>
								  		<input type="text" class="form-control" name="__startDate_${status.index}_${order.productSku}" id="startDate${status.index}_${order.productSku}"
											value="<fmt:formatDate type="date" pattern="${siteConfig['SITE_DATE_FORMAT'].value}" 
											timeZone="${siteConfig['SITE_TIME_ZONE'].value}" value="${order.dateOfPay}" />" 
											data-provide="datetimepicker" data-date-format="MM/DD/YYYY"/>   
								  	</c:otherwise>
								  </c:choose> 				 
							  </td>
							  
							  <td>
								  <c:choose>
								  	<c:when test="${order.commission != 0 and order.commission !=''}">				  		
								  		<select name="__paymentMethod_${status.index}" class="form-control">
								  			<option value=""></option>
								  			<option value="vba"><fmt:message key="vba"/></option>
										  	<c:forTokens items="${siteConfig['VBA_PAYMENT_OPTIONS'].value}" delims="," var="paymentType">
									          <option value="${paymentType}"> <c:if test="${order.paymentMethod == paymentType}">selected</c:if><c:out value="${paymentType}"/></option>
									  	    </c:forTokens>	
								  		</select>
								  	</c:when>
								  	<c:otherwise>
								  		<select name="__paymentMethod_${status.index}_${order.productSku}" class="form-control">
								  			<option value=""></option>
								  			<option value="vba"><fmt:message key="vba"/></option>
										  	<c:forTokens items="${siteConfig['VBA_PAYMENT_OPTIONS'].value}" delims="," var="paymentType">
									          <option value="${paymentType}"> <c:if test="${order.paymentMethod == paymentType}">selected</c:if><c:out value="${paymentType}"/></option>
									  	    </c:forTokens>	
								  		</select>
								  	</c:otherwise>
								  </c:choose>
							  </td>
							  
							  <td>
								  <c:choose>
								  	<c:when test="${order.commission != 0 and order.commission !=''}">				  		
								  		<input class="form-control" name="__note_${status.index}" type="text" value="${order.note}" size="20" maxlength="20" class="note">
								  	</c:when>
								  	<c:otherwise>
								  		<input class="form-control" name="__note_${status.index}_${order.productSku}" type="text" value="${order.note}" size="20" maxlength="20" class="note">
								  	</c:otherwise>
								  </c:choose>
							  </td>
		                </tr>
	                </c:forEach> 
	                <tr class="totals">
					  <td style="color:#666666;" colspan="8" align="left"><fmt:message key="total" /></td>
					  <td class="numberCol"><fmt:message key="${siteConfig['CURRENCY'].value}" /><fmt:formatNumber value="${totalSubTotal}" pattern="#,##0.00" /></td>
				  	</tr>	
                </tbody>
            </table> 
             
	        <div class="panel-footer">
	            <div class="clearfix"> 
	                <div class="pull-right">
	                	<sec:authorize ifAnyGranted="ROLE_AEM,ROLE_ADMIN_${prop_site_id},ROLE_OUTGOING_PAYMENT_CREATE">
							<input type="submit" name="__make_payment" value="<fmt:message key="makePayment" />" onclick="return checkStatus();" class="btn btn-primary">			
					  	</sec:authorize> 
	                </div>
	            </div>
	        </div>
	    </div>
	    <!-- End: Payment List Table -->
    </form>
</section>
<!-- End: Content -->

</sec:authorize>

  </tiles:putAttribute>
</tiles:insertDefinition>

<script type="text/javascript">
<!--
function checkStatus() {
	return confirm("An email will be sent to every supplier. Are you sure about the payments? ");
	return true;
}
//-->
</script>
 