<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="/WEB-INF/tlds/spring.tld" prefix="spring" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ taglib uri="http://www.springframework.org/security/tags" prefix="sec" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ taglib uri="/WEB-INF/tlds/custom.tld" prefix="wj" %>

<c:if test="${gSiteConfig['gPRICE_TABLE'] > 0}"> 
	<div class="line">
		<div class="form-row">
			<div class="col-label">
				<label for="customer_priceTable" class="col-form-label">
					<fmt:message key="productPriceTable" />
				</label>
			</div>
			<div class="col-lg-4"> 
				<form:select path="customer.priceTable" id="customer_priceTable" class="custom-select">
					<form:option value="0"><fmt:message key="regularPricing" /></form:option>
					<c:forEach begin="1" end="${gSiteConfig['gPRICE_TABLE']}" var="priceTable">
						<c:set var="key" value="priceTable${priceTable}"/>
						<c:choose>
							<c:when test="${labels[key] != null and labels[key] != ''}">
								<c:set var="label" value="${labels[key]}"/>
							</c:when>
							<c:otherwise>
								<c:set var="label">
									<fmt:message key="productPriceTable" /> ${priceTable}
								</c:set>
							</c:otherwise>
						</c:choose>
						<form:option value="${priceTable}"><c:out value="${label}"/></form:option>
					</c:forEach>
				</form:select>
	       </div>
	   </div>
   </div>
</c:if>