<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="/WEB-INF/tlds/spring.tld" prefix="spring" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ taglib uri="http://www.springframework.org/security/tags" prefix="sec" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ taglib uri="/WEB-INF/tlds/custom.tld" prefix="wj" %>

<div id="tab_4" class="tab-pane" role="tabpanel">
	<div class="line">
		<div class="form-row">
			<div class="col-label">
				<label for="customer_shippingTitle" class="col-form-label">
					<fmt:message key="shipping" /> <fmt:message key="options" />
				</label>
			</div>
			<div class="col-lg-4">
				<form:select path="customer.shippingTitle" id="customer_shippingTitle" class="custom-select">
					<form:option value=""><fmt:message key="default" /></form:option>
					<form:option value="No Shipping">No Shipping</form:option>
				</form:select>
			</div>
		</div>
	</div>
	<div class="line">
		<div class="form-row">
			<div class="col-label">
				<label for="customer_payment" class="col-form-label">
					<fmt:message key="payment" /> <fmt:message key="options" />
				</label>
			</div>
			<div class="col-lg-4"> 
				<form:select path="customer.payment" id="customer_payment" class="custom-select">
					<form:option value=""><fmt:message key="payment.immediate" /></form:option>
					<form:option value="ccOnly">credit card only</form:option>
					<c:forEach items="${paymentList}" var="paymentMethod">
						<c:if test="${paymentMethod.enabled and (not (fn:toLowerCase(fn:trim(paymentMethod.title)) == 'credit card'))}">
							<form:option value="${paymentMethod.title}">${paymentMethod.title}</form:option>
						</c:if>
					</c:forEach>
				</form:select>
			</div>
		</div>
	</div>
	<c:choose>
		<c:when test="${siteConfig['UNLIMITED_PRICE_TABLES'].value}">
			<%@ include file="/WEB-INF/jsp/admin-responsive/customers/unlimitedPriceTableDropdown.jsp" %>
		</c:when>
		<c:otherwise>
			<%@ include file="/WEB-INF/jsp/admin-responsive/customers/regularPriceTableDropdown.jsp" %>
		</c:otherwise>
	</c:choose>
                            
	<c:if test="${fn:length(siteConfig['SEHI_FEED'].value) > 0}">
		<div class="line">
			<div class="form-row">
				<div class="col-label">
					<label for="customer_priceTable" class="col-form-label">
						<fmt:message key="productPriceLevel" />
					</label>
				</div>
				<div class="col-lg-4"> 
					<form:select path="customer.priceLevel" id="customer_priceLevel" class="custom-select">
						<form:option value="">None</form:option>
						<c:forEach items="${priceLevels}" var="priceLevel">
							<form:option value="${priceLevel.id}"><c:out value="${priceLevel.name}" escapeXml="false"/></form:option>
						</c:forEach>
					</form:select> 
				</div>
			</div>
		</div>
	</c:if>
	<div class="line">
		<div class="form-row">
			<div class="col-label">
				<label for="customer_seeHiddenPrice" class="col-form-label">
					<fmt:message key="SeeHiddenPrice" />
				</label>
			</div>
			<div class="col-lg-4">
				<label class="custom-control custom-checkbox">
					<form:checkbox path="customer.seeHiddenPrice" id="customer_seeHiddenPrice" class="custom-control-input"/>
					<span class="custom-control-indicator"></span>
					<span class="custom-control-description">Yes</span>
				</label>
			</div>
		</div>
	</div>
	<c:if test="${siteConfig['SALES_REP_COMMISSION'].value == 'true'}">
		<div class="line">
			<div class="form-row">
				<div class="col-label">
					<label for="customer.seeHiddenPrice" class="col-form-label">
						<fmt:message key="DisableSalesRepCommission" />
					</label>
				</div>
				<div class="col-lg-4">
					<label class="custom-control custom-checkbox">
						<form:checkbox path="customer.disableSalesRepCommission" id="customer_disableSalesRepCommission" class="custom-control-input"/>
						<span class="custom-control-indicator"></span>
						<span class="custom-control-description">Yes</span>
					</label>
				</div>
			</div>
		</div>
	</c:if>
	<div class="line">
		<div class="form-row">
			<div class="col-label">
				<label class="col-form-label">
					<fmt:message key="protectedAccess" />
				</label>
			</div>
			<div class="col-lg-8">
				<c:forEach items="${customerForm.protectedAccess}" var="protectedAccess" varStatus="protectedStatus">
					<c:set var="key" value="protected${protectedStatus.index+1}"/>
					<c:choose>
						<c:when test="${labels[key] != null and labels[key] != ''}">
							<c:set var="label" value="${labels[key]}"/>
   						</c:when>
   						<c:otherwise>
   							<c:set var="label"> ${protectedStatus.index+1}</c:set>
   						</c:otherwise>
					</c:choose>
					<label class="custom-control custom-checkbox">
						<input name="__enabled_${protectedStatus.index}" id="__enabled_${protectedStatus.index}" type="checkbox" <c:if test="${protectedAccess}">checked</c:if>  class="custom-control-input">	
						<span class="custom-control-indicator"></span>
						<span class="custom-control-description">${label}</span>
					</label>
				</c:forEach>   
			</div>
		</div>
	</div>
	<c:if test="${siteConfig['SHIPMENT_RECEIVING_DAYS'].value == 'true'}">
		<div class="line">
			<div class="form-row">
				<div class="col-label">
					<label for="customer.seeHiddenPrice" class="col-form-label">
						<fmt:message key="shimentReceivingDays" />
					</label>
				</div>
				<div class="col-lg-8">
					<c:forEach items="${customerForm.shipmentDays}" var="shipmentDay" varStatus="shipmentDayStatus">
						<c:set var="key" value="shipment${shipmentDayStatus.index+1}"/>
					  	<c:set var="label"> 
						  	<c:choose>
							    <c:when test="${shipmentDayStatus.index == 0}">Sunday</c:when>
							    <c:when test="${shipmentDayStatus.index == 1}">Monday</c:when>
							    <c:when test="${shipmentDayStatus.index == 2}">Tuesday</c:when>
							    <c:when test="${shipmentDayStatus.index == 3}">Wednesday</c:when>
							    <c:when test="${shipmentDayStatus.index == 4}">Thursday</c:when>
							    <c:when test="${shipmentDayStatus.index == 5}">Friday</c:when>
							    <c:when test="${shipmentDayStatus.index == 6}">Saturday</c:when>
						  	</c:choose>
					  	</c:set>
						<label class="custom-control custom-checkbox">
							<input name="__shipment_days${shipmentDayStatus.index}" id="__shipment_days${shipmentDayStatus.index}" type="checkbox" <c:if test="${shipmentDay}">checked</c:if> class="custom-control-input">	
							<span class="custom-control-indicator"></span>
							<span class="custom-control-description">${label}</span>
						</label>
					</c:forEach>
				</div>
			</div>
		</div>
	</c:if>
	<div class="line">
		<div class="form-row">
			<div class="col-label">
				<label for="customer_loginSuccessURL" class="col-form-label">
					<fmt:message key="loginSuccessPage" />
				</label>
			</div>
			<div class="col-lg-4">
				<form:input path="customer.loginSuccessURL" id="customer_loginSuccessURL" maxlength="50" class="form-control" value="${loginSuccessURL}"/>
			</div>
		</div>
	</div>
	<c:if test="${gSiteConfig['gSHOPPING_CART'] == true and gSiteConfig['gSALES_REP'] == true}">
		<div class="line">
			<div class="form-row">
				<div class="col-label">
					<label for="customer_salesRepId" class="col-form-label">
						<fmt:message key="salesRep" />
					</label>
				</div>
				<div class="col-lg-4">
					<form:select path="customer.salesRepId" id="customer_salesRepId" onchange="changeHref(${customerForm.customer.id},this.value);" class="custom-select">
						<form:option value=""><fmt:message key="none" /></form:option>
						<c:forEach items="${salesReps}" var="salesRep">
							<form:option value="${salesRep.id}"><c:out value="${salesRep.name}" /></form:option>
						</c:forEach>
					</form:select>
	            </div>    
				<c:if test="${not customerForm.newCustomer}">
					<div class="col-lg-4">
						<a id="emailSalesRep" href="email.jhtm?id=${customerForm.customer.id}&salesrepid=${customerForm.customer.salesRepId}" class="btn btn-default"><fmt:message key="sendEmail" /></a>
					</div>
				</c:if>
			</div>
		</div>
	</c:if>
	<div class="line">
		<div class="form-row">
			<div class="col-label">
				<label for="customer_poRequired" class="col-form-label">
					<fmt:message key="purchaseOrderNumber" />
				</label>
			</div>
			<div class="col-lg-4">
				<form:select path="customer.poRequired" id="customer_poRequired" class="custom-select">
					<form:option value=""><fmt:message key="normal" /></form:option>
					<form:option value="warning"><fmt:message key="warning" /></form:option>
					<form:option value="required"><fmt:message key="required" /></form:option>
				</form:select>
			</div>
		</div>
	</div>
	<div class="line">
		<div class="form-row">
			<div class="col-label">
				<label for="customer_groupIdList" class="col-form-label">
					<fmt:message key="inGroup" />
				</label>
			</div>
			<div class="col-lg-4"> 
				<form:select path="customer.groupIdList" id="customer_groupIdList" multiple="true" class="form-control" >
					<form:option value="" label="Please Select"></form:option>
					<c:forEach items="${groupList}" var="group">
						<form:option value="${group.id}" label="${group.name}"></form:option>
					</c:forEach>      
				</form:select>	
			</div>
		</div>
	</div>
</div>