<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/security/tags" prefix="sec" %>

<tiles:insertDefinition name="admin.responsive-template.customers" flush="true">  
	<tiles:putAttribute name="topbar" value="/WEB-INF/jsp/admin-responsive/common/topbar.jsp?tab=partnersList">
	</tiles:putAttribute>
	<tiles:putAttribute name="content" type="string">
	 
		<!-- Begin: Content -->
		
		<section id="content" class="animated fadeIn"> 
		    <!-- Begin: Orders Table --> 
			<form action="partnersList.jhtm" method="post" id="list" name="list_form">  
			    <div class="panel panel-default">  
	                <div class="panel-menu">
	                    <table class="fluid-width">
	                        <tbody>
	                        <tr> 
	                            <td class="pageShowing pr-15"> 
									<c:if test="${model.partnersList.nrOfElements > 0}"> 
									  <fmt:message key="showing">
										<fmt:param value="${model.partnersList.firstElementOnPage + 1}"/>
										<fmt:param value="${model.partnersList.lastElementOnPage + 1}"/>
										<fmt:param value="${model.partnersList.nrOfElements}"/>
									  </fmt:message> 
								  	</c:if>
	                            </td>
	                            <td>
	                                <table class="pull-right">
	                                    <tbody>
	                                    <tr>
	                                        <td class="pageNumber pr-15 hidden-xs">
	                                            <table>
	                                                <tbody>
	                                                <tr>
	                                                    <td class="pr-5">Page</td>
	                                                    <td class="w-75">
	                                                    	<select name="page" id="page" onchange="submit()" class="form-control input-sm" title="Page Number">
															  <c:forEach begin="1" end="${model.partnersList.pageCount}" var="page">
															  	<option value="${page}" <c:if test="${page == (model.partnersList.page+1)}">selected</c:if>>${page}</option>
															  </c:forEach>
														  	</select> 
	                                                    </td>
	                                                    <td class="pl-5">of <c:out value="${model.partnersList.pageCount}"/>  </td>
	                                                </tr>
	                                                </tbody>
	                                            </table>
	                                        </td> 
	                                        <td class="pageSize pr-15 w-150 hidden-xs">
	                                        	<select name="size" onchange="document.getElementById('page').value=1;submit()" class="form-control input-sm" >
												    <c:forTokens items="10,25,50,100,500" delims="," var="current">
												  	  <option value="${current}" <c:if test="${current == model.partnersList.pageSize}">selected</c:if>>${current} <fmt:message key="perPage" /></option>
												    </c:forTokens>
											    </select>  
	                                        </td>
	                                        <td class="pageNav"> 
	                                            <div class="btn-group btn-group-sm pull-right">
		                                            <c:if test="${model.partnersList.firstPage}">
												 		<a href="javascript:void(0);" title="Previous" class="btn btn-dark">
	                                                    	<span class="fa fa-chevron-left"></span>
	                                                	</a>
													</c:if>
													<c:if test="${not model.partnersList.firstPage}">
														<a class="btn btn-dark" href="<c:url value="partnersList.jhtm"><c:param name="page" value="${model.partnersList.page}"/><c:param name="id" value="${model.id}"/></c:url>" >
															 <span class="fa fa-chevron-left"></span>
														</a>
													</c:if> 
													<c:if test="${model.partnersList.lastPage}">
													 	<a href="javascript:void(0);" title="Next" class="btn btn-dark">
													 		<span class="fa fa-chevron-right"></span>
														</a>
													</c:if>
													<c:if test="${not model.partnersList.lastPage}">
														<a class="btn btn-dark" href="<c:url value="partnersList.jhtm"><<c:param name="page" value="${model.partnersList.page+2}"/><c:param name="id" value="${model.id}"/></c:url>">
															<span class="fa fa-chevron-right"></span>
														</a>
													</c:if>
												</div>
	                                        </td>
	                                    </tr>
	                                    </tbody>
	                                </table>
	                            </td>
	                        </tr>
	                        </tbody>
	                    </table>
	                </div> 
	                <!-- Table Content -->
                    <table class="responsiveBootstrapFooTable table table-hover" data-sorting="true">
                        <thead>
                        <tr class="bg-light">  
                        	<th class="text-nowrap w-50" data-type="html" data-sortable="false"> 
                        		<label class="checkbox-inline">
		                           #
		                        </label>
		                    </th>
	                     	<th class="text-nowrap" data-type="html" data-breakpoints="xs">
	                     		<fmt:message key="partnerName" />
                     		</th>
                            <th class="text-nowrap" data-type="html" data-sortable="false"> 
                            	<<fmt:message key="created"/> <fmt:message key="date" />
                           	</th> 
                            <th class="text-nowrap" data-type="html" data-breakpoints="xs">
                            	<fmt:message key="active"/> 
                            </th> 
                        </tr>
                        </thead>
                         
                        <tbody>
	                        <c:forEach items="${model.partnersList.pageList}" var="partner" varStatus="status">
		                        <tr>
		                        	<td>
				                        <label class="checkbox-inline">
				                            ${status.index + 1 }.
				                        </label>
				                    </td> 
	                        	 	<td ><a href="addPartners.jhtm?pid=${partner.id}"><c:out value="${partner.partnerName}"/></td>
								    <td><fmt:formatDate type="date" timeStyle="default" value="${partner.createdDate}"/></td>
								    <td >
									    <c:choose>
											<c:when test="${partner.active}"><img src="../graphics/checkbox.png" alt="" title="" border="0"></c:when>
											<c:otherwise><img src="../graphics/box.png" alt="" title="" border="0"></c:otherwise>
										</c:choose>
								    </td> 
		                        </tr> 
	                        </c:forEach> 
                        </tbody>
                    </table> 
                    
                    <div class="panel-footer">
		                <div class="clearfix">  
		                   <div class="pull-right">
	                   			<input type="submit" name="__add" value="<fmt:message key="addPartnersMenuTitle"/>"  class="btn btn-primary mr-10"/> 
			                </div>
		                </div>
		            </div>  
			    </div>
		    </form>
		    <!-- End: Orders Table -->
		</section>
		<!-- End: Content --> 
		
 
	</tiles:putAttribute>
</tiles:insertDefinition>
 