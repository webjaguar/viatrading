<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="/WEB-INF/tlds/spring.tld" prefix="spring" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ taglib uri="http://www.springframework.org/security/tags" prefix="sec" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ taglib uri="/WEB-INF/tlds/custom.tld" prefix="wj" %>

<tiles:insertDefinition name="admin.responsive-template.customers" flush="true">
	<tiles:putAttribute name="topbar" value="/WEB-INF/jsp/admin-responsive/common/topbar.jsp?tab=customerFields"> 
	</tiles:putAttribute>
	<tiles:putAttribute name="content" type="string"> 
 		<sec:authorize ifAnyGranted="ROLE_AEM,ROLE_ADMIN_${prop_site_id},ROLE_CRM">  
			<!-- Begin: Content -->
			<section id="content" class="animated fadeIn"> 
			    <form:form commandName="crmFormBuilderForm" method="post"> 
							 	
		  		  	<!-- Error Message -->
			  	  	<c:if test="${!empty message}">
				  		<div class="message"><spring:message code="${message}" /></div>
				  	</c:if>
				 	
				 	<div class="panel panel-default">
			         	<ul class="nav panel-tabs panel-tabs-border">
			                <li class="active">
                                <li class="active">
				                    <a href="javascript:void(0);" data-toggle="tab" data-target="#tab_1"><fmt:message key="customerFields"/></a>
				                </li> 
			                </li>
			            </ul>
			            
			            <!-- Body -->
	                    <div class="panel-body p-0">
			                <div class="tab-content">
			                    <div id="tab_1" class="tab-pane active">
			                        <div class="table-responsive">
			                            <table class="table table-hover table-fixed-layout">
			                                <thead>
			                                    <tr class="bg-light">
			                                    	<th>&nbsp;</th>
												    <th><fmt:message key="Enabled" /></th>
												    <th><fmt:message key="required" /></th>
											     	<th class="w-50">Event Registration</th>
												    <th><fmt:message key="public" /></th>
												    <th><fmt:message key="showOnInvoice" /></th>
												    <th><fmt:message key="showOnQuote" /></th>
												    <c:if test="${siteConfig['CUSTOMER_FIELDS_ON_INVOICE_EXPORT'].value == 'true'}"	>
											    		<th class="w-50"><fmt:message key="showOnInvoice" /> (Export)</th>
												    </c:if>
												    <th class="w-80"><fmt:message key="customerfieldtype" /></th>
												    <th class="w-200"><fmt:message key="multiStore" /></th>
												    <th><fmt:message key="Name" /></th>
												    <th><fmt:message key="PreValue" /><span style="float:left"><img class="toolTipImg" title="Note::Seperate values with comma. Don't use space before/after comma." src="../graphics/question.gif" /></span></th>
											 	 </tr>
			                                </thead>
			                                <tbody>
			                                	<c:forEach items="${customerFieldsForm.customerFields}" var="customerField" varStatus="status">
												  <tr>
												    <td><fmt:message key="Field" /> <c:out value="${status.index + 1}"/> : </td>
												    <td><input class="form-control" name="__enabled_${customerField.id}" type="checkbox" <c:if test="${customerField.enabled}">checked</c:if>></td>
												    <td><input class="form-control" name="__required_${customerField.id}" type="checkbox" <c:if test="${customerField.required}">checked</c:if>></td>
												    <td><input class="form-control" name="__eventRegistration_${customerField.id}" type="checkbox" <c:if test="${customerField.eventRegistration}">checked</c:if>></td>
												    <td><input class="form-control" name="__publicField_${customerField.id}" type="checkbox" <c:if test="${customerField.publicField}">checked</c:if>></td>
												    <td><input class="form-control" name="__showOnInvoice_${customerField.id}" type="checkbox" <c:if test="${customerField.showOnInvoice}">checked</c:if>></td>
												    <td><input class="form-control" name="__showOnQuote_${customerField.id}" type="checkbox" <c:if test="${customerField.showOnQuote}">checked</c:if>></td>
												    <c:if test="${siteConfig['CUSTOMER_FIELDS_ON_INVOICE_EXPORT'].value == 'true'}"	>
											    	<td><input class="form-control" name="__showOnInvoiceExport_${customerField.id}" type="checkbox" <c:if test="${customerField.showOnInvoiceExport}">checked</c:if>></td>
												    </c:if>
												    <td>
														 <select class="form-control" name="__fieldType_${customerField.id}">
													        <option value="1" <c:if test="${customerField.fieldType == 1}">selected</c:if>>Text</option>  
													        <option value="2" <c:if test="${customerField.fieldType == 2}">selected</c:if>>Date</option>  
													    </select>
													</td>
													<td>
														<!-- input field -->
										  				<select class="form-control" name="__multiStoreHost_${customerField.id}">
										  					<option value=" "></option>
											  	    		<c:forEach items="${multiStores}" var="multiStore">
										  					<option value="${multiStore.host}" <c:if test="${multiStore.host == customerField.multiStoreHost}">selected<c:set var="hostFound" value="true"/></c:if>>${multiStore.host}</option>
															</c:forEach>
											    			<c:if test="${hostFound != true and customerField.multiStoreHost != ''}">
												    		<option value="${customerField.multiStoreHost}" selected><c:out value="${customerField.multiStoreHost}"/></option>
															</c:if>
														</select>
								            			<!-- end input field -->	
									 				</td>
												    <td><input class="form-control" type="text" name="__name_${customerField.id}" value="<c:out value="${customerField.name}"/>" class="textfield" maxlength="255" size="50"></td>
												    <td><input class="form-control" type="text" name="__preValue_${customerField.id}" value="<c:out value="${customerField.preValue}"/>" class="textfield" maxlength="1024" size="50"></td>
												  </tr>
												  </c:forEach>
			                                     
			                                </tbody>
			                            </table>
			                        </div>
			                    </div>
			                </div>
			            </div>
 
			            <!-- Footer -->
			            <div class="panel-footer">
			                <div class="clearfix">
			                    <div class="pull-right">
		                   	 		<input type="submit" value="<spring:message code="Update"/>"  class="btn btn-primary"> 
								</div>
							</div>
						</div>
			   		</div>
			     </form:form>
			</section>
		</sec:authorize>
	</tiles:putAttribute>
</tiles:insertDefinition> 