<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
	<c:forEach var="thisSalesRep" items="${salesRepTree.subAccounts}">
		<option value="${thisSalesRep.id}" <c:if test="${thisSalesRep.id == recursionPageId}">selected</c:if>>
			<c:forEach begin="1" end="${level}">
		    &nbsp;&nbsp;
		  	</c:forEach>
		  	<c:out value="${thisSalesRep.name}"/>
		</option> 
		<c:if test="${thisSalesRep.subAccounts != null and fn:length(thisSalesRep.subAccounts) gt 0}">
			<c:set var="level" value="${level + 1}" scope="request"/>
			<c:set var="salesRepTree" value="${thisSalesRep}" scope="request"/>
			<jsp:include page="/WEB-INF/jsp/admin2/orders/recursion.jsp"/>
			<c:set var="level" value="${level - 1}" scope="request"/>
		</c:if>
	</c:forEach>