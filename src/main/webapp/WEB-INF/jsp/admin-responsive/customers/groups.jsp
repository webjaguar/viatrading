<%@ page import="java.net.*" %>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/security/tags" prefix="sec" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ taglib uri="/WEB-INF/tlds/custom.tld" prefix="wj" %>
                              
<tiles:insertDefinition name="admin.responsive-template.customers.groups" flush="true">
	<tiles:putAttribute name="css">
	</tiles:putAttribute>
	<tiles:putAttribute name="content" type="string">
	    <sec:authorize ifAnyGranted="ROLE_AEM,ROLE_ADMIN_${prop_site_id},ROLE_CRM_GROUP">  
		    <form action="customerGroupList.jhtm" method="post" id="list" name="list_form">
				<div class="page-banner">
					<div class="breadcrumb"><span><a href="${_contextpath}/admin/catalog/dashboard.jhtm">Home</a></span><i class="sli-arrow-right"></i><span><a href="${_contextpath}/admin/customers/customerList.jhtm">Customers</a></span><i class="sli-arrow-right"></i><span>Groups</span></div>
				</div>
				<div class="page-head">
					<div class="row justify-content-between">
						<div class="col-sm-8 title">
							<h3>Groups</h3>
						</div>
						<div class="col-sm-4 actions">
							<div class="dropdown dropdown-actions">
								<button type="button" class="btn-outline dropdown-toggle active btn btn-primary" data-toggle="dropdown">
		                             <i class="sli-settings"></i> Actions
								</button>
								<div class="dropdown-menu dropdown-menu-right">
									<a class="dropdown-item"><fmt:message key="add" /></a>
	                   				<a class="dropdown-item">Delete Selected Groups</a> 
		                         </div>
		                    </div>
		                    <div class="dropdown dropdown-setting">
								<button type="button" class="btn-outline dropdown-toggle active btn btn-primary" data-toggle="dropdown">
		                             <i class="sli-screen-desktop"></i> Display
		                         </button>	                         
								<div class="dropdown-menu dropdown-menu-right">
									<div class="form-group">
				                    		<div class="option-header">Rows</div>
										<select class="custom-select form-control" name="size" onchange="document.getElementById('page').value=1;submit();">
											<c:forTokens items="10,25,50,100,200,500" delims="," var="current">
									  	  		<option value="${current}" <c:if test="${current == groupCustomerSearch.pageSize}">selected</c:if>>${current}</option>
								  			</c:forTokens>
										</select>
									</div>
									<div class="form-group">
				                    		<div class="option-header">Columns</div>
				                    		<div id="column-hider"></div>
									</div>
								</div>
		                    </div>
						</div>
					</div>
				</div>
				<div class="page-body">
					<div class="grid-stage">
						<c:if test="${model.groups.nrOfElements > 0}">
							<div class="page-stat">
								<fmt:message key="showing">
									<fmt:param value="${model.groups.firstElementOnPage + 1}"/>
									<fmt:param value="${model.groups.lastElementOnPage + 1}"/>
									<fmt:param value="${model.groups.nrOfElements}"/>
								</fmt:message>
							</div>
						</c:if>
					    <table id="grid"></table>
					    <div class="footer">
			                <div class="float-right">
								<c:if test="${model.groups.firstPage}">
							 		<span class="previous"><i class="sli-arrow-left" style="font-size: 0.7rem"></i> Previous</span>
								</c:if>
								<c:if test="${not model.groups.firstPage}">
									<a href="<c:url value="customerGroupList.jhtm"><c:param name="page" value="${model.groups.page}"/><c:param name="id" value="${model.id}"/><c:param name="size" value="${model.groups.pageSize}"/></c:url>">
										<span class="previous"><i class="sli-arrow-left" style="font-size: 0.7rem"></i> Previous</span>
									</a>
								</c:if>	
								<span class="status"><c:out value="${groupCustomerSearch.page}" /> of <c:out value="${model.groups.pageCount}" /></span>
								<c:if test="${model.groups.lastPage}">
  									<span class="next">Next <i class="sli-arrow-right" style="font-size: 0.7rem"></i></span>
 								</c:if>
								<c:if test="${not model.groups.lastPage}">
  									<a href="<c:url value="customerGroupList.jhtm"><c:param name="page" value="${model.groups.page+2}"/><c:param name="id" value="${model.id}"/><c:param name="size" value="${model.groups.pageSize}"/></c:url>">
										<span class="next">Next <i class="sli-arrow-right" style="font-size: 0.7rem"></i></span>
									</a>
 								</c:if>
							</div>
						</div>
					</div>
				</div>
			    <input type="hidden" id="page" name="page" value="${groupCustomerSearch.page}" />
			    <input type="hidden" id="sort" name="sort" value="${groupCustomerSearch.sort}" />
		    </form>
	    </sec:authorize>
	</tiles:putAttribute>
	<tiles:putAttribute name="js">
		<!-- DGRID TABLE SCRIPT MUST BE AT BOTTOM OF THE PAGE -->
		<script src="${_contextpath}/admin-responsive-static/assets/plugins/dojo/dojo.js" data-dojo-config="async: true"></script>
		<script>
			//Table Data Sanitization
			var data = [];
			<c:forEach items="${model.groups.pageList}" var="group" varStatus="status">
				data.push({
					id: '${group.id}',
					offset: '${status.count + model.groups.firstElementOnPage}.',
					name: "${group.name}",
					num_customer: '${group.numCustomer}',
					customerListURL: '<c:url value="customerList.jhtm"><c:param name="groupId" value="${group.id}"/></c:url>',
					created_by: '${wj:escapeJS(group.createdBy)}',
					created: '<fmt:formatDate type="date" timeStyle="default" value="${group.created}"/>',
					<!-- Missing WILDMAN_GROUP -->
				});
			</c:forEach>

			//Columns Definitions
			require(['dojo/_base/lang',
					'dojo/_base/declare',
					'dstore/Memory',
					'dstore/Trackable',
					'dgrid/OnDemandGrid',
					'dgrid/Editor',
					'dgrid/extensions/ColumnHider',
					'dgrid/extensions/ColumnReorder',
					'dgrid/extensions/ColumnResizer',
					'dgrid/Selector',
					'dijit/form/DropDownButton',
					'dijit/DropDownMenu',
					'dijit/MenuItem'
				], function (lang, declare, Memory, Trackable, OnDemandGrid, Editor, ColumnHider, ColumnReorder, ColumnResizer, Selector, DropDownButton, DropDownMenu, MenuItem) {
				var columns = {
					batch: {
						selector: 'checkbox',
						unhidable: true
					},
					offset: {
						label: '#',
						get: function(object){
							return object.offset;
						},
						sortable: false,
						unhidable: true
					},
					name: {
						label: '<fmt:message key="Name" />',
						get: function(object){
							return object.name;
						}
					},
					id: {
						label: '<fmt:message key="id" />',
						get: function(object){
							return object.id;
						},
						sortable: false,
					},
					num_customer:{
						label: '<fmt:message key="customers" />',
						renderCell: function(object){
							var link = document.createElement('a');
							link.href = object.customerListURL;
							link.text = object.num_customer;
							return link;
						},
					},
					created_by:{
						label: '<fmt:message key="createdBy" />',
						get: function(object){
							return object.created_by;
						},
					},
					created:{
						label: '<fmt:message key="created" />',
						get: function(object){
							return object.created;
						},
					}
					<!-- Missing WILDMAN_GROUP -->
				};
				var store = new (declare([Memory, Trackable]))({
					data: data,
					idProperty:"id"
				});
				
				var grid = new (declare([OnDemandGrid, Editor, ColumnHider, ColumnReorder, ColumnResizer, Selector]))({
					collection: store,
					selectionMode: "none",
					allowSelectAll: true,
					columns: columns
				},'grid');
				
				//sorting columns
				grid.on("dgrid-sort", lang.hitch(this, function(e){
					e.preventDefault();						
					var property = e.sort[0].property;
				    var order = e.sort[0].descending ? "desc" : "asc";
				    document.getElementById('sort').value = property + ' ' + order;
				    document.getElementById('list').submit();
				}));
				
				<c:if test="${groupCustomerSearch.sort == 'name desc'}">
					grid.updateSortArrow([{property: 'name', descending: true}],true);
				</c:if>
				<c:if test="${groupCustomerSearch.sort == 'name asc'}">
					grid.updateSortArrow([{property: 'name', ascending: true}],true);
				</c:if>
				
				<c:if test="${groupCustomerSearch.sort == 'num_customer desc'}">
					grid.updateSortArrow([{property: 'num_customer', descending: true}],true);
				</c:if>
				<c:if test="${groupCustomerSearch.sort == 'num_customer asc'}">
					grid.updateSortArrow([{property: 'num_customer', ascending: true}],true);
				</c:if>
				
				<c:if test="${groupCustomerSearch.sort == 'created_by desc'}">
					grid.updateSortArrow([{property: 'created_by', descending: true}],true);
				</c:if>
				<c:if test="${groupCustomerSearch.sort == 'created_by asc'}">
					grid.updateSortArrow([{property: 'created_by', ascending: true}],true);
				</c:if>
				
				<c:if test="${groupCustomerSearch.sort == 'created desc'}">
					grid.updateSortArrow([{property: 'created', descending: true}],true);
				</c:if>
				<c:if test="${groupCustomerSearch.sort == 'created asc'}">
					grid.updateSortArrow([{property: 'created', ascending: true}],true);
				</c:if>
				
				grid.on('dgrid-refresh-complete', function(event) {
					$('#grid-hider-menu').appendTo('#column-hider');
				});
				
				grid.startup();
			});
		</script>
	</tiles:putAttribute>
</tiles:insertDefinition>
 