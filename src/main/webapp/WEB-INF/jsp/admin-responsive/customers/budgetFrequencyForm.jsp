<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="/WEB-INF/tlds/spring.tld" prefix="spring" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ taglib uri="http://www.springframework.org/security/tags" prefix="sec" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ taglib uri="/WEB-INF/tlds/custom.tld" prefix="wj" %>

<div id="tab_12" class="tab-pane">
	<div class="line">
    		<div class="form-row">
    			<div class="col-label">
	        		<label for="customer_rewardPoints" class="col-form-label">
	        			<span class="fa fa-question-circle" data-toggle="tooltip" data-placement="top" title="Note::Edit Budget: Allow this customer to edit Budget frequencies on Front end.">
	        			</span><fmt:message key="budget" /> Edit On Front End
	        		</label>
	        	</div>
			<div class="col-lg-4">
				<label class="custom-control custom-checkbox">
					<form:checkbox path="customer.budgetFrequencyEdit" class="custom-control-input"/>
					<span class="custom-control-indicator"></span>
					<span class="custom-control-description">
						<c:set var="showAffiliateOption" value="true" /> Yes
					</span>
				</label>
			</div>
		</div>
	</div>
	<div class="line">
		<div class="form-row">
			<div class="col-label">
				<label for="customer_rewardPoints" class="col-form-label">
					<span class="fa fa-question-circle" data-toggle="tooltip" data-placement="top" title="Note::Budget Manager: All the order approvals should be sent to selected 'Budget Manager' email address"></span>
					<fmt:message key="budget" /> Manager
				</label>
			</div>
			<div class="col-lg-4">  
				<form:select path="customer.budgetFrequencyManager" class="custom-select">
					<form:option value=""></form:option>
					<c:forEach items="${managersList}" var="parent">
						<c:if test="${customerForm.customer.username != parent.username}">
							<form:option value="${parent.username}">${parent.username}</form:option>
						</c:if>
					</c:forEach>
				</form:select>
			</div>
		</div>
	</div>
	<div class="line">
		<div class="form-row">
			<div class="col-label">
				<label for="customer_rewardPoints" class="col-form-label">
					<fmt:message key="select" /> <fmt:message key="budget" />
				</label>
			</div>
			<div class="col-lg-4">  
				<select id="budFrqLabelsMap" name="budFrqLabelsMap" class="custom-select">
					<option value="">Select Budget Type</option>
					<c:forEach items="${budFrqLabelsMap}" var="type">
						<option value="${type.key}">${type.value}</option>
					</c:forEach>
			 	</select>
            </div>
        </div>
	</div>
	<input type="hidden" value="${customerForm.customer.id}" id="user_id" name="user_id">
	<input type="hidden" value="${group.id}" id="group_id" name="group_id">
</div>