<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="/WEB-INF/tlds/spring.tld" prefix="spring" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ taglib uri="http://www.springframework.org/security/tags" prefix="sec" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ taglib uri="/WEB-INF/tlds/custom.tld" prefix="wj" %>

<div id="tab_6" class="tab-pane" role="tabpanel">
    <div class="line">
        <div class="form-row">
			<div class="col-label">
				<label class="col-form-label">
					<span class="fa fa-question-circle" data-toggle="tooltip" data-placement="top" title="" data-original-title="Note::Points that earned through invoice."></span>
            			<fmt:message key="point" />
				</label>
            </div>
            <div class="col-lg-4">
				<label class="text">
					<c:out value="${customerForm.customer.loyaltyPoint}" />
				</label>
            </div>
        </div>
	</div>
	<div class="line">
		<div class="form-row">
			<div class="col-label">
				<label for="customer_pointValue" class="col-form-label">
					<span class="fa fa-question-circle" data-toggle="tooltip" data-placement="top" title="" data-original-title="Note::Specify the value of aech point here. For example 1Point = 1Dollar or 1Point = 0.5Dollar"></span>
	           		<fmt:message key="pointValue" />
	            </label>
            </div>
            <div class="col-lg-4">
				<div class="input-group">
					<span class="input-group-addon">1 <fmt:message key="point" /> = <fmt:message key="${siteConfig['CURRENCY'].value}" /> </span>
					<form:input path="customer.pointValue" id="customer_pointValue" class="form-control" maxlength="4" />
				</div>
            </div>
        </div>
	</div>
	<div class="line">
		<div class="form-row">
			<div class="col-label">
	            <label for="customer_pointThreshold" class="col-form-label">
					<span class="fa fa-question-circle" data-toggle="tooltip" data-placement="top" title="" data-original-title="Note::Convert points to credit after points reached to this threshold. If threshold is empty or 0, points never convert to credit."></span>
					<fmt:message key="threshold" />
				</label>
			</div>
            <div class="col-lg-4"> 
                <form:input path="customer.pointThreshold" id="customer_pointThreshold" class="form-control" />
            </div>
		</div>
	</div>
</div>