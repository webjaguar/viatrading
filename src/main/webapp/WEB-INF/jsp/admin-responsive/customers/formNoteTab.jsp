<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="/WEB-INF/tlds/spring.tld" prefix="spring" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ taglib uri="http://www.springframework.org/security/tags" prefix="sec" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ taglib uri="/WEB-INF/tlds/custom.tld" prefix="wj" %>

<div id="tab_7" class="tab-pane">
	<div class=line>
	    <div class="form-row">
			<div class="col-label">
				<label for="customer_note" class="col-form-label">
					<fmt:message key="note" /><i class="sli-magnifier"></i>
				</label>
	        </div>
	        <div class="col-lg-8">
				<div class="editor_wrapper" data-editor="customer_note">
					<form:textarea path="customer.note" id="customer_note" class="code-editor" rows="15" />
				</div>
            </div>
        </div>
    </div>
</div>