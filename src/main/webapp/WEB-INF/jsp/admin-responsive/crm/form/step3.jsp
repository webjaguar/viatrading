<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="/WEB-INF/tlds/spring.tld" prefix="spring" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ taglib uri="http://www.springframework.org/security/tags" prefix="sec" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ taglib uri="/WEB-INF/tlds/custom.tld" prefix="wj" %>

<tiles:insertDefinition name="admin.responsive-template.crm.formForm" flush="true">
	<tiles:putAttribute name="css"> 
	</tiles:putAttribute>
	<tiles:putAttribute name="content" type="string"> 
 		<sec:authorize ifAnyGranted="ROLE_AEM,ROLE_ADMIN_${prop_site_id},ROLE_CRM">
		    <form:form commandName="crmFormBuilderForm" method="post">
				<div class="page-banner">
					<div class="breadcrumb"><span><a href="${_contextpath}/admin/catalog/dashboard.jhtm">Home</a></span><i class="sli-arrow-right"></i><span><a href="${_contextpath}/admin/crm">CRM</a></span><i class="sli-arrow-right"></i><span><a href="${_contextpath}/admin/crm/crmFormList.jhtm">Forms</a></span><i class="sli-arrow-right"></i><span>Step 4 of 5</span></div>
				</div>
				<div class="page-head">
					<div class="row justify-content-between">
						<div class="col-sm-8 title">
							<h3>${crmFormBuilderForm.crmForm.formName}</h3>
						</div>
						<div class="col-sm-4 actions">
							<div class="dropdown dropdown-actions">
								<button type="button" class="btn-outline dropdown-toggle active btn btn-primary" data-toggle="dropdown">
									<i class="sli-settings"></i> Actions
		                         </button>
								<div class="dropdown-menu dropdown-menu-right">
									<c:if test="${!crmFormBuilderForm.newCrmForm}">
										<a class="dropdown-item" onClick="return confirm('Delete permanently?')"><spring:message code="delete"/></a>
							        </c:if>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="page-body">
					<div class="main-content">
						<div class="form-section">
							<div class="content">
							 	<ul class="nav nav-tabs" role="tablist">
					                <li class="nav-item">
					                    <a class="nav-link active" data-toggle="tab" role="tab" href="#tab_1">Header and Footer</a>
					                </li>
				                </ul>
				                 <div class="tab-content">
				                 	<div id="tab_1" class="tab-pane active">
										<jsp:include page="step3_tab.jsp" />
									</div>
				                 </div>
			                </div>
			                <div class="footer">
								<div class="form-row">
									<div class="col-lg-4">
									</div>
									<div class="col-lg-6">
										<input type="submit" value="<fmt:message key="preview" />" name="_target4" class="btn btn-primary">
										<input type="submit" value="<fmt:message key="prevStep" />" name="_target2" class="btn btn-default btn-flat">
									</div>
								</div>
				            </div>
		                </div>
					</div>
				</div>
		     </form:form>
		</sec:authorize>
	</tiles:putAttribute>
	<tiles:putAttribute name="js">
	</tiles:putAttribute>
</tiles:insertDefinition>