<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="/WEB-INF/tlds/spring.tld" prefix="spring" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ taglib uri="http://www.springframework.org/security/tags" prefix="sec" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ taglib uri="/WEB-INF/tlds/custom.tld" prefix="wj" %>

<tiles:insertDefinition name="admin.responsive-template.crm.formForm" flush="true">
	<tiles:putAttribute name="css">
		<style>
			#grid .field-name{
				width: 300px;
			}
			#grid .field-options{
				width: 200px;
			}
		</style>
	</tiles:putAttribute>
	<tiles:putAttribute name="content" type="string"> 
 		<sec:authorize ifAnyGranted="ROLE_AEM,ROLE_ADMIN_${prop_site_id},ROLE_CRM"> 
			<form:form commandName="crmFormBuilderForm" method="post">
				<div class="page-banner">
					<div class="breadcrumb"><span><a href="${_contextpath}/admin/catalog/dashboard.jhtm">Home</a></span><i class="sli-arrow-right"></i><span><a href="${_contextpath}/admin/crm">CRM</a></span><i class="sli-arrow-right"></i><span><a href="${_contextpath}/admin/crm/crmFormList.jhtm">Forms</a></span><i class="sli-arrow-right"></i><span>Step 4 of 5</span></div>
				</div>
				<div class="page-head">
					<div class="row justify-content-between">
						<div class="col-sm-8 title">
							<h3>${crmFormBuilderForm.crmForm.formName}</h3>
						</div>
						<div class="col-sm-4 actions">
							<div class="dropdown dropdown-actions">
								<button type="button" class="btn-outline dropdown-toggle active btn btn-primary" data-toggle="dropdown">
									<i class="sli-settings"></i> Actions
		                         </button>
								<div class="dropdown-menu dropdown-menu-right">
									<c:if test="${!crmFormBuilderForm.newCrmForm}">
										<c:if test="${!crmFormBuilderForm.newCrmForm}">
											<a class="dropdown-item" onClick="return confirm('Delete permanently?')"><spring:message code="delete"/></a>
								        </c:if>
									</c:if>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="page-body">
					<div class="main-content">
						<div class="form-section">
							<div class="content">
							 	<ul class="nav nav-tabs" role="tablist">
					                <li class="nav-item">
					                    <a class="nav-link active" data-toggle="tab" role="tab" href="#tab_1">Fields Options</a>
					                </li>
				                </ul>
				                 <div class="tab-content">
				                 	<div id="tab_1" class="tab-pane active">
										<div class="grid-stage">
											<table id="grid"></table>
										</div>
									</div>
				                 </div>
			                </div>
			                <div class="footer">
								<div class="form-row">
									<div class="col-lg-4">
									</div>
									<div class="col-lg-6">
										<input type="submit" value="<fmt:message key="nextStep" />" name="_target3" class="btn btn-primary">
										<input type="submit" value="<fmt:message key="prevStep" />" name="_target1" class="btn btn-default btn-flat">
									</div>
								</div>
				            </div>
		                </div>
	                </div>
                </div>
		     </form:form>
		</sec:authorize>
	</tiles:putAttribute>
	<tiles:putAttribute name="js">
		<!-- DGRID TABLE SCRIPT MUST BE AT BOTTOM OF THE PAGE -->
		<script src="${_contextpath}/admin-responsive-static/assets/plugins/dojo/dojo.js" data-dojo-config="async: true"></script>
		<script>
			function generateMainGrid(){
				//Table Data Sanitization
				var data = [];
				<c:forEach items="${crmFormBuilderForm.crmForm.crmFields}" var="field" varStatus="status">
					data.push({
						number: '${status.index + 1}',
						<c:if test="${field.type gt 2}">
							tooltip: "Separate options and options prices with comma.",
						</c:if>
						name: '${wj:escapeJS(field.fieldName)}',
						options: '${wj:escapeJS(crmFormBuilderForm.crmForm.crmFields[status.index].options)}',
					});
				</c:forEach>
				
				require(["dgrid/List", "dgrid/OnDemandGrid", "dgrid/Selection", "dgrid/Keyboard", "dgrid/Editor", "dijit/form/CheckBox", 
						"dijit/form/TimeTextBox", "dijit/form/Select", "dojo/data/ObjectStore", "dijit/form/FilteringSelect",
						"dojo/_base/declare", "dojo/_base/lang", "dojo/store/Memory", "dojo/store/Observable", "dgrid/test/data/createSyncStore", 
						'dgrid/extensions/ColumnHider', 'dgrid/extensions/ColumnReorder', 'dgrid/extensions/ColumnResizer',
					], function(List, OnDemandGrid, Selection, Keyboard, Editor, CheckBox, TimeTextBox, Select, ObjectStore, FilteringSelect,
							declare, lang, LegacyMemory, LegacyObservable, createSyncStore, ColumnHider, ColumnReorder, ColumnResizer){
					
					//Column Definitions
					var columns = {
						number: {
							label: '#',
							get: function(object){
								return object.number;
							},
							sortable: false,
						},
						name: {
							label: '<fmt:message key="fieldName" />',
							renderCell: function(object){
								var div = document.createElement('div');
								if(object.tooltip){
									var span = document.createElement('span');
									span.className = "fa fa-question-circle";
									span.setAttribute('title', object.tooltip);
									span.setAttribute('data-toggle',"tooltip");
									div.appendChild(span);
								}
								var txt = document.createElement('span');
								txt.textContent = object.name;
								div.appendChild(txt);
								return div;
							},
							sortable: false,
						},
						options: { 
							label: '<fmt:message key="options" />',
							editor: 'textarea',
							get: function(object){
								return object.options;
							},
							sortable: false,
						}
					};
					
					var grid = new (declare([OnDemandGrid, Selection, Keyboard, Editor, ColumnReorder, ColumnResizer]))({
						collection: createSyncStore({ data: data }),
						columns: columns,
						selectionMode: "none",
					},'grid');
					
					grid.on('dgrid-refresh-complete', function(event) {
						$('#grid-hider-menu').appendTo('#column-hider');
					});
					
					grid.startup();
				});
			}

			generateMainGrid();
		</script>
	</tiles:putAttribute>
</tiles:insertDefinition>