<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="/WEB-INF/tlds/spring.tld" prefix="spring" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ taglib uri="http://www.springframework.org/security/tags" prefix="sec" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ taglib uri="/WEB-INF/tlds/custom.tld" prefix="wj" %>

<div id="tab_1" class="tab-pane active">
	<div class="line">
		<div class="form-row">
			<div class="col-lg-4">
				<label for="crmForm_doubleOptIn" class="col-form-label">
					<fmt:message key="doubleOptIn" />
				</label>
			</div>
			<div class="col-lg-4">
				<label class="custom-control custom-checkbox">
      				<form:checkbox path="crmForm.doubleOptIn" id="crmForm_doubleOptIn" class="custom-control-input"/>
      				<span class="custom-control-indicator"></span>
      			</label>
			</div>
		</div>
	</div>
	<div class="line">
		<div class="form-row">
			<div class="col-lg-4">
				<label for="crmForm_header" class="col-form-label">
					<fmt:message key="header" />
				</label>
			</div>
			<div class="col-lg-6">
				<form:textarea rows="10" class="form-control" id="crmForm_header" path="crmForm.header" /> 
			</div>
		</div>
	</div>
	<div class="line">
		<div class="form-row">
			<div class="col-lg-4">
				<label for="crmForm_footer" class="col-form-label">
					<fmt:message key="footer" />
				</label>
			</div>
			<div class="col-lg-6">
				<form:textarea rows="10" class="form-control" id="crmForm_footer" path="crmForm.footer" /> 
			</div>
		</div>
	</div>
	<div class="line">
		<div class="form-row">
			<div class="col-lg-4">
				<label for="crmForm_sendButton" class="col-form-label">
					<fmt:message key="sendButton" />
				</label>
			</div>
			<div class="col-lg-2">
				<form:input path="crmForm.sendButton" class="form-control" id="crmForm_sendButton" />
			</div>
		</div>
	</div>
</div>