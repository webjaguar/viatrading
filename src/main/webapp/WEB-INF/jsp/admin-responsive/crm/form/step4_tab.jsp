<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="/WEB-INF/tlds/spring.tld" prefix="spring" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ taglib uri="http://www.springframework.org/security/tags" prefix="sec" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ taglib uri="/WEB-INF/tlds/custom.tld" prefix="wj" %>

<div id="tab_1" class="tab-pane active">
	<div class="line">
		<div class="form-row">
			<div class="col-lg-4">
				<label for="crmForm_code" class="col-form-label">
					<span class="fa fa-question-circle" data-toggle="tooltip" title="Do not use this code without saving form first."></span>
					Code
				</label>
			</div>
			<div class="col-lg-8">
				<div id="crmForm_code_wrapper" class="editor_wrapper">
					<textarea class="form-control" id="crmForm_code" name="crmForm.code" rows="15" > ${htmlCode}</textarea>
				</div>
			</div>
		</div>                 
	</div>
	<div class="line">
		<div class="form-row">
			<div class="col-lg-4">
				<label class="col-form-label">
					<fmt:message key="form" />
				</label>
			</div>
			<div class="col-lg-8">
				<div class="mt-1"></div>
				${form}
			</div>
		</div>
	</div>
</div>