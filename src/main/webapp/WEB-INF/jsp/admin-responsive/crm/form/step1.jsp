<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="/WEB-INF/tlds/spring.tld" prefix="spring" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ taglib uri="http://www.springframework.org/security/tags" prefix="sec" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ taglib uri="/WEB-INF/tlds/custom.tld" prefix="wj" %>

<tiles:insertDefinition name="admin.responsive-template.crm.formForm" flush="true">
	<tiles:putAttribute name="css">
		<style>
			#grid .field-name{
				width: 250px;
			}
			#grid .field-mapTo{
				width: 250px;
			}
		</style>
	</tiles:putAttribute>
	<tiles:putAttribute name="content" type="string"> 
 		<sec:authorize ifAnyGranted="ROLE_AEM,ROLE_ADMIN_${prop_site_id},ROLE_CRM"> 
			<form:form commandName="crmFormBuilderForm" method="post">
				<div class="page-banner">
					<div class="breadcrumb"><span><a href="${_contextpath}/admin/catalog/dashboard.jhtm">Home</a></span><i class="sli-arrow-right"></i><span><a href="${_contextpath}/admin/crm">CRM</a></span><i class="sli-arrow-right"></i><span><a href="${_contextpath}/admin/crm/crmFormList.jhtm">Forms</a></span><i class="sli-arrow-right"></i><span>Step 2 of 5</span></div>
				</div>
				<div class="page-head">
					<div class="row justify-content-between">
						<div class="col-sm-8 title">
							<h3>${crmFormBuilderForm.crmForm.formName}</h3>
						</div>
						<div class="col-sm-4 actions">
							<div class="dropdown dropdown-actions">
								<button type="button" class="btn-outline dropdown-toggle active btn btn-primary" data-toggle="dropdown">
									<i class="sli-settings"></i> Actions
		                         </button>
								<div class="dropdown-menu dropdown-menu-right">
									<c:if test="${!crmFormBuilderForm.newCrmForm}">
										<c:if test="${!crmFormBuilderForm.newCrmForm}">
											<a class="dropdown-item" onClick="return confirm('Delete permanently?')"><spring:message code="delete"/></a>
								        </c:if>
									</c:if>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="page-body">
					<div class="main-content">
						<div class="form-section">
							<div class="content">
							 	<ul class="nav nav-tabs" role="tablist">
					                <li class="nav-item">
					                    <a class="nav-link active" data-toggle="tab" role="tab" href="#tab_1">Field Details</a>
					                </li>
				                </ul>
				                 <div class="tab-content">
				                 	<div id="tab_1" class="tab-pane active">
										<div class="grid-stage">
											<table id="grid"></table>
										</div>
									</div>
				                 </div>
			                </div>
			                <div class="footer">
								<div class="form-row">
									<div class="col-lg-4">
									</div>
									<div class="col-lg-6">
										<input type="submit" value="<fmt:message key="nextStep" />" name="_target2" class="btn btn-primary">
										<input type="submit" value="<fmt:message key="prevStep" />" name="_target0" class="btn btn-default btn-flat">
									</div>
								</div>
				            </div>
		                </div>
	                </div>
                </div>
			</form:form>
		</sec:authorize>
	</tiles:putAttribute>
	<tiles:putAttribute name="js">
		<!-- DGRID TABLE SCRIPT MUST BE AT BOTTOM OF THE PAGE -->
		<script src="${_contextpath}/admin-responsive-static/assets/plugins/dojo/dojo.js" data-dojo-config="async: true"></script>
		<script>
			function generateMainGrid(){
				//Table Data Sanitization
				var data = [];
				<c:forEach var="field" begin="0" end="${nrOfFileds-1}" varStatus="status">
					data.push({
						number: '${field+1}',
						name: '${wj:escapeJS(crmFormBuilderForm.crmForm.crmFields[field].fieldName)}',
						contactFieldId: '${crmFormBuilderForm.crmForm.crmFields[field].contactFieldId}',
						fieldType: '${crmFormBuilderForm.crmForm.crmFields[field].type}',
						required: '${crmFormBuilderForm.crmForm.crmFields[field].required}'
					});
				</c:forEach>
				
				require(["dgrid/List", "dgrid/OnDemandGrid", "dgrid/Selection", "dgrid/Keyboard", "dgrid/Editor", "dijit/form/CheckBox", 
						"dijit/form/TimeTextBox", "dijit/form/Select", "dojo/data/ObjectStore", "dijit/form/FilteringSelect",
						"dojo/_base/declare", "dojo/_base/lang", "dojo/store/Memory", "dojo/store/Observable", "dgrid/test/data/createSyncStore", 
						'dgrid/extensions/ColumnHider', 'dgrid/extensions/ColumnReorder', 'dgrid/extensions/ColumnResizer',
					], function(List, OnDemandGrid, Selection, Keyboard, Editor, CheckBox, TimeTextBox, Select, ObjectStore, FilteringSelect,
							declare, lang, LegacyMemory, LegacyObservable, createSyncStore, ColumnHider, ColumnReorder, ColumnResizer){
	
					var pageData = {
						identifier: "value",
						items: []
					}
					<c:forEach var="number" begin="1" end="${crmFormBuilderForm.crmForm.pages}">
						pageData.items.push({value: '${number}', name: '${number}'});
					</c:forEach> 
					
					var pageStore = new ObjectStore({
						objectStore: LegacyObservable(new LegacyMemory({ data: pageData })),
						labelProperty: "name",
					});
					
					var mapToData = {
						identifier: "value",
						items: [
							{value:"1010", name:"First Name"},
							{value:"1020", name:"Last Name"},
							{value:"1030", name:"Email 1"},
							{value:"1040", name:"Email 2"},
							{value:"1050", name:"Phone 1"},
							{value:"1060", name:"Phone 2"},
							{value:"1070", name:"Fax"},
							{value:"1080", name:"Street"},
							{value:"1090", name:"City"},
							{value:"1100", name:"State"},
							{value:"1110", name:"Zip"},
							{value:"1120", name:"Country"},
							{value:"1130", name:"Notes"},
							{value:"1140", name:"Address Type"},
							{value:"1150", name:"Attachment"},
							{value:"1160", name:"Company"},
						  	<c:forEach items="${crmContactFields}" var="crmContactField">
								<c:if test="${crmContactField.fieldName != ''}">
									{value:"${crmContactField.id}", name:"${crmContactField.fieldName}"},
								</c:if>
							</c:forEach>
						]
					}

					var mapToStore = new ObjectStore({
						objectStore: LegacyObservable(new LegacyMemory({ data: mapToData })),
						labelProperty: "name",
					});
					
					var fieldTypeData = {
						identifier: "value",
						items: [
							{value:"1", name:"Text"},
							{value:"2", name:"Text Area"},
							{value:"3", name:"Check Box"},
							{value:"4", name:"Radio Button"},
							{value:"5", name:"Drop Down List"},
							{value:"6", name:"Attachment"},
							{value:"7", name:"Validation"},
						]
					}

					var fieldTypeStore = new ObjectStore({
						objectStore: LegacyObservable(new LegacyMemory({ data: fieldTypeData })),
						labelProperty: "name",
					});
					
					var requiredData = {
						identifier: "value",
						items: [
							{value:"false", name:"No"},
							{value:"true", name:"Yes"},
						]
					}

					var requiredStore = new ObjectStore({
						objectStore: LegacyObservable(new LegacyMemory({ data: requiredData })),
						labelProperty: "name",
					});
					
					//Column Definitions
					var columns = {
						number: {
							label: '#',
							get: function(object){
								return object.number;
							},
							sortable: false,
						},
						name: {
							editor: 'text',
							label: '<fmt:message key="fieldName" />',
						},
						page: {
							editor: Select,
							label: '<fmt:message key="page" />',
							field: "page",
							editorArgs: {
								store: pageStore
							}
						},
						mapTo: {
							editor: Select,
							label: '<fmt:message key="mapTo" />',
							field: "mapTo",
							editorArgs: {
								store: mapToStore
							},
							get: function(object){
								return object.contactFieldId;
							}
						},
						fieldType: {
							editor: Select,
							label: '<fmt:message key="fieldType" />',
							field: "fieldType",
							editorArgs: {
								store: fieldTypeStore
							},
							get: function(object){
								return object.fieldType;
							}
						},
						required: {
							editor: Select,
							label: '<fmt:message key="required" />',
							field: "required",
							editorArgs: {
								store: requiredStore
							},
							get: function(object){
								return object.required;
							}
						},
						
					};
					
					var grid = new (declare([OnDemandGrid, Selection, Keyboard, Editor, ColumnReorder, ColumnResizer]))({
						collection: createSyncStore({ data: data }),
						columns: columns,
						selectionMode: "none",
					},'grid');
					
					grid.on('dgrid-refresh-complete', function(event) {
						$('#grid-hider-menu').appendTo('#column-hider');
					});
					
					grid.startup();
				});
			}

			generateMainGrid();
		</script>
	</tiles:putAttribute>
</tiles:insertDefinition> 