<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="/WEB-INF/tlds/spring.tld" prefix="spring" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ taglib uri="http://www.springframework.org/security/tags" prefix="sec" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ taglib uri="/WEB-INF/tlds/custom.tld" prefix="wj" %>

<div id="tab_1" class="tab-pane active">
	<div class="line"> 
		<div class="form-row">
			<div class="col-lg-4">
				<label for="crmForm_formName" class="col-form-label">  
					<fmt:message key="name" />
				</label>
			</div>
			<div class="col-lg-4">
				<form:input path="crmForm.formName" class="form-control" maxlength="255" id="crmForm_formName" />
			</div>
		</div>
	</div>
	<div class="line">
		<div class="form-row">
			<div class="col-lg-4">
				<label class="col-form-label">  
					<span class="fa fa-question-circle" data-toggle="tooltip" title="Only one Email is required."></span>
					<fmt:message key="recipientEmail" />
				</label>
			</div>
			<div class="col-lg-4">
				<div class="input-group"> 
					<form:input path="crmForm.recipientEmail" class="form-control" maxlength="255" />
				</div>
			</div>
		</div> 
	</div>
	<div class="line">
		<div class="form-row">
			<div class="col-lg-4">
				<label class="col-form-label">  
					<span class="fa fa-question-circle" data-toggle="tooltip" title="Only works with Group which has PRIMARY GROUP flag ON."></span>
					<fmt:message key="checkGroupRecipientEmail" />
				</label>
			</div>
			<div class="col-lg-4">
				<div class="input-group">
					<label class="custom-control custom-checkbox">
						<form:checkbox path="crmForm.checkGroupRecipientEmail" class="custom-control-input"/>
						<span class="custom-control-indicator"></span>
					</label>
				</div>
			</div>
		</div> 
	</div>
	<div class="line">
		<div class="form-row">
			<div class="col-lg-4">
				<label for="crmForm_recipientCcEmail" class="col-form-label">
					<span class="fa fa-question-circle" data-toggle="tooltip" title="Multiple Emails is possible. Provide comma separated emails."></span>
					<fmt:message key="cc" />
				</label>
			</div>
			<div class="col-lg-4">
				<form:textarea path="crmForm.recipientCcEmail" id="crmForm_recipientCcEmail" class="form-control" rows="2"/>
			</div>
		</div>
	</div>
	<div class="line">
		<div class="form-row">
			<div class="col-lg-4">
				<label for="crmForm_recipientBccEmail" class="col-form-label">
					<span class="fa fa-question-circle" data-toggle="tooltip" title="Multiple Emails is possible. Provide comma separated emails."></span>
					<fmt:message key="bcc" />
				</label>
			</div>
			<div class="col-lg-4">
				<form:textarea path="crmForm.recipientBccEmail" id="crmForm_recipientBccEmail" class="form-control" rows="2"/>
			</div>
		</div>
	</div>
	<div class="line">
		<div class="form-row">
			<div class="col-lg-4">
				<label for="crmAccount_returnUrl" class="col-form-label"> 
					<fmt:message key="returnUrl" />
				</label>
			</div>
			<div class="col-lg-4"> 
				<form:input path="crmForm.returnUrl" id="crmForm_returnUrl" class="form-control" maxlength="255"/>
			</div>
		</div>
	</div>
	<div class="line"> 
		<div class="form-row">
			<div class="col-lg-4">
				<label for="crmForm_actionUrl" class="col-form-label"> 
					<fmt:message key="actionUrl" />
				</label>
			</div>
			<div class="col-lg-4"> 
				<form:input path="crmForm.actionUrl" id="crmForm_actionUrl" class="form-control" maxlength="255"/>
			</div>
		</div>
	</div>
	<div class="line">
		<div class="form-row">
			<div class="col-lg-4">
				<label for="crmForm_numberOfFields" class="col-form-label"> 
					<fmt:message key="numberOfFields" />
				</label>
			</div>
			<div class="col-lg-4"> 
				<form:input path="crmForm.numberOfFields" id="crmForm_numberOfFields" class="form-control" maxlength="255"/>
			</div>
		</div>
	</div>
	<div class="line">
		<div class="form-row">
			<div class="col-lg-4">
				<label for="crmForm_checkEventRegistration" class="col-form-label">   
					<fmt:message key="eventRegistration" />
				</label>
			</div>
			<div class="col-lg-4">
				<label class="custom-control custom-checkbox">
					<form:checkbox path="crmForm.checkEventRegistration" id="crmForm_checkEventRegistration" class="custom-control-input"/>
					<span class="custom-control-indicator"></span>
				</label>
			</div>
		</div>  
	</div>
	<div class="line">
		<div class="form-row">
			<div class="col-lg-4">
				<label for="crmForm_pages" class="col-form-label">   
					<fmt:message key="pages" />
				</label>
			</div>
			<div class="col-lg-4">
				<form:select path="crmForm.pages" class="custom-select" id="crmForm_pages">
					<c:forEach var="number" begin="1" end="4">
						<form:option value="${number}"><c:out value="${number}" /></form:option>
					</c:forEach>
				</form:select>
			</div>
		</div>
	</div> 
</div>