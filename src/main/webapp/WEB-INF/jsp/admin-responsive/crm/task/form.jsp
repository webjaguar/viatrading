<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="/WEB-INF/tlds/spring.tld" prefix="spring" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ taglib uri="http://www.springframework.org/security/tags" prefix="sec" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ taglib uri="/WEB-INF/tlds/custom.tld" prefix="wj" %>

<tiles:insertDefinition name="admin.responsive-template.crm.taskForm" flush="true">
	<tiles:putAttribute name="css"> 
	</tiles:putAttribute>
	<tiles:putAttribute name="content" type="string"> 
 		<sec:authorize ifAnyGranted="ROLE_AEM,ROLE_ADMIN_${prop_site_id},ROLE_CRM">  
			<form:form commandName="crmTaskForm" action="crmTaskForm.jhtm" method="post" class="page-form">
				<div class="page-banner">
					<div class="breadcrumb">
						<div class="breadcrumb"><span><a href="${_contextpath}/admin/catalog/dashboard.jhtm">Home</a></span><i class="sli-arrow-right"></i><span><a href="${_contextpath}/admin/crm">CRM</a></span><i class="sli-arrow-right"></i><span><a href="${_contextpath}/admin/crm/crmTaskList.jhtm">Tasks</a></span><i class="sli-arrow-right"></i><span>Form</span></div>
					</div>
				</div>
				<div class="page-head">
					<div class="row justify-content-between">
						<div class="col title">
							${crmTaskForm.crmTask.title}
						</div>
						<div class="col actions">
							<div class="dropdown">
								<button type="button" class="btn-outline dropdown-toggle active btn btn-primary" data-toggle="dropdown">
		                             <i class="sli-settings"></i> Actions
		                         </button>
		                         <div class="dropdown-menu dropdown-menu-right">
									<c:if test="${!crmTaskForm.newCrmTask}">
										<sec:authorize ifAnyGranted="ROLE_AEM,ROLE_ADMIN_${prop_site_id},ROLE_CRM">
											<a class="dropdown-item" onClick="return confirm('Delete permanently?')"><spring:message code="delete"/></a>
										</sec:authorize>
									</c:if>
		                         </div>
							</div>
						</div>
					</div>
				</div>
				<div class="page-body">
					<div class="main-content">
					  	<div class="form-section">
					        <div class="content">
					            <ul class="nav nav-tabs" role="tablist">
					                <li class="nav-item">
					                    <a class="nav-link active" data-toggle="tab" role="tab" href="#tab_1">Task Information</a>
					                </li>
				                </ul>
				                <div class="tab-content">
		                   			<jsp:include page="taskInformationTab.jsp" />
								</div>
			                </div>
			                <div class="footer">
								<div class="form-row">
									<div class="col-12">
										<c:if test="${crmTaskForm.newCrmTask}">
											<c:if test="${!scheduleDone}" >
												<sec:authorize ifAnyGranted="ROLE_AEM,ROLE_ADMIN_${prop_site_id},ROLE_CRM">
													<a class="btn btn-default"><spring:message code="add"/></a>
												</sec:authorize> 
											</c:if>
											<c:if test="${scheduleDone}" >
												<input type="submit" name="__back" value="<fmt:message key="back" />" class="btn btn-default"/>
											</c:if>
										</c:if>
										<c:if test="${!crmTaskForm.newCrmTask}">
											<sec:authorize ifAnyGranted="ROLE_AEM,ROLE_ADMIN_${prop_site_id},ROLE_CRM">
												<a class="btn btn-default"><spring:message code="update"/></a>
												<a class="btn btn-default">Update and Follow Up</a>
											</sec:authorize>
										</c:if>
									</div>
								</div>
				            </div>
		                </div>
	                </div>
				</div>
		   		<input type="hidden" name="taskId" value="${crmTaskForm.crmTask.id}" />
				<form:hidden path="newCrmTask" />
			</form:form>
		</sec:authorize>
	</tiles:putAttribute>
	<tiles:putAttribute name="js">
		<script>
			$('#crmTask_created').datepicker();
			$('#crmTask_dueDate').datepicker();
			$('#crmTask_reminder').datepicker();
		</script>
	</tiles:putAttribute>
</tiles:insertDefinition>