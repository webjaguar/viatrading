<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="/WEB-INF/tlds/spring.tld" prefix="spring" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ taglib uri="http://www.springframework.org/security/tags" prefix="sec" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ taglib uri="/WEB-INF/tlds/custom.tld" prefix="wj" %>

<div id="tab_1" class="tab-pane active">
	<div class="line">
		<div class="form-row">
			<div class="col-label">
				<label class="col-form-label">
					<fmt:message key="crmAccountName" />
				</label>
			</div>
			<div class="col-lg-4">
				<label class="text"><c:out value="${crmTaskForm.crmTask.accountName}" /></label>
			</div>
		</div>
	</div>
	<div class="line">
		<div class="form-row">
			<div class="col-label">
				<label class="col-form-label">
					<fmt:message key="crmContactName" />
				</label>
			</div>
			<div class="col-lg-4">
				<label class="text"><c:out value="${crmTaskForm.crmTask.contactName}" /></label>
			</div>
		</div>
	</div>
	<div class="line">
		<div class="form-row">
			<div class="col-label">
				<label class="col-form-label">
					<fmt:message key="phone" />
				</label>
			</div>
			<div class="col-lg-4">
				<label class="text"><c:out value="${crmTaskForm.crmTask.contactPhone}" /></label>
			</div>
		</div>
	</div>
	<div class="line">
		<div class="form-row">
			<div class="col-label">
				<label class="col-form-label">
					<fmt:message key="email" />
				</label>
			</div>
			<div class="col-lg-4">
				<label class="text"><c:out value="${crmTaskForm.crmTask.contactEmail}" /></label>
			</div>
		</div>
	</div>        
	<div class="line">
		<div class="form-row">
			<div class="col-label">
				<label for="crmTask_title" class="col-form-label">  
					<fmt:message key="title" />
				</label>
			</div>
			<div class="col-lg-4">
				<form:input path="crmTask.title" class="form-control" maxlength="255" size="60" id="crmTask_title" htmlEscape="true"/> 
			</div>
		</div>
	</div>
	<div class="line">
		<div class="form-row">
			<div class="col-label">
				<label for="crmTask_type" class="col-form-label">   
					<span class="requiredField"><fmt:message key="type" /></span>
				</label>
			</div>
			<div class="col-lg-4"> 
				<form:select path="crmTask.type" class="custom-select" id="crmTask_type">
					<form:option value="" label="Please Select" />
					<c:forTokens items="${model.taskTypes}" delims="," var="type" >
						<form:option value="${type}" label="${type}"/>
					</c:forTokens>
				</form:select>
			</div>
		</div> 
	</div>
	<div class="line">
		<div class="form-row">
			<div class="col-label">
				<label for="crmTask_assignedToId" class="col-form-label">
					<span class="requiredField"><fmt:message key="assignedTo" /></span>
				</label>
			</div>
			<div class="col-lg-4">
				<form:select path="crmTask.assignedToId" class="custom-select" id="crmTask_assignedToId">
					<option value="0" ><fmt:message key="admin" /></option>
					<form:options items="${model.userList}" itemValue="id" itemLabel="name"/>
				</form:select>
			</div>
		</div>
	</div>
	<div class="line">
		<div class="form-row">
			<div class="col-label">
				<label for="crmTask_created" class="col-form-label">
					<fmt:message key="created" />
				</label>
			</div>
			<div class="col-lg-4">
				<div class="input-group">
					<span class="input-group-addon">
						<span class="sli-calendar"></span>
					</span>
					<form:input path="crmTask.created" class="form-control" id="crmTask_created"/>	 
				</div>
			</div>
		</div>
	</div>
	<div class="line">
		<div class="form-row">
			<div class="col-label">
				<label for="crmTask_dueDate" class="col-form-label">
					<fmt:message key="dueDate" />
				</label>
			</div>
			<div class="col-lg-4">
				<div class="input-group">
					<span class="input-group-addon">
						<span class="sli-calendar"></span>
					</span>
					<form:input path="crmTask.dueDate" class="form-control" id="crmTask_dueDate"/>
				</div>
			</div>
			<c:if test="${crmTaskForm.crmTask.dueDate != null and crmTaskForm.crmTask.type != null}">
				<div class="col-lg-4">
					<a class="btn btn-default btn-flat">Copy to my Calendar</a>
				</div>
			</c:if>
		</div>
	</div>
	<div class="line">
		<div class="form-row">
			<div class="col-label">
				<label for="crmTask_description" class="col-form-label"> 
					<fmt:message key="description" />
				</label>
			</div>
			<div class="col-lg-4">
				<form:textarea path="crmTask.description" id="crmTask_description" rows="8" class="form-control"/>
			</div>
		</div>
	</div>
	<div class="line">
		<div class="form-row">
			<div class="col-label">
				<label for="crmTask_priority" class="col-form-label"> 
					<fmt:message key="rank" />
				</label>
			</div>
			<div class="col-lg-4"> 
				<form:select path="crmTask.priority" class="custom-select" id="crmTask_priority">
					<form:option value="" label="Please Select" />
					<c:forTokens items="${model.taskRanks}" delims="," var="rank" >
						<form:option value="${rank}" label="${rank}"/>
					</c:forTokens>
				</form:select>
			</div>
		</div>
	</div>
	<div class="line">
		<div class="form-row">
			<div class="col-label">
				<label for="crmTask_status" class="col-form-label"> 
					<fmt:message key="status" />
				</label>
			</div>
			<div class="col-lg-4"> 
				<form:select path="crmTask.status" class="custom-select" id="crmTask_status">
					<form:option value="" label="Please Select"/>
					<form:option value="01"><fmt:message key="taskStatus_01" /></form:option>
					<form:option value="02"><fmt:message key="taskStatus_02" /></form:option>
					<form:option value="03"><fmt:message key="taskStatus_03" /></form:option>
					<form:option value="04"><fmt:message key="taskStatus_04" /></form:option>
					<form:option value="05"><fmt:message key="taskStatus_05" /></form:option>
					<form:option value="06"><fmt:message key="taskStatus_06" /></form:option>
				</form:select>
			</div>
		</div>
	</div>
	<div class="line">
		<div class="form-row">
			<div class="col-label">
				<label for="crmTask_action" class="col-form-label"> 
					<fmt:message key="action" />
				</label>
			</div>
			<div class="col-lg-4">
				<form:select path="crmTask.action" class="custom-select" id="crmTask_action">
					<form:option value="" label="Please Select" />
					<c:forTokens items="${model.taskActions}" delims="," var="action" >
						<form:option value="${action}" label="${action}"/>
					</c:forTokens>
				</form:select>
			</div>
		</div>
	</div>
	<div class="line">
		<div class="form-row">
			<div class="col-label">
				<label for="crmTask_dueDate" class="col-form-label">
					<fmt:message key="reminder" /> Date
				</label>
			</div>
			<div class="col-lg-4">
				<div class="input-group">
					<span class="input-group-addon">
						<span class="sli-calendar"></span>
					</span>
					<form:input path="crmTask.reminder" class="form-control" id="crmTask_reminder" /> 
				</div>
			</div>
		</div> 
	</div>
	<div class="line">   
		<div class="form-row">
			<div class="col-label">
				<label for="crmTask_reminderTime" class="col-form-label">
					<fmt:message key="reminder" /> Time
				</label>
			</div>
			<div class="col-lg-4">
				<div class="form-group">
					<div class="input-group">
						<span class="input-group-addon">
							<span class="sli-clock"></span>
						</span> 	
						<form:select path="crmTask.reminderTime" class="custom-select" id="crmTask_reminderTime"> 
							<form:option value="0">12:00 AM</form:option><form:option value="30">12:30 AM</form:option><form:option value="60">1:00 AM</form:option><form:option value="90">1:30 AM</form:option><form:option value="120">2:00 AM</form:option><form:option value="150">2:30 AM</form:option><form:option value="180">3:00 AM</form:option><form:option value="210">3:30 AM</form:option><form:option value="240">4:00 AM</form:option><form:option value="270">4:30 AM</form:option><form:option value="300">5:00 AM</form:option><form:option value="330">5:30 AM</form:option><form:option value="360">6:00 AM</form:option><form:option value="390">6:30 AM</form:option><form:option value="420">7:00 AM</form:option><form:option value="450">7:30 AM</form:option><form:option value="480">8:00 AM</form:option><form:option value="510">8:30 AM</form:option><form:option value="540">9:00 AM</form:option><form:option value="570">9:30 AM</form:option><form:option value="600">10:00 AM</form:option><form:option value="630">10:30 AM</form:option><form:option value="660">11:00 AM</form:option><form:option value="690">11:30 AM</form:option><form:option value="720">12:00 PM</form:option><form:option value="750">12:30 PM</form:option><form:option value="780">1:00 PM</form:option><form:option value="810">1:30 PM</form:option><form:option value="840">2:00 PM</form:option><form:option value="870">2:30 PM</form:option><form:option value="900">3:00 PM</form:option><form:option value="930">3:30 PM</form:option><form:option value="960">4:00 PM</form:option><form:option value="990">4:30 PM</form:option><form:option value="1020">5:00 PM</form:option><form:option value="1050">5:30 PM</form:option><form:option value="1080">6:00 PM</form:option><form:option value="1110">6:30 PM</form:option><form:option value="1140">7:00 PM</form:option><form:option value="1170">7:30 PM</form:option><form:option value="1200">8:00 PM</form:option><form:option value="1230">8:30 PM</form:option><form:option value="1260">9:00 PM</form:option><form:option value="1290">9:30 PM</form:option><form:option value="1320">10:00 PM</form:option><form:option value="1350">10:30 PM</form:option><form:option value="1380">11:00 PM</form:option><form:option value="1410">11:30 PM</form:option>
						</form:select>
					</div>
				</div>
				<small class="help-text"><fmt:message key='currentTime' />: <c:out value="${model.currentTime}"></c:out></small>	                                        
			</div>
		</div>
	</div>
</div>