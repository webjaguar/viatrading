<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/security/tags" prefix="sec" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ taglib uri="/WEB-INF/tlds/custom.tld" prefix="wj" %>
 
<c:set var="now" value="<%=new java.util.Date()%>" />        
<tiles:insertDefinition name="admin.responsive-template.crm.task" flush="true">
	<tiles:putAttribute name="css">
		<style>
			#grid .field-title{
				width: 150px;
			}
			#grid .field-crmAccount{
				width: 150px;
			}
			#grid .field-crmContact{
				width: 150px;
			}
			#grid .field-assignedTo{
				width: 150px;
			}
			#grid .field-reminder{
				text-align: center;
			}
		</style>
	</tiles:putAttribute>
	<tiles:putAttribute name="content" type="string">
	    <sec:authorize ifAnyGranted="ROLE_AEM,ROLE_ADMIN_${prop_site_id},ROLE_CRM"> 
		    <form action="crmTaskList.jhtm" method="post" id="list" name="list_form">
				<div class="page-banner">
					<div class="breadcrumb"><span><a href="${_contextpath}/admin/catalog/dashboard.jhtm">Home</a></span><i class="sli-arrow-right"></i><span><a href="${_contextpath}/admin/crm">CRM</a></span><i class="sli-arrow-right"></i><span>Tasks</span></div>
				</div>
				<div class="page-head">
					<div class="row justify-content-between">
						<div class="col-sm-8 title">
							<h3>Tasks</h3>
						</div>
						<div class="col-sm-4 actions">
							<div class="dropdown dropdown-actions">
								<button type="button" class="btn-outline dropdown-toggle active btn btn-primary" data-toggle="dropdown">
		                             <i class="sli-settings"></i> Actions
		                         </button>
		                         <div class="dropdown-menu dropdown-menu-right">
			                         <sec:authorize ifAnyGranted="ROLE_AEM,ROLE_ADMIN_${prop_site_id},ROLE_CRM">
			                         	<a class="dropdown-item" onClick="return confirm('Delete permanently?')"><fmt:message key="delete"/></a>
							  	  	</sec:authorize>
		                         </div>
		                    </div>
		                    <div class="dropdown dropdown-setting">
								<button type="button" class="btn-outline dropdown-toggle active btn btn-primary" data-toggle="dropdown">
		                             <i class="sli-screen-desktop"></i> Display
		                         </button>	                         
								<div class="dropdown-menu dropdown-menu-right">
									<div class="form-group">
				                    		<div class="option-header">Rows</div>
										<select class="custom-select form-control" name="size" onchange="document.getElementById('page').value=1;submit();">
											<c:forTokens items="10,25,50,100,200,500" delims="," var="current">
									  	  		<option value="${current}" <c:if test="${current == crmTaskSearch.pageSize}">selected</c:if>>${current}</option>
								  			</c:forTokens>
										</select>
									</div>
									<div class="form-group">
				                    		<div class="option-header">Columns</div>
				                    		<div id="column-hider"></div>
									</div>
								</div>
		                    </div>
						</div>
					</div>
				</div>
				<div class="page-body">
					<div class="grid-stage">
						<c:if test="${model.count > 0}">
							<div class="page-stat">
								<fmt:message key="showing">
									<fmt:param value="${crmTaskSearch.offset+1}"/>
									<fmt:param value="${model.pageEnd}"/>
									<fmt:param value="${model.count}"/>
								  </fmt:message>
							</div> 
						</c:if>
					    <table id="grid"></table>
					    <div class="footer">
							<c:if test="${model.count > 0}">
				                <div class="float-right">
									<c:if test="${crmTaskSearch.page == 1}">
								 		<span class="previous"><i class="sli-arrow-left" style="font-size: 0.7rem"></i> Previous</span>
									</c:if>
									<c:if test="${crmTaskSearch.page != 1}">
										<a href="<c:url value="crmTaskList.jhtm"><c:param name="page" value="${crmTaskSearch.page-1}"/><c:param name="size" value="${crmTaskSearch.pageSize}"/></c:url>">
											<span class="previous"><i class="sli-arrow-left" style="font-size: 0.7rem"></i> Previous</span>
										</a>
									</c:if>	
									<span class="status"><c:out value="${crmTaskSearch.page}" /> of <c:out value="${model.pageCount}" /></span>
									<c:if test="${crmTaskSearch.page == model.pageCount}">
	  									<span class="next">Next <i class="sli-arrow-right" style="font-size: 0.7rem"></i></span>
	 								</c:if>
									<c:if test="${crmTaskSearch.page != model.pageCount}">
	  									<a href="<c:url value="crmTaskList.jhtm"><c:param name="page" value="${crmTaskSearch.page+1}"/><c:param name="size" value="${crmTaskSearch.pageSize}"/></c:url>">
											<span class="next">Next <i class="sli-arrow-right" style="font-size: 0.7rem"></i></span>
										</a>
	 								</c:if>
								</div>
							</c:if>
						</div>
					</div>
				</div>
			    <input type="hidden" id="page" name="page" value="${crmTaskSearch.page}"/>
			    	<input type="hidden" id="sort" name="sort" value="${crmTaskSearch.sort}" /> 
		    </form>
	    </sec:authorize>
	</tiles:putAttribute>
	<tiles:putAttribute name="js">
		<!-- DGRID TABLE SCRIPT MUST BE AT BOTTOM OF THE PAGE -->
		<script src="${_contextpath}/admin-responsive-static/assets/plugins/dojo/dojo.js" data-dojo-config="async: true"></script>
		<script>
			//Table Data Sanitization
			var data = [];
			<c:forEach items="${model.crmTaskList}" var="task" varStatus="status">
				data.push({
					id: ${task.id},
					title: "${wj:escapeJS(task.title)}",
					accountName: "${wj:escapeJS(task.accountName)}",
					contactId: "${task.contactId}",
					contactName: "${wj:escapeJS(task.contactName)}",
					created: '<fmt:formatDate type="date" timeStyle="default" value="${task.created}"/>',
					dueDate: '<fmt:formatDate type="date" timeStyle="default" value="${task.dueDate}"/>',
					assignedTo: '${wj:escapeJS(task.assignedTo)}',
					status: '<fmt:message key="taskStatus_${task.status}"/>',
					priority: '${task.priority}',
					<c:if test="${task.reminder > now}">
						reminder: '<fmt:formatDate type="both" timeStyle="full" value="${task.reminder}"/>',
					</c:if>
				});
			</c:forEach>
			
			require(["dgrid/List", "dgrid/OnDemandGrid", "dgrid/Selection", 'dgrid/Selector', "dgrid/Keyboard", "dgrid/Editor", "dijit/form/CheckBox", 
					"dijit/form/TimeTextBox", "dijit/form/Select", "dojo/data/ObjectStore", "dijit/form/FilteringSelect",
					'dijit/form/DropDownButton', 'dijit/DropDownMenu', 'dijit/MenuItem',
					"dojo/_base/declare", "dojo/_base/lang", "dojo/store/Memory", "dojo/store/Observable", "dgrid/test/data/createSyncStore", 
					'dgrid/extensions/ColumnHider', 'dgrid/extensions/ColumnReorder', 'dgrid/extensions/ColumnResizer', 'dojo/domReady!',
				], function(List, OnDemandGrid, Selection, Selector, Keyboard, Editor, CheckBox, TimeTextBox, Select, ObjectStore, FilteringSelect,
						DropDownButton, DropDownMenu, MenuItem,
						declare, lang, LegacyMemory, LegacyObservable, createSyncStore, ColumnHider, ColumnReorder, ColumnResizer){

				//Column Definitions
				var columns = {
			        batch: {
						selector: 'checkbox',
						unhidable: true
					},
			        <sec:authorize ifAnyGranted="ROLE_AEM,ROLE_ADMIN_${prop_site_id},ROLE_CRM">
						actions: {
							label: 'Actions',
							renderCell: function(object, data, td, options){
								var div = document.createElement("div");
								var menu = new DropDownMenu({
									style: "display: none;"
								});
								
								var menuItem;
								
								menuItem = new MenuItem({
							        label: '<fmt:message key="edit" />',
							        onClick: function(){
							        		location.href = 'crmTaskForm.jhtm?taskId='+object.id;
							        	}
							    });
							    menu.addChild(menuItem);
								
							    menuItem = new MenuItem({
							        label: '<fmt:message key="followUp" />',
							        onClick: function(){
							        		location.href = 'crmTaskForm.jhtm?taskId='+object.id+'&fupt=followup';
							        	}
							    });
							    menu.addChild(menuItem);
							    
							    menu.startup();
 
							    var dropdown = new DropDownButton({
							        iconClass: "sli-settings",
							        dropDown: menu,
							    },'button');
							    
							    div.appendChild(dropdown.domNode);
			                    return div;
							},
							sortable: false
						},
					</sec:authorize>
					title: {
						label: '<fmt:message key="title" />',
						get: function(object){
							return object.title;
						},
						sortable: false,
					},
					crmAccount: {
						label: '<fmt:message key="crmAccount" />',
						get: function(object){
							return object.accountName;
						},
						sortable: false,
					},
					crmContact: {
						label: '<fmt:message key="crmContact" />',
						renderCell: function(object){
							var link = document.createElement("a");
							link.className = 'plain';
							link.text = object.contactName;
							link.href = 'crmContactForm.jhtm?id='+object.contactId;
							return link;
						},
						sortable: false,
					},
					created: {
						label: '<fmt:message key="created" />',
						get: function(object){
							return object.created;
						},
						sortable: false,
					},
					dueDate: {
						label: '<fmt:message key="dueDate" />',
						get: function(object){
							return object.dueDate;
						},
						sortable: false,
					},
					assignedTo: {
						label: '<fmt:message key="assignedTo" />',
						get: function(object){
							return object.assignedTo;
						},
						sortable: false,
					},
					status: {
						label: '<fmt:message key="status" />',
						get: function(object){
							return object.status;
						},
						sortable: false,
					},
					priority: {
						label: '<fmt:message key="priority" />',
						get: function(object){
							return object.priority;
						},
						sortable: false,
					},
					reminder: {
						label: '<fmt:message key="reminder" />',
						renderCell: function(object){
							var div = document.createElement('div');

							if(object.reminder){
								var i = document.createElement("i");
								i.className = 'ion-icon ion-ios-stopwatch-outline';
								i.setAttribute('data-toggle','tooltip');
								i.setAttribute('data-placement','left');
								i.setAttribute('data-original-title',object.reminder);
								div.appendChild(i);
							}
							
							return div;
						},
						sortable: false,
					},
				};
				
				var grid = new (declare([OnDemandGrid, Selection, Selector, Keyboard, Editor, ColumnHider, ColumnReorder, ColumnResizer]))({
					collection: createSyncStore({ data: data }),
					selectionMode: "none",
					allowSelectAll: true,
					columns: columns,
				},'grid');
				
				grid.on('dgrid-refresh-complete', function(event) {
					$('#grid-hider-menu').appendTo('#column-hider');
				});
				
				grid.startup();
			});
		</script>
	</tiles:putAttribute>
</tiles:insertDefinition>
 