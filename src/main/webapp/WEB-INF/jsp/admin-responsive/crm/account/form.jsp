<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="/WEB-INF/tlds/spring.tld" prefix="spring" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ taglib uri="http://www.springframework.org/security/tags" prefix="sec" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ taglib uri="/WEB-INF/tlds/custom.tld" prefix="wj" %>

<tiles:insertDefinition name="admin.responsive-template.crm.accountForm" flush="true">
	<tiles:putAttribute name="css"> 
	</tiles:putAttribute>
	<tiles:putAttribute name="content" type="string"> 
 		<sec:authorize ifAnyGranted="ROLE_AEM,ROLE_ADMIN_${prop_site_id},ROLE_CRM">   
			<form:form commandName="crmAccountForm" action="crmAccountForm.jhtm" method="post" class="page-form">
				<div class="page-banner">
					<div class="breadcrumb"><span><a href="${_contextpath}/admin/catalog/dashboard.jhtm">Home</a></span><i class="sli-arrow-right"></i><span><a href="${_contextpath}/admin/crm">CRM</a></span><i class="sli-arrow-right"></i><span><a href="${_contextpath}/admin/crm/crmAccountList.jhtm">Accounts</a></span><i class="sli-arrow-right"></i><span>Form</span></div></div>
				<div class="page-head">
					<div class="row justify-content-between">
						<div class="col title">
							${crmAccountForm.crmAccount.accountName}
						</div>
						<div class="col actions">
							<div class="dropdown dropdown-actions">
								<button type="button" class="btn-outline dropdown-toggle active btn btn-primary" data-toggle="dropdown">
		                             <i class="sli-settings"></i> Actions
		                         </button>
		                         <div class="dropdown-menu dropdown-menu-right">
							      	<c:if test="${!crmAccountForm.newCrmAccount}">
								        	<c:if test="${crmAccountForm.crmAccount.id != 1}">
								        		<sec:authorize ifAnyGranted="ROLE_AEM,ROLE_ADMIN_${prop_site_id},ROLE_CRM">
												<a class="dropdown-item">Delete</a>
								        		</sec:authorize>
										</c:if>
									</c:if>
		                         </div>
							</div>
						</div>
					</div>
				</div>
				<div class="page-body">
					<div class="main-content">
						<div class="form-section">
							<div class="content">
							 	<ul class="nav nav-tabs" role="tablist">
					                <li class="nav-item">
					                    <a class="nav-link active" data-toggle="tab" role="tab" href="#tab_1">Account Information</a>
					                </li>
					                <li class="nav-item">
					                    <a class="nav-link" data-toggle="tab" role="tab" href="#tab_2">Additional Information</a>
					                </li>
					                <li class="nav-item">
					                		<div class="dropdown">
						                    <a class="nav-link" data-toggle="dropdown">Advanced <i class="fa fa-caret-down"></i></a>
											<div class="dropdown-menu dropdown-menu-right">
												<a class="dropdown-item" data-toggle="tab" role="tab" href="#tab_3">Address Information</a>
											</div>
										</div>
					                </li>
				                </ul>
				                <div class="tab-content">
									<jsp:include page="accountInformationTab.jsp" />
									<jsp:include page="additionalInformationTab.jsp" />
				                    <jsp:include page="addressInformationTab.jsp" />
				                </div>
			                </div>
			                <div class="footer">
								<div class="form-row">
									<div class="col-12">
										<c:if test="${crmAccountForm.newCrmAccount}">
								    			<sec:authorize ifAnyGranted="ROLE_AEM,ROLE_ADMIN_${prop_site_id},ROLE_CRM">
												<a class="dropdown-item">Add</a>
									        	</sec:authorize>  
								      	</c:if>
										<sec:authorize ifAnyGranted="ROLE_AEM,ROLE_ADMIN_${prop_site_id},ROLE_CRM">
											<a class="btn btn-default">Update</a>
							        		</sec:authorize>
										<button class="btn btn-default">Cancel</button>
									</div>
								</div>
				            </div>
		                </div>
	                </div>
                </div>
				<form:hidden path="newCrmAccount" />
			</form:form>
		</sec:authorize>
	</tiles:putAttribute>
	<tiles:putAttribute name="js"> 
	</tiles:putAttribute>
</tiles:insertDefinition>