<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ taglib uri="http://www.springframework.org/security/tags" prefix="sec" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ taglib uri="/WEB-INF/tlds/spring.tld" prefix="spring" %>
<%@ taglib uri="/WEB-INF/tlds/custom.tld" prefix="wj" %>

<tiles:insertDefinition name="admin.responsive-template.crm.account" flush="true">  
	<tiles:putAttribute name="css">
		<style>
			#grid .field-accountName{
			    width: 250px;
			}
			#grid .field-billingCity{
			    width: 120px;
			}
		</style>
	</tiles:putAttribute>
	<tiles:putAttribute name="content" type="string">
		<sec:authorize ifAnyGranted="ROLE_AEM,ROLE_ADMIN_${prop_site_id},ROLE_CRM">   
			<form action="crmAccountList.jhtm" method="post" id="list" name="list_form">
				<div class="page-banner">
					<div class="breadcrumb"><span><a href="${_contextpath}/admin/catalog/dashboard.jhtm">Home</a></span><i class="sli-arrow-right"></i><span><a href="${_contextpath}/admin/crm">CRM</a></span><i class="sli-arrow-right"></i><span>Accounts</span></div>
				</div>
				<div class="page-head">
					<div class="row justify-content-between">
						<div class="col-sm-8 title">
							<h3>Accounts</h3>
						</div>
						<div class="col-sm-4 actions">
							<div class="dropdown dropdown-actions">
								<button type="button" class="btn-outline dropdown-toggle active btn btn-primary" data-toggle="dropdown">
		                             <i class="sli-settings"></i> Actions
		                         </button>
		                         <div class="dropdown-menu dropdown-menu-right">
									<sec:authorize ifAnyGranted="ROLE_AEM,ROLE_ADMIN_${prop_site_id},ROLE_PRODUCT_FIELD_EDIT">
										<sec:authorize ifAnyGranted="ROLE_AEM,ROLE_ADMIN_${prop_site_id},ROLE_CRM">
											<a class="dropdown-item"><fmt:message key="add"/></a>
					  	  				</sec:authorize>  
									</sec:authorize>
		                         </div>
		                    </div>
		                    <div class="dropdown dropdown-setting">
								<button type="button" class="btn-outline dropdown-toggle active btn btn-primary" data-toggle="dropdown">
		                             <i class="sli-screen-desktop"></i> Display
		                         </button>	                         
								<div class="dropdown-menu dropdown-menu-right">
									<div class="form-group">
				                    		<div class="option-header">Rows</div>
										<select class="custom-select" name="size" onchange="document.getElementById('page').value=1;submit();">
											<c:forTokens items="10,25,50,100,200,500" delims="," var="current">
									  	  		<option value="${current}" <c:if test="${current == crmAccountSearch.pageSize}">selected</c:if>>${current}</option>
								  			</c:forTokens>
										</select>
									</div>
									<div class="form-group">
				                    		<div class="option-header">Columns</div>
				                    		<div id="column-hider"></div>
									</div>
								</div>
		                    </div>
						</div>
					</div>
				</div>
				<div class="page-body">
					<div class="grid-stage">
						<c:if test="${model.count > 0}">
							<div class="page-stat">
								<fmt:message key="showing">
									<fmt:param value="${crmAccountSearch.offset+1}"/>
									<fmt:param value="${model.pageEnd}"/>
									<fmt:param value="${model.count}"/>
								</fmt:message>
							</div> 
						</c:if>
					    <table id="grid"></table>
					    <div class="footer">
			                <div class="float-right">
								<c:if test="${crmAccountSearch.page == 1}">
							 		<span class="previous"><i class="sli-arrow-left" style="font-size: 0.7rem"></i> Previous</span>
								</c:if>
								<c:if test="${crmAccountSearch.page != 1}">
									<a href="<c:url value="crmAccountList.jhtm"><c:param name="page" value="${crmAccountSearch.page-1}"/></c:url>">
										<span class="previous"><i class="sli-arrow-left" style="font-size: 0.7rem"></i> Previous</span>
									</a>
								</c:if>	
								<span class="status"><c:out value="${crmAccountSearch.page}" /> of <c:out value="${model.pageCount}" /></span>
								<c:if test="${crmAccountSearch.page == model.pageCount}">
  									<span class="next">Next <i class="sli-arrow-right" style="font-size: 0.7rem"></i></span>
 								</c:if>
								<c:if test="${crmAccountSearch.page != model.pageCount}">
  									<a href="<c:url value="crmAccountList.jhtm"><c:param name="page" value="${crmAccountSearch.page+1}"/></c:url>">
										<span class="next">Next <i class="sli-arrow-right" style="font-size: 0.7rem"></i></span>
									</a>
 								</c:if>
							</div>
						</div>
					</div>
				</div>
				<input type="hidden" id="page" name="page" value="${crmAccountSearch.page}"/>
			    <input type="hidden" id="sort" name="sort" value="${crmAccountSearch.sort}" />
		    </form>
		</sec:authorize>
	</tiles:putAttribute>
	<tiles:putAttribute name="js">
		<!-- DGRID TABLE SCRIPT MUST BE AT BOTTOM OF THE PAGE -->
		<script src="${_contextpath}/admin-responsive-static/assets/plugins/dojo/dojo.js" data-dojo-config="async: true"></script>
		<script>
			//Table Data Sanitization
			var data = [];
			<c:forEach items="${model.crmAccountList}" var="account" varStatus="status">
				data.push({
					id: ${account.id},
					accountName: '${wj:escapeJS(account.accountName)}',
					rating: '${account.rating}',
					billingCity: '${wj:escapeJS(account.billingCity)}',
					phone: '${wj:escapeJS(account.phone)}',
					accountOwner: '${wj:escapeJS(account.accountOwner)}',
					accountType: '${wj:escapeJS(account.accountType)}',
				});
			</c:forEach>
			
			require(["dgrid/List", "dgrid/OnDemandGrid", "dgrid/Selection", "dgrid/Keyboard", "dgrid/Editor", "dijit/form/CheckBox", 
					"dijit/form/TimeTextBox", "dijit/form/Select", "dojo/data/ObjectStore", "dijit/form/FilteringSelect",
					'dijit/form/DropDownButton', 'dijit/DropDownMenu', 'dijit/MenuItem',
					"dojo/_base/declare", "dojo/_base/lang", "dojo/store/Memory", "dojo/store/Observable", "dgrid/test/data/createSyncStore", 
					'dgrid/extensions/ColumnHider', 'dgrid/extensions/ColumnReorder', 'dgrid/extensions/ColumnResizer',
				], function(List, OnDemandGrid, Selection, Keyboard, Editor, CheckBox, TimeTextBox, Select, ObjectStore, FilteringSelect,
						DropDownButton, DropDownMenu, MenuItem,
						declare, lang, LegacyMemory, LegacyObservable, createSyncStore, ColumnHider, ColumnReorder, ColumnResizer){
	
				//Column Definitions
				var columns = {
			        <sec:authorize ifAnyGranted="ROLE_AEM,ROLE_ADMIN_${prop_site_id},ROLE_CRM">
						actions: {
							label: 'Actions',
							renderCell: function(object, data, td, options){
								var div = document.createElement("div");
								var menu = new DropDownMenu({
									style: "display: none;"
								});
								
								var menuItem;
								
								menuItem = new MenuItem({
							        label: '<fmt:message key="edit" />',
							        onClick: function(){
							        		location.href = 'crmAccountForm.jhtm?id='+object.id;
							        	}
							    });
							    menu.addChild(menuItem);
								
							    menuItem = new MenuItem({
							        label: '<fmt:message key="addContact" />',
							        onClick: function(){
							        		location.href = 'crmContactForm.jhtm?accountId='+object.id;
							        	}
							    });
							    menu.addChild(menuItem);
							    
							    menuItem = new MenuItem({
							        label: '<fmt:message key="viewContact" />',
							        onClick: function(){
							        		location.href = 'crmContactList.jhtm?crmAccountId='+object.id;
							        	}
							    });
							    menu.addChild(menuItem);
							    
							    menu.startup();
	
							    var dropdown = new DropDownButton({
							        iconClass: "sli-settings",
							        dropDown: menu,
							    },'button');
							    
							    div.appendChild(dropdown.domNode);
			                    return div;
							},
							sortable: false
						},
					</sec:authorize>
					accountName: {
						label: '<fmt:message key="crmAccountName" />',
						renderCell: function(object){
							var link = document.createElement("a");
							link.className = "plain";
							link.text = object.accountName;
							link.href = "crmAccountForm.jhtm?id=" + object.id;
							return link;
						},
						sortable: false,
					},
					rating: {
						label: '<fmt:message key="rating" />',
						get: function(object){
							return object.rating;
						},
						sortable: false,
					},
					billingCity: {
						label: '<fmt:message key="billingCity" />',
						get: function(object){
							return object.billingCity;
						},
						sortable: false,
					},
					phone: {
						label: '<fmt:message key="phone" />',
						get: function(object){
							return object.phone;
						},
						sortable: false,
					},
					accountOwner: {
						label: '<fmt:message key="owner" />',
						get: function(object){
							return object.accountOwner;
						},
						sortable: false,
					},
					accountType:{
						label: '<fmt:message key="type" />',
						get: function(object){
							return object.accountOwner;
						},
						sortable: false,
					}
				};
				
				var grid = new (declare([OnDemandGrid, Selection, Keyboard, Editor, ColumnHider, ColumnReorder, ColumnResizer]))({
					collection: createSyncStore({ data: data }),
					columns: columns,
					selectionMode: "none",
				},'grid');
				
				grid.on('dgrid-refresh-complete', function(event) {
					$('#grid-hider-menu').appendTo('#column-hider');
				});
				
				grid.startup();
			});
		</script>
	</tiles:putAttribute>
</tiles:insertDefinition>