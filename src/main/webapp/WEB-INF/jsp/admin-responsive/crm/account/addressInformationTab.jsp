<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="/WEB-INF/tlds/spring.tld" prefix="spring" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ taglib uri="http://www.springframework.org/security/tags" prefix="sec" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ taglib uri="/WEB-INF/tlds/custom.tld" prefix="wj" %>

<div id="tab_3" class="tab-pane">
	<div class="row">
		<div class="col-lg-6">
			<div class="line">
	             <div class="form-row">
	             	<div class="col-label">
		                 <label for="crmAccount_billingStreet" class="col-form-label">
		                     <fmt:message key="billingStreet" />
		                 </label>
	                 </div>
	                 <div class="col-lg-8">
	                 	<form:textarea path="crmAccount.billingStreet" class="form-control" id="crmAccount_billingStreet" rows="5" /> 
	                 </div>
	             </div>
             </div>
			<div class="line">
				<div class="form-row">
					<div class="col-label">
						<label for="crmAccount_billingCity" class="col-form-label">
							<fmt:message key="billingCity" />
						</label>
					</div>
					<div class="col-lg-8">
						<form:input path="crmAccount.billingCity" class="form-control" maxlength="255" id="crmAccount_billingCity"/>
					</div>
				</div>
			</div>
			<div class="line">
				<div class="form-row">
					<div class="col-label">
						<label for="crmAccount_billingStateProvince" class="col-form-label">
							<fmt:message key="billingState" />
						</label>
					</div>
					<div class="col-lg-8">
						<form:input path="crmAccount.billingStateProvince" maxlength="255" class="form-control" id="crmAccount_billingStateProvince"/> 
					</div>
				</div>
			</div>
			<div class="line">
				<div class="form-row">
					<div class="col-label">
						<label for="crmAccount_billingZip" class="col-form-label">
							<fmt:message key="billingZip" />
						</label>
					</div>
					<div class="col-lg-8">
						<form:input path="crmAccount.billingZip" maxlength="255" class="form-control" id="crmAccount_billingZip"/> 
					</div>
				</div>
			</div>
			<div class="line">
				<div class="form-row">
					<div class="col-label">
						<label for="crmAccount_billingCountry" class="col-form-label">
							<fmt:message key="billingCountry" />
						</label>
					</div>
					<div class="col-lg-8">
						<form:select id="country" path="crmAccount.billingCountry" class="custom-select">
							<form:option value="" label="Please Select"/>
							<form:options items="${countries}" itemValue="code" itemLabel="name"/>
						</form:select>
					</div>
				</div>
			</div>
		</div>
		<div class="col-lg-6">
			<div class="line">
				<div class="form-row">
					<div class="col-label">
						<label for="crmAccount_shippingStreet" class="col-form-label">
							<fmt:message key="shippingStreet" />
						</label>
					</div>
					<div class="col-lg-8">
						<form:textarea path="crmAccount.shippingStreet" class="form-control" id="crmAccount_shippingStreet" rows="5"/> 
					</div>
				</div>
			</div>
			<div class="line">
				<div class="form-row">
					<div class=col-label>
						<label for="crmAccount_shippingCity" class="col-form-label">
							<fmt:message key="shippingCity" />
						</label>
					</div>
					<div class="col-lg-8">
						<input type="text" class="form-control" id="crmAccount_shippingCity" name="crmAccount.shippingCity">
					</div>
				</div>
			</div>
			<div class="line">
				<div class="form-row">
					<div class="col-label">
						<label for="crmAccount_shippingStateProvince" class="col-form-label">
							<fmt:message key="shippingState" />
						</label>
					</div>
					<div class="col-lg-8">
						<form:input path="crmAccount.shippingStateProvince" class="form-control" id="crmAccount_shippingStateProvince" maxlength="255"/> 
					</div>
				</div>
			</div>
			<div class="line">                
				<div class="form-row">
					<div class="col-label">
						<label for="crmAccount_shippingZip" class="col-form-label">
							<fmt:message key="shippingZip" />
						</label>
					</div>
					<div class="col-lg-8">
						<form:input path="crmAccount.shippingZip" class="form-control" id="crmAccount_shippingZip" maxlength="255"/> 
					</div>
				</div>
			</div>
			<div class="line">
				<div class="form-row">
					<div class="col-label">
						<label for="country" class="col-form-label">
							<fmt:message key="shippingCountry" />
						</label>
					</div>
					<div class="col-lg-8">
						<form:select id="country" path="crmAccount.shippingCountry" class="custom-select">
							<form:option value="" label="Please Select"/>
							<form:options items="${countries}" itemValue="code" itemLabel="name"/>
						</form:select>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>