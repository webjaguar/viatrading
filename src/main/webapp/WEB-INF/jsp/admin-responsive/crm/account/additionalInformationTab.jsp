<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="/WEB-INF/tlds/spring.tld" prefix="spring" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ taglib uri="http://www.springframework.org/security/tags" prefix="sec" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ taglib uri="/WEB-INF/tlds/custom.tld" prefix="wj" %>

<div id="tab_2" class="tab-pane">
	<div class="line">
		<div class="form-row">
			<div class="col-label">
				<label for="crmAccount_description" class="col-form-label">
					<fmt:message key="description" />
				</label>		                                
			</div>
			<div class="col-lg-4">
				<form:textarea path="crmAccount.description" id="crmAccount_description" rows="8" class="form-control"/> 
			</div>
		</div>
	</div>              
	<div class="line">          
		<div class="form-row">
			<div class="col-label">
				<label for="crmAccount_industry" class="col-form-label">
					<span class="fa fa-question-circle" data-toggle="tooltip" title="Select the type of industry for this account. To manage the available types of industry goto: SiteInfo-->Site Configuration-->CRM Configuration."></span>
					<fmt:message key="industry" />
				</label>
			</div>				                                
			<div class="col-lg-4">
				<form:select path="crmAccount.industry" class="custom-select" id="crmAccount_industry">
					<form:option value="" label=""></form:option>
					<c:forTokens items="${industry}" delims="," var="ind" >
						<form:option value="${ind}">${ind}</form:option>
					</c:forTokens>
				</form:select>
			</div>
		</div>
 	</div>
 	<div class="line">
		<div class="form-row">
			<div class="col-label">
				<label for="crmAccount_employees" class="col-form-label">
					<span class="fa fa-question-circle" data-toggle="tooltip" title="Enter the number of employees in the company."></span>
					<fmt:message key="employees" />
				</label>
			</div>
			<div class="col-lg-4"> 
				<form:input path="crmAccount.employees" maxlength="255" class="form-control" id="crmAccount_employees"/>
			</div>
		</div>
	</div>
	<div class="line">
		<div class="form-row">
			<div class="col-label">
				<label for="crmAccount_annualRevenue" class="col-form-label">
					<fmt:message key="annualRevenue" />
				</label>
			</div>
			<div class="col-lg-4">
				<form:input path="crmAccount.annualRevenue" class="form-control" maxlength="255" id="crmAccount_annualRevenue"/>
			</div>
		</div>
	</div>
</div>