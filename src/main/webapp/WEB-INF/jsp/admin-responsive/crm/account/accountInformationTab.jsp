<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="/WEB-INF/tlds/spring.tld" prefix="spring" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ taglib uri="http://www.springframework.org/security/tags" prefix="sec" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ taglib uri="/WEB-INF/tlds/custom.tld" prefix="wj" %>

<div id="tab_1" class="tab-pane active"> 
	<div class="line">
		<div class="form-row">
			<div class="col-label">
				<label for="numEmails" class="col-form-label">  
					<fmt:message key="crmAccountName" />
				</label>
			</div>
			<div class="col-lg-4">
				<c:choose>
					<c:when test="${crmAccountForm.crmAccount.id == 1}">
						<form:input path="crmAccount.accountName" id="formAccountName" class="form-control" maxlength="255" htmlEscape="true" disabled="true" />
					</c:when>
					<c:otherwise>
						<form:input path="crmAccount.accountName" id="formAccountName" class="form-control" maxlength="255" htmlEscape="true"  />
					</c:otherwise>
				</c:choose>
    			</div>
		</div>
	</div>
	<div class="line">
		<div class="form-row">
			<div class="col-label">
				<label class="col-form-label">
					<fmt:message key="crmAccountOwner" />
				</label>
			</div>
			<div class="col-lg-4">
				<p class="form-control-static"><c:out value="${crmAccountForm.crmAccount.accountOwner}" /></p>
			</div>
		</div>
	</div>
	<div class="line">
		<div class="form-row">
			<div class="col-label">
				<label for="crmAccount_accessUserId" class="col-form-label">
					<span class="fa fa-question-circle" data-toggle="tooltip" title="" data-original-title="Assign this account to a user. To manage users go to: More-->Access Privilege."></span>
					<fmt:message key="assignedTo" />
				</label>
			</div>
			<div class="col-lg-4">
				<form:select path="crmAccount.accessUserId" class="custom-select" id="crmAccount_accessUserId">
					<form:option value="" label="Select"></form:option>
						<c:forEach items="${accessUsers}" var="accessUser">
							<form:option value="${accessUser.id}" label="${accessUser.username}"></form:option>
    		  				</c:forEach>
				</form:select>
			</div>
		</div>
	</div>
	<div class="line">
		<div class="form-row">
			<div class="col-label">
				<label for="crmAccount_accountNumber" class="col-form-label">
					<fmt:message key="crmAccountNumber" />
				</label>
			</div>	
			<div class="col-lg-4">
				<form:input path="crmAccount.accountNumber" id="crmAccount_accountNumber" class="form-control" maxlength="255"/>
			</div>
		</div>
	</div>
	<div class="line">
		<div class="form-row">
			<div class="col-label">
				<label for="crmAccount_accountType" class="col-form-label">
					<span class="fa fa-question-circle" data-toggle="tooltip" title="Select the type of account for this account. To manage the available types of account goto: SiteInfo-->Site Configuration-->CRM Configuration."></span>
					<fmt:message key="crmAccountType" />
				</label>
			</div>
			<div class="col-lg-4">
				<form:select path="crmAccount.accountType" class="custom-select" id="crmAccount_accountType">
					<form:option value="" label=""></form:option>
					<c:forTokens items="${types}" delims="," var="type" >
						<form:option value="${type}">${type}</form:option>
					</c:forTokens>
				</form:select>
			</div>
		</div>
	</div>
	<div class="line">
		<div class="form-row">
			<div class="col-label">
				<label for=crmAccount_rating class="col-form-label"> 
					<fmt:message key="crmRating" />
				</label>
			</div>
			<div class="col-lg-4">
				<form:select path="crmAccount.rating" id="crmAccount_rating" class="custom-select">
					<form:option value="" label=""></form:option>
					<c:forTokens items="${rating}" delims="," var="rate" >
						<form:option value="${rate}">${rate}</form:option>
		    			</c:forTokens>
				</form:select>
			</div>
		</div> 
	</div>
	<div class="line">
		<div class="form-row">
			<div class="col-label">
				<label for="crmAccount_phone" class="col-form-label">
					<fmt:message key="phone" />
				</label>
			</div>
			<div class="col-lg-4">
				<form:input path="crmAccount.phone" class="form-control" id="crmAccount_phone" maxlength="255"/> 
			</div>
		</div>
	</div>
	<div class="line">
		<div class="form-row">
			<div class="col-label">
				<label for="crmAccount_fax" class="col-form-label">
					<fmt:message key="fax" />
				</label>
			</div>
			<div class="col-lg-4">
				<form:input path="crmAccount.fax" maxlength="255" class="form-control" id="crmAccount_fax" /> 
			</div>
		</div>
	</div>
	<div class="line">
		<div class="form-row">
			<div class="col-label">
				<label for="crmAccount_website" class="col-form-label">
					<fmt:message key="website" />
				</label>
			</div>
			<div class="col-lg-4">
				<form:input path="crmAccount.website" maxlength="255" class="form-control" id="crmAccount_website"/> 
			</div>
		</div>
	</div>
</div>