<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="/WEB-INF/tlds/spring.tld" prefix="spring" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ taglib uri="http://www.springframework.org/security/tags" prefix="sec" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ taglib uri="/WEB-INF/tlds/custom.tld" prefix="wj" %>

<tiles:insertDefinition name="admin.responsive-template.crm.contact" flush="true">
	<tiles:putAttribute name="topbar" value="/WEB-INF/jsp/admin-responsive/common/topbar.jsp?tab=crmMapCustomer">
	</tiles:putAttribute>
	<tiles:putAttribute name="content" type="string">
		<sec:authorize ifAnyGranted="ROLE_AEM">
			<!-- Begin: Content -->
			<section id="content" class="animated fadeIn"> 
			    <form  action="crmContactCustomerMap.jhtm" method="post">
				 	
			  		<!-- Error Message -->
				  	<c:if test="${!empty message}">
				  		<div class="message"><spring:message code="${message}" /></div>
				  	</c:if>
				 	
				 	<div class="panel panel-default">
			         	<ul class="nav panel-tabs panel-tabs-border">
			                <li class="active">
                                <li class="active">
				                    <a href="javascript:void(0);" data-toggle="tab" data-target="#tab_1"><fmt:message key="mapToCustomer" /></a>
				                </li> 
			                </li>
			            </ul>
			            
			            <!-- Body -->
	                    <div class="panel-body">
			                <div class="tab-content">
			                    <div id="tab_1" class="tab-pane active"> 
			                        <div class="form-horizontal"> 
			                        
			                            <div class="form-group">
		                                	<label for="getUsername" class="col-sm-3 col-md-3 col-lg-3 control-label">    
			                                	<input type="hidden" name="__key" value="getUsername"><fmt:message key="username" />::
                                			</label>
			                               	<div class="col-sm-9 col-md-8 col-lg-8">
												<select name="getUsername" class="form-control" id="getUsername">
										  	      <option value="">Please Select</option>
											      <c:forEach items="${fields}" var="field">
										  	        <option value="${field.value}" <c:if test="${field.value == data['getUsername']}">selected</c:if>>${field.key}</option>
												  </c:forEach>		
												</select>
	                                	 	</div>
			                            </div>  
			                             
			                       		<div class="form-group">
			                                <label for="getAddress_getFirstName" class="col-sm-3 col-md-3 col-lg-3 control-label">   
			                                	<input type="hidden" name="__key" value="getAddress_getFirstName"><fmt:message key="firstName" />:
			                                </label>
			                               <div class="col-sm-9 col-md-8 col-lg-8">
												<select name="getAddress_getFirstName" class="form-control" id="getAddress_getFirstName">
										  	      <option value="">Please Select</option>
											      <c:forEach items="${fields}" var="field">
											       <option value="${field.value}" <c:if test="${field.value == data['getAddress_getFirstName']}">selected</c:if>>${field.key}</option>
												  </c:forEach>		
												</select>
			                                </div> 
			                            </div> 
			                            
			                            <div class="form-group">
			                                <label for="getAddress_getLastName" class="col-sm-3 col-md-3 col-lg-3 control-label">   
			                                	<input type="hidden" name="__key" value="getAddress_getLastName"><fmt:message key="lastName" />:
			                                </label>
			                               <div class="col-sm-9 col-md-8 col-lg-8">
												<select name="getAddress_getLastName" class="form-control" id="getAddress_getLastName">
										  	      <option value="">Please Select</option>
											      <c:forEach items="${fields}" var="field">
										  	        <option value="${field.value}" <c:if test="${field.value == data['getAddress_getLastName']}">selected</c:if>>${field.key}</option>
												  </c:forEach>			
												</select>
			                                </div> 
			                            </div>   
			                            
			                            <div class="form-group">
			                                <label for="getAddress_getAddr1" class="col-sm-3 col-md-3 col-lg-3 control-label">   
			                                	<input type="hidden" name="__key" value="getAddress_getAddr1"><fmt:message key="address" />1:
			                                </label>
			                               <div class="col-sm-9 col-md-8 col-lg-8">
												<select name="getAddress_getAddr1" class="form-control" id="getAddress_getAddr1">
										  	      <option value="">Please Select</option>
											      <c:forEach items="${fields}" var="field">
										  	        <option value="${field.value}" <c:if test="${field.value == data['getAddress_getAddr1']}">selected</c:if>>${field.key}</option>
												  </c:forEach>				
												</select>
			                                </div> 
			                            </div>
			                            
			                            <div class="form-group">
			                                <label for="getAddress_getAddr2" class="col-sm-3 col-md-3 col-lg-3 control-label">   
			                                	<input type="hidden" name="__key" value="getAddress_getAddr2"><fmt:message key="address" />2:
			                                </label>
			                               <div class="col-sm-9 col-md-8 col-lg-8">
												<select name="getAddress_getAddr2" class="form-control" id="getAddress_getAddr2">
										  	      <option value="">Please Select</option>
											      <c:forEach items="${fields}" var="field">
										  	        <option value="${field.value}" <c:if test="${field.value == data['getAddress_getAddr2']}">selected</c:if>>${field.key}</option>
												  </c:forEach>					
												</select>
			                                </div> 
			                            </div>
			                            
			                            <div class="form-group">
			                                <label for="getAddress_getCity" class="col-sm-3 col-md-3 col-lg-3 control-label">   
			                                	<input type="hidden" name="__key" value="getAddress_getCity"><fmt:message key="city" />:
			                                </label>
			                               <div class="col-sm-9 col-md-8 col-lg-8">
												<select name="getAddress_getCity" class="form-control" id="getAddress_getCity">
													<option value="">Please Select</option>
											      	<c:forEach items="${fields}" var="field">
											  	        <option value="${field.value}" <c:if test="${field.value == data['getAddress_getCity']}">selected</c:if>>${field.key}</option>
												  	</c:forEach>				
												</select>
			                                </div> 
			                            </div>
			                            
                            			<div class="form-group">
			                                <label for="getAddress_getStateProvince" class="col-sm-3 col-md-3 col-lg-3 control-label">   
			                                	<input type="hidden" name="__key" value="getAddress_getStateProvince"><fmt:message key="stateProvince" />:
			                                </label>
			                               <div class="col-sm-9 col-md-8 col-lg-8">
												<select name="getAddress_getStateProvince" class="form-control" id="getAddress_getStateProvince">
													<option value="">Please Select</option>
											      	<c:forEach items="${fields}" var="field">
										  	        	<option value="${field.value}" <c:if test="${field.value == data['getAddress_getStateProvince']}">selected</c:if>>${field.key}</option>
												  	</c:forEach>					
												</select>
			                                </div> 
			                            </div>
			                            
			                            <div class="form-group">
			                                <label for="getAddress_getZip" class="col-sm-3 col-md-3 col-lg-3 control-label">   
			                                	<input type="hidden" name="__key" value="getAddress_getZip"><fmt:message key="zipCode" />:
			                                </label>
			                               <div class="col-sm-9 col-md-8 col-lg-8">
												<select name="getAddress_getZip" class="form-control" id="getAddress_getZip">
													<option value="">Please Select</option>
										      		<c:forEach items="${fields}" var="field">
											  	        <option value="${field.value}" <c:if test="${field.value == data['getAddress_getZip']}">selected</c:if>>${field.key}</option>
												  	</c:forEach>				
												</select>
			                                </div> 
			                            </div>
			                            
			                            <div class="form-group">
			                                <label for="getAddress_getCountry" class="col-sm-3 col-md-3 col-lg-3 control-label">   
			                                	<input type="hidden" name="__key" value="getAddress_getCountry"><fmt:message key="country" />:
			                                </label>
			                               <div class="col-sm-9 col-md-8 col-lg-8">
												<select name="getAddress_getCountry" class="form-control" id="getAddress_getCountry">
													<option value="">Please Select</option>
											      	<c:forEach items="${fields}" var="field">
											  	        <option value="${field.value}" <c:if test="${field.value == data['getAddress_getCountry']}">selected</c:if>>${field.key}</option>
												 	 </c:forEach>				
												</select>
			                                </div> 
			                            </div>
			                            
			                            <div class="form-group">
			                                <label for="getAddress_getPhone" class="col-sm-3 col-md-3 col-lg-3 control-label">   
			                                	<input type="hidden" name="__key" value="getAddress_getPhone"><fmt:message key="phone" />:
			                                </label>
			                               <div class="col-sm-9 col-md-8 col-lg-8">
												<select name="getAddress_getPhone" class="form-control" id="getAddress_getPhone">
													<option value="">Please Select</option>
											      	<c:forEach items="${fields}" var="field">
											  	        <option value="${field.value}" <c:if test="${field.value == data['getAddress_getPhone']}">selected</c:if>>${field.key}</option>
												  	</c:forEach>				
												</select>
			                                </div> 
			                            </div>
			                            
			                            <div class="form-group">
			                                <label for="getAddress_getCellPhone" class="col-sm-3 col-md-3 col-lg-3 control-label">   
			                                	<input type="hidden" name="__key" value="getAddress_getCellPhone"><fmt:message key="cellPhone" />:
			                                </label>
			                               <div class="col-sm-9 col-md-8 col-lg-8">
												<select name="getAddress_getCellPhone" class="form-control" id="getAddress_getCellPhone">
													<option value="">Please Select</option>
										      		<c:forEach items="${fields}" var="field">
											  	        <option value="${field.value}" <c:if test="${field.value == data['getAddress_getCellPhone']}">selected</c:if>>${field.key}</option>
												  	</c:forEach>				
												</select>
			                                </div> 
			                            </div>
			                            
			                            <div class="form-group">
			                                <label for="getAddress_getFax" class="col-sm-3 col-md-3 col-lg-3 control-label">   
			                                	<input type="hidden" name="__key" value="getAddress_getFax"><fmt:message key="fax" />:
			                                </label>
			                               <div class="col-sm-9 col-md-8 col-lg-8">
												<select name="getAddress_getFax" class="form-control" id="getAddress_getFax">
													<option value="">Please Select</option>
										      		<c:forEach items="${fields}" var="field">
											  	        <option value="${field.value}" <c:if test="${field.value == data['getAddress_getFax']}">selected</c:if>>${field.key}</option>
												  	</c:forEach>				
												</select>
			                                </div> 
			                            </div>
			                            
			                            <div class="form-group">
			                                <label for="getAddress_getCompany" class="col-sm-3 col-md-3 col-lg-3 control-label">   
			                                	<input type="hidden" name="__key" value="getAddress_getCompany"><fmt:message key="company" />:
			                                </label>
			                               <div class="col-sm-9 col-md-8 col-lg-8">
												<select name="getAddress_getCompany" class="form-control" id="getAddress_getCompany">
													<option value="">Please Select</option>
										      		<c:forEach items="${fields}" var="field">
											  	        <option value="${field.value}" <c:if test="${field.value == data['getAddress_getCompany']}">selected</c:if>>${field.key}</option>
												  	</c:forEach>				
												</select>
			                                </div> 
			                            </div>
			                            
			                            <div class="form-group">
			                                <label for="getTaxId" class="col-sm-3 col-md-3 col-lg-3 control-label">   
			                                	<input type="hidden" name="__key" value="getTaxId"><fmt:message key="taxId" />:
			                                </label>
			                               <div class="col-sm-9 col-md-8 col-lg-8">
												<select name="getTaxId" class="form-control" id="getTaxId">
													<option value="">Please Select</option>
										      		<c:forEach items="${fields}" var="field">
											  	        <option value="${field.value}" <c:if test="${field.value == data['getTaxId']}">selected</c:if>>${field.key}</option>
												  	</c:forEach>				
												</select>
			                                </div> 
			                            </div>
			                            
			                            <div class="form-group">
			                                <label for="getNote" class="col-sm-3 col-md-3 col-lg-3 control-label">   
			                                	<input type="hidden" name="__key" value="getNote"><fmt:message key="note" />:
			                                </label>
			                               <div class="col-sm-9 col-md-8 col-lg-8">
												<select name="getNote" class="form-control" id="getNote">
													<option value="">Please Select</option>
										      		<c:forEach items="${fields}" var="field">
											  	        <option value="${field.value}" <c:if test="${field.value == data['getNote']}">selected</c:if>>${field.key}</option>
												  	</c:forEach>				
												</select>
			                                </div> 
			                            </div>
			                            
			                            <div class="form-group">
			                                <label for="getTrackcode" class="col-sm-3 col-md-3 col-lg-3 control-label">   
			                                	<input type="hidden" name="__key" value="getTrackcode"><fmt:message key="trackcode" />:
			                                </label>
			                               <div class="col-sm-9 col-md-8 col-lg-8">
												<select name="getTrackcode" class="form-control" id="getTrackcode">
													<option value="">Please Select</option>
										      		<c:forEach items="${fields}" var="field">
											  	        <option value="${field.value}" <c:if test="${field.value == data['getTrackcode']}">selected</c:if>>${field.key}</option>
												  	</c:forEach>				
												</select>
			                                </div> 
			                            </div>
			                            
			                            <div class="form-group">
			                                <label for="getRegisteredBy" class="col-sm-3 col-md-3 col-lg-3 control-label">   
			                                	<input type="hidden" name="__key" value="getRegisteredBy"><fmt:message key="registeredBy" />:
			                                </label>
			                               <div class="col-sm-9 col-md-8 col-lg-8">
												<select name="getRegisteredBy" class="form-control" id="getTaxId">
													<option value="">Please Select</option>
										      		<c:forEach items="${fields}" var="field">
											  	        <option value="${field.value}" <c:if test="${field.value == data['getRegisteredBy']}">selected</c:if>>${field.key}</option>
												  	</c:forEach>				
												</select>
			                                </div> 
			                            </div>
			                            
			                            <c:forEach begin="1" end="20" var="num">
				                            <div class="form-group">
				                                <label for="getField${num}" class="col-sm-3 col-md-3 col-lg-3 control-label">   
				                                	<input type="hidden" name="__key" value="getField${num}"><fmt:message key="field" />${num}:
				                                </label>
				                               <div class="col-sm-9 col-md-8 col-lg-8">
													<select name="getField${num}" class="form-control" id="getField${num}">
											  	      <option value="">Please Select</option>
												      <c:forEach items="${fields}" var="field">
											  	        <option value="${field.value}" <c:if test="${field.value == data[number]}">selected</c:if>>${field.key}</option>
													  </c:forEach>		
													</select>
				                                </div> 
				                            </div>
			                            </c:forEach>
			                        </div> 
			                   	</div> 
                                
				            </div>
			            </div> 
			            
			            <!-- Footer -->
			            <div class="panel-footer">
			                <div class="clearfix">
			                    <div class="pull-right"> 
		                    	  	<sec:authorize ifAnyGranted="ROLE_AEM,ROLE_ADMIN_${prop_site_id}">
		                    	  		<button type="submit" class="btn btn-primary" name="__update" value="<fmt:message key="Update" />">
			                            	<span class="fa fa-refresh mr-5"></span> Update
		                        		</button>  
								  	</sec:authorize> 
								</div>
							</div>
						</div>
			   		</div>
			     </form>
			</section>
		</sec:authorize>
	</tiles:putAttribute>
</tiles:insertDefinition>
<script type="text/javascript"> 
<!--
window.addEvent('domready', function(){			
	var tabs1 = new SimpleTabs($('tab-block-1'), { selector: 'h4'});
	var Tips1 = new Tips($$('.toolTipImg'));
});
//-->
</script>		