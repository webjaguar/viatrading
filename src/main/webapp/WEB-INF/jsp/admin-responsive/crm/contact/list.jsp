<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ taglib uri="http://www.springframework.org/security/tags" prefix="sec" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ taglib uri="/WEB-INF/tlds/spring.tld" prefix="spring" %>
<%@ taglib uri="/WEB-INF/tlds/custom.tld" prefix="wj" %>

<tiles:insertDefinition name="admin.responsive-template.crm.contact" flush="true">
	<tiles:putAttribute name="css">
		<style>
			#grid .field-customer{
				text-align: center;
			}
			#grid .field-contactName{
				width: 150px;
			}
			#grid .field-email1{
				width: 150px;
			}
			#grid .field-accountName{
				width: 150px;
			}
			#grid .field-created{
				width: 120px;
			}
			#grid .field-city{
				width: 120px;
			}
			#grid .field-phone1{
				width: 120px;
			}
			#grid .field-short_desc{
				width: 150px;
			}
		</style>
	</tiles:putAttribute>
	<tiles:putAttribute name="content" type="string">
		<sec:authorize ifAnyGranted="ROLE_AEM,ROLE_ADMIN_${prop_site_id},ROLE_CRM">	
			<form action="crmContactList.jhtm" method="post" id="list" name="list_form">
				<div class="page-banner">
					<div class="breadcrumb"><span><a href="${_contextpath}/admin/catalog/dashboard.jhtm">Home</a></span><i class="sli-arrow-right"></i><span><a href="${_contextpath}/admin/crm">CRM</a></span><i class="sli-arrow-right"></i><span>Accounts</span></div>
				</div>
				<div class="page-head">
					<div class="row justify-content-between">
						<div class="col-sm-8 title">
							<h3>Contacts</h3>
						</div>
						<div class="col-sm-4 actions">
							<div class="dropdown dropdown-actions">
								<button type="button" class="btn-outline dropdown-toggle active btn btn-primary" data-toggle="dropdown">
		                             <i class="sli-settings"></i> Actions
		                         </button>
		                         <div class="dropdown-menu dropdown-menu-right">
									<sec:authorize ifAnyGranted="ROLE_AEM,ROLE_ADMIN_${prop_site_id},ROLE_CRM">
										<a class="dropdown-item"><fmt:message key="add"/></a>
										<a class="dropdown-item" onClick="return confirm('Delete permanently? Contacts converted to customer will not be deleted.')"><fmt:message key="delete"/></a>
							  	  	</sec:authorize>
		                         </div>
		                    </div>
		                    <div class="dropdown dropdown-setting">
								<button type="button" class="btn-outline dropdown-toggle active btn btn-primary" data-toggle="dropdown">
		                             <i class="sli-screen-desktop"></i> Display
		                         </button>	                         
								<div class="dropdown-menu dropdown-menu-right">
									<div class="form-group">
				                    		<div class="option-header">Rows</div>
										<select class="custom-select form-control" name="size" onchange="document.getElementById('page').value=1;submit();">
											<c:forTokens items="10,25,50,100,200,500" delims="," var="current">
									  	  		<option value="${current}" <c:if test="${current == crmContactSearch.pageSize}">selected</c:if>>${current}</option>
								  			</c:forTokens>
										</select>
									</div>
									<div class="form-group">
				                    		<div class="option-header">Columns</div>
				                    		<div id="column-hider"></div>
									</div>
								</div>
		                    </div>
						</div>
					</div>
				</div>
				<div class="page-body">
					<div class="grid-stage">
						<c:if test="${model.count > 0}">
							<div class="page-stat">
								<fmt:message key="showing">
									<fmt:param value="${crmContactSearch.offset+1}"/>
									<fmt:param value="${model.pageEnd}"/>
									<fmt:param value="${model.count}"/>
								  </fmt:message>
							</div> 
						</c:if>
					    <table id="grid"></table>
					    <div class="footer">
			                <div class="float-right">
								<c:if test="${crmContactSearch.page == 1}">
							 		<span class="previous"><i class="sli-arrow-left" style="font-size: 0.7rem"></i> Previous</span>
								</c:if>
								<c:if test="${crmContactSearch.page != 1}">
									<a href="<c:url value="crmContactList.jhtm"><c:param name="page" value="${crmContactSearch.page-1}"/></c:url>">
										<span class="previous"><i class="sli-arrow-left" style="font-size: 0.7rem"></i> Previous</span>
									</a>
								</c:if>	
								<span class="status"><c:out value="${crmAccountSearch.page}" /> of <c:out value="${model.pageCount}" /></span>
								<c:if test="${crmContactSearch.page == model.pageCount}">
  									<span class="next">Next <i class="sli-arrow-right" style="font-size: 0.7rem"></i></span>
 								</c:if>
								<c:if test="${crmContactSearch.page != model.pageCount}">
  									<a href="<c:url value="crmContactList.jhtm"><c:param name="page" value="${crmContactSearch.page+1}"/></c:url>">
										<span class="next">Next <i class="sli-arrow-right" style="font-size: 0.7rem"></i></span>
									</a>
 								</c:if>
							</div>
						</div>
					</div>
				</div>
			    <input type="hidden" id="page" name="page" />
			    <input type="hidden" id="sort" name="sort" value="${crmContactSearch.sort}" />
		    </form>
		</sec:authorize>
	</tiles:putAttribute>
	<tiles:putAttribute name="js">
		<!-- DGRID TABLE SCRIPT MUST BE AT BOTTOM OF THE PAGE -->
		<script src="${_contextpath}/admin-responsive-static/assets/plugins/dojo/dojo.js" data-dojo-config="async: true"></script>
		<script>
			//Table Data Sanitization
			var data = [];
			<c:forEach items="${model.crmContactList}" var="contact" varStatus="status">
				data.push({
					id: ${contact.id},
					accountId: ${contact.id},
					firstName: '${wj:escapeJS(contact.firstName)}',
					lastName: '${wj:escapeJS(contact.lastName)}',
					<c:if test="${contact.userId != null}">
						customer: true,
			        </c:if>
					email1: "${wj:escapeJS(contact.email1)}",
					<c:choose>
						<c:when test="${contact.accountId > 1}">
							accountId: ${contact.accountId},
							accountName: '${wj:escapeJS(contact.accountName)}',
						</c:when>
						<c:otherwise>
							accountName: '${wj:escapeJS(contact.accountName)}',
						</c:otherwise>
					</c:choose>
					created: '<fmt:formatDate type="date" timeStyle="default" value="${contact.created}"/>',
					leadSource: "${wj:escapeJS(contact.leadSource)}",
					city: "${wj:escapeJS(contact.city)}",
					phone1: "${wj:escapeJS(contact.phone1)}",
					trackcode: "${wj:escapeJS(contact.trackcode)}",
					user: "${wj:escapeJS(model.accessUserMap[contact.accessUserId].name)}",
					short_desc: "${wj:escapeJS(contact.shortDesc)}",
					<c:if test="${siteConfig['MBLOCK'].value == 'true'}">
						<c:if test="${contact.pushedToAccounting}">
							pushedToAccounting: true,
						</c:if>
				    </c:if>
				});
			</c:forEach>
			
			require(["dgrid/List", "dgrid/OnDemandGrid", "dgrid/Selection", 'dgrid/Selector', "dgrid/Keyboard", "dgrid/Editor", "dijit/form/CheckBox", 
					"dijit/form/TimeTextBox", "dijit/form/Select", "dojo/data/ObjectStore", "dijit/form/FilteringSelect",
					'dijit/form/DropDownButton', 'dijit/DropDownMenu', 'dijit/MenuItem',
					"dojo/_base/declare", "dojo/_base/lang", "dojo/store/Memory", "dojo/store/Observable", "dgrid/test/data/createSyncStore", 
					'dgrid/extensions/ColumnHider', 'dgrid/extensions/ColumnReorder', 'dgrid/extensions/ColumnResizer',
				], function(List, OnDemandGrid, Selection, Selector, Keyboard, Editor, CheckBox, TimeTextBox, Select, ObjectStore, FilteringSelect,
						DropDownButton, DropDownMenu, MenuItem,
						declare, lang, LegacyMemory, LegacyObservable, createSyncStore, ColumnHider, ColumnReorder, ColumnResizer){

				//Column Definitions
				var columns = {
			        batch: {
						selector: 'checkbox',
						unhidable: true
					},
			        <sec:authorize ifAnyGranted="ROLE_AEM,ROLE_ADMIN_${prop_site_id},ROLE_CRM">
						actions: {
							label: 'Actions',
							renderCell: function(object, data, td, options){
								var div = document.createElement("div");
								var menu = new DropDownMenu({
									style: "display: none;"
								});
								
								var menuItem;
								
								menuItem = new MenuItem({
							        label: '<fmt:message key="edit" />',
							        onClick: function(){
							        		location.href = 'crmContactForm.jhtm?id='+object.id+'&accountId='+object.accountId;
							        	}
							    });
							    menu.addChild(menuItem);
								
							    menuItem = new MenuItem({
							        label: '<fmt:message key="sendEmail" />',
							        onClick: function(){
							        		location.href = 'email.jhtm?contactId='+object.id;
							        	}
							    });
							    menu.addChild(menuItem);
							    
							    menuItem = new MenuItem({
							        label: '<fmt:message key="addTask" />',
							        onClick: function(){
							        		location.href = 'crmTaskForm.jhtm?accountId='+object.accountId+'&contactId='+object.id;
							        	}
							    });
							    menu.addChild(menuItem);
							    
							    menuItem = new MenuItem({
							        label: '<fmt:message key="viewTask" />',
							        onClick: function(){
							        		location.href = 'crmTaskList.jhtm?contactId='+object.id;
							        	}
							    });
							    menu.addChild(menuItem);
							    
							    menu.startup();
 
							    var dropdown = new DropDownButton({
							        iconClass: "sli-settings",
							        dropDown: menu,
							    },'button');
							    
							    div.appendChild(dropdown.domNode);
			                    return div;
							},
							sortable: false
						},
					</sec:authorize>
					contactName: {
						label: '<fmt:message key="crmContactName" />',
						renderCell: function(object){						
						 	var link = document.createElement("a");
							link.text = object.firstName + ' ' + object.lastName;
							link.href = 'crmContactForm.jhtm?id='+object.id+'&accountId='+object.accountId;
							return link;
						},
						sortable: false,
					},
					customer: {
						label: '<fmt:message key="customer" />',
						renderCell: function(object){
							var div = document.createElement("div");
							if(object.customer){
								var i = document.createElement("i");
								i.className = 'sli-check';
								div.appendChild(i);
							}
							return div;
						},
						sortable: false,
					},
					email1: {
						label: '<fmt:message key="email" />1',
						get: function(object){
							return object.email1;
						},
						sortable: false,
					},
					accountName: {
						label: '<fmt:message key="crmAccountName" />',
						renderCell: function(object){
							if(object.accountId){
								var link = document.createElement("a");
								link.className = 'plain';
								link.text = object.accountName;
								link.href = 'crmAccountForm.jhtm?id='+object.accountId;
								return link;
							}
							var span = document.createElement("span");
							span.textContent = object.accountName;
							return span;
						},
						sortable: false,
					},
					created: {
						label: '<fmt:message key="crmContactCreated" />',
						get: function(object){
							return object.created;
						},
						sortable: false,
					},
					leadSource: {
						label: '<fmt:message key="leadSource" />',
						get: function(object){
							return object.leadSource;
						},
						sortable: false,
					},
					city: {
						label: '<fmt:message key="city" />',
						get: function(object){
							return object.city;
						},
						sortable: false,
					},
					phone1: {
						label: '<fmt:message key="phone" />',
						get: function(object){
							return object.phone1;
						},
						sortable: false,
					},
					phone1: {
						label: '<fmt:message key="phone" />',
						get: function(object){
							return object.phone1;
						},
						sortable: false,
					},
					trackcode: {
						label: '<fmt:message key="trackcode" />',
						get: function(object){
							return object.trackcode;
						},
						sortable: false,
					},
					user: {
						label: '<fmt:message key="user" />',
						get: function(object){
							return object.user;
						},
						sortable: false,
					},
					short_desc: {
						label: '<fmt:message key="shortDesc" />',
						get: function(object){
							return object.short_desc;
						},
						sortable: false,
					}
					<c:if test="${siteConfig['MBLOCK'].value == 'true'}">
						pushedToAccounting: {
							label: '<fmt:message key="shortDesc" />',
							renderCell: function(object){
								var div = document.createElement("div");
								if(object.pushedToAccounting){
									var i = document.createElement("i");
									i.className = 'sli-check';
									div.appendChild(i);
								}
								return div;
							},
							sortable: false,
						}
					</c:if>
				};
				
				var grid = new (declare([OnDemandGrid, Selection, Selector, Keyboard, Editor, ColumnHider, ColumnReorder, ColumnResizer]))({
					collection: createSyncStore({ data: data }),
					selectionMode: "none",
					allowSelectAll: true,
					columns: columns,
				},'grid');
				
				grid.on('dgrid-refresh-complete', function(event) {
					$('#grid-hider-menu').appendTo('#column-hider');
				});
				
				grid.startup();
			});
		</script>
	</tiles:putAttribute>
</tiles:insertDefinition>