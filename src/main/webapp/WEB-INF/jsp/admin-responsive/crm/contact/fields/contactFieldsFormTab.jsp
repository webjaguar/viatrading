<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="/WEB-INF/tlds/spring.tld" prefix="spring" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ taglib uri="http://www.springframework.org/security/tags" prefix="sec" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ taglib uri="/WEB-INF/tlds/custom.tld" prefix="wj" %>

<div id="tab_1" class="tab-pane active"> 
	<div class="line"> 
		<div class="form-row">
			<div class="col-lg-4">
				<label for="crmContactField_enabled" class="col-form-label">   
					<span class="fa fa-fw fa-question-circle" data-toggle="tooltip" title="If checked, field will appear on Contact."></span>
						<fmt:message key="enabled" />
				</label>
			</div>
			<div class="col-lg-4">
				<label class="custom-control custom-checkbox">
					<form:checkbox path="crmContactField.enabled" id="crmContactField_enabled" class="custom-control-input" />
					<span class="custom-control-indicator"></span>
				</label>
			</div>
		</div>
	</div>
	<div class="line">
		<div class="form-row">
			<div class="col-lg-4">
				<label for="crmContactField_globalName" class="col-form-label">   
					<span class="fa fa-question-circle" data-toggle="tooltip" title="In order to match with Customer fields, ensure names are same. i.e., name should be 'Field 1', 'Field 2', etc."></span>
					<span class="requiredField"><fmt:message key="field" /></span>
				</label>
			</div>
			<div class="col-lg-4">
				<c:choose>
					<c:when test="${crmContactFieldForm.crmContactField.id lt 21 and crmContactFieldForm.crmContactField.id gt 0}">
						<form:input path="crmContactField.globalName" id="crmContactField_globalName" class="form-control" maxlength="255" htmlEscape="true" readonly="true"/>
					</c:when>
					<c:otherwise>
						<form:input path="crmContactField.globalName" id="crmContactField_globalName" class="form-control" maxlength="255" htmlEscape="true" />
					</c:otherwise>
				</c:choose>
			</div> 
		</div>  
	</div>
	<div class="line">
		<div class="form-row">
			<div class="col-lg-4">
				<label for=crmContactField_fieldName class="col-form-label"> 
					<fmt:message key="name" /> 
				</label>
			</div>
			<div class="col-lg-4">
				<form:input path="crmContactField.fieldName" class="form-control" id="crmContactField_fieldName" maxlength="255"/>
			</div>
		</div>
	</div>
	<div class="line">
		<div class="form-row">
			<div class="col-lg-4">
				<label for="crmContactField_fieldType" class="col-form-label">   
					<fmt:message key="type" />
				</label>
			</div>
			<div class="col-lg-4">
				<form:select path="crmContactField.fieldType" class="custom-select" id="crmContactField_fieldType">
					<form:option value="1" label="Text"></form:option>
					<form:option value="2" label="Text Area"></form:option>
					<form:option value="3" label="Check Box"></form:option>
					<form:option value="4" label="Radio Button"></form:option>
					<form:option value="5" label="Drop Down List"></form:option>
					<form:option value="7" label="Validation"></form:option>
				</form:select>
			</div>
		</div>  
	</div>
	<div class="line">
		<div class="form-row">
			<div class="col-lg-4">
				<label for="crmContactField_options" class="col-form-label">
					<fmt:message key="options" />
				</label>
			</div>
			<div class="col-lg-4">
				<form:textarea path="crmContactField.options" class="form-control" rows="8" id="crmContactField_options"/>
			</div>
		</div> 
	</div>
</div>                                