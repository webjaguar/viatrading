<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="/WEB-INF/tlds/spring.tld" prefix="spring" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ taglib uri="http://www.springframework.org/security/tags" prefix="sec" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ taglib uri="/WEB-INF/tlds/custom.tld" prefix="wj" %>

<tiles:insertDefinition name="admin.responsive-template.crm.contactFieldForm" flush="true">
	<tiles:putAttribute name="css">
	</tiles:putAttribute>
	<tiles:putAttribute name="content" type="string">
		<sec:authorize ifAnyGranted="ROLE_AEM,ROLE_ADMIN_${prop_site_id},ROLE_CRM_FIELD">
			<form:form commandName="crmContactFieldForm" method="post" class="page-form">
				<div class="page-banner">
					<div class="breadcrumb"><span><a href="${_contextpath}/admin/catalog/dashboard.jhtm">Home</a></span><i class="sli-arrow-right"></i><span><a href="${_contextpath}/admin/crm">CRM</a></span><i class="sli-arrow-right"></i><span><a href="${_contextpath}/admin/crm/crmContactFieldList.jhtm">Fields</a></span><i class="sli-arrow-right"></i><span>Form</span></div>
				</div>
				<div class="page-head">
					<div class="row justify-content-between">
						<div class="col-sm-8 title">
							<h3>${crmFormBuilderForm.crmForm.formName}</h3>
						</div>
						<div class="col-sm-4 actions">
							<div class="dropdown">
								<button type="button" class="btn-outline dropdown-toggle active btn btn-primary" data-toggle="dropdown">
									<i class="sli-settings"></i> Actions
		                         </button>
								<div class="dropdown-menu dropdown-menu-right">
									<c:if test="${crmContactFieldForm.newCrmContactField}">
									    <sec:authorize ifAnyGranted="ROLE_AEM,ROLE_ADMIN_${prop_site_id},ROLE_CRM"> 
											<a class="dropdown-item"><spring:message code="add"/></a>
								        </sec:authorize>
							      	</c:if>
							      	<c:if test="${!crmContactFieldForm.newCrmContactField}">
										<sec:authorize ifAnyGranted="ROLE_AEM,ROLE_ADMIN_${prop_site_id},ROLE_CRM">
											<a class="dropdown-item"><spring:message code="update"/></a>
										</sec:authorize>
										<c:if test="${crmContactFieldForm.crmContactField.id > 20}">
											<sec:authorize ifAnyGranted="ROLE_AEM,ROLE_ADMIN_${prop_site_id},ROLE_CRM">   
												<a class="dropdown-item" onClick="return confirm('Delete permanently?')"><spring:message code="delete"/></a>
											</sec:authorize>  
										</c:if>
									</c:if>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="page-body">
					<div class="main-content">
						<div class="form-section">
							<div class="content">
							 	<ul class="nav nav-tabs" role="tablist">
					                <li class="nav-item">
					                    <a class="nav-link active" data-toggle="tab" role="tab" href="#tab_1"><fmt:message key="field"/></a>
					                </li>
				                </ul>
				                 <div class="tab-content">
									<jsp:include page="contactFieldsFormTab.jsp" />
				                 </div>
			                </div>
				            <div class="footer">
								<div class="form-row">
									<div class="col-12">
										<input class="btn btn-default" type="submit" value="<fmt:message key="nextStep" />" name="_target1">
									</div>
								</div>
				            </div>
		                </div>
	                </div>
				</div>
				<form:hidden path="newCrmContactField" />
			</form:form>
		</sec:authorize>
	</tiles:putAttribute>
	<tiles:putAttribute name="js">
	</tiles:putAttribute>
</tiles:insertDefinition>