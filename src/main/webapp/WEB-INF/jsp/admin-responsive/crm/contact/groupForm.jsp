<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="/WEB-INF/tlds/spring.tld" prefix="spring" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ taglib uri="http://www.springframework.org/security/tags" prefix="sec" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ taglib uri="/WEB-INF/tlds/custom.tld" prefix="wj" %>

<tiles:insertDefinition name="admin.responsive-template.crm.groupForm" flush="true">
	<tiles:putAttribute name="css">
	</tiles:putAttribute>
	<tiles:putAttribute name="content" type="string">
		<sec:authorize ifAnyGranted="ROLE_AEM,ROLE_ADMIN_${prop_site_id},ROLE_CRM_GROUP">
			<form:form commandName="crmContactGroupForm" method="post" class="page-form">
				<div class="page-banner">
					<div class="breadcrumb"><span><a href="${_contextpath}/admin/catalog/dashboard.jhtm">Home</a></span><i class="sli-arrow-right"></i><span><a href="${_contextpath}/admin/crm">CRM</a></span><i class="sli-arrow-right"></i><span><a href="${_contextpath}/admin/crm/crmContactGroupList.jhtm">Groups</a></span><i class="sli-arrow-right"></i><span>Form</span></div>
				</div>
				<div class="page-head">
					<div class="row justify-content-between">
						<div class="col-sm-8 title">
							<h3>${crmContactGroupForm.crmContactGroup.name}</h3>
						</div>
						<div class="col-sm-4 actions">
							<div class="dropdown">
								<button type="button" class="btn-outline dropdown-toggle active btn btn-primary" data-toggle="dropdown">
									<i class="sli-settings"></i> Actions
		                         </button>
								<div class="dropdown-menu dropdown-menu-right">
									<c:if test="${!crmContactGroupForm.newCrmContactGroup}">
										<a class="dropdown-item" onClick="return confirm('Delete Group will cause other applications to malfunction which requires this Group. Do you still want to delete it permanently?')">Delete</a>
								  	</c:if>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="page-body">
					<div class="main-content">
						<div class="form-section">
							<div class="content">
							 	<ul class="nav nav-tabs" role="tablist">
					                <li class="nav-item">
					                    <a class="nav-link active" data-toggle="tab" role="tab" href="#tab_1"><fmt:message key="group"/></a>
					                </li>
									<c:if test="${!crmContactGroupForm.newCrmContactGroup}">
										<c:if test="${siteConfig['CLICK_DIALING'].value == 'true'}">
											<li>
												<a class="nav-link" data-toggle="tab" role="tab" href="#tab_2">Dialing Information</a>
											</li> 
										</c:if>
									</c:if>
								</ul>
				                 <div class="tab-content">
									<div id="tab_1" class="tab-pane active"> 
										<div class="line"> 
											<div class="form-row">
												<div class="col-lg-4">
													<label for="crmContactGroup_active" class="col-form-label">    
														<fmt:message key="active" />
													</label>
												</div>
												<div class="col-lg-4">
													<label class="custom-control custom-checkbox">
									      				<form:checkbox path="crmContactGroup.active" value="true" id="crmContactGroup_active" class="custom-control-input"/>
									      				<span class="custom-control-indicator"></span>
									      			</label>
												</div>
											</div>  
										</div> 
										<div class="line">
											<div class="form-row">
												<div class="col-lg-4">
													<label for="crmContactGroup_name" class="col-form-label">   
														<fmt:message key="field" />
													</label>
												</div>
												<div class="col-lg-4">
													<form:input path="crmContactGroup.name" class="form-control" id="crmContactGroup_name" />
												</div>
											</div>
										</div>
										<div class="line">
											<div class="form-row">
												<div class="col-lg-4">
													<label for="messageSelected" class="col-form-label">   
														<fmt:message key="message" />
													</label>
												</div>
												<div class="col-lg-4">
													<select id="messageSelected" name="messageSelected" class="custom-select" id="messageSelected">
														<option value="">choose a message</option>
														<c:forEach var="message" items="${messages}">
															<option value="${message.messageId}" <c:if test="${crmContactGroupForm.crmContactGroup.messageId == message.messageId}">selected="selected"</c:if>><c:out value="${message.messageName}"/></option>
														</c:forEach>
													</select>
												</div>
											</div>  
			                             </div>
			                        </div> 
			                        <c:if test="${!crmContactGroupForm.newCrmContactGroup}">
										<c:if test="${siteConfig['CLICK_DIALING'].value == 'true'}">
											<div id="tab_2" class="tab-pane"> 
												<div class="line"> 
													<div class="form-row">
														<div class="col-lg-4">
															<label for="crmContactGroup_closerIds" class="col-form-label">
																Closer Ids
															</label>
														</div>
														<div class="col-lg-4">
															<form:textarea path="crmContactGroup.closerIds" id="crmContactGroup_closerIds" rows="8" class="form-control"/>
														</div>
													</div> 
												</div>
												<div class="line">					                            
													<div class="form-row">
														<div class="col-lg-4">
															<label for="crmContactGroup_salesRepIds" class="col-form-label">
																<fmt:message key="salesRep"/> Ids
															</label>
														</div>
														<div class="col-lg-4">
															<form:textarea path="crmContactGroup.salesRepIds" class="form-control" rows="8" id="crmContactGroup_salesRepIds"/>
														</div>
													</div>
												</div>
												<div class="line">
													<div class="form-row">
														<div class="col-lg-4">
															<label for="crmContactGroup_dialingGroup" class="col-form-label">    
																<fmt:message key="dialingGroup" />
															</label>
														</div>
														<div class="col-lg-4">
															<label class="custom-control custom-checkbox">
											      				<form:checkbox path="crmContactGroup.dialingGroup" id="crmContactGroup_dialingGroup" class="custom-control-input"/>
											      				<span class="custom-control-indicator"></span>
											      			</label>
															
														</div>
													</div>
					                            </div>
					                            <div class="line">
						                            <div class="form-row">
														<div class="col-lg-4">
															<label for="crmContactGroup_dialingMessage" class="col-form-label">
																<fmt:message key="dialingMessage"/>
															</label>
														</div>
														<div class="col-lg-4">
															<form:textarea path="crmContactGroup.dialingMessage" id="crmContactGroup_dialingMessage" rows="10" class="form-control" />
														</div>
						                            </div>
												</div>
												<div class="line">
													<div class="form-row">
														<div class="col-lg-4">
															<label for="timezoneList" class="col-form-label">   
																Timezones
															</label>
														</div>
														<div class="col-lg-4">
															<label class="custom-control custom-checkbox">
											      				<input type="checkbox" id="HSTcheckbox" class="custom-control-input"/>
											      				<span class="custom-control-indicator"></span>
											      				<span class="custom-control-description">HST</span>
											      			</label>
															<label class="custom-control custom-checkbox">
											      				<input type="checkbox" id="AKSTcheckbox" class="custom-control-input"/> AKST
											      				<span class="custom-control-indicator"></span>
											      			</label>
											      			<label class="custom-control custom-checkbox">
											      				<input type="checkbox" id="PSTcheckbox" class="custom-control-input"/> PST
											      				<span class="custom-control-indicator"></span>
											      			</label>
											      			<label class="custom-control custom-checkbox">
											      				<input type="checkbox" id="MSTcheckbox" class="custom-control-input"/> MST
											      				<span class="custom-control-indicator"></span>
											      			</label>
											      			<label class="custom-control custom-checkbox">
											      				<input type="checkbox" id="CSTcheckbox" class="custom-control-input"/> CST
											      				<span class="custom-control-indicator"></span>
											      			</label>
															<label class="custom-control custom-checkbox">
											      				<input type="checkbox" id="ESTcheckbox" class="custom-control-input"/> EST
											      				<span class="custom-control-indicator"></span>
											      			</label>
															<form:input path="crmContactGroup.timezones" type="hidden" name="timezoneList" id="timezoneList"/>
														</div>
													</div>
					                            </div> 
											</div>
										</c:if>
									</c:if>
				                 </div>
			                </div>
			                <div class="footer">
								<div class="form-row">
									<div class="col-12">
										<c:if test="${crmContactGroupForm.newCrmContactGroup}"> 
			                        			<a class="btn btn-default"><spring:message code="add"/></a>
										</c:if>
										<c:if test="${!crmContactGroupForm.newCrmContactGroup}">
											<a class="btn btn-default"><spring:message code="update"/></a>
									  	</c:if>
									  	<a class="btn btn-default">Cancel</a>
									</div>
								</div>
				            </div>
		                </div>
	                </div>
                </div>
			</form:form>
		</sec:authorize>
	</tiles:putAttribute>
	<tiles:putAttribute name="js">
	</tiles:putAttribute>
</tiles:insertDefinition>