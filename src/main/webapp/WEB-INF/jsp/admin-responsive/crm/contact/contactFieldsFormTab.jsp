<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="/WEB-INF/tlds/spring.tld" prefix="spring" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ taglib uri="http://www.springframework.org/security/tags" prefix="sec" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ taglib uri="/WEB-INF/tlds/custom.tld" prefix="wj" %>

<c:if test="${fn:length(crmContactForm.crmContact.crmContactFields) gt 0}">
	<div id="tab_4" class="tab-pane"> 
		<c:forEach items="${crmContactForm.crmContact.crmContactFields}" var="field" varStatus="status">
			<div class="line">
				<div class="form-row">
					<div class="col-label">
						<label for="" class="col-form-label">
							${crmContactForm.crmContact.crmContactFields[status.index].fieldName}
						</label>
					</div>
					<div class="col-lg-4">
						<c:if test="${field.fieldType == 1 or field.fieldType == 7 }">
							<form:input path="crmContact.crmContactFields[${status.index}].fieldValue" class="form-control"/>
						</c:if>
						<c:if test="${field.fieldType == 2}">
							<form:textarea path="crmContact.crmContactFields[${status.index}].fieldValue" class="form-control" rows="8"/>
						</c:if>
						<c:if test="${field.fieldType == 3}">
							<c:forTokens items="${field.options}" delims="," var="option">
								<label class="custom-control custom-checkbox">
									<form:checkbox path="crmContact.crmContactFields[${status.index}].valueList" value="${fn:trim(option)}" label="${option}" class="custom-control-input"/>
									<span class="custom-control-indicator"></span>
								</label>
							</c:forTokens>
						</c:if>
						<c:if test="${field.fieldType == 4}">
							<c:forTokens items="${field.options}" delims="," var="option">
								<label class="custom-control custom-radio">
									<form:radiobutton path="crmContact.crmContactFields[${status.index}].fieldValue"  value="${fn:trim(option)}" label="${option}" class="custom-control-input"/>
									<span class="custom-control-indicator"></span>
								</label>
							</c:forTokens>
						</c:if>
						<c:if test="${field.fieldType == 5}" >
							<form:select path="crmContact.crmContactFields[${status.index}].fieldValue" class="custom-select">
								<form:option value="" label="Please Select"></form:option>
								<c:forTokens items="${field.options}" delims="," var="option">
									<form:option value="${fn:trim(option)}" label="${option}"></form:option>
								</c:forTokens>
							</form:select>
						</c:if>
					</div>
				</div>
			</div> 
		</c:forEach>
	</div> 
</c:if>