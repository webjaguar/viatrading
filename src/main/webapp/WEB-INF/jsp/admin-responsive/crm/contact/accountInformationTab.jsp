<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="/WEB-INF/tlds/spring.tld" prefix="spring" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ taglib uri="http://www.springframework.org/security/tags" prefix="sec" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ taglib uri="/WEB-INF/tlds/custom.tld" prefix="wj" %>

<div id="tab_1" class="tab-pane active">
	<div class="line">  
		<div class="form-row">
			<div class="col-label">
				<label class="col-form-label">  
					<fmt:message key="createdBy" />
				</label>
			</div>
			<div class="col-lg-4">
				<label class="text"><c:out value="${crmContactForm.crmContact.createdBy}"/></label>	
			</div>
		</div>
	</div>
	<div class="line">
		<div class="form-row">
			<div class="col-label">
				<label for="numEmails" class="col-form-label">  
					<fmt:message key="crmAccountName" />
				</label>
			</div>
			<div class="col-lg-4">
				<c:choose>
					<c:when test="${crmAccountForm.crmAccount.id == 1}">
						<form:input path="crmContact.accountName" id="formAccountName" class="form-control" maxlength="255" disabled="true" />
					</c:when>
					<c:otherwise>
						<form:input path="crmContact.accountName" id="formAccountName" class="form-control" maxlength="255" />
					</c:otherwise>
				</c:choose>
			</div>
		</div>
	</div>
	<div class="line">
		<div class="form-row">
			<div class="col-label">
				<label for="crmContact_accessUserId" class="col-form-label">   
					<span class="fa fa-question-circle" data-toggle="tooltip" title="Assign this account to a user. To manage users goto: More-->Access Privilege."></span>
					<fmt:message key="assignedTo" />
				</label>
			</div>
			<div class="col-lg-4">
				<form:select path="crmContact.accessUserId" class="custom-select" id="crmContact_accessUserId">
					<form:option value="" label="Select"></form:option>
					<c:forEach items="${accessUsers}" var="accessUser">
						<form:option value="${accessUser.id}" label="${accessUser.username}"></form:option>
					</c:forEach>
				</form:select>
			</div>
		</div>
	</div>
	<div class="line">
		<div class="form-row">
			<div class="col-label">
				<label for="crmContact_firstName" class="col-form-label">
					<span class="requiredField"> 
						<fmt:message key="firstName" />
					</span>
				</label>
			</div>
			<div class="col-lg-4">
				<form:input path="crmContact.firstName" class="form-control" maxlength="255" id="crmContact_firstName"/>
			</div>
			
			<c:if test="${not crmContactForm.newCrmContact}">
				<div class="col-lg-2">
					<a class="btn btn-default btn-flat" href="crmTaskForm.jhtm?accountId=${crmContactForm.crmContact.accountId}&contactId=${crmContactForm.crmContact.id}"><fmt:message key="addTask" /></a>
				</div>
			</c:if>
		</div>
	</div>
	<div class="line">
		<div class="form-row">
			<div class="col-label">
				<label for="crmContact_lastName" class="col-form-label">
					<fmt:message key="lastName" />
				</label>
			</div>
			<div class="col-lg-4">
				<form:input path="crmContact.lastName" class="form-control" maxlength="255" id="crmContact_lastName"/>
			</div>
		</div>
	</div>
	<div class="line">
		<div class="form-row">
			<div class="col-label">
				<label for="crmContact_title" class="col-form-label">
					<fmt:message key="title" />
				</label>
			</div>
			<div class="col-lg-4">
				<form:input path="crmContact.title" class="form-control" maxlength="255" id="crmContact_title"/>
			</div>
		</div>
	</div>
	<div class="line">
		<div class="form-row">
			<div class="col-label">
				<label for="crmContact_email1" class="col-form-label"> 
					<fmt:message key="email" /> 1 
				</label>
			</div>
			<div class="col-lg-4">
				<form:input path="crmContact.email1" class="form-control" maxlength="255" id="crmContact_email1"/>
			</div>
			<c:if test="${not crmContactForm.newCrmContact}">
				<div class="col-lg-2">
					<a class="btn btn-default btn-flat" href="email.jhtm?contactId=${crmContactForm.crmContact.id}"><fmt:message key="sendEmail" /></a>
				</div>
			</c:if>
		</div>
	</div>
	<div class="line">
		<div class="form-row">
			<div class="col-label">
				<label for="crmContact_email2" class="col-form-label"> 
					<fmt:message key="email" /> 2 
				</label>
			</div>
			<div class="col-lg-4">
				<form:input path="crmContact.email2" class="form-control" id="crmContact_email2" maxlength="255"/>
			</div>
		</div> 
	</div>
	<div class="line">
		<div class="form-row">
			<div class="col-label">
				<label for="crmContact_website" class="col-form-label"> 
					<fmt:message key="website" /> 
				</label>
			</div>
			<div class="col-lg-4">
				<form:input path="crmContact.website" class="form-control" id="crmContact_website" maxlength="255"/>
			</div>
		</div> 
	</div>                    
	<div class="line">
		<div class="form-row">
			<div class="col-label">
				<label for="crmContact_accessUserId" class="col-form-label">   
					<span class="fa fa-question-circle" data-toggle="tooltip" title="The Email address will be excluded from Mass Email Module."></span>
					<fmt:message key="unsubscribe" />
				</label>
			</div>
			<div class="col-lg-4">
				<label class="custom-control custom-checkbox">
					<form:checkbox path="crmContact.unsubscribe" class="custom-control-input"/>
					<span class="custom-control-indicator"></span>
				</label>
			</div>
		</div> 
	</div>
	<div class="line">
		<div class="form-row">
			<div class="col-label">
				<label for="crmContact_phone1" class="col-form-label"> 
					<fmt:message key="phone" /> 1 
				</label>
			</div>
			<div class="col-lg-4">
				<form:input path="crmContact.phone1" class="form-control" id="crmContact_phone1" maxlength="255"/>
			</div>
		</div> 
	</div>
	<div class="line">
		<div class="form-row">
			<div class="col-label">
				<label for="crmContact_phone2" class="col-form-label"> 
					<fmt:message key="phone" /> 2 
				</label>
			</div>
			<div class="col-lg-4">
				<form:input path="crmContact.phone2" class="form-control" id="crmContact_phone2" maxlength="255"/>
			</div>
		</div> 
	</div>
	<div class="line">
		<div class="form-row">
			<div class="col-label">
				<label for="crmContact_fax" class="col-form-label">
					<fmt:message key="fax" />
				</label>
			</div> 
			<div class="col-lg-4">
				<form:input path="crmContact.fax" class="form-control" id="crmContact_fax" maxlength="255"/>
			</div>
		</div>
	</div>
</div> 