<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="/WEB-INF/tlds/spring.tld" prefix="spring" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ taglib uri="http://www.springframework.org/security/tags" prefix="sec" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ taglib uri="/WEB-INF/tlds/custom.tld" prefix="wj" %>

<div id="tab_2" class="tab-pane">
	<div class="line">
		<div class="form-row">
			<div class="col-label">
				<label for="crmContact_contactType" class="col-form-label">
					<span class="fa fa-question-circle" data-toggle="tooltip" title="Select the type of contact for this contact. To manage the available types of contacts goto: SiteInfo-->Site Configuration-->CRM Configuration."></span>
					<fmt:message key="crmContactType" />
				</label>
			</div>
			<div class="col-lg-4">
				<form:select path="crmContact.contactType" class="custom-select" id="crmContact_contactType">
					<form:option value="" label=""></form:option>
					<c:forTokens items="${industry}" delims="," var="ind" >
						<form:option value="${ind}">${ind}</form:option>
					</c:forTokens>
				</form:select>
			</div>
		</div>
	</div>
	<div class="line">
		<div class="form-row">
			<div class="col-label">
				<label for="crmContact_rating" class="col-form-label">
					<span class="fa fa-question-circle" data-toggle="tooltip" title="Select the rating for this contact. To manage the available ratings goto: SiteInfo-->Site Configuration-->CRM Configuration."></span>
					<fmt:message key="crmRating" />
				</label>
			</div>
			<div class="col-lg-4">
				<form:select path="crmContact.rating" class="custom-select" id="crmContact_rating">
					<form:option value="" label=""></form:option>
					<c:forTokens items="${rating}" delims="," var="rate" >
						<form:option value="${rate}">${rate}</form:option>
					</c:forTokens>
				</form:select> 
			</div>
		</div>
	</div>
	<div class="line">
		<div class="form-row">
			<div class="col-label">
				<label for="crmContact_shortDesc" class="col-form-label">
					<fmt:message key="shortDesc" />
				</label>
			</div>
			<div class="col-lg-4">
				<form:textarea path="crmContact.shortDesc" id="crmContact_shortDesc" rows="8" class="form-control"/> 
			</div>
		</div>
	</div>
	<div class="line">
		<div class="form-row">
			<div class="col-label">
				<label for="crmContact_description" class="col-form-label">
					<fmt:message key="description" />
				</label>
			</div>
			<div class="col-lg-4">
				<form:textarea path="crmContact.description" id="crmContact_description" rows="8" class="form-control"/> 
			</div>
		</div>
	</div>
	<div class="line">
		<div class="form-row">
			<div class="col-label">
				<label for="crmContact_leadSource" class="col-form-label">
					<fmt:message key="leadSource" />
				</label>
			</div>
			<div class="col-lg-4">
				<form:select path="crmContact.leadSource" class="custom-select" id="crmContact_leadSource">
					<form:option value="" label=""></form:option>
					<c:forTokens items="${siteConfig['CRM_LEAD_SOURCE'].value}" delims="," var="leadsource" varStatus="status">
						<form:option value="${leadsource}"><c:out value="${leadsource}"/></form:option>
					</c:forTokens>
				</form:select>
			</div>
		</div>
	</div>
	<div class="line">
		<div class="form-row">
			<div class="col-label">
				<label for="crmContact_department" class="col-form-label">
					<fmt:message key="department" />
				</label>
			</div>
			<div class="col-lg-4">
				<form:textarea path="crmContact.department" id="crmContact_department" rows="8" maxlength="255" class="form-control"/> 
			</div>
		</div>
	</div>
</div>