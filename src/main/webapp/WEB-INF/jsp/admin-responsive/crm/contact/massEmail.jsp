<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="/WEB-INF/tlds/spring.tld" prefix="spring" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ taglib uri="http://www.springframework.org/security/tags" prefix="sec" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ taglib uri="/WEB-INF/tlds/custom.tld" prefix="wj" %>

<tiles:insertDefinition name="admin.responsive-template.crm.contact" flush="true">
	<tiles:putAttribute name="topbar" value="/WEB-INF/jsp/admin-responsive/common/topbar.jsp?tab=crmContactMassEmail">
	</tiles:putAttribute>
	<tiles:putAttribute name="content" type="string"> 
 		<sec:authorize ifAnyGranted="ROLE_AEM,ROLE_ADMIN_${prop_site_id},ROLE_EMAILMANAGER_CREATE">  
			<!-- Begin: Content -->
			<section id="content" class="animated fadeIn"> 
			    <form:form commandName="form" >
					<input type="hidden" name="_backend_user" value="<sec:authentication property="principal.username"/>" />
					<input type="hidden" name="_session_id" value="${pageContext.session.id}" />
					<input type="hidden" id="send" value="true">
				 	
				 	<!-- Error Message -->
				  	<c:if test="${message != null}">
						<div class="message"><fmt:message key="${message}"><fmt:param value="${form.numEmails}"/></fmt:message> </div>
				  	</c:if>
				  	<spring:hasBindErrors name="form">
						<div class="error"><fmt:message key='form.hasErrors'/></div>
				  	</spring:hasBindErrors>
				  	<c:if test="${pointError != null}">
						<div class="message"><fmt:message key="${pointError}"><fmt:param value="${form.numEmails}" /><fmt:param value="${massEmailPoint}" /></fmt:message></div>
				  	</c:if>
					  
				 	<div class="panel panel-default">
			         	<ul class="nav panel-tabs panel-tabs-border">
			                <li class="active">
			                    <a href="javascript:void(0);" data-toggle="tab" data-target="#tab_1">Mass Email</a>
			                </li>
			            </ul>
			            
			            <!-- Body -->
	                    <div class="panel-body">
			                <div class="tab-content">
			                    <div id="tab_1" class="tab-pane active"> 
			                        <div class="form-horizontal">
			                        	<div class="form-group">
			                                <label for="numEmails" class="col-sm-3 col-md-3 col-lg-3 control-label">  
			                                	Number of Emails to send: 
			                                </label>
			                                <label for="" class="col-sm-9 col-md-8 col-lg-8 control-label" style="text-align:left;">  
			                                	<c:out value="${form.numEmails}"/>  
			                                </label>
			                            </div> 
			                            
										<c:set var="fromErrors"><form:errors path="from"/></c:set> 
			                       		<div class="form-group <c:if test="${not empty fromErrors}"> has-error</c:if>"">
			                                <label for="from" class="col-sm-3 col-md-3 col-lg-3 control-label">  
		                                		<fmt:message key="from"/>:
			                                </label>
			                                <label for="" class="col-sm-9 col-md-8 col-lg-8 control-label" style="text-align:left;"> 
			                                	<!-- input field -->
			                                	<c:out value="${form.from}"/> 
											  	<form:errors path="from" cssClass="help-block" delimiter=", "/> 
											    <!-- end input field -->  
			                                </label>
			                            </div>
			                            
			                            <c:set var="subjectErrors"><form:errors path="subject"/></c:set> 
			                       		<div class="form-group <c:if test="${not empty subjectErrors}"> has-error</c:if>"">
			                                <label for="subject" class="col-sm-3 col-md-3 col-lg-3 control-label">  
		                                		<fmt:message key="subject"/>:
			                                </label>
			                                <div class="col-sm-9 col-md-8 col-lg-8">
			                                	<!-- input field -->
			                                	<form:input id="subject" path="subject" class="form-control" maxlength="200" htmlEscape="true"/>
								  			  	<c:forEach var="message" items="${messages}">
								    		  		<input type="text" disabled style="display:none;" id="subject${message.messageId}" name="subject" value="<c:out value="${message.subject}"/>" class="form-control" maxlength="50">
								  			  	</c:forEach>
			                                	<form:errors path="subject" cssClass="help-block"/> 
											    <!-- end input field -->  
			                                </div>
			                            </div> 
			                            
			                            <c:set var="htmlErrors"><form:errors path="html"/></c:set> 
			                            <div class="form-group <c:if test="${not empty htmlErrors}"> has-error</c:if>"">
			                                <label for="html" class="col-sm-3 col-md-3 col-lg-3 control-label">   
			                                	Html: 
			                                </label>
			                               <label for="" class="col-sm-9 col-md-8 col-lg-8 control-label" style="text-align:left;"> 
			                                	<form:checkbox path="html" onchange="showHtmlEditor(this)"/>
								  			  	<c:forEach var="message" items="${messages}">
								    		  		<input type="hidden" id="htmlID${message.messageId}" value="<c:out value="${message.html}"/>" />
								  			  	</c:forEach>
									            <form:errors path="html" cssClass="help-block"/>
			                                </label>
			                            </div> 
			                            
			                            <c:set var="messageErrors"><form:errors path="message"/></c:set> 
			                            <div class="form-group <c:if test="${not empty messageErrors}"> has-error</c:if>"">
			                                <label for="message" class="col-sm-3 col-md-3 col-lg-3 control-label"> 
			                                	<fmt:message key="message"/>:
			                                </label>
			                                <div class="col-sm-9 col-md-8 col-lg-8" id="preLoadMessage">
			                                	<form:textarea id="message" path="message" rows="15" class="form-control" htmlEscape="true"/>
									   			<div name="htmlLink" class="htmlEditorLink" style="text-align:right;display:none;" id="htmlEditor_link"><a href="#" onClick="loadEditor('message',-1)" class="htmleditor">- <fmt:message key="htmlEditor" /> -</a></div>
							  			  		<c:forEach var="message" items="${messages}">
								    				<textarea disabled style="display:none;" id="message${message.messageId}" name="message" rows="15" class="form-control"><c:out value="${message.message}"/></textarea>
								    				<div name="htmlLink" class="htmlEditorLink" style="text-align:right;display:none;" id="htmlEditor_link${message.messageId}"><a href="#" onClick="loadEditor('message${message.messageId}',${message.messageId})" class="htmleditor">- <fmt:message key="htmlEditor" /> -</a></div>
							  			  		</c:forEach>
									           	<form:errors path="message" cssClass="help-block"/>
			                                </div>
			                            </div>
			                            
			                            <div class="form-group">
			                                <label for="groupMessage" class="col-sm-3 col-md-3 col-lg-3 control-label"> 
			                                	<div class="requiredField">  
				                                	<fmt:message key="groupMessage"/>:
			                                	</div>
			                                </label>
			                                <div class="col-sm-9 col-md-8 col-lg-8">
			                                	<select id="groupSelected" onChange="getSiteMessage(this.value)" class="form-control">
											        <option value="0"><fmt:message key="chooseGroupMessage"/></option>
											        <option value="-1"> All Messages</option>
											  		<c:forEach var="group" items="${messageGroups}">
												    	<option value="${group.id}" <c:if test="${group.id == siteConfig['SITE_MESSAGE_GROUP_ID_FOR_MASS_EMAIL'].value}">selected</c:if>><c:out value="${group.name}"/></option>
											  		</c:forEach>
											  	</select>
			                                </div>
			                            </div> 
			                            
			                            <div class="form-group" id="messageSelect" style="display:none;">
			                                <label for="message" class="col-sm-3 col-md-3 col-lg-3 control-label"> 
			                                	<div class="requiredField">  
				                                	<fmt:message key="message"/>:
			                                	</div>
			                                </label>
			                                <div class="col-sm-9 col-md-8 col-lg-8" id="siteMessageSelect">
			                                	 
			                                </div>
			                            </div> 
			                            
			                            <div class="form-group">
			                                <label for="scheduleTime" class="col-sm-3 col-md-3 col-lg-3 control-label">  
			                                	<img class="toolTipImg" title="Note::Setup a time in future." src="../graphics/question.gif" />
                               					&nbsp;<fmt:message key="scheduleToSend" />: 
			                                </label>
			                                <div class="col-sm-9 col-md-8 col-lg-8">
			                                	<div class="input-group"> 
												 	<span class="input-group-addon">
					                                    <span class="fa fa-calendar"></span>
					                                </span> 
					                                
					                                <form:input path="scheduleTime" class="form-control" maxlength="20" data-provide="datetimepicker" data-date-format="MM/DD/YYYY hh:mm a"  />	
					                                
					                                <!-- <input name="scheduleTime" type="text" class="form-control" id="scheduleTime" 
															value="<fmt:formatDate type="date" pattern="MM/dd/yyyy hh:mm a" timeZone="${siteConfig['SITE_TIME_ZONE'].value}" value='${form.scheduleTime}'/>"
															data-provide="datetimepicker" data-date-format="MM/DD/YYYY hh:mm a"  />	
													 		
	                                	 			<form:input path="scheduleTime" class="gui-input"  id="scheduleTime" maxlength="20" readonly="true"/> 	
										   			 
										   			<input name="scheduleTime" id="scheduleTime" type="text" size="18" maxlength="20" readonly="true" value="<fmt:formatDate type="date" pattern="MM/dd/yyyy[hh:mm a]" timeZone="${siteConfig['SITE_TIME_ZONE'].value}" value='${form.scheduleTime}'/>" />
										   			 --> 
									   			</div> 
									   			<br>
									   			<div align="left" class="button" >
								   			  		<c:if test="${!scheduleDone and form.numEmails > 0}" >
									   			    	<input type="submit" name="__send" value="<fmt:message key="schedule" />" onclick="return checkMessage(2)" class="btn btn-default"/>
									   			  	</c:if>    
									   			</div>
									            <form:errors path="scheduleTime" cssClass="help-block" delimiter=", "/>
									             
			                                </div>
			                            </div> 
			                            
			                        </div> 
			                   	</div>
				            </div>
			            </div> 
			            
			            <!-- Footer -->
			            <div class="panel-footer">
			                <div class="clearfix">
			                    <div class="pull-right">
			                    	<c:if test="${!scheduleDone and form.numEmails > 0}" >
									    <input type="submit" name="__sendnow" value="<fmt:message key="send" /> Now" onclick="return checkMessage(1)" class="btn btn-primary mr-10"/>
								  	</c:if>
								  	<c:if test="${scheduleDone or form.numEmails < 1}" >
									    <input type="submit" name="__back" value="<fmt:message key="back" />" class="btn btn-default mr-10"/>
								  	</c:if> 
								</div>
							</div>
						</div>
			   		</div>
			     </form:form>
			</section>
		</sec:authorize>
	</tiles:putAttribute>
</tiles:insertDefinition>

<script type="text/javascript" src="<c:url value="/FCKeditor/fckeditor.js"/>"></script>
<script language="JavaScript" type="text/JavaScript">
<!--
  
$(function(){
	if(document.getElementById('groupSelected').value > 0){
		 getSiteMessage(document.getElementById('groupSelected').value);
	 }
});
	
function getSiteMessage(groupId) {
	jQuery.ajax({
		method: 'post', 
		url: '../config/show-ajax-groupSiteMessage.jhtm?groupId='+groupId,
        dataType: "html",
        success: function (response) {
        	document.getElementById("siteMessageSelect").innerHTML = response;
        	document.getElementById('messageSelect').style.display="block";
        },
        error: function (xhr, status) {
        	console.log("getSiteMessage ERROR");

        },
	}); 
}
//-->
</script>

<script language="JavaScript"> 
<!--
function trimString(str){
    var returnVal = "";
    for(var i = 0; i < str.length; i++){
      if(str.charAt(i) != ' '){
        returnVal += str.charAt(i)
      }
    }
    return returnVal;
}
function chooseMessage(el) {
   	messageList = document.getElementsByName('message');
   	var htmlEditorLinks = $('#preLoadMessage .htmlEditorLink');
   	    htmlEditorLinks.each(function(el) { 
			$(this).css("display","none");
	});
   	var iFrames = $('iframe');
   		iFrames.each(function(el) {
			$(this).css("display","none");
	});
   	
	for(var i=0; i<messageList.length; i++) {
		messageList[i].disabled=true;
		messageList[i].style.display="none";
	}
   	subjectList = document.getElementsByName('subject');
	for(var i=0; i<subjectList.length; i++) {
		subjectList[i].disabled=true;
		subjectList[i].style.display="none";
	}
   	document.getElementById('message'+el.value).disabled=false;
       document.getElementById('message'+el.value).style.display="block"; 
   	document.getElementById('subject'+el.value).disabled=false;
       document.getElementById('subject'+el.value).style.display="block"; 
       //console.log("Line 226 " +document.getElementById('htmlID'+el.value).value +", Value is " + el.value);
    if (document.getElementById('htmlID'+el.value).value == 'true') { 
    	$('#html1').prop("checked", true);
    	$('#htmlEditor_link'+el.value).css("display", "block");
	} else {
	    $('#html1').prop("checked",false);
	}              
}		
function checkMessage(type) {
	  if (document.getElementById('send').value == 'true') {
      var message = document.getElementById('message' + document.getElementById('messageSelected').value);
      var subject = document.getElementById('subject' + document.getElementById('messageSelected').value);
      if (trimString(subject.value) == "") {
        alert("Please put a subject");
        subject.focus();
    	return false;
      }
      if (trimString(message.value) == "") {
        alert("Please put a message");
        message.focus();
    	return false;
      }
    }
    if ( type == 1)
    {
      return confirm("Are you sure to send email(s) now?");
    } else {
      return confirm("Are you sure to send email(s) on the provided schedule time?");
    }
    
}
function loadEditor(el,id) {
  var oFCKeditor = new FCKeditor(el);
  oFCKeditor.BasePath = "<c:url value="/FCKeditor/"/>";
  oFCKeditor.Config["CustomConfigurationsPath"] = '<c:url value="/admin/javascript/fckconfig.js"/>';
  oFCKeditor.ToolbarSet = 'Html';
  oFCKeditor.Width = 650;
  oFCKeditor.Height = 400;
  oFCKeditor.ReplaceTextarea();
  document.getElementById('htmlEditor_link' + id).style.display="none";
}  
function showHtmlEditor(el) {
  if ( $('messageSelected').value == '') {
    if ( el.checked) {
    	$('htmlEditor_link').css("display", "block");
    } else {
        $('htmlEditor_link').css("display", "none");
    }
  }  else {
    //
  } 
  
}
//-->
</script>