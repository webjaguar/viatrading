<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/security/tags" prefix="sec" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ taglib uri="/WEB-INF/tlds/custom.tld" prefix="wj" %>
 
<tiles:insertDefinition name="admin.responsive-template.crm.contactField" flush="true">
	<tiles:putAttribute name="css">
		<style>
			#grid .field-globalName{
				width: 80px;
			}
			#grid .field-fieldName{
				width: 300px;
			}
			#grid .field-enabled{
				width: 80px;
				text-align: center;
			}
		</style>
	</tiles:putAttribute>
	<tiles:putAttribute name="content" type="string">
	    <sec:authorize ifAnyGranted="ROLE_AEM,ROLE_ADMIN_${prop_site_id},ROLE_CRM_FIELD"> 
		    <form action="crmContactFieldList.jhtm" method="post" id="list" name="list_form"> 
				<div class="page-banner">
					<div class="breadcrumb"><span><a href="${_contextpath}/admin/catalog/dashboard.jhtm">Home</a></span><i class="sli-arrow-right"></i><span><a href="${_contextpath}/admin/crm">CRM</a></span><i class="sli-arrow-right"></i><span>Fields</span></div>
				</div>
				<div class="page-head">
					<div class="row justify-content-between">
						<div class="col-sm-8 title">
							<h3>Fields</h3>
						</div>
						<div class="col-sm-4 actions">
							<div class="dropdown">
								<button type="button" class="btn-outline dropdown-toggle active btn btn-primary" data-toggle="dropdown">
		                             <i class="sli-settings"></i> Actions
		                         </button>
		                         <div class="dropdown-menu dropdown-menu-right">
						  	  		<sec:authorize ifAnyGranted="ROLE_AEM,ROLE_ADMIN_${prop_site_id},ROLE_CRM">
						  	  			<a class="dropdown-item"><fmt:message key="add"/></a>
							  	  	</sec:authorize>
		                         </div>
		                    </div>
		                    <div class="dropdown dropdown-setting">
								<button type="button" class="btn-outline dropdown-toggle active btn btn-primary" data-toggle="dropdown">
		                             <i class="sli-screen-desktop"></i> Display
		                         </button>	                         
								<div class="dropdown-menu dropdown-menu-right">
									<div class="form-group">
				                    		<div class="option-header">Rows</div>
										<select class="custom-select form-control" name="size" onchange="document.getElementById('page').value=1;submit();">
											<c:forTokens items="10,25,50" delims="," var="current">
									  	  		<option value="${current}" <c:if test="${current == model.fieldList.pageSize}">selected</c:if>>${current}</option>
								  			</c:forTokens>
										</select>
									</div>
								</div>
		                    </div>
						</div>
					</div>
				</div>
				<div class="page-body">
					<div class="grid-stage">
						<c:if test="${model.fieldList.nrOfElements > 0}">
							<div class="page-stat">
								<fmt:message key="showing">
									<fmt:param value="${model.fieldList.firstElementOnPage + 1}" />
									<fmt:param value="${model.fieldList.lastElementOnPage + 1}" />
									<fmt:param value="${model.fieldList.nrOfElements}" />
								</fmt:message>
							</div> 
						</c:if>
					    <table id="grid"></table>
					    <div class="footer">
			                <div class="float-right">
								<c:if test="${model.fieldList.firstPage}">
							 		<span class="previous"><i class="sli-arrow-left" style="font-size: 0.7rem"></i> Previous</span>
								</c:if>
								<c:if test="${not model.fieldList.firstPage}">
									<a href="<c:url value="crmContactFieldList.jhtm"><c:param name="page" value="${model.fieldList.page}"/><c:param name="size" value="${model.fieldList.pageSize}"/></c:url>">
										<span class="previous"><i class="sli-arrow-left" style="font-size: 0.7rem"></i> Previous</span>
									</a>
								</c:if>	
								<span class="status"><c:out value="${model.fieldList.page+1}" /> of <c:out value="${model.fieldList.pageCount}" /></span>
								<c:if test="${model.fieldList.lastPage}">
  									<span class="next">Next <i class="sli-arrow-right" style="font-size: 0.7rem"></i></span>
 								</c:if>
								<c:if test="${not model.fieldList.lastPage}">
  									<a href="<c:url value="crmContactFieldList.jhtm"><c:param name="page" value="${model.fieldList.page+2}"/><c:param name="size" value="${model.fieldList.pageSize}"/></c:url>">
										<span class="next">Next <i class="sli-arrow-right" style="font-size: 0.7rem"></i></span>
									</a>
 								</c:if>
							</div>
						</div>
					</div>
				</div>
                <input type="hidden" id="page" name="page"/>
			    <input type="hidden" id="sort" name="sort" value="${crmContactSearch.sort}" />
		    </form>
	    </sec:authorize>
	</tiles:putAttribute>
	<tiles:putAttribute name="js">
		<!-- DGRID TABLE SCRIPT MUST BE AT BOTTOM OF THE PAGE -->
		<script src="${_contextpath}/admin-responsive-static/assets/plugins/dojo/dojo.js" data-dojo-config="async: true"></script>
		<script>
			//Table Data Sanitization
			var data = [];
			<c:forEach items="${model.fieldList.pageList}" var="field" varStatus="status">
				data.push({
					offset: '${status.count + model.fieldList.firstElementOnPage}.',
					id: "${field.id}",
					globalName: "${wj:escapeJS(field.globalName)}",
					fieldName: "${wj:escapeJS(field.fieldName)}",
					<c:if test="${field.enabled}">
						enabled: true,
					</c:if>
				});
			</c:forEach>
			
			require(["dgrid/List", "dgrid/OnDemandGrid", "dgrid/Selection", 'dgrid/Selector', "dgrid/Keyboard", "dgrid/Editor", "dijit/form/CheckBox", 
					"dijit/form/TimeTextBox", "dijit/form/Select", "dojo/data/ObjectStore", "dijit/form/FilteringSelect",
					'dijit/form/DropDownButton', 'dijit/DropDownMenu', 'dijit/MenuItem',
					"dojo/_base/declare", "dojo/_base/lang", "dojo/store/Memory", "dojo/store/Observable", "dgrid/test/data/createSyncStore", 
					'dgrid/extensions/ColumnHider', 'dgrid/extensions/ColumnReorder', 'dgrid/extensions/ColumnResizer', 'dojo/domReady!',
				], function(List, OnDemandGrid, Selection, Selector, Keyboard, Editor, CheckBox, TimeTextBox, Select, ObjectStore, FilteringSelect,
						DropDownButton, DropDownMenu, MenuItem,
						declare, lang, LegacyMemory, LegacyObservable, createSyncStore, ColumnHider, ColumnReorder, ColumnResizer){
	
				//Column Definitions
				var columns = {
					offset: {
						label: '#',
						get: function(object){
							return object.offset;
						},
						sortable: false
					},
					globalName: {
						label: '<fmt:message key="field"/>',
						renderCell: function(object){
							var link = document.createElement("a");
							link.text = object.globalName;
							link.href = 'crmContactFieldForm.jhtm?id='+object.id;
							return link;
						},
						sortable: false
					},
					fieldName: {
						label: '<fmt:message key="name"/>',
						get: function(object){
							return object.fieldName;
						},
						sortable: false,
					},
					enabled: {
						label: '<fmt:message key="enabled"/>',
						renderCell: function(object, data, td, options){
							var div = document.createElement("div");
							if(object.enabled){
								var i = document.createElement("i");
								i.className = 'sli-check';
								div.appendChild(i);
							}
							return div;
						},
						sortable: false
					}
				};
				
				var grid = new (declare([OnDemandGrid, Selection, Selector, Keyboard, Editor, ColumnReorder, ColumnResizer]))({
					collection: createSyncStore({ data: data }),
					selectionMode: "none",
					allowSelectAll: true,
					columns: columns,
				},'grid');
				
				grid.on('dgrid-refresh-complete', function(event) {
					$('#grid-hider-menu').appendTo('#column-hider');
				});
				
				grid.startup();
			});
		</script>
	</tiles:putAttribute>
</tiles:insertDefinition>
 