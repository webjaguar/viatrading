<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="/WEB-INF/tlds/spring.tld" prefix="spring" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ taglib uri="http://www.springframework.org/security/tags" prefix="sec" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ taglib uri="/WEB-INF/tlds/custom.tld" prefix="wj" %>

<div id="tab_3" class="tab-pane">
	<div class="row">
		<div class="col-lg-6">
			<div class="line">
				<div class="form-row">
					<div class="col-label">
						<label for="crmContact_street" class="col-form-label">
							<fmt:message key="street" />
						</label>
					</div>
					<div class="col-lg-8">
						<form:textarea path="crmContact.street" class="form-control" id="crmContact_street" rows="5"/> 
					</div>
				</div>
			</div>
			<div class="line">
				<div class="form-row">
					<div class="col-label">
						<label for="crmContact_city" class="col-form-label">
							<fmt:message key="billingCity" />
						</label>
					</div>
					<div class="col-lg-8">
						<form:input path="crmContact.city" class="form-control" maxlength="255" id="crmContact_city"/>
					</div>
				</div>
			</div>
			<div class="line">
				<div class="form-row">
					<div class="col-label">
						<label for="crmAccount_billingStateProvince" class="col-form-label">
							<fmt:message key="stateProvince" />
						</label>
					</div>
					<div class="col-lg-8">
						<form:input path="crmContact.stateProvince" maxlength="255" class="form-control" id="crmContact_stateProvince"/> 
					</div>
				</div>
			</div>
			<div class="line">
				<div class="form-row">
					<div class="col-label">
						<label for="crmContact_zip" class="col-form-label">
							<fmt:message key="zipCode" />
						</label>
					</div>
					<div class="col-lg-8">
						<form:input path="crmContact.zip" maxlength="255" class="form-control" id="crmContact_zip"/> 
					</div>
				</div>
			</div>
			<div class="line">
				<div class="form-row">
					<div class="col-label">
						<label for="crmContact_country" class="col-form-label">
							<fmt:message key="country" />
						</label>
					</div>
					<div class="col-lg-8">
						<form:select id="crmContact_country" path="crmContact.country" class="custom-select">
							<form:option value="" label="Please Select"/>
							<form:options items="${countries}" itemValue="code" itemLabel="name"/>
						</form:select>
					</div>
				</div>
			</div>
			<div class="line">
				<div class="form-row">
					<div class="col-label">
						<label for="crmContact_addressType" class="col-form-label">
							<fmt:message key="addressType" />
						</label>
					</div>
					<div class="col-lg-8">
						<label class="custom-control custom-radio">
							<form:radiobutton path="crmContact.addressType" class="custom-control-input" value="Residential"/>
		                    	<span class="custom-control-indicator"></span>
		                    	<span class="custom-control-description"><fmt:message key="residential" /></span>
						</label>
	                    <label class="custom-control custom-radio">
							<form:radiobutton path="crmContact.addressType" class="custom-control-input" value="Commercial"/>
		                    	<span class="custom-control-indicator"></span>
		                    	<span class="custom-control-description"><fmt:message key="commercial" /></span>
						</label>
					</div>
				</div>
			</div>
		</div>
		<div class="col-lg-6">
			<div class="line">
				<div class="form-row">
					<div class="col-label">
						<label for="crmContact_otherStreet" class="col-form-label">
							<fmt:message key="otherStreet" />
						</label>
					</div>
					<div class="col-lg-8">
						<form:textarea path="crmContact.otherStreet" class="form-control" id="crmContact_otherStreet" rows="5"/> 
					</div>
				</div>
			</div>
			<div class="line">
				<div class="form-row">
					<div class=col-label>
						<label for="crmContact_otherCity" class="col-form-label">
							<fmt:message key="otherCity" />
						</label>
					</div>
					<div class="col-lg-8">
						<input type="text" class="form-control" id="crmContact_otherCity" name="crmContact.otherCity">
					</div>
				</div>
			</div>
			<div class="line">
				<div class="form-row">
					<div class="col-label">
						<label for="crmContact_otherStateProvince" class="col-form-label">
							<fmt:message key="otherState" />
						</label>
					</div>
					<div class="col-lg-8">
						<form:input path="crmContact.otherStateProvince" class="form-control" id="crmContact_otherStateProvince" maxlength="255"/> 
					</div>
				</div>
			</div>
			<div class="line">
				<div class="form-row">
					<div class="col-label">
						<label for="crmContact_otherZip" class="col-form-label">
							<fmt:message key="otherZip" />
						</label>
					</div>
					<div class="col-lg-8">
						<form:input path="crmContact.otherZip" class="form-control" id="crmContact_otherZip" maxlength="255"/> 
					</div>
				</div>
			</div>
			<div class="line">
				<div class="form-row">
					<div class="col-label">
						<label for="crmContact_otherCountry" class="col-form-label">
							<fmt:message key="otherCountry" />
						</label>
					</div>
					<div class="col-lg-8">
						<form:select id="crmContact_otherCountry" path="crmContact.otherCountry" class="custom-select">
							<form:option value="" label="Please Select"/>
							<form:options items="${countries}" itemValue="code" itemLabel="name"/>
						</form:select>
					</div>
				</div>
			</div>
			<div class="line">
				<div class="form-row">
					<div class="col-label">
						<label for="crmContact_otherAddressType" class="col-form-label">
							<fmt:message key="otherAddressType" />
						</label>
					</div>
					<div class="col-lg-8">
						<label class="custom-control custom-radio">
							<form:radiobutton path="crmContact.otherAddressType" class="custom-control-input" value="Residential"/>
		                    	<span class="custom-control-indicator"></span>
		                    	<span class="custom-control-description"><fmt:message key="residential" /></span>
						</label>
	                    <label class="custom-control custom-radio">
							<form:radiobutton path="crmContact.otherAddressType" class="custom-control-input" value="Commercial"/>
		                    	<span class="custom-control-indicator"></span>
		                    	<span class="custom-control-description"><fmt:message key="commercial" /></span>
						</label>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>