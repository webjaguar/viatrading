<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ taglib uri="http://www.springframework.org/security/tags" prefix="sec" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ taglib uri="/WEB-INF/tlds/spring.tld" prefix="spring" %>
<%@ taglib uri="/WEB-INF/tlds/custom.tld" prefix="wj" %>

<tiles:insertDefinition name="admin.responsive-template.report.salesRepDetail" flush="true">
	<tiles:putAttribute name="css">
		<style>
			#grid .field-salesRepName{
				width: 150px;
			}
			#grid .field-merchandize{
				text-align: right;
			}
			#grid .field-shippingCost{
				text-align: right;
			}
			#grid .field-ccFee{
				text-align: right;
				width: 150px;
			}
			#grid .field-tax{
				text-align: right;
			}
			#grid .field-grandTotal{
				text-align: right;
			}
			#grid .field-numOrder{
				text-align: right;
			}
			#grid .field-uniqueUser{
				text-align: right;
			}
			#grid .field-newUser{
				text-align: right;
			}
			#grid .field-newUserActive{
				text-align: right;
				width: 150px;
			}
			#grid .field-avgPerOrder{
				text-align: right;
				width: 150px;
			}
			#grid .field-avgPerCustomer{
				text-align: right;
				width: 150px;
			}
		</style>
	</tiles:putAttribute>
	<tiles:putAttribute name="content" type="string">
		<c:if test="${gSiteConfig['gREPORT']}">
			<sec:authorize ifAnyGranted="ROLE_AEM,ROLE_ADMIN_${prop_site_id},ROLE_REPORT_SALESREP_DAILY">
				<form action="salesRepDetailReport.jhtm" id="list" method="post">
					<div class="page-banner">
						<div class="breadcrumb"><span><a href="${_contextpath}/admin/catalog/dashboard.jhtm">Home</a></span><i class="sli-arrow-right"></i><span><a href="../reports"><fmt:message key="reports" /></a></span><i class="sli-arrow-right"></i><span>SalesRep Overview</span></div>
					</div>
					<div class="page-head">
						<div class="row justify-content-between">
							<div class="col-sm-8 title">
								<h3>SalesRep Overview</h3>
							</div>
						</div>
					</div>
					<div class="page-body">
						<table id="grid"></table>
					</div>
					<input type="hidden" id="sort" name="sort" value="${salesRepDetailFilter.sort}" />
				</form>
			</sec:authorize>
		</c:if>
	</tiles:putAttribute>
	<tiles:putAttribute name="js">
		<!-- DGRID TABLE SCRIPT MUST BE AT BOTTOM OF THE PAGE -->
		<script src="${_contextpath}/admin-responsive-static/assets/plugins/dojo/dojo.js" data-dojo-config="async: true"></script>
		<script>
			//Table Data Sanitization
			var data = [];
			<c:set var="merchandize" value="0"/>
			<c:set var="shippingCost" value="0"/>
			<c:set var="ccFee" value="0"/>
			<c:set var="tax" value="0"/>
			<c:set var="grandTotal" value="0"/>
			<c:set var="numOrder" value="0"/>
			<c:set var="uniqueUser" value="0"/>
			<c:set var="newUser" value="0"/>
			<c:set var="newUserActive" value="0"/>
			<c:set var="avgPerOrder" value="0"/>
			<c:set var="avgPerCustomer" value="0"/>
			<c:forEach items="${model.salesRepDetailReportsList}" var="salesRepDetailReport" varStatus="status">
		    		data.push({
		    			salesRepName: '<c:out value="${model.salesRepMap[salesRepDetailReport.salesRepId].name}"/>',
		    			merchandize: decodeHTML('<fmt:message key="${siteConfig[\'CURRENCY\'].value}" />') + '<fmt:formatNumber value="${salesRepDetailReport.merchandize}" pattern="#,##0.00" />',
		    			shippingCost: decodeHTML('<fmt:message key="${siteConfig[\'CURRENCY\'].value}" />') + '<fmt:formatNumber value="${salesRepDetailReport.shippingCost}" pattern="#,##0.00" />',
		    			ccFee: decodeHTML('<fmt:message key="${siteConfig[\'CURRENCY\'].value}" />') + '<fmt:formatNumber value="${salesRepDetailReport.ccFee}" pattern="#,##0.00" />',
		    			tax: decodeHTML('<fmt:message key="${siteConfig[\'CURRENCY\'].value}" />') + '<fmt:formatNumber value="${salesRepDetailReport.tax}" pattern="#,##0.00" />',
		    			grandTotal: decodeHTML('<fmt:message key="${siteConfig[\'CURRENCY\'].value}" />') + '<fmt:formatNumber value="${salesRepDetailReport.grandTotal}" pattern="#,##0.00" />',
		    			numOrder: '<c:out value="${salesRepDetailReport.numOrder}"/>',
		    			uniqueUser: '<c:out value="${salesRepDetailReport.uniqueUser}"/>',
		    			newUser: '<c:out value="${salesRepDetailReport.newUser}"/>',
		    			newUserActive: '<c:out value="${salesRepDetailReport.newUserActive}"/>',
		    			avgPerOrder: decodeHTML('<fmt:message key="${siteConfig[\'CURRENCY\'].value}" />') + '<fmt:formatNumber value="${salesRepDetailReport.avgGrandTotal}" pattern="#,##0.00" />',
		    			avgPerCustomer: decodeHTML('<fmt:message key="${siteConfig[\'CURRENCY\'].value}" />') + '<fmt:formatNumber value="${salesRepDetailReport.avgPerCustomer}" pattern="#,##0.00" />',
				});
		    		<c:set var="merchandize" value="${salesRepDetailReport.merchandize + merchandize}" />
	    			<c:set var="shippingCost" value="${salesRepDetailReport.shippingCost + shippingCost}" />
    				<c:set var="ccFee" value="${salesRepDetailReport.ccFee + ccFee}" />
				<c:set var="tax" value="${salesRepDetailReport.tax + tax}" />
				<c:set var="grandTotal" value="${salesRepDetailReport.grandTotal + grandTotal}" />
				<c:set var="numOrder" value="${salesRepDetailReport.numOrder + numOrder}" />
				<c:set var="uniqueUser" value="${salesRepDetailReport.uniqueUser + uniqueUser}" />
				<c:set var="newUser" value="${salesRepDetailReport.newUser + newUser}" />
				<c:set var="newUserActive" value="${salesRepDetailReport.newUserActive + newUserActive}" />
				<c:set var="avgPerOrder" value="${salesRepDetailReport.avgGrandTotal + avgPerOrder}" />
				<c:set var="avgPerCustomer" value="${salesRepDetailReport.avgPerCustomer + avgPerCustomer}" />
			</c:forEach>
	
			//Columns Definitions
			require(["dgrid/List", "dgrid/OnDemandGrid", "dgrid/Selection", "dgrid/Keyboard", "dgrid/Editor", "dijit/form/CheckBox", 
					"dijit/form/TimeTextBox", "dijit/form/Select", "dojo/data/ObjectStore", "dijit/form/FilteringSelect",
					'dijit/form/DropDownButton', 'dijit/DropDownMenu', 'dijit/MenuItem',
					"dojo/_base/declare", "dojo/_base/lang", "dojo/store/Memory", "dojo/store/Observable", "dgrid/test/data/createSyncStore", 
					'dgrid/extensions/ColumnReorder', 'dgrid/extensions/ColumnResizer', 'summary/SummaryRow',
				], function (List, OnDemandGrid, Selection, Keyboard, Editor, CheckBox, TimeTextBox, Select, ObjectStore, FilteringSelect,
							DropDownButton, DropDownMenu, MenuItem,
							declare, lang, LegacyMemory, LegacyObservable, createSyncStore, ColumnReorder, ColumnResizer, SummaryRow) {
				var columns = {
					salesRepName: {
						label: '<fmt:message key="salesRep" />',
						get: function(object){
							return object.salesRepName;
						},
						sortable: false,
					},
					merchandize: {
						label: '<fmt:message key="merchandise" />',
						get: function(object){
							return object.merchandize;
						},
						sortable: false,
					},
					shippingCost: {
						label: '<fmt:message key="shipping" />',
						get: function(object){
							return object.shippingCost;
						},
						sortable: false,
					},
					ccFee: {
						label: '<fmt:message key="ccFee" />',
						get: function(object){
							return object.ccFee;
						},
						sortable: false,
					},
					tax: {
						label: '<fmt:message key="tax" />',
						get: function(object){
							return object.tax;
						},
						sortable: false,
					},
					grandTotal: {
						label: '<fmt:message key="grandTotal" />',
						get: function(object){
							return object.grandTotal;
						},
						sortable: false,
					},
					numOrder: {
						label: '<fmt:message key="orders" />',
						get: function(object){
							return object.numOrder;
						},
						sortable: false,
					},
					uniqueUser: {
						label: '<fmt:message key="uniqueUser" />',
						get: function(object){
							return object.uniqueUser;
						},
						sortable: false,
					},
					newUser: {
						label: '<fmt:message key="newUser" />',
						get: function(object){
							return object.newUser;
						},
						sortable: false,
					},
					newUserActive: {
						label: '<fmt:message key="newUserActive" />',
						get: function(object){
							return object.newUserActive;
						},
						sortable: false,
					},
					avgPerOrder: {
						label: '<fmt:message key="averagePerOrder" />',
						get: function(object){
							return object.avgPerOrder;
						},
						sortable: false,
					},
					avgPerCustomer: {
						label: '<fmt:message key="averagePerCustomer" />',
						get: function(object){
							return object.avgPerCustomer;
						},
						sortable: false,
					},
				};
				
				var grid = new (declare([OnDemandGrid, Selection, Keyboard, Editor, ColumnReorder, ColumnResizer, SummaryRow]))({
					collection: createSyncStore({ data: data }),
					selectionMode: "none",
					allowSelectAll: true,
					columns: columns
				},'grid');
				
				var totals = {
					salesRepName: '<fmt:message key="total" />',
					merchandize: decodeHTML('<fmt:message key="${siteConfig[\'CURRENCY\'].value}" />') + '<fmt:formatNumber value="${merchandize}" pattern="#,##0.00" />',
					shippingCost: decodeHTML('<fmt:message key="${siteConfig[\'CURRENCY\'].value}" />') + '<fmt:formatNumber value="${shippingCost}" pattern="#,##0.00" />',
					ccFee: decodeHTML('<fmt:message key="${siteConfig[\'CURRENCY\'].value}" />') + '<fmt:formatNumber value="${ccFee}" pattern="#,##0.00" />',
					tax: decodeHTML('<fmt:message key="${siteConfig[\'CURRENCY\'].value}" />') + '<fmt:formatNumber value="${tax}" pattern="#,##0.00" />',
					grandTotal: decodeHTML('<fmt:message key="${siteConfig[\'CURRENCY\'].value}" />') + '<fmt:formatNumber value="${grandTotal}" pattern="#,##0.00" />',
					numOrder: '<fmt:formatNumber value="${numOrder}" pattern="#,##0" />',
					uniqueUser: '<fmt:formatNumber value="${uniqueUser}" pattern="#,##0" />',
					newUser: '<fmt:formatNumber value="${newUser}" pattern="#,##0" />',
					newUserActive: '<fmt:formatNumber value="${newUserActive}" pattern="#,##0" />',
					avgPerOrder: decodeHTML('<fmt:message key="${siteConfig[\'CURRENCY\'].value}" />') + '<fmt:formatNumber value="${avgPerOrder}" pattern="#,##0.00" />',
					avgPerCustomer: decodeHTML('<fmt:message key="${siteConfig[\'CURRENCY\'].value}" />') + '<fmt:formatNumber value="${avgPerCustomer}" pattern="#,##0.00" />',
				};

				grid.set('summary', totals);
				
				grid.on('dgrid-refresh-complete', function(event) {
					$('#grid-hider-menu').appendTo('#column-hider');
				});
				
				grid.startup();
			});
		</script>
	</tiles:putAttribute>
</tiles:insertDefinition>