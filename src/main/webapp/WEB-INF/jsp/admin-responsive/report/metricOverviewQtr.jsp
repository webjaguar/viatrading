<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ taglib uri="http://www.springframework.org/security/tags" prefix="sec" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ taglib uri="/WEB-INF/tlds/spring.tld" prefix="spring" %>
<%@ taglib uri="/WEB-INF/tlds/custom.tld" prefix="wj" %>

<tiles:insertDefinition name="admin.responsive-template.report.metricOverview" flush="true">
	<tiles:putAttribute name="css">
		<style>
			#grid .dgrid-cell{
				text-align:right;
				width: 70px;
			}
			#grid .field-name{
				width: 180px;
				text-align: left;
			}
			#grid td.field-name{
				font-weight: 600;
			}
			#grid td.field-Q1{
				font-weight: 600;
			}
			#grid td.field-Q2{
				font-weight: 600;
			}
			#grid td.field-Q3{
				font-weight: 600;
			}
			#grid td.field-Q4{
				font-weight: 600;
			}
			#grid td.field-total{
				font-weight: 600;
			}
			#grid .dgrid-row.summary .dgrid-cell{
			 	background-color: #f5f5f5;
			}
		</style>
	</tiles:putAttribute>
	<tiles:putAttribute name="content" type="string">
		<c:if test="${gSiteConfig['gREPORT']}">
			<sec:authorize ifAnyGranted="ROLE_AEM,ROLE_ADMIN_${prop_site_id},ROLE_REPORT_CUSTOMER_METRIC">
				<div class="page-banner">
					<div class="breadcrumb"><span><a href="${_contextpath}/admin/catalog/dashboard.jhtm">Home</a></span><i class="sli-arrow-right"></i><span><a href="../reports"><fmt:message key="reports" /></a></span><i class="sli-arrow-right"></i><span>Customer Metric Overview</span></div>
				</div>
				<div class="page-head">
					<div class="row justify-content-between">
						<div class="col-sm-8 title">
							<h3>Customer Metric Overview</h3>
						</div>
						<div class="col-sm-4 actions">
							<div class="dropdown">
								<button type="button" class="btn-outline dropdown-toggle active btn btn-primary" onclick="$('#search-dialog').modal('show')">
		                            <i class="sli-magnifier"></i>
		                         </button>
							</div>
						</div>
					</div>
				</div>
				<div class="page-body">
					<div class="grid-stage grid-stage-sm">
						<table id="grid"></table>
					</div>
				</div>
				<c:import url="/WEB-INF/jsp/admin-responsive/report/options/metricOverviewSearchOptions.jsp" />
			</sec:authorize>
		</c:if>
	</tiles:putAttribute>
	<tiles:putAttribute name="js">
		<!-- DGRID TABLE SCRIPT MUST BE AT BOTTOM OF THE PAGE -->
		<script src="${_contextpath}/admin-responsive-static/assets/plugins/dojo/dojo.js" data-dojo-config="async: true"></script>
		<script>
			//Table Data Sanitization
			var data = [];
			
			data.push({
    				name: '<fmt:message key="newRegistration" />',
		    		<c:forEach items="${model.reports}" var="reports" varStatus="status">
			    		<c:if test="${status.index % 3 == 0}">
						<c:set var="qtrNewRegistrations" value="0"/>
					</c:if>
					col_${status.index}: '<fmt:formatNumber value="${reports.newRegistrations}" pattern="#,##0"/>',
					<c:set var="qtrNewRegistrations" value="${reports.newRegistrations+qtrNewRegistrations}"/>
					<c:set var="totalNewRegistrations" value="${reports.newRegistrations+totalNewRegistrations}"/>
					<c:if test="${status.index % 3 == 2}">
						<c:set var="quarterIndex" value="${quarterIndex + 1}" />				 
						col_${status.index}_last: '<fmt:formatNumber value="${qtrNewRegistrations}" pattern="#,##0" />',
						<c:if test="${status.index == 2}"><c:set value="${qtrNewRegistrations}" var="q1NewRegistrations" /></c:if>
						<c:if test="${status.index == 5}"><c:set value="${qtrNewRegistrations}" var="q2NewRegistrations" /></c:if>	
						<c:if test="${status.index == 8}"><c:set value="${qtrNewRegistrations}" var="q3NewRegistrations" /></c:if>	
						<c:if test="${status.index == 11}"><c:set value="${qtrNewRegistrations}" var="q4NewRegistrations" /></c:if>			
					</c:if>
		    		</c:forEach>
				total: '<fmt:formatNumber value="${totalNewRegistrations}" pattern="#,##0"/>',
			});
			
			data.push({
				name: '<fmt:message key="newActivation" />',
				<c:forEach items="${model.reports}" var="reports" varStatus="status">
					<c:if test="${status.index % 3 == 0}">
						<c:set var="qtrNewActivations" value="0"/>
					</c:if>
					col_${status.index}: '<fmt:formatNumber value="${reports.newActivations}" pattern="#,##0"/>',
					<c:set var="qtrNewActivations" value="${reports.newActivations+qtrNewActivations}"/>
					<c:set var="totalNewActivations" value="${reports.newActivations+totalNewActivations}"/>
					<c:if test="${status.index % 3 == 2}">
						<c:set var="quarterIndex" value="${quarterIndex + 1}" />				 
						col_${status.index}_last: '<fmt:formatNumber value="${qtrNewActivations}" pattern="#,##0" />',		  
						<c:if test="${status.index == 2}"><c:set value="${qtrNewActivations}" var="q1NewActivations" /></c:if>
						<c:if test="${status.index == 5}"><c:set value="${qtrNewActivations}" var="q2NewActivations" /></c:if>	
						<c:if test="${status.index == 8}"><c:set value="${qtrNewActivations}" var="q3NewActivations" /></c:if>	
						<c:if test="${status.index == 11}"><c:set value="${qtrNewActivations}" var="q4NewActivations" /></c:if>				
					</c:if>
		    		</c:forEach>
				total: '<fmt:formatNumber value="${totalNewActivations}" pattern="#,##0"/>',
			});
			
			data.push({
				name: '<fmt:message key="closeRate" />',
		    		<c:forEach items="${model.reports}" var="reports" varStatus="status">
					<c:choose>
						<c:when test="${reports.newRegistrations!=0}">
							col_${status.index}: '<fmt:formatNumber value="${(reports.newActivations/reports.newRegistrations)*100}" pattern="#,##0.00" />%',
						</c:when>
						<c:otherwise>col_${status.index}: '0%',</c:otherwise>
					</c:choose>
					<c:if test="${status.index % 3 == 2}">
						<c:set var="quarterIndex" value="${quarterIndex + 1}" />
						col_${status.index}_last: 
						<c:if test="${status.index == 2}" >
							<c:if test="${q1NewRegistrations !=0}">'<fmt:formatNumber value="${(q1NewActivations/q1NewRegistrations)*100 }" pattern="#,##0.00" />%'</c:if>
							<c:if test="${q1NewRegistrations ==0}">'0%'</c:if>
						</c:if>				 
						<c:if test="${status.index == 5}" >
							<c:if test="${q2NewRegistrations !=0}">'<fmt:formatNumber value="${(q2NewActivations/q2NewRegistrations)*100}" pattern="#,##0.00"/>%'</c:if>
							<c:if test="${q2NewRegistrations ==0}">'0%'</c:if>
						</c:if>				  
						<c:if test="${status.index == 8}" >
							<c:if test="${q3NewRegistrations !=0}">'<fmt:formatNumber value="${(q3NewActivations/q3NewRegistrations)*100}" pattern="#,##0.00"/>%'</c:if>
							<c:if test="${q3NewRegistrations ==0}">'0%'</c:if>
						</c:if>				  
						<c:if test="${status.index == 11}" >
							<c:if test="${q4NewRegistrations !=0}">'<fmt:formatNumber value="${(q4NewActivations/q4NewRegistrations)*100}" pattern="#,##0.00"/>%'</c:if>
							<c:if test="${q4NewRegistrations ==0}">'0%'</c:if>
						</c:if>,
					</c:if>
		    		</c:forEach>
				<c:choose>
					<c:when test="${totalNewRegistrations!=0}">
						total: '<fmt:formatNumber value="${(totalNewActivations/totalNewRegistrations)*100}" pattern="#,##0.00"/>%',
					</c:when>
					<c:otherwise>
						total: '0%',
					</c:otherwise>
				</c:choose>
				summary: true,
			});
			
			data.push({
				name: '<fmt:message key="totalSpend" />',
				<c:forEach items="${model.reports}" var="reports" varStatus="status">
					<c:if test="${status.index % 3 == 0}">
						<c:set var="qtrTotalSpend" value="0"/>
					</c:if>
					col_${status.index}: '$<fmt:formatNumber value="${reports.totalSpend}" pattern="#,##0"/>',
					<c:set var="qtrTotalSpend" value="${reports.totalSpend+qtrTotalSpend}"/>
					<c:set var="totalTotalSpend" value="${reports.totalSpend+totalTotalSpend}"/>
					<c:if test="${status.index % 3 == 2}">
						<c:set var="quarterIndex" value="${quarterIndex + 1}" />				 
						col_${status.index}_last: '$<fmt:formatNumber value="${qtrTotalSpend}" pattern="#,##0" />',				  
						<c:if test="${status.index == 2}"><c:set value="${qtrTotalSpend}" var="q1TotalSpend" /></c:if>
						<c:if test="${status.index == 5}"><c:set value="${qtrTotalSpend}" var="q2TotalSpend" /></c:if>	
						<c:if test="${status.index == 8}"><c:set value="${qtrTotalSpend}" var="q3TotalSpend" /></c:if>	
						<c:if test="${status.index == 11}"><c:set value="${qtrTotalSpend}" var="q4TotalSpend" /></c:if>				
					</c:if>
		    		</c:forEach>
				total: '$<fmt:formatNumber value="${totalTotalSpend}" pattern="#,##0"/>',
			});
			
			data.push({
				name: '<fmt:message key="totalRegistration" />',
				<c:forEach items="${model.reports}" var="reports" varStatus="status">
					<c:if test="${status.index % 3 == 0}">
						<c:set var="totalRegistrations" value="0"/>
					</c:if> 
					col_${status.index}: '<fmt:formatNumber value="${reports.totalRegistration}" pattern="#,##0"/>',
					<c:set var="totalRegistrations" value="${reports.totalRegistration}"/>
					<c:if test="${status.index % 3 == 2}">
						<c:set var="quarterIndex" value="${quarterIndex + 1}" />				 
						col_${status.index}_last: '<fmt:formatNumber value="${totalRegistrations}" pattern="#,##0" />',				  
						<c:if test="${status.index == 2}" ><c:set value="${totalRegistrations}" var="q1totalRegistrations" /></c:if>
						<c:if test="${status.index == 5}" ><c:set value="${totalRegistrations}" var="q2totalRegistrations" /></c:if>	
						<c:if test="${status.index == 8}" ><c:set value="${totalRegistrations}" var="q3totalRegistrations" /></c:if>	
						<c:if test="${status.index == 11}" ><c:set value="${totalRegistrations}" var="q4totalRegistrations" /></c:if>				
					</c:if>	
		    		</c:forEach>
				<c:choose>
					<c:when test="${model.totalRegistrations == 0}">
						total: '<fmt:formatNumber value="${totalRegistrations}" pattern="#,##0"/>',
					</c:when>
					<c:otherwise>
						total: '<fmt:formatNumber value="${model.totalRegistrations}" pattern="#,##0"/>',
					</c:otherwise>
				</c:choose>
			});
			
			data.push({
				name: '<fmt:message key="totalActivation" />',
				<c:forEach items="${model.reports}" var="reports" varStatus="status">			  
					<c:if test="${status.index % 3 == 0}">
						<c:set var="totalActivations" value="0"/>
					</c:if> 
					col_${status.index}: '<fmt:formatNumber value="${reports.totalActivations}" pattern="#,##0"/>',
					<c:set var="totalActivations" value="${reports.totalActivations}"/>
					<c:if test="${status.index % 3 == 2}">
						<c:set var="quarterIndex" value="${quarterIndex + 1}" />				 
						col_${status.index}_last: '<fmt:formatNumber value="${totalActivations}" pattern="#,##0" />',				  			  
						<c:if test="${status.index == 2}"><c:set value="${totalActivations}" var="q1totalActivations" /></c:if>
						<c:if test="${status.index == 5}"><c:set value="${totalActivations}" var="q2totalActivations" /></c:if>	
						<c:if test="${status.index == 8}"><c:set value="${totalActivations}" var="q3totalActivations" /></c:if>	
						<c:if test="${status.index == 11}"><c:set value="${totalActivations}" var="q4totalActivations" /></c:if>					
					</c:if>
				</c:forEach>
				<c:choose>
					<c:when test="${model.totalRegistrations == 0}">
						total: '<fmt:formatNumber value="${totalActivations}" pattern="#,##0"/>',
					</c:when>
					<c:otherwise>
						total: '<fmt:formatNumber value="${model.totalActivations}" pattern="#,##0"/>',
					</c:otherwise>
				</c:choose>
			});
			
			data.push({
				name: '<fmt:message key="allTimeCloseRate" />',
				<c:forEach items="${model.reports}" var="reports" varStatus="status">			  
					<c:choose>
						<c:when test="${reports.totalRegistration!=0}">
							col_${status.index}: '<fmt:formatNumber value="${(reports.totalActivations/reports.totalRegistration)*100}" pattern="#,##0.00" />%',
						</c:when>
						<c:otherwise>
							col_${status.index}: '0%',</c:otherwise>
					</c:choose>
					<c:if test="${status.index % 3 == 2}">
						<c:set var="quarterIndex" value="${quarterIndex + 1}" />
						col_${status.index}_last: 
					  	<c:if test="${status.index == 2}" >
							<c:if test="${q1totalRegistrations !=0}">'<fmt:formatNumber value="${(q1totalActivations/q1totalRegistrations)*100 }" pattern="#,##0.00" />%'</c:if>
							<c:if test="${q1totalRegistrations ==0}">'0%</c:if>
						</c:if>				 
						<c:if test="${status.index == 5}" >
							<c:if test="${q2totalRegistrations !=0}">'<fmt:formatNumber value="${(q2totalActivations/q2totalRegistrations)*100}" pattern="#,##0.00"/>%'</c:if>
							<c:if test="${q2totalRegistrations ==0}">'0%'</c:if>
						</c:if>				  
						<c:if test="${status.index == 8}" >
							<c:if test="${q3totalRegistrations !=0}">'<fmt:formatNumber value="${(q3totalActivations/q3totalRegistrations)*100}" pattern="#,##0.00"/>%'</c:if>
							<c:if test="${q3totalRegistrations ==0}">'0%'</c:if>
						</c:if>				  
						<c:if test="${status.index == 11}" >
							<c:if test="${q4totalRegistrations !=0}">'<fmt:formatNumber value="${(q4totalActivations/q4totalRegistrations)*100}" pattern="#,##0.00"/>%'</c:if>
							<c:if test="${q4totalRegistrations ==0}">'0%'</c:if>
						</c:if>,
					</c:if>
				</c:forEach>
				<c:choose>
					<c:when test="${model.totalRegistrations!=0 or totalRegistrations!=0 }" >			  			   
						<c:if test="${model.totalRegistrations!=0}">
							total: '<fmt:formatNumber value="${(model.totalActivations/model.totalRegistrations)*100}" pattern="#,##0.00" />%',
						</c:if>
						<c:if test="${totalRegistrations!=0}">
							total: 'fmt:formatNumber value="${(totalActivations/totalRegistrations)*100}" pattern="#,##0.00" />%',
						</c:if>
					</c:when>	    	
					<c:otherwise>
						total: '0%',
					</c:otherwise>
				</c:choose>
				summary: true,
			});
			
			data.push({
				name: '<fmt:message key="tsRegistration" />',
		    		<c:forEach items="${model.reports}" var="reports" varStatus="status">
					<c:if test="${status.index % 3 == 0}">
						<c:set var="qtrtouchScreenRegistrations" value="0"/>
					</c:if> 
					col_${status.index}: '<fmt:formatNumber value="${reports.touchScreenRegistrations}" pattern="#,##0" />',
					<c:set var="qtrtouchScreenRegistrations" value="${reports.touchScreenRegistrations+qtrtouchScreenRegistrations}"/>
					<c:set var="totaltouchScreenRegistrations" value="${reports.touchScreenRegistrations+totaltouchScreenRegistrations}"/>
					<c:if test="${status.index % 3 == 2}">
						<c:set var="quarterIndex" value="${quarterIndex + 1}" />				 
						col_${status.index}_last: '<fmt:formatNumber value="${qtrtouchScreenRegistrations}" pattern="#,##0" />',				  
						<c:if test="${status.index == 2}"><c:set value="${qtrtouchScreenRegistrations}" var="q1touchScreenRegistrations" /></c:if>
						<c:if test="${status.index == 5}"><c:set value="${qtrtouchScreenRegistrations}" var="q2touchScreenRegistrations" /></c:if>	
						<c:if test="${status.index == 8}"><c:set value="${qtrtouchScreenRegistrations}" var="q3touchScreenRegistrations" /></c:if>	
						<c:if test="${status.index == 11}"><c:set value="${qtrtouchScreenRegistrations}" var="q4touchScreenRegistrations" /></c:if>				
					</c:if>
		    		</c:forEach>
				total: '<fmt:formatNumber value="${totaltouchScreenRegistrations}" pattern="#,##0" />',
			});
		    	
			data.push({
				name: '<fmt:message key="tsActivation" />',
				<c:forEach items="${model.reports}" var="reports" varStatus="status">			  
					<c:if test="${status.index % 3 == 0}">
						<c:set var="qtrtouchScreenActivations" value="0"/>
					</c:if>
					col_${status.index}: '<fmt:formatNumber value="${reports.touchScreenActivations}" pattern="#,##0" />',
					<c:set var="qtrtouchScreenActivations" value="${reports.touchScreenActivations+qtrtouchScreenActivations}"/>
					<c:set var="totaltouchScreenActivations" value="${reports.touchScreenActivations+totaltouchScreenActivations}"/>
					<c:if test="${status.index % 3 == 2}">
						<c:set var="quarterIndex" value="${quarterIndex + 1}" />				 
						col_${status.index}_last: '<fmt:formatNumber value="${qtrtouchScreenActivations}" pattern="#,##0" />',					  
						<c:if test="${status.index == 2}" ><c:set value="${qtrtouchScreenActivations}" var="q1touchScreenActivations" /></c:if>
						<c:if test="${status.index == 5}" ><c:set value="${qtrtouchScreenActivations}" var="q2touchScreenActivations" /></c:if>	
						<c:if test="${status.index == 8}" ><c:set value="${qtrtouchScreenActivations}" var="q3touchScreenActivations" /></c:if>	
						<c:if test="${status.index == 11}" ><c:set value="${qtrtouchScreenActivations}" var="q4touchScreenActivations" /></c:if>			
					</c:if>
				</c:forEach>
				total: '<fmt:formatNumber value="${totaltouchScreenActivations}" pattern="#,##0" />',
			});
			
			data.push({
				name: '<fmt:message key="closeRate" />',
				<c:forEach items="${model.reports}" var="reports" varStatus="status">
					<c:choose>
						<c:when test="${reports.touchScreenRegistrations!=0}">
							col_${status.index}: '<fmt:formatNumber value="${(reports.touchScreenActivations/reports.touchScreenRegistrations)*100}" pattern="#,##0.00" />%',
						</c:when>
						<c:otherwise>col_${status.index}: '0%',</c:otherwise>
					</c:choose>	
					<c:if test="${status.index % 3 == 2}">
						<c:set var="quarterIndex" value="${quarterIndex + 1}" />
						col_${status.index}_last: 
							<c:if test="${status.index == 2}">
								<c:if test="${q1touchScreenRegistrations !=0}">'<fmt:formatNumber value="${(q1touchScreenActivations/q1touchScreenRegistrations)*100 }" pattern="#,##0.00" />%'</c:if>
								<c:if test="${q1touchScreenRegistrations ==0}">'0%'</c:if>
							</c:if>
							<c:if test="${status.index == 5}">
								<c:if test="${q2touchScreenRegistrations !=0}">'<fmt:formatNumber value="${(q2touchScreenActivations/q2touchScreenRegistrations)*100}" pattern="#,##0.00"/>%'</c:if>
								<c:if test="${q2touchScreenRegistrations ==0}">'0%'</c:if>
							</c:if>
							<c:if test="${status.index == 8}" >
								<c:if test="${q3touchScreenRegistrations !=0}">'<fmt:formatNumber value="${(q3touchScreenActivations/q3touchScreenRegistrations)*100}" pattern="#,##0.00"/>%'</c:if>
								<c:if test="${q3touchScreenRegistrations ==0}">'0%'</c:if>
							</c:if>
							<c:if test="${status.index == 11}" >
								<c:if test="${q4touchScreenRegistrations !=0}">'<fmt:formatNumber value="${(q4touchScreenActivations/q4touchScreenRegistrations)*100}" pattern="#,##0.00"/>%'</c:if>
								<c:if test="${q4touchScreenRegistrations ==0}">'0%'</c:if>
							</c:if>,
					</c:if>
				</c:forEach>
				<c:choose>
					<c:when test="${totaltouchScreenRegistrations!=0}">
						total: '<fmt:formatNumber value="${(totaltouchScreenActivations/totaltouchScreenRegistrations)*100}" pattern="#,##0.00" />%',
					</c:when>
					<c:otherwise>
						total: '0%',
					</c:otherwise>
				</c:choose>
				summary: true,
			});
			
			data.push({
				name: '<fmt:message key="feRegistration" />',
		    		<c:forEach items="${model.reports}" var="reports" varStatus="status">
					<c:if test="${status.index % 3 == 0}">
						<c:set var="qtrfrontendRegistrations" value="0"/>
					</c:if>
					col_${status.index}: '<fmt:formatNumber value="${reports.frontendRegistrations}" pattern="#,##0" />',
					<c:set var="qtrfrontendRegistrations" value="${reports.frontendRegistrations+qtrfrontendRegistrations}"/>
					<c:set var="totalfrontendRegistrations" value="${reports.frontendRegistrations+totalfrontendRegistrations}"/>
					<c:if test="${status.index % 3 == 2}">
						<c:set var="quarterIndex" value="${quarterIndex + 1}" />				 
						col_${status.index}_last: '<fmt:formatNumber value="${qtrfrontendRegistrations}" pattern="#,##0" />',				  					  
						<c:if test="${status.index == 2}" ><c:set value="${qtrfrontendRegistrations}" var="q1frontendRegistrations" /></c:if>
						<c:if test="${status.index == 5}" ><c:set value="${qtrfrontendRegistrations}" var="q2frontendRegistrations" /></c:if>	
						<c:if test="${status.index == 8}" ><c:set value="${qtrfrontendRegistrations}" var="q3frontendRegistrations" /></c:if>	
						<c:if test="${status.index == 11}" ><c:set value="${qtrfrontendRegistrations}" var="q4frontendRegistrations" /></c:if>				
					</c:if>
		    		</c:forEach>
				total: '<fmt:formatNumber value="${totalfrontendRegistrations}" pattern="#,##0" />',
			});
			
			data.push({
				name: '<fmt:message key="feActivation" />',
				<c:forEach items="${model.reports}" var="reports" varStatus="status">			  
					<c:if test="${status.index % 3 == 0}">
						<c:set var="qtrfrontendActivations" value="0"/>
					</c:if>
					col_${status.index}: '<fmt:formatNumber value="${reports.frontendActivations}" pattern="#,##0" />',
					<c:set var="qtrfrontendActivations" value="${reports.frontendActivations+qtrfrontendActivations}"/>
					<c:set var="totalfrontendActivations" value="${reports.frontendActivations+totalfrontendActivations}"/>
					<c:if test="${status.index % 3 == 2}">
						<c:set var="quarterIndex" value="${quarterIndex + 1}" />				 
						col_${status.index}_last: '<fmt:formatNumber value="${qtrfrontendActivations}" pattern="#,##0" />',					  
						<c:if test="${status.index == 2}" ><c:set value="${qtrfrontendActivations}" var="q1frontendActivations" /></c:if>
						<c:if test="${status.index == 5}" ><c:set value="${qtrfrontendActivations}" var="q2frontendActivations" /></c:if>	
						<c:if test="${status.index == 8}" ><c:set value="${qtrfrontendActivations}" var="q3frontendActivations" /></c:if>	
						<c:if test="${status.index == 11}" ><c:set value="${qtrfrontendActivations}" var="q4frontendActivations" /></c:if>			
					</c:if>
				</c:forEach>
				total: '<fmt:formatNumber value="${totalfrontendActivations}" pattern="#,##0" />',
			});
			
			data.push({
				name: '<fmt:message key="closeRate" />',
				<c:forEach items="${model.reports}" var="reports" varStatus="status">
					<c:choose>
						<c:when test="${reports.frontendRegistrations!=0}">
							col_${status.index}: '<fmt:formatNumber value="${(reports.frontendActivations/reports.frontendRegistrations)*100}" pattern="#,##0.00" />%',
						</c:when>
						<c:otherwise>col_${status.index}: '0%',</c:otherwise>
					</c:choose>		
					<c:if test="${status.index % 3 == 2}">
						<c:set var="quarterIndex" value="${quarterIndex + 1}" />
						col_${status.index}_last: 
							<c:if test="${status.index == 2}">
								<c:if test="${q1frontendRegistrations !=0}">'<fmt:formatNumber value="${(q1frontendActivations/q1frontendRegistrations)*100 }" pattern="#,##0.00" />%'</c:if>
								<c:if test="${q1frontendRegistrations ==0}">'0%'</c:if>
							</c:if>
							<c:if test="${status.index == 5}">
								<c:if test="${q2frontendRegistrations !=0}">'<fmt:formatNumber value="${(q2frontendActivations/q2frontendRegistrations)*100}" pattern="#,##0.00"/>%'</c:if>
								<c:if test="${q2frontendRegistrations ==0}">'0%'</c:if>
							</c:if>
							<c:if test="${status.index == 8}">
								<c:if test="${q3frontendRegistrations !=0}">'<fmt:formatNumber value="${(q3frontendActivations/q3frontendRegistrations)*100}" pattern="#,##0.00"/>%'</c:if>
								<c:if test="${q3frontendRegistrations ==0}">'0%'</c:if>
							</c:if>
							<c:if test="${status.index == 11}">
								<c:if test="${q4frontendRegistrations !=0}">'<fmt:formatNumber value="${(q4frontendActivations/q4frontendRegistrations)*100}" pattern="#,##0.00"/>%'</c:if>
							<c:if test="${q4frontendRegistrations ==0}">'0%'</c:if>
					  </c:if>,
					</c:if>
				</c:forEach>
				<c:choose>
					<c:when test="${totalfrontendRegistrations!=0}">
						total: '<fmt:formatNumber value="${(totalfrontendActivations/totalfrontendRegistrations)*100}" pattern="#,##0.00" />%',
					</c:when>
					<c:otherwise>
						total: '0%',
					</c:otherwise>
				</c:choose>
				summary: true,
			});
			
			data.push({
				name: '<fmt:message key="lpLeads" />',
		    		<c:forEach items="${model.reports}" var="reports" varStatus="status">
					<c:if test="${status.index % 3 == 0}">
						<c:set var="qtrlandingPageLeads" value="0"/>
					</c:if>
					col_${status.index}: '<fmt:formatNumber value="${reports.landingPageLeads}" pattern="#,##0" />',
					<c:set var="qtrlandingPageLeads" value="${reports.landingPageLeads+qtrlandingPageLeads}"/>
					<c:set var="totallandingPageLeads" value="${reports.landingPageLeads+totallandingPageLeads}"/>
					<c:if test="${status.index % 3 == 2}">
						<c:set var="quarterIndex" value="${quarterIndex + 1}" />				 
						col_${status.index}_last: '<fmt:formatNumber value="${qtrlandingPageLeads}" pattern="#,##0" />',				  				  				  					  
						<c:if test="${status.index == 2}" ><c:set value="${qtrlandingPageLeads}" var="q1landingPageLeads" /></c:if>
						<c:if test="${status.index == 5}" ><c:set value="${qtrlandingPageLeads}" var="q2landingPageLeads" /></c:if>	
						<c:if test="${status.index == 8}" ><c:set value="${qtrlandingPageLeads}" var="q3landingPageLeads" /></c:if>	
						<c:if test="${status.index == 11}" ><c:set value="${qtrlandingPageLeads}" var="q4landingPageLeads" /></c:if>				
					</c:if>
		    		</c:forEach>
				total: '<fmt:formatNumber value="${totallandingPageLeads}" pattern="#,##0" />',
			});
			
			data.push({
				name: '<fmt:message key="lpRegistration" />',
		    		<c:forEach items="${model.reports}" var="reports" varStatus="status">
					<c:if test="${status.index % 3 == 0}">
						<c:set var="qtrlandingPageRegistrations" value="0"/>
					</c:if>
					col_${status.index}: '<fmt:formatNumber value="${reports.landingPageRegistrations}" pattern="#,##0" />',
					<c:set var="qtrlandingPageRegistrations" value="${reports.landingPageRegistrations+qtrlandingPageRegistrations}"/>
					<c:set var="totallandingPageRegistrations" value="${reports.landingPageRegistrations+totallandingPageRegistrations}"/>
					<c:if test="${status.index % 3 == 2}">
						<c:set var="quarterIndex" value="${quarterIndex + 1}" />				 
						col_${status.index}_last: '<fmt:formatNumber value="${qtrlandingPageRegistrations}" pattern="#,##0" />',				  				  				  					  
						<c:if test="${status.index == 2}"><c:set value="${qtrlandingPageRegistrations}" var="q1landingPageRegistrations" /></c:if>
						<c:if test="${status.index == 5}"><c:set value="${qtrlandingPageRegistrations}" var="q2landingPageRegistrations" /></c:if>	
						<c:if test="${status.index == 8}"><c:set value="${qtrlandingPageRegistrations}" var="q3landingPageRegistrations" /></c:if>	
						<c:if test="${status.index == 11}"><c:set value="${qtrlandingPageRegistrations}" var="q4landingPageRegistrations" /></c:if>				
					</c:if>
		    		</c:forEach>
				total: '<fmt:formatNumber value="${totallandingPageRegistrations}" pattern="#,##0" />',
			});
			
			data.push({
				name: '<fmt:message key="lpActivation" />',
		    		<c:forEach items="${model.reports}" var="reports" varStatus="status">
					<c:if test="${status.index % 3 == 0}">
						<c:set var="qtrlandingPageActivations" value="0"/>
					</c:if>
					col_${status.index}: '<fmt:formatNumber value="${reports.landingPageActivations}" pattern="#,##0" />',
					<c:set var="qtrlandingPageActivations" value="${reports.landingPageActivations+qtrlandingPageActivations}"/>
					<c:set var="totallandingPageActivations" value="${reports.landingPageActivations+totallandingPageActivations}"/>
					<c:if test="${status.index % 3 == 2}">
						<c:set var="quarterIndex" value="${quarterIndex + 1}" />				 
						col_${status.index}_last: '<fmt:formatNumber value="${qtrlandingPageActivations}" pattern="#,##0" />',				  				  				  				  					  
						<c:if test="${status.index == 2}"><c:set value="${qtrlandingPageActivations}" var="q1landingPageActivations" /></c:if>
						<c:if test="${status.index == 5}"><c:set value="${qtrlandingPageActivations}" var="q2landingPageActivations" /></c:if>	
						<c:if test="${status.index == 8}"><c:set value="${qtrlandingPageActivations}" var="q3landingPageActivations" /></c:if>	
						<c:if test="${status.index == 11}"><c:set value="${qtrlandingPageActivations}" var="q4landingPageActivations" /></c:if>				
					</c:if>
		    		</c:forEach>
				total: '<fmt:formatNumber value="${totallandingPageActivations}" pattern="#,##0" />',
			});
			
			data.push({
				name: '<fmt:message key="registrationCloseRate" />',
				<c:forEach items="${model.reports}" var="reports" varStatus="status">
					<c:choose>
						<c:when test="${reports.landingPageLeads!=0}">
							col_${status.index}: '<fmt:formatNumber value="${(reports.landingPageRegistrations/reports.landingPageLeads)*100}" pattern="#,##0.00" />%',
						</c:when>
						<c:otherwise>col_${status.index}: '0%',</c:otherwise>
					</c:choose>
					<c:if test="${status.index % 3 == 2}">
						<c:set var="quarterIndex" value="${quarterIndex + 1}" />
						col_${status.index}_last: 
							<c:if test="${status.index == 2}" >
								<c:if test="${q1landingPageLeads !=0}">'<fmt:formatNumber value="${(q1landingPageRegistrations/q1landingPageLeads)*100 }" pattern="#,##0.00" />%'</c:if>
								<c:if test="${q1landingPageLeads ==0}">'0%'</c:if>
							</c:if>
							<c:if test="${status.index == 5}" >
								<c:if test="${q2landingPageLeads !=0}">'<fmt:formatNumber value="${(q2landingPageRegistrations/q2landingPageLeads)*100}" pattern="#,##0.00"/>%'</c:if>
								<c:if test="${q2landingPageLeads ==0}">'0%'</c:if>
							</c:if>
							<c:if test="${status.index == 8}" >
								<c:if test="${q3landingPageLeads !=0}">'<fmt:formatNumber value="${(q3landingPageRegistrations/q3landingPageLeads)*100}" pattern="#,##0.00"/>%'</c:if>
								<c:if test="${q3landingPageLeads ==0}">'0%'</c:if>
							</c:if>
							<c:if test="${status.index == 11}" >
								<c:if test="${q4landingPageLeads !=0}">'<fmt:formatNumber value="${(q4landingPageRegistrations/q4landingPageLeads)*100}" pattern="#,##0.00"/>%'</c:if>
								<c:if test="${q4landingPageLeads ==0}">'0%'</c:if>
							</c:if>,
					</c:if>
				</c:forEach>
				<c:choose>
					<c:when test="${totallandingPageLeads!=0}">
						total: '<fmt:formatNumber value="${(totallandingPageRegistrations/totallandingPageLeads)*100}" pattern="#,##0.00" />%',
					</c:when>
					<c:otherwise>
						total: '0%',
					</c:otherwise>
				</c:choose>
				summary: true,
			});
			
			data.push({
				name: '<fmt:message key="activationCloseRate" />',
				<c:forEach items="${model.reports}" var="reports" varStatus="status">
					<c:choose>
						<c:when test="${reports.landingPageRegistrations!=0}">
							col_${status.index}: '<fmt:formatNumber value="${(reports.landingPageActivations/reports.landingPageRegistrations)*100}" pattern="#,##0" />%',
						</c:when>
						<c:otherwise>col_${status.index}: '0%',</c:otherwise>
					</c:choose>
					<c:if test="${status.index % 3 == 2}">
					 	<c:set var="quarterIndex" value="${quarterIndex + 1}" />
						col_${status.index}_last: 
							<c:if test="${status.index == 2}" >
								<c:if test="${q1landingPageRegistrations !=0}">'<fmt:formatNumber value="${(q1landingPageActivations/q1landingPageRegistrations)*100 }" pattern="#,##0" />%'</c:if>
								<c:if test="${q1landingPageRegistrations ==0}">'0%'</c:if>
							</c:if>
							<c:if test="${status.index == 5}" >
								<c:if test="${q2landingPageRegistrations !=0}">'<fmt:formatNumber value="${(q2landingPageActivations/q2landingPageRegistrations)*100}" pattern="#,##0"/>%'</c:if>
								<c:if test="${q2landingPageRegistrations ==0}">'0%'</c:if>
							</c:if>
							<c:if test="${status.index == 8}" >
								<c:if test="${q3landingPageRegistrations !=0}">'<fmt:formatNumber value="${(q3landingPageActivations/q3landingPageRegistrations)*100}" pattern="#,##0"/>%'</c:if>
								<c:if test="${q3landingPageRegistrations ==0}">'0%'</c:if>
							</c:if>
							<c:if test="${status.index == 11}" >
								<c:if test="${q4landingPageRegistrations !=0}">'<fmt:formatNumber value="${(q4landingPageActivations/q4landingPageRegistrations)*100}" pattern="#,##0"/>%'</c:if>
								<c:if test="${q4landingPageRegistrations ==0}">'0%'</c:if>
							</c:if>,
					</c:if>
				</c:forEach>
				<c:choose>
					<c:when test="${totallandingPageRegistrations!=0}">
						total: '<fmt:formatNumber value="${(totallandingPageActivations/totallandingPageRegistrations)*100}" pattern="#,##0" />%',
					</c:when>
					<c:otherwise>
						total: '0%',
					</c:otherwise>
				</c:choose>
				summary: true,
			});
			
			data.push({
				name: '<fmt:message key="allOtherRegistration" />',
		    		<c:forEach items="${model.reports}" var="reports" varStatus="status">
					<c:if test="${status.index % 3 == 0}">
						<c:set var="qtrallOtherRegistrations" value="0"/>
					</c:if>
					col_${status.index}: '<fmt:formatNumber value="${reports.allOtherRegistrations}" pattern="#,##0" />',
					<c:set var="qtrallOtherRegistrations" value="${reports.allOtherRegistrations+qtrallOtherRegistrations}"/>
					<c:set var="totalallOtherRegistrations" value="${reports.allOtherRegistrations+totalallOtherRegistrations}"/>
					<c:if test="${status.index % 3 == 2}">
						<c:set var="quarterIndex" value="${quarterIndex + 1}" />				 
						col_${status.index}_last: '<fmt:formatNumber value="${qtrallOtherRegistrations}" pattern="#,##0" />',				  			  				  				  				  					  
						<c:if test="${status.index == 2}" >	<c:set value="${qtrallOtherRegistrations}" var="q1allOtherRegistrations" /></c:if>
						<c:if test="${status.index == 5}" >	<c:set value="${qtrallOtherRegistrations}" var="q2allOtherRegistrations" /></c:if>	
						<c:if test="${status.index == 8}" >	<c:set value="${qtrallOtherRegistrations}" var="q3allOtherRegistrations" /></c:if>	
						<c:if test="${status.index == 11}" ><c:set value="${qtrallOtherRegistrations}" var="q4allOtherRegistrations" /></c:if>					
					</c:if>
		    		</c:forEach>
				total: '<fmt:formatNumber value="${totalallOtherRegistrations}" pattern="#,##0" />',
			});
			
			data.push({
				name: '<fmt:message key="allOtherActivation" />',
		    		<c:forEach items="${model.reports}" var="reports" varStatus="status">
					<c:if test="${status.index % 3 == 0}">
						<c:set var="qtrallOtherActivations" value="0"/>
					</c:if>
					col_${status.index}: '<fmt:formatNumber value="${reports.allOtherActivations}" pattern="#,##0" />',
					<c:set var="qtrallOtherActivations" value="${reports.allOtherActivations+qtrallOtherActivations}"/>
					<c:set var="totalallOtherActivations" value="${reports.allOtherActivations+totalallOtherActivations}"/>
					<c:if test="${status.index % 3 == 2}">
						<c:set var="quarterIndex" value="${quarterIndex + 1}" />				 
						col_${status.index}_last: '<fmt:formatNumber value="${qtrallOtherActivations}" pattern="#,##0" />',				  			  				  				  				  					  
						<c:if test="${status.index == 2}" >	<c:set value="${qtrallOtherActivations}" var="q1allOtherActivations" /></c:if>
						<c:if test="${status.index == 5}" >	<c:set value="${qtrallOtherActivations}" var="q2allOtherActivations" /></c:if>	
						<c:if test="${status.index == 8}" >	<c:set value="${qtrallOtherActivations}" var="q3allOtherActivations" /></c:if>	
						<c:if test="${status.index == 11}" ><c:set value="${qtrallOtherActivations}" var="q4allOtherActivations" /></c:if>				
					</c:if>
		    		</c:forEach>
				total: '<fmt:formatNumber value="${totalallOtherActivations}" pattern="#,##0" />',
			});
			
			data.push({
				name: '<fmt:message key="closeRate" />',
				<c:forEach items="${model.reports}" var="reports" varStatus="status">
					<c:choose>
						<c:when test="${reports.allOtherRegistrations!=0}">
							col_${status.index}: '<fmt:formatNumber value="${(reports.allOtherActivations/reports.allOtherRegistrations)*100}" pattern="#,##0.00" />%',
						</c:when>
						<c:otherwise>col_${status.index}: '0%',</c:otherwise>
					</c:choose>
					<c:if test="${status.index % 3 == 2}">
					 	<c:set var="quarterIndex" value="${quarterIndex + 1}" />
						col_${status.index}_last: 
							<c:if test="${status.index == 2}" >
								<c:if test="${q1allOtherRegistrations !=0}">'<fmt:formatNumber value="${(q1allOtherActivations/q1allOtherRegistrations)*100 }" pattern="#,##0.00" />%'</c:if>
								<c:if test="${q1allOtherRegistrations ==0}">'0%'</c:if>
							</c:if>				 
							<c:if test="${status.index == 5}" >
								<c:if test="${q2allOtherRegistrations !=0}">'<fmt:formatNumber value="${(q2allOtherActivations/q2allOtherRegistrations)*100}" pattern="#,##0.00"/>%'</c:if>
								<c:if test="${q2allOtherRegistrations ==0}">'0%'</c:if>
							</c:if>				  
							<c:if test="${status.index == 8}" >
								<c:if test="${q3allOtherRegistrations !=0}">'<fmt:formatNumber value="${(q3allOtherActivations/q3allOtherRegistrations)*100}" pattern="#,##0.00"/>%'</c:if>
								<c:if test="${q3allOtherRegistrations ==0}">'0%'</c:if>
							</c:if>				  
							<c:if test="${status.index == 11}" >
								<c:if test="${q4allOtherRegistrations !=0}">'<fmt:formatNumber value="${(q4allOtherActivations/q4allOtherRegistrations)*100}" pattern="#,##0.00"/>%'</c:if>
								<c:if test="${q4allOtherRegistrations ==0}">'0%'</c:if>
							</c:if>,
					</c:if>
				</c:forEach>
				<c:choose>
					<c:when test="${totalallOtherRegistrations!=0}">
						total: '<fmt:formatNumber value="${(totalallOtherActivations/totalallOtherRegistrations)*100}" pattern="#,##0.00" />%',
					</c:when>
					<c:otherwise>
						total: '0%',
					</c:otherwise>
				</c:choose>	
				summary: true,
			});
			
			data.push({
				name: '<fmt:message key="merchandise" />',
		    		<c:forEach items="${model.reports}" var="reports" varStatus="status">
					<c:if test="${status.index % 3 == 0}">
						<c:set var="qtrmerchandize" value="0"/>
					</c:if>
					col_${status.index}: '$<fmt:formatNumber value="${reports.merchandize}" pattern="#,##0" />',
					<c:set var="qtrmerchandize" value="${reports.merchandize+qtrmerchandize}"/>
					<c:set var="totalmerchandize" value="${reports.merchandize+totalmerchandize}"/>
					<c:if test="${status.index % 3 == 2}">
						<c:set var="quarterIndex" value="${quarterIndex + 1}" />				 
						col_${status.index}_last: '$<fmt:formatNumber value="${qtrmerchandize}" pattern="#,##0" />',				
					</c:if>
		    		</c:forEach>
				total: '$<fmt:formatNumber value="${totalmerchandize}" pattern="#,##0" />',
			});
			
			data.push({
				name: '<fmt:message key="shipping" />',
		    		<c:forEach items="${model.reports}" var="reports" varStatus="status">
					<c:if test="${status.index % 3 == 0}">
						<c:set var="qtrshipping" value="0"/>
					</c:if>
					col_${status.index}: '$<fmt:formatNumber value="${reports.shipping}" pattern="#,##0" />',
					<c:set var="qtrshipping" value="${reports.shipping+qtrshipping}"/>
					<c:set var="totalshipping" value="${reports.shipping+totalshipping}"/>
					<c:if test="${status.index % 3 == 2}">
						<c:set var="quarterIndex" value="${quarterIndex + 1}" />
						col_${status.index}_last: '$<fmt:formatNumber value="${qtrshipping}" pattern="#,##0" />',			
					</c:if>
		    		</c:forEach>
				total: '$<fmt:formatNumber value="${totalshipping}" pattern="#,##0" />',
			});
			
			data.push({
				name: '<fmt:message key="ccFee" />',
		    		<c:forEach items="${model.reports}" var="reports" varStatus="status">
					<c:if test="${status.index % 3 == 0}">
						<c:set var="qtrccFee" value="0"/>
					</c:if>
					col_${status.index}: '$<fmt:formatNumber value="${reports.ccFee}" pattern="#,##0" />',
					<c:set var="qtrccFee" value="${reports.ccFee+qtrccFee}"/>
					<c:set var="totalccFee" value="${reports.ccFee+totalccFee}"/>
					<c:if test="${status.index % 3 == 2}">
						<c:set var="quarterIndex" value="${quarterIndex + 1}" />				 
						col_${status.index}_last: '$<fmt:formatNumber value="${qtrccFee}" pattern="#,##0" />',				
					</c:if>
		    		</c:forEach>
				total: '$<fmt:formatNumber value="${totalccFee}" pattern="#,##0" />',
			});
			
			data.push({
				name: '<fmt:message key="grandTotal" />',
				<c:forEach items="${model.reports}" var="reports" varStatus="status">
					<c:if test="${status.index % 3 == 0}">
						<c:set var="qtrgrandTotal" value="0"/>
					</c:if>
					col_${status.index}: '$<fmt:formatNumber value="${reports.grandTotal}" pattern="#,##0" />',
					<c:set var="qtrgrandTotal" value="${reports.grandTotal+qtrgrandTotal}"/>
					<c:set var="totalgrandTotal" value="${reports.grandTotal+totalgrandTotal}"/>
					<c:if test="${status.index % 3 == 2}">
						<c:set var="quarterIndex" value="${quarterIndex + 1}" />				 
						col_${status.index}_last: '$<fmt:formatNumber value="${qtrgrandTotal}" pattern="#,##0" />',
						<c:if test="${quarterIndex == 97}">
							<c:set var="qtrgrandTotal_97" value="${qtrgrandTotal}"/>
						</c:if>
						<c:if test="${quarterIndex == 98}">
							<c:set var="qtrgrandTotal_98" value="${qtrgrandTotal}"/>
						</c:if>
						<c:if test="${quarterIndex == 99}">
							<c:set var="qtrgrandTotal_99" value="${qtrgrandTotal}"/>
						</c:if>
						<c:if test="${quarterIndex == 100}">
							<c:set var="qtrgrandTotal_100" value="${qtrgrandTotal}"/>
						</c:if>
				  	</c:if>
				</c:forEach>
				total: '$<fmt:formatNumber value="${totalgrandTotal}" pattern="#,##0" />',	
				summary: true,
			});
			
			data.push({
				name: '#<fmt:message key="customers" />',
		    		<c:forEach items="${model.reports}" var="reports" varStatus="status">
					<c:if test="${status.index % 3 == 0}">
						<c:set var="qtrcustomersPurchased" value="0"/>
					</c:if>
					col_${status.index}: '<fmt:formatNumber value="${reports.customersPurchased}" pattern="#,###" />',
					<c:set var="qtrcustomersPurchased" value="${reports.customersPurchased+qtrcustomersPurchased}"/>
					<c:set var="totalcustomersPurchased" value="${reports.customersPurchased+totalcustomersPurchased}"/>
					<c:if test="${status.index % 3 == 2}">
						<c:set var="quarterIndex" value="${quarterIndex + 1}" />				 
						col_${status.index}_last: '<fmt:formatNumber value="${qtrcustomersPurchased}" pattern="#,##0" />',				
					</c:if>
		    		</c:forEach>
				total: '<fmt:formatNumber value="${totalcustomersPurchased}" pattern="#,###" />',
			});
			
			data.push({
				name: '#<fmt:message key="orders" />',
		    		<c:forEach items="${model.reports}" var="reports" varStatus="status">
					<c:if test="${status.index % 3 == 0}">
						<c:set var="qtrOrder" value="0"/>
					</c:if>
					col_${status.index}: '<fmt:formatNumber value="${reports.totalOrders}" pattern="#,###" />',
					<c:set var="qtrOrder" value="${reports.totalOrders+qtrOrder}"/>
					<c:set var="totalOrder" value="${reports.totalOrders+totalOrder}"/>
					<c:if test="${status.index % 3 == 2}">
						<c:set var="quarterIndex" value="${quarterIndex + 1}" />				 
						col_${status.index}_last: '<fmt:formatNumber value="${qtrOrder}" pattern="#,##0" />',
						<c:if test="${quarterIndex == 105}">
							<c:set var="qtrOrder_105" value="${qtrOrder}"/>
						</c:if>
						<c:if test="${quarterIndex == 106}">
							<c:set var="qtrOrder_106" value="${qtrOrder}"/>
						</c:if>
						<c:if test="${quarterIndex == 107}">
							<c:set var="qtrOrder_107" value="${qtrOrder}"/>
						</c:if>
						<c:if test="${quarterIndex == 108}">
							<c:set var="qtrOrder_108" value="${qtrOrder}"/>
						</c:if>				
				  	</c:if>
		    		</c:forEach>
				<c:set var="qtrOrderValueQ1" value="${qtrOrder}"/>
				total: '<fmt:formatNumber value="${totalOrder}" pattern="#,###" />',
			});
			
			data.push({
				name: '<fmt:message key="avgOrders" />',
		    		<c:forEach items="${model.reports}" var="reports" varStatus="status">
					<c:if test="${status.index % 3 == 0}">
						<c:set var="qtravgOrders" value="0"/>
					</c:if>
					col_${status.index}: '$<fmt:formatNumber value="${reports.avgOrders}" pattern="#,###" />',
					<c:set var="qtravgOrders" value="${reports.avgOrders+qtravgOrders}"/>
					<c:set var="totalavgOrders" value="${reports.avgOrders+totalavgOrders}"/>
					<c:if test="${status.index % 3 == 2}">
						<c:set var="quarterIndex" value="${quarterIndex + 1}" />				 
						col_${status.index}_last: 
							<c:if test="${quarterIndex == 109}">
								'$<fmt:formatNumber value="${qtrgrandTotal_97 / qtrOrder_105}" pattern="#,##0" />'
							</c:if>
							<c:if test="${quarterIndex == 110}">
								'$<fmt:formatNumber value="${qtrgrandTotal_98 / qtrOrder_106}" pattern="#,##0" />'
							</c:if>
							<c:if test="${quarterIndex == 111}">
								'$<fmt:formatNumber value="${qtrgrandTotal_99 / qtrOrder_107}" pattern="#,##0" />'
							</c:if>
							<c:if test="${quarterIndex == 112}">
								'$<fmt:formatNumber value="${qtrgrandTotal_100 / qtrOrder_108}" pattern="#,##0" />'
							</c:if>,
				  	</c:if>
		    		</c:forEach>
				total: '$<fmt:formatNumber value="${totalavgOrders}" pattern="#,###" />',
			});
			
			data.push({
				name: '<fmt:message key="avgCustomers" />',
		    		<c:forEach items="${model.reports}" var="reports" varStatus="status">
					<c:if test="${status.index % 3 == 0}">
						<c:set var="qtravgCustomers" value="0"/>
					</c:if>
					col_${status.index}: '$<fmt:formatNumber value="${reports.avgCustomers}" pattern="#,###" />',
					<c:set var="qtravgCustomers" value="${reports.avgCustomers+qtravgCustomers}"/>
					<c:set var="totalavgCustomers" value="${reports.avgCustomers+totalavgCustomers}"/>
					<c:if test="${status.index % 3 == 2}">
						<c:set var="quarterIndex" value="${quarterIndex + 1}" />				 
						col_${status.index}_last: '$<fmt:formatNumber value="${qtravgCustomers}" pattern="#,##0" />',				
					</c:if>
		    		</c:forEach>
				total: '$<fmt:formatNumber value="${totalavgCustomers}" pattern="#,###" />',
			});
			
			//Columns Definitions
			require(["dgrid/List", "dgrid/OnDemandGrid", "dgrid/Selection", "dgrid/Keyboard", "dgrid/Editor", "dijit/form/CheckBox", 
					"dijit/form/TimeTextBox", "dijit/form/Select", "dojo/data/ObjectStore", "dijit/form/FilteringSelect",
					'dijit/form/DropDownButton', 'dijit/DropDownMenu', 'dijit/MenuItem',
					"dojo/_base/declare", "dojo/_base/lang", "dojo/store/Memory", "dojo/store/Observable", "dgrid/test/data/createSyncStore", 
					'dgrid/extensions/ColumnReorder', 'dgrid/extensions/ColumnResizer',
				], function (List, OnDemandGrid, Selection, Keyboard, Editor, CheckBox, TimeTextBox, Select, ObjectStore, FilteringSelect,
							DropDownButton, DropDownMenu, MenuItem,
							declare, lang, LegacyMemory, LegacyObservable, createSyncStore, ColumnReorder, ColumnResizer) {
				var columns = {
					name: {
						label: 'CUSTOMER METRICS',
						get: function(object){
							return object.name;
						},
						sortable: false,
					},
					JAN: {
						label: 'JAN',
						get: function(object){
							return object.col_0;
						},
						sortable: false,
					},
					FEB: {
						label: 'FEB',
						get: function(object){
							return object.col_1;
						},
						sortable: false,
					},
					MAR: {
						label: 'MAR',
						get: function(object){
							return object.col_2;
						},
						sortable: false,
					},
					Q1: {
						label: 'Q1',
						get: function(object){
							return object.col_2_last;
						},
						sortable: false,
					},
					APR: {
						label: 'APR',
						get: function(object){
							return object.col_3;
						},
						sortable: false,
					},
					MAY: {
						label: 'MAY',
						get: function(object){
							return object.col_4;
						},
						sortable: false,
					},
					JUNE: {
						label: 'JUNE',
						get: function(object){
							return object.col_5;
						},
						sortable: false,
					},
					Q2: {
						label: 'Q2',
						get: function(object){
							return object.col_5_last;
						},
						sortable: false,
					},
					JULY: {
						label: 'JULY',
						get: function(object){
							return object.col_6;
						},
						sortable: false,
					},
					AUG: {
						label: 'AUG',
						get: function(object){
							return object.col_7;
						},
						sortable: false,
					},
					SEP: {
						label: 'SEP',
						get: function(object){
							return object.col_8;
						},
						sortable: false,
					},
					Q3: {
						label: 'Q3',
						get: function(object){
							return object.col_8_last;
						},
						sortable: false,
					},
					OCT: {
						label: 'OCT',
						get: function(object){
							return object.col_9;
						},
						sortable: false,
					},
					NOV: {
						label: 'NOV',
						get: function(object){
							return object.col_10;
						},
						sortable: false,
					},
					DEC: {
						label: 'DEC',
						get: function(object){
							return object.col_11;
						},
						sortable: false,
					},
					Q4: {
						label: 'Q4',
						get: function(object){
							return object.col_11_last;
						},
						sortable: false,
					},
					total: {
						label: '<fmt:message key="total" />',
						get: function(object){
							return object.total;
						},
						sortable: false,
					}
				};
				
				var grid = new (declare([OnDemandGrid, Selection, Keyboard, Editor, ColumnReorder, ColumnResizer]))({
					collection: createSyncStore({ data: data }),
					selectionMode: "none",
					allowSelectAll: true,
					columns: columns,
					renderRow: function (object) {
						var rowElement = this.inherited(arguments);
				        if(object.summary){
				       	 	rowElement.className += ' summary';
				        }
				        return rowElement;
				    }
				},'grid');

				grid.startup();
			});
		</script>
	</tiles:putAttribute>
</tiles:insertDefinition>