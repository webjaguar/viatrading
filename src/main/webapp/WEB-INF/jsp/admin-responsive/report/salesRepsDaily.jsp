<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ taglib uri="http://www.springframework.org/security/tags" prefix="sec" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ taglib uri="/WEB-INF/tlds/spring.tld" prefix="spring" %>
<%@ taglib uri="/WEB-INF/tlds/custom.tld" prefix="wj" %>

<tiles:insertDefinition name="admin.responsive-template.report.salesRepsDaily" flush="true">
	<tiles:putAttribute name="css">
		<style>
			#grid .field-grandTotalShipped{
				text-align: right;
			}
			#grid .field-numOrderShipped{
				text-align: right;
			}
			#grid .field-uniqueUserShipped{
			    text-align: right;
			}
			#grid .field-grandTotalPenProc{
			    text-align: right;
			    width: 150px;
			}
			#grid .field-grandTotalPenProc{
			    text-align: right;
			}
			#grid .field-numOrderPenProc{
				text-align: right;
			}
			#grid .field-uniqueUserPenProc{
				text-align: right;
			}
		</style>
	</tiles:putAttribute>
	<tiles:putAttribute name="content" type="string">
		<sec:authorize ifAnyGranted="ROLE_AEM,ROLE_ADMIN_${prop_site_id},ROLE_REPORT_SALESREP_DAILY">
			<form name="salesRepList" id="salesRepList" action="">
				<div class="page-banner">
					<div class="breadcrumb"><span><a href="${_contextpath}/admin/catalog/dashboard.jhtm">Home</a></span><i class="sli-arrow-right"></i><span><a href="../reports"><fmt:message key="reports" /></a></span><i class="sli-arrow-right"></i><span>Sales Rep</span></div>
				</div>
				<div class="page-head">
					<div class="row justify-content-between">
						<div class="col-sm-8 title">
							<h3>Sales Rep Daily</h3>
						</div>
					</div>
				</div>
				<div class="page-body">
					<div class="grid-stage"> 
						<table id="grid"></table>
					</div>
				</div>
			</form>
		</sec:authorize>
	</tiles:putAttribute>
	<tiles:putAttribute name="js">
		<!-- DGRID TABLE SCRIPT MUST BE AT BOTTOM OF THE PAGE -->
		<script src="${_contextpath}/admin-responsive-static/assets/plugins/dojo/dojo.js" data-dojo-config="async: true"></script>
		<script>
			//Table Data Sanitization
			var data = [];
			<c:set var="totalGrandTotalShipped" value="0.0" />
			<c:set var="totalGrandTotalPenProc" value="0.0" />
			<c:set var="totalNumOrderShipped" value="0" />
			<c:set var="totalUniqueUserShipped" value="0" />
			<c:set var="totalNumOrderPenProc" value="0" />
			<c:set var="totalUniqueUserPenProc" value="0" />
			<c:forEach items="${model.salesRepReport}" var="salesRep" varStatus="status">
		    		data.push({
		    			offset: '${status.count}.',
		    			orderDate: '<fmt:formatDate type="date" timeStyle="default" value="${salesRep.orderDate}"/>',
		    			<c:if test="${!empty salesRep.grandTotalShipped}">
		    				grandTotalShipped: decodeHTML('<fmt:message key="${siteConfig[\'CURRENCY\'].value}" />') + '<fmt:formatNumber value="${salesRep.grandTotalShipped}" pattern="#,##0.00" />',
						<c:set var="totalGrandTotalShipped" value="${salesRep.grandTotalShipped + totalGrandTotalShipped}" />
			        </c:if>
			        numOrderShipped: '${salesRep.numOrderShipped}',
					uniqueUserShipped: '<c:out value="${salesRep.uniqueUserShipped}" />',
					<c:if test="${!empty salesRep.grandTotalPenProc}">
						grandTotalPenProc: decodeHTML('<fmt:message key="${siteConfig[\'CURRENCY\'].value}" />') + '<fmt:formatNumber value="${salesRep.grandTotalPenProc}" pattern="#,##0.00" />',
						<c:set var="totalGrandTotalPenProc" value="${salesRep.grandTotalPenProc + totalGrandTotalPenProc}" />
					</c:if>
					numOrderPenProc: '<c:out value="${salesRep.numOrderPenProc}" />',
					uniqueUserPenProc: '<c:out value="${salesRep.uniqueUserPenProc}" />',
				});
		    		<c:set var="totalNumOrderShipped" value="${salesRep.numOrderShipped + totalNumOrderShipped}" />
		    		<c:set var="totalUniqueUserShipped" value="${salesRep.uniqueUserShipped + totalUniqueUserShipped}" />
		    		<c:set var="totalNumOrderPenProc" value="${salesRep.numOrderPenProc + totalNumOrderPenProc}" />
	    			<c:set var="totalUniqueUserPenProc" value="${salesRep.uniqueUserPenProc + totalUniqueUserPenProc}" />
			</c:forEach>

			//Columns Definitions
			require(["dgrid/List", "dgrid/OnDemandGrid", "dgrid/Selection", "dgrid/Keyboard", "dgrid/Editor", "dijit/form/CheckBox", 
					"dijit/form/TimeTextBox", "dijit/form/Select", "dojo/data/ObjectStore", "dijit/form/FilteringSelect",
					'dijit/form/DropDownButton', 'dijit/DropDownMenu', 'dijit/MenuItem',
					"dojo/_base/declare", "dojo/_base/lang", "dojo/store/Memory", "dojo/store/Observable", "dgrid/test/data/createSyncStore", 
					'dgrid/extensions/ColumnReorder', 'dgrid/extensions/ColumnResizer', 'summary/SummaryRow',
				], function (List, OnDemandGrid, Selection, Keyboard, Editor, CheckBox, TimeTextBox, Select, ObjectStore, FilteringSelect,
							DropDownButton, DropDownMenu, MenuItem,
							declare, lang, LegacyMemory, LegacyObservable, createSyncStore, ColumnReorder, ColumnResizer, SummaryRow) {
				var columns = {
					offset: {
						label: '#',
						get: function(object){
							return object.offset;
						},
						sortable: false,
					},
					orderDate: {
						label: '<fmt:message key="date" />',
						get: function(object){
							return object.orderDate; 
						},
						sortable: false
					},
					grandTotalShipped: {
						label: '<fmt:message key="shipped" />',
						get: function(object){
							return object.grandTotalShipped; 
						},
						sortable: false
					},
					numOrderShipped: {
						label: '# Orders',
						get: function(object){
							return object.numOrderShipped; 
						},
						sortable: false
					},
					uniqueUserShipped: {
						label: '# Unique User',
						get: function(object){
							return object.uniqueUserShipped; 
						},
						sortable: false
					},
					grandTotalPenProc: {
						label: '<fmt:message key="pendingProcessing" />',
						get: function(object){
							return object.grandTotalPenProc; 
						},
						sortable: false
					},
					numOrderPenProc: {
						label: '# Orders',
						get: function(object){
							return object.numOrderPenProc; 
						},
						sortable: false
					},
					uniqueUserPenProc: {
						label: '# Unique User',
						get: function(object){
							return object.uniqueUserPenProc; 
						},
						sortable: false
					},
				};
				
				var grid = new (declare([OnDemandGrid, Selection, Keyboard, Editor, ColumnReorder, ColumnResizer, SummaryRow]))({
					collection: createSyncStore({ data: data }),
					selectionMode: "none",
					allowSelectAll: true,
					columns: columns
				},'grid');
				
				var totals = {
					orderDate: '<fmt:message key="total" />',
					grandTotalShipped: decodeHTML('<fmt:message key="${siteConfig[\'CURRENCY\'].value}" />') + '<fmt:formatNumber value="${totalGrandTotalShipped}" pattern="#,##0.00" />',
					numOrderShipped: '<fmt:formatNumber value="${totalNumOrderShipped}" pattern="#,##0" />',
					uniqueUserShipped: '<fmt:formatNumber value="${totalUniqueUserShipped}" pattern="#,##0" />',
					grandTotalPenProc: decodeHTML('<fmt:message key="${siteConfig[\'CURRENCY\'].value}" />') + '<fmt:formatNumber value="${totalGrandTotalPenProc}" pattern="#,##0.00" />',
					numOrderPenProc: '<fmt:formatNumber value="${totalNumOrderPenProc}" pattern="#,##0" />',
					uniqueUserPenProc: '<fmt:formatNumber value="${totalUniqueUserPenProc}" pattern="#,##0" />',
				};

				grid.set('summary', totals); 
				
				grid.on('dgrid-refresh-complete', function(event) {
					$('#grid-hider-menu').appendTo('#column-hider');
				});
				
				grid.startup();
			});
		</script>
	</tiles:putAttribute>
</tiles:insertDefinition>
