<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ taglib uri="http://www.springframework.org/security/tags" prefix="sec" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ taglib uri="/WEB-INF/tlds/spring.tld" prefix="spring" %>
<%@ taglib uri="/WEB-INF/tlds/custom.tld" prefix="wj" %>

<tiles:insertDefinition name="admin.responsive-template.report.ordersDaily" flush="true">
	<tiles:putAttribute name="css">
		<style>
			#grid .field-grand_total{
			    text-align:right;
			}
			#grid .field-merchandise{
			     text-align:right;
			}
			#grid .field-numOrder{
				text-align:right;
			}
			#grid .field-uniqueUser{
				text-align:right;
			}
		</style>
	</tiles:putAttribute>
	<tiles:putAttribute name="content" type="string">
		<c:if test="${gSiteConfig['gREPORT']}">
			<sec:authorize ifAnyGranted="ROLE_AEM,ROLE_ADMIN_${prop_site_id},ROLE_REPORT_ORDERS_DAILY">
				<form name="list" id="list">
					<div class="page-banner">
						<div class="breadcrumb"><span><a href="${_contextpath}/admin/catalog/dashboard.jhtm">Home</a></span><i class="sli-arrow-right"></i><span><a href="../reports"><fmt:message key="reports" /></a></span><i class="sli-arrow-right"></i><span><fmt:message key="ordersDaily" /></span></div>
					</div>
					<div class="page-head">
						<div class="row justify-content-between">
							<div class="col-sm-8 title">
								<h3><fmt:message key="ordersDaily" /></h3>
							</div>
							<div class="col-sm-4 actions">
			                    <div class="dropdown dropdown-setting">
									<button type="button" class="btn-outline dropdown-toggle active btn btn-primary" data-toggle="dropdown">
			                             <i class="sli-screen-desktop"></i> Display
			                         </button>	                         
									<div class="dropdown-menu dropdown-menu-right">
										<div class="form-group">
					                    		<div class="option-header">Rows</div>
											<select class="custom-select form-control" name="size" onchange="document.getElementById('page').value=1;submit();">
												<c:forTokens items="10,25,50,100,200,500" delims="," var="current">
										  	  		<option value="${current}" <c:if test="${current == shipDailyOrderReportFilter.pageSize}">selected</c:if>>${current}</option>
									  			</c:forTokens>
											</select>
										</div>
										<div class="form-group">
					                    		<div class="option-header">Columns</div>
					                    		<div id="column-hider"></div>
										</div>
									</div>
			                    </div>
			                    <div class="dropdown">
									<button type="button" class="btn-outline dropdown-toggle active btn btn-primary" onclick="$('#search-dialog').modal('show')">
			                            <i class="sli-magnifier"></i>
			                         </button>
								</div>
							</div>
						</div>
					</div>
					<div class="page-body">
						<div class="grid-stage"> 
							<table id="grid"></table>
							<div class="footer">
								<c:if test="${model.pageCount != 0}">
					 				<div class="float-right">
										<c:if test="${shipDailyOrderReportFilter.page == 1}">
											<span class="previous"><i class="sli-arrow-left" style="font-size: 0.7rem"></i> Previous</span>
										</c:if>
										<c:if test="${shipDailyOrderReportFilter.page != 1}">
											<a href="<c:url value="dailyOrderReport.jhtm"><c:param name="page" value="${shipDailyOrderReportFilter.page-1}"/></c:url>"><span class="previous"><i class="sli-arrow-left" style="font-size: 0.7rem"></i> Previous</span></a>
										</c:if>
										  
										<span class="status"><c:out value="${shipDailyOrderReportFilter.page}" /> of <c:out value="${model.pageCount}" /></span>
										
										<c:if test="${shipDailyOrderReportFilter.page == model.pageCount}">
											<span class="next">Next <i class="sli-arrow-right" style="font-size: 0.7rem"></i></span>
										</c:if>
										<c:if test="${shipDailyOrderReportFilter.page != model.pageCount}">
											<a href="<c:url value="dailyOrderReport.jhtm"><c:param name="page" value="${shipDailyOrderReportFilter.page+1}"/></c:url>"><span class="next">Next <i class="sli-arrow-right" style="font-size: 0.7rem"></i></span></a>
										</c:if>
									</div>
								</c:if>
					        </div>
						</div>
					</div>
					<input type="hidden" id="page" name="page"/>
					<div class="modal-danger">
						<div id="search-dialog" class="modal">
							<div class="modal-dialog modal-default" role="document">
								<div class="modal-content">
									<div class="modal-header">
										<h4 class="modal-title">Search Options</h4>
										<button type="button" class="close" aria-label="Close" data-dismiss="modal">
											<span aria-hidden="true">�</span>
										</button>
									</div>
									<div class="modal-body">
										<div class="form-section">
											<div class="row">
												<div class="col-sm-6">
													<div class="form-group">
								                    		<div class="col-form-label"><fmt:message key="dateType" /></div>
								                    		<select name="dateType" class="custom-select">
															<option value="orderDate">Order Date</option>
															<option value="shipDate" <c:if test="${shipDailyOrderReportFilter.dateType == 'shipDate'}">selected</c:if>>Ship Date</option>
														</select>
													</div>
													<!-- Missing salesRep -->
												</div>
												<div class="col-sm-6">
													<div class="form-group">
								                    		<div class="col-form-label">Start</div>
								                    		<input class="form-control" name="startDate" id="startDate" type="text" value="<fmt:formatDate type="date" pattern="MM/dd/yyyy" value="${shipDailyOrderReportFilter.startDate}"/>" />
													</div>
													<div class="form-group">
								                    		<div class="col-form-label">End</div>
								                    		<input class="form-control" name="endDate" id="endDate" type="text" value="<fmt:formatDate type="date" pattern="MM/dd/yyyy" value='${shipDailyOrderReportFilter.endDate}'/>" readonly size="11" />
													</div>
													<div class="form-group">
														<label class="custom-control custom-radio">
															<input id="today" type="radio" name="intervaldate" class="custom-control-input" value="quantity" onclick="UpdateStartEndDate(1);" ><span class="custom-control-indicator"></span><span class="custom-control-description">Today</span>
														</label><br/>
														<label class="custom-control custom-radio">
															<input id="weekToDate" type="radio" name="intervaldate" class="custom-control-input" value="total" onclick="UpdateStartEndDate(2);" ><span class="custom-control-indicator"></span><span class="custom-control-description">Week To Date</span>
														</label><br/>
														<label class="custom-control custom-radio">
															<input id="monthToDate" type="radio" name="intervaldate" class="custom-control-input" value="quantity" onclick="UpdateStartEndDate(3);" ><span class="custom-control-indicator"></span><span class="custom-control-description">Month To Date</span>
														</label><br/>
														<label class="custom-control custom-radio">
															<input id="yearToDate" type="radio" name="intervaldate" class="custom-control-input" value="total" onclick="UpdateStartEndDate(10);" ><span class="custom-control-indicator"></span><span class="custom-control-description">Year To Date</span>
														</label><br/>
														<label class="custom-control custom-radio">
															<input id="reset" type="radio" name="intervaldate" class="custom-control-input" value="total" onclick="UpdateStartEndDate(20);" ><span class="custom-control-indicator"></span><span class="custom-control-description">Reset</span>
														</label>
													</div>
												</div>
											</div>
										</div>
									</div>
									<div class="modal-footer">
										<button class="btn btn-primary">Search</button>
									</div>
								</div>
							</div>
						</div>
					</div>
				</form>
			</sec:authorize>
		</c:if>
	</tiles:putAttribute>
	<tiles:putAttribute name="js">
		<script src="${_contextpath}/admin-responsive-static/assets/js/report.js"></script>
		<script>
			$('#startDate').datepicker();
			$('#endDate').datepicker();
		</script>
		<!-- DGRID TABLE SCRIPT MUST BE AT BOTTOM OF THE PAGE -->
		<script src="${_contextpath}/admin-responsive-static/assets/plugins/dojo/dojo.js" data-dojo-config="async: true"></script>
		<script>
			//Table Data Sanitization
			var data = [];
			<c:set var="totalGrandTotal" value="0" />
		    <c:set var="merchandizeTotal" value="0" />
		    	<c:set var="totalOrders" value="0" />		
		    <c:set var="totalUniqueUser" value="0" />
			<c:forEach items="${model.salesReportsListDaily}" var="dayReport" varStatus="status">
				data.push({
					offset: '${status.count + shipDailyOrderReportFilter.offset}.',
					date: '<fmt:formatDate type="date" value="${dayReport.date}"/>',
					grand_total: decodeHTML('<fmt:message key="${siteConfig[\'CURRENCY\'].value}" />') + '<fmt:formatNumber value="${dayReport.grandTotal}" pattern="#,##0.00" />',
					merchandise: decodeHTML('<fmt:message key="${siteConfig[\'CURRENCY\'].value}" />') + '<fmt:formatNumber value="${dayReport.merchandize}" pattern="#,##0.00" />',
					numOrder: "${dayReport.numOrder}",
					uniqueUser: "${dayReport.uniqueUser}",
				});
				<c:set var="totalGrandTotal" value="${dayReport.grandTotal + totalGrandTotal}" />
			    <c:set var="merchandizeTotal" value="${dayReport.merchandize + merchandizeTotal}" />
			    	<c:set var="totalOrders" value="${dayReport.numOrder + totalOrders}" />		
			    <c:set var="totalUniqueUser" value="${dayReport.uniqueUser + totalUniqueUser}" />
			</c:forEach>

			//Columns Definitions
			require(['dojo/_base/lang',
					'dojo/_base/declare',
					'dstore/Memory',
					'dstore/Trackable',
					'dgrid/OnDemandGrid',
					'dgrid/Editor',
					'dgrid/extensions/ColumnHider',
					'dgrid/extensions/ColumnReorder',
					'dgrid/extensions/ColumnResizer',
					'dgrid/Selector',
					'dijit/form/DropDownButton',
					'dijit/DropDownMenu',
					'dijit/MenuItem',
					'summary/SummaryRow'
				], function (lang, declare, Memory, Trackable, OnDemandGrid, Editor, ColumnHider, ColumnReorder, ColumnResizer, Selector, DropDownButton, DropDownMenu, MenuItem, SummaryRow) {
				var columns = {
					offset: {
						label: '#',
						get: function(object){
							return object.offset;
						},
						sortable: false,
						unhidable: true
					},
					date: {
						label: '<fmt:message key="date" />',
						get: function(object){
							return object.date;
						},
						sortable: false,
					},
					grand_total: {
						label: '<fmt:message key="grandTotal" />',
						get: function(object){
							return object.grand_total;
						},
						sortable: false,
					},
					merchandise: {
						label: '<fmt:message key="merchandise" />',
						get: function(object){
							return object.merchandise;
						},
						sortable: false,
					},
					numOrder: {
						label: '# Order',
						get: function(object){
							return object.numOrder;
						},
						sortable: false,
					},
					uniqueUser: {
						label: 'Unique User',
						get: function(object){
							return object.uniqueUser;
						},
						sortable: false,
					},
				};
				var store = new (declare([Memory, Trackable]))({
					data: data
				});
				
				var grid = new (declare([OnDemandGrid, Editor, ColumnHider, ColumnReorder, ColumnResizer, Selector, SummaryRow]))({
					collection: store,
					selectionMode: "none",
					allowSelectAll: true,
					columns: columns
				},'grid');
				
				var totals = {
					date: 'Total',
					grand_total:  decodeHTML('<fmt:message key="${siteConfig[\'CURRENCY\'].value}" />') + '<fmt:formatNumber value="${totalGrandTotal}" pattern="#,##0.00" />',
					merchandise:  decodeHTML('<fmt:message key="${siteConfig[\'CURRENCY\'].value}" />') + '<fmt:formatNumber value="${merchandizeTotal}" pattern="#,##0.00" />',
					numOrder: '${totalOrders}',
					uniqueUser: '${totalUniqueUser}',
				};
				
				grid.set('summary', totals);
				
				grid.on('dgrid-refresh-complete', function(event) {
					$('#grid-hider-menu').appendTo('#column-hider');
				});
				
				grid.startup();
			});
		</script>
	</tiles:putAttribute>
</tiles:insertDefinition>
