<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ taglib uri="http://www.springframework.org/security/tags" prefix="sec" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ taglib uri="/WEB-INF/tlds/spring.tld" prefix="spring" %>
<%@ taglib uri="/WEB-INF/tlds/custom.tld" prefix="wj" %>

<tiles:insertDefinition name="admin.responsive-template.report.importExport" flush="true">
	<tiles:putAttribute name="css">
	</tiles:putAttribute>
	<tiles:putAttribute name="content" type="string">
		<sec:authorize ifAnyGranted="ROLE_AEM,ROLE_ADMIN_${prop_site_id},ROLE_REPORT_IMP_EXP">
			<form id="list" action="importExportHistory.jhtm" method="post">
				<div class="page-banner">
					<div class="breadcrumb"><span><a href="${_contextpath}/admin/catalog/dashboard.jhtm">Home</a></span><i class="sli-arrow-right"></i><span><a href="../reports"><fmt:message key="reports" /></a></span><i class="sli-arrow-right"></i><span>Customer Metric Overview</span></div>
				</div>
				<div class="page-head">
					<div class="row justify-content-between">
						<div class="col-sm-8 title">
							<h3>Import/Export</h3>
						</div>
						<div class="col-sm-4 actions">
		                    <div class="dropdown dropdown-setting">
								<button type="button" class="btn-outline dropdown-toggle active btn btn-primary" data-toggle="dropdown">
		                             <i class="sli-screen-desktop"></i> Display
		                         </button>	                         
								<div class="dropdown-menu dropdown-menu-right">
									<div class="form-group">
				                    		<div class="option-header">Rows</div>
										<select class="custom-select" name="size"  onchange="document.getElementById('page').value=1;submit()">
											<c:forTokens items="10,25,50,100,200,500" delims="," var="current">
												<option value="${current}" <c:if test="${current == model.importExportHistoryList.pageSize}">selected</c:if>>${current} <fmt:message key="perPage" /></option>
											</c:forTokens>
										</select>
									</div>
									<div class="form-group">
				                    		<div class="option-header">Columns</div>
				                    		<div id="column-hider"></div>
									</div>
								</div>
		                    </div>
						</div>
					</div>
				</div>
				<div class="page-body">
					<div class="grid-stage">
						<c:if test="${model.importExportHistoryList.nrOfElements > 0}">
							<div class="page-stat">
								<fmt:message key="showing">
									<fmt:param value="${model.importExportHistoryList.firstElementOnPage + 1}" />
									<fmt:param value="${model.importExportHistoryList.lastElementOnPage + 1}" />
									<fmt:param value="${model.importExportHistoryList.nrOfElements}" />
								</fmt:message>
							</div> 
						</c:if>
						<table id="grid"></table>
						<div class="footer">
							<div class="float-right">
								<c:if test="${model.importExportHistoryList.firstPage}">
									<span class="previous"><i class="sli-arrow-left" style="font-size: 0.7rem"></i> Previous</span>
								</c:if>
								<c:if test="${not model.importExportHistoryList.firstPage}">
									<a href="<c:url value="importExportHistory.jhtm"><c:param name="page" value="${model.importExportHistoryList.page}"/><c:param name="size" value="${model.purchaseOrders.pageSize}"/></c:url>">
										<span class="previous"><i class="sli-arrow-left" style="font-size: 0.7rem"></i> Previous</span>
									</a>
								</c:if>	
								<span class="status"><c:out value="${model.importExportHistoryList.page+1}" /> of <c:out value="${model.importExportHistoryList.pageCount}" /></span>
								<c:if test="${model.importExportHistoryList.lastPage}">
									<span class="next">Next <i class="sli-arrow-right" style="font-size: 0.7rem"></i></span>
								</c:if>
								<c:if test="${not model.importExportHistoryList.lastPage}">
									<a href="<c:url value="importExportHistory.jhtm"><c:param name="page" value="${model.importExportHistoryList.page+2}"/><c:param name="size" value="${model.purchaseOrders.pageSize}"/></c:url>">
										<span class="next">Next <i class="sli-arrow-right" style="font-size: 0.7rem"></i></span>
									</a>
								</c:if>
							</div>
						</div>
					</div>
				</div>
				<input type="hidden" id="sort" name="sort" value="${importExportHistorySearch.sort}" />
			</form>
		</sec:authorize>
	</tiles:putAttribute>
	<tiles:putAttribute name="js">
		<!-- DGRID TABLE SCRIPT MUST BE AT BOTTOM OF THE PAGE -->
		<script src="${_contextpath}/admin-responsive-static/assets/plugins/dojo/dojo.js" data-dojo-config="async: true"></script>
		<script>
			//Table Data Sanitization
			var data = [];
			<c:forEach items="${model.importExportHistoryList.pageList}" var="ieHistory" varStatus="status">
				data.push({
					offset: '${status.count + model.importExportHistoryList.firstElementOnPage}.',
					type: '${ieHistory.type}',
					created: '<fmt:formatDate type="both" timeStyle="full" pattern="MM/dd/yyyy hh:mm a" value="${ieHistory.created}"/>',
					username: '<c:out value="${ieHistory.username}" />',
					importedString: '${ieHistory.importedString}',
				});
			</c:forEach>
			
			require(["dgrid/List", "dgrid/OnDemandGrid", "dgrid/Selection", "dgrid/Keyboard", "dgrid/Editor", "dijit/form/CheckBox", 
					"dijit/form/TimeTextBox", "dijit/form/Select", "dojo/data/ObjectStore", "dijit/form/FilteringSelect",
					'dijit/form/DropDownButton', 'dijit/DropDownMenu', 'dijit/MenuItem',
					"dojo/_base/declare", "dojo/_base/lang", "dojo/store/Memory", "dojo/store/Observable", "dgrid/test/data/createSyncStore", 
					'dgrid/extensions/ColumnHider', 'dgrid/extensions/ColumnReorder', 'dgrid/extensions/ColumnResizer',
				], function(List, OnDemandGrid, Selection, Keyboard, Editor, CheckBox, TimeTextBox, Select, ObjectStore, FilteringSelect,
						DropDownButton, DropDownMenu, MenuItem,
						declare, lang, LegacyMemory, LegacyObservable, createSyncStore, ColumnHider, ColumnReorder, ColumnResizer){
	
				//Column Definitions
				var columns = {
					offset: {
						label: '#',
						get: function(object){
							return object.offset;
						},
						sortable: false,
						unhidable: true
					},
					type: {
						label: '<fmt:message key="type" />',
						get: function(object){
							return object.type;
						},
						sortable: false,
					},
					created: {
						label: '<fmt:message key="created" />',
						get: function(object){
							return object.created;
						},
						sortable: false,						
					},
					username: {
						label: '<fmt:message key="username" />',
						get: function(object){
							return object.username;
						},
						sortable: false,
					},
					importedString: {
						label: '<fmt:message key="import" />/<fmt:message key="export" />',
						get: function(object){
							return object.importedString;
						},
						sortable: false,
					}
				};
				
				var grid = new (declare([OnDemandGrid, Selection, Keyboard, Editor, ColumnHider, ColumnReorder, ColumnResizer]))({
					collection: createSyncStore({ data: data }),
					columns: columns,
					selectionMode: "none",
				},'grid');
				
				grid.on('dgrid-refresh-complete', function(event) {
					$('#grid-hider-menu').appendTo('#column-hider');
				});
				
				grid.startup();
			});
		</script>
	</tiles:putAttribute>
</tiles:insertDefinition>