<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ taglib uri="http://www.springframework.org/security/tags" prefix="sec" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ taglib uri="/WEB-INF/tlds/spring.tld" prefix="spring" %>
<%@ taglib uri="/WEB-INF/tlds/custom.tld" prefix="wj" %>

<tiles:insertDefinition name="admin.responsive-template.report.customers" flush="true">
	<tiles:putAttribute name="css">
		<style>
			#grid .field-customer{
				width: 150px;
			}
			#grid .field-company{
				width: 200px;
			}
			#grid .field-order_count{
			    width: 80px;
			    text-align: right;
			}
			#grid .field-grand_total{
			    text-align: right;
			}
		</style>
	</tiles:putAttribute>
	<tiles:putAttribute name="content" type="string">
		<sec:authorize ifAnyGranted="ROLE_AEM,ROLE_ADMIN_${prop_site_id},ROLE_REPORT_CUSTOMERS">
			<form name="customerList" id="list">
			  	<div class="page-banner">
					<div class="breadcrumb"><span><a href="${_contextpath}/admin/catalog/dashboard.jhtm">Home</a></span><i class="sli-arrow-right"></i><span><a href="../reports"><fmt:message key="reports" /></a></span><i class="sli-arrow-right"></i><span><fmt:message key="customersOverview" /></span></div>
				</div>
				<div class="page-head">
					<div class="row justify-content-between">
						<div class="col-sm-8 title">
							<h3><fmt:message key="customersOverview" /></h3>
						</div>
						<div class="col-sm-4 actions">
		                    <div class="dropdown dropdown-setting">
								<button type="button" class="btn-outline dropdown-toggle active btn btn-primary" data-toggle="dropdown">
		                             <i class="sli-screen-desktop"></i> Display
		                         </button>         
								<div class="dropdown-menu dropdown-menu-right">
									<div class="form-group">
				                    		<div class="option-header">Rows</div>
										<select class="custom-select form-control" name="size" onchange="document.getElementById('page').value=1;submit();">
											<c:forTokens items="10,25,50,100,200,500" delims="," var="current">
									  	  		<option value="${current}" <c:if test="${current == model.customerReports.pageSize}">selected</c:if>>${current}</option>
								  			</c:forTokens>
										</select>
									</div>
									<div class="form-group">
				                    		<div class="option-header">Columns</div>
				                    		<div id="column-hider"></div>
									</div>
								</div>
							</div>
							<div class="dropdown">
								<button type="button" class="btn-outline dropdown-toggle active btn btn-primary" onclick="$('#search-dialog').modal('show')">
		                            <i class="sli-magnifier"></i>
		                         </button>
	                         </div>
						</div>
					</div>
				</div>
				<div class="page-body">
					<div class="grid-stage"> 
						<table id="grid"></table>
						<div class="footer">
			                <div class="float-right">
								<c:if test="${model.customerReports.firstPage}">
							 		<span class="previous"><i class="sli-arrow-left" style="font-size: 0.7rem"></i> Previous</span>
								</c:if>
								<c:if test="${not model.customerReports.firstPage}">
									<a href="<c:url value="customerReport.jhtm"><c:param name="page" value="${model.customerReports.page}"/><c:param name="size" value="${model.inactiveCustomerReports.pageSize}"/></c:url>">
										<span class="previous"><i class="sli-arrow-left" style="font-size: 0.7rem"></i> Previous</span>
									</a>
								</c:if>
								<span class="status"><c:out value="${model.categories.page+1}" /> of <c:out value="${model.customerReports.pageCount}" /></span>
								<c:if test="${model.customerReports.lastPage}">
								 	<span class="next">Next <i class="sli-arrow-right" style="font-size: 0.7rem"></i></span>
								</c:if>
								<c:if test="${not model.customerReports.lastPage}">
									<a href="<c:url value="customerReport.jhtm"><c:param name="page" value="${model.customerReports.page+2}"/><c:param name="size" value="${model.customerReports.pageSize}"/></c:url>">
										<span class="next">Next <i class="sli-arrow-right" style="font-size: 0.7rem"></i></span>
									</a>
								</c:if>
							</div>
						</div>
					</div>
				</div>
				<input type="hidden" id="sort" name="sort" value="${customerFilter.sort}" />
				<div class="modal-danger">
					<div id="search-dialog" class="modal">
						<div class="modal-dialog modal-default" role="document">
							<div class="modal-content">
								<div class="modal-header">
									<h4 class="modal-title">Search Customers</h4>
									<button type="button" class="close" aria-label="Close" data-dismiss="modal">
										<span aria-hidden="true">�</span>
									</button>
								</div>
								<div class="modal-body">
									<div class="form-section">
										<div class="row">
											<div class="col-sm-6">
												<c:if test="${gSiteConfig['gGROUP_CUSTOMER']}">
												    <div class="form-group">
														<div class="col-form-label"><fmt:message key="group" /></div>
														<select name="groupId" class="custom-select">
															<option value=""></option>
															<c:forEach items="${model.groupList}" var="group">
																<option value="${group.id}" <c:if test="${customerFilter.groupId == group.id}">selected</c:if>><c:out value="${group.name}" /></option>
															</c:forEach>
														</select>
												    </div>
											    </c:if>
												<div class="form-group">
							                    		<div class="col-form-label"><fmt:message key="orderType" /></div>
													<select name="orderType" class="custom-select">
														<option value=""><fmt:message key="allOrders" /></option>
														<c:forTokens items="${siteConfig['ORDER_TYPE'].value}" delims="," var="type" varStatus="status">
															<option value ="${type}" <c:if test="${type == customerFilter.orderType}">selected</c:if>><c:out value ="${type}" /></option>
														</c:forTokens>
													</select>
												</div>
												<div class="form-group">
													<div class="col-form-label"><fmt:message key="shippingTitle" /></div>
													<input class="form-control" name="shippingMethod" type="text" value="<c:out value='${customerFilter.shippingMethod}' />" />
											    </div>
											    <div class="form-group">
							                    		<div class="col-form-label"><fmt:message key="firstName" /></div>
													<input class="form-control" name="firstname" type="text" value="<c:out value='${customerFilter.firstName}' />" />
												</div>
												<div class="form-group">
							                    		<div class="col-form-label"><fmt:message key="lastName" /></div>
													<input class="form-control" name="lastname" type="text" value="<c:out value='${customerFilter.lastName}' />" />
												</div>
											</div>
											<div class="col-sm-6">
												<div class="form-group">
							                    		<div class="col-form-label">Start</div>
							                    		<input class="form-control" name="startDate" id="startDate" type="text" value="<fmt:formatDate type="date" pattern="MM/dd/yyyy" value='${customerFilter.startDate}'/>" />
												</div>
												<div class="form-group">
							                    		<div class="col-form-label">End</div>
							                    		<input class="form-control" name="endDate" id="endDate" type="text" value="<fmt:formatDate type="date" pattern="MM/dd/yyyy" value='${customerFilter.endDate}'/>" />
												</div>
												<div class="form-group">
													<label class="custom-control custom-radio">
														<input id="today" type="radio" name="intervaldate" class="custom-control-input" value="quantity" onclick="UpdateStartEndDate(1);" ><span class="custom-control-indicator"></span><span class="custom-control-description">Today</span>
													</label><br/>
													<label class="custom-control custom-radio">
														<input id="weekToDate" type="radio" name="intervaldate" class="custom-control-input" value="total" onclick="UpdateStartEndDate(2);" ><span class="custom-control-indicator"></span><span class="custom-control-description">Week To Date</span>
													</label><br/>
													<label class="custom-control custom-radio">
														<input id="monthToDate" type="radio" name="intervaldate" class="custom-control-input" value="quantity" onclick="UpdateStartEndDate(3);" ><span class="custom-control-indicator"></span><span class="custom-control-description">Month To Date</span>
													</label><br/>
													<label class="custom-control custom-radio">
														<input id="yearToDate" type="radio" name="intervaldate" class="custom-control-input" value="total" onclick="UpdateStartEndDate(10);" ><span class="custom-control-indicator"></span><span class="custom-control-description">Year To Date</span>
													</label><br/>
													<label class="custom-control custom-radio">
														<input id="reset" type="radio" name="intervaldate" class="custom-control-input" value="total" onclick="UpdateStartEndDate(20);" ><span class="custom-control-indicator"></span><span class="custom-control-description">Reset</span>
													</label>
												</div>
											</div>
										</div>
									</div>
								</div>
								<div class="modal-footer">
									<button class="btn btn-primary">Search</button>
								</div>
							</div>
						</div>
					</div>
				</div>
			</form>
		</sec:authorize>
	</tiles:putAttribute>
	<tiles:putAttribute name="js">
		<script>
			$('#startDate').datepicker();
			$('#endDate').datepicker();
		</script>
		<!-- DGRID TABLE SCRIPT MUST BE AT BOTTOM OF THE PAGE -->
		<script src="${_contextpath}/admin-responsive-static/assets/plugins/dojo/dojo.js" data-dojo-config="async: true"></script>
		<script>
			//Table Data Sanitization
			var data = [];
			<c:set var="totalCountTotal" value="0" />
			<c:set var="totalSubTotal" value="0"/>
			<c:forEach items="${model.customerReports.pageList}" var="customer" varStatus="status">
		    		data.push({
		    			offset: '${status.count + model.orders.firstElementOnPage}.',
					firstName: '${customer.firstName}',
					lastName: '${customer.lastName}',
					company: '${customer.address.company}',
					salesRep: '${model.salesRepMap[customer.salesRepId].name}',
					order_count: '${customer.orderCount}',
					grand_total: decodeHTML('<fmt:message key="${siteConfig[\'CURRENCY\'].value}" />')+ '<fmt:formatNumber value="${customer.ordersGrandTotal}" pattern="#,##0.00" />',
				});
		    		<c:set var="totalCountTotal" value="${customer.orderCount + totalCountTotal}" />
	    			<c:set var="totalSubTotal" value="${customer.ordersGrandTotal + totalSubTotal}" />
			</c:forEach>

			//Columns Definitions
			require(['dojo/_base/lang',
					'dojo/_base/declare',
					'dstore/Memory',
					'dstore/Trackable',
					'dgrid/OnDemandGrid',
					'dgrid/Editor',
					'dgrid/extensions/ColumnHider',
					'dgrid/extensions/ColumnReorder',
					'dgrid/extensions/ColumnResizer',
					'dgrid/Selector',
					'dijit/form/DropDownButton',
					'dijit/DropDownMenu',
					'dijit/MenuItem',
					'summary/SummaryRow',
				], function (lang, declare, Memory, Trackable, OnDemandGrid, Editor, ColumnHider, ColumnReorder, ColumnResizer, Selector, DropDownButton, DropDownMenu, MenuItem, SummaryRow) {
				var columns = {
					offset: {
						label: '#',
						get: function(object){
							return object.offset;
						},
						sortable: false,
						unhidable: true
					},
					customer: {
						label: '<fmt:message key="customer" />',
						get: function(object){
							return object.firstName + ' ' + object.lastName; 
						},
						sortable: false
					},
					company: {
						label: '<fmt:message key="company" />',
						get: function(object){
							return object.company; 
						},
					},
					salesRep: {
						label: '<fmt:message key="salesRep" />',
						get: function(object){
							return object.salesRep; 
						},
						sortable: false
					},
					order_count: {
						label: '<fmt:message key="orderCount" />',
						get: function(object){
							return object.order_count; 
						},
					},
					grand_total: {
						label: '<fmt:message key="orderTotal" />',
						get: function(object){
							return object.grand_total; 
						},
					}
					
				};
				var store = new (declare([Memory, Trackable]))({
					data: data
				});
				
				var grid = new (declare([OnDemandGrid, Editor, ColumnHider, ColumnReorder, ColumnResizer, Selector, SummaryRow]))({
					collection: store,
					selectionMode: "none",
					allowSelectAll: true,
					columns: columns
				},'grid');
				
				var totals = {
					customer: '<fmt:message key="total" />',
					order_count: '<fmt:formatNumber value="${totalCountTotal}" pattern="#,##0" />',
					grand_total: '<fmt:formatNumber value="${totalSubTotal}" pattern="#,##0" />',
				};

				grid.set('summary', totals);
				grid.startup();
				//sorting columns
				grid.on("dgrid-sort", lang.hitch(this, function(e){
					e.preventDefault();						
					var property = e.sort[0].property;
				    var order = e.sort[0].descending ? "DESC" : "ASC";
				    document.getElementById('sort').value = property + ' ' + order;
				    document.getElementById('list').submit();
				}));
				
				<c:if test="${customerFilter.sort == 'company DESC'}">
					grid.updateSortArrow([{property: 'company', descending: true}],true);
				</c:if>
				<c:if test="${customerFilter.sort == 'company ASC'}">
					grid.updateSortArrow([{property: 'company', ascending: true}],true);
				</c:if>
				
				<c:if test="${customerFilter.sort == 'order_count DESC'}">
					grid.updateSortArrow([{property: 'order_count', descending: true}],true);
				</c:if>
				<c:if test="${customerFilter.sort == 'order_count ASC'}">
					grid.updateSortArrow([{property: 'order_count', ascending: true}],true);
				</c:if>
				
				<c:if test="${customerFilter.sort == 'grand_total DESC'}">
					grid.updateSortArrow([{property: 'grand_total', descending: true}],true);
				</c:if>
				<c:if test="${customerFilter.sort == 'grand_total ASC'}">
					grid.updateSortArrow([{property: 'grand_total', ascending: true}],true);
				</c:if>
				
				grid.on('dgrid-refresh-complete', function(event) {
					$('#grid-hider-menu').appendTo('#column-hider');
				});
				
				grid.startup();
			});
		</script>
	</tiles:putAttribute>
</tiles:insertDefinition>
