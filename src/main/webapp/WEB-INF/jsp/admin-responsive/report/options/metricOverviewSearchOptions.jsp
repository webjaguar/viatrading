<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ taglib uri="http://www.springframework.org/security/tags" prefix="sec" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ taglib uri="/WEB-INF/tlds/spring.tld" prefix="spring" %>
<%@ taglib uri="/WEB-INF/tlds/custom.tld" prefix="wj" %>

<form action="metricOverviewReport.jhtm" method="post">
	<div class="modal-danger">
		<div id="search-dialog" class="modal">
			<div class="modal-dialog modal-default" role="document">
				<div class="modal-content">
					<div class="modal-header">
						<h4 class="modal-title">Search Options</h4>
						<button type="button" class="close" aria-label="Close" data-dismiss="modal">
							<span aria-hidden="true">�</span>
						</button>
					</div>
					<div class="modal-body">
						<div class="form-section">
							<div class="row">
								<div class="col-sm-6">
									<div class="form-group">
				                    		<div class="col-form-label"><fmt:message key="dateType" /></div>
				                    		<select name="dateType" class="custom-select">
											<option value="orderDate">Order Date</option>
											<option value="shipDate" <c:if test="${metricOverviewFilter.dateType == 'shipDate'}">selected</c:if>>Ship Date</option>
										</select>
									</div>
									<div class="form-group">
				                    		<div class="col-form-label"><fmt:message key="salesRep" /></div>
										<select name="salesRepId" class="custom-select">
											<c:if test="${model.salesRepTree == null}">
												<option value="-1"></option>
												<c:forEach items="${model.salesReps}" var="salesRep">
													<option value="${salesRep.id}" <c:if test="${metricOverviewFilter.salesRepId == salesRep.id}">selected</c:if>><c:out value="${salesRep.name}" /></option>
												</c:forEach>
											</c:if>
											<c:if test="${model.salesRepTree != null}">
												<c:set var="salesRepTree" value="${model.salesRepTree}" scope="request"/>
												<c:set var="metricOverviewFilterSalesRepId" value="${metricOverviewFilter.salesRepId}"/>
												<option value="${salesRepTree.id}" <c:if test="${metricOverviewFilter.salesRepId == salesRepTree.id}">selected</c:if>><c:out value="${salesRepTree.name}" /></option>
												<c:set var="recursionPageId" value="${metricOverviewFilterSalesRepId}" scope="request"/>
												<c:set var="level" value="1" scope="request"/>
												<jsp:include page="/WEB-INF/jsp/admin/report/recursion.jsp"/>	       
											</c:if>
										</select>
									</div>
									<div class="form-group">
										<div class="col-form-label"><fmt:message key="orderType" /></div>
										<select name="orderType" class="custom-select">
											<option value=""><fmt:message key="allOrders" /></option>
											<c:forTokens items="${siteConfig['ORDER_TYPE'].value}" delims="," var="type" varStatus="status">
												<option value ="${type}" <c:if test="${type == metricOverviewFilter.orderType}">selected</c:if>><c:out value ="${type}" /></option>
											</c:forTokens>
										</select>
			                    		</div>
								</div>
								<div class="col-sm-6">
									<div class="form-group">
				                    		<div class="col-form-label"><fmt:message key="quarter"/></div>
										<select name="quarter" class="custom-select">
											<option value="1" <c:if test="${metricOverviewFilter.quarter == '1'}">selected</c:if>><fmt:message key="quarter" /></option>
											<option value="0" <c:if test="${metricOverviewFilter.quarter == '0'}">selected</c:if>><fmt:message key="year" /></option>
										</select>
									</div>
									<div class="form-group">
				                    		<div class="col-form-label"><fmt:message key="year"/></div>
										<select name="year" class="custom-select">
											<c:forEach items="${model.years}" var="year"	varStatus="status">
												<option <c:if test="${metricOverviewFilter.year == year}">selected</c:if> value="${year}">${year}</option>
											</c:forEach>
										</select>
									</div>
									<c:if test="${gSiteConfig['gADD_INVOICE']}">
										<div class="form-group">
											<div class="col-form-label"><fmt:message key="generatedFrom"/></div>
											<select name="backendOrder" class="custom-select">
												<option value=""><fmt:message key="allOrders" /></option>
												<option value="1" <c:if test="${metricOverviewFilter.backendOrder == '1'}">selected</c:if>><fmt:message key="backend" /></option>
												<option value="0" <c:if test="${metricOverviewFilter.backendOrder == '0'}">selected</c:if>><fmt:message key="frontend" /></option>
											</select>
										</div>
									</c:if>
								</div>
							</div>
						</div>
					</div>
					<div class="modal-footer">
						<button class="btn btn-primary">Search</button>
					</div>
				</div>
			</div>
		</div>
	</div>
</form>