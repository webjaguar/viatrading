<%@ page import="java.net.*" %>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/security/tags" prefix="sec" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ taglib uri="/WEB-INF/tlds/custom.tld" prefix="wj" %>

<tiles:insertDefinition name="admin.responsive-template.report.customersInactive" flush="true">
	<tiles:putAttribute name="css">
		<style>
			#grid .field-customer{
				width: 180px;
			}
			#grid .field-username{
				width: 180px;
			}
			#grid .field-num_of_logins{
			    text-align: right;
			}
			#grid .field-order_count{
			    text-align: right;
			}
			#grid .field-last_ordered_date{
			    width: 150px;
			}
			#grid .field-average_order{
				width: 120px;
			    text-align: right;
			}
			#grid .field-grand_total{
				width: 120px;
			    text-align: right;
			}
			#grid .field-address{
				width: 350px;
			}
			#grid .field-phone{
				width: 120px;
			}
		</style>
	</tiles:putAttribute>
	<tiles:putAttribute name="content" type="string">  
		<sec:authorize ifAnyGranted="ROLE_AEM,ROLE_ADMIN_${prop_site_id},ROLE_REPORT_CUSTOMERS">
			<form  id="list" name="list_form" method="post">
				<div class="page-banner">
					<div class="breadcrumb"><span><a href="${_contextpath}/admin/catalog/dashboard.jhtm">Home</a></span><i class="sli-arrow-right"></i><span><a href="../reports"><fmt:message key="reports" /></a></span><i class="sli-arrow-right"></i><span><fmt:message key="inactiveCustomers" /></span></div>
				</div>
				<div class="page-head">
					<div class="row justify-content-between">
						<div class="col-sm-8 title">
							<h3><fmt:message key="inactiveCustomers" /></h3>
						</div>
						<div class="col-sm-4 actions">
		                    <div class="dropdown dropdown-setting">
								<button type="button" class="btn-outline dropdown-toggle active btn btn-primary" data-toggle="dropdown">
		                             <i class="sli-screen-desktop"></i> Display
		                         </button>	                         
								<div class="dropdown-menu dropdown-menu-right">
									<div class="form-group">
				                    		<div class="option-header">Rows</div>
										<select class="custom-select form-control" name="size" onchange="document.getElementById('page').value=1;submit();" title="Page Size">
											<c:forTokens items="10,25,50,100,200,500" delims="," var="current">
									  	  		<option value="${current}" <c:if test="${current ==  model.inActiveCustomerFilter.pageSize}">selected</c:if>>${current}</option>
								  			</c:forTokens>
										</select>
									</div>
									<div class="form-group">
				                    		<div class="option-header">Columns</div>
				                    		<div id="column-hider"></div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="page-body">
					<div class="grid-stage"> 
						<table id="grid"></table>
						<div class="footer">
			                <div class="float-right">
								<c:if test="${model.inActiveCustomerFilter.page == 1}">
							 		<span class="previous"><i class="sli-arrow-left" style="font-size: 0.7rem"></i> Previous</span>
								</c:if>
								<c:if test="${model.inActiveCustomerFilter.page != 1}">
									<a href="#" onClick="document.getElementById('page').value='${model.inActiveCustomerFilter.page-1}';document.getElementById('list').submit()">
										<span class="previous"><i class="sli-arrow-left" style="font-size: 0.7rem"></i> Previous</span>
									</a>
								</c:if>
								<span class="status"><c:out value="${model.inActiveCustomerFilter.page}" /> of <c:out value="${model.pageCount}" /></span>
								<c:if test="${model.inActiveCustomerFilter.page == model.pageCount}">
								 	<span class="next">Next <i class="sli-arrow-right" style="font-size: 0.7rem"></i></span>
								</c:if>
								<c:if test="${model.inActiveCustomerFilter.page != model.pageCount}">
									<a href="#" onClick="document.getElementById('page').value='${model.inActiveCustomerFilter.page+1}';document.getElementById('list').submit()">
										<span class="next">Next <i class="sli-arrow-right" style="font-size: 0.7rem"></i></span>
									</a>
								</c:if>
							</div>
						</div>
					</div>
				</div>
			    <input type="hidden" id="page" name="page" value="${inActiveCustomerFilter.page}"/>
				<input type="hidden" id="sort" name="sort" value="${inActiveCustomerFilter.sort}" />
			</form>
		</sec:authorize>
	</tiles:putAttribute>
	<tiles:putAttribute name="js">
		<!-- DGRID TABLE SCRIPT MUST BE AT BOTTOM OF THE PAGE -->
		<script src="${_contextpath}/admin-responsive-static/assets/plugins/dojo/dojo.js" data-dojo-config="async: true"></script>
		<script>
			//Table Data Sanitization
			var data = [];
			<c:forEach items="${model.inactiveCustomerReports}" var="inactiveCustomer" varStatus="status">
				data.push({
					firstName: '${wj:escapeJS(inactiveCustomer.firstName)}',
					lastName: '${wj:escapeJS(inactiveCustomer.lastName)}',
					username: '${inactiveCustomer.username}',
					created: '<fmt:formatDate type="date" timeStyle="default" value="${inactiveCustomer.customerCreated}"/>',
					num_of_logins: '${inactiveCustomer.numOfLogins}',
					order_count: "${inactiveCustomer.orderCount}",
					last_ordered_date: '<fmt:formatDate type="date" timeStyle="default" value="${inactiveCustomer.lastOrderedDate}"/>',
					average_order: decodeHTML('<fmt:message key="${siteConfig[\'CURRENCY\'].value}" />') + '<fmt:formatNumber value="${inactiveCustomer.averageOrder}" pattern="#,##0.00" />',
					grand_total: decodeHTML('<fmt:message key="${siteConfig[\'CURRENCY\'].value}" />') + '<fmt:formatNumber value="${inactiveCustomer.ordersGrandTotal}" pattern="#,##0.00" />',
					address: "${wj:escapeJS(inactiveCustomer.addressToString)}",
					phone: "${wj:escapeJS(inactiveCustomer.address.phone)}",
					salesRep: '${wj:escapeJS(model.salesRepMap[inactiveCustomer.salesRepId].name)}',
				});
			</c:forEach>

			//Columns Definitions
			require(['dojo/_base/lang',
					'dojo/_base/declare',
					'dstore/Memory',
					'dstore/Trackable',
					'dgrid/OnDemandGrid',
					'dgrid/Editor',
					'dgrid/extensions/ColumnHider',
					'dgrid/extensions/ColumnReorder',
					'dgrid/extensions/ColumnResizer',
					'dgrid/Selector',
					'dijit/form/DropDownButton',
					'dijit/DropDownMenu',
					'dijit/MenuItem',
					'summary/SummaryRow'
				], function (lang, declare, Memory, Trackable, OnDemandGrid, Editor, ColumnHider, ColumnReorder, ColumnResizer, Selector, DropDownButton, DropDownMenu, MenuItem, SummaryRow) {
				var columns = {
					customer: {
						label: '<fmt:message key="customer" />',
						get: function(object){
							return object.firstName+' '+object.lastName;
						},
						sortable: false,
					},
					username: {
						label: '<fmt:message key="email" />',
						get: function(object){
							return object.username;
						},
					},
					created: {
						label: '<fmt:message key="date" />',
						get: function(object){
							return object.created;
						},
					},
					num_of_logins:{
						label: '<fmt:message key="numOfLogins" />',
						get: function(object){
							return object.num_of_logins;
						},
					},
					order_count:{
						label: '<fmt:message key="orderCount" />',
						get: function(object){
							return object.order_count;
						},
					},
					last_ordered_date:{
						label: '<fmt:message key="lastOrderedDate" />',
						get: function(object){
							return object.last_ordered_date;
						},
					},
					average_order: {
						label: '<fmt:message key="averageOrder" />',
						get: function(object){
							return object.average_order;
						},
					},
					grand_total: {
						label: '<fmt:message key="orderTotal" />',
						get: function(object){
							return object.grand_total;
						},
					},
					address: {
						label: '<fmt:message key="address" />',
						get: function(object){
							return object.address;
						},
						sortable: false
					},
					phone: {
						label: '<fmt:message key="phone" />',
						get: function(object){
							return object.phone;
						},
						sortable: false
					},
					salesRep: {
						label: '<fmt:message key="salesRep" />',
						get: function(object){
							return object.salesRep;
						},
						sortable: false
					},
				};
				var store = new (declare([Memory, Trackable]))({
					data: data
				});
				
				var grid = new (declare([OnDemandGrid, Editor, ColumnHider, ColumnReorder, ColumnResizer, Selector, SummaryRow]))({
					collection: store,
					selectionMode: "none",
					allowSelectAll: true,
					columns: columns
				},'grid');
				grid.startup();
				//sorting columns
				grid.on("dgrid-sort", lang.hitch(this, function(e){
					e.preventDefault();						
					var property = e.sort[0].property;
				    var order = e.sort[0].descending ? "DESC" : "ASC";
				    document.getElementById('sort').value = property + ' ' + order;
				    document.getElementById('list').submit();
				}));
				
				<c:if test="${inActiveCustomerFilter.sort == 'username DESC'}">
					grid.updateSortArrow([{property: 'username', descending: true}],true);
				</c:if>
				<c:if test="${inActiveCustomerFilter.sort == 'username ASC'}">
					grid.updateSortArrow([{property: 'username', ascending: true}],true);
				</c:if>
				
				<c:if test="${inActiveCustomerFilter.sort == 'created DESC'}">
					grid.updateSortArrow([{property: 'created', descending: true}],true);
				</c:if>
				<c:if test="${inActiveCustomerFilter.sort == 'created ASC'}">
					grid.updateSortArrow([{property: 'created', ascending: true}],true);
				</c:if>
				
				<c:if test="${inActiveCustomerFilter.sort == 'num_of_logins DESC'}">
					grid.updateSortArrow([{property: 'num_of_logins', descending: true}],true);
				</c:if>
				<c:if test="${inActiveCustomerFilter.sort == 'num_of_logins ASC'}">
					grid.updateSortArrow([{property: 'num_of_logins', ascending: true}],true);
				</c:if>
				
				<c:if test="${inActiveCustomerFilter.sort == 'order_count DESC'}">
					grid.updateSortArrow([{property: 'order_count', descending: true}],true);
				</c:if>
				<c:if test="${inActiveCustomerFilter.sort == 'order_count ASC'}">
					grid.updateSortArrow([{property: 'order_count', ascending: true}],true);
				</c:if>
				
				<c:if test="${inActiveCustomerFilter.sort == 'last_ordered_date DESC'}">
					grid.updateSortArrow([{property: 'last_ordered_date', descending: true}],true);
				</c:if>
				<c:if test="${inActiveCustomerFilter.sort == 'last_ordered_date ASC'}">
					grid.updateSortArrow([{property: 'last_ordered_date', ascending: true}],true);
				</c:if>
				
				<c:if test="${inActiveCustomerFilter.sort == 'average_order DESC'}">
					grid.updateSortArrow([{property: 'average_order', descending: true}],true);
				</c:if>
				<c:if test="${inActiveCustomerFilter.sort == 'average_order ASC'}">
					grid.updateSortArrow([{property: 'average_order', ascending: true}],true);
				</c:if>
				
				<c:if test="${inActiveCustomerFilter.sort == 'grand_total DESC'}">
					grid.updateSortArrow([{property: 'grand_total', descending: true}],true);
				</c:if>
				<c:if test="${inActiveCustomerFilter.sort == 'grand_total ASC'}">
					grid.updateSortArrow([{property: 'grand_total', ascending: true}],true);
				</c:if>
				
				grid.on('dgrid-refresh-complete', function(event) {
					$('#grid-hider-menu').appendTo('#column-hider');
				});
				
				grid.startup();
			});
		</script>
	</tiles:putAttribute>
</tiles:insertDefinition>
