<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ taglib uri="http://www.springframework.org/security/tags" prefix="sec" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ taglib uri="/WEB-INF/tlds/spring.tld" prefix="spring" %>
<%@ taglib uri="/WEB-INF/tlds/custom.tld" prefix="wj" %>

<tiles:insertDefinition name="admin.responsive-template.report.orderDetail" flush="true">
	<tiles:putAttribute name="css">
		<style>
			#grid .dgrid-cell{
				text-align:right;
			}
			#grid .dgrid-row.summary .dgrid-cell{
			 	font-weight: 700;
			}
			#grid .field-month{
				width: 60px;
				text-align:left;
			}
			#grid .field-merchandize{
				width: 90px;
			}
			#grid .field-ccFee{
				width: 110px;
			}
			#grid .field-grandTotal{
				width: 90px;
			}
			#grid .field-uniqueUser{
				width: 90px;
			}
			#grid .field-newUserActive{
				width: 110px;
			}
			#grid .field-avgGrandTotal{
				width: 100px;
			}
			#grid .field-avgPerCustomer{
				width: 120px;
			}	
		</style>
	</tiles:putAttribute>
	<tiles:putAttribute name="content" type="string">
		<c:if test="${gSiteConfig['gREPORT']}">
			<sec:authorize ifAnyGranted="ROLE_AEM,ROLE_ADMIN_${prop_site_id},ROLE_REPORT_ORDER_OVERVIEW">
				<form name="customerList" id="list">
					<div class="page-banner">
						<div class="breadcrumb"><span><a href="${_contextpath}/admin/catalog/dashboard.jhtm">Home</a></span><i class="sli-arrow-right"></i><span><a href="../reports"><fmt:message key="reports" /></a></span><i class="sli-arrow-right"></i><span><fmt:message key="ordersOverview" /></span></div>
					</div>
					<div class="page-head">
						<div class="row justify-content-between">
							<div class="col-sm-8 title">
								<h3><fmt:message key="ordersOverview" /></h3>
							</div>
							<div class="col-sm-4 actions">
			                    <div class="dropdown dropdown-setting">
									<button type="button" class="btn-outline dropdown-toggle active btn btn-primary" data-toggle="dropdown">
			                             <i class="sli-screen-desktop"></i> Display
			                         </button>	                         
									<div class="dropdown-menu dropdown-menu-right">
										<div class="form-group">
					                    		<div class="option-header">Columns</div>
					                    		<div id="column-hider"></div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
					<div class="page-body">
						<div class="grid-stage grid-stage-sm"> 
							<table id="grid"></table>
						</div>
					</div>
				</form>
			</sec:authorize>
		</c:if>
	 </tiles:putAttribute>    
	<tiles:putAttribute name="js">
		<!-- DGRID TABLE SCRIPT MUST BE AT BOTTOM OF THE PAGE -->
		<script src="${_contextpath}/admin-responsive-static/assets/plugins/dojo/dojo.js" data-dojo-config="async: true"></script>
		<script type="text/javascript">
				//Table Data Sanitization
				var data = [];
				<c:set var="merchandizeTotal" value="0"/>
			    <c:set var="shippingCostTotal" value="0"/>
			    <c:set var="ccFeeTotal" value="0"/>
			    <c:set var="taxTotal" value="0"/>
			    <c:set var="grandTotalTotal" value="0"/>
			    <c:set var="numOrderTotal" value="0"/>
			    <c:set var="newUserTotal" value="0"/>
			    <c:set var="uniqueUserTotal" value="0"/>
			    <c:set var="newUserActiveTotal" value="0"/>
			    <c:set var="avgPerOrderTotal" value="0"/>
			    <c:set var="avgPerCustomerTotal" value="0"/>
			    <c:set var="numCustomerActivatedTotal" value="0"/>
			    <c:set var="numCustomerRetainedTotal" value="0"/>
			    	<c:forEach items="${model.salesDetailReportsList}" var="saleDetailReport" varStatus="status">
			    		<c:if test="${status.index % 3 == 0}">
				  	    <c:set var="merchandize" value="0"/>
					    <c:set var="shippingCost" value="0"/>
					    <c:set var="ccFee" value="0"/>
					    <c:set var="tax" value="0"/>
					    <c:set var="grandTotal" value="0"/>
					    <c:set var="numOrder" value="0"/>
					    <c:set var="newUser" value="0"/>
					    <c:set var="uniqueUser" value="0"/>
					    <c:set var="newUserActive" value="0"/>
					    <c:set var="avgPerOrder" value="0"/>
					    <c:set var="avgPerCustomer" value="0"/>
					    <c:set var="numCustomerActivated" value="0"/>
					    <c:set var="numCustomerRetained" value="0"/>
				  	</c:if>
				    
				    <c:set var="merchandize" value="${saleDetailReport.merchandize + merchandize}" />
				    <c:set var="merchandizeTotal" value="${saleDetailReport.merchandize + merchandizeTotal}" />
				    <c:set var="shippingCost" value="${saleDetailReport.shippingCost + shippingCost}" />
				    <c:set var="shippingCostTotal" value="${saleDetailReport.shippingCost + shippingCostTotal}" />
				    <c:set var="ccFee" value="${saleDetailReport.ccFee + ccFee}" />
				    <c:set var="ccFeeTotal" value="${saleDetailReport.ccFee + ccFeeTotal}" />
				    <c:set var="tax" value="${saleDetailReport.tax + tax}" />
				    <c:set var="taxTotal" value="${saleDetailReport.tax + taxTotal}" />
				    <c:set var="grandTotal" value="${saleDetailReport.grandTotal + grandTotal}" />
				    <c:set var="grandTotalTotal" value="${saleDetailReport.grandTotal + grandTotalTotal}" />
				    <c:set var="numOrder" value="${saleDetailReport.numOrder + numOrder}" /><c:set var="numOrderTotal" value="${saleDetailReport.numOrder + numOrderTotal}" />
				    <c:set var="uniqueUser" value="${saleDetailReport.uniqueUser + uniqueUser}" />
				    <c:set var="uniqueUserTotal" value="${saleDetailReport.uniqueUser + uniqueUserTotal}" />
				    <c:set var="newUser" value="${saleDetailReport.newUser + newUser}" />
				    <c:set var="newUserTotal" value="${saleDetailReport.newUser + newUserTotal}" />
				    <c:set var="newUserActive" value="${saleDetailReport.newUserActive + newUserActive}" />
				    <c:set var="newUserActiveTotal" value="${saleDetailReport.newUserActive + newUserActiveTotal}" />
				    <c:set var="avgPerOrder" value="${saleDetailReport.avgGrandTotal + avgPerOrder}" />
				    <c:set var="avgPerOrderTotal" value="${saleDetailReport.avgGrandTotal + avgPerOrderTotal}" />				
				    <c:set var="avgPerCustomer" value="${saleDetailReport.avgPerCustomer + avgPerCustomer}" />
				    <c:set var="avgPerCustomerTotal" value="${saleDetailReport.avgPerCustomer + avgPerCustomerTotal}" />
				    <c:set var="numCustomerActivated" value="${saleDetailReport.numCustomerActivated + numCustomerActivated}" />
				    <c:set var="numCustomerActivatedTotal" value="${saleDetailReport.numCustomerActivated + numCustomerActivatedTotal}" />		
				    <c:set var="numCustomerRetained" value="${saleDetailReport.numCustomerRetained + numCustomerRetained}" />
				    <c:set var="numCustomerRetainedTotal" value="${saleDetailReport.numCustomerRetained + numCustomerRetainedTotal}" />		
				  	
					data.push({
						month: '${saleDetailReport.month}',
						merchandize: decodeHTML('<fmt:message key="${siteConfig[\'CURRENCY\'].value}" />') + '<fmt:formatNumber value="${saleDetailReport.merchandize}" pattern="#,##0.00" />',
						shippingCost: decodeHTML('<fmt:message key="${siteConfig[\'CURRENCY\'].value}" />') + '<fmt:formatNumber value="${saleDetailReport.shippingCost}" pattern="#,##0.00" />',
						ccFee: decodeHTML('<fmt:message key="${siteConfig[\'CURRENCY\'].value}" />') + '<fmt:formatNumber value="${saleDetailReport.ccFee}" pattern="#,##0.00" />',
						tax: decodeHTML('<fmt:message key="${siteConfig[\'CURRENCY\'].value}" />') + '<fmt:formatNumber value="${saleDetailReport.tax}" pattern="#,##0.00" />',
						grandTotal: decodeHTML('<fmt:message key="${siteConfig[\'CURRENCY\'].value}" />') + '<fmt:formatNumber value="${saleDetailReport.grandTotal}" pattern="#,##0.00" />',
						numOrder: '<fmt:formatNumber value="${saleDetailReport.numOrder}" pattern="#,##0" />',
						uniqueUser: '<fmt:formatNumber value="${saleDetailReport.uniqueUser}" pattern="#,##0" />',
						newUser: '<fmt:formatNumber value="${saleDetailReport.newUser}" pattern="#,##0" />',
						newUserActive: '<fmt:formatNumber value="${saleDetailReport.newUserActive}" pattern="#,##0" />',
						avgGrandTotal: decodeHTML('<fmt:message key="${siteConfig[\'CURRENCY\'].value}" />') + '<fmt:formatNumber value="${saleDetailReport.avgGrandTotal}" pattern="#,##0.00" />',
						avgPerCustomer: decodeHTML('<fmt:message key="${siteConfig[\'CURRENCY\'].value}" />') + '<fmt:formatNumber value="${saleDetailReport.avgPerCustomer}" pattern="#,##0.00" />',
						numCustomerActivated: '<fmt:formatNumber value="${saleDetailReport.numCustomerActivated}" pattern="#,##0" />',
						numCustomerRetained: '<fmt:formatNumber value="${saleDetailReport.numCustomerRetained}" pattern="#,##0" />',
						quarter_summary: false,
					});
					
					<c:if test="${status.index % 3 == 2}">
						<c:set var="quarterIndex" value="${quarterIndex + 1}" />
						data.push({
							month: 'Q<c:out value="${quarterIndex}" />',
							merchandize: decodeHTML('<fmt:message key="${siteConfig[\'CURRENCY\'].value}" />') + '<fmt:formatNumber value="${merchandize}" pattern="#,##0.00" />',
							shippingCost: decodeHTML('<fmt:message key="${siteConfig[\'CURRENCY\'].value}" />') + '<fmt:formatNumber value="${shippingCost}" pattern="#,##0.00" />',
							ccFee: decodeHTML('<fmt:message key="${siteConfig[\'CURRENCY\'].value}" />') + '<fmt:formatNumber value="${ccFee}" pattern="#,##0.00" />',
							tax: decodeHTML('<fmt:message key="${siteConfig[\'CURRENCY\'].value}" />') + '<fmt:formatNumber value="${tax}" pattern="#,##0.00" />',
							grandTotal: decodeHTML('<fmt:message key="${siteConfig[\'CURRENCY\'].value}" />') + '<fmt:formatNumber value="${grandTotal}" pattern="#,##0.00" />',
							numOrder: '<fmt:formatNumber value="${numOrder}" pattern="#,##0" />',
							uniqueUser: '<fmt:formatNumber value="${uniqueUser}" pattern="#,##0" />',
							newUser: '<fmt:formatNumber value="${newUser}" pattern="#,##0" />',
							newUserActive: '<fmt:formatNumber value="${newUserActive}" pattern="#,##0" />',
							avgGrandTotal: decodeHTML('<fmt:message key="${siteConfig[\'CURRENCY\'].value}" />') + '[<fmt:formatNumber value="${grandTotal/numOrder}" pattern="#,##0.00" />]',
							avgPerCustomer: decodeHTML('<fmt:message key="${siteConfig[\'CURRENCY\'].value}" />') + '<fmt:formatNumber value="${avgPerCustomer}" pattern="#,##0.00" />',
							numCustomerActivated: '<fmt:formatNumber value="${numCustomerActivated}" pattern="#,##0" />',
							numCustomerRetained: '<fmt:formatNumber value="${numCustomerRetained}" pattern="#,##0" />',
							quarter_summary: true,
						});
					</c:if>
				</c:forEach>
	
				//Columns Definitions
				require(['dojo/_base/lang',
						'dojo/_base/declare',
						'dstore/Memory',
						'dstore/Trackable',
						'dgrid/OnDemandGrid',
						'dgrid/Editor',
						'dgrid/extensions/ColumnHider',
						'dgrid/extensions/ColumnReorder',
						'dgrid/extensions/ColumnResizer',
						'dgrid/Selector',
						'dijit/form/DropDownButton',
						'dijit/DropDownMenu',
						'dijit/MenuItem',
						'summary/SummaryRow',
					], function (lang, declare, Memory, Trackable, OnDemandGrid, Editor, ColumnHider, ColumnReorder, ColumnResizer, Selector, DropDownButton, DropDownMenu, MenuItem, SummaryRow) {
					var columns = {
						month: {
							label: '<fmt:message key="month" />',
							get: function(object){
								return object.month; 
							},
							sortable: false
						},
						merchandize: {
							label: '<fmt:message key="merchandise" />',
							renderCell: function(object){
								return object.merchandize;
							},
							sortable: false
						},
						shippingCost: {
							label: '<fmt:message key="shipping" />',
							get: function(object){
								return object.shippingCost; 
							},
							sortable: false
						},
						ccFee: {
							label: '<fmt:message key="ccFee" />',
							get: function(object){
								return object.ccFee;
							},
							sortable: false
						},
						tax: {
							label: '<fmt:message key="tax" />',
							get: function(object){ 
								return object.tax;
							},
							sortable: false
						},
						grandTotal: {
							label: '<fmt:message key="grandTotal" />',
							get: function(object){
								return object.grandTotal;
							},
							sortable: false
						},
						numOrder: {
							label: '<fmt:message key="orders" />',
							get: function(object){
								return object.numOrder;
							},
							sortable: false
						},
						uniqueUser: {
							label: '<fmt:message key="uniqueUser" />',
							get: function(object){
								return object.uniqueUser;
							},
							sortable: false
						},
						newUser: {
							label: '<fmt:message key="newUser" />',
							get: function(object){
								return object.newUser;
							},
							sortable: false
						},
						newUserActive: {
							label: '<fmt:message key="newUserActive" />',
							get: function(object){
								return object.newUserActive;
							},
							sortable: false
						},
						avgGrandTotal: {
							label: '<fmt:message key="averagePerOrder" />',
							get: function(object){
								return object.avgGrandTotal;
							},
							sortable: false
						},
						avgPerCustomer: {
							label: '<fmt:message key="averagePerCustomer" />',
							get: function(object){
								return object.avgPerCustomer;
							},
							sortable: false
						},
						numCustomerActivated: {
							label: 'Activated',
							get: function(object){
								return object.numCustomerActivated;
							},
							sortable: false
						},
						numCustomerRetained: {
							label: 'Retained',
							get: function(object){
								return object.numCustomerRetained;
							},
							sortable: false
						},
					};
					var store = new (declare([Memory, Trackable]))({
						data: data
					});
					
					var grid = new (declare([OnDemandGrid, Editor, ColumnHider, ColumnReorder, ColumnResizer, Selector, SummaryRow]))({
						collection: store,
						selectionMode: "none",
						allowSelectAll: true,
						columns: columns,
						renderRow: function (object) {
							var rowElement = this.inherited(arguments);
					        if(object.quarter_summary){
					       	 	rowElement.className += ' summary';
					        }
					        return rowElement;
					    }
					},'grid');
					
					var totals = {
						month: '<fmt:message key="total" />',
						merchandize: decodeHTML('<fmt:message key="${siteConfig[\'CURRENCY\'].value}" />') + '<fmt:formatNumber value="${merchandizeTotal}" pattern="#,##0.00" />',
						shippingCost: decodeHTML('<fmt:message key="${siteConfig[\'CURRENCY\'].value}" />') + '<fmt:formatNumber value="${shippingCostTotal}" pattern="#,##0.00" />',
						ccFee: decodeHTML('<fmt:message key="${siteConfig[\'CURRENCY\'].value}" />') + '<fmt:formatNumber value="${ccFeeTotal}" pattern="#,##0.00" />',
						tax: decodeHTML('<fmt:message key="${siteConfig[\'CURRENCY\'].value}" />') + '<fmt:formatNumber value="${taxTotal}" pattern="#,##0.00" />',
						grandTotal: decodeHTML('<fmt:message key="${siteConfig[\'CURRENCY\'].value}" />') + '<fmt:formatNumber value="${grandTotalTotal}" pattern="#,##0.00" />',
						numOrder: '<fmt:formatNumber value="${numOrderTotal}" pattern="#,##0" />',
						uniqueUser: '<fmt:formatNumber value="${uniqueUserTotal}" pattern="#,##0" />',
						newUser: '<fmt:formatNumber value="${newUserTotal}" pattern="#,##0" />',
						newUserActive: '<fmt:formatNumber value="${newUserActiveTotal}" pattern="#,##0" />',
						avgGrandTotal: decodeHTML('<fmt:message key="${siteConfig[\'CURRENCY\'].value}" />') + '[<fmt:formatNumber value="${grandTotalTotal/numOrderTotal}" pattern="#,##0.00" />]',
						avgPerCustomer: decodeHTML('<fmt:message key="${siteConfig[\'CURRENCY\'].value}" />') + '<fmt:formatNumber value="${avgPerCustomerTotal}" pattern="#,##0.00" />',
						numCustomerActivated: '<fmt:formatNumber value="${numCustomerActivatedTotal}" pattern="#,##0" />',
						numCustomerRetained: '<fmt:formatNumber value="${numCustomerRetainedTotal}" pattern="#,##0" />',
						quarter_summary: true,
					};

					grid.set('summary', totals);
					
					grid.on('dgrid-refresh-complete', function(event) {
						$('#grid-hider-menu').appendTo('#column-hider');
					});
					
					grid.startup();
				});
		</script>
	</tiles:putAttribute>
</tiles:insertDefinition>