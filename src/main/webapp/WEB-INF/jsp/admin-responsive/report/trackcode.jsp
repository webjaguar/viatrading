<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ taglib uri="http://www.springframework.org/security/tags" prefix="sec" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ taglib uri="/WEB-INF/tlds/spring.tld" prefix="spring" %>
<%@ taglib uri="/WEB-INF/tlds/custom.tld" prefix="wj" %>

<tiles:insertDefinition name="admin.responsive-template.report.trackcode" flush="true">
	<tiles:putAttribute name="css">
		<style>
			#grid .field-grandTotal{
				text-align: right;
			}
			#grid .field-numOrder{
				text-align: right;
			}
			#grid .field-uniqueUser{
				text-align: right;
			}
		</style>
	</tiles:putAttribute>
  <tiles:putAttribute name="content" type="string">
		<sec:authorize ifAnyGranted="ROLE_AEM,ROLE_ADMIN_${prop_site_id},ROLE_REPORT_TRACKCODE">
			<form name="list" id="list" action="">
				<div class="page-banner">
					<div class="breadcrumb"><span><a href="${_contextpath}/admin/catalog/dashboard.jhtm">Home</a></span><i class="sli-arrow-right"></i><span><a href="../reports"><fmt:message key="reports" /></a></span><i class="sli-arrow-right"></i><span>Track Code</span></div>
				</div>
				<div class="page-head">
					<div class="row justify-content-between">
						<div class="col-sm-8 title">
							<h3>Track Code</h3>
						</div>
						<div class="col-sm-4 actions">
		                    <div class="dropdown dropdown-setting">
								<button type="button" class="btn-outline dropdown-toggle active btn btn-primary" data-toggle="dropdown">
		                             <i class="sli-screen-desktop"></i> Display
		                         </button>	                         
								<div class="dropdown-menu dropdown-menu-right">
									<div class="form-group">
				                    		<div class="option-header">Rows</div>
										<select class="custom-select" name="size"  onchange="document.getElementById('page').value=1;submit()">
											<c:forTokens items="10,25,50,100,200,500" delims="," var="current">
												<option value="${current}" <c:if test="${current == model.trackcodeReport.pageSize}">selected</c:if>>${current} <fmt:message key="perPage" /></option>
											</c:forTokens>
										</select>
									</div>
									<div class="form-group">
				                    		<div class="option-header">Columns</div>
				                    		<div id="column-hider"></div>
									</div>
								</div>
		                    </div>
						</div>
					</div>
				</div>
				<div class="page-body">
					<div class="grid-stage">
						<c:if test="${model.trackcodeReport.nrOfElements > 0}">
							<div class="page-stat">
								<fmt:message key="showing">
									<fmt:param value="${model.trackcodeReport.firstElementOnPage + 1}" />
									<fmt:param value="${model.trackcodeReport.lastElementOnPage + 1}" />
									<fmt:param value="${model.trackcodeReport.nrOfElements}" />
								</fmt:message>
							</div> 
						</c:if>
						<table id="grid"></table>
						<div class="footer">
							<div class="float-right">
								<c:if test="${model.trackcodeReport.firstPage}">
									<span class="previous"><i class="sli-arrow-left" style="font-size: 0.7rem"></i> Previous</span>
								</c:if>
								<c:if test="${not model.trackcodeReport.firstPage}">
									<a href="<c:url value="trackcodeReport.jhtm"><c:param name="page" value="${model.trackcodeReport.page}"/><c:param name="size" value="${model.trackcodeReport.pageSize}"/></c:url>">
										<span class="previous"><i class="sli-arrow-left" style="font-size: 0.7rem"></i> Previous</span>
									</a>
								</c:if>	
								<span class="status"><c:out value="${trackcodeReport.page + 1}" /> of <c:out value="${model.trackcodeReport.pageCount}" /></span>
								<c:if test="${model.trackcodeReport.lastPage}">
									<span class="next">Next <i class="sli-arrow-right" style="font-size: 0.7rem"></i></span>
								</c:if>
								<c:if test="${not model.trackcodeReport.lastPage}">
									<a href="<c:url value="trackcodeReport.jhtm"><c:param name="page" value="${model.trackcodeReport.page+2}"/><c:param name="size" value="${model.trackcodeReport.pageSize}"/></c:url>">
										<span class="next">Next <i class="sli-arrow-right" style="font-size: 0.7rem"></i></span>
									</a>
								</c:if>
							</div>
						</div>
					</div>
				</div>
				<input type="hidden" id="page" name="page" value="${model.trackcodeReport.page+1}"/>
				<input type="hidden" id="sort" name="sort" value="${trackcodeFilter.sort}" />
			</form>
		</sec:authorize>
	</tiles:putAttribute>
	<tiles:putAttribute name="js">
		<!-- DGRID TABLE SCRIPT MUST BE AT BOTTOM OF THE PAGE -->
		<script src="${_contextpath}/admin-responsive-static/assets/plugins/dojo/dojo.js" data-dojo-config="async: true"></script>
		<script>
			//Table Data Sanitization
			var data = [];
			<c:forEach items="${model.trackcodeReport.pageList}" var="trackcode" varStatus="status">
				data.push({
					offset: '${status.count + model.trackcodeReport.firstElementOnPage}.',
					trackcode: '${trackcode.trackcode}',
					grandTotal: decodeHTML('<fmt:message key="${siteConfig[\'CURRENCY\'].value}" />') + '<fmt:formatNumber value="${trackcode.grandTotal}" pattern="#,##0.00" />',
					numOrder: '${trackcode.numOrder}',
					uniqueUser: '${trackcode.uniqueUser}',
				});
			</c:forEach>
			
			require(["dgrid/List", "dgrid/OnDemandGrid", "dgrid/Selection", "dgrid/Keyboard", "dgrid/Editor", "dijit/form/CheckBox", 
					"dijit/form/TimeTextBox", "dijit/form/Select", "dojo/data/ObjectStore", "dijit/form/FilteringSelect",
					'dijit/form/DropDownButton', 'dijit/DropDownMenu', 'dijit/MenuItem',
					"dojo/_base/declare", "dojo/_base/lang", "dojo/store/Memory", "dojo/store/Observable", "dgrid/test/data/createSyncStore", 
					'dgrid/extensions/ColumnHider', 'dgrid/extensions/ColumnReorder', 'dgrid/extensions/ColumnResizer',
				], function(List, OnDemandGrid, Selection, Keyboard, Editor, CheckBox, TimeTextBox, Select, ObjectStore, FilteringSelect,
						DropDownButton, DropDownMenu, MenuItem,
						declare, lang, LegacyMemory, LegacyObservable, createSyncStore, ColumnHider, ColumnReorder, ColumnResizer){
	
				//Column Definitions
				var columns = {
					offset: {
						label: '#',
						get: function(object){
							return object.offset;
						},
						sortable: false,
						unhidable: true
					},
					trackcode: {
						label: '<fmt:message key="trackcode" />',
						get: function(object){
							return object.trackcode;
						},
						sortable: false,
					},
					grandTotal: {
						label: '<fmt:message key="grandTotal" />',
						get: function(object){
							return object.grandTotal;
						},
						sortable: false,
					},
					numOrder: {
						label: '# <fmt:message key="order" />',
						get: function(object){
							return object.numOrder;
						},
						sortable: false,
					},
					uniqueUser: {
						label: '<fmt:message key="uniqueUser" />',
						get: function(object){
							return object.uniqueUser;
						},
						sortable: false,
					},
				};
				
				var grid = new (declare([OnDemandGrid, Selection, Keyboard, Editor, ColumnHider, ColumnReorder, ColumnResizer]))({
					collection: createSyncStore({ data: data }),
					columns: columns,
					selectionMode: "none",
				},'grid');
				
				grid.on('dgrid-refresh-complete', function(event) {
					$('#grid-hider-menu').appendTo('#column-hider');
				});
				
				grid.startup();
			});
		</script>
	</tiles:putAttribute>
</tiles:insertDefinition>
