<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ taglib uri="http://www.springframework.org/security/tags" prefix="sec" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ taglib uri="/WEB-INF/tlds/spring.tld" prefix="spring" %>
<%@ taglib uri="/WEB-INF/tlds/custom.tld" prefix="wj" %>

<tiles:insertDefinition name="admin.responsive-template.report.abandonedShoppingCart" flush="true">
	<tiles:putAttribute name="css">
		<style>
			#grid .field-email{
				width: 200px;
			}
			#grid .field-action{
				text-align: center;
			}
			#grid .field-removeCart{
				text-align: center;
			}
			#grid .field-removeCart a{
				color: #d32f2f;
			}
			#grid .field-totalAmount{
				text-align: right;
			}
		</style>
		
	</tiles:putAttribute>
	<tiles:putAttribute name="content" type="string">
		<sec:authorize ifAnyGranted="ROLE_AEM,ROLE_ADMIN_${prop_site_id},ROLE_REPORT_ABANDONED_SHOPPING_CART">
			<form id="abandonedShoppingCart" action="abandonedShoppingCartController.jhtm" method="post">
				<div class="page-banner">
					<div class="breadcrumb"><span><a href="${_contextpath}/admin/catalog/dashboard.jhtm">Home</a></span><i class="sli-arrow-right"></i><span><a href="../reports"><fmt:message key="reports" /></a></span><i class="sli-arrow-right"></i><span><fmt:message key="abandonedShoppingCart"/></span></div>
				</div>
				<div class="page-head">
					<div class="row justify-content-between">
						<div class="col-sm-8 title">
							<h3><fmt:message key="abandonedShoppingCart"/></h3>
						</div>
						<div class="col-sm-4 actions">
		                    <div class="dropdown dropdown-setting">
								<button type="button" class="btn-outline dropdown-toggle active btn btn-primary" data-toggle="dropdown">
		                             <i class="sli-screen-desktop"></i> Display
		                         </button>	                         
								<div class="dropdown-menu dropdown-menu-right">
									<div class="form-group">
				                    		<div class="option-header">Rows</div>
										<select class="custom-select" name="size"  onchange="document.getElementById('page').value=1;submit()">
											<c:forTokens items="10,25,50,100" delims="," var="current">
												<option value="${current}" <c:if test="${current == abandonedShoppingCarts.pageSize}">selected</c:if>>${current} <fmt:message key="perPage" /></option>
											</c:forTokens>
										</select>
									</div>
									<div class="form-group">
				                    		<div class="option-header">Columns</div>
				                    		<div id="column-hider"></div>
									</div>
								</div>
		                    </div>
						</div>
					</div>
				</div>
				<div class="page-body">
					<div class="grid-stage">
						<c:if test="${model.abandonedShoppingCarts.nrOfElements > 0}">
							<div class="page-stat">
								<fmt:message key="showing">
									<fmt:param value="${model.abandonedShoppingCarts.firstElementOnPage + 1}"/>
									<fmt:param value="${model.abandonedShoppingCarts.lastElementOnPage + 1}"/>
									<fmt:param value="${model.abandonedShoppingCarts.nrOfElements}"/>
								</fmt:message>
							</div>
						</c:if>
						<table id="grid"></table>
						<div class="footer">
							<div class="float-right">
								<c:if test="${model.abandonedShoppingCarts.firstPage}">
									<span class="previous"><i class="sli-arrow-left" style="font-size: 0.7rem"></i> Previous</span>
								</c:if>
								<c:if test="${not model.abandonedShoppingCarts.firstPage}">
									<a href="<c:url value="abandonedShoppingCartController.jhtm"><c:param name="page" value="${model.abandonedShoppingCarts.page}"/><c:if test="${not model.search['homePage']}"><c:param name="parent" value="${model.search['parent']}"/></c:if></c:url>">
										<span class="previous"><i class="sli-arrow-left" style="font-size: 0.7rem"></i> Previous</span>
									</a>
								</c:if>	
								<span class="status"><c:out value="${model.abandonedShoppingCarts.page+1}" /> of <c:out value="${model.abandonedShoppingCarts.pageCount}" /></span>
								<c:if test="${model.abandonedShoppingCarts.lastPage}">
									<span class="next">Next <i class="sli-arrow-right" style="font-size: 0.7rem"></i></span>
								</c:if>
								<c:if test="${not model.abandonedShoppingCarts.lastPage}">
									<a href="<c:url value="abandonedShoppingCartController.jhtm"><c:param name="page" value="${model.abandonedShoppingCarts.page+2}"/><c:param name="size" value="${model.abandonedShoppingCarts.pageSize}"/></c:url>">
										<span class="next">Next <i class="sli-arrow-right" style="font-size: 0.7rem"></i></span>
									</a>
								</c:if>
							</div>
						</div>
					</div>
				</div>
				<input type="hidden" id="page" name="page" value="${model.abandonedShoppingCarts.page}"/>
				<input type="hidden" id="sort" name="sort" value="${abandonedShoppingCartSearch.sort}"/>
			</form>
		</sec:authorize>
	</tiles:putAttribute>
	<tiles:putAttribute name="js">
		<!-- DGRID TABLE SCRIPT MUST BE AT BOTTOM OF THE PAGE -->
		<script src="${_contextpath}/admin-responsive-static/assets/plugins/dojo/dojo.js" data-dojo-config="async: true"></script>
		<script>
			//Table Data Sanitization
			var data = [];
			<c:forEach items="${model.abandonedShoppingCarts.pageList}" var="abandonedShoppingCart" varStatus="status">
				data.push({
					offset: '${status.count + model.abandonedShoppingCarts.firstElementOnPage}.',
					email: '${abandonedShoppingCart.customerEmail}',
					emailURL: '${_contextpath}/admin/customers/customer.jhtm?id=${abandonedShoppingCart.userId}',
					lastName: '${wj:escapeJS(abandonedShoppingCart.customerLastName)}',
					firstName: '${wj:escapeJS(abandonedShoppingCart.customerFirstName)}',
					cartURL: '${_contextpath}/admin/customers/loginAsCustomer.jhtm?cid=${abandonedShoppingCart.token}&type=abandonedShoppingCart',
					cellPhone: '${abandonedShoppingCart.customerPhone}',
					createdDate: '<fmt:formatDate type="date" timeStyle="default" value="${abandonedShoppingCart.createdDate }"/>',
					totalAmount: '<fmt:formatNumber value="${abandonedShoppingCart.totalAmount}" pattern="$##,###.00" />',
				});
			</c:forEach>
			
			require(["dgrid/List", "dgrid/OnDemandGrid", "dgrid/Selection", "dgrid/Keyboard", "dgrid/Editor", "dijit/form/CheckBox", 
					"dijit/form/TimeTextBox", "dijit/form/Select", "dojo/data/ObjectStore", "dijit/form/FilteringSelect",
					'dijit/form/DropDownButton', 'dijit/DropDownMenu', 'dijit/MenuItem',
					"dojo/_base/declare", "dojo/_base/lang", "dojo/store/Memory", "dojo/store/Observable", "dgrid/test/data/createSyncStore", 
					'dgrid/extensions/ColumnHider', 'dgrid/extensions/ColumnReorder', 'dgrid/extensions/ColumnResizer'
				], function(List, OnDemandGrid, Selection, Keyboard, Editor, CheckBox, TimeTextBox, Select, ObjectStore, FilteringSelect,
						DropDownButton, DropDownMenu, MenuItem,
						declare, lang, LegacyMemory, LegacyObservable, createSyncStore, ColumnHider, ColumnReorder, ColumnResizer){
	
				//Column Definitions
				var columns = {
					offset: {
						label: '#',
						get: function(object){
							return object.offset;
						},
						sortable: false,
						unhidable: true
					},
					email: {
						label: '<fmt:message key="email" />',
						renderCell: function(object){
							var link = document.createElement('a');
							link.href = object.emailURL;
							link.textContent = object.email;
							return link;
						},
						sortable: false,
					},
					lastName: {
						label: '<fmt:message key="lastName"/>',
						get: function(object){
							return object.lastName;
						},
						sortable: false,
					},
					firstName: {
						label: '<fmt:message key="firstName"/>',
						get: function(object){
							return object.firstName;
						},
						sortable: false,
					},
					action: {
						label: '<fmt:message key="action"/>',
						renderCell: function(object){
							var link = document.createElement('a');
							link.href = object.cartURL;
							link.textContent = '<fmt:message key="seeCart"/>';
							return link;
						},
						sortable: false,
					},
					cellPhone: {
						label: '<fmt:message key="cellPhone" />',
						get: function(object){
							return object.cellPhone;
						},
						sortable: false,
					},
					createdDate: {
						label: '<fmt:message key="date" />',
						get: function(object){
							return object.createdDate;
						},
						sortable: false,
					},
					totalAmount: {
						label: '<fmt:message key="abandonedShoppingCartAmount" />',
						get: function(object){
							return object.totalAmount;
						},
						sortable: false,
					},
					removeCart:{
						label: '<fmt:message key="removeCart" />',
						renderCell: function(object){
							var link = document.createElement('a');
							link.href = '#';
							link.textContent = 'Remove Cart';
							return link;
						},
						sortable: false,
					}
				};
				
				var grid = new (declare([OnDemandGrid, Selection, Keyboard, Editor, ColumnHider, ColumnReorder, ColumnResizer]))({
					collection: createSyncStore({ data: data }),
					columns: columns,
					selectionMode: "none",
				},'grid');
				
				grid.on('dgrid-refresh-complete', function(event) {
					$('#grid-hider-menu').appendTo('#column-hider');
				});
				
				grid.startup();
			});
		</script>
	</tiles:putAttribute>
</tiles:insertDefinition>