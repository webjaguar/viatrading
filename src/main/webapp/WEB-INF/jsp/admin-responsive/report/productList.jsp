<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ taglib uri="http://www.springframework.org/security/tags" prefix="sec" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ taglib uri="/WEB-INF/tlds/spring.tld" prefix="spring" %>
<%@ taglib uri="/WEB-INF/tlds/custom.tld" prefix="wj" %>

<tiles:insertDefinition name="admin.responsive-template.report.product" flush="true">
	<tiles:putAttribute name="css">
		<style>
			#grid .field-customer{
				width: 150px;
			}
			#grid .field-salesRep{
				width: 150px;
			}
			#grid .field-shippingMethod{
				width: 200px;
			}
			#grid .field-company{
				width: 250px;
			}
			#grid .field-qty{
				width: 80px;
				text-align:right;
			}
			#grid .field-priceCasepackQty{
				text-align:right;
			}
			#grid .field-unitPrice{
				text-align:right;
			}
			#grid .field-subTotal{
				text-align:right;
			}
		</style>
	</tiles:putAttribute>
	<tiles:putAttribute name="content" type="string">
		<sec:authorize ifAnyGranted="ROLE_AEM,ROLE_ADMIN_${prop_site_id},ROLE_REPORT_PRODUCT">
			<form name="list" id="list" action="">
				<div class="page-banner">
					<div class="breadcrumb"><span><a href="${_contextpath}/admin/catalog/dashboard.jhtm">Home</a></span><i class="sli-arrow-right"></i><span><a href="../reports"><fmt:message key="reports" /></a></span><i class="sli-arrow-right"></i><span><fmt:message key="sku" /></span></div>
				</div>
				<div class="page-head">
					<div class="row justify-content-between">
						<div class="col-sm-8 title">
							<h3>Import/Export</h3>
						</div>
						<div class="col-sm-4 actions">
		                    <div class="dropdown dropdown-setting">
								<button type="button" class="btn-outline dropdown-toggle active btn btn-primary" data-toggle="dropdown">
		                             <i class="sli-screen-desktop"></i> Display
		                         </button>	                         
								<div class="dropdown-menu dropdown-menu-right">
									<div class="form-group">
				                    		<div class="option-header">Rows</div>
										<select class="custom-select" name="size"  onchange="document.getElementById('page').value=1;submit()">
											<c:forTokens items="10,25,50,100,200,500" delims="," var="current">
												<option value="${current}" <c:if test="${current == productListFilter.pageSize}">selected</c:if>>${current} <fmt:message key="perPage" /></option>
											</c:forTokens>
										</select>
									</div>
									<div class="form-group">
				                    		<div class="option-header">Columns</div>
				                    		<div id="column-hider"></div>
									</div>
								</div>
		                    </div>
						</div>
					</div>
				</div>
				<div class="page-body">
					<div class="grid-stage">
						<c:if test="${model.count > 0}">
							<div class="page-stat">
								<fmt:message key="showing">
									<fmt:param value="${productListFilter.offset + 1}"/>
									<fmt:param value="${model.pageEnd}"/>
									<fmt:param value="${model.count}"/>
								</fmt:message>
							</div> 
						</c:if>
						<table id="grid"></table>
						<div class="footer">
							<div class="float-right">
								<c:if test="${productListFilter.page == 1}">
									<span class="previous"><i class="sli-arrow-left" style="font-size: 0.7rem"></i> Previous</span>
								</c:if>
								<c:if test="${productListFilter.page != 1}">
									<a href="<c:url value="productListReport.jhtm"><c:param name="page" value="${productListFilter.page-1}"/></c:url>">
										<span class="previous"><i class="sli-arrow-left" style="font-size: 0.7rem"></i> Previous</span>
									</a>
								</c:if>	
								<span class="status"><c:out value="${productListFilter.page}" /> of <c:out value="${model.pageCount}" /></span>
								<c:if test="${productListFilter.page == model.pageCount}">
									<span class="next">Next <i class="sli-arrow-right" style="font-size: 0.7rem"></i></span>
								</c:if>
								<c:if test="${productListFilter.page != model.pageCount}">
									<a href="<c:url value="productListReport.jhtm"><c:param name="page" value="${productListFilter.page+1}"/></c:url>">
										<span class="next">Next <i class="sli-arrow-right" style="font-size: 0.7rem"></i></span>
									</a>
								</c:if>
							</div>
						</div>
					</div>
				</div>
				<input type="hidden" id="page" name="page" value="${productListFilter.page}"/>
				<input type="hidden" id="sort" name="sort" value="${productListFilter.sort}" />
			</form>
		</sec:authorize>
	</tiles:putAttribute>
	<tiles:putAttribute name="js">
		<!-- DGRID TABLE SCRIPT MUST BE AT BOTTOM OF THE PAGE -->
		<script src="${_contextpath}/admin-responsive-static/assets/plugins/dojo/dojo.js" data-dojo-config="async: true"></script>
		<script>
			//Table Data Sanitization
			var data = [];
			<c:forEach items="${model.productList}" var="productReport" varStatus="status">
				data.push({
					offset: '${status.count + productListFilter.offset}.',
					sku: '${productReport.sku}',
					orderId: '${productReport.orderId}',
					customerURL: '../customers/customer.jhtm?id=<c:out value="${productReport.customer.id}"/>',
					customerName: '<c:out value="${productReport.customer.address.firstName}"/> <c:out value="${productReport.customer.address.lastName}"/>',
					orderDate: '<fmt:formatDate type="date" timeStyle="default" value="${productReport.orderDate}"/>',
					<c:if test="${productReport.status != null}">
						status: '<fmt:message key="orderStatus_${productReport.status}" />',
					</c:if>
					salesRep: '${productReport.salesRep.name}',
					<c:if test="${productListFilter.dateType != 'orderDate'}">
						dateShipped: '<fmt:formatDate type="date" timeStyle="default" value="${productReport.shipped}"/>',
					</c:if>
					shippingMethod: '${productReport.shippingMethod}',
					company: '${productReport.company}',
					qty: '${productReport.quantity}',
					<c:if test="${gSiteConfig['gCASE_CONTENT']}">  
						content: "${productReport.caseContent}",
					</c:if>
					<c:if test="${gSiteConfig['gPRICE_CASEPACK']}">
						priceCasepackQty: "${productReport.priceCasepackQty}",
						<c:choose>
							<c:when test="${productReport.priceCasepackQty == null or (empty productReport.priceCasepackQty)}"> 
								<c:set var="totalCasepackQty" value="${productReport.quantity + totalCasepackQty}" />
							</c:when>
							<c:otherwise>
								<c:set var="totalCasepackQty" value="${productReport.priceCasepackQty + totalCasepackQty}" />
							</c:otherwise>
						</c:choose>
					</c:if>
					unitPrice: decodeHTML('<fmt:message key="${siteConfig[\'CURRENCY\'].value}" />') + '<fmt:formatNumber value="${productReport.unitPrice}" pattern="#,##0.00" />',
					subTotal: decodeHTML('<fmt:message key="${siteConfig[\'CURRENCY\'].value}" />') + '<fmt:formatNumber value="${productReport.subTotal}" pattern="#,##0.00" />',
					<c:set var="totalSubTotal" value="${productReport.subTotal + totalSubTotal}" />
				});
			</c:forEach>
			
			require(["dgrid/List", "dgrid/OnDemandGrid", "dgrid/Selection", "dgrid/Keyboard", "dgrid/Editor", "dijit/form/CheckBox", 
					"dijit/form/TimeTextBox", "dijit/form/Select", "dojo/data/ObjectStore", "dijit/form/FilteringSelect",
					'dijit/form/DropDownButton', 'dijit/DropDownMenu', 'dijit/MenuItem',
					"dojo/_base/declare", "dojo/_base/lang", "dojo/store/Memory", "dojo/store/Observable", "dgrid/test/data/createSyncStore", 
					'dgrid/extensions/ColumnHider', 'dgrid/extensions/ColumnReorder', 'dgrid/extensions/ColumnResizer', 'summary/SummaryRow'
				], function(List, OnDemandGrid, Selection, Keyboard, Editor, CheckBox, TimeTextBox, Select, ObjectStore, FilteringSelect,
						DropDownButton, DropDownMenu, MenuItem,
						declare, lang, LegacyMemory, LegacyObservable, createSyncStore, ColumnHider, ColumnReorder, ColumnResizer, SummaryRow){
	
				//Column Definitions
				var columns = {
					offset: {
						label: '#',
						get: function(object){
							return object.offset;
						},
						sortable: false,
						unhidable: true
					},
					sku: {
						label: '<fmt:message key="sku" />',
						get: function(object){
							return object.sku;
						},
						sortable: false,
					},
					orderId: {
						label: '<fmt:message key="orderNumber" />',
						renderCell: function(object){
							var link = document.createElement('a');
							link.href = '../orders/invoice.jhtm?order=' + object.orderId;
							link.textContent = object.orderId;
							return link;
						},
						sortable: false,
					},
					customer: {
						label: '<fmt:message key="billTo" />',
						renderCell: function(object){
							var link = document.createElement('a');
							link.href = object.customerURL;
							link.textContent = object.customerName;
							return link;
						},
						sortable: false,
					},
					orderDate: {
						label: '<fmt:message key="orderDate" />',
						get: function(object){
							return object.orderDate;
						},
						sortable: false,
					},
					status: {
						label: '<fmt:message key="status" />',
						get: function(object){
							return object.status;
						},
						sortable: false,
					},
					salesRep: {
						label: '<fmt:message key="salesRep" />',
						get: function(object){
							return object.salesRep;
						},
						sortable: false,
					},
					dateShipped: {
						label: '<fmt:message key="dateShipped" />',
						get: function(object){
							return object.dateShipped;
						},
						sortable: false,
					},
					shippingMethod: {
						label: '<fmt:message key="shippingMethod" />',
						get: function(object){
							return object.shippingMethod;
						},
						sortable: false,
					},
					company: {
						label: '<fmt:message key="company" />',
						get: function(object){
							return object.company;
						},
						sortable: false,
					},
					qty: {
						label: '<fmt:message key="qty" />',
						get: function(object){
							return object.qty;
						},
						sortable: false,
					},
					<c:if test="${gSiteConfig['gCASE_CONTENT']}">  
						content: {
							label: '<fmt:message key="content" />',
							get: function(object){
								return object.content;
							},
							sortable: false,
						},
					</c:if>
					<c:if test="${gSiteConfig['gPRICE_CASEPACK']}">
						priceCasepackQty:{
							label: 'Price case qty',
							get: function(object){
								return object.priceCasepackQty;
							},
							sortable: false,
						},
					</c:if>
					unitPrice:{
						label: '<fmt:message key="unitPrice" />',
						get: function(object){
							return object.unitPrice;
						},
						sortable: false,
					},
					subTotal:{
						label: '<fmt:message key="subTotal" />',
						get: function(object){
							return object.subTotal;
						},
						sortable: false,
					}
				};
				
				var grid = new (declare([OnDemandGrid, Selection, Keyboard, Editor, ColumnHider, ColumnReorder, ColumnResizer, SummaryRow]))({
					collection: createSyncStore({ data: data }),
					columns: columns,
					selectionMode: "none",
				},'grid');
				
				var totals = {
					sku: 'Total',
					qty: 
						<c:choose>
							<c:when test="${gSiteConfig['gPRICE_CASEPACK']}">
					     		'<fmt:formatNumber value="${totalCasepackQty}" pattern="#,##0" />'
							</c:when>
							<c:otherwise>
								'<fmt:formatNumber value="${totalQtu}" pattern="#,##0" />'
							</c:otherwise>
						</c:choose>,
					subTotal: decodeHTML('<fmt:message key="${siteConfig[\'CURRENCY\'].value}" />') + '<fmt:formatNumber value="${totalSubTotal}" pattern="#,##0.00" />'
				};

				grid.set('summary', totals);
				
				grid.on('dgrid-refresh-complete', function(event) {
					$('#grid-hider-menu').appendTo('#column-hider');
				});
				
				grid.startup();
			});
		</script>
	</tiles:putAttribute>
</tiles:insertDefinition>
