<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ taglib uri="http://www.springframework.org/security/tags" prefix="sec" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ taglib uri="/WEB-INF/tlds/spring.tld" prefix="spring" %>
<%@ taglib uri="/WEB-INF/tlds/custom.tld" prefix="wj" %>

<tiles:insertDefinition name="admin.responsive-template.report.salesRepDetail2" flush="true">
	<tiles:putAttribute name="css">
		<style>
			#grid .field-salesRepName{
				width: 150px;
			}
			#grid .field-merchandize{
				text-align: right;
			}
			#grid .field-grandTotal{
				text-align: right;
			}
			#grid .field-ppOrder{
				text-align: right;
			}
			#grid .field-shipAndPP{
				text-align: right;
				width: 150px;
			}
		</style>
	</tiles:putAttribute>
	<tiles:putAttribute name="content" type="string">
		<c:if test="${gSiteConfig['gREPORT']}">
			<sec:authorize ifAnyGranted="ROLE_AEM,ROLE_ADMIN_${prop_site_id},ROLE_REPORT_SALESREP_OVERVIEWII">
				<form action="salesRepDetailReport2.jhtm" id="list" method="post">
					<div class="page-banner">
						<div class="breadcrumb"><span><a href="${_contextpath}/admin/catalog/dashboard.jhtm">Home</a></span><i class="sli-arrow-right"></i><span><a href="../reports"><fmt:message key="reports" /></a></span><i class="sli-arrow-right"></i><span>SalesRep Overview II</span></div>
					</div>
					<div class="page-head">
						<div class="row justify-content-between">
							<div class="col-sm-8 title">
								<h3>SalesRep Overview II</h3>
							</div>
						</div>
					</div>
					<div class="page-body">
						<table id="grid"></table>
					</div>
					<input type="hidden" id="sort" name="sort" value="${salesRepDetailFilter2.sort}" />
				</form>
			</sec:authorize>
		</c:if>
	</tiles:putAttribute>
	<tiles:putAttribute name="js">
		<!-- DGRID TABLE SCRIPT MUST BE AT BOTTOM OF THE PAGE -->
		<script src="${_contextpath}/admin-responsive-static/assets/plugins/dojo/dojo.js" data-dojo-config="async: true"></script>
		<script>
			//Table Data Sanitization
			var data = [];
			<c:set var="merchandize" value="0"/>
			<c:set var="grandTotal" value="0"/>
			<c:set var="ppOrder" value="0"/>
			<c:set var="shipAndPP" value="0"/>
			<c:forEach items="${model.salesRepDetailReportsList}" var="salesRepDetailReport" varStatus="status">
		    		data.push({
		    			salesRepName: '<c:out value="${model.salesRepMap[salesRepDetailReport.salesRepId].name}"/>',
		    			merchandize: decodeHTML('<fmt:message key="${siteConfig[\'CURRENCY\'].value}" />') + '<fmt:formatNumber value="${salesRepDetailReport.merchandize}" pattern="#,##0.00" />',
		    			grandTotal: decodeHTML('<fmt:message key="${siteConfig[\'CURRENCY\'].value}" />') + '<fmt:formatNumber value="${salesRepDetailReport.grandTotal}" pattern="#,##0.00" />',
		    			ppOrder: decodeHTML('<fmt:message key="${siteConfig[\'CURRENCY\'].value}" />') + '<fmt:formatNumber value="${salesRepDetailReport.ppOrder}" pattern="#,##0.00" />',
		    			shipAndPP: decodeHTML('<fmt:message key="${siteConfig[\'CURRENCY\'].value}" />') + '<fmt:formatNumber value="${salesRepDetailReport.grandTotal+salesRepDetailReport.ppOrder}" pattern="#,##0.00" />',
				});
		    		<c:set var="merchandize" value="${salesRepDetailReport.merchandize + merchandize}" />
				<c:set var="grandTotal" value="${salesRepDetailReport.grandTotal + grandTotal}" />
				<c:set var="ppOrder" value="${salesRepDetailReport.ppOrder + ppOrder}" />
				<c:set var="shipAndPP" value="${salesRepDetailReport.grandTotal+salesRepDetailReport.ppOrder +shipAndPP}" />
			</c:forEach>
	
			//Columns Definitions
			require(["dgrid/List", "dgrid/OnDemandGrid", "dgrid/Selection", "dgrid/Keyboard", "dgrid/Editor", "dijit/form/CheckBox", 
					"dijit/form/TimeTextBox", "dijit/form/Select", "dojo/data/ObjectStore", "dijit/form/FilteringSelect",
					'dijit/form/DropDownButton', 'dijit/DropDownMenu', 'dijit/MenuItem',
					"dojo/_base/declare", "dojo/_base/lang", "dojo/store/Memory", "dojo/store/Observable", "dgrid/test/data/createSyncStore", 
					'dgrid/extensions/ColumnReorder', 'dgrid/extensions/ColumnResizer', 'summary/SummaryRow',
				], function (List, OnDemandGrid, Selection, Keyboard, Editor, CheckBox, TimeTextBox, Select, ObjectStore, FilteringSelect,
							DropDownButton, DropDownMenu, MenuItem,
							declare, lang, LegacyMemory, LegacyObservable, createSyncStore, ColumnReorder, ColumnResizer, SummaryRow) {
				var columns = {
					salesRepName: {
						label: '<fmt:message key="salesRep" />',
						get: function(object){
							return object.salesRepName;
						},
						sortable: false,
					},
					merchandize: {
						label: '<fmt:message key="merchandise" />',
						get: function(object){
							return object.merchandize;
						},
						sortable: false,
					},
					grandTotal: {
						label: '<fmt:message key="grandTotal" />',
						get: function(object){
							return object.grandTotal;
						},
						sortable: false,
					},
					ppOrder: {
						label: '<fmt:message key="ppOrder" />',
						get: function(object){
							return object.ppOrder;
						},
						sortable: false,
					},
					shipAndPP: {
						label: '<fmt:message key="shipAndPP" />',
						get: function(object){
							return object.shipAndPP;
						},
						sortable: false,
					}
				};
				
				var grid = new (declare([OnDemandGrid, Selection, Keyboard, Editor, ColumnReorder, ColumnResizer, SummaryRow]))({
					collection: createSyncStore({ data: data }),
					selectionMode: "none",
					allowSelectAll: true,
					columns: columns
				},'grid');
				
				var totals = {
					salesRepName: '<fmt:message key="total" />',
					merchandize: decodeHTML('<fmt:message key="${siteConfig[\'CURRENCY\'].value}" />') + '<fmt:formatNumber value="${merchandize}" pattern="#,##0.00" />',
					grandTotal: decodeHTML('<fmt:message key="${siteConfig[\'CURRENCY\'].value}" />') + '<fmt:formatNumber value="${grandTotal}" pattern="#,##0.00" />',
					ppOrder: decodeHTML('<fmt:message key="${siteConfig[\'CURRENCY\'].value}" />') + '<fmt:formatNumber value="${ppOrder}" pattern="#,##0.00" />',
					shipAndPP: decodeHTML('<fmt:message key="${siteConfig[\'CURRENCY\'].value}" />') + '<fmt:formatNumber value="${shipAndPP}" pattern="#,##0.00" />',
				};

				grid.set('summary', totals);
				
				grid.on('dgrid-refresh-complete', function(event) {
					$('#grid-hider-menu').appendTo('#column-hider');
				});
				
				grid.startup();
			});
		</script>
	</tiles:putAttribute>
</tiles:insertDefinition>