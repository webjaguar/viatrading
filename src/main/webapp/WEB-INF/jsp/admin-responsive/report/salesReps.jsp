<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ taglib uri="http://www.springframework.org/security/tags" prefix="sec" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ taglib uri="/WEB-INF/tlds/spring.tld" prefix="spring" %>
<%@ taglib uri="/WEB-INF/tlds/custom.tld" prefix="wj" %>

<tiles:insertDefinition name="admin.responsive-template.report.salesReps" flush="true">
	<tiles:putAttribute name="css">
		<style>
			#report-tbl{
				min-width: 100%;
			}
			#report-tbl > thead > tr > th {
				text-align: center;
			}
			
			#report-tbl > tbody > tr.line-row > td {
				text-align: center;
			}
		</style>
	</tiles:putAttribute>
	<tiles:putAttribute name="content" type="string">
		<sec:authorize ifAnyGranted="ROLE_AEM,ROLE_ADMIN_${prop_site_id},ROLE_REPORT_SALESREP">
			<form name="salesRepList" id="salesRepList" action="">
				<div class="page-banner">
					<div class="breadcrumb"><span><a href="${_contextpath}/admin/catalog/dashboard.jhtm">Home</a></span><i class="sli-arrow-right"></i><span><a href="../reports"><fmt:message key="reports" /></a></span><i class="sli-arrow-right"></i><span>Sales Rep</span></div>
				</div>
				<div class="page-head">
					<div class="row justify-content-between">
						<div class="col-sm-8 title">
							<h3>Sales Rep</h3>
						</div>
					</div>
				</div>
				<div class="page-body">
					<div class="table-wrapper">
						<table id="report-tbl" class="complex">
							<thead>
								<tr>
									<th><fmt:message key="Name" /></th>
									<c:choose>
										<c:when test="${salesRepFilter.quarter == '1'}">
											<th colspan="2">Jan</th>
											<th colspan="2">Feb</th>
											<th colspan="2">Mar</th>
										</c:when>
										<c:when test="${salesRepFilter.quarter == '2'}">
											<th colspan="2">Apr</th>
											<th colspan="2">May</th>
											<th colspan="2">Jun</th>
										</c:when>
										<c:when test="${salesRepFilter.quarter == '3'}">
											<th colspan="2">Jul</th>
											<th colspan="2">Aug</th>
											<th colspan="2">Sep</th>
										</c:when>
										<c:when test="${salesRepFilter.quarter == '4'}">
											<th colspan="2">Oct</th>
											<th colspan="2">Nov</th>
											<th colspan="2">Dec</th>
										</c:when>
										<c:when test="${salesRepFilter.quarter == '0'}">
											<th colspan="2">${salesRepFilter.year}</th>
										</c:when>
									</c:choose>
								</tr>
							</thead>
							<tbody>
								<tr class="line-row">
									<td class="indexCol" style="min-width: 200px">&nbsp;</td>
									<c:set var="totalGrandTotalShipped0" value="0.0" />
									<c:set var="totalGrandTotalShipped1" value="0.0" />
									<c:set var="totalGrandTotalShipped2" value="0.0" />
									<c:set var="totalGrandTotalPenProc0" value="0.0" />
									<c:set var="totalGrandTotalPenProc1" value="0.0" />
									<c:set var="totalGrandTotalPenProc2" value="0.0" />
									<c:choose>
										<c:when test="${salesRepFilter.quarter == '1' or salesRepFilter.quarter == '2' or salesRepFilter.quarter == '3'  or salesRepFilter.quarter == '4' }">
											<td style="min-width: 100px"><fmt:message key="shipped" /></td>
											<td style="min-width: 150px"><fmt:message key="pendingProcessing" /></td>
											<td style="min-width: 100px"><fmt:message key="shipped" /></td>
											<td style="min-width: 150px"><fmt:message key="pendingProcessing" /></td>
											<td style="min-width: 100px"><fmt:message key="shipped" /></td>
											<td style="min-width: 150px"><fmt:message key="pendingProcessing" /></td>
										</c:when>
										<c:when test="${salesRepFilter.quarter == '0'}">
											<td><fmt:message key="shipped" /></td><td><fmt:message key="pendingProcessing" /></td>
										</c:when>
									</c:choose>
								</tr>
								<c:forEach items="${model.salesRepReports}" var="salesReps" varStatus="outStatus">
									<tr>
										<c:forEach items="${salesReps}" var="salesRep" varStatus="status">
											<c:if test="${status.first}">
												<td><c:out value="${model.salesRepMap[salesRep.salesRepId].name}"/></td>
											</c:if>
											<td style="text-align: right">
												<c:if test="${!empty salesRep.grandTotalShipped}">
													<fmt:message key="${siteConfig['CURRENCY'].value}" /><fmt:formatNumber value="${salesRep.grandTotalShipped}" pattern="#,##0.00" />
													<c:if test="${status.index == 0}" ><c:set var="totalGrandTotalShipped0" value="${salesRep.grandTotalShipped + totalGrandTotalShipped0}" /></c:if>
													<c:if test="${status.index == 1}" ><c:set var="totalGrandTotalShipped1" value="${salesRep.grandTotalShipped + totalGrandTotalShipped1}" /></c:if>
													<c:if test="${status.index == 2}" ><c:set var="totalGrandTotalShipped2" value="${salesRep.grandTotalShipped + totalGrandTotalShipped2}" /></c:if>
												</c:if>
											</td>
											<td style="text-align: right">
												<c:if test="${!empty salesRep.grandTotalPenProc}">
													<fmt:message key="${siteConfig['CURRENCY'].value}" /><fmt:formatNumber value="${salesRep.grandTotalPenProc}" pattern="#,##0.00" />
													<c:if test="${status.index == 0}" ><c:set var="totalGrandTotalPenProc0" value="${salesRep.grandTotalPenProc + totalGrandTotalPenProc0}" /></c:if>
													<c:if test="${status.index == 1}" ><c:set var="totalGrandTotalPenProc1" value="${salesRep.grandTotalPenProc + totalGrandTotalPenProc1}" /></c:if>
													<c:if test="${status.index == 2}" ><c:set var="totalGrandTotalPenProc2" value="${salesRep.grandTotalPenProc + totalGrandTotalPenProc2}" /></c:if>
												</c:if>
											</td>
										</c:forEach>	
									</tr>
								</c:forEach>
								<tr class="total">
									<td style="text-align:right"><fmt:message key="total" /></td>
									<td style="text-align:right"><fmt:message key="${siteConfig['CURRENCY'].value}" /><fmt:formatNumber value="${totalGrandTotalShipped0}" pattern="#,##0.00" /></td>
									<td style="text-align:right"><fmt:message key="${siteConfig['CURRENCY'].value}" /><fmt:formatNumber value="${totalGrandTotalPenProc0}" pattern="#,##0.00" /></td>
									<c:if test="${salesRepFilter.quarter == '1' or salesRepFilter.quarter == '2' or salesRepFilter.quarter == '3' or salesRepFilter.quarter == '4'}">
										<td style="text-align:right"><fmt:message key="${siteConfig['CURRENCY'].value}" /><fmt:formatNumber value="${totalGrandTotalShipped1}" pattern="#,##0.00" /></td>
										<td style="text-align:right"><fmt:message key="${siteConfig['CURRENCY'].value}" /><fmt:formatNumber value="${totalGrandTotalPenProc1}" pattern="#,##0.00" /></td>
										<td style="text-align:right"><fmt:message key="${siteConfig['CURRENCY'].value}" /><fmt:formatNumber value="${totalGrandTotalShipped2}" pattern="#,##0.00" /></td>
										<td style="text-align:right"><fmt:message key="${siteConfig['CURRENCY'].value}" /><fmt:formatNumber value="${totalGrandTotalPenProc2}" pattern="#,##0.00" /></td>
									</c:if>
								</tr>
							</tbody>
						</table>
					</div>
				</div>
			</form>
		</sec:authorize>
	</tiles:putAttribute>
	<tiles:putAttribute name="js">
	</tiles:putAttribute>
</tiles:insertDefinition>
