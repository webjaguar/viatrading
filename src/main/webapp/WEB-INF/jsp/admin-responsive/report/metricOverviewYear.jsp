<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ taglib uri="http://www.springframework.org/security/tags" prefix="sec" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ taglib uri="/WEB-INF/tlds/spring.tld" prefix="spring" %>
<%@ taglib uri="/WEB-INF/tlds/custom.tld" prefix="wj" %>

<tiles:insertDefinition name="admin.responsive-template.report.metricOverview" flush="true">
	<tiles:putAttribute name="css">
		<style>
			#grid .dgrid-cell{
				text-align:right;
				width: 70px;
			}
			#grid .field-name{
				width: 180px;
				text-align: left;
			}
			#grid td.field-name{
				font-weight: 600;
			}
			#grid td.field-total{
				font-weight: 600;
			}
			#grid .dgrid-row.summary td.dgrid-cell{
			 	background-color: #f5f5f5;
			}
		</style>
	</tiles:putAttribute>
	<tiles:putAttribute name="content" type="string">
		<c:if test="${gSiteConfig['gREPORT']}">
			<sec:authorize ifAnyGranted="ROLE_AEM,ROLE_ADMIN_${prop_site_id},ROLE_REPORT_CUSTOMER_METRIC">
				<div class="page-banner">
					<div class="breadcrumb"><span><a href="${_contextpath}/admin/catalog/dashboard.jhtm">Home</a></span><i class="sli-arrow-right"></i><span><a href="../reports"><fmt:message key="reports" /></a></span><i class="sli-arrow-right"></i><span>Customer Metric Overview</span></div>
				</div>
				<div class="page-head">
					<div class="row justify-content-between">
						<div class="col-sm-8 title">
							<h3>Customer Metric Overview</h3>
						</div>
						<div class="col-sm-4 actions">
							<div class="dropdown">
								<button type="button" class="btn-outline dropdown-toggle active btn btn-primary" onclick="$('#search-dialog').modal('show')">
		                            <i class="sli-magnifier"></i>
		                         </button>
							</div>
						</div>
					</div>
				</div>
				<div class="page-body">
					<div class="grid-stage grid-stage-sm">
						<table id="grid"></table>
					</div>
				</div>
				<c:import url="/WEB-INF/jsp/admin-responsive/report/options/metricOverviewSearchOptions.jsp" />
			</sec:authorize>
		</c:if>
	</tiles:putAttribute>  
	<tiles:putAttribute name="js">
		<!-- DGRID TABLE SCRIPT MUST BE AT BOTTOM OF THE PAGE -->
		<script src="${_contextpath}/admin-responsive-static/assets/plugins/dojo/dojo.js" data-dojo-config="async: true"></script>
		<script>
			//Table Data Sanitization
			var data = [];
			
			data.push({
    				name: '<fmt:message key="newRegistration" />',
		    		<c:forEach items="${model.reports}" var="reports" varStatus="status">
		    			year_${status.index}: '<fmt:formatNumber value="${reports.newRegistrations}" pattern="#,##0"/>',
				    <c:set var="totalNewRegistrations" value="${reports.newRegistrations+totalNewRegistrations}"/>
				</c:forEach>
				total: '<fmt:formatNumber  value="${totalNewRegistrations}" pattern="#,##0"/>',
			});
			
			data.push({
				name: '<fmt:message key="newActivation" />',
		    		<c:forEach items="${model.reports}" var="reports" varStatus="status">
		    			year_${status.index}: '<fmt:formatNumber value="${reports.newActivations}" pattern="#,##0"/>',
		    			<c:set var="totalNewActivations" value="${reports.newActivations+totalNewActivations}"/>
				</c:forEach>
				total: '<fmt:formatNumber value="${totalNewActivations}" pattern="#,##0"/>',
			});
			
			data.push({
				name: '<fmt:message key="closeRate" />',
		    		<c:forEach items="${model.reports}" var="reports" varStatus="status">
					<c:choose>
						<c:when test="${reports.newRegistrations!=0}">
							year_${status.index}: '<fmt:formatNumber value="${(reports.newActivations/reports.newRegistrations)*100}" pattern="#,##0.00" />%',
						</c:when>
						<c:otherwise>year_${status.index}: '0%',</c:otherwise>
					</c:choose>
		    			<c:set var="totalNewActivations" value="${reports.newActivations+totalNewActivations}"/>
				</c:forEach>
				<c:choose>
					<c:when test="${totalNewRegistrations!=0}">
						total: '<fmt:formatNumber value="${(totalNewActivations/totalNewRegistrations)*100}" pattern="#,##0.00"/>%',
					</c:when>
					<c:otherwise>total: '0%',</c:otherwise>
				</c:choose>
				summary: true,
			});
			
			data.push({
				name: '<fmt:message key="totalSpend" />',
		    		<c:forEach items="${model.reports}" var="reports" varStatus="status">
		    			year_${status.index}: '$<fmt:formatNumber value="${reports.totalSpend}" pattern="#,##0"/>',
		    			<c:set var="totalTotalSpend" value="${reports.totalSpend+totalTotalSpend}"/>
				</c:forEach>
				total: '$<fmt:formatNumber value="${totalTotalSpend}" pattern="#,##0"/>',
			});
			
			data.push({
				name: '<fmt:message key="totalRegistration" />',
		    		<c:forEach items="${model.reports}" var="reports" varStatus="status">
		    			year_${status.index}: '<fmt:formatNumber value="${reports.totalRegistration}" pattern="#,##0"/>',
		    			<c:set var="totalRegistrations" value="${reports.totalRegistration+totalRegistrations}"/>
				</c:forEach>
				<c:choose>
					<c:when test="${model.totalRegistrations == 0}">
						total: '<fmt:formatNumber value="${totalRegistrations}" pattern="#,##0"/>',
					</c:when>
					<c:otherwise>
						total: '<fmt:formatNumber value="${model.totalRegistrations}" pattern="#,##0"/>',
					</c:otherwise>
				</c:choose>
			});
			
			data.push({
				name: '<fmt:message key="totalActivation" />',
		    		<c:forEach items="${model.reports}" var="reports" varStatus="status">
		    			year_${status.index}: '<fmt:formatNumber value="${reports.totalActivations}" pattern="#,##0"/>',
		    			<c:set var="totalActivations" value="${reports.totalActivations+totalActivations}"/>
				</c:forEach>
				<c:choose>
					<c:when test="${model.totalRegistrations == 0}">
						total: '<fmt:formatNumber value="${totalActivations}" pattern="#,##0"/>',
					</c:when>
					<c:otherwise>
						total: '<fmt:formatNumber value="${model.totalActivations}" pattern="#,##0"/>',
					</c:otherwise>
				</c:choose>
			});
			
			data.push({
				name: '<fmt:message key="allTimeCloseRate" />',
		    		<c:forEach items="${model.reports}" var="reports" varStatus="status">
					<c:choose>
						<c:when test="${reports.totalRegistration!=0}">
							year_${status.index}: '<fmt:formatNumber value="${(reports.totalActivations/reports.totalRegistration)*100}" pattern="#,##0.00" />%',
						</c:when>
						<c:otherwise>year_${status.index}: '0%',</c:otherwise>
					</c:choose>
				</c:forEach>
				<c:choose>
					<c:when test="${model.totalRegistrations!=0 or totalRegistrations!=0 }" >			  			   
						<c:if test="${model.totalRegistrations!=0}">
							total: '<fmt:formatNumber value="${(model.totalActivations/model.totalRegistrations)*100}" pattern="#,##0.00" />%',
						</c:if>
						<c:if test="${totalRegistrations!=0}">
							total: '<fmt:formatNumber value="${(totalActivations/totalRegistrations)*100}" pattern="#,##0.00" />%',
						</c:if>
					</c:when>
					<c:otherwise>total: '0%',</c:otherwise>
				</c:choose>
				summary: true,
			});
			
			data.push({
				name: '<fmt:message key="tsRegistration" />',
		    		<c:forEach items="${model.reports}" var="reports" varStatus="status">
		    			year_${status.index}: '<fmt:formatNumber value="${reports.touchScreenRegistrations}" pattern="#,##0" />',
		    			<c:set var="totaltouchScreenRegistrations" value="${reports.touchScreenRegistrations+totaltouchScreenRegistrations}"/>
				</c:forEach>
				total: '<fmt:formatNumber value="${totaltouchScreenRegistrations}" pattern="#,##0" />',
			});
			
			data.push({
				name: '<fmt:message key="tsActivation" />',
		    		<c:forEach items="${model.reports}" var="reports" varStatus="status">
		    			year_${status.index}: '<fmt:formatNumber value="${reports.touchScreenActivations}" pattern="#,##0" />',
		    			<c:set var="totaltouchScreenActivations" value="${reports.touchScreenActivations+totaltouchScreenActivations}"/>
				</c:forEach>
				total: '<fmt:formatNumber value="${totaltouchScreenActivations}" pattern="#,##0" />',
			});
			
			data.push({
				name: '<fmt:message key="closeRate" />',
		    		<c:forEach items="${model.reports}" var="reports" varStatus="status">
					<c:choose>
						<c:when test="${reports.touchScreenRegistrations!=0}">
							year_${status.index}: '<fmt:formatNumber value="${(reports.touchScreenActivations/reports.touchScreenRegistrations)*100}" pattern="#,##0.00" />%',
						</c:when>
						<c:otherwise>year_${status.index}: '0%',</c:otherwise>
					</c:choose>
				</c:forEach>
				<c:choose>
					<c:when test="${totaltouchScreenRegistrations!=0}">
						total: '<fmt:formatNumber value="${(totaltouchScreenActivations/totaltouchScreenRegistrations)*100}" pattern="#,##0.00" />%',
					</c:when>
					<c:otherwise>total: '0%',</c:otherwise>
				</c:choose>	
				summary: true,
			});
			
			data.push({
				name: '<fmt:message key="feRegistration" />',
				<c:forEach items="${model.reports}" var="reports" varStatus="status">
					year_${status.index}: '<fmt:formatNumber value="${reports.frontendRegistrations}" pattern="#,##0" />',
					<c:set var="totalfrontendRegistrations" value="${reports.frontendRegistrations+totalfrontendRegistrations}"/>
				</c:forEach>
				total: '<fmt:formatNumber value="${totalfrontendRegistrations}" pattern="#,##0" />',
			});
			
			data.push({
				name: '<fmt:message key="feActivation" />',
				<c:forEach items="${model.reports}" var="reports" varStatus="status">
					year_${status.index}: '<fmt:formatNumber value="${reports.frontendActivations}" pattern="#,##0" />',
					<c:set var="totalfrontendActivations" value="${reports.frontendActivations+totalfrontendActivations}"/>
				</c:forEach>
				total: '<fmt:formatNumber value="${totalfrontendActivations}" pattern="#,##0" />',
			});
			
			data.push({
				name: '<fmt:message key="closeRate" />',
		    		<c:forEach items="${model.reports}" var="reports" varStatus="status">
					<c:choose>
						<c:when test="${reports.frontendRegistrations!=0}">
							year_${status.index}: '<fmt:formatNumber value="${(reports.frontendActivations/reports.frontendRegistrations)*100}" pattern="#,##0.00" />%',
						</c:when>
						<c:otherwise>year_${status.index}: '0%',</c:otherwise>
					</c:choose>
				</c:forEach>
				<c:choose>
					<c:when test="${totalfrontendRegistrations!=0}">
						total: '<fmt:formatNumber value="${(totalfrontendActivations/totalfrontendRegistrations)*100}" pattern="#,##0.00" />%',
					</c:when>
					<c:otherwise>total: '0%',</c:otherwise>
				</c:choose>
				summary: true,
			});
			
			data.push({
				name: '<fmt:message key="lpLeads" />',
				<c:forEach items="${model.reports}" var="reports" varStatus="status">
					year_${status.index}: '<fmt:formatNumber value="${reports.landingPageLeads}" pattern="#,##0" />',
					<c:set var="totallandingPageLeads" value="${reports.landingPageLeads+totallandingPageLeads}"/>
				</c:forEach>
				total: '<fmt:formatNumber value="${totallandingPageLeads}" pattern="#,##0" />',
			});
			
			data.push({
				name: '<fmt:message key="lpRegistration" />',
				<c:forEach items="${model.reports}" var="reports" varStatus="status">
					year_${status.index}: '<fmt:formatNumber value="${reports.landingPageRegistrations}" pattern="#,##0" />',
					<c:set var="totallandingPageRegistrations" value="${reports.landingPageRegistrations+totallandingPageRegistrations}"/>
				</c:forEach>
				total: '<fmt:formatNumber value="${totallandingPageRegistrations}" pattern="#,##0" />',
			});
			
			data.push({
				name: '<fmt:message key="lpActivation" />',
				<c:forEach items="${model.reports}" var="reports" varStatus="status">
					year_${status.index}: '<fmt:formatNumber value="${reports.landingPageActivations}" pattern="#,##0" />',
					<c:set var="totallandingPageActivations" value="${reports.landingPageActivations+totallandingPageActivations}"/>
				</c:forEach>
				total: '<fmt:formatNumber value="${totallandingPageActivations}" pattern="#,##0" />',
			});
			
			data.push({
				name: '<fmt:message key="registrationCloseRate" />',
		    		<c:forEach items="${model.reports}" var="reports" varStatus="status">
					<c:choose>
						<c:when test="${reports.landingPageRegistrations!=0}">
							year_${status.index}: '<fmt:formatNumber value="${(reports.landingPageActivations/reports.landingPageRegistrations)*100}" pattern="#,##0.00" />%',
						</c:when>
						<c:otherwise>year_${status.index}: '0%',</c:otherwise>
					</c:choose>
				</c:forEach>
				<c:choose>
					<c:when test="${totallandingPageRegistrations!=0}">
						total: '<fmt:formatNumber value="${(totallandingPageActivations/totallandingPageRegistrations)*100}" pattern="#,##0.00" />%',
					</c:when>
					<c:otherwise>total: '0%',</c:otherwise>
				</c:choose>
				summary: true,
			});
			
			data.push({
				name: '<fmt:message key="activationCloseRate" />',
		    		<c:forEach items="${model.reports}" var="reports" varStatus="status">
					<c:choose>
						<c:when test="${reports.landingPageRegistrations!=0}">
							year_${status.index}: '<fmt:formatNumber value="${(reports.landingPageActivations/reports.landingPageRegistrations)*100}" pattern="#,##0" />%',
						</c:when>
						<c:otherwise>year_${status.index}: '0%',</c:otherwise>
					</c:choose>
				</c:forEach>
				<c:choose>
					<c:when test="${totallandingPageRegistrations!=0}">
						total: '<fmt:formatNumber value="${(totallandingPageActivations/totallandingPageRegistrations)*100}" pattern="#,##0" />%',
					</c:when>
					<c:otherwise>total: '0%',</c:otherwise>
				</c:choose>
				summary: true,
			});
			
			data.push({
				name: '<fmt:message key="allOtherRegistration" />',
				<c:forEach items="${model.reports}" var="reports" varStatus="status">
					year_${status.index}: '<fmt:formatNumber value="${reports.allOtherRegistrations}" pattern="#,##0" />',
					<c:set var="totalallOtherRegistrations" value="${reports.allOtherRegistrations+totalallOtherRegistrations}"/>
				</c:forEach>
				total: '<fmt:formatNumber value="${totalallOtherRegistrations}" pattern="#,##0" />',
			});
			
			data.push({
				name: '<fmt:message key="allOtherActivation" />',
				<c:forEach items="${model.reports}" var="reports" varStatus="status">
					year_${status.index}: '<fmt:formatNumber value="${reports.allOtherActivations}" pattern="#,##0" />',
					<c:set var="totalallOtherActivations" value="${reports.allOtherActivations+totalallOtherActivations}"/>
				</c:forEach>
				total: '<fmt:formatNumber value="${totalallOtherActivations}" pattern="#,##0" />',
			});
			
			data.push({
				name: '<fmt:message key="closeRate" />',
		    		<c:forEach items="${model.reports}" var="reports" varStatus="status">
					<c:choose>
						<c:when test="${reports.allOtherRegistrations!=0}">
							year_${status.index}: '<fmt:formatNumber value="${(reports.allOtherActivations/reports.allOtherRegistrations)*100}" pattern="#,##0.00" />%',
						</c:when>
						<c:otherwise>year_${status.index}: '0%',</c:otherwise>
					</c:choose>
				</c:forEach>
				<c:choose>
					<c:when test="${totalallOtherRegistrations!=0}">
						total: '<fmt:formatNumber value="${(totalallOtherActivations/totalallOtherRegistrations)*100}" pattern="#,##0.00" />%',
					</c:when>
					<c:otherwise>total: '0%',</c:otherwise>
				</c:choose>
				summary: true,
			});
			
			data.push({
				name: '<fmt:message key="merchandise" />',
				<c:forEach items="${model.reports}" var="reports" varStatus="status">
					year_${status.index}: '$<fmt:formatNumber value="${reports.merchandize}" pattern="#,##0" />',
					<c:set var="totalmerchandize" value="${reports.merchandize+totalmerchandize}"/>
				</c:forEach>
				total: '$<fmt:formatNumber value="${totalmerchandize}" pattern="#,##0" />',
			});
			
			data.push({
				name: '<fmt:message key="shipping" />',
				<c:forEach items="${model.reports}" var="reports" varStatus="status">
					year_${status.index}: '$<fmt:formatNumber value="${reports.shipping}" pattern="#,##0" />',
					<c:set var="totalshipping" value="${reports.shipping+totalshipping}"/>
				</c:forEach>
				total: '$<fmt:formatNumber value="${totalshipping}" pattern="#,##0" />',
			});
			
			data.push({
				name: '<fmt:message key="ccFee" />',
				<c:forEach items="${model.reports}" var="reports" varStatus="status">
					year_${status.index}: '$<fmt:formatNumber value="${reports.ccFee}" pattern="#,##0" />',
					<c:set var="totalccFee" value="${reports.ccFee+totalccFee}"/>
				</c:forEach>
				total: '$<fmt:formatNumber value="${totalccFee}" pattern="#,##0" />',
			});
			
			data.push({
				name: '<fmt:message key="grandTotal" />',
		    		<c:forEach items="${model.reports}" var="reports" varStatus="status">
		    			year_${status.index}: '$<fmt:formatNumber value="${reports.grandTotal}" pattern="#,##0" />',
		    			<c:set var="totalgrandTotal" value="${reports.grandTotal+totalgrandTotal}"/>
				</c:forEach>
				total: '$<fmt:formatNumber value="${totalgrandTotal}" pattern="#,##0" />',
				summary: true,
			});
			
			data.push({
				name: '#<fmt:message key="customers" />',
		    		<c:forEach items="${model.reports}" var="reports" varStatus="status">
		    			year_${status.index}: '<fmt:formatNumber value="${reports.customersPurchased}" pattern="#,###" />',
		    			<c:set var="totalcustomersPurchased" value="${reports.customersPurchased+totalcustomersPurchased}"/>
				</c:forEach>
				total: '<fmt:formatNumber value="${totalcustomersPurchased}" pattern="#,###" />',
			});
			
			data.push({
				name: '#<fmt:message key="orders" />',
		    		<c:forEach items="${model.reports}" var="reports" varStatus="status">
		    			year_${status.index}: '<fmt:formatNumber value="${reports.totalOrders}" pattern="#,###" />',
		    			<c:set var="totalOrder" value="${reports.totalOrders+totalOrder}"/>
				</c:forEach>
				total: '<fmt:formatNumber value="${totalOrder}" pattern="#,###" />',
			});
			
			data.push({
				name: '<fmt:message key="avgOrders" />',
		    		<c:forEach items="${model.reports}" var="reports" varStatus="status">
		    			year_${status.index}: '$<fmt:formatNumber value="${reports.avgOrders}" pattern="#,###" />',
		    			<c:set var="totalavgOrders" value="${reports.avgOrders+totalavgOrders}"/>
				</c:forEach>
				total: '$<fmt:formatNumber value="${totalavgOrders}" pattern="#,###" />',
			});
			
			data.push({
				name: '<fmt:message key="avgCustomers" />',
		    		<c:forEach items="${model.reports}" var="reports" varStatus="status">
		    			year_${status.index}: '$<fmt:formatNumber value="${reports.avgCustomers}" pattern="#,###" />',
		    			<c:set var="totalavgCustomers" value="${reports.avgCustomers+totalavgCustomers}"/>
				</c:forEach>
				total: '$<fmt:formatNumber value="${totalavgCustomers}" pattern="#,###" />',
			});
			
			//Columns Definitions
			require(["dgrid/List", "dgrid/OnDemandGrid", "dgrid/Selection", "dgrid/Keyboard", "dgrid/Editor", "dijit/form/CheckBox", 
					"dijit/form/TimeTextBox", "dijit/form/Select", "dojo/data/ObjectStore", "dijit/form/FilteringSelect",
					'dijit/form/DropDownButton', 'dijit/DropDownMenu', 'dijit/MenuItem',
					"dojo/_base/declare", "dojo/_base/lang", "dojo/store/Memory", "dojo/store/Observable", "dgrid/test/data/createSyncStore", 
					'dgrid/extensions/ColumnReorder', 'dgrid/extensions/ColumnResizer',
				], function (List, OnDemandGrid, Selection, Keyboard, Editor, CheckBox, TimeTextBox, Select, ObjectStore, FilteringSelect,
							DropDownButton, DropDownMenu, MenuItem,
							declare, lang, LegacyMemory, LegacyObservable, createSyncStore, ColumnReorder, ColumnResizer) {
				var columns = {
					name: {
						label: 'CUSTOMER METRICS',
						get: function(object){
							return object.name;
						},
						sortable: false,
					},
					<c:forEach items="${model.fiveYears}" var="year" varStatus="status">
						year_${status.index}: {
							label: '<c:out value="${year}"/>',
							get: function(object){
								return object.year_${status.index};
							},
							sortable: false,
						},
					</c:forEach>
					total: {
						label: '<fmt:message key="total" />',
						get: function(object){
							return object.total;
						},
						sortable: false,
					}
				};
				
				var grid = new (declare([OnDemandGrid, Selection, Keyboard, Editor, ColumnReorder, ColumnResizer]))({
					collection: createSyncStore({ data: data }),
					selectionMode: "none",
					allowSelectAll: true,
					columns: columns,
					renderRow: function (object) {
						var rowElement = this.inherited(arguments);
				        if(object.summary){
				       	 	rowElement.className += ' summary';
				        }
				        return rowElement;
				    }
				},'grid');

				grid.on('dgrid-refresh-complete', function(event) {
					$('#grid-hider-menu').appendTo('#column-hider');
				});
				
				grid.startup();
			});
		</script>
	</tiles:putAttribute>  
</tiles:insertDefinition>