<%@ page import="java.net.*" %>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/security/tags" prefix="sec" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ taglib uri="/WEB-INF/tlds/custom.tld" prefix="wj" %>

<tiles:insertDefinition name="admin.responsive-template.ticket" flush="true">
	<tiles:putAttribute name="css">
		<style>
			#grid .field-companyName{
			    width: 200px;
			}
			#grid .field-subject{
			    width: 200px;
			}
			#grid .field-customer{
			    width: 150px;
			}
			#grid .field-created{
			    width: 150px;
			}
			\#grid .field-lastModified{
			    width: 150px;
			}
		</style>
	</tiles:putAttribute>
	<tiles:putAttribute name="content" type="string">
		<sec:authorize ifAnyGranted="ROLE_AEM,ROLE_ADMIN_${prop_site_id},ROLE_TICKET_VIEW_LIST">  
			<c:if test="${gSiteConfig['gTICKET']}">	   
				<form id="list" action="ticketList.jhtm" method="post">
					<div class="page-banner">
						<div class="breadcrumb"><span><a href="${_contextpath}/admin/catalog/dashboard.jhtm">Home</a></span><i class="sli-arrow-right"></i><span><fmt:message key="ticket" /></span></div>
					</div>
					<div class="page-head">
						<div class="row justify-content-between">
							<div class="col-sm-8 title">
								<h3><fmt:message key="ticket" /></h3>
							</div>
							<div class="col-sm-4 actions">
			                    <div class="dropdown dropdown-setting">
									<button type="button" class="btn-outline dropdown-toggle active btn btn-primary" data-toggle="dropdown">
			                             <i class="sli-screen-desktop"></i> Display
			                         </button>	                         
									<div class="dropdown-menu dropdown-menu-right">
										<div class="form-group">
					                    		<div class="option-header">Rows</div>
											<select class="custom-select form-control" name="size" onchange="document.getElementById('page').value=1;submit();">
												<c:forTokens items="10,25,50,100,200,500" delims="," var="current">
										  	  		<option value="${current}" <c:if test="${current == model.tickets.pageSize}">selected</c:if>>${current}</option>
									  			</c:forTokens>
											</select>
										</div>
										<div class="form-group">
					                    		<div class="option-header">Columns</div>
					                    		<div id="column-hider"></div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
					<div class="page-body">
						<div class="grid-stage">
							<c:if test="${model.tickets.nrOfElements > 0}">
								<div class="page-stat">
									<fmt:message key="showing">
										<fmt:param value="${model.tickets.firstElementOnPage + 1}"/>
										<fmt:param value="${model.tickets.lastElementOnPage + 1}"/>
										<fmt:param value="${model.tickets.nrOfElements}"/>
									</fmt:message>
								</div>
							</c:if>
							<table id="grid"></table>
							<div class="footer">
				                <div class="float-right">
									<c:if test="${model.tickets.firstPage}">
								 		<span class="previous"><i class="sli-arrow-left" style="font-size: 0.7rem"></i> Previous</span>
									</c:if>
									<c:if test="${not model.tickets.firstPage}">
										<a href="<c:url value="ticketList.jhtm"><c:param name="page" value="${model.tickets.page}"/></c:url>">
											<span class="previous"><i class="sli-arrow-left" style="font-size: 0.7rem"></i> Previous</span>
										</a>
									</c:if>
									<span class="status"><c:out value="${model.tickets.page+1}" /> of <c:out value="${model.tickets.pageCount}" /></span>
									<c:if test="${model.tickets.lastPage}">
									 	<span class="next">Next <i class="sli-arrow-right" style="font-size: 0.7rem"></i></span>
									</c:if>
									<c:if test="${not model.tickets.lastPage}">
										<a href="<c:url value="ticketList.jhtm"><c:param name="page" value="${model.tickets.page+2}"/></c:url>">
											<span class="next">Next <i class="sli-arrow-right" style="font-size: 0.7rem"></i></span>
										</a>
									</c:if>
								</div>
							</div>
						</div>
					</div>
					<input type="hidden" id="sort" name="sort" value="${ticketSearch.sort}" />
				</form>
			</c:if>
		</sec:authorize>
	</tiles:putAttribute>
	<tiles:putAttribute name="js">
		<!-- DGRID TABLE SCRIPT MUST BE AT BOTTOM OF THE PAGE -->
		<script src="${_contextpath}/admin-responsive-static/assets/plugins/dojo/dojo.js" data-dojo-config="async: true"></script>
		<script>
			//Table Data Sanitization
			var data = [];
			
			<c:forEach items="${model.tickets.pageList}" var="ticket" varStatus="status">
		    		data.push({
		    			id: '${ticket.ticketId}',
					status: '${ticket.status}',
					companyName: '${ticket.companyName}',
					subject: '${ticket.subject}',
					type: '${ticket.type}',
					userId: '${ticket.userId}',
					firstName: '${ticket.firstName}',
					lastName: '${ticket.lastName}',
					created: '<fmt:formatDate type="both" timeStyle="full" pattern="MM/dd/yyyy hh:mm a" value="${ticket.created}"/>',
					lastModified: '<fmt:formatDate type="both" timeStyle="full" pattern="MM/dd/yyyy hh:mm a" value="${ticket.lastModified}"/>',
					<c:if test="${gSiteConfig['gMULTI_STORE'] > 0}">
						host: '${wj:escapeJS(ticket.host)}',
					</c:if>
				});
			</c:forEach>

			//Columns Definitions
			require(['dojo/_base/lang',
					'dojo/_base/declare',
					'dstore/Memory',
					'dstore/Trackable',
					'dgrid/OnDemandGrid',
					'dgrid/Editor',
					'dgrid/extensions/ColumnHider',
					'dgrid/extensions/ColumnReorder',
					'dgrid/extensions/ColumnResizer',
					'dgrid/Selector',
					'dijit/form/DropDownButton',
					'dijit/DropDownMenu',
					'dijit/MenuItem',
					'summary/SummaryRow',
				], function (lang, declare, Memory, Trackable, OnDemandGrid, Editor, ColumnHider, ColumnReorder, ColumnResizer, Selector, DropDownButton, DropDownMenu, MenuItem, SummaryRow) {
				var columns = {
					status: {
						label: '<fmt:message key="status" />',
						get: function(object){
							return object.status;
						},
						sortable: false,
					},
					id: {
						label: '<fmt:message key="ticket" />',
						renderCell: function(object){
							var link = document.createElement('a');
							link.href = 'ticketUpdate.jhtm?id='+object.id;
							link.textContent = object.id;
							return link;
						},
						sortable: false,
					},
					companyName: {
						label: '<fmt:message key="company" />',
						get: function(object){
							return object.companyName;
						},
						sortable: false,
					},
					subject:{
						label: '<fmt:message key="subject" />',
						get: function(object){
							return object.subject;
						},
						sortable: false,
					},
					type:{
						label: '<fmt:message key="type" />',
						get: function(object){
							return object.type;
						},
						sortable: false,
					},
					customer:{
						label: '<fmt:message key="customer" />',
						renderCell: function(object){
							var link = document.createElement('a');
							link.className = "plain";
							link.href = "../customers/customer.jhtm?id="+object.userId;
							link.textContent = object.lastName + ', ' + object.firstName;
							return link;
						},
						sortable: false,
					},
					created:{
						label: '<fmt:message key="created" />',
						get: function(object){
							return object.created;
						},
						sortable: false,
					},
					lastModified:{
						label: '<fmt:message key="lastModified" />',
						get: function(object){
							return object.lastModified;
						},
						sortable: false,
					},
					<c:if test="${gSiteConfig['gMULTI_STORE'] > 0}">
						host:{
							label: '<fmt:message key="multiStore" />',
							get: function(object){
								return object.host;
							},
							sortable: false,
						},
					</c:if>
				};
				var store = new (declare([Memory, Trackable]))({
					data: data
				});
				
				var grid = new (declare([OnDemandGrid, Editor, ColumnHider, ColumnReorder, ColumnResizer, Selector, SummaryRow]))({
					collection: store,
					selectionMode: "none",
					allowSelectAll: true,
					columns: columns
				},'grid');
				
				//sorting columns
				grid.on("dgrid-sort", lang.hitch(this, function(e){
					e.preventDefault();						
					var property = e.sort[0].property;
				    var order = e.sort[0].descending ? "DESC" : "ASC";
				    document.getElementById('sort').value = property + ' ' + order;
				    document.getElementById('list').submit();
				}));
				
				grid.on('dgrid-refresh-complete', function(event) {
					$('#grid-hider-menu').appendTo('#column-hider');
				});
				
				grid.startup();
			});
		</script>
	</tiles:putAttribute>    
</tiles:insertDefinition>
