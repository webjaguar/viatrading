<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ taglib uri="http://www.springframework.org/security/tags" prefix="sec" %>
<%@ taglib uri="/WEB-INF/tlds/spring.tld" prefix="spring" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ taglib uri="/WEB-INF/tlds/custom.tld" prefix="wj" %>

<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta name="description" content="Add your Site description Here.">
<link rel="shortcut icon" href="http://boblillypromo.wjserver590.com/assets/img/favicon.ico">

<tiles:insertDefinition name="admin.responsive-template.accounting" flush="true"> 
	<tiles:putAttribute name="css">
	</tiles:putAttribute>
	<tiles:putAttribute name="content" type="string">
		<!-- Begin: Content -->
		<section id="content" class="animated fadeIn"> 
		<form action="salesForceDataMapping.jhtm" method="post">
		<input type="hidden" name="id"  value="1" id="id">
		
		<input type="hidden" name="index"  value="1" id="index">
			 <%-- <div class="panel panel-default">  
			 	<!-- Menu Start -->
			 	<div class="panel-menu">
			 		<tbody>
                        <tr>
                            <td class="pr-15 w-175 hidden-xs"> Customer Mapping
                            </td>
                        </tr>
                    </tbody>
			 	
			 	</div>
				<!-- Menu End -->
				
				<table class="responsiveBootstrapFooTable table table-hover" data-sorting="true">
                	<thead>
                		<tr class="bg-light">
                			 <th class="text-nowrap" data-type="html" data-breakpoints="xs sm md"">
                                Sales Force Field Name 
                             </th>
                			<th class="text-nowrap " data-type="html">
                                Webjaguar Customer Field
                            </th>
                		</tr>
                    </thead>
                    <tbody>
                    
                    	<tr>
                    		<td>
                    			<input type="text" size="60" value="" id="custom_field_" />
                    		</td>
                    		<td>
                    		  <select class="form-control input-sm" name="wj_order_field"  title="Select Order Field" style="width:60%">
                    		  	<option value="">Select Customer Field</option>
                    		  	<option value="_cust_email">Email</option>
                    		  	<option value="_cust_name">First Name + Last Name</option>
                    		  	<option value="_cust_f_name">First Name</option>
                    		  	<option value="_cust_l_name">Last Name</option>
                    		  	<option value="_cust_account_num">Account #</option>
                    		  </select>
                    		</td>
                    	</tr>
                    	
                    	<tr>
                    		<td>
                    			<input type="text" size="60" value="" id="custom_field_" />
                    		</td>
                    		<td>
                    			<input type="text" size="40" value="" id="wj_field_" />
                    		</td>
                    	</tr>
                    </tbody>
               </table>
				
				
	
				<!-- Footer Start -->
				<div class="panel-footer">
	                <div class="clearfix">
	                   <div class="pull-right">  
							<button type="submit" class="btn btn-primary mr-10" name="__update" value="<fmt:message key="update" />">
		                        <i class="fa fa-plus mr-5"></i> Update
		                    </button>
		                </div>
	                </div>
	            </div>  
				<!-- Footer End -->
			</div> --%>
			
			
			<%-- <div class="panel panel-default">  
			 	<!-- Menu Start -->
			 	<div class="panel-menu">
			 		<tbody>
                        <tr>
                            <td class="pr-15 w-175 hidden-xs"> CRM Contacts Mapping
                            </td>
                        </tr>
                    </tbody>
			 	
			 	</div>
				<!-- Menu End -->
				
				<table class="responsiveBootstrapFooTable table table-hover" data-sorting="true">
                	<thead>
                		<tr class="bg-light">
                			 <th class="text-nowrap" data-type="html" data-breakpoints="xs sm md"">
                                Sales Force Field Name 
                             </th>
                			<th class="text-nowrap " data-type="html">
                                Webjaguar Contact Field
                            </th>
                		</tr>
                    </thead>
                    <tbody>
                    
                    	<tr>
                    		<td>
                    			<input type="text" size="60" value="" id="custom_field_" />
                    		</td>
                    		<td>
                    		  <select class="form-control input-sm" name="wj_order_field"  title="Select Order Field" style="width:60%">
                    		  	<option value="">Select Contact Field</option>
                    		  	<option value="_cust_email">Email</option>
                    		  	<option value="_cust_name">First Name + Last Name</option>
                    		  	<option value="_cust_f_name">First Name</option>
                    		  	<option value="_cust_l_name">Last Name</option>
                    		  	<option value="_cust_account_num">Account #</option>
                    		  </select>
                    		</td>
                    	</tr>
                    	
                    	<tr>
                    		<td>
                    			<input type="text" size="60" value="" id="custom_field_" />
                    		</td>
                    		<td>
                    			<input type="text" size="40" value="" id="wj_field_" />
                    		</td>
                    	</tr>
                    </tbody>
               </table>
				
				
	
				<!-- Footer Start -->
				<div class="panel-footer">
	                <div class="clearfix">
	                   <div class="pull-right">  
							<button type="submit" class="btn btn-primary mr-10" name="__update" value="<fmt:message key="update" />">
		                        <i class="fa fa-plus mr-5"></i> Update
		                    </button>
		                </div>
	                </div>
	            </div>  
				<!-- Footer End -->
			</div>
			 --%>
			
			
			
			
			 <div class="panel panel-default">  
			 	<!-- Menu Start -->
			 	<div class="panel-menu">
			 		<tbody>
                        <tr>
                            <td class="pr-15 w-175 hidden-xs"> Orders Mapping <a onClick="addField()"><img src="../graphics/add.png"></a>
                            </td>
                        </tr>
                    </tbody>
			 	
			 	</div>
				<!-- Menu End -->
				
				<table class="responsiveBootstrapFooTable table table-hover" data-sorting="true" id="orderMappingTable">
                	<thead>
                		<tr class="bg-light">
                			 <th class="text-nowrap" data-type="html" data-breakpoints="xs sm md"">
                                Sales Force Field Name 
                             </th>
                			<th class="text-nowrap " data-type="html">
                                Webjaguar Order Field
                            </th>
                		</tr>
                    </thead>
                    <tbody id="orderMappingTableBody">
                    
                    <c:set var="statusIndex" value="0" />            
                    <c:forEach items="${model.mappingForm.integrationMappin.fieldsMap}" var="fieldM" varStatus="status">
                     <c:set var="statusIndex" value="${statusIndex + 1}" />
                    	                    	
                    	<tr>
                    		<td>
                    			<input type="text" size="60" value="<c:out value="${fieldM.key}"/>" name="field_name_${statusIndex}" />
                    		</td>
                    		<td>
                    		  <c:choose>
                    		  	<c:when test="${!fn:contains(model.columnHeaders, fieldM.value)}">
                    		  		<input type="text" size="40" value="<c:out value="${fieldM.value}"/>" name="field_value_${statusIndex}" />
                    		  	</c:when>
                    		  	<c:otherwise>
		                    		  <select class="form-control input-sm" name="field_value_${statusIndex}"  title="Select Order Field" style="width:60%">
		                    		  	<option value="">Select Order Field</option>
		                    		  	<c:if test="">selected</c:if>
		                    		  	<option value="order_id" <c:if test="${fieldM.value == 'order_id'}">selected</c:if>>order #</option>
		                    		  	<option value="date_ordered" <c:if test="${fieldM.value == 'date_ordered'}">selected</c:if>>Date Ordered</option>
		                    		  	<option value="last_modified" <c:if test="${fieldM.value == 'last_modified'}">selected</c:if>>Last Modified</option>
		                    		  	<option value="status" <c:if test="${fieldM.value == 'status'}">selected</c:if>>Status</option>
		                    		  	<option value="field_1" <c:if test="${fieldM.value == 'field_1'}">selected</c:if>>Field 1</option>
		                    		  	<option value="field_2" <c:if test="${fieldM.value == 'field_2'}">selected</c:if>>Field 2</option>
		                    		  	<option value="field_3" <c:if test="${fieldM.value == 'field_3'}">selected</c:if>>Field 3</option>
		                    		  	<option value="field_4" <c:if test="${fieldM.value == 'field_4'}">selected</c:if>>Field 4</option>
		                    		  	<option value="field_5" <c:if test="${fieldM.value == 'field_5'}">selected</c:if>>Field 5</option>
		                    		  	<option value="trackcode" <c:if test="${fieldM.value == 'trackcode'}">selected</c:if>>Trackcode</option>
		                    		  	<option value="bill_to_addr1" <c:if test="${fieldM.value == 'bill_to_addr1'}">selected</c:if>>Billing Street</option>
		                    		  	<option value="bill_to_city" <c:if test="${fieldM.value == 'bill_to_city'}">selected</c:if>>Billing City</option>
		                    		  	<option value="bill_to_state_province" <c:if test="${fieldM.value == 'bill_to_state_province'}">selected</c:if>>Billing State</option>
		                    		  	<option value="bill_to_zip" <c:if test="${fieldM.value == 'bill_to_zip'}">selected</c:if>>Billing Zip Code</option>
		                    		  	<option value="bill_to_country" <c:if test="${fieldM.value == 'bill_to_country'}">selected</c:if>>Billing Country</option>
		                    		  	<option value="ship_to_first_name" <c:if test="${fieldM.value == 'ship_to_first_name'}">selected</c:if>>Ship To Name</option>
		                    		  	<option value="ship_to_addr1" <c:if test="${fieldM.value == 'ship_to_addr1'}">selected</c:if>>Shipping Street</option>
		                    		  	<option value="ship_to_city" <c:if test="${fieldM.value == 'ship_to_city'}">selected</c:if>>Shipping City</option>
		                    		  	<option value="ship_to_state_province" <c:if test="${fieldM.value == 'ship_to_state_province'}">selected</c:if>>Shipping State</option>
		                    		  	<option value="ship_to_zip" <c:if test="${fieldM.value == 'ship_to_zip'}">selected</c:if>>Shipping Zip Code</option>
		                    		  	<option value="ship_to_country" <c:if test="${fieldM.value == 'ship_to_country'}">selected</c:if>>Shipping Country</option>
		                    		  	<option value="tax_rate" <c:if test="${fieldM.value == 'tax_rate'}">selected</c:if>>Tax Rate</option>
		                    		  </select>
                    		  	</c:otherwise>
                    		  </c:choose>
                    		</td>
                    	</tr>
                    </c:forEach>
                    <%--
                    <tr>
			  			<td><a onClick="addField()"><img src="../graphics/add.png"></a></td>
			  			<td>&nbsp;&nbsp;</td>
			  		</tr> --%>
                    </tbody>
               </table>
				
				
	
				<!-- Footer Start -->
				<div class="panel-footer">
	                <div class="clearfix">
	                   <div class="pull-right">  
							<button type="submit" class="btn btn-primary mr-10" name="__update" value="<fmt:message key="update" />">
		                        <i class="fa fa-plus mr-5"></i> Update
		                    </button>
		                </div>
	                </div>
	            </div>  
				<!-- Footer End -->
			</div>
			
			
		</form>
		</section>
	</tiles:putAttribute>
	<tiles:putAttribute name="js">
		<script type="text/JavaScript">
		<!--	
			function addField() {
			  
			  var index = document.getElementById('index').value;
			  console.log("index: " + index);
			  var tr = document.createElement("tr");
		
			  var td1 = document.createElement("td");
			  td1.setAttribute("class", "footable-first-visible");
			  td1.setAttribute("style", "display: table-cell;");
			  var textfield1 = document.createElement("input");
			  textfield1.setAttribute("type", "text"); 
			  textfield1.setAttribute("name", "__name_new_"+index);
			  textfield1.setAttribute("maxlength", "255");
			  textfield1.setAttribute("size", "60");
			  td1.appendChild(textfield1);
			  tr.appendChild(td1);
			  console.log("Test td1");
			  
			  var td2 = document.createElement("td");
			  td2.setAttribute("class", "footable-first-visible");
			  td2.setAttribute("style", "display: table-cell;");
			  var textfield2 = document.createElement("input");
			  textfield2.setAttribute("type", "text"); 
			  textfield2.setAttribute("name", "__value_new_"+index);
			  textfield2.setAttribute("maxlength", "255");
			  td2.appendChild(textfield2);
			  tr.appendChild(td2);
			  console.log("Test td2: " + tr);
			  document.getElementById('orderMappingTableBody').appendChild(tr);
			  document.getElementById('index').value = parseInt(index) + 1;
			  console.log("index1: " + document.getElementById('index').value);
		}
		//-->
		</script>
	</tiles:putAttribute>
</tiles:insertDefinition>