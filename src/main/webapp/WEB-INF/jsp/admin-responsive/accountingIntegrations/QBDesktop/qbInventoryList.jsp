<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ taglib uri="http://www.springframework.org/security/tags" prefix="sec" %>
<%@ taglib uri="/WEB-INF/tlds/spring.tld" prefix="spring" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ taglib uri="/WEB-INF/tlds/custom.tld" prefix="wj" %>

<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta name="description" content="Add your Site description Here.">
<link rel="shortcut icon" href="http://boblillypromo.wjserver590.com/assets/img/favicon.ico">

<tiles:insertDefinition name="admin.responsive-template.accounting" flush="true"> 
	<tiles:putAttribute name="css">
	</tiles:putAttribute>>
	<tiles:putAttribute name="content" type="string">
	
		<!-- Begin: Content -->
		<section id="content" class="animated fadeIn"> 
		<form action="qBUpdateInventorySkuList.jhtm" method="post">
		<input type="hidden" name="id"  value="1" id="id">
		
		<input type="hidden" name="index"  value="1" id="index">
			 <%-- <div class="panel panel-default">  
			 	<!-- Menu Start -->
			 	<div class="panel-menu">
			 		<tbody>
                        <tr>
                            <td class="pr-15 w-175 hidden-xs"> Customer Mapping
                            </td>
                        </tr>
                    </tbody>
			 	
			 	</div>
				<!-- Menu End -->
				
				<table class="responsiveBootstrapFooTable table table-hover" data-sorting="true">
                	<thead>
                		<tr class="bg-light">
                			 <th class="text-nowrap" data-type="html" data-breakpoints="xs sm md"">
                                Sales Force Field Name 
                             </th>
                			<th class="text-nowrap " data-type="html">
                                Webjaguar Customer Field
                            </th>
                		</tr>
                    </thead>
                    <tbody>
                    
                    	<tr>
                    		<td>
                    			<input type="text" size="60" value="" id="custom_field_" />
                    		</td>
                    		<td>
                    		  <select class="form-control input-sm" name="wj_order_field"  title="Select Order Field" style="width:60%">
                    		  	<option value="">Select Customer Field</option>
                    		  	<option value="_cust_email">Email</option>
                    		  	<option value="_cust_name">First Name + Last Name</option>
                    		  	<option value="_cust_f_name">First Name</option>
                    		  	<option value="_cust_l_name">Last Name</option>
                    		  	<option value="_cust_account_num">Account #</option>
                    		  </select>
                    		</td>
                    	</tr>
                    	
                    	<tr>
                    		<td>
                    			<input type="text" size="60" value="" id="custom_field_" />
                    		</td>
                    		<td>
                    			<input type="text" size="40" value="" id="wj_field_" />
                    		</td>
                    	</tr>
                    </tbody>
               </table>
				
				
	
				<!-- Footer Start -->
				<div class="panel-footer">
	                <div class="clearfix">
	                   <div class="pull-right">  
							<button type="submit" class="btn btn-primary mr-10" name="__update" value="<fmt:message key="update" />">
		                        <i class="fa fa-plus mr-5"></i> Update
		                    </button>
		                </div>
	                </div>
	            </div>  
				<!-- Footer End -->
			</div> --%>
			
			
			<%-- <div class="panel panel-default">  
			 	<!-- Menu Start -->
			 	<div class="panel-menu">
			 		<tbody>
                        <tr>
                            <td class="pr-15 w-175 hidden-xs"> CRM Contacts Mapping
                            </td>
                        </tr>
                    </tbody>
			 	
			 	</div>
				<!-- Menu End -->
				
				<table class="responsiveBootstrapFooTable table table-hover" data-sorting="true">
                	<thead>
                		<tr class="bg-light">
                			 <th class="text-nowrap" data-type="html" data-breakpoints="xs sm md"">
                                Sales Force Field Name 
                             </th>
                			<th class="text-nowrap " data-type="html">
                                Webjaguar Contact Field
                            </th>
                		</tr>
                    </thead>
                    <tbody>
                    
                    	<tr>
                    		<td>
                    			<input type="text" size="60" value="" id="custom_field_" />
                    		</td>
                    		<td>
                    		  <select class="form-control input-sm" name="wj_order_field"  title="Select Order Field" style="width:60%">
                    		  	<option value="">Select Contact Field</option>
                    		  	<option value="_cust_email">Email</option>
                    		  	<option value="_cust_name">First Name + Last Name</option>
                    		  	<option value="_cust_f_name">First Name</option>
                    		  	<option value="_cust_l_name">Last Name</option>
                    		  	<option value="_cust_account_num">Account #</option>
                    		  </select>
                    		</td>
                    	</tr>
                    	
                    	<tr>
                    		<td>
                    			<input type="text" size="60" value="" id="custom_field_" />
                    		</td>
                    		<td>
                    			<input type="text" size="40" value="" id="wj_field_" />
                    		</td>
                    	</tr>
                    </tbody>
               </table>
				
				
	
				<!-- Footer Start -->
				<div class="panel-footer">
	                <div class="clearfix">
	                   <div class="pull-right">  
							<button type="submit" class="btn btn-primary mr-10" name="__update" value="<fmt:message key="update" />">
		                        <i class="fa fa-plus mr-5"></i> Update
		                    </button>
		                </div>
	                </div>
	            </div>  
				<!-- Footer End -->
			</div>
			 --%>
			
			
			
			
			 <div class="panel panel-default">  
			 	<!-- Menu Start -->
			 	<div class="panel-menu">
			 		<tbody>
                        <tr>
                            <td class="pr-15 w-175 hidden-xs"> Invnetory Sku List 
                            </td>
                        </tr>
                    </tbody>
			 	
			 	</div>
				<!-- Menu End -->
				
				<table class="responsiveBootstrapFooTable table table-hover" data-sorting="true" id="orderMappingTable">
                	<thead>
                		<tr class="bg-light">
                			 <th class="text-nowrap" data-type="html" data-breakpoints="xs sm md"">
                                Product Skus
                             </th>
                			<th class="text-nowrap " data-type="html">
                                &nbsp; &nbsp;
                            </th>
                		</tr>
                    </thead>
                    <tbody id="orderMappingTableBody">
                    
                    <c:forEach items="${model.skuList}" var="sku" varStatus="status">
                    	                    	
                    	<tr>
                    		<td>
                    			<label>${sku}</label>
                    		</td>
                    		<td> 
                    			&nbsp; &nbsp;
                    		</td>
                    	</tr>
                    </c:forEach>
                    </tbody>
               </table>
				
				
				
				<!-- Footer Start -->
				<div class="panel-footer">
	                <div class="clearfix">
	                	<div class="clearfix">
		                    <div class="pull-left">
								<div class="pull-left mb-xs-15">
									<p class="text-danger mb-0">	
                						<input type="checkbox" name="_clear_list" id="_clear_list"> &nbsp; Clear List
									</p>
								</div>
								</div></br>
								<div class="pull-left mb-xs-15">
									<p class="text-danger mb-0">	
										<a href="../catalog/productList.jhtm">View Product List</a>
									</p>
								</div>
		                    </div>
	                
	                
	                   <div class="pull-right">  
							<button type="submit" class="btn btn-primary mr-10" name="__update" value="<fmt:message key="update" />">
		                        <i class="fa fa-plus mr-5"></i> Update
		                    </button>
		                </div>
	                </div>
	            </div>
				<!-- Footer End -->
			</div>
			
			
		</form>
		</section>
	</tiles:putAttribute>
	<tiles:putAttribute name="js">
	
	<script type="text/JavaScript">
	<!--	
		function addField() {
		  
		  var index = document.getElementById('index').value;
		  
		  var tr = document.createElement("tr");
	
		  var td1 = document.createElement("td");
		  var textfield1 = document.createElement("input");
		  textfield1.setAttribute("type", "text"); 
		  textfield1.setAttribute("name", "__name_new_"+index);
		  textfield1.setAttribute("maxlength", "255");
		  textfield1.setAttribute("size", "60");
		  td1.appendChild(textfield1);
		  tr.appendChild(td1);
		  
		  
		  var td2 = document.createElement("td");
		  var textfield2 = document.createElement("input");
		  textfield2.setAttribute("type", "text"); 
		  textfield2.setAttribute("name", "__value_new_"+index);
		  textfield2.setAttribute("maxlength", "255");
		  td2.appendChild(textfield2);
		  tr.appendChild(td2);
		
		  document.getElementById('orderMappingTableBody').appendChild(tr);
		  document.getElementById('index').value= parseInt(index) + 1;
	}
	//-->
	</script>
	</tiles:putAttribute>
</tiles:insertDefinition>