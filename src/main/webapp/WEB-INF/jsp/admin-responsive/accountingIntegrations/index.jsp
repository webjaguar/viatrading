<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>


<tiles:insertDefinition name="admin.responsive-template.accounting" flush="true"> 
	<tiles:putAttribute name="css">
	</tiles:putAttribute>
	<tiles:putAttribute name="content" type="string">
		<section id="content" class="table-layout animated fadeIn">

			<!-- begin: .tray-center -->
			<div class="tray tray-center">

				<!-- dashboard tiles -->
				<div class="row">
				
		  		<c:if test="${siteConfig['SALES_FORCE_INTEGRATION'].value == 'true'}">
				
					<div class="col-sm-12 col-md-6 col-lg-4">  
						<a href="${_contextpath}/admin/accountingIntegrations/salesForceDataMapping.jhtm" class="panel panel-default panel-tile text-center br-w-2 br-system">
							<div class="panel-body">
								<h1 class="fs-30 mt-5 mb-0">Sales Force</h1>
								<h2 class="fs-20 text-system">Data Mapping</h2>
							</div>
						</a> 
					</div>
					
				</c:if>
				</div>


			</div>
			<!-- end: .tray-center -->

		</section>
	</tiles:putAttribute>
	<tiles:putAttribute name="js">
	</tiles:putAttribute>
</tiles:insertDefinition>
