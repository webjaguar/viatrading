<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="/WEB-INF/tlds/spring.tld" prefix="spring" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ taglib uri="http://www.springframework.org/security/tags" prefix="sec" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>

<div id="tab_4" class="tab-pane">
    	<sec:authorize ifAnyGranted="ROLE_AEM,ROLE_ADMIN_${prop_site_id},ROLE_CATEGORY_EDIT">
		<div class="line">
			<div class="form-row">
				<div class="col-label">
					<label for="category_linkType" class="col-form-label">
						<span class="fa fa-question-circle" data-toggle="tooltip" data-placement="top" title="Image::Upload the image of the product. Max Image size is 2MB."></span><fmt:message key="type" />
					</label>
				</div>
				<div class="col-lg-4"> 
					<form:select path="category.linkType" id="category_linkType" class="custom-select">     
					<c:forEach items="${linkTypes}" var="linkType">
						<form:option value="${linkType}"><fmt:message key="${linkType}" /></form:option>
		            </c:forEach> 
					</form:select>
				</div>
			</div>
        </div>
        <div class="line">
			<div class="form-row">
				<div class="col-label">
					<label for="category_linkImage" class="col-form-label">
						<span class="fa fa-question-circle" data-toggle="tooltip" data-placement="top" title="Image::Upload the image of the product. Max Image size is 2MB."></span><fmt:message key="linkImage" />
					</label>
				</div>
	            <div class="col-lg-4">
					<c:if test="${categoryForm.category.hasLinkImage}">
						<label>Image shown here</label>
				    	</c:if>
				    	<c:if test="${not categoryForm.category.hasLinkImage}">
						<input value="browse" type="file" name="link_image" id="category_linkImage"/>
				    	</c:if>
	            </div>
            </div>
        </div>
        <div class="line">
	        <div class="form-row">
				<div class="col-label">
					<label for="category_linkImageOver" class="col-form-label">
						<span class="fa fa-question-circle" data-toggle="tooltip" data-placement="top" title="Image::Upload the image of the product. Max Image size is 2MB."></span><fmt:message key="linkImage" /> (<fmt:message key="rollover" />):
					</label>
				</div>
	            <div class="col-lg4"> 
	                <c:if test="${categoryForm.category.hasLinkImageOver}">
						<label>Image shown here</label>
				    	</c:if>
				    	<c:if test="${not categoryForm.category.hasLinkImageOver}">
						<input value="browse" type="file" id="category_linkImageOver" name="link_image_over"/>
				    	</c:if>
	            </div>
	        </div>
        </div>
        <div class="line">
			<div class="form-row">
				<div class="col-label">
					<label for="category_thumbnailViewImage" class="col-form-label">
						<span class="fa fa-question-circle" data-toggle="tooltip" data-placement="top" title="Image::Upload the image of the product. Max Image size is 2MB."></span><fmt:message key="categoryBuilderImage" />
					</label>
				</div>
				<div class="col-lg-4"> 
					<c:if test="${categoryForm.category.hasCategoryBuilderImage}">
					    <label>Image shown here</label>
				    	</c:if>
				    	<c:if test="${not categoryForm.category.hasCategoryBuilderImage}">
						<input value="browse" type="file" name="category_builder_image" id="category_builder_image"/>
				    	</c:if>   
	            </div>
            </div>
        </div>
        <div class="line">
	        <div class="form-row">
				<div class="col-label">
					<label for="category_thumbnailViewImage" class="col-form-label text-sm-right">
						<span class="fa fa-question-circle" data-toggle="tooltip" data-placement="top" title="Image::Upload the image of the product. Max Image size is 2MB."></span><fmt:message key="thumbnailViewImage" />:
					</label>
	            </div>
	            <div class="col-lg-4"> 
				    <c:if test="${categoryForm.category.hasImage or (not empty (categoryForm.category.imageUrl) and categoryForm.category.imageUrl != null)}">
				    		<label>Image shown here</label>
				    	</c:if>
				    	<c:if test="${(not (categoryForm.category.hasImage)) and (empty (categoryForm.category.imageUrl))}">
						<input value="browse" id="category_thumbnailViewImage" type="file" name="_image"/>
					</c:if>	
	        		</div>
	        </div>
        </div>
        <div class="line">
	        <div class="form-row">
				<div class="col-label">
		            <label for="category_imageUrlAltName" class="col-form-label">
						<span class="fa fa-question-circle" data-toggle="tooltip" data-placement="top" title="Image Name::The Maximum character is 255."></span><fmt:message key="thumbnailViewImage" /> <fmt:message key="name" />
					</label>
				</div>
	            <div class="col-lg-4">
					<div class="form-group">
						<form:input path="category.imageUrlAltName" sid="category_imageUrlAltName" class="form-control form-control-sm"/>
	                </div>
	            </div>
	        </div>
	        <div class="form-row">
				<div class="col-label"></div>
				<div class="col-lg-4">
					<div class="form-group">
	                    <label class="custom-control custom-checkbox">
	                        <input type="checkbox" class="custom-control-input" <c:if test="${categoryForm.category.hasImage || (not empty (categoryForm.category.imageUrl))}">disabled</c:if> <c:if test="${not empty (categoryForm.category.imageUrl) and categoryForm.category.imageUrl != null}">checked="checked" </c:if> name="_imageWithName" />
	                        <span class="custom-control-indicator"></span>
	                        <span class="custom-control-description"><fmt:message key="imageWithName"/></span>
						</label>
	                </div>
                </div>
	        </div>
	        <div class="form-row">
				<div class="col-label"></div>
				<div class="col-lg-4">
					<small class="help-text">
	                    To change the old CAT_ID image name to more SEO friendly name, please delete the image if existing and upload it with the checkbox selected.
	                </small>
                </div>
	        </div>
        </div>
        <div class="line">
	        <div class="form-row">
				<div class="col-label">
		            <label for="category_detailViewImage" class="col-form-label">
		            		<span class="fa fa-question-circle" data-toggle="tooltip" data-placement="top" title="Image::Upload the image of the product. Max Image size is 2MB."></span><fmt:message key="detailViewImage" />
		            </label>
	            </div>
				<div class="col-lg-4"> 
					 <c:if test="${imageBigExist}">
						<label>Image shown here</label>
				    	</c:if>
				    	<c:if test="${not  imageBigExist}">
						<input value="browse" type="file" name="_image_big" id="category_detailViewImage"/>
					</c:if>
	            </div>
	       </div>
       </div>
    </sec:authorize>
</div>