<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="/WEB-INF/tlds/spring.tld" prefix="spring" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ taglib uri="http://www.springframework.org/security/tags" prefix="sec" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>

<c:forEach items="${languageCodes}" var="i18n" varStatus="loop">
	<div id="tab_lan_${loop.index }" class="tab-pane">
		<div class="line">
			<div class="form-row">
				<div class="col-label">
					<label for="__i18nName_${i18n.languageCode}" class="col-form-label">
						<span class="fa fa-question-circle" data-toggle="tooltip" data-placement="top" title="Field 1:: The Maximum character is 512"></span><fmt:message key="categoryName" />
		   			</label>
	   			</div>
				<div class="col-lg-4">
					<input type="text" name="__i18nName_${i18n.languageCode}" id="__i18nName_${i18n.languageCode}" class="form-control form-control-sm" value="<c:out value="${categoryForm.i18nCategory[i18n.languageCode].i18nName}"/>" maxlength="150">
				</div>
	        </div>
        </div>
        	<div class="line">
	        <div class="form-row">
				<div class="col-label">
					<label class="col-form-label">
						<fmt:message key="headTag" />
		   			</label>
	   			</div>
				<div class="col-lg-8">
					<div class="form-group">
						<textarea class="code-editor" name="__i18nHeadTag_${i18n.languageCode}" id="__i18nHeadTag_${i18n.languageCode}"><c:out value="${categoryForm.i18nCategory[i18n.languageCode].i18nHeadTag}"/></textarea>
					</div>					
	                <div>
	                     <a href="#" class="float-right" data-toggle="collapse" data-target="#i18nHeadTagMobile_group">
							<fmt:message key="showMobileCode" />
						</a>
	                </div>
				</div>
	        </div>
        </div>
        <div class="line collapse"  id="i18nHeadTagMobile_group">
	        <div class="form-row">
				<div class="col-label">
		        		<label class="col-form-label"> 
		      			<fmt:message key="headTag" /> <fmt:message key="mobile" />
		      		</label>
	      		</div>
	       		<div class="col-lg-8">
	           		<div class="form-group">
	                		<textarea class="code-editor" name="__i18nHeadTagMobile_${i18n.languageCode}" id="__i18nHeadTagMobile_${i18n.languageCode}"><c:out value="${categoryForm.i18nCategory[i18n.languageCode].i18nHeadTagMobile}"/></textarea>
	           		</div>        		
	       		</div>
	   		</div>
   		</div>
   		<div class="line">
	   		<div class="form-row">
	   			<div class="col-label">
					<label class="col-form-label">
						<fmt:message key="categoryHtml" />
		   			</label>
	   			</div>
				<div class="col-lg-8">
					<div class="form-group">
	                		<textarea class="code-editor" name="__i18nHtmlCode_${i18n.languageCode}" id="__i18nHtmlCode_${i18n.languageCode}"><c:out value="${categoryForm.i18nCategory[i18n.languageCode].i18nHtmlCode}"/></textarea>
	                </div>
	                <div>
						<a href="#" class="float-right" data-toggle="collapse" data-target="#i18nHtmlCodeMobile_group">
							<fmt:message key="showMobileCode" />
						</a>
	                </div>
				</div>
	        </div>
        </div>
        <div class="line collapse" id="i18nHtmlCodeMobile_group">
	        <div class="form-row">
				<div class="col-label">
		        		<label class="col-form-label"> 
		      			<fmt:message key="categoryHtml" /> <fmt:message key="mobile" />
		      		</label>
	      		</div>
	       		<div class="col-lg-8">
	           		<div class="form-group">
	           			<textarea class="code-editor" name="__i18nHtmlCodeMobile_${i18n.languageCode}" id="__i18nHtmlCodeMobile_${i18n.languageCode}"><c:out value="${categoryForm.i18nCategory[i18n.languageCode].i18nHtmlCodeMobile}"/></textarea>
	           		</div>        		
	       		</div>
	   		</div>
   		</div>
   		<c:if test="${gSiteConfig['gPROTECTED'] > 0}">
			<div class="line" <c:if test="${categoryForm.category.protectedLevel == '0'}">style="display:none"</c:if>>
	 			<div class="form-row">
	 				<div class="col-label">
			            <label class="col-form-label">
			            		<fmt:message key="protectedHtmlCode" />
			            	</label>
		            	</div>
		            <div class="col-lg-8">
		       			<div class="form-group">
		       				<textarea class="code-editor" name="__i18nProtectedHtmlCode_${i18n.languageCode}" id="__i18nProtectedHtmlCode_${i18n.languageCode}"><c:out value="${categoryForm.i18nCategory[i18n.languageCode].i18nProtectedHtmlCode}"/></textarea>
		           		</div>
			        </div>
	        		</div>
        		</div>
 		</c:if>
	</div>
</c:forEach>