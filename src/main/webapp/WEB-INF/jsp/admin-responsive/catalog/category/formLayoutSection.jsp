<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="/WEB-INF/tlds/spring.tld" prefix="spring" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ taglib uri="http://www.springframework.org/security/tags" prefix="sec" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<script type="text/javascript">
function showMobileCode(id){
	document.getElementById(id).disabled=false;
    document.getElementById(id).style.display="block"; 
}
</script>

<div id="tab_2" class="tab-pane" role="tabpanel"> 
	<sec:authorize ifAnyGranted="ROLE_AEM,ROLE_ADMIN_${prop_site_id},ROLE_CATEGORY_EDIT,ROLE_CATEGORY_CREATE">
        <div class="line">
			<div class="form-row">
				<div class="col-label">
		            <label for="category_productPerPage" class="col-form-label">
						<fmt:message key="productPerPage" />
		            </label>
	            </div>
             	<div class="col-lg-4">
             		<div class="form-group">
					  	<form:select path="category.productPerPage" id="category_productPerPage" class="custom-select">
					        <form:option value=""><fmt:message key="global"/></form:option>
					        <c:forEach begin="1" end="50" var="products">
					            <form:option value="${products}">${products}</form:option>
					        </c:forEach>      
					    </form:select>
				    </div>
      			</div>
            </div>
        </div>
        <div class="line">
			<div class="form-row">
				<div class="col-label">
		            <label for="category_layoutId" class="col-form-label">
						<span class="fa fa-question-circle" data-toggle="tooltip" data-placement="top" title="<fmt:message key="layout" />::For Parent, if category doesn't have a parent then layout will be the default layout"></span><fmt:message key="layout" />
					</label>
             	</div>
				<div class="col-lg-4">
					<div class="form-group">
						<form:select path="category.layoutId" id="category_layoutId" class="custom-select">
							<form:option value="0"><fmt:message key="parent"/></form:option>
							<form:option value="1"><fmt:message key="default"/></form:option>
							<c:forEach items="${layoutList}" var="option">
								<c:if test="${option.id > 1}">
									<form:option value="${option.id}">${option.id} - <c:out value="${option.name}"/></form:option>
								</c:if>
							</c:forEach>      
						</form:select>
					</div>
				</div>
            </div>
        </div>
        <div class="line">
			<div class="form-row">
				<div class="col-label">
		            <label for="category_subcatLevels" class="col-form-label">
						<fmt:message key="subcatLevels" />
					</label>
				</div>
				<div class="col-lg-4">
	                <div class="form-group">
				   		<form:select path="category.subcatLevels" id="category_subcatLevels" class="custom-select">
				        		<form:option value=""><fmt:message key="global"/></form:option>
				        		<c:forEach begin="0" end="2" var="subcatLevels">
				        			<form:option value="${subcatLevels}">${subcatLevels}</form:option>
				        		</c:forEach>
			      		</form:select>
		      		</div>
	      		</div>
            </div>
        </div>
        <div class="line">
			<div class="form-row">
				<div class="col-label">
		            <label for="category_subcatCols" class="col-form-label">
						<span class="fa fa-question-circle" data-toggle="tooltip" data-placement="top" title="NOTE::Applies if Sub Category Display is Row or Column"></span><fmt:message key="subcatCols" />
					</label>
				</div>
                <div class="col-lg-4">
					<div class="form-group">
		       			<form:select path="category.subcatCols" id="category_subcatCols" class="custom-select">
		        				<c:forEach begin="1" end="5" var="subcatCols">
			        				<form:option value="${subcatCols}">${subcatCols}</form:option>
			        			</c:forEach> 
			       		</form:select>
		       		</div>
	       		</div>
            </div>
        </div>        
        <div class="line">
			<div class="form-row">
				<div class="col-label">
		            <label for="category_subcatLocation" class="col-form-label">
						<fmt:message key="subcat" /> <fmt:message key="location" />
					</label>
				</div>
				<div class="col-lg-4">
					<div class="form-group">
						<form:select path="category.subcatLocation" id="category_subcatLocation" class="custom-select">
				        		<form:option value=""><fmt:message key="below" /> <fmt:message key="categoryHtml" /></form:option>
				        		<form:option value="1"><fmt:message key="above" /> <fmt:message key="categoryHtml" /></form:option>
			       		</form:select>
					</div>
				</div>
            </div>
        </div>
        <div class="line">
	        <div class="form-row">
				<div class="col-label">
		            <label for="category_displayMode" class="col-form-label">
						<fmt:message key="displayMode" />
					</label>
				</div>
                <div class="col-lg-4">
					<div class="form-group">
					    <form:select path="category.displayMode" id="category_displayMode" class="custom-select">
				            <form:option value=""><fmt:message key="global"/></form:option>
					        <c:choose>
					            <c:when test="${(siteConfig['TEMPLATE'].value == 'template6') or (siteConfig['TEMPLATE'].value == 'template7')}">
					              <form:option value="22">22. Slider Layout</form:option>
						          <form:option value="28-R">28. ADI-R</form:option>
					          	  <form:option value="29-R">29. Compact-R</form:option>
					          	  <form:option value="32">32. Quickmode Responsive</form:option>
					          	  <form:option value="34">34. Rockofftrade</form:option>
					          	  <form:option value="35">35. Achamaal</form:option>
					          	  <form:option value="36">36. PaperEnterprise</form:option>
					          	  <form:option value="37">37. StrategicMerch</form:option>
					          	  <form:option value="38">38. BestHandBag</form:option>
					          	  <form:option value="39">39. Pom</form:option>
					          	  <form:option value="40">40. Posh</form:option>
					          	  <form:option value="42">42. Mihair1</form:option>
					          	  <form:option value="43">43. Mihair2</form:option>
					          	  <form:option value="44">44. Mihair3</form:option>
					          	  <form:option value="45">45. ADI Grid</form:option>
					          	  <form:option value="46">46. ADI List</form:option>
					          	  <form:option value="47">47. ADI Gallery</form:option>
					          	  <form:option value="48">48. Wexpress</form:option>
					          	  <form:option value="49">49. Webrepsnew</form:option>
					          	  <form:option value="50">50. NewThumb</form:option> 
					          	  <form:option value="51">51. Carlex</form:option>
					            </c:when>
					            <c:otherwise>
					              <form:option value="1">1. <fmt:message key="default" /></form:option>
						          <form:option value="2">2. <fmt:message key="default" /> 2</form:option>
						          <form:option value="3">3. compact up</form:option>
						          <form:option value="4">4. compact short Desc.</form:option>
						          <form:option value="5">5. compact down</form:option>
						          <form:option value="6">6. compact popup</form:option>
						          <form:option value="7">7. compact</form:option>
						          <form:option value="7-1">7-1. compact</form:option>
						          <form:option value="8">8. compact one add to cart</form:option>
						          <form:option value="8-1">8-1. compact one add to cart</form:option>
						          <form:option value="9">9. compact first price</form:option>
						          <form:option value="10">10. divs</form:option>
						          <form:option value="11">11. price break</form:option>
						          <form:option value="12">12. compact one add to cart (MSRP)</form:option>
						          <c:if test="${siteConfig['PRODUCT_QUICK_VIEW'].value == 'true'}">
						          <form:option value="13">13. compact down new</form:option>
						          <form:option value="14">14. compact down new - 14</form:option>
						          <c:if test="${gSiteConfig['gSITE_DOMAIN'] == 'viatrading.com' or gSiteConfig['gSITE_DOMAIN'] == 'test.com'}">
						          <form:option value="15">15. compact down new - 15</form:option>
						          </c:if>
						          <c:if test="${gSiteConfig['gSITE_DOMAIN'] == 'myevergreen.com'}">
						          <form:option value="16">16. compact down new - 16</form:option>
						          </c:if>
						          </c:if>
						          <form:option value="17">17. compact short Desc. One add to cart</form:option>
						          <c:if test="${siteConfig['PRODUCT_QUICK_VIEW'].value == 'true'}">
						          <form:option value="18">18. compact short Desc.</form:option>
						          </c:if>
						          <form:option value="19">19. One product per row with LongDesc</form:option>
						          <c:if test="${gSiteConfig['gSITE_DOMAIN'] == 'bnoticed.com'}">
						          <form:option value="20">20. Bnoticed Search Display</form:option>
						          </c:if>
						          <form:option value="21">21. Responsive Layout</form:option>
						          <form:option value="quick"><fmt:message key="quickMode"/></form:option>
						          <c:if test="${siteConfig['PRODUCT_QUICK_MODE_2'].value == 'true'}">
						          	<form:option value="quick2"><fmt:message key="quickMode2"/></form:option>
						          	<form:option value="quick2-1"><fmt:message key="quickMode2"/>-1</form:option>
						          	<form:option value="quick2-2"><fmt:message key="quickMode2"/>-2</form:option>
						          </c:if>
						          <form:option value="quick3"><fmt:message key="quickMode"/>3</form:option>
						          <form:option value="22">22. Slider Layout</form:option>
						          <form:option value="23">23. Fluid Layout</form:option>
						          <form:option value="24">24. Holden Layout</form:option>
						          <form:option value="25">25. Packnwood Layout</form:option>
						          <form:option value="26">26. MSConcepts Layout</form:option>
						          <form:option value="27">27. WholesaleGoodz</form:option>
						        </c:otherwise>
					        </c:choose>
				       </form:select>
	      		   </div>
      		   </div>
            </div>
        </div>
		<div class="line">
			<div class="form-row">
				<div class="col-label">
		            <label for="category_displayModeGrid" class="col-form-label">
						<fmt:message key="displayModeGrid" />
		            </label>
	            </div>
                <div class="col-lg-4">
					<div class="form-group">
					    <form:select path="category.displayModeGrid" id="category_displayModeGrid" class="custom-select">
							<form:option value=""><fmt:message key="global"/></form:option>
							<form:option value="1">1. <fmt:message key="default" /></form:option>
							<form:option value="2">2. <fmt:message key="default" /> 2</form:option>
							<form:option value="3">3. compact up</form:option>
							<form:option value="4">4. compact short Desc.</form:option>
							<form:option value="5">5. compact down</form:option>
							<form:option value="6">6. compact popup</form:option>
							<form:option value="7">7. compact</form:option>
							<form:option value="7-1">7-1. compact</form:option>
							<form:option value="8">8. compact one add to cart</form:option>
							<form:option value="8-1">8-1. compact one add to cart</form:option>
							<form:option value="9">9. compact first price</form:option>
							<form:option value="10">10. divs</form:option>
							<form:option value="11">11. price break</form:option>
							<form:option value="12">12. compact one add to cart (MSRP)</form:option>
							<c:if test="${siteConfig['PRODUCT_QUICK_VIEW'].value == 'true'}">
								<form:option value="13">13. compact down new</form:option>
				          		<form:option value="14">14. compact down new - 14</form:option>
					          	<c:if test="${gSiteConfig['gSITE_DOMAIN'] == 'viatrading.com' or gSiteConfig['gSITE_DOMAIN'] == 'test.com'}">
					          		<form:option value="15">15. compact down new - 15</form:option>
					          	</c:if>
					          	<c:if test="${gSiteConfig['gSITE_DOMAIN'] == 'myevergreen.com'}">
					          		<form:option value="16">16. compact down new - 16</form:option>
					          	</c:if>
				          	</c:if>
				          	<form:option value="17">17. compact short Desc. One add to cart</form:option>
				          	<c:if test="${siteConfig['PRODUCT_QUICK_VIEW'].value == 'true'}">
				          		<form:option value="18">18. compact short Desc.</form:option>
				          	</c:if>
				          	<form:option value="19">19. One product per row with LongDesc</form:option>
				          	<c:if test="${gSiteConfig['gSITE_DOMAIN'] == 'bnoticed.com'}">
				          		<form:option value="20">20. Bnoticed Search Display</form:option>
				          	</c:if>
				          	<form:option value="quick"><fmt:message key="quickMode"/></form:option>
				          	<c:if test="${siteConfig['PRODUCT_QUICK_MODE_2'].value == 'true'}">
				          		<form:option value="quick2"><fmt:message key="quickMode2"/></form:option>
					          	<form:option value="quick2-1"><fmt:message key="quickMode2"/>-1</form:option>
					          	<form:option value="quick2-2"><fmt:message key="quickMode2"/>-2</form:option>
				          	</c:if>
				          	<form:option value="quick3"><fmt:message key="quickMode"/>3</form:option>
				          	<form:option value="23">23. Fluid Layout</form:option>
				          	<form:option value="26">26. MSConcepts Layout</form:option>
				          	<form:option value="27">27. WholesaleGoodz</form:option>
				          	<form:option value="32">32. Quickmode Responsive</form:option>
				          	<form:option value="34">34. Rockofftrade</form:option>
				        </form:select>
			        </div>
      			</div>
            </div>
        </div>
        <div class="line">
			<div class="form-row">
				<div class="col-label">
		            <label for="category_leftbarType" class="col-form-label">
						<fmt:message key="LeftBar" />
					</label>
				</div>
				<div class="col-lg-4">
	            		<div class="form-group">
					  	<form:select path="category.leftbarType" id="category_leftbarType" class="custom-select">
				  			<form:option value=""><fmt:message key="global"/></form:option>
					        <c:forTokens items="${siteConfig['LEFTBAR_TYPE'].value}" delims="," var="lType" varStatus="status">
					  	        <form:option value="${lType}"><fmt:message key="${lType}"/></form:option>
					  	    </c:forTokens>
				  	    </form:select>
			  	    </div>
			  	    <div class="form-group">
	                    <label class="custom-control custom-checkbox">
	                        <form:checkbox path="category.hideLeftBar" class="custom-control-input" />
	                        <span class="custom-control-indicator"></span>
	                        <span class="custom-control-description"><fmt:message key="hide" /></span>
	                    </label>
	                    <label class="custom-control custom-checkbox">
	                        <form:checkbox path="category.showSubcats" class="custom-control-input" />  
	                        <span class="custom-control-indicator"></span>
	                        <span class="custom-control-description"><fmt:message key="showSubcats" /></span>
	                    </label>
                    </div>
		  	    </div>
	  	    </div>
        </div>
        <div class="line">
			<div class="form-row">
				<div class="col-label">
		            <label class="col-form-label">
						<span class="fa fa-question-circle" data-toggle="tooltip" data-placement="top" title="NOTE::Applies if want to use full width"></span><fmt:message key="showFullWidth" />
					</label>
				</div>
				<div class="col-lg-4">
			  	    <div class="form-group">
		            		<label class="custom-control custom-radio">
							<form:radiobutton path="category.showFullWidth" class="custom-control-input" id="category_showFullWidth1" value="true" />
		                    	<span class="custom-control-indicator"></span>
		                    	<span class="custom-control-description"><fmt:message key="yes" /></span>
						</label>
						<label class="custom-control custom-radio">
		                		<form:radiobutton path="category.showFullWidth" class="custom-control-input" id="category_showFullWidth2" value="false" />
							<span class="custom-control-indicator"></span>
							<span class="custom-control-description"><fmt:message key="no" /></span>
						</label>
					</div>
				</div>
            </div>
        </div>
        <div class="line">
	        <div class="form-row">
		        <div class="col-label">
		            <label class="col-form-label">
						<span class="fa fa-question-circle" data-toggle="tooltip" data-placement="top" title="NOTE::Applies if want to hide header"></span><fmt:message key="Header" />
					</label>
				</div>
				<div class="col-lg-4">
					<div class="form-group">
		            		<label class="custom-control custom-radio">
				 			<form:radiobutton path="category.hideHeader" id="category_hideHeader1" class="custom-control-input" value="true" /> 
		                    	<span class="custom-control-indicator"></span>
		                    	<span class="custom-control-description"><fmt:message key="yes" /></span>
						</label>
						<label class="custom-control custom-radio">
		                		<form:radiobutton path="category.hideHeader" id="category_hideHeader2" class="custom-control-input" value="false" />
							<span class="custom-control-indicator"></span>
							<span class="custom-control-description"><fmt:message key="no" /></span>
						</label>	
					</div>
	            </div>
            </div>
        </div>
        <div class="line">
	        <div class="form-row">
				<div class="col-label">
		            <label class="col-form-label">
						<span class="fa fa-question-circle" data-toggle="tooltip" data-placement="top" title="NOTE::Applies if want to hide TopBar"></span><fmt:message key="TopBar" />
					</label>
				</div>
				<div class="col-lg-4">
					<div class="form-group">
					    <label class="custom-control custom-radio">
				 			<form:radiobutton path="category.hideTopBar" id="category_hideTopBar1" class="custom-control-input" value="true" /> 
		                    	<span class="custom-control-indicator"></span>
		                    	<span class="custom-control-description"><fmt:message key="yes" /></span>
						</label>
						<label class="custom-control custom-radio">
		                		<form:radiobutton path="category.hideTopBar" id="category_hideTopBar2" class="custom-control-input" value="false" />
							<span class="custom-control-indicator"></span>
							<span class="custom-control-description"><fmt:message key="no" /></span>
						</label>
					</div>
				</div>
            </div>
        </div>
        <div class="line">
            <div class="form-row">
	            <div class="col-label">
					<label class="col-form-label">
						<span class="fa fa-question-circle" data-toggle="tooltip" data-placement="top" title="NOTE::Applies if want to hide RightBar"></span><fmt:message key="RightBar" />
					</label>
				</div>
				<div class="col-lg-4">
					<div class="form-group">
				 		<label class="custom-control custom-radio">
				 			<form:radiobutton path="category.hideRightBar" id="category_hideRightBar1" class="custom-control-input" value="true" /> 
		                    	<span class="custom-control-indicator"></span>
		                    	<span class="custom-control-description"><fmt:message key="yes" /></span>
						</label>
						<label class="custom-control custom-radio">
		                		<form:radiobutton path="category.hideRightBar" id="category_hideRightBar2" class="custom-control-input" value="false" />
							<span class="custom-control-indicator"></span>
							<span class="custom-control-description"><fmt:message key="no" /></span>
						</label>	
					</div>
				</div>
            </div>
        </div>
        <div class="line">
			<div class="form-row">
				<div class="col-label">
		            <label class="col-form-label"> 
						<span class="fa fa-question-circle" data-toggle="tooltip" data-placement="top" title="NOTE::Applies if want to hide Footer"></span><fmt:message key="Footer" />
					</label>
				</div>
	            <div class="col-lg-4">
		            <div class="form-group">
					    <label class="custom-control custom-radio">
				 			<form:radiobutton path="category.hideFooter" id="category_hideFooter1" class="custom-control-input" value="true" /> 
		                    	<span class="custom-control-indicator"></span>
		                    	<span class="custom-control-description"><fmt:message key="yes" /></span>
						</label>
						<label class="custom-control custom-radio">
		                		<form:radiobutton path="category.hideFooter" id="category_hideFooter2" class="custom-control-input" value="false" />
							<span class="custom-control-indicator"></span>
							<span class="custom-control-description"><fmt:message key="no" /></span>
						</label>	
					</div>
	            </div>
            </div>
        </div>
        <div class="line">
            <div class="form-row">
				<div class="col-label">
					<label class="col-form-label">
						<span class="fa fa-question-circle" data-toggle="tooltip" data-placement="top" title="NOTE::Applies if want to hide BreadCrumbs"></span><fmt:message key="breadCrumbs" />
					</label>
				</div>
				<div class="col-lg-4">
		            <div class="form-group">
				 		<label class="custom-control custom-radio">
				 			<form:radiobutton path="category.hideBreadCrumbs" id="category_hideBreadCrumbs1" class="custom-control-input" value="true" /> 
		                    	<span class="custom-control-indicator"></span>
		                    	<span class="custom-control-description"><fmt:message key="yes" /></span>
						</label>
						<label class="custom-control custom-radio">
		                		<form:radiobutton path="category.hideBreadCrumbs" id="category_hideBreadCrumbs2" class="custom-control-input" value="false" />
							<span class="custom-control-indicator"></span>
							<span class="custom-control-description"><fmt:message key="no" /></span>
						</label>	
					</div>
				</div>
            </div>
		</div>
	</sec:authorize>
	<sec:authorize ifAnyGranted="ROLE_AEM,ROLE_ADMIN_${prop_site_id},ROLE_CATEGORY_EDIT,ROLE_CATEGORY_CREATE,ROLE_CATEGORY_UPDATE_HEAD_TAG">
		<div class="line">
			<div class="form-row">
				<div class="col-label">
		            <label for="category_headTag" class="col-form-label">
						<fmt:message key="headTag" />
					</label>
				</div>
	            <div class="col-lg-4">
					<div class="form-group">
					    <form:textarea class="form-control" path="category.headTag" id="category_headTag" rows="6"/>
	                </div>
	            </div>
            </div>
        </div>
	</sec:authorize> 
</div>