<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ taglib uri="http://www.springframework.org/security/tags" prefix="sec" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ taglib uri="/WEB-INF/tlds/spring.tld" prefix="spring" %>
<%@ taglib uri="/WEB-INF/tlds/custom.tld" prefix="wj" %>

<tiles:insertDefinition name="admin.responsive-template.catalog.category.form" flush="true">
	<tiles:putAttribute name="css">
	</tiles:putAttribute>
	<tiles:putAttribute name="content" type="string">
		<form:form commandName="categoryForm" method="post" enctype="multipart/form-data" class="page-form">
			<div class="page-banner">
				<div class="breadcrumb">
					<span><a href="${_contextpath}/admin/catalog/dashboard.jhtm">Home</a></span>
					<i class="sli-arrow-right"></i><span><a href="${_contextpath}/admin/catalog/categoryList.jhtm">Categories</a></span>
					<i class="sli-arrow-right"></i><span>${categoryForm.category.name}</span>
				</div>
			</div>
			<div class="page-head">
				<div class="row justify-content-between">
					<div class="col-sm-8 title">
						${categoryForm.category.name}
					</div>
					<div class="col-sm-4 actions">
						<div class="dropdown dropdown-actions">
							<button type="button" class="btn-outline dropdown-toggle active btn btn-primary" data-toggle="dropdown" aria-expanded="false">
	                             <i class="sli-settings"></i> Actions
	                         </button>
	                         <div class="dropdown-menu dropdown-menu-right">
								<c:if test="${!categoryForm.newCategory}">
									<sec:authorize ifAnyGranted="ROLE_AEM,ROLE_ADMIN_${prop_site_id},ROLE_CATEGORY_DELETE">
										<c:if test="${categoryForm.category.host == null and categoryForm.category.feed == null}">
											<a class="dropdown-item">Delete</a>
										</c:if>
									</sec:authorize>
								</c:if>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="page-body">
				<sec:authorize ifAnyGranted="ROLE_AEM,ROLE_ADMIN_${prop_site_id},ROLE_CATEGORY_EDIT,ROLE_CATEGORY_CREATE">
	  	 			<c:if test="${!categoryForm.newCategory}">
						<div class="minor-info">
							<span><span><fmt:message key="categoryId" />:</span> <span><c:out value="${categoryForm.category.id}"/></span></span>
							<span><span>Clicks/Views:</span> <span><c:out value="${categoryForm.category.numOfClicks}"/></span></span>
							<span><span><fmt:message key="dateAdded" />:</span> <span><fmt:formatDate type="both" timeZone="${siteConfig['SITE_TIME_ZONE'].value}" value="${categoryForm.category.created}"/></span></span>
							<span><span><fmt:message key="lastModified" />:</span> <span><fmt:formatDate type="both" timeZone="${siteConfig['SITE_TIME_ZONE'].value}" value="${categoryForm.category.lastModified}"/></span></span>
						</div>
					</c:if>
			  	</sec:authorize>
			  	<div class="main-content">
				  	<div class="form-section">
				        <div class="content">
				            <ul class="nav nav-tabs" role="tablist">
				                <li class="nav-item">
				                    <a class="nav-link active" data-toggle="tab" role="tab" href="#tab_1">Category Info</a>
				                </li>
				                <li class="nav-item">
				                    <a class="nav-link" data-toggle="tab" role="tab" href="#tab_2">Layout</a>
								</li>
				                <li class="nav-item">
				                		<div class="dropdown">
					                    <a class="nav-link" data-toggle="dropdown">Advanced <i class="fa fa-caret-down"></i></a>
										<div class="dropdown-menu dropdown-menu-right">
											<a class="dropdown-item" data-toggle="tab" role="tab" href="#tab_3">Options</a>
											<a class="dropdown-item" data-toggle="tab" role="tab" href="#tab_4">Category Link / Image</a>
											<a class="dropdown-item" data-toggle="tab" role="tab" href="#tab_5">SEO</a>
											<a class="dropdown-item" data-toggle="tab" role="tab" href="#tab_6">Message</a>
											<a class="dropdown-item" data-toggle="tab" role="tab" href="#tab_7">Dynamic Fields</a>
											<c:forEach items="${languageCodes}" var="i18n" varStatus="loop"> 
							                    <a class="dropdown-item" data-toggle="tab" role="tab" href="#tab_lan_${loop.index }">
							                    		<img src="${_contextpath}/admin-responsive-static/assets/flags/${i18n.languageCode}.png" class="mb-1" title="<fmt:message key="language_${i18n.languageCode}"/>"> <fmt:message key="language_${i18n.languageCode}"/>
							                    </a>
							                </c:forEach>
											<a class="dropdown-item" data-toggle="tab" role="tab" href="#tab_more_html">More HTML Code</a>
										</div>
									</div>
				                </li>
				            </ul>
			                <div class="tab-content">
			                    <div id="tab_1" class="tab-pane active" role="tabpanel">
									<sec:authorize ifAnyGranted="ROLE_AEM,ROLE_ADMIN_${prop_site_id},ROLE_CATEGORY_EDIT,ROLE_CATEGORY_CREATE">
			                       		<div class="line">
				                       		<div class="form-row">
				                       			<div class="col-label">
													<label for="category_name" class="col-form-label">
					                                		<span class="fa fa-question-circle" data-toggle="tooltip" data-placement="top" title="Name::The Maximum character is 150.For example: Test_Example."></span><fmt:message key="categoryName" />
													</label>
												</div>
												<div class="col-lg-4">
													<div class="form-group">
											  			<form:input path="category.name" id="category_name" maxlength="150" class="form-control"/>
											  		</div>
											  	</div>
									      	</div>
									      	<c:if test="${gSiteConfig['gMOD_REWRITE'] == '1'}">
									      		<div class="form-row">
											      	<div class="col-lg-3"></div>
										      		<div class="col-lg-4">
										      			<small class="help-text">
			                                        			The name will exist on URL so that the special (For instance <code>"/", "%", "@", "&"</code>) characters are not recommended.
			                                    			</small>
		                                    			</div>
	                                    			</div>
									      	</c:if>
										</div>
										<div class="line">
											<div class="form-row">
												<div class="col-label">
				                                		<label for="category_url" class="col-form-label">
				                               	 		<span class="fa fa-question-circle" data-toggle="tooltip" data-placement="top" title="Redirected URL::Url link of the new Category webpage(The maximum character is 255).For example: Http://www.google.com/webhp"></span><fmt:message key="categoryUrl" />
				                                		</label>
		                                			</div>
			                                		<div class="col-lg-4">
			                                			<div class="form-group">
												  		<form:input path="category.url" id="category_url" maxlength="255" class="form-control" placeholder="http://" />
												  	</div>
				                                </div>
				                                <div class="col-lg-3">
					                                <div class="form-group">
														<form:select path="category.urlTarget" id="category_urlTarget" class="custom-select" title="URL Target">
												        		<form:option value="" label="Same window"/>
												          	<form:option value="_blank" label="New window"/>
												        </form:select>
											        </div>
				                                </div>
			                                </div>
			                                <div class="form-row">
												<div class="col-lg-3">
												</div>
												<div class="col-lg-7">
													<small class="help-text">The special (For instance <code>"/", "%", "@", "&"</code> ) characters are not recommended.</small>
												</div>
			                                </div>
			                                <div class="form-row">
												<div class="col-label">
												</div>
												<div class="col-lg-4">
													<div class="form-group">
														<label class="custom-control custom-checkbox">
										      				<form:checkbox path="category.redirect301" class="custom-control-input" />
										      				<span class="custom-control-indicator"></span>
		                                     				<span class="custom-control-description">301 Moved Permanently</span>
										      			</label>
								      				</div>	
												</div>
			                                </div>
			                            </div>
			                            <c:if test="${siteConfig['CATEGORY_INTERSECTION'].value == 'true'}">
			                          		<div class="line">
			                          			<div class="form-row">
			                          				<div class="col-label">
						                                 <label for="category_categoryIds" class="col-form-label">
															<span class="fa fa-question-circle" data-toggle="tooltip" data-placement="top" title="Intersection/Union::Enter category ID of other categories that you want to get the intersection/union with this category. Use comma to seperate them."></span><fmt:message key="categoryIds" />
														</label>
					                                 </div>
			                                			<div class="col-lg-4">
			                                				<div class="form-group">
													    		<form:input path="category.categoryIds" id="category_categoryIds" maxlength="120" class="form-control"/>
													    </div>
													    <small class="help-text">For Example: 10,12</small>
														<div class="form-group">
															<label class="custom-control custom-radio">
																<form:radiobutton value="intersection" class="custom-control-input" path="category.setType" />
																<span class="custom-control-indicator"></span> 
																<span class="custom-control-description">Intersection</span>
															</label>
															<label class="custom-control custom-radio">
																<form:radiobutton value="union" class="custom-control-input" path="category.setType" />
																<span class="custom-control-indicator"></span>
																<span class="custom-control-description">Union</span>
															</label>
														</div>
											        </div>	 
				                                </div>
				                            </div>
										</c:if>
										<div class="line">
											<div class="form-row">
												<div class="col-label">
				                                 	<label for="sortBySelect" class="col-form-label">
				                                 		<fmt:message key="sortBy" />
				                                 	</label>
			                                 	</div>
			                                 	<div class="col-lg-4">
			                                 		<div class="form-group">
													    <form:select path="category.sortBy" id="sortBySelect" onchange="hideDate()" class="custom-select">
													        <form:option value=""><fmt:message key="global"/></form:option>
													        <c:if test="${siteConfig['CATEGORY_INTERSECTION'].value == 'true'}">
														    	 	<form:option value="best_seller"><fmt:message key="bestSeller"/></form:option>
														     	<form:option value="best_seller_sold"><fmt:message key="bestSellerSold"/></form:option>
														     	<form:option value="viewed desc"><fmt:message key="mostViewed"/></form:option>
														     	<form:option value="created desc"><fmt:message key="newArrival"/></form:option>
														     	<form:option value="rand()"><fmt:message key="random"/></form:option>
														     	<form:option value="search_rank"><fmt:message key="searchRank"/></form:option>
													     	</c:if>
													   </form:select>
												   </div>
											   </div>
			                                </div>
			                            </div>
										<c:if test="${gSiteConfig['gPRODUCT_FIELD_SEARCH']  }">
											<div class="line">
												<div class="form-row">
													<div class="col-label">
														<label for="productFieldSearch_id" class="col-form-label">
					                                			<fmt:message key="productFieldSearch" /> <span class="fa fa-question-circle" data-toggle="tooltip" data-placement="top" title="<fmt:message key="productFieldSearch" />::DropDowns or checkbox will showup to filter the products on this category. (If products have Product field.)"></span>
														</label>
													</div>
													<div class="col-lg-4">
														<div class="form-group">
															<label class="custom-control custom-checkbox">
										      					<form:checkbox id="productFieldSearch_id" path="category.productFieldSearch" class="custom-control-input" type="checkbox" onchange="showProductFieldSearchType();"/>
															</label>
														</div>
													</div>
				                                </div>
				                           </div>
				                           <div class="line">
												<div class="form-row">
													<div class="col-label">
														<label for="productFieldSearchType_id" class="col-form-label">
						                                   	<fmt:message key="productFieldSearchType" />
														</label>
				                                 	</div>
				                                	   <div class="col-lg-4">
														<div class="form-group">
															<form:select id="productFieldSearchType_id" path="category.productFieldSearchType" class="custom-select" onchange="showProductFieldSearchPosition();">
																<form:option value="0">Global</form:option>
																<form:option value="1">Dropdown</form:option>	
																<form:option value="2">Checkbox</form:option>	
																<form:option value="3">Custom Form</form:option>	
															</form:select>
													   </div>
												   </div>
				                                </div>
				                            </div>
				                            <div class="line">
												<div class="form-row">
													<div class="col-label">
														<label for="category_productFieldSearchPosition" class="col-form-label">
						                                	   <fmt:message key="productFieldSearchPosition" />
														</label>
													</div>
				                                	   	<div class="col-lg-4">
														<div class="form-group">
															<form:select path="category.productFieldSearchPosition" id="category_productFieldSearchPosition" class="custom-select">
																<form:option value=""/>	
																<form:option value="left">Left</form:option>
																<form:option value="top">Top</form:option>	
															</form:select>
														</div>
											 	   	</div>
				                                </div>
				                            </div>
			                   		  	</c:if>
			                   		  	<c:if test="${gSiteConfig['gSALES_PROMOTIONS']}">
				                            <div class="line">
				                                <div class="form-row">
													<div class="col-label">
					                                		<label for="category_salesTagTitle" class="col-form-label">
															<span class="fa fa-question-circle" data-toggle="tooltip" data-placement="top" title="<fmt:message key="salesTag" />::Products should be associated to this category."></span><fmt:message key="showProductWithSalesTag" />
														</label>
													</div>
			                                    		<div class="col-lg-4">
			                                    			<div class="form-group">
											       			<form:select path="category.salesTagTitle" id="category_salesTagTitle" class="custom-select">
																 <form:option value=""/>
																 <form:option value="all">All</form:option>	
																 <form:options items="${salesTagCodes}" itemValue="title" itemLabel="title"/>	
															</form:select>
														</div>
													</div>
				                                </div>
				                            </div>
			                            </c:if>
			                            <c:if test="${gSiteConfig['gMASTER_SKU']}">
											<div class="line">
												<div class="form-row">
													<div class="col-label">
					                                		<label for="category_parentChildDisplay" class="col-form-label">
															<span class="fa fa-question-circle" data-toggle="tooltip" data-placement="top" title="Parent-Child::Hide/Show children products on this Category"></span><fmt:message key="displayChildren" />
														</label>
													</div>
													<div class="col-lg-4">
				                                			<div class="form-group">
											       			<form:select path="category.parentChildDisplay" id="category_parentChildDisplay" class="custom-select">
																<form:option value="0" label="Global"/>	
																<form:option value="1" label="Show Children"/>	
																<form:option value="2" label="Hide Children"/>	
															</form:select>
														</div>
											 		</div>
				                                </div>
				                            </div>
										</c:if>
										<c:if test="${fn:contains(siteConfig['SITE_URL'].value, 'rainbow')}">
			                      			<div class="line">
				                                <div class="form-row">
					                                <div class="col-label">
						                                <label for="category_keyWords" class="col-form-label">
															<span class="fa fa-question-circle" data-toggle="tooltip" data-placement="top" title="Keywords::The Maximum character is 80"></span><fmt:message key="keywords" />
														</label>
													</div>
				                                		<div class="col-lg-6">
				                                			<div class="form-group">
											   				<form:input path="category.keyWords" id="category_keyWords" class="form-control" maxlength="255"/>
										   				</div>
												    </div>
				                                </div>
				                            </div>
										</c:if>
										<c:if test="${siteConfig['MULTIPLE_CATEGORY_PARENTS'].value == 'true'}">
			                   				<div class="line">
				                                <div class="form-row">
					                                <div class="col-label">
								                        <label for="category_categoryIds" class="col-form-label">
															Multiple Category Association <span class="fa fa-question-circle" data-toggle="tooltip" data-placement="top" title="Category Association::Enter Multiple Parent Category Ids here. Seperate them with comma or press enter."></span>
														</label>        
													</div>
				                                		<div class="col-lg-6">
				                                			<div class="form-group">
															<form:textarea path="category.categoryIds" id="category_categoryIds" rows="8" class="form-control"/>
										                </div>
											            <small class="help-text">
													        Enter Multiple Parent Category Ids here. Separate them with comma or press enter. 
													    </small>
												    </div>
				                                </div>
				                            </div>
										</c:if>
									</sec:authorize>
									<div class="line">
										<div class="form-row">
											<div class="col-label">
				                             	<label class="col-form-label">
				                                		<span class="fa fa-question-circle" data-toggle="tooltip" data-placement="top" title="HTML Code::Enter here your HTML code for category web page.And you can use Html Editor to make it easy."></span><fmt:message key="categoryHtml" />
				                               	</label>
		                                		</div>
			                                	<div class="col-lg-8">
			                                    <div class="form-group">
				                                    <form:textarea rows="8" id="category_htmlCode" class="code-editor" path="category.htmlCode"/>
			                                    </div>
			                                    <div>
			                                        <a class="float-right" data-toggle="collapse" href="#" data-target="#category_htmlCodeMobile_group">
			                                            Mobile HTML Code
			                                        </a>
			                                    </div>
		                                    </div>
	                                    </div>
		                            </div>
		                            <div class="line collapse" id="category_htmlCodeMobile_group">
										<div class="form-row">
											<div class="col-lg-4">
												<label class="col-form-label"> 
				                               		<span class="fa fa-question-circle" data-toggle="tooltip" data-placement="top" title="HTML Code::Enter here your HTML code for category web page.And you can use Html Editor to make it easy."></span><fmt:message key="categoryHtml" /> <fmt:message key="mobile" />
				                               	</label>
			                                </div>
											<div class="col-lg-8">
			                                    <div class="form-group">
			                                         <form:textarea rows="8" id="category_htmlCodeMobile" class="code-editor" path="category.htmlCodeMobile"/>
			                                    </div>
		                                    </div>
		                                </div>
									</div>
									<c:if test="${gSiteConfig['gPROTECTED'] > 0}">
		                           		<div class="line" id="protectedHtmlCode" <c:if test="${categoryForm.category.protectedLevel == '0'}">style="display:none"</c:if>>
											<div class="form-row">
												<div class="col-lg-4">
													<label class="col-form-label"><fmt:message key="protectedHtmlCode" /></label>
												</div>
				                                	<div class="col-8">
				                                    <div class="form-group"> 
				                                        	<form:textarea path="category.protectedHtmlCode" class="code-editor" id="category_protectedhtmlCode" rows="8"/>
				                                    </div>
				                                </div>
			                                </div>
			                            </div>
									</c:if>
		                            <div class="line">
										<div class="form-row">
											<div class="col-label">
												<label class="col-form-label">
													<span class="fa fa-question-circle" data-toggle="tooltip" data-placement="top" title="Footer::Enter the HTML Code for custom footer."></span><fmt:message key="Footer" /> <fmt:message key="categoryHtml" />
												</label>
											</div>
											<div class="col-lg-8">
			                                    <div class="form-group">
									      	  		<form:textarea rows="8" class="code-editor" id="category_footerHtmlCode" path="category.footerHtmlCode"/>
			                                    </div>
			                                    <div>
			                                        <a href="#" class="float-right" data-toggle="collapse" data-target="#category_footerHtmlMobile_group">
														Mobile HTML Code
			                                        </a>
			                                    </div>
											</div>
										</div>
	                                </div>
	                                <div class="line collapse" id="category_footerHtmlMobile_group">
										<div class="form-row">
											<div class="col-lg-4">
												<label class="col-form-label">
													<span class="fa fa-question-circle" data-toggle="tooltip" data-placement="top" title="Footer::Enter the HTML Code for custom footer."></span><fmt:message key="categoryHtml" /> <fmt:message key="mobile" />
												</label>
											</div>
											<div class="col-lg-8">
			                                    	<div class="form-group">
										      	  	<form:textarea rows="8" class="code-editor" id="category_footerHtmlMobile" path="category.footerHtmlCodeMobile"/>
			                                    	</div>
			                                </div>
		                                </div>
	                                </div>
								</div> 
			                    <!-- End Tab1 -->
			                    
			                    <c:import url="/WEB-INF/jsp/admin-responsive/catalog/category/formLayoutSection.jsp" />
			                     
			                    <div id="tab_3" class="tab-pane">
		                         	<sec:authorize ifAnyGranted="ROLE_AEM,ROLE_ADMIN_${prop_site_id},ROLE_CATEGORY_EDIT">
		                   				<div class="line">
			                   				<div class="form-row">
			                   					<div class="col-label">
													<label for="category_homePage" class="col-form-label">
														<fmt:message key="homePage" />
													</label>
												</div>
												<div class="col-lg-4"> 
				                                    <c:choose>
													  	<c:when test="${categoryForm.category.host == null}">
												            <form:select path="category.homePage" id="category_homePage" class="custom-select">
												                <form:option value="false"><fmt:message key="no"/></form:option>
												                <form:option value="true"><fmt:message key="yes"/></form:option>
											                </form:select>
											          	</c:when>
											          	<c:otherwise><div><label> <c:out value="${categoryForm.category.host}"/></label></div></c:otherwise>
										          	</c:choose>
												</div>
											</div>
										</div>
			                            
										<c:if test="${gSiteConfig['gPROTECTED'] > 0}"> 
			                   				<div class="line">
			                   					<div class="form-row">
			                   						<div class="col-label">
														<label for="category_protectedLevel" class="col-form-label">
															<fmt:message key="protectedLevel" />
														</label>
													</div>
													<div class="col-lg-4"> 
											            <form:select path="category.protectedLevel" id="category_protectedLevel" class="custom-select" onchange="toggleProtectedHtmlCode(this)">
											                <form:option value="0"><fmt:message key="none" /></form:option>      
											                <c:forEach begin="1" end="${gSiteConfig['gPROTECTED']}" var="protect">
													    	        <c:set var="key" value="protected${protect}"/>
													    			<c:choose>
													      	  		<c:when test="${labels[key] != null and labels[key] != ''}">
													        				<c:set var="label" value="${labels[key]}"/>
													      	  		</c:when>
													      	  		<c:otherwise><c:set var="label" value="${protect}"/></c:otherwise>
													    			</c:choose>		        
												        			<form:option value="${protectedLevels[protect-1]}"><c:out value="${label}"/></form:option>
													      	</c:forEach>
													    	</form:select>   
													</div>
												</div>
				                            </div>
			                            </c:if>
			                            
			                            <div class="line">
											<div class="form-row">
												<div class="col-label">
													<label for="category_showOnSearch" class="col-form-label">
														<span class="fa fa-question-circle" data-toggle="tooltip" data-placement="top" title="NOTE::Hide this category on search page."></span><fmt:message key="showOnSearch" />
													</label>
												</div>
												<div class="col-lg-8">
													<label class="custom-control custom-checkbox">
														<form:checkbox path="category.showOnSearch" id="category_showOnSearch" class="custom-control-input" />
														<span class="custom-control-indicator"></span>
													</label>
												</div>
			                                </div>
			                            </div>
			                            
		                             	<c:if test="${gSiteConfig['gASI'] != '' or siteConfig['INGRAM'].value != '' or gSiteConfig['gWEBJAGUAR_DATA_FEED'] > 0}">
		                             		<div class="line"> 
												<div class="form-row">
													<div class="col-label">
														<label for="category_feedFreeze" class="col-form-label">
															<span class="fa fa-question-circle" data-toggle="tooltip" data-placement="top" title="Freeze From Feed::Check this flag to freeze this category for future Feed update."></span><fmt:message key="feedFreeze" />
														</label>
													</div>
					                                <div class="col-lg-4">
					                                    <div class="checkbox">
					                                        <label class="custom-control custom-checkbox">
														       <form:checkbox path="category.feedFreeze" id="category_feedFreeze" class="custom-control-input" />
														       <span class="custom-control-indicator"></span>
														   </label>
					                                    </div>
					                                </div>
												</div>
											</div>
			                            </c:if>
										<div class="line">
											<div class="form-row">
												<div class="col-label">
													<label for="category_categoryGroupId" class="col-form-label">
														<span class="fa fa-question-circle" data-toggle="tooltip" data-placement="top" title="Gallery Group:: Select Group"></span><fmt:message key="galleryGroup" />
													</label>
												</div>
				                                <div class="col-lg-4"> 
				                                     <form:select path="category.categoryGroupId" id="category_categoryGroupId"  class="custom-select">  
														<form:option value="">Select Group</form:option>        
														<c:forEach items="${customerGroupList}" var="customerGroup">
															<form:option value="${customerGroup.id}" ><c:out value="${customerGroup.name}"/></form:option>
														</c:forEach>	
										            </form:select>
				                                </div>
				                            </div>
			                            </div>
		                            </sec:authorize>
			                    </div>
			                    <!-- End Tab3 -->
			                    
			                    <c:import url="/WEB-INF/jsp/admin-responsive/catalog/category/formImageSection.jsp" />	 
			                    
			                    <div id="tab_5" class="tab-pane">
									<div class="line">
										<div class="form-row">
											<div class="col-label">
												<label for="category_siteMapPriority" class="col-form-label">
													<span class="fa fa-question-circle" data-toggle="tooltip" data-placement="top" title="Site Map Priority::Select category priority for Site Map. Default priority is 0.5"></span><fmt:message key="priority" />
												</label>
											</div>
											<div class="col-lg-4"> 
												<form:select path="category.siteMapPriority" id="category_siteMapPriority" class="custom-select">
												<c:forEach begin="0" end="10" step="1" var="priority">
													<form:option value="${priority/10}" label="${priority/10}"></form:option>
												</c:forEach>
												</form:select>	
											</div>
										</div>
		                            </div>
		                            <div class="line">
										<div class="form-row">
											<div class="col-label">
												<label for="category_field1" class="col-form-label">
													<span class="fa fa-question-circle" data-toggle="tooltip" data-placement="top" title="Field 1:: The Maximum character is 512"></span><fmt:message key="categorField1" />
												</label>
											</div>
											<div class="col-lg-4"> 
												<form:input path="category.field1" id="category_field1" class="form-control" maxlength="255"/>
											</div>
										</div>
		                            </div>
		                            <div class="line">
			               				<div class="form-row">
			               					<div class="col-label">
												<label for="category_field2" class="col-form-label">
													<span class="fa fa-question-circle" data-toggle="tooltip" data-placement="top" title="Field 2:: The Maximum character is 512"></span><fmt:message key="categorField2" />
												</label>
											</div>
			                                <div class="col-lg-4">
										   		<form:input path="category.field2" id="category_field2" class="form-control" maxlength="255"/>
			                                </div>
			                            </div>
									</div>
									<div class="line">
										<div class="form-row">
											<div class="col-label">
												<label for="category_field3" class="col-form-label">
			                                	   		<span class="fa fa-question-circle" data-toggle="tooltip" data-placement="top" title="Field 3::The Maximum character is 512"></span><fmt:message key="categorField3" />
			                                		</label>
		                                		</div>
		                                		<div class="col-lg-4">
										   		<form:input path="category.field3" id="category_field3" class="form-control" maxlength="255" />
											</div>
	                                		</div>
									</div>
									<div class="line">
			               				<div class="form-row">
			               					<div class="col-label">
												<label for="category_field4" class="col-form-label">
					                                	<span class="fa fa-question-circle" data-toggle="tooltip" data-placement="top" title="Field 4::The Maximum character is 512"></span><fmt:message key="categorField4" />
				                                </label>
			                                </div>
			                                <div class="col-lg-4">
									   			<form:input path="category.field4" id="category_field4" class="form-control" maxlength="255"/>
			                                </div>
			                            </div>
									</div>
									<div class="line">
										<div class="form-row">
											<div class="col-label">
												<label for="category_field5" class="col-form-label">
													<span class="fa fa-question-circle" data-toggle="tooltip" data-placement="top" title="Field 5::The Maximum character is 512"></span><fmt:message key="categorField5" />
												</label>
											</div>
											<div class="col-lg-4">
										   		<form:input path="category.field5" id="category_field5" class="form-control" maxlength="255"/>
			                                	</div>
			                            </div>
		                            </div>
			                    </div>
			                    <!-- End Tab5 -->
			                    
			                    <div id="tab_6" class="tab-pane">
									<div class="line">
										<div class="form-row">
											<div class="col-label">
		                                			<label for="category_message1" class="col-form-label">
		                                				<fmt:message key="message1" />
		                                			</label>
		                                		</div>
			                                	<div class="col-lg-4">
										        <form:textarea path="category.message1" id="category_message1" rows="8" class="form-control" />   	
			                                	</div>
										</div>
									</div>
									<div class="line">
										<div class="form-row">
											<div class="col-label">
												<label for="category_message2" class="col-form-label">
													<fmt:message key="message2" />
												</label>
											</div>
			                                	<div class="col-lg-4">
										         <form:textarea path="category.message2" rows="8" id="category_message2" class="form-control" />
			                                	</div>
										</div>
		                            </div>
			                    </div>
			                    <!-- End Tab6 -->
			                    
			                    <div id="tab_7" class="tab-pane">
									<div class="line">
		               					<div class="form-row">
		               						<div class="col-label">
		                                			<label for="category_dynamicField1" class="col-form-label">
		                                				<fmt:message key="dynamicField1"/>
		                                			</label>
		                                		</div>
		                                		<div class="col-lg-4"> 
								   				<form:input path="category.dynamicField1" id="category_dynamicField1" class="form-control" maxlength="255" />
		                                		</div>
										</div>
									</div>
									<div class="line">
			               				<div class="form-row">
			               					<div class="col-label">
			                                		<label for="category_dynamicField2" class="col-form-label">
			                                			<fmt:message key="dynamicField2"/>
												</label>
			                                	</div>
			                                <div class="col-lg-4">
									   			<form:input path="category.dynamicField2" id="category_dynamicField2" class="form-control" maxlength="255"/>
			                                </div>
			                            </div>
		                            </div>
			                    </div>
			                    <!-- End Tab7 -->
			                    <c:import url="/WEB-INF/jsp/admin-responsive/catalog/category/formLanguageSection.jsp" />
			                    <div id="tab_more_html" class="tab-pane">
		                            <div class="form-section">
										<div class="line">
											<div class="form-row">
												<div class="col-label">
					                                <label class="col-form-label"> 
					                                		<span class="fa fa-question-circle" data-toggle="tooltip" data-placement="top" title="Left Bar(top):: Enter the HTML Code for custom Left Bar(top)."></span><fmt:message key="LeftBarTop" /> <fmt:message key="categoryHtml" />
					                                </label>
				                                </div>
			                                
												<div class="col-lg-8">
				                                    <div class="form-group">
										      	  		<form:textarea class="code-editor" rows="8" path="category.leftBarTopHtmlCode" id="category_leftBarTopHtmlCode"/>
				                                    </div>
			                                    </div>
		                                    </div>
			                            </div>
			                            
										<div class="line">
											<div class="form-row">
												<div class="col-label">
													<label class="col-form-label">
														<span class="fa fa-question-circle" data-toggle="tooltip" data-placement="top" title="Left Bar(bottom):: Enter the HTML Code for custom Left Bar(bottom)."></span><fmt:message key="LeftBarBottom" /> <fmt:message key="categoryHtml" />
													</label>
												</div>
												<div class="col-lg-8">
				                                    <div class="form-group">
										      	  		<form:textarea class="code-editor" rows="8" path="category.leftBarBottomHtmlCode" id="category_leftBarBottomHtmlCode"/>
				                                    </div>
			                                    </div>
			                                </div>
			                            </div>
			                            
			                            <div class="line">
											<div class="form-row">
												<div class="col-label">
					                                	<label class="col-form-label">
					                               		<span class="fa fa-question-circle" data-toggle="tooltip" data-placement="top" title="Right Bar(top):: Enter the HTML Code for custom Right Bar(top)."></span><fmt:message key="RightBarTop" /> <fmt:message key="categoryHtml" />
					                                	</label>
			                                    </div>
			                                    <div class="col-lg-8">
				                                    <div class="form-group">
														<form:textarea class="code-editor" rows="8" path="category.rightBarTopHtmlCode" id="category_rightBarTopHtmlCode"/>
				                                    </div>
			                                    </div>
			                                </div>
			                            	</div>
			                            
										<div class="line">
											<div class="form-row">
												<div class="col-label">
				                                		<label class="col-form-label">
														<span class="fa fa-question-circle" data-toggle="tooltip" data-placement="top" title="Right Bar(bottom):: Enter the HTML Code for custom Right Bar(bottom)."></span><fmt:message key="RightBarBottom" /> <fmt:message key="categoryHtml" />
													</label>
			                                		</div>
												<div class="col-lg-8">
				                                    <div class="form-group">
										      	  		<form:textarea class="code-editor" rows="8" path="category.rightBarBottomHtmlCode" id="category_rightBarBottomHtmlCode"/>
				                                    </div>
			                                    </div>
			                                </div>
			                            </div>
		                            </div>
			                    </div>
			                    <!-- End Tab8 -->
			                </div>
			            </div>
			            <div class="footer">
							<div class="form-row">
								<div class="col-12">
									<c:if test="${categoryForm.newCategory}">
										<sec:authorize ifAnyGranted="ROLE_AEM,ROLE_ADMIN_${prop_site_id},ROLE_CATEGORY_CREATE">
											<button class="btn btn-default"><fmt:message key="categoryAdd"/></button>
										</sec:authorize>
									</c:if>
									<c:if test="${!categoryForm.newCategory}">
										<sec:authorize ifAnyGranted="ROLE_AEM,ROLE_ADMIN_${prop_site_id},ROLE_CATEGORY_EDIT,ROLE_CATEGORY_UPDATE_HEAD_TAG"> 
											<button class="btn btn-default"><fmt:message key="update"/></button>
										</sec:authorize>
									</c:if>
									<button class="btn btn-default">Cancel</button>
								</div>
							</div>
			            </div>
		            </div>
	            </div>
			</div>
			<form:hidden path="newCategory" />
			<c:if test="${categoryForm.category.rank != null}">
				<form:hidden path="category.rank" />
			</c:if>
		</form:form> 
	</tiles:putAttribute>
	<tiles:putAttribute name="js">
	    <script src="${_contextpath}/admin-responsive-static/assets/plugins/tinymce/tinymce.min.js"></script>
	    <script>
			tinymce.init({
				selector: '.code-editor',
				toolbar_items_size : 'small',
				height: 250,
				theme: 'modern',
				plugins: 'code print preview fullpage searchreplace autolink directionality visualblocks visualchars fullscreen image link media template codesample table charmap hr pagebreak nonbreaking anchor toc insertdatetime advlist lists textcolor wordcount imagetools contextmenu colorpicker textpattern help',
				toolbar1: 'formatselect | bold italic strikethrough forecolor backcolor | link | alignleft aligncenter alignright alignjustify  | numlist bullist outdent indent  | removeformat',
				image_advtab: true,
				templates: [
					{ title: 'Test template 1', content: 'Test 1' },
					{ title: 'Test template 2', content: 'Test 2' }
				],
				content_css: [
					'//fonts.googleapis.com/css?family=Lato:300,300i,400,400i',
					'//www.tinymce.com/css/codepen.min.css',
					"${_contextpath}/admin-responsive-static/assets/css/mce-custom.css",
				]
			});
		</script>
	</tiles:putAttribute>
</tiles:insertDefinition>