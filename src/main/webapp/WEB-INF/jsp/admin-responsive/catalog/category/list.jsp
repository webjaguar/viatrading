<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ taglib uri="http://www.springframework.org/security/tags" prefix="sec" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ taglib uri="/WEB-INF/tlds/spring.tld" prefix="spring" %>
<%@ taglib uri="/WEB-INF/tlds/custom.tld" prefix="wj" %>

<tiles:insertDefinition name="admin.responsive-template.catalog.category" flush="true">
	<tiles:putAttribute name="css">
		<style>
			#grid .field-name{
			    width: 200px;
			}
			#grid .field-subCategories{
				width: 120px;
				text-align: right;
			}
			#grid .field-products{
				text-align: right;
			}
			#grid .field-numOfClicks{
				text-align: right;
			}
			#grid .field-rank{
				text-align: right;
			}
			#grid .field-rank input{
				text-align: right;
			}
			#grid .field-hasHtmlCode{
				text-align: center;
			}
		</style>
	</tiles:putAttribute>
	<tiles:putAttribute name="content" type="string">
		<form id="category-list-form" action="categoryList.jhtm" method="post">
			<div class="page-banner">
				<div class="breadcrumb"><span><a href="${_contextpath}/admin/catalog/dashboard.jhtm">Home</a></span><i class="sli-arrow-right"></i><span>Categories</span></div>
			</div>
	  		<div class="page-head">
				<div class="row justify-content-between">
					<div class="col-sm-8 title">
						<h3>Categories</h3>
					</div>
					<div class="col-sm-4 actions">
						<div class="dropdown dropdown-actions">
							<button type="button" class="btn-outline dropdown-toggle active btn btn-primary" data-toggle="dropdown">
	                             <i class="sli-settings"></i> Actions
	                         </button>
	                         <div class="dropdown-menu dropdown-menu-right">
								<a class="dropdown-item">New Category</a>
								<a class="dropdown-item">Update Ranking</a>
								<div tabindex="-1" class="dropdown-divider"></div>
								<c:if test="${!gSiteConfig['gADI'] and siteConfig['TRAVERS'].value == ''}">
									<sec:authorize ifAnyGranted="ROLE_AEM,ROLE_ADMIN_${prop_site_id},ROLE_CATEGORY_EXPORT">
										<a href="categoryExport.jhtm" class="dropdown-item"><fmt:message key="export"/></a>
									</sec:authorize>
									<c:choose>
										<c:when test="${gSiteConfig['gRESELLER'] == 'roi'}">
											<a href="categoryTreeImport.jhtm" class="dropdown-item"><fmt:message key="import"/></a>
										</c:when>
										<c:otherwise>
											<sec:authorize ifAnyGranted="ROLE_AEM">
												<a href="categoryTreeImport.jhtm" class="dropdown-item"><fmt:message key="import"/> <small>(AEM ONLY)</small></a>   
											</sec:authorize>
										</c:otherwise>
									</c:choose>
								</c:if>
	                         </div>
	                    </div>
	                    <div class="dropdown dropdown-setting">
							<button type="button" class="btn-outline dropdown-toggle active btn btn-primary" data-toggle="dropdown">
	                             <i class="sli-screen-desktop"></i> Display
	                         </button>
							<div class="dropdown-menu dropdown-menu-right">
								<div class="form-group">
			                    		<div class="option-header">Subcategories</div>
									<select name="parent" class="custom-select form-control" onchange="document.getElementById('page').value=1;submit();">
				                        <option value="" <c:if test="${current == model.categories.pageSize}">selected</c:if>>
					                       <fmt:message key="categoryMain" />
					                    </option>
					                    <option value='home' <c:if test="${model.search['homePage']}"> selected</c:if>>
					                       <fmt:message key="homePages" />
					                    </option>
					                    <c:if test="${gSiteConfig['gMULTI_STORE'] > 0}">
						                    <option value='multi' <c:if test="${model.search['multiStore']}"> selected</c:if>>
						                       <fmt:message key="multiStore" />
						                    </option>
										</c:if>
										<c:forEach items="${model.categoryTree}" var="category">
											<option value='${category.id}' <c:if test="${category.id == model.search['parent']}">selected</c:if>>
												<fmt:message key="categorySub" /> <c:out value="${category.name}" />
											</option>
										</c:forEach>
			                         </select>
								</div>
								<div class="form-group">
			                    		<div class="option-header">Rows</div>
									<select class="custom-select form-control" name="size" onchange="document.getElementById('page').value=1;submit();">
										<c:forTokens items="10,25,50,100" delims="," var="current">
											<option value="${current}" <c:if test="${current == model.categories.pageSize}">selected</c:if>>${current}</option>
										</c:forTokens>
									</select>
								</div>
								<div class="form-group">
			                    		<div class="option-header">Columns</div>
			                    		<div id="column-hider"></div>
								</div>
							</div>
	                    </div>
					</div>
				</div>
			</div>
			<div class="page-body">
				<div class="grid-stage">
				 	<c:if test="${model.categories.nrOfElements > 0}"> 
				 		<div class="page-stat">
							<fmt:message key="showing">
								<fmt:param value="${model.categories.firstElementOnPage + 1}" />
								<fmt:param value="${model.categories.lastElementOnPage + 1}" />
								<fmt:param value="${model.categories.nrOfElements}" />
							</fmt:message>
						</div> 
					</c:if>
		            <table id="grid"></table>
				    <div class="footer">
		                <div class="float-left">
		                    <p class="mb-0"><sup>1</sup> Not linkable on storefront</p>
		                    <p class="mb-0"><sup>2</sup> Hidden on storefront</p>
		                </div>
		                <div class="float-right">
							<c:if test="${model.categories.firstPage}">
						 		<span class="previous"><i class="sli-arrow-left" style="font-size: 0.7rem"></i> Previous</span>
							</c:if>
							<c:if test="${not model.categories.firstPage}">
								<a href="<c:url value="categoryList.jhtm"><c:param name="page" value="${model.categories.page}"/><c:if test="${not model.search['homePage']}"><c:param name="parent" value="${model.search['parent']}"/></c:if></c:url>" >
									<span class="previous"><i class="sli-arrow-left" style="font-size: 0.7rem"></i> Previous</span>
								</a>
							</c:if>
							<span class="status"><c:out value="${model.categories.page+1}" /> of <c:out value="${model.categories.pageCount}" /></span>
							<c:if test="${model.categories.lastPage}">
							 	<span class="next">Next <i class="sli-arrow-right" style="font-size: 0.7rem"></i></span>
							</c:if>
							<c:if test="${not model.categories.lastPage}">
								<a href="<c:url value="categoryList.jhtm"><c:param name="page" value="${model.categories.page+2}"/><c:if test="${not model.search['homePage']}"><c:param name="parent" value="${model.search['parent']}"/></c:if></c:url>">
									<span class="next">Next <i class="sli-arrow-right" style="font-size: 0.7rem"></i></span>
								</a>
							</c:if>
						</div>
					</div>
				</div>
			</div>
			<input type="hidden" id="page" name="page" value="${model.categories.page+1}"/>
		</form>
	</tiles:putAttribute>
	<tiles:putAttribute name="js">
		<!-- DGRID TABLE SCRIPT MUST BE AT BOTTOM OF THE PAGE -->
		<script src="${_contextpath}/admin-responsive-static/assets/plugins/dojo/dojo.js" data-dojo-config="async: true"></script>
		<script>
			//Table Data Sanitization
			var data = [];
			<c:forEach items="${model.categories.pageList}" var="category" varStatus="status">
				data.push({
					offset: '${status.count + model.categories.firstElementOnPage}.',
					name: '${wj:escapeJS(category.name)}',
					url: 'category.jhtm?id=${category.id}',
					<c:choose>
						<c:when test="${category.linkType == 'NONLINK'}">
							linkType: 1,
						</c:when>
						<c:when test="${category.linkType == 'HIDDEN'}">
							linkType: 2,
						</c:when>
					</c:choose>
					<c:if test="${model.search['multiStore']}">
						host: '${wj:escapeJS(category.host)}',
					</c:if>
					id: '${category.id}',
					<c:choose>
					  	<c:when test="${category.host != null}"> 
					  		externalURL: '//${category.host}',
			          	</c:when>
			          	<c:otherwise>
			          		<c:choose>
								<c:when test="${not empty gSiteConfig['gMOD_REWRITE'] and gSiteConfig['gMOD_REWRITE'] == '1'}">
									externalURL: "${siteConfig['SITE_URL'].value}${siteConfig['MOD_REWRITE_CATEGORY'].value}/${category.id}/${category.encodedName}.html", 
								</c:when>
								<c:otherwise>
									externalURL: "${siteConfig['SITE_URL'].value}category.jhtm?cid=${category.id}", 
								</c:otherwise>
							</c:choose>
			          	</c:otherwise>
		          	</c:choose>
		          	subCount: ${category.subCount},
		          	subCategoriesURL: '<c:url value="categoryList.jhtm"><c:param name="parent" value="${category.id}"/></c:url>',
		          	productCount: ${category.productCount},
		          	productsURL: '<c:url value="productList.jhtm"><c:param name="category" value="${category.id}"/></c:url>',
		          	numOfClicks: ${category.numOfClicks},
		          	<c:choose>
			          	<c:when test="${category.hasHtmlCode}">
							hasHtmlCode: true,
						</c:when>
			          	<c:otherwise>
							hasHtmlCode: false,
						</c:otherwise>
			        </c:choose>
			        rank: '${category.rank}',
			        <c:choose>
			          	<c:when test="${category.homePage}">
			          		homePage: true,
						</c:when>
			          	<c:otherwise>
			          		homePage: false,
						</c:otherwise>
			        </c:choose>
			        
			        <c:if test="${gSiteConfig['gPROTECTED'] > 0}">
						protectedLevelAsNumber: ${category.protectedLevelAsNumber},
					</c:if>
				});
			</c:forEach>
			//Columns Definitions
			require(['dojo/_base/lang',
					'dojo/_base/declare',
					'dstore/Memory',
					'dstore/Trackable',
					'dgrid/OnDemandGrid',
					'dgrid/Editor',
					'dgrid/extensions/ColumnHider',
					'dgrid/extensions/ColumnReorder',
					'dgrid/extensions/ColumnResizer'
				], function (lang, declare, Memory, Trackable, OnDemandGrid, Editor, ColumnHider, ColumnReorder, ColumnResizer) {
				var columns = {
					offset: {
						label: '#',
						get: function(object){
							return object.offset;
						},
						sortable: false,
						unhidable: true
					},
					name: {
						label: 'Name',
						renderCell: function(object, data, td, options){
							var span = document.createElement("span");
							span.textContent = object.name;
							var link = document.createElement("a");
							link.className = 'plain';
							link.appendChild(span);
							if(object.linkType){
								var sup = document.createElement("sup");
								sup.textContent = object.linkType;
								link.appendChild(sup);
							}
							link.href = object.url;
							return link;
						},
						sortable: false
					},
					id: {
						label: 'Category ID',
						renderCell: function(object, data, td, options){
							var div = document.createElement("div");
							div.innerHTML = object.id;
							var link = document.createElement("a");
							link.className = 'external-link';
							link.innerHTML = '<span class="sli-eye"></span>';
							link.href = object.externalURL;
							div.appendChild(link);
							return div;
						},
						sortable: false
					},
					subCategories: {
						label: 'Sub-Categories',
						renderCell: function(object){
							var link = document.createElement("a");
							link.innerHTML = object.subCount;
							link.href = object.subCategoriesURL;
							return link;
						},
						sortable: false
					},
					products: {
						label: 'Products',
						renderCell: function(object){
							var link = document.createElement("a");
							link.innerHTML = object.productCount;
							link.href = object.productsURL;
							return link;
						},
						sortable: false
					},
					numOfClicks: {
						label: 'Views',
						get: function(object){
							return object.numOfClicks;
						},
						sortable: false
					},
					rank: {
						label: '<fmt:message key="rank" />',
						editor: "text",
						sortable: false
					},
					hasHtmlCode: {
						label: 'HTML',
						renderCell: function(object){
							var div = document.createElement("div");
							if(object.hasHtmlCode){
								var i = document.createElement("i");
								i.className = 'sli-check';
								div.appendChild(i);
							}
							return div;
						},
						sortable: false
					},
					flags:{
						label: 'Flags',
						renderCell: function(object){
							var div = document.createElement("div");
							if(object.homePage){
								var i = document.createElement("i");
								i.className = 'sli-home';
								div.appendChild(i);
							}
							<c:if test="${gSiteConfig['gPROTECTED'] > 0}">
								if(object.protectedLevelAsNumber != 0){
									var i = document.createElement("i");
									i.className = 'sli-lock';
									var sup = document.createElement("sup");
									sup.innerHTML = object.protectedLevelAsNumber;
									div.appendChild(i);
									div.appendChild(sup);
								}
							</c:if>
							return div;
						},
						sortable: false
					}
				};
				
				var store = new (declare([Memory, Trackable]))({
					data: data,
					idProperty:"id"
				});

				var grid = new (declare([OnDemandGrid, Editor, ColumnHider, ColumnReorder, ColumnResizer]))({
					collection: store,
					columns: columns
				},'grid');
				
				grid.on('dgrid-refresh-complete', function(event) {
					$('#grid-hider-menu').appendTo('#column-hider');
				});
				
				grid.startup();
			});
		</script>
	</tiles:putAttribute>
</tiles:insertDefinition>