<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ taglib uri="http://www.springframework.org/security/tags" prefix="sec" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ taglib uri="/WEB-INF/tlds/spring.tld" prefix="spring" %>
<%@ taglib uri="/WEB-INF/tlds/custom.tld" prefix="wj" %>

<tiles:insertDefinition name="admin.responsive-template.catalog.product.form" flush="true">
	<tiles:putAttribute name="css">
		<style>
			.price-line{
			    border-bottom: 1px dashed #d7d7d9;
			    padding-bottom: 0.5rem;
			}
			#variants .field-name{
			    width: 120px;
			}
			#variants .field-shortDesc{
			    width: 120px;
			}
		</style>
	</tiles:putAttribute>
	<tiles:putAttribute name="content" type="string">
		<form:form commandName="productForm" method="post" name="productForm" class="page-form" enctype="multipart/form-data">
			<div class="page-banner">
			    <div class="breadcrumb">
					<span><a href="${_contextpath}/admin/catalog/dashboard.jhtm">Home</a></span>
					<i class="sli-arrow-right"></i><span><a href="${_contextpath}/admin/catalog/productList.jhtm">Products</a></span>
					<i class="sli-arrow-right"></i><span>${productForm.product.sku}</span>
				</div>
			</div>
			<div class="page-head">
				<div class="row justify-content-between">
					<div class="col-sm-8 title">
						${productForm.product.name}
					</div>
					<div class="col-sm-4 actions">
						<div class="dropdown">
							<button type="button" class="btn-outline dropdown-toggle active btn btn-primary" data-toggle="dropdown">
	                             <i class="sli-settings"></i> Actions
	                         </button>
	                         <div class="dropdown-menu dropdown-menu-right">
								<c:if test="${!productForm.newProduct}">
								  	<sec:authorize ifAnyGranted="ROLE_AEM,ROLE_ADMIN_${prop_site_id},ROLE_PRODUCT_EDIT">
										<c:if test="${gSiteConfig['gSHOPPING_CART']}">
											<a class="dropdown-item"><fmt:message key="removeFromCart" /></a>
										</c:if>
								  	</sec:authorize>
								  	<c:if test="${!gSiteConfig['gADI'] and (siteConfig['WAREHOUSE'].value != 'true')}">
										<sec:authorize ifAnyGranted="ROLE_AEM,ROLE_ADMIN_${prop_site_id},ROLE_PRODUCT_DELETE">
											<a class="dropdown-item"><fmt:message key="productDelete"/></a>
									  	</sec:authorize>
								   	</c:if>
								</c:if>
	                         </div>
						</div>
					</div>
				</div>
			</div>
			<div class="page-body">
				<c:if test="${!productForm.newProduct}">
					<div class="minor-info">
						<span>
							<span><fmt:message key="productId" /></span>: 
							<spring:bind path="productForm.product.id">
							    <span><c:out value="${status.value}"/></span>
						        <input type="hidden"  name="<c:out value="${status.expression}"/>" value="<c:out value="${status.value}"/>">
							</spring:bind>
						</span>
						<span><span><fmt:message key="createdBy" /></span>: <span><c:out value="${productForm.product.createdBy}"/></span></span>
						<span><span><fmt:message key="dateAdded" /></span>: <span><fmt:formatDate type="both" timeZone="${siteConfig['SITE_TIME_ZONE'].value}" value="${productForm.product.created}"/></span></span>
						<span><span><fmt:message key="lastModified" /></span>: <span><fmt:formatDate type="both" timeZone="${siteConfig['SITE_TIME_ZONE'].value}" value="${productForm.product.lastModified}"/></span></span>
					</div>
				</c:if>
			  	<div class="main-content">
				  	<div class="form-section">
				        <div class="content">
				            <ul class="nav nav-tabs" role="tablist">
				                <li class="nav-item">
				                    <a class="nav-link active" data-toggle="tab" role="tab" href="#tab_1">Product Info</a>
				                </li>
				                <li class="nav-item">
				                    <a class="nav-link" data-toggle="tab" role="tab" href="#tab_2"><fmt:message key="layout" /></a>
				                </li>
				                <li class="nav-item">
				                		<div class="dropdown">
					                    <a class="nav-link" data-toggle="dropdown">Advanced <i class="fa fa-caret-down"></i></a>
										<div class="dropdown-menu dropdown-menu-right">
											<c:if test="${not empty productForm.productFields or gSiteConfig['gPRODUCT_FIELDS_UNLIMITED'] > 0}">
								                <a class="dropdown-item" data-toggle="tab" role="tab" href="#tab_3">ProductField</a>
											</c:if>
										 	<c:if test="${gSiteConfig['gLOYALTY']}">
								                <a class="dropdown-item" data-toggle="tab" role="tab" href="#tab_4"><fmt:message key="loyalty" /></a>
							                </c:if>
							                <a class="dropdown-item" data-toggle="tab" role="tab" href="#tab_5">Price</a>
											<c:if test="${siteConfig['SEHI_FEED'].value != ''}">
								                <a class="dropdown-item" data-toggle="tab" role="tab" href="#tab_6"><fmt:message key="priceLevel" /></a>
											</c:if>
											<c:if test="${gSiteConfig['gINVENTORY']}">
												<a class="dropdown-item" data-toggle="tab" role="tab" href="#tab_7"><fmt:message key="inventory" /></a>
										  	</c:if>
										  	<c:if test="${gSiteConfig['gKIT']}">
											  	<a class="dropdown-item" data-toggle="tab" role="tab" href="#tab_8">Build A Kit</a>
											</c:if>
											<c:if test="${gSiteConfig['gAFFILIATE'] > 0 and siteConfig['NUMBER_PRODUCT_COMMISSION_TABLE'].value > 0}">
												<a class="dropdown-item" data-toggle="tab" role="tab" href="#tab_9"><fmt:message key="affiliate" /></a>
											</c:if>
											<c:if test="${gSiteConfig['gRECOMMENDED_LIST']}">
												<a class="dropdown-item" data-toggle="tab" role="tab" href="#tab_10"><fmt:message key="recommendedList" /></a>
											</c:if>
											<c:if test="${gSiteConfig['gPRODUCT_VARIANTS']}">
												<a class="dropdown-item" data-toggle="tab" role="tab" href="#tab_11"><fmt:message key="variants" /></a>
											</c:if>
											<c:if test="${gSiteConfig['gSUBSCRIPTION']}">
										  		<a class="dropdown-item" data-toggle="tab" role="tab" href="#tab_12"><fmt:message key="subscription" /></a>
										  	</c:if>
										  	<c:if test="${siteConfig['NEW_PROMO'].value == 'true'}">
										  		<a class="dropdown-item" data-toggle="tab" role="tab" href="#tab_13"><fmt:message key="decoration" /></a>
											</c:if>
											<c:if test="${gSiteConfig['gTAB'] > 0}">
												<a class="dropdown-item" data-toggle="tab" role="tab" href="#tab_14"><fmt:message key="tab" /></a>
											</c:if>
											<a class="dropdown-item" data-toggle="tab" role="tab" href="#tab_15"><fmt:message key="image" /></a>
											<a class="dropdown-item" data-toggle="tab" role="tab" href="#tab_16"><fmt:message key="note" /></a>
											<c:if test="${siteConfig['SITEMAP'].value == 'true'}">
												<a class="dropdown-item" data-toggle="tab" role="tab" href="#tab_18">SEO</a>
										    </c:if>
										    <%-- <c:if test="${siteConfig['AMAZON_FEED_ENABLE'].value == 'true'}">
										    		<a class="dropdown-item" data-toggle="tab" role="tab" href="#tab_19">Amazon</a>
										    </c:if> --%>
										    <c:if test="${siteConfig['QUICKBOOKS_INTEGRATION'].value == 'true'}">
										    		<a class="dropdown-item" data-toggle="tab" role="tab" href="#tab_20"><fmt:message key="quickbooks" /></a>
										    </c:if>
											<c:forEach items="${languageCodes}" var="i18n" varStatus="loop"> 
							                    <a class="dropdown-item" data-toggle="tab" role="tab" href="#tab_lan_${loop.index}">
							                    		<img src="${_contextpath}/admin-responsive-static/assets/flags/${i18n.languageCode}.png" class="mb-1" title="<fmt:message key="language_${i18n.languageCode}"/>"> <fmt:message key="language_${i18n.languageCode}"/>
							                    </a>
							                </c:forEach>
										</div>
									</div>
				                </li>
				            </ul>
				            <div class="tab-content">
				            		<c:import url="/WEB-INF/jsp/admin-responsive/catalog/product/formProductMain.jsp" />
				            		<c:import url="/WEB-INF/jsp/admin-responsive/catalog/product/formLayout.jsp" />
				            		<c:if test="${not empty productForm.productFields or gSiteConfig['gPRODUCT_FIELDS_UNLIMITED'] > 0}">
				            			<c:import url="/WEB-INF/jsp/admin-responsive/catalog/product/formFieldSection.jsp" />
				            		</c:if>
				            		<c:if test="${gSiteConfig['gLOYALTY']}">
				            			<c:import url="/WEB-INF/jsp/admin-responsive/catalog/product/formLoyaltySection.jsp" />
				            		</c:if>
				            		<c:import url="/WEB-INF/jsp/admin-responsive/catalog/product/formPriceSection.jsp" />
				            		<c:if test="${siteConfig['SEHI_FEED'].value != ''}">
								  	<c:import url="/WEB-INF/jsp/admin-responsive/catalog/product/formPriceLevels.jsp" />
								</c:if>
				            		<c:if test="${gSiteConfig['gINVENTORY']}">
									<c:import url="/WEB-INF/jsp/admin-responsive/catalog/product/inventoryTab.jsp" />
							  	</c:if>
							  	<c:if test="${gSiteConfig['gKIT']}">
							  		<c:import url="/WEB-INF/jsp/admin-responsive/catalog/product/formKitSection.jsp" />
							  	</c:if>
							  	<c:if test="${gSiteConfig['gAFFILIATE'] > 0 and siteConfig['NUMBER_PRODUCT_COMMISSION_TABLE'].value > 0}">
									<div id="tab_9" class="tab-pane" role="tabpanel">
										<c:forEach begin="1" end="${siteConfig['NUMBER_PRODUCT_COMMISSION_TABLE'].value}" var="commissionTable">
										  	<div class="line">
											  	<div class="form-row">
											  		<div class="col-label">
												    		<label class="col-form-label">
															<fmt:message key="commisionTable" /> ${commissionTable}
														</label>
													</div>
													<div class="col-lg-4">
														<div class="input-group">
											        			<form:input path="product.commissionTables.commissionTable${commissionTable}" class="form-control" maxlength="10"/>
											        			<span class="input-group-addon"><fmt:message key="${siteConfig['CURRENCY'].value}" /></span>
										        			</div>
										            </div>
											  	</div>
										  	</div>
										</c:forEach>
									</div>
								</c:if>
								<c:if test="${gSiteConfig['gRECOMMENDED_LIST']}">
									<c:import url="/WEB-INF/jsp/admin-responsive/catalog/product/formRecommendedSection.jsp" />
								</c:if>
								<c:if test="${gSiteConfig['gPRODUCT_VARIANTS']}">
							  		<c:import url="/WEB-INF/jsp/admin-responsive/catalog/product/formVariantSection.jsp" />
							  	</c:if>
							  	<c:if test="${gSiteConfig['gSUBSCRIPTION']}">
							  		<c:import url="/WEB-INF/jsp/admin-responsive/catalog/product/formSubscriptionSection.jsp" />
							  	</c:if>
							  	<c:if test="${siteConfig['NEW_PROMO'].value == 'true'}">
									<c:import url="/WEB-INF/jsp/admin-responsive/catalog/product/formDecorationSection.jsp" />
								</c:if>
								<c:if test="${gSiteConfig['gTAB'] > 0}">
									<div id="tab_14" class="tab-pane" role="tabpanel">
										<c:forEach begin="1" end="${gSiteConfig['gTAB']}" var="tabNumber">
											<div class="line">
												<div class="form-row">
													<div class="col-label">
												    		<label class="col-form-label">
															Tab${tabNumber} Name
														</label>
													</div>
											        <div class="col-lg-4">
												        <c:choose>
												  			<c:when test="${gSiteConfig['gADI'] and  tabNumber < 4}">
														    		<form:input path="product.tab${tabNumber}" disabled="true" class="form-control" />
													      	</c:when>
												  		  	<c:otherwise>
														    		<form:input path="product.tab${tabNumber}" class="form-control" />
													      	</c:otherwise>
												  		</c:choose>
													</div>
												</div>
											</div>
											<div class="line">
												<div class="form-row">
													<div class="col-label">
												    		<label class="col-form-label">
															Tab${tabNumber} Content
														</label>
													</div>
											        <div class="col-lg-8">
													    <c:choose>
												  			<c:when test="${siteConfig['DETAILS_LONG_DESC_HTML'].value != 'false'}">
												  				<div class="code-editor" data-editor="product_tab${tabNumber}Content">
											                     	<form:textarea path="product.tab${tabNumber}Content" id="product_tab${tabNumber}Content" />
											                     </div>
													      	</c:when>
												  		  	<c:otherwise>
														    		<form:textarea path="product.tab${tabNumber}Content" id="product_tab${tabNumber}Content" class="form-control" rows="8" />
													      	</c:otherwise>
												  		</c:choose>
													</div>
												</div>
											</div>
										</c:forEach>
									</div>
								</c:if>
								<div id="tab_15" class="tab-pane" role="tabpanel">
									<c:forEach items="${productForm.productImages}" var="image" varStatus="status">
										<div class="line">
											<div class="form-row">
												<div class="col-label">
											    		<label class="col-form-label">
														<span class="fa fa-question-circle" data-toggle="tooltip" data-placement="top" title="Image::Upload the image of the product. Max Image size is 2MB."></span>
														<fmt:message key="categoryName" />
													</label>
												</div>
										        <div class="col-lg-8">
								        				<div class="row">
								        					<c:if test="${image.imageUrl != null}">		
									        					<div class="col-lg-4">
									        						<div class="w-150">
										        						<c:choose>
																	    <c:when test="${image.absolute}">
																			<img src="<c:url value="${image.imageUrl}"/>?rnd=${randomNum}" class="img-thumbnail w-150 h-150" alt="">  
																	    </c:when>
																	    <c:otherwise>
																			<img src="<c:url value="/assets/Image/Product/detailsbig/${image.imageUrl}"/>?rnd=${randomNum}" class="img-thumbnail w-150 h-150" alt="">
																	    </c:otherwise>
																    </c:choose>
																    <div class="text-center">
																		<c:out value="${image.imageUrl}" />
																    </div>
															    </div>
									        					</div>
								        					</c:if>
														<div class="col-lg-8">
															<c:if test="${!gSiteConfig['gADI']}">
																<div class="row">
											        					<div class="col-lg-3">
											        						<label class="custom-control custom-checkbox mt-0">
													                        <input name="__clear_image_${image.index}" class="custom-control-input" type="checkbox">
													                        <span class="custom-control-indicator"></span>
													                        <span class="custom-control-description"><fmt:message key="remove" /></span>
													                    </label>
																	</div>
																	<c:if test="${image.imageUrl == null}">
																		<div class="col-lg-9">
																			<input value="browse" type="file" name="image_file_${image.index}"/>
																		</div>
															    		</c:if>
														    		</div>
													       	</c:if>
															<div class="row">
																<div class="col-lg-9">
														       		<label>Reference URL:</label>
																	<input id="referenceUrl" value="${image.referenceUrl}" name="_reference_url_${image.index}"  type="text" class="form-control"/>
																</div>
																<div class="col-lg-3">
														       		<label>Rank:</label>
																	<input id="imageRank" value="${image.imageRank}" name="_image_rank_${image.index}"  type="text" class="form-control"/>
																</div>
															</div>
														</div>
														<div class="divider"></div>
						        						</div>
												</div>
											</div>
										</div>
									</c:forEach>
									<c:if test="${!gSiteConfig['gADI']}">
										<div class="line">
											<div class="form-row">
												<div class="col-label">
											    		<label for="imageFolder" class="col-form-label">
														<span class="fa fa-question-circle" data-toggle="tooltip" data-placement="top" title="Folder Name::All the images under this folder will be displayed with this product, at the product's front end."></span>
														<fmt:message key="folder" /> <fmt:message key="Name" />
													</label>
												</div>
										        <div class="col-lg-4">
												    <form:input id="imageFolder" path="product.imageFolder" class="form-control"/>
												</div>
											</div>
										</div>
									</c:if>
									<c:if test="${siteConfig['PRODUCT_IMAGE_LAYOUTS'].value != ''}">  
									 	<div class="line">
										 	<div class="form-row">
										 		<div class="col-label">
											    		<label for="imageLayout" class="col-form-label">
											    			<fmt:message key="layout" />
													</label>
												</div>
										        <div class="col-lg-4">
										        		<form:select path="product.imageLayout" id="imageLayout" class="custom-select" onchange="toggleImageFolder()">
											        		<form:option value=""><fmt:message key="global"/></form:option> 
											          	<c:forTokens items="${siteConfig['PRODUCT_IMAGE_LAYOUTS'].value}" delims="," var="slideshow">
											            		<form:option value="${slideshow}"><fmt:message key="${slideshow}" /></form:option>
											          	</c:forTokens>
											       	</form:select>
												</div>
											</div>
										</div>
							  		</c:if>
								</div>
								<div id="tab_16" class="tab-pane" role="tabpanel">
									<div class="line">
										<div class="form-row">
											<div class="col-label">
										    		<label for="product_note" class="col-form-label">
										    			<span class="fa fa-question-circle" data-toggle="tooltip" data-placement="top" title="Note:: Enter the notes related to product here. Admin only."></span>
										    			Note
												</label>
											</div>
									        <div class="col-lg-4">
									        		<form:textarea path="product.note" id="product_note" class="form-control" rows="8" />
											</div>
										</div>
									</div>
									<c:if test="${gSiteConfig['gASI'] != ''}">
									  	<sec:authorize ifAnyGranted="ROLE_AEM,ROLE_ADMIN_${prop_site_id}">
										  	<div class="line">
											  	<div class="form-row">
											  		<div class="col-label">
												    		<label for="product_asiXML" class="col-form-label">
												    			ASI XML
														</label>
													</div>
											        <div class="col-lg-4">
											        		<form:textarea path="product.asiXML" id="product_asiXML" class="form-control" rows="8" disabled="true"/>
													</div>
												</div>
											</div>
									  	</sec:authorize>
								  	</c:if>
								</div>
								<%-- <c:if test="${gSiteConfig['gDOWNLOADABLE_PRODUCTS']}">
									<div id="tab_17" class="tab-pane" role="tabpanel">
										<div class="form-group form-row">
									    		<label class="col-sm-3 col-md-3 col-lg-3 col-form-label text-sm-right">
									    			<img class="toolTipImg" title="Note:: Enter the notes related to product here. Admin only." src="../graphics/question.gif" />
									    			Upload:
											</label>
									        <div class="col-sm-9 col-md-9 col-lg-9">
									        		<c:if test="${productForm.product.downloadableFileName != null and productForm.product.downloadableFileName != ''}">
											 		<c:out value="${productForm.product.downloadableFileName}"/>&nbsp;
											 		<label class="custom-control custom-checkbox mt-0">
								                        <input name="__clear_downloadableFile" type="checkbox" class="custom-control-input">
								                        <span class="custom-control-indicator"></span>
								                        <span class="custom-control-description"><fmt:message key="remove" /></span>
								                    </label>
											    </c:if>
											    <c:if test="${productForm.product.downloadableFileName == null or productForm.product.downloadableFileName == ''}">
											     	<input value="browse" type="file" name="upload_downloadableFile"/>  
											    </c:if>
											</div>
										</div>
									</div>
								</c:if> --%>
								<c:if test="${siteConfig['SITEMAP'].value == 'true'}">
			 						<div id="tab_18" class="tab-pane" role="tabpanel">
										<div class="line">
											<div class="form-row">
												<div class="col-label">
											    		<label for="product_siteMapPriority" class="col-form-label">
											    			<span class="fa fa-question-circle" data-toggle="tooltip" data-placement="top" title="Site Map Priority::Select product priority for Site Map. Default priority is 0.5"></span>
											    			<fmt:message key="priority" />
													</label>
												</div>
										        <div class="col-lg-4">
										        		<form:select path="product.siteMapPriority" id="product_siteMapPriority" class="custom-select">
											  	     	<c:forEach begin="0" end="10" step="1" var="priority">
											  	       		<form:option value="${priority/10}" label="${priority/10}"></form:option>
											  	     	</c:forEach>
											  	   </form:select>
												</div>
											</div>
										</div>
									</div>
							    </c:if>
							    <c:if test="${siteConfig['QUICKBOOKS_INTEGRATION'].value == 'true'}">
							    		<div id="tab_20" class="tab-pane" role="tabpanel">
							    			<div class="line">
								    			<div class="form-row">
								    				<div class="col-label">
											    		<label for="product_qbIncomeAccountType" class="col-form-label">
											    			<fmt:message key="qbIncomeAccountType" />
													</label>
												</div>
										        <div class="col-lg-4">
										        		<form:input path="product.qbIncomeAccountType" id="product_qbIncomeAccountType" class="form-control" maxlength="50"/>
												</div>
											</div>
										</div>
										<div class="line">
											<div class="form-row">
										    		<div class="col-label">
											    		<label for="product_qbExpenseAccountType" class="col-form-label">
											    			<fmt:message key="qbExpenseAccountType" />
													</label>
												</div>
										        <div class="col-lg-4">
										        		<form:input path="product.qbExpenseAccountType" id="product_qbExpenseAccountType" class="form-control" maxlength="50"/>
												</div>
											</div>
										</div>
							    		</div>
								</c:if>
								<c:import url="/WEB-INF/jsp/admin-responsive/catalog/product/formLanguageSection.jsp" />         
				            </div>
			 			</div>
		 				<div class="footer">
							<div class="form-row">
								<div class="col-12">
									<c:if test="${productForm.newProduct and !gSiteConfig['gADI']}">
										<sec:authorize ifAnyGranted="ROLE_AEM,ROLE_ADMIN_${prop_site_id},ROLE_PRODUCT_CREATE">
									  		<button class="btn btn-default"><fmt:message key="productAdd"/></button>
									  	</sec:authorize>  
									</c:if>
									<sec:authorize ifAnyGranted="ROLE_AEM,ROLE_ADMIN_${prop_site_id},ROLE_PRODUCT_EDIT">
				                        <button class="btn btn-default"><fmt:message key="productUpdate"/></button>
			                        </sec:authorize>
									<a class="btn btn-default">Cancel</a>
								</div>
							</div>
			            </div>
		 			</div>
				</div>
			</div>
			<form:hidden path="newProduct" />
			<form:hidden path="product.id" />
		</form:form>
	</tiles:putAttribute>
	<tiles:putAttribute name="js">
		<script src="${_contextpath}/admin-responsive-static/assets/plugins/tinymce/tinymce.min.js"></script>
		<script>
			tinymce.init({
				selector: '.code-editor',
				toolbar_items_size : 'small',
				height: 250,
				theme: 'modern',
				plugins: 'code print preview fullpage searchreplace autolink directionality visualblocks visualchars fullscreen image link media template codesample table charmap hr pagebreak nonbreaking anchor toc insertdatetime advlist lists textcolor wordcount imagetools contextmenu colorpicker textpattern help',
				toolbar1: 'formatselect | bold italic strikethrough forecolor backcolor | link | alignleft aligncenter alignright alignjustify  | numlist bullist outdent indent  | removeformat',
				image_advtab: true,
				templates: [
					{ title: 'Test template 1', content: 'Test 1' },
					{ title: 'Test template 2', content: 'Test 2' }
				],
				content_css: [
					'//fonts.googleapis.com/css?family=Lato:300,300i,400,400i',
					'//www.tinymce.com/css/codepen.min.css',
					"${_contextpath}/admin-responsive-static/assets/css/mce-custom.css",
				]
			});
		 	
		 	if(document.getElementById('product_discountStartDate')){
				$('#product_discountStartDate').datetimepicker({
					format: "MM-DD-YYYY hh:mm A"
				});
			}
			
			if(document.getElementById('product_discountEndDate')){
				$('#product_discountEndDate').datetimepicker({
					format: "MM-DD-YYYY hh:mm A",
				});
			}
		</script>
		<!-- DGRID TABLE SCRIPT MUST BE AT BOTTOM OF THE PAGE -->
		<script src="${_contextpath}/admin-responsive-static/assets/plugins/dojo/dojo.js" data-dojo-config="async: true"></script>
	 	<script>
	 		var kitParts, variants;
	 		
	 		function addKitParts() {
	 			var r = {sku: null, quantity: null};
	 			kitParts.collection.add(r);
			}
	 		
	 		function addVariant() {
	 			var r = {sku: null, name: null, shortDesc: null, price1: null};
	 			variants.collection.add(r);
			}
	 		
	 		function initializeFormKitGrid(){
	 			//Table Data Sanitization
				var data = [];
				<c:forEach items="${productForm.product.kitParts}" var="kitPart" varStatus="status">
					data.push({
						sku: '${kitPart.kitPartsSku}',
						quantity: '${kitPart.quantity}',
					});
				</c:forEach>
				
				require(["dgrid/List", "dgrid/OnDemandGrid", "dgrid/Selection", "dgrid/Keyboard", "dgrid/Editor", "dijit/form/CheckBox", 
						"dijit/form/TimeTextBox", "dijit/form/Select", "dojo/data/ObjectStore", "dijit/form/FilteringSelect",
						"dojo/_base/declare", "dojo/_base/lang", "dojo/store/Memory", "dojo/store/Observable", "dgrid/test/data/createSyncStore", 
						'dgrid/extensions/ColumnReorder', 'dgrid/extensions/ColumnResizer',
					], function(List, OnDemandGrid, Selection, Keyboard, Editor, CheckBox, TimeTextBox, Select, ObjectStore, FilteringSelect,
							declare, lang, LegacyMemory, LegacyObservable, createSyncStore, ColumnReorder, ColumnResizer){

					//Column Definitions
					var columns = {
						sku: {
							label: '<fmt:message key="kitPartSku" />',
							editor: 'text',
							sortable: false,
						},
						quantity: {
							label: '<fmt:message key="quantity" />',
							editor: 'text',
							sortable: false,
						},
					};
					
					kitParts = new (declare([OnDemandGrid, Selection, Keyboard, Editor, ColumnReorder, ColumnResizer]))({
						collection: createSyncStore({ data: data }),
						columns: columns,
						selectionMode: "none",
					},'kitParts');
					
					if(data.length == 0)
						addKitParts();
					
					kitParts.startup();
				});
	 		}
	 	
	 		function initializeVariantGrid(){
	 			//Table Data Sanitization
				var data = [];
				<c:forEach items="${productForm.product.variants}" var="variant" varStatus="status">
					data.push({
						sku: '${variant.sku}',
						name: '${variant.name}',
						shortDesc: '${variant.shortDesc}',
						price1: '${variant.price1}',
					});
				</c:forEach>
				
				require(["dgrid/List", "dgrid/OnDemandGrid", "dgrid/Selection", "dgrid/Keyboard", "dgrid/Editor", "dijit/form/CheckBox", 
						"dijit/form/TimeTextBox", "dijit/form/Select", "dojo/data/ObjectStore", "dijit/form/FilteringSelect",
						"dojo/_base/declare", "dojo/_base/lang", "dojo/store/Memory", "dojo/store/Observable", "dgrid/test/data/createSyncStore", 
						'dgrid/extensions/ColumnReorder', 'dgrid/extensions/ColumnResizer',
					], function(List, OnDemandGrid, Selection, Keyboard, Editor, CheckBox, TimeTextBox, Select, ObjectStore, FilteringSelect,
							declare, lang, LegacyMemory, LegacyObservable, createSyncStore, ColumnReorder, ColumnResizer){

					//Column Definitions
					var columns = {
						sku: {
							label: '<fmt:message key="sku" />',
							editor: 'text',
							sortable: false,
						},
						name: {
							label: '<fmt:message key="name" />',
							editor: 'text',
							sortable: false,
						},
						shortDesc: {
							label: '<fmt:message key="shortDesc" />',
							editor: 'text',
							sortable: false,
						},
						price1: {
							label: '<fmt:message key="price" />',
							editor: 'text',
							sortable: false,
						},
					};
					
					variants = new (declare([OnDemandGrid, Selection, Keyboard, Editor, ColumnReorder, ColumnResizer]))({
						collection: createSyncStore({ data: data }),
						columns: columns,
						selectionMode: "none",
					},'variants');
					
					if(data.length == 0)
						addVariant();
					
					variants.startup();
				});
	 		}
			
			initializeFormKitGrid();
			initializeVariantGrid();
		</script>
	</tiles:putAttribute>
</tiles:insertDefinition>