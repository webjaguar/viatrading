<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ taglib uri="http://www.springframework.org/security/tags" prefix="sec" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ taglib uri="/WEB-INF/tlds/spring.tld" prefix="spring" %>
<%@ taglib uri="/WEB-INF/tlds/custom.tld" prefix="wj" %>

<div id="tab_6" class="tab-pane" role="tabpanel">
	<c:forEach items="${productForm.product.priceLevelPrices}" var="priceLevelPrice" varStatus="plStatus">
	  	<div class="line">
		  	<div class="form-row">
			  	<div class="col-label">
			    		<label class="col-form-label">
						<fmt:message key="priceLevel" /> ${productForm.product.priceLevelPrices[plStatus.index].name}:
					</label>
				</div>
		        <div class="col-lg-4"> 
		        		<div class="form-group">
					    <form:input path="product.priceLevelPrices[${plStatus.index}].price" class="form-control" maxlength="10"/>
				    </div>
				</div>
			</div>
		</div>
	</c:forEach>
</div>