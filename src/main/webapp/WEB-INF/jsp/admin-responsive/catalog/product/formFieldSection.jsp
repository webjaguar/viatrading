<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ taglib uri="http://www.springframework.org/security/tags" prefix="sec" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ taglib uri="/WEB-INF/tlds/spring.tld" prefix="spring" %>
<%@ taglib uri="/WEB-INF/tlds/custom.tld" prefix="wj" %>

<div id="tab_3" class="tab-pane" role="tabpanel">	
    	<c:choose>
    		<c:when test="${gSiteConfig['gPRODUCT_FIELDS_UNLIMITED'] > 0}">
	  	</c:when>
	  	<c:otherwise>
	  		<c:forEach items="${productForm.productFields}" var="productField" varStatus="status">
		  		<div class="line">
			  		<div class="form-row">
				  		<div class="col-label">
					    		<label class="col-form-label">
					    			<c:if test="${siteConfig['DSI'].value != '' and siteConfig['CUSTOM_SHIPPING_FLAG'].value == productField.id}">
						        		<span class="fa fa-question-circle" data-toggle="tooltip" data-placement="top" title="Custom Shipping Flag::A value of '0' means this item has custom shipping."></span>
						       	</c:if>		
						       	<c:if test="${siteConfig['DSI'].value != '' and siteConfig['CUSTOM_SHIPPING_FIELD1'].value == productField.id}">
						       		<span class="fa fa-question-circle" data-toggle="tooltip" data-placement="top" title="<c:out value="${siteConfig['CUSTOM_SHIPPING_TITLE1'].value}" />::shipping cost"></span>
						       	</c:if>
						       	<c:if test="${siteConfig['DSI'].value != '' and siteConfig['CUSTOM_SHIPPING_FIELD2'].value == productField.id}">
						       		<span class="fa fa-question-circle" data-toggle="tooltip" data-placement="top" title="<c:out value="${siteConfig['CUSTOM_SHIPPING_TITLE2'].value}" />::shipping cost"></span>
						       	</c:if>
						       	<c:if test="${siteConfig['PRODUCT_INACTIVE_REDIRECT_FIELD'].value == productField.id}">
						       		<span class="fa fa-question-circle" data-toggle="tooltip" data-placement="top" title="Product Inactive Redirect URL::If this item is inactive then request will redirect to this URL."></span>
						       	</c:if>
								<c:out value="${productField.name}"/>
							</label>
						</div>
				        <div class="col-lg-4">
					        	<div class="form-group">
								<div class="input-group">
									<c:choose>
							     		<c:when test="${!empty productField.preValue and siteConfig['SEARCH_FIELDS_MULTIPLE'].value == 'true' and (productField.search == 'true' or productField.search2 == 'true')}">
								     		<form:input  path="product.field${productField.id}" class="form-control" maxlength="2080"/>
								     		<br/>
								     		<form:select path="product.field${productField.id}" class="productPreValueField form-control" multiple="multiple" onchange="showSelect(${productField.id},this.value)">
									        		<c:forTokens items="${productField.preValue}" delims="," var="dropDownValue">
									           		<form:option value="${fn:trim(dropDownValue)}" ><c:out value="${dropDownValue}"></c:out></form:option>
									         	</c:forTokens>
									         	<c:out value="${productField.values}"></c:out>
								       		</form:select>
								     	</c:when>
								     	<c:when test="${!empty productField.preValue and productField.search == 'false'}">
								       		<form:select path="product.field${productField.id}" class="productPreValueField form-control">
								         		<form:option value="" label="Please Select"/>
								         		<c:forTokens items="${productField.preValue}" delims="," var="dropDownValue">
								           			<form:option value="${fn:trim(dropDownValue)}" />
								         		</c:forTokens>
								       		</form:select>
								     	</c:when>
								     	<c:when test="${!empty productField.preValue}">
								       		<form:select path="product.field${productField.id}" class="productPreValueField form-control">
									        		<form:option value="" label="Please Select"/>
								         		<c:forTokens items="${productField.preValue}" delims="," var="dropDownValue">
								        		   		<form:option value="${fn:trim(dropDownValue)}" />
								         		</c:forTokens>
								       		</form:select>
								     	</c:when>
								     	<c:otherwise>
								       		<form:input path="product.field${productField.id}" class="form-control" maxlength="255"/>
								     	</c:otherwise>
								    </c:choose>
									<span class="input-group-addon">Field <c:out value="${productField.id}"/></span>
								</div>
							</div>
						</div>
					</div>
				</div>
    			</c:forEach>
    	  	</c:otherwise>
	</c:choose>
</div>