<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ taglib uri="http://www.springframework.org/security/tags" prefix="sec" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ taglib uri="/WEB-INF/tlds/spring.tld" prefix="spring" %>
<%@ taglib uri="/WEB-INF/tlds/custom.tld" prefix="wj" %>

<div id="tab_11" class="tab-pane" role="tabpanel">
	<div class="grid-stage">
		<div class="page-stat">
			<input type="hidden" name="_submit" id="__submit" value="deduct"/>
			<a onclick="addVariant()"><i class="sli-plus"></i> Add Variant</a>
		</div>
		<table id="variants"></table>
		<div class="footer">
			<div class="float-left">
				<p class="mb-0"><sup>1</sup> SKU needs to be unique per product.</p>
			   <p class="mb-0"><sup>2</sup> SKU is required to save variant in database.</p>
			   <p class="mb-0"><sup>3</sup> SKU, Name, and Price are required to show on front-end.</p>
			</div>
		</div>
	</div>
</div>