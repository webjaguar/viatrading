<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ taglib uri="http://www.springframework.org/security/tags" prefix="sec" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ taglib uri="/WEB-INF/tlds/spring.tld" prefix="spring" %>
<%@ taglib uri="/WEB-INF/tlds/custom.tld" prefix="wj" %>

<div id="tab_2" class="tab-pane" role="tabpanel">
	<div class="line">
		<div class="form-row">
	   		<div class="col-label">
	   			<label class="col-form-label">
	   				<fmt:message key="hide" /> <fmt:message key="Header" />
	   			</label>
			</div>
			<div class="col-lg-4"> 
				<div class="form-group">
					<label class="custom-control custom-radio">
						<form:radiobutton name="product.hideHeader" path="product.hideHeader" value="true" class="custom-control-input"/>
						<span class="custom-control-indicator"></span>
						<span class="custom-control-description"><fmt:message key="yes" /></span>
					</label>
					<label class="custom-control custom-radio">
						<form:radiobutton name="product.hideHeader" path="product.hideHeader" value="false" class="custom-control-input"/>
						<span class="custom-control-indicator"></span>
						<span class="custom-control-description"><fmt:message key="no" /></span>
					</label>
				</div>
			</div>
		</div>
	</div>
	<div class="line">
		<div class="form-row">
			<div class="col-label">
		   		<label class="col-form-label">
			   		<fmt:message key="hide" /> <fmt:message key="TopBar" />
				</label>
			</div>
			<div class="col-lg-4">
				<div class="form-group">
					<label class="custom-control custom-radio">
						<form:radiobutton name="product.hideTopBar" path="product.hideTopBar" value="true" class="custom-control-input"/>
						<span class="custom-control-indicator"></span>
						<span class="custom-control-description"><fmt:message key="yes" /></span>
					</label>
					<label class="custom-control custom-radio">
						<form:radiobutton name="product.hideTopBar" path="product.hideTopBar" value="false" class="custom-control-input"/>
						<span class="custom-control-indicator"></span>
						<span class="custom-control-description"><fmt:message key="no" /></span>
					</label>
				</div>
			</div>
		</div>
	</div>
	<div class="line">
		<div class="form-row">
			<div class="col-label">
				<label class="col-form-label">
					<fmt:message key="hide" /> <fmt:message key="LeftBar" />
				</label>
			</div>
			<div class="col-lg-4"> 
				<div class="form-group">
					<label class="custom-control custom-radio">
						<form:radiobutton name="product.hideLeftBar" path="product.hideLeftBar" value="true" class="custom-control-input"/>
						<span class="custom-control-indicator"></span>
						<span class="custom-control-description"><fmt:message key="yes" /></span>
					</label>
					<label class="custom-control custom-radio">
						<form:radiobutton name="product.hideLeftBar" path="product.hideLeftBar" value="false" class="custom-control-input"/>
						<span class="custom-control-indicator"></span>
						<span class="custom-control-description"><fmt:message key="no" /></span>
					</label>
				</div>
			</div>
		</div>
	</div>
	<div class="line">
		<div class="form-row">
			<div class="col-label">
				<label class="col-form-label">
					<fmt:message key="hide" /> <fmt:message key="RightBar" />
				</label>
			</div>
			<div class="col-lg-4">
				<div class="form-group">
					<label class="custom-control custom-radio">
						<form:radiobutton name="product.hideRightBar" path="product.hideRightBar" value="true" class="custom-control-input"/>
						<span class="custom-control-indicator"></span>
						<span class="custom-control-description"><fmt:message key="yes" /></span>
					</label>
					<label class="custom-control custom-radio">
						<form:radiobutton name="product.hideRightBar" path="product.hideRightBar" value="false" class="custom-control-input"/>
						<span class="custom-control-indicator"></span>
						<span class="custom-control-description"><fmt:message key="no" /></span>
					</label>
				</div>
			</div>
		</div>
	</div>
	<div class="line">
		<div class="form-row">
			<div class="col-label">
		   		<label class="col-form-label">
					<fmt:message key="hide" /> <fmt:message key="Footer" />
				</label>
			</div>
			<div class="col-lg-4">
				<div class="form-group">
					<label class="custom-control custom-radio">
						<form:radiobutton name="product.hideFooter" path="product.hideFooter" value="true" class="custom-control-input"/>
						<span class="custom-control-indicator"></span>
						<span class="custom-control-description"><fmt:message key="yes" /></span>
					</label>
					<label class="custom-control custom-radio">
						<form:radiobutton name="product.hideFooter" path="product.hideFooter" value="false" class="custom-control-input"/>
						<span class="custom-control-indicator"></span>
						<span class="custom-control-description"><fmt:message key="no" /></span>
					</label>
				</div>
			</div>
		</div>
	</div>
	<div class="line">
		<div class="form-row">
			<div class="col-label">
				<label class="col-form-label">
					<fmt:message key="hide" /> <fmt:message key="breadCrumbs" />
				</label>
			</div>
			<div class="col-lg-4">
				<div class="form-group">
					<label class="custom-control custom-radio">
						<form:radiobutton name="product.hideBreadCrumbs" path="product.hideBreadCrumbs" value="true" class="custom-control-input"/>
						<span class="custom-control-indicator"></span>
						<span class="custom-control-description"><fmt:message key="yes" /></span>
					</label>
					<label class="custom-control custom-radio">
						<form:radiobutton name="product.hideBreadCrumbs" path="product.hideBreadCrumbs" value="false" class="custom-control-input"/>
						<span class="custom-control-indicator"></span>
						<span class="custom-control-description"><fmt:message key="no" /></span>
					</label>
				</div>
			</div>
		</div>
	</div>
	<div class="line">
		<div class="form-row">
			<div class="col-label">
				<label class="col-form-label">
					<span class="fa fa-question-circle" data-toggle="tooltip" data-placement="top" title="Product Layout::Select the front end or the webpage layout."></span>
					<fmt:message key="productLayout" />
				</label>
			</div>
			<div class="col-lg-4"> 
				<form:select path="product.productLayout" id="product_productLayout" class="custom-select">
					<form:option value=""><fmt:message key="global"/></form:option>
					<c:forTokens items="${siteConfig['PRODUCT_DETAIL_LAYOUT'].value}" delims="," var="option" varStatus="status">
						<form:option value="${option}"><fmt:message key="${option}"/></form:option>
					</c:forTokens>
				</form:select>
			</div>
		</div>
	</div>
	<div class="line">
		<div class="form-row">
			<div class="col-label">
				<label class="col-form-label">
					<span class="fa fa-question-circle" data-toggle="tooltip" data-placement="top" title="Head Tag::Add your custom Head Tag HTML code here. Ths will override your Default Head Tag."></span>
					<fmt:message key="headTag" />
				</label>
			</div>
			<div class="col-lg-4"> 
				<form:textarea path="product.headTag" id="product_headTag" class="form-control" rows="8"/>
			</div>
		</div>
	</div>
	<div class="line">
		<div class="form-row">
			<div class="col-label">
				<label class="col-form-label">
					<span class="fa fa-question-circle" data-toggle="tooltip" data-placement="top" title="HTML Add To Cart::Please enter code for custom or different, add to cart form, here."></span>
					<fmt:message key="htmlAddToCart" />
				</label>
			</div>
			<div class="col-lg-8">
				<div class="editor_wrapper" data-editor="product_htmlAddToCart">
					<form:textarea class="code-editor" rows="8" path="product.htmlAddToCart" id="product_htmlAddToCart"/>
				</div>
			</div>
		</div>
	</div>
</div>