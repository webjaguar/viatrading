<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ taglib uri="http://www.springframework.org/security/tags" prefix="sec" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ taglib uri="/WEB-INF/tlds/spring.tld" prefix="spring" %>
<%@ taglib uri="/WEB-INF/tlds/custom.tld" prefix="wj" %>

<div id="tab_12" class="tab-pane" role="tabpanel">
	<div class="line">
		<div class="form-row">
			<div class="col-label">
			   	<label for="product_subscription" class="col-form-label">
					<fmt:message key="Enabled" />
				</label>
			</div>
			<div class="col-lg-4"> 
				<label class="custom-control custom-checkbox">
	                <form:checkbox path="product.subscription" id="product_subscription" class="custom-control-input" />
	                <span class="custom-control-indicator"></span>
	            </label>
			</div>
		</div>
	</div>
	<c:if test="${customerForm.protectedAccess != null}">
		<div class="line">
			<div class="form-row">
				<div class="col-label">
				   	<label class="col-form-label">
						<fmt:message key="protectedAccess" />
					</label>
				</div>
				<div class="col-lg-4"> 
					<ul class="control">
		       			<c:forEach items="${customerForm.protectedAccess}" var="protectedAccess" varStatus="protectedStatus">
		  	  	    			<c:set var="key" value="protected${protectedStatus.index+1}"/>
					    		<c:choose>
					      		<c:when test="${labels[key] != null and labels[key] != ''}">
					        			<c:set var="label" value="${labels[key]}"/>
					      		</c:when>
					      		<c:otherwise>
					      			<c:set var="label"> ${protectedStatus.index+1}</c:set>
					      		</c:otherwise>
					    		</c:choose>		        
				        		<li>
				        			<label class="custom-control custom-checkbox">
					                <input type="checkbox" name="__enabled_${protectedStatus.index}" class="custom-control-input" <c:if test="${protectedAccess}">checked</c:if> />
					                <span class="custom-control-indicator"></span>
					                <span class="custom-control-description">${label}</span>
					            </label>
				        		</li>
			        		</c:forEach>
			        </ul>
				</div>
			</div>
		</div>
    </c:if>
    <div class="line">
	 	<div class="form-row">
	 		<div class="col-label">
			   	<label class="col-form-label">
					<fmt:message key="frequency" />
				</label>
			</div>
			<div class="col-lg-4"> 
				<ul class="control">
	       			<c:forEach items="${productForm.autoShipFreq}" var="autoShip" varStatus="autoShipStatus">
	  	  	    			<c:set var="key" value="shipment${autoShipStatus.index+1}"/>
				  		<li>
				  			<label class="custom-control custom-checkbox">
					  			<input type="checkbox" name="__autoship_freq${autoShipStatus.index}" class="custom-control-input" <c:if test="${autoShip}">checked</c:if> />
					  			<span class="custom-control-indicator"></span>
					            <span class="custom-control-description"><fmt:message key="autoShip${autoShipStatus.index}"/></span>
				            </label>
				  		</li>
		        		</c:forEach>
		        </ul>
			</div>
		</div>
	</div>
	<div class="line">
		<div class="form-row">
			<div class="col-label">
			   	<label class="col-form-label">
			   		<span class="fa fa-question-circle" data-toggle="tooltip" data-placement="top" title="Note::Discount will only apply if price of item didn't come from price table or special pricing. If item has a valid sales tag, discount will be applied to the discounted price."></span>
					<fmt:message key='discount' />
				</label>
			</div>
			<div class="col-lg-8">
				<div class="row">
					<div class="col-lg-3">
						<form:input path="product.subscriptionDiscount" class="form-control"/>
					</div>
					<div class="col-lg-2">
						<form:select path="product.subscriptionType" class="custom-select">
				        		<form:option value="0"> %</form:option> 
				        		<form:option value="1"> $</form:option>
			 			</form:select>
					</div>
	 			</div> 
			</div>
		</div>
	</div>
</div>