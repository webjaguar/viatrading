<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ taglib uri="http://www.springframework.org/security/tags" prefix="sec" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ taglib uri="/WEB-INF/tlds/spring.tld" prefix="spring" %>
<%@ taglib uri="/WEB-INF/tlds/custom.tld" prefix="wj" %>
	
<div id="tab_13" class="tab-pane" role="tabpanel">
	<div class="line">
		<div class="form-row">
			<div class="col-label">
		    		<label for="product_screenPrintImprintId" class="col-form-label">
		    			<span class="fa fa-question-circle" data-toggle="tooltip" data-placement="top" title="Screen Print Imprint::Select Imprint Method from the drop down and it will decide the flow on product configurator."></span>
					<fmt:message key="screenPrintImprint" />
				</label>
			</div>
	        <div class="col-lg-4"> 
				<form:select path="product.screenPrintImprintId" id="product_screenPrintImprintId" class="custom-select" >
		        		<form:option value="">Please Select</form:option>
		          	<c:forEach items="${imprintMethodList}" var="imprint">
		            		<c:if test="${imprint.type == 'Screen Print'}">
		            			<form:option value="${imprint.id}">${imprint.name}</form:option>
		            		</c:if>
		          	</c:forEach>
		        </form:select>
			</div>
		</div>
	</div>
	<div class="line">
		<div class="form-row">
			<div class="col-label">
		    		<label for="product_padPrintImprintId" class="col-form-label">
		    			<span class="fa fa-question-circle" data-toggle="tooltip" data-placement="top" title="Pad Print::Select Imprint Method from the drop down and it will decide the flow on product configurator."></span>
					Pad Print
				</label>
			</div>
	        <div class="col-lg-4"> 
				<form:select path="product.padPrintImprintId" id="product_padPrintImprintId" class="custom-select">
		        		<form:option value="">Please Select</form:option>
	          		<c:forEach items="${imprintMethodList}" var="imprint">
	            			<c:if test="${imprint.type == 'Pad Print'}">
	          				<form:option value="${imprint.id}">${imprint.name}</form:option>
	            			</c:if>
	          		</c:forEach>
		        </form:select>
			</div>
		</div>
	</div>
	<div class="line">
		<div class="form-row">
			<div class="col-label">
		    		<label for="product_fullColorImprintId" class="col-form-label">
					<span class="fa fa-question-circle" data-toggle="tooltip" data-placement="top" title="Full Color Imprint::Select Imprint Method from the drop down and it will decide the flow on product configurator."></span>
					<fmt:message key="fullColorImprint" />
				</label>
			</div>
	        <div class="col-lg-4"> 
				<form:select path="product.fullColorImprintId" id="product_fullColorImprintId" class="custom-select" >
		        		<form:option value="">Please Select</form:option>
		          	<c:forEach items="${imprintMethodList}" var="imprint">
		            		<c:if test="${imprint.type == 'Full Color'}">
		          			<form:option value="${imprint.id}">${imprint.name}</form:option>
		            		</c:if>
		          	</c:forEach>
		        </form:select>
			</div>
		</div>
	</div>
	<div class="line">
		<div class="form-row">
			<div class="col-label">
		    		<label for="product_embroideryImprintId" class="col-form-label">
		    			<span class="fa fa-question-circle" data-toggle="tooltip" data-placement="top" title="Embroidery Imprint::Select Imprint Method from the drop down and it will decide the flow on product configurator."></span>
					<fmt:message key="embroideryImprint" />
				</label>
			</div>
	        <div class="col-lg-4"> 
				<form:select path="product.embroideryImprintId" id="product_embroideryImprintId" class="custom-select">
			    		<form:option value="">Please Select</form:option>
		          	<c:forEach items="${imprintMethodList}" var="imprint">
		            		<c:if test="${imprint.type == 'Embroidery'}">
		          			<form:option value="${imprint.id}">${imprint.name}</form:option>
		            		</c:if>
		          	</c:forEach>
		        </form:select>
			</div>
		</div>
	</div>
	<div class="line">
		<div class="form-row">
			<div class="col-label">
		    		<label for="product_laserEngravingImprintId" class="col-form-label">
					<span class="fa fa-question-circle" data-toggle="tooltip" data-placement="top" title="Laser engraving Imprint::Select Imprint Method from the drop down and it will decide the flow on product configurator."></span>
					<fmt:message key="laserEngravingImprint" />
				</label>
			</div>
	        <div class="col-lg-4"> 
				<form:select path="product.laserEngravingImprintId" id="product_laserEngravingImprintId" class="custom-select">
			    		<form:option value="">Please Select</form:option>
			        		<c:forEach items="${imprintMethodList}" var="imprint">
				            	<c:if test="${imprint.type == 'Laser Engraving'}">
				          		<form:option value="${imprint.id}">${imprint.name}</form:option>
				            </c:if>
			         	</c:forEach>
	        		</form:select>
			</div>
		</div>
	</div>
	<div class="line">
		<div class="form-row">
			<div class="col-label">
		    		<label for="product_debossImprintId" class="col-form-label">
		    			<span class="fa fa-question-circle" data-toggle="tooltip" data-placement="top" title="Deboss Imprint::Select Imprint Method from the drop down and it will decide the flow on product configurator."></span>
					<fmt:message key="debossImprint" />
				</label>
			</div>
	        <div class="col-lg-4">
				<form:select path="product.debossImprintId" id="product_debossImprintId" class="custom-select">
		        		<form:option value="">Please Select</form:option>
		          	<c:forEach items="${imprintMethodList}" var="imprint">
		            		<c:if test="${imprint.type == 'Deboss'}">
		          			<form:option value="${imprint.id}">${imprint.name}</form:option>
		            		</c:if>
		          	</c:forEach>
		        </form:select>
			</div>
		</div>
	</div>
	<div class="line">
		<div class="form-row">
			<div class="col-label">
		    		<label for="product_embossImprintId" class="col-form-label">
					<span class="fa fa-question-circle" data-toggle="tooltip" data-placement="top" title="Emboss Imprint::Select Imprint Method from the drop down and it will decide the flow on product configurator."></span>
					<fmt:message key="embossImprint" />
				</label>
			</div>
	        <div class="col-lg-4"> 
				<form:select path="product.embossImprintId" id="product_embossImprintId" class="custom-select" >
			    		<form:option value="">Please Select</form:option>
		        		<c:forEach items="${imprintMethodList}" var="imprint">
		            		<c:if test="${imprint.type == 'Emboss'}">
		          			<form:option value="${imprint.id}">${imprint.name}</form:option>
		            		</c:if>
		          	</c:forEach>
		        	</form:select>
			</div>
		</div>
	</div>
	<div class="line">
		<div class="form-row">
	    		<div class="col-label">
		    		<label for="product_heatTransferImprintId" class="col-form-label">
		    			<span class="fa fa-question-circle" data-toggle="tooltip" data-placement="top" title="Heat Transfer Imprint::Select Imprint Method from the drop down and it will decide the flow on product configurator."></span>
					<fmt:message key="heatTransferImprint" />:
				</label>
			</div>
	        <div class="col-lg-4"> 
				<form:select path="product.heatTransferImprintId" id="product_heatTransferImprintId" class="custom-select">
		        		<form:option value="">Please Select</form:option>
		          	<c:forEach items="${imprintMethodList}" var="imprint">
		            		<c:if test="${imprint.type == 'Heat Transfer'}">
		          			<form:option value="${imprint.id}">${imprint.name}</form:option>
		            		</c:if>
		          	</c:forEach>
		        </form:select>
			</div>
		</div>
	</div>
	<div class="line">
		<div class="form-row">
			<div class="col-label">
		    		<label for="product_etchingImprintId" class="col-form-label">
					<span class="fa fa-question-circle" data-toggle="tooltip" data-placement="top" title="Etching Imprint::Select Imprint Method from the drop down and it will decide the flow on product configurator."></span>
					<fmt:message key="etchingImprint" />
				</label>
			</div>
	        <div class="col-lg-4"> 
				<form:select path="product.etchingImprintId" id="product_etchingImprintId" class="custom-select">
		        		<form:option value="">Please Select</form:option>
			        <c:forEach items="${imprintMethodList}" var="imprint">
			            <c:if test="${imprint.type == 'Etching'}">
			          		<form:option value="${imprint.id}">${imprint.name}</form:option>
			            </c:if>
			        </c:forEach>
		        </form:select>
			</div>
		</div>
	</div>
	<div class="line">
		<div class="form-row">
			<div class="col-label">
		    		<label for="product_hotStampImprintId" class="col-form-label">
		    			<span class="fa fa-question-circle" data-toggle="tooltip" data-placement="top" title="Hot Stamp Imprint::Select Imprint Method from the drop down and it will decide the flow on product configurator."></span>
					<fmt:message key="hotStampImprint" />
				</label>
			</div>
	        <div class="col-lg-4"> 
				<form:select path="product.hotStampImprintId" id="product_hotStampImprintId" class="custom-select">
		        		<form:option value="">Please Select</form:option>
			        <c:forEach items="${imprintMethodList}" var="imprint">
			            <c:if test="${imprint.type == 'Hot Stamp'}">
			          		<form:option value="${imprint.id}">${imprint.name}</form:option>
			            </c:if>
			        </c:forEach>
		        </form:select>
			</div>
		</div>
	</div>
	<div class="line">
		<div class="form-row">
			<div class="col-label">
		    		<label for="product_blankSkuImprintId" class="col-form-label">
					<span class="fa fa-question-circle" data-toggle="tooltip" data-placement="top" title="Blank Imprint::Select Imprint Method from the drop down and it will decide the flow on product configurator."></span>
					<fmt:message key="blankImprint" />
				</label>
			</div>
	        <div class="col-lg-4"> 
				<form:select path="product.blankSkuImprintId" id="product_blankSkuImprintId" class="custom-select">
	            		<form:option value="">Please Select</form:option>
				    <c:forEach items="${imprintMethodList}" var="imprint">
			            <c:if test="${imprint.type == 'Blank'}">
			          		<form:option value="${imprint.id}">${imprint.name}</form:option>
			            </c:if>
		            </c:forEach>
		        </form:select>
			</div>
		</div>
	</div>
	<div class="line">
		<div class="form-row">
			<div class="col-label">
		    		<label for="product_sampleSku" class="col-form-label">
					<span class="fa fa-question-circle" data-toggle="tooltip" data-placement="top" title="Sample Sku::This Sku will decide the flow on product configurator."></span>
					<fmt:message key="sampleSku" />
				</label>
			</div>
	        <div class="col-lg-4"> 
				<form:input path="product.sampleSku" id="product_sampleSku" class="form-control" />
			</div>
		</div>
	</div>
	<div class="line">
		<div class="form-row">
			<div class="col-label">
		    		<label for="product_blankDec" class="col-form-label">
					<span class="fa fa-question-circle" data-toggle="tooltip" data-placement="top" title="Blank Sku::This Sku will decide the flow on product configurator."></span>
					<fmt:message key="blank" />
				</label>
			</div>
	        <div class="col-lg-4"> 
				<label class="custom-control custom-checkbox">
	                <form:checkbox path="product.blankDec" id="product_blankDec" class="custom-control-input"/>
	                <span class="custom-control-indicator"></span>
	            </label>
			</div>
		</div>
	</div>
</div>