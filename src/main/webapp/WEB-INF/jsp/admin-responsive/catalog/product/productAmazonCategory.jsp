<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ taglib uri="http://www.springframework.org/security/tags" prefix="sec" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ taglib uri="/WEB-INF/tlds/spring.tld" prefix="spring" %>
<%@ taglib uri="/WEB-INF/tlds/custom.tld" prefix="wj" %>

<div id="tab_19" class="tab-pane" role="tabpanel">
	<div class="line">
		<div class="form-row">
			<div class="col-label">
		    		<label for="product_amazonCategoryId" class="col-form-label">
					<fmt:message key="category" /> 1:
				</label>
			</div>
	        <div class="col-lg-4"> 
				<form:select path="product.amazonCategoryId" id="product_amazonCategoryId" class="form-control form-control-sm"> 
			   		<form:option value="">Please Select</form:option>
					<c:forEach items="${amazonCategoryList}" var="amz" varStatus="status">
						<form:option value="${amz.browseId}"> ${amz.category} </form:option>
					</c:forEach>
				</form:select>
			</div>
		</div>
	</div>
	<div class="line">
		<div class="form-row">
			<div class="col-label">
		    		<label for="product_amazonSubCategoryId" class="col-form-label">
					<fmt:message key="subCategory" /> 1:
				</label>
			</div>
	        <div class="col-lg-4"> 
				<form:select path="product.amazonSubCategoryId" id="product_amazonSubCategoryId" class="form-control form-control-sm">
		 	       <form:option value="">Please Select</form:option>
		 	    </form:select>
			</div>
		</div>
	</div>
	<div class="line">
		<div class="form-row">
			<div class="col-label">
		    		<label for="product_amazonCategoryId1" class="col-form-label">
					<fmt:message key="category" /> 2:
				</label>
			</div>
	        <div class="col-lg-4"> 
				<form:select path="product.amazonCategoryId1" id="product_amazonCategoryId1" class="form-control form-control-sm"> 
		   			<form:option value="">Please Select</form:option>
					<c:forEach items="${amazonCategoryList}" var="amz" varStatus="status">
						<form:option value="${amz.browseId}"> ${amz.category} </form:option>
					</c:forEach>
				</form:select>
			</div>
		</div>
	</div>
	<div class="line">
		<div class="form-row">
	    		<label for="product_amazonSubCategoryId1" class="col-sm-3 col-md-3 col-lg-3 col-form-label text-sm-right">
				<fmt:message key="subCategory" /> 2:
			</label>
	        <div class="col-sm-9 col-md-9 col-lg-9"> 
				<form:select path="product.amazonSubCategoryId1" id="product_amazonSubCategoryId1" class="form-control form-control-sm">
		 	    		<form:option value="">Please Select</form:option>
		 	    </form:select>
			</div>
		</div>
	</div>
</div>