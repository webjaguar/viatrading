<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ taglib uri="http://www.springframework.org/security/tags" prefix="sec" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ taglib uri="/WEB-INF/tlds/spring.tld" prefix="spring" %>
<%@ taglib uri="/WEB-INF/tlds/custom.tld" prefix="wj" %>

  <c:forEach items="${languageCodes}" var="i18n" varStatus="loop">
	<div id="tab_lan_${loop.index}" class="tab-pane" role="tabpanel">
		<div class="line">
			<div class="form-row">
				<div class="col-label">
			    		<label class="col-form-label">
						<fmt:message key="productName" />
					</label>
				</div>
		        <div class="col-lg-4"> 
					<input type="text" name="__i18nName_${i18n.languageCode}" value="<c:out value="${productForm.product.i18nProduct[i18n.languageCode].i18nName}"/>" class="form-control" maxlength="255">
				</div>
			</div>
		</div>
		<div class="line">
			<div class="form-row">
				<div class="col-label">
			    		<label class="col-form-label">
						<fmt:message key="productShortDesc" />
					</label>
				</div>
		        <div class="col-lg-4"> 
					<input type="text" name="__i18nShortDesc_${i18n.languageCode}" value="<c:out value="${productForm.product.i18nProduct[i18n.languageCode].i18nShortDesc}"/>" class="form-control" maxlength="512">
				</div>
			</div>
		</div>
		<div class="line">
			<div class="form-row">
				<div class="col-label">
			    		<label class="col-form-label">
						<fmt:message key="productLongDesc" />
					</label>
				</div>
		        <div class="col-lg-8"> 
		   			<c:choose>
			  			<c:when test="${siteConfig['DETAILS_LONG_DESC_HTML'].value != 'false'}">
			  				<div class="code-editor" data-editor="__i18nLongDesc_${i18n.languageCode}">
		                     	<textarea name="__i18nLongDesc_${i18n.languageCode}" id="__i18nLongDesc_${i18n.languageCode}"><c:out value="${productForm.product.i18nProduct[i18n.languageCode].i18nLongDesc}"/></textarea>
		                     </div>
				      	</c:when>
			  		  	<c:otherwise>
					    		<textarea name="__i18nLongDesc_${i18n.languageCode}" id="__i18nLongDesc_${i18n.languageCode}" class="form-control" rows="8"><c:out value="${productForm.product.i18nProduct[i18n.languageCode].i18nLongDesc}"/></textarea>
				      	</c:otherwise>
			  		</c:choose>
				</div>
			</div>
		</div>
		<c:forEach items="${productForm.productFields}" var="productField" varStatus="status"> 
			<div class="line">
				<div class="form-row">
					<div class="col-label">
				    		<label class="col-form-label">
							<c:out value="${i18nProductFields[i18n.languageCode][productField.id].name}"/> &nbsp; (Field <c:out value="${productField.id}"/>)
						</label>
					</div>
			        <div class="col-lg-4">
	                		<c:set var="i18nField" value="field${productField.id}"/>
				  	    <c:choose>
					    		<c:when test="${!empty i18nProductFields[i18n.languageCode][productField.id].preValue}">
					       		<select name="__i18nProductField${productField.id}_${i18n.languageCode}" class="custom-select">
					         		<option value="">Please Select</option>
					         		<c:forTokens items="${i18nProductFields[i18n.languageCode][productField.id].preValue}" delims="," var="dropDownValue">
					           			<option value="${dropDownValue}" <c:if test="${dropDownValue == productForm.product.i18nProduct[i18n.languageCode][i18nField]}">selected</c:if>>${dropDownValue}</option>
					         		</c:forTokens>
					       		</select>
					     	</c:when>
					     	<c:otherwise>
					  	  		<input type="text" name="__i18nProductField${productField.id}_${i18n.languageCode}" value="<c:out value="${productForm.product.i18nProduct[i18n.languageCode][i18nField]}"/>" class="form-control" maxlength="255">
					     	</c:otherwise>
					    </c:choose>
					</div>
				</div>
			</div>
    		</c:forEach>	
	</div>	
</c:forEach>