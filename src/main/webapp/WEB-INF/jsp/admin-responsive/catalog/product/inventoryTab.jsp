<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ taglib uri="http://www.springframework.org/security/tags" prefix="sec" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ taglib uri="/WEB-INF/tlds/spring.tld" prefix="spring" %>
<%@ taglib uri="/WEB-INF/tlds/custom.tld" prefix="wj" %>

<div id="tab_7" class="tab-pane" role="tabpanel">
	<c:if test="${siteConfig['PO_EXPECTED_DELIVERY_DATE'].value == 'true'}">
		<div class="line">
			<div class="form-row">
				<div class="col-label">
			    		<label class="col-form-label">
			    			<span class="fa fa-question-circle" data-toggle="tooltip" data-placement="top" title="Note::Add more product inventory through Purchase Order."></span>
						Expected Delivery Date
					</label>
				</div>
		        <div class="col-lg-4"> 
					<label class="text"><fmt:formatDate type="date" value="${productForm.product.expectedDeliveryDate}" timeZone="${siteConfig['SITE_TIME_ZONE'].value}" pattern="M/dd/yy"/></label>
				</div>
			</div>
		</div>
		<div class="line">
			<div class="form-row">
				<div class="col-label">
			    		<label class="col-form-label">
			    			<span class="fa fa-question-circle" data-toggle="tooltip" data-placement="top" title="Note::Add more product inventory through Purchase Order"></span>
						Expected Delivery Quantity
					</label>
				</div>
		        <div class="col-lg-4">
		        		<label class="text"><c:out value="${productForm.product.expectedDeliveryQty}" /></label>
				</div>
			</div>
		</div>
	</c:if>
	<div class="line">
		<div class="form-row">
			 <div class="col-label"> 
		    		<label class="col-form-label">
		    			<span class="fa fa-question-circle" data-toggle="tooltip" data-placement="top" title="Note::Add more product inventory through Purchase Order"></span>
					<fmt:message key="available_for_sale" />
				</label>
			</div>
	        <div class="col-lg-4"> 
	        		<label class="text"><c:out value="${productForm.product.inventoryAFS}" /></label>
			</div>
		</div>
	</div>
	<div class="line">
		<div class="form-row">
			<div class="col-label">
		    		<label for="product_inventory" class="col-form-label">
		    			<span class="fa fa-question-circle" data-toggle="tooltip" data-placement="top" title="Note::Add more product inventory through Purchase Order"></span>
					<fmt:message key="inventory_onhand" />
				</label>
			</div>
	        <div class="col-lg-4"> 
	        		<c:choose>
	  				<c:when test="${productForm.newProduct or (siteConfig['INVENTORY_UPDATE_FORM'].value == 'true' && siteConfig['INVENTORY_HISTORY'].value != 'true')}">
	  					<form:input path="product.inventory" id="product_inventory" class="form-control" maxlength="10" />
			    			<small class="help-text">
				  			Initial value will be the same for both Available for Sale and Inventory on hand.
					 	</small>
	  				</c:when>
	  				<c:otherwise>
	  					<label class="text"><c:out value="${productForm.product.inventory}" /></label>
	  				</c:otherwise>
	  			</c:choose>
			</div>
		</div>
	</div>
	<div class="line">
		<div class="form-row">
			<div class="col-label">
		    		<label for="product_inegInventory" class="col-form-label">
		    			<span class="fa fa-question-circle" data-toggle="tooltip" data-placement="top" title="Note::Allows customer to buy this product even if it is out of stock. Always product appears on fronEnd."></span>
					<fmt:message key="allowToBuyNegInventory" />
				</label>
			</div>
	        <div class="col-lg-4">
	        		<label class="custom-control custom-checkbox">
	        			<form:checkbox path="product.negInventory" id="product_inegInventory" class="custom-control-input"/>
					<span class="custom-control-indicator"></span>
	             </label>
			</div>
		</div>
	</div>
	<div class="line">
		<div class="form-row">
			<div class="col-label">
		    		<label for="product_showNegInventory" class="col-form-label">
		    			<span class="fa fa-question-circle" data-toggle="tooltip" data-placement="top" title="Note::Product will be displayed on front end even if inventory is negative. You can use Out of Stock message. SiteInfo -> Inventory Options."></span>
					<fmt:message key="showNegInventory" />
				</label>
			</div>
	        <div class="col-lg-4">
	        		<label class="custom-control custom-checkbox">
					<form:checkbox path="product.showNegInventory" id="product_showNegInventory" class="custom-control-input"/>
					<span class="custom-control-indicator"></span>
				</label>
			</div>
		</div>
	</div>
	<div class="line">
		<div class="form-row">
			<div class="col-label">
		    		<label for="product_lowInventory" class="col-form-label">
		    			<span class="fa fa-question-circle" data-toggle="tooltip" data-placement="top" title="Note::If the Inventory of this product falls bellow this number then it will be shown on Low Inventory List. Add Inventory using Purchase Order."></span>
					<fmt:message key="lowInventoryList" />
				</label>
			</div>
	        <div class="col-lg-4">
	        		<form:input path="product.lowInventory" id="product_lowInventory" class="form-control" maxlength="10" />
			</div>
		</div>
	</div>
	<div class="line">
		<div class="form-row">
			<div class="col-label">
		    		<label for="product_desiredInventory" class="col-form-label">
		    			<span class="fa fa-question-circle" data-toggle="tooltip" data-placement="top" title="Note::Desired inventory"></span>
					<fmt:message key="desiredInventory" />
				</label>
			</div>
	        <div class="col-lg-4">
	        		<form:input path="product.desiredInventory" id="product_desiredInventory" class="form-control" maxlength="10" />
			</div>
		</div>
	</div>
	<div class="line">
		<div class="form-row">
			<div class="col-label">
		    		<label for="product_inventoryItem" class="col-form-label">
		    			<span class="fa fa-question-circle" data-toggle="tooltip" data-placement="top" title="Note::"></span>
					<fmt:message key="inventoryItem" />
				</label>
			</div>
	        <div class="col-lg-4">
	        		<label class="custom-control custom-checkbox">
	        			<form:checkbox path="product.inventoryItem" id="product_inventoryItem" class="custom-control-input"/>
	                 <span class="custom-control-indicator"></span>
	             </label>  
			</div>
		</div>
	</div>
	<c:if test="${siteConfig['WAREHOUSE'].value == 'true'}">
		<div class="line">
			<div class="form-row">
				<div class="col-label">
			    		<label for="product_inventoryItem" class="col-form-label">
			    			<span class="fa fa-question-circle" data-toggle="tooltip" data-placement="top" title="Note::Default location for receiving inventory on PO"></span>
						Primary Location
					</label>
				</div>
		        <div class="col-lg-8">
		        		<div class="row">
	      				<div class="col-lg-6">
		        				<label>
			        				Warehouse:
			        			</label>
	       					<form:select path="product.primaryLocation.warehouse_id" class="custom-select">
				  				<option	value="">Please Select Warehouse</option>
				  				<c:forEach items="${warehouseList}" var="warehouse">
									<option value="${warehouse.id}" <c:if test="${productForm.product.primaryLocation.warehouse_id == warehouse.id}">selected="true"</c:if>>${warehouse.name}</option>
								</c:forEach>
				  			</form:select>
	      				</div>
      				</div>
      				<div class="row">
	      				<div class="col-lg-3">
	       	        			<label>
								<fmt:message key="zone" />:
							</label>
							<form:select path="product.primaryLocation.warehouseZone" class="custom-select">
				  				<option	value=""></option>
				  				<c:forEach items="${zoneList}" var="zone">
									<option value="${zone}" <c:if test="${productForm.product.primaryLocation.warehouseZone == zone}">selected="true"</c:if>>${zone}</option>
								</c:forEach>
				  			</form:select>
						</div>
	      				<div class="col-lg-3">
		        				<label>
			        				<fmt:message key="aisle" />:
			        			</label>
	       					<form:select path="product.primaryLocation.aisle" class="custom-select">
				  				<option	value=""></option>
				  				<c:forEach items="${aisleList}" var="aisle">
									<option value="${aisle}" <c:if test="${productForm.product.primaryLocation.aisle == aisle}">selected="true"</c:if>>${aisle}</option>
								</c:forEach>
				  			</form:select>
	      				</div>
	      				<div class="col-lg-3">
	       	        			<label>
								<fmt:message key="bin" />:
							</label>
							<form:select path="product.primaryLocation.bin" class="custom-select">
				  				<option	value=""></option>
				  				<c:forEach items="${binList}" var="bin">
									<option value="${bin}" <c:if test="${productForm.product.primaryLocation.bin == bin}">selected="true"</c:if>>${bin}</option>
								</c:forEach>
				  			</form:select>
						</div>
	      				<div class="col-lg-3">
	       	        			<label>
								<fmt:message key="level" />:
							</label>
							<form:select path="product.primaryLocation.level" class="custom-select">
			  					<option	value=""></option>
				  				<c:forEach items="${levelList}" var="level">
									<option value="${level}" <c:if test="${productForm.product.primaryLocation.level == level}">selected="true"</c:if>>${level}</option>
								</c:forEach>
				  			</form:select>
						</div>
			        	</div>
				</div>
			</div>
		</div>
		<div class="line">
			<div class="form-row">
				<div class="col-label">
			    		<label for="product_locationMinAmount" class="col-form-label">
			    			<span class="fa fa-question-circle" data-toggle="tooltip" data-placement="top" title="Note::"></span>
						Minimum Amount
					</label>
				</div>
		        <div class="col-lg-4">
		        		<form:input type="text" id="product_locationMinAmount" path="product.locationMinAmount" class="form-control"/>  
				</div>
			</div>
		</div>
		<div class="line">
			<div class="form-row">
				<div class="col-label">
			    		<label for="product_locationDesiredAmount" class="col-form-label">
			    			<span class="fa fa-question-circle" data-toggle="tooltip" data-placement="top" title="Note::"></span>
						Desired Amount
					</label>
				</div>
		        <div class="col-lg-4">
		        		<form:input type="text" id="product_locationDesiredAmount" path="product.locationDesiredAmount" class="form-control"/>  
				</div>
			</div>
		</div>
  	</c:if>
</div>