<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ taglib uri="http://www.springframework.org/security/tags" prefix="sec" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ taglib uri="/WEB-INF/tlds/spring.tld" prefix="spring" %>
<%@ taglib uri="/WEB-INF/tlds/custom.tld" prefix="wj" %>

<tiles:insertDefinition name="admin.responsive-template.catalog.product.form" flush="true">
	<tiles:putAttribute name="css">
		<style>
			#grid .dgrid-cell{
			    width: 100px;
			    text-align: center;
			}
			#grid .field-field{
			    text-align: left;
			}
			#grid .field-rank{
			    text-align: right;
			}
			#grid .field-rank .dgrid-input{
			    text-align: right;
			}
			#grid .field-name{
			    width: 200px;
			}
			#grid .field-groupName{
			    width: 200px;
			}
			#grid .field-preValue{
			    width: 200px;
			}
			
			.dgrid.grid_lan .dgrid-cell{
				width: 100px;
			    text-align: center;
			}
			.dgrid.grid_lan .field-field{
			    text-align: left;
			}
			.dgrid.grid_lan .field-name{
			    width: 200px;
			}
			.dgrid.grid_lan .field-preValue{
			    width: 200px;
			}
		</style>
	</tiles:putAttribute>
	<tiles:putAttribute name="content" type="string">
		<form method="post" name="productFields" class="page-form">
			<div class="page-banner">
				<div class="breadcrumb"><span><a href="${_contextpath}/admin/catalog/dashboard.jhtm">Home</a></span><i class="sli-arrow-right"></i><span><a href="${_contextpath}/admin/catalog/productList.jhtm">Products</a></span><i class="sli-arrow-right"></i><span>Fields</span></div>
			</div>
			<div class="page-head">
				<div class="row justify-content-between">
					<div class="col-sm-8 title">
						<h3><fmt:message key="productFields"/></h3>
					</div>
				</div>
			</div>
			<div class="page-body">
				<div class="main-content">
					<div class="form-section">
						<div class="content">
				            <ul class="nav nav-tabs" role="tablist">
				                <li class="nav-item">
				                    <a class="nav-link active" data-toggle="tab" role="tab" href="#tab_1"><fmt:message key="productFields"/></a>
				                </li>
				                <li class="nav-item">
				                		<div class="dropdown">
					                    <a class="nav-link" data-toggle="dropdown">Advanced <i class="fa fa-caret-down"></i></a>
										<div class="dropdown-menu dropdown-menu-right">
											<c:forEach items="${languageCodes}" var="i18n" varStatus="loop"> 
							                    <a class="dropdown-item" data-toggle="tab" role="tab" href="#tab_lan_${loop.index}">
							                    		<img src="${_contextpath}/admin-responsive-static/assets/flags/${i18n.languageCode}.png" class="mb-1" title="<fmt:message key="language_${i18n.languageCode}"/>"> <fmt:message key="language_${i18n.languageCode}"/>
							                    </a>
							                </c:forEach>
										</div>
									</div>
								</li>
							</ul>
							<div class="tab-content">
								<div id="tab_1" class="tab-pane active" role="tabpanel">
				                    	<div class="grid-stage grid-tage-sm">
										<table id="grid"></table>
									</div>
			                    </div>
								<c:forEach items="${languageCodes}" var="i18n" varStatus="loop">
									<div id="tab_lan_${loop.index}" class="tab-pane" role="tabpanel">
										<div class="grid-stage">
											<table id="grid_lan_${i18n.languageCode}" class="grid-sm grid_lan"></table>
										</div>
									</div>
								</c:forEach>
							</div>
						</div>
						<div class="footer">
							<div class="form-row">
								<div class="col-12">
									<sec:authorize ifAnyGranted="ROLE_AEM,ROLE_ADMIN_${prop_site_id},ROLE_PRODUCT_FIELD_EDIT">
										<a class="btn btn-default">Update</a>
									</sec:authorize>
									<a class="btn btn-default">Cancel</a>
								</div>
							</div>
			            </div>
					</div>
				</div>
			</div>
		</form>
	</tiles:putAttribute>
	<tiles:putAttribute name="js">
		<!-- DGRID TABLE SCRIPT MUST BE AT BOTTOM OF THE PAGE -->
		<script src="${_contextpath}/admin-responsive-static/assets/plugins/dojo/dojo.js" data-dojo-config="async: true"></script>
		<script>
			function generateMainGrid(){
				//Table Data Sanitization
				var data = [];
				<c:forEach items="${productFieldsForm.productFields}" var="productField" varStatus="status">
					data.push({
						id: ${status.index},
						field: '<fmt:message key="Field" /> <c:out value="${status.index + 1}"/>',
						<c:if test="${productField.enabled}">
							enabled: true,
						</c:if>
						<c:if test="${productField.detailsField}">
							details: true,
						</c:if>
						<c:if test="${productField.quickModeField}">
							quickMode: true,
						</c:if>
						<c:if test="${siteConfig['PRODUCT_QUICK_MODE_2'].value == 'true'}">
							<c:if test="${productField.quickMode2Field}">
								quickMode2: true,
							</c:if>
						</c:if>
						<c:if test="${productField.showOnThumbnailField}">
							showOnThumbnail: true,
						</c:if>
						<c:if test="${productField.showOnInvoice}">
							showOnInvoice: true,
						</c:if>
						<c:if test="${productField.showOnInvoiceBackend}">
							showOnInvoiceBackend: true,
						</c:if>
						<c:if test="${siteConfig['PRODUCT_FIELDS_ON_INVOICE_EXPORT'].value == 'true'}">
							<c:if test="${productField.showOnInvoiceExport}">
								showOnInvoiceExport: true,
							</c:if>
						</c:if>
						<c:if test="${gSiteConfig['gPRESENTATION']}">
					    		<c:if test="${productField.showOnPresentation}">
					    			showOnPresentation: true,
					    		</c:if>
					    </c:if>
					    <c:if test="${productField.showOnPurchaseOrder}">
							showOnPurchaseOrder: true,
					    </c:if>
					    <c:if test="${siteConfig['ADD_PRODUCT_ON_FRONTEND'].value == 'true' and gSiteConfig['gCUSTOMER_SUPPLIER']}">
							<c:if test="${productField.showOnProductFormFrontend}">
								showOnProductFormFrontend: true,	
							</c:if>
					    </c:if>
					    <c:if test="${productField.showOnDropDown}">
							showOnDropDown: true,	
						</c:if>
					    <c:if test="${productField.packingField}">
							showOnPacking: true,
					    </c:if>
					    <c:if test="${gSiteConfig['gMYLIST']}">
						    <c:if test="${productField.showOnMyList}">
								showOnMyList: true,
							</c:if>
					    </c:if>
					    <c:if test="${siteConfig['PRODUCT_FIELDS_SEARCH_TYPE'].value != '0'}"	>
							<c:if test="${productField.search}">
								search: true,
							</c:if>
							<c:if test="${siteConfig['PRODUCT_QUICK_MODE_2'].value == 'true'}">
								<c:if test="${productField.search2}">
									search2: true,
								</c:if>
							</c:if>
					    </c:if>
					    <c:if test="${gSiteConfig['gCOMPARISON']}">
							<c:if test="${productField.comparisonField}">
								compare: true,
							</c:if>
					    </c:if>    
					    <c:if test="${gSiteConfig['gSERVICE']}">
					    		<c:if test="${productField.serviceField}">
					    			service: true,
							</c:if>
					    </c:if>
					    <c:if test="${productField.productExport}">
							productExport: true,
						</c:if>
					    <c:if test="${productField.indexForSearch}">
							indexForSearch: true,
						</c:if>
					    <c:if test="${productField.indexForFilter}">
							indexForFilter: true,
						</c:if>
					    <c:if test="${productField.indexForPredictiveSearch}">
							indexForPredictiveSearch: true,
						</c:if>    
					    <c:if test="${productField.rangeFilter}">
							rangeFilter: true,
						</c:if>	    
					    <c:if test="${productField.checkboxFilter}">
					    		checkboxFilter: true,
						</c:if>
					    <c:if test="${productField.indexForSorting}">
							indexForSorting: true,
						</c:if>
					    <c:if test="${productField.showAdminFilter}">
							showAdminFilter: true,
						</c:if>
					    protectedLevel: '${productField.protectedLevel}',
					    rank: ${productField.rank},
					    fieldType: '${productField.fieldType}',
					    name: '${wj:escapeJS(productField.name)}',
					    groupName: '${wj:escapeJS(productField.groupName)}',
					    preValue: '${wj:escapeJS(productField.preValue)}',
					});
				</c:forEach>
				
				require(["dgrid/List", "dgrid/OnDemandGrid", "dgrid/Selection", "dgrid/Keyboard", "dgrid/Editor", "dijit/form/CheckBox", 
						"dijit/form/TimeTextBox", "dijit/form/Select", "dojo/data/ObjectStore", "dijit/form/FilteringSelect",
						"dojo/_base/declare", "dojo/_base/lang", "dojo/store/Memory", "dojo/store/Observable", "dgrid/test/data/createSyncStore", 
						'dgrid/extensions/ColumnReorder', 'dgrid/extensions/ColumnResizer',
					], function(List, OnDemandGrid, Selection, Keyboard, Editor, CheckBox, TimeTextBox, Select, ObjectStore, FilteringSelect,
							declare, lang, LegacyMemory, LegacyObservable, createSyncStore, ColumnReorder, ColumnResizer){
					
					//Generate Dropdown Data Store
					<c:if test="${gSiteConfig['gPROTECTED'] > 0}">
						var protectedData = {
							identifier: "value",
							items: [{value: '0', name: '0'}]
						}
						
						<c:forEach begin="1" end="${gSiteConfig['gPROTECTED']}" var="protect">
							<c:set var="key" value="protected${protect}"/>
							<c:choose>
								<c:when test="${labels[key] != null and labels[key] != ''}">
									<c:set var="label" value="${labels[key]}"/>
								</c:when>
								<c:otherwise>
									<c:set var="label" value="${protect}"/>
								</c:otherwise>
							</c:choose>
							protectedData.items.push({
								value: '${protectedLevels[protect-1]}',
								name: '${wj:escapeJS(label)}'
							});
						</c:forEach>

						var protectedStore = new ObjectStore({
							objectStore: LegacyObservable(new LegacyMemory({ data: protectedData })),
							labelProperty: "name",
						});
					</c:if>
					
					<c:if test="${!fn:contains(header['host'],'viatrading') and !fn:contains(header['host'],'wjserver180')}">
					 	var fieldTypeData = {
							identifier: "value",
							items: [
								{value: 'string', name: '<fmt:message key="text" />'},
								{value: 'number', name: '<fmt:message key="number" />'},
							]
						}
					 	var fieldTypeStore = new ObjectStore({
							objectStore: LegacyObservable(new LegacyMemory({ data: fieldTypeData })),
							labelProperty: "name",
						});
					</c:if>

					//Column Definitions
					var columns = {
						field: {
							label: '',
							get: function(object){ 
								return object.field;
							},
							sortable: false,
						},
						enabled: {
							editor: 'checkbox',
							label: '<fmt:message key="Enabled" />',
							get: function(object){ 
								return object.enabled;
							},
							sortable: false,
						},
						details: {
							editor: 'checkbox',
							label: '<fmt:message key="details" />',
							get: function(object){ 
								return object.details;
							},
							sortable: false,
						},
						enabled: {
							editor: 'checkbox',
							label: '<fmt:message key="Enabled" />',
							get: function(object){ 
								return object.enabled;
							},
							sortable: false,
						},
						quickMode: {
							editor: 'checkbox',
							label: '<fmt:message key="quickMode" />',
							get: function(object){ 
								return object.quickMode;
							},
							sortable: false,
						},
						<c:if test="${siteConfig['PRODUCT_QUICK_MODE_2'].value == 'true'}">
							quickMode2: {
								editor: 'checkbox',
								label: '<fmt:message key="quickMode2" />',
								get: function(object){ 
									return object.quickMode2;
								},
								sortable: false,
							},
						</c:if>
						showOnThumbnail: {
							editor: 'checkbox',
							label: '<fmt:message key="showOnThumbnail" />',
							get: function(object){ 
								return object.showOnThumbnail;
							},
							sortable: false,
						},
						showOnInvoice: {
							editor: 'checkbox',
							label: '<fmt:message key="showOnInvoice" />',
							get: function(object){ 
								return object.showOnInvoice;
							},
							sortable: false,
						},
						showOnInvoiceBackend: {
							editor: 'checkbox',
							label: '<fmt:message key="showOnInvoice" /> (<fmt:message key="backend" />)',
							get: function(object){ 
								return object.showOnInvoiceBackend;
							},
							sortable: false,
						},
						<c:if test="${siteConfig['PRODUCT_FIELDS_ON_INVOICE_EXPORT'].value == 'true'}">
							showOnInvoiceExport: {
								editor: 'checkbox',
								label: '<fmt:message key="showOnInvoice" /> (<fmt:message key="export" />)',
								get: function(object){ 
									return object.showOnInvoiceExport;
								},
							},
						</c:if>
						<c:if test="${gSiteConfig['gPRESENTATION']}">
							showOnPresentation: {
								editor: 'checkbox',
								label: '<fmt:message key="showOnPresentation" />',
								get: function(object){ 
									return object.showOnPresentation;
								},
							},
					    </c:if>
						showOnPurchaseOrder: {
							editor: 'checkbox',
							label: '<fmt:message key="showOnPurchaseOrder" />',
							get: function(object){ 
								return object.showOnPurchaseOrder;
							},
						},
						<c:if test="${siteConfig['ADD_PRODUCT_ON_FRONTEND'].value == 'true' and gSiteConfig['gCUSTOMER_SUPPLIER']}">
							showOnProductFormFrontend: {
								editor: 'checkbox',
								label: '<fmt:message key="showOnProductFormFrontend" />',
								get: function(object){ 
									return object.showOnProductFormFrontend;
								},
							},
						</c:if>
						showOnDropDown: {
							editor: 'checkbox',
							label: '<fmt:message key="showOnDropDown" />',
							get: function(object){ 
								return object.showOnDropDown;
							},
						},
						showOnPacking: {
							editor: 'checkbox',
							label: '<fmt:message key="showOnPacking" />',
							get: function(object){ 
								return object.showOnPacking;
							},
						},
						<c:if test="${gSiteConfig['gMYLIST']}">
							showOnMyList: {
								editor: 'checkbox',
								label: '<fmt:message key="showOnMyList" />',
								get: function(object){ 
									return object.showOnMyList;
								},
							},
					    </c:if>
						<c:if test="${siteConfig['PRODUCT_FIELDS_SEARCH_TYPE'].value != '0'}"	>
							search: {
								editor: 'checkbox',
								label: '<fmt:message key="search" />',
								get: function(object){ 
									return object.search;
								},
							},
						    <c:if test="${siteConfig['PRODUCT_QUICK_MODE_2'].value == 'true'}">
							    search2: {
									editor: 'checkbox',
									label: '<fmt:message key="search" /> 2',
									get: function(object){ 
										return object.search2;
									},
								},
						    </c:if>
					    </c:if>
					    <c:if test="${gSiteConfig['gCOMPARISON']}">
							compare: {
								editor: 'checkbox',
								label: '<fmt:message key="compare" />',
								get: function(object){ 
									return object.compare;
								},
							},
						</c:if>
						<c:if test="${gSiteConfig['gSERVICE']}">
							service: {
								editor: 'checkbox',
								label: '<fmt:message key="service" />',
								get: function(object){ 
									return object.service;
								},
							},
						</c:if>
						productExport: {
							editor: 'checkbox',
							label: '<fmt:message key="productExport" />',
							get: function(object){ 
								return object.productExport;
							},
						},
						indexForSearch: {
							editor: 'checkbox',
							label: '<fmt:message key="indexForSearch" />',
							get: function(object){ 
								return object.indexForSearch;
							},
						},
						indexForFilter: {
							editor: 'checkbox',
							label: '<fmt:message key="indexForFilter" />',
							get: function(object){ 
								return object.indexForFilter;
							},
						},
						indexForPredictiveSearch: {
							editor: 'checkbox',
							label: '<fmt:message key="indexForPredictiveSearch" />',
							get: function(object){ 
								return object.indexForPredictiveSearch;
							},
						},
						rangeFilter: {
							editor: 'checkbox',
							label: '<fmt:message key="rangeFilter" />',
							get: function(object){ 
								return object.rangeFilter;
							},
						},
						checkboxFilter: {
							editor: 'checkbox',
							label: '<fmt:message key="checkboxFilter" />',
							get: function(object){ 
								return object.checkboxFilter;
							},
						},
						indexForSorting: {
							editor: 'checkbox',
							label: 'Index for Sorting',
							get: function(object){ 
								return object.indexForSorting;
							},
						},
						showAdminFilter: {
							editor: 'checkbox',
							label: 'Show on Admin Filter',
							get: function(object){ 
								return object.showAdminFilter;
							},
						},
						<c:if test="${gSiteConfig['gPROTECTED'] > 0}">
						   protectedLevel: {
								editor: Select,
								label: '<fmt:message key="protectedLevel" />',
								field: "protectedLevel",
								editorArgs: {
									store: protectedStore,
									maxHeight: 220
								}
							},
					    </c:if>
						rank: {
							label: '<fmt:message key="rank" />',
							editor: "text",
							sortable: false
						},
						<c:if test="${!fn:contains(header['host'],'viatrading') and !fn:contains(header['host'],'wjserver180')}">
							fieldType: {
								editor: Select,
								label: '<fmt:message key="type" />',
								field: "fieldType",
								editorArgs: {
									store: fieldTypeStore
								}
							},
						</c:if>
						name: {
							label: '<fmt:message key="Name" />',
							editor: "text",
							sortable: false
						},
						groupName:{
							label: '<fmt:message key="group" /> <fmt:message key="name" />',
							editor: "text",
							sortable: false
						},
						preValue:{
							label: '<fmt:message key="PreValue" />',
							editor: "text",
							sortable: false
						}
					};
					
					var grid = new (declare([OnDemandGrid, Selection, Keyboard, Editor, ColumnReorder, ColumnResizer]))({
						collection: createSyncStore({ data: data }),
						columns: columns,
						selectionMode: "none",
					},'grid');

					grid.startup();
				});
			}
			
			function generateLanGrid(languageCode,data){
				require(["dgrid/List", "dgrid/OnDemandGrid", "dgrid/Selection", "dgrid/Keyboard", "dgrid/Editor", "dijit/form/CheckBox", 
						"dijit/form/TimeTextBox", "dijit/form/Select", "dojo/data/ObjectStore", "dijit/form/FilteringSelect",
						"dojo/_base/declare", "dojo/_base/lang", "dojo/store/Memory", "dojo/store/Observable", "dgrid/test/data/createSyncStore", 
						'dgrid/extensions/ColumnReorder', 'dgrid/extensions/ColumnResizer',
					], function(List, OnDemandGrid, Selection, Keyboard, Editor, CheckBox, TimeTextBox, Select, ObjectStore, FilteringSelect,
						declare, lang, LegacyMemory, LegacyObservable, createSyncStore, ColumnReorder, ColumnResizer){
				
					//Column Definitions
					var columns = {
						field:{
							label: '',
							get: function(object){ 
								return object.field;
							},
							sortable: false,
						},
						name: {
							label: '<fmt:message key="Name" />',
							editor: "text",
							sortable: false
						},
						preValue:{
							label: '<fmt:message key="PreValue" />',
							editor: "text",
							sortable: false
						}
					};

					var grid = new (declare([OnDemandGrid, Selection, Keyboard, Editor, ColumnReorder, ColumnResizer]))({
						collection: createSyncStore({ data: data }),
						columns: columns,
						selectionMode: "none",
					},'grid_lan_'+languageCode);
					
					grid.on('dgrid-refresh-complete', function(event) {
						$('#grid-hider-menu').appendTo('#column-hider');
					});
					
					grid.startup();
				});
			}

			generateMainGrid();
			<c:forEach items="${languageCodes}" var="i18n">
				var data = [];
				<c:forEach items="${productFieldsForm.productFields}" var="productField" varStatus="status">
					//Table Data Sanitization
					data.push({
						field: '<fmt:message key="Field" /> ${status.index + 1}',
					    name: '${wj:escapeJS(i18nProductFields[i18n.languageCode][productField.id].name)}',
					    preValue: '${wj:escapeJS(i18nProductFields[i18n.languageCode][productField.id].preValue)}',
					});
				</c:forEach>
				generateLanGrid('${i18n.languageCode}',data);
			</c:forEach>
		</script>
	</tiles:putAttribute>
</tiles:insertDefinition>