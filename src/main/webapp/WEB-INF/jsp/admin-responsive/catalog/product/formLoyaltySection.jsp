<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ taglib uri="http://www.springframework.org/security/tags" prefix="sec" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ taglib uri="/WEB-INF/tlds/spring.tld" prefix="spring" %>
<%@ taglib uri="/WEB-INF/tlds/custom.tld" prefix="wj" %>

<div id="tab_4" class="tab-pane" role="tabpanel">
  	<div class="line">
	  	<div class="form-row">
	  		<div class="col-lg-4">
		    		<label for="product_loyaltyPoint" class="col-form-label">
		    			<span class="fa fa-question-circle" data-toggle="tooltip" data-placement="top" title="Note::Customers will receive this amount of points if an order takes place."></span>
					<fmt:message key="loyalty" /> <fmt:message key="point" />:
				</label>
			</div>
	        <div class="col-lg-4"> 
			  	<form:input path="product.loyaltyPoint" size="10" id="product_loyaltyPoint" class="form-control" maxlength="10" />
			</div>
		</div>
	</div>
</div>