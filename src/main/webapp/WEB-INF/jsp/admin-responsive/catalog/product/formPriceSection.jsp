<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ taglib uri="http://www.springframework.org/security/tags" prefix="sec" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ taglib uri="/WEB-INF/tlds/spring.tld" prefix="spring" %>
<%@ taglib uri="/WEB-INF/tlds/custom.tld" prefix="wj" %>

<div id="tab_5" class="tab-pane" role="tabpanel">
	<c:if test="${gSiteConfig['gVARIABLE_PRICE']}">
		<div class="line">
			<div class="form-row">
				<div class="col-label">
		   			<label class="col-form-label">
		   				<span class="fa fa-question-circle" data-toggle="tooltip" data-placement="top" title="Note::Customer can enter the price of this product. Recommended usage: Donation"></span>
						<fmt:message key="price" /> <fmt:message key="enterByCustomer" />
					</label>
				</div>
				<div class="col-lg-4"> 
					<label class="custom-control custom-checkbox">
						<form:checkbox path="product.priceByCustomer" id="product_priceByCustomer" class="custom-control-input" onclick="togglePrice(this.id)" />
						<span class="custom-control-indicator"></span>
					</label>
				</div>
			</div>
		</div>
	</c:if>
	<div class="line">
		<div class="form-row">
			<div class="col-label">
		   		<label class="col-form-label">
		   			<span class="fa fa-question-circle" data-toggle="tooltip" data-placement="top" title="Taxable::Check if the product is taxable."></span>
					<fmt:message key="taxable" />
				</label>
			</div>
			<div class="col-lg-4"> 
				<label class="custom-control custom-checkbox">
					<form:checkbox path="product.taxable" id="product_taxable" class="custom-control-input"/>
					<span class="custom-control-indicator"></span>
				</label>
			</div>
		</div>
	</div>
	<c:if test="${gSiteConfig['gSPECIAL_TAX']}">
		<div class="line">
			<div class="form-row">
				<div class="col-label">
					<label class="col-form-label">
						<span class="fa fa-question-circle" data-toggle="tooltip" data-placement="top" title="Taxable::Check if the product is taxable."></span>
						<fmt:message key="special" /> <fmt:message key="tax" />
					</label>
				</div>
				<div class="col-lg-4"> 
					<label class="custom-control custom-checkbox">
						<form:checkbox path="product.enableSpecialTax" id="product_enableSpecialTax" class="custom-control-input"/>
						<span class="custom-control-indicator"></span>
					</label>
				</div>
			</div>
		</div>
	</c:if>
	<div class="line">
		<div id="fixPrice" class="form-row">
			<div class="col-label">
		  		<label class="col-form-label">
		  			<span class="fa fa-question-circle" data-toggle="tooltip" data-placement="top" title="Login Required::Check if price display needs login."></span>
					<fmt:message key="loginRequire" />
				</label>
			</div>
			<div class="col-lg-4">
				<label class="custom-control custom-checkbox">
					<form:checkbox path="product.loginRequire" id="product_loginRequire" class="custom-control-input"/>
                    <span class="custom-control-indicator"></span>
                </label>
			</div>
		</div>
	</div>
	<div class="line">
		<div class="form-row">
			<div class="col-label">
		  		<label for="product_hidePrice" class="col-form-label">
		  			<span class="fa fa-question-circle" data-toggle="tooltip" data-placement="top" title="Hide price::Check if want to hide price."></span>
					<fmt:message key="hidePrice" />
				</label>
			</div>
			<div class="col-lg-4"> 
				<label class="custom-control custom-checkbox">
					<form:checkbox path="product.hidePrice" id="product_hidePrice" class="custom-control-input"/>
					<span class="custom-control-indicator"></span>
				</label>
			</div>
		</div>
	</div>
	<c:if test="${siteConfig['PRODUCT_QUOTE'].value == 'true'}">
		<div class="line">
			<div class="form-row">
				<div class="col-label">
					<label class="col-form-label">
						<fmt:message key="quote" />
					</label>
				</div>
				<div class="col-lg-4"> 
					<label class="custom-control custom-checkbox">
						<form:checkbox path="product.quote" id="product_quote" class="custom-control-input"/>
						<span class="custom-control-indicator"></span>
					</label>
				</div>
			</div>
		</div>
	</c:if>
	<c:if test="${siteConfig['SALES_REP_COMMISSION'].value == 'true' or siteConfig['SALES_REP_COMMISSION_RANGE'].value == 'true'}">
 		<div class="line">
	 		<div class="form-row">
	 			<div class="col-label">
			    		<label for="product_commissionable" class="col-form-label">
			    			<span class="fa fa-question-circle" data-toggle="tooltip" data-placement="top" title="Note::"></span>
						Commissionable:
					</label>
				</div>
				<div class="col-lg-4"> 
					<label class="custom-control custom-checkbox">
	                      <form:checkbox path="product.commissionable" id="product_commissionable" class="custom-control-input"/>
	                      <span class="custom-control-indicator"></span>
	                  </label>
				</div>
	  		</div>
  		</div>
	</c:if>
	<div class="line">
		<div class="form-row">
			<div class="col-label">
				<label class="col-form-label">
					<fmt:message key="packing" />
				</label>
			</div>
			<div class="col-lg-4"> 
				<form:input path="product.packing" id="product_packing" class="form-control" maxlength="50"/>
			</div>
	 	</div>
 	</div>
 	<c:if test="${gSiteConfig['gCASE_CONTENT']}">
 		<div class="line">
			<div class="form-row">
				<div class="col-label">
					<label for="product_caseContent" class="col-form-label">
						<fmt:message key="caseContent" />
					</label>
				</div>
				<div class="col-lg-4"> 
					<form:input path="product.caseContent" id="product_caseContent" class="form-control" maxlength="8"/>
		         </div>
			</div>
		</div>
		<div class="line">
			<div class="form-row">
				<div class="col-label">
			  		<label for="product_cubicSize" class="col-form-label">
						<fmt:message key="cubicSize" />
					</label>
				</div>
				<div class="col-lg-4"> 
					<form:input path="product.cubicSize" id="product_cubicSize" class="form-control" maxlength="8"/>
		         </div>
			</div>
		</div>
		<div class="line">
			<div class="form-row">
				<div class="col-label">
			  		<label for="product_caseUnitTitle" class="col-form-label">
						<fmt:message key="caseUnitTitle" />:
					</label>
				</div>
				<div class="col-lg-4"> 
					<form:input path="product.caseUnitTitle" id="product_caseUnitTitle" class="form-control" maxlength="8"/>
		         </div>
			</div>
		</div>
	</c:if>
	<c:if test="${gSiteConfig['gASI'] != ''}">
		<div class="line">
			<div class="form-row">
				<div class="col-label">
			  		<label for="product_asiIgnorePrice" class="col-form-label">
			  			<span class="fa fa-question-circle" data-toggle="tooltip" data-placement="top" title="Ignore ASI Price::Check this flag to ignore Price provided by ASI."></span>
						<fmt:message key="ignoreASIPrice" />
					</label>
				</div>
				<div class="col-lg-4"> 
					<label class="custom-control custom-checkbox">
						<form:checkbox path="product.asiIgnorePrice" id="product_asiIgnorePrice" class="custom-control-input"/>
						<span class="custom-control-indicator"></span>
					</label>
				</div>
			</div>
		</div>
	</c:if>
	<c:if test="${gSiteConfig['gADI'] or fn:length(siteConfig['SEHI_FEED'].value) > 0 or fn:contains(siteConfig['SITE_URL'].value, 'bargainw') or fn:contains(siteConfig['SITE_URL'].value, 'rainbow')}">
		<div class="line">
			<div class="form-row">
				<div class="col-lg-4">
					<label for="product_discountedPrice" class="col-form-label">
						<fmt:message key="discountedPrice" />
					</label>
				</div>
				<div class="col-lg-4">
					<form:input path="product.discountedPrice" id="product_discountedPrice" class="form-control" maxlength="10"/>
				</div>
			</div>
		</div>
		<div class="line">
			<div class="form-row">
				<div class="col-label">
		  			<label for="product_discountedCost" class="col-form-label">
						<fmt:message key="discountedCost" />
					</label>
				</div>
				<div class="col-lg-4">
					<c:choose>
		      			<c:when test="${gSiteConfig['gADI'] or fn:contains(siteConfig['SITE_URL'].value, 'bargainw')}">
	      	   				<form:input path="product.discountedCost" disabled="true" id="product_discountedCost" class="form-control" maxlength="10"/>
						</c:when>
		      			<c:otherwise>
		      	   			<form:input path="product.discountedCost" id="product_discountedCost" class="form-control" maxlength="10"/>
						</c:otherwise>
		        		</c:choose>
	        		</div>
	         </div>
		</div>
		<div class="line">
			<div class="form-row">
				<div class="col-label">
			  		<label for="product_discountStartDate" class="col-form-label">
						<fmt:message key='discount' /> <fmt:message key='startDate' />
					</label>
				</div>
				<div class="col-lg-4">
					<form:input path="product.discountStartDate" id="product_discountStartDate" class="form-control" maxlength="19" />
				</div>
			</div>
		</div>
		<div class="line">
			<div class="form-row">
				<div class="col-label">
					<label for="product_discountEndDate" class="col-form-label">
						<fmt:message key='discount' /> <fmt:message key='endDate' />
					</label>
				</div>
				<div class="col-lg-4">
					<form:input path="product.discountEndDate" id="product_discountEndDate" class="form-control" maxlength="19" />
				</div>
			</div>
		</div>
	</c:if>
	<div class="line">
		<div class="form-row">
			<div class="col-label">
		 		<label class="col-form-label">
					<c:if test="${gSiteConfig['gPURCHASE_ORDER'] && siteConfig['SHOW_COST'].value == 'true'}">
						<a href="#"><i class="sli-magnifier"></i></a>
					</c:if>
					<fmt:message key="productPrice" /><c:if test="${gSiteConfig['gPRICE_TIERS'] > 1}"> 1</c:if>
				</label>
			</div>
			<div class="col-lg-8 price-line">
				<!-- When gPRICE_TIERS is equal to 1 -->
				<c:if test="${gSiteConfig['gPRICE_TIERS'] >= 1}">
					<div class="row">
						<div class="col-lg-3">
							<form:input path="product.price1" class="form-control" maxlength="10"/>
						</div>
		 				<c:if test="${gSiteConfig['gPRICE_TIERS'] > 1}">
		 					<div class="col-lg-3">
		    						<c:choose>
		   							<c:when test="${gSiteConfig['gMINIMUM_INCREMENTAL_QTY'] and productForm.product.minimumQty != null}">
		        							<input type="text" id="priceFrom_1" class="form-control" value="${productForm.product.minimumQty}" readonly="readonly">
		      						</c:when>
			      					<c:otherwise>
			        						<input type="text" id="priceFrom_1" class="form-control" value="1" readonly="readonly">
			      					</c:otherwise>
		    						</c:choose>
							</div>
		  					<span class="word-connect"><span>to</span></span>
		  					<div class="col-lg-3">
		  						<input type="text" id="priceTo_1" readonly="readonly" class="form-control form-control-sm">
		  					</div>
		  				</c:if>
	  				</div>
	  				<div class="row mt-1">
		  				<c:if test="${gSiteConfig['gSUPPLIER'] and siteConfig['COST_TIERS'].value == 'true'}">
		 	  				<c:choose>
		 	    					<c:when test="${gSiteConfig['gADI']}">
		     	    					<div class="col-lg-3">
		     	    						<label>
											<fmt:message key="productCost" />:		     	    							
		     	    						</label>
		      	    						<form:input path="product.cost1" disabled="true" class="form-control" maxlength="10"/>
		     	    					</div>
		     	    					<div class="col-lg-3">
		     	    						<label>
											<fmt:message key="markup" />:		     	    							
		     	    						</label>
					  					<div class="input-group">
				                				<form:input path="product.markupPercent1" class="form-control" maxlength="10" disabled="true"/>
				                				<span class="input-group-addon">%</span>
				              			</div>
				  					</div>
								</c:when>
		     					<c:otherwise>
			     					<div class="col-lg-3">
			     						<label>
											<fmt:message key="productCost" />:		     	    							
		     	    						</label>
			      						<form:input path="product.cost1" class="form-control" maxlength="10"/>
		     						</div>
		     						<div class="col-lg-3">
		     	    						<label>
											<fmt:message key="productMargin" />:		     	    							
		     	    						</label>
										<div class="input-group">
			                					<form:input path="product.marginPercent1" class="form-control" maxlength="10" disabled="true"/>
											<span class="input-group-addon">%</span>
			             				</div>
									</div>
		    						</c:otherwise>
							</c:choose>
						</c:if>
						<c:if test="${gSiteConfig['gPRICE_TIERS'] > 1}">
							<c:if test="${gSiteConfig['gSALES_PROMOTIONS']}">
								<div class="col-lg-3">
									<label>
										<fmt:message key="discount" />:		     	    							
	     	    						</label>
					        	  		<form:input path="product.discount1" class="form-control" maxlength="10"/>
			        				</div>
			        				<div class="col-lg-3">
			        					<label>
										&nbsp;		     	    							
	     	    						</label>
									<form:select path="product.discountPercent1" class="custom-select">
									    <form:option value="true" label="%" />
									    <form:option value="false"><fmt:message key="FIXED" /> $</form:option>
									</form:select>
								</div>
							</c:if>
						</c:if>
					</div>
				</c:if>
			</div>
		</div>
	</div>
	
	<c:if test="${gSiteConfig['gPRICE_TIERS'] > 1}">
		<c:forEach begin="2" end="${gSiteConfig['gPRICE_TIERS']}" var="priceTier">
			<div class="line">
				<div class="form-row">
					<div class="col-label">
						<label class="col-form-label">
							<span class="fa fa-question-circle" data-toggle="tooltip" data-placement="top" title="Price::It accept 7 integers and 3 decimals, For emaple,1234567.999."></span>
							<fmt:message key="productPrice" /> ${priceTier}
						</label>
					</div>
					<div class="col-lg-8 price-line">
       					<div class="row">
		       				<div class="col-lg-3">
		       					<form:input path="product.price${priceTier}" class="form-control form-control-sm" size="10" maxlength="10"/>
		  					</div>
		  					<div class="col-lg-3">
		    						<form:input path="product.qtyBreak${priceTier-1}" size="5" maxlength="10" onblur="updateQtyBreak(${priceTier})" id="priceFrom_${priceTier}" class="form-control form-control-sm"/>
							</div>
	  						<span class="word-connect"><span>to</span></span> 
	  						<div class="col-lg-3">
					  			<input type="text" id="priceTo_${priceTier}" size="5" readonly="readonly" class="form-control form-control-sm">
					  		</div>
				  		</div>
				  		<div class="row mt-1">
							<c:if test="${gSiteConfig['gSUPPLIER'] and siteConfig['COST_TIERS'].value == 'true'}">
								<c:choose>
									<c:when test="${gSiteConfig['gADI']}">
										<div class="col-lg-3">
											<label>
												<fmt:message key="productCost" />:
											</label>
											<form:input path="product.cost${priceTier}" disabled="true" class="form-control" maxlength="10"/>
	  	    								</div>
	  	    								<div class="col-lg-3">
	   	        								<label>
	 											<fmt:message key="markup" />:
	 										</label>
	 										<div class="input-group">
												<form:input path="product.markupPercent${priceTier}" class="form-control" maxlength="10" disabled="true"/>
												<span class="input-group-addon">%</span>
	             							</div>
										</div>
									</c:when>
									<c:otherwise>
										<div class="col-lg-3">
											<label>
												<fmt:message key="productCost" />:
											</label>
											<form:input path="product.cost${priceTier}" class="form-control" maxlength="10"/>
										</div>
										<div class="col-lg-3">
											<label>
												<fmt:message key="productMargin" />:
											</label>															
											<div class="input-group">
												<form:input path="product.marginPercent${priceTier}" class="form-control" maxlength="10" disabled="true"/>
												<span class="input-group-addon">%</span>
											</div>
										</div>
									</c:otherwise>
								</c:choose>
							</c:if>
	      					<c:if test="${gSiteConfig['gSALES_PROMOTIONS']}">
								<div class="col-lg-3">
									<label>
										<fmt:message key="discount" />:
									</label>	
									<form:input path="product.discount${priceTier}" class="form-control" maxlength="10"/>
								</div>
								<div class="col-lg-3">
									<label>
										&nbsp;
									</label>	
									<form:select path="product.discountPercent${priceTier}" class="custom-select">
										<form:option value="true" label="%" />
									    <form:option value="false"><fmt:message key="FIXED" /> $</form:option>
									</form:select>	
								</div>
							</c:if>
						</div>
					</div>
				</div>
			</div>
		</c:forEach>
	</c:if>
	<div class="line">
		<div class="form-row">
			<div class="col-label">
		  		<label class="col-form-label">
					<fmt:message key="hideMsrp" />
				</label>
			</div>
			<div class="col-lg-4">
				<label class="custom-control custom-checkbox">
					<form:checkbox path="product.hideMsrp" id="product_hideMsrp" class="custom-control-input" />
					<span class="custom-control-indicator"></span>
				</label>	
			</div>
		</div>
	</div>
	
	<c:choose>
		<c:when test="${siteConfig['UNLIMITED_PRICE_TABLES'].value}">
			<c:forEach items="${unlimitedPriceTables}" var="priceTable" varStatus="status">
				<div class="line">
					<div class="form-row">
	 					<label class="col-label">
	 						<span class="fa fa-question-circle" data-toggle="tooltip" data-placement="top" title="Price Table::It accept 7 integers and 3 decimals, For emaple,1234567.999."></span>
							<c:out value="${priceTable.tableName}"/>
						</label>	
						<div class="col-lg-4">
							<input type="text" class="form-control form-control-sm" value="${productForm.unlimitedPriceTableMap[priceTable.tableNumber].price}" id="unlimitedPriceTableId_${priceTable.tableNumber}" name="unlimitedPriceTableId_${priceTable.tableNumber}">	
						</div>
					</div>
				</div>
			</c:forEach>
		</c:when>
		<c:otherwise>
			<c:if test="${gSiteConfig['gPRICE_TABLE'] > 0}">
				<c:forEach begin="1" end="${gSiteConfig['gPRICE_TABLE']}" var="priceTable">
					<div class="line">
						<div class="form-row">
							<c:set var="key" value="priceTable${priceTable}"/>
							<c:choose>
								<c:when test="${labels[key] != null and labels[key] != ''}">
									<c:set var="label" value="${labels[key]}"/>
	  							</c:when>
							  	<c:otherwise>
									<c:set var="label"><fmt:message key="productPriceTable" /> ${priceTable}</c:set>
								</c:otherwise>
							</c:choose>
							<div class="col-label">
								<label class="col-form-label">
									<span class="fa fa-question-circle" data-toggle="tooltip" data-placement="top" title="Price Table::It accept 7 integers and 3 decimals, For emaple,1234567.999."></span>
									<c:out value="${label}"/>
								</label>
							</div>
							<div class="col-lg-4">
								<form:input path="product.priceTable${priceTable}" class="form-control form-control-sm" size="10" maxlength="10"/>
							</div>
						</div>
					</div>
				</c:forEach>
			</c:if>	
		</c:otherwise>
	</c:choose>
	
	<c:if test="${gSiteConfig['gPRICE_CASEPACK']}">
		<div class="line">
			<div class="form-row">
				<div class="col-label">
		  			<label class="col-form-label">
						<fmt:message key="casePackQty" />
					</label>
				</div>
				<div class="col-lg-4">
					<form:input path="product.priceCasePackQty" id="product_priceCasePackQty" class="form-control" maxlength="8" />
	         	</div>
			</div>
		</div>
        <c:forEach begin="1" end="${gSiteConfig['gPRICE_TABLE']}" var="priceTable">
			<div class="line">
				<div class="form-row">
					<div class="col-label">
		 				<label class="col-form-label">
		 					<span class="fa fa-question-circle" data-toggle="tooltip" data-placement="top" title="Price Case Pack::It accept 7 integers and 3 decimals, For emaple,1234567.999."></span>
							<fmt:message key="priceCasePack" /> ${priceTable}
						</label>
					</div>
					<div class="col-lg-4">
						<form:input path="product.priceCasePack${priceTable}" class="form-control" maxlength="10"/>
					</div>
	  			</div>
	  		</div>
		</c:forEach>
	</c:if>
</div>