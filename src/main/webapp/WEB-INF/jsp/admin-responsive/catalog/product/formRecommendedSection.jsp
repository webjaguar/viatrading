<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ taglib uri="http://www.springframework.org/security/tags" prefix="sec" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ taglib uri="/WEB-INF/tlds/spring.tld" prefix="spring" %>
<%@ taglib uri="/WEB-INF/tlds/custom.tld" prefix="wj" %>

<div id="tab_10" class="tab-pane" role="tabpanel">
	<div class="line">
		<div class="form-row">
	    		<div class="col-label">
		    		<label for="product_recommendedList" class="col-form-label">
					<span class="fa fa-question-circle" data-toggle="tooltip" data-placement="top" title="Recommended List::Mention multiple SKUs comma seperated. These Skus will appear on frontend with this product."></span>
					<fmt:message key="productSkus" />
				</label>
			</div>
			<div class="col-lg-4"> 
	        		<c:choose>
		  			<c:when test="${gSiteConfig['gADI']}">
	         			<form:textarea path="product.recommendedList" disabled="true" id="product_recommendedList" class="form-control" rows="5" />
		  		  	</c:when>
		  		  	<c:otherwise>
	         			<form:textarea path="product.recommendedList" id="product_recommendedList" class="form-control" rows="5" />
		  		  	</c:otherwise>
		  		</c:choose>
       		</div>
		</div>
	</div>
	<c:if test="${siteConfig['NEW_PROMO'].value == 'true'}">
		<div class="line">
			<div class="form-row">
				<div class="col-label">
			    		<label for="product_decoSku" class="col-form-label">
			    			<fmt:message key="decoSku" />
					</label>
				</div>
				<div class="col-lg-4"> 
		        		<form:textarea path="product.decoSku" disabled="false" id="product_decoSku" class="form-control" rows="5" />
	        		</div>
			</div>
		</div>
	</c:if>
	<div class="line">
		<div class="form-row">
			<div class="col-label">
		    		<label for="product_recommendedListTitle" class="col-form-label">
		    			<fmt:message key="recommendedList" /> <fmt:message key="title" />
				</label>
			</div>
			<div class="col-lg-4"> 
	        		<form:input path="product.recommendedListTitle" id="product_recommendedListTitle" class="form-control" />
			</div>
		</div>
	</div>
	<div class="line">
		<div class="form-row">
			<div class="col-label">
		    		<label for="product_recommendedList1" class="col-form-label">
		    			<span class="fa fa-question-circle" data-toggle="tooltip" data-placement="top" title="Recommended List::Mention multiple SKUs comma seperated. These Skus will appear on frontend with this product."></span>
		    			<fmt:message key="productSkus" />1
				</label>
			</div>
			<div class="col-lg-4">
				<form:textarea path="product.recommendedList1" id="product_recommendedList1" class="form-control" rows="5" /> 
       		</div>
		</div>
	</div>
	<div class="line">
		<div class="form-row">
			<div class="col-label">
		    		<label for="product_recommendedListTitle1" class="col-form-label">
		    			<fmt:message key="recommendedList" /> 1 <fmt:message key="title" />
				</label>
			</div>
			<div class="col-lg-4">
				<form:input path="product.recommendedListTitle1" id="product_recommendedListTitle1" class="form-control" /> 
       		</div>
		</div>
	</div>
	<div class="line">
		<div class="form-row">
			<div class="col-label">
		    		<label for="product_recommendedList2" class="col-form-label">
		    			<span class="fa fa-question-circle" data-toggle="tooltip" data-placement="top" title="Recommended List::Mention multiple SKUs comma seperated. These Skus will appear on frontend with this product."></span>
		    			<fmt:message key="productSkus" />2
				</label>
			</div>
			<div class="col-lg-4">
				<form:textarea path="product.recommendedList2" id="product_recommendedList2" class="form-control" rows="5" /> 
       		</div>
		</div>
	</div>
	<div class="line">
		<div class="form-row">
			<div class="col-label">
		    		<label for="product_recommendedListTitle2" class="col-form-label">
		    			<fmt:message key="recommendedList" /> 2 <fmt:message key="title" />
				</label>
			</div>
			<div class="col-lg-4">
				<form:input path="product.recommendedListTitle2" id="product_recommendedListTitle2" class="form-control" /> 
       		</div>
		</div>
	</div>
	<div class="line">
		<div class="form-row">
			<div class="col-label">
		    		<label for="product_recommendedListDisplay" class="col-form-label">
		    			<fmt:message key="recommendedList" /> <fmt:message key="display" />
				</label>
			</div>
			<div class="col-lg-4">
				<form:select id="product_recommendedListDisplay" path="product.recommendedListDisplay" class="custom-select">
		        		<form:option value=""><fmt:message key="global"/></form:option> 
		          	<form:option value="1">1. <fmt:message key="default" /></form:option>
		          	<form:option value="2">2. <fmt:message key="default" /> 2</form:option>
		          	<form:option value="3">3. compact up</form:option>
		          	<form:option value="4">4. compact short Desc.</form:option>
		          	<form:option value="5">5. compact down</form:option>
		          	<form:option value="6">6. compact popup</form:option>
		          	<form:option value="7">7. compact</form:option>
		          	<form:option value="8">8. compact one add to cart</form:option>
		          	<form:option value="9">9. compact first price</form:option>
		          	<form:option value="10">10. divs</form:option>
		          	<form:option value="11">11. price break</form:option>
		          	<form:option value="12">12. compact one add to cart (MSRP)</form:option>
		          	<form:option value="quick"><fmt:message key="quickMode"/></form:option>
		          	<form:option value="25">25.Packnwood</form:option>
		          	<c:if test="${siteConfig['PRODUCT_QUICK_MODE_2'].value == 'true'}">
		          		<form:option value="quick2"><fmt:message key="quickMode2"/></form:option>
		          		<form:option value="quick2-1"><fmt:message key="quickMode2"/>-1</form:option>
		          		<form:option value="quick2-2"><fmt:message key="quickMode2"/>-2</form:option>
		          	</c:if>		          
				  	<form:option value="quick3"><fmt:message key="quickMode"/>3</form:option>
		          	<c:forEach items="${productForm.productFields}" var="productField" varStatus="status">
		          		<form:option value="field${productField.id}">Drop Down -- <c:out value="${productField.name}"/> (Field <c:out value="${productField.id}"/>)</form:option>		          
				  	</c:forEach>
		        </form:select> 
       		</div>
		</div>
	</div>
</div>