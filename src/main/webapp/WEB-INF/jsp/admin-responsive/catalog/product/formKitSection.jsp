<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ taglib uri="http://www.springframework.org/security/tags" prefix="sec" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ taglib uri="/WEB-INF/tlds/spring.tld" prefix="spring" %>
<%@ taglib uri="/WEB-INF/tlds/custom.tld" prefix="wj" %>

<div id="tab_8" class="tab-pane" role="tabpanel">
	<c:choose>
		<c:when test="${fn:contains(siteConfig['SITE_URL'].value, 'eagletrade')}">
			<c:if test="${empty productForm.product.kitParts}">
				There are no kit parts
			</c:if>
	    		<c:if test="${!empty productForm.product.kitParts}">
				<div class="grid-stage" style="max-width: 520px">
					<div class="page-stat">
						<input type="hidden" name="_submit" id="__submit" value="deduct"/>
						<a onclick="document.getElementById('productForm').submit();"><i class="sli-minus"></i> Deduct Inventory</a>
					</div>
					<table id="kitParts"></table>
	                	<div class="footer">
		                <div class="float-left">
		                    <p class="mb-0"><sup>1</sup> All SKU needs to be unique in this list.</p>
		                    <p class="mb-0"><sup>2</sup> To remove a SKU, put the quantity to 0.</p>
		                </div>
					</div>
				</div>
			</c:if>
		</c:when>
		<c:otherwise>
			<div class="grid-stage" style="max-width: 520px">
				<div class="page-stat">
					<a onClick="addKitParts()"><i class="sli-plus"></i> Add New Part</a>
				</div>
				<table id="kitParts"></table>
                	<div class="footer">
	                <div class="float-left">
	                    <p class="mb-0"><sup>1</sup> All SKU needs to be unique in this list.</p>
	                    <p class="mb-0"><sup>2</sup> To remove a SKU, put the quantity to 0.</p>
	                </div>
				</div>
			</div>
		</c:otherwise>
	</c:choose>
</div>