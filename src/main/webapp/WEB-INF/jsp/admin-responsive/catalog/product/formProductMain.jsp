<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ taglib uri="http://www.springframework.org/security/tags" prefix="sec" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ taglib uri="/WEB-INF/tlds/spring.tld" prefix="spring" %>
<%@ taglib uri="/WEB-INF/tlds/custom.tld" prefix="wj" %>

<div id="tab_1" class="tab-pane active" role="tabpanel">
	<div class="line">
	    <div class="form-row">
	    		<div class="col-label">
				<label for="product_activeId" class="col-form-label">
					<span class="fa fa-question-circle" data-toggle="tooltip" data-placement="top" title="Active::Unchecking this, will not allow this product to be displayed on front end."></span><fmt:message key="active" />
				</label>
			</div>
	        <div class="col-lg-4"> 
		        <div class="form-group">
					<c:choose>
				  		<c:when test="${gSiteConfig['gADI']}">					
							<label class="custom-control custom-checkbox">
		                        <form:checkbox path="product.active" id="product_activeId" class="custom-control-input" disabled="true" />
		                        <span class="custom-control-indicator"></span>
		                    </label>
				  		</c:when>
				  		<c:otherwise>
				  			<label class="custom-control custom-checkbox">
		                        <form:checkbox path="product.active" id="product_activeId" class="custom-control-input"/>
		                        <span class="custom-control-indicator"></span>
		                    </label>
				  		</c:otherwise>
				  	</c:choose>
			  	</div>
			</div>
		</div>
	</div>
	<div class="line">
		<div class="form-row">
			<div class="col-label">
		    		<label for="product_name" class="col-form-label">
					<span class="fa fa-question-circle" data-toggle="tooltip" data-placement="top" title="Product Name::The Maximum character is 120.For example: Test_Example."></span><fmt:message key="productName" />
				</label>
			</div>
	        <div class="col-lg-8">
				<div class="form-group">
					<c:choose>
			  			<c:when test="${gSiteConfig['gADI']}">
			   				<form:input path="product.name" disabled="true" id="product_name" class="form-control" maxlength="120"/>
			  		  	</c:when>
			  		  	<c:otherwise>
			   				<form:input path="product.name" id="product_name" class="form-control" maxlength="120"/>
			  		  	</c:otherwise>
			  		</c:choose>
		  		</div>
			</div>
		</div>
		<div class="form-row">
			<div class="col-label">
			</div>
			<div class="col-lg-8">
				<small class="help-text">
					<c:if test="${gSiteConfig['gMOD_REWRITE'] == '1'}">
		      			The name will be on URL. The special characters are not recommended( "/","%","@","&",""").
		      		</c:if>
				</small>
			</div>
		</div>
	</div>
	<div class="line">
		<div class="form-row">
			<div class="col-label">
		    		<label for="product_sku" class="col-form-label">
					<span class="fa fa-question-circle" data-toggle="tooltip" data-placement="top" title="Stock Keeping Unit::Required. Alphanumeric number. The maximum character is 50."></span><fmt:message key="productSku" />
				</label>
			</div>
	        <div class="col-lg-4">
				<div class="form-group">
					<c:if test="${supplierPrefix != null}">
			  			<small><b>Prefix: <c:out value="${supplierPrefix}"/></b></small>
			  		</c:if>
			  		<c:choose>
			  			<c:when test="${gSiteConfig['gADI']}">
			   				<form:input path="product.sku" disabled="true" id="product_sku" class="form-control" maxlength="50" htmlEscape="true"/>
			  		  	</c:when>
			  		  	<c:otherwise>
			   				<form:input path="product.sku" id="product_sku" class="form-control" maxlength="50" htmlEscape="true"/>
			  		  	</c:otherwise>
			  		</c:choose>
		  		</div>
			</div>
		</div>
		<div class="form-row">
			<div class="col-label">
			</div>
			<div class="col-lg-4">
				<small class="help-text">
		    			<c:if test="${gSiteConfig['gMOD_REWRITE'] == '1'}">
		      			Changing SKU will change behaviour of some modules. It is recommended not to change SKU.
		      			<br/>
		      		</c:if>
		       		The special characters are not recommended (<code>"/","%","@","&"," ", "#"</code>).
		    		</small>
			</div>
		</div>
	</div>
	<div class="line">
		<div class="form-row">
			<div class="col-label">
		    		<label for="product_type" class="col-form-label">
					<span class="fa fa-question-circle" data-toggle="tooltip" data-placement="top" title="Parent Sku::Required. Alphanumeric number. The maximum character is 50."></span><fmt:message key="product" /> <fmt:message key="type" />
				</label>
			</div>
	        <div class="col-lg-4">
		        <div class="form-group">
			  		<form:select path="product.productType" id="product_type" class="custom-select">
						<form:option value="">Regular</form:option>
				 		<c:if test="${siteConfig['GROUP_REWARDS'].value == 'true'}">
				   			<form:option value="REW" ><fmt:message key='reward'/></form:option>
				   		</c:if>
				   		<c:if test="${gSiteConfig['gDOWNLOADABLE_PRODUCTS'] || siteConfig['TECKNOQUEST_INTEGRATION'].value == 'true'}">
				   			<form:option value="DOWNLOAD" ><fmt:message key='downloadable'/></form:option>
				   		</c:if>
					</form:select>
				</div>
			</div>
		</div>
	</div>
	<div class="line">
		<div class="form-row">
			<div class="col-label">
		    		<label for="product_master_sku" class="col-form-label">
					<span class="fa fa-question-circle" data-toggle="tooltip" data-placement="top" title="Parent Sku::Required. Alphanumeric number. The maximum character is 50."></span><fmt:message key="masterSku" />
				</label>
			</div>
	        <div class="col-lg-4">
				<div class="form-group">
		  			<form:input path="product.masterSku" id="product_master_sku" class="form-control"/>
		  		</div>
			</div>
		</div>
	</div>
	<div class="line">
		<div class="form-row">
			<div class="col-label">
		    		<label for="product_upc" class="col-form-label">
					<fmt:message key="upc" />
				</label>
			</div>
	        <div class="col-lg-4">
		        <div class="form-group">
			  		<c:choose>
			  			<c:when test="${gSiteConfig['gADI']}">
			   				<form:input path="product.upc" disabled="true" id="product_upc" class="form-control" maxlength="50" />
			  		  	</c:when>
			  		  	<c:otherwise>
			   				<form:input path="product.upc" id="product_upc" class="form-control" maxlength="50"/>
			  		  	</c:otherwise>
			  		</c:choose>
		  		</div>
			</div>
		</div>
	</div>
	<c:if test="${siteConfig['ETILIZE'].value != ''}">
		<div class="line">
			<div class="form-row">
				<div class="col-label">
			    		<label for="product_etilizeId" class="col-form-label">
						Etilize ID
					</label>
				</div>
		        <div class="col-lg-4">
			        <div class="form-group">
				  		<form:input path="product.etilizeId" id="product_etilizeId" class="form-control" maxlength="50"/>
			  		</div>
				</div>
			</div>
		</div>
	</c:if>
	<c:if test="${gSiteConfig['gALSO_CONSIDER']}">
		<div class="line">
			<div class="form-row">
				<div class="col-label">
			    		<label for="product_alsoConsider" class="col-form-label">
						<span class="fa fa-question-circle" data-toggle="tooltip" data-placement="top" title="Also Consider::Mention only one SKU that will appear on frontend with this product."></span><fmt:message key="alsoConsider" /><fmt:message key="productSku" />:
					</label>
				</div>
		        <div class="col-lg-4">
			        <div class="form-group">
						<form:input path="product.alsoConsider" id="product_alsoConsider" class="form-control"/>
					</div>
				</div>
		  	</div>
	  	</div>	
  	</c:if>
  	<div class="line">
	  	<div class="form-row">
		  	<div class="col-label">
		    		<label for="product_shortDesc" class="col-form-label">
					<span class="fa fa-question-circle" data-toggle="tooltip" data-placement="top" title="::The Maximum character is 512."></span><fmt:message key="productShortDesc" />
				</label>
			</div>
	        <div class="col-lg-8">
				<div class="form-group">
			  		<c:choose>
			  			<c:when test="${gSiteConfig['gADI']}">
			   				<form:input path="product.shortDesc" disabled="true" id="product_shortDesc" class="form-control" maxlength="1024"/>
			  		  	</c:when>
			  		  	<c:otherwise>
			   				<form:input path="product.shortDesc" id="product_shortDesc" class="form-control" maxlength="1024"/>
			  		  	</c:otherwise>
			  		</c:choose>
		  		</div>
			</div>
		</div>
	</div>
	<div class="line">
		<div class="form-row">
			<div class="col-label">
		    		<label for="product_longDesc" class="col-form-label">
					<fmt:message key="productLongDesc" />
				</label>
			</div>
	        <div class="col-lg-8">
				<div class="form-group">
			  		<c:choose>
		  		  		<c:when test="${gSiteConfig['gADI']}">
	                     	<form:textarea path="product.longDesc" disabled="true" id="product_longDesc" class="code-editor"/>
		  		  		</c:when>
		  		  		<c:otherwise>
	                     	<form:textarea path="product.longDesc" id="product_longDesc" class="code-editor" />
		  		  		</c:otherwise>
		  			</c:choose>
	  			</div>
			</div>
		</div>
	</div>
	<div class="line">
		<div class="form-row">
			<div class="col-label">
		    		<label for="product_weight" class="col-form-label">
					<fmt:message key="productWeight"/>
				</label>
			</div>
	        <div class="col-lg-4">
				<div class="form-group">
			  		<c:choose>
			  			<c:when test="${gSiteConfig['gADI']}">
			  				<div class="input-group">
			                		<form:input path="product.weight" disabled="true" id="product_weight" class="form-control"/>
			                		<span class="input-group-addon">lbs</span>
			              	</div>
			   		  	</c:when>
			  		  	<c:otherwise>
			  		  		<div class="input-group">
					  	    		<form:input path="product.weight" id="product_weight" class="form-control"/>
					  	    		<span class="input-group-addon">lbs</span>
				  	    		</div>
			  		  	</c:otherwise>
			  		</c:choose>
	 			</div>
			</div>
		</div>
	</div>
	<div class="line">
		<div class="form-row">
			<div class="col-label">
		    		<label for="product_packageL" class="col-form-label">
					<span class="fa fa-question-circle" data-toggle="tooltip" data-placement="top" title="Package Length::Used to calculate dimensional weight for UPS.Package Length will be allowed 8 integers and 2 decimals, For example,12345678.99."></span><fmt:message key="package" /> <fmt:message key="length" />
				</label>
			</div>
	        <div class="col-lg-4">
	  			<div class="input-group">
		  	    		<form:input path="product.packageL" id="product_packageL" class="form-control"/>
		  	    		<span class="input-group-addon">inch</span>
	  	    		</div>
			</div>
		</div>
	</div>
	<div class="line">
		<div class="form-row">
			<div class="col-label">
		    		<label for="product_packageW" class="col-form-label">
					<span class="fa fa-question-circle" data-toggle="tooltip" data-placement="top" title="Package Width::Used to calculate dimensional weight for UPS.Package Width will be allowed 8 integers and 2 decimals, For emaple,12345678.99."></span><fmt:message key="package" /> <fmt:message key="width" />
				</label>
			</div>
	        <div class="col-lg-4">
				<div class="input-group">
		  			<form:input path="product.packageW" id="product_packageW" class="form-control"/>
		  			<span class="input-group-addon">inch</span>
	  			</div>
			</div>
		</div>
	</div>
	<div class="line">
		<div class="form-row">
			<div class="col-label">
		    		<label for="product_packageH" class="col-form-label">
					<span class="fa fa-question-circle" data-toggle="tooltip" data-placement="top" title="Package Height::Used to calculate dimensional weight for UPS.Package Height will be allowed 8 integers and 2 decimals, For example,12345678.99."></span><fmt:message key="package" /> <fmt:message key="height" />
				</label>
			</div>
	        <div class="col-lg-4">
				<div class="input-group">
		  			<form:input path="product.packageH" id="product_packageH" class="form-control"/>
		  			<span class="input-group-addon">inch</span>
	  			</div>
			</div>
		</div>
	</div>
	<div class="line">
		<div class="form-row">
			<div class="col-label">
		    		<label for="product_upsMaxItemsInPackage" class="col-form-label">
					<span class="fa fa-question-circle" data-toggle="tooltip" data-placement="top" title="Max Items in Package::Used to calculate number of packages for UPS.Items will be allowed 6 integers and 2 decimals, For example,1234.99"></span><fmt:message key="upsMaxItemsInPackage" />
				</label>
			</div>
	        <div class="col-lg-4">
	  			<form:input path="product.upsMaxItemsInPackage" id="product_upsMaxItemsInPackage" class="form-control"/>
			</div>
		</div>
	</div>
	<div class="line">
		<div class="form-row">
			<div class="col-label">
		    		<label for="product_uspsMaxItemsInPackage" class="col-form-label">
					<span class="fa fa-question-circle" data-toggle="tooltip" data-placement="top" title="Max Items in Package::Used to calculate number of packages for USPS.Items will be allowed 6 integers and 2 decimals, For emaple,1234.99."></span><fmt:message key="uspsMaxItemsInPackage" />
				</label>
			</div>
	        <div class="col-lg-4">
	  			<form:input path="product.uspsMaxItemsInPackage" id="product_uspsMaxItemsInPackage" class="form-control"/>
			</div>
		</div>
	</div>
	<div class="line">
		<div class="form-row">
			<div class="col-label">
		    		<label for="product_shippingPackageId" class="col-form-label">
					Shipping Package
				</label>
			</div>
	        <div class="col-lg-4">
				<div class="form-group">
					<form:select path="product.shippingPackageId" id="product_shippingPackageId" class="custom-select">
						<form:option value="${null}"> No Shipping Package</form:option>
						<c:forEach items="${shippingPackageList}" var="option">
							<form:option value="${option.id}" ><c:out value="${option.name}"/></form:option>
						</c:forEach>	
					</form:select>
			    </div>
			</div>
		</div>
	</div>
	<c:if test="${gSiteConfig['gSALES_PROMOTIONS'] and productForm.product.productType != 'REW'}">
	  	<div class="line">
		  	<div class="form-row">
			  	<div class="col-label">
			    		<label for="product_salesTagId" class="col-form-label">
						SalesTag Code
					</label>
				</div>
		        <div class="col-lg-4">
					<form:select path="product.salesTagId" id="product_salesTagId" class="custom-select" >
						<form:option value="${null}"> <fmt:message key='NoSalesTag'/></form:option>
						<c:forEach items="${salesTagCodes}" var="option">
							<form:option value="${option.tagId}" ><c:out value="${option.title}"/></form:option>
						</c:forEach>	
					</form:select>
				</div>
			</div>
		</div>
	</c:if>
	<c:if test="${gSiteConfig['gMAIL_IN_REBATES'] > 0}">
		<div class="line">
			<div class="form-row">
				<div class="col-label">
			    		<label for="product_rebateId" class="col-form-label">
						<fmt:message key="mailInRebates" />
					</label>
				</div>
		        <div class="col-lg-4">
					<div class="form-group">
						<form:select path="product.rebateId" id="product_rebateId" class="custom-select">
							<form:option value="${null}"> <fmt:message key='NoRebate'/></form:option>
							<c:forEach items="${mailInRebates}" var="option">
						  		<form:option value="${option.rebateId}" ><c:out value="${option.title}"/></form:option>
						  	</c:forEach>	    
						</form:select>
				    </div>
				</div>
			</div>
		</div>
	</c:if>
	<c:if test="${gSiteConfig['gMANUFACTURER']}">
		<div class="line">
			<div class="form-row">
				<div class="col-label">
			    		<label for="product_manufactureName" class="col-form-label">
						<fmt:message key="manufacturer" />
					</label>
				</div>
		        <div class="col-lg-4">
					<div class="form-group">
						<c:choose>
				  			<c:when test="${gSiteConfig['gADI']}">
								<form:select disabled="true" path="product.manufactureName" id="product_manufactureName" class="custom-select" >
				   			  		<form:option value="${null}"></form:option>
							  		<c:forEach items="${manufacturers}" var="manufacturer">
							   			<form:option value="${manufacturer.name}" ><c:out value="${manufacturer.name}"/></form:option>
							  		</c:forEach>	    
								</form:select>	
						  	</c:when>
				  		  	<c:otherwise>
					  			<form:select path="product.manufactureName" id="product_manufactureName" class="custom-select" >
				   			  		<form:option value="${null}"></form:option>
							  		<c:forEach items="${manufacturers}" var="manufacturer">
							   			<form:option value="${manufacturer.name}" ><c:out value="${manufacturer.name}"/></form:option>
							  		</c:forEach>	    
								</form:select>	
						  	</c:otherwise>
				  		</c:choose>
					</div> 
				</div>
			</div>
		</div>
	</c:if>
	<c:if test="${gSiteConfig['gMINIMUM_INCREMENTAL_QTY']}">
		<div class="line">
			<div class="form-row">
				<div class="col-label">
			    		<label class="col-form-label">
						<fmt:message key="minimumToOrder" /> / <fmt:message key="incremental" />
					</label>
				</div>
				<div class="col-lg-8">
					<c:choose>
			  			<c:when test="${gSiteConfig['gADI']}">
			  				<div class="row">
				  				<div class="col-lg-2">
				  					<form:input path="product.minimumQty" disabled="true" id="product_minimumQty" class="form-control" maxlength="8"/>
								</div>
								<span class="word-connect"><span>/</span></span>
								<div class="col-lg-2">
									<form:input path="product.incrementalQty" disabled="true" class="form-control" maxlength="8"/>
								</div>
							</div>
			   		  	</c:when>
			  		  	<c:otherwise>
				  		  	<div class="row">
				  		  		<div class="col-lg-2">
				  		  			<form:input path="product.minimumQty" id="product_minimumQty" class="form-control" maxlength="8"/>
								</div>
						        <span class="word-connect"><span>/</span></span>
								<div class="col-lg-2">
									<form:input path="product.incrementalQty" class="form-control" maxlength="8"/>
								</div>
							</div>
						</c:otherwise>
					</c:choose>
		  		</div>
			</div>
		</div>
	</c:if>
	<c:if test="${gSiteConfig['gMYLIST']}">
		<div class="line">
			<div class="form-row">
				<div class="col-label">
			    		<label for="product_addToList" class="col-form-label">
						<span class="fa fa-question-circle" data-toggle="tooltip" data-placement="top" title="List::check it to add to list."></span><fmt:message key="addToList" />
					</label>
				</div>
		        <div class="col-lg-4">
					<div class="form-group">
						<label class="custom-control custom-checkbox">
		     				<form:checkbox path="product.addToList" id="product_addToList" class="custom-control-input" />
		     				<span class="custom-control-indicator"></span>
		     			</label>
	     			</div>
				</div>
			</div>
		</div>
	</c:if>
	<c:if test="${gSiteConfig['gPRESENTATION']}">
		<div class="line">
			<div class="form-row">
				<div class="col-label">
			    		<label for="product_addToPresentation" class="col-form-label">
						<span class="fa fa-question-circle" data-toggle="tooltip" data-placement="top" title="Presentation::check it to add to Presentation."></span><fmt:message key="addToPresentationList" />
					</label>
				</div>
		        <div class="col-lg-4">
					<div class="form-group">
						<label class="custom-control custom-checkbox">
		     				<form:checkbox path="product.addToPresentation" id="product_addToPresentation" class="custom-control-input" />
		     				<span class="custom-control-indicator"></span>
		     			</label>
	     			</div>
				</div>
			</div>
		</div>
	</c:if>
	<c:if test="${gSiteConfig['gCOMPARISON']}">
		<div class="line">
			<div class="form-row">
				<div class="col-label">
			    		<label for="product_compare" class="col-form-label">
						<fmt:message key="compare" />
					</label>
				</div>
		        <div class="col-lg-4">
					<div class="form-group">
						<label class="custom-control custom-checkbox">
		     				<form:checkbox path="product.compare" id="product_compare" class="custom-control-input" />
		     				<span class="custom-control-indicator"></span>
		     			</label>
					</div>
				</div>
			</div>
		</div>
  	</c:if>
  	<div class="line">
	  	<div class="form-row">
	  		<div class="col-label">
		    		<label for="product_searchable" class="col-form-label">
					<span class="fa fa-question-circle" data-toggle="tooltip" data-placement="top" title="Searchable::check it to be searched."></span><fmt:message key="searchable" />
				</label>
			</div>
	        <div class="col-lg-4">
				<div class="form-group">
					<label class="custom-control custom-checkbox">
		    				<form:checkbox path="product.searchable" id="product_searchable" class="custom-control-input" />
		    				<span class="custom-control-indicator"></span>
		    			</label>
			    </div>
			</div>
		</div>
	</div>
	<c:if test="${siteConfig['LUCENE_REINDEX'].value == 'true'}">
		<div class="line">
			<div class="form-row">
				<div class="col-label">
			    		<label for="product_searchRank" class="col-form-label">
						<span class="fa fa-question-circle" data-toggle="tooltip" data-placement="top" title="Searchable::check it to be searched."></span><fmt:message key="searchRank" />
					</label>
				</div>
		        <div class="col-lg-4">
					<div class="form-group">
						<c:choose>
					         <c:when test="${fn:contains(siteConfig['SITE_URL'].value, 'paperenterprise') or fn:contains(siteConfig['SITE_URL'].value, 'unbeatable')}">
						     	<form:input path="product.searchRank" id="product_searchRank" class="form-control" maxlength="16" />
					   		 </c:when>
					         <c:otherwise>
							     <form:select path="product.searchRank" id="product_searchRank" class="form-control">
							  	 	<c:forEach begin="1" end="100" var="rank">
						 				<form:option value="${rank}"><c:out value="${rank}" /></form:option>
							  	    </c:forEach>
						 		</form:select>
					         </c:otherwise>
				       </c:choose>
			       </div>
				</div>
			</div>
		</div>
	</c:if>
	<c:if test="${gSiteConfig['gASI'] != '' or siteConfig['INGRAM'].value != '' or gSiteConfig['gWEBJAGUAR_DATA_FEED'] > 0}">
		<div class="line">
			<div class="form-row">
				<div class="col-label">
			    		<label for="product_feedFreeze" class="col-form-label">
						<span class="fa fa-question-circle" data-toggle="tooltip" data-placement="top" title="Freeze From Feed::Check this flag to freeze this product for future Feed update."></span><fmt:message key="feedFreeze" />
					</label>
				</div>
		        <div class="col-lg-4">
					<div class="form-group">
						<label class="custom-control custom-checkbox">
							<form:checkbox path="product.feedFreeze" id="product_feedFreeze" class="custom-control-input"/>
			    				<span class="custom-control-indicator"></span>
			    			</label>
					</div> 
				</div>
			</div>
		</div>
  	</c:if>
  	<div class="line">
  		<div class="form-row">
	  		<div class="col-label">
		    		<label for="product_hideProduct" class="col-form-label">
					<span class="fa fa-question-circle" data-toggle="tooltip" data-placement="top" title="Hide from adding to order or po."></span>Hide Product
				</label>
			</div>
	        <div class="col-lg-4">
				<div class="form-group">
					<label class="custom-control custom-checkbox">
						<form:checkbox path="product.hideProduct" id="product_hideProduct" class="custom-control-input"/>
		    				<span class="custom-control-indicator"></span>
		    			</label>
				</div> 
			</div>
		</div>
	</div>
	<c:if test="${gSiteConfig['gSHOPPING_CART']}">
		<div class="line">
			<div class="form-row">
				<div class="col-label">
			    		<label for="product_customShippingEnabled" class="col-form-label">
						<span class="fa fa-question-circle" data-toggle="tooltip" data-placement="top" title="Custom Shipping::Check this flag to enable custom shipping for this product."></span><fmt:message key="custom" /> <fmt:message key="shipping" />
					</label>
				</div>
		        <div class="col-lg-4">
					<label class="custom-control custom-checkbox">
						<form:checkbox path="product.customShippingEnabled" id="product_customShippingEnabled" class="custom-control-input"/>
		    				<span class="custom-control-indicator"></span>
		    			</label> 
				</div>
			</div>
		</div>
  	</c:if>
  	<c:if test="${fn:contains(siteConfig['SITE_URL'].value,'church')}">
  		<div class="line">
	  		<div class="form-row">
	  			<div class="col-label">
			    		<label for="product_groundShipping" class="col-form-label">
						Ground Shipping
					</label>
				</div>
		        <div class="col-lg-4">
					<div class="form-group">
						<label class="custom-control custom-checkbox">
							<form:checkbox path="product.groundShipping" id="product_groundShipping" class="custom-control-input"/>
			    				<span class="custom-control-indicator"></span>
			    			</label>
					</div> 
				</div>
			</div>
			<div class="form-row">
	  			<div class="col-lg-4">
				</div>
		        <div class="col-lg-4">
					<small class="help-text">
		   				Ground shipping restriction for Church Partner.
			      	</small>
				</div>
			</div>
		</div>
  	</c:if>
   	<c:if test="${gSiteConfig['gWEATHER_CHANNEL']}">
  		<div class="line">
	  		<div class="form-row">
		  		<div class="col-label">
			    		<label for="product_temperature" class="col-form-label">
						<fmt:message key="temperature" />
					</label>
				</div>
		        <div class="col-lg-4">
					<div class="form-group">
						<div class="input-group">
				  			<form:input path="product.temperature" id="product_temperature" class="form-control"/>
				  			<span class="input-group-addon">F</span>
			  			</div>
					</div>
				</div>
			</div>
		</div>
  	</c:if>
  	<div class="line">
	  	<div class="form-row">
	  		<div class="col-label">
		    		<label for="product_temperature" class="col-form-label">
					<span class="fa fa-question-circle" data-toggle="tooltip" data-placement="top" title="Category Association::Enter Category Ids here. Seperate them with comma or press enter. It helps in searching."></span>Category Association
				</label>
			</div>
	        <div class="col-lg-4">
				<div class="form-group">
					<form:textarea path="product.categoryIds" id="product_temperature" class="form-control" rows="6"/>
			   </div>
			</div>
		</div>
		<div class="form-row">
	  		<div class="col-lg-4">
			</div>
	        <div class="col-lg-4">
				<small class="help-text">
					Enter Category Ids here. Seperate them with comma or press enter. It helps in searching.
			    </small>		
			</div>
		</div>
	</div>
	<div class="line">
		<div class="form-row">
			<div class="col-label">
		    		<label for="product_keywords" class="col-form-label">
					<span class="fa fa-question-circle" data-toggle="tooltip" data-placement="top" title="Keyword::Seperate them with comma or press enter. It helps in searching."></span><fmt:message key="keywords" />
				</label>
			</div>
	        <div class="col-lg-4">
				<div class="form-group">
					<form:textarea path="product.keywords" id="product_keywords" class="form-control" rows="6"/>
		        </div>
			</div>
		</div>
	</div>
	<c:choose>
		<c:when test="${siteConfig['PRODUCT_MULTI_OPTION'].value != 'true'}">
			<div class="line">
				<div class="form-row">
					<div class="col-label">
				    		<label for="product_optionCode" class="col-form-label">
							<span class="fa fa-question-circle" data-toggle="tooltip" data-placement="top" title="Option code::Add the available option code for the product. To manage option code go to Procucts --> Options. MAX 300 character"></span><fmt:message key="option" /> <fmt:message key="code" />
						</label>
					</div>
			        <div class="col-lg-4">
						<div class="form-group">
							<form:input path="product.optionCode" id="product_optionCode" class="form-control" maxlength="300"/>
						</div>
					</div>
				</div>
			</div>
		</c:when>
		<c:otherwise>
			<div class="line">
				<div class="form-row">
					<div class="col-label">
				    		<label for="product_optionCode" class="col-form-label">
							<span class="fa fa-question-circle" data-toggle="tooltip" data-placement="top" title="Option code::Add the available option code for the product. Use multiple option code seperated with comma. To manage option code go to Procucts --> Options."></span><fmt:message key="option" /> <fmt:message key="code" />
						</label>
					</div>
			        <div class="col-lg-4">
						<form:textarea path="product.optionCode" id="product_optionCode" class="form-control" rows="6"/>
					</div>
				</div>
			</div>
		</c:otherwise>
	</c:choose>
  	<c:if test="${gSiteConfig['gPRODUCT_REVIEW'] or siteConfig['PRODUCT_RATE'].value == 'true'}">
  		<div class="line">
	  		<div class="form-row">
	  			<div class="col-label">
			    		<label for="product_enableRate" class="col-form-label">
						<fmt:message key="review" />
					</label>
				</div>
		        <div class="col-lg-4">
					<div class="form-group">
						<label class="custom-control custom-checkbox">
							<form:checkbox path="product.enableRate" id="product_enableRate" class="custom-control-input"/>
			    			</label>
					</div> 
				</div>
			</div>
		</div>
  	</c:if>
  	<c:if test="${gSiteConfig['gPRICE_TIERS'] > 1}">
	  	<div class="line">
		  	<div class="form-row">
		  		<div class="col-label">
			    		<label for="product_crossItemsCode" class="col-form-label">
						<span class="fa fa-question-circle" data-toggle="tooltip" data-placement="top" title="Note::This code will allow qty break discounting on multiple items in a shopping cart."></span><fmt:message key="crossItems" /> <fmt:message key="code" />
					</label>
				</div>
		        <div class="col-lg-4">
					<div class="form-group">
						<form:input path="product.crossItemsCode" id="product_crossItemsCode" class="form-control" maxlength="50"/>
					</div>
				</div>
			</div>
			<div class="form-row">
		  		<div class="col-label">
		  		</div>
		  		<div class="col-lg-4">
		  			<small class="help-text">
						This code will allow qty break discounting on multiple items in a shopping cart. 
				    </small>
		  		</div>
	  		</div>
		</div>
	</c:if>
	<div class="line">
		<div class="form-row">
			<div class="col-label">
		    		<label for="product_aliasSku" class="col-form-label">
					<span class="fa fa-question-circle" data-toggle="tooltip" data-placement="top" title="Note::This sku is used for validation when moving warehouse inventory from sku to sku."></span>Alias Sku
				</label>
			</div>
	        <div class="col-lg-4">
				<div class="form-group">
					<form:input path="product.aliasSku" id="product_aliasSku" class="form-control" maxlength="50"/>
	   			</div>
			</div>
		</div>
		<div class="form-row">
	  		<div class="col-label">
	  		</div>
	  		<div class="col-lg-4">
	  			<small class="help-text">
					This code will be used for validation when moving warehouse inventory.
			    </small>
	  		</div>
  		</div>
	</div>
	<c:if test="${gSiteConfig['gPROTECTED'] > 0}">
		<div class="line">
			<div class="form-row">
				<div class="col-label">
					<label for="product_protectedLevel" class="col-form-label">
						<fmt:message key="protectedLevel" />
					</label>
				</div>
		        <div class="col-lg-4">
					<div class="form-group">
					    	<form:select path="product.protectedLevel" id="product_protectedLevel" class="custom-select">
				      		<form:option value="0"><fmt:message key="none"/></form:option>
					      	<c:forEach begin="1" end="${gSiteConfig['gPROTECTED']}" var="protect">
						    		<c:set var="key" value="protected${protect}"/>
						    		<c:choose>
					    		  		<c:when test="${labels[key] != null and labels[key] != ''}">
					        				<c:set var="label" value="${labels[key]}"/>
					      	  		</c:when>
					      	  		<c:otherwise><c:set var="label" value="${protect}"/></c:otherwise>
					    			</c:choose>		        
				        			<form:option value="${protectedLevels[protect-1]}"><c:out value="${label}"/></form:option>
				      	  	</c:forEach>
				   		</form:select>
			   		</div>
				</div>
			</div>
		</div>
	</c:if>
 	<c:if test="${gSiteConfig['gPRODUCT_EVENT']}">
 		<div class="line">
	 		<div class="form-row">
	 			<div class="col-label">
					<label for="product_eventProtection" class="col-form-label">
						<fmt:message key="event" /> <fmt:message key="protection" />
					</label>
				</div>
		        <div class="col-lg-4">
					<div class="form-group">
					    	<form:select path="product.eventProtection" id="product_eventProtection" class="custom-select">
							<form:option value="0"><fmt:message key="select"/> Protection</form:option>
					      	<form:option value="1">Contest Site Protection</form:option>
					      	<form:option value="2">Zip Code Protection</form:option>
					      	<form:option value="3">Contest Circuit Protection</form:option>
					    	</form:select>
				   </div> 
				</div>
			</div>
		</div>
	</c:if>
	
	<c:if test="${gSiteConfig['gBOX']}">
		<div class="line">
			<div class="form-row">
				<div class="col-label">
					<label for="product_boxSize" class="col-form-label">
						<span class="fa fa-question-circle" data-toggle="tooltip" data-placement="top" title="Note::If no value enter, User can not add this item to shopping cart."></span><fmt:message key="box"/> <fmt:message key="size"/>
					</label>
				</div>
		        <div class="col-lg-4">
					<div class="form-group">
				    		<form:input path="product.boxSize" id="product_boxSize" class="form-control" maxlength="10" />
				    	</div>
				</div>
			</div>
			<div class="form-row">
		  		<div class="col-label">
		  		</div>
		  		<div class="col-lg-4">
		  			<small class="help-text">
						 User must add this amount of item to the box or more. To add price for each extra look at the following field.
				    </small>
		  		</div>
	  		</div>
		</div>
		<div class="line">
			<div class="form-row">
				<div class="col-label">
					<label for="product_boxExtraAmt" class="col-form-label">
						<span class="fa fa-question-circle" data-toggle="tooltip" data-placement="top" title="Note::Customers will charge this amount if they add more than box size."></span><fmt:message key="extraAmount"/>
					</label>
				</div>
		        <div class="col-lg-4">
					<div class="form-group">
						<div class="input-group">
		                		<form:input path="product.boxExtraAmt" id="product_boxExtraAmt" class="form-control" maxlength="10" />
							<span class="input-group-addon"><fmt:message key="${siteConfig['CURRENCY'].value}" /></span>
		              	</div>
			    		</div>
				</div>
			</div>
		</div>
  	</c:if>
  	
  	<c:if test="${gSiteConfig['gCUSTOM_LINES']}">
  		<div class="line">
	  		<div class="form-row">
	  			<div class="col-label">
					<label for="product_numCustomLines" class="col-form-label">
						<span class="fa fa-question-circle" data-toggle="tooltip" data-placement="top" title="Note::Customers will provide more attribute for this product on front end."></span><fmt:message key="numberCustomLine"/>
					</label>
				</div>
		        <div class="col-lg-4">
					<div class="form-group">
					    <form:select id="product_numCustomLines" class="custom-select" path="product.numCustomLines">
						    <option value=""><fmt:message key="none"/></option>
					          	<c:forTokens items="1,2,3,4,5,6,7,8,9,10" delims="," var="current">
						  	<option value="${current}" <c:if test="${current == productForm.product.numCustomLines}">selected</c:if>>${current}</option>
						    </c:forTokens>
			      		</form:select>
					</div> 
				</div>
			</div>
		</div>
		<div class="line">
			<div class="form-row">
				<div class="col-label">
					<label for="product_customLineCharacter" class="col-form-label">
						<span class="fa fa-question-circle" data-toggle="tooltip" data-placement="top" title="Note::Maximum character in custom line."></span><fmt:message key="numberCharacter"/>
					</label>
				</div>
		        <div class="col-lg-4">
					<div class="form-group">
					    <form:select id="product_customLineCharacter" class="custom-select" path="product.customLineCharacter">
				          	<c:forEach begin="5" end="80" step="5" var="current">
								<option value="${current}" <c:if test="${current == productForm.product.customLineCharacter}">selected</c:if>>${current}</option>
						    </c:forEach>
			      		</form:select>
					</div>
				</div>
			</div>
		</div>
  	</c:if>
  	<c:if test="${siteConfig['CUSTOMER_GROUP_GALLERY'].value == 'true'}">
  		<div class="line">
	  		<div class="form-row">
	  			<div class="col-label">
					<label for="product_cdisableImprintOptions" class="col-form-label">
						<fmt:message key="disableImprintoptions"/>
					</label>
				</div>
		        <div class="col-lg-4">
					<div class="form-group">
					    <label class="custom-control custom-checkbox">
					    		<form:checkbox path="product.disableImprintOptions" id="product_cdisableImprintOptions" class="custom-control-input"/>
		                    <span class="custom-control-indicator"></span>
		                </label>
					</div> 
				</div>
			</div>
			<div class="form-row">
		  		<div class="col-lg-4">
		  		</div>
		  		<div class="col-lg-4">
		  			<small class="help-text">
						 No imprint logo selections for this product on front end.
				    </small>
		  		</div>
	  		</div>
		</div>
  	</c:if>
  	<div class="line">
	  	<div class="form-row">
	  		<div class="col-label">
				<label for="product_hazardousTier" class="col-form-label">
					<fmt:message key="hazardousTier"/>
				</label>
			</div>
	        <div class="col-lg-4">
				<div class="form-group">
				    <form:select path="product.hazardousTier" id="product_hazardousTier" class="custom-select">
						<option value=""></option>
			            	<c:forEach begin="1" end="10" var="current">
						  	  <option value="${current}" <c:if test="${current == productForm.product.hazardousTier}">selected</c:if>>${current}</option>
				   	 	</c:forEach>
		        		</form:select>
				</div> 
			</div>
		</div>
	</div>
</div>