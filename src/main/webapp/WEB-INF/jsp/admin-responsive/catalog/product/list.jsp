<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ taglib uri="http://www.springframework.org/security/tags" prefix="sec" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ taglib uri="/WEB-INF/tlds/spring.tld" prefix="spring" %>
<%@ taglib uri="/WEB-INF/tlds/custom.tld" prefix="wj" %>

<tiles:insertDefinition name="admin.responsive-template.catalog.product" flush="true">
	<tiles:putAttribute name="css">
		<style>
			#grid .field-name{
			    width: 350px;
			}
			#grid .field-salestag{
			    width: 120px;
			}
			#grid .field-sku{
			    width: 120px;
			}
			#grid .field-weight{
			    text-align: right;
			}
			#grid .field-price_1{
			    text-align: right;
			}
			#grid .field-created{
			    width: 120px;
			    text-align: center;
			}
			#grid .field-last_modified{
			    width: 120px;
			    text-align: center;
			}
			#grid .field-viewed{
			    width: 140px;
			    text-align: right;
			}
			#grid .field-supplier{
			    text-align: center;
			}
			#grid .field-flags{
			    text-align: center;
			}
		</style>
	</tiles:putAttribute>
	<tiles:putAttribute name="content" type="string">
		<sec:authorize ifAnyGranted="ROLE_AEM,ROLE_ADMIN_${prop_site_id},ROLE_PRODUCT_VIEW_LIST">
			<form action="productList.jhtm" method="post" id="list" name="list_form">
				<div class="page-banner">
					<div class="breadcrumb"><span><a href="${_contextpath}/admin/catalog/dashboard.jhtm">Home</a></span><i class="sli-arrow-right"></i><span>Products</span></div>
				</div>
				<div class="page-head">
					<div class="row justify-content-between">
						<div class="col-sm-8 title">
							<h3>Products</h3>
						</div>
						<div class="col-sm-4 actions">
							<div class="dropdown dropdown-actions">
								<button type="button" class="btn-outline dropdown-toggle active btn btn-primary" data-toggle="dropdown">
		                             <i class="sli-settings"></i> Actions
		                         </button>
		                         <div class="dropdown-menu dropdown-menu-right">
			                        <a class="dropdown-item">New Product</a>
			                        <div tabindex="-1" class="dropdown-divider"></div>
									<c:if test="${fn:contains(siteConfig['SITE_URL'].value, 'rainbow')}">
										<a href="listLimItems.jhtm" class="dropdown-item"><fmt:message key="limItems"/></a>
									</c:if>
									<c:if test="${siteConfig['VARIABLE_PRINT_PDF'].value == 'true'}">
										<a href="pdfBuilderList.jhtm" class="dropdown-item"><fmt:message key="pdfFormBuilder"/></a>
										<a href="pdfDropDownList.jhtm" class="dropdown-item"><fmt:message key="pdfDropDown"/></a>
										<a href="pdfDropDownImport.jhtm" class="dropdown-item"><fmt:message key="dropDown"/> <fmt:message key="import"/></a>
									</c:if>
									<c:if test="${!gSiteConfig['gADI'] and siteConfig['TRAVERS'].value == ''}">
										<a href="productImport.jhtm" class="dropdown-item"><fmt:message key="import"/></a>
										<a href="buildKitImport.jhtm" class="dropdown-item"><fmt:message key="import"/> Kits <br/> <small>[No Inventory Deductions]</small></a>
										<a href="productImageExport.jhtm" class="dropdown-item">Images <br/><small>(AEM ONLY)</small></a>
										<a href="javascript:void(0)" class="dropdown-item" data-target="#export-links" onclick="toggleSubDropdownItems(event,this)"><fmt:message key="export"/></a>
										<div class="sub-menu-items" id="export-links">
       											<a href="javascript: submitSearchForm('xls')" class="dropdown-item">EXCEL</a>
											<a href="javascript: submitSearchForm('csv')" class="dropdown-item">CSV</a>
										</div>
									</c:if>
									<c:choose>
										<c:when test="${gSiteConfig['gPRODUCT_FIELDS_UNLIMITED'] > 0}">
											<a href="productFieldsUnlimited.jhtm" class="dropdown-item"><fmt:message key="Fields"/></a>
											<a href="productUnlimitedExportList.jhtm" class="dropdown-item"><fmt:message key="fieldsExp"/></a>
											<a href="productUnlimitedImport.jhtm" class="dropdown-item"><fmt:message key="fieldsImport"/></a> 
										</c:when>
										<c:otherwise>
											<a href="productFields.jhtm" class="dropdown-item"><fmt:message key="Fields"/></a>
											<a href="productUnlimitedExportList.jhtm" class="dropdown-item"><fmt:message key="fieldsExp"/></a> 
										</c:otherwise>
									</c:choose>
									<a href="options.jhtm" class="dropdown-item"><fmt:message key="options"/></a>
									<a href="cloneProductOption.jhtm" class="dropdown-item"><fmt:message key="clone"/> <fmt:message key="option"/></a>
									<c:if test="${gSiteConfig['gPRODUCT_EVENT']}">
										<a href="productEventList.jhtm" class="dropdown-item"><fmt:message key="eventList"/></a>
									</c:if>
									<c:if test="${gSiteConfig['gIMPRINT_OPTIONS'] > 0}">
										<a href="imprintList.jhtm" class="dropdown-item"><fmt:message key="imprintList"/></a>
									</c:if>
									<c:if test="${siteConfig['NEW_PROMO'].value == 'true'}">
										<sec:authorize ifAnyGranted="ROLE_AEM,ROLE_ADMIN_${prop_site_id}">
											<a href="imprintMethodList.jhtm" class="dropdown-item"><fmt:message key="imprintList"/></a>
											<a href="imprintImport.jhtm" class="dropdown-item"><fmt:message key="imprintImport"/></a>
											<a href="imprintExport.jhtm" class="dropdown-item"><fmt:message key="imprintExport"/></a>
										</sec:authorize>
									</c:if>
									<c:if test="${siteConfig['CUSTOMER_GROUP_MARKUP'].value == 'true'}">
										<a href="markupList.jhtm" class="dropdown-item"><fmt:message key="markupList"/></a>
									</c:if>
									<c:if test="${gSiteConfig['gPRODUCT_REVIEW'] or siteConfig['PRODUCT_RATE'].value == 'true'}">
										<a href="productReviewList.jhtm" class="dropdown-item"><fmt:message key="reviews"/></a>
									</c:if>
									<c:if test="${siteConfig['LUCENE_REINDEX'].value == 'true'}">
										<sec:authorize ifAnyGranted="ROLE_AEM,ROLE_ADMIN_${prop_site_id},ROLE_INDEXING">
											<c:choose>
												<c:when test="${siteConfig['MULTIPLE_CATEGORY_REINDEX'].value == 'true'}">
													<a href="reIndexMain.jhtm" class="dropdown-item">Re-index<c:if test="${siteConfig['LUCENE_LAST_INDEX_DATE'].value != ''}"> (${siteConfig['LUCENE_LAST_INDEX_DATE'].value}) </c:if></a>
												</c:when>
												<c:otherwise>
													<a href="reIndex.jhtm?now" class="dropdown-item">Re-index<c:if test="${siteConfig['LUCENE_LAST_INDEX_DATE'].value != ''}"> (${siteConfig['LUCENE_LAST_INDEX_DATE'].value}) </c:if></a>
												</c:otherwise>
											</c:choose>
											<a href="adminReIndex.jhtm?now" class="dropdown-item">Admin Re-index<c:if test="${siteConfig['LUCENE_ADMIN_LAST_INDEX_DATE'].value != ''}"> (${siteConfig['LUCENE_ADMIN_LAST_INDEX_DATE'].value}) </c:if></a>
											<c:if test="${siteConfig['CUSTOMER_GROUP_MARKUP'].value == 'true'}">
												<a href="categoriesReIndex.jhtm?catReindexNow" class="dropdown-item">Categories Re-index<c:if test="${siteConfig['LUCENE_CATEGORIES_LAST_INDEX_DATE'].value != ''}"> (${siteConfig['LUCENE_CATEGORIES_LAST_INDEX_DATE'].value}) </c:if></a>
											</c:if>
										</sec:authorize>
									</c:if>
		                         </div>
		                    </div>
		                    <div class="dropdown dropdown-setting">
								<button type="button" class="btn-outline dropdown-toggle active btn btn-primary" data-toggle="dropdown">
		                             <i class="sli-screen-desktop"></i> Display
		                         </button>	                         
								<div class="dropdown-menu dropdown-menu-right">
									<div class="form-group">
				                    		<div class="option-header">Rows</div>
										<select class="custom-select form-control" name="size" onchange="document.getElementById('page').value=1;submit();" title="Page Size">
											<c:forTokens items="10,25,50,100,200,500" delims="," var="current">
									  	  		<option value="${current}" <c:if test="${current == productSearch.pageSize}">selected</c:if>>${current}</option>
								  			</c:forTokens>
										</select>
									</div>
									<div class="form-group">
				                    		<div class="option-header">Columns</div>
				                    		<div id="column-hider"></div>
									</div>
								</div>
		                    </div>
		                    <div class="dropdown">
								<button type="button" class="btn-outline dropdown-toggle active btn btn-primary" onclick="$('#search-dialog').modal('show')">
		                            <i class="sli-magnifier"></i>
		                         </button>
							</div>
						</div>
					</div>
				</div>
				<div class="page-body">
					<div class="grid-stage">
						<c:if test="${model.count > 0}">
							<div class="page-stat">
								<fmt:message key="showing">
									<fmt:param value="${productSearch.offset + 1}"/>
									<fmt:param value="${model.pageEnd}"/>
									<fmt:param value="${model.count}"/>
								</fmt:message>
							</div>
					  	</c:if> 
						<table id="grid"></table>
						<div class="footer">
			                <div class="float-right">
								<c:if test="${productSearch.page == 1}">
							 		<span class="previous"><i class="sli-arrow-left" style="font-size: 0.7rem"></i> Previous</span>
								</c:if>
								<c:if test="${productSearch.page != 1}">
									<a href="<c:url value="productList.jhtm"><c:param name="page" value="${productSearch.page-1}"/><c:param name="category" value="${model.search['category']}"/><c:param name="type" value="${param.type}"/></c:url>" >
										<span class="previous"><i class="sli-arrow-left" style="font-size: 0.7rem"></i> Previous</span>
									</a>
								</c:if>
								<span class="status"><c:out value="${productSearch.page}" /> of <c:out value="${model.pageCount}" /></span>
								<c:if test="${model.categories.lastPage}">
								 	<span class="next">Next <i class="sli-arrow-right" style="font-size: 0.7rem"></i></span>
								</c:if>
								<c:if test="${not model.categories.lastPage}">
									<a href="<c:url value="productList.jhtm"><c:param name="page" value="${productSearch.page+1}"/><c:param name="category" value="${model.search['category']}"/><c:param name="type" value="${param.type}"/></c:url>">
										<span class="next">Next <i class="sli-arrow-right" style="font-size: 0.7rem"></i></span>
									</a>
								</c:if>
							</div>
						</div>
					</div>
				</div>
				<input type="hidden" id="sort" name="sort" value="${productSearch.sort}" />
	        		<input type="hidden" name="type" value="${param.type}" />
	        		<input type="hidden" id="page" name="page" value="${productSearch.page}"/>
			</form>
		</sec:authorize>
	</tiles:putAttribute>
	<tiles:putAttribute name="js">
		<!-- DGRID TABLE SCRIPT MUST BE AT BOTTOM OF THE PAGE -->
		<script src="${_contextpath}/admin-responsive-static/assets/plugins/dojo/dojo.js" data-dojo-config="async: true"></script>
		<script>
			//Table Data Sanitization
			var data = [];
			<c:forEach items="${model.products}" var="product" varStatus="status">
				data.push({
					offset: '${status.count + productSearch.offset}.',
					<c:if test="${fn:contains(siteConfig['CUSTOMIZE_PRODUCT_LIST'].value,'name')}">
						name: '${wj:escapeJS(product.name)}',
						url: 'product.jhtm?id=${product.id}&category=${model.search["category"]}',
					</c:if>
					<c:if test="${product.thumbnail != null}">
			      		<c:if test="${not product.thumbnail.absolute}">
			      			thumbnailURL: '../../assets/Image/Product/detailsbig/'+"${wj:replaceUrl(product.thumbnail.imageUrl)}",
			      		</c:if>
		      			<c:if test="${product.thumbnail.absolute}">
		      				thumbnailURL: '${wj:replaceUrl(product.thumbnail.imageUrl)}',
			      		</c:if>
			  		</c:if>
					salestag: '${wj:escapeJS(product.salesTag.title)}',
					id: '${product.id}',
					<c:if test="${fn:contains(siteConfig['CUSTOMIZE_PRODUCT_LIST'].value,'sku')}">
						sku: '<c:out value="${product.sku}" escapeXml="false"/>',
					</c:if>
					<c:if test="${fn:contains(siteConfig['CUSTOMIZE_PRODUCT_LIST'].value,'price')}">
						price_1: '${product.price1}',
					</c:if>
					<c:if test="${gSiteConfig['gMINIMUM_INCREMENTAL_QTY'] and fn:contains(siteConfig['CUSTOMIZE_PRODUCT_LIST'].value,'min_to_order')}">
						minimum_qty: '${product.minimumQty}',
					</c:if>
					<c:if test="${fn:contains(siteConfig['CUSTOMIZE_PRODUCT_LIST'].value,'weight')}">
						weight: '${product.weight}',
					</c:if>
					<c:if test="${fn:contains(siteConfig['CUSTOMIZE_PRODUCT_LIST'].value,'date_added')}">
						created: '<fmt:formatDate type="date" value="${product.created}" timeZone="${siteConfig[\'SITE_TIME_ZONE\'].value}" pattern="M/dd/yy"/>',
					</c:if>
					<c:if test="${fn:contains(siteConfig['CUSTOMIZE_PRODUCT_LIST'].value,'last_modified')}">
						last_modified: '<fmt:formatDate type="date" value="${product.lastModified}" timeZone="${siteConfig[\'SITE_TIME_ZONE\'].value}" pattern="M/dd/yy"/>',
					</c:if>
					<c:if test="${fn:contains(siteConfig['CUSTOMIZE_PRODUCT_LIST'].value,'amazon_category')}">
						<c:set var="catMap" value="${model.amazonCategoryMap}"/>
						amazon_category_id: '<c:out value="${catMap[catId]}"/>',
						amazon_subcategory_id: '<c:out value="${product.amazonSubCategoryId}"/>',
						amazon_category_id_1: '<c:out value="${catMap[catId1]}"/>',
						amazon_subcategory_id_1: '<c:out value="${product.amazonSubCategoryId1}"/>',
				    	</c:if>
				    	<c:if test="${fn:contains(siteConfig['CUSTOMIZE_PRODUCT_LIST'].value,'clicks')}">
				    		viewed: '${product.viewed}',
				    	</c:if>
			    		<c:if test="${fn:contains(siteConfig['CUSTOMIZE_PRODUCT_LIST'].value,'qty_on_po')}">
				    		qtyOnPO: '${product.qtyOnPO}',
			    		</c:if>
			    		<c:if test="${fn:contains(siteConfig['SITE_URL'].value,'bnoticed')}">
				    		field42: '${wj:escapeJS(product.field42)}',
			    		</c:if>	
			    		<c:if test="${fn:contains(siteConfig['CUSTOMIZE_PRODUCT_LIST'].value,'supplier') and gSiteConfig['gSUPPLIER']}">
						encodedSku: '${wj:urlEncode(product.sku)}',
					</c:if>
					<c:if test="${fn:contains(siteConfig['CUSTOMIZE_PRODUCT_LIST'].value,'defaultSupplierName') and gSiteConfig['gSUPPLIER']}">
						defaultSupplierName: '${wj:escapeJS(product.defaultSupplierName)}',
					</c:if>
					<c:if test="${fn:contains(siteConfig['CUSTOMIZE_PRODUCT_LIST'].value,'suppliersku') and gSiteConfig['gSUPPLIER']}">
						defaultSupplierSku: '${wj:escapeJS(product.defaultSupplierSku)}',
					</c:if>
					<c:if test="${fn:contains(siteConfig['CUSTOMIZE_PRODUCT_LIST'].value,'field_41')}">
						field41: '${wj:escapeJS(product.field41)}',
					</c:if>
					<c:if test="${fn:contains(siteConfig['CUSTOMIZE_PRODUCT_LIST'].value,'field_42')}">
						field42: '${wj:escapeJS(product.field42)}',
				    </c:if>
					<c:if test="${fn:contains(siteConfig['CUSTOMIZE_PRODUCT_LIST'].value,'field_43')}">
						field43: '${wj:escapeJS(product.field43)}',
				    </c:if>
					<c:if test="${fn:contains(siteConfig['CUSTOMIZE_PRODUCT_LIST'].value,'field_59')}">
						field59: '${wj:escapeJS(product.field59)}',
				    </c:if>
					<c:if test="${fn:contains(siteConfig['CUSTOMIZE_PRODUCT_LIST'].value,'field_67')}">
						field67: '${wj:escapeJS(product.field67)}',
				    </c:if>
					<c:if test="${gSiteConfig['gPROTECTED'] > 0}">
						protectedLevelAsNumber: '${product.protectedLevelAsNumber}',
					</c:if>
				});
			</c:forEach>
			
			//Columns Definitions
			require(['dojo/_base/lang',
					'dojo/_base/declare',
					'dstore/Memory',
					'dstore/Trackable',
					'dgrid/OnDemandGrid',
					'dgrid/Editor',
					'dgrid/extensions/ColumnHider',
					'dgrid/extensions/ColumnReorder',
					'dgrid/extensions/ColumnResizer',
					'dgrid/Selector',
					'dijit/form/DropDownButton',
					'dijit/DropDownMenu',
					'dijit/MenuItem'
				], function (lang, declare, Memory, Trackable, OnDemandGrid, Editor, ColumnHider, ColumnReorder, ColumnResizer, Selector, DropDownButton, DropDownMenu, MenuItem) {
				var columns = {
					batch: {
						selector: 'checkbox',
						unhidable: true
					},
					offset: {
						label: '#',
						get: function(object){
							return object.offset;
						},
						sortable: false,
						unhidable: true
					},
					thumbnail:{
						label: '',
						renderCell: function(object){
							var img = document.createElement("img");
							img.src = object.thumbnailURL;
							img.className = 'img-thumbnail w-30 h-30';
							img.setAttribute('alt','')
							return img;
						},
					},
					<c:if test="${fn:contains(siteConfig['CUSTOMIZE_PRODUCT_LIST'].value,'name')}">
						name: {
							label: '<fmt:message key="productName" />',
							renderCell: function(object){
								var link = document.createElement("a");
								link.className = 'plain';
								link.text = object.name;
								link.href = object.url;
								return link;
							},
						},
					</c:if>
					salestag: {
						label: '<fmt:message key="salesTag" />',
						get: function(object){
							return object.salestag;
						},
						sortable: false
					},
					id: {
						label: '<fmt:message key="productId" />',
						get: function(object){
							return object.id;
						},
					},
					<c:if test="${fn:contains(siteConfig['CUSTOMIZE_PRODUCT_LIST'].value,'sku')}">
						sku: {
							label: '<fmt:message key="productSku" />',
							get: function(object){
								return object.sku;
							}
						},
					</c:if>
					<c:if test="${fn:contains(siteConfig['CUSTOMIZE_PRODUCT_LIST'].value,'price')}">
						price_1: {
							label: '<fmt:message key="productPrice" />',
							get: function(object){
								return object.price_1;
							}
						},
					</c:if>
					<c:if test="${gSiteConfig['gMINIMUM_INCREMENTAL_QTY'] and fn:contains(siteConfig['CUSTOMIZE_PRODUCT_LIST'].value,'min_to_order')}">
						minimum_qty: {
							label: '<fmt:message key="moq" />',
							get: function(object){
								return object.minimum_qty;
							}
						},
					</c:if>
					<c:if test="${fn:contains(siteConfig['CUSTOMIZE_PRODUCT_LIST'].value,'weight')}">
						weight: {
							label: '<fmt:message key="productWeight" />',
							get: function(object){
								return object.weight;
							}
						},
					</c:if>
					<c:if test="${fn:contains(siteConfig['CUSTOMIZE_PRODUCT_LIST'].value,'date_added')}">
						created: {
							label: '<fmt:message key="dateAdded" />',
							get: function(object){
								return object.created;
							}
						},
					</c:if>
					<c:if test="${fn:contains(siteConfig['CUSTOMIZE_PRODUCT_LIST'].value,'last_modified')}">
						last_modified: {
							label: '<fmt:message key="lastModified" />',
							get: function(object){
								return object.last_modified;
							}
						},
					</c:if>
					<c:if test="${fn:contains(siteConfig['CUSTOMIZE_PRODUCT_LIST'].value,'amazon_category')}">
						amazon_category_id: {
							label: '<fmt:message key="amazonCategory" />',
							get: function(object){
								return object.amazon_category_id;
							}
						},
						amazon_subcategory_id: {
							label: '<fmt:message key="amazonSubCategory" />',
							get: function(object){
								return object.amazon_subcategory_id;
							}
						},
						amazon_category_id_1: {
							label: '<fmt:message key="amazonCategory" />',
							get: function(object){
								return object.amazon_category_id_1;
							}
						},
						amazon_subcategory_id_1: {
							label: '<fmt:message key="amazonSubCategory" />',
							get: function(object){
								return object.amazon_subcategory_id_1;
							}
						},
					</c:if>
					<c:if test="${fn:contains(siteConfig['CUSTOMIZE_PRODUCT_LIST'].value,'clicks')}">
						viewed: {
							label: '<fmt:message key="numOfClicks" />',
							get: function(object){
								return object.viewed;
							}
						},
				    </c:if>
					<!-- MISSING ON HAND / AVAILABLE  -->
					<c:if test="${fn:contains(siteConfig['CUSTOMIZE_PRODUCT_LIST'].value,'qty_on_po')}">
						qtyOnPO: {
							label: 'On PO',
							get: function(object){
								return object.qtyOnPO;
							},
							sortable: false
						},
			    		</c:if>
					<c:if test="${fn:contains(siteConfig['SITE_URL'].value,'bnoticed')}">
						field42:{
							label: 'ASI Product Number',
							get: function(object){
								return object.field42;
							},
							sortable: false
						},
					</c:if>
				    <c:if test="${fn:contains(siteConfig['CUSTOMIZE_PRODUCT_LIST'].value,'supplier') and gSiteConfig['gSUPPLIER']}">
						supplier:{
							label: '<fmt:message key="supplier" />',
						    renderCell: function(object, data, td, options){
								var link = document.createElement("a");
								link.text = '<fmt:message key="supplier" />';
								link.href = "productSupplierList.jhtm?sku="+ object.encodedSku;
								return link;
							},
							sortable: false
						},
				    </c:if>
					<c:if test="${fn:contains(siteConfig['CUSTOMIZE_PRODUCT_LIST'].value,'defaultSupplierName') and gSiteConfig['gSUPPLIER']}">
						defaultSupplierName:{
							label: '<fmt:message key="primarySupplier" />',
						    get: function(object){
								return object.defaultSupplierName;
							},
							sortable: false
						},
					</c:if>
					<c:if test="${fn:contains(siteConfig['CUSTOMIZE_PRODUCT_LIST'].value,'suppliersku') and gSiteConfig['gSUPPLIER']}">
						defaultSupplierSku:{
							label: '<fmt:message key="supplierSku" />',
						    get: function(object){
								return object.defaultSupplierSku;
							},
							sortable: false
						},
					</c:if>
					<!-- MISSING ON RANK  -->
					<c:if test="${fn:contains(siteConfig['CUSTOMIZE_PRODUCT_LIST'].value,'field_41')}">
						field41:{
							label: 'model.idProductFieldMap[41].name',
						    get: function(object){
								return object.field41;
							},
							sortable: false
						},
				    </c:if>
					<c:if test="${fn:contains(siteConfig['CUSTOMIZE_PRODUCT_LIST'].value,'field_42')}">
						field42:{
							label: 'model.idProductFieldMap[42].name',
						    get: function(object){
								return object.field42;
							},
							sortable: false
						},
					</c:if>
					<c:if test="${fn:contains(siteConfig['CUSTOMIZE_PRODUCT_LIST'].value,'field_43')}">
						field43:{
							label: 'model.idProductFieldMap[43].name',
						    get: function(object){
								return object.field43;
							},
							sortable: false
						},
					</c:if>
					<c:if test="${fn:contains(siteConfig['CUSTOMIZE_PRODUCT_LIST'].value,'field_59')}">
						field59:{
							label: 'model.idProductFieldMap[59].name',
						    get: function(object){
								return object.field59;
							}
						},
					</c:if>
					<c:if test="${fn:contains(siteConfig['CUSTOMIZE_PRODUCT_LIST'].value,'field_67')}">
						field67:{
							label: 'model.idProductFieldMap[67].name',
						    get: function(object){
								return object.field67;
							},
							sortable: false
						},
					</c:if>
					flags:{
						label: 'Flags',
						renderCell: function(object, data, td, options){
							var div = document.createElement("div");
							<c:if test="${gSiteConfig['gPROTECTED'] > 0}">
								if(object.protectedLevelAsNumber != 0){
									var span = document.createElement("span");
									span.className = 'fa fa-lock';
									var sup = document.createElement("sup");
									sup.text = object.protectedLevelAsNumber;
									div.appendChild(span);
									div.appendChild(sup);
								}
							</c:if>
							return div;
						},
						sortable: false
					}
				};
				var store = new (declare([Memory, Trackable]))({
					data: data,
					idProperty:"id"
				});
				var grid = new (declare([OnDemandGrid, Editor, ColumnHider, ColumnReorder, ColumnResizer, Selector]))({
					collection: store,
					selectionMode: "none",
					allowSelectAll: true,
					columns: columns
				},'grid');
				
				//sorting columns
				grid.on("dgrid-sort", lang.hitch(this, function(e){
					e.preventDefault();						
					var property = e.sort[0].property;
				    var order = e.sort[0].descending ? "desc" : "asc";
				    document.getElementById('sort').value = property + ' ' + order;
				    document.getElementById('list').submit();
				}));
				
				<c:if test="${productSearch.sort == 'name desc'}">
					grid.updateSortArrow([{property: 'name', descending: true}],true);
				</c:if>
				<c:if test="${productSearch.sort == 'name asc'}">
					grid.updateSortArrow([{property: 'name', ascending: true}],true);
				</c:if>
				
				<c:if test="${productSearch.sort == 'id desc'}">
					grid.updateSortArrow([{property: 'id', descending: true}],true);
				</c:if>
				<c:if test="${productSearch.sort == 'id asc'}">
					grid.updateSortArrow([{property: 'id', ascending: true}],true);
				</c:if>
				
				<c:if test="${productSearch.sort == 'sku desc'}">
					grid.updateSortArrow([{property: 'sku', descending: true}],true);
				</c:if>
				<c:if test="${productSearch.sort == 'sku asc'}">
					grid.updateSortArrow([{property: 'sku', ascending: true}],true);
				</c:if>
				
				<c:if test="${productSearch.sort == 'price_1 desc'}">
					grid.updateSortArrow([{property: 'price_1', descending: true}],true);
				</c:if>
				<c:if test="${productSearch.sort == 'price_1 asc'}">
					grid.updateSortArrow([{property: 'price_1', ascending: true}],true);
				</c:if>
				
				<c:if test="${productSearch.sort == 'minimum_qty desc'}">
					grid.updateSortArrow([{property: 'minimum_qty', descending: true}],true);
				</c:if>
				<c:if test="${productSearch.sort == 'minimum_qty asc'}">
					grid.updateSortArrow([{property: 'minimum_qty', ascending: true}],true);
				</c:if>
				
				<c:if test="${productSearch.sort == 'weight desc'}">
					grid.updateSortArrow([{property: 'weight', descending: true}],true);
				</c:if>
				<c:if test="${productSearch.sort == 'weight asc'}">
					grid.updateSortArrow([{property: 'weight', ascending: true}],true);
				</c:if>
				
				<c:if test="${productSearch.sort == 'created desc'}">
					grid.updateSortArrow([{property: 'created', descending: true}],true);
				</c:if>
				<c:if test="${productSearch.sort == 'created asc'}">
					grid.updateSortArrow([{property: 'created', ascending: true}],true);
				</c:if>
				
				<c:if test="${productSearch.sort == 'last_modified desc'}">
					grid.updateSortArrow([{property: 'last_modified', descending: true}],true);
				</c:if>
				<c:if test="${productSearch.sort == 'last_modified asc'}">
					grid.updateSortArrow([{property: 'last_modified', ascending: true}],true);
				</c:if>
				
				<c:if test="${productSearch.sort == 'amazon_category_id desc'}">
					grid.updateSortArrow([{property: 'amazon_category_id', descending: true}],true);
				</c:if>
				<c:if test="${productSearch.sort == 'amazon_category_id asc'}">
					grid.updateSortArrow([{property: 'amazon_category_id', ascending: true}],true);
				</c:if>
			
				<c:if test="${productSearch.sort == 'amazon_subcategory_id desc'}">
					grid.updateSortArrow([{property: 'amazon_subcategory_id', descending: true}],true);
				</c:if>
				<c:if test="${productSearch.sort == 'amazon_subcategory_id asc'}">
					grid.updateSortArrow([{property: 'amazon_subcategory_id', ascending: true}],true);
				</c:if>
			
				<c:if test="${productSearch.sort == 'amazon_category_id_1 desc'}">
					grid.updateSortArrow([{property: 'amazon_category_id_1', descending: true}],true);
				</c:if>
				<c:if test="${productSearch.sort == 'amazon_category_id_1 asc'}">
					grid.updateSortArrow([{property: 'amazon_category_id_1', ascending: true}],true);
				</c:if>
		
				<c:if test="${productSearch.sort == 'amazon_subcategory_id_1 desc'}">
					grid.updateSortArrow([{property: 'amazon_subcategory_id_1', descending: true}],true);
				</c:if>
				<c:if test="${productSearch.sort == 'amazon_subcategory_id_1 asc'}">
					grid.updateSortArrow([{property: 'amazon_subcategory_id_1', ascending: true}],true);
				</c:if>
				
				<c:if test="${productSearch.sort == 'viewed desc'}">
					grid.updateSortArrow([{property: 'viewed', descending: true}],true);
				</c:if>
				<c:if test="${productSearch.sort == 'viewed asc'}">
					grid.updateSortArrow([{property: 'viewed', ascending: true}],true);
				</c:if>
				
				grid.on('dgrid-refresh-complete', function(event) {
					$('#grid-hider-menu').appendTo('#column-hider');
				});
				
				grid.startup();
			});
		</script>
	</tiles:putAttribute>
</tiles:insertDefinition>
