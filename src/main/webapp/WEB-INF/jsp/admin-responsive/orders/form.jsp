<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ taglib uri="http://www.springframework.org/security/tags" prefix="sec" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ taglib uri="/WEB-INF/tlds/spring.tld" prefix="spring" %>
<%@ taglib uri="/WEB-INF/tlds/custom.tld" prefix="wj" %>

<tiles:insertDefinition name="admin.responsive-template.orderForm" flush="true">
	<tiles:putAttribute name="css">
		<style>
			#grid .field-grandTotal{
			    text-align: right;
			}
			#grid .field-paid{
			    text-align: right;
			}
			#grid .field-due{
			    text-align: right;
			}
		</style>
	</tiles:putAttribute>
	<tiles:putAttribute name="content" type="string">
		<form:form commandName="invoiceForm" method="post">
			<input type="hidden" name="WantsBondField" value="${invoiceForm.order.wantsBond}" id="WantsBondField">
			<c:set var="pattern" value="#,##0.00"/>
			<c:set var="negpattern" value="-#,##0.00"/>
			<c:if test="${siteConfig['MAX_FRACTION_DIGIT'].value == '3'}">
				<c:set var="pattern" value="#,##0.000"/>
				<c:set var="negpattern" value="-#,##0.000"/>
			</c:if>
			<c:if test="${siteConfig['MAX_FRACTION_DIGIT'].value == '4'}">
				<c:set var="pattern" value="#,##0.0000"/>
				<c:set var="negpattern" value="-#,##0.0000"/>
			</c:if>
			<c:if test="${siteConfig['MAX_FRACTION_DIGIT'].value == '5'}">
				<c:set var="pattern" value="#,##0.00000"/>
				<c:set var="negpattern" value="-#,##0.00000"/>
			</c:if>
			<c:if test="${invoiceForm.order != null}">
				<spring:bind path="invoiceForm.order.orderId">
					<input type="hidden" name="${status.expression}" value="${status.value}">
				</spring:bind>
				<div class="page-banner">
					<div class="breadcrumb">
						<span><a href="${_contextpath}/admin/catalog/dashboard.jhtm">Home</a></span>
						<c:choose>
							<c:when test="${(gSiteConfig['gSHIPPING_QUOTE'] or siteConfig['PRODUCT_QUOTE'].value == 'true') and orderName == 'quotes'}">	        	
								<i class="sli-arrow-right"></i><span><a href="../orders/quotesList.jhtm">Quotes</a></span>
							</c:when>
							<c:otherwise>		       
								<i class="sli-arrow-right"></i><span><a href="../orders">Orders</a></span>
							</c:otherwise>	        
						</c:choose>	
						<i class="sli-arrow-right"></i><span>Order #${invoiceForm.order.orderId}</span>
					</div>
				</div>
				<div class="page-body">
					<div class="row justify-content-between statistic-banner">
						<c:if test="${!invoiceForm.newInvoice}">
							<c:choose>
								<c:when test="${siteConfig['CHANGE_ORDER_DATE'].value == 'true'}">
									<div class="col-sm-4 col-lg-2">
										<label class="col-form-label"><fmt:message key="${orderOrQuote }" /> <fmt:message key="date" /></label>
										<p>
											<form:input path="orderDate" readonly="true" class="form-control"/>
										</p>
									</div>
								</c:when>
								<c:otherwise>
									<div class="col-sm-4 col-lg-2">
										<label class="col-form-label"><fmt:message key="orderDate" /></label>
										<p>
											<fmt:formatDate type="both" timeZone="${siteConfig['SITE_TIME_ZONE'].value}" value="${invoiceForm.order.dateOrdered}"/>
										</p>
									</div>
								</c:otherwise>
							</c:choose>
							<c:if test="${siteConfig['DELIVERY_TIME'].value == 'true'}">
								<div class="col-sm-4 col-lg-2">
									<label class="col-form-label">Days to Ship</label>
									<p>
										<form:select path="order.shippingPeriod" class="custom-select">
											<c:forEach begin="0" end="10" step="1" var="value">
												<form:option value="${value}"><c:out value="${value}" /> Day</form:option>
											</c:forEach>
										</form:select>
									</p>
								</div>  
								<div class="col-sm-4 col-lg-2">
									<label class="col-form-label">Turn Over Day</label>
									<p>
										<form:select path="order.turnOverday" class="custom-select">
											<c:forEach begin="0" end="10" step="1" var="value">
												<form:option value="${value}"><c:out value="${value}" /> Day</form:option>
											</c:forEach>
										</form:select>
									</p>
								</div>  
							</c:if>
						</c:if>
						<c:if test="${orderOrQuote == 'quote'}">
							<div class="col-sm-4 col-lg-2">
								<label class="col-form-label"><fmt:message key="expectedCloseDate"/></label>
								<p>
				  					<form:input path="order.expectedCloseDate" size="18"/>
			  					</p>
		  					</div>
						</c:if>
						<div class="col-sm-4 col-lg-2">
							<label class="col-form-label">
								<fmt:message key="lastModified" />
							</label>
							<p>
								<fmt:formatDate type="both" timeZone="${siteConfig['SITE_TIME_ZONE'].value}" value="${invoiceForm.order.lastModified}"/>
							</p>
						</div>
						<div class="col-sm-4 col-lg-2">
							<label class="col-form-label">
								<fmt:message key="nextActivityDate"/>
							</label>
							<p>
								<form:input path="order.nextActivityDate" class="form-control"/>
							</p>
						</div>
					</div>
					<div class="main-content">
					  	<div class="form-section">
							<div class="content">
								<ul class="nav nav-tabs" role="tablist">
									<li class="nav-item">
										<a class="nav-link active" data-toggle="tab" role="tab" href="#order_tab"><fmt:message key="${orderName}"/></a>
									</li>
									<c:if test="${gSiteConfig['gPAYMENTS'] and orderName != 'quotes'}">
										<li class="nav-item">
											<a class="nav-link" data-toggle="tab" role="tab" href="#order_open_invoice_tab">Open Invoices</a>
										</li>
									</c:if>
								</ul>
							</div>
							<div class="tab-content">
								<c:import url="/WEB-INF/jsp/admin-responsive/orders/formTabs/order_tab.jsp" />
								<c:if test="${gSiteConfig['gPAYMENTS'] and orderName != 'quotes'}">
									<c:import url="/WEB-INF/jsp/admin-responsive/orders/formTabs/open_invoice_tab.jsp" />
								</c:if>
							</div>
		                </div>
	                </div>
				</div>
			</c:if>
		</form:form>
	</tiles:putAttribute>
	<tiles:putAttribute name="js">
		<!-- DGRID TABLE SCRIPT MUST BE AT BOTTOM OF THE PAGE -->
		<script src="${_contextpath}/admin-responsive-static/assets/plugins/dojo/dojo.js" data-dojo-config="async: true"></script>
		<script>
			//Table Data Sanitization
			var data = [];
			<c:set var="totalGrandTotal" value="0.0" />
			<c:set var="totalAmtPaid" value="0.0" />
			<c:set var="totalAmtDue" value="0.0" />
			<c:forEach items="${openInvoices}" var="openInvoice" varStatus="status">
				data.push({
					id: '${openInvoice.orderId}',
					offset: '<c:out value="${status.index + 1}"/>.',
					orderDate: '<fmt:formatDate type="date" timeStyle="default" timeZone="${siteConfig[\'SITE_TIME_ZONE\'].value}" value="${openInvoice.dateOrdered}"/>',
					status: '<c:choose><c:when test="${openInvoice.status != null}"><fmt:message key="orderStatus_${openInvoice.status}" /></c:when><c:otherwise></c:otherwise></c:choose>',
					grandTotal: '<fmt:formatNumber value="${openInvoice.grandTotal}" pattern="${pattern}" />',
					paid: '<fmt:formatNumber value="${openInvoice.amountPaid}" pattern="${pattern}" />',
					due: '<fmt:formatNumber value="${openInvoice.balance}" pattern="${pattern}" />',
				});
				<c:if test="${openInvoice.grandTotal != null}">
					<c:set var="totalGrandTotal" value="${openInvoice.grandTotal + totalGrandTotal}" />
				</c:if>
				<c:if test="${openInvoice.amountPaid != null}">
					<c:set var="totalAmtPaid" value="${openInvoice.amountPaid + totalAmtPaid}" />
				</c:if>
				<c:if test="${openInvoice.balance != null}">
					<c:set var="totalAmtDue" value="${openInvoice.balance + totalAmtDue}" />
				</c:if>
			</c:forEach>

			//Columns Definitions
			require(['dojo/_base/lang',
					'dojo/_base/declare',
					'dstore/Memory',
					'dstore/Trackable',
					'dgrid/OnDemandGrid',
					'dgrid/Editor',
					'dgrid/extensions/ColumnHider',
					'dgrid/extensions/ColumnReorder',
					'dgrid/extensions/ColumnResizer',
					'dgrid/Selector',
					'dijit/form/DropDownButton',
					'dijit/DropDownMenu',
					'dijit/MenuItem',
					'summary/SummaryRow'
				], function (lang, declare, Memory, Trackable, OnDemandGrid, Editor, ColumnHider, ColumnReorder, ColumnResizer, Selector, DropDownButton, DropDownMenu, MenuItem, SummaryRow) {
				var columns = {
					offset: {
						label: '#',
						get: function(object){
							return object.offset;
						},
						sortable: false,
					},
					orderNumber: {
						label: '<fmt:message key="orderNumber" />',
						renderCell: function(object){
							var link = document.createElement('a');
							link.href = "../orders/invoice.jhtm?order="+object.id;
							link.textContent = object.id;
							return link;
						},
						sortable: false,
					},
					orderDate: {
						label: '<fmt:message key="orderDate" />',
						get: function(object){
							return object.orderDate;
						},
						sortable: false,
					},
					status: {
						label: '<fmt:message key="status" />',
						get: function(object){
							return object.status;
						},
						sortable: false,
					},
					grandTotal: {
						label: '<fmt:message key="grandTotal" />',
						get: function(object){
							return object.grandTotal;
						},
						sortable: false,
					},
					paid: {
						label: '<fmt:message key="paid" />',
						get: function(object){
							return object.paid;
						},
						sortable: false,
					},
					due: {
						label: '<fmt:message key="due" />',
						get: function(object){
							return object.due;
						},
						sortable: false,
					},
				};
				var store = new (declare([Memory, Trackable]))({
					data: data,
					idProperty: "id"
				});
				
				var grid = new (declare([OnDemandGrid, Editor, ColumnReorder, ColumnResizer, Selector, SummaryRow]))({
					collection: store,
					selectionMode: "none",
					allowSelectAll: true,
					columns: columns
				},'grid');
				
				var totals = [
					{
						status: '<fmt:message key="total" />',
						grandTotal: '<fmt:formatNumber value="${totalGrandTotal}" pattern="${pattern}"/>',
						paid: '<fmt:formatNumber value="${totalAmtPaid}" pattern="${pattern}" />',
						due: '<fmt:formatNumber value="${totalAmtDue}" pattern="${pattern}" />'
					}
				];
				
				grid.set('summary', totals);
				
				grid.on('dgrid-refresh-complete', function(event) {
					$('#grid-hider-menu').appendTo('#column-hider');
				});
				
				grid.startup();
			});
		</script>
	</tiles:putAttribute>
</tiles:insertDefinition>