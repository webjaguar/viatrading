<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ taglib uri="http://www.springframework.org/security/tags" prefix="sec" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ taglib uri="/WEB-INF/tlds/spring.tld" prefix="spring" %>
<%@ taglib uri="/WEB-INF/tlds/custom.tld" prefix="wj" %>

<tiles:insertDefinition name="admin.responsive-template.order" flush="true">
	<tiles:putAttribute name="css">
		<link rel="stylesheet" href="${_contextpath}/admin-responsive-static/assets/css/invoice.css">
	</tiles:putAttribute>
	<tiles:putAttribute name="content" type="string">
		<sec:authorize ifAnyGranted="ROLE_AEM,ROLE_ADMIN_${prop_site_id},ROLE_INVOICE_VIEW_LIST,ROLE_INVOICE_EDIT,ROLE_INVOICE_STATUS">
			<c:if test="${order != null}">
				<div class="page-form">
					<div class="page-banner">
						<div class="breadcrumb"><span><a href="${_contextpath}/admin/catalog/dashboard.jhtm">Home</a></span><i class="sli-arrow-right"></i><span><a href="${_contextpath}/admin/orders">Orders</a></span><i class="sli-arrow-right"></i><span>Order #<c:out value="${order.orderId}"/></span></div>
					</div>
					<div class="page-head">
						<form action="invoice.jhtm" method="get" id="invoiceTools">
							<div class="row">
								<div class="col-12">
									<h2>Invoice: <c:out value="${order.orderId}"/></h2>
								</div>
							</div>
							<div class="row sub-status">
								<div class="col-lg-8">
									<span><fmt:message key="status"/>: <b><fmt:message key="${order.encodeStatus}"/></b></span>
								</div>
								<div class="col-lg-4">
									<div class="tab-actions">
										<c:set var="showEdit" value="true"/>  	    
										<sec:authorize ifAnyGranted="ROLE_AEM,ROLE_ADMIN_${prop_site_id},ROLE_SHIPPED_INVOICE_EDIT">
											<c:if test="${(siteConfig['ALLOW_EDIT_INVOICE_SHIPPED'].value == 'true' or !(order.status == 's' || order.status == 'x')) and (order.paymentMethod == null || order.paymentMethod != 'Amazon' || (order.paymentMethod == 'Amazon' and siteConfig['AMAZON_ORDER_ALLOW_EDIT'].value == 'true'))}"> 
												<c:set var="showEdit" value="false"/> 
												<a class="action-item action" href="<c:url value="invoiceForm.jhtm"><c:param name="orderId" value="${order.orderId}"/></c:url>">
													<i class="sli-note"></i>
												</a>
											</c:if>
										</sec:authorize>
										<c:if test="${showEdit}">
											<sec:authorize ifAllGranted="ROLE_INVOICE_EDIT">
												<c:if test="${!(order.status == 's' || order.status == 'x') and (order.paymentMethod == null || order.paymentMethod != 'Amazon' || (order.paymentMethod == 'Amazon' and siteConfig['AMAZON_ORDER_ALLOW_EDIT'].value == 'true'))}"> 
													<a class="action-item action" href="<c:url value="invoiceForm.jhtm"><c:param name="orderId" value="${order.orderId}"/></c:url>">
														<i class="sli-note"></i>
													</a> 
												</c:if>
											</sec:authorize>
										</c:if>
										<c:if test="${gSiteConfig['gPAYMENTS'] and !(siteConfig['ALLOW_EDIT_PAYMENT_SHIPPED'].value == 'false' || order.status == 'x')}">
											<div class="action-item dropdown">
												<a class="dropdown-toggle" data-toggle="dropdown">
													<i class="sli-credit-card"></i>
												</a>
												<div class="dropdown-menu dropdown-menu-right">
													<sec:authorize ifAnyGranted="ROLE_AEM,ROLE_ADMIN_${prop_site_id},ROLE_INCOMING_PAYMENT_CREATE">
														<c:if test="${(order.grandTotal - order.amountPaid) > 0}">
															<a class="dropdown-item" href="../customers/payment.jhtm?cid=${order.userId}&orderId=${order.orderId}" ><fmt:message key="add" /> <fmt:message key="payment"/></a>
														</c:if>
													</sec:authorize>
															  	       		
													<sec:authorize ifAnyGranted="ROLE_AEM,ROLE_ADMIN_${prop_site_id},ROLE_INCOMING_PAYMENT_CANCEL">
														<c:if test="${order.paymentHistory != null and !empty order.paymentHistory and fn:length(order.paymentHistory) gt 0}"> 
															<a class="dropdown-item" href="../customers/cancelPayments.jhtm?orderId=${order.orderId}&cid=${order.userId}" >
																<fmt:message key="cancel" /> <fmt:message key="payment"/>
															</a>
								               		 	</c:if>
													</sec:authorize>
												</div>
											</div>
										</c:if>
										<c:choose>
											<c:when test="${!order.paymentAlert}">
												<div class="action-item dropdown">
													<a class="dropdown-toggle" data-toggle="dropdown">
														<i class="sli-printer"></i>
													</a>
													<div class="dropdown-menu dropdown-menu-right">
														<a class="dropdown-item" href="invoice.jhtm?order=${order.orderId}&__print=1"><fmt:message key="printerFriendly" /></a>
														<c:if test="${gSiteConfig['gSITE_DOMAIN'] == 'tcintlinc.com'}">
															<a class="dropdown-item" href="invoice.jhtm?order=${order.orderId}&__print=2"><fmt:message key="printerFriendly" /> 2</a>
															<a href="invoice.jhtm?order=${order.orderId}&__print=3"><fmt:message key="pickingSheet" /></a>
														</c:if>
														<a class="dropdown-item" href="invoice.jhtm?order=${order.orderId}&__label=true"><fmt:message key="label" /></a>
														<a class="dropdown-item" href="invoice.jhtm?order=${order.orderId}&__printDiscount=true"><fmt:message key="showDiscount" /></a>
														<a class="dropdown-item" href="invoice.jhtm?order=${order.orderId}&__packing=true"><fmt:message key="blankPacking" /></a>
														<c:if test="${siteConfig['ORDER_OVERVIEW'].value == 'true'}">
															<a class="dropdown-item" href="invoice.jhtm?order=${order.orderId}&__overview=true"><fmt:message key="overview" /></a>
														</c:if> 
													</div>
												</div>
											</c:when>
											<c:otherwise>
												<sec:authorize ifAnyGranted="ROLE_AEM,ROLE_ADMIN_${prop_site_id},ROLE_OVERRIDE_PAYMENT_ALERT">
													<div class="action-item dropdown">
														<a class="dropdown-toggle" data-toggle="dropdown">
															<i class="sli-printer"></i>
														</a>
														<div class="dropdown-menu dropdown-menu-right">
															<a class="dropdown-item" href="invoice.jhtm?order=${order.orderId}&__print=1"><fmt:message key="printerFriendly" /></a>
															<c:if test="${gSiteConfig['gSITE_DOMAIN'] == 'tcintlinc.com'}">
																<a class="dropdown-item" href="invoice.jhtm?order=${order.orderId}&__print=2"><fmt:message key="printerFriendly" /> 2</a>
																<a href="invoice.jhtm?order=${order.orderId}&__print=3"><fmt:message key="pickingSheet" /></a>
															</c:if>
															<a class="dropdown-item" href="invoice.jhtm?order=${order.orderId}&__label=true"><fmt:message key="label" /></a>
															<a class="dropdown-item" href="invoice.jhtm?order=${order.orderId}&__printDiscount=true"><fmt:message key="showDiscount" /></a>
															<a class="dropdown-item" href="invoice.jhtm?order=${order.orderId}&__packing=true"><fmt:message key="blankPacking" /></a>
															<c:if test="${siteConfig['ORDER_OVERVIEW'].value == 'true'}">
																<a class="dropdown-item" href="invoice.jhtm?order=${order.orderId}&__overview=true"><fmt:message key="overview" /></a>
															</c:if> 
														</div>
													</div> 
												</sec:authorize>
											</c:otherwise>
										</c:choose>
										<sec:authorize ifAnyGranted="ROLE_AEM,ROLE_ADMIN_${prop_site_id},ROLE_INVOICE_PACKING_LIST,ROLE_DROP_SHIP_CREATE,ROLE_PURCHASE_ORDER_CREATE">
								    			<c:if test="${(!order.orderCancel) and (siteConfig['GENERATE_PACKING_LIST'].value == 'true' or (gSiteConfig['gINVENTORY'] and gSiteConfig['gPURCHASE_ORDER']))}">
												<div class="action-item dropdown">
													<a class="dropdown-toggle" data-toggle="dropdown">
														<i class="sli-settings"></i>
													</a>
													<div class="dropdown-menu dropdown-menu-right">
														<sec:authorize ifAnyGranted="ROLE_AEM,ROLE_ADMIN_${prop_site_id},ROLE_INVOICE_PACKING_LIST">
															<c:if test="${siteConfig['GENERATE_PACKING_LIST'].value == 'true'}"> 
																<a class="dropdown-item" href="generatePackingList.jhtm?orderId=${order.orderId}"><fmt:message key="packingList" /></a>
															</c:if>
														</sec:authorize>
														<c:if test="${siteConfig['GENERATE_PACKING_LIST'].value == 'true'}"> 
								           					<a class="dropdown-item" href="generatePickTicket.jhtm?orderId=${order.orderId}">Pick Ticket</a>
								         				</c:if>
														<c:if test="${gSiteConfig['gINVENTORY'] and gSiteConfig['gPURCHASE_ORDER']}">
															<sec:authorize ifAnyGranted="ROLE_AEM,ROLE_ADMIN_${prop_site_id},ROLE_DROP_SHIP_CREATE">
																<a class="dropdown-item" href="../inventory/purchaseOrderForm.jhtm?orderId=${order.orderId}&dropship=true" ><fmt:message key="dropShip" /></a>
															</sec:authorize>
															<sec:authorize ifAnyGranted="ROLE_AEM,ROLE_ADMIN_${prop_site_id},ROLE_PURCHASE_ORDER_CREATE">
																<a class="dropdown-item" href="../inventory/purchaseOrderForm.jhtm?orderId=${order.orderId}" ><fmt:message key="purchaseOrder" /></a>
															</sec:authorize> 
														</c:if>
														<c:if test="${gSiteConfig['gINVENTORY'] and gSiteConfig['gPURCHASE_ORDER'] and false}">
															<a class="dropdown-item" href="../inventory/ajaxShowOrderLineItems.jhtm?orderId=${order.orderId}">Create PO</a>
														</c:if>
														
														<c:if test="${!order.fulfilled and (!order.orderCancel) and (siteConfig['GENERATE_PACKING_LIST'].value == 'true' or (gSiteConfig['gINVENTORY'] and gSiteConfig['gPURCHASE_ORDER']))}">
															<sec:authorize ifAnyGranted="ROLE_AEM,ROLE_ADMIN_${prop_site_id},ROLE_INVOICE_PACKING_LIST,ROLE_DROP_SHIP_CREATE,ROLE_PURCHASE_ORDER_CREATE">   
																<c:if test="${gSiteConfig['gINVENTORY'] and gSiteConfig['gPURCHASE_ORDER']}">
																	<sec:authorize ifAnyGranted="ROLE_AEM,ROLE_ADMIN_${prop_site_id},ROLE_DROP_SHIP_CREATE">
																		<c:choose>
																			<c:when test="${gSiteConfig['gMOBILE_LAYOUT'] and _mobile=='_mobile'}">
																				<a class="dropdown-item" href="../inventory/mobileShowOrderLineItems.jhtm?orderId=${order.orderId}&dropShip=true">Create Purchase Order(s)</a>
																			</c:when>
																			<c:otherwise>
																				<a class="dropdown-item" href="../inventory/ajaxShowOrderLineItems.jhtm?orderId=${order.orderId}&dropShip=true" id="createPurchaseOrderButton">Create Purchase Order(s)</a> 
																			</c:otherwise>
																		</c:choose>
																	</sec:authorize>
																</c:if> 
															</sec:authorize>
														</c:if>
													</div>
												</div>
											</c:if>
										</sec:authorize>
									</div>
								</div>
							</div>
						</form>
					</div>
					<div class="page-body">
						<div class="minor-info">
							<c:if test="${order.lastModified != null}">
								<span><span><fmt:message key="lastModified" />:</span> <span><fmt:formatDate type="both" timeZone="${siteConfig['SITE_TIME_ZONE'].value}" pattern="${siteConfig['SITE_DATE_FORMAT'].value} hh:mm a" value="${order.lastModified}"/></span></span>
							</c:if>
							<c:if test="${gSiteConfig['gSALES_REP']}">
								<span><span><fmt:message key="salesRep"/>:</span> <span><c:out value="${salesRep.name}"/></span></span>
								<span><span><fmt:message key="processedBy"/>:</span> <span><c:out value="${salesRepProcessedBy.name}"/></span></span>
								<span><span><fmt:message key="orderedBy"/>:</span> <span><c:out value="${orderedBy}"/></span></span>
							</c:if>
						</div>
						<div class="main-content">
							<div class="form-section">
								<div class="content">
						            <ul class="nav nav-tabs" role="tablist">
						                <li class="nav-item">
						                    <a class="nav-link active" data-toggle="tab" role="tab" href="#tab_1">Order</a>
						                </li>
						                <c:if test="${gSiteConfig['gPAYMENTS']}">
											<a class="nav-link" data-toggle="tab" role="tab" href="#tab_2">
						                    		Open <fmt:message key="orders"/>
						                    </a>
						                </c:if>
						                <li class="nav-item">
						                		<div class="dropdown">
							                    <a class="nav-link" data-toggle="dropdown">Advanced <i class="fa fa-caret-down"></i></a>
												<div class="dropdown-menu dropdown-menu-right">
									                <c:if test="${(gSiteConfig['gBUDGET'] || gSiteConfig['gBUDGET_FREQUENCY'] > 0) and order.actionHistory != null and !empty order.actionHistory and fn:length(order.actionHistory) gt 0}">
									                    <a class="dropdown-item" data-toggle="tab" role="tab" href="#tab_3">
									                    		Approval History
									                    </a>
								                    </c:if>
								                    <sec:authorize ifAnyGranted="ROLE_AEM,ROLE_ADMIN_${prop_site_id},ROLE_PURCHASE_ORDER_UPDATE,ROLE_PURCHASE_ORDER_CREATE">     
														<c:if test="${poList != null and fn:length(poList) > 0}">
										                    <a class="dropdown-item" data-toggle="tab" role="tab" href="#tab_4">
										                    		Purchase Orders
										                    </a>
														</c:if>
													</sec:authorize>   
												</div>
											</div>
										</li>
									</ul>
									<div class="tab-content">
										<c:import url="/WEB-INF/jsp/admin-responsive/orders/invoice/main_tab.jsp" />
										<c:if test="${gSiteConfig['gPAYMENTS']}">
											<c:import url="/WEB-INF/jsp/admin-responsive/orders/invoice/open_order_tab.jsp" />
						                </c:if>
						                <c:if test="${(gSiteConfig['gBUDGET'] || gSiteConfig['gBUDGET_FREQUENCY'] > 0) and order.actionHistory != null and !empty order.actionHistory and fn:length(order.actionHistory) gt 0}">
											<c:import url="/WEB-INF/jsp/admin-responsive/orders/invoice/action_history_tab.jsp" />
										</c:if>
						                <sec:authorize ifAnyGranted="ROLE_AEM,ROLE_ADMIN_${prop_site_id},ROLE_PURCHASE_ORDER_UPDATE,ROLE_PURCHASE_ORDER_CREATE">     
											<c:if test="${poList != null and fn:length(poList) > 0}">
												<c:import url="/WEB-INF/jsp/admin-responsive/orders/invoice/purchase_order_tab.jsp" />
											</c:if>
										</sec:authorize>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
	    		</c:if>
		</sec:authorize>
	</tiles:putAttribute>
	<tiles:putAttribute name="js">
	 	<script>
		 	function subStatusOnchanged(el){
		 		var option = el.options[el.selectedIndex];
		 		var cc = option.getAttribute('cc');
		 		$('#cc').val(cc);
		 		
		 		var groupId = 0;//no Group
		 		var defaultSiteMessageId = option.getAttribute('default-site-message-id');
		 		if(defaultSiteMessageId){
		 			//Group All Messages
		 			groupId = -1;
		 		}
		 		$('#groupSelected').val(groupId);
	 			getSiteMessage(groupId,defaultSiteMessageId);
	 		}
		 	
		 	function subStatusGroupOnchanged(el){
	 			var groupId = null;
				var model = {};
				if(el.value!= 'All')
					model['groupId'] = $('#subStatusGroupId').val();

				$.post('show-ajax-subStatus.jhtm', model, function(data){
		      		$('#subStatusList').html(data);
				});
	 		}
			
		 	function chooseMessage(el){
		 		var messageList = [];
		 		$('[name=message]').each(function(index){
		 			 $(this).hide();
		 		});
		 		$('[name=subject]').each(function(index){
		 			 $(this).hide();
		 		});
		 		$('#subject'+el.value).show();
		 		$('#message'+el.value).show();
		 		
				if ($('#htmlID'+el.value).val() == 'true') {
					$('#htmlCheckbox').prop('checked', true);
				}
				else {
					$('#htmlCheckbox').prop('checked', false);
				}
		 	}
	 	
			function getSiteMessage(groupId,defaultSiteMessageId) {
				$.post('../config/show-ajax-groupSiteMessage.jhtm?groupId='+groupId,function(data){
					$('#siteMessageSelect').html(data);
					$('#messageSelected').val(defaultSiteMessageId).trigger('change');
				});
	 		}

		 	function notifyCustomer(){
				$('#notifyCustomer').toggle();
				$('#messageSelect').show();
			}
	 	</script>
		
		<!-- DGRID TABLE SCRIPT MUST BE AT BOTTOM OF THE PAGE -->
		<script src="${_contextpath}/admin-responsive-static/assets/plugins/dojo/dojo.js" data-dojo-config="async: true"></script>
		<script>
			function initializeOrderGrid(){
				//Table Data Sanitization
				var data = [];
				<c:forEach var="lineItem" items="${order.lineItems}" varStatus="status">
					data.push({
						offset: "${status.count}.",
						<c:if test="${lineItem.product.id != null}">
				        		productId: "${lineItem.product.id}",
				        </c:if>
				        sku: "${lineItem.product.sku}",
				        name: "${wj:escapeJS(lineItem.product.name)}",
				        <c:forEach items="${productFieldsHeader}" var="pFHeader" varStatus="status">
				        		<c:set var="check" value="0"/>
					  		<c:if test="${pFHeader.showOnInvoice or pFHeader.showOnInvoiceBackend}">
					  			<c:forEach items="${lineItem.productFields}" var="productField"> 
							   		<c:if test="${pFHeader.id == productField.id}">
							   			"${pFHeader.name}" : "${wj:escapeJS(productField.value)}",
								    		<c:set var="check" value="1"/>
								   </c:if>
							  	</c:forEach>
					  			<c:if test="${check == 0}">
									"${pFHeader.name}" : "",
							   	</c:if>
					  		</c:if>
					  	</c:forEach>
					  	<c:choose>
					    		<c:when test="${lineItem.priceCasePackQty != null}">
					    			quantity: '(<c:out value="${lineItem.priceCasePackQty}"/> per <c:out value="${siteConfig[\'PRICE_CASE_PACK_UNIT_TITLE\'].value}" /> )',
					    		</c:when>
						    	<c:otherwise>
						    		quantity: "${lineItem.quantity}",
						    	</c:otherwise>
						</c:choose>
						<c:if test="${siteConfig['GENERATE_PACKING_LIST'].value == 'true'}"> 
							processed: <c:out value="${lineItem.processed}"/>,
				  		</c:if>
						<c:if test="${not (order.hasRewardPoints && lineItem.product.productType == 'REW')}">
		    					price: decodeHTML('<fmt:message key="${siteConfig[\'CURRENCY\'].value}" />')+'<fmt:formatNumber value="${lineItem.unitPrice}" pattern="#,##0.00000#" maxFractionDigits="${siteConfig[\'MAX_FRACTION_DIGIT\'].value}" />', 
		              	</c:if>
				  		<c:if test="${gSiteConfig['gPURCHASE_ORDER'] && siteConfig['SHOW_COST'].value == 'true' && order.hasCost}">
							cost: decodeHTML('<fmt:message key="${siteConfig[\'CURRENCY\'].value}" />') + '<fmt:formatNumber value="${lineItem.unitCost}" pattern="#,##0.00"/>', 
						</c:if>
						<c:if test="${not (order.hasRewardPoints and lineItem.product.productType == 'REW')}">
							total: decodeHTML('<fmt:message key="${siteConfig[\'CURRENCY\'].value}" />') + '<fmt:formatNumber value="${lineItem.totalPrice}" pattern="#,##0.00000#" maxFractionDigits="${siteConfig[\'MAX_FRACTION_DIGIT\'].value}"/>',
					    </c:if>
					});
				</c:forEach>
				
				require(["dgrid/List", "dgrid/OnDemandGrid", "dgrid/Selection", "dgrid/Keyboard", "dgrid/Editor", "dijit/form/CheckBox", 
						"dijit/form/TimeTextBox", "dijit/form/Select", "dojo/data/ObjectStore", "dijit/form/FilteringSelect",
						'dijit/form/DropDownButton', 'dijit/DropDownMenu', 'dijit/MenuItem',
						"dojo/_base/declare", "dojo/_base/lang", "dojo/store/Memory", "dojo/store/Observable", "dgrid/test/data/createSyncStore", 
						'dgrid/extensions/ColumnHider', 'dgrid/extensions/ColumnReorder', 'dgrid/extensions/ColumnResizer',
					], function(List, OnDemandGrid, Selection, Keyboard, Editor, CheckBox, TimeTextBox, Select, ObjectStore, FilteringSelect,
							DropDownButton, DropDownMenu, MenuItem,
							declare, lang, LegacyMemory, LegacyObservable, createSyncStore, ColumnHider, ColumnReorder, ColumnResizer){

					//Column Definitions
					var columns = {
						offset: {
							label: 'Line#',
							get: function(object){
								return object.offset;
							},
							sortable: false,
						},
						sku: {
							label: '<fmt:message key="productSku" />',
							renderCell: function(object){
								if(object.productId){
									var link = document.createElement('a');
									link.text = object.sku;
									link.href = "./catalog/product.jhtm?id="+object.productId;
									return link;
								}
								var span = document.createElement('span');
								span.textContent = object.sku;
								return div;
							},
							sortable: false,
						},
						name: {
							label: '<fmt:message key="productName" />',
							get: function(object){
								return object.name;
							},
							sortable: false,
						},
						<c:forEach items="${productFieldsHeader}" var="productField">
				      		<c:if test="${productField.showOnInvoice or productField.showOnInvoiceBackend}">
				      			"${productField.name}": {
									label: '${productField.name}',
									get: function(object){
										return object['${productField.name}'];
									},
									sortable: false,
								},
					      	</c:if>
					    </c:forEach>
					    quantity: {
							label: '<fmt:message key="quantity" />',
							get: function(object){
								return object.quantity;
							},
							sortable: false,
						},
						<c:if test="${siteConfig['GENERATE_PACKING_LIST'].value == 'true'}">
							processed: {
								label: '<fmt:message key="packingList" />',
								get: function(object){
									return object.processed;
								},
								sortable: false,
							},
		                </c:if>
	                		price: {
							label: '<fmt:message key="productPrice" /><c:if test="${order.hasContent}"> / <c:out value="${siteConfig[\'CASE_UNIT_TITLE\'].value}" /></c:if>',
							get: function(object){
								return object.price;
							},
							sortable: false,
						},
						<c:if test="${gSiteConfig['gPURCHASE_ORDER'] && siteConfig['SHOW_COST'].value == 'true' && order.hasCost}">
							cost: {
								label: '<fmt:message key="cost" />',
								get: function(object){
									return object.cost;
								},
								sortable: false,
							},
					    </c:if>
						total: {
							label: '<fmt:message key="total" />',
							get: function(object){
								return object.total;
							},
							sortable: false,
						},
					};
					
					var grid = new (declare([OnDemandGrid, Selection, Keyboard, Editor, ColumnReorder, ColumnResizer]))({
						collection: createSyncStore({ data: data }),
						columns: columns,
						selectionMode: "none",
					},'grid');

					grid.on('dgrid-refresh-complete', function(event) {
						$('#grid-hider-menu').appendTo('#column-hider');
					});
					
					grid.startup();
				});
			}
			
			function initializeOrderStatusGrid(){
   				//Table Data Sanitization
   				var data = [];
   				<c:forEach var="orderStatus" items="${order.statusHistory}">
   					<c:if test="${orderStatus.status != 'xap' or order.status == 'xap'}">
	   					data.push({
	   						dateChanged: '<fmt:formatDate type="both" timeZone="${siteConfig[\'SITE_TIME_ZONE\'].value}" value="${orderStatus.dateChanged}"/>',
	   						status: '<fmt:message key="orderStatus_${orderStatus.status}"/>',
	   						<c:if test="${orderStatus.userNotified}">
								userNotified: true,
							</c:if>
	   						<c:choose>
								<c:when test="${fn:containsIgnoreCase(order.shippingMethod,'ups') or fn:containsIgnoreCase(order.shippingCarrier,'ups')}">
									trackingURL: "http://wwwapps.ups.com/WebTracking/processInputRequest?HTMLVersion=5.0&sort_by=status&tracknums_displayed=10&TypeOfInquiryNumber=T&loc=en_US&InquiryNumber1=<c:out value='${orderStatus.trackNum}' />&AgreeToTermsAndConditions=yes",
									trackingNumber: "${orderStatus.trackNum}",								
								</c:when>
								<c:when test="${fn:containsIgnoreCase(order.shippingMethod,'fedex') or fn:containsIgnoreCase(order.shippingCarrier,'fedex')}">
									trackingURL: "http://fedex.com/Tracking?action=track&language=english&cntry_code=us&mps=y&tracknumbers=<c:out value='${orderStatus.trackNum}' />&ascend_header=1",
									trackingNumber: "${orderStatus.trackNum}",
								</c:when>
								<c:when test="${fn:containsIgnoreCase(order.shippingMethod,'usps') or fn:containsIgnoreCase(order.shippingCarrier,'usps')}">
									trackingURL: "http://trkcnfrm1.smi.usps.com/PTSInternetWeb/InterLabelInquiry.do?strOrigTrackNum=<c:out value='${orderStatus.trackNum}' />",
									trackingNumber: "${orderStatus.trackNum}",
								</c:when>
								<c:when test="${fn:containsIgnoreCase(order.shippingMethod,'averitt') or fn:containsIgnoreCase(order.shippingCarrier,'averitt')}">
									trackingURL: "https://www.averittexpress.com/servlet/rsoLTLtrack?Type=PN&Number=<c:out value='${orderStatus.trackNum}' />",
									trackingNumber: "${orderStatus.trackNum}",
								</c:when>
								<c:otherwise>
									trackingNumber: '<c:out value="${orderStatus.trackNum}" />',
								</c:otherwise>
							</c:choose>
							comments: '${wj:escapeJS(orderStatus.comments)}',
							username: '${wj:escapeJS(orderStatus.username)}',
	   					});
   					</c:if>
   				</c:forEach>
   				
   				require(["dgrid/List", "dgrid/OnDemandGrid", "dgrid/Selection", "dgrid/Keyboard", "dgrid/Editor", "dijit/form/CheckBox", 
   						"dijit/form/TimeTextBox", "dijit/form/Select", "dojo/data/ObjectStore", "dijit/form/FilteringSelect",
   						'dijit/form/DropDownButton', 'dijit/DropDownMenu', 'dijit/MenuItem',
   						"dojo/_base/declare", "dojo/_base/lang", "dojo/store/Memory", "dojo/store/Observable", "dgrid/test/data/createSyncStore", 
   						'dgrid/extensions/ColumnHider', 'dgrid/extensions/ColumnReorder', 'dgrid/extensions/ColumnResizer',
   					], function(List, OnDemandGrid, Selection, Keyboard, Editor, CheckBox, TimeTextBox, Select, ObjectStore, FilteringSelect,
   							DropDownButton, DropDownMenu, MenuItem,
   							declare, lang, LegacyMemory, LegacyObservable, createSyncStore, ColumnHider, ColumnReorder, ColumnResizer){

   					//Column Definitions
   					var columns = {
   						dateChanged: {
   							label: 'Date Changed',
   							get: function(object){
   								return object.dateChanged;
   							},
   							sortable: false,
   						},
   						status: {
   							label: '<fmt:message key="status"/>',
   							get: function(object){
   								return object.status;
   							},
   							sortable: false,
   						},
   						customerNotified: {
							label: 'Customer Notified',
							renderCell: function(object){
								var div = document.createElement("div");
								if(object.userNotified){
									var i = document.createElement("i");
									i.className = 'sli-check';
									div.appendChild(i);
								}
								return div;
							},
							sortable: false,
						},
						trackingNumber: {
							label: '<fmt:message key="trackNum"/>',
							renderCell: function(object){
								if(object.trackingURL){
									var link = document.createElement("a");
									link.text = object.trackingNumber;
									link.href = object.trackingURL;
									link.setAttribute('target','_blank')
									return link;
								}
								var span = document.createElement("span");
								span.textContent = object.trackingNumber;
								return span;
							},
							sortable: false,
						},
						comments: {
							label: '<fmt:message key="comments"/>',
							get: function(object){
								return object.comments;
							},
							sortable: false,
						},
						username: {
							label: '<fmt:message key="user"/>',
							get: function(object){
								return object.username;
							},
							sortable: false,
						}
   					};
   					
   					var grid = new (declare([OnDemandGrid, Selection, Keyboard, Editor, ColumnReorder, ColumnResizer]))({
   						collection: createSyncStore({ data: data }),
   						columns: columns,
   						selectionMode: "none",
   					},'order-status-grid');

   					grid.on('dgrid-refresh-complete', function(event) {
   						$('#grid-hider-menu').appendTo('#column-hider');
   					});
   					
   					grid.startup();
   				});
   			}
			
			function initializeOpenOrderGrid(){
				<c:set var="totalGrandTotal" value="0.0" />
				<c:set var="totalAmtPaid" value="0.0" />
  				<c:set var="totalAmtDue" value="0.0" />
				//Table Data Sanitization
				var data = [];
   				<c:forEach items="${openInvoices}" var="openInvoice" varStatus="status">
					data.push({
   						offset: '${status.index + 1}',
   						orderId: '${openInvoice.orderId}',
   						dateOrdered: '<fmt:formatDate type="date" timeZone="${siteConfig[\'SITE_TIME_ZONE\'].value}" value="${openInvoice.dateOrdered}"/>',
   						<c:if test="${openInvoice.status != null}">
							status: '<fmt:message key="orderStatus_${openInvoice.status}" />',
						</c:if>
						grandTotal: decodeHTML('<fmt:message key="${siteConfig[\'CURRENCY\'].value}" />') + '<fmt:formatNumber value="${openInvoice.grandTotal}" pattern="#,##0.00" />',
						<c:if test="${openInvoice.grandTotal != null}">
							<c:set var="totalGrandTotal" value="${openInvoice.grandTotal + totalGrandTotal}" />
						</c:if>
						paid: decodeHTML('<fmt:message key="${siteConfig[\'CURRENCY\'].value}" />') + '<fmt:formatNumber value="${openInvoice.amountPaid}" pattern="#,##0.00" />',
						<c:if test="${openInvoice.amountPaid != null}">
							<c:set var="totalAmtPaid" value="${openInvoice.amountPaid + totalAmtPaid}" />
						</c:if>
						due: decodeHTML('<fmt:message key="${siteConfig[\'CURRENCY\'].value}" />') + '<fmt:formatNumber value="${openInvoice.balance}" pattern="#,##0.00" />',
						<c:if test="${openInvoice.balance != null}">
							<c:set var="totalAmtDue" value="${openInvoice.balance + totalAmtDue}" />
						</c:if>
   					});
   				</c:forEach>
   				
   				require(["dgrid/List", "dgrid/OnDemandGrid", "dgrid/Selection", "dgrid/Keyboard", "dgrid/Editor", "dijit/form/CheckBox", 
   						"dijit/form/TimeTextBox", "dijit/form/Select", "dojo/data/ObjectStore", "dijit/form/FilteringSelect",
   						'dijit/form/DropDownButton', 'dijit/DropDownMenu', 'dijit/MenuItem',
   						"dojo/_base/declare", "dojo/_base/lang", "dojo/store/Memory", "dojo/store/Observable", "dgrid/test/data/createSyncStore", 
   						'dgrid/extensions/ColumnHider', 'dgrid/extensions/ColumnReorder', 'dgrid/extensions/ColumnResizer', 'summary/SummaryRow'
   					], function(List, OnDemandGrid, Selection, Keyboard, Editor, CheckBox, TimeTextBox, Select, ObjectStore, FilteringSelect,
   							DropDownButton, DropDownMenu, MenuItem,
   							declare, lang, LegacyMemory, LegacyObservable, createSyncStore, ColumnHider, ColumnReorder, ColumnResizer, SummaryRow){

   					//Column Definitions
   					var columns = {
   						offset: {
   							label: '#',
   							get: function(object){
   								return object.offset;
   							},
   							sortable: false,
   						},
   						orderNumber: {
   							label: '<fmt:message key="orderNumber" />',
   							renderCell: function(object){
   								var link = document.createElement('a');
   								link.text = object.orderId;
   								link.href = '../orders/invoice.jhtm?order=' + object.orderId;
   								return link;
   							},
   							sortable: false,
   						},
   						dateOrdered: {
   							label: '<fmt:message key="orderDate" />',
   							get: function(object){
   								return object.dateOrdered;
   							},
   							sortable: false,
   						},
   						status: {
   							label: '<fmt:message key="status" />',
   							get: function(object){
   								return object.status;
   							},
   							sortable: false,
   						},
   						grandTotal: {
   							label: '<fmt:message key="grandTotal" />',
   							get: function(object){
   								return object.grandTotal;
   							},
   							sortable: false,
   						},
   						paid: {
   							label: '<fmt:message key="paid" />',
   							get: function(object){
   								return object.paid;
   							},
   							sortable: false,
   						},
   						due: {
   							label: '<fmt:message key="due" />',
   							get: function(object){
   								return object.due;
   							},
   							sortable: false,
   						},
   					};
   					
   					var grid = new (declare([OnDemandGrid, Selection, Keyboard, Editor, ColumnReorder, ColumnResizer, SummaryRow]))({
   						collection: createSyncStore({ data: data }),
   						columns: columns,
   						selectionMode: "none",
   					},'open-order-grid');
   					
   					var totals = [{
							status: '<fmt:message key="total" />',
							grandTotal: decodeHTML('<fmt:message key="${siteConfig[\'CURRENCY\'].value}" />') + '<fmt:formatNumber value="${totalGrandTotal}" pattern="#,##0.00" />',
							paid: decodeHTML('<fmt:message key="${siteConfig[\'CURRENCY\'].value}" />') + '<fmt:formatNumber value="${totalAmtPaid}" pattern="#,##0.00" />',
							due: decodeHTML('<fmt:message key="${siteConfig[\'CURRENCY\'].value}" />') + '<fmt:formatNumber value="${totalAmtDue}" pattern="#,##0.00" />',
						},{
							status: '<fmt:message key="payment" /> <fmt:message key="alert" /> - <fmt:message key="due" />',
							grandTotal: decodeHTML('<fmt:message key="${siteConfig[\'CURRENCY\'].value}" />') + '<fmt:formatNumber value="${customer.paymentAlert - totalAmtDue}" pattern="#,##0.00" />',
						}];
   						
					grid.set('summary', totals);

					grid.on('dgrid-refresh-complete', function(event) {
						$('#grid-hider-menu').appendTo('#column-hider');
					});
					
					grid.startup();
   				});
			}
			
			function initializePurchaseOrderGrid(){
				//Table Data Sanitization
				var data = [];
				<c:forEach items="${poList}" var="po" varStatus="status">
					data.push({
   						offset: '${status.index + 1}',
   						poId: '${po.poId}',
   						poNumber: '${po.poNumber}',
   						status: '<fmt:message key="${po.statusName}" />',
   						created: '<fmt:formatDate type="both" timeZone="${siteConfig[\'SITE_TIME_ZONE\'].value}" pattern="MM/dd/yyyy" value="${po.created}"/>',
   						total: decodeHTML('<fmt:message key="${siteConfig[\'CURRENCY\'].value}" />') + '<fmt:formatNumber value="${po.subTotal}" pattern="#,##0.00"/>',
   						shipVia: '${wj:escapeJS(po.shipVia)}',
   						shippingQuote: decodeHTML('<fmt:message key="${siteConfig[\'CURRENCY\'].value}" />') + '<fmt:formatNumber value="${po.shippingQuote}" pattern="#,##0.00"/>',
   						orderBy: '${wj:escapeJS(po.orderBy)}',
   						company: '${wj:escapeJS(po.company)}',
   						supplierInvoiceNumber: '${wj:escapeJS(po.supplierInvoiceNumber)}',
   						noteTrimmed: "${wj:escapeJS(po.noteTrimmed)}",
   					});
   				</c:forEach>
   				
   				require(["dgrid/List", "dgrid/OnDemandGrid", "dgrid/Selection", "dgrid/Keyboard", "dgrid/Editor", "dijit/form/CheckBox", 
   						"dijit/form/TimeTextBox", "dijit/form/Select", "dojo/data/ObjectStore", "dijit/form/FilteringSelect",
   						'dijit/form/DropDownButton', 'dijit/DropDownMenu', 'dijit/MenuItem',
   						"dojo/_base/declare", "dojo/_base/lang", "dojo/store/Memory", "dojo/store/Observable", "dgrid/test/data/createSyncStore", 
   						'dgrid/extensions/ColumnHider', 'dgrid/extensions/ColumnReorder', 'dgrid/extensions/ColumnResizer', 'summary/SummaryRow'
   					], function(List, OnDemandGrid, Selection, Keyboard, Editor, CheckBox, TimeTextBox, Select, ObjectStore, FilteringSelect,
   							DropDownButton, DropDownMenu, MenuItem,
   							declare, lang, LegacyMemory, LegacyObservable, createSyncStore, ColumnHider, ColumnReorder, ColumnResizer, SummaryRow){

   					//Column Definitions
   					var columns = {
   						offset: {
   							label: '#',
   							get: function(object){
   								return object.offset;
   							},
   							sortable: false,
   						},
   						poNumber: {
   							label: '<fmt:message key="poNumber" />',
   							renderCell: function(object){
   								var link = document.createElement('a');
   								link.text = object.poNumber;
   								link.href = '../inventory/purchaseOrderForm2.jhtm?poId=' + object.poId;
   								return link;
   							},
   							sortable: false,
   						},
   						status: {
   							label: '<fmt:message key="status" />',
   							get: function(object){
   								return object.status;
   							},
   							sortable: false,
   						},
   						created: {
   							label: '<fmt:message key="created" />',
   							get: function(object){
   								return object.created;
   							},
   							sortable: false,
   						},
   						total: {
   							label: '<fmt:message key="total" />',
   							get: function(object){
   								return object.total;
   							},
   							sortable: false,
   						},
   						shipVia: {
   							label: '<fmt:message key="shipVia" />',
   							get: function(object){
   								return object.shipVia;
   							},
   							sortable: false,
   						},
   						shippingQuote: {
   							label: '<fmt:message key="shippingQuote" />',
   							get: function(object){
   								return object.shippingQuote;
   							},
   							sortable: false,
   						},
   						orderBy: {
   							label: '<fmt:message key="orderBy" />',
   							get: function(object){
   								return object.orderBy;
   							},
   							sortable: false,
   						},
   						supplier: {
   							label: '<fmt:message key="supplier" />',
   							get: function(object){
   								return object.company;
   							},
   							sortable: false,
   						},
   						supplierInvoiceNumber: {
   							label: '<fmt:message key="supplierInvoiceNumber" />',
   							get: function(object){
   								return object.supplierInvoiceNumber;
   							},
   							sortable: false,
   						},
   						note: {
   							label: '<fmt:message key="note" />',
   							get: function(object){
   								return object.noteTrimmed;
   							},
   							sortable: false,
   						}
   					};
   					
   					var grid = new (declare([OnDemandGrid, Selection, Keyboard, Editor, ColumnReorder, ColumnResizer, SummaryRow]))({
   						collection: createSyncStore({ data: data }),
   						columns: columns,
   						selectionMode: "none",
   					},'purchase-order-grid');
   					
   					grid.on('dgrid-refresh-complete', function(event) {
   						$('#grid-hider-menu').appendTo('#column-hider');
   					});
   					
   					grid.startup();
   				});
			}
			
    			initializeOrderGrid();
    			initializeOrderStatusGrid();
    			initializeOpenOrderGrid();
    			initializePurchaseOrderGrid();
		</script>
	</tiles:putAttribute>  
</tiles:insertDefinition>