<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ taglib uri="http://www.springframework.org/security/tags" prefix="sec" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ taglib uri="/WEB-INF/tlds/spring.tld" prefix="spring" %>
<%@ taglib uri="/WEB-INF/tlds/custom.tld" prefix="wj" %>

<div class="row mb-3">
	<div class="col-md-6 col-lg-4">
		<div class="max-w-300">
			<c:if test="${!fn:contains(siteConfig['SITE_URL'].value,'framestoredirect')}">
				<div class="discount-block m-b-20">
					<div class="row m-b-5">
						<div class="col-12">
							<label class="col-form-label">
								Promo Discount
							</label>
						</div>
					</div>
					<div class="form-row m-b-5">
						<label class="col-12">
							<fmt:message key="code" />:
						</label>
						<div class="col-7">
							<form:input path="order.promo.title" class="form-control" disabled="${!gSiteConfig['gSALES_PROMOTIONS']}" id="promoTitle"/>
						</div>
						<div class="col-5">
							<select name="promoSelected" id="promoSelected" class="custom-select" <c:if test="${!gSiteConfig['gSALES_PROMOTIONS']}" >disabled="disabled"</c:if>>
								<option value="">Choose existing Promo Code</option> 
								<c:forEach items="${promoList}" var="promo"	varStatus="status">
									<option value="${promo.title}"><c:out value="${promo.title}" /></option>
								</c:forEach>
							</select> 
							<c:forEach items="${promoList}" var="promo" varStatus="status">
								<input type="hidden" id="discount_${promo.title}" value="${promo.discount}" />
								<input type="hidden" id="discount_type_${promo.title}" value="${promo.discountType}" />
								<input type="hidden" id="percent_${promo.title}" value="${promo.percent}" />
							</c:forEach>
						    <input type="hidden" id="discount_" value="" />
						    <input type="hidden" id="discount_type_" value="" />
						  	<input type="hidden" id="percent_" value="" />
						</div>
					</div>
					<div class="form-row m-b-5">
						<label class="col-12">
							<fmt:message key="discount" />:
						</label>
						<div class="col-7">
							<form:input path="order.promo.discount" class="form-control" disabled="${!gSiteConfig['gSALES_PROMOTIONS']}" id="promoDiscount"/>
						</div>
						<div class="col-5">
							<form:select path="order.promo.percent" disabled="${!gSiteConfig['gSALES_PROMOTIONS']}" id="promoPercent" class="custom-select">
								<form:option value="false"><fmt:message key="FIXED" />$</form:option>
								<form:option value="true">%</form:option>
							</form:select>
						</div>
					</div>
					<c:if test="${gSiteConfig['gSALES_PROMOTIONS_ADVANCED']}">
						<div class="form-row m-b-5">
							<label class="col-12">
								<fmt:message key="discount" /> <fmt:message key="type" />:
							</label>
							<div class="col-7">
								<form:select path="order.promo.discountType" disabled="${!gSiteConfig['gSALES_PROMOTIONS']}" id="promoDiscountType" class="custom-select">
									<form:option value="order"><fmt:message key="order" /></form:option>
									<form:option value="shipping"><fmt:message key="shipping" /></form:option>
								</form:select>
							</div>
						</div>
					</c:if>
				</div>
			</c:if>
			<c:if test="${orderName != 'quotes'}">
				<div class="form-row m-b-10">
					<div class="col-12">
						<label class="col-form-label">
							<fmt:message key="paymentMethod" />
						</label>
						<div class="form-group">
							<c:choose>
								<c:when test="${invoiceForm.order.workOrderNum != null}">
									<c:out value="${invoiceForm.order.paymentMethod}" />   
								</c:when>
								<c:otherwise>
									<form:select path="order.paymentMethod" class="custom-select">
										<form:option value="" label="Please Select"/>
										<form:option value="Credit Card" label="Credit Card"/>
										<c:forEach items="${customPayments}" var="paymentMethod">
											<c:if test="${paymentMethod.enabled}">					
												<option value="${paymentMethod.title}" <c:if test="${(customer.payment == paymentMethod.title) or (invoiceForm.order.paymentMethod == paymentMethod.title)}">selected="selected"</c:if>><c:out value="${paymentMethod.title}" /></option>
											</c:if>
										</c:forEach>			    
										<c:if test="${(gSiteConfig['gVIRTUAL_BANK_ACCOUNT'] or customer.credit > 0) and invoiceForm.order.grandTotal > 0 and customer.credit >= invoiceForm.order.grandTotal}">
											<form:option value="vba"><fmt:message key="vba"/></form:option>
										</c:if>
									</form:select>
								</c:otherwise>
							</c:choose>
						</div>
					</div>
				</div>
				<div class="form-row m-b-10">
					<div class="col-12">
						<label class="col-form-label">
							<c:out value="${siteConfig['SPECIAL_INSTRUCTIONS'].value}"/>
						</label>
						<div class="form-group">
							<spring:bind path="invoiceForm.order.invoiceNote">
								<textarea name="<c:out value="${status.expression}"/>" rows="8" cols="100" class="form-control"><c:out value="${status.value}"/></textarea>
							</spring:bind>
						</div>
					</div>
				</div>
			</c:if>
		</div>
	</div>
	<div class="col-md-6 col-lg-5">
		<div class="max-w-400">
			<c:if test="${invoiceForm.order.workOrderNum == null or canEdit}" >
				<c:forEach begin="1" end="20" step="1" var="skuIndex">
					<div class="form-row m-b-10">
						<div class="col-8">
							<label class="col-form-label">
								<fmt:message key="add" /> <fmt:message key="productSku" /> <fmt:formatNumber value="${skuIndex}" minIntegerDigits="2" pattern="##" />:
							</label>
							<div class="form-group">
								<input type="text" class="form-control" id="addSku${skuIndex}" name="__addProduct${skuIndex}_sku" value=""/>
							</div>
						</div>
						<div class="col-4">
							<label class="col-12 p-0">
								Qty: <fmt:formatNumber value="${skuIndex}" minIntegerDigits="2" pattern="##" />
							</label>
							<div class="form-group">
								<input type="text" class="form-control" id="addQty${skuIndex}" name="__addProduct${skuIndex}_qty" value=""/>
							</div>
						</div>
					</div>
				</c:forEach>
			</c:if>
		</div>
	</div>
</div>