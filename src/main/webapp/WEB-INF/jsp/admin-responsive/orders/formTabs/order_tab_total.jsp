<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ taglib uri="http://www.springframework.org/security/tags" prefix="sec" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ taglib uri="/WEB-INF/tlds/spring.tld" prefix="spring" %>
<%@ taglib uri="/WEB-INF/tlds/custom.tld" prefix="wj" %>

<div class="row">
	<div class="col-12">
		<table class="total">
			<tbody>
				<tr>
					<td>
						<fmt:message key="subTotal" />:
					</td>
					<td>
						<fmt:formatNumber value="${invoiceForm.order.subTotal}" pattern="${pattern}"/>
					</td>
				</tr>
				<c:if test="${false and siteConfig['GROSS_PROFIT_ON_ADMIN_INVOICE'].value == 'true' && totalCost != 0.0}">
					<tr>
						<td>
							Total GP:
						</td>
						<td>
							<fmt:formatNumber value="${((totalOriginalPrice - totalCost) / totalOriginalPrice ) * 100 }" pattern="${pattern}"/> %
						</td>
					</tr>
				</c:if>
				<c:if test="${invoiceForm.order.rushCharge != null}">
					<tr>
						<td><fmt:message key="rushCharge" />:</td>
						<td><fmt:formatNumber value="${invoiceForm.order.rushCharge}" pattern="${pattern}"/></td>
					</tr>
				</c:if>
				<c:if test="${invoiceForm.order.baggingCharge != null}">
					<tr>
						<td><fmt:message key="baggingCharge" />:</td>
						<td><fmt:formatNumber value="${invoiceForm.order.baggingCharge}" pattern="${pattern}"/></td>
					</tr>
				</c:if>
				<c:if test="${invoiceForm.order.budgetEarnedCredits > 0.00 }">
					<tr>
						<td><fmt:message key="f_earnedCredits" />:</td>
						<td><fmt:formatNumber value="${invoiceForm.order.budgetEarnedCredits}" pattern="${negpattern}"/></td>
					</tr>
				</c:if>
				<c:if test="${invoiceForm.order.promo.discountType eq 'order' }">
					<tr>
						<td>
							Discount For Promo Code <c:out value="${invoiceForm.order.promo.title}" />
							(<c:choose>
								<c:when test="${invoiceForm.order.promo.percent}">
									<fmt:formatNumber value="${invoiceForm.order.promo.discount}" pattern="${pattern}"/>%
								</c:when>
								<c:otherwise>
									$<fmt:formatNumber value="${invoiceForm.order.promo.discount}" pattern="${pattern}"/>
								</c:otherwise>
							</c:choose>):
						</td>
						<td>
							<c:choose>
								<c:when test="${invoiceForm.order.promo.percent}">
									(<fmt:formatNumber value="${invoiceForm.order.subTotal * ( invoiceForm.order.promo.discount / 100.00 )}" pattern="${pattern}"/>)
								</c:when>
								<c:when test="${(!invoiceForm.order.promo.percent) and (invoiceForm.order.promo.discount > invoiceForm.order.subTotal)}">
									(<fmt:formatNumber value="${invoiceForm.order.subTotal}" pattern="${pattern}"/>)
								</c:when>
								<c:otherwise>
									(<fmt:formatNumber value="${invoiceForm.order.promo.discount}" pattern="${pattern}"/>)
								</c:otherwise>
							</c:choose>
						</td>
					</tr>
				</c:if>
				<c:if test="${invoiceForm.order.lineItemPromos != null and fn:length(invoiceForm.order.lineItemPromos) gt 0}">
					<c:forEach items="${invoiceForm.order.lineItemPromos}" var="itemPromo" varStatus="status">
						<tr>
							<td>
								<c:choose>
									<c:when test="${itemPromo.value == 0}"><fmt:message key="promoCode" /></c:when>
									<c:otherwise><fmt:message key="discountForPromoCode" /></c:otherwise>
								</c:choose>
								<c:out value="${itemPromo.key}" />
							</td>
							<td>
								<c:choose>
									<c:when test="${itemPromo.value == 0}">&nbsp;</c:when>
									<c:otherwise>($<fmt:formatNumber value="${itemPromo.value}" pattern="${pattern}"/>)</c:otherwise>
								</c:choose>
							</td>
						</tr>
					</c:forEach>  
				</c:if>
				<c:if test="${gSiteConfig['gSITE_DOMAIN'] != 'yayme.com.au'}">
					<tr>
						<td>
							Tax:
							<spring:bind path="invoiceForm.order.taxRate">
								<input style="text-align: right;" type="text" name="${status.expression}" class="form-control" value="${status.value}" <c:if test="${gSiteConfig['gSPECIAL_TAX']}">readonly="readonly"</c:if>>
							</spring:bind>%
						</td>
						<td><fmt:formatNumber value="${invoiceForm.order.tax}" pattern="#,##0.00"/></td>
					</tr>
				</c:if>
				<c:if test="${not empty invoiceForm.order.totalHazMatFee}">
					<tr>
						<td> 
							Hazmat Fee:
						</td>
						<td>
							<spring:bind path="invoiceForm.order.totalHazMatFee">
								<input style="text-align: right" type="text" class="form-control" name="order.totalHazMatFee" value="${invoiceForm.order.totalHazMatFee}" id="totalHazMatFeeId">
							</spring:bind> 
						</td>
					</tr>
			    </c:if>
				<tr>
					<td>
						Shipping (
							<spring:bind path="invoiceForm.order.shippingMethod">
								<input type="text" name="${status.expression}" class="form-control" value="${status.value}" id="shippingTitle" style="width:150px;">
							</spring:bind>
						):
						<select name="customShippingRateSelected" id="customShippingRateSelected" style="width:250px;" class="custom-select">
							<option value="">Choose existing Custom Shipping</option>
							<c:forEach items="${customShippingRateList}" var="customShippingRate"	varStatus="status">
								<option value="${customShippingRate.title}"><c:out value="${customShippingRate.title}" /></option>
							</c:forEach>
						</select>
						<c:forEach items="${customShippingRateList}" var="customShippingRate"	varStatus="status">
							<input type="hidden" id="price_${customShippingRate.title}" value="${customShippingRate.price}" />
							<input type="hidden" id="handling_${customShippingRate.title}" value="${customShippingRate.handling}" />
						</c:forEach>
						<input type="hidden" id="price_" value="" />
					</td>
					<td>
						<spring:bind path="invoiceForm.order.shippingCost">
							<input style="text-align:right;" class="form-control" type="text" name="${status.expression}" value="${status.value}" id="shippingCost">
						</spring:bind> 
					</td>
			  	</tr>
				<tr>
					<td>
						Handling:
					</td>
					<td>
						<spring:bind path="invoiceForm.order.handlingCost">
							<input style="text-align: right" class="form-control" type="text" name="${status.expression}" value="${status.value}" id="handlingCost">
						</spring:bind> 
					</td>
				</tr>
				<c:if test="${invoiceForm.order.promo.discountType eq 'shipping'}">
					<tr>
						<td>
							Discount For Promo Code <c:out value="${invoiceForm.order.promo.title}" />
							(<c:choose>
								<c:when test="${invoiceForm.order.promo.percent}">
									<fmt:formatNumber value="${invoiceForm.order.promo.discount}" pattern="${pattern}"/>%
								</c:when>
								<c:otherwise>
									$<fmt:formatNumber value="${invoiceForm.order.promo.discount}" pattern="${pattern}"/>
								</c:otherwise>
							</c:choose>):
						</td>
						<td>
							<c:choose>
								<c:when test="${invoiceForm.order.promo.percent}">
									(<fmt:formatNumber value="${invoiceForm.order.shippingCost * ( invoiceForm.order.promo.discount / 100.00 )}" pattern="${pattern}"/>)
								</c:when>
								<c:when test="${(!invoiceForm.order.promo.percent) and (invoiceForm.order.promo.discount > invoiceForm.order.shippingCost)}">
									(<fmt:formatNumber value="${invoiceForm.order.shippingCost}" pattern="${pattern}"/>)
								</c:when>
								<c:otherwise>
									(<fmt:formatNumber value="${invoiceForm.order.promo.discount}" pattern="${pattern}"/>)
								</c:otherwise>
							</c:choose>    
						</td>
					</tr>
				</c:if>
				<c:if test="${invoiceForm.order.hasCustomShipping or invoiceForm.order.customShippingCost != null}">
					<tr>
						<td>
							Custom Shipping (<form:input path="order.customShippingTitle"/>)
						</td>
						<td><form:input path="order.customShippingCost" class="form-control" style="text-align: right"/></td>
					</tr>
				</c:if>
				<c:choose>
					<c:when test="${gSiteConfig['gBUDGET'] and invoiceForm.order.requestForCredit != null and invoiceForm.order.creditUsed == null and customer.creditAllowed != null and customer.creditAllowed > 0}">
						<tr>
							<td>
								<fmt:message key="creditRequestAmount">
									<fmt:param>${customer.creditAllowed}</fmt:param>
									<fmt:param><fmt:formatNumber value="${subTotalByPlan}" pattern="${pattern}"/></fmt:param>
									<fmt:param><fmt:formatNumber value="${(customer.creditAllowed / 100) * subTotalByPlan}" pattern="${pattern}"/></fmt:param>
								</fmt:message>
								<br/> 
								<fmt:message key="creditApprovalMessage" />
							</td>
							<c:choose>
								<c:when test="${invoiceForm.order.requestForCredit > 0 }">
									<td><fmt:formatNumber value="${invoiceForm.order.requestForCredit}" pattern="${pattern}"/></td>
								</c:when>
								<c:otherwise>
									<td>0.00</td>
								</c:otherwise>
							</c:choose>
						</tr>
					</c:when>
					<c:when test="${(gSiteConfig['gGIFTCARD'] or gSiteConfig['gVIRTUAL_BANK_ACCOUNT'] or gSiteConfig['gBUDGET']) and invoiceForm.order.creditUsed != null and invoiceForm.order.creditUsed > 0}">
						<tr>
							<td class="discount" colspan="${7+cols+cols2}" align="right"><fmt:message key="credit" />:</td>
							<td class="discount" align="right"><fmt:formatNumber value="${invoiceForm.order.creditUsed}" pattern="${negpattern}"/></td>
						</tr>
					</c:when>
				</c:choose>
				<tr>
					<td>
						CC Fee: <form:input path="order.ccFeeRate" class="form-control" style="text-align: right"/>
						<form:select path="order.ccFeePercent" class="custom-select" style="width:45px;">
							<form:option value="true">%</form:option>
							<form:option value="false">Fix</form:option>
						</form:select>
					</td>
					<td>
						<fmt:formatNumber value="${invoiceForm.order.ccFee}" pattern="${pattern}"/>
					</td>
				</tr>
				<c:if test="${invoiceForm.order.orderId == null and siteConfig['BUY_SAFE_URL'].value != ''}">
					<tr>
						<td>
							<fmt:message key="buySafeBondGuarntee" />:
							<select name="wantsBond" class="custom-select" style="width:60px;">
								<option value="true" <c:if test="${invoiceForm.order.wantsBond == 'true'}">selected="selected" </c:if>>Yes</option>
								<option value="false" <c:if test="${invoiceForm.order.wantsBond == 'false'}">selected="selected"</c:if>>No</option>
							</select>
						</td>
						<td><fmt:formatNumber value="${invoiceForm.order.bondCost}" pattern="${pattern}"/></td>
					</tr>
				</c:if>
				<c:if test="${invoiceForm.order.bondCost != null and invoiceForm.order.orderId != null}">
					<tr>
						<td>
							<c:if test="${!fn:contains(invoiceForm.order.status,'x')}">
								<a class="mr-10" href="invoice.jhtm?order=${invoiceForm.order.orderId}&__cancelBuySafe=true"><fmt:message key="cancel" /></a>
							</c:if>
							<fmt:message key="buySafeBondGuarntee" />:
						</td>
						<td class="invoice" align="right"><fmt:formatNumber value="${invoiceForm.order.bondCost}" pattern="${pattern}"/></td>
					</tr>
				</c:if>
				<tr>
					<td><fmt:message key="grandTotal" />:</td>
					<td><fmt:formatNumber value="${invoiceForm.order.grandTotal}" pattern="${pattern}"/></td>		   
				</tr>
				<tr>
					<td><fmt:message key="totalQuantity" />:</td>
					<td><fmt:formatNumber value="${invoiceForm.order.totalProductQuantity}" pattern="#,##0"/></td>
				</tr>
				<tr>
					<td><fmt:message key="totalWeight" />:</td>
					<td><fmt:formatNumber value="${invoiceForm.order.totalProductWeight}" pattern="#,##0.00"/></td>
				</tr>
				<c:if test="${gSiteConfig['gPAYMENTS']}">
					<tr>
						<td><fmt:message key="balance" />:</td>
						<td><fmt:formatNumber value="${invoiceForm.order.grandTotal - invoiceForm.order.amountPaid}" pattern="${pattern}"/></td>
					</tr>
				</c:if>
				<c:if test="${invoiceForm.order.hasRewardPoints}">
					<tr>
						<td><c:out value="${siteConfig['GROUP_REWARD_POINTS_TITLE'].value}"/>:</td>
						<td><fmt:formatNumber value="${invoiceForm.order.rewardPointsTotal}" pattern="#,##0.00"/></td>
					</tr>
				</c:if>
				<c:if test="${gSiteConfig['gSITE_DOMAIN'] == 'yayme.com.au'}">
					<tr>
						<td><fmt:message key="gst" />:</td>
						<td><fmt:formatNumber value="${invoiceForm.order.grandTotal/11}" pattern="#,##0.00"/></td>
					</tr>
				</c:if>
			</tbody>
		</table>
	</div>
</div>