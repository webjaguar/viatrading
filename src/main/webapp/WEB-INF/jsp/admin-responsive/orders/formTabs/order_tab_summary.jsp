<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ taglib uri="http://www.springframework.org/security/tags" prefix="sec" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ taglib uri="/WEB-INF/tlds/spring.tld" prefix="spring" %>
<%@ taglib uri="/WEB-INF/tlds/custom.tld" prefix="wj" %>

<div class="row justify-content-between summary-banner mt-3">
	<div class="col-md-6 col-lg-3 summary-box">
		<label class="col-form-label"><fmt:message key="billingInformation" /></label>
		<table class="summary-table">
			<tr>
				<td><fmt:message key="firstName" /></td>
				<td>
					<spring:bind path="invoiceForm.order.billing.firstName">
						<input type="text" name="${status.expression}" value="${status.value}" class="form-control">
					</spring:bind>
				</td>
			</tr>
			<tr>
				<td><fmt:message key="lastName" /></td>
				<td>
					<spring:bind path="invoiceForm.order.billing.lastName">
						<input type="text" name="${status.expression}" value="${status.value}" class="form-control">
					</spring:bind>
				</td>
			</tr>
			<tr>
				<td><fmt:message key="country" /></td>
				<td>
					<form:select path="order.billing.country" onchange="toggleStateProvince(this, 'billing')" class="custom-select">
						<option value="">Please Select</option>
						<form:options items="${countries}" itemValue="code" itemLabel="name"/>  
					</form:select>
				</td>
			</tr>
			<tr>
				<td><fmt:message key="company" /></td>
				<td>
					<spring:bind path="invoiceForm.order.billing.company">
						<input type="text" name="${status.expression}" value="${status.value}" size="20" class="form-control">
					</spring:bind>
				</td>
			</tr>
			<tr>
				<td><fmt:message key="address" /> 1</td>
				<td>
					<spring:bind path="invoiceForm.order.billing.addr1">
						<input type="text" name="${status.expression}" value="${status.value}" size="20" class="form-control">
					</spring:bind>
				</td>
			</tr>
			<tr>
				<td><fmt:message key="address" /> 2</td>
				<td>
					<spring:bind path="invoiceForm.order.billing.addr2">
						<input type="text" name="${status.expression}" value="${status.value}" size="20" class="form-control">
					</spring:bind>
				</td>
			</tr>
			<tr>
				<td><fmt:message key="city" /></td>
				<td>
					<spring:bind path="invoiceForm.order.billing.city">
						<input type="text" name="${status.expression}" value="${status.value}" class="form-control">
					</spring:bind>
				</td>
			</tr>
			<tr>
			    <td><fmt:message key="stateProvince" /></td>
				<td>
					<spring:bind path="invoiceForm.order.billing.stateProvince">
						<select id="billing_state" name="<c:out value="${status.expression}"/>" class="custom-select">
							<option value="">Please Select</option>
							<c:forEach items="${states}" var="state">
								<option value="${state.code}" <c:if test="${state.code == status.value}">selected</c:if>>${state.name}</option>
							</c:forEach>      
						</select>
						<select id="billing_ca_province" name="<c:out value="${status.expression}"/>" class="custom-select">
							<option value="">Please Select</option>
							<c:forEach items="${caProvinceList}" var="province">
								<option value="${province.code}" <c:if test="${province.code == status.value}">selected</c:if>>${province.name}</option>
							</c:forEach>      
						</select> 
						<input id="billing_province" type="text" name="<c:out value="${status.expression}"/>" value="<c:out value="${status.value}"/>" class="form-control">
					</spring:bind>
				</td>
			</tr>
			<tr>
				<td><fmt:message key="zipPostalCode" /></td>
				<td>
					<spring:bind path="invoiceForm.order.billing.zip">
						<input type="text" name="${status.expression}" value="${status.value}" class="form-control">
					</spring:bind>
				</td>
			</tr>
			<tr>
				<td><fmt:message key="phone" /></td>
				<td>
					<spring:bind path="invoiceForm.order.billing.phone">
						<input type="text" name="${status.expression}" value="${status.value}" class="form-control">
					</spring:bind> 
				</td>
			</tr>
			<tr>
				<td><fmt:message key="cellPhone" /></td>
				<td>
					<spring:bind path="invoiceForm.order.billing.cellPhone">
						<input type="text" name="${status.expression}" value="${status.value}" class="form-control">
					</spring:bind> 
				</td>
			</tr>
			<tr>
				<td><fmt:message key="fax" /></td>
				<td> 
					<spring:bind path="invoiceForm.order.billing.fax">
						<input type="text" name="${status.expression}" value="${status.value}" class="form-control">
					</spring:bind>
				</td>
			</tr>
		</table>
	</div>
	<div class="col-md-6 col-lg-3 summary-box">
		<label class="col-form-label">
			<c:choose>
				<c:when test="${invoiceForm.order.workOrderNum != null}">
					<fmt:message key="service" /> <fmt:message key="location" />
				</c:when>
				<c:otherwise>
					<fmt:message key="shippingInformation" />
				</c:otherwise>
			</c:choose>
		</label>
		<table class="summary-table">
			<c:if test="${addressList != null and invoiceForm.order.workOrderNum == null}">
				<tr>
					<td colspan="2" class="pr-0">
						<select style="font-size:10px;" name="addressSelected" id="addressSelected" onChange="chooseAddress(this)" class="custom-select">
							<c:forEach items="${addressList}" var="address" varStatus="status">
								<option value="${address.id}"><c:out value="${address.firstName}" /> <c:out value="${address.lastName}" /> <c:out value="${address.addr1}" /> <c:out value="${address.city}" /> <c:out value="${address.zip}" /> <c:out value="${address.stateProvince}" /> <c:out value="${address.country}" /></option>
							</c:forEach>
						</select>
						<c:forEach items="${addressList}" var="address" varStatus="status">
							<input type="hidden" id="firstname_${address.id}" value="${address.firstName}" />
							<input type="hidden" id="lastname_${address.id}" value="${address.lastName}" />
							<input type="hidden" id="country_${address.id}" value="${address.country}" />
							<input type="hidden" id="company_${address.id}" value="${address.company}" />
							<input type="hidden" id="addr1_${address.id}" value="${address.addr1}" />
							<input type="hidden" id="addr2_${address.id}" value="${address.addr2}" />
							<input type="hidden" id="city_${address.id}" value="${address.city}" />
							<input type="hidden" id="state_${address.id}" value="${address.stateProvince}" />
							<input type="hidden" id="zip_${address.id}" value="${address.zip}" />
							<input type="hidden" id="phone_${address.id}" value="${address.phone}" />
							<input type="hidden" id="cellPhone_${address.id}" value="${address.cellPhone}" />
							<input type="hidden" id="fax_${address.id}" value="${address.fax}" />
							<input type="hidden" id="residential_${address.id}" value="${address.residential}" />
							<input type="hidden" id="code_${address.id}" value="${address.code}" />
						</c:forEach>      
					</td>
				</tr>
			</c:if>
			<tr>
				<td><fmt:message key="firstName" /></td>
				<td>
					<form:input path="order.shipping.firstName" class="form-control"/>
				</td>
			</tr>
			<tr>
				<td><fmt:message key="lastName" />:</td>
				<td>
					<form:input path="order.shipping.lastName" class="form-control"/>
				</td>
			</tr>
			<tr>
				<td><fmt:message key="country" /></td>
				<td>
					<form:select path="order.shipping.country"  onchange="toggleStateProvince(this, 'shipping')" class="custom-select">
						<option value="">Please Select</option>
						<form:options items="${countries}" itemValue="code" itemLabel="name"/>
					</form:select>
				</td>
			</tr>
			<tr>
				<td><fmt:message key="company" /></td>
				<td>
					<form:input path="order.shipping.company" class="form-control"/>
				</td>
			</tr>
			<tr>
				<td><fmt:message key="address" /> 1</td>
				<td>
					<form:input path="order.shipping.addr1" class="form-control"/>
				</td>
			</tr>
			<tr>
				<td><fmt:message key="address" /> 2</td>
				<td>
					<form:input path="order.shipping.addr2" class="form-control"/>
				</td>
			</tr>
			<tr>
				<td><fmt:message key="city" /></td>
				<td>
					<form:input path="order.shipping.city" class="form-control" />
				</td>
			</tr>
			<tr>
				<td><fmt:message key="stateProvince" /></td>
				<td>
					<spring:bind path="invoiceForm.order.shipping.stateProvince" >
						<select id="shipping_state" name="<c:out value="${status.expression}"/>" class="custom-select">
							<option value="">Please Select</option>
							<c:forEach items="${states}" var="state">
								<option value="${state.code}" <c:if test="${state.code == status.value}">selected</c:if>>${state.name}</option>
							</c:forEach>      
						</select>
						<select id="shipping_ca_province" name="<c:out value="${status.expression}"/>" class="custom-select">
							<option value="">Please Select</option>
							<c:forEach items="${caProvinceList}" var="province">
								<option value="${province.code}" <c:if test="${province.code == status.value}">selected</c:if>>${province.name}</option>
							</c:forEach>      
						</select>
						<input id="shipping_province" type="text" name="<c:out value="${status.expression}"/>" value="<c:out value="${status.value}"/>" class="form-control">
					</spring:bind>
				</td>
			</tr>
			<tr>
				<td><fmt:message key="zipPostalCode" /></td>
				<td>
					<form:input path="order.shipping.zip" class="form-control"/>
				</td>
			</tr>
			<tr>
				<td><fmt:message key="phone" /></td>
				<td>
					<form:input path="order.shipping.phone" class="form-control"/>
				</td>
			</tr>
			<tr>
				<td><fmt:message key="cellPhone" /></td>
				<td>
					<form:input path="order.shipping.cellPhone" class="form-control"/>
				</td>
			</tr>  
			<tr>
				<td><fmt:message key="fax" /></td>
				<td>
					<form:input path="order.shipping.fax" class="form-control"/>
				</td>
			</tr>
			<tr>
				<td><fmt:message key="residential" /></td>
				<td> 
					<form:checkbox path="order.shipping.residential" class="form-control"/>
				</td>
			</tr>
			<tr>
				<td><fmt:message key="code" /></td>
				<td> 
					<form:input path="order.shipping.code" class="form-control"/>
				</td>
			</tr>
			<c:if test="${workOrder.service.alternateContact != ''}">	
				<tr>
					<td><fmt:message key="alternateContact" /></td>
					<td><c:out value="${workOrder.service.alternateContact}"/></td>
				</tr>
			</c:if>
		</table>
	</div>
	<div class="col-md-6 col-lg-3 summary-box">
		<label class="col-form-label"><fmt:message key="customerInformation" /></label>
		<table class="summary-table">
			<tr>
				<td><fmt:message key="account"/>#</td>
				<td><c:out value="${customer.accountNumber}"/></td>
			</tr>
			<tr>
				<td><fmt:message key="taxID"/>#</td>
				<td><c:out value="${customer.taxId}"/></td>
			</tr>
			<tr>
				<td><fmt:message key="${orderOrQuote}"/></td>
				<td><a href="ordersList.jhtm?email=<c:out value='${customer.username}'/>"><c:out value="${customerOrderInfo.orderCount}"/></a></td>
			</tr>
			<tr>
				<td><fmt:message key="lastOrder"/></td>
				<td><fmt:formatDate pattern="${siteConfig['SITE_DATE_FORMAT'].value}" timeZone="${siteConfig['SITE_TIME_ZONE'].value}" value="${customerOrderInfo.lastOrderDate}"/></td>
			</tr>
			<tr>
				<td><fmt:message key="invoiceDueDate"/></td>
				<td>
		  			<form:input path="order.invoiceDueDate" class="form-control"/>
				</td>
			</tr>
			<c:if test="${languageCodes != null}">
				<tr>
					<td><fmt:message key="language"/></td>
					<td>
						<form:select path="order.languageCode" class="custom-select">
							<form:option value="en"><fmt:message key="language_en"/></form:option>
							<c:forEach items="${languageCodes}" var="i18n">
								<form:option value="${i18n.languageCode}"><fmt:message key="language_${i18n.languageCode}"/></form:option>
							</c:forEach>
						</form:select>
					</td>
				</tr>
			</c:if>
			<tr>
				<td><fmt:message key="customerPhone"/>#</td>
				<td><c:out value="${phoneNumber}"/></td>
			</tr>
			<c:if test="${customerNote != null && customerNote != ''}" >
				<tr>
					<td><fmt:message key="customerNote"/></td>
					<td><c:out value="${customerNote}"/></td>
				</tr>
			</c:if>
		</table>
	</div>
	<div class="col-md-6 col-lg-3 summary-box">
		<label class="col-form-label"><fmt:message key="${orderName}Information" /></label>
		<table class="summary-table">
			<c:if test="${siteConfig['CLICK_DIALING'].value}">
				<tr>
					<td><fmt:message key="salesRep"/> 1</td>
					<td>
						<form:select path="order.salesRepId" class="custom-select">
							<form:option value=""><fmt:message key="none" /></form:option>
							<form:options items="${salesRepList}" itemValue="id" itemLabel="name"/>
						</form:select>
					</td>
				</tr>
				<tr>
					<td><fmt:message key="salesRep"/> 2</td>
					<td>
						<form:select path="order.salesRepId2" class="custom-select">
							<form:option value=""><fmt:message key="none" /></form:option>
							<form:options items="${salesRepList}" itemValue="id" itemLabel="name"/>
						</form:select>
					</td>
				</tr>
				<tr>
					<td>Dialer 1</td>
					<td>
						<form:select path="order.salesRepProcessedById" class="custom-select">
							<form:option value=""><fmt:message key="none" /></form:option>
							<form:options items="${salesRepList}" itemValue="id" itemLabel="name"/>
						</form:select>
					</td>
				</tr>
				<tr>
					<td>Dialer 2</td>
					<td>
						<form:select path="order.salesRepProcessedById2" class="custom-select">
							<form:option value=""><fmt:message key="none" /></form:option>
							<form:options items="${salesRepList}" itemValue="id" itemLabel="name"/>
						</form:select>
					</td>
				</tr>
				<tr>
					<td>Manager</td>
					<td>
						<form:select path="order.salesRepManagerId" class="custom-select">
							<form:option value=""><fmt:message key="none" /></form:option>
							<form:options items="${salesRepList}" itemValue="id" itemLabel="name"/>
						</form:select>
					</td>
				</tr>
			</c:if>
			<c:if test="${gSiteConfig['gSALES_REP'] and !siteConfig['CLICK_DIALING'].value}">
				<tr>
					<td><fmt:message key="salesRep"/></td>
					<td>
						<form:select path="order.salesRepId" class="custom-select">
							<form:option value=""><fmt:message key="none" /></form:option>
							<form:options items="${salesRepList}" itemValue="id" itemLabel="name"/>
						</form:select>
					</td>
				</tr>
				<c:if test="${!fn:contains(siteConfig['SITE_URL'].value, 'paperenter')}">
					<tr>
						<td><fmt:message key="processedBy" /></td>
						<td>
							<form:select path="order.salesRepProcessedById" class="custom-select">
								<form:option value=""><fmt:message key="none" /></form:option>
								<form:options items="${salesRepList}" itemValue="id" itemLabel="name"/>
							</form:select>
						</td>
					</tr>
				</c:if>
				<c:if test="${siteConfig['NO_COMMISSION_FLAG'].value == 'true'}">
					<tr>
						<td>Shared Commission</td>
						<td>
							<form:select path="order.nc" class="custom-select">
								<form:option value="true"><fmt:message key="yes" /></form:option>
								<form:option value="false"><fmt:message key="no" /></form:option>
							</form:select>
						</td>
					</tr>
				</c:if>
			</c:if>
			<c:if test="${gSiteConfig['gINVOICE_APPROVAL']}"> 
				<tr>
					<td>Require Approval</td>
					<td>
						<form:select path="order.approval" class="custom-select">
							<form:option value=""><fmt:message key="none" /></form:option>
							<form:option value="ra"><fmt:message key="approvalStatus_ra" /></form:option>
							<form:option value="ap"><fmt:message key="approvalStatus_ap" /></form:option>
						</form:select>
					</td>
				</tr>
			</c:if>
			<tr>
				<td><fmt:message key="${orderOrQuote }"/> <fmt:message key="type"/></td>			    	
				<td>
					<form:select path="order.orderType" class="custom-select">
						<form:option value=""></form:option>
						<c:forTokens items="${siteConfig['ORDER_TYPE'].value}" delims="," var="type" varStatus="status">
							<form:option value ="${type}"><c:out value ="${type}" /></form:option>
						</c:forTokens>
					</form:select>
				</td>
			</tr> 
			<c:if test="${orderOrQuote == 'quote'}">
				<td><fmt:message key="percentageToClose"/></td>
				<td>
					<form:input path="order.percentageToClose" class="form-control"/>%  
				</td>
			</c:if>
			<c:if test="${siteConfig['ORDER_FLAG1_NAME'].value != ''}">
				<tr>
					<td><c:out value="${siteConfig['ORDER_FLAG1_NAME'].value}" /></td>
					<td>
						<form:select path="order.flag1" class="custom-select">
							<form:option value=""></form:option>
							<form:option value="1"><fmt:message key="yes" /></form:option>
							<form:option value="0"><fmt:message key="no" /></form:option>
						</form:select>  
					</td>
				</tr>
			</c:if>
			<c:if test="${siteConfig['ORDER_CUSTOM_FIELD1_NAME'].value != ''}">
				<tr>
					<td><c:out value="${siteConfig['ORDER_CUSTOM_FIELD1_NAME'].value}" /></td>
					<td>
						<form:input path="order.customField1" class="form-control"/>  
					</td>
				</tr>
			</c:if>
			<c:if test="${siteConfig['ORDER_CUSTOM_FIELD2_NAME'].value != ''}">
				<tr>
					<td><c:out value="${siteConfig['ORDER_CUSTOM_FIELD2_NAME'].value}" /></td>
					<td>
			     		<form:input path="order.customField2" class="form-control"/>  
					</td>
				</tr>
			</c:if>
			<c:if test="${siteConfig['ORDER_CUSTOM_FIELD3_NAME'].value != ''}">
				<tr>
					<td><c:out value="${siteConfig['ORDER_CUSTOM_FIELD3_NAME'].value}" /></td>
					<td>
						<form:input path="order.customField3" class="form-control"/>  
					</td>
				</tr>
			</c:if>
			<tr>
				<td><fmt:message key="trackcode"/></td>
				<td>
					<form:input path="order.trackcode" class="form-control"/>
			    </td>
			</tr>
			<tr>
				<td><fmt:message key="description"/></td>
				<td>
					<form:input path="order.description" class="form-control"/>
				</td>
			</tr>
			<c:if test="${siteConfig['USER_EXPECTED_DELIVERY_TIME'].value == 'true'}" >
				<tr>
					<td><fmt:message key="userExpectedDueDate" /></td>
					<td>
						<form:input path="order.userDueDate" value="${order.userDueDate}" class="form-control"/>
					</td>
				</tr>
			</c:if>
			<c:if test="${siteConfig['REQUESTED_CANCEL_DATE'].value == 'true'}" >
				<tr>
					<td><fmt:message key="requestedCancelDate" /></td>
					<td>
						<form:input path="order.requestedCancelDate" value="${order.requestedCancelDate}" class="form-control"/>
					</td>
				</tr>
			</c:if>
			<sec:authorize ifAnyGranted="ROLE_AEM,ROLE_ADMIN_${prop_site_id},ROLE_OVERRIDE_PAYMENT_ALERT">
				<c:if test="${gSiteConfig['gPAYMENTS'] and customer.paymentAlert != null}">
					<tr>
						<td><fmt:message key="payment"/> <fmt:message key="alert"/></td>
						<td>
							<form:checkbox path="order.paymentAlert" />
						</td>
					</tr>
				</c:if>
			</sec:authorize>
		</table>
	</div>
</div>