<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="/WEB-INF/tlds/spring.tld" prefix="spring" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ taglib uri="http://www.springframework.org/security/tags" prefix="sec" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ taglib uri="/WEB-INF/tlds/custom.tld" prefix="wj" %>

<sec:authorize ifAnyGranted="ROLE_AEM,ROLE_ADMIN_${prop_site_id}">
	<c:set var="canEdit" value="true"/>
</sec:authorize>
<div class="table-wrapper">
	<table class="complex invoice">
		<thead>
			<tr>
			    <c:set var="cols" value="0"/>
			    <c:set var="cols2" value="0"/>
			    <c:set var="cols3" value="5"/>
				<c:if test="${invoiceForm.order.workOrderNum == null or canEdit}" >
					<c:set var="cols" value="${cols+1}"/>
					<th class="batch"><fmt:message key="remove" /></th>
				</c:if>
				<th class="offset"><fmt:message key="line" />#</th>
				<c:if test="${siteConfig['INVOICE_LINEITEMS_SORTING'].value == 'rank'}" >
					<c:set var="cols" value="${cols+1}"/>
				    	<c:set var="cols3" value="${cols3+1}"/>
					<th><fmt:message key="rank" /></th>
				</c:if>
				<th style="min-width: 100px;"><fmt:message key="productId" /></th>
				<th style="min-width: 100px;"><fmt:message key="productSku" /></th>
				<th style="min-width: 200px"><fmt:message key="productName" /></th>
				<c:if test="${siteConfig['INVOICE_NOTE_PER_LINEITEM'].value == 'true'}" >
					<c:set var="cols" value="${cols+1}"/>
					<th><fmt:message key="productNote" /></th>
				</c:if>
				<th style="text-align: center"><fmt:message key="quantity" /></th>
				<c:if test="${gSiteConfig['gINVENTORY'] && invoiceForm.order.hasLowInventoryMessage}">
					<c:set var="cols" value="${cols+1}"/>
					<th><fmt:message key="lowInventory" /></th>
				</c:if>
				<c:if test="${invoiceForm.order.hasPacking}">
					<c:set var="cols" value="${cols+1}"/>
					<th><fmt:message key="packing" /></th>
				</c:if>
				<c:if test="${invoiceForm.order.hasContent}">
					<c:set var="cols" value="${cols+1}"/>
						<th><fmt:message key="content" /></th>
				</c:if>
				<c:if test="${invoiceForm.order.hasCustomShipping}">
					<c:set var="cols" value="${cols+1}"/>
					<th><fmt:message key="customShipping" /></th>
				</c:if>
				<th style="text-align:center"><fmt:message key="productPrice" /><c:if test="${order.hasContent}"> / <c:out value="${siteConfig['CASE_UNIT_TITLE'].value}" /></c:if></th>
				<c:if test="${invoiceForm.order.hasRewardPoints}">
					<c:set var="cols2" value="${cols2+1}"/>
					<th><fmt:message key="points" /></th>
				</c:if>
				<th style="min-width: 80px; text-align: right;"><fmt:message key="productWeight" /></th>
				<c:if test="${gSiteConfig['gSPECIAL_PRICING']}" >
					<c:set var="cols2" value="${cols2+1}"/>
					<th style="min-width: 6px"><fmt:message key="specialPricing" /></th>
				</c:if>
				<c:if test="${gSiteConfig['gPURCHASE_ORDER'] && siteConfig['SHOW_COST'].value == 'true' && invoiceForm.order.hasCost}" >
					<c:set var="cols2" value="${cols2+1}"/>
					<th style="min-width:40px; text-align: right;"><fmt:message key="cost" /></th>
				</c:if>
				<c:if test="${false and siteConfig['GROSS_PROFIT_ON_ADMIN_INVOICE'].value == 'true'}">
					<c:set var="cols2" value="${cols2+1}"/>
					<th style="min-width:40px"><fmt:message key="cost" /></th>
					<c:set var="cols2" value="${cols2+1}"/>
					<th style="min-width:40px"><fmt:message key="gpMargin" /></th>
				</c:if>
				<th style="text-align: center"><fmt:message key="taxable" /></th>
				<c:if test="${gSiteConfig['gSPECIAL_TAX'] }">
					<c:set var="cols2" value="${cols2+1}"/>
					<th><fmt:message key="tax" /></th>
				</c:if>
				<th style="text-align:right"><fmt:message key="total" /></th>
			</tr>
		</thead>
		<tbody>
			<c:if test="${false and siteConfig['GROSS_PROFIT_ON_ADMIN_INVOICE'].value == 'true'}">
				<c:set value="0.0" var="totalOriginalPrice"/>
				<c:set value="0.0" var="totalCost"/>
			</c:if>
			<c:forEach var="lineItem" items="${invoiceForm.order.lineItems}" varStatus="status">
				<input type="hidden" name="index_${lineItem.lineNumber}"  value="1" id="index_${lineItem.lineNumber}">
				<c:if test="${status.first and (invoiceForm.order.workOrderNum == null or canEdit)}">
					<tr class="line-row">
						<td colspan="${cols3}">EXISTING ITEMS:</td>
						<td style="min-width:200px;">
							<table style="min-width:150px;">
								<tr>
									<td style="width:70px">Original</td>
									<td style="width:130px;text-align: center;">Quantity</td>
								</tr>
							</table>
						</td>
						<c:if test="${cols > 2}">
							<td colspan="${cols - 2}"></td>
						</c:if>
						<td style="min-width:${siteConfig['GROSS_PROFIT_ON_ADMIN_INVOICE'].value == 'true' ? '480px;' : '300px;'}">
							<table>
								<tr>
									<td style="text-align:right;width:80px">Original</td>
									<td style="text-align:right;width:140px">Discount</td>
									<c:if test="${siteConfig['GROSS_PROFIT_ON_ADMIN_INVOICE'].value == 'true'}">
										<td style="text-align:right;width:80px">Cost</td>
										<td style="text-align:right;width:80px">Margin</td>
									</c:if>
									<td style="width:80px;text-align: right;">Price</td>
								</tr>
							</table>
						</td> 
						<td colspan="${3+cols+cols2}"></td>
					</tr>
				</c:if>
				<tr>
					<c:if test="${invoiceForm.order.workOrderNum == null or canEdit}" >
						<td style="text-align:center"><input type="checkbox" name="__remove_${lineItem.lineNumber}" <c:if test="${lineItem.processed > 0}" >disabled='true'</c:if><c:if test="${gSiteConfig['gSPECIAL_PRICING']}" >onChange="toggleSpecialPricing(this, '__special_pricing_${lineItem.lineNumber}')"</c:if>></td>
					</c:if>
					<td style="text-align:center"><c:out value="${status.count}"/></td>
					<c:if test="${siteConfig['INVOICE_LINEITEMS_SORTING'].value == 'rank'}" >
						<td style="text-align:center"><input type="text" name="__rank_${lineItem.lineNumber}" value="<c:out value="${lineItem.rank}"/>" class="form-control"></td>
					</c:if>
					<td style="text-align:center"><c:out value="${lineItem.productId}"/></td>
					<td><c:out value="${lineItem.product.sku}"/></td>
					<td><input type="text" name="__name_${lineItem.lineNumber}" value="<c:out value="${lineItem.product.name}"/>" maxlength="120" class="form-control"><br/>
						<c:if test="${siteConfig['SHOW_IMAGE_ON_INVOICE'].value == 'true' and lineItem.hasProductAttributesImage}" >
							<c:forEach items="${lineItem.productAttributes}" var="productAttribute">
								<c:if test="${!empty productAttribute.imageUrl}" > 
									<c:choose>
										<c:when test="${fn:endsWith(productAttribute.imageUrl, 'pdf')}">
											<span class="optionLabel"><c:out value="${productAttribute.optionName}" />:</span>
											<a href="${productAttribute.imageUrl}" target="_blank">
												<span> <c:out value="${fn:split(productAttribute.imageUrl, '/')[fn:length(fn:split(productAttribute.imageUrl, '/')) - 1]}"></c:out> </span>
											</a>	
										</c:when>
										<c:otherwise>
											<c:choose>
												<c:when test="${productAttribute.absolute or fn:startsWith(productAttribute.imageUrl, '/temp')}"><img src="<c:url value="${productAttribute.imageUrl}"/>" border="0" class="invoiceOptionImage" /><c:out value="${fn:split(productAttribute.imageUrl, '/')[fn:length(fn:split(productAttribute.imageUrl, '/')) - 1]}"></c:out></c:when>
												<c:otherwise><img src="<c:url value="/assets/Image/Product/options/${productAttribute.imageUrl}"/>" border="0" class="invoiceOptionImage" /><c:out value="${fn:split(productAttribute.imageUrl, '/')[fn:length(fn:split(productAttribute.imageUrl, '/')) - 1]}"></c:out></c:otherwise>
											</c:choose>
										</c:otherwise>	   
								    </c:choose>
							    		<br/>
								</c:if>
							</c:forEach>
						</c:if>
						<c:if test="${false and lineItem.subscriptionInterval != null}">
							<c:set var="intervalType" value="${fn:substring(lineItem.subscriptionInterval, 0, 1)}"/>
							<c:set var="intervalUnit" value="${fn:substring(lineItem.subscriptionInterval, 1, 2)}"/>
							<div class="invoice_lineitem_subscription">
								<fmt:message key="subscription"/> <fmt:message key="details"/> : <fmt:message key="delivery"/>
								<c:choose>
									<c:when test="${intervalUnit == '1'}"><fmt:message key="every_${intervalType}"><fmt:param value="${intervalUnit}"/></fmt:message></c:when>
									<c:otherwise><fmt:message key="every_${intervalType}s"><fmt:param value="${intervalUnit}"/></fmt:message></c:otherwise>
								</c:choose>
							</div>
							<div class="invoice_lineitem_subscription"><c:out value="${lineItem.subscriptionCode}"/></div>
						</c:if>
					</td>
					<c:if test="${siteConfig['INVOICE_NOTE_PER_LINEITEM'].value == 'true'}" >
						<td>
							<textarea name="__lineItem_note_${lineItem.lineNumber}"><c:out value="${lineItem.note}"/></textarea>
						</td>
					</c:if>
					<td style="min-width:200px"> 
						<table>
							<tr>
								<td style="width:70px; text-align:center;">
									<c:out value="${lineItem.originalQuantity}"/>
								</td>
								<td style="width:70px;">
									<c:choose>
										<c:when test="${invoiceForm.order.workOrderNum != null and !canEdit}">
											<c:out value="${lineItem.quantity}"/>
										</c:when>
										<c:when test="${gSiteConfig['gINVENTORY']}" >
											<input style="text-align: right;" type="text" name="__quantity_<c:out value="${lineItem.lineNumber}"/>" value="${lineItem.quantity}" class="form-control">
										</c:when>
										<c:otherwise>
											<input style="text-align: right;" type="text" name="__quantity_<c:out value='${lineItem.lineNumber}'/>" value="${lineItem.quantity}" class="form-control">
										</c:otherwise>
									</c:choose>
								</td>
								<c:if test="${siteConfig['INVENTORY_SHOW_ONHAND'].value=='true' and lineItem.product.inventory!=null and lineItem.product.inventoryAFS != null}">
									<td style="width: 60px;text-align:center;"><span>(<c:out value="${lineItem.product.inventory}"/>/<c:out value="${lineItem.product.inventoryAFS}"/>)</span></td>
								</c:if>
							</tr>
							<c:if test="${lineItem.priceCasePackQty != null}">
								<tr>
									<td colspan="2">
										<div class="casePackPriceQty">(<c:out value="${lineItem.priceCasePackQty}"/> per <c:out value="${siteConfig['PRICE_CASE_PACK_UNIT_TITLE'].value}" /> )
										</div>
									</td>
								</tr>
							</c:if>
						</table>
					</td>
					<c:if test="${gSiteConfig['gINVENTORY'] && invoiceForm.order.hasLowInventoryMessage}">
						<td style="text-align:center"><input style="text-align: right;" type="text" name="__low_inventory_message_<c:out value='${lineItem.lineNumber}'/>" value="${lineItem.lowInventoryMessage}" class="form-control"></td>
					</c:if>
					<c:if test="${invoiceForm.order.hasPacking}">
						<td style="text-align:center"><c:out value="${lineItem.product.packing}" /></td>
					</c:if>
					<c:if test="${invoiceForm.order.hasContent}">
						<td style="text-align:center">
							<input style="text-align: right;" type="text" name="__caseContent_<c:out value='${lineItem.lineNumber}'/>" value="${lineItem.product.caseContent}" size="6">
						</td>
					</c:if>
					<c:if test="${invoiceForm.order.hasCustomShipping}"><td style="text-align:right;"><fmt:formatNumber value="${lineItem.customShippingCost}" pattern="${pattern}"/></td></c:if>
					<c:choose>
						<c:when test="${invoiceForm.order.hasRewardPoints && lineItem.product.productType == 'REW'}">
			    				<td style="text-align:right"></td>
							<td style="text-align:right"><fmt:formatNumber value="${lineItem.unitPrice}" pattern="#,##0.00"/></td>	
							<input type="hidden" name="__percent_<c:out value="${lineItem.lineNumber}"/>" value="false"/>
							<input type="hidden" name="__originalPrice_<c:out value="${lineItem.lineNumber}"/>" value="<fmt:formatNumber value="${lineItem.originalPrice}"/>" />
						</c:when>
		              	<c:otherwise>
							<td style="min-width:${siteConfig['GROSS_PROFIT_ON_ADMIN_INVOICE'].value == 'true' ? '480px;' : '300px;'}" style="text-align:right">
								<table>
									<tr>
										<td style="width:80px;"><input  style="text-align: right" type="text" id="lineItemPrice" onkeyup="changePrice(this.value,${lineItem.lineNumber})" name="__originalPrice_<c:out value="${lineItem.lineNumber}"/>" value="<fmt:formatNumber value="${lineItem.originalPrice}" pattern="${pattern}"/>" class="form-control" /></td>
										<td style="width:140px;">
											<input style="width:50%; text-align: right;" type="text" name="__discount_<c:out value="${lineItem.lineNumber}"/>" value="<c:out value="${lineItem.discount}"/>" class="form-control" />
											<select style="width:45%" name="__percent_<c:out value="${lineItem.lineNumber}"/>" class="custom-select">
												<option value="true">%</option><option <c:if test="${!lineItem.percent}">selected</c:if> value="false">Fix</option>
											</select>
										</td>
										<c:if test="${siteConfig['GROSS_PROFIT_ON_ADMIN_INVOICE'].value == 'true'}">
											<td style="width:80px;"><input  style="text-align: right" type="text" id="lineItemCost" onkeyup="changeCost(this.value,${lineItem.lineNumber})" value="<fmt:formatNumber value="${lineItem.cost / lineItem.quantity}" pattern="${pattern}"/>" class="form-control" /></td>
								       		<td style="width:80px;"><input  style="text-align: right" type="text" id="lineItemMargin" onkeyup="changeMargin(this.value,${lineItem.lineNumber})" value="<fmt:formatNumber value="${lineItem.marginPercent}" pattern="${pattern}"/>" class="form-control" />%</td>
										</c:if>
										<td style="width:80px;"><input style="text-align: right" type="text" id="lineItemFinalPrice" name="__unitPrice_<c:out value="${lineItem.lineNumber}"/>" value="<fmt:formatNumber value="${lineItem.originalPrice}" pattern="${pattern}"/>" class="form-control" /></td>
									</tr>
								</table>
							</td>
							<c:if test="${invoiceForm.order.hasRewardPoints}"> <td style="text-align:right"></td></c:if>
						</c:otherwise>
					</c:choose>
					<td>
						<input  style="text-align: right" type="text" name="__weight_<c:out value="${lineItem.lineNumber}"/>" value="<fmt:formatNumber value="${lineItem.weight}" pattern="##0.00"/>" class="form-control"/>
					</td>
					<c:if test="${gSiteConfig['gSPECIAL_PRICING']}" >
						<td style="text-align:center"><input type="checkbox" id="__special_pricing_${lineItem.lineNumber}" name="__special_pricing_${lineItem.lineNumber}" <c:if test="${lineItem.setSpecialPricing}">checked</c:if> /></td>
					</c:if>
					<c:if test="${false and siteConfig['GROSS_PROFIT_ON_ADMIN_INVOICE'].value == 'true' && lineItem.cost != null && lineItem.cost != 0.0}">
						<c:set value="${lineItem.cost + totalCost}" var="totalCost"/>
						<c:set value="${(lineItem.originalPrice * lineItem.quantity) + totalOriginalPrice}" var="totalOriginalPrice"/>
					</c:if>
					<c:if test="${gSiteConfig['gPURCHASE_ORDER'] && siteConfig['SHOW_COST'].value == 'true' && invoiceForm.order.hasCost}" >
						<td style="text-align: right"><c:out value="${lineItem.unitCost}" /></td>
					</c:if>
					<c:if test="${false and siteConfig['GROSS_PROFIT_ON_ADMIN_INVOICE'].value == 'true'}">
						<td style="text-align: right"><c:out value="${lineItem.cost}" /></td>
						<td style="text-align: right"><input  style="text-align: right" type="text" name="__unitMarginPercent_<c:out value="${lineItem.lineNumber}"/>" value="<fmt:formatNumber value="${lineItem.marginPercent}" pattern="${pattern}"/>" class="form-control" />%</td>
					</c:if>
					<td style="text-align:center"><input type="checkbox" name="__taxable_${lineItem.lineNumber}" <c:if test="${lineItem.product.taxable}">checked</c:if>></td>
					<c:if test="${gSiteConfig['gSPECIAL_TAX']}">
						<td style="text-align:center"><input style="text-align: right" type="text" name="__taxRate_${lineItem.lineNumber}" value="${lineItem.taxRate}" class="form-control" /></td>
					</c:if>
					<c:choose>
						<%--Reward Points --%>
						<c:when test="${invoiceForm.order.hasRewardPoints and lineItem.product.productType == 'REW'}">
							<td style="text-align:right"></td>
						</c:when>
						<c:otherwise>
							<td style="text-align:right"><fmt:formatNumber value="${lineItem.totalPrice}" pattern="${pattern}"/></td>
						</c:otherwise>
					</c:choose>
				</tr>
				<c:if test="${ gSiteConfig['gDOWNLOADABLE_PRODUCTS'] && lineItem.downloadableFileName != null}">
					<tr>
						<td align="left" colspan="${9+cols+cols2}">Download Link Exp Date: 
							<input type="text" value="${lineItem.downloadableExpDate}" id="downloadExpDate_${lineItem.lineNumber}" name="downloadExpDate_${lineItem.lineNumber}" size="20"/>
						</td>
					</tr>
				</c:if>
				<c:if test="${gSiteConfig['gRMA']}">
					<tr>
						<td colspan="${9+cols+cols2}">
							<table>
								<tr>
									<td><a onClick="addSerialNum(${lineItem.lineNumber}, '')"><img src="../graphics/add.png" width="16" border="0"></a></td>
									<td>S/N:</td>
									<td>
										<div id="__serialNums_${lineItem.lineNumber}">
											<c:forEach items="${lineItem.serialNums}" var="serialNum">
												<input type="text" name="__serialNum_${lineItem.lineNumber}" value="<c:out value="${serialNum}"/>" size="10" maxlength="50"
												<c:if test="${serialNumsMap[fn:toLowerCase(serialNum)] == 'duplicate'}">style="background-color:#EE0000;"</c:if>>,
											</c:forEach>
										</div>
									</td>
								</tr>
							</table>
						</td>
					</tr>
				</c:if>
			</c:forEach>
			<c:forEach var="lineItem" items="${invoiceForm.order.insertedLineItems}" varStatus="status">
				<input type="hidden" name="index_new_${status.index}"  value="1" id="index_new_${status.index}">
				<c:if test="${status.first and (invoiceForm.order.workOrderNum == null or canEdit)}">
					<tr class="line-row">
						<td colspan="${9+cols+cols2}">NEW ITEMS:</td>
					</tr>
				</c:if>
				<tr>
					<c:if test="${invoiceForm.order.workOrderNum == null or canEdit}" >
						<td style="text-align:center"><input type="checkbox" name="__remove_new_${status.index}" <c:if test="${gSiteConfig['gSPECIAL_PRICING']}" >onChange="toggleSpecialPricing(this, '__special_pricing_new_${status.index}')"</c:if>></td>
					</c:if>
					<td style="text-align:center"><c:out value="${invoiceForm.order.size + status.count}"/></td>
					<c:if test="${siteConfig['INVOICE_LINEITEMS_SORTING'].value == 'rank'}" >
						<td style="text-align:center"><input type="text" name="__rank${status.index}" value="<c:out value="${lineItem.rank}"/>" size="5"></td>
					</c:if>
					<td style="text-align:center"><c:out value="${lineItem.productId}"/></td>
					<td><c:out value="${lineItem.product.sku}"/></td>
					<td>
						<input type="text" name="__name_new_${status.index}" value="<c:out value="${lineItem.product.name}"/>" maxlength="120">
						<table id="optionsTable">
							<tr class="invoice_lineitem_attributes">
								<th colspan="3"><a onClick="addOptionNew('${status.index}')"><img src="../graphics/add.png" alt="Add Option" title="Add Option" border="0">Add New Option</a></th>
							</tr>
							<c:forEach items="${lineItem.newProductAttributes}" var="productAttribute" varStatus="productAttributeStatus">
								<tr class="invoice_lineitem_attributes${productAttributeStatus.count%2}">
									<td style="text-align:right"></td>
									<td class="optionName" style="white-space: nowrap" style="text-align:right"><input type="text" id="__optionName_new_${productAttribute.id}" placeholder="name"  name="__optionName_new_${productAttribute.id}" value="<c:out value="${productAttribute.optionName}"/>" maxlength="50"> </td>
									<td class="optionValue"><input type="text" id="__optionValueString_new_${productAttribute.id}" placeholder="value"  name="__optionValueString_new_${productAttribute.id}" value="<c:out value="${productAttribute.valueString}"/>" maxlength="50"></td>
								</tr>
							</c:forEach>
						</table>
					</td>
					<c:if test="${siteConfig['INVOICE_NOTE_PER_LINEITEM'].value == 'true'}" >
						<td>
							<textarea name="__lineItem_note_new_${status.index}"></textarea>
						</td>
					</c:if>
					<td align="left"> 
						<table>
							<tr>
								<c:choose>
									<c:when test="${gSiteConfig['gINVENTORY']}" >
										<td><input style="text-align: right;" type="text"  name="__quantity_new_${status.index}" value="${lineItem.quantity}" <c:if test="${lineItem.product.id != null}">id="__quantity_${lineItem.productId}" onchange="javascript:ajaxRequest(${lineItem.productId},'${lineItem.product.sku}');" </c:if> size="5"></td>
										<c:if test="${lineItem.product.id != null}"><td style="color:#FF0000;min-width:100px" id="container${lineItem.productId}" width="100%"></td></c:if>
									</c:when>
									<c:otherwise>
										<td><input style="text-align: right;" type="text" name="__quantity_new_${status.index}" value="${lineItem.quantity}" size="5"></td>
									</c:otherwise>
								</c:choose>
								<c:if test="${siteConfig['INVENTORY_SHOW_ONHAND'].value=='true' and lineItem.product.inventory!=null and lineItem.product.inventoryAFS != null}"><td><span>(<c:out value="${lineItem.product.inventory}"/>/<c:out value="${lineItem.product.inventoryAFS}"/>)</span></td></c:if>
							</tr>
						</table>
					</td>
					<c:if test="${gSiteConfig['gINVENTORY'] && invoiceForm.order.hasLowInventoryMessage}">
						<td style="text-align:center"><input style="text-align: right;" type="text" name="__low_inventory_message_new_<c:out value='${status.index}'/>" value="${lineItem.lowInventoryMessage}" size="10"></td>
					</c:if>
					<c:if test="${invoiceForm.order.hasPacking}">
						<td style="text-align:center"><c:out value="${lineItem.product.packing}" /></td>
					</c:if>
					<c:if test="${invoiceForm.order.hasContent}">
						<td style="text-align:center">
							<input style="text-align: right;" type="text" name="__caseContent_<c:out value='${status.index}'/>" value="${lineItem.product.caseContent}" size="6"></td>
					</c:if>
					<c:if test="${invoiceForm.order.hasCustomShipping}"><td style="text-align:right;"><fmt:formatNumber value="${lineItem.customShippingCost}" pattern="${pattern}"/></td></c:if>
					<c:choose>
						<c:when test="${invoiceForm.order.hasRewardPoints && lineItem.product.productType == 'REW'}">
							<td style="text-align:right"></td>
							<td style="text-align:right"><fmt:formatNumber value="${lineItem.unitPrice}" pattern="#,##0.00"/></td>
								<input type="hidden" name="__percent_new_<c:out value="${status.index}"/>" value="false"/>
								<input type="hidden" name="__originalPrice_new_<c:out value="${status.index}"/>" value="<fmt:formatNumber value="${lineItem.originalPrice}"/>" />
								<input type="hidden" name="__rewardProduct_<c:out value="${status.index}"/>" value="${lineItem.product.productType}" />
						</c:when>
						<c:otherwise>
							<td style="min-width:300px;" style="text-align:right">
								<table>
									<tr>
										<td style="min-width:80px;"><input  style="text-align: right" type="text" name="__originalPrice_new_<c:out value="${status.index}"/>" value="<fmt:formatNumber value="${lineItem.originalPrice}" pattern="${pattern}"/>" class="form-control" /></td>
										<td style="min-width:100px;"><input  style="text-align: right" type="text" name="__discount_new_<c:out value="${status.index}"/>" value="<c:out value="${lineItem.discount}"/>" class="form-control" />
											<select style="width:55px;" name="__percent_new_<c:out value="${status.index}"/>" class="custom-select">
												<option value="true">%</option><option <c:if test="${!lineItem.percent}">selected</c:if> value="false">Fix</option>
											</select>
										</td>
										<td style="min-width:80px;"><input  style="text-align: left" type="text" name="__unitPrice_new_<c:out value="${status.index}"/>" value="<fmt:formatNumber value="${lineItem.unitPrice}" pattern="${pattern}"/>" class="form-control"/></td>
									</tr>
								</table>
							</td>
							<c:if test="${invoiceForm.order.hasRewardPoints}"> <td style="text-align:right"></td></c:if>
						</c:otherwise>
					</c:choose>
					<td>
						<input  style="text-align: right" type="text" name="__addProdcutWeight_<c:out value="${status.index}"/>" value="<fmt:formatNumber value="${lineItem.weight}" pattern="##0.00"/>" class="form-control" />
					</td>
					<c:if test="${gSiteConfig['gSPECIAL_PRICING']}" >
						<td style="text-align:center"><input type="checkbox" id="__special_pricing_new_${status.index}" name="__special_pricing_new_${status.index}" <c:if test="${lineItem.setSpecialPricing}">checked</c:if> /></td>
					</c:if>
					<c:if test="${false and siteConfig['GROSS_PROFIT_ON_ADMIN_INVOICE'].value == 'true' && lineItem.cost != null && lineItem.cost != 0.0}">
						<c:set value="${lineItem.cost + totalCost}" var="totalCost"/>
						<c:set value="${(lineItem.originalPrice * lineItem.quantity) + totalOriginalPrice}" var="totalOriginalPrice"/>
					</c:if>
					<c:if test="${gSiteConfig['gPURCHASE_ORDER'] && siteConfig['SHOW_COST'].value == 'true'  && invoiceForm.order.hasCost}" >
						<td><c:out value="${lineItem.unitCost}" /></td>
					</c:if>
					<c:if test="${false and siteConfig['GROSS_PROFIT_ON_ADMIN_INVOICE'].value == 'true'}">
						<td><c:out value="${lineItem.cost}" /></td>
						<td style="text-align: right"><input style="text-align: right" type="text" name="__unitMarginPercent_<c:out value="${status.index}"/>" value="<fmt:formatNumber value="${lineItem.marginPercent}" pattern="${pattern}"/>" class="form-control" />%</td>
					</c:if>
					<td class="taxble"><input type="checkbox" name="__taxable_new_${status.index}" <c:if test="${lineItem.product.taxable}">checked</c:if>></td>
					<c:if test="${gSiteConfig['gSPECIAL_TAX']}">
						<td style="text-align:center"><input style="text-align: right" type="text" name="__taxRate_new_${status.index}" value="${lineItem.taxRate}" class="form-control" />%</td>
					</c:if>
					<c:choose>
						<%--Reward Points --%>
						<c:when test="${invoiceForm.order.hasRewardPoints and lineItem.product.productType == 'REW'}">
							<td style="text-align:right"></td>
						</c:when>
						<c:otherwise>
							<td style="text-align:right"><fmt:formatNumber value="${lineItem.totalPrice}" pattern="${pattern}"/></td>
						</c:otherwise>
					</c:choose>
				</tr>
				
				<c:if test="${gSiteConfig['gRMA']}">
					<tr>
						<td colspan="${8+cols+cols2}">
							<table>
								<tr>
									<td><a onClick="addSerialNum(${status.index}, 'new_')"><img src="../graphics/add.png" width="16" border="0"></a></td>
									<td>S/N:</td>
									<td>
										<div id="__serialNums_new_${status.index}">
											<c:forEach items="${lineItem.serialNums}" var="serialNum">
												<input type="text" name="__serialNum_new_${status.index}" value="<c:out value="${serialNum}"/>" size="10" maxlength="50"
												<c:if test="${serialNumsMap[fn:toLowerCase(serialNum)] == 'duplicate'}">style="background-color:#EE0000;"</c:if>>,
											</c:forEach>
										</div>
									</td>
								</tr>
							</table>
						</td>
					</tr>
				</c:if>
			</c:forEach>
		</tbody>
	</table>
</div>