<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ taglib uri="http://www.springframework.org/security/tags" prefix="sec" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ taglib uri="/WEB-INF/tlds/spring.tld" prefix="spring" %>
<%@ taglib uri="/WEB-INF/tlds/custom.tld" prefix="wj" %>

<div class="row mb-3">
	<div class="col-12">
		<div class="max-w-form">
			<c:if test="${shippingRates != null}">
				<div class="row">
					<div class="col-12">
						<label class="col-form-label"><fmt:message key="shipping"/></label>
					</div>
				</div>
				<c:forEach items="${shippingRates}" var="shippingrate" varStatus="loopStatus">
					<div class="row">
						<div class="col-2 text-center">
							<div class="row">
								<c:if test="${shippingrate.carrier == 'fedex'}" >
									<c:set var="fedEx" value="true" />
									<c:choose>
										<c:when test="${fn:contains(shippingrate.code,'EXPRESS') or fn:contains(shippingrate.code,'OVERNIGHT') or fn:contains(shippingrate.code,'2_DAY')}"><img width="80%" src="../../assets/Image/Layout/FedEx_Express_Normal_2Color_Positive_20.gif" /></c:when>
										<c:when test="${fn:contains(shippingrate.code,'GROUND_HOME')}"><img width="80%" src="../../assets/Image/Layout/FedEx_HomeDelivery_Normal_2Color_Positive_20.gif" /></c:when>
										<c:when test="${fn:contains(shippingrate.code,'GROUND')}"><img width="80%" src="../../assets/Image/Layout/FedEx_Ground_Normal_2Color_Positive_20.gif" /></c:when>
										<c:when test="${fn:contains(shippingrate.code,'FREIGHT')}"><img width="80%" src="../../assets/Image/Layout/FedEx_Freight_Normal_2Color_Positive_20.gif" /></c:when>
										<c:otherwise><img width="80%" src="../../assets/Image/Layout/FedEx_MultipleServices_Normal_2Color_Positive_20.gif" /></c:otherwise>
									</c:choose>
								</c:if>
								<c:if test="${shippingrate.carrier == 'ups'}" >
									<c:set var="ups" value="true" />
									<img width="20%" src="../../assets/Image/Layout/UPS_LOGO_S.gif" />
								</c:if>
							</div>
						</div>
						<div class="col-5">
							<label class="custom-control custom-radio">
			                		<input type="radio" class="custom-control-input" onchange="updateShipping('${shippingrate.title}','${shippingrate.price}');" id="scr_${loopStatus.index}" name="shippingRate" value="${loopStatus.index}" <c:if test="${(status.value == loopStatus.index) || (shippingrate.title == 'Free')}">checked</c:if>>
								<span class="custom-control-indicator"></span>
								<span class="custom-control-description"><c:out value="${shippingrate.title}" escapeXml="false"/></span>
							</label>
						</div>
						<div class="col-3 text-right">
							<c:if test="${shippingrate.price != null}"><fmt:message key="${siteConfig['CURRENCY'].value}" /></c:if><fmt:formatNumber value="${shippingrate.price}" pattern="${pattern}"/>
						</div>
						<c:if test="${siteConfig['DELIVERY_TIME'].value == 'true' and shippingrate.carrier == 'ups'}">
							<div class="col-2 text-center">
								<c:choose>
									<c:when test="${shippingrate.code == '03' and empty shippingrate.deliveryDate}"><a onclick="window.open(this.href,'','width=740,height=550'); return false;" href="category.jhtm?cid=49">See Map</a></c:when>
									<c:when test="${shippingrate.code == '03' and !empty shippingrate.deliveryDate}">Delivery Date: <fmt:formatDate type="date" timeStyle="default" timeZone="${siteConfig['SITE_TIME_ZONE'].value}"  value="${shippingrate.deliveryDate}"/><div style="font-size:10px;color:#444444;">( Not Guaranteed )</div></c:when>
									<c:otherwise>Delivery Date: <fmt:formatDate type="date" timeStyle="default" timeZone="${siteConfig['SITE_TIME_ZONE'].value}" value="${shippingrate.deliveryDate}"/></c:otherwise>
								</c:choose>
							</div>
						</c:if>
					</div>
				</c:forEach>
			</c:if>
		</div>
	</div>
</div>