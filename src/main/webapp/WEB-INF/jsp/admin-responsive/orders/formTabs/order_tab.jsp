<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ taglib uri="http://www.springframework.org/security/tags" prefix="sec" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ taglib uri="/WEB-INF/tlds/spring.tld" prefix="spring" %>
<%@ taglib uri="/WEB-INF/tlds/custom.tld" prefix="wj" %>

<div id="order_tab" class="tab-pane active" role="tabpanel">
	<c:import url="/WEB-INF/jsp/admin-responsive/orders/formTabs/order_tab_summary.jsp" />
	<c:import url="/WEB-INF/jsp/admin-responsive/orders/formTabs/order_tab_prefields.jsp" />
	<c:import url="/WEB-INF/jsp/admin-responsive/orders/formTabs/order_tab_invoice_tbl.jsp" />
	<c:import url="/WEB-INF/jsp/admin-responsive/orders/formTabs/order_tab_total.jsp" />	 						
	<c:import url="/WEB-INF/jsp/admin-responsive/orders/formTabs/order_tab_shipping_rates.jsp" />
	<c:import url="/WEB-INF/jsp/admin-responsive/orders/formTabs/order_tab_postfields.jsp" />				
	<div class="row">
		<div class="col-12">
			<div class="float-right">
				<input id="cancel" class="btn btn-default btn-flat" type="submit" value="<fmt:message key="cancel"/>" name="_cancel">
				<c:choose>
					<c:when test="${siteConfig['PAYMENT_ALERT_ORDER_RESTRICTION'].value == 'true' and invoiceForm.order.paymentAlert}">
						<sec:authorize ifAnyGranted="ROLE_AEM,ROLE_ADMIN_${prop_site_id},ROLE_OVERRIDE_ORDER_RESTRICTION_PAYMENT_ALERT">
							<c:if test="${invoiceForm.newInvoice and (gSiteConfig['gADD_INVOICE'] or gSiteConfig['gSERVICE'])}">
								<input id="addOrder" type="submit" name="__save" value="<fmt:message key="addOrder"/>" class="btn btn-primary"/>
							</c:if>
						</sec:authorize>
					</c:when>
					<c:otherwise>
						<c:if test="${invoiceForm.newInvoice and (gSiteConfig['gADD_INVOICE'] or gSiteConfig['gSERVICE'])}">
							<input id="addOrder" type="submit" name="__save" value="<fmt:message key="addOrder"/>" class="btn btn-primary"/>
						</c:if>
					</c:otherwise>
				</c:choose>
				<sec:authorize ifAnyGranted="ROLE_AEM,ROLE_ADMIN_${prop_site_id},ROLE_INVOICE_EDIT">
					<c:if test="${!invoiceForm.newInvoice}">
						<input id="saveChanges" type="submit" name="__save" value="<fmt:message key="saveChanges"/>" class="btn btn-primary">
					</c:if>
				</sec:authorize>
				<input id="recalculate" class="btn btn-primary btn-outline" type="submit" name="__update" value="<fmt:message key="recalculate"/>">
			</div>
		</div>
	</div>		
 </div>