<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ taglib uri="http://www.springframework.org/security/tags" prefix="sec" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ taglib uri="/WEB-INF/tlds/spring.tld" prefix="spring" %>
<%@ taglib uri="/WEB-INF/tlds/custom.tld" prefix="wj" %>

<div class="row mt-5">
	<div class="col-md-6 col-lg-4">
		<div class="max-w-300">
			<div class="form-row">
				<div class="col-12">
					<label class="col-form-label"><fmt:message key="purchaseOrder" /></label>
					<div class="form-group">
						<form:input path="order.purchaseOrder" maxlength="50" class="form-control"/>
					</div>
				</div>
			</div>
			<c:if test="${siteConfig['WORLD_SHIP_IMPORT_ORDERS'].value == true }" >
				<div class="form-row">
					<div class="col-12">
						<label class="col-form-label"><fmt:message key="purchaseOrder2" /></label>
						<div class="form-group">
							<input type="text" id="customerPO2" name="customerPO2" value="${upsWorldShip.customerPO2}" class="form-control">
						</div>
					</div>
				</div>
				<div class="form-row">
					<div class="col-12">
						<label class="col-form-label"><fmt:message key="carrierPhone" /></label>
						<div class="form-group">
							<input type="text" id="carrierPhone" name="carrierPhone" value="${upsWorldShip.carrierPhone}" class="form-control">
						</div>
					</div>
				</div>
			</c:if>
		</div>
	</div>
</div>