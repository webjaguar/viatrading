<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ taglib uri="http://www.springframework.org/security/tags" prefix="sec" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ taglib uri="/WEB-INF/tlds/spring.tld" prefix="spring" %>
<%@ taglib uri="/WEB-INF/tlds/custom.tld" prefix="wj" %>

<tiles:insertDefinition name="admin.responsive-template.order" flush="true">
	<tiles:putAttribute name="css">
		<style>
			#grid .field-bill_to{
			    width: 150px;
			}
			#grid .field-bill_to_company{
			    width: 150px;
			}
			#grid .field-order_id{
			    width: 110px;
			}
			#grid .field-sub_total{
			    text-align:right;
			}
			#grid .field-grand_total{
			    text-align:right;
			}
			#grid .field-status{
			    width: 80px;
			}
		</style>
	</tiles:putAttribute>
	<tiles:putAttribute name="content" type="string">
		<sec:authorize ifAnyGranted="ROLE_AEM,ROLE_ADMIN_${prop_site_id},ROLE_INVOICE_VIEW_LIST,ROLE_INVOICE_EDIT,ROLE_INVOICE_STATUS">
			<form action="ordersList.jhtm?orderName=${orderName}" id="list" name="list_form" method="post">
				<div class="page-banner">
					<div class="breadcrumb"><span><a href="${_contextpath}/admin/catalog/dashboard.jhtm">Home</a></span><i class="sli-arrow-right"></i><span><a href="${_contextpath}/admin/orders/">Orders</a></span><i class="sli-arrow-right"></i><span style="text-transform: capitalize">${model.orderName}</span></div>
				</div>
				<div class="page-head">
					<div class="row justify-content-between">
						<div class="col-sm-8 title">
							<h3 style="text-transform:capitalize">${model.orderName}</h3>
						</div>
						<div class="col-sm-4 actions">
		                    <div class="dropdown dropdown-setting">
								<button type="button" class="btn-outline dropdown-toggle active btn btn-primary" data-toggle="dropdown">
		                             <i class="sli-screen-desktop"></i> Display
		                         </button>	                         
								<div class="dropdown-menu dropdown-menu-right">
									<div class="form-group">
				                    		<div class="option-header">Rows</div>
										<select class="custom-select form-control" name="size" onchange="document.getElementById('page').value=1;submit();">
											<c:forTokens items="10,25,50,100,200,500" delims="," var="current">
									  	  		<option value="${current}" <c:if test="${current == orderSearch.pageSize}">selected</c:if>>${current}</option>
								  			</c:forTokens>
										</select>
									</div>
									<div class="form-group">
				                    		<div class="option-header">Columns</div>
				                    		<div id="column-hider"></div>
									</div>
								</div>
		                    </div>
						</div>
					</div>
				</div>
				<div class="page-body">
					<div class="grid-stage">
						<div class="page-stat">
							<fmt:message key="showing">
								<fmt:param value="${orderSearch.offset + 1}"/>
								<fmt:param value="${model.pageEnd}"/>
								<fmt:param value="${model.count}"/>
							</fmt:message> 
						</div>
						<table id="grid"></table>
						<div class="footer">
			 				<div class="float-right">
								<c:if test="${orderSearch.page == 1}">
							 		<span class="previous"><i class="sli-arrow-left" style="font-size: 0.7rem"></i> Previous</span>
								</c:if>
								<c:if test="${orderSearch.page != 1}">
									<a href="#" onClick="document.getElementById('page').value='${orderSearch.page-1}';document.getElementById('list').submit()">
										<span class="previous"><i class="sli-arrow-left" style="font-size: 0.7rem"></i> Previous</span>
									</a>
								</c:if>	
								<span class="status"><c:out value="${orderSearch.page}" /> of <c:out value="${model.pageCount}" /></span>
								<c:if test="${orderSearch.page == model.pageCount}">
 									<span class="next">Next <i class="sli-arrow-right" style="font-size: 0.7rem"></i></span>
								</c:if>
								<c:if test="${orderSearch.page != model.pageCount}">
									<a href="#" onClick="document.getElementById('page').value='${orderSearch.page+1}';document.getElementById('list').submit()">
										<span class="next">Next <i class="sli-arrow-right" style="font-size: 0.7rem"></i></span>
									</a>
								</c:if>
							</div>
				        </div>
		 			</div>
				</div>
				<input type="hidden" id="page" name="page" value="${orderSearch.page}"/>
				<input type="hidden" id="sort" name="sort" value="${orderSearch.sort}" />
			</form>
		</sec:authorize>
	</tiles:putAttribute>
	<tiles:putAttribute name="js">
		<!-- DGRID TABLE SCRIPT MUST BE AT BOTTOM OF THE PAGE -->
		<script src="${_contextpath}/admin-responsive-static/assets/plugins/dojo/dojo.js" data-dojo-config="async: true"></script>
		<script>
			//Table Data Sanitization
			var data = [];
			<c:forEach items="${model.orders}" var="order" varStatus="status">
				data.push({
					id: '${order.orderId}',
					offset: '${status.count + orderSearch.offset}.',
					<c:if test="${fn:contains(siteConfig['CUSTOMIZE_ORDER_LIST'].value,'billedTo')}">
			    			userId: '${order.userId}',
			    			bill_to_first_name: '${wj:escapeJS(order.billing.firstName)}',
			    			bill_to_last_name: '${wj:escapeJS(order.billing.lastName)}',
		    			</c:if>
					<c:if test="${fn:contains(siteConfig['CUSTOMIZE_ORDER_LIST'].value,'company')}">
						bill_to_company: '${wj:escapeJS(order.billing.company)}',
					</c:if>
					<c:if test="${fn:contains(siteConfig['CUSTOMIZE_ORDER_LIST'].value,'dateOrdered')}">
						date_ordered: '<fmt:formatDate type="date" timeStyle="default" pattern="${siteConfig[\'SITE_DATE_FORMAT\'].value}" timeZone="${siteConfig[\'SITE_TIME_ZONE\'].value}" value="${order.dateOrdered}"/>',
					</c:if>
					<c:if test="${fn:contains(siteConfig['CUSTOMIZE_ORDER_LIST'].value,'subTotal')}">
		        			sub_total: '<fmt:formatNumber value="${order.subTotal}" pattern="#,##0.00" />',
					</c:if>
					<c:if test="${fn:contains(siteConfig['CUSTOMIZE_ORDER_LIST'].value,'grandTotal')}">
		        			grand_total: '<fmt:formatNumber value="${order.grandTotal}" pattern="#,##0.00" />',
		        		</c:if>
					<c:if test="${fn:contains(siteConfig['CUSTOMIZE_ORDER_LIST'].value,'status')}">
						status: '<fmt:message key="orderStatus_${order.status}" />',
					</c:if>
					<c:if test="${fn:contains(siteConfig['CUSTOMIZE_ORDER_LIST'].value,'dateShipped')}">
						<c:choose>
       						<c:when test="${orderSearch.dateType == 'orderDate'}">
       							dateShipped: null,
	        					</c:when>
				       		<c:otherwise>
				        			dateShipped: '<fmt:formatDate type="date" timeStyle="default" timeZone="${siteConfig[\'SITE_TIME_ZONE\'].value}" value="${order.shipped}"/>',
				       		</c:otherwise>
						</c:choose>
					</c:if>
					<c:if test="${fn:contains(siteConfig['CUSTOMIZE_ORDER_LIST'].value,'trackCode')}">
						trackcode: "${order.trackcode}",
					</c:if>
					<c:if test="${gSiteConfig['gSALES_REP'] and fn:contains(siteConfig['CUSTOMIZE_ORDER_LIST'].value,'salesRep')}">
						salesRepName: "${wj:escapeJS(model.salesRepMap[order.salesRepId].name)}",
				    </c:if>
					<c:if test="${gSiteConfig['gMULTI_STORE'] > 0 and fn:contains(siteConfig['CUSTOMIZE_ORDER_LIST'].value,'multiStore')}">
						host: "${wj:escapeJS(order.host)}",
					</c:if>
				});
				<c:set var="totalSubTotal" value="${order.subTotal + totalSubTotal}" />
				<c:set var="totalGrandTotal" value="${order.grandTotal + totalGrandTotal}" />
			</c:forEach>

			//Columns Definitions
			require(['dojo/_base/lang',
					'dojo/_base/declare',
					'dstore/Memory',
					'dstore/Trackable',
					'dgrid/OnDemandGrid',
					'dgrid/Editor',
					'dgrid/extensions/ColumnHider',
					'dgrid/extensions/ColumnReorder',
					'dgrid/extensions/ColumnResizer',
					'dgrid/Selector',
					'dijit/form/DropDownButton',
					'dijit/DropDownMenu',
					'dijit/MenuItem',
					'summary/SummaryRow'
				], function (lang, declare, Memory, Trackable, OnDemandGrid, Editor, ColumnHider, ColumnReorder, ColumnResizer, Selector, DropDownButton, DropDownMenu, MenuItem, SummaryRow) {
				var columns = {
					batch: {
						selector: 'checkbox',
						unhidable: true
					},
					offset: {
						label: '#',
						get: function(object){
							return object.offset;
						},
						sortable: false,
						unhidable: true
					},
					<c:if test="${fn:contains(siteConfig['CUSTOMIZE_ORDER_LIST'].value,'billedTo')}">
						bill_to: {
							label: '<fmt:message key="billTo" />',
							renderCell: function(object, data, td, options){
								var link = document.createElement("a");
								link.text = object.bill_to_first_name+' '+object.bill_to_last_name;
								link.href = "../customers/customer.jhtm?id="+object.userId;
								return link;
							},
							sortable: false
						},
					</c:if>
					<c:if test="${fn:contains(siteConfig['CUSTOMIZE_ORDER_LIST'].value,'company')}">
						bill_to_company:{
		    	        			label: '<fmt:message key="company" />',
							get: function(object){
								return object.bill_to_company;
							}
		    	        		},
    					</c:if>
    					order_id:{
	    	        			label: '<fmt:message key="${orderName}Number"/>',
						renderCell: function(object){
							var link = document.createElement("a");
							link.text = object.id;
							link.href = 'invoice.jhtm?order='+object.id+'&orderName=${orderName}';
							return link;
						}
	    	        		},
	    	        		<c:if test="${fn:contains(siteConfig['CUSTOMIZE_ORDER_LIST'].value,'dateOrdered')}">
	    	        			date_ordered:{
		    	        			label: '<fmt:message key="orderDate" />',
							get: function(object){
								return object.date_ordered;
							}
		    	        		},
	    				</c:if>
	    	        		<c:if test="${fn:contains(siteConfig['CUSTOMIZE_ORDER_LIST'].value,'subTotal')}">
	    	        			sub_total:{
		    	        			label: '<fmt:message key="subTotal" />',
							get: function(object){
								return object.sub_total;
							}
		    	        		},
	    				</c:if>
	    	        		<c:if test="${fn:contains(siteConfig['CUSTOMIZE_ORDER_LIST'].value,'grandTotal')}">
	    	        			grand_total:{
		    	        			label: '<fmt:message key="grandTotal" />',
							get: function(object){
								return object.grand_total;
							}
		    	        		},
	    	        		</c:if>
	    	        		<c:if test="${fn:contains(siteConfig['CUSTOMIZE_ORDER_LIST'].value,'status')}">
	    	        			status:{
		    	        			label: '<fmt:message key="status" />',
		    	        			get: function(object){
								return object.status;
							},
		    	        		},
	    				</c:if>
	    	        		<c:if test="${fn:contains(siteConfig['CUSTOMIZE_ORDER_LIST'].value,'dateShipped')}">
						dateShipped:{
		    	        			label: '<fmt:message key="dateShipped" />',
		    	        			get: function(object){
								return object.dateShipped;
							},
							sortable: false
		    	        		},
					</c:if>
					<c:if test="${fn:contains(siteConfig['CUSTOMIZE_ORDER_LIST'].value,'trackCode')}">
						trackcode:{
		    	        			label: '<fmt:message key="trackcode" />',
		    	        			get: function(object){
								return object.trackcode;
							},
							sortable: false
		    	        		},
					</c:if>
	    	        		<c:if test="${gSiteConfig['gSALES_REP'] and fn:contains(siteConfig['CUSTOMIZE_ORDER_LIST'].value,'salesRep')}">
		    	        		trackcode:{
		    	        			label: '<fmt:message key="salesRep" />',
		    	        			get: function(object){
								return object.salesRepName;
							},
							sortable: false
		    	        		},
				    </c:if>
					<c:if test="${gSiteConfig['gMULTI_STORE'] > 0 and fn:contains(siteConfig['CUSTOMIZE_ORDER_LIST'].value,'multiStore')}">
						trackcode:{
		    	        			label: '<fmt:message key="multiStore" />',
		    	        			get: function(object){
								return object.host;
							},
							sortable: false
		    	        		},
					</c:if>
				};
				var store = new (declare([Memory, Trackable]))({
					data: data,
					idProperty: "id"
				});
				
				var grid = new (declare([OnDemandGrid, Editor, ColumnHider, ColumnReorder, ColumnResizer, Selector, SummaryRow]))({
					collection: store,
					selectionMode: "none",
					allowSelectAll: true,
					columns: columns
				},'grid');
				
				var totals = {order_id: 'Total'};
				
				<c:if test="${fn:contains(siteConfig['CUSTOMIZE_ORDER_LIST'].value,'subTotal')}">
					totals['sub_total'] = '<fmt:formatNumber value="${totalSubTotal}" pattern="#,##0.00" />';
				</c:if>
				
				<c:if test="${fn:contains(siteConfig['CUSTOMIZE_ORDER_LIST'].value,'grandTotal')}">
					totals['grand_total'] = '<fmt:formatNumber value="${totalGrandTotal}" pattern="#,##0.00" />';
				</c:if>
				
				grid.set('summary', totals);
				
				grid.startup();
				//sorting columns
				grid.on("dgrid-sort", lang.hitch(this, function(e){
					e.preventDefault();						
					var property = e.sort[0].property;
				    var order = e.sort[0].descending ? "desc" : "asc";
				    document.getElementById('sort').value = property + ' ' + order;
				    document.getElementById('list').submit();
				}));
				
				<c:if test="${orderSearch.sort == 'bill_to_company desc'}">
					grid.updateSortArrow([{property: 'bill_to_company', descending: true}],true);
				</c:if>
				<c:if test="${orderSearch.sort == 'bill_to_company asc'}">
					grid.updateSortArrow([{property: 'bill_to_company', ascending: true}],true);
				</c:if>
				
				<c:if test="${orderSearch.sort == 'order_id desc'}">
					grid.updateSortArrow([{property: 'order_id', descending: true}],true);
				</c:if>
				<c:if test="${orderSearch.sort == 'order_id asc'}">
					grid.updateSortArrow([{property: 'order_id', ascending: true}],true);
				</c:if>
				
				<c:if test="${orderSearch.sort == 'date_ordered desc'}">
					grid.updateSortArrow([{property: 'date_ordered', descending: true}],true);
				</c:if>
				<c:if test="${orderSearch.sort == 'date_ordered asc'}">
					grid.updateSortArrow([{property: 'date_ordered', ascending: true}],true);
				</c:if>
				
				<c:if test="${orderSearch.sort == 'sub_total desc'}">
					grid.updateSortArrow([{property: 'sub_total', descending: true}],true);
				</c:if>
				<c:if test="${orderSearch.sort == 'sub_total asc'}">
					grid.updateSortArrow([{property: 'sub_total', ascending: true}],true);
				</c:if>
				
				<c:if test="${orderSearch.sort == 'grand_total desc'}">
					grid.updateSortArrow([{property: 'grand_total', descending: true}],true);
				</c:if>
				<c:if test="${orderSearch.sort == 'grand_total asc'}">
					grid.updateSortArrow([{property: 'grand_total', ascending: true}],true);
				</c:if>
				
				<c:if test="${orderSearch.sort == 'status desc'}">
					grid.updateSortArrow([{property: 'status', descending: true}],true);
				</c:if>
				<c:if test="${orderSearch.sort == 'status asc'}">
					grid.updateSortArrow([{property: 'status', ascending: true}],true);
				</c:if>
				
				grid.on('dgrid-refresh-complete', function(event) {
					$('#grid-hider-menu').appendTo('#column-hider');
				});
				
				grid.startup();
			});
		</script>
	</tiles:putAttribute>
</tiles:insertDefinition>