<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ taglib uri="http://www.springframework.org/security/tags" prefix="sec" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ taglib uri="/WEB-INF/tlds/custom.tld" prefix="wj" %>

<!-- Approval History Tab. -->
<div id="tab_3" class="tab-pane">
	<table class="responsiveBootstrapFooTable table table-hover" data-sorting="true">
		<thead>
			<tr class=""> 
				<th data-type="html"  data-breakpoints="xs sm">Action By</th>
		  		<th data-type="html"  data-breakpoints="xs sm">Type</th>
		  		<th data-type="html"  data-breakpoints="xs sm">Date</th>
		  		<th data-type="html"  data-breakpoints="xs sm">Note</th> 
			</tr> 
		</thead>
		<tbody>
			<c:forEach var="order" items="${order.actionHistory}">
				<tr class="">
					<td class="" style="white-space: nowrap"><c:out value="${order.actionByName}"/></td>
					<td class="" style="white-space: nowrap"><c:out value="${order.actionType}"/></td>
					<td class="r" align="center" style="white-space: nowrap"><fmt:formatDate type="both" timeZone="${siteConfig['SITE_TIME_ZONE'].value}" value="${order.actionDate}"/></td>
					<td class="" align="center"><c:out value="${order.actionNote}"/></td>
				</tr>
			</c:forEach>
		</tbody>
	</table>
</div>