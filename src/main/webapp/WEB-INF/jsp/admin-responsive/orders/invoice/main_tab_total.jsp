<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ taglib uri="http://www.springframework.org/security/tags" prefix="sec" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ taglib uri="/WEB-INF/tlds/spring.tld" prefix="spring" %>
<%@ taglib uri="/WEB-INF/tlds/custom.tld" prefix="wj" %>

<div class="row">	
	<div class="col-12">
		<table class="total">
			<tbody>
				<tr>
					<td colspan="2"></td>
				</tr>
				<tr>
					<td>
						<fmt:message key="subTotal" />:
					</td>
					<td>
						<fmt:message key="${siteConfig['CURRENCY'].value}" /><fmt:formatNumber value="${order.subTotal}" pattern="#,##0.00000#" maxFractionDigits="${siteConfig['MAX_FRACTION_DIGIT'].value}"/>
					</td>
				</tr>
				<c:if test="${siteConfig['GROSS_PROFIT_ON_ADMIN_INVOICE'].value == 'true' && totalCost != 0.0 && totalCost != null}"> 
					<tr>
						<td>
							Total GP:
						</td>
						<td>
							<fmt:formatNumber value="${((totalOriginalPrice - totalCost) / totalOriginalPrice ) * 100 }" pattern="${pattern}"/> %
						</td>
					</tr>
				</c:if>
				<c:if test="${order.rushCharge != null}">
					<tr>
						<td><fmt:message key="rushCharge" />:</td>
						<td><fmt:formatNumber value="${order.rushCharge}" pattern="#,##0.00"/></td>
					</tr>
				</c:if>
				<c:if test="${order.baggingCharge != null}">
					<tr>
						<td><fmt:message key="baggingCharge" />:</td>
						<td><fmt:formatNumber value="${order.baggingCharge}" pattern="#,##0.00"/></td>
					</tr>
				</c:if>
				<c:if test="${order.lessThanMinCharge != null}">
					<tr>
						<td><fmt:message key="lessThanMinCharge" />:</td>
						<td><fmt:formatNumber value="${order.lessThanMinCharge}" pattern="#,##0.00"/></td>
					</tr>
				</c:if> 
				<c:if test="${order.budgetEarnedCredits > 0.00 }">
					<tr>
						<td><fmt:message key="f_earnedCredits" />: </td>
						<td><fmt:formatNumber value="${order.budgetEarnedCredits}" pattern="-#,##0.00"/></td>
					</tr>
				</c:if>
				<c:if test="${order.promo.title != null and order.promo.discountType eq 'order'}" >
					<tr>
						<td>
							<c:choose>
								<c:when test="${order.promo.discount == 0}"><fmt:message key="promoCode" /></c:when>
								<c:otherwise><fmt:message key="discountForPromoCode" /></c:otherwise>
							</c:choose>
							<c:out value="${order.promo.title}" />
							<c:choose>
								<c:when test="${order.promo.discount == 0}"></c:when>
								<c:when test="${order.promo.percent}">
									(<fmt:formatNumber value="${order.promo.discount}" pattern="#,##0.00"/>%)
								</c:when>
								<c:otherwise>
									(<fmt:message key="${siteConfig['CURRENCY'].value}" /><fmt:formatNumber value="${order.promo.discount}" pattern="#,##0.00"/>)
								</c:otherwise>
							</c:choose>
						</td>
						<td>
							<c:choose>
								<c:when test="${order.promo.discount == 0}">&nbsp;</c:when>
								<c:when test="${order.promo.percent}">
									(<fmt:formatNumber value="${order.subTotal * ( order.promo.discount / 100.00 )}" pattern="#,##0.00"/>)
								</c:when>
								<c:when test="${(!order.promo.percent) and (order.promo.discount > order.subTotal)}">
									(<fmt:message key="${siteConfig['CURRENCY'].value}" /><fmt:formatNumber value="${order.subTotal}" pattern="#,##0.00"/>)
								</c:when>
								<c:otherwise>
									(<fmt:message key="${siteConfig['CURRENCY'].value}" /><fmt:formatNumber value="${order.promo.discount}" pattern="#,##0.00"/>)
								</c:otherwise>
							</c:choose>
						</td>
					</tr>
				</c:if>
				<c:if test="${order.lineItemPromos != null and fn:length(order.lineItemPromos) gt 0}">
					<c:forEach items="${order.lineItemPromos}" var="itemPromo" varStatus="status">
						<tr>
				    			<td>
				     			<c:choose>
				       			<c:when test="${itemPromo.value == 0}"><fmt:message key="promoCode" /></c:when>
				       			<c:otherwise><fmt:message key="discountForPromoCode" /></c:otherwise>
				     			</c:choose>
				     			<c:out value="${itemPromo.key}" />
				    			</td>
				    			<td>
				     			<c:choose>
				       			<c:when test="${itemPromo.value == 0}">&nbsp;</c:when>
				      			 <c:otherwise>(<fmt:message key="${siteConfig['CURRENCY'].value}" /><fmt:formatNumber value="${itemPromo.value}" pattern="#,##0.00"/>)</c:otherwise>
				     			</c:choose>
				    			</td>
			 			</tr>
					</c:forEach>  
				</c:if>
				<c:choose>
					<c:when test="${order.taxOnShipping}">
						<tr>
							<td><fmt:message key="shippingHandling" /> (<c:out value="${order.shippingMethod}" escapeXml="false"/>):</td>
							<td>
								<c:if test="${order.shippingCost != null}">
									<fmt:message key="${siteConfig['CURRENCY'].value}" /><fmt:formatNumber value="${order.shippingCost}" pattern="#,##0.00"/>
								</c:if>
							</td>
						</tr>
						<c:if test="${gSiteConfig['gSITE_DOMAIN'] != 'yayme.com.au'}" >
							<tr>
								<td><fmt:message key="tax" />:</td>
								<td><fmt:message key="${siteConfig['CURRENCY'].value}" /><fmt:formatNumber value="${order.tax}" pattern="#,##0.00000#" maxFractionDigits="${siteConfig['MAX_FRACTION_DIGIT'].value}"/></td>
							</tr>
						</c:if>
					</c:when>
					<c:otherwise>
			  			<c:if test="${gSiteConfig['gSITE_DOMAIN'] != 'yayme.com.au'}" >
			  				<tr>
								<td><fmt:message key="tax" />:</td>
		  						<td><fmt:message key="${siteConfig['CURRENCY'].value}" /><fmt:formatNumber value="${order.tax}" pattern="#,##0.00000#" maxFractionDigits="${siteConfig['MAX_FRACTION_DIGIT'].value}"/></td>
		  					</tr>
						</c:if>
						<tr>
							<td><fmt:message key="shippingHandling" /> (<c:out value="${order.shippingMethod}" escapeXml="false"/>):</td>
							<td>
		    						<c:if test="${order.shippingCost != null}">
									<fmt:message key="${siteConfig['CURRENCY'].value}" /><fmt:formatNumber value="${order.shippingCost}" pattern="#,##0.00000#" maxFractionDigits="${siteConfig['MAX_FRACTION_DIGIT'].value}"/>
								</c:if>
		    					</td>
						</tr>
					</c:otherwise>
				</c:choose>
				<c:if test="${order.promo.title != null and order.promo.discountType eq 'shipping' and order.shippingCost != null}" >
					<tr>
						<td>
							<c:choose>
								<c:when test="${order.promo.discount == 0}"><fmt:message key="promoCode" /></c:when>
								<c:otherwise><fmt:message key="discountForPromoCode" /></c:otherwise>
							</c:choose>
							<c:out value="${order.promo.title}" />
							<c:choose>
								<c:when test="${order.promo.discount == 0}"></c:when>
								<c:when test="${order.promo.percent}">
									(<fmt:formatNumber value="${order.promo.discount}" pattern="#,##0.00"/>%)
								</c:when>
								<c:otherwise>
									(<fmt:message key="${siteConfig['CURRENCY'].value}" /><fmt:formatNumber value="${order.promo.discount}" pattern="#,##0.00"/>)
								</c:otherwise>
							</c:choose>
						</td>
						<td>
							<c:choose>
								<c:when test="${order.promo.discount == 0}">&nbsp;</c:when>
								<c:when test="${order.promo.percent}">
									(<fmt:formatNumber value="${order.shippingCost * ( order.promo.discount / 100.00 )}" pattern="#,##0.00"/>)
								</c:when>
								<c:when test="${(!order.promo.percent) and (order.promo.discount > order.shippingCost)}">
									(<fmt:message key="${siteConfig['CURRENCY'].value}" /><fmt:formatNumber value="${order.shippingCost}" pattern="#,##0.00"/>)
								</c:when>
								<c:otherwise>
									(<fmt:message key="${siteConfig['CURRENCY'].value}" /><fmt:formatNumber value="${order.promo.discount}" pattern="#,##0.00"/>)
								</c:otherwise>
							</c:choose>
						</td>
					</tr>
				</c:if>
				<c:if test="${order.customShippingCost != null}">
					<tr>
						<td><fmt:message key="customShipping" /> (<c:out value="${order.customShippingTitle}" escapeXml="false"/>):</td>
						<td><fmt:message key="${siteConfig['CURRENCY'].value}" /><fmt:formatNumber value="${order.customShippingCost}" pattern="#,##0.00"/></td>
					</tr>
				</c:if>
				<c:if test="${order.ccFee != null && order.ccFee != 0}">
					<tr>
						<td><fmt:message key="creditCardFee" />:</td>
						<td><fmt:message key="${siteConfig['CURRENCY'].value}" /><fmt:formatNumber value="${order.ccFee}" pattern="#,##0.00"/></td>
					</tr>
				</c:if>
				<c:choose>
					<c:when test="${gSiteConfig['gBUDGET'] and order.requestForCredit != null and order.creditUsed == null and customer.creditAllowed != null and customer.creditAllowed > 0}">
						<tr>
							<td>
								<fmt:message key="creditRequestAmount">
									<fmt:param>${customer.creditAllowed}</fmt:param>
									<fmt:param><fmt:formatNumber value="${subTotalByPlan}" pattern="#,##0.00"/></fmt:param>
									<fmt:param><fmt:formatNumber value="${(customer.creditAllowed / 100) * subTotalByPlan}" pattern="#,##0.00"/></fmt:param>
					    			</fmt:message> 	
				    				<br/>
								<fmt:message key="creditApprovalMessage" />
							</td>
							<c:choose>
								<c:when test="${order.requestForCredit > 0 }">
									<td><fmt:message key="${siteConfig['CURRENCY'].value}" /><fmt:formatNumber value="${order.requestForCredit}" pattern="-#,##0.00"/></td>
								</c:when>
								<c:otherwise>
									<td>0.00</td>
								</c:otherwise>
							</c:choose>	 
						</tr>
					</c:when>
					<c:when test="${(gSiteConfig['gGIFTCARD'] or gSiteConfig['gVIRTUAL_BANK_ACCOUNT'] or gSiteConfig['gBUDGET']) and order.creditUsed != null and order.creditUsed > 0}">
						<tr>
							<td><fmt:message key="credit" />:</td>
							<td><fmt:message key="${siteConfig['CURRENCY'].value}" /><fmt:formatNumber value="${order.creditUsed}" pattern="-#,##0.00"/></td>
						</tr>
					</c:when>
				</c:choose>
				<c:if test="${order.bondCost != null}">
					<tr>
						<td>
							<c:if test="${!fn:contains(order.status,'x')}">
								<a style="width: 100px; float: right; text-align: center; background-color:#FF9999; color:WHITE;" href="invoice.jhtm?order=${order.orderId}&__cancelBuySafe=true"><fmt:message key="cancel" /></a>
							</c:if>
						</td>
						<td><fmt:message key="buySafeBondGuarntee" />:</td>
						<td><fmt:message key="${siteConfig['CURRENCY'].value}" /><fmt:formatNumber value="${order.bondCost}" pattern="#,##0.00"/></td>
					</tr>
				</c:if>
				<tr>
					<td><fmt:message key="grandTotal" />:</td>
					<td><fmt:message key="${siteConfig['CURRENCY'].value}" /><fmt:formatNumber value="${order.grandTotal}" pattern="#,##0.00000#" maxFractionDigits="${siteConfig['MAX_FRACTION_DIGIT'].value}" /></td>
				</tr>
				<tr>
					<td><fmt:message key="totalQuantity" />:</td>
					<td><fmt:formatNumber value="${order.totalProductQuantity}" pattern="#,##0"/></td>
				</tr>
				<tr>
					<td><fmt:message key="totalWeight" />:</td>
					<td><fmt:formatNumber value="${order.totalProductWeight}" pattern="#,##0"/></td>
				</tr>
				<c:if test="${gSiteConfig['gPAYMENTS']}">
					<tr>
						<td><fmt:message key="balance" />:</td>
						<td><fmt:message key="${siteConfig['CURRENCY'].value}" /><fmt:formatNumber value="${order.grandTotal - order.amountPaid}" pattern="#,##0.00000#" maxFractionDigits="${siteConfig['MAX_FRACTION_DIGIT'].value}" /></td>
					</tr>
				</c:if>
				<c:if test="${gSiteConfig['gBUDGET'] && gSiteConfig['gWILDMAN_GROUP'] && order.budgetFrqType != null && order.budgetFrqType != ''}">
					<tr>
						<td><c:out value="Remaining ${budgetLabel} Balance"/>:</td>
						<td><fmt:message key="${siteConfig['CURRENCY'].value}" /><fmt:formatNumber value="${order.budgetFrqBalance}" pattern="#,##0.00"/></td>
					</tr>
				</c:if>
				<c:if test="${order.hasRewardPoints}">
					<tr>
						<td><c:out value="${siteConfig['GROUP_REWARD_POINTS_TITLE'].value}"/>:</td>
						<td><fmt:formatNumber value="${order.rewardPointsTotal}" pattern="#,##0.00"/></td>
					</tr>
				</c:if>
				<c:if test="${gSiteConfig['gSITE_DOMAIN'] == 'yayme.com.au'}">
					<tr>
						<td><fmt:message key="gst" /> :</td>
					 	<td><fmt:formatNumber value="${order.grandTotal/11}" pattern="#,##0.00"/></td>
					</tr>
				</c:if>
			</tbody>
		</table>
	</div>
</div>
<div class="row mt-1">
	<div class="col-12">
		<div>
			<b><fmt:message key="description"/>: </b>
			<c:out value="${order.description}" escapeXml="false"/>
		</div>
		<c:if test="${siteConfig['CINRAM_EXPORT_ORDER'].value == 'true'}"> 
			<div>
				<b><fmt:message key="okToShipPartial" />: </b>
				<c:choose> 
					<c:when test="${order.showPartiallyShipOk == true}" >
						Yes
					</c:when>
					<c:otherwise>
						No
					</c:otherwise>
				</c:choose>
			</div>
		</c:if>	
		<c:if test="${partnerList != null and fn:length(partnerList) gt 0}">
			<div>
				<b><fmt:message key="partnersList"/>: </b>
				<ul>
		 			<c:forEach items="${partnerList}" var="partner">
	 					<li>
		 					<c:out value="${partner.partnerName}"/>:
		 					<fmt:message key="${siteConfig['CURRENCY'].value}" /><fmt:formatNumber value="${partner.amount}" pattern="#,##0.00"/>
	 					</li>
		 			</c:forEach>
	 			</ul>
			</div>
			<div>
				<b><fmt:message key="total"/>:</b>
	 			<fmt:message key="${siteConfig['CURRENCY'].value}" /><fmt:formatNumber value="${order.budgetEarnedCredits}" pattern="#,##0.00"/>
			</div>
		</c:if>
		<div>
			<c:out value="${siteConfig['INVOICE_MESSAGE'].value}" escapeXml="false"/>
		</div>
		<div>
			<b><fmt:message key="paymentMethod" />:</b>
			<c:out value="${order.paymentMethod}" />
		</div>
	</div>
</div>