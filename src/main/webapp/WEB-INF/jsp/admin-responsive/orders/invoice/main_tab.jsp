<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ taglib uri="http://www.springframework.org/security/tags" prefix="sec" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ taglib uri="/WEB-INF/tlds/custom.tld" prefix="wj" %>

<div id="tab_1" class="tab-pane active" role="tabpanel">
	<c:import url="/WEB-INF/jsp/admin-responsive/orders/invoice/main_tab_summary.jsp" />        
	<div class="mt-5">
		<strong><fmt:message key="emailAddress" />:</strong>
		<a href="mailto:${customer.username}"><c:out value="${customer.username}"/></a> 
		<a class="ml-1" href="../customers/customer.jhtm?id=${customer.id}">
			<i class="sli-pencil"></i>
		</a>
	</div>
	
	<div class="grid-stage grid-stage-sm">
		<table id="grid" class="grid-sm mt-1 mb-1"></table>
	</div>
	
	<c:if test="${siteConfig['GROSS_PROFIT_ON_ADMIN_INVOICE'].value == 'true'}">
		<c:set value="0.0" var="totalOriginalPrice"/>
		<c:set value="0.0" var="totalCost"/>
	</c:if>
	
	<c:import url="/WEB-INF/jsp/admin-responsive/orders/invoice/main_tab_total.jsp" />
	
	<div class="grid-stage grid-stage-sm">	
		<table id="order-status-grid" class="mt-5 mb-3"></table>	
	</div>

	<c:import url="/WEB-INF/jsp/admin-responsive/orders/invoice/main_tab_order_status.jsp" />
</div>