<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/security/tags" prefix="sec" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ taglib uri="/WEB-INF/tlds/custom.tld" prefix="wj" %>

<div class="row justify-content-between summary-banner">
	<c:set value="${order.billing}" var="address"/>
	<div class="col-md-6 col-lg-3 summary-box">
		<label class="col-form-label"><fmt:message key="billingInformation" /></label>
		<table class="summary-table">
			<tr>
				<td>
					<c:out value="${address.firstName}"/>&nbsp;<c:out value="${address.lastName}"/>
				</td>
			</tr>
			<c:if test="${address.company != ''}">
				<tr>
					<td><c:out value="${address.company}"/></td>
				</tr>
			</c:if>
			<tr>
				<td>
					<c:out value="${address.addr1}"/><br>
					<c:if test="${address.addr2 != null and address.addr2 != '' }">
						<c:out value="${address.addr2}"/><br>
					</c:if>
					<c:out value="${address.city}"/>,&nbsp;<c:out value="${address.stateProvince}"/>&nbsp;<c:out value="${address.zip}"/><br>
					<c:if test="${!fn:contains(siteConfig['SITE_URL'].value, 'respectofflorida')}">
						<c:out value="${countries[address.country]}"/><br>
					</c:if>
					<c:if test="${siteConfig['SHIPPING_LTL'].value == 'true'}">
						<c:out value="${address.liftgateToString}"/><br>
					</c:if>
				</td>
			</tr>
			<c:if test="${siteConfig['HIDE_INVOICE_PHONE_INFO'].value != 'true'}">
				<c:if test="${address.phone != ''}">
					<tr>
						<td><fmt:message key="f_phone" />: <c:out value="${address.phone}"/></td>
					</tr>
				</c:if>
				<c:if test="${address.cellPhone != ''}">
					<tr>
						<td><fmt:message key="f_mobilePhone" />: <c:out value="${address.cellPhone}"/></td>
					</tr>
				</c:if>
				<c:if test="${address.fax != null and fn:length(fn:trim(address.fax)) > 0 and !fn:contains(siteConfig['SITE_URL'].value, 'respectofflorida')}">
					<tr>
						<td><fmt:message key="f_fax" /></td>
						<td><c:out value="${address.fax}"/></td>
					 </tr>
				</c:if>
			</c:if>
		</table>
	</div>
	<div class="col-md-6 col-lg-3 summary-box">
		<label class="col-form-label"><c:choose><c:when test="${order.workOrderNum != null}"><fmt:message key="service" /> <fmt:message key="location" /></c:when><c:otherwise><fmt:message key="shippingInformation" /></c:otherwise></c:choose></label>
		<table class="summary-table">
			<tr>
				<td>
					<c:out value="${address.firstName}"/>&nbsp;<c:out value="${address.lastName}"/>
				</td>
			</tr>
			<c:if test="${address.company != ''}">
				<tr>
					<td><c:out value="${address.company}"/></td>
				</tr>
			</c:if>
			<tr>
				<td>
					<c:out value="${address.addr1}"/><br>
					<c:if test="${address.addr2 != null and address.addr2 != '' }">
						<c:out value="${address.addr2}"/><br>
					</c:if>
					<c:out value="${address.city}"/>,&nbsp;<c:out value="${address.stateProvince}"/>&nbsp;<c:out value="${address.zip}"/><br>
					<c:if test="${order.purchaseOrder != '' and order.purchaseOrder != null and fn:contains(siteConfig['SITE_URL'].value, 'respectofflorida')}">
						<fmt:message key="poNumber" />:&nbsp;<c:out value="${order.purchaseOrder}"/><br>
					</c:if>
					<c:if test="${!fn:contains(siteConfig['SITE_URL'].value, 'respectofflorida')}">
						<c:out value="${countries[address.country]}"/><br>
					</c:if>
					<c:if test="${siteConfig['SHIPPING_LTL'].value == 'true'}">
						<c:out value="${address.liftgateToString}"/><br>
					</c:if>
				</td>
			</tr>
			<c:if test="${siteConfig['HIDE_INVOICE_PHONE_INFO'].value != 'true'}">
				<c:if test="${address.phone != ''}">
					<tr>
						<td><fmt:message key="f_phone" />: <c:out value="${address.phone}"/></td>
					</tr>
				</c:if>
				<c:if test="${address.cellPhone != ''}">
					<tr>
						<td><fmt:message key="f_mobilePhone" />: <c:out value="${address.cellPhone}"/></td>
					</tr>
				</c:if>
				<c:if test="${address.fax != null and fn:length(fn:trim(address.fax)) > 0 and !fn:contains(siteConfig['SITE_URL'].value, 'respectofflorida')}">
					<tr>
						<td><fmt:message key="f_fax" />: <c:out value="${address.fax}"/></td>
					 </tr>
				</c:if>
			</c:if>
			<c:if test="${not empty workOrder.service.alternateContact}">
				<tr>
					<td><fmt:message key="alternateContact" />: <c:out value="${workOrder.service.alternateContact}"/></td>
				</tr>
			</c:if>
		</table>
	</div>
	<div class="col-md-6 col-lg-3 summary-box summary-box-2">
		<table class="summary-table">
			<tr>
				<td><fmt:message key="account"/> Number:</td>
				<td><c:out value="${customer.accountNumber}"/></td>
			</tr>
			<tr>
				<td><fmt:message key="companyName"/>:</td>
				<td><c:out value="${customer.address.company}"/></td>
			</tr>
			<tr>
				<td><fmt:message key="taxID"/> Number:</td>
				<td><c:out value="${customer.taxId}"/></td>
			</tr>
			<tr>
				<td><fmt:message key="orderCount"/>:</td>
				<td><a href="ordersList.jhtm?email=<c:out value='${customer.username}'/>"><c:out value="${customerOrderInfo.orderCount}"/></a></td>
			</tr>
			<tr>
				<td><fmt:message key="lastOrder"/>:</td>
				<td><fmt:formatDate pattern="${siteConfig['SITE_DATE_FORMAT'].value}" timeZone="${siteConfig['SITE_TIME_ZONE'].value}" value="${customerOrderInfo.lastOrderDate}"/></td>
			</tr>
			<c:if test="${gSiteConfig['gPAYMENTS']}">			  
				<sec:authorize ifAnyGranted="ROLE_AEM,ROLE_ADMIN_${prop_site_id},ROLE_INCOMING_PAYMENT_VIEW_LIST">
					<tr>
						<td><fmt:message key="incomingPayments"/>:</td>
						<td><a href="../customers/payments.jhtm?cid=${customer.id}"> view</a></td>
					</tr>
				</sec:authorize>
				<c:if test="${gSiteConfig['gVIRTUAL_BANK_ACCOUNT']}">
					<sec:authorize ifAnyGranted="ROLE_AEM,ROLE_ADMIN_${prop_site_id},ROLE_OUTGOING_PAYMENT_VIEW_LIST">
						<tr>
							<td><fmt:message key="outgoingPayments"/>:</td>
							<td><a href="../customers/virtualBankPayment.jhtm?username=${customer.username}"> view</a></td>
						</tr>
					</sec:authorize>
				</c:if>
			</c:if> 
		</table>
	</div>
	<div class="col-md-6 col-lg-3 summary-box summary-box-2">
		<table class="summary-table">
			<c:if test="${order.host != null}">
				<tr>
					<td><fmt:message key="multiStore" />:</td>
					<td><c:out value="${order.host}"/></td> 
				</tr>
			</c:if>
			<c:if test="${relatedOrders1 != null and fn:length(relatedOrders1) > 0}">
                	<tr>
                		<td>Related Orders:</td>
                		<td>
                			<c:forEach items="${relatedOrders1}" var="relatedOrder">
							<a href="/admin/orders/invoice.jhtm?order=${relatedOrder.orderId}&orderName=order">${relatedOrder.orderId}</a>  
						</c:forEach>
					</td>
                	</tr>
			</c:if>
			<c:if test="${order.externalOrderId != null}">
                	<tr>
                		<td><fmt:message key="externalOrderId" />:</td>
                		<td><c:out value="${order.externalOrderId}"/></td>
                	</tr>
			</c:if>
			<c:if test="${order.subscriptionCode != null}">
				<tr>
					<td><fmt:message key="subscription" />:</td>
					<td><a href="subscription.jhtm?code=${order.subscriptionCode}"><c:out value="${order.subscriptionCode}"/></a></td>
				</tr>
			</c:if>
			<c:if test="${order.ipAddress != null}">
				<tr>
					<td><fmt:message key="ipAddress" />:</td>
					<td><c:out value="${order.ipAddress}"/></td>
				</tr>
			</c:if>
			<c:if test="${siteConfig['SUB_STATUS'].value != '0'}">
				<tr>
					<td><fmt:message key="subStatus"/>:</td>
					<c:if test="${order.subStatus != null}">
						<td><c:out value="${orderSubStatusMap[order.subStatus]}" /></td>
					</c:if>
				</tr>
			</c:if>
			<c:if test="${gSiteConfig['gADD_INVOICE']}">
				<tr>
					<td><fmt:message key="from"/>:</td>
					<td><fmt:message key="${order.backEndOrderString}"/></td>
				</tr>
			</c:if>
			<c:if test="${gSiteConfig['gINVOICE_APPROVAL'] and !empty order.approval}">
				<tr>
					<td><fmt:message key="approval"/>:</td>
					<td><fmt:message key="approvalStatus_${order.approval}" /></td>
				</tr>
			</c:if>
			<c:if test="${!empty order.orderType}">
				<tr>
					<td><fmt:message key="orderType"/>:</td>
					<td><c:out value="${order.orderType}"/></td>
				</tr>
			</c:if>
			<c:if test="${!empty order.trackcode}">
				<tr>
					<td><fmt:message key="trackcode"/>:</td>
					<td><c:out value="${order.trackcode}"/></td>
				</tr>
			</c:if>
			<c:if test="${order.flag1 != null}">
				<tr>
					<td><c:out value="${siteConfig['ORDER_FLAG1_NAME'].value}" />:</td>
					<td><fmt:message key="flag_${order.flag1}"/></td>
				</tr>
			</c:if>
			<c:if test="${siteConfig['ORDER_CUSTOM_FIELD1_NAME'].value != ''}">
				<tr>
					<td><c:out value="${siteConfig['ORDER_CUSTOM_FIELD1_NAME'].value}" />:</td>
					<td><c:out value="${order.customField1}"/></td>
				</tr>
			</c:if>
			<c:if test="${siteConfig['ORDER_CUSTOM_FIELD2_NAME'].value != ''}">
				<tr>
					<td><c:out value="${siteConfig['ORDER_CUSTOM_FIELD2_NAME'].value}" />:</td>
					<td><c:out value="${order.customField2}"/></td>
				</tr>
			</c:if>
			<c:if test="${siteConfig['ORDER_CUSTOM_FIELD3_NAME'].value != ''}">
				<tr>
					<td><c:out value="${siteConfig['ORDER_CUSTOM_FIELD3_NAME'].value}" />:</td>
					<td><c:out value="${order.customField3}"/></td>
				</tr>
			</c:if>
			<c:if test="${zipFile}">
				<tr>
					<td><fmt:message key="zipFile"/>:</td>
					<td><a href="<c:url value="/assets/orders_zip/${order.orderId}.zip"/>"><c:out value="${order.orderId}"/>.zip</a></td>
				</tr>
			</c:if>
			<c:if test="${order.attachmentsUrl != null}">
				<tr>
					<td><fmt:message key="attachment"/>:</td>
					<td><a href="<c:url value="${order.attachmentsUrl}"/>" target="_blank" ><fmt:message key="view" /></a></td>
				</tr>
			</c:if>
			<c:if test="${order.linkShare != null}">
				<tr>
					<td>LinkShare siteID:</td>
					<td><c:out value="${order.linkShare.siteID}"/></td>
				</tr>
				<tr>
					<td>LinkShare Entry:</td>
                		<td><fmt:formatDate type="both" timeZone="${siteConfig['SITE_TIME_ZONE'].value}" pattern="MM/dd/yyyy hh:mm a" value="${order.linkShare.dateEntered}"/></td>
				</tr> 
			</c:if> 
			<tr>
				<td><fmt:message key="orderDate" />:</td> 
				<td><fmt:formatDate type="both" timeZone="${siteConfig['SITE_TIME_ZONE'].value}" pattern="${siteConfig['SITE_DATE_FORMAT'].value} hh:mm a" value="${order.dateOrdered}"/></td>
			</tr>
			<tr>
                <td><fmt:message key="invoiceDueDate"/>:</td> 
				<td><fmt:formatDate type="both" timeZone="${siteConfig['SITE_TIME_ZONE'].value}" pattern="MM/dd/yyyy hh:mm a" value="${order.invoiceDueDate}"/></td>
			</tr>
			<c:if test="${siteConfig['DELIVERY_TIME'].value == 'true'}" >
				<td><fmt:message key="dueDate" />:</td>
				<td><fmt:formatDate type="both" timeZone="${siteConfig['SITE_TIME_ZONE'].value}" pattern="${siteConfig['SITE_DATE_FORMAT'].value}" value="${order.dueDate}"/></td>
			</c:if>
			
			<c:if test="${siteConfig['USER_EXPECTED_DELIVERY_TIME'].value == 'true'}" >
				<tr>
					<td><fmt:message key="userExpectedDueDate" />:</td>
					<td><fmt:formatDate type="both" timeZone="${siteConfig['SITE_TIME_ZONE'].value}" pattern="${siteConfig['SITE_DATE_FORMAT'].value}" value="${order.userDueDate}"/></td>
				</tr>
			</c:if>
			<c:if test="${siteConfig['REQUESTED_CANCEL_DATE'].value == 'true'}" >
				<tr>
					<td><fmt:message key="requestedCancelDate" />:</td>
					<td><fmt:formatDate type="both" timeZone="${siteConfig['SITE_TIME_ZONE'].value}" pattern="${siteConfig['SITE_DATE_FORMAT'].value}" value="${order.requestedCancelDate}"/></td>
				</tr>
			</c:if>
			<tr>
				<td><fmt:message key="nextActivityDate"/>:</td>
				<td><fmt:formatDate type="both" timeZone="${siteConfig['SITE_TIME_ZONE'].value}" pattern="MM/dd/yyyy hh:mm a" value="${order.nextActivityDate}"/></td>
			</tr>
		</table>
	</div>
</div>