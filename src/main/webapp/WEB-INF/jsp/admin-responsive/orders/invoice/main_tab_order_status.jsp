<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ taglib uri="http://www.springframework.org/security/tags" prefix="sec" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ taglib uri="/WEB-INF/tlds/spring.tld" prefix="spring" %>
<%@ taglib uri="/WEB-INF/tlds/custom.tld" prefix="wj" %>

<form:form commandName="orderStatus" action="invoice.jhtm" onsubmit="return checkMessage()" enctype="multipart/form-data"> 
	<div class="row mb-3">
		<div class="col-md-6 col-lg-4">
			<div class="max-w-300">
				<c:choose>
					<c:when test="${!((order.status == 's') || (order.status == 'x') || (order.status == 'xap' or order.status == 'ano' or order.status == 'xaf' or order.status == 'xl'))}">
						<div class="form-row">
							<div class="col-12">
								<label class="col-form-label"><fmt:message key="status"/>:</label>
								<div class="form-group">
									<form:select path="status" class="custom-select" id="status">
										<c:choose>
											<c:when test="${order.status == 'ars'}">
												<form:option value="ars"><fmt:message key="orderStatus_ars" /></form:option>
												<form:option value="xaf"><fmt:message key="orderStatus_xaf" /></form:option>
												<c:choose>
													<c:when test="${(!order.processed or order.fulfilled) && !order.paymentAlert}">
														<form:option value="s"><fmt:message key="orderStatus_s" /></form:option>
													</c:when>
													<c:otherwise>
														<sec:authorize ifAnyGranted="ROLE_AEM">
															<form:option value="s"><fmt:message key="orderStatus_s" /></form:option>
														</sec:authorize>
													</c:otherwise>
												</c:choose>
											</c:when>
											<c:otherwise>
												<form:option value="p"><fmt:message key="orderStatus_p" /></form:option>
												<form:option value="pr"><fmt:message key="orderStatus_pr" /></form:option>
												<c:choose>
													<c:when test="${!order.paymentAlert}">
														<form:option value="s"><fmt:message key="orderStatus_s" /></form:option>
													</c:when>
													<c:otherwise>
														<sec:authorize ifAnyGranted="ROLE_AEM,ROLE_ADMIN_${prop_site_id},ROLE_OVERRIDE_PAYMENT_ALERT">
															<form:option value="s"><fmt:message key="orderStatus_s" /></form:option>
														</sec:authorize>
													</c:otherwise>
												</c:choose>
												<form:option value="x"><fmt:message key="orderStatus_x" /></form:option>
												<form:option value="ps"><fmt:message key="orderStatus_ps" /></form:option>
												<c:if test="${order.status == 'xp'}">
													<form:option value="xp"><fmt:message key="orderStatus_xp" /></form:option>
												</c:if>
												<c:if test="${order.status == 'xnc'}">
													<form:option value="xnc"><fmt:message key="orderStatus_xnc" /></form:option>
												</c:if>
												<c:if test="${order.status == 'xeb'}">
													<form:option value="xeb"><fmt:message key="orderStatus_xeb" /></form:option>
												</c:if>
												<c:if test="${order.status == 'xg'}">
													<form:option value="xg"><fmt:message key="orderStatus_xg" /></form:option>
												</c:if>			        
												<c:if test="${order.status == 'xge'}">
													<form:option value="xge"><fmt:message key="orderStatus_xge" /></form:option>
												</c:if>		
												<c:if test="${order.status == 'xl'}">
													<form:option value="xl"><fmt:message key="orderStatus_xl" /></form:option>
												</c:if>	
												<c:if test="${order.status == 'xe'}">
													<form:option value="xe"><fmt:message key="orderStatus_xe" /></form:option>
												</c:if>	  
												<c:if test="${order.status == 'xmi'}">
													<form:option value="xmi"><fmt:message key="orderStatus_xmi" /></form:option>
												</c:if>	
											</c:otherwise>
										</c:choose>
									</form:select>
								</div>
							</div>
						</div>
					</c:when>
					<c:otherwise>
						<form:hidden path="status"/>
					</c:otherwise>
				</c:choose> 
				<c:if test="${siteConfig['SUB_STATUS'].value != '0'}">
					<c:if test="${fn:length(orderSubStatusGroup) > 0}">
						<div class="form-row">
							<div class="col-12">
								<label class="col-form-label"><fmt:message key="subStatus"/>&nbsp;<fmt:message key="group"/>:</label>
								<div class="form-group">
									<select class="custom-select" name="subStatusGroup" id="subStatusGroupId" onchange="subStatusGroupOnchanged(this)">
										<option value="All" />
										<c:forEach items="${orderSubStatusGroup}" var="subStatusGroup">
											<option value="${subStatusGroup.id}" <c:if test="${lastSubStatusGroup.id == subStatusGroup.id}">selected="selected"</c:if> ><c:out value="${subStatusGroup.name}" /></option>
										</c:forEach>
									</select>
								</div>
							</div>
						</div>
					</c:if> 
					<div class="form-row" <c:if test="${fn:length(orderSubStatusGroup) > 0 and fn:length(order.statusHistory) == 0}">style="display: none;"</c:if>>
						<div class="col-12">
							<label class="col-form-label"><fmt:message key="subStatus"/>:</label>
							<div class="form-group" id="subStatusList">
								<form:select class="custom-select" id="subStatus" path="subStatus" onchange="subStatusOnchanged(this)"> 
									<form:option value=""></form:option>
									<c:forEach items="${subStatusList}" var="subStatus">
										<form:option value="${subStatus.id}"><c:out value="${subStatus.name}" /></form:option>
									</c:forEach>
								</form:select> 
							</div>
						</div>
					</div>
				</c:if>
				<div class="form-row">
					<div class="col-12">
						<label class="col-form-label"><fmt:message key="trackNum"/>:</label>
						<div class="form-group">
							<form:input path="trackNum" class="form-control"/>
						</div>
					</div>
				</div>
				<div class="form-row">
					<div class="col-12">
						<label class="col-form-label"><fmt:message key="comments"/>:</label>
						<div class="form-group">
							<form:textarea class="form-control" path="comments" rows="8"/>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="col-md-6 col-lg-5">
			<div class="max-w-400">
				<div class="form-row">
					<div class="col-12">
						<div class="form-group">
							<label class="custom-control custom-checkbox">
								<form:checkbox class="custom-control-input" path="userNotified" onclick="notifyCustomer()"/> Notify Customer
								<span class="custom-control-indicator"></span>
							</label>
						</div>
					</div>
				</div>
				<div id="notifyCustomer" class="collapse">
					<div id="messageSelect" style="display:none;"> 
						<c:if test="${siteConfig['CUSTOM_SHIPPING_CONTACT_INFO'].value == 'true'}">
							<div class="form-row">
								<div class="col-12">
									<div class="form-group">
										<select class="custom-select" name="contact" onChange="insertContact(this)">
											<option value="">choose a contact</option>
											<c:forEach var="contact" items="${contacts}">
												<option value="${contact.id}"><c:out value="${contact.company}"/></option>
											</c:forEach>
										</select>
									</div>
								</div>
							</div>
							<c:forEach var="contact" items="${contacts}">
								<input type="hidden" id="contact_company_${contact.id}" value="${contact.company}" />
							</c:forEach>
						</c:if> 
						<div class="form-row">
							<div class="col-lg-6">
								<div class="form-group">
									<select id="groupSelected" class="custom-select" onChange="getSiteMessage(this.value)">
										<option value="0"><fmt:message key="chooseGroupMessage"/></option>
										<option value="-1">All Messages</option>
										<c:forEach var="group" items="${messageGroups}">
											<option value="${group.id}" <c:if test="${group.id == siteConfig['SITE_MESSAGE_GROUP_ID_FOR_ORDER_EMAIL'].value}">selected</c:if>><c:out value="${group.name}"/></option>
										</c:forEach>
									</select>
								</div>
							</div>
							<div class="col-lg-6">
								<div class="form-group" id="siteMessageSelect"></div>
							</div>
						</div> 
					</div>  
					<c:if test="${gSiteConfig['gPDF_INVOICE']}"> 
						<div class="form-row">
							<div class="col-12">
								<div class="form-group">
									<label class="custom-control custom-checkbox">
										<input type="checkbox" class="custom-control-input" name="__attach" value="true" id="attachInvoiceCheckbox"><fmt:message key="attach"/>&nbsp;<fmt:message key="invoice"/>
										<span class="custom-control-indicator"></span>
									</label>
								</div>
							</div>
						</div>
					</c:if>
					<div class="form-row">
						<div class="col-12">
							<label class="col-form-label"><fmt:message key="to"/>:</label><c:out value="${customer.username}"/>
						</div>
					</div>
					<div class="form-row">
						<div class="col-12">
							<label class="col-form-label"><fmt:message key="from"/>:</label>
							<div class="form-group">
								<form:input path="from" class="form-control"/>	
							</div>
						</div>
					</div>
					<div class="form-row">
						<div class="col-12">
							<label class="col-form-label">Cc:</label>
							<div class="form-group">
								<form:input path="cc" class="form-control"/> 	
							</div>
						</div>
					</div>
					<div class="form-row">
						<div class="col-12">
							<label class="col-form-label">Bcc:</label>
							<c:choose>
								<c:when test="${multiStore != null}">
									<c:out value="${multiStore.contactEmail}"/>
								</c:when>
								<c:otherwise>
									<c:out value="${siteConfig['CONTACT_EMAIL'].value}"/>
								</c:otherwise>
							</c:choose>  
						</div>
					</div>
					<div class="form-row">
						<div class="col-12">
							<label class="col-form-label"><fmt:message key="subject"/>:</label>
							<div class="form-group">
								<form:input id="subject" path="subject" class="form-control"/>
								<c:forEach var="message" items="${messages}">
									<input type="text" style="display:none;" id="subject${message.messageId}" name="subject" value="<c:out value="${message.subject}"/>" class="form-control">
								</c:forEach>
							</div> 
						</div>
					</div> 
					<div class="form-row">
						<label class="col-form-label col-lg-4">Attachment:</label>
						<div class="form-group col-lg-8">
							<input type="file" name="attachment">
						</div> 
					</div> 
					<div class="form-row">
						<div class="col-12">
							<div class="form-group">
								<label class="custom-control custom-checkbox">
									<form:checkbox path="htmlMessage" class="custom-control-input" id="htmlCheckbox"/>Html
									<span class="custom-control-indicator"></span>
								</label>
								<c:forEach var="message" items="${messages}">
									<input type="hidden" id="htmlID${message.messageId}" value="<c:out value="${message.html}"/>" />
								</c:forEach>
							</div>
						</div>
					</div> 
					<div class="form-row">
						<div class="col-12">
							<div class="form-group">
								<form:textarea id="message" path="message" class="form-control" rows="10"/>
								<c:forEach var="message" items="${messages}"> 
									<textarea style="display:none;" rows="10" id="message${message.messageId}" name="message" class="form-control"><c:out value="${message.message}"/></textarea>
								</c:forEach>
							</div> 
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="footer">
		<div class="form-row">
			<div class="col-12">
				<sec:authorize ifAnyGranted="ROLE_AEM,ROLE_ADMIN_${prop_site_id},ROLE_INVOICE_STATUS">
					<input type="hidden" name="invenotryFlag" id="invenotryFlag" value="${gSiteConfig['gINVENTORY']}"/>
					<input type="hidden" name="processed" id="processed" value="${order.processed}" />
					<input type="hidden" name="fulfilled" id="fulfilled" value="${order.fulfilled}" />
					<button class="btn btn-default">Update</button>
					<a class="btn btn-default" href="${_contextpath}/admin/orders">Cancel</a>
				</sec:authorize>   
			</div>
		</div>
	</div>
	<input type="hidden" name="order" value="<c:out value="${order.orderId}"/>" >
	<input type="hidden" name="balance" id="balance_${order.orderId}" value="<c:out value="${order.grandTotal - order.amountPaid}"/>" >
</form:form>