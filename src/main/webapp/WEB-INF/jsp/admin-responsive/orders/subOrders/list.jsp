<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ taglib uri="http://www.springframework.org/security/tags" prefix="sec" %>
<%@ taglib uri="/WEB-INF/tlds/spring.tld" prefix="spring" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>

<tiles:insertDefinition name="admin.responsive-template.orders" flush="true">
	<tiles:putAttribute name="tab"  value="subOrders" />
  	<tiles:putAttribute name="content" type="string">
	<tiles:putAttribute name="right_sidebar" value="/WEB-INF/jsp/admin-responsive/orders/right_sidebar.jsp?tab=subOrders" />
	
<sec:authorize ifAnyGranted="ROLE_AEM,ROLE_ADMIN_${prop_site_id},ROLE_INVOICE_VIEW_LIST,ROLE_INVOICE_EDIT,ROLE_INVOICE_STATUS"> 


	<!-- Begin: Content -->
	<section id="content" class="animated fadeIn"> 
	<form action="subOrdersList.jhtm?orderName=${orderName}" id="list" name="list_form" method="post">
		<input type="hidden" id="sort" name="sort" value="${subOrderSearch.sort}" />
		
		<!-- Error Message -->
	    <c:if test="${!empty model.message}">
		  <div class="highlight error"><spring:message code="${model.message}" arguments="${model.arguments}"/></div>
	    </c:if>
	    
		<button type="button" class="btn btn-danger btn-toggle-collapse mb-15 collapsed" data-toggle="collapse" data-target="#quickModeOptions">
			 <span class="fa fa-plus-minus mr-5"></span>  <fmt:message key="options" />
	    </button>
		
		<!-- Begin: quickModeOptions -->
		<c:if test="${model.count > 0}">
		<div id="quickModeOptions" class="collapse">  
			<div class="row">
				<c:if test="${gSiteConfig['gGROUP_CUSTOMER']}">
					 
					<c:if test="${model.count > 0 and siteConfig['BATCH_PRINT_INVOICE'].value == 'true'}">
			         	<div class="col-sm-6 col-md-4 col-lg-3">
							<div class="panel panel-dark" > 
								<div class="panel-heading" data-mh="option-panel-heading"> 
									<span class="panel-icon">
										<span class="fa fa-fw fa-question-circle" data-toggle="tooltip" title="<fmt:message key="batchPrint" />::Print multiple order">
										</span>
									</span>
									<span class="panel-title">Batch Print Packing</span>
									<span class="panel-title pull-right"><input type="checkbox" onclick="toggleAll(this)" name="__groupAssignAll" value="true"/> <fmt:message key="all"/></span>
								</div>
							 	<div class="panel-body" data-mh="option-panel-body"> 
							 		<div class="mb-15">
										<select name="packing_layout" class="form-control input-sm">
											<option value="packing"><fmt:message key="packingList1"/></option>
											<option value="packing2"><fmt:message key="packingList2"/></option>
											<option value="packing3"><fmt:message key="packingList3"/></option>
											<option value="packing4"><fmt:message key="packingList4"/></option>
										</select>
									</div>
									<input type="submit" name="__batchPackList" class="btn btn-sm btn-dark" value="<fmt:message key="batchPrint" />" onClick="return batchSelected()"/> 
								</div>
							</div>
						</div>
					</c:if>
					
					<c:if test="${model.count > 0 and siteConfig['WAREHOUSE'].value == 'true'}">
						<div class="col-sm-6 col-md-4 col-lg-3">
							<div class="panel panel-dark" > 
								<div class="panel-heading" data-mh="option-panel-heading"> 
									<span class="panel-icon">
										<span class="fa fa-fw fa-question-circle" data-toggle="tooltip" title="<fmt:message key="batchPrint" />::Print multiple order">
										</span>
									</span>
									<span class="panel-title">Batch Pick Tickets</span>
									<span class="panel-title pull-right"><input type="checkbox" onclick="toggleAll(this)" name="__groupAssignAll" value="true"/> <fmt:message key="all"/></span>
								</div>
							 	<div class="panel-body" data-mh="option-panel-body">  
							 		<input type="submit" name="__batchPickTicket" class="btn btn-sm btn-dark" value="<fmt:message key="batchPrint" />" onClick="return batchSelected()"/>  
								</div>
							</div>
						</div>
					</c:if>
					
					<c:if test="${model.count > 0 and siteConfig['WAREHOUSE'].value == 'true'}">
						<div class="col-sm-6 col-md-4 col-lg-3">
							<div class="panel panel-dark" > 
								<div class="panel-heading" data-mh="option-panel-heading"> 
									<span class="panel-icon">
										<span class="fa fa-fw fa-question-circle" data-toggle="tooltip" title="<fmt:message key="batchPrint" />::Print multiple order">
										</span>
									</span>
									<span class="panel-title">Batch Pack/Pick Tickets</span>
									<span class="panel-title pull-right"><input type="checkbox" onclick="toggleAll(this)" name="__groupAssignAll" value="true"/> <fmt:message key="all"/></span>
								</div>
							 	<div class="panel-body" data-mh="option-panel-body">  
							 		<div class="mb-15">
							 			<select name="packing_print_layout" class="form-control input-sm">
											<option value="packing"><fmt:message key="packingList1"/></option>
											<option value="packing2"><fmt:message key="packingList2"/></option>
											<option value="packing3"><fmt:message key="packingList3"/></option>
											<option value="packing4"><fmt:message key="packingList4"/></option>
										</select>
							 		</div>
							 		<input type="submit" name="__batchPrintPickTicket" value="<fmt:message key="batchPrint" />" onClick="return batchSelected()" class="btn btn-sm btn-dark"/> 
								</div>
							</div>
						</div>
					</c:if> 
				</c:if>
			</div>
		</div>
		</c:if>
		<!-- End: quickModeOptions -->

		<!-- Begin: Orders Table -->
		<div class="panel panel-default"> 
			<div class="panel-menu">
			
				<table class="fluid-width">
                    <tbody>
                    <tr>
                        <td class="pageShowing pr-15">
                        	<fmt:message key="showing">
								<fmt:param value="${subOrderSearch.offset + 1}"/>
								<fmt:param value="${model.pageEnd}"/>
								<fmt:param value="${model.count}"/>
							</fmt:message> 
                        </td>
                        <td>
                            <table class="pull-right">
                                <tbody>
                                <tr>
                                    <td class="pageNumber pr-15 hidden-xs">
                                        <table>
                                            <tbody>
                                            <tr>
                                                <td class="pr-5">Page</td>
                                                <td class="w-75"> 
                                                	<c:choose>
													  <c:when test="${model.pageCount <= 100 and model.pageCount > 0}">
														  <select name="page" id="page" onchange="submit()" class="form-control input-sm" title="Page Number">
														  <c:forEach begin="1" end="${model.pageCount}" var="page">
														  	<option value="${page}" <c:if test="${page == (subOrderSearch.page)}">selected</c:if>>${page}</option>
														  </c:forEach>
														  </select>
													  </c:when>
													  <c:otherwise>
														  <input type="text" id="page" name="page" value="${productSearch.page}" class="form-control input-sm" />
														  <input type="submit" value="go" class="form-control input-sm"/>
													  </c:otherwise>
												  	</c:choose> 
                                                </td>
                                                <td class="pl-5">of <c:out value="${model.pageCount}"/></td>
                                            </tr>
                                            </tbody>
                                        </table>
                                    </td>
                                    <td class="pageSize pr-15 w-150 hidden-xs">  
									 	<select name="size" onchange="document.getElementById('page').value=1;submit();" class="form-control input-sm" title="Page Size" >
										    <c:forTokens items="10,25,50,100,200,500,2000" delims="," var="current">
										  	  <option value="${current}" <c:if test="${current == subOrderSearch.pageSize}">selected</c:if>>${current} <fmt:message key="perPage" /></option>
										    </c:forTokens>
									    </select>
                                    </td>
                                    <td class="pageNav">
                                        <div class="btn-group btn-group-sm pull-right">
	                                        <c:choose>
	                                        	<c:when test="${subOrderSearch.page == 1}">
                                        		 	<a href="javascript:void(0);" title="Previous" class="btn btn-dark">
	                                        		 	<span class="fa fa-chevron-left"></span> 
                                        		 	</a>
                                       		 	</c:when>
	                                        	<c:otherwise>
	                                        		<a href="<c:url value="subOrdersList.jhtm?orderName=${orderName}"><c:param name="page" value="${subOrderSearch.page-1}"/></c:url>" title="Previous" class="btn btn-dark">
	                                        			<span class="fa fa-chevron-left"></span> 
	                                       			</a>
	                                        	</c:otherwise>
	                                       	</c:choose>
	                                        <c:choose>
	                                        	<c:when test="${subOrderSearch.page == model.pageCount}">
                                        		  	<a href="javascript:void(0);" title="Next" class="btn btn-dark"> 
	                                        			<span class="fa fa-chevron-right"></span>
                                        			</a>
	                                        	</c:when>
	                                        	<c:otherwise>
	                                        		<a href="<c:url value="subOrdersList.jhtm?orderName=${orderName}"><c:param name="page" value="${subOrderSearch.page+1}"/></c:url>" title="Next" class="btn btn-dark">
	                                        			 <span class="fa fa-chevron-right"></span> 
	                                        		</a>
	                                        	</c:otherwise>
	                                       	</c:choose> 
                                        </div>
                                    </td>
                                </tr>
                                </tbody>
                            </table>
                        </td>
                    </tr>
                    </tbody>
                </table> 
            </div> 
			<table class="responsiveBootstrapFooTable table table-hover" data-sorting="true">
				<c:set var="cols" value="0"/>
				<c:set var="right_cols" value="0"/>
				<thead>
					<tr class="bg-light"> 
						<th class="text-nowrap w-50" data-type="html" data-sortable="false">
							<c:set var="cols" value="${cols+1}"/>
	                        <label class="checkbox-inline">
	                            <input type="checkbox" onclick="toggleAll(this)" name="__groupAssignAll" value="true" class="checkAll"> #
	                        </label>
	                    </th> 
	                    
						<c:if test="${fn:contains(siteConfig['CUSTOMIZE_ORDER_LIST'].value,'billedTo')}">
							<c:set var="cols" value="${cols+1}"/>
			    			<c:choose>
			    	      		<c:when test="${subOrderSearch.sort == 'bill_to_last_name DESC, bill_to_first_name DESC'}">
					    	        <th class="text-nowrap" data-type="html">
										<a class="hdrLink" href="#" onClick="$('#sort').val('bill_to_last_name, bill_to_first_name');$('#list').submit();"><fmt:message key="billTo" /></a> 
			    	        		</th>
						    	</c:when>
					    		<c:when test="${subOrderSearch.sort == 'bill_to_last_name, bill_to_first_name'}">
					    	        <th class="text-nowrap" data-type="html">
										<a class="hdrLink" href="#" onClick="$('#sort').val('bill_to_last_name DESC, bill_to_first_name DESC');$('#list').submit();"><fmt:message key="billTo" /></a> 
			    	        		</th>
						    	</c:when>
					    		<c:otherwise>
			    	            	<th class="text-nowrap" data-type="html">
						    	        <a class="hdrLink" href="#" onClick="$('#sort').val('bill_to_last_name DESC, bill_to_first_name DESC');$('#list').submit();"><fmt:message key="billTo" /></a> 
			    	        		</th>
					    		</c:otherwise>
			    	    	</c:choose>
		    			</c:if>
						<c:if test="${fn:contains(siteConfig['CUSTOMIZE_ORDER_LIST'].value,'company')}">
							<c:set var="cols" value="${cols+1}"/>
			    			<c:choose>
			    	      		<c:when test="${subOrderSearch.sort == 'bill_to_company DESC'}">
			    	        		<th class="text-nowrap" data-type="html" data-breakpoints="xs">
										<a class="hdrLink" href="#" onClick="document.getElementById('sort').value='bill_to_company';document.getElementById('list').submit()"><fmt:message key="company" /></a> 
				    	        	</th>
			    	     		</c:when>
			    	      		<c:when test="${subOrderSearch.sort == 'bill_to_company'}">
			    	        		<th class="text-nowrap" data-type="html" data-breakpoints="xs">
										<a class="hdrLink" href="#" onClick="document.getElementById('sort').value='bill_to_company DESC';document.getElementById('list').submit()"><fmt:message key="company" /></a> 
				    	        	</th>
			    	     		</c:when>
			    	      		<c:otherwise>
					    	        <th class="text-nowrap" data-type="html" data-breakpoints="xs">
							    	 	<a class="hdrLink" href="#" onClick="document.getElementById('sort').value='bill_to_company DESC';document.getElementById('list').submit()"><fmt:message key="company" /></a> 
				    	      		</th>
			    	      		</c:otherwise>
			    	    	</c:choose>
		    			</c:if>
		    			
		    			<c:if test="${fn:contains(siteConfig['CUSTOMIZE_ORDER_LIST'].value,'poStatus')}">
		    				<c:set var="cols" value="${cols+1}"/>
			    			<c:choose>
			    	      		<c:when test="${subOrderSearch.sort == 'poStatus DESC'}">
			    	        		<th class="text-nowrap" data-type="html" data-breakpoints="xs">
										<a class="hdrLink" href="#" onClick="document.getElementById('sort').value='poStatus';document.getElementById('list').submit()"><fmt:message key="poAvailable" /></a> 
				    	        	</th>
			    	     		</c:when>
			    	      		<c:when test="${subOrderSearch.sort == 'poStatus'}">
			    	        		<th class="text-nowrap" data-type="html" data-breakpoints="xs">
										<a class="hdrLink" href="#" onClick="document.getElementById('sort').value='poStatus DESC';document.getElementById('list').submit()"><fmt:message key="poAvailable" /></a> 
				    	        	</th>
			    	     		</c:when>
			    	      		<c:otherwise>
					    	        <th class="text-nowrap" data-type="html" data-breakpoints="xs">
							    	 	<a class="hdrLink" href="#" onClick="document.getElementById('sort').value='poStatus DESC';document.getElementById('list').submit()"><fmt:message key="poAvailable" /></a> 
				    	      		</th>
			    	      		</c:otherwise>
			    	    	</c:choose>
		    			</c:if>
		    			<c:if test="${fn:contains(siteConfig['CUSTOMIZE_ORDER_LIST'].value,'orderNumber')}">
		    				<c:set var="cols" value="${cols+1}"/>
							<c:choose>
			    	      		<c:when test="${subOrderSearch.sort == 'order_id DESC'}">
			    	        		<th class="text-nowrap" data-type="html">
										<a class="hdrLink" href="#" onClick="document.getElementById('sort').value='order_id';document.getElementById('list').submit()"><fmt:message key="${orderName}Number" /></a> 
				    	        	</th>
			    	     		</c:when>
			    	      		<c:when test="${subOrderSearch.sort == 'order_id'}">
			    	        		<th class="text-nowrap" data-type="html">
										<a class="hdrLink" href="#" onClick="document.getElementById('sort').value='order_id DESC';document.getElementById('list').submit()"><fmt:message key="${orderName}Number" /></a> 
				    	        	</th>
			    	     		</c:when>
			    	      		<c:otherwise>
					    	        <th class="text-nowrap" data-type="html">
							    	 	<a class="hdrLink" href="#" onClick="document.getElementById('sort').value='order_id DESC';document.getElementById('list').submit()"><fmt:message key="${orderName}Number" /></a> 
				    	      		</th>
			    	      		</c:otherwise>
			    	    	</c:choose>
		    	    	</c:if>
		    	    	
		    	    	<c:if test="${fn:contains(siteConfig['CUSTOMIZE_ORDER_LIST'].value,'extOrderId')}">
		    	    		<c:set var="cols" value="${cols+1}"/>
		    	    		<c:choose>
			    	      		<c:when test="${subOrderSearch.sort == 'external_order_id DESC'}">
			    	        		<th class="text-nowrap" data-type="html">
										<a class="hdrLink" href="#" onClick="document.getElementById('sort').value='external_order_id';document.getElementById('list').submit()"><fmt:message key="externalOrderId" /></a> 
				    	        	</th>
			    	     		</c:when>
			    	      		<c:when test="${subOrderSearch.sort == 'external_order_id'}">
			    	        		<th class="text-nowrap" data-type="html">
										<a class="hdrLink" href="#" onClick="document.getElementById('sort').value='external_order_id DESC';document.getElementById('list').submit()"><fmt:message key="externalOrderId" /></a> 
				    	        	</th>
			    	     		</c:when>
			    	      		<c:otherwise>
					    	        <th class="text-nowrap" data-type="html">
							    	 	<a class="hdrLink" href="#" onClick="document.getElementById('sort').value='external_order_id DESC';document.getElementById('list').submit()"><fmt:message key="externalOrderId" /></a> 
				    	      		</th>
			    	      		</c:otherwise>
			    	    	</c:choose>
		    	    	</c:if>
		    	    	  
		    	    	<c:if test="${fn:contains(siteConfig['CUSTOMIZE_ORDER_LIST'].value,'mas200') and orderName != 'quotes'}">	
		    	    		<c:set var="cols" value="${cols+1}"/>
		    	    		<c:choose>
			    	      		<c:when test="${subOrderSearch.sort == 'mas200_orderno DESC'}">
			    	        		<th class="text-nowrap" data-type="html">
										<a class="hdrLink" href="#" onClick="document.getElementById('sort').value='mas200_orderno';document.getElementById('list').submit()">Mas200</a> 
				    	        	</th>
			    	     		</c:when>
			    	      		<c:when test="${subOrderSearch.sort == 'mas200_orderno'}">
			    	        		<th class="text-nowrap" data-type="html">
										<a class="hdrLink" href="#" onClick="document.getElementById('sort').value='mas200_orderno DESC';document.getElementById('list').submit()">Mas200</a> 
				    	        	</th>
			    	     		</c:when>
			    	      		<c:otherwise>
					    	        <th class="text-nowrap" data-type="html">
							    	 	<a class="hdrLink" href="#" onClick="document.getElementById('sort').value='mas200_orderno DESC';document.getElementById('list').submit()">Mas200</a> 
				    	      		</th>
			    	      		</c:otherwise>
			    	    	</c:choose>
		    	    	</c:if>
		    	    	
		    	    	<c:if test="${gSiteConfig['gSERVICE'] and fn:contains(siteConfig['CUSTOMIZE_ORDER_LIST'].value,'workOrder') and orderName != 'quotes'}">
		    	    		<c:set var="cols" value="${cols+1}"/>
		    	    		<c:choose>
			    	      		<c:when test="${subOrderSearch.sort == 'work_order_num DESC'}">
			    	        		<th class="text-nowrap" data-type="html">
										<a class="hdrLink" href="#" onClick="document.getElementById('sort').value='work_order_num';document.getElementById('list').submit()"><fmt:message key="workOrder" /></a> 
				    	        	</th>
			    	     		</c:when>
			    	      		<c:when test="${subOrderSearch.sort == 'work_order_num'}">
			    	        		<th class="text-nowrap" data-type="html">
										<a class="hdrLink" href="#" onClick="document.getElementById('sort').value='work_order_num DESC';document.getElementById('list').submit()"><fmt:message key="workOrder" /></a> 
				    	        	</th>
			    	     		</c:when>
			    	      		<c:otherwise>
					    	        <th class="text-nowrap" data-type="html">
							    	 	<a class="hdrLink" href="#" onClick="document.getElementById('sort').value='work_order_num DESC';document.getElementById('list').submit()"><fmt:message key="workOrder" /></a> 
				    	      		</th>
			    	      		</c:otherwise>
			    	    	</c:choose>
		    	    	</c:if>
		    	    	
		    	    	<c:if test="${(fn:contains(siteConfig['CUSTOMIZE_ORDER_LIST'].value,'po')) and orderName != 'quotes'}">
		    	    		<c:set var="cols" value="${cols+1}"/>
		    	    		<c:choose>
				    	      <c:when test="${subOrderSearch.sort == 'purchase_order DESC'}"> 
				    	      	<th class="text-nowrap" data-type="html">
				    	        <a class="hdrLink" href="#" onClick="document.getElementById('sort').value='purchase_order';document.getElementById('list').submit()">P.O.</a> 
				    	        </th>
				    	      </c:when>
				    	      <c:when test="${subOrderSearch.sort == 'purchase_order'}"> 
				    	      	<th class="text-nowrap" data-type="html">
				    	        <a class="hdrLink" href="#" onClick="document.getElementById('sort').value='purchase_order DESC';document.getElementById('list').submit()">P.O.</a> 
				    	        </th>
				    	      </c:when>
				    	      <c:otherwise> 
				    	      	<th class="text-nowrap" data-type="html">
				    	        <a class="hdrLink" href="#" onClick="document.getElementById('sort').value='purchase_order DESC';document.getElementById('list').submit()">P.O.</a> 
				    	        </th>
				    	      </c:otherwise>
				    	    </c:choose>  
		    	    	</c:if>
		    	    	
		    	    	<c:if test="${fn:contains(siteConfig['CUSTOMIZE_ORDER_LIST'].value,'pushedToQB')}">
					    	<c:if test="${siteConfig['QUICKBOOKS_INTEGRATION'].value == 'true'}">
						    	<c:set var="cols" value="${cols+1}"/>
						    	<th class="text-nowrap" data-type="html">
									<fmt:message key="pushedToQB" />
								</th>
							</c:if>
						</c:if>
						
		    			<c:if test="${fn:contains(siteConfig['CUSTOMIZE_ORDER_LIST'].value,'dateOrdered')}">
		    				<c:set var="cols" value="${cols+1}"/>
							<c:choose>
			    	      		<c:when test="${subOrderSearch.sort == 'date_ordered DESC'}">
			    	        		<th class="text-nowrap" data-type="html" data-breakpoints="xs sm">
										<a class="hdrLink" href="#" onClick="document.getElementById('sort').value='date_ordered';document.getElementById('list').submit()"><fmt:message key="orderDate" /></a> 
				    	        	</th>
			    	     		</c:when>
			    	      		<c:when test="${subOrderSearch.sort == 'date_ordered'}">
			    	        		<th class="text-nowrap" data-type="html" data-breakpoints="xs sm">
										<a class="hdrLink" href="#" onClick="document.getElementById('sort').value='date_ordered DESC';document.getElementById('list').submit()"><fmt:message key="orderDate" /></a> 
				    	        	</th>
			    	     		</c:when>
			    	      		<c:otherwise>
					    	        <th class="text-nowrap" data-type="html" data-breakpoints="xs sm">
							    	 	<a class="hdrLink" href="#" onClick="document.getElementById('sort').value='date_ordered DESC';document.getElementById('list').submit()"><fmt:message key="orderDate" /></a> 
				    	      		</th>
			    	      		</c:otherwise>
			    	    	</c:choose>
		    			</c:if>
		    			
		    			<c:if test="${fn:contains(siteConfig['CUSTOMIZE_ORDER_LIST'].value,'userDate')}"> 
		    				<c:set var="cols" value="${cols+1}"/>
		    				<th class="text-nowrap" data-type="html" data-breakpoints="xs sm">
					    	    <c:choose>
					    	      <c:when test="${subOrderSearch.sort == 'user_due_date DESC'}"> 
					    	        <a class="hdrLink" href="#" onClick="document.getElementById('sort').value='user_due_date';document.getElementById('list').submit()"><fmt:message key="userExpectedDueDate" /></a>
					    	       
					    	      </c:when>
					    	      <c:when test="${subOrderSearch.sort == 'user_due_date'}"> 
					    	        <a class="hdrLink" href="#" onClick="document.getElementById('sort').value='user_due_date DESC';document.getElementById('list').submit()"><fmt:message key="userExpectedDueDate" /></a>
					    	       
					    	      </c:when>
					    	      <c:otherwise> 
					    	        <a class="hdrLink" href="#" onClick="document.getElementById('sort').value='user_due_date DESC';document.getElementById('list').submit()"><fmt:message key="userExpectedDueDate" /></a>
					    	     
					    	      </c:otherwise>
					    	    </c:choose>   
				    	    </th>
						</c:if>
						
						<c:if test="${fn:contains(siteConfig['CUSTOMIZE_ORDER_LIST'].value,'cancelDate')}"> 
							<c:set var="cols" value="${cols+1}"/>
							<th class="text-nowrap" data-type="html" data-breakpoints="xs sm">
					    	    <c:choose>
				    	      		<c:when test="${subOrderSearch.sort == 'requested_cancel_date DESC'}"> 
						    	        <a class="hdrLink" href="#" onClick="document.getElementById('sort').value='requested_cancel_date';document.getElementById('list').submit()"><fmt:message key="requestedCancelDate" /></a>
						    	      
						    	      </c:when>
						    	      <c:when test="${subOrderSearch.sort == 'requested_cancel_date'}"> 
						    	        <a class="hdrLink" href="#" onClick="document.getElementById('sort').value='requested_cancel_date DESC';document.getElementById('list').submit()"><fmt:message key="requestedCancelDate" /></a>
						    	       
						    	      </c:when>
						    	      <c:otherwise> 
						    	        <a class="hdrLink" href="#" onClick="document.getElementById('sort').value='requested_cancel_date DESC';document.getElementById('list').submit()"><fmt:message key="requestedCancelDate" /></a>
						    	    
						    	      </c:otherwise>
				    	    	</c:choose>   
				    	    </th>
						</c:if>
						    
					    <c:if test="${fn:contains(siteConfig['CUSTOMIZE_ORDER_LIST'].value,'description')}"> 
					    	<c:set var="cols" value="${cols+1}"/>
					    	<th class="text-nowrap" data-type="html" data-breakpoints="xs sm">
					    	    <c:choose>
					    	      <c:when test="${subOrderSearch.sort == 'description DESC'}"> 
					    	        <a class="hdrLink" href="#" onClick="document.getElementById('sort').value='description';document.getElementById('list').submit()"><fmt:message key="description" /></a>
					    	       
					    	      </c:when>
					    	      <c:when test="${subOrderSearch.sort == 'description'}"> 
					    	        <a class="hdrLink" href="#" onClick="document.getElementById('sort').value='description DESC';document.getElementById('list').submit()"><fmt:message key="description" /></a>
					    	       
					    	      </c:when>
					    	      <c:otherwise> 
					    	        <a class="hdrLink" href="#" onClick="document.getElementById('sort').value='description DESC';document.getElementById('list').submit()"><fmt:message key="description" /></a>
					    	    
					    	      </c:otherwise>
					    	    </c:choose>   
			    	    	</th>
					    </c:if>
						    
						<c:if test="${fn:contains(siteConfig['CUSTOMIZE_ORDER_LIST'].value,'subTotal')}">
			    			<c:choose>
			    	      		<c:when test="${subOrderSearch.sort == 'sub_total DESC'}">
			    	        		<th class="text-nowrap" data-type="html" data-breakpoints="xs sm">
										<a class="hdrLink" href="#" onClick="document.getElementById('sort').value='sub_total';document.getElementById('list').submit()"><fmt:message key="subTotal" /></a> 
				    	        	</th>
			    	     		</c:when>
			    	      		<c:when test="${subOrderSearch.sort == 'sub_total'}">
			    	        		<th class="text-nowrap" data-type="html" data-breakpoints="xs sm">
										<a class="hdrLink" href="#" onClick="document.getElementById('sort').value='sub_total DESC';document.getElementById('list').submit()"><fmt:message key="subTotal" /></a> 
				    	        	</th>
			    	     		</c:when>
			    	      		<c:otherwise>
					    	        <th class="text-nowrap" data-type="html" data-breakpoints="xs sm">
							    	 	<a class="hdrLink" href="#" onClick="document.getElementById('sort').value='sub_total DESC';document.getElementById('list').submit()"><fmt:message key="subTotal" /></a> 
				    	      		</th>
			    	      		</c:otherwise>
			    	    	</c:choose>
		    			</c:if>
		    			
		    			<c:if test="${fn:contains(siteConfig['CUSTOMIZE_ORDER_LIST'].value,'productDiscount')}"> 
		    				<th class="text-nowrap" data-type="html" data-breakpoints="xs sm">
					    	    <c:choose>
					    	      <c:when test="${subOrderSearch.sort == 'promo_discount DESC'}"> 
					    	        <a class="hdrLink" href="#" onClick="document.getElementById('sort').value='promo_discount';document.getElementById('list').submit()">Product Discount</a> 
					    	      </c:when>
					    	      <c:when test="${subOrderSearch.sort == 'promo_discount'}"> 
					    	        <a class="hdrLink" href="#" onClick="document.getElementById('sort').value='promo_discount DESC';document.getElementById('list').submit()">Product Discount</a> 
					    	      </c:when>
					    	      <c:otherwise> 
					    	        <a class="hdrLink" href="#" onClick="document.getElementById('sort').value='promo_discount DESC';document.getElementById('list').submit()">Product Discount</a> 
					    	      </c:otherwise>
					    	    </c:choose>  
				    	    </th>
				    	</c:if>
				    	
					    <c:if test="${fn:contains(siteConfig['CUSTOMIZE_ORDER_LIST'].value,'shippingCost')}"> 
					    	<th class="text-nowrap" data-type="html" data-breakpoints="xs sm">
					    	    <c:choose>
					    	      <c:when test="${subOrderSearch.sort == 'shipping_cost DESC'}"> 
					    	        <a class="hdrLink" href="#" onClick="document.getElementById('sort').value='shipping_cost';document.getElementById('list').submit()">Shipping Cost</a> 
					    	      </c:when>
					    	      <c:when test="${subOrderSearch.sort == 'shipping_cost'}"> 
					    	        <a class="hdrLink" href="#" onClick="document.getElementById('sort').value='shipping_cost DESC';document.getElementById('list').submit()">Shipping Cost</a> 
					    	      </c:when>
					    	      <c:otherwise> 
					    	        <a class="hdrLink" href="#" onClick="document.getElementById('sort').value='shipping_cost DESC';document.getElementById('list').submit()">Shipping Cost</a> 
					    	      </c:otherwise>
					    	    </c:choose>   
				    	    </th>
					    </c:if>
					    
					    <c:if test="${fn:contains(siteConfig['CUSTOMIZE_ORDER_LIST'].value,'postageDiscount')}"> 
					    	<th class="text-nowrap" data-type="html" data-breakpoints="xs sm">
					    	    <c:choose>
					    	      <c:when test="${subOrderSearch.sort == 'promo_discount DESC'}"> 
					    	        <a class="hdrLink" href="#" onClick="document.getElementById('sort').value='promo_discount';document.getElementById('list').submit()">Postage Discount</a> 
					    	      </c:when>
					    	      <c:when test="${subOrderSearch.sort == 'promo_discount'}"> 
					    	        <a class="hdrLink" href="#" onClick="document.getElementById('sort').value='promo_discount DESC';document.getElementById('list').submit()">Postage Discount</a> 
					    	      </c:when>
					    	      <c:otherwise> 
					    	        <a class="hdrLink" href="#" onClick="document.getElementById('sort').value='promo_discount DESC';document.getElementById('list').submit()">Postage Discount</a> 
					    	      </c:otherwise>
					    	    </c:choose>   
				    	    </th>
					    </c:if>
		    			
						<c:if test="${fn:contains(siteConfig['CUSTOMIZE_ORDER_LIST'].value,'grandTotal')}">
			   				<c:choose>
			    	      		<c:when test="${subOrderSearch.sort == 'grand_total DESC'}">
			    	        		<th class="text-nowrap" data-type="html" data-breakpoints="xs sm">
										<a class="hdrLink" href="#" onClick="document.getElementById('sort').value='grand_total';document.getElementById('list').submit()"><fmt:message key="grandTotal" /></a> 
				    	        	</th>
			    	     		</c:when>
			    	      		<c:when test="${subOrderSearch.sort == 'grand_total'}">
			    	        		<th class="text-nowrap" data-type="html" data-breakpoints="xs sm">
										<a class="hdrLink" href="#" onClick="document.getElementById('sort').value='grand_total DESC';document.getElementById('list').submit()"><fmt:message key="grandTotal" /></a> 
				    	        	</th>
			    	     		</c:when>
			    	      		<c:otherwise>
					    	        <th class="text-nowrap" data-type="html" data-breakpoints="xs sm">
							    	 	<a class="hdrLink" href="#" onClick="document.getElementById('sort').value='grand_total DESC';document.getElementById('list').submit()"><fmt:message key="grandTotal" /></a> 
				    	      		</th>
			    	      		</c:otherwise>
			    	    	</c:choose>
		    			</c:if>

						    
						<c:if test="${gSiteConfig['gPAYMENTS'] and fn:contains(siteConfig['CUSTOMIZE_ORDER_LIST'].value,'amountPaid') and orderName != 'quotes'}"> 
							<th class="text-nowrap" data-type="html" data-breakpoints="xs sm">
					    	    <c:choose>
					    	      <c:when test="${subOrderSearch.sort == 'amount_paid DESC'}"> 
					    	        <a class="hdrLink" href="#" onClick="document.getElementById('sort').value='amount_paid';document.getElementById('list').submit()"><fmt:message key="amountPaid" /></a>
					    	        
					    	      </c:when>
					    	      <c:when test="${subOrderSearch.sort == 'amount_paid'}"> 
					    	        <a class="hdrLink" href="#" onClick="document.getElementById('sort').value='amount_paid DESC';document.getElementById('list').submit()"><fmt:message key="amountPaid" /></a>
					    	         
					    	      </c:when>
					    	      <c:otherwise> 
					    	        <a class="hdrLink" href="#" onClick="document.getElementById('sort').value='amount_paid DESC';document.getElementById('list').submit()"><fmt:message key="amountPaid" /></a>
					    	        
					    	      </c:otherwise>
					    	    </c:choose>   
				    	    </th>
						</c:if>
									    			
		    			<c:if test="${fn:contains(siteConfig['CUSTOMIZE_ORDER_LIST'].value,'echoSign')}">
		    				<c:set var="right_cols" value="${right_cols + 1 }"/>
		    				<th class="text-nowrap" data-type="html" data-breakpoints="xs sm">
						    	EchoSign 
					    	</th>
					    </c:if>
					    
						<c:if test="${fn:contains(siteConfig['CUSTOMIZE_ORDER_LIST'].value,'linkShare')}"> 
							<c:set var="right_cols" value="${right_cols + 1 }"/>
							<th class="text-nowrap" data-type="html" data-breakpoints="xs sm">
					    	    <c:choose>
					    	      <c:when test="${subOrderSearch.sort == 'ls_date_entered DESC'}"> 
					    	        <a class="hdrLink" href="#" onClick="document.getElementById('sort').value='ls_date_entered';document.getElementById('list').submit()">LinkShare</a> 
					    	      </c:when>
					    	      <c:when test="${subOrderSearch.sort == 'ls_date_entered'}"> 
					    	        <a class="hdrLink" href="#" onClick="document.getElementById('sort').value='ls_date_entered DESC';document.getElementById('list').submit()">LinkShare</a> 
					    	      </c:when>
					    	      <c:otherwise> 
					    	        <a class="hdrLink" href="#" onClick="document.getElementById('sort').value='ls_date_entered DESC';document.getElementById('list').submit()">LinkShare</a> 
					    	      </c:otherwise>
					    	    </c:choose>   
				    	    </th>
					    </c:if>	
				    
					    <c:if test="${fn:contains(siteConfig['CUSTOMIZE_ORDER_LIST'].value,'orderType')}"> 
					    	<c:set var="right_cols" value="${right_cols + 1 }"/>
					    	<th class="text-nowrap" data-type="html" data-breakpoints="xs sm">
					    	    <c:choose>
					    	      <c:when test="${subOrderSearch.sort == 'order_type DESC'}"> 
					    	        <a class="hdrLink" href="#" onClick="document.getElementById('sort').value='order_type';document.getElementById('list').submit()"><fmt:message key="${orderName}Type" /></a>
				    	          </c:when>
					    	      <c:when test="${subOrderSearch.sort == 'order_type'}"> 
					    	        <a class="hdrLink" href="#" onClick="document.getElementById('sort').value='order_type DESC';document.getElementById('list').submit()"><fmt:message key="${orderName}Type" /></a>
				    	          </c:when>
					    	      <c:otherwise> 
					    	        <a class="hdrLink" href="#" onClick="document.getElementById('sort').value='order_type DESC';document.getElementById('list').submit()"><fmt:message key="${orderName}Type" /></a>
					    	      </c:otherwise>
					    	    </c:choose>   
				    	    </th>
					    </c:if>
				    
					    <c:if test="${(fn:contains(siteConfig['CUSTOMIZE_ORDER_LIST'].value,'shippingTitle')) and orderName != 'quotes'}">
					    	<c:set var="right_cols" value="${right_cols + 1 }"/>
							<th class="text-nowrap" data-type="html" data-breakpoints="xs sm">     
					    	    <c:choose>
					    	      <c:when test="${subOrderSearch.sort == 'shipping_method DESC'}"> 
					    	        <a class="hdrLink" href="#" onClick="document.getElementById('sort').value='shipping_method';document.getElementById('list').submit()"><fmt:message key="shippingTitle" /></a>
					    	        
					    	      </c:when>
					    	      <c:when test="${subOrderSearch.sort == 'shipping_method'}"> 
					    	        <a class="hdrLink" href="#" onClick="document.getElementById('sort').value='shipping_method DESC';document.getElementById('list').submit()"><fmt:message key="shippingTitle" /></a>
					    	     
					    	      </c:when>
					    	      <c:otherwise> 
					    	        <a class="hdrLink" href="#" onClick="document.getElementById('sort').value='shipping_method DESC';document.getElementById('list').submit()"><fmt:message key="shippingTitle" /></a>
					    	        
					    	      </c:otherwise>
					    	    </c:choose>   
		    	   	 		</th> 
					    </c:if>
					    
						<c:if test="${fn:contains(siteConfig['CUSTOMIZE_ORDER_LIST'].value,'paymentMethod') and orderName != 'quotes'}"> 
							<c:set var="right_cols" value="${right_cols + 1 }"/>
							<th class="text-nowrap" data-type="html" data-breakpoints="xs sm">
					    	    <c:choose>
					    	      <c:when test="${subOrderSearch.sort == 'payment_method DESC'}"> 
					    	        <a class="hdrLink" href="#" onClick="document.getElementById('sort').value='payment_method';document.getElementById('list').submit()"><fmt:message key="paymentMethod" /></a>
					    	      
					    	      </c:when>
					    	      <c:when test="${subOrderSearch.sort == 'payment_method'}"> 
					    	        <a class="hdrLink" href="#" onClick="document.getElementById('sort').value='payment_method DESC';document.getElementById('list').submit()"><fmt:message key="paymentMethod" /></a>
					    	      </c:when>
					    	      <c:otherwise> 
					    	        <a class="hdrLink" href="#" onClick="document.getElementById('sort').value='payment_method DESC';document.getElementById('list').submit()"><fmt:message key="paymentMethod" /></a>
					    	       
					    	      </c:otherwise>
					    	    </c:choose>   
				    	    </th>
					    </c:if>
						    
		    			<c:if test="${gSiteConfig['gINVOICE_APPROVAL'] and fn:contains(siteConfig['CUSTOMIZE_ORDER_LIST'].value,'approval')}">
		    				<c:set var="right_cols" value="${right_cols + 1 }"/>
			    			<c:choose>
			    	      		<c:when test="${orderSearch.sort == 'approval DESC'}">
			    	        		<th class="text-nowrap" data-type="html" data-breakpoints="xs sm md lg">
										<a class="hdrLink" href="#" onClick="document.getElementById('sort').value='approval';document.getElementById('list').submit()"><fmt:message key="approval" /></a> 
				    	        	</th>
			    	     		</c:when>
			    	      		<c:when test="${orderSearch.sort == 'approval'}">
			    	        		<th class="text-nowrap" data-type="html" data-breakpoints="xs sm md lg">
										<a class="hdrLink" href="#" onClick="document.getElementById('sort').value='approval DESC';document.getElementById('list').submit()"><fmt:message key="approval" /></a> 
				    	        	</th>
			    	     		</c:when>
			    	      		<c:otherwise>
					    	        <th class="text-nowrap" data-type="html" data-breakpoints="xs sm md lg">
							    	 	<a class="hdrLink" href="#" onClick="document.getElementById('sort').value='approval DESC';document.getElementById('list').submit()"><fmt:message key="approval" /></a> 
				    	      		</th>
			    	      		</c:otherwise>
			    	    	</c:choose>
		    			</c:if>
		    			
						<c:if test="${fn:contains(siteConfig['CUSTOMIZE_ORDER_LIST'].value,'status')}">
							<c:set var="right_cols" value="${right_cols + 1 }"/>
			    			<c:choose>
			    	      		<c:when test="${orderSearch.sort == 'status DESC'}">
			    	        		<th class="text-nowrap" data-type="html" data-breakpoints="xs sm md lg">
										<a class="hdrLink" href="#" onClick="document.getElementById('sort').value='status';document.getElementById('list').submit()"><fmt:message key="status" /></a> 
				    	        	</th>
			    	     		</c:when>
			    	      		<c:when test="${orderSearch.sort == 'status'}">
			    	        		<th class="text-nowrap" data-type="html" data-breakpoints="xs sm md lg">
										<a class="hdrLink" href="#" onClick="document.getElementById('sort').value='status DESC';document.getElementById('list').submit()"><fmt:message key="status" /></a> 
				    	        	</th>
			    	     		</c:when>
			    	      		<c:otherwise>
					    	        <th class="text-nowrap" data-type="html" data-breakpoints="xs sm md lg">
							    	 	<a class="hdrLink" href="#" onClick="document.getElementById('sort').value='status DESC';document.getElementById('list').submit()"><fmt:message key="status" /></a> 
				    	      		</th>
			    	      		</c:otherwise>
			    	    	</c:choose>
		    			</c:if>
		    			
		    			<c:if test="${siteConfig['SUB_STATUS'].value != '0' and fn:contains(siteConfig['CUSTOMIZE_ORDER_LIST'].value,'subStatus')}">
		    				<c:set var="right_cols" value="${right_cols + 1 }"/>
			    			<c:choose>
			    	      		<c:when test="${orderSearch.sort == 'sub_status DESC'}">
			    	        		<th class="text-nowrap" data-type="html" data-breakpoints="xs sm md lg">
										<a class="hdrLink" href="#" onClick="document.getElementById('sort').value='sub_status';document.getElementById('list').submit()"><fmt:message key="subStatus" /></a> 
				    	        	</th>
			    	     		</c:when>
			    	      		<c:when test="${orderSearch.sort == 'sub_status'}">
			    	        		<th class="text-nowrap" data-type="html" data-breakpoints="xs sm md lg">
										<a class="hdrLink" href="#" onClick="document.getElementById('sort').value='sub_status DESC';document.getElementById('list').submit()"><fmt:message key="subStatus" /></a> 
				    	        	</th>
			    	     		</c:when>
			    	      		<c:otherwise>
					    	        <th class="text-nowrap" data-type="html" data-breakpoints="xs sm md lg">
							    	 	<a class="hdrLink" href="#" onClick="document.getElementById('sort').value='sub_status DESC';document.getElementById('list').submit()"><fmt:message key="subStatus" /></a> 
				    	      		</th>
			    	      		</c:otherwise>
			    	    	</c:choose>
		    			</c:if>
		    			
	    				<c:if test="${siteConfig['SUB_STATUS'].value != '0' and fn:contains(siteConfig['CUSTOMIZE_ORDER_LIST'].value,'ssGroup')}"> 
	    					<c:set var="right_cols" value="${right_cols + 1 }"/>
	    					<th class="text-nowrap" data-type="html" data-breakpoints="xs sm md lg">
				    	    	<a class="hdrLink" href="#"><fmt:message key="subStatus" /> <fmt:message key="group" /></a> 
				    	    </th>
				    	</c:if>
				    	
					    <c:if test="${fn:contains(siteConfig['CUSTOMIZE_ORDER_LIST'].value,'statusMessage')}"> 
					    	<c:set var="right_cols" value="${right_cols + 1 }"/>
					    	<th class="text-nowrap" data-type="html" data-breakpoints="xs sm md lg">
				    	    	<a class="hdrLink" href="#"><fmt:message key="message" /></a> 
			    	    	</th>
					    </c:if>
					    
					    <c:if test="${fn:contains(siteConfig['CUSTOMIZE_ORDER_LIST'].value,'nextActivityDate')}">
					    	<c:set var="right_cols" value="${right_cols + 1 }"/>
							<th class="text-nowrap" data-type="html" data-breakpoints="xs sm md lg">
					    	    <c:choose>
					    	      <c:when test="${subOrderSearch.sort == 'next_activity_date DESC'}"> 
					    	        <a class="hdrLink" href="#" onClick="document.getElementById('sort').value='next_activity_date';document.getElementById('list').submit()"><fmt:message key="nextActivityDate" /></a>
					    	       </c:when>
					    	      <c:when test="${subOrderSearch.sort == 'next_activity_date'}"> 
					    	        <a class="hdrLink" href="#" onClick="document.getElementById('sort').value='next_activity_date DESC';document.getElementById('list').submit()"><fmt:message key="nextActivityDate" /></a>
					    	      </c:when>
					    	      <c:otherwise> 
					    	        <a class="hdrLink" href="#" onClick="document.getElementById('sort').value='next_activity_date DESC';document.getElementById('list').submit()"><fmt:message key="nextActivityDate" /></a>
					    	      </c:otherwise>
					    	    </c:choose>  
							</th>
					    </c:if>
					    
					    <c:if test="${fn:contains(siteConfig['CUSTOMIZE_ORDER_LIST'].value,'deliveryPerson') and orderName != 'quotes'}">
					    	<c:set var="right_cols" value="${right_cols + 1 }"/>
								<th class="text-nowrap" data-type="html" data-breakpoints="xs sm md lg">
					    	    <c:choose>
					    	      <c:when test="${subOrderSearch.sort == 'delivery_person DESC'}"> 
					    	        <a class="hdrLink" href="#" onClick="document.getElementById('sort').value='delivery_person';document.getElementById('list').submit()"><fmt:message key="deliveryPerson" /></a>
					    	      </c:when>
					    	      <c:when test="${subOrderSearch.sort == 'delivery_person'}"> 
					    	        <a class="hdrLink" href="#" onClick="document.getElementById('sort').value='delivery_person DESC';document.getElementById('list').submit()"><fmt:message key="deliveryPerson" /></a>
					    	      </c:when>
					    	      <c:otherwise> 
					    	        <a class="hdrLink" href="#" onClick="document.getElementById('sort').value='delivery_person DESC';document.getElementById('list').submit()"><fmt:message key="deliveryPerson" /></a>
					    	      </c:otherwise>
					    	    </c:choose>  
								</th>
					    </c:if>
						    
		    			<c:if test="${fn:contains(siteConfig['CUSTOMIZE_ORDER_LIST'].value,'dateShipped')}">
		    				<c:set var="right_cols" value="${right_cols + 1 }"/>
			    			<th class="text-nowrap" data-type="html" data-breakpoints="xs sm md lg">
								<fmt:message key="dateShipped" />
							</th>
						</c:if>
						
						<c:if test="${fn:contains(siteConfig['CUSTOMIZE_ORDER_LIST'].value,'trackCode')}">
							<c:set var="right_cols" value="${right_cols + 1 }"/>
			    			<th class="text-nowrap" data-type="html" data-breakpoints="xs sm md lg">
								<c:choose>
					    	      <c:when test="${subOrderSearch.sort == 'trackcode DESC'}"> 
					    	        <a class="hdrLink" href="#" onClick="document.getElementById('sort').value='trackcode';document.getElementById('list').submit()"><fmt:message key="trackcode" /></a>
					    	      </c:when>
					    	      <c:when test="${subOrderSearch.sort == 'trackcode'}"> 
					    	        <a class="hdrLink" href="#" onClick="document.getElementById('sort').value='trackcode DESC';document.getElementById('list').submit()"><fmt:message key="trackcode" /></a>
					    	      </c:when>
					    	      <c:otherwise> 
					    	        <a class="hdrLink" href="#" onClick="document.getElementById('sort').value='trackcode DESC';document.getElementById('list').submit()"><fmt:message key="trackcode" /></a>
					    	      </c:otherwise>
					    	    </c:choose> 
							</th>
						</c:if>
						
						<c:if test="${fn:contains(siteConfig['CUSTOMIZE_ORDER_LIST'].value,'salesRep')}">
							<c:set var="right_cols" value="${right_cols + 1 }"/>
			    			<th class="text-nowrap" data-type="html" data-breakpoints="xs sm md lg">
								<c:choose>
					    	      <c:when test="${subOrderSearch.sort == 'salesrep_id DESC'}"> 
					    	        <a class="hdrLink" href="#" onClick="document.getElementById('sort').value='salesrep_id';document.getElementById('list').submit()"><fmt:message key="salesRep" /></a>
					    	      </c:when>
					    	      <c:when test="${subOrderSearch.sort == 'salesrep_id'}"> 
					    	        <a class="hdrLink" href="#" onClick="document.getElementById('sort').value='salesrep_id DESC';document.getElementById('list').submit()"><fmt:message key="salesRep" /></a>
					    	      </c:when>
					    	      <c:otherwise> 
					    	        <a class="hdrLink" href="#" onClick="document.getElementById('sort').value='salesrep_id DESC';document.getElementById('list').submit()"><fmt:message key="salesRep" /></a>
					    	      </c:otherwise>
					    	    </c:choose>  
							</th>
						</c:if>
						
						<c:if test="${gSiteConfig['gMULTI_STORE'] > 0 and fn:contains(siteConfig['CUSTOMIZE_ORDER_LIST'].value,'multiStore')}">
							<c:set var="right_cols" value="${right_cols + 1 }"/>
			    			<th class="text-nowrap" data-type="html" data-breakpoints="xs sm md lg">
								<fmt:message key="multiStore" />
							</th>
						</c:if>
					</tr>
				</thead>
				
				<tbody>
				  	<c:set var="totalSubTotal" value="0.0" />
			 	 	<c:set var="productDiscountTotal" value="0.0" />
				  	<c:set var="shippingCostTotal" value="0.0" />
				  	<c:set var="postageDiscountTotal" value="0.0" />
				  	<c:set var="totalGrandTotal" value="0.0" />
				  	<c:set var="totalAmountPaid" value="0.0" />
				  	
					<c:forEach items="${model.orders}" var="order" varStatus="status">
					<tr >
						<td>
							<label class="checkbox-inline">
	                            <input type="checkbox" class="check" name="__selected_id" value="${status.index}"> <c:out value="${status.count + subOrderSearch.offset}"/>.
	                            <input name="__selected_order_${status.index}" id="__selected_order_${status.index}" value="${order.subOrderId}" type="hidden" />
	                        </label> 
						</td> 
						  
						<c:if test="${fn:contains(siteConfig['CUSTOMIZE_ORDER_LIST'].value,'billedTo')}">
			    			<td>
								<a href="../customers/customer.jhtm?id=${order.userId }" ><c:out value="${order.billing.firstName}"/>&nbsp;<c:out value="${order.billing.lastName}"/></a>			
							</td>
						</c:if>
						<c:if test="${fn:contains(siteConfig['CUSTOMIZE_ORDER_LIST'].value,'company')}">
			    			<td>
								<c:out value="${order.billing.company}"/>
							</td>
						</c:if>
						
						<c:if test="${fn:contains(siteConfig['CUSTOMIZE_ORDER_LIST'].value,'poStatus')}">
							<td>
								<c:choose>
								    <c:when test="${order.poStatus}"><img src="../graphics/checkbox.png" alt="Printed" title="Printed" border="0" class="quickMode"></c:when>
								    <c:otherwise><img src="../graphics/box.png" alt="Not Printed" title="Not Printed" border="0" class="quickMode"></c:otherwise>
							  	</c:choose>								
							</td>
						</c:if>
						
						<c:if test="${fn:contains(siteConfig['CUSTOMIZE_ORDER_LIST'].value,'orderNumber')}">
							<td>
								<a href="subOrder.jhtm?subOrder=${order.subOrderId}&orderName=${orderName}" class="nameLink"><c:out value="${order.subOrderId}"/>
								<span class="sup">
								    <c:choose>
									  <c:when test="${order.subscriptionCode != null}">&sup1;<c:set var="hasSubscription" value="true"/></c:when>
								    </c:choose>
							  	</span>
							</td>
						</c:if>
						
						<c:if test="${fn:contains(siteConfig['CUSTOMIZE_ORDER_LIST'].value,'mas200') and orderName != 'quotes'}">
						  <td><c:if test="${order.mas200orderNo != null}"><c:out value="${order.mas200orderNo}"/> (<c:out value="${order.mas200status}"/>)</c:if></td>
						</c:if>
						
					    <c:if test="${fn:contains(siteConfig['CUSTOMIZE_ORDER_LIST'].value,'extOrderId')}">
						  <td><c:if test="${order.externalOrderId != null}"><c:out value="${order.externalOrderId}"/></c:if></td>
					    </c:if>
					    
						<c:if test="${gSiteConfig['gSERVICE'] and fn:contains(siteConfig['CUSTOMIZE_ORDER_LIST'].value,'workOrder') and orderName != 'quotes'}">
						  <td>
						    <a href="../service/workOrder.jhtm?num=${order.workOrderNum}" class="nameLink"><c:out value="${order.workOrderNum}"/></a>
						    <c:if test="${order.workOrderNum != null}">
						    <c:if test="${model.serviceMap[order.workOrderNum] != null}"><a href="<c:url value="/assets/pdf/${model.serviceMap[order.workOrderNum].servicePdfUrl}"/>" target="_blank"><img src="../graphics/pdf.gif" border="0"/></a></c:if>
						    <c:if test="${model.serviceMap[order.workOrderNum] == null}"><a href="../service/pdf.jhtm?num=${order.workOrderNum}"/><img src="../graphics/pdf_light.gif" border="0"></a></c:if>
							</c:if>
						  </td>
						</c:if>
									
						<c:if test="${fn:contains(siteConfig['CUSTOMIZE_ORDER_LIST'].value,'po') and orderName != 'quotes'}"> 
						  <td><c:out value="${order.purchaseOrder}"/></td>
						</c:if>  
						
						<!-- For checking pushed_to_qb -->
						<c:if test="${fn:contains(siteConfig['CUSTOMIZE_ORDER_LIST'].value,'pushedToQB')}">
							<c:if test="${siteConfig['QUICKBOOKS_INTEGRATION'].value == 'true'}">
								<td>
									<c:choose>
								    <c:when test="${order.pushedToQb}"><img src="../graphics/checkbox.png" alt="Pushed" title="PushedToQb" border="0" class="quickMode"></c:when>
								    <c:otherwise><img src="../graphics/box.png" alt="Not Pushed" title="Not PushedToQb" border="0" class="quickMode"></c:otherwise>
								  </c:choose>
								</td>
							</c:if>
						</c:if>
						
						<c:if test="${fn:contains(siteConfig['CUSTOMIZE_ORDER_LIST'].value,'dateOrdered')}">
			    			<td >
								<fmt:formatDate type="date" timeStyle="default" pattern="${siteConfig['SITE_DATE_FORMAT'].value}" timeZone="${siteConfig['SITE_TIME_ZONE'].value}" value="${order.dateOrdered}"/>
			    			</td>
						</c:if>
						
					 	<c:if test="${fn:contains(siteConfig['CUSTOMIZE_ORDER_LIST'].value,'userDate')}">  
					      <td><c:if test="${order.userDueDate != null}"><fmt:formatDate type="date" timeStyle="default" pattern="${siteConfig['SITE_DATE_FORMAT'].value}" timeZone="${siteConfig['SITE_TIME_ZONE'].value}" value="${order.userDueDate}"/></c:if></td>
					    </c:if>
						<c:if test="${fn:contains(siteConfig['CUSTOMIZE_ORDER_LIST'].value,'cancelDate')}"> 
						  <td><c:if test="${order.requestedCancelDate != null}"><fmt:formatDate type="date" timeStyle="default" pattern="${siteConfig['SITE_DATE_FORMAT'].value}" timeZone="${siteConfig['SITE_TIME_ZONE'].value}" value="${order.requestedCancelDate}"/></c:if></td>
					    </c:if>
					    <c:if test="${fn:contains(siteConfig['CUSTOMIZE_ORDER_LIST'].value,'description')}">  
					      <td><c:out value="${order.description}"/></td>
					    </c:if>
					    <c:if test="${fn:contains(siteConfig['CUSTOMIZE_ORDER_LIST'].value,'subTotal')}">  			
						  <td><fmt:formatNumber value="${order.subTotal}" pattern="#,##0.00" /></td><c:set var="totalSubTotal" value="${order.subTotal + totalSubTotal}" />
						</c:if>
						
						<c:if test="${fn:contains(siteConfig['CUSTOMIZE_ORDER_LIST'].value,'productDiscount')}">
					   		<c:set var="linePromoTotal" value="0"/>
							<c:if test="${order.lineItemPromos != null and fn:length(order.lineItemPromos) gt 0}">
		    					<c:forEach items="${order.lineItemPromos}" var="itemPromo" varStatus="status">
		    						<c:if test="${itemPromo.value != 0}">
		    							<c:set var="linePromoTotal" value="${itemPromo.value + linePromoTotal}"/>
		    						</c:if>
		    					</c:forEach>
		    				</c:if>
							<td>
							<c:if test="${order.promo.discountType == null or order.promo.discountType == 'order' }">
								<c:choose>
									<c:when test="${order.promo.percent}">
						            	<fmt:formatNumber value="${(order.subTotal * ( order.promoAmount / 100.00 ))+linePromoTotal}" pattern="#,##0.00"/>
						            	<c:set var="productDiscountTotal" value="${(order.subTotal * ( order.promoAmount / 100.00 ))+linePromoTotal+ productDiscountTotal}" />
						          	</c:when>
						          	<c:otherwise>
						          		<c:choose>
						          			<c:when test="${(order.promoAmount + linePromoTotal) > order.subTotal}" >
						          				<fmt:formatNumber value="${order.subTotal}" pattern="#,##0.00"/>
						          				<c:set var="productDiscountTotal" value="${order.subTotal + productDiscountTotal}" />
						          			</c:when>
						          			<c:otherwise>
						            			<fmt:formatNumber value="${order.promoAmount + linePromoTotal}" pattern="#,##0.00"/>
						            			<c:set var="productDiscountTotal" value="${order.promoAmount + linePromoTotal+ productDiscountTotal}" />
						          			</c:otherwise>
						          		</c:choose>
						            </c:otherwise>
					            </c:choose>
							</c:if>
							<c:if test="${order.promo.discountType == 'shipping'}">
								  <fmt:formatNumber value="${linePromoTotal}" pattern="#,##0.00"/>
						          <c:set var="productDiscountTotal" value="${linePromoTotal + productDiscountTotal}" />
						    </c:if>      
							</td>
						</c:if>
	     
						<c:if test="${fn:contains(siteConfig['CUSTOMIZE_ORDER_LIST'].value,'shippingCost')}">
							<td><fmt:formatNumber value="${order.shippingCost}" pattern="#,##0.00" /></td>
							<c:set var="shippingCostTotal" value="${order.shippingCost + shippingCostTotal}" />
						</c:if>
						
						<c:if test="${fn:contains(siteConfig['CUSTOMIZE_ORDER_LIST'].value,'postageDiscount')}">
							<td>
								<c:if test="${order.promo.discountType == 'shipping'}">
								 <c:choose>
							          <c:when test="${order.promo.percent}">
							            <fmt:formatNumber value="${(order.shippingCost * ( order.promoAmount / 100.00 ))}" pattern="#,##0.00"/>
							            <c:set var="postageDiscountTotal" value="${(order.shippingCost * ( order.promoAmount / 100.00 ))+ postageDiscountTotal}"/>
							          </c:when>		     
						          	  <c:otherwise>
						          		<c:choose>
						          			<c:when test="${order.promoAmount > order.shippingCost}" >
						          				<fmt:formatNumber value="${order.shippingCost}" pattern="#,##0.00"/>
						          				<c:set var="postageDiscountTotal" value="${order.shippingCost + postageDiscountTotal}" />
						          			</c:when>
						          			<c:otherwise>
		                                        <fmt:formatNumber value="${order.promoAmount}" pattern="#,##0.00"/>
		                                        <c:set var="postageDiscountTotal" value="${order.promoAmount + postageDiscountTotal}"/>
						          			</c:otherwise>
						          		</c:choose>
						             </c:otherwise>
						      	</c:choose>	    
							  </c:if>
							  <c:if test="${order.promo.discountType != 'shipping'}">
							  		<fmt:formatNumber value="0.0" pattern="#,##0.00"/>
							  </c:if>
							</td>
						</c:if>
						  
						<c:if test="${fn:contains(siteConfig['CUSTOMIZE_ORDER_LIST'].value,'grandTotal')}">
			    			<td>
								<fmt:formatNumber value="${order.grandTotal}" pattern="#,##0.00" />
				    			<c:set var="totalGrandTotal" value="${order.grandTotal + totalGrandTotal}" />
							</td>
						</c:if>
						 
						<c:if test="${gSiteConfig['gPAYMENTS'] and fn:contains(siteConfig['CUSTOMIZE_ORDER_LIST'].value,'amountPaid') and orderName != 'quotes'}">
						  <td><c:choose><c:when test="${order.amountPaid != 0 and order.amountPaid != null}"><fmt:formatNumber value="${order.amountPaid}" pattern="#,##0.00" /></c:when><c:otherwise>&nbsp;</c:otherwise></c:choose></td><c:set var="totalAmountPaid" value="${order.amountPaid + totalAmountPaid}" />
						</c:if>
						
						<c:if test="${fn:contains(siteConfig['CUSTOMIZE_ORDER_LIST'].value,'echoSign')}">
					      <td><c:if test="${model.echoSign[order.userId] != null}"></c:if><a href="<c:out value="${model.echoSign[order.userId].documentUrl}"/>" target="_blank"><c:out value="${model.echoSign[order.userId].documentName}"/></a></td>
					    </c:if>
						<c:if test="${fn:contains(siteConfig['CUSTOMIZE_ORDER_LIST'].value,'linkShare')}">
						  <td>
						  <c:choose>
						    <c:when test="${order.linkShare != null}"><img src="../graphics/checkbox.png" alt="<c:out value="${order.linkShare.siteID}" />" title="<c:out value="${order.linkShare.siteID}" />" border="0"></c:when>
						    <c:otherwise><img src="../graphics/box.png" alt="" title="" border="0"></c:otherwise>
						  </c:choose>		
						  </td>		
					    </c:if>
						<c:if test="${fn:contains(siteConfig['CUSTOMIZE_ORDER_LIST'].value,'orderType')}">
						  <td><c:out value="${order.orderType}" /></td>
						</c:if>
						<c:if test="${fn:contains(siteConfig['CUSTOMIZE_ORDER_LIST'].value,'shippingTitle') and orderName != 'quotes'}">
						  <td><c:out value="${order.shippingMethod}" escapeXml="false" /></td>
						</c:if>
						
						<c:if test="${fn:contains(siteConfig['CUSTOMIZE_ORDER_LIST'].value,'paymentMethod') and orderName != 'quotes'}">
						  <td><c:out value="${order.paymentMethod}" /></td>
						</c:if> 
						
						<c:if test="${gSiteConfig['gINVOICE_APPROVAL'] and fn:contains(siteConfig['CUSTOMIZE_ORDER_LIST'].value,'approval')}">
							<td>
								<c:choose><c:when test="${!empty order.approval}"><fmt:message key="approvalStatus_${order.approval}" /></c:when><c:otherwise>&nbsp;</c:otherwise></c:choose>
							</td>
						</c:if>
						
						<c:if test="${fn:contains(siteConfig['CUSTOMIZE_ORDER_LIST'].value,'status')}">
						  <td><c:if test="${fn:contains(siteConfig['CUSTOMIZE_ORDER_LIST'].value,'priority') and not (order.status == 's' or fn:contains(order.status,'x'))}"><img src="../graphics/priority_${order.priority}.png" border="0"></c:if><c:choose><c:when test="${order.status != null}"><fmt:message key="orderStatus_${order.status}" /></c:when><c:otherwise>&nbsp;</c:otherwise></c:choose></td>
					    </c:if>
					    <c:if test="${siteConfig['SUB_STATUS'].value != '0' and fn:contains(siteConfig['CUSTOMIZE_ORDER_LIST'].value,'subStatus')}">
						  <td><c:choose><c:when test="${order.subStatus != null}"><c:out value="${model.orderSubStatusMap[order.subStatus]}" /></c:when><c:otherwise>&nbsp;</c:otherwise></c:choose></td>
					    </c:if>
					    <c:if test="${siteConfig['SUB_STATUS'].value != '0' and fn:contains(siteConfig['CUSTOMIZE_ORDER_LIST'].value,'ssGroup')}">
						  <td><c:choose><c:when test="${order.subStatus != null}"><c:out value="${order.subStatusGroup}" /></c:when><c:otherwise>&nbsp;</c:otherwise></c:choose></td>
					    </c:if>
					    <c:if test="${fn:contains(siteConfig['CUSTOMIZE_ORDER_LIST'].value,'statusMessage')}">
					      <td><c:choose><c:when test="${order.statusMessage != null}"><c:out value="${order.statusMessage}" /></c:when><c:otherwise>&nbsp;</c:otherwise></c:choose></td>
					    </c:if>
					    <c:if test="${fn:contains(siteConfig['CUSTOMIZE_ORDER_LIST'].value,'nextActivityDate')}">
					      <td><fmt:formatDate type="date" timeStyle="default" timeZone="${siteConfig['SITE_TIME_ZONE'].value}" value="${order.nextActivityDate}"/></td>
					    </c:if>
					    <c:if test="${fn:contains(siteConfig['CUSTOMIZE_ORDER_LIST'].value,'deliveryPerson') and orderName != 'quotes'}">
						  <td><c:out value="${order.deliveryPerson}"/>
						</c:if>	
						
						<c:if test="${fn:contains(siteConfig['CUSTOMIZE_ORDER_LIST'].value,'dateShipped')}">
					      <td>
					      <c:choose>
					       <c:when test="${subOrderSearch.dateType == 'orderDate'}">
					        <img class="quickMode" id="shipDateHelp${order.orderId}" src="../graphics/question.gif" onclick="showShipDate(${order.orderId})"/><div id="shipDateBlock${order.orderId}"></div>
					       </c:when>
					       <c:otherwise>
					        <fmt:formatDate type="date" timeStyle="default" timeZone="${siteConfig['SITE_TIME_ZONE'].value}" value="${order.shipped}"/>
					       </c:otherwise>
					      </c:choose>
					      </td>	
					    </c:if>
		    
						<c:if test="${fn:contains(siteConfig['CUSTOMIZE_ORDER_LIST'].value,'trackCode')}">
			    			<td>
								<c:out value="${order.trackcode}"/>
							</td>
						</c:if>
						
						<c:if test="${gSiteConfig['gSALES_REP'] and fn:contains(siteConfig['CUSTOMIZE_ORDER_LIST'].value,'salesRep')}">
					      	<td>
								<c:out value="${model.salesRepMap[order.salesRepId].name}"/>
							</td>
					    </c:if>
						<c:if test="${gSiteConfig['gMULTI_STORE'] > 0 and fn:contains(siteConfig['CUSTOMIZE_ORDER_LIST'].value,'multiStore')}">
				 			<td>
								<c:out value="${order.host}"/>
							</td>
						</c:if>
					</tr> 
					</c:forEach>
				 
					<tr>
					  <td ><b><fmt:message key="total" /></b></td>
					   
					  <c:forEach var="i" begin="1" end="${cols - 1}">
					  	<td></td>
					  </c:forEach> 
					  <td><fmt:formatNumber value="${totalSubTotal}" pattern="#,##0.00" /></td>
					  <c:if test="${fn:contains(siteConfig['CUSTOMIZE_ORDER_LIST'].value,'productDiscount')}">
					   <td><fmt:formatNumber value="${productDiscountTotal}" pattern="#,##0.00" /></td>
					  </c:if>
					  <c:if test="${fn:contains(siteConfig['CUSTOMIZE_ORDER_LIST'].value,'shippingCost')}">
					   <td><fmt:formatNumber value="${shippingCostTotal}" pattern="#,##0.00" /></td>
					  </c:if>
					  <c:if test="${fn:contains(siteConfig['CUSTOMIZE_ORDER_LIST'].value,'postageDiscount')}">
					   <td><fmt:formatNumber value="${postageDiscountTotal}" pattern="#,##0.00" /></td>
					  </c:if>
					  <td><fmt:formatNumber value="${totalGrandTotal}" pattern="#,##0.00" /></td>
					  <c:if test="${gSiteConfig['gPAYMENTS'] and fn:contains(siteConfig['CUSTOMIZE_ORDER_LIST'].value,'amountPaid')}">
					    <td><fmt:formatNumber value="${totalAmountPaid}" pattern="#,##0.00" /></td>
					  </c:if> 
					  <c:forEach var="i" begin="1" end="${right_cols}">
					  	<td></td>
					  </c:forEach>
					</tr> 
				</tbody>
			</table>  
			<c:if test="${hasSubscription}">
				<div class="panel-footer">
	                <div class="clearfix">
	                    <div class="pull-left"> 
                    		<div class="pull-left mb-xs-15"> 
								<p class="text-danger mb-0"><sup></sup>
									&sup1; <fmt:message key="subscription" />
								</p>  
                       		</div> 
                   	 	</div> 
                	</div>
            	</div>   
            </c:if> 
		</div>
		<!-- End: Orders Table -->
 
		</form>
	</section>
	<!-- End: Content -->

			
</sec:authorize>
  </tiles:putAttribute>    
</tiles:insertDefinition>
	
	
<script type="text/javascript" src="../../admin/javascript/autocompleter1.2.js"></script>
<script type="text/javascript" src="../../admin/javascript/autocompleter.Request1.2.js"></script>
<script type="text/javascript" src="../../admin/javascript/observer.js"></script>
 
<c:if test="${gSiteConfig['gGROUP_CUSTOMER']}">
<script type="text/javascript" src="../../admin/javascript/multiple.open.accordion1.2.js"></script>
</c:if>
	 
<script type="text/javascript">
    (function ($) {
        'use strict';
        $(function () {
            // Init FancyBox
            $("a.viewOrderInvoice").fancybox({
                type: 'iframe',
                maxWidth: 1280,
                maxHeight: 720,
                fitToView: false,
                width: '90%',
                height: '90%',
                autoSize: false
            });

            // Init DatePicker
            $('#start_datepicker').datepicker({
                prevText: '<i class="fa fa-chevron-left"></i>',
                nextText: '<i class="fa fa-chevron-right"></i>',
                showButtonPanel: true
            });
            $('#end_datepicker').datepicker({
                prevText: '<i class="fa fa-chevron-left"></i>',
                nextText: '<i class="fa fa-chevron-right"></i>',
                showButtonPanel: true
            });
        });
    })(jQuery);
    
	$('.table').find('.checkbox-inline input:checkbox.checkAll').click(function(event) {
		var checked = !! event.currentTarget.checked;
		$('.table').find('tbody input:checkbox.check').prop('checked', checked);
	});
	
	window.addEvent('domready', function(){
		var box1 = new multiBox('mbOrder', {descClassName: 'descClassName',showNumbers: false,overlay: new overlay()});
		$$('img.action').each(function(img){
			//containers
			var actionList = img.getParent(); 
			var actionHover = actionList.getElements('div.action-hover')[0];
			actionHover.set('opacity',0);
			//show/hide
			img.addEvent('mouseenter',function() {
				actionHover.setStyle('display','block').fade('in');
			});
			actionHover.addEvent('mouseleave',function(){
				actionHover.fade('out');
			});
			actionList.addEvent('mouseleave',function() {
				actionHover.fade('out');
			});
		});
		// Create the accordian
		var myAccordion = new Fx.Accordion($$('div.arrow_drop'), $$('div.information'), {
			display: 1, alwaysHide: true,
	    	onActive: function() {$('information').removeClass('displayNone');}
		});
		$('quickModeTrigger').addEvent('click',function() {
		    $('mboxfull').setStyle('margin', '0');
		    $('OverlayContainer').hide();
		    $('subMenusContainer').hide();
		    $$('.quickMode').each(function(el){
		    	el.hide();
		    });
		    $$('.quickModeRemove').each(function(el){
		    	el.destroy();
		    });
		    $$('.listingsHdr3').each(function(el){
		    	el.removeProperties('colspan', 'class');
		    });
		    alert('Select the whole page, copy and paste to your excel file.');
		  });
		  var firstName = $('supplierName');
		  this["el1"] = $('supplierName');
		  new Autocompleter.Request.HTML(this["el1"], '../../admin/orders/show-ajax-suppliers.jhtm', {
		  'indicatorClass': 'autocompleter-loading',
			'postData': { 'search': 'firstName'}
		});
	});

	function toggleAll(el) {
		var ids = document.getElementsByName("__selected_id");	
	  	if (ids.length == 1)
	      document.list_form.__selected_id.checked = el.checked;
	    else
	      for (i = 0; i < ids.length; i++)
	        document.list_form.__selected_id[i].checked = el.checked;	
	}

	function batchSelected() {
		var ids = document.getElementsByName("__selected_id");	
	  	if (ids.length == 1) {
	      if (document.list_form.__selected_id.checked) {
	    	return confirm('Batch print orders?');
	      }
	    } else {
	      for (i = 0; i < ids.length; i++) {
	        if (document.list_form.__selected_id[i].checked) {
	    	  return confirm('Batch print orders?');
	        }
	      }
	    }

	    alert("Please select orders(s) to batch.");       
	    return false;
	}
</script>


