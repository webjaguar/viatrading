<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ taglib uri="http://www.springframework.org/security/tags" prefix="sec" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ taglib uri="/WEB-INF/tlds/spring.tld" prefix="spring" %>
<%@ taglib uri="/WEB-INF/tlds/custom.tld" prefix="wj" %>

<tiles:insertDefinition name="admin.responsive-template.order" flush="true">
	<tiles:putAttribute name="css">
		<link rel="stylesheet" href="${_contextpath}/admin-responsive-static/assets/css/invoice.css">
	</tiles:putAttribute>
	<tiles:putAttribute name="content" type="string">
		<c:if test="${order != null}">
			<div class="page-form">
				<div class="page-banner">
					<div class="breadcrumb"><span><a href="${_contextpath}/admin/catalog/dashboard.jhtm">Home</a></span><i class="sli-arrow-right"></i><span><a href="${_contextpath}/admin/orders/quotesList.jhtm">Quotes</a></span><i class="sli-arrow-right"></i><span> <fmt:message key="quote"/> # <c:out value="${param.order}"/></span></div>
				</div>
				<div class="page-head">
					<form action="invoice.jhtm" method="post" id="invoiceTools">
						<div class="row">
							<div class="col-12">
								<h2><fmt:message key="quote"/>: <c:out value="${order.orderId}"/></h2>
							</div>
						</div>
						<div class="row sub-status">
							<div class="col-lg-8">
								<span><fmt:message key="status"/>: <b><fmt:message key="${order.encodeStatus}"/></b></span>
							</div>
							<div class="col-lg-4">
								<div class="tab-actions">
									<sec:authorize ifAnyGranted="ROLE_AEM,ROLE_ADMIN_${prop_site_id},ROLE_INVOICE_EDIT">
										<c:if test="${(siteConfig['ALLOW_EDIT_INVOICE_SHIPPED'].value == 'true' or !(order.status == 's' || order.status == 'x')) and (order.paymentMethod == null || order.paymentMethod != 'Amazon' || (order.paymentMethod == 'Amazon' and siteConfig['AMAZON_ORDER_ALLOW_EDIT'].value == 'true'))}">
											<a class="action-item action" href="<c:url value="invoiceForm.jhtm"><c:param name="orderId" value="${order.orderId}"/><c:param name="orderName" value="${orderName}"/></c:url>">
												<i class="sli-note"></i>
											</a>
										</c:if>
									</sec:authorize>
									<c:if test="${gSiteConfig['gPDF_INVOICE']}">
										<a class="action-item action" href="email.jhtm?orderId=${order.orderId}"><i class="sli-envelope"></i></a>
									</c:if>
									<div class="action-item dropdown">
										<a class="dropdown-toggle" data-toggle="dropdown">
											<i class="sli-printer"></i>
										</a>
										<div class="dropdown-menu dropdown-menu-right">
											<a class="dropdown-item" href="invoice.jhtm?order=${order.orderId}&__print=true"><fmt:message key="printerFriendly" /></a>
											<a class="dropdown-item" href="invoice.jhtm?order=${order.orderId}&__label=true"><fmt:message key="label" /></a>
											<a class="dropdown-item" href="invoice.jhtm?order=${order.orderId}&__printDiscount=true"><fmt:message key="showDiscount" /></a>
											<c:if test="${siteConfig['ORDER_OVERVIEW'].value == 'true'}">
												<a class="dropdown-item" href="invoice.jhtm?order=${order.orderId}&__overview=true"><fmt:message key="overview" /></a>
											</c:if>
										</div>
									</div>
								</div>
							</div>
						</div>
						<input type="hidden" name="order" value="<c:out value="${order.orderId}"/>" >
					</form>
				</div>
				<div class="page-body">
					<div class="minor-info">
						<c:if test="${order.lastModified != null}">
							<span><span><fmt:message key="lastModified" />:</span> <span><fmt:formatDate type="both" timeStyle="full" pattern="MM/dd/yyyy hh:mm a" value="${order.lastModified}"/></td></span></span>
						</c:if>
						<c:if test="${gSiteConfig['gSALES_REP']}">
							<span><span><fmt:message key="salesRep"/>:</span> <span><c:out value="${salesRep.name}"/></span></span>
							<span><span><fmt:message key="processedBy"/>:</span> <span><c:out value="${salesRepProcessedBy.name}"/></span></span>
						</c:if>
					</div>
					<div class="main-content">
						<div class="form-section">
							<div class="content">
								<ul class="nav nav-tabs" role="tablist">
									<li class="nav-item">
					                    <a class="nav-link active" data-toggle="tab" role="tab" href="#tab_1">Quote</a>
					                </li>
					                <c:if test="${gSiteConfig['gBUDGET'] and fn:length(order.actionHistory) gt 0}">
						                <li class="nav-item">
						                    <a class="nav-link" data-toggle="tab" role="tab" href="#tab_2">Approval History</a>
						                </li>
									</c:if>
								</ul>
								<div class="tab-content">
									<c:import url="/WEB-INF/jsp/admin-responsive/orders/quote/main_tab.jsp" />
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</c:if>
		<c:if test="${gSiteConfig['gBUDGET'] and fn:length(order.actionHistory) gt 0}">
			<c:import url="/WEB-INF/jsp/admin-responsive/orders/quote/action_history_tab.jsp" />
		</c:if>
	</tiles:putAttribute>
	<tiles:putAttribute name="js">
		<script>
		 	function subStatusOnchanged(el){
		 		var option = el.options[el.selectedIndex];
		 		var cc = option.getAttribute('cc');
		 		$('#cc').val(cc);
		 		
		 		var groupId = 0;//no Group
		 		var defaultSiteMessageId = option.getAttribute('default-site-message-id');
		 		if(defaultSiteMessageId){
		 			//Group All Messages
		 			groupId = -1;
		 		}
		 		$('#groupSelected').val(groupId);
	 			getSiteMessage(groupId,defaultSiteMessageId);
	 		}
		 	
		 	function subStatusGroupOnchanged(el){
	 			var groupId = null;
				var model = {};
				if(el.value!= 'All')
					model['groupId'] = $('#subStatusGroupId').val();

				$.post('show-ajax-subStatus.jhtm', model, function(data){
		      		$('#subStatusList').html(data);
				});
	 		}
			
		 	function chooseMessage(el){
		 		var messageList = [];
		 		$('[name=message]').each(function(index){
		 			 $(this).hide();
		 		});
		 		$('[name=subject]').each(function(index){
		 			 $(this).hide();
		 		});
		 		$('#subject'+el.value).show();
		 		$('#message'+el.value).show();
		 		
				if ($('#htmlID'+el.value).val() == 'true') {
					$('#htmlCheckbox').prop('checked', true);
				}
				else {
					$('#htmlCheckbox').prop('checked', false);
				}
		 	}
	 	
			function getSiteMessage(groupId,defaultSiteMessageId) {
				$.post('../config/show-ajax-groupSiteMessage.jhtm?groupId='+groupId,function(data){
					$('#siteMessageSelect').html(data);
					$('#messageSelected').val(defaultSiteMessageId).trigger('change');
				});
	 		}

		 	function notifyCustomer(){
				$('#notifyCustomer').toggle();
				$('#messageSelect').show();
			}
	 	</script>
	 	
		<!-- DGRID TABLE SCRIPT MUST BE AT BOTTOM OF THE PAGE -->
		<script src="${_contextpath}/admin-responsive-static/assets/plugins/dojo/dojo.js" data-dojo-config="async: true"></script>
		<script>
			function initializeOrderGrid(){
				//Table Data Sanitization
				var data = [];
				<c:forEach var="lineItem" items="${order.lineItems}" varStatus="status">
					data.push({
						offset: "${status.count}.",
						<c:if test="${lineItem.product.id != null}">
				        		productId: "${lineItem.product.id}",
				        </c:if>
				        sku: "${lineItem.product.sku}",
				        name: "${wj:escapeJS(lineItem.product.name)}",
				        <c:forEach items="${productFieldsHeader}" var="pFHeader" varStatus="status">
				        		<c:set var="check" value="0"/>
					  		<c:if test="${pFHeader.showOnInvoice or pFHeader.showOnInvoiceBackend}">
					  			<c:forEach items="${lineItem.productFields}" var="productField"> 
							   		<c:if test="${pFHeader.id == productField.id}">
							   			"${pFHeader.name}" : "${wj:escapeJS(productField.value)}",
								    		<c:set var="check" value="1"/>
								   </c:if>
							  	</c:forEach>
					  			<c:if test="${check == 0}">
									"${pFHeader.name}" : "",
							   	</c:if>
					  		</c:if>
					  	</c:forEach>
					  	<c:choose>
					    		<c:when test="${lineItem.priceCasePackQty != null}">
					    			quantity: '(<c:out value="${lineItem.priceCasePackQty}"/> per <c:out value="${siteConfig[\'PRICE_CASE_PACK_UNIT_TITLE\'].value}" /> )',
					    		</c:when>
						    	<c:otherwise>
						    		quantity: "${lineItem.quantity}",
						    	</c:otherwise>
						</c:choose>
						<c:if test="${siteConfig['GENERATE_PACKING_LIST'].value == 'true'}"> 
							processed: <c:out value="${lineItem.processed}"/>,
				  		</c:if>
						<c:if test="${not (order.hasRewardPoints && lineItem.product.productType == 'REW')}">
		    					price: decodeHTML('<fmt:message key="${siteConfig[\'CURRENCY\'].value}" />')+'<fmt:formatNumber value="${lineItem.unitPrice}" pattern="#,##0.00000#" maxFractionDigits="${siteConfig[\'MAX_FRACTION_DIGIT\'].value}" />', 
		              	</c:if>
				  		<c:if test="${gSiteConfig['gPURCHASE_ORDER'] && siteConfig['SHOW_COST'].value == 'true' && order.hasCost}">
							cost: decodeHTML('<fmt:message key="${siteConfig[\'CURRENCY\'].value}" />') + '<fmt:formatNumber value="${lineItem.unitCost}" pattern="#,##0.00"/>', 
						</c:if>
						<c:if test="${not (order.hasRewardPoints and lineItem.product.productType == 'REW')}">
							total: decodeHTML('<fmt:message key="${siteConfig[\'CURRENCY\'].value}" />') + '<fmt:formatNumber value="${lineItem.totalPrice}" pattern="#,##0.00000#" maxFractionDigits="${siteConfig[\'MAX_FRACTION_DIGIT\'].value}"/>',
					    </c:if>
					});
				</c:forEach>
				
				require(["dgrid/List", "dgrid/OnDemandGrid", "dgrid/Selection", "dgrid/Keyboard", "dgrid/Editor", "dijit/form/CheckBox", 
						"dijit/form/TimeTextBox", "dijit/form/Select", "dojo/data/ObjectStore", "dijit/form/FilteringSelect",
						'dijit/form/DropDownButton', 'dijit/DropDownMenu', 'dijit/MenuItem',
						"dojo/_base/declare", "dojo/_base/lang", "dojo/store/Memory", "dojo/store/Observable", "dgrid/test/data/createSyncStore", 
						'dgrid/extensions/ColumnHider', 'dgrid/extensions/ColumnReorder', 'dgrid/extensions/ColumnResizer',
					], function(List, OnDemandGrid, Selection, Keyboard, Editor, CheckBox, TimeTextBox, Select, ObjectStore, FilteringSelect,
							DropDownButton, DropDownMenu, MenuItem,
							declare, lang, LegacyMemory, LegacyObservable, createSyncStore, ColumnHider, ColumnReorder, ColumnResizer){

					//Column Definitions
					var columns = {
						offset: {
							label: 'Line#',
							get: function(object){
								return object.offset;
							},
							sortable: false,
						},
						sku: {
							label: '<fmt:message key="productSku" />',
							renderCell: function(object){
								if(object.productId){
									var link = document.createElement('a');
									link.text = object.sku;
									link.href = "./catalog/product.jhtm?id="+object.productId;
									return link;
								}
								var span = document.createElement('span');
								span.textContent = object.sku;
								return div;
							},
							sortable: false,
						},
						name: {
							label: '<fmt:message key="productName" />',
							get: function(object){
								return object.name;
							},
							sortable: false,
						},
						<c:forEach items="${productFieldsHeader}" var="productField">
				      		<c:if test="${productField.showOnInvoice or productField.showOnInvoiceBackend}">
				      			"${productField.name}": {
									label: '${productField.name}',
									get: function(object){
										return object['${productField.name}'];
									},
									sortable: false,
								},
					      	</c:if>
					    </c:forEach>
					    quantity: {
							label: '<fmt:message key="quantity" />',
							get: function(object){
								return object.quantity;
							},
							sortable: false,
						},
						<c:if test="${siteConfig['GENERATE_PACKING_LIST'].value == 'true'}">
							processed: {
								label: '<fmt:message key="packingList" />',
								get: function(object){
									return object.processed;
								},
								sortable: false,
							},
		                </c:if>
	                		price: {
							label: '<fmt:message key="productPrice" /><c:if test="${order.hasContent}"> / <c:out value="${siteConfig[\'CASE_UNIT_TITLE\'].value}" /></c:if>',
							get: function(object){
								return object.price;
							},
							sortable: false,
						},
						<c:if test="${gSiteConfig['gPURCHASE_ORDER'] && siteConfig['SHOW_COST'].value == 'true' && order.hasCost}">
							cost: {
								label: '<fmt:message key="cost" />',
								get: function(object){
									return object.cost;
								},
								sortable: false,
							},
					    </c:if>
						total: {
							label: '<fmt:message key="total" />',
							get: function(object){
								return object.total;
							},
							sortable: false,
						},
					};
					
					var grid = new (declare([OnDemandGrid, Selection, Keyboard, Editor, ColumnReorder, ColumnResizer]))({
						collection: createSyncStore({ data: data }),
						columns: columns,
						selectionMode: "none",
					},'grid');

					grid.on('dgrid-refresh-complete', function(event) {
						$('#grid-hider-menu').appendTo('#column-hider');
					});
					
					grid.startup();
				});
			}
			
    			function initializeOrderStatusGrid(){
    				//Table Data Sanitization
    				var data = [];
    				<c:forEach var="orderStatus" items="${order.statusHistory}">
    					<c:if test="${orderStatus.status != 'xap' or order.status == 'xap'}">
 	   					data.push({
 	   						dateChanged: '<fmt:formatDate type="both" timeStyle="full" value="${orderStatus.dateChanged}"/>',
 	   						status: '<fmt:message key="orderStatus_${orderStatus.status}"/>',
							<c:if test="${siteConfig['SUB_STATUS'].value != '0'}">
								subStatus: '${orderSubStatusMap[orderStatus.subStatus]}',
							</c:if>
 	   						<c:if test="${orderStatus.userNotified}">
 								userNotified: true,
 							</c:if>
 	   						<c:choose>
 								<c:when test="${fn:containsIgnoreCase(order.shippingMethod,'ups') or fn:containsIgnoreCase(order.shippingCarrier,'ups')}">
 									trackingURL: "http://wwwapps.ups.com/WebTracking/processInputRequest?HTMLVersion=5.0&sort_by=status&tracknums_displayed=10&TypeOfInquiryNumber=T&loc=en_US&InquiryNumber1=<c:out value='${orderStatus.trackNum}' />&AgreeToTermsAndConditions=yes",
 									trackingNumber: "${orderStatus.trackNum}",								
 								</c:when>
 								<c:when test="${fn:containsIgnoreCase(order.shippingMethod,'fedex') or fn:containsIgnoreCase(order.shippingCarrier,'fedex')}">
 									trackingURL: "http://fedex.com/Tracking?action=track&language=english&cntry_code=us&mps=y&tracknumbers=<c:out value='${orderStatus.trackNum}' />&ascend_header=1",
 									trackingNumber: "${orderStatus.trackNum}",
 								</c:when>
 								<c:when test="${fn:containsIgnoreCase(order.shippingMethod,'usps') or fn:containsIgnoreCase(order.shippingCarrier,'usps')}">
 									trackingURL: "http://trkcnfrm1.smi.usps.com/PTSInternetWeb/InterLabelInquiry.do?strOrigTrackNum=<c:out value='${orderStatus.trackNum}' />",
 									trackingNumber: "${orderStatus.trackNum}",
 								</c:when>
 								<c:when test="${fn:containsIgnoreCase(order.shippingMethod,'averitt') or fn:containsIgnoreCase(order.shippingCarrier,'averitt')}">
 									trackingURL: "https://www.averittexpress.com/servlet/rsoLTLtrack?Type=PN&Number=<c:out value='${orderStatus.trackNum}' />",
 									trackingNumber: "${orderStatus.trackNum}",
 								</c:when>
 								<c:otherwise>
 									trackingNumber: '<c:out value="${orderStatus.trackNum}" />',
 								</c:otherwise>
 							</c:choose>
 							comments: '${wj:escapeJS(orderStatus.comments)}',
 							username: '${wj:escapeJS(orderStatus.username)}',
 	   					});
    					</c:if>
    				</c:forEach>
    				
    				require(["dgrid/List", "dgrid/OnDemandGrid", "dgrid/Selection", "dgrid/Keyboard", "dgrid/Editor", "dijit/form/CheckBox", 
    						"dijit/form/TimeTextBox", "dijit/form/Select", "dojo/data/ObjectStore", "dijit/form/FilteringSelect",
    						'dijit/form/DropDownButton', 'dijit/DropDownMenu', 'dijit/MenuItem',
    						"dojo/_base/declare", "dojo/_base/lang", "dojo/store/Memory", "dojo/store/Observable", "dgrid/test/data/createSyncStore", 
    						'dgrid/extensions/ColumnHider', 'dgrid/extensions/ColumnReorder', 'dgrid/extensions/ColumnResizer',
    					], function(List, OnDemandGrid, Selection, Keyboard, Editor, CheckBox, TimeTextBox, Select, ObjectStore, FilteringSelect,
    							DropDownButton, DropDownMenu, MenuItem,
    							declare, lang, LegacyMemory, LegacyObservable, createSyncStore, ColumnHider, ColumnReorder, ColumnResizer){

    					//Column Definitions
    					var columns = {
    						dateChanged: {
    							label: 'Date Changed',
    							get: function(object){
    								return object.dateChanged;
    							},
    							sortable: false,
    						},
    						status: {
    							label: '<fmt:message key="status"/>',
    							get: function(object){
    								return object.status;
    							},
    							sortable: false,
    						},
    						<c:if test="${siteConfig['SUB_STATUS'].value != '0'}">
    							subStatus: {
	    							label: '<fmt:message key="subStatus"/>',
	    							get: function(object){
	    								return object.subStatus;
	    							},
	    							sortable: false,
	    						},
						</c:if>
    						customerNotified: {
 							label: 'Customer Notified',
 							renderCell: function(object){
 								var div = document.createElement("div");
 								if(object.userNotified){
 									var i = document.createElement("i");
 									i.className = 'sli-check';
 									div.appendChild(i);
 								}
 								return div;
 							},
 							sortable: false,
 						},
 						trackingNumber: {
 							label: '<fmt:message key="trackNum"/>',
 							renderCell: function(object){
 								if(object.trackingURL){
 									var link = document.createElement("a");
 									link.text = object.trackingNumber;
 									link.href = object.trackingURL;
 									link.setAttribute('target','_blank')
 									return link;
 								}
 								var span = document.createElement("span");
 								span.textContent = object.trackingNumber;
 								return span;
 							},
 							sortable: false,
 						},
 						comments: {
 							label: '<fmt:message key="comments"/>',
 							get: function(object){
 								return object.comments;
 							},
 							sortable: false,
 						},
 						username: {
 							label: '<fmt:message key="user"/>',
 							get: function(object){
 								return object.username;
 							},
 							sortable: false,
 						}
    					};
    					
    					var grid = new (declare([OnDemandGrid, Selection, Keyboard, Editor, ColumnReorder, ColumnResizer]))({
    						collection: createSyncStore({ data: data }),
    						columns: columns,
    						selectionMode: "none",
    					},'order-status-grid');

    					grid.on('dgrid-refresh-complete', function(event) {
    						$('#grid-hider-menu').appendTo('#column-hider');
    					});
    					
    					grid.startup();
    				});
    			}
			
			initializeOrderGrid();
			initializeOrderStatusGrid();
		</script>
	</tiles:putAttribute>
</tiles:insertDefinition>