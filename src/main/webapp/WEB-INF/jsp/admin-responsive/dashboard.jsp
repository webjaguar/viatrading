<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ taglib uri="http://www.springframework.org/security/tags" prefix="sec" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ taglib uri="/WEB-INF/tlds/spring.tld" prefix="spring" %>
<%@ taglib uri="/WEB-INF/tlds/custom.tld" prefix="wj" %>

<tiles:insertDefinition name="admin.responsive-template">
	<tiles:putAttribute name="css">
		<link rel="stylesheet" href="${_contextpath}/admin-responsive-static/assets/plugins/chartist/dist/chartist.min.css">
		<link rel="stylesheet" href="${_contextpath}/admin-responsive-static/assets/plugins/chartist/plugins/chartist-plugin-tooltip.css">
		<link rel="stylesheet" href="${_contextpath}/admin-responsive-static/assets/plugins/chartist/plugins/chartist-plugin-legend.css">
		<link rel="stylesheet" href="${_contextpath}/admin-responsive-static/assets/plugins/jvectormap/jquery-jvectormap.css">
		
		<style>
			.widget{
			 	height: 350px;
			 }
			 .table-widget-3 .table td .rounded-circle{
			 	width: 50px;
			 	height: 50px;
				background-size: cover;
			 }
			 #best-selling-widget{
			 	height: inherit;
			 }
			.fixed-widget-8{
				height: 330px;
			}
			.fixed-widget-8 table > tbody > tr > td:nth-child(1){
				max-width: 200px;
				overflow: hidden;
				white-space: nowrap;
				text-overflow: ellipsis;
			}
			.fixed-widget-8 table > tbody > tr > td:nth-child(2){
				width: 50px;
				text-align: right;
			}
			.fixed-widget-8 table > tbody > tr > td:nth-child(3){
				width: 100px;
				text-align: right;
			}
			
			/* CHARTIST */
			#orders-chart .ct-series-a .ct-point{
				stroke: #388e3c;
			}
			#orders-chart .ct-series-a .ct-line{
				stroke: #388e3c;
			}
			#orders-chart .ct-series-a .ct-area{
			    fill: #388e3c;
			}
			
			#orders-chart .ct-series-b .ct-point{
				stroke: #ffa000;
			}
			#orders-chart .ct-series-b .ct-line{
				stroke: #ffa000;
				stroke-dasharray: 5px 2px;
			}
			#orders-chart .ct-series-b .ct-area{
			    fill: #ffa000;
			}
			
			#partial-orders-percentage-chart .ct-series-a .ct-slice-donut-solid{
				fill: #f05b4f;
			}
			#partial-orders-percentage-chart .ct-series-b .ct-slice-donut-solid{
				fill: #ffa000;
			}
			#partial-orders-percentage-chart .ct-series-c .ct-slice-donut-solid{
				fill: #388e3c;
			}
			
			#partial-orders-percentage-chart .ct-legend-inside{
				top: 30px;
				right: -70px;
			}
			
			#partial-orders-percentage-chart .ct-series-0:before {
				background-color: #f05b4f;
				border-color: #f05b4f;
			}
	        
			#partial-orders-percentage-chart .ct-series-1:before {
				background-color: #ffa000;
				border-color: #ffa000;
			}
	        
			#partial-orders-percentage-chart .ct-series-2:before {
				background-color: #388e3c;
				border-color: #388e3c;
			}
			
			#partial-orders-chart .ct-series-a .ct-bar{
				stroke: #f05b4f;
			}
			#partial-orders-chart .ct-series-b .ct-bar{
			    stroke: #ffa000;
			}
			#partial-orders-chart .ct-series-c .ct-bar{
			    stroke: #388e3c;
			}
			
			#customers-chart .ct-series-a .ct-bar{
			    stroke: #00a9f1;
			}
			
			#states-chart .ct-legend-inside{
				top: 30px;
				right: -40px;
			}
			
			#states-chart .ct-series-0:before {
				background-color: #d70206;
				border-color: #d70206;
			}
	        
			#states-chart .ct-series-1:before {
				background-color: #f05b4f;
				border-color: #f05b4f;
			}
	        
			#states-chart .ct-series-2:before {
				background-color: #f4c63d;
				border-color: #f4c63d;
			}
	        
			#states-chart .ct-series-3:before {
				background-color: #d17905;
				border-color: #d17905;
			}
	        
			#states-chart .ct-series-4:before {
				background-color: #453d3f;
				border-color: #453d3f;
			}
			
			#income-chart .ct-point{
				stroke: #388e3c;
			}
			#income-chart .ct-line{
				stroke: #388e3c;
			}
			#income-chart .ct-area{
			    fill: #388e3c;
			}
			#income-chart .ct-series-b .ct-point{
				stroke: #ffa000;
			}
			#income-chart .ct-series-b .ct-line{
				stroke: #ffa000;
				stroke-dasharray: 5px 2px;
			}
			#income-chart .ct-series-b .ct-area{
			    fill: #ffa000;
			}
		</style>
	</tiles:putAttribute>
	<tiles:putAttribute name="content">
		<c:choose>
			<c:when test="${model.reportingPeriod  == 'last_7_days'}">
				<c:set var="reportPeriodName" value="LAST 7 DAYS"/>
			</c:when>
			<c:when test="${model.reportingPeriod  == 'last_30_days'}">
				<c:set var="reportPeriodName" value="LAST 30 DAYS"/>
			</c:when>
			<c:when test="${model.reportingPeriod  == 'last_month'}">
				<c:set var="reportPeriodName" value="LAST MONTH"/>
			</c:when>
			<c:otherwise>
				<c:set var="reportPeriodName" value="LAST 12 MONTHS"/>
			</c:otherwise>
		</c:choose>
		
		<div class="page-banner">
			<div class="breadcrumb"><span>Home</span></div>
		</div>
		<div class="page-head">
			<div class="row justify-content-between">
				<div class="col-sm-8 title">
					<h3>Dashboard</h3>
				</div>
				<div class="col-sm-4 actions">
					<form id="reportingPeriodForm" action="${_contextpath}/admin/catalog/dashboard.jhtm" method="post">
						<div class="input-group max-w-200 float-right">
							<span class="input-group-addon">
								<span class="sli-calendar"></span>
							</span>
							<select class="custom-select" id="reportingPeriod" name="reportingPeriod">
								<option value="last_7_days" <c:if test="${model.reportingPeriod  == 'last_7_days'}">selected="selected"</c:if>>Last 7 Days</option>
								<option value="last_30_days" <c:if test="${model.reportingPeriod  == 'last_30_days'}">selected="selected"</c:if>>Last 30 Days</option>
								<option value="last_month" <c:if test="${model.reportingPeriod  == 'last_month'}">selected="selected"</c:if>>Last Month</option>
								<option value="last_12_months" <c:if test="${model.reportingPeriod  == 'last_12_months'}">selected="selected"</c:if>>Last 12 Months</option>
								<option value="last_year" <c:if test="${model.reportingPeriod  == 'last_year'}">selected="selected"</c:if>>Last Year</option>
								<option value="this_year" <c:if test="${model.reportingPeriod  == 'this_year'}">selected="selected"</c:if>>This Year</option>
				        		</select>
						</div>
					</form>
				</div>
			</div>
		</div>
		<div class="page-body">
			<div class="row">
				<div class="col-12 col-md-6 col-lg-6 col-xl-3 m-b-20">
					<div class="icon-widget-8 color-default">
						<ul class="list-group">
							<li class="list-group-item">
								<div class="row">
									<div class="col flex-80 text-center align-self-center p-t-5">
										<i class="sli-docs color-success"></i>
									</div>
									<div class="col max-w-0">
										<div class="number">${model.nOrders}</div>
										<div class="text" style="white-space: nowrap;">Orders</div>
									</div>
								</div>
							</li>
						</ul>
					</div>
				</div>
				<div class="col-12 col-md-6 col-lg-6 col-xl-3 m-b-20">
					<div class="icon-widget-8 color-default">
						<ul class="list-group">
							<li class="list-group-item">
								<div class="row">
									<div class="col flex-80 text-center align-self-center p-t-5">
										<i class="sli-graph color-success"></i>
									</div>
									<div class="col max-w-0">
										<div class="number"><fmt:message key="${siteConfig['CURRENCY'].value}" /><fmt:formatNumber value="${model.orderTotal}" pattern="#,##0.00" /></div>
										<div class="text" style="white-space: nowrap;">Income</div>
									</div>
								</div>
							</li>
						</ul>
					</div>
				</div>
				<div class="col-12 col-md-6 col-lg-6 col-xl-3 m-b-20">
					<div class="icon-widget-8 color-default">
						<ul class="list-group">
							<li class="list-group-item">
								<div class="row">
									<div class="col flex-80 text-center align-self-center p-t-5">
										<i class="sli-docs color-twitter"></i>
									</div>
									<div class="col max-w-0">
										<div class="number">${model.nQuotes}</div>
										<div class="text" style="white-space: nowrap;">Quotes</div>
									</div>
								</div>
							</li>
						</ul>
					</div>
				</div>
				<div class="col-12 col-md-6 col-lg-6 col-xl-3 m-b-20">
					<div class="icon-widget-8 color-default">
						<ul class="list-group">
							<li class="list-group-item">
								<div class="row">
									<div class="col flex-80 text-center align-self-center p-t-5">
										<i class="sli-notebook color-twitter"></i>
									</div>
									<div class="col max-w-0">
										<div class="number">${model.nTickets}</div>
										<div class="text" style="white-space: nowrap;">Tickets</div>
									</div>
								</div>
							</li>
						</ul>
					</div>
				</div>
				<div class="col-12 col-md-6 col-lg-6 col-xl-3 m-b-20">
					<div class="icon-widget-8 color-primary">
						<ul class="list-group">
							<li class="list-group-item">
								<div class="row">
									<div class="col flex-80 text-center align-self-center p-t-5">
										<i class="sli-user color-twitter"></i>
									</div>
									<div class="col max-w-0">
										<div class="number">${model.nCustomers}</div>
										<div class="text" style="white-space: nowrap;">Customers</div>
									</div>
								</div>
							</li>
						</ul>
					</div>
				</div>
				<div class="col-12 col-md-6 col-lg-6 col-xl-3 m-b-20">
					<div class="icon-widget-8 color-default">
						<ul class="list-group">
							<li class="list-group-item">
								<div class="row">
									<div class="col flex-80 text-center align-self-center p-t-5">
										<i class="sli-refresh color-danger"></i>
									</div>
									<div class="col max-w-0">
										<div class="number">${model.nPendingOrders}</div>
										<div class="text" style="white-space: nowrap;">Pending</div>
									</div>
								</div>
							</li>
						</ul>
					</div>
				</div>
				<div class="col-12 col-md-6 col-lg-6 col-xl-3 m-b-20">
					<div class="icon-widget-8 color-default">
						<ul class="list-group">
							<li class="list-group-item">
								<div class="row">
									<div class="col flex-80 text-center align-self-center p-t-5">
										<i class="sli-refresh color-warning"></i>
									</div>
									<div class="col max-w-0">
										<div class="number">${model.nProcessingOrders}</div>
										<div class="text" style="white-space: nowrap;">Processing</div>
									</div>
								</div>
							</li>
						</ul>
					</div>
				</div>
				<div class="col-12 col-md-6 col-lg-6 col-xl-3 m-b-20">
					<div class="icon-widget-8 color-danger">
						<ul class="list-group">
							<li class="list-group-item">
								<div class="row">
									<div class="col flex-80 text-center align-self-center p-t-5">
										<i class="sli-basket color-danger"></i>
									</div>
									<div class="col max-w-0" style="white-space: nowrap;">
										<div class="number">${model.nAbandonedCarts}</div>
										<div class="text">Abandon Carts</div>
									</div>
								</div>
							</li>
						</ul>
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col-lg-12">
					<div class="widget">
						<div class="row">
							<div class="col">
								<div class="title">Orders</div>
							</div>
						</div>
						<div class="row">
							<div class="col-12">
								<div class="row" style="height: 260px">
									<div class="ct-chart ct-golden-section" id="orders-chart"></div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col-lg-8">
					<div class="widget">
						<div class="row">
							<div class="col">
								<div class="title">Order States</div>
							</div>
						</div>
						<div class="row" style="max-height: 260px">
							<div class="ct-chart ct-golden-section" id="partial-orders-chart"></div>
						</div>
					</div>
				</div>
				<div class="col-lg-4">
					<div class="widget">
						<div class="row">
							<div class="col">
								<div class="title">Order States Summary</div>
								<div class="description">Distribution of order status</div>
							</div>
						</div>
						<div class="row">
							<div class="col-12">
								<div class="row" style="height: 230px; padding-right: 80px;">
									<div class="ct-chart ct-golden-section" id="partial-orders-percentage-chart"></div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col-12">
					<div class="widget">
						<div class="row">
							<div class="col">
								<div class="title">Customers</div>
							</div>
						</div>
						<div class="row" style="max-height: 260px">
							<div class="ct-chart ct-golden-section" id="customers-chart"></div>
						</div>
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col-lg-4">
					<div class="widget fixed-widget-8">
						<div class="row">
							<div class="col">
								<div class="title">Top States</div>
								<div class="description">Most orders will be shipped to these states</div>
							</div>
						</div>
						<div class="row">
							<div class="col-12">
								<div class="row" style="height: 230px; padding-right: 80px;">
									<div class="ct-chart ct-golden-section" id="states-chart"></div>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="col-lg-8">
					<div class="widget fixed-widget-8">
						<div class="row">
							<div class="col">
								<div class="title">Excellent Sales Reps</div>
								<div class="description">Sales Reps who placed most items</div>
							</div>
						</div>
						<div class="row">
							<div class="col">
								<div class="table-widget-8">
									<table class="table">
										<tbody>
											<c:forEach items="${model.topSalesReps}" var="sr" varStatus="status">
												<tr>
													<td style="text-transform: uppercase">${sr.name}</td>
													<td>${sr.numOrderShipped}</td>
													<td><fmt:message key="${siteConfig['CURRENCY'].value}" /><fmt:formatNumber value="${sr.grandTotalShipped}" pattern="#,##0.00" /></td>
												</tr>
											</c:forEach>
										</tbody>
									</table>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col-12">
					<div class="widget">
						<div class="row">
							<div class="col">
								<div class="title">Income</div>
							</div>
						</div>
						<div class="row" style="max-height: 260px">
							<div class="ct-chart ct-golden-section" id="income-chart"></div>
						</div>
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col-12 col-md-12 col-lg-6">
					<div class="widget fixed-widget-8">
						<div class="row">
							<div class="col">
								<div class="title">Abandoned Carts</div>
								<div class="description">Most abandoned carts and their value</div>
							</div>
						</div>
						<div class="row">
							<div class="col">
								<div class="table-widget-8">
									<table class="table table-unbordered table-striped">
										<tbody>
											<c:forEach items="${model.topAbandonedProducts}" var="p" varStatus="status">
												<tr>
													<td>${p.prodName}</td>
													<td>${p.abandonedCarts}</td>
													<td><fmt:message key="${siteConfig['CURRENCY'].value}" /><fmt:formatNumber value="${p.grossRev}" pattern="#,##0.00" /></td>
												</tr>
											</c:forEach>
										</tbody>
									</table>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="col-12 col-md-12 col-lg-6">
					<div class="widget fixed-widget-8">
						<div class="row">
							<div class="col">
								<div class="title">Top Cities</div>
								<div class="description">Most orders will be shipped to these cities</div>
							</div>
						</div>
						<div class="row">
							<div class="col">
								<div class="table-widget-8">
									<table class="table">
										<tbody>
											<c:forEach items="${model.topShippedCities}" var="c" varStatus="status">
												<tr>
													<td style="text-transform: uppercase">${c.city}, ${c.state}</td>
													<td>${c.numOrdr}</td>
													<td><fmt:message key="${siteConfig['CURRENCY'].value}" /><fmt:formatNumber value="${c.grossRev}" pattern="#,##0.00" /></td>
												</tr>
											</c:forEach>
										</tbody>
									</table>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col-12">
					<div id="best-selling-widget" class="widget">
						<div class="row">
							<div class="col">
								<div class="title">Best Selling</div>
								<div class="description">Online store best sellers</div>
							</div>
						</div>
						<div class="row">
							<div class="col">
								<div class="table-widget-3">
									<table class="table table-unbordered table-striped">
										<tbody>
											<!-- bestSellingProducts -->
											<c:forEach items="${model.bestSellingProducts}" var="p" varStatus="status">
												<tr>
													<td>
														<div style="background-image:url(../../assets/Image/Product/detailsbig/${wj:replaceUrl(p.thumbnailURL)})" class="rounded-circle"></div>
													</td>
													<td>#${p.sku}</td>
													<td>${p.prodName}</td>
													<td>${p.qtysold}</td>
													<td><fmt:message key="${siteConfig['CURRENCY'].value}" /><fmt:formatNumber value="${p.grossRev}" pattern="#,##0.00" /></td>
												</tr>
											</c:forEach>
										</tbody>
									</table>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col-12 col-md-12 col-lg-6">
					<div class="widget">
						<div class="row">
							<div class="col">
								<div class="title">Orders</div>
							</div>
						</div>
						<div class="row">
							<div id="orders_world_map" style="height: 230px; width: 100%;" class="map"></div>
							<div id="orders_usa_map" style="height: 230px; width: 100%; display:none;" class="map"></div>
						</div>
					</div>
				</div>
				<div class="col-12 col-md-12 col-lg-6">
					<div class="widget">
						<div class="row">
							<div class="col">
								<div class="title">Visitors</div>
							</div>
						</div>
						<div class="row">
							<div id="visitors_world_map" style="height: 230px; width: 100%;" class="map"></div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</tiles:putAttribute>
	<tiles:putAttribute name="js">
		<script src="${_contextpath}/admin-responsive-static/assets/plugins/chartist/dist/chartist.min.js"></script>
		<script src="${_contextpath}/admin-responsive-static/assets/plugins/chartist/plugins/chartist-plugin-tooltip.js"></script>
		<script src="${_contextpath}/admin-responsive-static/assets/plugins/chartist/plugins/chartist-plugin-legend.js"></script>
		<script src="${_contextpath}/admin-responsive-static/assets/plugins/jvectormap/jquery-jvectormap.min.js"></script>
	    <script src="${_contextpath}/admin-responsive-static/assets/plugins/jvectormap/jquery-jvectormap-world-mill.js"></script>
	    <script src="${_contextpath}/admin-responsive-static/assets/plugins/jvectormap/jquery-jvectormap-us-aea.js"></script>
		<script>
			var ordersLabel = [];
			var ordersData = [];
			<c:forEach items="${model.orderReports}" var="report" varStatus="status">
				<c:if test="${report.reportingPeriodGroup != 'w'}">
					ordersLabel[${status.index}] = '${report.startLabel}';
				</c:if>
				<c:if test="${report.reportingPeriodGroup == 'w'}">
					ordersLabel[${status.index}] = '';
				</c:if>
				<c:if test="${report.reportingPeriodGroup == 'w' and status.index == 0}">
					ordersLabel[${status.index}] = '${report.startLabel}';
				</c:if>
				<c:if test="${report.reportingPeriodGroup == 'w' and status.index == fn:length(model.orderReports)-1}">
					ordersLabel[${status.index}] = '${report.endLabel}';
				</c:if>
				ordersData[${status.index}] = {meta:'${report.summary}', value: ${report.count}};
			</c:forEach>
			<c:if test="${model.reportingPeriod == 'this_year'}">
				var lastYearOrdersData = [];
				<c:forEach items="${model.lastYearOrderReports}" var="report" varStatus="status">
					lastYearOrdersData[${status.index}] = {meta:'${report.summary}', value: ${report.count}};
				</c:forEach>
			</c:if>
			
			var quotesData = [];
			<c:forEach items="${model.quoteReports}" var="report" varStatus="status">
				quotesData[${status.index}] = {meta:'${report.summary}', value: ${report.count}};
			</c:forEach>
			
			var shippedData = [];
			<c:forEach items="${model.shippedOrderReports}" var="report" varStatus="status">
				shippedData[${status.index}] = {meta:'${report.summary}', value: ${report.count}};
			</c:forEach>
			
			var processingData = [];
			<c:forEach items="${model.processingOrderReports}" var="report" varStatus="status">
				processingData[${status.index}] = {meta:'${report.summary}', value: ${report.count}};
			</c:forEach>
			
			var pendingData = [];
			<c:forEach items="${model.pendingOrderReports}" var="report" varStatus="status">
				pendingData[${status.index}] = {meta:'${report.summary}', value: ${report.count}};
			</c:forEach>
			
			var incomeLabel = [];
			var incomeData = [];
		    <c:forEach items="${model.orderReports}" var="report" varStatus="status">
				<c:if test="${report.reportingPeriodGroup != 'w'}">
					incomeLabel[${status.index}] = '${report.startLabel}';
				</c:if>
				<c:if test="${report.reportingPeriodGroup == 'w'}">
					incomeLabel[${status.index}] = '';
				</c:if>
				<c:if test="${report.reportingPeriodGroup == 'w' and status.index == 0}">
					incomeLabel[${status.index}] = '${report.startLabel}';
				</c:if>
				<c:if test="${report.reportingPeriodGroup == 'w' and status.index == fn:length(model.orderReports)-1}">
					incomeLabel[${status.index}] = '${report.endLabel}';
				</c:if>
				incomeData[${status.index}] = {meta:'${report.summary}', value: ${report.total}};
			</c:forEach>
			<c:if test="${model.reportingPeriod == 'this_year'}">
				var lastYearIncomeData = [];
				<c:forEach items="${model.lastYearOrderReports}" var="report" varStatus="status">
					lastYearIncomeData[${status.index}] = {meta:'${report.summary}', value: ${report.total}};
				</c:forEach>
			</c:if>
			
			var customersLabel = [];
			var customersData = [];
		    <c:forEach items="${model.customerReports}" var="report" varStatus="status">
				<c:if test="${report.reportingPeriodGroup != 'w'}">
					customersLabel[${status.index}] = '${report.startLabel}';
				</c:if>
				<c:if test="${report.reportingPeriodGroup == 'w'}">
					customersLabel[${status.index}] = '';
				</c:if>
				<c:if test="${report.reportingPeriodGroup == 'w' and status.index == 0}">
					customersLabel[${status.index}] = '${report.startLabel}';
				</c:if>
				<c:if test="${report.reportingPeriodGroup == 'w' and status.index == fn:length(model.customerReports)-1}">
					customersLabel[${status.index}] = '${report.endLabel}';
				</c:if>
				customersData[${status.index}] = {meta:'${report.summary}', value: ${report.count}};
			</c:forEach>
			
			var statesLabel = [];
			var statesData = [];

			<c:forEach items="${model.topShippedStates}" var="s" varStatus="status">
				statesLabel[${status.index}] = '${s.state}';
				statesData[${status.index}] = ${s.numOrdr};
			</c:forEach>

			var markersData = [];
			<c:forEach items="${model.cityZipcodeObjectMap}" var="entry" varStatus="status">
				markersData[${status.index}] = {latLng: [${entry.value.latitude}, ${entry.value.longitude}],name: '${entry.value.city},${entry.value.stateAbbv}'};
			</c:forEach>
			
			function showOrdersMap(region) {
				if (region === 'world') {
					var map = $('#orders_world_map');
					map.show();
					map.vectorMap({
						map: 'world_mill',
						normalizeFunction: 'polynomial',
						backgroundColor: 'transparent',
						hoverOpacity: 0.7,
						hoverColor: false,
						zoomOnScroll: false,
						regionsSelectable: true,
						regionsSelectableOne: true,
						regionStyle: {
							initial: {
								fill: '#d2d6de',
								'fill-opacity': 1,
								stroke: 'none',
								'stroke-width': 0,
								'stroke-opacity': 1
							},
							hover: {
								fill: '#d2d6de',
								'fill-opacity': 0.7,
								cursor: 'pointer'
							},
							selected: {
								fill: '#28a745',
								"fill-opacity": 1,
								cursor: 'default'
							},
							selectedHover: {}
						},
						markerStyle: {
							initial: {
								fill: '#28a745',
								stroke: '#000'
							}
						},
						markers: markersData,
						onRegionClick: function (e, code) {
							if (code === 'US') {
								// switch map
								map.hide();
								showOrdersMap('usa');
							}
						}
					});
			    }
				else if (region === 'usa') {
					var map = $('#orders_usa_map');
					map.show();
					map.vectorMap({
						map: 'us_aea',
						normalizeFunction: 'polynomial',
						backgroundColor: 'transparent',
						hoverOpacity: 0.7,
						hoverColor: false,
						zoomOnScroll: false,
						regionsSelectable: true,
						regionsSelectableOne: true,
						regionStyle: {
							initial: {
								fill: '#d2d6de',
								'fill-opacity': 1,
								stroke: 'none',
								'stroke-width': 0,
								'stroke-opacity': 1
							},
							hover: {
								fill: '#d2d6de',
								'fill-opacity': 0.7,
								cursor: 'pointer'
							},
							selected: {
								fill: '#28a745',
								"fill-opacity": 1,
								cursor: 'default'
							},
							selectedHover: {}
						},
						markerStyle: {
							initial: {
								fill: '#28a745',
								stroke: '#000'
							}
						},
						markers: markersData
					});
				}
			}

			function showVisitorsMap(region) {
			    if (region === 'world') {
			      var map = $('#visitors_world_map');
			      var visitorsData = {
			        
			      };
			      map.show();
			      map.vectorMap({
			        map: 'world_mill',
			        zoomMax: 12,
			        normalizeFunction: 'polynomial',
			        backgroundColor: 'transparent',
			        hoverOpacity: 0.7,
			        hoverColor: false,
			        zoomOnScroll: false,
			        zoomMin: 1,
			        regionStyle: {
			          initial: {
			            fill: '#d2d6de',
			            'fill-opacity': 1,
			            stroke: 'none',
			            'stroke-width': 0,
			            'stroke-opacity': 1
			          },
			          hover: {
			            fill: '#28a745',
			            'fill-opacity': 0.7,
			            cursor: 'default'
			          }
			        },
			        series: {
			          regions: [
			            {
			              values: visitorsData,
			              scale: ['#92c1dc', '#ebf4f9'],
			              normalizeFunction: 'polynomial'
			            }
			          ]
			        },
			        onRegionTipShow: function (e, el, code) {
			          if (!!visitorsData[code]) {
			            el.html(el.html() + ': ' + visitorsData[code] + ' new visitors');
			          }
			        }
			      });
				}
			}
			
			function showOrdersChart(){
				new Chartist.Line('#orders-chart', {
					labels: ordersLabel,
					series: [ordersData<c:if test="${model.reportingPeriod == 'this_year'}">,lastYearOrdersData</c:if>]
				},{
					showArea: true,
					plugins: [
						Chartist.plugins.tooltip()
					]
				});
			}
			
			function showCustomersChart(){
				new Chartist.Bar('#customers-chart', {
					labels: customersLabel,
					series: [customersData]
				},{
					seriesBarDistance: 10,
					axisX: {
						offset: 60
					},
				});
			}
			
			function showTopStatesChart(){
				new Chartist.Pie('#states-chart', {
					labels: statesLabel,
					series: statesData
				},{
					showLabel: false,
					plugins: [
				        Chartist.plugins.legend()
				    ]
				});
			}
			
			function showIncomeChart(){
				new Chartist.Line('#income-chart', {
					labels: incomeLabel,
					series: [
						{
							name: 'series-1',
							data: incomeData
						}
						<c:if test="${model.reportingPeriod == 'this_year'}">
							,{
								name: 'series-2',
								data: lastYearIncomeData
							}
						</c:if>
					]
				},{
					showArea: true,
					plugins: [
						Chartist.plugins.tooltip({currency: '<fmt:message key="${siteConfig[\'CURRENCY\'].value}" />'})
					]
				});
			}
			
			function showPartialOrdersPercentageChart(){
				var nProcessings = ${model.nProcessingOrders};
				var nPendings = ${model.nPendingOrders};
				var nShippeds = ${model.nShippedOrders};
				if(nProcessings+nPendings+nShippeds > 0){
					new Chartist.Pie('#partial-orders-percentage-chart', {
						labels: ['Pending','Processing','Shipped'],
						series: [nPendings,nProcessings,nShippeds] 
					}, {
						donut: true,
						donutWidth: 40,
						donutSolid: true,
						showLabel: false,
						plugins: [
					        Chartist.plugins.legend()
					    ]
					});
				}
			}
			
			function showPartialOrdersChart(){
				new Chartist.Bar('#partial-orders-chart', {
					labels: ordersLabel,
					series: [pendingData,processingData,shippedData]
				},{
					stackBars: true
				});
			}
			
			$(document).ready(function(){
				// As options we currently only set a static size of 300x200 px. We can also omit this and use aspect ratio containers
				// as you saw in the previous example
				showOrdersChart();
				showCustomersChart();
				showPartialOrdersChart();
				showPartialOrdersPercentageChart();
				showIncomeChart();
				showTopStatesChart();
				showOrdersMap('world');
				showVisitorsMap('world');
				$(window).resize();
			});
			
			/* 
			** Reporting Period
			*/
			$("#reportingPeriod").change(function () {
				$("#reportingPeriodForm").submit();
			});
		</script>
	</tiles:putAttribute> 
</tiles:insertDefinition>