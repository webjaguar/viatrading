<!-- Header -->
<nav class="main-header navbar navbar-expand-lg sticky-top bg-danger navbar-dark">
    <!-- Logo -->
    <a class="navbar-brand text-xs-center" href="${_contextpath}/admin/catalog/dashboard.jhtm">
        <span class="logo hidden-md-down">
            <img src="${_contextpath}/admin-static/assets/responsive/img/logos/logo-white.png" alt="WebJaguar">
        </span>
        <span class="logo-mini">
            <img src="${_contextpath}/admin-static/assets/responsive/img/logos/logo-mini-white.png" alt="WebJaguar">
        </span>
    </a>

    <!-- Left navbar links -->
    <ul class="nav navbar-nav">
        <li class="nav-item">
            <a class="nav-link" data-widget="pushmenu" href="#">
                <span class="fa fa-bars"></span>
            </a>
        </li>
    </ul>
</nav>