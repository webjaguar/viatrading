<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<%@ taglib uri="http://www.springframework.org/security/tags" prefix="sec" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>

<div class="left-sidebar left-sidebar-1">
	<div class="wrapper">
		<div class="content">
			<div class="section">
				<ul class="list-unstyled mt-3">
					<li>
						<a href="${_contextpath}/admin/catalog/dashboard.jhtm" class="btn btn-default btn-flat btn-sidebar btn-sidebar-1 <c:if test="${param.tab == 'dashboard'}">is-open</c:if>">
							<i class="sli-chart"></i><span class="title">Dashboards</span>
						</a>
					</li>
					<li>
						<a class="btn btn-default btn-flat btn-sidebar btn-sidebar-1 has-children <c:if test="${param.tab == 'category' or param.tab == 'product' or param.tab == 'productOption'}">is-open</c:if>">
							<i class="sli-notebook"></i><span class="title">Catalog</span>
						</a>
						<div class="collapse <c:if test="${param.tab == 'category' or param.tab == 'product' or param.tab == 'productOption'}">show</c:if>">
							<ul class="list-unstyled">
								<li><a class="btn btn-default btn-flat btn-sidebar btn-sidebar-2 <c:if test="${param.tab == 'category'}">is-open</c:if>" href="${_contextpath}/admin/catalog/categoryList.jhtm"><i class=""></i><span class="title">Categories</span></a></li>
								<li><a class="btn btn-default btn-flat btn-sidebar btn-sidebar-2 <c:if test="${param.tab == 'product'}">is-open</c:if>" href="${_contextpath}/admin/catalog/productList.jhtm"><i class=""></i><span class="title">Products</span></a></li>
								<li><a class="btn btn-default btn-flat btn-sidebar btn-sidebar-2 <c:if test="${param.tab == 'productOption'}">is-open</c:if>" href="${_contextpath}/admin/catalog/options.jhtm"><i class=""></i><span class="title">Options</span></a></li>
							</ul>
						</div>
					</li>
					<c:if test="${gSiteConfig['gPURCHASE_ORDER']}">
						<li>
							<a class="btn btn-default btn-flat btn-sidebar btn-sidebar-1 <c:if test="${param.tab == 'po'}">is-open</c:if>" href="${_contextpath}/admin/inventory">
								<i class="sli-doc"></i><span class="title">Purchase Order</span>
							</a>
						</li>
					</c:if>
					<li>
						<a class="btn btn-default btn-flat btn-sidebar btn-sidebar-1 has-children <c:if test="${param.tab == 'customers' or param.tab == 'groups' or param.tab == 'customersIncomingPaymentList' or param.tab == 'affiliate'}">is-open</c:if>">
							<i class="sli-people"></i><span class="title">Customers</span>
						</a>
						<div class="collapse <c:if test="${param.tab == 'customers' or param.tab == 'groups' or param.tab == 'customersIncomingPaymentList' or param.tab == 'affiliate'}">show</c:if>">
							<ul class="list-unstyled">
								<li><a class="btn btn-default btn-flat btn-sidebar btn-sidebar-2 <c:if test="${param.tab == 'customers'}">is-open</c:if>" href="${_contextpath}/admin/customers/customerList.jhtm"><i class=""></i><span class="title">Customers</span></a></li>
								<li><a class="btn btn-default btn-flat btn-sidebar btn-sidebar-2 <c:if test="${param.tab == 'groups'}">is-open</c:if>" href="${_contextpath}/admin/customers/customerGroupList.jhtm"><i class=""></i><span class="title">Groups</span></a></li>
								<c:if test="${gSiteConfig['gPAYMENTS']}">
									<sec:authorize ifAnyGranted="ROLE_AEM,ROLE_ADMIN_${prop_site_id},ROLE_INCOMING_PAYMENT_VIEW_LIST">
										<li><a class="btn btn-default btn-flat btn-sidebar btn-sidebar-2 <c:if test="${param.tab == 'customersIncomingPaymentList'}">is-open</c:if>" href="${_contextpath}/admin/customers/payments.jhtm?cid="><i class=""></i><span class="title"><fmt:message key="incomingPayments"/></span></a></li>
									</sec:authorize>
								</c:if>
								<c:if test="${gSiteConfig['gAFFILIATE'] > 0}">
      								<sec:authorize ifAnyGranted="ROLE_AEM,ROLE_ADMIN_${prop_site_id},ROLE_CUSTOMER_AFFILIATE">
      									<li><a class="btn btn-default btn-flat btn-sidebar btn-sidebar-2 <c:if test="${param.tab == 'affiliate'}">is-open</c:if>" href="${_contextpath}/admin/customers/affiliateList.jhtm"><i class=""></i><span class="title"><fmt:message key="affiliateTabTitle"/></span></a></li>
      								</sec:authorize>
    								</c:if>					
							</ul>
						</div>
					</li>
					<li>
						<a class="btn btn-default btn-flat btn-sidebar btn-sidebar-1 has-children <c:if test="${param.tab == 'promos' or param.tab == 'salestags'}">is-open</c:if>">
							<i class="sli-handbag"></i><span class="title">Sales/Promos</span>
						</a>
						<div class="collapse <c:if test="${param.tab == 'promos' or param.tab == 'salestags'}">show</c:if>">
							<ul class="list-unstyled">
								<li><a class="btn btn-default btn-flat btn-sidebar btn-sidebar-2 <c:if test="${param.tab == 'promos'}">is-open</c:if>" href="${_contextpath}/admin/promos/"><i class=""></i><span class="title">Promotions</span></a></li>
								<li><a class="btn btn-default btn-flat btn-sidebar btn-sidebar-2 <c:if test="${param.tab == 'salestags'}">is-open</c:if>" href="${_contextpath}/admin/promos/salesTagList.jhtm"><i class=""></i><span class="title">Sales Tags</span></a></li>
							</ul>
						</div>
					</li>
					<li>
						<a class="btn btn-default btn-flat btn-sidebar btn-sidebar-1 has-children <c:if test="${param.tab == 'faq' or param.tab == 'policy' or param.tab == 'ticket'}">is-open</c:if>">
							<i class="sli-support"></i><span class="title">Support</span>
						</a>
						<div class="collapse <c:if test="${param.tab == 'faq' or param.tab == 'policy' or param.tab == 'ticket'}">show</c:if>">
							<ul class="list-unstyled">
								<li><a class="btn btn-default btn-flat btn-sidebar btn-sidebar-2 <c:if test="${param.tab == 'faq'}">is-open</c:if>" href="${_contextpath}/admin/faq/faqList.jhtm"><i class=""></i><span class="title">FAQ</span></a></li>
								<li><a class="btn btn-default btn-flat btn-sidebar btn-sidebar-2 <c:if test="${param.tab == 'policy'}">is-open</c:if>" href="${_contextpath}/admin/policy/policyList.jhtm"><i class=""></i><span class="title">Policy</span></a></li>
								<li><a class="btn btn-default btn-flat btn-sidebar btn-sidebar-2 <c:if test="${param.tab == 'ticket'}">is-open</c:if>" href="${_contextpath}/admin/ticket/"><i class=""></i><span class="title">Ticket</span></a></li>
								
							</ul>
						</div>
					</li>
					<li>
						<a class="btn btn-default btn-flat btn-sidebar btn-sidebar-1 <c:if test="${model.orderName == 'quotes'}">is-open</c:if>" href="${_contextpath}/admin/orders/quotesList.jhtm">
							<i class="sli-note"></i><span class="title">Quotes</span>
						</a>
					</li>
					<li>
						<a class="btn btn-default btn-flat btn-sidebar btn-sidebar-1 has-children <c:if test="${model.orderName == 'orders'}">is-open</c:if>">
							<i class="sli-basket"></i><span class="title">Orders</span>
						</a>
						<div class="collapse <c:if test="${model.orderName == 'orders'}">show</c:if>">
							<ul class="list-unstyled">
								<li><a class="btn btn-default btn-flat btn-sidebar btn-sidebar-2 <c:if test="${model.orderName == 'orders'}">is-open</c:if>" href="${_contextpath}/admin/orders/"><i class=""></i><span class="title">Orders</span></a></li>
							</ul>
						</div>
					</li>
					<li>
						<a href="${_contextpath}/admin/supplier/" class="btn btn-default btn-flat btn-sidebar btn-sidebar-1 <c:if test="${param.tab == 'supplier'}">is-open</c:if>">
							<i class="sli-rocket"></i><span class="title">Supplier</span>
						</a>
					</li>
					<li>
						<a class="btn btn-default btn-flat btn-sidebar btn-sidebar-1 has-children <c:if test="${param.tab == 'ordersDaily' or param.tab == 'orderDetailReport' or param.tab == 'customersReport' or param.tab == 'customersInactiveReport' or param.tab == 'salesRepsReport' or param.tab == 'salesRepsDailyReport' or param.tab == 'salesRepsDetailReport' or param.tab == 'salesRepsDetailReport2' or param.tab == 'metricOverviewReport' or param.tab == 'trackcodeReport' or param.tab == 'importExportReport' or param.tab == 'productReport' or param.tab == 'productReport2' or param.tab == 'abandonedShoppingCart'}">is-open</c:if>">
							<i class="sli-graph"></i><span class="title">Report</span>
						</a>
						<div class="collapse <c:if test="${param.tab == 'ordersDaily' or param.tab == 'orderDetailReport' or param.tab == 'customersReport' or param.tab == 'customersInactiveReport' or param.tab == 'salesRepsReport' or param.tab == 'salesRepsDailyReport' or param.tab == 'salesRepsDetailReport' or param.tab == 'salesRepsDetailReport2' or param.tab == 'metricOverviewReport' or param.tab == 'trackcodeReport' or param.tab == 'importExportReport' or param.tab == 'productReport' or param.tab == 'productReport2' or param.tab == 'abandonedShoppingCart'}">show</c:if>">
							<ul class="list-unstyled">
								<li><a class="btn btn-default btn-flat btn-sidebar btn-sidebar-2 <c:if test="${param.tab == 'ordersDaily'}">is-open</c:if>" href="${_contextpath}/admin/reports/dailyOrderReport.jhtm"><i class=""></i><span class="title">Orders Daily</span></a></li>
								<li><a class="btn btn-default btn-flat btn-sidebar btn-sidebar-2 <c:if test="${param.tab == 'orderDetailReport'}">is-open</c:if>" href="${_contextpath}/admin/reports/orderDetailReport.jhtm"><i class=""></i><span class="title">Orders Overview</span></a></li>
								<li><a class="btn btn-default btn-flat btn-sidebar btn-sidebar-2 <c:if test="${param.tab == 'customersReport'}">is-open</c:if>" href="${_contextpath}/admin/reports/customerReport.jhtm"><i class=""></i><span class="title">Customers Overview</span></a></li>
								<li><a class="btn btn-default btn-flat btn-sidebar btn-sidebar-2 <c:if test="${param.tab == 'customersInactiveReport'}">is-open</c:if>" href="${_contextpath}/admin/reports/customerInactiveReport.jhtm"><i class=""></i><span class="title">Customers Inactive</span></a></li>
								<li><a class="btn btn-default btn-flat btn-sidebar btn-sidebar-2 <c:if test="${param.tab == 'salesRepsReport'}">is-open</c:if>" href="${_contextpath}/admin/reports/salesRepReport.jhtm"><i class=""></i><span class="title">Sales Rep</span></a></li>
								<li><a class="btn btn-default btn-flat btn-sidebar btn-sidebar-2 <c:if test="${param.tab == 'salesRepsDailyReport'}">is-open</c:if>" href="${_contextpath}/admin/reports/salesRepReportDaily.jhtm"><i class=""></i><span class="title">Sales RepDaily</span></a></li>
								<li><a class="btn btn-default btn-flat btn-sidebar btn-sidebar-2 <c:if test="${param.tab == 'salesRepsDetailReport'}">is-open</c:if>" href="${_contextpath}/admin/reports/salesRepDetailReport.jhtm"><i class=""></i><span class="title">SalesRep Overview</span></a></li>
								<li><a class="btn btn-default btn-flat btn-sidebar btn-sidebar-2 <c:if test="${param.tab == 'salesRepsDetailReport2'}">is-open</c:if>" href="${_contextpath}/admin/reports/salesRepDetailReport2.jhtm"><i class=""></i><span class="title">SalesRep Overview II</span></a></li>
								<li><a class="btn btn-default btn-flat btn-sidebar btn-sidebar-2 <c:if test="${param.tab == 'metricOverviewReport'}">is-open</c:if>" href="${_contextpath}/admin/reports/metricOverviewReport.jhtm"><i class=""></i><span class="title">Customer Metric Overview</span></a></li>
								<li><a class="btn btn-default btn-flat btn-sidebar btn-sidebar-2 <c:if test="${param.tab == 'trackcodeReport'}">is-open</c:if>" href="${_contextpath}/admin/reports/trackcodeReport.jhtm"><i class=""></i><span class="title">Track Code</span></a></li>
								<li><a class="btn btn-default btn-flat btn-sidebar btn-sidebar-2 <c:if test="${param.tab == 'importExportReport'}">is-open</c:if>" href="${_contextpath}/admin/reports/importExportHistory.jhtm"><i class=""></i><span class="title">Import/Export</span></a></li>
								<li><a class="btn btn-default btn-flat btn-sidebar btn-sidebar-2 <c:if test="${param.tab == 'productReport'}">is-open</c:if>" href="${_contextpath}/admin/reports/productListReport.jhtm"><i class=""></i><span class="title">Product Report</span></a></li>
								<li><a class="btn btn-default btn-flat btn-sidebar btn-sidebar-2 <c:if test="${param.tab == 'abandonedShoppingCart'}">is-open</c:if>" href="${_contextpath}/admin/reports/abandonedShoppingCartController.jhtm"><i class=""></i><span class="title">Abandoned Shopping Cart</span></a></li>
							</ul>
						</div>
					</li>
					<li>
						<a class="btn btn-default btn-flat btn-sidebar btn-sidebar-1 has-children <c:if test="${param.tab == 'crmAccount' or param.tab == 'crmContact' or param.tab == 'crmTask' or param.tab == 'crmForm' or param.tab == 'crmContactField' or param.tab == 'crmGroup'}">is-open</c:if>">
							<i class="sli-pie-chart"></i><span class="title">CRM</span>
						</a>
						<div class="collapse <c:if test="${param.tab == 'crmAccount' or param.tab == 'crmContact' or param.tab == 'crmTask' or param.tab == 'crmForm' or param.tab == 'crmContactField' or param.tab == 'crmGroup'}">show</c:if>">
							<ul class="list-unstyled">
								<li><a class="btn btn-default btn-flat btn-sidebar btn-sidebar-2 <c:if test="${param.tab == 'crmAccount'}">is-open</c:if>" href="${_contextpath}/admin/crm/crmAccountList.jhtm"><i class=""></i><span class="title">Accounts</span></a></li>
								<li><a class="btn btn-default btn-flat btn-sidebar btn-sidebar-2 <c:if test="${param.tab == 'crmContact'}">is-open</c:if>" href="${_contextpath}/admin/crm/crmContactList.jhtm"><i class=""></i><span class="title">Contacts</span></a></li>
								<li><a class="btn btn-default btn-flat btn-sidebar btn-sidebar-2 <c:if test="${param.tab == 'crmTask'}">is-open</c:if>" href="${_contextpath}/admin/crm/crmTaskList.jhtm"><i class=""></i><span class="title">Tasks</span></a></li>
								<li><a class="btn btn-default btn-flat btn-sidebar btn-sidebar-2 <c:if test="${param.tab == 'crmForm'}">is-open</c:if>" href="${_contextpath}/admin/crm/crmFormList.jhtm"><i class=""></i><span class="title">Forms</span></a></li>
								<li><a class="btn btn-default btn-flat btn-sidebar btn-sidebar-2 <c:if test="${param.tab == 'crmContactField'}">is-open</c:if>" href="${_contextpath}/admin/crm/crmContactFieldList.jhtm"><i class=""></i><span class="title">Fields</span></a></li>
								<li><a class="btn btn-default btn-flat btn-sidebar btn-sidebar-2 <c:if test="${param.tab == 'crmGroup'}">is-open</c:if>" href="${_contextpath}/admin/crm/crmContactGroupList.jhtm"><i class=""></i><span class="title">Groups</span></a></li>
							</ul>
						</div>
					</li>
				</ul>
			</div>
		</div>
	</div>
</div>