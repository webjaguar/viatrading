<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/security/tags" prefix="sec" %> 
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ taglib uri="/WEB-INF/tlds/custom.tld" prefix="wj" %>

<tiles:insertDefinition name="admin.responsive-template.inventory.po" flush="true">
	<tiles:putAttribute name="css">
		<style>
			#grid .field-po_number{
			    width: 120px;
			}
			#grid .field-sub_total{
			    text-align: right;
			}
			#grid .field-shipping_quote{
			    width: 120px;
			}
			#grid .field-order_by{
			    width: 120px;
			}
			#grid .field-subStatus{
			    width: 120px;
			}
			#grid .field-subStatusGroup{
			    width: 160px;
			}
			#grid .field-company{
			    width: 150px;
			}
			#grid .field-supplier_invoice_num{
			    width: 200px;
			}
		</style>
	</tiles:putAttribute>
	<tiles:putAttribute name="content" type="string">
		<sec:authorize ifAnyGranted="ROLE_AEM,ROLE_ADMIN_${prop_site_id},ROLE_PURCHASE_ORDER_CREATE,ROLE_PURCHASE_ORDER_VIEW_LIST">
			<c:if test="${gSiteConfig['gINVENTORY'] and gSiteConfig['gPURCHASE_ORDER']}">
				<form action="purchaseOrders.jhtm" method="post"  id="list" name="list_form"> 
					<div class="page-banner">
						<div class="breadcrumb"><span><a href="${_contextpath}/admin/catalog/dashboard.jhtm">Home</a></span><i class="sli-arrow-right"></i><span>Purchase Orders</span></div>
					</div>
					<div class="page-head">
						<div class="row justify-content-between">
							<div class="col-sm-8 title">
								<h3>Purchase Orders</h3>
							</div>
							<div class="col-sm-4 actions">
							  	<div class="dropdown dropdown-actions">
									<button type="button" class="btn-outline dropdown-toggle active btn btn-primary" data-toggle="dropdown">
			                             <i class="sli-settings"></i> Actions
			                         </button>
			                         <div class="dropdown-menu dropdown-menu-right">
				                        <sec:authorize ifAnyGranted="ROLE_AEM,ROLE_ADMIN_${prop_site_id},ROLE_PURCHASE_ORDER_CREATE"> 
											<a class="dropdown-item"><fmt:message key='add' /> <fmt:message key='purchaseOrders'/></a>
									  	</sec:authorize>
			                         </div>
			                    </div>
			                    <div class="dropdown dropdown-setting">
									<button type="button" class="btn-outline dropdown-toggle active btn btn-primary" data-toggle="dropdown">
			                             <i class="sli-screen-desktop"></i> Display
			                         </button>	                         
									<div class="dropdown-menu dropdown-menu-right">
										<div class="form-group">
					                    		<div class="option-header">Rows</div>
											<select class="custom-select form-control" name="size" onchange="document.getElementById('page').value=1;submit();" title="Page Size">
												<c:forTokens items="10,25,50,100,200,500" delims="," var="current">
										  	  		<option value="${current}" <c:if test="${current == purchaseOrders.pageSize}">selected</c:if>>${current}</option>
									  			</c:forTokens>
											</select>
										</div>
										<div class="form-group">
					                    		<div class="option-header">Columns</div>
					                    		<div id="column-hider"></div>
										</div>
									</div>
			                    </div>
							</div>
						</div>
					</div>
					<div class="page-body">
						<div class="grid-stage">
							<c:if test="${model.purchaseOrders.nrOfElements > 0}">
								<div class="page-stat">
									<fmt:message key="showing">
										<fmt:param value="${model.purchaseOrders.firstElementOnPage + 1}" />
										<fmt:param value="${model.purchaseOrders.lastElementOnPage + 1}" />
										<fmt:param value="${model.purchaseOrders.nrOfElements}" />
									</fmt:message>
								</div>
							</c:if> 
							<table id="grid"></table>
							<div class="footer">
								 <div class="float-left">
				                    <p class="text-red mb-0"><i class="sli-check"></i> <fmt:message key="dropShip" /></p>
				                </div>
				                <div class="float-right">
									<c:if test="${model.purchaseOrders.firstPage}">
								 		<span class="previous"><i class="sli-arrow-left" style="font-size: 0.7rem"></i> Previous</span>
									</c:if>
									<c:if test="${not model.purchaseOrders.firstPage}">
										<a href="<c:url value="purchaseOrders.jhtm"><c:param name="page" value="${model.purchaseOrders.page}"/><c:param name="size" value="${model.purchaseOrders.pageSize}"/></c:url>"
											<span class="previous"><i class="sli-arrow-left" style="font-size: 0.7rem"></i> Previous</span>
										</a>
									</c:if>
									<span class="status"><c:out value="${model.purchaseOrders.page+1}" /> of <c:out value="${model.purchaseOrders.pageCount}" /></span>
									<c:if test="${model.purchaseOrders.lastPage}">
									 	<span class="next">Next <i class="sli-arrow-right" style="font-size: 0.7rem"></i></span>
									</c:if>
									<c:if test="${not model.purchaseOrders.lastPage}">
										<a href="<c:url value="purchaseOrders.jhtm"><c:param name="page" value="${model.purchaseOrders.page+2}"/><c:param name="size" value="${model.purchaseOrders.pageSize}"/></c:url>"
											<span class="next">Next <i class="sli-arrow-right" style="font-size: 0.7rem"></i></span>
										</a>
									</c:if>
								</div>
							</div>
						</div>
					</div>
				    <input type="hidden" name="pid" value="${model.pid}">
					<input type="hidden" id="sort" name="sort" value="${purchaseOrderSearch.sort}" />
				</form>
			</c:if>
		</sec:authorize>
	</tiles:putAttribute>
	<tiles:putAttribute name="js">
		<!-- DGRID TABLE SCRIPT MUST BE AT BOTTOM OF THE PAGE -->
		<script src="${_contextpath}/admin-responsive-static/assets/plugins/dojo/dojo.js" data-dojo-config="async: true"></script>
		<script>
			//Table Data Sanitization
			var data = [];
			<c:forEach items="${model.purchaseOrders.pageList}" var="po" varStatus="status">
				data.push({
					id: '${po.poId}',
					offset: '${status.count + model.purchaseOrders.firstElementOnPage}.',
					po_number: '${po.poNumber}',
					po_status: '<fmt:message key="${po.statusName}" />',
					created: '<fmt:formatDate type="both" timeStyle="full" pattern="MM/dd/yyyy" value="${po.created}"/>',
					due_date: '<fmt:formatDate type="both" timeStyle="full" pattern="MM/dd/yyyy" value="${po.dueDate}"/>',
					sub_total: '<fmt:formatNumber value="${po.subTotal}" pattern="#,##0.00"/>',
					ship_via: '${wj:escapeJS(po.shipVia)}',
					shipping_quote: '<fmt:formatNumber value="${po.shippingQuote}" pattern="#,##0.00"/>',
					order_by: "${wj:escapeJS(po.orderBy)}",
		  	  		company: '${po.company}',
		  	  		supplier_invoice_num: '${po.supplierInvoiceNumber }',
		  	  		note: "${wj:escapeJS(po.noteTrimmed)}",
				});
			</c:forEach>
			
			//Columns Definitions
			require(['dojo/_base/lang',
					'dojo/_base/declare',
					'dstore/Memory',
					'dstore/Trackable',
					'dgrid/OnDemandGrid',
					'dgrid/Editor',
					'dgrid/extensions/ColumnHider',
					'dgrid/extensions/ColumnReorder',
					'dgrid/extensions/ColumnResizer',
					'dgrid/Selector',
					'dijit/form/DropDownButton',
					'dijit/DropDownMenu',
					'dijit/MenuItem'
				], function (lang, declare, Memory, Trackable, OnDemandGrid, Editor, ColumnHider, ColumnReorder, ColumnResizer, Selector, DropDownButton, DropDownMenu, MenuItem) {
				var columns = {
					offset: {
						label: '#',
						get: function(object){
							return object.offset;
						},
						sortable: false,
						unhidable: true
					},
					po_number: {
						label: '<fmt:message key="poNumber" />',
						renderCell: function(object, data, td, options){
							var a = document.createElement("a");
							a.innerHTML = object.po_number
							a.href = 'purchaseOrderForm2.jhtm?poId='+object.id;
							return a;
						},
					},
					po_status: {
						label: '<fmt:message key="status" />',
						get: function(object){
							return object.po_status;
						},
						sortable: false
					},
					created: {
						label: '<fmt:message key="created" />',
						get: function(object){
							return object.created;
						},
					},
					due_date: {
						label: '<fmt:message key="dueDate" />',
						get: function(object){
							return object.due_date;
						},
					},
					sub_total: {
						label: '<fmt:message key="total" />',
						get: function(object){
							return object.sub_total;
						},
					},
					ship_via: {
						label: '<fmt:message key="shipVia" />',
						get: function(object){
							return object.ship_via;
						},
					},
					shipping_quote: {
						label: '<fmt:message key="shippingQuote" />',
						get: function(object){
							return object.shipping_quote;
						},
					},
					order_by: {
						label: '<fmt:message key="orderBy" />',
						get: function(object){
							return object.order_by;
						},
					},
					status: {
						label: '<fmt:message key="order" /><fmt:message key="status" />',
						get: function(object){
							return object.status;
						},
						sortable: false
					},
					subStatus: {
						label: '<fmt:message key="order" /><fmt:message key="subStatus" />',
						get: function(object){
							return object.subStatus;
						},
						sortable: false
					},
					subStatusGroup: {
						label: '<fmt:message key="order" /><fmt:message key="subStatus" /> <fmt:message key="group" />',
						get: function(object){
							return object.subStatusGroup;
						},
						sortable: false
					},
					company: {
						label: '<fmt:message key="supplier" />',
						get: function(object){
							return object.company;
						},
					},
					supplier_invoice_num: {
						label: '<fmt:message key="supplierInvoiceNumber" />',
						get: function(object){
							return object.supplier_invoice_num;
						},
					},
					note: {
						label: '<fmt:message key="note" />',
						get: function(object){
							return object.note;
						},
						sortable: false
					},
				};
				var store = new (declare([Memory, Trackable]))({
					data: data,
					idProperty:"id"
				});
				var grid = new (declare([OnDemandGrid, Editor, ColumnHider, ColumnReorder, ColumnResizer, Selector]))({
					collection: store,
					selectionMode: "none",
					allowSelectAll: true,
					columns: columns
				},'grid');
				
				//sorting columns
				grid.on("dgrid-sort", lang.hitch(this, function(e){
					e.preventDefault();						
					var property = e.sort[0].property;
				    var order = e.sort[0].descending ? "desc" : "asc";
				    document.getElementById('sort').value = property + ' ' + order;
				    document.getElementById('list').submit();
				}));
				<c:if test="${purchaseOrderSearch.sort == 'po_number desc'}">
					grid.updateSortArrow([{property: 'po_number', descending: true}],true);
				</c:if>
				<c:if test="${purchaseOrderSearch.sort == 'po_number asc'}">
					grid.updateSortArrow([{property: 'po_number', ascending: true}],true);
				</c:if>
				
				<c:if test="${purchaseOrderSearch.sort == 'next_activity_date desc'}">
					grid.updateSortArrow([{property: 'next_activity_date', descending: true}],true);
				</c:if>
				<c:if test="${purchaseOrderSearch.sort == 'next_activity_date asc'}">
					grid.updateSortArrow([{property: 'next_activity_date', ascending: true}],true);
				</c:if>
				
				<c:if test="${purchaseOrderSearch.sort == 'created desc'}">
					grid.updateSortArrow([{property: 'created', descending: true}],true);
				</c:if>
				<c:if test="${purchaseOrderSearch.sort == 'created asc'}">
					grid.updateSortArrow([{property: 'created', ascending: true}],true);
				</c:if>
				
				<c:if test="${purchaseOrderSearch.sort == 'due_date desc'}">
					grid.updateSortArrow([{property: 'due_date', descending: true}],true);
				</c:if>
				<c:if test="${purchaseOrderSearch.sort == 'due_date asc'}">
					grid.updateSortArrow([{property: 'due_date', ascending: true}],true);
				</c:if>
				
				<c:if test="${purchaseOrderSearch.sort == 'sub_total desc'}">
					grid.updateSortArrow([{property: 'sub_total', descending: true}],true);
				</c:if>
				<c:if test="${purchaseOrderSearch.sort == 'sub_total asc'}">
					grid.updateSortArrow([{property: 'sub_total', ascending: true}],true);
				</c:if>
				
				<c:if test="${purchaseOrderSearch.sort == 'ship_via desc'}">
					grid.updateSortArrow([{property: 'ship_via', descending: true}],true);
				</c:if>
				<c:if test="${purchaseOrderSearch.sort == 'ship_via asc'}">
					grid.updateSortArrow([{property: 'ship_via', ascending: true}],true);
				</c:if>
				
				<c:if test="${purchaseOrderSearch.sort == 'shipping_quote desc'}">
					grid.updateSortArrow([{property: 'shipping_quote', descending: true}],true);
				</c:if>
				<c:if test="${purchaseOrderSearch.sort == 'shipping_quote asc'}">
					grid.updateSortArrow([{property: 'shipping_quote', ascending: true}],true);
				</c:if>
				
				<c:if test="${purchaseOrderSearch.sort == 'order_by desc'}">
					grid.updateSortArrow([{property: 'order_by', descending: true}],true);
				</c:if>
				<c:if test="${purchaseOrderSearch.sort == 'order_by asc'}">
					grid.updateSortArrow([{property: 'order_by', ascending: true}],true);
				</c:if>
				
				<c:if test="${purchaseOrderSearch.sort == 'company desc'}">
					grid.updateSortArrow([{property: 'company', descending: true}],true);
				</c:if>
				<c:if test="${purchaseOrderSearch.sort == 'company asc'}">
					grid.updateSortArrow([{property: 'company', ascending: true}],true);
				</c:if>
				
				<c:if test="${purchaseOrderSearch.sort == 'supplier_invoice_num desc'}">
					grid.updateSortArrow([{property: 'supplier_invoice_num', descending: true}],true);
				</c:if>
				<c:if test="${purchaseOrderSearch.sort == 'supplier_invoice_num asc'}">
					grid.updateSortArrow([{property: 'supplier_invoice_num', ascending: true}],true);
				</c:if>

				grid.on('dgrid-refresh-complete', function(event) {
					$('#grid-hider-menu').appendTo('#column-hider');
				});
				
				grid.startup();
			});
		</script>
	</tiles:putAttribute>
</tiles:insertDefinition>