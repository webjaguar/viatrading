<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ taglib uri="/WEB-INF/tlds/spring.tld" prefix="spring" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib uri="http://www.springframework.org/security/tags" prefix="sec" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">

    <!-- Title -->
    <title>DropShip</title>

    <!-- Favicon -->
    <link rel="shortcut icon" href="/assets/admin/assets/img/favicon.ico">

    <!-- ========================= BEGIN: CSS ========================= -->

    <!-- Font CSS (Via CDN) -->
    <link rel='stylesheet' type="text/css" href="http://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700">

	<!-- Theme CSS -->
	<!--<link rel="stylesheet" type="text/css" href="${_contextpath}/admin-static/assets/skin/default_skin/css/theme.css">  -->
	<link rel="stylesheet" type="text/css" href="${_contextpath}/admin-static/assets/css/theme.css">

    <!-- Admin Forms CSS -->
    <link rel="stylesheet" type="text/css" href="${_contextpath}/admin-static/assets/css/admin-forms.css">

	<!-- FooTable Plugin CSS -->
	<link rel="stylesheet" type="text/css" href="${_contextpath}/admin-static/assets/plugins/footable/css/footable.core.min.css">

    <!-- ========================= END: CSS ========================= -->

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
    <script src="/assets/admin/assets/js/html5shiv.min.js"></script>
    <script src="/assets/admin/assets/js/respond.min.js"></script>
    <![endif]-->

</head>

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">

    <!-- Title -->
    <title>Invoice</title>

    <!-- Favicon -->
    <link rel="shortcut icon" href="/assets/admin/assets/img/favicon.ico">

    <!-- ========================= BEGIN: CSS ========================= -->

    <!-- Font CSS (Via CDN) -->
    <link rel='stylesheet' type="text/css" href="http://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700">

   	<!-- Theme CSS -->
	<link rel="stylesheet" type="text/css" href="${_contextpath}/admin-static/assets/skin/default_skin/css/theme.css">

   	<!-- Admin Forms CSS -->
	<link rel="stylesheet" type="text/css" href="${_contextpath}/admin-static/assets/admin-tools/admin-forms/css/admin-forms.min.css">	

   	<!-- FooTable Plugin CSS -->
	<link rel="stylesheet" type="text/css" href="${_contextpath}/admin-static/assets/plugins/footable/css/footable.core.min.css">

    <!-- ========================= END: CSS ========================= -->

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
    <script src="/assets/admin/assets/js/html5shiv.min.js"></script>
    <script src="/assets/admin/assets/js/respond.min.js"></script>
    <![endif]-->

</head>


<style type="text/css">
html {
overflow-x: hidden;
}
</style>   
<body style="background-color:#ffffff;">
<script type="text/javascript">
<!--
function updateItems(orderId, supplierId, dropShip){
	$('lineItmes').set('style','background:url(/assets/Image/Layout/spinner.gif) no-repeat center 50%; background-color : #EEEEEE;');
	$('lineItmes').fade('0.1');
	var requestHTMLData = new Request.HTML ({
		url: "../inventory/ajaxShowOrderLineItems.jhtm?&orderId="+orderId+"&supplierId="+supplierId+'&dropShip='+dropShip,
		onSuccess: function(response){ 
		},
		update: $('lineItmes')
	}).send();
	$('lineItmes').fade('1.0');
	$('lineItmes').set('style','background:url(none)');
}
function toggleAll(el) {
	var cbs = document.getElementsByName('__include_item');	
	for(var i=0; i < cbs.length; i++) {
	    if(cbs[i].type == 'checkbox') {
	      cbs[i].checked = el.checked;
	    }
	  }
	}
	

function addToPO(orderId, supplierId, dropShip){
		var itemSelected = false;
		var urlAppend = '?orderId='+orderId+'&supplierId='+supplierId+'&dropship='+dropShip;
		$('input.check').each(function(index, input){
			console.log("value is  " + $(input).val() );
			if(input.checked){
				itemSelected = true;
				urlAppend = urlAppend + '&__include_item='+input.value;
			}
		});
		if(!itemSelected) {
			alert("Please select Line Item(s).");       
			return false;
		}
		//parent.mbPOBox.close();
		parent.jQuery.fancybox.close();
	    setTimeout(function() {parent.location = "../inventory/purchaseOrder-ajax-Form.jhtm"+urlAppend;},250);
}
//-->
</script>

<!-- Start: Main -->
<div id="">
    <div class="panel">
        <div class="panel-body">
            <h3 class="mt0">Create Purchase Order</h3>

            <form class="form-inline mb20">
                <div class="text-center">
                    <div class="form-group center-children">
                        <label for="supplier"><fmt:message key="supplier"/>:</label> 
                        <select  class="form-control input-sm" name="supplier" onchange="updateItems('${model.order.orderId}', this.value, '${model.dropShip}')">
						  <option value="">Please Select</option>
						  <c:forEach items="${model.supplierList}" var="supplier">
							<option value="${supplier.id}" <c:if test="${model.supplierId == supplier.id}">selected="selected"</c:if>> <c:out value="${supplier.address.company}"/> </option>
						  </c:forEach>
						</select>
                    </div>
                </div>
            </form>

            <div class="panel">
                <div class="panel-body pn">
                    <table class="footable toggle-square-filled toggle-medium table table-hover admin-form tc-checkbox-1" data-sort="false">
                        <thead>
                        <tr class="bg-light">
                            <th class="mw10" data-toggle="true">
                                Include <input type="checkbox" onclick="toggleAll(this)">
                            </th>
                            <th class="w100">
                                #
                            </th>
                            <th class="">
                                <fmt:message key="productSku" />
                            </th>
                            <th class="" data-hide="phone,tablet">
                                <fmt:message key="productName" />
                            </th>
                            <th class="w100 text-center" data-hide="phone,tablet">
                                <fmt:message key="quantity" />
                            </th>
                        </tr>
                        </thead>
                        <tbody> 
	                        <c:forEach var="lineItem" items="${model.lineItems}" varStatus="status">
	                        	<tr class="">
	                            <td class="text-center" >  
	                                <label class="option block mn" style="float:left;">  
	                                	<input type="checkbox" name="__include_item" value="${lineItem.lineNumber}" class="check"/> 
	                                    <span class="checkbox mn"></span> 
	                                </label>
	                            </td>
	                            <td class="">
	                               <c:out value="${status.count}"/>.
	                            </td>
	                            <td class="">
	                                <c:out value="${lineItem.product.sku}"/>
	                            </td>
	                            <td class="">
	                               <c:out value="${lineItem.product.name}" escapeXml="false" />
	                            </td>
	                            <td class="text-center">
	                                <c:out value="${lineItem.quantity}"/>
	                            </td>
	                        </tr>
	                        </c:forEach>
                        </tbody>
                    </table>
                </div>
            </div>

            <!--
            <input type="button" name="submit" value="Add To PO" onclick="addToPO('${model.order.orderId}', '${model.supplierId}', '${model.dropShip}')">	
              -->
            <button type="button" name="submit" class="btn btn-primary btn-gradient" onclick="addToPO('${model.order.orderId}', '${model.supplierId}', '${model.dropShip}')">
                <i class="fa fa-plus mr5"></i> Add To PO
            </button>
        </div>
    </div>
</div>
<!-- End: Main -->

<!-- ========================= BEGIN: PAGE SCRIPTS ========================= -->

<!-- jQuery -->
<script src="${_contextpath}/admin-static/assets/jquery/jquery-1.11.1.min.js"></script>
<script src="${_contextpath}/admin-static/assets/jquery/jquery_ui/jquery-ui.min.js"></script>

<!-- FooTable Plugin -->
<script src="${_contextpath}/admin-static/assets/plugins/footable/footable.all.min.js"></script>
	
<!-- FooTable Addon -->
<script src="${_contextpath}/admin-static/assets/plugins/footable//footable.filter.min.js"></script>

<script type="text/javascript">
    (function ($) {
        $(function () {
            "use strict";
 
            $('.table').find('thead input:checkbox.checkAll').click(function (event) {
                var checked = !!event.currentTarget.checked;
                $('.table').find('tbody input:checkbox.check').prop('checked', checked);
            });
        });
    })(jQuery);
</script>

<!-- ========================= END: PAGE SCRIPTS ========================= -->


</body>
</html>