<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="/WEB-INF/tlds/spring.tld" prefix="spring" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ taglib uri="http://www.springframework.org/security/tags" prefix="sec" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ taglib uri="/WEB-INF/tlds/custom.tld" prefix="wj" %>

<tiles:insertDefinition name="admin.responsive-template.policy" flush="true">
	<tiles:putAttribute name="css">
		<style>
			#grid .field-name{
			    width: 300px;
			}
		</style>
	</tiles:putAttribute> 
	<tiles:putAttribute name="content" type="string"> 
		<form action="policyList.jhtm" method="get">
			<div class="page-banner">
				<div class="breadcrumb"><span><a href="${_contextpath}/admin/catalog/dashboard.jhtm">Home</a></span><i class="sli-arrow-right"></i><span>Policies</span></div>
			</div>
	  		<div class="page-head">
				<div class="row justify-content-between">
					<div class="col-sm-8 title">
						<h3>Policies</h3>
					</div>
					<div class="col-sm-4 actions">
						<div class="dropdown dropdown-actions">
							<button type="button" class="btn-outline dropdown-toggle active btn btn-primary" data-toggle="dropdown">
	                             <i class="sli-settings"></i> Actions
	                         </button>
	                         <div class="dropdown-menu dropdown-menu-right">
								<sec:authorize ifAnyGranted="ROLE_AEM,ROLE_ADMIN_${prop_site_id},ROLE_POLICY_CREATE">
									<a class="dropdown-item"><fmt:message key="policyAdd" /></a>
								</sec:authorize>
								<a class="dropdown-item"><fmt:message key="updateRanking" /></a>
	                         </div>
	                    </div>
					</div>
				</div>
			</div>
			<div class="page-body">
				<div class="grid-stage">
					<c:if test="${model.policies.nrOfElements > 0}">
						<div class="page-stat">
							<fmt:message key="showing">
								<fmt:param value="${model.policies.firstElementOnPage + 1}"/>
								<fmt:param value="${model.policies.lastElementOnPage + 1}"/>
								<fmt:param value="${model.policies.nrOfElements}"/>
							</fmt:message>
						</div>
					</c:if>
		            <table id="grid"></table>
					<div class="footer">
		                <div class="float-right">
							<c:if test="${model.policies.firstPage}">
						 		<span class="previous"><i class="sli-arrow-left" style="font-size: 0.7rem"></i> Previous</span>
							</c:if>
							<c:if test="${not model.policies.firstPage}">
								<a href="<c:url value="policyList.jhtm"><c:param name="page" value="${model.policies.page}"/></c:url>" >
									<span class="previous"><i class="sli-arrow-left" style="font-size: 0.7rem"></i> Previous</span>
								</a>
							</c:if>
							<span class="status"><c:out value="${model.policies.page+1}" /> of <c:out value="${model.policies.pageCount}"/></span>
							<c:if test="${model.policies.lastPage}">
							 	<span class="next">Next <i class="sli-arrow-right" style="font-size: 0.7rem"></i></span>
							</c:if>
							<c:if test="${not model.policies.lastPage}">
								<a href="<c:url value="policyList.jhtm"><c:param name="page" value="${model.policies.page+2}"/></c:url>">
									<span class="next">Next <i class="sli-arrow-right" style="font-size: 0.7rem"></i></span>
								</a>
							</c:if>
						</div>
					</div>
				</div>
			</div>
		</form>
	</tiles:putAttribute>
	<tiles:putAttribute name="js">
		<!-- DGRID TABLE SCRIPT MUST BE AT BOTTOM OF THE PAGE -->
		<script src="${_contextpath}/admin-responsive-static/assets/plugins/dojo/dojo.js" data-dojo-config="async: true"></script>
		<script>
			function generateMainGrid(){
				//Table Data Sanitization
				var data = [];
				<c:forEach items="${model.policies.pageList}" var="policy" varStatus="status">
					data.push({
						id: '${policy.id}',
						offset: '<c:out value="${status.count + model.policies.firstElementOnPage}"/>.',
						name: '${wj:escapeJS(policy.name)}',
						rank: '${policy.rank}',
					});
				</c:forEach>
				
				require(["dgrid/List", "dgrid/OnDemandGrid", "dgrid/Selection", "dgrid/Keyboard", "dgrid/Editor", "dijit/form/CheckBox", 
						"dijit/form/TimeTextBox", "dijit/form/Select", "dojo/data/ObjectStore", "dijit/form/FilteringSelect",
						"dojo/_base/declare", "dojo/_base/lang", "dojo/store/Memory", "dojo/store/Observable", "dgrid/test/data/createSyncStore", 
						'dgrid/extensions/ColumnReorder', 'dgrid/extensions/ColumnResizer',
					], function(List, OnDemandGrid, Selection, Keyboard, Editor, CheckBox, TimeTextBox, Select, ObjectStore, FilteringSelect,
							declare, lang, LegacyMemory, LegacyObservable, createSyncStore, ColumnReorder, ColumnResizer){

					//Column Definitions
					var columns = {
						offset: {
							label: '',
							get: function(object){ 
								return object.offset;
							},
							sortable: false,
						},
						name: {
							label: '<fmt:message key="policyName" />',
							renderCell: function(object){
								var link = document.createElement('a');
								link.text = object.name;
								link.href = 'policy.jhtm?id='+object.id;
								return link;
							},
							sortable: false,
						},
						rank: {
							editor: 'text',
							label: '<fmt:message key="rank" />',
							sortable: false,
						},
					};
					
					var grid = new (declare([OnDemandGrid, Selection, Keyboard, Editor, ColumnReorder, ColumnResizer]))({
						collection: createSyncStore({ data: data }),
						columns: columns,
						selectionMode: "none",
					},'grid');
					
					grid.startup();
				});
			}
			generateMainGrid();
		</script>
	</tiles:putAttribute>    
</tiles:insertDefinition>