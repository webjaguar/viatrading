<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="/WEB-INF/tlds/spring.tld" prefix="spring" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ taglib uri="http://www.springframework.org/security/tags" prefix="sec" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ taglib uri="/WEB-INF/tlds/custom.tld" prefix="wj" %>

<tiles:insertDefinition name="admin.responsive-template.customers.affiliate" flush="true">  
	<tiles:putAttribute name="css">
		<style>
			#grid .field-username{
			    width: 200px;
			}
			#grid .field-accountNumber{
			    width: 120px;
			}
			#grid .field-created{
			    width: 120px;
			}
			#grid .field-imported{
			    text-align: center;
			}
			#grid .field-numOfLogins{
			    text-align: right;
			}
			#grid .field-affiliateNumChild{
			    text-align: right;
			}
			#grid .field-affiliateNumOrder{
			    text-align: right;
			}
			#grid .field-commissionTotal{
			    text-align: right;
			}
			#grid .field-trackcode{
			    width: 150px;
			}
		</style>
	</tiles:putAttribute>
	<tiles:putAttribute name="content" type="string">
		<sec:authorize ifAnyGranted="ROLE_AEM,ROLE_ADMIN_${prop_site_id},ROLE_CUSTOMER_VIEW_LIST,ROLE_CUSTOMER_AFFILIATE">
			<form action="affiliateList.jhtm" id="list" method="post"> 
				<div class="page-banner">
				<div class="breadcrumb"><span><a href="${_contextpath}/admin/catalog/dashboard.jhtm">Home</a></span><i class="sli-arrow-right"></i><span><a href="${_contextpath}/admin/customers/">Customers</a></span><i class="sli-arrow-right"></i><span>Affiliates</span></div>
			</div>
	  		<div class="page-head">
				<div class="row justify-content-between">
					<div class="col-sm-8 title">
						<h3>Affiliates</h3>
					</div>
					<div class="col-sm-4 actions">
						<div class="dropdown dropdown-setting">
							<button type="button" class="btn-outline dropdown-toggle active btn btn-primary" data-toggle="dropdown">
	                             <i class="sli-screen-desktop"></i> Display
	                         </button>
							<div class="dropdown-menu dropdown-menu-right">
								<div class="form-group">
			                    		<div class="option-header">Rows</div>
									<select class="custom-select form-control" name="size" onchange="document.getElementById('page').value=1;submit();">
										<c:forTokens items="10,25,50,100,200,500" delims="," var="current">
											<option value="${current}" <c:if test="${current == affiliateSearch.pageSize}">selected</c:if>>${current}</option>
										</c:forTokens>
									</select>
								</div>
								<div class="form-group">
			                    		<div class="option-header">Columns</div>
			                    		<div id="column-hider"></div>
								</div>
							</div>
	                    </div>
					</div>
				</div>
			</div>
			<div class="page-body">
				<div class="grid-stage">
					<c:if test="${model.count > 0}">
						<div class="page-stat">
							<fmt:message key="showing">
								<fmt:param value="${affiliateSearch.offset + 1}"/>
								<fmt:param value="${model.pageEnd}"/>
								<fmt:param value="${model.count}"/>
						  	</fmt:message>
						</div>
					</c:if>
		            <table id="grid"></table>
				    <div class="footer">
					    <c:if test="${model.count > 0}">
			                <div class="float-right">
								<c:if test="${affiliateSearch.page == 1}">
							 		<span class="previous"><i class="sli-arrow-left" style="font-size: 0.7rem"></i> Previous</span>
								</c:if>
								<c:if test="${affiliateSearch.page != 1}">
									<a href="<c:url value="affiliateList.jhtm"><c:param name="page" value="${affiliateSearch.page-1}"/></c:url>" >
										<span class="previous"><i class="sli-arrow-left" style="font-size: 0.7rem"></i> Previous</span>
									</a>
								</c:if>
								<span class="status"><c:out value="${affiliateSearch.page}" /> of <c:out value="${model.pageCount}"/></span>
								<c:if test="${affiliateSearch.page == model.pageCount}">
								 	<span class="next">Next <i class="sli-arrow-right" style="font-size: 0.7rem"></i></span>
								</c:if>
								<c:if test="${affiliateSearch.page != model.pageCount}">
									<a href="<c:url value="affiliateList.jhtm"><c:param name="page" value="${affiliateSearch.page+1}"/></c:url>">
										<span class="next">Next <i class="sli-arrow-right" style="font-size: 0.7rem"></i></span>
									</a>
								</c:if>
							</div>
						</c:if>
					</div>
				</div>
			</div>
		    <input type="hidden" id="sort" name="sort" value="${affiliateSearch.sort}" />
		    </form>
		</sec:authorize>
	</tiles:putAttribute>
	<tiles:putAttribute name="js">
		<!-- DGRID TABLE SCRIPT MUST BE AT BOTTOM OF THE PAGE -->
		<script src="${_contextpath}/admin-responsive-static/assets/plugins/dojo/dojo.js" data-dojo-config="async: true"></script>
		<script>
			function generateMainGrid(){
				//Table Data Sanitization
				var data = [];
				<c:set var="totalCommission" value="0" />
				<c:forEach items="${model.affiliates}" var="customer" varStatus="status">
					data.push({
						customerId: '${customer.id}',
						lastName: '${wj:escapeJS(customer.address.lastName)}',
						firstName: '${wj:escapeJS(customer.address.firstName)}',
						username: '${wj:escapeJS(customer.username)}',
						accountNumber: '${wj:escapeJS(customer.accountNumber)}',
						created: '<fmt:formatDate type="date" timeStyle="default" value="${customer.created}"/>',
						<c:if test="${customer.imported}">
							imported: true,
						</c:if>
						numOfLogins: '${customer.numOfLogins}',
						affiliateNumChild: '${customer.affiliateNumChild}',
						affiliateNumOrder: '${customer.affiliateNumOrder}',
						commissionTotal: '<fmt:formatNumber value="${customer.commissionTotal}" pattern="#,##0.00" />',
						trackcode: '${customer.trackcode}',
						customerToken: '${customer.token}',
					});
					<c:set var="totalCommission" value="${customer.commissionTotal + totalCommission}" />
				</c:forEach>
				require(["dgrid/List", "dgrid/OnDemandGrid", "dgrid/Selection", "dgrid/Keyboard", "dgrid/Editor", "dijit/form/CheckBox", 
						"dijit/form/TimeTextBox", "dijit/form/Select", "dojo/data/ObjectStore", "dijit/form/FilteringSelect",
						"dojo/_base/declare", "dojo/_base/lang", "dojo/store/Memory", "dojo/store/Observable", "dgrid/test/data/createSyncStore", 
						'dgrid/extensions/ColumnHider', 'dgrid/extensions/ColumnReorder', 'dgrid/extensions/ColumnResizer', 'summary/SummaryRow',
						'dijit/form/DropDownButton', 'dijit/DropDownMenu', 'dijit/MenuItem',
					], function(List, OnDemandGrid, Selection, Keyboard, Editor, CheckBox, TimeTextBox, Select, ObjectStore, FilteringSelect,
							declare, lang, LegacyMemory, LegacyObservable, createSyncStore, ColumnHider, ColumnReorder, ColumnResizer, SummaryRow, DropDownButton, DropDownMenu, MenuItem){
	
					//Column Definitions
					var columns = {
						actions:{
							label: 'Actions',
							renderCell: function(object, data, td, options){
								var div = document.createElement("div");
								var menu = new DropDownMenu({
									style: "display: none;"
								});
								
								var menuItem;
								
								menuItem = new MenuItem({
							        label: '<fmt:message key="edit" />',
							        onClick: function(){
							        		location.href = '../customers/customer.jhtm?id='+object.customerId;
							        	}
							    });
							    menu.addChild(menuItem);
							    
							    menuItem = new MenuItem({
									label: '<fmt:message key="loginAsCustomer" />',
							        onClick: function(){
							        		location.href = '../customers/loginAsCustomer.jhtm?cid='+object.customerToken;
							        	}
							    });
							    menu.addChild(menuItem);
							    
							    menuItem = new MenuItem({
									label: 'Commission',
							        onClick: function(){
							        		location.href = '../customers/loginAsCustomer.jhtm?cid='+object.customerToken;
							        	}
							    });
							    menu.addChild(menuItem);
							    
							    menu.startup();
 
							    var dropdown = new DropDownButton({
							        iconClass: "sli-settings",
							        dropDown: menu,
							    },'button');
							    
							    div.appendChild(dropdown.domNode);
			                    return div;
							},
							sortable: false
						},
						lastName: {
							label: '<fmt:message key="lastName" />',
							get: function(object){
								return object.lastName;
							}
						},
						firstName: {
							label: '<fmt:message key="firstName" />',
							get: function(object){
								return object.firstName;
							}
						},
						username: {
							label: '<fmt:message key="emailAddress" />',
							get: function(object){
								return object.username;
							}
						},
						accountNumber: {
							label: '<fmt:message key="accountNumber" />',
							get: function(object){
								return object.accountNumber;
							}
						},
						created: {
							label: '<fmt:message key="accountCreated" />',
							get: function(object){
								return object.created;
							}
						},
						imported: {
							label: '<fmt:message key="imported" />',
							renderCell: function(object){
								var div = document.createElement("div");
								if(object.imported){
									var i = document.createElement("i");
									i.className = 'sli-check';
									div.appendChild(i);
								}
								return div;
							}
						},
						numOfLogins: {
							label: '<fmt:message key="numOfLogins" />',
							get: function(object){
								return object.numOfLogins;
							}
						},
						affiliateNumChild: {
							label: '<fmt:message key="child" />',
							renderCell: function(object){
								var link = document.createElement('a');
								link.href = '../customers/customerList.jhtm?affId='+object.customerId;
								link.text = object.affiliateNumChild;
								return link;
							}
						},
						affiliateNumOrder: {
							label: '#<fmt:message key="order" />',
							renderCell: function(object){
								var link = document.createElement('a');
								link.href = './orders/ordersList.jhtm?affId='+object.customerId;
								link.text = object.affiliateNumOrder;
								return link;
							}
						},
						commissionTotal:{
							label: '<fmt:message key="commission" />',
							get: function(object){
								return object.commissionTotal;
							}
						},
						trackcode:{
							label: '<fmt:message key="trackcode" />',
							get: function(object){
								return object.trackcode;
							}
						},
					};
					
					var grid = new (declare([OnDemandGrid, Selection, Keyboard, Editor, ColumnHider, ColumnReorder, ColumnResizer, SummaryRow]))({
						collection: createSyncStore({ data: data }),
						columns: columns,
						selectionMode: "none",
					},'grid');
					
					var totals = {accountNumber: 'Total'};
					totals['commissionTotal'] = '<fmt:formatNumber value="${totalCommission}" pattern="#,##0.00" />';
					grid.set('summary', totals);
					
					grid.on('dgrid-refresh-complete', function(event) {
						$('#grid-hider-menu').appendTo('#column-hider');
					});
					
					grid.startup();		
				});
			}
			
			generateMainGrid();
		</script>
	</tiles:putAttribute>
</tiles:insertDefinition>