<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>

<c:if test="${model.jbdRepSales != null}">

<fieldset class="myaccount_fieldset">
<legend class="myaccount_legend">Rep Sales</legend>
<%--
<table width="100%" border="0" cellpadding="1" cellspacing="1"> 
  <tr>
    <td width="50%" align="right"><b>Sales Person #: </b></td>
    <td width="50%"><c:out value="${model.jbdRepSales['SalespersonNumber']}" /></td>
  </tr>
  <tr>
    <td align="right"><b>Sub Rep Name: </b></td>
    <td><c:out value="${model.jbdRepSales['SubRepName']}" /></td>
  </tr>
  <tr>
    <td align="right"><b>Rep Group Name: </b></td>
    <td><c:out value="${model.jbdRepSales['RepGroupName']}" /></td>
  </tr>
</table>
<br/>
 --%>
<table align="center" border="0" cellpadding="1" cellspacing="1"> 
  <tr>
    <td width="50px" align="center">&nbsp;</td>
    <td width="100px" align="center"><b>Sales</b></td>
    <td width="100px" align="center"><b>Commission</b></td>
  </tr>
  <tr>
    <td align="right"><b>PTD: </b></td>
    <td align="right"><fmt:message key="${siteConfig['CURRENCY'].value}" /><fmt:formatNumber value="${model.jbdRepSales['SalesPTD']}" pattern="#,##0.00" /></td>
    <td align="right"><fmt:message key="${siteConfig['CURRENCY'].value}" /><fmt:formatNumber value="${model.jbdRepSales['CommissionPTD']}" pattern="#,##0.00" /></td>
  </tr>
  <tr>
    <td align="right"><b>YTD: </b></td>
    <td align="right"><fmt:message key="${siteConfig['CURRENCY'].value}" /><fmt:formatNumber value="${model.jbdRepSales['SalesYTD']}" pattern="#,##0.00" /></td>
    <td align="right"><fmt:message key="${siteConfig['CURRENCY'].value}" /><fmt:formatNumber value="${model.jbdRepSales['CommissionYTD']}" pattern="#,##0.00" /></td>
  </tr>  
  <tr>
    <td align="right"><b>PYR: </b></td>
    <td align="right"><fmt:message key="${siteConfig['CURRENCY'].value}" /><fmt:formatNumber value="${model.jbdRepSales['SalesPYR']}" pattern="#,##0.00" /></td>
    <td align="right"><fmt:message key="${siteConfig['CURRENCY'].value}" /><fmt:formatNumber value="${model.jbdRepSales['CommissionPYR']}" pattern="#,##0.00" /></td>
  </tr>
</table>
</fieldset>

</c:if>