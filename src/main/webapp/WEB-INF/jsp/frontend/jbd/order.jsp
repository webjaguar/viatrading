<%@ page import="java.util.*" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
  <head>
    <title>Invoice</title>
    <link href="assets/invoice.css" rel="stylesheet" type="text/css"/>
  </head>  
<body class="invoice">
<div style="width:700px;margin:auto;">

<c:if test="${model.order == null}">
  <div class="message">Sales Order not found.</div>
</c:if>

<c:if test="${model.order != null}">
<c:out value="${model.invoiceLayout.headerHtml}" escapeXml="false"/>

<table border="0" width="100%" cellspacing="3" cellpadding="1">
<tr valign="top">
<td>
<b>Bill To:</b>
<table border="0" cellpadding="0" cellspacing="0">
<tr><td><c:out value="${sessionCustomer.address.firstName}"/> <c:out value="${sessionCustomer.address.lastName}"/></td></tr>
<c:if test="${sessionCustomer.address.company != ''}">
<tr><td><c:out value="${sessionCustomer.address.company}"/></td></tr>
</c:if>
<tr><td><c:out value="${sessionCustomer.address.addr1}"/></td></tr>
<c:if test="${sessionCustomer.address.addr2 != ''}">
<tr><td><c:out value="${sessionCustomer.address.addr2}"/></td></tr>
</c:if>
<tr><td><c:out value="${sessionCustomer.address.city}"/>, <c:out value="${sessionCustomer.address.stateProvince}"/> <c:out value="${sessionCustomer.address.zip}"/></td></tr>
<tr><td><c:out value="${model.countries[sessionCustomer.address.country]}"/></td></tr>
<c:if test="${sessionCustomer.address.phone != ''}">
<tr><td>Tel: <c:out value="${sessionCustomer.address.phone}"/></td></tr>
</c:if>
<c:if test="${sessionCustomer.address.cellPhone != ''}">
<tr><td>Cel: <c:out value="${sessionCustomer.address.cellPhone}"/></td></tr>
</c:if>
<c:if test="${sessionCustomer.address.fax != ''}">
<tr><td>Fax: <c:out value="${sessionCustomer.address.fax}"/></td></tr>
</c:if>
</table>
</td>
<td>&nbsp;</td>
<td>
<b>Ship To:</b>
<table border="0" cellpadding="0" cellspacing="0">
<tr><td><c:out value="${model.shipping.firstName}"/> <c:out value="${model.shipping.lastName}"/></td></tr>
<c:if test="${model.shipping.company != ''}">
<tr><td><c:out value="${model.shipping.company}"/></td></tr>
</c:if>
<tr><td><c:out value="${model.shipping.addr1}"/></td></tr>
<c:if test="${model.shipping.addr2 != ''}">
<tr><td><c:out value="${model.shipping.addr2}"/></td></tr>
</c:if>
<tr><td><c:out value="${model.shipping.city}"/>, <c:out value="${model.shipping.stateProvince}"/> <c:out value="${model.shipping.zip}"/></td></tr>
<tr><td><c:out value="${model.countries[model.shipping.country]}"/></td></tr>
<c:if test="${model.shipping.phone != ''}">
<tr><td>Tel: <c:out value="${model.shipping.phone}"/></td></tr>
</c:if>
<c:if test="${model.shipping.cellPhone != ''}">
<tr><td>Cel: <c:out value="${model.shipping.cellPhone}"/></td></tr>
</c:if>
<c:if test="${model.shipping.fax != ''}">
<tr><td>Fax: <c:out value="${model.shipping.fax}"/></td></tr>
</c:if>
</table>
</td>
<td>&nbsp;</td>
<td align="right">
<table>
  <tr>
    <td>Sales Order #</td>
    <td>:&nbsp;&nbsp;</td>
    <td><b><c:out value="${model.order['SalesOrderNumber']}"/></b></td>
  </tr>
  <tr>
    <td>Order Date</td>
    <td>:&nbsp;&nbsp;</td>
    <td><fmt:formatDate type="date" value="${model.order['OrderDate']}" pattern="MM/dd/yy"/></td>
  </tr>
  <tr>
    <td>Status</td>
    <td>:&nbsp;&nbsp;</td>
    <c:set var="colspan" value="2"/>
    <c:set var="backOrdered" value="false"/>
    <c:choose>
	<c:when test="${model.order['OrderStatus'] == 'A' and model.order['QuantityBackOrdered'] > 0}">
		<c:set var="backOrdered" value="true"/>
    	<c:set var="colspan" value="3"/>
        <td>Back Ordered</td>
	</c:when>
	<c:when test="${model.order['OrderStatus'] == 'A'}">
        <td>Active</td>
	</c:when>
	<c:when test="${model.order['OrderStatus'] == 'C'}">
        <td>Completed</td>
	</c:when>
	<c:when test="${model.order['OrderStatus'] == 'X'}">
        <td>Cancelled</td>
	</c:when>
    <c:otherwise>
        <td><c:out value="${model.order['OrderStatus']}"/></td>
    </c:otherwise>
    </c:choose>
  </tr>
</table>
</tr>
</table>
<br/>
<table border="0" cellpadding="2" cellspacing="1" width="100%" class="invoice">
  <tr>
    <th width="5%" rowspan="2" class="invoice">Line#</th>
    <th rowspan="2" class="invoice">Item #</th>
    <th rowspan="2" class="invoice">DESCRIPTION</th>
    <th class="invoice" colspan="${colspan}">QUANTITY</th>
    <th rowspan="2" class="invoice">PRICE</th>
  </tr>
  <tr>
    <th width="10%" class="invoice">Ordered</th>
    <th width="10%" class="invoice">Shipped</th>
    <c:if test="${backOrdered}">
    <th width="10%" class="invoice">Back Ordered</th>
    </c:if>
  </tr>
<c:forEach var="lineItem" items="${model.lineItems}" varStatus="status">
<tr valign="top">
  <td class="invoice" align="center"><c:out value="${status.count}"/></td>
  <td class="invoice"><c:out value="${lineItem['ItemNumber']}"/></td> 
  <td class="invoice"><c:out value="${lineItem['ItemDescription']}"/></td>
  <td class="invoice" align="center"><c:out value="${lineItem['OriginalOrderQuantity']}"/></td>
  <td class="invoice" align="center"><c:out value="${lineItem['QuantityShipped']}"/></td>
  <c:if test="${backOrdered}">
  <td class="invoice" align="center"><c:out value="${lineItem['QuantityBackOrdered']}"/></td>
  </c:if>
  <td class="invoice" align="right"><fmt:message key="${siteConfig['CURRENCY'].value}" /><fmt:formatNumber value="${lineItem['LastUnitPrice']}" pattern="#,##0.00"/></td>
</tr>
</c:forEach>
  <tr bgcolor="#BBBBBB">
	<td colspan="${4+colspan}">&nbsp;</td>
  </tr>
</table>
<c:if test="${not empty model.tracking}">
<br/>
<table align="center" border="0" cellpadding="2" cellspacing="1" width="75%" class="invoice">
  <tr>
    <th class="invoice"><fmt:message key="trackNum"/></th>
    <th class="invoice">Package #</th>
    <th class="invoice">Weight</th>
    <th class="invoice">Invoice #</th>
  </tr>
  <c:forEach var="tracking" items="${model.tracking}">
  <tr>
    <td class="invoice" align="center"><c:out value="${tracking['TrackingNo']}" /></td>
    <td class="invoice" align="center"><c:out value="${tracking['PackageNumber']}" /></td>
    <td class="invoice" align="center"><c:out value="${tracking['Weight']}" /></td>
    <c:if test="${map['isValidInvoiceNumber']}">
    <td class="invoice" align="center"><a class="nameLink" href="jbd_invoice.jhtm?id=${map['InvoiceNumber']}"><c:out value="${map['InvoiceNumber']}"/></a></td>
    </c:if>
    <c:if test="${not map['isValidInvoiceNumber']}">
    <td class="invoice" align="center"><c:out value="${tracking['InvoiceNumber']}" /></td>
    </c:if>
  </tr>
  </c:forEach>
</table>
<div align="center"><a href="http://fedex.com/Tracking?action=track&language=english&cntry_code=us&mps=y&ascend_header=1&tracknumbers=<c:forEach var="tracking" items="${model.tracking}"><c:out value="${tracking['TrackingNo']}" />,</c:forEach>"><img src="assets/Image/Layout/FedEx_MultipleServices_Normal_2Color_Positive_20.gif" /></a></div>
</c:if>

<c:out value="${model.invoiceLayout.footerHtml}" escapeXml="false"/>
</c:if>

</div>

</body>
</html>