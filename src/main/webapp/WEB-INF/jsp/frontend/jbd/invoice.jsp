<%@ page import="java.util.*" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
  <head>
    <title>Invoice</title>
    <link href="assets/invoice.css" rel="stylesheet" type="text/css"/>
  </head>  
<body class="invoice">
<div style="width:700px;margin:auto;">

<c:if test="${model.invoice == null}">
  <div class="message">Invoice not found.</div>
</c:if>

<c:if test="${model.invoice != null}">
<c:out value="${model.invoiceLayout.headerHtml}" escapeXml="false"/>

<table border="0" width="100%" cellspacing="3" cellpadding="1">
<tr valign="top">
<td>
<b>Bill To:</b>
<table border="0" cellpadding="0" cellspacing="0">
<tr><td><c:out value="${sessionCustomer.address.firstName}"/> <c:out value="${sessionCustomer.address.lastName}"/></td></tr>
<c:if test="${sessionCustomer.address.company != ''}">
<tr><td><c:out value="${sessionCustomer.address.company}"/></td></tr>
</c:if>
<tr><td><c:out value="${sessionCustomer.address.addr1}"/></td></tr>
<c:if test="${sessionCustomer.address.addr2 != ''}">
<tr><td><c:out value="${sessionCustomer.address.addr2}"/></td></tr>
</c:if>
<tr><td><c:out value="${sessionCustomer.address.city}"/>, <c:out value="${sessionCustomer.address.stateProvince}"/> <c:out value="${sessionCustomer.address.zip}"/></td></tr>
<tr><td><c:out value="${model.countries[sessionCustomer.address.country]}"/></td></tr>
<c:if test="${sessionCustomer.address.phone != ''}">
<tr><td><fmt:message key="f_phone" />: <c:out value="${sessionCustomer.address.phone}"/></td></tr>
</c:if>
<c:if test="${sessionCustomer.address.cellPhone != ''}">
<tr><td><fmt:message key="f_mobilePhone" />: <c:out value="${sessionCustomer.address.cellPhone}"/></td></tr>
</c:if>
<c:if test="${sessionCustomer.address.fax != ''}">
<tr><td><fmt:message key="f_fax" />: <c:out value="${sessionCustomer.address.fax}"/></td></tr>
</c:if>
</table>
</td>
<td>&nbsp;</td>
<td>
<b>Ship To:</b>
<table border="0" cellpadding="0" cellspacing="0">
<tr><td><c:out value="${model.shipping.firstName}"/> <c:out value="${model.shipping.lastName}"/></td></tr>
<c:if test="${model.shipping.company != ''}">
<tr><td><c:out value="${model.shipping.company}"/></td></tr>
</c:if>
<tr><td><c:out value="${model.shipping.addr1}"/></td></tr>
<c:if test="${model.shipping.addr2 != ''}">
<tr><td><c:out value="${model.shipping.addr2}"/></td></tr>
</c:if>
<tr><td><c:out value="${model.shipping.city}"/>, <c:out value="${model.shipping.stateProvince}"/> <c:out value="${model.shipping.zip}"/></td></tr>
<tr><td><c:out value="${model.countries[model.shipping.country]}"/></td></tr>
<c:if test="${model.shipping.phone != ''}">
<tr><td><fmt:message key="f_phone" />: <c:out value="${model.shipping.phone}"/></td></tr>
</c:if>
<c:if test="${model.shipping.cellPhone != ''}">
<tr><td><fmt:message key="f_mobilePhone" />: <c:out value="${model.shipping.cellPhone}"/></td></tr>
</c:if>
<c:if test="${model.shipping.fax != ''}">
<tr><td><fmt:message key="f_fax" />: <c:out value="${model.shipping.fax}"/></td></tr>
</c:if>
</table>
</td>
<td>&nbsp;</td>
<td align="right">
<table>
  <tr>
    <td>Invoice #</td>
    <td>:&nbsp;&nbsp;</td>
    <td><b><c:out value="${model.invoice['InvoiceNumber']}"/></b></td>
  </tr>
  <tr>
    <td>Invoice Date</td>
    <td>:&nbsp;&nbsp;</td>
    <td><fmt:formatDate type="date" value="${model.invoice['InvoiceDate']}" pattern="MM/dd/yy"/></td>
  </tr>
  <tr>
    <td>Type</td>
    <td>:&nbsp;&nbsp;</td>
    <c:choose>
	<c:when test="${model.invoice['InvoiceType'] == 'I'}">
        <td>Invoice</td>
	</c:when>
	<c:when test="${model.invoice['InvoiceType'] == 'C'}">
        <td>Credit Memo</td>
	</c:when>
    <c:otherwise>
        <td><c:out value="${model.invoice['InvoiceType']}"/></td>
    </c:otherwise>
    </c:choose>
  </tr>
  <c:if test="${model.invoice['InvoiceType'] == 'I'}">
  <tr>
    <td>Sales Order #</td>
    <td>:&nbsp;&nbsp;</td>
    <td><c:out value="${model.invoice['SalesOrderNumber']}"/></td>
  </tr>
  </c:if>
</table>
</tr>
</table>
<br/>
<table border="0" cellpadding="2" cellspacing="1" width="100%" class="invoice">
  <tr> 
    <th width="5%" class="invoice">Line#</th>
    <th class="invoice">Item #</th>
    <th class="invoice">DESCRIPTION</th>
    <th width="10%" class="invoice" >Quantity Shipped</th>
    <th class="invoice">UNIT PRICE</th>
    <th class="invoice">EXTENDED</th>
  </tr>
<c:forEach var="lineItem" items="${model.lineItems}" varStatus="status">
<tr valign="top">
  <td class="invoice" align="center"><c:out value="${status.count}"/></td>
  <td class="invoice"><c:out value="${lineItem['ItemNumber']}"/></td> 
  <td class="invoice"><c:out value="${lineItem['ItemDescription']}"/></td>
  <td class="invoice" align="center"><c:out value="${lineItem['QuantityShipped']}"/></td>
  <td class="invoice" align="right"><fmt:message key="${siteConfig['CURRENCY'].value}" /><fmt:formatNumber value="${lineItem['UnitPrice']}" pattern="#,##0.00"/></td>
  <td class="invoice" align="right"><fmt:message key="${siteConfig['CURRENCY'].value}" /><fmt:formatNumber value="${lineItem['Extension']}" pattern="#,##0.00"/></td>
</tr>
</c:forEach>
  <tr bgcolor="#BBBBBB">
	<td colspan="${6}">&nbsp;</td>
  </tr>
</table>

<c:out value="${model.invoiceLayout.footerHtml}" escapeXml="false"/>
</c:if>

</div>

</body>
</html>