<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>

<tiles:insertDefinition name="${_template}" flush="true">
  <tiles:putAttribute name="content" type="string">

<c:out value="${model.jbdLayout.headerHtml}" escapeXml="false"/>

<table border="0" cellpadding="3" cellspacing="1" width="100%" class="listings"> 
  <tr class="listingsHdr2">
    <td align="center">Invoice #</td>
    <td align="center">Invoice Date</td>
    <td align="center">Invoice Type</td>
    <td align="center">Sales Order #</td>
    <td align="center">Total</td>
  </tr>
<c:forEach items="${model.list}" var="map" varStatus="status">
  <tr class="row${status.index % 2}">
    <td align="center"><a class="nameLink" href="jbd_invoice.jhtm?id=${map['InvoiceNumber']}"><c:out value="${map['InvoiceNumber']}"/></a></td>
    <td align="center"><fmt:formatDate type="date" value="${map['InvoiceDate']}" pattern="MM/dd/yy"/></td>
    <c:choose>
	<c:when test="${map['InvoiceType'] == 'I'}">
        <td align="center">Invoice</td>
	</c:when>
	<c:when test="${map['InvoiceType'] == 'C'}">
        <td align="center">Credit Memo</td>
	</c:when>
    <c:otherwise>
        <td align="center"><c:out value="${map['InvoiceType']}"/></td>
    </c:otherwise>
    </c:choose>
    <c:if test="${map['isValidSalesOrderNumber']}">
    <td align="center"><a class="nameLink" href="jbd_order.jhtm?id=${map['SalesOrderNumber']}"><c:out value="${map['SalesOrderNumber']}"/></a></td>
    </c:if>
    <c:if test="${not map['isValidSalesOrderNumber']}">
    <td align="center"><c:out value="${map['SalesOrderNumber']}"/></td>
    </c:if>
    <td align="right"><fmt:message key="${siteConfig['CURRENCY'].value}" /><fmt:formatNumber value="${map['TaxableAmount'] + map['NonTaxableAmount']}" pattern="#,##0.00" /></td>
  </tr>
</c:forEach>
<c:if test="${empty model.list}">
  <tr class="emptyList">
    <td colspan="5">&nbsp;</td>
  </tr>
</c:if>
</table>

<c:out value="${model.jbdLayout.footerHtml}" escapeXml="false"/>
    
  </tiles:putAttribute>
</tiles:insertDefinition>
