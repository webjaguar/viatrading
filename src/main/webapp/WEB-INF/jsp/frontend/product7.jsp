<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="/WEB-INF/tlds/spring.tld" prefix="spring" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>

<tiles:insertDefinition name="${_template}" flush="true">
  <tiles:putAttribute name="leftbar" value="/WEB-INF/jsp/frontend/layout/template${_leftBar}/leftbar.jsp" />
  <tiles:putAttribute name="content" type="string">

<c:if test="${model.mootools and param.hideMootools == null}">
<script src="${_contextpath}/javascript/mootools-1.2.5.1-more.js" type="text/javascript"></script>
</c:if>

<c:if test="${model.message != null}">
  <div class="message"><fmt:message key="${model.message}" /></div>
</c:if>

<c:if test="${model.product != null}">

<c:if test="${fn:trim(model.productLayout.headerHtml) != ''}">
  <c:set value="${model.productLayout.headerHtml}" var="headerHtml"/>
  <%@ include file="/WEB-INF/jsp/frontend/common/headerDynamicElement.jsp" %>
  <div><c:out value="${headerHtml}" escapeXml="false"/></div>
</c:if>

<c:set value="${model.product}" var="product"/>
<script language="JavaScript" type="text/JavaScript">
<!--
<c:if test="${siteConfig['DETAILS_IMAGE_WIDTH'].value != '' }" >
function zoomIn() {
	window.open('${_contextpath}/productImage.jhtm?id=${product.id}&imgUrl=' + document.getElementById("_image").src,'name','width=800,height=600,resizable=yes,scrollbars=1');
}
</c:if>
function toggleRecommended(id) {
	if (document.getElementById("checkbox_" + id).checked) {
		document.getElementById("rec_quantity_" + id).value = "1";
	} else {
		document.getElementById("rec_quantity_" + id).value = "";
	}		
}
//-->
</script>

<c:set var="details_images_style" value="" />
<c:set var="details_desc_style" value="" />

<%-- bread crumbs --%>
<c:import url="/WEB-INF/jsp/frontend/layout/${_template}/breadcrumbs.jsp" />

<%-- <div class="productLayout7"><c:out value="${product.shortDesc}" escapeXml="false" /></div> --%>

<table border="0" class="details7" cellspacing="0" cellpadding="0">
<tr valign="top">
 <td>
   <table border="0" cellspacing="0" cellpadding="0">
   <tr>
     <td>
     <c:choose>
       <c:when test="${model.flashSource != null and !empty model.flashSource}">
<script language="JavaScript" type="text/javascript">
	AC_FL_RunContent(
		'codebase', 'http://download.macromedia.com/pub/shockwave/cabs/flash/swflash.cab#version=10,0,0,0',
		'width', '300',
		'height', '330',
		'src', '${model.flashSource}',
		'quality', 'high',
		'pluginspage', 'http://www.adobe.com/go/getflashplayer',
		'align', 'middle',
		'play', 'true',
		'loop', 'true',
		'scale', 'showall',
		'wmode', 'transparent',
		'devicefont', 'false',
		'id', '${model.flashSource}',
		'bgcolor', '#ffffff',
		'name', '${model.flashSource}',
		'menu', 'true',
		'allowFullScreen', 'false',
		'allowScriptAccess','sameDomain',
		'movie', '${model.flashSource}',
		'salign', ''
		); //end AC code
</script>
<noscript>
	<object classid="clsid:d27cdb6e-ae6d-11cf-96b8-444553540000" codebase="http://download.macromedia.com/pub/shockwave/cabs/flash/swflash.cab#version=10,0,0,0" width="300" height="330" id="C2N360_300" align="middle">
	<param name="allowScriptAccess" value="sameDomain" />
	<param name="allowFullScreen" value="false" />
	<param name="movie" value="${_contextpath}/${model.flashSource}.swf" /><param name="quality" value="high" /><param name="wmode" value="transparent" /><param name="bgcolor" value="#ffffff" />	<embed src="${_contextpath}/${model.flashSource}.swf" quality="high" wmode="transparent" bgcolor="#ffffff" width="300" height="330" name="${model.flashSource}" align="middle" allowScriptAccess="sameDomain" allowFullScreen="false" type="application/x-shockwave-flash" pluginspage="http://www.adobe.com/go/getflashplayer" />
	</object>
</noscript>
       </c:when>
       <c:otherwise>
         <c:set var="productImage">
		<c:choose>
 		<c:when test="${fn:contains(siteConfig['PRODUCT_IMAGE_LAYOUTS'].value,'s2') and 
 				(product.imageLayout == 's2' or (empty product.imageLayout and siteConfig['PRODUCT_IMAGE_LAYOUT'].value == 's2'))}">
    		<link rel="stylesheet" href="${_contextpath}/assets/SmoothGallery/slideshow.css" type="text/css" media="screen" />
			<script type="text/javascript" src="${_contextpath}/javascript/slideshow.js"></script>
			<script type="text/javascript">		
			//<![CDATA[
	  		window.addEvent('domready', function(){
	    		var data = [<c:forEach items="${product.images}" var="image" varStatus="status">'<c:if test="${!image.absolute}">assets/Image/Product/detailsbig/</c:if><c:out value="${image.imageUrl}"/>'<c:if test="${! status.last}" >,</c:if></c:forEach> ];
	    		var myShow = new Slideshow('show', data, { controller: true, height: 300, hu: '', thumbnails: true, width: 400 });
				});
			//]]>
			</script>
	
			<div id="show" class="slideshow"> </div>
    		<div id="slideshow_margin"/>
 		</c:when>
 		<c:when test="${fn:contains(siteConfig['PRODUCT_IMAGE_LAYOUTS'].value,'mb') and 
 				(param.multibox == null or param.multibox != 'off') and
 				(product.imageLayout == 'mb' or (empty product.imageLayout and siteConfig['PRODUCT_IMAGE_LAYOUT'].value == 'mb'))}">
 		<link rel="stylesheet" href="${_contextpath}/assets/SmoothGallery/multibox.css" type="text/css" media="screen" />
 		<c:set value="${true}" var="multibox"/>
 		<script src="${_contextpath}/javascript/overlay.js" type="text/javascript" ></script>
 		<script src="${_contextpath}/javascript/multibox.js" type="text/javascript" ></script>
 		<script type="text/javascript">
			window.addEvent('domready', function(){
				var box = new multiBox('mbxwz', {overlay: new overlay()});
			});
 		</script>
   			<div class="details_image_box" id="details_image_boxId" style="${details_images_style}
	 		<c:if test="${siteConfig['DETAILS_IMAGE_WIDTH'].value != '' }">width: <c:out value="${siteConfig['DETAILS_IMAGE_WIDTH'].value}"/>px;</c:if>">
	 
			<c:forEach items="${model.product.images}" var="image" varStatus="status">
	  		<c:if test="${status.first}">
	    		<a href="<c:if test="${!image.absolute}">${_contextpath}/assets/Image/Product/detailsbig/</c:if><c:out value="${image.imageUrl}"/>" id="mb0" class="mbxwz" title=""><img src="<c:if test="${!image.absolute}">${_contextpath}/assets/Image/Product/detailsbig/</c:if><c:out value="${image.imageUrl}"/>" border="0" style="<c:if test="${siteConfig['DETAILS_IMAGE_WIDTH'].value != '' }">width: <c:out value="${siteConfig['DETAILS_IMAGE_WIDTH'].value}"/>px</c:if>" alt="<c:out value="${product.name}"/><c:if test="${product.shortDesc != ''}"> - <c:out value="${product.shortDesc}"/></c:if>"/></a>
	  		</c:if>
			</c:forEach>
    		<div style="clear: both;" >
    		<c:forEach items="${model.product.images}" var="image" varStatus="status">
	  		<c:if test="${status.count > 1}">
	      		<a href="<c:if test="${!image.absolute}">${_contextpath}/assets/Image/Product/detailsbig/</c:if><c:out value="${image.imageUrl}"/>" id="mb${status.count - 1}" class="mbxwz" title=""><img src="<c:if test="${!image.absolute}">${_contextpath}/assets/Image/Product/detailsbig/</c:if><c:out value="${image.imageUrl}"/>" border="0" class="details_thumbnail" alt="<c:out value="${product.name}"/><c:if test="${product.shortDesc != ''}"> - <c:out value="${product.shortDesc}"/></c:if>"/></a>
	  		</c:if>
			</c:forEach>
			</div>
			</div>        
 		</c:when>
 		<c:when test="${fn:contains(siteConfig['PRODUCT_IMAGE_LAYOUTS'].value,'qb') and 
 				(product.imageLayout == 'qb' or (empty product.imageLayout and siteConfig['PRODUCT_IMAGE_LAYOUT'].value == 'qb'))}">
 		<link rel="stylesheet" href="${_contextpath}/assets/SmoothGallery/QuickBox/quickbox.css" type="text/css" media="screen" />
 		<c:set value="${true}" var="quickbox"/>
 		<script src="${_contextpath}/javascript/QuickBox.js" type="text/javascript" ></script>
 		<script type="text/javascript">
			window.addEvent('domready', function(){
				new QuickBox();
			});
 		</script>
 			<div class="details_image_box" id="details_image_boxId" style="${details_images_style}
	 		<c:if test="${siteConfig['DETAILS_IMAGE_WIDTH'].value != '' }">width: <c:out value="${siteConfig['DETAILS_IMAGE_WIDTH'].value}"/>px;</c:if>">
	 		
			<c:forEach items="${model.product.images}" var="image" varStatus="status">
	  		<c:if test="${status.first}">
	    		<a href="<c:if test="${!image.absolute}">${_contextpath}/assets/Image/Product/detailsbig/</c:if><c:out value="${image.imageUrl}"/>" id="" class="" title="" rel="quickbox"><img src="<c:if test="${!image.absolute}">${_contextpath}/assets/Image/Product/detailsbig/</c:if><c:out value="${image.imageUrl}"/>" border="0" style="<c:if test="${siteConfig['DETAILS_IMAGE_WIDTH'].value != '' }">width: <c:out value="${siteConfig['DETAILS_IMAGE_WIDTH'].value}"/>px</c:if>" alt="<c:out value="${product.name}"/><c:if test="${product.shortDesc != ''}"> - <c:out value="${product.shortDesc}"/></c:if>"/></a>
	  		</c:if>
			</c:forEach>
    		<div style="clear: both;" >
    		<c:forEach items="${model.product.images}" var="image" varStatus="status">
	  		<c:if test="${status.count > 1}">
	      		<a href="<c:if test="${!image.absolute}">assets/Image/Product/detailsbig/</c:if><c:out value="${image.imageUrl}"/>" id="qb${status.count - 1}" class="" title="" rel="quickbox"><img src="<c:if test="${!image.absolute}">${_contextpath}/assets/Image/Product/detailsbig/</c:if><c:out value="${image.imageUrl}"/>" border="0" class="details_thumbnail" alt="<c:out value="${product.name}"/><c:if test="${product.shortDesc != ''}"> - <c:out value="${product.shortDesc}"/></c:if>"/></a>
	  		</c:if>
			</c:forEach>
			</div>
			</div>        
 		</c:when>
 		<c:otherwise>
    		<div class="details_image_box" id="details_image_boxId" style="${details_images_style}
	 		<c:if test="${siteConfig['DETAILS_IMAGE_WIDTH'].value != '' }">width: <c:out value="${siteConfig['DETAILS_IMAGE_WIDTH'].value}"/>px;</c:if>">
	 		
			<c:forEach items="${model.product.images}" var="image" varStatus="status">
			<c:if test="${status.first}">
		 		<img src="<c:if test="${!image.absolute}">${_contextpath}/assets/Image/Product/detailsbig/</c:if><c:out value="${image.imageUrl}"/>" border="0" id="_image" name="_image" class="details_image"
		 			style="<c:if test="${siteConfig['DETAILS_IMAGE_WIDTH'].value != '' }">width: <c:out value="${siteConfig['DETAILS_IMAGE_WIDTH'].value}"/>px</c:if>" alt="<c:out value="${product.name}"/><c:if test="${product.shortDesc != ''}"> - <c:out value="${product.shortDesc}"/></c:if>">
			<c:if test="${siteConfig['DETAILS_IMAGE_WIDTH'].value != '' }" >
			<div align="right">
			<a onclick="zoomIn()" title="Zoom in"><img src="${_contextpath}/assets/Image/Layout/zoom-icon.gif" alt="Zoom In" width="16" height="16" /></a>
			</div>
			</c:if>
			</c:if>
			
			<c:if test="${status.last and (status.count != 1)}">
			<br>
			<c:forEach items="${model.product.images}" var="thumb">
			<a href="#" class="details_thumbnail_anchor" onClick="_image.src='<c:if test="${!thumb.absolute}">${_contextpath}/assets/Image/Product/detailsbig/</c:if><c:out value="${thumb.imageUrl}"/>';return false;">
		 		<img src="<c:if test="${!thumb.absolute}">${_contextpath}/assets/Image/Product/detailsbig/</c:if><c:out value="${thumb.imageUrl}"/>" class="details_thumbnail" alt="<c:out value="${product.name}"/><c:if test="${product.shortDesc != ''}"> - <c:out value="${product.shortDesc}"/></c:if>">
			</a>
			</c:forEach>
			</c:if>
			</c:forEach>
			</div>
			<c:if test="${fn:contains(siteConfig['PRODUCT_IMAGE_LAYOUTS'].value,'ss') and 
					(product.imageLayout == 'ss'  or (empty product.imageLayout and siteConfig['PRODUCT_IMAGE_LAYOUT'].value == 'ss'))}">
			<object classid="clsid:D27CDB6E-AE6D-11cf-96B8-444553540000" codebase="http://download.macromedia.com/pub/shockwave/cabs/flash/swflash.cab#version=6,0,29,0" width="750" height="475">
	  		<param name="movie" value="${_contextpath}/assets/Flash/slideshow.swf?file=thumbnailer1.jhtm?id=${product.id}">
	  		<param name="quality" value="high">
	  		<embed src="${_contextpath}/assets/Flash/slideshow.swf?file=thumbnailer1.jhtm?id=${product.id}" quality="high" pluginspage="http://www.macromedia.com/go/getflashplayer" type="application/x-shockwave-flash" width="750" height="475"></embed>
			</object>
			</c:if>
 		</c:otherwise>
		</c:choose>
		</c:set>
		
		<c:out value="${productImage}" escapeXml="false" />
       </c:otherwise>
     </c:choose>
     </td>
     <td valign="top">
       <div class="details_product_name"><h1><c:out value="${product.name}" escapeXml="false" /></h1></div>
 	   <div class="details_long_desc"><c:out value="${product.longDesc}" escapeXml="false" /></div> 
 	   <div class="priceSection">
 	   	 <c:if test="${ (gSiteConfig['gSHOPPING_CART'] and (!empty product.price or product.priceByCustomer)) and (!product.loginRequire or userSession != null)}">
         <table border="0" class="prices" cellspacing="0" cellpadding="0">
	       <c:if test="${product.msrp != null}">
		     <tr>
		       <td class="msrp">
			     <span>Regular Price </span><fmt:message key="${siteConfig['CURRENCY'].value}" /><fmt:formatNumber value="${product.msrp}" pattern="#,##0.00" />
			   </td>
			 </tr>	
		   	</c:if>
		    <tr>
		     <td class="price">
			   <span>Your Price </span><fmt:message key="${siteConfig['CURRENCY'].value}" /><fmt:formatNumber value="${product.price1}" pattern="#,##0.00" />
			 </td>
			</tr>
			<tr>
			 <td>
			    <form action="${_contextpath}/addToCart.jhtm" name="this_form" method="post">
			    <input type="hidden" name="product.id" value="${product.id}">
         		<input type="hidden" name="quantity_${product.id}" id="quantity_${product.id}" value="1">
			    <c:forEach items="${model.recommendedList}" var="rec_product">
			    <input type="hidden" name="product.id" value="${rec_product.id}">
         		<input type="hidden" name="quantity_${rec_product.id}" id="rec_quantity_${rec_product.id}" value="">
			    </c:forEach>         		
       	 		<input type="image" border="0" name="_addtocart" class="_addtocart" src="${_contextpath}/assets/Image/Layout/button_addtocart${_lang}.gif" />
       	 		</form>
			 </td>
			</tr>
			<c:if test="${gSiteConfig['gMYLIST'] && product.addToList}">
			<tr id="mylist">
			 <td>
			   <form action="${_contextpath}/addToList.jhtm">
			     <input type="hidden" name="product.id" value="${product.id}">
			     <div id="addToList">
			      <input type="image" border="0" src="${_contextpath}/assets/Image/Layout/button_addtolist${_lang}.gif">
			     </div>
			   </form>
			 </td>
			</tr>
			</c:if>
			<c:if test="${gSiteConfig['gCOMPARISON'] && product.compare}">
			<tr id="compair">
			  <td>
			   <form action="${_contextpath}/comparison.jhtm" method="get">
			   <%-- 
			   <input type="hidden" name="forwardAction" value="<c:out value='${model.url}'/>">
			   --%>
			   <input type="hidden" name="product.id" value="${product.id}">
			   <input type="image" border="0" src="${_contextpath}/assets/Image/Layout/button_compare.gif">
			   </form>
			  </td>
			</tr>
			</c:if>
			<c:if test="${gSiteConfig['gPRODUCT_REVIEW'] and product.enableRate}">
			<script type="text/javascript">function openTab(){$('tab6').fireEvent('click');}</script>
			<tr>
			  <td>
				  <table class="quickProductReview" border="0">
					 <tr>
					  <td><div class="ratingTitle"><fmt:message key="rating" />:</div></td>
					  <td><div class="reviewStar"><jsp:include page="/WEB-INF/jsp/frontend/common/productRateImage.jsp"><jsp:param name="productRateAverage" value="${product.rate.average}" /></jsp:include></div></td>
					  <td>
					  	<div class="readReviewLink">
					  	  <c:choose>
					        <c:when test="${product.rate == null}">
					          <c:if test="${siteConfig['PRODUCT_REVIEW_TYPE'].value != 'anonymous'}">
					          (<a href="${_contextpath}/addProductReview.jhtm?sku=${model.product.sku}&cid=${model.cid}" ><fmt:message key="firstReview" /></a>)
					          </c:if>
					          <c:if test="${siteConfig['PRODUCT_REVIEW_TYPE'].value == 'anonymous'}">
					          (<a href="${_contextpath}/productReviewAnon.jhtm?sku=${model.product.sku}&cid=${model.cid}" ><fmt:message key="firstReview" /></a>)
					          </c:if>
					        </c:when>
					        <c:otherwise>
					          (<c:out value="${product.rate.numReview}" /> <fmt:message key="reviews" />, <a href="${model.url}#tabs" onclick="openTab();"><fmt:message key="f_readAll" /></a>)
					        </c:otherwise>
					      </c:choose>  
					  	</div>
					  </td>
					 </tr>
					</table>
			  </td>
			</tr>
			</c:if>
		 </table>
         </c:if>
       </div> 
     </td>
   </tr>
   <tr>
     <td colspan="2">
     <c:if test="${gSiteConfig['gTAB'] > 0 and !empty model.tabList}" >
		<script src="${_contextpath}/javascript/SimpleTabs1.2.js" type="text/javascript"></script>
		<script type="text/javascript">
			window.addEvent('domready', function(){
				var tabs1 = new SimpleTabs($('tab-block-1'), { selector: 'h4'});
			});
		</script>
		<!-- tabs -->
		<a name="tabs"></a>
		<div id="tab-block-1">
		<c:forEach items="${model.tabList}" var="tab" varStatus="status">
		<h4 id="${tab.tabNumber}" title="<c:out value="${tab.tabName}" escapeXml="false"/>"><c:out value="${tab.tabName}" escapeXml="false" /></h4>
			<div>
			<!-- start tab -->
			    <c:out value="${tab.tabContent}" escapeXml="false"/>
			<!-- end tab -->
			</div>
		</c:forEach>
		</div>
	 </c:if>
     </td>
   </tr>
   </table>
 </td>
 <td>

	  <div class="reviewList">
	  <!--  product review start -->
	  <c:if test="${gSiteConfig['gPRODUCT_REVIEW'] and siteConfig['PRODUCT_RATE'].value != 'true' and model.product.enableRate and model.productReviewListLast3 != null}">
		<form method="post">
		<table id="reviews" width="100%" align="center" cellspacing="0" cellpadding="0" border="0">
		  <tr>
			<td colspan="2">
				<table border="0" cellpadding="1" cellspacing="0" width="100%">
				  <tr><td colspan="2" class="productReview">
				  	<table class="productReviewTitle" border="0" cellpadding="0" cellspacing="0" width="100%">
					  <tr>
					  	<td class="productReview"><fmt:message key="productReviews" /></td>
					  	<td align="right" class="writeReview">
					  	<c:if test="${siteConfig['PRODUCT_REVIEW_TYPE'].value != 'anonymous'}">
				          <a href="${_contextpath}/addProductReview.jhtm?sku=${model.product.sku}&cid=${model.cid}" ><fmt:message key="reviewThisProduct" /></a>
				          </c:if>
				          <c:if test="${siteConfig['PRODUCT_REVIEW_TYPE'].value == 'anonymous'}">
				          <a href="${_contextpath}/productReviewAnon.jhtm?sku=${model.product.sku}&cid=${model.cid}" ><fmt:message key="reviewThisProduct" /></a>
				        </c:if>
				        </td>
					  </tr>
					</table></td>  	
				  </tr>
				<c:forEach items="${model.productReviewListLast3}" var="review" varStatus="status">
				  <tr class="row${status.index % 2}"><td>
				    <table border="0" cellpadding="0" cellspacing="0" width="100%">
				      <tr>
				      	<td align="right" class="reviewDate"><fmt:formatDate type="date" pattern="MM/dd/yyyy" value="${review.created}"/></td>
				      </tr> 	
				      <tr>  
					    <td class="reviewRate"><jsp:include page="/WEB-INF/jsp/frontend/common/productRateImage.jsp"><jsp:param name="productRateAverage" value="${review.rate}" /></jsp:include></td>
					  </tr>
					  <tr>
					    <td class="reviewTitle"><c:out value="${review.title}"/></td>
					  </tr>
					  <tr>  
					    <td class="reviewer">By: <c:out value="${review.userName}"/></td>
					  </tr>
					  <tr>  
					    <td class="reviewText"><span><c:out value="${review.text}" escapeXml="false"/></span></td>  				
					  </tr>
				     </table>
				  </tr>
				</c:forEach>
				</table>
			</td>
		  </tr>
		  <tr id="readmore">
		    <td><input type="button" onclick="showdiv('review');" value="<fmt:message key="f_readAll" />" id="button" name="button" /></td>
		  </tr>
		</table>
		</form>
		</c:if>
		<!-- product review end -->
	  </div>
	  
	  <c:if test="${gSiteConfig['gPRODUCT_REVIEW'] and siteConfig['PRODUCT_RATE'].value != 'true' and model.product.enableRate}">
	  <div id="review" class="reviewDiv" style="visibility: hidden;">
		<div class="closeBar">
	         <a style="font-size: 11px; margin-right: 5px;" class="fourAstyle" href="javascript:onclick=closediv('review');"><b>[X]</b></a>
        </div>
        <h4 title="Product Review" id="6">Review</h4>
		<div class="reviewList">
		  <%@ include file="/WEB-INF/jsp/frontend/common/productReviewList.jsp" %>
		</div>		
	  </div>
	  </c:if>	

	 <c:if test="${model.recommendedList != null}">
	   <table class="recommended_list_box" border="0" cellspacing="0" cellpadding="0">
	     <tr>
	       <td>
	       <c:choose>
			 <c:when test="${!empty product.recommendedListTitle }">
			  <div class="recommended_title"><c:out value="${product.recommendedListTitle}"/></div>
			 </c:when>
			 <c:otherwise>
			  <c:if test="${siteConfig['RECOMMENDED_LIST_TITLE'].value != ''}">
			   <div class="recommended_title"><c:out value="${siteConfig['RECOMMENDED_LIST_TITLE'].value}"/></div>
			  </c:if>
			 </c:otherwise>
			</c:choose>
	       </td>
	     </tr>
	     <tr>
	       <td align="center">
	       <div class="recommendedList">
	       <ul class="recList">
	       <c:forEach items="${model.recommendedList}" var="product" varStatus="status">
	       
	       <c:set var="productLink">${_contextpath}/product.jhtm?id=${product.id}</c:set>
		   <c:if test="${gSiteConfig['gMOD_REWRITE'] == '1' and not fn:contains(product.sku, '/')}">
			<c:set var="productLink">${_contextpath}/${siteConfig['MOD_REWRITE_PRODUCT'].value}/${product.encodedSku}/${product.encodedName}.html</c:set>
		   </c:if>	

	         <li>
	           <table class="recProduct" border="0" cellspacing="0" cellpadding="0">
	            <tr class="row${staus.index%2}" valign="top">
	              <td>
	                <input type="checkbox" id="checkbox_${product.id}" onClick="toggleRecommended(${product.id})"/>
	              </td>
	              <td>
	                <a href="${productLink}"><img src="<c:if test="${!product.thumbnail.absolute}">${_contextpath}/assets/Image/Product/thumb/</c:if><c:out value="${product.thumbnail.imageUrl}"/>" border="0" class="thumbnail_image" alt="<c:out value="${product.name}"/><c:if test="${product.shortDesc != ''}"> - <c:out value="${product.shortDesc}"/></c:if>"></a>
	              </td>
	              <td>
	                <table class="productInfo">
	                  <tr>
	                    <td class="shortDesc">
	                      <a href="${productLink}" class="thumbnail_item_name"><c:out value="${product.shortDesc}" escapeXml="false" /></a>
	                    </td>
	                  </tr>
	                  <tr>
	                    <td class="price">
	                      <span>Price:</span><fmt:message key="${siteConfig['CURRENCY'].value}" /><fmt:formatNumber value="${product.price1}" pattern="#,##0.00" />
	                    </td>
	                  </tr>
	                </table>
	              </td>
	            </tr>
	           </table>
	         </li>
	       </c:forEach>
	       </ul>
	       </div>
	       </td>
	     </tr>
	   </table>
      </c:if>
	</td>
</tr>
</table>


<c:if test="${fn:trim(model.productLayout.footerHtml) != ''}">
  <c:set value="${model.productLayout.footerHtml}" var="footerHtml"/>
  <%@ include file="/WEB-INF/jsp/frontend/common/footerDynamicElement.jsp" %>
  <div style="float:left;width:100%"><c:out value="${footerHtml}" escapeXml="false"/></div>
</c:if>
</c:if>
    
  </tiles:putAttribute>
</tiles:insertDefinition>