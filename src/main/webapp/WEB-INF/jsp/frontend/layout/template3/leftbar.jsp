<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>

<td valign="top" class="leftbar">
<c:out value="${layout.leftBarTopHtml}" escapeXml="false"/>
<c:choose>
<c:when test="${siteConfig['ACCORDION_TYPE'].value == 0}">
<script type="text/javascript" src="${_contextpath}/javascript/clientcide.2.2.0.js"></script>
<script type="text/javascript">
<!--
var mymultiOpen;
window.addEvent('domready', function(){			
	myMultiOpen = new MultipleOpenAccordion($('AccordionMulti'), {
		elements: $$('#AccordionMulti div.stretcher'),
		togglers: $$('#AccordionMulti div.stretchtoggle'),
		onActive: function(toggler, section){
			toggler.getElement('img').set('src', '${_contextpath}/assets/Image/Layout/filter_minus.jpg');
		},
		onBackground: function(toggler, section){
			toggler.getElement('img').set('src', '${_contextpath}/assets/Image/Layout/filter_plus.jpg');
		},
		openAll: false
	});
	
});
function toggleAll() {
	myMultiOpen.toggleAll(new Fx());
}
//-->
</script>
</c:when>
<c:otherwise>
<script src="${_contextpath}/javascript/mootools-1.2.5.1-more.js" type="text/javascript"></script>
<script type="text/javascript">
<!--
window.addEvent('domready', function(){
	var myAccordion = new Accordion($('AccordionMulti'), '#AccordionMulti div.stretchtoggle', '#AccordionMulti div.stretcher', {
		opacity: false,
		display: -1,
		onActive: function(toggler, section){
			toggler.getElement('img').set('src', '${_contextpath}/assets/Image/Layout/filter_minus.jpg');
		},
		onBackground: function(toggler, section){
			toggler.getElement('img').set('src', '${_contextpath}/assets/Image/Layout/filter_plus.jpg');
		}
	});
});	
//-->
</script>
</c:otherwise>
</c:choose>


<form method="post" action="${_contextpath}/category.jhtm">
<input type="hidden" value="${model.thisCategory.id}" name="cid"/>
<div id="categoryList_left" >
<div class="AccordionMulti" id="AccordionMulti">
<c:forEach items="${model.mainCategories}" var="category">
	  <div class="stretchtoggle" id="leftBar_main_cat_${category.id}">
	    <div class="category ${fn:replace(category.name, " ", "")}" >
		  <span class="category_plusminus">
		    <img border="0" style="margin-bottom: -2px;" alt="collapse" src="${_contextpath}/assets/Image/Layout/filter_minus.jpg"/>
		  </span>
		  <span class="category_name"><c:out value="${category.name}" escapeXml="false" /></span>
		</div>	
      </div>
	  <div class="stretcher">
	  <div class="subCategoryBox ${fn:replace(category.name, " ", "")}" >
	  <c:forEach items="${category.subCategories}" var="subCat" varStatus="subCatStatus">
	    <div class="subCategory" id="leftBar_sub_cat_${subCat.id}">
	      <span><a href="${_contextpath}/category.jhtm?cid=${subCat.id}"><c:out value="${subCat.name}" escapeXml="false" /></a></span>
	    </div>
	  </c:forEach>
	  </div> 
	  </div> 
</c:forEach>

</div>
</div>
</form>
<c:out value="${layout.leftBarBottomHtml}" escapeXml="false"/>
</td>