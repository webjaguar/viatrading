<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<c:if test="${layout.rightBarTopHtml != '' or layout.rightBarBottomHtml != ''}">
<td valign="top" class="rightbar">
<c:out value="${layout.rightBarTopHtml}" escapeXml="false"/>
<c:out value="${layout.rightBarBottomHtml}" escapeXml="false"/>
</td>
</c:if>