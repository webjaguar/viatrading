<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<table class="SearchBox" border="0" cellspacing="0" cellpadding="5" align="center">
  <tr>
    <td class="searchName" align="right"><fmt:message key="keywords" />: </td>
    <td class="inputSearch"><input type="text" name="keywords" value="<c:out value="${frontProductSearch.keywords}"/>" size="50">
    	<c:if test="${not model.minCharsMet}">
    		<div style="font-weight:bold;color:#990000">
    		  <fmt:message key="f_keywordsMinCharsError">
				<fmt:param><c:out value="${siteConfig['SEARCH_KEYWORDS_MINIMUM_CHARACTERS'].value}" /></fmt:param>
			  </fmt:message></div>
    	</c:if>
    	<c:if test="${model.hasKeywordsLessMinChars and model.minCharsMet}">
    		<div style="font-weight:bold;color:#990000">
    		  <fmt:message key="f_keywordsMinCharsIgnore">
				<fmt:param><c:out value="${siteConfig['SEARCH_KEYWORDS_MINIMUM_CHARACTERS'].value}" /></fmt:param>
			  </fmt:message></div>
    	</c:if>
    </td>
  </tr>
  <c:if test="${siteConfig['SEARCH_KEYWORDS_CONJUNCTION'].value == 'true'}">
  <tr>
    <td>&nbsp;</td>
    <td><input type="radio" name="comp" value="OR" <c:if test="${frontProductSearch.conjunction == 'OR'}">checked</c:if>><fmt:message key="f_atLeastOneWordMustMatch"/><br>
        <input type="radio" name="comp" value="AND" <c:if test="${frontProductSearch.conjunction == 'AND'}">checked</c:if>><fmt:message key="f_allWordsMustMatch"/></td>
  </tr>
  </c:if>
  <c:if test="${model.searchTree != null}">
  <tr>
    <td class="searchName"><fmt:message key="search" /> <fmt:message key="category" />: </td>
    <td class="inputSearch">
	  <select name="category" class="inputSearchSelect" style="width:200px">
		<option value=""><fmt:message key="allCategories"/></option>
		<c:forEach items="${model.searchTree}" var="category">
		<option value="${category.id}" <c:if test="${frontProductSearch.category == category.id}">selected</c:if>><c:out value="${category.name}" escapeXml="false" /></option>
		</c:forEach>
      </select>  
	</td>
  </tr>  
  <tr>
    <td>&nbsp;</td>
    <td>
	  <input name="includeSubs" type="checkbox" id="includeSubs" <c:if test="${frontProductSearch.includeSubs}">checked</c:if>> <fmt:message key="includeSubs" />
	</td>
  </tr>
  </c:if>
  <c:if test="${siteConfig['SEARCH_MINMAXPRICE'].value == 'true'}">
  <tr>
    <td class="searchName" align="right"><fmt:message key="productPrice" />: </td>
    <td class="inputSearchPrice">
      <input type="text" name="minPrice" value="<c:out value="${frontProductSearch.minPrice}"/>" size="10">
      -
      <input type="text" name="maxPrice" value="<c:out value="${frontProductSearch.maxPrice}"/>" size="10">
	</td>
  </tr>
  </c:if>
  <tr>
    <td class="submitSearch" colspan="2" align="right"><input type="submit" value="<fmt:message key="search" />" class="f_button" /></td>
  </tr>
</table>

