<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<head>
<c:if test="${model.product.imageCount > 1}" >
<script type="text/javascript">
<!-- 
current = 0;
function next() 
{
  if (document.slideform.slide[current+1]) 
  {
    document.images.show.src = document.slideform.slide[current+1].value;
    document.slideform.slide.selectedIndex = ++current;
  }
}
function previous() 
{
  if (current-1 >= 0) 
  {
    document.images.show.src = document.slideform.slide[current-1].value;
    document.slideform.slide.selectedIndex = --current;
  }
}
//-->
</script>
</c:if>
<style type="text/css">
.productName {color: #FFF; float:left;}
.printButton {float:right;}
</style>
</head>

<html>
<body>
<form name="slideform">
<table cellspacing="1" cellpadding="4" bgcolor="#000000">
<c:if test="${model.product.imageCount > 1}" >
<tr align="center">
  <td bgcolor="#C0C0C0">
  <input type="button" onClick="previous();" value="Previous" title="Previous">
  <input type="button" onClick="next();" value="Next" title="Next">
  </td>
</tr>
</c:if>
<tr>
<td><div class="productName"><c:out value="${model.product.name}"/></div><div class="printButton"><input type="button" style="cursor:pointer;" onclick="self.print()" value="Print"></div></td>
</tr>
<tr>
<td>
  <img src="<c:out value="${model.imgUrl}"/><c:if test="${model.product.feed == 'DSDI'}">&w=800&h=600</c:if>" name="show" />	
</td>
</tr>
<tr>
<c:forEach items="${model.product.images}" var="image" varStatus="status">
<c:choose>
  <c:when test="${image.absolute}">
	<input type="hidden" name="slide" value="<c:out value="${image.imageUrl}"/>" />
  </c:when>
  <c:otherwise>
	<input type="hidden" name="slide" value="assets/Image/Product/detailsbig/<c:out value="${image.imageUrl}"/>" />
  </c:otherwise>
</c:choose>
</c:forEach>
</tr>
</table>
</form>
</body>
</html>