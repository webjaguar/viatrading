<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN"
  "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title><c:if test="${model.product.field16 == null}"><c:out value="${model.product.name}" /></c:if><c:if test="${model.product.field16 != null}"><c:out value="${model.product.field16}" /></c:if></title>
<meta http-equiv="Content-type" content="text/html; charset=utf-8" />
<meta name="keywords" content="<c:if test="${model.product.field17 == null}">Meta Keywords</c:if><c:if test="${model.product.field17 != null}"><c:out value="${model.product.field17}" /></c:if>"/>
<meta name="description" content="<c:if test="${model.product.field18 == null}">Meta Description</c:if><c:if test="${model.product.field18 != null}"><c:out value="${model.product.field18}" /></c:if>"/>

<link rel="stylesheet" href="${_contextpath}/assets/SmoothGallery/popSlideShow.css" type="text/css" media="screen" />

<script src="http://ajax.googleapis.com/ajax/libs/jquery/1/jquery.min.js"></script>
<script type="text/javascript">
$.fn.infiniteCarousel = function () {
    function repeat(str, num) {
        return new Array( num + 1 ).join( str );
    }
    return this.each(function () {
        var $wrapper = $('> div', this).css('overflow', 'hidden'),
            $slider = $wrapper.find('> ul'),
            $items = $slider.find('> li'),
            $single = $items.filter(':first'),
            
            singleWidth = $single.outerWidth(), 
            visible = Math.ceil($wrapper.innerWidth() / singleWidth),
            currentPage = 1,
            pages = Math.ceil($items.length / visible);  

        $items.mouseover(function () {
   			$('#bigImage').find('img').attr('src',$(this).find('img:first').attr('src'));
        });       
        if (($items.length % visible) != 0) {
            $slider.append(repeat('<li class="empty" />', visible - ($items.length % visible)));
            $items = $slider.find('> li');
        }
        $items.filter(':first').before($items.slice(- visible).clone().addClass('cloned'));
        $items.filter(':last').after($items.slice(0, visible).clone().addClass('cloned'));
        $items = $slider.find('> li'); // reselect
        $wrapper.scrollLeft(singleWidth * visible);
        function gotoPage(page) {
            var dir = page < currentPage ? -1 : 1,
                n = Math.abs(currentPage - page),
                left = singleWidth * dir * visible * n;
            
            $wrapper.filter(':not(:animated)').animate({
                scrollLeft : '+=' + left
            }, 500, function () {
                if (page == 0) {
                    $wrapper.scrollLeft(singleWidth * visible * pages);
                    page = pages;
                } else if (page > pages) {
                    $wrapper.scrollLeft(singleWidth * visible);
                    page = 1;
                } 
                currentPage = page;
            });                
            return false;
        }
        $wrapper.after('<a class="arrow back">&lt;</a><a class="arrow forward">&gt;</a>');
        $('a.back', this).click(function () {
            return gotoPage(currentPage - 1);                
        });
        $('a.forward', this).click(function () {
            return gotoPage(currentPage + 1);
        });
        $(this).bind('goto', function (event, page) {
            gotoPage(page);
        });
    });  
};
$(document).ready(function () {
  $('.infiniteCarousel').infiniteCarousel();
});
</script>
</head>
<body>

<div class="imageViewWrapper">
<div id="bigImage">

<c:forEach items="${model.product.images}" var="image" varStatus="status">

 <c:if test="${status.first}" >
 <c:choose>
  <c:when test="${image.absolute}">
	<div><img src="<c:out value="${image.imageUrl}"/>" alt="${model.product.name}"/></div>
  </c:when>
  <c:otherwise>
	<div><img src="${_contextpath}/assets/Image/Product/detailsbig/<c:out value="${image.imageUrl}"/>" alt="${model.product.name}"/></div>
  </c:otherwise>
</c:choose>
 </c:if>
</c:forEach>
</div>
<div class="infiniteCarousel">
   <div class="wrapper">
   <ul>
<c:forEach items="${model.product.images}" var="image" varStatus="status">
<c:choose>
  <c:when test="${image.absolute}">
	<li><img src="<c:out value="${image.imageUrl}"/>" alt="${model.product.name}"/></li>
  </c:when>
  <c:otherwise>
	<li><img src="${_contextpath}/assets/Image/Product/detailsbig/<c:out value="${image.imageUrl}"/>" alt="${model.product.name}"/></li>
  </c:otherwise>
</c:choose>
</c:forEach>
   </ul>        
   </div>
</div>
</div>
</body>
</html>