<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>

<c:forEach items="${model.breadCrumbs}" var="category" varStatus="status">
<c:if test="${status.first}">
<!-- bread crumbs -->
<table border="0" cellpadding="0" cellspacing="0" class="breadcrumbs" id="breadcrumbs">
  <tr>
    <td><a href="${_contextpath}/" class="breadcrumb_home"><c:choose><c:when test="${siteConfig['BREADCRUMBS_HOME_TITLE'].value != ''}"><c:out value="${siteConfig['BREADCRUMBS_HOME_TITLE'].value}" /></c:when><c:otherwise><fmt:message key="home" /></c:otherwise></c:choose></a>
</c:if>
	> 
	<c:choose>
	  <c:when test="${category.linkType != 'NONLINK'}"><a href="${_contextpath}/<c:choose><c:when test="${gSiteConfig['gMOD_REWRITE'] == '1'}">${siteConfig['MOD_REWRITE_CATEGORY'].value}/${category.id}/${category.encodedName}.html</c:when><c:otherwise>category.jhtm?cid=${category.id}</c:otherwise></c:choose>"
		 class="breadcrumb"><c:out value="${category.name}" escapeXml="false" /></a></c:when>
	  <c:when test="${category.linkType == 'NONLINK'}"><span class="breadcrumb_off"><c:out value="${category.name}" escapeXml="false" /></span></c:when>
	</c:choose>
<c:if test="${status.last}">
<c:if test="${model.product != null}">
	>
	<span class="breadcrumb_productName"><c:out value="${model.product.name}" escapeXml="false" /></span>
</c:if>
	</td>
  </tr>
</table>
</c:if>
</c:forEach>  