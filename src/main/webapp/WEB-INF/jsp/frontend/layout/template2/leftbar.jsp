<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>

<td valign="top" class="leftbar">
<c:out value="${layout.leftBarTopHtml}" escapeXml="false"/>
<c:if test="${!empty model.dropDownList}" >
<script type="text/javascript" src="${_contextpath}/javascript/clientcide.2.2.0.js"></script>
<script type="text/javascript">
<!--

var mymultiOpen;
window.addEvent('domready', function(){	
	myMultiOpen = new MultipleOpenAccordion($('AccordionMulti'), {
		elements: $$('#AccordionMulti div.stretcher'),
		togglers: $$('#AccordionMulti div.stretchtoggle'),
		onActive: function(toggler, section){
			toggler.getElement('img').set('src', '${_contextpath}/assets/Image/Layout/filter_minus.jpg');
		},
		onBackground: function(toggler, section){
			toggler.getElement('img').set('src', '${_contextpath}/assets/Image/Layout/filter_plus.jpg');
		},
		openAll: true
	});
	<c:if test="${gSiteConfig['gSITE_DOMAIN'] == 'hometheatergear.com'}">	
	$$('span.toolTipImg').each(function(element,index) {  
	    var content = element.get('title').split('::');  
	    element.store('tip:title', content[0]);  
	    element.store('tip:text', content[1]);  
		});
	var Tips1 = new Tips($$('.toolTipImg'));
	</c:if>
});
function toggleAll() {
	myMultiOpen.toggleAll(new Fx());
}
//-->
</script>

<form method="get" action="${_contextpath}/category.jhtm">
<input type="hidden" value="${model.thisCategory.id}" name="cid"/>
<div id="narrowList_left" >
<div class="AccordionMulti" id="AccordionMulti">
<div id="narrowList_title_box"><div id="narrowList_title">Narrow Results By:</div></div>
<c:set value="cat_${model.thisCategory.id}" var="thisCat"/>
<c:forEach items="${model.dropDownList}" var="dropDownList" varStatus="dropDownStatus">
<c:set value="field_${dropDownList.fieldId}" var="field"/>
	  <div class="stretchtoggle">
	    <div style="">
		  <a class="filter_header" class="filter_header_expand" href="#">
		  <span class="filter_plusminus">
		    <img border="0" style="margin-bottom: -2px;" alt="collapse" src="${_contextpath}/assets/Image/Layout/filter_minus.jpg"/>
		  </span>
		  <span class="filter_name"><c:out value="${dropDownList.name}" /></span>
		  </a>
		</div>	
      </div>
	  <div class="stretcher">
	  <div class="filter_choices ${fn:replace(dropDownList.name, " ", "")}" >
	  <c:forEach items="${dropDownList.value}" var="value" varStatus="status">
	    <div class="filter">
	    <input id="field_${dropDownList.fieldId}_${value}" onclick="javascript:submit();" type="checkbox" name="field_${dropDownList.fieldId}" 
	    	<c:forEach items="${catProductFieldMap[thisCat][field].selectedValues}" var="selectedValue">
	      		<c:if test="${selectedValue == value}">checked</c:if>
	    	</c:forEach> value="<c:out value="${value}"/>" />
	    <c:choose>
        <c:when test="${gSiteConfig['gSITE_DOMAIN'] == 'hometheatergear.com'}">	
	      <span class="toolTipImg" title="::<img src='${_contextpath}/assets/FilterTipImages/${fn:replace(value, " ", "")}.jpg' />" ><c:out value="${value}" /></span>
        </c:when>
        <c:otherwise>
          <span>
            <c:choose>
              <c:when test="${fn:contains(value, '|')}">
                 <c:out value="${fn:split(value, '|')[1]}" />
              </c:when>
              <c:otherwise>
                 <c:out value="${value}" />
              </c:otherwise>
            </c:choose>
         </span>
        </c:otherwise>	
        </c:choose>    
	    </div>
	  </c:forEach>
	  </div> 
	  </div> 
</c:forEach>
<input type="hidden" name="field_1" value=""/>
<div id="filter_button">
<div class="search">
<div class="reset">
<img border="0" name="_reset" alt="reset" src="${_contextpath}/assets/Image/Layout/button_filter_reset.gif" onclick="window.location='${_contextpath}/category.jhtm?cid=${model.thisCategory.id}&field_1='"/>
</div>
<a href="#" onclick="javascript:toggleAll();">toggleAll</a>
</div>
</div>
<div id="narrowList_footer_box"><div id="narrowList_footer"></div></div>

</div>
</div>
</form>
</c:if>
<c:out value="${layout.leftBarBottomHtml}" escapeXml="false"/>
</td>