<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>

<c:if test="${siteConfig['SUBCAT_DISPLAY'].value == 'onePerRow' and not empty model.subcatLinksPerColMap}">
<c:set var="subcatCol" value="0"/>
<c:set var="subcatCount" value="1"/>

<table class="subCategories" border="0" <c:if test="${siteConfig['SUBCAT_DISPLAY'].value == 'onePerRow'}">width="100%"</c:if>>
<tr>
<td>
<div class="subCatAnchorHeader"><c:out value="${model.thisCategory.name}" escapeXml="false" /></div>
<table class="subCatAnchor" border="0" width="100%">
<tr>
<td width="${100/model.thisCategory.subcatCols}%" valign="top">

<c:forEach items="${model.subCategories}" var="category" varStatus="status">
  <div><a href="#<c:out value='${category.name}' />" class="subCatLinkAnchor"><c:out value="${category.name}" escapeXml="false" /></a></div>
<c:choose>
  <c:when test="${model.subcatLinksPerCol[subcatCol] == subcatCount && not status.last}">
    </td>
    <td width="${100/model.thisCategory.subcatCols}%" valign="top">
	<c:set var="subcatCol" value="${subcatCol+1}"/>
	<c:set var="subcatCount" value="1"/>
  </c:when>
  <c:otherwise>
    <c:set var="subcatCount" value="${subcatCount+1}"/>
  </c:otherwise>
</c:choose>

<c:if test="${status.last}">
<c:forEach begin="${subcatCol+2}" end="${model.thisCategory.subcatCols}">
</td>
<td width="${100/model.thisCategory.subcatCols}%">&nbsp;
</c:forEach>
</c:if>

</c:forEach>

</td>
</tr>
</table>
</td>
</tr>
</table>
</c:if>

<c:set var="subcatCol" value="0"/>
<c:set var="subcatCount" value="1"/>

<table class="subCategories" border="0" <c:if test="${siteConfig['SUBCAT_DISPLAY'].value == 'onePerRow'}">width="100%"</c:if>>
<tr>
<td valign="top">

<c:forEach items="${model.subCategories}" var="category" varStatus="status">

   <c:if test="${siteConfig['SUBCAT_DISPLAY'].value == 'cell'}">
   <div class="categoryWrapper" style="float:left;">
   </c:if>

   <c:if test="${siteConfig['SUBCAT_DISPLAY'].value == 'onePerRow' and not empty model.subcatLinksPerColMap}">
   <table width="100%" border="0" cellpadding="0" cellspacing="0" class="subCatHeader"><tr><td>
   </c:if>

   <%-- TO BE DELETED <a name="<c:out value='${category.name}'/>"></a>  --%>
   <c:if test="${category.linkType != 'NONLINK'}">      
	  <a href="${_contextpath}/<c:choose><c:when test="${gSiteConfig['gMOD_REWRITE'] == '1'}">${siteConfig['MOD_REWRITE_CATEGORY'].value}/${category.id}/${category.encodedName}.html</c:when><c:otherwise>category.jhtm?cid=${category.id}</c:otherwise></c:choose>" <c:if test="${category.urlTarget == '_blank'}">target="_blank"</c:if> class="subCatLink">
   </c:if>
   <c:choose>
	    <c:when test="${ not empty (category.imageUrl) and category.imageUrl != null}">
	    	<c:choose>
	    	<c:when test="${category.absolute}">
	    		<img src="<c:url value="${category.imageUrl}"/>" border="0" alt="${category.imageUrlAltName}">
	    	</c:when>
	    	<c:otherwise>
	    		<img src="<c:url value="${_contextpath}/assets/Image/Category/${category.imageUrl}"/>" border="0" alt="${category.imageUrlAltName}">
	    	</c:otherwise>
	    	</c:choose>
	    </c:when>
	    <c:when test="${category.hasImage}">
		  	<img src="<c:url value="${_contextpath}/assets/Image/Category/cat_${category.id}.gif"/>" border="0" alt="${category.imageUrlAltName}">
		</c:when>
   </c:choose>
   <div class="catName" <c:if test="${siteConfig['SUBCAT_DISPLAY'].value != 'onePerRow'}">align="center"</c:if>>
      <c:out value="${category.name}" escapeXml="false" />
      <c:if test="${(category.productCount > 0) and (siteConfig['SUBCAT_PRODUCT_COUNT'].value == 'true')}">(<c:out value="${category.productCount}" />)</c:if></div>
   <c:if test="${category.linkType != 'NONLINK'}">
	  </a> 
   </c:if>
   
   <c:if test="${siteConfig['SUBCAT_DISPLAY'].value == 'onePerRow' and not empty model.subcatLinksPerColMap}">
   </td><td><a href="#" class="back_to_top"><fmt:message key="backToTop" /></a></td></tr></table>
   </c:if>
   
   <c:if test="${not empty category.subCategories}">
   <c:set var="subsubcatCol" value="0"/>
   <c:set var="subsubcatCount" value="1"/>
   
   <c:if test="${siteConfig['SUBCAT_DISPLAY'].value == 'onePerRow'}">
   <table border="0" width="100%">
   <tr>
   <td width="${100/model.thisCategory.subcatCols}%" valign="top">
   </c:if>

   <c:forEach items="${category.subCategories}" var="subCat" varStatus="subCatStatus">
     <c:if test="${subCatStatus.index < siteConfig['SUBCAT_MAX'].value || siteConfig['SUBCAT_DISPLAY'].value == 'onePerRow'}">     
     <div <c:if test="${subCat.linkType == 'NONLINK'}">class="subSubCatLink_nonlink"</c:if>>
  	 <c:if test="${subCat.linkType != 'NONLINK'}"><a href="${_contextpath}/<c:choose><c:when test="${gSiteConfig['gMOD_REWRITE'] == '1'}">${siteConfig['MOD_REWRITE_CATEGORY'].value}/${subCat.id}/${subCat.encodedName}.html</c:when><c:otherwise>category.jhtm?cid=${subCat.id}</c:otherwise></c:choose>" <c:if test="${subCat.urlTarget == '_blank'}">target="_blank"</c:if> class="subSubCatLink<c:if test="${subCat.hasSubcats}">Plus</c:if>"></c:if>
  	 <c:out value="${subCat.name}" escapeXml="false" />
	 <c:if test="${(subCat.productCount > 0) and (siteConfig['SUBCAT_PRODUCT_COUNT'].value == 'true')}">(<c:out value="${subCat.productCount}" />)</c:if>
  	 <c:if test="${subCat.linkType != 'NONLINK'}"></a></c:if> 
	</div>
	</c:if>
	<c:if test="${subCatStatus.index == siteConfig['SUBCAT_MAX'].value && siteConfig['SUBCAT_DISPLAY'].value != 'onePerRow'}">
	<div><a href="${_contextpath}/<c:choose><c:when test="${gSiteConfig['gMOD_REWRITE'] == '1'}">category/${category.id}/${category.encodedName}.html</c:when><c:otherwise>category.jhtm?cid=${category.id}</c:otherwise></c:choose>" class="subSubCatLink">More...</a></div>
	</c:if>
    
	<c:choose>
	  <c:when test="${model.subcatLinksPerColMap[category.id][subsubcatCol] == subsubcatCount && not subCatStatus.last}">
	    </td>
	    <td width="${100/model.thisCategory.subcatCols}%" valign="top">
		<c:set var="subsubcatCol" value="${subsubcatCol+1}"/>
		<c:set var="subsubcatCount" value="1"/>
	  </c:when>
	  <c:otherwise>
	    <c:set var="subsubcatCount" value="${subsubcatCount+1}"/>
	  </c:otherwise>
	</c:choose>    
	
	<c:if test="${siteConfig['SUBCAT_DISPLAY'].value == 'onePerRow' and subCatStatus.last}">
	<c:forEach begin="${subsubcatCol+2}" end="${model.thisCategory.subcatCols}">
	</td>
	<td width="${100/model.thisCategory.subcatCols}%">&nbsp;
	</c:forEach>
	</c:if>
	
   </c:forEach>
   
   <c:if test="${siteConfig['SUBCAT_DISPLAY'].value == 'onePerRow'}">
   </td>
   </tr>
   </table>
   </c:if>
   
   </c:if>	

  	<br/>

<c:choose>
  <c:when test="${model.subcatLinksPerCol[subcatCol] == subcatCount && not status.last && siteConfig['SUBCAT_DISPLAY'].value != 'onePerRow'}">
    </td>
    <td valign="top">
	<c:set var="subcatCol" value="${subcatCol+1}"/>
	<c:set var="subcatCount" value="1"/>
  </c:when>
  <c:otherwise>
    <c:set var="subcatCount" value="${subcatCount+1}"/>
  </c:otherwise>
</c:choose>

   <c:if test="${siteConfig['SUBCAT_DISPLAY'].value == 'cell'}">
   </div>
   </c:if>

</c:forEach>

</td>
</tr>
</table>

