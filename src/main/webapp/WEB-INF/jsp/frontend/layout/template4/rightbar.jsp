<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<c:if test="${layout.rightBarTopHtml != '' or layout.rightBarBottomHtml != ''}">
<div id="rightSidebarWrapper">
	<div id="rightSidebar">
<c:out value="${layout.rightBarTopHtml}" escapeXml="false"/>
<c:out value="${layout.rightBarBottomHtml}" escapeXml="false"/>
    </div><!-- end rightSidebar -->
    </div><!-- end rightSidebarWrapper -->
<div class="clearboth">&nbsp;</div><!-- floating ends here -->
</c:if>