<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>

<c:if test="${!empty model.dropDownList}" >
<div class="topCheckBoxFilter_main">
<div id="narrowList_title_box_top"><div id="narrowList_title_top">Narrow Results By:</div></div>
<c:set value="cat_${model.thisCategory.id}" var="thisCat"/>
	<div class="checkBoxFilter">
		<c:forEach items="${model.dropDownList}" var="dropDownList" varStatus="dropDownStatus">
			<c:set value="field_${dropDownList.fieldId}" var="field"/>
			<ul>
				<div class="stretchtoggle_top">
				  <a class="filter_header" class="filter_header_expand" href="#">
				    <span class="filter_name  <c:if test="${fn:length(catProductFieldMap[thisCat][field].selectedValues) > 0}">filter_selected</c:if>"><c:out value="${dropDownList.name}" /></span>
				  </a>	
				</div>
		  		<div class="stretcher_top">
		  		<div class="filter_choices_top ${fn:replace(dropDownList.name,"", "")}" >
				<c:forEach items="${dropDownList.value}" var="value" varStatus="status">
				<li>
				<div class="filter_top">
				    <input id="field_${dropDownList.fieldId}_${value}" onclick="javascript:submit();" type="checkbox" name="field_${dropDownList.fieldId}" 
				    	<c:forEach items="${catProductFieldMap[thisCat][field].selectedValues}" var="selectedValue">
				      		<c:if test="${selectedValue == value}">checked</c:if>
				    	</c:forEach> value="<c:out value="${value}"/>" />
				    <c:choose>
			        <c:when test="${gSiteConfig['gSITE_DOMAIN'] == 'hometheatergear.com'}">	
				      <span class="toolTipImg" title="::<img src='${_contextpath}/assets/FilterTipImages/${fn:replace(value, " ", "")}.jpg' />" ><c:out value="${value}" /></span>
			        </c:when>
			        <c:otherwise>
			          <span><c:out value="${value}" /></span>
			        </c:otherwise>	
			        </c:choose>    
				</div>
				</li>			
				</c:forEach>
				</div>
				</div>		
			</ul>
		</c:forEach>
	</div>
	
	<input type="hidden" name="field_1" value=""/>
	<div id="filter_button_top">
	<div class="reset_top">
		<img border="0" name="_reset" alt="reset" src="${_contextpath}/assets/Image/Layout/button_filter_reset.gif" onclick="window.location='${_contextpath}/category.jhtm?cid=${model.thisCategory.id}&field_1='"/>
	</div>
	</div>
</div>
</c:if>