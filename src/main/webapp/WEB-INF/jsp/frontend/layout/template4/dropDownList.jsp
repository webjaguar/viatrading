<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<c:if test="${!empty model.dropDownList}" >
<div id="dropDownList_main" >
<p id="dropDownList_title">Narrow Results By:</p>
<p class="dropDownList_p1">
<c:set value="cat_${model.thisCategory.id}" var="thisCat"/>
<c:forEach items="${model.dropDownList}" var="dropDownList" varStatus="dropDownStatus">
<c:set value="field_${dropDownList.fieldId}" var="field"/>
	<select name="field_${dropDownList.fieldId}" class="dropDownList_select" onchange="submit();">
	    <option value="">All <c:out value="${dropDownList.name}" /></option>
	  <c:forEach items="${dropDownList.value}" var="value">
	  	<option value="<c:out value="${value}" />" 
	    	<c:forEach items="${catProductFieldMap[thisCat][field].selectedValues}" var="selectedValue">
	      		<c:if test="${selectedValue == value}">selected</c:if>
	    	</c:forEach>><c:out value="${value}" /></option>
	  </c:forEach>
	</select>
	<c:if test="${dropDownStatus.count % 4 == 0 }">
	  <br />
	</c:if>
</c:forEach>
<br/>
<input type="button" value="Reset" onclick="window.location='category.jhtm?cid=${model.thisCategory.id}&field_1='">
</p>
</div>
</c:if>