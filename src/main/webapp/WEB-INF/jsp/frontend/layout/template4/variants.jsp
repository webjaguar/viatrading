<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>

<form action="${_contextpath}/addVariantToCart.jhtm" method="post">
<input type="hidden" name="variantId" value="${model.product.id}"/>

<table border="0" cellpadding="3" cellspacing="1" align="center" class="quickmode">
  <tr>
    <th class="quickmode"><fmt:message key="productSku" /></th>
	<th class="quickmode"><fmt:message key="description" /></th>
	<th class="quickmode">Price</th>
	<c:if test="${gSiteConfig['gSHOPPING_CART']}">
	<th class="quickmode">Quantity</th>
	</c:if>
  </tr>
<c:forEach items="${model.product.variants}" var="variant" varStatus="status">
  <tr class="row${status.index % 2}">
    <td align="center"><c:out value="${variant.sku}" escapeXml="false" /></td>
    <td>
      <c:out value="${variant.name}" escapeXml="false" />
      <c:if test="${variant.shortDesc != null}"><br/><c:out value="${variant.shortDesc}" escapeXml="false" /></c:if>
    </td>
    <td align="right"><fmt:message key="${siteConfig['CURRENCY'].value}" /><fmt:formatNumber value="${variant.price1}" pattern="#,##0.00" /></td>
    <td align="center">
    	<input type="hidden" name="variant_${variant.id}_sku" value="<c:out value="${variant.sku}"/>"/>
    	<input type="text" size="6" maxlength="5" name="variant_${variant.id}_qty" value=""><c:if test="${product.packing != ''}"> <c:out value="${product.packing}"/> </c:if>
    </td>
  </tr>
</c:forEach>
</table>
<c:if test="${gSiteConfig['gSHOPPING_CART']}">
<div id="quickmode_v1_addtocart" align="right">
<input type="image" value="Add to Cart" name="_addtocart_quick" class="_addtocart_quick" src="${_contextpath}/assets/Image/Layout/button_addtocart${_lang}.gif" >
</div>
</c:if>
</form>
