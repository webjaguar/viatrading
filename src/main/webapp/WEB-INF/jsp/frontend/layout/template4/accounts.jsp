<%@ page import="java.io.*,java.util.*" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>

<c:forEach items="${model.accounts.pageList}" var="account" varStatus="status">
<c:if test="${status.first}">
<form action="${_contextpath}/category.jhtm">
<input type="hidden" name="id" value="${param.id}">
<div class="pageNavi">Page 
		<select name="page" onchange="submit()">
		<c:forEach begin="1" end="${model.accounts.pageCount}" var="page">
  	        <option value="${page}" <c:if test="${page == (model.accounts.page+1)}">selected</c:if>>${page}</option>
		</c:forEach>
		</select>
of <c:out value="${model.accounts.pageCount}"/></div>
</form>
</c:if>
<fieldset class="register_fieldset">
<legend><fmt:message key="company" /></legend>
<table border="0" cellpadding="0" cellspacing="2" width="100%">
<tr>
<td>
<table border="0" cellpadding="0" cellspacing="2" width="100%">
  <tr>
    <td width="30%" align="right"><fmt:message key="companyName" />:</td>
    <td>&nbsp;</td>
    <td><a href="member<c:out value="${account.id}"/>.jhtm"><c:out value="${account.address.company}"/></a></td>
  </tr>
  <tr>
    <td align="right"><fmt:message key="contactPerson" />:</td>
    <td>&nbsp;</td>
    <td><c:out value="${account.address.firstName}"/> <c:out value="${account.address.lastName}"/></td>
  </tr>
  <tr>
    <td align="right"><fmt:message key="address" />:</td>
    <td>&nbsp;</td>
    <td><c:out value="${account.address.addr1}"/> <c:out value="${account.address.addr2}"/></td>
  </tr>
  <tr>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td><c:out value="${account.address.city}"/>, <c:out value="${account.address.stateProvince}"/>
    		<c:out value="${account.address.country}"/>
    </td>
  </tr>
  <tr>
    <td align="right"><fmt:message key="zipCode" />:</td>
    <td>&nbsp;</td>
    <td><c:out value="${account.address.zip}"/></td>
  </tr>
  <tr>
    <td align="right"><fmt:message key="phone" />:</td>
    <td>&nbsp;</td>
    <td><c:out value="${account.address.phone}"/></td>
  </tr>
  <tr>
    <td align="right"><fmt:message key="fax" />:</td>
    <td>&nbsp;</td>
    <td><c:out value="${account.address.fax}"/></td>
  </tr>
  <tr>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td align="right">Website:</td>
    <td>&nbsp;</td>
    <td><a href="<c:out value="${account.field8}"/>" target="_blank"><c:out value="${account.field8}"/></a></td>
  </tr>
  <tr>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td><c:out value="${account.shortDesc}"/></td>
  </tr>
</table>  
</td>
<td width="100" valign="top">
<c:set var="supplierId" value="${account.supplierId}"/>
<%
Map<String, Object> model = (HashMap<String, Object>) request.getAttribute("model");
File baseImageFile = (File) model.get("baseImageFile");
if (pageContext.getAttribute("supplierId") != null && new File(baseImageFile, "CompanyLogo/logo_" + pageContext.getAttribute("supplierId") + ".gif").exists()) {
	pageContext.setAttribute("logo", "assets/Image/CompanyLogo/logo_" + pageContext.getAttribute("supplierId") + ".gif");
} else {
	pageContext.setAttribute("logo", null);
}
 %>
<c:if test="${logo != null}"><img src="${logo}" width="100"></c:if>
<c:if test="${logo == null}">&nbsp;</c:if>
</td>
</tr>
</table>  
</fieldset>
<c:if test="${status.last}">
</c:if>
</c:forEach>
