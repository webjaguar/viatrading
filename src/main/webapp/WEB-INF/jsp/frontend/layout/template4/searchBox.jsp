<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<div class="searchBox">
 <div class="searchKeywordWrapper" id="searchKeywordWrapperId">
   <div class="searchName"><fmt:message key="keywords" />:</div>
   <div class="searchInput">
     <input type="text" name="keywords" value="<c:out value="${frontProductSearch.keywords}"/>" size="50">
     <c:if test="${not model.minCharsMet}">
    	<div style="font-weight:bold;color:#990000">
    	  <fmt:message key="f_keywordsMinCharsError">
			<fmt:param><c:out value="${siteConfig['SEARCH_KEYWORDS_MINIMUM_CHARACTERS'].value}" /></fmt:param>
		  </fmt:message></div>
     </c:if>
     <c:if test="${model.hasKeywordsLessMinChars and model.minCharsMet}">
    	<div style="font-weight:bold;color:#990000">
    	  <fmt:message key="f_keywordsMinCharsIgnore">
			<fmt:param><c:out value="${siteConfig['SEARCH_KEYWORDS_MINIMUM_CHARACTERS'].value}" /></fmt:param>
		  </fmt:message></div>
     </c:if>
   </div>
   <div style="clear:both;"></div>
 </div>
 <c:if test="${siteConfig['SEARCH_KEYWORDS_CONJUNCTION'].value == 'true'}">
  <div class="searchConjunctionWrapper" id="searchConjunctionWrapperId">
    <div id="scOr">
      <input type="radio" name="comp" value="OR" <c:if test="${frontProductSearch.conjunction == 'OR'}">checked</c:if>><fmt:message key="f_atLeastOneWordMustMatch"/>
    </div>
    <div id="scAnd">
      <input type="radio" name="comp" value="AND" <c:if test="${frontProductSearch.conjunction == 'AND'}">checked</c:if>><fmt:message key="f_allWordsMustMatch"/>
    </div>
  </div>
  <div style="clear:both;"></div>
 </c:if>
 
 <c:if test="${model.searchTree != null}">
 <div class="searchTreeWrapper" id="searchTreeWrapperId">
   <div class="searchName"><fmt:message key="search" /> <fmt:message key="category" />:</div>
   <div class="searchInput">
	  <select name="category" class="inputSearchSelect" style="width:200px">
		<option value=""><fmt:message key="allCategories"/></option>
		<c:forEach items="${model.searchTree}" var="category">
		<option value="${category.id}" <c:if test="${frontProductSearch.category == category.id}">selected</c:if>><c:out value="${category.name}" escapeXml="false" /></option>
		</c:forEach>
      </select>  
   </div>
   <div style="clear:both;"></div>
   <div class="searchIncludeSubsWrapper" id="searchIncludeSubsWrapperId">
	  <input name="includeSubs" type="checkbox" id="includeSubs" <c:if test="${frontProductSearch.includeSubs}">checked</c:if>> <fmt:message key="includeSubs" />
   </div>
 </div>
 </c:if>
 
 
  <c:if test="${siteConfig['SEARCH_MINMAXPRICE'].value == 'true'}">
  <div class="searchPriceWrapper" id="searchPriceWrapperId">
    <div class="searchName" ><fmt:message key="productPrice" />:</div>
    <div class="searchInput" align="left">
      <input type="text" name="minPrice" value="<c:out value="${frontProductSearch.minPrice}"/>" size="10">
      -
      <input type="text" name="maxPrice" value="<c:out value="${frontProductSearch.maxPrice}"/>" size="10">
    </div>
    <div style="clear:both;"></div>
  </div>
  </c:if>
  
  <div class="submitSearch" align="right"><input type="submit" value="<fmt:message key="search" />" class="f_button" /></div>
</div>