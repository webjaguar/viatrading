<%@ page import="java.util.*" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>

<c:if test="${siteConfig['PRODUCT_QUICK_VIEW'].value == 'true' and ( model.thisCategory.displayMode == '13' or siteConfig['CATEGORY_DISPLAY_MODE'].value == '13')}">
<link rel="stylesheet" href="${_contextpath}/assets/SmoothGallery/QuickView/multiboxQV.css" type="text/css" media="screen" />
<script type="text/javascript">
window.addEvent('domready', function(){
	$$('div.thumbnail_image_link').each(function(imgDiv){
		//containers
		imgDiv.addEvent('mouseenter',function() {	
			var quickView = imgDiv.getFirst('a.quickViewsh');
			quickView.setStyle('display','block').fade('in');
		});

		imgDiv.addEvent('mouseleave',function() {
			var quickView = imgDiv.getFirst('a.quickViewsh');
			quickView.setStyle('display','block').fade('out');
		});
	});
	quickViewBox = new multiBox('quickViewsh',  {showNumbers:false, showControls:false, _onOpen: function() {if (typeof myVerticalSlide != "undefined"){myVerticalSlide.slideOut();}}, overlay: new overlay({opacity:'0.3'})});
});	
</script>
</c:if>
<c:if test="${siteConfig['PRODUCT_QUICK_VIEW'].value == 'true' and ( model.thisCategory.displayMode == '14' or siteConfig['CATEGORY_DISPLAY_MODE'].value == '14')}">
<link rel="stylesheet" href="${_contextpath}/assets/SmoothGallery/QuickView/multiboxQV.css" type="text/css" media="screen" />
<script type="text/javascript">
	window.addEvent('domready', function(){	
	$$('img.qvImg').each(function(image){
		image.addEvent('mouseenter',function() {	
			image.setProperty('src','${_contextpath}/assets/Image/Layout/quick_view_active.gif');
		});
		image.addEvent('mouseleave',function() {	
			image.setProperty('src','${_contextpath}/assets/Image/Layout/quick_view_inactive.gif');
		});
	});
	quickViewBox = new multiBox('quickViewsh14',  {showNumbers:false, showControls:false, _onOpen: function() {if (typeof myVerticalSlide != "undefined"){myVerticalSlide.slideOut();}}, overlay: new overlay({opacity:'0.3'})});
});
</script>
</c:if>
<c:if test="${siteConfig['PRODUCT_QUICK_VIEW'].value == 'true' and ( model.thisCategory.displayMode == '15' or siteConfig['CATEGORY_DISPLAY_MODE'].value == '15')}">
<link rel="stylesheet" href="${_contextpath}/assets/SmoothGallery/QuickView/multiboxQV.css" type="text/css" media="screen" />
<script type="text/javascript">
	window.addEvent('domready', function(){	
	$$('img.qvImg').each(function(image){
		image.addEvent('mouseenter',function() {	
			image.setProperty('src','${_contextpath}/assets/Image/Layout/quick_view_active.gif');
		});
		image.addEvent('mouseleave',function() {	
			image.setProperty('src','${_contextpath}/assets/Image/Layout/quick_view_inactive.gif');
		});
	});
	quickViewBox = new multiBox('quickViewsh15',  {showNumbers:false, showControls:false, _onOpen: function() {if (typeof myVerticalSlide != "undefined"){myVerticalSlide.slideOut();}}, overlay: new overlay({opacity:'0.3'})});
});
</script>
</c:if>
<c:if test="${siteConfig['PRODUCT_QUICK_VIEW'].value == 'true' and ( model.thisCategory.displayMode == '16' or siteConfig['CATEGORY_DISPLAY_MODE'].value == '16')}">
<link rel="stylesheet" href="${_contextpath}/assets/SmoothGallery/QuickView/multiboxQV.css" type="text/css" media="screen" />
<script type="text/javascript">
window.addEvent('domready', function(){
	$$('div.thumbnail_image_link').each(function(imgDiv){
		//containers
		imgDiv.addEvent('mouseenter',function() {	
			var quickView = imgDiv.getFirst('a.quickViewsh16');
			quickView.setStyle('display','block').fade('in');
		});

		imgDiv.addEvent('mouseleave',function() {
			var quickView = imgDiv.getFirst('a.quickViewsh16');
			quickView.setStyle('display','block').fade('out');
		});
	});
	quickViewBox = new multiBox('quickViewsh16',  {showNumbers:false, showControls:false, _onOpen: function(){if (typeof myVerticalSlide != "undefined") {myVerticalSlide.slideOut();}}, overlay: new overlay({opacity:'0.3'})});
});	
</script>
</c:if>
<c:if test="${siteConfig['PRODUCT_QUICK_VIEW'].value == 'true' and ( model.thisCategory.displayMode == '18' or siteConfig['CATEGORY_DISPLAY_MODE'].value == '18')}">
<link rel="stylesheet" href="${_contextpath}/assets/SmoothGallery/QuickView/multiboxQV.css" type="text/css" media="screen" />
<script type="text/javascript">
window.addEvent('domready', function(){
	$$('div.thumbnail_image_link').each(function(imgDiv){
		//containers
		imgDiv.addEvent('mouseenter',function() {	
			var quickView = imgDiv.getFirst('a.quickViewsh');
			quickView.setStyle('display','block').fade('in');
		});

		imgDiv.addEvent('mouseleave',function() {
			var quickView = imgDiv.getFirst('a.quickViewsh');
			quickView.setStyle('display','block').fade('out');
		});
	});
	quickViewBox = new multiBox('quickViewsh',  {showNumbers:false, showControls:false, _onOpen: function(){if (typeof myVerticalSlide != "undefined") {myVerticalSlide.slideOut();}}, overlay: new overlay({opacity:'0.3'})});
});	
</script>
</c:if>