<%@ page import="java.util.*" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<c:if test="${!empty model.products}">
<c:import url="/WEB-INF/jsp/frontend/layout/${_template}/quickViewJavascript.jsp"/>

<script type="text/javascript">
<c:if test="${siteConfig['MINI_CART'].value == 'true'}">
function showCart() {
	callCart(800);  
}
</c:if>
function checkNumber( aNumber )
{
	var goodChars = "0123456789";
	var i = 0;
	if ( aNumber == "" )
		return 0; //empty
	
	for ( i = 0 ; i <= aNumber.length - 1 ; i++ )
	{
		if ( goodChars.indexOf( aNumber.charAt(i) ) == -1 ) 
			return -1; //invalid number			
	}
	return 1; //valid number
}
function checkForm()
{
	var productList = document.getElementsByName('product.id');
	var allQtyEmpty = true;
	for(var i=0; i<productList.length; i++) 
	{
	    var el = document.getElementById("quantity_" + productList[i].value);
		if ( el != null )
		{
			if ( checkNumber( el.value ) == 1  )
			{
				allQtyEmpty = false;
			}
			else if ( checkNumber( el.value ) == -1 ) 
			{
				alert("invalid Quantity");
				el.focus();
				return false;
			}			
		}	
	}
	if ( allQtyEmpty )
	{
		alert("Please Enter Quantity.");
		return false;
	}
	else
		return true;	
}
</script>
<c:set var="pageShowing">
<form class="formPageNavigation" action="${_contextpath}/<c:choose><c:when test="${gSiteConfig['gMOD_REWRITE'] == '1'}">${siteConfig['MOD_REWRITE_CATEGORY'].value}/${model.thisCategory.id}/${model.thisCategory.encodedName}.html</c:when><c:otherwise>category.jhtm</c:otherwise></c:choose>">
<table class="pagenavBox" border="0" cellpadding="0" cellspacing="0" width="100%">
  <tr>
    <td class="pageShowing">
    <c:if test="${model.count > 0}">
    <fmt:message key="showing">
	  <fmt:param value="${frontEndProductSearch.offset + 1}"/>
	  <fmt:param value="${model.pageEnd}"/>
	  <fmt:param value="${model.count}"/>
    </fmt:message>
    </c:if> 
    </td>
    <td class="pageNavi">
    <c:if test="${gSiteConfig['gMOD_REWRITE'] != '1'}">
    <input type="hidden" name="cid" value="${model.thisCategory.id}">
    </c:if>
    Page 
    <c:choose>
      <c:when test="${model.pageCount <= 100 and model.pageCount > 0}">
        <select name="page" id="page" onchange="submit()">
        <c:forEach begin="1" end="${model.pageCount}" var="page">
  	      <option value="${page}" <c:if test="${page == (frontEndProductSearch.page)}">selected</c:if>>${page}</option>
        </c:forEach>
       </select>
      </c:when>
      <c:otherwise>
        <input type="text" id="page" name="page" value="${frontEndProductSearch.page}" size="5" class="textfield50" />
        <input type="submit" value="go"/>
      </c:otherwise>
    </c:choose>
    of <c:out value="${model.pageCount}"/>
    | 
    <c:if test="${frontEndProductSearch.page == 1}"><span class="pageNaviDead"><fmt:message key="f_previous" /></span></c:if>
    <c:if test="${frontEndProductSearch.page != 1}"><a href="${_contextpath}/<c:choose><c:when test="${gSiteConfig['gMOD_REWRITE'] == '1'}">${siteConfig['MOD_REWRITE_CATEGORY'].value}/${model.thisCategory.id}/${model.thisCategory.encodedName}.html?page=${frontEndProductSearch.page-1}&sortBy=${model.sortBy}</c:when><c:otherwise><c:url value="category.jhtm"><c:param name="page" value="${frontEndProductSearch.page-1}"/><c:param name="cid" value="${model.thisCategory.id}"/><c:param name="sortBy" value="${model.sortBy}"/></c:url></c:otherwise></c:choose>" class="pageNaviLink"><fmt:message key="f_previous" /></a></c:if>
    | 
    <c:if test="${frontEndProductSearch.page == model.pageCount}"><span class="pageNaviDead"><fmt:message key="f_next" /></span></c:if>
    <c:if test="${frontEndProductSearch.page != model.pageCount}"><a href="${_contextpath}/<c:choose><c:when test="${gSiteConfig['gMOD_REWRITE'] == '1'}">${siteConfig['MOD_REWRITE_CATEGORY'].value}/${model.thisCategory.id}/${model.thisCategory.encodedName}.html?page=${frontEndProductSearch.page+1}&sortBy=${model.sortBy}</c:when><c:otherwise><c:url value="category.jhtm"><c:param name="page" value="${frontEndProductSearch.page+1}"/><c:param name="cid" value="${model.thisCategory.id}"/><c:param name="sortBy" value="${model.sortBy}"/></c:url></c:otherwise></c:choose>" class="pageNaviLink"><fmt:message key="f_next" /></a></c:if>
    </td>
  </tr>
</table>
<c:if test="${siteConfig['PRODUCT_SORT_BY_FRONTEND'].value == 'true'}">
<table id="sort_box" border="0" cellpadding="0" cellspacing="0" width="100%">
<tr>
  <td id="sortby"><span><fmt:message key="f_sortby"/></span>
    <select id="" name="sort" onchange="submit()">
      <option value="0">${model.sortBy}</option>
      <option value="10" <c:if test="${frontEndProductSearch.sort == 'name'}">selected="selected"</c:if>><fmt:message key="Name"/></option>
      <option value="20" <c:if test="${frontEndProductSearch.sort == 'price_1 ASC'}">selected="selected"</c:if>><fmt:message key="price"/> (<fmt:message key="${siteConfig['CURRENCY'].value}" /> <fmt:message key="to"/> <fmt:message key="${siteConfig['CURRENCY'].value}" /><fmt:message key="${siteConfig['CURRENCY'].value}" />)</option>
      <option value="30" <c:if test="${frontEndProductSearch.sort == 'price_1 DESC'}">selected="selected"</c:if>><fmt:message key="price"/> (<fmt:message key="${siteConfig['CURRENCY'].value}" /><fmt:message key="${siteConfig['CURRENCY'].value}" /> <fmt:message key="to"/> <fmt:message key="${siteConfig['CURRENCY'].value}" />)</option>
      <c:if test="${siteConfig['PRODUCT_RATE'].value =='true' or gSiteConfig['gPRODUCT_REVIEW']}">
        <option value="40" <c:if test="${frontEndProductSearch.sort == 'rate DESC'}">selected="selected"</c:if>>Rating</option>
      </c:if>  
    </select>
  </td>
  <td id="pagesize"><span>PAGE SIZE</span>
  <select name="size" onchange="document.getElementById('page').value=1;submit()">
    <c:forEach items="${model.productPerPageList}"  var="current">
	  <option value="${current}" <c:if test= "${current == frontEndProductSearch.pageSize}">selected</c:if>>${current} <fmt:message key="perPage" /></option>
	</c:forEach>
  </select>
  </td>
</tr>
</table>
</c:if>
</form>
</c:set>
<c:out value="${pageShowing}" escapeXml="false" />
</c:if>
<c:set var="numCul" value="${siteConfig['NUMBER_PRODUCT_PER_ROW'].value}" />
<c:if test="${!empty model.products}">
<table class="thumbnail_item_header" border="0" cellpadding="0" cellspacing="0" width="100%">
  <tr><td class="header"></td></tr>
</table>
</c:if>
<c:forEach items="${model.products}" var="product" varStatus="status">  

<c:set var="productLink">${_contextpath}/product.jhtm?id=${product.id}<c:if test="${model.product == null or siteConfig['RECOMMENDED_LIST_SHOW_BREADCRUMB'].value == 'true'}">&cid=<c:out value="${param.cid}"/></c:if></c:set>
<c:if test="${gSiteConfig['gMOD_REWRITE'] == '1' and not fn:contains(product.sku, '/')}">
  <c:set var="productLink">${_contextpath}/${siteConfig['MOD_REWRITE_PRODUCT'].value}/${product.encodedSku}/${product.encodedName}.html<c:if test="${model.product == null or siteConfig['RECOMMENDED_LIST_SHOW_BREADCRUMB'].value == 'true'}">?cid=<c:out value="${param.cid}"/></c:if></c:set>
</c:if>	
  <c:choose>
      <c:when test="${ model.showAddPresentation }">
    	<%@ include file="/WEB-INF/jsp/frontend/thumbnail/presentation/view1.jsp" %>
  	  </c:when>
      <c:when test="${(model.thisCategory.displayMode == 'quick' ) or (empty model.thisCategory.displayMode and siteConfig['CATEGORY_DISPLAY_MODE'].value == 'quick')}">
        <%@ include file="/WEB-INF/jsp/frontend/quickmode/view1.jsp" %>
      </c:when>
      <c:when test="${(model.thisCategory.displayMode == 'quick3' ) or (empty model.thisCategory.displayMode and siteConfig['CATEGORY_DISPLAY_MODE'].value == 'quick')}">
        <%@ include file="/WEB-INF/jsp/frontend/quickmode/view3.jsp" %>
      </c:when>
	  <c:when test="${model.thisCategory.displayMode == '2' or (empty model.thisCategory.displayMode and siteConfig['CATEGORY_DISPLAY_MODE'].value == '2')}">
	    <%@ include file="/WEB-INF/jsp/frontend/thumbnail/view2.jsp" %>
      </c:when>
	  <c:when test="${model.thisCategory.displayMode == '3' or (empty model.thisCategory.displayMode and siteConfig['CATEGORY_DISPLAY_MODE'].value == '3')}">
		<%@ include file="/WEB-INF/jsp/frontend/thumbnail/view3.jsp" %>
	  </c:when>
	  <c:when test="${model.thisCategory.displayMode == '4' or (empty model.thisCategory.displayMode and siteConfig['CATEGORY_DISPLAY_MODE'].value == '4')}">
		<%@ include file="/WEB-INF/jsp/frontend/thumbnail/view4.jsp" %>
	  </c:when>
	  <c:when test="${model.thisCategory.displayMode == '5' or (empty model.thisCategory.displayMode and siteConfig['CATEGORY_DISPLAY_MODE'].value == '5')}">
		<%@ include file="/WEB-INF/jsp/frontend/thumbnail/view5.jsp" %>
	  </c:when>
	  <c:when test="${model.thisCategory.displayMode == '6' or (empty model.thisCategory.displayMode and siteConfig['CATEGORY_DISPLAY_MODE'].value == '6')}">
		<%@ include file="/WEB-INF/jsp/frontend/thumbnail/view6.jsp" %>
	  </c:when>
	  <c:when test="${model.thisCategory.displayMode == '7' or (empty model.thisCategory.displayMode and siteConfig['CATEGORY_DISPLAY_MODE'].value == '7')}">
		<%@ include file="/WEB-INF/jsp/frontend/thumbnail/view7.jsp" %>
	  </c:when>
	  <c:when test="${model.thisCategory.displayMode == '8' or (empty model.thisCategory.displayMode and siteConfig['CATEGORY_DISPLAY_MODE'].value == '8')}">
		<%@ include file="/WEB-INF/jsp/frontend/thumbnail/view8.jsp" %>
	  </c:when>
	  <c:when test="${model.thisCategory.displayMode == '9' or (empty model.thisCategory.displayMode and siteConfig['CATEGORY_DISPLAY_MODE'].value == '9')}">
		<%@ include file="/WEB-INF/jsp/frontend/thumbnail/view9.jsp" %>
	  </c:when>
	  <c:when test="${(model.thisCategory.displayMode == '10' ) or (empty model.thisCategory.displayMode and siteConfig['CATEGORY_DISPLAY_MODE'].value == '10')}">
        <%@ include file="/WEB-INF/jsp/frontend/thumbnail/view10.jsp" %>
      </c:when>
      <c:when test="${(model.thisCategory.displayMode == '11' ) or (empty model.thisCategory.displayMode and siteConfig['CATEGORY_DISPLAY_MODE'].value == '11')}">
        <%@ include file="/WEB-INF/jsp/frontend/thumbnail/view11.jsp" %>
      </c:when>
      <c:when test="${(model.thisCategory.displayMode == '12' ) or (empty model.thisCategory.displayMode and siteConfig['CATEGORY_DISPLAY_MODE'].value == '12')}">
        <%@ include file="/WEB-INF/jsp/frontend/thumbnail/view12.jsp" %>
      </c:when>
      <c:when test="${(model.thisCategory.displayMode == '13' ) or (empty model.thisCategory.displayMode and siteConfig['CATEGORY_DISPLAY_MODE'].value == '13')}">
        <%@ include file="/WEB-INF/jsp/frontend/thumbnail/view13.jsp" %>
      </c:when>
	  <c:when test="${(model.thisCategory.displayMode == '14' ) or (empty model.thisCategory.displayMode and siteConfig['CATEGORY_DISPLAY_MODE'].value == '14')}">
        <%@ include file="/WEB-INF/jsp/frontend/thumbnail/view14.jsp" %>
      </c:when>
      <c:when test="${(model.thisCategory.displayMode == '15' ) or (empty model.thisCategory.displayMode and siteConfig['CATEGORY_DISPLAY_MODE'].value == '15')}">
        <%@ include file="/WEB-INF/jsp/frontend/viatrading/thumbnail/view1.jsp" %>
      </c:when>
      <c:when test="${(model.thisCategory.displayMode == '16' ) or (empty model.thisCategory.displayMode and siteConfig['CATEGORY_DISPLAY_MODE'].value == '16')}">
        <%@ include file="/WEB-INF/jsp/frontend/thumbnail/view16.jsp" %>
      </c:when>
      <c:when test="${(model.thisCategory.displayMode == '17' ) or (empty model.thisCategory.displayMode and siteConfig['CATEGORY_DISPLAY_MODE'].value == '17')}">
        <%@ include file="/WEB-INF/jsp/frontend/thumbnail/view17.jsp" %>
      </c:when>
      <c:when test="${(model.thisCategory.displayMode == '18' ) or (empty model.thisCategory.displayMode and siteConfig['CATEGORY_DISPLAY_MODE'].value == '18')}">
        <%@ include file="/WEB-INF/jsp/frontend/thumbnail/view18.jsp" %>
      </c:when>
	  <c:otherwise>
	    <%@ include file="/WEB-INF/jsp/frontend/thumbnail/view1.jsp" %>
      </c:otherwise>
  </c:choose>
</c:forEach>

<c:if test="${!empty model.products && model.pageCount > 1}">
<c:out value="${pageShowing}" escapeXml="false" />
</c:if>