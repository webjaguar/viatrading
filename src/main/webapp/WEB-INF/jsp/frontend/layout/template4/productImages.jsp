<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<head>
<c:if test="${model.product.imageCount > 1}" >
<script language="JavaScript">
<!-- 
current = 0;
function next() 
{
  if (document.slideform.slide[current+1]) 
  {
    document.images.show.src = document.slideform.slide[current+1].value;
    document.slideform.slide.selectedIndex = ++current;
  }
}
function previous() 
{
  if (current-1 >= 0) 
  {
    document.images.show.src = document.slideform.slide[current-1].value;
    document.slideform.slide.selectedIndex = --current;
  }
}
//-->
</script>
</c:if>
</head>

<html>
<body>
<form name="slideform">
<table cellspacing="1" cellpadding="4" bgcolor="#000000">
<c:if test="${model.product.imageCount > 1}" >
<tr align="center">
  <td bgcolor="#C0C0C0">
  <input type="button" onClick="previous();" value="Previous" title="Previous">
  <input type="button" onClick="next();" value="Next" title="Next">
  </td>
</tr>
</c:if>
<tr>
<td>
  <img src="<c:out value="${model.imgUrl}"/>" name="show" />
</td>
</tr>
<tr>
<c:forEach items="${model.product.images}" var="image" varStatus="status">
<c:choose>
  <c:when test="${image.absolute}">
	<input type="hidden" name="slide" value="<c:out value="${image.imageUrl}"/>" />
  </c:when>
  <c:otherwise>
	<input type="hidden" name="slide" value="assets/Image/Product/detailsbig/<c:out value="${image.imageUrl}"/>" />
  </c:otherwise>
</c:choose>
</c:forEach>
</tr>
</table>
</form>
</body>
</html>