<%@ page import="com.webjaguar.model.*, java.text.NumberFormat, java.util.*, java.text.*,java.lang.reflect.Method" %>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<tiles:importAttribute scope="request" />
<c:out value="${siteConfig['DOCTYPE'].value}" escapeXml="false"/>
<html>
<head>
<c:set var="gCUSTOMER_FIELDS" value="${gSiteConfig['gCUSTOMER_FIELDS']}"/>
<% 
 NumberFormat nf = NumberFormat.getInstance( );
 nf.setMaximumFractionDigits( 2 );
 nf.setMinimumFractionDigits( 2 );
 Layout layout = (Layout) request.getAttribute("layout");
 Cart sessionCart = (Cart) request.getSession().getAttribute( "sessionCart" );
 Cart userCart = (Cart) request.getSession().getAttribute( "userCart" );
 UserSession userSession = (UserSession) request.getAttribute( "userSession" );
 Customer sessionCustomer = (Customer) request.getAttribute("sessionCustomer");
 SalesRep sessionRelesRep = (SalesRep) request.getAttribute("sessionSalesRep");
 Map<String, Configuration> siteConfig = (Map<String, Configuration>) request.getAttribute( "siteConfig" );
 if (userSession == null ) {
 	if (request.getSession().getAttribute( "sessionCart" ) == null ) {
 		layout.replace("#cartItems#", "0");
 		layout.replace("#cartSubTotal#", "0.00");
 	} else {
 		layout.replace("#cartItems#", "" + sessionCart.getNumberOfItems());
 		layout.replace("#cartSubTotal#", "" + nf.format(sessionCart.getSubTotal()) );
 	}
	layout.replace("#firstname#", "");
	layout.replace("#lastname#", "");	
	layout.replace("#email#", "");
 } else {
 	if (request.getSession().getAttribute( "userCart" ) == null ) {
 		layout.replace("#cartItems#", "0");
 		layout.replace("#cartSubTotal#", "0.00");
 	} else {
 		layout.replace("#cartItems#", "" + userCart.getNumberOfItems());
 		layout.replace("#cartSubTotal#", "" + nf.format(userCart.getSubTotal()) );
 	}
 	layout.replace("#firstname#", userSession.getFirstName());
	layout.replace("#lastname#", userSession.getLastName());
	layout.replace("#email#", userSession.getUsername());
	layout.replace("#accountnumber#", sessionCustomer.getAccountNumber());
	layout.replace("#company#", sessionCustomer.getAddress().getCompany());
	layout.replace("#addr1#", sessionCustomer.getAddress().getAddr1());
	layout.replace("#addr2#", sessionCustomer.getAddress().getAddr1());
	layout.replace("#city#", sessionCustomer.getAddress().getCity());
	layout.replace("#state#", sessionCustomer.getAddress().getStateProvince());
	layout.replace("#zip#", sessionCustomer.getAddress().getZip());
	layout.replace("#country#", sessionCustomer.getAddress().getCountry());
	layout.replace("#phone#", sessionCustomer.getAddress().getPhone());
	layout.replace("#fax#", sessionCustomer.getAddress().getFax());
	layout.replace("#cellphone#", sessionCustomer.getAddress().getCellPhone());
	layout.replace("#loginSuccessPage#", (sessionCustomer.getLoginSuccessURL()==null||sessionCustomer.getLoginSuccessURL().equals(""))? siteConfig.get("LOGIN_SUCCESS_URL").getValue():sessionCustomer.getLoginSuccessURL() );
	int gCUSTOMER_FIELDS = (Integer) pageContext.getAttribute("gCUSTOMER_FIELDS");
	if (gCUSTOMER_FIELDS > 0) {
		// customer fields
		Class<Customer> c = Customer.class;
		Method m = null;
		Object arglist[] = null;
		for (int i=1; i<=gCUSTOMER_FIELDS; i++) {
			try {
				m = c.getMethod("getField" + i);
				layout.replace("#customerfield"+i+"#", (String) m.invoke(sessionCustomer, arglist));
			} catch (Exception e)  {
				// do nothing
			}
		}		
	}
 }
 if (sessionRelesRep != null) {
	 layout.replace("#salesRepName#", sessionRelesRep.getName());
 }
 SimpleDateFormat df = new SimpleDateFormat("EEEE MM/dd/yyyy");
 layout.replace("#date#", df.format(new Date()));
 layout.replace("#trackcode#", (String) request.getAttribute("trackcode"));
 layout.replace("#id#", request.getParameter("id") != null ? request.getParameter("id") : "");
%>
<c:if test="${siteConfig['MOOTOOLS'].value == 'true'}">
<script src="${_contextpath}/javascript/mootools-1.2.5-core.js" type="text/javascript"></script>
<script src="${_contextpath}/javascript/mootools-1.2.5.1-more.js" type="text/javascript"></script>
 <script src="${_contextpath}/javascript/overlay.js" type="text/javascript" ></script>
 <script src="${_contextpath}/javascript/multibox.js" type="text/javascript" ></script>
</c:if>
<c:out value="${layout.headTag}" escapeXml="false"/>
<c:if test="${fn:contains(gSiteConfig['gDATA_FEED'],'blueCherry')}">
<script language="JavaScript" src="https://secure1.offerpoint.net/inchannel/maq.js"></script>
</c:if>
<c:if test="${model.canonicalLink != null}">
<link rel="canonical" href="<c:out value="${model.canonicalLink}" />"/>
</c:if>
</head>  
<body>
<c:set var="dynamicMenu" value="${dynamicMenu}"/>
<c:if test="${dynamicMenu != null}">
<c:set value="${gSiteConfig['gMOD_REWRITE']}" var="gMOD_REWRITE"/>
<c:set value="${siteConfig['MOD_REWRITE_CATEGORY'].value}" var="MOD_REWRITE_CATEGORY"/>
<c:set value="${siteConfig['ABOUT_US_ID'].value}" var="ABOUT_US_ID"/>
<c:set value="${siteConfig['CONTACT_US_ID'].value}" var="CONTACT_US_ID"/>
<div style="display:none">
<%
for (Category dynamicCategory: (List<Category>) pageContext.getAttribute("dynamicMenu")) {
	dynamicMenu(dynamicCategory, 1, out, (String) request.getAttribute("_contextpath"), (String) pageContext.getAttribute("gMOD_REWRITE"), (String) pageContext.getAttribute("MOD_REWRITE_CATEGORY")
			, (String) pageContext.getAttribute("ABOUT_US_ID"), (String) pageContext.getAttribute("CONTACT_US_ID"));
}
%>
</div>
</c:if>

<div id="pageWrapper">
    <c:if test="${layout.headerHtml != ''}">
	<div id="pageHeaderWrapper">
		<div id="pageHeader">
			<c:out value="${layout.headerHtml}" escapeXml="false"/>
			<div class="clearboth">&nbsp;</div><!-- stop floating (if any) -->
		</div><!-- end pageHeader -->
	</div><!-- end pageHeaderWrapper -->
	</c:if>
	<c:if test="${layout.topBarHtml != ''}">
	<div id="pageTopbarWrapper">
		<div id="pageTopbar">
			<c:out value="${layout.topBarHtml}" escapeXml="false"/>
			<div class="clearboth">&nbsp;</div><!-- stop floating (if any) -->
		</div><!-- end pageTopbar -->
	</div><!-- end pageTopbarWrapper -->
	</c:if>
	<div id="pageBodyWrapper">
		<div id="pageBody" class="bothSidebars">
		    <c:if test="${not layout.hideLeftBar}">
			<div id="leftSidebarWrapper">
				<div id="leftSidebar">
					<tiles:insertAttribute name="leftbar" />
				</div><!-- end leftSidebar -->
			</div><!-- end leftSidebarWrapper -->
			</c:if>
			<div id="contentWrapper" class="bothSidebars">
				<div id="content">
					<tiles:insertAttribute name="content" />
					<div class="clearboth">&nbsp;</div><!-- stop floating (if any) -->
				</div><!-- end content -->
			</div><!-- end contentWrapperThreeCol -->
			<tiles:insertAttribute name="rightbar" />
			<div class="clearboth" id="clearboth">&nbsp;</div><!-- stop floating (if any) -->
		</div><!-- end pageBody -->
	</div><!-- end pageBodyWrapper -->
	<c:if test="${layout.footerHtml != ''}">
	<div id="pageFooterWrapper">
		<div id="pageFooter">
			<c:out value="${layout.footerHtml}" escapeXml="false"/>
			<div class="clearboth">&nbsp;</div><!-- stop floating (if any) -->
		</div><!-- end pageFooter -->
	</div><!-- end pageFooterWrapper -->
	</c:if>
	<c:if test="${layout.aemFooter != ''}">
	<div id="pageSubFooterWrapper">
		<div id="pageSubFooter">
			<c:out value="${layout.aemFooter}" escapeXml="false"/>
			<div class="clearboth">&nbsp;</div><!-- stop floating (if any) -->
		</div><!-- end pageSubFooter -->
	</div><!-- end pageSubFooterWrapper -->
	</c:if>
</div><!-- end pageWrapper -->
</body>
</html>

<%!
void dynamicMenu(Category category, int level, JspWriter out, String _contextpath, String gMOD_REWRITE, String MOD_REWRITE_CATEGORY
		,String ABOUT_US_ID,String CONTACT_US_ID) throws Exception {	
	for (int i=0; i<level; i++) out.print(" ");
	out.print("<ul class=\"level-" + level + "\"");
	if (level == 1) out.print(" id=\"ul-cat_" + category.getId() + "\"");
	out.print(">\n");
	for (Category thisCategory: category.getSubCategories()) {
		for (int i=0; i<level; i++) out.print(" ");
		out.print(" <li><span>");
		if (!thisCategory.getLinkType().equals(CategoryLinkType.NONLINK)) {
			out.print("<a href=\"" + _contextpath + "/");
			if (!ABOUT_US_ID.isEmpty() && thisCategory.getId().toString().equals(ABOUT_US_ID)) {
				out.print("aboutus.jhtm");
			} else if (!CONTACT_US_ID.isEmpty() && thisCategory.getId().toString().equals(CONTACT_US_ID)) {
				out.print("contactus.jhtm");	
			} else if (gMOD_REWRITE != null && gMOD_REWRITE.equals("1")) {
				out.print(MOD_REWRITE_CATEGORY + "/" + thisCategory.getId() + "/" + thisCategory.getEncodedName() + ".html");				
			} else {
				out.print("category.jhtm?cid=" + thisCategory.getId());
			}
			out.print("\"");
			if (thisCategory.getUrlTarget() != null && thisCategory.getUrlTarget().equals("_blank")) {
				out.print(" target=\"_blank\"");
			}
			out.print(">");
		}
		out.print(thisCategory.getName());
		if (!thisCategory.getLinkType().equals(CategoryLinkType.NONLINK)) {
			out.print("</a>");	
		}
		out.print("</span>");
		if (thisCategory.getSubCategories().size() > 0) {
			out.print("\n");
			dynamicMenu(thisCategory, level+1, out, _contextpath, gMOD_REWRITE, MOD_REWRITE_CATEGORY, ABOUT_US_ID, CONTACT_US_ID);
			out.print(" ");
			for (int i=0; i<level; i++) out.print(" ");
		}
		out.println("</li>");
   	}
	for (int i=0; i<level; i++) out.print(" ");
	out.println("</ul>");
}
%>