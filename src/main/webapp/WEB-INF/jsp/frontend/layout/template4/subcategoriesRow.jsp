<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<c:set var="numCul" value="${model.thisCategory.subcatCols}" />
<c:if test="${model.thisCategory.subcatCols == 0}">
<c:set var="numCul" value="1" />
</c:if>
<c:set var="index" value="0" />
<c:forEach items="${model.subCategories}" var="category" varStatus="status">
<c:set var="index" value="${index+1}" />
<c:if test="${status.first}" >
<table class="subCategories" >
</c:if>
  <c:if test="${ index == 1 }">
    <tr>
  </c:if>  
  <td valign="top">

   <c:if test="${category.linkType != 'NONLINK'}">      
	  <a href="${_contextpath}/<c:choose><c:when test="${gSiteConfig['gMOD_REWRITE'] == '1'}">${siteConfig['MOD_REWRITE_CATEGORY'].value}/${category.id}/${category.encodedName}.html</c:when><c:otherwise>category.jhtm?cid=${category.id}</c:otherwise></c:choose>" <c:if test="${category.urlTarget == '_blank'}">target="_blank"</c:if> class="subCatLink">
   </c:if>
   <c:choose>
	    <c:when test="${ not empty (category.imageUrl) and category.imageUrl != null}">
	    	<c:choose>
	    	<c:when test="${category.absolute}">
	    		<img src="<c:url value="${category.imageUrl}"/>" border="0" alt="${category.imageUrlAltName}">
	    	</c:when>
	    	<c:otherwise>
	    		<img src="<c:url value="${_contextpath}/assets/Image/Category/${category.imageUrl}"/>" border="0" alt="${category.imageUrlAltName}">
	    	</c:otherwise>
	    	</c:choose>
	    </c:when>
	    <c:when test="${category.hasImage}">
		  	<img src="<c:url value="${_contextpath}/assets/Image/Category/cat_${category.id}.gif"/>" border="0" alt="${category.imageUrlAltName}">
		</c:when>
   </c:choose>
   <div class="catName" align="center"><c:out value="${category.name}" escapeXml="false" />	
   <c:if test="${(category.productCount > 0) and (siteConfig['SUBCAT_PRODUCT_COUNT'].value == 'true')}">(<c:out value="${category.productCount}" />)</c:if></div><br />
   <c:if test="${category.linkType != 'NONLINK'}">      
	  </a> 
   </c:if>		
   <c:forEach items="${category.subCategories}" var="subCat" varStatus="subCatStatus">
     <c:if test="${subCatStatus.index < siteConfig['SUBCAT_MAX'].value}">
  	 <c:choose>
  	   <c:when test="${subCat.linkType != 'NONLINK'}"><a href="${_contextpath}/<c:choose><c:when test="${gSiteConfig['gMOD_REWRITE'] == '1'}">${siteConfig['MOD_REWRITE_CATEGORY'].value}/${subCat.id}/${subCat.encodedName}.html</c:when><c:otherwise>category.jhtm?cid=${subCat.id}</c:otherwise></c:choose>" <c:if test="${subCat.urlTarget == '_blank'}">target="_blank"</c:if> class="subSubCatLink<c:if test="${subCat.hasSubcats}">Plus</c:if>"></c:when>
  	   <c:otherwise><img class="subSubCatLink<c:if test="${subCat.hasSubcats}">Plus</c:if>"></c:otherwise>
  	 </c:choose>

  	 <c:out value="${subCat.name}" escapeXml="false" />
	 <c:if test="${subCat.productCount > 0}">(<c:out value="${subCat.productCount}" />)</c:if>
	<c:if test="${subCat.linkType != 'NONLINK'}"> 
	  </a>
	</c:if>	  
	<br>
	</c:if>
	<c:if test="${subCatStatus.index == siteConfig['SUBCAT_MAX'].value}">
	  <a href="${_contextpath}/<c:choose><c:when test="${gSiteConfig['gMOD_REWRITE'] == '1'}">category/${category.id}/${category.encodedName}.html</c:when><c:otherwise>category.jhtm?cid=${category.id}</c:otherwise></c:choose>" class="subSubCatLink">More...</a><br>
    </c:if>
	<c:if test="${subCatStatus.last}"><br></c:if>	
   </c:forEach> 
  </td>
  <c:if test="${index % numCul == 0 }">
    </tr>
    <c:set var="index" value="0" />
  </c:if>
<c:if test="${status.last}">
 </table>
</c:if>
</c:forEach>


