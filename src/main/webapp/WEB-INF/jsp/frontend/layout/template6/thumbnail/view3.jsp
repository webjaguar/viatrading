<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ taglib uri="/WEB-INF/tlds/custom.tld" prefix="wj" %>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>

<script type="text/javascript">
	$(function(){
		$('.scroll-pane').jScrollPane({
			showArrows: true,
			verticalDragMinHeight: 30,
			verticalDragMaxHeight: 30,
			horizontalDragMinWidth: 30,
			horizontalDragMaxWidth: 30
		});
	});
	
	function changeme(fieldId){
		document.getElementById('sort').value=fieldId;
		document.getElementById('sorttype').value=document.getElementById('state').value;
		document.forms["sortFieldForm"].submit();
	}
	
	function resetButton(){
		document.getElementById('reset').value='reset';
	}
	
	function nextButton(fieldId){
		if(typeof fieldId=="undefined"){
			fieldId = 1;
		}
		document.getElementById('page').value=fieldId+1;
		document.forms["sortFieldForm"].submit();
	}
	
	function previousButton(fieldId){
		if(typeof fieldId=="undefined"){
			fieldId = 2;
	    }
		document.getElementById('page').value=fieldId-1;
		document.forms["sortFieldForm"].submit();
	}
</script>

<script type="text/javascript">
$(window).load(function() {
	$(window).resize(function(){
		var w=window, d=document, e=d.documentElement, g=d.getElementsByTagName('body')[0];
		var screen_width = w.innerWidth||e.clientWidth||g.clientWidth;
		var screen_height = w.innerHeight||e.clientHeight||g.clientHeight;
		if(screen_width > 767) {
			// Enable floating cart
			$("#parentPopup .cart").unbind().click(
				function(){
					if ($("#cart-dropdown2").is(":hidden")) {
						$("#cart-dropdown2").stop().slideDown("fast", function(){
							if(!$("#cart-dropdown2").hasClass("is-loading")) {
								$("#cart-dropdown2").addClass("is-loading").showLoading();
								 $.ajax({
								 	url: "ajaxViewCart.jhtm",
								 	type: "GET",
								 	dataType: "html",
								 	success: function (data) {
										$("#cart-dropdown2").removeClass("is-loading").hideLoading();
										 $("#cart-dropdown2").html(data);
								 	},
								 	error: function() {
								 		$("#cart-dropdown2").removeClass("is-loading").hideLoading();
								 		$("#cart-dropdown2").html('&lt;div class="error-message"&gt;Error Loading Cart&lt;/div&gt;');
								 	}
								 });
							}														
						});
					}
					else {
						if(!$("#cart-dropdown2").hasClass("is-loading")) {
							$("#cart-dropdown2").stop().slideUp("fast");
						}
					}
				}
			);
			$("#cart-dropdown2").unbind().hover(
				function(){
					if ($("#cart-dropdown2").is(":hidden")) {
						$("#cart-dropdown2").stop().slideDown("fast");
					}
					else {
						$("#cart-dropdown2").stop().slideDown(0);
					}
				},
				function(){
					if(!$("#cart-dropdown2").hasClass("is-loading")) {
						$("#cart-dropdown2").stop().slideUp("fast");
					}
				}
			);
		}
		else {
			// Disable and Hide floating cart on small devices
			$("#parentPopup .cart").unbind();
			$("#cart-dropdown2").unbind().hide();
		}
	}).trigger('resize');
});
</script>

<script>

var gPosX;
var gPosY; 

function popup(event,sku,id) {

  var PosX = 0;
  var PosY = 0; 
  PosX = event.clientX;
  PosY = event.clientY;
  
  gPosX = PosX;
  gPosY = PosY;
  
  //alert(PosX); alert(PosY);
  var xmlhttp;
  if (window.XMLHttpRequest)
  {// code for IE7+, Firefox, Chrome, Opera, Safari
  	xmlhttp=new XMLHttpRequest();
  }
  else
  {// code for IE6, IE5
  	xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
  }
	
  xmlhttp.onreadystatechange=function()
  {	
    if (xmlhttp.readyState==4 && xmlhttp.status==200)
    {
    	document.getElementById("cart-dropdown2").style.position = "fixed";
        document.getElementById("cart-dropdown2").style.top = PosY+'px';
        document.getElementById("cart-dropdown2").style.left = (PosX+100)+'px';
        document.getElementById("cart-dropdown2").innerHTML = xmlhttp.responseText;
        document.getElementById("cart-dropdown2").style.display = 'block';
        document.getElementById("cart-dropdown2").style.backgroundColor = 'white';
        document.getElementById("cart-dropdown2").style.border = "solid #000000";
        document.getElementById("cart-dropdown2").style.zIndex = 1000;
    }
  }
  
  xmlhttp.open("GET","https://www.viatrading.com/ajaxParentSku.jhtm?sku="+sku,true);
  xmlhttp.send();
}

function popupClose() {
	var PosX = 0;
	var PosY = 0; 
	PosX = event.clientX;
	PosY = event.clientY;
	if(Math.abs(gPosX-PosX)>100 && Math.abs(gPosY-PosY)>100)
	document.getElementById("cart-dropdown2").style.display = 'none';
}
</script>


</head>
<body>


<div style="display: none;" id="cart-dropdown2" class="">
</div>


<!-- ========== PAGE BODY ========== -->
<div id="pageBodyWrapper">
	<div id="pageBody" class="container-fluid">
		<div class="row row-offcanvas row-offcanvas-left">
			<!-- ========== CONTENT ========== -->
			<div id="contentWrapper">
				<div id="content">
					<div class="col-sm-12">
						<div id="loadCenterWrapper">
							<div class="row">
								<div class="col-sm-12">
									<div class="narrowResultsToggle">
										<button class="btn btn-lg narrowResultsToggle_btn" type="button" data-toggle="collapse" data-target="#loadCenterFilters_table_wrapper">
											Narrow Results &nbsp;<span class="narrowResultsToggle_icon"></span>
										</button>	
									</div>
									
									
									<form id="sortFieldForm" name="sortFieldForm" class="formPageNavigation clearfix" action="${_contextpath}/<c:choose><c:when test="${gSiteConfig['gMOD_REWRITE'] == '1'}">${siteConfig['MOD_REWRITE_CATEGORY'].value}/${model.thisCategory.id}/${model.thisCategory.encodedName}.html</c:when><c:otherwise>category.jhtm?</c:otherwise></c:choose>">
									<input type="hidden" name="cid" id="cid" value="${model.cid}">
									<input type="hidden" name="sort" id="sort">
									<input type="hidden" name="sorttype" id="sorttype">
									<input type="hidden" name="state" id="state" value="${model.state}">
									<input type="hidden" name="userId" id="state" value="${model.userId}">
									<c:set value="cat_${model.thisCategory.id}" var="thisCat"/>
										  <div id="loadCenterFilters_table_wrapper" class="table-responsive collapse in">																						
										  <table id="loadCenterFilters_table" class="table">
											<thead>												
												<tr>
													<c:forEach items="${model.dropDownList}" var="dropDownList" varStatus="dropDownStatus">
															
													<c:choose>
										
													<c:when test="${fn:containsIgnoreCase(dropDownList.name, 'Packing')}">
														<th class="loadCenterFilter_th_packing">
														${dropDownList.name}
														<div class="info_tooltip">
															<i class="fa fa-info-circle" data-toggle="tooltip" data-placement="top" data-original-title="The type of packing in which the load will be packed and shipped"></i>
														</div>
													</c:when>
													
													<c:when test="${fn:containsIgnoreCase(dropDownList.name, 'Category')}">
														<th class="loadCenterFilter_th_category">
														${dropDownList.name}
														<div class="info_tooltip">
															<i class="fa fa-info-circle" data-toggle="tooltip" data-placement="top" data-original-title="The category of products found in the load. Some loads can have multiple product categories assigned to them while others may just say General Merchandise but include products from several categories"></i>
														</div>
													</c:when>
													
													<c:when test="${fn:containsIgnoreCase(dropDownList.name, 'FOB')}">
														<th class="loadCenterFilter_th_fob">
														${dropDownList.name}
														<div class="info_tooltip">
															<i class="fa fa-info-circle" data-toggle="tooltip" data-placement="top" data-original-title="Origin of the load (from where the load will be shipped to you)"></i>
														</div>
													</c:when>
													
													<c:when test="${fn:containsIgnoreCase(dropDownList.name, 'Store')}">
														<th class="loadCenterFilter_th_store">
														${dropDownList.name}
														<div class="info_tooltip">
															<i class="fa fa-info-circle" data-toggle="tooltip" data-placement="top" data-original-title="Abbreviation of the store from which the load originated (store names cannot be advertised)."></i>
														</div>
													</c:when>
													
													<c:when test="${fn:containsIgnoreCase(dropDownList.name, 'Product Condition')}">
														<th class="loadCenterFilter_th_productCondition">
														${dropDownList.name}
														<div class="info_tooltip">
															<i class="fa fa-info-circle" data-toggle="tooltip" data-placement="top" data-original-title="Condition of the goods found in the load Product Condition Definitions"></i>
														</div>
													</c:when>
													
													<c:when test="${fn:containsIgnoreCase(dropDownList.name, '# of Pallets')}">
														<th class="loadCenterFilter_th_nbPallets">
														${dropDownList.name}
														<div class="info_tooltip">
															<i class="fa fa-info-circle" data-toggle="tooltip" data-placement="top" data-original-title="# of pallets in the load"></i>
														</div>
													</c:when>
													
													<c:when test="${fn:containsIgnoreCase(dropDownList.name, 'Manifested / Links')}">
														<th class="loadCenterFilter_th_manifest">
														${dropDownList.name}
														<div class="info_tooltip">
															<i class="fa fa-info-circle" data-toggle="tooltip" data-placement="top" data-original-title="Whether the load is Manifested, Partially Manifested or Unmanifested. Click on the link to view Manifested or Partially Manifested loads"></i>
														</div>
													</c:when>
													
													<c:when test="${fn:containsIgnoreCase(dropDownList.name, 'LoadPrice')}">
														<th class="loadCenterFilter_th_priceRange">
														${dropDownList.name}
														<div class="info_tooltip">
															<i class="fa fa-info-circle" data-toggle="tooltip" data-placement="top" data-original-title="Limits the number of loads to only those within a selected price range"></i>
														</div>
													</c:when>
													
													<c:when test="${fn:containsIgnoreCase(dropDownList.name, 'Product Features')}">
														<th class="loadCenterFilter_th_productFeatures">
														${dropDownList.name}
														<div class="info_tooltip">
															<i class="fa fa-info-circle" data-toggle="tooltip" data-placement="top" data-original-title=" Limit the loads shown to those that are on sale or one-off opportunistic buys"></i>
														</div>
													</c:when>
													
													<c:when test="${fn:containsIgnoreCase(dropDownList.name, 'Program')}">
														<th class="loadCenterFilter_th_program">
														${dropDownList.name}
														<div class="info_tooltip">
															<i class="fa fa-info-circle" data-toggle="tooltip" data-placement="top" data-original-title="Abbreviation of the store from which the load originated (store names cannot be advertised)."></i>
														</div>
													</c:when>
													
													<c:when test="${fn:containsIgnoreCase(dropDownList.name, 'Units')}">
														<th class="loadCenterFilter_th_program">
														${dropDownList.name}
														<div class="info_tooltip">
															<i class="fa fa-info-circle" data-toggle="tooltip" data-placement="top" data-original-title="Abbreviation of the units."></i>
														</div>
													</c:when>
													
													<c:when test="${fn:containsIgnoreCase(dropDownList.name, 'Price Refers To')}">
														<th class="loadCenterFilter_th_program">
														${dropDownList.name}
														<div class="info_tooltip">
															<i class="fa fa-info-circle" data-toggle="tooltip" data-placement="top" data-original-title="Abbreviation of the price refers to."></i>
														</div>
													</c:when>
													
													<c:when test="${fn:containsIgnoreCase(dropDownList.name, 'Price/Unit')}">
														<th class="loadCenterFilter_th_program">
														${dropDownList.name}
														<div class="info_tooltip">
															<i class="fa fa-info-circle" data-toggle="tooltip" data-placement="top" data-original-title="Abbreviation of the price/unit"></i>
														</div>
													</c:when>
													
													<c:when test="${fn:containsIgnoreCase(dropDownList.name, 'Wholesale Value')}">
														<th class="loadCenterFilter_th_program">
														${dropDownList.name}
														<div class="info_tooltip">
															<i class="fa fa-info-circle" data-toggle="tooltip" data-placement="top" data-original-title="Abbreviation of the Wholesale Value"></i>
														</div>
													</c:when>
													
													<c:otherwise>
														<th class="loadCenterFilter_th_store">
														${dropDownList.name}
														<div class="info_tooltip">
															<i class="fa fa-info-circle" data-toggle="tooltip" data-placement="top" data-original-title="Abbreviation of the store from which the load originated (store names cannot be advertised)."></i>
														</div>
													</c:otherwise>
													</c:choose>

														
													</th>
													</c:forEach>		
												</tr>
											</thead>
											<tbody>
											  <tr>								
												
												    <c:forEach items="${model.dropDownList}" var="dropDownList" varStatus="dropDownStatus">
												    <c:set value="field_${dropDownList.fieldId}" var="field"/>
												    
												    
												    <c:choose>												
													<c:when test="${dropDownList.name=='Packing'}">
														<td class="loadCenterFilter_td_packing">
													</c:when>
													
													<c:when test="${dropDownList.name=='Category'}">
														<td class="loadCenterFilter_td_category">
													</c:when>
													
													<c:when test="${dropDownList.name=='FOB'}">
														<td class="loadCenterFilter_td_fob">
													</c:when>
													
													<c:when test="${dropDownList.name=='Store'}">
														<td class="loadCenterFilter_td_store">
													</c:when>
													
													<c:when test="${dropDownList.name=='Product Condition'}">
														<td class="loadCenterFilter_td_productCondition">
													</c:when>
													
													<c:when test="${dropDownList.name=='# of Pallets'}">
														<td class="loadCenterFilter_td_nbPallets">
													</c:when>
													
													<c:when test="${dropDownList.name=='Manifested / Links'}">
														<td class="loadCenterFilter_td_manifest">
													</c:when>
													
													<c:when test="${dropDownList.name=='Lot Price Range'}">
														<td class="loadCenterFilter_td_priceRange">
													</c:when>
													
													<c:when test="${dropDownList.name=='Product Features'}">
														<td class="loadCenterFilter_td_productFeatures">
													</c:when>
													
													<c:when test="${dropDownList.name=='Program'}">
														<td class="loadCenterFilter_td_program">
													</c:when>
													
													<c:otherwise>
														<td class="loadCenterFilter_th_store">
													</c:otherwise>
													</c:choose>
												    
												    
												
													<div class="td_content_wrapper">
													<div class="filter_checkboxes_wrapper scroll-pane">	
													<c:forEach items="${dropDownList.value}" var="value" varStatus="status">
													<div class="checkbox">
													<label>
													<input type="checkbox" id="field_${dropDownList.fieldId}_${value}" name="field_${dropDownList.fieldId}" onclick="javascript:submit();"	
													<c:forEach items="${model.catProductFieldMap[thisCat][field].selectedValues}" var="selectedValue">
													<c:out value="${selectedValue}"/>
										      		<c:if test="${selectedValue == value}">checked</c:if>
										    	    </c:forEach> value="<c:out value="${value}"/>" />
													<c:out value="${value}"/>
													</label>
													</div>
													</c:forEach>					
													</div>
													</div>
													</td>
													</c:forEach>
															
												</tr>
											</tbody>
										  </table>
					  
										  </div>							  
										  
									  	<div class="row">
					  						<input type="hidden" name="reset" id="reset">
											<div class="col-sm-12">
											<div class="btn_wrapper download_btn_wrapper">
												<a href="#" onclick="resetButton()">
												<button type="submit" class="btn btn-lg addToCart_btn">
													Reset
												</button>
												</a>
											</div>
										</div>
										</div>
									
								</div>
							</div>
							
						
							
														
							<div class="row">
							  <div class="col-sm-12">
								<div class="pageNavigation pageNavigationTop">
							      	<div class="pageShowing">
									  <span>Showing ${frontEndProductSearch.offset + 1}-${model.pageEnd} of ${model.count}</span>
									</div>
									<div class="pageNavigationControl">
									  <c:if test="${siteConfig['PRODUCT_SORT_BY_FRONTEND'].value == 'true'}">
									  <div class="sortby">
										<span><fmt:message key="f_sortby"/></span>
										
										
										
										
										<select class="selectpicker" name="fsort" onchange="submit();">
										  <option value="">Please Select</option>
										  <option value="10" <c:if test="${model.fsort == 10}">selected="selected"</c:if>><fmt:message key="Name"/></option>
									      <option value="20" <c:if test="${model.fsort == 20}">selected="selected"</c:if>><fmt:message key="price"/> (<fmt:message key="${siteConfig['CURRENCY'].value}" /> <fmt:message key="to"/> <fmt:message key="${siteConfig['CURRENCY'].value}" /><fmt:message key="${siteConfig['CURRENCY'].value}" />)</option>
									      <option value="30" <c:if test="${model.fsort == 30}">selected="selected"</c:if>><fmt:message key="price"/> (<fmt:message key="${siteConfig['CURRENCY'].value}" /><fmt:message key="${siteConfig['CURRENCY'].value}" /> <fmt:message key="to"/> <fmt:message key="${siteConfig['CURRENCY'].value}" />)</option>
									      <c:if test="${siteConfig['PRODUCT_RATE'].value =='true' or gSiteConfig['gPRODUCT_REVIEW']}">
									        <option value="40" <c:if test="${model.fsort == 40}">selected="selected"</c:if>>Rating</option>
									      </c:if>
										</select>
										
										
										
									  </div>
									  </c:if>
									  <div class="pagesize">
										<span><fmt:message key="f_pageSize"/></span>
										<select name="size" class="selectpicker" onchange="document.getElementById('page').value=1;submit();">   
										  <c:forEach items="${model.productPerPageList}"  var="current">
											<option value="${current}" <c:if test= "${current == frontEndProductSearch.pageSize}">selected</c:if>>
												
											<c:choose>
											<c:when test="${current==1000}">
												All
											</c:when>
											<c:otherwise>
												${current}
											</c:otherwise>
											</c:choose>	
											
											 <fmt:message key="perPage" /></option>
										  </c:forEach>
										</select>
									  </div>
									  <c:if test="${model.pageCount > 1}">
									  <div class="page">
										<span><fmt:message key="page"/></span>
										<c:choose>
									      <c:when test="${model.pageCount <= 100 and model.pageCount > 0}">
									        
									        <select class="selectpicker" name="page" id="page" onchange="submit()">
											  <c:forEach begin="1" end="${model.pageCount}" var="page">
									  	        <option value="${page}" <c:if test="${page == (model.page)}">selected</c:if>>${page}</option>
									          </c:forEach>
											</select>
											<span>of ${model.pageCount}</span>
									      </c:when>
									      <c:otherwise>
									        <input type="text" id="page" name="page" value="${model.page}" size="25" class="textfield50" />
									        <input type="submit" value="go"/>
									      </c:otherwise>
									    </c:choose>
									  </div>
									  <div class="pageNav">
										<span class="pageNavLink">
										  <c:if test="${frontEndProductSearch.page == 1}">
										  <a href="#" onclick="previousButton(${model.page})">
										  <span class="pageNavPrev">
										  <i class="fa fa-angle-double-left fa-fw"></i>
										  </span>
										  </a>
										  </c:if>
										  <c:if test="${frontEndProductSearch.page != 1}">
										  <a href="#" class="pageNaviLink" onclick="previousButton(${model.page})">
										  <span class="pageNavPrev">
										  <i class="fa fa-angle-double-left fa-fw">
										  </i>
										  </span>
										  </a>
										  </span>
										  </c:if>
							    		</span>
										<span class="pageNavLink">
										  <c:if test="${frontEndProductSearch.page == model.pageCount}">
										  <a href="#">
										  <span class="pageNavNext"><i class="fa fa-angle-double-right fa-fw"></i></span></a></c:if>
										  <c:if test="${frontEndProductSearch.page != model.pageCount}">
										  <span class="pageNavLink">
										  <a href="#" onclick="nextButton(${model.page})">
										  <span class="pageNavNext">
										  <i class="fa fa-angle-double-right fa-fw">
										  </i>
										  </span>
										  </a>
										  </span>
										  </c:if>
										</span>
									  </div>
									  </c:if>
									   <c:if test="${model.pageCount <= 1}">
							               <input type="hidden" id="page" name="page" value="${frontEndProductSearch.page}" size="25" class="textfield50" />
							           </c:if>
									</div>
								  </form>
								</div>
							  </div>
							</div>


							<form action="${_contextpath}/addToCart.jhtm" method="post">
							<input type="hidden" id="loadcenter" name="loadcenter" value="true">
							<input type="hidden" name="page" value="1">
							<input type="hidden" name="size" value="52">
							<div id="loadCenter_table_wrapper" class="table-responsive">
								<table id="loadCenter_table" class="table">
									<thead>
										<tr>					
													<th class="loadCenter_th_sku">
														<div class="th_content_wrapper">
															<div class="info_tooltip">
																<i data-original-title="The SKU of products found in the load." data-placement="top" data-toggle="tooltip" class="fa fa-info-circle"></i>
															</div>
															<a href="#" onclick="changeme('50')"><span style="color:white">Sku</span></a>
														</div>
													</th>
													<th class="loadCenter_th_name">
														<div class="th_content_wrapper">
															<div class="info_tooltip">
																<i class="fa fa-info-circle" data-toggle="tooltip" data-placement="top" data-original-title="Short description of the load"></i>
															</div>
															<a href="#" onclick="changeme('10')"><span style="color:white">Name</span></a>
														</div>
													</th>
													<th class="loadCenter_th_store">
														<div class="th_content_wrapper">
															<div class="info_tooltip">
																<i class="fa fa-info-circle" data-toggle="tooltip" data-placement="top" data-original-title="Abbreviation of the store from which the load originated (store names cannot be advertised)"></i>
															</div>
															<a href="#" onclick="changeme('45')"><span style="color:white">Store</span></a>
														</div>
													</th>
													<th class="loadCenter_th_category">
														<div class="th_content_wrapper">
															<div class="info_tooltip">
																<i class="fa fa-info-circle" data-toggle="tooltip" data-placement="top" data-original-title="The category of products found in the load. Some loads can have multiple product categories assigned to them while others may just say General Merchandise but include products from several categories"></i>
															</div>
															<a href="#" onclick="changeme('2')"><span style="color:white">Category</span></a>
														</div>
													</th>
													<th class="loadCenter_th_productCondition">
														<div class="th_content_wrapper">
															<div class="info_tooltip">
																<i class="fa fa-info-circle" data-toggle="tooltip" data-placement="top" data-original-title="Condition of the goods found in the load"></i>
															</div>
															<a href="#" onclick="changeme('24')"><span style="color:white">Product Condition</span></a>
														</div>
													</th>
													<th class="loadCenter_th_packing">
														<div class="th_content_wrapper">
															<div class="info_tooltip">
																<i class="fa fa-info-circle" data-toggle="tooltip" data-placement="top" data-original-title="The type of packing in which the load will arrive"></i>
															</div>
															<a href="#" onclick="changeme('1')"><span style="color:white">Packing</span></a>
														</div>
													</th>
													<th class="loadCenter_th_nbUnits">
														<div class="th_content_wrapper">
															<div class="info_tooltip">
																<i class="fa fa-info-circle" data-toggle="tooltip" data-placement="top" data-original-title="Approximate unit count in the load"></i>
															</div>
															<a href="#" onclick="changeme('7')"><span style="color:white"># Units</span></a>
														</div>
													</th>
													<th class="loadCenter_th_nbPallets">
														<div class="th_content_wrapper">
															<div class="info_tooltip">
																<i class="fa fa-info-circle" data-toggle="tooltip" data-placement="top" data-original-title="# of pallets in the load"></i>
															</div>
															<a href="#" onclick="changeme('15')"><span style="color:white"># Pallets</span></a>
														</div>
													</th>
													<th class="loadCenter_th_fob">
														<div class="th_content_wrapper">
															<div class="info_tooltip">
																<i class="fa fa-info-circle" data-toggle="tooltip" data-placement="top" data-original-title="Origin of the load (from where the load will be shipped to you)"></i>
															</div>
															<a href="#" onclick="changeme('13')"><span style="color:white">FOB</span></a>
														</div>
													</th>
													<th class="loadCenter_th_manifest">
														<div class="th_content_wrapper">
															<div class="info_tooltip">
																<i class="fa fa-info-circle" data-toggle="tooltip" data-placement="top" data-original-title="Whether the load is Manifested, Partially Manifested or Unmanifested. Click on the link to view Manifested or Partially Manifested loads"></i>
															</div>
															<a href="#" onclick="changeme('14')"><span style="color:white">Manifest</span></a>
														</div>
													</th>
													<th class="loadCenter_th_retailValue">
														<div class="th_content_wrapper">
															<div class="info_tooltip">
																<i class="fa fa-info-circle" data-toggle="tooltip" data-placement="top" data-original-title="Approximate total Retail Value of the load."></i>
															</div>
															<a href="#" onclick="changeme('41')"><span style="color:white">Retail Value</span></a>
														</div>
													</th>
													<th class="loadCenter_th_approxRetailPerUnit">
														<div class="th_content_wrapper">
															<div class="info_tooltip">
																<i class="fa fa-info-circle" data-toggle="tooltip" data-placement="top" data-original-title="Approximate average retail value per unit in the load"></i>
															</div>
															<a href="#" onclick="changeme('42')"><span style="color:white">Approx Retail/Unit</span></a>
														</div>
													</th>
													<th class="loadCenter_th_wholesaleValue">
														<div class="th_content_wrapper">
															<div class="info_tooltip">
																<i class="fa fa-info-circle" data-toggle="tooltip" data-placement="top" data-original-title="Approximate total WSV of the load."></i>
															</div>
															<a href="#" onclick="changeme('10')"><span style="color:white">Wholesale Value</span></a>
														</div>
													</th>
													<th class="loadCenter_th_approxWholesalePerUnit">
														<div class="th_content_wrapper">
															<div class="info_tooltip">
																<i class="fa fa-info-circle" data-toggle="tooltip" data-placement="top" data-original-title="Approximate average WSV per unit in the load"></i>
															</div>
															<a href="#" onclick="changeme('43')"><span style="color:white">Approx Wholesale/Unit</span></a>
														</div>
													</th>
													<th class="loadCenter_th_loadPricePercentage">
														<div class="th_content_wrapper">
															<div class="info_tooltip">
																<i class="fa fa-info-circle" data-toggle="tooltip" data-placement="top" data-original-title="The % of WSV or Retail Value you pay for the load"></i>
															</div>
															<a href="#" onclick="changeme('11')"><span style="color:white">Load Price %</span></a>
														</div>
													</th>
													<th class="loadCenter_th_pricePerPallet">
														<div class="th_content_wrapper">
															<div class="info_tooltip">
																<i class="fa fa-info-circle" data-toggle="tooltip" data-placement="top" data-original-title="Your average price per pallet for this load"></i>
															</div>
															<a href="#" onclick="changeme('44')"><span style="color:white">Price/Pallet</span></a>
														</div>
													</th>
													<th class="loadCenter_th_pricePerUnit">
														<div class="th_content_wrapper">
															<div class="info_tooltip">
																<i class="fa fa-info-circle" data-toggle="tooltip" data-placement="top" data-original-title="Your average price per unit for this load"></i>
															</div>
															<a href="#" onclick="changeme('8')"><span style="color:white">Price/Unit</span></a>
														</div>
													</th>
													<th class="loadCenter_th_notes">
														<div class="th_content_wrapper">
															<div class="info_tooltip">
																<i class="fa fa-info-circle" data-toggle="tooltip" data-placement="top" data-original-title="The specific offer or discount for this load. Discounts will be adjusted manually after checkout"></i>
															</div>
															<a href="#" onclick="changeme('47')"><span style="color:white">Notes</span></a>
														</div>
													</th>
													<th class="loadCenter_th_price">
														<div class="th_content_wrapper">
															<div class="info_tooltip">
																<i class="fa fa-info-circle" data-toggle="tooltip" data-placement="top" data-original-title="Your price for the load"></i>
															</div>
															<a href="#" onclick="changeme('112')"><span style="color:white">Price</span></a>
														</div>
													</th>
													<th class="loadCenter_th_addToCartCheckbox">
														<div class="th_content_wrapper">
															<div class="info_tooltip">
																<i class="fa fa-info-circle" data-toggle="tooltip" data-placement="top" data-original-title="Select the checkbox(es) of the SKUs you wish to purchase. Then click the "Add to Cart" button"></i>
															</div>
															Add To Cart
														</div>
													</th>
													<th class="loadCenter_th_manifestCheckbox">
														<div class="th_content_wrapper">
															<div class="info_tooltip">
																<i class="fa fa-info-circle" data-toggle="tooltip" data-placement="top" data-original-title="Select the checkbox(es) of the SKUs you wish to download the manifest for. Then click the "Download" button"></i>
															</div>
															Manifest
														</div>
													</th>
												</tr>
									    </thead>
									   <tbody>

										<c:forEach items="${model.products}" var="product">
										<tr>
										    <c:set var="productLink">${_contextpath}/product.jhtm?<c:if test="${product.masterSku != '' and product.masterSku != null}">sku=${product.masterSku}</c:if><c:if test="${product.masterSku == '' or product.masterSku == null}">id=${product.id}</c:if><c:if test="${model.product == null or siteConfig['RECOMMENDED_LIST_SHOW_BREADCRUMB'].value == 'true'}">&cid=<c:out value="${param.cid}"/></c:if></c:set>
										    <c:if test="${gSiteConfig['gMOD_REWRITE'] == '1' and not fn:contains(product.sku, '/')}">
 												<c:set var="productLink">${_contextpath}/${siteConfig['MOD_REWRITE_PRODUCT'].value}/<c:if test="${product.masterSku != '' and product.masterSku != null}">${product.encodedMasterSku}</c:if><c:if test="${product.masterSku == '' or product.masterSku == null}">${product.encodedSku}</c:if>/${product.encodedName}.html</c:set>
										    </c:if>	
											
											<td class="loadCenter_td_sku">
                                            	<div class="td_content_wrapper">
                                            	<c:choose>
                                            	<c:when test="${model.invoiceSku == null or empty model.invoiceSku}">
                                            	   <a href="${productLink}" style="color: #337ab7;text-decoration: underline;" onmouseover="popup(event,'${product.sku}','${product.id}')" onmouseout="popupClose()">${product.sku}</a>
                                            	</c:when>
                                            	<c:otherwise>
                                            		<c:forEach items="${model.invoiceSku}" var="invoiceSku">
                                            		<c:choose>
                                            		<c:when test="${invoiceSku == product.sku}">              
                                                       	 <a href="${productLink}" style="color: #ff4500;text-decoration: underline;" onmouseover="popup(event,'${product.sku}','${product.id}')" onmouseout="popupClose()">${product.sku}</a>
                                            		</c:when>
                                            		<c:otherwise>
                                                    	 <a href="${productLink}" style="color: #337ab7;text-decoration: underline;" onmouseover="popup(event,'${product.sku}','${product.id}')" onmouseout="popupClose()">${product.sku}</a>
                                            		</c:otherwise>
                                                    </c:choose>
                                            	 </c:forEach>
                                            	 </c:otherwise>
                                            	</c:choose>
                                            	 </div>
                                            </td>
											
											<td class="loadCenter_td_name">
												<div class="td_content_wrapper">
													${product.name}
												</div>
											</td>
											
											<td class="loadCenter_td_store">
												<div class="td_content_wrapper">
													${wj:removeChar(product.field45, ',' , true, true) }
												</div>
											</td>
											<td class="loadCenter_td_category">
												<div class="td_content_wrapper">
													${wj:removeChar(product.field2, ',' , true, true) }
												</div>
											</td>
											<td class="loadCenter_td_productCondition">
												<div class="td_content_wrapper">
													${wj:removeChar(product.field24, ',' , true, true) }
												</div>
											</td>
											<td class="loadCenter_td_packing">
												<div class="td_content_wrapper">
													${wj:removeChar(product.field1, ',' , true, true) }
												</div>
											</td>
											<td class="loadCenter_td_nbUnits">
												<div class="td_content_wrapper">
													${product.field7}
												</div>
											</td>
											<td class="loadCenter_td_nbPallets">
												<div class="td_content_wrapper">
													${product.field15}
												</div>
											</td>
											<td class="loadCenter_td_fob">
												<div class="td_content_wrapper">
													${wj:removeChar(product.field13, ',' , true, true) }
												</div>
											</td>
											<td class="loadCenter_td_manifest">
												<div class="td_content_wrapper">
												<c:choose>
												<c:when test="${wj:removeChar(product.field14, ',' , true, true) == 'Unmanifested' or (product.sku == 'MIDO-LN-LOAD')}">
												<a class="thumbnail_item_name">
		    										<c:out value="${wj:removeChar(product.field14, ',' , true, true) }" escapeXml="false" /></a></c:when>
												<c:otherwise>
		    										<a href="https://www.viatrading.com/dv/manifest/manifestdetails.php?SKU=
		    										${product.sku}&id=${product.id}&cid=${model.userId}" class="thumbnail_item_name" 
		    										target="_blank">
		    										<c:out value="${wj:removeChar(product.field14, ',' , true, true) }" escapeXml="false" /></a>
												</c:otherwise>
												</c:choose>
												</div>
											</td>
											<td class="loadCenter_td_retailValue">
												<div class="td_content_wrapper">
													
											<c:choose>
                     						<c:when test="true">
                     						<c:catch var="catchFormatterException">
                             					<fmt:formatNumber value="${wj:trimAndSplit(product.field41, '|' , false, ',', true, true)}" pattern="$#,###.00" />
                     						</c:catch>
                             				<c:if test = "${catchFormatterException != null}"><c:out value="${wj:trimAndSplit(product.field41, '|' , false, ',', true, true)}" escapeXml="false"/></c:if>
                     						</c:when>
                     						<c:otherwise>
                             					<c:out value="${wj:trimAndSplit(product.field41, '|' , false, ',', true, true)}" escapeXml="false"/>
                     						</c:otherwise>
                     						</c:choose>
                     						
												</div>
											</td>
											<td class="loadCenter_td_approxRetailPerUnit">
												<div class="td_content_wrapper">
													<c:choose>
													<c:when test="true">
                     <c:catch var="catchFormatterException">
                             <fmt:formatNumber value="${wj:trimAndSplit(product.field42, '|' , false, ',', true, true)}" pattern="$#,###.00" />
                     </c:catch>
                             <c:if test = "${catchFormatterException != null}"><c:out value="${wj:trimAndSplit(product.field42, '|' , false, ',', true, true)}" escapeXml="false"/></c:if>
                     </c:when>
                     <c:otherwise>
                             <c:out value="${wj:trimAndSplit(product.field42, '|' , false, ',', true, true)}" escapeXml="false"/>
                     </c:otherwise>
                     </c:choose>
												</div>
											</td>
											<td class="loadCenter_td_wholesaleValue">
												<div class="td_content_wrapper">
												
												
												<c:choose>
													<c:when test="true">
                     <c:catch var="catchFormatterException">
                             <fmt:formatNumber value="${wj:trimAndSplit(product.field10, '|' , false, ',', true, true)}" pattern="$#,###.00" />
                     </c:catch>
                             <c:if test = "${catchFormatterException != null}"><c:out value="${wj:trimAndSplit(product.field10, '|' , false, ',', true, true)}" escapeXml="false"/></c:if>
                     </c:when>
                     <c:otherwise>
                             <c:out value="${wj:trimAndSplit(product.field10, '|' , false, ',', true, true)}" escapeXml="false"/>
                     </c:otherwise>
                     </c:choose>
												
													
												</div>
											</td>
											<td class="loadCenter_td_approxWholesalePerUnit">
												<div class="td_content_wrapper">
																		<c:choose>
													<c:when test="true">
                     <c:catch var="catchFormatterException">
                             <fmt:formatNumber value="${wj:trimAndSplit(product.field43, '|' , false, ',', true, true)}" pattern="$#,###.00" />
                     </c:catch>
                             <c:if test = "${catchFormatterException != null}"><c:out value="${wj:trimAndSplit(product.field43, '|' , false, ',', true, true)}" escapeXml="false"/></c:if>
                     </c:when>
                     <c:otherwise>
                             <c:out value="${wj:trimAndSplit(product.field43, '|' , false, ',', true, true)}" escapeXml="false"/>
                     </c:otherwise>
                     </c:choose>
												</div>
											</td>
											<td class="loadCenter_td_loadPricePercentage">
												<div class="td_content_wrapper">
													${product.field11}
												</div>
											</td>
											<td class="loadCenter_td_pricePerPallet">
												<div class="td_content_wrapper">
													
													<c:choose>
													<c:when test="true">
                     <c:catch var="catchFormatterException">
                             <fmt:formatNumber value="${wj:trimAndSplit(product.field44, '|' , false, ',', true, true)}" pattern="$#,###.00" />
                     </c:catch>
                             <c:if test = "${catchFormatterException != null}"><c:out value="${wj:trimAndSplit(product.field44, '|' , false, ',', true, true)}" escapeXml="false"/></c:if>
                     </c:when>
                     <c:otherwise>
                             <c:out value="${wj:trimAndSplit(product.field44, '|' , false, ',', true, true)}" escapeXml="false"/>
                     </c:otherwise>
                     </c:choose>
													
													
													
													
													
													
													
													
												</div>
											</td>
											<td class="loadCenter_td_pricePerUnit">
												<div class="td_content_wrapper">
													 <c:choose>
                     <c:when test="true">
                     <c:catch var="catchFormatterException">
                             <fmt:formatNumber value="${wj:trimAndSplit(product.field12, '|' , false, ',', true, true)}" pattern="$#,###.00" />
                     </c:catch>
                             <c:if test = "${catchFormatterException != null}"><c:out value="${wj:trimAndSplit(product.field12, '|' , false, ',', true, true)}" escapeXml="false"/></c:if>
                     </c:when>
                     <c:otherwise>
                             <c:out value="${wj:trimAndSplit(product.field12, '|' , false, ',', true, true)}" escapeXml="false"/>
                     </c:otherwise>
                     </c:choose>
												</div>
											</td>
											<td class="loadCenter_td_notes">
												<div class="td_content_wrapper">
													${product.field47}
												</div>
											</td>
											<td class="loadCenter_td_price">
												<div class="td_content_wrapper">
													<fmt:message key="${siteConfig['CURRENCY'].value}"/><fmt:formatNumber value="${wj:trimAndSplit(product.price1, '|' , false, ',', true, true)}" pattern="#,###.00" />
												</div>
											</td>
																            
											<c:set var="showAddToCart" value="true" />
     										<input type="hidden" name="quantity_${product.id}" value="1">
     										<input type="hidden" name="product.id" value="${product.id}">
     										<input type="hidden" name="quantity_${product.id}_${product.sku}" id="quantity_${product.id}_${product.sku}" value="${product.sku}"/>
											<input type="hidden" name="hidden.id" id="hidden_${product.id}" value="0">
											
											<td class="loadCenter_td_addToCartCheckbox">
												<div class="td_content_wrapper">
													<input id="checkbox_${product.id}" name="checkbox_${product.id}" type="checkbox" onClick="checkEnabled('hidden_${product.id}',${product.id})">
												</div>
											</td>		
											
											<td class="loadCenter_td_manifestCheckbox">
												<div class="td_content_wrapper">
													<input id="quantity_${product.id}" name="quantity_${product.id}" type="checkbox" onClick="checkEnabled('hidden_${product.id}',${product.id})" value="1">
													<input id="quantity_${product.id}_${product.sku}" name="quantity_${product.id}_${product.sku}" type="hidden" value="${product.sku}">
												</div>
											</td>
								
										</tr>
										</c:forEach>
										
									
									</tbody>
								</table>
							</div>

							<div class="buttons_wrapper clearfix">
								<div class="btn_wrapper backToHome_btn_wrapper">
									<a class="btn btn-lg backToHome_btn" href="https://www.viatrading.com">
										<span class="backToHome_icon"></span>&nbsp; Back To Home
									</a>
								</div>
								
								<div class="btn_wrapper addToCart_btn_wrapper">
									<button type="submit" class="btn btn-lg addToCart_btn">
										Add To Cart &nbsp;<span class="addToCart_icon"></span>
									</button>
								</div>
								
								<div class="btn_wrapper download_btn_wrapper">
									<a class="btn btn-lg download_btn" href="javascript:DownloadCSV()">
										Download &nbsp;<span class="download_icon" onclick="javascript:DownloadCSV()"></span>
									</a>
								</div>
							</div>
							
							</div>
							
							</form>
					</div>
				</div>
			</div>
			<!-- ========== END CONTENT ========== -->
		</div>
	</div>
</div>




<script src="assets/js/jquery-1.6.4.min.js" type="text/javascript"></script> 
<script src="assets/js/jquery-ui.min.js" type="text/javascript"></script>
<script src="assets/js/jquery.bgiframe-2.1.3-pre.js" type="text/javascript"></script> 
<script src="assets/js/jquery.glob.min.js" type="text/javascript"></script> 
<script src="assets/js/jquery.mousewheel.min.js" type="text/javascript"></script> 
<script src="assets/js/raphael-min.js" type="text/javascript"></script> 
<script src="assets/js/jquery.wijmo-open.1.1.6.min.js" type="text/javascript"></script> 
<script src="assets/fancybox/jquery.easing-1.3.pack.js" type="text/javascript"></script> 
<script src="assets/fancybox/jquery.fancybox-1.3.4.pack.js" type="text/javascript"></script> 
<script src="assets/js/jquery.cookie.js" type="text/javascript"></script>
<script src="assets/js/jcookies.js" type="text/javascript"></script>
<script src="assets/js/popup.js" type="text/javascript"></script> 
<script src="assets/js/via.js?v=1.1" type="text/javascript"></script> 
<script src="assets/js/widgets.js" type="text/javascript"></script>
<script src="assets/js/jquery.anythingslider.min.js" type="text/javascript"></script> 
<script src="assets/js/jquery.anythingslider.fx.min.js" type="text/javascript"></script> 
<script src="assets/js/jquery.loginlink.js" type="text/javascript"></script>
<script type="text/javascript" src="/assets/js/additional-methods.min.js"></script>
<script type="text/javascript" src="/assets/js/jquery.validate.min.js"></script>

<script type="text/javascript">
  		jQuery(document).ready(function($) {
        $('#emf-form').validate();
        $("#accordion").wijaccordion({header: "h1", requireOpenedPane: false});
        $("#accordionClosed").wijaccordion({header: "h3", requireOpenedPane: false, selectedIndex: "-1"});
        $('.tabs').wijtabs({alignment: 'left'});
        $('.homeTabs').wijtabs({alignment: 'top', selectedIndex: '0'});
        $('.homepageVideo').wijtabs({alignment: 'left', selectedIndex: '0'});
        $('.loginLink').loginlink();
        $(document).trigger('resize');
		$(".iframe").fancybox({
        'width'             : 780,
        'height'            : '80%',
        'autoScale'         : true,
        'transitionIn'      : 'none',
        'transitionOut'     : 'none',
        'type'              : 'iframe'
    	});

        $('#featuredLoads').anythingSlider({
                buildArrows : false,
		buildNavigation : false,
		buildStartStop : false,
		autoPlay : true,
		toggleArrows : false,
		delay : 8000,
		resizeContents : false});
		$("#filterToggle").click(function () {
		if ($(this).hasClass("hide")) {
			$(".topCheckBoxFilter_main").hide("slow");
			$(this).addClass("show").removeClass("hide").attr("value","Show Filters");
		}
		else {
			$(".topCheckBoxFilter_main").show("slow");
			$(this).addClass("hide").removeClass("show").attr("value","Hide Filters");
		};
	});


 
});

function openExcel () {
        window.open("https://www.viatrading.com/dv/widgets/formattedtable.htm");
}

function DownloadCSV() {
	var SKUs = "Manifest";
	var gaValues = "0";
	jQuery("[name^=quantity]:checked").each(function(index) {
    	SKUs += "___"+jQuery(this).next().val();
    	gaValues += ","+jQuery(this).next().val();
	});
	if (SKUs != "Manifest") {
		jQuery.get("/dv/manifest/downloadloadcenter.php", { SKU: SKUs, CHECKONLY: 1 }, function(data) {
		if (data == "SUCCESS") {
				window.open("/dv/manifest/downloadloadcenter.php?SKU="+SKUs,'_self');
				var skuArray = gaValues.split(',');
				jQuery.each(skuArray,function(i) {
					if(!(skuArray[i] == 0)) {alert(skuArray[i]);
						_gaq.push(['_trackEvent', 'Load Center', 'Download Manifest', skuArray[i]]);
					};
				});
			} else {
				alert(data);				
			}
		});
	} else {
		alert('Please select a load to download.');
	}
}

function stumbleBadge() {
	var protocol = document.location.protocol;
	if(	protocol == 'http:'){
		(function() {
		    var li = document.createElement('script'); li.type = 'text/javascript'; li.async = true;
		    li.src = ('https:' == document.location.protocol ? 'https:' : 'http:') + '//platform.stumbleupon.com/1/widgets.js';
		    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(li, s);
		 })();

	}
}

function checkEnabled(val,id) {
	if(document.getElementById(val).value=="0"){
		document.getElementById(val).value = id;
	}else{
		document.getElementById(val).value = "0";
	}
}
</script>

