<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="/WEB-INF/tlds/spring.tld" prefix="spring"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib uri="/WEB-INF/tlds/custom.tld" prefix="wj" %>

<tiles:insertDefinition name="${_template}" flush="true">  
<tiles:putAttribute name="leftbar" value="/WEB-INF/jsp/frontend/layout/template${_leftBar}/leftbar.jsp" />
	<tiles:putAttribute name="content" type="string">



<link rel="shortcut icon" href="../assets/responsive/img/favicon.ico">
<title>Home</title>


<script type="text/javascript">
<!--
	$(function(){
		$(".fancybox-thumbs").fancybox({
			padding: 1,
			helpers	: {
				overlay : {
					css : {
						'background' : 'rgba(0, 0, 0, 0.8)'
					}
				},
				title : {
					type : 'outside',
					position : 'top'
				},
				thumbs	: {
					width	: 100,
					height	: 75
				}
			}
		});
	});
	
	function showTab() {
		if( $('#avaiLoadsTab').length ){
			$("#avaiLoadsTab").tab('show');
		} else {
			location.href = '/login.jhtm?forwardAction='+document.URL;
		}
	}
//-->
</script>

</head>
<body>
	<!-- ========== PAGE WRAPPER ========== -->
	<div id="pageWrapper">


		
		<!-- ========== PAGE BODY ========== -->
		<div id="pageBodyWrapper">
			<div id="pageBody" class="container">
				<div class="row row-offcanvas row-offcanvas-left">
					<!-- ========== LEFT SIDE BAR ========== -->
					<div id="leftSidebarWrapper">
						<div id="leftSidebar">
							<div id="btn-toggle-offcanvas" class="visible-xs">
								<button type="button" class="btn btn-primary" data-toggle="offcanvas">
									<i class="fa fa-bars"></i>
								</button>
							</div>
							<div class="col-xs-6 col-sm-3 col-md-2 sidebar-offcanvas" id="sidebar" role="navigation">
								<div class="leftbar_catLinks_wrapper">
									<div id=""><a class="leftbar_catLink" href="#">ALL PRODUCTS</a></div>
									<div id=""><a class="leftbar_catLink_selected" href="#">NEW ARRIVALS</a></div>
									<div id=""><a class="leftbar_subcatLink" href="#">CLOTHING</a></div>
									<div id=""><a class="leftbar_subcatLink" href="#">COSMETICS/HBA</a></div>
									<div id=""><a class="leftbar_subcatLink" href="#">DOMESTICS</a></div>
									<div id=""><a class="leftbar_subcatLink" href="#">ELECTRONICS</a></div>
									<div id=""><a class="leftbar_subcatLink_selected" href="#">ACCESSORIES</a></div>
									<div id=""><a class="leftbar_subcatLink" href="#">FURNITURE</a></div>
									<div id=""><a class="leftbar_subcatLink" href="#">GENERAL MERCHANDISE</a></div>
									<div id=""><a class="leftbar_subcatLink" href="#">HOME &amp; KITCHEN</a></div>
									<div id=""><a class="leftbar_subcatLink" href="#">PACKING SUPPLIES</a></div>
									<div id=""><a class="leftbar_subcatLink" href="#">SEASONAL/HOLIDAY</a></div>
									<div id=""><a class="leftbar_subcatLink" href="#">SHOES</a></div>
									<div id=""><a class="leftbar_subcatLink" href="#">TOOLS</a></div>
									<div id=""><a class="leftbar_subcatLink" href="#">TOYS</a></div>
									<div id=""><a class="leftbar_catLink" href="#">LOADS &amp; TRUCKLOADS</a></div>
									<div id=""><a class="leftbar_catLink" href="#">LOAD CENTER</a></div>
									<div id=""><a class="leftbar_subcatLink" href="#">ON SALE</a></div>
									<div id=""><a class="leftbar_subcatLink" href="#">ONE TIME DEALS</a></div>
									<div id=""><a class="leftbar_subcatLink" href="#">RETAIL DISPLAY ITEMS</a></div>
									<div id=""><a class="leftbar_subcatLink" href="#">STARTER PACKS</a></div>
									<div id=""><a class="leftbar_subcatLink" href="#">DOLLAR STORE ITEMS</a></div>
									<div id=""><a class="leftbar_subcatLink" href="#">AMAZON FBA ITEMS</a></div>
								</div>
							</div>
						</div>
					</div>
					<!-- ========== END LEFT SIDE BAR ========== -->

					<!-- ========== CONTENT ========== -->
					<div id="contentWrapper">
						<div id="content">
							<div class="col-sm-9 col-md-10">
								<div class="row">
									<div class="col-sm-12">
										<div id="product_details_1" class="clearfix">
											<div class="clearfix">
												<div class="social_media_share">
													<div class="addthis_native_toolbox"></div>
												</div>
											</div>
											<c:set value="${model.product}" var="product" />
											<div class="details_info_box clearfix">
												<div class="row">
													<div class="col-sm-12 col-md-6">
														<div class="item_name">
															<h1>Lot Details</h1>
														</div>
														<div class="item_sku">
															<span class="label_title">SKU:</span>
															<span class="value">${product.sku}</span>
														</div>
														
														<div class="item_condition">
															<span class="label_title">Condition:</span>
															<span class="value">${product.field24}</span>
														</div>
														
														<div class="item_condition">
															<span class="label_title">Manifest Status:</span>
															<span class="value">${product.field14}</span>
														</div>
									
														<div class="item_condition">
															<span class="label_title">FOB Point:</span>
															<span class="value">${product.field13}</span>
														</div>

														<div class="unit_price">
															<span class="label_title"># of Units:</span>
															<span class="price_value">${product.field7}</span>
														</div>
														
														<div class="unit_price">
															<span class="label_title">Price/Unit:</span>
															<span class="price_value">$${product.field8}</span>
														</div>
														
														<div class="big_price total_price">
															<span class="label_title">Lot Price:</span>
															<span class="price_value">${product.field11}</span>
															<a class="getshippingQuoteLink" href="#">(Get a Shipping Quote)</a>
														</div>	
										
														<div class="big_price total_price">
															<span class="label_title">Total Price:</span>
															<span class="price_value">$${product.field7*product.field8}</span>
															<a class="getshippingQuoteLink" href="#">(Get a Shipping Quote)</a>
														</div>	
														
														<form id="addToCartForm" action="${_contextpath}/addToCart.jhtm" method="post" class="clearfix">															
															
															<div class="quantity">
																<div class="clearfix">
																	<div class="pull-left">
																		<span class="label_title">Quantity:</span>
																	</div>
																	<div class="pull-left">
																		<input type="text" class="touchspin form-control" style="display: block;" name="quantity_${product.id}">
																		<span class="input-group-btn-vertical">
																		</span>
																	</div>
																	<div class="clearfix">
																		
																		<label class="checkbox-inline">
																			<input type="checkbox" value="" id=""> Include Retail Display
																		</label>
																	</div>				
																</div>
															</div>
															
															<div class="saving_info_wrapper">
																<div class="title">Buy More and Save!</div>
																<ul class="saving_options">
																	<c:if test="${model.product.qtyBreak1 != null}">
																		<li>Buy ${model.product.qtyBreak1} or more: ${model.product.price2}/unit and ${(model.product.price2)*model.product.field7}/lot (Save %${(100*(model.product.price1-model.product.price2))/model.product.price1} or $${model.product.field7*(model.product.price1-model.product.price2)})</li>
		  															</c:if>
		  															
		  															<c:if test="${model.product.qtyBreak2 != null}">
																		<li>Buy ${model.product.qtyBreak2} or more: ${model.product.price3}/unit and ${(model.product.price3)*model.product.field7}/lot (Save %${(100*(model.product.price1-model.product.price3))/model.product.price1} or $${model.product.field7*(model.product.price1-model.product.price3)})</li>
		  															</c:if>
		  															
		  															<c:if test="${model.product.qtyBreak3 != null}">
																		<li>Buy ${model.product.qtyBreak3} or more: ${model.product.price4}/unit and ${(model.product.price4)*model.product.field7}/lot (Save %${(100*(model.product.price1-model.product.price4))/model.product.price1} or $${model.product.field7*(model.product.price1-model.product.price4)})</li>
		  															</c:if>
		  															
		  															<c:if test="${model.product.qtyBreak4 != null}">
																		<li>Buy ${model.product.qtyBreak4} or more: ${model.product.price5}/unit and ${(model.product.price5)*model.product.field7}/lot (Save %${(100*(model.product.price1-model.product.price5))/model.product.price1} or $${model.product.field7*(model.product.price1-model.product.price5)})</li>
		  															</c:if>
		  															
		  															<c:if test="${model.product.qtyBreak5 != null}">
																		<li>Buy ${model.product.qtyBreak5} or more: ${model.product.price6}/unit and ${(model.product.price6)*model.product.field7}/lot (Save %${(100*(model.product.price1-model.product.price6))/model.product.price1} or $${model.product.field7*(model.product.price1-model.product.price6)})</li>
		  															</c:if>
		  															
		  															<c:if test="${model.product.qtyBreak6 != null}">
																		<li>Buy ${model.product.qtyBreak6} or more: ${model.product.price7}/unit and ${(model.product.price7)*model.product.field7}/lot (Save %${(100*(model.product.price1-model.product.price7))/model.product.price1} or $${model.product.field7*(model.product.price1-model.product.price7)})</li>
		  															</c:if>		
																</ul>
															</div>
															
															<div class="buttons_wrapper clearfix">
																<div class="btn_wrapper addToCart_btn_wrapper">
																	<button type="submit" class="btn btn-lg addToCart_btn">
																		Add To Cart &nbsp;<span class="addToCart_icon"></span>
																	</button>
																</div>
																<div class="btn_wrapper addToList_btn_wrapper">
																	<a class="btn addToList_btn" href="${_contextpath}/addToList.jhtm">
																		Add To Wishlist
																	</a>
																</div>
															</div>
															
															<div class="buttons_wrapper clearfix">
																<div class="btn_wrapper seeLoadPrices_btn_wrapper">
																	<a onclick="showTab();" class="btn btn-lg seeLoadPrices_btn">
																		See Load Prices
																	</a>
																</div>
																<div class="btn_wrapper addToList_btn_wrapper">
																	<a class="btn addToList_btn" href="${_contextpath}/addToList.jhtm">
																		Add To Wishlist
																	</a>
																</div>
															</div>
					
														</form>
													</div>
													
													<div class="col-sm-12 col-md-6">
														<div class="item_rating clearfix">
															<div class="rating_wrapper">
																<span class="label_title">Rating:</span>
																<div class="star_rating_wrapper">
																	<input class="rating" data-readonly="true" data-value="2.5">
																</div>
															</div>
															<div class="item_reviews">3 Reviews | <a href="${_contextpath}/addProductReview.jhtm?sku=${product.sku}&cid=">Write a review</a></div>
														</div>
														<div class="item_image_box clearfix">
															<div class="item_image_wrapper">
																<img src="https://www.viatrading.com/assets/Image/Product/detailsbig/load-ctc.gif" class="item_image img-responsive center-block">
															</div>
														</div>
														<div class="item_image_thumbnails clearfix">
															<ul class="clearfix">	
															<c:if test="${fn:length(model.product.images) > 1}">
																<c:forEach items="${model.product.images}" var="image" varStatus="status">
																<c:if test="${status.index > -1}">
																<li>
																<div class="thumbnail_image">
		     														<a href="<c:if test="${!image.absolute}">${_contextpath}/assets/Image/Product/detailsbig/</c:if><c:out value="${image.imageUrl}"/>" data-hover="swap-img" data-target=".item_image">
			  														<img src="<c:if test="${!image.absolute}">${_contextpath}/assets/Image/Product/detailsbig/</c:if><c:out value="${image.imageUrl}"/>" class="img-responsive center-block">
			  														</a>
		    													</div>
																</li>
		    													</c:if>
		  														</c:forEach>
	 														 </c:if> 
															</ul>
														</div>
													</div>
												</div>
											</div>							
					
											<%@ include file="/WEB-INF/jsp/frontend/layout/template6/productTabs3.jsp" %>
										
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
					<!-- ========== END CONTENT ========== -->
				</div>
			</div>
		</div>
		<!-- ========== END PAGE BODY ========== -->
		
	

	</div>


	</tiles:putAttribute>
</tiles:insertDefinition>