<%@ page session="false" %>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>

<tiles:insertDefinition name="${_template}" flush="true">
  <c:if test="${_leftBar != '1' and _leftBar != '4'}">
    <tiles:putAttribute name="leftbar" value="/WEB-INF/jsp/frontend/layout/template${_leftBar}/leftbar.jsp" />
  </c:if>
  <tiles:putAttribute name="content" type="string">				

<body>
	<div id="pageWrapper">
		
		<!-- ========================= -->
		<!--          HEADER           -->
		<!-- ========================= -->
		<div id="pageHeaderWrapper">
			<div id="pageHeader"></div>
		</div>
		
		<!-- ========================= -->
		<!--           BODY            -->
		<!-- ========================= -->
		<div id="pageBodyWrapper">
			<div id="pageBody" class="container">
				<div class="row row-offcanvas row-offcanvas-left">

					<!-- ========================= -->
					<!--          CONTENT          -->
					<!-- ========================= -->
					<div id="contentWrapper">
						<div id="content">

							<div class="col-sm-12">
								<div class="selectOptionsWrapper">
									<div class="headerTitle selectOptionsTitle"><fmt:message key="f_cart_intermidiate__selectOptions" /></div>

									<div class="clearfix"></div>

							    <form action="${_contextpath}/addToCart.jhtm?intermediateCart=true" name="this_form" method="post" role="form">
									<div class="selectOptions_table" style="border:0px">
										  <div class="selectOptionsHdr">
												<div class="row clearfix">
													<div class="col-sm-12 col-md-1">
														<div class="listImageHeader">Image</div>
													</div>
													<div class="col-sm-12 col-md-2">
														<div class="listSkuHeader"><fmt:message key="sku"/></div>
													</div>
													<div class="col-sm-12 col-md-4">
														<div class="listNameHeader"><fmt:message key="product"/></div>
													</div>
													<div class="col-sm-12 col-md-3">
														<div class="listDescriptionHeader"><fmt:message key="description"/></div>
													</div>
													<div class="col-sm-12 col-md-1">
														<div class="listPriceHeader"><fmt:message key="price"/></div>
													</div>
													<div class="col-sm-12 col-md-1">
														<div class="listQtyHeader"><fmt:message key="quantity"/></div>
													</div>
												</div>
										  </div>
										  <div class="selectOptionsDetails">	
											 <c:forEach items="${model.productsWithOptions}" var="cartItem" varStatus="status">
											   <c:set value="${cartItem.product}" var="product"/>
											
											   <div  
												<c:choose>
												<c:when test="${status.index%2==0}">
													class="odd_row clearfix"
												</c:when>
												<c:otherwise>
													class="even_row clearfix"
												</c:otherwise>
												</c:choose>
												>
	    										<%@ include file="/WEB-INF/jsp/frontend/quickmode/viewQuickForm.jsp" %>
	
													<div class="col-sm-12 col-md-1">
															<div class="listQtyHeader visible-xs visible-sm">Quantity</div>
															<div class="listQty">
																<input name="quantity_${status.index}" size="5" maxlength="5" type="hidden" value="${cartItem.quantity}" />
																${cartItem.quantity}
															</div>
															<div class="clearfix"></div>
													</div>	
											   </div>
											</c:forEach>
										  </div>
										  <div class="selectOptionsFtr">
												<div class="row clearfix">
													
												</div>
										  </div>	
			    					</div>
									<div class="row">
										<div class="col-sm-12">
											<div class="addToCart_btn_wrapper">
												<button type="submit" class="btn addToCart_btn"><fmt:message key="addToCart" /></button>
											</div>
										</div>
								    </div>
							    </form>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		</div>
		<!-- ========================= -->
		<!--          FOOTER           -->
		<!-- ========================= -->
		<div id="pageFooterWrapper">
			<div id="pageFooter"></div>
		</div>
	</div>
</body>

  </tiles:putAttribute>
</tiles:insertDefinition>
