<%@ page session="false" %>
<%@ page import="java.util.*" %>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="/WEB-INF/tlds/spring.tld" prefix="spring" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ taglib uri="/WEB-INF/tlds/custom.tld" prefix="wj" %>

<tiles:insertDefinition name="${_template}" flush="true">
  
  
  <c:if test="${_leftBar != '1' and _leftBar != '4'}">
    <tiles:putAttribute name="leftbar" value="/WEB-INF/jsp/frontend/layout/template${_leftBar}/leftbar.jsp" />
  </c:if>
  <tiles:putAttribute name="content" type="string">

<c:if test="${fn:trim(model.productLayout.headerHtml) != ''}">
  <c:set value="${model.productLayout.headerHtml}" var="headerHtml"/>
  <%@ include file="/WEB-INF/jsp/frontend/common/headerDynamicElement.jsp" %>
  <div><c:out value="${headerHtml}" escapeXml="false"/></div>
</c:if>

<c:if test="${model.message != null}">
  <div class="message"><fmt:message key="${model.message}" /></div>
</c:if>

<c:if test="${model.product != null}">
<c:set value="${model.product}" var="product"/>

<%-- bread crumbs --%>
<c:if test="${model.breadCrumbs != null and fn:length(model.breadCrumbs) > 0}">
  <c:import url="/WEB-INF/jsp/frontend/layout/${_template}/breadcrumbs.jsp" />
</c:if>

<div class="col-sm-12">

<div id="product_details" class="row">
  <div class="col-sm-4">
	<div class="details_image_box <c:if test="${product.salesTag != null}">is_ribboned</c:if>">
	   <c:if test="${product.salesTag != null}">
	     <div class="ribbon_wrapper">
		   <div class="ribbon ribbon_1">${product.salesTag.title}</div>
	     </div>
	   </c:if>
	   <img src="<c:if test="${!model.product.images[0].absolute}">${_contextpath}/assets/Image/Product/detailsbig/</c:if><c:out value="${model.product.images[0].imageUrl}"/>" title="111" class="img-responsive center-block">
	</div>
	<c:if test="${fn:length(model.product.images) > 1}">
	  <div class="details_image_thumbnails">
	    <ul class="clearfix">
		  <c:forEach items="${model.product.images}" var="image" varStatus="status">
			<c:if test="${status.index > 0}">
			<li>
		      <a href="<c:if test="${!image.absolute}">${_contextpath}/assets/Image/Product/detailsbig/</c:if><c:out value="${image.imageUrl}"/>" data-lightbox="product_images" data-title="<c:out value="${product.name}" escapeXml="false" />">
			    <img src="<c:if test="${!image.absolute}">${_contextpath}/assets/Image/Product/thumb/</c:if><c:out value="${image.thumbnailImageUrl}"/>" class="img-responsive">
			  </a>
		    </li>
		    </c:if>
		  </c:forEach>
		</ul>
	  </div>
	  </c:if>  
	
	  <c:if test="${gSiteConfig['gPRODUCT_REVIEW'] and product.enableRate}">
		<%@ include file="/WEB-INF/jsp/frontend/layout/template6/productReview.jsp" %>
	  </c:if>
    </div>
	
	<div class="col-sm-8">
	  <div class="details_desc clearfix">
		<c:if test="${model.manufacturer.imageUrl != null and fn:length(model.manufacturer.imageUrl) > 10}">
		<div class="details_brand">
		  <img src="${model.manufacturer.imageUrl}" class="img-responsive">
		</div>
		</c:if>
		<div class="details_item_name">
		  <h1><c:out value="${product.name}" escapeXml="false"/></h1>
		</div>
		<div class="details_info">
		  <div class="details_product_number">
			<span><fmt:message key="product" /> #:</span> <c:out value="${product.sku}" escapeXml="false"/>
		  </div>
		  
		  <c:forEach items="${product.productFieldsUnlimitedNameValue}" var="productFielUnlimitedNameValue" varStatus="status"> 
	        <c:if test="${productFielUnlimitedNameValue.displayName == 'Product Model' and productFielUnlimitedNameValue.displayValue != null}">
		      <div class="details_model_number">
		  	    <span><c:out value="${productFielUnlimitedNameValue.displayName}" escapeXml="false" /> #:</span> <c:out value="${productFielUnlimitedNameValue.displayValue}" escapeXml="false"/>
		  	  </div>
		    </c:if>
		  </c:forEach>
		  
		  <div class="details_availability">
			<span>Availability: </span> 
			<c:choose>
		  		<c:when test="${product.inventory > 0}">
		  			IN STOCK
		  		</c:when>
		  		<c:otherwise>
		  			SPECIAL ORDER
		  		</c:otherwise>
		  	</c:choose>
		  </div>
		  <div class="clear"></div>
		</div>
		
		<c:choose>
		  <c:when test="${ (gSiteConfig['gSHOPPING_CART'] and (!empty product.price or product.priceByCustomer)) and (!product.loginRequire or userSession != null)}">
	        <c:choose>
	          <c:when test="${fn:length(product.price) > 1}">
	            <div class="prices_table">
				  <div class="quantities">
					<div class="quantities_title"> <fmt:message key="quantity" />: </div>
					  <c:forEach items="${product.price}" var="price" varStatus="statusPrice">
				        <div class="quantity_cell">
				          <c:if test="${price.qtyFrom != null}">
						    <c:choose>
						      <c:when test="${price.qtyTo != null}">
						        <c:out value="${price.qtyFrom}" /> -  <c:out value="${price.qtyTo}" />
						      </c:when>
						      <c:otherwise>
						        <c:out value="${price.qtyFrom}" /> +
						      </c:otherwise>
						    </c:choose>
						  </c:if>
						  <c:if test="${price.qtyFrom == null}">
						    1 +
						  </c:if>
				        </div>
					  </c:forEach>
				  </div>
				  <div class="prices">
					<div class="prices_title"><fmt:message key="price" />:</div>
					<c:forEach items="${product.price}" var="price" varStatus="statusPrice">
				    <div class="price_cell <c:if test="${product.salesTag != null}">strikethrough</c:if>">
					  <span class="price">  
					    <fmt:message key="${siteConfig['CURRENCY'].value}" /><fmt:formatNumber value="${price.amt}" pattern="#,##0.00#" maxFractionDigits="${siteConfig['MAX_FRACTION_DIGIT'].value}" />
				      </span>
					  <span class="caseContent">
					  <c:choose>
				        <c:when test="${product.caseContent != null}">(<c:out value="${product.caseContent}" /> 
				        <c:choose>
	  						<c:when test="${product.caseUnitTitle != null}"><c:out value="${product.caseUnitTitle}" /></c:when> 
	  						<c:otherwise><c:out value="${siteConfig['CASE_UNIT_TITLE'].value}" /></c:otherwise>
	  					</c:choose>
				        /<c:out value="${product.packing}" />)</c:when>
				        <c:otherwise><c:out value="${product.packing}" /></c:otherwise>
				      </c:choose>
					  </span>
					</div>
					</c:forEach>
				  </div>
				  <c:if test="${product.salesTag != null}">
					<div class="prices">
					<div class="prices_title"></div>
					<c:forEach items="${product.price}" var="price" varStatus="statusPrice">
				    <div class="price_cell new_price">
					  <span class="price">  
					    <fmt:message key="${siteConfig['CURRENCY'].value}" /><fmt:formatNumber value="${price.discountAmt}" pattern="#,##0.00#" maxFractionDigits="${siteConfig['MAX_FRACTION_DIGIT'].value}" />
				      </span>
					  <span class="caseContent">
					  <c:choose>
				        <c:when test="${product.caseContent != null}">(<c:out value="${product.caseContent}" /> <c:out value="${siteConfig['CASE_UNIT_TITLE'].value}" />/<c:out value="${product.packing}" />)</c:when>
				        <c:otherwise><c:out value="${product.packing}" /></c:otherwise>
				      </c:choose>
					  </span>
					</div>
					</c:forEach>
				  </div>
				  </c:if>
					
				  <div class="clear"></div>
				</div>
	          </c:when>
	          <c:otherwise>
		        <c:choose>
				    <c:when test="${product.salesTag != null}">
				    <div class="price_wrapper">
				  	<div class="price_title"> <fmt:message key="f_price" />:</div>
				      <div class="price_value strikethrough">
				            <span class="price">
				        	  <fmt:message key="${siteConfig['CURRENCY'].value}" /><fmt:formatNumber value="${product.price[0].amt}" pattern="#,##0.00#" maxFractionDigits="${siteConfig['MAX_FRACTION_DIGIT'].value}" />
				      		</span>
					        <span class="caseContent">
					        <c:choose>
				        	  <c:when test="${product.caseContent != null}">(<c:out value="${product.caseContent}" /> 
				        		<c:choose>
			  						<c:when test="${product.caseUnitTitle != null}"><c:out value="${product.caseUnitTitle}" /></c:when> 
			  						<c:otherwise><c:out value="${siteConfig['CASE_UNIT_TITLE'].value}" /></c:otherwise>
	  							</c:choose>
				        		/<c:out value="${product.packing}" />)</c:when>
				        	  <c:otherwise><c:out value="${product.packing}" /></c:otherwise>
				      		</c:choose>
					        </span>
				      </div>
				      <div class="price_value new_price">
				            <span class="price"><fmt:message key="${siteConfig['CURRENCY'].value}" /><fmt:formatNumber value="${product.price[0].discountAmt}" pattern="#,##0.00#" maxFractionDigits="${siteConfig['MAX_FRACTION_DIGIT'].value}" /></span>
				            <span class="caseContent">
				              <c:choose>
				        	  <c:when test="${product.caseContent != null}">(<c:out value="${product.caseContent}" /> 
				        		<c:choose>
			  						<c:when test="${product.caseUnitTitle != null}"><c:out value="${product.caseUnitTitle}" /></c:when> 
			  						<c:otherwise><c:out value="${siteConfig['CASE_UNIT_TITLE'].value}" /></c:otherwise>
	  							</c:choose>
				        		/<c:out value="${product.packing}" />)</c:when>
				        	  <c:otherwise><c:out value="${product.packing}" /></c:otherwise>
				      		</c:choose>
				            </span>
				      </div>
				      </div>
				    </c:when>
					<c:otherwise>
						<div class="price_wrapper">
						      <span class="price_title"><fmt:message key="f_price" />:</span>
						      <span class="price_value"><fmt:message key="${siteConfig['CURRENCY'].value}" /><fmt:formatNumber value="${product.price[0].amt}" pattern="#,##0.00#" maxFractionDigits="${siteConfig['MAX_FRACTION_DIGIT'].value}" /></span>
				        </div>
					</c:otherwise>				  
				  </c:choose>
				  <div class="clear"></div>
				
	          </c:otherwise>
	        </c:choose>
		    
		    
		    <c:if test="${product.salesTag != null}">
			<div id="discount_wrapper">
			  <c:if test="${product.salesTag.image}">
			  <div id="discount">
				<img src="${_contextpath}/assets/Image/PromoSalesTag/salestag_${product.salesTag.tagId}.jpg" class="img-responsive">
			  </div>
			  </c:if>
			  
			  <c:if test="${siteConfig['SALES_TAG_COUNTDOWN'].value == 'true' and product.salesTag.inEffect and product.salesTag.daysLeft < 7 }">
			  <script type="text/javascript" src="${_contextpath}/javascript/countDown.js"></script>
			  <script type="text/javascript">
				<!--
			 	jQuery("document").ready(function() {
					countdown();
				});
				//-->
			  </script>
			  <input type="hidden" value="${product.salesTag.endDate}" id="endDateValue"/>
			  <div id="countdown_dashboard">
				<div class="title" id="titleBox">Time Left:</div>
				<div class="digit day" id="daysBox"></div>
				<div class="colon" id="daysColon">:</div>
				<div class="digit hour" id="hoursBox"></div>
				<div class="colon" id="daysColon">:</div>
				<div class="digit min" id="minsBox"></div>
				<div class="colon" id="daysColon">:</div>
				<div class="digit sec" id="secsBox"></div>
			  </div>
			  </c:if>
			  
			  <div class="clear"></div>
			</div>
			</c:if>
			  
		    <form id="addToCartForm" action="${_contextpath}/addToCart.jhtm" method="post" class="clearfix">
			  <input name="product.id" type="hidden" value="${product.id}">
		      <div class="quantity_wrapper">
				<span class="quantity_title"><fmt:message key="quantity" /></span>
				
				<c:choose>
				   <c:when test="${gSiteConfig['gINVENTORY'] and product.inventory != null and !product.negInventory and product.inventory <= 0}" >
				   	 <input name="quantity_${product.id}" disabled="disabled" size="5" maxlength="5" class="qty_input disabled" type="text" autocomplete="off" value="${product.minimumQty}">	
				   </c:when>
				   <c:otherwise>
					<input name="quantity_${product.id}" size="5" maxlength="5" class="qty_input" type="text" autocomplete="off" value="${product.minimumQty}">	
				   </c:otherwise>
				</c:choose>
			  </div>
			  <div class="addToCart_btn_wrapper">
				<c:choose>
				   <c:when test="${gSiteConfig['gINVENTORY'] and product.inventory != null and !product.negInventory and product.inventory <= 0}" >
				   		<span class="btn outOfStock_btn">Out of Stock</span>
				   </c:when>
				   <c:otherwise>
				   		<button type="submit" class="btn addToCart_btn">
							Add To Cart &nbsp;<span class="addToCart_icon"></span>
						</button>
				   </c:otherwise>
				</c:choose>
			  </div>
			  <div class="addToList_btn_wrapper">
				<a class="btn addToList_btn" href="${_contextpath}/addToList.jhtm?product.id=${product.id}">
				  Add To List &nbsp;<span class="addToList_icon"></span>
				</a>
			  </div>
			  <c:if test="${siteConfig['PRODUCT_QUOTE'].value == 'true' and product.quote}">
				<div class="addToQuote_btn_wrapper" id="addToQuoteListWrapperId${product.id}">
				  <a class="btn addToQuote_btn" href="#" onClick="addToQuoteList('${product.id}');" >
					Add To Quote &nbsp;<span class="addToQuote_icon"></span>
				  </a>
				</div>
			  </c:if>
			
			  <%--
			  <div class="addToCompare_btn_wrapper" id="addToCompareWrapperId${product.id}">
			    <a class="btn addToCompare_btn" href="#" onClick="addToCompareList('${product.id}');" >
				  Compare &nbsp;<span class="addToCompare_icon"></span>
				</a>
			  </div>
			   --%>
			</form>	
		  </c:when>
	      <c:when test="${siteConfig['PRODUCT_QUOTE'].value == 'true' and product.quote}">
			<div class="addToList_btn_wrapper">
			  <a class="btn addToList_btn" href="${_contextpath}/addToList.jhtm?product.id=${product.id}">
			    Add To List &nbsp;<span class="addToList_icon"></span>
			  </a>
			</div>
			<div class="addToQuote_btn_wrapper" id="addToQuoteListWrapperId${product.id}">
			 <a class="btn addToQuote_btn" href="#" onClick="addToQuoteList('${product.id}');" >
				Add To Quote &nbsp;<span class="addToQuote_icon"></span>
			  </a>
			</div>
			<%--
			<div class="addToCompare_btn_wrapper" id="addToCompareWrapperId${product.id}">
		      <a class="btn addToCompare_btn" href="#" onClick="addToCompareList('${product.id}');" >
				Compare &nbsp;<span class="addToCompare_icon"></span>
			  </a>
			</div>
			 --%>
	      </c:when>
	    </c:choose>	
											
		<%@ include file="/WEB-INF/jsp/frontend/layout/template6/productTabs.jsp" %>

	  </div>
	</div>
  </div>
</c:if>

<c:if test="${fn:trim(model.productLayout.footerHtml) != ''}">
  <c:set value="${model.productLayout.footerHtml}" var="footerHtml"/>
  <%@ include file="/WEB-INF/jsp/frontend/common/footerDynamicElement.jsp" %>
  <div style="float:left;width:100%"><c:out value="${footerHtml}" escapeXml="false"/></div>
</c:if>

 
<c:if test="${siteConfig['PRODUCT_QUOTE'].value == 'true' and product.quote}">  
<script type="text/javascript">
<!--
function addToQuoteList(productId){
	$.ajax({
		url: "${_contextpath}/quoteList.jhtm?productId="+productId,
		context: document.body
	}).done(function() {
		$("#addToQuoteListWrapperId"+productId).html('<a href=\"${_contextpath}/quoteViewList.jhtm\" class=\"btn addToQuote_btn\">View Quote List</a>');
	});
}

function addToCompareList(productId){
	$.ajax({
		url: "${_contextpath}/ajaxAddToCompare.jhtm?&productId="+productId,
		context: document.body
	}).done(function() {
		$("#addToCompareWrapperId"+productId).html('<a href=\"${_contextpath}/comparison.jhtm\" class=\"btn addToCompare_btn\">View Comparison</a>');
	});
}
//-->
</script>
</c:if>    
  </tiles:putAttribute>
</tiles:insertDefinition>