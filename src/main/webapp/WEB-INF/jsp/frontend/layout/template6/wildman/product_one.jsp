<%@ page session="false" %>
<%@ page import="java.util.*" %>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="/WEB-INF/tlds/spring.tld" prefix="spring" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ taglib uri="/WEB-INF/tlds/custom.tld" prefix="wj" %>

<tiles:insertDefinition name="${_template}" flush="true">
  <c:if test="${_leftBar != '1' and _leftBar != '4'}">
    <tiles:putAttribute name="leftbar" value="/WEB-INF/jsp/frontend/layout/template${_leftBar}/leftbar.jsp" />
  </c:if>
  <tiles:putAttribute name="content" type="string">
        <script src="//ajax.googleapis.com/ajax/libs/jqueryui/1.10.4/jquery-ui.min.js"></script>
        <script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1.7.2/jquery.min.js"></script>
        <link rel="stylesheet" href="//ajax.googleapis.com/ajax/libs/jqueryui/1.10.4/themes/smoothness/jquery-ui.css" />
        <c:if test="${siteConfig['CUSTOMER_GROUP_GALLERY'].value == 'true'}">
          <script type="text/javascript" src="${_contextpath}/javascript/drop-down-image.js"></script>
        </c:if>
        <script type="text/javascript">
        <!--
        try {
            $.noConflict();
        } catch (e) { }
        //-->
        </script>
        <script type="text/javascript">
            $(function(){
                $('#cloudzoom_1').bind('click',function(){
                    var cloudZoom = $(this).data('CloudZoom');
                    cloudZoom.closeZoom();
                    $.fancybox.open(cloudZoom.getGalleryList());
                    return false;
                });
            });
            CloudZoom.quickStart();
        </script>
  
        <script type="text/javascript">
        <!--
        var jqry = jQuery.noConflict();
        
        jqry(window).load(function(){
            jqry( ".imprintLocation" ).each(function( index ) {
                jqry( "#imprintLogoOptionsWrapper"+parseInt(index+2) ).slideUp( "fast", function(){
                    jqry( "#imprintLogoOptionsWrapper"+parseInt(index+2) ).css('overflow','visible'); 
                } );
            });
        });
        
        function updateMonogramText(text){
            jqry( "#monogramText" ).val("Monogram Text_value_"+text);
        }
        function loadLocation(){
			var size = jqry( ".imprintLocation" ).size();
            jqry( ".imprintLocation" ).each(function( index ) {
                if(jqry( this ).val() == null || jqry( this ).val().length == 0) {
                    if(jqry( "#imprintLogoOptionsWrapper"+parseInt(index+1) ).css('display') != 'none') {
                        alert('Please select location from available options first.');
                    } else {
                        jqry( "#imprintLogoOptionsWrapper"+parseInt(index+1) ).slideDown( "slow", function(){
                            jqry( "#imprintLogoOptionsWrapper"+parseInt(index+1) ).css('overflow','visible'); 
                        } );
                    }
                    return false;
                }
                if((index + 1) == (size -1)){
					jqry( ".applyAnotherLogo_btn_wrapper" ).hide();
				}
            });
        }
        function updateLocations(monogramId) {

        	var monogramLocation = 1;
        	jqry( ".imprintLocation" ).each(function( index ) {
        		if(jqry( this ).val() != null && jqry( this ).val().length > 0) {
        			monogramLocation = parseInt(monogramLocation) + 1;
        		}
        	});
        	jqry( "#monogramLocation" ).attr("name", "monogramLocation_"+monogramId+"_"+monogramLocation);
        	jqry( "#monogramColor" ).attr("name", "monogramColor_"+monogramId+"_"+monogramLocation);
        	jqry( "#monogramFont" ).attr("name", "monogramFont_"+monogramId+"_"+monogramLocation);

        	try {
        		var selectedOptions = [];
        		jqry( ".imprintLocation" ).each(function( outerIndex ) {
        			if(jqry( this ).val() != null && jqry( this ).val().length > 0) {
        				var selectedValue = jqry( this ).val().split("value_")[1];
        				
        				selectedOptions.push(jqry( this ).val().split("value_")[1]);
        				jqry( ".imprintLocation" ).each(function( innerIndex ) {
        					if(innerIndex > outerIndex) {
        						var selectIndex = jqry( this ).attr("name").split("_")[2];
        						var tobeRemovedOption = "Imprint Location "+selectIndex+"_value_"+selectedValue;
        						jqry(this).find('option').each(function() {
        							var selectedVal= jqry( this ).val().split("value_")[1]; 
        							if(jqry.inArray(selectedVal, selectedOptions) == -1 && jqry( this ).is(':disabled')) {
        								jqry( this ).removeAttr('disabled');
        							}
        							if(jqry(this).val() == tobeRemovedOption) {
        								jqry( this ).attr('disabled','disabled');
        							}
        						});
        					}
        				});
        				
        				jqry( "#monogramLocation" ).find('option').each(function() {
        					var selectedVal= jqry( this ).val(); 
        					if(jqry.inArray(selectedVal, selectedOptions) == -1 && jqry( this ).is(':disabled')) {
        						jqry( this ).removeAttr('disabled');
        					}
        					if(jqry(this).val() == selectedValue) {
        						jqry( this ).attr('disabled','disabled');
        					}
        				});
        			}
        			
        			for(var i = 0; i < jqry( ".imprintLocation" ).length; i++) {
        				if(i !=  outerIndex) {
        					var location = jqry( "#imprintLocation"+(i+1) ).val();
        					if(jqry( "#imprintLocation"+(i+1) ).val() != null) {
        						var val = jqry( "#imprintLocation"+(i+1) ).val().split("value_")[1];
        						var selectedValue = jqry( "#imprintLocation"+(outerIndex+1) ).val().split("value_")[1];
        						if(val == selectedValue) {
        							jqry( "#imprintLocation"+(i+1) ).val('');
        						}
        					}
        				}
        			}
        		});
        		
        	} catch(e) {}
        }
        function applyStyle() {
			if($('#monogramFont').val() != null ) {
				
				if($('#monogramFont').val() == 'Block Upper Case') {
				 	$('#monogramTextValueId').css("font-style", 'normal'); //normal|italic|oblique|initial|inherit
				 	$('#monogramTextValueId').css("font-family", 'Helvetica'); //Helvitica|Times New Roman
				 	$('#monogramTextValueId').css("font-weight", 'bold'); //normal|bold|bolder|lighter|number|initial|inherit
				 	$('#monogramTextValueId').css("text-transform", 'uppercase'); //none|capitalize|uppercase|lowercase|initial|inherit
				}
				else if($('#monogramFont').val() == 'Block Lower Case') {
					 $('#monogramTextValueId').css("font-style", 'normal'); //normal|italic|oblique|initial|inherit
					 $('#monogramTextValueId').css("font-family", 'Helvetica'); //Helvitica|Times New Roman
					 $('#monogramTextValueId').css("font-weight", 'bold'); //normal|bold|bolder|lighter|number|initial|inherit
					 $('#monogramTextValueId').css("text-transform", 'lowercase'); //none|capitalize|uppercase|lowercase|initial|inherit
				}
				else if($('#monogramFont').val() == 'Italic Upper Case') {
					 $('#monogramTextValueId').css("font-style", 'italic'); //normal|italic|oblique|initial|inherit
					 $('#monogramTextValueId').css("font-family", 'Times New Roman'); //Helvitica|Times New Roman
					 $('#monogramTextValueId').css("font-weight", 'bold'); //normal|bold|bolder|lighter|number|initial|inherit
					 $('#monogramTextValueId').css("text-transform", 'uppercase'); //none|capitalize|uppercase|lowercase|initial|inherit
				}
				else if($('#monogramFont').val() == 'Italic Lower Case') {
					 $('#monogramTextValueId').css("font-style", 'italic'); //normal|italic|oblique|initial|inherit
					 $('#monogramTextValueId').css("font-family", 'Times New Roman'); //Helvitica|Times New Roman
					 $('#monogramTextValueId').css("font-weight", 'bold'); //normal|bold|bolder|lighter|number|initial|inherit
					 $('#monogramTextValueId').css("text-transform", 'lowercase'); //none|capitalize|uppercase|lowercase|initial|inherit
				}
				else if($('#monogramFont').val() == 'Script') {
					 $('#monogramTextValueId').css("font-style", 'italic'); //normal|italic|oblique|initial|inherit
					 $('#monogramTextValueId').css("font-family", 'Brush Script MT'); //Helvitica|Times New Roman
					 $('#monogramTextValueId').css("font-weight", 'normal'); //normal|bold|bolder|lighter|number|initial|inherit
					 $('#monogramTextValueId').css("text-transform", 'none'); //none|capitalize|uppercase|lowercase|initial|inherit
				}
			}
		}
        function checkForm() {
            
            if(document.getElementById('monogramTextValueId') != null && document.getElementById('monogramLocation') != null &&
                    document.getElementById('monogramFont') != null && document.getElementById('monogramColor') != null) {
                var monogramText = document.getElementById('monogramTextValueId').value;
                var monogramLocation = document.getElementById('monogramLocation').value;
                var monogramFont = document.getElementById('monogramFont').value;
                var monogramColor = document.getElementById('monogramColor').value;
                
                if(monogramText != '' || monogramLocation != '' || monogramFont != '' || monogramColor != '') {
                    if(monogramText == '' || monogramLocation == '' || monogramFont == '' || monogramColor == '') {
                        alert('Please make sure to provide all the Optional Personalizations');
                        return false;
                    }
                }
            }
            var productList = document.getElementsByName('product.id');
            var allQtyEmpty = true;
            for(var i=0; i<productList.length; i++) {
                if(productList[i].value == '' || productList[i].value == null){
                    alert("Select Product");
                    return false;
                }
                
                var el = document.getElementById("quantity_"+productList[i].value);
                if ( el != null && el != ' ') {
                    try {
                        if(parseInt(el.value) > 0) {
                            allQtyEmpty = false;
                            break;
                        }
                    } catch(err) {
                        alert("invalid Quantity");
                        el.focus();
                        return false;
                    }
                }
            }
            if ( allQtyEmpty ) {
                alert("Please Enter Quantity.");
                return false;
            } else {
                try { 
                    if(document.getElementById("childSelectId") != null && document.getElementById("childSelectId").value == '') {
                        alert('Please select a Product.');
                        return false;
                    }
                    $('newSteps').set('style','background:url(/assets/Image/Layout/spinner.gif) no-repeat center 50%; background-color : #EEEEEE;');
                    if($('productOptionsId') != null) {
                        $('productOptionsId').fade('0.1');
                    }
                    $('addToCartId').set('style', 'display: none');
                    $('buttonContainer_waitMessage').set('style', 'background:#FCB144;color:#FFFFFF;font-weight:bold;padding:8px 2px 7px 14px;display:block;');
                    $('buttonContainer_waitMessage').set('text', 'Please Wait...We are calculating price');
                } catch(err) {
                      //Handle errors here
                }
                
                return true;    
            }
        }
        
        function locate(productId){
            window.location.replace(productId);
        }
        -->
        </script>
        <script type="text/javascript">
        <!--
        function updateChildProduct(productId){
            if(productId == null || productId == ''){
                return false;
            }
            $.ajax({
                url: "${_contextpath}/ajaxUpdateChildProduct2.jhtm?id="+productId+"&layout=39",
                success: function(data) {
                    var image_box = $('<div />').append(data).find('.details_image_box').html();
                    $('.details_image_box').html(image_box);
                    var image_thumbnails = $('<div />').append(data).find('.details_image_thumbnails').html();
                    $('.details_image_thumbnails').html(image_thumbnails);
                    var item_name = $('<div />').append(data).find('.details_item_name').html();
                    $('.details_item_name').html(item_name);
                    var sku = $('<div />').append(data).find('.details_sku').html();
                    $('.details_sku').html(sku);
                    var desc_wrapper = $('<div />').append(data).find('.details_desc_wrapper').html();
                    $('.details_desc_wrapper').html(desc_wrapper);
                    var child = $('<div />').append(data).find('.child').html();
                    $('.child').html(child);
                    var quantity = $('<div />').append(data).find('.quantity').html();
                    $('.quantity').html(quantity);
                    var hiddenProductId = $('<div />').append(data).find('.hiddenProductId').html();
                    $('.hiddenProductId').html(hiddenProductId);
                }
            })
        }
        function updateParentProduct(productId) {
            if(productId != null || productId != ''){
                window.location = "${_contextpath}/product.jhtm?id="+productId;
                window.navigate("${_contextpath}/product.jhtm?id="+productId); //works only with IE
            }
        }
        <c:if test="${siteConfig['PRODUCT_QUOTE'].value == 'true' and product.quote}" >
        function emailCheck(str){
             if(/^[a-z0-9._%-]+@[a-z0-9.-]+\.[a-z]{2,4}$/i.test(str) == false) {
                alert('Invalid Email');
                return false;
             }
             return true;
        }
        function assignValue(ele, value) {
        $(ele).value = value;
        }
        function phoneCheck(str){
            if (str.search(/^\d{10}$/)==-1) {
                alert("Please enter a valid 10 digit number");
                return false;
            } 
        }
        function saveContact(){
            try {
                if( !validateContactInfo() ) {
                    return;
                }
            }catch(err){}
            
            var sku = $('sku').value;
            var name = $('name').value;
            var firstName = $('firstName').value;
            var lastName = $('lastName').value;
            var company = $('company').value;
            var address = $('address').value;
            var city = $('city').value;
            var state = $('state').value;
            var zipCode = $('zipCode').value;
            var country = $('country').value;
            var email = $('email').value;
            var phone = $('phone').value;
            var note = $('note').value;
            var request = new Request({
               url: "${_contextpath}/insert-ajax-contact.jhtm?firstName="+firstName+"&lastName="+lastName+"&company="+company+"&address="+address+"&city="+city+"&state="+state+"&zipCode="+zipCode+"&country="+country+"&email="+email+"&phone="+phone+"&note="+note+"&sku="+sku+"&name="+name+"&leadSource=quote",
               method: 'post',
               onFailure: function(response){
                   saveContactBox.close();
                   $('successMessage').set('html', '<div style="color : RED; text-align : LEFT;">We are experincing problem in saving your quote. <br/> Please try again later.</div>');
               },
               onSuccess: function(response) {
                    saveContactBox.close();
                    $('mbQuote').style.display = 'none';
                    $('successMessage').set('html', response);
               }
            }).send();
        }
        </c:if>
        //-->
        </script>
        
<!-- Script End -->
        
<c:if test="${fn:trim(model.productLayout.headerHtml) != ''}">
  <c:set value="${model.productLayout.headerHtml}" var="headerHtml"/>
  <%@ include file="/WEB-INF/jsp/frontend/common/headerDynamicElement.jsp" %>
  <div><c:out value="${headerHtml}" escapeXml="false"/></div>
</c:if> 
  
<c:if test="${model.message != null}">
  <div class="message"><fmt:message key="${model.message}" /></div>
</c:if>

<c:if test="${model.product != null}">
<c:set value="${model.product}" var="product"/>

<%-- bread crumbs --%>
<c:import url="/WEB-INF/jsp/frontend/layout/${_template}/breadcrumbs.jsp" />

<div class="col-sm-12">
    <div id="product_details_2" class="row">
        <div class="col-sm-4">
            
            <!-- Image Start -->
            <div class="details_image_box">
            <c:forEach items="${model.product.images}" var="image" varStatus="status">
              <c:if test="${status.first}">
                <img src="<c:if test="${!image.absolute}">${_contextpath}/assets/Image/Product/detailsbig/</c:if><c:out value="${image.imageUrl}"/>" title="" class="cloudzoom img-responsive center-block" id="cloudzoom_1" data-cloudzoom="zoomImage:'<c:if test="${!image.absolute}">${_contextpath}/assets/Image/Product/detailsbig/</c:if><c:out value="${image.imageUrl}"/>', zoomSizeMode:'lens', captionPosition:'bottom', autoInside:500, touchStartDelay:100, lensWidth:150, lensHeight:150">
            </c:if>
            </c:forEach>
            </div>
            <div class="details_image_thumbnails">
                <ul class="clearfix">
                    <c:forEach items="${model.product.images}" var="image" varStatus="status">
                        <li>
                            <img src="<c:if test="${!image.absolute}">${_contextpath}/assets/Image/Product/thumb/</c:if><c:out value="${image.imageUrl}"/>" title="" class="img-responsive cloudzoom-gallery" data-cloudzoom="useZoom:'#cloudzoom_1', image:'<c:if test="${!image.absolute}">${_contextpath}/assets/Image/Product/detailsbig/</c:if><c:out value="${image.imageUrl}"/>', zoomImage:'<c:if test="${!image.absolute}">${_contextpath}/assets/Image/Product/detailsbig/</c:if><c:out value="${image.imageUrl}"/>'">
                        </li>
                    </c:forEach>
                </ul>
            </div>
            <!-- Image End -->
            
            
            
            <!-- Start Price -->
            <div class="pricesTableWrapper">
                <div class="pricesTable table-responsive">
                      <!-- If mulitple prices are available, show price grid.-->
                         <table id="asiPriceTable" class="table table-bordered">

                         <tbody>
                                     <c:choose>
                                          <c:when test="${fn:length(product.price) < 2}" >
                                          <tr class="hdrRow">
                           <th class="hdrColumn" colspan="2">Prices</th>
                          </tr>
                                          </c:when>
                                          <c:otherwise>
                                          <tr class="hdrRow">
                            <th class="hdrColumn" colspan="${fn:length(product.price)}">Prices</th>
                          </tr>
                                          </c:otherwise>
                                     </c:choose>

                      
                       <c:choose>
                           <c:when test="${fn:length(model.slavesList) gt 0}">
                            <c:forEach items="${model.slavesList}" var="childProduct" varStatus="priceGridStatus">
                           <c:if test="${priceGridStatus.first}">
                          
                              <c:choose>
                                <c:when test="${childProduct.priceSize < 2 and ! (fn:containsIgnoreCase(childProduct.minimumQty, ''))}">
                                    <tr><td class="value"><c:out value='${childProduct.minimumQty}' /></td></tr>
                                </c:when>
                                <c:otherwise>
                                  <c:forEach items="${childProduct.price}" var="price" varStatus="statusPrice">
                                  <c:if test="${(price.qtyFrom == null and ! (fn:containsIgnoreCase(childProduct.minimumQty, ''))) || price.qtyFrom != null}">
                                      <tr>
                                      <td class="value">
                                      <c:choose>
                                        <c:when test="${price.qtyFrom == null}"><c:out value='${childProduct.minimumQty}' /></c:when>
                                        <c:otherwise><c:out value="${price.qtyFrom}" /></c:otherwise>
                                      </c:choose>
                                      <c:choose>
                                        <c:when test="${price.qtyTo == null}"></c:when>
                                        <c:otherwise>-<c:out value="${price.qtyTo}" /></c:otherwise>
                                      </c:choose>
                                    </td>
                                    </tr>
                                  </c:if>
                                  </c:forEach>
                                </c:otherwise>
                              </c:choose>
                               </c:if>
                            <tr>
                              <th class="title">${childProduct.field4}</th>
                              <c:forEach items="${childProduct.price}" var="price" varStatus="statusPrice">
                                <c:choose>
                                  <c:when test="${childProduct.salesTag != null and childProduct.salesTag.discount != 0.0}">
                                    <td class="value"><fmt:message key="${siteConfig['CURRENCY'].value}" /><fmt:formatNumber value="${price.discountAmt}" pattern="#,##0.00" /></td>
                                  </c:when>
                                  <c:otherwise>
                                    <td class="value">
                                    <c:choose>
                                        <c:when test="${childProduct.productType == 'REW'}">
                                          <c:out value="${siteConfig['GROUP_REWARD_POINTS_TITLE'].value}" /><fmt:formatNumber value="${price.amt}" pattern="#,##0.00" />
                                        </c:when>
                                        <c:otherwise>
                                            <fmt:message key="${siteConfig['CURRENCY'].value}" /><fmt:formatNumber value="${price.amt}" pattern="#,##0.00" />
                                        </c:otherwise>
                                    </c:choose>
                                      <c:set var="lastPrice" value="${price.amt}"/>
                                    </td>
                                  </c:otherwise>
                                </c:choose>
                              </c:forEach>
                            </tr>
                            </c:forEach>
                           </c:when>
                           <c:otherwise>
                             <tr>
                              <td class="bold center"></td>
                              <c:choose>
                                <c:when test="${product.priceSize < 2}">
                                  <td class="value"> <c:out value='${product.minimumQty}' /></td>
                                </c:when>
                                <c:otherwise>
                                  <c:forEach items="${product.price}" var="price" varStatus="statusPrice">
                                    <td class="value">
                                      <c:choose>
                                        <c:when test="${price.qtyFrom == null}"><c:out value='${product.minimumQty}' /></c:when>
                                        <c:otherwise><c:out value="${price.qtyFrom}" /></c:otherwise>
                                      </c:choose>
                                      <c:choose>
                                        <c:when test="${price.qtyTo == null}"></c:when>
                                        <c:otherwise>-<c:out value="${price.qtyTo}" /></c:otherwise>
                                      </c:choose>
                                    </td>
                                  </c:forEach>
                                </c:otherwise>
                              </c:choose>
                               </tr>
                               <tr>
                                  <th class="title">${product.field4}</th>
                                   <c:forEach items="${product.price}" var="price" varStatus="statusPrice">
                                    <c:choose>
                                      <c:when test="${product.salesTag != null and product.salesTag.discount != 0.0}">
                                        <td class="value"><fmt:message key="${siteConfig['CURRENCY'].value}" /><fmt:formatNumber value="${price.discountAmt}" pattern="#,##0.00" /></td>
                                      </c:when>
                                      <c:otherwise>
                                        <td class="value">
                                          <fmt:message key="${siteConfig['CURRENCY'].value}" /><fmt:formatNumber value="${price.amt}" pattern="#,##0.00" />
                                          <c:set var="lastPrice" value="${price.amt}"/>
                                        </td>
                                      </c:otherwise>
                                    </c:choose>
                                  </c:forEach>
                               </tr>
                               
                           </c:otherwise>
                       </c:choose> 
                       </tbody>
                      </table>
              </div>
            </div>
            <!-- End Price -->
            
        </div>
        
        <div class="col-sm-8">
            <div class="details_desc clearfix">
                <div class="details_item_name"><h1><c:out value="${product.name}" escapeXml="false" /></h1></div>
                <div class="details_sku" id="details_sku_id">Product Number: &nbsp; <c:out value="${product.sku}" /></div>
                <br/>
                <c:if test="${siteConfig['PRODUCT_QUOTE'].value == 'true' and product.quote}">
                <c:choose>
                  <c:when test="${siteConfig['QUOTE_AS_ORDER'].value == 'true'}">
                  <script type="text/javascript">
                      function addToQuoteList(productId){
                            var requestHTMLData = new Request.HTML ({
                                url: "${_contextpath}/quoteList.jhtm?productId="+productId,
                                onSuccess: function(response){
                                    $('addToQuoteListWrapperId'+productId).set('html', '<h4><fmt:message key="f_addedToQuoteList" /></h4> <a href="${_contextpath}/quoteViewList.jhtm" rel="type:element" id="quoteListId" class="quoteList"> <input type="image" name="_viewQuoteList" class="_viewQuoteList" src="${_contextpath}/assets/Image/Layout/button_viewQuoteList.jpg" /> </a> ');
                                }
                            }).send();
                      }
                  </script>
                  <div class="addToQuoteListWrapper" id="addToQuoteListWrapperId${product.id}">
                    <input type="image" border="0" name="addtoquoteList" id="addtoquoteList" class="_addtoquoteList" value="${product.id}" src="${_contextpath}/assets/Image/Layout/button_addToQuoteList.jpg" onClick="addToQuoteList('${product.id}');" />     
                  </div>
                  </c:when>
                  <c:otherwise>
                      <link rel="stylesheet" href="${_contextpath}/assets/SmoothGallery/multiboxASI.css" type="text/css" media="screen" />
                    <c:if test="${!multibox}">
                     <script src="${_contextpath}/javascript/overlay.js" type="text/javascript" ></script>
                     <script src="${_contextpath}/javascript/multibox.js" type="text/javascript" ></script>
                    </c:if>
                    <script type="text/javascript">
                    window.addEvent('domready', function(){            
                        saveContactBox = new multiBox('mbQuote', {showControls : false, useOverlay: false, showNumbers: false });
                    });
                    </script>
                  <a href="#${product.id}" rel="type:element" id="mbQuote" class="mbQuote"><input type="image" border="0" name="_addtocart" class="_addtocart" src="${_contextpath}/assets/Image/Layout/button_addtoquote${_lang}.gif" /></a>
                  <div id="${product.id}" class="miniWindowWrapper mbPriceQuote" style="height:700px">
                    <form autocomplete="off" onsubmit="saveContact(); return false;">
                    <input type="hidden" value="${product.sku}" id="sku"/>
                    <input type="hidden" value="${product.name}" id="name"/>
                    <div class="header"><fmt:message key="f_description"/></div>
                    <fieldset class="top">
                        <label class="AStitle"><fmt:message key="firstName"/><span class="requiredFieldStar" id="requiredField_firstName"></span></label>
                        <input name="firstName" id="firstName" type="text" size="15" onkeyup="assignValue('firstName', this.value);"/>
                    </fieldset>
                    <fieldset class="bottom">
                        <label class="AStitle"><fmt:message key="lastName"/><span class="requiredFieldStar" id="requiredField_lastName"></span></label>
                        <input name="lastName" id="lastName" type="text" size="15" onkeyup="assignValue('lastName', this.value);"/>
                    </fieldset>
                    <fieldset class="bottom">
                        <label class="AStitle"><fmt:message key="company"/></label>
                        <input name="company" id="company" type="text" size="15" onkeyup="assignValue('company', this.value);"/>
                    </fieldset>
                    <fieldset class="bottom">
                        <label class="AStitle"><fmt:message key="address"/></label>
                        <input name="address" id="address" type="text" size="15" onkeyup="assignValue('address', this.value);"/>
                    </fieldset>
                    <fieldset class="bottom">
                        <label class="AStitle"><fmt:message key="city"/></label>
                        <input name="city" id="city" type="text" size="15" onkeyup="assignValue('city', this.value);"/>
                    </fieldset>
                    <fieldset class="bottom">
                        <label class="AStitle"><fmt:message key="state"/></label>
                        <input name="state" id="state" type="text" size="15" onkeyup="assignValue('state', this.value);"/>
                    </fieldset>
                    <fieldset class="bottom">
                        <label class="AStitle"><fmt:message key="zipCode"/></label>
                        <input name="zipCode" id="zipCode" type="text" size="15" onkeyup="assignValue('zipCode', this.value);"/>
                    </fieldset>
                    <fieldset class="bottom">
                        <label class="AStitle"><fmt:message key="country"/></label>
                        <input name="country" id="country" type="text" size="15" onkeyup="assignValue('country', this.value);"/>
                    </fieldset>
                    <fieldset class="bottom">
                        <label class="AStitle"><fmt:message key="email"/><span class="requiredFieldStar" id="requiredField_email"></span></label>
                        <input name="email" id="email" type="text" size="15" onkeyup="assignValue('email', this.value);"/>
                    </fieldset>
                    <fieldset class="bottom">
                        <label class="AStitle"><fmt:message key="phone"/><span class="requiredFieldStar" id="requiredField_phone"></span></label>
                        <input name="phone" id="phone" type="text" size="15" onkeyup="assignValue('phone', this.value);"/>
                    </fieldset>
                    <fieldset class="bottom">
                        <label class="AStitle"><fmt:message key="message"/></label>
                        <textarea name="note" id="note" onkeyup="assignValue('note', this.value);"></textarea>
                    </fieldset>
                    <fieldset>
                        <div id="button">
                            <input type="submit" value="Send"/>
                        </div>
                    </fieldset>
                    </form>
                  </div>
                  <div id="successMessage"></div>
                  </c:otherwise>
                </c:choose>
                </c:if>
                
                <!-- Description Start -->
                <c:if test="${(siteConfig['DETAILS_SHORT_DESC'].value == 'true' and product.shortDesc != '' and product.shortDesc != null) || product.longDesc != ''}">
                <div class="details_desc_wrapper">
                    <div class="details_desc_title">Product Description</div>
                    <div class="details_desc_content">
                        <c:if test="${siteConfig['DETAILS_SHORT_DESC'].value == 'true' and product.shortDesc != '' and product.shortDesc != null}">
                            <div class="details_short_desc"><c:out value="${product.shortDesc}" escapeXml="false" /></div>
                        </c:if>
                        <c:if test="${product.longDesc != ''}">
                            <div class="details_long_desc"><c:out value="${product.longDesc}" escapeXml="false" /></div>
                        </c:if>
                    </div>
                </div>
                </c:if>
                <!-- Description End -->
                <form action="${_contextpath}/addToCart.jhtm" method="post" onsubmit="return checkForm()" id="addToCartForm" enctype="multipart/form-data">
               
                <div class="hiddenProductId">
                <input type="hidden" value="${product.id}" name="product.id" id="product.id" />
                 </div>
                 <!-- Start Parent DropDown -->
                    <c:if test="${fn:length(model.siblingsList) gt 0}">
                    <div class="configBox_wrapper variant parent">
                    <div class="configBox_title">Select <c:out value="${product.field4}" escapeXml="false" /></div>
                      <div class="configbox_content">
                        <select name="childSelect" id="childSelectId" onchange="updateParentProduct(this.value)">
                          <option value="">Please Select</option>
                          <c:forEach items="${model.siblingsList}" var="slaveProduct" varStatus="statusPrice">
                              <c:if test="${(!empty slaveProduct.price or slaveProduct.priceByCustomer) and (!slaveProduct.loginRequire or userSession != null)}">
                              <c:if test="${(slaveProduct.type != 'box') and (empty slaveProduct.numCustomLines)}">
                                <c:if test="${!(gSiteConfig['gINVENTORY'] and slaveProduct.inventory != null and !slaveProduct.negInventory and slaveProduct.inventory <= 0)}">
                                  <c:set var="showAddToCart" value="true" />
                                  <option value="${slaveProduct.id}"  <c:if test="${product.id == slaveProduct.id}">selected="selected"</c:if>>
                                      <c:out value="${slaveProduct.field4}" escapeXml="false" />
                                   </option>
                                </c:if>
                              </c:if>
                            </c:if>
                          </c:forEach>
                        </select>
                      </div>
                   </div>
                 </c:if>
                    <!-- End Parent DropDown -->
                    
                    <!-- Start Child DropDown -->
                 <c:if test="${fn:length(model.slavesList) gt 0}">
                   <div class="configBox_wrapper variant child">
                    <div class="configBox_title">Select Product</div>
                    <div class="configbox_content">
                        <select name="childSelect" id="childSelectId" onchange="updateChildProduct(this.value)">
                          <option value="">Please Select</option>
                          <c:forEach items="${model.slavesList}" var="childProduct" varStatus="statusPrice">
                              <c:if test="${(!empty childProduct.price or childProduct.priceByCustomer) and (!childProduct.loginRequire or userSession != null)}">
                              <c:if test="${(childProduct.type != 'box') and (empty childProduct.numCustomLines)}">
                                <c:if test="${!(gSiteConfig['gINVENTORY'] and childProduct.inventory != null and !childProduct.negInventory and childProduct.inventory <= 0)}">
                                  <c:set var="showAddToCart" value="true" />
                                  <option value="${childProduct.id}"  <c:if test="${product.id == childProduct.id}">selected="selected"</c:if>>
                                      <c:out value="${childProduct.field4}" escapeXml="false" />
                                   </option>
                                </c:if>
                              </c:if>
                            </c:if>
                          </c:forEach>
                        </select>
                     </div>
                   </div>
                  </c:if>
                  <!-- End Child DropDown -->
                  
                  
                  
                  <!-- Qty Block -->
                   <div class="configBox_wrapper quantity">
                    <c:set value="0" var="childMinQty"/>
                    <c:if test="${fn:length(model.slavesList) gt 0}">
                       <c:forEach items="${model.slavesList}" var="childProduct" varStatus="productStatus1" >
                          <c:if test="${productStatus1.first}">
                           <c:set value="${childProduct.minimumQty}" var="childMinQty"/> 
                          </c:if>
                          <c:if test="${childProduct.minimumQty < childMinQty}">
                           <c:set value="${childProduct.minimumQty}" var="childMinQty"/> 
                          </c:if>
                       </c:forEach>
                    </c:if>
                    <div class="configBox_title">Enter Quantity <c:if test="${childMinQty >= 2}"><span class="headerMinQty">Min Qty: <c:out value="${childMinQty}" escapeXml="false" /></span></c:if></div>
                    <div class="configbox_content">
                        <div class="quantityList">
                            <div class="variantBlock sizeWrapperBox" id="selectedSizeBox_${product.field4}" >
                                 <div class="qtyInputBlock">
                                     <input type="text" autocomplete="off" size="5" maxlength="5" name="quantity_${product.id}" id="quantity_${product.id}" value="${product.minimumQty}" >
                                 </div>
                                   <c:if test="${product.inventory != null }">
                                       <div class="inStockTitle">In Stock</div>
                                       <div class="inStockValue"><c:out value="${product.inventory}"/></div>
                                   </c:if>
                            </div>
						<div style="clear: both;"></div>
                       </div>
                   </div>
                   </div>
                   <!-- End Quantity -->
                  
                    <c:if test="${model.storeImprintOptions != null and fn:length(model.storeImprintOptions) > 0}">
                       <c:import url="/WEB-INF/jsp/frontend/layout/template6/wildman/imprintOptions.jsp" />
                    </c:if>

                    <div class="addToCart_btn_wrapper">
                        <button type="submit" class="btn addToCart_btn">
                            Add To Cart &nbsp;<span class="addToCart_icon"></span>
                        </button>
                    </div>
                    
                </form>
                
            </div>
        </div>
    </div>
</div>

</c:if>

  </tiles:putAttribute>
</tiles:insertDefinition>