<%@ page session="false"%>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib uri="/WEB-INF/tlds/custom.tld" prefix="wj"%>

<tiles:insertDefinition name="${_template}" flush="true">
	<c:if test="${_leftBar != '1' and _leftBar != '4'}">
		<tiles:putAttribute name="leftbar" value="/WEB-INF/jsp/frontend/layout/template${_leftBar}/leftbar.jsp" />
	</c:if>
	<tiles:putAttribute name="content" type="string">
		<c:out value="${model.quoteListLayout.headerHtml}" escapeXml="false" />

		<div class="col-breadcrumb">
			<ul class="breadcrumb">
				<li><a href="${_contextpath}/account.jhtm"> <fmt:message key="myAccount" /> </a></li>
				<li class="active"><fmt:message key="quoteList" /></li>
			</ul>
		</div>


		<c:if test="${fn:trim(model.mylistLayout.headerHtml) != ''}">
			<c:set value="${model.mylistLayout.headerHtml}" var="headerHtml" />
			<c:set value="${fn:replace(headerHtml, '#groupName#', model.groupName)}" var="headerHtml" />
		</c:if>
		<c:out value="${headerHtml}" escapeXml="false" />

		<div class="col-sm-12">
			<div class="myQuoteListWrapper">
				<div class="headerTitle myQuoteList">
					<fmt:message key="quoteList" />
				</div>

				<div class="clearfix"></div>

				<c:choose>
					<c:when test="${model.successMessage != null}">
				  		<div class="message"><fmt:message key="${model.successMessage}"/></div>
					</c:when>	
					<c:when test="${model.products.nrOfElements == 0}">
						<div class="message">Your Quote List is empty.</div>
					</c:when>
					<c:otherwise>
						<c:forEach items="${model.groupProductsMap}" var="groupProductMap">
							<form name="mylist" method="post"
								action="${_contextpath}/quoteViewList.jhtm">
								<div class="myQuoteList_table">
									<div class="myQuoteListHdr">
										<div class="row clearfix">
											<div class="col-sm-12 col-md-1">
												<div class="listSelectHeader">
													<input type="checkbox" onclick="toggleAll(this)">
												</div>
											</div>
											<div class="col-sm-12 col-md-1">
												<div class="listImageHeader"></div>
											</div>
											<div class="col-sm-12 col-md-5">
												<div class="listSkuHeader">
													<fmt:message key="product" />
												</div>
											</div>
											<div class="col-sm-12 col-md-3">
												<div class="listPriceHeader">
													<fmt:message key="f_price" />
												</div>
											</div>
											<div class="col-sm-12 col-md-1">
											</div>
											<div class="col-sm-12 col-md-1">
												<div class="listQtyHeader">
													<fmt:message key="f_quantity" />
												</div>
											</div>
										</div>
									</div>

									<div class="myQuoteListDetails">
										<c:forEach items="${groupProductMap.value}" var="product" varStatus="status">
											
											<input type="hidden" value="${product.id}" name="product_Id" />	
											<input type="hidden" value="${product.sku}" name="productSku" />
											<input type="hidden" value="${product.name}" name="productName" />	
											<c:set var="statusClass" value="odd_row" />
											<c:if test="${(status.index + 1) % 2 == 0}">
												<c:set var="statusClass" value="even_row" />
											</c:if>
											<c:set var="productLink">${_contextpath}/product.jhtm?id=${product.id}</c:set>
											<c:if
												test="${gSiteConfig['gMOD_REWRITE'] == '1' and not fn:contains(product.sku, '/')}">
												<c:set var="productLink">${_contextpath}/${siteConfig['MOD_REWRITE_PRODUCT'].value}/${product.encodedSku}/${product.encodedName}.html</c:set>
											</c:if>


											<div class="${statusClass} clearfix">
												<div class="col-sm-12 col-md-1">
													<div class="listSelectHeader visible-xs visible-sm"></div>
													<div class="listSelect">
														<input type="checkbox" name="__selected_id" value="${product.id}">
													</div>
													<div class="clearfix"></div>
												</div>
												<div class="col-sm-12 col-md-1">
													<div class="listImageHeader visible-xs visible-sm">&nbsp;</div>
													<div class="listImageWrapper">
														<c:if test="${product.thumbnail != null}">
															<img class="listImage img-responsive" src="<c:if test="${!product.thumbnail.absolute}">${_contextpath}/assets/Image/Product/thumb/</c:if>${product.thumbnail.imageUrl}" border="0" alt="${product.alt}" />
															<br />
														</c:if>
													</div>
													<div class="clearfix"></div>
												</div>
												<div class="col-sm-12 col-md-5">
													<div class="listNameHeader visible-xs visible-sm">
														<fmt:message key="product" />
													</div>
													<div class="listName">
													<div class="listNameContent">
														<a href="${productLink}" class="list_item_name"><c:out value="${product.name}" escapeXml="false" /></a>
														<a href="${productLink}" class="list_item_sku"><c:out value="${product.sku}" escapeXml="false" /></a>
														<c:if test="${gSiteConfig['gADI']}"> 	
											          		<div class="list_item_brand"><c:out value="${product.manufactureName}" escapeXml="false"/></div>
													  	</c:if>
													</div>
													</div>
													
													<div class="clearfix"></div>
												</div>
												<div class="col-sm-12 col-md-3">
													<div class="listPriceHeader visible-xs visible-sm">
														<fmt:message key="f_price" />
													</div>
													<div class="listPrice">
														<%@ include file="/WEB-INF/jsp/frontend/layout/template6/price/price.jsp" %>
													</div>
													<div class="clearfix"></div>
												</div>
												<div class="col-sm-12 col-sm-1"></div>
												<div class="col-sm-12 col-md-1">
													<div class="listQtyHeader visible-xs visible-sm">
														<fmt:message key="f_quantity" />
													</div>
													<div class="listQty">
														<c:set var="productQuantity" value="quantity_${product.id}"></c:set>
											    		<c:set var="quantityAmt" value="tempQuantity_${id}"/>
												    	<input name="quantity_${product.id}" id="quantity_${product.id}" value="${model[quantityAmt]}" type="text" size="6" />    	
													</div>
													<div class="clearfix"></div>
												</div>
											</div>
										</c:forEach>
									</div>

									<div class="myQuoteListFtr">
										<div class="row clearfix">
											<div class="col-sm-12">
												<div class="deleteProducts_btn_wrapper">
													<button type="submit" name="__delete" class="deleteProducts_btn btn btn-link" onClick="return deleteSelected()">Delete Selected Products</button>
												</div>
											</div>
										</div>
									</div>
								</div>

								<div id="quoteWrapperId" class="quoteWrapper">

									<!-- Error Message -->
									<c:if test="${model.message != null}">
										<div class="message">
											<fmt:message key="${model.message}" />
										</div>
									</c:if>
									<!-- Success Message -->
									<c:if test="${model.successMessage != null}">
										<div class="message">
											<fmt:message key="${model.successMessage}" />
										</div>
									</c:if>
									<c:if test="${model.successMessage == null}">
										
										
										
										<div class="row">
										<div class="col-sm-6">
											<div id="contactInformationForm" class="form-horizontal">
												<div class="form-group">
													<div class="col-sm-12">
														<span class="headerLabel"><fmt:message key="f_quoteHeaderText" /></span>
													</div>
												</div>
												<div class="form-group">
													<div class="col-sm-4">
														<label for="customer_address_firstName" class="control-label">
															<fmt:message key="firstName" /> <sup class="requiredField">*</sup>
														</label>
													</div>
													<div class="col-sm-8">
													    <input type="text" class="form-control" id="customer_address_firstName" name="firstName" value="${model.customer.address.firstName}" size="25" />
													</div>
												</div>
												<div class="form-group">
													<div class="col-sm-4">
														<label for="customer_address_lastName" class="control-label">
															<fmt:message key="lastName" /> <sup class="requiredField">*</sup>
														</label>
													</div>
													<div class="col-sm-8">
													    <input type="text" class="form-control" id="customer_address_lastName" name="lastName" value="${model.customer.address.lastName}" size="25" />
													</div>
												</div>
												<div class="form-group">
													<div class="col-sm-4">
														<label for="customer_address_company" class="control-label">
															<fmt:message key="company" />
														</label>
													</div>
													<div class="col-sm-8">
													    <input type="text" class="form-control" id="customer_address_company" name="company" value="${model.customer.address.company}" size="25" />
													</div>
												</div>
												<div class="form-group">
													<div class="col-sm-4">
														<label for="customer_address_country" class="control-label">
															<fmt:message key="country" />
														</label>
													</div>
													<div class="col-sm-8">
														<select class="form-control" id="customer_address_country"  name="country" >
														  <c:forEach items="${model.countrylist}" var="country">
												  	        <option value="${country.code}" >${country.name}</option>
												 		  </c:forEach>
														</select>
							   						</div>
												</div>
												<div class="form-group">
													<div class="col-sm-4">
														<label for="customer_address_addr1" class="control-label">
															<fmt:message key="address" />
														</label>
													</div>
													<div class="col-sm-8">
													    <input type="text" class="form-control" id="customer_address_addr1" name="address1" value="${model.customer.address.addr1}" size="25" />
													</div>
												</div>
												<div class="form-group">
													<div class="col-sm-4">
														<label for="customer_address_city" class="control-label">
															<fmt:message key="city" />
														</label>
													</div>
													<div class="col-sm-8">
													    <input type="text" class="form-control" id="customer_address_city" name="city" value="${model.customer.address.addr1}" size="25" />
													</div>
												</div>
												<div class="form-group">
													<div class="col-sm-4">
														<label for="" class="control-label">
															<fmt:message key="stateProvince" />
														</label>
													</div>
													<div class="col-sm-8">
														<input type="text" class="form-control"  name="state" id="customer_address_stateProvince" value="${model.customer.address.stateProvince}" />
														<select class="form-control" name="state" id="customer_address_state">
															<c:forEach items="${model.statelist}" var="state">
													  	      <option value="${state.code}" >${state.name}</option>
															</c:forEach>      
														</select> 
														<select class="form-control" name="state" id="customer_address_ca_province">
														    <c:forEach items="${model.caProvinceList}" var="province">
													  	      <option value="${province.code}">${province.name}</option>
															</c:forEach>      
														</select>
													</div>
												</div>
												<div class="form-group">
													<div class="col-sm-4">
														<label for="customer_address_zip" class="control-label">
															 <fmt:message key="zipCode" />
														</label>
													</div>
													<div class="col-sm-8">
													    <input type="text" class="form-control" id="customer_address_zip" name="zipCode" value="${model.customer.address.zip}" size="25" />
													</div>
												</div>
												<div class="form-group">
													<div class="col-sm-4">
														<label for="customer_address_phone" class="control-label">
															<fmt:message key="phone" /> <sup class="requiredField">*</sup>
														</label>
													</div>
													<div class="col-sm-8">
													    <input type="text" class="form-control" id="customer_address_phone" name="phone" value="${model.customer.address.phone}" size="25" />
													</div>
												</div>
												<div class="form-group">
													<div class="col-sm-4">
														<label for="customer_username" class="control-label">
															<fmt:message key="email" /> <sup class="requiredField">*</sup>
														</label>
													</div>
													<div class="col-sm-8">
													    <input type="text" class="form-control" id="customer_username" name="email" value="${model.customer.username}" size="25" />
													</div>
												</div>
												<div class="form-group">
													<div class="col-sm-4">
														<label for="contact_message" class="control-label">
															<fmt:message key="note" /> <sup class="requiredField">*</sup>
														</label>
													</div>
													<div class="col-sm-8">
														<textarea class="form-control" id="contact_message" name="note">${model.note}</textarea>
													</div>
												</div>
												<div class="form-group">
													<div class="col-sm-12">
														<div id="send_btn_wrapper">
															<button type="submit" value="Send" name="send" class="btn btn-default">Send</button>
														</div>
													</div>
												</div>
											</div>
										</div>
									</div>

									</c:if>
								</div>
							</form>
						</c:forEach>
					</c:otherwise>
				</c:choose>
			</div>
		</div>

	<c:out value="${model.quoteListLayout.footerHtml}" escapeXml="false" />


<script type="text/javascript" language="JavaScript">
<!--
function toggleAll(el) {
	var ids = document.getElementsByName("__selected_id");	
  	if (ids.length == 1)
      document.mylist.__selected_id.checked = el.checked;
    else
      for (i = 0; i < ids.length; i++)
        document.mylist.__selected_id[i].checked = el.checked;	
} 
function deleteSelected() {
	var ids = document.getElementsByName("__selected_id");	
  	if (ids.length == 1) {
      if (document.mylist.__selected_id.checked) {
    	return true;
      }
    } else {
      for (i = 0; i < ids.length; i++) {
        if (document.mylist.__selected_id[i].checked) {
    	  return true;
        }
      }
    }
    alert("Please select product(s) to remove.");       
    return false;
}

function updateValue(id, value) {
	$('optionNVPair'+id).value = id+'_name_Notes_value_'+value;
}
//-->
</script>
	</tiles:putAttribute>
</tiles:insertDefinition>