<%@ page session="false" %>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ taglib uri="/WEB-INF/tlds/spring.tld" prefix="spring" %>
<%@ taglib uri="/WEB-INF/tlds/custom.tld" prefix="wj" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
	<tiles:insertDefinition name="${_template}" flush="true">
	  <c:if test="${_leftBar != '1' and _leftBar != '4'}">
	    <tiles:putAttribute name="leftbar" value="/WEB-INF/jsp/frontend/layout/template6/leftbar.jsp" />
	  </c:if>
	  <tiles:putAttribute name="content" type="string">
	  

	  		
		<!-- ========== PAGE BODY ========== -->
		<div id="pageBodyWrapper">
			<div id="pageBody" class="container">
				<div class="row row-offcanvas row-offcanvas-left">
					<!-- ========== CONTENT ========== -->
					<div id="contentWrapper">
						<div id="content">
							<div class="col-sm-12">
	
								<div class="invoiceWrapper">
									<script type="text/javascript">
										$(window).load(function() {
											$(window).resize(function(){
												var w=window, d=document, e=d.documentElement, g=d.getElementsByTagName('body')[0];
												var screen_width = w.innerWidth||e.clientWidth||g.clientWidth;
												var screen_height = w.innerHeight||e.clientHeight||g.clientHeight;

												if(screen_width > 767) {
													var boxes = $('.shippingInfoBox, .billingInfoBox, .invoiceInfoBox');
													boxes.css('height','auto');
													var boxes_maxHeight = Math.max.apply(null, boxes.map(function(){
														return $(this).height();
													}).get());
													boxes.height(boxes_maxHeight);
												}
												else {
													var boxes = $('.shippingInfoBox, .billingInfoBox, .invoiceInfoBox');
													boxes.css('height','auto');
												}
											}).trigger('resize');
										});
									</script>
									<div class="infoBoxesWrapper">
									
										<div class="row">
											<div class="col-sm-4">
												<div class="shippingInfoBox">
													<h3 class="shippingInfoTitle">Shipping Information</h3>
													<div class="shippingInfo">	
														<c:set value="${order.shipping}" var="address"/>
						    							<%@ include file="/WEB-INF/jsp/frontend/layout/template6/address.jsp" %>
													</div>
												</div>
											</div>
											<div class="col-sm-4">
												<div class="billingInfoBox">
													<h3 class="billingInfoTitle">Billing Information</h3>
													<div class="billingInfo">
														<c:set value="${order.billing}" var="address"/>
						    							<%@ include file="/WEB-INF/jsp/frontend/layout/template6/address.jsp" %>
													</div>
												</div>
											</div>
											<div class="col-sm-4">
												<div class="invoiceInfoBox">
													<h3 class="invoiceInfoTitle">Invoice Information <a href="javascript:window.print()"><img border="0" src="../graphics/printer.png"></a></h3>
													<div class="invoiceInfo">
														<div><strong>
														<c:choose>
														<c:when test="${quote != true}">
															<fmt:message key="f_order"/>
														</c:when>
														<c:otherwise>
															<fmt:message key="f_quote"/>
														</c:otherwise>
														</c:choose> #:
														</strong> 
														<c:out value="${order.orderId}"/>
														&nbsp;
														<c:if test="${order.purchaseOrder != '' and order.purchaseOrder != null}">
															(P.O. # ${order.purchaseOrder})
														</c:if>
														</div>
														<div><strong>Date:</strong> <fmt:formatDate type="date" timeStyle="default" value="${order.dateOrdered}"/></div>
														<div><strong>Delivery Date:</strong> <fmt:formatDate type="date" timeStyle="default" value="${order.dueDate}"/></div>
														<div><strong>Promo Code:</strong> ${order.promoCode}</div>
														<div><strong>Special Instructions:</strong> ${order.invoiceNote}</div>
														<div><strong><fmt:message key="status"/>: </strong><fmt:message key="orderStatus_${order.status}" /></div>
													</div>
												</div>
											</div>
										</div>
									</div>

									<div class="clearfix">
										<div class="invoiceTitle">Invoice</div>
									</div>

									<div class="invoice_table">
										<div class="invoiceHdr hidden-xs hidden-sm">
											<div class="row">
												<div class="col-sm-12 col-md-1">
													<div class="cartImageHeader">Image</div>
												</div>
												<div class="col-sm-12 col-md-3">
													<div class="cartNameHeader">Name</div>
												</div>
												<div class="col-sm-12 col-md-1">
													<div class="cartPackingHeader">Packing</div>
												</div>
												<div class="col-sm-12 col-md-1">
													<div class="cartWeightHeader">Weight</div>
												</div>
												<div class="col-sm-12 col-md-1">
													<div class="cartQtyHeader">Quantity</div>
												</div>
												<div class="col-sm-12 col-md-1">
													<div class="cartUnitsHeader"># Units</div>
												</div>
												<div class="col-sm-12 col-md-2">
													<div class="cartPriceHeader">Price/Lot</div>
												</div>
												<div class="col-sm-12 col-md-2">
													<div class="cartSubtotalHeader">Subtotal</div>
												</div>
											</div>
										</div>

										<div class="invoiceMobileHdr visible-xs visible-sm">
											<div class="row">
												<div class="col-sm-12">
													Cart Items
												</div>
											</div>
										</div>

										<div class="invoiceDetails">
											
											
											<c:forEach var="lineItem" items="${order.lineItems}" varStatus="lineItemStatus">
											
											
											<div 
											<c:choose>
												<c:when test="${lineItemStatus.index % 2 == 0}">
													class="odd_row clearfix"
												</c:when>
												<c:otherwise>
													class="even_row clearfix"
												</c:otherwise>
											</c:choose>
											>
												<div class="col-sm-12 col-md-1">
													<div class="cartImageHeader visible-xs visible-sm">Image</div>
													<div class="cartImageWrapper">
														<a href="#"><img class="cartImage img-responsive" src="<c:if test="${!lineItem.product.thumbnail.absolute}">assets/Image/Product/thumb/</c:if>${lineItem.product.thumbnail.imageUrl}" border="0" />
														</a>
													</div>
													<div class="clearfix"></div>
												</div>
												<div class="col-sm-12 col-md-3">
													<div class="cartNameHeader visible-xs visible-sm">Name</div>
													<div class="cartName">
														<div class="cartItemNameSkuWrapper">
															<a href="#" class="cart_item_name">${lineItem.product.name}</a>
															<br>
															<a href="#" class="cart_item_sku">
																<span class="sku_label">SKU:</span>
																${lineItem.product.sku}
															</a>
														</div>
													</div>
													<c:if test="${lineItem.includeRetailDisplay}">
														Include Retail Display
													</c:if>
													<div class="clearfix"></div>
												</div>
												<div class="col-sm-12 col-md-1">
													<div class="cartPackingHeader visible-xs visible-sm">Packing</div>
													<div class="cartPacking">Pallet</div>
													<div class="clearfix"></div>
												</div>
												<div class="col-sm-12 col-md-1">
													<div class="cartWeightHeader visible-xs visible-sm">Weight</div>
													<div class="cartWeight">${lineItem.product.weight} lbs</div>
													<div class="clearfix"></div>
												</div>
												<div class="col-sm-12 col-md-1">
													<div class="cartQtyHeader visible-xs visible-sm">Quantity</div>
													<div class="cartQty">${lineItem.quantity}</div>
													<div class="clearfix"></div>
												</div>
												<div class="col-sm-12 col-md-1">
													<div class="cartUnitsHeader visible-xs visible-sm"># Units</div>
													<div class="cartUnits">${lineItem.product.field7}</div>
													<div class="clearfix"></div>
												</div>
												<div class="col-sm-12 col-md-2">
													<div class="cartPriceHeader visible-xs visible-sm">Price/Lot</div>
													<div class="cartPrice"><fmt:message key="${siteConfig['CURRENCY'].value}" /><fmt:formatNumber value="${lineItem.unitPrice}" pattern="###,##0.00" />
													</div>
													<div class="clearfix"></div>
												</div>
												<div class="col-sm-12 col-md-2">
													<div class="cartSubtotalHeader visible-xs visible-sm">Subtotal</div>
													<div class="cartSubtotal"><fmt:message key="${siteConfig['CURRENCY'].value}" /><fmt:formatNumber value="${lineItem.unitPrice*lineItem.quantity}" pattern="###,##0.00" />
													</div>
													<div class="clearfix"></div>
												</div>
											</div>
											
										</c:forEach>
									
											
										</div>
										<div class="invoiceFtr">
											<div class="row">
												<div class="col-xs-4 col-sm-4 col-md-offset-7 col-md-3 right_bordered">
													<div class="subtotal_label">Subtotal</div>
												</div>
												<div class="col-xs-8 col-sm-8 col-md-2">
													<div class="subtotal_value">
													<fmt:message key="${siteConfig['CURRENCY'].value}" /><fmt:formatNumber value="${order.subTotal}" pattern="###,##0.00" />
													</div>
												</div>
											</div>
											
											
											<c:if test="${order.promo != null}">
											<div class="row">
												<div class="col-xs-4 col-sm-4 col-md-offset-7 col-md-3 right_bordered">
													<div class="shippingAndHandling_label">
													
													<c:choose>
        											<c:when test="${order.promoAmount == 0}"><fmt:message key="promoCode" /></c:when>
        											<c:otherwise><fmt:message key="discountForPromoCode" /></c:otherwise>
      												</c:choose>
      												${order.promo.title}
      												<c:choose>
          											<c:when test="${order.promoAmount == 0}"></c:when>
          											<c:when test="${order.promo.percent}">
            											(<fmt:formatNumber value="${order.promo.discount}" pattern="###,##0.00"/>%)
          											</c:when>
          											<c:otherwise>
            											(<fmt:message key="${siteConfig['CURRENCY'].value}" /><fmt:formatNumber value="${order.promo.discount}" pattern="###,##0.00"/>)
          											</c:otherwise>
      												</c:choose>
													
													</div>
												</div>
												<div class="col-xs-8 col-sm-8 col-md-2">
													<div class="shippingAndHandling_value">
														<c:choose>
        													<c:when test="${order.promoAmount == 0}">&nbsp;</c:when>
        													<c:otherwise>(<fmt:message key="${siteConfig['CURRENCY'].value}" /><fmt:formatNumber value="${order.promoAmount}" pattern="#,##0.00"/>)</c:otherwise>
      													</c:choose>
													</div>
												</div>
											</div>
											</c:if>
											
											<div class="row">
												<div class="col-xs-4 col-sm-4 col-md-offset-7 col-md-3 right_bordered">
													<div class="tax_label"><fmt:message key="tax" />:</div>
												</div>
												<div class="col-xs-8 col-sm-8 col-md-2">
													<div class="tax_value">
													<fmt:message key="${siteConfig['CURRENCY'].value}" /><fmt:formatNumber value="${order.tax}" pattern="###,##0.00" />
													</div>
												</div>
											</div>
											<div class="row">
												<div class="col-xs-4 col-sm-4 col-md-offset-7 col-md-3 right_bordered">
													<div class="shippingAndHandling_label"><fmt:message key="shippingHandling" /> (<c:out value="${order.shippingMethod}" escapeXml="false"/>):</div>
												</div>
												<div class="col-xs-8 col-sm-8 col-md-2">
													<div class="shippingAndHandling_value">
														<c:if test="${order.shippingCost!=null}">
															<fmt:message key="${siteConfig['CURRENCY'].value}" /><fmt:formatNumber value="${order.shippingCost}" pattern="###,##0.00"/>
														</c:if>
													</div>
												</div>
											</div>
											
											<c:if test="${order.ccFee != null and order.ccFee > 0}">
											<div class="row">
												<div class="col-xs-4 col-sm-4 col-md-offset-7 col-md-3 right_bordered">
													<div class="creditCardFee_label">Credit Card Fee (3%)</div>
												</div>
												<div class="col-xs-8 col-sm-8 col-md-2">
													<div class="creditCardFee_value">
														<fmt:message key="${siteConfig['CURRENCY'].value}" /><fmt:formatNumber value="${order.ccFee}" pattern="###,##0.00"/>
													</div>
												</div>
											</div>
  											</c:if>
											
											
											<div class="row">
												<div class="col-xs-4 col-sm-4 col-md-offset-7 col-md-3 right_bordered">
													<div class="grandTotal_label">GRAND TOTAL</div>
												</div>
												<div class="col-xs-8 col-sm-8 col-md-2">
													<div class="grandTotal_value">
													<fmt:message key="${siteConfig['CURRENCY'].value}" /><fmt:formatNumber value="${order.grandTotal}" pattern="###,##0.00" />
													</div>
												</div>
											</div>
										</div>
									</div>

									<div id="invoiceFooterWrapper">
										<div class="continueShoppingWrapper">
											<a href="home.jhtm" class="continueShopping"><fmt:message key="startNewShopping" /></a>
										</div>

										<p>You may print this page and keep it for your records.</p>
										<br>
										<p>You will receive an email within 2 workng hours (Monday - Friday) with the total for your order and payment instructions. Please note you can log in to your account anytime and <strong>view this or past invoices</strong>. Please refer to the <strong>confirmation email</strong> you received when you registered to obtain your account login and password. Thank you!</p>
									</div>

								</div>
							</div>
						</div>
					</div>
					<!-- ========== END CONTENT ========== -->
				</div>
			</div>
		</div>
		<!-- ========== END PAGE BODY ========== -->
	  
		</tiles:putAttribute>
	</tiles:insertDefinition>