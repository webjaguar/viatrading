<%@ page session="false" %>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="/WEB-INF/tlds/spring.tld" prefix="spring" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ page contentType="text/html; charset=ISO-8859-1" session="false" %>

<script src="/dv/w3data.js"></script>
<script type='text/javascript' src='/wp-includes/js/jquery/jquery.js?ver=1.12.4'></script>
<script type='text/javascript' src='/wp-content/themes/bridge/js/plugins.js?ver=4.6.1'></script>
<script type='text/javascript' src='/wp-content/themes/bridge/js/default.min.js?ver=4.6.1'></script>

<tiles:insertDefinition name="${_template}" flush="true">
  <c:if test="${_leftBar != '1' and _leftBar != '4'}">
    <tiles:putAttribute name="leftbar" value="/WEB-INF/jsp/frontend/layout/template${_leftBar}/leftbar.jsp" />
  </c:if>
  <tiles:putAttribute name="content" type="string">
  
<script type="text/javascript">

function automateCityState(zipCode) {
	var xmlhttp = new XMLHttpRequest();
	var url = "${_contextpath}/jsonZipCode.jhtm?zipCode="+zipCode;
	xmlhttp.onreadystatechange = function() {
	    if (xmlhttp.readyState == 4 && xmlhttp.status == 200) {
	    	var obj = JSON.parse(xmlhttp.responseText);   
	    	document.getElementById('customer_address_city').value = obj.city;
			document.getElementById('customer_address_state').value = obj.stateAbbv;  	
	    }
	}
	xmlhttp.open("GET", url, true);
	xmlhttp.send();
}
</script>
<script type="text/javascript">
function validatePhone() {
	var country = $("#customer_address_country").val();
	if(country  == 'United States' || country  == 'Canada'|| country  == 'US' || country  == 'CA'){
		$("#customer_address_phone").mask("(999)999-9999");	
	} else {
		$("#customer_address_phone").unmask("(999)999-9999");
	}
	
}

function validateCellPhone() {
	var country = $("#customer_address_country").val();
	if(country  == 'United States' || country  == 'Canada'|| country  == 'US' || country  == 'CA'){
	  $("#customer_address_cellPhone").mask("(999)999-9999");	
		
	} else{
		
		 $("#customer_address_cellPhone").unmask("(999)999-9999");	
	} 
	
	
}
var timeout = null;
function validateUsername(){
	
	 clearTimeout(timeout);
	 timeout = setTimeout(function () {
		 if (($("#customer_confirmUsername").val()) != ($("#customer_username").val())){
				$("#uname").attr('class', 'form-group has-feedback has-error');
				$("#usernameError").show();
			} else {
				$("#uname").attr('class', 'form-group');
				$("#usernameError").hide();
			}
	    }, 1000);
	
};

function validatePassword(){
	
	 clearTimeout(timeout);
	 timeout = setTimeout(function () {
		 if (($("#customer_password").val()) != ($("#confirm_password").val())){
				$("#passwd").attr('class', 'form-group has-feedback has-error');
				$("#pwdError").show();
			} else {
				$("#passwd").attr('class', 'form-group');
				$("#pwdError").hide();
			}
	    }, 1000);
	
};

//add star if only the country selected was USA
function selectCountry(e) {
	/* console.log(document.getElementById("customer_address_country")); */
	var index = document.getElementById("customer_address_country").selectedIndex;
	var option = document.getElementById("customer_address_country").options;
	var value = option[index];
	
	if(value.text !== "United States" && value.text !== "Canada" && value.text !== "Please Select") {
		document.getElementById("zipcodeStar").innerHTML = "";
	}else{
		document.getElementById("zipcodeStar").innerHTML = "*";

	}
}

window.onload = function () { selectCountry(); }

</script>


  
<c:if test="${!gSiteConfig['gREGISTRATION_DISABLED']}">

<c:out value="${model.createAccount.headerHtml}" escapeXml="false"/>

<spring:hasBindErrors name="customerForm">
<span class="error">Please fix all errors!</span>
</spring:hasBindErrors>

<div  class="col-sm-12">
  <div class="message"> <c:out value="${model.error}"></c:out> </div>
  <form:form commandName="customerForm" id="register_newCustomer_form" cssClass="form-horizontal" role="form" action="${_contextpath}/register.jhtm" method="post" enctype="multipart/form-data">
  <input type="hidden" name="_page" value="0">
  <form:hidden path="forwardAction" />
  
  <div class="row">
	  <div class="col-sm-12">
		<div class="requiredFieldLabel">* Required Field</div>
	  </div>
	</div>
	<div id="customerEmailAndPassword">
	  <div class="row">
		<div class="col-sm-6">
		  <div class="form-group">
			<div class="col-sm-12">
			  <h3><fmt:message key="emailAddressAndPassword" /></h3>
			</div>
		  </div>
		
		 <!--  
		 <spring:bind path="customer.registerCellPhone">
		  <div class="form-group <c:if test="${status.error}">has-feedback has-error</c:if>">
			<div class="col-sm-4">
			  <label for="register_CellPhone" class="control-label">
				<fmt:message key="registerCellPhone" /> <sup class="requiredField"></sup>
			  </label>
			</div>
		    <div class="col-sm-8">
			  <label for="register_CellPhone" class="control-label">
				<form:checkbox path="customer.registerCellPhone" value="true" onclick="validate()"/> (If you dont have an email address, you can register with your mobile phone number)
			  </label>
			</div>
		  </div>
		  </spring:bind>-->
		  
		  <spring:bind path="customer.username">
		  <div  class="form-group <c:if test="${status.error}">has-feedback has-error</c:if>">
			<div class="col-sm-4">
			  <label for="customer_username" class="control-label">
				<fmt:message key="emailAddress" /> <sup class="requiredField">*</sup>
			  </label>
			</div>
		    <div class="col-sm-8">
			  	<form:input autocomplete="off" path="customer.username" htmlEscape="true" id="customer_username" cssClass="form-control"/>
	  		  	<c:if test="${status.error}">
	  		  		<i class="form-control-feedback glyphicon glyphicon-remove"></i>
            		<small class="help-block error-message">
            			<form:errors path="customer.username" cssClass="error" />
            		</small>    
	  		  	</c:if>
	  		</div>
		  </div>
		  </spring:bind>
		  
		  <c:if test="${siteConfig['CONFIRM_USERNAME'].value == 'true'}">
		  <spring:bind path="confirmUsername">
		  <div id = "uname" class="form-group <c:if test="${status.error}">has-feedback has-error</c:if>">
			<div class="col-sm-4">
			  <label for="customer_username" class="control-label">
				<fmt:message key="confirmEmailAddress" /> <sup class="requiredField">*</sup>
			  </label>
			</div>
		    <div class="col-sm-8">
			  <form:input autocomplete="off" path="confirmUsername" htmlEscape="true" id="customer_confirmUsername" onkeyup="validateUsername()" cssClass="form-control"/>
	  		  <c:if test="${status.error}">
	  		  	<i class="form-control-feedback glyphicon glyphicon-remove"></i>
            	<small class="help-block error-message">
            		<form:errors path="confirmUsername" cssClass="error" />
            	</small>  
             
	  		  </c:if>
	  		  	<div id="usernameError" style="display :none">
            	<i class="form-control-feedback glyphicon glyphicon-remove"></i>
            	<small class="help-block error-message">MATCHING USERNAME REQUIRED
            	</small>
            	</div> 
    		</div>
		  </div>
		  </spring:bind>
		  </c:if>

		  <spring:bind path="customer.password">
		  <div  class="form-group <c:if test="${status.error}">has-feedback has-error</c:if>">
		  <div class="col-sm-4">
			  <label for="customer_password" class="control-label">
				<fmt:message key="password" /> <sup class="requiredField">*</sup>
			  </label>
			</div>
		    <div class="col-sm-8">
			  <form:password path="customer.password" htmlEscape="true" id="customer_password" cssClass="form-control" onblur="passwordAtLeastFiveCharacters();"/>
	  		  <c:if test="${status.error}">
	  		  	<i class="form-control-feedback glyphicon glyphicon-remove"></i>
            	<small class="help-block error-message">
		  		  <form:errors path="customer.password" cssClass="error" />
	        	</small>    
	  		  </c:if>
			</div>
		  </div>
		  </spring:bind>
		  
		  <spring:bind path="confirmPassword">
		  <div id ="passwd" class="form-group <c:if test="${status.error}">has-feedback has-error</c:if>">
		  <div class="col-sm-4">
			  <label for="confirm_password" class="control-label">
				<fmt:message key="confirmPassword" /> <sup class="requiredField">*</sup>
			  </label>
			</div>
			<div class="col-sm-8">
			  <form:password path="confirmPassword" htmlEscape="true" id="confirm_password" cssClass="form-control" onkeyup="validatePassword()"/>
	  		  <c:if test="${status.error}">
	  		  	<i class="form-control-feedback glyphicon glyphicon-remove"></i>
            	<small class="help-block error-message">
		  		  <form:errors path="confirmPassword" cssClass="error" />    
	        	</small>    
	  		  </c:if>
	  		  	<div id="pwdError" style="display :none">
            	<i class="form-control-feedback glyphicon glyphicon-remove"></i>
            	<small class="help-block error-message">MATCHING PASSWORDS REQUIRED
            	</small>
            	</div> 
			</div>
		  </div>
		  </spring:bind>
		</div>
		
		<div class="col-sm-6">
		  <div id="customerEmailAndPassword_right_wrapper">
		  </div>
		</div>
	  </div>
	</div>
	<div id="customerInformation">
	  <div class="row">
		<div class="col-sm-6">
		  <div class="form-group">
			<div class="col-sm-12">
			  <h3>Your Information</h3>
			</div>
		  </div>
		  
		  <spring:bind path="customer.address.firstName">
		  <div class="form-group <c:if test="${status.error}">has-feedback has-error</c:if>">
		  <div class="col-sm-4">
			  <label for="customer_address_firstName" class="control-label">
				<fmt:message key="firstName" /> <sup class="requiredField">*</sup>
			  </label>
			</div>
			<div class="col-sm-8">
			  <form:input autocomplete="off" path="customer.address.firstName" id="customer_address_firstName" htmlEscape="true" cssClass="form-control"/>
	  		  <c:if test="${status.error}">
	  		  	<i class="form-control-feedback glyphicon glyphicon-remove"></i>
            	<small class="help-block error-message">
		  		  <form:errors path="customer.address.firstName" cssClass="error" />    
	        	</small>    
	  		  </c:if>
			</div>
		  </div>
		  </spring:bind>
		  
		  <spring:bind path="customer.address.lastName">
		  <div class="form-group <c:if test="${status.error}">has-feedback has-error</c:if>">
		  <div class="col-sm-4">
			  <label for="customer_address_lastName" class="control-label">
				<fmt:message key="lastName" /> <sup class="requiredField">*</sup>
			  </label>
			</div>
		    <div class="col-sm-8">
			  <form:input autocomplete="off" path="customer.address.lastName" id="customer_address_lastName" htmlEscape="true" cssClass="form-control"/>
	  		  <c:if test="${status.error}">
	  		  	<i class="form-control-feedback glyphicon glyphicon-remove"></i>
            	<small class="help-block error-message">
		  		  <form:errors path="customer.address.lastName" cssClass="error" />    
	        	</small>    
	  		  </c:if>
			</div>
		  </div>
		  </spring:bind>
		  
		  	  <spring:bind path="customer.address.company">
		  <div class="form-group <c:if test="${status.error}">has-feedback has-error</c:if>">
		  <div class="col-sm-4">
			  <label for="customer_address_company" class="control-label">
				<fmt:message key="companyName" /> <c:if test="${siteConfig['CUSTOMERS_REQUIRED_COMPANY'].value == 'true'}"> <sup class="requiredField">*</sup> </c:if>
			  </label>
			</div>
			<div class="col-sm-8">
			  <form:input autocomplete="off" path="customer.address.company" id="customer_address_company"  htmlEscape="true" cssClass="form-control"/>
	  		  <c:if test="${status.error}">
	  		  	<i class="form-control-feedback glyphicon glyphicon-remove"></i>
            	<small class="help-block error-message">
		  		  <form:errors path="customer.address.company" cssClass="error" />    
	        	</small>    
	  		  </c:if>
			</div>
		  </div>
		  </spring:bind>
		 

  	  <spring:bind path="customer.address.addr1">
		  <div class="form-group <c:if test="${status.error}">has-feedback has-error</c:if>">
		  <div class="col-sm-4">
			  <label for="customer_address_addr1" class="control-label">
				<fmt:message key="address" /> 1 <sup class="requiredField">*</sup>
			  </label>
			</div>
			<div class="col-sm-8">
			  <form:input autocomplete="off" path="customer.address.addr1" onFocus="geolocate()" id="street_number" htmlEscape="true" cssClass="form-control"/>
	  		  <c:if test="${status.error}">
	  		  	<i class="form-control-feedback glyphicon glyphicon-remove"></i>
            	<small class="help-block error-message">
		  		  <form:errors path="customer.address.addr1" cssClass="error" />    
	        	</small>    
	  		  </c:if>
			</div>
		  </div>
		  </spring:bind>         
    
 
      
      <script>
      // This example displays an address form, using the autocomplete feature
      // of the Google Places API to help users fill in the information.

      // This example requires the Places library. Include the libraries=places
      // parameter when you first load the API. For example:
      // <script src="https://maps.googleapis.com/maps/api/js?key=YOUR_API_KEY&libraries=places">

      var placeSearch, autocomplete;
      var componentForm = {
        street_number: 'short_name',
        route: 'long_name',
        locality: 'long_name',
        administrative_area_level_1: 'short_name',
        country: 'long_name',
        postal_code: 'short_name'
      };

      function initAutocomplete() {
        // Create the autocomplete object, restricting the search to geographical
        // location types.
        autocomplete = new google.maps.places.Autocomplete(
            /** @type {!HTMLInputElement} */(document.getElementById('street_number')),
            {types: ['geocode']});

        // When the user selects an address from the dropdown, populate the address
        // fields in the form.
        autocomplete.addListener('place_changed', fillInAddress);
      }

      function fillInAddress() {
        // Get the place details from the autocomplete object.
        var place = autocomplete.getPlace();

        for (var component in componentForm) {
          document.getElementById(component).value = '';
          document.getElementById(component).disabled = false;
        }

        // Get each component of the address from the place details
        // and fill the corresponding field on the form.
        for (var i = 0; i < place.address_components.length; i++) {
          var addressType = place.address_components[i].types[0];
          if (componentForm[addressType]) {
            var val = place.address_components[i][componentForm[addressType]];
            document.getElementById(addressType).value = val;
          }
        }
       
        var state = document.getElementById("administrative_area_level_1");
        var ctry = document.getElementById("country");
        var US_state = document.getElementById("customer_address_state");
        var canada_state = document.getElementById("customer_address_ca_province");
        var other_state =  document.getElementById("customer_address_stateProvince");
         
        if(state.value != '' ){
        	if(ctry.value == 'United States'){
        		US_state.value = state.value;
        		
        	} else if(ctry.value == 'Canada'){
        		canada_state.value = state.value;
        	} else {
        		other_state.value = state.value;
        	}
        	
        }
        
        
        var CountryDropDown = document.getElementById('customer_address_country');
    	
    	for ( var i = 0; i < CountryDropDown.options.length; i++ ) {
    		  if ( CountryDropDown.options[i].text == ctry.value ) {
    			  CountryDropDown.options[i].selected = true;
    	           
    	        }
    	}
    	
    	$("#customer_address_country").change();

      }
      
      // Bias the autocomplete object to the user's geographical location,
      // as supplied by the browser's 'navigator.geolocation' object.
      function geolocate() {
        if (navigator.geolocation) {
          navigator.geolocation.getCurrentPosition(function(position) {
            var geolocation = {
              lat: position.coords.latitude,
              lng: position.coords.longitude
            };
            var circle = new google.maps.Circle({
              center: geolocation,
              radius: position.coords.accuracy
            });
            autocomplete.setBounds(circle.getBounds()); 
          });
        }
      }
      
    </script>
    
    
    <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAaH9bJSASDQAmiOQkoER4dLFaGzQiZ2nQ&libraries=places&callback=initAutocomplete"
        async defer></script>
		
			  
		  
	
		  
		 
	
		  
		  <spring:bind path="customer.address.addr2">
		  <div class="form-group <c:if test="${status.error}">has-feedback has-error</c:if>">
		  <div class="col-sm-4">
			  <label for="customer_address_addr2" class="control-label">
				<fmt:message key="address" /> 2
			  </label>
			</div>
			<div class="col-sm-8">
			  <form:input autocomplete="off" path="customer.address.addr2" id="route" htmlEscape="true" cssClass="form-control"/>
	  		  <c:if test="${status.error}">
	  		  	<i class="form-control-feedback glyphicon glyphicon-remove"></i>
            	<small class="help-block error-message">
		  		  <form:errors path="customer.address.addr2" cssClass="error" />    
	        	</small>    
	  		  </c:if>
			</div>
		  </div>
		  </spring:bind>
		  
		<spring:bind path="customer.address.zip">
		<div class="form-group <c:if test="${status.error}">has-feedback has-error</c:if>">
		  <div class="col-sm-4">
			<label for="customer_address_zip" class="control-label">
			  <fmt:message key="zipCode" /> <sup id="zipcodeStar" class="requiredField">*</sup>
			</label>
		  </div>
		  <div class="col-sm-8">
			<form:input autocomplete="off" id="postal_code" path="customer.address.zip" htmlEscape="true" cssClass="form-control" onkeyup="automateCityState(document.getElementById('customer_address_zip').value)"/>
	  		  <c:if test="${status.error}">
	  		  	<i class="form-control-feedback glyphicon glyphicon-remove"></i>
            	<small class="help-block error-message">
		    	  <form:errors path="customer.address.zip" cssClass="error" />    
				</small>    
	  		  </c:if>
    	  </div>
		</div>
		</spring:bind>
		  
		  <spring:bind path="customer.address.city">
		  <div class="form-group <c:if test="${status.error}">has-feedback has-error</c:if>">
		  <div class="col-sm-4">
			  <label for="customer_address_city" class="control-label">
				<fmt:message key="city" /> <sup class="requiredField">*</sup>
			  </label>
			</div>
		    <div class="col-sm-8">
			  <form:input autocomplete="off" path="customer.address.city" id="locality"  htmlEscape="true" cssClass="form-control"/>
	  		  <c:if test="${status.error}">
	  		  	<i class="form-control-feedback glyphicon glyphicon-remove"></i>
            	<small class="help-block error-message">
		  		  <form:errors path="customer.address.city" cssClass="error" />    
	        	</small>    
	  		  </c:if>
			</div>
		  </div>
		  </spring:bind>
		  
		  
		  <spring:bind path="customer.address.stateProvince">
		  <div class="form-group <c:if test="${status.error}">has-feedback has-error</c:if>">
		  <div class="col-sm-4">
			  <label for="" class="control-label">
				<fmt:message key="stateProvince" /> <sup class="requiredField">*</sup>
			  </label>
			</div>
			<div class="col-sm-8">
			  <form:select id="customer_address_state" class="form-control" path="customer.address.stateProvince" disabled="${customerForm.customer.address.country != 'US'}">
	            <form:option value="" label="Please Select"/>
	            <form:options items="${model.statelist}" itemValue="code" itemLabel="name"/>
	          </form:select>
	          <form:select id="customer_address_ca_province" path="customer.address.stateProvince" disabled="${customerForm.customer.address.country != 'CA'}">
	            <form:option value="" label="Please Select"/>
	            <form:options items="${model.caProvinceList}" itemValue="code" itemLabel="name"/>
	          </form:select>
	          <form:input autocomplete="off" id="customer_address_stateProvince" class="form-control" path="customer.address.stateProvince" htmlEscape="true" disabled="${customerForm.customer.address.country == 'US' or customerForm.customer.address.country == 'CA' or customerForm.customer.address.country == '' or customerForm.customer.address.country == null}"/>
	          <div id="stateProvinceNA" class="checkbox">
				<label>
				  <form:checkbox path="customer.address.stateProvinceNA" id="customer_address_stateProvinceNA" value="true"  disabled="${customerForm.customer.address.country == 'US' or customerForm.customer.address.country == 'CA' or customerForm.customer.address.country == '' or customerForm.customer.address.country == null}"/><fmt:message key="notApplicable"/>
       		    </label>
			  </div>
	  		  <c:if test="${status.error}">
	  		  	<i class="form-control-feedback glyphicon glyphicon-remove"></i>
            	<small class="help-block error-message">
				  <form:errors path="customer.address.stateProvince" cssClass="error" />
	        	</small>    
	  		  </c:if>
       	  </div>
		</div>
		</spring:bind>
		
		 <form:input type= "hidden" id="administrative_area_level_1" class="form-control" path=""/>
		
		  
		
	<!--   <form:input id ="country" type="hidden" path="" onkeyup="automateCityState(document.getElementById('customer_address_zip').value)"/> -->	
		 
		  <spring:bind path="customer.address.country">
		  <div class="form-group <c:if test="${status.error}">has-feedback has-error</c:if>">
		  <div class="col-sm-4">
			  <label for="customer_address_country" class="control-label">
				<fmt:message key="country" /> <sup class="requiredField">*</sup>
			  </label>
			</div>
			<div class="col-sm-8">
			 <form:select id="customer_address_country" class="form-control" path="customer.address.country" onchange="selectCountry()" >
		         <form:option value="" label="Please Select"/>
		        <form:options items="${model.countrylist}" itemValue="code" itemLabel="name"/>
		      </form:select>		       
		    		     
	  		  <c:if test="${status.error}">
	  		  	<i class="form-control-feedback glyphicon glyphicon-remove"></i>
            	<small class="help-block error-message">
			      <form:errors path="customer.address.country" cssClass="error" />      
	        	</small>    
	  		  </c:if>
		    </div>
		  </div>
		  </spring:bind>
		
		
					<form:input id="country" class="form-control" type="hidden" path=""/>
			
		
		
		<c:if test="${gSiteConfig['gWILDMAN_GROUP'] and !empty customerForm.accountLocationMap}">
   		  <spring:bind path="customer.accountNumber">
		  <div class="form-group <c:if test="${status.error}">has-feedback has-error</c:if>">
		  <div class="col-sm-4">
			  <label for="customer_address_addr2" class="control-label">
				<fmt:message key="location" /> <sup class="requiredField">*</sup>
			  </label>
			</div>
			<div class="col-sm-8">
			  <form:select path="customer.accountNumber">
	            <form:option value="" label="Please Select"/>
	            <c:forEach items="${customerForm.accountLocationMap}" var="accountLocation">         	
	          	  <option value="${accountLocation.value}" <c:if test="${customerForm.customer.accountNumber == accountLocation.value}">selected</c:if>>${accountLocation.key}</option>
	        	</c:forEach>
	          </form:select>
	          <c:if test="${status.error}">
            	<small class="help-block error-message">
				  <form:errors path="customer.accountNumber" cssClass="error" />
	        	</small>    
	  		  </c:if>
			</div>
		  </div>
		  </spring:bind>
 	    </c:if>
			
		<c:if test="${siteConfig['ADDRESS_RESIDENTIAL_COMMERCIAL'].value > 1}">
		<spring:bind path="customer.address.residential">
		<div class="form-group <c:if test="${status.error}">has-feedback has-error</c:if>">
		  <div class="col-sm-4">
			<label for="customer_address_zip" class="control-label">
			  <fmt:message key="deliveryType" />
			</label>
		  </div>
		  <div class="col-sm-8">
			<form:radiobutton path="customer.address.residential" value="true"/>: <fmt:message key="residential" /><br />
    		<form:radiobutton path="customer.address.residential" value="false"/>: <fmt:message key="commercial" /> 
    	  </div>
		</div>
		</spring:bind>
		
	    <c:if test="${siteConfig['SHIPPING_LTL'].value == 'true'}">
	      <spring:bind path="customer.address.liftGate">
		  <div class="form-group <c:if test="${status.error}">has-feedback has-error</c:if>">
		    <div class="col-sm-4">
			  <label for="customer_address_zip" class="control-label">
			    <fmt:message key="liftGateDelivery" />
			  </label>
		    </div>
		    <div class="col-sm-8">
			  <form:checkbox path="customer.address.liftGate" value="true"/><a href="${_contextpath}/category.jhtm?cid=307" onclick="window.open(this.href,'','width=600,height=300,resizable=yes'); return false;"><img class="toolTipImg" src="${_contextpath}/assets/Image/Layout/question.gif" border="0" /></a> 
	        </div>
		  </div>
		  </spring:bind>
		</c:if>
  		</c:if>
  
		<spring:bind path="customer.address.phone">
		<div class="form-group <c:if test="${status.error}">has-feedback has-error</c:if>">
		  <div class="col-sm-4">
			<label for="customer_address_phone" class="control-label">
			  <fmt:message key="phone" /> <sup class="requiredField">*</sup>
			</label>
		  </div>
		  <div class="col-sm-8">
			<form:input autocomplete="off" path="customer.address.phone" id="customer_address_phone"
			onkeyup="validatePhone();"
			htmlEscape="true" cssClass="form-control"/>
	  		  <c:if test="${status.error}">
	  		  	<i class="form-control-feedback glyphicon glyphicon-remove"></i>
            	<small class="help-block error-message">
			  		<form:errors path="customer.address.phone" cssClass="error" />    
				</small>    
	  		  </c:if>
    	  </div>
		</div>
		</spring:bind>
		
		<spring:bind path="customer.address.cellPhone">
		  <div class="form-group <c:if test="${status.error}">has-feedback has-error</c:if>">
		  <div class="col-sm-4">
			<label for="customer_address_cellPhone" class="control-label">
			  <fmt:message key="cellPhone" />
			</label>
		  </div>
		  <div class="col-sm-8">
			<form:input autocomplete="off" id="customer_address_cellPhone"  onkeyup="validateCellPhone();" class="form-control" path="customer.address.cellPhone" htmlEscape="true"/>
	  		  <c:if test="${status.error}">
	  		  	<i class="form-control-feedback glyphicon glyphicon-remove"></i>
            	<small class="help-block error-message">
			  		<form:errors path="customer.address.cellPhone" cssClass="error" />    
				</small>    
	  		  </c:if>
    	  </div>
		  </div>
		</spring:bind>
		
	  <spring:bind path="customer.address.mobileCarrierId">
		<div class="form-group <c:if test="${status.error}">has-feedback has-error</c:if>"">
			<div class="col-sm-4" id="mobileCarrierIdLabel">
				<label for="mobileCarrierId" class="control-label">
					<fmt:message key='mobileCarrier' />
				</label>
			</div>
			<div class="col-sm-8" id="mobileCarrierId" >	
				<form:select path="customer.address.mobileCarrierId" class="form-control">
   					<form:option value="" label="Please Select"/>
   					<form:options items="${model.mobileCarrierList}" itemValue="id" itemLabel="carrier"/>
   					</form:select>
   					  <c:if test="${status.error}">
			  		  	<i class="form-control-feedback glyphicon glyphicon-remove"></i>
		            	<small class="help-block error-message">
  					       <form:errors path="customer.address.mobileCarrierId" cssClass="error" />   		
			        	</small>    
	  		       </c:if>		
			 </div>
		 </div>
	  </spring:bind>
		
		<div class="form-group">
			<div class="col-sm-4" id="textMessageNotify">
				<label class="control-label">
					<fmt:message key="textMessageNotification" />
				</label>
			</div>
			<div class="col-sm-8" id="textMessageNotifyMsg">
				<div class="checkbox">
					<label>
						<form:checkbox path="customer.textMessageNotify"/>
					</label>
				</div>
				<small class="help-block"><strong><fmt:message key="disclaimer" />:</strong> 
				<i><fmt:message key="disclaimerExp" /></i></small>
			</div>
		</div>	
		
		<spring:bind path="customer.address.fax">
		<div class="form-group <c:if test="${status.error}">has-feedback has-error</c:if>">
		  <div class="col-sm-4">
			<label for="customer_address_fax" class="control-label">
			  <fmt:message key="fax" />
			</label>
		  </div>
		  <div class="col-sm-8">
			<form:input autocomplete="off" path="customer.address.fax" id="customer_address_fax" class="form-control" htmlEscape="true"/>
	  		  <c:if test="${status.error}">
	  		  	<i class="form-control-feedback glyphicon glyphicon-remove"></i>
            	<small class="help-block error-message">
			  		<form:errors path="customer.address.fax" cssClass="error" />    
				</small>    
	  		  </c:if>
    	  </div>
		</div>
		</spring:bind>
		
		<c:if test="${gSiteConfig['gI18N'] == '1'}">
		<spring:bind path="customer.languageCode">
		<div class="form-group <c:if test="${status.error}">has-feedback has-error</c:if>">
		  <div class="col-sm-4">
			<label for="customer_address_fax" class="control-label">
			  <fmt:message key="language" />
			</label>
		  </div>
		  <div class="col-sm-8">
		    <form:select path="customer.languageCode">
		      <form:option value="en"><fmt:message key="language_en"/></form:option>
		      <c:forEach items="${model.languageCodes}" var="i18n">         	
		       	<option value="${i18n.languageCode}" <c:if test="${customerForm.customer.languageCode == i18n.languageCode}">selected</c:if>><fmt:message key="language_${i18n.languageCode}"/></option>
		      </c:forEach>
			</form:select>
		  </div>
		</div>
		</spring:bind>
	    </c:if>
  		
  		<c:if test="${gSiteConfig['gMASS_EMAIL'] or siteConfig['CUSTOMER_MASS_EMAIL'].value == 'true'}">
	  	<spring:bind path="subscribeEmail">
		<div class="form-group <c:if test="${status.error}">has-feedback has-error</c:if>">
		  <div class="col-sm-12">
			<div id="subscribeCheckBoxId" class="checkbox">
			  <label><form:checkbox path="subscribeEmail"  />
			    <fmt:message key="f_subscribeMailingList" />
	     	  </label>
		    </div>
		  </div>
		</div>
	    </spring:bind>
	    </c:if>
	    
	    <c:if test="${customerForm.customerFields != null and siteConfig['CUSTOMER_FIELDS_ON_STEP1_REGISTRATION'].value == 'true'}">
          <c:forEach items="${customerForm.customerFields}" var="customerField">
            <spring:bind path="customer.field${customerField.id}">
		    <div class="form-group <c:if test="${status.error}">has-feedback has-error</c:if>">
		    <div class="col-sm-4">
				<label for="customer_address_fax" class="control-label">
				  <c:out value="${customerField.name}"/> <c:if test="${customerField.required}" ><div class="requiredField" align="right"> * </div></c:if>
  				</label>
			  </div>
			  <div class="col-sm-8">
			    <c:choose>
			      <c:when test="${!empty customerField.preValue}">
			        <form:select path="customer.field${customerField.id}" cssClass="form-control">
				      <form:option value="" label="Please Select"/>
			          <c:forTokens items="${customerField.preValue}" delims="," var="dropDownValue">
			            <form:option value="${dropDownValue}" />
			          </c:forTokens>
			        </form:select>
			      </c:when>
			      <c:otherwise>
			        <form:input autocomplete="off" path="customer.field${customerField.id}"  htmlEscape="true" maxlength="255" size="20" cssClass="form-control"/> 
			      </c:otherwise>
			    </c:choose>
		  		<c:if test="${status.error}">
		  		  	<i class="form-control-feedback glyphicon glyphicon-remove"></i>
	            	<small class="help-block error-message">
					    <form:errors path="customer.field${customerField.id}" cssClass="error" />
					</small>    
		  		</c:if>
    		  </div>
			</div>
			</spring:bind>
		  </c:forEach>
		</c:if>
	    
	    <c:if test="${siteConfig['NOTE_ON_REGISTRATION'].value == 'true'}">
		  <spring:bind path="customer.note">
		  <div class="form-group <c:if test="${status.error}">has-feedback has-error</c:if>">
		    <div class="col-sm-4">
			  <label for="customer_note" class="control-label">
			    <fmt:message key="note" />
			  </label>
		    </div>
		    <div class="col-sm-8">
		      <form:textarea path="customer.note" htmlEscape="true" cssClass="registrationNote"/>
		  		<c:if test="${status.error}">
		  		  	<i class="form-control-feedback glyphicon glyphicon-remove"></i>
	            	<small class="help-block error-message">
 					  <form:errors path="customer.note" cssClass="error" />  
					</small>    
		  		</c:if>
			  <div id="noteOnRegistration" class="note" style="color:#FF0000"><fmt:message key="noteOnRegistrationMessage" /></div>
		    </div>
		  </div>
		  </spring:bind>
		</c:if>
	    
	    
	  </div>
	    <div class="col-sm-6">
		  <div id="customerInformation_right_wrapper">
		  </div>
		</div>
	
	  </div>
	  
	 </div> 
	 
	 
	 <c:if test="${gSiteConfig['gTAX_EXEMPTION']}">
	    <div id="taxId">
		  <div class="row">
		    <div class="col-sm-12">
			  <div class="form-group">
			    <div class="col-sm-12">
				  <h3>Tax Information</h3>
			    </div>
			  </div>
			  <spring:bind path="customer.taxId">
		  	  <div class="form-group <c:if test="${status.error}">has-feedback has-error</c:if>">
			    <div class="col-sm-3">
				  <label for="customer_taxId" class="control-label">
				    <fmt:message key="f_taxId" /> <c:if test="${siteConfig['TAX_ID_REQUIRED'].value == 'true'}">*</c:if> :
				  </label>
			    </div>
			    <div class="col-sm-3">
				  <form:input autocomplete="off" path="customer.taxId" cssClass="form-control" id="customer_taxId" maxlength="30" htmlEscape="true"/>
		  		<c:if test="${status.error}">
		  		  	<i class="form-control-feedback glyphicon glyphicon-remove"></i>
	            	<small class="help-block error-message">
		  			  <form:errors path="customer.taxId" cssClass="error" />      
 					</small>    
		  		</c:if>
			    </div>
			  </div>
			  </spring:bind>
			  
			  <div class="form-group">
			    <div class="col-sm-12">
				  <span class="taxIdNote"><c:out value="${siteConfig['TAX_ID_NOTE'].value}" /></span>
			    </div>
			  </div>
		    </div>
		    <div class="col-sm-6"></div>
		  </div>
	    </div>
		</c:if>
	  
	  
	  <div id="form_buttons">
		<div class="row">
		  <div class="col-sm-12">
			<div id="buttons_wrapper">
			<c:choose>
			  <c:when test="${customerForm.customerFields != null and siteConfig['CUSTOMER_FIELDS_ON_STEP1_REGISTRATION'].value != 'true'}">
			    <button type="submit"name="_target1" class="btn btn-default" ><fmt:message key="nextStep" /></button>
			  </c:when>
			  <c:otherwise>
			    <button type="submit" name="_target2" class="btn btn-default"><fmt:message key="nextStep" /></button>
			  </c:otherwise>
			</c:choose>
			</div>
		  </div>
		</div>
	  </div>
	</form:form>
  </div>
</div>

<script language="JavaScript">
<!--
function toggleCellPhone() {
	document.getElementById('customer_address_cellPhone').value=document.getElementById('customer_username2').value;
}
//-->
</script>

<script language="JavaScript">
<!--
function toggleStateProvince(el) {
  if (el.value == "US") {
      document.getElementById('mobileCarrierId').disabled=false;
      document.getElementById('mobileCarrierId').style.display="table-row";
      document.getElementById('mobileCarrierIdLabel').disabled=false;
      document.getElementById('mobileCarrierIdLabel').style.display="table-row";
      document.getElementById('textMessageNotify').disabled=false;
      document.getElementById('textMessageNotify').style.display="table-row";
      document.getElementById('textMessageNotifyMsg').disabled=false;
      document.getElementById('textMessageNotifyMsg').style.display="table-row";
  } else if (el.value == "CA") {
      document.getElementById('mobileCarrierId').disabled=true;
      document.getElementById('mobileCarrierId').style.display="none";
      document.getElementById('mobileCarrierIdLabel').disabled=true;
      document.getElementById('mobileCarrierIdLabel').style.display="none";
      document.getElementById('textMessageNotify').disabled=true;
      document.getElementById('textMessageNotify').style.display="none";
      document.getElementById('textMessageNotifyMsg').disabled=true;
      document.getElementById('textMessageNotifyMsg').style.display="none";
  } else {
      document.getElementById('mobileCarrierId').disabled=true;
      document.getElementById('mobileCarrierId').style.display="none";
      document.getElementById('mobileCarrierIdLabel').disabled=true;
      document.getElementById('mobileCarrierIdLabel').style.display="none";
      document.getElementById('textMessageNotify').disabled=true;
      document.getElementById('textMessageNotify').style.display="none";
      document.getElementById('textMessageNotifyMsg').disabled=true;
      document.getElementById('textMessageNotifyMsg').style.display="none";
  }
}
toggleStateProvince(document.getElementById('country'));
//-->
</script>
<script type=text/javascript>
<!--
function validate(){
	var remember = document.getElementById('customer.registerCellPhone1');
	if (remember.checked == 1){
		document.getElementById('customer_username').disabled=true;
		document.getElementById('customer_confirmUsername').disabled=true;
	}else{
		document.getElementById('customer_username').disabled=false;
		document.getElementById('customer_confirmUsername').disabled=false;
	}
}
//-->



function passwordAtLeastFiveCharacters(e) {
	var inputPassword = document.getElementById("customer_password");
	 var error = inputPassword.parentNode.getElementsByClassName("error")[0];
	 
		if(inputPassword.value.length < 5 && typeof error === 'undefined' ) {
			var smallError = document.createElement('small');
			smallError.className = "error";
			smallError.innerHTML = " should be at least 5 characters!";
			smallError.style.color = "#a94442"; 
			inputPassword.parentNode.insertBefore(smallError, inputPassword.nextSibling);
			
		}else if(inputPassword.value.length >= 5) {
			var smallError = inputPassword.nextSibling;
			if(smallError !== null) {
				smallError.parentNode.removeChild(smallError); 
			}
	
		}
}



</script>


<c:out value="${model.createAccount.footerHtml}" escapeXml="false"/>


</c:if>
  </tiles:putAttribute>
</tiles:insertDefinition>