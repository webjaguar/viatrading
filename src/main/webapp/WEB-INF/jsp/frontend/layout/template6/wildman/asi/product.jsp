<%@ page session="false" %>
<%@ page import="java.util.*" %>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="/WEB-INF/tlds/spring.tld" prefix="spring" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ taglib uri="/WEB-INF/tlds/custom.tld" prefix="wj" %>
<!-- test -->
<tiles:insertDefinition name="${_template}" flush="true">
  <c:if test="${_leftBar != '1' and _leftBar != '4'}">
    <tiles:putAttribute name="leftbar" value="/WEB-INF/jsp/frontend/layout/template${_leftBar}/leftbar.jsp" />
  </c:if>
  <tiles:putAttribute name="content" type="string">
        <script src="//ajax.googleapis.com/ajax/libs/jqueryui/1.10.4/jquery-ui.min.js"></script>
        <script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1.7.2/jquery.min.js"></script>
        <link rel="stylesheet" href="//ajax.googleapis.com/ajax/libs/jqueryui/1.10.4/themes/smoothness/jquery-ui.css" />
        <c:if test="${siteConfig['CUSTOMER_GROUP_GALLERY'].value == 'true'}">
          <script type="text/javascript" src="${_contextpath}/javascript/drop-down-image.js"></script>
        </c:if>

        <script type="text/javascript">
        <!--
        try {
            $.noConflict();
        } catch (e) { }
        //-->
        </script>
        <script type="text/javascript">
            $(function(){
                $('#cloudzoom_1').bind('click',function(){
                    var cloudZoom = $(this).data('CloudZoom');
                    cloudZoom.closeZoom();
                    $.fancybox.open(cloudZoom.getGalleryList());
                    return false;
                });
            });
            CloudZoom.quickStart();
        </script>

        <script type="text/javascript">
        <!--
        var jqry = jQuery.noConflict();

        jqry(window).load(function(){
            jqry( ".imprintLocation" ).each(function( index ) {
                jqry( "#imprintLogoOptionsWrapper"+parseInt(index+2) ).slideUp( "fast", function(){
                    jqry( "#imprintLogoOptionsWrapper"+parseInt(index+2) ).css('overflow','visible');
                } );
            });
        });

        function updateMonogramText(text){
            jqry( "#monogramText" ).val("Monogram Text_value_"+text);
        }
        function loadLocation(){
            jqry( ".imprintLocation" ).each(function( index ) {
                if(jqry( this ).val() == null || jqry( this ).val().length == 0) {
                    if(jqry( "#imprintLogoOptionsWrapper"+parseInt(index+1) ).css('display') != 'none') {
                        alert('Please select location from available options first.');
                    } else {
                        jqry( "#imprintLogoOptionsWrapper"+parseInt(index+1) ).slideDown( "slow", function(){
                            jqry( "#imprintLogoOptionsWrapper"+parseInt(index+1) ).css('overflow','visible');
                        } );
                    }
                    return false;
                }
            });
        }
        function updateLocations(monogramId) {

        	var monogramLocation = 1;
        	jqry( ".imprintLocation" ).each(function( index ) {
        		if(jqry( this ).val() != null && jqry( this ).val().length > 0) {
        			monogramLocation = parseInt(monogramLocation) + 1;
        		}
        	});
        	jqry( "#monogramLocation" ).attr("name", "monogramLocation_"+monogramId+"_"+monogramLocation);
        	jqry( "#monogramColor" ).attr("name", "monogramColor_"+monogramId+"_"+monogramLocation);
        	jqry( "#monogramFont" ).attr("name", "monogramFont_"+monogramId+"_"+monogramLocation);

        	try {
        		var selectedOptions = [];
        		jqry( ".imprintLocation" ).each(function( outerIndex ) {
        			if(jqry( this ).val() != null && jqry( this ).val().length > 0) {
        				var selectedValue = jqry( this ).val().split("value_")[1];
        				
        				selectedOptions.push(jqry( this ).val().split("value_")[1]);
        				jqry( ".imprintLocation" ).each(function( innerIndex ) {
        					if(innerIndex > outerIndex) {
        						var selectIndex = jqry( this ).attr("name").split("_")[2];
        						var tobeRemovedOption = "Imprint Location "+selectIndex+"_value_"+selectedValue;
        						jqry(this).find('option').each(function() {
        							var selectedVal= jqry( this ).val().split("value_")[1]; 
        							if(jqry.inArray(selectedVal, selectedOptions) == -1 && jqry( this ).is(':disabled')) {
        								jqry( this ).removeAttr('disabled');
        							}
        							if(jqry(this).val() == tobeRemovedOption) {
        								jqry( this ).attr('disabled','disabled');
        							}
        						});
        					}
        				});
        				
        				jqry( "#monogramLocation" ).find('option').each(function() {
        					var selectedVal= jqry( this ).val(); 
        					if(jqry.inArray(selectedVal, selectedOptions) == -1 && jqry( this ).is(':disabled')) {
        						jqry( this ).removeAttr('disabled');
        					}
        					if(jqry(this).val() == selectedValue) {
        						jqry( this ).attr('disabled','disabled');
        					}
        				});
        			}
        			
        			for(var i = 0; i < jqry( ".imprintLocation" ).length; i++) {
        				if(i !=  outerIndex) {
        					var location = jqry( "#imprintLocation"+(i+1) ).val();
        					if(jqry( "#imprintLocation"+(i+1) ).val() != null) {
        						var val = jqry( "#imprintLocation"+(i+1) ).val().split("value_")[1];
        						var selectedValue = jqry( "#imprintLocation"+(outerIndex+1) ).val().split("value_")[1];
        						if(val == selectedValue) {
        							jqry( "#imprintLocation"+(i+1) ).val('');
        						}
        					}
        				}
        			}
        		});
        		
        	} catch(e) {}
        }
        function updateProductColor(color) {
            jqry("#selectedProductColor").val("Product color_value_"+color);

            jqry(".selected").each(function() {
                jqry(this).attr('class', 'colorBoxWrapper');
            });
            var selectedId = "selectedColorBox_"+color;
            jqry("[id='"+selectedId+"']").attr('class', 'colorBoxWrapper selected');
        }
        function checkForm() {

            if(document.getElementById('monogramTextValueId') != null && document.getElementById('monogramLocation') != null &&
                    document.getElementById('monogramFont') != null && document.getElementById('monogramColor') != null) {
                var monogramText = document.getElementById('monogramTextValueId').value;
                var monogramLocation = document.getElementById('monogramLocation').value;
                var monogramFont = document.getElementById('monogramFont').value;
                var monogramColor = document.getElementById('monogramColor').value;

                if(monogramText != '' || monogramLocation != '' || monogramFont != '' || monogramColor != '') {
                    if(monogramText == '' || monogramLocation == '' || monogramFont == '' || monogramColor == '') {
                        alert('Please make sure to provide all the Optional Personalizations');
                        return false;
                    }
                }
            }
            var productList = document.getElementsByName('product.id');
            var allQtyEmpty = true;
            for(var i=0; i<productList.length; i++) {
                if(productList[i].value == '' || productList[i].value == null){
                    alert("Select Product");
                    return false;
                }

                var el = document.getElementById("quantity_"+productList[i].value);
                if ( el != null && el != ' ') {
                    try {
                        if(parseInt(el.value) > 0) {
                            allQtyEmpty = false;
                            break;
                        }
                    } catch(err) {
                        alert("invalid Quantity");
                        el.focus();
                        return false;
                    }
                }
            }
            if ( allQtyEmpty ) {
                alert("Please Enter Quantity.");
                return false;
            } else {
                try {
$('newSteps').set('style','background:url(/assets/Image/Layout/spinner.gif) no-repeat center 50%; background-color : #EEEEEE;');
                    if($('productOptionsId') != null) {
                        $('productOptionsId').fade('0.1');
                    }
                    $('addToCartId').set('style', 'display: none');
                    $('buttonContainer_waitMessage').set('style', 'background:#FCB144;color:#FFFFFF;font-weight:bold;padding:8px 2px 7px 14px;display:block;');
                    $('buttonContainer_waitMessage').set('text', 'Please Wait...We are calculating price');
                } catch(err) {
                      //Handle errors here
                }

                return true;
            }
        }

        function locate(productId){
            window.location.replace(productId);
        }
        -->
        </script>
        <script type="text/javascript">
        <!--
        function updateChildProduct(productId){
            if(productId == null || productId == ''){
                return false;
            }
            $.ajax({
                url: "${_contextpath}/ajaxUpdateChildProduct2.jhtm?id="+productId+"&layout=39",
                success: function(data) {
                    var image_box = $('<div />').append(data).find('.details_image_box').html();
                    $('.details_image_box').html(image_box);
                    var image_thumbnails = $('<div />').append(data).find('.details_image_thumbnails').html();
$('.details_image_thumbnails').html(image_thumbnails);
                    var item_name = $('<div />').append(data).find('.details_item_name').html();
                    $('.details_item_name').html(item_name);
                    var sku = $('<div />').append(data).find('.details_sku').html();
                    $('.details_sku').html(sku);
                    var desc_wrapper = $('<div />').append(data).find('.details_desc_wrapper').html();
                    $('.details_desc_wrapper').html(desc_wrapper);
                    var child = $('<div />').append(data).find('.child').html();
                    $('.child').html(child);
                }
            })
        }
        function updateParentProduct(productId) {
            if(productId != null || productId != ''){
                window.location = "${_contextpath}/product.jhtm?id="+productId;
window.navigate("${_contextpath}/product.jhtm?id="+productId); //works only with IE
            }
        }
        <c:if test="${siteConfig['PRODUCT_QUOTE'].value == 'true' and product.quote}" >
        function emailCheck(str){
             if(/^[a-z0-9._%-]+@[a-z0-9.-]+\.[a-z]{2,4}$/i.test(str) == false) {
                alert('Invalid Email');
                return false;
             }
             return true;
        }
        function assignValue(ele, value) {
        $(ele).value = value;
        }
        function phoneCheck(str){
            if (str.search(/^\d{10}$/)==-1) {
                alert("Please enter a valid 10 digit number");
                return false;
            }
        }
        function saveContact(){
            try {
                if( !validateContactInfo() ) {
                    return;
                }
            }catch(err){}

            var sku = $('sku').value;
            var name = $('name').value;
            var firstName = $('firstName').value;
            var lastName = $('lastName').value;
            var company = $('company').value;
            var address = $('address').value;
            var city = $('city').value;
            var state = $('state').value;
            var zipCode = $('zipCode').value;
            var country = $('country').value;
            var email = $('email').value;
            var phone = $('phone').value;
            var note = $('note').value;
            var request = new Request({
               url: "${_contextpath}/insert-ajax-contact.jhtm?firstName="+firstName+"&lastName="+lastName+"&company="+company+"&address="+address+"&city="+city+"&state="+state+"&zipCode="+zipCode+"&country="+country+"&email="+email+"&phone="+phone+"&note="+note+"&sku="+sku+"&name="+name+"&leadSource=quote",
               method: 'post',
               onFailure: function(response){
                   saveContactBox.close();
                   $('successMessage').set('html', '<div style="color : RED; text-align : LEFT;">We are experincing problem in saving your quote. <br/> Please try again later.</div>');
               },
               onSuccess: function(response) {
                    saveContactBox.close();
                    $('mbQuote').style.display = 'none';
                    $('successMessage').set('html', response);
               }
            }).send();
        }
        </c:if>
        //-->
        </script>

<!-- Script End -->

<c:if test="${fn:trim(model.productLayout.headerHtml) != ''}">
  <c:set value="${model.productLayout.headerHtml}" var="headerHtml"/>
  <%@ include file="/WEB-INF/jsp/frontend/common/headerDynamicElement.jsp" %>
  <div><c:out value="${headerHtml}" escapeXml="false"/></div>
</c:if>

<c:if test="${model.message != null}">
  <div class="message"><fmt:message key="${model.message}" /></div>
</c:if>

<c:if test="${model.product != null}">
<c:set value="${model.product}" var="product"/>

<%-- bread crumbs --%>
<c:import url="/WEB-INF/jsp/frontend/layout/${_template}/breadcrumbs.jsp" />

<div class="col-sm-12">
    <div id="product_details_2" class="row">
        <div class="col-sm-4">

            <!-- Image Start -->
            <div class="details_image_box">
            <c:forEach items="${model.product.images}" var="image" varStatus="status">
              <c:if test="${status.first}">
                <img src="<c:out value="${fn:replace(product.thumbnail.imageUrl, 'size=large', 'size=normal')}"/>" title="" class="cloudzoom img-responsive center-block" id="cloudzoom_1" data-cloudzoom="zoomImage:'${image.imageUrl}', zoomSizeMode:'lens', captionPosition:'bottom', autoInside:500, touchStartDelay:100, lensWidth:150, lensHeight:150">
            </c:if>
            </c:forEach>
            </div>
            <div class="details_image_thumbnails">
                <ul class="clearfix">
                    <c:forEach items="${model.product.images}" var="image" varStatus="status">
                        <li>
                            <img src="<c:out value="${fn:replace(image.imageUrl, 'size=large', 'size=normal')}"/>" title="" class="img-responsive cloudzoom-gallery" data-cloudzoom="useZoom:'#cloudzoom_1', image:'<c:out value="${fn:replace(image.imageUrl, 'size=large', 'size=normal')}"/>', zoomImage:'${image.imageUrl}'">
                        </li>
                    </c:forEach>
                </ul>
            </div>
            <!-- Image End -->


        <c:set var="displayQuote" value="false" />

            <!-- Start Price -->
            <div class="pricesTableWrapper">
                <div class="pricesTable table-responsive">
                    <c:choose>
                    <c:when test="${model.product.asiIgnorePrice}">
                          <table id="asiPriceTable" class="table table-bordered">
                          <tbody>
                        <c:if test="${model.samePriceForAllTiers == null || !model.samePriceForAllTiers}">
                          <tr>
                            <th class="title"><fmt:message key="quantity" /></th>
                            <c:forEach items="${model.product.price}" var="price" varStatus="priceStatus">
                              <td class="value"><c:out value="${price.qtyFrom}" /></td>
                            </c:forEach>
                          </tr>
                        </c:if>
                        <tr>
                          <th class="title"><fmt:message key="price" /></th>
                          <c:choose>
                            <c:when test="${model.samePriceForAllTiers == null || !model.samePriceForAllTiers}">
                              <c:forEach items="${model.product.price}" var="price" varStatus="priceStatus">
                                <td class="value" <c:if test="${product.salesTag != null and product.salesTag.discount != 0.0}" >style="text-decoration: line-through"</c:if> >
                                  <fmt:message key="${siteConfig['CURRENCY'].value}" /><fmt:formatNumber value="${price.amt}" pattern="#,##0.00#" maxFractionDigits="${siteConfig['MAX_FRACTION_DIGIT'].value}" />
                                </td>
                              </c:forEach>
                            </c:when>
                            <c:otherwise>
                              <td class="value" <c:if test="${product.salesTag != null and product.salesTag.discount != 0.0}" >style="text-decoration: line-through"</c:if> >
                                <fmt:message key="${siteConfig['CURRENCY'].value}" /><fmt:formatNumber value="${model.product.price[0].amt}" pattern="#,##0.00#" maxFractionDigits="${siteConfig['MAX_FRACTION_DIGIT'].value}" />
                              </td>
                            </c:otherwise>
                          </c:choose>
                        </tr>
                        <c:if test="${model.product.salesTag != null and model.product.salesTag.discount != 0.0}">
                          <tr>
                            <th class="title"><fmt:message key="f_youPay" /></th>
                          <c:choose>
                            <c:when test="${model.samePriceForAllTiers == null || !model.samePriceForAllTiers}">
                              <c:forEach items="${model.product.price}" var="price" varStatus="priceStatus">
                                <td class="value">
                                  <c:choose>
                                    <c:when test="${product.salesTag.percent}">
                                      <fmt:message key="${siteConfig['CURRENCY'].value}" /><fmt:formatNumber value="${price.amt *( (100 - product.salesTag.discount) / 100)}" pattern="#,##0.00" maxFractionDigits="2" />
                                    </c:when>
                                    <c:otherwise>
                                      <fmt:message key="${siteConfig['CURRENCY'].value}" /><fmt:formatNumber value="${price.amt - product.salesTag.discount}" pattern="#,##0.00" maxFractionDigits="2" />
                                    </c:otherwise>
                                  </c:choose>
                                  </td>
                                </c:forEach>
                            </c:when>
                            <c:otherwise>
                              <td class="value">
                                  <c:choose>
                                    <c:when test="${product.salesTag.percent}">
                                      <fmt:message key="${siteConfig['CURRENCY'].value}" /><fmt:formatNumber value="${model.product.price.amt *( (100 - product.salesTag.discount) / 100)}" pattern="#,##0.00" maxFractionDigits="2" />
                                    </c:when>
                                    <c:otherwise>
                                      <fmt:message key="${siteConfig['CURRENCY'].value}" /><fmt:formatNumber value="${model.product.price.amt - product.salesTag.discount}" pattern="#,##0.00" maxFractionDigits="2" />
                                    </c:otherwise>
                                  </c:choose>
                                </td>
                            </c:otherwise>
                          </c:choose>
                          </tr>
                        </c:if>
                        </tbody>
                      </table>
                    </c:when>
                    <c:otherwise>
                      <c:choose>
                        <c:when test="${model.asiProduct.variants != null && fn:length(model.asiProduct.variants.variant) > 0}">
                        <!-- Products with variants-->
                                 <table id="asiPriceTable" class="table table-bordered">
                                 <tbody>


                            <c:choose>
                                          <c:when test="${fn:length(model.asiProduct.variants.variant[0].prices.price) + 1 < 2}" >
                                          <tr class="hdrRow">
                           <th class="hdrColumn" colspan="2">Prices</th>
                          </tr>
                                          </c:when>
                                          <c:otherwise>
                                          <tr class="hdrRow">
                            <th class="hdrColumn" colspan="${fn:length(model.asiProduct.variants.variant[0].prices.price) + 1}">Prices</th>
                          </tr>
                                          </c:otherwise>
                                 </c:choose>



                            <c:forEach items="${model.asiProduct.variants.variant}" var="variant" varStatus="variantStatus">

                             <c:if test="${model.variantsSelectionList != null and fn:contains(model.variantsSelectionList, variant.description)}">

                              <c:if test="${variantStatus.index == 0 && (model.samePriceForAllTiers == null || !model.samePriceForAllTiers)  }">
                              <tr>
                                <th class="title"></th>
                                <c:forEach items="${variant.prices.price}" var="price" varStatus="priceStatus">
                                  <td class="value"><c:out value="${variant.prices.price[priceStatus.index].quantity.range.from}" /></td>
                                </c:forEach>
                              </tr>
                              </c:if>
                              <tr>
                                <th class="title"><c:out value="${variant.description}"  escapeXml="false" /></th>
                                <c:choose>
                                  <c:when test="${fn:length(variant.prices.price) == 0 || variant.prices.price[0].price == null}">
                                    <c:set var="displayQuote" value="true" />
                                    <td class="value">
                                         <input type="image" border="0" onclick="return addToQuoteList('${product.id}');" name="_addtocart" class="_addtocart" src="${_contextpath}/assets/Image/Layout/button_requestQuote${_lang}.gif" />
                                     </td>
                                  </c:when>
                                  <c:when test="${model.samePriceForAllTiers == null || !model.samePriceForAllTiers}">
                                    <c:forEach items="${variant.prices.price}" var="price" varStatus="priceStatus">
                                    <td class="value"  <c:if test="${(product.salesTag != null and product.salesTag.discount != 0.0) or (siteConfig.get['ASI_PRICE_MARK_UP_CHANGE_BASE_PRICE'].value != 'true' and model.asiProduct2 != null)}" >style="text-decoration: line-through"</c:if> >
                                      <fmt:message key="${siteConfig['CURRENCY'].value}" /><fmt:formatNumber value="${variant.prices.price[priceStatus.index].price}" pattern="#,##0.00#" maxFractionDigits="${siteConfig['MAX_FRACTION_DIGIT'].value}" />
                                    </td>
                                    </c:forEach>
                                  </c:when>
                                  <c:otherwise>
                                          <c:choose>
                                            <c:when test="${model.product.productType == 'REW'}">
                                             <td class="value">
                                              <c:out value="${siteConfig['GROUP_REWARD_POINTS_TITLE'].value}" /> <fmt:formatNumber value="${variant.prices.price[0].price}" pattern="#,##0.00#" maxFractionDigits="${siteConfig['MAX_FRACTION_DIGIT'].value}" />
                                             </td>
                                            </c:when>
                                            <c:otherwise>
                                              <td class="value" <c:if test="${(product.salesTag != null and product.salesTag.discount != 0.0) or (siteConfig.get['ASI_PRICE_MARK_UP_CHANGE_BASE_PRICE'].value != 'true' and model.asiProduct2 != null)}" >style="text-decoration: line-through"</c:if> >
                                               <fmt:message key="${siteConfig['CURRENCY'].value}" /><fmt:formatNumber value="${variant.prices.price[0].price}" pattern="#,##0.00#" maxFractionDigits="${siteConfig['MAX_FRACTION_DIGIT'].value}" />
                                             </td>
                                            </c:otherwise>
                                        </c:choose>
                                 </c:otherwise>
                                </c:choose>
                               </tr>
                              <c:choose>
                                <c:when test="${product.salesTag != null and product.salesTag.discount != 0.0}">
                                    <c:forEach items="${model.asiProduct.variants.variant}" var="variant2" varStatus="variant2Status">
                                      <c:if test="${variant2Status.index == variantStatus.index}">
                                          <tr>
                                          <th class="title"><fmt:message key="f_youPay" /></th>
                                        <c:choose>
                                            <c:when test="${fn:length(variant.prices.price) == 0 || variant.prices.price[0].price == null}">
                                              <c:set var="displayQuote" value="true" />
                                            <td class="value">
                                                <input type="image" border="0" onclick="return addToQuoteList('${product.id}');"  name="_addtocart" class="_addtocart" src="${_contextpath}/assets/Image/Layout/button_requestQuote${_lang}.gif" />
                                             </td>
                                          </c:when>
                                            <c:when test="${model.samePriceForAllTiers == null || !model.samePriceForAllTiers}">
                                               <c:forEach items="${variant2.prices.price}" var="price" varStatus="priceStatus">
                                              <td class="value">
                                                <c:choose>
                                                    <c:when test="${product.salesTag.percent}">
                                                    <fmt:message key="${siteConfig['CURRENCY'].value}" /><fmt:formatNumber value="${variant2.prices.price[priceStatus.index].price *( (100 - product.salesTag.discount) / 100)}" pattern="#,##0.00" maxFractionDigits="2" />
                                                    </c:when>
<c:otherwise>
                                                    <fmt:message key="${siteConfig['CURRENCY'].value}" /><fmt:formatNumber value="${variant2.prices.price[priceStatus.index].price - product.salesTag.discount}" pattern="#,##0.00" maxFractionDigits="2" />
</c:otherwise>
                                                </c:choose>
                                                </td>
                                              </c:forEach>
                                             </c:when>
                                             <c:otherwise>
                                             <td class="value">
                                              <c:choose>
                                                  <c:when test="${product.salesTag.percent}">
                                                  <fmt:message key="${siteConfig['CURRENCY'].value}" /><fmt:formatNumber value="${variant2.prices.price[0].price *( (100 - product.salesTag.discount) / 100)}" pattern="#,##0.00" maxFractionDigits="2" />
                                                  </c:when>
<c:otherwise>
                                                  <fmt:message key="${siteConfig['CURRENCY'].value}" /><fmt:formatNumber value="${variant2.prices.price[0].price - product.salesTag.discount}" pattern="#,##0.00" maxFractionDigits="2" />
</c:otherwise>
                                              </c:choose>
                                              </td>
                                            </c:otherwise>
                                        </c:choose>
                                        </tr>
                                    </c:if>
                                  </c:forEach>
                                </c:when>
                                <c:when test="${siteConfig.get['ASI_PRICE_MARK_UP_CHANGE_BASE_PRICE'].value != 'true' and model.asiProduct2 != null}">
                                    <c:forEach items="${model.asiProduct2.variants.variant}" var="variant2" varStatus="variant2Status">
                                      <c:if test="${variant2Status.index == variantStatus.index}">
                                        <tr>
                                          <th class="title"><fmt:message key="f_youPay" /></th>
                                        <c:choose>
                                            <c:when test="${fn:length(variant2.prices.price) == 0 || variant2.prices.price[0].price < 0}">
                                              <c:set var="displayQuote" value="true" />
                                            <td class="value">
                                                <input type="image" border="0" onclick="return addToQuoteList('${product.id}');"  name="_addtocart" class="_addtocart" src="${_contextpath}/assets/Image/Layout/button_requestQuote${_lang}.gif" />
                                             </td>
                                          </c:when>
                                            <c:when test="${model.samePriceForAllTiers == null || !model.samePriceForAllTiers}">
                                            <c:forEach items="${variant2.prices.price}" var="price" varStatus="priceStatus">
                                              <td class="value">
                                              <c:choose>
                                              <c:when test="${model.product.productType == 'REW'}">
                                                 <c:out value="${siteConfig['GROUP_REWARD_POINTS_TITLE'].value}" />
                                              </c:when>
                                              <c:otherwise>
                                                  <fmt:message key="${siteConfig['CURRENCY'].value}" />
                                              </c:otherwise>
                                              </c:choose>
                                              <fmt:formatNumber value="${variant2.prices.price[priceStatus.index].price}" pattern="#,##0.00" maxFractionDigits="2" />
                                                </td>
                                              </c:forEach>
                                             </c:when>
                                             <c:otherwise>
                                            <td class="value">
                                            <c:choose>
                                              <c:when test="${model.product.productType == 'REW'}">
                                                 <c:out value="${siteConfig['GROUP_REWARD_POINTS_TITLE'].value}" />
                                              </c:when>
                                              <c:otherwise>
                                                  <fmt:message key="${siteConfig['CURRENCY'].value}" />
                                              </c:otherwise>
                                              </c:choose>
                                            <fmt:formatNumber value="${variant2.prices.price[0].price}" pattern="#,##0.00" maxFractionDigits="2" />
                                              </td>
                                            </c:otherwise>
                                        </c:choose>
                                        </tr>
                                    </c:if>
                                    </c:forEach>
                                </c:when>
                              </c:choose>
                              </c:if>
                            </c:forEach>
                            </tbody>
                          </table>
                        </c:when>
                        <c:otherwise>
                        <!-- Products w/o variants -->
                                   <table id="asiPriceTable" class="table table-bordered">
                              <tbody>

                              <tr class="hdrRow">
                                <th class="hdrColumn" colspan="${fn:length(model.asiProduct.prices.price)}">Prices</th>
                              </tr>



                              <c:if test="${model.samePriceForAllTiers == null || !model.samePriceForAllTiers}">
                                <tr>
                                  <c:forEach items="${model.asiProduct.prices.price}" var="price" varStatus="priceStatus">
                                    <td class="value"><c:out value="${model.asiProduct.prices.price[priceStatus.index].quantity.range.from}" /></td>
                                  </c:forEach>
                                </tr>
                              </c:if>
                              <tr>
                                <c:choose>
                                  <c:when test="${fn:length(model.asiProduct.prices.price) == 0 || model.asiProduct.prices.price[0].price == null}">
                                    <c:set var="displayQuote" value="true" />
                                    <td class="value">
                                         <input type="image" border="0" onclick="return addToQuoteList('${product.id}');" name="_addtocart" class="_addtocart" src="${_contextpath}/assets/Image/Layout/button_requestQuote${_lang}.gif" />
                                     </td>
                                  </c:when>
                                  <c:when test="${model.samePriceForAllTiers == null || !model.samePriceForAllTiers}">
                                    <c:forEach items="${model.asiProduct.prices.price}" var="price" varStatus="priceStatus">
                                      <c:choose>
                                          <c:when test="${model.product.productType == 'REW'}">
                                          <td class="value">
                                             <c:out value="${siteConfig['GROUP_REWARD_POINTS_TITLE'].value}" /> <fmt:formatNumber value="${model.asiProduct.prices.price[priceStatus.index].price}" pattern="#,##0.00#" maxFractionDigits="${siteConfig['MAX_FRACTION_DIGIT'].value}" />
                                          </td>
                                          </c:when>
                                          <c:otherwise>
                                          <td class="value" <c:if test="${(product.salesTag != null and product.salesTag.discount != 0.0) or (siteConfig.get['ASI_PRICE_MARK_UP_CHANGE_BASE_PRICE'].value != 'true' and model.asiProduct2 != null)}" >style="text-decoration: line-through"</c:if> >
                                             <fmt:message key="${siteConfig['CURRENCY'].value}" /><fmt:formatNumber value="${model.asiProduct.prices.price[priceStatus.index].price}" pattern="#,##0.00#" maxFractionDigits="${siteConfig['MAX_FRACTION_DIGIT'].value}" />
                                          </td>
                                          </c:otherwise>
                                      </c:choose> 
                                    </c:forEach>
                                   </c:when>
                                  <c:otherwise>
                                  <c:choose>
                                          <c:when test="${model.product.productType == 'REW'}">
                                          <td class="value">
                                             <c:out value="${siteConfig['GROUP_REWARD_POINTS_TITLE'].value}" /> <fmt:formatNumber value="${model.asiProduct.prices.price[0].price}" pattern="#,##0.00#" maxFractionDigits="${siteConfig['MAX_FRACTION_DIGIT'].value}" />
                                          </td>
                                          </c:when>
                                          <c:otherwise>
                                            <td class="value" <c:if test="${(product.salesTag != null and product.salesTag.discount != 0.0) or (siteConfig.get['ASI_PRICE_MARK_UP_CHANGE_BASE_PRICE'].value != 'true' and model.asiProduct2 != null)}" >style="text-decoration: line-through"</c:if> >
                                               <fmt:message key="${siteConfig['CURRENCY'].value}" /><fmt:formatNumber value="${model.asiProduct.prices.price[0].price}" pattern="#,##0.00#" maxFractionDigits="${siteConfig['MAX_FRACTION_DIGIT'].value}" />
                                            </td>
                                    </c:otherwise>
                                    </c:choose>
                                  </c:otherwise>
                                </c:choose>
                              </tr>
                              <c:choose>
                                <c:when test="${product.salesTag != null and product.salesTag.discount != 0.0}">
                                    <tr>
                                      <c:choose>
                                      <c:when test="${fn:length(model.asiProduct.prices.price) == 0 || model.asiProduct.prices.price[0].price == null}">
                                      <c:set var="displayQuote" value="true" />
                                        <td class="value">
                                          <input type="image" border="0" onclick="return addToQuoteList('${product.id}');" name="_addtocart" class="_addtocart" src="${_contextpath}/assets/Image/Layout/button_requestQuote${_lang}.gif" />
                                         </td>
                                      </c:when>
                                      <c:when test="${model.samePriceForAllTiers == null || !model.samePriceForAllTiers}">
                                        <c:forEach items="${model.asiProduct.prices.price}" var="price" varStatus="priceStatus">
                                          <td class="value">
                                            <c:choose>
                                                <c:when test="${product.salesTag.percent}">
                                                <fmt:message key="${siteConfig['CURRENCY'].value}" /><fmt:formatNumber value="${model.asiProduct.prices.price[priceStatus.index].price *( (100 - product.salesTag.discount) / 100)}" pattern="#,##0.00" maxFractionDigits="2" />
                                                </c:when>
                                                <c:otherwise>
                                                <fmt:message key="${siteConfig['CURRENCY'].value}" /><fmt:formatNumber value="${model.asiProduct.prices.price[priceStatus.index].price - product.salesTag.discount}" pattern="#,##0.00" maxFractionDigits="2" />
                                                </c:otherwise>
                                            </c:choose>
                                            </td>
                                          </c:forEach>
                                      </c:when>
                                      <c:otherwise>
                                        <td class="value">
                                          <c:choose>
                                              <c:when test="${product.salesTag.percent}">
                                              <fmt:message key="${siteConfig['CURRENCY'].value}" /><fmt:formatNumber value="${model.asiProduct.prices.price[0].price *( (100 - product.salesTag.discount) / 100)}" pattern="#,##0.00" maxFractionDigits="2" />
                                              </c:when>
                                              <c:otherwise>
                                              <fmt:message key="${siteConfig['CURRENCY'].value}" /><fmt:formatNumber value="${model.asiProduct.prices.price[0].price - product.salesTag.discount}" pattern="#,##0.00" maxFractionDigits="2" />
                                              </c:otherwise>
                                          </c:choose>
                                            </td>
                                        </c:otherwise>
                                    </c:choose>
                                    </tr>
                                </c:when>
                                <c:when test="${siteConfig.get['ASI_PRICE_MARK_UP_CHANGE_BASE_PRICE'].value != 'true' and model.asiProduct2 != null}">
                                  <tr>
                                    <c:choose>
                                     <c:when test="${fn:length(model.asiProduct.prices.price) == 0 || model.asiProduct.prices.price[0].price == null}">
                                      <c:set var="displayQuote" value="true" />
                                      <td class="value">
                                           <input type="image" border="0" onclick="return addToQuoteList('${product.id}');" name="_addtocart" class="_addtocart" src="${_contextpath}/assets/Image/Layout/button_requestQuote${_lang}.gif" />
                                       </td>
                                    </c:when>
                                       <c:when test="${model.samePriceForAllTiers == null || !model.samePriceForAllTiers}">
                                      <c:forEach items="${model.asiProduct2.prices.price}" var="price" varStatus="priceStatus">
                                        <td class="value">
                                        <c:choose>
                                          <c:when test="${model.product.productType == 'REW'}">
                                            <c:out value="${siteConfig['GROUP_REWARD_POINTS_TITLE'].value}" />
                                          </c:when>
                                          <c:otherwise>
                                                <fmt:message key="${siteConfig['CURRENCY'].value}" />
                                            </c:otherwise>
                                            </c:choose>
                                            <fmt:formatNumber value="${model.asiProduct2.prices.price[priceStatus.index].price}" pattern="#,##0.00" maxFractionDigits="2" />
                                          </td>
                                        </c:forEach>
                                    </c:when>
                                    <c:otherwise>
                                    <td class="value">
                                    <c:choose>
                                      <c:when test="${model.product.productType == 'REW'}">
                                        <c:out value="${siteConfig['GROUP_REWARD_POINTS_TITLE'].value}" />
                                      </c:when>
                                      <c:otherwise>
                                          <fmt:message key="${siteConfig['CURRENCY'].value}" />
                                        </c:otherwise>
                                       </c:choose>
                                       <fmt:formatNumber value="${model.asiProduct2.prices.price[0].price}" pattern="#,##0.00" maxFractionDigits="2" />
                                      </td>
                                    </c:otherwise>
                                  </c:choose>
                                  </tr>
                                </c:when>
                              </c:choose>
                              </tbody>
                            </table>
                        </c:otherwise>
                      </c:choose>
                    </c:otherwise>
                </c:choose>
              </div>
            </div>
            <!-- End Price -->

        </div>

        <div class="col-sm-8">
            <div class="details_desc clearfix">
                <div class="details_item_name"><h1><c:out value="${product.name}" escapeXml="false" /></h1></div>
                <div class="details_sku">Product Number: &nbsp; <c:out value="${model.asiProduct.number}" />&nbsp;<c:out value="${model.asiProduct.supplier.asiNumber}" /> </div>
                <br/>
                <c:if test="${siteConfig['PRODUCT_QUOTE'].value == 'true' and product.quote}">
                <c:choose>
                  <c:when test="${siteConfig['QUOTE_AS_ORDER'].value == 'true'}">
                  <script type="text/javascript">
                      function addToQuoteList(productId){
                            var requestHTMLData = new Request.HTML ({
                                url: "${_contextpath}/quoteList.jhtm?productId="+productId,
                                onSuccess: function(response){
$('addToQuoteListWrapperId'+productId).set('html', '<h4><fmt:message key="f_addedToQuoteList" /></h4> <a href="${_contextpath}/quoteViewList.jhtm" rel="type:element" id="quoteListId" class="quoteList"> <input type="image" name="_viewQuoteList" class="_viewQuoteList" src="${_contextpath}/assets/Image/Layout/button_viewQuoteList.jpg" /> </a> ');
                                }
                            }).send();
                      }
                  </script>
                  <div class="addToQuoteListWrapper" id="addToQuoteListWrapperId${product.id}">
                    <input type="image" border="0" name="addtoquoteList" id="addtoquoteList" class="_addtoquoteList" value="${product.id}" src="${_contextpath}/assets/Image/Layout/button_addToQuoteList.jpg" onClick="addToQuoteList('${product.id}');" />
                  </div>
                  </c:when>
                  <c:otherwise>
                      <link rel="stylesheet" href="${_contextpath}/assets/SmoothGallery/multiboxASI.css" type="text/css" media="screen" />
                    <c:if test="${!multibox}">
                     <script src="${_contextpath}/javascript/overlay.js" type="text/javascript" ></script>
                     <script src="${_contextpath}/javascript/multibox.js" type="text/javascript" ></script>
                    </c:if>
                    <script type="text/javascript">
                    window.addEvent('domready', function(){
                        saveContactBox = new multiBox('mbQuote', {showControls : false, useOverlay: false, showNumbers: false });
                    });
                    </script>
                  <a href="#${product.id}" rel="type:element" id="mbQuote" class="mbQuote"><input type="image" border="0" name="_addtocart" class="_addtocart" src="${_contextpath}/assets/Image/Layout/button_addtoquote${_lang}.gif" /></a>
                  <div id="${product.id}" class="miniWindowWrapper mbPriceQuote" style="height:700px">
                    <form autocomplete="off" onsubmit="saveContact(); return false;">
                    <input type="hidden" value="${product.sku}" id="sku"/>
                    <input type="hidden" value="${product.name}" id="name"/>
                    <div class="header"><fmt:message key="f_description"/></div>
                    <fieldset class="top">
                        <label class="AStitle"><fmt:message key="firstName"/><span class="requiredFieldStar" id="requiredField_firstName"></span></label>
                        <input name="firstName" id="firstName" type="text" size="15" onkeyup="assignValue('firstName', this.value);"/>
                    </fieldset>
                    <fieldset class="bottom">
                        <label class="AStitle"><fmt:message key="lastName"/><span class="requiredFieldStar" id="requiredField_lastName"></span></label>
                        <input name="lastName" id="lastName" type="text" size="15" onkeyup="assignValue('lastName', this.value);"/>
                    </fieldset>
                    <fieldset class="bottom">
                        <label class="AStitle"><fmt:message key="company"/></label>
                        <input name="company" id="company" type="text" size="15" onkeyup="assignValue('company', this.value);"/>
                    </fieldset>
                    <fieldset class="bottom">
                        <label class="AStitle"><fmt:message key="address"/></label>
                        <input name="address" id="address" type="text" size="15" onkeyup="assignValue('address', this.value);"/>
                    </fieldset>
                    <fieldset class="bottom">
                        <label class="AStitle"><fmt:message key="city"/></label>
                        <input name="city" id="city" type="text" size="15" onkeyup="assignValue('city', this.value);"/>
                    </fieldset>
                    <fieldset class="bottom">
                        <label class="AStitle"><fmt:message key="state"/></label>
                        <input name="state" id="state" type="text" size="15" onkeyup="assignValue('state', this.value);"/>
                    </fieldset>
                    <fieldset class="bottom">
                        <label class="AStitle"><fmt:message key="zipCode"/></label>
                        <input name="zipCode" id="zipCode" type="text" size="15" onkeyup="assignValue('zipCode', this.value);"/>
                    </fieldset>
                    <fieldset class="bottom">
                        <label class="AStitle"><fmt:message key="country"/></label>
                        <input name="country" id="country" type="text" size="15" onkeyup="assignValue('country', this.value);"/>
                    </fieldset>
                    <fieldset class="bottom">
                        <label class="AStitle"><fmt:message key="email"/><span class="requiredFieldStar" id="requiredField_email"></span></label>
                        <input name="email" id="email" type="text" size="15" onkeyup="assignValue('email', this.value);"/>
                    </fieldset>
                    <fieldset class="bottom">
                        <label class="AStitle"><fmt:message key="phone"/><span class="requiredFieldStar" id="requiredField_phone"></span></label>
                        <input name="phone" id="phone" type="text" size="15" onkeyup="assignValue('phone', this.value);"/>
                    </fieldset>
                    <fieldset class="bottom">
                        <label class="AStitle"><fmt:message key="message"/></label>
                        <textarea name="note" id="note" onkeyup="assignValue('note', this.value);"></textarea>
                    </fieldset>
                    <fieldset>
                        <div id="button">
                            <input type="submit" value="Send"/>
                        </div>
                    </fieldset>
                    </form>
                  </div>
                  <div id="successMessage"></div>
                  </c:otherwise>
                </c:choose>
                </c:if>

                <!-- Description Start -->
                <c:if test="${(siteConfig['DETAILS_SHORT_DESC'].value == 'true' and product.shortDesc != '' and product.shortDesc != null) || product.longDesc != ''}">
                <div class="details_desc_wrapper">
                    <div class="details_desc_title">Product Description</div>
                    <div class="details_desc_content">
                        <c:if test="${siteConfig['DETAILS_SHORT_DESC'].value == 'true' and product.shortDesc != '' and product.shortDesc != null}">
                            <div class="details_short_desc"><c:out value="${product.shortDesc}" escapeXml="false" /></div>
                        </c:if>
                        <c:if test="${product.longDesc != ''}">
                            <div class="details_long_desc"><c:out value="${product.longDesc}" escapeXml="false" /></div>
                        </c:if>
                    </div>
                </div>
                </c:if>
                <!-- Description End -->
                <form action="${_contextpath}/addASIToCart.jhtm" name="this_form" method="post" onsubmit="return validateForm(${model.product.id});" enctype="multipart/form-data">

                <input type="hidden" value="${model.product.id}" name="product.id" id="product.id" />


                 <!-- Variant Selection -->
                <c:if test="${model.variantsSelectionList != null}">
                     <div class="configBox_wrapper variant">
                    <div class="configBox_title">Select Type</div>
                    <div class="configbox_content">
                      <select name="selectedVariant" id="variantSelect" onchange="updateVariant(this.value, '${model.product.id}');">
                        <option value="">Select Type</option>
                        <c:forEach items="${model.variantsSelectionList}" var="variant" varStatus="status">
                            <option value="${variant}"><c:out value="${variant}" escapeXml="false"/></option>
                        </c:forEach>
                      </select>
                      </div>
                  </div>
                </c:if>

                <c:if test="${model.variantsSelectionList != null}">
                     <c:forEach items="${model.variantsSelectionList}" var="variant" varStatus="status">
                     <input type="hidden" id="variantColors${variant}" value="${model.variantColorsMap[variant]}">
                     <input type="hidden" id="variantMaterials${variant}" value="${model.variantMaterialsMap[variant]}">
                     <input type="hidden" id="variantShapes${variant}" value="${model.variantShapesMap[variant]}">
                     <input type="hidden" id="variantSizes${variant}" value="${model.variantSizesMap[variant]}">
                   </c:forEach>
                 </c:if>

                 <c:if test="${model.productColorCodeMap != null}">
                    <div class="configBox_wrapper color">
                    <div class="configBox_title">Select Product Color</div>
                    <div class="configbox_content">
                    <div class="colorList">
                      <input type="hidden" id="selectedProductColor" name="optionNVPair">
                      <c:forEach items="${model.productColorCodeMap}" var="color" varStatus="status">
                          <div class="colorBoxWrapper" id="selectedColorBox_${color.key}">
                            <div class="color" id="color" <c:if test="${color.value != null}">style="background-image: url(${color.value})"</c:if> onclick="updateProductColor('${color.key}')">
                            </div>
                            <div class="color_name"> ${color.key} </div>
                          </div>
                      </c:forEach>
                      <div style="clear: both;"></div>
                     </div>
                   </div>
                  </div>
                  </c:if>

                 <c:if test="${!displayQuote}">
                  <div class="configBox_wrapper quantity">
                    <div class="configBox_title">Enter Quantity <c:if test="${model.minQuantity >= 2}"><span class="headerMinQty">Min Qty: <c:out value="${model.minQuantity}" escapeXml="false" /></span></c:if></div>
                    <div class="configbox_content">
                    <div class="quantityList">
                      <c:choose>
                      <c:when test="${model.allowedSizesToBuy != null && fn:length(model.allowedSizesToBuy) > 0}">
                          <c:forEach items="${model.allowedSizesToBuy}" var="size" varStatus="variantStatus">

                          <c:if test="${variantStatus.index == 0}">
                          <div class="titleBlock" >
                              <div class="variantTitle"></div>
                              <div class="qtytitle"> <fmt:message key="quantity" /> </div>
                          </div>
                          </c:if>
                          <div class="variantBlock sizeWrapperBox" id="selectedSizeBox_${size}" >
                              <div class="variantTitle"> <c:out value="${size}" /> </div>
                              <div class="qtyInputBlock">
                                <input name="multipleVariantNames" value="${size}" type="hidden" class="variantsName" />
                                <input name="multiQuantity_${model.product.id}_${size}" class="qtyInput" id="multiQuantity_${model.product.id}_${size}"   maxlength="5" type="text" autocomplete="off" value=""  />
                            </div>
                          </div>
                        </c:forEach>
                        <div style="clear: both;"></div>
                        </c:when>
                        <c:otherwise>
                        <input name="quantity_${model.product.id}" class="qtyInput" id="quantity_${model.product.id}"  value="${model.minQuantity}" type="text" autocomplete="off" style="width: 50px;" maxlength="5" />
                      </c:otherwise>
                    </c:choose>
                    </div>
                    <input value="${model.minQuantity}" type="hidden" id="minQty" />
                    </div>
                  </div>

                  <c:if test="${model.materials != null and fn:length(model.materials) > 1}">
                  <div class="configBox_wrapper material">
                    <div class="configBox_title">Choose Material</div>
                    <div class="configbox_content">
                      <select style="width: 157;" class="optionValueSelect material" onchange="updateText(this.value, 'material');">
                        <option value="">Select Material</option>
                        <c:forEach items="${model.materials}" var="material">
                          <option value="Material_value_${material}"><c:out value="${material}"/></option>
                        </c:forEach>
                      </select>
                      <input value="" type="hidden" name="optionNVPair" id="material" />
                    </div>
                  </div>
                  </c:if>

                  <c:if test="${model.shapes != null and fn:length(model.shapes) > 1}">
                  <div class="configBox_wrapper shape">
                    <div class="configBox_title">Choose Shape</div>
                    <div class="configbox_content">
                      <select style="width: 157;" class="optionValueSelect shape" onchange="updateText(this.value, 'shape');">
                        <option value="">Select Shape</option>
                        <c:forEach items="${model.shapes}" var="shape">
                          <option value="Shape_value_${shape}"><c:out value="${shape}"/></option>
                        </c:forEach>
                      </select>
                      <input value="" type="hidden" name="optionNVPair" id="shape" />
                    </div>
                  </div>
                  </c:if>

                 <c:choose>
                    <c:when test="${model.storeImprintOptions != null and fn:length(model.storeImprintOptions) > 0}">
                        <c:import url="/WEB-INF/jsp/frontend/layout/template6/wildman/imprintOptions.jsp" />

                    </c:when>
                    <c:otherwise>

                     <c:if test="${siteConfig['CUSTOMER_GROUP_GALLERY'].value == 'true' and !model.product.disableImprintOptions}">

                      <c:if test="${model.imprintPriceMap != null}">
                          <c:choose>
                            <c:when test="${fn:length(model.imprintPriceMap) > 1}">
                              <input value="" type="hidden" id="imprintCharge" name="asiAdditionalCharge" />
                          </c:when>
                            <c:otherwise>
                              <c:forEach items="${model.imprintPriceMap}" var="imprint" varStatus="status">
                              <input value="${imprint.value}" type="hidden" id="imprintCharge" name="additionalCharge" />
                            </c:forEach>
                          </c:otherwise>
                          </c:choose>


                          <div class="configBox_wrapper imprintMethod">
                          <div class="configBox_title">Choose Imprint Method</div>
                          <div class="configbox_content">
                            <c:forEach items="${model.imprintPriceMap}" var="imprint" varStatus="status">
                              <input value="${imprint.value}" type="hidden" id="imprintMethod_${imprint.key}" />
                            </c:forEach>

                            <select name="optionNVPair" onchange="updateImprint('${this.value}');">
                            <c:forEach items="${model.imprintPriceMap}" var="imprint" varStatus="status">
                              <option value="Imprint Method_value_${imprint.key} <c:if test="${imprint.value != null && imprint.value gt 0.00}">$<fmt:formatNumber value="${imprint.value}" pattern="#,##0.00#" maxFractionDigits="${siteConfig['MAX_FRACTION_DIGIT'].value}" /></c:if>">
                                <c:out value="${imprint.key}" escapeXml="false"/> <c:if test="${imprint.value != null && imprint.value gt 0.00}">$<fmt:formatNumber value="${imprint.value}" pattern="#,##0.00#" maxFractionDigits="${siteConfig['MAX_FRACTION_DIGIT'].value}" /></c:if>
                              </option>
                            </c:forEach>
                              </select>
                          </div>
                        </div>
                        </c:if>

                      <div class="configBox_wrapper imprintOptions">
                        <div class="configBox_title">Imprint Options</div>

                        <div class="configbox_content">
                            <div id="wASItext">After you have placed your order please email artwork to your inside account manager and include your web order #.
                            <br/><br/>If you are a new customer or don't have your account manager's email on file, send artwork to capart@wildmanbg.com<br/><br/> </div>

                               <div class="optionWrapper">
                                   <div class="optionTitle"><c:out value="Provide Imprint Colors"/>: </div>
                                <div class="optionBlock">
                                  <input type="radio" name="multiColorImprint" value="Single Color" <c:if test="${model.selectedMultiColorImprint == 'Single Color'}">checked="checked"</c:if> class="optionValueRadio" onclick="updateRadioOptions(this.name, this.value)"/> Single Color
                                  <input type="radio" name="multiColorImprint" value="Multi Color" <c:if test="${model.selectedMultiColorImprint == 'Multi Color'}">checked="checked"</c:if> class="optionValueRadio" onclick="updateRadioOptions(this.name, this.value)" /> Multiple Colors
                                  <input type="hidden" id="multiColorImprint" value="${model.selectedMultiColorImprint}" />
                                  <input type="hidden" name="optionNVPair" value="Imprint Color Type_value_${fn:escapeXml(model.selectedMultiColorImprint)}"/>
                                </div>
                            </div>

                            <div class="optionWrapper">
                                   <div class="optionTitle"><c:out value="Provide Imprint Colors"/>: </div>
                                <div class="optionBlock">
                                  <textarea name="imprintColors" id="imprintColors" cols="25" rows="5" onchange="updateOptions()"><c:out value="${model.selectedImprintColors}"/></textarea>
                                  <input type="hidden" name="optionNVPair" value="Imprint Colors_value_${fn:escapeXml(model.selectedImprintColors)}"/>
                                 </div>
                            </div>

                            <div class="optionWrapper">
                              <div class="optionTitle"><c:out value="Additional Locations"/>: </div>
                              <div class="optionBlock">
                                <input type="radio" name="multiLocationImprint" value="Yes" class="optionValueRadio" <c:if test="${model.selectedImprintLocation == 'Yes'}">checked="checked"</c:if> onclick="updateRadioOptions(this.name, this.value)" /> <fmt:message key="yes"/>
                                <input type="radio" name="multiLocationImprint" value="No" class="optionValueRadio" <c:if test="${model.selectedImprintLocation == 'No'}">checked="checked"</c:if> onclick="updateRadioOptions(this.name, this.value)" /> <fmt:message key="no"/>
                                <input type="hidden" id="multiLocationImprint" value="${model.selectedImprintLocation}"/>
                                <input type="hidden" name="optionNVPair" value="Imprint Location_value_${fn:escapeXml(model.selectedImprintLocation)}"/>
                              </div>
                            </div>

                            <div class="optionWrapper">
                                   <div class="optionTitle"><c:out value="Additional Information"/>: </div>
                                <div class="optionBlock">
                                  <textarea name="additionalInfo" id="additionalInfo" cols="25" rows="5" onchange="updateOptions()"><c:out value="${model.selectedAdditionalInfo}"/></textarea>
                                  <input type="hidden" name="optionNVPair" value="Additional Info_value_${fn:escapeXml(model.selectedAdditionalInfo)}"/>
                                </div>
                            </div>
                        </div>
                    </div>

                    </c:if>
                    </c:otherwise>
                  </c:choose>

                    <div class="addToCart_btn_wrapper">
                        <button type="submit" class="btn addToCart_btn">
                            Add To Cart &nbsp;<span class="addToCart_icon"></span>
                        </button>
                    </div>
            </c:if>
            <c:if test="${displayQuote}">
                <input type="image" border="0" onclick="return addToQuoteList('${product.id}');"  name="_addtocart" class="_addtocart" src="${_contextpath}/assets/Image/Layout/button_requestQuote${_lang}.gif" />
            </c:if>
              </form>

            </div>
        </div>
    </div>
</div>



<c:if test="${model.alsoConsiderMap[product.id] != null}">
   <c:set var="product" value="${model.alsoConsiderMap[product.id]}" />
   <%@ include file="/WEB-INF/jsp/frontend/common/alsoconsider.jsp" %>
   <c:set value="${model.product}" var="product"/>
</c:if>
</c:if>

<c:if test="${fn:trim(model.productLayout.footerHtml) != ''}">
  <c:set value="${model.productLayout.footerHtml}" var="footerHtml"/>
  <%@ include file="/WEB-INF/jsp/frontend/common/footerDynamicElement.jsp" %>
  <div style="float:left;width:100%"><c:out value="${footerHtml}" escapeXml="false"/></div>
</c:if>

  </tiles:putAttribute>
</tiles:insertDefinition> 
