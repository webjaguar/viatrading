					<!-- ========== LEFT SIDE BAR ========== -->
					<div id="leftSidebarWrapper">
						<div id="leftSidebar">
							<div id="btn-toggle-offcanvas" class="visible-xs">
								<button type="button" class="btn btn-primary" data-toggle="offcanvas">
									<i class="fa fa-bars"></i>
								</button>
							</div>
							<div class="col-xs-6 col-sm-3 col-md-2 sidebar-offcanvas" id="sidebar" role="navigation">
								<form action="/lsearch2.jhtm" method="get" id="luceneSearch">
									<div class="luceneSearchWrapper">
										<div class="luceneSearchBox">
											<div class="luceneSearchSelectionTitle">
												Keywords: LOAD
											</div>

											<div class="luceneSearchFilter Price">
												<div class="filterTitle">Price</div>
												<div class="filterValues">
													<input type="text" class="filterInput minPriceInput form-control input-sm" name="minPrice" placeholder="Min Price">
													<span class="luceneSearchFilterPriceSeparator">-</span>
													<input type="text" class="filterInput maxPriceInput form-control input-sm" name="maxPrice" placeholder="Max Price">
													<div class="btn_wrapper">
														<button type="button" class="btn btn-sm search_btn">Go</button>
													</div>
												</div>
											</div>

											<div class="luceneSearchFilter Quantity">
												<div class="filterTitle">Min Qty</div>
												<div class="filterValues">
													<input type="text" class="filterInput minQtyInput form-control input-sm" name="minQty" placeholder="Min Qty">
													<div class="btn_wrapper">
														<button type="button" class="btn btn-sm search_btn">Go</button>
													</div>
												</div>
											</div>

											<div class="luceneSearchSelectionWrapper">
												<div class="luceneSearchSelectionTitle">Lucene Search Selection</div>
												<div class="checkbox">
													<label>
														<input type="checkbox"> Option 1
													</label>
												</div>
												<div class="checkbox">
													<label>
														<input type="checkbox" checked="checked"> Option 1
													</label>
												</div>
												<div class="checkbox x-checkbox">
													<label>
														<input type="checkbox" checked="checked" onclick="removeFilter('Category', '0');"> Option 2
													</label>
												</div>
											</div>

											<div class="luceneFilterSelectionWrapper">
												<div class="luceneFilterSelectionTitle">Lucene Filter Selection</div>
												<div class="checkbox">
													<label>
														<input type="checkbox"> Option 1
													</label>
												</div>
												<div class="checkbox">
													<label>
														<input type="checkbox" checked="checked"> Option 1
													</label>
												</div>
												<div class="checkbox x-checkbox">
													<label>
														<input type="checkbox" checked="checked" onclick="removeFilter('Category', '0');"> Option 2
													</label>
												</div>
											</div>

											<!-- Filters with groups (2 Levels) -->
											<div class="luceneSearchFilters">
												<div class="filterGroupTitle">
													<div class="filterGroupTitleToggle" data-toggle="collapse" data-target="#filterGroupValues_1">
														Filter Group (2 Levels)
													</div>
												</div>
												<div id="filterGroupValues_1" class="filterGroupValues collapse in">
													<div class="luceneSearchFilter">
														<div class="filterTitle">
															<div class="filterTitleToggle" data-toggle="collapse" data-target="#filterValues_1">
																Filter 1
															</div>
														</div>
														<div id="filterValues_1" class="filterValues collapse in">
															<div class="filterValue">
																<a href="#">
																	Value 1
																	<span class="filterValueCount">(1)</span>
																</a>
															</div>
															<div class="filterValue">
																<a href="#">
																	Value 2
																	<span class="filterValueCount">(2)</span>
																</a>
															</div>
														</div>
													</div>
													<div class="luceneSearchFilter">
														<div class="filterTitle">
															<div class="filterTitleToggle" data-toggle="collapse" data-target="#filterValues_2">
																Filter 2
															</div>
														</div>
														<div id="filterValues_2" class="filterValues collapse in">
															<div class="filterValue">
																<a href="#">
																	Value 1
																	<span class="filterValueCount">(1)</span>
																</a>
															</div>
															<div class="filterValue">
																<a href="#">
																	Value 2
																	<span class="filterValueCount">(2)</span>
																</a>
															</div>
														</div>
													</div>
												</div>
											</div>

											<!-- Filters without groups (1 Level) -->
											<div class="luceneSearchFilters">
												<div class="luceneSearchFilter">
													<div class="filterTitle">
														<div class="filterTitleToggle" data-toggle="collapse" data-target="#filterValues_5">
															Packing
														</div>
													</div>
													<div id="filterValues_5" class="filterValues collapse in">
														<div class="filterValue Packing  Case">
															<a href="#" onclick="submitSearch('Packing',' Case');">
															   Case
															  <span class="filterValueCount">
															  (20)
															  </span>
															</a>
														</div>
														<div class="filterValue Packing  LTL">
															<a href="#" onclick="submitSearch('Packing',' LTL');">
															   LTL
															  <span class="filterValueCount">
															  (139)
															  </span>
															</a>
														</div>
														<div class="filterValue Packing  Load">
															<a href="#" onclick="submitSearch('Packing',' Load');">
															   Load
															  <span class="filterValueCount">
															  (189)
															  </span>
															</a>
														</div>
														<div class="filterValue Packing  Pallet">
															<a href="#" onclick="submitSearch('Packing',' Pallet');">
															   Pallet
															  <span class="filterValueCount">
															  (146)
															  </span>
															</a>
														</div>
													</div>
												</div>
												<div class="luceneSearchFilter">
													<div class="filterTitle">
														<div class="filterTitleToggle" data-toggle="collapse" data-target="#filterValues_6">
															Product Category
														</div>
													</div>
													<div id="filterValues_6" class="filterValues collapse in">
														<div class="filterValue">
															<div class="checkbox">
																<label>
																	<input type="checkbox" onclick="submitSearch('Category','Clothing');"> Clothing
																	<span class="filterValueCount">(171)</span>
																</label>
															</div>
														</div>
														<div class="filterValue">
															<div class="checkbox">
																<label>
																	<input type="checkbox" onclick="submitSearch('Category','Cosmetics');"> Cosmetics
																	<span class="filterValueCount">(1)</span>
																</label>
															</div>
														</div>
														<div class="filterValue">
															<div class="checkbox">
																<label>
																	<input type="checkbox" onclick="submitSearch('Category','Cosmetics/HBA');"> Cosmetics/HBA
																	<span class="filterValueCount">(3)</span>
																</label>
															</div>
														</div>
														<div class="filterValue">
															<div class="checkbox">
																<label>
																	<input type="checkbox" onclick="submitSearch('Category','Domestics');"> Domestics
																	<span class="filterValueCount">(20)</span>
																</label>
															</div>
														</div>
														<div class="filterValue">
															<div class="checkbox">
																<label>
																	<input type="checkbox" onclick="submitSearch('Category','Electronics');"> Electronics
																	<span class="filterValueCount">(14)</span>
																</label>
															</div>
														</div>
														<div class="filterValue">
															<div class="checkbox">
																<label>
																	<input type="checkbox" onclick="submitSearch('Category','Fashion Accessories');"> Fashion Accessories
																	<span class="filterValueCount">(8)</span>
																</label>
															</div>
														</div>
														<div class="filterValue">
															<div class="checkbox">
																<label>
																	<input type="checkbox" onclick="submitSearch('Category','Footwear');"> Footwear
																	<span class="filterValueCount">(43)</span>
																</label>
															</div>
														</div>
														<div class="filterValue">
															<div class="checkbox">
																<label>
																	<input type="checkbox" onclick="submitSearch('Category','Furniture');"> Furniture
																	<span class="filterValueCount">(7)</span>
																</label>
															</div>
														</div>
														<div class="filterValue">
															<div class="checkbox">
																<label>
																	<input type="checkbox" onclick="submitSearch('Category','General Merchandise');"> General Merchandise
																	<span class="filterValueCount">(146)</span>
																</label>
															</div>
														</div>
														<div class="filterValue">
															<div class="checkbox">
																<label>
																	<input type="checkbox" onclick="submitSearch('Category','Handbags');"> Handbags
																	<span class="filterValueCount">(28)</span>
																</label>
															</div>
														</div>
														<div class="filterValue">
															<div class="checkbox">
																<label>
																	<input type="checkbox" onclick="submitSearch('Category','Housewares');"> Housewares
																	<span class="filterValueCount">(25)</span>
																</label>
															</div>
														</div>
														<div class="filterValue">
															<div class="checkbox">
																<label>
																	<input type="checkbox" onclick="submitSearch('Category','Office Supplies');"> Office Supplies
																	<span class="filterValueCount">(9)</span>
																</label>
															</div>
														</div>
													</div>
												</div>
												<div class="luceneSearchFilter">
													<div class="filterTitle">
														<div class="filterTitleToggle" data-toggle="collapse" data-target="#filterValues_7">
															Production Condition
														</div>
													</div>
													<div id="filterValues_7" class="filterValues collapse in">
														<div class="filterValue">
															<div class="checkbox">
																<label>
																	<input type="checkbox"> Customer Returns
																	<span class="filterValueCount">(5)</span>
																</label>
															</div>
														</div>
														<div class="filterValue">
															<div class="checkbox">
																<label>
																	<input type="checkbox"> Irregulars
																	<span class="filterValueCount">(2)</span>
																</label>
															</div>
														</div>
														<div class="filterValue">
															<div class="checkbox">
																<label>
																	<input type="checkbox"> Master Case
																	<span class="filterValueCount">(10)</span>
																</label>
															</div>
														</div>
														<div class="filterValue">
															<div class="checkbox">
																<label>
																	<input type="checkbox"> New Overstock
																	<span class="filterValueCount">(8)</span>
																</label>
															</div>
														</div>
														<div class="filterValue">
															<div class="checkbox">
																<label>
																	<input type="checkbox"> Pre-Worn
																	<span class="filterValueCount">(9)</span>
																</label>
															</div>
														</div>
														<div class="filterValue">
															<div class="checkbox">
																<label>
																	<input type="checkbox"> Shelf Pulls
																	<span class="filterValueCount">(6)</span>
																</label>
															</div>
														</div>
													</div>
												</div>
											</div>
										</div>
									</div>
								</form>

							</div>
						</div>
					</div>
					<!-- ========== END LEFT SIDE BAR ========== -->
