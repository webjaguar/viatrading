<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib uri="/WEB-INF/tlds/spring.tld" prefix="spring"%>
<%@ taglib uri="/WEB-INF/tlds/custom.tld" prefix="wj"%>
<%@ page session="false"%>

<tiles:insertDefinition name="${_template}" flush="true">
	<c:if test="${_leftBar != '1' and _leftBar != '4'}">
		<tiles:putAttribute name="leftbar"
			value="/WEB-INF/jsp/frontend/layout/template6/leftbar.jsp" />
	</c:if>
	<tiles:putAttribute name="content" type="string">

		<div class="col-breadcrumb">
			<ul class="breadcrumb">
				<li><a href="${_contextpath}/account.jhtm">My Account</a></li>
				<li class="active">Address</li>
			</ul>
		</div>
		
		<c:if test="${fn:trim(model.myAddressLayout.headerHtml) != ''}">
			<c:set value="${model.myAddressLayout.headerHtml}" var="headerHtml" />
			<c:set
				value="${fn:replace(headerHtml, '#firstname#', userSession.firstName)}"
				var="headerHtml" />
			<c:set
				value="${fn:replace(headerHtml, '#lastname#', userSession.lastName)}"
				var="headerHtml" />
			<c:set
				value="${fn:replace(headerHtml, '#email#', userSession.username)}"
				var="headerHtml" />
			<c:set
				value="${fn:replace(headerHtml, '#accountnumber#', model.customer.accountNumber)}"
				var="headerHtml" />
			<c:out value="${headerHtml}" escapeXml="false" />
		</c:if>
		
		
		
		<div class="col-sm-12">
			<div class="message"></div>
			<form id="accountAddressesForm" class="form-horizontal" role="form"
				method="POST" action="#">
				<div id="accountAddresses">
					<div class="row">
						<div class="col-sm-6">
							<h3><fmt:message key="addressBook" /></h3>

							<c:forEach items="${model.addresses.pageList}" var="address" varStatus="status">
							<div class="address">
								<b><c:out value="${address.firstName}" />  <c:out value="${address.lastName}" /> </b><br>
									<c:if test="${address.company != ''}"><c:out value="${address.company}" /><br></c:if><c:out value="${address.addr1}" />
										<c:if test="${!empty address.addr2}"><c:out value="${address.addr2}" /></c:if>, <c:out value="${address.city}" />, <c:out value="${address.stateProvince}" /> <c:out value="${address.zip}" />, <c:out value="${address.country}" /><br> 
								
								<c:if test="${siteConfig['EDIT_ADDRESS_ON_FRONTEND'].value == 'true'}">
								  <a href="account_address.jhtm?id=${address.id}" class="editAddressLink">Edit</a> 
								</c:if>
								<c:if test="${address.id != model.defaultAddress.id}">
									<span class="linkSeparator">|</span>
									<a href="account_addresses.jhtm?deletedId=${address.id}" name="__delete" class="editAddressLink"><fmt:message key="delete" /></a> 
								</c:if>
								<c:if test="${address.id == model.defaultAddress.id}">
									<span class="linkSeparator">|</span><fmt:message key="primaryAddress" />
								</c:if>		
							</div>
							</c:forEach>
						</div>
						<div class="col-sm-6"></div>
					</div>
				</div>
				<c:if test="${siteConfig['EDIT_ADDRESS_ON_FRONTEND'].value == 'true'}">
				<div id="form_buttons">
					<div class="row">
						<div class="col-sm-12">
							<div id="buttons_wrapper">
								<button type="submit" name="__add" class="btn btn-default">Add New Address</button>
							</div>
						</div>
					</div>
				</div>
				</c:if>
				
			</form>

		</div>
		
		<c:if test="${fn:trim(model.myAddressLayout.footerHtml) != ''}">
			<c:set value="${model.myAddressLayout.footerHtml}" var="footerHtml" />
			<c:set
				value="${fn:replace(footerHtml, '#firstname#', userSession.firstName)}"
				var="footerHtml" />
			<c:set
				value="${fn:replace(footerHtml, '#lastname#', userSession.lastName)}"
				var="footerHtml" />
			<c:set
				value="${fn:replace(footerHtml, '#email#', userSession.username)}"
				var="footerHtml" />
			<c:set
				value="${fn:replace(footerHtml, '#accountnumber#', model.customer.accountNumber)}"
				var="footerHtml" />
			<c:out value="${footerHtml}" escapeXml="false" />
		</c:if>

	</tiles:putAttribute>
</tiles:insertDefinition>
