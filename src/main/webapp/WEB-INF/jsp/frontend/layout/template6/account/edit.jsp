<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="/WEB-INF/tlds/spring.tld" prefix="spring"%>
<%@ taglib uri="/WEB-INF/tlds/custom.tld" prefix="wj"%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page session="false"%>

<tiles:insertDefinition name="${_template}" flush="true">
	<c:if test="${_leftBar != '1' and _leftBar != '4'}">
		<tiles:putAttribute name="leftbar"
			value="/WEB-INF/jsp/frontend/layout/template6/leftbar.jsp" />
	</c:if>
	<tiles:putAttribute name="content" type="string">

		<div class="col-breadcrumb">
			<ul class="breadcrumb">
				<li><a href="${_contextpath}/account.jhtm">My Account</a></li>
				<li class="active">Edit</li>
			</ul>
		</div>
		
		<c:out value="${model.myAddressEdit.headerHtml}" escapeXml="false" />
		
		<div class="col-sm-12">
			<div class="message">
				<spring:hasBindErrors name="addressForm">
					<span class="error"><fmt:message key="form.hasErrors" /></span>
				</spring:hasBindErrors>
			</div>
			
			<form id="editEmailAddressAndPasswordForm" class="form-horizontal" role="form" method="POST" action="#">
				<div class="row">
					<div class="col-sm-12">
						<div class="requiredFieldLabel">* Required Field</div>
					</div>
				</div>
				<div id="editEmailAddressAndPassword">
					<div class="row">
						<div class="col-sm-6">
							<div class="form-group">
								<div class="col-sm-12">
									<h3><c:if test="${siteConfig['EDIT_EMAIL_ON_FRONTEND'].value == 'true'}"><fmt:message key="emailAddress"/></c:if>&nbsp;<fmt:message key="password"/>
									</h3>
								</div>
							</div>
							<c:if test="${siteConfig['EDIT_EMAIL_ON_FRONTEND'].value == 'true'}">
							<div class="form-group">
								<div class="col-sm-4">
									<label for="customer_username" class="control-label">
										<fmt:message key="emailAddress" /> <sup class="requiredField">*</sup>
									</label>
								</div>
								<div class="col-sm-8">
									<spring:bind path="customerForm.customer.username">
										<input type="text" class="form-control" id="customer_username" name="<c:out value="${status.expression}"/>" value="<c:out value="${status.value}"/>" />
										<span class="error"><c:out value="${status.errorMessage}" /></span>
									</spring:bind>
								</div>
							</div>
							</c:if>
							
							<c:if test="${siteConfig['CURRENT_PASSWORD_CHECK'].value == 'true'}">
							<div class="form-group">
								<div class="col-sm-4">
									<label for="customer_password" class="control-label">
										<fmt:message key="currentPassword" /> <sup class="requiredField">*</sup>
									</label>
								</div>
								<div class="col-sm-8">
									
									<spring:bind path="customerForm.currentPassword">
										<input type="password" class="form-control" id="currentPassword" name="<c:out value="${status.expression}"/>" value="<c:out value="${status.value}"/>" />
										<span class="error"><c:out value="${status.errorMessage}" escapeXml="false" /></span>
									</spring:bind>
								</div>
							</div>
							</c:if>
							
							<div class="form-group">
								<div class="col-sm-4">
									<label for="customer_password" class="control-label">
										<fmt:message key="newPassword" /> <sup class="requiredField">*</sup>
									</label>
								</div>
								<div class="col-sm-8">
									<spring:bind path="customerForm.customer.password">
										<input type="password" class="form-control" id="customer_password" name="<c:out value="${status.expression}"/>" value="<c:out value="${status.value}"/>" />
										<span class="error"><c:out value="${status.errorMessage}" /></span>
									</spring:bind>
								</div>
							</div>
							<div class="form-group">
								<div class="col-sm-4">
									<label for="confirm_password" class="control-label">
										<fmt:message key="confirmPassword" /> <sup class="requiredField">*</sup>
									</label>
								</div>
								<div class="col-sm-8">
									<spring:bind path="customerForm.confirmPassword">
										<input type="password" class="form-control" id="confirm_password" name="<c:out value="${status.expression}"/>" value="<c:out value="${status.value}"/>" />
										<span class="error"><c:out value="${status.errorMessage}" /></span>
									</spring:bind>
								</div>
							</div>
						</div>
						<div class="col-sm-6"></div>
					</div>
				</div>
				<div id="form_buttons">
					<div class="row">
						<div class="col-sm-12">
							<div id="buttons_wrapper">
								<button type="submit" class="btn btn-default"><fmt:message key="saveChanges" /></button>
								<button type="submit" class="btn btn-default" name="_cancel"><fmt:message key="cancel" /></button>
							</div>
						</div>
					</div>
				</div>
			</form>

		</div>
		<c:out value="${model.myAddressEdit.footerHtml}" escapeXml="false" />

	</tiles:putAttribute>
</tiles:insertDefinition>