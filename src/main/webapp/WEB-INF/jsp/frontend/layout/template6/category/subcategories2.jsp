<%@ page session="false" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>

<div class="row">
	<div class="col-sm-12">
		<div class="row">
			<div class="sub_categories_wrapper">
				<c:forEach items="${model.subCategories}" var="category" varStatus="status">
				<c:set var="subCatLink" value="#"></c:set>
				<c:if test="${category.linkType != 'NONLINK'}">      
				  <c:choose>
				    <c:when test="${gSiteConfig['gMOD_REWRITE'] == '1'}">
				      <c:set var="subCatLink" value="${_contextpath}/${siteConfig['MOD_REWRITE_CATEGORY'].value}/${category.id}/${category.encodedName}.html"></c:set>
				    </c:when>
				    <c:otherwise>
					  <c:set var="subCatLink" value="${_contextpath}/category.jhtm?cid=${category.id}"></c:set>
				    </c:otherwise>
				  </c:choose>
				</c:if>
   
				
				<div class="col-sm-6 col-md-4 col-lg-3">
					<div class="sub_category_wrapper">
						<a href="${subCatLink}" class="sub_category_link">
							<div class="sub_category_name_wrapper">
								<div class="sub_category_name">
									<c:out value="${category.name}" escapeXml="false" />
								</div>
							</div>
							<div class="sub_category_image_wrapper">
								<c:choose>
								  <c:when test="${ not empty (category.imageUrl) and category.imageUrl != null}">
								  	<c:choose>
								      <c:when test="${category.absolute}">
								    	<img src="<c:url value="${category.imageUrl}"/>" class="img-responsive center-block sub_category_image" alt="${category.imageUrlAltName}">
								      </c:when>
								      <c:otherwise>
								    	<img src="<c:url value="${_contextpath}/assets/Image/Category/${category.imageUrl}"/>" class="img-responsive center-block sub_category_image" alt="${category.imageUrlAltName}">
								      </c:otherwise>
								    </c:choose>
								  </c:when>
								  <c:when test="${category.hasImage}">
									<img src="<c:url value="${_contextpath}/assets/Image/Category/cat_${category.id}.gif"/>" class="img-responsive center-block sub_category_image" alt="${category.imageUrlAltName}">
								  </c:when>
							    </c:choose>
							</div>
							
							<div class="sub_sub_categories_links">
								<ul>
									<c:forEach items="${category.subCategories}" var="subCat" varStatus="subCatStatus">
										<c:if test="${subCatStatus.index < 5}">
										<c:set var="subSubCatLink" value="#" />
										<c:if test="${subCat.linkType != 'NONLINK'}">
											<c:choose>
											    <c:when test="${gSiteConfig['gMOD_REWRITE'] == '1'}">
											      <c:set var="subSubCatLink" value="${_contextpath}/${siteConfig['MOD_REWRITE_CATEGORY'].value}/${subCat.id}/${subCat.encodedName}.html"></c:set>
											    </c:when>
											    <c:otherwise>
											  <c:set var="subSubCatLink" value="${_contextpath}/category.jhtm?cid=${subCat.id}"></c:set>
											    </c:otherwise>
											</c:choose>
										</c:if>
										<li><a href="${subSubCatLink}"><c:out value="${subCat.name}" escapeXml="false" /></a></li>
										</c:if>
									</c:forEach>
								</ul>
							</div>
							<div class="shop_all_wrapper">
								<a href="${subCatLink}" class="shop_all">Shop All</a>
							</div>
						</a>
					</div>
				</div>
				</c:forEach>
			</div>
		</div>
	</div>
</div>
<script type="text/javascript">
<!--	
$(window).load(function() {
    $(window).resize(function(){
           if($(window).width() > 767) {
                 var sub_category_image_wrapper_maxHeight = Math.max.apply(null, $(".sub_category_image_wrapper").map(function(){
                        return $(this).height();
                 }).get());
                  $(".sub_category_image_wrapper").height(sub_category_image_wrapper_maxHeight);

                 var sub_category_name_wrapper_maxHeight = Math.max.apply(null, $(".sub_category_name_wrapper").map(function(){
                        return $(this).height();
                 }).get());
                  $(".sub_category_name_wrapper").height(sub_category_name_wrapper_maxHeight);

                 var sub_sub_categories_links_maxHeight = Math.max.apply(null, $(".sub_sub_categories_links").map(function(){
                        return $(this).height();
                 }).get());
                  $(".sub_sub_categories_links").height(sub_sub_categories_links_maxHeight);

                 var sub_category_wrapper_maxHeight = Math.max.apply(null, $(".sub_category_wrapper").map(function(){
                        return $(this).height();
                 }).get());
                 $(".sub_category_wrapper").height(sub_category_wrapper_maxHeight);
           }
           else {
                 $(".sub_category_image_wrapper").css('height','auto');
                 $(".sub_category_name_wrapper").css('height','auto');
                 $(".sub_sub_categories_links").css('height','auto');
                 $(".sub_category_wrapper").css('height','auto');
           }
    }).trigger('resize');
});

//-->
</script>