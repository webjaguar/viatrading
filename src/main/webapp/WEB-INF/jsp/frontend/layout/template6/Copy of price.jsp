
  
  


<div class="col-sm-12">

<div id="product_details" class="row">
 
	
	<div class="col-sm-8">
	  <div class="details_desc clearfix">
		
		

	
		<c:choose>
		  <c:when test="${ (gSiteConfig['gSHOPPING_CART'] and (!empty product.price or product.priceByCustomer)) and (!product.loginRequire or userSession != null)}">
	        <div class="prices_table">
			  <c:if test="${fn:length(product.price) > 1}">
			  <div class="quantities">
				<div class="quantities_title"> <fmt:message key="quantity" /> </div>
				  <c:forEach items="${product.price}" var="price" varStatus="statusPrice">
			        <div class="quantity_cell">
			          <c:if test="${price.qtyFrom != null}">
					    <c:choose>
					      <c:when test="${price.qtyTo != null}">
					        <c:out value="${price.qtyFrom}" /> -  <c:out value="${price.qtyTo}" />
					      </c:when>
					      <c:otherwise>
					        <c:out value="${price.qtyFrom}" /> +
					      </c:otherwise>
					    </c:choose>
					  </c:if>
					  <c:if test="${price.qtyFrom == null}">
					    1 +
					  </c:if>
			        </div>
				  </c:forEach>
			  </div>
			  </c:if>
			  <div class="prices">
				<div class="prices_title"><fmt:message key="price" /></div>
				<c:forEach items="${product.price}" var="price" varStatus="statusPrice">
			    <div class="price_cell <c:if test="${product.salesTag != null}">strikethrough</c:if>">
				  <span class="price">  
				    <fmt:message key="${siteConfig['CURRENCY'].value}" /><fmt:formatNumber value="${price.amt}" pattern="#,##0.00#" maxFractionDigits="${siteConfig['MAX_FRACTION_DIGIT'].value}" />
			      </span>
				  <span class="caseContent">
				  <c:choose>
			        <c:when test="${product.caseContent != null}">(<c:out value="${product.caseContent}" /> 
			        <c:choose>
  						<c:when test="${product.caseUnitTitle != null}"><c:out value="${product.caseUnitTitle}" /></c:when> 
  						<c:otherwise><c:out value="${siteConfig['CASE_UNIT_TITLE'].value}" /></c:otherwise>
  					</c:choose>
			        /<c:out value="${product.packing}" />)</c:when>
			        <c:otherwise><c:out value="${product.packing}" /></c:otherwise>
			      </c:choose>
				  </span>
				</div>
				</c:forEach>
			  </div>
			  <c:if test="${product.salesTag != null}">
				<div class="prices">
				<div class="prices_title"><fmt:message key="price" /></div>
				<c:forEach items="${product.price}" var="price" varStatus="statusPrice">
			    <div class="price_cell new_price">
				  <span class="price">  
				    <fmt:message key="${siteConfig['CURRENCY'].value}" /><fmt:formatNumber value="${price.discountAmt}" pattern="#,##0.00#" maxFractionDigits="${siteConfig['MAX_FRACTION_DIGIT'].value}" />
			      </span>
				  <span class="caseContent">
				  <c:choose>
			        <c:when test="${product.caseContent != null}">(<c:out value="${product.caseContent}" /> <c:out value="${siteConfig['CASE_UNIT_TITLE'].value}" />/<c:out value="${product.packing}" />)</c:when>
			        <c:otherwise><c:out value="${product.packing}" /></c:otherwise>
			      </c:choose>
				  </span>
				</div>
				</c:forEach>
			  </div>
			  
			  </c:if>
				
			  <div class="clear"></div>
			</div>
		    
		
		  </c:when>  
	    </c:choose>	
	    		

	  </div>
	</div>
  </div>
