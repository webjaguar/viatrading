<%@ page session="false" %>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ taglib uri="/WEB-INF/tlds/custom.tld" prefix="wj" %>

<tiles:insertDefinition name="${_template}" flush="true">
  <c:if test="${_leftBar != '1' and _leftBar != '4'}">
     <tiles:putAttribute name="leftbar" value="/WEB-INF/jsp/frontend/layout/template6/leftbar.jsp" />
  </c:if>
  <tiles:putAttribute name="content" type="string">
	<div class="col-breadcrumb">
		<ul class="breadcrumb">
			<li><a href="${_contextpath}/account.jhtm">My Account</a></li>
			<li class="active">Reorder</li>
		</ul>
	</div>

<c:if test="${gSiteConfig['gMYORDER_LIST']}">

<c:if test="${fn:trim(model.myOrderListLayout.headerHtml) != ''}">
  <c:set value="${model.myOrderListLayout.headerHtml}" var="headerHtml"/>
  <c:set value="${fn:replace(headerHtml, '#firstname#', userSession.firstName)}" var="headerHtml"/>
  <c:set value="${fn:replace(headerHtml, '#lastname#', userSession.lastName)}" var="headerHtml"/>
  <c:set value="${fn:replace(headerHtml, '#email#', userSession.username)}" var="headerHtml"/>
  <c:out value="${headerHtml}" escapeXml="false"/>
</c:if>

<div class="col-sm-12">
	<div class="reorderListWrapper">
		<div class="headerTitle reorderList">Reorder</div>

		<div class="clearfix"></div>

		<div class="row">
			<div class="col-sm-12">
				<div class="pageNavigation">
					
					<form class="formPageNavigation clearfix" name="pagination" action="#">
					    <div class="pageNavigationControl">
							<%--
							<div class="pagesize">
								<span>Page Size</span> <select class="selectpicker"
									name="pagesize">
									<option value="10">10 per page</option>
									<option value="20">20 per page</option>
									<option value="25" selected="">25 per page</option>
									<option value="50">50 per page</option>
									<option value="100">100 per page</option>
								</select>
							</div>
							--%>
							<div class="page">
								<span>Page</span> 
								<select class="selectpicker" name="page" onchange="submit()">
									<c:forEach begin="1" end="${model.myOrderLineItems.pageCount}" var="page">
							  	        <option value="${page}" <c:if test="${page == (model.myOrderLineItems.page+1)}">selected</c:if>>${page}</option>
									</c:forEach>
								</select>
								<span>of <c:out value="${model.myOrderLineItems.pageCount}"/></span>
							</div>
							<%-- 
							<c:if test="${model.myOrderLineItems.pageCount > 1}">
								<div class="pageNav">
									<c:if test="${model.myOrderLineItems.page == 0}"><span class="pageNavLink disabled"><a href="#"><span class="pageNavPrev"></span></a></span></c:if>
					            	<c:if test="${model.myOrderLineItems.page > 0}"><span class="pageNavLink"><a href="${_contextpath}/reorder.jhtm.jhtm?order=${model.order}&page=${model.myOrderLineItems.page+1}" class="pageNaviLink"><span class="pageNavPrev"></span></a></span></c:if>
					    			
									<c:if test="${model.myOrderLineItems.page == (model.myOrderLineItems.pageCount -1)}"><span class="pageNavLink disabled"><a href="#"><span class="pageNavNext"></span></a></span></c:if>
									<c:if test="${model.myOrderLineItems.page != (model.myOrderLineItems.pageCount - 1)}"><span class="pageNavLink"><a href="${_contextpath}/reorder.jhtm.jhtm?order=${model.order}&page=${model.myOrderLineItems.page+1}"><span class="pageNavNext"></span></a></span></c:if>
								</div>
							</c:if>
						


							<div class="pageNav">
								<span class="pageNavLink"> <a href="#"><span class="pageNavPrev"></span></a></span> 
								<span class="pageNavLink"> <a href="#"><span class="pageNavNext"></span></a></span>
							</div>--%>
						</div>
					</form>
				</div>
			</div>
		</div>

		<form action="${_contextpath}/addToCart.jhtm" method="post" onsubmit="return checkForm()">
		<div class="reorderList_table">
			<div class="reorderListHdr">
				<div class="row clearfix">
					<div class="col-sm-12 col-md-1">
						<div class="listLineNumberHeader">#</div>
					</div>
					<div class="col-sm-12 col-md-1">
						<div class="listImageHeader"></div>
					</div>
					<div class="col-sm-12 col-md-5">
						<div class="listNameHeader"><fmt:message key="product" /></div>
					</div>
					<div class="col-sm-12 col-md-3">
						<div class="listPriceHeader">Price</div>
					</div>
					<div class="col-sm-12 col-md-1"></div>
					<div class="col-sm-12 col-md-1">
						<div class="listQtyHeader"><fmt:message key="qty" /></div>
					</div>
				</div>
			</div>

			<div class="reorderListDetails">
				<c:forEach items="${model.myOrderLineItems.pageList}" var="product" varStatus="status">
					<c:set var="productLink">${_contextpath}/product.jhtm?id=${product.id}<c:if test="${model.product == null or siteConfig['RECOMMENDED_LIST_SHOW_BREADCRUMB'].value == 'true'}">&cid=<c:out value="${param.cid}"/></c:if></c:set>
					<c:if test="${gSiteConfig['gMOD_REWRITE'] == '1' and not fn:contains(product.sku, '/')}">
					  <c:set var="productLink">${_contextpath}/${siteConfig['MOD_REWRITE_PRODUCT'].value}/${product.encodedSku}/${product.encodedName}.html<c:if test="${model.product == null or siteConfig['RECOMMENDED_LIST_SHOW_BREADCRUMB'].value == 'true'}">?cid=<c:out value="${param.cid}"/></c:if></c:set>
					</c:if>	
					<input type="hidden" name="product.id" value="${product.id}">
					<c:set var="statusClass" value="odd_row" />
				    <c:if test="${(status.index + 1) % 2 == 0}">
				      <c:set var="statusClass" value="even_row" />
				    </c:if>
					<div class="${statusClass} clearfix">
						<div class="col-sm-12 col-md-1">
							<div class="listLineNumberHeader visible-xs visible-sm"><fmt:message key="line" /> #</div>
							<div class="listLineNumber"><c:out value ="${status.index + 1}" /></div>
							<div class="clearfix"></div>
						</div>
						<div class="col-sm-12 col-md-1">
							<div class="listImageHeader visible-xs visible-sm">&nbsp;</div>
							<div class="listImageWrapper">
								<c:if test="${product.thumbnail == null}"></c:if>
							    <c:if test="${product.thumbnail != null}">
									<c:choose>
								  		<c:when test="${product.asiId != null}">
								  	    	<img class="listImage img-responsive" src="<c:out value="${product.thumbnail.imageUrl}"/>" border="0" class="thumbnail_image" alt="<c:out value="${product.name}"/><c:if test="${product.shortDesc != ''}"> - <c:out value="${product.shortDesc}"/></c:if>">
								  	    </c:when>
								  	    <c:otherwise>
								  	    	<a href="<c:if test="${!product.thumbnail.absolute}">${_contextpath}/assets/Image/Product/detailsbig/</c:if>${product.thumbnail.imageUrl}" data-lightbox="product_images_${product.id}" data-title="<c:out value="${product.name}" escapeXml="false" />">
									          <img class="listImage img-responsive" src="<c:if test="${!product.thumbnail.absolute}">${_contextpath}/assets/Image/Product/thumb/</c:if>${product.thumbnail.imageUrl}" border="0" alt="${product.alt }" alt="<c:out value="${product.name}"/><c:if test="${product.shortDesc != ''}"> - <c:out value="${product.shortDesc}"/></c:if>"/>
									        </a>
								  	    </c:otherwise>
							  	  	</c:choose>
							  	</c:if>
							</div>
							<div class="clearfix"></div>
						</div>
						<div class="col-sm-12 col-md-5">
							<div class="listNameHeader visible-xs visible-sm"><fmt:message key="f_productName" /></div>
							<div class="listName">
							 <div class="listNameContent">
								<a href="${productLink}"><c:out value="${product.name}" escapeXml="false" /></a>
								<a href="${productLink}" class="list_item_sku"><c:out value="${product.sku}" escapeXml="false" /></a>
								<c:if test="${gSiteConfig['gADI']}"> 	
					          		<div class="list_item_brand"><c:out value="${product.manufactureName}" escapeXml="false"/></div>
							  	</c:if>
							 </div>
							</div>
							<div class="clearfix"></div>
						</div>
						<div class="col-sm-12 col-md-3">
							<div class="listPriceHeader visible-xs visible-sm">Price</div>
							<div class="listPriceInfoWrapper">
								<%@ include file="/WEB-INF/jsp/frontend/layout/template6/price/price.jsp" %>
							</div>
							<div class="clearfix"></div>
						</div>
						<div class="col-sm-12 col-md-1"></div>
						<div class="col-sm-12 col-md-1">
							<div class="listQtyHeader visible-xs visible-sm"><fmt:message key="qty" /></div>
							<div class="listQty">
						 	<c:choose>
							    <c:when test="${gSiteConfig['gINVENTORY'] and product.inventory != null and !product.negInventory and product.inventory <= 0}" >
							      <c:out value="${siteConfig['OUT_OF_STOCK_TITLE'].value}" escapeXml="false"/>
							    </c:when>
								<c:otherwise>
							      <c:set var="showAddToCart" value="true" />
							      <c:if test="${product.active}">
							      	<input type="text" autocomplete="off" size="6" maxlength="5" name="quantity_${product.id}" id="quantity_${product.id}" value="<c:out value="${cartItem.quantity}"/>" > 
							      </c:if>
							      <c:if test="${gSiteConfig['gINVENTORY'] and product.inventory != null and product.inventory < product.lowInventory}">
							      	<c:out value="${siteConfig['LOW_STOCK_TITLE'].value}" escapeXml="false"/>
							      </c:if>	      
							    </c:otherwise>
						 	</c:choose>    
							</div>
							<div class="clearfix"></div>
						</div>
					</div>
			
				</c:forEach>
				
			</div>

			<div class="reorderListFtr">
				<div class="row clearfix"></div>
			</div>
		</div>

		<div class="row">
			<div class="col-sm-12">
				<div class="addToCart_btn_wrapper">
					<button type="submit" class="btn addToCart_btn">Add To Cart</button>
				</div>
			</div>
		</div>
		</form>
	</div>
</div>

  
</c:if>

<c:if test="${fn:trim(model.myOrderListLayout.footerHtml) != ''}">
  <c:set value="${model.myOrderListLayout.footerHtml}" var="footerHtml"/>
  <c:set value="${fn:replace(footerHtml, '#firstname#', userSession.firstName)}" var="footerHtml"/>
  <c:set value="${fn:replace(footerHtml, '#lastname#', userSession.lastName)}" var="footerHtml"/>
  <c:set value="${fn:replace(footerHtml, '#email#', userSession.username)}" var="footerHtml"/>
  <c:out value="${footerHtml}" escapeXml="false"/>
</c:if>

  
  </tiles:putAttribute>
 </tiles:insertDefinition>