<%@ page session="false" %>
<%@ page import="java.util.*" %>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="/WEB-INF/tlds/spring.tld" prefix="spring" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ taglib uri="/WEB-INF/tlds/custom.tld" prefix="wj" %>

<c:set value="${false}" var="multibox"/>

  
<c:if test="${model.message != null}">
  <div class="message"><fmt:message key="${model.message}" /></div>
</c:if>

<c:if test="${model.product != null}">
<c:set value="${model.product}" var="product"/>



  
<%-- bread crumbs --%>
<c:import url="/WEB-INF/jsp/frontend/layout/${_template}/breadcrumbs.jsp" />


	<!-- image start -->
   		<div class="details_image_box">
			<c:forEach items="${model.product.images}" var="image" varStatus="status">
		  	<c:if test="${status.first}">
				<img src="<c:if test="${!image.absolute}">${_contextpath}/assets/Image/Product/detailsbig/</c:if><c:out value="${image.imageUrl}"/>" title="Image ${status.index}" class="cloudzoom img-responsive center-block" id="cloudzoom_1" data-cloudzoom="zoomImage:'<c:if test="${!image.absolute}">${_contextpath}/assets/Image/Product/superBig/</c:if><c:out value="${image.imageUrl}"/>', zoomSizeMode:'lens', captionPosition:'bottom', autoInside:500, touchStartDelay:100, lensWidth:150, lensHeight:150">
			</c:if>
			</c:forEach>
		</div>
		<div class="details_image_thumbnails">
			<ul class="clearfix">
				<c:forEach items="${model.product.images}" var="image" varStatus="status">
					<li>
						<img src="<c:if test="${!image.absolute}">${_contextpath}/assets/Image/Product/thumb/</c:if><c:out value="${image.imageUrl}"/>" title="Image ${status.index}" class="img-responsive cloudzoom-gallery" data-cloudzoom="useZoom:'#cloudzoom_1', image:'<c:if test="${!image.absolute}">${_contextpath}/assets/Image/Product/detailsbig/</c:if><c:out value="${image.imageUrl}"/>', zoomImage:'<c:if test="${!image.absolute}">${_contextpath}/assets/Image/Product/superBig/</c:if><c:out value="${image.imageUrl}"/>'">
					</li>
				</c:forEach>
			</ul>
		</div>
	<!-- image end -->

		
		<div class="details_item_name" id="details_item_name_id"><h1><c:out value="${product.name}" escapeXml="false" /></h1></div>
		<div class="details_sku" id="details_sku_id">Product Number: &nbsp; <c:out value="${product.sku}" /> </div>  
		<br/>
		<!-- Description Start -->
		<c:if test="${(siteConfig['DETAILS_SHORT_DESC'].value == 'true' and product.shortDesc != '') || product.longDesc != ''}">
		<div class="details_desc_wrapper">
			<div class="details_desc_title">Product Description</div>
			<div class="details_desc_content">
				<c:if test="${siteConfig['DETAILS_SHORT_DESC'].value == 'true' and product.shortDesc != ''}">
					<div class="details_short_desc"><c:out value="${product.shortDesc}" escapeXml="false" /></div>
				</c:if>
				<c:if test="${product.longDesc != ''}">
					<div class="details_long_desc"><c:out value="${product.longDesc}" escapeXml="false" /></div>
				</c:if>
			</div>
		</div>
		</c:if>
		<!-- Description End -->
		
	   <div class="hiddenProductId">
		<input type="hidden" value="${product.id}" name="product.id" id="product.id" />
	   </div>
	   <!-- Start Child DropDown -->
		 <c:if test="${fn:length(model.slavesList) gt 0}">
		   <div class="configBox_wrapper variant child">
			<div class="configBox_title">Select Product</div>
			<div class="configbox_content">
	            <select name="childSelect" onchange="updateChildProduct(this.value)">
				  <option value="">Please Select</option>
				  <c:forEach items="${model.slavesList}" var="childProduct" varStatus="statusPrice">
			  	    <c:if test="${(!empty childProduct.price or childProduct.priceByCustomer) and (!childProduct.loginRequire or userSession != null)}">
			          <c:if test="${(childProduct.type != 'box') and (empty childProduct.numCustomLines)}">
				        <c:if test="${!(gSiteConfig['gINVENTORY'] and childProduct.inventory != null and !childProduct.negInventory and childProduct.inventory <= 0)}">
				          <c:set var="showAddToCart" value="true" />
			              <option value="${childProduct.id}"  <c:if test="${product.id == childProduct.id}">selected="selected"</c:if>>
			              	<c:out value="${childProduct.field4}" escapeXml="false" />
				 	      </option>
			            </c:if>
				      </c:if>
			        </c:if>
			      </c:forEach>
				</select>
			 </div>
		   </div>
		  </c:if>
		<!-- End Child DropDown -->
		  
	  	<!-- Qty Block -->
          <div class="configBox_wrapper quantity">
           <c:set value="0" var="childMinQty"/>
           <c:if test="${fn:length(model.slavesList) gt 0}">
              <c:forEach items="${model.slavesList}" var="childProduct" varStatus="productStatus1" >
                 <c:if test="${productStatus1.first}">
                  <c:set value="${childProduct.minimumQty}" var="childMinQty"/> 
                 </c:if>
                 <c:if test="${childProduct.minimumQty < childMinQty}">
                  <c:set value="${childProduct.minimumQty}" var="childMinQty"/> 
                 </c:if>
              </c:forEach>
           </c:if>
           <div class="configBox_title">Enter Quantity <c:if test="${childMinQty >= 2}"><span class="headerMinQty">Min Qty: <c:out value="${childMinQty}" escapeXml="false" /></span></c:if></div>
           <div class="configbox_content">
               <div class="quantityList">
                  <div class="variantBlock sizeWrapperBox" id="selectedSizeBox_${product.field4}" >
                       <div class="qtyInputBlock">
                           <input type="text" autocomplete="off" size="5" maxlength="5" name="quantity_${product.id}" id="quantity_${product.id}" value="${product.minimumQty}" >
                       </div>
                       <c:if test="${product.inventory != null }">
                           <div class="inStockTitle">In Stock</div>
                           <div class="inStockValue"><c:out value="${product.inventory}"/></div>
                       </c:if>
                  </div>
			<div style="clear: both;"></div>
              </div>
           </div>
          </div>
          <!-- End Quantity -->
    
</c:if>