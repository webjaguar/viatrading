<%@ page session="false" %>
<%@ page import="com.webjaguar.model.LuceneProductSearch"%>
<%@ page import="java.util.*" %>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>




<tiles:insertDefinition name="${_template}" flush="true">
  <c:set var="contentClass" value="col-sm-12"></c:set>
  <c:if test="${gSiteConfig['gPRODUCT_FIELDS_UNLIMITED'] > 0 or siteConfig['SHOW_LUCENE_SEARCH_FILTER'].value == 'true'}">
	<tiles:putAttribute name="leftbar" value="/WEB-INF/jsp/frontend/layout/template6/lucene/searchFilters.jsp" />
    <c:set var="contentClass" value="col-sm-9 col-md-10"></c:set>
  </c:if>
  
  <tiles:putAttribute name="content" type="string">


<style>
.product_sku_wrapper {
    top: 0 !important;
}
.product_wrapper {
    margin-bottom: 15px !important;
}
</style>

<script type="text/javascript">
<!--
$(window).load(function() {
   $(window).resize(function(){
      if($(window).width() > 767) {
            var product_sku_wrapper_maxHeight = Math.max.apply(null, $(".product_sku_wrapper").map(function(){
                   return $(this).height();
            }).get());
            var product_image_wrapper_maxHeight = Math.max.apply(null, $(".product_image_wrapper").map(function(){
                return $(this).height();
         	}).get());
         	var product_name_wrapper_maxHeight = Math.max.apply(null, $(".product_name_wrapper").map(function(){
                   return $(this).height();
            }).get());
            var product_wrapper_maxHeight = Math.max.apply(null, $(".product_wrapper").map(function(){
                   return $(this).height();
            }).get());
            $(".product_sku_wrapper").height(product_sku_wrapper_maxHeight);
            $(".product_image_wrapper").height(product_image_wrapper_maxHeight);
            $(".product_name_wrapper").height(product_name_wrapper_maxHeight);
            $(".product_wrapper").height(product_wrapper_maxHeight);
      }
      else {
            $(".product_image_wrapper").css('height','auto');
            $(".product_name_wrapper").css('height','auto');
            $(".product_wrapper").css('height','auto');
            $(".product_sku_wrapper").css('height','auto');
	  }
   }).trigger('resize');
});
//-->
</script>

<div class="${contentClass}">


<%-- bread crumbs --%>
<c:if test="${model.breadcrumb != null}">
  <c:import url="/WEB-INF/jsp/frontend/layout/template6/breadcrumbs.jsp" />
</c:if>

<div class="row">
  <div class="col-sm-12">
    <%--Hide this top level search box as it will appear from the header and not part of design --%>
    <c:if test="${false}">
    <form action="lsearch.jhtm" method="post"  id="searchFormId" name="search" <c:if test="${model.hideSearchBox}">style="display: none;"</c:if> >
      <input type="hidden" name="page" id="page" value="1">
      <div class="searchFormContainer clearfix">
		<div class="searchFormHeader clearfix"></div>
		<div class="searchForm clearfix">
		  <div class="searchBox clearfix">
			<div class="searchKeywordWrapper" id="searchKeywordWrapperId">
			  <div class="searchName"><fmt:message key="keywords" />:</div>
			  <div class="searchInput">
				<input type="text" name="keywords" class="searchText" value="<c:out value="${lProductSearch.keywords}"/>" size="50" id="search_keywords" autocomplete="off"/>
      		    
      		    <c:if test="${model.minCharsMet != null and not model.minCharsMet}">
		    	  <div style="font-weight:bold;color:#990000">
		    	 	<fmt:message key="f_keywordsMinCharsError">
					  <fmt:param><c:out value="${siteConfig['SEARCH_KEYWORDS_MINIMUM_CHARACTERS'].value}" /></fmt:param>
				    </fmt:message>
				  </div>
		        </c:if>
		        <c:if test="${model.hasKeywordsLessMinChars and model.minCharsMet}">
		    	  <div style="font-weight:bold;color:#990000">
		    	    <fmt:message key="f_keywordsMinCharsIgnore">
					  <fmt:param><c:out value="${siteConfig['SEARCH_KEYWORDS_MINIMUM_CHARACTERS'].value}" /></fmt:param>
				    </fmt:message>
				  </div>
		        </c:if>
      		  </div>
			</div>
		    
		    <%--
		    <div class="searchConjunctionWrapper" id="searchConjunctionWrapperId">
			  <label id="scOr" class="radio-inline">
				<input type="radio" name="comp" value="OR"> at least one word
			  </label>
			  <label id="scAnd" class="radio-inline">
				<input type="radio" name="comp" value="AND">all words
			  </label>
			</div>
			 --%>
		    
			
			<c:if test="${false and siteConfig['SEARCH_MINMAXPRICE'].value == 'true'}">
  			<div class="searchBox_clear_1"></div>
			<div class="searchPriceWrapper" id="searchPriceWrapperId">
			  <div class="searchName"><fmt:message key="price" /></div>
			  <div class="searchInput">
				<input type="text" name="minPrice" class="minPrice">
				<span class="searcPriceRangeSeparator">-</span>
				<input type="text" name="maxPrice" class="maxPrice">
			  </div>
			</div>
			</c:if>
			
			<%@ include file="/WEB-INF/jsp/frontend/lucene/searchCustom.jsp" %>
  			<div class="submitSearch" id="searchButton">
			  <button type="submit" class="btn search_btn">
				<fmt:message key="search" />
			  </button>
			</div>
		  </div>
		</div>
	  </div>
	</form>
	</c:if>
    <div class="searchBox_clear_1"></div>
			
  </div>
</div>



<c:if test="${model.productSearchLayout.headerHtml!=''}" >
 <c:out value="${model.productSearchLayout.headerHtml}" escapeXml="false"/>    
</c:if>  


<c:if test="${model.count == 0 or fn:length(model.results) == 0}">
	<%-- If indexing is not completed and products are made inactive, it shows pagination without search results --%>
	<span id="nothing_found">Your search for <b><c:out value="${lProductSearch.keywords}" /></b> returned no results. Please try entering different search keywords.</span>
</c:if>

<c:if test="${model.count > 0 and fn:length(model.results) > 0}">
<c:if test="${fn:length(model.comparisonMap) > 0}">
<div class="row">
   <div class="col-sm-12">
      <div class="comparedProductsThumbnailsWrapper">
		<%--
		<div class="comparedProductsThumbnails">
		  <ul>
			<c:forEach items="${model.comparisonMap}" var="">
			<li>
			  <a href="#"><span class="removeFromCompareList"><i class="fa fa-times-circle"></i></span></a>
			  <img src="img/products/product_thumbnail_65x65.jpg">
			</li>
		  	</c:forEach>
		  </ul>
		</div>
		 --%>
		<div class="compareButtonWrapper">
		  <a href="/comparison.jhtm" class="btn compareButton">View Comparison</a>
		</div>
	  </div>
   </div>
</div>
</c:if>

<div class="row">
  <div class="col-sm-12">
	<div class="pageNavigation">
	  <form name="pagination" id="paginationFormId" class="formPageNavigation clearfix" action="${_contextpath}/lsearch.jhtm">
	  
		<div style="display: none;">
			<c:set var="appnedToLink" value=""/>
			<c:if test="${lProductSearch.sort != null }">
			  <c:set var="appnedToLink" value="${appnedToLink}&sort=${lProductSearch.sort}"/>
			</c:if>
			<c:if test="${lProductSearch.minPrice != null }">
			  <input type="hidden" name="minPrice" value="${lProductSearch.minPrice}"/>
			  <c:set var="appnedToLink" value="${appnedToLink}&minPrice=${lProductSearch.minPrice}"/>
			</c:if>
			<c:if test="${lProductSearch.maxPrice != null }">
			  <input type="hidden" name="maxPrice" value="${lProductSearch.maxPrice}"/>
			  <c:set var="appnedToLink" value="${appnedToLink}&maxPrice=${lProductSearch.maxPrice}"/>
			</c:if>
			<c:if test="${lProductSearch.extraFieldMap['madeIn'] == 'USA'}">
			  <input type="hidden" name="madeIn" value="USA"/>
			  <c:set var="appnedToLink" value="${appnedToLink}&madeIn=USA"/>
			</c:if> 
			<c:if test="${lProductSearch.extraFieldMap['eco'] == 'true'}">
			  <input type="hidden" name="eco" value="true"/>
			  <c:set var="appnedToLink" value="${appnedToLink}&eco=true"/>
			</c:if> 
			<c:if test="${lProductSearch.minQty != null}">
			  <input type="hidden" name="minQty" value="${lProductSearch.minQty}"/>
			  <c:set var="appnedToLink" value="${appnedToLink}&minQty=${lProductSearch.minQty}"/>
			</c:if> 
			<c:if test="${lProductSearch.maxQty != null}">
			  <input type="hidden" name="maxQty" value="${lProductSearch.maxQty}"/>
			  <c:set var="appnedToLink" value="${appnedToLink}&maxQty=${lProductSearch.maxQty}"/>
			</c:if> 
			<c:if test="${lProductSearch.salesTagActive != null}">
			  <input type="hidden" name="st" value="${lProductSearch.salesTagActive}"/>
			  <c:set var="appnedToLink" value="${appnedToLink}&st=${lProductSearch.salesTagActive}"/>
			</c:if> 
			<c:if test="${lProductSearch.specialPricingAvailable != null}">
			  <input type="hidden" name="sp" value="${lProductSearch.specialPricingAvailable}"/>
			  <c:set var="appnedToLink" value="${appnedToLink}&sp=${lProductSearch.specialPricingAvailable}"/>
			</c:if> 
			<c:if test="${lProductSearch.noKeywords != null}">
			  <input type="hidden" name="noKeywords" value="${lProductSearch.noKeywords}"/>
			  <c:set var="appnedToLink" value="${appnedToLink}&noKeywords=${lProductSearch.noKeywords}"/>
			</c:if> 
			<c:if test="${lProductSearch.supplierAccountNumber != null}">
			  <input type="hidden" name="s" value="${lProductSearch.supplierAccountNumber}"/>
			  <c:set var="appnedToLink" value="${appnedToLink}&s=${lProductSearch.supplierAccountNumber}"/>
			</c:if> 
			<c:if test="${model.cid != null }">
			  <input type="hidden" name="cid" value="${model.cid}"/>
			  <c:set var="appnedToLink" value="${appnedToLink}&cid=${model.cid}"/>
			</c:if>
			<c:if test="${model.searchIndexedCatId != null }">
			  <input type="hidden" name="searchIndexedCatId" value="${model.searchIndexedCatId}"/>
			  <c:set var="appnedToLink" value="${appnedToLink}&searchIndexedCatId=${model.searchIndexedCatId}"/>
			</c:if>
			<c:if test="${model.hideSearchBox}">
			  <input type="hidden" name="hideSearchBox" value="${model.hideSearchBox}"/>
			  <c:set var="appnedToLink" value="${appnedToLink}&hideSearchBox=${model.hideSearchBox}"/>
			</c:if>
			<c:forEach items="${lProductSearch.filters}" var="filter">
			  <input type="hidden" name="facetNameValue" id="filter${filter.parent}" value="${fn:escapeXml(filter.parent)}_value_${fn:escapeXml(filter.name)}"></input>
			  <c:set var="appnedToLink" value="${appnedToLink}&facetNameValue=${fn:escapeXml(filter.parent)}_value_${fn:escapeXml(filter.name)}"/>
			</c:forEach>
			
			<c:set var="appnedToLink" value="${appnedToLink}&view=${param.view}"/>
			<input type="hidden" name="view" id="view" value="<c:out value='${param.view}' />" />
		</div>
	  
	  	<c:if test="${gSiteConfig['gMOD_REWRITE'] != '1'}">
    	  <input type="hidden" name="cid" value="${model.thisCategory.id}">
    	</c:if>
      	<div class="pageShowing">
		  <span>Showing ${model.start + 1}-${model.pageEnd} of ${model.count}</span>
		</div>
		<div class="pageNavigationControl">
		  <c:if test="${siteConfig['PRODUCT_SORT_BY_FRONTEND'].value == 'true'}">
		  <div class="sortby">
			<span><fmt:message key="f_sortby"/></span>
			<select class="selectpicker" name="sort" onchange="submit();">
			  <c:choose>
				  <c:when test="${gSiteConfig['gADI']}">
	    		    <option value="rlv" <c:if test="${lProductSearch.sort == 'rlv'}">selected="selected"</c:if>>Relevance</option>
	    		    <option value="name" <c:if test="${lProductSearch.sort == 'name'}">selected="selected"</c:if>>Product Name</option>
				    <option value="braz" <c:if test="${lProductSearch.sort == 'braz'}">selected="selected"</c:if>>Brand A-Z</option>
				    <option value="brza" <c:if test="${lProductSearch.sort == 'brza'}">selected="selected"</c:if>>Brand Z-A</option>
				    <option value="lth" <c:if test="${lProductSearch.sort == 'lth'}">selected="selected"</c:if>>Price (Low to High)</option>
			        <option value="htl" <c:if test="${lProductSearch.sort == 'htl'}">selected="selected"</c:if>>Price (High to Low)</option>
				  </c:when>	
			      <c:otherwise>
	    		    <option value="" <c:if test="${lProductSearch.sort == ''}">selected="selected"</c:if>>Please Select</option>
				    <option value="name" <c:if test="${lProductSearch.sort == 'name'}">selected="selected"</c:if>>Name</option>
				    <option value="lth" <c:if test="${lProductSearch.sort == 'lth'}">selected="selected"</c:if>>Price ($ to $$)</option>
			        <option value="htl" <c:if test="${lProductSearch.sort == 'htl'}">selected="selected"</c:if>>Price ($$ to $)</option>
				  </c:otherwise>	
		      </c:choose>
		    </select>
		  </div>
		  </c:if>
		  <div class="pagesize">
			<span><fmt:message key="f_pageSize"/></span>
			<select name="size" class="selectpicker" onchange="document.getElementById('page').value=1;submit();">   
			  <c:forEach items="${fn:split(siteConfig['PRODUCT_PAGE_SIZE'].value, ',')}"  var="current">
				<option value="${current}" <c:if test= "${current == lProductSearch.pageSize}">selected</c:if>>${current} <fmt:message key="perPage" /></option>
			  </c:forEach>
			</select>
		  </div>
		  <c:if test="${model.pageCount > 1}">
		  <div class="page">
			<span><fmt:message key="page"/></span>
			<c:choose>
		      <c:when test="${model.pageCount <= 100 and model.pageCount > 0}">
		        
		        <select class="selectpicker" name="page" id="page" onchange="submit()">
				  <c:forEach begin="1" end="${model.pageCount}" var="page">
		  	        <option value="${page}" <c:if test="${page == (lProductSearch.page)}">selected</c:if>>${page}</option>
		          </c:forEach>
				</select>
				<span>of ${model.pageCount}</span>
		      </c:when>
		      <c:otherwise>
		        <input type="text" id="page" name="page" value="${lProductSearch.page}" size="5" class="textfield50" />
		        <input type="submit" value="go"/>
		      </c:otherwise>
		    </c:choose>
		  </div>
		  
		  <div class="pageNav">
			<span class="pageNavLink">
			  <c:if test="${lProductSearch.page == 1}"><a href="#"><span class="pageNavPrev"><i class="fa fa-angle-double-left fa-fw"></i></span></a></c:if>
			  <c:if test="${lProductSearch.page != 1}"><a href="${_contextpath}/lsearch.jhtm?page=${lProductSearch.page-1}${appnedToLink}"><span class="pageNavPrev"><i class="fa fa-angle-double-left fa-fw"></i></span></a></c:if>
			</span>
			<span class="pageNavLink">
			  <c:if test="${lProductSearch.page == model.pageCount}"><a href="#"><span class="pageNavNext"><i class="fa fa-angle-double-right fa-fw"></i></span></a></c:if>
			  <c:if test="${lProductSearch.page != model.pageCount}"><a href="${_contextpath}/lsearch.jhtm?page=${lProductSearch.page+1}${appnedToLink}"><span class="pageNavNext"><i class="fa fa-angle-double-right fa-fw"></i></span></a></c:if>
			</span>
		  </div>
		  </c:if>
		</div>
	  </form>
	</div>
  </div>
</div>
</c:if>

<div class="row">
  <input type="hidden" id="maxComparisons" value="${model.maxComparisons}">
  <input type="hidden" id="currentComparison" value="${fn:length(model.comparisonMap)}">
  
  <c:forEach items="${model.results}" var="product" varStatus="status">  
    <c:set var="productLink">${_contextpath}/product.jhtm?id=${product.id}<c:if test="${model.product == null or siteConfig['RECOMMENDED_LIST_SHOW_BREADCRUMB'].value == 'true'}">&cid=<c:out value="${param.cid}"/></c:if></c:set>
	<c:if test="${gSiteConfig['gMOD_REWRITE'] == '1' and not fn:contains(product.sku, '/')}">
  	  <c:set var="productLink">${_contextpath}/${siteConfig['MOD_REWRITE_PRODUCT'].value}/${product.encodedSku}/${product.encodedName}.html<c:if test="${model.product == null or siteConfig['RECOMMENDED_LIST_SHOW_BREADCRUMB'].value == 'true'}">?cid=<c:out value="${param.cid}"/></c:if></c:set>
    </c:if>
	<c:choose>
		<c:when test="${siteConfig['SEARCH_DISPLAY_MODE'].value == '28-R'}">
	      <%@ include file="/WEB-INF/jsp/frontend/layout/template6/thumbnail/view1.jsp" %>
	    </c:when> 
	    <c:when test="${siteConfig['SEARCH_DISPLAY_MODE'].value == '36'}">
	      <%@ include file="/WEB-INF/jsp/frontend/layout/template6/thumbnail/view3.jsp" %>
	    </c:when> 
	    <c:otherwise>
	      <%@ include file="/WEB-INF/jsp/frontend/layout/template6/thumbnail/view2.jsp" %>
	    </c:otherwise> 
	</c:choose>
  </c:forEach>
</div>

<c:if test="${model.count > 0 and fn:length(model.results) > 0}">


<c:if test="${fn:length(model.comparisonMap) > 0}">
<div class="row">
   <div class="col-sm-12">
      <div class="comparedProductsThumbnailsWrapper">
		<%--
		<div class="comparedProductsThumbnails">
		  <ul>
			<c:forEach items="${model.comparisonMap}" var="">
			<li>
			  <a href="#"><span class="removeFromCompareList"><i class="fa fa-times-circle"></i></span></a>
			  <img src="img/products/product_thumbnail_65x65.jpg">
			</li>
		  	</c:forEach>
		  </ul>
		</div>
		 --%>
		<div class="compareButtonWrapper">
		  <a href="/comparison.jhtm" class="btn compareButton">View Comparison</a>
		</div>
	  </div>
   </div>
</div>
</c:if>

<div class="row">
  <div class="col-sm-12">
	<div class="pageNavigation">
	  <form name="pagination" id="paginationFormId" class="formPageNavigation clearfix" action="${_contextpath}/lsearch.jhtm">
	  	<div style="display: none;">
			<c:set var="appnedToLink" value=""/>
			<c:if test="${lProductSearch.sort != null }">
			  <input type="hidden" name="sort" value="${lProductSearch.sort}"/>
			  <c:set var="appnedToLink" value="${appnedToLink}&sort=${lProductSearch.sort}"/>
			</c:if>
			<c:if test="${lProductSearch.minPrice != null }">
			  <input type="hidden" name="minPrice" value="${lProductSearch.minPrice}"/>
			  <c:set var="appnedToLink" value="${appnedToLink}&minPrice=${lProductSearch.minPrice}"/>
			</c:if>
			<c:if test="${lProductSearch.maxPrice != null }">
			  <input type="hidden" name="maxPrice" value="${lProductSearch.maxPrice}"/>
			  <c:set var="appnedToLink" value="${appnedToLink}&maxPrice=${lProductSearch.maxPrice}"/>
			</c:if>
			<c:if test="${lProductSearch.extraFieldMap['madeIn'] == 'USA'}">
			  <input type="hidden" name="madeIn" value="USA"/>
			  <c:set var="appnedToLink" value="${appnedToLink}&madeIn=USA"/>
			</c:if> 
			<c:if test="${lProductSearch.extraFieldMap['eco'] == 'true'}">
			  <input type="hidden" name="eco" value="true"/>
			  <c:set var="appnedToLink" value="${appnedToLink}&eco=true"/>
			</c:if> 
			<c:if test="${lProductSearch.minQty != null}">
			  <input type="hidden" name="minQty" value="${lProductSearch.minQty}"/>
			  <c:set var="appnedToLink" value="${appnedToLink}&minQty=${lProductSearch.minQty}"/>
			</c:if> 
			<c:if test="${lProductSearch.maxQty != null}">
			  <input type="hidden" name="maxQty" value="${lProductSearch.maxQty}"/>
			  <c:set var="appnedToLink" value="${appnedToLink}&maxQty=${lProductSearch.maxQty}"/>
			</c:if> 
			<c:if test="${lProductSearch.salesTagActive != null}">
			  <input type="hidden" name="st" value="${lProductSearch.salesTagActive}"/>
			  <c:set var="appnedToLink" value="${appnedToLink}&st=${lProductSearch.salesTagActive}"/>
			</c:if> 
			<c:if test="${lProductSearch.specialPricingAvailable != null}">
			  <input type="hidden" name="sp" value="${lProductSearch.specialPricingAvailable}"/>
			  <c:set var="appnedToLink" value="${appnedToLink}&sp=${lProductSearch.specialPricingAvailable}"/>
			</c:if> 
			<c:if test="${lProductSearch.noKeywords != null}">
			  <input type="hidden" name="noKeywords" value="${lProductSearch.noKeywords}"/>
			  <c:set var="appnedToLink" value="${appnedToLink}&noKeywords=${lProductSearch.noKeywords}"/>
			</c:if> 
			<c:if test="${lProductSearch.supplierAccountNumber != null}">
			  <input type="hidden" name="s" value="${lProductSearch.supplierAccountNumber}"/>
			  <c:set var="appnedToLink" value="${appnedToLink}&s=${lProductSearch.supplierAccountNumber}"/>
			</c:if> 
			<c:if test="${model.cid != null }">
			  <input type="hidden" name="cid" value="${model.cid}"/>
			  <c:set var="appnedToLink" value="${appnedToLink}&cid=${model.cid}"/>
			</c:if>
			<c:if test="${model.searchIndexedCatId != null }">
			  <input type="hidden" name="searchIndexedCatId" value="${model.searchIndexedCatId}"/>
			  <c:set var="appnedToLink" value="${appnedToLink}&searchIndexedCatId=${model.searchIndexedCatId}"/>
			</c:if>
			<c:if test="${model.hideSearchBox}">
			  <input type="hidden" name="hideSearchBox" value="${model.hideSearchBox}"/>
			  <c:set var="appnedToLink" value="${appnedToLink}&hideSearchBox=${model.hideSearchBox}"/>
			</c:if>
			<c:forEach items="${lProductSearch.filters}" var="filter">
			  <input type="hidden" name="facetNameValue" id="filter${filter.parent}" value="${fn:escapeXml(filter.parent)}_value_${fn:escapeXml(filter.name)}"></input>
			  <c:set var="appnedToLink" value="${appnedToLink}&facetNameValue=${fn:escapeXml(filter.parent)}_value_${fn:escapeXml(filter.name)}"/>
			</c:forEach>
			
			<c:set var="appnedToLink" value="${appnedToLink}&view=${param.view}"/>
			<input type="hidden" name="view" id="view" value="<c:out value='${param.view}' />" />
		</div>
	  	<c:if test="${gSiteConfig['gMOD_REWRITE'] != '1'}">
    	  <input type="hidden" name="cid" value="${model.thisCategory.id}">
    	</c:if>
      	<div class="pageShowing">
		  <span>Showing ${model.start + 1}-${model.pageEnd} of ${model.count}</span>
		</div>
		<div class="pageNavigationControl">
		  <c:if test="${siteConfig['PRODUCT_SORT_BY_FRONTEND'].value == 'true'}">
		  <div class="sortby">
			<span><fmt:message key="f_sortby"/></span>
			<select class="selectpicker" name="sort" onchange="submit();">
				<c:choose>
				<c:when test="${gSiteConfig['gADI']}">
	    		    <option value="rlv" <c:if test="${lProductSearch.sort == 'rlv'}">selected="selected"</c:if>>Relevance</option>
	    		    <option value="name" <c:if test="${lProductSearch.sort == 'name'}">selected="selected"</c:if>>Product Name</option>
				    <option value="braz" <c:if test="${lProductSearch.sort == 'braz'}">selected="selected"</c:if>>Brand A-Z</option>
				    <option value="brza" <c:if test="${lProductSearch.sort == 'brza'}">selected="selected"</c:if>>Brand Z-A</option>
				    <option value="lth" <c:if test="${lProductSearch.sort == 'lth'}">selected="selected"</c:if>>Price (Low to High)</option>
			        <option value="htl" <c:if test="${lProductSearch.sort == 'htl'}">selected="selected"</c:if>>Price (High to Low)</option>
				  </c:when>	
			      <c:otherwise>
	    		    <option value="" <c:if test="${lProductSearch.sort == ''}">selected="selected"</c:if>>Please Select</option>
				    <option value="name" <c:if test="${lProductSearch.sort == 'name'}">selected="selected"</c:if>>Name</option>
				    <option value="lth" <c:if test="${lProductSearch.sort == 'lth'}">selected="selected"</c:if>>Price ($ to $$)</option>
			        <option value="htl" <c:if test="${lProductSearch.sort == 'htl'}">selected="selected"</c:if>>Price ($$ to $)</option>
				  </c:otherwise>
				  </c:choose>
			</select>
		  </div>
		  </c:if>
		  <div class="pagesize">
			<span><fmt:message key="f_pageSize"/></span>
			<select name="size" class="selectpicker" onchange="document.getElementById('page').value=1;submit();">   
			  <c:forEach items="${fn:split(siteConfig['PRODUCT_PAGE_SIZE'].value, ',')}"  var="current">
				<option value="${current}" <c:if test= "${current == lProductSearch.pageSize}">selected</c:if>>${current} <fmt:message key="perPage" /></option>
			  </c:forEach>
			</select>
		  </div>
		  <c:if test="${model.pageCount > 1}">
		  <div class="page">
			<span><fmt:message key="page"/></span>
			<c:choose>
		      <c:when test="${model.pageCount <= 100 and model.pageCount > 0}">
		        
		        <select class="selectpicker" name="page" id="page" onchange="submit()">
				  <c:forEach begin="1" end="${model.pageCount}" var="page">
		  	        <option value="${page}" <c:if test="${page == (lProductSearch.page)}">selected</c:if>>${page}</option>
		          </c:forEach>
				</select>
				<span>of ${model.pageCount}</span>
		      </c:when>
		      <c:otherwise>
		        <input type="text" id="page" name="page" value="${lProductSearch.page}" size="5" class="textfield50" />
		        <input type="submit" value="go"/>
		      </c:otherwise>
		    </c:choose>
		  </div>
		  <div class="pageNav">
			<span class="pageNavLink">
			  <c:if test="${lProductSearch.page == 1}"><a href="#"><span class="pageNavPrev"><i class="fa fa-angle-double-left fa-fw"></i></span></a></c:if>
			  <c:if test="${lProductSearch.page != 1}"><a href="${_contextpath}/lsearch.jhtm?page=${lProductSearch.page-1}${appnedToLink}"><span class="pageNavPrev"><i class="fa fa-angle-double-left fa-fw"></i></span></a></c:if>
			</span>
			<span class="pageNavLink">
			  <c:if test="${lProductSearch.page == model.pageCount}"><a href="#"><span class="pageNavNext"><i class="fa fa-angle-double-right fa-fw"></i></span></a></c:if>
			  <c:if test="${lProductSearch.page != model.pageCount}"><a href="${_contextpath}/lsearch.jhtm?page=${lProductSearch.page+1}${appnedToLink}"><span class="pageNavNext"><i class="fa fa-angle-double-right fa-fw"></i></span></a></c:if>
			</span>
		  </div>
		  </c:if>
		</div>
	  </form>
	</div>
  </div>
</div>
</c:if>



<c:if test="${model.count > 0 && model.pageCount > 1}">
<c:out value="${pageShowing}" escapeXml="false" />
</c:if>

<c:if test="${model.productSearchLayout.footerHtml!=''}">
 <c:out value="${model.productSearchLayout.footerHtml}" escapeXml="false"/>    
</c:if> 

</div>

<script type="text/javascript">
<!--
function checkNumber( aNumber ) {
	var goodChars = "0123456789";
	var i = 0;
	if ( aNumber == "" ) {
		return 0; //empty
	}
	for ( i = 0 ; i <= aNumber.length - 1 ; i++ ) {
		if ( goodChars.indexOf( aNumber.charAt(i) ) == -1 ) 
			return -1; //invalid number			
	}	
	return 1; //valid number
}
function checkForm() {
	var productList = document.getElementsByName('product.id');
	var allQtyEmpty = true;
	for(var i=0; i<productList.length; i++)  {
	    var el = document.getElementById("quantity_" + productList[i].value);
		if ( el != null ) {
			if ( checkNumber( el.value ) == 1  ) {
				allQtyEmpty = false;
			} else if ( checkNumber( el.value ) == -1 ) {
				alert("invalid Quantity");
				el.focus();
				return false;
			}			
		}	
	}
	if ( allQtyEmpty ) {
		alert("Please Enter Quantity.");
		return false;
	} else
		return true;	
}

function compareProduct(productId){
	if(parseInt($('#currentComparison').val()) < parseInt($('#maxComparisons').val())) {
		
		$.ajax({
			url: "${_contextpath}/ajaxAddToCompare.jhtm?&productId="+productId,
			context: document.body
		}).done(function() {
			$('#compare-product'+productId).html('<a href=\"${_contextpath}/comparison.jhtm\" class=\"viewComparison_btn\">View Comparison</a>');
			$('#compare-product'+productId).attr('class', 'viewComparison_btn_wrapper');
			
			$('#currentComparison').val(parseInt($('#currentComparison').val()) + 1);
		});
	} else {
		alert('Max comparison limit reached.');
	}
}
//-->
</script>
   
  </tiles:putAttribute>
</tiles:insertDefinition>
