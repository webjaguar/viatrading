<%@ page session="false" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>

<div><fmt:message key="shoppingcart.cardType" />: <c:out value="${creditCard.type}"/></div>
<div>
	<fmt:message key="shoppingcart.cardNumber" />:
	<c:choose>
	<c:when test="${fn:length(creditCard.number) <= 4}">
	  <c:out value="${creditCard.number}"/>
	</c:when>
	<c:otherwise>
	  <c:forEach begin="1" end="${fn:length(creditCard.number)-4}" var="current"><c:out value="*"/></c:forEach>
	  ${fn:substring(creditCard.number, fn:length(creditCard.number)-4, fn:length(creditCard.number))}
	</c:otherwise>
	</c:choose>
</div>
<div><fmt:message key="expirationDate" />: <c:out value="${creditCard.expireMonth}"/>/<c:out value="${creditCard.expireYear}"/></div>
