<%@ page session="false" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>

<c:if test="${model.productReviewList.nrOfElements > 0}">
 <form id="productReviewForm" class="form-horizontal" role="form" method="POST" action="#">
        <div class="reviewList">
                <c:choose>
              <c:when test="${siteConfig['PRODUCT_REVIEW_TYPE'].value != 'anonymous'}">
                <a class="reviewProductLink" href="${_contextpath}/addProductReview.jhtm?sku=${model.product.sku}&cid=${model.cid}" ><fmt:message key="writeReview" /></a>
              </c:when>
              <c:when test="${siteConfig['PRODUCT_REVIEW_TYPE'].value == 'anonymous'}">
                <a class="reviewProductLink" href="${_contextpath}/productReviewAnon.jhtm?sku=${model.product.sku}&cid=${model.cid}" ><fmt:message key="writeReview" /> </a>
              </c:when>
            </c:choose>
            <div class="row">
                	<div class="col-sm-12">
                <c:forEach items="${model.productReviewList.pageList}" var="review" varStatus="status">
                
                        <div class="review">
                                <div class="reviewRate">
                                        <div class="product_rating">
                                                <div class="rating_stars">
                                                    <c:choose>
                                                         <c:when test="${review.rate == null}"><img src="${_contextpath}/assets/Image/Layout/star_0.gif" alt=<c:out value="${review.rate}"/> title="<c:out value="${review.rate}"/>" /></c:when>
                                                         <c:when test="${review.rate > 0 and review.rate <= 0.6}"><img src="${_contextpath}/assets/Image/Layout/star_0h.gif" alt=<c:out value="${review.rate}"/>Stars title="<c:out value="${review.rate}"/>"/></c:when>
                                                         <c:when test="${review.rate > 0.6 and review.rate <= 1}"><img src="${_contextpath}/assets/Image/Layout/star_1.gif" alt=<c:out value="${review.rate}"/> title="<c:out value="${review.rate}"/>"/></c:when>
                                                         <c:when test="${review.rate > 1 and review.rate <= 1.6}"><img src="${_contextpath}/assets/Image/Layout/star_1h.gif" alt=<c:out value="${review.rate}"/> title="<c:out value="${review.rate}"/>" /></c:when>
                                                         <c:when test="${review.rate > 1.6 and review.rate <= 2}"><img src="${_contextpath}/assets/Image/Layout/star_2.gif" alt=<c:out value="${review.rate}"/> title="<c:out value="${review.rate}"/>"/></c:when>
                                                         <c:when test="${review.rate > 2 and review.rate <= 2.6}"><img src="${_contextpath}/assets/Image/Layout/star_2h.gif" alt=<c:out value="${review.rate}"/> title="<c:out value="${review.rate}"/>"/></c:when>
                                                         <c:when test="${review.rate > 2.6 and review.rate <= 3}"><img src="${_contextpath}/assets/Image/Layout/star_3.gif" alt=<c:out value="${review.rate}"/> title="<c:out value="${review.rate}"/>"/></c:when>
                                                         <c:when test="${review.rate > 3 and review.rate <= 3.6}"><img src="${_contextpath}/assets/Image/Layout/star_3h.gif" alt=<c:out value="${review.rate}"/> title="<c:out value="${review.rate}"/>"/></c:when>
                                                         <c:when test="${review.rate > 3.6 and review.rate <= 4}"><img src="${_contextpath}/assets/Image/Layout/star_4.gif" alt=<c:out value="${review.rate}"/> Stars title="<c:out value="${review.rate}"/>"/></c:when>
                                                         <c:when test="${review.rate > 4 and review.rate <= 4.6}"><img src="${_contextpath}/assets/Image/Layout/star_4h.gif" alt=<c:out value="${review.rate}"/> Stars title="<c:out value="${review.rate}"/>"/></c:when>
                                                         <c:when test="${review.rate > 4.6}"><img src="${_contextpath}/assets/Image/Layout/star_5.gif" alt=<c:out value="${review.rate}"/> title="<c:out value="${review.rate}"/>"/></c:when>
                                                    </c:choose>
                                                </div>
                                  				<div class="clearfix"></div>
                                        </div>
                                </div>
                            <div class="reviewTitle"><c:out value="${review.title}"/></div>
                            <div class="clearfix"></div>
                            <div class="reviewer">By: <c:out value="${review.userName}"/></div>
                            <div class="reviewDate"><fmt:formatDate  value="${review.created}"/></div>
                            <div class="reviewText">
                            	<c:out value="${review.text}" escapeXml="false"/>
                            </div>
                        </div>
                     
                </c:forEach>
                 </div>
                  </div>   
        </div>
    </form>
</c:if>