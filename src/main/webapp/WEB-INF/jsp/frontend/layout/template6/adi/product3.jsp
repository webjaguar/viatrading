<%@ page session="false" %>
<%@ page import="java.util.*" %>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="/WEB-INF/tlds/spring.tld" prefix="spring" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ taglib uri="/WEB-INF/tlds/custom.tld" prefix="wj" %>


<tiles:insertDefinition name="${_template}" flush="true">
  
  
  <c:if test="${_leftBar != '1' and _leftBar != '4'}">
    <tiles:putAttribute name="leftbar" value="/WEB-INF/jsp/frontend/layout/template${_leftBar}/leftbar.jsp" />
  </c:if>
  <tiles:putAttribute name="content" type="string">

<c:if test="${fn:trim(model.productLayout.headerHtml) != ''}">
  <c:set value="${model.productLayout.headerHtml}" var="headerHtml"/>
  <%@ include file="/WEB-INF/jsp/frontend/common/headerDynamicElement.jsp" %>
  <div><c:out value="${headerHtml}" escapeXml="false"/></div>
</c:if>

<c:if test="${model.message != null}">
  <div class="message"><fmt:message key="${model.message}" /></div>
</c:if>

<c:if test="${model.product != null}">
<c:set value="${model.product}" var="product"/>

<%-- bread crumbs --%>
<c:if test="${model.breadCrumbs != null and fn:length(model.breadCrumbs) > 0}">
  <c:import url="/WEB-INF/jsp/frontend/layout/${_template}/breadcrumbs.jsp" />
</c:if>

<div class="col-sm-12">

<div id="product_details" class="row">
  <div class="col-sm-4">
	<div class="details_image_box">
	   <a href="<c:if test="${!model.product.images[0].absolute}">${_contextpath}/assets/Image/Product/detailsbig/</c:if><c:out value="${model.product.images[0].imageUrl}"/>" data-lightbox="product_images" data-title="<c:out value="${product.name}" escapeXml="false" />">
		<img src="<c:if test="${!model.product.images[0].absolute}">${_contextpath}/assets/Image/Product/detailsbig/</c:if><c:out value="${model.product.images[0].imageUrl}"/>" title="${model.product.images[0].imageUrl}" class="img-responsive center-block">
	  </a>
	</div>
	<c:if test="${fn:length(model.product.images) > 1}">
	  <div class="details_image_thumbnails">
	    <ul class="clearfix">
		  <c:forEach items="${model.product.images}" var="image" varStatus="status">
			<c:if test="${status.index > 0}">
			<li>
		      <a href="<c:if test="${!image.absolute}">${_contextpath}/assets/Image/Product/detailsbig/</c:if><c:out value="${image.imageUrl}"/>" data-lightbox="product_images" data-title="<c:out value="${product.name}" escapeXml="false" />">
			  <img src="<c:if test="${!image.absolute}">${_contextpath}/assets/Image/Product/thumb/</c:if><c:out value="${image.imageUrl}"/>" class="img-responsive">
			  </a>
		    </li>
		    </c:if>
		  </c:forEach>
		</ul>
	  </div>
	  </c:if>  
	
	  <c:if test="${gSiteConfig['gPRODUCT_REVIEW'] and product.enableRate}">
		<%@ include file="/WEB-INF/jsp/frontend/layout/template6/productReview.jsp" %>
	  </c:if>
    </div>
	
	<div class="col-sm-8">
	  <div class="details_desc clearfix">
		<c:if test="${model.manufacturer.imageUrl != null and fn:length(model.manufacturer.imageUrl) > 10}">
		<div class="details_brand">
		  <img src="${model.manufacturer.imageUrl}" class="img-responsive">
		</div>
		</c:if>
		<div class="details_info">
		  <div class="details_product_number">
			<span><fmt:message key="sku" /> :</span> <c:out value="${product.sku}" escapeXml="false"/>
		  </div>
		  
		  <c:forEach items="${product.productFieldsUnlimitedNameValue}" var="productFielUnlimitedNameValue" varStatus="status"> 
	        <c:if test="${productFielUnlimitedNameValue.displayName == 'Product Model' and productFielUnlimitedNameValue.displayValue != null}">
		      <div class="details_model_number">
		  	    <span><c:out value="${productFielUnlimitedNameValue.displayName}" escapeXml="false" /> #:</span> <c:out value="${productFielUnlimitedNameValue.displayValue}" escapeXml="false"/>
		  	  </div>
		    </c:if>
		  </c:forEach>
		  
		  <div class="clear"></div>
		</div>
		
		<div class="details_item_name">
		  <h1><c:out value="${product.name}" escapeXml="false"/></h1>
		</div>
		<div class="details_info">
			<c:out value="${product.shortDesc}" escapeXml="false" />
		</div>
		<c:choose>
		  <c:when test="${ (gSiteConfig['gSHOPPING_CART'] and (!empty product.price or product.priceByCustomer)) and (!product.loginRequire or userSession != null)}">
	        <div class="prices_table">
			  <c:if test="${fn:length(product.price) > 1}">
			  <div class="quantities">
				<div class="quantities_title"> <fmt:message key="quantity" /> </div>
				  <c:forEach items="${product.price}" var="price" varStatus="statusPrice">
			        <div class="quantity_cell">
			          <c:if test="${price.qtyFrom != null}">
					    <c:choose>
					      <c:when test="${price.qtyTo != null}">
					        <c:out value="${price.qtyFrom}" /> -  <c:out value="${price.qtyTo}" />
					      </c:when>
					      <c:otherwise>
					        <c:out value="${price.qtyFrom}" /> +
					      </c:otherwise>
					    </c:choose>
					  </c:if>
					  <c:if test="${price.qtyFrom == null}">
					    1 +
					  </c:if>
			        </div>
				  </c:forEach>
			  </div>
			  </c:if>
			  <div class="prices">
				<div class="prices_title"><fmt:message key="price" /></div>
				<c:forEach items="${product.price}" var="price" varStatus="statusPrice">
			    <div class="price_cell <c:if test="${product.salesTag != null}">strikethrough</c:if>">
				  <span class="price">  
				    <fmt:message key="${siteConfig['CURRENCY'].value}" /><fmt:formatNumber value="${price.amt}" pattern="#,##0.00#" maxFractionDigits="${siteConfig['MAX_FRACTION_DIGIT'].value}" />
			      </span>
				  <span class="caseContent">
				  <c:choose>
			        <c:when test="${product.caseContent != null}">(<c:out value="${product.caseContent}" /> 
			        <c:choose>
  						<c:when test="${product.caseUnitTitle != null}"><c:out value="${product.caseUnitTitle}" /></c:when> 
  						<c:otherwise><c:out value="${siteConfig['CASE_UNIT_TITLE'].value}" /></c:otherwise>
  					</c:choose>
			        /<c:out value="${product.packing}" />)</c:when>
			        <c:otherwise><c:out value="${product.packing}" /></c:otherwise>
			      </c:choose>
				  </span>
				</div>
				</c:forEach>
			  </div>
			  <c:if test="${product.salesTag != null}">
				<div class="prices">
				<div class="prices_title"><fmt:message key="price" /></div>
				<c:forEach items="${product.price}" var="price" varStatus="statusPrice">
			    <div class="price_cell new_price">
				  <span class="price">  
				    <fmt:message key="${siteConfig['CURRENCY'].value}" /><fmt:formatNumber value="${price.discountAmt}" pattern="#,##0.00#" maxFractionDigits="${siteConfig['MAX_FRACTION_DIGIT'].value}" />
			      </span>
				  <span class="caseContent">
				  <c:choose>
			        <c:when test="${product.caseContent != null}">(<c:out value="${product.caseContent}" /> <c:out value="${siteConfig['CASE_UNIT_TITLE'].value}" />/<c:out value="${product.packing}" />)</c:when>
			        <c:otherwise><c:out value="${product.packing}" /></c:otherwise>
			      </c:choose>
				  </span>
				</div>
				</c:forEach>
			  </div>
			  
			  </c:if>
				
			  <div class="clear"></div>
			</div>
		    
		    <c:if test="${product.salesTag != null}">
			<div id="discount_wrapper">
			  <div id="discount">
				<img src="${_contextpath}/assets/Image/PromoSalesTag/salestag_${product.salesTag.tagId}.gif" class="img-responsive">
			  </div>
			  
			  <c:if test="${siteConfig['SALES_TAG_COUNTDOWN'].value == 'true' and product.salesTag.inEffect}">
			  <script type="text/javascript" src="${_contextpath}/javascript/countDown.js"></script>
			  <script type="text/javascript">
				<!--
			 	jQuery("document").ready(function() {
					countdown();
				});
				//-->
			  </script>
			  <input type="hidden" value="${product.salesTag.endDate}" id="endDateValue"/>
			  <div id="countdown_dashboard">
				<div class="title" id="titleBox">Time Left:</div>
				<div class="digit day" id="daysBox"></div>
				<div class="colon" id="daysColon">:</div>
				<div class="digit hour" id="hoursBox"></div>
				<div class="colon" id="daysColon">:</div>
				<div class="digit min" id="minsBox"></div>
				<div class="colon" id="daysColon">:</div>
				<div class="digit sec" id="secsBox"></div>
			  </div>
			  </c:if>
			  
			  <div class="clear"></div>
			</div>
			</c:if>
			  
		    <form id="addToCartForm" action="${_contextpath}/addToCart.jhtm" method="post" class="clearfix">
			  <input name="product.id" type="hidden" value="${product.id}">
			    <%@ include file="/WEB-INF/jsp/frontend/layout/template6/productOptions.jsp" %>
			  <br/>
		      <div class="quantity_wrapper">
				<span class="quantity_title"><fmt:message key="quantity" /></span>
				<input name="quantity_${product.id}" size="5" maxlength="5" class="quantity_input" 
				<c:choose>
					<c:when test="${product.minimumQty > 0}">
						value="${product.minimumQty}"
					</c:when>
					<c:otherwise>
						value="1"
					</c:otherwise>
				</c:choose>
				type="text">
			  </div>
			  <div class="addToCart_btn_wrapper">
				<button type="submit" class="btn addToCart_btn">
				Add To Cart &nbsp;<span class="addToCart_icon"></span>
			    </button>
			  </div>
			  <div class="addToList_btn_wrapper">
				<a class="btn addToList_btn" href="${_contextpath}/addToList.jhtm?product.id=${product.id}">
				  Add To List &nbsp;<span class="addToList_icon"></span>
				</a>
			  </div>
			  <c:if test="${siteConfig['PRODUCT_QUOTE'].value == 'true' and product.quote}">
				<div class="addToQuote_btn_wrapper" id="addToQuoteListWrapperId${product.id}">
				  <a class="btn addToQuote_btn" href="#" onClick="addToQuoteList('${product.id}');" >
					Add To Quote &nbsp;<span class="addToQuote_icon"></span>
				  </a>
				</div>
			  </c:if>
			
			  <%--
			  <div class="addToCompare_btn_wrapper" id="addToCompareWrapperId${product.id}">
			    <a class="btn addToCompare_btn" href="#" onClick="addToCompareList('${product.id}');" >
				  Compare &nbsp;<span class="addToCompare_icon"></span>
				</a>
			  </div>
			   --%>
			</form>	
		  </c:when>
	      <c:when test="${siteConfig['PRODUCT_QUOTE'].value == 'true' and product.quote}">
			<div class="addToList_btn_wrapper">
			  <a class="btn addToList_btn" href="${_contextpath}/addToList.jhtm?product.id=${product.id}">
			    Add To List &nbsp;<span class="addToList_icon"></span>
			  </a>
			</div>
			<div class="addToQuote_btn_wrapper" id="addToQuoteListWrapperId${product.id}">
			 <a class="btn addToQuote_btn" href="#" onClick="addToQuoteList('${product.id}');" >
				Add To Quote &nbsp;<span class="addToQuote_icon"></span>
			  </a>
			</div>
			<%--
			<div class="addToCompare_btn_wrapper" id="addToCompareWrapperId${product.id}">
		      <a class="btn addToCompare_btn" href="#" onClick="addToCompareList('${product.id}');" >
				Compare &nbsp;<span class="addToCompare_icon"></span>
			  </a>
			</div>
			 --%>
	      </c:when>
	    </c:choose>	
	    
	    <c:if test="${gSiteConfig['gINVENTORY'] and product.inventory != null and !empty siteConfig['INVENTORY_ON_HAND'].value}" >
	       <c:choose>
	    	  <c:when test="${gSiteConfig['gSITE_DOMAIN'] == 'yayme.com.au'}">
	    	    <c:if test="${model.salesRepLoggedIn}">
	    	      <div class="inventory_onhand">
					<span class="inventory_onhand_title"><c:out value="${siteConfig['INVENTORY_ON_HAND'].value}" escapeXml="false"/></span>
					<span class="inventory_onhand_value"><c:out value="${product.inventory}" /></span>
				  </div>
	    	    </c:if>
	    	  </c:when>
	    	  <c:otherwise>
	    	      <div class="inventory_onhand">
					<span class="inventory_onhand_title"><c:out value="${siteConfig['INVENTORY_ON_HAND'].value}" escapeXml="false"/></span>
					<span class="inventory_onhand_value"><c:out value="${product.inventory}" /></span>
				  </div>
	    	  </c:otherwise>
	 	   </c:choose>
	 	</c:if>
	 						
		<%@ include file="/WEB-INF/jsp/frontend/layout/template6/productTabs3.jsp" %>

	  </div>
	</div>
  </div>
</c:if>

<div class="row">
	<div class="col-sm-12">
		<div id="promoContainer">
			<label for="promoCodeInput" class="promoTitle">     </label>
		</div>
	</div>
</div>


<c:if test="${model.recommendedList != null}">
<div class="row">
	<div class="col-sm-12">
		<div class="recommendedListWrapper">
			<div class="recommendedListHdr">	
			<c:choose>
 				<c:when test="${!empty product.recommendedListTitle }">
  					<div class="recommended_list"><c:out value="${product.recommendedListTitle}"/></div>
 				</c:when>
 				<c:otherwise>
  					<c:if test="${siteConfig['RECOMMENDED_LIST_TITLE'].value != ''}">
   						<div class="recommended_list"><c:out value="${siteConfig['RECOMMENDED_LIST_TITLE'].value}"/></div>
 			 		</c:if>
 				</c:otherwise>
			</c:choose>	
			</div>
			<div class="recommendedListDetails">
				
				<c:set var="numCul" value="${siteConfig['NUMBER_PRODUCT_PER_ROW'].value}" />
				<c:forEach items="${model.recommendedList}" var="product" varStatus="status">
				
				<c:set var="productLink">${_contextpath}/product.jhtm?id=${product.id}<c:if test="${model.product == null or siteConfig['RECOMMENDED_LIST_SHOW_BREADCRUMB'].value == 'true'}">&cid=<c:out value="${param.cid}"/></c:if></c:set>
				<c:if test="${gSiteConfig['gMOD_REWRITE'] == '1' and not fn:contains(product.sku, '/')}">
  					<c:set var="productLink">${_contextpath}/${siteConfig['MOD_REWRITE_PRODUCT'].value}/${product.encodedSku}/${product.encodedName}.html<c:if test="${model.product == null or siteConfig['RECOMMENDED_LIST_SHOW_BREADCRUMB'].value == 'true'}">?cid=<c:out value="${param.cid}"/></c:if></c:set>
				</c:if>	
				
				<div <c:choose><c:when test="${status.index % 2 == 0}">class="odd_row clearfix"</c:when><c:otherwise>class="even_row clearfix"</c:otherwise></c:choose>>
						<%@ include file="/WEB-INF/jsp/frontend/thumbnail/view28.jsp" %>
				</div>
				</c:forEach>	
			 </div>
			</div>
		</div>
	</div>
</div>
</c:if>

<c:if test="${fn:trim(model.productLayout.footerHtml) != ''}">
  <c:set value="${model.productLayout.footerHtml}" var="footerHtml"/>
  <%@ include file="/WEB-INF/jsp/frontend/common/footerDynamicElement.jsp" %>
  <div style="float:left;width:100%"><c:out value="${footerHtml}" escapeXml="false"/></div>
</c:if>

 
<c:if test="${siteConfig['PRODUCT_QUOTE'].value == 'true' and product.quote}">  
<script type="text/javascript">
<!--
function addToQuoteList(productId){
	$.ajax({
		url: "${_contextpath}/quoteList.jhtm?productId="+productId,
		context: document.body
	}).done(function() {
		$("#addToQuoteListWrapperId"+productId).html('<a href=\"${_contextpath}/quoteViewList.jhtm\" class=\"btn addToQuote_btn\">View Quote List</a>');
	});
}

function addToCompareList(productId){
	$.ajax({
		url: "${_contextpath}/ajaxAddToCompare.jhtm?&productId="+productId,
		context: document.body
	}).done(function() {
		$("#addToCompareWrapperId"+productId).html('<a href=\"${_contextpath}/comparison.jhtm\" class=\"btn addToCompare_btn\">View Comparison</a>');
	});
}
//-->
</script>
</c:if>    
  </tiles:putAttribute>
</tiles:insertDefinition>