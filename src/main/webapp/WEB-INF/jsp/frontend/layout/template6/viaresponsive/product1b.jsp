<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="/WEB-INF/tlds/spring.tld" prefix="spring"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib uri="/WEB-INF/tlds/custom.tld" prefix="wj" %>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>

<tiles:insertDefinition name="${_template}" flush="true">  
<tiles:putAttribute name="leftbar" value="/WEB-INF/jsp/frontend/layout/template${_leftBar}/leftbar.jsp" />
	<tiles:putAttribute name="content" type="string">
	
<link rel="stylesheet" type="text/css" href="/assets/css/rightShoppingCart.css">
<link rel="stylesheet" type="text/css" href="/assets/plugins/owl-carousel/css/owl.carousel.css">
<link rel="stylesheet" type="text/css" href="/assets/plugins/owl-carousel/css/owl.theme.default.css">
<link rel="stylesheet" type="text/css" href="/assets/plugins/owl-carousel/css/animate.css">
<link rel="stylesheet" type="text/css" href="/assets/css/slider-theme.css">
<link rel="stylesheet" type="text/css" href="/assets/css/carousel-theme.css">
<link rel="stylesheet" type="text/css" href="/assets/css/sc-carousel-theme.css">
<link rel="stylesheet" type="text/css" href="/assets/css/categories-carousel-theme.css">
<script type="text/javascript" src="/assets/plugins/owl-carousel/js/owl.carousel.js"></script>
<style>
#new_arrivals_wrapper {
    padding-top: 120px !important;
}
</style>
<style>
.product_sku_wrapper {
    top: 0 !important;
}
.product_wrapper {
    margin-bottom: 15px !important;
}
</style>

<link rel="shortcut icon" href="../assets/responsive/img/favicon.ico">
<script type="text/javascript">
$(function() {
	/*$("#banner_slider").owlCarousel({
		themeClass: "slider-theme",
		navText: [],
		items: 1,
		loop: true,
		nav: true,
		dots: false,
		autoplay: true,
		autoplaySpeed: 1000,
		navSpeed: 1000,
		dotsSpeed: 1000,
		dragEndSpeed: 1000,
		autoplayTimeout: 5000,
		responsive:{
			0:{
				nav: false,
				dots: false,
			},
			768:{
				nav: true,
				dots: false
			}
		}
	});*/
	$(".products_carousel").owlCarousel({
		themeClass: "carousel-theme",
		navText: [],
		loop: true,
		dots: false,
		autoplay: true,
		margin: 30,
		autoplayTimeout: 5000,
		responsiveClass: true,
		responsive:{
			0:{
				items: 1,
				slideBy: 1,
				nav: true
			},
			550:{
				items: 2,
				slideBy: 2,
				nav: true
			},
			768:{
				items: 3,
				slideBy: 3,
				nav: true
			},
			992:{
				items: 4,
				slideBy: 4,
				nav: true
			},
			1200: {
				items: 4,
				slideBy: 4,
				nav: true
			}
		}
	}).on('changed.owl.carousel', function(event) {
		$(window).trigger('resize');
	});
});
</script>

<script type="text/javascript">
$(function(){
	$(".fancybox-thumbs").fancybox({
		padding: 1,
		helpers	: {
			overlay : {
				css : {
					'background' : 'rgba(0, 0, 0, 0.8)'
				}
			},
			title : {
				type : 'outside',
				position : 'top'
			},
			thumbs	: {
				width	: 100,
				height	: 75
			}
		}
	});
});

function assignValue(id) {
	if(document.getElementById('include_'+id).checked) {
		document.getElementById('include_'+id).value = '1';
	}else{
		document.getElementById('include_'+id).value = '0';		
	}
}
</script>
<script>
	$.get('/dv/liveapps/captureskuview.php',{ sku: "${model.product.sku}", cid: "${model.userId}" });
</script>

</head>


						<div id="leftSidebarWrapper">
						<div id="leftSidebar">
							<div id="btn-toggle-offcanvas" class="visible-xs">
								<button type="button" class="btn btn-primary" data-toggle="offcanvas">
									<i class="fa fa-bars"></i>
								</button>
							</div>
							<div class="col-xs-6 col-sm-3 col-md-2 sidebar-offcanvas" id="sidebar" role="navigation">
								
								
 <c:if test="${(model.lang==null || model.lang!='es')}"> 
 <div class="leftbar_catLinks_wrapper">
	<div id=""><a class="leftbar_catLink" href="/wholesale/283/All_Products.html">ALL PRODUCTS</a></div>
	<div id=""><a class="leftbar_catLink" href="/wholesale/617/New-Arrivals.html">NEW ARRIVALS</a></div>
	<div id=""><a class="leftbar_catLink" href="/wholesale/738/Load-Center.html">LOAD CENTER</a></div>
	<div id=""><a class="leftbar_subcatLink" href="/wholesale/4/Clothing.html">CLOTHING</a></div>
	<div id=""><a class="leftbar_subcatLink" href="/wholesale/7/Cosmetics_Perfume_HBA.html">COSMETICS/HBA</a></div>
	<div id=""><a class="leftbar_subcatLink" href="/wholesale/9/Domestics.html">DOMESTICS</a></div>
	<div id=""><a class="leftbar_subcatLink" href="/wholesale/2/Electronics.html">ELECTRONICS</a></div>
	<div id=""><a class="leftbar_subcatLink" href="/wholesale/5/Fashion_Accessories.html">ACCESSORIES</a></div>
	<div id=""><a class="leftbar_subcatLink" href="/wholesale/344/Furniture.html">FURNITURE</a></div>
	<div id=""><a class="leftbar_subcatLink" href="/wholesale/8/General_Merchandise.html">GENERAL MERCHANDISE</a></div>
	<div id=""><a class="leftbar_subcatLink" href="/wholesale/368/Home_Kitchen.html">HOME &amp; KITCHEN</a></div>
	<div id=""><a class="leftbar_subcatLink" href="/wholesale/352/Seasonal.html">SEASONAL/HOLIDAY</a></div>
	<div id=""><a class="leftbar_subcatLink" href="/wholesale/6/Shoes.html">SHOES</a></div>
	<div id=""><a class="leftbar_subcatLink" href="/wholesale/3/Tools.html">TOOLS</a></div>
	<div id=""><a class="leftbar_subcatLink" href="/wholesale/362/Toys.html">TOYS</a></div>
	
 </div>
 </c:if>
 
  <c:if test="${model.lang!=null && model.lang=='es'}"> 
  <div class="leftbar_catLinks_wrapper">
    <h3>Categorias de Producto</h3>
	<div id=""><a class="leftbar_catLink" href="/wholesale/283/Productos.html">Todos los Productos</a></div>
	<div id=""><a class="leftbar_catLink" href="/wholesale/617/Nuevas-Llegadas.html">Nuevas Llegadas</a></div>
	<div id=""><a class="leftbar_catLink" href="/wholesale/738/Centro-de-Lotes.html">Centro de Lotes</a></div>
	<div id=""><a class="leftbar_subcatLink" href="/wholesale/5/Accesorios.html">Accesorios de Moda</a></div>
	<div id=""><a class="leftbar_subcatLink" href="/wholesale/6/Calzado.html">Calzado</a></div>
	<div id=""><a class="leftbar_subcatLink" href="/wholesale/368/Cocina.html">Cocina y Hogar</a></div>
	<div id=""><a class="leftbar_subcatLink" href="/wholesale/7/Cosmeticos.html">Cosméticos</a></div>
	<div id=""><a class="leftbar_subcatLink" href="/wholesale/9/Domesticos.html">Domésticos</a></div>
	<div id=""><a class="leftbar_subcatLink" href="/wholesale/2/Electronicos.html">Electrónicos</a></div>
	<div id=""><a class="leftbar_subcatLink" href="/wholesale/3/Herramientas.html">Herramientas</a></div>
	<div id=""><a class="leftbar_subcatLink" href="/wholesale/362/Juguetes.html">Juguetes</a></div>
	<div id=""><a class="leftbar_subcatLink" href="/wholesale/8/Mercancia-General.html">Mercancía General</a></div>
	<div id=""><a class="leftbar_subcatLink" href="/wholesale/344/Muebles.html">Muebles</a></div>
	<div id=""><a class="leftbar_subcatLink" href="/wholesale/4/Ropa.html">Ropa</a></div>
	<div id=""><a class="leftbar_subcatLink" href="/wholesale/352/Temporada.html">Temporada</a></div>
 </div>
 </c:if>
								</div>
						</div>
					</div>
					

					<!-- ========== CONTENT ========== -->
					<div id="contentWrapper">
						<div id="content">
							<div class="col-sm-9 col-md-10">
								<div class="row">
									<div class="col-sm-9">
										<div id="product_details_1" class="clearfix">
											
											<c:set value="${model.product}" var="product" />
											
											<c:if test="${model.adminLogin != null and model.adminLogin}">
												<a class="getshippingQuoteLink" href="${_contextpath}/admin/catalog/product.jhtm?id=${product.id}">Edit Page</a>
											</c:if>		
											<div class="details_info_box clearfix">
												<div class="row">
													<div class="col-sm-12 col-md-6">
														<div class="item_name">
															<h1><fmt:message key="lotDetails"/></h1>
														</div>
														<div class="item_sku">
															<span class="label_title"><fmt:message key="sku"/>:</span>
															<span class="value">${product.sku}</span>
														</div>
														
														<div class="item_condition">
															<span class="label_title"><fmt:message key="condition"/>:</span>
															<span class="value"><c:out value="${wj:removeChar(product.field24, ',' , true, true) }"/></span>
														</div>
									
														<div class="unit_price">
															<span class="label_title"><fmt:message key="units"/>:</span>
															<span class="price_value"><c:out value="${product.field29}" /></span>
														</div>
													
														<div class="unit_price">
															<span class="label_title"><fmt:message key="unitPrice"/>:</span>
															<span class="price_value">${product.field8}</span>
														</div>
										
														<div class="big_price total_price">
															<span class="label_title"><fmt:message key="lotDetails"/>:</span>
																<fmt:message key="${siteConfig['CURRENCY'].value}"/><fmt:formatNumber value="${wj:removeChar(product.price1, ',' , true, true) }" pattern="#,###.00"/>
																<c:if test="${product.field6 != null and product.field6 !=''}">
																	&nbsp;/${product.field6}
																</c:if>
															<a class="getshippingQuoteLink" href="#"></a>
														</div>	
														
														<form id="addToCartForm" action="${_contextpath}/addToCart.jhtm" method="post" class="clearfix">
															<input type="hidden" name="product.id" value="${product.id}">
															<input type="hidden" name="addToCartProduct" value="true">																
															<div class="quantity">
																<div class="clearfix">
																	<div class="pull-left">
																		<span class="label_title"><fmt:message key="quantity"/>:</span>
																	</div>
																	<div class="pull-left">
																		<input type="text" class="touchspin form-control" style="display: block;" name="quantity_${product.id}" value="1">
																		<span class="input-group-btn-vertical">
																		</span>
																	</div>
																	<div class="clearfix">
																		<c:if test="${product.includeRetailEnabled}">
																		<label class="checkbox-inline">
																			<input type="checkbox" id="include_${product.id}" name="include_${product.id}" onclick="assignValue(${product.id})"> <fmt:message key="includeRetailDisplay"/>
																		</label>
																		</c:if>
																	</div>				
																</div>
															</div>
																
															<c:catch var="e">
															<fmt:parseNumber  var="field29" type="number" value="${product.field29}"  />
															<div class="details_product_addtocart_price">
															<c:if test="${ (gSiteConfig['gSHOPPING_CART'] and (!empty product.price or product.priceByCustomer)) and (!product.loginRequire or userSession != null)}">
																<c:if test="${product.price2 != null}">
																	<div class="saving_info_wrapper">
																		<div class="title">
																			<fmt:message key="buyMoreAndSave"/><c:out value="!"/>
																		</div>
																		<ul class="saving_options">
																		<c:forEach items="${product.price}" var="price">
																			<c:if test="${price.amt != 0 and price.qtyFrom > 1}">
																				<div>
																					 <li>
																					 	<strong><fmt:message key="buy"/> <c:out value="${price.qtyFrom}"/> <fmt:message key="orMore"/>: </strong>
																					 	$<fmt:formatNumber value="${price.amt/field29}" pattern="#,##0.00"/>/<c:out value="${siteConfig['CASE_UNIT_TITLE'].value}" /> - $<fmt:formatNumber value="${price.amt * product.caseContent}" pattern="#,##0.00"/>/<fmt:message key="lot"/>
																					 	<strong>(<fmt:message key="save"/> <fmt:formatNumber value="${(product.price1 - price.amt)/product.price1*100}" pattern="#0"/>% or $<fmt:formatNumber value="${(product.price1 - price.amt)*product.caseContent}" pattern="#,##0.00"/> )</strong>
																					 </li>
																				</div>
																			</c:if>
																		</c:forEach>
																		</ul>
																	</div>
																</c:if>
															</c:if>
															</div>
															</c:catch>	
															
															<div class="buttons_wrapper clearfix">
																<div class="btn_wrapper addToCart_btn_wrapper">
																	<button type="submit" class="btn btn-lg addToCart_btn">
																		<fmt:message key="addToCart"/> &nbsp;<span class="addToCart_icon"></span>
																	</button>
																</div>
																<div class="btn_wrapper addToList_btn_wrapper">
																	<a class="btn addToList_btn" href="${_contextpath}/addToList.jhtm?product.id=${product.id}">
																		<fmt:message key="addToWishList"/>
																	</a>
																</div>
															</div>
															
														</form>
													</div>
													
													<div class="col-sm-12 col-md-6">
														
														 <div class="item_image_box clearfix">
															<div class="item_image_wrapper is_tagged">                                                     
                                                             <c:if test="${fn:length(model.product.images) > 0}">
                                                             <c:forEach items="${model.product.images}" var="image" varStatus="status">
                                                             <c:out value="${image.imageUrl}"/>
                                                             <c:if test="${status.index == 0}">
                                                             <li>
                                                             <div class="thumbnail_image">
                                                             <a href="<c:if test="${!image.absolute}">${_contextpath}/assets/Image/Product/thumb/</c:if><c:out value="${image.imageUrl}"/>" data-hover="swap-img" data-target=".item_image">
                                                             <img class="item_image img-responsive center-block"  src="<c:if test="${!image.absolute}">${_contextpath}/assets/Image/Product/thumb/</c:if><c:out value="${image.imageUrl}"/>" class="img-responsive center-block">
                                                             </a>
                                                             </div>
                                                             </li>
                                                             </c:if>
                                                             </c:forEach>
                                                             </c:if>
                                                             </div>
                                                         </div>

														<div class="item_image_thumbnails clearfix">
															<ul class="clearfix">	
															<c:if test="${fn:length(model.product.images) > 0}">
																<c:forEach items="${model.product.images}" var="image" varStatus="status">
																<c:if test="${status.index > -1}">
																<li>
																<div class="thumbnail_image">
		     														<a href="<c:if test="${!image.absolute}">${_contextpath}/assets/Image/Product/detailsbig/</c:if><c:out value="${image.imageUrl}"/>" data-hover="swap-img" data-target=".item_image">
			  														<img src="<c:if test="${!image.absolute}">${_contextpath}/assets/Image/Product/detailsbig/</c:if><c:out value="${image.imageUrl}"/>" class="img-responsive center-block">
			  														</a>
		    													</div>
																</li>
		    													</c:if>
		  														</c:forEach>
	 														 </c:if> 
															</ul>
														</div>
													</div>
												</div>
											</div>							
					
											<%@ include file="/WEB-INF/jsp/frontend/layout/template6/productTabs3.jsp" %>		
												
											<div id="new_arrivals_wrapper">
		<div id="new_arriavals" class="container">
			<script type="text/javascript">
				$(window).load(function() {
					$(window).resize(function(){
						$(".products_carousel .product_name_wrapper").css('height','auto');
						var product_name_wrapper_maxHeight = Math.max.apply(null, $(".products_carousel .product_name_wrapper").map(function(){
							return $(this).height();
						}).get());
						$(".products_carousel .product_name_wrapper").height(product_name_wrapper_maxHeight);

						$(".products_carousel .product_image_wrapper").css('height','auto');
						var product_image_wrapper_maxHeight = Math.max.apply(null, $(".products_carousel .product_image_wrapper").map(function(){
							return $(this).height();
						}).get());
						$(".products_carousel .product_image_wrapper").height(product_image_wrapper_maxHeight);

						$(".products_carousel .product_bordered_box").css('height','auto');
						var product_bordered_box_maxHeight = Math.max.apply(null, $(".products_carousel .product_bordered_box").map(function(){
							return $(this).height();
						}).get());
						$(".products_carousel .product_bordered_box").height(product_bordered_box_maxHeight);

						$(".products_carousel .product_wrapper").css('height','auto');
						var product_wrapper_maxHeight = Math.max.apply(null, $(".products_carousel .product_wrapper").map(function(){
							return $(this).height();
						}).get());
						$(".products_carousel .product_wrapper").height(product_wrapper_maxHeight);
					}).trigger('resize');
				});
			</script>
			
				${model.recommendedSkuList}

		</div>
	</div>
										</div>
									
									
									<!-- ========== floating shopping cart begin ========== -->						
									<div class="col-sm-3">
									<script type="text/javascript">
										$(window).load(function() {
											var rightShoppingCartBoxTop = 0;
											$(window).scroll(function() {
												if($(window).scrollTop() > $('.products_wrapper').offset().top) {
													if($(window).scrollTop() < ($('.products_wrapper').offset().top + $('.products_wrapper').height() - $('#rightShoppingCartBox').height())) {
														$('#rightShoppingCartBox').css('position','fixed');
														rightShoppingCartBoxTop = 0;
													}
													if($('.products_wrapper').height() > $('#rightShoppingCartBox').height()) {
														if($(window).scrollTop() > ($('.products_wrapper').offset().top + $('.products_wrapper').height() - $('#rightShoppingCartBox').height())) {
															$('#rightShoppingCartBox').css('position','absolute');
															rightShoppingCartBoxTop = $('.products_wrapper').height() - $('#rightShoppingCartBox').height();
														}
													}
												}
												if($(window).scrollTop() < $('.products_wrapper').offset().top) {
													$('#rightShoppingCartBox').css('position','relative');
													rightShoppingCartBoxTop = 0;
												}

												$('#rightShoppingCartBox').css('width', $('#rightShoppingCartBox_wrapper').width());
												$('#rightShoppingCartBox').css('top', rightShoppingCartBoxTop + 'px');
											}).resize(function(){
												$(window).trigger('scroll');
											}).trigger('scroll');
										});
										</script>
										
										<div id="rightShoppingCartBox_wrapper">
											<div id="rightShoppingCartBox">
												<div id="right_shopping_cart">
													
													<div class="clearfix" id="right_shopping_cart_header">
														<div class="sc_hdr_label"><fmt:message key="cartItems"/>
															<a href="${_contextpath}/viewCart.jhtm">
																<c:choose>
 <c:when test="${(model.lang==null || model.lang!='es')}">
        <img src="https://www.viatrading.com/assets/icon/cart.png" style="width:2em; height:2em;position:relative;left:60px"/>
</c:when>
<c:otherwise>
        <img src="https://www.viatrading.com/assets/icon/cart.png" style="width:2em; height:2em;position:relative;left:85px"/>
</c:otherwise>
</c:choose>

<c:choose>
 <c:when test="${(model.lang==null || model.lang!='es')}">
        <span style="color:black;position:relative;left:55px">
</c:when>
<c:otherwise>
        <span style="color:black;position:relative;left:80px">
</c:otherwise>
</c:choose>	
																<c:choose>
																<c:when test="${model.cart.quantity==null}">
																	0
																</c:when>
																<c:otherwise>
																	${model.cart.quantity}
																</c:otherwise>														
																</c:choose>	
																</span>
															</a>
														</div>
													</div>
													
													<div class="clearfix" id="right_shopping_cart_items">
													
													<c:forEach items="${model.cart.cartItems}" var="cartItem" varStatus="cartStatus">

                                                                                                           <c:set var="productLink2">${_contextpath}/product.jhtm?id=${cartItem.product.id}<c:if test="${model.cartItem.product == null or siteConfig['RECOMMENDED_LIST_SHOW_BREADCRUMB'].value == 'true'}">&cid=<c:out value="${param.cid}"/></c:if></c:set>
                                                                                                                <c:if test="${gSiteConfig['gMOD_REWRITE'] == '1' and not fn:contains(cartItem.product.sku, '/')}">
                                                                                                                        <c:set var="productLink2">${_contextpath}/${siteConfig['MOD_REWRITE_PRODUCT'].value}/${cartItem.product.encodedSku}/${cartItem.product.encodedName}.html<c:if test="${model.cartItem.product == null or siteConfig['RECOMMENDED_LIST_SHOW_BREADCRUMB'].value == 'true'}">?cid=<c:out value="${param.cid}"/></c:if></c:set>
                                                                                                                </c:if>

                                                                                                                <div class="product clearfix">
                                                                                                                        <div class="product_sku">
                                                                                                                                ${cartItem.product.sku}
                                                                                                                        </div>
                                                                                                                        <div class="product_tableGroup">
                                                                                                                                <div class="product_image_cell">
                                                                                                                                        <div class="product_image">
                                                                                                                                                <a href="${productLink2}"><img class="cartImage img-responsive" src="<c:if test="${!cartItem.product.thumbnail.absolute}">${_contextpath}/assets/Image/Product/thumb/</c:if>${cartItem.product.thumbnail.imageUrl}" border="0" alt="${cartItem.product.alt}"/></a>
                                                                                                                                        </div>
                                                                                                                                </div>
                                                                                                                                <div class="product_name_cell">
                                                                                                                                        <div class="product_name">
                                                                                                                                                <a href="${productLink2}">${cartItem.product.name}</a>
                                                                                                                                        </div>
                                                                                                                                        <div class="quantity_and_price">
                                                                                                                                                <span class="quantity">${cartItem.quantity}</span> x <span class="price"><fmt:message key="${siteConfig['CURRENCY'].value}" /><fmt:formatNumber value="${cartItem.unitPrice}" pattern="###,##0.00"/></span>
                                                                                                                                        </div>
                                                                                                                                </div>
                                                                                                                        </div>

                                                                                                                                <div class="pull-left">
                                                                                                                <form id="addToCartForm" action="${_contextpath}/addToCart.jhtm" method="post" class="clearfix">
                                                                                                                        <input type="hidden" name="product.id" value="${cartItem.product.id}">
                                                                                                                        <input type="hidden" name="mainProductId" value="${model.product.id}">
                                                                                                                        <input type="hidden" name="quantity_${cartItem.product.id}" value="1">
                                                                                                                        <input type="hidden" name="addToCartProduct" value="true">
                                                                                                                        <span class="input-group-btn-vertical">
                                                                                                                                <button type="submit" name="increase" class="btn btn-default bootstrap-touchspin-up" style="padding-left:5px !important;padding-right:5px !important;padding-top:0px !important;padding-bottom:0px !important; margin-top:1px; margin-bottom:-2px;" type="button">+</button>
                                                                                                                                <button type="submit" name="decrease" class="btn btn-default bootstrap-touchspin-down" style="padding-left:6px !important;padding-right:6px !important;padding-top:0px !important;padding-bottom:0px !important; margin-top:1px; margin-bottom:-2px;" type="button">-</button>
                                                                                                                        </span>
                                                                                                                </form>
                                                                                                                </div>



                                                                                                                </div>


                                                                                                        </c:forEach>	
													</div>
									
													<div class="clearfix" id="right_shopping_cart_footer">
														
														<div class="sc_ftr_info_box sc_subtotal clearfix">
															<div class="sc_ftr_info_label_cell">
																<div class="sc_ftr_info_label subtotal_label">
																	<fmt:message key="subtotal"/>
																</div>
															</div>
															<div class="sc_ftr_info_value_cell">
																<div class="sc_ftr_info_value subtotal_value">
																	<fmt:message key="${siteConfig['CURRENCY'].value}" /><fmt:formatNumber value="${model.subTotal}" pattern="###,##0.00"/>
																</div>
															</div>
														</div>
														
														<div class="sc_ftr_info_box sc_discount clearfix">
															<div class="sc_ftr_info_label_cell">
																<div class="sc_ftr_info_label discount_label">
																	<fmt:message key="discount"/>
																</div>
															</div>
															<div class="sc_ftr_info_value_cell">
																<div class="sc_ftr_info_value discount_value">
																	<fmt:message key="${siteConfig['CURRENCY'].value}" /><fmt:formatNumber value="${model.discount}" pattern="###,##0.00"/>
																</div>
															</div>
														</div>
														
														<div class="sc_ftr_info_box sc_tax clearfix">
															<div class="sc_ftr_info_label_cell">
																<div class="sc_ftr_info_label tax_label">
																	<fmt:message key="tax"/>
																</div>
															</div>
															<div class="sc_ftr_info_value_cell">
																<div class="sc_ftr_info_value tax_value">
																	<fmt:message key="${siteConfig['CURRENCY'].value}" /><fmt:formatNumber value="${model.tax}" pattern="###,##0.00"/>
																</div>
															</div>
														</div>
														
														
														<div class="sc_ftr_info_box sc_shippingAndHandling clearfix">
															<div class="sc_ftr_info_label_cell">
																<div class="sc_ftr_info_label shippingAndHandling_label">
																	<fmt:message key="shipping"/>
																</div>
															</div>
															<div class="sc_ftr_info_value_cell">
																<div id="shippingType" class="sc_ftr_info_value shippingAndHandling_value">
																	
																	<c:choose>	
																		<c:when test="${userSession != null && model.shipping == null}">
																			<a href='#'>Not Available</a>
																		</c:when>	
																		<c:when test="${model.shipping == null}">
																			<a href='${_contextpath}/login.jhtm'><fmt:message key="loginRegister"/></a>
																		</c:when>				
																		<c:otherwise>
																			${model.shipping}
																		</c:otherwise>
																	</c:choose>
																	
																</div>
															</div>
														</div>
														<div class="sc_ftr_info_box sc_shippingAndHandling clearfix">
															<div class="sc_ftr_info_label_cell">
																<div class="sc_ftr_info_label shippingAndHandling_label">
																	<fmt:message key="shippingCost"/>
																</div>
															</div>
															<div class="sc_ftr_info_value_cell">
																<div id="shippingCost" class="sc_ftr_info_value shippingAndHandling_value">
																
																	<c:choose>			
																		<c:when test="${model.totalCharges == null}">
																			$0.00
																		</c:when>				
																		<c:otherwise>
																			<fmt:message key="${siteConfig['CURRENCY'].value}" /><fmt:formatNumber value="${model.totalCharges}" pattern="###,##0.00"/>
																		</c:otherwise>
																	</c:choose>
	
																</div>
															</div>
														</div>
														
														<div class="sc_ftr_info_box sc_grandTotal clearfix">
															<div class="sc_ftr_info_label_cell">
																<div class="sc_ftr_info_label grandTotal_label">
																	<fmt:message key="grandTotal"/>:
																</div>
															</div>
															<div class="sc_ftr_info_value_cell">
																<div id="grandTotal" class="sc_ftr_info_value grandTotal_value">
															
																	<fmt:message key="${siteConfig['CURRENCY'].value}" /><fmt:formatNumber value="${model.grandTotal}" pattern="###,##0.00"/>
																			
																</div>
															</div>
														</div>
														
														<div class="buttons clearfix">
															<a href="${_contextpath}/viewCart.jhtm" class="btn viewCart_btn">View Cart</a>
															<a href="${_contextpath}/checkout1.jhtm" class="btn checkout_btn">Checkout</a>
														</div>
													</div>
												</div>

												<div id="sc_relatedProducts">
													<div id="sc_relatedProducts_header">
														<div class="sc_hdr_label">
															<fmt:message key="relatedProducts"/>
														</div>
													</div>
													<div id="sc_relatedProducts_content">
														<script type="text/javascript">
															$(function(){
																$(".sc_relatedProducts_carouse").owlCarousel({
																	themeClass: 'sc-carousel-theme',
																	navText: [],
																	loop: true,
																	dots: false,
																	autoplay: true,
																	margin: 30,
																	autoplayTimeout: 5000,
																	items: 1,
																	slideBy: 1,
																	nav: true
																});
															});
														</script>
														
														
														<div class="sc_relatedProducts_carousel_wrapper">
															<div class="sc_relatedProducts_carouse owl-carousel">														
																${model.sb}
															</div>
														</div>													
													</div>
												</div>
											</div>
										</div>
									</div>
									<!-- ========== floating shopping cart end ========== -->
									
								</div>
							</div>
						</div>
					</div>
					<!-- ========== END CONTENT ========== -->
	


	</tiles:putAttribute>
</tiles:insertDefinition>