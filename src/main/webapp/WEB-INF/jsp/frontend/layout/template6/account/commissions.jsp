<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="/WEB-INF/tlds/spring.tld" prefix="spring" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ taglib uri="/WEB-INF/tlds/custom.tld" prefix="wj" %>
<%@ page session="false" %>

<tiles:insertDefinition name="${_template}" flush="true">
	<c:if test="${_leftBar != '1' and _leftBar != '4'}">
     <tiles:putAttribute name="leftbar" value="/WEB-INF/jsp/frontend/layout/templatr6/leftbar.jsp" />
    </c:if>
	<tiles:putAttribute name="content" type="string">
    
    <div class="col-breadcrumb">
		<ul class="breadcrumb">
			<li><a href="${_contextpath}/account.jhtm">My Account</a></li>
			<li class="active">Comissions</li>
		</ul>
	</div>

	<div class="col-sm-12">
	<div class="accountAffiliateCommissionsWrapper">
		<form method="post">
		<div class="headerTitle accountAffiliateCommissionsTitle">
			<fmt:message key="commissionsYouMakeFromLevel" /> <c:out value="${model.level}"></c:out> 
		</div>

		<div class="clearfix"></div>

		<div class="pageNavi">Page 
			<select name="page" onchange="submit()">
				<c:forEach begin="1" end="${model.orders.pageCount}" var="page">
		  	        <option value="${page}" <c:if test="${page == (model.orders.page+1)}">selected</c:if>>${page}</option>
				</c:forEach>
			</select> of <c:out value="${model.orders.pageCount}"/>
		</div>
		<div class="clearfix"></div>
		
		<div class="accountAffiliateCommissions_table">
			<div class="accountAffiliateCommissionsHdr">
				<div class="row clearfix">
	
	
					<div class="col-sm-12 col-md-2">
						<div class="orderDateHeader"><fmt:message key="orderDate" /></div>
					</div>
					<div class="col-sm-12 col-md-2">
						<div class="orderNumberHeader"><fmt:message key="orderNumber" /></div>
					</div>
					<div class="col-sm-12 col-md-1">
						<div class="totalHeader"><fmt:message key="total" /></div>
					</div>
					<div class="col-sm-12 col-md-1">
						<div class="statusHeader"><fmt:message key="status" /></div>
					</div>
					<div class="col-sm-12 col-md-1">
						<div class="myCommissionHeader"><fmt:message key="myCommission" /></div>
					</div>
					<div class="col-sm-12 col-md-1">
						<div class="paymentStatusHeader"><fmt:message key="paymentStatus" /></div>
					</div>
					<div class="col-sm-12 col-md-2">
						<div class="checkNumberHeader"><fmt:message key="checkNumber" /></div>
					</div>
					<div class="col-sm-12 col-md-2">
						<div class="dateHeader"><fmt:message key="date" /></div>
					</div>
				</div>
			</div>

			<div class="accountAffiliateCommissionsDetails">
			<c:forEach items="${model.orders.pageList}" var="order" varStatus="status">
				<c:set var="yearMonthNew" value="${fn:split(model.orders.pageList[status.index].orderDate,'-')[0]}${fn:split(model.orders.pageList[status.index].orderDate,'-')[1]}"></c:set>
				<c:set var="yearMonthOld" value="${fn:split(model.orders.pageList[status.index+1].orderDate,'-')[0]}${fn:split(model.orders.pageList[status.index+1].orderDate,'-')[1]}"></c:set>
				<c:set var="row" value="odd_row" />
				<c:if test="${status.index % 2 == 0}">
					<c:set var="row" value="even_row" />
				</c:if>
				<div class="${row} clearfix">
					<div class="col-sm-12 col-md-2">
						<div class="orderDateHeader visible-xs visible-sm"><fmt:message key="orderDate" /></div>
						<div class="orderDate"><fmt:formatDate type="date" timeStyle="default" value="${order.orderDate}"/></div>
						<div class="clearfix"></div>
					</div>
					<div class="col-sm-12 col-md-2">
						<div class="orderNumberHeader visible-xs visible-sm"><fmt:message key="orderNumber" /></div>
						<div class="orderNumber">
							<c:if test="${siteConfig['SHOW_CHILD_INVOICE'].value == 'true'}">
								<a href="invoice.jhtm?order=${order.orderId}" class="nameLink">
							</c:if>
							<c:out value="${order.orderId}"/>
							<c:if test="${siteConfig['SHOW_CHILD_INVOICE'].value == 'true'}">
								</a>
							</c:if>
						</div>
						<div class="clearfix"></div>
					</div>
					<div class="col-sm-12 col-md-1">
						<div class="totalHeader visible-xs visible-sm"><fmt:message key="total" /></div>
						<div class="total"><fmt:formatNumber value="${order.subTotal}" pattern="#,##0.00" /></div>
						<div class="clearfix"></div>
					</div>
					<div class="col-sm-12 col-md-1">
						<div class="statusHeader visible-xs visible-sm"><fmt:message key="status" /></div>
						<div class="status"><c:out value="${order.orderStatusToString}" /></div>
						<div class="clearfix"></div>
					</div>
					<div class="col-sm-12 col-md-1">
						<div class="myCommissionHeader visible-xs visible-sm"><fmt:message key="myCommission" /></div>
						<div class="myCommission"><fmt:formatNumber value="${order.myCommission}" pattern="#,##0.00" /></div>
						<div class="clearfix"></div>
					</div>
					<div class="col-sm-12 col-md-1">
						<div class="paymentStatusHeader visible-xs visible-sm"><fmt:message key="paymentStatus" /></div>
						<div class="paymentStatus">
							<c:if test="${model.level == 1}">
						  		<c:out value="${order.commissionLevel1StatusToString}"/>
						  	</c:if>
							<c:if test="${model.level == 2}">
						  		<c:out value="${order.commissionLevel2StatusToString}"/>
						  	</c:if>
						</div>
						<div class="clearfix"></div>
					</div>
					
		
					
					<div class="col-sm-12 col-md-2">
						<div class="checkNumberHeader visible-xs visible-sm"><fmt:message key="checkNumber" /></div>
						<div class="checkNumber">
							<c:if test="${model.level == 1}">
								<c:out value="${order.checkNumberLevel1}" />
							</c:if>
							<c:if test="${model.level == 2}">
								<c:out value="${order.checkNumberLevel2}" />
							</c:if>
						</div>
						<div class="clearfix"></div>
					</div>
					<div class="col-sm-12 col-md-2">
						<div class="dateHeader visible-xs visible-sm"><fmt:message key="date" /></div>
						<div class="date">
							<c:if test="${model.level == 1}">
								<fmt:formatDate type="date" timeStyle="default" value="${order.dateLevel1}"/>
							</c:if>
							<c:if test="${model.level == 2}">
								<fmt:formatDate type="date" timeStyle="default" value="${order.dateLevel2}"/>
							</c:if>
						</div>
						<div class="clearfix"></div>
					</div>
				</div>
			</c:forEach>
			</div>
			
			
			<div class="accountAffiliateCommissionsFtr">
				<div class="row clearfix">
					<div class="col-sm-6">
						<div class="totalCommissionFooterLabel"><fmt:message key="totalCommissionForTheMonth" /></div>
					</div>
					<div class="col-sm-1">
						<div class="totalCommissionFooterValue">
							<c:if test="${!(yearMonthNew eq yearMonthOld)}">
								<fmt:formatNumber value="${model.map[yearMonthNew]}" pattern="#,##0.00" />
							</c:if>
							<c:if test="${yearMonthNew eq null}">
								<fmt:formatNumber value="${model.map[yearMonthNew]}" pattern="#,##0.00" />
							</c:if>
  						</div>
					</div>
					<div class="col-sm-5"></div>
				</div>
			</div>
		</div>
		</form>
		
	</div>
	</div>

  </tiles:putAttribute>
</tiles:insertDefinition>