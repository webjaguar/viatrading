<%@ page session="false" %>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>

<div id="btn-toggle-offcanvas" class="visible-xs">
  <button type="button" class="btn btn-primary" data-toggle="offcanvas">
	<i class="fa fa-bars"></i>
  </button>
</div>
  
<div class="col-xs-6 col-sm-3 col-md-2 sidebar-offcanvas" id="sidebar" role="navigation">
  <c:set var="actionUrl" value="lsearch.jhtm" />
  <c:if test="${model.categorySearchFilter}">
    <c:set var="actionUrl" value="category.jhtm" />
  </c:if>
  <form action="${_contextpath}/${actionUrl}" method="get" id="luceneSearch">
  <c:if test="${model.categorySearchFilter}">
    <input type="hidden" name="cid" value="<c:out value="${model.cid}"/>">
  </c:if>
  <div class="luceneSearchWrapper">
	  <div class="luceneSearchBox">
   	
		<input type="hidden" name="cid" value="<c:out value="${model.cid}"/>">
    	<input type="hidden" name="hideSearchBox" value="<c:out value="${model.hideSearchBox}"/>">
    	<input type="hidden" id="filterKeywords" name="keywords" value="<c:out value="${lProductSearch.keywords}"/>" maxlength="10">

			<c:if test="${lProductSearch.rangeFilters != null}">
		    <div class="luceneSearchSelectionWrapper">
		   	  <c:forEach items="${lProductSearch.rangeFilters}" var="rangeFilter">
		        <c:set var="filterValueExist" /> 
		        <c:forEach items="${rangeFilter.value}" var="rangeFilterAttribute">
		   	       <input type="hidden" name="${rangeFilterAttribute.name}" id="range_${rangeFilterAttribute.name}" value="${rangeFilterAttribute.value}"></input>
		           <c:if test="${rangeFilterAttribute.value != ''}"> 
		   	        <c:set var="filterValueExist" value="true"/> 
		          </c:if>
		   	    </c:forEach>
		
		   	    <c:if test="${filterValueExist == 'true'}"> 
		   	      <div class="luceneSearchSelectionTitle"><c:out value="${rangeFilter.key}" /></div>
		          <label class="checkbox-inline">
					<input type="checkbox" checked="checked" onclick="removeRangeFilter('${rangeFilter.value[0].filterId}');"></input> </span>
		            <c:forEach items="${rangeFilter.value}" var="rangeFilterAttribute" varStatus="attriStatus">
		   	          <c:if test="${attriStatus.index == 1 and rangeFilterAttribute.value != ''}">- </c:if><c:out value="${rangeFilterAttribute.value}" />
		   	        </c:forEach>
				  </label>
		   	    </c:if>
		      </c:forEach>
			</div>
		    </c:if>
 	
  			<c:if test="${model.filterMap != null and fn:length(lProductSearch.filters) > 0}">
    		  <div class="luceneSearchSelectionWrapper">
			   	<div class="luceneSearchSelectionTitle">You Selected</div>
			    <c:forEach items="${lProductSearch.filters}" var="filter" varStatus="filterStatus">
			   	  <div class="checkbox x-checkbox">
		            <label>
		                  <input type="checkbox" checked="checked" onclick="removeFilter('${filter.parent}', '${filterStatus.index}');"> <c:out value="${filter.displayName}" />
		            </label>
		      	  </div>
    		   	</c:forEach>
			  </div>
    		</c:if>
    		
    		<div id="luceneFilterSelWrapper" style="display: none;">
		   	  <c:forEach items="${lProductSearch.filters}" var="filter" varStatus="filterStatus" >
		   	      <input type="hidden" name="facetNameValue" id="filter${filter.parent}_${filterStatus.index}" value="${fn:escapeXml(filter.parent)}_value_${fn:escapeXml(filter.name)}"></input>
		      </c:forEach>
			</div>
  			
  			<c:if test="${model.filterMap != null and fn:length(model.filterMap) > 0}">
    		  <div class="luceneSearchFilters">
				<c:forEach items="${model.filterMap}" var="filterPair" varStatus="groupStatus">
      			<c:if test="${filterPair.key != null and filterPair.key != ''}">
		          <div class="filterGroupTitle">
		            <div class="filterGroupTitleToggle <c:if test="${filterPair.key != null and  filterPair.key != '' and filterPair.key != 'Category' and filterPair.key != 'Brand' and filterPair.key != 'Save'}">collapsed</c:if>" <c:if test="${filterPair.key != null and filterPair.key == 'Save'}">style="color:red;font-weight: bolder;font-size: 14px;"</c:if> data-toggle="collapse" data-target="#group${groupStatus.index+1}">
					  ${filterPair.key}
				    </div>
		          </div>
		        </c:if>
         
         		<c:choose>
         		  <c:when test="${fn:length(filterPair.value) > 1}">
         		    
         		<div id="group${groupStatus.index+1}" class="filterGroupValues collapse in">
				<c:forEach items="${filterPair.value}" var="filter" varStatus="status">
        		  <div class="luceneSearchFilter">
					<div class="filterTitle">
					  <c:set var="filterName" />
					  <c:choose>
			         	<c:when test="${filter.displayName != null}"><c:set var="filterName" value="${filter.displayName}"/></c:when>
			         	<c:otherwise><c:set var="filterName" value="${filter.name}"/></c:otherwise>
			          </c:choose>
  				  	  <div class="filterTitleToggle" data-toggle="collapse" data-target="#filter${groupStatus.index+1}_${status.index + 1}">
						${filterName}
					  </div>
					</div>
					
					<div id="filter${groupStatus.index+1}_${status.index + 1}" class="filterValues collapse in <c:if test="${fn:length(filter.subFilters) > 10 and filter.name != 'Origin'}">filterValuesForLuceneSearch</c:if>" >      	
		 			  <c:forEach items="${filter.subFilters}" var="subFilter" varStatus="statusI">
            		    <c:if test="${statusI.index < 50}">
              			  <div class="filterValue ${filter.name} ${subFilter.displayName}<c:if test="${statusI.index > 5}">filterIValue filterIValue${status.index}</c:if>">
				            <c:if test="${filter.checkboxFilter}">
				               <input type="checkbox" <c:if test="${subFilter.selected}">checked="checked"</c:if> onclick="submitSearch('${filter.name}','${fn:escapeXml(fn:replace(subFilter.name, "'", "\\'"))}' <c:if test="${subFilter.selected}">, 'true'</c:if>);">
					        </c:if>
				            <a href="#" onclick="submitSearch('${filter.name}','${fn:escapeXml(fn:replace(subFilter.name, "'", "\\'"))}');">
				              <c:out value="${subFilter.displayName}" escapeXml="false"/>
				              <span class="filterValueCount">
				                (<c:out value="${subFilter.resultCount}" escapeXml="false"/>)
				              </span>
				            </a>
				          </div>
			            </c:if>
					  </c:forEach>
					  
					  <c:if test="${filter.rangeFilter}">
				   	  <span class="luceneSearchSelection">
				        <input type="text" placeholder="min" class="filterInput"  style="width : 40px;"  name="field_${filter.filterId}_min" maxlength="10" width="10">
				         - 
				        <input type="text" placeholder="max" class="filterInput"  style="width : 40px;"  name="field_${filter.filterId}_max" maxlength="10" width="10">
				        <input type="submit" value="go" onclick="removeRangeFilter('${filter.filterId}')" />
				      </span>
		            </c:if>
					</div>
				  </div>
				
				</c:forEach>
				</div>

				</c:when>
				<c:otherwise>
				
         		    <c:set var="openFilter" value="" />
         		    <c:if test="${filterPair.key == null or  filterPair.key == '' or filterPair.key == 'Category' or filterPair.key == 'Brand' or filterPair.key == 'Save'}">
         		      <c:set var="openFilter" value="in" />
         		    </c:if>  
        		  
        		  
        		  <div id="group${groupStatus.index+1}" class="filterGroupValues collapse ${openFilter}">
				  <c:set var="filter" value="${filterPair.value[0]}" />
        		  <div class="luceneSearchFilter">
					
					<div id="filter${groupStatus.index+1}_${status.index + 1}" class="filterValues collapse in <c:if test="${fn:length(filter.subFilters) > 10 and filter.name != 'Origin'}">filterValuesForLuceneSearch</c:if>" >      	
		 			  <c:forEach items="${filter.subFilters}" var="subFilter" varStatus="statusI">
            		    <c:if test="${statusI.index < 50}">
              			  <div class="filterValue ${filter.name} ${subFilter.displayName}<c:if test="${statusI.index > 5}">filterIValue filterIValue${status.index}</c:if>">
					            <c:choose>
				                  <c:when test="${subFilter.redirectUrl != null}">
				                    <a href="${subFilter.redirectUrl}">
					                  <c:out value="${subFilter.displayName}" escapeXml="false"/>
					                  <span class="filterValueCount">
					                  (<c:out value="${subFilter.resultCount}" escapeXml="false"/>)
					                  </span>
				             	   </a>
				                  </c:when>
				                  <c:otherwise>
					                <c:if test="${filter.checkboxFilter}">
				                  	    <input type="checkbox" <c:if test="${subFilter.selected}">checked="checked"</c:if> onclick="submitSearch('${filter.name}','${fn:escapeXml(fn:replace(subFilter.name, "'", "\\'"))}' <c:if test="${subFilter.selected}">, 'true'</c:if>);">
					                </c:if>
				                    <a href="#" onclick="submitSearch('${filter.name}','${fn:escapeXml(subFilter.name)}');">
					                  <c:out value="${subFilter.displayName}" escapeXml="false"/>
					                  <span class="filterValueCount">
					                  (<c:out value="${subFilter.resultCount}" escapeXml="false"/>)
					                  </span>
					                </a>
					              </c:otherwise> 
				                </c:choose>
				                </div>
				        </c:if>
					  </c:forEach>
					  
					  <c:if test="${filter.rangeFilter}">
				   	  <span class="luceneSearchSelection">
				        <input type="text" placeholder="min" class="filterInput"  style="width : 40px;"  name="field_${filter.filterId}_min" maxlength="10" width="10">
				         - 
				        <input type="text" placeholder="max" class="filterInput"  style="width : 40px;"  name="field_${filter.filterId}_max" maxlength="10" width="10">
				        <input type="submit" value="go" onclick="removeRangeFilter('${filter.filterId}')" />
				      </span>
		            </c:if>
					</div>
				  </div>
				</div>
				
				
				
				
				</c:otherwise>
				</c:choose>
				</c:forEach>
			  </div>
			  </c:if>
    		</div>
		  </div>
		</form>
	  </div>

<script type="text/javascript">
<!--
function submitSearch(name, value, remove){
	
	if(remove) {
		$('input[name="facetNameValue"]').each(function(index) { 
		    if($(this).val() == name+"_value_"+value) {
		    	$(this).remove();
		    }
		})
	} else {
		var hiddenField = document.createElement("input");
		
		hiddenField.setAttribute("type", "hidden"); 
		hiddenField.setAttribute("name", "facetNameValue"); 
		hiddenField.setAttribute("value", name+"_value_"+value); 
		
		document.getElementById('luceneSearch').appendChild(hiddenField);
	}
	
	$('form#luceneSearch').submit();
}
function removeFilter(name, index){
	$("[id='filter"+name+"_"+index+"']").remove();
	$('form#luceneSearch').submit();
}
function removeRangeFilter(id){
	if($('range_field_'+id+'_min') != null) {
		$('range_field_'+id+'_min').dispose();
	}
	if($('range_field_'+id+'_max') != null) {
		$('range_field_'+id+'_max').dispose();
	}
	$('form#luceneSearch').submit();
}
//-->
</script>