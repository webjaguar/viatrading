<%@ page session="false" %>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="/WEB-INF/tlds/spring.tld" prefix="spring" %>

<tiles:insertDefinition name="${_template}" flush="true">
  <tiles:putAttribute name="content" type="string">
  
	<div class="col-sm-12">
		<form id="forgetPasswordForm" class="form-horizontal" role="form" method="post">
		<input type="hidden" name="_page" value="0">
			<div class="message"></div>
			<div id="forgetPassword">
				
				<div class="form-group">
					<div class="col-sm-12">
						<h3>Change Your Password in Three Easy Steps.</h3>
					</div>
				</div>
				<div class="form-group">
					<div class="col-sm-12">
						<div class="stepLabel"><span><img src="${_contextpath}/assets/Image/Layout/select_right_8x8.gif" border="0"> Step 1.</span> Enter the e-mail address associated with your <c:out value="${model.SITE_NAME}"/> account.</div>
					</div>
				</div>
				<div class="row">
					<div class="col-sm-6">
						<spring:bind path="forgetPasswordForm.email">
						<div class="form-group <c:if test="${status.error}">has-feedback has-error</c:if>">
							<div class="col-sm-4">
								<label for="emailAddress" class="control-label">
									<fmt:message key="emailAddress"/> <sup class="requiredField">*</sup>
								</label>
							</div>
							<div class="col-sm-8">
								<input type="text" autocomplete="off" class="form-control" name="<c:out value="${status.expression}"/>" value="<c:out value="${status.value}"/>"/>
								<c:if test="${status.error}">
									<i class="form-control-feedback glyphicon glyphicon-remove"></i>
									<small class="help-block error-message"><spring:bind path="forgetPasswordForm.message"><c:out value="${status.errorMessage}" escapeXml="false"/></spring:bind></small>
								</c:if>
							</div>
						</div>
						</spring:bind>
					</div>
					<div class="col-sm-6"></div>
				</div>
			</div>

			<div id="form_buttons">
				<div class="row">
					<div class="col-sm-12">
						<div id="buttons_wrapper">
							<button type="submit" name="_target1" class="btn btn-default"><fmt:message key="nextStep" /></button>
						</div>
					</div>
				</div>
			</div>
		</form>
	</div>
	
  </tiles:putAttribute>
</tiles:insertDefinition>
	