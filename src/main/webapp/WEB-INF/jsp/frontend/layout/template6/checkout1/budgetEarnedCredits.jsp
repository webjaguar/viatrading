<%@ page session="false" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>

<c:if test="${orderForm.customer.budgetPartnersList != null and fn:length(orderForm.customer.budgetPartnersList) gt 0 }" >

<div class="row">
	<div class="col-sm-12">
		<div id="giftCardContainer">
			<div class="giftCardMesage">
				<fmt:message key="f_earnedCredits" /><br>
				<fmt:message key="f_budgetPlanANotice"/>
			</div> 
			<c:forEach items="${orderForm.customer.budgetPartnersList}" var="partner">
				<c:set value="partnerAmt_${partner.partnerId}" var="partnerAmt"/>
				<c:if test="${partner.amount > 0.00 }">
				  <label class="giftCardCodeTitle" for="giftCardCodeInput">
				  	  <c:out value="${partner.partnerName}"/><br>
				      Available: <fmt:message key="${siteConfig['CURRENCY'].value}" /><fmt:formatNumber value="${partner.amount}" pattern="#,##0.00" /><br>
				  </label>
				  <input type="text" name="applyAmount_${partner.partnerId}" value="${model[partnerAmt]}" />
				</c:if>
			</c:forEach>
		</div>
	</div>
</div>


</c:if>
 