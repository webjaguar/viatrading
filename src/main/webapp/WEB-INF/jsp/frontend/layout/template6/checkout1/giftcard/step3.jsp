<%@ page session="false" %>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="/WEB-INF/tlds/spring.tld" prefix="spring" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>

<tiles:insertDefinition name="${_template}" flush="true">
  <tiles:putAttribute name="content" type="string">

<div class="col-breadcrumb">
	<ul class="breadcrumb">
		<li><a href="${_contextpath}/account.jhtm">My Account</a></li>
		<li class="active">Gift Card</li>
	</ul>
</div>

<c:out value="${giftCardLayout.headerHtml}" escapeXml="false"/>


<div class="col-sm-12">
	<form:form  commandName="giftCardForm" method="post">
	
		<input type="hidden" name="_page" value="2">
		
		<c:if test="${model.message != null}">
		  <div class="message"><fmt:message key="${model.message}" /></div>
		</c:if>
		<div class="message"><form:errors path="*" cssClass="errorBox"/></div>
		
		<div class="row">
			<div class="col-sm-12">
				<div class="giftCard_table">
					<div class="giftCardHdr">
						<div class="row clearfix">
							<div class="col-sm-12 col-md-2">
								<div class="lineNmbrHeader"><fmt:message key="line" /> #</div>
							</div>
							<div class="col-sm-12 col-md-6">
								<div class="giftCardItemHeader"><fmt:message key="item" /></div>
							</div>
							<div class="col-sm-12 col-md-2">
								<div class="giftCardAmountHeader"><fmt:message key="amount" /></div>
							</div>
							<div class="col-sm-12 col-md-2">
								<div class="giftCardQuantityHeader"><fmt:message key="qty" /></div>
							</div>
						</div>
					</div>

					<div class="giftCardDetails">
						
						<c:if test="${giftCardForm.giftCardOrder.amount1 != null}">
  						<div class="odd_row clearfix">
							<div class="col-sm-12 col-md-2">
								<div class="lineNmbrHeader visible-xs visible-sm"><fmt:message key="line" /></div>
								<div class="lineNmbr">1</div>
								<div class="clearfix"></div>
							</div>
							<div class="col-sm-12 col-md-6">
								<div class="giftCardItemHeader visible-xs visible-sm"><fmt:message key="item" /></div>
								<div class="giftCardItem"><fmt:message key="giftCard" /></div>
								<div class="clearfix"></div>
							</div>
							<div class="col-sm-12 col-md-2">
								<div class="giftCardAmountHeader visible-xs visible-sm"><fmt:message key="amount" /></div>
								<div class="giftCardAmount"><fmt:message key="${siteConfig['CURRENCY'].value}" /><fmt:formatNumber value="${giftCardForm.giftCardOrder.amount1}" pattern="#,##0.00#" maxFractionDigits="${siteConfig['MAX_FRACTION_DIGIT'].value}"/></div>
								<div class="clearfix"></div>
							</div>
							<div class="col-sm-12 col-md-2">
								<div class="giftCardQuantityHeader visible-xs visible-sm"><fmt:message key="quantity" /></div>
								<div class="giftCardQuantity"><c:out value="${giftCardForm.giftCardOrder.quantity1}"/></div>
								<div class="clearfix"></div>
							</div>
						</div>
						</c:if>
						
						<c:if test="${giftCardForm.giftCardOrder.amount2 != null}">
  						<div class="even_row clearfix">
							<div class="col-sm-12 col-md-2">
								<div class="lineNmbrHeader visible-xs visible-sm"><fmt:message key="line" /></div>
								<div class="lineNmbr">2</div>
								<div class="clearfix"></div>
							</div>
							<div class="col-sm-12 col-md-6">
								<div class="giftCardItemHeader visible-xs visible-sm"><fmt:message key="item" /></div>
								<div class="giftCardItem"><fmt:message key="giftCard" /></div>
								<div class="clearfix"></div>
							</div>
							<div class="col-sm-12 col-md-2">
								<div class="giftCardAmountHeader visible-xs visible-sm"><fmt:message key="amount" /></div>
								<div class="giftCardAmount"><fmt:message key="${siteConfig['CURRENCY'].value}" /><fmt:formatNumber value="${giftCardForm.giftCardOrder.amount2}" pattern="#,##0.00#" maxFractionDigits="${siteConfig['MAX_FRACTION_DIGIT'].value}"/></div>
								<div class="clearfix"></div>
							</div>
							<div class="col-sm-12 col-md-2">
								<div class="giftCardQuantityHeader visible-xs visible-sm"><fmt:message key="quantity" /></div>
								<div class="giftCardQuantity"><c:out value="${giftCardForm.giftCardOrder.quantity2}"/></div>
								<div class="clearfix"></div>
							</div>
						</div>
						</c:if>
						
						<c:if test="${giftCardForm.giftCardOrder.amount3 != null}">
  						<div class="odd_row clearfix">
							<div class="col-sm-12 col-md-2">
								<div class="lineNmbrHeader visible-xs visible-sm"><fmt:message key="line" /></div>
								<div class="lineNmbr">3</div>
								<div class="clearfix"></div>
							</div>
							<div class="col-sm-12 col-md-6">
								<div class="giftCardItemHeader visible-xs visible-sm"><fmt:message key="item" /></div>
								<div class="giftCardItem"><fmt:message key="giftCard" /></div>
								<div class="clearfix"></div>
							</div>
							<div class="col-sm-12 col-md-2">
								<div class="giftCardAmountHeader visible-xs visible-sm"><fmt:message key="amount" /></div>
								<div class="giftCardAmount"><fmt:message key="${siteConfig['CURRENCY'].value}" /><fmt:formatNumber value="${giftCardForm.giftCardOrder.amount3}" pattern="#,##0.00#" maxFractionDigits="${siteConfig['MAX_FRACTION_DIGIT'].value}"/></div>
								<div class="clearfix"></div>
							</div>
							<div class="col-sm-12 col-md-2">
								<div class="giftCardQuantityHeader visible-xs visible-sm"><fmt:message key="quantity" /></div>
								<div class="giftCardQuantity"><c:out value="${giftCardForm.giftCardOrder.quantity3}"/></div>
								<div class="clearfix"></div>
							</div>
						</div>
						</c:if>
						
						<c:if test="${giftCardForm.giftCardOrder.amount4 != null}">
  						<div class="even_row clearfix">
							<div class="col-sm-12 col-md-2">
								<div class="lineNmbrHeader visible-xs visible-sm"><fmt:message key="line" /></div>
								<div class="lineNmbr">4</div>
								<div class="clearfix"></div>
							</div>
							<div class="col-sm-12 col-md-6">
								<div class="giftCardItemHeader visible-xs visible-sm"><fmt:message key="item" /></div>
								<div class="giftCardItem"><fmt:message key="giftCard" /></div>
								<div class="clearfix"></div>
							</div>
							<div class="col-sm-12 col-md-2">
								<div class="giftCardAmountHeader visible-xs visible-sm"><fmt:message key="amount" /></div>
								<div class="giftCardAmount"><fmt:message key="${siteConfig['CURRENCY'].value}" /><fmt:formatNumber value="${giftCardForm.giftCardOrder.amount4}" pattern="#,##0.00#" maxFractionDigits="${siteConfig['MAX_FRACTION_DIGIT'].value}"/></div>
								<div class="clearfix"></div>
							</div>
							<div class="col-sm-12 col-md-2">
								<div class="giftCardQuantityHeader visible-xs visible-sm"><fmt:message key="quantity" /></div>
								<div class="giftCardQuantity"><c:out value="${giftCardForm.giftCardOrder.quantity4}"/></div>
								<div class="clearfix"></div>
							</div>
						</div>
						</c:if>
						
						<c:if test="${giftCardForm.giftCardOrder.amount5 != null}">
  						<div class="even_row clearfix">
							<div class="col-sm-12 col-md-2">
								<div class="lineNmbrHeader visible-xs visible-sm"><fmt:message key="line" /></div>
								<div class="lineNmbr">5</div>
								<div class="clearfix"></div>
							</div>
							<div class="col-sm-12 col-md-6">
								<div class="giftCardItemHeader visible-xs visible-sm"><fmt:message key="item" /></div>
								<div class="giftCardItem"><fmt:message key="giftCard" /></div>
								<div class="clearfix"></div>
							</div>
							<div class="col-sm-12 col-md-2">
								<div class="giftCardAmountHeader visible-xs visible-sm"><fmt:message key="amount" /></div>
								<div class="giftCardAmount"><fmt:message key="${siteConfig['CURRENCY'].value}" /><fmt:formatNumber value="${giftCardForm.giftCardOrder.amount5}" pattern="#,##0.00#" maxFractionDigits="${siteConfig['MAX_FRACTION_DIGIT'].value}"/></div>
								<div class="clearfix"></div>
							</div>
							<div class="col-sm-12 col-md-2">
								<div class="giftCardQuantityHeader visible-xs visible-sm"><fmt:message key="quantity" /></div>
								<div class="giftCardQuantity"><c:out value="${giftCardForm.giftCardOrder.quantity5}"/></div>
								<div class="clearfix"></div>
							</div>
						</div>
						</c:if>
						
					</div>

					<div class="giftCardFtr"></div>
				</div>
			</div>
		</div>
									
		<div id="form_buttons">
			<div class="row">
				<div class="col-sm-12">
					<div id="buttons_wrapper">
						<button type="submit" name="_target1" class="btn btn-default">Back</button>
						<button type="submit" name="_finish" class="btn btn-default">Checkout</button>
					</div>
				</div>
			</div>
		</div>
	</form:form>
</div>
						
<c:out value="${giftCardLayout.footerHtml}" escapeXml="false"/>

  </tiles:putAttribute>
</tiles:insertDefinition>