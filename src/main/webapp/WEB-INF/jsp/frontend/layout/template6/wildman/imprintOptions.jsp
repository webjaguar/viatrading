<%@ page session="false" %>
<%@ page import="java.util.*" %>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="/WEB-INF/tlds/spring.tld" prefix="spring" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>

<div class="configBox_wrapper imprintLogo">
	    <div class="configBox_title">Select Logo Application</div>
	    <div class="configbox_content">
	    <input type="hidden" id="selectedLocations" value="1"/>
		<c:forEach items="${model.storeImprintOptions}" var="imprint" varStatus="imprintStatus">
		  <div id="imprintLogoOptionsWrapper${imprintStatus.index + 1}" class="imprintLogoOptionsWrapper" >
		  <c:if test="${imprint.required}">
		    <input type="hidden" class="requiredImprintStatusValue" value="${imprint.id}_${imprintStatus.index + 1}"/>
		  </c:if>
		  
		  <input type="hidden" name="imprintId" value="${imprint.id}"/>
		  <div class="logoList">
	        <div class="demo-content">
			  <div class="demo-live">
				<div id="demoBasic_${imprintStatus.index + 1}"></div>
				<div class="spacer"></div>
				<input type="hidden" name="imprint_logo_${imprint.id}_${imprintStatus.index + 1}" value="" id="imprint_logo_${imprint.id}_${imprintStatus.index + 1}">
			  </div>
			  <script type="text/javascript">
				<!--
				var ddData = [
					<c:forEach items="${model.galleryFiles}" var="logo" varStatus="status">
					{
				    	text: "${logo.name}",
				    	value: ${status.index + 1},
				    	selected: false,
				    	imageSrc: "${_contextpath}${logo.imageUrl}"
			        }<c:if test="${(status.index + 1) < fn:length(model.galleryFiles)}">,</c:if>
					</c:forEach>
				    ];
						
					jqry('#demoBasic_${imprintStatus.index + 1}').ddslick({
				    	data: ddData,
					   	imagePosition: "left",
					   	selectText: "Logo ${imprintStatus.index + 1}",
					   	onSelected: function (data) {
							$('#imprint_logo_${imprint.id}_${imprintStatus.index + 1}').val($('#demoBasic_${imprintStatus.index + 1} .dd-selected-text').text());
					   	}
					});
				//-->
			  </script>
			</div>
	     </div>
	     <div class="locationList">
	       <select name="imprintOptionNVPair_${imprint.id}_${imprintStatus.index + 1}" onchange="updateLocations('${model.storeImprintOptions[imprintStatus.index + 1].id}');" class="optionValueSelect imprintLocation" id="imprintLocation${imprintStatus.index + 1}">
			 <option value="">Location ${imprintStatus.index + 1} </option>
			 <c:forEach items="${fn:split(imprint.location, ',')}" var="imprintLocation" varStatus="imLocation">
			   <option value="Imprint Location ${imprintStatus.index + 1}_value_${fn:trim(imprintLocation)}" <c:if test="${model.selectedImprintLocation == imprintLocation}">selected="selected"</c:if>><c:out value="${imprintLocation}"/></option>
			 </c:forEach>
		   </select>
	     </div>
	     
	     <c:if test="${imprint.color != null and fn:length(imprint.color) > 0}">
	     <div class="imprintColorList">
   	       <select name="imprintColor_${imprint.id}_${imprintStatus.index + 1}" class="optionValueSelect" id="imprintColor${imprintStatus.index + 1}">
			 <option value="">Custom Thread Color ${imprintStatus.index + 1} </option>
			 <c:forEach items="${fn:split(imprint.color, ',')}" var="imprintColor" varStatus="imColor">
			   <option value="${imprintColor}"><c:out value="${imprintColor}"/></option>
			 </c:forEach>
		   </select>
	     </div>
	     </c:if>  
	     
	   </div>
	   </c:forEach>
	   
	   <c:if test="${fn:length(model.storeImprintOptions) gt 1}">
		   <div class="applyAnotherLogo_btn_wrapper">
				<button type="button" class="btn btn-default" onclick="loadLocation();">
					APPLY ANOTHER LOGO
				</button>
		   </div>
	    </c:if>
	    
	   </div>
	   </div>
	   
	   
	  <c:if test="${model.monogramImprintOption != null}">
  	  <div class="configBox_wrapper optionalPersonalization">
	    <div class="configBox_title">Optional Personalization</div>
	    <div class="configbox_content">
		    <div class="optionWrapper">
		      <input type="hidden" name="optionNVPair" id="monogramText" value="monogram text_value_${fn:escapeXml(model.variant)}"/>
			  <textarea name="monogramTextValue" id="monogramTextValueId" class="optionTextArea" onkeyup="updateMonogramText(this.value);" rows="5" cols="25"></textarea>
		    </div>
	
		    <div class=optionWrapper >
		      <select style="width: 157;" name="monogramLocation_${model.monogramImprintOption.id}_${imprintStatus.index + 1}" class="optionValueSelect monogramLocation" id="monogramLocation">
			    <option value="">Select Location</option>
				<c:forEach items="${fn:split(model.monogramImprintOption.location, ',')}" var="monogramLocation">
				  <option value="${fn:trim(monogramLocation)}"><c:out value="${monogramLocation}"/></option>
				</c:forEach>
			  </select>
		    </div>
		    
		   <div class="optionWrapper">
		     <select style="width: 157;" name="monogramFont_${model.monogramImprintOption.id}_${imprintStatus.index + 1}" class="optionValueSelect" id="monogramFont" onChange="applyStyle()">
			   <option value="">Pick Monogramming font</option>
			   <option value="Block Upper Case">Block Upper Case</option>
			   <option value="Block Lower Case">Block Lower Case</option>
			   <option value="Script">Script</option>
			   <option value="Italic Upper Case">Italic Upper Case</option>
			   <option value="Italic Lower Case">Italic Lower Case</option>
			 </select>
		   </div>
		   
		   <div class="optionWrapper">
		      <select style="width: 157;" name="monogramColor_${model.monogramImprintOption.id}_${imprintStatus.index + 1}" class="optionValueSelect" id="monogramColor">
				<option value="">Select Color</option>
				<c:forEach items="${fn:split(model.monogramImprintOption.monogramColors, ',')}" var="imprintMonogramColor" varStatus="imColor">
				  <option value="${imprintMonogramColor}"><c:out value="${imprintMonogramColor}"/></option>
				</c:forEach>
			  </select>
		   </div>
	   </div>
	   </div>
	 </c:if>
	  