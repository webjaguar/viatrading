<%@ page session="false" %>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="/WEB-INF/tlds/spring.tld" prefix="spring" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="/WEB-INF/tlds/custom.tld" prefix="wj" %>

<%@ page import="java.util.List"%>
<tiles:insertDefinition name="${_template}" flush="true">
  <c:if test="${_leftBar != '1' and _leftBar != '4'}">
    <tiles:putAttribute name="leftbar" value="/WEB-INF/jsp/frontend/layout/template${_leftBar}/leftbar.jsp" />
  </c:if>
  <tiles:putAttribute name="content" type="string">

<div class="col-breadcrumb">
	<ul class="breadcrumb">
		<li><a href="${_contextpath}/account.jhtm">My Account</a></li>
		<li class="active">Orders</li>
	</ul>
</div>

<c:if test="${fn:trim(model.myOrderHistoryLayout.headerHtml) != ''}">
  <c:set value="${model.myOrderHistoryLayout.headerHtml}" var="headerHtml"/>
  <c:set value="${fn:replace(headerHtml, '#firstname#', userSession.firstName)}" var="headerHtml"/>
  <c:set value="${fn:replace(headerHtml, '#lastname#', userSession.lastName)}" var="headerHtml"/>
  <c:set value="${fn:replace(headerHtml, '#email#', userSession.username)}" var="headerHtml"/>
  <c:set value="${fn:replace(headerHtml, '#lastOrderId#', model.lastOrderId)}" var="headerHtml"/>
  <c:set value="${fn:replace(headerHtml, '#lastOrderStatus#', model.lastStatus)}" var="headerHtml"/>
  <c:set value="${fn:replace(headerHtml, '#lastOrderSubStatus#', model.lastSubStatus)}" var="headerHtml"/>
  <c:out value="${headerHtml}" escapeXml="false"/>
</c:if>


<div class="col-sm-12">
	<div class="accountOrdersWrapper">
		<div class="headerTitle accountOrders"><fmt:message key="orderHistoryStatus" /></div>

		<div class="clearfix"></div>
<%--

		<c:choose>
		  <c:when test="${model.orders.pageCount == null or (model.orders.pageCount == 1 and fn:length(model.orders.pageList) == 0)}">
			<div class="message">Your List is empty.</div>
		  </c:when>
		  <c:otherwise>
			<div class="row">
				<div class="col-sm-12">
					<div class="pageNavigation">
						<form class="formPageNavigation clearfix" name="pagination" method="post">
							<input type="hidden" id="sort" name="sort" value="${orderSearch.sort}" /> 
							<div class="pageNavigationControl">
								<div class="page">
									<span>Page</span> 
									<select class="selectpicker" name="page" onchange="submit()">
										<c:forEach begin="1" end="${model.orders.pageCount}" var="page">
								  	        <option value="${page}" <c:if test="${page == (model.orders.page+1)}">selected</c:if>>${page}</option>
										</c:forEach>
									</select>
									<span>of <c:out value="${model.orders.pageCount}"/></span>
								</div>
								
							</div>
						</form>
					</div>
				</div>
			</div>
	
			<div class="accountOrders_table">
				<div class="accountOrdersHdr">
					<div class="row clearfix">
						<div class="col-sm-12 col-md-2">
							<div class="listOrderDateHeader">
								<a href="#"><fmt:message key="orderDate" /></a>
							</div>
						</div>
						<div class="col-sm-12 col-md-2">
							<div class="listOrderIDHeader"><fmt:message key="order" /></div>
						</div>
						<div class="col-sm-12 col-md-1">
							<div class="listPONumHeader">P.O.#</div>
						</div>
						<div class="col-sm-12 col-md-2">
							<div class="listStatusHeader"><fmt:message key="status" /></div>
						</div>
						<div class="col-sm-12 col-md-1">
							<div class="listTotalHeader"><fmt:message key="total" /></div>
						</div>
						<div class="col-sm-12 col-md-2">
							<div class="listReorderHeader"><fmt:message key="f_reorder" /></div>
						</div>
						<div class="col-sm-12 col-md-2">
							<div class="listTrackCodeHeader"><fmt:message key="f_trackingNumber" /></div>
						</div>
					</div>
				</div>
	
				<div class="accountOrdersDetails">
					<c:forEach items="${model.orders.pageList}" var="order" varStatus="status">
					<c:set var="statusClass" value="odd_row" />
					<c:if test="${(status.index + 1) % 2 == 0}">
						<c:set var="statusClass" value="even_row" />
					</c:if>
				    		
					<div class="${statusClass} clearfix">
						<div class="col-sm-12 col-md-2">
							<div class="listOrderDateHeader visible-xs visible-sm"><fmt:message key="orderDate" /></div>
							<div class="listOrderDate"><fmt:formatDate type="date" timeStyle="default" value="${order.dateOrdered}"/></div>
							<div class="clearfix"></div>
						</div>
						<div class="col-sm-12 col-md-2">
							<div class="listOrderIDHeader visible-xs visible-sm"><fmt:message key="order" /></div>
							<div class="listOrderID">
								<a href="invoice.jhtm?order=${order.orderId}" class="nameLink"><c:out value="${order.orderId}"/></a>
							</div>
							<div class="clearfix"></div>
						</div>
						<div class="col-sm-12 col-md-1">
							<div class="listPONumHeader visible-xs visible-sm">P.O.#</div>
							<div class="listPONum">${order.purchaseOrder}</div>
							<div class="clearfix"></div>
						</div>
						<div class="col-sm-12 col-md-2">
							<div class="listStatusHeader visible-xs visible-sm"><fmt:message key="status" /></div>
							<div class="listStatus"><fmt:message key="orderStatus_${order.status}"/></div>
							<div class="clearfix"></div>
						</div>
						<div class="col-sm-12 col-md-1">
							<div class="listTotalHeader visible-xs visible-sm"><fmt:message key="total" /></div>
							<div class="listTotal"><fmt:message key="${siteConfig['CURRENCY'].value}" /><fmt:formatNumber value="${order.grandTotal}" pattern="#,##0.00" /></div>
							<div class="clearfix"></div>
						</div>
						<div class="col-sm-12 col-md-2">
							<div class="listReorderHeader visible-xs visible-sm"><fmt:message key="f_reorderItems" /></div>
							<div class="listReorder">
								<a href="reorder.jhtm?order=${order.orderId}"><fmt:message key="f_reorderItems" /></a>
							</div>
							<div class="clearfix"></div>
						</div>
						<div class="col-sm-12 col-md-2">
							<div class="listTrackCodeHeader visible-xs visible-sm"><fmt:message key="f_trackingNumber" /></div>
							<div class="listTrackCode">
								<c:set var="tackNumFound" value="false" />
								
								<c:forEach items="${model.trackNumMap[order.orderId]}" var="orderstatus" varStatus="status"><c:if test="${orderstatus.trackNum != null and fn:trim(orderstatus.trackNum) != ''}"><c:if test="${tackNumFound}"><br/></c:if><c:set var="tackNumFound" value="true" /><c:choose><c:when test="${fn:containsIgnoreCase(order.shippingMethod,'ups') or fn:containsIgnoreCase(order.shippingCarrier,'ups')}"><a target="_blank" href="http://wwwapps.ups.com/WebTracking/processInputRequest?HTMLVersion=5.0&sort_by=status&tracknums_displayed=10&TypeOfInquiryNumber=T&loc=en_US&InquiryNumber1=<c:out value='${orderstatus.trackNum}' />&AgreeToTermsAndConditions=yes"><c:out value="${orderstatus.trackNum}"/></a></c:when><c:when test="${fn:containsIgnoreCase(order.shippingMethod,'fedex') or fn:containsIgnoreCase(order.shippingCarrier,'fedex')}"><a target="_blank" href="http://fedex.com/Tracking?action=track&language=english&cntry_code=us&mps=y&tracknumbers=<c:out value='${orderStatus.trackNum}' />&ascend_header=1"><c:out value="${orderstatus.trackNum}"/></a></c:when><c:when test="${fn:containsIgnoreCase(order.shippingMethod,'usps') or fn:containsIgnoreCase(order.shippingCarrier,'usps')}"><a target="_blank" href="http://trkcnfrm1.smi.usps.com/PTSInternetWeb/InterLabelInquiry.do?strOrigTrackNum=<c:out value='${orderStatus.trackNum}' />"><c:out value="${orderstatus.trackNum}"/></a></c:when><c:otherwise><c:out value="${orderstatus.trackNum}"/></c:otherwise></c:choose></c:if></c:forEach>
							</div>
							<div class="clearfix"></div>
						</div>
					</div>
					</c:forEach>
				</div>
	
				<div class="accountOrdersFtr">
					<div class="row clearfix"></div>
				</div>
			</div>
		  </c:otherwise>
		</c:choose>
--%>
		<script type="text/javascript" src="/dv/liveapps/scripts.js.php"></script>
        <link rel="stylesheet" type="text/css" href="/dv/liveapps/myaccount.css">
	
		<div id="myorders">
		<script type="text/javascript">
		<!--
			$(document).ready(function() {
				myorders(0,0,${model.userId});
			});
		//-->
		</script>
		
		
		</div>


	</div>
</div>

<c:if test="${fn:trim(model.myOrderHistoryLayout.footerHtml) != ''}">
  <c:set value="${model.myOrderHistoryLayout.footerHtml}" var="footerHtml"/>
  <c:set value="${fn:replace(footerHtml, '#firstname#', userSession.firstName)}" var="footerHtml"/>
  <c:set value="${fn:replace(footerHtml, '#lastname#', userSession.lastName)}" var="footerHtml"/>
  <c:set value="${fn:replace(footerHtml, '#email#', userSession.username)}" var="footerHtml"/>
  <c:set value="${fn:replace(footerHtml, '#lastOrderId#', model.lastOrderId)}" var="footerHtml"/>
  <c:set value="${fn:replace(footerHtml, '#lastOrderStatus#', model.lastStatus)}" var="footerHtml"/>
  <c:set value="${fn:replace(footerHtml, '#lastOrderSubStatus#', model.lastSubStatus)}" var="footerHtml"/>
  <c:out value="${footerHtml}" escapeXml="false"/>
</c:if>

  </tiles:putAttribute>
</tiles:insertDefinition>
