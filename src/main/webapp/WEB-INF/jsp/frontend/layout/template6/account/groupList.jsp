<%@ page session="false"%>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="/WEB-INF/tlds/custom.tld" prefix="wj"%>

<tiles:insertDefinition name="${_template}" flush="true">
	<c:if test="${_leftBar != '1' and _leftBar != '4'}">
		<tiles:putAttribute name="leftbar"
			value="/WEB-INF/jsp/frontend/layout/template${_leftBar}/leftbar.jsp" />
	</c:if>
	<tiles:putAttribute name="content" type="string">
		<c:if test="${model.groupList.nrOfElements > 0}">
			<script language="JavaScript">
<!--
function toggleAll(el) {
	var ids = document.getElementsByName("__selected_id");	
  	if (ids.length == 1)
      document.mylist.__selected_id.checked = el.checked;
    else
      for (i = 0; i < ids.length; i++)
        document.mylist.__selected_id[i].checked = el.checked;	
} 

function deleteSelected() {
	var ids = document.getElementsByName("__selected_id");	
  	if (ids.length == 1) {
      if (document.mylist.__selected_id.checked) {
    	return true;
      }
    } else {
      for (i = 0; i < ids.length; i++) {
        if (document.mylist.__selected_id[i].checked) {
    	  return true;
        }
      }
    }

    alert("Please select product(s) to delete.");       
    return false;
}
//-->
</script>
		</c:if>
		<div id="pageWrapper">

			<c:out value="${model.myGrouplistLayout.headerHtml}"
				escapeXml="false" />

			<!-- ========================= -->
			<!--           BODY            -->
			<!-- ========================= -->
			<div id="pageBodyWrapper">
				<div id="pageBody" class="container">
					<div class="row row-offcanvas row-offcanvas-left">

						<!-- ========================= -->
						<!--          CONTENT          -->
						<!-- ========================= -->
						<div id="contentWrapper">
							<div id="content">
								<div class="col-breadcrumb">
									<ul class="breadcrumb">
										<li><a href="${_contextpath}/account.jhtm">My Account</a></li>
										<li class="active">Group List</li>
									</ul>
								</div>

								<div class="col-sm-12">
									<div class="groupListWrapper">
										<div class="headerTitle groupList">Group List</div>

										<div class="clearfix"></div>

										<c:if test="${!empty model.message}">
											<div class="message">
												<fmt:message key="${model.message}" />
											</div>
										</c:if>

										<div class="row">
											<div class="col-sm-12">
												<div class="pageNavigation">
													<form class="formPageNavigation clearfix" name="pagination"
														action="#">
														<div class="pageNavigationControl">
															<%-- 
														<div class="pagesize">
															<span>Page Size</span>
															<select class="selectpicker" name="pagesize">
																<option value="10">10 per page</option>
																<option value="20">20 per page</option>
																<option value="25" selected="">25 per page</option>
																<option value="50">50 per page</option>
																<option value="100">100 per page</option>
															</select>
														</div>--%>
															<div class="page">
																<span>Page</span> <select name="page"
																	class="selectpicker" onchange="submit()">
																	<c:forEach begin="1" end="${model.groupList.pageCount}"
																		var="page">
																		<option value="${page}"
																			<c:if test="${page == (model.groupList.page+1)}">selected</c:if>>${page}</option>
																	</c:forEach>
																</select> <span>of <c:out
																		value="${model.groupList.pageCount}" /></span>
															</div>
															<%-- 
														<div class="pageNav">
															<span class="pageNavLink">
																<a href="#"><span class="pageNavPrev"></span></a>
															</span>
															<span class="pageNavLink">
																<a href="#"><span class="pageNavNext"></span></a>
															</span>
														</div>--%>
														</div>
													</form>
												</div>
											</div>
										</div>
										<form name="mylist" method="post">
											<div class="addGroupWrapper">
												<input type="text" class="groupName" name="_groupname">
												<div class="addGroup_btn_wrapper">
													<button type="submit" class="btn addGroup_btn" name="_addGroup">Add Group</button>
												</div>
												<div class="clearfix"></div>
											</div>
											<c:if test="${model.groupList.nrOfElements > 0}">
												<div class="groupList_table">
													<div class="groupListHdr">
														<div class="row clearfix">
															<c:if test="${model.groupList.nrOfElements > 0}">
																<div class="col-sm-12 col-md-1">
																	<div class="listSelectHeader">
																		<input type="checkbox" name="selected_id"
																			onclick="toggleAll(this)">
																	</div>
																</div>
															</c:if>
															<div class="col-sm-12 col-md-10">
																<div class="listNameHeader">Name</div>
															</div>
															<div class="col-sm-12 col-md-1"></div>
														</div>
													</div>

													<div class="groupListDetails">
														<c:forEach items="${model.groupList.pageList}"
															var="myListGroup" varStatus="status">
															<div
																class="<c:if test="${status.index % 2 == 0}">evenClass</c:if><c:if test="${status.index % 2 != 0}">rowClass</c:if> clearfix">
																<c:if test="${model.groupList.nrOfElements > 0}">
																	<div class="col-sm-12 col-md-1">
																		<div class="listSelectHeader visible-xs visible-sm"></div>
																		<div class="listSelect">
																			<input type="checkbox" name="__selected_id"
																				value="${myListGroup.id}">
																		</div>
																		<div class="clearfix"></div>
																	</div>
																</c:if>
																<div class="col-sm-12 col-md-10">
																	<div class="listNameHeader visible-xs visible-sm">Name</div>
																	<div class="listName">
																		<a href="viewList.jhtm?gid=${myListGroup.id}"
																			class="nameLink"><c:out
																				value="${myListGroup.name}" /></a>
																	</div>
																	<div class="clearfix"></div>
																</div>
																<%-- 
													<div class="col-sm-12 col-md-1">
														<div class="listDelete"><a href="#">Delete</a></div>
														<div class="clearfix"></div>
													</div>--%>
															</div>
														</c:forEach>
													</div>
													<c:if test="${model.groupList.nrOfElements > 0}">
														<div class="groupListFtr">
															<div class="row clearfix">
																<div class="col-sm-12">
																	<div class="deleteSelectedGroup_btn_wrapper">
																		<button type="submit" name="__delete_group"
																			class="btn deleteSelectedGroup_btn"
																			onclick="return deleteSelected()">Delete
																			Selected Group</button>
																	</div>
																</div>
															</div>
														</div>
													</c:if>
												</div>
											</c:if>
										</form>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>

			<c:out value="${model.myGrouplistLayout.footerHtml}"
				escapeXml="false" />
		</div>

	</tiles:putAttribute>
</tiles:insertDefinition>