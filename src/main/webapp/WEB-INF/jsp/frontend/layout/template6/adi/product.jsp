<%@ page session="false" %>
<%@ page import="java.util.*" %>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="/WEB-INF/tlds/spring.tld" prefix="spring" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ taglib uri="/WEB-INF/tlds/custom.tld" prefix="wj" %>

<tiles:insertDefinition name="${_template}" flush="true">
  
  
  <c:if test="${_leftBar != '1' and _leftBar != '4'}">
    <tiles:putAttribute name="leftbar" value="/WEB-INF/jsp/frontend/layout/template${_leftBar}/leftbar.jsp" />
  </c:if>
  <tiles:putAttribute name="content" type="string">

<c:if test="${fn:trim(model.productLayout.headerHtml) != ''}">
  <c:set value="${model.productLayout.headerHtml}" var="headerHtml"/>
  <%@ include file="/WEB-INF/jsp/frontend/common/headerDynamicElement.jsp" %>
  <div><c:out value="${headerHtml}" escapeXml="false"/></div>
</c:if>

<c:if test="${model.message != null}">
  <div class="message"><fmt:message key="${model.message}" /></div>
</c:if>

<c:if test="${model.product != null}">
<c:set value="${model.product}" var="product"/>

<%-- bread crumbs --%>
<c:if test="${model.breadCrumbs != null and fn:length(model.breadCrumbs) > 0}">
  <c:import url="/WEB-INF/jsp/frontend/layout/${_template}/breadcrumbs.jsp" />
</c:if>

<div class="col-sm-12">
  <div id="product_details" class="row">
	<div class="col-sm-4">
	  <div class="details_image_box">
		<a href="<c:if test="${!model.product.images[0].absolute}">${_contextpath}/assets/Image/Product/detailsbig/</c:if><c:out value="${model.product.images[0].imageUrl}"/>" data-lightbox="product_images" data-title="<c:out value="${product.name}" escapeXml="false" />">
		  <img src="<c:if test="${!model.product.images[0].absolute}">${_contextpath}/assets/Image/Product/detailsbig/</c:if><c:out value="${model.product.images[0].imageUrl}"/>" title="111" class="img-responsive center-block">
		</a>
	  </div>
	  <c:if test="${fn:length(model.product.images) > 1}">
	  <div class="details_image_thumbnails">
	    <ul class="clearfix">
		  <c:forEach items="${model.product.images}" var="image" varStatus="status">
			<c:if test="${status.index > 0}">
			<li>
		      <a href="<c:if test="${!image.absolute}">${_contextpath}/assets/Image/Product/detailsbig/</c:if><c:out value="${image.imageUrl}"/>" data-lightbox="product_images" data-title="<c:out value="${product.name}" escapeXml="false" />">
			    <img src="<c:if test="${!image.absolute}">${_contextpath}/assets/Image/Product/thumb/</c:if><c:out value="${image.thumbnailImageUrl}"/>" class="img-responsive">
			  </a>
		    </li>
		    </c:if>
		  </c:forEach>
		</ul>
	  </div>
	  </c:if>
	</div>

	<div class="col-sm-8">
	  <div class="details_desc clearfix">
		
		<div class="details_sku"><c:out value="${product.sku}" escapeXml="false"/></div>
		
		<div class="details_item_name">
		  <h1>
		    <c:choose>
		      <c:when test="${fn:contains(siteConfig['SITE_URL'].value, 'adi') and fn:trim(product.longDesc) != ''}">
		        <c:out value="${product.longDesc}" escapeXml="false"/>
		      </c:when>
		      <c:otherwise>
			    <c:out value="${product.name}" escapeXml="false"/>
		      </c:otherwise>
		    </c:choose>
		  </h1>
		</div>
		
		<c:if test="${siteConfig['DETAILS_SHORT_DESC'].value == 'true' and product.shortDesc != ''}">
		  <div class="details_item_description"><c:out value="${product.shortDesc}" escapeXml="false"/></div>
		</c:if>
		
		<c:choose>
		  <c:when test="${ (gSiteConfig['gSHOPPING_CART'] and (!empty product.price or product.priceByCustomer)) and (!product.loginRequire or userSession != null)}">
		    <div class="prices">
		      <span class="price_title">Price</span>
			  <c:forEach items="${product.price}" var="price" varStatus="statusPrice">
			    <span class="price_info">
				  <c:if test="${price.qtyFrom != null}">
				  <span class="qtyBreak"> 
				    <c:choose>
				      <c:when test="${price.qtyTo != null}">
				        <c:out value="${price.qtyFrom}" /> -  <c:out value="${price.qtyTo}" />
				      </c:when>
				      <c:otherwise>
				        <c:out value="${price.qtyFrom}" /> +
				      </c:otherwise>
				    </c:choose>
				  </span>
				  </c:if>
				  <span class="price">
				    <fmt:message key="${siteConfig['CURRENCY'].value}" /><fmt:formatNumber value="${price.amt}" pattern="#,##0.00#" maxFractionDigits="${siteConfig['MAX_FRACTION_DIGIT'].value}" />
			      </span>
				  <span class="caseContent">
				  <c:choose>
			        <c:when test="${product.caseContent != null}">(<c:out value="${product.caseContent}" />     
			        <c:choose>
  						<c:when test="${product.caseUnitTitle != null}"><c:out value="${product.caseUnitTitle}" /></c:when> 
  						<c:otherwise><c:out value="${siteConfig['CASE_UNIT_TITLE'].value}" /></c:otherwise>
  					</c:choose>           
			        /<c:out value="${product.packing}" />)</c:when>
			        <c:otherwise><c:out value="${product.packing}" /></c:otherwise>
			      </c:choose>
				  </span>
				</span>
			  </c:forEach>
		    </div>
		    <form id="addToCartForm" action="${_contextpath}/addToCart.jhtm" method="post" class="clearfix">
		      <input name="product.id" type="hidden" value="${product.id}">
		        <c:if test="${(!gSiteConfig['gINVENTORY']) or (gSiteConfig['gINVENTORY'] and product.inventory == null) or (gSiteConfig['gINVENTORY'] and product.inventory != null and !product.negInventory and product.inventory > 0)}" >
	              <div class="quantity_wrapper">
			        <span class="quantity_title">Quantity</span>
					<input class="quantity_input" size="5" maxlength="5" name="quantity_${product.id}" value="${product.minimumQty}" type="text">
				  </div>
				  <div class="addToCart_btn_wrapper">
					<button type="submit" class="btn addToCart_btn">
					  Add To Cart &nbsp;<span class="addToCart_icon"></span>
					</button>
				  </div>
		  		</c:if>
		  		<div class="addToList_btn_wrapper">
			      <a class="btn addToList_btn" href="${_contextpath}/addToList.jhtm?product.id=${product.id}">
					Add To List &nbsp;<span class="addToList_icon"></span>
				  </a>
		  		</div>
			    <c:if test="${siteConfig['PRODUCT_QUOTE'].value == 'true' and product.quote}">
			    <div class="addToQuote_btn_wrapper" id="addToQuoteListWrapperId${product.id}">
				  <a class="btn addToQuote_btn" href="#" onClick="addToQuoteList('${product.id}');" >
					Add To Quote &nbsp;<span class="addToQuote_icon"></span>
				  </a>
				</div>	
			    </c:if>
			  
			    <div class="addToCompare_btn_wrapper" id="addToCompareWrapperId${product.id}">
				  <a class="btn addToCompare_btn" href="#" onClick="addToCompareList('${product.id}');" >
				    Compare &nbsp;<span class="addToCompare_icon"></span>
				  </a>
			    </div>
			  </form>
        	</c:when>
		    <c:when test="${siteConfig['PRODUCT_QUOTE'].value == 'true' and product.quote}">
		      
		      <form id="addToQuoteForm" action="/addToQuote.jhtm" method="post" class="clearfix">
		      <div class="addToQuote_btn_wrapper" id="addToQuoteListWrapperId${product.id}">
				<a class="btn addToQuote_btn" href="#" onClick="addToQuoteList('${product.id}');" >
				  Add To Quote &nbsp;<span class="addToQuote_icon"></span>
				</a>
			  </div>	
		      <div class="addToList_btn_wrapper">
			    <a class="btn addToList_btn" href="${_contextpath}/addToList.jhtm?product.id=${product.id}">
				  Add To List &nbsp;<span class="addToList_icon"></span>
				</a>
		  	  </div>
			  
			  <div class="addToCompare_btn_wrapper" id="addToCompareWrapperId${product.id}">
				<a class="btn addToCompare_btn" onClick="addToCompareList('${product.id}');"  href="#">
				  Compare &nbsp;<span class="addToCompare_icon"></span>
				</a>
			  </div>
			  </form>
		    </c:when>
		  </c:choose>
		
		<div class="row">
		  <c:if test="${gSiteConfig['gPRODUCT_REVIEW'] and product.enableRate}">
			<div class="col-sm-4">
			  <%@ include file="/WEB-INF/jsp/frontend/layout/template6/productReview.jsp" %>
		  	</div>
		  </c:if>
		  <div class="col-sm-4">
			<div class="product_availability">
			  <div class="availability_title">Availability</div>
			  <c:set var="stockStatus" value="inStock" />
			  <c:choose>
			    <c:when test="${gSiteConfig['gINVENTORY'] and product.inventory != null and !product.negInventory and product.inventory <= 0}">
			      <c:set var="stockStatus" value="outOfStock" />
			    </c:when>   
			    <c:when test="${gSiteConfig['gINVENTORY'] and product.inventory != null and product.inventory < product.lowInventory}">
			      <c:set var="stockStatus" value="lowStock" />
			    </c:when>
			    <c:otherwise>
			      <c:set var="stockStatus" value="inStock" />
			    </c:otherwise>
		      </c:choose>
			  
			  <div class="availability_status ${stockStatus}">
			    <c:choose>
			      <c:when test="${gSiteConfig['gINVENTORY'] and product.inventory != null and !product.negInventory and product.inventory <= 0}">
			        <c:out value="${siteConfig['OUT_OF_STOCK_TITLE'].value}" escapeXml="false"/> 
			      </c:when>   
			      <c:when test="${gSiteConfig['gINVENTORY'] and product.inventory != null and product.inventory < product.lowInventory}">
			       	<c:out value="${siteConfig['LOW_STOCK_TITLE'].value}" escapeXml="false"/>
			      </c:when>
			      <c:otherwise>
			        <c:out value="In Stock" escapeXml="false"/>
			      </c:otherwise>
				</c:choose>
			  </div>
			</div>
		  </div>
		  <c:forEach items="${product.productFieldsUnlimitedNameValue}" var="productFielUnlimitedNameValue" varStatus="status"> 
	        <c:if test="${productFielUnlimitedNameValue.displayName == 'Product Model' and productFielUnlimitedNameValue.displayValue != null}">
		      <div class="col-sm-4">
				<div class="product_model">
				  <div class="model_title"><c:out value="${productFielUnlimitedNameValue.displayName}" escapeXml="false" /></div>
			      <div class="model_content"><c:out value="${productFielUnlimitedNameValue.displayValue}" escapeXml="false"/></div>
		       	</div>
		  	  </div>
		    </c:if>
		  </c:forEach>
		</div>
	  </div>
	</div>
<div class="col-sm-12">
 <%@ include file="/WEB-INF/jsp/frontend/layout/template6/productTabs.jsp" %>
</div>										

</div>
</div>
</c:if>

<c:if test="${fn:trim(model.productLayout.footerHtml) != ''}">
  <c:set value="${model.productLayout.footerHtml}" var="footerHtml"/>
  <%@ include file="/WEB-INF/jsp/frontend/common/footerDynamicElement.jsp" %>
  <div style="float:left;width:100%"><c:out value="${footerHtml}" escapeXml="false"/></div>
</c:if>

 
<c:if test="${siteConfig['PRODUCT_QUOTE'].value == 'true' and product.quote}">  
<script type="text/javascript">
<!--
function addToQuoteList(productId){
	$.ajax({
		url: "${_contextpath}/quoteList.jhtm?productId="+productId,
		context: document.body
	}).done(function() {
		$("#addToQuoteListWrapperId"+productId).html('<a href=\"${_contextpath}/quoteViewList.jhtm\" class=\"btn addToQuote_btn\">View Quote List</a>');
	});
}

function addToCompareList(productId){
	$.ajax({
		url: "${_contextpath}/ajaxAddToCompare.jhtm?&productId="+productId,
		context: document.body
	}).done(function() {
		$("#addToCompareWrapperId"+productId).html('<a href=\"${_contextpath}/comparison.jhtm\" class=\"btn addToCompare_btn\">View Comparison</a>');
	});
}
//-->
</script>
</c:if>    
  </tiles:putAttribute>
</tiles:insertDefinition>