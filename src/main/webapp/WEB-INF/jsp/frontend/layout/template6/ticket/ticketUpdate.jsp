<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="/WEB-INF/tlds/custom.tld" prefix="wj" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>

<tiles:insertDefinition name="${_template}" flush="true">
  <c:if test="${_leftBar != '1' and _leftBar != '4'}">
    <tiles:putAttribute name="leftbar" value="/WEB-INF/jsp/frontend/layout/template${_leftBar}/leftbar.jsp" />
  </c:if>
  <tiles:putAttribute name="content" type="string">
  <%-- breadcrumb --%>
  
  
  <div class="col-breadcrumb">
	<ul class="breadcrumb">
		<li><a href="${_contextpath}/account.jhtm">My Account</a></li>
		<li class="active">Ticket Update</li>
	</ul>
</div>


<div class="col-sm-12">
	
	<c:out value="${model.updateTicketLayout.headerHtml}" escapeXml="false"/>  
	
	<c:if test="${model.message != null}">
	  <div class="message"><fmt:message key="${model.message}" /></div>
	</c:if>
	
	
	<div id="ticketCommentsWrapper">
		<div class="row">
			<div class="col-sm-12">
				<div class="form-group">
					<h3 class="ticketCommentsTitle">
						Tickets / <span><c:out value="${ticketForm.ticket.subject}" escapeXml="false"/> (<c:out value="${ticketForm.ticket.ticketId}" />)</span>
					</h3>
				</div>
				<div class="ticketComments">
					
					<c:forEach items="${model.questionAnswerList}" var="ticket" varStatus="status">
					 <c:set var="ticketClass" value="ticketUser" />
					 <c:set var="createdBy" value="${model.firstName}" />
					 <c:if test="${ticket.ticket.createdBy != NULL}">
					 	<c:set var="ticketClass" value="ticketAdmin" />
					 	<c:set var="createdBy" value="${siteConfig['COMPANY_NAME'].value}" />
					 </c:if>    
					 
					 <c:if test="${status.first}">
					    <div class="${ticketClass}">
							<div class="row">
								<div class="col-sm-4 col-md-3 col-lg-2">
									<div class="commentHead">
										<div class="commentName"><c:out value="${createdBy}"/> Said:</div>
										<div class="commentTime"><fmt:formatDate type="date" dateStyle="full" value="${ticket.ticket.created}" pattern="MM/dd/yyyy (hh:mm)a zz "/></div>
									</div>
								</div>
								<div class="col-sm-8 col-md-9 col-lg-10">
									<div class="commentContent">
										<c:out value="${wj:newLineToBreakLine(ticket.ticket.question)}" escapeXml="false"/>
									</div>
									<c:if test="${ticket.ticketVersion.attachment != null}">
								     <div class="attachmentLink">
								       <a href="${_contextpath}/temp/Ticket/customer_${ticket.ticket.userId}/${ticket.ticketVersion.attachment}"> <c:out value="${ticket.ticketVersion.attachment}"></c:out> </a>
							         </div>
								   </c:if>
					    		</div>
							</div>
						</div>
					 </c:if>
					 <c:if test="${!empty ticket.ticketVersion.questionDescription}">
					    <div class="${ticketClass}">
							<div class="row">
								<div class="col-sm-4 col-md-3 col-lg-2">
									<div class="commentHead">
										<div class="commentName"><c:out value="${createdBy}"/> Said:</div>
										<div class="commentTime"><fmt:formatDate type="date" dateStyle="full" value="${ticket.ticket.created}" pattern="MM/dd/yyyy (hh:mm)a zz "/></div>
									</div>
								</div>
								<div class="col-sm-8 col-md-9 col-lg-10">
									<div class="commentContent">
										<c:out value="${wj:newLineToBreakLine(ticket.ticketVersion.questionDescription)}" escapeXml="false"/>
									</div>
									<c:if test="${ticket.ticketVersion.attachment != null}">
								     <div class="attachmentLink">
								       <a href="${_contextpath}/temp/Ticket/customer_${ticket.ticket.userId}/${ticket.ticketVersion.attachment}"> <c:out value="${ticket.ticketVersion.attachment}"></c:out> </a>
							         </div>
								   </c:if>
					    		</div>
							</div>
						</div>
					 </c:if>
					</c:forEach>
				</div>
			</div>
		</div>
	</div>

	<c:if test="${!empty model.questionAnswerList and ticketForm.ticket.status == 'open'}">
	<form:form  id="updateTicketForm" role="form" action="/account_ticketUpdate.jhtm" commandName="ticketForm" method="post" enctype="multipart/form-data">
	<input type="hidden" name="id" value="<c:out value='${ticketForm.ticket.ticketId}'/>" />
		<div id="newTicket">
			<div class="row">
				<div class="col-sm-6">
					<div class="form-group">
						<h3><fmt:message key="updateTicket" /></h3>
					</div>
					<div class="form-group">
						<label for="ticketVersion_questionDescription"
							class="control-label"> <fmt:message key="comment" /> <sup
							class="requiredField">*</sup>
						</label>
					    <form:textarea id="ticketVersion_questionDescription" class="form-control" path="ticketVersion.questionDescription" htmlEscape="false" />
	  					<form:errors path="ticketVersion.questionDescription" cssClass="error"/>
						
						<c:if test="${siteConfig['TICKET_ATTACHMENT'].value == 'true'}">
						<div class="ticketAttachmentWrapper">
						  <span><fmt:message key="attachment" />:</span>
					      <input value="browse" type="file" name="attachment"/>
						</div>
						</c:if>
	
					
					</div>
				</div>
				<div class="col-sm-6"></div>
			</div>
		</div>
		<div id="form_buttons">
			<div class="row">
				<div class="col-sm-12">
					<div id="buttons_wrapper">
						<button type="submit" name="__update_ticket" class="btn btn-default">Update
							Ticket</button>
						<button type="submit" name="__close_ticket"	class="btn btn-default">Close
							Ticket</button>
					</div>
				</div>
			</div>
		</div>
	</form:form>
	</c:if>

	<c:out value="${model.updateTicketLayout.footerHtml}" escapeXml="false"/>  
	
</div>

  </tiles:putAttribute>
</tiles:insertDefinition>