<%@ page session="false" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ taglib uri="/WEB-INF/tlds/custom.tld" prefix="wj" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<style>
.product_wrapper {
    height: 370px !important;
    margin-bottom: 0;
}	
.col-sm-12 col-md-6 col-lg-4 {
    height: 380px !important;
    margin-bottom: 0;
}
</style>
	<c:if test="${product.productLayout=='047'}"> 
			<div class="col-sm-12 col-md-6 col-lg-4">
				<div class="product_wrapper">
				
				  <c:if test="${product.field28 != null and product.field28 !=''}">
				  	<c:set var="productLink" value="${product.field28}"/>
				  </c:if>

				  <c:choose>
					<c:when test="${product.field32 != '' and product.field32 != null}">
					<div class="product_bordered_box is_ribboned">
						<div class="ribbon_wrapper">
							<c:choose>
							<c:when test="${product.field32 == 'Back_in_Stock'}">
								<div class="ribbon ribbon_3">BACK IN STOCK</div>
							</c:when>
							<c:when test="${product.field32 == 'Out_of_Stock'}">
								<div class="ribbon ribbon_4">OUT OF STOCK</div>
							</c:when>
							<c:when test="${product.field32 == 'Load_Added'}">
								<div class="ribbon ribbon_5">NEW LOAD ADDED</div>
							</c:when>
							<c:when test="${product.field32 == 'New_Item'}">
								<div class="ribbon ribbon_6">NEW ARRIVAL</div>
							</c:when>
							<c:otherwise>
									<div class="ribbon ribbon_3">BACK IN STOCK</div>
							</c:otherwise>
							</c:choose>
						</div>
						<div class="product_sku_wrapper">
							<div class="product_sku">
								${product.sku}
							</div>
						</div>
						<div class="product_image_wrapper">
							<div class="product_image">
								<a href="${productLink}">
									<img src="<c:if test="${!product.thumbnail.absolute}">${_contextpath}/assets/Image/Product/thumb/</c:if><c:out value="${product.thumbnail.imageUrl}"/>" border="0" class="img-responsive center-block" alt="<c:out value="${product.name}"/><c:if test="${product.shortDesc != ''}"> - <c:out value="${product.shortDesc}"/></c:if>">
								</a>
							</div>
						</div>
					</div>
					</c:when>
					<c:otherwise>
						<div class="product_bordered_box">
						<div class="product_sku_wrapper">
							<div class="product_sku">
								${product.sku}
							</div>
						</div>
						<div class="product_image_wrapper">
							<div class="product_image">
								<a href="${productLink}">
									<img src="<c:if test="${!product.thumbnail.absolute}">${_contextpath}/assets/Image/Product/thumb/</c:if><c:out value="${product.thumbnail.imageUrl}"/>" border="0" class="img-responsive center-block" alt="<c:out value="${product.name}"/><c:if test="${product.shortDesc != ''}"> - <c:out value="${product.shortDesc}"/></c:if>">
								</a>
							</div>
						</div>
						</div>
					</c:otherwise>
					</c:choose>

					<div class="product_name_wrapper">
					<div class="product_name">
							<a href="${productLink}">${product.name}</a>
					</div>
					
					<div class="product_condition">
						<c:out value="${wj:removeChar(product.field24, ',' , true, true) }"/>
					</div>
					</div>
					
					<div class="clearfix">
						<div class="pull-left">
							<div class="nb_units">
								${product.field29}
							</div>
						</div>
						<div class="pull-right">
							<div class="unit_price">
								${product.field8}
							</div>
						</div>
					</div>

					<div class="total_price" style="margin-bottom:35px !important; weight:5px !important">
					<a href="${productLink}">
						<fmt:message key="${siteConfig['CURRENCY'].value}"/><fmt:formatNumber value="${wj:removeChar(product.price1, ',' , true, true) }" pattern="#,###.00"/>
						<c:if test="${product.field6 != null and product.field6 !=''}">
						&nbsp;/${product.field6}
						</c:if>
					</a>
					</div>
					
					<c:set var="link" value=""/>
					<c:choose>
					<c:when test="${!model.luceneSearch}">
						<c:set var="link" value="/addToCart.jhtm?addToCartCategory=true&cid=${model.thisCategory.id}&product.id=${product.id}&quantity_${product.id}=1"/>
					</c:when>
					<c:otherwise>
						<c:set var="link" value="/addToCart.jhtm?addToCartLucene=true&searchKeyword=${model.searchKeyword}&cid=${model.thisCategory.id}&product.id=${product.id}&quantity_${product.id}=1"/>
					</c:otherwise>
					</c:choose>
					<div class="add-to-cart">
					<a href="${_contextpath}${link}">
						<fmt:message key="addToCart"/>
					</a>
					</div>	
					
				</div>
			</div>
		</c:if>

	<c:if test="${product.productLayout=='048'}"> 
			<div class="col-sm-12 col-md-6 col-lg-4">
			<div class="product_wrapper">
			
			    <c:if test="${product.field28 != null and product.field28 !=''}">
				  <c:set var="productLink" value="${product.field28}"/>
				</c:if>
				
				<c:choose>
					<c:when test="${product.field32 != '' and product.field32 != null}">
					<div class="product_bordered_box is_ribboned">
						<div class="ribbon_wrapper">
							<c:choose>
							<c:when test="${product.field32 == 'Back_in_Stock'}">
								<div class="ribbon ribbon_3">BACK IN STOCK</div>
							</c:when>
							<c:when test="${product.field32 == 'Out_of_Stock'}">
								<div class="ribbon ribbon_4">OUT OF STOCK</div>
							</c:when>
							<c:when test="${product.field32 == 'Load_Added'}">
								<div class="ribbon ribbon_5">NEW LOAD ADDED</div>
							</c:when>
							<c:when test="${product.field32 == 'New_Item'}">
								<div class="ribbon ribbon_6">NEW ARRIVAL</div>
							</c:when>
							<c:otherwise>
									<div class="ribbon ribbon_3">BACK IN STOCK</div>
							</c:otherwise>
							</c:choose>
						</div>
						<div class="product_sku_wrapper">
							<div class="product_sku">
								${product.sku}
							</div>
						</div>
						<div class="product_image_wrapper">
							<div class="product_image">
								<a href="${productLink}">
									<img src="<c:if test="${!product.thumbnail.absolute}">${_contextpath}/assets/Image/Product/thumb/</c:if><c:out value="${product.thumbnail.imageUrl}"/>" border="0" class="img-responsive center-block" alt="<c:out value="${product.name}"/><c:if test="${product.shortDesc != ''}"> - <c:out value="${product.shortDesc}"/></c:if>">
								</a>
							</div>
						</div>
					</div>
					</c:when>
					<c:otherwise>
						<div class="product_bordered_box">
						<div class="product_sku_wrapper">
							<div class="product_sku">
								${product.sku}
							</div>
						</div>
						<div class="product_image_wrapper">
							<div class="product_image">
								<a href="${productLink}">
									<img src="<c:if test="${!product.thumbnail.absolute}">${_contextpath}/assets/Image/Product/thumb/</c:if><c:out value="${product.thumbnail.imageUrl}"/>" border="0" class="img-responsive center-block" alt="<c:out value="${product.name}"/><c:if test="${product.shortDesc != ''}"> - <c:out value="${product.shortDesc}"/></c:if>">
								</a>
							</div>
						</div>
						</div>
					</c:otherwise>
				  </c:choose>
				  
				<div class="product_name_wrapper">
				<div class="product_name">
						<a href="${productLink}">${product.name}</a>
				</div>
				
				<div class="product_condition">
					<c:out value="${wj:removeChar(product.field24, ',' , true, true) }"/>
				</div>
				</div>
				
				<div class="clearfix">
					<div class="pull-left">
						<div class="nb_units">
							${product.field29}
						</div>
					</div>
					<div class="pull-right">
						<div class="unit_price">
							${product.field8}
						</div>
					</div>
				</div>
				
				<div class="total_price" style="margin-bottom:35px !important; weight:5px !important">
				<a href="${productLink}">
					<fmt:message key="${siteConfig['CURRENCY'].value}"/><fmt:formatNumber value="${wj:removeChar(product.priceRangeMinimum, ',' , true, true) }" pattern="#,###.00"/>
							-
					<fmt:message key="${siteConfig['CURRENCY'].value}"/><fmt:formatNumber value="${wj:removeChar(product.priceRangeMaximum, ',' , true, true) }" pattern="#,###.00"/>
					<c:if test="${product.field6 != null and product.field6 !=''}">
						&nbsp;/${product.field6}
					</c:if>
				</a>
				</div>
				
					<c:set var="link" value=""/>
					<c:choose>
					<c:when test="${!model.luceneSearch}">
						<c:set var="link" value="/addToCart.jhtm?addToCartCategory=true&cid=${model.thisCategory.id}&product.id=${product.id}&quantity_${product.id}=1"/>
					</c:when>
					<c:otherwise>
						<c:set var="link" value="/addToCart.jhtm?addToCartLucene=true&searchKeyword=${model.searchKeyword}&cid=${model.thisCategory.id}&product.id=${product.id}&quantity_${product.id}=1"/>
					</c:otherwise>
					</c:choose>
					<div class="add-to-cart">
					<a href="${_contextpath}${link}">
						<fmt:message key="addToCart"/>
					</a>
					</div>
				
			</div>
		</div>
	  </c:if>

	<c:if test="${product.productLayout=='049'}"> 
			<div class="col-sm-12 col-md-6 col-lg-4">
			<div class="product_wrapper">
			
			    <c:if test="${product.field28 != null and product.field28 !=''}">
					<c:set var="productLink" value="${product.field28}"/>
				</c:if>
				
				<c:choose>
					<c:when test="${product.field32 != '' and product.field32 != null}">
					<div class="product_bordered_box is_ribboned">
						<div class="ribbon_wrapper">
							<c:choose>
							<c:when test="${product.field32 == 'Back_in_Stock'}">
								<div class="ribbon ribbon_3">BACK IN STOCK</div>
							</c:when>
							<c:when test="${product.field32 == 'Out_of_Stock'}">
								<div class="ribbon ribbon_4">OUT OF STOCK</div>
							</c:when>
							<c:when test="${product.field32 == 'Load_Added'}">
								<div class="ribbon ribbon_5">NEW LOAD ADDED</div>
							</c:when>
							<c:when test="${product.field32 == 'New_Item'}">
								<div class="ribbon ribbon_6">NEW ARRIVAL</div>
							</c:when>
							<c:otherwise>
									<div class="ribbon ribbon_3">BACK IN STOCK</div>
							</c:otherwise>
							</c:choose>
						</div>
						<div class="product_sku_wrapper">
							<div class="product_sku">
								${product.sku}
							</div>
						</div>
						<div class="product_image_wrapper">
							<div class="product_image">
								<a href="${productLink}">
									<img src="<c:if test="${!product.thumbnail.absolute}">${_contextpath}/assets/Image/Product/thumb/</c:if><c:out value="${product.thumbnail.imageUrl}"/>" border="0" class="img-responsive center-block" alt="<c:out value="${product.name}"/><c:if test="${product.shortDesc != ''}"> - <c:out value="${product.shortDesc}"/></c:if>">
								</a>
							</div>
						</div>
					</div>
					</c:when>
					<c:otherwise>
						<div class="product_bordered_box">
						<div class="product_sku_wrapper">
							<div class="product_sku">
								${product.sku}
							</div>
						</div>
						<div class="product_image_wrapper">
							<div class="product_image">
								<a href="${productLink}">
									<img src="<c:if test="${!product.thumbnail.absolute}">${_contextpath}/assets/Image/Product/thumb/</c:if><c:out value="${product.thumbnail.imageUrl}"/>" border="0" class="img-responsive center-block" alt="<c:out value="${product.name}"/><c:if test="${product.shortDesc != ''}"> - <c:out value="${product.shortDesc}"/></c:if>">
								</a>
							</div>
						</div>
						</div>
					</c:otherwise>
				  </c:choose>
				  
				<div class="product_name_wrapper">
				<div class="product_name">
						<a href="${productLink}">${product.name}</a>
				</div>
				
				<div class="product_condition">
					<c:out value="${wj:removeChar(product.field24, ',' , true, true) }"/>
				</div>
				</div>
				
				<div class="product_manifest_status">
				
				<c:choose>
					<c:when test="${wj:removeChar(product.field14, ',' , true, true) == 'Manifested'}">
						<fmt:message key="manifested"/>
					</c:when>
					<c:when test="${wj:removeChar(product.field14, ',' , true, true) == 'Unmanifested'}">
						<fmt:message key="unManifested"/>
					</c:when>
					<c:when test="${wj:removeChar(product.field14, ',' , true, true) == 'Partially Manifested'}">
						<fmt:message key="partiallyManifested"/>
					</c:when>
					<c:otherwise>
						<fmt:message key="manifested"/>
					</c:otherwise>														
				</c:choose>	
				
				</div>
				
				<div class="price_percentage" style="margin-bottom:35px !important; weight:5px !important">
				<a href="${productLink}">
					${product.field11}
				</a>
				</div>
				
					<c:set var="link" value=""/>
					<c:choose>
					<c:when test="${!model.luceneSearch}">
						<c:set var="link" value="/addToCart.jhtm?addToCartCategory=true&cid=${model.thisCategory.id}&product.id=${product.id}&quantity_${product.id}=1"/>
					</c:when>
					<c:otherwise>
						<c:set var="link" value="/addToCart.jhtm?addToCartLucene=true&searchKeyword=${model.searchKeyword}&cid=${model.thisCategory.id}&product.id=${product.id}&quantity_${product.id}=1"/>
					</c:otherwise>
					</c:choose>
					<div class="add-to-cart">
					<a href="${_contextpath}${link}">
						<fmt:message key="addToCart"/>
					</a>
					</div>	
				
			</div>
		</div>									
		</c:if>

	<c:if test="${product.productLayout=='050'}"> 
			<div class="col-sm-12 col-md-6 col-lg-4">
			<div class="product_wrapper">
				
				<c:if test="${product.field28 != null and product.field28 !=''}">
					<c:set var="productLink" value="${product.field28}"/>
				</c:if>
				
				<div class="product_bordered_box is_ribboned">
					<div class="ribbon_wrapper">
						<div class="ribbon ribbon_1">ON SALE</div>
					</div>
					<div class="product_sku_wrapper">
						<div class="product_sku">
							${product.sku}
						</div>
					</div>
					<div class="product_image_wrapper">
						<div class="product_image">
							<a href="${productLink}">
								<img src="<c:if test="${!product.thumbnail.absolute}">${_contextpath}/assets/Image/Product/thumb/</c:if><c:out value="${product.thumbnail.imageUrl}"/>" border="0" class="img-responsive center-block" alt="<c:out value="${product.name}"/><c:if test="${product.shortDesc != ''}"> - <c:out value="${product.shortDesc}"/></c:if>">
							</a>
						</div>
					</div>
				</div>
				
				<div class="product_name_wrapper">
				<div class="product_name">
						<a href="${productLink}">${product.name}</a>
				</div>
				
				<div class="product_condition">
					<c:out value="${wj:removeChar(product.field24, ',' , true, true) }"/>
				</div>
				</div>
				
				<div class="clearfix">
					<div class="pull-left">
						<div class="nb_units">
							${product.field29}
						</div>
					</div>
					<div class="pull-right">
						<div class="nb_units">
							${product.field8}
						</div>
					</div>
				</div>
				
				<div class="total_price discount" style="margin-bottom:35px !important; weight:5px !important">
				<a href="${productLink}">
					<span class="strikethrough">
						<span class="old_price"><fmt:message key="${siteConfig['CURRENCY'].value}"/><fmt:formatNumber value="${wj:removeChar(product.price1, ',' , true, true) }" pattern="#,###.00"/></span>
					</span>
					<span class="new_price"><fmt:message key="${siteConfig['CURRENCY'].value}"/><fmt:formatNumber value="${product.totalPriceDiscount}" pattern="#,###.00"/></span>
					<c:if test="${product.field6 != null and product.field6 !=''}">
						&nbsp;/${product.field6}
					</c:if>
				</a>
				</div>
				
					<c:set var="link" value=""/>
					<c:choose>
					<c:when test="${!model.luceneSearch}">
						<c:set var="link" value="/addToCart.jhtm?addToCartCategory=true&cid=${model.thisCategory.id}&product.id=${product.id}&quantity_${product.id}=1"/>
					</c:when>
					<c:otherwise>
						<c:set var="link" value="/addToCart.jhtm?addToCartLucene=true&searchKeyword=${model.searchKeyword}&cid=${model.thisCategory.id}&product.id=${product.id}&quantity_${product.id}=1"/>
					</c:otherwise>
					</c:choose>
					<div class="add-to-cart">
					<a href="${_contextpath}${link}">
						<fmt:message key="addToCart"/>
					</a>
					</div>	
				
			</div>
		</div>
	</c:if>

	<c:if test="${product.productLayout=='051'}">
			<div class="col-sm-12 col-md-6 col-lg-4">
			<div class="product_wrapper">
			
				<c:if test="${product.field28 != null and product.field28 !=''}">
					<c:set var="productLink" value="${product.field28}"/>
				</c:if>
				
				<div class="product_bordered_box is_ribboned">
					<div class="ribbon_wrapper">
						<div class="ribbon ribbon_2">ONE TIME DEAL</div>
					</div>
					<div class="product_sku_wrapper">
						<div class="product_sku">
							${product.sku}
						</div>
					</div>
					<div class="product_image_wrapper">
						<div class="product_image">
							<a href="${productLink}">
								<img src="<c:if test="${!product.thumbnail.absolute}">${_contextpath}/assets/Image/Product/thumb/</c:if><c:out value="${product.thumbnail.imageUrl}"/>" border="0" class="img-responsive center-block" alt="<c:out value="${product.name}"/><c:if test="${product.shortDesc != ''}"> - <c:out value="${product.shortDesc}"/></c:if>">
							</a>
						</div>
					</div>
				</div>
				
				<div class="product_name_wrapper">
				<div class="product_name">
						<a href="${productLink}">${product.name}</a>
				</div>
				
				<div class="clearfix">
					<div class="pull-left">
						<div class="product_condition">
							<c:out value="${wj:removeChar(product.field24, ',' , true, true) }"/>
						</div>
					</div>
					<div class="pull-right">
						<div class="product_manifest_status">
							<c:choose>
							<c:when test="${wj:removeChar(product.field14, ',' , true, true) == 'Manifested'}">
								<fmt:message key="manifested"/>
							</c:when>
							<c:when test="${wj:removeChar(product.field14, ',' , true, true) == 'Unmanifested'}">
								<fmt:message key="unManifested"/>
							</c:when>
							<c:when test="${wj:removeChar(product.field14, ',' , true, true) == 'Partially Manifested'}">
								<fmt:message key="partiallyManifested"/>
							</c:when>
							<c:otherwise>
								<fmt:message key="manifested"/>
							</c:otherwise>														
						</c:choose>	
						</div>
					</div>
				</div>
				</div>
				
				<div class="clearfix">
					<div class="pull-left">
						<div class="nb_units">
							${product.field29}
						</div>
					</div>
					<div class="pull-right">
						<div class="unit_price">
							${product.field8}
						</div>
					</div>
				</div>

				<div class="total_price" style="margin-bottom:35px !important; weight:5px !important">
				<a href="${productLink}">
					<fmt:message key="${siteConfig['CURRENCY'].value}"/><fmt:formatNumber value="${wj:removeChar(product.price1, ',' , true, true) }" pattern="#,###.00"/>
					<c:if test="${product.field6 != null and product.field6 !=''}">
						&nbsp;/${product.field6}
					</c:if>
				</a>
				</div>
				
					<c:set var="link" value=""/>
					<c:choose>
					<c:when test="${!model.luceneSearch}">
						<c:set var="link" value="/addToCart.jhtm?addToCartCategory=true&cid=${model.thisCategory.id}&product.id=${product.id}&quantity_${product.id}=1"/>
					</c:when>
					<c:otherwise>
						<c:set var="link" value="/addToCart.jhtm?addToCartLucene=true&searchKeyword=${model.searchKeyword}&cid=${model.thisCategory.id}&product.id=${product.id}&quantity_${product.id}=1"/>
					</c:otherwise>
					</c:choose>
					<div class="add-to-cart">
					<a href="${_contextpath}${link}">
						<fmt:message key="addToCart"/>
					</a>
					</div>
				
			</div>
		</div>
	</c:if>
	
	
	<c:if test="${product.productLayout!='047' && product.productLayout!='048' && product.productLayout!='049' && product.productLayout!='050' && product.productLayout!='051'}"> 
			<div class="col-sm-12 col-md-6 col-lg-4">
				<div class="product_wrapper">
				
				 <c:if test="${product.field28 != null and product.field28 !=''}">
					<c:set var="productLink" value="${product.field28}"/>
				 </c:if>
					
				  <c:choose>
					<c:when test="${product.field32 != '' and product.field32 != null}">
					<div class="product_bordered_box is_ribboned">
						<div class="ribbon_wrapper">
							<c:choose>
							<c:when test="${product.field32 == 'Back_in_Stock'}">
								<div class="ribbon ribbon_3">BACK IN STOCK</div>
							</c:when>
							<c:when test="${product.field32 == 'Out_of_Stock'}">
								<div class="ribbon ribbon_4">OUT OF STOCK</div>
							</c:when>
							<c:when test="${product.field32 == 'Load_Added'}">
								<div class="ribbon ribbon_5">NEW LOAD ADDED</div>
							</c:when>
							<c:when test="${product.field32 == 'New_Item'}">
								<div class="ribbon ribbon_6">NEW ARRIVAL</div>
							</c:when>
							<c:otherwise>
									<div class="ribbon ribbon_3">BACK IN STOCK</div>
							</c:otherwise>
							</c:choose>
						</div>
						<div class="product_sku_wrapper">
							<div class="product_sku">
								${product.sku}
							</div>
						</div>
						<div class="product_image_wrapper">
							<div class="product_image">
								<a href="${productLink}">
									<img src="<c:if test="${!product.thumbnail.absolute}">${_contextpath}/assets/Image/Product/thumb/</c:if><c:out value="${product.thumbnail.imageUrl}"/>" border="0" class="img-responsive center-block" alt="<c:out value="${product.name}"/><c:if test="${product.shortDesc != ''}"> - <c:out value="${product.shortDesc}"/></c:if>">
								</a>
							</div>
						</div>
					</div>
					</c:when>
					<c:otherwise>
						<div class="product_bordered_box">
						<div class="product_sku_wrapper">
							<div class="product_sku">
								${product.sku}
							</div>
						</div>
						<div class="product_image_wrapper">
							<div class="product_image">
								<a href="${productLink}">
									<img src="<c:if test="${!product.thumbnail.absolute}">${_contextpath}/assets/Image/Product/thumb/</c:if><c:out value="${product.thumbnail.imageUrl}"/>" border="0" class="img-responsive center-block" alt="<c:out value="${product.name}"/><c:if test="${product.shortDesc != ''}"> - <c:out value="${product.shortDesc}"/></c:if>">
								</a>
							</div>
						</div>
						</div>
					</c:otherwise>
					</c:choose>
					
					<div class="product_name_wrapper">
					<div class="product_name">
							<a href="${productLink}">${product.name}</a>
					</div>
					
					<div class="product_condition">
						<c:out value="${wj:removeChar(product.field24, ',' , true, true) }"/>
					</div>
					</div>
					
					
					<div class="clearfix">
						<div class="pull-left">
							<div class="nb_units">
								${product.field29}
							</div>
						</div>
						<div class="pull-right">
							<div class="unit_price">
								${product.field8}
							</div>
						</div>
					</div>	
				
					<div class="total_price" style="margin-bottom:35px !important; weight:5px !important">
					<a href="${productLink}">
						<fmt:message key="${siteConfig['CURRENCY'].value}"/><fmt:formatNumber value="${wj:removeChar(product.price1, ',' , true, true) }" pattern="#,###.00"/>
						<c:if test="${product.field6 != null and product.field6 !=''}">
							&nbsp;/${product.field6}
						</c:if>
					</a>
					</div>
					
					<c:set var="link" value=""/>
					<c:choose>
					<c:when test="${!model.luceneSearch}">
						<c:set var="link" value="/addToCart.jhtm?addToCartCategory=true&cid=${model.thisCategory.id}&product.id=${product.id}&quantity_${product.id}=1"/>
					</c:when>
					<c:otherwise>
						<c:set var="link" value="/addToCart.jhtm?addToCartLucene=true&searchKeyword=${model.searchKeyword}&cid=${model.thisCategory.id}&product.id=${product.id}&quantity_${product.id}=1"/>
					</c:otherwise>
					</c:choose>
					<div class="add-to-cart">
					<a href="${_contextpath}${link}">
						<fmt:message key="addToCart"/>
					</a>
					</div>	
					
				</div>
			</div>
	</c:if>
