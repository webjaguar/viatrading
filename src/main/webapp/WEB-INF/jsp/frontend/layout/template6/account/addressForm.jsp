<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="/WEB-INF/tlds/spring.tld" prefix="spring" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib uri="/WEB-INF/tlds/custom.tld" prefix="wj" %>
<%@ page session="false" %>

<script type="text/javascript">
function validatePhone() {
	var country = $("#customer_address_country").val();
	if(country  == 'US' || country  == 'CA' ){
		$("#customer_address_phone").mask("(999)999-9999");	
	} else{
		$("#customer_address_phone").unmask("(999)999-9999");
	}
	
}

function validateCellPhone() {
	var country = $("#customer_address_country").val();
	if(country  == 'US' || country  == 'CA' ){
	  $("#customer_address_cellPhone").mask("(999)999-9999");	
	} else {
	  $("#customer_address_cellPhone").unmask("(999)999-9999");
	}
}
</script>

<tiles:insertDefinition name="${_template}" flush="true">
  <c:if test="${_leftBar != '1' and _leftBar != '4'}">
    <tiles:putAttribute name="leftbar" value="/WEB-INF/jsp/frontend/layout/template${_leftBar}/leftbar.jsp" />
  </c:if>
  <tiles:putAttribute name="content" type="string">

	<div class="col-breadcrumb">
		<ul class="breadcrumb">
			<li><a href="${_contextpath}/account.jhtm">My Account</a></li>
			<li class="active">Edit Address</li>
		</ul>
	</div>

<c:if test="${siteConfig['EDIT_ADDRESS_ON_FRONTEND'].value == 'true'}" >

<div class="col-sm-12">
	<div class="message"></div>
	<form:form  commandName="addressForm" method="post" id="accountAddressForm" class="form-horizontal" role="form">
		<div class="row">
			<div class="col-sm-12">
				<div class="requiredFieldLabel">* Required Field</div>
			</div>
		</div>
		<div id="accountAddress">
			<div class="row">
				<div class="col-sm-6">
					<div class="form-group">
						<div class="col-sm-12">
							<h3>
								<c:if test="${addressForm.newAddress}"><fmt:message key="addressNew" /></c:if>
								<c:if test="${!addressForm.newAddress}"><fmt:message key="addressEdit" /></c:if>
							</h3>
						</div>
					</div>
					<spring:bind path="address.firstName">
		  			<div class="form-group <c:if test="${status.error}">has-feedback has-error</c:if>">
						<div class="col-sm-4">
							<label for="customer_address_firstName" class="control-label">
								<fmt:message key="firstName" /><sup class="requiredField">*</sup>
							</label>
						</div>
						<div class="col-sm-8">
							<form:input class="form-control" id="customer_address_firstName"  path="address.firstName" htmlEscape="true" />
				  		  	<c:if test="${status.error}">
				  		  		<i class="form-control-feedback glyphicon glyphicon-remove"></i>
			            		<small class="help-block error-message">
			            			<form:errors path="address.firstName" />
			            		</small>    
				  		  	</c:if>
						</div>
					</div>
					</spring:bind>
					
					<spring:bind path="address.lastName">
		  			<div class="form-group <c:if test="${status.error}">has-feedback has-error</c:if>">
						<div class="col-sm-4">
							<label for="customer_address_lastName" class="control-label">
								<fmt:message key="lastName" /> <sup class="requiredField">*</sup>
							</label>
						</div>
						<div class="col-sm-8">
							<form:input class="form-control" id="customer_address_lastName"  path="address.lastName" htmlEscape="true" />
				  		  	<c:if test="${status.error}">
				  		  		<i class="form-control-feedback glyphicon glyphicon-remove"></i>
			            		<small class="help-block error-message">
			            			<form:errors path="address.lastName"  />
			            		</small>    
				  		  	</c:if>
						</div>
					</div>
					</spring:bind>
					
					
					<spring:bind path="address.country">
		  			<div class="form-group <c:if test="${status.error}">has-feedback has-error</c:if>">
						<div class="col-sm-4">
							<label for="customer_address_country" class="control-label">
								<fmt:message key="country" /> <sup class="requiredField">*</sup>
							</label>
						</div>
						<div class="col-sm-8">
							<form:select class="form-control" id="customer_address_country" path="address.country">
								<form:option value="" label="Please Select"/>
							    <form:options items="${model.countrylist}" itemValue="code" itemLabel="name"/>
							</form:select>
				  		  	<c:if test="${status.error}">
				  		  		<i class="form-control-feedback glyphicon glyphicon-remove"></i>
			            		<small class="help-block error-message">
			            			<form:errors path="address.country"  />
			            		</small>    
				  		  	</c:if>
						</div>
					</div>
					</spring:bind>
					
					<spring:bind path="address.company">
		  			<div class="form-group <c:if test="${status.error}">has-feedback has-error</c:if>">
						<div class="col-sm-4">
							<label for="customer_address_company" class="control-label">
								<fmt:message key="company" /> </label>
						</div>
						<div class="col-sm-8">
							<form:input class="form-control" id="customer_address_company"  path="address.company" htmlEscape="true" />
				  		  	<c:if test="${status.error}">
				  		  		<i class="form-control-feedback glyphicon glyphicon-remove"></i>
			            		<small class="help-block error-message">
			            			<form:errors path="address.company"  />
			            		</small>    
				  		  	</c:if>
						</div>
					</div>
					</spring:bind>
					
					<spring:bind path="address.addr1">
		  			<div class="form-group <c:if test="${status.error}">has-feedback has-error</c:if>">
						<div class="col-sm-4">
							<label for="customer_address_addr1" class="control-label">
								<fmt:message key="address" /> 1 <sup class="requiredField">*</sup>
							</label>
						</div>
						<div class="col-sm-8">
							<form:input class="form-control" id="customer_address_addr1"  path="address.addr1" htmlEscape="true" />
				  		  	<c:if test="${status.error}">
				  		  		<i class="form-control-feedback glyphicon glyphicon-remove"></i>
			            		<small class="help-block error-message">
			            			<form:errors path="address.addr1"  />
			            		</small>    
				  		  	</c:if>
						</div>
					</div>
					</spring:bind>
					
					<spring:bind path="address.addr2">
		  			<div class="form-group <c:if test="${status.error}">has-feedback has-error</c:if>">
						<div class="col-sm-4">
							<label for="customer_address_addr2" class="control-label">
								<fmt:message key="address" /> 2 </label>
						</div>
						<div class="col-sm-8">
							<form:input class="form-control" id="customer_address_addr2"  path="address.addr2" htmlEscape="true" />
				  		  	<c:if test="${status.error}">
				  		  		<i class="form-control-feedback glyphicon glyphicon-remove"></i>
			            		<small class="help-block error-message">
			            			<form:errors path="address.addr2"  />
			            		</small>    
				  		  	</c:if>
						</div>
					</div>
					</spring:bind>
					
					<spring:bind path="address.city">
		  			<div class="form-group <c:if test="${status.error}">has-feedback has-error</c:if>">
						<div class="col-sm-4">
							<label for="customer_address_city" class="control-label">
								<fmt:message key="city" /> <sup class="requiredField">*</sup>
							</label>
						</div>
						<div class="col-sm-8">
							<form:input class="form-control" id="customer_address_city"  path="address.city" htmlEscape="true" />
				  		  	<c:if test="${status.error}">
				  		  		<i class="form-control-feedback glyphicon glyphicon-remove"></i>
			            		<small class="help-block error-message">
			            			<form:errors path="address.city"  />
			            		</small>    
				  		  	</c:if>
						</div>
					</div>
					</spring:bind>
					
					<spring:bind path="address.stateProvince">
		  			<div class="form-group <c:if test="${status.error}">has-feedback has-error</c:if>">
						<div class="col-sm-4">
							<label for="" class="control-label"> <fmt:message key="stateProvince" /> <sup
								class="requiredField">*</sup>
							</label>
						</div>
						<div class="col-sm-8">
							<form:input type="text" class="form-control" id="customer_address_stateProvince" path="address.stateProvince" />
							<div id="stateProvinceNA" class="checkbox">
							<label> 
								<form:checkbox id="customer_address_stateProvinceNA" path="address.stateProvinceNA" />Not Applicable
							</label>
							</div>
							<form:select class="form-control" id="customer_address_state"  path="address.stateProvince">
							    <form:option value="" label="Please Select"/>
								<form:options items="${model.statelist}" itemValue="code" itemLabel="name"/>
							</form:select> 
							<form:select class="form-control" id="customer_address_ca_province"  path="address.stateProvince">
							   	<form:option value="" label="Please Select"/>
							    <form:options items="${model.caProvinceList}" itemValue="code" itemLabel="name"/>
							</form:select>
						</div>
					</div>
					</spring:bind>
					
					<spring:bind path="address.zip">
		  			<div class="form-group <c:if test="${status.error}">has-feedback has-error</c:if>">
						<div class="col-sm-4">
							<label for="customer_address_zip" class="control-label">
								<fmt:message key="zipCode" /> <sup class="requiredField">*</sup>
							</label>
						</div>
						<div class="col-sm-8">
							<form:input class="form-control" id="customer_address_zip"  path="address.zip" htmlEscape="true" />
				  		  	<c:if test="${status.error}">
				  		  		<i class="form-control-feedback glyphicon glyphicon-remove"></i>
			            		<small class="help-block error-message">
			            			<form:errors path="address.zip"  />
			            		</small>    
				  		  	</c:if>
						</div>
					</div>
					</spring:bind>
					
					<spring:bind path="address.phone">
		  			<div class="form-group <c:if test="${status.error}">has-feedback has-error</c:if>">
						<div class="col-sm-4">
							<label for="customer_address_phone" class="control-label">
								<fmt:message key="phone" /> <sup class="requiredField">*</sup>
							</label>
						</div>
						<div class="col-sm-8">
							<form:input class="form-control" id="customer_address_phone"  onkeyup="validatePhone();"  path="address.phone" htmlEscape="true" />
				  		  	<c:if test="${status.error}">
				  		  		<i class="form-control-feedback glyphicon glyphicon-remove"></i>
			            		<small class="help-block error-message">
			            			<form:errors path="address.phone"  />
			            		</small>    
				  		  	</c:if>
						</div>
					</div>
					</spring:bind>
					
					<spring:bind path="address.cellPhone">
		  			<div class="form-group <c:if test="${status.error}">has-feedback has-error</c:if>">
						<div class="col-sm-4">
							<label for="customer_address_cellPhone" class="control-label">
								<fmt:message key="cellPhone" /> </label>
						</div>
						<div class="col-sm-8">
							<form:input class="form-control" id="customer_address_cellPhone" onkeyup="validateCellPhone()" path="address.cellPhone" htmlEscape="true" />
				  		  	<c:if test="${status.error}">
				  		  		<i class="form-control-feedback glyphicon glyphicon-remove"></i>
			            		<small class="help-block error-message">
			            			<form:errors path="address.cellPhone"  />
			            		</small>    
				  		  	</c:if>
						</div>
					</div>
					</spring:bind>
					
					<c:if test="${siteConfig['ADDRESS_RESIDENTIAL_COMMERCIAL'].value >= 2}">
					<div class="form-group">
						<div class="col-sm-4">
							<label for="customer_address_residential" class="control-label">
								<fmt:message key="deliveryType" /> </label>
						</div>
						<div class="col-sm-8">
							<form:radiobutton path="address.residential" value="true"/>: <fmt:message key="residential" /><br />
							<form:radiobutton path="address.residential" value="false"/>: <fmt:message key="commercial" />
						    <form:errors path="address.residential"  />
						</div>
					</div>
					    <c:if test="${siteConfig['SHIPPING_LTL'].value == 'true'}">
						<div class="form-group">
							<div class="col-sm-4">
								<label for="customer_address_liftGate" class="control-label">
									<fmt:message key="liftGateDelivery" /> </label>
							</div>
							<div class="col-sm-8">
								<form:checkbox path="address.liftGate" value="true"/>
					            <form:errors path="address.liftGate"  />
							</div>
						</div>
					    </c:if>  
					</c:if>
					
					<spring:bind path="address.fax">
		  			<div class="form-group <c:if test="${status.error}">has-feedback has-error</c:if>">
						<div class="col-sm-4">
							<label for="customer_address_fax" class="control-label">
								<fmt:message key="fax" /> </label>
						</div>
						<div class="col-sm-8">
							<form:input class="form-control" id="customer_address_fax"  path="address.fax" htmlEscape="true" />
				  		  	<c:if test="${status.error}">
				  		  		<i class="form-control-feedback glyphicon glyphicon-remove"></i>
			            		<small class="help-block error-message">
			            			<form:errors path="address.fax"  />
			            		</small>    
				  		  	</c:if>
						</div>
					</div>
					</spring:bind>
					
					<c:if test="${not addressForm.address.primary}">
						<div class="form-group">
							<div class="col-sm-4">
								<label for="customer_address_setAsPrimaryAddress" class="control-label">
									<fmt:message key="setAsPrimaryAddress" /> </label>
							</div>
							<div class="col-sm-8">
								<form:checkbox path="setAsPrimary" value="true"/>
					        </div>
						</div>
					</c:if>
					
					
				</div>
				<div class="col-sm-6"></div>
			</div>
		</div>
		<div id="form_buttons">
			<div class="row">
				<div class="col-sm-12">
					<div id="buttons_wrapper">
						<c:if test="${addressForm.newAddress}">
			  				<button type="submit" value="<spring:message code="addressAdd"/>" class="btn btn-default">Add Address</button>
						</c:if>
						
						<c:if test="${!addressForm.newAddress}">
						  <button type="submit" value="<spring:message code="addressUpdate"/>" class="btn btn-default">Update Address</button>
						  <c:if test="${not addressForm.address.primary}">
						  	<button type="submit" value="<spring:message code="addressDelete"/>" name="_delete" onClick="return confirm('Delete permanently?')" class="btn btn-default">Delete Address</button>
						  </c:if>
						</c:if>
  						<button type="submit" value="<spring:message code="cancel"/>" name="_cancel" class="btn btn-default">Cancel</button>
					</div>
				</div>
			</div>
		</div>
	</form:form>

</div>


</c:if>
  </tiles:putAttribute>
</tiles:insertDefinition>