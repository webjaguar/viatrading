<%@ page session="false" %>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="/WEB-INF/tlds/spring.tld" prefix="spring" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="/WEB-INF/tlds/custom.tld" prefix="wj" %>

<tiles:insertDefinition name="${_template}" flush="true">
  <c:if test="${_leftBar != '1' and _leftBar != '4'}">
     <tiles:putAttribute name="leftbar" value="/WEB-INF/jsp/frontend/layout/template${_leftBar}/leftbar.jsp" />
  </c:if>
  <tiles:putAttribute name="content" type="string">
	<script type="text/JavaScript">
	<!--
	function checkCode(){
	  if ( document.getElementById('__claimCode').value == "" ) {
	  	alert("<fmt:message key='giftcard.empty' />");
	  	return false;
	  }
	}
	//-->
	</script>
	
	<div class="col-breadcrumb">
		<ul class="breadcrumb">
			<li><a href="#">My Account</a></li>
			<li class="active">Gift Card</li>
		</ul>
	</div>
	
	
	<div class="col-sm-12">
		<form id="giftCardBalanceForm" class="form-horizontal" role="form" method="POST">
			
			<c:if test="${model.message != null}">
			  <div class="message"><fmt:message key="${model.message}" /></div>
			</c:if>
			<div id="giftCardBalance">
				<div class="row">
					<div class="col-sm-6">
						<div class="form-group">
							<div class="col-sm-12">
								<h4><fmt:message key="viewYourBalanceOrRedeemANewGiftCertificate/Card" /></h4>
							</div>
						</div>
						<div class="form-group">
							<div class="col-sm-12">
								<b><fmt:message key="currentCredit"></fmt:message> <fmt:message key="${siteConfig['CURRENCY'].value}"/><c:out value="${model.credit}"/></b>
							</div>
						</div>
	
						<div class="form-group">
							<div class="col-sm-12">
								<h4>Gift Card Redeem</h4>
							</div>
						</div>
						<div class="form-group">
							<div class="col-sm-12">
								<b><fmt:message key="wantToRedeemANewGiftCertificate/card" /></b>
							</div>
						</div>
						<div class="form-group">
							<div class="col-sm-3">
								<label class="control-label">
									<fmt:message key="giftCard" /> <fmt:message key="code" />:
								</label>
							</div>
							<div class="col-sm-9">
								<input type="text" class="form-control" id="__claimCode" name="__claimCode">
							</div>
						</div>
						
						<c:if test="${model.giftCardSuccessful == true }">
							<small class="help-block">
					  		<fmt:message key="giftCardSuccessful">
							  	<c:choose>
							  		<c:when test="${siteConfig['CURRENCY'].value == 'EUR'}">
							  			<fmt:param value="&#8364;" /> 
							  		</c:when>
							  		<c:when test="${siteConfig['CURRENCY'].value == 'GBP'}">
							  			<fmt:param value="&#163;"/> 
							  		</c:when>
							  		<c:when test="${siteConfig['CURRENCY'].value == 'JPY'}">
							  			<fmt:param value="&#165;"/> 
							  		</c:when>
							  		<c:when test="${siteConfig['CURRENCY'].value == 'CAD'}">
							  			<fmt:param value="CAD&#36;"/> 
							  		</c:when>
							  		<c:otherwise>
							  		    <fmt:param value="&#36;" />
							  		</c:otherwise>
							  	</c:choose><fmt:param value="${model.credit}"/>
						  	</fmt:message>
							</small>
					  	</c:if>
						
						<div id="form_buttons">
							<div class="row">
								<div class="col-sm-3"></div>
								<div class="col-sm-9">
									<div id="buttons_wrapper">
										<button type="submit" class="btn btn-default">Redeem</button>
									</div>
								</div>
							</div>
						</div>
					</div>
					<div class="col-sm-6"></div>
				</div>
			</div>
		</form>
		
	</div>

  </tiles:putAttribute>
</tiles:insertDefinition>