<%@ page session="false" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ taglib uri="/WEB-INF/tlds/custom.tld" prefix="wj" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>




<div class="col-sm-12 col-md-6 col-lg-4">
  <div class="product_wrapper <c:if test="${product.salesTag != null}">is_ribboned</c:if>" >
	
	<c:set var="multiplier" value="1"/>
	<c:if test="${siteConfig['CASE_CONTENT_LAYOUT'].value == '1' and product.caseContent > 0}">
	  <c:set var="multiplier" value="${product.caseContent}"/>	
	</c:if>  	  
  	 
	<c:if test="${product.salesTag != null}">
  		<div class="ribbon_wrapper">
	  		<div class="ribbon ribbon_1"> <c:out value="${product.salesTag.title}" escapeXml="false" /> </div>
		</div>
  	</c:if>
	<div class="product_image_wrapper">
	  <a href="${productLink}">
		<img src="<c:if test="${!product.thumbnail.absolute}">${_contextpath}/assets/Image/Product/thumb/</c:if><c:out value="${product.thumbnail.imageUrl}"/>" border="0" class="img-responsive center-block product_image" alt="<c:out value="${product.name}"/><c:if test="${product.shortDesc != ''}"> - <c:out value="${product.shortDesc}"/></c:if>">
  	  </a>
	</div>
	<div class="product_name_wrapper">
	  <div class="product_name">
		<a href="${productLink}">
		  <c:out value="${product.name}" escapeXml="false"/>
		 </a>
	  </div>
	</div>
	<div class="product_sku_wrapper" style="height: 30px;">
	  <div class="product_name">
		<c:out value="${product.sku}" escapeXml="false"/>
	  </div>
	</div>
	
	<c:choose>
	  <c:when test="${product.salesTag != null}">
		
		<div class="price_discount">
		  <span class="price_title"><fmt:message key="f-price-range" />:</span>
		  <span class="strikethrough">
			<span class="old_price"><fmt:message key="${siteConfig['CURRENCY'].value}" /><fmt:formatNumber value="${product.originalPriceRangeMinimum*multiplier}" pattern="#,##0.00" /></span>
		  </span>
		  <span class="new_price"><fmt:message key="${siteConfig['CURRENCY'].value}" /><fmt:formatNumber value="${product.priceRangeMinimum*multiplier}" pattern="#,##0.00" /></span>
		</div>
		
		
	  </c:when>  
	  <c:otherwise>
		<div class="price_range">
		  <span class="price_title"><fmt:message key="f-price-range" />: </span>
		  <span class="p_from_id"><fmt:message key="${siteConfig['CURRENCY'].value}" /><fmt:formatNumber value="${product.priceRangeMinimum*multiplier}" pattern="#,##0.00" /><c:if test="${siteConfig['CASE_CONTENT_LAYOUT'].value == '1' and product.caseContent > 0 and product.packing != ''}"><c:out value="${product.packing}"/></c:if></span>
		  <c:if test="${product.priceRangeMaximum != null and product.priceRangeMaximum > product.priceRangeMinimum}">
			<span class="price_range_separator">-</span>
			<span class="p_to_id"><fmt:message key="${siteConfig['CURRENCY'].value}" /><fmt:formatNumber value="${product.priceRangeMaximum*multiplier}" pattern="#,##0.00" /></span>
		  </c:if>
		</div>
	  </c:otherwise>
	</c:choose>
	
	
	
	<form id="addToCartForm" action="${_contextpath}/addToCart.jhtm" method="post" class="addToCartForm clearfix">
	  <input name="product.id" type="hidden" value="${product.id}">
	  <div class="qty_wrapper">
		<span class="qty_title">Quantity</span>
		<c:choose>
		   <c:when test="${gSiteConfig['gINVENTORY'] and product.inventory != null and !product.negInventory and product.inventory <= 0}" >
		   	 <input name="quantity_${product.id}" disabled="disabled" size="5" maxlength="5" class="qty_input disabled" type="text" autocomplete="off" value="${product.minimumQty}">	
		   </c:when>
		   <c:otherwise>
			<input name="quantity_${product.id}" size="5" maxlength="5" class="qty_input" type="text" autocomplete="off" value="${product.minimumQty}">	
		   </c:otherwise>
		</c:choose>
	  </div>
	  <div class="btn-group">
		<c:choose>
		   <c:when test="${gSiteConfig['gINVENTORY'] and product.inventory != null and !product.negInventory and product.inventory <= 0}" >
		   		<span class="btn outOfStock_btn">Out of Stock</span>
		   </c:when>
		   <c:otherwise>
		   		<button type="submit" class="btn addToCart_btn">
					Add To Cart &nbsp;<span class="addToCart_icon"></span>
				</button>
		   </c:otherwise>
		</c:choose>
		
		
		<a href="/addToList.jhtm?product.id=${product.id}" class="btn addToList_btn" data-toggle="tooltip" title="Add To List">
		  <span class="addToList_icon"></span>
		</a>
		<%--
		<a href="#" class="btn compare_btn" data-toggle="tooltip" title="Compare">
		  <span class="compare_icon"></span>
		</a>
	     --%>
		
	  </div>
	  <c:choose>
        <c:when test="${model.comparisonMap[product.id] != null}">
          <div class="viewComparison_btn_wrapper" id="compare-product${product.id}">
		    <a class="viewComparison_btn" href="${_contextpath}/comparison.jhtm">View Comparison</a>
		  </div>
        </c:when>
        <c:otherwise>
          <div class="compare_checkbox_wrapper checkbox" id="compare-product${product.id}" >
    		<label>
		        <input type="checkbox" class="compare_checkbox" value="308" onchange="compareProduct('${product.id}');"> Compare
		    </label>
		  </div>
        </c:otherwise>
      </c:choose>
	</form>
  </div>
</div>
