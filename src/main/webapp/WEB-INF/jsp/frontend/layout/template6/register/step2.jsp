<%@ page session="false" %>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="/WEB-INF/tlds/spring.tld" prefix="spring" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ page contentType="text/html; charset=ISO-8859-1" session="false" %>

<script src="/dv/w3data.js"></script>
<script type='text/javascript' src='/wp-includes/js/jquery/jquery.js?ver=1.12.4'></script>
<script type='text/javascript' src='/wp-content/themes/bridge/js/plugins.js?ver=4.6.1'></script>
<script type='text/javascript' src='/wp-content/themes/bridge/js/default.min.js?ver=4.6.1'></script>

<tiles:insertDefinition name="${_template}" flush="true">
  <c:if test="${_leftBar != '1' and _leftBar != '4'}">
    <tiles:putAttribute name="leftbar" value="/WEB-INF/jsp/frontend/layout/template${_leftBar}/leftbar.jsp" />
  </c:if>
  <tiles:putAttribute name="content" type="string">

<form:form commandName="customerForm" method="post" enctype="multipart/form-data">
<input type="hidden" name="_page" value="1">

<c:if test="${gSiteConfig['gCUSTOMER_CATEGORY'] != null and false}">
<script type="text/javascript" src="javascript/mootools-1.2.5-core.js"></script>
<script language="JavaScript" type="text/JavaScript">
<!--
function updateCategory(id, selected) {
		var request = new Request.JSON({
			url: "json.jhtm?action=category&bid=<c:out value="${gSiteConfig['gCUSTOMER_CATEGORY']}"/>&cid=" + selected,
			onRequest: function() { 
				document.getElementById(id + "_spinner").style.display="block";
			},
			onComplete: function(jsonObj) {
				document.getElementById(id + "_spinner").style.display="none";
				var categoryIds = document.getElementById(id + "_categoryIds");
				if (jsonObj.subcats.length > 0) {
					while (categoryIds.length > 0) {
    					categoryIds.remove(0);
					} 		
					i = 0;
					jsonObj.subcats.each(function(category) {
						categoryIds.options[i++] = new Option(category.name, category.id);
					});				
					var parentId = document.getElementById(id + "_parentId");
					while (parentId.length > 1) {
	    				parentId.remove(1);
					} 
					if (jsonObj.tree.length > 0) {		
						i = 1;
						jsonObj.tree.each(function(category) {
							parentId.options[i++] = new Option(category.name, category.id, false, true);
						});				
					}
				}
			}
		}).send();
}
//-->
</script>

<fieldset id="extraInfo" class="register_fieldset">
<legend>Industry</legend>
<table border="0" cellpadding="3" cellspacing="1">
  <c:forEach items="${customerForm.parentIds}" var="parentId" varStatus="status">
  <tr>
    <td align="right">&nbsp;<fmt:message key="category" /> : </td>
    <td>
      <table>
      <tr><td>
	    <select name="parentIds" id="cat${status.index}_parentId" style="width:300px;" onchange="updateCategory('cat${status.index}', this.value)">
	      <c:forEach items="${model.tree[status.index]}" var="category" varStatus="treeStatus">
	      <c:if test="${treeStatus.first and category.id != gSiteConfig['gCUSTOMER_CATEGORY']}">
  	      <option value="<c:out value="${gSiteConfig['gCUSTOMER_CATEGORY']}"/>"><fmt:message key="categoryMain" /></option>
	      </c:if>
  	      <option value="${category.id}" <c:if test="${category.id == parentId}">selected</c:if>>
  	        <c:choose>
  	          <c:when test="${treeStatus.first and category.id == gSiteConfig['gCUSTOMER_CATEGORY']}"><fmt:message key="categoryMain" /></c:when>
  	          <c:otherwise><c:out value="${category.name}"/></c:otherwise>
  	        </c:choose>
  	      </option>
	      </c:forEach>
	    </select>
	  </td><td>
	    <img id="cat${status.index}_spinner" src="${_contextpath}/assets/Image/Layout/spinner.gif" alt="Spinner" style="display:none">
	  </td></tr>
	  </table>
    </td>
  </tr>   
  <tr>
    <td align="right">&nbsp;</td>
    <td>
	  <select name="cat${status.index}_categoryIds" id="cat${status.index}_categoryIds" size="5" style="width:300px;" onchange="updateCategory('cat${status.index}', this.value)">	  
	    <c:forEach items="${model.categories[status.index]}" var="category">
  	    <option value="${category.id}" <c:if test="${category.id == customerForm.categoryIds[status.index]}">selected</c:if>><c:out value="${category.name}"/></option>
	    </c:forEach>
	  </select>
    </td>
  </tr>  
  </c:forEach>
</table>
</fieldset>
</c:if>

<fieldset id="extraInfo" class="register_fieldset">
<legend>
<c:choose>
  <c:when test="${model.lang=='es'}" >
    Información Extra
  </c:when>
  <c:otherwise>
    Extra Information
  </c:otherwise>
</c:choose>
</legend>
<table border="0" cellpadding="3" cellspacing="1">
<c:forEach items="${customerForm.customerFields}" var="customerField">
  <tr>
    <td width="5px">
  	<c:if test="${customerField.required}" ><div class="requiredField" align="right"> * </div></c:if>
  	</td>
    <td>
    
    <c:choose>
    <c:when test="${model.lang=='es'}" >
    	<c:out value="${customerField.nameEs}"/>:
    </c:when>
    <c:otherwise>
    	<c:out value="${customerField.name}"/>:
    </c:otherwise>
    </c:choose>
    
    </td>
    <td>  
    <c:choose>
     <c:when test="${!empty customerField.preValue}">
     
       <form:select path="customer.field${customerField.id}" cssErrorClass="errorField">
		      <form:option value="">Please Select</form:option>
		      <c:forTokens items="${customerField.preValue}" delims="," var="dropDownValue">         	
			            <form:option value="${dropDownValue}" />
		      </c:forTokens>
	   </form:select>

       
     </c:when>
     <c:otherwise>
       <form:input autocomplete="off" path="customer.field${customerField.id}"  htmlEscape="true" maxlength="255" size="20" cssErrorClass="errorField"/> 
     </c:otherwise>
    </c:choose><form:errors path="customer.field${customerField.id}" cssClass="error" />
    </td>
  </tr>
</c:forEach> 
</table>      
</fieldset>
  <div id="form_buttons">
		<div class="row">
		  <div class="col-sm-12">
			<div id="buttons_wrapper" style="display: inline-block;padding: 10px 1150 0 0;">		
			    <button type="submit" name="_target0" class="btn btn-default">Back</button>		
			</div>
			<div style="display: inline-block;" id="buttonWrapper">
 				 <input type="submit" value="<fmt:message key="nextStep" />" name="_target2" class="btn btn-default" />
			</div>
		  </div>
		</div>
	  </div>
	  
	  

</form:form>

  </tiles:putAttribute>
</tiles:insertDefinition>