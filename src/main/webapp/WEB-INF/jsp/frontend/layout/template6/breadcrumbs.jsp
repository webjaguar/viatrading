<%@ page session="false" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>

<div class="col-breadcrumb <c:if test="${not layout.hideLeftBar}">breadLeft</c:if>">
  <c:set var="linkTitle" />
  <c:set var="link" />

  <c:choose>
    <c:when test="${siteConfig['BREADCRUMBS_HOME_TITLE'].value != ''}">
      <c:set var="homePageLinkTitle" value="${siteConfig['BREADCRUMBS_HOME_TITLE'].value}"/>
    </c:when> 
	<c:otherwise>
	  <c:set var="homePageLinkTitle" value="home" />
    </c:otherwise>
  </c:choose>


  <ul class="breadcrumb">
	<%--Home Page Link--%>
	<li><a href="${_contextpath}/"><c:out value="${homePageLinkTitle}" /></a></li>
	
	<%--Rest of the breadcrumbs--%>
	<c:forEach items="${model.breadCrumbs}" var="category" varStatus="status">
	  <c:choose>
	    <c:when test="${status.last and model.product != null}">
	      <c:set var="linkTitle" value="${model.product.name}"/>
          <c:set var="link" value="#" />
        </c:when>
        <c:otherwise>
          <c:choose>
		    <c:when test="${category.linkType != 'NONLINK' and gSiteConfig['gMOD_REWRITE'] == '1'}">
		      <c:set var="link" value="${_contextpath}/${siteConfig['MOD_REWRITE_CATEGORY'].value}/${category.id}/${category.encodedName}.html" />
            </c:when>
		    <c:when test="${category.linkType != 'NONLINK' and gSiteConfig['gMOD_REWRITE'] != '1'}">
		      <c:set var="link" value="category.jhtm?cid=${category.id}" />
            </c:when>
		    <c:otherwise>
			  <c:set var="link" value="#" />
            </c:otherwise>
		  </c:choose>
		  <c:set var="linkTitle" value="${category.name}"/>
        </c:otherwise>
      </c:choose>
	  
	  <c:choose>
	    <c:when test="${status.last}">
	    	<li class="active"><c:out value="${linkTitle}" /></li>
	    </c:when>
	    <c:otherwise>
	     	<li><a href="${link}"><c:out value="${linkTitle}" /></a></li>
	    </c:otherwise>
	  </c:choose>
	  
	  
	</c:forEach>
	</ul>
</div>