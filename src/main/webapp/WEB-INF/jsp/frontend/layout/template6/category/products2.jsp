<%@ page session="false" %>
<%@ page import="java.util.*" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="/WEB-INF/tlds/custom.tld" prefix="wj" %>

<c:if test="${!empty model.products}">
				
<c:if test="${siteConfig['MINI_CART'].value == 'true'}">
  <script type="text/javascript">
  <!--
  function showCart() { callCart(800); }
  //-->
  </script>
</c:if>
<input type="hidden" id="maxComparisons" value="${model.maxComparisons}">
<input type="hidden" id="currentComparison" value="${fn:length(model.comparisonMap)}">

<c:set var="contentClass" value="col-sm-9 col-md-10"></c:set>
<c:if test="${layout.hideLeftBar}">
  <c:set var="contentClass" value="col-sm-12"></c:set>
</c:if>


<c:if test="${fn:length(model.comparisonMap) > 0}">
<div class="row">
   <div class="col-sm-12">
      <div class="comparedProductsThumbnailsWrapper">
		<%--
		<div class="comparedProductsThumbnails">
		  <ul>
			<c:forEach items="${model.comparisonMap}" var="">
			<li>
			  <a href="#"><span class="removeFromCompareList"><i class="fa fa-times-circle"></i></span></a>
			  <img src="img/products/product_thumbnail_65x65.jpg">
			</li>
		  	</c:forEach>
		  </ul>
		</div>
		 --%>
		<div class="compareButtonWrapper">
		  <a href="/comparison.jhtm" class="btn compareButton">View Comparison</a>
		</div>
	  </div>
   </div>
</div>
</c:if>


<div class="row">
  <div class="col-sm-12">
	<div class="pageNavigation">
	  <c:set var="postUrl" value="${_contextpath}/category.jhtm?cid=${model.thisCategory.id}"/>
      <c:if test="${gSiteConfig['gMOD_REWRITE'] == '1'}">
    	<c:set var="postUrl" value="${_contextpath}/${siteConfig['MOD_REWRITE_CATEGORY'].value}/${model.thisCategory.id}/${model.thisCategory.encodedName}.html?" />
   	  </c:if>
      	
	  
	  <form name="pagination" id="paginationFormId" class="formPageNavigation clearfix" action="${postUrl}">
	  	<c:if test="${gSiteConfig['gMOD_REWRITE'] != '1'}">
    	  <input type="hidden" name="cid" value="${model.thisCategory.id}">
    	</c:if>
      	
      	<c:set var="navigationLink" value="${postUrl}"/>
      	<c:if test="${catLuceneSearch != null}">
			<c:if test="${catLuceneSearch.sort != null }">
			  <c:set var="navigationLink" value="${navigationLink}sort=${catLuceneSearch.sort}"/>
			</c:if>
      		<c:if test="${catLuceneSearch.filters != null}">
		  	<c:forEach items="${catLuceneSearch.filters}" var="filter" varStatus="filterStatus" >
				<input type="hidden" name="facetNameValue" value="${fn:escapeXml(filter.parent)}_value_${fn:escapeXml(filter.name)}"></input>
				<c:set var="navigationLink" value="${navigationLink}&facetNameValue=${wj:urlEncode(filter.parent)}_value_${wj:urlEncode(filter.name)}"/>
			</c:forEach>
	      	</c:if>
	      	<c:if test="${catLuceneSearch.pageSize != null }">
			  <c:set var="navigationLink" value="${navigationLink}&size=${catLuceneSearch.pageSize}"/>
			</c:if>
         </c:if>
      	
      	<div class="pageShowing">
		  <span>Showing ${model.start + 1}-${model.pageEnd} of ${model.count}</span>
		</div>
		<div class="pageNavigationControl">
		  <c:if test="${siteConfig['PRODUCT_SORT_BY_FRONTEND'].value == 'true'}">
		  <div class="sortby">
			<span><fmt:message key="f_sortby"/></span>
			<select class="selectpicker" name="sort" onchange="submit();">
			  <c:choose>
				  <c:when test="${gSiteConfig['gADI']}">
	    		    <option value="rlv" <c:if test="${catLuceneSearch.sort == 'rlv'}">selected="selected"</c:if>>Relevance</option>
				    <option value="name" <c:if test="${catLuceneSearch.sort == 'name'}">selected="selected"</c:if>>Product Name</option>
				    <option value="braz" <c:if test="${catLuceneSearch.sort == 'braz'}">selected="selected"</c:if>>Brand A-Z</option>
				    <option value="brza" <c:if test="${catLuceneSearch.sort == 'brza'}">selected="selected"</c:if>>Brand Z-A</option>
				    <option value="lth" <c:if test="${catLuceneSearch.sort == 'lth'}">selected="selected"</c:if>>Price (Low to High)</option>
			        <option value="htl" <c:if test="${catLuceneSearch.sort == 'htl'}">selected="selected"</c:if>>Price (High to Low)</option>
				  </c:when>	
			      <c:otherwise>
	    			<option value="" <c:if test="${catLuceneSearch.sort == ''}">selected="selected"</c:if>>Please Select</option>
					<option value="name" <c:if test="${catLuceneSearch.sort == 'name'}">selected="selected"</c:if>>Name</option>
					<option value="lth" <c:if test="${catLuceneSearch.sort == 'lth'}">selected="selected"</c:if>>Price ($ to $$)</option>
				    <option value="htl" <c:if test="${catLuceneSearch.sort == 'htl'}">selected="selected"</c:if>>Price ($$ to $)</option>
		          </c:otherwise>	
		      </c:choose>
		    </select>
		  </div>
		  </c:if>
		  <div class="pagesize">
			<span><fmt:message key="f_pageSize"/></span>
			<select name="size" class="selectpicker" onchange="document.getElementById('page').value=1;submit();">   
			  <c:forEach items="${model.productPerPageList}"  var="current">
				<option value="${current}" <c:if test= "${current == catLuceneSearch.pageSize}">selected</c:if>>${current} <fmt:message key="perPage" /></option>
			  </c:forEach>
			</select>
		  </div>
		  <c:if test="${model.pageCount > 1}">
		  <div class="page">
			<span><fmt:message key="page"/></span>
			<c:choose>
		      <c:when test="${model.pageCount <= 100 and model.pageCount > 0}">
		        <select class="selectpicker" name="page" id="page" onchange="submit()">
				  <c:forEach begin="1" end="${model.pageCount}" var="page">
		  	        <option value="${page}" <c:if test="${page == (catLuceneSearch.page)}">selected</c:if>>${page}</option>
		          </c:forEach>
				</select>
				<span>of ${model.pageCount}</span>
		      </c:when>
		      <c:otherwise>
		        <input type="text" id="page" name="page" value="${catLuceneSearch.page}" size="5" class="textfield50" />
		        <input type="submit" value="go"/>
		      </c:otherwise>
		    </c:choose>
		  </div>
		  <div id="pageNav" class="pageNav">
			<c:if test="${catLuceneSearch.page == 1}"><span class="pageNavLink disabled"><a href="#"><span class="pageNavPrev"></span></a></span></c:if>
            <c:if test="${catLuceneSearch.page != 1}"><span class="pageNavLink"><a href="${navigationLink}&page=${catLuceneSearch.page-1}"><span class="pageNavPrev"></span></a></span></c:if>
    		
			<c:if test="${catLuceneSearch.page == model.pageCount}"><span class="pageNavLink disabled"><a href="#"><span class="pageNavNext"></span></a></span></c:if>
			<c:if test="${catLuceneSearch.page != model.pageCount}"><span class="pageNavLink"><a href="${navigationLink}&page=${catLuceneSearch.page+1}"><span class="pageNavNext"></span></a></span></c:if>
    	  </div>
		  </c:if>
		   <c:if test="${model.pageCount <= 1}">
               <input type="hidden" id="page" name="page" value="${catLuceneSearch.page}" size="5" class="textfield50" />
           </c:if>
		</div>
	  </form>
	</div>
  </div>
</div>


<div class="row">
  <c:choose>
	  <c:when test="${(paramView == '22') or ( empty paramView and ((model.thisCategory.displayMode == '22') or (empty model.thisCategory.displayMode and siteConfig['CATEGORY_DISPLAY_MODE'].value == '22')))}">
	     <%@ include file="/WEB-INF/jsp/frontend/thumbnail/sliderView.jsp" %>
	  </c:when>
	  <c:otherwise>
	  	<c:forEach items="${model.products}" var="product" varStatus="status">  
			<c:set var="productLink">${_contextpath}/product.jhtm?id=${product.id}<c:if test="${model.product == null or siteConfig['RECOMMENDED_LIST_SHOW_BREADCRUMB'].value == 'true'}">&cid=<c:out value="${param.cid}"/></c:if></c:set>
			<c:if test="${gSiteConfig['gMOD_REWRITE'] == '1' and not fn:contains(product.sku, '/')}">
		  	  <c:set var="productLink">${_contextpath}/${siteConfig['MOD_REWRITE_PRODUCT'].value}/${product.encodedSku}/${product.encodedName}.html<c:if test="${model.product == null or siteConfig['RECOMMENDED_LIST_SHOW_BREADCRUMB'].value == 'true'}">?cid=<c:out value="${param.cid}"/></c:if></c:set>
		    </c:if>
			
			<c:choose>
				<c:when test="${(paramView == '28-R') or ( empty paramView and ((model.thisCategory.displayMode == '28-R') or (empty model.thisCategory.displayMode and siteConfig['CATEGORY_DISPLAY_MODE'].value == '28-R')))}">
			      <%@ include file="/WEB-INF/jsp/frontend/layout/template6/thumbnail/view1.jsp" %>
			    </c:when> 
			    <c:otherwise>
			      <%@ include file="/WEB-INF/jsp/frontend/layout/template6/thumbnail/view2.jsp" %>
			    </c:otherwise> 
			</c:choose>
		</c:forEach>
	  </c:otherwise>
  </c:choose>
</div>

<div class="row">
  <div class="col-sm-12">
	<div class="pageNavigation">
	  <c:set var="postUrl" value="${_contextpath}/category.jhtm?cid=${model.thisCategory.id}"/>
      <c:if test="${gSiteConfig['gMOD_REWRITE'] == '1'}">
    	<c:set var="postUrl" value="${_contextpath}/${siteConfig['MOD_REWRITE_CATEGORY'].value}/${model.thisCategory.id}/${model.thisCategory.encodedName}.html?" />
   	  </c:if>
      	
	  
	  <form name="pagination" id="paginationFormId" class="formPageNavigation clearfix" action="${postUrl}">
	  	<c:if test="${gSiteConfig['gMOD_REWRITE'] != '1'}">
    	  <input type="hidden" name="cid" value="${model.thisCategory.id}">
    	</c:if>
      	
      	<c:set var="navigationLink" value="${postUrl}"/>
      	<c:if test="${catLuceneSearch != null}">
			<c:if test="${catLuceneSearch.sort != null }">
			  <c:set var="navigationLink" value="${navigationLink}sort=${catLuceneSearch.sort}"/>
			</c:if>
      		<c:if test="${catLuceneSearch.filters != null}">
		  	<c:forEach items="${catLuceneSearch.filters}" var="filter" varStatus="filterStatus" >
				<input type="hidden" name="facetNameValue" value="${fn:escapeXml(filter.parent)}_value_${fn:escapeXml(filter.name)}"></input>
				<c:set var="navigationLink" value="${navigationLink}&facetNameValue=${wj:urlEncode(filter.parent)}_value_${wj:urlEncode(filter.name)}"/>
			</c:forEach>
	      	</c:if>
	      	<c:if test="${catLuceneSearch.pageSize != null }">
			  <c:set var="navigationLink" value="${navigationLink}&size=${catLuceneSearch.pageSize}"/>
			</c:if>
         </c:if>
      	
      	<div class="pageShowing">
		  <span>Showing ${model.start + 1}-${model.pageEnd} of ${model.count}</span>
		</div>
		<div class="pageNavigationControl">
		  <c:if test="${siteConfig['PRODUCT_SORT_BY_FRONTEND'].value == 'true'}">
		  <div class="sortby">
			<span><fmt:message key="f_sortby"/></span>
			<select class="selectpicker" name="sort" onchange="submit();">
<c:choose>
				  <c:when test="${gSiteConfig['gADI']}">
	    		    <option value="rlv" <c:if test="${catLuceneSearch.sort == 'rlv'}">selected="selected"</c:if>>Relevance</option>
				    <option value="name" <c:if test="${catLuceneSearch.sort == 'name'}">selected="selected"</c:if>>Product Name</option>
				    <option value="braz" <c:if test="${catLuceneSearch.sort == 'braz'}">selected="selected"</c:if>>Brand A-Z</option>
				    <option value="brza" <c:if test="${catLuceneSearch.sort == 'brza'}">selected="selected"</c:if>>Brand Z-A</option>
				    <option value="lth" <c:if test="${catLuceneSearch.sort == 'lth'}">selected="selected"</c:if>>Price (Low to High)</option>
			        <option value="htl" <c:if test="${catLuceneSearch.sort == 'htl'}">selected="selected"</c:if>>Price (High to Low)</option>
				  </c:when>	
			      <c:otherwise>
	    			<option value="" <c:if test="${catLuceneSearch.sort == ''}">selected="selected"</c:if>>Please Select</option>
					<option value="name" <c:if test="${catLuceneSearch.sort == 'name'}">selected="selected"</c:if>>Name</option>
					<option value="lth" <c:if test="${catLuceneSearch.sort == 'lth'}">selected="selected"</c:if>>Price ($ to $$)</option>
				    <option value="htl" <c:if test="${catLuceneSearch.sort == 'htl'}">selected="selected"</c:if>>Price ($$ to $)</option>
		          </c:otherwise>	
		      </c:choose>		    </select>
		  </div>
		  </c:if>
		  <div class="pagesize">
			<span><fmt:message key="f_pageSize"/></span>
			<select name="size" class="selectpicker" onchange="document.getElementById('page').value=1;submit();">   
			  <c:forEach items="${model.productPerPageList}"  var="current">
				<option value="${current}" <c:if test= "${current == catLuceneSearch.pageSize}">selected</c:if>>${current} <fmt:message key="perPage" /></option>
			  </c:forEach>
			</select>
		  </div>
		  <c:if test="${model.pageCount > 1}">
		  <div class="page">
			<span><fmt:message key="page"/></span>
			<c:choose>
		      <c:when test="${model.pageCount <= 100 and model.pageCount > 0}">
		        <select class="selectpicker" name="page" id="page" onchange="submit()">
				  <c:forEach begin="1" end="${model.pageCount}" var="page">
		  	        <option value="${page}" <c:if test="${page == (catLuceneSearch.page)}">selected</c:if>>${page}</option>
		          </c:forEach>
				</select>
				<span>of ${model.pageCount}</span>
		      </c:when>
		      <c:otherwise>
		        <input type="text" id="page" name="page" value="${catLuceneSearch.page}" size="5" class="textfield50" />
		        <input type="submit" value="go"/>
		      </c:otherwise>
		    </c:choose>
		  </div>
		  <div id="pageNav" class="pageNav">
			<c:if test="${catLuceneSearch.page == 1}"><span class="pageNavLink disabled"><a href="#"><span class="pageNavPrev"></span></a></span></c:if>
            <c:if test="${catLuceneSearch.page != 1}"><span class="pageNavLink"><a href="${navigationLink}&page=${catLuceneSearch.page-1}"><span class="pageNavPrev"></span></a></span></c:if>
    		
			<c:if test="${catLuceneSearch.page == model.pageCount}"><span class="pageNavLink disabled"><a href="#"><span class="pageNavNext"></span></a></span></c:if>
			<c:if test="${catLuceneSearch.page != model.pageCount}"><span class="pageNavLink"><a href="${navigationLink}&page=${catLuceneSearch.page+1}"><span class="pageNavNext"></span></a></span></c:if>
    	  </div>
		  </c:if>
		   <c:if test="${model.pageCount <= 1}">
               <input type="hidden" id="page" name="page" value="${catLuceneSearch.page}" size="5" class="textfield50" />
           </c:if>
		</div>
	  </form>
	</div>
  </div>
</div>


<script type="text/javascript">
<!--
function compareProduct(productId){
	if(parseInt($('#currentComparison').val()) < parseInt($('#maxComparisons').val())) {
		
		$.ajax({
			url: "${_contextpath}/ajaxAddToCompare.jhtm?&productId="+productId,
			context: document.body
		}).done(function() {
			$('#compare-product'+productId).html('<a href=\"${_contextpath}/comparison.jhtm\" class=\"viewComparison_btn\">View Comparison</a>');
			$('#compare-product'+productId).attr('class', 'viewComparison_btn_wrapper');
			
			$('#currentComparison').val(parseInt($('#currentComparison').val()) + 1);
			
		});
	} else {
		alert('Max comparison limit reached.');
	}
}


$(window).load(function() {
   $(window).resize(function(){
      if($(window).width() > 767) {
            var product_image_wrapper_maxHeight = Math.max.apply(null, $(".product_image_wrapper").map(function(){
                   return $(this).height();
            }).get());
            var product_name_wrapper_maxHeight = Math.max.apply(null, $(".product_name_wrapper").map(function(){
                   return $(this).height();
            }).get());
            var product_wrapper_maxHeight = Math.max.apply(null, $(".product_wrapper").map(function(){
                   return $(this).height();
            }).get());
            $(".product_image_wrapper").height(product_image_wrapper_maxHeight);
            $(".product_name_wrapper").height(product_name_wrapper_maxHeight);
            $(".product_wrapper").height(product_wrapper_maxHeight);
      }
      else {
            $(".product_image_wrapper").css('height','auto');
            $(".product_name_wrapper").css('height','auto');
            $(".product_wrapper").css('height','auto');
      }
   }).trigger('resize');
});
//-->
</script>
</c:if>
