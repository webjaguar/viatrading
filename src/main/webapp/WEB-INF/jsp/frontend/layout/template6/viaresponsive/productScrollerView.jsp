<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="/WEB-INF/tlds/spring.tld" prefix="spring"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib uri="/WEB-INF/tlds/custom.tld" prefix="wj" %>

<div id="new_arrivals_wrapper">
		<div id="new_arriavals" class="container">
			<script type="text/javascript">
				$(window).load(function() {
					$(window).resize(function(){
						$(".products_carousel .product_name_wrapper").css('height','auto');
						var product_name_wrapper_maxHeight = Math.max.apply(null, $(".products_carousel .product_name_wrapper").map(function(){
							return $(this).height();
						}).get());
						$(".products_carousel .product_name_wrapper").height(product_name_wrapper_maxHeight);

						$(".products_carousel .product_image_wrapper").css('height','auto');
						var product_image_wrapper_maxHeight = Math.max.apply(null, $(".products_carousel .product_image_wrapper").map(function(){
							return $(this).height();
						}).get());
						$(".products_carousel .product_image_wrapper").height(product_image_wrapper_maxHeight);

						$(".products_carousel .product_bordered_box").css('height','auto');
						var product_bordered_box_maxHeight = Math.max.apply(null, $(".products_carousel .product_bordered_box").map(function(){
							return $(this).height();
						}).get());
						$(".products_carousel .product_bordered_box").height(product_bordered_box_maxHeight);

						$(".products_carousel .product_wrapper").css('height','auto');
						var product_wrapper_maxHeight = Math.max.apply(null, $(".products_carousel .product_wrapper").map(function(){
							return $(this).height();
						}).get());
						$(".products_carousel .product_wrapper").height(product_wrapper_maxHeight);
					}).trigger('resize');
				});
			</script>
			<div class="products_carousel_wrapper">		
		<div id="slideShow">
		</div>
	</div>
</div>
</div>