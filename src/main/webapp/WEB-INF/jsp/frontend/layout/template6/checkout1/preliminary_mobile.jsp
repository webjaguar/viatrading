<%@ page session="false" %>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib uri="/WEB-INF/tlds/spring.tld" prefix="spring" %>
<%@ taglib uri="/WEB-INF/tlds/custom.tld" prefix="wj" %>

<tiles:insertDefinition name="${_template}" flush="true">
  <c:if test="${_leftBar != '1' and _leftBar != '4'}">
    <tiles:putAttribute name="leftbar" value="/WEB-INF/jsp/frontend/layout/template${_leftBar}/leftbar.jsp" />
  </c:if>
  <tiles:putAttribute name="content" type="string">
<script type="text/javascript">
function checkPO( poRequired ) {
	if ( poRequired == 'warning' && document.getElementById('order.purchaseOrder').value == '' ) {
		reply = confirm("You have not typed a Purchase Order #. Purchase anyway?");
    	if (reply == false) {
     	document.getElementById('order.purchaseOrder').focus();
     	return false;
     	}
     	if (reply == true) document.getElementById('order.invoiceNote').value = "Call to request PO before shipping. \n" +  document.getElementById('order.invoiceNote').value;
	} else if ( poRequired == 'required' && document.getElementById('order.purchaseOrder').value == '' ) {
		alert("Please enter Purchase Order #");
   		document.getElementById('order.purchaseOrder').focus();
   		return false;
	}
}
function updateAccountNumber() {
	var accountNum =  document.getElementById("userAccountId").value;
	document.getElementById("accountNumberSpanId").innerHTML = accountNum;
}
-->	
</script>
<c:out value="${invoiceLayout.headerHtml}" escapeXml="false"/>
<div class="col-sm-12">
	<div class="shoppingCartWizard clearfix">
		<div class="wizard clearfix">
			<div class="wizard-bar" style="width: 33.333%;">
				<div class="wizard-step active clearfix">
					<div class="stepNmbr">1</div>
					<div class="stepDesc">
						Shopping Cart
					</div>
				</div>
			</div>
			<div class="wizard-bar" style="width: 33.333%;">
				<div class="wizard-step active clearfix">
					<div class="stepNmbr">2</div>
					<div class="stepDesc">
						Shipping &amp; Payment
					</div>
				</div>
			</div>
			<div class="wizard-bar" style="width: 33.333%;">
				<div class="wizard-step clearfix">
					<div class="stepNmbr">3</div>
					<div class="stepDesc">
						Order Confirmation
					</div>
				</div>
			</div>
		</div>
		<div class="progress">
			<div class="progress-bar" style="width: 33.333%;"></div>
			<div class="progress-bar" style="width: 33.333%;"></div>
		</div>
	</div>

	<div class="customerInfoBox">
		<div class="row">
			<div class="col-sm-6">
				<div class="customerShippingInfoBox">
					<form:form commandName="orderForm" action="checkout1.jhtm" method="post">
						<input type="hidden" name="newShippingAddress" value="true"> 
						<div class="headerTitle shippingInfo"><fmt:message key="shippingInformation" /></div>
						<div class="customerShippingInfo">
							<c:set value="${orderForm.order.shipping}" var="address"/>
						    <%@ include file="/WEB-INF/jsp/frontend/layout/template6/address.jsp" %>
						    <div class="buttonWrapper">
								<button type="submit" name="_target0" class="btn btn-default">Change</button>
							</div>
						</div>
					</form:form>
				</div>
			</div>
			<div class="col-sm-6">
				<div class="customerBillingInfoBox">
					<form action="checkout1.jhtm" method="post">
						<input type="hidden" name="newBillingAddress" value="true">
						<div class="headerTitle billingInfo"><fmt:message key="billingInformation" /></div>
						<div class="customerBillingInfo">
							<c:set value="${orderForm.order.billing}" var="address"/>
							<c:set value="true" var="billing" />
							<%@ include file="/WEB-INF/jsp/frontend/layout/template6/address.jsp" %>
							<div class="buttonWrapper">
								<button type="submit" name="_target1" class="btn btn-default">Change</button>
							</div>
						</div>
					</form>
				</div>
			</div>
		</div>
	</div>

	<div class="mainFormWrapper">
		<form:form commandName="orderForm" action="checkout1.jhtm" method="post" id="orderFormDetail">
			<div class="row">
				<div class="col-sm-6">
					<div class="shippingBox">
						<div class="headerTitle shippingMethodTitle"><fmt:message key="shippingMethod" /></div>
						<div class="shippingWrapper">
							<spring:bind path="orderForm.shippingRateIndex">
							<span class="error"><c:out value="${status.errorMessage}"/></span>
							<c:if test="${orderForm.order.hasRegularShipping}">
							  <div class="shippingMethods">
							    <c:forEach items="${orderForm.ratesList}" var="shippingrate" varStatus="loopStatus">
								  <div class="shippingMethod radio">
									<label> 
						              <input type="radio" id="scr_${loopStatus.index}" name="${status.expression}" value="${loopStatus.index}" <c:if test="${(status.value == loopStatus.index) || (shippingrate.title == 'Free')}">checked</c:if>  onclick="toggleShipping()">
								      <c:if test="${shippingrate.carrier == 'fedex'}" >
								        <c:set var="fedEx" value="true" />
								        <c:choose>
								          <c:when test="${fn:contains(shippingrate.code,'EXPRESS') or fn:contains(shippingrate.code,'OVERNIGHT') or fn:contains(shippingrate.code,'2_DAY')}"><img class="carrier_logo" src="assets/Image/Layout/FedEx_Express_Normal_2Color_Positive_20.gif" alt="FedEx normal"/></c:when>
								          <c:when test="${fn:contains(shippingrate.code,'GROUND_HOME')}"><img class="carrier_logo" src="assets/Image/Layout/FedEx_HomeDelivery_Normal_2Color_Positive_20.gif" alt="FedEx homedelivery"/></c:when>
								          <c:when test="${fn:contains(shippingrate.code,'GROUND')}"><img class="carrier_logo"ier fedex" src="assets/Image/Layout/FedEx_Ground_Normal_2Color_Positive_20.gif" alt="FedEx ground"/></c:when>
								          <c:when test="${fn:contains(shippingrate.code,'FREIGHT')}"><img class="carrier_logo" src="assets/Image/Layout/FedEx_Freight_Normal_2Color_Positive_20.gif" alt="FedEx freight"/></c:when>
								          <c:otherwise><img class="carrier_logo" src="assets/Image/Layout/FedEx_MultipleServices_Normal_2Color_Positive_20.gif" alt="FedEx multipleServices"/></c:otherwise>
								        </c:choose>
								      </c:if>
								      <c:if test="${shippingrate.carrier == 'ups'}" >
								        <c:set var="ups" value="true" />
								        <img class="carrier_logo" src="assets/Image/Layout/UPS_LOGO_S.gif" alt="UPS"/>
								      </c:if>
										
									  <div class="carrier_info">
										<span class="carrier"><c:out value="${shippingrate.title}" escapeXml="false"/></span> 
										<span class="shippingRate">
										  <c:if test="${shippingrate.price != null}">
        									<fmt:message key="${siteConfig['CURRENCY'].value}" /><fmt:formatNumber value="${shippingrate.price}" pattern="#,##0.00"/>
        							      </c:if>
     									</span>
									  </div>
									</label>
								  </div>
							    </c:forEach>
							  </div>
						    </c:if>	
							<c:if test="${! orderForm.addressValidation.valid}">
							  <div class="shippingAddressError">
								<c:out value="${orderForm.addressValidation.errorDescription}" escapeXml="false"/>
							  </div>
							</c:if>
							</spring:bind>
						  
						  <c:if test="${orderForm.customRatesList != null}">
							<spring:bind path="orderForm.customShippingRateIndex">
							<span class="error"><c:out value="${status.errorMessage}"/></span>
							<div class="shippingWrapper">
							<span class="error"></span>
							<div class="shippingMethods">
								<c:forEach items="${orderForm.customRatesList}" var="shippingrate" varStatus="loopStatus">
								<div class="shippingMethod radio">
								  <label> 
									<input type="radio" id="ccr_${loopStatus.index}" name="<c:out value="${status.expression}"/>" value="${loopStatus.index}" <c:if test="${status.value == loopStatus.index}">checked</c:if>  onclick="toggleShipping()">
							        <div class="carrier_info">
										<span class="carrier"><c:out value="${shippingrate.title}" escapeXml="false"/></span> 
										<c:if test="${shippingrate.price != null}">
										  <span class="shippingRate"><fmt:message key="${siteConfig['CURRENCY'].value}" /> <fmt:formatNumber value="${shippingrate.price}" pattern="#,##0.00"/></span>
										</c:if>	
									</div>
								  </label>
								</div>
								</c:forEach>
							</div>
							<div class="shippingAddressError"></div>
						    </div>
							</spring:bind>
						</c:if>
						</div>
					</div>
				</div>
				<div class="col-sm-6">
					<div class="paymentBox">
						
						<div class="headerTitle paymentMethodTitle"><fmt:message key="f_paymentMethod" /></div>
						<div class="paymentWrapper">
							 <c:choose>
								<c:when test="${orderForm.order.promoAvailable && orderForm.order.grandTotal <= 0}">
								  <!-- If Promo is available and Grand Total == 0 then disable the payment method -->
								  <fmt:message key="f_paymentNotRequired" />
								</c:when>
								<c:otherwise>
								  <spring:bind path="order.paymentMethod">
									<span class="error"><c:out value="${status.errorMessage}"/></span>
								  </spring:bind>
								  
								  <div class="paymentMethods">
									<c:choose>
							          <c:when test="${orderForm.custPayment != null and orderForm.custPayment != ''}">
							          <c:choose>
							          	<c:when test="${orderForm.custPayment == 'ccOnly'}">
							          		<c:if test="${gSiteConfig['gPAYPAL'] < 2 and siteConfig['CREDIT_CARD_PAYMENT'].value != ''}">
												<c:set value="true" var="creditcardbox"/>
												<div class="paymentMethod radio">
												  <label> 
												   	 <form:radiobutton path="order.paymentMethod" checked="checked" id="creditCard_radio" value="Credit Card" />
													 <fmt:message key="shoppingcart.creditcard" /><img class="creditCard_logo" src="assets/Image/Layout/creditCard.jpg" border="0" alt="<fmt:message key='shoppingcart.creditcard' />">
												  </label>
												</div>
												
												<div class="creditCardBox form-horizontal" style="display: none;">
												  
												  <div class="form-group">
													<div class="col-sm-12">
														<div class="enterInfo">Enter your information here:</div>
													</div>
												  </div>
												  
												  <div class="form-group">
													<div class="col-sm-12 col-md-6 col-lg-5">
														<label for="cc_type" class="control-label"> <fmt:message key="cardType" />: <sup class="requiredField">*</sup>
														</label>
													</div>
													<div class="col-sm-12 col-md-6 col-lg-7">
														<spring:bind path="orderForm.order.creditCard.type">
												    	   <select id="cc_type" name="<c:out value="${status.expression}"/>" class="form-control">
		                                                       <c:forTokens items="${siteConfig['CREDIT_CARD_PAYMENT'].value}" delims="," var="cc">
		                                                       <option value="${cc}" <c:if test="${status.value == cc}">selected</c:if>><fmt:message key="${cc}"/></option>
		                                                       </c:forTokens>
		                                                   </select>
													    </spring:bind>
												 	</div>
												</div>
												  
												<spring:bind path="orderForm.order.creditCard.number">
												<div class="form-group <c:if test="${status.error}">has-error</c:if>">
													<div class="col-sm-12 col-md-6 col-lg-5">
														<label for="cc_num" class="control-label">  <fmt:message key="cardNumber" />: <sup class="requiredField">*</sup>
														</label>
													</div>
													<div class="col-sm-12 col-md-6 col-lg-7">
													   <input id="cc_num" type="text" class="form-control" name="<c:out value="${status.expression}"/>" value="<c:out value="${status.value}"/>" maxlength="16" size="16">
												       <c:if test="${status.error}">
												         <small class="help-block error-message"><c:out value="${status.errorMessage}"/></small>
													   </c:if>
													</div>
												</div>
												</spring:bind> 
												 
												<spring:bind path="orderForm.order.creditCard.cardCode">
												<div class="form-group <c:if test="${status.error}">has-error</c:if>">
													<div class="col-sm-12 col-md-6 col-lg-5">
														<label for="cc_code" class="control-label"> <fmt:message key="cardVerificationCode" />: <sup class="requiredField">*</sup>
														</label>
													</div>
													<div class="col-sm-12 col-md-6 col-lg-7">
														<div class="row">
															<div class="col-sm-4 col-md-5 col-lg-4">
																
																<input id="cc_code"  type="text" class="form-control" name="<c:out value="${status.expression}"/>" value="<c:out value="${status.value}"/>" maxlength="4" size="4">
															</div>
															<div class="col-sm-8 col-md-7 col-lg-8">
															<a class="cvv2_help" href="#" onClick="openW('assets/CVV2.html')"><fmt:message key="shoppingcart.whereDoIFindthisCode" /></a>
			 									  			</div>
		 									  		   </div> 
		 									  		   <c:if test="${status.error}">
													    	<small class="help-block error-message"><c:out value="${status.errorMessage}"/></small>
													   </c:if>
													</div>
												</div>  
												</spring:bind>  
												  
												 
												 
												 <div class="form-group">
														<div class="col-sm-12 col-md-6 col-lg-5">
															<label class="control-label"> <fmt:message key="expDate" /> (MM/YY): <sup
																class="requiredField">*</sup>
															</label>
														</div>
														<div class="col-sm-12 col-md-6 col-lg-7">
															<div class="row">
																<div class="col-xs-6 col-sm-6">
																<spring:bind path="orderForm.order.creditCard.expireMonth">
																  <select id="cc_expireMonth" class="form-control" name="<c:out value="${status.expression}"/>">
																    <c:forTokens items="01,02,03,04,05,06,07,08,09,10,11,12" delims="," var="ccExpireMonth">
															          <option value="${ccExpireMonth}" <c:if test="${status.value==ccExpireMonth}">selected</c:if>><c:out value="${ccExpireMonth}"/></option>
															        </c:forTokens>
																  </select>
															    </spring:bind> 
					       										</div>
																<div class="col-xs-6 col-sm-6">
																	<spring:bind path="orderForm.order.creditCard.expireYear">
																	  <select id="cc_expireYear" class="form-control" name="<c:out value="${status.expression}"/>">
																	    <c:forEach items="${model.expireYears}" var="ccExpireYear">
																         <option value="${ccExpireYear}" <c:if test="${status.value==ccExpireYear}">selected</c:if>><c:out value="${ccExpireYear}"/></option>
																	    </c:forEach>
																	  </select>
																	  <font color="red"><c:out value="${status.errorMessage}"/></font>
															        </spring:bind> 
																</div>
															</div>
														</div>
													</div>
												</div>
										  </c:if>
							          	</c:when>
							          	<c:otherwise>
							          		<div><input type="radio" value="<c:out value="${orderForm.custPayment}"/>" name="order.paymentMethod" checked> <b><c:out value="${orderForm.custPayment}"/></b></div>
							          	</c:otherwise>
							          </c:choose>
									  </c:when>
									  <c:otherwise>
										
										<c:if test="${orderForm.order.grandTotal > 0.0}">	    
										  <c:if test="${siteConfig['GEMONEY_DOMAIN'].value != ''
												and fn:trim(siteConfig['GEMONEY_MERCHANTID'].value) != ''
												and fn:trim(siteConfig['GEMONEY_PASSWORD'].value) != ''
												and gSiteConfig['gPAYPAL'] < 2
												}">
											<c:set value="true" var="gemoneybox"/>
											<div class="paymentMethod radio">
											  <label> 
											   <form:radiobutton path="order.paymentMethod" id="ge_radio" value="GE Money" />  
											   <c:out value="${siteConfig['GEMONEY_PROGRAM_NAME'].value}"/>
											  </label>
											</div>
										  </c:if>
											
										  <c:if test="${gSiteConfig['gPAYPAL'] < 2 and siteConfig['CREDIT_CARD_PAYMENT'].value != ''}">
											<c:set value="true" var="creditcardbox"/>
											<div class="paymentMethod radio">
											  <label> 
											   	 <form:radiobutton path="order.paymentMethod" checked="checked" id="creditCard_radio" value="Credit Card" />
												 <fmt:message key="shoppingcart.creditcard" /><img class="creditCard_logo" src="assets/Image/Layout/creditCard.jpg" border="0" alt="<fmt:message key='shoppingcart.creditcard' />">
											  </label>
											</div>
											
											<div class="creditCardBox form-horizontal" style="display: none;">
											  
											  <div class="form-group">
												<div class="col-sm-12">
													<div class="enterInfo">Enter your information here:</div>
												</div>
											  </div>
											  
											  <spring:bind path="orderForm.order.creditCard.type">
											  <div class="form-group <c:if test="${status.error}">has-error</c:if>">
												<div class="col-sm-12 col-md-6 col-lg-5">
													<label for="cc_type" class="control-label"> <fmt:message key="cardType" />: <sup class="requiredField">*</sup>
													</label>
												</div>
												<div class="col-sm-12 col-md-6 col-lg-7">
													   <select id="cc_type" name="<c:out value="${status.expression}"/>" class="form-control">
	                                                     <option value="">Please Select</option>
						                                 <c:forTokens items="${siteConfig['CREDIT_CARD_PAYMENT'].value}" delims="," var="cc">
	                                                       <option value="${cc}" <c:if test="${status.value == cc}">selected</c:if>><fmt:message key="${cc}"/></option>
	                                                     </c:forTokens>
	                                                   </select>
	                                                   <c:if test="${status.error}">
														<small class="help-block error-message"><form:errors path="order.creditCard.type"/></small>
													   </c:if>
	                                                   
												</div>
											  </div>
											  </spring:bind>
											 	
											<spring:bind path="orderForm.order.creditCard.number">
											<div class="form-group <c:if test="${status.error}">has-error</c:if>">
												<div class="col-sm-12 col-md-6 col-lg-5">
													<label for="cc_num" class="control-label">  <fmt:message key="cardNumber" />: <sup class="requiredField">*</sup>
													</label>
												</div>
												<div class="col-sm-12 col-md-6 col-lg-7">
												   <input id="cc_num" type="text" class="form-control" name="<c:out value="${status.expression}"/>" value="<c:out value="${status.value}"/>" maxlength="16" size="16">
											       <c:if test="${status.error}">
											         <small class="help-block error-message"><c:out value="${status.errorMessage}"/></small>
												   </c:if>
												</div>
											</div>
											</spring:bind> 
											 
											<spring:bind path="orderForm.order.creditCard.cardCode">
											<div class="form-group <c:if test="${status.error}">has-error</c:if>">
												<div class="col-sm-12 col-md-6 col-lg-5">
													<label for="cc_code" class="control-label"> <fmt:message key="cardVerificationCode" />: <sup class="requiredField">*</sup>
													</label>
												</div>
												<div class="col-sm-12 col-md-6 col-lg-7">
													<div class="row">
														<div class="col-sm-4 col-md-5 col-lg-4">
															
															<input id="cc_code"  type="text" class="form-control" name="<c:out value="${status.expression}"/>" value="<c:out value="${status.value}"/>" maxlength="4" size="4">
														</div>
														<div class="col-sm-8 col-md-7 col-lg-8">
														<a class="cvv2_help" href="#" onClick="openW('assets/CVV2.html')"><fmt:message key="shoppingcart.whereDoIFindthisCode" /></a>
		 									  			</div>
	 									  		   </div> 
	 									  		   <c:if test="${status.error}">
												    	<small class="help-block error-message"><c:out value="${status.errorMessage}"/></small>
												   </c:if>
												</div>
											</div>  
											</spring:bind>  
											  
											 
											 
											 <div class="form-group">
													<div class="col-sm-12 col-md-6 col-lg-5">
														<label class="control-label"> <fmt:message key="expDate" /> (MM/YY): <sup
															class="requiredField">*</sup>
														</label>
													</div>
													<div class="col-sm-12 col-md-6 col-lg-7">
														<div class="row">
															<div class="col-xs-6 col-sm-6">
															<spring:bind path="orderForm.order.creditCard.expireMonth">
															  <select id="cc_expireMonth" class="form-control" name="<c:out value="${status.expression}"/>">
															    <c:forTokens items="01,02,03,04,05,06,07,08,09,10,11,12" delims="," var="ccExpireMonth">
														          <option value="${ccExpireMonth}" <c:if test="${status.value==ccExpireMonth}">selected</c:if>><c:out value="${ccExpireMonth}"/></option>
														        </c:forTokens>
															  </select>
														    </spring:bind> 
				       										</div>
															<div class="col-xs-6 col-sm-6">
																<spring:bind path="orderForm.order.creditCard.expireYear">
																  <select id="cc_expireYear" class="form-control" name="<c:out value="${status.expression}"/>">
																    <c:forEach items="${model.expireYears}" var="ccExpireYear">
															         <option value="${ccExpireYear}" <c:if test="${status.value==ccExpireYear}">selected</c:if>><c:out value="${ccExpireYear}"/></option>
																    </c:forEach>
																  </select>
																  <font color="red"><c:out value="${status.errorMessage}"/></font>
														        </spring:bind> 
															</div>
														</div>
													</div>
												</div>
											</div>
										  </c:if>
											
									      <c:if test="${gSiteConfig['gPAYPAL'] > 0}">
											 <div class="paymentMethod radio">
											  <label> 
											   	 <form:radiobutton path="order.paymentMethod" value="PayPal" />
											     <a href="#" onclick="javascript:window.open('https://www.paypal.com/cgi-bin/webscr?cmd=xpt/Marketing/popup/OLCWhatIsPayPal-outside','olcwhatispaypal','toolbar=no, location=no, directories=no, status=no, menubar=no, scrollbars=yes, resizable=yes, width=400, height=350');"><img  src="https://www.paypal.com/en_US/i/logo/PayPal_mark_60x38.gif" border="0" alt="Acceptance Mark"></a>
											     <div id="paypalNote" class="note"><c:out value="${siteConfig['PAYPAL_PAYMENT_METHOD_NOTE'].value}" escapeXml="false"/></div>
											  </label>
											</div>
										  </c:if>
										</c:if>
										
										
										
										<c:if test="${gSiteConfig['gPAYPAL'] < 2}">
										  <c:if test="${orderForm.order.grandTotal > 0.0}">
										
											<c:if test="${siteConfig['AMAZON_URL'].value != ''
												and fn:trim(siteConfig['AMAZON_MERCHANT_ID'].value) != ''
												and fn:trim(siteConfig['AMAZON_ACCESS_KEY'].value) != ''
												and fn:trim(siteConfig['AMAZON_SECRET_KEY'].value) != ''
												}">
											 <div class="paymentMethod radio">
											   <label> 
											   	 <form:radiobutton path="order.paymentMethod" value="Amazon" />
											     <img src="assets/Image/Layout/amazon-payments.jpg" border="0" alt="Amazon Payments">
											   </label>
											 </div>
											</c:if>
										
											<c:if test="${gSiteConfig['gCREDIT_CARD_PAYMENT'] == 'netcommerce'}">
											  <div class="paymentMethod radio">
											    <label> 
											   	  <form:radiobutton path="order.paymentMethod" value="NetCommerce" /></td>  
											      <img src="http://www.netcommerce.com.lb/logo/NCseal_S.gif" border="0" alt="NetCommerceSecuritySeal">NetCommerce
											    </label>
											  </div>
											</c:if>
										
											<c:if test="${gSiteConfig['gCREDIT_CARD_PAYMENT'] == 'bankaudi'}">
										  	  <div class="paymentMethod radio">
											    <label> 
											   	  <form:radiobutton path="order.paymentMethod" value="BankAudi" /></td>  
											      BankAudi
											    </label>
											  </div>
											</c:if>
										
											<c:if test="${siteConfig['EBILLME_URL'].value != ''
												and fn:trim(siteConfig['EBILLME_MERCHANTTOKEN'].value) != ''
												and fn:trim(siteConfig['EBILLME_CANCEL_URL'].value) != ''
												and fn:trim(siteConfig['EBILLME_ERROR_URL'].value) != ''
												}">
											  <c:set value="true" var="ebillmebox"/>	
											  <div class="paymentMethod radio">
											    <label> 
											      <form:radiobutton path="order.paymentMethod" id="ebillme_radio" value="eBillme" />
											      <img src="https://www.ebillme.com/checkout/eBillme_logo_large_95x40.gif" alt="eBillme - Secure Cash Payments" title="eBillme - Secure Cash Payments" width="95" height="40" border="0" />
											    </label>
											  </div>
											</c:if>
										
											<c:if test="${siteConfig['GOOGLE_CHECKOUT_URL'].value != ''}">
											  <c:set value="true" var="googlebox"/>
											  <div class="paymentMethod radio">
											    <label> 
											      <form:radiobutton path="order.paymentMethod" id="google_radio" value="Google" />
											      <img src="https://checkout.google.com/buttons/checkout.gif?merchant_id=${siteConfig['GOOGLE_MERCHANT_ID'].value}&w=168&h=44&style=trans&variant=text&loc=en_US" alt="Google Checkout" />
											    </label>
											  </div>
											</c:if>
										</c:if>
										
										<!-- Custome Payments -->
										<c:forEach items="${model.customPayments}" var="paymentMethod">
										  <c:if test="${paymentMethod.enabled and not paymentMethod.internal and (not (fn:toLowerCase(fn:trim(paymentMethod.title)) == 'credit card') or siteConfig['CREDIT_CARD_PAYMENT'].value == '')}">
										    <div class="paymentMethod radio">
											  <label> 
											    <form:radiobutton path="order.paymentMethod" value="${paymentMethod.title}"   />
											    <c:out value="${paymentMethod.title}"/>
										        <c:if test="${paymentMethod.customPrice != null}">
										          <input type="hidden" name="isCODAvailable" value="true" id="isCODAvailable">
										        </c:if>
										    </label>
											</div>
										  </c:if>
										</c:forEach>
										</c:if>
										
										<form:errors path="order.creditCard.expireMonth">
										<c:if test="${siteConfig['CC_FAILURE_URL'].value != ''}">
										  <c:catch var="ccFailureUrlException">
											<c:import url="${siteConfig['CC_FAILURE_URL'].value}"/>
										  </c:catch>
										</c:if>
										<c:if test="${siteConfig['CC_FAILURE_URL'].value == '' or ccFailureUrlException != null}">
										  <div id="ccInfoError">
											<fmt:message key="shoppingcart.pleaseCallUs" /><fmt:message key="shoppingcart.thereSeemsToBeAProblemWithYourCCInformation" /></b><br />
											<fmt:message key="shoppingcart.ForYourReferenceATransmissionErrorEmailHasBeenForwardedToYou" />
											
											<fmt:message key="shoppingcart.mostErrorsAreSimplyTypographical" /></b><br />
											<fmt:message key="shoppingcart.weAskYouToReviewTheFollowing" />
											<div style="color:red;text-align:left">
											  <ul>
											    <li><fmt:message key="shoppingcart.creditCardNumber" /></li>
											    <li><fmt:message key="shoppingcart.ccidNumberCVV2" />
											      <ul>
											        <li><fmt:message key="shoppingcart.visaMasterCardLast3difitsOnBackOfCard" /></li>
											        <li><fmt:message key="shoppingcart.americanExp4DigitsInSmallPrintInTheUpper" /><br />
											        <fmt:message key="shoppingcart.rightSectionOfTheFrontOfTheCard" /></li>
											      </ul>
											    </li>
											    <li><fmt:message key="expirationDate" /></li>
											  </ul>
											</div>
											<div align="center"><fmt:message key="shoppingcart.ifYouContinueToGetThisError" /></div>
											<div align="center"><b><fmt:message key="shoppingcart.needHelp" /><br />
											  <fmt:message key="shoppingcart.pleaseCallUs" />
											  <c:out value="${siteConfig['COMPANY_PHONE'].value}"/>
											  </b></div>
											<div class="errorNote">
												<font color = "red">Gateway Error Note: <c:out value="${orderForm.order.creditCard.paymentNote}"/></font>
											</div>
										</div>
										</c:if>
										</form:errors>
									 </c:otherwise>
								    </c:choose>							  
								</div>
							  </c:otherwise>
							</c:choose>
							<c:choose>	
								  <c:when test="${gSiteConfig['gWILDMAN_GROUP']}">
							        <c:if test="${model.customerGroup != null && model.customerGroup.checkoutPurchaseOrder == 'true'}">
									  <div class="purchaseOrder">
									    <label for="order.purchaseOrder"><fmt:message key="shoppingcart.purchaseOrder" /></label> 
									    <form:input path="order.purchaseOrder" maxlength="50" htmlEscape="true"/>
									    <form:errors path="order.purchaseOrder" cssClass="error" />
								      </div>
							        </c:if>
								  </c:when>
								  <c:otherwise>
									<div class="purchaseOrder">
									  <label for="order.purchaseOrder"><fmt:message key="shoppingcart.purchaseOrder" /></label> 
									  <form:input path="order.purchaseOrder" maxlength="50" htmlEscape="true"/>
									  <form:errors path="order.purchaseOrder" cssClass="error" />
									</div>								
								  </c:otherwise>
							</c:choose>
								
							<c:if test="${fn:contains(siteConfig['USER_EXPECTED_DELIVERY_TIME'].value, 'true')}" >
								<div class="userExpectedDueDate">
									<label for="order_userDueDate"><fmt:message key="userExpectedDueDate" />:</label>
										<div id="order_userDueDate_wrapper" class="input-group date" data-date-format="MM-DD-YYYY">
											<input id="order_userDueDate" name="order.userDueDate" class="form-control" type="text">
											<span class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span></span>
										</div>
										<c:if test="${!fn:contains(siteConfig['USER_EXPECTED_DELIVERY_TIME'].value, 'script')}" >
										<script type="text/javascript">
										$(function(){
											$('#order_userDueDate_wrapper').datetimepicker({
												pickTime: false
											});
										});
										</script>
										</c:if>
									<form:errors path="order.userDueDate" cssClass="error"/>
								</div>
							</c:if>
							<c:if test="${siteConfig['REQUESTED_CANCEL_DATE'].value == 'true'}" >
								<div class="requestedCancelDate">
									<label for="order_requestedCancelDate"><fmt:message key="requestedCancelDate" />:</label>
										<div id="order_requestedCancelDate_wrapper" class="input-group date" data-date-format="MM-DD-YYYY">
											<input id="order_requestedCancelDate" name="order.requestedCancelDate" class="form-control" type="text">
											<span class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span></span>
										</div>
										<script type="text/javascript">
											$(function(){
												$('#order_requestedCancelDate_wrapper').datetimepicker({
													pickTime: false
												});
											});
										</script>
									<form:errors path="order.requestedCancelDate" cssClass="error"/>
								</div>
							</c:if>
						</div>
					</div>
				</div>
			</div>
			
			
			<c:if test="${gSiteConfig['gWILDMAN_GROUP']}">
			    
				<!-- User Account Numbers From Locations -->
				<c:if test="${!empty model.accountLocationMap}">
				<div class="row">
					<div class="col-sm-12">
						<div class="locationInfoBox">
							<div class="headerTitle locationInfoTitle"><fmt:message key="f_locationInformation" /></div>
							<div class="locationInfoWrapper">
								<span class="error"></span>
								<div class="locationAccountNum">
									<div class="row">
										<div class="col-sm-6 col-md-5">
											<select name="userAccount" id="userAccountId" style="width:50%;" onchange="updateAccountNumber()"  <c:if test="${orderForm.customer.defaultLocation}">hidden</c:if>>
											<c:forEach items="${model.accountLocationMap}"  var="accountLocation">
												<option value="${accountLocation.value}" <c:if test="${orderForm.customer.accountNumber == accountLocation.value}">selected</c:if>>${accountLocation.key}</option>
											</c:forEach>
											</select>
										</div>
										<div class="col-sm-6 col-md-7">
											<div class="accountNumberWrapper">
												<c:if test="${orderForm.customer.accountNumber != null}">
												<b>Account #:</b> <span id="accountNumberSpanId">${orderForm.customer.accountNumber}</span>
												</c:if>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
				</c:if>
				
				<c:if test="${orderForm.rushCharge != null || orderForm.baggingCharge != null}">
				<div class="row">
					<!-- Rush Charge -->
					<c:if test="${orderForm.rushCharge != null}">
						<div class="col-sm-6">
							<div class="rushServiceBox">
								<div class="headerTitle rushServiceTitle"><fmt:message key="rushService" /></div>
								<div class="rushServiceWrapper">
									<span class="error"></span>
									<div class="rushService">
										<div class="checkbox">
											<label>
												<form:checkbox path="hasRushService" onclick="toggleSelection()"/> <fmt:message key="rushCharge" /> <fmt:message key="${siteConfig['CURRENCY'].value}" /><fmt:formatNumber value="${orderForm.rushCharge}" pattern="#,##0.00"/>
											</label>
										</div>
										<div class="rushServiceDisclaimer"><span>*</span> Ship on or before <fmt:formatDate type="date" value="${rushServiceDate}" /></div>
									</div>
								</div>
							</div>
						</div>
					</c:if>
					
					<!-- Bagging Charge -->
					<c:if test="${orderForm.baggingCharge != null}">
						<div class="col-sm-6">
							<div class="baggingServiceBox">
								<div class="headerTitle baggingServiceTitle"><fmt:message key="baggingService" /></div>
								<div class="baggingServiceWrapper">
									<span class="error"></span>
									<div class="baggingService">
										<div class="checkbox">
											<label>
												<form:checkbox path="hasBaggingService" onclick="toggleSelection()"/> <fmt:message key="baggingCharge" />  <fmt:message key="${siteConfig['CURRENCY'].value}" /><fmt:formatNumber value="${model.customerGroup.baggingCharge}" pattern="#,##0.00"/>
											</label>
										</div>
									</div>
								</div>
							</div>
						</div>
					</c:if>
				</div>
				</c:if>
			</c:if>
			
			<div class="row">
				<div class="col-sm-12">
					<div class="reviewOrderWrapper">
						<div class="headerTitle reviewOrder">Review Order</div>

						<div class="reviewOrder_table">
							<div class="invoiceHdr">
								<div class="row clearfix">
									<div class="col-sm-12 col-md-1">
										<div class="cartLineNmbrHeader"><fmt:message key="line" />#</div>
									</div>
									<div class="col-sm-12 col-md-1">
										<div class="cartImageHeader"><fmt:message key="product" /></div>
									</div>
									<div class="col-sm-12 col-md-5">
										<div class="cartNameHeader"></div>
									</div>
									<div class="col-sm-12 col-md-1">
										<div class="cartQtyHeader"><fmt:message key="quantity" /></div>
									</div>
									<div class="col-sm-12 col-md-2">
										<div class="cartPriceHeader"><fmt:message key="productPrice" />
											<c:if test="${orderForm.order.hasRewardPoints}">
											  / <fmt:message key="points" />
											</c:if>
										</div>
									</div>
									<div class="col-sm-12 col-md-2">
										<div class="cartTotalHeader"><fmt:message key="total" /></div>
									</div>
								</div>
							</div>

							<div class="invoiceDetails">
								<c:forEach var="lineItem" items="${orderForm.order.lineItems}" varStatus="lineItemStatus">
								<c:set var="statusClass" value="odd_row" />
							    <c:if test="${(lineItemStatus.index + 1) % 2 == 0}">
							      <c:set var="statusClass" value="even_row" />
							    </c:if>
								<c:set var="productLink">${_contextpath}/product.jhtm?id=${lineItem.product.id}</c:set>
								<c:if test="${gSiteConfig['gMOD_REWRITE'] == '1' and not fn:contains(lineItem.product.sku, '/')}">
								  <c:set var="productLink">${_contextpath}/${siteConfig['MOD_REWRITE_PRODUCT'].value}/${lineItem.product.encodedSku}/${lineItem.product.encodedName}.html</c:set>
								</c:if>
								
								
								<div class="${statusClass} clearfix">
									
									
									<div class="col-sm-12 col-md-1">
										<div class="cartLineNmbrHeader visible-xs visible-sm"><fmt:message key="line" /> #</div>
										<div class="cartLineNmbr">${lineItemStatus.index + 1}</div>
										<div class="clearfix"></div>
									</div>
									
									<div class="col-sm-12 col-md-1">
										<div class="cartImageHeader visible-xs visible-sm">&nbsp;</div>
										<div class="cartImageWrapper">
										  	<c:if test="${siteConfig['SHOW_IMAGE_ON_INVOICE'].value == 'true' and lineItem.product.thumbnail != null}">
										  		<img class="cartImage img-responsive" src="<c:if test="${!lineItem.product.thumbnail.absolute}">${_contextpath}/assets/Image/Product/thumb/</c:if>${lineItem.product.thumbnail.imageUrl}" border="0" alt="${lineItem.product.alt}"/><br />
    										</c:if>
    									</div>
    									<div class="clearfix"></div>
									</div>
									
									<div class="col-sm-12 col-md-5">
										<div class="cartNameHeader visible-xs visible-sm"><fmt:message key="product" /></div>
										<div class="cartName">
											<div class="cartNameContent">
											<a href="${productLink}" class="cart_item_name"> <c:out value="${lineItem.product.name}" escapeXml="false" /> </a>
											<a href="${productLink}" class="cart_item_sku"><c:out value="${lineItem.product.sku}" escapeXml="false" /></a>
										  	<div class="cart_item_brand"><c:out value="${lineItem.product.manufactureName}" escapeXml="false"/></div>
											
										  	<c:if test="${(lineItem.productAttributes != null  or lineItem.asiProductAttributes != null) and (lineItem.itemGroup == null or !lineItem.itemGroupMainItem)}">
							    	  		  <div class="productOptions">
											  <c:forEach items="${lineItem.productAttributes}" var="productAttribute" varStatus="productAttributeStatus">
												<div class="option">
												<c:if test="${(! empty productAttribute.optionName) or (!empty productAttribute.valueString) or (! empty productAttribute.optionPriceOriginal)}">
												  	<span class="optionLabel"><c:out value="${productAttribute.optionName}"/>:</span> 
												  	<span class="optionValue"><c:out value="${productAttribute.valueString}" escapeXml="false"/></span>
													<c:if test="${(! empty productAttribute.optionPriceOriginal) && productAttribute.optionPriceOriginal != 0}">
													  <span class="optionPrice"><fmt:formatNumber value="${productAttribute.optionPriceOriginal}" pattern="#,##0.00" /> </span>
													  <span class="optionPriceMessage"> <fmt:message key="${productAttribute.optionPriceMessageF}"/> </span>
													</c:if>
												</c:if>
												<c:if test="${gSiteConfig['gPRODUCT_EVENT'] and productAttribute.eventType > 0}">
												<div id="productAttribute_${lineItem.lineNumber}">
												  <c:choose>
													<c:when test="${productAttribute.eventType == 1}">
														<input size="10px" class="eventName" type="text" autocomplete="off" value="${productAttribute.eventName}" name="__eventName_update_${productAttribute.id}"/>
														<input size="10px" class="eventZip" type="text" autocomplete="off" value="${productAttribute.zipCode}" name="__eventZipCode_update_${productAttribute.id}"/>
														<input size="10px" class="eventDate" type="text" autocomplete="off" value="${productAttribute.date}" name="__eventDate_update_${productAttribute.id}"/>
													</c:when>
													<c:when test="${productAttribute.eventType == 2}">
														<input size="10px" type="text" autocomplete="off" value="${productAttribute.zipCode}" name="__eventZipCode_update_${productAttribute.id}"/>
													</c:when>
													<c:when test="${productAttribute.eventType == 3}">
														<input size="10px" type="text" autocomplete="off" value="${productAttribute.eventName}" name="__eventName_update_${productAttribute.id}"/>
													</c:when>
												  </c:choose>
												</div>
												</c:if>
												</div>
								  	  		  </c:forEach>
								  	  		  <c:if test="${siteConfig['SHOW_IMAGE_ON_INVOICE'].value == 'true' and fn:length(lineItem.productAttributes) > 0}">
											    <c:forEach items="${lineItem.productAttributes}" var="productAttribute">
											      <c:if test="${!empty productAttribute.imageUrl}" >
											      <div class="option">
											            <span class="optionLabel"><c:out value="${productAttribute.optionName}" /> Image:</span>
											            <c:choose>
													      <c:when test="${productAttribute.absolute}">
													  	    <span class="optionValue"><img class="cartOptionImage" src="${productAttribute.imageUrl}" border="0" /></span>
													      </c:when>
													      <c:otherwise>
													   	 	<span class="optionValue"><img src="${_contextpath}/assets/Image/Product/options/<c:out value="${productAttribute.imageUrl}"/>" alt="Product option" class="cartOptionImage" /></span>
													      </c:otherwise>
													    </c:choose>
													    <span>( <c:out value="${fn:split(productAttribute.imageUrl, '/')[fn:length(fn:split(productAttribute.imageUrl, '/')) - 1]}"></c:out> )</span>
											      </div>
											      </c:if>
											    </c:forEach>
											  </c:if>
										  	  <c:forEach items="${lineItem.asiProductAttributes}" var="asiProductAttribute" varStatus="asiProductAttributeStatus">
												<div class="option">
													<span class="optionLabel"><c:out value="${asiProductAttribute.asiOptionName}"/>:</span> 
													<span class="optionValue"><c:out value="${asiProductAttribute.asiOptionValue}" escapeXml="false"/></span>
												</div>
											  </c:forEach>
											  </div>
										  	</c:if>
										</div>
										</div>
										<div class="clearfix"></div>
									</div>
									<div class="col-sm-12 col-md-1">
										<div class="cartQtyHeader visible-xs visible-sm"><fmt:message key="quantity" /></div>
										<div class="cartQty"><c:out value="${lineItem.quantity}" escapeXml="false" /></div>
										<div class="clearfix"></div>
									</div>
									<div class="col-sm-12 col-md-2">
										<div class="cartPriceHeader visible-xs visible-sm"><fmt:message key="productPrice" /></div>
										<div class="cartPrice">
										  <c:choose>
									  		<c:when test="${lineItem.product.caseContent != null}">
									  	 		<fmt:message key="${siteConfig['CURRENCY'].value}" /><fmt:formatNumber value="${lineItem.unitPrice * lineItem.product.caseContent}" pattern="#,##0.00#" maxFractionDigits="${siteConfig['MAX_FRACTION_DIGIT'].value}"/> <c:out value="${lineItem.product.packing}" escapeXml="false" />
									 	 	</c:when>
									 	 	<c:otherwise>
									 	 		<c:choose>
													<c:when test="${orderForm.order.hasRewardPoints and lineItem.product.productType == 'REW'}">
														<fmt:formatNumber value="${lineItem.unitPrice}" pattern="#,##0.00#" maxFractionDigits="${siteConfig['MAX_FRACTION_DIGIT'].value}"/>
													</c:when>
													<c:otherwise>
									 	 				<fmt:message key="${siteConfig['CURRENCY'].value}" /><fmt:formatNumber value="${lineItem.unitPrice}" pattern="#,##0.00#" maxFractionDigits="${siteConfig['MAX_FRACTION_DIGIT'].value}"/> <c:out value="${lineItem.product.packing}" escapeXml="false" />
													</c:otherwise>
												</c:choose>
									 	 	</c:otherwise>
									  	  </c:choose>
									  	</div>
										<div class="clearfix"></div>
									</div>
									
									<div class="col-sm-12 col-md-2">
										<div class="cartTotalHeader visible-xs visible-sm"><fmt:message key="total" /></div>
										<div class="cartTotal"><fmt:message key="${siteConfig['CURRENCY'].value}" /><fmt:formatNumber value="${lineItem.totalPrice}" pattern="#,##0.00"/></div>
										<div class="clearfix"></div>
									</div>
								</div>
								</c:forEach>
							</div>

							<div class="invoiceFtr">
								<div class="row">
									<div class="col-sm-offset-6 col-sm-4">
										<div class="subTotalLabel"><fmt:message key="subTotal" />:</div>
									</div>
									<div class="col-sm-2">
										<div class="subTotalValue"><fmt:message key="${siteConfig['CURRENCY'].value}" /><fmt:formatNumber value="${orderForm.order.subTotal}" pattern="#,##0.00"/></div>
									</div>
								</div>
								
								<c:if test="${orderForm.order.rushCharge != null}">
									<div class="row">
										<div class="col-sm-offset-6 col-sm-4">
											<div class="subTotalLabel"><fmt:message key="rushCharge" />:</div>
										</div>
										<div class="col-sm-2">
											<div class="subTotalValue"><fmt:message key="${siteConfig['CURRENCY'].value}" /><fmt:formatNumber value="${orderForm.order.rushCharge}" pattern="#,##0.00"/></div>
										</div>
									</div>
								</c:if>
								<c:if test="${orderForm.order.baggingCharge != null}">
									<div class="row">
										<div class="col-sm-offset-6 col-sm-4">
											<div class="subTotalLabel"><fmt:message key="baggingCharge" />:</div>
										</div>
										<div class="col-sm-2">
											<div class="subTotalValue"><fmt:message key="${siteConfig['CURRENCY'].value}" /><fmt:formatNumber value="${orderForm.order.baggingCharge}" pattern="#,##0.00"/></div>
										</div>
									</div>
								</c:if>
								
								<c:if test="${orderForm.order.budgetEarnedCredits > 0.00}">
									<div class="row">
										<div class="col-sm-offset-6 col-sm-4">
											<div class="promoCodeDiscountLabel"><fmt:message key="f_earnedCredits" />:</div>
										</div>
										<div class="col-sm-2">
											<div class="promoCodeDiscountValue"><fmt:formatNumber value="${orderForm.order.budgetEarnedCredits}" pattern="-#,##0.00"/></div>
										</div>
									</div>
								</c:if>
								
								<c:if test="${orderForm.order.promo != null and orderForm.order.promo.discountType eq 'order'}">
								  <div class="row">
									
									<div class="col-sm-offset-6 col-sm-4">
									  <div class="promoCodeDiscountLabel">
									  <c:choose>
								        <c:when test="${orderForm.order.promoAmount == 0}"><fmt:message key="promoCode" /></c:when>
								        <c:otherwise><fmt:message key="discountForPromoCode" /></c:otherwise>
								      </c:choose>
								      <c:out value="${orderForm.order.promo.title}" />
								      <c:choose>
								        <c:when test="${orderForm.order.promoAmount == 0}"></c:when>
								        <c:when test="${orderForm.order.promo.percent}">
								          (<fmt:formatNumber value="${orderForm.order.promo.discount}" pattern="#,##0.00"/>%)
								        </c:when>
								        <c:otherwise>
								          (<fmt:message key="${siteConfig['CURRENCY'].value}" /><fmt:formatNumber value="${orderForm.order.promo.discount}" pattern="#,##0.00"/>)
								        </c:otherwise>
								      </c:choose>
								      </div>
								    </div>
									<div class="col-sm-2">
									  <div class="promoCodeDiscountValue">
									  <c:choose>
								        <c:when test="${orderForm.order.promoAmount == 0}">&nbsp;</c:when>
								        <c:otherwise>(<fmt:message key="${siteConfig['CURRENCY'].value}" /><fmt:formatNumber value="${orderForm.order.promoAmount}" pattern="#,##0.00"/>)</c:otherwise>
								      </c:choose>
								      </div>
									</div>
								  </div>
								</c:if>
								
								<c:if test="${orderForm.order.lineItemPromos != null and fn:length(orderForm.order.lineItemPromos) gt 0}">
								    <c:forEach items="${orderForm.order.lineItemPromos}" var="itemPromo" varStatus="status">
								    <div class="row">
									  <div class="col-sm-offset-6 col-sm-4">
									    <div class="promoCodeDiscountLabel">
											<c:choose>
										        <c:when test="${itemPromo.value == 0}"><fmt:message key="promoCode" /></c:when>
										        <c:otherwise><fmt:message key="discountForPromoCode" /></c:otherwise>
										    </c:choose>
										    <c:out value="${itemPromo.key}" />
								        </div>
									  </div>
									  <div class="col-sm-2">
									    <div class="promoCodeDiscountValue">
									    	<c:choose>
									       		<c:when test="${itemPromo.value == 0}">&nbsp;</c:when>
									       		<c:otherwise>(<fmt:message key="${siteConfig['CURRENCY'].value}" /><fmt:formatNumber value="${itemPromo.value}" pattern="#,##0.00"/>)</c:otherwise>
									      	</c:choose>
									    </div>
									  </div>
									</div>
								    </c:forEach>  
								</c:if>
								
								
								<c:if test="${orderForm.order.tax != null}">
								<c:choose>
								  <c:when test="${orderForm.order.taxOnShipping}">
								    <div class="row">
									  <div class="col-sm-offset-6 col-sm-4">
									    <div class="shippingHandlingLabel">
									      <fmt:message key="shippingHandling" /> (<c:out value="${orderForm.order.shippingMethod}" escapeXml="false"/>):
								        </div>
									  </div>
									  <div class="col-sm-2">
									    <div class="shippingHandlingValue"><fmt:message key="${siteConfig['CURRENCY'].value}" /><fmt:formatNumber value="${orderForm.order.shippingCost}" pattern="#,##0.00"/></div>
									  </div>
									</div>
									<div class="row">
									  <div class="col-sm-offset-6 col-sm-4">
									    <div class="taxLabel"><fmt:message key="tax" /></div>
									  </div>
									  <div class="col-sm-2">
										<div class="taxValue"><fmt:message key="${siteConfig['CURRENCY'].value}" /><fmt:formatNumber value="${orderForm.order.tax}" pattern="#,##0.00"/></div>
									  </div>
									</div>
								  </c:when>
								  <c:otherwise>
								    <div class="row">
									  <div class="col-sm-offset-6 col-sm-4">
									    <div class="taxLabel"><fmt:message key="tax" /></div>
									  </div>
									  <div class="col-sm-2">
										<div class="taxValue"><fmt:message key="${siteConfig['CURRENCY'].value}" /><fmt:formatNumber value="${orderForm.order.tax}" pattern="#,##0.00"/></div>
									  </div>
									</div>
								  	<div class="row">
									  <div class="col-sm-offset-6 col-sm-4">
									    <div class="shippingHandlingLabel">
									      <fmt:message key="shippingHandling" /> (<c:out value="${orderForm.order.shippingMethod}" escapeXml="false"/>):
								        </div>
									  </div>
									  <div class="col-sm-2">
									    <div class="shippingHandlingValue"><fmt:message key="${siteConfig['CURRENCY'].value}" /><fmt:formatNumber value="${orderForm.order.shippingCost}" pattern="#,##0.00"/></div>
									  </div>
									</div>
									<c:if test="${orderForm.order.promo.discountType == 'shipping'}">
									<div class="row">
									  <div class="col-sm-offset-6 col-sm-4">
									    <div class="shippingHandlingLabel">   
									    <fmt:message key="discountForShipping" />
									    <c:choose>
								        	<c:when test="${orderForm.order.promo.percent}">
								         		 (<fmt:formatNumber value="${orderForm.order.promo.discount}" pattern="#,##0.00"/>%)
								        	</c:when>
								        	<c:otherwise>
								         	 (<fmt:message key="${siteConfig['CURRENCY'].value}" /><fmt:formatNumber value="${orderForm.order.promo.discount}" pattern="#,##0.00"/>)
								        	</c:otherwise>
								        </c:choose> 
								        </div>
									  </div>
									  <div class="col-sm-2">
									    <div class="promoCodeDiscountValue"><fmt:message key="${siteConfig['CURRENCY'].value}" /><fmt:formatNumber value="${orderForm.order.promoAmount}" pattern="#,##0.00"/></div>
									  </div>
									</div>
									</c:if>
								  </c:otherwise>
								</c:choose>
								</c:if>
								
 								<div class="row">
									<div class="col-sm-offset-6 col-sm-4">
										<div class="grandTotalLabel"><fmt:message key="grandTotal" />:</div>
									</div>
									<div class="col-sm-2">
										<div class="grandTotalValue"><fmt:message key="${siteConfig['CURRENCY'].value}" /><fmt:formatNumber value="${orderForm.order.grandTotal}" pattern="#,##0.00"/></div>
									</div>
								</div>
								<c:if test="${gSiteConfig['gGIFTCARD'] and order.creditUsed != null}">
									<div class="row">
										<div class="col-sm-offset-6 col-sm-4">
											<div class="creditDiscountLabel"><fmt:message key="credit" />:</div>
										</div>
										<div class="col-sm-2">
											<div class="creditDiscountValue"><fmt:formatNumber value="${orderForm.order.creditUsed}" pattern="-#,##0.00"/></div>
										</div>
									</div>
								</c:if>
								<div class="row">
									<div class="col-sm-offset-6 col-sm-4">
										<div class="blanceLabel"><fmt:message key="balance" />:</div>
									</div>
									<div class="col-sm-2">
										<div class="blanceValue"><fmt:message key="${siteConfig['CURRENCY'].value}" /><fmt:formatNumber value="${orderForm.order.balance}" pattern="#,##0.00"/></div>
									</div>
								</div>
								<c:if test="${orderForm.order.hasRewardPoints}">
									<div class="row">
										<div class="col-sm-offset-6 col-sm-4">
											<div class="rewardLabel"><c:out value="${siteConfig['GROUP_REWARD_POINTS_TITLE'].value}" /> :</div>
										</div>
										<div class="col-sm-2">
											<div class="rewardValue"><fmt:formatNumber value="${orderForm.order.rewardPointsTotal}" pattern="#,##0.00"/></div>
										</div>
									</div>
								</c:if>
							</div>
						</div>
					</div>
				</div>
			</div>
			
			<div class="row">
				<div class="col-sm-12">
					<div class="purchase_btn_wrapper clearfix">
						<button type="submit" class="purchase_btn btn" name="_finish" onclick="return checkPO('<c:out value='${orderForm.poRequired}'/>');">Purchase</button>
					</div>
				</div>
			</div>

			<c:if test="${gSiteConfig['gSALES_PROMOTIONS']}">
			<c:choose>
				<c:when test="${gSiteConfig['gWILDMAN_GROUP']}">
					<c:if test="${ model.customerGroup != null && model.customerGroup.checkoutPromocode != 'true'}">
						<div class="row">
							<div class="col-sm-12">
								<div id="promoContainer">
									<label for="promoCodeInput" class="promoTitle"><fmt:message key="f_enterPromoCode"/>:</label> 
									<form:input id="promoCodeInput" path="order.promoCode" htmlEscape="true"/>
									<div class="promo_btn_wrapper">
										<button type="submit" name="_target2" class="btn btn-default" onclick="return loadPromoCode(this.form)">Apply Promo Code</button>
									</div>
									<c:if test="${orderForm.tempPromoErrorMessage != null}"><span class="error"><fmt:message key="${orderForm.tempPromoErrorMessage}"/></span></c:if>
				 
								</div>
							</div>
						</div>
					</c:if>
				</c:when>
				<c:otherwise>
					<div class="row">
						<div class="col-sm-12">
							<div id="promoContainer">
								<label for="promoCodeInput" class="promoTitle"><fmt:message key="f_enterPromoCode"/>:</label> 
								<form:input id="promoCodeInput" path="order.promoCode" htmlEscape="true"/>
								<div class="promo_btn_wrapper">
									<button type="submit" name="_target2" class="btn btn-default" onclick="return loadPromoCode(this.form)">Apply Promo Code</button>
								</div>
								<c:if test="${orderForm.tempPromoErrorMessage != null}"><span class="error"><fmt:message key="${orderForm.tempPromoErrorMessage}"/></span></c:if>
			 				</div>
						</div>
					</div>
				</c:otherwise>
			</c:choose>
			</c:if>
			
			
			<!-- Earned Credits -->
			<c:if test="${gSiteConfig['gBUDGET'] &&  siteConfig['BUDGET_ADD_PARTNER'].value == 'true'}">
			<c:import url="/WEB-INF/jsp/frontend/layout/template6/checkout1/budgetEarnedCredits.jsp" />
			<c:if test="${orderForm.customer.budgetPartnersList != null and fn:length(orderForm.customer.budgetPartnersList) gt 0 }" >	
				<input type="image" border="0" id="__applyCredits" src="assets/Image/Layout/button_apply.gif" />
			</c:if>
			</c:if>
			
			<c:choose>
				<c:when test="${gSiteConfig['gBUDGET'] and model.creditApplied != null and !(orderForm.customer.creditAllowed < 0.0)}">
				<div class="creditWrapper">
				<div class="creditHd"><fmt:message key="f_credit" /></div>
				  <c:choose>
				  <c:when test="${model.mdfDisable == true}">
				  	<form:checkbox path="order.requestForCredit" value="${model.creditApplied}" disabled="true" />
				    <fmt:message key="f_mdfDisable"/>
				  </c:when>
				  <c:otherwise>
					<div class="note">
					  <fmt:message key="f_creditAvailable"/>: <fmt:message key="${siteConfig['CURRENCY'].value}" /><fmt:formatNumber value="${orderForm.customerCredit}" pattern="#,##0.00" />
					</div>
					<div class="note">
					  <fmt:message key="f_creditAllowed"/>: <fmt:message key="${siteConfig['CURRENCY'].value}" /><fmt:formatNumber value="${model.creditApplied}" pattern="#,##0.00" />
					</div>
				  	<form:checkbox path="order.requestForCredit" value="${model.creditApplied}" />
				  	<c:out value="${siteConfig['CREDIT_APPROVAL_MESSAGE'].value}" escapeXml="false"/>
				  </c:otherwise>
				  </c:choose>
				</div>
				</c:when>
				
				
				<c:when test="${ gSiteConfig['gGIFTCARD'] or (gSiteConfig['gVIRTUAL_BANK_ACCOUNT'] and orderForm.customerCredit > 0) }">
				<c:choose>
					<c:when test="${orderForm.customerCredit > 0}">
					<div class="row">
						<div class="col-sm-12">	
							<div id="availableCreditContainer">	
							<label class="availableCreditTitle"><fmt:message key="f_credit" /></label>
							<c:choose>
							   <c:when test="${orderForm.customerCredit >= orderForm.order.grandTotal}">
							   <div class="radio">
                                <label>
             					<input id="applyUserPoint1" name="order.paymentMethod" type="radio" value="vba"> 
							      <fmt:message key="applyOfMyCreditToThisOrder">
								      <c:choose>
								  		<c:when test="${siteConfig['CURRENCY'].value == 'EUR'}">
								  			<fmt:param value="&#8364;" /> 
								  		</c:when>
								  		<c:when test="${siteConfig['CURRENCY'].value == 'GBP'}">
								  			<fmt:param value="&#163;"/> 
								  		</c:when>
								  		<c:when test="${siteConfig['CURRENCY'].value == 'JPY'}">
								  			<fmt:param value="&#165;"/> 
								  		</c:when>
								  		<c:when test="${siteConfig['CURRENCY'].value == 'CAD'}">
								  			<fmt:param value="CAD&#36;"/> 
								  		</c:when>
								  		<c:otherwise>
								  		    <fmt:param value="&#36;" />
								  		</c:otherwise>
									  </c:choose><fmt:param value="${orderForm.customerCredit}"/>
								   </fmt:message>
								   </label>
								 </div>
                               <input type="hidden" name="_applyUserPoint" value="on">   
							  </c:when>
							    <c:otherwise>  
							      <div class="checkbox">
                                      <label>
                                      <input id="applyUserPoint1" name="applyUserPoint" type="checkbox" value="true"> 
									   <fmt:message key="applyOfMyCreditToThisOrder">
								   	   <c:choose>
								  		<c:when test="${siteConfig['CURRENCY'].value == 'EUR'}">
								  			<fmt:param value="&#8364;" /> 
								  		</c:when>
								  		<c:when test="${siteConfig['CURRENCY'].value == 'GBP'}">
								  			<fmt:param value="&#163;"/> 
								  		</c:when>
								  		<c:when test="${siteConfig['CURRENCY'].value == 'JPY'}">
								  			<fmt:param value="&#165;"/> 
								  		</c:when>
								  		<c:when test="${siteConfig['CURRENCY'].value == 'CAD'}">
								  			<fmt:param value="CAD&#36;"/> 
								  		</c:when>
								  		<c:otherwise>
								  		    <fmt:param value="&#36;" />
								  		</c:otherwise>
									    </c:choose><fmt:param value="${orderForm.customerCredit}"/>
								   		</fmt:message>
                                        </label>
                                       </div>
                                       <input type="hidden" name="_applyUserPoint" value="on">
							    </c:otherwise>
							 </c:choose>
							</div>
						</div>
					</div>
					</c:when>
				</c:choose>
				</c:when>
			</c:choose>
			
			
			<c:if test="${gSiteConfig['gGIFTCARD']}">
			<c:choose>
				<c:when test="${gSiteConfig['gWILDMAN_GROUP']}">
					<c:if test="${ model.customerGroup != null && model.customerGroup.checkoutGiftcard != 'true'}">
					<div class="row">
						<div class="col-sm-12">
							<div id="giftCardContainer">
								<div class="giftCardMesage">
									<fmt:message key="enterNewGiftCards" /><br />
		  							<fmt:message key="IfYouHaveMoreThanOneCodeEnterItBelowAndClickTheApplyButtonToUpdateThisPage" />
								</div>
								<label for="giftCardCodeInput" class="giftCardCodeTitle">
								  <fmt:message key="f_giftCardHeader"/>:
								</label> 
								<form:input id="giftCardCodeInput"  path="tempGiftCardCode" size="20"/>
								<c:if test="${orderForm.errorMessage != null}">
									<span class="error"><fmt:message key="${orderForm.errorMessage}"/></span>
								</c:if>
								
								<div class="giftCard_btn_wrapper">
									<button type="submit" id="__claimCode" name="_redeem" class="btn btn-default" onclick="return checkCode();">Apply Gift Card Code</button>
								</div>
							</div>
						</div>
					</div>
				 </c:if>
				</c:when>
				<c:otherwise>
					<div class="row">
						<div class="col-sm-12">
							<div id="giftCardContainer">
								<div class="giftCardMesage">
									<fmt:message key="enterNewGiftCards" /><br />
		  							<fmt:message key="IfYouHaveMoreThanOneCodeEnterItBelowAndClickTheApplyButtonToUpdateThisPage" />
								</div>
								<label for="giftCardCodeInput" class="giftCardCodeTitle">
								  <fmt:message key="f_giftCardHeader"/>:
								</label> 
								<form:input id="giftCardCodeInput"  path="tempGiftCardCode" size="20"/>
								<c:if test="${orderForm.errorMessage != null}">
									<span class="error"><fmt:message key="${orderForm.errorMessage}"/></span>
								</c:if>
								
								<div class="giftCard_btn_wrapper">
									<button type="submit" id="__claimCode" name="_redeem" class="btn btn-default" onclick="return checkCode();">Apply Gift Card Code</button>
								</div>
							</div>
						</div>
					</div>
				</c:otherwise>
			</c:choose>
			</c:if>
			
			<div class="row">
				<div class="col-sm-12">
					<div class="invoiceNote">
						<label for="order_invoiceNote" class="invoiceNoteTitle"><c:out value="${siteConfig['SPECIAL_INSTRUCTIONS'].value}"/>:</label>
						<form:textarea id="order_invoiceNote" path="order.invoiceNote"  rows="3" cols="40" htmlEscape="true" cssClass="textfield" />
					</div>
				</div>
			</div>
			
			<c:if test="${gSiteConfig['gORDER_FILEUPLOAD'] > 0}">
				<div class="row">
					<div class="col-sm-12">
						<div class="attachment">
							<label class="attachFileTitle">Attach File</label>
							<input type="file" class="file_attachment">
						</div>
					</div>
				</div>
			</c:if>
		</form:form>
	</div>

</div>

<script type="text/javascript">
<!--
$(window).load(function() {
	$(window).resize(function(){
		if($(window).width() > 767) {
			if($('.customerShippingInfoBox').height() > $('.customerBillingInfoBox').height()) {
				$('.customerBillingInfoBox').height($('.customerShippingInfoBox').height());
			}
			else if($('.customerShippingInfoBox').height() < $('.customerBillingInfoBox').height()) {
				$('.customerShippingInfoBox').height($('.customerBillingInfoBox').height());
			}

			if($('.shippingBox').height() > $('.paymentBox').height()) {
				$('.paymentBox').height($('.shippingBox').height());
			}
			else if($('.shippingBox').height() < $('.paymentBox').height()) {
				$('.shippingBox').height($('.paymentBox').height());
			}
			if($('.rushServiceBox') != null && $('.baggingServiceBox') != null) {
			 	if($('.rushServiceBox').height() > $('.baggingServiceBox').height()) {
	                $('.baggingServiceBox').height($('.rushServiceBox').height());
	            }
	            else if($('.rushServiceBox').height() < $('.baggingServiceBox').height()) {
	                $('.rushServiceBox').height($('.baggingServiceBox').height());
	            }
			}
		}
		else {
			$('.customerShippingInfoBox').css('height','auto');
			$('.customerBillingInfoBox').css('height','auto');
			$('.shippingBox').css('height','auto');
			$('.paymentBox').css('height','auto');
			if($('.rushServiceBox') != null && $('.baggingServiceBox') != null) {
            	$('.rushServiceBox').css('height','auto');
            	$('.baggingServiceBox').css('height','auto');
			}
		}
	}).trigger('resize');
});
function toggleShipping() {
	$("#orderFormDetail").css('background:url(/assets/Image/Layout/spinner.gif) no-repeat center 50%; background-color : #EEEEEE;');
	$("#orderFormDetail").fadeTo( "slow" , 0.5, function() {});
	$("#orderFormDetail").submit();
}
function toggleSelection() {
	document.getElementById('orderFormDetail').style['background'] = 'url(/assets/Image/Layout/spinner.gif) no-repeat center 50%'; 
	document.getElementById('orderFormDetail').style['background-color'] = '#EEEEEE'; 
	document.getElementById('orderFormDetail').submit(); 
}
function loadPromoCode( aform ) {
	
	if ( document.getElementById('promoCodeInput').value == '') {
		alert("Promo code is empty!");
		return false;
	}
}
<c:if test="${gSiteConfig['gGIFTCARD']}">
function checkCode(){
  if ( document.getElementById('giftCardCodeInput').value == "" ) {
  	alert("<fmt:message key='giftcard.empty' />");
  	return false;
  }
}
</c:if>
//-->
</script>
  </tiles:putAttribute>
</tiles:insertDefinition>