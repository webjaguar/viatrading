<%@ page session="false" %>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>

<!-- ========== LEFT SIDE BAR ========== -->
  <div id="btn-toggle-offcanvas" class="visible-xs">
	<button type="button" class="btn btn-primary" data-toggle="offcanvas">
	  <i class="fa fa-bars"></i>
	</button>
  </div>
  <div class="col-xs-6 col-sm-3 col-md-2 sidebar-offcanvas" id="sidebar" role="navigation">
	
	<c:set var="actionUrl" value="${_contextpath}/category.jhtm?cid=${model.thisCategory.id}" />
	<c:if test="${gSiteConfig['gMOD_REWRITE'] == '1'}">
		<c:set var="actionUrl" value="${_contextpath}/${siteConfig['MOD_REWRITE_CATEGORY'].value}/${model.thisCategory.id}/${model.thisCategory.encodedName}.html" />
	</c:if>	            
  
	<form action="${actionUrl}" method="get" id="luceneSearch">
	  <input type="hidden" name="hideSearchBox" value="<c:out value="${model.hideSearchBox}"/>">
      <input type="hidden" id="filterKeywords" name="keywords" value="<c:out value="${catLuceneSearch.keywords}"/>" maxlength="10">
      <input type="hidden" name="cid" value="<c:out value="${model.thisCategory.id}"/>">
      
      <div class="luceneSearchWrapper">
		<div class="luceneSearchBox">
		  
		  <c:if test="${model.filterMap != null and (fn:length(catLuceneSearch.filters) > 1 or (fn:length(catLuceneSearch.filters) == 1))}">
    	    <div class="luceneSearchSelectionWrapper">
			  <div class="luceneSearchSelectionTitle">You Selected</div>
			  <c:forEach items="${catLuceneSearch.filters}" var="filter" varStatus="filterStatus">
			      <div class="checkbox x-checkbox">
		            <label>
		              <input type="checkbox" checked="checked" onclick="removeFilter('${filter.parent}', '${filterStatus.index}');"> <c:out value="${filter.displayName}" />
		            </label>
		      	  </div>
    		  </c:forEach>
			</div>
    	  </c:if>
    	
    	  <div id="luceneFilterSelWrapper" style="display: none;">
		    <c:forEach items="${catLuceneSearch.filters}" var="filter" varStatus="filterStatus" >
		      <input type="hidden" name="facetNameValue" id="filter${filter.parent}_${filterStatus.index}" value="${fn:escapeXml(filter.parent)}_value_${fn:escapeXml(filter.name)}"></input>
		    </c:forEach>
		  </div>
    																			
		  <c:if test="${model.filterMap != null}">
    		<div class="luceneSearchFilters">
			  <c:forEach items="${model.filterMap}" var="filterPair" varStatus="groupStatus">
      			<c:if test="${filterPair.key != null and filterPair.key != ''}">
		          <div class="filterGroupTitle">
		            <div class="filterGroupTitleToggle <c:if test="${filterPair.key != null and  filterPair.key != '' and filterPair.key != 'Brand' and filterPair.key != 'Save'}">collapsed</c:if>" 
		            <c:if test="${filterPair.key != null and filterPair.key == 'Save'}">style="color:red;font-weight: bolder;font-size: 14px;"</c:if> data-toggle="collapse" data-target="#group${groupStatus.index+1}">
						${filterPair.key}
				    </div>
		          </div>
		        </c:if>
         
         		<c:choose>
         		  <c:when test="${fn:length(filterPair.value) > 1}">
         		
         			<div id="group${groupStatus.index+1}" class="filterGroupValues collapse in">
					<c:forEach items="${filterPair.value}" var="filter" varStatus="status">
	        		  <div class="luceneSearchFilter">
						<div class="filterTitle">
						  <c:set var="filterName" />
						  <c:choose>
				         	<c:when test="${filter.displayName != null}"><c:set var="filterName" value="${filter.displayName}"/></c:when>
				         	<c:otherwise><c:set var="filterName" value="${filter.name}"/></c:otherwise>
				          </c:choose>
	  				  	  <div class="filterTitleToggle" data-toggle="collapse" data-target="#filter${groupStatus.index+1}_${status.index + 1}">
							${filterName}
						  </div>
						</div>
					
						<div id="filter${groupStatus.index+1}_${status.index + 1}" class="filterValues collapse in <c:if test="${fn:length(filter.subFilters) > 10 and filter.name != 'Origin'}">filterValuesForLuceneSearch</c:if>" >      	
			 			  <c:forEach items="${filter.subFilters}" var="subFilter" varStatus="statusI">
	            		    <c:if test="${statusI.index < 50}">
	              			    
	                			    <div class="filterValue ${filter.name} ${subFilter.displayName}<c:if test="${statusI.index > 5}">filterIValue filterIValue${status.index}</c:if>">
					                <c:choose>
					                  <c:when test="${subFilter.redirectUrl != null}">
					                    <a href="${subFilter.redirectUrl}">
						                  <c:out value="${subFilter.displayName}" escapeXml="false"/>
						                  <span class="filterValueCount">
						                  (<c:out value="${subFilter.resultCount}" escapeXml="false"/>)
						                  </span>
					             	   </a>
					                  </c:when>
					                  <c:otherwise>
						               		 <input type="checkbox" <c:if test="${subFilter.selected}">checked="checked"</c:if> onclick="submitSearch('${filter.name}','${fn:escapeXml(fn:replace(subFilter.name, "'", "\\'"))}' <c:if test="${subFilter.selected}">, 'true'</c:if>);">
						                <a href="#" onclick="submitSearch('${filter.name}','${fn:escapeXml(subFilter.name)}' <c:if test="${subFilter.selected}">, 'true'</c:if>);">
						                  <c:out value="${subFilter.displayName}" escapeXml="false"/>
						                  <span class="filterValueCount">
						                  (<c:out value="${subFilter.resultCount}" escapeXml="false"/>)
						                  </span>
						                </a>
						              </c:otherwise>
					                </c:choose>
					                </div>
				            </c:if>
						  </c:forEach>
					  
						  <c:if test="${filter.rangeFilter}">
					   	  <span class="luceneSearchSelection">
					        <input type="text" placeholder="min" class="filterInput"  style="width : 40px;"  name="field_${filter.filterId}_min" maxlength="10" width="10">
					         - 
					        <input type="text" placeholder="max" class="filterInput"  style="width : 40px;"  name="field_${filter.filterId}_max" maxlength="10" width="10">
					        <input type="submit" value="go" onclick="removeRangeFilter('${filter.filterId}')" />
					      </span>
			            </c:if>
					  </div>
				  </div>
				</c:forEach>
				</div>
			  </c:when>
         	  <c:otherwise>
         		  <div id="group${groupStatus.index+1}" class="filterGroupValues collapse in">
				  <c:set var="filter" value="${filterPair.value[0]}" />
        		  <div class="luceneSearchFilter">
					
					<div id="filter${groupStatus.index+1}_${status.index + 1}" class="filterValues collapse in <c:if test="${fn:length(filter.subFilters) > 10 and filter.name != 'Origin'}">filterValuesForLuceneSearch</c:if>" >      	
		 			  <c:forEach items="${filter.subFilters}" var="subFilter" varStatus="statusI">
            		    <c:if test="${statusI.index < 50}">
              			      <div class="filterValue ${filter.name} ${subFilter.displayName}<c:if test="${statusI.index > 5}">filterIValue filterIValue${status.index}</c:if>">
					            <c:choose>
				                  <c:when test="${subFilter.redirectUrl != null}">
				                    <a href="${subFilter.redirectUrl}">
					                  <c:out value="${subFilter.displayName}" escapeXml="false"/>
					                  <span class="filterValueCount">
					                  (<c:out value="${subFilter.resultCount}" escapeXml="false"/>)
					                  </span>
				             	   </a>
				                  </c:when>
				                  <c:otherwise>
					                <c:if test="${filter.checkboxFilter}">
				                  	  <input type="checkbox" <c:if test="${subFilter.selected}">checked="checked"</c:if>  onclick="submitSearch('${filter.name}','${fn:escapeXml(fn:replace(subFilter.name, "'", "\\'"))}' <c:if test="${subFilter.selected}">, 'true'</c:if>);">
				                	</c:if>
				                    <a href="#" onclick="submitSearch('${filter.name}','${fn:escapeXml(subFilter.name)}' <c:if test="${subFilter.selected}">, 'true'</c:if>);">
					                  <c:out value="${subFilter.displayName}" escapeXml="false"/>
					                  <span class="filterValueCount">
					                  (<c:out value="${subFilter.resultCount}" escapeXml="false"/>)
					                  </span>
					                </a>
					              </c:otherwise> 
				                </c:choose>
				                </div>
				        </c:if>
					  </c:forEach>
					  
					  <c:if test="${filter.rangeFilter}">
				   	  <span class="luceneSearchSelection">
				        <input type="text" placeholder="min" class="filterInput"  style="width : 40px;"  name="field_${filter.filterId}_min" maxlength="10" width="10">
				         - 
				        <input type="text" placeholder="max" class="filterInput"  style="width : 40px;"  name="field_${filter.filterId}_max" maxlength="10" width="10">
				        <input type="submit" value="go" onclick="removeRangeFilter('${filter.filterId}')" />
				      </span>
		            </c:if>
					</div>
				  </div>
				</div>
         	  </c:otherwise>
         	</c:choose>
          </c:forEach>
		</div>
	  </c:if>	
	</div>
  </div>
  
 <c:if test="${(model.lang==null || model.lang!='es')}"> 
 <div class="leftbar_catLinks_wrapper">
	<div id=""><a class="leftbar_catLink" href="/wholesale/283/All_Products.html">ALL PRODUCTS</a></div>
	<div id=""><a class="leftbar_catLink" href="/wholesale/617/New-Arrivals.html">NEW ARRIVALS</a></div>
	<div id=""><a class="leftbar_catLink" href="/wholesale/738/Load-Center.html">LOAD CENTER</a></div>
	<div id=""><a class="leftbar_subcatLink" href="/wholesale/4/Clothing.html">CLOTHING</a></div>
	<div id=""><a class="leftbar_subcatLink" href="/wholesale/7/Cosmetics_Perfume_HBA.html">COSMETICS/HBA</a></div>
	<div id=""><a class="leftbar_subcatLink" href="/wholesale/9/Domestics.html">DOMESTICS</a></div>
	<div id=""><a class="leftbar_subcatLink" href="/wholesale/2/Electronics.html">ELECTRONICS</a></div>
	<div id=""><a class="leftbar_subcatLink" href="/wholesale/5/Fashion_Accessories.html">ACCESSORIES</a></div>
	<div id=""><a class="leftbar_subcatLink" href="/wholesale/344/Furniture.html">FURNITURE</a></div>
	<div id=""><a class="leftbar_subcatLink" href="/wholesale/8/General_Merchandise.html">GENERAL MERCHANDISE</a></div>
	<div id=""><a class="leftbar_subcatLink" href="/wholesale/368/Home_Kitchen.html">HOME &amp; KITCHEN</a></div>
	<div id=""><a class="leftbar_subcatLink" href="/wholesale/352/Seasonal.html">SEASONAL/HOLIDAY</a></div>
	<div id=""><a class="leftbar_subcatLink" href="/wholesale/6/Shoes.html">SHOES</a></div>
	<div id=""><a class="leftbar_subcatLink" href="/wholesale/3/Tools.html">TOOLS</a></div>
	<div id=""><a class="leftbar_subcatLink" href="/wholesale/362/Toys.html">TOYS</a></div>
	
 </div>
 </c:if>
 
  <c:if test="${model.lang!=null && model.lang=='es'}"> 
  <div class="leftbar_catLinks_wrapper">
    <h3>Categorias de Producto</h3>
	<div id=""><a class="leftbar_catLink" href="/wholesale/283/Productos.html">Todos los Productos</a></div>
	<div id=""><a class="leftbar_catLink" href="/wholesale/617/Nuevas-Llegadas.html">Nuevas Llegadas</a></div>
	<div id=""><a class="leftbar_catLink" href="/wholesale/738/Centro-de-Lotes.html">Centro de Lotes</a></div>
	<div id=""><a class="leftbar_subcatLink" href="/wholesale/5/Accesorios.html">Accesorios de Moda</a></div>
	<div id=""><a class="leftbar_subcatLink" href="/wholesale/6/Calzado.html">Calzado</a></div>
	<div id=""><a class="leftbar_subcatLink" href="/wholesale/368/Cocina.html">Cocina y Hogar</a></div>
	<div id=""><a class="leftbar_subcatLink" href="/wholesale/7/Cosmeticos.html">Cosméticos</a></div>
	<div id=""><a class="leftbar_subcatLink" href="/wholesale/9/Domesticos.html">Domésticos</a></div>
	<div id=""><a class="leftbar_subcatLink" href="/wholesale/2/Electronicos.html">Electrónicos</a></div>
	<div id=""><a class="leftbar_subcatLink" href="/wholesale/3/Herramientas.html">Herramientas</a></div>
	<div id=""><a class="leftbar_subcatLink" href="/wholesale/362/Juguetes.html">Juguetes</a></div>
	<div id=""><a class="leftbar_subcatLink" href="/wholesale/8/Mercancia-General.html">Mercancía General</a></div>
	<div id=""><a class="leftbar_subcatLink" href="/wholesale/344/Muebles.html">Muebles</a></div>
	<div id=""><a class="leftbar_subcatLink" href="/wholesale/4/Ropa.html">Ropa</a></div>
	<div id=""><a class="leftbar_subcatLink" href="/wholesale/352/Temporada.html">Temporada</a></div>
 </div>
 </c:if>
 
  
</div>
</form>
<!-- ========== END LEFT SIDE BAR ========== -->
					
					
<script type="text/javascript">
<!--
function submitSearch(name, value, remove){
	
	if(remove) {
		$('input[name="facetNameValue"]').each(function(index) { 
		    if($(this).val() == name+"_value_"+value) {
		    	$(this).remove();
		    }
		})
	} else {
		var hiddenField = document.createElement("input");
		
		hiddenField.setAttribute("type", "hidden"); 
		hiddenField.setAttribute("name", "facetNameValue"); 
		hiddenField.setAttribute("value", name+"_value_"+value); 
		
		document.getElementById('luceneSearch').appendChild(hiddenField);
	}
	
	$('form#luceneSearch').submit();
}

function removeFilter(name, index){
	$("[id='filter"+name+"_"+index+"']").remove();
	$('form#luceneSearch').submit();
}
function removeRangeFilter(id){
	if($('range_field_'+id+'_min') != null) {
		$('range_field_'+id+'_min').dispose();
	}
	if($('range_field_'+id+'_max') != null) {
		$('range_field_'+id+'_max').dispose();
	}
	$('form#luceneSearch').submit();
}
//-->
</script>