<%@ page session="false" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>

<div><strong><c:out value="${address2.firstName}"/>&nbsp;<c:out value="${address2.lastName}"/></strong></div>
<c:if test="${address2.company != ''}">
  <div><c:out value="${address2.company}"/></div>
</c:if>
<div><c:out value="${address2.addr1}"/></div>
<c:if test="${address2.addr2 != ''}">
  <div><c:out value="${address2.addr2}"/></div>
</c:if>
<div><c:out value="${address2.city}"/>,&nbsp;<c:out value="${address2.stateProvince}"/>&nbsp;<c:out value="${address2.zip}"/></div>

<c:if test="${billing != 'true' and order.purchaseOrder != '' and order.purchaseOrder != null and fn:contains(siteConfig['SITE_URL'].value, 'respectofflorida')}">
  <div><fmt:message key="poNumber" />:&nbsp;<c:out value="${order.purchaseOrder}"/></div>
</c:if>

<c:if test="${!gSiteConfig['gADI'] and !fn:contains(siteConfig['SITE_URL'].value, 'respectofflorida')}">
  <div><c:out value="${countries[address2.country]}"/></div>
</c:if>

<c:if test="${billing != 'true'}">
  <c:if test="${siteConfig['HIDE_INVOICE_ADDRESS_TYPE'].value != 'true'}">
    <div><c:out value="${address2.resToString}"/></div>
  </c:if>
</c:if>

<c:if test="${siteConfig['HIDE_INVOICE_PHONE_INFO'].value != 'true'}">
  <div class="phone">
    <span>Phone:</span> <c:out value="${address2.phone}"/>
  </div>
</c:if>