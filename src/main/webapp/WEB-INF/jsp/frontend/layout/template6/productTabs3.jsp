<%@ page session="false" %>
<%@ page import="java.util.*" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="/WEB-INF/tlds/custom.tld" prefix="wj" %>

<script type="text/javascript">
  function addToCart(productid)
 {
	  id = "hidden_"+productid;
	  alert(id);
	  alert(document.getElementById(id).value);
	  document.getElementById(id).innerHTML = 0;
	  alert('id');
 }
</script>

  <div class="tabs_wrapper">
	<ul id="product-tabs" class="nav nav-tabs">
	  
	<c:set var="tabTitleCount" value="0" />
	  
	<c:if test="${fn:length(product.longDesc) > 0}">
	    <c:set var="tabTitleCount" value="${tabTitleCount + 1}" />
	    <li <c:if test="${tabTitleCount == 1}">class="active"</c:if>><a href="#tab${tabTitleCount}" data-toggle="tab"><fmt:message key="description" /></a></li>
	</c:if>
	  
	<c:set var="tabTitleCount" value="${tabTitleCount + 1}" />
	 <li <c:if test="${tabTitleCount == 1}">class="active"</c:if>><a href="#tab${tabTitleCount}" data-toggle="tab"><fmt:message key="specification" /></a></li>
	
	  
	<c:if test="${gSiteConfig['gTAB'] > 0 and !empty model.tabList}" >
	<c:forEach items="${model.tabList}" var="tab" varStatus="status">
	 	<c:set var="tabTitleCount" value="${tabTitleCount + 1}" />
	   <c:choose>
	   <c:when test="${tab.tabNumber == 3 and userSession != null}">
	 	<li <c:if test="${tabTitleCount == 1}">class="active"</c:if>>
	 	<a href="#tab${tabTitleCount}" data-toggle="tab" id="avaiLoadsTab">
	 		<fmt:message key="availableLoads" />
	 	</a>
	 	</li>
	 	</c:when>
	 	<c:otherwise>
	        <c:if test="${tab.tabNumber != 3}">
	 			<li <c:if test="${tabTitleCount == 1}">class="active"</c:if>>
	 			<a href="#tab${tabTitleCount}" data-toggle="tab">
	 			
	 			<c:choose>
	 			<c:when test="${tab.tabName == 'Specifications'}">
	 				<fmt:message key="specification" />
	 			</c:when>
	 			<c:when test="${tab.tabName == 'Description'}">
	 				<fmt:message key="description" />
	 			</c:when>
	 			<c:when test="${tab.tabName == 'More Pictures'}">
	 				<fmt:message key="morePictures" />
	 			</c:when>
	 			<c:when test="${tab.tabName == 'Selling Tips'}">
	 				<fmt:message key="sellingTips" />
	 			</c:when>
	 			<c:otherwise>
	 				<c:out value="${tab.tabName}" escapeXml="false" />
	 			</c:otherwise>
	 			</c:choose>

	 			</a>
	 			</li>
	 	    </c:if>
	 	</c:otherwise>
	 	</c:choose>
	</c:forEach>
	</c:if>
	 
	<c:if test="${fn:length(model.listOfFiles) > 0}">
	  <c:set var="tabTitleCount" value="${tabTitleCount + 1}" />
	   <li <c:if test="${tabTitleCount == 1}">class="active"</c:if>><a href="#tab${tabTitleCount}" data-toggle="tab"><fmt:message key="morePictures" /></a></li>
	</c:if>
	
	  
	</ul>
	<div id="product-tabs-content" class="tab-content">
	  
	  <c:set var="tabValueCount" value="0" />
	  <c:if test="${fn:length(product.longDesc) > 0}">
	    <c:set var="tabValueCount" value="${tabValueCount + 1}" />
	    <div class="tab-pane fade <c:if test="${tabValueCount == 1}">in active</c:if>" id="tab${tabValueCount}">
		  <div class="details_long_desc">
			<c:out value="${product.longDesc}" escapeXml="false" />
		  </div>
	    </div>
	  </c:if>
	  
	  
  <c:set var="tabValueCount" value="${tabValueCount + 1}" />
  	 <div class="tab-pane fade <c:if test="${tabValueCount == 1}">in active</c:if>" id="tab${tabValueCount}">
		<div class="details_specifications_tab">
		<div class="odd_row clearfix">
			<div class="spec_title">
				<fmt:message key="store"/>
			</div>
			<div class="spec_info">
				${wj:removeChar(product.field45, ',' , true, true) }
			</div>
		</div>
		<div class="even_row clearfix">
			<div class="spec_title">
				<fmt:message key="productCondition"/>
			</div>
			<div class="spec_info">
				${wj:removeChar(product.field24, ',' , true, true) }
			</div>
		</div>
		<div class="odd_row clearfix">
			<div class="spec_title">
				<fmt:message key="units"/>
			</div>
			<div class="spec_info">
				${product.field7}
			</div>
		</div>
		<div class="even_row clearfix">
			<div class="spec_title">
				<fmt:message key="pallets"/>
			</div>
			<div class="spec_info">
				${product.field15}
			</div>
		</div>
		<div class="odd_row clearfix">
			<div class="spec_title">
				<fmt:message key="weight"/>
			</div>
			<div class="spec_info">
				<fmt:formatNumber value="${wj:removeChar(product.weight, ',' , true, true) }" pattern="#,###"/>
			</div>
		</div>
		<div class="even_row clearfix">
			<div class="spec_title">
				FOB
			</div>
			<div class="spec_info">
				${wj:removeChar(product.field13, ',' , true, true) }
			</div>
		</div>
		<div class="odd_row clearfix">
			<div class="spec_title">
				<fmt:message key="manifested"/> / <fmt:message key="links"/>
			</div>
			<div class="spec_info">
				${wj:removeChar(product.field14, ',' , true, true) }
			</div>
		</div>
		<div class="even_row clearfix">
			<div class="spec_title">
				<fmt:message key="loadPrice"/> %
			</div>
			<div class="spec_info">
				${product.field11}
			</div>
		</div>
		<div class="odd_row clearfix">
			<div class="spec_title">
				<fmt:message key="specSheet"/>
			</div>
			<div class="spec_info">
				<a target="_blank" href="#">																	
				${product.field23}
				</a>
			</div>
		</div>
	</div>
	</div>
 
	  

  <c:if test="${gSiteConfig['gTAB'] > 0 and !empty model.tabList}">
   <c:forEach items="${model.tabList}" var="tab" varStatus="status">
    <c:set var="tabValueCount" value="${tabValueCount + 1}" />
    <div class="tab-pane fade <c:if test="${tabValueCount == 1}">in active</c:if>" id="tab${tabValueCount}">
	  <div class="details_long_desc">
	   <c:choose>
	   <c:when test="${tab.tabNumber == 3 and userSession != null}">
		  

   
	<div class="availableLoads_tab">
	<form action="${_contextpath}/addToCart.jhtm" method="post" onsubmit="return checkForm()">
	<input type="hidden" id="loadcenter" name="loadcenter" value="true">
	<div class="table-responsive" id="availableLoads_table_wrapper">

	<c:set var="productLink">${_contextpath}/product.jhtm?id=${product.id}<c:if test="${model.product == null or siteConfig['RECOMMENDED_LIST_SHOW_BREADCRUMB'].value == 'true'}">&cid=<c:out value="${param.cid}"/></c:if></c:set>
	<c:if test="${gSiteConfig['gMOD_REWRITE'] == '1' and not fn:contains(product.sku, '/')}">
  	<c:set var="productLink">${_contextpath}/${siteConfig['MOD_REWRITE_PRODUCT'].value}/${product.encodedSku}/${product.encodedName}.html<c:if test="${model.product == null or siteConfig['RECOMMENDED_LIST_SHOW_BREADCRUMB'].value == 'true'}">?cid=<c:out value="${param.cid}"/></c:if></c:set>
	</c:if>	

	<table class="table" id="availableLoads_table">
	<thead>
	<tr>
  
   <th class="availableLoads_th_sku">
   <div class="th_content_wrapper">
   	   Sku
   </div>
   </th>
 
    <th class="availableLoads_th_nbUnits">
    <div class="th_content_wrapper">
  		<fmt:message key="units"/>
  	</div>
	</th>
	
	<th class="availableLoads_th_nbUnits">
    <div class="th_content_wrapper">
		 <fmt:message key="pallets"/>
  	</div>
	</th>	
	 
	<th class="availableLoads_th_nbUnits">
    <div class="th_content_wrapper">
  		FOB
  	</div>
	</th>	
	 
	<th class="availableLoads_th_nbUnits">
    <div class="th_content_wrapper">
  		 <fmt:message key="manifested"/> / <fmt:message key="links"/>
  	</div>
	</th>	
	 
	<th class="availableLoads_th_nbUnits">
    <div class="th_content_wrapper">
  		<fmt:message key="loadPrice"/>
  	</div>
	</th>	
	 
	<th class="availableLoads_th_nbUnits">
    <div class="th_content_wrapper">
  		<fmt:message key="price"/>/<fmt:message key="unit"/>
  	</div>
	</th>	 
	 
	<th class="availableLoads_th_nbUnits">
    <div class="th_content_wrapper">
  		<fmt:message key="price"/>
  	</div>
	</th>	
	
	<th class="availableLoads_th_addToCartCheckbox">
		<div class="th_content_wrapper">
			<fmt:message key="addToCart"/>
		</div>
	</th>
	
 	 </tr>
	</thead>

		  
	<tbody>		  
	<c:forEach items="${model.slavesList}" var="product" varStatus="status">	
		<tr class="row${status.index % 2}">

			<c:set var="productLink">${_contextpath}/product.jhtm?id=${product.id}<c:if test="${model.product == null or siteConfig['RECOMMENDED_LIST_SHOW_BREADCRUMB'].value == 'true'}">&cid=<c:out value="${param.cid}"/></c:if></c:set>
			<c:if test="${gSiteConfig['gMOD_REWRITE'] == '1' and not fn:contains(product.sku, '/')}">
	  		<c:set var="productLink">${_contextpath}/${siteConfig['MOD_REWRITE_PRODUCT'].value}/${product.encodedSku}/${product.encodedName}.html<c:if test="${model.product == null or siteConfig['RECOMMENDED_LIST_SHOW_BREADCRUMB'].value == 'true'}">?cid=<c:out value="${param.cid}"/></c:if></c:set>
			</c:if>									

    
		    <td class="availableLoads_td_sku">
			<div class="td_content_wrapper">
		  	${product.sku}
		  	 </div>
	        </td>
		  
			<td class="availableLoads_td_sku">
			<div class="td_content_wrapper">
		  	${product.field7}
		  	 </div>
	        </td>		  
			 
			 
			 <td class="availableLoads_td_sku">
			<div class="td_content_wrapper">
		  	${product.field15}
		  	 </div>
	        </td>
	        
	        
	        <td class="availableLoads_td_sku">
			<div class="td_content_wrapper">
		  	${wj:removeChar(product.field13, ',' , true, true) }
		  	 </div>
	        </td>
	        
	        
	        <td class="availableLoads_td_manifest">
			<div class="td_content_wrapper">
		    <a href="https://www.viatrading.com/dv/manifest/manifestdetails.php?SKU=${product.sku}&id=${product.id}&cid=${model.userId}" class="thumbnail_item_name" target="_blank"><c:out value="${wj:removeChar(product.field14, ',' , true, true)}" escapeXml="false" /></a>
		  	</div>
	        </td>

	           
	        <td class="availableLoads_td_sku">
			<div class="td_content_wrapper">
		  	${product.field11}
		  	 </div>
	        </td>        
	        
	        
	         <td class="availableLoads_td_sku">
               <div class="td_content_wrapper">
                     <c:choose>
                     <c:when test="true">
                     <c:catch var="catchFormatterException">
                             <fmt:formatNumber value="${wj:trimAndSplit(product.field12, '|' , false, ',', true, true)}" pattern="$#,###.00" />
                     </c:catch>
                             <c:if test = "${catchFormatterException != null}"><c:out value="${wj:trimAndSplit(product.field12, '|' , false, ',', true, true)}" escapeXml="false"/></c:if>
                     </c:when>
                     <c:otherwise>
                             <c:out value="${wj:trimAndSplit(product.field12, '|' , false, ',', true, true)}" escapeXml="false"/>
                     </c:otherwise>
                     </c:choose>
               </div>
            </td>
		 
		  	<td class="availableLoads_td_sku">
            <div class="td_content_wrapper">
            	<fmt:message key="${siteConfig['CURRENCY'].value}"/><fmt:formatNumber value="${wj:trimAndSplit(product.price1, '|' , false, ',', true, true)}" pattern="#,###.00" />
            </div>
            </td>
		 
  
  			<input type="hidden" name="quantity_${product.id}" value="1">
  			<input type="hidden" name="product.id" value="${product.id}">
 		 	<input type="hidden" name="quantity_${product.id}_${product.sku}" id="quantity_${product.id}_${product.sku}" value="${product.sku}"/>
  			<input type="hidden" name="hidden.id" id="hidden_${product.id}" value="0">
  
  			<td class="availableLoads_td_addToCartCheckbox">
				<div class="td_content_wrapper">
					<input id="checkbox_${product.id}" name="checkbox_${product.id}" type="checkbox" onClick="checkEnabled('hidden_${product.id}',${product.id})">
				</div>
  			</td>
  
		</tr>
	</c:forEach>

<tbody>		
</table>
 </div>
 
 
 <div class="buttons_wrapper clearfix">
 	<button class="btn addToCart_btn" type="submit">
			<fmt:message key="addToCart"/> &nbsp;<span class="addToCart_icon"></span>
	</button>
 </div>


 </form>
</div> 

		
	   </c:when>
	   <c:otherwise>
	    <c:if test="${tab.tabNumber != 3}">
	   		<c:out value="${tab.tabContent}" escapeXml="false"/>
	   	</c:if>
	   </c:otherwise>
	  </c:choose>
	  </div>
    </div>
   </c:forEach> 
  </c:if>
  
  
  <c:if test="${fn:length(model.listOfFiles) > 0}">  
	<c:set var="tabValueCount" value="${tabValueCount + 1}" />
	<div class="tab-pane fade <c:if test="${tabValueCount == 1}">in active</c:if>" id="tab${tabValueCount}">
		<div class="details_pictures_tab">
			<div class="thumbs_container">
			<div class="row">
			<c:forEach items="${model.listOfFiles}" var="file" varStatus="status">
			<div class="col-xs-4 col-sm-4 col-md-3 col-lg-2">
			<div class="thumbnail_image">
			<a href="${_contextpath}/assets/Image/Product/detailsbig/${model.morePicture}/<c:out value="${file.name}"/>" data-fancybox-group="fancybox-thumbs" data-fancybox-type="image" class="fancybox-thumbs">
				<img src="${_contextpath}/assets/Image/Product/detailsbig/${model.morePicture}/<c:out value="${file.name}"/>" class="img-responsive center-block"/>
			</a>
			</div>
			</div>
			</c:forEach>	
			</div>
			</div>
		</div>
	</div>
  </c:if>
 
 
 
<script type="text/javascript">
function checkEnabled(val,id) {
	if(document.getElementById(val).value=="0"){
		document.getElementById(val).value = id;
	}else{
		document.getElementById(val).value = "0";
	}
}

</script>
	  
 </div>
</div>