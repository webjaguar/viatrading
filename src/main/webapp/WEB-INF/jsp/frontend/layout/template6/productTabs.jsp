<%@ page session="false" %>
<%@ page import="java.util.*" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>

  <div class="tabs_wrapper">
	<ul id="product-tabs" class="nav nav-tabs">
	  
	  <c:set var="tabTitleCount" value="0" />
	  <c:if test="${fn:length(product.longDesc) > 0}">
	    <c:set var="tabTitleCount" value="${tabTitleCount + 1}" />
	    <li class="active"><a href="#tab${tabTitleCount}" data-toggle="tab"><fmt:message key="description" /></a></li>
	  </c:if>
	  
	  <c:choose>
  		<c:when test="${gSiteConfig['gPRODUCT_FIELDS_UNLIMITED'] > 0 and fn:length(product.productFieldsUnlimitedNameValue) > 0}">
	  	  <c:set var="tabTitleCount" value="${tabTitleCount + 1}" />
	      <li <c:if test="${tabTitleCount == 1}">class="active"</c:if> ><a href="#tab${tabTitleCount}" data-toggle="tab"><c:out value="${siteConfig['PRODUCT_FIELDS_TITLE'].value}" /></a></li>
        </c:when>
	    <c:when test="${fn:length(product.productFields) > 0}">
	  	  <c:set var="tabTitleCount" value="${tabTitleCount + 1}" />
	      <li <c:if test="${tabTitleCount == 1}">class="active"</c:if>><a href="#tab${tabTitleCount}" data-toggle="tab"><c:out value="${siteConfig['PRODUCT_FIELDS_TITLE'].value}" /></a></li>
        </c:when>
	  </c:choose>
	  
	  
	   <c:if test="${model.productReviewList.nrOfElements > 0}">
	  <c:set var="tabTitleCount" value="${tabTitleCount + 1}" />
	    <li <c:if test="${tabTitleCount == 1}">class="active"</c:if>><a href="#tab${tabTitleCount}" data-toggle="tab"><fmt:message key="productReviews" /></a></li>
	  </c:if>
	  
	  <c:if test="${fn:length(model.recommendedList) > 0}">
	  <c:set var="tabTitleCount" value="${tabTitleCount + 1}" />
	  <li>
	    <a href="#tab${tabTitleCount}" data-toggle="tab">
	      <c:choose>
		    <c:when test="${!empty product.recommendedListTitle}">
			  <c:out value="${product.recommendedListTitle}"/>
			</c:when>
			<c:otherwise>
			  <c:out value="${siteConfig['RECOMMENDED_LIST_TITLE'].value}"/>
			</c:otherwise>
		  </c:choose>
	    </a>
	  </li>
	  </c:if>
	  
	  <c:if test="${gSiteConfig['gTAB'] > 0 and !empty model.tabList}" >
	  <c:forEach items="${model.tabList}" var="tab" varStatus="status">
	    <c:set var="tabTitleCount" value="${tabTitleCount + 1}" />
	  	<li><a href="#tab${tabTitleCount}" data-toggle="tab"><c:out value="${tab.tabName}"/></a></li>
	  
	  </c:forEach> 
	  </c:if>
	</ul>
	
	<div id="product-tabs-content" class="tab-content">
	  
	  <c:set var="tabValueCount" value="0" />
	  <c:if test="${fn:length(product.longDesc) > 0}">
	    <c:set var="tabValueCount" value="${tabValueCount + 1}" />
	    <div class="tab-pane fade in active" id="tab${tabValueCount}">
		  <div class="details_long_desc">
		    <c:choose>
		      <c:when test="${gSiteConfig['gADI']}">
		        <h4>Main Features</h4>
			    <c:out value="${product.longDesc}" escapeXml="false" />
				<c:forEach items="${model.fieldGroupMap}" var="fieldGroup" varStatus="status"> 
				  <c:forEach items="${fieldGroup.value}" var="productFielUnlimitedNameValue" varStatus="status"> 
			        <c:if test="${productFielUnlimitedNameValue.displayName == 'Miscellaneous_Package Contents' or productFielUnlimitedNameValue.name == 'Miscellaneous_Package Contents'}">
				      <h4>Package Contents</h4>
				      <c:out value="${productFielUnlimitedNameValue.displayValue}" escapeXml="false"/>
				    </c:if>
				  </c:forEach>
				</c:forEach>
			  </c:when>
			  <c:otherwise>
				  <c:out value="${product.longDesc}" escapeXml="false" />
			  </c:otherwise>
			</c:choose>
		  </div>
	    </div>
	  </c:if>
	  
	  
	  
	  <c:if test="${(gSiteConfig['gPRODUCT_FIELDS_UNLIMITED'] > 0 and fn:length(product.productFieldsUnlimitedNameValue) > 0) or (fn:length(product.productFields) > 0)}">
	  <c:set var="tabValueCount" value="${tabValueCount + 1}" />
	  <div class="tab-pane fade <c:if test="${tabValueCount == 1}">in active</c:if>" id="tab${tabValueCount}">
		<div class="details_specification">
		  <%@ include file="/WEB-INF/jsp/frontend/layout/template6/productFields.jsp" %>
		</div>
	  </div>
	  </c:if>
	  
	  
	  <c:if test="${model.productReviewList.nrOfElements > 0}">
	  <c:set var="tabValueCount" value="${tabValueCount + 1}" />
	  <div class="tab-pane fade <c:if test="${tabValueCount == 1}">in active</c:if>" id="tab${tabValueCount}">
		<div class="details_features">
			<%@ include file="/WEB-INF/jsp/frontend/layout/template6/productReviewList.jsp" %>
		</div>
	  </div>
	  </c:if>
	  
	  <c:if test="${fn:length(model.recommendedList) > 0}">
	  <c:set var="tabValueCount" value="${tabValueCount + 1}" />
	  <div class="tab-pane fade <c:if test="${tabValueCount == 1}">in active</c:if>" id="tab${tabValueCount}">
		<div class="similar_products">
		<!-- Carousel -->
		  <div class="products_carousel owl-carousel">
			
		    
		    <c:forEach items="${model.recommendedList}" var="rProduct" varStatus="status">

			  <c:set var="productLink">${_contextpath}/product.jhtm?id=${rProduct.id}<c:if test="${model.product == null or siteConfig['RECOMMENDED_LIST_SHOW_BREADCRUMB'].value == 'true'}">&cid=<c:out value="${param.cid}"/></c:if></c:set>
			  <c:if test="${gSiteConfig['gMOD_REWRITE'] == '1' and not fn:contains(rProduct.sku, '/')}">
		  	    <c:set var="productLink">${_contextpath}/${siteConfig['MOD_REWRITE_PRODUCT'].value}/${rProduct.encodedSku}/${rProduct.encodedName}.html<c:if test="${model.product == null or siteConfig['RECOMMENDED_LIST_SHOW_BREADCRUMB'].value == 'true'}">?cid=<c:out value="${param.cid}"/></c:if></c:set>
			  </c:if>	
			
			  <div class="product">
			    <div class="product_image_div">
				  <a href="${productLink}">
				    <img class="item" src="<c:if test="${!rProduct.thumbnail.absolute}">${_contextpath}/assets/Image/Product/thumb/</c:if><c:out value="${rProduct.thumbnail.imageUrl}"/>">
				  </a>
			    </div>
			    <div class="product_name"><a href="${productLink}"><c:out value="${rProduct.name}" escapeXml="false" /></a></div>
			    <div class="product_price"> 
			      <c:choose>
				    <c:when test="${fn:length(rProduct.price) > 1}">
					  <span class="p_from_id"><fmt:message key="${siteConfig['CURRENCY'].value}" /><fmt:formatNumber type="currency"  pattern="#,##0.00" value="${rProduct.priceRangeMinimum}" /> </span>
					  <span class="price_range_separator">-</span>
					  <span class="p_to_id"><fmt:message key="${siteConfig['CURRENCY'].value}" /><fmt:formatNumber type="currency"  pattern="#,##0.00" value="${rProduct.priceRangeMaximum}" /></span>
				    </c:when>
				    <c:otherwise>
					  <span class="p_from_id"><fmt:message key="${siteConfig['CURRENCY'].value}" /><fmt:formatNumber type="currency"  pattern="#,##0.00" value="${rProduct.price1}" /> </span>
					</c:otherwise>
				  </c:choose>
			  
			    </div>
			  </div>
		    </c:forEach>
		  </div>
		  <!-- /Carousel -->
		</div>
	  </div>
	  </c:if>
	  
	  <c:if test="${gSiteConfig['gTAB'] > 0 and !empty model.tabList}" >
	  <c:forEach items="${model.tabList}" var="tab" varStatus="status">
	    <c:set var="tabValueCount" value="${tabValueCount + 1}" />
	    <div class="tab-pane fade <c:if test="${tabValueCount == 1}">in active</c:if>" id="tab${tabValueCount}">
		  <div class="details_long_desc">
			<c:out value="${tab.tabContent}" escapeXml="false"/>
		  </div>
	    </div>
	   </c:forEach> 
	  </c:if>
	  
	  
	</div>
  </div>