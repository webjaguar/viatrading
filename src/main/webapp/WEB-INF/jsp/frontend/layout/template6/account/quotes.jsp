<%@ page session="false" %>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="/WEB-INF/tlds/spring.tld" prefix="spring" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="/WEB-INF/tlds/custom.tld" prefix="wj" %>

<tiles:insertDefinition name="${_template}" flush="true">
  <c:if test="${_leftBar != '1' and _leftBar != '4'}">
    <tiles:putAttribute name="leftbar" value="/WEB-INF/jsp/frontend/layout/template6/leftbar.jsp" />
  </c:if>
  <tiles:putAttribute name="content" type="string">
  
<div class="col-breadcrumb">
	<ul class="breadcrumb">
		<li><a href="${_contextpath}/account.jhtm">My Account</a></li>
		<li class="active">Quotes</li>
	</ul>
</div>

<div class="col-sm-12">
	<div class="accountQuotesWrapper">
		<div class="headerTitle accountQuotes">My Quote History Information</div>

		<div class="clearfix"></div>

		<c:choose>
		  <c:when test="${model.quotes.pageCount == null or (model.quotes.pageCount == 1 and fn:length(model.quotes.pageList) == 0)}">
		  	<div class="message">Your List is empty.</div>
		  </c:when>
		  <c:otherwise>
			<c:out value="${model.myQuoteHistoryLayout.headerHtml}" escapeXml="false"/>
			
			<c:choose>
			  <c:when test="${model.quotes.pageCount == null or (model.quotes.pageCount == 1 and fn:length(model.quotes.pageList) == 0)}">
				<div class="message">Your List is empty.</div>
			  </c:when>
			  <c:otherwise>
				<div class="row">
				<div class="col-sm-12">
					<div class="pageNavigation">
						<form class="formPageNavigation clearfix" name="pagination">
							<div class="pageNavigationControl">
								<%--
								<div class="pagesize">
									<span>Page Size</span> 
									<select class="selectpicker" name="size" onchange="submit()">
										<c:forTokens items="10,25,50" delims="," var="current">
									  	  <option value="${current}" <c:if test="${current == model.quotes.pageSize}">selected</c:if>>${current} <fmt:message key="perPage" /></option>
									    </c:forTokens>
									</select>
								</div>
								 --%>
								
								<div class="page">
									<span>Page</span> 
									<select class="selectpicker" name="page" onchange="submit()">
										<c:forEach begin="1" end="${model.quotes.pageCount}" var="page">
								  	        <option value="${page}" <c:if test="${page == (model.quotes.page+1)}">selected</c:if>>${page}</option>
										</c:forEach>
									</select>
									<span>of <c:out value="${model.quotes.pageCount}"/></span>
								</div>
								<%--
								<div class="pageNav">
									<span class="pageNavLink"> <a href="#"><span
											class="pageNavPrev"></span></a>
									</span> <span class="pageNavLink"> <a href="#"><span
											class="pageNavNext"></span></a>
									</span>
								</div>
								 --%>
								
							</div>
						</form>
					</div>
				</div>
				</div>
			  </c:otherwise>
			</c:choose>
			<div class="accountQuotes_table">
				<div class="accountQuotesHdr">
					<div class="row clearfix">
						<div class="col-sm-12 col-md-2">
							<div class="listQuoteRequestedHeader">
								<a href="#"><fmt:message key="quoteRequested" /></a>
							</div>
						</div>
						<div class="col-sm-12 col-md-7">
							<div class="listQuoteHeader"><fmt:message key="quote" /></div>
						</div>
						<div class="col-sm-12 col-md-2">
							<div class="listStatusHeader"><fmt:message key="status" /></div>
						</div>
						<div class="col-sm-12 col-md-1">
							<div class="listTotalHeader"><fmt:message key="total" /></div>
						</div>
					</div>
				</div>
	
				<div class="accountQuotesDetails">
					<c:forEach items="${model.quotes.pageList}" var="quote" varStatus="status">
					<div class="odd_row clearfix">
						<div class="col-sm-12 col-md-2">
							<div class="listQuoteRequestedHeader visible-xs visible-sm"><fmt:message key="quoteRequested" /></div>
							<div class="listQuoteRequested"><fmt:formatDate type="date" timeStyle="default" value="${quote.dateOrdered}"/></div>
							<div class="clearfix"></div>
						</div>
						<div class="col-sm-12 col-md-7">
							<div class="listQuoteHeader visible-xs visible-sm"><fmt:message key="quote" /></div>
							<div class="listQuote"><a href="invoice.jhtm?quote=${quote.orderId}" class="nameLink"><c:out value="${quote.orderId}"/></a></div>
							<div class="clearfix"></div>
						</div>
						<div class="col-sm-12 col-md-2">
							<div class="listStatusHeader visible-xs visible-sm"><fmt:message key="status" /></div>
							<div class="listStatus"><fmt:message key="orderStatus_${quote.status}"/></div>
							<div class="clearfix"></div>
						</div>
						<div class="col-sm-12 col-md-1">
							<div class="listTotalHeader visible-xs visible-sm"><fmt:message key="total" /></div>
							<div class="listTotal"><fmt:message key="${siteConfig['CURRENCY'].value}" /><fmt:formatNumber value="${quote.grandTotal}" pattern="#,##0.00" /></div>
							<div class="clearfix"></div>
						</div>
					</div>
					</c:forEach>
				</div>
	
				<div class="accountQuotesFtr">
					<div class="row clearfix"></div>
				</div>
			</div>
		  </c:otherwise>
		</c:choose>


	</div>
</div>
  
<c:out value="${model.myQuoteHistoryLayout.footerHtml}" escapeXml="false"/>

  </tiles:putAttribute>
</tiles:insertDefinition>
