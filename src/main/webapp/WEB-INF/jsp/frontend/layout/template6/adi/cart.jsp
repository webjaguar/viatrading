<%@ page session="false" %>
<%@ page import="com.webjaguar.model.*, java.text.NumberFormat, java.util.*" %>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ page session="false" %>

<%
 NumberFormat nf = NumberFormat.getInstance( );
 nf.setMaximumFractionDigits(2);
 nf.setMinimumFractionDigits(2);
 Map<String, Object> model = (HashMap<String, Object>) request.getAttribute("model");
 Cart cart = (Cart) model.get("cart");
 pageContext.setAttribute("cartSubTotal", nf.format(cart.getSubTotal()));
%>

<tiles:insertDefinition name="${_template}" flush="true">
  <c:if test="${_leftBar != '1' and _leftBar != '4'}">
    <tiles:putAttribute name="leftbar" value="/WEB-INF/jsp/frontend/layout/template6/leftbar.jsp" />
  </c:if>
  <tiles:putAttribute name="content" type="string">
<script type="text/javascript">
<!--
  try {
	$.noConflict();
  }catch(err){}
//-->
</script>

<c:if test="${siteConfig['BUY_SAFE_URL'].value != ''}">
<script type="text/javascript">
<!--
function buySAFEOnClick(NewWantsBondValue) {
	document.cart.WantsBondField.value = NewWantsBondValue;
    document.cart.submit();
}
//-->
</script>
</c:if>

<c:if test="${model.cXmlEncodedCart != null}">
<script type="text/javascript">
<!--
function clearCart() {
	try {
		new Request.HTML ({
			url: "${_contextpath}/clear-ajax-cart.jhtm",
			onSuccess: function(response){
			},
			onFailure: function(response){
			}
		}).send();
	}catch(err){}
	
	// submit cart to punchout buyer. Wait for 1 sec otherwise cart will not be cleared as Chrome blocks this post request
	setTimeout(submitForm,1000);
}
function submitForm() {
	$('checkoutForm').submit();
}
//-->
</script>
</c:if>

<script type="text/javascript">
<!--
function toggleChildSkus(skuGroup) {
	var show = false;
	$$('.'+skuGroup).each(function(ele){
		if(ele.style.display == ''){
			ele.style.display= "none";
			show = true;
			$('toggleBoxId'+skuGroup).set('text', '<fmt:message key="details" />');
		} else {
			ele.style.display= "";
			show = false;
			$('toggleBoxId'+skuGroup).set('text', '<fmt:message key="hide" />');
		}
	});
	if(!show){
		$('parentSkuTotalQtyHdr'+skuGroup).style.display = 'none';
		$('parentSkuTotalPriceHdr'+skuGroup).style.display = 'none';
		$('parentSkuTotalQtyFtr'+skuGroup).style.display = '';
		$('parentSkuTotalPriceFtr'+skuGroup).style.display = '';
	} else {
		$('parentSkuTotalQtyHdr'+skuGroup).style.display = '';
		$('parentSkuTotalPriceHdr'+skuGroup).style.display = '';
		$('parentSkuTotalQtyFtr'+skuGroup).style.display = 'none';
		$('parentSkuTotalPriceFtr'+skuGroup).style.display = 'none';
	}
}
//-->
</script>
<c:if test="${siteConfig['MINI_CART'].value == 'true'}">
<script type="text/javascript">
parent.showCart();
</script>
</c:if>


<div class="col-sm-12">
  
  <c:if test="${fn:trim(model.cartLayout.headerHtml) != ''}">
    <c:set value="${model.cartLayout.headerHtml}" var="headerHtml"/>
    <c:set value="${fn:replace(headerHtml, '#cartsubtotal#', cartSubTotal)}" var="headerHtml"/>
    <c:out value="${headerHtml}" escapeXml="false"/>
  </c:if>
  
  
  
  <div class="shoppingCartWizard clearfix">
	<div class="wizard clearfix">
	  <div class="wizard-bar" style="width: 33.333%;">
		<div class="wizard-step active clearfix">
		  <div class="stepNmbr">1</div>
		  <div class="stepDesc">
			Shopping Cart
		  </div>
		</div>
	  </div>
	  <div class="wizard-bar" style="width: 33.333%;">
		<div class="wizard-step clearfix">
		  <div class="stepNmbr">2</div>
		  <div class="stepDesc">
			Shipping &amp; Payment
		  </div>
		</div>
	  </div>
	  <div class="wizard-bar" style="width: 33.333%;">
		<div class="wizard-step clearfix">
		  <div class="stepNmbr">3</div>
		  <div class="stepDesc">
			Order Confirmation
		  </div>
		</div>
	  </div>
	</div>
	<div class="progress">
	  <div class="progress-bar" style="width: 33.333%;"></div>
	</div>
  </div>

  <div class="cartWrapper">
	<div class="headerTitle shoppingCart"> <fmt:message key="shoppingCart" /> </div>

	<div class="clearfix"></div>

	<c:if test="${!empty model.message}">
	  <div class="message"><fmt:message key="${model.message}" /></div>
	</c:if>
	<c:if test="${!empty model.cart.minOrderMessage}">
	  <div class="message"><fmt:message key="${model.cart.minOrderMessage}" /> <fmt:message key="${siteConfig['CURRENCY'].value}" /><fmt:formatNumber value="${model.cart.minOrderAmt}" pattern="#,##0.00"/></div>
	</c:if>
	<c:if test="${!empty model.cart.minIncMessage}">
	  <div class="message"><fmt:message key="${model.cart.minIncMessage}" /></div>
	</c:if>
	<c:if test="${!empty model.cart.productEventValidationMessage}">
	  <div class="message"><fmt:message key="${model.cart.productEventValidationMessage}" /></div>
	</c:if>
	<c:if test="${model.needSupplierID == true && !empty model.cart.selectSupplierMessage}">
	  <div class="message"><fmt:message key="${model.cart.selectSupplierMessage}" /></div>
	</c:if>
	<c:if test="${model.cart.numberOfItems == 0}">
	  <div class="message"><b><fmt:message key="shoppingcart.empty" /></b></div><br/><br/>
	</c:if>
	
	
	<c:if test="${model.cart.numberOfItems > 0}">
	<form action="viewCart.jhtm" method="post" name="cart">
	<input type="hidden" value="Update" name="_update">
	<div class="shoppingCart_table">
	  <div class="shoppingCartHdr">
		<div class="row clearfix">
		  <div class="col-sm-12 col-md-1">
			<div class="cartRemoveHeader"><fmt:message key="remove" /></div>
		  </div>
		  <div class="col-sm-12 col-md-1">
			<div class="cartImageHeader"><fmt:message key="product" /></div>
		  </div>
		  <div class="col-sm-12 col-md-3">
			<div class="cartNameHeader"></div>
		  </div>
		  <div class="col-sm-12 col-md-2">
			<div class="cartPackingHeader">Availability</div>
		  </div>
		  <div class="col-sm-12 col-md-1">
			<div class="cartQtyHeader"><fmt:message key="qty" /></div>
		  </div>
		  <div class="col-sm-12 col-md-2">
			<div class="cartPriceHeader"><fmt:message key="price" /></div>
		  </div>
		  <div class="col-sm-12 col-md-2">
			<div class="cartTotalHeader"><fmt:message key="total" /></div>
		  </div>
		</div>
	  </div>

	  <c:forEach items="${model.cart.cartItems}" var="cartItem" varStatus="cartStatus">
	    <c:set var="statusClass" value="odd_row" />
	    <c:if test="${(cartStatus.index + 1) % 2 == 0}">
	      <c:set var="statusClass" value="even_row" />
	    </c:if>
	    <c:choose>
		  <c:when test="${cartItem.index > 0}"><c:set var="cartItemIndex" value="${cartItem.index}"/></c:when>
		  <c:otherwise><c:set var="cartItemIndex" value="${cartStatus.index}"/></c:otherwise>
		</c:choose>
	    <c:choose>
		  <c:when test="${cartItem.index > 0}"><c:set var="cartItemIndex" value="${cartItem.index}"/></c:when>
		  <c:otherwise><c:set var="cartItemIndex" value="${cartStatus.index}"/></c:otherwise>
		</c:choose>
	  
	  <div class="shoppingCartDetails">
		<div class="${statusClass} clearfix">
		  <div class="col-sm-12 col-md-1">
			<div class="cartRemoveHeader visible-xs visible-sm"><fmt:message key="remove" /></div>
			<div class="cartRemove">
			  <c:if test="${!cartItem.dealMainItem and (cartItem.itemGroup == null or (cartItem.itemGroup != null and cartItem.itemGroupMainItem))}"><input type="checkbox" name="remove" value="${cartItemIndex}"></c:if>
  			</div>
			<div class="clearfix"></div>
		  </div>
		  
		  <div class="col-sm-12 col-md-1">
			<c:if test="${siteConfig['SHOW_IMAGE_ON_INVOICE'].value == 'true' and cartItem.product.thumbnail != null and (cartItem.itemGroup == null or !cartItem.itemGroupMainItem)}">
		        <div class="cartImageHeader visible-xs visible-sm">&nbsp;</div>
		        <div class="cartImageWrapper">
		          <a href="<c:if test="${!cartItem.product.thumbnail.absolute}">${_contextpath}/assets/Image/Product/detailsbig/</c:if>${cartItem.product.thumbnail.imageUrl}" data-lightbox="product_images_${cartItem.product.id}" data-title="<c:out value="${cartItem.product.name}" escapeXml="false" />">
		            <img class="cartImage img-responsive" src="<c:if test="${!cartItem.product.thumbnail.absolute}">${_contextpath}/assets/Image/Product/thumb/</c:if>${cartItem.product.thumbnail.imageUrl}" border="0" alt="${cartItem.product.alt }"/>
		          </a>
		        </div>
		        <div class="clearfix"></div>
			</c:if>
		  </div>
		  
		  
		  <div class="col-sm-12 col-md-3">
			<div class="cartNameHeader visible-xs visible-sm"><fmt:message key="product" /></div>
			<div class="cartName">
			  
			  <c:set var="productLink">${_contextpath}/product.jhtm?id=${cartItem.product.id}</c:set>
			  <c:if test="${gSiteConfig['gMOD_REWRITE'] == '1' and not fn:contains(cartItem.product.sku, '/')}">
				<c:set var="productLink">${_contextpath}/${siteConfig['MOD_REWRITE_PRODUCT'].value}/${cartItem.product.encodedSku}/${cartItem.product.encodedName}.html</c:set>
			  </c:if>	
      	   	  
      	   	  <div class="cartItemSkuNameWrapper">
				<a href="${productLink}" class="shoppingcart_item_name"> <c:out value="${cartItem.product.name}" escapeXml="false" /> </a>
				<a href="${productLink}" class="shoppingcart_item_sku"><c:out value="${cartItem.product.sku}" escapeXml="false" /></a>
			  	<c:if test="${(cartItem.productAttributes != null  or cartItem.asiProductAttributes != null) and (cartItem.itemGroup == null or !cartItem.itemGroupMainItem)}">
    	  		  <div class="productOptions">
				  <c:forEach items="${cartItem.productAttributes}" var="productAttribute" varStatus="productAttributeStatus">
					<div class="option">
					<c:if test="${(! empty productAttribute.optionName) or (!empty productAttribute.valueString) or (! empty productAttribute.optionPriceOriginal)}">
					  	<span class="optionLabel"><c:out value="${productAttribute.optionName}"/>:</span> 
					  	<span class="optionValue"><c:out value="${productAttribute.valueString}" escapeXml="false"/></span>
						<c:if test="${(! empty productAttribute.optionPriceOriginal) && productAttribute.optionPriceOriginal != 0}">
						  <span class="optionPrice"><fmt:formatNumber value="${productAttribute.optionPriceOriginal}" pattern="#,##0.00" /> </span>
						  <span class="optionPriceMessage"> <fmt:message key="${productAttribute.optionPriceMessageF}"/> </span>
						</c:if>
					</c:if>
					<c:if test="${gSiteConfig['gPRODUCT_EVENT'] and productAttribute.eventType > 0}">
					<div id="productAttribute_${lineItem.lineNumber}">
					  <c:choose>
						<c:when test="${productAttribute.eventType == 1}">
							<input size="10px" class="eventName" type="text" autocomplete="off" value="${productAttribute.eventName}" name="__eventName_update_${productAttribute.id}"/>
							<input size="10px" class="eventZip" type="text" autocomplete="off" value="${productAttribute.zipCode}" name="__eventZipCode_update_${productAttribute.id}"/>
							<input size="10px" class="eventDate" type="text" autocomplete="off" value="${productAttribute.date}" name="__eventDate_update_${productAttribute.id}"/>
						</c:when>
						<c:when test="${productAttribute.eventType == 2}">
							<input size="10px" type="text" autocomplete="off" value="${productAttribute.zipCode}" name="__eventZipCode_update_${productAttribute.id}"/>
						</c:when>
						<c:when test="${productAttribute.eventType == 3}">
							<input size="10px" type="text" autocomplete="off" value="${productAttribute.eventName}" name="__eventName_update_${productAttribute.id}"/>
						</c:when>
					  </c:choose>
					</div>
					</c:if>
					</div>
	  	  		  </c:forEach>
	  	  		  <c:if test="${siteConfig['SHOW_IMAGE_ON_INVOICE'].value == 'true' and fn:length(cartItem.productAttributes) > 0}">
				    <c:forEach items="${cartItem.productAttributes}" var="productAttribute">
				      <c:if test="${!empty productAttribute.imageUrl}" >
				      <div class="option">
				            <span class="optionLabel"><c:out value="${productAttribute.optionName}" /> Image:</span>
				            <c:choose>
						      <c:when test="${productAttribute.absolute}">
						  	    <span class="optionValue"><img class="cartOptionImage" src="${productAttribute.imageUrl}" border="0" /></span>
						      </c:when>
						      <c:otherwise>
						   	 	<span class="optionValue"><img src="${_contextpath}/assets/Image/Product/options/<c:out value="${productAttribute.imageUrl}"/>" alt="Product option" class="cartOptionImage" /></span>
						      </c:otherwise>
						    </c:choose>
						    <span>( <c:out value="${fn:split(productAttribute.imageUrl, '/')[fn:length(fn:split(productAttribute.imageUrl, '/')) - 1]}"></c:out> )</span>
				      </div>
				      </c:if>
				    </c:forEach>
				  </c:if>
			  	  <c:forEach items="${cartItem.asiProductAttributes}" var="asiProductAttribute" varStatus="asiProductAttributeStatus">
					<div class="option">
						<span class="optionLabel"><c:out value="${asiProductAttribute.asiOptionName}"/>:</span> 
						<span class="optionValue"><c:out value="${asiProductAttribute.asiOptionValue}" escapeXml="false"/></span>
					</div>
				  </c:forEach>
				  </div>
			  	</c:if>
			  	<%--Show brand always for ADI Customer --%>
			  	<c:if test="${model.cart.hasManufacturer or gSiteConfig['gADI']}"> 	
	          		<div class="shoppingcart_item_brand"><c:out value="${cartItem.product.manufactureName}" escapeXml="false"/></div>
			  	</c:if>
			  	<c:if test="${gSiteConfig['gMINIMUM_INCREMENTAL_QTY'] and cartItem.showMinIncMessage and !empty cartItem.minimumIncrementalQtyMessage}">
			        <c:set value="${siteConfig['MINIMUM_MESSAGE'].value}" var="minMessage"/>
			        <c:set value="${fn:replace(minMessage, '#min#', cartItem.product.minimumQty)}" var="minMessage"/>
			        <c:set value="${siteConfig['INCREMENTAL_MESSAGE'].value}" var="incMessage"/>
			        <c:set value="${fn:replace(incMessage, '#inc#', cartItem.product.incrementalQty)}" var="incMessage"/>
			        <c:if test="${cartItem.priceCasePackQty == null}">
			          <div class="message"><c:if test="${!empty cartItem.product.minimumQty}" >&nbsp; <c:out value="${minMessage}" escapeXml="false"/></c:if><c:if test="${!empty cartItem.product.incrementalQty}" ><c:out value="${incMessage}" escapeXml="false"/></c:if></div>
			        </c:if>
			    </c:if>
			    <c:if test="${!empty cartItem.inventoryMessage}">
					<div class="message"><c:out value="${cartItem.inventoryMessage}" /></div>
			    </c:if>
			
			  </div>
			</div>
			<div class="clearfix"></div>
		  </div>
    	  <div class="col-sm-12 col-md-2">
		  	<div class="cartPackingHeader visible-xs visible-sm">Availability</div>
		  	<div class="cartPacking">
		  	<c:choose>
		  		<c:when test="${cartItem.product.inventory > 0}">
		  			IN STOCK
		  		</c:when>
		  		<c:when test="${cartItem.product.negInventory && (cartItem.product.inventory == 0 || cartItem.product.inventory < 0)}">
		  			SPECIAL ORDER
		  		</c:when>
		  		<c:otherwise>
		  		</c:otherwise>
		  	</c:choose>
		  	</div>
		  	<div class="clearfix"></div>
	      </div>
		  <div class="col-sm-12 col-md-1">
			<div class="cartQtyHeader visible-xs visible-sm"><fmt:message key="qty" /></div>
			<c:set var="errorMessage" />
			<c:if test="${!empty cartItem.inventoryMessage}">
				<c:set var="errorMessage" value="true"/>
			</c:if>
			<c:if test="${gSiteConfig['gMINIMUM_INCREMENTAL_QTY'] and cartItem.showMinIncMessage and !empty cartItem.minimumIncrementalQtyMessage}">
				<c:set var="errorMessage" value="true"/>
			</c:if>
			
			<div class="cartQty <c:if test="${errorMessage}">has-error</c:if>">
			  <input type="hidden" name="index" value="<c:out value="${cartItem.index}"/>">
      		  <input type="hidden" name="pid_${cartItem.index}" value="${cartItem.product.id}">
     		  <input type="text" autocomplete="off" size="4" maxlength="5" name="qty_${cartItemIndex}" value="${cartItem.quantity}"  <c:if test="${cartItem.asiProductAttributes != null or (cartItem.product.feed == 'ASI') or (cartItem.itemGroup != null and !cartItem.itemGroupMainItem)}">readonly="readonly"</c:if>>
	        </div>
			<div class="clearfix"></div>
		  </div>
		  <div class="col-sm-12 col-md-2">
			<div class="cartPriceHeader visible-xs visible-sm"><fmt:message key="price" /></div>
			<div class="cartPrice"><fmt:message key="${siteConfig['CURRENCY'].value}" /><fmt:formatNumber value="${cartItem.unitPrice}" pattern="#,##0.00#" maxFractionDigits="${siteConfig['MAX_FRACTION_DIGIT'].value}"/> <c:out value="${cartItem.product.packing}" escapeXml="false" /></div>
			<div class="clearfix"></div>
		  </div>
		  <div class="col-sm-12 col-md-2">
			<div class="cartTotalHeader visible-xs visible-sm"><fmt:message key="total" /></div>
			<div class="cartTotal"><fmt:message key="${siteConfig['CURRENCY'].value}" /><fmt:formatNumber value="${cartItem.total}" pattern="#,##0.00#" maxFractionDigits="${siteConfig['MAX_FRACTION_DIGIT'].value}"/></div>
			<div class="clearfix"></div>
		  </div>
    	  
		</div>
	  </div>
      </c:forEach>
      
	  <div class="shoppingCartFtr">
		<div class="row clearfix">
		<div class="col-sm-1">
		 <div class="cartUpdateFooter">
		 	 <button class="cartUpdate_btn btn btn-link" type="submit" name="_update"><fmt:message key="update" /></button>
		  </div>
		</div>
		  <div class="col-sm-8">
			<div class="cartWeightFooter">
			  <c:if test="${!gSiteConfig['gADI']}"> 	
	          	<fmt:message key="totalWeight" />: <fmt:formatNumber value="${model.cart.weight}" pattern="#,##0.00"/>
			  </c:if>
			</div>
		  </div>
		  <div class="col-sm-3">
			<div class="cartSubtotalFooter">
			  <fmt:message key="subTotal" />: <fmt:message key="${siteConfig['CURRENCY'].value}" /><fmt:formatNumber value="${model.cart.subTotal}" pattern="#,##0.00"/>
			</div>
		  </div>
		</div>
	  </div>
	</div>

	
	<div class="row">
		<div class="col-sm-6">
			<div class="continueShopping_btn_wrapper clearfix">
			<a class="continueShopping_btn btn" href="javascript:history.back(1);"><fmt:message key="continueShopping" /></a>
			</div>
		</div>
		<div class="col-sm-6">
			<div class="proceedCheckout_btn_wrapper clearfix">
			<c:choose>
		  	  <c:when test="${gSiteConfig['gCHECKOUT_MODE'] == '1'}">
		  		<button type="submit" class="proceedCheckout_btn btn" border="0" name="_checkout.x">Checkout</button>
			  </c:when>
			  <c:when test="${gSiteConfig['gCHECKOUT_MODE'] == '2'}">
		        <button type="submit" class="proceedCheckout_btn btn" border="0" name="_checkout1.x">Checkout</button>
			  </c:when>
			  <c:when test="${gSiteConfig['gCHECKOUT_MODE'] == '3'}">
		        <button type="submit" class="proceedCheckout_btn btn" border="0" name="_checkout3.x">Checkout</button>
			  </c:when>
			  <c:when test="${gSiteConfig['gCHECKOUT_MODE'] == '4' && siteConfig['SHIPPING_PER_SUPPLIER'].value == 'true'}">
		  		<button type="submit" class="proceedCheckout_btn btn" border="0" name="_checkout4.x">Checkout</button>
			  </c:when>
			  <c:otherwise>
		        <button type="submit" class="proceedCheckout_btn btn" border="0" name="_checkout1.x">Checkout</button>
		  	  </c:otherwise>
			</c:choose>
			</div>
		</div>
	</div>
									
	
	
	

    
    <c:if test="${gSiteConfig['gSALES_PROMOTIONS'] and siteConfig['PROMO_ON_SHOPPING_CART'].value == 'true'}">
	  <div id="promoContainer">
	    <label for="promoCodeInput" class="promoTitle"><fmt:message key="f_enterPromoCode"/>:</label>
	    <input type="text" autocomplete="off" id="promoCodeInput" name="promoCode" value="${model.cart.promoCode}" />
	    <div class="promo_btn_wrapper">
		  <button type="submit" class="btn promo_btn">Apply Promo Code</button>
	    </div>
	    <c:if test="${model.cart.tempPromoErrorMessage != null}"><span class="error"><fmt:message key="${model.cart.tempPromoErrorMessage}"/></span></c:if>
	  </div>  
	</c:if>
    </form>
    
    </c:if>
    
  </div>
  
  <c:if test="${( (gSiteConfig['gRECOMMENDED_LIST'] and siteConfig['SHOPPING_CART_RECOMMENDED_LIST'].value == 'true') or (siteConfig['UPSELL_RECOMMENDED_LIST'].value == 'true')) and fn:length(model.recommendedList)!=0 }">
  <div class="row">
	<div class="col-sm-12">
		<div class="recommendedListWrapper">
			<div class="recommendedListHdr">	
			<c:choose>
 				<c:when test="${!empty product.recommendedListTitle }">
  					<div class="recommended_list"><c:out value="${product.recommendedListTitle}"/></div>
 				</c:when>
 				<c:otherwise>
  					<c:if test="${siteConfig['RECOMMENDED_LIST_TITLE'].value != ''}">
   						<div class="recommended_list"><c:out value="${siteConfig['RECOMMENDED_LIST_TITLE'].value}"/></div>
 			 		</c:if>
 				</c:otherwise>
			</c:choose>	
			</div>
			<div class="recommendedListDetails">
				
				<c:set var="numCul" value="${siteConfig['NUMBER_PRODUCT_PER_ROW'].value}" />
				<c:forEach items="${model.recommendedList}" var="product" varStatus="status">
				
				<c:set var="productLink">${_contextpath}/product.jhtm?id=${product.id}<c:if test="${model.product == null or siteConfig['RECOMMENDED_LIST_SHOW_BREADCRUMB'].value == 'true'}">&cid=<c:out value="${param.cid}"/></c:if></c:set>
				<c:if test="${gSiteConfig['gMOD_REWRITE'] == '1' and not fn:contains(product.sku, '/')}">
  					<c:set var="productLink">${_contextpath}/${siteConfig['MOD_REWRITE_PRODUCT'].value}/${product.encodedSku}/${product.encodedName}.html<c:if test="${model.product == null or siteConfig['RECOMMENDED_LIST_SHOW_BREADCRUMB'].value == 'true'}">?cid=<c:out value="${param.cid}"/></c:if></c:set>
				</c:if>	
				
				<div <c:choose><c:when test="${status.index % 2 == 0}">class="odd_row clearfix"</c:when><c:otherwise>class="even_row clearfix"</c:otherwise></c:choose>>
						<%@ include file="/WEB-INF/jsp/frontend/thumbnail/view28.jsp" %>
				</div>
				</c:forEach>	
			 </div>
			</div>
		</div>
	</div>
  </div>
</c:if>

<c:if test="${fn:trim(model.cartLayout.footerHtml) != ''}">
  <c:set value="${model.cartLayout.footerHtml}" var="footerHtml"/>
  <c:set value="${fn:replace(footerHtml, '#cartsubtotal#', cartSubTotal)}" var="footerHtml"/>
  <c:out value="${footerHtml}" escapeXml="false"/>
</c:if>
  
  
</div>
  </tiles:putAttribute>
</tiles:insertDefinition>
