<%@ page session="false" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="/WEB-INF/tlds/custom.tld" prefix="wj" %>

<c:choose>
  <c:when test="${gSiteConfig['gPRODUCT_FIELDS_UNLIMITED'] > 0}">
	<c:forEach items="${model.fieldGroupMap}" var="fieldGroup" varStatus="status"> 
	  <div class="details_specification">
	    <c:if test="${fieldGroup.key != null and fn:length(fieldGroup.key) > 0}">
	    <div class="spec_group_title_row clearfix">
		  <div class="spec_group_title">
		    <c:out value="${fieldGroup.key}" escapeXml="false" />
		  </div>
	    </div>
	    </c:if>
	  <c:forEach items="${fieldGroup.value}" var="productFielUnlimitedNameValue" varStatus="status"> 
	    <div <c:choose><c:when test="${status.index % 2 == 0}">class="even_row clearfix"</c:when><c:otherwise>class="odd_row clearfix"</c:otherwise></c:choose>>
		<div class="spec_title">
		  <c:choose>
		    <c:when test="${productFielUnlimitedNameValue.displayName != null and productFielUnlimitedNameValue.displayName != ''}">
		      <c:out value="${productFielUnlimitedNameValue.displayName}" escapeXml="false" />
			</c:when>
		    <c:otherwise>
		      <c:out value="${productFielUnlimitedNameValue.name}" escapeXml="false" />
			</c:otherwise>
		  </c:choose>
		</div>
		<div class="spec_info">
		  <c:out value="${productFielUnlimitedNameValue.displayValue}" escapeXml="false"/>
		</div>
	  </div>
	  </c:forEach>
	  </div>
	  <div class="clearfix"><br></div>
	  
	</c:forEach>
	
	<%--
	<c:forEach items="${product.productFieldsUnlimitedNameValue}" var="productFielUnlimitedNameValue" varStatus="status"> 
	  <div <c:choose><c:when test="${status.index % 2 == 0}">class="even_row clearfix"</c:when><c:otherwise>class="odd_row clearfix"</c:otherwise></c:choose>>
		<div class="spec_title">
		  <c:choose>
		    <c:when test="${productFielUnlimitedNameValue.displayName != null and productFielUnlimitedNameValue.displayName != ''}">
		      <c:out value="${productFielUnlimitedNameValue.displayName}" escapeXml="false" />
			</c:when>
		    <c:otherwise>
		      <c:out value="${productFielUnlimitedNameValue.name}" escapeXml="false" />
			</c:otherwise>
		  </c:choose>
		</div>
		<div class="spec_info">
		  <c:out value="${productFielUnlimitedNameValue.displayValue}" escapeXml="false"/>
		</div>
	  </div>
	</c:forEach>
     --%>
	
  </c:when>
  <c:otherwise>
	<c:forEach items="${product.productFields}" var="productField" varStatus="status">
	  <div <c:choose><c:when test="${status.index % 2 == 0}">class="even_row clearfix"</c:when><c:otherwise>class="odd_row clearfix"</c:otherwise></c:choose>>
		<div class="spec_title">
		  <c:out value="${productField.name}" escapeXml="false" />
		</div>
		<div class="spec_info">
		  <c:out value="${productField.value}" escapeXml="false"/>
		</div>
	  </div>
	</c:forEach>
  </c:otherwise>
</c:choose>