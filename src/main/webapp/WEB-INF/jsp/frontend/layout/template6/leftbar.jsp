<%@ page session="false" %>
<%@ page import="com.webjaguar.model.Category,com.webjaguar.model.CategoryLinkType,java.util.*" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

								
<c:out value="${layout.leftBarTopHtml}" escapeXml="false"/>

<c:if test="${gSiteConfig['gSEARCH'] == 'triguide'}">
<c:import url="/WEB-INF/jsp/frontend/triguide/search.jsp" />
</c:if>

<c:if test="${gSiteConfig['gSHARED_CATEGORIES'] != ''}">
<c:set var="sharedCategories">
<c:catch var="exception">
<c:import url="${gSiteConfig['gSHARED_CATEGORIES']}sharedCategoryLinks.jsp">
  <c:param name="sid" value="${paramValues.sid[0]}" />
  <c:param name="sid" value="${paramValues.sid[1]}" />
  <c:param name="keyword" value="true" />
  <c:param name="field_12" value="${param.field_12}" />
</c:import>
</c:catch>
</c:set>
</c:if>

<c:if test="${siteConfig['SHARED_CATEGORIES_POSITION'].value == '1'}">
  <c:out value="${sharedCategories}" escapeXml="false" />
</c:if>
 
<c:if test="${(model != null) && (model.mainCategories != null)}">
<c:set value="${model.mainCategories}" var="mainCategories"/>
<c:set value="${gSiteConfig['gMOD_REWRITE']}" var="gMOD_REWRITE"/>
<c:set value="${siteConfig['MOD_REWRITE_CATEGORY'].value}" var="MOD_REWRITE_CATEGORY"/>
<c:set value="${_leftBar}" var="LEFTBAR_DEFAULT_TYPE"/>
<div class="leftbar_catlinks_wrapper">
  <% 
	if (pageContext.getAttribute("LEFTBAR_DEFAULT_TYPE").equals("4")) {
		buildTree4((ArrayList<Category>) pageContext.getAttribute("mainCategories"), 0, out, (String) request.getAttribute("_contextpath"), (String) pageContext.getAttribute("gMOD_REWRITE"), (String) pageContext.getAttribute("MOD_REWRITE_CATEGORY"), 0);
	} else {
		buildTree((ArrayList<Category>) pageContext.getAttribute("mainCategories"), 0, out, (String) request.getAttribute("_contextpath"), (String) pageContext.getAttribute("gMOD_REWRITE"), (String) pageContext.getAttribute("MOD_REWRITE_CATEGORY"));	
	}
  %>
</div>
</c:if>

<c:if test="${siteConfig['SHARED_CATEGORIES_POSITION'].value == '2'}">
<c:out value="${sharedCategories}" escapeXml="false" />
</c:if>

<c:out value="${layout.leftBarBottomHtml}" escapeXml="false"/>

<%!
void buildTree(List<Category> categories, int level, JspWriter out, String _contextpath, String gMOD_REWRITE, String MOD_REWRITE_CATEGORY) throws Exception {
	
	for (Category thisCategory: categories) {			
		
		if (!thisCategory.getLinkType().equals(CategoryLinkType.NONLINK)) {
			out.println("<div id=\"cid" + thisCategory.getId() + "\">");
			out.print("<a href=\"" + _contextpath + "/");
			if (gMOD_REWRITE != null && gMOD_REWRITE.equals("1")) {
				out.print(MOD_REWRITE_CATEGORY + "/" + thisCategory.getId() + "/" + thisCategory.getEncodedName() + ".html");				
			} else {
				out.print("category.jhtm?cid=" + thisCategory.getId());
			}
			out.print("\"");			
			if (thisCategory.getUrlTarget() != null && thisCategory.getUrlTarget().equals("_blank")) {
				out.print(" target=\"_blank\"");
			}
			if (!thisCategory.isHasLinkImage()) {
				out.print(" class=\"leftbar_");
				if (level > 2) {
					out.print("sub" + level);
				} else {
					for (int i=0; i<level; i++) out.print("sub");				
				}
				out.print("catLink");
				if (thisCategory.isSelected()) {
					out.print("_selected");					
				}
				out.print("\"");
			}
			out.print(">");				
		} 
		if (thisCategory.isHasLinkImage()) {
			out.print("<img src=\"" + _contextpath + "/assets/Image/Category/catlink_");
	  		if (thisCategory.isSelected() && thisCategory.isHasLinkImageOver()) {
	  			out.print("over_");
	  		}
  			out.print(thisCategory.getId() + ".gif\"");
	  		if (!thisCategory.isSelected() && thisCategory.isHasLinkImageOver()) {
	  			out.print(" onMouseOver=\"this.src='" + _contextpath + "/assets/Image/Category/catlink_over_" + thisCategory.getId() + ".gif'\"");
	  			out.print(" onMouseOut=\"this.src='" + _contextpath + "/assets/Image/Category/catlink_" + thisCategory.getId() + ".gif'\"");
	  		}
	  		out.print(" border=\"0\">");
		} else {
			if(thisCategory.getLinkType().equals(CategoryLinkType.NONLINK)) {
				out.println("<div class=\"leftbar_catLink_nonlink\">" + thisCategory.getName());
			} else {
				out.print(thisCategory.getName());
			}
		}
		if (!thisCategory.getLinkType().equals(CategoryLinkType.NONLINK)) {
			out.print("</a>");	
		}
		out.print("</div>");	
			
		buildTree(thisCategory.getSubCategories(), level+1, out, _contextpath, gMOD_REWRITE, MOD_REWRITE_CATEGORY);				
   
	
	}
}

void buildTree4(List<Category> categories, int level, JspWriter out, String _contextpath, String gMOD_REWRITE, String MOD_REWRITE_CATEGORY, int cid) throws Exception {	
	out.print("<ul id=\"ul_cid_" + cid + "\" class=\"level-" + level);
	if (level == 0) out.print(" cssmw-left");
	out.println("\">");
	for (Category thisCategory: categories) {
		out.print("<li class=\"cat\" id=\"li_cid_" + thisCategory.getId() + "\"><span>");
		if (!thisCategory.getLinkType().equals(CategoryLinkType.NONLINK)) {
			out.print("<a href=\"" + _contextpath + "/");
			if (gMOD_REWRITE != null && gMOD_REWRITE.equals("1")) {
				out.print(MOD_REWRITE_CATEGORY + "/" + thisCategory.getId() + "/" + thisCategory.getEncodedName() + ".html");				
			} else {
				out.print("category.jhtm?cid=" + thisCategory.getId());
			}				
			out.print("\"");
			if (thisCategory.getUrlTarget() != null && thisCategory.getUrlTarget().equals("_blank")) {
				out.print(" target=\"_blank\"");
			}
			out.print(">");
		}
		out.print(thisCategory.getName());
		if (!thisCategory.getLinkType().equals(CategoryLinkType.NONLINK)) {
			out.print("</a>");	
		}
		out.print("</span>");
		if (thisCategory.getSubCategories().size() > 0) {
			buildTree4(thisCategory.getSubCategories(), level+1, out, _contextpath, gMOD_REWRITE, MOD_REWRITE_CATEGORY, thisCategory.getId());							
		}
		out.println("</li>");
   	}
	out.println("</ul>");
}
%>