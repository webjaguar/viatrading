<%@ page session="false" %>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>

<tiles:insertDefinition name="${_template}" flush="true">
  <c:if test="${_leftBar != '1' and _leftBar != '4'}">
    <tiles:putAttribute name="leftbar" value="/WEB-INF/jsp/frontend/layout/template${_leftBar}/leftbar.jsp" />
  </c:if>
  <tiles:putAttribute name="content" type="string">


<c:if test="${gSiteConfig['gPRODUCT_REVIEW'] and model.product.enableRate!=false}">

<c:out value="${model.productReviewLayout.headerHtml}" escapeXml="false"/>

<div class="col-sm-12">
	<form:form commandName="productReviewForm" method="post">
		<input type="hidden" name="_page" value="1">
		<div class="reviewList">
			<div class="row">
				<div class="col-sm-12">
					<div class="review">
						<div class="reviewRate">
							<div class="product_rating">
								<div class="rating_stars">
									
									<c:choose>
								     <c:when test="${productReviewForm.productReview.rate == 0}"><img src="${_contextpath}/assets/Image/Layout/star_0.gif" alt=<c:out value="${product.rate.average}"/> title="<c:out value="${product.rate.average}"/>" border="0" /></c:when>
								     <c:when test="${productReviewForm.productReview.rate >= 1 and productReviewForm.productReview.rate < 2}"><img src="${_contextpath}/assets/Image/Layout/star_1.gif" alt=<c:out value="${productReviewForm.productReview.rate}"/> title="<c:out value="${productReviewForm.productReview.rate}"/>" border="0" /></c:when>
								     <c:when test="${productReviewForm.productReview.rate >= 2 and productReviewForm.productReview.rate < 3}"><img src="${_contextpath}/assets/Image/Layout/star_2.gif" alt=<c:out value="${productReviewForm.productReview.rate}"/> title="<c:out value="${productReviewForm.productReview.rate}"/>" border="0" /></c:when>
								     <c:when test="${productReviewForm.productReview.rate >= 3 and productReviewForm.productReview.rate < 4}"><img src="${_contextpath}/assets/Image/Layout/star_3.gif" alt=<c:out value="${productReviewForm.productReview.rate}"/> title="<c:out value="${productReviewForm.productReview.rate}"/>" border="0" /></c:when>
								     <c:when test="${productReviewForm.productReview.rate >= 4 and productReviewForm.productReview.rate < 5}"><img src="${_contextpath}/assets/Image/Layout/star_4.gif" alt=<c:out value="${productReviewForm.productReview.rate}"/> title="<c:out value="${productReviewForm.productReview.rate}"/>" border="0" /></c:when>
								     <c:when test="${productReviewForm.productReview.rate == 5}"><img src="${_contextpath}/assets/Image/Layout/star_5.gif" alt=<c:out value="${productReviewForm.productReview.rate}"/> title="<c:out value="${productReviewForm.productReview.rate}"/>" border="0" /></c:when>
								   </c:choose>
									<%--
									<ul class="rating">
										<li id="1" class="rate one"><span title="1 Star"></span>
										</li>
										<li id="2" class="rate two"><span title="2 Stars"></span>
										</li>
										<li id="3" class="rate three"><span title="3 Stars"></span>
										</li>
										<li id="4" class="rate four"><span title="4 Stars"></span>
										</li>
										<li id="5" class="rate five"><span title="5 Stars"></span>
										</li>
									</ul>
									 --%>
									
								</div>
								<div class="clearfix"></div>
							</div>
						</div>
						<div class="reviewTitle"><c:out value="${productReviewForm.productReview.title}" /></div>
						<div class="clearfix"></div>
						<div class="reviewer">By: <c:out value="${productReviewForm.productReview.userName}" /></div>
						<div class="reviewDate"><fmt:formatDate type="date" pattern="MM/dd/yyyy" value="${model.now}"/></div>
						<div class="reviewText"><c:out value="${productReviewForm.productReview.text}" escapeXml="false"/></div>
					</div>
				</div>
			</div>
		</div>

		<div id="form_buttons">
			<div class="row">
				<div class="col-sm-12">
					<div id="buttons_wrapper">
						<button type="submit" name="_target0" class="btn btn-default">Back</button>
						<button type="submit" name="_finish" class="btn btn-default">Submit</button>
					</div>
				</div>
			</div>
		</div>
	</form:form>
</div>





<c:out value="${model.productReviewLayout.footerHtml}" escapeXml="false"/>
</c:if>
  </tiles:putAttribute>
</tiles:insertDefinition>
