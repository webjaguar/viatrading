<%@ page session="false" %>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="/WEB-INF/tlds/spring.tld" prefix="spring" %>
<%@ page contentType="text/html; charset=ISO-8859-1" session="false" %>

<script src="/dv/w3data.js"></script>
<script type='text/javascript' src='/wp-includes/js/jquery/jquery.js?ver=1.12.4'></script>
<script type='text/javascript' src='/wp-content/themes/bridge/js/plugins.js?ver=4.6.1'></script>
<script type='text/javascript' src='/wp-content/themes/bridge/js/default.min.js?ver=4.6.1'></script>

<tiles:insertDefinition name="${_template}" flush="true">
  <c:if test="${_leftBar != '1' and _leftBar != '4'}">
    <tiles:putAttribute name="leftbar" value="/WEB-INF/jsp/frontend/layout/template${_leftBar}/leftbar.jsp" />
  </c:if>
  <tiles:putAttribute name="content" type="string">
  
<%@ include file="/WEB-INF/jsp/custom/step3-1.jsp" %>  

<c:out value="${model.verifyAccount.headerHtml}" escapeXml="false"/>



<div class="col-sm-12">
	<div class="message"></div>
		<form id="verify_registration_form" class="form-horizontal" role="form" method="POST" action="#" enctype="multipart/form-data">
			<input type="hidden" name="_page" value="2">
			<div id="registration_information">
				<div class="row">
					<div class="col-sm-6">
						<div class="form-group">
							<div class="col-sm-12">
								<h3>Verify Information</h3>
							</div>
						</div>
						<div class="form-group">
							<div class="col-sm-4">
								<label class="control-label"><fmt:message key="email" /></label>
							</div>
							<div class="col-sm-8">
								<p class="form-control-static"><c:out value="${customerForm.customer.username}"/></p>
							</div>
						</div>
						<div class="form-group">
							<div class="col-sm-4">
								<label class="control-label"><fmt:message key="firstName" /></label>
							</div>
							<div class="col-sm-8">
								<p class="form-control-static"><c:out value="${customerForm.customer.address.firstName}"/></p>
							</div>
						</div>
						<div class="form-group">
							<div class="col-sm-4">
								<label class="control-label"><fmt:message key="lastName" /></label>
							</div>
							<div class="col-sm-8">
								<p class="form-control-static"><c:out value="${customerForm.customer.address.lastName}"/></p>
							</div>
						</div>
						<div class="form-group">
							<div class="col-sm-4">
								<label class="control-label"><fmt:message key="company" /></label>
							</div>
							<div class="col-sm-8">
								<p class="form-control-static"><c:out value="${customerForm.customer.address.company}"/></p>
							</div>
						</div>
						<div class="form-group">
							<div class="col-sm-4">
								<label class="control-label"><fmt:message key="address" /></label>
							</div>
							<div class="col-sm-8">
								<p class="form-control-static">
									<c:out value="${customerForm.customer.address.addr1}"/> <c:out value="${customerForm.customer.address.addr2}"/>
									<br />
									<c:out value="${customerForm.customer.address.city}"/>, <c:out value="${customerForm.customer.address.stateProvince}"/>
    								<br />
									<c:out value="${countries[customerForm.customer.address.country]}" />
								</p>
							</div>
						</div>
						<div class="form-group">
							<div class="col-sm-4">
								<label class="control-label"><fmt:message key="zipCode" /></label>
							</div>
							<div class="col-sm-8">
								<p class="form-control-static"><c:out value="${customerForm.customer.address.zip}"/></p>
							</div>
						</div>
						<div class="form-group">
							<div class="col-sm-4">
								<label class="control-label"><fmt:message key="phone" /></label>
							</div>
							<div class="col-sm-8">
								<p class="form-control-static"><c:out value="${customerForm.customer.address.phone}"/></p>
							</div>
						</div>
						<div class="form-group">
							<div class="col-sm-4">
								<label class="control-label"><fmt:message key="cellPhone" /></label>
							</div>
							<div class="col-sm-8">
								<p class="form-control-static"><c:out value="${customerForm.customer.address.cellPhone}"/></p>
							</div>
						</div>
						<div class="form-group">
							<div class="col-sm-4">
								<label class="control-label"><fmt:message key="fax" /></label>
							</div>
							<div class="col-sm-8">
								<p class="form-control-static"><c:out value="${customerForm.customer.address.fax}"/></p>
							</div>
						</div>
						<c:if test="${gSiteConfig['gTAX_EXEMPTION']}">
						<div class="form-group">
							<div class="col-sm-4">
								<label class="control-label"><fmt:message key="taxId" /></label>
							</div>
							<div class="col-sm-8">
								<p class="form-control-static"><c:out value="${customerForm.customer.taxId}"/></p>
							</div>
						</div>
						</c:if>  
						
					</div>
					<div class="col-sm-6"></div>
				</div>
			</div>
			
			<div id="form_buttons">
				<div class="row">
					<div class="col-sm-12">
						<div id="buttons_wrapper">
							<button type="submit" name="_target0" class="btn btn-default">Change</button>
							<button type="submit" name="_finish" class="btn btn-default">Register</button>
						</div>
					</div>
				</div>
			</div>
		</form>
									
	</div>
</div>

<c:out value="${model.verifyAccount.footerHtml}" escapeXml="false"/>

<%@ include file="/WEB-INF/jsp/custom/step3-2.jsp" %>

  </tiles:putAttribute>
</tiles:insertDefinition>
