<%@ page session="false" %>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="/WEB-INF/tlds/spring.tld" prefix="spring" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>

<tiles:insertDefinition name="${_template}" flush="true">
  <c:if test="${_leftBar != '1' and _leftBar != '4'}">
    <tiles:putAttribute name="leftbar" value="/WEB-INF/jsp/frontend/layout/template${_leftBar}/leftbar.jsp" />
  </c:if>
  <tiles:putAttribute name="content" type="string">

<div class="col-breadcrumb">
	<ul class="breadcrumb">
		<li><a href="${_contextpath}/account.jhtm">My Account</a></li>
		<li class="active">Shipping Address</li>
	</ul>
</div>

<c:out value="${model.shippingLayout.headerHtml}" escapeXml="false"/>

<div class="col-sm-12">
	<div id="shipping_billing_addressWrapper">
		<div class="shipping_billing_address">
			<h3><fmt:message key="addressBook" /></h3>
			<c:forEach items="${model.addressBook}" var="address">
			<div class="address_row">
    		 <div class="row">
				<div class="col-sm-8">
					<div class="address">
						<b><c:out value="${address.firstName}"/> <c:out value="${address.lastName}"/></b> 
						<c:if test="${address.primary}">
							<span class="primary_address_small">(Primary Address)</span> 
						</c:if>
						<c:if test="${address.company != ''}"><c:out value="${address.company}"/>,</c:if>
						<c:out value="${address.addr1}"/><c:if test="${!empty address.addr2}"> <c:out value="${address.addr2}"/></c:if>,
						<c:out value="${address.city}"/>, <c:out value="${address.stateProvince}"/> <c:out value="${address.zip}"/>,<c:out value="${countries[address.country]}" /> <br><br>
					</div>
				</div>
				<div class="col-sm-4">
					<div class="button_wrapper">
						<form  action="checkout1.jhtm" method="post">
							<button type="submit" name="_target2" class="btn btn-default"><fmt:message key="shoppingcart.shipToThisAddress" /></button>
							<input type="hidden" name="newShippingAddress" value="false">
							<input type="hidden" name="order.shipping.id" value="<c:out value="${address.id}"/>">
							<input type="hidden" name="order.shipping.lastName" value="<c:out value="${address.lastName}"/>"/>
							<input type="hidden" name="order.shipping.firstName" value="<c:out value="${address.firstName}"/>"/>
							<input type="hidden" name="order.shipping.company" value="<c:out value="${address.company}"/>"/>
							<input type="hidden" name="order.shipping.addr1" value="<c:out value="${address.addr1}"/>"/>
							<input type="hidden" name="order.shipping.addr2" value="<c:out value="${address.addr2}"/>"/>
							<input type="hidden" name="order.shipping.city" value="<c:out value="${address.city}"/>"/>
							<input type="hidden" name="order.shipping.stateProvince" value="<c:out value="${address.stateProvince}"/>"/>
							<input type="hidden" name="order.shipping.zip" value="<c:out value="${address.zip}"/>"/>
							<input type="hidden" name="order.shipping.country" value="<c:out value="${address.country}"/>"/>
							<input type="hidden" name="order.shipping.phone" value="<c:out value="${address.phone}"/>"/>
							<input type="hidden" name="order.shipping.cellPhone" value="<c:out value="${address.cellPhone}"/>"/>
							<input type="hidden" name="order.shipping.fax" value="<c:out value="${address.fax}"/>"/>
							<input type="hidden" name="order.shipping.residential" value="<c:out value="${address.residential}"/>"/>
							<input type="hidden" name="order.shipping.liftGate" value="<c:out value="${address.liftGate}"/>"/>
							<input type="hidden" name="order.shipping.code" value="<c:out value="${address.code}"/>">
						</form>
					</div>
				</div>
			  </div>
			</div>
			</c:forEach>
		</div>
		
		<c:if test="${siteConfig['ADD_ADDRESS_ON_CHECKOUT'].value == 'true' or not orderForm.newShippingAddress}">
		<form:form commandName="orderForm" id="change_shipping_billing_addressForm" class="form-horizontal" role="form" method="POST" action="#">
			<input id="newShippingAddress" name="newShippingAddress" value="true" type="hidden"/>
			<input type="hidden" name="save" value="true">
			<div class="row">
				<div class="col-sm-12">
					<div class="requiredFieldLabel">* Required Field</div>
				</div>
			</div>
			<div id="change_shipping_billing_address">
				<div class="row">
					<div class="col-sm-6">
						<div class="form-group">
							<div class="col-sm-12">
								<h3>New Address</h3>
							</div>
						</div>
						<div class="form-group">
							<div class="col-sm-4">
								<label for="customer_address_firstName" class="control-label">
									<fmt:message key="firstName" /> <sup class="requiredField">*</sup>
								</label>
							</div>
							<div class="col-sm-8">
								<form:input class="form-control" id="customer_address_firstName"  path="order.shipping.firstName" htmlEscape="true" />
							    <form:errors path="order.shipping.firstName" cssClass="error" />
							</div>
						</div>
						<div class="form-group">
							<div class="col-sm-4">
								<label for="customer_address_lastName" class="control-label">
									<fmt:message key="lastName" /> <sup class="requiredField">*</sup>
								</label>
							</div>
							<div class="col-sm-8">
								<form:input class="form-control" id="customer_address_lastName"  path="order.shipping.lastName" htmlEscape="true" />
							    <form:errors path="order.shipping.lastName" cssClass="error" />
							</div>
						</div>
						<div class="form-group">
							<div class="col-sm-4">
								<label for="customer_address_country" class="control-label">
									<fmt:message key="country" /> <sup class="requiredField">*</sup>
								</label>
							</div>
							<div class="col-sm-8">
								<form:select class="form-control" id="customer_address_country"  path="order.shipping.country">
								  <c:forEach items="${model.countrylist}" var="country">
						  	        <form:option value="${country.code}" >${country.name}</form:option>
						 		  </c:forEach>
								</form:select>
							    <form:errors path="order.shipping.country" cssClass="error" />
							</div>
						</div>
						<div class="form-group">
							<div class="col-sm-4">
								<label for="customer_address_company" class="control-label"><fmt:message key="company" /> </label>
							</div>
							<div class="col-sm-8">
								<form:input class="form-control" id="customer_address_company"  path="order.shipping.company" htmlEscape="true" />
							    <form:errors path="order.shipping.company" cssClass="error" />
							</div>
						</div>
						<div class="form-group">
							<div class="col-sm-4">
								<label for="customer_address_addr1" class="control-label">
									<fmt:message key="address" /> 1 <sup class="requiredField">*</sup>
								</label>
							</div>
							<div class="col-sm-8">
								<form:input class="form-control" id="customer_address_addr1"  path="order.shipping.addr1" htmlEscape="true" />
							    <form:errors path="order.shipping.addr1" cssClass="error" />
							</div>
						</div>
						<div class="form-group">
							<div class="col-sm-4">
								<label for="customer_address_addr2" class="control-label">
									<fmt:message key="address" /> 2 </label>
							</div>
							<div class="col-sm-8">
								<form:input class="form-control" id="customer_address_addr2"  path="order.shipping.addr2" htmlEscape="true" />
							    <form:errors path="order.shipping.addr2" cssClass="error" />
							</div>
						</div>
						<div class="form-group">
							<div class="col-sm-4">
								<label for="customer_address_city" class="control-label">
									<fmt:message key="city" /> <sup class="requiredField">*</sup>
								</label>
							</div>
							<div class="col-sm-8">
								<form:input class="form-control" id="customer_address_city"  path="order.shipping.city" htmlEscape="true" />
							    <form:errors path="order.shipping.city" cssClass="error" />
							</div>
						</div>
						<div class="form-group">
							<div class="col-sm-4">
								<label for="" class="control-label"> <fmt:message key="stateProvince" /> <sup
									class="requiredField">*</sup>
								</label>
							</div>
							<div class="col-sm-8">
								<form:input type="text" class="form-control" id="customer_address_stateProvince" path="order.shipping.stateProvince" />
								<div id="stateProvinceNA" class="checkbox">
									<label> 
									  <form:checkbox id="customer_address_stateProvinceNA" path="order.shipping.stateProvinceNA" />Not Applicable
									</label>
								</div>
								<form:select class="form-control" id="customer_address_state"  path="order.shipping.stateProvince">
									<form:option value="" label="Please Select"/>
	            					<c:forEach items="${model.statelist}" var="state">
							  	      <form:option value="${state.code}" >${state.name}</form:option>
									</c:forEach>      
								</form:select> 
								<form:select class="form-control" id="customer_address_ca_province"  path="order.shipping.stateProvince">
								    <form:option value="" label="Please Select"/>
	            					<c:forEach items="${model.caProvinceList}" var="province">
							  	      <form:option value="${province.code}">${province.name}</form:option>
									</c:forEach>      
								</form:select>
							</div>
						</div>
						<div class="form-group">
							<div class="col-sm-4">
								<label for="customer_address_zip" class="control-label">
									<fmt:message key="zipCode" /> <sup class="requiredField">*</sup>
								</label>
							</div>
							<div class="col-sm-8">
								<form:input class="form-control" id="customer_address_zip"  path="order.shipping.zip" htmlEscape="true" />
							    <form:errors path="order.shipping.zip" cssClass="error" />
							</div>
						</div>
						<div class="form-group">
							<div class="col-sm-4">
								<label for="customer_address_phone" class="control-label">
									<fmt:message key="phone" /> <sup class="requiredField">*</sup>
								</label>
							</div>
							<div class="col-sm-8">
								<form:input class="form-control" id="customer_address_phone"  path="order.shipping.phone" htmlEscape="true" />
							    <form:errors path="order.shipping.phone" cssClass="error" />
							</div>
						</div>
						<div class="form-group">
							<div class="col-sm-4">
								<label for="customer_address_cellPhone" class="control-label">
									<fmt:message key="cellPhone" /> </label>
							</div>
							<div class="col-sm-8">
								<form:input class="form-control" id="customer_address_cellPhone"  path="order.shipping.cellPhone" htmlEscape="true" />
							    <form:errors path="order.shipping.cellPhone" cssClass="error" />
							</div>
						</div>
						<div class="form-group">
							<div class="col-sm-4">
								<label for="customer_address_fax" class="control-label">
									<fmt:message key="fax" /> </label>
							</div>
							<div class="col-sm-8">
								<form:input class="form-control" id="customer_address_fax"  path="order.shipping.fax" htmlEscape="true" />
							    <form:errors path="order.shipping.fax" cssClass="error" />
							</div>
						</div>
						
						
						<c:if test="${siteConfig['ADDRESS_RESIDENTIAL_COMMERCIAL'].value >= 2}">
    					<div class="form-group">
							<div class="col-sm-4">
								<label for="customer_address_fax" class="control-label">
									<fmt:message key="deliveryType" /> </label>
							</div>
							<div class="col-sm-8">
								<form:radiobutton class="form-control" path="order.shipping.residential" value="true"/>: <fmt:message key="residential" /><br /><form:radiobutton class="form-control" path="order.shipping.residential" value="false"/>: <fmt:message key="commercial" /> 
    						    <form:errors path="order.shipping.residential" cssClass="error" />
							</div>
						</div>
    					</c:if>
  
    					<c:choose>
    						<c:when test="${siteConfig['ADDRESS_RESIDENTIAL_COMMERCIAL'].value == 0}">
    							<form:hidden path="order.shipping.residential" value="false"/>
    						</c:when>
    						<c:when test="${siteConfig['ADDRESS_RESIDENTIAL_COMMERCIAL'].value == 1}">
    							<form:hidden path="order.shipping.residential" value="true"/>
    						</c:when>
  						</c:choose>
  						
					</div>
					<div class="col-sm-6"></div>
				</div>
			</div>
			<div class="button_wrapper">
				<button type="submit" class="btn btn-default" name="_target2"><fmt:message key="shoppingcart.shipToThisAddress" /></button>
			</div>
 		</form:form>
		</c:if>
	</div>
</div>

<c:out value="${model.shippingLayout.footerHtml}" escapeXml="false"/>

<iframe src="${_contextpath}/sessionKeepAlive.jsp" width="0" height="0" frameborder="0"></iframe>

  </tiles:putAttribute>
</tiles:insertDefinition>