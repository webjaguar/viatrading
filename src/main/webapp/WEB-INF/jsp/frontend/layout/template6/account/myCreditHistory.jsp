<%@ page session="false" %>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="/WEB-INF/tlds/custom.tld" prefix="wj" %>

<tiles:insertDefinition name="${_template}" flush="true">
  <c:if test="${_leftBar != '1' and _leftBar != '4'}">
     <tiles:putAttribute name="leftbar" value="/WEB-INF/jsp/frontend/layout/template${_leftBar}/leftbar.jsp" />
  </c:if>
  <tiles:putAttribute name="content" type="string">
  
<script type="text/javascript" src="/dv/liveapps/scripts.js.php"></script>
<link rel="stylesheet" type="text/css" href="/dv/liveapps/myaccount.css"> 
  
<script type="text/javascript">

$(document).ready(function() {

vbareport(0,0,${model.userId});

});

</script>


<div id="vbareport">

</div>
	
	<c:out value="${model.viewMyBalanceLayout.footerHtml}" escapeXml="false"/>
	
  </tiles:putAttribute>
</tiles:insertDefinition>