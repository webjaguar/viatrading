<%@ page session="false" %>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="/WEB-INF/tlds/spring.tld" prefix="spring" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>

<tiles:insertDefinition name="${_template}" flush="true">
  <tiles:putAttribute name="content" type="string">

	<div class="col-breadcrumb">
		<ul class="breadcrumb">
			<li><a href="account.jhtm">My Account</a></li>
			<li class="active">Gift Card</li>
		</ul>
	</div>
	
	<c:out value="${giftCardLayout.headerHtml}" escapeXml="false"/>
	
	<div class="col-sm-12">
			<form:form  commandName="giftCardForm" method="post" id="giftCardForm" class="form-horizontal" role="form">
			<input type="hidden" name="_page" value="0">
			
			<c:if test="${message != null}">
			  <div class="message"><fmt:message key="${message}" /> <c:out value="${gSiteConfig['gCREDIT_CARD_PAYMENT']}"/>.</div>
			</c:if>
			
			<div id="giftCard">
				<div class="row">
					<div class="col-sm-12 col-md-10 col-lg-8">
						<div class="form-group">
							<div class="col-sm-12">
								<h3>Gift Card</h3>
							</div>
						</div>
						
						<div class="form-group">
							<div class="col-sm-12">
								<label class="control-label">
									Maximum allowed limit to buy gift cards is <fmt:message key="${siteConfig['CURRENCY'].value}"/><fmt:formatNumber value="${siteConfig['GIFTCARD_MAX_AMOUNT'].value}"/>.
								</label>
							</div>
						</div>
						<div class="form-group">
							<div class="col-sm-2">
								<label class="control-label">
									<fmt:message key="amount" /><sup class="requiredField">*</sup>
								</label>
							</div>
							<spring:bind path="giftCardOrder.amount1"> 
							<div class="col-sm-2 <c:if test="${status.error}">has-error</c:if>">
								<form:select dir="rtl" path="giftCardOrder.amount1" class="form-control giftCardOrder_amount">
									
									<form:option dir="ltr" value="" >Select</form:option>
					  	          	<c:forTokens items="${siteConfig['GIFTCARD_DENOMINATIONS'].value}" delims="," var="giftCardAmount" varStatus="status">
					  	          		<form:option value="${giftCardAmount}"><fmt:message key="${siteConfig['CURRENCY'].value}" /><fmt:formatNumber value="${giftCardAmount}" pattern="#,##0.00#" maxFractionDigits="${siteConfig['MAX_FRACTION_DIGIT'].value}"/></form:option>
					  	          	</c:forTokens>
								</form:select>
								<c:if test="${status.error}">
								<small class="help-block error-message"><form:errors path="giftCardOrder.amount1" /></small>
								</c:if>
							</div>
							</spring:bind>
							<%--ADI Wants only one quantity and input box --%>
							<c:if test="${!gSiteConfig['gADI']}"> 	
							<spring:bind path="giftCardOrder.amount2">
							<div class="col-sm-2 <c:if test="${status.error}">has-error</c:if>">
								<form:select dir="rtl" path="giftCardOrder.amount2" class="form-control giftCardOrder_amount">
									<form:option value="">Select</form:option>
					  	          	<c:forTokens items="${siteConfig['GIFTCARD_DENOMINATIONS'].value}" delims="," var="giftCardAmount" varStatus="status">
					  	          		<form:option value="${giftCardAmount}"><fmt:message key="${siteConfig['CURRENCY'].value}" /><fmt:formatNumber value="${giftCardAmount}" pattern="#,##0.00#" maxFractionDigits="${siteConfig['MAX_FRACTION_DIGIT'].value}"/></form:option>
					  	          	</c:forTokens>
								</form:select>
								<c:if test="${status.error}">
								<i class="form-control-feedback glyphicon glyphicon-remove"></i>
								<small class="help-block error-message"><form:errors path="giftCardOrder.amount2" /></small>
								</c:if>
							</div>
							</spring:bind>
							<spring:bind path="giftCardOrder.amount3">
							<div class="col-sm-2 <c:if test="${status.error}">has-feedback has-error</c:if>">
								<form:select dir="rtl" path="giftCardOrder.amount3" class="form-control giftCardOrder_amount">
									<form:option value="">Select</form:option>
					  	          	<c:forTokens items="${siteConfig['GIFTCARD_DENOMINATIONS'].value}" delims="," var="giftCardAmount" varStatus="status">
					  	          		<form:option value="${giftCardAmount}"><fmt:message key="${siteConfig['CURRENCY'].value}" /><fmt:formatNumber value="${giftCardAmount}" pattern="#,##0.00#" maxFractionDigits="${siteConfig['MAX_FRACTION_DIGIT'].value}"/></form:option>
					  	          	</c:forTokens>
								</form:select>
								<c:if test="${status.error}">
								<i class="form-control-feedback glyphicon glyphicon-remove"></i>
								<small class="help-block error-message"><form:errors path="giftCardOrder.amount3" /></small>
								</c:if>
							</div>
							</spring:bind>
							<spring:bind path="giftCardOrder.amount4">
							<div class="col-sm-2 <c:if test="${status.error}">has-feedback has-error</c:if>">
								<form:select dir="rtl" path="giftCardOrder.amount4" class="form-control giftCardOrder_amount">
									<form:option value="">Select</form:option>
					  	          	<c:forTokens items="${siteConfig['GIFTCARD_DENOMINATIONS'].value}" delims="," var="giftCardAmount" varStatus="status">
					  	          		<form:option value="${giftCardAmount}"><fmt:message key="${siteConfig['CURRENCY'].value}" /><fmt:formatNumber value="${giftCardAmount}" pattern="#,##0.00#" maxFractionDigits="${siteConfig['MAX_FRACTION_DIGIT'].value}"/></form:option>
					  	          	</c:forTokens>
								</form:select>
								<c:if test="${status.error}">
								<i class="form-control-feedback glyphicon glyphicon-remove"></i>
								<small class="help-block error-message"><form:errors path="giftCardOrder.amount4" /></small>
								</c:if>
							</div>
							</spring:bind>
							<spring:bind path="giftCardOrder.amount5">
							<div class="col-sm-2 <c:if test="${status.error}">has-feedback has-error</c:if>">
								<form:select dir="rtl" path="giftCardOrder.amount5" class="form-control giftCardOrder_amount">
									<form:option value="">Select</form:option>
					  	          	<c:forTokens items="${siteConfig['GIFTCARD_DENOMINATIONS'].value}" delims="," var="giftCardAmount" varStatus="status">
					  	          		<form:option value="${giftCardAmount}"><fmt:message key="${siteConfig['CURRENCY'].value}" /><fmt:formatNumber value="${giftCardAmount}" pattern="#,##0.00#" maxFractionDigits="${siteConfig['MAX_FRACTION_DIGIT'].value}"/></form:option>
					  	          	</c:forTokens>
								</form:select>
								<c:if test="${status.error}">
								<i class="form-control-feedback glyphicon glyphicon-remove"></i>
								<small class="help-block error-message"><form:errors path="giftCardOrder.amount5" /></small>
								</c:if>
							</div>
							</spring:bind>
							</c:if>
						</div>
						<div class="form-group">
							<div class="col-sm-2">
								<label class="control-label">
									<fmt:message key="quantity" /><sup class="requiredField">*</sup>
								</label>
							</div>
							<spring:bind path="giftCardOrder.quantity1">
							<div class="col-sm-2 <c:if test="${status.error}">has-feedback has-error</c:if>">
								<form:input path="giftCardOrder.quantity1" class="form-control giftCardOrder_quantity" maxlength="4" />
								<c:if test="${status.error}">
								<i class="form-control-feedback glyphicon glyphicon-remove"></i>
								<small class="help-block error-message"><form:errors path="giftCardOrder.quantity1" /></small>
								</c:if>
							</div>
							</spring:bind>
							<c:if test="${!gSiteConfig['gADI']}"> 	
							<spring:bind path="giftCardOrder.quantity2">
							<div class="col-sm-2 <c:if test="${status.error}">has-feedback has-error</c:if>">
								<form:input path="giftCardOrder.quantity2" class="form-control giftCardOrder_quantity" maxlength="4"/>
								<c:if test="${status.error}">
								<i class="form-control-feedback glyphicon glyphicon-remove"></i>
								<small class="help-block error-message"><form:errors path="giftCardOrder.quantity2" /></small>
								</c:if>
							</div>
							</spring:bind>
							<spring:bind path="giftCardOrder.quantity3">
							<div class="col-sm-2 <c:if test="${status.error}">has-feedback has-error</c:if>">
								<form:input path="giftCardOrder.quantity3" class="form-control giftCardOrder_quantity" maxlength="4" />
								<c:if test="${status.error}">
								<i class="form-control-feedback glyphicon glyphicon-remove"></i>
								<small class="help-block error-message"><form:errors path="giftCardOrder.quantity3" /></small>
								</c:if>
							</div>
							</spring:bind>
							<spring:bind path="giftCardOrder.quantity4">
							<div class="col-sm-2 <c:if test="${status.error}">has-feedback has-error</c:if>">
								<form:input path="giftCardOrder.quantity4" class="form-control giftCardOrder_quantity" maxlength="4"/>
								<c:if test="${status.error}">
								<i class="form-control-feedback glyphicon glyphicon-remove"></i>
								<small class="help-block error-message"><form:errors path="giftCardOrder.quantity4" /></small>
								</c:if>
							</div>
							</spring:bind>
							<spring:bind path="giftCardOrder.quantity5">
							<div class="col-sm-2 <c:if test="${status.error}">has-feedback has-error</c:if>">
								<form:input path="giftCardOrder.quantity5" class="form-control giftCardOrder_quantity" maxlength="4"/>
								<c:if test="${status.error}">
								<i class="form-control-feedback glyphicon glyphicon-remove"></i>
								<small class="help-block error-message"><form:errors path="giftCardOrder.quantity5" /></small>
								</c:if>
 							</div>
 							</spring:bind>
 							</c:if>
						</div>

						<div class="form-group">
							<div class="col-sm-12">
								<label class="control-label">
									<fmt:message key="enterToAndFromAsYou'dLikeThemToAppearOnTheGiftCard" />
								</label>
							</div>
						</div>
						 <div class="form-group">
							<div class="col-sm-2">
								<label class="control-label">
									<fmt:message key="to" /><sup class="requiredField">*</sup>
								</label>
							</div>
					 		<spring:bind path="giftCard.recipientFirstName"> 
								<div class="col-sm-5 <c:if test="${status.error}">has-feedback has-error</c:if>">
									<form:input path="giftCard.recipientFirstName" placeholder="First Name" class="form-control"/>
									<c:if test="${status.error}">
									<i class="form-control-feedback glyphicon glyphicon-remove"></i>
									<small class="help-block error-message"><form:errors path="giftCard.recipientFirstName" /></small>
									</c:if>
								</div>
							</spring:bind>
					 		<spring:bind path="giftCard.recipientLastName">
								<div class="col-sm-5 <c:if test="${status.error}">has-feedback has-error</c:if>">
									<form:input path="giftCard.recipientLastName" placeholder="Last Name"  class="form-control"/>
									<c:if test="${status.error}">
									<i class="form-control-feedback glyphicon glyphicon-remove"></i>
									<small class="help-block error-message"><form:errors path="giftCard.recipientLastName" /></small>
									</c:if>
								</div>
							</spring:bind>
						</div>
						<div class="form-group">
							<div class="col-sm-2">
								<label class="control-label">
									<fmt:message key="from" /><sup class="requiredField">*</sup>
								</label>
							</div>
							<spring:bind path="giftCard.senderFirstName">
								<div class="col-sm-5 <c:if test="${status.error}">has-feedback has-error</c:if>">
									<form:input path="giftCard.senderFirstName" placeholder="First Name"  class="form-control"/>
									<c:if test="${status.error}">
									<i class="form-control-feedback glyphicon glyphicon-remove"></i>
									<small class="help-block error-message"><form:errors path="giftCard.senderFirstName" /></small>
									</c:if>
								</div>
							</spring:bind>
							<spring:bind path="giftCard.senderLastName">
							<div class="col-sm-5 <c:if test="${status.error}">has-feedback has-error</c:if>">
								<form:input path="giftCard.senderLastName"  placeholder="Last Name" class="form-control"/>
								<c:if test="${status.error}">
								<i class="form-control-feedback glyphicon glyphicon-remove"></i>
								<small class="help-block error-message"><form:errors path="giftCard.senderLastName" /></small>
								</c:if>
							</div>
							</spring:bind>
						</div>

						<div class="form-group">
							<div class="col-sm-12">
								<label class="control-label">
									<fmt:message key="whatIsTheRecipientsEmailAddress" />
								</label>
							</div>
						</div>
						<div class="form-group">
							<div class="col-sm-3">
								<label class="control-label">
									<fmt:message key="email" /><sup class="requiredField">*</sup>
								</label>
							</div>
							<spring:bind path="giftCard.recipientEmail">
							<div class="col-sm-9 <c:if test="${status.error}">has-feedback has-error</c:if>">
								<form:input path="giftCard.recipientEmail" class="form-control"/>
								<c:if test="${status.error}">
								<i class="form-control-feedback glyphicon glyphicon-remove"></i>
								<small class="help-block error-message"><form:errors path="giftCard.recipientEmail" /></small>
								</c:if>
							</div>
							</spring:bind>
						</div>
						<div class="form-group">
							<div class="col-sm-3">
								<label class="control-label">
									<fmt:message key="confirmEmail" /><sup class="requiredField">*</sup>
								</label>
							</div>
							<spring:bind path="confirmEmail">
							<div class="col-sm-9 <c:if test="${status.error}">has-feedback has-error</c:if>">
								<form:input path="confirmEmail" class="form-control"/>
								<c:if test="${status.error}">
								<i class="form-control-feedback glyphicon glyphicon-remove"></i>
								<small class="help-block error-message"><form:errors path="confirmEmail" /></small>
								</c:if>
							</div>
							</spring:bind>
						</div>

						<div class="form-group">
							<div class="col-sm-12">
								<label class="control-label">
									<fmt:message key="wouldYouLikeToIncludeAMessageForTheRecipient" /> (<fmt:message key="optional" />)
								</label>
							</div>
						</div>
						<div class="form-group">
							<div class="col-sm-3">
								<label class="control-label">
									<fmt:message key="message" />
								</label>
							</div>
							<spring:bind path="giftCard.message">
							<div class="col-sm-9 <c:if test="${status.error}">has-feedback has-error</c:if>">
								<form:textarea path="giftCard.message" rows="10" class="form-control"/>
							</div>
							</spring:bind>
						</div>

					</div>
					<div class="col-sm-6"></div>
				</div>
			</div>
			<div id="form_buttons">
				<div class="row">
					<div class="col-sm-12">
						<div id="buttons_wrapper">
							<c:if test="${true or paymentGateway != false}">
					       	 	<button type="submit" class="btn btn-default" name="_cancel" />Cancel</button>
						 	 	<button type="submit" class="btn btn-default" name="_target1" />Next</button>
						  	</c:if>
						</div>
					</div>
				</div>
			</div>
		</form:form>
	</div>
	
	<c:out value="${giftCardLayout.footerHtml}" escapeXml="false"/>
	
</tiles:putAttribute>
</tiles:insertDefinition>