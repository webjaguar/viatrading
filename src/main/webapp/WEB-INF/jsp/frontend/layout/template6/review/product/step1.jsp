<%@ page session="false" %>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<script type="text/javascript" src="javascript/mootools-1.2.5-core.js"></script>

<tiles:insertDefinition name="${_template}" flush="true">
  <c:if test="${_leftBar != '1' and _leftBar != '4'}">
    <tiles:putAttribute name="leftbar" value="/WEB-INF/jsp/frontend/layout/template${_leftBar}/leftbar.jsp" />
  </c:if>
  <tiles:putAttribute name="content" type="string">
<c:if test="${gSiteConfig['gPRODUCT_REVIEW']}">
<c:out value="${model.productReviewLayout.headerHtml}" escapeXml="false"/>

<c:if test="${model.product != null and model.product.enableRate}">



<div class="col-sm-12">
	<form:form commandName="productReviewForm" method="post" class="form-horizontal" role="form">
		<input type="hidden" name="_page" value="0">
		<c:if test="${message != null}">
			<div class="message">error message</div>
		</c:if>
			
		<div id="addProductReview">
			<div class="row">
				<div class="col-sm-6">
					<div class="form-group">
						<div class="col-sm-12">
							<h3>Add Product Review</h3>
						</div>
					</div>
					<spring:bind path="productReview.userName"> 
					<div class="form-group <c:if test="${status.error}">has-feedback has-error</c:if>">
						<div class="col-sm-4">
							<label for="productReview_userName" class="control-label">
								<fmt:message key="displayName"/> <sup class="requiredField">*</sup>
							</label>
						</div>
						<div class="col-sm-8">
						  	<form:input class="form-control" id="productReview_userName"  path="productReview.userName"/>
  							<c:if test="${status.error}">
								<i class="form-control-feedback glyphicon glyphicon-remove"></i>
								<small class="help-block error-message"><form:errors path="productReview.userName" /></small>
  							</c:if>
  						</div>
					</div>
					</spring:bind>
					
					
					<spring:bind path="productReview.rate"> 
					<div class="form-group <c:if test="${status.error}">has-feedback has-error</c:if>">
						<div class="col-sm-4">
							<label for="productReview_rate" class="control-label">
								<fmt:message key="overalRating"/> <sup class="requiredField">*</sup>
							</label>
						</div>
						<div class="col-sm-8">
							<div class="product_rating">
								<div class="rating_stars">
									<ul class="rating">
										<li id="1" class="rate one"><span title="1 Star"></span>
										</li>
										<li id="2" class="rate two"><span title="2 Stars"></span>
										</li>
										<li id="3" class="rate three"><span title="3 Stars"></span>
										</li>
										<li id="4" class="rate four"><span title="4 Stars"></span>
										</li>
										<li id="5" class="rate five"><span title="5 Stars"></span>
										</li>
									</ul>
								</div>
								<div class="clearfix"></div>
							</div>
							
							<form:input class="form-control" id="productReview_rate" path="productReview.rate"/>
  							<c:if test="${status.error}">
								<i class="form-control-feedback glyphicon glyphicon-remove"></i>
								<small class="help-block error-message"><form:errors path="productReview.rate" /></small>
  							</c:if>
						</div>
					</div>
					</spring:bind>
					
					<spring:bind path="productReview.title"> 
					<div class="form-group <c:if test="${status.error}">has-feedback has-error</c:if>">
						<div class="col-sm-4">
							<label for="productReview_title" class="control-label">
								Review Title <sup class="requiredField">*</sup>
							</label>
						</div>
						<div class="col-sm-8">
							<form:input class="form-control" id="productReview_title" path="productReview.title"/>
  							<c:if test="${status.error}">
								<i class="form-control-feedback glyphicon glyphicon-remove"></i>
								<small class="help-block error-message"><form:errors path="productReview.title" /></small>
  							</c:if>
						</div>
					</div>
					</spring:bind>
					
					
					<spring:bind path="productReview.text"> 
					<div class="form-group <c:if test="${status.error}">has-feedback has-error</c:if>">
						<div class="col-sm-4">
							<label for="productReview_text" class="control-label">
								Review <sup class="requiredField">*</sup>
							</label>
						</div>
						<div class="col-sm-8">
							<form:textarea id="productReview_text" class="form-control" rows="8" path="productReview.text"/>
  							<c:if test="${status.error}">
								<i class="form-control-feedback glyphicon glyphicon-remove"></i>
								<small class="help-block error-message"><form:errors path="productReview.text" /></small>
  							</c:if>
  						</div>
					</div>
					</spring:bind>

				</div>
				<div class="col-sm-6"></div>
			</div>
		</div>

		<div id="form_buttons">
			<div class="row">
				<div class="col-sm-12">
					<div id="buttons_wrapper">
						<button type="submit" name="_target1" class="btn btn-default">Next</button>
					</div>
				</div>
			</div>
		</div>
	</form:form>
</div>












</c:if>
<script type="text/javascript">
<!--
$('.rate').each(function(i, obj) {
	$( this ).click(function() {
		$('#productReview_rate').val($( this ).attr('id'));
		
	});
});
//-->
</script>

<c:if test="${model.product.enableRate!=true}">
At present, this product does not require your feedback.Try again in future.
</c:if>
<c:out value="${model.productReviewLayout.footerHtml}" escapeXml="false"/>
</c:if>
  </tiles:putAttribute>
</tiles:insertDefinition>