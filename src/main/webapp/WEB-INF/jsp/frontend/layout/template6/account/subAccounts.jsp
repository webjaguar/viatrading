<%@ page session="false" %>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="/WEB-INF/tlds/spring.tld" prefix="spring" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ taglib uri="/WEB-INF/tlds/custom.tld" prefix="wj" %>

<c:if test="${gSiteConfig['gSUB_ACCOUNTS']}">
<tiles:insertDefinition name="${_template}" flush="true">
  <c:if test="${_leftBar != '1' and _leftBar != '4'}">
     <tiles:putAttribute name="leftbar" value="/WEB-INF/jsp/frontend/layout/template${_leftBar}/leftbar.jsp" />
  </c:if>
  <tiles:putAttribute name="content" type="string">

		<div class="col-breadcrumb">
			<ul class="breadcrumb">
				<li><a href="${_contextpath}/account.jhtm">My Account</a></li>
				<li class="active">Sub Accounts</li>
			</ul>
		</div>

		<div class="col-sm-12">
			<div class="subAccountsWrapper">
				<div class="headerTitle subAccountsTitle">Sub Accounts</div>

				<div class="clearfix"></div>

				<div class="row">
					<div class="col-sm-12">
						<div class="pageNavigation">
							<form class="formPageNavigation clearfix" name="pagination" action="#">
								<div class="pageNavigationControl">
								<%--
									<div class="pagesize">
										<span>Page Size</span>
										<select class="selectpicker" name="pagesize">
											<option value="10">10 per page</option>
											<option value="20">20 per page</option>
											<option value="25" selected="">25 per page</option>
											<option value="50">50 per page</option>
											<option value="100">100 per page</option>
										</select>
									</div> --%>
									<div class="page">
										<span>Page</span>
										<select class="selectpicker" name="page" onchange="submit()">
										<c:forEach begin="1" end="${model.accounts.pageCount}" var="page">
								  	        <option value="${page}" <c:if test="${page == (model.accounts.page+1)}">selected</c:if>>${page}</option>
										</c:forEach>
										</select>
										<span>of <c:out value="${model.accounts.pageCount}"/></span>
									</div>
									<%--
									<div class="pageNav">
										<span class="pageNavLink">
											<a href="#"><span class="pageNavPrev"></span></a>
										</span>
										<span class="pageNavLink">
											<a href="#"><span class="pageNavNext"></span></a>
										</span>
									</div> --%>
								</div>
							</form>
						</div>
					</div>
				</div>
				
				
				<form action="account_subs.jhtm" id="list" method="post">
				<input type="hidden" id="cid" name="cid"> 
				<input type="hidden" id="sort" name="sort" value="${subAccountCustomerSearch.sort}" />
				<input type="hidden" id="managerCredit" name="managerCredit" value="${model.customerCredit}"/>
				<input type="hidden" id="managerPartnerCredit" name="managerPartnerCredit" value="${model.mainPartner.amount}"/>
				<input type="hidden" id="managerRewardPoints" name="managerRewardPoints" value="${model.rewardPoints}"/>
				
				
				
				<div class="subAccountsDropDown">
					<div class="form-group">
						<select name="parent" onChange="submit()">
							<option value=""><fmt:message key="subAccounts" /></option>
							<c:forEach items="${model.familyTree}" var="customer">
							<option value="${customer.id}"
							<c:if test="${customer.id == subAccountCustomerSearch['parent']}">selected</c:if>>
							<fmt:message key="subAccounts" /> <fmt:message key="of" /> <c:out value="${customer.username}" /> 
						  </option>
						</c:forEach>
						</select>
					</div>
				</div>

				<div class="subAccounts_table">
					<div class="subAccountsHdr">
						<div class="row clearfix">
							<div class="col-sm-12 col-md-4">
								<div class="emailHeader">
									<c:if test="${fn:contains(siteConfig['CUSTOMIZE_SUBACCOUNT_LIST'].value,'email')}">
								      <c:choose>
								   	      <c:when test="${subAccountCustomerSearch.sort == 'username desc'}">
								   	        <a class="sortLink" href="#" onClick="document.getElementById('sort').value='username';document.getElementById('list').submit()"><fmt:message key="email" /></a>
								   	      </c:when>
								   	      <c:when test="${subAccountCustomerSearch.sort == 'username'}">
								   	        <a class="sortLink" href="#" onClick="document.getElementById('sort').value='username desc';document.getElementById('list').submit()"><fmt:message key="email" /></a>
								   	      </c:when>
								   	      <c:otherwise>
								   	        <a class="sortLink" href="#" onClick="document.getElementById('sort').value='username desc';document.getElementById('list').submit()"><fmt:message key="email" /></a>
								   	      </c:otherwise>
								   	  </c:choose>  	  
								  	</c:if>
								</div>
							</div>
							<div class="col-sm-12 col-md-3">
								<div class="nameHeader">
									<c:if test="${fn:contains(siteConfig['CUSTOMIZE_SUBACCOUNT_LIST'].value,'lastName')}">
								      <c:choose>
								   	      <c:when test="${subAccountCustomerSearch.sort == 'last_name desc'}">
								   	        <a class="sortLink" href="#" onClick="document.getElementById('sort').value='last_name';document.getElementById('list').submit()"><fmt:message key="name" /></a>
								   	      </c:when>
								   	      <c:when test="${subAccountCustomerSearch.sort == 'last_name'}">
								   	        <a class="sortLink" href="#" onClick="document.getElementById('sort').value='last_name desc';document.getElementById('list').submit()"><fmt:message key="name" /></a>
								   	      </c:when>
								   	      <c:otherwise>
								   	        <a class="sortLink" href="#" onClick="document.getElementById('sort').value='last_name desc';document.getElementById('list').submit()"><fmt:message key="name" /></a>
								   	      </c:otherwise>
								   	  </c:choose>  	  
								  	</c:if>
								</div>
							</div>
							<div class="col-sm-12 col-md-1">
								<div class="accountNumberHeader">
									<c:if test="${fn:contains(siteConfig['CUSTOMIZE_SUBACCOUNT_LIST'].value,'accountNumber')}">
								  	  <c:choose>
								   	      <c:when test="${subAccountCustomerSearch.sort == 'account_number desc'}">
								   	        <a class="sortLink" href="#" onClick="document.getElementById('sort').value='account_number';document.getElementById('list').submit()"><fmt:message key="accountNumber" /></a>
								   	      </c:when>
								   	      <c:when test="${subAccountCustomerSearch.sort == 'account_number'}">
								   	        <a class="sortLink" href="#" onClick="document.getElementById('sort').value='account_number desc';document.getElementById('list').submit()"><fmt:message key="accountNumber" /></a>
								   	      </c:when>
								   	      <c:otherwise>
								   	        <a class="sortLink" href="#" onClick="document.getElementById('sort').value='account_number desc';document.getElementById('list').submit()"><fmt:message key="accountNumber" /></a>
								   	      </c:otherwise>
								   	  </c:choose> 
								  	</c:if>
								</div>
							</div>
							<div class="col-sm-12 col-md-1">
								<c:if test="${gSiteConfig['gSHOPPING_CART'] and fn:contains(siteConfig['CUSTOMIZE_SUBACCOUNT_LIST'].value,'orders')}">
							  	  	<div class="ordersHeader"><fmt:message key="orderCount" /></div>
								</c:if>
							</div>
							<div class="col-sm-12 col-md-1">
							  	<c:if test="${gSiteConfig['gSHOPPING_CART'] and  fn:contains(siteConfig['CUSTOMIZE_SUBACCOUNT_LIST'].value,'orderTotal')}">
									<div class="orderTotalHeader"><fmt:message key="orderTotal" /></div>
								</c:if>	
							</div>
							<div class="col-sm-12 col-md-1">
								<c:if test="${fn:contains(siteConfig['CUSTOMIZE_SUBACCOUNT_LIST'].value,'subAccounts')}">
							  	  <fmt:message key="subAccounts" />
							  	</c:if>
							</div>												
							<div class="col-sm-12 col-md-1">
								<c:if test="${fn:contains(siteConfig['CUSTOMIZE_SUBACCOUNT_LIST'].value,'quote')}">
									<div class="quoteHeader"><fmt:message key="quote" /></div>
								</c:if>
							</div>
						</div>
					</div>
					
					<div class="subAccountsDetails">
					<c:forEach items="${model.accounts.pageList}" var="account" varStatus="status">
					<c:set value="odd_row" var="divRow" />
					<c:if test="${status.index % 2 == 0}"><c:set value="even_row" var="divRow" /></c:if>
						<div class="${divRow} clearfix">
							<div class="col-sm-12 col-md-4">
								<div class="emailHeader visible-xs visible-sm">Email</div>
								<div class="email">
									<c:if test="${fn:contains(siteConfig['CUSTOMIZE_SUBACCOUNT_LIST'].value,'email')}">
								      <div class="customerEmail"><c:out value="${account.username}"/></div>
								      <c:if test="${siteConfig['SUBACCOUNT_LOGINAS_FRONTEND'].value == 'true' and fn:contains(siteConfig['CUSTOMIZE_SUBACCOUNT_LIST'].value,'loginAs')}">
									  	  <div class="loginAsCustomer"><a href="loginAsCustomer.jhtm?cid=${account.token}" target="_blank"><fmt:message key="loginAsCustomer" /></a></div>		
									  </c:if>
								    </c:if>
							
									<c:if test="${fn:contains(siteConfig['CUSTOMIZE_SUBACCOUNT_LIST'].value,'subAccounts')}">
                                    <div class=""creditAvailable>Credit Available
                                    <c:choose>
                                        <c:when test="${gSiteConfig['gBUDGET']}">
                                              <a href="javascript:void(0)" data-toggle="modal" data-target="#credit_modal_1"><c:out value="${account.credit}"/></a>
                                        </c:when>
                                        <c:otherwise>
                                               <c:out value="${account.credit}"/>
                                        </c:otherwise>
                                    </c:choose>
                                    </div>
                                     </c:if>
                                 
                                 
                                     <div class="rewardPoints">Reward Points
                                     <c:choose>
                                             <c:when test="${siteConfig['GROUP_REWARDS'].value != null}">
                                                     <a href="javascript:void(0)" data-toggle="modal" data-target="#reward_modal_1"><c:out value="${account.rewardPoints}"/></a>
                                             </c:when>
                                             <c:otherwise>
                                                     <c:out value="${account.rewardPoints}"/>
                                             </c:otherwise>
                                     </c:choose>
                                     </div>
							
								    
								</div>
								<div class="clearfix"></div>
							</div>
							<div class="col-sm-12 col-md-3">
								<div class="nameHeader visible-xs visible-sm">Name</div>
								<div class="name">
									<c:if test="${fn:contains(siteConfig['CUSTOMIZE_SUBACCOUNT_LIST'].value,'lastName')}">
      									<div class="customerName"><c:out value="${account.address.firstName}"/> <c:out value="${account.address.lastName}"/></div>
								    </c:if>
								    <c:if test="${fn:contains(siteConfig['CUSTOMIZE_SUBACCOUNT_LIST'].value,'companyName')}">  	
								    	<div class="companyName"><c:out value="${account.address.company}"/></div>
								    </c:if>
								</div>
								<div class="clearfix"></div>
							</div>
							<div class="col-sm-12 col-md-1">
								<div class="accountNumberHeader visible-xs visible-sm">Account Number</div>
								<c:if test="${fn:contains(siteConfig['CUSTOMIZE_SUBACCOUNT_LIST'].value,'accountNumber')}">  	
							      <div class="accountNumber"><c:out value="${account.accountNumber}"/></div>
							    </c:if>
								<div class="clearfix"></div>
							</div>
							<div class="col-sm-12 col-md-1">
								<div class="ordersHeader visible-xs visible-sm">Orders</div>
								<c:if test="${gSiteConfig['gSHOPPING_CART'] and fn:contains(siteConfig['CUSTOMIZE_SUBACCOUNT_LIST'].value,'orders')}">
									<div class="orders"><c:out value="${account.orderCount}"/></div>
							    </c:if>
								<div class="clearfix"></div>
							</div>
							<div class="col-sm-12 col-md-1">
								<div class="orderTotalHeader visible-xs visible-sm">Order Total</div>
								<c:if test="${gSiteConfig['gSHOPPING_CART'] and fn:contains(siteConfig['CUSTOMIZE_SUBACCOUNT_LIST'].value,'orderTotal')}">
								<div class="orderTotal"><c:if test="${account.ordersGrandTotal != null}">&nbsp;</c:if>
									<c:if test="${account.ordersGrandTotal != null}"><a href="account_orders.jhtm?id=${account.id}" style="color:blue;"><fmt:message key="${siteConfig['CURRENCY'].value}" /><fmt:formatNumber value="${account.ordersGrandTotal}" pattern="#,##0.00" /></a></c:if></div>
								</c:if>
								<div class="clearfix"></div>
							</div>
							<div class="col-sm-12 col-md-1">
							<c:if test="${fn:contains(siteConfig['CUSTOMIZE_SUBACCOUNT_LIST'].value,'subAccounts')}">
								<div class="subAccountsHeader visible-xs visible-sm">Sub Accounts</div>
								<c:if test="${fn:contains(siteConfig['CUSTOMIZE_SUBACCOUNT_LIST'].value,'subAccounts')}">
							     <div class="subAccounts">
							    	<c:if test="${account.subCount > 0}"><a href="account_subs.jhtm?parent=${account.id}" style="color:blue;"><c:out value="${account.subCount}"/></a></c:if>
							    	<c:if test="${account.subCount == 0}">0</c:if>
							      </div>
							    </c:if>
								<div class="clearfix"></div>
							</c:if>
							</div>
							
							<%-- No Need to implement for now because there will be a change in partner's credit implementation
							<c:if test="${model.mainPartner != null}">   	
						       	<td align="center">
						      	<c:choose>
						        <c:when test="${gSiteConfig['gBUDGET'] and siteConfig['BUDGET_ADD_PARTNER'].value == 'true' and !empty account.partnersList}">   
						       	  <c:forEach items="${account.partnersList}" var="partner">
						       	  	<c:if test="${model.mainPartner.partnerId == partner.partnerId}">
							       	  	<a href="#${account.id}_accountId" rel="type:element" id="mbAddPartnerCredit${account.id}" class="mbAddPartnerCredit"><fmt:message key="${siteConfig['CURRENCY'].value}" /><fmt:formatNumber value="${partner.amount}" pattern="#,##0.00" /></a>
								        <div id="${account.id}_accountId" class="miniWindowWrapper addPartnerCreditBox">
								        <div class="header"><fmt:message key="add"/> <fmt:message key="partner"/> <fmt:message key="credit"/></div>
								        <fieldset class="field top">
								   	    	<label class="AStitle"><fmt:message key="customer"/> <fmt:message key="name"/></label>
								   	    	<input name="name" type="text" value="<c:out value="${account.address.firstName}"/> <c:out value="${account.address.lastName}"/>" disabled="disabled"/>
								   	  	</fieldset>
								   	  	<fieldset class="field">
								   	    	<label class="AStitle"><fmt:message key="credit"/> <fmt:message key="available"/></label>
								   	    	<input name="creditAvailable" type="text" value="<fmt:formatNumber value="${model.mainPartner.amount}" pattern="#,##0.00" />" disabled="disabled"/>
								   	  	</fieldset>
								   	  	<fieldset class="field bottom">
								   	    	<label class="AStitle"><fmt:message key="credit"/></label>
								   	    	<input name="__partnerCredit_" type="text" id="partnerCredit_${account.id}" onchange="partnerCreditValue(this.value, ${account.id});"/> 
								   	  	</fieldset>
								   	  	<div class="button">
											<input type="submit" value="Add"  name="__add_partner_credit_" onclick="addPartnerCredit(${account.id},${partner.partnerId});"/>
										</div>
								        </div>	
						       	  	</c:if>
						       	  </c:forEach>
						        </c:when>
						        <c:otherwise>
						          	<a href="#${account.id}_accountId" rel="type:element" id="mbAddPartnerCredit${account.id}" class="mbAddPartnerCredit">0.00</a>
							        <div id="${account.id}_accountId" class="miniWindowWrapper addPartnerCreditBox">
							        <div class="header"><fmt:message key="add"/> <fmt:message key="partner"/> <fmt:message key="credit"/></div>
							        <fieldset class="field top">
							   	    	<label class="AStitle"><fmt:message key="customer"/> <fmt:message key="name"/></label>
							   	    	<input name="name" type="text" value="<c:out value="${account.address.firstName}"/> <c:out value="${account.address.lastName}"/>" disabled="disabled"/>
							   	  	</fieldset>
							   	  	<fieldset class="field">
							   	    	<label class="AStitle"><fmt:message key="credit"/> <fmt:message key="available"/></label>
							   	    	<input name="creditAvailable" type="text" value="<fmt:formatNumber value="${model.mainPartner.amount}" pattern="#,##0.00" />" disabled="disabled"/>
							   	  	</fieldset>
							   	  	<fieldset class="field bottom">
							   	    	<label class="AStitle"><fmt:message key="credit"/></label>
							   	    	<input name="__partnerCredit_" type="text" id="partnerCredit_${account.id}" onchange="partnerCreditValue(this.value, ${account.id});"/>
							   	  	</fieldset>
							   	  	<div class="button">
										<input type="submit" value="Add"  name="__add_partner_credit_" onclick="addPartnerCredit(${account.id},${model.mainPartner.partnerId});"/>
									</div>
							        </div>	
						        </c:otherwise>
						        </c:choose>
						        </td>
						    </c:if> --%>
							
							
							
							<div class="col-sm-12 col-md-1">
								<div class="quoteHeader visible-xs visible-sm">Quote</div>
								<c:if test="${fn:contains(siteConfig['CUSTOMIZE_SUBACCOUNT_LIST'].value,'quote')}">
									<div class="quote"><c:if test="${account.ordersGrandTotal != null}">&nbsp;</c:if>
										<a href="account_orders.jhtm?getQuoteList=true&id=${account.id}" style="color:blue;">Quotes</a>
									</div>
								</c:if>
								<div class="clearfix"></div>
							</div>
						</div>
						
						
						<!-- Start Credit Modules -->
						<c:if test="${gSiteConfig['gBUDGET']}"> 
							<div class="modal fade" id="credit_modal_1" tabindex="-1" role="dialog" aria-hidden="true">
								<div class="modal-dialog modal-sm">
									<div class="modal-content">
										<div class="modal-header">
											<button type="button" class="close" data-dismiss="modal">
												<span aria-hidden="true">&times;</span>
												<span class="sr-only">Close</span>
											</button>
											<h4 class="modal-title"><fmt:message key="add"/> <fmt:message key="credit"/></h4>
										</div>
										<div class="modal-body">
											<div class="form-group">
												<label><fmt:message key="customer"/> <fmt:message key="name"/></label>
												<input type="text" class="form-control" name="name" value="<fmt:message key="customer"/> <fmt:message key="name"/>" disabled>
											</div>
											<div class="form-group">
												<label><fmt:message key="credit"/> <fmt:message key="available"/></label>
												<input type="text" class="form-control" name="creditAvailable" value="<fmt:formatNumber value="${account.credit}" pattern="#,##0.00" />" disabled>
											</div>
											<div class="form-group">
												<label><fmt:message key="credit"/></label>
												<input type="text" class="form-control" name="__credit_" id="credit_${account.id}" onkeyup="creditValue(this.value, ${account.id});">
											</div>
										</div>
										<div class="modal-footer">
											<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
											<button type="button" class="btn btn-default" value="Add" name="__add_credit_" onclick="addCredit('${account.id}');">Add</button>
										</div>
									</div>
								</div>
							</div>
			
							<div class="modal fade" id="credit_modal_2" tabindex="-1" role="dialog" aria-hidden="true">
								<div class="modal-dialog modal-sm">
									<div class="modal-content">
										<div class="modal-header">
											<button type="button" class="close" data-dismiss="modal">
												<span aria-hidden="true">&times;</span>
												<span class="sr-only">Close</span>
											</button>
											<h4 class="modal-title">Add Credit</h4>
										</div>
										<div class="modal-body">
											<div class="form-group">
												<label><fmt:message key="customer"/> <fmt:message key="name"/></label>
												<input type="text" class="form-control" name="name" value="<c:out value="${account.address.firstName}"/> <c:out value="${account.address.lastName}"/>" disabled>
											</div>
											<div class="form-group">
												<label><fmt:message key="credit"/> <fmt:message key="available"/></label>
												<input type="text" class="form-control" name="creditAvailable" value="<fmt:formatNumber value="${account.credit}" pattern="#,##0.00" />" disabled>
											</div>
											<div class="form-group">
												<label><fmt:message key="credit"/></label>
												<input type="text" class="form-control" name="__credit_" id="credit_${account.id}" onkeyup="creditValue(this.value, ${account.id});">
											</div>
										</div>
										<div class="modal-footer">
											<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
											<button type="button" class="btn btn-default" name="__add_credit_" onclick="addCredit('${account.id}');">Add</button>
										</div>
									</div>
								</div>
							</div>
						</c:if>
							<!-- end Credit Modals -->
			
							<!-- Reward Modals -->
        				<c:if test="${siteConfig['GROUP_REWARDS'].value != null}">
							<div class="modal fade" id="reward_modal_1" tabindex="-1" role="dialog" aria-hidden="true">
								<div class="modal-dialog modal-sm">
									<div class="modal-content">
										<div class="modal-header">
											<button type="button" class="close" data-dismiss="modal">
												<span aria-hidden="true">&times;</span>
												<span class="sr-only">Close</span>
											</button>
											<h4 class="modal-title"><fmt:message key="add"/> <fmt:message key="points"/></h4>
											<div class="availablePoints"><fmt:message key="available"/> <fmt:message key="points"/> : <fmt:formatNumber value="${model.rewardPoints}" pattern="#,##0.00" /></div>
										</div>
										<div class="modal-body">
											<div class="form-group">
												<label><fmt:message key="customer"/> <fmt:message key="name"/></label>
												<input type="text" class="form-control" name="name" value="<c:out value="${account.address.firstName}"/> <c:out value="${account.address.lastName}"/>" disabled>
											</div>
											<div class="form-group">
												<label><fmt:message key="points"/> <fmt:message key="available"/></label>
												<input type="text" class="form-control" name="pointsAvailable" value="<fmt:formatNumber value="${account.rewardPoints}" pattern="#,##0.00" />" disabled>
											</div>
											<div class="form-group">
												<label><fmt:message key="points"/></label>
												<input type="text" id="points_${account.id}" onkeyup="pointsValue(this.value, ${account.id});" class="form-control" name="__points_" value="">
											</div>
										</div>
										<div class="modal-footer">
											<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
											<button type="button" class="btn btn-default" name="__add_points_" onclick="addPoints('${account.id}');">Add</button>
										</div>
									</div>
								</div>
							</div>
			
							<div class="modal fade" id="reward_modal_2" tabindex="-1" role="dialog" aria-hidden="true">
								<div class="modal-dialog modal-sm">
									<div class="modal-content">
										<div class="modal-header">
											<button type="button" class="close" data-dismiss="modal">
												<span aria-hidden="true">&times;</span>
												<span class="sr-only">Close</span>
											</button>
											<h4 class="modal-title">Add Points</h4>
											<div class="availablePoints">Available Points : $0.00</div>
										</div>
										<div class="modal-body">
											<div class="form-group">
												<label>Customer Name</label>
												<input type="text" class="form-control" name="name" value="Gaby Abo Khaled" disabled>
											</div>
											<div class="form-group">
												<label>Points Available</label>
												<input type="text" class="form-control" name="pointsAvailable" value="$0.00" disabled>
											</div>
											<div class="form-group">
												<label>Points</label>
												<input type="text" class="form-control" name="__points_" value="">
											</div>
										</div>
										<div class="modal-footer">
											<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
											<button type="button" class="btn btn-default" name="__add_points_" onclick="addPoints('1');">Add</button>
										</div>
									</div>
								</div>
							</div>
						</c:if>
						<!-- End Credit Modules -->
						
						
					</c:forEach>
					</div>
					<div class="subAccountsFtr">
						<div class="row clearfix"></div>
					</div>
				</div>
			</form>
			</div>
		</div>
		
		
<script type="text/javascript">
<!--
function creditValue(credit,userId) {
	document.getElementById('credit_'+userId).value = credit;
}	
function addCredit(userId) {
	var addCredit =  parseFloat($('#credit_'+userId).val());
	var managerCredit = document.getElementById('managerCredit').value;
	
	if(addCredit > managerCredit) {
		alert("There are no sufficient funds to make the transfer.");
	}else {
		$.ajax({
			   url: "customer-ajax-credit.jhtm",
			   type:'POST',
			   data: "userId="+userId+"&addCredit="+addCredit,
			 complete: function(){
			   document.location.reload(); 
			 }
		});
		<%--
		var request = new Request({
			url: "customer-ajax-credit.jhtm?userId="+userId+"&addCredit="+addCredit,
			method: 'post',
			onComplete: function(response) { 
				location.reload(true);
			}
		}).send(); --%>
	}
}
function pointsValue(points,userId) {
	document.getElementById('points_'+userId).value = points;
}
function addPoints(userId) {
	var addPoints =  parseFloat($('#points_'+userId).val());
	var managerRewardPoints = document.getElementById('managerRewardPoints').value;
	
	if(addPoints > managerRewardPoints) {
		alert("There are no sufficient points to make the transfer.");
	}else {
		$.ajax({
			   url: "customer-ajax-credit.jhtm",
			   type:'POST',
			   data: "userId="+userId+"&addPoints="+addPoints,
			 complete: function(){
			   document.location.reload(); 
			 }
		});
		<%--
		var request = new Request({
			url: "customer-ajax-credit.jhtm?userId="+userId+"&addPoints="+addPoints,
			method: 'post',
			onComplete: function(response) { 
				location.reload(true);
			}
		}).send(); --%>
	}
}
function partnerCreditValue(credit,userId) {
	document.getElementById('partnerCredit_'+userId).value = credit;
}

function addPartnerCredit(userId, partnerId) {
	var addPartnerCredit =  parseFloat($('#partnerCredit_'+userId).val());
	var managerPartnerCredit = document.getElementById('managerPartnerCredit').value;
	
	if(addPartnerCredit > managerPartnerCredit) {
		alert("There are no sufficient funds to make the transfer.");
	}else {
		$.ajax({
			   url: "customer-ajax-credit.jhtm",
			   type:'POST',
			   data: "userId="+userId+"&addPartnerCredit="+addPartnerCredit+"&partnerId="+partnerId,
			 complete: function(){
			   document.location.reload(); 
			 }
		});
		<%--
		var request = new Request({
			url: "customer-ajax-credit.jhtm?userId="+userId+"&addPartnerCredit="+addPartnerCredit+"&partnerId="+partnerId,
			method: 'post',
			onComplete: function(response) { 
				location.reload(true);
			}
		}).send(); --%>
	}
}
function deductCredit(userId) {
	var deductCredit = $('#credit_'+userId).val();
	$.ajax({
		   url: "customer-ajax-credit.jhtm",
		   type:'POST',
		   data: "userId="+userId+"&deductCredit="+deductCredit,
		 complete: function(){
		   document.location.reload(); 
		 }
	});
	<%--
	var request = new Request({
		url: "customer-ajax-credit.jhtm?userId="+userId+"&deductCredit="+deductCredit,
		method: 'post',
		onComplete: function(response) { 
			location.reload(true);
		}
	}).send();--%>
}
//-->
</script>
	
</tiles:putAttribute>
</tiles:insertDefinition>
</c:if>
	