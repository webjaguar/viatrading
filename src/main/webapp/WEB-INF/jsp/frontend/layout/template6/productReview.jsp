<%@ page session="false" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>


<div class="product_rating">
  <div class="rating_stars" style="text-align: center;">
	<c:choose>
      <c:when test="${product.rate.average > 0 and product.rate.average <= 0.6}"><c:set var="star" value="0h"/></c:when>
	  <c:when test="${product.rate.average > 0.6 and product.rate.average <= 1}"><c:set var="star" value="1"/></c:when>
	  <c:when test="${product.rate.average > 1 and product.rate.average <= 1.6}"><c:set var="star" value="1h"/></c:when>
	  <c:when test="${product.rate.average > 1.6 and product.rate.average <= 2}"><c:set var="star" value="2"/></c:when>
	  <c:when test="${product.rate.average > 2 and product.rate.average <= 2.6}"><c:set var="star" value="2h"/></c:when>
	  <c:when test="${product.rate.average > 2.6 and product.rate.average <= 3}"><c:set var="star" value="3"/></c:when>
	  <c:when test="${product.rate.average > 3 and product.rate.average <= 3.6}"><c:set var="star" value="3h"/></c:when>
	  <c:when test="${product.rate.average > 3.6 and product.rate.average <= 4}"><c:set var="star" value="4"/></c:when>
	  <c:when test="${product.rate.average > 4 and product.rate.average <= 4.6}"><c:set var="star" value="4h"/></c:when>
	  <c:when test="${product.rate.average > 4.6}"><c:set var="star" value="5"/></c:when>
	  <c:otherwise><c:set var="star" value="0"/></c:otherwise>
	</c:choose>
	
	<c:choose>
      <c:when test="${siteConfig['PRODUCT_REVIEW_TYPE'].value != 'anonymous'}">
      <a href="${_contextpath}/addProductReview.jhtm?sku=${model.product.sku}&cid=${model.cid}" >
      	<img src="${_contextpath}/assets/Image/Layout/star_${star}.gif" alt="<c:out value="${param.productRateAverage}"/>" title="<c:out value="${param.productRateAverage}"/>" border="0" />
      </a>
    </c:when>
    <c:otherwise>
      <a href="${_contextpath}/productReviewAnon.jhtm?sku=${model.product.sku}&cid=${model.cid}" >
      	<img src="${_contextpath}/assets/Image/Layout/star_${star}.gif" alt="<c:out value="${param.productRateAverage}"/>" title="<c:out value="${param.productRateAverage}"/>" border="0" />
      </a>
    </c:otherwise>
    </c:choose> 
  </div>
  <div class="clearfix"></div>
  <div class="rating_note">
    <c:if test="${product.rate != null}">
      <a href="${model.url}#reviews"><c:out value="${product.rate.numReview}" /> 
      	<c:choose>
      	  <c:when test="${product.rate.numReview > 1}">
      	  	<fmt:message key="reviews" />
      	  </c:when>
      	  <c:otherwise>
      	  	Review
      	  </c:otherwise>
      	</c:choose>
      </a> | 
    </c:if>
    <c:choose>
      <c:when test="${siteConfig['PRODUCT_REVIEW_TYPE'].value != 'anonymous'}">
        <a href="${_contextpath}/addProductReview.jhtm?sku=${model.product.sku}&cid=${model.cid}" ><fmt:message key="writeReview" /></a>
      </c:when>
      <c:when test="${siteConfig['PRODUCT_REVIEW_TYPE'].value == 'anonymous'}">
        <a href="${_contextpath}/productReviewAnon.jhtm?sku=${model.product.sku}&cid=${model.cid}" ><fmt:message key="writeReview" /> </a>
      </c:when>
    </c:choose>
    
  </div>
</div>