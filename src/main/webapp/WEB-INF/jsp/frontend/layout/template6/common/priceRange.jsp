<%@ page session="false" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>

<c:if test="${product.priceRangeMinimum != null}">
  <c:choose>
  	<c:when test="${!product.loginRequire or (product.loginRequire and userSession != null)}" >
  	  <c:if test="${product.priceRangeMinimum > 0 || siteConfig['SHOW_ZERO_PRICE'].value == 'true'}">
  	  
		<c:set var="multiplier" value="1"/>
		<c:if test="${siteConfig['CASE_CONTENT_LAYOUT'].value == '1' and product.caseContent > 0}">
		  <c:set var="multiplier" value="${product.caseContent}"/>	
		</c:if>  	  
  	  <c:choose>
  	  	<c:when test="${siteConfig['GROUP_REWARDS'].value == 'true' and product.productType == 'REW'}">
  	  	<div class="price_range">
	  	  <span class="price_title"><fmt:message key="f-price-range" />: </span>
		  <span class="p_from_id"><c:out value="${siteConfig['GROUP_REWARD_POINTS_TITLE'].value}" /> <fmt:formatNumber value="${product.priceRangeMinimum}" pattern="#,##0.00" /></span>
		  <c:if test="${product.priceRangeMaximum != null and product.priceRangeMaximum > product.priceRangeMinimum}">
		  	<span class="price_range_separator">-</span>
		  	<span class="p_to_id"><c:out value="${siteConfig['GROUP_REWARD_POINTS_TITLE'].value}" /> <fmt:formatNumber value="${product.priceRangeMaximum}" pattern="#,##0.00" /></span>
		  </c:if>
		</div>	
  	  	</c:when>
  	  	<c:otherwise>
	  	  <c:choose>
	  	    <c:when test="${product.salesTag != null}">
		  	  <div class="price_range">
		  	    <span class="price_title"><fmt:message key="f-price-range" />: </span>
			    <span class="strikethrough">
					<span class="old_price"><fmt:message key="${siteConfig['CURRENCY'].value}" /><fmt:formatNumber value="${product.originalPriceRangeMinimum*multiplier}" pattern="#,##0.00" /></span>
				</span>
				<c:if test="${product.originalPriceRangeMaximum != null and product.originalPriceRangeMaximum > product.originalPriceRangeMinimum}">
				    <span class="price_range_separator">-</span>
			        <span class="strikethrough">
						<span class="new_price"><fmt:message key="${siteConfig['CURRENCY'].value}" /><fmt:formatNumber value="${product.originalPriceRangeMaximum*multiplier}" pattern="#,##0.00" /></span>
				    </span>
			    </c:if>
			  </div>
		  	  <div class="price_range">
		  	    <span class="price_title">Sale For: </span>
			    <span class="p_from_id"><fmt:message key="${siteConfig['CURRENCY'].value}" /><fmt:formatNumber value="${product.priceRangeMinimum*multiplier}" pattern="#,##0.00" /><c:if test="${siteConfig['CASE_CONTENT_LAYOUT'].value == '1' and product.caseContent > 0 and product.packing != ''}"><c:out value="${product.packing}"/></c:if></span>
			      <c:if test="${product.priceRangeMaximum != null and product.priceRangeMaximum > product.priceRangeMinimum}">
			        <span class="price_range_separator">-</span>
			        <span class="p_to_id"><fmt:message key="${siteConfig['CURRENCY'].value}" /><fmt:formatNumber value="${product.priceRangeMaximum*multiplier}" pattern="#,##0.00" /></span>
			      </c:if>
			  </div>
	  	    </c:when>  
	  	    <c:otherwise>
		  	  <div class="price_range">
		  	    <span class="price_title"><fmt:message key="f-price-range" />: </span>
			    <span class="p_from_id"><fmt:message key="${siteConfig['CURRENCY'].value}" /><fmt:formatNumber value="${product.priceRangeMinimum*multiplier}" pattern="#,##0.00" /><c:if test="${siteConfig['CASE_CONTENT_LAYOUT'].value == '1' and product.caseContent > 0 and product.packing != ''}"><c:out value="${product.packing}"/></c:if></span>
			      <c:if test="${product.priceRangeMaximum != null and product.priceRangeMaximum > product.priceRangeMinimum}">
			        <span class="price_range_separator">-</span>
			        <span class="p_to_id"><fmt:message key="${siteConfig['CURRENCY'].value}" /><fmt:formatNumber value="${product.priceRangeMaximum*multiplier}" pattern="#,##0.00" /></span>
			      </c:if>
			  </div>
	  	    </c:otherwise>
	  	  </c:choose>
	  	  
  	  	</c:otherwise>
  	  </c:choose>
	  </c:if>
	</c:when>
	<c:otherwise>
	  <a href="${_contextpath}/login.jhtm?noMessage=t&forwardAction=<c:out value='${model.url}'/>" ><img border="0" name="_logintoviewprice" class="_logintoviewprice" src="${_contextpath}/assets/Image/Layout/login_to_view_price${_lang}.gif" alt="Login to view Price"/></a>
	</c:otherwise>
  </c:choose>
</c:if>
