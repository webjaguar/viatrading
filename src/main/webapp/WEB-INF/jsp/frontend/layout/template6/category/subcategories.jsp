<%@ page session="false" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>


<div class="col-sm-12">
  <div class="category_head_name">
	<h1><c:out value="${model.thisCategory.name}" escapeXml="false" /></h1>
  </div>
</div>
										
<c:forEach items="${model.subCategories}" var="category" varStatus="status">
<div class="col-sm-6 col-md-4 col-lg-3">
  <div class="category_wrapper">
	<c:set var="subCatLink" value="#"></c:set>
	<c:if test="${category.linkType != 'NONLINK'}">      
	  <c:choose>
	    <c:when test="${gSiteConfig['gMOD_REWRITE'] == '1'}">
	      <c:set var="subCatLink" value="${_contextpath}/${siteConfig['MOD_REWRITE_CATEGORY'].value}/${category.id}/${category.encodedName}.html"></c:set>
	    </c:when>
	    <c:otherwise>
		  <c:set var="subCatLink" value="${_contextpath}/category.jhtm?cid=${category.id}"></c:set>
	    </c:otherwise>
	  </c:choose>
	</c:if>
   
	<a href="${subCatLink}" class="sub_category_link">
	  <div class="category_image_wrapper">
		<c:choose>
		  <c:when test="${ not empty (category.imageUrl) and category.imageUrl != null}">
		  	<c:choose>
		      <c:when test="${category.absolute}">
		    	<img src="<c:url value="${category.imageUrl}"/>" class="img-responsive center-block category_image" border="0" alt="${category.imageUrlAltName}">
		      </c:when>
		      <c:otherwise>
		    	<img src="<c:url value="${_contextpath}/assets/Image/Category/${category.imageUrl}"/>" class="img-responsive center-block category_image" border="0" alt="${category.imageUrlAltName}">
		      </c:otherwise>
		    </c:choose>
		  </c:when>
		  <c:when test="${category.hasImage}">
			<img src="<c:url value="${_contextpath}/assets/Image/Category/cat_${category.id}.gif"/>" class="img-responsive center-block category_image" border="0" alt="${category.imageUrlAltName}">
		  </c:when>
	    </c:choose>
	  </div>
	  <div class="category_name_wrapper">
		<div class="category_name"><c:out value="${category.name}" escapeXml="false" /></div>
	  </div>
	</a>
  </div>
</div>
</c:forEach>