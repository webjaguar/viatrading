<%@ page session="false" %>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>

<tiles:insertDefinition name="${_template}" flush="true">
  <tiles:putAttribute name="content" type="string">
<c:if test="${gSiteConfig['gCOMPARISON']}">

<div class="comparisonTitle"><c:out value="${siteConfig['COMPARISON_TITLE'].value}"/></div>

<c:if test="${model.message != null}">
  <div class="message"><fmt:message key="${model.message}"><fmt:param value="${siteConfig['COMPARISON_MAX'].value}"/></fmt:message></div>
</c:if>

<c:if test="${fn:length(model.products) == 0}">
  <div class="message"><fmt:message key="comparison.empty" /></div>
</c:if>


<c:if test="${fn:length(model.products)> 0}">
<script language="JavaScript">
<!--
function toggleAll(el) {
	var ids = document.getElementsByName("__selected_id");	
  	if (ids.length == 1)
      document.form.__selected_id.checked = el.checked;
    else
      for (i = 0; i < ids.length; i++)
        document.form.__selected_id[i].checked = el.checked;	
}  

function deleteSelected() {
	var ids = document.getElementsByName("__selected_id");	
  	if (ids.length == 1) {
      if (document.form.__selected_id.checked) {
    	return true;
      }
    } else {
      for (i = 0; i < ids.length; i++) {
        if (document.form.__selected_id[i].checked) {
    	  return true;
        }
      }
    }

    alert("Please select product(s) to remove.");       
    return false;
}

function addToCart(aform) {
    aform.action = 'addToCart.jhtm';
    submit();
}
//-->
</script>


  <div class="products_comparison">
	
    <div class="row">
	  <div class="col-sm-12 col-md-3 col-lg-3">
		<div class="backToResultsWrapper">
			<a href="javascript:history.back(1);" class="backToResults">Back to results</a>
		</div>
		<div class="clearfix"></div>
	  </div>
	  
	  <c:forEach items="${model.products}" var="product" varStatus="status">
        <c:set var="productLink">${_contextpath}/product.jhtm?id=${product.id}<c:if test="${product == null or siteConfig['RECOMMENDED_LIST_SHOW_BREADCRUMB'].value == 'true'}">&cid=<c:out value="${param.cid}"/></c:if></c:set>
		<c:if test="${gSiteConfig['gMOD_REWRITE'] == '1' and not fn:contains(product.sku, '/')}">
		  <c:set var="productLink">${_contextpath}/${siteConfig['MOD_REWRITE_PRODUCT'].value}/${product.encodedSku}/${product.encodedName}.html<c:if test="${model.product == null or siteConfig['RECOMMENDED_LIST_SHOW_BREADCRUMB'].value == 'true'}">?cid=<c:out value="${param.cid}"/></c:if></c:set>
		</c:if>	
    
	    <div class="col-sm-4 col-md-3 col-lg-3">
		  <div class="product_wrapper">
		    <div class="product_image_wrapper">
			  <a href="${productLink}">
			    <img src="<c:if test="${!product.thumbnail.absolute}">${_contextpath}/assets/Image/Product/thumb/</c:if>${product.thumbnail.imageUrl}" class="img-responsive center-block product_image">
			  </a>
		    </div>
		    <div class="product_name_wrapper">
			  <div class="product_name">
			    <a href="${productLink}"> <c:out value="${product.name}" escapeXml="false" /> </a>
			  </div>
		    </div>
		    
		   <c:if test="${!product.loginRequire or (product.loginRequire and userSession != null)}" >
  	       <div class="price_range">
				  <span class="price_title"> <fmt:message key="price" />: </span>
				  <span class="p_from_id"><fmt:message key="${siteConfig['CURRENCY'].value}" /><fmt:formatNumber value="${product.priceRangeMinimum}" pattern="#,##0.00#" maxFractionDigits="${siteConfig['MAX_FRACTION_DIGIT'].value}" /></span>
				  <c:if test="${product.priceRangeMaximum != null }">
				    <span class="price_range_separator">-</span>
				    <span class="p_to_id"><fmt:message key="${siteConfig['CURRENCY'].value}" /><fmt:formatNumber value="${product.priceRangeMaximum}" pattern="#,##0.00#" maxFractionDigits="${siteConfig['MAX_FRACTION_DIGIT'].value}" /></span>
				  </c:if>
			  </div>
			  <form class="addToCartForm clearfix" action="/addToCart.jhtm" method="post">
		      <input type="hidden" name="product.id" value="${product.id}">
			  <div class="qty_wrapper">
				<span class="qty_title">Quantity</span>
				<c:choose>
				   <c:when test="${gSiteConfig['gINVENTORY'] and product.inventory != null and !product.negInventory and product.inventory <= 0}" >
				   	 <input name="quantity_${product.id}" disabled="disabled" size="5" maxlength="5" class="qty_input disabled" type="text" autocomplete="off" value="${product.minimumQty}">	
				   </c:when>
				   <c:otherwise>
					<input name="quantity_${product.id}" size="5" maxlength="5" class="qty_input" type="text" autocomplete="off" value="${product.minimumQty}">	
				   </c:otherwise>
				</c:choose>
			  </div>
		      <div class="btn-group">
			    <button type="submit" class="btn addToCart_btn">
				  Add To Cart &nbsp;<span class="addToCart_icon"></span>
			    </button>
			    <a href="/addToList.jhtm?product.id=${product.id}" class="btn addToList_btn" data-toggle="tooltip" title="Add To List">
				  <span class="addToList_icon"></span>
			    </a>
			  </div>
		    </form>
		    </c:if>
		  </div>
	    </div>
	  </c:forEach>
	</div>
	
	<form name="form" action="${_contextpath}/comparison.jhtm" method="post">
	<div class="row">
	  <div class="comparison_group_title">General</div>
	</div>
	
	<c:set var="rowCount" value="1" />
    <c:if test="${rowCount % 2 == 0}">
	  <c:set var="rowClass" value="even_row" />
	</c:if>
	<c:if test="${rowCount % 2 != 0}">
	  <c:set var="rowClass" value="odd_row" />
	</c:if>
	
		    	
	
	<div class="row">
	  <div class="comparison_group">
		<c:set var="rowClass" value="odd_row" />
		<div class="${rowClass} clearfix">
		  <c:set var="rowCount" value="${rowCount + 1}" />
    	  <div class="col-sm-12 col-md-3 col-lg-3">
			<div class="spec_title"><fmt:message key="product" /></div>
			<div class="clearfix"></div>
		  </div>
		    
		  <c:forEach items="${model.products}" var="product" varStatus="status">
            <div class="col-sm-12 col-md-3 col-lg-3">
			  <div class="sku_label visible-xs visible-sm"><c:out value="${product.sku}" escapeXml="false" /></div>
			  <div class="spec_info"><c:out value="${product.name}" escapeXml="false" /></div>
			  <div class="clearfix"></div>
		    </div>
		  </c:forEach>
		</div>
		
		
		<c:if test="${siteConfig['COMPARISON_SHORT_DESC_TITLE'].value != ''}">
		  <c:if test="${rowCount % 2 == 0}">
			<c:set var="rowClass" value="even_row" />
		  </c:if>
		  <c:if test="${rowCount % 2 != 0}">
			<c:set var="rowClass" value="odd_row" />
		  </c:if>
	
		  <div class="${rowClass} clearfix">
		    <c:set var="rowCount" value="${rowCount + 1}" />
    	  
		    <div class="col-sm-12 col-md-3 col-lg-3">
			  <div class="spec_title">Description</div>
			  <div class="clearfix"></div>
		    </div>
		    <c:forEach items="${model.products}" var="product" varStatus="status">
        	<div class="col-sm-12 col-md-3 col-lg-3">
			  <div class="sku_label visible-xs visible-sm"><c:out value="${product.sku}" escapeXml="false" /></div>
			  <div class="spec_info"><c:out value="${product.shortDesc}" escapeXml="false" /></div>
			  <div class="clearfix"></div>
		    </div>
		    </c:forEach>
		  </div>
		</c:if>
		
		<c:if test="${!product.loginRequire or (product.loginRequire and userSession != null)}" >
  	      <c:if test="${rowCount % 2 == 0}">
			<c:set var="rowClass" value="even_row" />
		  </c:if>
		  <c:if test="${rowCount % 2 != 0}">
			<c:set var="rowClass" value="odd_row" />
		  </c:if>
	      <div class="${rowClass} clearfix">
		    <c:set var="rowCount" value="${rowCount + 1}" />
    	  
		    <div class="col-sm-12 col-md-3 col-lg-3">
			  <div class="spec_title">Price</div>
			  <div class="clearfix"></div>
		    </div>
		    <c:forEach items="${model.products}" var="product" varStatus="status">
        	<div class="col-sm-12 col-md-3 col-lg-3">
			  <div class="sku_label visible-xs visible-sm"><c:out value="${product.sku}" escapeXml="false" /></div>
			  <div class="spec_info">
			    <div class="comparisonPrice"><fmt:message key="${siteConfig['CURRENCY'].value}" /><fmt:formatNumber value="${product.priceRangeMinimum}" pattern="#,##0.00#" maxFractionDigits="${siteConfig['MAX_FRACTION_DIGIT'].value}" /></div>
			  </div>
			  <div class="clearfix"></div>
		    </div>
			</c:forEach>
		  </div>
		</c:if>
		
		
		<c:set var="fieldCount" value="1" />
		<c:if test="${rowClass == 'even_row'}">
		  <c:set var="fieldCount" value="2" />
		</c:if>
		<c:forEach items="${model.productFields}" var="productField" varStatus="fieldStatus">
		  <c:if test="${productField.comparisonField}">
			<c:if test="${rowCount % 2 == 0}">
			  <c:set var="rowClass" value="even_row" />
			</c:if>
			<c:if test="${rowCount % 2 != 0}">
			  <c:set var="rowClass" value="odd_row" />
			</c:if>
	      
		  	<div class="${rowClass} clearfix">
			  <c:set var="rowCount" value="${rowCount + 1}" />
    	  	  <div class="col-sm-12 col-md-3 col-lg-3">
				<div class="spec_title"><c:out value="${productField.name}" escapeXml="false" /></div>
				<div class="clearfix"></div>
			  </div>
				         
			  <c:forEach items="${model.products}" var="product">
		  	    <c:forEach items="${product.productFields}" var="productField2">
		          <c:if test="${productField2.comparisonField and (productField.id == productField2.id)}">
		            <div class="col-sm-12 col-md-3 col-lg-3">
					  <div class="sku_label visible-xs visible-sm"><c:out value="${product.sku}" escapeXml="false" /></div>
					  <div class="spec_info"><c:out value="${productField2.value}" escapeXml="false" /></div>
					  <div class="clearfix"></div>
				    </div>
				  </c:if>
		  	    </c:forEach>
		      </c:forEach>
		    </div>
		  </c:if>
		</c:forEach>
		
		<c:forEach items="${model.productFieldsUnlimited}" var="productField" varStatus="fieldStatus">
		  <c:if test="${productField.comparisonField}">
			<c:if test="${rowCount % 2 == 0}">
			  <c:set var="rowClass" value="even_row" />
			</c:if>
			<c:if test="${rowCount % 2 != 0}">
			  <c:set var="rowClass" value="odd_row" />
			</c:if>
	      
		  	<div class="${rowClass} clearfix">
			  <c:set var="rowCount" value="${rowCount + 1}" />
    	  	  <div class="col-sm-12 col-md-3 col-lg-3">
				<div class="spec_title"><c:out value="${productField.displayName}" escapeXml="false" /></div>
				<div class="clearfix"></div>
			  </div>
				         
			  <c:forEach items="${model.products}" var="product">
		  	    <c:set var="fieldFound" value="false"/>
	    	    <c:forEach items="${product.productFieldsUnlimitedNameValue}" var="productField2">
		          <c:if test="${productField.name == productField2.name}">
                    <c:set var="fieldFound" value="true"/>
                    <div class="col-sm-12 col-md-3 col-lg-3">
					  <div class="sku_label visible-xs visible-sm"><c:out value="${product.sku}" escapeXml="false" /></div>
					  <div class="spec_info"><c:out value="${productField2.displayValue}" escapeXml="false" /></div>
					  <div class="clearfix"></div>
				    </div>
				  </c:if>
		  	    </c:forEach>
		  	    <c:if test="${fieldFound == 'false'}">
		          <div class="col-sm-12 col-md-3 col-lg-3">
					<div class="sku_label visible-xs visible-sm"><c:out value="${product.sku}" escapeXml="false" /></div>
					<div class="spec_info"><c:out value="${productField2.value}" escapeXml="false" /></div>
					<div class="clearfix"></div>
				  </div>
		        </c:if>
			  </c:forEach>
		    </div>
		  </c:if>
		</c:forEach>
		
		
		<c:if test="${rowCount % 2 == 0}">
		  <c:set var="rowClass" value="even_row" />
		</c:if>
		<c:if test="${rowCount % 2 != 0}">
		  <c:set var="rowClass" value="odd_row" />
		</c:if>
	      
		<div class="${rowClass} clearfix">
		  <div class="col-sm-12 col-md-3 col-lg-3 hidden-xs hidden-sm"></div>
		  <c:forEach items="${model.products}" var="product">
		  <div class="col-sm-12 col-md-3 col-lg-3">
			<div class="product_sku visible-xs visible-sm"><c:out value="${product.sku}" escapeXml="false" /></div>
			<div class="p_select_checkbox_wrapper">
			  <input name="__selected_id" value="${product.id}" type="checkbox" class="p_select_checkbox">
			</div>
			<div class="clearfix"></div>
		  </div>
		  </c:forEach>
		</div>
		  	  
		
		
		
	  </div>
	</div>
    
    <div class="row">
	  <div class="col-sm-12">
		<div class="compareRemoveButtonWrapper">
		  <input type="submit" value="Remove" name="__delete" class="btn compareRemoveButton" onClick="return deleteSelected()"></input>
		</div>
	  </div>
	</div>
		
    </form>


</div>

</c:if>

</c:if>
  </tiles:putAttribute>
</tiles:insertDefinition>
