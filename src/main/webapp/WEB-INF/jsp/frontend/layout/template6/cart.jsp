<%@ page session="false" %>
<%@ page import="com.webjaguar.model.*, java.text.NumberFormat, java.util.*" %>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page session="false" %>


<script src="/dv/w3data.js"></script>
<script type='text/javascript' src='/wp-includes/js/jquery/jquery.js?ver=1.12.4'></script>
<script type='text/javascript' src='/wp-content/themes/bridge/js/plugins.js?ver=4.6.1'></script>
<script type='text/javascript' src='/wp-content/themes/bridge/js/default.min.js?ver=4.6.1'></script>


<tiles:insertDefinition name="${_template}" flush="true">
  <c:if test="${_leftBar != '1' and _leftBar != '4'}">
    <tiles:putAttribute name="leftbar" value="/WEB-INF/jsp/frontend/layout/template6/leftbar.jsp" />
  </c:if>
  <tiles:putAttribute name="content" type="string">

<c:if test="${siteConfig['BUY_SAFE_URL'].value != ''}">
<script type="text/javascript">
<!--
function buySAFEOnClick(NewWantsBondValue) {
	document.cart.WantsBondField.value = NewWantsBondValue;
    document.cart.submit();
}
//-->
</script>
</c:if>

<c:if test="${model.cXmlEncodedCart != null}">
<script type="text/javascript">
<!--
function clearCart() {
	try {
		new Request.HTML ({
			url: "${_contextpath}/clear-ajax-cart.jhtm",
			onSuccess: function(response){
			},
			onFailure: function(response){
			}
		}).send();
	}catch(err){}
	
	// submit cart to punchout buyer. Wait for 1 sec otherwise cart will not be cleared as Chrome blocks this post request
	setTimeout(submitForm,1000);
}
function submitForm() {
	$('checkoutForm').submit();
}
//-->
</script>
</c:if>

<script type="text/javascript">
<!--
function toggleChildSkus(skuGroup) {
	var show = false;
	$$('.'+skuGroup).each(function(ele){
		if(ele.style.display == ''){
			ele.style.display= "none";
			show = true;
			$('toggleBoxId'+skuGroup).set('text', '<fmt:message key="details" />');
		} else {
			ele.style.display= "";
			show = false;
			$('toggleBoxId'+skuGroup).set('text', '<fmt:message key="hide" />');
		}
	});
	if(!show){
		$('parentSkuTotalQtyHdr'+skuGroup).style.display = 'none';
		$('parentSkuTotalPriceHdr'+skuGroup).style.display = 'none';
		$('parentSkuTotalQtyFtr'+skuGroup).style.display = '';
		$('parentSkuTotalPriceFtr'+skuGroup).style.display = '';
	} else {
		$('parentSkuTotalQtyHdr'+skuGroup).style.display = '';
		$('parentSkuTotalPriceHdr'+skuGroup).style.display = '';
		$('parentSkuTotalQtyFtr'+skuGroup).style.display = 'none';
		$('parentSkuTotalPriceFtr'+skuGroup).style.display = 'none';
	}
}
//-->
</script>


<div class="col-sm-12">
  
  <c:if test="${fn:trim(model.cartLayout.headerHtml) != ''}">
    <c:set value="${model.cartLayout.headerHtml}" var="headerHtml"/>
    <c:set value="${fn:replace(headerHtml, '#cartsubtotal#', cartSubTotal)}" var="headerHtml"/>
    <c:out value="${headerHtml}" escapeXml="false"/>
  </c:if>
  
  <div class="shoppingCartWizard clearfix">
			<div class="wizard-bar-wrapper">
				<ul class="wizard-bar">
					<li class="wizard-step active">Cart</li>
					<li class="wizard-step">Shipping</li>
					<li class="wizard-step">Checkout</li>
				</ul>
			</div>
		</div>

  <div class="cartWrapper clearfix">
	<div class="headerTitle shoppingCart"></div>

	<form action="viewCart.jhtm" method="post" name="cart">
	
	<div class="shoppingCartWrapper">
			<div class="clearfix">
				<div class="shoppingCartTitle">Shopping Cart</div>
			</div>

			<!-- 
			<div class="clearfix">
				<div class="error_message">Your Shopping Cart is empty.</div>
			</div>
			-->
			
			<div class="shoppingCart_table">
				<div class="shoppingCartHdr hidden-xs hidden-sm">
					<div class="row">
						<div class="col-sm-12 col-md-1">
							<div class="cartImageHeader">Image</div>
						</div>
						<div class="col-sm-12 col-md-3">
							<div class="cartNameHeader">Name</div>
						</div>
						<div class="col-sm-12 col-md-1">
							<div class="cartPackingHeader">Packing</div>
						</div>
						<div class="col-sm-12 col-md-1">
							<div class="cartWeightHeader">Weight</div>
						</div>
						<div class="col-sm-12 col-md-1">
							<div class="cartQtyHeader">Quantity</div>
						</div>
						<div class="col-sm-12 col-md-1">
							<div class="cartUnitsHeader"># Units</div>
						</div>
						<div class="col-sm-12 col-md-2">
							<div class="cartPriceHeader">Price/Lot</div>
						</div>
						<div class="col-sm-12 col-md-1">
							<div class="cartSubtotalHeader">Subtotal</div>
						</div>
						<div class="col-sm-12 col-md-1">
							<div class="cartRemoveHeader">Remove</div>
						</div>
					</div>
				</div>

				<div class="shoppingCartMobileHdr visible-xs visible-sm">
					<div class="row">
						<div class="col-sm-12">
							Cart Items
						</div>
					</div>
				</div>

				<div class="shoppingCartDetails">
					
					<c:forEach items="${model.cart.cartItems}" var="cartItem" varStatus="cartStatus">
					<c:set var="productLink2">${_contextpath}/product.jhtm?id=${cartItem.product.id}<c:if test="${model.cartItem.product == null or siteConfig['RECOMMENDED_LIST_SHOW_BREADCRUMB'].value == 'true'}">&cid=<c:out value="${param.cid}"/></c:if></c:set>
					<c:if test="${gSiteConfig['gMOD_REWRITE'] == '1' and not fn:contains(cartItem.product.sku, '/')}">
						<c:set var="productLink2">${_contextpath}/${siteConfig['MOD_REWRITE_PRODUCT'].value}/${cartItem.product.encodedSku}/${cartItem.product.encodedName}.html<c:if test="${model.cartItem.product == null or siteConfig['RECOMMENDED_LIST_SHOW_BREADCRUMB'].value == 'true'}">?cid=<c:out value="${param.cid}"/></c:if></c:set>
					</c:if>
					
					<div class="even_row clearfix">
						<div class="col-sm-12 col-md-1">
							<div class="cartImageHeader visible-xs visible-sm">Image</div>
							<div class="cartImageWrapper">
								<a href="${productLink2}"><img class="cartImage img-responsive" src="<c:if test="${!cartItem.product.thumbnail.absolute}">${_contextpath}/assets/Image/Product/thumb/</c:if>${cartItem.product.thumbnail.imageUrl}" border="0" alt="${cartItem.product.alt}"/></a>
							</div>
							<div class="clearfix"></div>
						</div>
						<div class="col-sm-12 col-md-3">
							<div class="cartNameHeader visible-xs visible-sm">Name</div>
							<div class="cartName">
								<div class="cartItemNameSkuWrapper">
									<a class="cart_item_name" href="${productLink2}">${cartItem.product.name}</a>
									<br>
									<a class="cart_item_sku" href="#">
										<span class="sku_label">SKU:</span>
										${cartItem.product.sku}
									</a>
								</div>
							</div>
							<c:if test="${cartItem.includeRetailDisplay}">
								Include Retail Display
							</c:if>
							<div class="clearfix"></div>
						</div>
						<div class="col-sm-12 col-md-1">
							<div class="cartPackingHeader visible-xs visible-sm">Packing</div>
							<div class="cartPacking">${cartItem.product.packing}</div>
							<div class="clearfix"></div>
						</div>
						<div class="col-sm-12 col-md-1">
							<div class="cartWeightHeader visible-xs visible-sm">Weight</div>
							<div class="cartWeight">${cartItem.product.weight} lbs</div>
							<div class="clearfix"></div>
						</div>
						
						<div class="col-sm-12 col-md-1">
							<div class="cartQtyHeader visible-xs visible-sm">Quantity</div>
							<div class="cartQty">
							<input type="hidden" name="index" value="<c:out value="${cartItem.index}"/>">
							<input type="text" class="touchspin form-control" value="${cartItem.quantity}" style="display: block;" name="qty_${cartItem.index}">
							<span class="input-group-btn-vertical">
							</span>
							</div>
							<div class="clearfix"></div>
						</div>
		
						<div class="col-sm-12 col-md-1">
							<div class="cartUnitsHeader visible-xs visible-sm"># Units</div>
							<div class="cartUnits">${cartItem.product.field7}</div>
							<div class="clearfix"></div>
						</div>
						<div class="col-sm-12 col-md-2">
							<div class="cartPriceHeader visible-xs visible-sm">Price/Lot</div>
							<div class="cartPrice">
								<fmt:message key="${siteConfig['CURRENCY'].value}" /><fmt:formatNumber value="${cartItem.unitPrice}" pattern="###,##0.00"/>
							</div>
							<div class="clearfix"></div>
						</div>
						<div class="col-sm-12 col-md-1">
							<div class="cartSubtotalHeader visible-xs visible-sm">Subtotal</div>
							<div class="cartSubtotal"><fmt:message key="${siteConfig['CURRENCY'].value}" /><fmt:formatNumber value="${cartItem.total}" pattern="###,##0.00"/></div>
							<div class="clearfix"></div>
						</div>
						<div class="col-sm-12 col-md-1">
							<div class="cartRemoveHeader visible-xs visible-sm">Remove</div>
							<div class="cartRemove">
								<input type="checkbox" name="remove" value="${cartItem.index}">
							</div>
							<div class="clearfix"></div>
						</div>
					</div>
					</c:forEach>

				</div>

				<div class="shoppingCartFtr">
					<div class="row">
						<div class="hidden-xs hidden-sm col-md-offset-1 col-md-4">
							<span class="total_label">TOTAL</span>
						</div>
						<div class="col-sm-12 col-md-1">
							<div class="row">
								<div class="col-xs-4 col-sm-4 hidden-md hidden-lg">
									<div class="totalWeight_label">Total Weight:</div>
								</div>
								<div class="col-xs-8 col-sm-4 col-md-12">
									<div class="totalWeight_value">${model.cart.weight} lbs</div>
								</div>
							</div>
						</div>
						<div class="col-sm-12 col-md-1">
							<div class="row">
								<div class="col-xs-4 col-sm-4 hidden-md hidden-lg">
									<div class="totalQty_label">Total Quantity:</div>
								</div>
								<div class="col-xs-8 col-sm-4 col-md-12">
									<div class="totalQty_value">${model.cart.quantity}</div>
								</div>
							</div>
						</div>
						<div class="col-sm-12 col-md-offset-1 col-md-2">
							<div class="row">
								<div class="col-xs-4 col-sm-4 hidden-md hidden-lg">
									<div class="grandTotal_label">GRAND TOTAL</div>
								</div>
								<div class="col-xs-8 col-sm-4 col-md-12">
									<div class="grandTotal_value">
										<fmt:message key="${siteConfig['CURRENCY'].value}" /><fmt:formatNumber value="${model.cart.subTotal}" pattern="###,##0.00"/>
									</div>
								</div>
							</div>
						</div>	
					</div>
					
					
				</div>
			</div>

			<div class="buttons_wrapper clearfix">
				<div class="btn_wrapper update_btn_wrapper">
					<button class="btn btn-lg update_btn" type="submit" name="_update" value=""><fmt:message key="update" />&nbsp;<span class="update_icon"></span></button>			
				</div>
				<div class="btn_wrapper getShippingOptions_btn_wrapper">				
					<c:choose>
 							<c:when test="${gSiteConfig['gCHECKOUT_MODE'] == '1'}">
								<button type="submit" class="btn btn-lg getShippingOptions_btn" border="0" name="_checkout.x">Get Shipping Options &nbsp;<span class="getShippingOptions_icon"></span></button>
							</c:when>
							<c:when test="${gSiteConfig['gCHECKOUT_MODE'] == '2'}">
    							<button type="submit" class="btn btn-lg getShippingOptions_btn" border="0" name="_checkout1.x">Get Shipping Options &nbsp;<span class="getShippingOptions_icon"></span></button>
							</c:when>
							<c:when test="${gSiteConfig['gCHECKOUT_MODE'] == '3'}">
    							<button type="submit" class="btn btn-lg getShippingOptions_btn" border="0" name="_checkout3.x">Get Shipping Options &nbsp;<span class="getShippingOptions_icon"></span></button>
							</c:when>
							<c:when test="${gSiteConfig['gCHECKOUT_MODE'] == '4' && siteConfig['SHIPPING_PER_SUPPLIER'].value == 'true'}">
								<button type="submit" class="btn btn-lg getShippingOptions_btn" border="0" name="_checkout4.x">Get Shipping Options &nbsp;<span class="getShippingOptions_icon"></span></button>
							</c:when>
							<c:otherwise>
    							<button type="submit" class="btn btn-lg getShippingOptions_btn" border="0" name="_checkout1.x">Get Shipping Options &nbsp;<span class="getShippingOptions_icon"></span></button>
 							</c:otherwise>
					</c:choose>		
				</div>
			</div>
			
			<c:if test="${fn:trim(model.cartLayout.footerHtml) != ''}">
  				<c:set value="${model.cartLayout.footerHtml}" var="footerHtml"/>
  				<c:set value="${fn:replace(footerHtml, '#cartsubtotal#', cartSubTotal)}" var="footerHtml"/>
  				<c:out value="${footerHtml}" escapeXml="false"/>
			</c:if>
		
			
		</div>
   </form> 
  </div>
  
  

  
  
</div>

	</tiles:putAttribute>
</tiles:insertDefinition>
