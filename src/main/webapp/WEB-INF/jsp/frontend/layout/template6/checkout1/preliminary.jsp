<%@ page session="false" %>
<%@ page import="com.webjaguar.model.*, java.text.NumberFormat, java.util.*" %>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page session="false" %>

<script src="/dv/w3data.js"></script>
<script type='text/javascript' src='/wp-includes/js/jquery/jquery.js?ver=1.12.4'></script>
<script type='text/javascript' src='/wp-content/themes/bridge/js/plugins.js?ver=4.6.1'></script>
<script type='text/javascript' src='/wp-content/themes/bridge/js/default.min.js?ver=4.6.1'></script><!---->

<tiles:insertDefinition name="${_template}" flush="true">
  <c:if test="${_leftBar != '1' and _leftBar != '4'}">
    <tiles:putAttribute name="leftbar" value="/WEB-INF/jsp/frontend/layout/template6/leftbar.jsp" />
  </c:if>
  <tiles:putAttribute name="content" type="string">
<title>Home</title>


<script type="text/javascript">
<!--

$(window).load(function() {
	
	$(window).resize(function(){
		if($(window).width() > 767) {
			if($('.customerShippingInfoBox').height() > $('.customerBillingInfoBox').height()) {
				$('.customerBillingInfoBox').height($('.customerShippingInfoBox').height());
			}
			else if($('.customerShippingInfoBox').height() < $('.customerBillingInfoBox').height()) {
				$('.customerShippingInfoBox').height($('.customerBillingInfoBox').height());
			}

			if($('.shippingBox').height() > $('.paymentBox').height()) {
				$('.paymentBox').height($('.shippingBox').height());
			}
			else if($('.shippingBox').height() < $('.paymentBox').height()) {
				$('.shippingBox').height($('.paymentBox').height());
			}
			if($('.rushServiceBox') != null && $('.baggingServiceBox') != null) {
			 	if($('.rushServiceBox').height() > $('.baggingServiceBox').height()) {
	                $('.baggingServiceBox').height($('.rushServiceBox').height());
	            }
	            else if($('.rushServiceBox').height() < $('.baggingServiceBox').height()) {
	                $('.rushServiceBox').height($('.baggingServiceBox').height());
	            }
			}
		}
		else {
			$('.customerShippingInfoBox').css('height','auto');
			$('.customerBillingInfoBox').css('height','auto');
			$('.shippingBox').css('height','auto');
			$('.paymentBox').css('height','auto');
			if($('.rushServiceBox') != null && $('.baggingServiceBox') != null) {
            	$('.rushServiceBox').css('height','auto');
            	$('.baggingServiceBox').css('height','auto');
			}
		}
	}).trigger('resize');
	
	<c:if test="${gSiteConfig['gWILDMAN_GROUP']}">
		$( "#ajax_customer_account_number" ).autocomplete({
			 source: "${_contextpath}/json/json2.jhtm?action=userLocationMap",
		     minLength: 1,
		     select: function(event, ui) {
		         document.getElementById("accountNumberSpanId").innerHTML = ui.item.id;
		     },
		 });
	</c:if>
});
function toggleShipping(price) {
	$("#orderFormDetail").css('background:url(/assets/Image/Layout/spinner.gif) no-repeat center 50%; background-color : #EEEEEE;');
	$("#orderFormDetail").fadeTo( "slow" , 0.5, function() {});
	if(document.getElementById('newShippingCost') != null) {
		document.getElementById('newShippingCost').value = price;
	}
	$("#orderFormDetail").submit();
}
function toggleSelection() {
	document.getElementById('orderFormDetail').style['background'] = 'url(/assets/Image/Layout/spinner.gif) no-repeat center 50%'; 
	document.getElementById('orderFormDetail').style['background-color'] = '#EEEEEE'; 
	document.getElementById('orderFormDetail').submit(); 
}
function toggleCustomerCredit(credit) {
	$("#orderFormDetail").submit();
}
function loadPromoCode( aform ) {
	
	if ( document.getElementById('promoCodeInput').value == '') {
		alert("Promo code is empty!");
		return false;
	}
}
<c:if test="${gSiteConfig['gGIFTCARD']}">
function checkCode(){
  if ( document.getElementById('giftCardCodeInput').value == "" ) {
  	alert("<fmt:message key='giftcard.empty' />");
  	return false;
  }
}
</c:if>
//-->
</script>

</head>
<body>
	<!-- ========== PAGE WRAPPER ========== -->
	<div id="pageWrapper">

		<!-- ========== PAGE BODY ========== -->
		<div id="pageBodyWrapper">
			<div id="pageBody" class="container">
				<div class="row row-offcanvas row-offcanvas-left">
					<!-- ========== CONTENT ========== -->
					
					<div id="contentWrapper">
						<div id="content">
							<div class="col-sm-12">
								<div class="shoppingCartWizard clearfix">
									<div class="wizard-bar-wrapper">
										<ul class="wizard-bar">
											<li class="wizard-step active">Cart</li>
											<li class="wizard-step active">Shipping</li>
											<li class="wizard-step">Checkout</li>
										</ul>
									</div>
								</div>

								<div class="reviewOrderWrapper">
									<script type="text/javascript">
										$(window).load(function() {
											$(window).resize(function(){
												var w=window, d=document, e=d.documentElement, g=d.getElementsByTagName('body')[0];
												var screen_width = w.innerWidth||e.clientWidth||g.clientWidth;
												var screen_height = w.innerHeight||e.clientHeight||g.clientHeight;

												if(screen_width > 767) {
													if(screen_width > 991) {
														var boxes = $('.shippingInfoBox, .billingInfoBox, .shippingMethodBox, .paymentMethodBox');
														boxes.css('height','auto');
														var boxes_maxHeight = Math.max.apply(null, boxes.map(function(){
															return $(this).height();
														}).get());
														boxes.height(boxes_maxHeight);
													}
													else {
														var boxes_1 = $('.shippingInfoBox, .billingInfoBox');
														var boxes_2 = $('.shippingMethodBox, .paymentMethodBox');
														boxes_1.css('height','auto');
														boxes_2.css('height','auto');
														var boxes_1_maxHeight = Math.max.apply(null, boxes_1.map(function(){
															return $(this).height();
														}).get());
														boxes_1.height(boxes_1_maxHeight);
														var boxes_2_maxHeight = Math.max.apply(null, boxes_2.map(function(){
															return $(this).height();
														}).get());
														boxes_2.height(boxes_2_maxHeight);
													}
												}
												else {
													var boxes = $('.shippingInfoBox, .billingInfoBox, .shippingMethodBox, .paymentMethodBox');
													boxes.css('height','auto');
												}
											}).trigger('resize');
										});
									</script>
									
									
									
									
									<div class="infoBoxesWrapper">
										<div class="row">
										
											<form action="checkout1.jhtm" method="post">	
											<div class="col-sm-6 col-md-3">
													<div class="shippingInfoBox">
													<input type="hidden" name="newShippingAddress" value="true"> 
													<h3 class="shippingInfoTitle">Shipping Information</h3>
													<div class="shippingInfo">	
						    							<div><strong><c:out value="${orderForm.order.shipping.firstName}"/>&nbsp;<c:out value="${orderForm.order.shipping.lastName}"/></strong></div>
														<c:if test="${orderForm.order.shipping.company != ''}">
														  <div><c:out value="${orderForm.order.shipping.company}"/></div>
														</c:if>
														<div><c:out value="${orderForm.order.shipping.addr1}"/></div>
														<c:if test="${orderForm.order.shipping.addr2 != ''}">
														  <div><c:out value="${orderForm.order.shipping.addr2}"/></div>
														</c:if>
														<div><c:out value="${orderForm.order.shipping.city}"/>,&nbsp;<c:out value="${orderForm.order.shipping.stateProvince}"/>&nbsp;<c:out value="${orderForm.order.shipping.zip}"/></div>
														
														<c:if test="${billing != 'true' and order.purchaseOrder != '' and order.purchaseOrder != null and fn:contains(siteConfig['SITE_URL'].value, 'respectofflorida')}">
														  <div><fmt:message key="poNumber" />:&nbsp;<c:out value="${order.purchaseOrder}"/></div>
														</c:if>
														
														<c:if test="${!gSiteConfig['gADI'] and !fn:contains(siteConfig['SITE_URL'].value, 'respectofflorida')}">
														  <div><c:out value="${countries[orderForm.order.shipping.country]}"/></div>
														</c:if>
														
														<c:if test="${billing != 'true'}">
														  <c:if test="${siteConfig['HIDE_INVOICE_ADDRESS_TYPE'].value != 'true'}">
														    <div><c:out value="${orderForm.order.shipping.resToString}"/></div>
														  </c:if>
														  <c:if test="${siteConfig['SHIPPING_LTL'].value == 'true'}">
														    <div><c:out value="${orderForm.order.shipping.liftgateToString}"/></div>
														  </c:if>
														</c:if>
														
														<c:if test="${siteConfig['HIDE_INVOICE_PHONE_INFO'].value != 'true'}">
														  <div class="phone">
														    <span>Phone:</span> <c:out value="${orderForm.order.shipping.phone}"/>
														  </div>
														</c:if>
													</div>
													<div class="btn_wrapper change_btn_wrapper">
														<button type="submit" name="_target0" class="btn btn-orange change_btn">Change</button>
													</div>
												</div>
											</div>
											</form>
											
											<form action="checkout1.jhtm" method="post">	
											<div class="col-sm-6 col-md-3">
												<div class="billingInfoBox">
												<input type="hidden" name="newBillingAddress" value="true">
													<h3 class="billingInfoTitle">Billing Information</h3>
													<div class="billingInfo">
						    							<div><strong><c:out value="${orderForm.order.billing.firstName}"/>&nbsp;<c:out value="${orderForm.order.billing.lastName}"/></strong></div>
														<c:if test="${orderForm.order.billing.company != ''}">
														  <div><c:out value="${orderForm.order.billing.company}"/></div>
														</c:if>
														<div><c:out value="${orderForm.order.billing.addr1}"/></div>
														<c:if test="${orderForm.order.billing.addr2 != ''}">
														  <div><c:out value="${orderForm.order.billing.addr2}"/></div>
														</c:if>
														<div><c:out value="${orderForm.order.billing.city}"/>,&nbsp;<c:out value="${orderForm.order.billing.stateProvince}"/>&nbsp;<c:out value="${orderForm.order.billing.zip}"/></div>
														
														<c:if test="${billing != 'true' and order.purchaseOrder != '' and order.purchaseOrder != null and fn:contains(siteConfig['SITE_URL'].value, 'respectofflorida')}">
														  <div><fmt:message key="poNumber" />:&nbsp;<c:out value="${order.purchaseOrder}"/></div>
														</c:if>
														
														<c:if test="${!gSiteConfig['gADI'] and !fn:contains(siteConfig['SITE_URL'].value, 'respectofflorida')}">
														  <div><c:out value="${countries[orderForm.order.billing.country]}"/></div>
														</c:if>
														
														<c:if test="${billing != 'true'}">
														  <c:if test="${siteConfig['HIDE_INVOICE_ADDRESS_TYPE'].value != 'true'}">
														    <div><c:out value="${orderForm.order.billing.resToString}"/></div>
														  </c:if>
														</c:if>
														
														<c:if test="${siteConfig['HIDE_INVOICE_PHONE_INFO'].value != 'true'}">
														  <div class="phone">
														    <span>Phone:</span> <c:out value="${orderForm.order.billing.phone}"/>
														  </div>
														</c:if>
													</div>
													<div class="btn_wrapper change_btn_wrapper">
														<button type="submit" name="_target1" class="btn btn-orange change_btn">Change</button>
													</div>
												</div>
											</div>
											</form>
											
											<form action="checkout1.jhtm" method="post" name="orderFormDetail" id="orderFormDetail"> 
											
											<div class="col-sm-6 col-md-3">
												<div class="shippingMethodBox">
													<h3 class="shippingMethodTitle">Shipping Method</h3>
													<div class="shippingMethod">
														
														
														<c:if test="${model.finish && (orderForm.order.shippingMethod==null || orderForm.order.shippingMethod=='')}">
															<span style="color:red"><c:out value="Please choose a shipping method."/></span>
														</c:if>
														<c:if test="${orderForm.ratesList != null}">
														<c:forEach items="${orderForm.ratesList}" var="shippingrate" varStatus="loopStatus">
														<div class="shipping_method_options_wrapper">
														<div class="radio shipping_method_option">
														<input type="radio" name="shippingRateIndex" id="shippingRateIndex" onclick="toggleShipping(${shippingrate.price})" value="${loopStatus.index}"
														<c:if test="${(status.value == loopStatus.index) || (shippingrate.title == 'Free') || (orderForm.order.shippingMethod == shippingrate.title)}">checked</c:if>
														>
															<span class="carrier">${shippingrate.title}</span>
															<c:if test="${shippingrate.price != null}">
																<span class="shipping_rate"><fmt:message key="${siteConfig['CURRENCY'].value}"/><fmt:formatNumber value="${shippingrate.price}" pattern="#,##0.00"/></span>
															</c:if>
														</div>
														</div>				
														</c:forEach>						
														<div class="shipping_method_note">
															<strong>Note:</strong>
															<c:if test="${model.note != null && model.note!='null'}">
																${model.note}
															</c:if>
														</div>				
														</c:if>
														
														
													</div>
												</div>
											</div>							
											
	
											
											<div class="col-sm-6 col-md-3">
												<div class="paymentMethodBox">
													<h3 class="paymentMethodTitle">Payment Method</h3>
													<div class="paymentMethod">
														<div class="payment_method_options_wrapper">
															<c:if test="${model.finish && (orderForm.order.paymentMethod==null || orderForm.order.paymentMethod=='')}">
																<span style="color:red"><c:out value="Please choose a payment method."/></span>
															</c:if>
															<c:forEach items="${model.customPayments}" var="paymentMethod" varStatus="loopStatus">
																<c:if test="${paymentMethod.title != null && paymentMethod.title != '' && paymentMethod.enabled  && !paymentMethod.internal}">
																<div class="radio payment_method_option">
																	<label>
																		<input type="radio" name="order.paymentMethod" onclick="submit()" value="${paymentMethod.title}" id="order.paymentMethod${loopStatus.index}" 
																		<c:if test="${(status.value == loopStatus.index) || (orderForm.order.paymentMethod == paymentMethod.title)}">
																			checked
																		</c:if>>
																		${paymentMethod.title}
																		
																		
																	</label>
																</div>
																</c:if>											
															</c:forEach>
														</div>
													</div>
												</div>
											</div>
											
											
										</div>
									</div>

									<div class="clearfix">
										<div class="reviewOrderTitle">Review Order</div>
									</div>

									<div class="reviewOrder_table">
										<div class="reviewOrderHdr hidden-xs hidden-sm">
											<div class="row">
												<div class="col-sm-12 col-md-1">
													<div class="cartImageHeader">Image</div>
												</div>
												<div class="col-sm-12 col-md-3">
													<div class="cartNameHeader">Name</div>
												</div>
												<div class="col-sm-12 col-md-1">
													<div class="cartPackingHeader">Packing</div>
												</div>
												<div class="col-sm-12 col-md-1">
													<div class="cartWeightHeader">Weight</div>
												</div>
												<div class="col-sm-12 col-md-1">
													<div class="cartQtyHeader">Quantity</div>
												</div>
												<div class="col-sm-12 col-md-1">
													<div class="cartUnitsHeader"># Units</div>
												</div>
												<div class="col-sm-12 col-md-2">
													<div class="cartPriceHeader">Price/Lot</div>
												</div>
												<div class="col-sm-12 col-md-2">
													<div class="cartSubtotalHeader">Subtotal</div>
												</div>
											</div>
										</div>

										<div class="reviewOrderMobileHdr visible-xs visible-sm">
											<div class="row">
												<div class="col-sm-12">
													Cart Items
												</div>
											</div>
										</div>

										<div class="reviewOrderDetails">
											
											<c:forEach var="lineItem" items="${orderForm.order.lineItems}" varStatus="lineItemStatus">
											
												<div 
												<c:choose>
													<c:when test="${lineItemStatus.index % 2 == 0}">
														class="odd_row clearfix"
													</c:when>
													<c:otherwise>
														class="even_row clearfix"
													</c:otherwise>
												</c:choose>
												>
										
												<div class="col-sm-12 col-md-1">
													<div class="cartImageHeader visible-xs visible-sm">Image</div>
													<div class="cartImageWrapper">
														<a href="#"><img class="cartImage img-responsive" src="<c:if test="${!lineItem.product.thumbnail.absolute}">${_contextpath}/assets/Image/Product/thumb/</c:if>${lineItem.product.thumbnail.imageUrl}" border="0" alt="${lineItem.product.alt}"/></a>
													</div>
													<div class="clearfix"></div>
												</div>
												<div class="col-sm-12 col-md-3">
													<div class="cartNameHeader visible-xs visible-sm">Name</div>
													<div class="cartName">
														<div class="cartItemNameSkuWrapper">
															<a href="#" class="cart_item_name">${lineItem.product.name}</a>
															<br>
															<a href="#" class="cart_item_sku">
																<span class="sku_label">SKU:</span>
																${lineItem.product.sku}
															</a>
														</div>
													</div>
													<c:if test="${lineItem.includeRetailDisplay}">
														Include Retail Display
													</c:if>
													<div class="clearfix"></div>
												</div>
												<div class="col-sm-12 col-md-1">
													<div class="cartPackingHeader visible-xs visible-sm">Packing</div>
													<div class="cartPacking">${lineItem.product.packing}</div>
													<div class="clearfix"></div>
												</div>
												<div class="col-sm-12 col-md-1">
													<div class="cartWeightHeader visible-xs visible-sm">Weight</div>
													<div class="cartWeight">${lineItem.product.weight} lbs</div>
													<div class="clearfix"></div>
												</div>
												<div class="col-sm-12 col-md-1">
													<div class="cartQtyHeader visible-xs visible-sm">Quantity</div>
													<div class="cartQty">${lineItem.quantity}</div>
													<div class="clearfix"></div>
												</div>
												<div class="col-sm-12 col-md-1">
													<div class="cartUnitsHeader visible-xs visible-sm"># Units</div>
													<div class="cartUnits">${lineItem.product.field7}</div>
													<div class="clearfix"></div>
												</div>
												<div class="col-sm-12 col-md-2">
													<div class="cartPriceHeader visible-xs visible-sm">Price/Lot</div>
													<div class="cartPrice"><fmt:message key="${siteConfig['CURRENCY'].value}" /><fmt:formatNumber value="${lineItem.unitPrice}" pattern="###,##0.00" /></div>
													<div class="clearfix"></div>
												</div>
												<div class="col-sm-12 col-md-2">
													<div class="cartSubtotalHeader visible-xs visible-sm">Subtotal</div>
													<div class="cartSubtotal"><fmt:message key="${siteConfig['CURRENCY'].value}" /><fmt:formatNumber value="${lineItem.totalPrice}" pattern="###,##0.00" /></div>
													<div class="clearfix"></div>
												</div>
											</div>
											
										  </c:forEach>	
										</div>
										<div class="reviewOrderFtr">
											<div class="row">
												<div class="col-xs-4 col-sm-4 col-md-offset-7 col-md-3 right_bordered">
													<div class="subtotal_label">Subtotal</div>
												</div>
												<div class="col-xs-8 col-sm-8 col-md-2">
													<div class="subtotal_value">
													<fmt:message key="${siteConfig['CURRENCY'].value}" /><fmt:formatNumber value="${orderForm.order.subTotal}" pattern="###,##0.00" />
													</div>
												</div>
											</div>
											<c:if test="${orderForm.order.promo != null and (orderForm.order.promo.discountType eq 'order')}">
											<div class="row">
												<div class="col-xs-4 col-sm-4 col-md-offset-7 col-md-3 right_bordered">
													<div class="shippingAndHandling_label">
													
													<c:choose>
        											<c:when test="${orderForm.order.promoAmount == 0}"><fmt:message key="promoCode" /></c:when>
        											<c:otherwise><fmt:message key="discountForPromoCode" /></c:otherwise>
      												</c:choose>
      												${orderForm.order.promo.title}
      												<c:choose>
          											<c:when test="${orderForm.order.promoAmount == 0}"></c:when>
          											<c:when test="${orderForm.order.promo.percent}">
            											(<fmt:formatNumber value="${orderForm.order.promo.discount}" pattern="###,##0.00"/>%)
          											</c:when>
          											<c:otherwise>
            											(<fmt:message key="${siteConfig['CURRENCY'].value}" /><fmt:formatNumber value="${orderForm.order.promo.discount}" pattern="###,##0.00"/>)
          											</c:otherwise>
      												</c:choose>
													
													</div>
												</div>
												<div class="col-xs-8 col-sm-8 col-md-2">
													<div class="shippingAndHandling_value">
														<c:choose>
        													<c:when test="${orderForm.order.promoAmount == 0}">&nbsp;</c:when>
        													<c:otherwise>(<fmt:message key="${siteConfig['CURRENCY'].value}" /><fmt:formatNumber value="${orderForm.order.promoAmount}" pattern="#,##0.00"/>)</c:otherwise>
      													</c:choose>
													</div>
												</div>
											</div>
											</c:if>
											
											<div class="row">
												<div class="col-xs-4 col-sm-4 col-md-offset-7 col-md-3 right_bordered">
													<div class="tax_label">Tax</div>
												</div>
												<div class="col-xs-8 col-sm-8 col-md-2">
													<div class="tax_value">
													<fmt:message key="${siteConfig['CURRENCY'].value}" /><fmt:formatNumber value="${orderForm.order.tax}" pattern="###,##0.00" />
													</div>
												</div>
											</div>
											
											<div class="row">
												<div class="col-xs-4 col-sm-4 col-md-offset-7 col-md-3 right_bordered">
													<div class="shippingAndHandling_label"><fmt:message key="shippingHandling" /> (<c:out value="${orderForm.order.shippingMethod}" escapeXml="false"/>):</div>
												</div>
												<div class="col-xs-8 col-sm-8 col-md-2">
													<div class="shippingAndHandling_value">
													<c:if test="${orderForm.order.shippingCost!=null}">
														<fmt:message key="${siteConfig['CURRENCY'].value}" /><fmt:formatNumber value="${orderForm.order.shippingCost}" pattern="###,##0.00" />
													</c:if>
													</div>
												</div>
											</div>
											
											<c:if test="${orderForm.order.ccFee != null and orderForm.order.ccFee > 0}">
											<div class="row">
												<div class="col-xs-4 col-sm-4 col-md-offset-7 col-md-3 right_bordered">
													<div class="shippingAndHandling_label">Credit Card/Paypal Fee:</div>
												</div>
												<div class="col-xs-8 col-sm-8 col-md-2">
													<div class="shippingAndHandling_value">
													<fmt:message key="${siteConfig['CURRENCY'].value}" /><fmt:formatNumber value="${orderForm.order.ccFee}" pattern="###,##0.00" />
													</div>
												</div>
											</div>
											</c:if>
  
											<div class="row">
												<div class="col-xs-4 col-sm-4 col-md-offset-7 col-md-3 right_bordered">
													<div class="grandTotal_label">GRAND TOTAL</div>
												</div>
												<div class="col-xs-8 col-sm-8 col-md-2">
													<div class="grandTotal_value">
													<fmt:message key="${siteConfig['CURRENCY'].value}" /><fmt:formatNumber value="${orderForm.order.grandTotal}" pattern="###,##0.00" />
													</div>
												</div>
											</div>
										</div>
									</div>

									<div class="row">
										<div class="col-sm-6">
											<p>Not in the USA? Use our <a href="#">currency converter</a> to see the cost of your order in your local currency.</p>
											<p>We are now offering <strong>online shipping quotes</strong> for many orders. Please note we are currently unable to quote the following online:</p>
											<ul>
												<li>International pallet orders</li>
												<li>Ocean containers</li>
												<li>Full Truckloads</li>
												<li>More than 6-7 Pallets within the USA</li>
											</ul>
											<p>To obtain a <strong>shipping quote for one of the scenarios above</strong>, please fill out our shipping quote request form and we will get back to you shortly with a quote. Thank you!</p>
										</div>
										<div class="col-sm-6">
											<div id="promoCodeWrapper">
												<div class="row">
													<div class="col-sm-12">
														<label for="promoCodeInput" class="promoCodeLabel">Enter Promo Code:</label>
													</div>
													<div class="col-sm-12">
														<div class="input-group">
															<input type="text" id="order.promoCode" class="form-control" name="order.promoCode" value="${promoCode}">
															<span class="input-group-btn">
																<button class="btn btn-orange applyPromoCode_btn" onclick="return loadPromoCode(this.form)" name="_target2">Apply Promo Code</button>
															</span>
														</div>
													</div>
												</div>
											</div>
											
											<div id="specialInstructionsWrapper">
												<div class="row">
													<div class="col-sm-12">
														<label for="specialInstructionsText" class="specialInstructionsLabel">Special Instructions:</label>
													</div>
													<div class="col-sm-12">
														<textarea id="order.invoiceNote" name="order.invoiceNote" style="height:135px" rows="4" cols="65" htmlEscape="true"></textarea>
													</div>
												</div>
											</div>
											
											<div class="buttons_wrapper clearfix">		
												<button class="btn btn-lg modifyCart_btn" type="submit" name="_modify"><span class="modifyCart_icon"></span>&nbsp; Modify Cart</button>					
												<button class="btn btn-lg placeOrder_btn" type="submit" name="_finish">Place Order &nbsp;<span class="placeOrder_icon"></span></button>			
											</div>
											
											
										</div>
									</div>
									</form>
									
									
									
								</div>
							</div>
						</div>
					</div>
					<!-- ========== END CONTENT ========== -->
				</div>
			</div>
		</div>
		
		<!-- ========== END PAGE BODY ========== -->
		
		

	</div>
	<!-- ========== END PAGE WRAPPER ========== -->
	
	
	
	
	
	
	</tiles:putAttribute>
</tiles:insertDefinition>