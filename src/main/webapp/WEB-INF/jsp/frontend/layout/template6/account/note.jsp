<%@ page session="false" %>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ taglib uri="/WEB-INF/tlds/custom.tld" prefix="wj" %>

<tiles:insertDefinition name="${_template}" flush="true">
	<c:if test="${_leftBar != '1' and _leftBar != '4'}">
		<tiles:putAttribute name="leftbar" value="/WEB-INF/jsp/frontend/layout/template6/leftbar.jsp" />
	</c:if>
	<tiles:putAttribute name="content" type="string">

		<div class="col-breadcrumb">
			<ul class="breadcrumb">
				<li><a href="${_contextpath}/account.jhtm">My Account</a></li>
				
                <c:choose>
                  <c:when test="${param.note == 1 }">
                    <li class="active"><c:out value="${siteConfig['CUSTOMER_CUSTOM_NOTE_1'].value}"/></li>
                  </c:when>
                  <c:when test="${param.note == 2 }">
                    <li class="active"><c:out value="${siteConfig['CUSTOMER_CUSTOM_NOTE_2'].value}"/></li>
                  </c:when>
                  <c:when test="${param.note == 3 }">
                    <li class="active"><c:out value="${siteConfig['CUSTOMER_CUSTOM_NOTE_3'].value}"/></li>
                  </c:when>
                </c:choose>
                
			</ul>
		</div>


<div class="col-sm-12">
     <div class="customerCustomNote">
		<c:choose>
			<c:when test="${param.note == 1}">
				<div class="noteHeader"><c:out value="${siteConfig['CUSTOMER_CUSTOM_NOTE_1'].value}"/></div>
				<div class="note"><c:out value="${model.customer['customNote1']}" escapeXml="false" /></div>
			</c:when>
		    <c:when test="${param.note == 2}">
				<div class="noteHeader"><c:out value="${siteConfig['CUSTOMER_CUSTOM_NOTE_2'].value}"/></div>
				<div class="note"><c:out value="${model.customer['customNote2']}" escapeXml="false" /></div>
			</c:when>
			<c:when test="${param.note == 3}">
				<div class="noteHeader"><c:out value="${siteConfig['CUSTOMER_CUSTOM_NOTE_3'].value}"/></div>
				<div class="note"><c:out value="${model.customer['customNote3']}" escapeXml="false" /></div>
			</c:when>
		</c:choose>
	</div>
</div>

  </tiles:putAttribute>
</tiles:insertDefinition>


