<%@ page session="false" %>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page session="false" %>
<tiles:insertDefinition name="${_template}" flush="true">
  <c:if test="${_leftBar != '1' and _leftBar != '4'}">
    <tiles:putAttribute name="leftbar" value="/WEB-INF/jsp/frontend/layout/template6/leftbar.jsp" />
  </c:if>
  <tiles:putAttribute name="content" type="string">
  
  
<script type="text/javascript">

$.get('/dv/liveapps/checkcustomer.php',{cid:"${model.id}"});

</script>
  
<script type="text/javascript">

       $(window).load(function() {

              $(window).resize(function(){

                     var w=window, d=document, e=d.documentElement, g=d.getElementsByTagName('body')[0];

                     var screen_width = w.innerWidth||e.clientWidth||g.clientWidth;

                     var screen_height = w.innerHeight||e.clientHeight||g.clientHeight;

 

                     if(screen_width > 767) {

                           $(".accountSettingsSection").css('height','auto');

                           var boxes = [], left_boxes = [], right_boxes = [];

                           $(".col-sm-6 .accountSettingsSection").each(function(){

                                  boxes.push("#"+$(this).attr("id"));

                           });

                           for(var i=0; i < boxes.length; i++) {

                                  (i % 2 == 0 ? left_boxes : right_boxes).push(boxes[i]);

                           }

                           for(var i=0; i < Math.min(left_boxes.length, right_boxes.length); i++) {

                                  var adjacentBoxes = $(""+left_boxes[i]+","+right_boxes[i]+"");

                                  var adjacentBoxes_maxHeight = Math.max.apply(null, adjacentBoxes.map(function(){

                                         return $(this).height();

                                  }).get());

                                  adjacentBoxes.height(adjacentBoxes_maxHeight);

                           }

                     }

                     else {

                           $(".accountSettingsSection").css('height','auto');

                     }

              }).trigger('resize');

       });

</script>
		<div id="accountSettingsWrapper">
		<div class="row">
			
			
			<div class="col-sm-12">
				<h2></h2>
			</div>
			
			
			<c:if test="${gSiteConfig['gCUSTOMER_SUPPLIER'] and model.customer.supplierId != null}">
			<div class="col-sm-6">
				<div id="account_Manager" class="accountSettingsSection">
					<h3><i class="fa fa-user-secret"></i> <fmt:message key="f_accountManager" /></h3>
					<div><b><fmt:message key="f_accountManagerName" />:</b> <c:out value="${model.salesRep.name}" /></div>
					<div><b><fmt:message key="f_accountManagerEmail" />:</b> <c:out value="${model.salesRep.email}" /></div>
					<div><b><fmt:message key="f_accountManagerPhone" />:</b> <c:out value="${model.salesRep.phone}" /></div>
				</div>
			</div>
			
			<div class="col-sm-6">
				<div id="liquidatenow_settings" class="accountSettingsSection" 
				<c:if test="${gSiteConfig['gCUSTOMER_SUPPLIER'] and model.customer.supplierId != null}">
				style="visibility: visible;"
				</c:if>
				<c:if test="${!gSiteConfig['gCUSTOMER_SUPPLIER'] or model.customer.supplierId == null}">
				style="visibility:hidden;"
				</c:if>
				>
				<div class="row">
					<div class="col-sm-12 col-md-6">
						<div class="accountSettingsLeft">
							<img src="assets/responsive/img/liquidatenow_logo.png" class="img-responsive liquidatenow_logo">
						</div>
					</div>
					<div class="col-sm-12 col-md-6">
						<div class="accountSettingsRight">
							<div><a href="${_contextpath}/account_products.jhtm"></a>&nbsp;&nbsp;&nbsp;</div>
							<div><a href="${_contextpath}/consignment_sales.jhtm"></a>&nbsp;&nbsp;&nbsp;</div>
							<div><a href="${_contextpath}/account_dashboard.jhtm?id=${model.id}"><fmt:message key="f_viewMyDashboard"/></a></div>
							<div><a href="${_contextpath}/credit_history.jhtm"></a>&nbsp;&nbsp;&nbsp;</div>
							<div><a href="${_contextpath}/account_products.jhtm"></a>&nbsp;&nbsp;&nbsp;</div>
						</div>
					</div>
				</div>
				</div>
			</div>
			</c:if>
			
			<c:if test="${!gSiteConfig['gCUSTOMER_SUPPLIER'] or model.customer.supplierId == null}">
			<div class="col-sm-12">
				<div id="account_Manager" class="accountSettingsSection">
					<h3><i class="fa fa-user-secret"></i> <fmt:message key="f_accountManager" /></h3>
					<div><b><fmt:message key="f_accountManagerName" />:</b> <c:out value="${model.salesRep.name}" /></div>
					<div><b><fmt:message key="f_accountManagerEmail" />:</b> <c:out value="${model.salesRep.email}" /></div>
					<div><b><fmt:message key="f_accountManagerPhone" />:</b> <c:out value="${model.salesRep.phone}" /></div>
				</div>
			</div>
			</c:if>
			
			
			<div class="col-sm-6">
				<div id="account_MyAccount" class="accountSettingsSection">
					<h3><i class="fa fa-user"></i> <fmt:message key="myAccount" /></h3>
					<div class="row">
						<div class="col-sm-12 col-md-6">
							<div class="accountSettingsLeft">
								<div><b><fmt:message key="name"/>:</b> <c:out value="${model.customer.address.firstName}" />&nbsp;<c:out value="${model.customer.address.lastName}" /></div>
								<div><b><fmt:message key="email"/>:</b> <c:out value="${model.customer.address.email}" /></div>
								<c:if test="${model.customer.address.phone != null}">
								<div><b><fmt:message key="phone" />:</b> <c:out value="${model.customer.address.phone}" /></div>
								</c:if>
							</div>
						</div>
						<div class="col-sm-12 col-md-6">
							<div class="accountSettingsRight">
								<div>
								<a href="account_edit.jhtm">
								<fmt:message key="shoppingcart.changeYourEmailOrPassword" />
								</a>
								</div>
								<div><a href="${_contextpath}/account_addresses.jhtm"><fmt:message key="shoppingcart.manageStoredAddress" /></a></div>
								<div><a href="${_contextpath}/account_information_edit.jhtm"><fmt:message key="shoppingcart.editMyInformation"/></a></div>
								<div><a href="https://www.viatrading.com/notification-center/">My Notification Settings</a></div>
								
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="col-sm-6">
				<div id="account_MyList" class="accountSettingsSection">
					<h3><i class="fa fa-list-alt"></i> 
					
					<c:choose>
							<c:when test="${siteConfig['MY_LIST_TITLE'].value != ''}"><c:out value="${siteConfig['MY_LIST_TITLE'].value}" /></c:when>
							<c:otherwise><fmt:message key="myList" /></c:otherwise>
					</c:choose>				
					
					</h3>
					<div class="row">
						<div class="col-sm-12 col-md-6">
							<div class="accountSettingsLeft">
								<div><c:out value="${fn:length(model.myList)}" />  Item<c:if test="${fn:length(model.myList) > 1 or fn:length(model.myList) == 0}">s</c:if> in List</div>
							</div>
						</div>
						<div class="col-sm-12 col-md-6">
							<div class="accountSettingsRight">
							
							
							<div>
							<a href="${_contextpath}/viewList.jhtm"><fmt:message key="view" />&nbsp;
							<c:choose>
							<c:when test="${siteConfig['MY_LIST_TITLE'].value != ''}">
							<c:out value="${siteConfig['MY_LIST_TITLE'].value}" />
							</c:when>
							<c:otherwise><fmt:message key="myList" />
							</c:otherwise>
							</c:choose>
							</a>
							</div>
							</div>
							
							
						</div>
					</div>
				</div>
			</div>
			<div class="col-sm-6">
				<div id="account_MyOrderList" class="accountSettingsSection">
					<h3><i class="fa fa-history"></i><fmt:message key="f_viewMyOrderList" /></h3>
					<div class="row">
						<div class="col-sm-12 col-md-6">
							<div class="accountSettingsLeft">
								<div>0 items in Order List</div>
							</div>
						</div>
						<div class="col-sm-12 col-md-6">
							<div class="accountSettingsRight">
								<div><a href="${_contextpath}/viewMyOrderList.jhtm"><fmt:message key="f_viewMyPurchasedProductList"/></a></div>
								<div><a href="${_contextpath}/account_orders.jhtm"><fmt:message key="f_viewMyOrderHistoryStatus" /></a></div>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="col-sm-6">
				<div id="account_Rma" class="accountSettingsSection">
					<h3><i class="fa fa-calendar"></i> <fmt:message key="shoppingcart.registerforevents" /></h3>
					<div class="row">
						<div class="col-sm-12 col-md-6">
							<div class="accountSettingsLeft">
								<div>${model.eventCount} Registered Events</div>
							</div>
						</div>
						<div class="col-sm-12 col-md-6">
							<div class="accountSettingsRight">
								<div><a href="${_contextpath}/loginEventRegister.jhtm"><fmt:message key="shoppingcart.viewEventsandRegisters" /></a></div>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="col-sm-6">
				<div id="account_Support" class="accountSettingsSection">
					<h3><i class="fa fa-envelope"></i> <fmt:message key="f_support" /></h3>
					<div class="row">
						<div class="col-sm-12 col-md-6">
							<div class="accountSettingsLeft">
								<div>${model.ticketCount} Tickets</div>
							</div>
						</div>
						<div class="col-sm-12 col-md-6">
							<div class="accountSettingsRight">
								<div><a href="${_contextpath}/account_ticket.jhtm"><fmt:message key="newTicket" /></a></div>
								<div><a href="${_contextpath}/account_ticketList.jhtm"><fmt:message key="tickets" /></a></div>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="col-sm-6">
				<div id="account_TaxInfo" class="accountSettingsSection">
					<h3><i class="fa fa-usd"></i> <fmt:message key="f_taxInfo" /></h3>
					<div><c:out value="${model.customer.taxId}" /></div>
					
					<c:if test="${model.customer.taxId == null or empty model.customer.taxId}" >
						<div style="color:#ff0000"><fmt:message key="f_taxNote" /></div>
					</c:if>											
				
				</div>
			</div>
					
										
										
			<c:if test="${siteConfig['PRODUCT_QUOTE'].value == 'true'}">
			<div class="col-sm-6">
				<div id="account_MyQuoteList" class="accountSettingsSection">
					<h3>
						<i class="fa fa-file"></i> <fmt:message key="myQuoteHistory" />
					</h3>
					<div class="row">
						<div class="col-sm-12 col-md-6">
							<div class="accountSettingsLeft">
								<div><c:out value="${fn:length(model.quoteList)}" /> Quote<c:if test="${fn:length(model.quoteList) > 1 or fn:length(model.quoteList) == 0}">s</c:if></div>
							</div>
						</div>
	
						<div class="col-sm-12 col-md-6">
							<div class="accountSettingsRight">
								<div>
									<a href="${_contextpath}/account_quotes.jhtm"><fmt:message key="view" />&nbsp;<fmt:message key="myQuoteHistory" /></a>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			</c:if>
			
	
			
			
						
			<c:if test="${model.hasCustomNote1 or model.hasCustomNote2 or model.hasCustomNote3}">
			<div class="col-sm-6">
				<div id="account_Miscellaneous" class="accountSettingsSection">
					<h3><i class="fa fa-link"></i> <fmt:message key="misc" /></h3>
					<div class="row">
					     <div class="col-sm-12 col-md-6">
							<div class="accountSettingsLeft">
								<div>&nbsp;</div>
							</div>
						</div>
						<div class="col-sm-12 col-md-6">
							<div class="accountSettingsRight">
								<c:if test="${model.hasCustomNote1}">
			                       <div><a href="account.jhtm?note=1"><c:out value="${siteConfig['CUSTOMER_CUSTOM_NOTE_1'].value}"/></a></div>
			                    </c:if>
			                    <c:if test="${model.hasCustomNote2}">
			                       <div><a href="account.jhtm?note=2"><c:out value="${siteConfig['CUSTOMER_CUSTOM_NOTE_2'].value}"/></a></div>
			                    </c:if>
			                    <c:if test="${model.hasCustomNote3}">
			                       <div><a href="account.jhtm?note=3"><c:out value="${siteConfig['CUSTOMER_CUSTOM_NOTE_3'].value}"/></a></div>
			                    </c:if>								
							</div>
						</div>
					</div>
				</div>
			</div>
			</c:if>
		
			
			<c:if test="${gSiteConfig['gRMA'] and model.customer.supplierId == null}">
			<div class="col-sm-6">
				<div id="account_Rma" class="accountSettingsSection">
					<h3>
						<i class="fa fa-envelope-o"></i> <fmt:message key="f_rma" />
					</h3>
					<div class="row">
						<div class="col-sm-12 col-md-6">
							<div class="accountSettingsLeft">
								<div>0 Requests</div>
							</div>
						</div>
						<div class="col-sm-12 col-md-6">
							<div class="accountSettingsRight">
								<div>
									<a href="account_rmas.jhtm"><fmt:message key="f_rmaRequest" /></a><br/>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			</c:if>

			

			
			<c:if test="${gSiteConfig['gLOYALTY'] and siteConfig['LOYALTY_SHOW_ON_MYACCOUNT'].value == 'true' and model.customer.supplierId == null}">
			<div class="col-sm-6">
				<div id="account_Loyalty" class="accountSettingsSection">
					<h3>
						<i class="fa fa-heart"></i> <fmt:message key="f_loyalty" />
					</h3>
					<div class="row">
						<div class="col-sm-12 col-md-6">
							<div class="accountSettingsLeft">
								<div>
								   <fmt:message key="totalPoint(s)" />:&nbsp;<fmt:formatNumber value="${model.customer.loyaltyPoint}" pattern="#,##0.00" />
								</div>
							</div>
						</div>
						<div class="col-sm-12 col-md-6">
							<div class="accountSettingsRight"></div>
						</div>
					</div>
				</div>
			</div>
			</c:if>			
		
			<c:if test="${gSiteConfig['gSUB_ACCOUNTS'] and model.hasSubAccounts}">
			<div class="col-sm-6">
				<div id="account_MyList" class="accountSettingsSection">
					<h3>
						<i class="fa fa-user"></i> <fmt:message key="f_subAccounts" />
					</h3>
					<div class="row">
						<div class="col-sm-12 col-md-6">
							<div class="accountSettingsRight">
								<div>&nbsp;</div>
							</div>
						</div>
						<div class="col-sm-12 col-md-6">
							<div class="accountSettingsRight">
								<div><a href="account_subs.jhtm" class="myaccount_link"><fmt:message key="view" />&nbsp;<fmt:message key="f_subAccounts" /></a></div>
							</div>
						</div>
					</div>
				</div>
			</div>
			</c:if>
			
			<div class="col-sm-12">
				<div id="button_wrapper">
					<a class="btn btn-default" href="${_contextpath}/logout.jhtm"><fmt:message key="logout"/></a>
				</div>
			</div>
										
					
		</div>
		</div>
  			

<c:if test="${fn:trim(model.myaccountLayout.footerHtml) != ''}">
  <c:set value="${model.myaccountLayout.footerHtml}" var="footerHtml"/>
  <c:set value="${fn:replace(footerHtml, '#firstname#', userSession.firstName)}" var="footerHtml"/>
  <c:set value="${fn:replace(footerHtml, '#lastname#', userSession.lastName)}" var="footerHtml"/>
  <c:set value="${fn:replace(footerHtml, '#email#', userSession.username)}" var="footerHtml"/>
  <c:set value="${fn:replace(footerHtml, '#accountnumber#', model.customer.accountNumber)}" var="footerHtml"/>
  <c:if test="${gSiteConfig['gSALES_REP']}">
    <c:set value="${fn:replace(footerHtml, '#salesRepName#', model.salesRep.name)}" var="footerHtml"/>
    <c:set value="${fn:replace(footerHtml, '#salesRepEmail#', model.salesRep.email)}" var="footerHtml"/>
    <c:set value="${fn:replace(footerHtml, '#salesRepPhone#', model.salesRep.phone)}" var="footerHtml"/>
    <c:set value="${fn:replace(footerHtml, '#salesRepCellPhone#', model.salesRep.cellPhone)}" var="footerHtml"/>
    <c:set value="${fn:replace(footerHtml, '#salesRepAddress1#', model.salesRep.address.addr1)}" var="footerHtml"/>
    <c:set value="${fn:replace(footerHtml, '#salesRepAddress2#', model.salesRep.address.addr2)}" var="footerHtml"/>
    <c:set value="${fn:replace(footerHtml, '#salesRepCity#', model.salesRep.address.city)}" var="footerHtml"/>
    <c:set value="${fn:replace(footerHtml, '#salesRepCountry#', model.salesRep.address.country)}" var="footerHtml"/>
    <c:set value="${fn:replace(footerHtml, '#salesRepStateProvince#', model.salesRep.address.stateProvince)}" var="footerHtml"/>
    <c:set value="${fn:replace(footerHtml, '#salesRepZip#', model.salesRep.address.zip)}" var="footerHtml"/>
    <c:set value="${fn:replace(footerHtml, '#salesRepFax#', model.salesRep.address.fax)}" var="footerHtml"/>
    <c:set value="${fn:replace(footerHtml, '#salesRepCompany#', model.salesRep.address.company)}" var="footerHtml"/>
  </c:if>
  <c:out value="${footerHtml}" escapeXml="false"/>
</c:if>

<script type="text/javascript">
$(document).ready(function() {
       $.get('https://www.viatrading.com/dv/liveapps/checkcustomer.php',{cid:${model.customer.id} ,random:Math.random()});
});
</script>

  </tiles:putAttribute>
</tiles:insertDefinition>
