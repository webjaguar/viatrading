<%@ page session="false" %>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ taglib uri="/WEB-INF/tlds/custom.tld" prefix="wj" %>

<tiles:insertDefinition name="${_template}" flush="true">
  <c:if test="${_leftBar != '1' and _leftBar != '4'}">
    <tiles:putAttribute name="leftbar" value="/WEB-INF/jsp/frontend/layout/template6/leftbar.jsp" />
  </c:if>
  <tiles:putAttribute name="content" type="string">
  
<div class="col-breadcrumb">
	<ul class="breadcrumb">
		<li><a href="${_contextpath}/account.jhtm">My Account</a></li>
		<li class="active">
			<c:choose>
				<c:when test="${siteConfig['VIEW_MY_ORDER_LIST_TITLE'].value != ''}">
					<c:out value="${siteConfig['VIEW_MY_ORDER_LIST_TITLE'].value}" />
				</c:when>
				<c:otherwise>
					<fmt:message key="view" />&nbsp;<fmt:message key="myOrderList" />
				</c:otherwise>
			</c:choose>
		</li>
	</ul>
</div>
<c:if test="${gSiteConfig['gMYORDER_LIST']}">

<c:if test="${fn:trim(model.myOrderListLayout.headerHtml) != ''}">
  <c:set value="${model.myOrderListLayout.headerHtml}" var="headerHtml"/>
  <c:set value="${fn:replace(headerHtml, '#firstname#', userSession.firstName)}" var="headerHtml"/>
  <c:set value="${fn:replace(headerHtml, '#lastname#', userSession.lastName)}" var="headerHtml"/>
  <c:set value="${fn:replace(headerHtml, '#email#', userSession.username)}" var="headerHtml"/>
  <c:out value="${headerHtml}" escapeXml="false"/>
</c:if>

<div class="col-sm-12">
	<div class="myOrderListWrapper">
		<div class="headerTitle myOrderList">
			<c:choose>
				<c:when test="${siteConfig['VIEW_MY_ORDER_LIST_TITLE'].value != ''}">
					<c:out value="${siteConfig['VIEW_MY_ORDER_LIST_TITLE'].value}" />
				</c:when>
				<c:otherwise>
					<fmt:message key="view" />&nbsp;<fmt:message key="myOrderList" />
				</c:otherwise>
			</c:choose>
		</div>

		<div class="clearfix"></div>

		<c:choose>
		  <c:when test="${model.myOrderProducts.pageCount == null}">
			<div class="message">Your List is empty.</div>
		  </c:when>
		  <c:otherwise>

			<div class="row">
				<div class="col-sm-12">
					<div class="pageNavigation">
							<form class="formPageNavigation clearfix" name="pagination">
							<div class="pageNavigationControl">
								<div class="page">
									<span>Page</span> 
									<select class="selectpicker" name="page" onchange="submit()">
										<c:forEach begin="1" end="${model.myOrderProducts.pageCount}" var="page">
								  	        <option value="${page}" <c:if test="${page == (model.myOrderProducts.page+1)}">selected</c:if>>${page}</option>
										</c:forEach>
									</select>
									<span>of <c:out value="${model.myOrderProducts.pageCount}"/></span>
								</div>
								<c:if test="${model.myOrderProducts.pageCount > 1}">
								<div class="pageNav">
									<c:if test="${model.myOrderProducts.page == 0}"><span class="pageNavLink disabled"><a href="#"><span class="pageNavPrev"></span></a></span></c:if>
					            	<c:if test="${model.myOrderProducts.page > 0}"><span class="pageNavLink"><a href="${_contextpath}/viewMyOrderList.jhtm?page=${model.myOrderProducts.page+1}" class="pageNaviLink"><span class="pageNavPrev"></span></a></span></c:if>
					    			
									<c:if test="${model.myOrderProducts.page == (model.myOrderProducts.pageCount -1)}"><span class="pageNavLink disabled"><a href="#"><span class="pageNavNext"></span></a></span></c:if>
									<c:if test="${model.myOrderProducts.page != (model.myOrderProducts.pageCount - 1)}"><span class="pageNavLink"><a href="${_contextpath}/viewMyOrderList.jhtm?page=${model.myOrderProducts.page+1}"><span class="pageNavNext"></span></a></span></c:if>
								</div>
								</c:if>
							</div>
							</form>
					</div>
				</div>
			</div>
			
			<form action="${_contextpath}/addToCart.jhtm" method="post" onsubmit="return checkForm()" id="view1">
			<div class="myOrderList_table">
				<div class="myOrderListHdr">
					<div class="row clearfix">
						<div class="col-sm-12 col-md-1">
						<c:if test="${siteConfig['PRODUCT_QUOTE'].value == 'true' and (model.showProductQuoteColumn == null or model.showProductQuoteColumn)}" >
							<div class="listSelectHeader">
								<div class="listQuoteHeader">
									<input type="checkbox" onclick="toggleAll(this)">
								</div>
							</div>
						</c:if>
						</div>
						<div class="col-sm-12 col-md-1">
							<div class="listImageHeader"></div>
						</div>
						<div class="col-sm-12 col-md-5">
							<div class="listSkuHeader"><fmt:message key="product" /></div>
						</div>
						<div class="col-sm-12 col-md-3">
							<div class="listPriceHeader"><fmt:message key="f_price"/></div>
						</div>
						<div class="col-sm-12 col-md-1">
						</div>
						<div class="col-sm-12 col-md-1">
							<div class="listQtyHeader"><fmt:message key="f_quantity"/></div>
						</div>
					</div>
				</div>
	
				<div class="myOrderListDetails">
					<c:forEach items="${model.myOrderProducts.pageList}" var="product" varStatus="status">
					<c:set var="statusClass" value="odd_row" />
				    <c:if test="${(status.index + 1) % 2 == 0}">
				      <c:set var="statusClass" value="even_row" />
				    </c:if>
					<c:set var="productLink">${_contextpath}/product.jhtm?id=${product.id}<c:if test="${product == null or siteConfig['RECOMMENDED_LIST_SHOW_BREADCRUMB'].value == 'true'}">&cid=<c:out value="${param.cid}"/></c:if></c:set>
					<c:if test="${gSiteConfig['gMOD_REWRITE'] == '1' and not fn:contains(product.sku, '/')}">
	  					<c:set var="productLink">${_contextpath}/${siteConfig['MOD_REWRITE_PRODUCT'].value}/${product.encodedSku}/${product.encodedName}.html<c:if test="${product == null or siteConfig['RECOMMENDED_LIST_SHOW_BREADCRUMB'].value == 'true'}">?cid=<c:out value="${param.cid}"/></c:if></c:set>
					</c:if>
					<div class="${statusClass} clearfix">
						 <c:if test="${siteConfig['PRODUCT_QUOTE'].value == 'true' and (model.showProductQuoteColumn == null or model.showProductQuoteColumn)}" >
						 <div class="col-sm-12 col-md-1">
							 <c:if test="${product.quote}">
							 	<div class="listSelectHeader visible-xs visible-sm"></div>
								<div class="listSelect">
									<%-- <input type="checkbox" name="selected_id">--%>
									<input name="__selected_product_id" value="${product.id}" type="checkbox"/>	 	
								</div>
								<div class="clearfix"></div>
							 </c:if>
						 </div>
						</c:if>
						<div class="col-sm-12 col-md-1">
							<div class="listImageHeader visible-xs visible-sm">&nbsp;</div>
							<div class="listImageWrapper">
								<c:choose>
									<c:when test="${product.thumbnail == null}">&nbsp;</c:when>
								    <c:otherwise>
								    	<a href="<c:if test="${!product.thumbnail.absolute}">${_contextpath}/assets/Image/Product/detailsbig/</c:if>${product.thumbnail.imageUrl}" data-lightbox="product_images_${product.id}" data-title="<c:out value="${product.name}" escapeXml="false" />">
								          <img class="listImage img-responsive" src="<c:if test="${!product.thumbnail.absolute}">${_contextpath}/assets/Image/Product/thumb/</c:if>${product.thumbnail.imageUrl}" border="0" alt="${product.alt }" alt="<c:out value="${product.name}"/><c:if test="${product.shortDesc != ''}"> - <c:out value="${product.shortDesc}"/></c:if>"/>
								        </a>
								  	</c:otherwise>
								</c:choose>
							</div>
							<div class="clearfix"></div>
						</div>
						<div class="col-sm-12 col-md-5">
							<div class="listNameHeader visible-xs visible-sm"><fmt:message key="product" /></div>
							<div class="listName">
								<div class="listNameContent">
									<a href="${productLink}"><c:out value="${product.name}" escapeXml="false" /></a>
									<a href="${productLink}" class="list_item_sku"><c:out value="${product.sku}" escapeXml="false" /></a>
									<c:if test="${gSiteConfig['gADI']}">
										<div class="list_item_brand"><c:out value="${product.manufactureName}" escapeXml="false" /></div>
									</c:if>
								</div>
							</div>
							<div class="clearfix"></div>
						</div>
						<div class="col-sm-12 col-md-3">
							<div class="listPriceHeader visible-xs visible-sm"><fmt:message key="f_price" /></div>
							<div class="listPrice">
							<input type="hidden" name="product.id" value="${product.id}">
							<%@ include file="/WEB-INF/jsp/frontend/layout/template6/price/price.jsp" %>
							</div>
							<div class="clearfix"></div>
						</div>
						<div class="col-sm-12 col-sm-1"></div>
						<div class="col-sm-12 col-md-1">
							<div class="listQtyHeader visible-xs visible-sm"><fmt:message key="f_quantity" /></div>
							<div class="listQty">
								<input type="text" autocomplete="off" class="quantity" size="6" maxlength="5" name="quantity_${product.id}" id="quantity_${product.id}" value="<c:out value="${cartItem.quantity}"/>">
	        				</div>
							<div class="clearfix"></div>
						</div>
					</div>
					</c:forEach>
				</div>
				
				<div class="myOrderListFtr">
					<div class="row clearfix">
						<div class="col-sm-12">
							<a href="${_contextpath}/addToQuote.jhtm" class="addToQuote_link" onclick="addSelectedItemsToQuote();">Add To Quote</a>
						</div>
					</div>
				</div>
			</div>
	
			<div class="row">
				<div class="col-sm-12">
					<div class="addToCart_btn_wrapper">
						<button type="submit" class="btn addToCart_btn">Add To Cart</button>
					</div>
				</div>
			</div>
			</form>

		  </c:otherwise>
		</c:choose>
		

			
	</div>
</div>

<c:if test="${fn:trim(model.myOrderListLayout.footerHtml) != ''}">
  <c:set value="${model.myOrderListLayout.footerHtml}" var="footerHtml"/>
  <c:set value="${fn:replace(footerHtml, '#firstname#', userSession.firstName)}" var="footerHtml"/>
  <c:set value="${fn:replace(footerHtml, '#lastname#', userSession.lastName)}" var="footerHtml"/>
  <c:set value="${fn:replace(footerHtml, '#email#', userSession.username)}" var="footerHtml"/>
  <c:out value="${footerHtml}" escapeXml="false"/>
</c:if>

</c:if>

<c:if test="${siteConfig['PRODUCT_QUOTE'].value == 'true'}">
<script type="text/javascript">
<!--
function addToQuoteList(productId){
	$.ajax({
		url: "${_contextpath}/quoteList.jhtm?productId="+productId,
		context: document.body
	}).done(function() {
	});
}

function toggleAll(ele){
	$("input[name=__selected_product_id]").each(function(){
		if(ele.checked) {
			$(this).prop('checked', true); 
		} else {
			$(this).prop('checked', false);
		} 
	});
}

function addSelectedItemsToQuote(){
	$("input[name=__selected_product_id]").each(function(){
		if($(this).is(':checked')) {
			//alert($(this).val());  
			$.ajax({
					url: "${_contextpath}/quoteList.jhtm?productId="+$(this).val(),
					context: document.body
			}).done(function() {
			}); 
		}
	});
}
//-->
</script>
</c:if>

  </tiles:putAttribute>
</tiles:insertDefinition>