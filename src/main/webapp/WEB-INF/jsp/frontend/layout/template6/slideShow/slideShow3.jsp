<%@ page session="false" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>

<c:if test="${featuredProducts != null}">
<div class="products_carousel owl-carousel">
  <c:forEach items="${featuredProducts}" var="product" varStatus="status">
	<c:set var="productLink">${_contextpath}/product.jhtm?id=${product.id}<c:if test="${model.product == null or siteConfig['RECOMMENDED_LIST_SHOW_BREADCRUMB'].value == 'true'}">&cid=<c:out value="${param.cid}"/></c:if></c:set>
	<c:if test="${gSiteConfig['gMOD_REWRITE'] == '1' and not fn:contains(product.sku, '/')}">
	  <c:set var="productLink">${_contextpath}/${siteConfig['MOD_REWRITE_PRODUCT'].value}/${product.encodedSku}/${product.encodedName}.html<c:if test="${model.product == null or siteConfig['RECOMMENDED_LIST_SHOW_BREADCRUMB'].value == 'true'}">?cid=<c:out value="${param.cid}"/></c:if></c:set>
	</c:if>	
			
	<div class="product">
	  <div class="product_image_div">
		<a href="${productLink}">
		  <img class="item" src="<c:if test="${!product.thumbnail.absolute}">${_contextpath}/assets/Image/Product/thumb/</c:if><c:out value="${product.thumbnail.imageUrl}"/>">
		</a>
	  </div>
	  <div class="product_name"><a href="${productLink}"><c:out value="${product.name}" escapeXml="false" /></a></div>
	  <div class="product_price"> 
		<c:choose>
		  <c:when test="${fn:length(product.price) > 1}">
			<span class="p_from_id"><fmt:message key="${siteConfig['CURRENCY'].value}" /><fmt:formatNumber type="currency"  pattern="#,##0.00" value="${product.priceRangeMinimum}" /> </span>
			<span class="price_range_separator">-</span>
			<span class="p_to_id"><fmt:message key="${siteConfig['CURRENCY'].value}" /><fmt:formatNumber type="currency"  pattern="#,##0.00" value="${product.priceRangeMaximum}" /></span>
		  </c:when>
		  <c:otherwise>
			<span class="p_from_id"><fmt:message key="${siteConfig['CURRENCY'].value}" /><fmt:formatNumber type="currency"  pattern="#,##0.00" value="${product.price1}" /> </span>
		  </c:otherwise>
		</c:choose>
	  </div>
	</div>
  </c:forEach>
</div>
</c:if>