<%@ page session="false" %>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ taglib uri="/WEB-INF/tlds/custom.tld" prefix="wj" %>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>



<tiles:insertDefinition name="${_template}" flush="true">
  <c:if test="${_leftBar != '1' and _leftBar != '4'}">
    <tiles:putAttribute name="leftbar" value="/WEB-INF/jsp/frontend/layout/template${_leftBar}/leftbar.jsp" />
  </c:if>
  <tiles:putAttribute name="content" type="string">
  
  
  
  
<div class="col-breadcrumb">
	<ul class="breadcrumb">
		<li><a href="${_contextpath}/account.jhtm"> <fmt:message key="myAccount" /> </a></li>
		<li class="active"><fmt:message key="myList" /></li>
	</ul>
</div>

<c:if test="${gSiteConfig['gMYLIST']}">

<c:if test="${fn:trim(model.mylistLayout.headerHtml) != ''}">
  <c:set value="${model.mylistLayout.headerHtml}" var="headerHtml"/>
  <c:set value="${fn:replace(headerHtml, '#groupName#', model.groupName)}" var="headerHtml"/>
</c:if>
<c:out value="${headerHtml}" escapeXml="false"/>



<div class="col-sm-12">
	<div class="myListWrapper">
		<div class="headerTitle myList"><fmt:message key="myList" /></div>

		<div class="clearfix"></div>

		<c:choose>
			<c:when test="${model.products.nrOfElements == 0}">
				<div class="message">Your List is empty.</div>
			</c:when>
			<c:otherwise>
				<c:if test="${model.products.pageCount > 1}">
				<div class="row">
					<div class="col-sm-12">
						<div class="pageNavigation">
							<form class="formPageNavigation clearfix" name="pagination" action="${_contextpath}/myList.jhtm" method="post">
								<div class="pageNavigationControl">
									<%--
									<div class="pagesize">
										<span>Page Size</span> 
										<select class="selectpicker" name="pagesize">
											<option value="10">10 per page</option>
											<option value="20">20 per page</option>
											<option value="25" selected="">25 per page</option>
											<option value="50">50 per page</option>
											<option value="100">100 per page</option>
										</select>
									</div>
									 --%>
									
									<div class="page">
										<span>Page</span> 
										<select class="selectpicker" name="page" onchange="submit()" >
											<c:forEach begin="1" end="${model.products.pageCount}" var="page">
									  	    	<option value="${page}" <c:if test="${page == (model.products.page+1)}">selected</c:if>>${page}</option>
											</c:forEach>
										</select> <span>of <c:out value="${model.products.pageCount}"/></span>
									</div>
									
									<%--
									<div class="pageNav">
										<span class="pageNavLink"> <a href="#"><span
												class="pageNavPrev"></span></a>
										</span> <span class="pageNavLink"> <a href="#"><span
												class="pageNavNext"></span></a>
										</span>
									</div>
									--%>
								</div>
							</form>
						</div>
					</div>
				</div>
				</c:if>
			
				<form name="mylist" method="post">
					<div class="myList_table">
					
						<div class="myListHdr">
							<div class="row clearfix">
								<div class="col-sm-12 col-md-1">
									<div class="listSelectHeader">
										<input type="checkbox" onclick="toggleAll(this)">
									</div>
								</div>
								<div class="col-sm-12 col-md-1">
									<div class="listImageHeader"><fmt:message key="product"/></div>
								</div>
								<div class="col-sm-12 col-md-4">
									<div class="listNameHeader"></div>
								</div>
								<div class="col-sm-12 col-md-3">
									<div class="listPriceHeader"><fmt:message key="f_price"/></div>
								</div>
								<div class="col-sm-12 col-md-2">
									<div class="listAddedHeader"><fmt:message key="f_added"/></div>
								</div>
								<div class="col-sm-12 col-md-1">
									<div class="listQtyHeader"><fmt:message key="f_quantity"/></div>
								</div>
							</div>
						</div>
		
						<div class="myListDetails">
							<c:forEach items="${model.products.pageList}" var="product" varStatus="status">
							<c:set var="statusClass" value="odd_row" />
						    <c:if test="${(status.index + 1) % 2 == 0}">
						      <c:set var="statusClass" value="even_row" />
						    </c:if>
				    		<c:set var="productLink">${_contextpath}/product.jhtm?id=${product.id}</c:set>
							<c:if test="${gSiteConfig['gMOD_REWRITE'] == '1' and not fn:contains(product.sku, '/')}">
							  <c:set var="productLink">${_contextpath}/${siteConfig['MOD_REWRITE_PRODUCT'].value}/${product.encodedSku}/${product.encodedName}.html</c:set>
							</c:if>	
			      	   	  
				    
							<div class="${statusClass} clearfix">
								<div class="col-sm-12 col-md-1">
									<div class="listSelectHeader visible-xs visible-sm">Select</div>
									<div class="listSelect">
										<input type="checkbox" name="__selected_id" value="${product.id}" > 
									</div>
									<div class="clearfix"></div>
								</div>
								<div class="col-sm-12 col-md-1">
									<div class="listImageHeader visible-xs visible-sm">&nbsp;</div>
									<div class="listImageWrapper">
										<c:if test="${product.thumbnail != null}">
			      							<img class="listImage img-responsive" src="<c:if test="${!product.thumbnail.absolute}">${_contextpath}/assets/Image/Product/thumb/</c:if>${product.thumbnail.imageUrl}" border="0" alt="${product.alt}"/><br />
			    						</c:if>
			    					</div>
									<div class="clearfix"></div>
								</div>
								<div class="col-sm-12 col-md-4">
									<div class="listSkuHeader visible-xs visible-sm"><fmt:message key="product"/></div>
									<div class="listName">
										<div class="listNameContent">
											<a href="${productLink}" class="list_item_name"><c:out value="${product.name}" escapeXml="false" /></a>
											<a href="${productLink}" class="list_item_sku"><c:out value="${product.sku}" escapeXml="false" /></a>
											<c:if test="${gSiteConfig['gADI']}"> 	
								          		<div class="list_item_brand"><c:out value="${product.manufactureName}" escapeXml="false"/></div>
										  	</c:if>
										</div>
									</div>
									<div class="clearfix"></div>
								</div>
								<div class="col-sm-12 col-md-3">
									<div class="listPriceHeader visible-xs visible-sm"><fmt:message key="f_price"/></div>
									<div class="listPrice">
										<%@ include file="/WEB-INF/jsp/frontend/layout/template6/price/price.jsp" %>
									</div>
									<div class="clearfix"></div>
								</div>
								<div class="col-sm-12 col-md-2">
									<div class="listAddedHeader visible-xs visible-sm"><fmt:message key="f_added"/></div>
									<div class="listAdded"><fmt:formatDate type="date" timeStyle="default" value="${product.created}"/></div>
									<div class="clearfix"></div>
								</div>
								<div class="col-sm-12 col-md-1">
									<div class="listQtyHeader visible-xs visible-sm"><fmt:message key="f_quantity"/></div>
									<div class="listQty">
										<input type="hidden" name="product.id" id="product_${product.id}" value="${product.id}">

										<c:choose>
										<c:when test="${product.minimumQty == null || product.minimumQty<=0}">
											<input type="text" size="6" maxlength="5" name="quantity_${product.id}" id="quantity_${product.id}" value="1" >
										</c:when>
										<c:otherwise>
											<input type="text" size="6" maxlength="5" name="quantity_${product.id}" id="quantity_${product.id}" value="${product.minimumQty}" >
										</c:otherwise>
										</c:choose>
										
									</div>
									<div class="clearfix"></div>
								</div>
							</div>
							</c:forEach>
						</div>
					
						<div class="myListFtr">
							<div class="row clearfix">
								<div class="col-sm-12">
									<%--
									<select name="selectAction" id="selectActionId">
										<option value="1" selected>Delete</option>
										<option value="2">Add To Quote</option>
									</select>
									<button type="submit" class="btn btn-default" onclick="checkAction()">Submit</button>--%>
									<button type="submit" name="__delete" class="btn deleteProducts_btn" onClick="return deleteSelected()" >Delete Selected Products</button>
									<%--
									<input type="hidden" name="__delete" id="__delete">
									<input type="hidden" name="_addtoquote_quick"> --%>
								</div>
							</div>
							<%-- 
							<c:if test="${fn:length(model.groupList) > 0}">
								<div class="row clearfix">
									<div class="col-sm-4">
										<div class="listSelectGroup">
											<select name="_groupId">
										      <option value="0">Select Group</option>    
											  <c:forEach items="${model.groupList}" var="group">
											     <c:if test="${group.id != model.viewGroupId}">
												  <option value="${group.id}"><c:out value="${group.name}" /></option>
												 </c:if>    
											  </c:forEach>
										    </select>
										</div>
									</div>
									<div class="col-sm-4"></div>
									<div class="col-sm-4">
										<div class="addToGroup_btn_wrapper">
											<button type="submit" class="btn addToGroup_btn">Add To
												Group</button>
										</div>
									</div>
								</div>
							</c:if>--%>
						</div>
				    </div>
					
					<div class="row">
						<div class="col-sm-12">
							<c:if test="${gSiteConfig['gSHOPPING_CART']}">
							<div class="addToCart_btn_wrapper">
								<button type="submit" name="_addtocart_quick" class="btn addToCart_btn" onclick="return addToCart(this.form);">Add To
									Cart</button>
							</div>
							</c:if>
						</div>
					</div>
				</form>	
			</c:otherwise>
		</c:choose>
		
	</div>
</div>  
  
</c:if>  
  
  
<c:if test="${model.products.nrOfElements > 0}">
<script type="text/javascript">
<!--

function toggleAll(ele){
	$("input[name=__selected_id]").each(function(){
		if(ele.checked) {
			$(this).prop('checked', true); 
		} else {
			$(this).prop('checked', false);
		} 
	});
}

function deleteSelected(){
	var itemSel = false;
	$("input[name=__selected_id]").each(function(){
		if($(this).prop('checked')) {
			itemSel = true;
			return false;
		} 
	});
	
	if(itemSel) {
		return confirm('Delete permanently?');
	} else {
	    alert("Please select product(s) to delete.");       
	    return false;
	}
}

function addToCart(aform) {
    var itemSel = false;
	$("input[name=__selected_id]").each(function(){
		var productId = $(this).val();
		if($(this).prop('checked')) {
			$("#product_"+productId).prop('disabled', false);
			itemSel = true;
		} else {
			$("#product_"+productId).prop('disabled', true);
		}
	});
	
	if(!itemSel) {
		alert("Please select product(s) to add to cart.");
		return false;
	}
	$(aform).attr('action', "addToCart.jhtm").submit();
}
//-->
</script>

<%-- 
<c:if test="${siteConfig['PRODUCT_QUOTE'].value == 'true'}">
<script type="text/javascript">
<!--
function checkAction() {
	var ids = document.getElementsByName("__selected_id");
	var check = false;
	for (i = 0; i < ids.length; i++) {
        if (document.mylist.__selected_id[i].checked) {
        	check = true;
        }
    }
	if(check){
		var action = document.getElementById("selectActionId").value;
		if(action == 1) {
			document.getElementById("__delete").value = "delete";
		}
	} else {
		alert("Please select product(s) to delete.");       
	    return false;
	}
}
function addToQuote(aform) {
    aform.action = 'addToQuote.jhtm';
    submit();
}
function addToQuoteList(productId){
	$.ajax({
		url: "${_contextpath}/quoteList.jhtm?productId="+productId,
		context: document.body
	}).done(function() {
		$("#addToQuoteListWrapperId"+productId).html('<a href=\"${_contextpath}/quoteViewList.jhtm\" class=\"btn addToQuote_btn\">View Quote List</a>');
	});
}
//-->
</script>
</c:if>
--%>
</c:if>

<c:if test="${fn:trim(model.mylistLayout.footerHtml) != ''}">
  <c:set value="${model.mylistLayout.footerHtml}" var="footerHtml"/>
  <c:set value="${fn:replace(footerHtml, '#groupName#', model.groupName)}" var="footerHtml"/>
</c:if>
<c:out value="${footerHtml}" escapeXml="false"/>



  </tiles:putAttribute>
</tiles:insertDefinition>
