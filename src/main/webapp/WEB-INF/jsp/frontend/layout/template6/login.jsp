<%@ page session="false" %>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="/WEB-INF/tlds/spring.tld" prefix="spring" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>


<script src="/dv/w3data.js"></script>
<script type='text/javascript' src='/wp-includes/js/jquery/jquery.js?ver=1.12.4'></script>
<script type='text/javascript' src='/wp-content/themes/bridge/js/plugins.js?ver=4.6.1'></script>
<script type='text/javascript' src='/wp-content/themes/bridge/js/default.min.js?ver=4.6.1'></script>



<tiles:insertDefinition name="${_template}" flush="true">
  <c:if test="${_leftBar != '1' and _leftBar != '4'}">
    <tiles:putAttribute name="leftbar" value="/WEB-INF/jsp/frontend/layout/template${_leftBar}/leftbar.jsp" />
  </c:if>
  <tiles:putAttribute name="content" type="string">
  
<div class="col-sm-12">
  <div id="loginWrapper">
	<div class="row">
	  <div class="col-sm-offset-2 col-sm-8 col-md-offset-3 col-md-6">
	    <c:if test="${!(gSiteConfig['gREGISTRATION_DISABLED'] or siteConfig['PROTECTED_HOST'].value == header['host'])}">
	    <c:out value="${model.loginLayout.headerHtml}" escapeXml="false"/>
		
		<%@ include file="/WEB-INF/jsp/custom/login1.jsp" %>
		
		<c:choose>
		  <c:when test="${empty model.containsMaliciousCode}">
		    <c:if test="${param.noMessage != 't'}">
			  <c:if test="${!empty model.message}">
			    <div class="error"><br/><b><font color="RED"><fmt:message key="${model.message}"><fmt:param value="${siteConfig['IP_ADDRESS_ERROR_MESSAGE'].value}"/></fmt:message></font></b></div>
			  </c:if>
		    </c:if>
		  </c:when>
		  <c:otherwise>
		    <div class="error"><br/><b><font color="RED"><fmt:message key="form.removeScriptTags"></fmt:message></font></b></div>
		  </c:otherwise>
		</c:choose>
	    
	    
		<form id="login_existingCustomer_form" class="form-horizontal" role="form" method="POST" action="${model.secureUrl}/login.jhtm">
		  <c:if test="${!empty signonForwardAction}">
			<input id="forwardActionId" type="hidden" name="forwardAction" value="${signonForwardAction}"/>
		  </c:if>
		  
		  <input id="redirectSku" type="hidden" name="redirectSku" value="${model.redirectSku}"/>
		  
		  <div class="form-group">
			<div class="col-sm-12">
			  <h3>Customer Login</h3>
			</div>
		  </div>
		  <div class="form-group">
			<div class="col-sm-4">
			  <label for="username" class="control-label email_label">Email Address or Cell Phone</label>
			</div>
			<div class="col-sm-8">
			  <div class="input-group">
				<span class="input-group-addon"><span class="glyphicon glyphicon-user"></span></span>
				  <c:choose>
				    <c:when test="${model.message != 'REMOVE_SCRIPTS'}"><input type="text" autocomplete="off" class="form-control" id="username" value="${param.username}" name="username" placeholder="Email"></c:when>
				    <c:otherwise><input type="text" autocomplete="off" class="form-control" id="username" name="username" placeholder="Email"></c:otherwise>
				  </c:choose>
			  </div>
			</div>
		  </div>
		  
		  <div class="form-group">
			<div class="col-sm-4">
			  <label for="password" class="control-label">Password</label>
			</div>
			<div class="col-sm-8">
			  <div class="input-group">
				<span class="input-group-addon"><span class="glyphicon glyphicon-lock"></span></span>
				<input type="password" class="form-control" id="password" name="password" placeholder="Password">
			  </div>
			</div>
		  </div>
		  <c:if test="${siteConfig['COOKIE_AGE_KEEP_ME_LOGIN'].value != -1}">
		  <div class="form-group">
			<div class="col-sm-offset-4 col-sm-8">
			  <div class="checkbox">
				<label><input type="checkbox" name="keepmelogin" value="1">Remember me</label>
			  </div>
			</div>
		  </div>
		  </c:if>
		  
		  <div class="form-group">
			<div class="col-sm-offset-4 col-sm-8">
			  <button type="submit" class="btn btn-default">Log In</button>
			</div>
		  </div>
		  <div class="form-group">
			<div class="col-sm-offset-4 col-sm-8">
			  <p class="help-block">
				Forgot your password? <a class="forgetPassword" href="${_contextpath}/forgetPassword.jhtm">Click to reset</a>
			  </p>
			  <p class="help-block">
				New Customer? <a class="forgetPassword" href="/register.jhtm<c:if test="${!empty signonForwardAction}">?forwardAction=${signonForwardAction}</c:if>">Create New Account</a>
			  </p>
			</div>
		  </div>
		</form>
		
		<%@ include file="/WEB-INF/jsp/custom/login2.jsp" %>
		
		
		</c:if>
	  </div>
	</div>
  </div>
</div>  
  
  
<c:out value="${model.loginLayout.footerHtml}" escapeXml="false"/>

  </tiles:putAttribute>
</tiles:insertDefinition>
