<%@ page session="false" %>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="/WEB-INF/tlds/spring.tld" prefix="spring" %>

<tiles:insertDefinition name="${_template}" flush="true">
  <tiles:putAttribute name="content" type="string">
  
	<div class="col-sm-12">
		<form id="forgetPasswordForm" class="form-horizontal" role="form" method="post">
			<div class="message"></div>
			<div id="forgetPassword">
				
				<div class="form-group">
					<div class="col-sm-12">
						<h3>Change Your Password in Three Easy Steps.</h3>
					</div>
				</div>
				<div class="form-group">
					<div class="col-sm-12">
						<div class="stepLabel"><span> Step 1.</span> Enter the e-mail address associated with your <c:out value="${model.SITE_NAME}"/> account (done).</div>
					</div>
				</div>
				<div class="form-group">
					<div class="col-sm-12">
						<div class="stepLabel"><span><img src="${_contextpath}/assets/Image/Layout/select_right_8x8.gif" border="0"> Step 2.</span> Check your e-mail (including your Spam or Junk filter/s) and open the message from <c:out value="${model.CONTACT_EMAIL}"/> that contains the subject line "<c:out value="${model.SITE_NAME}"/> Password Assistance." Click on the link indicated in the message to get to Step 3 </div>
					</div>
				</div>
				<div class="form-group">
					<div class="col-sm-12">
						We have sent an e-mail to ${forgetPasswordForm.email}. When you receive the e-mail please click on the link in the e-mail.
					</div>
				</div>
				
			</div>
		</form>
	</div>
	

  </tiles:putAttribute>
</tiles:insertDefinition>