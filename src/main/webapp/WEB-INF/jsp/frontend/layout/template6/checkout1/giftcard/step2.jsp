<%@ page session="false" %>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ taglib uri="/WEB-INF/tlds/spring.tld" prefix="spring" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>

<tiles:insertDefinition name="${_template}" flush="true">
  <tiles:putAttribute name="content" type="string">

<div class="col-breadcrumb">
	<ul class="breadcrumb">
		<li><a href="${_contextpath}/account.jhtm">My Account</a></li>
		<li class="active">Gift Card</li>
	</ul>
</div>

<c:out value="${giftCardLayout.headerHtml}" escapeXml="false"/>
<c:if test="${model.message != null}">
	<div class="message"><fmt:message key="${model.message}" /></div>
</c:if>
  

<div class="col-sm-12">
  <form:form commandName="giftCardForm" class="form-horizontal" role="form" method="POST">
  
  <input type="hidden" name="_page" value="1">
  
  <div id="giftCard">
    <div class="row">
		<div class="col-sm-6">
			<div class="form-group">
				<div class="col-sm-12">
					<h3><fmt:message key="billingInformation" /></h3>
				</div>
			</div>
			<spring:bind path="giftCardOrder.billing.firstName">
				<div class="form-group <c:if test="${status.error}">has-feedback has-error</c:if>">
					<div class="col-sm-4">
						<label for="giftCardOrder_billing_firstName" class="control-label"> <fmt:message key="firstName" /> <sup class="requiredField">*</sup></label>
					</div>
					<div class="col-sm-8">
						<form:input class="form-control" id="giftCardOrder_billing_firstName" path="giftCardOrder.billing.firstName" size="20"/>
	      				<c:if test="${status.error}">
							<i class="form-control-feedback glyphicon glyphicon-remove"></i> 
							<small class="help-block error-message"><form:errors path="giftCardOrder.billing.firstName" /></small>
						</c:if>
	      			</div>	
				</div>
			</spring:bind>
					
			<spring:bind path="giftCardOrder.billing.lastName">
				<div class="form-group <c:if test="${status.error}">has-feedback has-error</c:if>">
					<div class="col-sm-4">
						<label for="giftCardOrder_billing_lastName" class="control-label"> <fmt:message key="lastName" /> <sup class="requiredField">*</sup></label>
					</div>
					<div class="col-sm-8">
						<form:input class="form-control" id="giftCardOrder_billing_lastName" path="giftCardOrder.billing.lastName" size="20"/>
	      				<c:if test="${status.error}">
							<i class="form-control-feedback glyphicon glyphicon-remove"></i> 
							<small class="help-block error-message"><form:errors path="giftCardOrder.billing.lastName" /></small>
						</c:if>
	      			</div>	
				</div>
			</spring:bind>
					
			<spring:bind path="giftCardOrder.billing.addr1">
				<div class="form-group <c:if test="${status.error}">has-feedback has-error</c:if>">
					<div class="col-sm-4">
						<label for="giftCardOrder_billing_addr1" class="control-label"> <fmt:message key="address" /> 1 <sup class="requiredField">*</sup></label>
					</div>
					<div class="col-sm-8">
						<form:input class="form-control" id="giftCardOrder_billing_addr1" path="giftCardOrder.billing.addr1" size="20"/>
	      				<c:if test="${status.error}">
							<i class="form-control-feedback glyphicon glyphicon-remove"></i> 
							<small class="help-block error-message"><form:errors path="giftCardOrder.billing.addr1" /></small>
						</c:if>
	      			</div>	
				</div>
			</spring:bind>

			<spring:bind path="giftCardOrder.billing.addr2">
				<div class="form-group <c:if test="${status.error}">has-feedback has-error</c:if>">
					<div class="col-sm-4">
						<label for="giftCardOrder_billing_addr2" class="control-label"> <fmt:message key="address" /> 2 </label>
					</div>
					<div class="col-sm-8">
						<form:input class="form-control" id="giftCardOrder_billing_addr2" path="giftCardOrder.billing.addr2" size="20"/>
	      				<c:if test="${status.error}">
							<i class="form-control-feedback glyphicon glyphicon-remove"></i> 
							<small class="help-block error-message"><form:errors path="giftCardOrder.billing.addr2" /></small>
						</c:if>
	      			</div>	
				</div>
			</spring:bind>
					
			<spring:bind path="giftCardOrder.billing.city">
				<div class="form-group <c:if test="${status.error}">has-feedback has-error</c:if>">
					<div class="col-sm-4">
						<label for="giftCardOrder_billing_city" class="control-label"> <fmt:message key="city" /> <sup class="requiredField">*</sup></label>
					</div>
					<div class="col-sm-8">
						<form:input class="form-control" id="giftCardOrder_billing_city" path="giftCardOrder.billing.city" size="20"/>
	      				<c:if test="${status.error}">
							<i class="form-control-feedback glyphicon glyphicon-remove"></i> 
							<small class="help-block error-message"><form:errors path="giftCardOrder.billing.city" /></small>
						</c:if>
	      			</div>	
				</div>
			</spring:bind>
			
			<spring:bind path="giftCardOrder.billing.country">
				<div class="form-group <c:if test="${status.error}">has-feedback has-error</c:if>">
					<div class="col-sm-4">
						<label for="giftCardOrder_billing_country" class="control-label"> <fmt:message key="country" /> <sup class="requiredField">*</sup></label>
					</div>
					<div class="col-sm-8">
						<form:select class="form-control" id="giftCardOrder_billing_country" path="giftCardOrder.billing.country" >
					       <form:option value="" label="Please Select"/>
					       <form:options items="${model.countrylist}" itemValue="code" itemLabel="name"/>
					    </form:select>
						<c:if test="${status.error}">
							<i class="form-control-feedback glyphicon glyphicon-remove"></i> 
							<small class="help-block error-message">
							<form:errors path="giftCardOrder.billing.country" />      
		  				</small>
						</c:if>
	      			</div>	
				</div>
			</spring:bind>
			
			
			  
					
			<spring:bind path="giftCardOrder.billing.stateProvince">
				<div class="form-group">
					<div class="col-sm-4">
						<label for="" class="control-label"> State/Province <sup
							class="requiredField">*</sup>
						</label>
					</div>
					<div class="col-sm-8">
						<form:input class="form-control" id="giftCardOrder_billing_stateProvince" path="giftCardOrder.billing.stateProvince" htmlEscape="true" />
		      			<div id="stateProvinceNA" class="checkbox">
							<label>
							  <form:checkbox path="giftCardOrder.billing.stateProvince" id="giftCardOrder_billing_stateProvinceNA" value="true"  disabled="${customerForm.customer.address.country == 'US' or customerForm.customer.address.country == 'CA' or customerForm.customer.address.country == '' or customerForm.customer.address.country == null}"/><fmt:message key="notApplicable"/>
							</label>
						</div>
						<form:select class="form-control" id="giftCardOrder_billing_state" path="giftCardOrder.billing.stateProvince" disabled="${giftCardForm.giftCardOrder.billing.country != 'US'}">
				           <form:option value="" label="Please Select"/>
				           <form:options items="${model.statelist}" itemValue="code" itemLabel="name"/>
				        </form:select>
		         
				        <form:select class="form-control" id="giftCardOrder_billing_ca_province" path="giftCardOrder.billing.stateProvince" disabled="${giftCardForm.giftCardOrder.billing.country != 'CA'}">
				           <form:option value="" label="Please Select"/>
				           <form:options items="${model.caProvinceList}" itemValue="code" itemLabel="name"/>
				        </form:select>
		         
					    <c:if test="${status.error}">
							<form:errors path="giftCardOrder.billing.stateProvince" cssClass="error" />
						</c:if>
					</div>
				</div>
			</spring:bind>
					
			<spring:bind path="giftCardOrder.billing.zip">
				<div class="form-group <c:if test="${status.error}">has-feedback has-error</c:if>">
					<div class="col-sm-4">
						<label for="giftCardOrder_billing_zip" class="control-label"> <fmt:message key="zipCode" /> <sup class="requiredField">*</sup></label>
					</div>
					<div class="col-sm-8">
						<form:input class="form-control" id="giftCardOrder_billing_zip" path="giftCardOrder.billing.zip" size="20"/>
	      				<c:if test="${status.error}">
							<i class="form-control-feedback glyphicon glyphicon-remove"></i> 
							<small class="help-block error-message"><form:errors path="giftCardOrder.billing.zip" /></small>
						</c:if>
	      			</div>	
				</div>
			</spring:bind>
					
			<spring:bind path="giftCardOrder.billing.phone">
				<div class="form-group <c:if test="${status.error}">has-feedback has-error</c:if>">
					<div class="col-sm-4">
						<label for="giftCardOrder_billing_phone" class="control-label"> <fmt:message key="phone" /> </label>
					</div>
					<div class="col-sm-8">
						<form:input class="form-control" id="giftCardOrder_billing_phone" path="giftCardOrder.billing.phone" size="20"/>
	      				<c:if test="${status.error}">
							<i class="form-control-feedback glyphicon glyphicon-remove"></i> 
							<small class="help-block error-message"><form:errors path="giftCardOrder.billing.phone" /></small>
						</c:if>
	      			</div>	
				</div>
			</spring:bind>
					
			<spring:bind path="giftCardOrder.billing.cellPhone">
				<div class="form-group <c:if test="${status.error}">has-feedback has-error</c:if>">
					<div class="col-sm-4">
						<label for="giftCardOrder_billing_cellPhone" class="control-label"> <fmt:message key="cellPhone" /></label>
					</div>
					<div class="col-sm-8">
						<form:input class="form-control" id="giftCardOrder_billing_cellPhone" path="giftCardOrder.billing.cellPhone" size="20"/>
	      				<c:if test="${status.error}">
							<i class="form-control-feedback glyphicon glyphicon-remove"></i> 
							<small class="help-block error-message"><form:errors path="giftCardOrder.billing.cellPhone" /></small>
						</c:if>
	      			</div>	
				</div>
			</spring:bind>
					
			<spring:bind path="giftCardOrder.paymentMethod">
				<div class="message"><c:out value="${status.errorMessage}"/></div>
			</spring:bind>		 
		
			<c:if test="${gSiteConfig['gPAYPAL'] < 2 and siteConfig['CREDIT_CARD_PAYMENT'].value != ''}">
			<div class="form-group">
				<div class="col-sm-12">
					<div id="cc_radio" class="radio">
						<label> 
						 	<form:radiobutton id="giftCardOrder_paymentMethod" path="giftCardOrder.paymentMethod" value="Credit Card" /> <fmt:message key="payWithCreditCard" />
				   		</label>
					</div>
				</div>
			</div>
			
			<spring:bind path="giftCardOrder.creditCard.type">
			<div class="form-group <c:if test="${status.error}">has-error</c:if>">
				<div class="col-sm-4">
					<label for="giftCardOrder_creditCard_type" class="control-label">
						<fmt:message key="cardType" /> <sup class="requiredField">*</sup>
					</label>
				</div>
				<div class="col-sm-8">
					<form:select class="form-control" id="giftCardOrder_creditCard_type" path="giftCardOrder.creditCard.type">
				       <option value="">Please Select</option>
					   <c:forTokens items="VISA,AMEX,MC,DISC" delims="," var="cc">
					   		<option value="${cc}" <c:if test="${giftCardForm.giftCardOrder.creditCard.type == cc}">selected</c:if>><fmt:message key="${cc}"/></option>
					   </c:forTokens>
					</form:select>
      				<c:if test="${status.error}">
						<small class="help-block error-message"><form:errors path="giftCardOrder.creditCard.type"/></small>
					</c:if>
				</div>
			</div>
			</spring:bind>
			
			<spring:bind path="giftCardOrder.creditCard.number">
			<div class="form-group <c:if test="${status.error}">has-feedback has-error</c:if>">
				<div class="col-sm-4">
					<label for="giftCardOrder_creditCard_number"
						class="control-label"> <fmt:message key="cardNumber" /> <sup
						class="requiredField">*</sup>
					</label>
				</div>
				<div class="col-sm-8">
					<form:input path="giftCardOrder.creditCard.number" class="form-control" id="giftCardOrder_creditCard_number" maxlength="16" size="16"/>
      				<c:if test="${status.error}">
						<i class="form-control-feedback glyphicon glyphicon-remove"></i> 
						<small class="help-block error-message"><form:errors path="giftCardOrder.creditCard.number"/></small>
					</c:if>
	      		</div>
			</div>
			</spring:bind>
					
			<spring:bind path="giftCardOrder.creditCard.cardCode">
			<div class="form-group <c:if test="${status.error}">has-feedback has-error</c:if>">
				<div class="col-sm-4">
					<label for="giftCardOrder_creditCard_cardCode"
						class="control-label"> Verification Code <sup
						class="requiredField">*</sup>
					</label>
				</div>
				<div class="col-sm-8">
					<form:input path="giftCardOrder.creditCard.cardCode" id="giftCardOrder_creditCard_cardCode" class="form-control" maxlength="4" size="4"/>
					<c:if test="${status.error}">
						<i class="form-control-feedback glyphicon glyphicon-remove"></i> 
						<small class="help-block error-message"><form:errors path="giftCardOrder.creditCard.cardCode"/></small>
					</c:if>
				</div>
			</div>
			</spring:bind>
					
		  	<div class="form-group">
				<div class="col-sm-4">
					<label for="" class="control-label"> <fmt:message key="expDate" /> (MM/YY): <sup
						class="requiredField">*</sup>
					</label>
				</div>
				<div class="col-sm-8">
					<div class="row">
						<div class="col-xs-6 col-sm-6">
							<form:select class="form-control" id="giftCardOrder_creditCard_expireMonth" path="giftCardOrder.creditCard.expireMonth">
						       <c:forTokens items="01,02,03,04,05,06,07,08,09,10,11,12" delims="," var="ccExpireMonth">
					            <option value="${ccExpireMonth}" <c:if test="${giftCardForm.giftCardOrder.creditCard.expireMonth==ccExpireMonth}">selected</c:if>><c:out value="${ccExpireMonth}"/></option>
					           </c:forTokens>
						    </form:select>
	      				</div>
						<div class="col-xs-6 col-sm-6">
							<form:select class="form-control" id="giftCardOrder_creditCard_expireYear" path="giftCardOrder.creditCard.expireYear">
						       <c:forEach items="${model.expireYears}" var="ccExpireYear">
					            <option value="${ccExpireYear}" <c:if test="${giftCardForm.giftCardOrder.creditCard.expireYear==ccExpireYear}">selected</c:if>><c:out value="${ccExpireYear}"/></option>
						       </c:forEach>
						    </form:select>
						</div>
					</div>
				</div>
			</div>
			</c:if>
						
			<c:if test="${gSiteConfig['gPAYPAL'] > 0}">
				<spring:bind path="giftCardOrder.paymentMethod">
					<div class="form-group">
						<div class="col-sm-12">
							<div id="cc_radio" class="radio">
								<label> 
								 	<form:radiobutton id="giftCardOrder_paymentMethod" path="giftCardOrder.paymentMethod" value="PayPal" /> Paypal
				   				</label>
							</div>
						</div>
					</div>
					<div class="form-group">
						<div class="col-sm-12">
						   <div id="paypalTextImageBox" class="imgText">
						       <div id="paypalImage" class="image"><a href="#" onclick="javascript:window.open('https://www.paypal.com/cgi-bin/webscr?cmd=xpt/Marketing/popup/OLCWhatIsPayPal-outside','olcwhatispaypal','toolbar=no, location=no, directories=no, status=no, menubar=no, scrollbars=yes, resizable=yes, width=400, height=350');"><img  src="https://www.paypal.com/en_US/i/logo/PayPal_mark_60x38.gif" border="0" alt="Acceptance Mark"></a></div>
							    <div id="paypalNone" class="note"><c:out value="${siteConfig['PAYPAL_PAYMENT_METHOD_NOTE'].value}" escapeXml="false"/></div>
							</div>
						</div>
					</div>
				</spring:bind>
			</c:if>
	    
		</div>

	</div>
	<div class="col-sm-6"></div>
  </div>
  <div id="form_buttons">
	<div class="row">
		<div class="col-sm-12">
			<div id="buttons_wrapper">
				<button type="submit" name="_target0" class="btn btn-default"><fmt:message key="back" /></button>
				<button type="submit" name="_target2" class="btn btn-default"><fmt:message key="next" /></button>
			</div>
		</div>
	</div>
  </div>		
  </form:form>	
</div>




<c:out value="${giftCardLayout.footerHtml}" escapeXml="false"/>


<script language="JavaScript">
<!--

function toggleStateProvince(el) {
  if (el.value == "US") {
      document.getElementById('state').disabled=false;
      document.getElementById('state').style.display="block";
      document.getElementById('ca_province').disabled=true;
      document.getElementById('ca_province').style.display="none";
      document.getElementById('province').disabled=true;
      document.getElementById('province').style.display="none";
      document.getElementById('addressStateProvinceNA').disabled=true;
      document.getElementById('stateProvinceNA').style.display="none";
  } else if (el.value == "CA") {
      document.getElementById('state').disabled=true;
      document.getElementById('state').style.display="none";
      document.getElementById('ca_province').disabled=false;
      document.getElementById('ca_province').style.display="block";
      document.getElementById('province').disabled=true;
      document.getElementById('province').style.display="none";
      document.getElementById('addressStateProvinceNA').disabled=true;
      document.getElementById('stateProvinceNA').style.display="none";
  } else {
      document.getElementById('state').disabled=true;
      document.getElementById('state').style.display="none";
      document.getElementById('ca_province').disabled=true;
      document.getElementById('ca_province').style.display="none";
      document.getElementById('province').disabled=false;
      document.getElementById('province').style.display="block";
      document.getElementById('addressStateProvinceNA').disabled=false;
      document.getElementById('stateProvinceNA').style.display="block";
  }
}
toggleStateProvince(document.getElementById('country'));
//-->
</script>

  </tiles:putAttribute>
</tiles:insertDefinition>