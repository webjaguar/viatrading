<%@ page session="false" %>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="/WEB-INF/tlds/spring.tld" prefix="spring" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>

<tiles:insertDefinition name="${_template}" flush="true">
  <c:if test="${_leftBar != '1' and _leftBar != '4'}">
    <tiles:putAttribute name="leftbar" value="/WEB-INF/jsp/frontend/layout/template${_leftBar}/leftbar.jsp" />
  </c:if>
  <tiles:putAttribute name="content" type="string">
<c:if test="${!gSiteConfig['gREGISTRATION_DISABLED']}">

<c:out value="${model.createAccount.headerHtml}" escapeXml="false"/>

<spring:hasBindErrors name="customerForm">
<span class="error">Please fix all errors!</span>
</spring:hasBindErrors>

<div class="col-sm-12">
  <div class="message"> <c:out value="${model.error}"></c:out> </div>
  <form:form commandName="customerForm" id="register_newCustomer_form" cssClass="form-horizontal" role="form" action="${_contextpath}/register.jhtm" method="post" enctype="multipart/form-data">
  <input type="hidden" name="_page" value="0">
  <form:hidden path="forwardAction" />
  
  <div class="row">
	  <div class="col-sm-12">
		<div class="requiredFieldLabel">* Required Field</div>
	  </div>
	</div>
	<div id="customerEmailAndPassword">
	  <div class="row">
		<div class="col-sm-6">
		  <div class="form-group">
			<div class="col-sm-12">
			  <h3><fmt:message key="emailAddressAndPassword" /></h3>
			</div>
		  </div>
		  <spring:bind path="customer.username">
		  <div class="form-group <c:if test="${status.error}">has-feedback has-error</c:if>">
			<div class="col-sm-4">
			  <label for="customer_username" class="control-label">
				<fmt:message key="emailAddress" /> <sup class="requiredField">*</sup>
			  </label>
			</div>
		    <div class="col-sm-8">
			  	<form:input autocomplete="off" path="customer.username" htmlEscape="true" id="customer_username" cssClass="form-control"/>
	  		  	<c:if test="${status.error}">
	  		  		<i class="form-control-feedback glyphicon glyphicon-remove"></i>
            		<small class="help-block error-message">
            			<form:errors path="customer.username" cssClass="error" />
            		</small>    
	  		  	</c:if>
	  		</div>
		  </div>
		  </spring:bind>
		  <c:if test="${siteConfig['CONFIRM_USERNAME'].value == 'true'}">
		  <spring:bind path="confirmUsername">
		  <div class="form-group <c:if test="${status.error}">has-feedback has-error</c:if>">
			<div class="col-sm-4">
			  <label for="customer_username" class="control-label">
				<fmt:message key="confirmEmailAddress" /> <sup class="requiredField">*</sup>
			  </label>
			</div>
		    <div class="col-sm-8">
			  <form:input autocomplete="off" path="confirmUsername" htmlEscape="true" id="customer_confirmUsername" cssClass="form-control"/>
	  		  <c:if test="${status.error}">
	  		  	<i class="form-control-feedback glyphicon glyphicon-remove"></i>
            	<small class="help-block error-message">
            		<form:errors path="confirmUsername" cssClass="error" />
            	</small>    
	  		  </c:if>
    		</div>
		  </div>
		  </spring:bind>
		  </c:if>
  
		  <spring:bind path="customer.password">
		  <div class="form-group <c:if test="${status.error}">has-feedback has-error</c:if>">
		  <div class="col-sm-4">
			  <label for="customer_password" class="control-label">
				<fmt:message key="password" /> <sup class="requiredField">*</sup>
			  </label>
			</div>
		    <div class="col-sm-8">
			  <form:password path="customer.password" htmlEscape="true" id="customer_password" cssClass="form-control"/>
	  		  <c:if test="${status.error}">
	  		  	<i class="form-control-feedback glyphicon glyphicon-remove"></i>
            	<small class="help-block error-message">
		  		  <form:errors path="customer.password" cssClass="error" />
	        	</small>    
	  		  </c:if>
			</div>
		  </div>
		  </spring:bind>
		  
		  <spring:bind path="confirmPassword">
		  <div class="form-group <c:if test="${status.error}">has-feedback has-error</c:if>">
		  <div class="col-sm-4">
			  <label for="confirm_password" class="control-label">
				<fmt:message key="confirmPassword" /> <sup class="requiredField">*</sup>
			  </label>
			</div>
			<div class="col-sm-8">
			  <form:password path="confirmPassword" htmlEscape="true" id="confirm_password" cssClass="form-control"/>
	  		  <c:if test="${status.error}">
	  		  	<i class="form-control-feedback glyphicon glyphicon-remove"></i>
            	<small class="help-block error-message">
		  		  <form:errors path="confirmPassword" cssClass="error" />    
	        	</small>    
	  		  </c:if>
			</div>
		  </div>
		  </spring:bind>
		</div>
		
		<div class="col-sm-6">
		  <div id="customerEmailAndPassword_right_wrapper">
			<img src="${_contextpath}/assets/template6/img/register/customerEmailAndPassword_image.png" class="img-responsive center-block">
		  </div>
		</div>
	  </div>
	</div>
	<div id="customerInformation">
	  <div class="row">
		<div class="col-sm-6">
		  <div class="form-group">
			<div class="col-sm-12">
			  <h3>Your Information</h3>
			</div>
		  </div>
		  
		  <spring:bind path="customer.address.firstName">
		  <div class="form-group <c:if test="${status.error}">has-feedback has-error</c:if>">
		  <div class="col-sm-4">
			  <label for="customer_address_firstName" class="control-label">
				<fmt:message key="firstName" /> <sup class="requiredField">*</sup>
			  </label>
			</div>
			<div class="col-sm-8">
			  <form:input autocomplete="off" path="customer.address.firstName" id="customer_address_firstName" htmlEscape="true" cssClass="form-control"/>
	  		  <c:if test="${status.error}">
	  		  	<i class="form-control-feedback glyphicon glyphicon-remove"></i>
            	<small class="help-block error-message">
		  		  <form:errors path="customer.address.firstName" cssClass="error" />    
	        	</small>    
	  		  </c:if>
			</div>
		  </div>
		  </spring:bind>
		  
		  <spring:bind path="customer.address.lastName">
		  <div class="form-group <c:if test="${status.error}">has-feedback has-error</c:if>">
		  <div class="col-sm-4">
			  <label for="customer_address_lastName" class="control-label">
				<fmt:message key="lastName" /> <sup class="requiredField">*</sup>
			  </label>
			</div>
		    <div class="col-sm-8">
			  <form:input autocomplete="off" path="customer.address.lastName" id="customer_address_lastName" htmlEscape="true" cssClass="form-control"/>
	  		  <c:if test="${status.error}">
	  		  	<i class="form-control-feedback glyphicon glyphicon-remove"></i>
            	<small class="help-block error-message">
		  		  <form:errors path="customer.address.lastName" cssClass="error" />    
	        	</small>    
	  		  </c:if>
			</div>
		  </div>
		  </spring:bind>
		  
		  
		  <spring:bind path="customer.address.country">
		  <div class="form-group <c:if test="${status.error}">has-feedback has-error</c:if>">
		  <div class="col-sm-4">
			  <label for="customer_address_country" class="control-label">
				<fmt:message key="country" /> <sup class="requiredField">*</sup>
			  </label>
			</div>
			<div class="col-sm-8">
			  <form:select id="customer_address_country" class="form-control" path="customer.address.country">
		        <form:option value="" label="Please Select"/>
		        <form:options items="${model.countrylist}" itemValue="code" itemLabel="name"/>
		      </form:select>
	  		  <c:if test="${status.error}">
	  		  	<i class="form-control-feedback glyphicon glyphicon-remove"></i>
            	<small class="help-block error-message">
			      <form:errors path="customer.address.country" cssClass="error" />      
	        	</small>    
	  		  </c:if>
		    </div>
		  </div>
		  </spring:bind>
		  
		  <spring:bind path="customer.address.company">
		  <div class="form-group <c:if test="${status.error}">has-feedback has-error</c:if>">
		  <div class="col-sm-4">
			  <label for="customer_address_company" class="control-label">
				<fmt:message key="companyName" /> <c:if test="${siteConfig['CUSTOMERS_REQUIRED_COMPANY'].value == 'true'}"> <sup class="requiredField">*</sup> </c:if>
			  </label>
			</div>
			<div class="col-sm-8">
			  <form:input autocomplete="off" path="customer.address.company" id="customer_address_company"  htmlEscape="true" cssClass="form-control"/>
	  		  <c:if test="${status.error}">
	  		  	<i class="form-control-feedback glyphicon glyphicon-remove"></i>
            	<small class="help-block error-message">
		  		  <form:errors path="customer.address.company" cssClass="error" />    
	        	</small>    
	  		  </c:if>
			</div>
		  </div>
		  </spring:bind>
		  
		  <spring:bind path="customer.address.addr1">
		  <div class="form-group <c:if test="${status.error}">has-feedback has-error</c:if>">
		  <div class="col-sm-4">
			  <label for="customer_address_addr1" class="control-label">
				<fmt:message key="address" /> 1 <sup class="requiredField">*</sup>
			  </label>
			</div>
			<div class="col-sm-8">
			  <form:input autocomplete="off" path="customer.address.addr1" id="customer_address_addr1" htmlEscape="true" cssClass="form-control"/>
	  		  <c:if test="${status.error}">
	  		  	<i class="form-control-feedback glyphicon glyphicon-remove"></i>
            	<small class="help-block error-message">
		  		  <form:errors path="customer.address.addr1" cssClass="error" />    
	        	</small>    
	  		  </c:if>
			</div>
		  </div>
		  </spring:bind>
		  
		  <spring:bind path="customer.address.addr2">
		  <div class="form-group <c:if test="${status.error}">has-feedback has-error</c:if>">
		  <div class="col-sm-4">
			  <label for="customer_address_addr2" class="control-label">
				<fmt:message key="address" /> 2
			  </label>
			</div>
			<div class="col-sm-8">
			  <form:input autocomplete="off" path="customer.address.addr2" id="customer_address_addr2" htmlEscape="true" cssClass="form-control"/>
	  		  <c:if test="${status.error}">
	  		  	<i class="form-control-feedback glyphicon glyphicon-remove"></i>
            	<small class="help-block error-message">
		  		  <form:errors path="customer.address.addr2" cssClass="error" />    
	        	</small>    
	  		  </c:if>
			</div>
		  </div>
		  </spring:bind>
		  
		  <spring:bind path="customer.address.city">
		  <div class="form-group <c:if test="${status.error}">has-feedback has-error</c:if>">
		  <div class="col-sm-4">
			  <label for="customer_address_city" class="control-label">
				<fmt:message key="city" /> <sup class="requiredField">*</sup>
			  </label>
			</div>
		    <div class="col-sm-8">
			  <form:input autocomplete="off" path="customer.address.city" id="customer_address_city"  htmlEscape="true" cssClass="form-control"/>
	  		  <c:if test="${status.error}">
	  		  	<i class="form-control-feedback glyphicon glyphicon-remove"></i>
            	<small class="help-block error-message">
		  		  <form:errors path="customer.address.city" cssClass="error" />    
	        	</small>    
	  		  </c:if>
			</div>
		  </div>
		  </spring:bind>
		  
		  <spring:bind path="customer.address.stateProvince">
		  <div class="form-group <c:if test="${status.error}">has-feedback has-error</c:if>">
		  <div class="col-sm-4">
			  <label for="" class="control-label">
				<fmt:message key="stateProvince" /> <sup class="requiredField">*</sup>
			  </label>
			</div>
			<div class="col-sm-8">
			  <form:select id="customer_address_state" class="form-control" path="customer.address.stateProvince" disabled="${customerForm.customer.address.country != 'US'}">
	            <form:option value="" label="Please Select"/>
	            <form:options items="${model.statelist}" itemValue="code" itemLabel="name"/>
	          </form:select>
	          <form:select id="customer_address_ca_province" path="customer.address.stateProvince" disabled="${customerForm.customer.address.country != 'CA'}">
	            <form:option value="" label="Please Select"/>
	            <form:options items="${model.caProvinceList}" itemValue="code" itemLabel="name"/>
	          </form:select>
	          <form:input autocomplete="off" id="customer_address_stateProvince" class="form-control" path="customer.address.stateProvince" htmlEscape="true" disabled="${customerForm.customer.address.country == 'US' or customerForm.customer.address.country == 'CA' or customerForm.customer.address.country == '' or customerForm.customer.address.country == null}"/>
	          <div id="stateProvinceNA" class="checkbox">
				<label>
				  <form:checkbox path="customer.address.stateProvinceNA" id="customer_address_stateProvinceNA" value="true"  disabled="${customerForm.customer.address.country == 'US' or customerForm.customer.address.country == 'CA' or customerForm.customer.address.country == '' or customerForm.customer.address.country == null}"/><fmt:message key="notApplicable"/>
       		    </label>
			  </div>
	  		  <c:if test="${status.error}">
	  		  	<i class="form-control-feedback glyphicon glyphicon-remove"></i>
            	<small class="help-block error-message">
				  <form:errors path="customer.address.stateProvince" cssClass="error" />
	        	</small>    
	  		  </c:if>
       	  </div>
		</div>
		</spring:bind>
		
		<c:if test="${gSiteConfig['gWILDMAN_GROUP'] and !empty customerForm.accountLocationMap}">
   		  <spring:bind path="customer.accountNumber">
		  <div class="form-group <c:if test="${status.error}">has-feedback has-error</c:if>">
		  <div class="col-sm-4">
			  <label for="customer_address_addr2" class="control-label">
				<fmt:message key="location" /> <sup class="requiredField">*</sup>
			  </label>
			</div>
			<div class="col-sm-8">
			  <form:select path="customer.accountNumber">
	            <form:option value="" label="Please Select"/>
	            <c:forEach items="${customerForm.accountLocationMap}" var="accountLocation">         	
	          	  <option value="${accountLocation.value}" <c:if test="${customerForm.customer.accountNumber == accountLocation.value}">selected</c:if>>${accountLocation.key}</option>
	        	</c:forEach>
	          </form:select>
			</div>
		  </div>
		  </spring:bind>
 	    </c:if>
		
		
		<spring:bind path="customer.address.zip">
		<div class="form-group <c:if test="${status.error}">has-feedback has-error</c:if>">
		  <div class="col-sm-4">
			<label for="customer_address_zip" class="control-label">
			  <fmt:message key="zipCode" /> <sup class="requiredField">*</sup>
			</label>
		  </div>
		  <div class="col-sm-8">
			<form:input autocomplete="off" id="customer_address_zip" path="customer.address.zip" htmlEscape="true" cssClass="form-control"/>
	  		  <c:if test="${status.error}">
	  		  	<i class="form-control-feedback glyphicon glyphicon-remove"></i>
            	<small class="help-block error-message">
		    	  <form:errors path="customer.address.zip" cssClass="error" />    
				</small>    
	  		  </c:if>
    	  </div>
		</div>
		</spring:bind>
		
		<c:if test="${siteConfig['ADDRESS_RESIDENTIAL_COMMERCIAL'].value > 1}">
		<spring:bind path="customer.address.residential">
		<div class="form-group <c:if test="${status.error}">has-feedback has-error</c:if>">
		  <div class="col-sm-4">
			<label for="customer_address_zip" class="control-label">
			  <fmt:message key="deliveryType" />
			</label>
		  </div>
		  <div class="col-sm-8">
			<form:radiobutton path="customer.address.residential" value="true"/>: <fmt:message key="residential" /><br />
    		<form:radiobutton path="customer.address.residential" value="false"/>: <fmt:message key="commercial" /> 
    	  </div>
		</div>
		</spring:bind>
		
	    <c:if test="${siteConfig['SHIPPING_LTL'].value == 'true'}">
	      <spring:bind path="customer.address.liftGate">
		  <div class="form-group <c:if test="${status.error}">has-feedback has-error</c:if>">
		    <div class="col-sm-4">
			  <label for="customer_address_zip" class="control-label">
			    <fmt:message key="liftGateDelivery" />
			  </label>
		    </div>
		    <div class="col-sm-8">
			  <form:checkbox path="customer.address.liftGate" value="true"/><a href="${_contextpath}/category.jhtm?cid=307" onclick="window.open(this.href,'','width=600,height=300,resizable=yes'); return false;"><img class="toolTipImg" src="${_contextpath}/assets/Image/Layout/question.gif" border="0" /></a> 
	        </div>
		  </div>
		  </spring:bind>
		</c:if>
  		</c:if>
  
		<spring:bind path="customer.address.phone">
		<div class="form-group <c:if test="${status.error}">has-feedback has-error</c:if>">
		  <div class="col-sm-4">
			<label for="customer_address_phone" class="control-label">
			  <fmt:message key="phone" /> <sup class="requiredField">*</sup>
			</label>
		  </div>
		  <div class="col-sm-8">
			<form:input autocomplete="off" path="customer.address.phone" id="customer_address_phone" htmlEscape="true" cssClass="form-control"/>
	  		  <c:if test="${status.error}">
	  		  	<i class="form-control-feedback glyphicon glyphicon-remove"></i>
            	<small class="help-block error-message">
			  		<form:errors path="customer.address.phone" cssClass="error" />    
				</small>    
	  		  </c:if>
    	  </div>
		</div>
		</spring:bind>
		
		<spring:bind path="customer.address.cellPhone">
		<div class="form-group <c:if test="${status.error}">has-feedback has-error</c:if>">
		  <div class="col-sm-4">
			<label for="customer_address_cellPhone" class="control-label">
			  <fmt:message key="cellPhone" />
			</label>
		  </div>
		  <div class="col-sm-8">
			<form:input autocomplete="off" id="customer_address_cellPhone" class="form-control" path="customer.address.cellPhone" htmlEscape="true"/>
	  		  <c:if test="${status.error}">
	  		  	<i class="form-control-feedback glyphicon glyphicon-remove"></i>
            	<small class="help-block error-message">
			  		<form:errors path="customer.address.cellPhone" cssClass="error" />    
				</small>    
	  		  </c:if>
    	  </div>
		</div>
		</spring:bind>
		
		<spring:bind path="customer.address.fax">
		<div class="form-group <c:if test="${status.error}">has-feedback has-error</c:if>">
		  <div class="col-sm-4">
			<label for="customer_address_fax" class="control-label">
			  <fmt:message key="fax" />
			</label>
		  </div>
		  <div class="col-sm-8">
			<form:input autocomplete="off" path="customer.address.fax" id="customer_address_fax" class="form-control" htmlEscape="true"/>
	  		  <c:if test="${status.error}">
	  		  	<i class="form-control-feedback glyphicon glyphicon-remove"></i>
            	<small class="help-block error-message">
			  		<form:errors path="customer.address.fax" cssClass="error" />    
				</small>    
	  		  </c:if>
    	  </div>
		</div>
		</spring:bind>
		
		<c:if test="${gSiteConfig['gI18N'] == '1'}">
		<spring:bind path="customer.languageCode">
		<div class="form-group <c:if test="${status.error}">has-feedback has-error</c:if>">
		  <div class="col-sm-4">
			<label for="customer_address_fax" class="control-label">
			  <fmt:message key="language" />
			</label>
		  </div>
		  <div class="col-sm-8">
		    <form:select path="customer.languageCode">
		      <form:option value="en"><fmt:message key="language_en"/></form:option>
		      <c:forEach items="${model.languageCodes}" var="i18n">         	
		       	<option value="${i18n.languageCode}" <c:if test="${customerForm.customer.languageCode == i18n.languageCode}">selected</c:if>><fmt:message key="language_${i18n.languageCode}"/></option>
		      </c:forEach>
			</form:select>
		  </div>
		</div>
		</spring:bind>
	    </c:if>
  		
  		<c:if test="${gSiteConfig['gMASS_EMAIL'] or siteConfig['CUSTOMER_MASS_EMAIL'].value == 'true'}">
	  	<spring:bind path="subscribeEmail">
		<div class="form-group <c:if test="${status.error}">has-feedback has-error</c:if>">
		  <div class="col-sm-12">
			<div id="subscribeCheckBoxId" class="checkbox">
			  <label><form:checkbox path="subscribeEmail"  />
			    <fmt:message key="f_subscribeMailingList" />
	     	  </label>
		    </div>
		  </div>
		</div>
	    </spring:bind>
	    </c:if>
	    
	    <c:if test="${customerForm.customerFields != null and siteConfig['CUSTOMER_FIELDS_ON_STEP1_REGISTRATION'].value == 'true'}">
          <c:forEach items="${customerForm.customerFields}" var="customerField">
            <spring:bind path="customer.field${customerField.id}">
		    <div class="form-group <c:if test="${status.error}">has-feedback has-error</c:if>">
		    <div class="col-sm-4">
				<label for="customer_address_fax" class="control-label">
				  <c:out value="${customerField.name}"/> <c:if test="${customerField.required}" ><div class="requiredField" align="right"> * </div></c:if>
  				</label>
			  </div>
			  <div class="col-sm-8">
			    <c:choose>
			      <c:when test="${!empty customerField.preValue}">
			        <form:select path="customer.field${customerField.id}" cssClass="form-control">
				      <form:option value="" label="Please Select"/>
			          <c:forTokens items="${customerField.preValue}" delims="," var="dropDownValue">
			            <form:option value="${dropDownValue}" />
			          </c:forTokens>
			        </form:select>
			      </c:when>
			      <c:otherwise>
			        <form:input autocomplete="off" path="customer.field${customerField.id}"  htmlEscape="true" maxlength="255" size="20" cssClass="form-control"/> 
			      </c:otherwise>
			    </c:choose>
		  		<c:if test="${status.error}">
		  		  	<i class="form-control-feedback glyphicon glyphicon-remove"></i>
	            	<small class="help-block error-message">
					    <form:errors path="customer.field${customerField.id}" cssClass="error" />
					</small>    
		  		</c:if>
    		  </div>
			</div>
			</spring:bind>
		  </c:forEach>
		</c:if>
	    
	    <c:if test="${siteConfig['NOTE_ON_REGISTRATION'].value == 'true'}">
		  <spring:bind path="customer.note">
		  <div class="form-group <c:if test="${status.error}">has-feedback has-error</c:if>">
		    <div class="col-sm-4">
			  <label for="customer_note" class="control-label">
			    <fmt:message key="note" />
			  </label>
		    </div>
		    <div class="col-sm-8">
		      <form:textarea path="customer.note" htmlEscape="true" cssClass="registrationNote"/>
		  		<c:if test="${status.error}">
		  		  	<i class="form-control-feedback glyphicon glyphicon-remove"></i>
	            	<small class="help-block error-message">
 					  <form:errors path="customer.note" cssClass="error" />  
					</small>    
		  		</c:if>
			  <div id="noteOnRegistration" class="note" style="color:#FF0000"><fmt:message key="noteOnRegistrationMessage" /></div>
		    </div>
		  </div>
		  </spring:bind>
		</c:if>
	    
	    
	  </div>
	    <div class="col-sm-6">
		  <div id="customerInformation_right_wrapper">
			<img src="${_contextpath}/assets/template6/img/register/customerInformation_image.png" class="img-responsive center-block">
		  </div>
		</div>
	
	  </div>
	  
	 </div> 
	 
	 
	 <c:if test="${gSiteConfig['gTAX_EXEMPTION']}">
	    <div id="taxId">
		  <div class="row">
		    <div class="col-sm-12">
			  <div class="form-group">
			    <div class="col-sm-12">
				  <h3>Tax Information</h3>
			    </div>
			  </div>
			  <spring:bind path="customer.taxId">
		  	  <div class="form-group <c:if test="${status.error}">has-feedback has-error</c:if>">
			    <div class="col-sm-3">
				  <label for="customer_taxId" class="control-label">
				    <fmt:message key="f_taxId" /> <c:if test="${siteConfig['TAX_ID_REQUIRED'].value == 'true'}">*</c:if> :
				  </label>
			    </div>
			    <div class="col-sm-3">
				  <form:input autocomplete="off" path="customer.taxId" cssClass="form-control" id="customer_taxId" maxlength="30" htmlEscape="true"/>
		  		<c:if test="${status.error}">
		  		  	<i class="form-control-feedback glyphicon glyphicon-remove"></i>
	            	<small class="help-block error-message">
		  			  <form:errors path="customer.taxId" cssClass="error" />      
 					</small>    
		  		</c:if>
			    </div>
			  </div>
			  </spring:bind>
			  
			  <div class="form-group">
			    <div class="col-sm-12">
				  <span class="taxIdNote"><c:out value="${siteConfig['TAX_ID_NOTE'].value}" /></span>
			    </div>
			  </div>
		    </div>
		    <div class="col-sm-6"></div>
		  </div>
	    </div>
		</c:if>
	  
	  
	  <div id="form_buttons">
		<div class="row">
		  <div class="col-sm-12">
			<div id="buttons_wrapper">
			<c:choose>
			  <c:when test="${customerForm.customerFields != null and siteConfig['CUSTOMER_FIELDS_ON_STEP1_REGISTRATION'].value != 'true'}">
			    <button type="submit"name="_target1" class="btn btn-default" ><fmt:message key="nextStep" /></button>
			  </c:when>
			  <c:otherwise>
			    <button type="submit" name="_target2" class="btn btn-default"><fmt:message key="nextStep" /></button>
			  </c:otherwise>
			</c:choose>
			</div>
		  </div>
		</div>
	  </div>
	</form:form>
  </div>
</div>

<c:out value="${model.createAccount.footerHtml}" escapeXml="false"/>

</c:if>
  </tiles:putAttribute>
</tiles:insertDefinition>