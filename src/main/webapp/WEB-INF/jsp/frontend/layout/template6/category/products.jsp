<%@ page session="false" %>
<%@ page import="java.util.*" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ taglib uri="/WEB-INF/tlds/custom.tld" prefix="wj" %>
<%@ page session="false" %>

<link rel="stylesheet" type="text/css" href="/assets/css/rightShoppingCart.css">
<link rel="stylesheet" type="text/css" href="/assets/plugins/owl-carousel/css/owl.carousel.css">
<link rel="stylesheet" type="text/css" href="/assets/plugins/owl-carousel/css/owl.theme.default.css">
<link rel="stylesheet" type="text/css" href="/assets/plugins/owl-carousel/css/animate.css">
<link rel="stylesheet" type="text/css" href="/assets/css/slider-theme.css">
<link rel="stylesheet" type="text/css" href="/assets/css/carousel-theme.css">
<link rel="stylesheet" type="text/css" href="/assets/css/sc-carousel-theme.css">
<link rel="stylesheet" type="text/css" href="/assets/css/categories-carousel-theme.css">
<script type="text/javascript" src="/assets/plugins/owl-carousel/js/owl.carousel.js"></script>

<style>
.product_sku_wrapper {
    top: 0 !important;
}
.product_wrapper {
    margin-bottom: 15px !important;
}
</style>

<div class="row">
  <div class="col-sm-12">
	<div class="pageNavigation">
	  <form name="pagination" id="paginationFormId" class="formPageNavigation clearfix" action="${_contextpath}/<c:choose><c:when test="${gSiteConfig['gMOD_REWRITE'] == '1'}">${siteConfig['MOD_REWRITE_CATEGORY'].value}/${model.thisCategory.id}/${model.thisCategory.encodedName}.html</c:when><c:otherwise>category.jhtm?</c:otherwise></c:choose>">
	  	<c:if test="${gSiteConfig['gMOD_REWRITE'] != '1'}">
    	  <input type="hidden" name="cid" value="${model.thisCategory.id}">
    	</c:if>
      	<div class="pageShowing">
		  <span>Showing ${frontEndProductSearch.offset + 1}-${model.pageEnd} of ${model.count}</span>
		</div>
		<div class="pageNavigationControl">
		  <c:if test="${siteConfig['PRODUCT_SORT_BY_FRONTEND'].value == 'true'}">
		  <div class="sortby">
			<span><fmt:message key="f_sortby"/></span>
			<select class="selectpicker" name="sort" onchange="submit();">
			  <option value="">Please Select</option>
			  <option value="10" <c:if test="${frontEndProductSearch.sort == 'name'}">selected="selected"</c:if>><fmt:message key="Name"/></option>
		      <option value="20" <c:if test="${frontEndProductSearch.sort == 'price_1 ASC'}">selected="selected"</c:if>><fmt:message key="price"/> (<fmt:message key="${siteConfig['CURRENCY'].value}" /> <fmt:message key="to"/> <fmt:message key="${siteConfig['CURRENCY'].value}" /><fmt:message key="${siteConfig['CURRENCY'].value}" />)</option>
		      <option value="30" <c:if test="${frontEndProductSearch.sort == 'price_1 DESC'}">selected="selected"</c:if>><fmt:message key="price"/> (<fmt:message key="${siteConfig['CURRENCY'].value}" /><fmt:message key="${siteConfig['CURRENCY'].value}" /> <fmt:message key="to"/> <fmt:message key="${siteConfig['CURRENCY'].value}" />)</option>
		      <c:if test="${siteConfig['PRODUCT_RATE'].value =='true' or gSiteConfig['gPRODUCT_REVIEW']}">
		        <option value="40" <c:if test="${frontEndProductSearch.sort == 'rate DESC'}">selected="selected"</c:if>>Rating</option>
		      </c:if>
			</select>
		  </div>
		  </c:if>
		  <div class="pagesize">
			<span><fmt:message key="f_pageSize"/></span>
			<select name="size" class="selectpicker" onchange="document.getElementById('page').value=1;submit();">   
			  <c:forEach items="${model.productPerPageList}"  var="current">
				<option value="${current}" <c:if test= "${current == frontEndProductSearch.pageSize}">selected</c:if>>${current} <fmt:message key="perPage" /></option>
			  </c:forEach>
			</select>
		  </div>
		  <c:if test="${model.pageCount > 1}">
		  <div class="page">
			<span><fmt:message key="page"/></span>
			<c:choose>
		      <c:when test="${model.pageCount <= 100 and model.pageCount > 0}">
		        
		        <select class="selectpicker" name="page" id="page" onchange="submit()">
				  <c:forEach begin="1" end="${model.pageCount}" var="page">
		  	        <option value="${page}" <c:if test="${page == (frontEndProductSearch.page)}">selected</c:if>>${page}</option>
		          </c:forEach>
				</select>
				<span>of ${model.pageCount}</span>
		      </c:when>
		      <c:otherwise>
		        <input type="text" id="page" name="page" value="${frontEndProductSearch.page}" size="5" class="textfield50" />
		        <input type="submit" value="go"/>
		      </c:otherwise>
		    </c:choose>
		  </div>
		  <div class="pageNav">
			<span class="pageNavLink">
			  <c:if test="${frontEndProductSearch.page == 1}"><a href="#"><span class="pageNavPrev"><i class="fa fa-angle-double-left fa-fw"></i></span></a></c:if>
			  <c:if test="${frontEndProductSearch.page != 1}"><a href="${_contextpath}/<c:choose><c:when test="${gSiteConfig['gMOD_REWRITE'] == '1'}">${siteConfig['MOD_REWRITE_CATEGORY'].value}/${model.thisCategory.id}/${model.thisCategory.encodedName}.html?page=${frontEndProductSearch.page-1}&sortBy=${model.sortBy}</c:when><c:otherwise><c:url value="category.jhtm"><c:param name="page" value="${frontEndProductSearch.page-1}"/><c:param name="cid" value="${model.thisCategory.id}"/><c:param name="view" value="${paramView}"/><c:param name="sortBy" value="${model.sortBy}"/></c:url></c:otherwise></c:choose>" class="pageNaviLink"><span class="pageNavPrev"><i class="fa fa-angle-double-left fa-fw"></i></span></a></span></c:if>
    		</span>
			<span class="pageNavLink">
			  <c:if test="${frontEndProductSearch.page == model.pageCount}"><a href="#"><span class="pageNavNext"><i class="fa fa-angle-double-right fa-fw"></i></span></a></c:if>
			  <c:if test="${frontEndProductSearch.page != model.pageCount}"><span class="pageNavLink"><a href="${_contextpath}/<c:choose><c:when test="${gSiteConfig['gMOD_REWRITE'] == '1'}">${siteConfig['MOD_REWRITE_CATEGORY'].value}/${model.thisCategory.id}/${model.thisCategory.encodedName}.html?page=${frontEndProductSearch.page+1}&sortBy=${model.sortBy}</c:when><c:otherwise><c:url value="category.jhtm"><c:param name="page" value="${frontEndProductSearch.page-1}"/><c:param name="cid" value="${model.thisCategory.id}"/><c:param name="view" value="${paramView}"/><c:param name="sortBy" value="${model.sortBy}"/></c:url></c:otherwise></c:choose>"><span class="pageNavNext"><i class="fa fa-angle-double-right fa-fw"></i></span></a></span></c:if>
			</span>
		  </div>
		  </c:if>
		   <c:if test="${model.pageCount <= 1}">
               <input type="hidden" id="page" name="page" value="${frontEndProductSearch.page}" size="5" class="textfield50" />
           </c:if>
		</div>
	  </form>
	</div>
  </div>
</div>



<div class="row">
  <div class="col-sm-12">
	
  </div>
</div>


<div class="row">
<div class="col-sm-7 col-md-8 col-lg-9">
<div class="products_wrapper">
<div class="row">

  <c:choose>
  	  <c:when test="${(paramView == '22') or ( empty paramView and ((model.thisCategory.displayMode == '22') or (empty model.thisCategory.displayMode and siteConfig['CATEGORY_DISPLAY_MODE'].value == '22')))}">
	  </c:when>
	  <c:otherwise>
	 
	  												
		<c:forEach items="${model.products}" var="product" varStatus="status">
	  									
			<c:set var="productLink">${_contextpath}/product.jhtm?id=${product.id}<c:if test="${model.product == null or siteConfig['RECOMMENDED_LIST_SHOW_BREADCRUMB'].value == 'true'}">&cid=<c:out value="${param.cid}"/></c:if></c:set>
			<c:if test="${gSiteConfig['gMOD_REWRITE'] == '1' and not fn:contains(product.sku, '/')}">
		  	  <c:set var="productLink">${_contextpath}/${siteConfig['MOD_REWRITE_PRODUCT'].value}/${product.encodedSku}/${product.encodedName}.html<c:if test="${model.product == null or siteConfig['RECOMMENDED_LIST_SHOW_BREADCRUMB'].value == 'true'}">?cid=<c:out value="${param.cid}"/></c:if></c:set>
		    </c:if>
			<c:choose>
				<c:when test="${(paramView == '28-R') or ( empty paramView and ((model.thisCategory.displayMode == '28-R') or (empty model.thisCategory.displayMode and siteConfig['CATEGORY_DISPLAY_MODE'].value == '28-R')))}">
			      <%@ include file="/WEB-INF/jsp/frontend/layout/template6/thumbnail/view1.jsp" %>
			    </c:when> 
			    <c:otherwise>
			    
			    <c:choose>
				<c:when test="${!model.modifiedDirectory}">
               	 	<%@ include file="/WEB-INF/jsp/frontend/layout/template6/thumbnail/view2.jsp" %>
     			</c:when>
				<c:otherwise>
		        	<%@ include file="/WEB-INF/jsp/frontend/layout/template6/thumbnail/view2.jsp" %>
				</c:otherwise>
				</c:choose>

			    </c:otherwise> 
			</c:choose>
			
		</c:forEach>
		
		
			
	  </c:otherwise>
  </c:choose>
</div>
</div>
</div>

	
	
									<div class="col-sm-5 col-md-4 col-lg-3">
										
										<script type="text/javascript">
										$(window).load(function() {
											var rightShoppingCartBoxTop = 0;
											$(window).scroll(function() {
												if($(window).scrollTop() > $('.products_wrapper').offset().top) {
													if($(window).scrollTop() < ($('.products_wrapper').offset().top + $('.products_wrapper').height() - $('#rightShoppingCartBox').height())) {
														$('#rightShoppingCartBox').css('position','fixed');
														rightShoppingCartBoxTop = 0;
													}
													if($('.products_wrapper').height() > $('#rightShoppingCartBox').height()) {
														if($(window).scrollTop() > ($('.products_wrapper').offset().top + $('.products_wrapper').height() - $('#rightShoppingCartBox').height())) {
															$('#rightShoppingCartBox').css('position','absolute');
															rightShoppingCartBoxTop = $('.products_wrapper').height() - $('#rightShoppingCartBox').height();
														}
													}
												}
												if($(window).scrollTop() < $('.products_wrapper').offset().top) {
													$('#rightShoppingCartBox').css('position','relative');
													rightShoppingCartBoxTop = 0;
												}

												$('#rightShoppingCartBox').css('width', $('#rightShoppingCartBox_wrapper').width());
												$('#rightShoppingCartBox').css('top', rightShoppingCartBoxTop + 'px');
											}).resize(function(){
												$(window).trigger('scroll');
											}).trigger('scroll');
										});
										</script>
										
										<div id="rightShoppingCartBox_wrapper">
											<div id="rightShoppingCartBox">
												<div id="right_shopping_cart">
													
													<div class="clearfix" id="right_shopping_cart_header">
														<div class="sc_hdr_label"><fmt:message key="cartItems"/>
															<a href="${_contextpath}/viewCart.jhtm">
<c:choose>
 <c:when test="${(model.lang==null || model.lang!='es')}">
        <img src="https://www.viatrading.com/assets/icon/cart.png" style="width:2em; height:2em;position:relative;left:75px"/>
</c:when>
<c:otherwise>
        <img src="https://www.viatrading.com/assets/icon/cart.png" style="width:2em; height:2em;position:relative;left:100px"/>
</c:otherwise>
</c:choose>

<c:choose>
 <c:when test="${(model.lang==null || model.lang!='es')}">
        <span style="color:black;position:relative;left:70px"/>
</c:when>
<c:otherwise>
        <span style="color:black;position:relative;left:95px"/>
</c:otherwise>
</c:choose>
																<c:choose>
																<c:when test="${model.cart.quantity==null}">
																	0
																</c:when>
																<c:otherwise>
																	${model.cart.quantity}
																</c:otherwise>														
																</c:choose>	
																</span>
															</a>
														</div>
													</div>
													
										<div class="clearfix" id="right_shopping_cart_items">

                                                                                                        <c:forEach items="${model.cart.cartItems}" var="cartItem" varStatus="cartStatus">

                                                                                                           <c:set var="productLink2">${_contextpath}/product.jhtm?id=${cartItem.product.id}<c:if test="${model.cartItem.product == null or siteConfig['RECOMMENDED_LIST_SHOW_BREADCRUMB'].value == 'true'}">&cid=<c:out value="${param.cid}"/></c:if></c:set>
                                                                                                                <c:if test="${gSiteConfig['gMOD_REWRITE'] == '1' and not fn:contains(cartItem.product.sku, '/')}">
                                                                                                                        <c:set var="productLink2">${_contextpath}/${siteConfig['MOD_REWRITE_PRODUCT'].value}/${cartItem.product.encodedSku}/${cartItem.product.encodedName}.html<c:if test="${model.cartItem.product == null or siteConfig['RECOMMENDED_LIST_SHOW_BREADCRUMB'].value == 'true'}">?cid=<c:out value="${param.cid}"/></c:if></c:set>
                                                                                                                </c:if>

                                                                                                                <div class="product clearfix">                                                                                                                        <div class="product_sku">
                                                                                                                                ${cartItem.product.sku}
                                                                                                                        </div>
                                                                                                                        <div class="product_tableGroup">
                                                                                                                                <div class="product_image_cell">
                                                                                                                                        <div class="product_image">
<a href="${productLink2}"><img class="cartImage img-responsive" src="<c:if test="${!cartItem.product.thumbnail.absolute}">${_contextpath}/assets/Image/Product/thumb/</c:if>${cartItem.product.thumbnail.imageUrl}" border="0" alt="${cartItem.product.alt}"/></a>
                                                                                                                                        </div>
                                                                                                                                </div>
                                                                                                                                <div class="product_name_cell">
                                                                                                                                        <div class="product_name">
                                                                                                                                                <a href="${productLink2}">${cartItem.product.name}</a>
                                                                                                                                        </div>
                                                                                                                                        <div class="quantity_and_price">
                                                                                                                                                <span class="quantity">${cartItem.quantity}</span> x <span class="price"><fmt:message key="${siteConfig['CURRENCY'].value}" /><fmt:formatNumber value="${cartItem.unitPrice}" pattern="###,##0.00"/></span>
                                                                                                                                        </div>
                                                                                                                                </div>
                                                                                                                        </div>



                                                                                                                <div class="pull-left">
                                                                                                                <form id="addToCartForm" action="${_contextpath}/addToCart.jhtm" method="post" class="clearfix">
                                                                                                                        <input type="hidden" name="product.id" value="${cartItem.product.id}">
                                                                                                                        <input type="hidden" name="quantity_${cartItem.product.id}" value="1">
                                                                                                                        <input type="hidden" name="cid" value="${model.thisCategory.id}">
                                                                                                                        <input type="hidden" name="addToCartCategory" value="true">                                                                                                                        <span class="input-group-btn-vertical">
                                                                                                                                <button type="submit" name="increase" class="btn btn-default bootstrap-touchspin-up" style="padding-left:5px !important;padding-right:5px !important;padding-top:0px !important;padding-bottom:0px !important; margin-top:1px; margin-bottom:-2px;" type="button">+</button>
                                                                                                                                <button type="submit" name="decrease" class="btn btn-default bootstrap-touchspin-down" style="padding-left:6px !important;padding-right:6px !important;padding-top:0px !important;padding-bottom:0px !important; margin-top:1px; margin-bottom:-2px;" type="button">-</button>
                                                                                                                        </span>
                                                                                                                </form>
                                                                                                                </div>

                                                                                                                </div>




                                                                                                        </c:forEach>



                                                                                                        </div>
													
													
													
													<div class="clearfix" id="right_shopping_cart_footer">
														
														<div class="sc_ftr_info_box sc_subtotal clearfix">
															<div class="sc_ftr_info_label_cell">
																<div class="sc_ftr_info_label subtotal_label">
																	<fmt:message key="subtotal"/>
																</div>
															</div>
															<div class="sc_ftr_info_value_cell">
																<div class="sc_ftr_info_value subtotal_value">
																	<fmt:message key="${siteConfig['CURRENCY'].value}" /><fmt:formatNumber value="${model.subTotal}" pattern="###,##0.00"/>
																</div>
															</div>
														</div>
														
														<div class="sc_ftr_info_box sc_discount clearfix">
															<div class="sc_ftr_info_label_cell">
																<div class="sc_ftr_info_label discount_label">
																	<fmt:message key="discount"/>
																</div>
															</div>
															<div class="sc_ftr_info_value_cell">
																<div class="sc_ftr_info_value discount_value">
																	<fmt:message key="${siteConfig['CURRENCY'].value}" /><fmt:formatNumber value="${model.discount}" pattern="###,##0.00"/>
																</div>
															</div>
														</div>
														
														<div class="sc_ftr_info_box sc_tax clearfix">
															<div class="sc_ftr_info_label_cell">
																<div class="sc_ftr_info_label tax_label">
																	<fmt:message key="tax"/>
																</div>
															</div>
															<div class="sc_ftr_info_value_cell">
																<div class="sc_ftr_info_value tax_value">
																	<fmt:message key="${siteConfig['CURRENCY'].value}" /><fmt:formatNumber value="${model.tax}" pattern="###,##0.00"/>
																</div>
															</div>
														</div>
														
														
														<div class="sc_ftr_info_box sc_shippingAndHandling clearfix">
															<div class="sc_ftr_info_label_cell">
																<div class="sc_ftr_info_label shippingAndHandling_label">
																	<fmt:message key="shipping"/>
																</div>
															</div>
															<div class="sc_ftr_info_value_cell">
																<div id="shippingType" class="sc_ftr_info_value shippingAndHandling_value">
																	
																	<c:choose>	
																		<c:when test="${userSession != null && model.shipping == null}">
																			<a href='#'>Not Available</a>
																		</c:when>	
																		<c:when test="${model.shipping == null}">
																			<a href='${_contextpath}/login.jhtm'><fmt:message key="loginRegister"/></a>
																		</c:when>				
																		<c:otherwise>
																			${model.shipping}
																		</c:otherwise>
																	</c:choose>
																	
																</div>
															</div>
														</div>
														<div class="sc_ftr_info_box sc_shippingAndHandling clearfix">
															<div class="sc_ftr_info_label_cell">
																<div class="sc_ftr_info_label shippingAndHandling_label">
																	<fmt:message key="shippingCost"/>
																</div>
															</div>
															<div class="sc_ftr_info_value_cell">
																<div id="shippingCost" class="sc_ftr_info_value shippingAndHandling_value">
																
																	<c:choose>			
																		<c:when test="${model.totalCharges == null}">
																			$0.00
																		</c:when>				
																		<c:otherwise>
																			<fmt:message key="${siteConfig['CURRENCY'].value}" /><fmt:formatNumber value="${model.totalCharges}" pattern="###,##0.00"/>
																		</c:otherwise>
																	</c:choose>
	
																</div>
															</div>
														</div>
														
														<div class="sc_ftr_info_box sc_grandTotal clearfix">
															<div class="sc_ftr_info_label_cell">
																<div class="sc_ftr_info_label grandTotal_label">
																	<fmt:message key="grandTotal"/>:
																</div>
															</div>
															<div class="sc_ftr_info_value_cell">
																<div id="grandTotal" class="sc_ftr_info_value grandTotal_value">
															
																	<fmt:message key="${siteConfig['CURRENCY'].value}" /><fmt:formatNumber value="${model.grandTotal}" pattern="###,##0.00"/>
																			
																</div>
															</div>
														</div>
														
														<div class="buttons clearfix">
															<a href="${_contextpath}/viewCart.jhtm" class="btn viewCart_btn"><fmt:message key="viewCart"/></a>
															<a href="${_contextpath}/checkout1.jhtm" class="btn checkout_btn"><fmt:message key="checkout"/></a>
														</div>
													</div>
												</div>

												<div id="sc_relatedProducts">
													<div id="sc_relatedProducts_header">
														<div class="sc_hdr_label">
															<fmt:message key="relatedProducts"/>
														</div>
													</div>
													<div id="sc_relatedProducts_content">
														<script type="text/javascript">
															$(function(){
																$(".sc_relatedProducts_carouse").owlCarousel({
																	themeClass: 'sc-carousel-theme',
																	navText: [],
																	loop: true,
																	dots: false,
																	autoplay: true,
																	margin: 30,
																	autoplayTimeout: 5000,
																	items: 1,
																	slideBy: 1,
																	nav: true
																});
															});
														</script>
														
														
														<div class="sc_relatedProducts_carousel_wrapper">
															<div class="sc_relatedProducts_carouse owl-carousel">
																
																${model.sb}
																
	
															</div>
														</div>													
		
														
														
														
														
														
														
														
														
													</div>
												</div>
												
												
												
												
												
												
												
												
												
												
												
												
												
												
											</div>
										</div>
									</div>	
	
	
	
	
</div>	
	
	
	
<div class="row">
  <div class="col-sm-12">
	<div class="pageNavigation">
	  <form name="pagination" id="paginationFormId" class="formPageNavigation clearfix" action="${_contextpath}/<c:choose><c:when test="${gSiteConfig['gMOD_REWRITE'] == '1'}">${siteConfig['MOD_REWRITE_CATEGORY'].value}/${model.thisCategory.id}/${model.thisCategory.encodedName}.html</c:when><c:otherwise>category.jhtm?</c:otherwise></c:choose>">
	  	<c:if test="${gSiteConfig['gMOD_REWRITE'] != '1'}">
    	  <input type="hidden" name="cid" value="${model.thisCategory.id}">
    	</c:if>
      	<div class="pageShowing">
		  <span>Showing ${frontEndProductSearch.offset + 1}-${model.pageEnd} of ${model.count}</span>
		</div>
		<div class="pageNavigationControl">
		  <c:if test="${siteConfig['PRODUCT_SORT_BY_FRONTEND'].value == 'true'}">
		  <div class="sortby">
			<span><fmt:message key="f_sortby"/></span>
			<select class="selectpicker" name="sort" onchange="submit();">
			  <option value="">Please Select</option>
			  <option value="10" <c:if test="${frontEndProductSearch.sort == 'name'}">selected="selected"</c:if>><fmt:message key="Name"/></option>
		      <option value="20" <c:if test="${frontEndProductSearch.sort == 'price_1 ASC'}">selected="selected"</c:if>><fmt:message key="price"/> (<fmt:message key="${siteConfig['CURRENCY'].value}" /> <fmt:message key="to"/> <fmt:message key="${siteConfig['CURRENCY'].value}" /><fmt:message key="${siteConfig['CURRENCY'].value}" />)</option>
		      <option value="30" <c:if test="${frontEndProductSearch.sort == 'price_1 DESC'}">selected="selected"</c:if>><fmt:message key="price"/> (<fmt:message key="${siteConfig['CURRENCY'].value}" /><fmt:message key="${siteConfig['CURRENCY'].value}" /> <fmt:message key="to"/> <fmt:message key="${siteConfig['CURRENCY'].value}" />)</option>
		      <c:if test="${siteConfig['PRODUCT_RATE'].value =='true' or gSiteConfig['gPRODUCT_REVIEW']}">
		        <option value="40" <c:if test="${frontEndProductSearch.sort == 'rate DESC'}">selected="selected"</c:if>>Rating</option>
		      </c:if>
			</select>
		  </div>
		  </c:if>
		  <div class="pagesize">
			<span><fmt:message key="f_pageSize"/></span>
			<select name="size" class="selectpicker" onchange="document.getElementById('page').value=1;submit();">   
			  <c:forEach items="${model.productPerPageList}"  var="current">
				<option value="${current}" <c:if test= "${current == frontEndProductSearch.pageSize}">selected</c:if>>${current} <fmt:message key="perPage" /></option>
			  </c:forEach>
			</select>
		  </div>
		  <c:if test="${model.pageCount > 1}">
		  <div class="page">
			<span><fmt:message key="page"/></span>
			<c:choose>
		      <c:when test="${model.pageCount <= 100 and model.pageCount > 0}">
		        
		        <select class="selectpicker" name="page" id="page" onchange="submit()">
				  <c:forEach begin="1" end="${model.pageCount}" var="page">
		  	        <option value="${page}" <c:if test="${page == (frontEndProductSearch.page)}">selected</c:if>>${page}</option>
		          </c:forEach>
				</select>
				<span>of ${model.pageCount}</span>
		      </c:when>
		      <c:otherwise>
		        <input type="text" id="page" name="page" value="${frontEndProductSearch.page}" size="5" class="textfield50" />
		        <input type="submit" value="go"/>
		      </c:otherwise>
		    </c:choose>
		  </div>
		  <div class="pageNav">
			<span class="pageNavLink">
			  <c:if test="${frontEndProductSearch.page == 1}"><a href="#"><span class="pageNavPrev"><i class="fa fa-angle-double-left fa-fw"></i></span></a></c:if>
			  <c:if test="${frontEndProductSearch.page != 1}"><a href="${_contextpath}/<c:choose><c:when test="${gSiteConfig['gMOD_REWRITE'] == '1'}">${siteConfig['MOD_REWRITE_CATEGORY'].value}/${model.thisCategory.id}/${model.thisCategory.encodedName}.html?page=${frontEndProductSearch.page-1}&sortBy=${model.sortBy}</c:when><c:otherwise><c:url value="category.jhtm"><c:param name="page" value="${frontEndProductSearch.page-1}"/><c:param name="cid" value="${model.thisCategory.id}"/><c:param name="view" value="${paramView}"/><c:param name="sortBy" value="${model.sortBy}"/></c:url></c:otherwise></c:choose>" class="pageNaviLink"><span class="pageNavPrev"><i class="fa fa-angle-double-left fa-fw"></i></span></a></span></c:if>
    		</span>
			<span class="pageNavLink">
			  <c:if test="${frontEndProductSearch.page == model.pageCount}"><a href="#"><span class="pageNavNext"><i class="fa fa-angle-double-right fa-fw"></i></span></a></c:if>
			  <c:if test="${frontEndProductSearch.page != model.pageCount}"><span class="pageNavLink"><a href="${_contextpath}/<c:choose><c:when test="${gSiteConfig['gMOD_REWRITE'] == '1'}">${siteConfig['MOD_REWRITE_CATEGORY'].value}/${model.thisCategory.id}/${model.thisCategory.encodedName}.html?page=${frontEndProductSearch.page+1}&sortBy=${model.sortBy}</c:when><c:otherwise><c:url value="category.jhtm"><c:param name="page" value="${frontEndProductSearch.page-1}"/><c:param name="cid" value="${model.thisCategory.id}"/><c:param name="view" value="${paramView}"/><c:param name="sortBy" value="${model.sortBy}"/></c:url></c:otherwise></c:choose>"><span class="pageNavNext"><i class="fa fa-angle-double-right fa-fw"></i></span></a></span></c:if>
			</span>
		  </div>
		  </c:if>
		   <c:if test="${model.pageCount <= 1}">
               <input type="hidden" id="page" name="page" value="${frontEndProductSearch.page}" size="5" class="textfield50" />
           </c:if>
		</div>
	  </form>
	</div>
  </div>
</div>	