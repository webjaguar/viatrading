<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib uri="/WEB-INF/tlds/custom.tld" prefix="wj" %>

<tiles:insertDefinition name="${_template}" flush="true">
  <c:if test="${_leftBar != '1' and _leftBar != '4'}">
    <tiles:putAttribute name="leftbar" value="/WEB-INF/jsp/frontend/layout/template${_leftBar}/leftbar.jsp" />
  </c:if>
  <tiles:putAttribute name="content" type="string">
  
  
<c:if test="${model.message != null}">
  <div class="message"><fmt:message key="${model.message}" /></div>
</c:if>
		<div class="col-breadcrumb">
			<ul class="breadcrumb">
				<li><a href="${_contextpath}/account.jhtm">My Account</a></li>
				<li class="active">New Ticket</li>
			</ul>
		</div>
		
		<c:out value="${model.newTicketLayout.headerHtml}" escapeXml="false"/>
		
		<div class="col-sm-12">
			<div class="message"></div>
			<form:form id="newTicketForm" commandName="ticketForm" method="post" enctype="multipart/form-data">
				<div class="row">
					<div class="col-sm-12">
						<div class="requiredFieldLabel">* Required Field</div>
					</div>
				</div>
				<div id="newTicket">
					<div class="row">
						<div class="col-sm-6">
							<div class="form-group">
								<h3><fmt:message key="submitASupportTicket" /></h3>
							</div>
							<div class="form-group">
								<p><fmt:message key="describeYourProblemOrQuestion" /></p>
							</div>
							
							<spring:bind path="ticket.subject">
		  					<div class="form-group <c:if test="${status.error}">has-feedback has-error</c:if>">
								<label for="ticket_subject" class="control-label">
									<fmt:message key="subject" /> <sup class="requiredField">*</sup>
								</label>
								<form:input path="ticket.subject" size="50" cssClass="form-control" />
						  		<small class="help-block error-message">
			            			<form:errors path="ticket.subject" />												
								</small>  
							</div>
							</spring:bind>
							
							<spring:bind path="ticket.question">
		  					<div class="form-group <c:if test="${status.error}">has-feedback has-error</c:if>">
								<label for="ticket_question" class="control-label">
									<fmt:message key="description/question" /> <sup class="requiredField">*</sup>
								</label>
								<form:textarea path="ticket.question" rows="8" cols="50" htmlEscape="false" cssClass="form-control" />
			            		<small class="help-block error-message">
			            			<form:errors path="ticket.question" />
								</small>    
							</div>
							</spring:bind>
						</div>
						<div class="col-sm-6"></div>
					</div>
				</div>
				<div id="form_buttons">
					<div class="row">
						<div class="col-sm-12">
							<div id="buttons_wrapper">
								<button type="submit" class="btn btn-default">Create New Ticket</button>
								<button type="submit" name="_cancel" class="btn btn-default">Cancel</button>
							</div>
						</div>
					</div>
				</div>
			</form:form>
		</div>
		
		<c:out value="${model.newTicketLayout.footerHtml}" escapeXml="false"/>
	
  </tiles:putAttribute>
</tiles:insertDefinition>