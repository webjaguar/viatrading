<%@ page session="false" %>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>

<tiles:insertDefinition name="${_template}" >

<%--If template is set as 1 and leftBar is set as 4 this check will see that it doesen't overwrite the leftBar. --%>
<%--We don't need this check once we change this code to: /WEB-INF/jsp/frontend/layout/${_template}/leftbar${_leftBar}.jsp --%>
  <c:choose>
    <c:when test="${siteConfig['SHOW_LUCENE_SEARCH_FILTER'].value == 'true' and (model.thisCategory.leftbarType == '6' or siteConfig['LEFTBAR_DEFAULT_TYPE'].value == '6')}">
    	<tiles:putAttribute name="leftbar" value="/WEB-INF/jsp/frontend/layout/template6/category/filter.jsp" />
    </c:when>
    <c:otherwise>
 	  <c:if test="${_leftBar != '1' and _leftBar != '4'}">
    	<tiles:putAttribute name="leftbar" value="/WEB-INF/jsp/frontend/layout/template${_leftBar}/leftbar.jsp" />
      </c:if>
    </c:otherwise>
  </c:choose>
  <tiles:putAttribute name="content" type="string">
  
	<script type="text/javascript">
	<!--
	$(window).load(function() {
		   $(window).resize(function(){
		      if($(window).width() > 767) {
		            var product_image_wrapper_maxHeight = Math.max.apply(null, $(".product_image_wrapper").map(function(){
		                   return $(this).height();
		            }).get());
		            var product_sku_wrapper_maxHeight = Math.max.apply(null, $(".product_sku_wrapper").map(function(){
		                   return $(this).height();
		            }).get());
		            var product_name_wrapper_maxHeight = Math.max.apply(null, $(".product_name_wrapper").map(function(){
		                   return $(this).height();
		            }).get());
		            var product_wrapper_maxHeight = Math.max.apply(null, $(".product_wrapper").map(function(){
		                   return $(this).height();
		            }).get());
		            $(".product_image_wrapper").height(product_image_wrapper_maxHeight);
		            $(".product_sku_wrapper").height(product_sku_wrapper_maxHeight);
		            $(".product_name_wrapper").height(product_name_wrapper_maxHeight);
		            $(".product_wrapper").height(product_wrapper_maxHeight);
		      }
		      else {
		            $(".product_image_wrapper").css('height','auto');
		            $(".product_name_wrapper").css('height','auto');
		            $(".product_wrapper").css('height','auto');
		            $(".product_sku_wrapper").css('height','auto');
			  }
		   }).trigger('resize');
		});	
	//-->
	</script>


<script type="text/javascript">
//Add in default layout on using sitelayout
/*
$(document).ready(function(){ 
 loadajax();
});

function loadajax(){
	if(document.getElementById("shippingCost") != null){
	 $.ajax({ 
		 
		    url:"calcShipping.jhtm",
		    type:"post",
		    success:function(data){
		        console.log("shipping calculated");;
		         if(data.shipping!=null){
		    	     $('#shippingType').html(data.shipping);
		         }
		         if(data.totalCharges!=null){
                     var shipTotal = data.totalCharges;
		    	     $('#shippingCost').html('$'+shipTotal.toLocaleString("en-US"));
		         }
                  if(data.grandTotal!=0.0  && data.grandTotal!= null ){
                       var gTotal = data.grandTotal;
                       $('#grandTotal').html('$'+gTotal.toLocaleString("en-US"));
                   }
		     }
         });
    }
}
*/
</script>

	<c:set var="contentClass" value="col-sm-12"></c:set>
	<c:if test="${not layout.hideLeftBar}">
  		<c:set var="contentClass" value="col-sm-9 col-md-10"></c:set>
	</c:if>
	<div class="${contentClass}">  
	  
<c:if test="${gSiteConfig['gSHARED_CATEGORIES'] != ''}">
<c:if test="${param.sid != null}">
<c:catch var="exception">
<c:forEach items="${paramValues.sid}" var="sid" varStatus="status">
<c:if test="${status.last}">
<c:import url="${gSiteConfig['gSHARED_CATEGORIES']}sharedCategory.jsp">
  <c:param name="sid" value="${sid}" />
</c:import>
</c:if>
</c:forEach>
</c:catch>
</c:if>
</c:if>

<c:if test="${(model != null) && (model.thisCategory != null)}">

<%-- bread crumbs --%>
<c:if test="${model.breadCrumbs != null}">
</c:if>


<%-- global category header --%>
<c:if test="${model.categoryLayout.headerHtml != ''}">
  <c:out value="${model.categoryLayout.headerHtml}" escapeXml="false"/>
</c:if>




<c:if test="${model.imageBigExist}"><div align="center"><img src="${_contextpath}/assets/Image/Category/cat_${model.thisCategory.id}_big.gif" border="0" alt="<c:out value="${model.thisCategory.name}"/>"/></div></c:if>


<c:choose>
 <c:when test="${siteConfig['CATEGORY_HTML_TABLE'].value == 'true'}">
 <c:choose>
 <c:when test="${userSession == null}">
         <c:out value="${model.thisCategory['htmlCode']}" escapeXml="false" />
 </c:when>
 <c:otherwise>
         <c:out value="${model.thisCategory['protectedHtmlCode']}" escapeXml="false" />
 </c:otherwise>
 </c:choose>
 </c:when>
 <c:otherwise>
 <table border="0" cellpadding="0" cellspacing="0" class="categoryHtml">
  <tr>
    <td><c:out value="${model.thisCategory['htmlCode']}" escapeXml="false" /></td>
  </tr>
 </table>
 </c:otherwise>
</c:choose>
<c:if test="${model.adminLogin != null and model.adminLogin}">
	<a class="getshippingQuoteLink" href="${_contextpath}/admin/catalog/category.jhtm?id=${model.thisCategory.id}">Edit Page</a>
</c:if>		

<%-- subcategories --%>
<c:if test="${model.productFieldSearchType > 0}" >
<%-- Narrow Results --%>
<c:set var="actionLink">${_contextpath}/category.jhtm</c:set>
<c:if test="${gSiteConfig['gMOD_REWRITE'] == '1'}">
  <c:set var="actionLink">${_contextpath}/${siteConfig['MOD_REWRITE_CATEGORY'].value}/${model.thisCategory.id}/${model.thisCategory.encodedName}.html</c:set>
</c:if>
<form action="${actionLink}" method="get" name="search" id="searchFormId" onsubmit="return validateForm(this);">
	<c:if test="${gSiteConfig['gMOD_REWRITE']!= '1' }">
		<input type="hidden" name="cid" value="${model.thisCategory.id}" />
	</c:if>
	<%--Narrow Results  with Drop downs --%>
	<c:if test="${model.productFieldSearchType == '1'}">
		<c:import url="/WEB-INF/jsp/frontend/layout/template1/dropDownList.jsp" />
	</c:if>
	<%--Narrow Results with Check boxes --%>
	<c:if test="${model.productFieldSearchType == '2' and model.thisCategory.productFieldSearchPosition == 'top'}">
		<c:import url="/WEB-INF/jsp/frontend/layout/template4/topCheckBoxFilter.jsp" /> 
	</c:if>
	<%--Search Form (number fields MIN-MAX search) --%>
	<c:if test="${model.productFieldSearchType == '3'}">
		<%--custom category --%>
		<%@ include file="/WEB-INF/jsp/frontend/categoryCustom.jsp" %>
	</c:if>
</form>
</c:if>


<%-- products --%>

<c:choose>
	<c:when test="${model.thisCategory.layoutId == '10'}">
		<c:choose>
		<c:when test="${!model.modifiedDirectory}">
                <c:import url="/WEB-INF/jsp/frontend/layout/template6/category/products.jsp" />
     	</c:when>
		<c:otherwise>
		        <c:import url="/WEB-INF/jsp/${model.directory}/products.jsp" />
		</c:otherwise>
		</c:choose>
	</c:when>
	<c:when test="${model.thisCategory.layoutId == '7'}">
	
		<c:choose>
		<c:when test="${userSession == null}">
			<%@ include file="/WEB-INF/jsp/frontend/layout/template6/thumbnail/viewNotLogin.jsp" %>
		</c:when>
		<c:otherwise>
			<%@ include file="/WEB-INF/jsp/frontend/layout/template6/thumbnail/view3.jsp" %>
		</c:otherwise>
		</c:choose>
	
		
	</c:when>
	 <c:when test="${model.thisCategory.id != '811' && model.thisCategory.id != '835' && model.thisCategory.id != '834' && model.thisCategory.id != '557' && model.thisCategory.id != '556' && model.thisCategory.id != '833'}">
     	<c:choose>
		<c:when test="${!model.modifiedDirectory}">
    		<c:import url="/WEB-INF/jsp/frontend/layout/template6/category/products.jsp" />
    	</c:when>
		<c:otherwise>
			<c:import url="/WEB-INF/jsp/${model.directory}/products.jsp" />
		</c:otherwise>
		</c:choose>
     </c:when>
	<c:otherwise>
		
	</c:otherwise>
</c:choose>

<c:if test="${not empty model.thisCategory['footerHtmlCode']}">
<c:if test="${!(model.userSessionEnabled && model.thisCategory.layoutId == '7')}">
<table border="0" cellpadding="0" cellspacing="0" class="categoryHtml">
  <tr>
    <td><c:out value="${model.thisCategory['footerHtmlCode']}" escapeXml="false" /></td>
  </tr>
</table>
</c:if>
</c:if>

<%-- global category footer --%>
<c:if test="${model.categoryLayout.footerHtml != ''}">
  <c:out value="${model.categoryLayout.footerHtml}" escapeXml="false"/>
</c:if>

<%--Put extra div for responsiveness on categories that has left bar --%>
</div>
</c:if>

  </tiles:putAttribute>
</tiles:insertDefinition>
