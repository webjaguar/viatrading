<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="/WEB-INF/tlds/spring.tld" prefix="spring" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>

<tiles:insertDefinition name="${_template}" flush="true">
  <c:if test="${_leftBar != '1' and _leftBar != '4'}">
    <tiles:putAttribute name="leftbar" value="/WEB-INF/jsp/frontend/layout/template${_leftBar}/leftbar.jsp" />
  </c:if>
  <tiles:putAttribute name="content" type="string">
  
  
  
<div class="col-sm-12">
  <div id="loginWrapper">
	<div class="row">
	  <div class="col-sm-offset-2 col-sm-8 col-md-offset-3 col-md-6">
	    <c:out value="${model.loginLayout.headerHtml}" escapeXml="false"/>
		
		<c:choose>
		  <c:when test="${empty model.containsMaliciousCode}">
		    <c:if test="${param.noMessage != 't'}">
			  <c:if test="${!empty model.message}">
			    <div class="error"><br/><b><font color="RED"><fmt:message key="${model.message}"><fmt:param value="${siteConfig['IP_ADDRESS_ERROR_MESSAGE'].value}"/></fmt:message></font></b></div>
			  </c:if>
		    </c:if>
		  </c:when>
		  <c:otherwise>
		    <div class="error"><br/><b><font color="RED"><fmt:message key="form.removeScriptTags"></fmt:message></font></b></div>
		  </c:otherwise>
		</c:choose>
	    
	    
		<form id="login_existingCustomer_form" class="form-horizontal" role="form" method="POST" action="${model.secureUrl}/srLogin.jhtm">
		  <c:if test="${!empty signonForwardAction}">
			<input id="forwardActionId" type="hidden" name="forwardAction" value="${signonForwardAction}"/>
		  </c:if>
		  
		  <div class="form-group">
			<div class="col-sm-12">
			  <h3> <fmt:message key="salesRepLogin" /> </h3>
			</div>
		  </div>
		  <div class="form-group">
			<div class="col-sm-4">
			  <label for="username" class="control-label email_label"><fmt:message key="accountNumber"/></label>
			</div>
			<div class="col-sm-8">
			  <div class="input-group">
				<span class="input-group-addon"><span class="glyphicon glyphicon-user"></span></span>
				  <input type="text" class="form-control" autocomplete="off" name="accountNumber" placeholder="Account Number" value="${param.accountNumber}" />
			  </div>
			</div>
		  </div>
		  <div class="form-group">
			<div class="col-sm-4">
			  <label for="password" class="control-label">Password</label>
			</div>
			<div class="col-sm-8">
			  <div class="input-group">
				<span class="input-group-addon"><span class="glyphicon glyphicon-lock"></span></span>
				<input type="password" class="form-control" id="password" name="password" placeholder="Password">
			  </div>
			</div>
		  </div>
		  
		  <div class="form-group">
			<div class="col-sm-offset-4 col-sm-8">
			  <button type="submit" class="btn btn-default">Log In</button>
			</div>
		  </div>
		  
		</form>
		
	  </div>
	</div>
  </div>
</div>  
  
<c:out value="${model.loginLayout.footerHtml}" escapeXml="false"/>

  </tiles:putAttribute>
</tiles:insertDefinition>
