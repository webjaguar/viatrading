<%@ page session="false" %>
<%@ page import="com.webjaguar.model.*, java.text.NumberFormat, java.util.*, java.text.*,java.lang.reflect.Method" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<c:set var="gCUSTOMER_FIELDS" value="${gSiteConfig['gCUSTOMER_FIELDS']}"/>
<% 
 NumberFormat nf = NumberFormat.getInstance( );
 nf.setMaximumFractionDigits( 2 );
 nf.setMinimumFractionDigits( 2 );
 Layout layout = (Layout) request.getAttribute("layout");
 Cart sessionCart = (Cart) request.getSession().getAttribute( "sessionCart" );
 Cart userCart = (Cart) request.getSession().getAttribute( "userCart" );
 UserSession userSession = (UserSession) request.getAttribute( "userSession" );
 Customer sessionCustomer = (Customer) request.getAttribute("sessionCustomer");
 SalesRep sessionRelesRep = (SalesRep) request.getAttribute("sessionSalesRep"); 
 Category sessionCategory = (Category) request.getSession().getAttribute("Category");
 if(sessionCategory != null){
    layout.replace("#name#", sessionCategory.getName() != null ? sessionCategory.getName() : "");
    layout.replace("#catfield1#", sessionCategory.getField1()  != null ? sessionCategory.getField1() : "");
    layout.replace("#catfield2#", sessionCategory.getField2()  != null ? sessionCategory.getField2() : "");
    layout.replace("#catfield3#", sessionCategory.getField3()  != null ? sessionCategory.getField3() : "");
    layout.replace("#catfield4#", sessionCategory.getField4()  != null ? sessionCategory.getField4() : "");
    layout.replace("#catfield5#", sessionCategory.getField5()  != null ? sessionCategory.getField5() : "");
 }
 Affiliate affiliate = (Affiliate) request.getAttribute("affiliateInfo");
 String promocode = (String) request.getAttribute("promocode");
 Map<String, Configuration> siteConfig = (Map<String, Configuration>) request.getAttribute( "siteConfig" );
 if (userSession == null ) {
 	if (request.getSession().getAttribute( "sessionCart" ) == null ) {
 		layout.replace("#cartItems#", "0");
 		layout.replace("#cartTotalUnits#", "0");
 		layout.replace("#cartSubTotal#", "0.00");
 	} else {
 		layout.replace("#cartItems#", "" + sessionCart.getNumberOfItems());
 		layout.replace("#cartTotalUnits#", "" + sessionCart.getQuantity());
 		layout.replace("#cartSubTotal#", "" + nf.format(sessionCart.getSubTotal()) );
 	}
	layout.replace("#firstname#", "");
	layout.replace("#lastname#", "");	
	layout.replace("#email#", "");
	affiliate = (Affiliate) request.getAttribute("affiliateInfo");
	if(promocode != null){
		layout.replace("#promocode#", promocode);
	} else {
		layout.replace("#promocode#", "");
	}
 } else {
 	if (request.getSession().getAttribute( "userCart" ) == null ) {
 		layout.replace("#cartItems#", "0");
 		layout.replace("#cartTotalUnits#", "0");
 		layout.replace("#cartSubTotal#", "0.00");
 	} else {
 		layout.replace("#cartItems#", "" + userCart.getNumberOfItems());
 		layout.replace("#cartTotalUnits#", "" + userCart.getQuantity());
 		layout.replace("#cartSubTotal#", "" + nf.format(userCart.getSubTotal()) );
 	}
 	layout.replace("#firstname#", userSession.getFirstName());
	layout.replace("#lastname#", userSession.getLastName());
	layout.replace("#email#", userSession.getUsername());
	
	affiliate = (Affiliate) request.getAttribute("parentAffiliateInfo");
	
	layout.replace("#accountnumber#", sessionCustomer.getAccountNumber());
	layout.replace("#company#", sessionCustomer.getAddress().getCompany());
	layout.replace("#addr1#", sessionCustomer.getAddress().getAddr1());
	layout.replace("#addr2#", sessionCustomer.getAddress().getAddr1());
	layout.replace("#city#", sessionCustomer.getAddress().getCity());
	layout.replace("#state#", sessionCustomer.getAddress().getStateProvince());
	layout.replace("#zip#", sessionCustomer.getAddress().getZip());
	layout.replace("#country#", sessionCustomer.getAddress().getCountry());
	layout.replace("#phone#", sessionCustomer.getAddress().getPhone());
	layout.replace("#fax#", sessionCustomer.getAddress().getFax());
	layout.replace("#cellphone#", sessionCustomer.getAddress().getCellPhone());
	layout.replace("#loginSuccessPage#", (sessionCustomer.getLoginSuccessURL()==null||sessionCustomer.getLoginSuccessURL().equals(""))? siteConfig.get("LOGIN_SUCCESS_URL").getValue():sessionCustomer.getLoginSuccessURL() );
	int gCUSTOMER_FIELDS = (Integer) pageContext.getAttribute("gCUSTOMER_FIELDS");
	if (gCUSTOMER_FIELDS > 0) {
		Class<Customer> c = Customer.class;
		Method m = null;
		Object arglist[] = null;
		for (int i=1; i<=gCUSTOMER_FIELDS; i++) {
			try {
				m = c.getMethod("getField" + i);
				layout.replace("#customerfield"+i+"#", (String) m.invoke(sessionCustomer, arglist));
			} catch (Exception e)  {
			}
		}		
	}
 }
 layout.replace("#affLogo#", "");
 layout.replace("#affContactus#", "");
 layout.replace("#affAboutus#", "");
 
 if (sessionRelesRep != null) {
	 layout.replace("#salesRepName#", sessionRelesRep.getName());
 }
 SimpleDateFormat df = new SimpleDateFormat("EEEE MM/dd/yyyy");
 layout.replace("#date#", df.format(new Date()));
 layout.replace("#trackcode#", (String) request.getAttribute("trackcode"));
 layout.replace("#id#", request.getParameter("id") != null ? request.getParameter("id") : "");
%>