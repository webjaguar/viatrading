<%@ page session="false" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>

<div class="listPriceInfoWrapper">
  <c:set var="price1" value="0.0" scope="page" />
  <c:choose>
  	<c:when test="${!product.loginRequire or userSession != null && !product.altLoginRequire}" >
	  <c:forEach items="${product.price}" var="price" varStatus="statusPrice">
	  <c:if test="${price.amt > 0 || siteConfig['SHOW_ZERO_PRICE'].value == 'true'}">
	    <c:choose>
	      <c:when test="${gSiteConfig['gPRICE_CASEPACK'] and product.priceCasePackQty != null}">
		      <c:set var="multiplier" value="1"/>
		      <c:set var="caseAmt" value="false"/>
		        <c:if test="${siteConfig['CASE_CONTENT_LAYOUT'].value == '1' and product.priceCasePackQty > 0}">
				  <c:set var="multiplier" value="${product.caseContent}"/>	
		        </c:if>
		        
		        <div class="price_info">
					<span class="qtyBreak">&nbsp;</span>
					<span class="price" <c:if test="${product.salesTag != null}" >style="text-decoration: line-through"</c:if>>
						<fmt:message key="${siteConfig['CURRENCY'].value}" /><fmt:formatNumber value="${price.amt*multiplier}" pattern="#,##0.00" />
						<c:set var="price1" value="${price.amt}" />
					</span>
					<c:if test="${product.salesTag != null}" >
			    	<span class="price" <c:if test="${product.salesTag != null}" >style="text-decoration: line-through"</c:if>>
						<fmt:message key="${siteConfig['CURRENCY'].value}" /><fmt:formatNumber value="${price.discountAmt*multiplier}" pattern="#,##0.00#" maxFractionDigits="${siteConfig['MAX_FRACTION_DIGIT'].value}" />
						<c:set var="price1" value="${price.discountAmt}" />
					</span>
					</c:if>
					<span class="caseContent">&nbsp; </span>
				</div>
	      </c:when>
	      <c:otherwise>
	        <c:set var="multiplier" value="1"/>
	        <c:if test="${siteConfig['CASE_CONTENT_LAYOUT'].value == '1' and product.caseContent > 0}">
			  <c:set var="multiplier" value="${product.caseContent}"/>	
	        </c:if>
	        
	        <div class="price_info">
				<span class="qtyBreak">
				<c:if test="${price.qtyFrom != null}">
		    		${price.qtyFrom}<c:if test="${price.qtyTo != null}">-${price.qtyTo}</c:if><c:if test="${price.qtyTo == null}">+</c:if> 
		    	</c:if>
		    	</span>
				<span class="price" <c:if test="${product.salesTag != null}" >style="text-decoration: line-through"</c:if>>
					<fmt:message key="${siteConfig['CURRENCY'].value}" /><fmt:formatNumber value="${price.amt*multiplier}" pattern="#,##0.00#" maxFractionDigits="${siteConfig['MAX_FRACTION_DIGIT'].value}" />
					<c:set var="price1" value="${price.amt}" />
				</span>
				<c:if test="${product.salesTag != null}" >
		    	<span class="price">
					<fmt:message key="${siteConfig['CURRENCY'].value}" /><fmt:formatNumber value="${price.discountAmt*multiplier}" pattern="#,##0.00#" maxFractionDigits="${siteConfig['MAX_FRACTION_DIGIT'].value}" />
					<c:set var="price1" value="${price.discountAmt}" />
				</span>
				</c:if>
		    
				<span class="caseContent">
		          <c:choose>
			        <c:when test="${product.caseContent != null}">(<c:out value="${product.caseContent}" /> 
			        	<c:choose>
			  				<c:when test="${product.caseUnitTitle != null}"><c:out value="${product.caseUnitTitle}" /></c:when> 
			    			<c:otherwise><c:out value="${siteConfig['CASE_UNIT_TITLE'].value}" /></c:otherwise>
			    		</c:choose>/<c:out value="${product.packing}" />)
			    	</c:when>
			        <c:otherwise><c:out value="${product.packing}" /></c:otherwise>
			      </c:choose>
				</span>
			</div>
		  </c:otherwise>
	    </c:choose>
	  </c:if>
	  </c:forEach> 
	</c:when>
	<c:otherwise>
	  <c:choose>
	    <c:when test="${product.altLoginRequire}">
	      <img border="0" name="_altLogintoviewprice" class="_altLogintoviewprice" src="${_contextpath}/assets/Image/Layout/altLogin_to_view_price${_lang}.gif" alt="Login to view price"/>
	    </c:when>
	    <c:otherwise>
	      <a href="${_contextpath}/login.jhtm?noMessage=t&forwardAction=<c:out value='${model.url}'/>" ><img border="0" name="_logintoviewprice" class="_logintoviewprice" src="${_contextpath}/assets/Image/Layout/login_to_view_price${_lang}.gif" alt="Login to view price"/></a>
	    </c:otherwise>
	  </c:choose>
	</c:otherwise>
  </c:choose>
</div>