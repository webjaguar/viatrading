<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="/WEB-INF/tlds/spring.tld" prefix="spring" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ taglib uri="/WEB-INF/tlds/custom.tld" prefix="wj" %>
<%@ page session="false" %>

<tiles:insertDefinition name="${_template}" flush="true">
	<c:if test="${_leftBar != '1' and _leftBar != '4'}">
     <tiles:putAttribute name="leftbar" value="/WEB-INF/jsp/frontend/layout/template${_leftBar}/leftbar.jsp" />
    </c:if>
	<tiles:putAttribute name="content" type="string">
    <%-- breadcrumb --%>
    <c:out value="${wj:generateBreadCrumb('Comissions')}" escapeXml="false"/>  

<br><br><br>

<form method="post">
<table width="90%" align="center" cellspacing="5" cellpadding="0" border="0">
  <tr>
	<td class="listingsHdr1"><fmt:message key="commissionsYouMakeFromLevel" /> <c:out value="${model.level}"></c:out> </td>
	<td>
<div class="pageNavi">Page 
		<select name="page" onchange="submit()">
		<c:forEach begin="1" end="${model.orders.pageCount}" var="page">
  	        <option value="${page}" <c:if test="${page == (model.orders.page+1)}">selected</c:if>>${page}</option>
		</c:forEach>
		</select>
of <c:out value="${model.orders.pageCount}"/></div>
	</td>
  </tr>
  <tr>
	<td colspan="2">
<table border="0" cellpadding="3" cellspacing="1" width="100%" class="listings"> 
  <tr class="listingsHdr2" align="center">
  	<td class="nameCol" colspan="4"><fmt:message key="order" /></td>
  	<td class="nameCol" colspan="4"><fmt:message key="commission" /></td>
  </tr>
  <tr class="listingsHdr2">
  	<td class="nameCol"><fmt:message key="orderDate" /></td>
  	<td class="nameCol"><fmt:message key="orderNumber" /></td>
  	<td class="nameCol"><fmt:message key="total" /></td>
  	<td class="nameCol"><fmt:message key="status" /></td>
  	<td class="nameCol"><fmt:message key="myCommission" /></td>
  	<td class="nameCol"><fmt:message key="paymentStatus" /></td>
   	<td class="nameCol"><fmt:message key="checkNumber" /></td>
  	<td class="nameCol"><fmt:message key="date" /></td>
 </tr>
  
<c:forEach items="${model.orders.pageList}" var="order" varStatus="status">
<c:set var="yearMonthNew" value="${fn:split(model.orders.pageList[status.index].orderDate,'-')[0]}${fn:split(model.orders.pageList[status.index].orderDate,'-')[1]}"></c:set>
<c:set var="yearMonthOld" value="${fn:split(model.orders.pageList[status.index+1].orderDate,'-')[0]}${fn:split(model.orders.pageList[status.index+1].orderDate,'-')[1]}"></c:set>

	<tr class="row${status.index % 2}">
    	<td class="nameCol" style="white-space: nowrap"><fmt:formatDate type="date" timeStyle="default" value="${order.orderDate}"/></td>			
		<td class="nameCol"><c:if test="${siteConfig['SHOW_CHILD_INVOICE'].value == 'true'}"><a href="invoice.jhtm?order=${order.orderId}" class="nameLink"></c:if><c:out value="${order.orderId}"/><c:if test="${siteConfig['SHOW_CHILD_INVOICE'].value == 'true'}"></a></c:if></td>
		<td class="nameCol"><fmt:formatNumber value="${order.subTotal}" pattern="#,##0.00" /></td>
		<td class="nameCol"><c:out value="${order.orderStatusToString}" /></td>
		<td class="nameCol"><fmt:formatNumber value="${order.myCommission}" pattern="#,##0.00" /></td>
		<c:if test="${model.level == 1}">
	  		<td class="nameCol"><c:out value="${order.commissionLevel1StatusToString}"/></td>
		</c:if>
		<c:if test="${model.level == 2}">
	  		<td class="nameCol"><c:out value="${order.commissionLevel2StatusToString}"/></td>
		</c:if>
		<c:if test="${model.level == 1}">
			<td class="nameCol"><c:out value="${order.checkNumberLevel1}" /></td>
			<td class="nameCol" style="white-space: nowrap"><fmt:formatDate type="date" timeStyle="default" value="${order.dateLevel1}"/></td>
		</c:if>
		<c:if test="${model.level == 2}">
			<td class="nameCol"><c:out value="${order.checkNumberLevel2}" /></td>
			<td class="nameCol" style="white-space: nowrap"><fmt:formatDate type="date" timeStyle="default" value="${order.dateLevel2}"/></td>
		</c:if>
	</tr>
  
<c:if test="${!(yearMonthNew eq yearMonthOld)}">
  	<tr class="listingsHdr2" align="center">
		<td class="nameCol" colspan="4"><fmt:message key="totalCommissionForTheMonth" /> </td>
	  	<td class="nameCol" colspan="4"><fmt:formatNumber value="${model.map[yearMonthNew]}" pattern="#,##0.00" /></td>
	</tr> 
</c:if>
  
<c:if test="${yearMonthNew eq null}">
  	<tr class="listingsHdr2" align="center">
		<td class="nameCol" colspan="4"><fmt:message key="totalCommissionForTheMonth" />
	  	<td class="nameCol" colspan="4"><fmt:formatNumber value="${model.map[yearMonthNew]}" pattern="#,##0.00" /></td>
	</tr> 
</c:if>
  
</c:forEach>
<c:if test="${model.orders.nrOfElements == 0}">
  <tr class="emptyList"><td colspan="6">&nbsp;</td></tr>
</c:if>
</table>
	</td>
  </tr>
</table>
</form>

  </tiles:putAttribute>
</tiles:insertDefinition>
