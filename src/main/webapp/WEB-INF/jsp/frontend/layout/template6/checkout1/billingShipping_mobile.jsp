<%@ page session="false" %>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="/WEB-INF/tlds/spring.tld" prefix="spring" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>

<tiles:insertDefinition name="${_template}" flush="true">
  <c:if test="${_leftBar != '1' and _leftBar != '4'}">
    <tiles:putAttribute name="leftbar" value="/WEB-INF/jsp/frontend/layout/template${_leftBar}/leftbar.jsp" />
  </c:if>
  <tiles:putAttribute name="content" type="string">
  
<c:out value="${model.billingShippingLayout.headerHtml}" escapeXml="false"/>  


<div class="col-sm-12">
	<div class="shoppingCartWizard clearfix">
		<div class="wizard clearfix">
			<div class="wizard-bar" style="width: 33.333%;">
				<div class="wizard-step active clearfix">
					<div class="stepNmbr">1</div>
					<div class="stepDesc">
						Shopping Cart
					</div>
				</div>
			</div>
			<div class="wizard-bar" style="width: 33.333%;">
				<div class="wizard-step active clearfix">
					<div class="stepNmbr">2</div>
					<div class="stepDesc">
						Shipping &amp; Payment
					</div>
				</div>
			</div>
			<div class="wizard-bar" style="width: 33.333%;">
				<div class="wizard-step clearfix">
					<div class="stepNmbr">3</div>
					<div class="stepDesc">
						Order Confirmation
					</div>
				</div>
			</div>
		</div>
		<div class="progress">
			<div class="progress-bar" style="width: 33.333%;"></div>
			<div class="progress-bar" style="width: 33.333%;"></div>
		</div>
	</div>

	<div class="billingShippingWrapper">
		<div class="returningCustomerBox">
			<div class="headerTitle returningCustomer"><fmt:message key="returningCustomer"/></div>
			<div id="loginInfoBox">
				<div class="row">
					<div class="col-sm-offset-2 col-sm-8 col-md-offset-3 col-md-6">
						<form id="login_existingCustomer_form" action="billingShipping.jhtm" class="form-horizontal"
							role="form" method="POST" action="/login.jhtm">
							<div class="form-group">
								<div class="col-sm-12">
									<h3><fmt:message key="login"/></h3>
								</div>
							</div>
							<div class="form-group">
								<div class="col-sm-4">
									<label for="username" class="control-label email_label"><fmt:message key="emailAddress"/></label>
								</div>
								<div class="col-sm-8">
									<div class="input-group">
										<span class="input-group-addon">
											<span class="glyphicon glyphicon-user"></span>
										</span> 
										<input type="text" class="form-control" id="username" placeholder="Email" name="username" value="${param.username}" />
									</div>
								</div>
							</div>
							<div class="form-group">
								<div class="col-sm-4">
									<label for="password" class="control-label"><fmt:message key="password"/></label>
								</div>
								<div class="col-sm-8">
									<div class="input-group">
										<span class="input-group-addon">
											<span class="glyphicon glyphicon-lock"></span>
										</span> 
										<input type="password" class="form-control" id="password" placeholder="Password" name="password" />
									</div>
								</div>
							</div>
							<div class="form-group">
								<div class="col-sm-offset-4 col-sm-8">
									<button type="submit" class="btn btn-default" name="_login" ><fmt:message key="logIn"/></button>
								</div>
							</div>
							<div class="form-group">
								<div class="col-sm-offset-4 col-sm-8">
									<p class="help-block">
										Forgot your password? 
										<a href="forgetPassword.jhtm" class="forgetPassword">Click to reset</a>
									</p>
								</div>
							</div>
						</form>
					</div>
				</div>
			</div>
		</div>

		<c:if test="${!(gSiteConfig['gREGISTRATION_DISABLED'] or siteConfig['PROTECTED_HOST'].value == header['host'])}">
		<div class="newCustomerBox">
			<div class="headerTitle newCustomer"><fmt:message key="f_checkout1_newCustomer"/></div>
		<form:form commandName="customerForm" action="billingShipping.jhtm" method="post">
			<div class="row">
				<div class="col-sm-6">
					<div class="billingInfoBox">
						<div class="headerTitle billingInfo"><fmt:message key="billingInformation"/></div>
						<div class="billingInfoForm">
							<div class="message"></div>
							<div class="form-horizontal">
								<div id="billingInformation">
									<div class="row">
										<div class="col-sm-12">
											<div class="form-group">
												<div class="col-sm-4">
													<label for="customer_address_firstName"
														class="control-label"> <fmt:message key="firstName"/> <sup
														class="requiredField">*</sup>
													</label>
												</div>
												<div class="col-sm-8">
													<form:input cssClass="form-control" id="customer_address_firstName" path="customer.address.firstName" htmlEscape="true"/>
	  												<form:errors path="customer.address.firstName" cssClass="error" />    
    											</div>
											</div>
											<div class="form-group">
												<div class="col-sm-4">
													<label for="customer_address_lastName"
														class="control-label"> <fmt:message key="lastName"/> <sup
														class="requiredField">*</sup>
													</label>
												</div>
												<div class="col-sm-8">
													<form:input cssClass="form-control" id="customer_address_lastName" path="customer.address.lastName" htmlEscape="true"/>
	  												<form:errors path="customer.address.lastName" cssClass="error" />    
    											</div>
											</div>
											<div class="form-group">
												<div class="col-sm-4">
													<label for="customer_address_country" class="control-label">
														<fmt:message key="country"/> <sup class="requiredField">*</sup>
													</label>
												</div>
												<div class="col-sm-8">
													<form:select class="form-control" id="customer_address_country" path="customer.address.country">
												       <form:option value="" label="Please Select"/>
												       <form:options items="${model.countrylist}" itemValue="code" itemLabel="name"/>
												    </form:select>
												    <form:errors path="customer.address.country" cssClass="error" />      
												</div>
											</div>
											<div class="form-group">
												<div class="col-sm-4">
													<label for="customer_address_company" class="control-label">
														<fmt:message key="company" /> <c:if test="${siteConfig['CUSTOMERS_REQUIRED_COMPANY'].value == 'true'}"><sup class="requiredField">*</sup></c:if>
														</label>
												</div>
												<div class="col-sm-8">
													<form:input class="form-control" id="customer_address_company" path="customer.address.company" htmlEscape="true"/>
	  												<form:errors path="customer.address.company" cssClass="error" />    
												</div>
											</div>
											<div class="form-group">
												<div class="col-sm-4">
													<label for="customer_address_addr1" class="control-label">
														<fmt:message key="address" /> 1 <sup class="requiredField">*</sup>
													</label>
												</div>
												<div class="col-sm-8">
													<form:input class="form-control" id="customer_address_addr1" path="customer.address.addr1" htmlEscape="true"/>
													<form:errors path="customer.address.addr1" cssClass="error" />    
    											</div>
											</div>
											<div class="form-group">
												<div class="col-sm-4">
													<label for="customer_address_addr2" class="control-label">
														<fmt:message key="address" /> 2 </label>
												</div>
												<div class="col-sm-8">
													<form:input class="form-control" id="customer_address_addr2" path="customer.address.addr2" htmlEscape="true"/>
													<form:errors path="customer.address.addr1" cssClass="error" />    
    											</div>
											</div>
											<div class="form-group">
												<div class="col-sm-4">
													<label for="customer_address_city" class="control-label">
														<fmt:message key="city" /> <sup class="requiredField">*</sup>
													</label>
												</div>
												<div class="col-sm-8">
													<form:input class="form-control" id="customer_address_city" path="customer.address.city" htmlEscape="true"/>
													<form:errors path="customer.address.city" cssClass="error" />    
												</div>
											</div>
											<div class="form-group">
												<div class="col-sm-4">
													<label for="" class="control-label"> <fmt:message key="stateProvince" />
														<sup class="requiredField">*</sup>
													</label>
												</div>
												<div class="col-sm-8">
													<form:input class="form-control" id="customer_address_stateProvince" path="customer.address.stateProvince" htmlEscape="true" disabled="${customerForm.customer.address.country == 'US' or customerForm.customer.address.country == 'CA' or customerForm.customer.address.country == '' or customerForm.customer.address.country == null}"/>
         											<form:errors path="customer.address.stateProvince" cssClass="error" />  
													
													
													<div id="stateProvinceNA" class="checkbox">
														<label> 
														  <form:checkbox path="customer.address.stateProvinceNA" id="customer_address_stateProvinceNA" value="true"  disabled="${customerForm.customer.address.country == 'US' or customerForm.customer.address.country == 'CA' or customerForm.customer.address.country == '' or customerForm.customer.address.country == null}"/> Not Applicable
														</label>
													</div>

													<form:select class="form-control"  id="customer_address_state" path="customer.address.stateProvince" disabled="${customerForm.customer.address.country != 'US'}">
											          <form:option value="" label="Please Select"/>
											          <form:options items="${model.statelist}" itemValue="code" itemLabel="name"/>
											        </form:select>
        	
										            <form:select class="form-control" id="customer_address_ca_province" path="customer.address.stateProvince" disabled="${customerForm.customer.address.country != 'CA'}">
											          <form:option value="" label="Please Select"/>
											          <form:options items="${model.caProvinceList}" itemValue="code" itemLabel="name"/>
											        </form:select>
         										</div>
											</div>
											<div class="form-group">
												<div class="col-sm-4">
													<label for="customer_address_zip" class="control-label">
														<fmt:message key="zipCode" /> <sup class="requiredField">*</sup>
													</label>
												</div>
												<div class="col-sm-8">
								  					<form:input class="form-control" id="customer_address_zip" path="customer.address.zip" htmlEscape="true"/>
	  												<form:errors path="customer.address.zip" cssClass="error" />    
												</div>
											</div>
											<div class="form-group">
												<div class="col-sm-4">
													<label for="customer_address_phone" class="control-label">
														<fmt:message key="phone" /> <sup class="requiredField">*</sup>
													</label>
												</div>
												<div class="col-sm-8">
												    <form:input class="form-control" id="customer_address_phone"  path="customer.address.phone" htmlEscape="true"/>
												    <form:errors path="customer.address.phone" cssClass="error" />    
												</div>
											</div>
											<div class="form-group">
												<div class="col-sm-4">
													<label for="customer_address_cellPhone"
														class="control-label"> <fmt:message key="cellPhone" /> </label>
												</div>
												<div class="col-sm-8">
											        <form:input class="form-control" id="customer_address_cellPhone" path="customer.address.cellPhone" htmlEscape="true"/>
												    <form:errors path="customer.address.cellPhone" cssClass="error" />    
												</div>
											</div>
											<div class="form-group">
												<div class="col-sm-4">
													<label for="customer_address_fax" class="control-label">
														<fmt:message key="fax" /> </label>
												</div>
												<div class="col-sm-8">
											        <form:input class="form-control" id="customer_address_fax" path="customer.address.fax" htmlEscape="true"/>
												    <form:errors path="customer.address.fax" cssClass="error" />    
												</div>
											</div>
											<div class="form-group">
												<div class="col-sm-4">
													<label for="customer_username" class="control-label">
														<fmt:message key="email" /> <sup class="requiredField">*</sup>
													</label>
												</div>
												<div class="col-sm-8">
												    <form:input class="form-control" id="customer_username" path="customer.username" maxlength="80" htmlEscape="true"/>
												    <form:errors path="customer.username" cssClass="error" />    
												</div>
											</div>
											<div class="form-group">
												<div class="col-sm-4">
													<label for="customer_password" class="control-label">
														<fmt:message key="password" /> <sup class="requiredField">*</sup>
													</label>
												</div>
												<div class="col-sm-8">
												    <form:password class="form-control" id="customer_password" path="customer.password" htmlEscape="true"/>
												    <form:errors path="customer.password" cssClass="error" />
												</div>
											</div>
											<div class="form-group">
												<div class="col-sm-4">
													<label for="confirm_password" class="control-label">
													  <fmt:message key="confirmPassword" /> <sup class="requiredField">*</sup>
													</label>
												</div>
												<div class="col-sm-8">
													<form:password class="form-control" id="confirm_password" path="confirmPassword" htmlEscape="true"/>
													<form:errors path="confirmPassword" cssClass="error" />    
												</div>
											</div>
											<c:if test="${gSiteConfig['gMASS_EMAIL'] or siteConfig['CUSTOMER_MASS_EMAIL'].value == 'true'}">
  											<div class="form-group">
												<div class="col-sm-12">
													<div id="subscribeCheckBoxId" class="checkbox">
														<label><form:checkbox path="subscribeEmail"  /><fmt:message key="f_subscribeMailingList" /></label>
													</div>
												</div>
											</div>
											</c:if>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="col-sm-6">
					<div class="shippingInfoBox">
						<div class="headerTitle shippingInfo"><fmt:message key="shippingInformation"/></div>
						<div class="shippingInfoForm">
							<div class="message"></div>
							<div class="form-horizontal">
								<div id="shippingInformation">
									<div class="row">
										<div class="col-sm-12">
											<div class="form-group">
												<div class="col-sm-12">
													<div id="sameAsBillingWrapper" class="checkbox">
														<label> 
														<form:checkbox id="sameAsBilling" path="sameAsBilling" onclick="toggleShipping(this)" disabled="${siteConfig['ADD_ADDRESS_ON_CHECKOUT'].value != 'true'}"/>
														<fmt:message key="sameAsBilling"/>
														</label>
													</div>
												</div>
											</div>
											<div class="form-group">
												<div class="col-sm-4">
													<label for="shipping_firstName" class="control-label">
														<fmt:message key="firstName"/> <sup class="requiredField">*</sup>
													</label>
												</div>
												<div class="col-sm-8">
													<form:input class="form-control" id="shipping_firstName" path="shipping.firstName" htmlEscape="true"/>
													<form:errors path="shipping.firstName" cssClass="error" />    
												</div>
											</div>
											<div class="form-group">
												<div class="col-sm-4">
													<label for="shipping_lastName" class="control-label">
														<fmt:message key="lastName"/><sup class="requiredField">*</sup>
													</label>
												</div>
												<div class="col-sm-8">
													<form:input class="form-control" id="shipping_lastName" path="shipping.firstName" htmlEscape="true"/>
													<form:errors path="shipping.firstName" cssClass="error" />    
												</div>
											</div>
											<div class="form-group">
												<div class="col-sm-4">
													<label for="shipping_country" class="control-label"><fmt:message key="country"/> <sup class="requiredField">*</sup>
													</label>
												</div>
												<div class="col-sm-8">
													<form:select class="form-control" id="shipping_country" path="shipping.country" onchange="toggleShippingStateProvince(this)" cssStyle="width:150px">
													  <form:option value="" label="Please Select"/>
													  <form:options items="${model.countrylist}" itemValue="code" itemLabel="name"/>
													</form:select>
													<form:errors path="shipping.country" cssClass="error" />      
												</div>
											</div>
											<div class="form-group">
												<div class="col-sm-4">
													<label for="shipping_company" class="control-label">
														<fmt:message key="company"/> <c:if test="${siteConfig['CUSTOMERS_REQUIRED_COMPANY'].value == 'true'}"><sup class="requiredField">*</sup></c:if></label>
												</div>
												<div class="col-sm-8">
													<form:input class="form-control" id="shipping_company"  path="shipping.company" htmlEscape="true"/>
													<form:errors path="shipping.company" cssClass="error" />    
												</div>
											</div>
											<div class="form-group">
												<div class="col-sm-4">
													<label for="shipping_addr1" class="control-label">
														<fmt:message key="address" /> 1 <sup class="requiredField">*</sup>
													</label>
												</div>
												<div class="col-sm-8">
													<form:input class="form-control" id="shipping_addr1" path="shipping.addr1" htmlEscape="true"/>
													<form:errors path="shipping.addr1" cssClass="error" />    
												</div>
											</div>
											<div class="form-group">
												<div class="col-sm-4">
													<label for="shipping_addr2" class="control-label">
														<fmt:message key="address" /> 2 </label>
												</div>
												<div class="col-sm-8">
													<form:input class="form-control" id="shipping_addr2" path="shipping.addr2" htmlEscape="true"/>
													<form:errors path="shipping.addr2" cssClass="error" />    
												</div>
											</div>
											<div class="form-group">
												<div class="col-sm-4">
													<label for="shipping_city" class="control-label">
														<fmt:message key="city" /> <sup class="requiredField">*</sup>
													</label>
												</div>
												<div class="col-sm-8">
													<form:input class="form-control" id="shipping_city" path="shipping.city" htmlEscape="true"/>
													<form:errors path="shipping.city" cssClass="error" />    
												</div>
											</div>
											<div class="form-group">
												<div class="col-sm-4">
													<label for="" class="control-label"> State/Province
														<sup class="requiredField">*</sup>
													</label>
												</div>
												<div class="col-sm-8">
													<form:input class="form-control" id="shipping_stateProvince" path="shipping.stateProvince" htmlEscape="true" disabled="${customerForm.shipping.country == 'US' or customerForm.shipping.country == 'CA' or customerForm.shipping.country == '' or customerForm.shipping.country == null}"/>
    												<div id="stateProvinceNA_shipping" class="checkbox">
														<label> 
															<form:checkbox path="shipping.stateProvinceNA" id="shipping_stateProvinceNA" value="true"  disabled="${customerForm.shipping.country == 'US' or customerForm.shipping.country == 'CA' or customerForm.shipping.country == '' or customerForm.shipping.country == null}"/> Not Applicable
														</label>
													</div>
													<form:select class="form-control" id="shipping_state" path="shipping.stateProvince" disabled="${customerForm.shipping.country != 'US'}" cssStyle="width:150px">
													   <form:option value="" label="Please Select"/>
													   <form:options items="${model.statelist}" itemValue="code" itemLabel="name"/>
													</form:select>
         											<form:select class="form-control" id="shipping_ca_province" path="shipping.stateProvince" disabled="${customerForm.shipping.country != 'CA'}" cssStyle="width:150px">
												       <form:option value="" label="Please Select"/>
												       <form:options items="${model.caProvinceList}" itemValue="code" itemLabel="name"/>
												    </form:select>
											        <form:errors path="shipping.stateProvince" cssClass="error" />
        										</div>
											</div>
											<div class="form-group">
												<div class="col-sm-4">
													<label for="shipping_zip" class="control-label">
														<fmt:message key="zipCode" /> <sup class="requiredField">*</sup>
													</label>
												</div>
												<div class="col-sm-8">
													<form:input class="form-control" id="shipping_zip" path="shipping.zip" htmlEscape="true"/>
													<form:errors path="shipping.zip" cssClass="error" />    
												</div>
											</div>
											<div class="form-group">
												<div class="col-sm-4">
													<label for="shipping_phone" class="control-label">
														<fmt:message key="phone" /> <sup class="requiredField">*</sup>
													</label>
												</div>
												<div class="col-sm-8">
										  			<form:input class="form-control" id="shipping_phone" path="shipping.phone" htmlEscape="true"/>
												    <form:errors path="shipping.phone" cssClass="error" />    
												</div>
											</div>
											<div class="form-group">
												<div class="col-sm-4">
													<label for="shipping_cellPhone" class="control-label">
														<fmt:message key="cellPhone" /> </label>
												</div>
												<div class="col-sm-8">
										  			<form:input class="form-control" id="shipping_cellPhone" path="shipping.cellPhone" htmlEscape="true"/>
												    <form:errors path="shipping.cellPhone" cssClass="error" />    
												</div>
											</div>
											<div class="form-group">
												<div class="col-sm-4">
													<label for="shipping_fax" class="control-label">
														<fmt:message key="fax" /> </label>
												</div>
												<div class="col-sm-8">
										  			<form:input class="form-control" id="shipping_fax" path="shipping.fax" htmlEscape="true"/>
												    <form:errors path="shipping.fax" cssClass="error" />    
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="col-sm-12">
					<c:if test="${gSiteConfig['gTAX_EXEMPTION']}">
					<div class="taxExemptionBox">
						<div class="row">
							<div class="col-sm-6">
								<div class="headerTitle taxInformation"><fmt:message key="taxInformation"/></div>
								<div class="taxInfo">
									<div class="form-horizontal">
										<div class="form-group">
											<div class="col-sm-5">
												<label for="customer_taxId" class="control-label">
													<fmt:message key="taxId" /> </label>
											</div>
											<div class="col-sm-7">
										      	<form:input class="form-control" id="customer_taxId" path="customer.taxId" maxlength="30" htmlEscape="true"/>
												<form:errors path="customer.taxId" cssClass="error" />      
											</div>
										</div>
									</div>
								</div>
								<div class="form-horizontal">
									<div class="form-group">
										<div class="col-sm-12">
											<span class="taxIdNote"><c:out value="${siteConfig['TAX_ID_NOTE'].value}" /></span>
										</div>
									</div>
								</div>
							</div>
							<div class="col-sm-6"></div>
						</div>
					</div>
					</c:if>
					<div id="form_buttons">
						<div class="row">
							<div class="col-sm-12">
								<div id="buttons_wrapper">
									<button type="submit" class="btn btn-default">Next
										Step</button>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</form:form>
		</div>
		</c:if>
	</div>
</div>


<iframe src="${_contextpath}/sessionKeepAlive.jsp" width="0" height="0" frameborder="0"></iframe>

  </tiles:putAttribute>
</tiles:insertDefinition>
