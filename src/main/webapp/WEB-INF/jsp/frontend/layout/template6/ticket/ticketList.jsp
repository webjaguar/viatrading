<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ taglib uri="/WEB-INF/tlds/custom.tld" prefix="wj" %>

<tiles:insertDefinition name="${_template}" flush="true">
  <c:if test="${_leftBar != '1' and _leftBar != '4'}">
    <tiles:putAttribute name="leftbar" value="/WEB-INF/jsp/frontend/layout/template${_leftBar}/leftbar.jsp" />
  </c:if>
  <tiles:putAttribute name="content" type="string">
  

		<div class="col-breadcrumb">
			<ul class="breadcrumb">
				<li><a href="${_contextpath}/account.jhtm">My Account</a></li>
				<li class="active">Ticket List</li>
			</ul>
		</div>
		
		<c:out value="${model.ticketsLayout.headerHtml}" escapeXml="false"/>  

		<div class="col-sm-12">
			<div class="accountTicketListWrapper">
				<div class="headerTitle accountTicketList">Ticket List</div>

				<div class="clearfix"></div>

				<c:if test="${model.message != null}">
				  <div class="message"><fmt:message key="${model.message}" /></div>
				</c:if>

				<c:if test="${fn:length(model.ticketList.pageList) == 0}">
				  No Tickets found
				</c:if>
				<c:if test="${fn:length(model.ticketList.pageList) > 0}">
				<div class="row">
					<div class="col-sm-12">
						<div class="pageNavigation">
							<form class="formPageNavigation clearfix" name="pagination" method="post">
								<div class="pageNavigationControl">
									<%-- 
									<div class="pagesize">
										<span>Page Size</span>
										<select class="selectpicker" name="pagesize">
											<option value="10">10 per page</option>
											<option value="20">20 per page</option>
											<option value="25" selected="">25 per page</option>
											<option value="50">50 per page</option>
											<option value="100">100 per page</option>
										</select>
									</div>--%>
									<div class="page">
										<span>Page</span>
										<select class="selectpicker" name="page" onchange="submit()">
										<c:forEach begin="1" end="${model.ticketList.pageCount}" var="page">
								  	        <option value="${page}" <c:if test="${page == (model.ticketList.page+1)}">selected</c:if>>${page}</option>
										</c:forEach>
										</select>
										<span>of <c:out value="${model.ticketList.pageCount}"/></span>
									</div>
									<%--
									<div class="pageNav">
										<span class="pageNavLink">
											<a href="#"><span class="pageNavPrev"></span></a>
										</span>
										<span class="pageNavLink">
											<a href="#"><span class="pageNavNext"></span></a>
										</span>
									</div> --%>
								</div>
							</form>
						</div>
					</div>
				</div>
				<form method="post">
				<div class="accountTicketListFilter">
					<div class="row clearfix">
						<div class="col-sm-12 col-md-2">
							<div class="form-group">
								<label for="status">Status</label>
								<select name="status" class="ticketStatusFilter">
							        <option value="open" <c:if test="${ticketSearch.status == 'open'}">selected</c:if>>Open</option>
							        <option value="close" <c:if test="${ticketSearch.status == 'close'}">selected</c:if>>Close</option> 
							        <option value="" <c:if test="${ticketSearch.status == ''}">selected</c:if>>All</option> 
							    </select>
							</div>
						</div>
						<div class="col-sm-12 col-md-2">
							<div class="form-group">
								<label for="ticket_id">Ticket ID</label>
								<input type="text" name="ticket_id" value="<c:out value='${ticketSearch.ticketId}' />" class="ticketIDFilter">
							</div>
						</div>
						<div class="col-sm-12 col-md-4">
							<div class="form-group">
								<label for="subject">Subject</label>
								<input type="text" name="subject" value="<c:out value='${ticketSearch.subject}' />" class="ticketSubjectFilter">
							</div>
						</div>
						<div class="col-sm-12 col-md-4">
							<div class="form-group">
								<button type="submit" class="btn search_btn">Search</button>
							</div>
						</div>
					</div>
				</div>

				<div class="accountTicketList_table">
					<div class="accountTicketListHdr">
						<div class="row clearfix">
							<div class="col-sm-12 col-md-2">
								<div class="listStatusHeader"><fmt:message key="status" /></div>
							</div>
							<div class="col-sm-12 col-md-2">
								<div class="listTicketNumberHeader"><fmt:message key="ticketNumber" /></div>
							</div>
							<div class="col-sm-12 col-md-4">
								<div class="listSubjectHeader"><fmt:message key="subject" /></div>
							</div>
							<div class="col-sm-12 col-md-2">
								<div class="listCreatedHeader"><fmt:message key="created" /></div>
							</div>
							<div class="col-sm-12 col-md-2">
								<div class="listLastModifiedHeader"><fmt:message key="lastModified" /></div>
							</div>
						</div>
					</div>

					<div class="accountTicketListDetails">
					<c:forEach items="${model.ticketList.pageList}" var="ticket" varStatus="status">
						<c:set var="statusClass" value="odd_row" />
					    <c:if test="${(status.index + 1) % 2 == 0}">
					      <c:set var="statusClass" value="even_row" />
					    </c:if>
						<div class="${statusClass} clearfix">
							<div class="col-sm-12 col-md-2">
								<div class="listStatusHeader visible-xs visible-sm"><fmt:message key="status" /></div>
								<div class="listStatus"><c:out value="${ticket.status}"/></div>
								<div class="clearfix"></div>
							</div>
							<div class="col-sm-12 col-md-2">
								<div class="listTicketNumberHeader visible-xs visible-sm"><fmt:message key="ticketNumber" />Ticket Number</div>
								<div class="listTicketNumber"><a href="account_ticketUpdate.jhtm?id=${ticket.ticketId}" class="nameLink"><c:out value="${ticket.ticketId}"/></a></div>
								<div class="clearfix"></div>
							</div>
							<div class="col-sm-12 col-md-4">
								<div class="listSubjectHeader visible-xs visible-sm"><fmt:message key="subject" />Subject</div>
								<div class="listSubject"><c:out value="${ticket.subject}"/></div>
								<div class="clearfix"></div>
							</div>
							<div class="col-sm-12 col-md-2">
								<div class="listCreatedHeader visible-xs visible-sm"><fmt:message key="lastModified" />Created</div>
								<div class="listCreated"><fmt:formatDate type="both" timeStyle="full" pattern="MM/dd/yyyy hh:mm a" value="${ticket.created}"/></div>
								<div class="clearfix"></div>
							</div>
							<div class="col-sm-12 col-md-2">
								<div class="listLastModifiedHeader visible-xs visible-sm"><fmt:message key="lastModified" />Last Modified</div>
								<div class="listLastModified"><fmt:formatDate type="both" timeStyle="full" pattern="MM/dd/yyyy hh:mm a" value="${ticket.lastModified}"/></div>
								<div class="clearfix"></div>
							</div>
						</div>
						</c:forEach>
					</div>

					<div class="accountTicketListFtr">
						<div class="row clearfix"></div>
					</div>
				</div>
				</form>
				</c:if>
				
			
			</div>
		</div>
		
	<c:out value="${model.ticketsLayout.headerHtml}" escapeXml="false"/>
		
  </tiles:putAttribute>
</tiles:insertDefinition>