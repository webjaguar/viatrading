<%@ page session="false" %>
<%@ page import="com.webjaguar.model.*, java.text.NumberFormat, java.util.*, java.text.*,java.lang.reflect.Method" %>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<tiles:importAttribute scope="request" />
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<!-- ========================= CSS ========================= -->
		<%--
		<!-- Bootstrap v3.1.1 -->
		<link rel="stylesheet" type="text/css" href="${_contextpath}/assets/template6/css/bootstrap.css">
		<link rel="stylesheet" type="text/css" href="${_contextpath}/assets/template6/css/bootstrap-theme.css">
		<!-- Font Awesome 4.1.0 -->
		<link rel="stylesheet" type="text/css" href="${_contextpath}/assets/template6/css/font-awesome.css">
		<!-- OwlCarousel -->
		<link rel="stylesheet" type="text/css" href="${_contextpath}/assets/template6/plugins/owl-carousel/css/owl.carousel.css">
		<link rel="stylesheet" type="text/css" href="${_contextpath}/assets/template6/plugins/owl-carousel/css/owl.theme.css">
		<link rel="stylesheet" type="text/css" href="${_contextpath}/assets/template6/plugins/owl-carousel/css/owl.transitions.css">
		<link rel="stylesheet" type="text/css" href="${_contextpath}/assets/template6/css/slider-theme.css">
		<link rel="stylesheet" type="text/css" href="${_contextpath}/assets/template6/css/carousel-theme.css">
		<link rel="stylesheet" type="text/css" href="${_contextpath}/assets/template6/plugins/lightbox/css/lightbox.css">
		<!-- BootstrapValidator -->
		<link rel="stylesheet" type="text/css" href="${_contextpath}/assets/template6/css/bootstrapValidator.css">
		<!-- intlTelInput -->
		<link rel="stylesheet" type="text/css" href="${_contextpath}/assets/template6/css/intlTelInput.css">
 
		<!-- Page Styles -->
		<%-- Put on Layout Head Tag
		<link rel="stylesheet" type="text/css" href="${_contextpath}/assets/template6/css/webjaguar.css">
		 --%>
		<%--
		<link rel="stylesheet" type="text/css" href="${_contextpath}/assets/template6/css/template_1.css">
	 	--%>
		
	<!-- ========================= JS ========================= -->
		<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
		
		<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
		<!--[if lt IE 9]>
		    <script src="${_contextpath}/assets/template6/js/libs/html5shiv.js"></script>
		    <script src="${_contextpath}/assets/template6/js/libs/respond.min.js"></script>
		<![endif]-->
		<!-- jQuery JavaScript Library v1.11.1 -->
		<%--
		<script type="text/javascript" src="${_contextpath}/assets/template6/js/libs/jquery.min.js"></script>
		<!-- Bootstrap v3.1.1 -->
		<script type="text/javascript" src="${_contextpath}/assets/template6/js/libs/bootstrap.min.js"></script>
		<!-- Modernizr -->
		<script type="text/javascript" src="${_contextpath}/assets/template6/js/libs/modernizr.js"></script>
		<!-- TouchSwipe -->
		<script type="text/javascript" src="${_contextpath}/assets/template6/js/libs/jquery.touchSwipe.js"></script>
		<!-- BootstrapValidator -->
		<script type="text/javascript" src="${_contextpath}/assets/template6/js/libs/bootstrapValidator.js"></script>
		<!-- intlTelInput -->
		<script type="text/javascript" src="${_contextpath}/assets/template6/js/libs/intlTelInput.js"></script>
 		<!-- bootstrap-hover-dropdown -->
		<script type="text/javascript" src="${_contextpath}/assets/template6/js/libs/bootstrap-hover-dropdown.js"></script>
		<!-- Bootstrap Tab Collapse -->
		<script type="text/javascript" src="${_contextpath}/assets/template6/js/libs/bootstrap-tabcollapse.js"></script>
		<!-- Bootstrap Datepicker -->
		<script type="text/javascript" src="${_contextpath}/assets/template6/js/libs/bootstrap-datepicker.js"></script>
		<!-- jQuery OwlCarousel v1.3.3 -->
		<script type="text/javascript" src="${_contextpath}/assets/template6/plugins/owl-carousel/js/owl.carousel.js"></script>
		<!-- LightBox -->
		<script type="text/javascript" src="${_contextpath}/assets/template6/plugins/lightbox/js/lightbox.js"></script>
		<!-- isValidNumber -->
		<script type="text/javascript" src="${_contextpath}/assets/template6/js/libs/isValidNumber.js"></script>
 		
 		<!-- Page JS -->
		<script type="text/javascript" src="${_contextpath}/assets/template6/js/init/webjaguar.js"></script>
	 --%>
		
	<c:set var="gCUSTOMER_FIELDS" value="${gSiteConfig['gCUSTOMER_FIELDS']}"/>
	<jsp:include page="/WEB-INF/jsp/frontend/layout/template6/headerScriptlets.jsp" />
	<c:out value="${layout.headTag}" escapeXml="false"/>
	<c:if test="${model.canonicalLink != null}">
	  <link rel="canonical" href="<c:out value="${model.canonicalLink}" />"/>
	</c:if>
  </head>  

  <body>
  <%--
  <c:set var="dynamicMenu" value="${dynamicMenu}"/>
	<c:if test="${dynamicMenu != null}">
	  <c:set value="${gSiteConfig['gMOD_REWRITE']}" var="gMOD_REWRITE"/>
	  <c:set value="${siteConfig['MOD_REWRITE_CATEGORY'].value}" var="MOD_REWRITE_CATEGORY"/>
	  <c:set value="${siteConfig['ABOUT_US_ID'].value}" var="ABOUT_US_ID"/>
	  <c:set value="${siteConfig['CONTACT_US_ID'].value}" var="CONTACT_US_ID"/>
	  
	  <!-- navbar -->
	  <nav class="navbar navbar-default" role="navigation">
	    <div class="navbar-header">
		  <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
		    <span class="sr-only">Toggle navigation</span>
		    <span class="icon-bar"></span>
		    <span class="icon-bar"></span>
		    <span class="icon-bar"></span>
		  </button>
		  <a class="navbar-brand visible-xs" href="#">Menu</a>
	    </div>
    
	    <div class="collapse navbar-collapse">
		  <ul class="nav navbar-nav">
    		<div style="display:none">
			  <%
				for (Category dynamicCategory: (List<Category>) pageContext.getAttribute("dynamicMenu")) {
					dynamicMenu(dynamicCategory, 1, out, (String) request.getAttribute("_contextpath"), (String) pageContext.getAttribute("gMOD_REWRITE"), (String) pageContext.getAttribute("MOD_REWRITE_CATEGORY")
						, (String) pageContext.getAttribute("ABOUT_US_ID"), (String) pageContext.getAttribute("CONTACT_US_ID"));
				}
			  %>
			</div>
		  </ul>
		</div>
	  </nav>
	  <!-- /navbar -->

	</c:if>
 --%>
	
    <div id="pageWrapper">
      <!-- ========================= -->
	  <!--     HEADER                -->
	  <!-- ========================= -->
	  <div id="pageHeaderWrapper">
		<div id="pageHeader">
          <c:if test="${layout.headerHtml != ''}">
	   		<c:out value="${layout.headerHtml}" escapeXml="false"/>
		  </c:if>
		</div>
		<!-- end pageHeader -->
	  </div>
	  <!-- end pageHeaderWrapper -->
	  
	  <!-- ========================= -->
	  <!--     TOP BAR               -->
	  <!-- ========================= -->
	  <div id="pageTopbarWrapper">
		<div id="pageTopbar">
		  <c:if test="${layout.topBarHtml != ''}">
	  	    <c:out value="${layout.topBarHtml}" escapeXml="false"/>
		  </c:if>
		</div>
		<!-- end pageTopbar -->
	  </div>
	  <!-- end pageTopbarWrapper -->
	  
	
	  <!-- ========================= -->
	  <!--     BODY                  -->
	  <!-- ========================= -->
	  <div id="pageBodyWrapper">
		<div id="pageBody" class="container">
		  <div class="row row-offcanvas row-offcanvas-left">
		    <!-- ========================= -->
			<!--     LEFT SIDE BAR         -->
			<!-- ========================= -->
			<div id="leftSidebarWrapper">
			  <div id="leftSidebar">
				<c:if test="${not layout.hideLeftBar}">
			  	  <tiles:insertAttribute name="leftbar" />
				</c:if>
			  </div>
			  <!-- end leftSidebar -->
			</div>
			<!-- end leftSidebarWrapper -->
			
			<!-- ========================= -->
			<!--     CONTENT               -->
			<!-- ========================= -->
			<div id="contentWrapper">
				<div id="content">
					<tiles:insertAttribute name="content" />
				</div>
				<!-- end content -->
			</div>
			<!-- end contentWrapperThreeCol -->
			
			<!--     RIGHT SIDE BAR         -->
			<!-- ========================= -->
			<div id="rightSidebarWrapper">
			  <div id="rightSidebar">
              	<tiles:insertAttribute name="rightbar" />
			  </div>
			  <!-- /rightSidebar -->
			</div>
			<!-- /rightSidebarWrapper -->    
			
		  </div>
		</div>
		<!-- end pageBody -->
	</div>
	<!-- end pageBodyWrapper -->
	
	<!-- ========================= -->
	<!--     FOOTER                -->
	<!-- ========================= -->
	<div id="pageFooterWrapper">
	  <div id="pageFooter">
		  <c:if test="${layout.footerHtml != ''}">
			<c:out value="${layout.footerHtml}" escapeXml="false"/>
		  </c:if>
	  </div>
	  <!-- end pageFooter -->
	</div>
	<!-- end pageFooterWrapper -->
	
	  <div id="pageSubFooterWrapper">
		<div id="pageSubFooter">
			<c:if test="${layout.aemFooter != ''}">
			  <c:out value="${layout.aemFooter}" escapeXml="false"/>
		    </c:if>
		</div>
		<!-- end pageSubFooter -->
	  </div>
	  <!-- end pageSubFooterWrapper -->

  </div>
  
<!-- end pageWrapper -->
</body>
</html>


<%--
<!-- navbar -->
<nav class="navbar navbar-default" role="navigation">
  <div class="navbar-header">
	<button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
	  <span class="sr-only">Toggle navigation</span>
	  <span class="icon-bar"></span>
	  <span class="icon-bar"></span>
	  <span class="icon-bar"></span>
	</button>
	<a class="navbar-brand visible-xs" href="#">Menu</a>
  </div>
  <div class="collapse navbar-collapse">
	<ul class="nav navbar-nav">
	  <li><a href="/home.jhtm">Home</a></li>
	  <li><a href="/account.jhtm">My Account</a></li>
	  <li><a href="/faq.jhtm">FAQ</a></li>
	  <li><a href="/policy.jhtm">Policy</a></li>
	  <li><a href="/viewCart.jhtm">View Cart</a></li>
	  <li class="dropdown">
	    <a href="#" class="dropdown-toggle" data-toggle="dropdown">Products <b class="caret"></b></a>
		<ul class="dropdown-menu">
		  <li><a href="products.html">All Products</a></li>
		  <li><a href="product_details.html">Product Details</a></li>
		  <li class="dropdown-submenu">
		    <a href="#">Level 2</a>
			<ul class="dropdown-menu">
			  <li class="dropdown-submenu">
			    <a href="#">Level 3</a>
				<ul class="dropdown-menu">
				  <li><a href="#">Level 4</a></li>
				</ul>
			  </li>
			</ul>
		  </li>
		</ul>
	  </li>
	</ul>
  </div>
</nav>
<!-- /navbar -->
 --%>






<%!
void dynamicMenu(Category category, int level, JspWriter out, String _contextpath, String gMOD_REWRITE, String MOD_REWRITE_CATEGORY
		,String ABOUT_US_ID,String CONTACT_US_ID) throws Exception {	
	
	out.print("<li");
	if (level == 1 && category.getSubCategories().size() > 0) {
		out.print(" class=\"dropdown\"");
	} else if(level == 2 && category.getSubCategories().size() > 0) {
		out.print(" class=\"dropdown-submenu\"");
	}
	out.print(">\n");
	
	if(category.getSubCategories().size() > 0) {
		out.print("<a href=\"#\" class=\"dropdown-toggle\" data-toggle=\"dropdown\">"+category.getName()+" <b class=\"caret\"></b></a>");
	} else {
		if (gMOD_REWRITE != null && gMOD_REWRITE.equals("1")) {
			out.print("<a href=\""+MOD_REWRITE_CATEGORY + "/" + category.getId() + "/" + category.getEncodedName() + ".html\""+category.getName()+"</a>");				
		} else {
			out.print("<a href=\"category.jhtm?cid="+ category.getId()+"\">"+category.getName()+"</a>" );
		}
		
	}
	
	if(level > 1) {
		out.print("<ul class=\"dropdown-menu\">");
		for (Category thisCategory: category.getSubCategories()) {
			dynamicMenu(thisCategory, level+1, out, _contextpath, gMOD_REWRITE, MOD_REWRITE_CATEGORY, ABOUT_US_ID, CONTACT_US_ID);
		}
		out.print("</ul>");
	}
	
	out.println("</li>");
}
%>