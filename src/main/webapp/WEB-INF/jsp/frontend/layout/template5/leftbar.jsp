<%@ page import="com.webjaguar.model.Category,com.webjaguar.model.CategoryLinkType,java.util.*" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<c:if test="${(model != null) && (model.mainCategories != null)}">
<c:out value="${layout.leftBarTopHtml}" escapeXml="false"/>

<link rel="stylesheet" href="${_contextpath}/assets/leftBar/T5/menu.css" type="text/css" media="screen" />
<script src="http://ajax.googleapis.com/ajax/libs/jquery/1/jquery.min.js"></script>
<script type="text/javascript">
<!--
// put it in try catch as next click throws exception here
try{
	$.noConflict();
}catch(err){}
jQuery(document).ready(function($) {
  // Handler for .ready() called.
  $(".leftNavWrapper li.cat").click(function(e) {
    $(this).toggleClass('hover', e.type == 'mouseenter');
  });
 
 
  $(".leftNavWrapper li.cat div").html("");
 
  $(".leftNavWrapper li.cat").click(function() {
    // hide previosuly opened divs
	$( ".leftNavWrapper li.cat div.sub" ).each(function() {
      $(this).hide();
    });
    var _this = $(this);
    $('div.leftNavWrapper').find("div").hide();
   
    // stop propagation, if clicked on a tag
    _this.children("a").click(function(event){
      event.stopPropagation();
    });
   
    var url = _this.children("div").data("contentUrl"); 
    $.get(url, function(getHtml) {
      console.log(getHtml);
	   _this.closest("li").find("div").html(getHtml);
    });
  
    _this.closest("li").find("div").show(10);
    _this.closest("li").find("div").mouseleave(function () {
      _this.closest("li").find("div").fadeOut();
    });
  
    return false;
  });
  $(".leftNavWrapper").mouseleave(function(){  
    $('div.leftNavWrapper').find("div").hide(); //hide the button
  });
});
//-->
</script>

<c:set value="${model.mainCategories}" var="mainCategories"/>
<c:set value="${gSiteConfig['gMOD_REWRITE']}" var="gMOD_REWRITE"/>
<c:set value="${siteConfig['MOD_REWRITE_CATEGORY'].value}" var="MOD_REWRITE_CATEGORY"/>
<c:set value="${_leftBar}" var="LEFTBAR_DEFAULT_TYPE"/>
<% 
	buildTree5((ArrayList<Category>) pageContext.getAttribute("mainCategories"), 0, out, (String) request.getAttribute("_contextpath"), (String) pageContext.getAttribute("gMOD_REWRITE"), (String) pageContext.getAttribute("MOD_REWRITE_CATEGORY"), 0);	
%>
</c:if>

<c:out value="${layout.leftBarBottomHtml}" escapeXml="false"/>

<%!
void buildTree5(List<Category> categories, int level, JspWriter out, String _contextpath, String gMOD_REWRITE, String MOD_REWRITE_CATEGORY, int cid) throws Exception {	
	out.print("<ul class=\"leftNavWrapper\">");
	for (Category thisCategory: categories) {
		out.print("<li class=\"cat\" id=\"li_cid_" + thisCategory.getId() + "\"><span>");
		if (!thisCategory.getLinkType().equals(CategoryLinkType.NONLINK)) {
			out.print("<a href=\"" + _contextpath + "/");
			if (gMOD_REWRITE != null && gMOD_REWRITE.equals("1")) {
				out.print(MOD_REWRITE_CATEGORY + "/" + thisCategory.getId() + "/" + thisCategory.getEncodedName() + ".html");				
			} else {
				out.print("category.jhtm?cid=" + thisCategory.getId());
			}				
			out.print("\"");
			if (thisCategory.getUrlTarget() != null && thisCategory.getUrlTarget().equals("_blank")) {
				out.print(" target=\"_blank\"");
			}
			out.print(" class=\"leftbar_catLink\" >");
		}
		out.print(thisCategory.getName());
		if (!thisCategory.getLinkType().equals(CategoryLinkType.NONLINK)) {
			out.print("</a>");	
		}
		System.out.println("");
		out.print("</span><span class=\"menu-arrow\"></span><div class=\"sub\" data-content-url=\"");
		out.print(thisCategory.getField5() + "\"></div>");
		out.println("</li>");
   	}
	out.println("</ul>");
}
%>