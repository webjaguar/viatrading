<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>

<tiles:insertDefinition name="${_template}" flush="true">
  <tiles:putAttribute name="content" type="string">
<c:if test="${gSiteConfig['gBUY_REQUEST']}">
<style type="text/css"> 
.buyRequest{ border:1px solid #E0E0E0;padding:10px;margin:10px;}
.buyRequest{ border:1px solid #E0E0E0;padding:10px;margin:10px;}
.admin {background-color:#DDDDDD;}
.user {background-color:#CCDDFF;}
pre {font-family: Verdana,Arial,Helvetica,sans-serif;}
</style>
<script type="text/javascript">
<!--
function toggleMe(id) {
	
	if (document.getElementById(id).style.display == 'none')
		document.getElementById(id).style.display = 'block';
	else
		document.getElementById(id).style.display = 'none';		
}

//-->
</script>
<c:if test="${model.message != null}">
  <div class="message"><fmt:message key="${model.message}" /></div>
</c:if>

<div style="border-top:1px dotted #DDDDDD;margin:8px 0 5px 10px; font-weight:700;color:#DB0B2B"><fmt:message key="buyRequest" /></div>
<c:if test="${buyRequestForm.buyRequest != null}">
   <div class="buyRequest admin">
   <div style="font-weight:700;"><c:out value="${model.owner.address.firstName}"/> (<fmt:message key="buyRequest" />):</div>
   <i><fmt:formatDate type="date" dateStyle="full" value="${buyRequestForm.buyRequest.created}" pattern="MM/dd/yyyy (hh:mm)a zz "/></i><br /><br />
   <table  border="0" style="width:100%">
   <tr valign="top">
    <td>
     <div class="name"><c:out value="${buyRequestForm.buyRequest.name}" escapeXml="false"/></div>
     <div class="shortDesc"><c:out value="${buyRequestForm.buyRequest.shortDesc}" escapeXml="false"/></div></td>
    <td><div class="longDesc"><pre><c:out value="${buyRequestForm.buyRequest.longDesc}" escapeXml="false"/></pre></div></td>
   </tr>
   </table>
   </div>
</c:if>

<c:choose>
 <c:when test="${model.buyRequestVersions != null}">
   <div style="border-top:1px dotted #DDDDDD; margin:8px 0 5px 10px; font-weight:700;color:#DB0B2B"><fmt:message key="buyReply" /></div>
 <c:forEach items="${model.buyRequestVersions}" var="versions">
   <div class="buyRequest <c:choose><c:when test="${buyRequestForm.buyRequest.ownerId != versions.userId }">user</c:when><c:otherwise>admin</c:otherwise></c:choose>">
   <c:if test="${buyRequestForm.buyRequest.status == 'open' and model.userSession.userid != versions.userId}" >
     <div style="padding:10px;border:1px solid #999999; float:right;width:100px;"><span style="cursor:pointer;cursor:hand" onclick="toggleMe(${versions.id});" >Reply</span></div>
   </c:if>
   <div style="font-weight:700;"><c:out value="${versions.userName}"/> (<fmt:message key="buyReply" />):</div>
   <i><fmt:formatDate type="date" dateStyle="full" value="${versions.created}" pattern="MM/dd/yyyy (hh:mm)a zz "/></i><br /><br />
   <pre><c:out value="${versions.dialog}" escapeXml="false"/></pre><br />
   <div class="reply" >
     <form:form  commandName="buyRequestForm" method="post">
	 <div id="<c:out value="${versions.id}"/>" style="width:100%;display: none;">
	  <input type="hidden" name="id" value="<c:out value='${buyRequestForm.buyRequest.id}'/>" />
	  <input type="hidden" name="oid" value="<c:out value='${versions.userId}'/>" />
	  <c:if test="${buyRequestForm.buyRequest.status == 'open' }">
	  <div style="border-top:1px dotted #DDDDDD; margin:8px 0 5px 10px; font-weight:700;color:#DB0B2B"><fmt:message key="addComment" /></div>
	  <table border="0" cellpadding="5" cellspacing="5" width="100%">  
	    <tr>
	      <td><form:textarea path="buyRequestVersion.dialog" rows="8" cols="50" htmlEscape="true"/><form:errors path="buyRequestVersion.dialog" cssClass="error"/></td>
	    </tr>  
	  </table>
	  <div style="padding-top: 10px">
	    <input type="submit" name="__update" value="<fmt:message key="update" />">
	  </div>
	  </c:if> 
	</div>
	</form:form>
   </div>
   </div>
 </c:forEach>
 <c:choose>
  <c:when test="${model.buyRequestVersionsSize < 1}">
	<form:form  commandName="buyRequestForm" method="post">
	<div style="width:100%;">
	<input type="hidden" name="id" value="<c:out value='${buyRequestForm.buyRequest.id}'/>" />
	<input type="hidden" name="oid" value="<c:out value='${buyRequestForm.buyRequest.ownerId}'/>" />
	<c:if test="${buyRequestForm.buyRequest.status == 'open' }">
	<div style="border-top:1px dotted #DDDDDD; margin:8px 0 5px 10px; font-weight:700;color:#DB0B2B"><fmt:message key="addComment" /></div>
	<table border="0" cellpadding="5" cellspacing="5" width="100%">  
	  <tr>
	    <td><form:textarea path="buyRequestVersion.dialog" rows="8" cols="50" htmlEscape="true"/><form:errors path="buyRequestVersion.dialog" cssClass="error"/></td>
	  </tr>  
	</table>
	<div style="padding-top: 10px">
	  <input type="submit" name="__update" value="<fmt:message key="update" />">
	</div> 
	</c:if> 
	</div>
	</form:form>
  </c:when>
  <c:otherwise>
    <c:if test="${buyRequestForm.buyRequest.ownerId == model.userSession.userid}">
    <div style="border-top:1px dotted #DDDDDD; margin:40px 0 5px 10px; font-weight:700;color:#DB0B2B">
	  <fmt:message key="close" /><br /><br />
	  <input type="submit" name="__close" value="<fmt:message key="close" />" onClick="return confirm('Close this permanently?')">
	</div>
	</c:if>
  </c:otherwise>
 </c:choose>
 </c:when>
 <c:otherwise>
    <div>Error Message</div>
 </c:otherwise>
</c:choose>

</c:if>
  </tiles:putAttribute>
</tiles:insertDefinition>
