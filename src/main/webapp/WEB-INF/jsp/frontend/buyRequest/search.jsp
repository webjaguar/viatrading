<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>

<tiles:insertDefinition name="${_template}" flush="true">
  <tiles:putAttribute name="content" type="string">
<c:if test="${gSiteConfig['gBUY_REQUEST']}">
<form action="brSearch.jhtm" method="post">
<c:if test="${model.searchTree == null}">
<input type="hidden" name="category" value="<c:out value="${buyRequestSearch.category}"/>" />
</c:if>
<table border="0" cellspacing="0" cellpadding="5" align="center">
  <tr>
    <td><fmt:message key="keywords" />: </td>
    <td><input type="text" name="keywords" value="<c:out value="${buyRequestSearch.keywords}"/>" size="50">
    	<c:if test="${not model.minCharsMet}">
    		<div style="font-weight:bold;color:#990000">
    		  <fmt:message key="f_keywordsMinCharsError">
				<fmt:param><c:out value="${siteConfig['SEARCH_KEYWORDS_MINIMUM_CHARACTERS'].value}" /></fmt:param>
			  </fmt:message>
    	</c:if>
    	<c:if test="${model.hasKeywordsLessMinChars and model.minCharsMet}">
    		<div style="font-weight:bold;color:#990000">
    		  <fmt:message key="f_keywordsMinCharsIgnore">
				<fmt:param><c:out value="${siteConfig['SEARCH_KEYWORDS_MINIMUM_CHARACTERS'].value}" /></fmt:param>
			  </fmt:message>
    	</c:if>
    </td>
  </tr>
  <c:if test="${model.searchTree != null}">
  <tr>
    <td><fmt:message key="category" />: </td>
    <td>
	  <select name="category" style="width:200px">
		<option value=""><fmt:message key="allCategories"/></option>
		<c:forEach items="${model.searchTree}" var="category">
		<option value="${category.id}" <c:if test="${productSearch.category == category.id}">selected</c:if>><c:out value="${category.name}" escapeXml="false" /></option>
		</c:forEach>
      </select>  
	</td>
  </tr>  
  <tr>
    <td>&nbsp;</td>
    <td>
	  <input name="includeSubs" type="checkbox" <c:if test="${buyRequestSearch.includeSubs}">checked</c:if>> <fmt:message key="includeSubs" />
	</td>
  </tr>
  </c:if>
<%@ include file="/WEB-INF/jsp/frontend/searchCustom.jsp" %>
  <tr>
    <td colspan="2" align="right"><input type="submit" value="Search"></td>
  </tr>
</table>
</form>

<c:if test="${model.minCharsMet}">
<div class="searchTitle"><fmt:message key="f_searchResults"/>:</div>

<c:if test="${model.count == 0}">Nothing found</c:if>
</c:if>
<c:if test="${model.count > 0}">
<c:set var="pageShowing">
<table border="0" cellpadding="0" cellspacing="1" width="100%">
  <tr>
  <td class="pageShowing">
  <fmt:message key="showing">
	<fmt:param value="${buyRequestSearch.offset + 1}"/>
	<fmt:param value="${model.pageEnd}"/>
	<fmt:param value="${model.count}"/>
  </fmt:message>
  </td>
  <td class="pageNavi">
<form action="search.jhtm" method="get">
Page 
<c:choose>
<c:when test="${model.pageCount <= 100 and model.pageCount > 0}">
	    <select name="page" id="page" onchange="submit()">
		<c:forEach begin="1" end="${model.pageCount}" var="page">
  	        <option value="${page}" <c:if test="${page == (buyRequestSearch.page)}">selected</c:if>>${page}</option>
		</c:forEach>
		</select>
</c:when>
<c:otherwise>
		<input type="text" id="page" name="page" value="${buyRequestSearch.page}" size="5" class="textfield50" />
		<input type="submit" value="go"/>
</c:otherwise>
</c:choose>
of <c:out value="${model.pageCount}"/>
| 
<c:if test="${buyRequestSearch.page == 1}"><span class="pageNaviDead"><fmt:message key="f_previous" /></span></c:if>
<c:if test="${buyRequestSearch.page != 1}"><a href="search.jhtm?page=${buyRequestSearch.page-1}" class="pageNaviLink"><fmt:message key="f_previous" /></a></c:if>
| 
<c:if test="${buyRequestSearch.page == model.pageCount}"><span class="pageNaviDead"><fmt:message key="f_next" /></span></c:if>
<c:if test="${buyRequestSearch.page != model.pageCount}"><a href="brSearch.jhtm?page=${buyRequestSearch.page+1}" class="pageNaviLink"><fmt:message key="f_next" /></a></c:if>
<select name="size" onchange="document.getElementById('page').value=1;submit()">
<c:forTokens items="10,25,50,100" delims="," var="current">
 <option value="${current}" <c:if test="${current == buyRequestSearch.pageSize}">selected</c:if>>${current} <fmt:message key="perPage" /></option>
</c:forTokens>
</select>
</form>
  </td>
  </tr>
</table>
</c:set>
<c:out value="${pageShowing}" escapeXml="false" />
</c:if>


<table border="0" cellpadding="3" cellspacing="1" align="center" class="quickmode">
<c:forEach items="${model.buyRequests}" var="buyRequest" varStatus="status">

<c:if test="${status.first}">    
  <tr>
    <th class="quickmode"><fmt:message key="Name" /></th>
    <th class="quickmode"><c:out value="${siteConfig['QUICK_MODE_SHORT_DESC_TITLE'].value}"/></th>
    <th class="quickmode"><fmt:message key="action" /></th>
  </tr>
</c:if> 
  <tr class="row${status.index % 2}">
    <td><c:out value="${buyRequest.name}" /></td>
    <td><c:out value="${buyRequest.shortDesc}" /></td>
    <td><span class="link"><a href="buyRequestUpdate.jhtm?id=${buyRequest.id}" >contact</a></span>
    <span class="link"><a href="buyRequest_addToList.jhtm?br_id=${buyRequest.id}" >add to list</a></span>
    </td>
  </tr> 

</c:forEach>
</table>


<c:if test="${model.count > 0 && model.pageCount > 1}">
<c:out value="${pageShowing}" escapeXml="false" />
</c:if>

</c:if>    
  </tiles:putAttribute>
</tiles:insertDefinition>
