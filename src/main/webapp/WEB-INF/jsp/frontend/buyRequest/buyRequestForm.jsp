<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>

<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>

<tiles:insertDefinition name="${_template}" flush="true">
  <tiles:putAttribute name="content" type="string">
<c:if test="${gSiteConfig['gBUY_REQUEST']}">  
<script type="text/javascript" src="javascript/mootools-1.2.5-core.js"></script>  
<script type="text/JavaScript">
<!--
function updateCategory(id, selected) {
		var request = new Request.JSON({
			url: "json.jhtm?action=category&bid=<c:out value="${siteConfig['BUY_REQUEST_CATEGORY_ID'].value}"/>&cid=" + selected,
			onRequest: function() { 
				document.getElementById(id + "_spinner").style.display="block";
			},
			onComplete: function(jsonObj) {
				document.getElementById(id + "_spinner").style.display="none";
				var categoryIds = document.getElementById(id + "_categoryIds");
				if (jsonObj.subcats.length > 0) {
					while (categoryIds.length > 0) {
    					categoryIds.remove(0);
					} 		
					i = 0;
					jsonObj.subcats.each(function(category) {
						categoryIds.options[i++] = new Option(category.name, category.id);
					});				
					var parentId = document.getElementById(id + "_parentId");
					while (parentId.length > 1) {
	    				parentId.remove(1);
					} 
					if (jsonObj.tree.length > 0) {		
						i = 1;
						jsonObj.tree.each(function(category) {
							parentId.options[i++] = new Option(category.name, category.id, false, true);
						});				
					}
				}
			}
		}).send();
}
//-->
</script>
  
<c:if test="${model.message != null}">
  <div class="message"><fmt:message key="${model.message}" /></div>
</c:if>

<form:form  commandName="buyRequestForm" method="post">

<table width="100%" border="0" class="buyRequestForm">
  <tr>
    <td colspan="2" style="font-weight:700;color:#DD3204">
     Request to buy
    </td>
  </tr> 
  <tr>
    <td align="right"><fmt:message key="productName" /> : </td>
    <td>
	  <form:input path="buyRequest.name" cssClass="textfield" size="50" maxlength="100" htmlEscape="true"/>
	  <form:errors path="buyRequest.name" cssClass="error" />
    </td>
  </tr>   
  <tr>
    <td align="right"><fmt:message key="productShortDesc" /> : </td>
    <td>
	  <form:input path="buyRequest.shortDesc" cssClass="textfield" size="50" maxlength="512" htmlEscape="true"/>
	  <form:errors path="buyRequest.shortDesc" cssClass="error" />
    </td>
  </tr> 
  <tr>
    <td align="right" valign="top"><fmt:message key="productLongDesc" /> : </td>
    <td>
	  <form:textarea path="buyRequest.longDesc" rows="10" cols="40" cssClass="textfield" htmlEscape="true"/>
	  <form:errors path="buyRequest.longDesc" cssClass="error" />
    </td>
  </tr> 
  <tr>
    <td align="right" valign="top"><fmt:message key="keywords" /> : </td>
    <td>
	  <form:textarea path="buyRequest.keywords" rows="10" cols="40" cssClass="textfield" htmlEscape="true"/>
	  <form:errors path="buyRequest.keywords" cssClass="error" />
    </td>
  </tr> 
<c:forEach items="${buyRequestForm.parentIds}" var="parentId" varStatus="status">
  <tr>
    <td align="right">&nbsp;<fmt:message key="category" /> : </td>
    <td>
      <table>
      <tr><td>
	    <select name="parentIds" id="cat${status.index}_parentId" style="width:300px;" onchange="updateCategory('cat${status.index}', this.value)">
	      <c:forEach items="${model.tree[status.index]}" var="category" varStatus="treeStatus">
	      <c:if test="${treeStatus.first and category.id != siteConfig['BUY_REQUEST_CATEGORY_ID'].value}">
  	      <option value="<c:out value="${siteConfig['BUY_REQUEST_CATEGORY_ID'].value}"/>"><fmt:message key="categoryMain" /></option>
	      </c:if>
  	      <option value="${category.id}" <c:if test="${category.id == parentId}">selected</c:if>>
  	        <c:choose>
  	          <c:when test="${treeStatus.first and category.id == siteConfig['BUY_REQUEST_CATEGORY_ID'].value}"><fmt:message key="categoryMain" /></c:when>
  	          <c:otherwise><c:out value="${category.name}"/></c:otherwise>
  	        </c:choose>
  	      </option>
	      </c:forEach>
	    </select>
	  </td><td>
	    <img id="cat${status.index}_spinner" src="assets/Image/Layout/spinner.gif" style="display:none">
	  </td></tr>
	  </table>
    </td>
  </tr>   
  <tr>
    <td align="right">&nbsp;</td>
    <td>
	  <select name="cat${status.index}_categoryIds" id="cat${status.index}_categoryIds" size="5" style="width:300px;" onchange="updateCategory('cat${status.index}', this.value)">	  
	    <c:forEach items="${model.categories[status.index]}" var="category">
  	    <option value="${category.id}" <c:if test="${category.id == productForm.categoryIds[status.index]}">selected</c:if>><c:out value="${category.name}"/></option>
	    </c:forEach>
	  </select>
    </td>
  </tr>  
</c:forEach>      
  </table>
  
<div align="center">
<c:if test="${buyRequestForm.newBuyRequest}">
  <input type="submit" value="<fmt:message key="add" />" />
</c:if>
<c:if test="${!buyRequestForm.newBuyRequest}">
  <input type="submit" name="_update" value="<fmt:message key="update" />" />
  <input type="submit" name="_delete" value="<fmt:message key="delete" />"  onClick="return confirm('Delete permanently?')" />
</c:if>
  <input type="submit" name="_cancel" value="<fmt:message key="cancel" />" />
</div>
  
</form:form>
</c:if>
  </tiles:putAttribute>
</tiles:insertDefinition>
