<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="/WEB-INF/tlds/spring.tld" prefix="spring"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib uri="/WEB-INF/tlds/custom.tld" prefix="wj" %>

<tiles:insertDefinition name="${_template}" flush="true">
	<tiles:putAttribute name="leftbar" value="/WEB-INF/jsp/frontend/layout/template${_leftBar}/leftbar.jsp" />
	<tiles:putAttribute name="content" type="string">
<script>
function emailCheck(str){
	 if(/^[a-z0-9._%-]+@[a-z0-9.-]+\.[a-z]{2,4}$/i.test(str) == false) {
		alert('Invalid Email');
		return false;
	 }
	 return true;
}
function assignValue(ele, value) {
$(ele).value = value;
}
function phoneCheck(str){
	if (str.search(/^\d{10}$/)==-1) {
		alert("Please enter a valid 10 digit number");
		return false;
	} 
}
function saveContact(){
	try {
		if( !validateContactInfo() ) {
			return;
		}
	}catch(err){}
	
	var sku = $('sku').value;
	var name = $('name').value;
	var firstName = $('firstName').value;
	var lastName = $('lastName').value;
	var company = $('company').value;
	var address = $('address').value;
	var city = $('city').value;
	var state = $('state').value;
	var zipCode = $('zipCode').value;
	var country = $('country').value;
	var email = $('email').value;
	var phone = $('phone').value;
	var note = $('note').value;
	var request = new Request({
	   url: "${_contextpath}/insert-ajax-contact.jhtm?firstName="+firstName+"&lastName="+lastName+"&company="+company+"&address="+address+"&city="+city+"&state="+state+"&zipCode="+zipCode+"&country="+country+"&email="+email+"&phone="+phone+"&note="+note+"&sku="+sku+"&name="+name+"&leadSource=quote",
       method: 'post',
       onFailure: function(response){
           saveContactBox.close();
           $('mbQuote').style.display = 'none';
           $('successMessage').set('html', '<div style="color : RED; text-align : LEFT;">We are experincing problem in saving your quote. <br/> Please try again later.</div>');
       },
       onSuccess: function(response) {
            saveContactBox.close();
            $('mbQuote').style.display = 'none';
            $('successMessage').set('html', response);
       }
	}).send();
}
function addToQuoteList(productId){
	var requestHTMLData = new Request.HTML ({
		url: "${_contextpath}/quoteList.jhtm?productId="+productId,
		onSuccess: function(response){
			$('addToQuoteListWrapperId'+productId).set('html', '<h4><fmt:message key="f_addedToQuoteList" /></h4> <a href="${_contextpath}/quoteViewList.jhtm" rel="type:element" id="quoteListId" class="quoteList"> <input type="image" border="0" name="_viewQuoteList" class="_viewQuoteList" src="${_contextpath}/assets/Image/Layout/button_viewQuoteList.jpg" /> </a> ');
		}
	}).send();
}
//-->
</script>
		<c:if test="${model.message != null}">
			<div class="message"><fmt:message key="${model.message}" /></div>
		</c:if>
		<c:if test="${model.product != null}">
			<c:if test="${fn:trim(model.productLayout.headerHtml) != ''}">
				<c:set value="${model.productLayout.headerHtml}" var="headerHtml" />
				<%@ include file="/WEB-INF/jsp/frontend/common/headerDynamicElement.jsp"%>
				<div><c:out value="${headerHtml}" escapeXml="false" /></div>
			</c:if>
			<c:set value="${model.product}" var="product" />
			<script language="JavaScript" type="text/JavaScript">
			<c:if test="${siteConfig['DETAILS_IMAGE_WIDTH'].value != '' }" >
			function zoomIn() {
				window.open('${_contextpath}/productImage.jhtm?id=${product.id}&layout=2&imgUrl=' + document.getElementById("_image").src,'name','width=800,height=600,resizable=yes,scrollbars=1');
			}
			</c:if>
			</script>
			<c:set var="details_images_style" value="" />
			<c:set var="details_desc_style" value="" />
			<c:set var="productImage">
				<c:choose>
					<c:when
						test="${fn:contains(siteConfig['PRODUCT_IMAGE_LAYOUTS'].value,'s2') and 
 		(product.imageLayout == 's2' or (empty product.imageLayout and siteConfig['PRODUCT_IMAGE_LAYOUT'].value == 's2'))}">
						<link rel="stylesheet"
							href="${_contextpath}/assets/SmoothGallery/slideshow.css"
							type="text/css" media="screen" />
						<script type="text/javascript"
							src="${_contextpath}/javascript/slideshow.js"></script>
						<script type="text/javascript">		
	//<![CDATA[
	  window.addEvent('domready', function(){
	    var data = [<c:forEach items="${product.images}" var="image" varStatus="status">'<c:if test="${!image.absolute}">${_contextpath}/assets/Image/Product/detailsbig/</c:if><c:out value="${image.imageUrl}"/>'<c:if test="${! status.last}" >,</c:if></c:forEach> ];
	    var myShow = new Slideshow('show', data, { controller: true, height: 300, hu: '', thumbnails: true, width: 400 });
		});
	//]]>
	</script>

						<div id="show" class="slideshow"></div>
						<div id="slideshow_margin"></div>
					</c:when>
					<c:when
						test="${fn:contains(siteConfig['PRODUCT_IMAGE_LAYOUTS'].value,'mb') and 
 		(param.multibox == null or param.multibox != 'off') and
 		(product.imageLayout == 'mb' or (empty product.imageLayout and siteConfig['PRODUCT_IMAGE_LAYOUT'].value == 'mb'))}">
						<link rel="stylesheet"
							href="${_contextpath}/assets/SmoothGallery/multibox.css"
							type="text/css" media="screen" />
						<c:set value="${true}" var="multibox" />
						<script src="${_contextpath}/javascript/overlay.js"
							type="text/javascript"></script>
						<script src="${_contextpath}/javascript/multibox.js"
							type="text/javascript"></script>
						<script type="text/javascript">
	window.addEvent('domready', function(){
		var box = new multiBox('mbxwz', {overlay: new overlay()});
	});
 </script>
						<div class="details_image_box" id="details_image_boxId"
							style="${details_images_style}
	 <c:if test="${siteConfig['DETAILS_IMAGE_WIDTH'].value != '' }">width: <c:out value="${siteConfig['DETAILS_IMAGE_WIDTH'].value}"/>px;</c:if>">

						<c:forEach items="${model.product.images}" var="image"
							varStatus="status">
							<c:if test="${status.first}">
								<a
									href="<c:if test="${!image.absolute}">${_contextpath}/assets/Image/Product/detailsbig/</c:if><c:out value="${image.imageUrl}"/>"
									id="mb0" class="mbxwz" title=""><img
									src="<c:if test="${!image.absolute}">${_contextpath}/assets/Image/Product/detailsbig/</c:if><c:out value="${image.imageUrl}"/>"
									border="0"
									style="<c:if test="${siteConfig['DETAILS_IMAGE_WIDTH'].value != '' }">width: <c:out value="${siteConfig['DETAILS_IMAGE_WIDTH'].value}"/>px</c:if>"
									alt="<c:out value="${product.name}"/><c:if test="${product.shortDesc != ''}"> - <c:out value="${product.shortDesc}"/></c:if>" /></a>
							</c:if>
						</c:forEach>
						<div style="clear: both;"><c:forEach
							items="${model.product.images}" var="image" varStatus="status">
							<c:if test="${status.count > 1}">
								<a
									href="<c:if test="${!image.absolute}">${_contextpath}/assets/Image/Product/detailsbig/</c:if><c:out value="${image.imageUrl}"/>"
									id="mb${status.count - 1}" class="mbxwz" title=""><img
									src="<c:if test="${!image.absolute}">${_contextpath}/assets/Image/Product/detailsbig/</c:if><c:out value="${image.imageUrl}"/>"
									border="0" class="details_thumbnail"
									alt="<c:out value="${product.name}"/><c:if test="${product.shortDesc != ''}"> - <c:out value="${product.shortDesc}"/></c:if>" /></a>
							</c:if>
						</c:forEach></div>
						</div>
					</c:when>
					<c:when
						test="${fn:contains(siteConfig['PRODUCT_IMAGE_LAYOUTS'].value,'qb') and 
 		(product.imageLayout == 'qb' or (empty product.imageLayout and siteConfig['PRODUCT_IMAGE_LAYOUT'].value == 'qb'))}">
						<link rel="stylesheet"
							href="${_contextpath}/assets/SmoothGallery/QuickBox/quickbox.css"
							type="text/css" media="screen" />
						<c:set value="${true}" var="quickbox" />
						<script src="${_contextpath}/javascript/QuickBox.js"
							type="text/javascript">
						</script>
						<script type="text/javascript">
							window.addEvent('domready', function(){
							new QuickBox();
							});
 						</script>
						<div class="details_image_box" id="details_image_boxId" style="${details_images_style} <c:if test="${siteConfig['DETAILS_IMAGE_WIDTH'].value != '' }">width: <c:out value="${siteConfig['DETAILS_IMAGE_WIDTH'].value}"/>px;</c:if>">
						<c:forEach items="${model.product.images}" var="image"
							varStatus="status">
							<c:if test="${status.first}">
								<a
									href="<c:if test="${!image.absolute}">${_contextpath}/assets/Image/Product/detailsbig/</c:if><c:out value="${image.imageUrl}"/>"
									id="" class="" title="" rel="quickbox"><img
									src="<c:if test="${!image.absolute}">${_contextpath}/assets/Image/Product/detailsbig/</c:if><c:out value="${image.imageUrl}"/>"
									border="0"
									style="<c:if test="${siteConfig['DETAILS_IMAGE_WIDTH'].value != '' }">width: <c:out value="${siteConfig['DETAILS_IMAGE_WIDTH'].value}"/>px</c:if>"
									alt="<c:out value="${product.name}"/><c:if test="${product.shortDesc != ''}"> - <c:out value="${product.shortDesc}"/></c:if>" /></a>
							</c:if>
						</c:forEach>
						<div style="clear: both;"><c:forEach
							items="${model.product.images}" var="image" varStatus="status">
							<c:if test="${status.count > 1}">
								<a
									href="<c:if test="${!image.absolute}">${_contextpath}/assets/Image/Product/detailsbig/</c:if><c:out value="${image.imageUrl}"/>"
									id="qb${status.count - 1}" class="" title="" rel="quickbox"><img
									src="<c:if test="${!image.absolute}">${_contextpath}/assets/Image/Product/detailsbig/</c:if><c:out value="${image.imageUrl}"/>"
									border="0" class="details_thumbnail"
									alt="<c:out value="${product.name}"/><c:if test="${product.shortDesc != ''}"> - <c:out value="${product.shortDesc}"/></c:if>" /></a>
							</c:if>
						</c:forEach></div>
						</div>
					</c:when>
					<c:otherwise>
						<div class="details_image_box" id="details_image_boxId" style="${details_images_style} <c:if test="${siteConfig['DETAILS_IMAGE_WIDTH'].value != '' }">width: <c:out value="${siteConfig['DETAILS_IMAGE_WIDTH'].value}"/>px;</c:if>">
						<c:forEach items="${model.product.images}" var="image"
							varStatus="status">
							<c:if test="${status.first}">
								<img
									src="<c:if test="${!image.absolute}">${_contextpath}/assets/Image/Product/detailsbig/</c:if><c:out value="${image.imageUrl}"/>"
									border="0" id="_image" name="_image" class="details_image"
									style="<c:if test="${siteConfig['DETAILS_IMAGE_WIDTH'].value != '' }">width: <c:out value="${siteConfig['DETAILS_IMAGE_WIDTH'].value}"/>px</c:if>"
									alt="<c:out value="${product.name}"/><c:if test="${product.shortDesc != ''}"> - <c:out value="${product.shortDesc}"/></c:if>">
								<c:if test="${siteConfig['DETAILS_IMAGE_WIDTH'].value != '' }">
								</c:if>
							</c:if>

							<c:if test="${status.last and (status.count != 1)}">
								<br>
								<c:forEach items="${model.product.images}" var="thumb"
									varStatus="thumbstatus">
									<c:if test="${thumbstatus.index < 4}">
										<a href="#" class="details_thumbnail_anchor"
											onClick="_image.src='<c:if test="${!thumb.absolute}">${_contextpath}/assets/Image/Product/detailsbig/</c:if><c:out value="${thumb.imageUrl}"/>';return false;">
										<img
											src="<c:if test="${!thumb.absolute}">${_contextpath}/assets/Image/Product/detailsbig/</c:if><c:out value="${thumb.imageUrl}"/>"
											class="details_thumbnail"
											alt="<c:out value="${product.name}"/><c:if test="${product.shortDesc != ''}"> - <c:out value="${product.shortDesc}"/></c:if>">
										</a>
									</c:if>
								</c:forEach>
								<div class="moreImageWrapper" align="right"><a
									onclick="zoomIn()" title="View More Images"><span>View
								More Images</span></a></div>
							</c:if>
						</c:forEach></div>
						<c:if test="${fn:contains(siteConfig['PRODUCT_IMAGE_LAYOUTS'].value,'ss') and (product.imageLayout == 'ss'  or (empty product.imageLayout and siteConfig['PRODUCT_IMAGE_LAYOUT'].value == 'ss'))}">
							<object classid="clsid:D27CDB6E-AE6D-11cf-96B8-444553540000"
								codebase="http://download.macromedia.com/pub/shockwave/cabs/flash/swflash.cab#version=6,0,29,0"
								width="750" height="475">
								<param name="movie"
									value="${_contextpath}/assets/Flash/slideshow.swf?file=thumbnailer1.jhtm?id=${product.id}">
								<param name="quality" value="high">
								<embed
									src="${_contextpath}/assets/Flash/slideshow.swf?file=thumbnailer1.jhtm?id=${product.id}"
									quality="high"
									pluginspage="http://www.macromedia.com/go/getflashplayer"
									type="application/x-shockwave-flash" width="750" height="475"></embed>
							</object>
						</c:if>
					</c:otherwise>
				</c:choose>
			</c:set>


			<%-- bread crumbs --%>
			<c:import url="/WEB-INF/jsp/frontend/layout/${_template}/breadcrumbs.jsp" />
			<div class="details_product_container">
				<c:if test="${siteConfig['SHOW_SKU'].value == 'true' and product.sku != null}">
					<div class="details_product_container_productsku"><c:out value="${product.sku}" /></div>
				</c:if>
				<div class="product_detail_price">
					<c:choose>
						<c:when test="${product.salesTag != null and !product.salesTag.percent}">
						  <%@ include file="/WEB-INF/jsp/frontend/common/price3.jsp" %>
						</c:when>
						<c:otherwise>
						  <%@ include file="/WEB-INF/jsp/frontend/common/price.jsp" %>
						</c:otherwise>
					</c:choose>
				</div>
				<div class="details_product_addtocart_container">
					<form action="${_contextpath}/addToCart.jhtm" name="this_form" method="post" onsubmit="return checkForm()">
					<div class="details_product_addtocart_button">
					<c:if test="${ (gSiteConfig['gSHOPPING_CART'] and (!empty product.price or product.priceByCustomer)) and (!product.loginRequire or userSession != null)}">
						<input type="hidden" name="product.id" value="${product.id}">
						<div class="details_addInput"><div class="quantity">Quantity: </div><input class="details_product_addtocart_input" type="text" name="quantity_${product.id}" id="quantity_${product.id}" value="1"></div>
						<div class="details_addButton">
						<c:set var="zeroPrice" value="false" /> 
						<c:forEach items="${product.price}" var="price">
							<c:if test="${price.amt == 0}">
								<c:set var="zeroPrice" value="true" />
							</c:if>
						</c:forEach> 
						<c:choose>
							<c:when test="${product.htmlAddToCart != null and product.htmlAddToCart != ''}">
								<c:out value="${product.htmlAddToCart}" escapeXml="false" />
							</c:when>
							<c:otherwise>
								<input type="image" border="0" name="_addtocart" class="_addtocart" src="${_contextpath}/assets/Image/Layout/button_addtocart.gif" />
							</c:otherwise>
						</c:choose> 
						<c:if test="${gSiteConfig['gINVENTORY'] and product.inventory != null and product.inventory < product.lowInventory}">
							<c:out value="${siteConfig['LOW_STOCK_TITLE'].value}" escapeXml="false" />
						</c:if>
						<c:if test="${gSiteConfig['gINVENTORY'] and product.inventory != null and !empty siteConfig['INVENTORY_ON_HAND'].value}" >
                          <div class="inventory_onhand_wrapper">
                            <div class="inventory_onhand"><c:out value="${siteConfig['INVENTORY_ON_HAND'].value}" escapeXml="false"/>&nbsp;<span><c:out value="${product.inventory}" /></span></div></td>     
                          </div>
                         </c:if>
						</div>
						<div style="clear:both;"></div>
					</c:if>			
					</div>
					
					</form>
				</div>
				<c:if test="${siteConfig['PRODUCT_QUOTE'].value == 'true' and product.quote}">
					<link rel="stylesheet" href="${_contextpath}/assets/SmoothGallery/multiboxASI.css" type="text/css" media="screen" />
					<c:if test="${!multibox}">
					 <script src="${_contextpath}/javascript/overlay.js" type="text/javascript" ></script>
					 <script src="${_contextpath}/javascript/multibox.js" type="text/javascript" ></script>
					</c:if>
					<script type="text/javascript">
					window.addEvent('domready', function(){			
						saveContactBox = new multiBox('mbQuote', {showControls : false, useOverlay: false, showNumbers: false });
					});
					</script>

				  <div class="addToQuoteWrapper" id="addToQuoteWrapperId${product.id}">
				  <a href="#${product.id}" rel="type:element" id="mbQuote" class="mbQuote">
				  	<input type="image" border="0" name="_addtocart" class="_addtocart" src="${_contextpath}/assets/Image/Layout/button_addtoquote${_lang}.gif" />
				  </a> 
				  </div>
				  <div class="addToQuoteListWrapper" id="addToQuoteListWrapperId${product.id}">
				  	<input type="image" border="0" name="addtoquoteList" id="addtoquoteList" class="_addtoquoteList" value="${product.id}" src="${_contextpath}/assets/Image/Layout/button_addToQuoteList.jpg" onClick="addToQuoteList('${product.id}');" /> 	
				  </div>
				  <div id="${product.id}" class="miniWindowWrapper mbPriceQuote" style="height:700px">
					<form autocomplete="off" onsubmit="saveContact(); return false;">
					<input type="hidden" value="${product.sku}" id="sku"/>
					<input type="hidden" value="${product.name}" id="name"/>
					
				  	<div class="header">Please provide your contact information</div>
					<fieldset class="top">
						<label class="AStitle"><fmt:message key="firstName"/></label>
						<input name="firstName" id="firstName" type="text" size="15" onkeyup="assignValue('firstName', this.value);"/>
					</fieldset>
					<fieldset class="bottom">
						<label class="AStitle"><fmt:message key="lastName"/></label>
						<input name="lastName" id="lastName" type="text" size="15" onkeyup="assignValue('lastName', this.value);"/>
					</fieldset>
					<fieldset class="bottom">
						<label class="AStitle"><fmt:message key="company"/></label>
						<input name="company" id="company" type="text" size="15" onkeyup="assignValue('company', this.value);"/>
					</fieldset>
					<fieldset class="bottom">
						<label class="AStitle"><fmt:message key="address"/></label>
						<input name="address" id="address" type="text" size="15" onkeyup="assignValue('address', this.value);"/>
					</fieldset>
					<fieldset class="bottom">
						<label class="AStitle"><fmt:message key="city"/></label>
						<input name="city" id="city" type="text" size="15" onkeyup="assignValue('city', this.value);"/>
					</fieldset>
					<fieldset class="bottom">
						<label class="AStitle"><fmt:message key="state"/></label>
						<input name="state" id="state" type="text" size="15" onkeyup="assignValue('state', this.value);"/>
					</fieldset>
					<fieldset class="bottom">
						<label class="AStitle"><fmt:message key="zipCode"/></label>
						<input name="zipCode" id="zipCode" type="text" size="15" onkeyup="assignValue('zipCode', this.value);"/>
					</fieldset>
					<fieldset class="bottom">
						<label class="AStitle"><fmt:message key="country"/></label>
						<input name="country" id="country" type="text" size="15" onkeyup="assignValue('country', this.value);"/>
					</fieldset>
					<fieldset class="bottom">
						<label class="AStitle"><fmt:message key="email"/></label>
						<input name="email" id="email" type="text" size="15" onkeyup="assignValue('email', this.value);"/>
					</fieldset>
					<fieldset class="bottom">
						<label class="AStitle"><fmt:message key="phone"/></label>
						<input name="phone" id="phone" type="text" size="15" onkeyup="assignValue('phone', this.value);"/>
					</fieldset>
					<fieldset class="bottom">
						<label class="AStitle"><fmt:message key="message"/></label>
						<textarea name="note" id="note" onkeyup="assignValue('note', this.value);"></textarea>
					</fieldset>
					<fieldset>
						<div id="button">
							<input type="submit" value="Send"/>
						</div>
					</fieldset>
					</form>
				  </div>
				  <div id="successMessage"></div>	
				</c:if>
				<c:forEach items="${product.productFields}" var="productField" varStatus="row">
					<c:if test="${row.first}">
						<c:if test="${siteConfig['PRODUCT_FIELDS_TITLE'].value != ''}">
							<div class="details_fields_title"><c:out value="${siteConfig['PRODUCT_FIELDS_TITLE'].value}"/></div>
						</c:if>
						<table class="details_fields">
					</c:if>
						<tr>
							<td class="details_field_name_row${row.index % 2}"><c:out value="${productField.name}" escapeXml="false" /> </td>
						<c:choose>
						<c:when test="${productField.formatType != null and productField.formatType != '' and productField.fieldType == 'number' }">
						<td class="details_field_value_row${row.index % 2}"><fmt:formatNumber value="${wj:trimAndSplit(productField.value, '|' , false, ',', true, true)}" pattern="${productField.formatType }" /></td>
						</c:when>
						<c:otherwise>
						<td class="details_field_value_row${row.index % 2}"><c:out value="${wj:trimAndSplit(productField.value, '|' , false, ',', true, true)}" escapeXml="false"/></td>
						</c:otherwise>
						</c:choose>						
						</tr>
					<c:if test="${row.last}">
						</table>
					</c:if>
				</c:forEach>
				<div class="details_product_long_desc_container">
					<c:out value="${product.longDesc}" escapeXml="false" />
				</div>
				<c:set var="productImageUrl" value=""/>
				<c:forEach items="${model.product.images}" var="image"	varStatus="status">
					<c:if test="${status.first}">
						<c:set var="productImageUrl" value="${image.imageUrl}"/>
					</c:if>
	 			</c:forEach>
	 			<c:if test="${product.shortDesc != '' or productImageUrl != ''}">
					<div class="details_product_image_desc_container">
						<c:if test="${productImageUrl != ''}">
							<div class="details_product_image_desc_container_image">
					  	  				<img class="details_product_image_desc_container_imageclass" alt="<c:out value="${product.name}"/>"  src="${_contextpath}/assets/Image/Product/detailsbig/<c:out value="${productImageUrl}"/>" />
			 			  	</div>
		 			  	</c:if>
		 			  	<div class="details_product_short_desc_container">
			  	  	  	<c:if test="${product.shortDesc != ''}">
			  	  	  		<c:out value="${product.shortDesc}"/>
			  	  	  	</c:if>
			  	      </div>
		 		  </div>
	 		   </c:if>
			</div>				
		</c:if>

	</tiles:putAttribute>
</tiles:insertDefinition>