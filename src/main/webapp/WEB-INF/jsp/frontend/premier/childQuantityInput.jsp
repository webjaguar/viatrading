<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
  <c:choose>
    <c:when test="${empty model.childProductsList}">
      <div id="child_sku_step" class="custom_step">
  		<c:set var="stepIndex" value="${stepIndex + 1}" />
  		<span class="number custom_step_number"><c:out value="${stepIndex}" /></span>
		<div class="hdr"><fmt:message key="quantity" /></div>
		  <div class="option_wrapper">
		    <div class="help"> 
		      <label>Provide Quantity</label>
		    </div>
		    <div class="option_selection">
		      <input type="text" size="5" maxlength="5" class="quantityBox" name="quantity_${model.product.id}" id="quantity_${model.product.id}" value="${model.productQtyMap[model.product.id]}"> <c:if test="${model.product.packing != ''}"> <c:out value="${model.product.packing}"/> </c:if>
	 		  <input type="hidden" value="${model.product.id}" name="product.id" id="product.id"  class="optionSelect"/>
	          <c:set var="showAddToCart" value="true" />
    	    </div>
		  </div>
  		  <div class="clear"></div>
  		</div>
    </c:when>
    <c:otherwise>
      <c:choose>
        <c:when test="${model.product.productLayout == '018' or (model.product.productLayout == '' and siteConfig['PRODUCT_DEFAULT_DETAIL_LAYOUT'].value == '018')}">
          <!-- Product Selection Starts -->
          <div id="child_sku_step" class="custom_step">
  		    <c:set var="stepIndex" value="${stepIndex + 1}" />
  			<span class="number custom_step_number"><c:out value="${stepIndex}" /></span>
		    <div class="hdr"><fmt:message key="f_select" /> <fmt:message key="product" /></div>
		    <div class="option_wrapper">
		      <div class="help"> 
		        <label>Select Product</label>
		      </div>
		      <div class="option_selection">
		        <select name="childSelect" onchange="updateChildProduct(this.value)" class="optionSelect" id="childSelect">
			  	  <option value="">Please Select</option>
				  <c:forEach items="${model.childProductsList}" var="slaveProduct" varStatus="statusPrice">
			  	    <c:if test="${(!empty slaveProduct.price or slaveProduct.priceByCustomer) and (!slaveProduct.loginRequire or userSession != null)}">
			          <c:if test="${(slaveProduct.type != 'box') and (empty slaveProduct.numCustomLines)}">
				        <c:if test="${!(gSiteConfig['gINVENTORY'] and slaveProduct.inventory != null and !slaveProduct.negInventory and slaveProduct.inventory <= 0)}">
				          <c:set var="showAddToCart" value="true" />
			              <option value="${slaveProduct.id}" <c:if test="${model.selectedChild == slaveProduct.id}">selected="selected"</c:if>>
				          <c:forEach items="${slaveProduct.productFields}" var="productField">
				            <c:out value="${productField.value}" escapeXml="false" />
				          </c:forEach>
				 	      </option>
			            </c:if>
				      </c:if>
			        </c:if>
			      </c:forEach>
			    </select>
			    <input type="hidden" value="${model.selectedChild}" name="product.id" id="product.id" />
		      </div>
		    </div>
  		  	  <div class="clear"></div>
  		  </div>
  		  <!-- Product Selection Ends -->
  		  
  		  <!-- Quantity Input Starts -->
          <div id="child_sku_step" class="custom_step">
  		    <c:set var="stepIndex" value="${stepIndex + 1}" />
  			<span class="number custom_step_number"><c:out value="${stepIndex}" /></span>
		    <div class="hdr"><fmt:message key="quantity" /></div>
		    <div class="option_wrapper">
		      <div class="help"> 
		        <label>Provide Quantity</label>
		      </div>
		      <div class="option_selection" id="quantityboxId">
		         <input type="text" size="5" maxlength="5" class="quantityBox" name="quantity_${model.selectedChild}" id="quantity_${model.selectedChild}" value="${model.productQty}"><c:if test="${slaveProduct.packing != ''}"> <c:out value="${slaveProduct.packing}"/> </c:if>
			  </div>
			  <div class="clear"></div>
		    </div>
  		  </div>
  		  <!-- Quantity Input Ends -->
        </c:when>
        <c:otherwise>
          <div id="qty_step" class="custom_step">
  		    <c:set var="stepIndex" value="${stepIndex + 1}" />
  			<span class="number custom_step_number"><c:out value="${stepIndex}" /></span>
		    <div class="hdr"><fmt:message key="quantity" /></div>
		    <div class="option_wrapper">
		      <div class="help"> 
		        <label>Sizes</label>
		      </div>
		      <div class="option_selection">
		        <table border="0" cellpadding="0" cellspacing="0" class="qtyGrid">
			      <tr class="fieldRow">
			      <c:forEach items="${model.childProductsList}" var="slaveFieldName" varStatus="statusPrice" >
			      <!-- column starts -->
			        <td class="bold center">
			    	  <c:forEach items="${slaveFieldName.productFields}" var="productField">
				        <c:out value="${productField.value}" escapeXml="false" />
				      </c:forEach>
			    	</td>
			      </c:forEach>
			      </tr>
			      <tr class="qtyRow" <c:if test="${multiPrice and product.endQtyPricing and !(product.salesTag != null and product.salesTag.discount != 0.0)}">style="text-decoration: line-through;"</c:if>>
			      <c:forEach items="${model.childProductsList}" var="slaveProduct" varStatus="statusPrice">
				    <input type="hidden" value="${slaveProduct.id}" name="product.id"/>
					  <c:if test="${gSiteConfig['gSHOPPING_CART'] and (model.slavesShowQtyColumn ==  null or model.slavesShowQtyColumn)}">
					    <c:set var="zeroPrice" value="false"/>
					    <c:forEach items="${slaveProduct.price}" var="price">
					      <c:if test="${price.amt == 0}">
					        <c:set var="zeroPrice" value="true"/>
					      </c:if>
					    </c:forEach>
				    	<!-- column starts -->
			    		<td class="bold center"> 
			    		  <c:choose>
					        <c:when test="${slaveProduct.type == 'box'}">
					          <a href="${productLink}"><img border="0" src="${_contextpath}/assets/Image/Layout/button_buildthebox.gif" /></a>
					        </c:when>
					        <c:when test="${gSiteConfig['gVARIABLE_PRICE'] and slaveProduct.priceByCustomer}">
					          <c:set var="showAddToCart" value="true" />
					          <input type="checkbox" class="quantityBox" name="quantity_${slaveProduct.id}" id="quantity_${slaveProduct.id}" value="${slaveProduct.minimumQty}" />
					        </c:when>
					        <c:when test="${!empty slaveProduct.numCustomLines}">
					          <a href="${productLink}"><img border="0" src="${_contextpath}/assets/Image/Layout/button_continue.gif" /></a>
					        </c:when>
					        <c:otherwise>
					          <c:choose>
					            <c:when test="${gSiteConfig['gINVENTORY'] and slaveProduct.inventory != null and !slaveProduct.negInventory and slaveProduct.inventory <= 0}" >
					              <c:out value="${siteConfig['OUT_OF_STOCK_TITLE'].value}" escapeXml="false"/>
					            </c:when>
					            <c:when test="${zeroPrice and siteConfig['ZERO_PRICE_HTML'].value != ''}">
					              <c:out value="${siteConfig['ZERO_PRICE_HTML'].value}" escapeXml="false"/>
					            </c:when>
					            <c:otherwise>
					              <c:set var="showAddToCart" value="true" />
					              <input type="text" size="5" maxlength="5"  class="quantityBox" name="quantity_${slaveProduct.id}" id="quantity_${slaveProduct.id}" value="<c:out value="${model.productQtyMap[slaveProduct.id]}"/>" ><c:if test="${slaveProduct.packing != ''}"> <c:out value="${slaveProduct.packing}"/> </c:if>
					              <c:if test="${gSiteConfig['gINVENTORY'] and slaveProduct.inventory != null and slaveProduct.inventory < slaveProduct.lowInventory}">
					                <c:out value="${siteConfig['LOW_STOCK_TITLE'].value}" escapeXml="false"/>
					              </c:if>
					            </c:otherwise>
					          </c:choose>
					        </c:otherwise>
					      </c:choose>
					    </td>
			        	<!-- column ends -->
					  </c:if>
					</c:forEach>
				    </tr>
			      </table>
		        </div>
		      <div class="clear"></div>
		    </div>
		  </div>
        </c:otherwise>
      </c:choose>
    </c:otherwise>
  </c:choose>