<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
	               
<script type="text/javascript">
var box2 = {};
window.addEvent('domready',function() {
	  box2 = new multiBox('mbCategory', {waitDuration: 5,showControls: false,overlay: new overlay()});
});
function updateImprintColor(optionId, optionIndex){
	var selectedColors = ""; 
	var selectedColorCount = 0;
	//alert('optionId'+optionId+' optionIndex'+optionIndex);
	$$('.imprintColorCheckBox'+optionId+'-'+optionIndex).each(function(ele){
		if(ele.checked) {
			selectedColorCount = selectedColorCount + 1;
			selectedColors = selectedColors + ele.value+",";
		}
	});
	//alert('selectedColors'+selectedColors);
	if(parseInt($('maxNumberOfColors'+optionId+'-'+optionIndex).value) < parseInt(selectedColorCount)) {
		alert('You already selected '+$('maxNumberOfColors'+optionId+'-'+optionIndex).value+ ' colors. Please remove some of them to select new color.');
		return false;
	}
	$('selectedImprintColors'+optionId+'-'+optionIndex).value= selectedColors;
	$('customColors'+optionId+'-'+optionIndex).value= selectedColors;
}
</script>

<div class="multiOptions" id="multiOptions">
  <c:set var="stepIndex" />
  <!-- Step 1 Starts for Quantity Input-->
  <%@ include file="/WEB-INF/jsp/frontend/premier/childQuantityInput.jsp" %>
  <!-- Step 1 Ends -->

  <div class="productOption" id="productOptionsId">
  <c:forEach items="${model.optionList}" var="productOption" varStatus="optionStatus">
  <!-- Step Starts -->
  <div class="custom_step">
  	<c:set var="stepIndex" value="${stepIndex + 1}" />
  	<span class="number custom_step_number"><c:out value="${stepIndex}" /></span>
    <div class="hdr"><c:out value="${productOption.name}" /></div>
    <div class="option_wrapper">
      <div class="help"> 
        <label>
          <c:choose>
            <c:when test="${productOption.type == 'cus'}">Provide</c:when>
            <c:otherwise>Choose</c:otherwise>
          </c:choose>
         <c:out value="${productOption.name}" /> </label>
        <div class="helpText">
          <c:out value="${productOption.helpText}" escapeXml="false"></c:out>
        </div>
      </div>
      
      <div class="option_selection">
      <!-- Start : Block of values and variable, required to set option selection  -->
      <input type="hidden" name="optionCode_${productOption.productId}" value="<c:out value="${productOption.optionCode}"/>"/>	
      <div style="display:none;">
        <c:set var="optName" value="${productOption.optionCode}-option_values_${productOption.productId}_${productOption.index}"></c:set>
        <c:set var="optValue"></c:set>
        <c:forEach var="entry" items="${model.originalSelection}" varStatus="status">
          <c:if test="${optName == entry.key}">
            <c:set var="optValue" value="${entry.value}"></c:set>  
          </c:if>
        </c:forEach>
      </div>
      <!-- End : Block of values and variable, required to set option selection  -->
      
      <div class="productOptionNVPair" id="productOptionNVPairId${optionStatus.index}">
    	<c:set var="includedProducts" value=""/>
		<div class="optionValue">
		  <!-- option value start -->
		  <c:if test="${!empty productOption.values}">
		  <input type="hidden" name="<c:out value="${productOption.optionCode}"/>-option_${productOption.productId}" value="<c:out value="${productOption.index}"/>"/>
		  <select onchange="updateSteps(${model.product.id});"  name="<c:out value="${productOption.optionCode}"/>-option_values_${productOption.productId}_<c:out value="${productOption.index}" />" class="optionSelect">
		  <c:forEach items="${productOption.values}" var="prodOptValue" varStatus="povStatus">
		  	  <option  <c:if test="${prodOptValue.index == optValue}">selected="selected"</c:if> value="<c:out value="${prodOptValue.index}"/>"><c:out value="${prodOptValue.name}"/><c:if test="${prodOptValue.optionPrice != null}"><c:choose><c:when test="${prodOptValue.optionPrice > 0}"> + </c:when><c:otherwise> - </c:otherwise></c:choose><fmt:message key="${siteConfig['CURRENCY'].value}" /><fmt:formatNumber value="${prodOptValue.absoluteOptionPrice}" pattern="#,##0.00" /></c:if></option>
		      <c:if test="${prodOptValue.index == optValue}">
		 	    <c:set var="includedProducts" value="${prodOptValue.includedProductList}"/>	    
		      </c:if>
		  </c:forEach>
		  </select>
		  </c:if>
		  <!-- option value ends -->
		  
		  <!-- custom text start -->
		  <c:if test="${siteConfig['CUSTOM_TEXT_PRODUCT'].value == 'true' and empty productOption.values  and productOption.type == 'cus'}">
		    <c:set var="optName" value="${productOption.optionCode}-option_values_cus_${productOption.productId}_${productOption.index}"></c:set>
        	<input type="hidden" name="<c:out value="${productOption.optionCode}"/>-option_${productOption.productId}" value="<c:out value="${productOption.index}"/>"/>
		    <textarea name="<c:out value="${productOption.optionCode}"/>-option_values_cus_${productOption.productId}_<c:out value="${productOption.index}" />" class="optionTextArea"><c:out value="${fn:trim(model.originalCustomText[optName])}" /></textarea>
		  </c:if>
		  <!-- custom text end -->
	
		  <!-- color swatch start -->
		  <c:if test="${ productOption.type == 'colorSwatch'}">
		    <c:set var="colorSwatchId" value="${productOption.optionId}-${productOption.index}"/>
	        <c:set var="selectedColors" />
	        <div class="imprintColorsWrapper" style="width: 400px;">
              <div class="imprintColors" id="imprintColorsId${optionStatus.index}" style="width:400px;">
        	  <c:forEach items="${model.colors}" var="color" varStatus="imprintColorStatus">
          	    <c:set var="selected" value="false"/>
	        	<div id="imprintColor${imprintColorStatus.index +1}" style="width: 90px; float: left; padding: 3px;">
	               <c:forEach items="${model.locationSelectedColorMap[colorSwatchId]}" var="selectedColor">
	                 <c:if test="${selectedColor == color.name}">
	                   <c:choose>
	                     <c:when test="${selectedColors != null and selectedColors != ''}">
	                       <c:set var="selectedColors" value="${selectedColors}, ${selectedColor}"/>
	              	     </c:when>
	                     <c:otherwise>
	                       <c:set var="selectedColors" value="${selectedColor}"/>
	              	     </c:otherwise>
	                   </c:choose>
	                  <%--
	              	  <div style="display: none;">
	              	    <input type="hidden" name="optionNVPair" value="Imprint Color_value_${fn:escapeXml(selectedColor)}"/>
 		              </div>
 		              --%> 
 		              <c:set var="selected" value="true"/>
	                </c:if>
	                </c:forEach>
	              <div style="cursor: pointer; float: left;"> 
	                <input type="checkbox" value="${color.name}" class="imprintColorCheckBox${productOption.optionId}-${productOption.index}" <c:if test="${selected}">checked="checked"</c:if> onclick="return updateImprintColor('${productOption.optionId}','${productOption.index}');"/> 
	              </div>
			      <div class="imprintColorBox" id="imprint${imprintColorStatus.index + 1}" style="background-color: ${color.value}; padding:13px; margin: 2px 0 10 3px; border-radius : 2px; width: 25px; float: left;"></div> 
	            </div>
	            <c:if test="${(imprintColorStatus.index + 1) % 3 == 0}">
	              <div style="clear: both;"></div>
	            </c:if>
	          </c:forEach>
	          <div style="display: none;">
	        	<input type="hidden" id="customColors${productOption.optionId}-${productOption.index}" name="<c:out value="${productOption.optionCode}"/>-option_values_cus_${productOption.productId}_<c:out value="${productOption.index}" />" value="${fn:escapeXml(selectedColors)}" />
		      </div>  
	              
	        </div>
	        <div style="clear: both;"></div>
		  </div>
		  <input type="hidden" class="colorSwatch" name="colorSwatch" value="${productOption.optionId}-${productOption.index}"/>
		  <input type="hidden" name="maxNumberOfColors${productOption.optionId}-${productOption.index}" id="maxNumberOfColors${productOption.optionId}-${productOption.index}" value="<c:out value="${productOption.numberOfColors}"/>"/>
		  <input type="hidden" name="<c:out value="${productOption.optionCode}"/>-option_${productOption.productId}" value="<c:out value="${productOption.index}"/>"/>
		  <input type="hidden" id="selectedImprintColors${productOption.optionId}-${productOption.index}" name="<c:out value="${productOption.optionCode}"/>-option_values_color_swatch_${productOption.productId}_<c:out value="${productOption.index}" />" value="${selectedColors}"/>
		</c:if>
		<!-- color swatch end -->
	
		</div>
	    <!-- class to clear the float, if any -->
	    <div class="clear"></div>

		<!-- attachment for included products start -->
		<c:if test="${includedProducts != null && !empty includedProducts}">
		  <div class="attachment">
		    <c:forEach items="${includedProducts}" var="includedProduct" varStatus="productStatus">
		      <c:if test="${includedProduct.attachment}">
			    <div class="optionName">
		    	  <fmt:message key="upload" />&nbsp;<fmt:message key="attachment" />:
		        </div>
		  	    <input value="browse" type="file" name="attachment_${includedProduct.sku}"/>
			  </c:if>
		   	</c:forEach>
		  </div>
	      <!-- class to clear the float, if any -->
	      <div class="clear"></div>
		</c:if>
		<!-- attachment for included products end -->
  	  </div>
      </div>
      <div class="clear"></div>
    </div>
  </div>
  <!-- Step Ends -->
  </c:forEach>
  </div>

<c:if test="${!empty model.imageUrlSet}">
<div id="images" >
<c:forEach items="${model.imageUrlSet}" var="imageUrl">
  <div style="display: inline;"><img class="optionImage" <c:if test="${imageUrl != null}">src="${_contextpath}/assets/Image/Product/options/${imageUrl}"</c:if>></div>
</c:forEach>
</div>
</c:if>

<div style="clear: both;"></div> 
<input type="hidden" name="newPrice" value="<fmt:formatNumber value="${model.newPrice}" pattern="#,##0.00"/>" id="newPrice"/>
</div>


<input type="image" value="Add to Cart" name="_addtocart_quick" class="_addtocart_quick" src="${_contextpath}/assets/Image/Layout/button_addtocart${_lang}.gif" >