<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<tiles:insertDefinition name="${_template}" flush="true">
  <tiles:putAttribute name="content" type="string">

<c:if test="${(model != null) && (model.policies != null)}">

<c:out value="${model.policyLayout.headerHtml}" escapeXml="false"/>

<c:forEach items="${model.policies}" var="policy" varStatus="status">
<c:if test="${status.first}"><ul></c:if>
  <li><a href="policy.jhtm#${status.index}" class="policyLink"><c:out value="${policy.name}" escapeXml="false" /></a></li>
<c:if test="${status.last}"></ul></c:if>
</c:forEach>

<c:forEach items="${model.policies}" var="policy" varStatus="status">
<p class="policy">
  <a name="${status.index}"><span class="policyName"><c:out value="${policy.name}" escapeXml="false" /></span></a><br>
  <c:out value="${policy.content}" escapeXml="false" />
</p>
</c:forEach>

<c:out value="${model.policyLayout.footerHtml}" escapeXml="false"/>

</c:if>
    
  </tiles:putAttribute>
</tiles:insertDefinition>