<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>

<tiles:insertDefinition name="${_template}" flush="true">
  <tiles:putAttribute name="content" type="string">

<c:out value="${model.newTicketLayout.headerHtml}" escapeXml="false"/>
  
<c:if test="${model.message != null}">
  <div class="message"><fmt:message key="${model.message}" /></div>
</c:if>

<div class="TicketFormWrapper">
<form:form  commandName="ticketForm" method="post" enctype="multipart/form-data">
<h2><fmt:message key="submitASupportTicket" /></h2>
<p><fmt:message key="describeYourProblemOrQuestion" /></p>
<dl>
  <dt><label><fmt:message key="subject" /></label></dt>
  <dd><form:input path="ticket.subject" size="50" cssClass="input" cssErrorClass="error"/><form:errors path="ticket.subject" cssClass="error"/></dd>
  
  <dt><label><fmt:message key="description/question" /></label></dt>
  <dd><form:textarea path="ticket.question" rows="8" cols="50" htmlEscape="false" cssClass="textarea" cssErrorClass="error"/><form:errors path="ticket.question" cssClass="error"/></dd>
</dl>

  <c:if test="${siteConfig['TICKET_ATTACHMENT'].value == 'true'}">
  <div class="ticketAttachmentWrapper">
    <span style="width:100px;text-align:right"><fmt:message key="attachment" />:</span>
    <input value="browse" type="file" name="attachment"/>
  </div>
  </c:if>
  <c:if test="${model.ticketType != null and not empty model.ticketType }">
  <div class="ticketType">
  <h2><fmt:message key="serviceType" /></h2>
  <p>Please select the service associated with your issue.</p>
  <dl>
    <dt><label><fmt:message key="type" /></label></dt>
    <dd>
	  <form:select path="ticket.type" cssClass="ticketTypeSelect">
	    <form:option value="">Please Select</form:option>
	    <form:options items="${model.ticketType}" />
	    <%-- 
		  <form:option value="">Please Select</form:option>
		  <form:option value="Site/FTP Access">Site/FTP Access</form:option>
		  <form:option value="Design">Design</form:option>
		  <form:option value="SSL">SSL</form:option>
	      <form:option value="New Feature">New Feature</form:option>
		  <form:option value="SEO">SEO</form:option>
		  <form:option value="Email">Email</form:option>
		  <form:option value="Error">Error</form:option>
	      <form:option value="Bug Report">Bug Report</form:option>
	      <form:option value="Site Down">Site Down</form:option>
	      <form:option value="Other">Other</form:option>
	      --%>
	  </form:select>
      <form:errors path="ticket.type" cssClass="error"/>
    </dd>
  </dl> 
  </div>
</c:if>
<div class="formAction">
      <input type="submit" value="Create New Ticket" />
	  <input type="submit" value="Cancel" name="_cancel" />
</div>
  
</form:form>
</div>

<c:out value="${model.newTicketLayout.footerHtml}" escapeXml="false"/>

  </tiles:putAttribute>
</tiles:insertDefinition>