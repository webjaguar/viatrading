<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>

<tiles:insertDefinition name="${_template}" flush="true">
  <tiles:putAttribute name="content" type="string">
  
<c:out value="${model.ticketsLayout.headerHtml}" escapeXml="false"/>  
  
<br><br><br>
<form method="post">
<table width="90%" align="center" cellspacing="5" cellpadding="0" border="0">
  <tr>
  	<td class="listingsHdr1"><fmt:message key="tickets" /></td>
	<td>
<div class="pageNavi">Page 
		<select name="page" onchange="submit()">
		<c:forEach begin="1" end="${model.ticketList.pageCount}" var="page">
  	        <option value="${page}" <c:if test="${page == (model.ticketList.page+1)}">selected</c:if>>${page}</option>
		</c:forEach>
		</select>
of <c:out value="${model.ticketList.pageCount}"/></div>
	</td>
  </tr>
  <tr>
	<td colspan="2">
<table border="0" cellpadding="3" cellspacing="1" width="100%" class="listings"> 
<thead>
  <tr class="listingsHdr2">
    <th class="nameCol"><select name="status">
        <option value="open" <c:if test="${ticketSearch.status == 'open'}">selected</c:if>>Open</option>
        <option value="close" <c:if test="${ticketSearch.status == 'close'}">selected</c:if>>Close</option> 
        <option value="" <c:if test="${ticketSearch.status == ''}">selected</c:if>>All</option> 
      </select>
    </th>
  	<th class="nameCol"><input name="ticket_id" type="text" value="<c:out value='${ticketSearch.ticketId}' />" size="15" /></th>
  	<th class="nameCol"><input name="subject" type="text" value="<c:out value='${ticketSearch.subject}' />" size="15" /></th>
  	<th class="nameCol" colspan="2" align="center"><input type="submit" value="Search" /></th>
  </tr>
  <tr class="listingsHdr2">
    <th class="nameCol"><fmt:message key="status" /></th>
  	<th class="nameCol"><fmt:message key="ticketNumber" /></th>
  	<th class="nameCol"><fmt:message key="subject" /></th>
  	<th class="nameCol"><fmt:message key="created" /></th>
  	<th align="center"><fmt:message key="lastModified" /></th>
  </tr>
</thead>
<tbody> 
<c:forEach items="${model.ticketList.pageList}" var="ticket" varStatus="status">
  <tr class="row${status.index % 2}">
    <td class="nameCol"><c:out value="${ticket.status}"/></td>
	<td class="nameCol"><a href="account_ticketUpdate.jhtm?id=${ticket.ticketId}" class="nameLink"><c:out value="${ticket.ticketId}"/></a></td>
    <td class="nameCol"><c:out value="${ticket.subject}"/></td>
    <td class="nameCol"><fmt:formatDate type="both" timeStyle="full" pattern="MM/dd/yyyy hh:mm a" value="${ticket.created}"/></td>	
    <td class="nameCol"><fmt:formatDate type="both" timeStyle="full" pattern="MM/dd/yyyy hh:mm a" value="${ticket.lastModified}"/></td>	
  </tr>
</c:forEach>
<c:if test="${model.ticketList.nrOfElements == 0}">
  <tr class="emptyList"><td colspan="5">&nbsp;</td></tr>
</c:if>
</tbody>
</table>
<br/>
	</td>
  </tr>
</table>
</form>

<c:out value="${model.ticketsLayout.footerHtml}" escapeXml="false"/>

  </tiles:putAttribute>
</tiles:insertDefinition>
