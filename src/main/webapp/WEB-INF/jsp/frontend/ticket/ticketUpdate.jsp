<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="/WEB-INF/tlds/custom.tld" prefix="wj" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>

<tiles:insertDefinition name="${_template}" flush="true">
  <tiles:putAttribute name="content" type="string">
<style type="text/css">
.ticketAdmin{background-color:#DDDDDD; border:1px solid #E0E0E0;padding:10px;margin:10px;}
.ticketUser{background-color:#CCDDFF; border:1px solid #E0E0E0;padding:10px;margin:10px;}
</style>
  
<c:out value="${model.updateTicketLayout.headerHtml}" escapeXml="false"/>  
  
<c:if test="${model.message != null}">
  <div class="message"><fmt:message key="${model.message}" /></div>
</c:if>

<div class="ticketUpdateComment">
  <h2>Tickets / <span><c:out value="${ticketForm.ticket.subject}" escapeXml="false"/> (<c:out value="${ticketForm.ticket.ticketId}" escapeXml="false"/>)</span></h2>
</div>
<c:forEach items="${model.questionAnswerList}" var="ticket" varStatus="status">
 <c:if test="${status.first}">
   
   <c:choose>
     <c:when test="${ticket.ticket.createdBy != NULL}">
       <div class="ticketAdmin">
       <div class="commentHead admin">
         <p class="commentName"><c:out value="${siteConfig['COMPANY_NAME'].value}"/> Said:</p>
         <p class="commentTime"><fmt:formatDate type="date" dateStyle="full" value="${ticket.ticket.created}" pattern="MM/dd/yyyy (hh:mm)a zz "/></p>
       </div>
       <br />
       <div class="commentContent admin"><c:out value="${wj:newLineToBreakLine(ticket.ticket.question)}" escapeXml="false"/></div>
       <c:if test="${ticket.ticketVersion.attachment != null}">
	     <div class="attachmentLink">
	       <a href="${_contextpath}/temp/Ticket/customer_${ticket.ticket.userId}/${ticket.ticketVersion.attachment}"> <c:out value="${ticket.ticketVersion.attachment}"></c:out> </a>
         </div>
	   </c:if>
       </div>
     </c:when>
     <c:otherwise>
       <div class="ticketUser">
       <div class="commentHead user">
         <p class="commentName"><c:out value="${model.firstName}"/> Said:</p>
         <p class="commentTime"><fmt:formatDate type="date" dateStyle="full" value="${ticket.ticket.created}" pattern="MM/dd/yyyy (hh:mm)a zz "/></p>
       </div>
       <br />
       <div class="commentContent user"><c:out value="${wj:newLineToBreakLine(ticket.ticket.question)}" escapeXml="false"/></div>
       <c:if test="${ticketForm.ticketVersion.attachment != null}">
	     <div class="attachmentLink">
             <a href="${_contextpath}/temp/Ticket/customer_${ticket.ticket.userId}/${ticketForm.ticketVersion.attachment}"><c:out value="${ticketForm.ticketVersion.attachment}"></c:out></a>
         </div>
         </c:if>
       </div>
     </c:otherwise>
  </c:choose>
   
 </c:if>
 <c:if test="${!empty ticket.ticketVersion.questionDescription}">
   <div class="ticket${ticket.ticketVersion.createdByType}">
   <div class="commentHead user">
     <p class="commentName">
       <c:choose>
       <c:when test="${ticket.ticketVersion.createdByType == 'Admin'}"><c:out value="${siteConfig['COMPANY_NAME'].value}"/> </c:when>
       <c:otherwise><c:out value="${model.firstName}"/> </c:otherwise>
       </c:choose><fmt:message key="said" />:
    </p>
    <p class="commentTime"><fmt:formatDate type="date" dateStyle="full" value="${ticket.ticketVersion.created}" pattern="MM/dd/yyyy (hh:mm)a zz "/></p>
  </div>
  <br />
  <div class="commentContent user"><c:out value="${wj:newLineToBreakLine(ticket.ticketVersion.questionDescription)}" escapeXml="false"/></div>
    <c:if test="${ticket.ticketVersion.attachment != null}">
	  <div class="attachmentLink">
	     <a href="${_contextpath}/temp/Ticket/customer_${ticket.ticket.userId}/${ticket.ticketVersion.attachment}"> <c:out value="${ticket.ticketVersion.attachment}"></c:out> </a>
      </div>
	</c:if>
  </div>
 </c:if>
</c:forEach>

<c:choose>
 <c:when test="${!empty model.questionAnswerList}">
	
	<form:form  commandName="ticketForm" method="post" enctype="multipart/form-data">
	<input type="hidden" name="id" value="<c:out value='${ticketForm.ticket.ticketId}'/>" />
	<c:if test="${ticketForm.ticket.status == 'open' }">
	<h2><fmt:message key="updateTicket" /></h2>
	<dl>
	  <dt><label><fmt:message key="comment" /></label></dt>
	  <dd><form:textarea path="ticketVersion.questionDescription" rows="8" cols="50" htmlEscape="false" cssClass="textarea" cssErrorClass="error"/><form:errors path="ticketVersion.questionDescription" cssClass="error"/></dd>
	</dl>
	<c:if test="${siteConfig['TICKET_ATTACHMENT'].value == 'true'}">
	<div class="ticketAttachmentWrapper">
	  <span style="width:100px;text-align:right"><fmt:message key="attachment" />:</span>
      <input value="browse" type="file" name="attachment"/>
	</div>
	</c:if>
	<div class="formAction">
	      <input type="submit" name="__update_ticket" value="<fmt:message key="updateTicket" />">
	</div>
	<div class="formAction cancel">
	      <input type="submit" name="__close_ticket" value="<fmt:message key="closeTicket" />" onClick="return confirm('Close this Ticket permanently?')">
	</div>
	</c:if>
	
  </form:form>
 </c:when>
 <c:otherwise>
    <div>Error Message</div>
 </c:otherwise>
</c:choose>

<c:out value="${model.updateTicketLayout.footerHtml}" escapeXml="false"/>  

  </tiles:putAttribute>
</tiles:insertDefinition>