<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="/WEB-INF/tlds/spring.tld" prefix="spring"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>

<tiles:insertDefinition name="${_template}" flush="true">
	<tiles:putAttribute name="leftbar" value="/WEB-INF/jsp/frontend/layout/template${_leftBar}/leftbar.jsp" />
	<tiles:putAttribute name="content" type="string">
<script type="text/javascript">
	window.addEvent('domready', function(){
		var tabs1 = new SimpleTabs($('tab-block-1'), { selector: 'h4'});
	});
</script>
<script type="text/javascript">
function getOption(productId){
	var urlappend = '';	
	$$('select.optionSelect').each(function(select){
		urlappend = urlappend + '&option='+select.name+'-'+select.value;
	});
	var requestHTMLData = new Request.HTML ({
			url: "${_contextpath}/show-ajax-options.jhtm?&productId="+productId+urlappend,
			update: $('newOption'),
			onSuccess: function () {
				$('pricing').set('html', $('newPrice').value);
			}
		}).send();
}
</script>

		<c:if test="${model.message != null}">
			<div class="message"><fmt:message key="${model.message}" /></div>
		</c:if>
		<c:if test="${model.product != null}">
			<c:if test="${fn:trim(model.productLayout.headerHtml) != ''}">
				<c:set value="${model.productLayout.headerHtml}" var="headerHtml" />
				<%@ include file="/WEB-INF/jsp/frontend/common/headerDynamicElement.jsp"%>
				<div><c:out value="${headerHtml}" escapeXml="false" /></div>
			</c:if>
			<c:set value="${model.product}" var="product" />
			<script language="JavaScript" type="text/JavaScript">
			<c:if test="${siteConfig['DETAILS_IMAGE_WIDTH'].value != '' }" >
			function zoomIn() {
				window.open('${_contextpath}/productImage.jhtm?id=${product.id}&layout=2&imgUrl=' + document.getElementById("_image").src,'name','width=800,height=600,resizable=yes,scrollbars=1');
			}
			</c:if>
			</script>
			<c:set var="details_images_style" value="" />
			<c:set var="details_desc_style" value="" />
			<c:set var="productImage">
				<c:choose>
					<c:when
						test="${fn:contains(siteConfig['PRODUCT_IMAGE_LAYOUTS'].value,'s2') and 
 		(product.imageLayout == 's2' or (empty product.imageLayout and siteConfig['PRODUCT_IMAGE_LAYOUT'].value == 's2'))}">
						<link rel="stylesheet"
							href="${_contextpath}/assets/SmoothGallery/slideshow.css"
							type="text/css" media="screen" />
						<script type="text/javascript"
							src="${_contextpath}/javascript/slideshow.js"></script>
						<script type="text/javascript">		
	//<![CDATA[
	  window.addEvent('domready', function(){
	    var data = [<c:forEach items="${product.images}" var="image" varStatus="status">'<c:if test="${!image.absolute}">${_contextpath}/assets/Image/Product/detailsbig/</c:if><c:out value="${image.imageUrl}"/>'<c:if test="${! status.last}" >,</c:if></c:forEach> ];
	    var myShow = new Slideshow('show', data, { controller: true, height: 300, hu: '', thumbnails: true, width: 400 });
		});
	//]]>
	</script>

						<div id="show" class="slideshow"></div>
						<div id="slideshow_margin"></div>
					</c:when>
					<c:when
						test="${fn:contains(siteConfig['PRODUCT_IMAGE_LAYOUTS'].value,'mb') and 
 		(param.multibox == null or param.multibox != 'off') and
 		(product.imageLayout == 'mb' or (empty product.imageLayout and siteConfig['PRODUCT_IMAGE_LAYOUT'].value == 'mb'))}">
						<link rel="stylesheet"
							href="${_contextpath}/assets/SmoothGallery/multibox.css"
							type="text/css" media="screen" />
						<c:set value="${true}" var="multibox" />
						<script src="${_contextpath}/javascript/overlay.js"
							type="text/javascript"></script>
						<script src="${_contextpath}/javascript/multibox.js"
							type="text/javascript"></script>
						<script type="text/javascript">
	window.addEvent('domready', function(){
		var box = new multiBox('mbxwz', {overlay: new overlay()});
	});
 </script>
						<div class="details_image_box" id="details_image_boxId"
							style="${details_images_style}
	 <c:if test="${siteConfig['DETAILS_IMAGE_WIDTH'].value != '' }">width: <c:out value="${siteConfig['DETAILS_IMAGE_WIDTH'].value}"/>px;</c:if>">

						<c:forEach items="${model.product.images}" var="image"
							varStatus="status">
							<c:if test="${status.first}">
								<a
									href="<c:if test="${!image.absolute}">${_contextpath}/assets/Image/Product/detailsbig/</c:if><c:out value="${image.imageUrl}"/>"
									id="mb0" class="mbxwz" title=""><img
									src="<c:if test="${!image.absolute}">${_contextpath}/assets/Image/Product/detailsbig/</c:if><c:out value="${image.imageUrl}"/>"
									border="0"
									style="<c:if test="${siteConfig['DETAILS_IMAGE_WIDTH'].value != '' }">width: <c:out value="${siteConfig['DETAILS_IMAGE_WIDTH'].value}"/>px</c:if>"
									alt="<c:out value="${product.name}"/><c:if test="${product.shortDesc != ''}"> - <c:out value="${product.shortDesc}"/></c:if>" /></a>
							</c:if>
						</c:forEach>
						<div style="clear: both;"><c:forEach
							items="${model.product.images}" var="image" varStatus="status">
							<c:if test="${status.count > 1}">
								<c:if test="${status.count < 5}">
								<a
									href="<c:if test="${!image.absolute}">${_contextpath}/assets/Image/Product/detailsbig/</c:if><c:out value="${image.imageUrl}"/>"
									id="mb${status.count - 1}" class="mbxwz" title="">
									<img
									src="<c:if test="${!image.absolute}">${_contextpath}/assets/Image/Product/detailsbig/</c:if><c:out value="${image.imageUrl}"/>"
									border="0" class="details_thumbnail"
									alt="<c:out value="${product.name}"/>
									<c:if test="${product.shortDesc != ''}"> - <c:out value="${product.shortDesc}"/></c:if>" /></a>
							</c:if>
							<c:if test="${status.count > 5}">
								<a
									href="<c:if test="${!image.absolute}">${_contextpath}/assets/Image/Product/detailsbig/</c:if><c:out value="${image.imageUrl}"/>"
									id="mb${status.count - 1}" class="mbxwz" title="">
									<img
									src="<c:if test="${!image.absolute}">${_contextpath}/assets/Image/Product/detailsbig/</c:if><c:out value="${image.imageUrl}"/>"
									border="0" class="details_thumbnail_hide"
									alt="<c:out value="${product.name}"/>
									<c:if test="${product.shortDesc != ''}"> - <c:out value="${product.shortDesc}"/></c:if>" /></a>
							</c:if>
							</c:if>
						</c:forEach></div>
						</div>
					</c:when>
					<c:when
						test="${fn:contains(siteConfig['PRODUCT_IMAGE_LAYOUTS'].value,'qb') and 
 		(product.imageLayout == 'qb' or (empty product.imageLayout and siteConfig['PRODUCT_IMAGE_LAYOUT'].value == 'qb'))}">
						<link rel="stylesheet"
							href="${_contextpath}/assets/SmoothGallery/QuickBox/quickbox.css"
							type="text/css" media="screen" />
						<c:set value="${true}" var="quickbox" />
						<script src="${_contextpath}/javascript/QuickBox.js"
							type="text/javascript"></script>
						<script type="text/javascript">
	window.addEvent('domready', function(){
		new QuickBox();
	});
 </script>
						<div class="details_image_box" id="details_image_boxId" style="${details_images_style} <c:if test="${siteConfig['DETAILS_IMAGE_WIDTH'].value != '' }">width: <c:out value="${siteConfig['DETAILS_IMAGE_WIDTH'].value}"/>px;</c:if>">
						<c:forEach items="${model.product.images}" var="image"
							varStatus="status">
							<c:if test="${status.first}">
								<a
									href="<c:if test="${!image.absolute}">${_contextpath}/assets/Image/Product/detailsbig/</c:if><c:out value="${image.imageUrl}"/>"
									id="" class="" title="" rel="quickbox"><img
									src="<c:if test="${!image.absolute}">${_contextpath}/assets/Image/Product/detailsbig/</c:if><c:out value="${image.imageUrl}"/>"
									border="0"
									style="<c:if test="${siteConfig['DETAILS_IMAGE_WIDTH'].value != '' }">width: <c:out value="${siteConfig['DETAILS_IMAGE_WIDTH'].value}"/>px</c:if>"
									alt="<c:out value="${product.name}"/><c:if test="${product.shortDesc != ''}"> - <c:out value="${product.shortDesc}"/></c:if>" /></a>
							</c:if>
						</c:forEach>
						<div style="clear: both;"><c:forEach
							items="${model.product.images}" var="image" varStatus="status">
							<c:if test="${status.count > 1}">
								<a
									href="<c:if test="${!image.absolute}">${_contextpath}/assets/Image/Product/detailsbig/</c:if><c:out value="${image.imageUrl}"/>"
									id="qb${status.count - 1}" class="" title="" rel="quickbox"><img
									src="<c:if test="${!image.absolute}">${_contextpath}/assets/Image/Product/detailsbig/</c:if><c:out value="${image.imageUrl}"/>"
									border="0" class="details_thumbnail"
									alt="<c:out value="${product.name}"/><c:if test="${product.shortDesc != ''}"> - <c:out value="${product.shortDesc}"/></c:if>" /></a>
							</c:if>
						</c:forEach></div>
						</div>
					</c:when>
					<c:otherwise>
						<div class="details_image_box" id="details_image_boxId" style="${details_images_style} <c:if test="${siteConfig['DETAILS_IMAGE_WIDTH'].value != '' }">width: <c:out value="${siteConfig['DETAILS_IMAGE_WIDTH'].value}"/>px;</c:if>">
						<c:forEach items="${model.product.images}" var="image"
							varStatus="status">
							<c:if test="${status.first}">
								<img
									src="<c:if test="${!image.absolute}">${_contextpath}/assets/Image/Product/detailsbig/</c:if><c:out value="${image.imageUrl}"/>"
									border="0" id="_image" name="_image" class="details_image"
									style="<c:if test="${siteConfig['DETAILS_IMAGE_WIDTH'].value != '' }">width: <c:out value="${siteConfig['DETAILS_IMAGE_WIDTH'].value}"/>px</c:if>"
									alt="<c:out value="${product.name}"/><c:if test="${product.shortDesc != ''}"> - <c:out value="${product.shortDesc}"/></c:if>">
								<c:if test="${siteConfig['DETAILS_IMAGE_WIDTH'].value != '' }">
								</c:if>
							</c:if>

							<c:if test="${status.last and (status.count != 1)}">
								<br>
								<c:forEach items="${model.product.images}" var="thumb"
									varStatus="thumbstatus">
									<c:if test="${thumbstatus.index < 4}">
										<a href="#" class="details_thumbnail_anchor"
											onClick="_image.src='<c:if test="${!thumb.absolute}">${_contextpath}/assets/Image/Product/detailsbig/</c:if><c:out value="${thumb.imageUrl}"/>';return false;">
										<img
											src="<c:if test="${!thumb.absolute}">${_contextpath}/assets/Image/Product/detailsbig/</c:if><c:out value="${thumb.imageUrl}"/>"
											class="details_thumbnail"
											alt="<c:out value="${product.name}"/><c:if test="${product.shortDesc != ''}"> - <c:out value="${product.shortDesc}"/></c:if>">
										</a>
									</c:if>
								</c:forEach>
								<div class="moreImageWrapper" align="right"><a
									onclick="zoomIn()" title="View More Images"><span>View
								More Images</span></a></div>
							</c:if>
						</c:forEach></div>
						<c:if test="${fn:contains(siteConfig['PRODUCT_IMAGE_LAYOUTS'].value,'ss') and (product.imageLayout == 'ss'  or (empty product.imageLayout and siteConfig['PRODUCT_IMAGE_LAYOUT'].value == 'ss'))}">
							<object classid="clsid:D27CDB6E-AE6D-11cf-96B8-444553540000"
								codebase="http://download.macromedia.com/pub/shockwave/cabs/flash/swflash.cab#version=6,0,29,0"
								width="750" height="475">
								<param name="movie"
									value="${_contextpath}/assets/Flash/slideshow.swf?file=thumbnailer1.jhtm?id=${product.id}">
								<param name="quality" value="high">
								<embed
									src="${_contextpath}/assets/Flash/slideshow.swf?file=thumbnailer1.jhtm?id=${product.id}"
									quality="high"
									pluginspage="http://www.macromedia.com/go/getflashplayer"
									type="application/x-shockwave-flash" width="750" height="475"></embed>
							</object>
						</c:if>
					</c:otherwise>
				</c:choose>
			</c:set>


			<%-- bread crumbs --%>
			<c:import url="/WEB-INF/jsp/frontend/layout/${_template}/breadcrumbs.jsp" />
			<div class="details_main_description">
			<div class="details_main_upper_container">
			<div class="details_product_image_container"><c:out value="${productImage}" escapeXml="false" /></div>
			<div class="details_product_addtocart_container">
			<form action="${_contextpath}/addToCart.jhtm" name="this_form" method="post" onsubmit="return checkForm()">
			<div class="details_product_addtocart_desc">			
			<div class="details_product_addtocart_productname_box">
				<div class="details_product_addtocart_productname">
					<h1><c:out value="${product.name}" escapeXml="false" /></h1>
				</div>
			</div>
			<div class="details_product_addtocart_productprice_box">
				<table cellpadding="0" cellspacing="0" width="100%">
					<tr>
						<td class="details_product_addtocart_productprice_td" >
							<c:if test="${siteConfig['SHOW_SKU'].value == 'true' and product.sku != null}">
								<div class="details_product_addtocart_productsku"><c:out value="Item code: " /><c:out value="${product.sku}" /></div>
							</c:if>
							<c:if test="${product.field85 != null or product.field85 != ''}">
								<div class="details_product_Field85">		
								 <c:out value="${product.field85}" escapeXml="false"/> 			
								</div>
							</c:if>
							<c:if test="${product.field84 != null or product.field84 != ''}">
								<div class="details_product_Field84">		
								 <c:out value="${product.field84}" escapeXml="false"/> 			
								</div>
							</c:if>	
							<div class="details_product_prices">
							<!-- Sales tag begin-->
								 <c:set var="price1" value="0.0" scope="page" />
									<table class="prices" border="0">
										  <c:choose>
										  	<c:when test="${!product.loginRequire or userSession != null}" >
											  <c:forEach items="${product.price}" var="price" varStatus="statusPrice">
											  <c:if test="${price.amt > 0 || siteConfig['SHOW_ZERO_PRICE'].value == 'true'}">
											  <c:choose>
											    <c:when test="${gSiteConfig['gVARIABLE_PRICE'] and product.priceByCustomer}">
											    <tr class="a">  
												    <td colspan="4">&nbsp;</td>
											    </tr>  	
											    </c:when>
											  	<c:when test="${ (statusPrice.first and price.qtyFrom != null and product.salesTag != null)}">
											  	<tr class="b">  
												    <td>&nbsp;</td>
												    <td class="qtyBreakTitle">Quantity</td>
												    <td>&nbsp;</td>
												    <td class="qtyBreakTitle"><c:out value="${siteConfig['PRICE_ONSALE_TITLE'].value}" /><c:if test="${product.caseContent != null and siteConfig['CASE_CONTENT_LAYOUT'].value == ''}">/<c:out value="${siteConfig['CASE_UNIT_TITLE'].value}" /></c:if></td>
												    
											    </tr>
											  	</c:when>
											  	<c:when test="${ (statusPrice.first and price.qtyFrom == null and product.salesTag != null and product.salesTag.discount != 0.0)}">
											  	<tr class="c">  
												    <td>&nbsp;</td>
												    <td class="qtyBreakTitle">&nbsp;</td>
												    <td class="qtyBreakTitle"><c:out value="${siteConfig['PRICE_BEFORE_TITLE'].value}" /></td>
											    	<td class="qtyBreakTitle"><c:out value="${siteConfig['PRICE_ONSALE_TITLE'].value}" /><c:if test="${product.caseContent != null and siteConfig['CASE_CONTENT_LAYOUT'].value == ''}">/<c:out value="${siteConfig['CASE_UNIT_TITLE'].value}" /></c:if></td>
												    
											    </tr>  	
											  	</c:when>
											  	<c:when test="${ (statusPrice.first and price.qtyFrom == null and product.salesTag == null)}">
											  	<tr class="d">   
												    <td colspan="4">&nbsp;</td>
											    </tr>  	
											  	</c:when>
											  </c:choose>
											  <tr>
											    <c:choose>
											      <c:when test="${gSiteConfig['gVARIABLE_PRICE'] and product.priceByCustomer}">
											        <td colspan="4">&nbsp;</td>
											      </c:when>
											      <c:otherwise>
											        <c:set var="multiplier" value="1"/>
											        <c:if test="${siteConfig['CASE_CONTENT_LAYOUT'].value == '1' and product.caseContent > 0}">
													  <c:set var="multiplier" value="${product.caseContent}"/>	
											        </c:if>
											        <td class="priceTitle"><c:if test="${statusPrice.first}"><fmt:message key="f-price" />:</c:if><c:if test="${statusPrice.first == false}">&nbsp;</c:if></td>
												    <td class="qtyBreak">
												    <c:if test="${price.qtyFrom != null}">
												    	${price.qtyFrom}<c:if test="${price.qtyTo != null}">-${price.qtyTo}</c:if><c:if test="${price.qtyTo == null}">+</c:if> <c:choose><c:when test="${product.caseContent != 1}"> <c:out value="${product.packing}" /></c:when><c:otherwise> units</c:otherwise></c:choose>
												    </c:if>
												    &nbsp;</td>
												    <td class="price" <c:if test="${product.salesTag != null and product.salesTag.discount != 0.0}" >style="text-decoration: line-through"</c:if>><fmt:message key="${siteConfig['CURRENCY'].value}" /><span id="pricing"><fmt:formatNumber value="${price.amt*multiplier}" pattern="#,##0.00" /></span></td><c:set var="price1" value="${price.amt}" />
													<c:if test="${product.salesTag != null and product.salesTag.discount != 0.0 }" >
												      <td class="price"><fmt:message key="${siteConfig['CURRENCY'].value}" /><span id="pricing"><fmt:formatNumber value="${price.discountAmt*multiplier}" pattern="#,##0.00" /></span></td>
												      <c:set var="price1" value="${price.discountAmt}" />
												    </c:if>
												    <td class="caseContent" style="color:#990000; font-size:10px; white-space: nowrap">
												      <c:choose>
												        <c:when test="${product.caseContent != null}">(<c:out value="${product.caseContent}" /> <c:out value="${siteConfig['CASE_UNIT_TITLE'].value}" />/<c:out value="${product.packing}" />)</c:when>
												        <c:otherwise><c:out value="${product.packing}" /></c:otherwise>
												      </c:choose>
												    </td>
												  </c:otherwise>
											    </c:choose>  
											  </tr>
											  </c:if>
											  </c:forEach> 
											  <c:if test="${empty product.price and gSiteConfig['gMASTER_SKU'] and not empty product.masterSku}"><a href="${_contextpath}/category.jhtm?cid=852" class="clickForQuote">CLICK FOR QUOTE</a></c:if> 
											</c:when>
											<c:otherwise>
											  <a href="${_contextpath}/login.jhtm?noMessage=t&forwardAction=<c:out value='${model.url}'/>" ><img border="0" name="_logintoviewprice" class="_logintoviewprice" src="${_contextpath}/assets/Image/Layout/login_to_view_price${_lang}.gif" /></a>
											</c:otherwise>
										  </c:choose>	
										  <c:if test="${gSiteConfig['gINVENTORY'] and product.inventory != null}" >
										    <c:if test="${!empty siteConfig['INVENTORY_ON_HAND'].value}" ><div class="inventory_onhand"><c:out value="${siteConfig['INVENTORY_ON_HAND'].value}" escapeXml="false"/>&nbsp;<span><c:out value="${product.inventory}" /></span></div></c:if>
										  </c:if>  
										  <c:if test="${product.savingsAmount != null}">
										  <tr>
										    <td class="youSaveTitle">You Save:</td>
										    <td class="youSave"><c:out value="${product.savingsAmount}" />
										    	(<fmt:formatNumber maxFractionDigits="2"><c:out value="${product.savingsPercentage}" /></fmt:formatNumber>%)</td>
										  </tr>
										  </c:if>
									</table>	
									<!-- Sales tag ends-->								  
							</div>
							<c:if test="${product.msrp != null}">
								<div class="details_product_msrp">
									<c:out value="MSRP: " /><fmt:message key="${siteConfig['CURRENCY'].value}" /><fmt:formatNumber value="${product.msrp}" pattern="#,##0.00" />
								</div>
						   	</c:if>
						</td>	
						<td>
						  <c:if test="${product.field20 != null and !empty product.field20 }">			
							<div class="details_product_swatches">
								<a href="http://hometheatergear.com/category.jhtm?cid=${product.field20}"><img src="${_contextpath}/assets/Image/Layout/click_swatches.gif"></a> 			
						   	</div>
						  </c:if>	
						</td>	
					</tr>			
				</table>
		   	</div>
		   	<c:if test="${ !empty product.productOptions }">
		   		<div class="details_product_options"><%@ include file="/WEB-INF/jsp/frontend/common/productOptions5.jsp" %></div>
		   	</c:if>			   
		   		<div class="details_product_bussiness_ruletable">
		   		<table cellpadding="0" cellspacing="0" width="100%">
			   		<tr>
				   		<c:if test="${product.field21 != null and !empty product.field21 }">
				   		   <td class="details_product_feilds"><div class="details_product_freeshipping"><img class="details_product_freeshipping_image" src="${_contextpath}/assets/Image/Layout/truck.gif"><div class="details_product_freeshipping_msg"><a href="http://hometheatergear.com/category.jhtm?cid=${product.field21}" target="_blank">Has Free Shipping</a></div></div></td>
				   		</c:if>
				   		<c:if test="${product.field19 != null and !empty product.field19 }">
				   			<td class="details_product_feilds">
				   				<div class="details_product_priceguarantee"><a href="${_contextpath}/category.jhtm?cid=${product.field19} " target="_blank">
				   					<img class="details_product_priceguarantee_image" src="${_contextpath}/assets/Image/Layout/price_guarantee.gif"></a>
				   				</div>
				   			</td>	
				   		</c:if>		   			
			   		</tr>
			   		<tr>
				   		<c:if test="${product.field23 != null and !empty product.field23 }">
				   			<td class="details_product_feilds">
					   			<div class="details_product_deliverydays"><a href="${_contextpath}/category.jhtm?cid=126" target="_blank"><img class="details_product_deliverydays_image" src="${_contextpath}/assets/Image/Layout/delivery_box.gif"><c:out  value="${product.field23}" /></a>
					   			</div>
				   			</td>
				   		</c:if>
				   		<c:if test="${product.field18 != null and !empty product.field18 }">
				   			<td class="details_product_feilds">
					   			<div class="details_product_roomplanner"><a href="${_contextpath}/category.jhtm?cid=${product.field18}" target="_blank"><img class="details_product_roomplanner_image" src="${_contextpath}/assets/Image/Layout/room_planner.gif"></a>
								</div>
							</td>
						</c:if>
			   		</tr>
			   		<tr>
				   		<c:if test="${product.field22 != null and !empty product.field22 }">
				   			<td class="details_product_feilds">
					   			<div class="details_product_whiteglove"><img class="details_product_whiteglove_image" src="${_contextpath}/assets/Image/Layout/house.gif"><div class="details_product_whiteglove_msg">White Gloves</div>
					   			</div>
				   			</td>
				   		</c:if>			   			
			   		</tr>
		   		</table>
		   	</div>
		   	</div>
			
			<div class="details_product_addtocart_button">			
			<c:if test="${ (gSiteConfig['gSHOPPING_CART'] and (!empty product.price or product.priceByCustomer)) and (!product.loginRequire or userSession != null)}">
				<input type="hidden" name="product.id" value="${product.id}">
				<div class="details_addInput"><div class="quantity">Quantity: </div><input class="details_product_addtocart_input" type="text" name="quantity_${product.id}" id="quantity_${product.id}" value="1"></div>
				<div class="details_addButton">
				<c:set var="zeroPrice" value="false" /> 
				<c:forEach items="${product.price}" var="price">
					<c:if test="${price.amt == 0}">
						<c:set var="zeroPrice" value="true" />
					</c:if>
				</c:forEach> 
				<c:choose>
					<c:when test="${product.htmlAddToCart != null and product.htmlAddToCart != ''}">
						<c:out value="${product.htmlAddToCart}" escapeXml="false" />
					</c:when>
					<c:otherwise>
						<input type="image" border="0" name="_addtocart" class="_addtocart" src="${_contextpath}/assets/Image/Layout/button_addtocart.gif" />
					</c:otherwise>
				</c:choose> 
				<c:if test="${gSiteConfig['gINVENTORY'] and product.inventory != null and product.inventory < product.lowInventory}">
					<c:out value="${siteConfig['LOW_STOCK_TITLE'].value}" escapeXml="false" />
				</c:if>
				</div>
				<div style="clear:both;"></div>
			</c:if>			
			</div>
			
			</form>
			<div class="details_product_whishList">
			   					<c:if test="${gSiteConfig['gMYLIST'] && product.addToList}">
									<form action="${_contextpath}/addToList.jhtm">
									<input type="hidden" name="product.id" value="${product.id}">
									<div id="addToList">
									<input type="image" border="0" src="${_contextpath}/assets/Image/Layout/add_wish.gif">
									</div>
									</form>
								</c:if>
			   				</div>
			</div>
			
			</div>
			<div class="details_main_lower_container">
			
			<c:if test="${gSiteConfig['gTAB'] > 0 and !empty model.tabList}">
			<div class="details_product_tab_container">
			<div class="details_tab">
				<script src="${_contextpath}/javascript/SimpleTabs1.2.js" type="text/javascript"></script>
				
				<!-- tabs -->
				<div id="tab-block-1">
				<c:forEach items="${model.tabList}" var="tab" varStatus="status">
					<h4 id="${tab.tabNumber}" title="<c:out value="${tab.tabName}" escapeXml="false"/>"> <c:out value="${tab.tabName}" escapeXml="false" /></h4>
					<div>
					<!-- start tab --> 
					<c:choose>
					  <c:when test="${tab.tabNumber == 1}">
					   	<div class="details_product_long_desc_container"><c:out value="${product.longDesc}" escapeXml="false" /></div>
					  </c:when>
					  <c:otherwise>
					  <c:out value="${tab.tabContent}" escapeXml="false" /> 
					  </c:otherwise>
					</c:choose>
					<!-- end tab -->
					</div>
				</c:forEach>
				</div>
			</div>
			</div>	
			</c:if>
			
			
			</div>
			</div>
		</c:if>

	</tiles:putAttribute>
</tiles:insertDefinition>