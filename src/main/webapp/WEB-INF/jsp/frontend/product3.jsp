<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="/WEB-INF/tlds/spring.tld" prefix="spring" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ taglib uri="/WEB-INF/tlds/custom.tld" prefix="wj" %>

<tiles:insertDefinition name="${_template}" flush="true">
  <tiles:putAttribute name="leftbar" value="/WEB-INF/jsp/frontend/layout/template${_leftBar}/leftbar.jsp" />
  <tiles:putAttribute name="content" type="string">

<c:if test="${model.mootools and param.hideMootools == null}">
<script src="javascript/mootools-1.2.5.1-more.js" type="text/javascript"></script>
</c:if>

<c:if test="${model.message != null}">
  <div class="message"><fmt:message key="${model.message}" /></div>
</c:if>

<c:if test="${model.product != null}">

<c:if test="${fn:trim(model.productLayout.headerHtml) != ''}">
  <c:set value="${model.productLayout.headerHtml}" var="headerHtml"/>
  <%@ include file="/WEB-INF/jsp/frontend/common/headerDynamicElement.jsp" %>
  <div><c:out value="${headerHtml}" escapeXml="false"/></div>
</c:if>

<c:set value="${model.product}" var="product"/>
<script language="JavaScript" type="text/JavaScript">
<!--
<c:if test="${siteConfig['DETAILS_IMAGE_WIDTH'].value != '' }" >
function zoomIn() {
	window.open('productImage.jhtm?id=${product.id}&imgUrl=' + document.getElementById("_image").src,'name','width=800,height=600,resizable=yes,scrollbars=1');
}
</c:if>
//-->
</script>

<c:set var="details_images_style" value="" />
<c:set var="details_desc_style" value="" />

<%-- bread crumbs --%>
<c:import url="/WEB-INF/jsp/frontend/layout/${_template}/breadcrumbs.jsp" />

<div class="productLayout3"><c:out value="${product.shortDesc}" escapeXml="false" /></div>
<div class="details_product_name"><h1><c:out value="${product.name}" escapeXml="false" /></h1></div>
<c:if test="${siteConfig['SHOW_SKU'].value == 'true' and product.sku != null}">
	<div class="details_sku"><c:out value="${product.sku}" /></div>  
</c:if>
<table border="0" class="details3" cellspacing="0" cellpadding="0">
<tr valign="top">
 <td>
 
 <c:set var="productImage">
<c:choose>
 <c:when test="${fn:contains(siteConfig['PRODUCT_IMAGE_LAYOUTS'].value,'s2') and 
 		(product.imageLayout == 's2' or (empty product.imageLayout and siteConfig['PRODUCT_IMAGE_LAYOUT'].value == 's2'))}">
    <link rel="stylesheet" href="${_contextpath}/assets/SmoothGallery/slideshow.css" type="text/css" media="screen" />
	<script type="text/javascript" src="javascript/slideshow.js"></script>
	<script type="text/javascript">		
	//<![CDATA[
	  window.addEvent('domready', function(){
	    var data = [<c:forEach items="${product.images}" var="image" varStatus="status">'<c:if test="${!image.absolute}">assets/Image/Product/detailsbig/</c:if><c:out value="${image.imageUrl}"/>'<c:if test="${! status.last}" >,</c:if></c:forEach> ];
	    var myShow = new Slideshow('show', data, { controller: true, height: 300, hu: '', thumbnails: true, width: 400 });
		});
	//]]>
	</script>
	
	<div id="show" class="slideshow"> </div>
    <div id="slideshow_margin"/>
 </c:when>
 <c:when test="${fn:contains(siteConfig['PRODUCT_IMAGE_LAYOUTS'].value,'mb') and 
 		(param.multibox == null or param.multibox != 'off') and
 		(product.imageLayout == 'mb' or (empty product.imageLayout and siteConfig['PRODUCT_IMAGE_LAYOUT'].value == 'mb'))}">
 <link rel="stylesheet" href="${_contextpath}/assets/SmoothGallery/multibox.css" type="text/css" media="screen" />
 <c:set value="${true}" var="multibox"/>
 <script src="javascript/overlay.js" type="text/javascript" ></script>
 <script src="javascript/multibox.js" type="text/javascript" ></script>
 <script type="text/javascript">
	window.addEvent('domready', function(){
		var box = new multiBox('mbxwz', {overlay: new overlay()});
	});
 </script>
 	<div class="details_image_box" id="details_image_boxId" style="${details_images_style}
	 <c:if test="${siteConfig['DETAILS_IMAGE_WIDTH'].value != '' }">width: <c:out value="${siteConfig['DETAILS_IMAGE_WIDTH'].value}"/>px;</c:if>">
	 
	<c:forEach items="${model.product.images}" var="image" varStatus="status">
	  <c:if test="${status.first}">
	    <a href="<c:if test="${!image.absolute}">assets/Image/Product/detailsbig/</c:if><c:out value="${image.imageUrl}"/>" id="mb0" class="mbxwz" title=""><img src="<c:if test="${!image.absolute}">assets/Image/Product/detailsbig/</c:if><c:out value="${image.imageUrl}"/>" border="0" style="<c:if test="${siteConfig['DETAILS_IMAGE_WIDTH'].value != '' }">width: <c:out value="${siteConfig['DETAILS_IMAGE_WIDTH'].value}"/>px</c:if>" alt="<c:out value="${product.name}"/><c:if test="${product.shortDesc != ''}"> - <c:out value="${product.shortDesc}"/></c:if>"/></a>
	  </c:if>
	</c:forEach>
    <div style="clear: both;" >
    <c:forEach items="${model.product.images}" var="image" varStatus="status">
	  <c:if test="${status.count > 1}">
	      <a href="<c:if test="${!image.absolute}">assets/Image/Product/detailsbig/</c:if><c:out value="${image.imageUrl}"/>" id="mb${status.count - 1}" class="mbxwz" title=""><img src="<c:if test="${!image.absolute}">assets/Image/Product/detailsbig/</c:if><c:out value="${image.imageUrl}"/>" border="0" class="details_thumbnail" alt="<c:out value="${product.name}"/><c:if test="${product.shortDesc != ''}"> - <c:out value="${product.shortDesc}"/></c:if>"/></a>
	  </c:if>
	</c:forEach>
	</div>
	</div>        
 </c:when>
 <c:when test="${fn:contains(siteConfig['PRODUCT_IMAGE_LAYOUTS'].value,'qb') and 
 		(product.imageLayout == 'qb' or (empty product.imageLayout and siteConfig['PRODUCT_IMAGE_LAYOUT'].value == 'qb'))}">
 <link rel="stylesheet" href="assets/SmoothGallery/QuickBox/quickbox.css" type="text/css" media="screen" />
 <c:set value="${true}" var="quickbox"/>
 <script src="javascript/QuickBox.js" type="text/javascript" ></script>
 <script type="text/javascript">
	window.addEvent('domready', function(){
		new QuickBox();
	});
 </script>
 	<div class="details_image_box" id="details_image_boxId" style="${details_images_style}
	 <c:if test="${siteConfig['DETAILS_IMAGE_WIDTH'].value != '' }">width: <c:out value="${siteConfig['DETAILS_IMAGE_WIDTH'].value}"/>px;</c:if>">
	 
	<c:forEach items="${model.product.images}" var="image" varStatus="status">
	  <c:if test="${status.first}">
	    <a href="<c:if test="${!image.absolute}">assets/Image/Product/detailsbig/</c:if><c:out value="${image.imageUrl}"/>" id="" class="" title="" rel="quickbox"><img src="<c:if test="${!image.absolute}">assets/Image/Product/detailsbig/</c:if><c:out value="${image.imageUrl}"/>" border="0" style="<c:if test="${siteConfig['DETAILS_IMAGE_WIDTH'].value != '' }">width: <c:out value="${siteConfig['DETAILS_IMAGE_WIDTH'].value}"/>px</c:if>" alt="<c:out value="${product.name}"/><c:if test="${product.shortDesc != ''}"> - <c:out value="${product.shortDesc}"/></c:if>"/></a>
	  </c:if>
	</c:forEach>
    <div style="clear: both;" >
    <c:forEach items="${model.product.images}" var="image" varStatus="status">
	  <c:if test="${status.count > 1}">
	      <a href="<c:if test="${!image.absolute}">assets/Image/Product/detailsbig/</c:if><c:out value="${image.imageUrl}"/>" id="qb${status.count - 1}" class="" title="" rel="quickbox"><img src="<c:if test="${!image.absolute}">assets/Image/Product/detailsbig/</c:if><c:out value="${image.imageUrl}"/>" border="0" class="details_thumbnail" alt="<c:out value="${product.name}"/><c:if test="${product.shortDesc != ''}"> - <c:out value="${product.shortDesc}"/></c:if>"/></a>
	  </c:if>
	</c:forEach>
	</div>
	</div>        
 </c:when>
 <c:otherwise>
    <div class="details_image_box" id="details_image_boxId" style="${details_images_style}
	 <c:if test="${siteConfig['DETAILS_IMAGE_WIDTH'].value != '' }">width: <c:out value="${siteConfig['DETAILS_IMAGE_WIDTH'].value}"/>px;</c:if>">
	 
	<c:forEach items="${model.product.images}" var="image" varStatus="status">
	<c:if test="${status.first}">
		 <img src="<c:if test="${!image.absolute}">assets/Image/Product/detailsbig/</c:if><c:out value="${image.imageUrl}"/>" border="0" id="_image" name="_image" class="details_image"
		 	style="<c:if test="${siteConfig['DETAILS_IMAGE_WIDTH'].value != '' }">width: <c:out value="${siteConfig['DETAILS_IMAGE_WIDTH'].value}"/>px</c:if>" alt="<c:out value="${product.name}"/><c:if test="${product.shortDesc != ''}"> - <c:out value="${product.shortDesc}"/></c:if>">
	<c:if test="${siteConfig['DETAILS_IMAGE_WIDTH'].value != '' }" >
	<div align="right">
	<a onclick="zoomIn()" title="Zoom in"><img src="${_contextpath}/assets/Image/Layout/zoom-icon.gif" alt="Zoom In" width="16" height="16" /></a>
	</div>
	</c:if>
	</c:if>
	
	<c:if test="${status.last and (status.count != 1)}">
	<br>
	<c:forEach items="${model.product.images}" var="thumb">
	<a href="#" class="details_thumbnail_anchor" onClick="_image.src='<c:if test="${!thumb.absolute}">assets/Image/Product/detailsbig/</c:if><c:out value="${thumb.imageUrl}"/>';return false;">
		 <img src="<c:if test="${!thumb.absolute}">assets/Image/Product/detailsbig/</c:if><c:out value="${thumb.imageUrl}"/>" class="details_thumbnail" alt="<c:out value="${product.name}"/><c:if test="${product.shortDesc != ''}"> - <c:out value="${product.shortDesc}"/></c:if>">
	</a>
	</c:forEach>
	</c:if>
	</c:forEach>
	</div>
	<c:if test="${fn:contains(siteConfig['PRODUCT_IMAGE_LAYOUTS'].value,'ss') and 
			(product.imageLayout == 'ss'  or (empty product.imageLayout and siteConfig['PRODUCT_IMAGE_LAYOUT'].value == 'ss'))}">
	<object classid="clsid:D27CDB6E-AE6D-11cf-96B8-444553540000" codebase="http://download.macromedia.com/pub/shockwave/cabs/flash/swflash.cab#version=6,0,29,0" width="750" height="475">
	  <param name="movie" value="assets/Flash/slideshow.swf?file=thumbnailer1.jhtm?id=${product.id}">
	  <param name="quality" value="high">
	  <embed src="${_contextpath}/assets/Flash/slideshow.swf?file=thumbnailer1.jhtm?id=${product.id}" quality="high" pluginspage="http://www.macromedia.com/go/getflashplayer" type="application/x-shockwave-flash" width="750" height="475"></embed>
	</object>
	</c:if>
 </c:otherwise>
</c:choose>
</c:set>

<c:out value="${productImage}" escapeXml="false" />
</td>
<td>
<table border="0" class="details_desc" width="100%" cellspacing="0" cellpadding="0">
<tr valign="top">
 <td width="100%">
 <form action="${_contextpath}/addToCart.jhtm" name="this_form" method="post" onsubmit="return checkForm()">
 <table border="0" class="priceSection" cellspacing="0" cellpadding="0">
    <c:if test="${ (gSiteConfig['gSHOPPING_CART'] and (!empty product.price or product.priceByCustomer)) and (!product.loginRequire or userSession != null)}">
      <tr>
        <td>
	        <table border="0" class="prices" cellspacing="0" cellpadding="0">
	        <c:if test="${product.msrp != null}">
			<tr>
				<td class="msrp">
				  <span>Regular Price </span><fmt:message key="${siteConfig['CURRENCY'].value}" /><fmt:formatNumber value="${product.msrp}" pattern="#,##0.00" />
				</td>
			</tr>	
			</c:if>
			<tr>
				<td class="price">
				  <span>Your Price </span><fmt:message key="${siteConfig['CURRENCY'].value}" /><fmt:formatNumber value="${product.price1}" pattern="#,##0.00" />
				</td>
			</tr>	
			</table>
        </td>
        <td align="right">
			<input type="hidden" name="product.id" value="${product.id}">
        	<input type="hidden" name="quantity_${product.id}" id="quantity_${product.id}" value="1">
       		<input type="image" border="0" name="_addtocart" class="_addtocart" src="${_contextpath}/assets/Image/Layout/button_addtocart${_lang}.gif" />
        </td>
      </tr> 
      <c:if test="${gSiteConfig['gINVENTORY'] and product.inventory != null and !empty siteConfig['INVENTORY_ON_HAND'].value}" >
	    <tr class="inventory_onhand_wrapper">
	      <td><div class="inventory_onhand"><c:out value="${siteConfig['INVENTORY_ON_HAND'].value}" escapeXml="false"/>&nbsp;<span><c:out value="${product.inventory}" /></span></div></td>     
	    </tr>
      </c:if> 
    </c:if>
      <tr>
        <td colspan="2">
        <div class="details_long_desc"><c:out value="${product.longDesc}" escapeXml="false" /></div> 
    	<div class="details_options">
    		<c:choose>
			<c:when test="${siteConfig['SMART_DROPDOWN'].value == 'true'}">
			  <%@ include file="/WEB-INF/jsp/frontend/common/productOptions5.jsp" %>
			</c:when>
			<c:otherwise>
			  <%@ include file="/WEB-INF/jsp/frontend/common/productOptions.jsp" %>
			</c:otherwise>
			</c:choose>
    	</div>
        </td>
      </tr>
    </table>
    </form>  
 </td>
</tr>
<tr>
 <td>
	<c:choose>
	<c:when test="${gSiteConfig['gPRODUCT_REVIEW'] and product.enableRate}">
	<%@ include file="/WEB-INF/jsp/frontend/common/quickProductReview.jsp" %>
	</c:when>
	<c:when test="${siteConfig['PRODUCT_RATE'].value == 'true' and product.enableRate}">
	<%@ include file="/WEB-INF/jsp/frontend/common/productRate.jsp" %>
	</c:when>
	</c:choose>
	
	<c:if test="${gSiteConfig['gMYLIST'] && product.addToList}">
	<form action="${_contextpath}/addToList.jhtm">
	<input type="hidden" name="product.id" value="${product.id}">
	<div id="addToList">
	<input type="image" border="0" src="${_contextpath}/assets/Image/Layout/button_addtolist${_lang}.gif">
	</div>
	</form>
	</c:if>
	
	<c:if test="${gSiteConfig['gCOMPARISON'] && product.compare}">
	<form action="${_contextpath}/comparison.jhtm" method="get">
	<%-- 
	<input type="hidden" name="forwardAction" value="<c:out value='${model.url}'/>">
	--%>
	<input type="hidden" name="product.id" value="${product.id}">
	<input type="image" border="0" src="${_contextpath}/assets/Image/Layout/button_compare${_lang}.gif">
	</form>
	</c:if>
  
	<c:forEach items="${product.productFields}" var="productField" varStatus="row">
	<c:if test="${row.first}">
	<c:if test="${siteConfig['PRODUCT_FIELDS_TITLE'].value != ''}">
	<div class="details_fields_title"><c:out value="${siteConfig['PRODUCT_FIELDS_TITLE'].value}"/></div>
	</c:if>
	<table class="details_fields">
	</c:if>
	<tr>
	<td class="details_field_name_row${row.index % 2}"><c:out value="${productField.name}" escapeXml="false" /> </td>
	<c:choose>
	<c:when test="${productField.formatType != null and productField.formatType != '' and productField.fieldType == 'number' }">
	<td class="details_field_value_row${row.index % 2}"><fmt:formatNumber value="${wj:trimAndSplit(productField.value, '|' , false, ',', true, true)}" pattern="${productField.formatType }" /></td>
	</c:when>
	<c:otherwise>
	<td class="details_field_value_row${row.index % 2}"><c:out value="${wj:trimAndSplit(productField.value, '|' , false, ',', true, true)}" escapeXml="false"/></td>
	</c:otherwise>
	</c:choose>	
	</tr>
	<c:if test="${row.last}">
	</table>
	</c:if>
	</c:forEach>
 </td>
</tr>
</table>
 </td>

</tr>
</table>

<table border="0" class="details3" cellspacing="0" cellpadding="0">
<tr valign="top">
 <td>
 <table border="0" cellspacing="0" cellpadding="0" style="width: 100%;">
 <tr>
 <c:if test="${model.recommendedList != null or model.alsoConsiderMap[product.id] != null}">
 <td valign="top">
    <table border="0" class="leftBox" cellspacing="0" cellpadding="0">
	 <tr>
	 <td valign="top">
	   <c:if test="${model.alsoConsiderMap[product.id] != null}">
	   <c:set var="product" value="${model.alsoConsiderMap[product.id]}" />
	   <table class="alsoconsider_list_box" border="0" cellspacing="0" cellpadding="0">
	     <tr valign="top">
	       <td>
	       <c:if test="${siteConfig['ALSO_CONSIDER_TITLE'].value != ''}">
             <div class="also_consider_title"><c:out value="${siteConfig['ALSO_CONSIDER_TITLE'].value}"/></div>
           </c:if>
	       </td>
	     </tr>
	     <tr>
	       <td align="center">
	       <div class="also_consider_list">
	       <ul class="recList">
	         <li>
	           <table style="width:100%" border="0" cellspacing="0" cellpadding="0">
				<tr class="image" valign="top">
	               <td align="center">
	                 <a href="product.jhtm?id=${product.id}"><img src="<c:if test="${!product.thumbnail.absolute}">assets/Image/Product/thumb/</c:if><c:out value="${product.thumbnail.imageUrl}"/>" border="0" class="also_consider_image"<c:if test="${siteConfig['ALSO_CONSIDER_THUMB_HEIGHT'].value != ''}">height="<c:out value="${siteConfig['ALSO_CONSIDER_THUMB_HEIGHT'].value}"/>"</c:if>></a>
	               </td>
	             </tr>
	             <tr class="name" valign="top">
	               <td align="center">
	                 <a href="product.jhtm?id=${product.id}" class="also_consider_item_name"><c:out value="${product.name}" escapeXml="false" /></a>
	               </td>
	             </tr>
	           </table>
	         </li>
	       </ul>
	       </div>
	       </td>
	     </tr>  
	   </table>  
	   <c:set value="${model.product}" var="product"/>
	   </c:if>
	 </td>
	 </tr>
	 <tr>
	 <td valign="top">
	 <c:if test="${model.recommendedList != null}">
	   <table class="recommended_list_box" border="0" cellspacing="0" cellpadding="0">
	     <tr>
	       <td>
	       <c:choose>
			 <c:when test="${!empty product.recommendedListTitle }">
			  <div class="recommended_title"><c:out value="${product.recommendedListTitle}"/></div>
			 </c:when>
			 <c:otherwise>
			  <c:if test="${siteConfig['RECOMMENDED_LIST_TITLE'].value != ''}">
			   <div class="recommended_title"><c:out value="${siteConfig['RECOMMENDED_LIST_TITLE'].value}"/></div>
			  </c:if>
			 </c:otherwise>
			</c:choose>
	       </td>
	     </tr>
	     <tr>
	       <td align="center">
	       <div class="recommendedList">
	       <ul class="recList">
	       <c:forEach items="${model.recommendedList}" var="product" varStatus="status">
	         <li>
	           <table style="width:100%" border="0" cellspacing="0" cellpadding="0">
				<tr class="image" valign="top">
	               <td align="center">
	                 <a href="product.jhtm?id=${product.id}"><img src="<c:if test="${!product.thumbnail.absolute}">assets/Image/Product/thumb/</c:if><c:out value="${product.thumbnail.imageUrl}"/>" border="0" class="thumbnail_image" alt="<c:out value="${product.name}"/><c:if test="${product.shortDesc != ''}"> - <c:out value="${product.shortDesc}"/></c:if>"></a>
	               </td>
	             </tr>
	             <tr class="name" valign="top">
	               <td align="center">
	                 <a href="product.jhtm?id=${product.id}" class="thumbnail_item_name"><c:out value="${product.shortDesc}" escapeXml="false" /></a>
	               </td>
	             </tr>
	           </table>
	         </li>
	       </c:forEach>
	       </ul>
	       </div>
	       </td>
	     </tr>
	   </table>
	    </c:if>
	 </td>
	 </tr>
	 </table>
 </td>
 </c:if>	 
 <td valign="top" style="width:100%;">
 <%@ include file="/WEB-INF/jsp/frontend/common/productTabs.jsp" %>
 </td>
 </tr>
 </table>
 </td>

</tr> 
</table>

<c:if test="${gSiteConfig['gPRODUCT_REVIEW'] and siteConfig['PRODUCT_RATE'].value != 'true' and model.product.enableRate}">
<div class="reviewList">
<%@ include file="/WEB-INF/jsp/frontend/common/productReviewList.jsp" %>
</div>
</c:if>

<c:if test="${fn:trim(model.productLayout.footerHtml) != ''}">
  <c:set value="${model.productLayout.footerHtml}" var="footerHtml"/>
  <%@ include file="/WEB-INF/jsp/frontend/common/footerDynamicElement.jsp" %>
  <div style="float:left;width:100%"><c:out value="${footerHtml}" escapeXml="false"/></div>
</c:if>
</c:if>
    
  </tiles:putAttribute>
</tiles:insertDefinition>