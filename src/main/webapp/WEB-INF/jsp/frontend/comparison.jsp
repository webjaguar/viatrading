<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>

<tiles:insertDefinition name="${_template}" flush="true">
  <tiles:putAttribute name="content" type="string">
<c:if test="${gSiteConfig['gCOMPARISON']}">

<div class="comparisonTitle"><c:out value="${siteConfig['COMPARISON_TITLE'].value}"/></div>

<c:if test="${model.message != null}">
  <div class="message"><fmt:message key="${model.message}"><fmt:param value="${siteConfig['COMPARISON_MAX'].value}"/></fmt:message></div>
</c:if>

<c:if test="${fn:length(model.products) == 0}">
  <div class="message"><fmt:message key="comparison.empty" /></div>
</c:if>


<c:if test="${fn:length(model.products)> 0}">
<script language="JavaScript">
<!--
function toggleAll(el) {
	var ids = document.getElementsByName("__selected_id");	
  	if (ids.length == 1)
      document.form.__selected_id.checked = el.checked;
    else
      for (i = 0; i < ids.length; i++)
        document.form.__selected_id[i].checked = el.checked;	
}  

function deleteSelected() {
	var ids = document.getElementsByName("__selected_id");	
  	if (ids.length == 1) {
      if (document.form.__selected_id.checked) {
    	return true;
      }
    } else {
      for (i = 0; i < ids.length; i++) {
        if (document.form.__selected_id[i].checked) {
    	  return true;
        }
      }
    }

    alert("Please select product(s) to remove.");       
    return false;
}

function addToCart(aform) {
    aform.action = 'addToCart.jhtm';
    submit();
}
//-->
</script>

<form name="form" action="${_contextpath}/comparison.jhtm" method="post">
<table width="90%" align="center" cellspacing="5" cellpadding="0" border="0">
  <tr>
	<td>
<table border="0" cellpadding="3" cellspacing="1" width="100%" class="comparison"> 
  <tr>
    <td class="comparisonHdr">Image</td>
	<c:forEach items="${model.products}" var="product" varStatus="status">
    <td class="comparisonCol${status.index % 2}">
      <c:if test="${product.thumbnail != null}">
	    <img class="compareImage" border="0" <c:if test="${!product.thumbnail.absolute}"> src="<c:url value="${_contextpath}/assets/Image/Product/thumb/${product.thumbnail.imageUrl}"/>" </c:if> <c:if test="${product.thumbnail.absolute}"> src="<c:url value="${product.thumbnail.imageUrl}"/>" </c:if> />
	  </c:if>
    </td>
	</c:forEach> 
  </tr>
  <tr>
    <td class="comparisonHdr">Name</td>
	<c:forEach items="${model.products}" var="product" varStatus="status">
    <td class="comparisonCol${status.index % 2}"><a href="product.jhtm?id=${product.id}" 
	  			class="comparisonNameLink"><c:out value="${product.name}" /></a></td>
	</c:forEach> 
  </tr>
<c:if test="${siteConfig['COMPARISON_SHORT_DESC_TITLE'].value != ''}">
  <tr>
    <td class="comparisonHdr"><c:out value="${siteConfig['COMPARISON_SHORT_DESC_TITLE'].value}"/></td>
	<c:forEach items="${model.products}" var="product" varStatus="status">
    <td class="comparisonCol${status.index % 2}"><c:out value="${product.shortDesc}" escapeXml="false"/></td>
	</c:forEach> 
  </tr>
</c:if>
  <tr>
    <td class="comparisonHdr">Price</td>
	<c:forEach items="${model.products}" var="product" varStatus="status">
    <td class="comparisonCol${status.index % 2}">
<c:if test="${product.priceRangeMinimum != null}">
  <c:choose>
  	<c:when test="${!product.loginRequire or (product.loginRequire and userSession != null)}" >
  	  <div class="comparisonPrice">
	  <fmt:message key="${siteConfig['CURRENCY'].value}" /><fmt:formatNumber value="${product.priceRangeMinimum}" pattern="#,##0.00" />
	  <c:if test="${product.priceRangeMaximum != null}">
	  -
	  <fmt:message key="${siteConfig['CURRENCY'].value}" /><fmt:formatNumber value="${product.priceRangeMaximum}" pattern="#,##0.00" />
	  </c:if>
	  </div>
	</c:when>
	<c:otherwise>
	  <a href="${_contextpath}/login.jhtm?noMessage=t&forwardAction=<c:out value='${model.url}'/>" ><img border="0" name="_logintoviewprice" class="_logintoviewprice" src="${_contextpath}/assets/Image/Layout/login_to_view_price${_lang}.gif" /></a>
	</c:otherwise>
  </c:choose>
</c:if>    
    </td>
	</c:forEach>   
  </tr>
  <c:forEach items="${model.productFields}" var="productField">
  <c:if test="${productField.comparisonField}">
  <tr>
  	<td class="comparisonHdr"><c:out value="${productField.name}" escapeXml="false" /></td>
    <c:set var="status" value="0"/>
	<c:forEach items="${model.products}" var="product">
	<c:forEach items="${product.productFields}" var="productField2">
    <c:if test="${productField2.comparisonField and (productField.id == productField2.id)}">
    <td class="comparisonCol${status % 2}"><c:out value="${productField2.value}" escapeXml="false" />&nbsp;</td>
    <c:set var="status" value="${status+1}"/>
    </c:if>
    </c:forEach>
	</c:forEach> 
  </tr>
  </c:if>
  </c:forEach>
  <tr>
    <td class="comparisonHdr">&nbsp;</td>
	<c:forEach items="${model.products}" var="product" varStatus="status">
    <td class="comparisonCol${status.index % 2}"><input name="__selected_id" value="${product.id}" type="checkbox"></td>
	</c:forEach> 
  </tr>
</table>
	</td>
  </tr>
  <tr>
	<td align="right"><input type="submit" name="__delete" value="Remove Selected Products" onClick="return deleteSelected()"></td>
  </tr>
</table>
</form>
</c:if>

</c:if>
  </tiles:putAttribute>
</tiles:insertDefinition>
