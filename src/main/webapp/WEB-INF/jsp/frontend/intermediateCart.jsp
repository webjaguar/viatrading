<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>

<tiles:insertDefinition name="${_template}" flush="true">
  <tiles:putAttribute name="content" type="string">

<div class="shoppingCartHdr"><fmt:message key="f_cart_intermidiate__selectOptions" /></div>

<c:forEach items="${model.productsWithOptions}" var="cartItem" varStatus="status">
    <c:set value="${cartItem.product}" var="product"/>
    <%@ include file="/WEB-INF/jsp/frontend/quickmode/view1.jsp" %>
</c:forEach>
 
  </tiles:putAttribute>
</tiles:insertDefinition>
