<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ taglib uri="/WEB-INF/tlds/custom.tld" prefix="wj" %>

<tiles:insertDefinition name="${_template}" flush="true">
  <tiles:putAttribute name="content" type="string">
  
<c:out value="${model.quoteListLayout.headerHtml}" escapeXml="false"/>

<c:if test="${model.products.nrOfElements > 0}">
<script type="text/javascript" language="JavaScript">
<!--
function deleteSelected() {
	var ids = document.getElementsByName("__selected_id");	
  	if (ids.length == 1) {
      if (document.form.__selected_id.checked) {
    	return true;
      }
    } else {
      for (i = 0; i < ids.length; i++) {
        if (document.form.__selected_id[i].checked) {
    	  return true;
        }
      }
    }

    alert("Please select product(s) to remove.");       
    return false;
}

//-->
</script>


  
<form action="${_contextpath}/quoteViewList.jhtm" name="form" method="post" id="quoteForm" >
<c:forEach items="${model.groupProductsMap}" var="groupProductMap" >
<div class="tableTitle" align="left">Type: <c:out value="${groupProductMap.key}"></c:out></div>

	<input type="hidden" name="adminEmailSubject"  id="adminEmailSubject" value="${siteConfig['QUOTE_ADMIN_EMAIL_SUBJECT'].value}"/>	
	<input type="hidden" value="${model.size}" id="size"/>	
	<table border="0" cellpadding="3" cellspacing="1" align="center" class="quickmode">
	<tr>
	   <c:if test="${siteConfig['QUICK_MODE_THUMB_HEIGHT'].value > 0 or siteConfig['QUICK_MODE_THUMB_HEIGHT'].value == ''}" >
	    	<th class="quickmode imageHeader"><fmt:message key="image" /></th>
	   </c:if> 
	   <c:if test="${siteConfig['SHOW_SKU'].value == 'true'}">
	    <th class="quickmode productSkuHeader"><fmt:message key="productSku" /></th>
	   </c:if> 
	    <th class="quickmode productNameHeaderId" id="productNameHeaderId"><fmt:message key="f_productName" /></th>
		<c:if test="${siteConfig['QUICK_MODE_SHORT_DESC_TITLE'].value != '' and (showDesc == null or showDesc)}">
		    <th class="quickmode SDescHeader"><c:out value="${siteConfig['QUICK_MODE_SHORT_DESC_TITLE'].value}"/></th>
		</c:if>
		<c:forEach items="${model.productFieldsMap[groupProductMap.key]}" var="productField">
		  <c:if test="${productField.quickModeField}">
		    <th class="quickmode fieldNameHeader"><c:out value="${productField.name}" escapeXml="false" /></th>
		  </c:if>
		</c:forEach>
		<c:if test="${model.showPriceColumn == null or model.showPriceColumn}">
		  <th class="quickmode priceQuoteHeader"><fmt:message key="f_price_quote" /></th>
		</c:if>
		<c:if test="${model.showOptionColumn == null or model.showOptionColumn}">
		<th class="quickmode optionQuoteHeaderOption"><fmt:message key="f_option_quote" /></th>
		</c:if>
		<th class="quickmode qtyHeader"><fmt:message key="f_quantity_quickMode" /></th>
		<th class="quickmode removeHeader"><fmt:message key="remove" /></th>
	</tr>
	<c:forEach items="${groupProductMap.value}" var="product" varStatus="status">	
	<input type="hidden" value="${product.id}" name="product_Id" />	
	<input type="hidden" value="${product.sku}" name="productSku" />
	<input type="hidden" value="${product.name}" name="productName" />	
	<tr class="row${status.index % 2}">	
		  <c:if test="${(showImage == null or showImage) and (siteConfig['QUICK_MODE_THUMB_HEIGHT'].value > 0 or siteConfig['QUICK_MODE_THUMB_HEIGHT'].value == '')}" >
		 	<td class="list image">
			  <div align="center">
			    <c:if test="${product.thumbnail == null}">&nbsp;</c:if>
			    <c:if test="${product.thumbnail != null}">
				  <a href="${productLink}">
			  	  <c:choose>
			  	   <c:when test="${product.asiId != null}">
			  	   <img src="<c:out value="${fn:replace(product.thumbnail.imageUrl, 'prodimgs', 'prodthumbs')}"/>" border="0" class="thumbnail_image" alt="<c:out value="${product.name}"/><c:if test="${product.shortDesc != ''}"> - <c:out value="${product.shortDesc}"/></c:if>">
			  	   </c:when>
			  	   <c:otherwise>
			  	   <img src="<c:if test="${!product.thumbnail.absolute}">${_contextpath}/assets/Image/Product/thumb/</c:if><c:out value="${fn:replace(product.thumbnail.imageUrl, 'http://www.dollaritem.com/wms/images/catalog/', 'http://www.dollaritem.com/wms/images/thumbs/')}"/>" border="0" class="thumbnail_image" alt="<c:out value="${product.name}"/><c:if test="${product.shortDesc != ''}"> - <c:out value="${product.shortDesc}"/></c:if>">
			  	   </c:otherwise>
			  	  </c:choose>
			  	  </a>
			    </c:if>
			  </div>
			</td>
		  </c:if>
	      <c:if test="${siteConfig['SHOW_SKU'].value == 'true'}">
    	   <td class="list sku"><c:out value="${product.sku}" /></td>
    	  </c:if>	
	      <td class="list name"><a href="product.jhtm?id=${product.id}" class="comparisonNameLink"><c:out value="${product.name}" /></a></td>
	      <c:if test="${siteConfig['QUICK_MODE_SHORT_DESC_TITLE'].value != '' and (showDesc == null or showDesc)}">
			<td class="list shortDesc"><c:out value="${product.shortDesc}" escapeXml="false" /></td>
		  </c:if>	    	
	      <c:forEach items="${product.productFields}" var="productField">
			<c:if test="${productField.quickModeField}">
			 <td class="list productField" align="center"><c:out value="${wj:trimAndSplit(productField.value, '|' , false, ',', true, true)}" escapeXml="false"/></td>
			</c:if>
		  </c:forEach>			
    	  <c:if test="${model.showPriceColumn == null or model.showPriceColumn}">    		
			  <td class="list price" width="30%" style="white-space: nowrap">
				<input type="hidden" name="product.id" value="${product.id}">
				<c:choose>
				<c:when test="${product.salesTag != null and !product.salesTag.percent}">
				  <%@ include file="/WEB-INF/jsp/frontend/common/price3.jsp" %>
				</c:when>
				<c:otherwise>
				  <%@ include file="/WEB-INF/jsp/frontend/common/price.jsp" %>
				</c:otherwise>
				</c:choose>
				<%@ include file="/WEB-INF/jsp/frontend/common/masterSku.jsp" %>
			  </td>	
		  </c:if>
		  <c:if test="${model.showOptionColumn == null or model.showOptionColumn}">
	    	  <td class="list options"><%@ include file="/WEB-INF/jsp/frontend/common/productOptions.jsp" %></td>
	      </c:if>
	      <td class="list qty">
    		<c:set var="productQuantity" value="quantity_${product.id}"></c:set>
	    	<input name="quantity_${product.id}" id="quantity_${product.id}" value="${model[productQuantity]}" type="text" size="6" class="quantityValue" />    	
	      </td>
	    	<td class="list remove"><input name="__selected_id" value="${product.id}" type="checkbox" ></td>
	</tr>	
	</c:forEach>
	</table>
	<br/>
	<div  id="quickmode_v1_addtocart" align="right">
		<input type="submit" name="__delete" value="Remove" onClick="return deleteSelected();">
	</div>
	
</c:forEach>
	
	<%--FORM to save the contact in CRM --%>
	 
	<div id="quoteWrapperId" class="quoteWrapper">	
	<div class="header"><fmt:message key="f_quoteHeaderText"/></div>
	
	<!-- Error Message -->
  	<c:if test="${model.message != null}">
  		<div class="message"><fmt:message key="${model.message}"/></div>
	</c:if>
	<!-- Success Message -->	
	<c:if test="${model.successMessage != null}">
  		<div class="message"><fmt:message key="${model.successMessage}"/></div>
	</c:if>	
	  
		<table border="0" cellpadding="4" cellspacing="1">
		  	<tr>	
				<td><label class="AStitle"><fmt:message key="firstName"/></label></td>
				<td>
					<input name="firstName" id="firstName" type="text" value="${model.customer.address.firstName}" size="25"/>
				</td>	
			</tr>
			<tr>
				<td><label class="AStitle"><fmt:message key="lastName"/></label></td>
				<td><input name="lastName" id="lastName" type="text" value="${model.customer.address.lastName}" size="25" /></td>	
			</tr>
			<tr>
				<td><label class="AStitle"><fmt:message key="company"/></label></td>
				<td><input name="company" id="company" type="text" value="${model.customer.address.company}" size="25" /></td>
			</tr>
			<tr>
				<td><label class="AStitle"><fmt:message key="address"/></label></td>
				<td><input name="address" id="address" type="text" value="${model.customer.address.addr1}" size="25" /></td>
			</tr>
			<tr>
				<td><label class="AStitle"><fmt:message key="city"/></label></td>
				<td><input name="city" id="city" type="text" value="${model.customer.address.city}" size="25" /></td>
			</tr>
			<tr>
				<td><label class="AStitle"><fmt:message key="state"/></label></td>
				<td><input name="state" id="state" type="text" value="${model.customer.address.stateProvince}" size="25" /></td>
			</tr>
			<tr>
				<td><label class="AStitle"><fmt:message key="zipCode"/></label></td>
				<td><input name="zipCode" id="zipCode" type="text" value="${model.customer.address.zip}" size="25" /></td>
			</tr>
			<tr>
				<td><label class="AStitle"><fmt:message key="country"/></label></td>
				<td><input name="country" id="country" type="text"  value="${model.customer.address.country}" size="25" /></td>
			</tr>
			<tr>
				<td><label class="AStitle"><fmt:message key="email"/></label></td>
				<td><input name="email" id="email" type="text" value="${model.customer.username}" size="25" /></td>
			</tr>
			<tr>
				<td><label class="AStitle"><fmt:message key="phone"/></label></td>
				<td><input name="phone" id="phone" type="text" value="${model.customer.address.phone}" size="25" /></td>
			</tr>
			<tr>
				<td><label class="AStitle"><fmt:message key="message"/></label></td>
				<td><textarea name="note" id="note">${model.note}</textarea></td>
			</tr>
			<tr>
			<td align="center">
				<div id="button" >
					<input  type="submit" value="Send" name="send"  onclick="saveContact();"/>
				</div>
			</td>
			</tr>
		</table>	
	</div> 	
</form>
</c:if>

<c:out value="${model.quoteListLayout.footerHtml}" escapeXml="false"/>

	</tiles:putAttribute>
</tiles:insertDefinition>