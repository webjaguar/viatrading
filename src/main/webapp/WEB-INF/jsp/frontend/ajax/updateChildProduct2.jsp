<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ taglib uri="/WEB-INF/tlds/custom.tld" prefix="wj" %>

<c:choose>
 <c:when test="${fn:contains(siteConfig['PRODUCT_IMAGE_LAYOUTS'].value,'mz') and 
                (product.imageLayout == 'mz' or (empty product.imageLayout and siteConfig['PRODUCT_IMAGE_LAYOUT'].value == 'mz'))}">
   <link rel="stylesheet" href="${_contextpath}/assets/SmoothGallery/MagicZoomPlus/magiczoomplus.css" type="text/css" media="screen" />
   <script src="${_contextpath}/javascript/magiczoomplus.js" type="text/javascript" ></script>
   <div class="details_image_box" id="details_image_boxId" style="${details_images_style}
        <c:if test="${siteConfig['DETAILS_IMAGE_WIDTH'].value != '' }">width: <c:out value="${siteConfig['DETAILS_IMAGE_WIDTH'].value}"/>px;</c:if>">
     <c:forEach items="${model.product.images}" var="image" varStatus="status">
       <c:if test="${status.first}">
         <a href="<c:if test="${!image.absolute}">${_contextpath}/assets/Image/Product/superBig/</c:if><c:out value="${fn:replace(image.imageUrl, 'detailsbig', 'superBig')}"/>" rel="zoom-width:480px;zoom-height:360px;expand-align:screen; expand-position:top=0; expand-size:original; selectors-effect:pounce; zoom-position:#zoomImageId;" class="MagicZoomPlus" id="Zoomer" >
           <img alt="" src="<c:if test="${!image.absolute}">${_contextpath}/assets/Image/Product/detailsbig/</c:if><c:out value="${fn:replace(product.thumbnail.imageUrl, 'size=large', 'size=normal')}"/>" border="0" class="normal_image" alt="<c:out value="${product.name}"/><c:if test="${product.shortDesc != ''}"> - <c:out value="${product.shortDesc}"/></c:if>" />
         </a>
       </c:if>
     </c:forEach>
     <div style="clear: both;" >
       <div id="thumbnailWrapper">
         <c:forEach items="${model.product.images}" var="thumbImage" varStatus="status">
           <c:if test="${status.count > 0}">
             <c:choose>
               <c:when test="${product.asiId != null or true}">
                 <a class="thumb_link" rev="<c:if test="${!thumbImage.absolute}">${_contextpath}/assets/Image/Product/detailsbig/</c:if><c:out value="${fn:replace(thumbImage.imageUrl, 'size=large', 'size=normal')}"/>" rel="zoom-id:Zoomer;zoom-width:480px;zoom-height:360px;zoom-position:#zoomImageId;" href="<c:if test="${!thumbImage.absolute}">${_contextpath}/assets/Image/Product/superBig/</c:if><c:out value="${fn:replace(thumbImage.imageUrl, 'detailsbig', 'superBig')}"/>" style="outline: 0pt none; display: inline-block;">
                   <img alt="" src="<c:if test="${!thumbImage.absolute}">${_contextpath}/assets/Image/Product/thumb/</c:if><c:out value="${fn:replace(thumbImage.imageUrl, 'size=large', 'size=small')}"/>">
                 </a>
               </c:when>
               <c:otherwise>
                 <a href="<c:if test="${!image.absolute}">${_contextpath}/assets/Image/Product/detailsbig/</c:if><c:out value="${image.imageUrl}"/>" rel="zoom-id:Zoomer;zoom-width:480px;zoom-height:360px;zoom-position:#zoomImageId;"  rev="<c:if test="${!image.absolute}">${_contextpath}/assets/Image/Product/thumb/</c:if><c:out value="${image.imageUrl}"/>" title="" >
                 <img src="<c:if test="${!image.absolute}">${_contextpath}/assets/Image/Product/thumb/</c:if><c:out value="${image.imageUrl}"/>" border="0" class="zoom_details_thumbnail" alt="<c:out value="${product.name}"/><c:if test="${product.shortDesc != ''}"> - <c:out value="${product.shortDesc}"/></c:if>"/></a>
               </c:otherwise>
             </c:choose>
           </c:if>
         </c:forEach>
       </div>
     </div>
   </div>
 </c:when>
 <c:when test="${fn:contains(siteConfig['PRODUCT_IMAGE_LAYOUTS'].value,'qb') and 
 		(product.imageLayout == 'qb' or (empty product.imageLayout and siteConfig['PRODUCT_IMAGE_LAYOUT'].value == 'qb'))}">
   <link rel="stylesheet" href="${_contextpath}/assets/SmoothGallery/QuickBox/quickbox.css" type="text/css" media="screen" />
   <c:set value="${true}" var="quickbox"/>
   <script src="javascript/QuickBox.js" type="text/javascript" ></script>
   <script type="text/javascript">
	  window.addEvent('domready', function(){
		new QuickBox();
	  });
   </script>
   <div class="details_image_box" id="details_image_boxId" style="${details_images_style}
	    <c:if test="${siteConfig['DETAILS_IMAGE_WIDTH'].value != '' }">width: <c:out value="${siteConfig['DETAILS_IMAGE_WIDTH'].value}"/>px;</c:if>">
	 <c:forEach items="${model.product.images}" var="image" varStatus="status">
	   <c:if test="${status.first}">
	     <a href="<c:if test="${!image.absolute}">${_contextpath}/assets/Image/Product/detailsbig/</c:if><c:out value="${image.imageUrl}"/>" id="" class="" title="" rel="quickbox"><img src="<c:if test="${!image.absolute}">${_contextpath}/assets/Image/Product/detailsbig/</c:if><c:out value="${image.imageUrl}"/>" border="0" style="<c:if test="${siteConfig['DETAILS_IMAGE_WIDTH'].value != '' }">width: <c:out value="${siteConfig['DETAILS_IMAGE_WIDTH'].value}"/>px</c:if>" alt="<c:out value="${product.name}"/><c:if test="${product.shortDesc != ''}"> - <c:out value="${product.shortDesc}"/></c:if>"/></a>
	   </c:if>
	 </c:forEach>
     <div style="clear: both;" >
       <c:forEach items="${model.product.images}" var="image" varStatus="status">
	     <c:if test="${status.count > 1}">
	       <a href="<c:if test="${!image.absolute}">${_contextpath}/assets/Image/Product/detailsbig/</c:if><c:out value="${image.imageUrl}"/>" id="qb${status.count - 1}" class="" title="" rel="quickbox"><img src="<c:if test="${!image.absolute}">${_contextpath}/assets/Image/Product/detailsbig/</c:if><c:out value="${image.imageUrl}"/>" border="0" class="details_thumbnail" alt="<c:out value="${product.name}"/><c:if test="${product.shortDesc != ''}"> - <c:out value="${product.shortDesc}"/></c:if>"/></a>
	     </c:if>
	   </c:forEach>
	 </div>
   </div>        
 </c:when>
 <c:otherwise>
   <link rel="stylesheet" href="${_contextpath}/assets/SmoothGallery/multibox.css" type="text/css" media="screen" />
   <c:set value="${true}" var="multibox"/>
   <script src="javascript/overlay.js" type="text/javascript" ></script>
   <script src="javascript/multibox.js" type="text/javascript" ></script>
   <script type="text/javascript">
	 window.addEvent('domready', function(){
	   var box = new multiBox('mbxwz', {overlay: new overlay()});
	 });
   </script>
   <div class="details_image_box" id="details_image_boxId" style="${details_images_style}
	 <c:if test="${siteConfig['DETAILS_IMAGE_WIDTH'].value != '' }">width: <c:out value="${siteConfig['DETAILS_IMAGE_WIDTH'].value}"/>px;</c:if>">
	 <c:forEach items="${model.product.images}" var="image" varStatus="status">
	   <c:if test="${status.first}">
	     <a href="<c:if test="${!image.absolute}">${_contextpath}/assets/Image/Product/detailsbig/</c:if><c:out value="${image.imageUrl}"/>" id="mb0" class="mbxwz" title=""><img src="<c:if test="${!image.absolute}">${_contextpath}/assets/Image/Product/detailsbig/</c:if><c:out value="${image.imageUrl}"/>" border="0" style="<c:if test="${siteConfig['DETAILS_IMAGE_WIDTH'].value != '' }">width: <c:out value="${siteConfig['DETAILS_IMAGE_WIDTH'].value}"/>px</c:if>" alt="<c:out value="${product.name}"/><c:if test="${product.shortDesc != ''}"> - <c:out value="${product.shortDesc}"/></c:if>"/></a>
	   </c:if>
	 </c:forEach>
     <div style="clear: both;" >
       <c:forEach items="${model.product.images}" var="image" varStatus="status">
	    <c:if test="${status.count >= 1}">
	       <a href="<c:if test="${!image.absolute}">${_contextpath}/assets/Image/Product/detailsbig/</c:if><c:out value="${image.imageUrl}"/>" id="mb${status.count}" class="mbxwz" title=""><img src="<c:if test="${!image.absolute}">${_contextpath}/assets/Image/Product/detailsbig/</c:if><c:out value="${image.imageUrl}"/>" border="0" class="details_thumbnail" alt="<c:out value="${product.name}"/><c:if test="${product.shortDesc != ''}"> - <c:out value="${product.shortDesc}"/></c:if>"/></a>
	     </c:if>
	   </c:forEach>
	 </div>
   </div>        
 </c:otherwise>
</c:choose>

<!-- sku start -->
  <div class="details_sku" id="details_sku_id"><c:out value="${model.product.sku}" /></div>
<!-- sku end -->

<!-- name start -->
<div class="details_item_name" id="details_item_name_id">
  <h1><c:out value="${model.product.name}" escapeXml="false" /></h1>
</div>
<!-- name end -->

<!-- short desc start -->
  <div class="details_short_desc" id="details_short_desc_id"><c:out value="${model.product.shortDesc}" escapeXml="false" /></div>
<!-- short desc end -->

<!-- long desc start -->
  <div class="details_long_desc" id="details_long_desc_id"><c:out value="${model.product.longDesc}" escapeXml="false" /></div>
<!-- long desc end -->

<!-- hidden product id start -->
    <input type="hidden" value="${model.product.id}" name="product.id" id="product.id" />
<!-- hidden product id end -->

<!-- hidden product id start -->
    <span id="quantityboxId">
      <input type="text" size="5" maxlength="5" class="quantityBox" name="quantity_${model.product.id}" id="quantity_${model.product.id}" value="${model.product.minimumQty}" >
	</span>
	
<!-- hidden product id end -->

<!-- inventory message start -->
    <span id="inventoryMessage">
    <c:if test="${gSiteConfig['gINVENTORY'] and model.product.inventory != null }">
      <c:if test="${!empty siteConfig['INVENTORY_ON_HAND'].value}" >
			 <div class="inventory_onhand"><c:out value="${siteConfig['INVENTORY_ON_HAND'].value}" escapeXml="false"/>&nbsp;<span><c:out value="${model.product.inventory}" /></span></div>
		  </c:if>
		  <c:if test="${model.product.inventory < model.product.lowInventory}">
	     <div class="inventory_low"><c:out value="${siteConfig['LOW_STOCK_TITLE'].value}" escapeXml="false"/></div>
	    </c:if>
	    <c:if test="${!model.product.negInventory and model.product.inventory <= 0}" >
	     <div class="inventoryOutofStock"><c:out value="${siteConfig['OUT_OF_STOCK_TITLE'].value}" escapeXml="false"/></div> 
	    </c:if>
	  </c:if>
    </span>
<!-- inventory message end -->

<!-- product fields start -->
<c:if test="${fn:length(model.product.productFields) > 0}">
<div class="productFields" id="productFieldsId">
  <table class="details_fields">
    <c:forEach items="${model.product.productFields}" var="productField" varStatus="row">
    <tr>
      <td class="details_field_name_row${row.index % 2}"><c:out value="${productField.name}" escapeXml="false" /> </td>
      <c:choose>
		<c:when test="${productField.formatType != null and productField.formatType != '' and productField.fieldType == 'number' }">
			<td class="details_field_value_row${row.index % 2}"><fmt:formatNumber value="${wj:trimAndSplit(productField.value, '|' , false, ',', true, true)}" pattern="${productField.formatType }" /></td>
		</c:when>
		<c:otherwise>
			<td class="details_field_value_row${row.index % 2}"><c:out value="${wj:trimAndSplit(productField.value, '|' , false, ',', true, true)}" escapeXml="false"/></td>
		</c:otherwise>
	</c:choose>
    </tr>
  </c:forEach>
  </table>
</div>
</c:if>
<!-- product fields end -->
		 