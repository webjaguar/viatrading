<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<c:choose>
  <c:when test="${model.success}">
    <div style="color : green; text-align : LEFT;">Thank You for your interest. <br/> You will be contacted soon.</div>
  </c:when>
  <c:otherwise>
    <div style="color : RED; text-align : LEFT;">Please enter First Name, Last Name and either phone or email. <br/> Please try again.</div>
  </c:otherwise>
</c:choose>

