<%@ page import="com.webjaguar.model.*, java.text.NumberFormat, java.util.*" %>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>

<div class="miniCartHeader">
 <div class="viewCart"><a href="${_contextpath}/viewCart.jhtm">View Cart</a></div>
 <c:choose>
  <c:when test="${gSiteConfig['gSHOPPING_CART_1']}">
  <div class="checkout"><a href="${_contextpath}/checkout1.jhtm" class="shoppingcart_checkout_link">
    <img src="${_contextpath}/assets/Image/Layout/button_miniCart_checkout${_lang}.gif" border="0"/>
  </a></div>
  </c:when>
  <c:otherwise>
  <div class="checkout"><a href="${_contextpath}/checkout.jhtm" class="shoppingcart_checkout_link">
    <img src="${_contextpath}/assets/Image/Layout/button_miniCart_checkout${_lang}.gif" border="0"/>
  </a></div>
  </c:otherwise>
</c:choose>
</div>
<div class="miniCartSubTotalWrapperTop">
 <div class="miniCartNumItems"><c:out value="${model.cart.quantity}"/> items in your cart</div>
 <div class="miniCartSubTotal"><span>SubTotal:</span> <fmt:message key="${siteConfig['CURRENCY'].value}" /><fmt:formatNumber value="${model.cart.subTotal}" pattern="#,##0.00"/></div>
</div>
<div style="clear:both;" ></div>
<div class="miniCartWrapper">
<c:forEach items="${model.cart.cartItems}" var="cartItem" varStatus="cartStatus">
<c:set var="productLink">${_contextpath}/product.jhtm?id=${cartItem.product.id}</c:set>
<c:if test="${gSiteConfig['gMOD_REWRITE'] == '1' and not fn:contains(cartItem.product.sku, '/')}">
  <c:set var="productLink">${_contextpath}/${siteConfig['MOD_REWRITE_PRODUCT'].value}/${cartItem.product.encodedSku}/${cartItem.product.encodedName}.html</c:set>
</c:if>	

<div class="miniCartProductWrapper">
<div class="miniCartImage">
<a  href="${productLink}" class="shoppingcart_item_image"><img class="cartImage" src="<c:if test="${!cartItem.product.thumbnail.absolute}">${_contextpath}/assets/Image/Product/thumb/</c:if>${cartItem.product.thumbnail.imageUrl}" border="0" /></a>
</div>
<div class="miniCartSpec">
<ul>
  <li class="miniCartName name"><fmt:message key="product" />: <c:out value="${cartItem.product.name}"/></li>
  <li class="miniCartName sku"><fmt:message key="sku" />: <c:out value="${cartItem.product.sku}"/></li>
  <li class="miniCartName qty"><fmt:message key="qty" />: <c:out value="${cartItem.quantity}"/></li>
  <li class="miniCartName price1"><fmt:message key="price" />: 
  <c:set var="multiplier" value="1"/>
  <c:if test="${siteConfig['CASE_CONTENT_LAYOUT'].value == '1' and cartItem.product.caseContent > 0}">
     <c:set var="multiplier" value="${cartItem.product.caseContent}"/>	
  </c:if>
  <fmt:message key="${siteConfig['CURRENCY'].value}" /><fmt:formatNumber value="${cartItem.unitPrice*multiplier}" pattern="#,##0.00"/></li>
</ul>
</div>
</div>
<div style="clear:both;" ></div>
</c:forEach>
</div>
<div style="clear:both;" ></div>
<div class="miniCartSubTotalWrapperBottom">
 <div class="miniCartNumItems"><c:out value="${model.cart.quantity}"/> items in your cart</div>
 <div class="miniCartSubTotal"><span>SubTotal:</span> <fmt:message key="${siteConfig['CURRENCY'].value}" /><fmt:formatNumber value="${model.cart.subTotal}" pattern="#,##0.00"/></div>
</div>
<div style="clear:both;" ></div>
<div class="miniCartFooter">
 <div class="viewCart"><a href="${_contextpath}/viewCart.jhtm">View Cart</a></div>
 <c:choose>
  <c:when test="${gSiteConfig['gSHOPPING_CART_1']}">
  <div class="checkout"><a href="${_contextpath}/checkout1.jhtm" class="shoppingcart_checkout_link">
    <img src="${_contextpath}/assets/Image/Layout/button_miniCart_checkout${_lang}.gif" border="0"/>
  </a></div>
  </c:when>
  <c:otherwise>
  <div class="checkout"><a href="${_contextpath}/checkout.jhtm" class="shoppingcart_checkout_link">
    <img src="${_contextpath}/assets/Image/Layout/button_miniCart_checkout${_lang}.gif" border="0"/>
  </a></div>
  </c:otherwise>
</c:choose>
</div>