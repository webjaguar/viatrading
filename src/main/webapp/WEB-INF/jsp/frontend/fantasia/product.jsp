<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>

<tiles:insertDefinition name="${_template}" flush="true">  
<tiles:putAttribute name="leftbar" value="/WEB-INF/jsp/frontend/layout/template${_leftBar}/leftbar.jsp" />

		<tiles:putAttribute name="content" type="string">

				<c:if test="${fn:trim(model.productLayout.headerHtml) != ''}">
						<c:set value="${model.productLayout.headerHtml}" var="headerHtml" />
						<%@ include file="/WEB-INF/jsp/frontend/common/headerDynamicElement.jsp"%>
						<div>
								<c:out value="${headerHtml}" escapeXml="false" />
						</div>
				</c:if>

				<c:if test="${model.message != null}">
						<div class="message">
								<fmt:message key="${model.message}" />
						</div>
				</c:if>

				<c:if test="${model.product != null}">

<c:set value="${model.product}" var="product" />
<script language="JavaScript" type="text/JavaScript">
function checkQty( pid, boxExtraAmt )
{
	var product = document.getElementById('quantity_' + pid);
	if (checkNumber(product.value) == -1 || product.value == 0) {
		product.value = 1;
	}
	return true;
}
function checkForm()
{
	var productList = document.getElementsByName('product.id');
	var allQtyEmpty = true;
	for(var i=0; i<productList.length; i++) 
	{
	    var el = document.getElementById("quantity_" + productList[i].value);
		if ( el != null )
		{
			if ( checkNumber( el.value ) == 1  )
			{
				allQtyEmpty = false;
			}
			else if ( checkNumber( el.value ) == -1 ) 
			{
				alert("invalid Quantity");
				el.focus();
				return false;
			}			
		}	
	}
	if ( allQtyEmpty )
	{
		alert("Please Enter Quantity.");
		return false;
	}
	else
		return true;	
}
<c:if test="${siteConfig['PRODUCT_QUOTE'].value == 'true' and product.quote}" >
function emailCheck(str){
	 if(/^[a-z0-9._%-]+@[a-z0-9.-]+\.[a-z]{2,4}$/i.test(str) == false) {
		alert('Invalid Email');
		return false;
	 }
	 return true;
}
function assignValue(ele, value) {
$(ele).value = value;
}
function phoneCheck(str){
	if (str.search(/^\d{10}$/)==-1) {
		alert("Please enter a valid 10 digit number");
		return false;
	} 
}
function saveContact(){
	try {
		if( !validateContactInfo() ) {
			return;
		}
	}catch(err){}
	
	var sku = $('sku').value;
	var name = $('name').value;
	var firstName = $('firstName').value;
	var lastName = $('lastName').value;
	var company = $('company').value;
	var address = $('address').value;
	var city = $('city').value;
	var state = $('state').value;
	var zipCode = $('zipCode').value;
	var country = $('country').value;
	var email = $('email').value;
	var phone = $('phone').value;
	var note = $('note').value;
	var request = new Request({
	   url: "${_contextpath}/insert-ajax-contact.jhtm?firstName="+firstName+"&lastName="+lastName+"&company="+company+"&address="+address+"&city="+city+"&state="+state+"&zipCode="+zipCode+"&country="+country+"&email="+email+"&phone="+phone+"&note="+note+"&sku="+sku+"&name="+name+"&leadSource=quote",
       method: 'post',
       onFailure: function(response){
           saveContactBox.close();
           $('successMessage').set('html', '<div style="color : RED; text-align : LEFT;">We are experincing problem in saving your quote. <br/> Please try again later.</div>');
       },
       onSuccess: function(response) {
            saveContactBox.close();
            $('mbQuote').style.display = 'none';
            $('successMessage').set('html', response);
       }
	}).send();
}
</c:if>
//-->
</script>

						<c:choose>
								<c:when test="${siteConfig['DETAILS_IMAGE_LOCATION'].value == 'left' }">
										<c:set var="details_images_style" value="float:left;padding:3px 15px 15px 3px;" />
										<c:set var="details_desc_style" value="float:left;padding:0 0 0 15px;" />
								</c:when>
								<c:otherwise>
										<c:set var="details_images_style" value="padding:0 0 15px 0;" />
										<c:set var="details_desc_style" value="" />
								</c:otherwise>
						</c:choose>

						<script type="text/javascript">
var box2 = {};
window.addEvent('domready',function() {
	  box2 = new multiBox('mbCategory', {waitDuration: 5,showControls: false,overlay: new overlay()});
});
function zoomIn(productId) {
	window.open('${_contextpath}/productImage.jhtm?id='+productId+'&layout=2&imgUrl=' + document.getElementById("_image").src,'name','width=800,height=600,resizable=yes,scrollbars=1');
}
</script>

						<div style="${details_desc_style}" class="detaileFan">
								<table border="0" cellpadding="2" cellspacing="0" width="100%">
										<tbody>
										    <tr>
										        <td>
										        <c:import url="/WEB-INF/jsp/frontend/layout/${_template}/breadcrumbs.jsp" />
										        </td>
										    </tr>
												<tr>
														<%--Start of Image --%>
														<td>
																<div>
																		<table border="0" cellpadding="2" cellspacing="0" width="100%">
																				<tbody>
																						<tr>

																								<td valign="top">
																								<c:set var="productImage">
																												<c:choose>
																														<c:when
																																test="${fn:contains(siteConfig['PRODUCT_IMAGE_LAYOUTS'].value,'mb') and 
					 		(param.multibox == null or param.multibox != 'off') and
					 		(product.imageLayout == 'mb' or (empty product.imageLayout and siteConfig['PRODUCT_IMAGE_LAYOUT'].value == 'mb'))}">
																																<link rel="stylesheet" href="${_contextpath}/assets/SmoothGallery/multibox.css" type="text/css" media="screen" />
																																<c:set value="${true}" var="multibox" />
																																<script src="${_contextpath}/javascript/overlay.js" type="text/javascript"></script>
																																<script src="${_contextpath}/javascript/multibox.js" type="text/javascript"></script>
																																<script type="text/javascript">
						window.addEvent('domready', function(){
							var box = new multiBox('mbxwz', {overlay: new overlay()});
						});
					 </script>
																																<div class="imageWrapperClass">
																																		<div class="details_image_box" id="details_image_boxId">
																																				<c:forEach items="${model.product.images}" var="image" varStatus="status">
																																						<c:if test="${status.first}">
																																								<c:choose>
																																										<c:when test="${product.asiId != null}">
																																												<a href="<c:out value="${fn:replace(product.thumbnail.imageUrl, 'prodimgs', 'prodbigimgs')}"/>" id="mb0" class="mbxwz"
																																														title="<c:out value="${product.name}"/><c:if test="${product.shortDesc != ''}"> - <c:out value="${product.shortDesc}"/></c:if>"><img
																																														src="<c:out value="${fn:replace(product.thumbnail.imageUrl, 'prodimgs', 'prodbigimgs')}"/>" border="0"
																																														style="<c:if test="${siteConfig['DETAILS_IMAGE_WIDTH'].value != '' }">width: <c:out value="${siteConfig['DETAILS_IMAGE_WIDTH'].value}"/>px</c:if>"
																																														alt="<c:out value="${product.name}"/><c:if test="${product.shortDesc != ''}"> - <c:out value="${product.shortDesc}"/></c:if>" /></a>
																																										</c:when>
																																										<c:otherwise>
																																												<a href="<c:if test="${!image.absolute}">${_contextpath}/assets/Image/Product/detailsbig/</c:if><c:out value="${image.imageUrl}"/>"
																																														id="mb0" class="mbxwz" title="<c:out value="${product.name}"/>"><img
																																														src="<c:if test="${!image.absolute}">${_contextpath}/assets/Image/Product/detailsbig/</c:if><c:out value="${image.imageUrl}"/>"
																																														border="0"
																																														style="<c:if test="${siteConfig['DETAILS_IMAGE_WIDTH'].value != '' }">width: <c:out value="${siteConfig['DETAILS_IMAGE_WIDTH'].value}"/>px</c:if>"
																																														alt="<c:out value="${product.name}"/><c:if test="${product.shortDesc != ''}"> - <c:out value="${product.shortDesc}"/></c:if>" /></a>
																																										</c:otherwise>
																																								</c:choose>
																																						</c:if>
																																				</c:forEach>
																																		</div>
																																		<div class="thumbImageClass">
																																				<c:forEach items="${model.product.images}" var="image" varStatus="status">
																																						<c:if test="${status.count > 1}">
																																								<c:choose>
																																										<c:when test="${product.asiId != null}">
																																												<a href="<c:out value="${fn:replace(product.thumbnail.imageUrl, 'prodimgs', 'prodbigimgs')}"/>" id="mb${status.count - 1}" class="mbxwz"
																																														title="<c:out value="${product.name}"/><c:if test="${product.shortDesc != ''}"> - <c:out value="${product.shortDesc}"/></c:if>"><img
																																														src="<c:out value="${fn:replace(product.thumbnail.imageUrl, 'prodimgs', 'prodbigimgs')}"/>" border="0"
																																														style="<c:if test="${siteConfig['DETAILS_IMAGE_WIDTH'].value != '' }">width: <c:out value="${siteConfig['DETAILS_IMAGE_WIDTH'].value}"/>px</c:if>"
																																														alt="<c:out value="${product.name}"/><c:if test="${product.shortDesc != ''}"> - <c:out value="${product.shortDesc}"/></c:if>" /></a>
																																										</c:when>
																																										<c:otherwise>
																																												<a href="<c:if test="${!image.absolute}">${_contextpath}/assets/Image/Product/detailsbig/</c:if><c:out value="${image.imageUrl}"/>"
																																														id="mb${status.count - 1}" class="mbxwz"
																																														title="<c:out value="${product.name}"/><c:if test="${product.shortDesc != ''}"> - <c:out value="${product.shortDesc}"/></c:if>"><img
																																														src="<c:if test="${!image.absolute}">${_contextpath}/assets/Image/Product/detailsbig/</c:if><c:out value="${image.imageUrl}"/>"
																																														border="0" class="details_thumbnail"
																																														alt="<c:out value="${product.name}"/><c:if test="${product.shortDesc != ''}"> - <c:out value="${product.shortDesc}"/></c:if>" /></a>
																																										</c:otherwise>
																																								</c:choose>
																																						</c:if>
																																				</c:forEach>
																																		</div>
																																</div>
																														</c:when>
																														<c:otherwise>
																																<div class="imageWrapperClass">
																																		<div class="details_image_box"
																																				style="${details_images_style}
								<c:if test="${siteConfig['DETAILS_IMAGE_WIDTH'].value != '' }">width: <c:out value="${siteConfig['DETAILS_IMAGE_WIDTH'].value}"/>px;</c:if>">
																																				<c:forEach items="${model.product.images}" var="image" varStatus="status">
																																						<c:if test="${status.first}">
																																								<img src="<c:if test="${!image.absolute}">${_contextpath}/assets/Image/Product/detailsbig/</c:if><c:out value="${image.imageUrl}"/>"
																																										border="0" id="_image" name="_image" class="details_image"
																																										style="<c:if test="${siteConfig['DETAILS_IMAGE_WIDTH'].value != '' }">width: <c:out value="${siteConfig['DETAILS_IMAGE_WIDTH'].value}"/>px</c:if>"
																																										alt="<c:out value="${product.name}"/><c:if test="${product.shortDesc != ''}"> - <c:out value="${product.shortDesc}"/></c:if>">
																																								<c:if test="${siteConfig['DETAILS_IMAGE_WIDTH'].value != '' }">
																																										<div align="right">
																																												<a onclick="zoomIn()" title="Zoom in"><img src="${_contextpath}/assets/Image/Layout/zoom-icon.gif" alt="Zoom In" width="16"
																																														height="16" /></a>
																																										</div>
																																								</c:if>
																																						</c:if>
																																						<td><c:if test="${status.last and (status.count != 1)}">
																																										<c:forEach items="${model.product.images}" var="thumb">
																																												<div>
																																														<a href="#" class="details_thumbnail_anchor"
																																																onClick="_image.src='<c:if test="${!thumb.absolute}">${_contextpath}/assets/Image/Product/detailsbig/</c:if><c:out value="${thumb.imageUrl}"/>';return false;">
																																																<img src="<c:if test="${!thumb.absolute}">${_contextpath}/assets/Image/Product/detailsbig/</c:if><c:out value="${thumb.imageUrl}"/>"
																																																class="details_thumbnail"
																																																alt="<c:out value="${product.name}"/><c:if test="${product.shortDesc != ''}"> - <c:out value="${product.shortDesc}"/></c:if>">
																																														</a>
																																												</div>
																																										</c:forEach>
																																								</c:if></td>
																																				</c:forEach>
																																		</div>
																																		<c:if
																																				test="${fn:contains(siteConfig['PRODUCT_IMAGE_LAYOUTS'].value,'ss') and 
								(product.imageLayout == 'ss'  or (empty product.imageLayout and siteConfig['PRODUCT_IMAGE_LAYOUT'].value == 'ss'))}">
																																				<object classid="clsid:D27CDB6E-AE6D-11cf-96B8-444553540000"
																																						codebase="http://download.macromedia.com/pub/shockwave/cabs/flash/swflash.cab#version=6,0,29,0" width="750" height="475">
																																						<param name="movie" value="${_contextpath}/assets/Flash/slideshow.swf?file=thumbnailer1.jhtm?id=${product.id}">
																																						<param name="quality" value="high">
																																						<embed src="${_contextpath}/assets/Flash/slideshow.swf?file=thumbnailer1.jhtm?id=${product.id}" quality="high"
																																								pluginspage="http://www.macromedia.com/go/getflashplayer" type="application/x-shockwave-flash" width="750" height="475"></embed>
																																				</object>
																																		</c:if>
																																</div>
																														</c:otherwise>
																												</c:choose>
																										</c:set> <c:out value="${productImage}" escapeXml="false" /></td>
																						</tr>
																				</tbody>
																		</table>
																</div>
														</td>
														<%--End of Image --%>
														<td valign="top">
																<div>
																		<%--Start of Name,Short Description and Long Description --%>
																		<table border="0" cellpadding="2" cellspacing="2" width="90%">
																				<tbody>
																						<tr>
																								<td>
																										<div class="details_item_name">
																												<h1>
																														<c:out value="${product.name}" escapeXml="false" />
																												</h1>
																										</div>
																								</td>
																								<td>
																										<div class="details_short_desc">
																												<c:out value="${product.shortDesc}" escapeXml="false" />
																										</div>
																								</td>
																						</tr>
																				</tbody>
																		</table>
																</div>
																<div class="details_long_desc">
																		<c:out value="${product.longDesc}" escapeXml="false" />
																</div> <%--End of Name,Short Description and Long Description --%>
																<div class="horizontalRuleClass"></div>
																<%-- Start Price and Add To Cart --%>
																<c:if test="${ (gSiteConfig['gSHOPPING_CART'] and (!empty product.price or product.priceByCustomer)) and (!product.loginRequire or userSession != null)}">
																<form action="${_contextpath}/addToCart.jhtm" name="this_form" method="post" onsubmit="return checkForm()">
																  <input type="hidden" name="product.id" value="${product.id}">
																	<div id="priceWrapper">
																		<table border="0" cellpadding="2" cellspacing="2" width="90%">
																				<tbody>
																						<tr>
																								<td>
																										<div class="cartonClass">
																												<c:out value="${product.field15}" />
																										</div>
																								</td>
																						</tr>
																						<tr>
																						  <td>
																						  <c:choose>
																							<c:when test="${product.salesTag != null and !product.salesTag.percent}">
																							  <%@ include file="/WEB-INF/jsp/frontend/common/price3.jsp" %>
																							</c:when>
																							<c:otherwise>
																							  <%@ include file="/WEB-INF/jsp/frontend/common/price.jsp" %>
																							</c:otherwise>
																							</c:choose>
																						  </td>
																						</tr>
																						<tr>
																								<td>
																										<table>
																												<tr>
																														<c:choose>
																																<c:when test="${gSiteConfig['gVARIABLE_PRICE'] and product.priceByCustomer}">
																																		<td colspan="2"><input type="hidden" name="quantity_${product.id}" id="quantity_${product.id}" value="1"> <input type="image"
																																				border="0" name="_addtocart" class="_addtocart" src="${_contextpath}/assets/Image/Layout/button_addtocart${_lang}.gif" /></td>
																																</c:when>
																																<c:otherwise>
																																		<c:set var="zeroPrice" value="false" />
																																		<c:forEach items="${product.price}" var="price">
																																				<c:if test="${price.amt == 0}">
																																						<c:set var="zeroPrice" value="true" />
																																				</c:if>
																																		</c:forEach>
																																		<c:choose>
																																				<c:when test="${gSiteConfig['gINVENTORY'] and product.inventory != null and !product.negInventory and product.inventory <= 0}">
																																						<td class="inventoryOutofStock"><c:out value="${siteConfig['OUT_OF_STOCK_TITLE'].value}" escapeXml="false" /></td>
																																				</c:when>
																																				<c:when test="${zeroPrice and siteConfig['ZERO_PRICE_HTML'].value != ''}">
																																						<td><c:out value="${siteConfig['ZERO_PRICE_HTML'].value}" escapeXml="false" /></td>
																																				</c:when>
																																				<c:otherwise>
																																						<td class="quantityBlock" style="white-space: nowrap">
																																								<div class="quantity">
																																										<fmt:message key="quantity" />
																																										:&nbsp;
																																										<c:choose>
																																												<c:when test="${siteConfig['PRODUCT_QTY_DROPDOWN'].value != ''}">
																																														<%@ include file="/WEB-INF/jsp/frontend/common/qtyDropDown.jsp"%>
																																												</c:when>
																																												<c:otherwise>
																																														<input type="text" size="6" maxlength="5" name="quantity_${product.id}" id="quantity_${product.id}"
																																																value="<c:out value='${product.minimumQty}' />">
																																												</c:otherwise>
																																										</c:choose>
																																										<c:if test="${product.packing != ''}">
																																												<c:out value="${product.packing}" />
																																										</c:if>
																																								</div>
																																						</td>
																																						<c:choose>
																																								<c:when test="${!empty product.numCustomLines}">
																																										<td><input type="image" id="continueButton" border="0" src="${_contextpath}/assets/Image/Layout/button_continue.gif"
																																												onClick="return continueButton(${product.id})"></td>
																																								</c:when>
																																								<c:otherwise>
																																										<td><input type="image" border="0" name="_addtocart" class="_addtocart"
																																												src="${_contextpath}/assets/Image/Layout/button_addtocart${_lang}.gif" onClick="return checkQty(${product.id})" /></td>
																																								</c:otherwise>
																																						</c:choose>
																																						<c:if test="${gSiteConfig['gINVENTORY'] and product.inventory != null and product.inventory < product.lowInventory}">
																																								<td class="inventoryLow"><c:out value="${siteConfig['LOW_STOCK_TITLE'].value}" escapeXml="false" /></td>
																																						</c:if>
																																				</c:otherwise>
																																		</c:choose>
																																</c:otherwise>
																														</c:choose>
																												</tr>
																												<c:if test="${gSiteConfig['gINVENTORY'] and product.inventory != null and !empty siteConfig['INVENTORY_ON_HAND'].value}">
																														<tr class="inventory_onhand_wrapper">
																																<td><div class="inventory_onhand">
																																				<c:out value="${siteConfig['INVENTORY_ON_HAND'].value}" escapeXml="false" />
																																				&nbsp;<span><c:out value="${product.inventory}" /></span>
																																		</div></td>
																														</tr>
																												</c:if>
																										</table>

																										<div class="productFieldClass">
																												<table>
																														<tbody>
																																<tr>
																																		<th>Dimensions:</th>
																																		<td>L: <c:out value="${product.field16}" /></td>
																																		<td>W: <c:out value="${product.field17}" /></td>
																																		<td>D: <c:out value="${product.field18}" /></td>
																																</tr>
																														</tbody>
																												</table>
																										</div>
																										<div class="weightClass">
																												Weight:
																												<c:out value="${product.weight}" />
																										</div>
																								</td>
																								<%-- End Price and Add To Cart --%>

																								<%--Start Image Field --%>
																								<td class="filedImageClass">
																										<div>Distributors</div> <c:out value="${product.field14}" escapeXml="false"/>
																								</td>
																								<%--End Image Field --%>

																						</tr>
																				</tbody>
																		</table>

																</div>
																</form>
																</c:if>
														</td>

														<%--Start of Name and Short Description --%>
												</tr>
										</tbody>
								</table>
						</div>

				</c:if>

		</tiles:putAttribute>
</tiles:insertDefinition>