<%@ page import="java.util.*" %>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="/WEB-INF/tlds/spring.tld" prefix="spring" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ taglib uri="/WEB-INF/tlds/custom.tld" prefix="wj" %>

<tiles:insertDefinition name="${_template}" flush="true">  
<tiles:putAttribute name="leftbar" value="/WEB-INF/jsp/frontend/layout/template${_leftBar}/leftbar.jsp" />
<c:if test="${siteConfig['LEFTBAR_DEFAULT_TYPE'].value == '3' }">
  <c:set value="false" var="hideMootools" />
</c:if>
  <tiles:putAttribute name="content" type="string">
<c:set value="${false}" var="multibox"/>

<link rel="stylesheet" href="${_contextpath}/assets/SmoothGallery/multiboxASI.css" type="text/css" media="screen" />
<link rel="stylesheet" href="${_contextpath}/assets/asi_product.css" type="text/css" media="screen" />

<c:if test="${model.mootools and param.hideMootools == null and hideMootools == null}">
<script src="/javascript/mootools-1.2.5.1-more.js" type="text/javascript"></script>
</c:if>

<c:if test="${fn:trim(model.productLayout.headerHtml) != ''}">
  <c:set value="${model.productLayout.headerHtml}" var="headerHtml"/>
  <%@ include file="/WEB-INF/jsp/frontend/common/headerDynamicElement.jsp" %>
  <div><c:out value="${headerHtml}" escapeXml="false"/></div>
</c:if>

<c:if test="${model.message != null}">
  <div class="message"><fmt:message key="${model.message}" /></div>
</c:if>

<c:if test="${model.product != null}">
<c:set value="${model.product}" var="product"/>
<script language="JavaScript" type="text/JavaScript">
<!--
var saveContactBox;
window.addEvent('domready', function(){			 
	saveContactBox = new multiBox('mbAsiPrice', {showControls : false, useOverlay: false, showNumbers: false });
});
<c:if test="${siteConfig['DETAILS_IMAGE_WIDTH'].value != '' }" >
function zoomIn() {
	window.open('productImage.jhtm?id=${product.id}&imgUrl=' + document.getElementById("_image").src,'name','width=800,height=600,resizable=yes,scrollbars=1');
}
</c:if>
function checkNumber( aNumber )
{
	var goodChars = "0123456789";
	var i = 0;
	if ( aNumber == "" )
		return 0; //empty
	
	for ( i = 0 ; i <= aNumber.length - 1 ; i++ )
	{
		if ( goodChars.indexOf( aNumber.charAt(i) ) == -1 ) 
			return -1; //invalid number			
	}	
	return 1; //valid number
}
function checkQty( pid, boxExtraAmt ){
	var product = document.getElementById('quantity_' + pid);
	if ( checkNumber( product.value ) == -1 || product.value == 0 ) {
	    alert("invalid Quantity");
		product.focus();
		return false;
	}
	return true;
}
function checkForm()
{
	var productList = document.getElementsByName('product.id');
	var allQtyEmpty = true;
	for(var i=0; i<productList.length; i++) 
	{
	    var el = document.getElementById("quantity_" + productList[i].value);
		if ( el != null )
		{
			if ( checkNumber( el.value ) == 1  )
			{
				allQtyEmpty = false;
			}
			else if ( checkNumber( el.value ) == -1 ) 
			{
				alert("invalid Quantity");
				el.focus();
				return false;
			}			
		}	
	}
	if ( allQtyEmpty )
	{
		alert("Please Enter Quantity.");
		return false;
	}
	else
		return true;	
}
function emailCheck(str){
	 if(/^[a-z0-9._%-]+@[a-z0-9.-]+\.[a-z]{2,4}$/i.test(str) == false) {
		alert('Invalid Email');
		return false;
	 }
	 return true;
}
function phoneCheck(str){
	if (str.search(/^\d{10}$/)==-1) {
		alert("Please enter a valid 10 digit number");
		return false;
	} 
}
function assignValue(ele, value) {
	$(ele).value = value;
}
function saveContactInfo(index) {
	if($('firstName_'+index).value == '' || $('lastName_'+index).value == '') {
		alert('Provide your first and last name.');
		return false;
	}
	if($('phone_'+index).value == '' && $('email_'+index).value == '') {
		alert('Provide at least one contact ( Email or Phone)');
		return false;
	}
	if ($('email_'+index).value != '' && emailCheck($('email_'+index).value)==false){
		$('email_'+index).value="";
		$('email_'+index).focus();
		return false;
	}
	if ($('phone_'+index).value != '' && phoneCheck($('phone_'+index).value)==false){
		$('phone_'+index).value="";
		$('phone_'+index).focus();
		return false;
	}
	var priceGridId = $('priceGridId_'+index).value;
	var priceGridDependecyValue = $('priceGridDependecyValue_'+index).value;
	var sku = $('sku_'+index).value;
	var name = $('name_'+index).value;
	var firstName = $('firstName_'+index).value;
	var lastName = $('lastName_'+index).value;
	var email = $('email_'+index).value;
	var phone = $('phone_'+index).value;
	var note = $('note_'+index).value;
	var request = new Request({
		url: "${_contextpath}/insert-ajax-contact.jhtm?priceGridId="+priceGridId+"&priceGridDependecyValue="+priceGridDependecyValue+"&firstName="+firstName+"&lastName="+lastName+"&email="+email+"&phone="+phone+"&note="+note+"&sku="+sku+"&name="+name+"&leadSource=quote",
		method: 'post',
		onFailure: function(response){
        	saveContactBox.close();
        	$('mbAsiPrice'+index).style.display = 'none';
			$('successMessage').set('html', '<div style="color : RED; text-align : LEFT;">We are experincing problem in saving your quote. <br/> Please try again later.</div>');
    	},
    	onSuccess: function(response) {
            saveContactBox.close();
            $('mbAsiPrice'+index).style.display = 'none';
			$('successMessage_'+index).set('html', '<div style="color : RED; text-align : LEFT;">Thank You for your interest. <br/> You will be contacted soon.</div>');
    	}
	}).send();
}
//-->
</script>


<c:choose>
  <c:when test="${siteConfig['DETAILS_IMAGE_LOCATION'].value == 'left' }">
    <c:set var="details_images_style" value="float:left;padding:3px 15px 15px 3px;" />
    <c:set var="details_desc_style" value="float:left;padding:0 0 0 15px;" />
  </c:when>
</c:choose>

<%-- bread crumbs --%>
<c:import url="/WEB-INF/jsp/frontend/layout/${_template}/breadcrumbs.jsp" />

<table class="container-details" border="0" cellpadding="0" cellspacing="0">
<tr class="top">
<td class="topLeft"><img src="${_contextpath}/assets/Image/Layout/topleft.gif" border="0" alt="" /></td>
<td class="topTop"></td>
<td class="topRight"><img src="${_contextpath}/assets/Image/Layout/topright.gif" border="0" alt="" /></td>
</tr>
<tr class="middle"><td class="middleLeft"></td>
<td class="middleMiddle">

<div class="details8">
<table border="0" cellpadding="0" cellspacing="0">
<tr valign="top">
<td>
<!-- image end -->
<c:set var="productImage">
<c:choose>
 <c:when test="${fn:contains(siteConfig['PRODUCT_IMAGE_LAYOUTS'].value,'s2') and 
 		(product.imageLayout == 's2' or (empty product.imageLayout and siteConfig['PRODUCT_IMAGE_LAYOUT'].value == 's2'))}">
    <link rel="stylesheet" href="assets/SmoothGallery/slideshow.css" type="text/css" media="screen" />
	<script type="text/javascript" src="javascript/slideshow.js"></script>
	<script type="text/javascript">		
	//<![CDATA[
	  window.addEvent('domready', function(){
	    var data = [<c:forEach items="${product.images}" var="image" varStatus="status">'<c:if test="${!image.absolute}">assets/Image/Product/detailsbig/</c:if><c:out value="${image.imageUrl}"/>'<c:if test="${! status.last}" >,</c:if></c:forEach> ];
	    var myShow = new Slideshow('show', data, { controller: true, height: 302, hu: '', thumbnails: true ,width:300, resize:'length'});
		});
	//]]>
	</script>
	
	<div id="show" class="slideshow"> </div>
    <div id="slideshow_margin" ></div>
    <div style="clear:both;" ></div>
 </c:when>
 <c:when test="${fn:contains(siteConfig['PRODUCT_IMAGE_LAYOUTS'].value,'mb') and 
 		(param.multibox == null or param.multibox != 'off') and
 		(product.imageLayout == 'mb' or (empty product.imageLayout and siteConfig['PRODUCT_IMAGE_LAYOUT'].value == 'mb'))}">
 <link rel="stylesheet" href="${_contextpath}/assets/SmoothGallery/multibox.css" type="text/css" media="screen" />
 <c:set value="${true}" var="multibox"/>
 <script src="${_contextpath}/javascript/overlay.js" type="text/javascript" ></script>
 <script src="${_contextpath}/javascript/multibox.js" type="text/javascript" ></script>
 <script type="text/javascript">
	window.addEvent('domready', function(){
		var box = new multiBox('mbxwz', {overlay: new overlay()});
	});
 </script>
 	<div class="details_image_box" id="details_image_boxId" style="${details_images_style}
	 <c:if test="${siteConfig['DETAILS_IMAGE_WIDTH'].value != '' }">width: <c:out value="${siteConfig['DETAILS_IMAGE_WIDTH'].value}"/>px;</c:if>">
	 
	<c:forEach items="${model.product.images}" var="image" varStatus="status">
	  <c:if test="${status.first}">
	    <c:choose>
	  	   <c:when test="${product.asiId != null}">
	  	   <a href="<c:out value="${fn:replace(product.thumbnail.imageUrl, 'prodimgs', 'prodbigimgs')}"/>" id="mb0" class="mbxwz" title="<c:out value="${product.name}"/><c:if test="${product.shortDesc != ''}"> - <c:out value="${product.shortDesc}"/></c:if>"><img src="<c:out value="${fn:replace(product.thumbnail.imageUrl, 'prodimgs', 'prodbigimgs')}"/>" border="0" style="<c:if test="${siteConfig['DETAILS_IMAGE_WIDTH'].value != '' }">width: <c:out value="${siteConfig['DETAILS_IMAGE_WIDTH'].value}"/>px</c:if>" alt="<c:out value="${product.name}"/><c:if test="${product.shortDesc != ''}"> - <c:out value="${product.shortDesc}"/></c:if>"/></a>
	  	   </c:when>
	  	   <c:otherwise>
	  	   <a href="<c:if test="${!image.absolute}">assets/Image/Product/detailsbig/</c:if><c:out value="${image.imageUrl}"/>" id="mb0" class="mbxwz" title=""><img src="<c:if test="${!image.absolute}">assets/Image/Product/detailsbig/</c:if><c:out value="${image.imageUrl}"/>" border="0" style="<c:if test="${siteConfig['DETAILS_IMAGE_WIDTH'].value != '' }">width: <c:out value="${siteConfig['DETAILS_IMAGE_WIDTH'].value}"/>px</c:if>" alt="<c:out value="${product.name}"/><c:if test="${product.shortDesc != ''}"> - <c:out value="${product.shortDesc}"/></c:if>"/></a>
	  	   </c:otherwise>
	  	  </c:choose>
	  </c:if>
	</c:forEach>
    <div style="clear: both;" >
    <div class="thumbnailWrapper">
    <c:forEach items="${model.product.images}" var="thumb" varStatus="status">
	  <c:if test="${status.count > 1}">
	      <c:choose>
	  	   <c:when test="${product.asiId != null}">
	  	   <a href="<c:out value="${fn:replace(thumb.imageUrl, 'prodimgs', 'prodbigimgs')}"/>" id="mb${status.count - 1}" class="mbxwz" title="<c:out value="${product.name}"/>"><img src="<c:out value="${fn:replace(thumb.imageUrl, 'prodimgs', 'prodbigimgs')}"/>" border="0" class="details_thumbnail" alt="<c:out value="${product.name}"/><c:if test="${product.shortDesc != ''}"> - <c:out value="${product.shortDesc}"/></c:if>"/></a>
	  	   </c:when>
	  	   <c:otherwise>
	  	   <a href="<c:if test="${!thumb.absolute}">assets/Image/Product/detailsbig/</c:if><c:out value="${thumb.imageUrl}"/>" id="mb${status.count - 1}" class="mbxwz" title=""><img src="<c:if test="${!thumb.absolute}">assets/Image/Product/detailsbig/</c:if><c:out value="${thumb.imageUrl}"/>" border="0" class="details_thumbnail" alt="<c:out value="${product.name}"/><c:if test="${product.shortDesc != ''}"> - <c:out value="${product.shortDesc}"/></c:if>"/></a>
	  	   </c:otherwise>
	  	  </c:choose>
	  </c:if>
	</c:forEach>
	</div>
	</div>
	</div>        
 </c:when>
 <c:when test="${fn:contains(siteConfig['PRODUCT_IMAGE_LAYOUTS'].value,'qb') and 
 		(product.imageLayout == 'qb' or (empty product.imageLayout and siteConfig['PRODUCT_IMAGE_LAYOUT'].value == 'qb'))}">
 <link rel="stylesheet" href="${_contextpath}/assets/SmoothGallery/QuickBox/quickbox.css" type="text/css" media="screen" />
 <c:set value="${true}" var="quickbox"/>
 <script src="${_contextpath}/javascript/QuickBox.js" type="text/javascript" ></script>
 <script type="text/javascript">
	window.addEvent('domready', function(){
		new QuickBox();
	});
 </script>
 	<div class="details_image_box" id="details_image_boxId" style="${details_images_style}
	 <c:if test="${siteConfig['DETAILS_IMAGE_WIDTH'].value != '' }">width: <c:out value="${siteConfig['DETAILS_IMAGE_WIDTH'].value}"/>px;</c:if>">
	 
	<c:forEach items="${model.product.images}" var="image" varStatus="status">
	  <c:if test="${status.first}">
	  	 <c:choose>
	  	   <c:when test="${product.asiId != null}">
	  	   <a href="<c:out value="${fn:replace(product.thumbnail.imageUrl, 'prodimgs', 'prodbigimgs')}"/>" id="" class="" rel="quickbox" title="<c:out value="${product.name}"/><c:if test="${product.shortDesc != ''}"> - <c:out value="${product.shortDesc}"/></c:if>"><img src="<c:out value="${fn:replace(product.thumbnail.imageUrl, 'prodimgs', 'prodbigimgs')}"/>" border="0" style="<c:if test="${siteConfig['DETAILS_IMAGE_WIDTH'].value != '' }">width: <c:out value="${siteConfig['DETAILS_IMAGE_WIDTH'].value}"/>px</c:if>" alt="<c:out value="${product.name}"/><c:if test="${product.shortDesc != ''}"> - <c:out value="${product.shortDesc}"/></c:if>"/></a>
	  	   </c:when>
	  	   <c:otherwise>
	  	   <a href="<c:if test="${!image.absolute}">assets/Image/Product/detailsbig/</c:if><c:out value="${image.imageUrl}"/>" id="" class="" title="<c:out value="${product.name}"/>" rel="quickbox"><img src="<c:if test="${!image.absolute}">assets/Image/Product/detailsbig/</c:if><c:out value="${image.imageUrl}"/>" border="0" style="<c:if test="${siteConfig['DETAILS_IMAGE_WIDTH'].value != '' }">width: <c:out value="${siteConfig['DETAILS_IMAGE_WIDTH'].value}"/>px</c:if>" alt="<c:out value="${product.name}"/><c:if test="${product.shortDesc != ''}"> - <c:out value="${product.shortDesc}"/></c:if>"/></a>
	  	   </c:otherwise>
	  	  </c:choose>
	  </c:if>
	</c:forEach>
    <div style="clear: both;" >
    <div class="thumbnailWrapper">
    <c:forEach items="${model.product.images}" var="thumb" varStatus="status">
	  <c:if test="${status.count > 1}">
	      <c:choose>
	  	   <c:when test="${product.asiId != null}">
	  	   <a href="<c:out value="${fn:replace(thumb.imageUrl, 'prodimgs', 'prodbigimgs')}"/>" id="qb${status.count - 1}" class="" rel="quickbox" title="<c:out value="${product.name}"/><c:if test="${product.shortDesc != ''}"> - <c:out value="${product.shortDesc}"/></c:if>"><img src="<c:out value="${fn:replace(thumb.imageUrl, 'prodimgs', 'prodbigimgs')}"/>" border="0" class="details_thumbnail" alt="<c:out value="${product.name}"/><c:if test="${product.shortDesc != ''}"> - <c:out value="${product.shortDesc}"/></c:if>"/></a>
	  	   </c:when>
	  	   <c:otherwise>
	  	   <a href="<c:if test="${!thumb.absolute}">assets/Image/Product/detailsbig/</c:if><c:out value="${thumb.imageUrl}"/>" id="qb${status.count - 1}" class="" title="" rel="quickbox"><img src="<c:if test="${!thumb.absolute}">assets/Image/Product/detailsbig/</c:if><c:out value="${thumb.imageUrl}"/>" border="0" class="details_thumbnail" alt="<c:out value="${product.name}"/><c:if test="${product.shortDesc != ''}"> - <c:out value="${product.shortDesc}"/></c:if>"/></a>
	  	   </c:otherwise>
	  	  </c:choose>
	  </c:if>
	</c:forEach>
	</div>
	</div>        
 </c:when>
 <c:otherwise>
    <div class="details_image_box" id="details_image_boxId" style="${details_images_style}
	 <c:if test="${siteConfig['DETAILS_IMAGE_WIDTH'].value != '' }">width: <c:out value="${siteConfig['DETAILS_IMAGE_WIDTH'].value}"/>px;</c:if>">
	 
	<c:forEach items="${model.product.images}" var="image" varStatus="status">
	<c:if test="${status.first}">
		 <img src="<c:if test="${!image.absolute}">assets/Image/Product/detailsbig/</c:if><c:out value="${image.imageUrl}"/>" border="0" id="_image" name="_image" class="details_image"
		 	style="<c:if test="${siteConfig['DETAILS_IMAGE_WIDTH'].value != '' }">width: <c:out value="${siteConfig['DETAILS_IMAGE_WIDTH'].value}"/>px</c:if>" alt="<c:out value="${product.name}"/><c:if test="${product.shortDesc != ''}"> - <c:out value="${product.shortDesc}"/></c:if>">
	<c:if test="${siteConfig['DETAILS_IMAGE_WIDTH'].value != '' }" >
	<div align="right">
	<a onclick="zoomIn()" title="Zoom in"><img src="${_contextpath}/assets/Image/Layout/zoom-icon.gif" alt="Zoom In" width="16" height="16" /></a>
	</div>
	</c:if>
	</c:if>
	
	<c:if test="${status.last and (status.count != 1)}">
	<br>
	<c:forEach items="${model.product.images}" var="thumb">
	<a href="#" class="details_thumbnail_anchor" onClick="_image.src='<c:if test="${!thumb.absolute}">assets/Image/Product/detailsbig/</c:if><c:out value="${thumb.imageUrl}"/>';return false;">
		 <img src="<c:if test="${!thumb.absolute}">assets/Image/Product/detailsbig/</c:if><c:out value="${thumb.imageUrl}"/>" class="details_thumbnail" alt="<c:out value="${product.name}"/><c:if test="${product.shortDesc != ''}"> - <c:out value="${product.shortDesc}"/></c:if>">
	</a>
	</c:forEach>
	</c:if>
	</c:forEach>
	</div>
	<c:if test="${fn:contains(siteConfig['PRODUCT_IMAGE_LAYOUTS'].value,'ss') and 
			(product.imageLayout == 'ss'  or (empty product.imageLayout and siteConfig['PRODUCT_IMAGE_LAYOUT'].value == 'ss'))}">
	<object classid="clsid:D27CDB6E-AE6D-11cf-96B8-444553540000" codebase="http://download.macromedia.com/pub/shockwave/cabs/flash/swflash.cab#version=6,0,29,0" width="750" height="475">
	  <param name="movie" value="${_contextpath}/assets/Flash/slideshow.swf?file=thumbnailer1.jhtm?id=${product.id}">
	  <param name="quality" value="high">
	  <embed src="${_contextpath}/assets/Flash/slideshow.swf?file=thumbnailer1.jhtm?id=${product.id}" quality="high" pluginspage="http://www.macromedia.com/go/getflashplayer" type="application/x-shockwave-flash" width="750" height="475"></embed>
	</object>
	</c:if>
 </c:otherwise>
</c:choose>
</c:set>
<c:out value="${productImage}" escapeXml="false" />
<!-- image end -->
</td>
<td>
<!-- details start -->
<div style="${details_desc_style}" class="details_desc">
<c:if test="${siteConfig['SHOW_SKU'].value == 'true'}">
<c:if test="${product.sku != null}">
<div class="details_sku"><c:out value="${product.sku}" /></div>  
</c:if>
</c:if>

<div class="details_item_name"><h1><c:out value="${product.name}" escapeXml="false" /></h1></div>

<c:if test="${siteConfig['DETAILS_SHORT_DESC'].value == 'true' and product.shortDesc != ''}">
<div class="details_short_desc"><c:out value="${product.shortDesc}" escapeXml="false" /></div>  
</c:if>

<c:if test="${gSiteConfig['gBUDGET_PRODUCT']}">
<div class="budget_balance"><fmt:message key="balance" />: <c:choose><c:when test="${model.budgetProduct[product.sku] != null}"><c:out value="${model.budgetProduct[product.sku].balance}"/></c:when><c:otherwise>0</c:otherwise></c:choose></div>
</c:if>

<c:if test="${siteConfig['DETAILS_LONG_DESC_LOCATION'].value == 'above' and product.longDesc != ''}">
<div class="details_long_desc"><c:out value="${product.longDesc}" escapeXml="false" /></div> 
</c:if>
<%-- 
<%@ include file="/WEB-INF/jsp/frontend/common/price5.jsp" %>
--%>



<c:if test="${siteConfig['TECHNOLOGO'].value != ''}">	   
<div id="technologoWrapper">
 <a href="http://www.technologo.com/techno.OnDemand?&email=<c:out value="${siteConfig['TECHNOLOGO'].value}" />&sku=${product.id}&name=${product.nameWithoutQuotes}&imagelocation_0=${wj:asiBigImageName(product.thumbnail.imageUrl)}&whiteout=true&orientation=10000&removelogo=true" target="_blank">
 <img src="${_contextpath}/assets/Image/Layout/button_create_virtual.gif"  border="0" alt="create virtual" />
 </a>
</div>
</c:if>

<c:if test="${gSiteConfig['gDEALS'] > 0 and product.deal.htmlCode != null}">
	<c:out value="${product.deal.htmlCode}" escapeXml="false"/>
</c:if>
<c:if test="${gSiteConfig['gDEALS'] > 0 and product.deal.parentHtmlCode != null}">
	<c:out value="${product.deal.parentHtmlCode}" escapeXml="false"/>
</c:if>

<c:choose>
<c:when test="${gSiteConfig['gPRODUCT_REVIEW'] and product.enableRate}">
<%@ include file="/WEB-INF/jsp/frontend/common/quickProductReview.jsp" %>
</c:when>
<c:when test="${siteConfig['PRODUCT_RATE'].value == 'true' and product.enableRate}">
<%@ include file="/WEB-INF/jsp/frontend/common/productRate.jsp" %>
</c:when>
</c:choose>

<c:if test="${siteConfig['DETAILS_LONG_DESC_LOCATION'].value != 'above' and product.longDesc != ''}">
<div class="details_long_desc"><c:out value="${product.longDesc}" escapeXml="false" /></div>  
</c:if>

<%-- ASI layout have MyList for all products --%>
<c:if test="${gSiteConfig['gMYLIST']}">
<form action="${_contextpath}/addToList.jhtm">
  <input type="hidden" name="product.id" value="${product.id}">
  <div id="addToListId" class="addToList">
    <input type="image" border="0" src="${_contextpath}/assets/Image/Layout/button_addtolist${_lang}.gif">
  </div>
</form>
</c:if>
</div>
</div>
<!-- details end -->
</td>
</tr>
</table>
            
<c:forEach items="${product.productFields}" var="productField" varStatus="row">
<c:if test="${row.first}">
<c:if test="${siteConfig['PRODUCT_FIELDS_TITLE'].value != ''}">
<div class="details_fields_title"><c:out value="${siteConfig['PRODUCT_FIELDS_TITLE'].value}"/></div>
</c:if>
<table class="details_fields">
</c:if>
<tr>
<td class="details_field_name_row${row.index % 2}"><c:out value="${productField.name}" escapeXml="false" /> </td>
<c:choose>
<c:when test="${productField.formatType != null and productField.formatType != '' and productField.fieldType == 'number' }">
<td class="details_field_value_row${row.index % 2}"><fmt:formatNumber value="${wj:trimAndSplit(productField.value, '|' , false, ',', true, true)}" pattern="${productField.formatType }" /></td>
</c:when>
<c:otherwise>
<td class="details_field_value_row${row.index % 2}"><c:out value="${wj:trimAndSplit(productField.value, '|' , false, ',', true, true)}" escapeXml="false"/></td>
</c:otherwise>
</c:choose>
</tr>
<c:if test="${row.last}">
</table>
</c:if>
</c:forEach>
</div>

<c:if test="${multibox != 'true'}">
<script src="${_contextpath}/javascript/overlay.js" type="text/javascript" ></script>
<script src="${_contextpath}/javascript/multibox.js" type="text/javascript" ></script>
</c:if>
<script type="text/javascript">
var quickViewBox = {};
function closeBox() {
	quickViewBox.close();
}
window.addEvent('domready', function(){
	quickViewBox = new multiBox('showCartmb',  {showNumbers:false, showControls:false, initialWidth:710, overlay: new overlay({opacity:'0.3'})});
});
</script>

<c:set value="${model.asiProduct}" var="asiProduct"></c:set>
<c:if test="${ product.salesTag.image and !empty product.price }" >
<div class="saleTagImage">
 <img src="${_contextpath}/assets/Image/PromoSalesTag/salestag_${product.salesTag.tagId}.gif" />
</div>
</c:if>

<div class="asiOptionsWrapper asiOptionsPrice">
  <div class="asiOptionHdr">Price(s)</div>
  <div class="asiDataBlock">
  	<c:if test="${asiProduct.pricing != null and asiProduct.pricing.priceGrid != null}">
  		
  	  <c:choose>
  	  <c:when test="${model.product.asiIgnorePrice}">


  		<c:forEach items="${asiProduct.pricing.priceGrid}" var="priceGrid" varStatus="stat">
  		<c:if test="${priceGrid.base}">
  			<c:set var="quantity" value="Quantity"></c:set> 
  			<c:set var="listPrice" value="Price"></c:set>
  			<c:set var="netCost" value="Net Cost"></c:set>
  			<c:set var="profit" value="Profit"></c:set>
  			<c:forEach items="${model.product.price}" var="price">
  				<c:set var="quantity" value="${quantity};${price.qtyFrom}"></c:set>
  				<c:set var="listPrice" value="${listPrice};${price.amt}"></c:set>
  			<%-- 	<c:set var="netCost" value="${netCost};${price.cost}"></c:set>
  				<c:set var="profit" value="${profit};${price.value - price.cost}"></c:set> --%>
  			</c:forEach>
  			<c:forEach begin="${fn:length(model.product.price)}" end="7">
  				<c:set var="quantity" value="${quantity}; &nbsp"></c:set>
  				<c:set var="listPrice" value="${listPrice};&nbsp"></c:set>
  			<%-- 	<c:set var="netCost" value="${netCost};&nbsp"></c:set>
  				<c:set var="profit" value="${profit}; &nbsp"></c:set> --%>
  			</c:forEach>
  			<table class="asiPriceTable" cellpadding="0" cellspacing="0" border="0">
  			<tr class="asiPriceTableHeader" height="40px;">
  				<th colspan="9" class="asiPriceTable">
					<c:if test="${priceGrid.dependencies != null and priceGrid.dependencies.dependency != null}">
					  <c:out value="${priceGrid.dependencies.dependency[0].value}"/>
  					</c:if>
  					<!--   				
  				    <c:if test="${model.priceGridNameMap != null && model.priceGridNameMap[priceGrid.id] != null}">${model.priceGridNameMap[priceGrid.id]}</c:if>
  				     -->
  				</th>
  			</tr>
  			<tr class="asiPriceTable" height="40px;">
  			<c:forTokens items="${quantity}" delims=";" var="data">
  				<td class="asiPriceTable" width="60px;">${data}</td>
  			</c:forTokens>
  			</tr>
  			<tr class="asiPriceTable" height="40px;">
  			<c:forTokens items="${listPrice}" delims=";" var="data" varStatus="status">
  			    <c:choose>
  				  <c:when test="${status.index == 0 or data == 'QUR'}">
  				    <td class="asiPriceTable" width="60px;">${data}</td>
  			 	  </c:when>
  				  <c:otherwise>
  				    <td class="asiPriceTable" <c:if test="${product.salesTag != null and product.salesTag.discount != 0.0 and data != '&nbsp'}" >style="text-decoration: line-through"</c:if> width="60px;">${data}</td>
  			 	  </c:otherwise>
  				</c:choose>  
  			</c:forTokens>
  			</tr>
  			<c:if test="${product.salesTag != null and product.salesTag.discount != 0.0 and !fn:contains(listPrice, 'QUR')}" >
  			<tr class="asiPriceTable" height="40px;">
  			<c:forTokens items="${listPrice}" delims=";" var="data" varStatus="status">
  				<td class="asiPriceTable" width="60px;">
  				  <c:choose>
  				    <c:when test="${status.index == 0}">
  				      <fmt:message key="f_priceOnSaleTitle"></fmt:message>
  				  	</c:when>
  				  	<c:when test="${data == '&nbsp'}"></c:when>
  				  	<c:otherwise>
  				  	  <c:choose>
  				    	<c:when test="${product.salesTag.percent}">
  				      	  <fmt:formatNumber value="${data *( (100 - product.salesTag.discount) / 100)}" pattern="#,##0.00" maxFractionDigits="2" />
  				    	</c:when>
  				    	<c:otherwise>
  				      	  <fmt:formatNumber value="${data - product.salesTag.discount}" pattern="#,##0.00" maxFractionDigits="2" />
  				    	</c:otherwise>
  				  	  </c:choose>
  				  	</c:otherwise>
  				  </c:choose>
  				</td>
  			</c:forTokens>
  			</tr>
  			</c:if>
  			<%-- 
  			<tr class="asiPriceTable" height="40px;">
  			<c:forTokens items="${netCost}" delims=";" var="data">
  				<td class="asiPriceTable" width="60px;">${data}</td>
  			</c:forTokens>
  			</tr>
  			<tr class="asiPriceTable" height="40px;">
  			<c:forTokens items="${profit}" delims=";" var="data">
  				<td class="asiPriceTable" width="60px;">${data}</td>
  			</c:forTokens>
  			</tr>
  			--%> 
  			<tr class="asiPriceTable" height="40px;">
  			<td colspan="9">
  			<div class="priceInclude">
  				<c:if test="${priceGrid.priceIncludes != null and fn:length(priceGrid.priceIncludes) != 0}">
  				<div class="name">Price Includes:</div>
  				<div class="value">
  				<c:forEach items="${priceGrid.priceIncludes}" var="priceInclude" varStatus="status">
  					<c:out value="${priceInclude}"/>
  					<c:if test="${status.index < fn:length(priceGrid.priceIncludes) - 1}"> ,</c:if>
  				</c:forEach>
  				</div>
  				</c:if>
  			</div>
  			
  			<div class="addToCart" style="float: right;">
  				<c:choose>
  				  <c:when test="${(priceGrid.id eq 'pricegrid-99') and !model.product.asiIgnorePrice}">
  				  <a href="#${stat.index}" rel="type:element" id="mbAsiPrice${stat.index}" class="mbAsiPrice"><input type="image" border="0" name="_addtocart" class="_addtocart" src="${_contextpath}/assets/Image/Layout/button_addtocart${_lang}.gif" /></a>
			      	<div id="${stat.index}" class="miniWindowWrapper asiQuote">
		  			<form autocomplete="off" onsubmit="saveContactInfo('${stat.index}'); return false;">
		  			<input type="hidden" value="${priceGrid.id}" id="priceGridId_${stat.index}"/>
		  			<input type="hidden" value="${product.sku}" id="sku_${stat.index}"/>
		  			<input type="hidden" value="${product.name}" id="name_${stat.index}"/>
		  			<c:if test="${priceGrid.dependencies != null and priceGrid.dependencies.dependency != null}">
					  <input type="hidden" value="${priceGrid.dependencies.dependency[0].value}" id="priceGridDependecyValue_${stat.index}"/>
	    	  		</c:if>
  					<div class="header">Please provide your contact information</div>
	    	  		<fieldset class="top">
	    	    		<label class="AStitle"><fmt:message key="firstName"/></label>
	    	    		<input name="firstName_${stat.index}" id="firstName_${stat.index}" type="text" size="15" onkeyup="assignValue('firstName_${stat.index}', this.value);"/>
	    	  		</fieldset>
	    	  		<fieldset class="bottom">
	    	    		<label class="AStitle"><fmt:message key="lastName"/></label>
	    	    		<input name="lastName_${stat.index}" id="lastName_${stat.index}" type="text" size="15" onkeyup="assignValue('lastName_${stat.index}', this.value);"/>
	    	  		</fieldset>
	    	  		<fieldset class="bottom">
	    	    		<label class="AStitle"><fmt:message key="email"/></label>
	    	    		<input name="email_${stat.index}" id="email_${stat.index}" type="text" size="15" onkeyup="assignValue('email_${stat.index}', this.value);"/>
	    	  		</fieldset>
	    	  		<fieldset class="bottom">
	    	    		<label class="AStitle"><fmt:message key="phone"/></label>
	    	    		<input name="phone_${stat.index}" id="phone_${stat.index}" type="text" size="15" onkeyup="assignValue('phone_${stat.index}', this.value);"/>
	    	  		</fieldset>
	    	  		<fieldset class="bottom">
	    	    		<label class="AStitle"><fmt:message key="note"/></label>
	    	    		<textarea name="note_${stat.index}" id="note_${stat.index}" onkeyup="assignValue('note_${stat.index}', this.value);"></textarea>
	    	    	</fieldset>
	    	  		<fieldset>
	    	     		<div id="button">
	      					<input type="submit" value="Send"/>
	      				</div>
	    	  		</fieldset>
		  			</form>
		  			</div>
		  			<div id="successMessage_${stat.index}"></div>
				  </c:when>
  				  <c:otherwise>
  				    <a href="${_contextpath}/showAsiOptions.jhtm?id=${product.id}&initialDependencyValue=${fn:escapeXml(priceGrid.dependencies.dependency[0].value)}&initialDependencyId=${priceGrid.dependencies.dependency[0].id}&priceGridId=${priceGrid.id}" rel="width:600,height:515" id="showCartmb${stat.index}" class="showCartmb" title="<fmt:message key='session' />"><input type="image" border="0" name="_addtocart" class="_addtocart" src="${_contextpath}/assets/Image/Layout/button_addtocart${_lang}.gif" /></a>
				  </c:otherwise>
  				</c:choose>
  			</div>
  			</td>
  			</tr>
  			</table>
  			<br>
  		</c:if>
		</c:forEach>
  	  </c:when>
  	  <c:otherwise>
  		<c:forEach items="${asiProduct.pricing.priceGrid}" var="priceGrid" varStatus="stat">
  		<c:if test="${priceGrid.base}">
  			<c:set var="quantity" value="Quantity"></c:set> 
  			<c:set var="listPrice" value="Price"></c:set>
  			<c:set var="netCost" value="Net Cost"></c:set>
  			<c:set var="profit" value="Profit"></c:set>
  			<c:forEach items="${priceGrid.price}" var="price">
  				<c:set var="quantity" value="${quantity};${price.quantity}"></c:set>
  				<c:set var="listPrice" value="${listPrice};${price.value}"></c:set>
  			<%-- 	<c:set var="netCost" value="${netCost};${price.cost}"></c:set>
  				<c:set var="profit" value="${profit};${price.value - price.cost}"></c:set> --%>
  			</c:forEach>
  			<c:forEach begin="${fn:length(priceGrid.price)}" end="7">
  				<c:set var="quantity" value="${quantity}; &nbsp"></c:set>
  				<c:set var="listPrice" value="${listPrice};&nbsp"></c:set>
  			<%-- 	<c:set var="netCost" value="${netCost};&nbsp"></c:set>
  				<c:set var="profit" value="${profit}; &nbsp"></c:set> --%>
  			</c:forEach>
  			<table class="asiPriceTable" cellpadding="0" cellspacing="0" border="0">
  			<tr class="asiPriceTableHeader" height="40px;">
  				<th colspan="9" class="asiPriceTable">
					<c:if test="${priceGrid.dependencies != null and priceGrid.dependencies.dependency != null}">
					  <c:out value="${priceGrid.dependencies.dependency[0].value}"/>
  					</c:if>
  					<!--   				
  				    <c:if test="${model.priceGridNameMap != null && model.priceGridNameMap[priceGrid.id] != null}">${model.priceGridNameMap[priceGrid.id]}</c:if>
  				     -->
  				</th>
  			</tr>
  			<tr class="asiPriceTable" height="40px;">
  			<c:forTokens items="${quantity}" delims=";" var="data">
  				<td class="asiPriceTable" width="60px;">${data}</td>
  			</c:forTokens>
  			</tr>
  			<tr class="asiPriceTable" height="40px;">
  			<c:forTokens items="${listPrice}" delims=";" var="data" varStatus="status">
  			    <c:choose>
  				  <c:when test="${status.index == 0 or data == 'QUR'}">
  				    <td class="asiPriceTable" width="60px;">${data}</td>
  			 	  </c:when>
  				  <c:otherwise>
  				    <td class="asiPriceTable" <c:if test="${product.salesTag != null and product.salesTag.discount != 0.0 and data != '&nbsp'}" >style="text-decoration: line-through"</c:if> width="60px;">${data}</td>
  			 	  </c:otherwise>
  				</c:choose>  
  			</c:forTokens>
  			</tr>
  			<c:if test="${product.salesTag != null and product.salesTag.discount != 0.0 and !fn:contains(listPrice, 'QUR')}" >
  			<tr class="asiPriceTable" height="40px;">
  			<c:forTokens items="${listPrice}" delims=";" var="data" varStatus="status">
  				<td class="asiPriceTable" width="60px;">
  				  <c:choose>
  				    <c:when test="${status.index == 0}">
  				      <fmt:message key="f_priceOnSaleTitle"></fmt:message>
  				  	</c:when>
  				  	<c:when test="${data == '&nbsp'}"></c:when>
  				  	<c:otherwise>
  				  	  <c:choose>
  				    	<c:when test="${product.salesTag.percent}">
  				      	  <fmt:formatNumber value="${data *( (100 - product.salesTag.discount) / 100)}" pattern="#,##0.00" maxFractionDigits="2" />
  				    	</c:when>
  				    	<c:otherwise>
  				      	  <fmt:formatNumber value="${data - product.salesTag.discount}" pattern="#,##0.00" maxFractionDigits="2" />
  				    	</c:otherwise>
  				  	  </c:choose>
  				  	</c:otherwise>
  				  </c:choose>
  				</td>
  			</c:forTokens>
  			</tr>
  			</c:if>
  			<%-- 
  			<tr class="asiPriceTable" height="40px;">
  			<c:forTokens items="${netCost}" delims=";" var="data">
  				<td class="asiPriceTable" width="60px;">${data}</td>
  			</c:forTokens>
  			</tr>
  			<tr class="asiPriceTable" height="40px;">
  			<c:forTokens items="${profit}" delims=";" var="data">
  				<td class="asiPriceTable" width="60px;">${data}</td>
  			</c:forTokens>
  			</tr>
  			--%> 
  			<tr class="asiPriceTable" height="40px;">
  			<td colspan="9">
  			<div class="priceInclude">
  				<c:if test="${priceGrid.priceIncludes != null and fn:length(priceGrid.priceIncludes) != 0}">
  				<div class="name">Price Includes:</div>
  				<div class="value">
  				<c:forEach items="${priceGrid.priceIncludes}" var="priceInclude" varStatus="status">
  					<c:out value="${priceInclude}"/>
  					<c:if test="${status.index < fn:length(priceGrid.priceIncludes) - 1}"> ,</c:if>
  				</c:forEach>
  				</div>
  				</c:if>
  			</div>
  			
  			<div class="addToCart" style="float: right;">
  				<c:choose>
  				  <c:when test="${!(priceGrid.id eq 'pricegrid-99')}">
  				    <a href="${_contextpath}/showAsiOptions.jhtm?id=${product.id}&initialDependencyValue=${priceGrid.dependencies.dependency[0].value}&initialDependencyId=${priceGrid.dependencies.dependency[0].id}&priceGridId=${priceGrid.id}" rel="width:600,height:515" id="showCartmb${stat.index}" class="showCartmb" title="<fmt:message key='session' />"><input type="image" border="0" name="_addtocart" class="_addtocart" src="${_contextpath}/assets/Image/Layout/button_addtocart${_lang}.gif" /></a>
				  </c:when>
  				  <c:otherwise>
  				    <a href="#${stat.index}" rel="type:element" id="mbAsiPrice${stat.index}" class="mbAsiPrice"><input type="image" border="0" name="_addtocart" class="_addtocart" src="${_contextpath}/assets/Image/Layout/button_addtocart${_lang}.gif" /></a>
			      	<div id="${stat.index}" class="miniWindowWrapper asiQuote">
		  			<input type="hidden" value="${priceGrid.id}" id="priceGridId_${stat.index}"/>
		  			<input type="hidden" value="${product.sku}" id="sku_${stat.index}"/>
		  			<input type="hidden" value="${product.name}" id="name_${stat.index}"/>
		  			<c:if test="${priceGrid.dependencies != null and priceGrid.dependencies.dependency != null}">
					  <input type="hidden" value="${priceGrid.dependencies.dependency[0].value}" id="priceGridDependecyValue_${stat.index}"/>
	    	  		</c:if>
  					<div class="header">Please provide your contact information</div>
	    	  		<fieldset class="top">
	    	    		<label class="AStitle"><fmt:message key="firstName"/></label>
	    	    		<input name="firstName_${stat.index}" id="firstName_${stat.index}" type="text" size="15" onkeyup="assignValue('firstName_${stat.index}', this.value);"/>
	    	  		</fieldset>
	    	  		<fieldset class="bottom">
	    	    		<label class="AStitle"><fmt:message key="lastName"/></label>
	    	    		<input name="lastName_${stat.index}" id="lastName_${stat.index}" type="text" size="15" onkeyup="assignValue('lastName_${stat.index}', this.value);"/>
	    	  		</fieldset>
	    	  		<fieldset class="bottom">
	    	    		<label class="AStitle"><fmt:message key="email"/></label>
	    	    		<input name="email_${stat.index}" id="email_${stat.index}" type="text" size="15" onkeyup="assignValue('email_${stat.index}', this.value);"/>
	    	  		</fieldset>
	    	  		<fieldset class="bottom">
	    	    		<label class="AStitle"><fmt:message key="phone"/></label>
	    	    		<input name="phone_${stat.index}" id="phone_${stat.index}" type="text" size="15" onkeyup="assignValue('phone_${stat.index}', this.value);"/>
	    	  		</fieldset>
	    	  		<fieldset class="bottom">
	    	    		<label class="AStitle"><fmt:message key="note"/></label>
	    	    		<textarea name="note_${stat.index}" id="note_${stat.index}" onkeyup="assignValue('note_${stat.index}', this.value);"></textarea>
	    	    	</fieldset>
	    	  		<fieldset>
	    	     		<div id="button">
	      					<input type="submit" value="Send"  onclick="saveContactInfo('${stat.index}');"/>
	      				</div>
	    	  		</fieldset>
		  			</div>
		  			<div id="successMessage_${stat.index}"></div>
				  </c:otherwise>
  				</c:choose>
  			</div>
  			</td>
  			</tr>
  			</table>
  			<br>
  		</c:if>
		</c:forEach>
  		  
  		  
  	  </c:otherwise>
  	  </c:choose>
  		
  		<%-- 
		<c:set var="opName" value=""/>
	 	<c:set var="myIndex" value="0"/> 
	    <c:forEach items="${model.asiPrices}" var="priceGrid" varStatus="statusGridPrice">
	    <c:if test="${opName != priceGrid.optionName}">
	    <c:set var="opName" value="${priceGrid.optionName}"></c:set>
		<div class="addToCart">
			
       	 	<a href="#htmlElement${myIndex}" rel="type:element" id="mb${myIndex}" class="mbxwz" title="<fmt:message key='session' />"><input type="image" border="0" name="_addtocart" class="_addtocart" src="${_contextpath}/assets/Image/Layout/button_addtocart${_lang}.gif" /></a>
			<div class="multiBoxDesc mb${myIndex}"></div><br />
			<div id="htmlElement${myIndex}" style="width:550px;">
			    <!-- Custom add to Cart -->
				<form action="addASIToCart.jhtm" name="this_form${priceGrid.optionName}" method="get" onsubmit="return checkForm()">
				<input type="hidden" name="product.id" value="${product.id}">
				
				<c:forEach items="${model.asiProductOption}" var="productOption" varStatus="poSatus">
					<input type="hidden" name="asi_option_${product.id}_${priceGrid.optionName}" value="<c:out value="${productOption.name}"/>">
					<c:out value="${productOption.name}"/>
					<div>
					<select name="asi_option_values_${product.id}_${productOption.name}_${priceGrid.optionName}" class="optionSelect">
					  <c:forEach items="${productOption.values}" var="prodOptValue" varStatus="povStatus">
					     <option value="${prodOptValue.name}"><c:out value="${prodOptValue.name}"/></option>
					  </c:forEach>
					</select>
					</div>
				</c:forEach>
				
				<input type="text" maxlength="5" name="quantity_${product.id}" id="quantity_${product.id}" value="<c:out value='${product.minimumQty}' />">
				<input type="hidden" name="optionPrice_${product.id}" value="<c:out value='${priceGrid.optionName}' />">
				<input type="image" border="0" name="_addtocart" class="_addtocart" src="${_contextpath}/assets/Image/Layout/button_addtocart${_lang}.gif" />
       	 		</form>
				<!-- Custom add to Cart -->
			   </div>
		</div>
		</c:if>
		</c:forEach>
		--%>
		
	</c:if>
  </div>
</div>

<c:if test="${model.additionalCharges != null and fn:length(model.additionalCharges) > 0}">
  <div class="asiOptionsWrapper">
  <div class="asiOptionHdr">Additional Charges May Apply</div>
  <div class="asiDataBlock">
  	<c:forEach items="${model.additionalCharges}" var="attribute">
  	  <c:if test="${attribute.value != null}">
  		<div class="name">${attribute.key}: &nbsp; </div>
  		<div class="value"><c:out value="${attribute.value}" escapeXml="false"/></div>
  		<br>
	  </c:if>
  	</c:forEach>
  </div>
  </div>
</c:if>

<c:if test="${model.productAttributes != null}">
  <div class="asiOptionsWrapper">
  <div class="asiOptionHdr">Product Attributes</div>
  <div class="asiDataBlock">
  	<c:forEach items="${model.productAttributes}" var="attribute">
  	  <c:if test="${attribute.value != null}">
  		<div class="name">${attribute.key}: &nbsp; </div>
  		<c:set var="optionValue"></c:set>
  		<c:forEach items="${attribute.value}" var="value" varStatus="status">
  			<c:choose>
  			  <c:when test="${status.index == 0}">
  				<c:set var="optionValue" value="${value}"></c:set>
  			  </c:when>
  			  <c:otherwise>
  				<c:set var="optionValue" value="${value}; ${optionValue}"></c:set>
  			  </c:otherwise>
  			</c:choose>
  		</c:forEach>
  		<div class="value"><c:out value="${optionValue}" escapeXml="false"/></div>
  		<br>
	  </c:if>
  	</c:forEach>
  </div>
  </div>
</c:if>
<c:if test="${model.imprintInfo != null and fn:length(model.imprintInfo) > 0}">
  <div class="asiOptionsWrapper">
  <div class="asiOptionHdr">Imprint Information</div>
  <div class="asiDataBlock">
  	<c:forEach items="${model.imprintInfo}" var="attribute">
  	  <c:if test="${attribute.value != null}">
  		<div class="name">${attribute.key}: &nbsp; </div>
  		<c:set var="optionValue"></c:set>
  		<c:forEach items="${attribute.value}" var="value" varStatus="status">
   			<c:choose>
  			  <c:when test="${status.index == 0}">
  				<c:set var="optionValue" value="${value}"></c:set>
  			  </c:when>
  			  <c:otherwise>
  				<c:set var="optionValue" value="${value}; ${optionValue}"></c:set>
  			  </c:otherwise>
  			</c:choose>
  		</c:forEach>
  		<div class="value"><c:out value="${optionValue}" escapeXml="false"/></div>
  		<br>
	  </c:if>
  	</c:forEach>
  </div>
  </div>
</c:if>

<c:if test="${asiProduct.productOptions != null and asiProduct.productOptions.criteriaSet != null}">
	<c:set var="isOptionExist" value="0" />
	<c:forEach items="${asiProduct.productOptions.criteriaSet}" var="criteria">
  		<c:if test="${criteria.name eq 'ProductOption'}"><c:set var="isOptionExist" value="1" /></c:if>
  	</c:forEach>
	<c:if test="${isOptionExist == 1}">
	<div class="asiOptionsWrapper">
  	<div class="asiOptionHdr">Product Options</div>
  	<div class="asiDataBlock">
  	<c:forEach items="${asiProduct.productOptions.criteriaSet}" var="criteria">
  		<c:if test="${criteria.name eq 'ProductOption' and criteria.options == null}">
  			<div class="name"><c:out value="${criteria.description}" escapeXml="false"/> </div>
  			<div class="value"> &nbsp; </div>
  			<br>
  		</c:if>
  		<c:if test="${criteria.name eq 'ProductOption' and criteria.options != null}">
  			<div class="name">Product Option: &nbsp; </div>
  			<c:set var="optionValue"></c:set>
  			<c:forEach items="${criteria.options.option}" var="option" varStatus="status">
  			<c:choose>
  			  <c:when test="${status.index == 0}">
  				<c:set var="optionValue" value="${option.value}"></c:set>
  			  </c:when>
  			  <c:otherwise>
  				<c:set var="optionValue" value="${option.value}; ${optionValue}"></c:set>
  			  </c:otherwise>
  			</c:choose>
  			</c:forEach>
  			<div class="value"><c:out value="${optionValue}" escapeXml="false"/></div>
  			<br>
		</c:if>
	</c:forEach>
  	</div>
  	</div>
	</c:if>
</c:if>
<c:if test="${asiProduct.generalOptions != null and asiProduct.generalOptions.criteriaSet != null && !empty asiProduct.generalOptions.criteriaSet}">
  	<div class="asiOptionsWrapper">
  	<div class="asiOptionHdr">Other Options</div>
    <div class="asiDataBlock">
  	<c:forEach items="${asiProduct.generalOptions.criteriaSet}" var="criteria">
  		<c:set var="optionValue"></c:set>
  		<c:forEach items="${criteria.options.option}" var="option" varStatus="status">
  			<c:choose>
  			  <c:when test="${status.index == 0}">
  				<c:set var="optionValue" value="${option.detail}"></c:set>
  			  </c:when>
  			  <c:otherwise>
  				<c:set var="optionValue" value="${option.detail}; ${optionValue}"></c:set>
  			  </c:otherwise>
  			</c:choose>
  		</c:forEach>
  		<c:if test="${optionValue == '' and criteria.description != null}">
  			<c:set var="optionValue" value="${criteria.description}"></c:set>
  		</c:if>
  		<div class="name">
  		<c:choose>
  		  <c:when test="${criteria.label != null}"><c:out value="${criteria.label[0]}"></c:out>: &nbsp;</c:when>
  		  <c:otherwise><c:out value="${criteria.name}"></c:out>: &nbsp;</c:otherwise>
  		</c:choose>
  		</div>
  		<div class="value"><c:out value="${optionValue}" escapeXml="false"/></div>
  		<br/>
  	</c:forEach>
  	</div>
  	</div>
</c:if>


</td><td class="middleRight"></td>
</tr>
<tr class="bottom">
<td class="bottomLeft"><img src="${_contextpath}/assets/Image/Layout/bottomleft.gif"  border="0" alt="" /></td>
<td class="bottomBottom"></td>
<td class="bottomRight"><img src="${_contextpath}/assets/Image/Layout/bottomright.gif"  border="0" alt="" /></td>
</tr>
</table>

<%@ include file="/WEB-INF/jsp/frontend/common/productTabs.jsp" %>

<c:if test="${model.alsoConsiderMap[product.id] != null}">
   <c:set var="product" value="${model.alsoConsiderMap[product.id]}" />
   <%@ include file="/WEB-INF/jsp/frontend/common/alsoconsider.jsp" %>
   <c:set value="${model.product}" var="product"/>
</c:if>


<c:set var="recommendedList_HTML">
<c:if test="${model.recommendedList != null}">
<div style="float:left;width:100%">
<c:choose>
 <c:when test="${!empty product.recommendedListTitle }">
  <div class="recommended_list"><c:out value="${product.recommendedListTitle}"/></div>
 </c:when>
 <c:otherwise>
  <c:if test="${siteConfig['RECOMMENDED_LIST_TITLE'].value != ''}">
   <div class="recommended_list"><c:out value="${siteConfig['RECOMMENDED_LIST_TITLE'].value}"/></div>
  </c:if>
 </c:otherwise>
</c:choose>
<c:set var="numCul" value="${siteConfig['NUMBER_PRODUCT_PER_ROW'].value}" />
<c:forEach items="${model.recommendedList}" var="product" varStatus="status">

<c:set var="productLink">${_contextpath}/product.jhtm?id=${product.id}<c:if test="${model.product == null or siteConfig['RECOMMENDED_LIST_SHOW_BREADCRUMB'].value == 'true'}">&cid=<c:out value="${param.cid}"/></c:if></c:set>
<c:if test="${gSiteConfig['gMOD_REWRITE'] == '1' and not fn:contains(product.sku, '/')}">
  <c:set var="productLink">${_contextpath}/${siteConfig['MOD_REWRITE_PRODUCT'].value}/${product.encodedSku}/${product.encodedName}.html<c:if test="${model.product == null or siteConfig['RECOMMENDED_LIST_SHOW_BREADCRUMB'].value == 'true'}">?cid=<c:out value="${param.cid}"/></c:if></c:set>
</c:if>

<c:choose>
 <c:when test="${( model.product.recommendedListDisplay == 'quick') or (empty model.product.recommendedListDisplay and siteConfig['RECOMMENDED_LIST_DISPLAY'].value == 'quick')}">
    <%@ include file="/WEB-INF/jsp/frontend/quickmode/view1.jsp" %>
 </c:when>
 <c:when test="${( model.product.recommendedListDisplay == '2') or (empty model.product.recommendedListDisplay and siteConfig['RECOMMENDED_LIST_DISPLAY'].value == '2')}">
    <%@ include file="/WEB-INF/jsp/frontend/thumbnail/view2.jsp" %>
 </c:when>
 <c:when test="${( model.product.recommendedListDisplay == '3') or (empty model.product.recommendedListDisplay and siteConfig['RECOMMENDED_LIST_DISPLAY'].value == '3')}">
	<%@ include file="/WEB-INF/jsp/frontend/thumbnail/view3.jsp" %>
 </c:when>
 <c:when test="${( model.product.recommendedListDisplay == '4') or (empty model.product.recommendedListDisplay and siteConfig['RECOMMENDED_LIST_DISPLAY'].value == '4')}">
	<%@ include file="/WEB-INF/jsp/frontend/thumbnail/view4.jsp" %>
 </c:when>
 <c:when test="${( model.product.recommendedListDisplay == '5') or (empty model.product.recommendedListDisplay and siteConfig['RECOMMENDED_LIST_DISPLAY'].value == '5')}">
	<%@ include file="/WEB-INF/jsp/frontend/thumbnail/view5.jsp" %>
 </c:when>
 <c:when test="${( model.product.recommendedListDisplay == '6') or (empty model.product.recommendedListDisplay and siteConfig['RECOMMENDED_LIST_DISPLAY'].value == '6')}">
	<%@ include file="/WEB-INF/jsp/frontend/thumbnail/view6.jsp" %>
 </c:when>
 <c:when test="${( model.product.recommendedListDisplay == '7') or (empty model.product.recommendedListDisplay and siteConfig['RECOMMENDED_LIST_DISPLAY'].value == '7')}">
	<%@ include file="/WEB-INF/jsp/frontend/thumbnail/view7.jsp" %>
 </c:when>
 <c:when test="${( model.product.recommendedListDisplay == '8') or (empty model.product.recommendedListDisplay and siteConfig['RECOMMENDED_LIST_DISPLAY'].value == '8')}">
	<%@ include file="/WEB-INF/jsp/frontend/thumbnail/view8.jsp" %>
 </c:when>
 <c:when test="${( model.product.recommendedListDisplay == '9') or (empty model.product.recommendedListDisplay and siteConfig['RECOMMENDED_LIST_DISPLAY'].value == '9')}">
	<%@ include file="/WEB-INF/jsp/frontend/thumbnail/view9.jsp" %>
 </c:when>
 <c:when test="${( model.product.recommendedListDisplay == '10') or (empty model.product.recommendedListDisplay and siteConfig['RECOMMENDED_LIST_DISPLAY'].value == '10')}">
    <%@ include file="/WEB-INF/jsp/frontend/thumbnail/view10.jsp" %>
 </c:when>
 <c:when test="${( model.product.recommendedListDisplay == '11') or (empty model.product.recommendedListDisplay and siteConfig['RECOMMENDED_LIST_DISPLAY'].value == '11')}">
    <%@ include file="/WEB-INF/jsp/frontend/thumbnail/view11.jsp" %>
 </c:when>
 <c:when test="${( model.product.recommendedListDisplay == '12') or (empty model.product.recommendedListDisplay and siteConfig['RECOMMENDED_LIST_DISPLAY'].value == '12')}">
    <%@ include file="/WEB-INF/jsp/frontend/thumbnail/view12.jsp" %>
 </c:when>
 <c:otherwise>
	<%@ include file="/WEB-INF/jsp/frontend/thumbnail/view1.jsp" %>
 </c:otherwise>
</c:choose>
</c:forEach>
</div>
</c:if>
</c:set>
<c:out value="${recommendedList_HTML}" escapeXml="false" />


<c:if test="${gSiteConfig['gPRODUCT_REVIEW'] and siteConfig['PRODUCT_RATE'].value != 'true' and model.product.enableRate}">
<div style="float:left;width:100%">
<%@ include file="/WEB-INF/jsp/frontend/common/productReviewList.jsp" %>
</div>
</c:if>
</c:if>

<c:if test="${fn:trim(model.productLayout.footerHtml) != ''}">
  <c:set value="${model.productLayout.footerHtml}" var="footerHtml"/>
  <%@ include file="/WEB-INF/jsp/frontend/common/footerDynamicElement.jsp" %>
  <div style="float:left;width:100%"><c:out value="${footerHtml}" escapeXml="false"/></div>
</c:if>



    
  </tiles:putAttribute>
</tiles:insertDefinition>