<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="/WEB-INF/tlds/spring.tld" prefix="spring" %>

<tiles:insertDefinition name="${_template}" flush="true">
  <tiles:putAttribute name="content" type="string">

<form method="post">
<input type="hidden" name="_page" value="2">
<spring:bind path="forgetPasswordForm.token">
<input type="hidden" name="<c:out value="${status.expression}"/>" value="<c:out value="${status.value}"/>"/>
</spring:bind>
<fieldset class="register_fieldset" id="forgetPassword_fieldsetId">
<legend>Change Your Password in Three Easy Steps.</legend>
<table>
  <tr> 
    <td align="right" style="white-space: nowrap" class="done_step">Step 1.</td>
    <td class="done_step">Enter the e-mail address associated with your <c:out value="${model.SITE_NAME}"/> account (done). 
    </td>
  </tr>
  <tr valign="top"> 
    <td align="right" style="white-space: nowrap" class="done_step">Step 2.</td>
    <td class="done_step">Check your e-mail and click the link (done).</td>
  </tr>
  <tr> 
    <td align="right" style="white-space: nowrap"><img src="${_contextpath}/assets/Image/Layout/select_right_8x8.gif" border="0"> Step 3.</td>
    <td>Please enter your new password twice below, then click "Submit."</td>
  </tr>
  <tr> 
    <td>&nbsp;</td>
    <td> 
      <table border="0" cellpadding="3" cellspacing="1">
        <tr> 
          <td style="white-space: nowrap"><fmt:message key="newPassword" />:</td>
          <td> 
		      <spring:bind path="forgetPasswordForm.newPassword">
		      <input type="password" name="<c:out value="${status.expression}"/>" value="<c:out value="${status.value}"/>"/>
		      <c:out value="${status.errorMessage}"/>
		      </spring:bind>
          </td>
        </tr>
        <tr> 
          <td style="white-space: nowrap"><fmt:message key="confirmPassword" />:</td>
          <td> 
		      <spring:bind path="forgetPasswordForm.confirmPassword">
		      <input type="password" name="<c:out value="${status.expression}"/>" value="<c:out value="${status.value}"/>"/>
		      <c:out value="${status.errorMessage}"/>
		      </spring:bind>
          </td>
        </tr>
      </table>
      <spring:bind path="forgetPasswordForm.message">
	  <c:out value="${status.errorMessage}"/>
      </spring:bind>
    </td>
  </tr>
</table>
</fieldset>
<div align="center">
  <input type="submit" value="<fmt:message key="submit" />" name="_finish">
</div>
</form>

  </tiles:putAttribute>
</tiles:insertDefinition>