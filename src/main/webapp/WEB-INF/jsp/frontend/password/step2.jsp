<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="/WEB-INF/tlds/spring.tld" prefix="spring" %>

<tiles:insertDefinition name="${_template}" flush="true">
  <tiles:putAttribute name="content" type="string">

<form method="post">
<fieldset class="register_fieldset" id="forgetPassword_fieldsetId">
<legend>Change Your Password in Three Easy Steps.</legend>
<table>
  <tr> 
    <td align="right" style="white-space: nowrap" class="done_step">Step 1.</td>
    <td class="done_step">Enter the e-mail address associated with your <c:out value="${model.SITE_NAME}"/> account (done). 
    </td>
  </tr>
  <tr valign="top"> 
    <td align="right" style="white-space: nowrap"><img src="${_contextpath}/assets/Image/Layout/select_right_8x8.gif" border="0"> Step 2.</td>
    <td>Check your e-mail and open the message from <c:out value="${model.CONTACT_EMAIL}"/> that contains the subject line "<c:out value="${model.SITE_NAME}"/> Password Assistance." Click on the link indicated in the message to get to Step 3 
    </td>
  </tr>
  <tr> 
    <td colspan="2">&nbsp;</td>
  </tr>
</table>
We have sent an e-mail to ${forgetPasswordForm.email}. When you receive the e-mail please click on the link in the e-mail. 
</fieldset>
</form>

  </tiles:putAttribute>
</tiles:insertDefinition>