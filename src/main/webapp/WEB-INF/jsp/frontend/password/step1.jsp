<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="/WEB-INF/tlds/spring.tld" prefix="spring" %>

<tiles:insertDefinition name="${_template}" flush="true">
  <tiles:putAttribute name="content" type="string">

<form method="post">
<input type="hidden" name="_page" value="0">
<fieldset class="register_fieldset" id="forgetPassword_fieldsetId">
<legend>Change Your Password in Three Easy Steps.</legend>
<table>
  <tr> 
    <td style="white-space: nowrap"><img src="${_contextpath}/assets/Image/Layout/select_right_8x8.gif" border="0"> Step 1.</td>
    <td>Enter the e-mail address associated with your <c:out value="${model.SITE_NAME}"/> account. 
    </td>
  </tr>
  <tr> 
    <td>&nbsp;</td>
    <td> 
      <table border="0" cellpadding="3" cellspacing="1">
        <tr> 
          <td style="white-space: nowrap"><fmt:message key="emailAddress"/>:</td>
          <td> 
		      <spring:bind path="forgetPasswordForm.email">
		      <input type="text" name="<c:out value="${status.expression}"/>" value="<c:out value="${status.value}"/>"/>
		      <span class="error"><c:out value="${status.errorMessage}"/></span>
		      </spring:bind>
          </td>
        </tr>
      </table>
      <spring:bind path="forgetPasswordForm.message">
	  <span class="error"><c:out value="${status.errorMessage}" escapeXml="false"/></span>
      </spring:bind>
    </td>
  </tr>
</table>
</fieldset>
<div align="center">
  <input type="submit" value="<fmt:message key="nextStep" />" name="_target1">
</div>
</form>

  </tiles:putAttribute>
</tiles:insertDefinition>