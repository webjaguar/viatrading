<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>

<%-- subcategories --%>
<c:if test="${model.thisCategory.subcatLocation == '1' && not empty model.subCategories}">
<c:choose>
 <c:when test="${siteConfig['SUBCAT_DISPLAY'].value == 'row'}"><c:import url="/WEB-INF/jsp/frontend/layout/template4/subcategoriesRow.jsp" /></c:when>
 <c:otherwise><c:import url="/WEB-INF/jsp/frontend/layout/template4/subcategories.jsp" /></c:otherwise>
</c:choose>
</c:if>


<c:if test="${model.thisCategory['htmlCode'] != '' }">
<table border="0" cellpadding="0" cellspacing="0" class="categoryHtml">
  <tr>
    <td><c:out value="${model.thisCategory['htmlCode']}" escapeXml="false" /></td>
  </tr>
</table>
</c:if>


<%-- subcategories --%>
<c:if test="${model.thisCategory.subcatLocation != '1' && not empty model.subCategories}">
<c:choose>
 <c:when test="${siteConfig['SUBCAT_DISPLAY'].value == 'row'}"><c:import url="/WEB-INF/jsp/frontend/layout/template4/subcategoriesRow.jsp" /></c:when>
 <c:otherwise><c:import url="/WEB-INF/jsp/frontend/layout/template4/subcategories.jsp" /></c:otherwise>
</c:choose>
</c:if>