<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="/WEB-INF/tlds/spring.tld" prefix="spring" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>

<tiles:insertDefinition name="${_template}" flush="true">
  <tiles:putAttribute name="content" type="string">
  
<c:out value="${model.loginLayout.headerHtml}" escapeXml="false"/>

<c:if test="${param.noMessage != 't'}">
	<c:if test="${!empty model.message}">
	  <div class="error"><br/><b><font color="RED"><fmt:message key="${model.message}"><fmt:param value="${siteConfig['IP_ADDRESS_ERROR_MESSAGE'].value}"/></fmt:message></font></b></div>
	</c:if>
</c:if>

<c:if test="${model.evergreenMessage}">
  <div><br/><b><font color="RED">Unable to find your account information. If you are an existing Evergreen customer with an account number, please contact Evergreen customer support. Otherwise, please create a new account.</font></b></div>
</c:if>

<%@ include file="/WEB-INF/jsp/custom/login1.jsp" %>
<table class="loginWrapper" align="center" border="0">
<tr valign="top">
<c:if test="${!(gSiteConfig['gREGISTRATION_DISABLED'] or siteConfig['PROTECTED_HOST'].value == header['host'])}">
<c:set var="newCustomer">
<td>
<fieldset class="register_newCustomer_fieldset">
<legend><fmt:message key="newCustomers"/></legend>
<form action="register.jhtm" method="GET">
<c:if test="${!empty signonForwardAction}">
<input id="forwardActionId" type="hidden" name="forwardAction" value="${signonForwardAction}"/>
</c:if>
<table class="newCustomerWrapper" align="center" border="0" height="110">
<tr>
<td><input id="createNewAccountId" type="submit" value="<fmt:message key="createNewAccount"/>" class="f_button" /></td>
</tr>
</table>
</form>
</fieldset>
</td>
</c:set>
<c:if test="${gSiteConfig['gACCOUNTING'] != 'EVERGREEN'}">
<c:out value="${newCustomer}" escapeXml="false" />
</c:if>
</c:if>
<td>
<fieldset class="register_returnCustomer_fieldset">
<legend><c:choose><c:when test="${gSiteConfig['gREGISTRATION_DISABLED']}">Please Sign In</c:when><c:otherwise><fmt:message key="returningCustomers"/></c:otherwise></c:choose></legend>
<form action="${model.secureUrl}login.jhtm" method="POST">
<c:if test="${!empty signonForwardAction}">
<input id="forwardActionId" type="hidden" name="forwardAction" value="${signonForwardAction}"/>
</c:if>
<table class="returnCustomerWrapper" align="center" border="0" height="110" cellspacing="0" cellpadding="0">
<tr class="username">
<td><fmt:message key="emailAddress"/>:</td>
<td><input type="text" name="username" value="${param.username}" /></td>
</tr>
<c:if test="${gSiteConfig['gREGISTRATION_TOUCH_SCREEN']}">
<tr class="username">
<td colspan="2">OR</td>
</tr>
<tr class="username">
<td><fmt:message key="idCard" />:</td>
<td><input type="text" name="cardid" value="" /></td>
</tr>
</c:if>
<tr class="password">
<td><fmt:message key="password"/>:</td>
<td><input type="password" name="password" value="" /></td>
</tr>
<c:if test="${siteConfig['COOKIE_AGE_KEEP_ME_LOGIN'].value != -1}">
<tr class="keepMeLogin">
<td><fmt:message key="f_keepMeLogin" />:</td>
<td><input type="checkbox" name="keepmelogin" value="1" /></td>
</tr>
</c:if>
<tr>
<td>&nbsp;</td>
<td><a href="forgetPassword.jhtm" class="forgotPassword"><fmt:message key="forgotYourPassword?"/></a></td>
</tr>
<tr>
<td>&nbsp;</td>
<td align="left" id="loginID"><input type="submit" value="<fmt:message key="logIn"/>" class="f_button" /></td>
</tr>
</table>
</form>
</fieldset>
</td>
</tr>
<c:if test="${gSiteConfig['gACCOUNTING'] == 'EVERGREEN'}">
<tr>
<td>
<fieldset class="register_fieldset">
<legend>Already an Evergreen Customer but New to the Web?</legend>
<form action="login.jhtm" method="POST">
<c:if test="${!empty signonForwardAction}">
<input type="hidden" name="forwardAction" value="${signonForwardAction}"/>
</c:if>
<table align="center" border="0" height="110">
<tr>
<td><fmt:message key="accountNumber"/>:</td>
<td><input type="text" name="accountNum" value="${param.accountNum}" /></td>
</tr>
<tr>
<td><fmt:message key="zipCode"/>:</td>
<td><input type="text" name="zipCode" value="${param.zipCode}" /></td>
</tr>
<tr>
<td colspan="2" align="center" id="newAccountId"><input type="submit" value="<fmt:message key="submit"/>" class="f_button"/></td>
</tr>
</table>
</form>
</fieldset>
</td>
</tr>
<tr>
<c:out value="${newCustomer}" escapeXml="false" />
</tr>
</c:if>
</table>
<%@ include file="/WEB-INF/jsp/custom/login2.jsp" %>

<c:out value="${model.loginLayout.footerHtml}" escapeXml="false"/>

  </tiles:putAttribute>
</tiles:insertDefinition>
