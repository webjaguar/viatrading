<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ taglib uri="/WEB-INF/tlds/spring.tld" prefix="spring" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ taglib uri="http://www.springframework.org/security/tags" prefix="sec" %>

<tiles:insertDefinition name="${_template}" flush="true">
  <tiles:putAttribute name="content" type="string">  

<div class="crmErrorFormBox" id="crmErrorFormBoxId">
  <div class="errorHandler">
    <spring:hasBindErrors name="crmFormProcessorForm">
		<div class="message">Please fix all errors!</div>
	  </spring:hasBindErrors>
  </div>
  <div class="headerTitle  crmErrorForm">
    <c:out value="${crmForm.header}" escapeXml="false"/>
  </div>
  <div class="crmErrorFormWrapper" id="crmErrorFormWrapperId">
    <form:form commandName="crmFormProcessorForm" action="formProcessor.jhtm" method="post" enctype="multipart/form-data" >
  
	  <table cellpadding="0" cellspacing="0" border="0" class="crmFormBox" >
	  <c:set var="index" value="0"/>
	  <c:forEach items="${crmForm.crmFields}" var="field" varStatus="status">
	  <tr>
	    <td>${field.fieldName} :</td>
	    <c:choose>
	    <c:when test="${field.contactFieldId == 1010}">
	    <td><form:input path="crmContact.firstName" htmlEscape="true"/></td>
	    <td><form:errors path="crmContact.firstName" cssClass="error"/></td>
	    </c:when>
	    
	    <c:when test="${field.contactFieldId == 1020}">
	    <td><form:input path="crmContact.lastName" htmlEscape="true"/></td>
	    <td><form:errors path="crmContact.lastName" cssClass="error"/></td>
	    </c:when>
	    
	    <c:when test="${field.contactFieldId == 1030}">
	    <td><form:input path="crmContact.email1" htmlEscape="true"/></td>
	    <td><form:errors path="crmContact.email1" cssClass="error"/></td>
	    </c:when>
	    
	    <c:when test="${field.contactFieldId == 1040}">
	    <td><form:input path="crmContact.email2" htmlEscape="true"/></td>
	    <td><form:errors path="crmContact.email2" cssClass="error"/></td>
	    </c:when>
	    
	    <c:when test="${field.contactFieldId == 1050}">
	    <td><form:input path="crmContact.phone1" htmlEscape="true"/></td>
	    <td><form:errors path="crmContact.phone1" cssClass="error"/></td>
	    </c:when>
	    
	    <c:when test="${field.contactFieldId == 1060}">
	    <td><form:input path="crmContact.phone2" htmlEscape="true"/></td>
	    <td><form:errors path="crmContact.phone2" cssClass="error"/></td>
	    </c:when>
	    
	    <c:when test="${field.contactFieldId == 1070}">
	    <td><form:input path="crmContact.fax" htmlEscape="true"/></td>
	    <td><form:errors path="crmContact.fax" cssClass="error"/></td>
	    </c:when>
	    
	    <c:when test="${field.contactFieldId == 1080}">
	    <td><form:textarea path="crmContact.street" htmlEscape="true"/></td>
	    <td><form:errors path="crmContact.street" cssClass="error"/></td>
	    </c:when>
	    
	    <c:when test="${field.contactFieldId == 1090}">
	    <td><form:input path="crmContact.city" htmlEscape="true"/></td>
	    <td><form:errors path="crmContact.city" cssClass="error"/></td>
	    </c:when>
	    
	    <c:when test="${field.contactFieldId == 1100}">
	    <td><form:input path="crmContact.stateProvince" htmlEscape="true"/></td>
	    <td><form:errors path="crmContact.stateProvince" cssClass="error"/></td>
	    </c:when>
	    
	    <c:when test="${field.contactFieldId == 1110}">
	    <td><form:input path="crmContact.zip" htmlEscape="true"/></td>
	    <td><form:errors path="crmContact.zip" cssClass="error"/></td>
	    </c:when>
	    
	    <c:when test="${field.contactFieldId == 1120}">
	    <td><form:input path="crmContact.country" htmlEscape="true"/></td>
	    <td><form:errors path="crmContact.country" cssClass="error"/></td>
	    </c:when>
	    
	    <c:when test="${field.contactFieldId == 1130}">
	    <td><form:input path="crmContact.description" htmlEscape="true"/></td>
	    <td><form:errors path="crmContact.description" cssClass="error"/></td>
	    </c:when>
	    
	    <c:when test="${field.contactFieldId == 1160}">
	    <td><form:input path="crmContact.language" htmlEscape="true"/></td>
	    <td><form:errors path="crmContact.language" cssClass="error"/></td>
	    </c:when>
	    
	    <c:when test="${field.contactFieldId == 1140}">
	    <td><c:forTokens items="${field.options}" delims="," var="option">
	        <form:radiobutton path="crmContact.addressType" value="${option}" label="${option}"/>
	       </c:forTokens>
	    </td>
	    <td><form:errors path="crmContact.addressType" cssClass="error"/></td>
	    </c:when>
	    
	    <c:otherwise>
	      <c:if test="${field.type == 1 or field.type == 7}">
	      <td><form:input path="crmContact.crmContactFields[${index}].fieldValue" htmlEscape="true"/></td>
	      </c:if>
	     
	    
	    <c:if test="${field.type == 2}">
	      <td><form:textarea path="crmContact.crmContactFields[${index}].fieldValue"/></td>
	    </c:if>
	    
	    <c:if test="${field.type == 3}">
	    <td>
	      <c:forTokens items="${field.options}" delims="," var="option" >
	      	<form:checkbox path="crmContact.crmContactFields[${index}].fieldValue" value="${option}" label="${option}"/>
	      </c:forTokens>
	    </td>  
	    </c:if>
	    
	    <c:if test="${field.type == 4}">
	      <td>
	      <c:forTokens items="${field.options}" delims="," var="option">
	        <form:radiobutton path="crmContact.crmContactFields[${index}].fieldValue" value="${option}" label="${option}"/>
	      </c:forTokens>
	      </td>
	    </c:if>
	    
	    <c:if test="${field.type == 5}">
	      <td>
	      <form:select path="crmContact.crmContactFields[${index}].fieldValue">
	      <form:option value="" label="Please Select">Please Select</form:option>
	      <c:forTokens items="${field.options}" delims="," var="option">
	        <form:option value="${option}" label="${option}">${option}</form:option>
	      </c:forTokens>
	      </form:select>
	      </td>
	    </c:if>
	    <c:if test="${field.type == 6}">
	      <td>
	        <input type="file" name="attachment">
	        <form:errors path="crmContact.attachment" cssClass="error"/>
	      </td>
	    </c:if>
	    <td>
	      <form:errors path="crmContact.crmContactFields[${index}].fieldValue" cssClass="error"/>
	    </td>
	    <input type="hidden" value="${field.contactFieldId}" name="field${index}"></input>  	  
	    <c:set var="index" value="${index+1}"/>
	    </c:otherwise>
	    </c:choose>
	    
	  </tr>
	  </c:forEach>
	  
	  <c:if test="${crmFormProcessorForm.aQuest != null and crmFormProcessorForm.aQuest != ''}">
	    <tr>
	      <td>
	        <c:out value="${crmFormProcessorForm.aQuest}"></c:out>
	      </td>
	      <td>
	        <form:input path="uAns" htmlEscape="true"/>
	        <form:errors path="uAns" cssClass="error"/>
	      </td>
	   </tr> 
	  </c:if>
	    
	  </table>
	  
	  <input type="hidden" value="${crmForm.formNumber}" name="formNumber"></input>
	  <input type="hidden" value="${groupId}" name="groupId"></input>
	  <input type="hidden" value="${returnUrl}" name="returnUrl"></input>
	  <input type="hidden" value="${updateIfExist}" name="updateIfExist"></input>
	  <input type="hidden" value="${adminEmailSubject}" name="adminEmailSubject"></input>
	  <form:hidden path="crmContact.trackcode"/>  	  
	  <form:hidden path="crmContact.leadSource"/>  	  
	  <form:hidden path="aAns"/>  	  
	  <input type="submit" value="${crmForm.sendButton}" name="sendButton"></input>
	    	  
	</form:form>
  </div>
  <div class="footerTitle  crmErrorForm">
    <c:out value="${crmForm.footer}" escapeXml="false"/>
  </div>
  
</div>

  </tiles:putAttribute>
</tiles:insertDefinition>