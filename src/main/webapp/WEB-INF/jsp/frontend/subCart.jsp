<%@ page import="com.webjaguar.model.*, java.text.NumberFormat, java.util.*" %>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<!-- This JSP is only for Master skus, added on the cart with child skus; Just for display -->
  <c:set var="tempCartItem" value="${model.groupItems[cartItem.itemGroup]}"></c:set>
  <c:set var="tempProductLink" value="${_contextpath}/product.jhtm?id=${tempCartItem.product.id}"></c:set>
  <c:if test="${gSiteConfig['gMOD_REWRITE'] == '1' and not fn:contains(tempCartItem.product.sku, '/')}">
    <c:set var="tempProductLink" value="${_contextpath}/${siteConfig['MOD_REWRITE_PRODUCT'].value}/${tempCartItem.product.encodedSku}/${tempCartItem.product.encodedName}.html"></c:set>
  </c:if>	
  
  <c:choose>
    <c:when test="${displaySkuAndName}">
  	  <!-- Sku and Name Starts-->
  	  <tr valign="top" class="shoppingCart${currentItemIndex % 2} parentSkuTitle">
        <td class="cartItemTitle cartName" valign="top">
		  <c:if test="${siteConfig['SHOW_IMAGE_ON_INVOICE'].value == 'true' and tempCartItem.product.thumbnail != null}">
    	    <div class="cartImageWrapper">
			  <a href="<c:if test="${!tempCartItem.product.thumbnail.absolute}">${_contextpath}/assets/Image/Product/thumb/</c:if>${tempCartItem.product.thumbnail.imageUrl}" rel="width:600,height:515" id="imageMb${cartStatus.index}" class="imageMb shoppingcart_item_image">
         	    <img class="cartImage" src="<c:if test="${!tempCartItem.product.thumbnail.absolute}">${_contextpath}/assets/Image/Product/thumb/</c:if>${tempCartItem.product.thumbnail.imageUrl}" border="0" />
			  </a>
			</div>
		  </c:if>
		  <div class="cartItemSkuNameWrapper">
    	    <a  href="${tempProductLink}" class="shoppingcart_item_name parentCartItem"><c:out value="${tempCartItem.product.name}"/></a>
		    <br />
		    <c:if test="${siteConfig['SHOW_SKU'].value == 'true' and tempCartItem.product.sku != null}">
	  	      <a  href="${tempProductLink}" class="shoppingcart_item_sku parentCartItem"><c:out value="${tempCartItem.product.sku}"/></a>
		    </c:if>
		  </div>  
		  <!-- clear float, if any -->
	  	  <div style="clear: both;" class="clear"></div>  
	  	  <table border="0" cellspacing="1" cellpadding="0">
		  <c:forEach items="${tempCartItem.productAttributes}" var="productAttribute" varStatus="productAttributeStatus">
			<tr class="shoppingCartOption${productAttributeStatus.count%2}">
			<td class="optionName" style="white-space: nowrap" align="right"><c:out value="${productAttribute.optionName}"/>: </td>
			<td class="optionValue" style="padding-left:5px;"><c:out value="${productAttribute.valueString}" escapeXml="false"/></td>
			  <td class="optionValue" style="padding-left:5px;"><c:out value="${productAttribute.optionPriceOriginal}" escapeXml="false"/></td>
			</tr>
		  </c:forEach>
		  <c:forEach items="${tempCartItem.asiProductAttributes}" var="asiProductAttribute" varStatus="asiProductAttributeStatus">
			<tr class="shoppingCartOption${asiProductAttributeStatus.count%2}">
			<td class="optionName" style="white-space: nowrap" align="right"><c:out value="${asiProductAttribute.asiOptionName}"/>: </td>
			<td class="optionValue" style="padding-left:5px;"><c:out value="${asiProductAttribute.asiOptionValue}" escapeXml="false"/></td>
			</tr>
		  </c:forEach>
		  </table>  		
		  <c:if test="${tempCartItem.subscriptionInterval != null}">
		  <c:set var="intervalType" value="${fn:substring(tempCartItem.subscriptionInterval, 0, 1)}"/>
		  <c:set var="intervalUnit" value="${fn:substring(tempCartItem.subscriptionInterval, 1, 2)}"/>
		  <div style="color: #EE0000;"><fmt:message key="subscription"/> <fmt:message key="details"/>: <fmt:message key="delivery"/>
		    <c:choose>
		      <c:when test="${intervalUnit == '1'}"><fmt:message key="every_${intervalType}"><fmt:param value="${intervalUnit}"/></fmt:message></c:when>
		      <c:otherwise><fmt:message key="every_${intervalType}s"><fmt:param value="${intervalUnit}"/></fmt:message></c:otherwise>
		    </c:choose>
		    <c:if test="${tempCartItem.product.subscriptionDiscount != null}">
		      (Save <fmt:formatNumber value="${tempCartItem.product.subscriptionDiscount}" pattern="#,##0.00" />%)
		    </c:if>
		  </div>  					
		  </c:if>
		  <c:if test="${siteConfig['CASE_CONTENT_LAYOUT'].value == '2' and tempCartItem.priceCasePackQty != null}">
		    <div><c:out value="${siteConfig['PRICE_CASE_PACK_UNIT_TITLE'].value}" /> Qty: <c:out value="${tempCartItem.priceCasePackQty}" /></div>
		  </c:if>
	  </td>
        <td class="cartQty" style="align:left; padding-left:10px ">
	      <table id="parentSkuTotalQtyHdr${cartItem.itemGroup}">
	        <tr>
		      <td width="15px;" align="center"><c:out value="${tempCartItem.quantity}"/></td>
		      <td valign="top"></td>
		    </tr>
		    <c:if test="${gSiteConfig['gPRICE_CASEPACK'] and tempCartItem.priceCasePackQty != null}">
			<tr>
		  	  <td colspan="2"><input type="checkbox" name="casePacing" checked="checked" disabled="disabled"/><c:out value="${siteConfig['PRICE_CASE_PACK_UNIT_TITLE'].value}" /></td>
			</tr>
			</c:if>
	  	  </table>
		</td>
		<td class="cartPrice" align="center"></td>
    	<td class="cartTotal" align="center">
      	  <c:choose>
            <c:when test="${gSiteConfig['gVARIABLE_PRICE'] and tempCartItem.variableUnitPrice != null}">
              <div id="parentSkuTotalPriceHdr${cartItem.itemGroup}"><fmt:message key="${siteConfig['CURRENCY'].value}" /><fmt:formatNumber value="${tempCartItem.totalVariablePrice}" pattern="#,##0.00"/></div>
        	</c:when>
        	<c:otherwise>
          	  <div id="parentSkuTotalPriceHdr${cartItem.itemGroup}"><fmt:message key="${siteConfig['CURRENCY'].value}" /><fmt:formatNumber value="${tempCartItem.tempItemTotal}" pattern="#,##0.00"/></div>        
        	</c:otherwise>
      	  </c:choose>
    	</td>
    	<c:if test="${model.cart.hasManufacturer}"> 	
        <td align="right">
          <span class="brandName"><c:out value="${tempCartItem.product.manufactureName}"/></span>
        </td>
      	</c:if>
      <td align="center"></td>
	</tr>
	<!-- Sku and Name Ends-->
  </c:when>
  
  <c:when test="${showQtyAndPrice}">
  <!-- Quantity and Price Starts-->
  <tr valign="top" class="shoppingCart${currentItemIndex % 2} parentQtyTotal">
    <td></td>
    <td class="cartQty" style="align:left; padding-left:10px ">
	  <table id="parentSkuTotalQtyFtr${cartItem.itemGroup}" style="display: none;">
	    <tr>
		  <td width="15px;" align="center" style="font-size: 16px;"><c:out value="${tempCartItem.quantity}"/></td>
		  <td valign="top"></td>
		</tr>
		<c:if test="${gSiteConfig['gPRICE_CASEPACK'] and tempCartItem.priceCasePackQty != null}">
		<tr>
		  <td colspan="2"><input type="checkbox" name="casePacing" checked="checked" disabled="disabled"/><c:out value="${siteConfig['PRICE_CASE_PACK_UNIT_TITLE'].value}" /></td>
		</tr>
		</c:if>
	  </table>
	</td>
	<td class="cartPrice" align="center"></td>
    <td class="cartTotal" align="center" style="font-size: 16px;">
      <c:choose>
        <c:when test="${gSiteConfig['gVARIABLE_PRICE'] and tempCartItem.variableUnitPrice != null}">
          <div id="parentSkuTotalPriceFtr${cartItem.itemGroup}"  style="display: none;"><fmt:message key="${siteConfig['CURRENCY'].value}" /><fmt:formatNumber value="${tempCartItem.totalVariablePrice}" pattern="#,##0.00"/></div>
        </c:when>
        <c:otherwise>
          <div id="parentSkuTotalPriceFtr${cartItem.itemGroup}"  style="display: none;"><fmt:message key="${siteConfig['CURRENCY'].value}" /><fmt:formatNumber value="${tempCartItem.tempItemTotal}" pattern="#,##0.00"/></div>        
        </c:otherwise>
      </c:choose>
    </td>
	<c:if test="${model.cart.hasManufacturer}"> 	
      <td align="right">
        <span class="brandName"><c:out value="${tempCartItem.product.manufactureName}"/></span>
      </td>
    </c:if>
    <td align="center"></td>
  </tr>
  <!-- Quantity and Price Ends-->
  </c:when>	  
</c:choose>
  