<%@ page import="java.io.*,java.util.*" %>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>

<%
File baseImageFile = new File(getServletContext().getRealPath("/assets/Image"));
Properties prop = new Properties();
try {
	prop.load(new FileInputStream(getServletContext().getRealPath("WEB-INF/properties/site.properties")));
	if (prop.get("site.root") != null) {
		baseImageFile = new File((String) prop.get("site.root") + "/assets/Image");
	}
} catch (Exception e) {}
%>

<tiles:insertDefinition name="${_template}" flush="true">
  <tiles:putAttribute name="content" type="string">
<script type="text/javascript">
<!--
function checkNumber( aNumber )
{
	var goodChars = "0123456789";
	var i = 0;
	if ( aNumber == "" )
		return 0; //empty
	
	for ( i = 0 ; i <= aNumber.length - 1 ; i++ )
	{
		if ( goodChars.indexOf( aNumber.charAt(i) ) == -1 ) 
			return -1; //invalid number			
	}	
	return 1; //valid number
}
function checkForm()
{
	var productList = document.getElementsByName('product.id');
	var allQtyEmpty = true;
	for(var i=0; i<productList.length; i++) 
	{
	    var el = document.getElementById("quantity_" + productList[i].value);
		if ( el != null )
		{
			if ( checkNumber( el.value ) == 1  )
			{
				allQtyEmpty = false;
			}
			else if ( checkNumber( el.value ) == -1 ) 
			{
				alert("invalid Quantity");
				el.focus();
				return false;
			}			
		}	
	}
	if ( allQtyEmpty )
	{
		alert("Please Enter Quantity.");
		return false;
	}
	else
		return true;	
}
//-->
</script>

<c:if test="${model.account == null}">
Member not found.
</c:if>

<c:if test="${model.account != null}">
<c:set var="supplierId" value="${model.account.supplierId}"/>
<%
if (pageContext.getAttribute("supplierId") != null && new File(baseImageFile, "CompanyLogo/logo_" + pageContext.getAttribute("supplierId") + ".gif").exists()) {
	pageContext.setAttribute("logo", "assets/Image/CompanyLogo/logo_" + pageContext.getAttribute("supplierId") + ".gif");
} else {
	pageContext.setAttribute("logo", null);
}
%>
<fieldset class="register_fieldset">
<legend><fmt:message key="company" /></legend>
<table border="0" cellpadding="0" cellspacing="2" width="100%">
  <tr>
    <td style="width:200px;">
      <div class="companyimg">
	    <c:if test="${logo != null}"><img src="${logo}" border="0" width="200"></c:if>
		<c:if test="${logo == null}">&nbsp;</c:if>
      </div>
      <c:if test="${gSiteConfig['gCOMPANY_REVIEW']}">
        <div class="reviewStar">
	  	<c:choose>
	     <c:when test="${model.account.rate == null}"><img src="${_contextpath}/assets/Image/Layout/star_0.gif" alt=<c:out value="${product.rate.average}"/> title="<c:out value="${product.rate.average}"/>" border="0" /></c:when>
	     <c:when test="${model.account.rate.average > 0 and model.account.rate.average <= 0.6}"><img src="${_contextpath}/assets/Image/Layout/star_0h.gif" alt=<c:out value="${model.account.rate.average}"/> title="<c:out value="${model.account.rate.average}"/>" border="0" /></c:when>
	     <c:when test="${model.account.rate.average > 0.6 and model.account.rate.average <= 1}"><img src="${_contextpath}/assets/Image/Layout/star_1.gif" alt=<c:out value="${model.account.rate.average}"/> title="<c:out value="${model.account.rate.average}"/>" border="0" /></c:when>
	     <c:when test="${model.account.rate.average > 1 and model.account.rate.average <= 1.6}"><img src="${_contextpath}/assets/Image/Layout/star_1h.gif" alt=<c:out value="${model.account.rate.average}"/> title="<c:out value="${model.account.rate.average}"/>" border="0" /></c:when>
	     <c:when test="${model.account.rate.average > 1.6 and model.account.rate.average <= 2}"><img src="${_contextpath}/assets/Image/Layout/star_2.gif" alt=<c:out value="${model.account.rate.average}"/> title="<c:out value="${model.account.rate.average}"/>" border="0" /></c:when>
	     <c:when test="${model.account.rate.average > 2 and model.account.rate.average <= 2.6}"><img src="${_contextpath}/assets/Image/Layout/star_2h.gif" alt=<c:out value="${model.account.rate.average}"/> title="<c:out value="${model.account.rate.average}"/>" border="0" /></c:when>
	     <c:when test="${model.account.rate.average > 2.6 and model.account.rate.average <= 3}"><img src="${_contextpath}/assets/Image/Layout/star_3.gif" alt=<c:out value="${model.account.rate.average}"/> title="<c:out value="${model.account.rate.average}"/>" border="0" /></c:when>
	     <c:when test="${model.account.rate.average > 3 and model.account.rate.average <= 3.6}"><img src="${_contextpath}/assets/Image/Layout/star_3h.gif" alt=<c:out value="${model.account.rate.average}"/> title="<c:out value="${model.account.rate.average}"/>" border="0" /></c:when>
	     <c:when test="${model.account.rate.average > 3.6 and model.account.rate.average <= 4}"><img src="${_contextpath}/assets/Image/Layout/star_4.gif" alt=<c:out value="${model.account.rate.average}"/> title="<c:out value="${model.account.rate.average}"/>" border="0" /></c:when>
	     <c:when test="${model.account.rate.average > 4 and model.account.rate.average <= 4.6}"><img src="${_contextpath}/assets/Image/Layout/star_4h.gif" alt=<c:out value="${model.account.rate.average}"/> title="<c:out value="${model.account.rate.average}"/>" border="0" /></c:when>
	     <c:when test="${model.account.rate.average > 4.6}"><img src="${_contextpath}/assets/Image/Layout/star_5.gif" alt=<c:out value="${model.account.rate.average}"/> title="<c:out value="${model.account.rate.average}"/>" border="0" /></c:when>
	   </c:choose>
        </div>
      </c:if>  
    </td>
    <td>
      <table border="0" cellpadding="0" cellspacing="2" width="100%">
      <tr>
	    <td width="20%" align="right"><fmt:message key="companyName" />:</td>
	    <td>&nbsp;</td>
	    <td><c:out value="${model.account.address.company}"/></td>
	  </tr>
	  <tr>
	    <td align="right"><fmt:message key="contactPerson" />:</td>
	    <td>&nbsp;</td>
	    <td><c:out value="${model.account.address.firstName}"/> <c:out value="${model.account.address.lastName}"/></td>
	  </tr>
	  <tr>
	    <td align="right"><fmt:message key="address" />:</td>
	    <td>&nbsp;</td>
	    <td><c:out value="${model.account.address.addr1}"/> <c:out value="${model.account.address.addr2}"/></td>
	  </tr>
	  <tr>
	    <td>&nbsp;</td>
	    <td>&nbsp;</td>
	    <td><c:out value="${model.account.address.city}"/>, <c:out value="${model.account.address.stateProvince}"/>
	    		<c:out value="${model.account.address.country}"/>
	    </td>
	  </tr>
	  <tr>
	    <td align="right"><fmt:message key="zipCode" />:</td>
	    <td>&nbsp;</td>
	    <td><c:out value="${model.account.address.zip}"/></td>
	  </tr>
	  <tr>
	    <td align="right"><fmt:message key="phone" />:</td>
	    <td>&nbsp;</td>
	    <td><c:out value="${model.account.address.phone}"/></td>
	  </tr>
	  <tr>
	    <td align="right"><fmt:message key="fax" />:</td>
	    <td>&nbsp;</td>
	    <td><c:out value="${model.account.address.fax}"/></td>
	  </tr>
	  <tr>
	    <td>&nbsp;</td>
	    <td>&nbsp;</td>
	    <td>&nbsp;</td>
	  </tr>
	  <tr>
	    <td align="right">Website:</td>
	    <td>&nbsp;</td>
	    <td><a href="<c:out value="${model.account.field8}"/>" target="_blank"><c:out value="${model.account.field8}"/></a></td>
	  </tr>
	  <tr>
	    <td>&nbsp;</td>
	    <td>&nbsp;</td>
	    <td><c:out value="${model.account.shortDesc}"/></td>
	  </tr>
	  </table>
    </td>
  </tr>
  <tr>
    <td colspan="2"><c:out value="${model.account.longDesc}" escapeXml="false" /></td>
  </tr>
</table>  
</fieldset>
</c:if>

<c:if test="${model.count > 0}">
<c:set var="pageShowing">
<table border="0" cellpadding="0" cellspacing="1" width="100%">
  <tr>
  <td class="pageShowing">
  <fmt:message key="showing">
	<fmt:param value="${frontProductSearchMember.offset + 1}"/>
	<fmt:param value="${model.pageEnd}"/>
	<fmt:param value="${model.count}"/>
  </fmt:message>
  </td>
  <td class="pageNavi">
<form action="member.jhtm" method="get">
Page 
<c:choose>
<c:when test="${model.pageCount <= 100 and model.pageCount > 0}">
	    <select name="page" id="page" onchange="submit()">
		<c:forEach begin="1" end="${model.pageCount}" var="page">
  	        <option value="${page}" <c:if test="${page == (frontProductSearchMember.page)}">selected</c:if>>${page}</option>
		</c:forEach>
		</select>
</c:when>
<c:otherwise>
		<input type="text" id="page" name="page" value="${frontProductSearchMember.page}" size="5" class="textfield50" />
		<input type="submit" value="go"/>
</c:otherwise>
</c:choose>
of <c:out value="${model.pageCount}"/>
| 
<c:if test="${frontProductSearchMember.page == 1}"><span class="pageNaviDead"><fmt:message key="f_previous" /></span></c:if>
<c:if test="${frontProductSearchMember.page != 1}"><a href="member.jhtm?page=${frontProductSearchMember.page-1}" class="pageNaviLink"><fmt:message key="f_previous" /></a></c:if>
| 
<c:if test="${frontProductSearchMember.page == model.pageCount}"><span class="pageNaviDead"><fmt:message key="f_next" /></span></c:if>
<c:if test="${frontProductSearchMember.page != model.pageCount}"><a href="member.jhtm?page=${frontProductSearchMember.page+1}" class="pageNaviLink"><fmt:message key="f_next" /></a></c:if>
<select name="size" onchange="document.getElementById('page').value=1;submit()">
<c:forTokens items="10,25,50,100" delims="," var="current">
 <option value="${current}" <c:if test="${current == frontProductSearchMember.pageSize}">selected</c:if>>${current} <fmt:message key="perPage" /></option>
</c:forTokens>
</select>
</form>
  </td>
  </tr>
</table>
</c:set>
<c:out value="${pageShowing}" escapeXml="false" />
</c:if>

<form action="${_contextpath}/addToCart.jhtm" method="post" onsubmit="return checkForm()">

<c:forEach items="${model.products}" var="product" varStatus="status">
    
<%@ include file="/WEB-INF/jsp/frontend/quickmode/view1.jsp" %>

</c:forEach>

</form>

<c:if test="${model.count > 0 && model.pageCount > 1}">
<c:out value="${pageShowing}" escapeXml="false" />
</c:if>

<c:if test="${gSiteConfig['gCOMPANY_REVIEW']}">
<div style="float:left;width:100%">
<%@ include file="/WEB-INF/jsp/frontend/common/companyReviewList.jsp" %>
</div>
</c:if>
    
  </tiles:putAttribute>
</tiles:insertDefinition>
