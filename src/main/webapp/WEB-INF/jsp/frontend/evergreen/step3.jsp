<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="/WEB-INF/tlds/spring.tld" prefix="spring" %>

<tiles:insertDefinition name="${_template}" flush="true">
  <tiles:putAttribute name="content" type="string">

<form method="post">
<input type="hidden" name="_page" value="2">
<fieldset class="register_fieldset">
<legend><fmt:message key="pleaseVerify" /></legend>
<table border="0" cellpadding="3" cellspacing="1">
  <tr>
    <td><fmt:message key="accountNumber" />:</td>
    <td><c:out value="${customerForm.customer.accountNumber}"/></td>
  </tr>
  <tr>
    <td><fmt:message key="company" />:</td>
    <td><c:out value="${customerForm.customer.address.company}"/></td>
  </tr> 
  <tr>
    <td><fmt:message key="firstName" />:</td>
    <td><c:out value="${customerForm.customer.address.firstName}"/></td>
  </tr>
  <tr>
    <td><fmt:message key="lastName" />:</td>
    <td><c:out value="${customerForm.customer.address.lastName}"/></td>
  </tr> 
  <tr>
    <td><fmt:message key='emailAddress' />:</td>
    <td><c:out value="${customerForm.customer.username}"/></td>
  </tr>
</table>
</fieldset>
<div align="center">
  <input type="submit" value="<fmt:message key="change" />" name="_target0">
  <input type="submit" value="<fmt:message key="register" />" name="_finish">
</div>
</form>

  </tiles:putAttribute>
</tiles:insertDefinition>