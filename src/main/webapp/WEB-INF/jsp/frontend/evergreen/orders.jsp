<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>

<tiles:insertDefinition name="${_template}" flush="true">
  <tiles:putAttribute name="content" type="string">

<script type="text/javascript" language="javascript" src="evergreen_orders/evergreen_orders.nocache.js"></script>

<input id="pendingIds" type="hidden" value="<c:forEach items="${model.list}" var="map" varStatus="status"><c:if test="${map['STATUS'] == 'Pending'}">${status.index},</c:if></c:forEach>"/>
<input id="unableToShip1Ids" type="hidden" value="<c:forEach items="${model.list}" var="map" varStatus="status"><c:if test="${map['STATUS'] == '1.Unable to Ship'}">${status.index},</c:if></c:forEach>"/>
<input id="unableToShip2Ids" type="hidden" value="<c:forEach items="${model.list}" var="map" varStatus="status"><c:if test="${map['STATUS'] == '2.Unable to Ship'}">${status.index},</c:if></c:forEach>"/>
<input id="holdForConfirmationIds" type="hidden" value="<c:forEach items="${model.list}" var="map" varStatus="status"><c:if test="${map['STATUS'] == 'Hold for Confirmation'}">${status.index},</c:if></c:forEach>"/>
<input id="pleasePrepayIds" type="hidden" value="<c:forEach items="${model.list}" var="map" varStatus="status"><c:if test="${map['STATUS'] == 'Please Prepay'}">${status.index},</c:if></c:forEach>"/>
<input id="creditCardDeclinedIds" type="hidden" value="<c:forEach items="${model.list}" var="map" varStatus="status"><c:if test="${map['STATUS'] == 'Credit Card Declined'}">${status.index},</c:if></c:forEach>"/>
<input id="creditCardInvalidIds" type="hidden" value="<c:forEach items="${model.list}" var="map" varStatus="status"><c:if test="${map['STATUS'] == 'Credit Card Invalid'}">${status.index},</c:if></c:forEach>"/>

<style type="text/css">
 .normalLink {
 		color: blue;
 		text-decoration: underline; 
 		cursor: pointer;
		}
 .redLink {
 		color: red;
 		text-decoration: underline; 
 		cursor: pointer;
		}
 .ccLabelColumn {
 		text-align: right;
		}
 .cardError {
 		color: red;
		}
 .orderNum {
 		font-weight: bold;
		}
</style>

<c:out value="${model.evergreenLayout.headerHtml}" escapeXml="false"/>

<table border="0" cellpadding="3" cellspacing="1" width="100%" class="listings"> 
  <tr class="listingsHdr2">
    <td align="center">Order ID</td>
    <td align="center">Order Date</td>
    <td class="nameCol">PO #</td>
    <td align="center">Status</td>
    <td align="center">Booked Total</td>
    <td align="center">Shipped Total</td>
    <td align="center">Actual Delivery Date</td>
    <td align="center">ORDER SOURCE</td>
  </tr>
<c:forEach items="${model.list}" var="map" varStatus="status">
  <tr class="row${status.index % 2}">
    <td align="center"><input type="hidden" id="status${status.index}_orderId" value="${map['ORDER_ID']}"><a class="nameLink" href="evergreen_order.jhtm?id=${map['ORDER_ID']}"><c:out value="${map['ORDER_ID']}"/></a></td>
    <td align="center"><fmt:formatDate type="date" value="${map['DATA_ENTRY_DATE']}" pattern="MM/dd/yy"/></td>
    <td class="nameCol"><c:out value="${map['PONUM']}"/></td>
    <c:choose>
	<c:when test="${map['STATUS'] == 'Pending'}">
        <td id="status${status.index}" class="normalLink" align="center"></td>
	</c:when>
	<c:when test="${map['STATUS'] == '1.Unable to Ship' 
				or map['STATUS'] == '2.Unable to Ship' 
				or map['STATUS'] == 'Hold for Confirmation'
				or map['STATUS'] == 'Please Prepay'
				or map['STATUS'] == 'Credit Card Declined'
				or map['STATUS'] == 'Credit Card Invalid'
				}">
        <td id="status${status.index}" class="redLink" align="center"></td>
	</c:when>
    <c:otherwise>
        <td align="center"><c:out value="${map['STATUS']}"/></td>
    </c:otherwise>
    </c:choose>
    <td align="right"><fmt:message key="${siteConfig['CURRENCY'].value}" /><fmt:formatNumber value="${map['BOOK_AMT']}" pattern="#,##0.00" /></td>
    <td align="right"><fmt:message key="${siteConfig['CURRENCY'].value}" /><fmt:formatNumber value="${map['SHIP_AMT']}" pattern="#,##0.00" /></td>
    <td align="center"><fmt:formatDate type="date" value="${map['REL_D_DATE']}" pattern="MM/dd/yy"/></td>
    <td align="center"><c:out value="${map['ORDER_SOURCE']}"/></td>
  </tr>
</c:forEach>
<c:if test="${empty model.list}">
  <tr class="emptyList">
    <td colspan="8">&nbsp;</td>
  </tr>
</c:if>
</table>

<c:out value="${model.evergreenLayout.footerHtml}" escapeXml="false"/>
    
  </tiles:putAttribute>
</tiles:insertDefinition>
