<%@ page import="java.util.*" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
  <head>
    <title>Invoice</title>
    <link href="assets/invoice.css" rel="stylesheet" type="text/css"/>
  </head>  
<body class="invoice">
<div style="width:700px;margin:auto;">

<c:if test="${message != null}">
  <div class="message"><fmt:message key="${message}" /></div>
</c:if>

<c:if test="${order != null}">
<c:out value="${invoiceLayout.headerHtml}" escapeXml="false"/>

<table border="0" width="100%" cellspacing="3" cellpadding="1">
<tr valign="top">
<td>
<b>Bill To:</b>
<br>
<table border="0" cellpadding="0" cellspacing="0">
<tr><td><c:out value="${client['B_STREET1']}"/></td></tr>
<c:if test="${client['B_STREET2'] != ''}">
<tr><td><c:out value="${client['B_STREET1']}"/></td></tr>
</c:if>
<tr><td><c:out value="${client['B_CITY']}"/>, <c:out value="${client['B_STATE']}"/> <c:out value="${client['B_ZIP']}"/></td></tr>
<c:if test="${client['TELEPHONE'] != ''}">
<tr><td>Tel: <c:out value="${client['TELEPHONE']}"/></td></tr>
</c:if>
<tr><td>Fax: <c:out value="${client['FAX_NUMBER']}"/></td></tr>
<tr><td>POC: <c:out value="${client['CLIENT_POC']}"/></td></tr>
</table>
</td>
<td>&nbsp;</td>
<td>
<b>Ship To:</b>
<br>
<table border="0" cellpadding="0" cellspacing="0">
<tr><td><c:out value="${order['S_STREET1']}"/></td></tr>
<c:if test="${order['S_STREET2'] != ''}">
<tr><td><c:out value="${order['S_STREET2']}"/></td></tr>
</c:if>
<tr><td><c:out value="${order['S_CITY']}"/>, <c:out value="${order['S_STATE']}"/> <c:out value="${order['S_ZIP']}"/></td></tr>
</table>
</td>
<td>&nbsp;</td>
<td align="right">
<table>
  <tr>
    <td>Order #</td>
    <td>:&nbsp;&nbsp;</td>
    <td><b><c:out value="${order['ORDER_ID']}"/></b></td>
  </tr>
  <tr>
    <td>ORDER DATE</td>
    <td>:&nbsp;&nbsp;</td>
    <td><fmt:formatDate type="date" value="${order['ORDER_DATE']}" pattern="MM/dd/yy"/></td>
  </tr>
  <tr>
    <td>SHIP DATE</td>
    <td>:&nbsp;&nbsp;</td>
    <td><fmt:formatDate type="date" value="${order['SHIP_DATE']}" pattern="MM/dd/yy"/></td>
  </tr>
  <tr>
    <td>CANCEL DATE</td>
    <td>:&nbsp;&nbsp;</td>
    <td><fmt:formatDate type="date" value="${order['CANCEL_DATE']}" pattern="MM/dd/yy"/></td>
  </tr>
  <tr>
    <td>PO #</td>
    <td>:&nbsp;&nbsp;</td>
    <td><c:out value="${order['PONUM']}"/></td>
  </tr>
  <tr>
    <td>Order Source #</td>
    <td>:&nbsp;&nbsp;</td>
    <td><c:out value="${order['ORDER_SOURCE']}"/></td>
  </tr>
  <tr>
    <td>Rep#</td>
    <td>:&nbsp;&nbsp;</td>
    <td><c:out value="${order['REP_ID1']}"/></td>
  </tr>
</table>
</tr>
</table>
<b>Client ID#:</b>&nbsp;<c:out value="${client['WHOLE_S_ID']}"/><br>
<b>Client Name:</b>&nbsp;<c:out value="${client['CLIENTNAME']}"/><br>
<b><fmt:message key="emailAddress" />:</b>&nbsp;<c:out value="${client['EMAIL_ADDR']}"/>
<table border="0" cellpadding="2" cellspacing="1" width="100%" class="invoice">
  <tr>
    <c:set var="cols" value="0"/>  
    <th width="5%" class="invoice">SEQ</th>
    <th class="invoice">Item #</th>
    <th class="invoice">UPC</th>
    <th class="invoice">DESCRIPTION</th>
    <th width="10%" class="invoice">QTY</th>
    <th class="invoice">UNIT PRICE</th>
    <th class="invoice">AMOUNT</th>
    <th class="invoice">SHPAMT</th>
  </tr>
<c:forEach var="lineItem" items="${lineItems}" varStatus="status">
<tr valign="top">
  <td class="invoice" align="center"><c:out value="${status.count}"/></td>
  <td class="invoice"><c:out value="${lineItem['ITEM_ID']}"/></td> 
  <td class="invoice"><c:out value="${lineItem['UPC']}"/></td>
  <td class="invoice"><c:out value="${lineItem['ITEM_NAME']}"/></td>
  <td class="invoice" align="center"><c:out value="${lineItem['QTY']}"/></td>
  <td class="invoice" align="right"><fmt:message key="${siteConfig['CURRENCY'].value}" /><fmt:formatNumber value="${lineItem['U_PRICE']}" pattern="#,##0.00"/></td>
  <td class="invoice" align="right"><fmt:message key="${siteConfig['CURRENCY'].value}" /><fmt:formatNumber value="${lineItem['U_PRICE']*lineItem['QTY']}" pattern="#,##0.00"/></td>  
  <td class="invoice" align="right"><fmt:message key="${siteConfig['CURRENCY'].value}" /><fmt:formatNumber value="${lineItem['LINETOTAL_SHIPPED']}" pattern="#,##0.00"/></td>
</tr>
</c:forEach>
  <tr>
    <td class="invoice" colspan="6" align="right"><fmt:message key="subTotal" />:</td>
    <td class="invoice" align="right"><fmt:message key="${siteConfig['CURRENCY'].value}" /><fmt:formatNumber value="${order['BOOK_AMT']}" pattern="#,##0.00"/></td>
    <td class="invoice" align="right"><fmt:message key="${siteConfig['CURRENCY'].value}" /><fmt:formatNumber value="${order['SHIP_AMT']}" pattern="#,##0.00"/></td>
  </tr>
  <tr bgcolor="#BBBBBB">
	<td colspan="8">&nbsp;</td>
  </tr>
  <tr>
    <td class="invoice" colspan="7" align="right">Shipping:</td>
    <td class="invoice" align="right"><fmt:message key="${siteConfig['CURRENCY'].value}" /><fmt:formatNumber value="${order['SHIPPING']}" pattern="#,##0.00"/></td>
  </tr>
  <tr>
    <td class="invoice" colspan="7" align="right">Handling:</td>
    <td class="invoice" align="right"><fmt:message key="${siteConfig['CURRENCY'].value}" /><fmt:formatNumber value="${order['HANDLING']}" pattern="#,##0.00"/></td>
  </tr>
  <tr>
    <td class="invoice" colspan="7" align="right">Discount Total:</td>
    <td class="invoice" align="right"><fmt:message key="${siteConfig['CURRENCY'].value}" /><fmt:formatNumber value="${order['DISCOUNT_TOTAL']}" pattern="#,##0.00"/></td>
  </tr>
  <tr>
    <td class="invoice" colspan="7" align="right">Sub Total:</td>
    <td class="invoice" align="right"><fmt:message key="${siteConfig['CURRENCY'].value}" /><fmt:formatNumber value="${order['SUBTOTAL']}" pattern="#,##0.00"/></td>
  </tr>
  <tr>
    <td class="invoice" colspan="7" align="right">Credit Payment:</td>
    <td class="invoice" align="right"><fmt:message key="${siteConfig['CURRENCY'].value}" /><fmt:formatNumber value="${order['PAY_AMT']}" pattern="#,##0.00"/></td>
  </tr>
  <tr>
    <td class="invoice" colspan="7" align="right">COD Charge:</td>
    <td class="invoice" align="right"><fmt:message key="${siteConfig['CURRENCY'].value}" /><fmt:formatNumber value="${order['COD_CHARGE']}" pattern="#,##0.00"/></td>
  </tr>
</table>
<table>
  <tr>
    <td>
    <b><fmt:message key="paymentMethod" />:</b> <c:out value="${order['PAY_METHOD']}" />
    </td>
  </tr>
</table>
<table>
  <tr>
    <td valign="top"><b><fmt:message key="trackNum"/>:</b></td>
    <td valign="top"><table>
        <tr><td>
          <a href="http://wwwapps.ups.com/WebTracking/processInputRequest?HTMLVersion=5.0&sort_by=status&tracknums_displayed=10&TypeOfInquiryNumber=T&loc=en_US&InquiryNumber1=<c:out value="${order['SHIPMENT_NO']}" />&AgreeToTermsAndConditions=yes"><c:out value="${order['SHIPMENT_NO']}" /></a>
        </td></tr>
    </table></td>
  </tr>
</table>

<c:out value="${invoiceLayout.footerHtml}" escapeXml="false"/>
</c:if>

</div>

</body>
</html>