<%@ page import="java.io.*,java.util.*,java.awt.image.BufferedImage,javax.imageio.ImageIO,com.webjaguar.thirdparty.evergreen.ClaimHeader" %>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="/WEB-INF/tlds/spring.tld" prefix="spring" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>

<tiles:insertDefinition name="${_template}" flush="true">
  <tiles:putAttribute name="content" type="string">

<c:if test="${model.claimHeader == null}">
  <div class="message">Missing Claim Form.</div>
</c:if>

<c:if test="${model.claimHeader != null}">

<h3>&nbsp;&nbsp;Claim Form Complete - Credit Memo # ${model.claimHeader.aeClaimID}</h3>

<fieldset class="register_fieldset">
<legend>Your Order Claim</legend>
<table border="0" cellpadding="3" cellspacing="1" width="100%">
  <tr>
    <td colspan="2">Your order claim report is complete. You can view this record any time by visiting "<a href="evergreen_myCreditsDamagedReportsList.jhtm">My Claims &amp; Credits Report</a>" under <a href="account.jhtm">My Account</a>.</td>
  </tr>
  <tr>
    <td colspan="2">&nbsp;</td>
  </tr>
  <tr>
    <td align="right">Contact Name:</td>
    <td><c:out value="${model.claimHeader.contactName}"/></td>
  </tr>
  <tr>
    <td align="right">Best Phone Number:</td>
    <td><c:out value="${model.claimHeader.contactPhone}"/></td>
  </tr>
  <tr>
    <td align="right">Order Number:</td>
    <td><c:out value="${model.claimHeader.orderID}"/></td>
  </tr>
  <tr>
    <td valign="top" align="right">Damaged SKU's:</td>
    <td>
      <table width="95%" border="0" cellspacing="0" cellpadding="2">
        <tr bgcolor="#999999">
          <td align="center"><b>SKU's</b></td>
          <td align="center"><b>Quantity</b></td>
          <td align="center"><b>Issue</b></td>
        </tr>
        <c:forEach var="claimDetail" items="${model.claimDetails}">
        <tr>
          <td align="center"><c:out value="${claimDetail.claimSku}"/></td>
          <td align="center"><c:out value="${claimDetail.claimQty}"/></td>
          <td align="center"><c:out value="${claimDetail.claimType}"/></td>          
        </tr>
 		</c:forEach>
      </table></td>
  </tr>
  <tr>
    <td valign="top" align="right">Issue:</td>
    <td><c:out value="${model.claimHeader.claimDesc}"/></td>
  </tr>
<%-- Spring3 Move this Controller
Map<String, Object> model = (HashMap<String, Object>) request.getAttribute("model");
ClaimHeader claimHeader = (ClaimHeader) model.get("claimHeader");
File claimFolder = new File(getServletContext().getRealPath("/assets/claim_folder"));
Properties prop = new Properties();
try {
	prop.load(new FileInputStream(getServletContext().getRealPath("WEB-INF/properties/site.properties")));
	if (prop.get("site.root") != null) {
		claimFolder = new File((String) prop.get("site.root"), "/assets/claim_folder");
	}
} catch (Exception e) {}
File claimImage = new File(claimFolder, claimHeader.getAeClaimID() + ".jpg");
if (claimImage.exists()) {
	BufferedImage img = ImageIO.read(claimImage);
	pageContext.setAttribute("imageWidth", img.getWidth());
--%>
  <tr>
    <td valign="top" align="right">Picture of damaged Goods:</td>
    <c:if test="${imageWidth > 200}">
    <td><a href="#" onClick="window.open('assets/claim_folder/<c:out value="${model.claimHeader.aeClaimID}"/>.jpg','name','width=800,height=600,resizable=yes,scrollbars=1')"><img src="assets/claim_folder/<c:out value="${model.claimHeader.aeClaimID}"/>.jpg" width="200"/></a></td>
    </c:if>
    <c:if test="${imageWidth <= 200}">
    <td><img src="assets/claim_folder/<c:out value="${model.claimHeader.aeClaimID}"/>.jpg"/></td>
    </c:if>
  </tr>
<%-- Spring3 Move this Controller } --%>  
</table>
</fieldset>

</c:if>

  </tiles:putAttribute>
</tiles:insertDefinition>