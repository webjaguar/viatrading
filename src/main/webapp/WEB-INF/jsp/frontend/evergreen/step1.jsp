<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="/WEB-INF/tlds/spring.tld" prefix="spring" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>

<tiles:insertDefinition name="${_template}" flush="true">
  <tiles:putAttribute name="content" type="string">
<c:if test="${!gSiteConfig['gREGISTRATION_DISABLED']}">

<spring:hasBindErrors name="customerForm">
<span class="error">Please fix all errors!</span>
<%-- 
<c:forEach items="${errors.allErrors}" var="error">
<li>
<spring:message message="${error}"/>
</li>
</c:forEach>
--%>
</spring:hasBindErrors>

<form:form commandName="customerForm" action="register.jhtm" method="post">
<input type="hidden" name="_page" value="0">
<spring:bind path="customerForm.forwardAction">
<input type="hidden" name="<c:out value="${status.expression}"/>" value="<c:out value="${status.value}"/>"/>
</spring:bind>
<fieldset class="register_fieldset">
<legend><fmt:message key="emailAddressAndPassword"/></legend><div class="requiredFieldRight" align="right">* required field</div>
<table border="0" cellpadding="3" cellspacing="1">
  <tr>
    <td>&nbsp;</td>
    <td><fmt:message key="emailAddress" />:</td>
    <td>
      <c:out value="${customerForm.customer.username}"/>
      <form:errors path="customer.username" cssClass="error" />
    </td>
  </tr>
  <tr>
    <td><div class="requiredField"> * </div></td>
    <td><fmt:message key="password" />:</td>
    <td>
      <spring:bind path="customerForm.customer.password">
      <input type="password" name="<c:out value="${status.expression}"/>" value="<c:out value="${status.value}"/>"/>
      <span class="error"><c:out value="${status.errorMessage}"/></span>
      </spring:bind>
    </td>
  </tr>
  <tr>
    <td><div class="requiredField"> * </div></td>
    <td><fmt:message key="confirmPassword" />:</td>
    <td>
      <spring:bind path="customerForm.confirmPassword">
      <input type="password" name="<c:out value="${status.expression}"/>" value="<c:out value="${status.value}"/>"/>
      <span class="error"><c:out value="${status.errorMessage}"/></span>
      </spring:bind>
    </td>
  </tr>
</table>
</fieldset>
<fieldset class="register_fieldset">
<legend>Account Information</legend>
<table border="0" cellpadding="3" cellspacing="1">
  <tr>
    <td>&nbsp;</td>
    <td><fmt:message key="accountNumber" />:</td>
    <td><c:out value="${customerForm.customer.accountNumber}"/></td>
  </tr>
  <tr>
    <td>&nbsp;</td>
    <td><fmt:message key="company" />:</td>
    <td>
      <c:out value="${customerForm.customer.address.company}"/>
      <form:errors path="customer.address.company" cssClass="error" />
    </td>
  </tr>
  <tr>
    <td>&nbsp;</td>
    <td><fmt:message key="firstName" />:</td>
    <td>
	  <c:out value="${customerForm.customer.address.firstName}"/>
	  <form:errors path="customer.address.firstName" cssClass="error" />
    </td>
  </tr>
  <tr>
    <td><div class="requiredField"> * </div></td>
    <td><fmt:message key="lastName" />:</td>
    <td>
	  <form:input path="customer.address.lastName" htmlEscape="true" />
	  <form:errors path="customer.address.lastName" cssClass="error" />
    </td>
  </tr>
</table>
</fieldset>

<div align="center">
<c:choose>
  <c:when test="${customerForm.customerFields != null}">
    <input type="submit" value="<fmt:message key="nextStep" />" name="_target1">
  </c:when>
  <c:otherwise>
    <input type="submit" value="<fmt:message key="nextStep" />" name="_target2">
  </c:otherwise>
</c:choose>
<%--
  <input type="submit" value="Register" name="_finish">
--%>
</div>
</form:form>

</c:if>
  </tiles:putAttribute>
</tiles:insertDefinition>