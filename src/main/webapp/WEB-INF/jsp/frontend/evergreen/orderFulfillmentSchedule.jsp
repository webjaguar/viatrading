<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>

<tiles:insertDefinition name="${_template}" flush="true">
  <tiles:putAttribute name="content" type="string">

<c:out value="${model.evergreenLayout.headerHtml}" escapeXml="false"/>

<table border="0" cellpadding="3" cellspacing="1" width="100%" class="listings"> 
  <tr class="listingsHdr2">
    <td align="center">Invoice#</td>
    <td align="center">Today's Fill Rate</td>
    <td align="center">15 Day Fill Rate</td>
    <td align="center">30 Day Fill Rate</td>
    <td align="center">60 Day Fill Rate</td>
    <td align="center">Over 60 Days Fill Rate</td>
    <td align="center">Complete Details</td>
  </tr>
<c:forEach items="${model.list}" var="map" varStatus="status">
  <tr class="row${status.index % 2}">
    <td align="center"><a class="nameLink" href="evergreen_order.jhtm?id=${map['EveOrderID']}"><c:out value="${map['EveOrderID']}"/></a></td>
    <td align="center"><c:out value="${map['RateToday']}"/>%</td>
    <td align="center"><c:out value="${map['RateIn15Day']}"/>%</td>
    <td align="center"><c:out value="${map['RateIn30Day']}"/>%</td>
    <td align="center"><c:out value="${map['RateIn60Day']}"/>%</td>
    <td align="center"><c:out value="${map['RateOver60']}"/>%</td>
    <td align="center"><a class="nameLink" href="evergreen_orderFulfillmentDetails.jhtm?id=${map['EveOrderID']}">CLICK HERE</a></td>
  </tr>
</c:forEach>
<c:if test="${empty model.list}">
  <tr class="emptyList">
    <td colspan="7">&nbsp;</td>
  </tr>
</c:if>
</table>

<c:out value="${model.evergreenLayout.footerHtml}" escapeXml="false"/>
    
  </tiles:putAttribute>
</tiles:insertDefinition>