<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="/WEB-INF/tlds/spring.tld" prefix="spring" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>

<tiles:insertDefinition name="${_template}" flush="true">
  <tiles:putAttribute name="content" type="string">

<c:if test="${gSiteConfig['gACCOUNTING'] == 'EVERGREEN'}"> 

<form:form commandName="claimForm" action="evergreen_claimForm.jhtm" method="post">
<input type="hidden" name="_page" value="0">

<div class="requiredFieldRight" align="right">* required field</div>
<h3>&nbsp;&nbsp;Report a Problem with Order</h3>

<fieldset class="register_fieldset">
<legend>Your Order Claim Form </legend>
<table border="0" cellpadding="3" cellspacing="1" width="100%" class="clainForm">
  <tr>
    <td align="right"><span style="color: #FF0000;">*</span> Invoice Number:</td>
    <td>
      <form:select path="claimHeader.orderID" items="${claimForm.orders}" />
      <form:errors path="claimHeader.orderID" cssClass="error" />
    </td>
  </tr>
</table>
</fieldset>

<div align="center">
  <input type="submit" value="<fmt:message key="nextStep" />" name="_target1">
</div>

</form:form>

</c:if>

  </tiles:putAttribute>
</tiles:insertDefinition>