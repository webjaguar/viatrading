<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="/WEB-INF/tlds/spring.tld" prefix="spring" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>

<tiles:insertDefinition name="${_template}" flush="true">
  <tiles:putAttribute name="content" type="string">

<form:form commandName="claimForm" action="evergreen_claimForm.jhtm" method="post" enctype="multipart/form-data">
<input type="hidden" name="_page" value="1">

<div class="requiredFieldRight" align="right">* required field</div>
<h3>&nbsp;&nbsp;Report a Problem with Order</h3>

<fieldset class="register_fieldset">
<legend>Your Order Claim Form </legend>
<table border="0" cellpadding="3" cellspacing="1" width="100%" class="clainForm">
  <c:if test="${webServiceError}">
  <tr>
    <td align="center" colspan="2" class="error"><b>There was an issue sending your request. Please try again later<br/>or contact your Sales Rep for help.</b></td>
  </tr>
  </c:if>
  <tr>
    <td align="right"><span style="color: #FF0000;">*</span> Contact Name:</td>
    <td><form:input path="claimHeader.contactName" maxlength="255" />
		<form:errors path="claimHeader.contactName" cssClass="error"/></td>
  </tr>
  <tr>
    <td align="right"><span style="color: #FF0000;">*</span> Best Phone Number:</td>
    <td><form:input path="claimHeader.contactPhone" maxlength="50" />
		<form:errors path="claimHeader.contactPhone" cssClass="error"/></td>
  </tr>
  <tr>
    <td align="right">Invoice Number:</td>
    <td><c:out value="${claimForm.claimHeader.orderID}"/></td>
  </tr>
  <tr>
    <td valign="top" align="right"><span style="color: #FF0000;">*</span> Identify SKU's affected:</td>
    <td>
      <table width="95%" border="0" cellspacing="0" cellpadding="2">
        <tr bgcolor="#999999">
          <td align="center"><b>SKU's Ordered</b></td>
          <td align="center"><b>Quantity Shipped</b></td>
          <td align="center"><b>Quantity Affected</b></td>
          <td align="center"><b>Issue Type</b></td>
        </tr>
        <c:if test="${emptyClaim}">
        <tr>
          <td align="center" colspan="4" class="error"><b>Please identify the SKU's affected.</b></td>
        </tr>
        </c:if>
        <c:forEach var="claimDetail" items="${claimForm.claimDetails}">
        <tr>
          <td align="center"><c:out value="${claimDetail.claimSku}"/></td>
          <td align="center"><c:out value="${claimDetail.qty}"/></td>
          <td <c:if test="${claimDetailErrors[claimDetail] == 'claimQty'}">style="background:red;"</c:if>>
            <input type="text" name="claimQty_${claimDetail.claimSku}" value="<c:out value="${claimDetail.claimQty}"/>" type="text" size="8" maxlength="5">
          </td>
          <td <c:if test="${claimDetailErrors[claimDetail] == 'claimType'}">style="background:red;"</c:if>><select name="claimType_${claimDetail.claimSku}">
            <option value="">Please Select</option>
            <option <c:if test="${claimDetail.claimType == 'Damaged'}">selected</c:if>>Damaged</option>
            <option <c:if test="${claimDetail.claimType == 'Missing'}">selected</c:if>>Missing</option>
            <option <c:if test="${claimDetail.claimType == 'Wrong Item'}">selected</c:if>>Wrong Item</option>
            <option <c:if test="${claimDetail.claimType == 'Overage'}">selected</c:if>>Overage</option>
            <option <c:if test="${claimDetail.claimType == 'Defective'}">selected</c:if>>Defective</option>
          </select></td>
        </tr>
 		</c:forEach>
      </table></td>
  </tr>
  <tr>
    <td valign="top" align="right"><span style="color: #FF0000;">*</span> Explain Issue:</td>
    <td><form:textarea rows="8" cols="30" path="claimHeader.claimDesc" htmlEscape="true" />
	    <form:errors path="claimHeader.claimDesc" cssClass="error" /></td>
  </tr>
  <tr>
    <td align="right">Upload Photos of damaged goods <br>
      (if any):</td>
    <td><input type="file" name="file"></td>
  </tr>
</table>
</fieldset>

<div align="center">
  <input type="submit" value="<fmt:message key="submit" />" name="_finish" <c:if test="${webServiceError}">disabled="true"</c:if>>
  <input type="submit" value="<fmt:message key="change" />" name="_target0">
</div>

</form:form>

  </tiles:putAttribute>
</tiles:insertDefinition>