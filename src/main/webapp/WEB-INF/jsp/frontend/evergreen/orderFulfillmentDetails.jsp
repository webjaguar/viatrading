<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>

<tiles:insertDefinition name="${_template}" flush="true">
  <tiles:putAttribute name="content" type="string">

<c:out value="${model.evergreenLayout.headerHtml}" escapeXml="false"/>

<div><b>Order #: <c:out value="${param.id}"/></b></div>
<table border="0" cellpadding="3" cellspacing="1" width="100%" class="listings"> 
  <tr class="listingsHdr2">
    <td align="center" rowspan="2">SKUs</td>
    <td align="center" colspan="5">Available</td>
    <td align="center" rowspan="2">Quantity Ordered</td>
    <td align="center" rowspan="2">Quantity that will be Filled</td>
    <td align="center" rowspan="2">Cannot Fill</td>
  </tr>
  <tr class="listingsHdr2">
    <td align="center">Today</td>
    <td align="center">In 15 Days</td>
    <td align="center">In 30 Days</td>
    <td align="center">In 60 Days</td>
    <td align="center">After 60 Days</td>
  </tr>
  <c:set var="totalBookQty" value="0"/>
  <c:set var="totalQtyWill" value="0"/>
<c:forEach items="${model.list}" var="map" varStatus="status">
  <c:set var="qtyWill" value="${map['AvQtyToday']+map['AvIn15Day']+map['AvIn30Day']+map['AvIn60Day']+map['AvAfter60Day']}"/>
  <c:set var="totalBookQty" value="${totalBookQty+map['BookQty']}"/>
  <c:set var="totalQtyWill" value="${totalQtyWill+qtyWill}"/>
  <c:set var="qtyCannot" value="${map['BookQty']-qtyWill}"/>
  <tr class="row${status.index % 2}">
    <td align="center"><a href="assets/Image/Product/thumb/<c:out value="${map['ItemID']}"/>.jpg" id="mb0" class="mb nameLink"><c:out value="${map['ItemID']}"/></a></td>
    <td align="center"><c:if test="${map['AvQtyToday'] != '0'}"><c:out value="${map['AvQtyToday']}"/></c:if></td>
    <td align="center"><c:if test="${map['AvIn15Day'] != '0'}"><c:out value="${map['AvIn15Day']}"/></c:if></td>
    <td align="center"><c:if test="${map['AvIn30Day'] != '0'}"><c:out value="${map['AvIn30Day']}"/></c:if></td>
    <td align="center"><c:if test="${map['AvIn60Day'] != '0'}"><c:out value="${map['AvIn60Day']}"/></c:if></td>
    <td align="center"><c:if test="${map['AvAfter60Day'] != '0'}"><c:out value="${map['AvAfter60Day']}"/></c:if></td>
    <td align="center"><c:if test="${map['BookQty'] != '0'}"><c:out value="${map['BookQty']}"/></c:if></td>
    <td align="center"><c:out value="${qtyWill}"/></td>
    <td align="center"><c:if test="${qtyCannot > '0'}"><c:out value="${qtyCannot}"/></c:if></td>
  </tr>
</c:forEach>
<c:if test="${empty model.list}">
  <tr class="emptyList">
    <td colspan="9">&nbsp;</td>
  </tr>
</c:if>
<c:if test="${not empty model.list}">
  <tr class="listingsHdr2">
    <td align="center">Fill Rate</td>
    <td align="center"><c:out value="${model.map['RateToday']}"/>%</td>
    <td align="center"><c:out value="${model.map['RateIn15Day']}"/>%</td>
    <td align="center"><c:out value="${model.map['RateIn30Day']}"/>%</td>
    <td align="center"><c:out value="${model.map['RateIn60Day']}"/>%</td>
    <td align="center"><c:out value="${model.map['RateOver60']}"/>%</td>
    <td align="center"><c:out value="${totalBookQty}"/></td>
    <td align="center"><c:if test="${totalBookQty>0}"><fmt:formatNumber value="${totalQtyWill/totalBookQty*100}" pattern="#,##0"/>%</c:if></td>
    <td align="center"><c:if test="${totalBookQty>0}"><fmt:formatNumber value="${100-(totalQtyWill/totalBookQty*100)}" pattern="#,##0"/>%</c:if></td>
  </tr>
</c:if>
</table>

<c:out value="${model.evergreenLayout.footerHtml}" escapeXml="false"/>
    
  </tiles:putAttribute>
</tiles:insertDefinition>