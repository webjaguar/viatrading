<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>

<c:if test="${model.evergreenClientCredit != null}">

<fieldset class="myaccount_fieldset">
<legend class="myaccount_legend">Evergreen Credit</legend>
<table width="100%" class="creditForm">
<tr>
<td width="50%" valign="top">
<table border="0" cellpadding="1" cellspacing="1"> 
  <tr>
    <td align="right"><b>Client ID: </b></td>
    <td><c:out value="${model.evergreenClientCredit['EveClientID']}" /></td>
  </tr>
  <tr>
    <td align="right"><b>Credit Limit: </b></td>
    <td><c:if test="${model.evergreenClientCredit['CreditLimit'] != null && model.evergreenClientCredit['CreditLimit'] != ''}"><fmt:message key="${siteConfig['CURRENCY'].value}" /><fmt:formatNumber value="${model.evergreenClientCredit['CreditLimit']}" pattern="#,##0.00" /></c:if></td>
  </tr>
  <tr><td>&nbsp;</td></tr>
  <tr>
    <td align="right"><b>Current Balance: </b></td>
    <td align="right"><fmt:formatNumber value="${model.evergreenClientCredit['CurrentBalance']}" pattern="#,##0.00" /></td>
  </tr>
  <tr>
    <td align="right"><b>Past Due Balance: </b></td>
    <td style="border-bottom: #919191 double;" align="right"><fmt:formatNumber value="${model.evergreenClientCredit['TotalBalance'] - model.evergreenClientCredit['CurrentBalance']}" pattern="#,##0.00" /></td>
  </tr>
  <tr>
    <td align="right"><b>TOTAL BALANCE: </b></td>
    <td align="right"><fmt:message key="${siteConfig['CURRENCY'].value}" /><fmt:formatNumber value="${model.evergreenClientCredit['TotalBalance']}" pattern="#,##0.00" /></td>
  </tr>
</table>
</td>
<td width="50%">
<table border="0" cellpadding="1" cellspacing="1">
  <tr>
    <td colspan="2"><b>PAST DUE</b></td>
  </tr> 
  <tr>
    <td width="100" align="right" style="white-space: nowrap">1-15 days: </td>
    <td width="50%"><fmt:message key="${siteConfig['CURRENCY'].value}" /><fmt:formatNumber value="${model.evergreenClientCredit['Pastdue1_15']}" pattern="#,##0.00" /></td>
  </tr>
  <tr>
    <td align="right" style="white-space: nowrap">16-30 days: </td>
    <td><fmt:message key="${siteConfig['CURRENCY'].value}" /><fmt:formatNumber value="${model.evergreenClientCredit['Pastdue16_30']}" pattern="#,##0.00" /></td>
  </tr>
  <tr>
    <td align="right" style="white-space: nowrap">31-60 days: </td>
    <td><fmt:message key="${siteConfig['CURRENCY'].value}" /><fmt:formatNumber value="${model.evergreenClientCredit['Pastdue31_45']+model.evergreenClientCredit['Pastdue46_60']}" pattern="#,##0.00" /></td>
  </tr>
  <tr>
    <td align="right" style="white-space: nowrap">61-90 days: </td>
    <td><fmt:message key="${siteConfig['CURRENCY'].value}" /><fmt:formatNumber value="${model.evergreenClientCredit['Pastdue61_75']+model.evergreenClientCredit['Pastdue76_90']}" pattern="#,##0.00" /></td>
  </tr>
  <tr>
    <td align="right" style="white-space: nowrap">91-120 days: </td>
    <td><fmt:message key="${siteConfig['CURRENCY'].value}" /><fmt:formatNumber value="${model.evergreenClientCredit['Pastdue91_105']+model.evergreenClientCredit['Pastdue105_120']}" pattern="#,##0.00" /></td>
  </tr>
  <tr>
    <td align="right" style="white-space: nowrap">Over 120 days: </td>
    <td><fmt:message key="${siteConfig['CURRENCY'].value}" /><fmt:formatNumber value="${model.evergreenClientCredit['PastdueOver_120']}" pattern="#,##0.00" /></td>
  </tr>
</table>
</td>
</tr>
</table>
</fieldset>

</c:if>