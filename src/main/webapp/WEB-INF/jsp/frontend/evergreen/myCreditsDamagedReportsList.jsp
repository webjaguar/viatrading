<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>

<tiles:insertDefinition name="${_template}" flush="true">
  <tiles:putAttribute name="content" type="string">

<c:out value="${model.evergreenLayout.headerHtml}" escapeXml="false"/>

<table border="0" cellpadding="3" cellspacing="1" width="100%" class="listings"> 
  <tr class="listingsHdr2">
    <td align="center">Web Claim #</td>
    <td align="center">Evergreen Claim #</td>
    <td align="center">Invoice #</td>
    <td align="center">Delivery Date</td>
    <td align="center">PO #</td>
    <td align="center">Status</td>
    <td align="center">Booked Total</td>
    <td align="center">Shipped Total</td>
    <td align="center">Amount Paid</td>
    <td align="center">Credit Total</td>
    <td align="center">Total Due</td>
  </tr>
<c:forEach items="${model.list}" var="map" varStatus="status">
  <tr class="row${status.index % 2}">
    <td align="center"><a class="nameLink" href="evergreen_claim.jhtm?id=${map['AECLAIMID']}"><c:out value="${map['AECLAIMID']}"/></a></td>
    <td align="center"><a class="nameLink" href="evergreen_myCreditsDamagedReport.jhtm?id=${map['EVECLAIMID']}"><c:out value="${map['EVECLAIMID']}"/></a></td>
	<c:if test="${empty map['EVECLAIMID']}">
    <td align="center"><c:out value="${map['ORDERID']}"/></td>
	</c:if>    
	<c:if test="${not empty map['EVECLAIMID']}">
    <td align="center"><a class="nameLink" href="evergreen_myCreditsDamagedReport.jhtm?id=${map['EVECLAIMID']}"><c:out value="${map['ORDERID']}"/></a></td>
	</c:if>        
    <td align="center"><fmt:formatDate type="date" value="${map['order']['REL_D_DATE']}" pattern="MM/dd/yy"/></td>
    <td align="center"><c:out value="${map['order']['PONUM']}"/></td>
    <td align="center"><c:out value="${map['order']['STATUS']}"/></td>
    <td align="right"><fmt:message key="${siteConfig['CURRENCY'].value}" /><fmt:formatNumber value="${map['order']['BOOK_AMT']}" pattern="#,##0.00" /></td>
    <td align="right"><fmt:message key="${siteConfig['CURRENCY'].value}" /><fmt:formatNumber value="${map['order']['SHIP_AMT']}" pattern="#,##0.00" /></td>
    <td align="center"><fmt:message key="${siteConfig['CURRENCY'].value}" /><fmt:formatNumber value="${map['order']['PAY_AMT']}" pattern="#,##0.00" /></td>
    <td align="center" style="color:red;"><c:if test="${not empty map['CREDITAMT']}">(<fmt:message key="${siteConfig['CURRENCY'].value}" /><fmt:formatNumber value="${map['CREDITAMT']}" pattern="#,##0.00" />)</c:if></td>
    <td align="center"><fmt:message key="${siteConfig['CURRENCY'].value}" /><fmt:formatNumber value="${map['order']['DUE_AMT']}" pattern="#,##0.00" /></td>
  </tr>
</c:forEach>
<c:if test="${empty model.list}">
  <tr class="emptyList">
    <td colspan="11">&nbsp;</td>
  </tr>
</c:if>
</table>

<c:out value="${model.evergreenLayout.footerHtml}" escapeXml="false"/>
    
  </tiles:putAttribute>
</tiles:insertDefinition>
