<%@ page import="java.util.*" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
  <head>
    <title>Invoice</title>
    <link href="assets/invoice.css" rel="stylesheet" type="text/css"/>
  </head>  
<body class="invoice">
<div style="width:700px;margin:auto;">

<c:if test="${model.order != null}">
<c:out value="${model.evergreenLayout.headerHtml}" escapeXml="false"/>

<table border="0" width="100%" cellspacing="3" cellpadding="1" class="orderDetails">
<tr>
  <td><div class="infoHeader">Client Info:</div>
    <table border="0" cellpadding="0" cellspacing="0" class="info">
		<tr><td>Client#: <c:out value="${model.client['WHOLE_S_ID']}"/></td></tr>
		<tr><td>Business Name:<c:out value="${model.client['CLIENTNAME']}"/></td></tr>
		<tr><td>Client Name: <c:out value="${model.client['CLIENT_POC']}"/></td></tr>
		<tr><td>Phone#: <c:out value="${model.client['TELEPHONE']}"/></td></tr>
		<tr><td>Fax#: <c:out value="${model.client['FAX_NUMBER']}"/></td></tr>
		<tr><td>Email: <c:out value="${model.client['EMAIL_ADDR']}"/></td></tr>
	</table>
  </td>
  <td><div class="infoHeader">Order Info:</div>
    <table border="0" cellpadding="0" cellspacing="0" class="info">
		<tr><td>Order#: <c:out value="${model.order['ORDER_ID']}"/></td></tr>
		<tr><td>Order Date: <fmt:formatDate type="date" value="${model.order['ORDER_DATE']}" pattern="MM/dd/yyyy"/></td></tr>
		<tr><td>Ship Date: <fmt:formatDate type="date" value="${model.order['SHIP_DATE']}" pattern="MM/dd/yyyy"/></td></tr>
		<tr><td>PO#: <c:out value="${model.order['PONUM']}"/></td></tr>
		<tr><td>Payment Method: <c:out value="${model.order['PAY_METHOD']}" /></td></tr>
		<tr><td>Order Source: <c:out value="${model.order['ORDER_SOURCE']}"/></td></tr>
		<tr><td>Rep#: <c:out value="${model.order['REP_ID1']}"/></td></tr>
	</table>
  </td>
  <td><div class="infoHeader">Shipping Info:</div>
    <table border="0" cellpadding="0" cellspacing="0" class="info">
		<tr><td>Ship Method: <c:out value="${model.order['CR_SHIPING_COMPANY']}"/></td></tr>
		<tr><td>Carrier: <c:out value="${model.order['CR_CARRIER']}"/></td></tr>
		<tr><td>Tracking#: <c:out value="${model.order['CR_TRACKING_NUMBER']}"/></td></tr>
	</table>
  </td>
</tr>

<tr valign="top">
<td><div class="infoHeader">Billing Address To:</div>
	<table border="0" cellpadding="0" cellspacing="0" class="info">
	<tr><td><c:out value="${model.client['B_STREET1']}"/></td></tr>
	<c:if test="${model.client['B_STREET2'] != ''}">
	<tr><td><c:out value="${model.client['B_STREET1']}"/></td></tr>
	</c:if>
	<tr><td><c:out value="${model.client['B_CITY']}"/>, <c:out value="${model.client['B_STATE']}"/> <c:out value="${model.client['B_ZIP']}"/></td></tr>
	</table>
</td>
<td>&nbsp;</td>
<td><div class="infoHeader">Shipping Address:</div>
	<table border="0" cellpadding="0" cellspacing="0" class="info">
	<tr><td><c:out value="${model.order['S_STREET1']}"/></td></tr>
	<c:if test="${model.order['S_STREET2'] != ''}">
	<tr><td><c:out value="${model.order['S_STREET2']}"/></td></tr>
	</c:if>
	<tr><td><c:out value="${model.order['S_CITY']}"/>, <c:out value="${model.order['S_STATE']}"/> <c:out value="${model.order['S_ZIP']}"/></td></tr>
	</table>
</td>
<td>&nbsp;</td>
</tr>
</table>
<%-- 
<div class="claim_div">
<b>Claim ID#:</b> <c:out value="${model.claim['EVECLAIMID']}"/><br/>
<b>Date Reported:</b> <fmt:formatDate type="date" value="${model.claim['CLAIMDATE']}" pattern="MM/dd/yy"/><br/>
<b>Reported by:</b> <c:out value="${model.claim['CONTACTNAME']}"/><br/>
<b>Report Claim:</b> <c:out value="${model.claim['CLAIMDESC']}"/>
</div>
--%>
<table border="0" cellpadding="2" cellspacing="1" width="100%" class="invoice">
  <tr>
    <th class="invoice">ITEM #</th>
    <th class="invoice">DESCRIPTION</th>
    <th class="invoice">UNIT PRICE</th>
    <th width="10%" class="invoice">BOOK QTY</th>
    <th width="10%" class="invoice">SHIPPED QTY</th>
    <th class="invoice">BOOK AMT</th>
    <th class="invoice">SHIPPED AMT</th>
  </tr>
<c:forEach var="lineItem" items="${model.lineItems}" varStatus="status">
<tr valign="top">
  <td class="invoice"><c:out value="${lineItem['ITEM_ID']}"/></td> 
  <td class="invoice"><c:out value="${lineItem['ITEM_NAME']}"/></td>
  <td class="invoice" align="right"><fmt:message key="${siteConfig['CURRENCY'].value}" /><fmt:formatNumber value="${lineItem['U_PRICE']}" pattern="#,##0.00"/></td>
  <td class="invoice" align="center"><c:out value="${lineItem['QTY']}"/></td>
  <td class="invoice" align="center"><c:out value="${lineItem['SHIPPED']}"/></td>
  <td class="invoice" align="right"><fmt:message key="${siteConfig['CURRENCY'].value}" /><fmt:formatNumber value="${lineItem['U_PRICE']*lineItem['QTY']}" pattern="#,##0.00"/></td>  
  <td class="invoice" align="right"><fmt:message key="${siteConfig['CURRENCY'].value}" /><fmt:formatNumber value="${lineItem['LINETOTAL_SHIPPED']}" pattern="#,##0.00"/></td>
</tr>
</c:forEach>
  <tr>
    <td class="invoice" colspan="5" align="right"><fmt:message key="subTotal" />:</td>
    <td class="invoice" align="right"><fmt:message key="${siteConfig['CURRENCY'].value}" /><fmt:formatNumber value="${model.order['BOOK_AMT']}" pattern="#,##0.00"/></td>
    <td class="invoice" align="right"><fmt:message key="${siteConfig['CURRENCY'].value}" /><fmt:formatNumber value="${model.order['SHIP_AMT']}" pattern="#,##0.00"/></td>
  </tr>
  <tr bgcolor="#BBBBBB">
	<td colspan="7">&nbsp;</td>
  </tr>
</table>
  
<table border="0" cellpadding="2" cellspacing="1" width="100%" class="invoice">  
  <tr><td class="invoice" colspan="9" style="margin:16px 1px;"><b>Claims</b></td></tr>
  <tr>
  	<th class="invoice">ITEM #</th>
    <th class="invoice">DESCRIPTION</th>
    <th class="invoice">UPC</th>
    <th class="invoice">UNIT PRICE</th>
    <th width="10%" class="invoice">CLIAM QTY</th>
    <th width="10%" class="invoice">CHECKIN QTY</th>
    <th class="invoice">RETURN REASON</th>
    <th class="invoice">RETURN STATUS</th>    
  </tr>
  <c:forEach var="lineItem" items="${model.lineItems}" varStatus="status">
  <c:if test="${model.details[fn:toUpperCase(lineItem['ITEM_ID'])] != null}">
  <tr>
	  <td class="invoice" style="background:#FFFFCC;"><c:out value="${lineItem['ITEM_ID']}"/></td>
	  <td class="invoice" style="background:#FFFFCC;"><c:out value="${lineItem['ITEM_NAME']}"/></td>
	  <td class="invoice" style="background:#FFFFCC;"><c:out value="${lineItem['UPC']}"/></td>
	  <td class="invoice" style="background:#FFFFCC;text-align:right"><fmt:message key="${siteConfig['CURRENCY'].value}" /><fmt:formatNumber value="${lineItem['U_PRICE']}" pattern="#,##0.00"/></td>
	  <td class="invoice" style="background:#FFFFCC;text-align:right">(<c:out value="${model.details[fn:toUpperCase(lineItem['ITEM_ID'])]['ClaimQty']}"/>)</td>
	  <td class="invoice" style="background:#FFFFCC;text-align:right"><c:out value="${model.details[fn:toUpperCase(lineItem['ITEM_ID'])]['CheckinQty']}"/></td>
	  <td class="invoice" style="background:#FFFFCC;"><c:out value="${model.details[fn:toUpperCase(lineItem['ITEM_ID'])]['ClaimType']}"/></td>
	  <td class="invoice" style="background:#FFFFCC;text-align:right"><c:out value="${model.details[fn:toUpperCase(lineItem['ITEM_ID'])]['ClaimStatus']}"/></td>  
  </tr>
  </c:if>
  </c:forEach>
  <tr bgcolor="#BBBBBB">
	<td colspan="8">&nbsp;</td>
  </tr>
  <tr>
    <td class="invoice" colspan="7" align="right">Shipment Subtotal:</td>
    <td class="invoice" align="right" style="color:red;"><fmt:message key="${siteConfig['CURRENCY'].value}" /><fmt:formatNumber value="${model.order['CR_SUBTOTAL']}" pattern="#,##0.00"/></td>
  </tr>
  <tr>
    <td class="invoice" colspan="7" align="right">Shipping and Handling:</td>
    <td class="invoice" align="right" style="color:red;"><fmt:message key="${siteConfig['CURRENCY'].value}" /><fmt:formatNumber value="${model.order['CR_SHIPPING_AND_HANDLING']}" pattern="#,##0.00"/></td>
  </tr>
  <tr>
    <td class="invoice" colspan="7" align="right">Shipping Savings:</td>
    <td class="invoice" align="right">(<fmt:message key="${siteConfig['CURRENCY'].value}" /><fmt:formatNumber value="${model.order['CR_SHIPPING_SAVINGS']}" pattern="#,##0.00"/>)</td>
  </tr>
  <tr>
    <td class="invoice" colspan="7" align="right">Discount:</td>
    <td class="invoice" align="right" style="color:red;">(<fmt:message key="${siteConfig['CURRENCY'].value}" /><fmt:formatNumber value="${model.order['CR_DISCOUNT_TOTAL']}" pattern="#,##0.00"/>)</td>
  </tr>
  <tr>
    <td class="invoice" colspan="7" align="right">Payment:</td>
    <td class="invoice" align="right">(<fmt:message key="${siteConfig['CURRENCY'].value}" /><fmt:formatNumber value="${model.order['CR_PAYMENT']}" pattern="#,##0.00"/>)</td>
  </tr>
  <tr>
    <td class="invoice" colspan="7" align="right">Claim Amount:</td>
    <td class="invoice" align="right">(<fmt:message key="${siteConfig['CURRENCY'].value}" /><fmt:formatNumber value="${model.order['CR_CLAIM_AMT']}" pattern="#,##0.00"/>)</td>
  </tr>
  <tr>
    <td class="invoice" colspan="7" align="right">Claim Adjustment:</td>
    <td class="invoice" align="right"><fmt:message key="${siteConfig['CURRENCY'].value}" /><fmt:formatNumber value="${model.order['CR_CLAIM_ADJUSTMENT']}" pattern="#,##0.00"/></td>
  </tr>
  <tr>
    <td class="invoice" colspan="7" align="right"><b>Balance:</b></td>
    <td class="invoice" align="right"><b>(<fmt:message key="${siteConfig['CURRENCY'].value}" /><fmt:formatNumber value="${model.order['CR_BALANCE']}" pattern="#,##0.00"/></b>)</td>
  </tr>
</table>
<%-- 
<table>
  <tr>
    <td valign="top"><b><fmt:message key="trackNum"/>:</b></td>
    <td valign="top"><table>
        <tr><td>
          <a href="http://wwwapps.ups.com/WebTracking/processInputRequest?HTMLVersion=5.0&sort_by=status&tracknums_displayed=10&TypeOfInquiryNumber=T&loc=en_US&InquiryNumber1=<c:out value="${order['SHIPMENT_NO']}" />&AgreeToTermsAndConditions=yes"><c:out value="${order['SHIPMENT_NO']}" /></a>
        </td></tr>
    </table></td>
  </tr>
</table>
--%>
<c:out value="${model.evergreenLayout.footerHtml}" escapeXml="false"/>
</c:if>

</div>

</body>
</html>