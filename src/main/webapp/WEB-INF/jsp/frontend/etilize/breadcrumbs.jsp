<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<c:if test="${not model.product.hideBreadCrumbs and (model.breadCrumbs == null || empty model.breadCrumbs)}">
<c:forEach items="${model.etilizeBreadCrumbs}" var="category" varStatus="status">
<c:if test="${status.first}">
<!-- bread crumbs -->
<table border="0" cellpadding="0" cellspacing="0" class="breadcrumbs">
  <tr>
</c:if> 
<c:if test="${not status.first}">
	&gt;
</c:if>
	<a href="${_contextpath}/esearch.jhtm?parId=${category.id}" class="breadcrumb"><c:out value="${category.name}" escapeXml="false" /></a>
<c:if test="${status.last}">
	</td>
  </tr>
</table>
</c:if>
</c:forEach> 
</c:if>