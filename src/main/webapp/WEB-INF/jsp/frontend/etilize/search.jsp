<%@ page import="java.util.*,com.webjaguar.model.*" %>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>

<%-- DONE Spring3 --%> 
<% 
Layout layout = (Layout) request.getAttribute("layout");
layout.setHeadTag(layout.getHeadTag() + "\n<link href=\"assets/stylesheet_esearch.css\" rel=\"stylesheet\" type=\"text/css\">");
%>

<tiles:insertDefinition name="${_template}" flush="true">
  <tiles:putAttribute name="leftbar" value="/WEB-INF/jsp/frontend/etilize/leftbar.jsp" /> 
  <tiles:putAttribute name="rightbar" value="/WEB-INF/jsp/common/blank.html" />
  <tiles:putAttribute name="content" type="string">

<script language="JavaScript" type="text/JavaScript">
<!--
function checkNumber(aNumber){
	var goodChars = "0123456789";
	var i = 0;
	if (aNumber == "")
		return 0; //empty
	
	for (i = 0 ; i <= aNumber.length - 1 ; i++) {
		if (goodChars.indexOf(aNumber.charAt(i)) == -1) 
			return -1; //invalid number			
	}	
	return 1; //valid number
}
//-->
</script>

<c:import url="/WEB-INF/jsp/frontend/etilize/breadcrumbs.jsp" />

<c:if test="${model.displayName != null}">
<div class="display">
Search Results for: <span class="displayName"><c:out value="${model.displayName}"/></span>
</div>
</c:if>

<c:set var="viewing">
<div class="viewing">
	<div class="viewing_results">Viewing Results: ${model.viewingStart} - ${model.viewingEnd} Of ${model.searchResult.count}</div>
	<div class="viewing_navi">
									
<c:if test="${model.search.pageNo != 1}">
<a href="#" onClick="document.getElementById('pageNo').value='1';document.getElementById('esearch').submit()" class="viewing_navi_on">&lt;&lt;</a>
</c:if>
<c:if test="${model.search.pageNo == 1}">
<span class="viewing_navi_off">&lt;&lt;</span>
</c:if>			

<span class="viewing_navi_sep">|</span>
				
<c:if test="${model.search.pageNo != 1}">
<a href="#" onClick="document.getElementById('pageNo').value='${model.search.pageNo-1}';document.getElementById('esearch').submit()" class="viewing_navi_on">Previous</a>
</c:if>
<c:if test="${model.search.pageNo == 1}">
<span class="viewing_navi_off">Previous</span>
</c:if>

<span class="viewing_navi_sep">|</span>

<c:forEach begin="${model.pageStart}" end="${model.pageEnd}" var="pageNo">
<c:if test="${pageNo != (model.search.pageNo)}">
<a href="#" onClick="document.getElementById('pageNo').value='${pageNo}';document.getElementById('esearch').submit()" class="viewing_navi_on">${pageNo}</a>
</c:if>
<c:if test="${pageNo == (model.search.pageNo)}">
<span class="viewing_pageNo">${pageNo}</span>
</c:if>
<span class="viewing_navi_sep">|</span>
</c:forEach>
			
<c:if test="${model.search.pageNo != model.numOfPages}">
<a href="#" onClick="document.getElementById('pageNo').value='${model.search.pageNo+1}';document.getElementById('esearch').submit()" class="viewing_navi_on">Next</a>
</c:if>
<c:if test="${model.search.pageNo == model.numOfPages}">
<span class="viewing_navi_off">Next</span>
</c:if>

<span class="viewing_navi_sep">|</span>
		
<c:if test="${model.search.pageNo != model.numOfPages}">
<a href="#" onClick="document.getElementById('pageNo').value='${model.numOfPages}';document.getElementById('esearch').submit()" class="viewing_navi_on">&gt;&gt;</a>
</c:if>
<c:if test="${model.search.pageNo == model.numOfPages}">
<span class="viewing_navi_off">&gt;&gt;</span>
</c:if>	

</div>				
</div>
</c:set>
<c:out value="${viewing}" escapeXml="false" />

<div class="sorting">
	<div class="sorting_sort">Sort Results By:
		<select onchange="document.getElementById('sort').value=this.value;document.getElementById('pageNo').value='1';document.getElementById('esearch').submit()">
			<option value="">Relevance</option>
			<option value="price" <c:if test="${param.sort == 'price'}">selected</c:if>>Price: Low to High</option>
			<option value="-price" <c:if test="${param.sort == '-price'}">selected</c:if>>Price: High to Low</option>
		</select>
	</div>
	<div class="sorting_pageSize">Results Per Page:
		<select onchange="document.getElementById('pageSize').value=this.value;document.getElementById('pageNo').value='1';document.getElementById('esearch').submit()">
		<c:forEach begin="10" end="50" var="pageSize" step="10">
			<option value="${pageSize}" <c:if test="${pageSize == (model.search.pageSize)}">selected</c:if>>${pageSize}</option>
		</c:forEach>
		</select>
	</div>
</div>

<br/>

<c:forEach items="${model.products}" var="product" varStatus="status">

<c:set var="productLink">${_contextpath}/product.jhtm?id=${product.id}<c:if test="${model.product == null or siteConfig['RECOMMENDED_LIST_SHOW_BREADCRUMB'].value == 'true'}">&cid=<c:out value="${param.cid}"/></c:if></c:set>
<c:if test="${gSiteConfig['gMOD_REWRITE'] == '1' and not fn:contains(product.sku, '/')}">
  <c:set var="productLink">${_contextpath}/${siteConfig['MOD_REWRITE_PRODUCT'].value}/${product.encodedSku}/${product.encodedName}.html<c:if test="${model.product == null or siteConfig['RECOMMENDED_LIST_SHOW_BREADCRUMB'].value == 'true'}">?cid=<c:out value="${param.cid}"/></c:if></c:set>
</c:if>	

<%@ include file="/WEB-INF/jsp/frontend/thumbnail/view1.jsp" %>

</c:forEach>

<c:out value="${viewing}" escapeXml="false" />   
    
  </tiles:putAttribute>
</tiles:insertDefinition>
<%-- TO BE REMOVED LATER; Printing string , numberic, etc at the end of jsp that is not useful information

<%@page import="javax.xml.parsers.*,org.w3c.dom.*,java.net.*"%>
<%
StringBuffer uri = new StringBuffer("http://ws.spexlive.net/service/rest/catalog?");
uri.append("method=search");
uri.append("&appId=223632");
uri.append("&catalog=na");
uri.append("&keywordFilter=inkjet");
//urlBuffer.append("&attributeFilters=" + search.getSelectFilters().getAttribute());			
uri.append("&attributeFilters=3");			

DocumentBuilderFactory docBuilderFactory = DocumentBuilderFactory.newInstance();
DocumentBuilder docBuilder = docBuilderFactory.newDocumentBuilder();
Document doc = docBuilder.parse(uri.toString());

Element root = doc.getDocumentElement();
NodeList attributes = ((Element) doc.getElementsByTagName("attributeFilters").item(0)).getElementsByTagName("attribute");
for (int i=0; i<attributes.getLength(); i++) {
	Node attributeNode = attributes.item(i);
	if (attributeNode.getNodeType() == Node.ELEMENT_NODE) {
		Element attribute = (Element) attributeNode;
		out.println(attribute.getAttribute("type"));
	}
}
%>
--%>