<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>

<td valign="top" class="e_leftbar">
<form id="esearch" action="${_contextpath}/esearch.jhtm" method="get">
<input type="hidden" name="pageNo" id="pageNo" value="${model.search.pageNo}">
<input type="hidden" name="pageSize" id="pageSize" value="${model.search.pageSize}">
<input type="hidden" name="sort" id="sort" value="${param.sort}">
<input type="hidden" name="manufacturerId" id="manufacturerId" value="${param.manufacturerId}">
<input type="hidden" name="categoryId" id="categoryId" value="${param.categoryId}">
<input type="hidden" name="parId" value="${param.parId}">
<input type="hidden" name="display" value="${param.display}">
<input type="hidden" name="cid" value="${param.cid}">
<table border="0" cellpadding="0" cellspacing="0">
<tr>
<td class="searchFilter"> 
Search Filters
</td>
</tr>
<tr>
<td>
<div class="selected_filter"> 
<div class="selected_filter_title"> 
 Selected Search Filter(s):
</div>
</div>
<c:set var="filtered" value="false"/>

<%-- manufacturer filter --%>
<c:if test="${param.manufacturerId > 0}">
<c:forEach items="${model.searchResult.manufacturerFilters.mf}" var="manufacturerFilter" varStatus="status">
<c:if test="${status.first}">
<a href="#" onClick="document.getElementById('manufacturerId').value='';document.getElementById('pageNo').value='1';document.getElementById('esearch').submit()" class="selected_filters" title="Click to remove this filter from search results.">Manufacturer: <c:out value="${manufacturerFilter.name}" escapeXml="false"/></a>
<c:set var="filtered" value="true"/>
<c:set var="manuFiltered" value="true"/>
</c:if>
</c:forEach>
</c:if>

<%-- category filter --%>
<c:if test="${param.categoryId > 0}">
<a href="#" onClick="document.getElementById('categoryId').value='';document.getElementById('pageNo').value='1';document.getElementById('esearch').submit()" class="selected_filters" title="Click to remove this filter from search results.">Category: <c:out value="${model.etilizeCategories[param.categoryId].name}" escapeXml="false"/><c:if test="${model.etilizeCategories[param.categoryId] == null}">${param.categoryId}</c:if></a>
<c:set var="filtered" value="true"/>
</c:if>

<%-- attribute filter --%>
<c:forEach items="${model.attributes}" var="attribute" varStatus="status">
<%
String attribute = (String) pageContext.getAttribute("attribute");
pageContext.setAttribute("displayName", attribute.substring(attribute.lastIndexOf("|")+1));
%>
<input type="hidden" name="attribute" id="attribute${status.index}" value="<c:out value="${attribute}"/>">
<a href="#" onClick="document.getElementById('attribute${status.index}').disabled=true;document.getElementById('pageNo').value='1';document.getElementById('esearch').submit()" class="selected_filters" title="Click to remove this filter from search results.">
Feature:
<c:choose>
<c:when test="${fn:contains(attribute, '|EQ|')}">=</c:when>
<c:when test="${fn:contains(attribute, '|GE|')}">>=</c:when>
<c:when test="${fn:contains(attribute, '|LE|')}"><=</c:when>
</c:choose>
<c:out value="${displayName}" escapeXml="false"/>
</a>
<c:set var="filtered" value="true"/>
</c:forEach>
<input type="hidden" name="attribute" id="attribute" value="">

<c:if test="${not filtered}">
<div class="no_filters"> 
No Filters Selected.
</div>
</c:if>
<br/>

<div class="filter">
<span class="filter_title">Keyword Filter</span>
<table border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td><input type="text" name="keywords" value="<c:out value="${param.keywords}"/>"></td>
    <td><input type="button" value="Update" onClick="document.getElementById('categoryId').value='';document.getElementById('manufacturerId').value='';document.getElementById('pageNo').value='1';<c:forEach items="${model.attributes}" var="attribute" varStatus="status">document.getElementById('attribute${status.index}').disabled=true;</c:forEach>document.getElementById('esearch').submit()"></td>
  </tr>
</table>
</div>

<div class="filter_category">
<div class="filter">
<c:forEach items="${model.searchResult.categoryFilters.cf}" var="categoryFilter" varStatus="status">
<c:if test="${status.first}">
<span class="filter_title">Category Filter</span>
</c:if>
<div class="filter_category_${categoryFilter.id}">
<a class="filter_link" href="#" onClick="document.getElementById('categoryId').value='${categoryFilter.id}';document.getElementById('pageNo').value='1';document.getElementById('esearch').submit()"><c:out value="${categoryFilter.name}" escapeXml="false"/> (<c:out value="${categoryFilter.count}"/>)</a>
</div>
</c:forEach>
</div>
</div>

<c:if test="${not manuFiltered}">
<div class="filter_manufacturer">
<div class="filter">
<c:forEach items="${model.searchResult.manufacturerFilters.mf}" var="manufacturerFilter" varStatus="status">
<c:if test="${status.first}">
<span class="filter_title">Top Manufacturers</span>
</c:if>
<div class="filter_manufacturer_${manufacturerFilter.id}">
<a class="filter_link" href="#" onClick="document.getElementById('manufacturerId').value='${manufacturerFilter.id}';document.getElementById('pageNo').value='1';document.getElementById('esearch').submit()"><c:out value="${manufacturerFilter.name}" escapeXml="false"/> (<c:out value="${manufacturerFilter.count}"/>)</a>
</div>
</c:forEach>
</div>
</div>
</c:if>

<c:forEach items="${model.searchResult.attributeFilters.attribute}" var="attributeFilter" varStatus="status">
<c:if test="${status.first}">
<div class="filter">
<span class="filter_title">Features</span>
</c:if>
<div class="filter_select_${attributeFilter.id}">
<c:choose>
<c:when test="${attributeFilter.type == 'numeric'}">
<select class="filter_select_operator" id="filter_operator_${status.index}">
<option value="EQ">=</option>
<option value="GE">&gt;=</option>
<option value="LE">&lt;=</option>
</select>
</c:when>
<c:otherwise>
<input type="hidden" id="filter_operator_${status.index}" value="">
</c:otherwise>
</c:choose>
<select class="filter_select_${attributeFilter.type}" onchange="document.getElementById('attribute').value='${attributeFilter.id}|'+document.getElementById('filter_operator_${status.index}').value+'|'+this.value;document.getElementById('pageNo').value='1';document.getElementById('esearch').submit()">
<option><c:out value="${attributeFilter.name}" escapeXml="false"/> (${attributeFilter.count})</option>
<option value="" disabled="disabled">------------------</option>
<c:forEach items="${attributeFilter.value}" var="attributeFilterValue" varStatus="status">
<option value="${attributeFilterValue.id}|<c:out value="${attributeFilterValue.display}"/>"><c:out value="${attributeFilterValue.display}" escapeXml="false"/> (${attributeFilterValue.count})</option>
</c:forEach>
</select>
<div style="padding-top:11px;"> </div>
</div>
<c:if test="${status.last}">
</div>
</c:if>
</c:forEach>
</td>
</tr>
</table>

<img src="${_contextpath}/assets/Image/Layout/transparent.gif" border="0" width="${layout.leftPaneWidth}" height="1">
</form>
</td>
