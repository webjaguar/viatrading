<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="/WEB-INF/tlds/spring.tld" prefix="spring" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>

<tiles:insertDefinition name="${_template}" flush="true">
  <tiles:putAttribute name="content" type="string">
<c:if test="${gSiteConfig['gREGISTRATION_TOUCH_SCREEN']}">
<link rel="stylesheet" href="https://www.viatrading.com/assets/touchscreen/language.css" type="text/css"/>

  	<form action="touchPrintLabel.jhtm" method="POST">
  	<div class="cardWrapper">
  		<lable>Card Id: </lable>
  		<input type="text" name="__cardId" />
  	</div>
  	<div class="qtyWrapper">
  		<lable><fmt:message key="quantity"/>: </lable>
	  	<select name="__qty">
	    <c:forEach var="i" begin="1" end="10">
			<option value="${i}" >${i}</option>
		</c:forEach>
	    </select>
  	</div>
  	
  	<div class="buttonWrapper">
  		<input type="submit" name="__printPdf" value="Print"/>
  	</div>  		
  	</form>
  	
</c:if>
 </tiles:putAttribute>
</tiles:insertDefinition>