<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/> 
<link href="https://www.viatrading.com/assets/css/reset.css" rel="stylesheet" type="text/css"/>
<link href="https://www.viatrading.com/assets/css/anythingslider.css" rel="stylesheet" type="text/css"/>
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" integrity="sha384-1q8mTJOASx8j1Au+a5WDVnPi2lkFfwwEAa8hDDdjZlpLegxhjVME1fgjWPGmkzs7" crossorigin="anonymous">
<link href="/dv/tsr/css/tsr.css" rel="stylesheet">
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="/WEB-INF/tlds/spring.tld" prefix="spring" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
 

<link rel="stylesheet" type="text/css" href="/assets/responsive/css/font-awesome.css">

<script type="text/javascript">

  var _gaq = _gaq || [];
  _gaq.push(['_setAccount', 'UA-1342297-1']);
  _gaq.push(['_trackPageview']);

  (function() {
    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
  })();

</script>
</head>
<body><script type="text/javascript">
//<![CDATA[
try{(function(a){var b="http://",c="www.viatrading.com",d="/cdn-cgi/cl/",e="img.gif",f=new a;f.src=[b,c,d,e].join("")})(Image)}catch(e){}
//]]>
</script>qweqweqweqweqweqweqwe
<div id="pageContainer">

<!-- NAVBAR 
================================================== -->
        <div class="navbar-wrapper">
            <div class="container">
                <nav class="navbar navbar-inverse navbar-fixed-top">
                    <div class="container">
                        <div class="row">
                            <div class="col-md-1">
                                <a href="category.jhtm?cid=834&language=en" class="btn btn-back btn-lg btn-default" role="button">Back</a>
                            </div>
                            <div class="col-md-10"></div>
                            <div class="col-md-1"><img class="via-logo" src="/dv/tsr/imgs/via-logo-white.png"/></div>
                        </div>
                    </div>
                </nav>
            </div>
        </div>
<!-- NAVBAR END
================================================== -->

<div id="content">
<h1>Survey for New Customers</h1>
 
 
<div id="surveyMonkeyInfo" style="width:100%;font-size:10px;color:#666;">
    <div>
        <iframe id="sm_e_s" src="https://www.research.net/jsEmbed.aspx?sm=3I_2foWC_2bWfYPGdROWIfXGqw_3d_3d" width="100%" height="880" style="border:0px;padding-bottom:4px;" frameborder="0" allowtransparency="true">

        </iframe>
    </div>
</div>
 
 
</div>
</div>
<script src="http://ajax.aspnetcdn.com/ajax/jquery/jquery-1.7.1.min.js" type="text/javascript"></script>
<script src="../assets/js/jquery.anythingslider.min.js" type="text/javascript"></script>
<script src="../assets/js/jquery.anythingslider.fx.min.js" type="text/javascript"></script>
<script type="text/javascript">
$(document).ready(function(e) {
    $('#testimonials').anythingSlider({
		buildArrows : true,
		buildNavigation : false,
		buildStartStop : false,
		autoPlay : true,
		toggleArrows : false,
		delay : 4000,
		resizeContents : false,
		expand : true
	}).anythingSliderFx({
		".panel p.fade":[ "fade", "", "", "linear" ]
	});
});
</script>

    <!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
    <!--<script src="js/bootstrap.min.js"></script>
     Latest compiled and minified JavaScript -->
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js" integrity="sha384-0mSbJDEHialfmuBBQP6A4Qrprq5OVfW37PRR3j5ELqxss1yVqOtnepnHVP9aJ7xS" crossorigin="anonymous"></script>
    <!-- IE10 viewport hack for Surface/desktop Windows 8 bug 
    <script src="bootstrap-3-3-6/docs/assets/js/ie10-viewport-bug-workaround.js"></script>-->
</body>
</html>
