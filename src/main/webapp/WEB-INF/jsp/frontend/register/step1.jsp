<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="/WEB-INF/tlds/spring.tld" prefix="spring" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>

<tiles:insertDefinition name="${_template}" flush="true">
  <tiles:putAttribute name="content" type="string">
 
<script type="text/javascript">
function automateCityState(zipCode) {
	var request = new Request.JSON({
		url: "${_contextpath}/jsonZipCode.jhtm?zipCode="+zipCode,
		onRequest: function() { 
		},
		onComplete: function(jsonObj) {
			document.getElementById('customer.address.city').value = jsonObj.city;
			document.getElementById('state').value = jsonObj.stateAbbv;
			$$('.highlight').each(function(el) {
				var end = el.getStyle('background-color');
				end = (end == 'transparent') ? '#fff' : end;
				var myFx = new Fx.Tween(el, {duration: 500, wait: false});
				myFx.start('background-color', '#f00', end);
			});
		}
	}).send();
}
</script>

<script type="text/javascript">

var zChar = new Array(' ', '(', ')', '-', '.');
var maxphonelength = 13;
var phonevalue1;
var phonevalue2;
var cursorposition;

function ParseForNumber1(object){
phonevalue1 = ParseChar(object.value, zChar);
}
function ParseForNumber2(object){
phonevalue2 = ParseChar(object.value, zChar);
}

function backspacerUP(object,e) { 
if(e){ 
e = e 
} else {
e = window.event 
} 
if(e.which){ 
var keycode = e.which 
} else {
var keycode = e.keyCode 
}

ParseForNumber1(object)

if(keycode >= 48){
ValidatePhone(object)
}
}

function backspacerDOWN(object,e) { 
if(e){ 
e = e 
} else {
e = window.event 
} 
if(e.which){ 
var keycode = e.which 
} else {
var keycode = e.keyCode 
}
ParseForNumber2(object)
} 

function GetCursorPosition(){

var t1 = phonevalue1;
var t2 = phonevalue2;
var bool = false
for (i=0; i<t1.length; i++)
{
if (t1.substring(i,1) != t2.substring(i,1)) {
if(!bool) {
cursorposition=i
bool=true
}
}
}
}

function ValidatePhone(object){

var p = phonevalue1

p = p.replace(/[^\d]*/gi,"")

if (p.length < 3) {
object.value=p
} else if(p.length==3){
pp=p;
d4=p.indexOf('(')
d5=p.indexOf(')')
if(d4==-1){
pp="("+pp;
}
if(d5==-1){
pp=pp+")";
}
object.value = pp;
} else if(p.length>3 && p.length < 7){
p ="(" + p; 
l30=p.length;
p30=p.substring(0,4);
p30=p30+")"

p31=p.substring(4,l30);
pp=p30+p31;

object.value = pp; 

} else if(p.length >= 7){
p ="(" + p; 
l30=p.length;
p30=p.substring(0,4);
p30=p30+")"

p31=p.substring(4,l30);
pp=p30+p31;

l40 = pp.length;
p40 = pp.substring(0,8);
p40 = p40 + "-"

p41 = pp.substring(8,l40);
ppp = p40 + p41;

object.value = ppp.substring(0, maxphonelength);
}

GetCursorPosition()

if(cursorposition >= 0){
if (cursorposition == 0) {
cursorposition = 2
} else if (cursorposition <= 2) {
cursorposition = cursorposition + 1
} else if (cursorposition <= 5) {
cursorposition = cursorposition + 2
} else if (cursorposition == 6) {
cursorposition = cursorposition + 2
} else if (cursorposition == 7) {
cursorposition = cursorposition + 4
e1=object.value.indexOf(')')
e2=object.value.indexOf('-')
if (e1>-1 && e2>-1){
if (e2-e1 == 4) {
cursorposition = cursorposition - 1
}
}
} else if (cursorposition < 11) {
cursorposition = cursorposition + 3
} else if (cursorposition == 11) {
cursorposition = cursorposition + 1
} else if (cursorposition >= 12) {
cursorposition = cursorposition
}

var txtRange = object.createTextRange();
txtRange.moveStart( "character", cursorposition);
txtRange.moveEnd( "character", cursorposition - object.value.length);
txtRange.select();
}

}

function ParseChar(sStr, sChar)
{
if (sChar.length == null) 
{
zChar = new Array(sChar);
}
else zChar = sChar;

for (i=0; i<zChar.length; i++)
{
sNewStr = "";

var iStart = 0;
var iEnd = sStr.indexOf(sChar[i]);

while (iEnd != -1)
{
sNewStr += sStr.substring(iStart, iEnd);
iStart = iEnd + 1;
iEnd = sStr.indexOf(sChar[i], iStart);
}
sNewStr += sStr.substring(sStr.lastIndexOf(sChar[i]) + 1, sStr.length);

sStr = sNewStr;
}

return sNewStr;
}
</script>

<c:if test="${!gSiteConfig['gREGISTRATION_DISABLED']}">

<c:out value="${model.createAccount.headerHtml}" escapeXml="false"/>

<spring:hasBindErrors name="customerForm">
<span class="error">Please fix all errors!</span>
</spring:hasBindErrors>

<form:form commandName="customerForm" action="register.jhtm" method="post">
<input type="hidden" name="_page" value="0">
<form:hidden path="forwardAction" />
<fieldset id="customerEmail" class="register_fieldset">
<legend><fmt:message key="emailAddressAndPassword"/></legend><div class="requiredFieldRight" align="right">* required field</div>
<table border="0" cellpadding="3" cellspacing="1">
  <tr>
    <td><div class="requiredField" align="right"> * </div></td>
    <td><fmt:message key="emailAddress" />:</td>
    <td>
	  <form:input path="customer.username" maxlength="80" htmlEscape="true" cssErrorClass="errorField"/>
	  <form:errors path="customer.username" cssClass="error" />    
    </td>
  </tr>
  <c:if test="${siteConfig['CONFIRM_USERNAME'].value == 'true'}">
  <tr>
    <td><div class="requiredField" align="right"> * </div></td>
    <td><fmt:message key="confirmEmailAddress" />:</td>
    <td>
	  <form:input path="confirmUsername" maxlength="80" htmlEscape="true" cssErrorClass="errorField"/>
	  <form:errors path="confirmUsername" cssClass="error" />
    </td>
  </tr>
  </c:if>
  <tr>
    <td><div class="requiredField" align="right"> * </div></td>
    <td><fmt:message key="password" />:</td>
    <td>
	  <form:password path="customer.password" htmlEscape="true" cssErrorClass="errorField"/>
	  <form:errors path="customer.password" cssClass="error" />
    </td>
  </tr>
  <tr>
    <td><div class="requiredField" align="right"> * </div></td>
    <td><fmt:message key="confirmPassword" />:</td>
    <td>
	  <form:password path="confirmPassword" htmlEscape="true" cssErrorClass="errorField"/>
	  <form:errors path="confirmPassword" cssClass="error" />    
    </td>
  </tr>
  <tr>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>
      <span class="note"><fmt:message key="password" />&nbsp;<fmt:message key="form.charLessMin"><fmt:param value="${customerForm.minPasswordLength}"/></fmt:message></span>
    </td>
  </tr>
</table>
</fieldset>
<fieldset id="customerInfo" class="register_fieldset">
<legend><fmt:message key="yourInformation"/></legend>
<table border="0" cellpadding="3" cellspacing="1">
  <tr>
    <td><div class="requiredField" align="right"> * </div></td>
    <td><fmt:message key="firstName" />:</td>
    <td>
	  <form:input path="customer.address.firstName" htmlEscape="true" cssErrorClass="errorField"/>
	  <form:errors path="customer.address.firstName" cssClass="error" />    
    </td>
  </tr>
  <tr>
    <td><div class="requiredField" align="right"> * </div></td>
    <td><fmt:message key="lastName" />:</td>
    <td>
	  <form:input path="customer.address.lastName" htmlEscape="true" cssErrorClass="errorField"/>
	  <form:errors path="customer.address.lastName" cssClass="error" />    
    </td>
  </tr>
  <tr>
    <td><div class="requiredField" align="right"> * </div></td>
    <td><fmt:message key='country' />:</td>
    <td>
      <form:select id="country" path="customer.address.country" onchange="toggleStateProvince(this)">
       <form:option value="" label="Please Select"/>
      <form:options items="${model.countrylist}" itemValue="code" itemLabel="name"/>
      </form:select>
      <form:errors path="customer.address.country" cssClass="error" />      
    </td>
  </tr>
  <tr>
    <td><div class="requiredField" align="right"> * </div></td>
    <td><fmt:message key="zipCode" />:</td>
    <td>
	  <form:input id="automate" path="customer.address.zip" htmlEscape="true"  onkeyup="automateCityState(document.getElementById('automate').value)"/>  
	  <form:input id="noautomate" path="customer.address.zip" htmlEscape="true" />
	  <form:errors path="customer.address.zip" cssClass="error" />    
    </td>
  </tr>
  <tr>
    <td><div class="requiredField" align="right"><c:if test="${siteConfig['CUSTOMERS_REQUIRED_COMPANY'].value == 'true'}"> * </c:if></div></td>
    <td><fmt:message key="companyName" />:</td>
    <td>
	  <form:input path="customer.address.company" htmlEscape="true" cssErrorClass="errorField"/>
	  <form:errors path="customer.address.company" cssClass="error" />    
    </td>
  </tr>
  <tr>
    <td><div class="requiredField" align="right"> * </div></td>
    <td><fmt:message key="address" /> 1:</td>
    <td>
	  <form:input path="customer.address.addr1" htmlEscape="true" cssErrorClass="errorField"/>
	  <form:errors path="customer.address.addr1" cssClass="error" />    
    </td>
  </tr>
  <tr>
    <td><div class="requiredField" align="right"></div></td>
    <td><fmt:message key="address" /> 2:</td>
    <td>
	  <form:input path="customer.address.addr2" htmlEscape="true"/>
	  <form:errors path="customer.address.addr2" cssClass="error" />    
    </td>
  </tr>
  <tr>
    <td><div class="requiredField" align="right"> * </div></td>
    <td><fmt:message key="city" />:</td>
    <td>
	  <form:input id="customer.address.city" path="customer.address.city" htmlEscape="true" cssClass="highlight" cssErrorClass="errorField" />
	  <form:errors path="customer.address.city" cssClass="error" />    
    </td>
  </tr>
  <tr> 
    <td><div class="requiredField" align="right"> * </div></td>
    <td><fmt:message key="stateProvince" />:</td>
    <td>
      <table cellspacing="0" cellpadding="0">
       <tr>
       <td>
         <form:select id="state" path="customer.address.stateProvince" cssClass="highlight" disabled="${customerForm.customer.address.country != 'US'}">
           <form:option value="" label="Please Select"/>
           <form:options items="${model.statelist}" itemValue="code" itemLabel="name"/>
         </form:select>
         <form:select id="ca_province" path="customer.address.stateProvince" disabled="${customerForm.customer.address.country != 'CA'}">
           <form:option value="" label="Please Select"/>
           <form:options items="${model.caProvinceList}" itemValue="code" itemLabel="name"/>
         </form:select>
         <form:input id="province" path="customer.address.stateProvince" htmlEscape="true" disabled="${customerForm.customer.address.country == 'US' or customerForm.customer.address.country == 'CA' or customerForm.customer.address.country == '' or customerForm.customer.address.country == null}"/>
       </td>
       </tr>
       <tr>
       <td class="stateNotapplicable"><div id="stateProvinceNA">&nbsp;<form:checkbox path="customer.address.stateProvinceNA" id="addressStateProvinceNA" value="true"  disabled="${customerForm.customer.address.country == 'US' or customerForm.customer.address.country == 'CA' or customerForm.customer.address.country == '' or customerForm.customer.address.country == null}"/><fmt:message key="notApplicable"/></div></td>
       <td>
         <form:errors path="customer.address.stateProvince" cssClass="error" />
       </td>
       </tr>
      </table>    
    </td>
  </tr>
  <c:if test="${siteConfig['ADDRESS_RESIDENTIAL_COMMERCIAL'].value > 1}">
    <tr>
    <td><div class="requiredField" align="right"></div></td>
    <td><fmt:message key="deliveryType" />:</td>
    <td><form:radiobutton path="customer.address.residential" value="true"/>: <fmt:message key="residential" /><br /><form:radiobutton path="customer.address.residential" value="false"/>: <fmt:message key="commercial" /> </td>
    <td></td>
    </tr>
    <c:if test="${siteConfig['SHIPPING_LTL'].value == 'true'}">
      <tr>
      <td><div class="requiredField" align="right"></div></td>
      <td><fmt:message key="liftGateDelivery" />:</td>
      <td><form:checkbox path="customer.address.liftGate" value="true"/><a href="${_contextpath}/category.jhtm?cid=307" onclick="window.open(this.href,'','width=600,height=300,resizable=yes'); return false;"><img class="toolTipImg" src="${_contextpath}/assets/Image/Layout/question.gif" border="0" /></a> </td>
      </tr>
    </c:if>
  </c:if>
  <tr>
    <td><div class="requiredField" align="right"> * </div></td>
    <td><fmt:message key="phone" />:</td>
    <td>
	  <form:input path="customer.address.phone" htmlEscape="true" cssErrorClass="errorField" id="phone_num"/>
	  <form:input path="customer.address.phone"  onkeydown="javascript:backspacerDOWN(this,event);" onkeyup="javascript:backspacerUP(this,event);" id="us_phone_num"/>  
	  <form:errors path="customer.address.phone" cssClass="error" />    
    </td>
  </tr>
  <tr>
    <td><div class="requiredField" align="right"></div></td>
    <td><fmt:message key="cellPhone" />: </td>
    <td>            
      <form:input path="customer.address.cellPhone" htmlEscape="true" id="cell_phone_num"/>
      <form:input path="customer.address.cellPhone"  onkeydown="javascript:backspacerDOWN(this,event);" onkeyup="javascript:backspacerUP(this,event);" id="us_cell_phone_num"/>  
	  <form:errors path="customer.address.cellPhone" cssClass="error" />    
    </td>
  </tr>
  <tr id="mobileCarrierIdSelect">
  	<td><div class="requiredField" align="right"></div></td>
    <td><fmt:message key='mobileCarrier' />:</td>
    <td>
      <form:select id="mobileCarrierId" path="customer.address.mobileCarrierId" >
      <form:option value="" label="Please Select"/>
      <form:options items="${model.mobileCarrierList}" itemValue="id" itemLabel="carrier"/>
      </form:select>
      <form:errors path="customer.address.mobileCarrierId" cssClass="error" />      
    </td>
  </tr>
  <tr id="textMessageNotify">
  	<td><div class="requiredField" align="right"></div></td>
    <td><fmt:message key="textMessageNotification" />:</td>
    <td><form:checkbox path="customer.textMessageNotify"/></td>
  </tr>
  <tr id="textMessageDisclaimer">
  	<td><div class="requiredField" align="right"></div></td>
  	<td><div class="requiredField" align="right"></div></td>
      <td class="disclaimer">
     <span class="helpNote"><p><fmt:message key="textMessageDisclaimer"/></p></span>
  	</td>  
  </tr>
  <tr>
  <td><div class="requiredField" align="right"></div></td>
	<td><fmt:message key="email" /> <fmt:message key="notification" />:</td>
	<td><form:checkbox path="customer.emailNotify"/></td>
	</tr>
  <tr id="emailDisclaimer">
  	<td><div class="requiredField" align="right"></div></td>
  	<td><div class="requiredField" align="right"></div></td>
      <td class="disclaimer">
     <span class="helpNote"><p><fmt:message key="emailDisclaimer"/></p></span>
  	</td>  
  </tr> 
  <tr>
    <td><div class="requiredField" align="right"></div></td>
    <td><fmt:message key="fax" />:</td>
    <td>
      <form:input path="customer.address.fax" htmlEscape="true"/>
	  <form:errors path="customer.address.fax" cssClass="error" />    
    </td>
  </tr>
  <c:if test="${gSiteConfig['gI18N'] == '1'}">
  <tr id="languageId">
    <td><div class="requiredField" align="right"></div></td>
    <td><fmt:message key="language" />:</td>
    <td>
      <form:select path="customer.languageCode">
        <form:option value="en"><fmt:message key="language_en"/></form:option>
        <c:forEach items="${model.languageCodes}" var="i18n">         	
          	<option value="${i18n.languageCode}" <c:if test="${customerForm.customer.languageCode == i18n.languageCode}">selected</c:if>><fmt:message key="language_${i18n.languageCode}"/></option>
        </c:forEach>
	  </form:select>
    </td>
  </tr>
  </c:if>
  <c:if test="${gSiteConfig['gMASS_EMAIL'] or siteConfig['CUSTOMER_MASS_EMAIL'].value == 'true'}">
  	<tr class="subscribeCheckBox" id="subscribeCheckBoxId">  	
      <td><form:checkbox path="subscribeEmail"  /></td>
      <td colspan="2"><fmt:message key="f_subscribeMailingList" /></td>
  	</tr>
  </c:if>
</table>
</fieldset>

<c:if test="${siteConfig['NOTE_ON_REGISTRATION'].value == 'true'}">
<fieldset class="register_fieldset">
<legend><fmt:message key="registerNote"/></legend>
<table border="0" cellpadding="3" cellspacing="1">
  <tr>
    <td><div class="requiredField" align="right"></div></td>
    <td style="white-space: nowrap"><fmt:message key="note" /> :</td>
    <td>
      <form:textarea path="customer.note" htmlEscape="true" cssClass="registrationNote"/>
	  <form:errors path="customer.note" cssClass="error" />  
    </td>
  </tr>
  <tr>
    <td>&nbsp;</td>
    <td  colspan="2">
      <div id="noteOnRegistration" class="note" style="color:#FF0000"><fmt:message key="noteOnRegistrationMessage" /></div>
    </td>
  </tr>
</table>
</fieldset>  
</c:if>

<c:if test="${gSiteConfig['gTAX_EXEMPTION']}">
<fieldset id="taxId" class="register_fieldset">
<legend><fmt:message key="taxInformation"/></legend>
<table border="0" cellpadding="3" cellspacing="1">
  <tr>
    <c:choose>
      <c:when test="${siteConfig['TAX_ID_REQUIRED'].value == 'true'}"><td><div class="requiredField" align="right"> * </div></td></c:when>
      <c:otherwise><td><div class="requiredField" align="right"></div></td></c:otherwise>
    </c:choose>
    <td style="white-space: nowrap"><fmt:message key="f_taxId" /> :</td>
    <td>
      <form:input path="customer.taxId" maxlength="30" htmlEscape="true" cssErrorClass="errorField"/>
	  <form:errors path="customer.taxId" cssClass="error" />      
    </td>
  </tr>
  <tr>
    <td>&nbsp;</td>
    <td colspan="2">
      <div id="taxIdNote" class="note" style="color:#FF0000">
      <!-- viatrading wants a Spanish text also 
      <c:out value="${siteConfig['TAX_ID_NOTE'].value}" />
      -->
      <fmt:message key="f_taxNote" /></div>
    </td>
  </tr>
</table>
</fieldset>  
</c:if>

<div align="center" id="buttonWrapper">
<c:choose>
  <c:when test="${customerForm.customerFields != null}">
    <input type="submit" value="<fmt:message key="nextStep" />" name="_target1" class="f_button" />
  </c:when>
  <c:otherwise>
    <input type="submit" value="<fmt:message key="nextStep" />" name="_target2" class="f_button" />
  </c:otherwise>
</c:choose>
<%--
  <input type="submit" value="Register" name="_finish">
--%>
</div>
</form:form>

<c:out value="${model.createAccount.footerHtml}" escapeXml="false"/>

<script language="JavaScript">
<!--
function toggleStateProvince(el) {
  if (el.value == "US") {
      document.getElementById('state').disabled=false;
      document.getElementById('state').style.display="block";
      document.getElementById('ca_province').disabled=true;
      document.getElementById('ca_province').style.display="none";
      document.getElementById('province').disabled=true;
      document.getElementById('province').style.display="none";
      document.getElementById('addressStateProvinceNA').disabled=true;
      document.getElementById('stateProvinceNA').style.display="none";
      document.getElementById('mobileCarrierIdSelect').disabled=false;
      document.getElementById('mobileCarrierIdSelect').style.display="table-row";
      document.getElementById('textMessageNotify').disabled=false;
      document.getElementById('textMessageNotify').style.display="table-row";
      document.getElementById('textMessageDisclaimer').disabled=false;
      document.getElementById('textMessageDisclaimer').style.display="table-row";
      document.getElementById('automate').disabled=false;
      document.getElementById('automate').style.display="block";
      document.getElementById('noautomate').disabled=true;
      document.getElementById('noautomate').style.display="none";
      document.getElementById('phone_num').disabled=true;
      document.getElementById('phone_num').style.display="none";
      document.getElementById('us_phone_num').disabled=false;
      document.getElementById('us_phone_num').style.display="block";
      document.getElementById('cell_phone_num').disabled=true;
      document.getElementById('cell_phone_num').style.display="none";
      document.getElementById('us_cell_phone_num').disabled=false;
      document.getElementById('us_cell_phone_num').style.display="block";
  } else if (el.value == "CA") {
      document.getElementById('state').disabled=true;
      document.getElementById('state').style.display="none";
      document.getElementById('ca_province').disabled=false;
      document.getElementById('ca_province').style.display="block";
      document.getElementById('province').disabled=true;
      document.getElementById('province').style.display="none";
      document.getElementById('addressStateProvinceNA').disabled=true;
      document.getElementById('stateProvinceNA').style.display="none";
      document.getElementById('mobileCarrierIdSelect').disabled=true;
      document.getElementById('mobileCarrierIdSelect').style.display="none";
      document.getElementById('textMessageNotify').disabled=true;
      document.getElementById('textMessageNotify').style.display="none";
      document.getElementById('textMessageDisclaimer').disabled=true;
      document.getElementById('textMessageDisclaimer').style.display="none";
      document.getElementById('automate').disabled=true;
      document.getElementById('automate').style.display="none";
      document.getElementById('noautomate').disabled=false;
      document.getElementById('noautomate').style.display="block";
      document.getElementById('us_phone_num').disabled=true;
      document.getElementById('us_phone_num').style.display="none";
      document.getElementById('phone_num').disabled=false;
      document.getElementById('phone_num').style.display="block";
      document.getElementById('us_cell_phone_num').disabled=true;
      document.getElementById('us_cell_phone_num').style.display="none";
      document.getElementById('cell_phone_num').disabled=false;
      document.getElementById('cell_phone_num').style.display="block";
  } else {
      document.getElementById('state').disabled=true;
      document.getElementById('state').style.display="none";
      document.getElementById('ca_province').disabled=true;
      document.getElementById('ca_province').style.display="none";
      document.getElementById('province').disabled=false;
      document.getElementById('province').style.display="block";
      document.getElementById('addressStateProvinceNA').disabled=false;
      document.getElementById('stateProvinceNA').style.display="block";
      document.getElementById('mobileCarrierIdSelect').disabled=true;
      document.getElementById('mobileCarrierIdSelect').style.display="none";
      document.getElementById('textMessageNotify').disabled=true;
      document.getElementById('textMessageNotify').style.display="none";
      document.getElementById('textMessageDisclaimer').disabled=true;
      document.getElementById('textMessageDisclaimer').style.display="none";
      document.getElementById('automate').disabled=true;
      document.getElementById('automate').style.display="none";
      document.getElementById('noautomate').disabled=false;
      document.getElementById('noautomate').style.display="block";
      document.getElementById('us_phone_num').disabled=true;
      document.getElementById('us_phone_num').style.display="none";
      document.getElementById('phone_num').disabled=false;
      document.getElementById('phone_num').style.display="block";
      document.getElementById('us_cell_phone_num').disabled=true;
      document.getElementById('us_cell_phone_num').style.display="none";
      document.getElementById('cell_phone_num').disabled=false;
      document.getElementById('cell_phone_num').style.display="block";
  }
}
toggleStateProvince(document.getElementById('country'));
//-->
</script>

</c:if>
  </tiles:putAttribute>
</tiles:insertDefinition>