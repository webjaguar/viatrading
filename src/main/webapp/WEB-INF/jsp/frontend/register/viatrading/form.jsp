
<%@ page session="false" %>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="/WEB-INF/tlds/spring.tld" prefix="spring" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>


<tiles:insertDefinition name="${_template}" flush="true">
  <c:if test="${_leftBar != '1' and _leftBar != '4'}">
    <tiles:putAttribute name="leftbar" value="/WEB-INF/jsp/frontend/layout/template${_leftBar}/leftbar.jsp" />
  </c:if>
  <tiles:putAttribute name="content" type="string">
<c:if test="${!gSiteConfig['gREGISTRATION_DISABLED']}">

<c:out value="${model.createAccount.headerHtml}" escapeXml="false"/>

<script type="text/javascript">
function automateCityState(zipCode) {

 var xmlhttp;
  if (window.XMLHttpRequest)
  {// code for IE7+, Firefox, Chrome, Opera, Safari
  	xmlhttp=new XMLHttpRequest();
  }
  else
  {// code for IE6, IE5
  	xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
  }
	
  xmlhttp.onreadystatechange=function()
  {	
    if (xmlhttp.readyState==4 && xmlhttp.status==200){
	var jsonObj = JSON.parse(xmlhttp.responseText);
	if(jsonObj!=null && jsonObj.city!=null)
	document.getElementById('customer_address_city').value = jsonObj.city;
	if(jsonObj!=null && jsonObj.stateAbbv!=null)
        document.getElementById('customer_address_state').value = jsonObj.stateAbbv;
	
	}
  }
  
  xmlhttp.open("GET","/jsonZipCode.jhtm?zipCode="+zipCode,true);
  xmlhttp.send();

}

</script>

<spring:hasBindErrors name="customerForm">
<span class="error">Please fix all errors!</span>
</spring:hasBindErrors>

<div class="col-sm-12">
  <div class="message"> <c:out value="${model.error}"></c:out> </div>
  <form:form id="register_newCustomer_form" commandName="customerForm" action="viaRegister.jhtm" method="post" cssClass="form-horizontal" role="form" enctype="multipart/form-data">	

 <input type="hidden" name="_page" value="0">
  <form:hidden path="forwardAction" />
  
  <div class="row">
	  <div class="col-sm-12">
		<div class="requiredFieldLabel">* Required Field</div>
	  </div>
	</div>
	<div id="customerEmailAndPassword">
	  <div class="row">
		<div class="col-sm-6">
		  <div class="form-group">
			<div class="col-sm-12">
			  <h3><fmt:message key="emailAddressAndPassword" /></h3>
			</div>
		  </div>
		  <spring:bind path="customer.username">
		  <div class="form-group <c:if test="${status.error}">has-feedback has-error</c:if>">
			<div class="col-sm-4">
			  <label for="customer_username" class="control-label">
				<fmt:message key="emailAddress" /> <sup class="requiredField">*</sup>
			  </label>
			</div>
		    <div class="col-sm-8">
			  	<form:input autocomplete="off" path="customer.username" htmlEscape="true" id="customer_username" cssClass="form-control"/>
	  		  	<c:if test="${status.error}">
	  		  		<i class="form-control-feedback glyphicon glyphicon-remove"></i>
            		<small class="help-block error-message">
            			<form:errors path="customer.username" cssClass="error" />
            		</small>    
	  		  	</c:if>
	  		</div>
		  </div>
		  </spring:bind>
		  
 
		  <spring:bind path="customer.password">
		  <div class="form-group <c:if test="${status.error}">has-feedback has-error</c:if>">
		  <div class="col-sm-4">
			  <label for="customer_password" class="control-label">
				<fmt:message key="password" /> <sup class="requiredField">*</sup>
			  </label>
			</div>
		    <div class="col-sm-8">
			  <form:password path="customer.password" htmlEscape="true" id="customer_password" cssClass="form-control"/>
	  		  <c:if test="${status.error}">
	  		  	<i class="form-control-feedback glyphicon glyphicon-remove"></i>
            	<small class="help-block error-message">
		  		  <form:errors path="customer.password" cssClass="error" />
	        	</small>    
	  		  </c:if>
			</div>
		  </div>
		  </spring:bind>
		  
		  <spring:bind path="customer.password">
		  <div class="form-group <c:if test="${status.error}">has-feedback has-error</c:if>">
		  <div class="col-sm-4">
			  <label for="customer_password" class="control-label">
				<fmt:message key="password" /> <sup class="requiredField">*</sup>
			  </label>
			</div>
		    <div class="col-sm-8">
			  <form:password path="customer.password" htmlEscape="true" id="customer_password" cssClass="form-control"/>
	  		  <c:if test="${status.error}">
	  		  	<i class="form-control-feedback glyphicon glyphicon-remove"></i>
            	<small class="help-block error-message">
		  		  <form:errors path="customer.password" cssClass="error" />
	        	</small>    
	  		  </c:if>
			</div>
		  </div>
		  </spring:bind>
		  
		</div>
		
		<div class="col-sm-6">
		  <div id="customerEmailAndPassword_right_wrapper">
		  </div>
		</div>
	  </div>
	</div>
	<div id="customerInformation">
	  <div class="row">
		<div class="col-sm-6">
		  <div class="form-group">
			<div class="col-sm-12">
			  <h3>Your Information</h3>
			</div>
		  </div>
		  
		  <spring:bind path="customer.address.firstName">
		  <div class="form-group <c:if test="${status.error}">has-feedback has-error</c:if>">
		  <div class="col-sm-4">
			  <label for="customer_address_firstName" class="control-label">
				<fmt:message key="firstName" /> <sup class="requiredField">*</sup>
			  </label>
			</div>
			<div class="col-sm-8">
			  <form:input autocomplete="off" path="customer.address.firstName" id="customer_address_firstName" htmlEscape="true" cssClass="form-control"/>
	  		  <c:if test="${status.error}">
	  		  	<i class="form-control-feedback glyphicon glyphicon-remove"></i>
            	<small class="help-block error-message">
		  		  <form:errors path="customer.address.firstName" cssClass="error" />    
	        	</small>    
	  		  </c:if>
			</div>
		  </div>
		  </spring:bind>
		  
		  <spring:bind path="customer.address.lastName">
		  <div class="form-group <c:if test="${status.error}">has-feedback has-error</c:if>">
		  <div class="col-sm-4">
			  <label for="customer_address_lastName" class="control-label">
				<fmt:message key="lastName" /> <sup class="requiredField">*</sup>
			  </label>
			</div>
		    <div class="col-sm-8">
			  <form:input autocomplete="off" path="customer.address.lastName" id="customer_address_lastName" htmlEscape="true" cssClass="form-control"/>
	  		  <c:if test="${status.error}">
	  		  	<i class="form-control-feedback glyphicon glyphicon-remove"></i>
            	<small class="help-block error-message">
		  		  <form:errors path="customer.address.lastName" cssClass="error" />    
	        	</small>    
	  		  </c:if>
			</div>
		  </div>
		  </spring:bind>


		  <spring:bind path="customer.address.company">
                  <div class="form-group <c:if test="${status.error}">has-feedback has-error</c:if>">
                  <div class="col-sm-4">
                          <label for="customer_address_company" class="control-label">
                                <fmt:message key="companyName" /> <c:if test="${siteConfig['CUSTOMERS_REQUIRED_COMPANY'].value == 'true'}"> <sup class="requiredField">*</sup> </c:if>
                          </label>
                        </div>
                        <div class="col-sm-8">
                          <form:input autocomplete="off" path="customer.address.company" id="customer_address_company"  htmlEscape="true" cssClass="form-control"/>
                          <c:if test="${status.error}">
                                <i class="form-control-feedback glyphicon glyphicon-remove"></i>
                <small class="help-block error-message">
                                  <form:errors path="customer.address.company" cssClass="error" />
                        </small>
                          </c:if>
                        </div>
                  </div>
                  </spring:bind>

	
		  <spring:bind path="customer.address.addr1">
                  <div class="form-group <c:if test="${status.error}">has-feedback has-error</c:if>">
                  <div class="col-sm-4">
                          <label for="customer_address_addr1" class="control-label">
                                <fmt:message key="address" /> 1 <sup class="requiredField">*</sup>
                          </label>
                        </div>
                        <div class="col-sm-8">
                          <form:input autocomplete="off" path="customer.address.addr1" id="customer_address_addr1" htmlEscape="true" cssClass="form-control"/>
                          <c:if test="${status.error}">
                                <i class="form-control-feedback glyphicon glyphicon-remove"></i>
                <small class="help-block error-message">
                                  <form:errors path="customer.address.addr1" cssClass="error" />
                        </small>
                          </c:if>
                        </div>
                  </div>
                  </spring:bind>

                  <spring:bind path="customer.address.addr2">
                  <div class="form-group <c:if test="${status.error}">has-feedback has-error</c:if>">
                  <div class="col-sm-4">
                          <label for="customer_address_addr2" class="control-label">
                                <fmt:message key="address" /> 2
                          </label>
                        </div>
                        <div class="col-sm-8">
                          <form:input autocomplete="off" path="customer.address.addr2" id="customer_address_addr2" htmlEscape="true" cssClass="form-control"/>
                          <c:if test="${status.error}">
                                <i class="form-control-feedback glyphicon glyphicon-remove"></i>
                <small class="help-block error-message">
                                  <form:errors path="customer.address.addr2" cssClass="error" />
                        </small>
                          </c:if>
                        </div>
                  </div>
                  </spring:bind>
	  
		  
		  <spring:bind path="customer.address.country">
		  <div class="form-group <c:if test="${status.error}">has-feedback has-error</c:if>">
		  <div class="col-sm-4">
			  <label for="customer_address_country" class="control-label">
				<fmt:message key="country" /> <sup class="requiredField">*</sup>
			  </label>
			</div>
			<div class="col-sm-8">
			  <form:select id="customer_address_country" class="form-control" path="customer.address.country">
		        <form:option value="" label="Please Select"/>
		        <form:options items="${countrylist}" itemValue="code" itemLabel="name"/>
		      </form:select>
	  		  <c:if test="${status.error}">
	  		  	<i class="form-control-feedback glyphicon glyphicon-remove"></i>
            	<small class="help-block error-message">
			      <form:errors path="customer.address.country" cssClass="error" />      
	        	</small>    
	  		  </c:if>
		    </div>
		  </div>
		  </spring:bind>
		  
  		<spring:bind path="customer.address.zip">
                <div class="form-group <c:if test="${status.error}">has-feedback has-error</c:if>">
                  <div class="col-sm-4">
                        <label for="customer_address_zip" class="control-label">
                          <fmt:message key="zipCode" /> <sup class="requiredField">*</sup>
                        </label>
                  </div>
                  <div class="col-sm-8">
                        <form:input autocomplete="off" id="automate" path="customer.address.zip" htmlEscape="true" cssClass="form-control" onkeyup="automateCityState(document.getElementById('automate').value)"/>
                          <c:if test="${status.error}">
                                <i class="form-control-feedback glyphicon glyphicon-remove"></i>
                <small class="help-block error-message">
                          <form:errors path="customer.address.zip" cssClass="error" />
                                </small>
                          </c:if>
          </div>
                </div>
                </spring:bind>
		  
		  <spring:bind path="customer.address.stateProvince">
		  <div class="form-group <c:if test="${status.error}">has-feedback has-error</c:if>">
		  <div class="col-sm-4">
			  <label for="" class="control-label">
				<fmt:message key="stateProvince" /> <sup class="requiredField">*</sup>
			  </label>
			</div>
			<div class="col-sm-8">
			  <form:select id="customer_address_state" name="customer.address.state" class="form-control" path="customer.address.stateProvince" disabled="${customerForm.customer.address.country != 'US'}">
	            <form:option value="" label="Please Select"/>
	            <form:options items="${statelist}" itemValue="code" itemLabel="name"/>
	          </form:select>
	          <form:select id="customer_address_ca_province" class="form-control" path="customer.address.stateProvince" disabled="${customerForm.customer.address.country != 'CA'}">
	            <form:option value="" label="Please Select"/>
	            <form:options items="${caProvinceList}" itemValue="code" itemLabel="name"/>
	          </form:select>
	          <form:input autocomplete="off" id="customer_address_stateProvince" class="form-control" path="customer.address.stateProvince" htmlEscape="true" disabled="${customerForm.customer.address.country == 'US' or customerForm.customer.address.country == 'CA' or customerForm.customer.address.country == '' or customerForm.customer.address.country == null}"/>
	          <div id="stateProvinceNA" class="checkbox">
				<label>
				  <form:checkbox path="customer.address.stateProvinceNA" id="customer_address_stateProvinceNA" value="true"  disabled="${customerForm.customer.address.country == 'US' or customerForm.customer.address.country == 'CA' or customerForm.customer.address.country == '' or customerForm.customer.address.country == null}"/><fmt:message key="notApplicable"/>
       		    </label>
			  </div>
	  		  <c:if test="${status.error}">
	  		  	<i class="form-control-feedback glyphicon glyphicon-remove"></i>
            	<small class="help-block error-message">
				  <form:errors path="customer.address.stateProvince" cssClass="error" />
	        	</small>    
	  		  </c:if>
       	  </div>
		</div>
		</spring:bind>
		
		<c:if test="${gSiteConfig['gWILDMAN_GROUP'] and !empty customerForm.accountLocationMap}">
   		  <spring:bind path="customer.accountNumber">
		  <div class="form-group <c:if test="${status.error}">has-feedback has-error</c:if>">
		  <div class="col-sm-4">
			  <label for="customer_address_addr2" class="control-label">
				<fmt:message key="location" /> <sup class="requiredField">*</sup>
			  </label>
			</div>
			<div class="col-sm-8">
			  <form:select path="customer.accountNumber">
	            <form:option value="" label="Please Select"/>
	            <c:forEach items="${customerForm.accountLocationMap}" var="accountLocation">         	
	          	  <option value="${accountLocation.value}" <c:if test="${customerForm.customer.accountNumber == accountLocation.value}">selected</c:if>>${accountLocation.key}</option>
	        	</c:forEach>
	          </form:select>
	          <c:if test="${status.error}">
            	<small class="help-block error-message">
				  <form:errors path="customer.accountNumber" cssClass="error" />
	        	</small>    
	  		  </c:if>
			</div>
		  </div>
		  </spring:bind>
 	    </c:if>

		<spring:bind path="customer.address.city"> 
                  <div class="form-group <c:if test="${status.error}">has-feedback has-error</c:if>">
                  <div class="col-sm-4">
                          <label for="customer_address_city" class="control-label">
                                <fmt:message key="city" /> <sup class="requiredField">*</sup>
                          </label>
                        </div>
                    <div class="col-sm-8">
                          <form:input autocomplete="off" path="customer.address.city" id="customer_address_city"  name="customer.address.city" htmlEscape="true" cssClass="form-control"/>
                          <c:if test="${status.error}">
                                <i class="form-control-feedback glyphicon glyphicon-remove"></i>
                <small class="help-block error-message">
                                  <form:errors path="customer.address.city" cssClass="error" />
                        </small>
                          </c:if>
                        </div>
                  </div>
                  </spring:bind>		
		
		<spring:bind path="customer.address.phone">
		<div class="form-group <c:if test="${status.error}">has-feedback has-error</c:if>">
		  <div class="col-sm-4">
			<label for="customer_address_phone" class="control-label">
			  <fmt:message key="phone" /> <sup class="requiredField">*</sup>
			</label>
		  </div>
		  <div class="col-sm-8">
			<form:input autocomplete="off" path="customer.address.phone" id="customer_address_phone" htmlEscape="true" cssClass="form-control"/>
	  		  <c:if test="${status.error}">
	  		  	<i class="form-control-feedback glyphicon glyphicon-remove"></i>
            	<small class="help-block error-message">
			  		<form:errors path="customer.address.phone" cssClass="error" />    
				</small>    
	  		  </c:if>
    	  </div>
		</div>
		</spring:bind>
		
		<spring:bind path="customer.address.cellPhone">
		<div class="form-group <c:if test="${status.error}">has-feedback has-error</c:if>">
		  <div class="col-sm-4">
			<label for="customer_address_cellPhone" class="control-label">
			  <fmt:message key="cellPhone" />
			</label>
		  </div>
		  <div class="col-sm-8">
			<form:input autocomplete="off" id="customer_address_cellPhone" class="form-control" path="customer.address.cellPhone" htmlEscape="true"/>
	  		  <c:if test="${status.error}">
	  		  	<i class="form-control-feedback glyphicon glyphicon-remove"></i>
            	<small class="help-block error-message">
			  		<form:errors path="customer.address.cellPhone" cssClass="error" />    
				</small>    
	  		  </c:if>
    	  </div>
		</div>
		</spring:bind>
		

	     <c:if test="${gSiteConfig['gMASS_EMAIL'] or siteConfig['CUSTOMER_MASS_EMAIL'].value == 'true'}">
                <spring:bind path="subscribeEmail">
                <div class="form-group <c:if test="${status.error}">has-feedback has-error</c:if>">
                  <div class="col-sm-12">
                        <div id="subscribeCheckBoxId" class="checkbox">
                          <label><form:checkbox path="subscribeEmail"  />
                            <fmt:message key="f_subscribeMailingList" />
                  </label>
                    </div>
                  </div>
                </div>
            </spring:bind>
            </c:if>

                <spring:bind path="customer.amazonName">
                <div class="form-group <c:if test="${status.error}">has-feedback has-error</c:if>">
                  <div class="col-sm-4">
                        <label for="customer_address_fax" class="control-label">
                          <fmt:message key="amazonName" />:
                        </label>
                  </div>
                  <div class="col-sm-8">
                        <form:input autocomplete="off" path="customer.amazonName" id="customer_address_fax" class="form-control" htmlEscape="true"/>
                          <c:if test="${status.error}">
                                <i class="form-control-feedback glyphicon glyphicon-remove"></i>
                <small class="help-block error-message">
                                        <form:errors path="customer.amazonName" cssClass="error" />
                                </small>
                          </c:if>
	          </div>
                </div>
                </spring:bind>

		<spring:bind path="customer.websiteUrl">
                <div class="form-group <c:if test="${status.error}">has-feedback has-error</c:if>">
                  <div class="col-sm-4">
                        <label for="customer_address_fax" class="control-label">
                          <fmt:message key="websiteUrl" />:
                        </label>
                  </div>
                  <div class="col-sm-8">
                        <form:input autocomplete="off" path="customer.websiteUrl" id="customer_address_fax" class="form-control" htmlEscape="true"/>
                          <c:if test="${status.error}">
                                <i class="form-control-feedback glyphicon glyphicon-remove"></i>
                <small class="help-block error-message">
                                        <form:errors path="customer.websiteUrl" cssClass="error" />
                                </small>
                          </c:if>
                  </div>
                </div>
                </spring:bind>

		<spring:bind path="customer.ebayName">
                <div class="form-group <c:if test="${status.error}">has-feedback has-error</c:if>">
                  <div class="col-sm-4">
                        <label for="customer_address_fax" class="control-label">
                          <fmt:message key="ebayName" />:
                        </label>
                  </div>
                  <div class="col-sm-8">
                        <form:input autocomplete="off" path="customer.ebayName" id="customer_address_fax" class="form-control" htmlEscape="true"/>
                          <c:if test="${status.error}">
                                <i class="form-control-feedback glyphicon glyphicon-remove"></i>
                <small class="help-block error-message">
                                        <form:errors path="customer.ebayName" cssClass="error" />
                                </small>
                          </c:if>
                  </div>
                </div>
                </spring:bind>

		
		<spring:bind path="customer.address.fax">
		<div class="form-group <c:if test="${status.error}">has-feedback has-error</c:if>">
		  <div class="col-sm-4">
			<label for="customer_address_fax" class="control-label">
			  <fmt:message key="fax" />
			</label>
		  </div>
		  <div class="col-sm-8">
			<form:input autocomplete="off" path="customer.address.fax" id="customer_address_fax" class="form-control" htmlEscape="true"/>
	  		  <c:if test="${status.error}">
	  		  	<i class="form-control-feedback glyphicon glyphicon-remove"></i>
            	<small class="help-block error-message">
			  		<form:errors path="customer.address.fax" cssClass="error" />    
				</small>    
	  		  </c:if>
    	  	</div>
		</div>
		</spring:bind>
		
		<c:if test="${gSiteConfig['gI18N'] == '1'}">
		<spring:bind path="customer.languageCode">
		<div class="form-group <c:if test="${status.error}">has-feedback has-error</c:if>">
		  <div class="col-sm-4">
			<label for="customer_address_fax" class="control-label">
			  <fmt:message key="language" />
			</label>
		  </div>
		  <div class="col-sm-8">
		    <form:select path="customer.languageCode" class="form-control">
		      <form:option value="en"><fmt:message key="language_en"/></form:option>
		      <c:forEach items="${languageCodes}" var="i18n">         	
		       	<option value="${i18n.languageCode}" <c:if test="${customerForm.customer.languageCode == i18n.languageCode}">selected</c:if>><fmt:message key="language_${i18n.languageCode}"/></option>
		      </c:forEach>
			</form:select>
		  </div>
		</div>
		</spring:bind>
	    </c:if>


	    	<c:if test="${customerForm.customerFields != null}">
          <c:forEach items="${customerForm.customerFields}" var="customerField">
            <spring:bind path="customer.field${customerField.id}">
		    <div class="form-group <c:if test="${status.error}">has-feedback has-error</c:if>">
		    <div class="col-sm-4">
				<label for="customer_address_fax" class="control-label">
				  <c:out value="${customerField.name}"/> <c:if test="${customerField.required}" ><div class="requiredField" align="right"> * </div></c:if>
  				</label>
			  </div>
			  <div class="col-sm-8">
			    <c:choose>
			      <c:when test="${!empty customerField.preValue}">
			        <form:select path="customer.field${customerField.id}" cssClass="form-control">
				      <form:option value="" label="Please Select"/>
			          <c:forTokens items="${customerField.preValue}" delims="," var="dropDownValue">
			            <form:option value="${dropDownValue}" />
			          </c:forTokens>
			        </form:select>
			      </c:when>
			      <c:otherwise>
			        <form:input autocomplete="off" path="customer.field${customerField.id}"  htmlEscape="true" maxlength="255" size="20" cssClass="form-control"/> 
			      </c:otherwise>
			    </c:choose>
		  		<c:if test="${status.error}">
		  		  	<i class="form-control-feedback glyphicon glyphicon-remove"></i>
	            	<small class="help-block error-message">
					    <form:errors path="customer.field${customerField.id}" cssClass="error" />
					</small>    
		  		</c:if>
    		  </div>
			</div>
			</spring:bind>
		  </c:forEach>
	     </c:if>

		
	    
	    <c:if test="${siteConfig['NOTE_ON_REGISTRATION'].value == 'true'}">
		  <spring:bind path="customer.note">
		  <div class="form-group <c:if test="${status.error}">has-feedback has-error</c:if>">
		    <div class="col-sm-4">
			  <label for="customer_note" class="control-label">
			    <fmt:message key="note" />
			  </label>
		    </div>
		    <div class="col-sm-8">
		      <form:textarea path="customer.note" htmlEscape="true" cssClass="registrationNote"/>
		  		<c:if test="${status.error}">
		  		  	<i class="form-control-feedback glyphicon glyphicon-remove"></i>
	            	<small class="help-block error-message">
 					  <form:errors path="customer.note" cssClass="error" />  
					</small>    
		  		</c:if>
			  <div id="noteOnRegistration" class="note" style="color:#FF0000"><fmt:message key="noteOnRegistrationMessage" /></div>
		    </div>
		  </div>
		  </spring:bind>
		</c:if>
		
		
		<spring:bind path="customer.rating1">
		<div class="form-group <c:if test="${status.error}">has-feedback has-error</c:if>">
		  <div class="col-sm-4">
			<label for="customer_address_fax" class="control-label">
			  <fmt:message key="rating1" />
			</label>
		  </div>
		  <div class="col-sm-8">
	  		  <form:select path="customer.rating1" autocomplete="off" id="customer.rating1" class="form-control" htmlEscape="true">
	  			<form:option value=""></form:option>
	  			<c:forTokens items="${siteConfig['CUSTOMER_RATING_1'].value}" delims="," var="type" varStatus="status">
					<form:option value ="${type}"><c:out value ="${type}" /></form:option>
		  		</c:forTokens>
			  </form:select>
    	  	</div>
		</div>
		</spring:bind>
		
		<spring:bind path="customer.rating2">
		<div class="form-group <c:if test="${status.error}">has-feedback has-error</c:if>">
		  <div class="col-sm-4">
			<label for="customer_address_fax" class="control-label">
			  <fmt:message key="rating2" />
			</label>
		  </div>
		  <div class="col-sm-8">
			 <form:select path="customer.rating2" autocomplete="off" id="customer.rating2" class="form-control" htmlEscape="true">
	  			<form:option value=""></form:option>
	  			<c:forTokens items="${siteConfig['CUSTOMER_RATING_2'].value}" delims="," var="type" varStatus="status">
					<form:option value ="${type}"><c:out value ="${type}" /></form:option>
		  		</c:forTokens>
			  </form:select>
    	  	</div>
		</div>
		</spring:bind>
		
		<spring:bind path="customer.newDialingNote.note">
		<div class="form-group <c:if test="${status.error}">has-feedback has-error</c:if>">
		  <div class="col-sm-4">
			<label for="customer_address_fax" class="control-label">
			  <fmt:message key="dialingNotes" />
			</label>
		  </div>
		  <div class="col-sm-8">
			<form:input autocomplete="off" path="customer.newDialingNote.note" id="customer.newDialingNote.note" class="form-control" htmlEscape="true"/>
	  		  <c:if test="${status.error}">
	  		  	<i class="form-control-feedback glyphicon glyphicon-remove"></i>
            	<small class="help-block error-message">
			  		<form:errors path="customer.newDialingNote.note" cssClass="error" />    
				</small>    
	  		  </c:if>
    	  	</div>
		</div>
		</spring:bind>
		
		
		<spring:bind path="customer.salesRepId">
		<div class="form-group <c:if test="${status.error}">has-feedback has-error</c:if>">
		  <div class="col-sm-4">
			<label for="customer_address_fax" class="control-label">
			  <fmt:message key="accountManager" />
			</label>
		  </div>
		  <div class="col-sm-8">
		    <form:select path="customer.salesRepId" class="form-control">
		    	<option value="-1">Please Select:</option>
		      <c:forEach items="${salesRepList}" var="salesRep">         	
		       	<option value="${salesRep.id}">${salesRep.name}</option>
		      </c:forEach>
			</form:select>
		  </div>
		</div>
		</spring:bind>
		
	
	  </div>
	    <div class="col-sm-6">
		  <div id="customerInformation_right_wrapper">
		  </div>
		</div>
	
	  </div>

	</div>
	 <div id="form_buttons">
                <div class="row">
                  <div class="col-sm-2">
                        <div id="buttons_wrapper">
                            <button type="submit" name="submit" id="submit" class="btn btn-default" value="Submit"><fmt:message key="submit" /></button>
                            <button type="submit" name="reset" id="reset" class="btn btn-default" onclick="clearForm()"><fmt:message key="reset" /></button>
                        </div>
                  </div>
                </div>
          </div>

	 
	</form:form>
  </div>
</div>


<script language="JavaScript">
<!--

function clearForm() {

}

//-->
</script>


<c:out value="${model.createAccount.footerHtml}" escapeXml="false"/>

</c:if>
  </tiles:putAttribute>
  </tiles:insertDefinition>


