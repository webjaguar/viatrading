<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="/WEB-INF/tlds/spring.tld" prefix="spring" %>

<tiles:insertDefinition name="${_template}" flush="true">
  <tiles:putAttribute name="content" type="string">
  
<%@ include file="/WEB-INF/jsp/custom/step3-1.jsp" %>  

<c:out value="${model.verifyAccount.headerHtml}" escapeXml="false"/>

<form method="post">
<input type="hidden" name="_page" value="2">
<fieldset id="verify" class="register_fieldset">
<legend><fmt:message key="pleaseVerify" /></legend>
<table border="0" cellpadding="3" cellspacing="1">
  <tr>
    <td><fmt:message key='emailAddress' />:</td>
    <td><c:out value="${customerForm.customer.username}"/></td>
  </tr>
  <tr>
    <td><fmt:message key="firstName" />:</td>
    <td><c:out value="${customerForm.customer.address.firstName}"/></td>
  </tr>
  <tr>
    <td><fmt:message key="lastName" />:</td>
    <td><c:out value="${customerForm.customer.address.lastName}"/></td>
  </tr>
  <tr>
    <td><fmt:message key="company" />:</td>
    <td><c:out value="${customerForm.customer.address.company}"/></td>
  </tr>
  <tr>
    <td><fmt:message key="address" />:</td>
    <td><c:out value="${customerForm.customer.address.addr1}"/> <c:out value="${customerForm.customer.address.addr2}"/></td>
  </tr>
  <tr>
    <td>&nbsp;</td>
    <td><c:out value="${customerForm.customer.address.city}"/>, <c:out value="${customerForm.customer.address.stateProvince}"/>
    		<c:out value="${countries[customerForm.customer.address.country]}" />
    </td>
  </tr>
  <tr>
    <td><fmt:message key="zipCode" />:</td>
    <td><c:out value="${customerForm.customer.address.zip}"/></td>
  </tr>
  <c:if test="${siteConfig['ADDRESS_RESIDENTIAL_COMMERCIAL'].value >= 2}">
    <tr>
    <td><fmt:message key="deliveryType" />:</td>
    <td><c:out value="${customerForm.customer.address.resToString}"/></td>
    </tr>
    <c:if test="${siteConfig['SHIPPING_LTL'].value == 'true'}">
      <tr>
      <td><fmt:message key="liftGateDelivery" />:</td>
      <td><c:out value="${customerForm.customer.address.liftgateToString}"/></td>
      </tr>
    </c:if>
  </c:if>
  <tr>
    <td><fmt:message key="phone" />:</td>
    <td><c:out value="${customerForm.customer.address.phone}"/></td>
  </tr>
  <tr>
    <td><fmt:message key="cellPhone" />:</td>
    <td><c:out value="${customerForm.customer.address.cellPhone}"/></td>
  </tr>
  <tr>
    <td><fmt:message key="fax" />:</td>
    <td><c:out value="${customerForm.customer.address.fax}"/></td>
  </tr>
<c:if test="${gSiteConfig['gTAX_EXEMPTION']}">
  <tr>
    <td style="white-space: nowrap"><fmt:message key="taxId" /> :</td>
	<td><c:out value="${customerForm.customer.taxId}"/></td>
  </tr>
</c:if>  
</table>
</fieldset>
<div align="center" id="buttonWrapper">
  <input type="submit" value="<fmt:message key="change" />" name="_target0">
<c:choose>
  <c:when test="${(gSiteConfig['gSITE_DOMAIN'] == 'besthandbagwholesale.com' || gSiteConfig['gSITE_DOMAIN'] == 'handbagdistributor-temp.com' || gSiteConfig['gSITE_DOMAIN'] == 'wholesalehandbagsusa-temp.com' ) 
  					&& customerForm.customer.address.country == 'US'
  					&& customerForm.customer.address.stateProvince == 'TX'}">
    <input type="submit" value="<fmt:message key="nextStep" />" name="_target3" class="f_button" />
  </c:when>
  <c:otherwise>
    <input type="submit" value="<fmt:message key="register" />" name="_finish" class="f_button finish" />
  </c:otherwise>
</c:choose>
</div>
</form>

<c:out value="${model.verifyAccount.footerHtml}" escapeXml="false"/>

<%@ include file="/WEB-INF/jsp/custom/step3-2.jsp" %>

  </tiles:putAttribute>
</tiles:insertDefinition>
