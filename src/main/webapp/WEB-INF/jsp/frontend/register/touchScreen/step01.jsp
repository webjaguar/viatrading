<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>

<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
 
<link href="https://www.viatrading.com/assets/css/reset.css" rel="stylesheet" type="text/css"/>
<link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css">
<link href="https://www.viatrading.com/assets/css/anythingslider.css" rel="stylesheet" type="text/css"/>
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" integrity="sha384-1q8mTJOASx8j1Au+a5WDVnPi2lkFfwwEAa8hDDdjZlpLegxhjVME1fgjWPGmkzs7" crossorigin="anonymous">
<link href="dv/tsr/css/tsr.css" rel="stylesheet">
<!--[if lte IE 7]>
<link href="assets/css/anythingslider-ie.css" rel="stylesheet" type="text/css" />
<![endif]-->
<script type="text/javascript">

  var _gaq = _gaq || [];
  _gaq.push(['_setAccount', 'UA-1342297-1']);
  _gaq.push(['_trackPageview']);

  (function() {
    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
  })();

</script>
</head>
<body class="lightgreybg"><script type="text/javascript">
//<![CDATA[
try{(function(a){var b="http://",c="www.viatrading.com",d="/cdn-cgi/cl/",e="img.gif",f=new a;f.src=[b,c,d,e].join("")})(Image)}catch(e){}
//]]>
</script>

<!-- NAVBAR 
================================================== -->
        <div class="navbar-wrapper">
            <div class="container">
                <nav class="navbar navbar-inverse navbar-fixed-top">
                    <div class="container">
                        <div class="row">
                            <div class="col-md-1">
                                <a href="touchRegister.jhtm?trackcode=&language=en&target=01&${status.expression}=${status.value}" class="btn btn-back btn-lg btn-default disabled" role="button" style="width:47%; background-color:#005986; color: #fff;">
                                    <div onclick="submitForm('en')">En</div>
                                </a>                            

                                <a href="touchRegister.jhtm?trackcode=&language=es&target=01&${status.expression}=${status.value}" class="btn btn-back btn-lg btn-default" role="button" style="width:47%; margin-left:3%; /*background: #0074af; color: #fff;*/">
                                    <div onclick="submitForm('es')">Es</div>
                                </a>
                            </div>
                            <div class="col-md-2"></div>                            
                            <div class="col-md-2">
                            	<a href="touchRegister.jhtm?language=en&target=1&=" class="btn btn-back btn-lg btn-default" role="button" style="background: #ffee00; color: #000;">Sign Up</a>
                            </div>
                            <div class="col-md-2">
                            	<a href="touchLogin.jhtm?language=en&regEvent=false" class="btn btn-back btn-lg btn-default" role="button">Log In</a>
                            </div>
                            <div class="col-md-2">
                            	<a href="category.jhtm?cid=834&language=en" class="btn btn-back btn-lg btn-default" role="button">Take Survey</a>
                            </div>
                            <div class="col-md-2"></div>
                            <div class="col-md-1"><img class="via-logo" src="dv/tsr/imgs/via-logo-white.png"/></div>
                        </div>
                    </div>
                </nav>
            </div>
        </div>
<!-- NAVBAR END
================================================== -->

<!-- TEMP
================================================== -->
<img width="100%" src="dv/tsr/imgs/welcomescreen-0.jpg"/>
<!-- TEMP END
================================================== -->

<%--<!-- PRESENTATION
================================================== -->
<div>
    <!-- Nav tabs -->
    <ul class="nav nav-tabs" role="tablist">
    </ul>
    <!-- Tab panes -->
    
    <div class="tab-content">
        <div role="tabpanel" class="tab-pane" id="via">...</div>
        <div role="tabpanel" class="tab-pane active" id="events">       
        </div>
        <div role="tabpanel" class="tab-pane" id="warehouse">
            <h1>Warehouse Map</h1>
            <p>Here you will find some of our most profitable merchandise and their location  on the warehouse. You can also find the facilities.</p>
        </div>
        <div role="tabpanel" class="tab-pane" id="shipping">
            <h1>Free Shipping on YELLOW zone</h1>
            <p>Including residential with liftgate within 100 miles from Via Trading.</p>
            <!-- Google map start -->
                <div id="map" style="height:770px; margin-top:30px;"></div>
                <script>
                  // This example creates circles on the map, representing populations in North
                  // America.
            
                  // First, create an object containing LatLng and population for each city.
                  var citymap = {
                    chicago: {
                      center: {lat: 33.9258529, lng: -118.230869},
                      population: 500000
                    },
                    newyork: {
                      center: {lat: 33.9258529, lng: -118.230869},
                      population: 2000000
                    },
                    losangeles: {
                      center: {lat: 33.9258529, lng: -118.230869},
                      population: 4000000
                    }
                  };
            
                  function initMap() {
                    // Create the map.
                    var map = new google.maps.Map(document.getElementById('map'), {
                      zoom: 9,
                      center: {lat: 33.9258529, lng: -118.230869},
                      mapTypeId: google.maps.MapTypeId.ROADMAP
                    });
            
                    // Construct the circle for each value in citymap.
                    // Note: We scale the area of the circle based on the population.
                    for (var city in citymap) {
                      // Add the circle for this city to the map.
                      var cityCircle = new google.maps.Circle({
                        strokeColor: '#FF0000',
                        strokeOpacity: 0.8,
                        strokeWeight: 2,
                        fillColor: '#FF0000',
                        fillOpacity: 0.15,
                        map: map,
                        center: citymap[city].center,
                        radius: Math.sqrt(citymap[city].population) * 100
                      });
                    }
                  }
                </script>
                <script async defer
                    src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAJN_FlTHqSaOTD-lrv71O8K93Hr9oP2kQ&callback=initMap">
                </script>
            <!-- Google map end -->
        </div>
        <div role="tabpanel" class="tab-pane" id="specials">...</div>
        <div role="tabpanel" class="tab-pane" id="policy">
            <h1>Policy & Procedure</h1>
        </div>                
    </div>
</div>
<!-- PRESENTATION End
================================================== -->--%>

<div id="pageContainer">
<!--<div id="header">
<img class="logo" src="https://www.viatrading.com/wp-content/uploads/vialogo-transparent-web.png"/>
<a class="button back-button" href="touchRegister.jhtm">Back</a>
</div>
<div id="content">
<h1><fmt:message key="welcome"/></h1>
<div class="thirdPage floatLeft">
<div><a style="height:190px !important" class="button big-button3" href="touchRegister.jhtm?language=en&target=1&${status.expression}=${status.value}">New Customer<br/>
Sign Up</a></div>
</div>
<div class="thirdPage floatLeft">
<div><a style="height:190px !important" class="button big-button3" href="touchLogin.jhtm?language=en&regEvent=false">Login<br/>
Account</a></div>
</div>
<div class="thirdPage floatLeft">
<div><a style="height:190px !important" class="button big-button3" href="category.jhtm?cid=834&language=en">Take<br/>
Survey</a></div>
</div>
<div class="clearboth"></div>
</div>
<div class="info-wrapper">
 
<div id="virtualTour">
<div id="testimonialSlider">
<ul id="testimonials">
<li>
<div class="slideContent">
<p class="fade"><img src="/tsr-new/slides/eventsEN.jpg"/></p>
</div>
</li>
<li>
<div class="slideContent">
<p class="fade"><img src="/tsr-new/slides/rafflesEN.jpg"/></p>
</div>
</li>
<li>
<div class="slideContent">
<p class="fade"><img src="/tsr-new/slides/socalEN.jpg"/></p>
</div>
</li>
</ul>
</div>
</div>
 
</div>-->
</div>
<script src="https://ajax.aspnetcdn.com/ajax/jquery/jquery-1.7.1.min.js" type="text/javascript"></script>
<script src="https://www.viatrading.com/assets/js/jquery.anythingslider.min.js" type="text/javascript"></script>
<script src="https://www.viatrading.com/assets/js/jquery.anythingslider.fx.min.js" type="text/javascript"></script>
<script type="text/javascript">
$(document).ready(function(e) {
    $('#testimonials').anythingSlider({
		buildArrows : true,
		buildNavigation : false,
		buildStartStop : false,
		autoPlay : true,
		toggleArrows : false,
		delay : 4000,
		resizeContents : false,
		expand : true
	}).anythingSliderFx({
		".panel p.fade":[ "fade", "", "", "linear" ]
	});
});
</script>
</body>
</html>