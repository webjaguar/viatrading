<%@ page contentType="text/html; charset=ISO-8859-1" session="false" %>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="/WEB-INF/tlds/spring.tld" prefix="spring" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>

<link href="https://www.viatrading.com/assets/css/reset.css" rel="stylesheet" type="text/css"/>
<link href="https://www.viatrading.com/assets/css/tsr-New.css" rel="stylesheet" type="text/css"/>
<link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css">
<link href="https://www.viatrading.com/assets/css/anythingslider.css" rel="stylesheet" type="text/css"/>
<%@ page contentType="text/html; charset=ISO-8859-1" session="false" %>
</head>
<body>
<div id="pageContainer">
<div id="header">
<img class="logo" src="https://www.viatrading.com/wp-content/uploads/vialogo-transparent-web.png"/>
<a class="button back-button" href="touchRegister.jhtm?language=es&target=3">Regresar</a>
</div>
<div id="content">
<h1>Direcci�n De Env�o</h1>


<form:form commandName="customerForm" method="post">
<input type="hidden" name="_page" value="6">

<div class="formBox">
<table class="form" width="100%" border="0" cellspacing="0" cellpadding="0">
<tr colspan="5">
  <td align="center" class="text" colspan="5">Please review the information below for accuracy. If you need to go back to correct an entry, please click "back." If the information is correct, please click "register" to complete your registration. 
Thank you!</td>
  <td><div class="title" align="right"></div></td>
</tr>
  <tr>
    <td align="center" class="text" colspan="5"></td>
  </tr>
  <tr align="center" >
    <td colspan="5">
      <table align="center" class="confirm" width="80%" border="0" cellspacing="0" cellpadding="3">
		<tr>
		  <td class="name"><fmt:message key='emailAddress' />:</td>
    	  <td class="value"><c:out value="${customerForm.customer.username}"/></td>
        </tr>
	    <tr>
	      <td class="name"><fmt:message key='customerId' />:</td>
	      <td class="value"><c:out value="${customerForm.customer.cardId}"/></td>
	    </tr>
	    <tr>
		    <td class="name"><fmt:message key="firstName" />:</td>
		    <td class="value"><c:out value="${customerForm.customer.address.firstName}"/></td>
		</tr>
		<tr>
		    <td class="name"><fmt:message key="lastName" />:</td>
		    <td class="value"><c:out value="${customerForm.customer.address.lastName}"/></td>
		</tr>
		<tr>
		    <td class="name"><fmt:message key="company" />:</td>
		    <td class="value"><c:out value="${customerForm.customer.address.company}"/></td>
		</tr>
		<tr>
		    <td class="name"><fmt:message key="address" />:</td>
		    <td class="value"><c:out value="${customerForm.customer.address.addr1}"/></td>
		</tr>
		<tr>
		    <td class="name">&nbsp;</td>
		    <td class="value"><c:out value="${customerForm.customer.address.city}"/>, <c:out value="${customerForm.customer.address.stateProvince}"/>
		    		<c:out value="${customerForm.customer.address.zip}"/> <c:out value="${countries[customerForm.customer.address.country]}" />
		    </td>
		</tr>
		<c:if test="${siteConfig['ADDRESS_RESIDENTIAL_COMMERCIAL'].value >= 2}">
		<tr>
		    <td class="name"><fmt:message key="deliveryType" />:</td>
		    <td class="value"><c:out value="${customerForm.customer.address.resToString}"/></td>
		</tr>
		<c:if test="${siteConfig['SHIPPING_LTL'].value == 'true'}">
		<tr>
		    <td class="name"><fmt:message key="liftGateDelivery" />:</td>
		    <td class="value"><c:out value="${customerForm.customer.address.liftgateToString}"/></td>
		</tr>
		</c:if>
        </c:if>
		<tr>
		    <td class="name"><fmt:message key="phone" />:</td>
		    <td class="value"><c:out value="${customerForm.customer.address.phone}"/></td>
		</tr>
		<tr>
		    <td class="name"><fmt:message key="cellPhone" />:</td>
		    <td class="value"><c:out value="${customerForm.customer.address.cellPhone}"/></td>
		</tr>
		<tr>
		    <td class="name"><fmt:message key="fax" />:</td>
		    <td class="value"><c:out value="${customerForm.customer.address.fax}"/></td>
		</tr>
		<c:if test="${!customerForm.equalAddress}">
		<tr>
		    <td class="name"><fmt:message key="shippingAddress" />:</td>
		    <td class="value"><c:out value="${customerForm.shipping.addr1}"/></td>
		</tr>
		<tr>
		    <td class="name">&nbsp;</td>
		    <td class="value"><c:out value="${customerForm.shipping.city}"/>, <c:out value="${customerForm.shipping.stateProvince}"/>
		    		<c:out value="${customerForm.shipping.zip}"/> <c:out value="${countries[customerForm.shipping.country]}" />
		    </td>
		</tr>
		</c:if>
		<c:if test="${siteConfig['ADDRESS_RESIDENTIAL_COMMERCIAL'].value >= 2}">
		<tr>
		    <td class="name"><fmt:message key="deliveryType" />:</td>
		    <td class="value"><c:out value="${customerForm.shipping.resToString}"/></td>
		</tr>
		<c:if test="${siteConfig['SHIPPING_LTL'].value == 'true'}">
		<tr>
		    <td class="name"><fmt:message key="liftGateDelivery" />:</td>
		    <td class="value"><c:out value="${customerForm.shipping.liftgateToString}"/></td>
		</tr>
		</c:if>
        </c:if>
		<c:if test="${gSiteConfig['gTAX_EXEMPTION']}">
		<tr>
		    <td class="name" style="white-space: nowrap"><fmt:message key="taxId" /> :</td>
			<td class="value"><c:out value="${customerForm.customer.taxId}"/></td>
		</tr>
		</c:if>
		
		<c:forEach items="${customerForm.customerFields}" var="customerField">
		<tr>
		  <td class="name"><c:out value="${customerField.name}"/></td>
		  <td class="value"><c:choose>
		     <c:when test="${!empty customerField.preValue}">
		       <form:select path="customer.field${customerField.id}" disabled="true" cssStyle="background-image: none !important;color:#000000 !important;background-color:#ffffff !important;width:190px;font-size:1em;border:0;">
			       <form:option value="" label="Please Select"/>
		         <c:forTokens items="${customerField.preValue}" delims="," var="dropDownValue">
		           <form:option value="${dropDownValue}" />
		         </c:forTokens>
		       </form:select>
		     </c:when>
		     <c:otherwise>
		       <form:input path="customer.field${customerField.id}"  disabled="true" htmlEscape="true" cssStyle="color:#000000 !important;background-color:#ffffff !important;width:186px;font-size:1em;border:0;" />
		     </c:otherwise>
		    </c:choose>
		  </td>
		</tr>
		<tr style="line-height:7px;">
		  <td></td>
		  <td><form:errors path="customer.field${customerField.id}" cssClass="error" /></td>
		</tr> 
		</c:forEach>	    
	  </table>
	</td>    
  </tr>
  <tr>
    <td colspan="5">
    <div style="padding-top:50px;" align="center">
    <!--  <input class="button" type="submit" value="<fmt:message key="prevStep" />" name="_target5" />  -->
    <input class="button" type="submit" value="Continuar" name="_finish" />
    </div>
    </td>
  </tr> 
  <tr style="line-height:7px;"><td>&nbsp</td></tr> 


<tr>
	<td style="width:390px; !important"><div class="requiredField" align="right"></div></td>
	<td style="width:250px; !important"></td>
	<td style="width:250px; !important"><div class="requiredField" align="right"></div></td>
	<td><input class="buttonInput button back-button" style="width:20%; !important" type="submit" value="Continuar" name="_finish"></td>
</tr>


</table>
</div>
</form:form>


</div>
</div>
<iframe src="sessionKeepAlive.jsp" width="0" height="0" frameborder="0"></iframe>

</body>
</html>
