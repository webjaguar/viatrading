<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="/WEB-INF/tlds/spring.tld" prefix="spring" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<link href="https://www.viatrading.com/assets/css/reset.css" rel="stylesheet" type="text/css"/>
<link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css">
<link href="https://www.viatrading.com/assets/css/anythingslider.css" rel="stylesheet" type="text/css"/>
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" integrity="sha384-1q8mTJOASx8j1Au+a5WDVnPi2lkFfwwEAa8hDDdjZlpLegxhjVME1fgjWPGmkzs7" crossorigin="anonymous">
<link href="dv/tsr/css/tsr.css" rel="stylesheet">
</head>
<body>

<script type="text/javascript">
function liftGateEnable(val) {
	if(val){
		document.getElementById('shipping.liftGate').checked = true;
	}
	else{
		document.getElementById('shipping.liftGate').checked = false;
	}	
}
</script>

<!-- NAVBAR 
================================================== -->
        <div class="navbar-wrapper">
            <div class="container">
                <nav class="navbar navbar-inverse navbar-fixed-top">
                    <div class="container">
                        <div class="row">
	                        <div class="col-md-1">
                            	<a href="touchRegister.jhtm?language=en&target=1" class="btn btn-back btn-lg btn-default" role="button">Back</a>
                            </div>
                            <div class="col-md-10 signup-img"><img src="dv/tsr/imgs/signup-en-2.png"/></div>
                            <div class="col-md-1"><img class="via-logo" src="dv/tsr/imgs/via-logo-white.png"/></div>
                        </div>
                    </div>
                </nav>
            </div>
        </div>
<!-- NAVBAR END
================================================== -->

<div id="pageContainer">
<div id="content">
<h2><fmt:message key="shippingAddress" /></h2>
<p class="page-desc">
<fmt:message key="step2HeaderMessage"/>
</p>
<form:form commandName="customerForm" action="touchRegister.jhtm" method="post">
<input type="hidden" name="_page" value="3">
<form:hidden path="language" />
<form:hidden path="customer.address.country" />
<div class="formBox">

<!-- FORM 
================================================== -->
        <div class="form-wrapper">
            <div class="container">
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label>
                                <fmt:message key="company"/>
                            </label>
                            <form:input path="customer.address.company" type="text"/>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label>
                                <fmt:message key="stateTaxExemptionID"/>
                            </label>
                            <form:input id="customer.taxId" path="customer.taxId" class="input"/>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label>
                                <fmt:message key="mainTypeOfBusiness"/>
                            </label>
                            <form:input id="customer.field2" path="customer.field2" />
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label>
                                <fmt:message key="yearsInBusiness"/>
                            </label>
                            <form:input id="customer.field8" path="customer.field8"/>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label>
                                <fmt:message key="yourWebsiteURL"/>
                            </label>
                            <form:input id="customer.field6" path="customer.field6" />
                        </div>
                    </div>

					<c:if test="${siteConfig['ADDRESS_RESIDENTIAL_COMMERCIAL'].value >= 2}">
                        <div class="col-md-2">
                            <div class="form-group">
                                <span>
                                    <label>
                                        <form:radiobutton onclick="liftGateEnable(true)" style="width:50px !important; height:50px; float:left;" path="shipping.residential" value="true"/>
                                        <span style="margin-left:60px; display:block; margin-top: 15px;">
                                            <fmt:message key="residential" /> <fmt:message key="delivery"/>
                                        </span>
                                    </label>
                                </span>
                            </div>
                        </div>
                        <div class="col-md-2">
                            <div class="form-group">
                                <span>
                                    <label>
                                        <form:radiobutton onclick="liftGateEnable(false)" style="width:50px !important; height:50px; float:left;" path="shipping.residential" value="false"/>
                                        <span style="margin-left:60px; display:block; margin-top: 15px;">
                                            <fmt:message key="commercial" /> <fmt:message key="delivery"/>
                                        </span>
                                    </label>
                                </span>
                            </div>
                        </div>
                        <div class="col-md-2">
                            <div class="form-group">
                                <c:if test="${siteConfig['SHIPPING_LTL'].value == 'true'}">
                                    <span>
                                        <form:checkbox style="width:50px !important; margin-top: -11px;" class="shipping-liftGate1" path="shipping.liftGate" id="shipping.liftGate" value="true"/>
                                        <span style="margin-left:60px; display:block; margin-top: 20px;">
                                            <fmt:message key="liftGateDelivery" />
                                        </span>
                                    </span>
                                </c:if>
                            </div>
                        </div>
					</c:if>
                </div>
                
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label>
                                <div class="requiredField" align="right"> * </div><fmt:message key="address" />
                                <form:errors path="shipping.addr1" class="errorRequiredField"/>
                            </label>
                            <form:input path="shipping.addr1" cssClass="input"/>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label>
                                <div class="requiredField" align="right"> * </div><fmt:message key="address" />
                                <form:errors path="shipping.addr2" class="errorRequiredField"/>
                            </label>
                            <form:input path="shipping.addr2" cssClass="input"/>
                        </div>
                    </div>
                </div>
                
                <div class="row">
                   <div class="col-md-6">
                        <div class="form-group">
                            <label>
                                <div class="requiredField" align="right"> * </div><fmt:message key="city" />
                                <form:errors path="shipping.city" class="errorRequiredField"/>
                            </label>
                            <form:input path="shipping.city" cssClass="input" cssErrorClass="errorField"  />
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label>
                                <div class="requiredField" align="right"> * </div><fmt:message key="stateProvince" />
                                <form:errors path="shipping.stateProvince" class="errorRequiredField"/>
                            </label>
                            <form:select id="state" path="shipping.stateProvince" class="highlight" style="width: 533px; display: none;" disabled="">
                            <option value="">Please Select</option>
                            <c:forEach items="${model.statelist}" var="state">
                                <option value="${state.code}" <c:if test="${state.code == model.cust.address.stateProvince}">selected</c:if>>${state.name}</option>
                            </c:forEach>	
                            </form:select>
                            
                            
                            <form:select id="ca_province2" path="shipping.stateProvince" class="highlight" style="width: 533px; display: none;" disabled="">
                            <option value="">Please Select</option>
                            <c:forEach items="${model.caProvinceList}" var="state">
                                <option value="${state.code}" <c:if test="${state.code == model.cust.address.stateProvince}">selected</c:if>>${state.name}</option>
                            </c:forEach>	
                            </form:select>
                            
                            <form:input id="province" path="shipping.stateProvince" style="display: block;"/>
                        </div>
                    </div>
                  
                </div>

                <div class="row">  
                <div class="col-md-6">
                        <div class="form-group">
                            <label>
                                <div class="requiredField" align="right"> * </div><fmt:message key="zipCode" />
                                <form:errors path="shipping.zip" class="errorRequiredField"/>
                            </label>
                            <form:input path="shipping.zip" cssClass="input" cssErrorClass="errorField"  />
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label>
                                <div class="requiredField" align="right"> * </div><fmt:message key="country"/>
                                <form:errors path="shipping.country" class="errorRequiredField"/>
                            </label>
                            <form:select id="shipping.country" path="shipping.country" name="shipping.country" onchange="toggleStateProvince(this)">
                                <option value="">Please Select</option>
                                <c:forEach items="${model.countrylist}" var="country">
                                	<option value="${country.code}" <c:if test="${country.code == model.cust.address.country}">selected</c:if>>${country.name}</option>
                                </c:forEach>		
							</form:select>
                        </div>
                    </div>
                   
                  
                </div>   
                 <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label>
                                <div class="requiredField" align="right"> * </div><fmt:message key="phone" />
                                <form:errors path="shipping.phone" class="errorRequiredField"/>
                            </label>
                            <form:input path="shipping.phone" cssClass="input" cssErrorClass="errorField" />
                        </div>
                    </div> </div>                                                                            
            </div>
		</div>
<!-- FORM End
================================================== -->        

</div>

<!-- FOOTER 
================================================== -->
        <div class="footer-wrapper">
            <div class="container">
                <div class="row">
                	<div class="col-md-11"></div>
                    <div class="col-md-1">
						<input class="btn btn-lg btn-primary" type="submit" value="Next" name="_target4">
                    </div>
				</div>
            </div>
        </div>
<!-- FOOTER END
================================================== -->

</form:form>

</div>
</div>
<iframe src="sessionKeepAlive.jsp" width="0" height="0" frameborder="0"></iframe>

<script language="JavaScript">
<!--
function toggleStateProvince(el) {
  if (el.value == "US") {
      document.getElementById('state').disabled=false;
      document.getElementById('state').style.display="block";
      document.getElementById('ca_province2').disabled=true;
      document.getElementById('ca_province2').style.display="none";
      document.getElementById('province').disabled=true;
      document.getElementById('province').style.display="none";
      document.getElementById('addressStateProvinceNA').disabled=true;
      document.getElementById('stateProvinceNA').style.display="none";
  } else if (el.value == "CA") {
      document.getElementById('state').disabled=true;
      document.getElementById('state').style.display="none";
      document.getElementById('ca_province2').disabled=false;
      document.getElementById('ca_province2').style.display="block";
      document.getElementById('province').disabled=true;
      document.getElementById('province').style.display="none";
      document.getElementById('addressStateProvinceNA').disabled=true;
      document.getElementById('stateProvinceNA').style.display="none";
  } else {
      document.getElementById('state').disabled=true;
      document.getElementById('state').style.display="none";
      document.getElementById('ca_province2').disabled=true;
      document.getElementById('ca_province2').style.display="none";
      document.getElementById('province').disabled=false;
      document.getElementById('province').style.display="block";
      document.getElementById('addressStateProvinceNA').disabled=false;
      document.getElementById('stateProvinceNA').style.display="block";
  }
}
toggleStateProvince(document.getElementById('shipping.country'));
//-->
</script>

</body>
</html>