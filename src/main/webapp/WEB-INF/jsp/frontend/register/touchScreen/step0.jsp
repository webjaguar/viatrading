<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<%@ page contentType="text/html; charset=ISO-8859-1" session="false" %>
<link href="https://www.viatrading.com/assets/css/reset.css" rel="stylesheet" type="text/css"/>
<link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css">
<link href="https://www.viatrading.com/assets/css/anythingslider.css" rel="stylesheet" type="text/css"/>
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" integrity="sha384-1q8mTJOASx8j1Au+a5WDVnPi2lkFfwwEAa8hDDdjZlpLegxhjVME1fgjWPGmkzs7" crossorigin="anonymous">
<link href="dv/tsr/css/tsr.css" rel="stylesheet">
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/> 
<meta name="viewport" content="width=device-width, initial-scale=1"> 
<!--[if lte IE 7]>
<link href="assets/css/anythingslider-ie.css" rel="stylesheet" type="text/css" />
<![endif]-->
<script type="text/javascript">

  var _gaq = _gaq || [];	
  _gaq.push(['_setAccount', 'UA-1342297-1']);
  _gaq.push(['_trackPageview']);

  (function() {
    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
  })();

</script>
</head>
<body>

<!-- NAVBAR 
================================================== -->
        <div class="navbar-wrapper">
            <div class="container">
                <nav class="navbar navbar-inverse navbar-fixed-top">
                    <div class="container">
                        <div class="row">
                            <div class="col-md-11"></div>
                            <div class="col-md-1"><img class="via-logo" src="dv/tsr/imgs/via-logo-white.png"/></div>
                        </div>
                    </div>
                </nav>
            </div>
        </div>
<!-- NAVBAR END
================================================== -->
        
<div id="pageContainer">

<script language="JavaScript">
function submitForm(language) {
	document.getElementById('language').value=language;
	document.customerForm.submit();
}
</script>

<script type="text/javascript">

  var _gaq = _gaq || [];
  _gaq.push(['_setAccount', 'UA-1342297-1']);
  _gaq.push(['_trackPageview', '/tsr/index.html']);

  (function() {
    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
  })();

</script>
</head>
<body>
<script type="text/javascript">
//<![CDATA[
try{(function(a){var b="http://",c="www.viatrading.com",d="/cdn-cgi/cl/",e="img.gif",f=new a;f.src=[b,c,d,e].join("")})(Image)}catch(e){}
//]]>
</script>

<form:form commandName="customerForm" name="customerForm" action="touchRegister.jhtm" method="post">
<input type="hidden" name="_page" value="0">
<input type="hidden" id="language" name="language" value="en">
<input type="hidden" id="target" name="_target3">

<!-- Content 
================================================== -->
		<div class="container" style="margin-top:250px;">
            <div class="row">
                <div class="col-md-2"></div>
                <div class="col-md-4">
                	<div class="bigbtn">
                        <a href="touchRegister.jhtm?trackcode=&language=en&target=01&${status.expression}=${status.value}">
                            <div class="bigbtntitle" onclick="submitForm('en')">En</div>
                            <h1>English</h1>
                        </a>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="bigbtn">
                        <a href="touchRegister.jhtm?trackcode=&language=es&target=01&${status.expression}=${status.value}">
                            <div class="bigbtntitle" onclick="submitForm('es')">Es</div>
                            <h1>Espa&ntilde;ol</h1>
                        </a>
                    </div>
                </div>
                <div class="col-md-2"></div>                                
            </div>
        </div>
<!-- Content END
================================================== -->        

</form:form>
</body>
</html>

<iframe src="sessionKeepAlive.jsp" width="0" height="0" frameborder="0"></iframe>