<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="/WEB-INF/tlds/spring.tld" prefix="spring" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>

<tiles:insertDefinition name="${_template}" flush="true">
  <tiles:putAttribute name="content" type="string">
<link rel="stylesheet" href="https://www.viatrading.com/assets/touchscreen/register/${customerForm.language}/stylesheet_5.css" type="text/css"/>

<form:form commandName="customerForm" method="post">
<input type="hidden" name="_page" value="6">

<div class="formBox">
<table class="form" width="100%" border="0" cellspacing="0" cellpadding="0">
<tr>
  <td><div class="title" align="left"><fmt:message key="thankYou" /></div></td>
  <td><div class="title" align="right"><a href="javascript:window.print()"><fmt:message key="print" /></a></div></td>
</tr>
  <tr>
    <td align="center" class="text" colspan="2"></td>
  </tr>
  <tr align="center" >
    <td colspan="2">
      <table align="center" class="confirm" width="80%" border="0" cellspacing="0" cellpadding="3">
		<tr>
		  <td class="name"><fmt:message key='emailAddress' />:</td>
    	  <td class="value"><c:out value="${customerForm.customer.username}"/></td>
        </tr>
	    <tr>
	      <td class="name"><fmt:message key='customerId' />:</td>
	      <td class="value"><c:out value="${customerForm.customer.cardId}"/></td>
	    </tr>
	    <tr>
		    <td class="name"><fmt:message key="firstName" />:</td>
		    <td class="value"><c:out value="${customerForm.customer.address.firstName}"/></td>
		</tr>
		<tr>
		    <td class="name"><fmt:message key="lastName" />:</td>
		    <td class="value"><c:out value="${customerForm.customer.address.lastName}"/></td>
		</tr>
		<tr>
		    <td class="name"><fmt:message key="company" />:</td>
		    <td class="value"><c:out value="${customerForm.customer.address.company}"/></td>
		</tr>
		<tr>
		    <td class="name"><fmt:message key="address" />:</td>
		    <td class="value"><c:out value="${customerForm.customer.address.addr1}"/></td>
		</tr>
		<tr>
		    <td class="name">&nbsp;</td>
		    <td class="value"><c:out value="${customerForm.customer.address.city}"/>, <c:out value="${customerForm.customer.address.stateProvince}"/>
		    		<c:out value="${customerForm.customer.address.zip}"/> <c:out value="${countries[customerForm.customer.address.country]}" />
		    </td>
		</tr>
		<c:if test="${siteConfig['ADDRESS_RESIDENTIAL_COMMERCIAL'].value >= 2}">
		<tr>
		    <td class="name"><fmt:message key="deliveryType" />:</td>
		    <td class="value"><c:out value="${customerForm.customer.address.resToString}"/></td>
		</tr>
		<c:if test="${siteConfig['SHIPPING_LTL'].value == 'true'}">
		<tr>
		    <td class="name"><fmt:message key="liftGateDelivery" />:</td>
		    <td class="value"><c:out value="${customerForm.customer.address.liftgateToString}"/></td>
		</tr>
		</c:if>
        </c:if>
		<tr>
		    <td class="name"><fmt:message key="phone" />:</td>
		    <td class="value"><c:out value="${customerForm.customer.address.phone}"/></td>
		</tr>
		<tr>
		    <td class="name"><fmt:message key="cellPhone" />:</td>
		    <td class="value"><c:out value="${customerForm.customer.address.cellPhone}"/></td>
		</tr>
		<tr>
		    <td class="name"><fmt:message key="fax" />:</td>
		    <td class="value"><c:out value="${customerForm.customer.address.fax}"/></td>
		</tr>
		<c:if test="${!customerForm.equalAddress}">
		<tr>
		    <td class="name"><fmt:message key="shippingAddress" />:</td>
		    <td class="value"><c:out value="${customerForm.shipping.addr1}"/></td>
		</tr>
		<tr>
		    <td class="name">&nbsp;</td>
		    <td class="value"><c:out value="${customerForm.shipping.city}"/>, <c:out value="${customerForm.shipping.stateProvince}"/>
		    		<c:out value="${customerForm.shipping.zip}"/> <c:out value="${countries[customerForm.shipping.country]}" />
		    </td>
		</tr>
		</c:if>
		<c:if test="${siteConfig['ADDRESS_RESIDENTIAL_COMMERCIAL'].value >= 2}">
		<tr>
		    <td class="name"><fmt:message key="deliveryType" />:</td>
		    <td class="value"><c:out value="${customerForm.shipping.resToString}"/></td>
		</tr>
		<c:if test="${siteConfig['SHIPPING_LTL'].value == 'true'}">
		<tr>
		    <td class="name"><fmt:message key="liftGateDelivery" />:</td>
		    <td class="value"><c:out value="${customerForm.shipping.liftgateToString}"/></td>
		</tr>
		</c:if>
        </c:if>
		<c:if test="${gSiteConfig['gTAX_EXEMPTION']}">
		<tr>
		    <td class="name" style="white-space: nowrap"><fmt:message key="taxId" /> :</td>
			<td class="value"><c:out value="${customerForm.customer.taxId}"/></td>
		</tr>
		</c:if>
		
		<c:forEach items="${customerForm.customerFields}" var="customerField">
		<tr>
		  <td class="name"><c:out value="${customerField.name}"/></td>
		  <td class="value"><c:choose>
		     <c:when test="${!empty customerField.preValue}">
		       <form:select path="customer.field${customerField.id}" disabled="true" cssStyle="background-image: none !important;color:#000000 !important;background-color:#ffffff !important;width:190px;font-size:1em;border:0;">
			       <form:option value="" label="Please Select"/>
		         <c:forTokens items="${customerField.preValue}" delims="," var="dropDownValue">
		           <form:option value="${dropDownValue}" />
		         </c:forTokens>
		       </form:select>
		     </c:when>
		     <c:otherwise>
		       <form:input path="customer.field${customerField.id}"  disabled="true" htmlEscape="true" cssStyle="color:#000000 !important;background-color:#ffffff !important;width:186px;font-size:1em;border:0;" />
		     </c:otherwise>
		    </c:choose>
		  </td>
		</tr>
		<tr style="line-height:7px;">
		  <td></td>
		  <td><form:errors path="customer.field${customerField.id}" cssClass="error" /></td>
		</tr> 
		</c:forEach>	    
	  </table>
	</td>    
  </tr>
  <tr>
    <td colspan="2">
    <div style="padding-top:50px;" align="center">
    <!--  <input class="button" type="submit" value="<fmt:message key="prevStep" />" name="_target5" />  -->
    <input class="button" type="submit" value="<fmt:message key="startOver" />" name="_finish" />
    </div>
    </td>
  </tr> 
</table>
</div>
</form:form>

<iframe src="sessionKeepAlive.jsp" width="0" height="0" frameborder="0"></iframe>

  </tiles:putAttribute>
</tiles:insertDefinition>
