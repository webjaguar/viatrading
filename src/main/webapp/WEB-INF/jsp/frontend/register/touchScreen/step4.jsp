<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="/WEB-INF/tlds/spring.tld" prefix="spring" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<link href="https://www.viatrading.com/assets/css/reset.css" rel="stylesheet" type="text/css"/>
<link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css">
<link href="https://www.viatrading.com/assets/css/anythingslider.css" rel="stylesheet" type="text/css"/>
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" integrity="sha384-1q8mTJOASx8j1Au+a5WDVnPi2lkFfwwEAa8hDDdjZlpLegxhjVME1fgjWPGmkzs7" crossorigin="anonymous">
<link href="dv/tsr/css/tsr.css" rel="stylesheet">
</head>
<body class="rinfo">

<!-- NAVBAR 
================================================== -->
        <div class="navbar-wrapper">
            <div class="container">
                <nav class="navbar navbar-inverse navbar-fixed-top">
                    <div class="container">
                        <div class="row">
	                        <div class="col-md-1">
                            	<a href="touchRegister.jhtm?language=en&target=3" class="btn btn-back btn-lg btn-default" role="button">Back</a>
                            </div>
                            <div class="col-md-10 signup-img"><img src="dv/tsr/imgs/signup-en-4.png"/></div>
                            <div class="col-md-1"><img class="via-logo" src="dv/tsr/imgs/via-logo-white.png"/></div>
                        </div>
                    </div>
                </nav>
            </div>
        </div>
<!-- NAVBAR END
================================================== -->

<div id="pageContainer">

<div id="content">
<h2><fmt:message key='reviewInformation' /></h2>

<tr>
	<td align="center" class="text" colspan="4">
    <p class="page-desc">Please review the information below for accuracy. If you need to go back to correct an entry, please click "back." If the information is correct, please click "register" to complete your registration. 
Thank you!.</p>
	</td>
</tr>

<form:form commandName="customerForm" method="post">
<input type="hidden" name="_page" value="7">
<input type="hidden" name="target" value="7">
<form:hidden path="language" />

<div class="formBox hlines">

<!-- FORM 
================================================== -->
        <div class="form-wrapper">
            <div class="container">

                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
							<fmt:message key="firstName" />:
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
							<c:out value="${customerForm.customer.address.firstName}"/>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
							<fmt:message key="lastName" />:
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
							<c:out value="${customerForm.customer.address.lastName}"/>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
							<fmt:message key="company" />:
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
							<c:out value="${customerForm.customer.address.company}"/>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
							<fmt:message key="address" />:
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
							<c:out value="${customerForm.customer.address.addr1}"/> 
							<c:if test="${customerForm.customer.address.addr2 != null}"><br>
							<c:out value="${customerForm.customer.address.addr2}"/>
							</c:if>
                        </div>
                    </div>
                     
                      
							
                 
                    
                </div>

                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">

                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
							<c:out value="${customerForm.customer.address.city}"/>, <c:out value="${customerForm.customer.address.stateProvince}"/>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">

                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
							<c:out value="${customerForm.customer.address.zip}"/>, <c:out value="${countryCodes[customerForm.customer.address.country]}" />
                        </div>
                    </div>
                </div>

                <c:if test="${siteConfig['ADDRESS_RESIDENTIAL_COMMERCIAL'].value >= 2}">
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group name">
                                <fmt:message key="deliveryType" />:
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group value">
                                <c:out value="${customerForm.customer.address.resToString}"/>
                            </div>
                        </div>
                    </div>
                    <c:if test="${siteConfig['SHIPPING_LTL'].value == 'true'}">
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group name">
                                    <fmt:message key="liftGateDelivery" />:
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group value">
                                    <c:out value="${customerForm.customer.address.liftgateToString}"/>
                                </div>
                            </div>
                        </div>
                    </c:if>
                </c:if>

                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
							<fmt:message key="phone" />:
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
							<c:out value="${customerForm.customer.address.phone}"/>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
							<fmt:message key="cellPhone" />:
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
							<c:out value="${customerForm.customer.address.cellPhone}"/>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
							<fmt:message key="fax" />:
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
							<c:out value="${customerForm.customer.address.fax}"/>
                        </div>
                    </div>
                </div>

                <c:if test="${!customerForm.equalAddress}">
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                				<fmt:message key="shippingAddress" />:
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                				<c:out value="${customerForm.shipping.addr1}"/>
                            </div>
                        </div>
                    </div>
                    
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                				<c:out value="${customerForm.shipping.city}"/>, <c:out value="${customerForm.shipping.stateProvince}"/>
		    					<c:out value="${customerForm.shipping.zip}"/> <c:out value="${countries[customerForm.shipping.country]}" />
                            </div>
                        </div>
                    </div>                
                </c:if>

				<c:if test="${gSiteConfig['gTAX_EXEMPTION']}">
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
    							<fmt:message key="taxId" /> :
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
    							<c:out value="${customerForm.customer.taxId}"/>
                            </div>
                        </div>
                    </div>
				</c:if>                

                <c:forEach items="${customerForm.customerFields}" var="customerField">
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <c:out value="${customerField.name}"/>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <c:choose>
                                    <c:when test="${!empty customerField.preValue}">
                                    <form:select path="customer.field${customerField.id}" disabled="true">
                                        <form:option value="" label="Please Select"/>
                                        <c:forTokens items="${customerField.preValue}" delims="," var="dropDownValue">
                                            <form:option value="${dropDownValue}" />
                                        </c:forTokens>
                                    </form:select>
                                    </c:when>
                                    <c:otherwise>
                                        <form:input path="customer.field${customerField.id}"  disabled="true" htmlEscape="true"/>
                                    </c:otherwise>
                                </c:choose>
                            </div>
                        </div>
                    </div>
                </c:forEach>

            </div>
        </div>

<!-- FORM End
================================================== -->

</div>

<!-- FOOTER 
================================================== -->
        <div class="footer-wrapper">
            <div class="container">
                <div class="row">
                	<div class="col-md-11"></div>
                    <div class="col-md-1">
						<input class="btn btn-lg btn-primary" type="submit" value="Next" name="_target6">
                    </div>
				</div>
            </div>
        </div>
<!-- FOOTER END
================================================== -->

</form:form>

</div>
</div>
<iframe src="sessionKeepAlive.jsp" width="0" height="0" frameborder="0"></iframe>

</body>
</html>