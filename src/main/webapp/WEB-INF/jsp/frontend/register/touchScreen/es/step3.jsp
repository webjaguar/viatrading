<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="/WEB-INF/tlds/spring.tld" prefix="spring"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>

<head>
<%@ page language="java" contentType="text/html; charset=UTF-8"	pageEncoding="UTF-8"%>
<link href="https://www.viatrading.com/assets/css/reset.css" rel="stylesheet" type="text/css" />
<link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css">
<link href="https://www.viatrading.com/assets/css/anythingslider.css" rel="stylesheet" type="text/css" />
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" integrity="sha384-1q8mTJOASx8j1Au+a5WDVnPi2lkFfwwEAa8hDDdjZlpLegxhjVME1fgjWPGmkzs7" crossorigin="anonymous">
<link href="dv/tsr/css/tsr.css" rel="stylesheet">
</head>
<body>

<!-- NAVBAR 
================================================== -->
	<div class="navbar-wrapper">
		<div class="container">
			<nav class="navbar navbar-inverse navbar-fixed-top">
				<div class="container">
					<div class="row">
						<div class="col-md-1">
							<a href="touchRegister.jhtm?language=es&target=2"
								class="btn btn-back btn-lg btn-default" role="button">Atrás</a>
						</div>
						<div class="col-md-10 signup-img">
							<img
								src="dv/tsr/imgs/signup-sp-3.png" />
						</div>
						<div class="col-md-1">
							<img class="via-logo"
								src="dv/tsr/imgs/via-logo-white.png" />
						</div>
					</div>
				</div>
			</nav>
		</div>
	</div>
<!-- NAVBAR END
================================================== -->

	<div id="pageContainer">
		<div id="content">
			<h2>
				Más información
			</h2>

			<p class="page-desc">
				<fmt:message key="step3HeaderMessage" />
			</p>

			<form:form commandName="customerForm" method="post">
				<input type="hidden" name="_page" value="4">
				<form:hidden path="language" />

				<div class="formBox">

<!-- FORM 
================================================== -->
					<div class="form-wrapper">
						<div class="container">

							<div class="row">
								<c:forEach items="${customerForm.customerFields}"
									var="customerField">
									<div class="col-md-6">
										<div class="form-group">
											<label>
												<div class="requiredField" align="right">
													<c:if test="${customerField.required}"> * </c:if>
												</div> <c:choose>

													<c:when test="${customerField.id==1}">
														<c:out value="Cómo supo de nosotros?" />
													</c:when>

													<c:when test="${customerField.id==2}">
														<c:out value="Tipo De Negocio" />
													</c:when>

													<c:when test="${customerField.id==3}">
														<c:out
															value="Tiene Gerente de Cuenta/ Ha Hablado Con Alguien De Nuestro Equipo Anteriormente? Con Quien?" />
													</c:when>

													<c:when test="${customerField.id==4}">
														<c:out value="Ha Comprado Con Via Trading Anteriormente?" />
													</c:when>

													<c:when test="${customerField.id==5}">
														<c:out value="Gerente De Cuentas Original" />
													</c:when>

													<c:when test="${customerField.id==6}">
														<c:out value="Tu Dirección Web" />
													</c:when>

													<c:when test="${customerField.id==7}">
														<c:out value="LEAD LIST" />
													</c:when>

													<c:when test="${customerField.id==8}">
														<c:out value="Años en el negocio" />
													</c:when>

													<c:when test="${customerField.id==9}">
														<c:out
															value="Ha Comprado Devoluciones De Clientes Anteriormente?" />
													</c:when>

												</c:choose> <form:errors path="customer.field${customerField.id}"
													cssClass="error" />
											</label>
											<c:choose>
												<c:when test="${!empty customerField.preValue}">
													<form:select path="customer.field${customerField.id}"
														class="select" cssErrorClass="errorField">
														<form:option value="" label="Please Select" />
														<c:forTokens items="${customerField.preValue}" delims=","
															var="dropDownValue">
															<form:option value="${dropDownValue}" />
														</c:forTokens>
													</form:select>
												</c:when>
												<c:otherwise>
													<form:input path="customer.field${customerField.id}"
														htmlEscape="true" cssClass="input"
														cssErrorClass="errorField" />
												</c:otherwise>
											</c:choose>
										</div>
									</div>
								</c:forEach>
							</div>

							<div class="row">
								<div class="col-md-6">
									<div class="form-group">
										<label>
											<div class="requiredField" align="right"></div> <fmt:message
												key='amazonName' />
										</label>
										<!-- input field -->
										<form:input path="customer.amazonName" htmlEscape="true"
											/>
										<form:errors path="customer.amazonName" cssClass="error" />
										<!-- end input field -->
									</div>
								</div>
								<div class="col-md-6">
									<div class="form-group">
										<label>
											<div class="requiredField" align="right"></div>
											<fmt:message key='ebayName' />:
										</label>
										<!-- input field -->
										<form:input path="customer.ebayName" htmlEscape="true"
											/>
										<form:errors path="customer.ebayName" cssClass="error" />
										<!-- end input field -->
									</div>
								</div>
							</div>

						</div>
					</div>
<!-- FORM End
================================================== -->

				</div>

<!-- FOOTER 
================================================== -->
                <div class="footer-wrapper">
                    <div class="container">
                        <div class="row">
                            <div class="col-md-11"></div>
                            <div class="col-md-1">
                                <input class="btn btn-lg btn-primary" type="submit" value="Siguiente"
                                    name="_target5">
                            </div>
                        </div>
                    </div>
                </div>
<!-- FOOTER END
================================================== -->

			</form:form>

		</div>
	</div>
	<iframe src="sessionKeepAlive.jsp" width="0" height="0" frameborder="0"></iframe>

</body>
</html>