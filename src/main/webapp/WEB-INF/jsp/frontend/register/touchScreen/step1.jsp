<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="/WEB-INF/tlds/spring.tld" prefix="spring" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<script type="text/javascript" src="assets/template6/js/libs/jquery.min.js"></script>
<script type="text/javascript" src="assets/responsive/js/libs/jquery.mask.js"></script>
<link href="https://www.viatrading.com/assets/css/reset.css" rel="stylesheet" type="text/css"/>
<link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css">
<link href="https://www.viatrading.com/assets/css/anythingslider.css" rel="stylesheet" type="text/css"/>
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" integrity="sha384-1q8mTJOASx8j1Au+a5WDVnPi2lkFfwwEAa8hDDdjZlpLegxhjVME1fgjWPGmkzs7" crossorigin="anonymous">
<link href="https://www.viatrading.com/dv/tsr/css/tsr.css" rel="stylesheet">
<script type="text/javascript">

  var _gaq = _gaq || [];
  _gaq.push(['_setAccount', 'UA-1342297-1']);
  _gaq.push(['_trackPageview']);

  (function() {
    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
  })();

</script>
<script type="text/javascript">
function validatePhone() {
  var country = document.getElementById('customer.address.country').value;	
  if(country  == 'US' || country  == 'CA' ){
	  $("#phone_num").mask("(999)999-9999");	
	} else {
	  $("#phone_num").unmask("(999)999-9999");	
	}
}

function validateCellPhone() {
 var country =  document.getElementById('customer.address.country').value;
 if(country  == 'US' || country  == 'CA' ){
	$("#cell_phone_num").mask("(999)999-9999");	
 } else {
	 $("#cell_phone_num").unmask("(999)999-9999");	
 }
}

//add star if only the country selected was USA
function selectCountry(e) {
	/* console.log(document.getElementById("customer_address_country")); */
	var index = document.getElementById("customer.address.country").selectedIndex;
	var option = document.getElementById("customer.address.country").options;
	var value = option[index];	
	if(value.text !== "United States" && value.text !== "Canada" && value.text !== "Please Select") {
		document.getElementById("zipcodeStar").innerHTML = "";
	}else{
		document.getElementById("zipcodeStar").innerHTML = "*";

	}
}

window.onload = function () { selectCountry(); }
</script>
</head>
<body><script type="text/javascript">
//<![CDATA[
try{(function(a){var b="http://",c="www.viatrading.com",d="/cdn-cgi/cl/",e="img.gif",f=new a;f.src=[b,c,d,e].join("")})(Image)}catch(e){}
//]]>
</script>

<style>
.form-group.floatcheckbox2 {
    position: absolute;
    right: 205px;
    top: 6px;
    width: 200px;
}
</style>

<!-- NAVBAR 
================================================== -->
        <div class="navbar-wrapper">
            <div class="container">
                <nav class="navbar navbar-inverse navbar-fixed-top">
                    <div class="container">
                        <div class="row">
	                        <div class="col-md-1">
                            	<a href="touchRegister.jhtm?trackcode=&language=en&target=01" class="btn btn-back btn-lg btn-default" role="button">Back</a>
                            </div>
                            <div class="col-md-10 signup-img"><img src="dv/tsr/imgs/signup-en-1.png"/></div>
                            <div class="col-md-1"><img class="via-logo" src="dv/tsr/imgs/via-logo-white.png"/></div>
                        </div>
                    </div>
                </nav>
            </div>
        </div>
<!-- NAVBAR END
================================================== -->

<div id="pageContainer">

<div id="content">
<h2>General Information</h2>
<p class="page-desc">
<fmt:message key="step1HeaderMessage"/>
</p>
 
<script type="text/javascript">

function automateCityState(zipCode) {
	var request = new Request.JSON({
		url: "/jsonZipCode.jhtm?zipCode="+zipCode,
		onRequest: function() { 
		},
		onComplete: function(jsonObj) {
			document.getElementById('customer.address.city').value = jsonObj.city;
			document.getElementById('state').value = jsonObj.stateAbbv;
			$$('.highlight').each(function(el) {
				var end = el.getStyle('background-color');
				end = (end == 'transparent') ? '#fff' : end;
				var myFx = new Fx.Tween(el, {duration: 500, wait: false});
				myFx.start('background-color', '#f00', end);
			});
		}
	}).send();
}
</script>

<script type="text/javascript">
function cellPhoneRegister() {
	if(!document.getElementById('emailNotify2').checked){
		document.getElementById('customer.username').disabled = false;
		document.getElementById('customer.username').style.backgroundColor = "#EEE";
		document.getElementById('customer.emailNotify1').checked = true;
	}
	else{
		document.getElementById('customer.username').disabled = true;
		document.getElementById('customer.username').style.backgroundColor = "#999";
		document.getElementById('customer.emailNotify1').checked = false;
	}	
}
</script>

<script type="text/javascript">

var zChar = new Array(' ', '(', ')', '-', '.');
var maxphonelength = 13;
var phonevalue1;
var phonevalue2;
var cursorposition;

function ParseForNumber1(object){
phonevalue1 = ParseChar(object.value, zChar);
}
function ParseForNumber2(object){
phonevalue2 = ParseChar(object.value, zChar);
}

function backspacerUP(object,e) { 
if(e){ 
e = e 
} else {
e = window.event 
} 
if(e.which){ 
var keycode = e.which 
} else {
var keycode = e.keyCode 
}

ParseForNumber1(object)

if(keycode >= 48){
ValidatePhone(object)
}
}

function backspacerDOWN(object,e) { 
if(e){ 
e = e 
} else {
e = window.event 
} 
if(e.which){ 
var keycode = e.which 
} else {
var keycode = e.keyCode 
}
ParseForNumber2(object)
} 

function GetCursorPosition(){

var t1 = phonevalue1;
var t2 = phonevalue2;
var bool = false
for (i=0; i<t1.length; i++)
{
if (t1.substring(i,1) != t2.substring(i,1)) {
if(!bool) {
cursorposition=i
bool=true
}
}
}
}

function ValidatePhone(object){

var p = phonevalue1

p = p.replace(/[^\d]*/gi,"")

if (p.length < 3) {
object.value=p
} else if(p.length==3){
pp=p;
d4=p.indexOf('(')
d5=p.indexOf(')')
if(d4==-1){
pp="("+pp;
}
if(d5==-1){
pp=pp+")";
}
object.value = pp;
} else if(p.length>3 && p.length < 7){
p ="(" + p; 
l30=p.length;
p30=p.substring(0,4);
p30=p30+")"

p31=p.substring(4,l30);
pp=p30+p31;

object.value = pp; 

} else if(p.length >= 7){
p ="(" + p; 
l30=p.length;
p30=p.substring(0,4);
p30=p30+")"

p31=p.substring(4,l30);
pp=p30+p31;

l40 = pp.length;
p40 = pp.substring(0,8);
p40 = p40 + "-"

p41 = pp.substring(8,l40);
ppp = p40 + p41;

object.value = ppp.substring(0, maxphonelength);
}

GetCursorPosition()

if(cursorposition >= 0){
if (cursorposition == 0) {
cursorposition = 2
} else if (cursorposition <= 2) {
cursorposition = cursorposition + 1
} else if (cursorposition <= 5) {
cursorposition = cursorposition + 2
} else if (cursorposition == 6) {
cursorposition = cursorposition + 2
} else if (cursorposition == 7) {
cursorposition = cursorposition + 4
e1=object.value.indexOf(')')
e2=object.value.indexOf('-')
if (e1>-1 && e2>-1){
if (e2-e1 == 4) {
cursorposition = cursorposition - 1
}
}
} else if (cursorposition < 11) {
cursorposition = cursorposition + 3
} else if (cursorposition == 11) {
cursorposition = cursorposition + 1
} else if (cursorposition >= 12) {
cursorposition = cursorposition
}

var txtRange = object.createTextRange();
txtRange.moveStart( "character", cursorposition);
txtRange.moveEnd( "character", cursorposition - object.value.length);
txtRange.select();
}

}

function ParseChar(sStr, sChar)
{
if (sChar.length == null) 
{
zChar = new Array(sChar);
}
else zChar = sChar;

for (i=0; i<zChar.length; i++)
{
sNewStr = "";

var iStart = 0;
var iEnd = sStr.indexOf(sChar[i]);

while (iEnd != -1)
{
sNewStr += sStr.substring(iStart, iEnd);
iStart = iEnd + 1;
iEnd = sStr.indexOf(sChar[i], iStart);
}
sNewStr += sStr.substring(sStr.lastIndexOf(sChar[i]) + 1, sStr.length);

sStr = sNewStr;
}

return sNewStr;
}
</script>

<form:form commandName="customerForm" id="customerForm" action="touchRegister.jhtm" method="post">
<input type="hidden" name="target" value="2">
<input type="hidden" name="_page" value="2">
<input type="hidden" name="userid" id="userid" value="${model.userid}">
<div class="formBox">

<!-- FORM 
================================================== -->
        <div class="form-wrapper">
            <div class="container">
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label>
                                <div class="requiredField" align="right"> * </div><fmt:message key="firstName"/>
                                <form:errors path="customer.address.firstName" class="errorRequiredField"/>
                            </label>
                            <form:input path="customer.address.firstName" type="text"/>
                        </div>
                    </div>
                    <div class="col-md-6">
                    	<div class="form-group">
                            <label>
                                <div class="requiredField" align="right"> * </div><fmt:message key="lastName"/>
                                <form:errors path="customer.address.lastName" class="errorRequiredField"/>
                            </label>
                            <form:input path="customer.address.lastName" type="text"/>
						</div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-6">
                    	<div class="form-group">
                            <label>
                                <div class="requiredField" align="right"> * </div><fmt:message key="country"/>
                                <form:errors path="customer.address.country" class="errorRequiredField"/>
                            </label>
                            <form:select id="customer.address.country" path="customer.address.country" name="customer.address.country" onchange="selectCountry()">
                                <option value="">Please Select</option>
                                <c:forEach items="${model.countrylist}" var="country">
                                    <option value="${country.code}" <c:if test="${country.code == countrySelected}">selected</c:if>>${country.name}</option>
                                </c:forEach>		
                            </form:select>
                        </div>
                    </div>
                    <div class="col-md-6">
                    	<div class="form-group">
                            <label>
                                <fmt:message key="language"/>
                            </label>
                            <form:select id="customer.languageCode" path="customer.languageCode">
                                <option value="en" selected="selected">English</option>
                                <option value="es">Spanish</option>
                            </form:select>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-6">
                    	<div class="form-group">
                            <label>
                                <div id="star" class="requiredField" align="right"> * </div><fmt:message key="createPassword"/>
                                <form:errors path="customer.password" class="errorRequiredField"/>
                            </label>
                            <form:input path="customer.password" type="password" onblur="passwordAtLeastFiveCharacters();"/>
                        </div>
                    </div>
                    <div class="col-md-6">
                    	<div class="form-group">
                            <label>
                                <div class="requiredField" align="right"> * </div><fmt:message key="confirmPassword"/>
                                <form:errors path="confirmPassword" class="errorRequiredField"/>
                            </label>
                            <form:input path="confirmPassword" type="password"/>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-6">
	                    <div class="form-group">
                            <label>
                                <div class="requiredField" align="right">  </div><fmt:message key="cellPhone"/>
                            <form:errors path="customer.address.cellPhone" class="errorRequiredField"/>
                            </label>
                            <form:input id="cell_phone_num" onkeyup="validateCellPhone()" path="customer.address.cellPhone"/>
						</div>
	                    <div class="form-group floatcheckbox">
                            <form:checkbox class="Customer-Email-Notify" id="customer.textMessageNotify1" path="customer.textMessageNotify" />
                            <input type="hidden" name="_customer.textMessageNotify" value="on"/>
                            <span style="margin-left:60px; display:block;"><fmt:message key="sendMeTextMessageNotifications"/></span>
						</div>                        
                    </div>
                    <div class="col-md-6">
	                    <div class="form-group">
                            <label>
                                <div class="requiredField" align="right"> * </div><fmt:message key="emailAddress"/>
                                <form:errors path="customer.username" class="errorRequiredField"/>
                            </label>
                            <form:input path="customer.username" type="text"/>
						</div>
						<div class="form-group floatcheckbox2">
                            <form:checkbox class="Customer-Email-Notify" id="emailNotify2" path="customer.usernameDisabled" onclick="cellPhoneRegister()"/>
                            <input type="hidden" name="_customer.emailNotify" value="on">
                            <span style="margin-left:60px; display:block;"><fmt:message key="dontHaveEmail"/></span>
						</div>
	                    <div class="form-group floatcheckbox">
                            <form:checkbox class="Customer-Email-Notify" id="customer.emailNotify1" path="customer.emailNotify"/>
                            <input type="hidden" name="_customer.emailNotify" value="on">
                            <span style="margin-left:60px; display:block;"><fmt:message key="sendMeEmailNotifications"/></span>
						</div>                        
                    </div>
                </div>
                <div class="row" id="mobileCarrierId" style="display: none;">
                    <div class="col-md-6">
	                    <div class="form-group">
                            <label>
                                <div class="requiredField" align="right"> * </div><fmt:message key="mobileCarrier"/>
                                <form:errors path="customer.address.mobileCarrierId" class="errorRequiredField"/>
                            </label>
                            <form:select id="mobileCarrierIdSelect" path="customer.address.mobileCarrierId" style="display: none;" disabled="">
                            <option value="">Please Select</option>
                            <option value="1">AT&amp;T</option><option value="2">Verizon </option><option value="3">T-Mobile</option><option value="4">Sprint PCS</option><option value="5">Virgin Mobile</option><option value="6">US Cellular</option><option value="7">Nextel</option><option value="8">Boost</option><option value="9">Alltel</option><option value="10">Alltel Wireless</option><option value="11">Cricket Wireless</option><option value="12">Sprint</option><option value="13">MetroPCS</option><option value="14">Boost Mobile</option>
                            </form:select >
						</div>
                    </div>
                    <div class="col-md-6">
	                    <div class="form-group">
                            <div id="textMessageDisclaimer">
                                <span class="helpNote"><fmt:message key="textMessageDisclaimer"/>
                                </span>
                            </div>
						</div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-6">
	                    <div class="form-group">
                            <label>
                                <div class="requiredField" align="right"> * </div><fmt:message key="phone"/>
                            <form:errors path="customer.address.phone" class="errorRequiredField"/>
                            </label>
                            <form:input id="phone_num" path="customer.address.phone" onkeyup="validatePhone()" style="display: block;"/> 
						</div>
                    </div>
                    <div class="col-md-6">
	                    <div class="form-group">
                            <label>
                                <fmt:message key="fax"/>
                            </label>
                            <form:input id="customer.address.fax" path="customer.address.fax" class="input"/>
						</div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-6">
	                    <div class="form-group">
                            <label>
                                <div class="requiredField" align="right"> * </div><fmt:message key="address"/>
                                <form:errors path="customer.address.addr1"   class="errorRequiredField"/>
                            </label>
                            <form:input id="street_number" path="customer.address.addr1" onFocus="geolocate()" class="input"/>
						</div>
                    </div>
                    
                      <div class="col-md-6">
	                    <div class="form-group">
                            <label>
                                <div class="requiredField" align="right">  </div><fmt:message key="address"/>2
                                <form:errors path="customer.address.addr2" class="errorRequiredField"/>
                            </label>
                            <form:input id="route" path="customer.address.addr2" class="input"/>
						</div>
                    </div>
                   
                   
                   
                </div>
                
                
                
                 <script>
      // This example displays an address form, using the autocomplete feature
      // of the Google Places API to help users fill in the information.

      // This example requires the Places library. Include the libraries=places
      // parameter when you first load the API. For example:
      // <script src="https://maps.googleapis.com/maps/api/js?key=YOUR_API_KEY&libraries=places">

      var placeSearch, autocomplete;
      var componentForm = {
        street_number: 'short_name',
        route: 'long_name',
        locality: 'long_name',
        administrative_area_level_1: 'short_name',
        country: 'long_name',
        postal_code: 'short_name'
      };

      function initAutocomplete() {
        // Create the autocomplete object, restricting the search to geographical
        // location types.
        autocomplete = new google.maps.places.Autocomplete(
            /** @type {!HTMLInputElement} */(document.getElementById('street_number')),
            {types: ['geocode']});

        // When the user selects an address from the dropdown, populate the address
        // fields in the form.
        autocomplete.addListener('place_changed', fillInAddress);
      }

      function fillInAddress() {
        // Get the place details from the autocomplete object.
        var place = autocomplete.getPlace();

        for (var component in componentForm) {
          document.getElementById(component).value = '';
          document.getElementById(component).disabled = false;
        }

        // Get each component of the address from the place details
        // and fill the corresponding field on the form.
        for (var i = 0; i < place.address_components.length; i++) {
          var addressType = place.address_components[i].types[0];
          if (componentForm[addressType]) {
            var val = place.address_components[i][componentForm[addressType]];
            document.getElementById(addressType).value = val;
          }
        }
       
        var state = document.getElementById("administrative_area_level_1");
        var ctry = document.getElementById("country");
        var US_state = document.getElementById("state");
        var canada_state = document.getElementById("ca_province");
        var other_state =  document.getElementById("province");
         
        
   var CountryDropDown = document.getElementById('customer.address.country');
    	
    	for ( var i = 0; i < CountryDropDown.options.length; i++ ) {
    		  if ( CountryDropDown.options[i].text == ctry.value ) {
    			  CountryDropDown.options[i].selected = true;
    	        }
    	}
    	
    	toggleStateProvince(document.getElementById('customer.address.country'));
    	
        if(state.value != '' ){
        	if(ctry.value == 'United States'){
        		US_state.value = state.value;
        		
        	} else if(ctry.value == 'Canada'){
        		canada_state.value = state.value;
        	} else {
        		other_state.value = state.value;
        	}
        	
        }
        
        
     

      }
      
      // Bias the autocomplete object to the user's geographical location,
      // as supplied by the browser's 'navigator.geolocation' object.
      function geolocate() {
        if (navigator.geolocation) {
          navigator.geolocation.getCurrentPosition(function(position) {
            var geolocation = {
              lat: position.coords.latitude,
              lng: position.coords.longitude
            };
            var circle = new google.maps.Circle({
              center: geolocation,
              radius: position.coords.accuracy
            });
            autocomplete.setBounds(circle.getBounds()); 
          });
        }
      }
      
    </script>
    <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAaH9bJSASDQAmiOQkoER4dLFaGzQiZ2nQ&libraries=places&callback=initAutocomplete"
        async defer></script>
					
					<!-- Just for google maps -->
		 		<form:input type= "hidden" id="administrative_area_level_1" class="form-control" path=""/>
		 		<form:input id="country" class="form-control" type="hidden" path=""/>
		 
                <div class="row">
                
                 <div class="col-md-6">
	                    <div class="form-group">
                            <label>
                                <div class="requiredField" align="right"> * </div><fmt:message key="city"/>
                                <form:errors path="customer.address.city" class="errorRequiredField"/>
                            </label>
                            <form:input id="locality" path="customer.address.city" class="input" type="text"/>
						</div>
                    </div>
                    <div class="col-md-6">
	                    <div class="form-group">
                            <label>
                                <div class="requiredField" align="right"> * </div><fmt:message key="stateProvince"/>
                                <form:errors path="customer.address.stateProvince" class="errorRequiredField"/>
                            </label>
                            <form:select id="state" path="customer.address.stateProvince" class="highlight" style="display: none;" disabled="">
                            <option value="">Please Select</option>
                            <c:forEach items="${model.statelist}" var="state">
                                <option value="${state.code}" <c:if test="${state.code == stateSelected}">selected</c:if>>${state.name}</option>
                            </c:forEach>	
                            </form:select>
                            
                            <form:select id="ca_province" path="customer.address.stateProvince" class="highlight" style="display: none;" disabled="">
                            <option value="">Please Select</option>
                            <c:forEach items="${model.caProvinceList}" var="state">
                                <option value="${state.code}" <c:if test="${state.code == stateSelected}">selected</c:if>>${state.name}</option>
                            </c:forEach>	
                            </form:select>
                            
                            <form:input id="province" path="customer.address.stateProvince" style="display: block;"/>
						</div>
	                    <div class="form-group floatcheckbox stateNotapplicable">
                            <div id="stateProvinceNA" style="display: block;">&nbsp;
                            <form:checkbox id="addressStateProvinceNA" path="customer.address.stateProvinceNA" value="true"/><fmt:message key="notApplicable"/></div>
						</div>                        
                    </div>
                                    
                </div> 
               
                 <div class="row">
                   <div class="col-md-6">
	                    <div class="form-group">
                            <label>
                                <div id="zipcodeStar" class="requiredField" align="right"> * </div><fmt:message key="zipPostalCode"/>
                                <form:errors path="customer.address.zip" class="errorRequiredField"/>
                            </label>
                            <form:input path="customer.address.zip" id="postal_code" disabled="" style="display: block;"/>
						</div>
                    </div>  </div> 
                
            </div>
		</div>
<!-- FORM END
================================================== -->

</div>

<!-- FOOTER 
================================================== -->
        <div class="form-group">
        <div class="footer-wrapper">
            
                <div class="row">
                	<div class="col-md-11"></div>
                    <div class="col-md-1">
						<input class="btn btn-lg btn-primary" type="submit" value="Next" name="_target3">
                        <!--<a href="touchRegister.jhtm?trackcode=&language=en&target=01" class="btn btn-back btn-lg btn-default" role="button">Back</a>-->
                    </div>
				</div>
            </div>
        </div>
<!-- FOOTER END
================================================== -->

</form:form>
 
</div>
</div>
<iframe src="sessionKeepAlive.jsp" width="0" height="0" frameborder="0"></iframe>
<script language="JavaScript">
<!--
function toggleStateProvince(el) {
  if (el.value == "US") {
      document.getElementById('state').disabled=false;
      document.getElementById('state').style.display="block";
      document.getElementById('ca_province').disabled=true;
      document.getElementById('ca_province').style.display="none";
      document.getElementById('province').disabled=true;
      document.getElementById('province').style.display="none";
      document.getElementById('addressStateProvinceNA').disabled=true;
      document.getElementById('stateProvinceNA').style.display="none";
      document.getElementById('mobileCarrierIdSelect').disabled=false;
      document.getElementById('mobileCarrierIdSelect').style.display="block";
      document.getElementById('mobileCarrierId').disabled=false;
      document.getElementById('mobileCarrierId').style.display="block";	  
      document.getElementById('textMessageDisclaimer').disabled=false;
      document.getElementById('textMessageDisclaimer').style.display="table-row";

  } else if (el.value == "CA") {
      document.getElementById('state').disabled=true;
      document.getElementById('state').style.display="none";
      document.getElementById('ca_province').disabled=false;
      document.getElementById('ca_province').style.display="block";
      document.getElementById('province').disabled=true;
      document.getElementById('province').style.display="none";
      document.getElementById('addressStateProvinceNA').disabled=true;
      document.getElementById('stateProvinceNA').style.display="none";
      document.getElementById('mobileCarrierIdSelect').disabled=true;
      document.getElementById('mobileCarrierIdSelect').style.display="none";
      document.getElementById('mobileCarrierId').disabled=true;
      document.getElementById('mobileCarrierId').style.display="none";
      document.getElementById('textMessageNotify').disabled=true;
      document.getElementById('textMessageNotify').style.display="none";
      document.getElementById('textMessageDisclaimer').disabled=true;
      document.getElementById('textMessageDisclaimer').style.display="none";

  } else {
      document.getElementById('state').disabled=true;
      document.getElementById('state').style.display="none";
      document.getElementById('ca_province').disabled=true;
      document.getElementById('ca_province').style.display="none";
      document.getElementById('province').disabled=false;
      document.getElementById('province').style.display="block";
      document.getElementById('addressStateProvinceNA').disabled=false;
      document.getElementById('stateProvinceNA').style.display="block";
      document.getElementById('mobileCarrierIdSelect').disabled=true;
      document.getElementById('mobileCarrierIdSelect').style.display="none";
      document.getElementById('mobileCarrierId').disabled=true;
      document.getElementById('mobileCarrierId').style.display="none";
      document.getElementById('textMessageNotify').disabled=true;
      document.getElementById('textMessageNotify').style.display="none";
      document.getElementById('textMessageDisclaimer').disabled=true;
      document.getElementById('textMessageDisclaimer').style.display="none";
 
  }
}
document.getElementById('emailNotify2').checked = false;
toggleStateProvince(document.getElementById('customer.address.country'));
//-->

function passwordAtLeastFiveCharacters(e) {
	var divStar = document.getElementById("star");
	/* console.log(fmt.parentNode); */
	var inputPassword = document.getElementById("customer.password");
	 var error = divStar.parentNode.getElementsByClassName("error")[0];
	/*  console.log(error); */
	 
		if(inputPassword.value.length < 5 && typeof error === 'undefined' ) {
			var smallError = document.createElement('small');
			smallError.className = "error";
			smallError.innerHTML = " should be at least 5 characters!";
			smallError.style.color = "white"; 
			/* inputPassword.parentNode.insertBefore(smallError, inputPassword.nextSibling); */
			divStar.parentNode.appendChild(smallError);
			
		}else if(inputPassword.value.length >= 5) {
			/* var smallError = inputPassword.nextSibling; */
			/* console.log(fmt.parentNode); */
			if(error !== null && typeof error !== 'undefined') {
				error.parentNode.removeChild(error); 
			}
	
		}
}
</script>
</body>
</html>