<%@ page import="com.webjaguar.thirdparty.echosign.*,echosign.api.dto8.*,echosign.api.dto.*" %>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="/WEB-INF/tlds/spring.tld" prefix="spring" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>

<c:set var="regEmail" value="${customerForm.customer.username}"/>
<c:set var="ECHOSIGN_API_KEY" value="${siteConfig['ECHOSIGN_API_KEY'].value}"/>
<%
String widgetJavascript = "<script type='text/javascript' language='JavaScript' src='https://secure.echosign.com/public/widget?f=XTRNFF6A3NQXJ'></script>";

EchoSignApi api = new EchoSignApi();
EmbeddedWidgetCreationResult creationResult = api.personalizeEmbeddedWidget(pageContext.getAttribute("ECHOSIGN_API_KEY").toString(), widgetJavascript, pageContext.getAttribute("regEmail").toString(), null);
if (creationResult.isSuccess()) {
	out.println(creationResult.getJavascript());
} else {
	out.println(widgetJavascript);	
}
%>

<tiles:insertDefinition name="${_template}" flush="true">
  <tiles:putAttribute name="content" type="string">
<link rel="stylesheet" href="https://www.viatrading.com/assets/touchscreen/register/${customerForm.language}/stylesheet_6.css" type="text/css"/>

<table class="form" width="100%" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td colspan="4"><div class="title" align="left">&nbsp;</div></td>
  </tr>
</table>
user:${customerForm.customer.username}

<br/><br/>

<script type="text/javascript">
<!--
function submitForm() {
	document.getElementById('registerForm').submit();
}
//-->
</script>

<form:form commandName="customerForm" id="registerForm" method="post">
<input type="hidden" name="_page" value="7">
<input type="hidden" name="_finish">
<form:hidden path="language" />
</form:form>

<iframe src="sessionKeepAlive.jsp" width="0" height="0" frameborder="0"></iframe>

  </tiles:putAttribute>
</tiles:insertDefinition>
