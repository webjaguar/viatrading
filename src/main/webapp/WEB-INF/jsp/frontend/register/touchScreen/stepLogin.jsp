<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="/WEB-INF/tlds/spring.tld" prefix="spring" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>

<link href="https://www.viatrading.com/assets/css/reset.css" rel="stylesheet" type="text/css"/>
<link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css">
<link href="https://www.viatrading.com/assets/css/anythingslider.css" rel="stylesheet" type="text/css"/>
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" integrity="sha384-1q8mTJOASx8j1Au+a5WDVnPi2lkFfwwEAa8hDDdjZlpLegxhjVME1fgjWPGmkzs7" crossorigin="anonymous">
<link href="dv/tsr/css/tsr.css" rel="stylesheet">
<script type="text/javascript">
  var _gaq = _gaq || [];
  _gaq.push(['_setAccount', 'UA-1342297-1']);
  _gaq.push(['_trackPageview']);

  (function() {
    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
  })();

</script>
</head>
<body>
<script type="text/javascript">
//<![CDATA[
try{(function(a){var b="http://",c="www.viatrading.com",d="/cdn-cgi/cl/",e="img.gif",f=new a;f.src=[b,c,d,e].join("")})(Image)}catch(e){}
//]]>
</script>

<!-- NAVBAR 
================================================== -->
        <div class="navbar-wrapper">
            <div class="container">
                <nav class="navbar navbar-inverse navbar-fixed-top">
                    <div class="container">
                        <div class="row">
	                        <div class="col-md-1">
                            	<a href="touchRegister.jhtm?trackcode=&language=en&target=01" class="btn btn-back btn-lg btn-default" role="button">Back</a>
                            </div>
                            <div class="col-md-10"></div>
                            <div class="col-md-1"><img class="via-logo" src="dv/tsr/imgs/via-logo-white.png"/></div>
                        </div>
                    </div>
                </nav>
            </div>
        </div>
<!-- NAVBAR END
================================================== -->

<div id="pageContainer">

<div id="content">
<h2>Login</h2>
<br/>

<script type="text/javascript">

function automateCityState(zipCode) {
	var request = new Request.JSON({
		url: "/jsonZipCode.jhtm?zipCode="+zipCode,
		onRequest: function() { 
		},
		onComplete: function(jsonObj) {
			document.getElementById('customer.address.city').value = jsonObj.city;
			document.getElementById('state').value = jsonObj.stateAbbv;
			$$('.highlight').each(function(el) {
				var end = el.getStyle('background-color');
				end = (end == 'transparent') ? '#fff' : end;
				var myFx = new Fx.Tween(el, {duration: 500, wait: false});
				myFx.start('background-color', '#f00', end);
			});
		}
	}).send();
}
</script>

<script type="text/javascript">
function cellPhoneRegister() {
	if(document.getElementById('loginvia').checked){
		document.getElementById('cell').value = "true";
	}
	else{
		document.getElementById('cell').value = "false";
	}	
}
</script>

<br/>

<form:form commandName="customerForm" id="customerForm" action="touchLogin.jhtm" method="post">
<input type="hidden" name="target" value="2">
<input type="hidden" name="_page" value="2">
<input type="hidden" name="language" value="en">
<div class="formBox">
<input id="cell" name="cell" type="hidden"/>

<!-- FORM 
================================================== -->
        <div class="form-wrapper">
            <div class="container">
                <div class="row">
                    <div class="col-md-4">
                        <div class="form-group">
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
							<label>
                                <fmt:message key="emailAddress"/> or <fmt:message key="cellPhoneNumber"/>
                            </label>
                            <form:input path="customer.username" type="text"/>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                        </div>
                    </div>
                </div>
                
                <div class="row">
                    <div class="col-md-4">
                        <div class="form-group">
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
							<label>
                                <fmt:message key="password"/>
                            </label>
                            <form:input path="customer.password" type="password"/>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                        </div>
                    </div>
                </div>
                
                <div class="row">
                    <div class="col-md-4">
                        <div class="form-group">
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                            <input class="btn btn-lg btn-primary" type="submit" value="login" name="_target3"> <div class="error2">${error}</div>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                        </div>
                    </div>
                </div>

            </div>
		</div>
<!-- FORM END
================================================== -->

</div>
</form:form>
 
</div>
</div>
<iframe src="sessionKeepAlive.jsp" width="0" height="0" frameborder="0"></iframe>
</body>
</html>