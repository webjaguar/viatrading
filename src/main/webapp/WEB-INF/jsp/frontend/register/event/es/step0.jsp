<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<%@ page contentType="text/html; charset=ISO-8859-1" session="false" %>
<link href="https://www.viatrading.com/assets/css/reset.css" rel="stylesheet" type="text/css"/>
<link href="https://www.viatrading.com/assets/css/anythingslider.css" rel="stylesheet" type="text/css"/>
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css">
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" integrity="sha384-1q8mTJOASx8j1Au+a5WDVnPi2lkFfwwEAa8hDDdjZlpLegxhjVME1fgjWPGmkzs7" crossorigin="anonymous">
<link href="dv/tsr/css/tsr.css" rel="stylesheet">
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="/WEB-INF/tlds/spring.tld" prefix="spring" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<link rel="stylesheet" type="text/css" href="https://www.viatrading.com/assets/responsive/css/font-awesome.css">
<link href="https://www.viatrading.com/assets/css/theme/jquery-ui-1.8.13.custom.css" rel="stylesheet" type="text/css"/>
<link href="http://cdn.wijmo.com/jquery.wijmo-open.1.1.6.css" rel="stylesheet" type="text/css"/>
<link href="http://cdn.wijmo.com/jquery.wijmo-complete.1.1.6.css" rel="stylesheet" type="text/css"/>
<link href="https://www.viatrading.com/assets/fancybox/jquery.fancybox-1.3.4.css" rel="stylesheet" type="text/css"/>

<script>
function submitForm(eventId, id, userid) {
        var xmlhttp = new XMLHttpRequest();
        var url = "${_contextpath}/eventRegister.jhtm?eventId=" + eventId + "&id=" + userid;
        xmlhttp.onreadystatechange = function() {
            if (xmlhttp.readyState == 4 && xmlhttp.status == 200) {
            	var element = document.getElementById('registerButton' + id);
        		element.classList.add("registered");                       
            }
        }
        xmlhttp.open("GET", url, true);
        xmlhttp.send();
}
</script>
</head>
<body>


<!-- NAVBAR 
================================================== -->
        <div class="navbar-wrapper">
            <div class="container">
                <nav class="navbar navbar-inverse navbar-fixed-top">
                    <div class="container">
                        <div class="row">
	                        <div class="col-md-1">
                            	<a href="touchRegister.jhtm?language=es&target=01&=" class="btn btn-back btn-lg btn-default" role="button">Atr�s</a>
                            </div>
                            <div class="col-md-10 signup-img"></div>
                            <div class="col-md-1"><img class="via-logo" src="dv/tsr/imgs/via-logo-white.png"/></div>
                        </div>
                    </div>
                </nav>
            </div>
        </div>
<!-- NAVBAR END
================================================== -->


<div id="pageContainer">
<!--<div id="header">
    <img class="logo" src="https://www.viatrading.com/wp-content/uploads/vialogo-transparent-web.png"/>
    <a class="button back-button" href="touchRegister.jhtm?language=es&target=01&=">R-egresar</a>
</div>-->
<div id="content">

<!-- EVENT STATUS 
================================================== -->
    <div class="container">
        <div class="row">
            <div class="col-md-12">
	            <span class="successmessage">${eventStatus}</span>
            </div>
        </div>
    </div>
<!-- EVENT STATUS END 
================================================== -->  

<form id="eventForm" method="get" action="eventRegister.jhtm" name="eventForm">
<input type="hidden" value="1" name="_page">
<input id="event.id" type="hidden" value="" name="event.id">
<input id="cartNo" type="hidden" value="" name="cartNo">
<input id="target" type="hidden" name="_target1">
<input id="language" type="hidden" name="language" value="es">
 
<ul class="event-list">

<c:forEach items="${model.events}" var="event">
<c:if test="${event.active}">
<c:choose>
<c:when test="${event.eventName=='Auction'}">
<li id="registerButton1" class="thumbnail">
    <div class="video-thumb">
        <a class="fancyVideo" href="#auctionSWF">
        	<img src="tsr-new/video-thumb-auction.jpg">
        </a>
    </div>
    <div class="caption">
        <h1>Subastas en Vivo</h1>
        <p>El 1<sup>er</sup> y 3<sup>er</sup> Jueves del mes a las 11am. Nuestras subastas ofrecen mas de 250 lotes de productos en docenas de categorias con apuestos empezando a solo $1. Venga temprano para ver los lotes antes del evento.</p>
        <input class="scanned-card" id="cardId${event.id}" name="cardId${event.id}" placeholder="lea su tarjeta" type="text"/>
        <input class="btn btn-lg btn-primary" type="button" value="Registrese" onclick="submitForm(${event.id},'1', ${userid})" />
    </div>
</li>
</c:when>
<c:when test="${event.eventName=='KM Raffle'}">
<li id="registerButton2" class="thumbnail">
    <div class="video-thumb">
        <a class="fancyVideo" href="#crazysatSWF">
        	<img src="tsr-new/video-thumb-crazysaturdaysale.jpg">
        </a>
    </div>
    <div class="caption">
        <h1>Venta Loca de S�bado</h1>
        <p>Este evento ofrece precios bajisimos en mas de 500 paletas de nuestro inventario hasta 75% de descuento.</p>
        <input class="scanned-card" placeholder="lea su tarjeta" type="text" id="cardId${event.id}" name="cardId${event.id}" />
        <input class="btn btn-lg btn-primary" type="button" value="Registrese" onclick="submitForm(${event.id},'2', ${userid})" />
	</div>
</li>
</c:when>
<c:when test="${event.eventName=='Crazy Thursday'}">
<li id="registerButton3" class="thumbnail">
    <div class="video-thumb">
        <a class="fancyVideo" href="#crazysatSWF">
        	<img src="tsr-new/video-thumb-crazysaturdaysale.jpg">
        </a>
    </div>
    <div class="caption">
        <h1>Jueves Loco<!--${event.eventName}--></h1>
        <p>
        	Cada 2<sup>o</sup> Jueves del mes a las 11am. Descuentos increibles en lotes populares. Docenas de lotes a menos de $100.
        </p>
        <input class="scanned-card" placeholder="scan card" type="text" id="cardId${event.id}" name="cardId${event.id}" />
        <input class="btn btn-lg btn-primary" type="button" value="Registrese" onclick="submitForm(${event.id},'3', ${userid})" />
	</div>
</li>
</c:when>
<c:when test="${event.eventName=='KM Monday'}">
<li id="registerButton4" class="thumbnail">
    <div class="video-thumb">
        <a class="fancyVideo" href="#crazysatSWF">
        	<img src="tsr-new/video-thumb-crazysaturdaysale.jpg">
        </a>
    </div>
    <div class="caption">
        <h1>Venta Loca de S�bado</h1>
        <p>Este evento ofrece precios bajisimos en mas de 500 paletas de nuestro inventario hasta 75% de descuento.</p>
        <input class="scanned-card" placeholder="lea su tarjeta" type="text" id="cardId${event.id}" name="cardId${event.id}" />
        <input class="btn btn-lg btn-primary" type="button" value="Registrese" onclick="submitForm(${event.id},'4', ${userid})" />
	<div class="caption">
</li>
</c:when>
<c:when test="${event.eventName=='300 Pallet'}">
<li id="registerButton5" class="thumbnail">
	<div class="video-thumb">
        <a class="fancyVideo" href="#300palletSWF">
        	<img src="tsr-new/video-thumb-300pallet2.jpg">
        </a>
    </div>
    <div class="caption">
        <h1>Dia de 300 Descuentos</h1>
        <p>Bajamos los precios en 300 de nuestros lotes mas populares durante evento donde el primer llegado es el primer servido!</p>
        <input class="scanned-card" placeholder="lea su tarjeta" type="text" id="cardId${event.id}" name="cardId${event.id}" />
        <input class="btn btn-lg btn-primary" type="button" value="Registrese" onclick="submitForm(${event.id},'5', ${userid})" />
	</div>
</li>
</c:when>
<c:when test="${event.eventName=='300 Pallet Event'}">
<li id="registerButton6" class="thumbnail">
    <div class="video-thumb">
        <a class="fancyVideo" href="#300palletSWF">
        	<img src="tsr-new/video-thumb-300pallet2.jpg">
        </a>
    </div>
    <div class="caption">
        <h1>Dia de 300 Descuentos</h1>
        <p>Bajamos los precios en 300 de nuestros lotes mas populares durante evento donde el primer llegado es el primer servido!</p>
        <input class="scanned-card" placeholder="lea su tarjeta" type="text" id="cardId${event.id}" name="cardId${event.id}" />
        <input class="btn btn-lg btn-primary" type="button" value="Registrese" onclick="submitForm(${event.id},'6', ${userid})" />
	</div>
</li>
</c:when>

<c:otherwise>
<li id="registerButton7" class="thumbnail">
    <div class="video-thumb">
        <a class="fancyVideo" href="#auctionSWF">
        	<img src="tsr-new/video-thumb-auction.jpg">
        </a>
    </div>
    
        <h1>Subastas en Vivo</h1>
        <p>Nuestras subastas ofrecen mas de 250 lotes de productos en docenas de categorias con apuestos empezando a solo $1.</p>
        <input class="scanned-card" placeholder="lea su tarjeta" type="text" id="cardId${event.id}" name="cardId${event.id}" />
        <input class="btn btn-lg btn-primary" type="button" value="Registrese" onclick="submitForm(${event.id},'7', ${userid})" />

</li>
</c:otherwise>
</c:choose>
</c:if>
</c:forEach>

<!-- MONTHLY RAFFLE CHECK IN -->
<li id="mraffle" class="thumbnail">
    <div class="video-thumb">
        <!--<a class="fancyVideo" href="#auctionSWF">-->
            <img src="dv/tsr/imgs/monthly-raffle-video-thumb.jpg">
        <!--</a>-->
    </div>
    <div class="caption">
        <h1>Rifa Mensual</h1>
        <p>
        	Cada 4<sup>o</sup> Jueves del mes a las 4pm. Cada compra que hace durante el mes le consigue una entrada a la rifa. Debe estar presente para participar. Oprima el bot�n el d�a del evento para registrarse.
        </p>
        <input id="mrafflebtn" class="btn btn-lg btn-primary" type="submit" value="Registrese" onclick="submitForm(${event.id})" />
        <input id="mrafflebtn-disabled" class="btn btn-lg btn-primary" type="submit" disabled="disabled" value="Registrese" />
        <div class="registered-message">Ya eatas registrado!</div>
    </div>
</li>
<!-- MONTHLY RAFFLE CHECK IN END -->

</ul>
</form>
<div class="clearboth"></div>
</div>
 
</div>

<c:forEach items="${model.events}" var="event">
<c:if test="${event.active}">

<div style="display:none;">
<c:choose>
	<c:when test="${event.eventName=='Auction'}">
	
	<div style="display:none;">
<div id="auctionSWF">
<iframe src="//fast.wistia.net/embed/iframe/ic7uop2awa" allowtransparency="true" frameborder="0" scrolling="no" class="wistia_embed" name="wistia_embed" allowfullscreen="" mozallowfullscreen="" webkitallowfullscreen="" oallowfullscreen="" msallowfullscreen="" width="1200" height="700">
</iframe>
</div>
	
	</c:when>
	<c:when test="${event.eventName=='KM Raffle'}">
	
	<div style="display:none;">
<div id="crazysatSWF">
<iframe src="//fast.wistia.net/embed/iframe/9vrxl3gddp" allowtransparency="true" frameborder="0" scrolling="no" class="wistia_embed" name="wistia_embed" allowfullscreen="" mozallowfullscreen="" webkitallowfullscreen="" oallowfullscreen="" msallowfullscreen="" width="1200" height="700">
</iframe>
</div>
	
	</c:when>
	<c:when test="${event.eventName=='KM Monday'}">
	
	<div style="display:none;">
<div id="crazysatSWF">
<iframe src="//fast.wistia.net/embed/iframe/9vrxl3gddp" allowtransparency="true" frameborder="0" scrolling="no" class="wistia_embed" name="wistia_embed" allowfullscreen="" mozallowfullscreen="" webkitallowfullscreen="" oallowfullscreen="" msallowfullscreen="" width="1200" height="700">
</iframe>
</div>
	
	</c:when>
	<c:when test="${event.eventName=='300 Pallet'}">
	
	<div style="display:none;">
<div id="300palletSWF">
<iframe src="//fast.wistia.net/embed/iframe/kia7yocpbg" allowtransparency="true" frameborder="0" scrolling="no" class="wistia_embed" name="wistia_embed" allowfullscreen="" mozallowfullscreen="" webkitallowfullscreen="" oallowfullscreen="" msallowfullscreen="" width="1200" height="700">
</iframe>
</div>
	
	</c:when>
	<c:when test="${event.eventName=='300 Pallet Event'}">
	
	<div style="display:none;">
<div id="300palletSWF">
<iframe src="//fast.wistia.net/embed/iframe/kia7yocpbg" allowtransparency="true" frameborder="0" scrolling="no" class="wistia_embed" name="wistia_embed" allowfullscreen="" mozallowfullscreen="" webkitallowfullscreen="" oallowfullscreen="" msallowfullscreen="" width="1200" height="700">
</iframe>
</div>
	
	</c:when>
	<c:otherwise>
	
	<div style="display:none;">
<div id="auctionSWF">
<iframe src="//fast.wistia.net/embed/iframe/ic7uop2awa" allowtransparency="true" frameborder="0" scrolling="no" class="wistia_embed" name="wistia_embed" allowfullscreen="" mozallowfullscreen="" webkitallowfullscreen="" oallowfullscreen="" msallowfullscreen="" width="1200" height="700">
</iframe>
</div>
	</div>
	</c:otherwise>

</c:choose>

<div class="lagButton" onclick="submitForm(${event.id})">
  <div class="eventName"><c:out value="${event.eventName}" /></div>
  <div class="eventDate"><fmt:formatDate type="both" timeStyle="full" pattern="MM/dd/yyyy hh:mm a" value="${event.startDate}"/></div>
</div>

</c:if>
</c:forEach>
 
<div style="display:none;">
<div id="auctionSWF">
<iframe src="//fast.wistia.net/embed/iframe/ic7uop2awa" allowtransparency="true" frameborder="0" scrolling="no" class="wistia_embed" name="wistia_embed" allowfullscreen="" mozallowfullscreen="" webkitallowfullscreen="" oallowfullscreen="" msallowfullscreen="" width="1200" height="700">
</iframe>
</div>
<div style="display:none;">
<div id="raffleSWF">
<iframe src="//fast.wistia.net/embed/iframe/gdbq25nj00" allowtransparency="true" frameborder="0" scrolling="no" class="wistia_embed" name="wistia_embed" allowfullscreen="" mozallowfullscreen="" webkitallowfullscreen="" oallowfullscreen="" msallowfullscreen="" width="1200" height="700">
</iframe>
</div>
<div style="display:none;">
<div id="300palletSWF">
<iframe src="//fast.wistia.net/embed/iframe/kia7yocpbg" allowtransparency="true" frameborder="0" scrolling="no" class="wistia_embed" name="wistia_embed" allowfullscreen="" mozallowfullscreen="" webkitallowfullscreen="" oallowfullscreen="" msallowfullscreen="" width="1200" height="700">
</iframe>
</div>
<div style="display:none;">
<div id="crazysatSWF">
<iframe src="//fast.wistia.net/embed/iframe/9vrxl3gddp" allowtransparency="true" frameborder="0" scrolling="no" class="wistia_embed" name="wistia_embed" allowfullscreen="" mozallowfullscreen="" webkitallowfullscreen="" oallowfullscreen="" msallowfullscreen="" width="1200" height="700">
</iframe>
</div>
<script src="https://ajax.aspnetcdn.com/ajax/jquery/jquery-1.6.4.min.js" type="text/javascript"></script>
<script src="https://ajax.aspnetcdn.com/ajax/jquery.ui/1.8.11/jquery-ui.min.js" type="text/javascript"></script>
<script src="https://cdn.wijmo.com/external/jquery.bgiframe-2.1.3-pre.js" type="text/javascript"></script>
<script src="https://cdn.wijmo.com/external/jquery.glob.min.js" type="text/javascript"></script>
<script src="https://cdn.wijmo.com/external/raphael-min.js" type="text/javascript"></script>
<script src="https://cdn.wijmo.com/jquery.wijmo-open.1.1.6.min.js" type="text/javascript"></script>
<script src="https://viatrading.com/assets/fancybox/jquery.easing-1.3.pack.js" type="text/javascript"></script>
<script src="https://viatrading.com/assets/fancybox/jquery.fancybox-1.3.4.pack.js" type="text/javascript"></script>
<script type="text/javascript">
 $.noConflict();
  jQuery(document).ready(function($) {
        $(".fancyVideo").fancybox();
  });
</script>
<script type="text/javascript">
jQuery(document).ready(function(e) {
	jQuery.get('/dv/liveapps/rafflecheckin.php',{cid:'${userid}',action:'Check'},function(data) {
		if (data.length > 0) {
			jQuery('#mraffle').addClass(data);
		}
	});
	jQuery('#mrafflebtn').click(function(e) {
		alert('Clicked');
		jQuery.get('/dv/liveapps/rafflecheckin.php',{cid:'${userid}',action:'Click'},function(data) {
			if (data == 'Success') {
				jQuery('#mraffle').addClass('registered');
			} else {
				alert(data);
			}
		});
    });
});
</script>
</div></div></div></div>
 
</body>
</html>