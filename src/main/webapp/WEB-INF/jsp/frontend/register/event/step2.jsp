<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="/WEB-INF/tlds/spring.tld" prefix="spring" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>

<link href="https://www.viatrading.com/assets/css/reset.css" rel="stylesheet" type="text/css"/>
<link href="https://www.viatrading.com/assets/css/tsr-New.css" rel="stylesheet" type="text/css"/>
<link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css">
<link href="https://www.viatrading.com/assets/css/anythingslider.css" rel="stylesheet" type="text/css"/>

<%@ page contentType="text/html; charset=ISO-8859-1" session="false" %>


<div id="pageTitleWrapper">
<div id="pageTitle" class="container">
<h1 class="responsive-title">Register For Events</h1>
</div>
</div>
<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">

<script src="/assets/template6/js/libs/html5shiv.js"></script>
<script src="/assets/template6/js/libs/respond.min.js"></script>

<link rel="stylesheet" type="text/css" href="/css/all1.css">
<link rel="stylesheet" type="text/css" href="/css/all2.css">
<link rel="stylesheet" type="text/css" href="/assets/responsive/css/font-awesome.css">

<script src="/assets/responsive/js/libs/html5shiv.min.js"></script>
<script src="/assets/responsive/js/libs/respond.min.js"></script>

<script type="text/javascript" src="/assets/responsive/js/libs/jquery.min.js"></script>

<script type="text/javascript" src="/assets/responsive/js/libs/bootstrap.min.js"></script>

<script type="text/javascript" src="/assets/responsive/plugins/flexmenu/js/jquery.flexmenu.js"></script>

<script type="text/javascript" src="/assets/responsive/plugins/flowtype/flowtype.js"></script>

<script type="text/javascript" src="/assets/responsive/js/libs/intlTelInput.js"></script>

<script type="text/javascript" src="/assets/responsive/plugins/showLoading/jquery.showLoading.js"></script>

<script type="text/javascript" src="/assets/responsive/plugins/bootstrap-touchspin/jquery.bootstrap-touchspin.js"></script>

<script type="text/javascript" src="/assets/responsive/js/init/webjaguar.js"></script>

<script src="/assets/responsive/js/libs/slippry.min.js"></script>

<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-1342297-1', 'auto');
  ga('send', 'pageview');

</script>
<title>Via Trading | Wholesale Merchandise for Your Resale Business</title>
<meta name="description" content="Wholesale Merchandise Liquidator: Buy wholesale liquidations, overstocks, customer returns and and closeout liquidation inventory for resale. Wholesale Merchandise for eBay, Amazon, Flea Markets and Discount Stores in Los Angeles">
</head>
<body><script type="text/javascript">

try{(function(a){var b="http://",c="www.viatrading.com",d="/cdn-cgi/cl/",e="img.gif",f=new a;f.src=[b,c,d,e].join("")})(Image)}catch(e){}

</script>
<div id="pageWrapper">



<div id="pageHeaderWrapper">
<div id="pageHeader">
</div>

</div>




<div id="pageTopbarWrapper">
<div id="pageTopbar">
</div>

</div>




<div id="pageBodyWrapper">
<div id="pageBody" class="container">
<div class="row row-offcanvas row-offcanvas-left">



<div id="leftSidebarWrapper">
<div id="leftSidebar">
</div>

</div>

<div id="contentWrapper">
<div id="content">
<div class="col-sm-12">
        <div id="registerForEventsContentWrapper">
                <h3 class="text-center">Thank you for registering for the auction.</h3>
                <p><c:out value="${eventForm.event.htmlCode}" escapeXml="false"/></p>
        </div>
</div>





</div>
</div>
</div>

</div>



<div id="rightSidebarWrapper">
<div id="rightSidebar">
</div>

</div>

</div>
</div>

</div>



<div id="pageFooterWrapper">
<div id="pageFooter">
<script type="text/javascript">
function createCookie(name,value,days) {
        if (days) {
                var date = new Date();
                date.setTime(date.getTime()+(days*24*60*60*1000));
                var expires = "; expires="+date.toGMTString();
        }
        else var expires = "";
        document.cookie = name+"="+value+expires+"; path=/";
}

function readCookie(name) {
        var nameEQ = name + "=";
        var ca = document.cookie.split(';');
        for(var i=0;i < ca.length;i++) {
                var c = ca[i];
                while (c.charAt(0)==' ') c = c.substring(1,c.length);
                if (c.indexOf(nameEQ) == 0) return c.substring(nameEQ.length,c.length);
        }
        return null;
}

function eraseCookie(name) {
        createCookie(name,"",-1);
}

function PopUp(page) {
        createCookie('PopUpStatus','Shown',30)
        $.fancybox({
                'type': 'iframe',
                'href': page,
                'padding': 0,
                'width': '450px',
                'height': '600px',
        });
}

var PopUpStatus = readCookie('PopUpStatus');

$(document).ready(function(e) {
    if (PopUpStatus != 'Shown') {
            setTimeout(function() {PopUp('/dv/liveapps/constantcontactpopup.php');},5000);
    }
});
</script>
<div id="l2s_trk" style="z-index:99;"><a href="http://live2support.com" style="font-size:1px;">Live2Support.com</a></div><script type="text/javascript">   var l2s_pht=escape(location.protocol); if(l2s_pht.indexOf("http")==-1) l2s_pht='http:'; (function () { document.getElementById('l2s_trk').style.visibility='hidden'; var l2scd = document.createElement('script');  l2scd.type = 'text/javascript'; l2scd.async = true; l2scd.src = ('https:' == document.location.protocol ? 'https://' : 'http://') + 's01.live2support.com/js/lsjs1.php?stid=2114'; var l2sscr = document.getElementsByTagName('script')[0]; l2sscr.parentNode.insertBefore(l2scd, l2sscr); })();  </script>
<div id="footer_wrapper">

<div id="footer">
<div class="container">
<div id="footerTop" class="row">
<div id="footerGroup1" class="col-sm-12 col-md-12 col-lg-7">
<div class="row">
<div class="col-sm-3">
</div>
<div class="col-sm-3">
</div>
<div class="col-sm-3">
</div>
<div class="col-sm-3">
</div>
</div>
</div>
<div id="footerGroup2" class="col-sm-12 col-md-12 col-lg-5">
<div class="row">
<div class="col-sm-5">
<div id="bigLinks">
</div>
</div>
<div class="col-sm-7">

</div>
</div>
</div>
</div>
<div id="footer_bottom">
<div class="row">
<div class="col-sm-4"></div>
<div class="col-sm-4" style="width:590px;margin:10px auto;">
<div style="float:left">
</div>
<div style="float:left;margin:auto">

</div>
<div style="float:left;margin:auto;">
</div>
<div class="col-sm-4"></div>
</div>
</div>
<div class="row">
<div class="col-sm-5">
</div>
<div class="col-sm-7">
</div>
</div>
</div>
</div>
</div>
</div>
</div>

</div>

<div id="pageSubFooterWrapper">
<div id="pageSubFooter">
</div>

</div>

</div>

</body>
</html>





