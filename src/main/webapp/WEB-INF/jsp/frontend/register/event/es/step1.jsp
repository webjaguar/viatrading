<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="/WEB-INF/tlds/spring.tld" prefix="spring" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>

<%@ page contentType="text/html; charset=ISO-8859-1" session="false" %>
<link rel="stylesheet" type="text/css" href="https://www.viatrading.com/assets/responsive/css/font-awesome.css">

<link href="https://www.viatrading.com/assets/css/reset.css" rel="stylesheet" type="text/css"/>
<link href="https://www.viatrading.com/assets/css/tsr-New.css" rel="stylesheet" type="text/css"/>
<link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css">
<link href="https://www.viatrading.com/assets/css/anythingslider.css" rel="stylesheet" type="text/css"/>

<tiles:insertDefinition name="${_template}" flush="true">
  <tiles:putAttribute name="content" type="string">
<c:if test="${gSiteConfig['gEVENT']}">
<link rel="stylesheet" href="https://www.viatrading.com/assets/touchscreen/event/${eventForm.language}/stylesheet_1.css" type="text/css"/>

<form:form commandName="eventForm" action="eventRegister.jhtm" method="post">
<input type="hidden" name="_page" value="1">
<form:hidden path="language" />

<div class="formBox">
<table class="form" width="100%" border="0" cellspacing="0" cellpadding="0">
<tr>
  <td colspan="4"><div class="title" align="left"><fmt:message key="scanCard" /></div></td>
</tr>
<tr>
  <td colspan="6">
  <div id="scanTitle" align="center">
  	<fmt:message key="scanYourCard" />
  </div>
  </td>
</tr>
<tr>
  <td colspan="6">
  <div id="scanInput" align="center">
  	<form:input path="cardId" cssClass="input" cssErrorClass="errorField" />
  </div>
  <div id="suspendMessage" align="center">
  <c:if test="${eventForm.customer.suspendedEvent}">
  	<fmt:message key="f-suspendFromEventRegistration"/>
  </c:if>
  </div>
  </td>
</tr>
<tr>
  <td colspan="6">
  <div id="scanError" align="center">
  	<form:errors path="cardId" cssClass="error" />
  </div>
  </td>
</tr>
  
<tr>
  <td colspan="6">
  <div id="eventName" align="center">
  	<c:out value="${eventForm.event.eventName}" />
  </div>
  <div id="eventDate" align="center">
  	<fmt:formatDate type="both" timeStyle="full" pattern="MM/dd/yyyy hh:mm a" value="${eventForm.event.startDate}"/>
  </div>
  </td>
</tr>
<tr>
  <td colspan="6">
  <div class="navButton" align="center">
  <input class="button right" type="submit" value="<fmt:message key="register" />" name="_finish">
  <input class="button left" type="submit" value="<fmt:message key="viewOtherEvents" />" name="_target0">
  </div>
  </td>
</tr>
</table>
</div>

</form:form>

</c:if>
  </tiles:putAttribute>
</tiles:insertDefinition>