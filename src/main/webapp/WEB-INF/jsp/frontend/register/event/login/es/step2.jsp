<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="/WEB-INF/tlds/spring.tld" prefix="spring" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>

<tiles:insertDefinition name="${_template}" flush="true">
  <tiles:putAttribute name="content" type="string">
<c:if test="${gSiteConfig['gEVENT']}">
<link rel="stylesheet" href="https://www.viatrading.com/assets/touchscreen/event/${eventForm.language}/stylesheet_2.css" type="text/css"/>

<form:form commandName="eventForm" action="eventRegister.jhtm" method="post">
<input type="hidden" name="_page" value="2">
<form:hidden path="language" />

<div class="formBox">
<table class="form" width="100%" border="0" cellspacing="0" cellpadding="0">
<tr>
  <td colspan="6"><div class="title" align="left"><fmt:message key="print" /></div></td>
</tr>
<tr>
  <td colspan="6">
  <div id="scanTitle" align="center">
  	<fmt:message key="print" />
  </div>
  <div id="scanTitle" align="right">
  	<a href="javascript:window.print()"><fmt:message key="print" /></a>
  </div>
  </td>
</tr>
<tr>
  <td colspan="6">
  <div id="printArea" align="center">
    <c:out value="${eventForm.event.htmlCode}" escapeXml="false"/>
  </div>  
  </td>
</tr>
<tr>
  <td colspan="6">
  <div class="navButton" align="center">
    <input class="button" type="submit" value="<fmt:message key="back" />" name="_target1">
    <span><a href="touchRegister.jhtm"><fmt:message key="startOver" /></a></span>
  </div>
  </td>
</tr>
</table>
</div>

</form:form>

</c:if>
  </tiles:putAttribute>
</tiles:insertDefinition>