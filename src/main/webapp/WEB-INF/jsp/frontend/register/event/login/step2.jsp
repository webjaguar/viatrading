<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="/WEB-INF/tlds/spring.tld" prefix="spring" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>

<tiles:insertDefinition name="${_template}" flush="true">
  <tiles:putAttribute name="content" type="string">
<c:if test="${gSiteConfig['gEVENT']}">
<link rel="stylesheet" href="https://www.viatrading.com/assets/touchscreen/event/${eventForm.language}/stylesheet_2.css" type="text/css"/>

<div id="pageBodyWrapper">
<div id="pageBody" class="container">
	<div class="row row-offcanvas row-offcanvas-left">
		<!-- ========== CONTENT ========== -->
		<div id="contentWrapper">
			<div id="content">
				<div class="col-sm-12">
					<div id="registerForEventsContentWrapper">
						<h3 class="text-center">Thank you for registering for the auction.</h3>
						<p>    
							<c:out value="${eventForm.event.htmlCode}" escapeXml="false"/>
						</p>
					</div>
				</div>
			</div>
		</div>
		<!-- ========== END CONTENT ========== -->
		</div>
	</div>
</div>

</c:if>

</tiles:putAttribute>
</tiles:insertDefinition>