<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="/WEB-INF/tlds/spring.tld" prefix="spring" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>

<tiles:insertDefinition name="${_template}" flush="true">
  <tiles:putAttribute name="content" type="string">
<c:if test="${gSiteConfig['gEVENT']}">
<link rel="stylesheet" href="https://www.viatrading.com/assets/eventLogin/stylesheet_1.css" type="text/css"/>

<form:form commandName="eventForm" action="loginEventRegister.jhtm" method="post">
<input type="hidden" name="_page" value="1">
<form:hidden path="language" />
<!-- ========== PAGE BODY ========== -->
<div id="pageBodyWrapper">
	<div id="pageBody" class="container">
		<div class="row row-offcanvas row-offcanvas-left">
			<!-- ========== CONTENT ========== -->
			<div id="contentWrapper">
				<div id="content">
					<div class="col-sm-12">
						<div id="registerForEventsContentWrapper">
							<p class="text-center" style="font-size: 18px;">To register in person, scan your customer card into the slot below. <br>
							To register online, please click the "Register" button below.</p>
							<br>
							<div class="row">
								<div class="col-sm-offset-3 col-sm-6 col-md-offset-4 col-md-4">
									
										<div class="form-group">
											<form:input path="cardId" class="form-control" value="${model.customer.cardId}"   />
										</div>
										
										<c:choose>
										<c:when test="${eventForm.event.eventName == 'Auction' }">
											<p class="text-center">Auction<br>
										</c:when>	
										
										<c:when test="${eventForm.event.eventName == '300 Pallet Event' }">
											<p class="text-center">300 Pallet Blowout <br>
										</c:when>	
										
										<c:when test="${eventForm.event.eventName == 'Crazy Saturday Sale' }">
											<p class="text-center">Crazy Saturday Sale<br>
										</c:when>
										
										<c:otherwise>
											<p class="text-center">${eventForm.event.eventName}<br>
										</c:otherwise>
											
										</c:choose>
										
										<fmt:formatDate type="both" dateStyle="full" timeStyle="full" pattern="EEEE, MMMM dd H:mm a" value="${eventForm.event.startDate}"/>
										</p>
										

										<div class="buttons_wrapper clearfix">
											<div class="btn_wrapper login_btn_wrapper">
												<button type="submit" class="btn btn-lg btn-orange">
												<input type="hidden" name="_finish" value="_finish" />
													Register
												</button>
											</div>
											<div class="btn_wrapper createAccount_btn_wrapper">
												<button type="submit" class="btn btn-lg btn-orange">
												
													Other Events
												</button>
											</div>
										</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			<!-- ========== END CONTENT ========== -->
		</div>
	</div>

<!-- ========== END PAGE BODY ========== -->


</form:form>






</c:if>
  </tiles:putAttribute>
</tiles:insertDefinition>