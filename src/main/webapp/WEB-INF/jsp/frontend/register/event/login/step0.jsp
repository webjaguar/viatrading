<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="/WEB-INF/tlds/spring.tld" prefix="spring" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>

<tiles:insertDefinition name="${_template}" flush="true">
  <tiles:putAttribute name="content" type="string">


<script language="JavaScript">
function submitForm(eventId) {
	document.getElementById('event.id').value=eventId;
	document.eventForm.submit();
}
</script>




	<div class="col-sm-12">
		<div id="registerForEventsContentWrapper">
			<script type="text/javascript">
				$(window).load(function() {
					$(window).resize(function(){
						var w=window, d=document, e=d.documentElement, g=d.getElementsByTagName('body')[0];
						var screen_width = w.innerWidth||e.clientWidth||g.clientWidth;
						var screen_height = w.innerHeight||e.clientHeight||g.clientHeight;

						if(screen_width > 767) {
							$(".event_title").css('height','auto');
							var event_title_maxHeight = Math.max.apply(null, $(".event_title").map(function(){
								return $(this).height();
							}).get());
							$(".event_title").height(event_title_maxHeight);

							$(".sp_event_title").css('height','auto');
							var sp_event_title_maxHeight = Math.max.apply(null, $(".sp_event_title").map(function(){
								return $(this).height();
							}).get());
							$(".sp_event_title").height(sp_event_title_maxHeight);

							$(".event_image_wrapper").css('height','auto');
							var event_image_wrapper_maxHeight = Math.max.apply(null, $(".event_image_wrapper").map(function(){
								return $(this).height();
							}).get());
							$(".event_image_wrapper").height(event_image_wrapper_maxHeight);

							$(".date_time").css('height','auto');
							var date_time_maxHeight = Math.max.apply(null, $(".date_time").map(function(){
								return $(this).height();
							}).get());
							$(".date_time").height(date_time_maxHeight);

							$(".event").css('height','auto');
							var event_maxHeight = Math.max.apply(null, $(".event").map(function(){
								return $(this).height();
							}).get());
							$(".event").height(event_maxHeight);
						}
						else {
							$(".event_title").css('height','auto');
							$(".sp_event_title").css('height','auto');
							$(".event_image_wrapper").css('height','auto');
							$(".date_time").css('height','auto');
							$(".event").css('height','auto');
						}
					}).trigger('resize');
				});
			</script>
			<div class="events_wrapper">
				<div class="row">

					<form:form commandName="eventForm" name="eventForm" action="loginEventRegister.jhtm" method="post">
					<input type="hidden" name="_page" value="0">
					<form:hidden path="event.id" />
					<input type="hidden" id="target" name="_target1">

					<c:forEach items="${model.events}" var="event">
					<c:choose>
					<c:when test="${event.eventName == 'Auction' }">
					<div class="col-sm-4">
						<div class="event clearfix">
							<a class="event_link" onclick="submitForm(${event.id})">
								<div class="event_title">Live Auction</div>
								<div class="sp_event_title">Subasta en Vivo</div>
								<div class="event_image_wrapper">
									<img id="1" src="assets/responsive/img/events/event_1.jpg" alt="Live Auction" class="img-responsive center-block" onclick="submitForm(${event.id})">
								</div>
								<div class="date_time">
									<div class="date"><fmt:formatDate type="both" dateStyle="full" timeStyle="full" pattern="EEEE, MMMM dd H:mm a" value="${event.startDate}"/></div>
								</div>
							</a>
						</div>
					</div>			
					</c:when>
					
					
					<c:when test="${event.eventName == '300 Pallet Event' }">
					<div class="col-sm-4">
						<div class="event clearfix">
							<a class="event_link" onclick="submitForm(${event.id})">
								<div class="event_title">Crazy Thursday</div>
								<div class="sp_event_title">Jueves Loco</div>
								<div class="event_image_wrapper">
									<img id="2" src="assets/responsive/img/events/event_2.jpg" alt="300 Pallet Blowout" class="img-responsive center-block" onclick="submitForm(${event.id})">
								</div>
								<div class="date_time">
									<div class="date"><fmt:formatDate type="both" dateStyle="full" timeStyle="full" pattern="EEEE, MMMM dd H:mm a" value="${event.startDate}"/></div>
								</div>
							</a>
						</div>
					</div>
					</c:when>
					
					<c:when test="${event.eventName == 'Crazy Saturday Sale'}">
					<div class="col-sm-4">
						<div class="event clearfix">
							<a class="event_link" onclick="submitForm(${event.id})">
								<div class="event_title">Crazy Saturday Sale</div>
								<div class="sp_event_title">Venta Loca del Sabado</div>
								<div class="event_image_wrapper">
									<img id="3" src="assets/responsive/img/events/event_3.jpg" alt="Crazy Saturday Sale" class="img-responsive center-block" onclick="submitForm(${event.id})">
								</div>
								<div class="date_time">
									<div class="date"><fmt:formatDate type="both" dateStyle="full" timeStyle="full" pattern="EEEE, MMMM dd H:mm a" value="${event.startDate}"/></div>
								</div>
							</a>
						</div>
					</div>			
					</c:when>
					
					<c:when test="${event.eventName == 'Crazy Thursday'}">
					<div class="col-sm-4">
						<div class="event clearfix">
							<a class="event_link" onclick="submitForm(${event.id})">
								<div class="event_title">Crazy Thursday</div>
								<div class="sp_event_title">Venta De Jueves Loco</div>
								<div class="event_image_wrapper">
									<img id="3" src="assets/responsive/img/events/event_3.jpg" alt="Crazy Thursday" class="img-responsive center-block" onclick="submitForm(${event.id})">
								</div>
								<div class="date_time">
									<div class="date"><fmt:formatDate type="both" dateStyle="full" timeStyle="full" pattern="EEEE, MMMM dd H:mm a" value="${event.startDate}"/></div>
								</div>
							</a>
						</div>
					</div>			
					</c:when>
			
					<c:otherwise>
					<div class="col-sm-4">
						<div class="event clearfix">
							<a class="event_link" onclick="submitForm(${event.id})">
								<div class="event_title">${event.title}</div>
								<div class="sp_event_title">Venta De Jueves Loco</div>
								<div class="event_image_wrapper">
									<img id="3" src="assets/Image/interface/300pall/300PalletInstructions_03.jpg" alt="Crazy Saturday Sale" class="img-responsive center-block" onclick="submitForm(${event.id})">
								</div>
								<div class="date_time">
									<div class="date"><fmt:formatDate type="both" dateStyle="full" timeStyle="full" pattern="EEEE, MMMM dd H:mm a" value="${event.startDate}"/></div>
								</div>
							</a>
						</div>
					</div>			
					</c:otherwise>
					</c:choose>
					</c:forEach>
					
					</form:form>
					

				</div>
			</div>
		</div>
	</div>



  </tiles:putAttribute>
</tiles:insertDefinition>
