<%@ page import="com.webjaguar.thirdparty.echosign.*,echosign.api.dto8.*,echosign.api.dto.*,com.webjaguar.model.*,java.util.*" %>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="/WEB-INF/tlds/spring.tld" prefix="spring" %>

<tiles:insertDefinition name="${_template}" flush="true">
  <tiles:putAttribute name="content" type="string">

<div align="center">
<span style="font-size: medium;">
<b><span style="font-family: Comic Sans MS;color: rgb(153, 51, 0);">DO NOT REGISTER WITHOUT A VALID SELLER'S PERMIT</span></b>
<br/><br/>
NOTE: For all customers in the State Of Texas, you must eSign this<br/>
<span style="color: rgb(153, 51, 0);">TEXAS SALES AND USE TAX RESALE CERTIFICATE (form 01-339)</span><br/>
to register.
</span>
</div>  
<br/><br/>

<c:set var="regEmail" value="${customerForm.customer.username}"/>
<c:set var="ECHOSIGN_API_KEY" value="${siteConfig['ECHOSIGN_API_KEY'].value}"/>
<%
String widgetJavascript = "<script type='text/javascript' language='JavaScript' src='https://secure.echosign.com/public/widget?f=5TNGEQ2LCXN4'></script>";
if (request.getHeader("host").equalsIgnoreCase("www.wholesalehandbagsusa.com") || request.getHeader("host").equalsIgnoreCase("www.wholesalehandbagsusa-temp.com")) {
	widgetJavascript = "<script type='text/javascript' language='JavaScript' src='https://secure.echosign.com/public/widget?f=EG49275DXQ7I'></script>";
} else if (request.getHeader("host").equalsIgnoreCase("www.handbagdistributor.com") || request.getHeader("host").equalsIgnoreCase("www.handbagdistributor-temp.com")) {
	widgetJavascript = "<script type='text/javascript' language='JavaScript' src='https://secure.echosign.com/public/widget?f=EGDQL532422T'></script>";	
} else if (request.getHeader("host").equalsIgnoreCase("www.handbagsupplier.com")) {
	widgetJavascript = "<script type='text/javascript' language='JavaScript' src='https://secure.echosign.com/public/widget?f=EGHSC46YXX73'></script>";	
} else if (request.getHeader("host").equalsIgnoreCase("www.wholesalewesternhandbags.com")) {
	widgetJavascript = "<script type='text/javascript' language='JavaScript' src='https://secure.echosign.com/public/widget?f=EG4G4Z356577'></script>";	
}

EchoSignApi api = new EchoSignApi();
EmbeddedWidgetCreationResult creationResult = api.personalizeEmbeddedWidget(pageContext.getAttribute("ECHOSIGN_API_KEY").toString(), widgetJavascript, pageContext.getAttribute("regEmail").toString(), null);
if (creationResult.isSuccess()) {
	out.println(creationResult.getJavascript());
} else {
	out.println(widgetJavascript);	
}
%>

<br/><br/>

<script type="text/javascript">
<!--
function submitForm() {
	document.getElementById('registerForm').submit();
}
//-->
</script>

<form id="registerForm" method="post">
<input type="hidden" name="_page" value="3">
<input type="hidden" name="_finish">
</form>

<iframe src="sessionKeepAlive.jsp" width="0" height="0" frameborder="0"></iframe>

  </tiles:putAttribute>
</tiles:insertDefinition>
