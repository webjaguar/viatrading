<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">

<c:if test="${model.featuredProducts != null}">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<link rel="stylesheet" href="${_contextpath}/assets/SmoothGallery/<c:out value="${model.stylesheet}" />.css" type="text/css" media="screen" />

<script type="text/javascript" src="${_contextpath}/javascript/mootools-1.2.5-core.js"></script>
<script type="text/javascript">		
	//<![CDATA[
	window.addEvent('domready', function(){
	var numItem=<c:out value="${model.numItem}" />;
	var page=<c:out value="${model.page}" />;
	var currentPage=1;
	var totIncrement=0;
	var boxRight=<c:out value="${model.numBlock}" />;
	var boxLeft=1;
	var increment=<c:out value="${model.increment}" />;
	var maxRightIncrement=increment*(-<c:out value="${model.numBlock}" />);
	var next = function() {
			totIncrement-=increment;
			fx.start('margin-left',totIncrement);
			boxRight+=<c:out value="${model.numBlock}" />;
			boxLeft+=<c:out value="${model.numBlock}" />;
			currentPage++;
			if(currentPage == page){
				$('next').setStyle('backgroundImage','url(${_contextpath}/assets/Image/Layout/nextl.jpg)');
				$('next').removeEvents('click');
			} 
			if(currentPage != 1){
				$('previous').setStyle('backgroundImage','url(${_contextpath}/assets/Image/Layout/prev.jpg)');}
			$('pageNav').highlight('#ddf');
			$('pageNav').set('html', boxLeft + ' - '+ boxRight + ' of ' + numItem);
			$('previous').addEvent('click',prev);
	}
	var prev = function() {
			totIncrement+=increment;
			fx.start('margin-left',totIncrement);
			boxRight-=<c:out value="${model.numBlock}" />;
			boxLeft-=<c:out value="${model.numBlock}" />;
			currentPage--;
			if(currentPage == 1){
				$('previous').setStyle('backgroundImage','url(${_contextpath}/assets/Image/Layout/prevl.jpg)');
				$('next').setStyle('backgroundImage','url(${_contextpath}/assets/Image/Layout/next.jpg)');
				$('previous').removeEvents('click');	
			} 
			if(currentPage != page){
				$('next').setStyle('backgroundImage','url(${_contextpath}/assets/Image/Layout/next.jpg)');}
			$('pageNav').highlight('#ddf');
			$('pageNav').set('html', boxLeft + ' - '+ boxRight + ' of ' + numItem);		
			$('next').addEvent('click',next);
	}
	
	var fx=new Fx.Tween($('myList'),{
		duration:1000,
		transition:'back:in:out'});
		$('previous').setStyle('backgroundImage','url(${_contextpath}/assets/Image/Layout/prevl.jpg)');
		$('pageNav').set('html', boxLeft + ' - '+ boxRight + ' of ' + numItem);	
		if (currentPage != page) {
			$('next').addEvent('click',next);
		} else {
			$('next').setStyle('backgroundImage','url(${_contextpath}/assets/Image/Layout/nextl.jpg)');
		}
});
//]]>
</script>
</head>
<body style="padding:0;margin:0;">



<div id="container">
<div id="stage">
<ul id="myList">
<c:forEach items="${model.featuredProducts}" var="product" varStatus="status">
<c:set var="productLink">${_contextpath}/product.jhtm?id=${product.id}<c:if test="${model.product == null or siteConfig['RECOMMENDED_LIST_SHOW_BREADCRUMB'].value == 'true'}">&cid=<c:out value="${param.cid}"/></c:if></c:set>
<c:if test="${gSiteConfig['gMOD_REWRITE'] == '1' and not fn:contains(product.sku, '/')}">
  <c:set var="productLink">${_contextpath}/${siteConfig['MOD_REWRITE_PRODUCT'].value}/${product.encodedSku}/${product.encodedName}.html<c:if test="${model.product == null or siteConfig['RECOMMENDED_LIST_SHOW_BREADCRUMB'].value == 'true'}">?cid=<c:out value="${param.cid}"/></c:if></c:set>
</c:if>

  <li id="l${status.index+1}">
  <div class="box">
    <div>
      <div class="product_image">
        <a href="${productLink}" target="_top" >
          <c:choose>
           <c:when test="${model.detailsbig}">
             <c:forEach items="${product.images}" var="image" varStatus="statusImage">
	  			<c:if test="${statusImage.first}">
	    		<img src="<c:if test="${!image.absolute}">${_contextpath}/assets/Image/Product/detailsbig/</c:if><c:out value="${image.imageUrl}"/>" alt="<c:out value="${product.name}" />" border="0"/> 
	  			</c:if>
			</c:forEach> 
           </c:when>
           <c:otherwise>
             <c:choose>
	  	       <c:when test="${product.asiId != null}">
	  	         <img src="<c:out value="${fn:replace(product.thumbnail.imageUrl, 'prodimgs', 'prodbigimgs')}"/>" alt="<c:out value="${product.name}" />" border="0"  />
	  	       </c:when>
	  	       <c:otherwise>
	  	         <img src="<c:if test="${!product.thumbnail.absolute}">${_contextpath}/assets/Image/Product/thumb/</c:if><c:out value="${product.thumbnail.imageUrl}" />" alt="<c:out value="${product.name}" />" border="0"/> 
	  	       </c:otherwise>
	  	     </c:choose>
           </c:otherwise>
          </c:choose>
		</a>
      </div> 
      <div class="product_name"><a href="${productLink}" target="_top" ><c:out value="${product.name}" escapeXml="false"/></a></div>
      <div class="product_desc"><c:out value="${product.id}" /><c:out value="${product.shortDesc}" /></div> 
      <c:if test="${(gSiteConfig['gPRODUCT_REVIEW'] or siteConfig['PRODUCT_RATE'].value == 'true' ) and product.enableRate}">
	  <div class="quickProductReview" align="center">
	    <a href="${productLink}"/>
		<c:choose>
	     <c:when test="${product.rate == null}"><img src="${_contextpath}/assets/Image/Layout/star_0.gif" alt=<c:out value="${product.rate.average}"/> title="<c:out value="${product.rate.average}"/>" border="0" /></c:when>
	     <c:when test="${product.rate.average > 0 and product.rate.average <= 0.6}"><img src="${_contextpath}/assets/Image/Layout/star_0h.gif" alt=<c:out value="${product.rate.average}"/> title="<c:out value="${product.rate.average}"/>" border="0" /></c:when>
	     <c:when test="${product.rate.average > 0.6 and product.rate.average <= 1}"><img src="${_contextpath}/assets/Image/Layout/star_1.gif" alt=<c:out value="${product.rate.average}"/> title="<c:out value="${product.rate.average}"/>" border="0" /></c:when>
	     <c:when test="${product.rate.average > 1 and product.rate.average <= 1.6}"><img src="${_contextpath}/assets/Image/Layout/star_1h.gif" alt=<c:out value="${product.rate.average}"/> title="<c:out value="${product.rate.average}"/>" border="0" /></c:when>
	     <c:when test="${product.rate.average > 1.6 and product.rate.average <= 2}"><img src="${_contextpath}/assets/Image/Layout/star_2.gif" alt=<c:out value="${product.rate.average}"/> title="<c:out value="${product.rate.average}"/>" border="0" /></c:when>
	     <c:when test="${product.rate.average > 2 and product.rate.average <= 2.6}"><img src="${_contextpath}/assets/Image/Layout/star_2h.gif" alt=<c:out value="${product.rate.average}"/> title="<c:out value="${product.rate.average}"/>" border="0" /></c:when>
	     <c:when test="${product.rate.average > 2.6 and product.rate.average <= 3}"><img src="${_contextpath}/assets/Image/Layout/star_3.gif" alt=<c:out value="${product.rate.average}"/> title="<c:out value="${product.rate.average}"/>" border="0" /></c:when>
	     <c:when test="${product.rate.average > 3 and product.rate.average <= 3.6}"><img src="${_contextpath}/assets/Image/Layout/star_3h.gif" alt=<c:out value="${product.rate.average}"/> title="<c:out value="${product.rate.average}"/>" border="0" /></c:when>
	     <c:when test="${product.rate.average > 3.6 and product.rate.average <= 4}"><img src="${_contextpath}/assets/Image/Layout/star_4.gif" alt=<c:out value="${product.rate.average}"/> title="<c:out value="${product.rate.average}"/>" border="0" /></c:when>
	     <c:when test="${product.rate.average > 4 and product.rate.average <= 4.6}"><img src="${_contextpath}/assets/Image/Layout/star_4h.gif" alt=<c:out value="${product.rate.average}"/> title="<c:out value="${product.rate.average}"/>" border="0" /></c:when>
	     <c:when test="${product.rate.average > 4.6}"><img src="${_contextpath}/assets/Image/Layout/star_5.gif" alt=<c:out value="${product.rate.average}"/> title="<c:out value="${product.rate.average}"/>" border="0" /></c:when>
	   </c:choose></a>
	  </div>
	  </c:if>
	  <c:if test="${siteConfig['HTML_SCROLLER_WITH_PRICE'].value == 'true'}">
	  <div class="product_price"><%@ include file="/WEB-INF/jsp/frontend/common/priceRange.jsp" %></div>
	  </c:if>
    </div>
  </div>
  </li>
</c:forEach>  
</ul>
</div>
<div id="pageNav"></div>
</div>
<div id="slider-buttons">
<div id="previous" class="prev"></div><div id="next" class="next"></div>
</div>

</body>
</html>
</c:if>
