<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">

<c:if test="${subCategories != null}">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<style type="text/css">
body {
	background: #ddd;
	margin: 0; padding: 0;
	font: normal 10px Verdana, Arial, Helvetica, sans-serif;
}
*{outline: none;}
img {border: 0;}
.container {
	width: 790px;
	padding: 0;
	margin: 0 auto;
}
.slideshow4 {
	position: absolute;
	left: 50%; top: 50%;
	margin: -140px 0 0 -395px;
}


/*--Main Container--*/
.main_view {
	float: left;
	position: relative;
}
/*--Window/Masking Styles--*/
.window {
	height:286px;	width: 790px;
	overflow: hidden; /*--Hides anything outside of the set width/height--*/
	position: relative;
}
.image_reel {
	position: absolute;
	top: 0; left: 0;
}
.image_reel img {float: left;}

/*--Paging Styles--*/
.paging {
	position: absolute;
	bottom: 40px; right: -7px;
	width: 178px; height:47px;
	z-index: 100; /*--Assures the paging stays on the top layer--*/
	text-align: center;
	line-height: 40px;
	background: url(paging_bg2.png) no-repeat;
	display: none; /*--Hidden by default, will be later shown with jQuery--*/
}
.paging a {
	padding: 5px;
	text-decoration: none;
	color: #fff;
}
.paging a.active {
	font-weight: bold; 
	background: #920000; 
	border: 1px solid #610000;
	-moz-border-radius: 3px;
	-khtml-border-radius: 3px;
	-webkit-border-radius: 3px;
}
.paging a:hover {font-weight: bold;}
</style>

<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.3/jquery.min.js"></script>
<script type="text/javascript">

$(document).ready(function() {

	//Set Default State of each portfolio piece
	$(".paging").show();
	$(".paging a:first").addClass("active");
		
	//Get size of images, how many there are, then determin the size of the image reel.
	var imageWidth = $(".window").width();
	var imageSum = $(".image_reel img").size();
	var imageReelWidth = imageWidth * imageSum;
	
	//Adjust the image reel to its new size
	$(".image_reel").css({'width' : imageReelWidth});
	
	//Paging + Slider Function
	rotate = function(){	
		var triggerID = $active.attr("rel") - 1; //Get number of times to slide
		var image_reelPosition = triggerID * imageWidth; //Determines the distance the image reel needs to slide

		$(".paging a").removeClass('active'); //Remove all active class
		$active.addClass('active'); //Add active class (the $active is declared in the rotateSwitch function)
		
		//Slider Animation
		$(".image_reel").animate({ 
			left: -image_reelPosition
		}, 500 );
		
	}; 
	
	//Rotation + Timing Event
	rotateSwitch = function(){		
		play = setInterval(function(){ //Set timer - this will repeat itself every 3 seconds
			$active = $('.paging a.active').next();
			if ( $active.length === 0) { //If paging reaches the end...
				$active = $('.paging a:first'); //go back to first
			}
			rotate(); //Trigger the paging and slider function
		}, 7000); //Timer speed in milliseconds (3 seconds)
	};
	
	rotateSwitch(); //Run function on launch
	
	//On Hover
	$(".image_reel a").hover(function() {
		clearInterval(play); //Stop the rotation
	}, function() {
		rotateSwitch(); //Resume rotation
	});	
	
	//On Click
	$(".paging a").click(function() {	
		$active = $(this); //Activate the clicked paging
		//Reset Timer
		clearInterval(play); //Stop the rotation
		rotate(); //Trigger rotation immediately
		rotateSwitch(); // Resume rotation
		return false; //Prevent browser jump to link anchor
	});	
	
});
</script>

</head>
<body style="padding:0;margin:0;">

  <div class="slideshow4">
    	
        <div class="main_view">

            <div class="window">	
                <div class="image_reel">
                  <c:forEach items="${subCategories}" var="category" varStatus="status">
                    <a href="${_contextpath}/category.jhtm?cid=<c:out value='${category.id}' />">                    	
                    	<c:choose>
						    <c:when test="${ not empty (category.imageUrl) and category.imageUrl != null}">
						    	<c:choose>
						    	<c:when test="${category.absolute}">
						    		<img src="<c:url value="${category.imageUrl}"/>" alt="<c:out value='${category.imageUrlAltName}' />">
						    	</c:when>
						    	<c:otherwise>
						    		<img src="${_contextpath}/assets/Image/Category/<c:out value='${category.imageUrl}' />" alt="<c:out value='${category.imageUrlAltName}' />">
						    	</c:otherwise>
						    	</c:choose>
						    </c:when>
	    					<c:when test="${category.hasImage}">
								<img src="${_contextpath}/assets/Image/Category/cat_<c:out value='${category.id}' />.gif" alt="<c:out value='${category.imageUrlAltName}' />" />
							</c:when>
					    </c:choose>
                    </a>
                  </c:forEach>
                </div>
            </div>
            <div class="paging">
                <c:forEach items="${subCategories}" var="category" varStatus="status">
                <a href="#" rel="<c:out value='${category.id}' />"><c:out value='${status.index}' /></a>
                </c:forEach>
            </div>
        </div>
    </div>

</body>
</html>
</c:if>