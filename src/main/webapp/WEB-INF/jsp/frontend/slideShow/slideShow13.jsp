<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">

<c:if test="${model.featuredProducts != null}">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<c:if test="${!empty model.stylesheet}">
<link rel="stylesheet" href="${_contextpath}/assets/SmoothGallery/<c:out value="${model.stylesheet}" />.css" type="text/css" media="screen" />
</c:if>

<script type="text/javascript" src="${_contextpath}/javascript/mootools-1.2.5-core.js"></script>
<script type="text/javascript" src="${_contextpath}/javascript/mootools-1.2.5-core.js"></script>
<script type="text/javascript">		
	//<![CDATA[
	window.addEvent('domready', function(){
	var numItem = <c:out value="${model.numItem}" />;
	//var maxBoxRight = <c:out value="${maxItemNumber}" />;
	var page = <c:out value="${model.page}" />;
	var currentPage = 1;
	var totIncrement=0;
	var boxRight=3;
	var boxLeft=1;
	var increment=636;
	var maxRightIncrement=increment*(-3);
	var next = function() {
			totIncrement-=increment;
			fx.start('margin-left',totIncrement);
			boxRight+=3;
			boxLeft+=3;
			currentPage++;
			if(currentPage == page){
				$('next').setStyle('backgroundImage','url(${_contextpath}/assets/Image/Layout/nextl.jpg)');
				$('next').removeEvents('click');
			} 
			if(currentPage != 1){
				$('previous').setStyle('backgroundImage','url(${_contextpath}/assets/Image/Layout/prev.jpg)');}
			$('pageNav').highlight('#ddf');
			$('pageNav').set('html', boxLeft + ' - '+ boxRight + ' of ' + numItem);
			$('previous').addEvent('click',prev);
	}
	var prev = function() {
			totIncrement+=increment;
			fx.start('margin-left',totIncrement);
			boxRight-=3;
			boxLeft-=3;
			currentPage--;
			if(currentPage == 1){
				$('previous').setStyle('backgroundImage','url(${_contextpath}/assets/Image/Layout/prevl.jpg)');
				$('next').setStyle('backgroundImage','url(${_contextpath}/assets/Image/Layout/next.jpg)');
				$('previous').removeEvents('click');	
			} 
			if(currentPage != page){
				$('next').setStyle('backgroundImage','url(${_contextpath}/assets/Image/Layout/next.jpg)');}
			$('pageNav').highlight('#ddf');
			$('pageNav').set('html', boxLeft + ' - '+ boxRight + ' of ' + numItem);		
			$('next').addEvent('click',next);
	}
	
	var fx=new Fx.Tween($('myList'),{
		duration:1000,
		transition:'back:in:out'});
		$('previous').setStyle('backgroundImage','url(${_contextpath}/assets/Image/Layout/prevl.jpg)');
		$('pageNav').set('html', boxLeft + ' - '+ boxRight + ' of ' + numItem);	
		if (currentPage != page) {
			$('next').addEvent('click',next);
		} else {
			$('next').setStyle('backgroundImage','url(${_contextpath}/assets/Image/Layout/nextl.jpg)');
		}
});
//]]>
</script>
</head>
<body style="padding:0;margin:0;">

<div id="container">
<div id="stage">
<ul id="myList">
<c:forEach items="${model.featuredProducts}" var="product" varStatus="status">
  <li id="l${status.index+1}">
  <div class="box">
    <div>
      <div class="product_image">
        <a href="product.jhtm?id=${product.id}" target="_top" >
          <img src="<c:if test="${!product.thumbnail.absolute}">${_contextpath}/assets/Image/Product/thumb/</c:if><c:out value="${product.thumbnail.imageUrl}" />" alt="" border="0"/> 
		</a>
      </div> 
      <div class="product_name"><a href="product.jhtm?id=${product.id}" target="_top" ><c:out value="${product.name}" /></a></div>
      <div class="product_desc"><c:out value="${product.id}" /><c:out value="${product.shortDesc}" /></div> 
      <c:if test="${(gSiteConfig['gPRODUCT_REVIEW'] or siteConfig['PRODUCT_RATE'].value == 'true' ) and product.enableRate}">
	  <div class="quickProductReview" align="center">
	    <a href="product.jhtm?id=${product.id}&cid=<c:out value="${param.cid}"/>#reviews">
		<c:choose>
	     <c:when test="${product.rate == null}"><img src="${_contextpath}/assets/Image/Layout/star_0.gif" alt=<c:out value="${product.rate.average}"/> title="<c:out value="${product.rate.average}"/>" border="0" /></c:when>
	     <c:when test="${product.rate.average > 0 and product.rate.average <= 0.6}"><img src="${_contextpath}/assets/Image/Layout/star_0h.gif" alt=<c:out value="${product.rate.average}"/> title="<c:out value="${product.rate.average}"/>" border="0" /></c:when>
	     <c:when test="${product.rate.average > 0.6 and product.rate.average <= 1}"><img src="${_contextpath}/assets/Image/Layout/star_1.gif" alt=<c:out value="${product.rate.average}"/> title="<c:out value="${product.rate.average}"/>" border="0" /></c:when>
	     <c:when test="${product.rate.average > 1 and product.rate.average <= 1.6}"><img src="${_contextpath}/assets/Image/Layout/star_1h.gif" alt=<c:out value="${product.rate.average}"/> title="<c:out value="${product.rate.average}"/>" border="0" /></c:when>
	     <c:when test="${product.rate.average > 1.6 and product.rate.average <= 2}"><img src="${_contextpath}/assets/Image/Layout/star_2.gif" alt=<c:out value="${product.rate.average}"/> title="<c:out value="${product.rate.average}"/>" border="0" /></c:when>
	     <c:when test="${product.rate.average > 2 and product.rate.average <= 2.6}"><img src="${_contextpath}/assets/Image/Layout/star_2h.gif" alt=<c:out value="${product.rate.average}"/> title="<c:out value="${product.rate.average}"/>" border="0" /></c:when>
	     <c:when test="${product.rate.average > 2.6 and product.rate.average <= 3}"><img src="${_contextpath}/assets/Image/Layout/star_3.gif" alt=<c:out value="${product.rate.average}"/> title="<c:out value="${product.rate.average}"/>" border="0" /></c:when>
	     <c:when test="${product.rate.average > 3 and product.rate.average <= 3.6}"><img src="${_contextpath}/assets/Image/Layout/star_3h.gif" alt=<c:out value="${product.rate.average}"/> title="<c:out value="${product.rate.average}"/>" border="0" /></c:when>
	     <c:when test="${product.rate.average > 3.6 and product.rate.average <= 4}"><img src="${_contextpath}/assets/Image/Layout/star_4.gif" alt=<c:out value="${product.rate.average}"/> title="<c:out value="${product.rate.average}"/>" border="0" /></c:when>
	     <c:when test="${product.rate.average > 4 and product.rate.average <= 4.6}"><img src="${_contextpath}/assets/Image/Layout/star_4h.gif" alt=<c:out value="${product.rate.average}"/> title="<c:out value="${product.rate.average}"/>" border="0" /></c:when>
	     <c:when test="${product.rate.average > 4.6}"><img src="${_contextpath}/assets/Image/Layout/star_5.gif" alt=<c:out value="${product.rate.average}"/> title="<c:out value="${product.rate.average}"/>" border="0" /></c:when>
	   </c:choose></a>
	  </div>
	  </c:if>
    </div>
  </div>
  </li>
</c:forEach>  
</ul>
</div>
<div id="pageNav"></div>
</div>
<div id="slider-buttons">
<div id="previous" class="prev"></div><div id="next" class="next"></div>
</div>

</body>
</html>
</c:if>
