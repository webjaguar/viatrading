<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<c:if test="${subCategories != null}">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <c:if test="${!empty stylesheet}">
    <link rel="stylesheet" href="${_contextpath}/assets/SmoothGallery/<c:out value="${stylesheet}" />.css" type="text/css" media="screen" />
    </c:if>
    
    <c:set var="absoluteImages" value="false" />
    <c:forEach items="${subCategories}" var="category" varStatus="status"> 
      <c:if test="${category.imageUrl != null and category.absolute}">
        <c:set var="absoluteImages" value="true" />
      </c:if>
    </c:forEach>
    <script type="text/javascript" src="${_contextpath}/javascript/mootools-1.2.5-core.js"></script>
	<script type="text/javascript" src="${_contextpath}/javascript/mootools-1.2.5.1-more.js"></script>
	<script type="text/javascript" src="${_contextpath}/javascript/slideshow.js"></script>
	<script type="text/javascript">		
	//<![CDATA[
	  window.addEvent('domready', function(){
	    var data = {<c:forEach items="${subCategories}" var="category" varStatus="status"> <c:if test="${category.imageUrl != null}">'<c:out value='${category.imageUrl}' />'</c:if> <c:if test="${category.imageUrl == null}">'cat_<c:out value='${category.id}' />.gif'</c:if> : { caption: '<c:out value='${category.name}' />', href: 'category.jhtm?cid=${category.id}'}<c:if test="${! status.last}" >,</c:if></c:forEach> };
	    var myShow = new Slideshow('show', data, { <c:if test="${random}">random: true,</c:if> <c:if test="${controller}">controller: true,</c:if> <c:if test="${thumb}">thumbnails: true,</c:if> <c:if test="${captions}">captions: true,</c:if> <c:if test="${!overlap}">overlap: false,</c:if> <c:if test="${!empty resize}">resize: <c:out value="${resize}" />,</c:if> <c:if test="${!empty duration}">duration: <c:out value="${duration}" />,</c:if> <c:if test="${!empty transition}">transition: '<c:out value="${transition}" />',</c:if> <c:if test="${!empty height}">height: <c:out value="${height}" />,</c:if> hu: '<c:if test="${!absoluteImages}">assets/Image/Category/</c:if>', <c:if test="${!empty delay}">delay: <c:out value="${delay}" />,</c:if> <c:if test="${!empty height}">width: <c:out value="${width}" /></c:if> });
	  });
	//]]>
	</script>
</head>
<body style="padding:0;margin:0;">

  <div id="show" class="slideshow"></div>

</body>
</html>
</c:if>