<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">

<c:if test="${product != null}">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<c:if test="${!empty stylesheet}">
<link rel="stylesheet" href="${_contextpath}/assets/SmoothGallery/<c:out value="${stylesheet}" />.css" type="text/css" media="screen" />
</c:if>

<script type="text/javascript" src="${_contextpath}/javascript/mootools-1.2.5-core.js"></script>
<script type="text/javascript" src="${_contextpath}/javascript/mootools-1.2.5-core.js"></script>
<script type="text/javascript">
window.addEvent('domready',function(){var totIncrement=0;var boxRight=3;var boxLeft=1;var increment=636;var maxRightIncrement=increment*(-3);var fx=new Fx.Tween($('myList'),{duration:1000,transition:'back:in:out'});$('previous').addEvent('click',function(){if(totIncrement<0){totIncrement+=increment;fx.start('margin-left',totIncrement);boxRight-=3;boxLeft-=3}if(boxLeft==1){$('previous').setStyle('backgroundImage','url(assets/Image/Layout/prevl.jpg)')}if(boxRight<9){$('next').setStyle('backgroundImage','url(assets/Image/Layout/next.jpg)')}});$('next').addEvent('click',function(){if(totIncrement>maxRightIncrement){totIncrement-=increment;fx.start('margin-left',totIncrement);boxRight+=3;boxLeft+=3}if(boxRight==9){$('next').setStyle('backgroundImage','url(assets/Image/Layout/nextl.jpg)')}if(boxLeft<9){$('previous').setStyle('backgroundImage','url(assets/Image/Layout/prev.jpg)')}})});
</script>
</head>
<body style="padding:0;margin:0;">

<div id="stage">
<ul id="myList">
<c:forEach items="${product}" var="product" varStatus="status">
  <li id="l${status.index+1}">
  <div class="box">
    <div>
      <a href="${_contextpath}/category.jhtm?cid=${product.id}" target="_top" >
      <div class="product_image">
        <img src="${_contextpath}/assets/Image/Product/thumb/<c:out value="${product.thumbnail.imageUrl}" />" border="0" alt="" />
      </div> 
      <div class="product_name"><c:out value="${product.name}" /></div>
      <div class="product_desc"><c:out value="${product.id}" /><c:out value="${product.shortDesc}" /></div> 
      </a>
      <div class="product_review"></div>
    </div>
  </div>
  </li>
</c:forEach>  
</ul>
</div>

<div id="slider-buttons">
<div id="previous" class="prev"></div><div id="next" class="next"></div>
</div>

</body>
</html>
</c:if>