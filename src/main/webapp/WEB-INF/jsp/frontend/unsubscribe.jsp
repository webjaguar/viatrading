<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>

<tiles:insertDefinition name="${_template}" flush="true">
  <tiles:putAttribute name="content" type="string">
	<form:form commandName="frontend/unsubscribe" method="post">

<c:choose>
<c:when test="${(model.message == null)}">
<table>
	<tr>
		<td>
			<div class="listfl"><div class="requiredField"><fmt:message key="unsubscribeReason"/> : </div></div>
		</td>
		<td>
			<select name="unsubscribeReason" >
			<option value=""></option>
			<option value="I did not subscribe">I did not subscribe</option>
			<option value="I receive too many emails">I receive too many emails</option>
			<option value="I am no longer in business">I am no longer in business</option>
			<option value="Emails are not relevant to me">Emails are not relevant to me</option>
			</select>
		</td>
	</tr>
	<tr><td>&nbsp</td></tr>
	<tr><td>&nbsp</td></tr>
	<tr>
		<td><div class="listfl"><fmt:message key="otherReasons" /> : </div>
		</td>
		<td>
			<!-- input field -->
        	 <input type="text" name="otherReasons" />
  			<!-- end input field -->   	
		</td>
	</tr>
</table>
	<!-- start button -->
	<div align="left" class="button"> 
 	 <input type="submit" name="__send" value="<fmt:message key="submit" />">
	</div>
	<!-- end button -->	 
</c:when>
<c:otherwise>
	<c:out value="${model.message}" escapeXml="false"/>
</c:otherwise>
</c:choose>

    </form:form>
  </tiles:putAttribute>
</tiles:insertDefinition>