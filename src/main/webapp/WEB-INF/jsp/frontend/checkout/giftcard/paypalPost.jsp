<%@ page import="com.webjaguar.model.*, java.text.*" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<html>
<head>
<c:set value="${invoiceLayout.headTag}" var="headTag"/>
<c:set value="${fn:replace(headTag, '#order#', order.orderId)}" var="headTag"/>
<c:set value="${fn:replace(headTag, '#ordergrandtotal#', order.grandTotal)}" var="headTag"/>
<c:set value="${fn:replace(headTag, '#ordersubtotal#', order.subTotal)}" var="headTag"/>
<c:set value="${fn:replace(headTag, '#ordertotalquantity#', order.totalQuantity)}" var="headTag"/>
<c:set value="${fn:replace(headTag, '#orderdiscount#', order.promoAmount)}" var="headTag"/>
<c:out value="${headTag}" escapeXml="false"/>
</head> 
<body>

<c:set value="${invoiceLayout.headerHtml}" var="headerHtml"/>
<c:set value="${fn:replace(headerHtml, '#order#', order.orderId)}" var="headerHtml"/>
<c:set value="${fn:replace(headerHtml, '#ordergrandtotal#', order.grandTotal)}" var="headerHtml"/>
<c:set value="${fn:replace(headerHtml, '#ordersubtotal#', order.subTotal)}" var="headerHtml"/>
<c:set value="${fn:replace(headerHtml, '#ordertotalquantity#', order.totalQuantity)}" var="headerHtml"/>
<c:set value="${fn:replace(headerHtml, '#orderdiscount#', order.promoAmount)}" var="headerHtml"/>
<c:out value="${headerHtml}" escapeXml="false"/>
<form name="paypalForm" method="POST" action="<c:out value="${siteConfig['PAYPAL_URL'].value}"/>cgi-bin/webscr">

<input type="hidden" name="cmd" value="_xclick">
<input type="hidden" name="business" value="<c:out value="${siteConfig['PAYPAL_BUSINESS'].value}"/>">
<input type="hidden" name="item_name" value="Gift Card">
<input type="hidden" name="currency_code" value="<c:out value="${siteConfig['CURRENCY'].value}"/>">
<input type="hidden" name="no_shipping" value="1">
<input type="hidden" name="invoice" value="<c:out value="${order.orderId}"/>">
<input type="hidden" name="amount" value="<c:out value="${order.grandTotal}" />">
<input type="hidden" name="notify_url" value="<c:out value="${siteConfig['SITE_URL'].value}"/>giftCardPaypal_ipn.jhtm">
<c:if test="${siteConfig['PAYPAL_RETURN'].value != ''}">
<input type="hidden" name="return" value="<c:out value="${siteConfig['PAYPAL_RETURN'].value}"/>">
</c:if>
<c:if test="${siteConfig['PAYPAL_CANCEL_RETURN'].value != ''}">
<input type="hidden" name="cancel_return" value="<c:out value="${siteConfig['PAYPAL_CANCEL_RETURN'].value}"/>">
</c:if>

<noscript>
<input type="submit" class="f_button" value="Please click to continue paying by Paypal">
</noscript>

</form>
<c:set value="${invoiceLayout.footerHtml}" var="footerHtml"/>
<c:set value="${fn:replace(footerHtml, '#order#', order.orderId)}" var="footerHtml"/>
<c:set value="${fn:replace(footerHtml, '#ordergrandtotal#', order.grandTotal)}" var="footerHtml"/>
<c:set value="${fn:replace(footerHtml, '#ordersubtotal#', order.subTotal)}" var="footerHtml"/>
<c:set value="${fn:replace(footerHtml, '#ordertotalquantity#', order.totalQuantity)}" var="footerHtml"/>
<c:set value="${fn:replace(footerHtml, '#orderdiscount#', order.promoAmount)}" var="footerHtml"/>
<c:out value="${footerHtml}" escapeXml="false"/>

<%@ include file="/WEB-INF/jsp/frontend/checkout/paypalCustom.jsp" %>
</body>
<script type="text/javascript" language="JavaScript">
<!--
document.paypalForm.submit();
//-->
</script>
</html>
