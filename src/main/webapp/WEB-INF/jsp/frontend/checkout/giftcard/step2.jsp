<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ taglib uri="/WEB-INF/tlds/spring.tld" prefix="spring" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>

<tiles:insertDefinition name="${_template}" flush="true">
  <tiles:putAttribute name="content" type="string">


<c:out value="${giftCardLayout.headerHtml}" escapeXml="false"/>

<c:if test="${model.message != null}">
  <div class="message"><fmt:message key="${model.message}" /></div>
</c:if>

<form:form  commandName="giftCardForm" method="post">
<input type="hidden" name="_page" value="1">
<table width="100%" border="0" cellpadding="3" class="giftcardBox">
 
  <tr>
    <td><form:errors path="giftCardOrder.creditCard.*" cssClass="errorBox"/></td>
  </tr>
  <tr>
    <td>
	  <ol>
	   <li style="list-style-type:none"><fmt:message key="billingInformation" />
	    <table class="box">
	     <tr>
	      <td><fmt:message key="firstName" />:</td>
	      <td><form:input path="giftCardOrder.billing.firstName" size="20"/><form:errors path="giftCardOrder.billing.firstName" /></td>
	     </tr>
	     <tr>
	      <td><fmt:message key="lastName" />:</td>
	      <td><form:input path="giftCardOrder.billing.lastName" size="20"/><form:errors path="giftCardOrder.billing.lastName" /></td>
	     </tr>
	     <tr>
	      <td><fmt:message key="country" />:</td>
	      <td>
		      <form:select id="country" path="giftCardOrder.billing.country" onchange="toggleStateProvince(this)">
		       <form:option value="" label="Please Select"/>
		       <form:options items="${model.countrylist}" itemValue="code" itemLabel="name"/>
		      </form:select>
		      <form:errors path="giftCardOrder.billing.country" cssClass="error" />      
		  </td>
	      <!-- <td><form:input path="giftCardOrder.billing.country" size="20"/><form:errors path="giftCardOrder.billing.country" /></td>  -->
	     </tr>
	     <tr>
	      <td><fmt:message key="address" />1:</td>
	      <td><form:input path="giftCardOrder.billing.addr1" size="20"/><form:errors path="giftCardOrder.billing.addr1" /></td>
	     </tr>
	     <tr>
	      <td><fmt:message key="address" />2:</td>
	      <td><form:input path="giftCardOrder.billing.addr2" size="20"/><form:errors path="giftCardOrder.billing.addr2" /></td>
	     </tr>
	     <tr>
	      <td><fmt:message key="city" />:</td>
	      <td><form:input path="giftCardOrder.billing.city" size="20"/><form:errors path="giftCardOrder.billing.city" /></td>
	     </tr>
	     <tr>
	      <td><fmt:message key="stateProvince" />:</td>
	      <td>
		      <table cellspacing="0" cellpadding="0">
		       <tr>
		       <td>
		         <form:select id="state" path="giftCardOrder.billing.stateProvince" disabled="${giftCardForm.giftCardOrder.billing.country != 'US'}">
		           <form:option value="" label="Please Select"/>
		           <form:options items="${model.statelist}" itemValue="code" itemLabel="name"/>
		         </form:select>
		         <form:select id="ca_province" path="giftCardOrder.billing.stateProvince" disabled="${giftCardForm.giftCardOrder.billing.country != 'CA'}">
		           <form:option value="" label="Please Select"/>
		           <form:options items="${model.caProvinceList}" itemValue="code" itemLabel="name"/>
		         </form:select>
		         <form:input id="province" path="giftCardOrder.billing.stateProvince" htmlEscape="true" disabled="${giftCardForm.giftCardOrder.billing.country == 'US' or giftCardForm.giftCardOrder.billing.country == 'CA' or giftCardForm.giftCardOrder.billing.country == '' or giftCardForm.giftCardOrder.billing.country == null}"/>
		       </td>
		       <td><div id="stateProvinceNA">&nbsp;<form:checkbox path="giftCardOrder.billing.stateProvince" id="addressStateProvinceNA" value="true"  disabled="${giftCardForm.giftCardOrder.billing.country == 'US' or giftCardForm.giftCardOrder.billing.country == 'CA' or giftCardForm.giftCardOrder.billing.country == '' or giftCardForm.giftCardOrder.billing.country == null}"/> Not Applicable</div></td>
		       <td>&nbsp;</td>
		       <td>
		         <form:errors path="giftCardOrder.billing.stateProvince" cssClass="error" />
		       </td>
		       </tr>
		      </table>    
		  </td>
	     </tr>
	     <tr>
	      <td><fmt:message key="zipPostalCode" />:</td>
	      <td><form:input path="giftCardOrder.billing.zip" size="20"/><form:errors path="giftCardOrder.billing.zip" /></td>
	     </tr>
	     <tr>
	      <td><fmt:message key="phone" />:</td>
	      <td><form:input path="giftCardOrder.billing.phone" size="20"/><form:errors path="giftCardOrder.billing.phone" /></td>
	     </tr>
	    </table>
	    </li>
	    
	    <li style="list-style-type:none">
	    <spring:bind path="giftCardOrder.paymentMethod">
			<span class="error"><c:out value="${status.errorMessage}"/></span>
		</spring:bind>		 
	    <c:if test="${gSiteConfig['gPAYPAL'] < 2 and siteConfig['CREDIT_CARD_PAYMENT'].value != ''}">
	    <div class="radio">
	    	<form:radiobutton path="giftCardOrder.paymentMethod" id="cc_radio" value="Credit Card" /> <fmt:message key="payWithCreditCard" />
	    </div>
	    <table class="box">
	     <tr>
	      <td><fmt:message key="cardType" />:*</td>
	      <td>
	      <form:select path="giftCardOrder.creditCard.type">
	       <c:forTokens items="VISA,AMEX,MC,DISC" delims="," var="cc">
             <option value="${cc}" <c:if test="${giftCardForm.giftCardOrder.creditCard.type == cc}">selected</c:if>><fmt:message key="${cc}"/></option>
           </c:forTokens>
	      </form:select>
	      </td>
	     </tr>
	     <tr>
	      <td><fmt:message key="cardNumber" />:*</td>
	      <td><form:input path="giftCardOrder.creditCard.number" maxlength="16" size="16"/><form:errors path="giftCardOrder.creditCard.number" cssClass="error"/></td>
	     </tr>
	     <tr>
	      <td><fmt:message key="cardVerificationCode" />:*</td>
	      <td><form:input path="giftCardOrder.creditCard.cardCode" maxlength="4" size="4"/><form:errors path="giftCardOrder.creditCard.cardCode" cssClass="error"/></td>
	     </tr>
	     <tr>
	      <td><fmt:message key="expDate" /> (MM/YY):*</td>
	      <td>
	      <form:select path="giftCardOrder.creditCard.expireMonth">
	       <c:forTokens items="01,02,03,04,05,06,07,08,09,10,11,12" delims="," var="ccExpireMonth">
            <option value="${ccExpireMonth}" <c:if test="${giftCardForm.giftCardOrder.creditCard.expireMonth==ccExpireMonth}">selected</c:if>><c:out value="${ccExpireMonth}"/></option>
           </c:forTokens>
	      </form:select>
	      <form:select path="giftCardOrder.creditCard.expireYear">
	       <c:forEach items="${model.expireYears}" var="ccExpireYear">
            <option value="${ccExpireYear}" <c:if test="${giftCardForm.giftCardOrder.creditCard.expireYear==ccExpireYear}">selected</c:if>><c:out value="${ccExpireYear}"/></option>
	       </c:forEach>
	      </form:select>
	      </td>
	     </tr>
	    </table>
	    </c:if>
	    <c:if test="${gSiteConfig['gPAYPAL'] > 0}">
			<spring:bind path="giftCardOrder.paymentMethod">
			<div class="clear"></div>
			<div id="paypal" class="payment">
			 <div class="ritWrapper">
			   <div class="radio"><input type="radio" value="PayPal" name="<c:out value="${status.expression}"/>"  <c:if test="${status.value == 'PayPal'}">checked</c:if>></div> 
			   <div id="paypalTextImageBox" class="imgText">
			     <div id="paypalText" class="text"></div>
			     <div id="paypalImage" class="image"><a href="#" onclick="javascript:window.open('https://www.paypal.com/cgi-bin/webscr?cmd=xpt/Marketing/popup/OLCWhatIsPayPal-outside','olcwhatispaypal','toolbar=no, location=no, directories=no, status=no, menubar=no, scrollbars=yes, resizable=yes, width=400, height=350');"><img  src="https://www.paypal.com/en_US/i/logo/PayPal_mark_60x38.gif" border="0" alt="Acceptance Mark"></a></div>
			     <div id="paypalNone" class="note"><c:out value="${siteConfig['PAYPAL_PAYMENT_METHOD_NOTE'].value}" escapeXml="false"/></div>
			   </div>
			 </div>
			</div>
			</spring:bind>
		</c:if>
	    </li>
	  </ol>
	</td>
    <td></td>
  </tr>  
  <tr align="center">
    <td colspan="2" id="buttonWrapper">    
	  <input type="submit" class="f_button" name="_target0" value="<fmt:message key="back" />" />
	  <input type="submit"  class="f_button" name="_target2" value="<fmt:message key="next" />" />
    </td>
  </tr>
  </table>
  
</form:form>

<c:out value="${giftCardLayout.footerHtml}" escapeXml="false"/>

<script language="JavaScript">
<!--

function toggleStateProvince(el) {
  if (el.value == "US") {
      document.getElementById('state').disabled=false;
      document.getElementById('state').style.display="block";
      document.getElementById('ca_province').disabled=true;
      document.getElementById('ca_province').style.display="none";
      document.getElementById('province').disabled=true;
      document.getElementById('province').style.display="none";
      document.getElementById('addressStateProvinceNA').disabled=true;
      document.getElementById('stateProvinceNA').style.display="none";
  } else if (el.value == "CA") {
      document.getElementById('state').disabled=true;
      document.getElementById('state').style.display="none";
      document.getElementById('ca_province').disabled=false;
      document.getElementById('ca_province').style.display="block";
      document.getElementById('province').disabled=true;
      document.getElementById('province').style.display="none";
      document.getElementById('addressStateProvinceNA').disabled=true;
      document.getElementById('stateProvinceNA').style.display="none";
  } else {
      document.getElementById('state').disabled=true;
      document.getElementById('state').style.display="none";
      document.getElementById('ca_province').disabled=true;
      document.getElementById('ca_province').style.display="none";
      document.getElementById('province').disabled=false;
      document.getElementById('province').style.display="block";
      document.getElementById('addressStateProvinceNA').disabled=false;
      document.getElementById('stateProvinceNA').style.display="block";
  }
}
toggleStateProvince(document.getElementById('country'));
//-->
</script>

  </tiles:putAttribute>
</tiles:insertDefinition>