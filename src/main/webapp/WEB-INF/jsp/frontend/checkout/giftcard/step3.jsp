<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="/WEB-INF/tlds/spring.tld" prefix="spring" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>

<tiles:insertDefinition name="${_template}" flush="true">
  <tiles:putAttribute name="content" type="string">
<style>
<!--
table.invoice {
	font-size: 8pt;
	font-family: Verdana, Arial, Helvetica, sans-serif;
	color: #000000;
	background: #000000;
	}
th.invoice {
	color: #DDDDDD;
	}
td.invoice {
	background: #FFFFFF;
	}
td.discount {
	background: #FFFFFF;
	color:  #FF0000;
	font-weight:bold;
	}	
.invoiceHdr {
	padding-top: 10px;
	padding-bottom: 10px;
	text-align: center;
	font-weight:bold;
	font-size: 14pt;
	}
.invoiceHdr2 {
	border: 1px solid #919191;
	color: #666666;
	background: #DDDDDD;
	}
.invoice_lineitem_attributes {
	font-size: 7pt;
	}
.error {
	color: #FF0000;
	}
-->
</style>

<c:out value="${giftCardLayout.headerHtml}" escapeXml="false"/>
  
<c:if test="${model.message != null}">
  <div class="message"><fmt:message key="${model.message}" /></div>
</c:if>

<form:form  commandName="giftCardForm" method="post">
<input type="hidden" name="_page" value="2">
<table width="100%" border="0" cellpadding="3" class="giftcardBox">

  <tr>
    <td><form:errors path="*" cssClass="errorBox"/></td>
  </tr>
  <tr>
    <td>
	  <ol>
	    <li style="list-style-type:none"><table border="0" cellpadding="2" cellspacing="1" width="100%" class="invoice">
  <tr>
    <c:set var="cols" value="0"/>  
    <th width="5%" class="invoice"><fmt:message key="line" />#</th>
    <th class="invoice"><fmt:message key="item" /></th>
    <th class="invoice"><fmt:message key="amount" /></th>
    <th width="10%" class="invoice"><fmt:message key="quantity" /></th>
  </tr>

<tr valign="top">
  <td class="invoice" align="center">1.</td>
  <td class="invoice"><fmt:message key="giftCard" /></td>
  <td class="invoice"><c:out value="${giftCardForm.giftCard.amount}"/></td>    
  <td class="invoice" align="center"><c:out value="${giftCardForm.giftCardOrder.quantity}"/> </td>
</tr>

  <tr bgcolor="#BBBBBB">
	<td colspan="4">&nbsp;</td>
  </tr>
  <tr>
    <td class="invoice" colspan="3" align="right"><fmt:message key="subTotal" />:</td>
    <td class="invoice" align="right"><fmt:formatNumber value="${giftCardForm.giftCardOrder.subTotal}" pattern="#,##0.00"/></td>
  </tr>
</table>
	    </li>
	  </ol>
	</td>
    <td></td>
  </tr>  
  <tr align="center">
    <td colspan="2" id="buttonWrapper">
	  <input type="submit" class="f_button" name="_target1" value="Back" />
	  <input type="submit" class="f_button" name="_finish" value="checkout" />
    </td>
  </tr>
  </table>
  
</form:form>

<c:out value="${giftCardLayout.footerHtml}" escapeXml="false"/>

  </tiles:putAttribute>
</tiles:insertDefinition>