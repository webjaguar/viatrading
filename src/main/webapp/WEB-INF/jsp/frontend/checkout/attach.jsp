<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>

<tiles:insertDefinition name="${_template}" flush="true">
  <tiles:putAttribute name="content" type="string">

<c:out value="${model.attachFilesLayout.headerHtml}" escapeXml="false"/>

<div style="width:95%;margin:auto;">

<form:form commandName="orderForm" method="post" enctype="multipart/form-data">
<table>
<c:set var="indexFile" value="0"/>
<c:forEach items="${attachedFiles}" var="file">
<c:set var="indexFile" value="${indexFile+1}"/>
<tr>
  <td><b>File ${indexFile}:</b></td>
  <td><c:out value="${file['file'].name}"/> 
      <c:choose>
        <c:when test="${file['size'] > (1024*1024)}">
    	  (<fmt:formatNumber value="${file['size']/1024/1024}" pattern="#,##0.0"/> MB)
    	</c:when>
        <c:when test="${file['size'] > 1024}">
    	  (<fmt:formatNumber value="${file['size']/1024}" pattern="#,##0"/> KB)
    	</c:when>
    	<c:otherwise>
    	  (1 KB)
    	</c:otherwise>
      </c:choose> 
  	  <input type="submit" name="remove_${indexFile-1}" value="<fmt:message key="remove" />">
  </td>
</tr>
</c:forEach>
<c:if test="${gSiteConfig['gORDER_FILEUPLOAD'] > indexFile}">
<c:set var="showAttach" value="true"/>
<c:forEach begin="${indexFile+1}" end="${gSiteConfig['gORDER_FILEUPLOAD']}" var="indexFile" varStatus="status">
<tr>
  <td><b>File ${indexFile}:</b></td>
  <td><input value="browse" type="file" name="file_${status.count-1}" id="file_${status.count-1}"/></td>
</tr>
</c:forEach>
</c:if>
<tr>
  <td colspan="2">&nbsp;</td>
</tr>
</table>
<c:if test="${showAttach}">
<input type="submit" value="Attach" id="_target5" name="_target5">
</c:if>
<input type="submit" value="Back to Invoice" name="_target4">
<c:if test="${showAttach}">
<div style="color:#FF0000;padding:10px">Note: Maximum 2 MB per upload</div>
</c:if>
</form:form>

</div>

<c:out value="${model.attachFilesLayout.footerHtml}" escapeXml="false"/>

  </tiles:putAttribute>
</tiles:insertDefinition>