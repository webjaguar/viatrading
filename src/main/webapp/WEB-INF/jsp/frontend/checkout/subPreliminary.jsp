<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib uri="/WEB-INF/tlds/spring.tld" prefix="spring" %>

<c:set var="tempLineItem" value="${model.groupItems[lineItem.itemGroup]}"/>
 
<tr valign="top">
<c:choose>
  <c:when test="${displaySkuAndName}">
  <td class="invoice" align="center"></td>
  <td class="invoice">
	<c:if test="${tempLineItem.customImageUrl != null}">
      <a href="framer/pictureframer/images/FRAMED/${tempLineItem.customImageUrl}" onclick="window.open(this.href,'','width=640,height=480,resizable=yes'); return false;"><img class="invoiceImage" src="framer/pictureframer/images/FRAMED/${tempLineItem.customImageUrl}" border="0" /></a>
    </c:if>  
    <c:if test="${siteConfig['SHOW_IMAGE_ON_INVOICE'].value == 'true' and tempLineItem.product.thumbnail != null}">
      <img class="invoiceImage" src="<c:if test="${!tempLineItem.product.thumbnail.absolute}">assets/Image/Product/thumb/</c:if>${tempLineItem.product.thumbnail.imageUrl}" border="0" /><br />
    </c:if><div class="sku" style="font-size: 16px;"><c:out value="${tempLineItem.product.sku}"/></div>
  </td>
  <td class="invoice" style="font-size: 16px;"><c:if test="${tempLineItem.customXml != null}">Custom Frame - </c:if><c:out value="${tempLineItem.product.name}"/>
    <table border="0" cellspacing="1" cellpadding="0">
	  <c:forEach items="${tempLineItem.productAttributes}" var="productAttribute" varStatus="productAttributeStatus">
		<tr class="invoice_tempLineItem_attributes${productAttributeStatus.count%2}">
		<td style="white-space: nowrap" align="right"><c:out value="${productAttribute.optionName}"/>: </td>
		<td style="width:100%;padding-left:5px;"><c:out value="${productAttribute.valueString}" escapeXml="false"/></td>
		</tr>
	  </c:forEach>
	  <c:forEach items="${lineItem.asiProductAttributes}" var="asiProductAttribute" varStatus="asiProductAttributeStatus">
		<tr class="invoice_lineitem_attributes${asiProductAttributeStatus.count%2}">
		<td style="white-space: nowrap" align="right"><c:out value="${asiProductAttribute.asiOptionName}"/>: </td>
		<td style="width:100%;padding-left:5px;"><c:out value="${asiProductAttribute.asiOptionValue}" escapeXml="false"/></td>
		</tr>
	  </c:forEach>
	  <%@ include file="/WEB-INF/jsp/frontend/common/customLinesInvoice.jsp"%>
    </table>
	<c:if test="${lineItemtempLineItemptionInterval != null}">
	  <c:set var="intervalType" value="${fn:substring(tempLineItem.subscriptionInterval, 0, 1)}"/>
	  <c:set var="intervalUnit" value="${fn:substring(tempLineItem.subscriptionInterval, 1, 2)}"/>
	  <div class="invoice_lineitem_subscription">
		<fmt:message key="subscription"/> <fmt:message key="details"/> : <fmt:message key="delivery"/>
		<c:choose>
		  <c:when test="${intervalUnit == '1'}"><fmt:message key="every_${intervalType}"><fmt:param value="${intervalUnit}"/></fmt:message></c:when>
		  <c:otherwise><fmt:message key="every_${intervalType}s"><fmt:param value="${intervalUnit}"/></fmt:message></c:otherwise>
		</c:choose>
	  </div>
	</c:if>
  <c:if test="${siteConfig['SHOW_IMAGE_ON_INVOICE'].value == 'true'}" >
  <c:forEach items="${tempLineItem.productAttributes}" var="productAttribute">
    <c:if test="${!empty productAttribute.imageUrl}" > 
      <c:if test="${!cartItem.product.thumbnail.absolute}"><img src="<c:url value="/assets/Image/Product/options/${productAttribute.imageUrl}"/>" border="0" class="invoiceOptionImage" /></c:if>
      <c:if test="${cartItem.product.thumbnail.absolute}"><img src="<c:url value="${productAttribute.imageUrl}"/>" border="0" class="invoiceOptionImage" /></c:if>
    </c:if>
  </c:forEach>
  </c:if> 
  </td>
  <td class="invoice" align="center"></td>
  </c:when>
  <c:otherwise>
    <td class="invoice"></td>
    <td class="invoice"></td>
    <td class="invoice"></td>
  </c:otherwise>
  </c:choose>  
  <c:forEach items="${orderForm.productFieldsHeader}" var="pFHeader" varStatus="status"><c:set var="check" value="0"/>
  <c:if test="${pFHeader.showOnInvoice}">
  <c:forEach items="${lineItem.productFields}" var="productField"> 
   <c:if test="${pFHeader.id == productField.id}">
    <td class="invoice"> </td><c:set var="check" value="1"/>
   </c:if>       
  </c:forEach>
  <c:if test="${check == 0}">
    <td class="invoice">&nbsp;</td>
   </c:if>
  </c:if>
  </c:forEach> 
  <c:if test="${siteConfig['ORDER_FULFILMENT'].value == 'true' and orderForm.customer.id == '20995'}" >
  	<td class="invoice" align="center">
  	<c:choose>
  	<c:when test="${tempLineItem.shippingDays == null or tempLineItem.shippingDays == ''}">
  		<fmt:message key="f_outOfStock" />
  	</c:when>
  	<c:when test="${tempLineItem.shippingDays == 0}">
  		<fmt:message key="f_immediately" />
  	</c:when>
  	<c:otherwise>
  		<c:out value="${tempLineItem.shippingDays}"/> Days
  	</c:otherwise>
  	</c:choose>	
  	</td>
  </c:if>
  
  
<c:choose>
  <c:when test="${showQtyAndPrice}">
  <td class="invoice" align="center" style="font-size: 16px;">
    <c:choose>
        <c:when test="${gSiteConfig['gVARIABLE_PRICE'] and tempLineItem.product.priceByCustomer}"></c:when>
        <c:otherwise>
          <c:out value="${tempLineItem.quantity}"/>
          <c:if test="${tempLineItem.priceCasePackQty != null}"><div class="casePackPriceQty">(<c:out value="${tempLineItem.priceCasePackQty}"/> per <c:out value="${siteConfig['PRICE_CASE_PACK_UNIT_TITLE'].value}" />)</div></c:if>
        </c:otherwise>
    </c:choose>       
  </td>
  <c:if test="${gSiteConfig['gINVENTORY'] && orderForm.order.hasLowInventoryMessage}">
	<td class="invoice" align="center"><c:out value="${tempLineItem.lowInventoryMessage}" /></td>
  </c:if>
  <c:choose>
	<c:when test="${siteConfig['CASE_CONTENT_LAYOUT'].value == '1'}">
		<c:choose>
	  		<c:when test="${tempLineItem.product.caseContent != null}">
	  	 		 <td class="invoice" align="right"><fmt:formatNumber value="${tempLineItem.unitPrice * tempLineItem.product.caseContent}" pattern="#,##0.00#" maxFractionDigits="${siteConfig['MAX_FRACTION_DIGIT'].value}"/></td>
	 	 	</c:when>
	 	 	<c:otherwise>
	 	 		<td class="invoice" align="right"><fmt:formatNumber value="${tempLineItem.unitPrice}" pattern="#,##0.00#" maxFractionDigits="${siteConfig['MAX_FRACTION_DIGIT'].value}"/></td>
	 	 	</c:otherwise>
	  	</c:choose>
	 </c:when>
	 <c:when test="${siteConfig['CASE_CONTENT_LAYOUT'].value == '2'}">
    	<td class="invoice" align="right"><fmt:formatNumber value="${tempLineItem.unitPrice}" pattern="#,##0.00#" maxFractionDigits="${siteConfig['MAX_FRACTION_DIGIT'].value}"/></td>
     </c:when>
	 <c:otherwise>
	 	<c:if test="${orderForm.order.hasPacking}">
		  <td class="invoice" align="center"><c:out value="${tempLineItem.product.packing}" /></td>
		  </c:if>
		  <c:if test="${orderForm.order.hasContent}">
		    <td class="invoice" align="center"><c:out value="${tempLineItem.product.caseContent}" /></td>
		  </c:if>
		  <td class="invoice" align="right"><fmt:formatNumber value="${tempLineItem.unitPrice}" pattern="#,##0.00#" maxFractionDigits="${siteConfig['MAX_FRACTION_DIGIT'].value}"/></td> 	
	 </c:otherwise>
  </c:choose>
  <c:if test="${orderForm.order.hasCustomShipping}">
	<td class="invoice" align="center">
		<c:if test="${!empty tempLineItem.customShippingCost}">
			<c:out value="${orderForm.order.customShippingTitle}" />
		</c:if>
	</td>
  </c:if>
  <td class="invoice" align="right" style="font-size: 16px;"><fmt:formatNumber value="${tempLineItem.tempItemTotal}" pattern="#,##0.00"/></td>
  </c:when>
  <c:otherwise>
    <c:if test="${gSiteConfig['gINVENTORY'] && orderForm.order.hasLowInventoryMessage}">
	  <td class="invoice" align="center"></td>
    </c:if>
    <td class="invoice" align="center"></td>
    <c:if test="${orderForm.order.hasCustomShipping}">
      <td class="invoice" align="center"></td>
    </c:if>
    <td class="invoice" align="center"></td>
  </c:otherwise>
</c:choose>

</tr>
  
