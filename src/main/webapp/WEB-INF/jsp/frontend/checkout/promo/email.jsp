<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="/WEB-INF/tlds/spring.tld" prefix="spring" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>

<tiles:insertDefinition name="${_template}" flush="true">
  <tiles:putAttribute name="content" type="string">
<link rel="stylesheet" href="${_contextpath}/assets/SmoothGallery/multiboxASI.css" type="text/css"/>
<script type="text/JavaScript">
<!--
var box = {};
window.addEvent('domready', function(){	
	box = new multiBox('mb0', {descClassName: 'multiBoxDesc',showNumbers: false,openFromLink: false});
	$('giftCard.code').addEvent('keyup',function(){
		//alert(1);
    	$('previewLink').href = "${_contextpath}/promoEmailPreview.jhtm?promo="+$('giftCard.code').value;
    	var requestHTMLData = new Request.HTML ({
    		url: "${_contextpath}/ajaxShowPromo.jhtm?promo="+$('giftCard.code').value,
    		update: $('promoAmount'),
        	onSuccess: function(response){ 
    		}
    	}).send();
    });
	$('giftCard.senderFirstName').addEvent('keyup',function(){
		generateUrl();
	});
	$('giftCard.senderLastName').addEvent('keyup',function(){
		generateUrl();
	});
	$('giftCard.recipientFirstName').addEvent('keyup',function(){
		generateUrl();
	});
	$('giftCard.recipientLastName').addEvent('keyup',function(){
		generateUrl();
	});
	$('giftCard.message').addEvent('keyup',function(){
		generateUrl();
	});
});
function generateUrl(){
	var urlAppend = "${_contextpath}/promoEmailPreview.jhtm?";
	if($('giftCard.code').value != null) {
		urlAppend = urlAppend +"promo="+$('giftCard.code').value;
	}
	if($('giftCard.senderFirstName').value != null) {
		urlAppend = urlAppend +"&sF="+$('giftCard.senderFirstName').value;
	}
	if($('giftCard.senderLastName').value != null) {
		urlAppend = urlAppend +"&sL="+$('giftCard.senderLastName').value;
	}
	if($('giftCard.recipientFirstName').value != null) {
		urlAppend = urlAppend +"&rF="+$('giftCard.recipientFirstName').value;
	}
	if($('giftCard.recipientLastName').value != null) {
		urlAppend = urlAppend +"&rL="+$('giftCard.recipientLastName').value;
	}
	if($('giftCard.message').value != null) {
		urlAppend = urlAppend +"&m="+$('giftCard.message').value;
	}
	$('previewLink').href = urlAppend;
}
//-->
</script>

<c:if test="${message != null}">
  <div class="message"><fmt:message key="${message}" />.</div>
</c:if>

<form:form  commandName="giftCardForm" method="post">
<input type="hidden" name="_page" value="0">
<table width="100%" border="0" cellpadding="3" class="giftcardBox">
  <tr>
    <td>
      <div id="giftCardEmailTitle">Email Gift Certificate</div>
	  <ol>
	    <li><fmt:message key="f_enterYourCouponCode"/>.
	    <table class="box">
	     <tr>
	      <td><fmt:message key="f_couponCode" />:</td>
	      <td><form:input path="giftCard.code"/><form:errors path="giftCard.code" cssClass="error"/><span id="promoAmount"></span></td>
	     </tr>
	    </table> 
	    </li>
	    <li><fmt:message key="f_enterToAndFrom" />
	    <table class="box">
	     <tr>
	      <td><fmt:message key="to" />:</td>
	      <td><form:input path="giftCard.recipientFirstName"/><div class="note"><form:errors path="giftCard.recipientFirstName" cssClass="error"/>(recipient's first name here)</div></td>
	      <td><form:input path="giftCard.recipientLastName"/><div class="note"><form:errors path="giftCard.recipientLastName" cssClass="error"/>(last name here)</div></td>
	     </tr>
	     <tr>
	      <td><fmt:message key="from" />:</td>
	      <td><form:input path="giftCard.senderFirstName"/><div class="note"><form:errors path="giftCard.senderFirstName" cssClass="error"/>(your first name here)</div></td>
	      <td><form:input path="giftCard.senderLastName"/><div class="note"><form:errors path="giftCard.senderLastName" cssClass="error"/>(last name here)</div></td>
	     </tr>
	    </table>
	    </li>
	    <li><fmt:message key="whatIsTheRecipientsEmailAddress" />
	    <table class="box">
	     <tr>
	      <td><fmt:message key="email" />:</td>
	      <td><form:input path="giftCard.recipientEmail"/><form:errors path="giftCard.recipientEmail" cssClass="error"/></td>
	     </tr>
	     <tr> 
	      <td><fmt:message key="confirmEmail" />:</td>
	      <td><form:input path="confirmEmail"/><form:errors path="confirmEmail" cssClass="error"/></td>
	     </tr>
	    </table>
	    </li>
	    <li><fmt:message key="wouldYouLikeToIncludeAMessageForTheRecipient" /> (<fmt:message key="optional" />)
	    <table class="box">
	     <tr>
	      <td><fmt:message key="message" />:</td>
	      <td><form:textarea path="giftCard.message" rows="9" cols="40"/></td>
	     </tr>
	    </table>
		</li>
	  </ol>
	</td>
    <td></td>
  </tr>  
  <tr align="center">
    <td colspan="2" id="buttonWrapper">
       	 <input type="submit" class="f_button" name="_cancel" value="<fmt:message key="cancel" />" />
	 	 <input type="submit" class="f_button" name="_send" value="<fmt:message key="send" />" />
	 	 <a id="previewLink" rel="width:850,height:400" id="mb0" class="mb0">
	 	 	<input type="button" class="f_button" name="_preview" value="<fmt:message key="preview" />" />
	 	 </a>
	</td>
  </tr>
  </table>
  
</form:form>

  </tiles:putAttribute>
</tiles:insertDefinition>