<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<c:choose>
  <c:when test="${model.promo != null}">
    <span class="couponAmount">Gift Certificate Amount: $<c:out value="${model.promo.discount}" /></span>
  </c:when>
  <c:otherwise>
      <span class="couponAmount">Invalid Voucher Code</span>
  </c:otherwise>
</c:choose>