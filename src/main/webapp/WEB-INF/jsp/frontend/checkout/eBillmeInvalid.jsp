<%@ page import="java.io.*,java.util.*" %>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>

<tiles:insertDefinition name="${_template}" flush="true">
  <tiles:putAttribute name="content" type="string">
  
Your order has been successfully placed.<br/>
Please click <a href="invoice.jhtm?order=${order.orderId}">here</a>.<br/>

  </tiles:putAttribute>
</tiles:insertDefinition>