<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>

<c:if test="${orderForm.customer.budgetPartnersList != null and fn:length(orderForm.customer.budgetPartnersList) gt 0 }" >
<table border="0" cellpadding="3" cellspacing="1" width="100%">
  <tr><td class="addressBook"><fmt:message key="f_earnedCredits" /></td></tr>
</table>

<c:forEach items="${orderForm.customer.budgetPartnersList}" var="partner">
<c:set value="partnerAmt_${partner.partnerId}" var="partnerAmt"/>
<c:if test="${partner.amount > 0.00 }">
  <div class="earnedCreditsWrapper">
  	  <div class="partnerName"><c:out value="${partner.partnerName}"/></div>
      <div class="partnerAmount">Available: <fmt:message key="${siteConfig['CURRENCY'].value}" /><fmt:formatNumber value="${partner.amount}" pattern="#,##0.00" /></div>
  	  <div class="apply"><input type="text" name="applyAmount_${partner.partnerId}" value="${model[partnerAmt]}" /></div>
  </div> 
</c:if>
</c:forEach>


</c:if>
