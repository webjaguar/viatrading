<%@ page import="com.webjaguar.model.*, java.text.*" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ taglib uri="/WEB-INF/tlds/custom.tld" prefix="wj" %>
<html>
<head>
<c:set value="${invoiceLayout.headTag}" var="headTag"/>
<c:set value="${fn:replace(headTag, '#order#', order.orderId)}" var="headTag"/>
<c:set value="${fn:replace(headTag, '#ordergrandtotal#', order.grandTotal)}" var="headTag"/>
<c:set value="${fn:replace(headTag, '#ordersubtotal#', order.subTotal)}" var="headTag"/>
<c:set value="${fn:replace(headTag, '#ordertotalquantity#', order.totalQuantity)}" var="headTag"/>
<c:set value="${fn:replace(headTag, '#orderdiscount#', order.promoAmount)}" var="headTag"/>
<c:out value="${headTag}" escapeXml="false"/>
</head> 
<body>

<c:set value="${invoiceLayout.headerHtml}" var="headerHtml"/>
<c:set value="${fn:replace(headerHtml, '#order#', order.orderId)}" var="headerHtml"/>
<c:set value="${fn:replace(headerHtml, '#ordergrandtotal#', order.grandTotal)}" var="headerHtml"/>
<c:set value="${fn:replace(headerHtml, '#ordersubtotal#', order.subTotal)}" var="headerHtml"/>
<c:set value="${fn:replace(headerHtml, '#ordertotalquantity#', order.totalQuantity)}" var="headerHtml"/>
<c:set value="${fn:replace(headerHtml, '#orderdiscount#', order.promoAmount)}" var="headerHtml"/>
<c:out value="${headerHtml}" escapeXml="false"/>

<form name="eBillmeForm" method="POST" action="<c:out value="${siteConfig['EBILLME_URL'].value}"/>">

<%
 String secureUrl = request.getScheme() + "://" + request.getHeader("host") + request.getContextPath() + "/"; 
%>
<input type="hidden" name="returnUrl" value="<%= secureUrl %>eBillme.jhtm">
<input type="hidden" name="cancelUrl" value="<c:out value="${siteConfig['EBILLME_CANCEL_URL'].value}"/>">
<input type="hidden" name="errorUrl" value="<c:out value="${siteConfig['EBILLME_ERROR_URL'].value}"/>">

<input type="hidden" name="apiversion" value="3.0">
<c:if test="${siteConfig['EBILLME_URL'].value == 'https://my.eBillme.com/Auth/'}">
<input type="hidden" name="transtype" value="P">
</c:if>
<c:if test="${siteConfig['EBILLME_URL'].value == 'https://test.modasolutions.com/Auth/'}">
<input type="hidden" name="transtype" value="T">
</c:if>
<input type="hidden" name="merchanttoken" value="<c:out value="${siteConfig['EBILLME_MERCHANTTOKEN'].value}"/>">
<input type="hidden" name="ordernumber" value="<c:out value="${order.orderId}"/>">
<input type="hidden" name="totalprice" value="<c:out value="${wj:decimalFormatString(order.balance,null)}" escapeXml="false"/>">
<input type="hidden" name="currency" value="<c:out value="${siteConfig['CURRENCY'].value}"/>">
<input type="hidden" name="commandtype" value="_wTrans">

<input type="hidden" name="ipaddress" value="<c:out value="${order.ipAddress}"/>">
 
<input type="hidden" name="buyerdetails_firstname" value="${order.billing.firstName}">
<input type="hidden" name="buyerdetails_lastname" value="${order.billing.lastName}">
<input type="hidden" name="buyerdetails_email" value="<c:out value="${username}"/>">
<input type="hidden" name="buyerdetails_address1" value="${order.billing.addr1}">
<input type="hidden" name="buyerdetails_address2" value="${order.billing.addr2}">
<input type="hidden" name="buyerdetails_city" value="${order.billing.city}">
<input type="hidden" name="buyerdetails_state" value="${order.billing.stateProvince}">
<input type="hidden" name="buyerdetails_country" value="${order.billing.country}">
<input type="hidden" name="buyerdetails_zipcode" value="${order.billing.zip}">
<input type="hidden" name="buyerdetails_phone1" value="${order.billing.phone}">
<input type="hidden" name="buyerdetails_merchantrating" value="<c:choose><c:when test="${orderForm.creditCardAttempts > 0}">2</c:when><c:otherwise>3</c:otherwise></c:choose>">

<input type="hidden" name="shippingdetails_firstname" value="${order.shipping.firstName}">
<input type="hidden" name="shippingdetails_lastname" value="${order.shipping.lastName}">
<input type="hidden" name="shippingdetails_email" value="<c:out value="${username}"/>">
<input type="hidden" name="shippingdetails_address1" value="${order.shipping.addr1}">
<input type="hidden" name="shippingdetails_address2" value="${order.shipping.addr2}">
<input type="hidden" name="shippingdetails_city" value="${order.shipping.city}">
<input type="hidden" name="shippingdetails_state" value="${order.shipping.stateProvince}">
<input type="hidden" name="shippingdetails_country" value="${order.shipping.country}">
<input type="hidden" name="shippingdetails_zipcode" value="${order.shipping.zip}">
<input type="hidden" name="shippingdetails_shippingmethod" value="STANDARD">

<noscript>
<input type="submit" value="Please click to continue paying by eBillme">
</noscript>


</form>
<c:set value="${invoiceLayout.footerHtml}" var="footerHtml"/>
<c:set value="${fn:replace(footerHtml, '#order#', order.orderId)}" var="footerHtml"/>
<c:set value="${fn:replace(footerHtml, '#ordergrandtotal#', order.grandTotal)}" var="footerHtml"/>
<c:set value="${fn:replace(footerHtml, '#ordersubtotal#', order.subTotal)}" var="footerHtml"/>
<c:set value="${fn:replace(footerHtml, '#ordertotalquantity#', order.totalQuantity)}" var="footerHtml"/>
<c:set value="${fn:replace(footerHtml, '#orderdiscount#', order.promoAmount)}" var="footerHtml"/>
<c:out value="${footerHtml}" escapeXml="false"/>

<%@ include file="/WEB-INF/jsp/frontend/checkout/eBillmeCustom.jsp" %>
</body>
<script type="text/javascript" language="JavaScript">
<!--
document.eBillmeForm.submit();
//-->
</script>
</html>
