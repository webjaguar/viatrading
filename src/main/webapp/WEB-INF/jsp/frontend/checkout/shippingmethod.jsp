<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ taglib uri="/WEB-INF/tlds/spring.tld" prefix="spring" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>

<tiles:insertDefinition name="${_template}" flush="true">
  <tiles:putAttribute name="content" type="string">
  
<div><c:out value="${model.shippingLayout.headerHtml}" escapeXml="false"/></div>  

<div style="width:95%;margin:auto;">

<table border="0" cellpadding="3" cellspacing="1" width="100%">
  <tr><td colspan="2" class="addressBook"><fmt:message key="shoppingcart.shipTo" /></td></tr>
  <tr><td>
    <b><c:out value="${orderForm.order.shipping.firstName}"/> <c:out value="${orderForm.order.shipping.lastName}"/></b><br />
    <span class="addressBook"><c:if test="${orderForm.order.shipping.company != ''}"><c:out value="${orderForm.order.shipping.company}"/>,</c:if>
      <c:out value="${orderForm.order.shipping.addr1}"/><c:if test="${!empty orderForm.order.shipping.addr2}"> <c:out value="${orderForm.order.shipping.addr2}"/></c:if>,
      <c:out value="${orderForm.order.shipping.city}"/>, 
      <c:out value="${orderForm.order.shipping.stateProvince}"/> <c:out value="${orderForm.order.shipping.zip}"/>,
      <c:out value="${countries[orderForm.order.shipping.country]}" /></span>
  </td>
  <td align="right" id="anotherShipToId">
    <form action="checkout.jhtm" method="post">
    <input type="submit" value="<fmt:message key="shoppingcart.shipToAnotherAddress" />" name="_target0" class="f_button" /> 
    <input type="hidden" name="newShippingAddress" value="true"> 
    </form>
  </td>
  </tr>
</table> 
<p>

<form action="checkout.jhtm" method="post" name="shippingForm">
<c:if test="${gSiteConfig['gWEATHER_CHANNEL'] and (orderForm.order.weather != null) }" >
<table border="0" cellpadding="3" cellspacing="1" width="100%">
  <tr><td class="addressBook"><fmt:message key="weather" /></td></tr>
</table>
<table cellpadding="5" cellspacing="5" bgcolor="#DDDDDD">
<tr align="center">
<td>
  <img src="assets/Image/TWC/32x32/<c:out value='${orderForm.order.weather.ccIcon}' />.png" /> <br />
  <c:out value="${orderForm.order.weather.ccTextCondition}" />   
</td>
<td>
  <h4 style="color:#F46227">High</h4> <br /><c:out value="${orderForm.order.weather.high}" />&deg; F
</td>
<td>
  <h4 style="color:#00B2EB">Low</h4> <br /><c:out value="${orderForm.order.weather.low}" />&deg; F
</td>
<td><div style="float:left"><a href="http://www.weather.com/?prod=xoap&par=1044739873" target="blank"><img border="0" src="assets/Image/TWC/logos/TWClogo_64px.png" /></a></div></td>
</tr>
</table>
</c:if>

<table border="0" cellpadding="3" cellspacing="1" width="100%">
  <tr><td class="addressBook"><fmt:message key="shippingMethod" /></td></tr>
</table>
<c:if test="${orderForm.order.shippingMessage != null and !empty orderForm.order.shippingMessage}">
<div class="shippingMessage" id="shippingMessageId">
<c:out value="${orderForm.order.shippingMessage}" escapeXml="false"></c:out>
</div>
</c:if>
<c:if test="${orderForm.customRatesList != null and orderForm.order.hasRegularShipping}">
<br/><br/>
 <table border="0" cellpadding="2" cellspacing="0" width="100%" class="shoppingCart">
  <tr class="shoppingCart">
	<c:set var="cols" value="0"/>
    <th width="5%" class="invoice"><fmt:message key="line" />#</th>
    <th class="invoice"><fmt:message key="productSku" /></th>
    <th class="invoice"><fmt:message key="productName" /></th>
    <th width="10%" class="invoice"><fmt:message key="quantity" /></th>
    <c:if test="${gSiteConfig['gINVENTORY'] && orderForm.order.hasLowInventoryMessage}">
      <c:set var="cols" value="${cols+1}"/>
	  <th width="10%" class="invoice"><fmt:message key="lowInventory" /></th>
	</c:if>
    <c:if test="${orderForm.order.hasPacking}">
	  <c:set var="cols" value="${cols+1}"/>
      <th width="10%" class="invoice"><fmt:message key="packing" /></th>
    </c:if>
    <c:if test="${orderForm.order.hasContent}">
	  <c:set var="cols" value="${cols+1}"/>
      <th class="invoice"><fmt:message key="content" /></th>
    </c:if>
    <th class="invoice"><fmt:message key="productPrice" /><c:if test="${orderForm.order.hasContent}">/<c:out value="${siteConfig['CASE_UNIT_TITLE'].value}" /></c:if></th>
    <th class="invoice"><fmt:message key="total" /></th>
  </tr>
<c:forEach var="lineItem" items="${orderForm.order.lineItems}" varStatus="lineItemStatus">
<c:if test="${empty lineItem.customShippingCost}">
<tr valign="top" class="shoppingCart${lineItemStatus.index % 2}">
  <td class="invoice" align="center"><c:out value="${lineItem.lineNumber}"/></td>
  <td class="invoice">
    <c:if test="${siteConfig['SHOW_IMAGE_ON_INVOICE'].value == 'true' and lineItem.product.thumbnail != null}">
      <img class="cartImage" src="<c:if test="${!lineItem.product.thumbnail.absolute}">assets/Image/Product/thumb/</c:if>${lineItem.product.thumbnail.imageUrl}" border="0" />
    </c:if><c:out value="${lineItem.product.sku}"/></td>
  <td class="invoice"><c:out value="${lineItem.product.name}"/>
    <table border="0" cellspacing="1" cellpadding="0">
	  <c:forEach items="${lineItem.productAttributes}" var="productAttribute" varStatus="productAttributeStatus">
		<tr class="invoice_lineitem_attributes${productAttributeStatus.count%2}">
		<td class="optionName" style="white-space: nowrap" align="right"><c:out value="${productAttribute.optionName}"/>: </td>
		<td class="optionValue" style="padding-left:5px;"><c:out value="${productAttribute.valueString}" escapeXml="false"/></td>
		<c:if test="${(! empty productAttribute.optionPriceOriginal) && productAttribute.optionPriceOriginal != 0}">
		<td class="optionPrice"><fmt:formatNumber value="${productAttribute.optionPriceOriginal}" pattern="#,##0.00" /></td>
		<td class="optionNote" style="padding-left:5px;"><fmt:message key="${productAttribute.optionPriceMessageF}"/></td>
		</c:if>
		</tr>
	  </c:forEach>
	  <%@ include file="/WEB-INF/jsp/frontend/common/customLinesInvoice.jsp"%>
    </table>
  <c:if test="${siteConfig['SHOW_IMAGE_ON_INVOICE'].value == 'true' and lineItem.hasProductAttributesImage}" >
  <c:forEach items="${lineItem.productAttributes}" var="productAttribute">
    <c:if test="${!empty productAttribute.imageUrl}" > 
      <c:if test="${!cartItem.product.thumbnail.absolute}"><img src="<c:url value="/assets/Image/Product/options/${productAttribute.imageUrl}"/>" border="0" class="invoiceOptionImage" /></c:if>
      <c:if test="${cartItem.product.thumbnail.absolute}"><img src="<c:url value="${productAttribute.imageUrl}"/>" border="0" class="invoiceOptionImage" /></c:if>
    </c:if>
  </c:forEach>
  </c:if> 
  </td>  
  <td class="invoice" align="center"><c:out value="${lineItem.quantity}"/></td>
  <c:if test="${gSiteConfig['gINVENTORY'] && orderForm.order.hasLowInventoryMessage}">
	<td class="invoice" align="center"><c:out value="${lineItem.lowInventoryMessage}" /></td>
  </c:if>
  <c:if test="${orderForm.order.hasPacking}">
  <td class="invoice" align="center"><c:out value="${lineItem.product.packing}" /></td>
  </c:if>
  <c:if test="${orderForm.order.hasContent}">
    <td class="invoice" align="center"><c:out value="${lineItem.product.caseContent}" /></td>
  </c:if>
  <td class="invoice" align="right"><fmt:formatNumber value="${lineItem.unitPrice}" pattern="#,##0.00#" maxFractionDigits="${siteConfig['MAX_FRACTION_DIGIT'].value}"/></td>
  <td class="invoice" align="right"><fmt:formatNumber value="${lineItem.totalPrice}" pattern="#,##0.00"/></td>
 </tr>
 </c:if>
 </c:forEach>
 </table>
</c:if>

<c:if test="${orderForm.order.hasRegularShipping}">
<spring:bind path="orderForm.shippingRateIndex">
<span class="error"><c:out value="${status.errorMessage}"/></span>
<table border="0" cellpadding="2">
  <c:forEach items="${orderForm.ratesList}" var="shippingrate" varStatus="loopStatus">
    <c:set var="class" value="customShipping"/>
    <c:if test="${shippingrate.carrier != null }">
      <c:set var="class" value="regularShipping"/>
    </c:if>
    <tr class="${class}">
      <td align="center" height="45px">
        <c:if test="${shippingrate.carrier == 'fedex'}" >
        <c:set var="fedEx" value="true" />
        <c:choose>
         <c:when test="${fn:contains(shippingrate.code,'EXPRESS') or fn:contains(shippingrate.code,'OVERNIGHT') or fn:contains(shippingrate.code,'2_DAY')}"><img width="80%" src="assets/Image/Layout/FedEx_Express_Normal_2Color_Positive_20.gif" /></c:when>
         <c:when test="${fn:contains(shippingrate.code,'GROUND_HOME')}"><img width="80%" src="assets/Image/Layout/FedEx_HomeDelivery_Normal_2Color_Positive_20.gif" /></c:when>
         <c:when test="${fn:contains(shippingrate.code,'GROUND')}"><img width="80%" src="assets/Image/Layout/FedEx_Ground_Normal_2Color_Positive_20.gif" /></c:when>
         <c:when test="${fn:contains(shippingrate.code,'FREIGHT')}"><img width="80%" src="assets/Image/Layout/FedEx_Freight_Normal_2Color_Positive_20.gif" /></c:when>
         <c:otherwise><img width="80%" src="assets/Image/Layout/FedEx_MultipleServices_Normal_2Color_Positive_20.gif" /></c:otherwise>
        </c:choose>
        </c:if>
        <c:if test="${shippingrate.carrier == 'ups'}" >
         <c:set var="ups" value="true" />
         <img width="30%" src="assets/Image/Layout/UPS_LOGO_S.gif" />
        </c:if>
      </td>
      <td>
       <input type="radio" id="scr_${loopStatus.index}" name="<c:out value="${status.expression}"/>" value="${loopStatus.index}" <c:if test="${(status.value == loopStatus.index) || (shippingrate.title == 'Free')}">checked</c:if>>
      </td>
      <td>
        <c:out value="${shippingrate.title}" escapeXml="false"/>
      </td>
      <td align="right">
        <c:if test="${shippingrate.price != null}"><fmt:message key="${siteConfig['CURRENCY'].value}" /></c:if><fmt:formatNumber value="${shippingrate.price}" pattern="#,##0.00"/>
      </td>
      <c:if test="${siteConfig['DELIVERY_TIME'].value == 'true' and shippingrate.carrier == 'ups'}">
      <td style="padding-left:10px;"align="center">
        <c:choose>
 	 	  <c:when test="${shippingrate.code == '03' and empty shippingrate.deliveryDate}"><a onclick="window.open(this.href,'','width=740,height=550'); return false;" href="category.jhtm?cid=49">See Map</a></c:when>
 	 	  <c:when test="${shippingrate.code == '03' and !empty shippingrate.deliveryDate}">Delivery Date: <fmt:formatDate type="date" timeStyle="default" value="${shippingrate.deliveryDate}"/><div style="font-size:10px;color:#444444;">( Not Guaranteed )</div></c:when>
 	 	  <c:otherwise>Delivery Date: <fmt:formatDate type="date" timeStyle="default" value="${shippingrate.deliveryDate}"/></c:otherwise>
 		</c:choose>
      </td>
      </c:if> 
      <c:if test="${shippingrate.description != null}">
      <td>
        <c:out value="${shippingrate.description}" escapeXml="false"/>
      </td>
      </c:if>
    </tr>
  </c:forEach>
</table>
</spring:bind>
<c:if test="${fedEx}">
 <c:out value="FedEx<sup>&#174;</sup> service marks are owned by Federal Express Corporation and used with permission." escapeXml="false"/>
</c:if>
<c:if test="${ups}"><br />
 <c:out value="UPS, the UPS brand mark, and the Color Brown are trademarks of 
United Parcel Service of America, Inc. All Rights Reserved." escapeXml="false"/>
</c:if>
</c:if>


<c:if test="${orderForm.customRatesList != null}">
<c:if test="${orderForm.order.hasRegularShipping}">
<br/><br/>
<table border="0" cellpadding="2" cellspacing="0" width="100%" class="shoppingCart">
  <tr class="shoppingCart">
	<c:set var="cols" value="0"/>
    <th width="5%" class="invoice"><fmt:message key="line" />#</th>
    <th class="invoice"><fmt:message key="productSku" /></th>
    <th class="invoice"><fmt:message key="productName" /></th>
    <th width="10%" class="invoice"><fmt:message key="quantity" /></th>
    <c:if test="${gSiteConfig['gINVENTORY'] && orderForm.order.hasLowInventoryMessage}">
      <c:set var="cols" value="${cols+1}"/>
	  <th width="10%" class="invoice"><fmt:message key="lowInventory" /></th>
	</c:if>
    <c:if test="${orderForm.order.hasPacking}">
	  <c:set var="cols" value="${cols+1}"/>
      <th width="10%" class="invoice"><fmt:message key="packing" /></th>
    </c:if>
    <c:if test="${orderForm.order.hasContent}">
	  <c:set var="cols" value="${cols+1}"/>
      <th class="invoice"><fmt:message key="content" /></th>
    </c:if>
    <th class="invoice"><fmt:message key="productPrice" /><c:if test="${orderForm.order.hasContent}">/<c:out value="${siteConfig['CASE_UNIT_TITLE'].value}" /></c:if></th>
    <th class="invoice"><fmt:message key="total" /></th>
  </tr>
<c:forEach var="lineItem" items="${orderForm.order.lineItems}" varStatus="lineItemStatus">
<c:if test="${!empty lineItem.customShippingCost}">
<tr valign="top" class="shoppingCart${lineItemStatus.index % 2}">
  <td class="invoice" align="center"><c:out value="${lineItem.lineNumber}"/></td>
  <td class="invoice">
    <c:if test="${siteConfig['SHOW_IMAGE_ON_INVOICE'].value == 'true' and lineItem.product.thumbnail != null}">
      <img class="cartImage" src="<c:if test="${!lineItem.product.thumbnail.absolute}">assets/Image/Product/thumb/</c:if>${lineItem.product.thumbnail.imageUrl}" border="0" />
    </c:if><c:out value="${lineItem.product.sku}"/></td>
  <td class="invoice"><c:out value="${lineItem.product.name}"/>
    <table border="0" cellspacing="1" cellpadding="0">
	  <c:forEach items="${lineItem.productAttributes}" var="productAttribute" varStatus="productAttributeStatus">
		<tr class="invoice_lineitem_attributes${productAttributeStatus.count%2}">
		<td class="optionName" style="white-space: nowrap" align="right"><c:out value="${productAttribute.optionName}"/>: </td>
		<td class="optionValue" style="padding-left:5px;"><c:out value="${productAttribute.valueString}" escapeXml="false"/></td>
		<c:if test="${(! empty productAttribute.optionPriceOriginal) && productAttribute.optionPriceOriginal != 0}">
		<td class="optionPrice"><fmt:formatNumber value="${productAttribute.optionPriceOriginal}" pattern="#,##0.00" /></td>
		<td class="optionNote" style="padding-left:5px;"><fmt:message key="${productAttribute.optionPriceMessageF}"/></td>
		</c:if>
		</tr>
	  </c:forEach>
	  <%@ include file="/WEB-INF/jsp/frontend/common/customLinesInvoice.jsp"%>
    </table>
  <c:if test="${siteConfig['SHOW_IMAGE_ON_INVOICE'].value == 'true' and lineItem.hasProductAttributesImage}" >
  <c:forEach items="${lineItem.productAttributes}" var="productAttribute">
    <c:if test="${!empty productAttribute.imageUrl}" > 
      <c:if test="${!cartItem.product.thumbnail.absolute}"><img src="<c:url value="/assets/Image/Product/options/${productAttribute.imageUrl}"/>" border="0" class="invoiceOptionImage" /></c:if>
      <c:if test="${cartItem.product.thumbnail.absolute}"><img src="<c:url value="${productAttribute.imageUrl}"/>" border="0" class="invoiceOptionImage" /></c:if>
    </c:if>
  </c:forEach>
  </c:if> 
  </td>  
  <td class="invoice" align="center"><c:out value="${lineItem.quantity}"/></td>
  <c:if test="${gSiteConfig['gINVENTORY'] && orderForm.order.hasLowInventoryMessage}">
	<td class="invoice" align="center"><c:out value="${lineItem.lowInventoryMessage}" /></td>
  </c:if>
  <c:if test="${orderForm.order.hasPacking}">
  <td class="invoice" align="center"><c:out value="${lineItem.product.packing}" /></td>
  </c:if>
  <c:if test="${orderForm.order.hasContent}">
    <td class="invoice" align="center"><c:out value="${lineItem.product.caseContent}" /></td>
  </c:if>
  <td class="invoice" align="right"><fmt:formatNumber value="${lineItem.unitPrice}" pattern="#,##0.00"/></td>
  <td class="invoice" align="right"><fmt:formatNumber value="${lineItem.totalPrice}" pattern="#,##0.00"/></td>
</tr>
</c:if>
</c:forEach>
</table>
</c:if>
<spring:bind path="orderForm.customShippingRateIndex">
<span class="error"><c:out value="${status.errorMessage}"/></span>
<table border="0" cellpadding="2">
  <c:forEach items="${orderForm.customRatesList}" var="shippingrate" varStatus="loopStatus">
    <tr>
      <td>
        <input type="radio" id="ccr_${loopStatus.index}" name="<c:out value="${status.expression}"/>" value="${loopStatus.index}" <c:if test="${status.value == loopStatus.index}">checked</c:if>>
      </td>
      <td>
        <c:out value="${shippingrate.title}" escapeXml="false"/>
      </td>
      <td align="right">
        <c:if test="${shippingrate.price != null}"><fmt:message key="${siteConfig['CURRENCY'].value}" /></c:if><fmt:formatNumber value="${shippingrate.price}" pattern="#,##0.00"/>
      </td>  
    </tr>
  </c:forEach>
</table>
</spring:bind>
</c:if>

<c:if test="${siteConfig['ORDER_FULFILMENT'].value == 'true' and orderForm.customer.id == '20995'}">
<table border="0" cellpadding="3" cellspacing="1" width="100%">
  <tr><td colspan="2" class="addressBook"><fmt:message key="f_shippingDates" /></td></tr>
</table>

<c:forEach items="${model.brandName}" var="brandName1" varStatus="loopStatus">
<b><c:out value="${brandName1}" /></b>

<!-- Error Message -->
<c:if test="${model.message != null}">
 <c:set value="treshold_${brandName1}" var="treshold"/> 
 <c:if test="${model[treshold] == true}" >
 	<div class="message">Brand: <c:out value="${brandName1}"/></div> 	
 	<div class="message"><fmt:message key="${model.message}" /></div>
 </c:if>
</c:if>

<c:set var="days_immediate" value="immediate_${brandName1}"></c:set>
<table border="0" cellpadding="2" >	
<tr class="shoppingCart">
	<th width="5%">Image</th>
	<th width="5%" class="">SKU</th>
	<th width="5%" class="">Product Name</th>
	<th width="5%" class="">Price</th>
	<th width="5%" class="">Immediate<br><c:set var="days_immediate" value="immediate_${brandName1}"></c:set>
		<c:if test="${model[days_immediate] < 200 and model[days_immediate] > 0.0}"><div style="color:#FF0000">$<c:out value="${model[days_immediate]}"/></div></c:if>
		<c:if test="${model[days_immediate] > 200}">$<c:out value="${model[days_immediate]}"/></c:if>
	</th>	
	<th width="5%" class="">15 Days<br><c:set var="days_15" value="days_15_${brandName1}"></c:set>
		<c:if test="${model[days_15] < 200 and model[days_15] > 0.0}"><div style="color:#FF0000">$<c:out value="${model[days_15]}"/></div></c:if>
		<c:if test="${model[days_15] > 200}">$<c:out value="${model[days_15]}"/></c:if>
	</th>
	<th width="5%" class="">30 Days<br><c:set var="days_30" value="days_30_${brandName1}"></c:set>
		<c:if test="${model[days_30] < 200 and model[days_30] > 0.0}"><div style="color:#FF0000">$<c:out value="${model[days_30]}"/></div></c:if>
		<c:if test="${model[days_30] > 200}">$<c:out value="${model[days_30]}"/></c:if>
	</th>
	<th width="5%" class="">60 Days<br><c:set var="days_60" value="days_60_${brandName1}"></c:set>
		<c:if test="${model[days_60] < 200 and model[days_60] > 0.0}"><div style="color:#FF0000">$<c:out value="${model[days_60]}"/></div></c:if>
		<c:if test="${model[days_60] > 200}">$<c:out value="${model[days_60]}"/></c:if>
	</th>
	<th width="5%" class="">90 Days<br><c:set var="days_90" value="days_90_${brandName1}"></c:set>
		<c:if test="${model[days_90] < 200 and model[days_90] > 0.0}"><div style="color:#FF0000">$<c:out value="${model[days_90] }"/></div></c:if>
		<c:if test="${model[days_90] > 200}">$<c:out value="${model[days_90] }"/></c:if>
	</th>
	<th width="5%" class="">120 Days<br><c:set var="days_120" value="days_120_${brandName1}"></c:set>
		<c:if test="${model[days_120] < 200 and model[days_120] > 0.0}"><div style="color:#FF0000">$<c:out value="${model[days_120] }"/></div></c:if>
		<c:if test="${model[days_120] > 200}">$<c:out value="${model[days_120] }"/></c:if>
	</th>
	<th width="5%" class="">Out of Stock<br>$<c:set var="outOfStockPrice" value="outOfStockPrice_${brandName1}"></c:set><c:out value="${model[outOfStockPrice]}" /></th>
</tr>



<c:forEach items="${model.cartItems}" var="cartItems" >

<c:if test="${brandName1 == cartItems.product.manufactureName}">
<c:set value="${brandName1}" var="brandName" /> 
<tr  class="shoppingCart1">
	<td>
		<img class="cartImage" src="<c:if test="${!cartItems.product.thumbnail.absolute}">${_contextpath}/assets/Image/Product/thumb/</c:if>${cartItems.product.thumbnail.imageUrl}" border="0" />
	</td>
	<td>
		<c:out value="${cartItems.product.sku}"></c:out>
	</td>
	<td>
		<c:out value="${cartItems.product.name}"></c:out>
	</td>	
	<td>
		<fmt:message key="${siteConfig['CURRENCY'].value}" /><fmt:formatNumber value="${cartItems.totalPrice}" pattern="#,##0.00"/>
	</td>
	
	<c:set var="show" value='0'/>
	<c:if test="${cartItems.product.field6 == 'Available' }">	
		<c:set var="show" value='1'/>
		<td>
	       <input  type="radio"  name="${cartItems.product.id}_${loopStatus.index}" value="0" <c:if test="${cartItems.shippingDays == 0}">checked="checked"</c:if>>
	    </td>
    </c:if>
    <c:if test="${!(cartItems.product.field6 == 'Available')}">	
		<td>&nbsp;</td>
    </c:if>
	<c:if test="${cartItems.product.field6 == 'Available In 15 Days' or show == '1'}">
		<c:set var="show" value='1'/>
		<td>
	       <input type="radio" name="${cartItems.product.id}_${loopStatus.index}" value="15" <c:if test="${cartItems.shippingDays == 15 }">checked="checked"</c:if>>
	    </td>
    </c:if>
    <c:if test="${!(cartItems.product.field6 == 'Available In 15 Days' or show == '1')}">	
		<td>&nbsp;</td>
    </c:if>
    <c:if test="${cartItems.product.field6 == 'Available In 30 Days' or show == '1'}">
    	<c:set var="show" value='1'/>
		<td>
	       <input type="radio" name="${cartItems.product.id}_${loopStatus.index}" value="30" <c:if test="${cartItems.shippingDays == 30 }">checked="checked"</c:if> >
	    </td>
    </c:if>
    <c:if test="${!(cartItems.product.field6 == 'Available In 30 Days' or show == '1')}">	
		<td>&nbsp;</td>
    </c:if>
    <c:if test="${cartItems.product.field6 == 'Available In 60 Days' or show == '1'}">
    	<c:set var="show" value='1'/>
		<td>
	       <input type="radio" name="${cartItems.product.id}_${loopStatus.index}" value="60" <c:if test="${cartItems.shippingDays == 60 }">checked="checked"</c:if>>
	    </td>
    </c:if>
    <c:if test="${!(cartItems.product.field6 == 'Available In 60 Days' or show == '1')}">	
		<td>&nbsp;</td>
    </c:if>
    <c:if test="${cartItems.product.field6 == 'Available In 90 Days' or show == '1'}">
    	<c:set var="show" value='1'/>
		<td>
	       <input  type="radio" name="${cartItems.product.id}_${loopStatus.index}"  value="90" <c:if test="${cartItems.shippingDays == 90 }">checked="checked"</c:if>>
	    </td>
    </c:if>
    <c:if test="${!(cartItems.product.field6 == 'Available In 90 Days' or show == '1')}">	
		<td>&nbsp;</td>
    </c:if>
    <c:if test="${cartItems.product.field6 == 'Available In 120 Days' or show == '1'}">
    	<c:set var="show" value='1'/>
		<td>
	       <input type="radio" name="${cartItems.product.id}_${loopStatus.index}"  value="120" <c:if test="${cartItems.shippingDays == 120 }">checked="checked"</c:if>>
	    </td>
    </c:if>
    <c:if test="${!(cartItems.product.field6 == 'Available In 120 Days' or show == '1')}">
		<td>&nbsp;</td>
    </c:if>
    <c:if test="${cartItems.product.field6 == null or cartItems.product.field6 == 'Temporarily Out Of Stock' or cartItems.product.field6 == 'Out Of Stock' or cartItems.product.field6 == ''}">
		<td>
	      <input  type="radio" name="${cartItems.product.id}_${loopStatus.index}" checked="checked" >
	   </td>
    </c:if>
    <c:if test="${cartItems.product.field6 != null and cartItems.product.field6 != 'Temporarily Out Of Stock' and cartItems.product.field6 !='Out Of Stock' and cartItems.product.field6 != '' }">
	     <td>&nbsp;</td>
    </c:if>
</tr> 
</c:if> 
</c:forEach> 
</table>
</c:forEach>
</c:if>



<div align="center" class="nbButton" style="margin:5px;">
<c:if test="${siteConfig['ORDER_FULFILMENT'].value == 'true' and orderForm.customer.id == '20995'}">
<div align="right" class="next"><input type="submit" name="recalculate" value="Recalculate"></div><br/>
</c:if>
<div align="right" class="next"><input type="image" src="assets/Image/Layout/button_next${_lang}.gif" name="_target3"></div>
<div align="left" class="back" style="display:none;"><input type="image" src="assets/Image/Layout/button_prev${_lang}.gif" name="_viewcart"></div>
</div>
</form>
</div>
<c:out value="${model.shippingLayout.footerHtml}" escapeXml="false"/>

</tiles:putAttribute>
</tiles:insertDefinition>
