<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ taglib uri="/WEB-INF/tlds/spring.tld" prefix="spring" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<c:out value="${invoiceLayout.headTag}" escapeXml="false"/>
</head> 
   
<body class="invoice">
<div style="width:700px;margin:auto;">

<c:out value="${invoiceLayout.headerHtml}" escapeXml="false"/>
<table class="invoiceHeader">
<tr valign="top">
<td>
<form:form commandName="orderForm"  action="ppProCheckout.jhtm" method="post">
<input type="hidden" name="newBillingAddress" value="true">
<b><fmt:message key="billingInformation" />:</b>
<c:set value="${orderForm.order.billing}" var="address"/>
<c:set value="true" var="billing" />
<%@ include file="/WEB-INF/jsp/frontend/common/address.jsp" %>
<c:set value="false" var="billing" />
<c:if test="${orderForm =='ppp_UseMyAccount'}">
	<input type="image" border="0" name="_target2" src="assets/Image/Layout/button_change${_lang}.gif">
</c:if>
</form:form>
</td>
<td>&nbsp;</td>
<td>
<form:form commandName="orderForm"  action="ppProCheckout.jhtm" method="post">
<input type="hidden" name="newShippingAddress" value="true"> 
<b><fmt:message key="shippingInformation" />:</b>
<c:set value="${orderForm.order.shipping}" var="address"/>
<%@ include file="/WEB-INF/jsp/frontend/common/address.jsp" %>
<c:if test="${method =='ppp_UseMyAccount'}">
	<input type="image" border="0" name="_target0" src="assets/Image/Layout/button_change${_lang}.gif">
</c:if>
</form:form>
</td>
<c:if test="${siteConfig['DELIVERY_TIME'].value == 'true' and !empty orderForm.order.dueDate}">
<td>&nbsp;</td>
<td>
<b><fmt:message key="f_deliveryDate" />:</b>
<div class="deliveryDate"><fmt:formatDate type="date" timeStyle="default" value="${orderForm.order.dueDate}"/></div>
</td>
</c:if>
</tr>
</table>
<br/><br/>
<div id="helperHeadBox">
  <div id="emailAddress"><b><fmt:message key="emailAddress" />:</b>&nbsp;<c:out value="${orderForm.username}"/></div>  
</div>
<div style="clear:both;"></div>
<form:form commandName="orderForm" action="ppProCheckout.jhtm" method="post">
<input type="hidden" name="_centinelDebug" value="false">
<table border="0" cellpadding="2" cellspacing="1" width="100%" class="invoice">
  <tr>
	<c:set var="cols" value="0"/>
    <th width="5%" class="invoice"><fmt:message key="line" />#</th>
    <th class="invoice"><fmt:message key="productSku" /></th>
    <th class="invoice"><fmt:message key="productName" /></th>
    <c:forEach items="${orderForm.productFieldsHeader}" var="productField">
      <c:if test="${productField.showOnInvoice}">
      <th class="invoice"><c:out value="${productField.name}" /></th><c:set var="cols" value="${cols+1}"/>
      </c:if>
    </c:forEach>
    <c:if test="${siteConfig['ORDER_FULFILMENT'].value == 'true' and orderForm.customer.id == '20995'}">
      <c:set var="cols" value="${cols+1}"/>
      <th class="invoice"><fmt:message key="shippingDays" /></th>
    </c:if>
    <th width="10%" class="invoice"><fmt:message key="quantity" /></th>
    <c:if test="${gSiteConfig['gINVENTORY'] && orderForm.order.hasLowInventoryMessage}">
      <c:set var="cols" value="${cols+1}"/>
	  <th width="10%" class="invoice"><fmt:message key="lowInventory" /></th>
	</c:if>
	<c:if test="${siteConfig['CASE_CONTENT_LAYOUT'].value == ''}">	
	    <c:if test="${orderForm.order.hasPacking}">
		  <c:set var="cols" value="${cols+1}"/>
	      <th width="10%" class="invoice"><fmt:message key="packing" /></th>
	    </c:if>
	    <c:if test="${orderForm.order.hasContent}">
		  <c:set var="cols" value="${cols+1}"/>
	      <th class="invoice"><fmt:message key="content" /></th>
	    </c:if>
	</c:if>	 
	<c:if test="${orderForm.order.hasCustomShipping}">
		<c:set var="cols" value="${cols+1}"/>
		<th class="invoice"><fmt:message key="customShipping" /></th>
	</c:if>
    <%-- <th class="invoice"><fmt:message key="productPrice" /><c:if test="${orderForm.order.hasContent}">/<c:out value="${siteConfig['CASE_UNIT_TITLE'].value}" /></c:if></th>--%>
    <c:choose>
      <c:when test="${siteConfig['CASE_CONTENT_LAYOUT'].value == ''}">
          <th class="invoice"><div><fmt:message key="productPrice" /><c:if test="${orderForm.order.hasContent}">/<c:out value="${siteConfig['CASE_UNIT_TITLE'].value}" /></c:if></div></th>
      </c:when>
      <c:otherwise>
          <th class="invoice"><div><fmt:message key="productPrice" /></div></th>
      </c:otherwise>
    </c:choose>
    
    <c:if test="${orderForm.order.lineItemPromos != null and fn:length(orderForm.order.lineItemPromos) gt 0}">
      <c:set var="cols" value="${cols+1}"/>
	  <th class="invoice"><fmt:message key="discount" /></th>
    </c:if>
    
    <th class="invoice"><fmt:message key="total" /></th>
  </tr>  
<c:forEach var="lineItem" items="${orderForm.order.lineItems}">
<tr valign="top">
  <td class="invoice" align="center"><c:out value="${lineItem.lineNumber}"/></td>
  <td class="invoice">
	<c:if test="${lineItem.customImageUrl != null}">
      <a href="framer/pictureframer/images/FRAMED/${lineItem.customImageUrl}" onclick="window.open(this.href,'','width=640,height=480,resizable=yes'); return false;"><img class="invoiceImage" src="framer/pictureframer/images/FRAMED/${lineItem.customImageUrl}" border="0" /></a>
    </c:if>  
    <c:if test="${siteConfig['SHOW_IMAGE_ON_INVOICE'].value == 'true' and lineItem.product.thumbnail != null}">
      <img class="invoiceImage" src="<c:if test="${!lineItem.product.thumbnail.absolute}">assets/Image/Product/thumb/</c:if>${lineItem.product.thumbnail.imageUrl}" border="0" /><br />
    </c:if><div class="sku"><c:out value="${lineItem.product.sku}"/></div></td>
  <td class="invoice"><c:if test="${lineItem.customXml != null}">Custom Frame - </c:if><c:out value="${lineItem.product.name}"/>
    <table border="0" cellspacing="1" cellpadding="0">
	  <c:forEach items="${lineItem.productAttributes}" var="productAttribute" varStatus="productAttributeStatus">
		<tr class="invoice_lineitem_attributes${productAttributeStatus.count%2}">
		<td class="optionName" style="white-space: nowrap" align="right"><c:out value="${productAttribute.optionName}"/>: </td>
		<td class="optionValue" style="padding-left:5px;"><c:out value="${productAttribute.valueString}" escapeXml="false"/></td>
		<c:if test="${(! empty productAttribute.optionPriceOriginal) && productAttribute.optionPriceOriginal != 0}">
		<td class="optionPrice"><fmt:formatNumber value="${productAttribute.optionPriceOriginal}" pattern="#,##0.00" /></td>
		<td class="optionNote" style="padding-left:5px;"><fmt:message key="${productAttribute.optionPriceMessageF}"/></td>
		</c:if>
		</tr>
	  </c:forEach>
	  <c:forEach items="${lineItem.asiProductAttributes}" var="asiProductAttribute" varStatus="asiProductAttributeStatus">
		<tr class="invoice_lineitem_attributes${asiProductAttributeStatus.count%2}">
		<td style="white-space: nowrap" align="right"><c:out value="${asiProductAttribute.asiOptionName}"/>: </td>
		<td style="width:100%;padding-left:5px;"><c:out value="${asiProductAttribute.asiOptionValue}" escapeXml="false"/></td>
		</tr>
	  </c:forEach>
	  <%@ include file="/WEB-INF/jsp/frontend/common/customLinesInvoice.jsp"%>
    </table>
	<c:if test="${lineItem.subscriptionInterval != null}">
	  <c:set var="intervalType" value="${fn:substring(lineItem.subscriptionInterval, 0, 1)}"/>
	  <c:set var="intervalUnit" value="${fn:substring(lineItem.subscriptionInterval, 1, 2)}"/>
	  <div class="invoice_lineitem_subscription">
		<fmt:message key="subscription"/> <fmt:message key="details"/> : <fmt:message key="delivery"/>
		<c:choose>
		  <c:when test="${intervalUnit == '1'}"><fmt:message key="every_${intervalType}"><fmt:param value="${intervalUnit}"/></fmt:message></c:when>
		  <c:otherwise><fmt:message key="every_${intervalType}s"><fmt:param value="${intervalUnit}"/></fmt:message></c:otherwise>
		</c:choose>
	  </div>
	</c:if>
  <c:if test="${siteConfig['SHOW_IMAGE_ON_INVOICE'].value == 'true' and lineItem.hasProductAttributesImage}" >
  <c:forEach items="${lineItem.productAttributes}" var="productAttribute">
    <c:if test="${!empty productAttribute.imageUrl}" > 
      <c:if test="${!cartItem.product.thumbnail.absolute}"><img src="<c:url value="/assets/Image/Product/options/${productAttribute.imageUrl}"/>" border="0" class="invoiceOptionImage" /></c:if>
      <c:if test="${cartItem.product.thumbnail.absolute}"><img src="<c:url value="${productAttribute.imageUrl}"/>" border="0" class="invoiceOptionImage" /></c:if>
    </c:if>
  </c:forEach>
  </c:if> 
  </td>  
  <c:forEach items="${orderForm.productFieldsHeader}" var="pFHeader" varStatus="status"><c:set var="check" value="0"/>
  <c:if test="${pFHeader.showOnInvoice}">
  <c:forEach items="${lineItem.productFields}" var="productField"> 
   <c:if test="${pFHeader.id == productField.id}">
    <td class="invoice"><c:out value="${productField.value}" /></td><c:set var="check" value="1"/>
   </c:if>       
  </c:forEach>
  <c:if test="${check == 0}">
    <td class="invoice">&nbsp;</td>
   </c:if>
  </c:if>
  </c:forEach> 
  <c:if test="${siteConfig['ORDER_FULFILMENT'].value == 'true' and orderForm.customer.id == '20995'}" >
  	<td class="invoice" align="center">
  	<c:choose>
  	<c:when test="${lineItem.shippingDays == null or lineItem.shippingDays == ''}">
  		<fmt:message key="f_outOfStock" />
  	</c:when>
  	<c:when test="${lineItem.shippingDays == 0}">
  		<fmt:message key="f_immediately" />
  	</c:when>
  	<c:otherwise>
  		<c:out value="${lineItem.shippingDays}"/> Days
  	</c:otherwise>
  	</c:choose>	
  	</td>
  </c:if>
  <td class="invoice" align="center">
    <c:choose>
        <c:when test="${gSiteConfig['gVARIABLE_PRICE'] and lineItem.product.priceByCustomer}"></c:when>
        <c:otherwise>
          <c:out value="${lineItem.quantity}"/>
          <c:if test="${lineItem.priceCasePackQty != null}"><div class="casePackPriceQty">(<c:out value="${lineItem.priceCasePackQty}"/> per <c:out value="${siteConfig['PRICE_CASE_PACK_UNIT_TITLE'].value}" />)</div></c:if>
        </c:otherwise>
    </c:choose>       
  </td>
  <c:if test="${gSiteConfig['gINVENTORY'] && orderForm.order.hasLowInventoryMessage}">
	<td class="invoice" align="center"><c:out value="${lineItem.lowInventoryMessage}" /></td>
  </c:if>
  <c:choose>
	<c:when test="${siteConfig['CASE_CONTENT_LAYOUT'].value == '1'}">
		<c:choose>
	  		<c:when test="${lineItem.product.caseContent != null}">
	  	 		 <td class="invoice" align="right"><fmt:formatNumber value="${lineItem.unitPrice * lineItem.product.caseContent}" pattern="#,##0.00#" maxFractionDigits="3"/></td>
	 	 	</c:when>
	 	 	<c:otherwise>
	 	 		<td class="invoice" align="right"><fmt:formatNumber value="${lineItem.unitPrice}" pattern="#,##0.00#" maxFractionDigits="3"/></td>
	 	 	</c:otherwise>
	  	</c:choose>
	 </c:when>
	 <c:when test="${siteConfig['CASE_CONTENT_LAYOUT'].value == '2'}">
    	<td class="invoice" align="right"><fmt:formatNumber value="${lineItem.unitPrice}" pattern="#,##0.00#" maxFractionDigits="3"/></td>
     </c:when>
	 <c:otherwise>
	 	<c:if test="${orderForm.order.hasPacking}">
		  <td class="invoice" align="center"><c:out value="${lineItem.product.packing}" /></td>
		  </c:if>
		  <c:if test="${orderForm.order.hasContent}">
		    <td class="invoice" align="center"><c:out value="${lineItem.product.caseContent}" /></td>
		  </c:if>
		  <td class="invoice" align="right"><fmt:formatNumber value="${lineItem.unitPrice}" pattern="#,##0.00#" maxFractionDigits="3"/></td> 	
	 </c:otherwise>
  </c:choose>
  <c:if test="${orderForm.order.hasCustomShipping}">
	<td class="invoice" align="center">
		<c:if test="${!empty lineItem.customShippingCost}">
			<c:out value="${orderForm.order.customShippingTitle}" />
		</c:if>
	</td>
  </c:if>
  <c:if test="${orderForm.order.lineItemPromos != null and fn:length(orderForm.order.lineItemPromos) gt 0}">
     <c:choose>
       <c:when test="${lineItem.promo.percent}">
         <td class="discount" align="right"><c:if test="${lineItem.promo != null}"><c:out value="${lineItem.promo.title}" /> : <fmt:formatNumber value="${(lineItem.totalPrice * lineItem.promo.discount) / 100}" pattern="#,##0.00"/></c:if></td>
       </c:when>
       <c:otherwise>
         <td class="discount" align="right"><c:if test="${lineItem.promo != null}"><c:out value="${lineItem.promo.title}" /> : <fmt:formatNumber value="${lineItem.promo.discount}" pattern="#,##0.00"/></c:if></td>
       </c:otherwise>
     </c:choose>
  </c:if>
  <td class="invoice" align="right"><fmt:formatNumber value="${lineItem.totalPrice}" pattern="#,##0.00"/></td>
</tr>
</c:forEach>
  <tr bgcolor="#BBBBBB">
	<td colspan="${6+cols}">&nbsp;</td>
  </tr>
  <tr>
    <td class="invoice" colspan="${5+cols}" align="right"><fmt:message key="subTotal" />:</td>
    <td class="invoice" align="right"><fmt:formatNumber value="${orderForm.order.subTotal}" pattern="#,##0.00"/></td>
  </tr>
  <c:if test="${orderForm.order.promo != null and orderForm.order.promo.discountType eq 'order'}">
    <tr>
      <td class="discount" colspan="${5+cols}" align="right">
      <c:choose>
        <c:when test="${orderForm.order.promoAmount == 0}"><fmt:message key="promoCode" /></c:when>
        <c:otherwise><fmt:message key="discountForPromoCode" /></c:otherwise>
      </c:choose>
      <c:out value="${orderForm.order.promo.title}" />
      <c:choose>
          <c:when test="${orderForm.order.promoAmount == 0}"></c:when>
          <c:when test="${orderForm.order.promo.percent}">
            (<fmt:formatNumber value="${orderForm.order.promo.discount}" pattern="#,##0.00"/>%)
          </c:when>
          <c:otherwise>
            (<fmt:message key="${siteConfig['CURRENCY'].value}" /><fmt:formatNumber value="${orderForm.order.promo.discount}" pattern="#,##0.00"/>)
          </c:otherwise>
      	</c:choose>
      </td>
      <td class="discount" align="right">
      <c:choose>
        <c:when test="${orderForm.order.promoAmount == 0}">&nbsp;</c:when>
        <c:otherwise>(<fmt:message key="${siteConfig['CURRENCY'].value}" /><fmt:formatNumber value="${orderForm.order.promoAmount}" pattern="#,##0.00"/>)</c:otherwise>
      </c:choose>
      </td>
    </tr>  
  </c:if>
  <c:if test="${orderForm.order.lineItemPromos != null and fn:length(orderForm.order.lineItemPromos) gt 0}">
    <c:forEach items="${orderForm.order.lineItemPromos}" var="itemPromo" varStatus="status">
    <tr>
      <td class="discount" colspan="${5+cols}" align="right">
      <c:choose>
        <c:when test="${itemPromo.value == 0}"><fmt:message key="promoCode" /></c:when>
        <c:otherwise><fmt:message key="discountForPromoCode" /></c:otherwise>
      </c:choose>
      <c:out value="${itemPromo.key}" />
      </td>
      <td class="discount" align="right">
      <c:choose>
        <c:when test="${itemPromo.value == 0}">&nbsp;</c:when>
        <c:otherwise>(<fmt:message key="${siteConfig['CURRENCY'].value}" /><fmt:formatNumber value="${itemPromo.value}" pattern="#,##0.00"/>)</c:otherwise>
      </c:choose>
      </td>
    </tr>
    </c:forEach>  
  </c:if>
  <c:choose>
    <c:when test="${order.taxOnShipping}">
  	  <tr>
    	<td class="invoice" colspan="${5+cols}" align="right"><fmt:message key="shippingHandling" /> (<c:out value="${order.shippingMethod}" escapeXml="false"/>):</td>
    	<td class="invoice" align="right"><fmt:formatNumber value="${order.shippingCost}" pattern="#,##0.00"/></td>
  	  </tr>
  	  <tr>
    	<td class="invoice" colspan="${5+cols}" align="right"><fmt:message key="tax" />:</td>
    	<td class="invoice" align="right"><fmt:formatNumber value="${order.tax}" pattern="#,##0.00"/></td>
  	  </tr>
    </c:when>
    <c:otherwise>
  	  <tr>
    	<td class="invoice" colspan="${5+cols}" align="right"><fmt:message key="tax" />:</td>
    	<td class="invoice" align="right"><fmt:formatNumber value="${order.tax}" pattern="#,##0.00"/></td>
  	  </tr>
  	  <tr>
    	<td class="invoice" colspan="${5+cols}" align="right"><fmt:message key="shippingHandling" /> (<c:out value="${order.shippingMethod}" escapeXml="false"/>):</td>
    	<td class="invoice" align="right"><fmt:formatNumber value="${order.shippingCost}" pattern="#,##0.00"/></td>
  	  </tr>
    </c:otherwise>
  </c:choose>
  <c:if test="${orderForm.order.hasCustomShipping}">
    <td class="invoice" colspan="${5+cols}" align="right"><fmt:message key="customShipping" /> (<c:out value="${orderForm.order.customShippingTitle}" escapeXml="false"/>):</td>
    <td class="invoice" align="right"><fmt:formatNumber value="${orderForm.order.customShippingCost}" pattern="#,##0.00"/></td>
  </c:if>
  <c:if test="${orderForm.order.promo != null and orderForm.order.promo.discountType eq 'shipping'}">
    <tr>
      <td class="discount" colspan="${5+cols}" align="right">
      <c:choose>
        <c:when test="${orderForm.order.promoAmount == 0}"><fmt:message key="promoCode" /></c:when>
        <c:otherwise><fmt:message key="discountForPromoCode" /></c:otherwise>
      </c:choose>
      <c:out value="${orderForm.order.promo.title}" />
      <c:choose>
          <c:when test="${orderForm.order.promoAmount == 0}"></c:when>
          <c:when test="${orderForm.order.promo.percent}">
            (<fmt:formatNumber value="${orderForm.order.promo.discount}" pattern="#,##0.00"/>%)
          </c:when>
          <c:otherwise>
            (<fmt:message key="${siteConfig['CURRENCY'].value}" /><fmt:formatNumber value="${orderForm.order.promo.discount}" pattern="#,##0.00"/>)
          </c:otherwise>
      	</c:choose>
      </td>
      <td class="discount" align="right">
      <c:choose>
        <c:when test="${orderForm.order.promoAmount == 0}">&nbsp;</c:when>
        <c:otherwise>(<fmt:message key="${siteConfig['CURRENCY'].value}" /><fmt:formatNumber value="${orderForm.order.promoAmount}" pattern="#,##0.00"/>)</c:otherwise>
      </c:choose>
      </td>
    </tr>  
  </c:if>
  <c:if test="${gSiteConfig['gGIFTCARD'] and orderForm.order.creditUsed != null}">
  <tr>
    <td class="discount" colspan="${5+cols}" align="right"><fmt:message key="credit" />:</td>
    <td class="discount" align="right"><fmt:formatNumber value="${orderForm.order.creditUsed}" pattern="#,##0.00"/></td>
  </tr>
  </c:if>
  <c:if test="${siteConfig['BUY_SAFE_URL'].value != ''}">
  <tr>
    <td class="invoice" colspan="${5+cols}" align="right"><fmt:message key="optionalBuySafe3In1Guarntee" /> <a href="viewCart.jhtm">( change )</a>:</td>
    <td class="invoice" align="right">
      <c:choose>
        <c:when test="${orderForm.order.wantsBond}"><fmt:formatNumber value="${orderForm.order.bondCost}" pattern="#,##0.00"/></c:when>
        <c:otherwise><fmt:formatNumber value="0.00"/></c:otherwise>
      </c:choose>
    </td>
  </tr>
  </c:if>
  <tr>
    <td class="invoice" colspan="${5+cols}" align="right"><fmt:message key="grandTotal" />:</td>
    <td align="right" bgcolor="#F8FF27"><fmt:message key="${siteConfig['CURRENCY'].value}" /><fmt:formatNumber value="${orderForm.order.grandTotal}" pattern="#,##0.00"/></td>
  </tr>
</table>
<c:out value="${siteConfig['INVOICE_MESSAGE'].value}" escapeXml="false"/>
<br/><br/> 
<c:if test="${message != null }">
	<c:out value="${message}"/>
</c:if>

<div id="purchaseButton" align="center" class="nbButton" style="margin:5px;">
<input type="hidden" value="${payPalToken}" name="token"/>
<input type="hidden" value="${payPalPayerId}" name="PayerID"/>
<input type="hidden" value="${payPalOrderId}" name="order_id"/>
<div align="right" class="next"><input type="image" src="assets/Image/Layout/button_purchase${_lang}.gif" id="button_purchase" name="_finish" ></div>
</div>
</form:form>
<!-- Don't enable the footer for PayPalPro as we are not saving the orderid from PayPalUrl, it might result in exceptions. -->
<!--<c:out value="${invoiceLayout.footerHtml}" escapeXml="false"/> -->
</div>
</body>
</html>