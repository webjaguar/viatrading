<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<c:set value="${invoiceLayout.headTag}" var="headTag"/>
<c:set value="${fn:replace(headTag, '#order#', order.orderId)}" var="headTag"/>
<c:set value="${fn:replace(headTag, '#ordergrandtotal#', order.grandTotal)}" var="headTag"/>
<c:set value="${fn:replace(headTag, '#ordersubtotal#', order.subTotal)}" var="headTag"/>
<c:set value="${fn:replace(headTag, '#ordertotalquantity#', order.totalQuantity)}" var="headTag"/>
<c:set value="${fn:replace(headTag, '#orderdiscount#', order.promoAmount)}" var="headTag"/>
<c:out value="${headTag}" escapeXml="false"/>
</head>  
<body class="invoice">
<c:if test="${gSiteConfig['gACCOUNTING'] == 'EVERGREEN' and (orderForm.customer.accountNumber == null or empty orderForm.customer.accountNumber)}">
 <link rel="stylesheet" href="assets/SmoothGallery/multibox.css" type="text/css" media="screen" />
 <script src="javascript/mootools-1.2.5-core.js" type="text/javascript"></script>
 <script src="javascript/mootools-1.2.5.1-more.js" type="text/javascript"></script>
 <script src="javascript/overlay.js" type="text/javascript" ></script>
 <script src="javascript/multibox.js" type="text/javascript" ></script>
<script type="text/javascript">
window.addEvent('domready', function(){			
	var box = new multiBox('mbxwz', {overlay: new overlay()});
	box.open($('mb1'));
});
</script>
<a href="#htmlElement" rel="type:element" id="mb1" class="mbxwz" title="Note">Note</a>
    <div class="multiBoxDesc mb1"></div><br />
    <div id="htmlElement" style="width:350px;">
    <div style="width:95%;padding:10px;height:270px;">
        Thank you for your order and welcome to Evergreen! If you are a first time customer, here's how you can speed up the time to receive your shipment.
        <ul>
          <li>
          Click <a href="assets/Evergreen_Credit_App.pdf" target="_blank">here</a> to fill out the credit application form and either email to customer service at <a href="mailto:customersupport@myevergreen.com">customersupport@myevergreen.com</a> or FAX the form to  <div>(804) 545 8035.</div><br /><br />
          </li>
          <li>
          Also, if you selected to pay with a Purchase Order, we will work with you to set up your payment terms. First time orders that use a credit card payment method can be processed faster. Click <a href="assets/Evergreen_Credit_App.pdf" target="_blank">here</a> to submit your credit card information if you would like to update your payment method.
          </li>
        </ul>
        </div>
        </div>
</c:if>
<div style="width:700px;margin:auto;">

<c:set value="${invoiceLayout.headerHtml}" var="headerHtml"/>
<c:set value="${fn:replace(headerHtml, '#order#', order.orderId)}" var="headerHtml"/>
<c:set value="${fn:replace(headerHtml, '#ordergrandtotal#', order.grandTotal)}" var="headerHtml"/>
<c:set value="${fn:replace(headerHtml, '#ordersubtotal#', order.subTotal)}" var="headerHtml"/>
<c:set value="${fn:replace(headerHtml, '#ordertotalquantity#', order.totalQuantity)}" var="headerHtml"/>
<c:set value="${fn:replace(headerHtml, '#orderdiscount#', order.promoAmount)}" var="headerHtml"/>
<c:out value="${headerHtml}" escapeXml="false"/>

<table border="0" width="100%" cellspacing="3" cellpadding="1">
<tr valign="top">
<td>
<b><fmt:message key="billingInformation" />:</b>
<c:set value="${order.billing}" var="address"/>
<c:set value="true" var="billing" />
<%@ include file="/WEB-INF/jsp/frontend/common/address.jsp" %>
<c:set value="false" var="billing" />
</td>
<td>&nbsp;</td>
<td>
<b><fmt:message key="shippingInformation" />:</b>
<c:set value="${order.shipping}" var="address"/>
<%@ include file="/WEB-INF/jsp/frontend/common/address.jsp" %>
</td>
<td>&nbsp;</td>
<td align="right">
<table class="invoiceHeader">
  <tr>
    <td><fmt:message key="invoice"/> #</td>
    <td>:&nbsp;&nbsp;</td>
    <td><b><c:out value="${order.orderId}"/></b></td>
  </tr>
  <tr>
    <td><fmt:message key="date" /></td>
    <td>:&nbsp;&nbsp;</td>
    <td><fmt:formatDate type="date" timeStyle="default" value="${order.dateOrdered}"/></td>
  </tr>
  <c:if test="${siteConfig['DELIVERY_TIME'].value == 'true' and !empty order.dueDate}">
  <tr>
    <td><fmt:message key="f_deliveryDate" /></td>
    <td>:&nbsp;&nbsp;</td>
    <td class="deliveryDate"><fmt:formatDate type="date" timeStyle="default" value="${order.dueDate}"/></td>
  </tr>
  </c:if>
  <c:if test="${order.userDueDate != null}" >
  <tr>
    <td><fmt:message key="userExpectedDueDate" /></td>
    <td>:&nbsp;&nbsp;</td>
    <td class="userExpectedDueDate"><fmt:formatDate type="date" timeStyle="default" value="${order.userDueDate}"/></td>
  </tr>  
  </c:if>
  <c:if test="${order.requestedCancelDate != null}" >
  <tr>
    <td><fmt:message key="requestedCancelDate" /></td>
    <td>:&nbsp;&nbsp;</td>
    <td class="requestedCancelDate"><fmt:formatDate type="date" timeStyle="default" value="${order.requestedCancelDate}"/></td>
  </tr>  
  </c:if>

</table>
</tr>
</table>
<p>
<c:if test="${order.purchaseOrder != '' and order.purchaseOrder != null}" >
<b><fmt:message key="purchaseOrder" /></b>: <c:out value="${order.purchaseOrder}" /><br />
</c:if>
<div class="emailAddress"><b><fmt:message key="emailAddress" />:</b>&nbsp;<c:out value="${username}"/></div>
<table border="0" cellpadding="2" cellspacing="1" width="100%" class="invoice">
  <tr>
    <c:set var="cols" value="0"/>  
    <th width="5%" class="invoice"><fmt:message key="line" />#</th>
    <th class="invoice"><fmt:message key="productSku" /></th>
    <th class="invoice"><fmt:message key="productName" /></th>
    <c:forEach items="${orderForm.productFieldsHeader}" var="productField">
      <c:if test="${productField.showOnInvoice}">
      <th class="invoice"><c:out value="${productField.name}" /></th><c:set var="cols" value="${cols+1}"/>
      </c:if>
    </c:forEach>
    <th width="10%" class="invoice"><fmt:message key="quantity" /></th>
    <c:if test="${gSiteConfig['gINVENTORY'] && order.hasLowInventoryMessage}">
      <c:set var="cols" value="${cols+1}"/>
	  <th width="10%" class="invoice"><fmt:message key="lowInventory" /></th>
	</c:if>
	<c:if test="${siteConfig['CASE_CONTENT_LAYOUT'].value == ''}">
	    <c:if test="${order.hasPacking}">
	      <c:set var="cols" value="${cols+1}"/>
	      <th width="10%" class="invoice"><fmt:message key="packing" /></th>
	    </c:if>
	    <c:if test="${order.hasContent}">
	      <c:set var="cols" value="${cols+1}"/>
	      <th class="invoice"><fmt:message key="content" /></th>
	    </c:if>
	</c:if>
	<c:if test="${order.hasCustomShipping}">
      <c:set var="cols" value="${cols+1}"/>
	  <th class="invoice"><fmt:message key="customShipping" /></th>
	</c:if>
    <%-- <th class="invoice"><fmt:message key="productPrice" /><c:if test="${order.hasContent}">/<c:out value="${siteConfig['CASE_UNIT_TITLE'].value}" /></c:if></th> --%>
    <c:choose>
      <c:when test="${siteConfig['CASE_CONTENT_LAYOUT'].value == ''}">
          <th class="invoice"><div><fmt:message key="productPrice" /><c:if test="${order.hasContent}">/<c:out value="${siteConfig['CASE_UNIT_TITLE'].value}" /></c:if></div></th>
      </c:when>
      <c:otherwise>
          <th class="invoice"><div><fmt:message key="productPrice" /></div></th>
      </c:otherwise>
    </c:choose>
    <c:if test="${order.lineItemPromos != null and fn:length(order.lineItemPromos) gt 0}">
      <c:set var="cols" value="${cols+1}"/>
	  <th class="invoice"><fmt:message key="discount" /></th>
	</c:if>
	<th class="invoice"><fmt:message key="total" /></th>
  </tr>
<c:forEach var="lineItem" items="${order.lineItems}">
<tr valign="top">
  <td class="invoice" align="center"><c:out value="${lineItem.lineNumber}"/></td>
  <td class="invoice">
	<c:if test="${lineItem.customImageUrl != null}">
      <a href="assets/customImages/${order.orderId}/${lineItem.customImageUrl}" onclick="window.open(this.href,'','width=640,height=480,resizable=yes'); return false;"><img class="invoiceImage" src="assets/customImages/${order.orderId}/${lineItem.customImageUrl}" border="0" /></a>
    </c:if>    
    <c:if test="${siteConfig['SHOW_IMAGE_ON_INVOICE'].value == 'true' and lineItem.product.thumbnail != null}">
      <img class="invoiceImage" src="<c:if test="${!lineItem.product.thumbnail.absolute}">assets/Image/Product/thumb/</c:if>${lineItem.product.thumbnail.imageUrl}" border="0" />
    </c:if><div class="sku"><c:out value="${lineItem.product.sku}"/></div>
  <c:if test="${fn:contains(gSiteConfig['gDATA_FEED'],'blueCherry')}">
  <img src="zmam=31251023&zmas=8&zmaq=N&quantity=<c:out value="${lineItem.quantity}"/>&pcode=<c:out value="${lineItem.product.sku}"/>&zman=<c:out value="${order.orderId}"/>&zmat=<c:out value="${order.grandTotal}"/>" width="0" height="0" border="0">
  </c:if>
  </td>
  <td class="invoice"><c:if test="${lineItem.customXml != null}">Custom Frame - </c:if><c:out value="${lineItem.product.name}"/>
    <table border="0" cellspacing="1" cellpadding="0">
	  <c:forEach items="${lineItem.productAttributes}" var="productAttribute" varStatus="productAttributeStatus">
		<tr class="invoice_lineitem_attributes${productAttributeStatus.count%2}">
		<td class="optionName" style="white-space: nowrap" align="right"><c:out value="${productAttribute.optionName}"/>: </td>
		<td class="optionValue" style="padding-left:5px;"><c:out value="${productAttribute.valueString}" escapeXml="false"/></td>
		<c:if test="${(! empty productAttribute.optionPriceOriginal) && productAttribute.optionPriceOriginal != 0}">
		<td class="optionPrice"><fmt:formatNumber value="${productAttribute.optionPriceOriginal}" pattern="#,##0.00" /></td>
		<td class="optionNote" style="padding-left:5px;"><fmt:message key="${productAttribute.optionPriceMessageF}"/></td>
		</c:if>
		</tr>
	  </c:forEach>
	  <c:forEach items="${lineItem.asiProductAttributes}" var="asiProductAttribute" varStatus="asiProductAttributeStatus">
		<tr class="invoice_lineitem_attributes${asiProductAttributeStatus.count%2}">
		<td style="white-space: nowrap" align="right"><c:out value="${asiProductAttribute.asiOptionName}"/>: </td>
		<td style="width:100%;padding-left:5px;"><c:out value="${asiProductAttribute.asiOptionValue}" escapeXml="false"/></td>
		</tr>
	  </c:forEach>
	  <%@ include file="/WEB-INF/jsp/frontend/common/customLinesInvoice.jsp"%>
    </table>
	<c:if test="${lineItem.subscriptionInterval != null}">
	  <c:set var="intervalType" value="${fn:substring(lineItem.subscriptionInterval, 0, 1)}"/>
	  <c:set var="intervalUnit" value="${fn:substring(lineItem.subscriptionInterval, 1, 2)}"/>
	  <div class="invoice_lineitem_subscription">
		<fmt:message key="subscription"/> <fmt:message key="details"/> : <fmt:message key="delivery"/>
		<c:choose>
		  <c:when test="${intervalUnit == '1'}"><fmt:message key="every_${intervalType}"><fmt:param value="${intervalUnit}"/></fmt:message></c:when>
		  <c:otherwise><fmt:message key="every_${intervalType}s"><fmt:param value="${intervalUnit}"/></fmt:message></c:otherwise>
		</c:choose>
	  </div>
	  <div class="invoice_lineitem_subscription"><c:out value="${lineItem.subscriptionCode}"/></div>
	</c:if>
  <c:if test="${siteConfig['SHOW_IMAGE_ON_INVOICE'].value == 'true' and lineItem.hasProductAttributesImage}" >
  <c:forEach items="${lineItem.productAttributes}" var="productAttribute">
    <c:if test="${!empty productAttribute.imageUrl}" > 
      <c:if test="${!cartItem.product.thumbnail.absolute}"><img src="<c:url value="/assets/Image/Product/options/${productAttribute.imageUrl}"/>" border="0" class="invoiceOptionImage" /></c:if>
      <c:if test="${cartItem.product.thumbnail.absolute}"><img src="<c:url value="${productAttribute.imageUrl}"/>" border="0" class="invoiceOptionImage" /></c:if>
    </c:if>
  </c:forEach>
  </c:if> 
  </td>    
  <c:forEach items="${orderForm.productFieldsHeader}" var="pFHeader" varStatus="status"><c:set var="check" value="0"/>
  <c:if test="${pFHeader.showOnInvoice}">
  <c:forEach items="${lineItem.productFields}" var="productField"> 
   <c:if test="${pFHeader.id == productField.id}">
    <td class="invoice"><c:out value="${productField.value}" /></td><c:set var="check" value="1"/>
   </c:if>       
  </c:forEach>
  <c:if test="${check == 0}">
    <td class="invoice">&nbsp;</td>
   </c:if>
  </c:if>
  </c:forEach> 
  <td class="invoice" align="center">
    <c:choose>
        <c:when test="${gSiteConfig['gVARIABLE_PRICE'] and lineItem.product.priceByCustomer}"></c:when>
        <c:otherwise>
          <c:out value="${lineItem.quantity}"/>
          <c:if test="${lineItem.priceCasePackQty != null}"><div class="casePackPriceQty">(<c:out value="${lineItem.priceCasePackQty}"/> per <c:out value="${siteConfig['PRICE_CASE_PACK_UNIT_TITLE'].value}" />)</div></c:if>
        </c:otherwise>
    </c:choose>       
  </td>
  <c:if test="${gSiteConfig['gINVENTORY'] && order.hasLowInventoryMessage}">
	<td class="invoice" align="center"><c:out value="${lineItem.lowInventoryMessage}" /></td>
  </c:if>
  <c:choose>
	<c:when test="${siteConfig['CASE_CONTENT_LAYOUT'].value == '1'}">
   		<c:choose>
	  		<c:when test="${lineItem.product.caseContent != null}">
	  	 		 <td class="invoice" align="right"><fmt:formatNumber value="${lineItem.unitPrice * lineItem.product.caseContent}" pattern="#,##0.00#" maxFractionDigits="3"/></td>
	 	 	</c:when>
	 	 	<c:otherwise>
	 	 		<td class="invoice" align="right"><fmt:formatNumber value="${lineItem.unitPrice}" pattern="#,##0.00#" maxFractionDigits="3"/></td>
	 	 	</c:otherwise>
	  	</c:choose>
    </c:when>
    <c:when test="${siteConfig['CASE_CONTENT_LAYOUT'].value == '2'}">
    	<td class="invoice" align="right"><fmt:formatNumber value="${lineItem.unitPrice}" pattern="#,##0.00#" maxFractionDigits="3"/></td>
    </c:when>
	<c:otherwise>
	  	<c:if test="${order.hasPacking }">
		  <td class="invoice" align="center"><c:out value="${lineItem.product.packing}" /></td>
		</c:if>
		<c:if test="${order.hasContent}">
		    <td class="invoice" align="center"><c:out value="${lineItem.product.caseContent}" /></td>
		</c:if>
		<td class="invoice" align="right"><fmt:formatNumber value="${lineItem.unitPrice}" pattern="#,##0.00#" maxFractionDigits="3"/></td> 	
	</c:otherwise>
  </c:choose>  
  <c:if test="${order.hasCustomShipping}">
	<td class="invoice" align="center">
		<c:if test="${!empty lineItem.customShippingCost}">
		  <c:out value="${order.customShippingTitle}" />
		</c:if>
	</td>
  </c:if>
  <c:if test="${order.lineItemPromos != null and fn:length(order.lineItemPromos) gt 0}">
     <c:choose>
       <c:when test="${lineItem.promo.percent}">
         <td class="discount" align="right"><c:if test="${lineItem.promo != null}"><c:out value="${lineItem.promo.title}" /> : <fmt:formatNumber value="${(lineItem.totalPrice * lineItem.promo.discount) / 100}" pattern="#,##0.00"/></c:if></td>
       </c:when>
       <c:otherwise>
         <td class="discount" align="right"><c:if test="${lineItem.promo != null}"><c:out value="${lineItem.promo.title}" /> : <fmt:formatNumber value="${lineItem.promo.discount}" pattern="#,##0.00"/></c:if></td>
       </c:otherwise>
     </c:choose>
  </c:if>
  <td class="invoice" align="right"><fmt:formatNumber value="${lineItem.totalPrice}" pattern="#,##0.00"/></td>
</tr>
</c:forEach>
  <tr bgcolor="#BBBBBB">
	<td colspan="${6+cols}">&nbsp;</td>
  </tr>
  <tr>
    <td class="invoice" colspan="${5+cols}" align="right"><fmt:message key="subTotal" />:</td>
    <td class="invoice" align="right"><fmt:formatNumber value="${order.subTotal}" pattern="#,##0.00"/></td>
  </tr>
  <c:if test="${order.promo.title != null and order.promo.discountType eq 'order' }">
    <tr>
      <td class="discount" colspan="${5+cols}" align="right">
      <c:choose>
        <c:when test="${order.promoAmount == 0}"><fmt:message key="promoCode" /></c:when>
        <c:otherwise><fmt:message key="discountForPromoCode" /> </c:otherwise>
      </c:choose>
      <c:out value="${order.promo.title}" />
      <c:choose>
		  <c:when test="${order.promoAmount == 0}"></c:when>
          <c:when test="${order.promo.percent}">
            (<fmt:formatNumber value="${order.promo.discount}" pattern="#,##0.00"/>%)
          </c:when>
          <c:otherwise>
            (<fmt:message key="${siteConfig['CURRENCY'].value}" /><fmt:formatNumber value="${order.promo.discount}" pattern="#,##0.00"/>)
          </c:otherwise>
      	</c:choose>
      </td>
      <td class="discount" align="right">
      <c:choose>
        <c:when test="${order.promoAmount == 0}">&nbsp;</c:when>
        <c:otherwise>(<fmt:message key="${siteConfig['CURRENCY'].value}" /><fmt:formatNumber value="${order.promoAmount}" pattern="#,##0.00"/>)</c:otherwise>
      </c:choose>
      </td>
    </tr>  
  </c:if>  
  <c:if test="${order.lineItemPromos != null and fn:length(order.lineItemPromos) gt 0}">
    <c:forEach items="${order.lineItemPromos}" var="itemPromo" varStatus="status">
    <tr>
      <td class="discount" colspan="${5+cols}" align="right">
      <c:choose>
        <c:when test="${itemPromo.value == 0}"><fmt:message key="promoCode" /></c:when>
        <c:otherwise><fmt:message key="discountForPromoCode" /></c:otherwise>
      </c:choose>
      <c:out value="${itemPromo.key}" />
      </td>
      <td class="discount" align="right">
      <c:choose>
        <c:when test="${itemPromo.value == 0}">&nbsp;</c:when>
        <c:otherwise>(<fmt:message key="${siteConfig['CURRENCY'].value}" /><fmt:formatNumber value="${itemPromo.value}" pattern="#,##0.00"/>)</c:otherwise>
      </c:choose>
      </td>
    </tr>
    </c:forEach>  
  </c:if>
  <c:choose>
    <c:when test="${order.taxOnShipping}">
  	  <tr>
    	<td class="invoice" colspan="${5+cols}" align="right"><fmt:message key="shippingHandling" /> (<c:out value="${order.shippingMethod}" escapeXml="false"/>):</td>
    	<td class="invoice" align="right"><fmt:formatNumber value="${order.shippingCost}" pattern="#,##0.00"/></td>
  	  </tr>
  	  <tr>
    	<td class="invoice" colspan="${5+cols}" align="right"><fmt:message key="tax" />:</td>
    	<td class="invoice" align="right"><fmt:formatNumber value="${order.tax}" pattern="#,##0.00"/></td>
  	  </tr>
    </c:when>
    <c:otherwise>
  	  <tr>
    	<td class="invoice" colspan="${5+cols}" align="right"><fmt:message key="tax" />:</td>
    	<td class="invoice" align="right"><fmt:formatNumber value="${order.tax}" pattern="#,##0.00"/></td>
  	  </tr>
  	  <tr>
    	<td class="invoice" colspan="${5+cols}" align="right"><fmt:message key="shippingHandling" /> (<c:out value="${order.shippingMethod}" escapeXml="false"/>):</td>
    	<td class="invoice" align="right"><fmt:formatNumber value="${order.shippingCost}" pattern="#,##0.00"/></td>
  	  </tr>
    </c:otherwise>
  </c:choose>
  <c:if test="${order.promo != null and order.promo.discountType eq 'shipping' and order.shippingCost != null}">
    <tr>
      <td class="discount" colspan="${5+cols}" align="right">
      <c:choose>
        <c:when test="${order.promoAmount == 0}"><fmt:message key="promoCode" /></c:when>
        <c:otherwise><fmt:message key="discountForPromoCode" /> </c:otherwise>
      </c:choose>
      <c:out value="${order.promo.title}" />
      <c:choose>
		  <c:when test="${order.promoAmount == 0}"></c:when>
          <c:when test="${order.promo.percent}">
            (<fmt:formatNumber value="${order.promo.discount}" pattern="#,##0.00"/>%)
          </c:when>
          <c:otherwise>
            (<fmt:message key="${siteConfig['CURRENCY'].value}" /><fmt:formatNumber value="${order.promo.discount}" pattern="#,##0.00"/>)
          </c:otherwise>
      	</c:choose>
      </td>
      <td class="discount" align="right">
      <c:choose>
        <c:when test="${order.promoAmount == 0}">&nbsp;</c:when>
        <c:otherwise>(<fmt:message key="${siteConfig['CURRENCY'].value}" /><fmt:formatNumber value="${order.promoAmount}" pattern="#,##0.00"/>)</c:otherwise>
      </c:choose>
      </td>
    </tr>  
  </c:if>  
  <c:if test="${order.hasCustomShipping}">
    <td class="invoice" colspan="${5+cols}" align="right"><fmt:message key="customShipping" /> (<c:out value="${order.customShippingTitle}" escapeXml="false"/>):</td>
    <td class="invoice" align="right"><fmt:formatNumber value="${order.customShippingCost}" pattern="#,##0.00"/></td>
  </c:if>
  <c:if test="${order.ccFee != null and order.ccFee > 0}">
    <td class="invoice" colspan="${5+cols}" align="right"><fmt:message key="creditCardFee" />:</td>
    <td class="invoice" align="right"><fmt:formatNumber value="${order.ccFee}" pattern="#,##0.00"/></td>
  </c:if>
  <c:if test="${gSiteConfig['gGIFTCARD'] && order.creditUsed != null}">
  <tr>
    <td class="discount" colspan="${5+cols}" align="right"><fmt:message key="credit" />:</td>
    <td class="discount" align="right"><fmt:formatNumber value="${order.creditUsed}" pattern="-#,##0.00"/></td>
  </tr>
  </c:if>
  <c:if test="${buySafeResponse != null && buySafeResponse.isBuySafeEnabled}">
  <tr>
    <td class="invoice" colspan="${5+cols}" align="right">
      ${buySafeResponse.cartLineDisplayText} &nbsp;${buySafeResponse.bondingSignal}&nbsp; <a href="http://www.buysafe.com/questions" target="_blank">Questions?</a>
      
    </td>
    <td class="invoice" align="right">
      <fmt:formatNumber value="${buySafeResponse.totalBondCost}" pattern="#,##0.00"/>
    </td>
  </tr>
  </c:if>
  <tr>
    <td class="invoice" colspan="${5+cols}" align="right"><fmt:message key="grandTotal" />:</td>
    <td align="right" bgcolor="#F8FF27"><fmt:message key="${siteConfig['CURRENCY'].value}" /><fmt:formatNumber value="${order.grandTotal}" pattern="#,##0.00"/></td>
  </tr>
</table>
<c:out value="${siteConfig['INVOICE_MESSAGE'].value}" escapeXml="false"/>
<p>
<table>
<tr>
<td>
    <b><fmt:message key="paymentMethod" />:</b> <c:out value="${order.paymentMethod}" />
    <p>
<c:if test="${order.creditCard.number != null and order.creditCard.number != ''}">
 <c:set value="${order.creditCard}" var="creditCard"/>
 <%@ include file="/WEB-INF/jsp/frontend/common/creditcard.jsp" %>
</c:if>
</td>
</tr>
</table>
<c:if test="${order.eBillme.orderrefid != null}">
<c:set var="frontend" value="true"/>
<%@ include file="/WEB-INF/jsp/frontend/common/eBillme.jsp" %>
</c:if>
<c:if test="${order.invoiceNote != null and order.invoiceNote != ''}">
	<table class="specialInstruction" width="100%">
		<tr >
		  <td valign="top"><br /> 
		    <b><c:out value="${siteConfig['SPECIAL_INSTRUCTIONS'].value}"/>:</b>  	
		  </td>
		</tr>
		<tr>
		  <td>
		  <c:out value="${order.invoiceNote}" escapeXml="false"/>
		  </td>
		</tr>
	</table>
</c:if>
<p>

<c:set value="${invoiceLayout.footerHtml}" var="footerHtml"/>
<c:set value="${fn:replace(footerHtml, '#order#', order.orderId)}" var="footerHtml"/>
<c:set value="${fn:replace(footerHtml, '#ordergrandtotal#', order.grandTotal)}" var="footerHtml"/>
<c:set value="${fn:replace(footerHtml, '#ordersubtotal#', order.subTotal)}" var="footerHtml"/>
<c:set value="${fn:replace(footerHtml, '#ordertotalquantity#', order.totalQuantity)}" var="footerHtml"/>
<c:set value="${fn:replace(footerHtml, '#orderdiscount#', order.promoAmount)}" var="footerHtml"/>
<c:out value="${footerHtml}" escapeXml="false"/>

</div>
<c:if test="${fn:contains(gSiteConfig['gDATA_FEED'],'blueCherry')}">
<script language="JavaScript" src="https://secure1.offerpoint.net/inChannel/ma2q.js"></script>
</c:if>
<%@ include file="/WEB-INF/jsp/frontend/checkout/freeCode.jsp" %>
</body>
</html>
