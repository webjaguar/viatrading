<%@ page import="com.webjaguar.model.*, java.text.*" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="/WEB-INF/tlds/custom.tld" prefix="wj" %>
<html>
<body>
<form name="netCommerceForm" method="POST" action="https://www.netcommercepay.com/iPAY/Default.asp">

<%-- FOR TESTING UNCOMMENT THIS
<input type="hidden" name="payment_mode" value="test">
--%>

<%
 String secureUrl = request.getScheme() + "://" + request.getHeader("host") + request.getContextPath() + "/"; 
%>

<%-- signature=sha256(txtAmount + txtCurrency + txtIndex + txtMerchNum + txthttp + md5_key) --%>
<input type="hidden" name="txtAmount" value="<c:out value="${wj:decimalFormatString(order.balance,null)}" escapeXml="false"/>">
<input type="hidden" name="txtCurrency" value="840"> <%-- 840 for USD 422 for LBP --%>
<input type="hidden" name="txtIndex" value="<c:out value="${order.orderId}"/>">
<input type="hidden" name="txtMerchNum" value="<c:out value="${siteConfig['CREDIT_ACCOUNT'].value}"/>">
<input type="hidden" name="txthttp" value="<c:out value="${siteConfig['SECURE_URL'].value}netcommerce.jhtm"/>">
<c:set var="signatureTxt" value="${wj:decimalFormatString(order.balance,null)}840${order.orderId}${siteConfig['CREDIT_ACCOUNT'].value}${siteConfig['SECURE_URL'].value}netcommerce.jhtm${siteConfig['NET_COMMERCE_MD5_KEY'].value}"/>
<input type="hidden" name="signature" value="<c:out value="${wj:sha256(signatureTxt)}"/>">

<noscript>
<input type="submit" value="Please click to continue paying by NetCommerce">
</noscript>

</form>
</body>
<script type="text/javascript" language="JavaScript">
<!--
document.netCommerceForm.submit();
//-->
</script>
</html>