<%@ page import="com.webjaguar.model.*, java.text.*" %>
<%@ page import="java.net.*,java.util.*,org.apache.commons.codec.binary.Base64,com.webjaguar.thirdparty.payment.amazon.*" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<html>
<head>
<c:set value="${invoiceLayout.headTag}" var="headTag"/>
<c:set value="${fn:replace(headTag, '#order#', order.orderId)}" var="headTag"/>
<c:set value="${fn:replace(headTag, '#ordergrandtotal#', order.grandTotal)}" var="headTag"/>
<c:set value="${fn:replace(headTag, '#ordersubtotal#', order.subTotal)}" var="headTag"/>
<c:set value="${fn:replace(headTag, '#ordertotalquantity#', order.totalQuantity)}" var="headTag"/>
<c:set value="${fn:replace(headTag, '#orderdiscount#', order.promoAmount)}" var="headTag"/>
<c:out value="${headTag}" escapeXml="false"/>
</head> 
<body>
<%
	// NOTE: Amazon computes tax after discount. To match order total, gTAX_AFTER_DISCOUNT must be true.
	// splitting custom shipping will not work because amazon splits invoice by shipping/MerchantId

Map<String, Configuration> siteConfig = (Map<String, Configuration>) request.getAttribute("siteConfig");
Order order = (Order) request.getAttribute("order");

NumberFormat nf = new DecimalFormat("#0.00");

StringBuffer xmlCart = new StringBuffer(
		  "<?xml version=\"1.0\" encoding=\"UTF-8\"?>"
		+ "<Order xmlns=\"http://payments.amazon.com/checkout/2009-05-15/\">"
		+ "<Cart>"
		+ "<Items>"
		);

for (LineItem lineItem: order.getLineItems()) {
	xmlCart.append(
			  "<Item>"
			+ "<SKU>" 
			+ lineItem.getProduct().getSku() 
			+ "</SKU>"
			+ "<MerchantId>"
			+ siteConfig.get("AMAZON_MERCHANT_ID").getValue().trim()
			+ "</MerchantId>" 
			+ "<Title>"
			+ lineItem.getProduct().getName().replace("�", "")
			+ "</Title>"
			);
	
	if (lineItem.getProductAttributes() != null && !lineItem.getProductAttributes().isEmpty()) {
		xmlCart.append("<Description>");
		boolean start = true;
		for (ProductAttribute productAttribute: lineItem.getProductAttributes()) {
			if (start) {
				start = false;
			} else {
				xmlCart.append(", ");
			}
			xmlCart.append(productAttribute.getOptionName() + ": " + productAttribute.getValueString());		
		}
		xmlCart.append("</Description>");		
	}
	
	xmlCart.append(
			  "<Price>"
			+ "<Amount>" 
			+ nf.format(lineItem.getUnitPrice())
			+ "</Amount>"
			+ "<CurrencyCode>" + siteConfig.get("CURRENCY").getValue() + "</CurrencyCode>"
			+ "</Price>"
			+ "<Quantity>"
			+ ((lineItem.getProduct().getCaseContent() == null) ? 1 : lineItem.getProduct().getCaseContent()) * lineItem.getQuantity()
			+ "</Quantity>"
			);
	if (lineItem.getProduct().isTaxable()) {			
		xmlCart.append(
		  "<TaxTableId>Tax-Table-1</TaxTableId>"
		);
	}
	
	xmlCart.append(
			  "<ShippingMethodIds>"
			+   "<ShippingMethodId>Ship-Method-1</ShippingMethodId>"
			+ "</ShippingMethodIds>"
			);

	// line number
	xmlCart.append(
			  "<ItemCustomData>"
			+   "<LineNumber>" + lineItem.getLineNumber() + "</LineNumber>"
			+ "</ItemCustomData>"
			);	
	
	xmlCart.append("</Item>");	
}

xmlCart.append("</Items>");

if (order.getPromo() != null && order.getPromo().getTitle() != null && order.getPromo().getDiscountType().equals("order")) {
	xmlCart.append("<CartPromotionId>Cart-Promotion-1</CartPromotionId>");
}

// order id
xmlCart.append(
		  "<CartCustomData>"
		+   "<OrderNumber>" + order.getOrderId() + "</OrderNumber>"
		+ "</CartCustomData>"
		);

xmlCart.append("</Cart>");	

xmlCart.append(
		  "<TaxTables>"
		+   "<TaxTable>"
		+     "<TaxTableId>Tax-Table-1</TaxTableId>"
		+ 	  "<TaxRules>"	
		+ 		"<TaxRule>"	
		+ 		  "<Rate>"
		+ order.getTaxRate()/100
		+         "</Rate>"	
		+ 		  "<PredefinedRegion>WorldAll</PredefinedRegion>"
		+       "</TaxRule>"			
		+     "</TaxRules>"	
		+   "</TaxTable>"		
		+ "</TaxTables>"
		);

if (order.getPromo() != null && order.getPromo().getTitle() != null && order.getPromo().getDiscountType().equals("order")) {
	xmlCart.append(
	  "<Promotions>"
	+   "<Promotion>"
	+     "<PromotionId>Cart-Promotion-1</PromotionId>"
	+ 	  "<Description>"
	+ order.getPromo().getTitle()
	+  	  "</Description>"	
	+ 	  "<Benefit>"	
	+ 	    "<FixedAmountDiscount>"
	+ 	      "<Amount>"
	+ nf.format(order.getPromoAmount())
	+ 	      "</Amount>"
	+ 	      "<CurrencyCode>" + siteConfig.get("CURRENCY").getValue() + "</CurrencyCode>"
	+       "</FixedAmountDiscount>"	
	+     "</Benefit>"			
	+   "</Promotion>"		
	+ "</Promotions>"
	);	
}

double shippingCost = 0.00;
if (order.getShippingCost() != null) {
	shippingCost = shippingCost + order.getShippingCost();
}
if (order.ishasCustomShipping()) {
	shippingCost = shippingCost + order.getCustomShippingCost();
}

String shippingMethod = "";
if (order.getShippingMethod() != null) {
	shippingMethod = order.getShippingMethod().replace("�", "").replace("<sup>", "").replace("</sup>", "");
}
if (order.ishasCustomShipping()) {
	if (shippingMethod.trim().length() > 0) {
		shippingMethod = shippingMethod + " / " + order.getCustomShippingTitle();		
	} else {
		shippingMethod = order.getCustomShippingTitle();
	}
}

xmlCart.append(
		  "<ShippingMethods>"
		+ "<ShippingMethod>"
		+ "<ShippingMethodId>Ship-Method-1</ShippingMethodId>"
		+ "<ServiceLevel>Standard</ServiceLevel>"
		+ "<Rate>"
		+ "<ShipmentBased>"
		+ "<Amount>"
		+ nf.format(shippingCost)
		+ "</Amount>"
		+ "<CurrencyCode>" + siteConfig.get("CURRENCY").getValue() + "</CurrencyCode>"
		+ "</ShipmentBased>"
		+ "</Rate>"
		+ "<IncludedRegions>"
		+ "<PredefinedRegion>WorldAll</PredefinedRegion>"
		+ "</IncludedRegions>"
		+ "<DisplayableShippingLabel>" 
		+ shippingMethod
		+ "</DisplayableShippingLabel>"
		+ "</ShippingMethod>"
		+ "</ShippingMethods>"
		);

xmlCart.append(
		  "<ShippingAddresses>"
		+ "<ShippingAddress>"
		+ "  <Name>"
		+ order.getShipping().getFirstName() + " " + order.getShipping().getLastName()
		+ "  </Name>"
		+ "  <AddressFieldOne>"
		+ order.getShipping().getAddr1()
		);
if (order.getShipping().getAddr2() != null && order.getShipping().getAddr2().length() > 0) {
	xmlCart.append(" " + order.getShipping().getAddr2());
}
xmlCart.append(
		  "  </AddressFieldOne>"
		+ "  <City>"
		+ order.getShipping().getCity()
		+ "  </City>" 
		+ "  <State>"
		+ order.getShipping().getStateProvince()
		+ "  </State>"
		+ "  <PostalCode>"
		+ order.getShipping().getZip()
		+ "  </PostalCode>"
		+ "  <CountryCode>"
		+ order.getShipping().getCountry()
		+ "  </CountryCode>"
		+ "  <PhoneNumber>"
		+ order.getShipping().getPhone()
		+ "  </PhoneNumber>"
		+ "</ShippingAddress>"
		+ "</ShippingAddresses>"
		+ "<DisablePromotionCode>true</DisablePromotionCode>"
		+ "</Order>"		
		);

String base64XmlCart = new String(Base64.encodeBase64(xmlCart.toString().getBytes()));
String signature = AmazonApi.getSignature(xmlCart.toString(), siteConfig.get("AMAZON_SECRET_KEY").getValue().trim());
%>

<c:set value="${invoiceLayout.headerHtml}" var="headerHtml"/>
<c:set value="${fn:replace(headerHtml, '#order#', order.orderId)}" var="headerHtml"/>
<c:set value="${fn:replace(headerHtml, '#ordergrandtotal#', order.grandTotal)}" var="headerHtml"/>
<c:set value="${fn:replace(headerHtml, '#ordersubtotal#', order.subTotal)}" var="headerHtml"/>
<c:set value="${fn:replace(headerHtml, '#ordertotalquantity#', order.totalQuantity)}" var="headerHtml"/>
<c:set value="${fn:replace(headerHtml, '#orderdiscount#', order.promoAmount)}" var="headerHtml"/>
<c:out value="${headerHtml}" escapeXml="false"/>

<form method="POST" name="amazonForm" action="<c:out value="${siteConfig['AMAZON_URL'].value}"/>checkout/<c:out value="${fn:trim(siteConfig['AMAZON_MERCHANT_ID'].value)}"/>?order-channel=apm"> <%-- &debug=true --%>
<input type="hidden" name="order-input" value="type:merchant-signed-order/aws-accesskey/1;order:<%= base64XmlCart %>;signature:<%= signature %>;aws-access-key-id:<c:out value="${fn:trim(siteConfig['AMAZON_ACCESS_KEY'].value)}"/>">
<noscript>
<input alt="Checkout with Amazon Payments" src="<c:out value="${siteConfig['AMAZON_URL'].value}"/>gp/cba/button?ie=UTF8&color=orange&background=white&cartOwnerId=<c:out value="${fn:trim(siteConfig['AMAZON_MERCHANT_ID'].value)}"/>&size=medium" type="image">
</noscript>
</form>

<c:set value="${invoiceLayout.footerHtml}" var="footerHtml"/>
<c:set value="${fn:replace(footerHtml, '#order#', order.orderId)}" var="footerHtml"/>
<c:set value="${fn:replace(footerHtml, '#ordergrandtotal#', order.grandTotal)}" var="footerHtml"/>
<c:set value="${fn:replace(footerHtml, '#ordersubtotal#', order.subTotal)}" var="footerHtml"/>
<c:set value="${fn:replace(footerHtml, '#ordertotalquantity#', order.totalQuantity)}" var="footerHtml"/>
<c:set value="${fn:replace(footerHtml, '#orderdiscount#', order.promoAmount)}" var="footerHtml"/>
<c:out value="${footerHtml}" escapeXml="false"/>

<%@ include file="/WEB-INF/jsp/frontend/checkout/amazonCustom.jsp" %>
</body>
<script type="text/javascript" language="JavaScript">
<!--
document.amazonForm.submit();
//-->
</script>
</html>
