<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib uri="/WEB-INF/tlds/spring.tld" prefix="spring" %>
<script type="text/JavaScript">
<!-- //test test
function checkCode(){
  if ( document.getElementById('__remove_payment_id').value != "" ) {
  	alert("Do you want to delete the card?");
  	return true;
  }
}
window.onload=function(){
	document.getElementById('removePayment').style.display="none";
	if(document.getElementById('__paymentId').value == "-2"){
		document.getElementById('ccInfoFormId').style.display="block";		
	}
};
function setPayment(dropdown){
	 var index  = dropdown.selectedIndex;
	 var selValue = dropdown.options[index].value;
	 if(selValue == "-1") {
		document.getElementById('removePayment').style.display="none";
	 }
	 if(selValue == "-2") {
		document.getElementById('ccInfoFormId').style.display="block";
		document.getElementById('removePayment').style.display="none";
	 } else {
		document.getElementById('ccInfoFormId').style.display="none";
		if(selValue != "-1" && selValue != "-2") {
		document.getElementById('removePayment').style.display="block";
		}
	 }
}
//-->
</script>

<div id="paymentMethodsList" class="gatewayPaymentMethods">
<!-- <div id="paymentMethodsList" class="gatewayPaymentMethods" <c:if test="${model.gatewayPaymentMethods == null && fn:length(model.gatewayPaymentMethods) == 0}">style="display:none;"</c:if>>
 -->
	<div class="ccList">
	<div class="divFormName">Select Payment: </div>
	<div class="paymentType">
	<spring:bind path="orderForm.order.ccToken">
	<select id="__paymentId" name="<c:out value="${status.expression}"/>" onchange="setPayment(this.form.__paymentId)">
          <option value="-1"  <c:if test="${status.value == '-1'}">selected</c:if>>Please Select</option>
          <option value="-2"  <c:if test="${status.value == '-2'}">selected</c:if>>Add New Card</option>
		<c:forEach items="${model.gatewayPaymentMethods}" var="paymentMethod">
			<option value="${paymentMethod.key}" <c:if test="${status.value == paymentMethod.key}">selected</c:if>><c:out value="${paymentMethod.value}"/></option>
		</c:forEach>
	</select>
	</spring:bind>
	</div>
	</div>
	<div class="button remove" id="removePayment" >
		 <input type="submit" id="__remove_payment_id" name="__remove_payment" onclick="return checkCode();" value="Remove" />
	</div>
	<!-- 
	<div class="button addNew">
		<input id="_add_payment_id" name="_add_payment" type="button" value="AddNewCard"/>
	</div> -->
</div>
<div style="clear:both;"></div>
<div id="ccInfoFormId" class="divCCInfoForm" <c:if test="${model.gatewayPaymentMethods != null && fn:length(model.gatewayPaymentMethods) gt 0}">style="display:none;"</c:if>>
	<div class="divCardType">
	<div class="divFormName"><fmt:message key="cardType" />:* </div>
	<div class="divCCType">
	  <spring:bind path="orderForm.tempCreditCard.type">
	  <select name="<c:out value="${status.expression}"/>" >
	    <c:forTokens items="${siteConfig['CREDIT_CARD_PAYMENT'].value}" delims="," var="cc">
          <option value="${cc}" <c:if test="${status.value == cc}">selected</c:if>><fmt:message key="${cc}"/></option>
        </c:forTokens>
	  </select>
      <font color = "red"><c:out value="${status.errorMessage}"/></font>
      </spring:bind>   
	</div>
	</div>
	<div style="clear:both;"></div>
	<div class="divCardName">
	<div class="divFormName"><fmt:message key="cardNumber" />:* </div>	
	<div class="divCCNumber">
		   <spring:bind path="orderForm.tempCreditCard.number">
		   <input id="ccNum" type="text" name="<c:out value="${status.expression}"/>" value="<c:out value="${status.value}"/>" <c:if test="${orderForm.order.paymentMethod != 'Credit Card'}">disabled</c:if> maxlength="16" size="16">
	       <font color = "red"><c:out value="${status.errorMessage}"/></font>
	       </spring:bind>   
	       <!-- debug: <c:out value="${orderForm.tempCreditCard.paymentNote}"/> -->
	</div>
	</div>
	<div style="clear:both;"></div>
	<div class="divCardVeriCode">
	     <div class="divFormName"><fmt:message key="cardVerificationCode" />:*</div>
	     <div>
		   <spring:bind path="orderForm.tempCreditCard.cardCode">
		   <input type="text" name="<c:out value="${status.expression}"/>" value="<c:out value="${status.value}"/>" maxlength="4" size="4">
	       <font color = "red"><c:out value="${status.errorMessage}"/></font>
	       </spring:bind>   
		   <a class="cvv2" href="#" onClick="openW('assets/CVV2.html')"><fmt:message key="shoppingcart.whereDoIFindthisCode" /></a>
		 </div>
	</div>
	<div style="clear:both;"></div>
	<div class="divCardExpDate">
	     <div class="divFormName"><fmt:message key="expDate" /> (MM/YY):*</div>
	     <div>
		   <spring:bind path="orderForm.tempCreditCard.expireMonth">
		   <select name="<c:out value="${status.expression}"/>">
		    <c:forTokens items="01,02,03,04,05,06,07,08,09,10,11,12" delims="," var="ccExpireMonth">
	          <option value="${ccExpireMonth}" <c:if test="${status.value==ccExpireMonth}">selected</c:if>><c:out value="${ccExpireMonth}"/></option>
	        </c:forTokens>
		   </select>
	       </spring:bind> 
	       <spring:bind path="orderForm.tempCreditCard.expireYear">
		   <select name="<c:out value="${status.expression}"/>">
		    <c:forEach items="${model.expireYears}" var="ccExpireYear">
	         <option value="${ccExpireYear}" <c:if test="${status.value==ccExpireYear}">selected</c:if>><c:out value="${ccExpireYear}"/></option>
		    </c:forEach>
		   </select>
		   <font color="red"><c:out value="${status.errorMessage}"/></font>
	       </spring:bind>  
		 </div>
	</div>
	<div style="clear:both;"></div>
	<div class="divSaveCC">
		<form:checkbox path="saveCC" value="true"  />Do you want us to save your card?
	</div>
	<div style="clear:both;"></div>
	<c:if test="${siteConfig['CC_BILLING_ADDRESS'].value == 'true'}" >
	<div class="ccBilling">
		<lable><fmt:message key="f_ccBillingAddres"/>: </lable>
		<form:textarea path="order.ccBillingAddress"/>
	</div>
	</c:if>
</div>

<script type="text/JavaScript">
<!--
function addCard() {
  if (document.getElementById('_add_payment_id').value != "") {
    document.getElementById('ccInfoFormId').style.display="block";
    document.getElementById('paymentMethodsList').style.display="none";
  } else {	
	document.getElementById('paymentMethodsList').style.display="block";
    document.getElementById('ccInfoFormId').style.display="none";
  } 
}
//-->
</script>