<%@ page import="com.webjaguar.model.*, java.text.*" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ taglib uri="/WEB-INF/tlds/custom.tld" prefix="wj" %>
<html>
<head>
<c:set value="${invoiceLayout.headTag}" var="headTag"/>
<c:set value="${fn:replace(headTag, '#order#', order.orderId)}" var="headTag"/>
<c:set value="${fn:replace(headTag, '#ordergrandtotal#', order.grandTotal)}" var="headTag"/>
<c:set value="${fn:replace(headTag, '#ordersubtotal#', order.subTotal)}" var="headTag"/>
<c:set value="${fn:replace(headTag, '#ordertotalquantity#', order.totalQuantity)}" var="headTag"/>
<c:set value="${fn:replace(headTag, '#orderdiscount#', order.promoAmount)}" var="headTag"/>
<c:out value="${headTag}" escapeXml="false"/>
</head>
<body>
<%
 String secureUrl = request.getScheme() + "://" + request.getHeader("host") + request.getContextPath() + "/"; 
%>
<c:set value="${invoiceLayout.headerHtml}" var="headerHtml"/>
<c:set value="${fn:replace(headerHtml, '#order#', order.orderId)}" var="headerHtml"/>
<c:set value="${fn:replace(headerHtml, '#ordergrandtotal#', order.grandTotal)}" var="headerHtml"/>
<c:set value="${fn:replace(headerHtml, '#ordersubtotal#', order.subTotal)}" var="headerHtml"/>
<c:set value="${fn:replace(headerHtml, '#ordertotalquantity#', order.totalQuantity)}" var="headerHtml"/>
<c:set value="${fn:replace(headerHtml, '#orderdiscount#', order.promoAmount)}" var="headerHtml"/>
<c:out value="${headerHtml}" escapeXml="false"/>

<form name="geMoneyForm" method="POST" action="https://<c:out value="${siteConfig['GEMONEY_DOMAIN'].value}"/>/process/shoppingCartProcessor.do">

<input type="hidden" name="shopperId" value="<%= request.getSession().getId() %>"/>
<input type="hidden" name="merchantId" value="<c:out value="${siteConfig['GEMONEY_MERCHANTID'].value}"/>"/>
<input type="hidden" name="homeUrl" value="<c:out value="${siteConfig['SITE_URL'].value}"/>"/>
<input type="hidden" name="clientTransactionId" value="<c:out value="${order.orderId}"/>"/>
<input type="hidden" name="purchaseNotificationUrl" value="<%= secureUrl %>geMoney/purchaseNotification.jhtm"/>
<input type="hidden" name="clientUnsuccessPurchaseUrl" value="<%= secureUrl %>checkout.jhtm"/>
<input type="hidden" name="clientSuccessfulPurchaseUrl" value="<%= secureUrl %>invoice.jhtm?order=${order.orderId}"/>
<input type="hidden" name="clientSuccessfulApplyUrl" value="<%= secureUrl %>invoice.jhtm?order=${order.orderId}"/>
<input type="hidden" name="clientUnsuccessApplyUrl" value="<%= secureUrl %>checkout.jhtm"/>
<input type="hidden" name="creditApplyNotificationUrl" value="<%= secureUrl %>geCapital/creditApplyNotification.jhtm"/>
<input type="hidden" name="billToFirstName" value="${order.billing.firstName}"/>
<input type="hidden" name="billToMiddleInitial" value=""/>
<input type="hidden" name="billToLastName" value="${order.billing.lastName}"/>
<input type="hidden" name="billToAddress1" value="${order.billing.addr1}"/>
<input type="hidden" name="billToAddress2" value="${order.billing.addr2}"/>
<input type="hidden" name="billToCity" value="${order.billing.city}"/>
<input type="hidden" name="billToState" value="${order.billing.stateProvince}"/>
<input type="hidden" name="billToZipCode" value="${fn:trim(order.billing.zip)}"/>
<input type="hidden" name="billToHomePhone" value="${fn:trim(order.billing.phone)}"/>
<input type="hidden" name="transactionAmount" value="<c:out value="${wj:decimalFormatString(order.balance,null)}" escapeXml="false"/>"/>
<input type="hidden" name="promoCode" value="<c:out value="${siteConfig['GEMONEY_PROMOCODE'].value}"/>"/>
<input type="hidden" name="clientTestFlag" value="<c:choose><c:when test="${siteConfig['GEMONEY_DOMAIN'].value == 'www.secureb2c.com'}">N</c:when><c:otherwise>Y</c:otherwise></c:choose>"/>
<input type="hidden" name="clientToken" value="<c:out value="${geMoneyStrToken}"/>"/>
<input type="hidden" name="billToAccountNumber" value="<c:out value="${order.geMoney.acctNumber}"/>"/>
<input type="hidden" name="billToSsn" value="<c:out value="${order.geMoney.ssn}"/>"/>

<noscript>
<input type="submit" value="Please click to continue paying by <c:out value="${siteConfig['GEMONEY_PROGRAM_NAME'].value}"/>">
</noscript>

</form>
<c:set value="${invoiceLayout.footerHtml}" var="footerHtml"/>
<c:set value="${fn:replace(footerHtml, '#order#', order.orderId)}" var="footerHtml"/>
<c:set value="${fn:replace(footerHtml, '#ordergrandtotal#', order.grandTotal)}" var="footerHtml"/>
<c:set value="${fn:replace(footerHtml, '#ordersubtotal#', order.subTotal)}" var="footerHtml"/>
<c:set value="${fn:replace(footerHtml, '#ordertotalquantity#', order.totalQuantity)}" var="footerHtml"/>
<c:set value="${fn:replace(footerHtml, '#orderdiscount#', order.promoAmount)}" var="footerHtml"/>
<c:out value="${footerHtml}" escapeXml="false"/>

<%@ include file="/WEB-INF/jsp/frontend/checkout/geMoneyPostCustom.jsp" %> 
</body>
<script type="text/javascript" language="JavaScript">
<!--
document.geMoneyForm.submit();
//-->
</script>
</html>
