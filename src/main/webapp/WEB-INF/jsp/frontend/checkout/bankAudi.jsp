<%@ page import="java.util.*" %>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>

<%
Map<String, String> responseCode = new HashMap<String, String>();
responseCode.put("0", "Transaction Successful");
responseCode.put("?", "Transaction status is unknown");
responseCode.put("1", "Unknown Error");
responseCode.put("2", "Bank Declined Transaction");
responseCode.put("3", "No Reply from Bank");
responseCode.put("4", "Expired Card");
responseCode.put("5", "Insufficient funds");
responseCode.put("6", "Error Communicating with Bank");
responseCode.put("7", "Payment Server System Error");
responseCode.put("8", "Transaction Type Not Supported");
responseCode.put("9", "Bank declined transaction (Do not contact Bank)");
responseCode.put("A", "Transaction Aborted");
responseCode.put("C", "Transaction Cancelled");
responseCode.put("D", "Deferred transaction has been received and is awaiting processing");
responseCode.put("E", "Invalid Credit Card");
responseCode.put("F", "3D Secure Authentication failed");
responseCode.put("I", "Card Security Code verification failed");
responseCode.put("G", "Invalid Merchant");
responseCode.put("L", "Shopping Transaction Locked (Please try the transaction again later)");
responseCode.put("N", "Cardholder is not enrolled in Authentication scheme");
responseCode.put("P", "Transaction has been received by the Payment Adaptor and is being processed");
responseCode.put("R", "Transaction was not processed - Reached limit of retry attempts allowed");
responseCode.put("S", "Duplicate SessionID (OrderInfo)");
responseCode.put("T", "Address Verification Failed");
responseCode.put("U", "Card Security Code Failed");
responseCode.put("V", "Address Verification and Card Security Code Failed");
responseCode.put("X", "Credit Card Blocked");
responseCode.put("Y", "Invalid URL");                
responseCode.put("B", "Transaction was not completed");                
responseCode.put("M", "Please enter all required fields");                
responseCode.put("J", "Transaction already in use");
responseCode.put("BL", "Card Bin Limit Reached");                
responseCode.put("CL", "Card Limit Reached");                
responseCode.put("LM", "Merchant Amount Limit Reached");                
responseCode.put("Q", "IP Blocked");                
responseCode.put("R", "Transaction was not processed - Reached limit of retry attempts allowed");                
responseCode.put("Z", "Bin Blocked");
pageContext.setAttribute("responseCode", responseCode);
%>


<tiles:insertDefinition name="${_template}" flush="true">
  <tiles:putAttribute name="content" type="string">

<b>Payment Result</b>
<hr>
<div style="
	padding-top: 10px;
	padding-bottom: 10px;
	color: #FF0000;
	font-weight:bold;
	font-size: 12px;
">
Transaction is <c:choose><c:when test="${model.bankAudi.vpc_TxnResponseCode == '0'}">Successful</c:when><c:otherwise>Refused</c:otherwise></c:choose>
</div>
<table>
<tr>
 <td style="text-align:right">Merchant Transaction Reference:</td><td><c:out value="${model.bankAudi.merchTxnRef}"/></td>
</tr>
<tr>
 <td style="text-align:right">Merchant ID:</td><td><c:out value="${model.bankAudi.merchant}"/></td>
</tr>
<tr>
 <td style="text-align:right">Order Information:</td><td><c:out value="${model.bankAudi.orderInfo}"/></td>
</tr>
<tr>
 <td style="text-align:right">Purchase Amount:</td><td><fmt:formatNumber value="${model.bankAudi.amount/100}" pattern="#,##0.00"/></td>
</tr>
<tr>
 <td style="text-align:right">VPC Transaction Response Code:</td><td><c:out value="${model.bankAudi.vpc_TxnResponseCode}"/></td>
</tr>
<tr>
 <td style="text-align:right">Transaction Response Code Description:</td><td><c:out value="${responseCode[model.bankAudi.vpc_TxnResponseCode]}"/></td>
</tr>
<tr>
 <td style="text-align:right">Message:</td><td><c:out value="${model.bankAudi.vpc_Message}"/></td>
</tr>
<c:if test="${model.bankAudi.vpc_TxnResponseCode != '7'}">
<tr>
 <td style="text-align:right">Receipt Number:</td><td><c:out value="${model.bankAudi.vpc_ReceiptNo}"/></td>
</tr>
<tr>
 <td style="text-align:right">Transaction Number:</td><td><c:out value="${model.bankAudi.vpc_TransactionNo}"/></td>
</tr>
<tr>
 <td style="text-align:right">Acquirer Response Code:</td><td><c:out value="${model.bankAudi.vpc_AcqResponseCode}"/></td>
</tr>
<tr>
 <td style="text-align:right">Bank Authorization ID:</td><td><c:out value="${model.bankAudi.vpc_AuthorizeId}"/></td>
</tr>
<tr>
 <td style="text-align:right">Batch Number:</td><td><c:out value="${model.bankAudi.vpc_BatchNo}"/></td>
</tr>
<tr>
 <td style="text-align:right">Card Type:</td><td><c:out value="${model.bankAudi.vpc_Card}"/></td>
</tr>
</c:if>
</table>
<br/><br/>
<b>Please keep this information for order tracking.</b>
  </tiles:putAttribute>
</tiles:insertDefinition>
