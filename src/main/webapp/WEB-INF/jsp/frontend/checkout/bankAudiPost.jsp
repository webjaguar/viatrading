<%@ page import="com.webjaguar.model.*, java.text.*,java.util.*,com.webjaguar.thirdparty.payment.bankaudi.*" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<html>
<body>
<form name="bankAudiForm" method="POST" action="https://gw1.audicards.com/TPGWeb/payment/prepayment.action">

<c:set var="CREDIT_ACCOUNT" value="${siteConfig['CREDIT_ACCOUNT'].value}" />
<c:set var="CREDIT_PASSWORD" value="${siteConfig['CREDIT_PASSWORD'].value}" />
<c:set var="CREDIT_MERCHANT_ID" value="${siteConfig['CREDIT_MERCHANT_ID'].value}" />
<% 
 NumberFormat nf = new DecimalFormat("#0");
 Order order = (Order) request.getAttribute("order");
 pageContext.setAttribute("orderGrandTotal", nf.format(order.getBalance()*100));

 String secureUrl = request.getScheme() + "://" + request.getHeader("host") + request.getContextPath() + "/";  
 
 // create vpc_SecureHash
 Map<String, String> params = new TreeMap<String, String>();
 params.put("accessCode", (String) pageContext.getAttribute("CREDIT_ACCOUNT"));
 params.put("merchTxnRef", order.getOrderId() + "");
 params.put("merchant", (String) pageContext.getAttribute("CREDIT_MERCHANT_ID"));
 params.put("orderInfo", order.getOrderId() + "");
 params.put("amount", (String) pageContext.getAttribute("orderGrandTotal"));
 params.put("returnURL", secureUrl + "bankaudi.jhtm?action=py");
 StringBuffer data = new StringBuffer((String) pageContext.getAttribute("CREDIT_PASSWORD"));
 for (String key: params.keySet()) {
	 data.append(params.get(key));
 }
 BankAudi bankAudi = new BankAudi();
 pageContext.setAttribute("vpc_SecureHash", bankAudi.md5(data.toString()));
%>
<input type="hidden" name="accessCode" value="<c:out value="${siteConfig['CREDIT_ACCOUNT'].value}"/>">
<input type="hidden" name="amount" value="<c:out value="${orderGrandTotal}"/>">
<input type="hidden" name="merchTxnRef" value="<c:out value="${order.orderId}"/>">
<input type="hidden" name="merchant" value="<c:out value="${siteConfig['CREDIT_MERCHANT_ID'].value}"/>">
<input type="hidden" name="orderInfo" value="<c:out value="${order.orderId}"/>">
<input type="hidden" name="returnURL" value="<c:out value="<%= secureUrl %>"/>bankaudi.jhtm?action=py">
<input type="hidden" name="vpc_SecureHash" value="<c:out value="${vpc_SecureHash}"/>" />

<noscript>
<input type="submit" value="Please click to continue paying by BankAudi">
</noscript>

</form>
</body>
<script type="text/javascript" language="JavaScript">
<!--
document.bankAudiForm.submit();
//-->
</script>
</html>