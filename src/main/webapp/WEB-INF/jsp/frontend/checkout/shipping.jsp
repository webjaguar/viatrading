<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="/WEB-INF/tlds/spring.tld" prefix="spring" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>

<tiles:insertDefinition name="${_template}" flush="true">
  <tiles:putAttribute name="content" type="string">

<div><c:out value="${model.shippingLayout.headerHtml}" escapeXml="false"/></div>
<div style="width:95%;margin:auto;">

<table border="0" cellpadding="3" cellspacing="1" width="100%">
<tr><td colspan="2" class="addressBook"><fmt:message key="addressBook" /></td></tr>
<c:forEach items="${model.addressBook}" var="address">
<tr>
<td>
<b><c:out value="${address.firstName}"/> <c:out value="${address.lastName}"/></b><br />
<span class="addressBook"><c:if test="${address.company != ''}"><c:out value="${address.company}"/>,</c:if>
<c:out value="${address.addr1}"/><c:if test="${!empty address.addr2}"> <c:out value="${address.addr2}"/></c:if>,
<c:out value="${address.city}"/>, 
<c:out value="${address.stateProvince}"/> <c:out value="${address.zip}"/>,
<c:out value="${countries[address.country]}" /></span><br />
</td>
<td align="right">
<form  action="checkout.jhtm" method="post">
<input type="submit" value="<fmt:message key="shoppingcart.shipToThisAddress" />" name="_target1">
<input type="hidden" name="newShippingAddress" value="false">
<input type="hidden" name="order.shipping.id" value="<c:out value="${address.id}"/>">
<input type="hidden" name="order.shipping.lastName" value="<c:out value="${address.lastName}"/>"/>
<input type="hidden" name="order.shipping.firstName" value="<c:out value="${address.firstName}"/>"/>
<input type="hidden" name="order.shipping.company" value="<c:out value="${address.company}"/>"/>
<input type="hidden" name="order.shipping.addr1" value="<c:out value="${address.addr1}"/>"/>
<input type="hidden" name="order.shipping.addr2" value="<c:out value="${address.addr2}"/>"/>
<input type="hidden" name="order.shipping.city" value="<c:out value="${address.city}"/>"/>
<input type="hidden" name="order.shipping.stateProvince" value="<c:out value="${address.stateProvince}"/>"/>
<input type="hidden" name="order.shipping.zip" value="<c:out value="${address.zip}"/>"/>
<input type="hidden" name="order.shipping.country" value="<c:out value="${address.country}"/>"/>
<input type="hidden" name="order.shipping.phone" value="<c:out value="${address.phone}"/>"/>
<input type="hidden" name="order.shipping.cellPhone" value="<c:out value="${address.cellPhone}"/>"/>
<input type="hidden" name="order.shipping.fax" value="<c:out value="${address.fax}"/>"/>
<input type="hidden" name="order.shipping.residential" value="<c:out value="${address.residential}"/>"/>
<input type="hidden" name="order.shipping.liftGate" value="<c:out value="${address.liftGate}"/>"/>
<input type="hidden" name="order.shipping.code" value="<c:out value="${address.code}"/>">
</form>
</td>
</tr>
</c:forEach>
</table>
<p>  
<c:if test="${siteConfig['ADD_ADDRESS_ON_CHECKOUT'].value == 'true' or not orderForm.newShippingAddress}">
<form:form commandName="orderForm" method="post">
<form:hidden path="newShippingAddress" />
<form:hidden path="order.shipping.id" />
<input type="hidden" name="save" value="true">
<table border="0" cellpadding="3" cellspacing="1" width="100%">
  <tr>
  	<td colspan="2" class="addressBook">
  	  <c:choose>
  	  <c:when test="${orderForm.newShippingAddress}">
  	    <fmt:message key="addressNew" />
  	  </c:when>
  	  <c:otherwise>
  	    <fmt:message key="addressEdit" />
  	  </c:otherwise>
	  </c:choose>
  	</td>
  </tr>
  <tr>
    <td class="formNameReq"><fmt:message key="firstName" />:</td>
    <td>
      <form:input path="order.shipping.firstName" htmlEscape="true" />
      <form:errors path="order.shipping.firstName" cssClass="error" />
    </td>
  </tr>
  <tr>
    <td class="formNameReq"><fmt:message key="lastName" />:</td>
    <td>
      <spring:bind path="orderForm.order.shipping.lastName">
      <input type="text" name="<c:out value="${status.expression}"/>" value="<c:out value="${status.value}"/>"/>
      <span class="error"><c:out value="${status.errorMessage}"/></span>
      </spring:bind>
    </td>
  </tr>
  <tr>
    <td class="formNameReq"><fmt:message key='country' />:</td>
    <td>
      <spring:bind path="orderForm.order.shipping.country">
		<select id="country" name="<c:out value="${status.expression}"/>" onChange="toggleStateProvince(this)">
  	      <option value="">Please Select</option>
	      <c:forEach items="${model.countrylist}" var="country">
  	        <option value="${country.code}" <c:if test="${country.code == status.value}">selected</c:if>>${country.name}</option>
		  </c:forEach>
		</select>
		<span class="error"><c:out value="${status.errorMessage}"/></span>
      </spring:bind>
    </td>
  </tr>
  <tr>
    <td class="formName<c:if test="${siteConfig['CUSTOMERS_REQUIRED_COMPANY'].value == 'true'}">Req</c:if>"><fmt:message key="company" />:</td>
    <td>
      <spring:bind path="orderForm.order.shipping.company">
      <input type="text" name="<c:out value="${status.expression}"/>" value="<c:out value="${status.value}"/>"/>
      <span class="error"><c:out value="${status.errorMessage}"/></span>
      </spring:bind>
    </td>
  </tr>
  <tr>
    <td class="formNameReq"><fmt:message key="address" /> 1:</td>
    <td>
      <spring:bind path="orderForm.order.shipping.addr1">
      <input type="text" name="<c:out value="${status.expression}"/>" value="<c:out value="${status.value}"/>"/>
      <span class="error"><c:out value="${status.errorMessage}"/></span>
      </spring:bind>
    </td>
  </tr>
  <tr>
    <td class="formName"><fmt:message key="address" /> 2:</td>
    <td>
      <spring:bind path="orderForm.order.shipping.addr2">
      <input type="text" name="<c:out value="${status.expression}"/>" value="<c:out value="${status.value}"/>"/>
      </spring:bind>
    </td>
  </tr>
  <tr>
    <td class="formNameReq"><fmt:message key="city" />:</td>
    <td>
      <spring:bind path="orderForm.order.shipping.city">
      <input type="text" name="<c:out value="${status.expression}"/>" value="<c:out value="${status.value}"/>"/>
      <span class="error"><c:out value="${status.errorMessage}"/></span>
      </spring:bind>
    </td>
  </tr>
  <tr>
    <td class="formNameReq"><fmt:message key="stateProvince" />:</td>
    <td>
      <spring:bind path="orderForm.order.shipping.stateProvince">
      <table cellspacing="0" cellpadding="0">
       <tr>
       <td>
		<select id="state" name="<c:out value="${status.expression}"/>" <c:if test="${orderForm.order.shipping.country != 'US'}">disabled style="display:none;"</c:if>>
  	      <option value="">Please Select</option>
	      <c:forEach items="${model.statelist}" var="state">
  	        <option value="${state.code}" <c:if test="${state.code == status.value}">selected</c:if>>${state.name}</option>
		  </c:forEach>      
        </select>
		<select id="ca_province" name="<c:out value="${status.expression}"/>">
  	      <option value="">Please Select</option>
	      <c:forEach items="${model.caProvinceList}" var="province">
  	        <option value="${province.code}" <c:if test="${province.code == status.value}">selected</c:if>>${province.name}</option>
		  </c:forEach>      
        </select> 
      <input id="province" type="text" name="<c:out value="${status.expression}"/>" value="<c:out value="${status.value}"/>" <c:if test="${orderForm.order.shipping.country == 'US'}">disabled style="display:none;"</c:if>>
       </td>
       <td><div id="stateProvinceNA"><form:checkbox path="order.shipping.stateProvinceNA" id="addressStateProvinceNA" value="true" /><fmt:message key="notApplicable" /> Not Applicable</div></td>
       <td>&nbsp;</td>
       <td>
      <span class="error"><c:out value="${status.errorMessage}"/></span>
       </td>
       </tr>
      </table>
      </spring:bind>
    </td>
  </tr>  
  <tr>
    <td class="formNameReq"><fmt:message key="zipCode" />:</td>
    <td>
      <spring:bind path="orderForm.order.shipping.zip">
      <input type="text" name="<c:out value="${status.expression}"/>" value="<c:out value="${status.value}"/>"/>
      <span class="error"><c:out value="${status.errorMessage}"/></span>
      </spring:bind>
    </td>
  </tr>  
  <tr>
    <td class="formNameReq"><fmt:message key="phone" />:</td>
    <td>
      <spring:bind path="orderForm.order.shipping.phone">
      <input type="text" name="<c:out value="${status.expression}"/>" value="<c:out value="${status.value}"/>"/>
      <span class="error"><c:out value="${status.errorMessage}"/></span>
      </spring:bind>
    </td>
  </tr>
  <tr>
    <td class="formName"><fmt:message key="cellPhone" />:</td>
    <td>
      <form:input path="order.shipping.cellPhone"/>
    </td>
  </tr>  
  <tr>
    <td class="formName"><fmt:message key="fax" />:</td>
    <td>
      <spring:bind path="orderForm.order.shipping.fax">
      <input type="text" name="<c:out value="${status.expression}"/>" value="<c:out value="${status.value}"/>"/>
      </spring:bind>
    </td>
  </tr>
  <c:if test="${siteConfig['ADDRESS_RESIDENTIAL_COMMERCIAL'].value >= 2}">
    <tr>
    <td class="formName"><fmt:message key="deliveryType" />:</td>
    <td><form:radiobutton path="order.shipping.residential" value="true"/>: <fmt:message key="residential" /><br /><form:radiobutton path="order.shipping.residential" value="false"/>: <fmt:message key="commercial" /> </td>
    <td></td>
    </tr>
    <c:if test="${siteConfig['SHIPPING_LTL'].value == 'true'}">
      <tr>
      <td class="formName"><fmt:message key="liftGateDelivery" />:</td>
      <td><form:checkbox path="order.shipping.liftGate" value="true"/><a href="category.jhtm?cid=307" onclick="window.open(this.href,'','width=600,height=300,resizable=yes'); return false;"><img class="toolTipImg" src="assets/Image/Layout/question.gif" border="0" /></a></td>
      </tr>
    </c:if>  
  </c:if>
  <tr>
    <td>&nbsp;</td>
    <td><input type="submit" value="<fmt:message key="shoppingcart.shipToThisAddress" />" name="_target1"></td>
  </tr>
</table>
</form:form>
</c:if>

</div>
<c:out value="${model.shippingLayout.footerHtml}" escapeXml="false"/>

<script language="JavaScript">
<!--
function toggleStateProvince(el) {
  if (el.value == "US") {
      document.getElementById('state').disabled=false;
      document.getElementById('state').style.display="block";
      document.getElementById('ca_province').disabled=true;
      document.getElementById('ca_province').style.display="none";
      document.getElementById('province').disabled=true;
      document.getElementById('province').style.display="none";
      document.getElementById('addressStateProvinceNA').disabled=true;
      document.getElementById('stateProvinceNA').style.display="none";
  } else if (el.value == "CA") {
      document.getElementById('state').disabled=true;
      document.getElementById('state').style.display="none";
      document.getElementById('ca_province').disabled=false;
      document.getElementById('ca_province').style.display="block";
      document.getElementById('province').disabled=true;
      document.getElementById('province').style.display="none";
      document.getElementById('addressStateProvinceNA').disabled=true;
      document.getElementById('stateProvinceNA').style.display="none";
  } else {
      document.getElementById('state').disabled=true;
      document.getElementById('state').style.display="none";
      document.getElementById('ca_province').disabled=true;
      document.getElementById('ca_province').style.display="none";
      document.getElementById('province').disabled=false;
      document.getElementById('province').style.display="block";
      document.getElementById('addressStateProvinceNA').disabled=false;
      document.getElementById('stateProvinceNA').style.display="block";
  }
}
toggleStateProvince(document.getElementById('country'));
//-->
</script>

  </tiles:putAttribute>
</tiles:insertDefinition>