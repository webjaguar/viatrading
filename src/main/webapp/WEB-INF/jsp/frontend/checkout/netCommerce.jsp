<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>

<tiles:insertDefinition name="${_template}" flush="true">
  <tiles:putAttribute name="content" type="string">

<b>Payment Result</b>
<hr>
<div style="
	padding-top: 10px;
	padding-bottom: 10px;
	color: #FF0000;
	font-weight:bold;
	font-size: 12px;
">
Transaction is <c:choose><c:when test="${model.netCommerce.respVal == '1'}">Authorized</c:when><c:otherwise>Refused</c:otherwise></c:choose>
</div>
<p>
<b>Amount:</b> <fmt:formatNumber value="${model.netCommerce.txtAmount}" pattern="#,##0.00"/> <c:out value="${model.netCommerce.txtCurrency}"/><br>
<b>Order Number:</b> <c:out value="${model.netCommerce.txtIndex}"/><br>
<c:if test="${model.netCommerce.respVal == '1'}">
<b>Authorization Number:</b> <c:out value="${model.netCommerce.txtNumAut}"/><br>
</c:if>
<!-- 
<b>Description:</b> <c:out value="${model.netCommerce.respMsg}"/>
 -->
<p>
Please keep this information for order tracking.
  </tiles:putAttribute>
</tiles:insertDefinition>
