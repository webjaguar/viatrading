<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<c:set var="tempLineItem" value="${groupItems[lineItem.itemGroup]}"/>

<tr valign="top" class="parentSku invoice${currentItemIndex%2}">
<c:choose>
  <c:when test="${displaySkuAndName}">
  <td class="invoice " align="center"></td>
  <td class="invoice">
	<c:if test="${tempLineItem.customImageUrl != null}">
      <a href="assets/customImages/${order.orderId}/${tempLineItem.customImageUrl}" onclick="window.open(this.href,'','width=640,height=480,resizable=yes'); return false;"><img class="invoiceImage" src="assets/customImages/${order.orderId}/${tempLineItem.customImageUrl}" border="0" /></a>
    </c:if>    
    <c:if test="${siteConfig['SHOW_IMAGE_ON_INVOICE'].value == 'true' and tempLineItem.product.thumbnail != null}">
      <img class="invoiceImage" src="<c:if test="${!tempLineItem.product.thumbnail.absolute}">assets/Image/Product/thumb/</c:if>${tempLineItem.product.thumbnail.imageUrl}" border="0" />
    </c:if><div class="sku"><c:out value="${tempLineItem.product.sku}"/></div>
  </td>
  <td class="invoice"><c:if test="${tempLineItem.customXml != null}">Custom Frame - </c:if><c:out value="${tempLineItem.product.name}"/>
    <table border="0" cellspacing="1" cellpadding="0">
	  <c:forEach items="${tempLineItem.productAttributes}" var="productAttribute" varStatus="productAttributeStatus">
		<tr class="invoice_lineitem_attributes${productAttributeStatus.count%2}">
		<td class="optionName" style="white-space: nowrap" align="right"><c:out value="${productAttribute.optionName}"/>: </td>
		<td class="optionValue" style="padding-left:5px;"><c:out value="${productAttribute.valueString}" escapeXml="false"/></td>
		<c:if test="${(! empty productAttribute.optionPriceOriginal) && productAttribute.optionPriceOriginal != 0}">
		<td class="optionPrice"><fmt:formatNumber value="${productAttribute.optionPriceOriginal}" pattern="#,##0.00" /></td>
		<td class="optionNote" style="padding-left:5px;"><fmt:message key="${productAttribute.optionPriceMessageF}"/></td>
		</c:if>
		</tr>
	  </c:forEach>
	  <c:forEach items="${tempLineItem.asiProductAttributes}" var="asiProductAttribute" varStatus="asiProductAttributeStatus">
		<tr class="invoice_lineitem_attributes${asiProductAttributeStatus.count%2}">
		<td style="white-space: nowrap" align="right"><c:out value="${asiProductAttribute.asiOptionName}"/>: </td>
		<td style="width:100%;padding-left:5px;"><c:out value="${asiProductAttribute.asiOptionValue}" escapeXml="false"/></td>
		</tr>
	  </c:forEach>
	  <%@ include file="/WEB-INF/jsp/frontend/common/customLinesInvoice.jsp"%>
    </table>
	<c:if test="${tempLineItem.subscriptionInterval != null}">
	  <c:set var="intervalType" value="${fn:substring(tempLineItem.subscriptionInterval, 0, 1)}"/>
	  <c:set var="intervalUnit" value="${fn:substring(tempLineItem.subscriptionInterval, 1, 2)}"/>
	  <div class="invoice_lineitem_subscription">
		<fmt:message key="subscription"/> <fmt:message key="details"/> : <fmt:message key="delivery"/>
		<c:choose>
		  <c:when test="${intervalUnit == '1'}"><fmt:message key="every_${intervalType}"><fmt:param value="${intervalUnit}"/></fmt:message></c:when>
		  <c:otherwise><fmt:message key="every_${intervalType}s"><fmt:param value="${intervalUnit}"/></fmt:message></c:otherwise>
		</c:choose>
	  </div>
	  <div class="invoice_lineitem_subscription"><c:out value="${tempLineItem.subscriptionCode}"/></div>
	</c:if>
	<c:if test="${tempLineItem.attachment != null}">
      <a href="${_contextpath}/temp/Order/${orderForm.order.orderId}/${tempLineItem.attachment}">
    	<c:out value="${tempLineItem.attachment}"/>
      </a>
    </c:if>
  </td>
  <td class="invoice" align="center"></td>
  </c:when>
  <c:otherwise>
    <td class="invoice"></td>
    <td class="invoice"></td>
    <td class="invoice"></td>
  </c:otherwise>
  </c:choose>
      
  <c:set var="fieldsHeader" value="${orderForm.productFieldsHeader}"></c:set>
  <c:if test="${viewInvoice != null}">
    <c:set var="fieldsHeader" value="${productFieldsHeader}"></c:set>
  </c:if>
  <c:forEach items="${fieldsHeader}" var="pFHeader" varStatus="status"><c:set var="check" value="0"/>
    <c:if test="${pFHeader.showOnInvoice}">
	  <c:forEach items="${lineItem.productFields}" var="productField"> 
	    <c:if test="${pFHeader.id == productField.id}">
	      <td class="invoice"></td><c:set var="check" value="1"/>
	    </c:if>       
	  </c:forEach>
	  <c:if test="${check == 0}">
	    <td class="invoice">&nbsp;</td>
	  </c:if>
	</c:if>
  </c:forEach> 
    
<c:choose>
  <c:when test="${showQtyAndPrice}">
  <td class="invoice" align="center">
    <c:choose>
        <c:when test="${gSiteConfig['gVARIABLE_PRICE'] and tempLineItem.product.priceByCustomer}"></c:when>
        <c:otherwise>
          <c:out value="${tempLineItem.quantity}"/>
          <c:if test="${tempLineItem.priceCasePackQty != null}"><div class="casePackPriceQty">(<c:out value="${tempLineItem.priceCasePackQty}"/> per <c:out value="${siteConfig['PRICE_CASE_PACK_UNIT_TITLE'].value}" />)</div></c:if>
        </c:otherwise>
    </c:choose>       
  </td>
  <c:if test="${gSiteConfig['gINVENTORY'] && order.hasLowInventoryMessage}">
	<td class="invoice" align="center"></td>
  </c:if>
  <td class="invoice" align="right"></td>
  <c:if test="${order.hasCustomShipping}">
	<td class="invoice" align="center"></td>
  </c:if>
  <c:if test="${order.lineItemPromos != null and fn:length(order.lineItemPromos) gt 0}">
         <td class="discount" align="right"></td>
  </c:if>
  <td class="invoice" align="right"><fmt:formatNumber value="${tempLineItem.tempItemTotal}" pattern="#,##0.00"/></td>
  </c:when>
  <c:otherwise>
    <td class="invoice" align="center"></td>
    <c:if test="${gSiteConfig['gINVENTORY'] && order.hasLowInventoryMessage}">
	  <td class="invoice" align="center"></td>
    </c:if>
    <td class="invoice" align="center"></td>
    <c:if test="${order.hasCustomShipping}">
      <td class="invoice" align="center"></td>
    </c:if>
    <c:if test="${order.lineItemPromos != null and fn:length(order.lineItemPromos) gt 0}">
      <td class="discount" align="right"></td>
    </c:if>
    <td class="invoice" align="center"></td>
  </c:otherwise>
  </c:choose>
</tr>