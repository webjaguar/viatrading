<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="/WEB-INF/tlds/spring.tld" prefix="spring" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ taglib uri="/WEB-INF/tlds/custom.tld" prefix="wj" %>

<tiles:insertDefinition name="${_template}" flush="true">
  <tiles:putAttribute name="leftbar" value="/WEB-INF/jsp/frontend/layout/template${_leftBar}/leftbar.jsp" />
  <tiles:putAttribute name="content" type="string">
  
<script language="JavaScript" type="text/JavaScript">
<!--
function assignValue(ele, value) {
	$(ele).value = value;
}
function saveContact(){
		try {
			if( !validateContactInfo() ) {
				return;
			}
		}catch(err){}
		var sku = $('sku').value;
		var name = $('name').value;
		var firstName = $('firstName').value;
		var lastName = $('lastName').value;
		var company = $('company').value;
		var address = $('address').value;
		var city = $('city').value;
		var state = $('state').value;
		var zipCode = $('zipCode').value;
		var country = $('country').value;
		var email = $('email').value;
		var phone = $('phone').value;
		var note = $('note').value;
		var request = new Request({
		   url: "${_contextpath}/insert-ajax-contact.jhtm?firstName="+firstName+"&lastName="+lastName+"&company="+company+"&address="+address+"&city="+city+"&state="+state+"&zipCode="+zipCode+"&country="+country+"&email="+email+"&phone="+phone+"&note="+note+"&sku="+sku+"&name="+name+"&leadSource=quote",
	       method: 'post',
	       onFailure: function(response){
	           saveContactBox.close();
	           $('mbQuote').style.display = 'none';
	           $('successMessage').set('html', '<div style="color : RED; text-align : LEFT;">We are experincing problem in saving your quote. <br/> Please try again later.</div>');
	       },
	       onSuccess: function(response) {
	            saveContactBox.close();
	            $('mbQuote').style.display = 'none';
	            $('successMessage').set('html', response);
	       }
		}).send();
}
function updateRegularPrice() {
	var optionPrice = 0.0;
	$$('select.optionSelect').each(function(select){
		$$('input.optionNameValues').each(function(input){
			if('list_'+select.name == input.name){
				var array = input.value.split(',');
				for(var i=0; i< array.length; i++) {
					if(select.value == i) {
						optionPrice = optionPrice + parseFloat(array[i]);
					}
				}	
			}
			$('sale_price').set('html', (parseFloat($('hidden_sale_price').value)+optionPrice).toFixed(2));
		});
	});
}
//-->
</script>

<c:if test="${model.mootools and param.hideMootools == null}">
<script src="${_contextpath}/javascript/mootools-1.2.5.1-more.js" type="text/javascript"></script>
</c:if>

<c:if test="${model.message != null}">
  <div class="message"><fmt:message key="${model.message}" /></div>
</c:if>

<c:if test="${model.product != null}">

<c:if test="${fn:trim(model.productLayout.headerHtml) != ''}">
  <c:set value="${model.productLayout.headerHtml}" var="headerHtml"/>
  <%@ include file="/WEB-INF/jsp/frontend/common/headerDynamicElement.jsp" %>
  <div><c:out value="${headerHtml}" escapeXml="false"/></div>
</c:if>

<c:set value="${model.product}" var="product"/>
<script language="JavaScript" type="text/JavaScript">
<!--
<c:if test="${siteConfig['DETAILS_IMAGE_WIDTH'].value != '' }" >
function zoomIn() {
	window.open('productImage.jhtm?id=${product.id}&imgUrl=' + document.getElementById("_image").src,'name','width=800,height=600,resizable=yes,scrollbars=1');
}
</c:if>
//-->
</script>

<c:set var="details_images_style" value="" />
<c:set var="details_desc_style" value="" />

<%-- bread crumbs --%>
<c:import url="/WEB-INF/jsp/frontend/layout/${_template}/breadcrumbs.jsp" />
<div class="details_main_description">
<div class="details_product_name"><c:out value="${product.name}" escapeXml="false" /></div>
<div class="productLayout4"><h1><c:out value="${product.shortDesc}" escapeXml="false" /></h1></div>
<c:if test="${siteConfig['DETAILS_SHORT_DESC'].value == 'true' and product.shortDesc != ''}">
</c:if>
</div>
<c:if test="${siteConfig['SHOW_SKU'].value == 'true' and product.sku != null}">
	<div class="details_sku"><c:out value="${product.sku}" /></div>  
</c:if>
<table border="0" class="details4" cellspacing="0" cellpadding="0">
<tr valign="top">
 <td>
 <c:set var="productImage">
<c:choose>
 <c:when test="${fn:contains(siteConfig['PRODUCT_IMAGE_LAYOUTS'].value,'s2') and 
 		(product.imageLayout == 's2' or (empty product.imageLayout and siteConfig['PRODUCT_IMAGE_LAYOUT'].value == 's2'))}">
    <link rel="stylesheet" href="${_contextpath}/assets/SmoothGallery/slideshow.css" type="text/css" media="screen" />
	<script type="text/javascript" src="${_contextpath}/javascript/slideshow.js"></script>
	<script type="text/javascript">		
	//<![CDATA[
	  window.addEvent('domready', function(){
	    var data = [<c:forEach items="${product.images}" var="image" varStatus="status">'<c:if test="${!image.absolute}">${_contextpath}/assets/Image/Product/detailsbig/</c:if><c:out value="${image.imageUrl}"/>'<c:if test="${! status.last}" >,</c:if></c:forEach> ];
	    var myShow = new Slideshow('show', data, { controller: true, height: 300, hu: '', thumbnails: true, width: 400 });
		});
	//]]>
	</script>
	
	<div id="show" class="slideshow"> </div>
    <div id="slideshow_margin"></div>
 </c:when>
 <c:when test="${fn:contains(siteConfig['PRODUCT_IMAGE_LAYOUTS'].value,'mb') and 
 		(param.multibox == null or param.multibox != 'off') and
 		(product.imageLayout == 'mb' or (empty product.imageLayout and siteConfig['PRODUCT_IMAGE_LAYOUT'].value == 'mb'))}">
 <link rel="stylesheet" href="${_contextpath}/assets/SmoothGallery/multibox.css" type="text/css" media="screen" />
 <c:set value="${true}" var="multibox"/>
 <script src="${_contextpath}/javascript/overlay.js" type="text/javascript" ></script>
 <script src="${_contextpath}/javascript/multibox.js" type="text/javascript" ></script>
 <script type="text/javascript">
	window.addEvent('domready', function(){
		var box = new multiBox('mbxwz', {overlay: new overlay()});
	});
 </script>
 	<div class="details_image_box" id="details_image_boxId" style="${details_images_style}
	 <c:if test="${siteConfig['DETAILS_IMAGE_WIDTH'].value != '' }">width: <c:out value="${siteConfig['DETAILS_IMAGE_WIDTH'].value}"/>px;</c:if>">
	 
	<c:forEach items="${model.product.images}" var="image" varStatus="status">
	  <c:if test="${status.first}">
	    <a href="<c:if test="${!image.absolute}">${_contextpath}/assets/Image/Product/detailsbig/</c:if><c:out value="${image.imageUrl}"/>" id="mb0" class="mbxwz" title=""><img src="<c:if test="${!image.absolute}">${_contextpath}/assets/Image/Product/detailsbig/</c:if><c:out value="${image.imageUrl}"/>" border="0" style="<c:if test="${siteConfig['DETAILS_IMAGE_WIDTH'].value != '' }">width: <c:out value="${siteConfig['DETAILS_IMAGE_WIDTH'].value}"/>px</c:if>" alt="<c:out value="${product.name}"/><c:if test="${product.shortDesc != ''}"> - <c:out value="${product.shortDesc}"/></c:if>"/></a>
	  </c:if>
	</c:forEach>
    <div style="clear: both;" >
    <c:forEach items="${model.product.images}" var="image" varStatus="status">
	  <c:if test="${status.count > 1}">
	      <a href="<c:if test="${!image.absolute}">${_contextpath}/assets/Image/Product/detailsbig/</c:if><c:out value="${image.imageUrl}"/>" id="mb${status.count - 1}" class="mbxwz" title=""><img src="<c:if test="${!image.absolute}">${_contextpath}/assets/Image/Product/detailsbig/</c:if><c:out value="${image.imageUrl}"/>" border="0" class="details_thumbnail" alt="<c:out value="${product.name}"/><c:if test="${product.shortDesc != ''}"> - <c:out value="${product.shortDesc}"/></c:if>"/></a>
	  </c:if>
	</c:forEach>
	</div>
	</div>        
 </c:when>
 <c:when test="${fn:contains(siteConfig['PRODUCT_IMAGE_LAYOUTS'].value,'qb') and 
 		(product.imageLayout == 'qb' or (empty product.imageLayout and siteConfig['PRODUCT_IMAGE_LAYOUT'].value == 'qb'))}">
 <link rel="stylesheet" href="${_contextpath}/assets/SmoothGallery/QuickBox/quickbox.css" type="text/css" media="screen" />
 <c:set value="${true}" var="quickbox"/>
 <script src="${_contextpath}/javascript/QuickBox.js" type="text/javascript" ></script>
 <script type="text/javascript">
	window.addEvent('domready', function(){
		new QuickBox();
	});
 </script>
 	<div class="details_image_box" id="details_image_boxId" style="${details_images_style}
	 <c:if test="${siteConfig['DETAILS_IMAGE_WIDTH'].value != '' }">width: <c:out value="${siteConfig['DETAILS_IMAGE_WIDTH'].value}"/>px;</c:if>">
	 
	<c:forEach items="${model.product.images}" var="image" varStatus="status">
	  <c:if test="${status.first}">
	    <a href="<c:if test="${!image.absolute}">${_contextpath}/assets/Image/Product/detailsbig/</c:if><c:out value="${image.imageUrl}"/>" id="" class="" title="" rel="quickbox"><img src="<c:if test="${!image.absolute}">${_contextpath}/assets/Image/Product/detailsbig/</c:if><c:out value="${image.imageUrl}"/>" border="0" style="<c:if test="${siteConfig['DETAILS_IMAGE_WIDTH'].value != '' }">width: <c:out value="${siteConfig['DETAILS_IMAGE_WIDTH'].value}"/>px</c:if>" alt="<c:out value="${product.name}"/><c:if test="${product.shortDesc != ''}"> - <c:out value="${product.shortDesc}"/></c:if>"/></a>
	  </c:if>
	</c:forEach>
    <div style="clear: both;" >
    <c:forEach items="${model.product.images}" var="image" varStatus="status">
	  <c:if test="${status.count > 1}">
	      <a href="<c:if test="${!image.absolute}">${_contextpath}/assets/Image/Product/detailsbig/</c:if><c:out value="${image.imageUrl}"/>" id="qb${status.count - 1}" class="" title="" rel="quickbox"><img src="<c:if test="${!image.absolute}">${_contextpath}/assets/Image/Product/detailsbig/</c:if><c:out value="${image.imageUrl}"/>" border="0" class="details_thumbnail" alt="<c:out value="${product.name}"/><c:if test="${product.shortDesc != ''}"> - <c:out value="${product.shortDesc}"/></c:if>"/></a>
	  </c:if>
	</c:forEach>
	</div>
	</div>        
 </c:when>
 <c:otherwise>
    <div class="details_image_box" id="details_image_boxId" style="${details_images_style}
	 <c:if test="${siteConfig['DETAILS_IMAGE_WIDTH'].value != '' }">width: <c:out value="${siteConfig['DETAILS_IMAGE_WIDTH'].value}"/>px;</c:if>">
	 
	<c:forEach items="${model.product.images}" var="image" varStatus="status">
	<c:if test="${status.first}">
		 <img src="<c:if test="${!image.absolute}">${_contextpath}/assets/Image/Product/detailsbig/</c:if><c:out value="${image.imageUrl}"/>" border="0" id="_image" name="_image" class="details_image"
		 	style="<c:if test="${siteConfig['DETAILS_IMAGE_WIDTH'].value != '' }">width: <c:out value="${siteConfig['DETAILS_IMAGE_WIDTH'].value}"/>px</c:if>" alt="<c:out value="${product.name}"/><c:if test="${product.shortDesc != ''}"> - <c:out value="${product.shortDesc}"/></c:if>">
	<c:if test="${siteConfig['DETAILS_IMAGE_WIDTH'].value != '' }" >
	<div align="right">
	<a onclick="zoomIn()" title="Zoom in"><img src="${_contextpath}/assets/Image/Layout/zoom-icon.gif" alt="Zoom In" width="16" height="16" /></a>
	</div>
	</c:if>
	</c:if>
	
	<c:if test="${status.last and (status.count != 1)}">
	<br>
	<c:forEach items="${model.product.images}" var="thumb">
	<a href="#" class="details_thumbnail_anchor" onClick="_image.src='<c:if test="${!thumb.absolute}">${_contextpath}/assets/Image/Product/detailsbig/</c:if><c:out value="${thumb.imageUrl}"/>';return false;">
		 <img src="<c:if test="${!thumb.absolute}">${_contextpath}/assets/Image/Product/detailsbig/</c:if><c:out value="${thumb.imageUrl}"/>" class="details_thumbnail" alt="<c:out value="${product.name}"/><c:if test="${product.shortDesc != ''}"> - <c:out value="${product.shortDesc}"/></c:if>">
	</a>
	</c:forEach>
	</c:if>
	</c:forEach>
	</div>
	<c:if test="${fn:contains(siteConfig['PRODUCT_IMAGE_LAYOUTS'].value,'ss') and 
			(product.imageLayout == 'ss'  or (empty product.imageLayout and siteConfig['PRODUCT_IMAGE_LAYOUT'].value == 'ss'))}">
	<object classid="clsid:D27CDB6E-AE6D-11cf-96B8-444553540000" codebase="http://download.macromedia.com/pub/shockwave/cabs/flash/swflash.cab#version=6,0,29,0" width="750" height="475">
	  <param name="movie" value="${_contextpath}/assets/Flash/slideshow.swf?file=thumbnailer1.jhtm?id=${product.id}">
	  <param name="quality" value="high">
	  <embed src="${_contextpath}/assets/Flash/slideshow.swf?file=thumbnailer1.jhtm?id=${product.id}" quality="high" pluginspage="http://www.macromedia.com/go/getflashplayer" type="application/x-shockwave-flash" width="750" height="475"></embed>
	</object>
	</c:if>
 </c:otherwise>
</c:choose>
</c:set>
<div id="productInfoId" class="productInfo">
<div class="productImage">
<c:out value="${productImage}" escapeXml="false" />
</div>
<div class="details_desc">
<div class="details_long_desc"><c:out value="${product.longDesc}" escapeXml="false" /></div>
<div class="addToCartWrapper" id="addToCartWrapper">
<form action="${_contextpath}/addToCart.jhtm" name="this_form" method="post" onsubmit="return checkForm()"> 
<c:if test="${ (gSiteConfig['gSHOPPING_CART'] and (!empty product.price or product.priceByCustomer)) and (!product.loginRequire or userSession != null)}">
  <div id="details_prices_id" class="details_prices">
    <%@ include file="/WEB-INF/jsp/frontend/common/price4.jsp" %>
  </div>	
</c:if>
<div class="details_options">
    <%@ include file="/WEB-INF/jsp/frontend/common/productOptions4.jsp" %>
</div>
<c:if test="${ (gSiteConfig['gSHOPPING_CART'] and (!empty product.price or product.priceByCustomer)) and (!product.loginRequire or userSession != null)}">
      <input type="hidden" name="product.id" value="${product.id}">
      <input type="hidden" name="quantity_${product.id}" id="quantity_${product.id}" value="1">
      <div class="details_addButton">
      <c:set var="zeroPrice" value="false"/>
	  <c:forEach items="${product.price}" var="price">
	   <c:if test="${price.amt == 0}">
     	<c:set var="zeroPrice" value="true"/>
	   </c:if>
 	  </c:forEach>
      <c:choose>
        <c:when test="${gSiteConfig['gINVENTORY'] and product.inventory != null and !product.negInventory and product.inventory <= 0}" >
	     <c:out value="${siteConfig['OUT_OF_STOCK_TITLE'].value}" escapeXml="false"/> 
	    </c:when>
	    <c:when test="${zeroPrice and siteConfig['ZERO_PRICE_HTML'].value != ''}">
	     <c:out value="${siteConfig['ZERO_PRICE_HTML'].value}" escapeXml="false"/>
	    </c:when>
	    <c:otherwise>
	      <c:choose>
	        <c:when test="${product.htmlAddToCart != null and product.htmlAddToCart != ''}">
	          <c:out value="${product.htmlAddToCart}" escapeXml="false"/>
	        </c:when>
	        <c:otherwise>
	          <input type="image" border="0" name="_addtocart" class="_addtocart" src="${_contextpath}/assets/Image/Layout/button_addtocart${_lang}.gif" />
	        </c:otherwise>
	      </c:choose>
	      <c:if test="${gSiteConfig['gINVENTORY'] and product.inventory != null and product.inventory < product.lowInventory}">
	         <c:out value="${siteConfig['LOW_STOCK_TITLE'].value}" escapeXml="false"/> 
	      </c:if>  
		</c:otherwise>
      </c:choose>
      </div> 
</c:if>
</form>
</div>
<c:if test="${gSiteConfig['gINVENTORY'] and product.inventory != null and !empty siteConfig['INVENTORY_ON_HAND'].value}" >
  <div class="inventory_onhand"><c:out value="${siteConfig['INVENTORY_ON_HAND'].value}" escapeXml="false"/>&nbsp;<span><c:out value="${product.inventory}" /></span></div>
</c:if>
<c:if test="${(gSiteConfig['gPRODUCT_REVIEW'] or siteConfig['PRODUCT_RATE'].value == 'true') and product.enableRate}">
<div class="prod_review">
  <c:choose>
	<c:when test="${gSiteConfig['gPRODUCT_REVIEW'] and product.enableRate and siteConfig['PRODUCT_REVIEW_THIRDPARTY'].value == 'false'}">
	<%@ include file="/WEB-INF/jsp/frontend/common/quickProductReview4.jsp" %>
	</c:when>
	<c:when test="${siteConfig['PRODUCT_RATE'].value == 'true' and product.enableRate and siteConfig['PRODUCT_REVIEW_THIRDPARTY'].value == 'false'}">
	<%@ include file="/WEB-INF/jsp/frontend/common/productRate.jsp" %>
	</c:when>
	<c:when test="${(gSiteConfig['gPRODUCT_REVIEW'] or siteConfig['PRODUCT_RATE'].value == 'true' ) and product.enableRate and siteConfig['PRODUCT_REVIEW_THIRDPARTY'].value == 'true'}">
	<%--Add FreeCode for detailview review --%>
	</c:when>
  </c:choose>
</div>
</c:if>

<c:if test="${(gSiteConfig['gCOMPARISON'] && product.compare) || gSiteConfig['gCOMPARISON']}">
<div id="addToList_and_compare" class="addToList_and_compare">
<c:if test="${gSiteConfig['gMYLIST'] && product.addToList}">
   <form action="${_contextpath}/addToList.jhtm">
     <input type="hidden" name="product.id" value="${product.id}"> 
	 <div id="details_addToList" class="details_addToList">
	   <input type="image" border="0" src="${_contextpath}/assets/Image/Layout/button_addtolist${_lang}.gif">
	 </div>
   </form>
</c:if>
	
<c:if test="${gSiteConfig['gCOMPARISON'] && product.compare}">
  <form action="${_contextpath}/comparison.jhtm" method="get">
	<%-- 
	<input type="hidden" name="forwardAction" value="<c:out value='${model.url}'/>">
	--%>
	<input type="hidden" name="product.id" value="${product.id}">
	<div id="details_compare" class="details_compare">
	<input type="image" border="0" src="${_contextpath}/assets/Image/Layout/button_compare${_lang}.gif">
	</div>
  </form>
</c:if>
</div>
</c:if>
 	
<c:forEach items="${product.productFields}" var="productField" varStatus="row">
  <c:if test="${row.first and siteConfig['PRODUCT_FIELDS_TITLE'].value != ''}">
    <div class="details_fields_wrapper">
	<div class="details_fields_title"><c:out value="${siteConfig['PRODUCT_FIELDS_TITLE'].value}"/></div>
  </c:if>
  <div class="details_fields">
	<div class="details_field_name_row${row.index % 2}"><c:out value="${productField.name}" escapeXml="false" /> </div>
  	<c:choose>
	<c:when test="${productField.formatType != null and productField.formatType != '' and productField.fieldType == 'number' }">
	<div class="details_field_value_row${row.index % 2}"><fmt:formatNumber value="${wj:trimAndSplit(productField.value, '|' , false, ',', true, true)}" pattern="${productField.formatType }" /></div>
	</c:when>
	<c:otherwise>
	<div class="details_field_value_row${row.index % 2}"><c:out value="${wj:trimAndSplit(productField.value, '|' , false, ',', true, true)}" escapeXml="false"/></div>
	</c:otherwise>
	</c:choose>	
  </div>
  <c:if test="${row.last and siteConfig['PRODUCT_FIELDS_TITLE'].value != ''}">
    </div>
  </c:if>
</c:forEach>

<c:if test="${gSiteConfig['gMAIL_IN_REBATES'] > 0 and product.mailInRebate.htmlCode != null}">
  <div class="details_rebate_htmlCode"><c:out value="${product.mailInRebate.htmlCode}" escapeXml="false"/></div>
</c:if>

</div><!-- details_desc close -->

<div class="addToQuote">
	<c:if test="${siteConfig['PRODUCT_QUOTE'].value == 'true' and product.quote}">
		<link rel="stylesheet" href="${_contextpath}/assets/SmoothGallery/multiboxASI.css" type="text/css" media="screen" />
		<c:if test="${!multibox}">
		 <script src="${_contextpath}/javascript/overlay.js" type="text/javascript" ></script>
		 <script src="${_contextpath}/javascript/multibox.js" type="text/javascript" ></script>
		</c:if>
		<script type="text/javascript">
		window.addEvent('domready', function(){			
			saveContactBox = new multiBox('mbQuote', {showControls : false, useOverlay: false, showNumbers: false });
		});
		</script>

	  <div class="addToQuoteWrapper" id="addToQuoteWrapperId${product.id}">
	  <a href="#${product.id}" rel="type:element" id="mbQuote" class="mbQuote">
	  	<input type="image" border="0" name="_addtocart" class="_addtocart" src="${_contextpath}/assets/Image/Layout/button_addtoquote${_lang}.gif" />
	  </a> 
	  </div>
	  
	  <div id="${product.id}" class="miniWindowWrapper mbPriceQuote" style="height:700px">
		<form autocomplete="off" onsubmit="saveContact(); return false;">
		<input type="hidden" value="${product.sku}" id="sku"/>
		<input type="hidden" value="${product.name}" id="name"/>
		
	  	<div class="header"><fmt:message key="provideContactInfo"/></div>
		<fieldset class="top">
			<label class="AStitle"><fmt:message key="firstName"/></label>
			<input name="firstName" id="firstName" type="text" size="15" onkeyup="assignValue('firstName', this.value);"/>
		</fieldset>
		<fieldset class="bottom">
			<label class="AStitle"><fmt:message key="lastName"/></label>
			<input name="lastName" id="lastName" type="text" size="15" onkeyup="assignValue('lastName', this.value);"/>
		</fieldset>
		<fieldset class="bottom">
			<label class="AStitle"><fmt:message key="company"/></label>
			<input name="company" id="company" type="text" size="15" onkeyup="assignValue('company', this.value);"/>
		</fieldset>
		<fieldset class="bottom">
			<label class="AStitle"><fmt:message key="address"/></label>
			<input name="address" id="address" type="text" size="15" onkeyup="assignValue('address', this.value);"/>
		</fieldset>
		<fieldset class="bottom">
			<label class="AStitle"><fmt:message key="city"/></label>
			<input name="city" id="city" type="text" size="15" onkeyup="assignValue('city', this.value);"/>
		</fieldset>
		<fieldset class="bottom">
			<label class="AStitle"><fmt:message key="state"/></label>
			<input name="state" id="state" type="text" size="15" onkeyup="assignValue('state', this.value);"/>
		</fieldset>
		<fieldset class="bottom">
			<label class="AStitle"><fmt:message key="zipCode"/></label>
			<input name="zipCode" id="zipCode" type="text" size="15" onkeyup="assignValue('zipCode', this.value);"/>
		</fieldset>
		<fieldset class="bottom">
			<label class="AStitle"><fmt:message key="country"/></label>
			<input name="country" id="country" type="text" size="15" onkeyup="assignValue('country', this.value);"/>
		</fieldset>
		<fieldset class="bottom">
			<label class="AStitle"><fmt:message key="email"/></label>
			<input name="email" id="email" type="text" size="15" onkeyup="assignValue('email', this.value);"/>
		</fieldset>
		<fieldset class="bottom">
			<label class="AStitle"><fmt:message key="phone"/></label>
			<input name="phone" id="phone" type="text" size="15" onkeyup="assignValue('phone', this.value);"/>
		</fieldset>
		<fieldset class="bottom">
			<label class="AStitle"><fmt:message key="message"/></label>
			<textarea name="note" id="note" onkeyup="assignValue('note', this.value);"></textarea>
		</fieldset>
		<fieldset>
			<div id="button">
				<input type="submit" value="Send"/>
			</div>
		</fieldset>
		</form>
	  </div>
	  <div id="successMessage"></div>	
	</c:if>
</div>

<div class="productImage">
<c:out value="${productImage}" escapeXml="false" />
</div>
</div><!-- productInfo close -->
<!-- also consider start -->
<c:if test="${model.alsoConsiderMap[product.id] != null}">
   <c:set var="product" value="${model.alsoConsiderMap[product.id]}" />
   <%@ include file="/WEB-INF/jsp/frontend/common/alsoconsider.jsp" %>
   <c:set value="${model.product}" var="product"/>
</c:if>
<!-- also consider end -->

<!-- recommendedList start-->
<c:if test="${model.recommendedList != null}">
<div class="details_recommendedList" id="details_recommendedList">
<div class="details_recommendedListContainer" style="float:left;width:100%">
<c:choose>
 <c:when test="${!empty product.recommendedListTitle }">
  <div class="recommended_list"><c:out value="${product.recommendedListTitle}"/></div>
 </c:when>
 <c:otherwise>
  <c:if test="${siteConfig['RECOMMENDED_LIST_TITLE'].value != ''}">
   <div class="recommended_list"><c:out value="${siteConfig['RECOMMENDED_LIST_TITLE'].value}"/></div>
  </c:if>
 </c:otherwise>
</c:choose>
<c:set var="numCul" value="${siteConfig['NUMBER_PRODUCT_PER_ROW'].value}" />
<c:forEach items="${model.recommendedList}" var="product" varStatus="status">

<c:set var="productLink">${_contextpath}/product.jhtm?id=${product.id}<c:if test="${model.product == null or siteConfig['RECOMMENDED_LIST_SHOW_BREADCRUMB'].value == 'true'}">&cid=<c:out value="${param.cid}"/></c:if></c:set>
<c:if test="${gSiteConfig['gMOD_REWRITE'] == '1' and not fn:contains(product.sku, '/')}">
  <c:set var="productLink">${_contextpath}/${siteConfig['MOD_REWRITE_PRODUCT'].value}/${product.encodedSku}/${product.encodedName}.html<c:if test="${model.product == null or siteConfig['RECOMMENDED_LIST_SHOW_BREADCRUMB'].value == 'true'}">?cid=<c:out value="${param.cid}"/></c:if></c:set>
</c:if>

<c:choose>
 <c:when test="${( model.product.recommendedListDisplay == 'quick') or (empty model.product.recommendedListDisplay and siteConfig['RECOMMENDED_LIST_DISPLAY'].value == 'quick')}">
    <%@ include file="/WEB-INF/jsp/frontend/quickmode/view1.jsp" %>
 </c:when>
 <c:when test="${( model.product.recommendedListDisplay == '2') or (empty model.product.recommendedListDisplay and siteConfig['RECOMMENDED_LIST_DISPLAY'].value == '2')}">
    <%@ include file="/WEB-INF/jsp/frontend/thumbnail/view2.jsp" %>
 </c:when>
 <c:when test="${( model.product.recommendedListDisplay == '3') or (empty model.product.recommendedListDisplay and siteConfig['RECOMMENDED_LIST_DISPLAY'].value == '3')}">
	<%@ include file="/WEB-INF/jsp/frontend/thumbnail/view3.jsp" %>
 </c:when>
 <c:when test="${( model.product.recommendedListDisplay == '4') or (empty model.product.recommendedListDisplay and siteConfig['RECOMMENDED_LIST_DISPLAY'].value == '4')}">
	<%@ include file="/WEB-INF/jsp/frontend/thumbnail/view4.jsp" %>
 </c:when>
 <c:when test="${( model.product.recommendedListDisplay == '5') or (empty model.product.recommendedListDisplay and siteConfig['RECOMMENDED_LIST_DISPLAY'].value == '5')}">
	<%@ include file="/WEB-INF/jsp/frontend/thumbnail/view5.jsp" %>
 </c:when>
 <c:when test="${( model.product.recommendedListDisplay == '6') or (empty model.product.recommendedListDisplay and siteConfig['RECOMMENDED_LIST_DISPLAY'].value == '6')}">
	<%@ include file="/WEB-INF/jsp/frontend/thumbnail/view6.jsp" %>
 </c:when>
 <c:when test="${( model.product.recommendedListDisplay == '7') or (empty model.product.recommendedListDisplay and siteConfig['RECOMMENDED_LIST_DISPLAY'].value == '7')}">
	<%@ include file="/WEB-INF/jsp/frontend/thumbnail/view7.jsp" %>
 </c:when>
 <c:when test="${( model.product.recommendedListDisplay == '8') or (empty model.product.recommendedListDisplay and siteConfig['RECOMMENDED_LIST_DISPLAY'].value == '8')}">
	<%@ include file="/WEB-INF/jsp/frontend/thumbnail/view8.jsp" %>
 </c:when>
 <c:when test="${( model.product.recommendedListDisplay == '9') or (empty model.product.recommendedListDisplay and siteConfig['RECOMMENDED_LIST_DISPLAY'].value == '9')}">
	<%@ include file="/WEB-INF/jsp/frontend/thumbnail/view9.jsp" %>
 </c:when>
 <c:when test="${( model.product.recommendedListDisplay == '10') or (empty model.product.recommendedListDisplay and siteConfig['RECOMMENDED_LIST_DISPLAY'].value == '10')}">
    <%@ include file="/WEB-INF/jsp/frontend/thumbnail/view10.jsp" %>
 </c:when>
 <c:when test="${( model.product.recommendedListDisplay == '11') or (empty model.product.recommendedListDisplay and siteConfig['RECOMMENDED_LIST_DISPLAY'].value == '11')}">
    <%@ include file="/WEB-INF/jsp/frontend/thumbnail/view11.jsp" %>
 </c:when>
 <c:when test="${( model.product.recommendedListDisplay == '12') or (empty model.product.recommendedListDisplay and siteConfig['RECOMMENDED_LIST_DISPLAY'].value == '12')}">
    <%@ include file="/WEB-INF/jsp/frontend/thumbnail/view12.jsp" %>
 </c:when>
 <c:otherwise>
	<%@ include file="/WEB-INF/jsp/frontend/thumbnail/view1.jsp" %>
 </c:otherwise>
</c:choose>
</c:forEach>
</div>
</div>
</c:if>
<!-- recommendedList end -->

<!-- tab start -->
<div class="details_tab">
<c:if test="${gSiteConfig['gTAB'] > 0 and !empty model.tabList}" >
<script src="${_contextpath}/javascript/SimpleTabs1.2.js" type="text/javascript"></script>
<script type="text/javascript">
	window.addEvent('domready', function(){
		var tabs1 = new SimpleTabs($('tab-block-1'), { selector: 'h4'});
	});
</script>
<!-- tabs -->
<div id="tab-block-1">
<c:forEach items="${model.tabList}" var="tab" varStatus="status">
<h4 id="${tab.tabNumber}" title="<c:out value="${tab.tabName}" escapeXml="false"/>"><c:out value="${tab.tabName}" escapeXml="false" /></h4>
	<div>
	<!-- start tab -->
	<c:choose>
	<c:when test="${tab.tabNumber == 3}">
	<c:if test="${tab.tabContent != null and !empty tab.tabContent}"><c:catch var="exception"><c:import url="http://www.number1direct.com/${tab.tabContent}" /></c:catch></c:if>
	<script type="text/javascript">POWERREVIEWS.display.engine(document, {pr_page_id : '<c:out value="${model.product.sku}"/>',pr_write_review : '/category.jhtm?cid=2277&pr_page_id=<c:out value="${model.product.sku}"/>'});</script>
	</c:when>
	<c:when test="${tab.tabNumber == 6}">
	<p><strong>We try to answer your questions as soon as possible but we may not be able to respond for up to 3 business days. If you need immediate assistance, your best option is to give us a call at 1-888-768-1710.</strong></p>
	<c:if test="${tab.tabContent != null and !empty tab.tabContent}"><c:catch var="exception"><c:import url="http://www.number1direct.com/${tab.tabContent}" /></c:catch></c:if>
	<!--Answer Box Begin--><script type="text/javascript">POWERREVIEWS.display.productAnswers(document,{pr_page_id:'<c:out value="${model.product.sku}"/>',pr_ask_question:'/category.jhtm?cid=2277&pr_page_id=<c:out value="${model.product.sku}"/>&appName=askQuestion',pr_answer_question:'/category.jhtm?cid=2277&pr_page_id=<c:out value="${model.product.sku}"/>&appName=answerQuestion&questionId=@@@QUESTION_ID@@@'});</script><!--Answer Box End-->
	</c:when>
	<c:otherwise><c:out value="${tab.tabContent}" escapeXml="false"/></c:otherwise>
	</c:choose>
	<!-- end tab -->
	</div>
</c:forEach>
</div>
</c:if>
</div>
<!-- tab end -->

</td>
</tr>
</table>

<c:if test="${gSiteConfig['gPRODUCT_REVIEW'] and siteConfig['PRODUCT_RATE'].value != 'true' and model.product.enableRate}">
<div class="reviewList">
<%@ include file="/WEB-INF/jsp/frontend/common/productReviewList.jsp" %>
</div>
</c:if>

<c:if test="${fn:trim(model.productLayout.footerHtml) != ''}">
  <c:set value="${model.productLayout.footerHtml}" var="footerHtml"/>
  <%@ include file="/WEB-INF/jsp/frontend/common/footerDynamicElement.jsp" %>
  <div style="float:left;width:100%"><c:out value="${footerHtml}" escapeXml="false"/></div>
</c:if>
</c:if>
    
  </tiles:putAttribute>
</tiles:insertDefinition>