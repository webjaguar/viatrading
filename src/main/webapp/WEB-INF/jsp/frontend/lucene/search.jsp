<%@ page import="java.util.*" %>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>

<tiles:insertDefinition name="${_template}" flush="true">
  <tiles:putAttribute name="content" type="string">
<script type="text/javascript">
<!--

window.addEvent('domready', function(){
	$('searchTitle').addEvent('click',function() {
		$('content').setStyle('margin', '0');
		$('topNavigation').hide();
	  	$('leftSidebarWrapper').hide();
	  	$('leftSidebarWrapper').hide();
	  	$('pageHeaderWrapper').hide();
	  	$('pageFooter').hide();
	  	$('searchForm').hide();
	  	$('searchTitle').destroy();
	  	$('quoteButton').destroy();
	  	$$('.quickmode_v1_addtocart_top').each(function(el){
    		el.destroy();
    	});
	  	$$('.quickmode_v1_addtocart_bottom').each(function(el){
    		el.destroy();
    	});
	  	$$('.quickmodeQuote').each(function(el){
    		el.destroy();
    	});
	  	$$('.image').each(function(el){
    		el.destroy();
    	});
    	$$('.desc').each(function(el){
    		el.destroy();
    	});
    	$$('.price').each(function(el){
    		el.destroy();
    	});
    	$$('.qty').each(function(el){
    		el.destroy();
    	});
    	$$('.pagenavBox').each(function(el){
    		el.destroy();
    	});
	  	$$('.formPageNavigation').each(function(el){
    		el.destroy();
    	});
	  	$$('.listingsHdr3').each(function(el){
    		el.removeProperties('colspan', 'class');
    	});
	  	alert('Select the whole page, copy and paste to your excel file.');
  	});
});

function checkNumber( aNumber )
{
	var goodChars = "0123456789";
	var i = 0;
	if ( aNumber == "" )
		return 0; //empty
	
	for ( i = 0 ; i <= aNumber.length - 1 ; i++ )
	{
		if ( goodChars.indexOf( aNumber.charAt(i) ) == -1 ) 
			return -1; //invalid number			
	}	
	return 1; //valid number
}
function checkForm()
{
	var productList = document.getElementsByName('product.id');
	var allQtyEmpty = true;
	for(var i=0; i<productList.length; i++) 
	{
	    var el = document.getElementById("quantity_" + productList[i].value);
		if ( el != null )
		{
			if ( checkNumber( el.value ) == 1  )
			{
				allQtyEmpty = false;
			}
			else if ( checkNumber( el.value ) == -1 ) 
			{
				alert("invalid Quantity");
				el.focus();
				return false;
			}			
		}	
	}
	if ( allQtyEmpty )
	{
		alert("Please Enter Quantity.");
		return false;
	}
	else
		return true;	
}
//-->
</script>
<%-- bread crumbs --%>
<c:import url="/WEB-INF/jsp/frontend/layout/${_template}/breadcrumbs.jsp" />
<form action="lsearch.jhtm" method="post" id="searchForm" onsubmit="pageTracker._trackEvent('User Actions', 'Part Number Search', 'Keywords('+document.getElementById('search_keywords').value+') Mfg('+document.getElementById('search_mfg').value+')', 1);">
<input type="hidden" name="page" value="1">
<table border="0" cellspacing="0" cellpadding="5" align="center">
  <tr>
    <td><fmt:message key="keywords" />: </td>
    <td><input type="text" name="keywords" value="<c:out value="${lProductSearch.keywords}"/>" size="50" id="search_keywords">
    	<c:if test="${not model.minCharsMet}">
    		<div style="font-weight:bold;color:#990000">
    		  <fmt:message key="f_keywordsMinCharsError">
				<fmt:param><c:out value="${siteConfig['SEARCH_KEYWORDS_MINIMUM_CHARACTERS'].value}" /></fmt:param>
			  </fmt:message>
    	</c:if>
    	<c:if test="${model.hasKeywordsLessMinChars and model.minCharsMet}">
    		<div style="font-weight:bold;color:#990000">
    		  <fmt:message key="f_keywordsMinCharsIgnore">
				<fmt:param><c:out value="${siteConfig['SEARCH_KEYWORDS_MINIMUM_CHARACTERS'].value}" /></fmt:param>
			  </fmt:message>
    	</c:if>
    	<c:if test="${model.tooManyClauses}">
    		<div style="font-weight:bold;color:#990000">
    		  <fmt:message key="tooManyClausesError"/>
    	</c:if>
    </td>
  </tr>
<%@ include file="/WEB-INF/jsp/frontend/lucene/searchCustom.jsp" %>
  <tr>
    <td colspan="2" align="right"><input type="submit" value="Search"></td>
  </tr>
</table>
</form>
<c:if test="${model.productSearchLayout.headerHtml!=''}" >
 <c:out value="${model.productSearchLayout.headerHtml}" escapeXml="false"/>    
</c:if>  
<c:if test="${model.minCharsMet && (model.tooManyClauses == null or not model.tooManyClauses)}">
<div class="searchTitle" id="searchTitle">Search Results:</div>

<c:if test="${model.count == 0}"><span id="nothing_found">Nothing found</span></c:if>
</c:if>
<c:if test="${model.count > 0}">

<c:set var="paramView" value=""/>
<c:choose>
  <c:when test="${param.view != null and !empty param.view}">
    <c:set var="paramView" value="${param.view}"/>
  </c:when>
  <c:otherwise>
    <c:set var="paramView" value="${siteConfig['SEARCH_DISPLAY_MODE'].value}"/>
  </c:otherwise>
</c:choose>

<c:set var="pageShowing">
<table border="0" cellpadding="0" cellspacing="1" width="100%" class="pagenavBox">
  <tr>
  <td class="pageShowing">
  <fmt:message key="showing">
	<fmt:param value="${model.start + 1}"/>
	<fmt:param value="${model.pageEnd}"/>
	<fmt:param value="${model.count}"/>
  </fmt:message>
  </td>
  <td class="pageNavi">
<form action="lsearch.jhtm" method="get" id="paginationFormId" class="formPageNavigation">
Page 
<c:choose>
<c:when test="${model.pageCount <= 100 and model.pageCount > 0}">
	    <select name="page" id="page" onchange="submit()">
		<c:forEach begin="1" end="${model.pageCount}" var="page">
  	        <option value="${page}" <c:if test="${page == (lProductSearch.page)}">selected</c:if>>${page}</option>
		</c:forEach>
		</select>
</c:when>
<c:otherwise>
		<input type="text" id="page" name="page" value="${lProductSearch.page}" size="5" class="textfield50" />
		<input type="submit" value="go"/>
</c:otherwise>
</c:choose>
of <c:out value="${model.products.pageCount}"/>
| 
<c:if test="${lProductSearch.page == 1}"><span class="pageNaviDead"><fmt:message key="f_previous" /></span></c:if>
<c:if test="${lProductSearch.page != 1}"><a href="lsearch.jhtm?page=${lProductSearch.page-1}<c:if test="${lProductSearch.minPrice != null }">&minPrice=${lProductSearch.minPrice}</c:if><c:if test="${lProductSearch.maxPrice != null }">&maxPrice=${lProductSearch.maxPrice}</c:if><c:if test="${model.cid != null }">&cid=${model.cid}</c:if>&view=<c:out value='${paramView}' /><c:if test="${lProductSearch.productFieldMap['madeIn'].value == 'USA'}">&madeIn=USA</c:if><c:if test="${lProductSearch.productFieldMap['eco'] == 'true'}">&eco=true</c:if><c:if test="${lProductSearch.minQty != null}">&minQty=${lProductSearch.minQty}</c:if><c:if test="${lProductSearch.supplierAccountNumber != null}">&s=${lProductSearch.supplierAccountNumber}</c:if>" class="pageNaviLink"><fmt:message key="f_previous" /></a></c:if>
| 
<c:if test="${lProductSearch.page == model.pageCount}"><span class="pageNaviDead"><fmt:message key="f_next" /></span></c:if>
<c:if test="${lProductSearch.page != model.pageCount}"><a href="lsearch.jhtm?page=${lProductSearch.page+1}<c:if test="${lProductSearch.minPrice != null }">&minPrice=${lProductSearch.minPrice}</c:if><c:if test="${lProductSearch.maxPrice != null }">&maxPrice=${lProductSearch.maxPrice}</c:if><c:if test="${model.cid != null }">&cid=${model.cid}</c:if>&view=<c:out value='${paramView}' /><c:if test="${lProductSearch.productFieldMap['madeIn'].value == 'USA'}">&madeIn=USA</c:if><c:if test="${lProductSearch.productFieldMap['eco'] == 'true'}">&eco=true</c:if><c:if test="${lProductSearch.minQty != null}">&minQty=${lProductSearch.minQty}</c:if><c:if test="${lProductSearch.supplierAccountNumber != null}">&s=${lProductSearch.supplierAccountNumber}</c:if>" class="pageNaviLink"><fmt:message key="f_next" /></a></c:if>
<select name="size" onchange="document.getElementById('page').value=1;document.getElementById('view').value=${paramView};submit()">
<c:forTokens items="${siteConfig['PRODUCT_PAGE_SIZE'].value}" delims="," var="current">
 <option value="${current}" <c:if test="${current == lProductSearch.pageSize}">selected</c:if>>${current} <fmt:message key="perPage" /></option>
</c:forTokens>
</select>

<c:if test="${lProductSearch.minPrice != null }">
  <input type="hidden" name="minPrice" value="${lProductSearch.minPrice}"/>
</c:if>
<c:if test="${lProductSearch.maxPrice != null }">
  <input type="hidden" name="maxPrice" value="${lProductSearch.maxPrice}"/>
</c:if>
<c:if test="${lProductSearch.extraFieldMap['madeIn'] == 'USA'}">
  <input type="hidden" name="madeIn" value="USA"/>
</c:if> 
<c:if test="${lProductSearch.extraFieldMap['eco'] == 'true'}">
  <input type="hidden" name="eco" value="true"/>
</c:if> 
<c:if test="${lProductSearch.minQty != null}">
  <input type="hidden" name="minQty" value="${lProductSearch.minQty}"/>
</c:if> 
<c:if test="${lProductSearch.supplierAccountNumber != null}">
  <input type="hidden" name="s" value="${lProductSearch.supplierAccountNumber}"/>
</c:if> 
<c:if test="${model.cid != null }">
  <input type="hidden" name="cid" value="${model.cid}"/>
</c:if>
<input type="hidden" name="view" id="view" value="<c:out value='${param.view}' />" />
</form>
  </td>
  </tr>
</table>

</c:set>
<c:out value="${pageShowing}" escapeXml="false" />
</c:if>

<form action="${_contextpath}/addToCart.jhtm" method="post" onsubmit="return checkForm()">

<c:forEach items="${model.results}" var="product" varStatus="status">

<c:set var="productLink">${_contextpath}/product.jhtm?id=${product.id}<c:if test="${model.product == null or siteConfig['RECOMMENDED_LIST_SHOW_BREADCRUMB'].value == 'true'}">&cid=<c:out value="${param.cid}"/></c:if></c:set>
<c:if test="${gSiteConfig['gMOD_REWRITE'] == '1' and not fn:contains(product.sku, '/')}">
  <c:set var="productLink">${_contextpath}/${siteConfig['MOD_REWRITE_PRODUCT'].value}/${product.encodedSku}/${product.encodedName}.html<c:if test="${model.product == null or siteConfig['RECOMMENDED_LIST_SHOW_BREADCRUMB'].value == 'true'}">?cid=<c:out value="${param.cid}"/></c:if></c:set>
</c:if>

<c:set var="numCul" value="${siteConfig['NUMBER_PRODUCT_PER_ROW'].value}" />
<c:choose>
 <c:when test="${( param.view == '1') or ( empty param.view and siteConfig['SEARCH_DISPLAY_MODE'].value == '1')}">
    <%@ include file="/WEB-INF/jsp/frontend/thumbnail/view1.jsp" %>
 </c:when>
 <c:when test="${( param.view == '2') or ( empty param.view and siteConfig['SEARCH_DISPLAY_MODE'].value == '2')}">
    <%@ include file="/WEB-INF/jsp/frontend/thumbnail/view2.jsp" %>
 </c:when>
 <c:when test="${( param.view == '3') or ( empty param.view and siteConfig['SEARCH_DISPLAY_MODE'].value == '3')}">
	<%@ include file="/WEB-INF/jsp/frontend/thumbnail/view3.jsp" %>
 </c:when>
 <c:when test="${( param.view == '4') or ( empty param.view and siteConfig['SEARCH_DISPLAY_MODE'].value == '4')}">
	<%@ include file="/WEB-INF/jsp/frontend/thumbnail/view4.jsp" %>
 </c:when>
 <c:when test="${( param.view == '5') or ( empty param.view and siteConfig['SEARCH_DISPLAY_MODE'].value == '5')}">
	<%@ include file="/WEB-INF/jsp/frontend/thumbnail/view5.jsp" %>
 </c:when>
 <c:when test="${( param.view == '6') or ( empty param.view and siteConfig['SEARCH_DISPLAY_MODE'].value == '6')}">
	<%@ include file="/WEB-INF/jsp/frontend/thumbnail/view6.jsp" %>
 </c:when>
 <c:when test="${( param.view == '7') or ( empty param.view and siteConfig['SEARCH_DISPLAY_MODE'].value == '7')}">
	<%@ include file="/WEB-INF/jsp/frontend/thumbnail/view7.jsp" %>
 </c:when>
 <c:when test="${( param.view == '8') or ( empty param.view and siteConfig['SEARCH_DISPLAY_MODE'].value == '8')}">
	<%@ include file="/WEB-INF/jsp/frontend/thumbnail/view8.jsp" %>
 </c:when>
 <c:when test="${( param.view == '9') or ( empty param.view and siteConfig['SEARCH_DISPLAY_MODE'].value == '9')}">
	<%@ include file="/WEB-INF/jsp/frontend/thumbnail/view9.jsp" %>
 </c:when>
 <c:when test="${( param.view == '10') or ( empty param.view and siteConfig['SEARCH_DISPLAY_MODE'].value == '10')}">
    <%@ include file="/WEB-INF/jsp/frontend/thumbnail/view10.jsp" %>
 </c:when>
 <c:when test="${( param.view == '11') or ( empty param.view and siteConfig['SEARCH_DISPLAY_MODE'].value == '11')}">
    <%@ include file="/WEB-INF/jsp/frontend/thumbnail/view11.jsp" %>
 </c:when>
 <c:when test="${( param.view == '12') or ( empty param.view and siteConfig['SEARCH_DISPLAY_MODE'].value == '12')}">
    <%@ include file="/WEB-INF/jsp/frontend/thumbnail/view12.jsp" %>
 </c:when>
 <c:when test="${ (gSiteConfig['gSITE_DOMAIN'] == 'viatrading.com' or gSiteConfig['gSITE_DOMAIN'] == 'test.com') and  (( param.view == '15') or ( empty param.view and siteConfig['SEARCH_DISPLAY_MODE'].value == '15'))}">
    <%@ include file="/WEB-INF/jsp/frontend/viatrading/thumbnail/view1.jsp" %>
 </c:when>
 <c:otherwise>
 	<%@ include file="/WEB-INF/jsp/frontend/quickmode/view1.jsp" %>
 </c:otherwise>
</c:choose>
</c:forEach>

</form>

<c:if test="${model.count > 0 && model.pageCount > 1}">
<c:out value="${pageShowing}" escapeXml="false" />
</c:if>

<c:if test="${model.productSearchLayout.footerHtml!=''}">
 <c:out value="${model.productSearchLayout.footerHtml}" escapeXml="false"/>    
</c:if> 
   
  </tiles:putAttribute>
</tiles:insertDefinition>
