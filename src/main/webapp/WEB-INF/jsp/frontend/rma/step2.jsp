<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>

<tiles:insertDefinition name="${_template}" flush="true">
  <tiles:putAttribute name="content" type="string">
  
<c:if test="${gSiteConfig['gRMA']}">   
  
<c:if test="${model.message != null}">
  <div class="message"><fmt:message key="${model.message}" /></div>
</c:if>

<form:form  commandName="rma" method="post">
<fieldset class="register_fieldset">
<legend><fmt:message key="new" /> <fmt:message key="rmaRequest" /></legend>
<table border="0" cellpadding="3">
  <tr>
    <td style="width:100px;text-align:right"><fmt:message key="order" /> #:</td>
    <td><c:out value="${rma.orderId}" />
		<form:errors path="orderId" cssClass="error"/></td>
  </tr>
  <tr>
    <td style="width:100px;text-align:right"><fmt:message key="S/N" />:</td>
    <td><form:select path="serialNum">
	  	  <form:option value="">Please select</form:option>
	  	  <c:forEach items="${rma.serialNums}" var="serialNum">
	   	  <form:option value="${serialNum}" />
	  	  </c:forEach>	    
	 	</form:select>    
		<form:errors path="serialNum" cssClass="error"/></td>
  </tr>
  <c:if test="${lastRma != null}">
  <tr>
    <td align="right" class="error"><fmt:message key="existingRequest"/>:</td>
    <td><a href="rma.jhtm?num=${lastRma.rmaNum}" class="nameLink"><c:out value="${lastRma.rmaNum}"/></a>
    	<c:out value="${lastRma.status}"/></td>
  </tr>
  <tr>
    <td align="right" class="error"><fmt:message key="reportDate" />:</td>
    <td><fmt:formatDate type="date" timeStyle="default" value="${lastRma.reportDate}"/></td>
  </tr>
  </c:if>
  <tr>
    <td align="right"><fmt:message key="action" />:</td>
    <td>
	 <form:select path="action">
	   <form:option value="">Please select</form:option>
	   <form:option value="Replacement"/>
	   <form:option value="Credit"/>
	 </form:select>
	 <form:errors path="action" cssClass="error"/>
    </td>
  </tr>
  <tr>
    <td align="right"><fmt:message key="problem" />:</td>
    <td><form:input id="serialNum" path="problem" size="50" maxlength="255" />
		<form:errors path="problem" cssClass="error"/></td>
  </tr>
    <tr>
      <td valign="top" align="right"><fmt:message key="comments" />:</td>
      <td><form:textarea rows="8" cols="50" path="comments" htmlEscape="true" /></td>       
    </tr>
</table>
</fieldset>
<div align="center">
  <input type="submit" value="<fmt:message key="prevStep" />" name="_target0">
  <input type="submit" value="<fmt:message key="nextStep" />" name="_target2">
</div>
</form:form>

</c:if>

  </tiles:putAttribute>
</tiles:insertDefinition>
