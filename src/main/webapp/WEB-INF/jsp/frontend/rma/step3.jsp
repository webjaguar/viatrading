<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>

<tiles:insertDefinition name="${_template}" flush="true">
  <tiles:putAttribute name="content" type="string">
  
<c:if test="${gSiteConfig['gRMA']}">   
  
<c:if test="${model.message != null}">
  <div class="message"><fmt:message key="${model.message}" /></div>
</c:if>

<form:form  commandName="rma" method="post">
<fieldset class="register_fieldset">
<legend><fmt:message key="pleaseVerify" /></legend>
<table border="0" cellpadding="3">
  <tr>
    <td style="width:100px;text-align:right"><fmt:message key="order" /> #:</td>
    <td><c:out value="${rma.orderId}"/></td>
  </tr>
  <tr>
    <td style="width:100px;text-align:right"><fmt:message key="S/N" />:</td>
    <td><c:out value="${rma.serialNum}"/></td>
  </tr>
  <tr>
    <td align="right"><fmt:message key="sku" />:</td>
    <td><c:out value="${rma.lineItem.product.sku}"/></td>
  </tr>
  <tr>
    <td align="right"><fmt:message key="description" />:</td>
    <td><c:out value="${rma.lineItem.product.name}"/></td>
  </tr>
  <tr>
    <td valign="top" align="right"><fmt:message key="action" />:</td>
    <td><c:out value="${rma.action}"/></td>
  </tr>
  <tr>
    <td valign="top" align="right"><fmt:message key="problem" />:</td>
    <td><c:out value="${rma.problem}"/></td>
  </tr>
  <tr>
    <td valign="top" align="right"><fmt:message key="comments" />:</td>
    <td><c:out value="${rma.comments}"/></td>
  </tr>
</table>
</fieldset>
<div align="center">
  <input type="submit" value="<fmt:message key="prevStep" />" name="_target1">
  <input type="submit" value="<fmt:message key="submit" />" name="_finish">
</div>
</form:form>

</c:if>

  </tiles:putAttribute>
</tiles:insertDefinition>
