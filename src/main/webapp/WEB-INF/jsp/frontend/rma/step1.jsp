<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>

<tiles:insertDefinition name="${_template}" flush="true">
  <tiles:putAttribute name="content" type="string">
  
<c:if test="${gSiteConfig['gRMA']}">  
  
<c:if test="${model.message != null}">
  <div class="message"><fmt:message key="${model.message}" /></div>
</c:if>

<form:form  commandName="rma" method="post">
<fieldset class="register_fieldset">
<legend><fmt:message key="new" /> <fmt:message key="rmaRequest" /></legend>
<table border="0" width="100%" cellpadding="3">
  <tr>
    <td style="text-align:center"><fmt:message key="order" /> #</td>
    <td rowspan="3" style="text-align:center"><fmt:message key="and/or" /></td>
    <td style="text-align:center"><fmt:message key="S/N" /></td>
  </tr>
  <tr>
    <td align="center"><form:input id="orderId" path="orderId" size="20" maxlength="50" /></td>
    <td align="center"><form:input id="serialNum" path="serialNum" size="20" maxlength="50" /></td>
  </tr>
  <tr>
    <td align="center"><form:errors path="orderId" cssClass="error"/></td>
    <td align="center"><form:errors path="serialNum" cssClass="error"/></td>
  </tr>
</table>
</fieldset>
<div align="center">
  <input type="submit" value="<fmt:message key="nextStep" />" name="_target1">
</div>
</form:form>

<c:if test="${serviceItems != null}">
<script language="JavaScript">
<!--
function toggleSku(el) {
  document.getElementById('serialNum').value = document.getElementById('s/n_'+el.value).value;
  if (el.value == "") {
      document.getElementById('serialNum').disabled=false;
  } else {
      document.getElementById('serialNum').disabled=true;
  }
}
if (document.getElementById('itemId').value != "") {
  toggleSku(document.getElementById('itemId'));
}
//-->
</script>
</c:if>

</c:if>

  </tiles:putAttribute>
</tiles:insertDefinition>
