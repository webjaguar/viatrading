<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>

<tiles:insertDefinition name="${_template}" flush="true">
  <tiles:putAttribute name="content" type="string">
<c:out value="${model.deliveryTimeLayout.headerHtml}" escapeXml="false"/>
<script type="text/javascript">
<!--

//-->
</script>
<c:if test="${siteConfig['DELIVERY_TIME'].value == 'true'}">
<div class="deliveryTimeForm">
<form action="${_contextpath}/deliveryTime.jhtm" method="post">
<div class="dtFormTitle">Estimated Shipping</div>
<table border="0" cellspacing="0" cellpadding="5" class="dtFormTable">
  <tr>
    <td class="title">Enter Shipping Zip Code</td>
    <td class="value"><input type="text" name="_zipcode" value="<c:out value="${model.zipcode}"/>" size="9" height="40" maxlength="6"></td>
  </tr>
  <c:if test="${!empty siteConfig['UPS_SHIPPER_NUMBER'].value}">
  <tr>
    <td>Please enter State/Province:</td>
    <td>
      <select id="state" name="_stateProvince" style="width: 250px;">
	  <option value="0">Please Select</option>
		<c:forEach items="${model.statelist}" var="state">
		  <option value="<c:out value="${state.code}"/>" <c:if test="${state.code == model.stateProvince}">Selected</c:if>>${state.name}</option>
		</c:forEach>      
      </select>
    </td>
  </tr>
  </c:if>
</table>
<div class="dtMessage"></div>
<div class="dtSubmit" id="dtSubmit"><input type="submit" value="Continue"></div>
</form>
</div>

<c:if test="${!empty model.zipcode}">

<!-- Error Message -->
<c:out value="${model.message}"></c:out>
	
<div class="deliveryTimeBox">
<div class="dtFormTitle">Estimated Shipping</div>
<table border="0" cellspacing="0" cellpadding="5" class="dtScheduleTable" id="dtScheduleTableId">
  <tr>
    <td class="title">Order placed today by 12:00 PM PST will leave our Headquarters on:</td>
    <td class="value"><fmt:formatDate type="date" timeStyle="default" value="${model.shippingTime}"/></td>
  </tr>
</table>

<div class="dtFormTitle">Estimated Delivery Schedule</div>
<table border="0" cellspacing="0" cellpadding="5" class="dtScheduleTable">
  <tr>
    <th class="title">Shipping Method</th>
    <th class="value">Delivery Time</th>
  </tr>
  <c:forEach items="${model.ratesList}" var="rate" varStatus="status">
  <tr>
	<td class="title"><c:out value="${rate.title}" /></td>
	<td class="value">
	  <c:choose>
	    <c:when test="${rate.code == '03' and empty rate.deliveryDate}"><a onclick="window.open(this.href,'','width=740,height=550'); return false;" href="category.jhtm?cid=49">See Map</a></c:when>
	    <c:when test="${rate.code == '03' and !empty rate.deliveryDate}"><fmt:formatDate type="date" timeStyle="default" value="${rate.deliveryDate}"/><span style="font-size:10px;color:#ff0000;"> ( Not Guaranteed )</span></c:when>
		<c:otherwise><fmt:formatDate type="date" timeStyle="default" value="${rate.deliveryDate}"/></c:otherwise>
	  </c:choose>
	</td>
  </tr>
  </c:forEach>
</table>

<div class="dtMessage"></div>
		
</div>
</c:if>
</c:if>
<c:out value="${model.deliveryTimeLayout.footerHtml}" escapeXml="false"/>

  </tiles:putAttribute>
</tiles:insertDefinition>
