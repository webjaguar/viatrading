<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<tiles:insertDefinition name="${_template}" flush="true">
  <tiles:putAttribute name="content" type="string">

<div class="exception">
Error!
<c:out value="${model.message}" default="No further information was provided."/>
</div>


    
  </tiles:putAttribute>
</tiles:insertDefinition>
