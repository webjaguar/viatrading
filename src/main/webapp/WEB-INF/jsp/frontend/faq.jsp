<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="/WEB-INF/tlds/custom.tld" prefix="wj" %>

<tiles:insertDefinition name="${_template}" flush="true">
  <tiles:putAttribute name="content" type="string">

<c:if test="${(model != null) && (model.faqs != null)}">
<script type="text/javascript" src="javascript/clientcide.2.2.0.js"></script>
<script type="text/javascript">
<!--
var mymultiOpen;
window.addEvent('domready', function(){			
	myMultiOpen = new MultipleOpenAccordion($('AccordionMulti'), {
		elements: $$('#AccordionMulti div.stretcher'),
		togglers: $$('#AccordionMulti div.faqstretchtoggle'),
		onActive: function(toggler, section){
			toggler.getElement('img').set('src', '${_contextpath}/assets/Image/Layout/filter_minus.jpg');
		},
		onBackground: function(toggler, section){
			toggler.getElement('img').set('src', '${_contextpath}/assets/Image/Layout/filter_plus.jpg');
		},
		openAll: false,
		firstElementsOpen: []
	});
	
});
function toggleAll() {
	myMultiOpen.toggleAll(new Fx());
}
//-->
</script>
<c:out value="${model.faqLayout.headerHtml}" escapeXml="false"/>

<form action="${_contextpath}/faq.jhtm" name="myform" method="post" >
<div class="faqSearch">
<table width="100%" border="0" cellspacing="3" cellpadding="0">
  <tr>
    <td><fmt:message key="searchWords" /></td>
  </tr>
  <tr>
    <td><input type="text" name="__keyword" value="<c:out value="${model.keyword}"/>" /></td>
    <td><input type="radio" name="comp" value="OR"   <c:if test="${faqSearch.conjunction eq 'OR' or faqSearch.conjunction eq null}">checked ="checked"</c:if> ><fmt:message key="f_atLeastOneWordMustMatch"/><br>
        <input type="radio" name="comp" value="AND" <c:if test="${faqSearch.conjunction eq 'AND'}">checked ="checked"</c:if>><fmt:message key="f_allWordsMustMatch"/><br>
        <input type="radio" name="comp" value="EXACT" <c:if test="${faqSearch.conjunction eq 'EXACT'}">checked ="checked"</c:if>><fmt:message key="f_exactWordsMustMatch"/>
    </td>
    <c:if test="${gSiteConfig['gGROUP_FAQ']}">
    <td class="faqGroup">
	<select style="width:200px;" name="__group_id" onchange="document.myform.submit();">
	<option value=""><fmt:message key="allGroups" /></option>
	 <c:forEach items="${model.groupList}" var="group" varStatus="status">
	   <option value="<c:out value='${group.id}' escapeXml="false" />" <c:if test="${model.groupId == group.id}">selected</c:if>><c:out value="${group.name}" escapeXml="false" /></option>
	 </c:forEach>
	</select>
	</td>
	</c:if>
	<td><input type="submit" name="Search" value="<fmt:message key="search" />" /></td>
  </tr>
  <tr>
    <td>&nbsp;</td>
  </tr>
</table>
</div>

<div class="toggleAllBox" style="floar:left;"><a href="#" onclick="javascript:toggleAll();"><fmt:message key="viewAllAnswers" /></a></div>
<table border="0" cellpadding="0" cellspacing="1" width="100%">
	<tr>
	    <c:if test="${model.faqs.nrOfElements > 0}">
		<td class="pageShowing">
		  <fmt:message key="showing">
			<fmt:param value="${model.faqs.firstElementOnPage + 1}"/>
			<fmt:param value="${model.faqs.lastElementOnPage + 1}"/>
			<fmt:param value="${model.faqs.nrOfElements}"/>
		  </fmt:message>
		</td>
		</c:if>	
	    <td>
		<div class="pageNavi"><fmt:message key="page" /> 
				<c:forEach begin="1" end="${model.faqs.pageCount}" var="page" varStatus="pageStatus">
			      <c:if test="${pageStatus.first}"><select name="page" onchange="submit()"></c:if>
		  	        <option value="${page}" <c:if test="${page == (model.faqs.page+1)}">selected</c:if>>${page}</option>
				  <c:if test="${pageStatus.last}"></select></c:if>
				</c:forEach>
		<fmt:message key="of" /> <c:out value="${model.faqs.pageCount}"/></div>
		</td>
   </tr>
</table>
<div id="faqBox">
<div class="AccordionMulti" id="AccordionMulti">
<c:forEach items="${model.faqs.pageList}" var="faq" varStatus="status">
  <div class="faqstretchtoggle">
	<div class="question">
		  <a href="#<c:out value='${status.index}' />">
		  <span class="question_plusminus">
		    <img border="0" style="margin-bottom: -2px;" alt="collapse" src="${_contextpath}/assets/Image/Layout/filter_minus.jpg"/>
		  </span>
		  <span class="question_name"><c:out value="${faq.question}" escapeXml="false" /></span>
		  </a>
    </div>	
  </div>
  <div class="stretcher">
	<div class="answer">
	     <span><c:out value="${faq.answer}" escapeXml="false" /><br />
	     <span class="directLink"><p>Direct link to this Question:</p><c:out value="${siteConfig['SITE_URL'].value}"/>faq.jhtm?question=<c:out value="${wj:urlEncode(faq.question)}" escapeXml="false" /></span>
	     </span> 
	</div>
  </div>
</c:forEach>
</div>
</div>
</form>

<c:out value="${model.faqLayout.footerHtml}" escapeXml="false"/>

</c:if>   
  </tiles:putAttribute>
</tiles:insertDefinition>