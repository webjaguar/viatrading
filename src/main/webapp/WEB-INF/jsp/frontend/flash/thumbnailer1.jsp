<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<c:if test="${product != null}">
<?xml version="1.0" encoding="utf-8"?>
<thumbnailer width="100" height="475" distance="0" acceleration="20" path="" imgPath="">
<c:forEach items="${product.images}" var="image">
<item>
   <thumb file="assets/Image/Product/thumb/<c:out value="${image.imageUrl}"/>"></thumb>
   <image file="assets/Image/Product/detailsbig/<c:out value="${image.imageUrl}"/>"></image>
</item>
</c:forEach>
</thumbnailer>
</c:if>