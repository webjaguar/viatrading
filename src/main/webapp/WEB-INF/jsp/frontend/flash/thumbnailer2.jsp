<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<c:if test="${subCategories != null}">
<?xml version="1.0" encoding="utf-8"?>
<thumbnailer width="466" height="133" distance="5" movement="Combined" acceleration="50" path="" imgPath="">
<c:forEach items="${subCategories}" var="category">
<item>
   <thumb file="${_contextpath}/assets/Image/Category/cat_${category.id}.gif">
   <event type="press" url="${_contextpath}/category.jhtm?cid=${category.id}" target="_self"/>
   </thumb>
   <image file="${_contextpath}/assets/Image/Category/cat_${category.id}.gif"></image>
</item>
</c:forEach>
</thumbnailer>
</c:if>