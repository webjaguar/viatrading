<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<c:if test="${featuredProducts != null}">
<?xml version="1.0" encoding="utf-8"?>
<thumbnailer width="466" height="133" distance="5" movement="Combined" acceleration="50" path="" imgPath="">
<c:forEach items="${featuredProducts}" var="product">
<item>
   <c:choose>
  	  <c:when test="${product.thumbnail.absolute}">
   <thumb file="<c:out value="${product.thumbnail.imageUrl}"/>">
   <event type="press" url="product.jhtm?id=${product.id}" target="_self"/>
   </thumb>
   <image file="<c:out value="${product.thumbnail.imageUrl}"/>"></image>
  	  </c:when>
  	  <c:otherwise>
   <thumb file="assets/Image/Product/thumb/<c:out value="${product.thumbnail.imageUrl}"/>">
   <event type="press" url="product.jhtm?id=${product.id}" target="_self"/>
   </thumb>
   <image file="assets/Image/Product/thumb/<c:out value="${product.thumbnail.imageUrl}"/>"></image>
  	  </c:otherwise>
   </c:choose>
</item>
</c:forEach>
</thumbnailer>
</c:if>

