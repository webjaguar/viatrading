<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>

<tiles:insertDefinition name="${_template}" flush="true">
  <tiles:putAttribute name="content" type="string">
  
<c:if test="${gSiteConfig['gSERVICE']}">  

<c:if test="${model.message != null}">
  <div class="message"><fmt:message key="${model.message}" /></div>
</c:if>

<form:form  commandName="serviceForm" method="post">
<fieldset class="register_fieldset">
<legend><fmt:message key="new" /> <fmt:message key="serviceRequest" /></legend>
<table border="0" cellpadding="3">
  <c:if test="${serviceItems != null}">
  <tr>
    <td style="width:100px;text-align:right"><c:out value="${siteConfig['SERVICEABLE_ITEM_TITLE'].value}"/> ID:</td>
    <td>
	 <form:select path="service.item.id" id="itemId" onchange="toggleSku(this)">
	  <form:option value="">Please select</form:option>
	  <c:forEach items="${serviceItems}" var="item">
	   <c:if test="${item.active or serviceForm.service.item.id == item.id}">
	   <form:option value="${item.id}" ><c:out value="${item.itemId}"/></form:option>
	   </c:if>
	  </c:forEach>	    
	 </form:select>
	 <input type="hidden" id="s/n_" value="">
	  <c:forEach items="${serviceItems}" var="item">
	   <input type="hidden" id="s/n_${item.id}" value="<c:out value="${item.serialNum}"/>">
	  </c:forEach>
	  <form:errors path="service.item.id" cssClass="error"/>
    </td>
  </tr>
  <c:if test="${lastService != null}">
  <tr>
    <td align="right" class="error"><fmt:message key="existingRequest"/>:</td>
    <td><a href="service.jhtm?num=${lastService.serviceNum}" class="nameLink"><c:out value="${lastService.serviceNum}"/></a>
    	<c:out value="${lastService.status}"/></td>
  </tr>
  <tr>
    <td align="right" class="error"><fmt:message key="reportDate" />:</td>
    <td><fmt:formatDate type="date" timeStyle="default" value="${lastService.reportDate}"/></td>
  </tr>
  </c:if>
  </c:if>
  <tr>
    <td style="width:100px;text-align:right"><fmt:message key="S/N" />:</td>
    <td><form:input id="serialNum" path="service.item.serialNum" size="20" maxlength="50" htmlEscape="true"/>
		<form:errors path="service.item.serialNum" cssClass="error"/></td>
  </tr>
  <tr>
    <td align="right"><fmt:message key="service"/> <fmt:message key="type" />:</td>
    <td>
	 <form:select path="service.type">
	   <form:option value="on site">on site</form:option>
	   <form:option value="in house">in house</form:option>
	   <form:option value="CPP">CPP</form:option>
	   <form:option value="Warranty">Warranty</form:option>
	 </form:select>
    </td>
  </tr>
  <tr>
    <td align="right"><fmt:message key="problem" />:</td>
    <td><form:input path="service.problem" size="50" maxlength="255" />
		<form:errors path="service.problem" cssClass="error"/></td>
  </tr>
    <tr>
      <td valign="top" align="right"><fmt:message key="comments" />:</td>
      <td><form:textarea rows="8" cols="40" path="service.comments" htmlEscape="true" /></td>       
    </tr>
  <tr>
    <td colspan="2">Inbound <fmt:message key="trackNum" /> (if sending printer to us)</td>
  </tr>
  <tr>
  	<td>&nbsp;</td>
    <td>
	 <form:select path="service.trackNumInCarrier">
	   <form:option value="">Choose a carrier</form:option>
	   <form:option value="UPS">UPS</form:option>
	   <form:option value="FedEx">FedEx</form:option>
	   <form:option value="USPS">USPS</form:option>
	   <form:option value="Trucks">Trucks</form:option>
	   <form:option value="Others">Others</form:option>
	 </form:select>
     <form:input path="service.trackNumIn" maxlength="50" htmlEscape="true"/>
     <form:errors path="service.trackNumIn" cssClass="error"/>
     <form:errors path="service.trackNumInCarrier" cssClass="error"/>
    </td>
  </tr>

  <tr>
    <td align="right"><fmt:message key="purchaseOrder" />:</td>
    <td><form:input path="service.purchaseOrder" size="50" maxlength="255" />
		<form:errors path="service.purchaseOrder" cssClass="error"/></td>
  </tr>
  <tr>
    <td align="right">3rd Party Work Order:</td>
    <td><form:input path="service.wo3rd" size="50" maxlength="255" />
		<form:errors path="service.wo3rd" cssClass="error"/></td>
  </tr>
  
</table>
</fieldset>
<div align="center">
  <input type="submit" value="<fmt:message key="nextStep" />" name="_target1">
</div>
</form:form>

<c:if test="${serviceItems != null}">
<script language="JavaScript">
<!--
function toggleSku(el) {
  document.getElementById('serialNum').value = document.getElementById('s/n_'+el.value).value;
  if (el.value == "") {
      document.getElementById('serialNum').disabled=false;
  } else {
      document.getElementById('serialNum').disabled=true;
  }
}
if (document.getElementById('itemId').value != "") {
  toggleSku(document.getElementById('itemId'));
}
//-->
</script>
</c:if>

</c:if>

  </tiles:putAttribute>
</tiles:insertDefinition>
