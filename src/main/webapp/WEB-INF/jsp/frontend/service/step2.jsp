<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib uri="/WEB-INF/tlds/custom.tld" prefix="wj" %>

<tiles:insertDefinition name="${_template}" flush="true">
  <tiles:putAttribute name="content" type="string">

<c:if test="${gSiteConfig['gSERVICE']}">

<c:if test="${model.message != null}">
  <div class="message"><fmt:message key="${model.message}" /></div>
</c:if>

<form:form  commandName="serviceForm" method="post" enctype="multipart/form-data">
<c:if test="${alternateContacts == null}">
<input type="hidden" name="service.alternateContact" value="">
</c:if>
<fieldset class="register_fieldset">
<legend><fmt:message key="pleaseVerify" /></legend>
<table border="0" cellpadding="3">
  <tr>
    <td style="width:100px;text-align:right"><c:out value="${siteConfig['SERVICEABLE_ITEM_TITLE'].value}"/> ID:</td>
    <td><c:out value="${serviceItem.itemId}"/></td>
  </tr>
  <tr>
    <td style="width:100px;text-align:right"><fmt:message key="S/N" />:</td>
    <td>
      <c:choose>
	    <c:when test="${serviceItem != null}">
          <c:out value="${serviceItem.serialNum}"/>
        </c:when>
        <c:otherwise>
    	  <c:out value="${serviceForm.service.item.serialNum}"/>
    	  <form:errors path="service.item.serialNum" cssClass="error"/>
		</c:otherwise>
	  </c:choose>
    </td>
  </tr>
  <c:if test="${serviceItem != null and serviceItem.address != null}">
  <tr>
	<td valign="top" align="right"><c:out value="${siteConfig['SERVICEABLE_ITEM_TITLE'].value}"/> <fmt:message key="location" />:</td>	
	<td>
	<c:if test="${serviceItem.address.company != ''}">
<c:out value="${serviceItem.address.company}"/><br/>
</c:if>
<c:out value="${serviceItem.address.firstName}"/> <c:out value="${serviceItem.address.lastName}"/><br/>
<c:out value="${serviceItem.address.addr1}"/><br/>
<c:if test="${serviceItem.address.addr2 != ''}">
<c:out value="${serviceItem.address.addr2}"/><br/>
</c:if>
<c:out value="${serviceItem.address.city}"/>, <c:out value="${serviceItem.address.stateProvince}"/> <c:out value="${serviceItem.address.zip}"/><br/>
<c:out value="${countries[serviceItem.address.country]}"/><br/>
<c:if test="${serviceItem.address.phone != ''}">
<fmt:message key="f_phone" />: <c:out value="${serviceItem.address.phone}"/><br/>
</c:if>
<c:if test="${serviceItem.address.fax != ''}">
<fmt:message key="f_fax" />: <c:out value="${serviceItem.address.fax}"/><br/>
</c:if>
<c:if test="${serviceItem.address.cellPhone != ''}">
<fmt:message key="f_mobilePhone" />: <c:out value="${serviceItem.address.cellPhone}"/><br/>
</c:if>
<c:if test="${serviceItem.address.email != ''}">
<fmt:message key="email" />: <c:out value="${serviceItem.address.email}"/><br/>
</c:if>
	</td>
  </tr>
  </c:if>
  <c:if test="${alternateContacts != null}">
  <tr>
    <td align="right"><fmt:message key="alternateContact" />:</td>
	<td>
      <form:select path="service.alternateContact">
       <form:option value="" label="Please Select"/>
       <form:options items="${alternateContacts}"/>
      </form:select>
      <form:errors path="service.alternateContact" cssClass="error" />     
	</td>
  </tr>
  </c:if>  
  <tr>
    <td align="right"><fmt:message key="sku" />:</td>
    <td>
    <c:if test="${serviceItem != null}">
      <c:out value="${serviceItem.sku}"/>
    </c:if>
    </td>
  </tr>
  <c:if test="${model.product != null}">
  <tr>
    <td align="right"><fmt:message key="description" />:</td>
    <td><c:out value="${model.product.name}"/></td>
  </tr>
<c:forEach items="${model.product.productFields}" var="productField" varStatus="status">
  <tr>
    <td align="right"><c:out value="${productField.name}"/>:</td>
    <c:choose>
		<c:when test="${productField.formatType != null and productField.formatType != '' and productField.fieldType == 'number' }">
			<td><fmt:formatNumber value="${wj:trimAndSplit(productField.value, '|' , false, ',', true, true)}" pattern="${productField.formatType }" /></td>
		</c:when>
		<c:otherwise>
			<td><c:out value="${wj:trimAndSplit(productField.value, '|' , false, ',', true, true)}" escapeXml="false"/></td>
		</c:otherwise>
	</c:choose>    
  </tr>
</c:forEach>
  </c:if>
  <tr>
    <td align="right"><fmt:message key="service"/> <fmt:message key="type" />:</td>
    <td><c:out value="${serviceForm.service.type}"/></td>
  </tr>
  <tr>
    <td valign="top" align="right"><fmt:message key="problem" />:</td>
    <td><c:out value="${serviceForm.service.problem}"/></td>
  </tr>
  <tr>
    <td valign="top" align="right"><fmt:message key="comments" />:</td>
    <td><c:out value="${serviceForm.service.comments}"/></td>
  </tr>
  <c:if test="${serviceForm.service.trackNumIn != ''}">
  <tr>
    <td colspan="2">Inbound <fmt:message key="trackNum" /></td>
  </tr>
  <tr>
    <td align="right"><c:out value="${serviceForm.service.trackNumInCarrier}"/>:</td>
    <td><c:out value="${serviceForm.service.trackNumIn}"/></td>
  </tr>
  </c:if>
  <c:if test="${serviceForm.service.purchaseOrder != ''}">
  <tr>
    <td align="right"><fmt:message key="purchaseOrder" />:</td>
    <td><c:out value="${serviceForm.service.purchaseOrder}"/></td>
  </tr>
  <tr>
    <td>&nbsp;</td>
    <td><input value="browse" type="file" name="file_purchaseOrder"/></td>
  </tr>
  <c:if test="${file_purchaseOrder_error}">
  <tr>
    <td>&nbsp;</td>
    <td><form:errors path="service.purchaseOrder" cssClass="error"/></td>
  </tr>
  </c:if>
  </c:if>
  <c:if test="${serviceForm.service.wo3rd != ''}">
  <tr>
    <td align="right">3rd Party Work Order:</td>
    <td><c:out value="${serviceForm.service.wo3rd}"/></td>
  </tr>
  <tr>
    <td>&nbsp;</td>
    <td><input value="browse" type="file" name="file_wo3rd"/></td>
  </tr>
  <c:if test="${file_wo3rd_error}">
  <tr>
    <td>&nbsp;</td>
    <td><form:errors path="service.wo3rd" cssClass="error"/></td>
  </tr>
  </c:if>
  </c:if>  
</table>
</fieldset>
<div align="center">
  <input type="submit" value="<fmt:message key="change" />" name="_target0">
  <input type="submit" value="<fmt:message key="submit" />" name="_finish">
</div>
</form:form>

</c:if>

  </tiles:putAttribute>
</tiles:insertDefinition>
