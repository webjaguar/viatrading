<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ page import="java.util.*" %>
<%
if (request.getSession().getAttribute("triguideManufacturers") != null) {
  int numOfCols = 3;
  double total = ((List) (request.getSession().getAttribute("triguideManufacturers"))).size();
  Integer manufacturersPerCol = new Integer((int) Math.ceil(total/numOfCols));
  request.setAttribute("manufacturersPerCol", manufacturersPerCol);
}
%>

<c:forEach items="${triguideManufacturers}" var="manufacturer" varStatus="status">
<c:if test="${status.first}">
<table border="1">
  <tr>
    <td valign="top">
</c:if>
<a href="search1.jhtm?manufacturerId=${manufacturer['id']}" class="manufacturer_link"><c:out value="${manufacturer['name']}"/></a><br>
<c:if test="${status.count % manufacturersPerCol == 0}">
    </td>
    <td valign="top">
</c:if>
<c:if test="${status.last}">
	</td>
  </tr>
</table>
</c:if>
</c:forEach>
