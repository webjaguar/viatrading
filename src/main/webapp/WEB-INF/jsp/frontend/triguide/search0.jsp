<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<tiles:insertDefinition name="${_template}" flush="true">
  <tiles:putAttribute name="content" type="string">

<c:if test="${gSiteConfig['gSEARCH'] == 'triguide'}">
<c:import url="/WEB-INF/jsp/frontend/triguide/manufacturers.jsp" />    
</c:if>
    
  </tiles:putAttribute>
</tiles:insertDefinition>
