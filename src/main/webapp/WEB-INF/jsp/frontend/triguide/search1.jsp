<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="/WEB-INF/tlds/spring.tld" prefix="spring" %>
<%@ page import="java.util.*" %>
<%
Map map = (HashMap) request.getAttribute("model");
if (map.containsKey("triguideModels")) {
  int numOfCols = 2;
  double total = ((List) (map.get("triguideModels"))).size();
  request.setAttribute("found", new Double(total));
  Integer modelLinksPerCol = new Integer((int) Math.ceil(total/numOfCols));
  request.setAttribute("modelLinksPerCol", modelLinksPerCol);
}
%>

<tiles:insertDefinition name="${_template}" flush="true">
  <tiles:putAttribute name="content" type="string">

<c:if test="${gSiteConfig['gSEARCH'] == 'triguide'}">


<div align="center">

<form action="search1.jhtm">
<select name="manufacturerId" onchange="submit()" class="search_options">
  <c:forEach items="${triguideManufacturers}" var="manufacturer">
    <option <c:if test="${param.manufacturerId == manufacturer['id']}">selected</c:if>
  	        value="<c:out value="${manufacturer['id']}"/>"><c:out value="${manufacturer['name']}"/></option>
  </c:forEach>
</select>
</form>

<table cellspacing="5">
<tr>
<td>
<form action="search1.jhtm">
<input type="hidden" name="manufacturerId" value ="${param.manufacturerId}">
<input type="hidden" name="modelType" value ="313">
<input name="modelType" type="image" value="313" src="assets/Image/button_inkjet_<c:choose><c:when test="${param.modelType == 313}">on</c:when><c:otherwise>off</c:otherwise></c:choose>.gif">
</form>
</td>
<td>
<form action="search1.jhtm">
<input type="hidden" name="manufacturerId" value ="${param.manufacturerId}">
<input type="hidden" name="modelType" value ="194">
<input name="modelType" type="image" value="194" src="assets/Image/button_toner_<c:choose><c:when test="${param.modelType == 194}">on</c:when><c:otherwise>off</c:otherwise></c:choose>.gif">
</form>
</td>
<td>
<form action="search1.jhtm">
<input type="hidden" name="manufacturerId" value ="${param.manufacturerId}">
<input type="hidden" name="modelType" value ="18185">
<input name="modelType" type="image" value="18185" src="assets/Image/button_ribbons_<c:choose><c:when test="${param.modelType == 18185}">on</c:when><c:otherwise>off</c:otherwise></c:choose>.gif">
</form>
</td>
<td>
<form action="search1.jhtm">
<input type="hidden" name="manufacturerId" value ="${param.manufacturerId}">
<input type="hidden" name="modelType" value ="15629">
<input name="modelType" type="image" value="15629" src="assets/Image/button_other_<c:choose><c:when test="${param.modelType == 15629}">on</c:when><c:otherwise>off</c:otherwise></c:choose>.gif">
</form>
</td>
<td>
<form action="search1.jhtm">
<input type="hidden" name="manufacturerId" value ="${param.manufacturerId}">
<input type="hidden" name="modelType" value ="32767">
<input name="modelType" type="image" value="32767" src="assets/Image/button_allmodels_<c:choose><c:when test="${param.modelType == null or param.modelType == 32767}">on</c:when><c:otherwise>off</c:otherwise></c:choose>.gif">
</form>
</td>
</tr>
</table>
</div>

<c:if test="${model.triguideModels != null}">
<div align="center">found: <fmt:formatNumber value="${found}" pattern="#,##0"/></div>
<table width="90%" border="1" align="center">
<tr>
<td valign="top">
<c:forEach items="${model.triguideModels}" var="model" varStatus="status">
<a href="search1.jhtm?modelId=${model['id']}&manufacturerId=${param.manufacturerId}" class="model_link"><c:out value="${model['name']}"/></a><br>
<c:if test="${status.count % modelLinksPerCol == 0}">
</td>
<td valign="top">
</c:if>
</c:forEach>
</td>
</tr>
</table>
</c:if>  
    
</c:if>
    
  </tiles:putAttribute>
</tiles:insertDefinition>
