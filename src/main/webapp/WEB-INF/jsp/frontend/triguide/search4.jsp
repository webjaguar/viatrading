<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="/WEB-INF/tlds/spring.tld" prefix="spring" %>

<tiles:insertDefinition name="${_template}" flush="true">
  <tiles:putAttribute name="content" type="string">

<c:if test="${gSiteConfig['gSEARCH'] == 'triguide'}">

<c:if test="${model.triguideModels != null}">
<table>
<tr>
<td colspan="2">
<div class="search_results_title">
MFG PART # SEARCH -&gt; <c:out value="${param.match2}"/>
</div>
</td>
</tr>
<tr>
<td colspan="2">
<table border="0" cellspacing="1" cellpadding="5" class="search_results_table">
<c:forEach items="${model.triguideModels}" var="triguideModel" varStatus="status">
<c:if test="${status.first}">
<tr><td>&nbsp;</td><td class="search_results_header">MANUFACTURER</td><td class="search_results_header">SELECT YOUR PART NUMBER</td></tr>
</c:if>
<tr class="search_results_${status.index % 2}">
<td class="search_results_index_col"><c:out value="${status.index + model.pageStart}" />.</td>
<td class="search_results_manufacturer"><c:out value="${triguideModel['manufacturer']}"/></td>
<td class="search_results_link_col">
<a href="search1.jhtm?oemSupplyId=${triguideModel['id']}" class="search_results_link"><c:out value="${triguideModel['part_number']}"/></a>
</td>
</tr>
<c:if test="${status.last}">
<c:set var="pageEnd" value="${status.index + model.pageStart}"/>
</c:if>
</c:forEach>
</table>
</td>
</tr>
<c:if test="${pageEnd != null}">
<tr>
<td class="search_results_showing"><b>Search results:</b> showing <b>${model.pageStart}</b>-<b>${pageEnd}</b> of <b>${model.count}</b> parts found</td>
<td align="right" class="search_results_page">
<form action="search1.jhtm" method="get">
<input type="hidden" name="match2" value="<c:out value="${param.match2}"/>">
		Page 
		        <select name="page" onchange="submit()">
				<c:forEach begin="1" end="${model.pageCount}" var="page">
		  	        <option value="${page}" <c:if test="${page == model.pageIndex}">selected</c:if>>${page}</option>
				</c:forEach>
				</select>
		of <b><c:out value="${model.pageCount}"/></b>
</form>
</tr>
</c:if>
<c:if test="${pageEnd == null}">
<tr><td colspan="2">Nothing found.</td></tr>
</c:if>
</table>
</c:if>  
    
</c:if>
    
  </tiles:putAttribute>
</tiles:insertDefinition>
