<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>

<tiles:insertDefinition name="${_template}" flush="true">
  <tiles:putAttribute name="content" type="string">

<c:if test="${gSiteConfig['gSEARCH'] == 'triguide'}">

<c:if test="${model.triguideSupplies != null}">

<c:if test="${param.triguide == 'true'}">
<table border="1">
  <tr>
    <td>id</td>
    <td>sku</td>
    <td>is_oem</td>
    <td>brand</td>
    <td>part_number</td>
    <td>custom0</td>
    <td>custom1</td>
    <td>custom2</td>
    <td>custom3</td>
  </tr>
<c:forEach items="${model.triguideSupplies}" var="supply">
  <tr>
    <td><c:out value="${supply['id']}"/></td>
    <td><c:out value="${supply['sku']}"/></td>
    <td><c:out value="${supply['is_oem']}"/></td>
    <td><c:out value="${supply['brand']}"/></td>
    <td><c:out value="${supply['part_number']}"/></td>
    <td><c:out value="${supply['custom0']}"/></td>
    <td><c:out value="${supply['custom1']}"/></td>
    <td><c:out value="${supply['custom2']}"/></td>
    <td><c:out value="${supply['custom3']}"/></td>
  </tr>
</c:forEach>
</table>
</c:if>

<c:set value="false" var="itemFound"/>
<c:forEach items="${model.triguideSupplies}" var="supply" varStatus="status">
  <c:if test="${supply.product != null}">
    <c:set value="true" var="itemFound"/>
	<c:set value="${supply.product}" var="product"/>
	<%@ include file="/WEB-INF/jsp/frontend/thumbnail/view1.jsp" %>
  </c:if>
</c:forEach>

<c:if test="${itemFound == false}">
<c:out value="${siteConfig['SEARCH_NOTHING_FOUND_MESSAGE'].value}" escapeXml="false"/>
</c:if>

</c:if>  
    
</c:if>
    
  </tiles:putAttribute>
</tiles:insertDefinition>
