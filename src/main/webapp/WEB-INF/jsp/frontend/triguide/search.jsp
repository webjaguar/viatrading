<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>

<script language="JavaScript">
<!--
function checkSearchForm2() {
    var match = document.getElementById('match').value;
    
    if (match == "" | match == "Type in Model") {
      alert("Please enter a model");
      document.getElementById('match').focus();
      return false;
    }
}
function checkSearchForm3() {
    var match2 = document.getElementById('match2').value;
    
    if (match2 == "" | match2 == "Type in Part #") {
      alert("Please enter a part #");
      document.getElementById('match2').focus();
      return false;
    }
}
//-->
</script>

<form action="search1.jhtm" onSubmit="return checkSearchForm2()" class="search_form_1">
<div class="search">
<div class="search_title">MODEL SEARCH</div>
<table cellspacing="0" cellpadding="0" border="0">
<tr>
<td><input name="match" id="match" type="text" size="20" value="<c:choose><c:when test="${param.match != null}"><c:out value="${param.match}"/></c:when><c:otherwise>Type in Model</c:otherwise></c:choose>" onFocus="this.value=''" class="search_textfield"></td>
<td><input type="image" value="Search" src="assets/Image/Layout/button_search.gif" ></td>
</tr>
</table>
<div class="search_example">example: Xerox 7300</div>
</div>
</form>
<div class="search_or">- OR -</div>
<form action="search1.jhtm" onSubmit="return checkSearchForm3()" class="search_form_2">
<div class="search">
<div class="search_title">MFG PART # SEARCH</div>
<table cellspacing="0" cellpadding="0" border="0">
<tr>
<td><input name="match2" id="match2" type="text" size="20" value="<c:choose><c:when test="${param.match2 != null}"><c:out value="${param.match2}"/></c:when><c:otherwise>Type in Part #</c:otherwise></c:choose>" onFocus="this.value=''" class="search_textfield"></td>
<td><input type="image" value="Search" src="assets/Image/Layout/button_search.gif" ></td>
</tr>
</table>
<div class="search_example">example: C9730</div>
</div>
</form>
<div class="search_or">- OR -</div>
<form action="search1.jhtm" class="search_form_3">
<input type="hidden" name="search" value="1">
<div class="search">
<div class="search_title">MANUFACTURER SEARCH</div>
      <select name="manufacturerId" onchange="submit()" class="search_options">
  	        <option value="">Select Manufacturer</option>
    	<c:forEach items="${triguideManufacturers}" var="manufacturer">
  	        <option <c:if test="${param.manufacturerId == manufacturer['id']}">selected</c:if>
  	        value="<c:out value="${manufacturer['id']}"/>"><c:out value="${manufacturer['name']}"/></option>
		</c:forEach>
	  </select>
</div>
</form>
 