<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="/WEB-INF/tlds/spring.tld" prefix="spring" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>

<tiles:insertDefinition name="${_template}" flush="true">
  <tiles:putAttribute name="content" type="string">
  
<link href="/assets/truckForm.css" type="text/css" rel="stylesheet" />

<c:if test="${!empty message }">
 <p class="message"><c:out value="${message}" /></p>
</c:if>
<form:form commandName="form" method="post" cssClass="truckForm"> 
  <h3>Truck Shipment Entry</h3>

  <lable for="accountNumber">Account #:</lable>
  <div class="truckEntry">
    <form:input path="accountNumber" cssClass="field"/>
    <h2><form:errors path="accountNumber" cssClass="error" /></h2>
  </div>
  

  <lable for="companyName">Account Name</lable>
  <div class="truckEntry">
    <form:input path="companyName" cssClass="field"/>
    <h2><form:errors path="companyName" cssClass="error" /></h2>
  </div>

  
  <lable for="customerPO">Purchase Order <span class="required">*</span></lable>
  <div class="truckEntry">
    <form:input path="customerPO" cssClass="field"/>
    <h2><form:errors path="customerPO" cssClass="error" /></h2>
  </div>  
  
  <lable for="lineItems">Line Item #:</lable>
  <div class="truckEntry">
  <form:input path="lineItems" cssClass="field"/>
    <h2><form:errors path="lineItems" cssClass="error" /></h2>
  </div>  

  <lable for="trackingNumber">Tracking # <span class="required">*</span>:</lable>
  <div class="truckEntry">
  <form:input path="trackingNumber" cssClass="field"/>
    <h2><form:errors path="trackingNumber" cssClass="error" /></h2>
  </div>  

  <lable for="addr1">Address:</lable>
  <div class="truckEntry">
  <form:input path="addr1" cssClass="field"/>
    <h2><form:errors path="addr1" cssClass="error" /></h2>
  </div>  

  <lable for="city">City:</lable>
  <div class="truckEntry">
  <form:input path="city" cssClass="field"/>
    <h2><form:errors path="city" cssClass="error" /></h2>
  </div>  

  <lable for="stateProvince">State:</lable>
  <div class="truckEntry">
  <form:input path="stateProvince" cssClass="field"/>
    <h2><form:errors path="stateProvince" cssClass="error" /></h2>
  </div>  

  <lable for="numberPackages"># of Ctns:</lable>
  <div class="truckEntry">
  <form:input path="numberPackages" cssClass="field"/>
    <h2><form:errors path="numberPackages" cssClass="error" /></h2>
  </div>  

  <lable for="weight">Weight:</lable>
  <div class="truckEntry">
  <form:input path="weight" cssClass="field"/>
    <h2><form:errors path="weight" cssClass="error" /></h2>
  </div>  

  <lable for="shippingCompany">Type:</lable>
  <div class="truckEntry">
  <form:select path="shippingCompany">
    <form:option value="Collect">Collect</form:option>
    <form:option value="Prepaid">Prepaid</form:option>
    <form:option value="Third Party">Third Party</form:option>
    <form:option value="FedEx Ground">FedEx Ground</form:option>
    <form:option value="FedEx Priority #1">FedEx Priority #1</form:option>
    <form:option value="FedEd 2 Day">FedEd 2 Day</form:option>
  </form:select>
    <h2><form:errors path="shippingCompany" cssClass="error" /></h2>
  </div>  

  <lable for="truckCompany">Truck Company:</lable>
  <div class="truckEntry">
  <form:select path="truckCompany">
    <option value=""> - No Selection -  </option><option 523-5020="" (800)="" pyle="" duie="" a.="">A. DUIE PYLE (800) 523-5020 </option><option 1-800-438-0654="" (sc)="" cooper="" aaa="">AAA COOPER (SC) 1-800-438-0654 </option><option 243-0800="" (631)="" abf="">ABF (631) 243-0800 </option><option 1-800-521-5113="" (sc)="" abf="">ABF (SC) 1-800-521-5113 </option><option 462-8771="" (800)="" express="" airbourne="">AIRBOURNE EXPRESS (800) 462-8771 </option><option 767-2255="" (516)="" airlands="">AIRLANDS (516) 767-2255 </option><option 777-8686="" (631)="" freight="" american="">AMERICAN FREIGHT (631) 777-8686 </option><option 1-800-814-1978="" (sc)="" freight="" american="">AMERICAN FREIGHT (SC) 1-800-814-1978 </option><option 950-9804="" (800)="" anr="">ANR (800) 950-9804 </option><option 1-800-283-7488="" (sc)="" express="" averitt="">AVERITT EXPRESS (SC) 1-800-283-7488 </option><option 1-704-455-2335="" (sc)="" bentley="">BENTLEY (SC) 1-704-455-2335 </option><option 1-888-523-6866="" express="" benton="">BENTON EXPRESS  1-888-523-6866 </option><option 1-800-672-5789="" (sc)="" bestway="">BESTWAY (SC) 1-800-672-5789 </option><option 631-3045="" (800)="" billings="">BILLINGS (800) 631-3045 </option><option 225-1005="" (800)="" boehles="">BOEHLES (800) 225-1005 </option><option 882-3338="" (800)="" express="" burlington="">BURLINGTON EXPRESS (800) 882-3338 </option><option 525-3728="" (800)="" west="" cal="">CAL WEST (800) 525-3728 </option><option express="" campbells="">CAMPBELLS EXPRESS  </option><option 243-4530="" (631)="" star="" red="" trucking="" cbl="">CBL TRUCKING/RED STAR (631) 243-4530 </option><option 243-1404="" (631)="" ccx="">CCX (631) 243-1404 </option><option 777-1300="" (631)="" transport="" central="">CENTRAL TRANSPORT (631) 777-1300 </option><option 1-800-537-4785="" (sc)="" transport="" central="">CENTRAL TRANSPORT (SC) 1-800-537-4785 </option><option 631-243-1404="" ny="" con-way="">CON-WAY ny 631-243-1404 </option><option 1-704-599-5100="" freight="" consolidated="">CONSOLIDATED FREIGHT 1-704-599-5100 </option><option 752-0440="" (516)="" freight="" consolidated="">CONSOLIDATED FREIGHT (516) 752-0440 </option><option 1-803-327-9050="" (sc)="" conway="">CONWAY (SC) 1-803-327-9050 </option><option 201-558-4760="" crystal="">CRYSTAL  201-558-4760 </option><option 468-9999="" (800)="" transport="" daylight="">DAYLIGHT TRANSPORT (800) 468-9999 </option><option 1-800-225-5345="" dhl="">DHL 1-800-225-5345 </option><option diamond="">DIAMOND  </option><option )="" ny="" (="" line="" dot="">DOT LINE ( NY )  </option><option 1-800-365-0100="" express="" edi="">EDI EXPRESS  1-800-365-0100 </option><option 293-4590="" (631)="" estes="">ESTES (631) 293-4590 </option><option 1-704-597-9130="" (sc)="" express="" estes="">ESTES EXPRESS (SC) 1-704-597-9130 </option><option 1-631-777-8686="" (ny)="" frieght="" ex="" fed="">FED EX FRIEGHT (NY) 1-631-777-8686 </option><option 1-631-249-5070="" national="" ex="" fed="">FED EX NATIONAL 1-631-249-5070 </option><option 1-800-463-3339="" express="" federal="">FEDERAL EXPRESS 1-800-463-3339 </option><option 696-2950="" (610)="" frames="">FRAMES (610) 696-2950 </option><option 1-803-366-4181="" (sc)="" fredickson="">FREDICKSON (SC) 1-803-366-4181 </option><option 1-888-826-4321="" gmg="">GMG  1-888-826-4321 </option><option 342-5463="" (800)="" god="">GOD (800) 342-5463 </option><option 1-704-338-9103="" (sc)="" goggins="">GOGGINS (SC) 1-704-338-9103 </option><option trucking="" hercules="">HERCULES TRUCKING       </option><option 823-7777="" (716)="" star="" red="" holland="">HOLLAND/RED STAR (716) 823-7777 </option><option 274-2200="" (800)="" express="" howards="">HOWARDS EXPRESS (800) 274-2200 </option><option 722-2213="" (800)="" freight="" hyman="">HYMAN FREIGHT (800) 722-2213 </option><option 667-6731="" (631)="" express="" j.p.="">J.P. EXPRESS (631) 667-6731 </option><option 1-888-465-3842="" (sc)="" jevic="">JEVIC (SC) 1-888-465-3842 </option><option 449-6987="" (800)="" transport="" jevic="">JEVIC TRANSPORT (800) 449-6987 </option><option maloney="" jj="">JJ MALONEY  </option><option air="" land="">LAND AIR       </option><option 893-6342="" (800)="" megasystems="">MEGASYSTEMS (800) 893-6342 </option><option t&#38;c="" metro="">METRO T&amp;C       </option><option 1-866-579-2974="" (sc)="" milan="">MILAN (SC) 1-866-579-2974 </option><option x4="" 249-6420="" (631)="" nemf="">NEMF (631) 249-6420 X4 </option><option 877-870-4031="" century="" new="">NEW CENTURY 877-870-4031 </option><option 366-7590="" (718)="" penn="" new="">NEW PENN (718) 366-7590 </option><option 392-5363="" (715)="" northway="">NORTHWAY (715) 392-5363 </option><option 1-800-866-6923="" (n.y.)="" nyce="">NYCE (N.Y.) 1-800-866-6923 </option><option 320-2313="" (800)="" dominion="" old="">OLD DOMINION (800) 320-2313 </option><option 1-704-399-4502="" (sc)="" dominion="" old="">OLD DOMINION (SC) 1-704-399-4502 </option><option 927-1377="" (800)="" nemf="" overland="">OVERLAND/NEMF (800) 927-1377 </option><option 1-800-677-9393="" (sc)="" overnite="">OVERNITE (SC) 1-800-677-9393 </option><option 541-1616="" (800)="" transport="" overnite="">OVERNITE TRANSPORT (800) 541-1616 </option><option 1-800-243-9914="" trucking="" pace="">PACE TRUCKING 1-800-243-9914 </option><option truck="" perfered="">PERFERED TRUCK  </option><option trucking="" perfered="">PERFERED TRUCKING  </option><option 8033663250="" transport="" piedmont="">PIEDMONT TRANSPORT 8033663250 </option><option 622-6555="" (800)="" rock="" plymouth="">PLYMOUTH ROCK (800) 622-6555 </option><option 744-7669="" (888)="" protrans="">PROTRANS (888) 744-7669 </option><option 383-5161="" (718)="" trucking="" r&#38;l="">R&amp;L TRUCKING (718) 383-5161 </option><option 586-9300="" (631)="" star="" red="">RED STAR (631) 586-9300 </option><option 1-704-551-4134="" (sc)="" roadrunner="">ROADRUNNER (SC) 1-704-551-4134 </option><option 872-2725="" (800)="" pak="" rps-road="">RPS-ROAD PAK  (800) 872-2725 </option><option 1-704-599-7007="" (sc)="" saia="">SAIA (SC) 1-704-599-7007 </option><option 1-704-597-9820="" (sc)="" southeastern="">SOUTHEASTERN (SC) 1-704-597-9820 </option><option inc.="" express,="" teal's="">TEAL'S EXPRESS, INC.  </option><option 1-800-849-0189="" trucking="" terminal="">TERMINAL TRUCKING 1-800-849-0189 </option><option 1-888-878-9229="" overland="" tst="">TST OVERLAND 1-888-878-9229 </option><option 1-800-541-1616="" (ny)="" frieght="" ups="">UPS FRIEGHT (NY) 1-800-541-1616 </option><option 1-800-333-7400="" fright="" ups="">UPS FRIGHT 1-800-333-7400 </option><option 1-800-222-8333="" (sc)="" ups="">UPS (SC) 1-800-222-8333 </option><option 1-800-755-3313="" (sc)="" dugan="" usf="">USF DUGAN (SC) 1-800-755-3313 </option><option 1-866-465-5263="" (ny)="" holland="" usf="">USF HOLLAND (NY) 1-866-465-5263 </option><option 1-704-599-2040="" (sc)="" holland="" usf="">USF HOLLAND (SC) 1-704-599-2040 </option><option 1-704-599-7050="" (sc)="" redstar="" usf="">USF REDSTAR (SC) 1-704-599-7050 </option><option 829-0830="" (800)="" trucking="" vallerie="">VALLERIE TRUCKING (800) 829-0830 </option><option (888)848-2591="" trucking="" vita="">VITA TRUCKING (888)848-2591 </option><option 1-800-523-0596="" express="" volpe="">VOLPE EXPRESS 1-800-523-0596 </option><option 251-1015="" (800)="" trucking="" volunteer="">VOLUNTEER TRUCKING (800) 251-1015 </option><option 1-800-334-9273="" (sc)="" ward="">WARD (SC) 1-800-334-9273 </option><option 526-9273="" (800)="" trucking="" ward="">WARD TRUCKING (800) 526-9273 </option><option 1-800-334-9273="" (sc)="" wathins="">WATHINS (SC) 1-800-334-9273 </option><option 249-5070="" (631)="" watkins="">WATKINS (631) 249-5070 </option><option 1-800-353-2537="" (sc)="" wilson="">WILSON (SC) 1-800-353-2537 </option><option 1-800-762-3929="" yrc="">YRC 1-800-762-3929 </option>
  </form:select>
    <h2><form:errors path="truckCompany" cssClass="error" /></h2>
  </div>  

  <div class="submitWrapper">
  <input type="submit" value="submit" name="Submit" class="submit" />
  <input type="submit" name="_reset" value="Reset this form"> 
  </div>
</form:form> 

  
  </tiles:putAttribute>
</tiles:insertDefinition>