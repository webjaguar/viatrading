<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="/WEB-INF/tlds/spring.tld" prefix="spring" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>


<script src="/dv/w3data.js"></script>
<script type='text/javascript' src='/wp-includes/js/jquery/jquery.js?ver=1.12.4'></script>
<script type='text/javascript' src='/wp-content/themes/bridge/js/plugins.js?ver=4.6.1'></script>
<script type='text/javascript' src='/wp-content/themes/bridge/js/default.min.js?ver=4.6.1'></script>


<tiles:insertDefinition name="${_template}" flush="true">
 <tiles:putAttribute name="content" type="string">
  <c:out value="${model.viewMyReportLayout.headerHtml}" escapeXml="false"/>
 <!--<form id="list" method="post">
 <input type="hidden" id="sort" name="sort" value="${model.sort}" />
  <table width="90%" align="center" cellspacing="5" cellpadding="0" border="0">
  <tr>
	<td>
	<div class="pageNavi">Page 
		<select name="page" onchange="submit()">
		<c:forEach begin="1" end="${model.orders.pageCount}" var="page">
  	        <option value="${page}" <c:if test="${page == (model.orders.page+1)}">selected</c:if>>${page}</option>
		</c:forEach>
		</select>
		of <c:out value="${model.orders.pageCount}"/></div>
	</td>
  </tr>
  <tr>  
	<td colspan="2">
    <table border="0" cellpadding="3" cellspacing="1" width="100%" class="listings">
    <tr class="listingsHdr2">
		<td align="left">
			<table cellspacing="0" cellpadding="1">
			<tr>
		   	<c:choose>
			   	<c:when test ="${model.sort == 'date_ordered desc' }">
				  	<td class="listingsHdr3">
				  	<a class="sortLink" href="#" onClick="document.getElementById('sort').value='date_ordered';document.getElementById('list').submit()"> <fmt:message key="orderDate" /></a>
				  	</td>
			  	</c:when>
			  	<c:when test ="${model.sort == 'date_ordered' }">
				  	<td class="listingsHdr3">
				  	<a class="sortLink" href="#" onClick="document.getElementById('sort').value='date_ordered desc';document.getElementById('list').submit()"> <fmt:message key="orderDate" /></a>				  	
				  	</td>
			  	</c:when>
			  	<c:otherwise>
				  	<td class="listingsHdr3">
				  	<a class="sortLink" href="#" onClick="document.getElementById('sort').value='date_ordered desc';document.getElementById('list').submit()"> <fmt:message key="orderDate" /></a>
				  	</td>				  
			  	</c:otherwise>
		  	</c:choose>
		  	</tr>
			</table>
		</td>
		<td align="left">
			<table cellspacing="0" cellpadding="1">
			<tr>
		   	<c:choose>
			   	<c:when test ="${model.sort == 'date_shipped desc' }">
				  	<td class="listingsHdr3">
				  	<a class="sortLink" href="#" onClick="document.getElementById('sort').value='date_shipped';document.getElementById('list').submit()"> <fmt:message key="shipDate" /></a>
				  	</td>
			  	</c:when>
			  	<c:when test ="${model.sort == 'date_shipped' }">
				  	<td class="listingsHdr3">
				  	<a class="sortLink" href="#" onClick="document.getElementById('sort').value='date_shipped desc';document.getElementById('list').submit()"> <fmt:message key="shipDate" /></a>				  	
				  	</td>
			  	</c:when>
			  	<c:otherwise>
				  	<td class="listingsHdr3">
				  	<a class="sortLink" href="#" onClick="document.getElementById('sort').value='date_shipped desc';document.getElementById('list').submit()"> <fmt:message key="shipDate" /></a>
				  	</td>				  
			  	</c:otherwise>
		  	</c:choose>
		  	</tr>
			</table>
		</td>
	  	<td align="left">
				<table cellspacing="0" cellpadding="1">
				<tr>
			   	<c:choose>
				   	<c:when test ="${model.sort == 'order_id desc' }">
					  	<td class="listingsHdr3">
					  	<a class="sortLink" href="#" onClick="document.getElementById('sort').value='order_id';document.getElementById('list').submit()"> <fmt:message key="invoiceN" /></a>
					  	</td>
				  	</c:when>
				  	<c:when test ="${model.sort == 'order_id' }">
					  	<td class="listingsHdr3">
					  	<a class="sortLink" href="#" onClick="document.getElementById('sort').value='order_id desc';document.getElementById('list').submit()"> <fmt:message key="invoiceN" /></a>				  	
					  	</td>
				  	</c:when>
				  	<c:otherwise>
					  	<td class="listingsHdr3">
					  	<a class="sortLink" href="#" onClick="document.getElementById('sort').value='order_id desc';document.getElementById('list').submit()"> <fmt:message key="invoiceN" /></a>
					  	</td>				  
				  	</c:otherwise>
			  	</c:choose>
			  	</tr>
				</table>
		</td> 
		<td align="left">
				<table cellspacing="0" cellpadding="1">
				<tr>
			   	<c:choose>
				   	<c:when test ="${model.sort == 'product_sku desc' }">
					  	<td class="listingsHdr3">
					  	<a class="sortLink" href="#" onClick="document.getElementById('sort').value='product_sku';document.getElementById('list').submit()"> <fmt:message key="sku" /></a>
					  	</td>
				  	</c:when>
				  	<c:when test ="${model.sort == 'product_sku' }">
					  	<td class="listingsHdr3">
					  	<a class="sortLink" href="#" onClick="document.getElementById('sort').value='product_sku desc';document.getElementById('list').submit()"> <fmt:message key="sku" /></a>				  	
					  	</td>
				  	</c:when>
				  	<c:otherwise>
					  	<td class="listingsHdr3">
					  	<a class="sortLink" href="#" onClick="document.getElementById('sort').value='product_sku desc';document.getElementById('list').submit()"> <fmt:message key="sku" /></a>
					  	</td>				  
				  	</c:otherwise>
			  	</c:choose>
			  	</tr>
				</table>
		</td>
	  	<td align="left">
				<table cellspacing="0" cellpadding="1">
				<tr>
			   	<c:choose>
				   	<c:when test ="${model.sort == 'product_name desc' }">
					  	<td class="listingsHdr3">
					  	<a class="sortLink" href="#" onClick="document.getElementById('sort').value='product_name';document.getElementById('list').submit()"> <fmt:message key="productName" /></a>
					  	</td>
				  	</c:when>
				  	<c:when test ="${model.sort == 'product_name' }">
					  	<td class="listingsHdr3">
					  	<a class="sortLink" href="#" onClick="document.getElementById('sort').value='product_name desc';document.getElementById('list').submit()"> <fmt:message key="productName" /></a>				  	
					  	</td>
				  	</c:when>
				  	<c:otherwise>
					  	<td class="listingsHdr3">
					  	<a class="sortLink" href="#" onClick="document.getElementById('sort').value='product_name desc';document.getElementById('list').submit()"> <fmt:message key="productName" /></a>
					  	</td>				  
				  	</c:otherwise>
			  	</c:choose>
			  	</tr>
				</table>
		</td>  	
	  	<td class="nameCol" colspan="2"><fmt:message key="qty" /></td>
	  	<td class="nameCol" colspan="2"><fmt:message key="orderType" /></td>
	  	<td class="nameCol" colspan="2"><fmt:message key="subTotal" /></td>
	  	<td class="nameCol" colspan="2"><fmt:message key="unitPrice" /></td>
	  	<td class="nameCol" colspan="2"><fmt:message key="payRate" /></td>
	  	<td class="nameCol" colspan="2"><fmt:message key="netAmount" /></td>  	
	  	<td class="nameCol" colspan="2"><fmt:message key="status" /></td>
	  	<td class="nameCol" colspan="2"><fmt:message key="payDate" /></td>
	  	<td class="nameCol" colspan="2"><fmt:message key="payMethod" /></td>
	  	<td class="nameCol" colspan="2"><fmt:message key="note" /></td>
  	</tr>
<c:forEach items="${model.orders.pageList}" var="order" varStatus="status">
  <tr class="row${status.index % 2}">
    <td class="nameCol"><fmt:formatDate type="date" value="${order.dateOrdered}" pattern="M/dd/yy"/></td>
    <td class="nameCol"><fmt:formatDate type="date" value="${order.dateShipped}" pattern="M/dd/yy"/></td>
	<td class="nameCol"><c:out value="${order.orderId}"/></td>
	<td class="nameCol"><c:out value="${order.productSku}"/></td>
	<td class="nameCol" ><c:out value="${order.productName}"/></td>
	<td class="nameCol" colspan="2"><c:out value="${order.quantity}"/></td>
	<td class="nameCol" colspan="2"><c:out value="${order.orderType}"/></td>
	<td class="nameCol" colspan="2">
		<c:if test="${order.costPercent == true}">
		<fmt:message key="${siteConfig['CURRENCY'].value}" /><fmt:formatNumber value="${order.subTotal}" pattern="#,##0.00" />
		</c:if>
		<c:if test="${order.costPercent == false}">
		n/a
		</c:if>
	</td>
	<td class="nameCol" colspan="2">
		<c:if test="${order.costPercent == true}">
		<fmt:message key="${siteConfig['CURRENCY'].value}" /><fmt:formatNumber value="${order.unitPrice}" pattern="#,##0.00" />
		</c:if>
		<c:if test="${order.costPercent == false}">
		n/a
		</c:if>
	</td>
	<td class="nameCol" colspan="2">
		<c:if test="${order.costPercent == true}">
		<fmt:formatNumber value="${order.cost}" pattern="#,##0.00" />%
		</c:if>
		<c:if test="${order.costPercent == false}">
		$<fmt:formatNumber value="${order.cost}" pattern="#,##0.00" />
		</c:if>
	</td>	
	<td class="nameCol" colspan="2"><fmt:message key="${siteConfig['CURRENCY'].value}" /><fmt:formatNumber value="${order.netAmt}" pattern="#,##0.00" /></td>
	<td class="nameCol" colspan="2">
		<c:choose>
	    <c:when test="${order.status == true }">
	    	<fmt:message key="paid"/>
	    </c:when>
	    <c:otherwise>
	    	<fmt:message key="due"/>
	    </c:otherwise>
	    </c:choose>
	</td>
    <td class="nameCol" colspan="2">
    <c:choose>
	    <c:when test="${order.status == true }">
    		<fmt:formatDate type="date" value="${order.payDate}" pattern="M/dd/yy"/>
	    </c:when>
	    <c:otherwise>
	    	n/a
	    </c:otherwise>
	</c:choose>
	</td>
    <td class="nameCol" colspan="2">
    <c:choose>
	    <c:when test="${order.status == true }">
	    	<c:out value="${order.payMethod}"/>
	    </c:when>
	    <c:otherwise>
			n/a
	    </c:otherwise>
	    </c:choose>
    </td>
    <td class="nameCol" colspan="2">
    <c:choose>
	    <c:when test="${order.note != null }">
	    	<c:out value="${order.note}"/>
	    </c:when>
	    <c:otherwise>
			n/a
	    </c:otherwise>
	 </c:choose>
    </td>		
  </tr> 
</c:forEach>
    </table>
    <table border="0" cellpadding="0" cellspacing="1" width="100%">
	  <tr>
	  <td align="right">
	  <select name="size" onchange="submit()">
	    <c:forTokens items="10,25,50,100,500" delims="," var="current">
	  	  <option value="${current}" <c:if test="${current == model.orders.pageSize}">selected</c:if>>${current} <fmt:message key="perPage" /></option>
	    </c:forTokens>
	  </select>
	  </td>
	  </tr>
   </table>
  </table>
 </form>-->

		<div id="accountSettingsWrapper">
		<div class="row">
			<div class="col-sm-12">
				<h2></h2>
			</div>
			<div class="col-sm-6">
				<div id="account_Manager" class="accountSettingsSection">
					<h1><font size="48" color="#4682B4"><i><b><fmt:message key="f_my" /></b></i></font><font size="48" color="#FFA812"><i><b><fmt:message key="f_report"/></b></i></font></h1>					
				</div>
			</div>
			<div class="col-sm-6">
				<div id="account_Manager" class="accountSettingsSection" style="visibility: visible;">
				<div class="row">
					<div class="col-sm-12 col-md-6">
						<div class="accountSettingsLeft">
						</div>
					</div>
					<div class="col-sm-12 col-md-6">
						<div class="accountSettingsRight">
							<img src="assets/responsive/img/liquidatenow_logo.png" class="img-responsive liquidatenow_logo">
						</div>
					</div>
				</div>
				</div>
			</div>				
					
		</div>
		</div>

 
 		<script type="text/javascript" src="/dv/liveapps/scripts.js.php"></script>
        <link rel="stylesheet" type="text/css" href="/dv/liveapps/myaccount.css">
	
		<div id="lnowreport">
		<script type="text/javascript">
		<!--
			$(document).ready(function() {
				lnowreport (0,0,${model.userId});
			});
		//-->
		</script>
		
		
		</div>
 
 
 
  <c:out value="${model.viewMyReportLayout.footerHtml}" escapeXml="false"/>
 </tiles:putAttribute>
</tiles:insertDefinition>