<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ taglib uri="/WEB-INF/tlds/custom.tld" prefix="wj" %>

<tiles:insertDefinition name="${_template}" flush="true">
  <tiles:putAttribute name="content" type="string">

<table>
<tr>
<td valign="top">
<table border="0" cellpadding="3">
  <tr>
    <td align="right"><c:out value="${siteConfig['SERVICEABLE_ITEM_TITLE'].value}"/> ID:</td>
    <td><c:out value="${model.item.itemId}"/></td>
  </tr>
  <tr>
    <td align="right"><fmt:message key="S/N" />:</td>
    <td><c:out value="${model.item.serialNum}"/></td>
  </tr>
  <tr>
    <td align="right"><fmt:message key="sku" />:</td>
    <td><c:out value="${model.item.sku}"/></td>
  </tr>
  <tr>
    <td align="right"><fmt:message key="description" />:</td>
    <td><c:out value="${model.product.name}"/></td>
  </tr>
<c:forEach items="${model.product.productFields}" var="productField" varStatus="status">
  <tr>
    <td align="right"><c:out value="${productField.name}"/>:</td>
    <c:choose>
		<c:when test="${productField.formatType != null and productField.formatType != '' and productField.fieldType == 'number' }">
			<td class="details_field_value_row${row.index % 2}">
			<c:catch var="catchFormatterException">
				<fmt:formatNumber value="${wj:trimAndSplit(productField.value, '|' , false, ',', true, true)}" pattern="${productField.formatType}" />
			</c:catch>
			<c:if test = "${catchFormatterException != null}"><c:out value="${wj:trimAndSplit(productField.value, '|' , false, ',', true, true)}" escapeXml="false"/></c:if>
			</td>
		</c:when>
		<c:otherwise>
			<td class="details_field_value_row${row.index % 2}"><c:out value="${wj:trimAndSplit(productField.value, '|' , false, ',', true, true)}" escapeXml="false"/></td>
		</c:otherwise>
	</c:choose>     
  </tr>
</c:forEach>
</table>
</td>
<td valign="top">
<table border="0" cellpadding="3">
  <tr>
	<td valign="top" align="right"><c:out value="${siteConfig['SERVICEABLE_ITEM_TITLE'].value}"/> <fmt:message key="location" />:</td>	
	<td>
	<c:if test="${model.item.address.company != ''}">
<c:out value="${model.item.address.company}"/><br/>
</c:if>
<c:out value="${model.item.address.firstName}"/> <c:out value="${model.item.address.lastName}"/><br/>
<c:out value="${model.item.address.addr1}"/> <c:out value="${model.item.address.addr2}"/><br/>
<c:out value="${model.item.address.city}"/>, <c:out value="${model.item.address.stateProvince}"/> <c:out value="${model.item.address.zip}"/><br/>
<c:out value="${model.countries[model.item.address.country]}"/><br/>
<c:if test="${model.item.address.phone != ''}">
<fmt:message key="f_phone" />: <c:out value="${model.item.address.phone}"/><br/>
</c:if>
<fmt:message key="f_fax" />: <c:out value="${model.item.address.fax}"/>
	</td>
  </tr>
</table>
</td>
</tr>
</table>

<div align="center"><b><fmt:message key="serviceHistory" /></b></div>
<table cellspacing="5" cellpadding="0" border="0" align="center">
  <tr>
    <td>Total Cost Of Operation (TCO): </td>
	<td align="right"><fmt:message key="${siteConfig['CURRENCY'].value}" /><fmt:formatNumber value="${model.tco}" pattern="#,##0.00"/></td>
  </tr>
  <c:if test="${model.bwTotal != null}">
  <tr>
    <td>B&amp;W Total Pages Printed: </td>
	<td align="right"><fmt:formatNumber value="${model.bwTotal}" pattern="#,##0"/></td>
  </tr>
  <tr>
    <td>B&amp;W Total Cost Per Page (TCPP): </td>
	<td align="right"><fmt:message key="${siteConfig['CURRENCY'].value}" /><fmt:formatNumber value="${model.tco/model.bwTotal}" pattern="#,##0.000"/></td>
  </tr>
  </c:if>
  <c:if test="${model.colorTotal != null}">
  <tr>
    <td>Color Total Pages Printed: </td>
	<td align="right"><fmt:formatNumber value="${model.colorTotal}" pattern="#,##0"/></td>
  </tr>
  <tr>
    <td>Color Total Cost Per Page (TCPP): </td>
	<td align="right"><fmt:message key="${siteConfig['CURRENCY'].value}" /><fmt:formatNumber value="${model.tco/model.colorTotal}" pattern="#,##0.000"/></td>
  </tr>
  </c:if>
</table>
<table width="100%" align="center" cellspacing="5" cellpadding="0" border="0">
  <tr>
	<td colspan="2">
<table border="0" cellpadding="3" cellspacing="1" width="100%" class="listings"> 
  <tr class="listingsHdr2">
    <c:set var="cols" value="0"/>
    <td width="15%" class="woHdr"><fmt:message key="itemCode" /></td>
    <td class="woHdr"><fmt:message key="description" /></td>
	<td width="10%" class="woHdr" align="center">In SN</td>
	<td width="10%" class="woHdr" align="center">Out SN</td>
    <td width="5%" class="woHdr" align="center"><fmt:message key="qty" /></td>
    <c:if test="${hasPacking}">
	  <c:set var="cols" value="${cols+1}"/>
      <td width="10%" class="woHdr"><fmt:message key="packing" /></td>
    </c:if>
    <c:if test="${hasContent}">
	  <c:set var="cols" value="${cols+1}"/>
      <td class="woHdr"><fmt:message key="content" /></td>
    </c:if>
    <td width="10%" class="woHdr" align="center"><fmt:message key="productPrice" /><c:if test="${order.hasContent}">/<c:out value="${siteConfig['CASE_UNIT_TITLE'].value}" /></c:if></td>
	  <c:set var="cols" value="${cols+3}"/>
	  <td align="center">End Count</td>
	  <td align="center"><fmt:message key="total"/> <fmt:message key="page"/></td>
	  <td align="center">CPP</td>
  </tr>
<c:forEach items="${model.workOrders}" var="workOrder" varStatus="status">
  <tr class="row${status.index % 2}">
	<td colspan="${6+cols}">
	  <table width="100%">
	  <tr>
	  <td valign="top"><b><fmt:message key="workOrder"/> # <c:out value="${workOrder.service.serviceNum}"/></b></td>
	  <td valign="top">	  
	  <table>
	    <tr>
	     <td align="right"><fmt:message key="status" /> :</td>
		 <td><c:out value="${workOrder.service.status}"/></td>
		</tr>
	    <tr>
	     <td align="right"><fmt:message key="reportDate" /> :</td>
		 <td><fmt:formatDate type="date" timeStyle="default" value="${workOrder.service.reportDate}"/></td>
		</tr>
	    <tr>
	     <td align="right"><fmt:message key="completeDate" /> :</td>
		 <td><fmt:formatDate type="date" timeStyle="default" value="${workOrder.service.completeDate}"/></td>
		</tr>
	  </table>
	  </td>
	  <td valign="top">	  
	  <table>
	    <c:if test="${workOrder.outSN != null and fn:trim(workOrder.outSN) != ''}">
	    <tr>
	     <td align="right">Out SN :</td>
		 <td><c:out value="${workOrder.outSN}"/></td>
		</tr>
		</c:if>
		<c:if test="${workOrder.inSN != null and fn:trim(workOrder.inSN) != ''}">
	    <tr>
	     <td align="right">In SN :</td>
		 <td><c:out value="${workOrder.inSN}"/></td>
		</tr>
		</c:if>
	    <tr>
	     <td align="right"><fmt:message key="bwCount" /> :</td>
		 <td><fmt:formatNumber value="${workOrder.bwCount}" pattern="#,##0"/></td>
		</tr>
	    <tr>
	     <td align="right"><fmt:message key="colorCount" /> :</td>
		 <td><fmt:formatNumber value="${workOrder.colorCount}" pattern="#,##0"/></td>
		</tr>
	    <tr>
	     <td align="right"><fmt:message key="technician" /> :</td>
		 <td><c:out value="${workOrder.technician}"/></td>
		</tr>
	  </table>
	  </td>
	  </tr>
	  </table>
	</td>
  </tr>
  <c:forEach var="lineItem" items="${workOrder.lineItems}">
  <tr class="row${status.index % 2}">
	<td><c:out value="${lineItem.product.sku}"/></td>
	<td><c:out value="${lineItem.product.name}"/></td>
	<td align="center"><c:out value="${lineItem.inSN}" /></td>
	<td align="center"><c:out value="${lineItem.outSN}" /></td>
	<td align="center"><c:out value="${lineItem.quantity}" /></td>
	<c:if test="${workOrder.hasPacking}">
	<td align="center"><c:out value="${lineItem.product.packing}" /></td>
	</c:if>
	<c:if test="${workOrder.hasContent}">
	<td align="center"><c:out value="${lineItem.product.caseContent}" /></td>
	</c:if>
  	<td align="right"><c:if test="${lineItem.unitPrice != null}"><fmt:message key="${siteConfig['CURRENCY'].value}" /></c:if><fmt:formatNumber value="${lineItem.unitPrice}" pattern="#,##0.00"/></td>
	<td align="right"><fmt:formatNumber value="${lineItem.bwEndCount}" pattern="#,##0"/></td>
	<td align="right"><fmt:formatNumber value="${lineItem.bwTotalCount}" pattern="#,##0"/></td>
  	<td align="right"><c:if test="${lineItem.bwTotalCount != null and lineItem.unitPrice != null}"><fmt:message key="${siteConfig['CURRENCY'].value}" /><fmt:formatNumber value="${lineItem.unitPrice / lineItem.bwTotalCount}" pattern="#,##0.000"/></c:if></td>
  </tr>
  <c:if test="${lineItem.notes != ''}">
  <tr class="row${status.index % 2}">
  	<td colspan="${6+cols}"><div style="padding:0px 50px 0px 50px"><fmt:message key="workNotes" />: <c:out value="${lineItem.notes}" /></div></td>
  </tr>
  </c:if>
  </c:forEach>  
</c:forEach>
</table> 
	</td>
  </tr>
</table>

  </tiles:putAttribute>
</tiles:insertDefinition>
