<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="/WEB-INF/tlds/spring.tld" prefix="spring" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>

<tiles:insertDefinition name="${_template}" flush="true">
  <tiles:putAttribute name="content" type="string">

<c:if test="${model.brand != null}">

<table width="90%" align="center" cellspacing="5" cellpadding="0" border="0">
  <tr>
  	<td class="listingsHdr1"><c:out value="${model.brand.name}"/> <fmt:message key="report" /></td>
	<td align="right">&nbsp;</td>
  </tr>
  <tr>
	<td colspan="2">
<table border="0" cellpadding="3" cellspacing="1" width="100%" class="listings"> 
  <tr class="listingsHdr2">
  	<td rowspan="2" class="nameCol"><fmt:message key="productSku" /></td>
  	<td rowspan="2" class="nameCol"><fmt:message key="description" /></td>
    <td rowspan="2" align="center"><fmt:message key="inventory" /></td>
    <td colspan="2" align="center"><fmt:message key="orderTotal" /></td>
  </tr>
  <tr class="listingsHdr2">
    <td align="center"><fmt:message key="quantity" /></td>
    <td align="center"><fmt:message key="price" /></td>
  </tr>
<c:forEach items="${model.products}" var="product" varStatus="status">
  <tr class="row${status.index % 2}">
    <td class="nameCol"><a href="product.jhtm?sku=<c:out value="${product.sku}"/>" class="nameLink"><c:out value="${product.sku}"/></a></td>			
	<td class="nameCol">
		<c:out value="${product.name}"/>
		<c:if test="${product.shortDesc != null and product.shortDesc != ''}"><br/><c:out value="${product.shortDesc}"/></c:if>
		<c:if test="${product.longDesc != null and product.longDesc != ''}"><br/><c:out value="${product.longDesc}"/></c:if>	
	</td>
	<td align="center"><c:out value="${product.inventory}"/></td>
	<td align="center"><a href="reportByBrand.jhtm?brand=<c:out value="${model.brand.skuPrefix}"/>&sku=<c:out value="${product.sku}"/>" class="nameLink"><c:out value="${model.orderReportMap[product.sku]['total_quantity']}"/></a></td>
	<td align="right">
	  <c:choose>
	  <c:when test="${model.orderReportMap[product.sku]['total_price'] > 0}">
	    <a href="reportByBrand.jhtm?brand=<c:out value="${model.brand.skuPrefix}"/>&sku=<c:out value="${product.sku}"/>" class="nameLink"><fmt:message key="${siteConfig['CURRENCY'].value}" /><fmt:formatNumber value="${model.orderReportMap[product.sku]['total_price']}" pattern="#,##0.00" /></a>
	  </c:when>
	  <c:otherwise>-</c:otherwise>
	  </c:choose>
	</td>
  </tr>
</c:forEach>
<c:if test="${empty model.products}">
  <tr>
    <td colspan="5" class="row0">&nbsp;</td>
  </tr>
</c:if>
</table>
	</td>
  </tr>
</table>

</c:if>

  </tiles:putAttribute>
</tiles:insertDefinition>
