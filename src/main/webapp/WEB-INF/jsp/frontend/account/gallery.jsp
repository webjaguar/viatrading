<%@ page import="java.util.Random" %>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>

<%
pageContext.setAttribute("randomNum", new Random().nextInt(1000));
%>

<tiles:insertDefinition name="${_template}" flush="true">
  <tiles:putAttribute name="content" type="string">
<script type="text/javascript">
<!--
function optionsSelected() {
	var indexs = document.getElementsByName("__selected_index");	
	if (indexs.length == 1) {
      if (document.product_form.__selected_index.checked) {
    	return true;
      }
    } else {
      for (i = 0; i < indexs.length; i++) {
        if (document.product_form.__selected_index[i].checked) {
    	  return true;
        }
      }
    }
    alert("Please select image(s).");       
    return false;
} 
//-->
</script>  
<div style="width:95%;margin:auto;">

<form:form commandName="productForm" method="post" enctype="multipart/form-data" name="product_form">
<input type="hidden" name="selected" id="selected">

<c:if test="${message != null}">
  <div class="message"><fmt:message key="${message}"/></div>
</c:if>

<fieldset class="register_fieldset">
<legend>Gallery</legend>
<c:forEach items="${galleryFiles}" var="file" varStatus="status">
<div style="background-color:#C2C29E;float:left;margin:0px 5px 5px 0px;padding:5px;text-align:center;border:2px #ffffff solid;<c:if test="${file['selected']}">border:2px #ff0000 solid;</c:if>">
	<div style="margin: 10px;padding: 2px;border:1px #ffffff solid;">
      <img src="assets/Image/Product/detailsbig/<c:out value="${productForm.product.defaultSupplierId}"/>/<c:out value="${file['file'].name}"/>?rnd=${randomNum}" border="0" height="100">
	</div>
	<div>
	  <c:out value="${file['file'].name}"/>
	  <c:choose>
        <c:when test="${file['size'] > (1024*1024)}">
    	  (<fmt:formatNumber value="${file['size']/1024/1024}" pattern="#,##0.0"/> MB)
    	</c:when>
        <c:when test="${file['size'] > 1024}">
    	  (<fmt:formatNumber value="${file['size']/1024}" pattern="#,##0"/> KB)
    	</c:when>
    	<c:otherwise>
    	  (1 KB)
    	</c:otherwise>
      </c:choose> 	  
	</div>	
	<input type="checkbox" name="__selected_index" value="${status.index}" /> 
</div>
</c:forEach>	
</fieldset>
	<div class="buttonWrapper">
		<input type="submit" name="_target0" value="<fmt:message key="select" />" onClick="return optionsSelected()">
		<input type="submit" name="_delete" value="<fmt:message key="delete" />" onClick="return optionsSelected();return confirm('Delete permanently?')">
	</div>

<div class="uploadWrapper">
<c:if test="${canUpload}">
<input value="browse" type="file" name="image_file"/>
<input type="submit" value="Upload" name="_upload">
</c:if>
<c:if test="${canUpload}">
<div style="color:#FF0000;padding:10px">Note: Maximum 2 MB per upload</div>
</c:if>
<input type="submit" value="Back to Product" name="_target0">
</div>
</form:form>

</div>

  </tiles:putAttribute>
</tiles:insertDefinition>