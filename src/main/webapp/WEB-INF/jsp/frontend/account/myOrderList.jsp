<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>

<tiles:insertDefinition name="${_template}" flush="true">
  <tiles:putAttribute name="content" type="string">
<c:if test="${gSiteConfig['gMYORDER_LIST']}">
  
<c:if test="${fn:trim(model.myOrderListLayout.headerHtml) != ''}">
  <c:set value="${model.myOrderListLayout.headerHtml}" var="headerHtml"/>
  <c:set value="${fn:replace(headerHtml, '#firstname#', userSession.firstName)}" var="headerHtml"/>
  <c:set value="${fn:replace(headerHtml, '#lastname#', userSession.lastName)}" var="headerHtml"/>
  <c:set value="${fn:replace(headerHtml, '#email#', userSession.username)}" var="headerHtml"/>
  <c:out value="${headerHtml}" escapeXml="false"/>
</c:if>

  <c:forEach items="${model.myOrderProducts.pageList}" var="product" varStatus="status">
  <c:if test="${status.first}">
  <form method="post">
  <div class="pageNavi">Page 
		<select name="page" onchange="submit()">
		<c:forEach begin="1" end="${model.myOrderProducts.pageCount}" var="page">
  	        <option value="${page}" <c:if test="${page == (model.myOrderProducts.page+1)}">selected</c:if>>${page}</option>
		</c:forEach>
		</select>
  of <c:out value="${model.myOrderProducts.pageCount}"/>
  </div> 
  </form>
  </c:if>
    <%@ include file="/WEB-INF/jsp/frontend/quickmode/view1.jsp" %>
 </c:forEach>
</c:if>

<c:if test="${fn:trim(model.myOrderListLayout.footerHtml) != ''}">
  <c:set value="${model.myOrderListLayout.footerHtml}" var="footerHtml"/>
  <c:set value="${fn:replace(footerHtml, '#firstname#', userSession.firstName)}" var="footerHtml"/>
  <c:set value="${fn:replace(footerHtml, '#lastname#', userSession.lastName)}" var="footerHtml"/>
  <c:set value="${fn:replace(footerHtml, '#email#', userSession.username)}" var="footerHtml"/>
  <c:out value="${footerHtml}" escapeXml="false"/>
</c:if>

  </tiles:putAttribute>
</tiles:insertDefinition>
