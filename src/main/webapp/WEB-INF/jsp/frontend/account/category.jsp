<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="/WEB-INF/tlds/spring.tld" prefix="spring" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>

<tiles:insertDefinition name="${_template}" flush="true">
  <tiles:putAttribute name="content" type="string">

<c:if test="${gSiteConfig['gCUSTOMER_CATEGORY'] != null}">

<form:form commandName="customerForm" method="post">

<script type="text/javascript" src="javascript/mootools-1.2.5-core.js"></script>
<script language="JavaScript" type="text/JavaScript">
<!--
function updateCategory(id, selected) {
		var request = new Request.JSON({
			url: "json.jhtm?action=category&bid=<c:out value="${gSiteConfig['gCUSTOMER_CATEGORY']}"/>&cid=" + selected,
			onRequest: function() { 
				document.getElementById(id + "_spinner").style.display="block";
			},
			onComplete: function(jsonObj) {
				document.getElementById(id + "_spinner").style.display="none";
				var categoryIds = document.getElementById(id + "_categoryIds");
				if (jsonObj.subcats.length > 0) {
					while (categoryIds.length > 0) {
    					categoryIds.remove(0);
					} 		
					i = 0;
					jsonObj.subcats.each(function(category) {
						categoryIds.options[i++] = new Option(category.name, category.id);
					});				
					var parentId = document.getElementById(id + "_parentId");
					while (parentId.length > 1) {
	    				parentId.remove(1);
					} 
					if (jsonObj.tree.length > 0) {		
						i = 1;
						jsonObj.tree.each(function(category) {
							parentId.options[i++] = new Option(category.name, category.id, false, true);
						});				
					}
				}
			}
		}).send();
}
//-->
</script>

<fieldset class="register_fieldset">
<legend>Industry</legend>
<table border="0" cellpadding="3" cellspacing="1">
  <c:forEach items="${customerForm.parentIds}" var="parentId" varStatus="status">
  <tr>
    <td align="right">&nbsp;<fmt:message key="category" /> : </td>
    <td>
      <table>
      <tr><td>
	    <select name="parentIds" id="cat${status.index}_parentId" style="width:300px;" onchange="updateCategory('cat${status.index}', this.value)">
	      <c:forEach items="${model.tree[status.index]}" var="category" varStatus="treeStatus">
	      <c:if test="${treeStatus.first and category.id != gSiteConfig['gCUSTOMER_CATEGORY']}">
  	      <option value="<c:out value="${gSiteConfig['gCUSTOMER_CATEGORY']}"/>"><fmt:message key="categoryMain" /></option>
	      </c:if>
  	      <option value="${category.id}" <c:if test="${category.id == parentId}">selected</c:if>>
  	        <c:choose>
  	          <c:when test="${treeStatus.first and category.id == gSiteConfig['gCUSTOMER_CATEGORY']}"><fmt:message key="categoryMain" /></c:when>
  	          <c:otherwise><c:out value="${category.name}"/></c:otherwise>
  	        </c:choose>
  	      </option>
	      </c:forEach>
	    </select>
	  </td><td>
	    <img id="cat${status.index}_spinner" src="assets/Image/Layout/spinner.gif" style="display:none">
	  </td></tr>
	  </table>
    </td>
  </tr>   
  <tr>
    <td align="right">&nbsp;</td>
    <td>
	  <select name="cat${status.index}_categoryIds" id="cat${status.index}_categoryIds" size="5" style="width:300px;" onchange="updateCategory('cat${status.index}', this.value)">	  
	    <c:forEach items="${model.categories[status.index]}" var="category">
  	    <option value="${category.id}" <c:if test="${category.id == customerForm.categoryIds[status.index]}">selected</c:if>><c:out value="${category.name}"/></option>
	    </c:forEach>
	  </select>
    </td>
  </tr>  
  </c:forEach>
</table>
</fieldset>

<div align="center">
  <input type="submit" value="<spring:message code="saveChanges"/>">
  <input type="submit" value="<spring:message code="cancel"/>" name="_cancel">
</div>

</form:form>

</c:if>

  </tiles:putAttribute>
</tiles:insertDefinition>
