<%@ page session="false" %>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page session="false" %>


<tiles:insertDefinition name="${_template}" flush="true">
  <c:if test="${_leftBar != '1' and _leftBar != '4'}">
    <tiles:putAttribute name="leftbar" value="/WEB-INF/jsp/frontend/layout/template6/leftbar.jsp" />
  </c:if>
  <tiles:putAttribute name="content" type="string">


<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<script type="text/javascript" src="/dv/liquidatenow/Scripts/jquery.js"></script>
<script type="text/javascript" src="/dv/liquidatenow/Scripts/cufon-yui.js"></script>
<script type="text/javascript" src="/dv/liquidatenow/Scripts/font2.js"></script>
<script type="text/javascript" src="/dv/liquidatenow/Scripts/scripts.js"></script>
<link href='http://fonts.googleapis.com/css?family=Roboto:400,500,700' rel='stylesheet' type='text/css'>
<link rel="stylesheet" type="text/css" href="/dv/Scripts/bootstrap-daterangepicker/bootstrap.min.css"/>
<link rel="stylesheet" type="text/css" href="/dv/Scripts/bootstrap-daterangepicker/daterangepicker-bs3.css"/>
<script type="text/javascript" src="/dv/Scripts/bootstrap-daterangepicker/bootstrap.min.js"></script>
<script type="text/javascript" src="/dv/Scripts/bootstrap-daterangepicker/moment.min.js"></script>
<script type="text/javascript" src="/dv/Scripts/bootstrap-daterangepicker/daterangepicker.js"></script>
<script type="text/javascript" src="/dv/Scripts/tipsy/jquery.tipsy.js"></script>
<link rel="stylesheet" type="text/css" href="/dv/Scripts/tipsy/tipsy.css"/>
<script type="text/javascript">
var typingTimer, QueryResultperPage, main_ajax_call, PageTitle;
var d = new Date(), m = d.getMonth() - d.getMonth() % 3;
$(document).ready(function() {
	$('#ProductSearchButton').click(function() {
		ProductResult($('#ProductSearchButton .QuerySort').val(),1);
	})
	$('#AccountSearchButton').click(function() {
		AccountResult($('#AccountSearchButton .QuerySort').val(),1);
	})
	$('#reportrange').daterangepicker(
		{
			ranges: {
			   'Today': [moment(), moment()],
			   'Yesterday': [moment().subtract('days', 1), moment()],
			   'Week to Date': [moment().startOf('week'), moment()],
			   'Last 14 Days': [moment().subtract('days', 13), moment()],
			   'Month to Date': [moment().startOf('month'), moment()],
			   'Quarter to Date': [moment(new Date(d.getFullYear(), m, 1)), moment()],
			   'Year to Date': [moment().startOf('year'), moment()],
			   '2 Years': [moment().subtract('years', 1).startOf('year'), moment()],
			   '3 Years': [moment().subtract('years', 2).startOf('year'), moment()],
			   '4 Years': [moment().subtract('years', 3).startOf('year'), moment()],
			   '5 Years': [moment().subtract('years', 4).startOf('year'), moment()]
			},
			startDate: moment().startOf('year'),
			endDate: moment()
		},
		function(start, end) {
			$('#ProductSummary .DateRange').val(start.format('YYYY-MM-DD') + ' to ' + end.format('YYYY-MM-DD'));
		}
	);	
	ProductResult('',1,0);
	AccountResult('',1,0);
});
function ProductResult(querysort,page,ResultperPage) {
	if ($('#ProductSummaryForm').is(':disabled')) {
		main_ajax_call.abort();
	}
	var LoadTime = new Date().getTime();
	$('#ProductSummaryForm').fadeTo('fast', 0.5);
	$('#ProductSummaryForm').attr('disabled', 'disabled');
	if (querysort == "") querysort = $('#ProductSummaryForm .QuerySort').val();
	ResultperPage = typeof ResultperPage !== 'undefined' ? ResultperPage : $('#ProductSummaryForm .QueryResultperPage').val();
	
	main_ajax_call = $.get('/dv/orderreportnewdb/lnowproductsummary.php', { Function: 'QueryResult', View: 'Dashboard', DateRange: $('#ProductSummary .DateRange').val(), QuerySort: querysort, QueryPage: page,  QueryResultperPage: ResultperPage, id: ${model.id} }, function(data){
		$('#ProductSummaryForm').html(data);
		$('#ProductSummaryForm').removeAttr('disabled');
		$('#ProductSummaryForm').fadeTo('fast', 1);
	});
}

function AccountResult(querysort,page,ResultperPage) {
	if ($('#AccountSummaryForm').is(':disabled')) {
		main_ajax_call.abort();
	}
	var LoadTime = new Date().getTime();
	$('#AccountSummaryForm').fadeTo('fast', 0.5);
	$('#AccountSummaryForm').attr('disabled', 'disabled');
	querysort = typeof querysort !== 'undefined' ? querysort : $('#AccountSummaryForm .QuerySort').val();
	ResultperPage = typeof ResultperPage !== 'undefined' ? ResultperPage : $('#AccountSummaryForm .QueryResultperPage').val();
	
	main_ajax_call = $.get('/dv/orderreportnewdb/lnowaccountsummary.php', { Function: 'QueryResult', View: 'Dashboard', DateRange: $('#AccountSummary .DateRange').val(), QuerySort: querysort, QueryPage: page,  QueryResultperPage: ResultperPage, id: ${model.id}, DateSummary: $('#DateSummary').val() }, function(data){
		$('#AccountSummaryForm').html(data);
		$('#AccountSummaryForm').removeAttr('disabled');
		$('#AccountSummaryForm').fadeTo('fast', 1);
		$('#SalesGraph').attr('src','/dv/orderreportnewdb/lnowaccountsummary.php?Function=Graph&DateRange='+$('#AccountSummary .DateRange').val()+'&id=${model.id}&DateSummary='+$('#DateSummary').val());
	});
}

</script>
<style type="text/css">
#TotalCredit {
	font-size: 16px;
	font-weight: bold;
	float: right;
}
.centerautowidth {
	display:inline-block;
}
input[type="button"] {
  background: #3498db;
  background-image: -webkit-linear-gradient(top, #3498db, #2980b9);
  background-image: -moz-linear-gradient(top, #3498db, #2980b9);
  background-image: -ms-linear-gradient(top, #3498db, #2980b9);
  background-image: -o-linear-gradient(top, #3498db, #2980b9);
  background-image: linear-gradient(to bottom, #3498db, #2980b9);
  -webkit-border-radius: 10;
  -moz-border-radius: 10;
  border-radius: 10px;
  -webkit-box-shadow: 0px 1px 3px #666666;
  -moz-box-shadow: 0px 1px 3px #666666;
  box-shadow: 0px 1px 3px #666666;
  font-family: Arial;
  color: #ffffff;
  font-size: 16px;
  padding: 10px 20px 10px 20px;
  border: solid #1f628d 2px;
  text-decoration: none;
}
input[type="button"] {
  background: #3cb0fd;
  background-image: -webkit-linear-gradient(top, #3cb0fd, #3498db);
  background-image: -moz-linear-gradient(top, #3cb0fd, #3498db);
  background-image: -ms-linear-gradient(top, #3cb0fd, #3498db);
  background-image: -o-linear-gradient(top, #3cb0fd, #3498db);
  background-image: linear-gradient(to bottom, #3cb0fd, #3498db);
  text-decoration: none;
}
/* Luis */
.Report {
	/*border: 2px solid #F0A132;*/
	background-color: #fff;
	border: solid 1px #eee;
	padding: 1%;
}
body {
	background-color: #f9f9f9;
}
table {
	width: 100%;
}
.btn-dash{
	height: 30px !important;
	line-height: 20px !important;
	padding: 0 !important;
	width: 60px !important;
	font-weight: bold !important;
	text-transform: uppercase !important;
	color: #fff !important;
	border-radius: 4px !important;
	border: none !important;
	box-shadow: none !important;
	right: 0px !important;
}
#total-links{
	list-style:none !important;
	padding-left:0 !important;
	font-size:14px;
}
#total-links li{
	margin-bottom: 10px;
	background-image: none;
}
.total-credit{
	font-weight: bold;	
}
</style><!-- InstanceEndEditable -->
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-48084701-2', 'liquidatenow.com');
  ga('send', 'pageview');

</script></head>

<body>

  <div id="Content"><!-- InstanceBeginEditable name="Body" -->
    <div id="DashboardHeader"></div>
    <table border="0" align="center" cellpadding="5" cellspacing="10">
      <tr>
        <td valign="top" class="Report">
            <img src="/dv/liquidatenow/innerimages/mainlogo.png" width="229" height="134" style="margin-bottom:10px;" />
            <!--<div id="TotalCredit">Total Credit: $0</div>-->
            <ul id="total-links">
                <li><a href="${_contextpath}/account_products.jhtm"><fmt:message key="f_viewMyProducts" /></a></li>
                <li><a href="${_contextpath}/consignment_sales.jhtm"><fmt:message key="f_viewMySales"/></a></li>
                <li><a href="${_contextpath}/consignment_report.jhtm"><fmt:message key="f_viewMyReport"/></a></li>
                <li><a href="${_contextpath}/dv/orderreportnewdb/lnowproductsummary.php?id=${model.id}"><fmt:message key="f_salesBySku"/></a></li>
                <li><a href="${_contextpath}/dv/orderreportnewdb/lnowaccountsummary.php?id=${model.id}"><fmt:message key="f_totalSales"/></a></li>
                <li><a href="${_contextpath}/account.jhtm"><fmt:message key="f_backToCustomerAccount"/></a></li>
                <li class="total-credit"><fmt:message key="f_vbaBalance" />:&nbsp;<fmt:message key="${siteConfig['CURRENCY'].value}" /><fmt:formatNumber value="${model.customer.credit}" pattern="#,###.##"/></li>                      
                <li><a href="${_contextpath}/credit_history.jhtm"><fmt:message key="f_viewMyBalance"/></a></li>
            </ul>
        </td>
        <td class="Report" id="ProductSummary"><h2>Sales by SKU</h2>
          <table border="0" align="center" cellpadding="10" cellspacing="0">
            <tr>
              <td>Date Range:</td>
              <td><div id="reportrange"> <i class="fa fa-calendar fa-lg"></i>
                <c:set var="now" value="<%=new java.util.Date()%>" />
                <input type="text" class="DateRange" value="2015-01-01 to <fmt:formatDate value="${now}" pattern="yyyy-MM-dd"/>" size="30" />
                <b class="caret"></b></div></td>
              <td><input type="button" name="ProductSearchButton" class="btn-dash SearchButton" id="ProductSearchButton" value="Go" /></td>
            </tr>
          </table>
          <table border="0" align="center" cellpadding="10" cellspacing="0">
        <tr>
          <td id="ProductSummaryForm"><img src="/dv/images/timerpreloader.gif" width="26" height="32" /></td>
        </tr>
      </table>
      </tr>
        <tr>
          <td valign="top" class="Report" id="AccountSummary"><h2>Total Sales</h2>
            <table border="0" align="center" cellpadding="10" cellspacing="0">
              <tr>
                <td align="right">Years:</td>
                <td><label>
                
                
	        <select name="DateRange" class="DateRange">
	           <option value="2015-01-01 to <fmt:formatDate value="${now}" pattern="yyyy-MM-dd"/>">1</option>
                <option value="2014-01-01 to <fmt:formatDate value="${now}" pattern="yyyy-MM-dd"/>">2</option>
                <option value="2013-01-01 to <fmt:formatDate value="${now}" pattern="yyyy-MM-dd"/>">3</option>
                <option value="2012-01-01 to <fmt:formatDate value="${now}" pattern="yyyy-MM-dd"/>">4</option>
                <option value="2011-01-01 to <fmt:formatDate value="${now}" pattern="yyyy-MM-dd"/>">5</option>
	          </select>
	        </label></td>
                <td rowspan="2" valign="bottom"><input type="button" name="AccountSearchButton" id="AccountSearchButton" class="btn-dash SearchButton" value="Go" /></td>
              </tr>
              <tr>
                <td align="right">View By:</td>
                <td><label>
                  <select name="DateSummary" id="DateSummary">
                    <option>Month</option>
                    <option>Quarter</option>
                    <option>Year</option>
                  </select>
                </label></td>
              </tr>
          </table>
          <table border="0" align="center" cellpadding="10" cellspacing="0">
        <tr>
          <td id="AccountSummaryForm"><img src="/dv/images/timerpreloader.gif" width="26" height="32" /></td>
        </tr>
      </table>
          <td valign="top" class="Report" id="AccountSummary"><h2>Total Sales</h2>
          <img name="" src="" width="100%" height="264" alt="" id="SalesGraph" /></td>
        </tr>
      </table>
</body>

<c:if test="${fn:trim(model.myaccountLayout.footerHtml) != ''}">
  <c:set value="${model.myaccountLayout.footerHtml}" var="footerHtml"/>
  <c:set value="${fn:replace(footerHtml, '#firstname#', userSession.firstName)}" var="footerHtml"/>
  <c:set value="${fn:replace(footerHtml, '#lastname#', userSession.lastName)}" var="footerHtml"/>
  <c:set value="${fn:replace(footerHtml, '#email#', userSession.username)}" var="footerHtml"/>
  <c:set value="${fn:replace(footerHtml, '#accountnumber#', model.customer.accountNumber)}" var="footerHtml"/>
  <c:if test="${gSiteConfig['gSALES_REP']}">
    <c:set value="${fn:replace(footerHtml, '#salesRepName#', model.salesRep.name)}" var="footerHtml"/>
    <c:set value="${fn:replace(footerHtml, '#salesRepEmail#', model.salesRep.email)}" var="footerHtml"/>
    <c:set value="${fn:replace(footerHtml, '#salesRepPhone#', model.salesRep.phone)}" var="footerHtml"/>
    <c:set value="${fn:replace(footerHtml, '#salesRepCellPhone#', model.salesRep.cellPhone)}" var="footerHtml"/>
    <c:set value="${fn:replace(footerHtml, '#salesRepAddress1#', model.salesRep.address.addr1)}" var="footerHtml"/>
    <c:set value="${fn:replace(footerHtml, '#salesRepAddress2#', model.salesRep.address.addr2)}" var="footerHtml"/>
    <c:set value="${fn:replace(footerHtml, '#salesRepCity#', model.salesRep.address.city)}" var="footerHtml"/>
    <c:set value="${fn:replace(footerHtml, '#salesRepCountry#', model.salesRep.address.country)}" var="footerHtml"/>
    <c:set value="${fn:replace(footerHtml, '#salesRepStateProvince#', model.salesRep.address.stateProvince)}" var="footerHtml"/>
    <c:set value="${fn:replace(footerHtml, '#salesRepZip#', model.salesRep.address.zip)}" var="footerHtml"/>
    <c:set value="${fn:replace(footerHtml, '#salesRepFax#', model.salesRep.address.fax)}" var="footerHtml"/>
    <c:set value="${fn:replace(footerHtml, '#salesRepCompany#', model.salesRep.address.company)}" var="footerHtml"/>
  </c:if>
  <c:out value="${footerHtml}" escapeXml="false"/>
</c:if>

  </tiles:putAttribute>
</tiles:insertDefinition>
