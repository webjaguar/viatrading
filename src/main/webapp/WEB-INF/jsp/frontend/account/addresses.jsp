<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ taglib uri="/WEB-INF/tlds/spring.tld" prefix="spring" %>

<tiles:insertDefinition name="${_template}" flush="true">
  <tiles:putAttribute name="content" type="string">
  
<c:if test="${fn:trim(model.myAddressLayout.headerHtml) != ''}">
  <c:set value="${model.myAddressLayout.headerHtml}" var="headerHtml"/>
  <c:set value="${fn:replace(headerHtml, '#firstname#', userSession.firstName)}" var="headerHtml"/>
  <c:set value="${fn:replace(headerHtml, '#lastname#', userSession.lastName)}" var="headerHtml"/>
  <c:set value="${fn:replace(headerHtml, '#email#', userSession.username)}" var="headerHtml"/>
  <c:set value="${fn:replace(headerHtml, '#accountnumber#', model.customer.accountNumber)}" var="headerHtml"/>
  <c:out value="${headerHtml}" escapeXml="false"/>
</c:if>


<form method="post">
<table width="90%" align="center" cellspacing="5" cellpadding="0" border="0">
  <tr>
    <c:if test="${siteConfig['EDIT_ADDRESS_ON_FRONTEND'].value == 'true'}" >
	  <td><input type="submit" name="__add" value="<spring:message code="addressAdd" />"></td>
	</c:if>
	<td>
<div class="pageNavi"><fmt:message key="page" /> 
		<select name="page" onchange="submit()">
		<c:forEach begin="1" end="${model.addresses.pageCount}" var="page">
  	        <option value="${page}" <c:if test="${page == (model.addresses.page+1)}">selected</c:if>>${page}</option>
		</c:forEach>
		</select>
<fmt:message key="of" />  <c:out value="${model.addresses.pageCount}"/></div>
	</td>
  </tr>
  <tr>
	<td colspan="2">
<table border="0" cellpadding="3" cellspacing="0" width="100%" class="listings">
  <tr><td colspan="2" class="addressBook"><fmt:message key="addressBook" /></td></tr>
<c:forEach items="${model.addresses.pageList}" var="address" varStatus="status">
  <tr class="row${status.index % 2}">
    <c:if test="${siteConfig['EDIT_ADDRESS_ON_FRONTEND'].value == 'true'}" >
	  <td class="nameCol"><a href="account_address.jhtm?id=${address.id}" class="nameLink"><fmt:message key="edit" /></a></td>
	</c:if>
    <td class="nameCol"><b><c:out value="${address.firstName}"/> <c:out value="${address.lastName}"/></b>
    <c:if test="${address.id == model.defaultAddress.id}"><span class="primary_address_small">(<fmt:message key="primaryAddress" />primary address)</span></c:if><br>
<c:if test="${address.company != ''}"><c:out value="${address.company}"/>,</c:if>
<c:out value="${address.addr1}"/><c:if test="${!empty address.addr2}"> <c:out value="${address.addr2}"/></c:if>,
<c:out value="${address.city}"/>, 
<c:out value="${address.stateProvince}"/> <c:out value="${address.zip}"/>,
<c:out value="${address.country}"/><br></td>			
  </tr>
</c:forEach>
<c:if test="${model.addresses.nrOfElements == 0}">
  <tr class="emptyList"><td colspan="2">&nbsp;</td></tr>
</c:if>
</table>
	</td>
  </tr>
</table>
</form>

<c:if test="${fn:trim(model.myAddressLayout.footerHtml) != ''}">
  <c:set value="${model.myAddressLayout.footerHtml}" var="footerHtml"/>
  <c:set value="${fn:replace(footerHtml, '#firstname#', userSession.firstName)}" var="footerHtml"/>
  <c:set value="${fn:replace(footerHtml, '#lastname#', userSession.lastName)}" var="footerHtml"/>
  <c:set value="${fn:replace(footerHtml, '#email#', userSession.username)}" var="footerHtml"/>
  <c:set value="${fn:replace(footerHtml, '#accountnumber#', model.customer.accountNumber)}" var="footerHtml"/>
  <c:out value="${footerHtml}" escapeXml="false"/>
</c:if>

  </tiles:putAttribute>
</tiles:insertDefinition>
