<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>

<tiles:insertDefinition name="${_template}" flush="true">
  <tiles:putAttribute name="content" type="string">

<c:if test="${gSiteConfig['gMYLIST']}">
<c:if test="${model.groupList.nrOfElements > 0}">
<script language="JavaScript">
<!--
function toggleAll(el) {
	var ids = document.getElementsByName("__selected_id");	
  	if (ids.length == 1)
      document.mylist.__selected_id.checked = el.checked;
    else
      for (i = 0; i < ids.length; i++)
        document.mylist.__selected_id[i].checked = el.checked;	
}  

function deleteSelected() {
	var ids = document.getElementsByName("__selected_id");	
  	if (ids.length == 1) {
      if (document.mylist.__selected_id.checked) {
    	return true;
      }
    } else {
      for (i = 0; i < ids.length; i++) {
        if (document.mylist.__selected_id[i].checked) {
    	  return true;
        }
      }
    }

    alert("Please select product(s) to delete.");       
    return false;
}
//-->
</script>
</c:if>
<form name="mylist" method="post">
<table width="90%" align="center" cellspacing="5" cellpadding="0" border="0">
  <tr>
	<td>&nbsp;</td>
	<td>
<div class="pageNavi">Page 
		<select name="page" onchange="submit()">
		<c:forEach begin="1" end="${model.groupList.pageCount}" var="page">
  	        <option value="${page}" <c:if test="${page == (model.groupList.page+1)}">selected</c:if>>${page}</option>
		</c:forEach>
		</select>
of <c:out value="${model.groupList.pageCount}"/></div>
	</td>
  </tr>
  <tr>
	<td colspan="2">
<table border="0" cellpadding="3" cellspacing="1" width="100%" class="listings"> 
  <tr class="listingsHdr2">
   <td><input type="text" name="_groupname" size="20" /></td>
   <td><input type="image" value="Add Group" name="_addGroup" src="assets/Image/Layout/button_addGroup.gif" ></td>
  </tr>
  <tr class="listingsHdr2">
  <c:set var="cols" value="0"/>
<c:if test="${model.groupList.nrOfElements > 0}">
	<c:set var="cols" value="${cols+1}"/>
	<td align="left" class="nameCol"><input type="checkbox" onclick="toggleAll(this)"></td>
</c:if>
    <td class="nameCol" colspan="${2+cols}">Name</td>
  </tr>
<c:forEach items="${model.groupList.pageList}" var="myListGroup" varStatus="status">
  <tr class="row${status.index % 2}">
<c:if test="${model.groupList.nrOfElements > 0}">
    <td align="left" class="nameCol"><input name="__selected_id" value="${myListGroup.id}" type="checkbox"></td>
</c:if>
    <td class="nameCol">
     <a href="viewList.jhtm?gid=${myListGroup.id}" class="nameLink"><c:out value="${myListGroup.name}" /></a>
    </td>
  </tr>
</c:forEach>
<c:if test="${model.groupList.nrOfElements == 0}">
  <tr class="emptyList"><td colspan="${2+cols}">&nbsp;</td></tr>
</c:if>
</table>
	</td>
  </tr>
<c:if test="${model.groupList.nrOfElements > 0}">
  <tr>
	<td><input type="submit" name="__delete_group" value="Delete Selected Group" onClick="return deleteSelected()"></td>
  </tr>
</c:if>
</table>
</form>

</c:if>

  </tiles:putAttribute>
</tiles:insertDefinition>
