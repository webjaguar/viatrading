<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<tiles:insertDefinition name="${_template}" flush="true">
  <tiles:putAttribute name="content" type="string">

<c:out value="${model.echoSignError}" />
<c:out value="${model.creationResult.errorCode}" />

<c:if test="${model.widget != null}">

<c:out value="${model.widget.javascript}" escapeXml="false" /> 
<c:out value="${model.widget.url}" escapeXml="true" /> 


</c:if>


  </tiles:putAttribute>
</tiles:insertDefinition>
