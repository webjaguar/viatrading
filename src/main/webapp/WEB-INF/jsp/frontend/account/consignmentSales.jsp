<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>


<script src="/dv/w3data.js"></script>
<script type='text/javascript' src='/wp-includes/js/jquery/jquery.js?ver=1.12.4'></script>
<script type='text/javascript' src='/wp-content/themes/bridge/js/plugins.js?ver=4.6.1'></script>
<script type='text/javascript' src='/wp-content/themes/bridge/js/default.min.js?ver=4.6.1'></script>


<tiles:insertDefinition name="${_template}" flush="true">
  <tiles:putAttribute name="content" type="string">
  
<c:out value="${model.viewMySalesLayout.headerHtml}" escapeXml="false"/>
<c:if test="${gSiteConfig['gCUSTOMER_SUPPLIER'] and model.supplierId != null }">    

<link rel="stylesheet" type="text/css" media="all" href="javascript/jscalendar-1.0/skins/aqua/theme.css" title="Aqua" /> <!-- calendar stylesheet -->
<script type="text/javascript" src="javascript/jscalendar-1.0/calendar.js"></script> <!-- main calendar program -->
<script type="text/javascript" src="javascript/jscalendar-1.0/lang/calendar-en.js"></script>
<script type="text/javascript" src="javascript/jscalendar-1.0/calendar-setup.js"></script>


<!--
<form>
<table width="90%" align="center" cellspacing="5" cellpadding="0" border="0">
  <tr>
	<td>
	<div class="pageNavi">Page 
		<select name="page" onchange="submit()">
		<c:forEach begin="1" end="${model.salesList.pageCount}" var="page">
  	        <option value="${page}" <c:if test="${page == (model.salesList.page+1)}">selected</c:if>>${page}</option>
		</c:forEach>
		</select>
		of <c:out value="${model.salesList.pageCount}"/></div>
	</td>
  </tr>
   <tr>
	<td>
	<div class="pageNavi">
	  	<fmt:message key='startDate' />:
		<input style="width:90px;margin-right:5px;" name="startDate" id="consignmentSalesSearch.startDate" type="text" value="<fmt:formatDate type="date" pattern="MM/dd/yyyy" value='${consignmentSalesSearch.startDate}'/>"  size="11" />
		<script type="text/javascript">	
		    Calendar.setup({
		        inputField     :    "consignmentSalesSearch.startDate",   // id of the input field
		        showsTime      :    false,
		        ifFormat       :    "%m/%d/%Y",   // format of the input field
		        button         :    "consignmentSalesSearch.startDate"   // trigger for the calendar (button ID)
		    });
        </script>
	  	<fmt:message key='endDate' />:
		<input style="width:90px;margin-right:5px;" name="endDate" id="consignmentSalesSearch.endDate" type="text" value="<fmt:formatDate type="date" pattern="MM/dd/yyyy" value='${consignmentSalesSearch.endDate}'/>"  size="11" />
		<script type="text/javascript">	
		    Calendar.setup({
		        inputField     :    "consignmentSalesSearch.endDate",   // id of the input field
		        showsTime      :    false,
		        ifFormat       :    "%m/%d/%Y",   // format of the input field
		        button         :    "consignmentSalesSearch.endDate"   // trigger for the calendar (button ID)
		    });
        </script> 	
		<input type="submit" value="<fmt:message key="search" />" name="_go">
	</div>
	</td>
  </tr>
  <tr>
	<td colspan="2">
<table border="0" cellpadding="3" cellspacing="1" width="100%" class="listings"> 
 <tr class="listingsHdr2">
	<td align="center" class="listingsHdr3"><fmt:message key="productSku" /></td>    
	<td align="center" class="listingsHdr3"><fmt:message key="productName" /></td>
	<td align="center" class="listingsHdr3"><fmt:message key="payRate" /></td>
	<td align="center" class="listingsHdr3"><fmt:message key="note" /></td>
	<td align="center" class="listingsHdr3"><fmt:message key="quantity"/></td>
	<td align="center" class="listingsHdr3"><fmt:message key="sales"/></td>
	<td align="center" class="listingsHdr3"><fmt:message key="mySales"/></td>
  </tr>
<c:forEach items="${model.salesList.pageList}" var="product" varStatus="status">
  <tr class="row${status.index % 2}">
	<td class="numberCol" align="center" style="white-space: nowrap"><c:out value="${product.productSku}"/></td>
    <td class="numberCol" align="center" style="white-space: nowrap"><c:out value="${product.productName}" /><%--<a href="product.jhtm?id=${product.id}" 
	  			class="nameLink"><c:out value="${product.productName}" /></a> --%></td>
	<td class="numberCol" align="center" style="white-space: nowrap">
		<c:choose>
		<c:when test="${product.costPercent == true}">
			<fmt:formatNumber value="${product.cost}" pattern="#,##0.00" />%
		</c:when>
		<c:otherwise>
			<fmt:message key="${siteConfig['CURRENCY'].value}"/><fmt:formatNumber value="${product.cost}" pattern="#,##0.00" />
		</c:otherwise>
		</c:choose>
    </td>
	<td class="numberCol" align="center" style="white-space: nowrap"><c:out value="${product.note}"/></td>
	<td class="numberCol" align="center" style="white-space: nowrap"><c:out value="${product.quantity}"/><c:set value="${product.quantity + totalQuantity}" var="totalQuantity"/></td>
	<td class="numberCol" align="center" style="white-space: nowrap">
	<c:choose>
		<c:when test="${product.costPercent == true}">
			<fmt:message key="${siteConfig['CURRENCY'].value}"/><fmt:formatNumber value="${product.totalSales}" pattern="#,##0.00" />
		</c:when>
		<c:otherwise>
			n/a
		</c:otherwise>
	</c:choose></td>
	<td class="numberCol" align="center" style="white-space: nowrap">
	<fmt:message key="${siteConfig['CURRENCY'].value}"/><fmt:formatNumber value="${product.myTotalSales}"  pattern="#,##0.00"/><c:set value="${product.myTotalSales + myTotalSales}" var="myTotalSales"/></td>	
  </tr>
</c:forEach>
<tr bgcolor="#FFFFFF"> <%--Empty Row--%>
	<%-- make this Field Configurable --%>
	<td colspan="7"><c:out value="${product.field_40 }"/></td>
</tr>
<tr style="color:#FFFFFF;font-weight:bold">
   <td class="nameCol" colspan="4"  align="left" ><fmt:message key="total" /></td>
   <td class="nameCol" align="center"><c:out value="${totalQuantity}"/></td>
   <td class="nameCol"></td>
   <td class="nameCol" align="center"><fmt:message key="${siteConfig['CURRENCY'].value}"/><fmt:formatNumber value="${myTotalSales}" pattern="#,##0.00" /></td>
 </tr>
</table>
	</td>
  </tr>   
  <tr>
  <td class="error"><fmt:message key="note" />: <fmt:message key="f_consignmentMessage"/></td>
  </tr> 
</table>
</form>
-->
</c:if>

		<div id="accountSettingsWrapper">
		<div class="row">
			<div class="col-sm-12">
				<h2></h2>
			</div>
			<div class="col-sm-6">
				<div id="account_Manager" class="accountSettingsSection">
					<h1><font size="48" color="#4682B4"><i><b><fmt:message key="f_my" /></b></i></font><font size="48" color="#FFA812"><i><b><fmt:message key="f_sales" /></b></i></font></h1>					
				</div>
			</div>
			<div class="col-sm-6">
				<div id="account_Manager" class="accountSettingsSection" style="visibility: visible;">
				<div class="row">
					<div class="col-sm-12 col-md-6">
						<div class="accountSettingsLeft">
						</div>
					</div>
					<div class="col-sm-12 col-md-6">
						<div class="accountSettingsRight">
							<img src="assets/responsive/img/liquidatenow_logo.png" class="img-responsive liquidatenow_logo">
						</div>
					</div>
				</div>
				</div>
			</div>				
					
		</div>
		</div>


		<script type="text/javascript" src="/dv/liveapps/scripts.js.php"></script>
        <link rel="stylesheet" type="text/css" href="/dv/liveapps/myaccount.css">
	
		<div id="lnowsales">
		<script type="text/javascript">
		<!--
			$(document).ready(function() {
				lnowsales(0,0,${model.userId});
			});
		//-->
		</script>
		
		</div>



<c:out value="${model.viewMySalesLayout.footerHtml}" escapeXml="false"/>
  </tiles:putAttribute>
</tiles:insertDefinition>
