<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>

<tiles:insertDefinition name="${_template}" flush="true">
  <tiles:putAttribute name="content" type="string">

<form:form  commandName="customer" method="post">
<table border="0" cellpadding="3" cellspacing="1" width="100%">
  <tr><td colspan="2" class="addressBook"><fmt:message key="company" /> <fmt:message key="description" /></td>
  </tr>
  <tr>
    <td align="right"><fmt:message key="shortDesc" />:</td>
    <td><form:input path="shortDesc" size="50" maxlength="255" />
		<form:errors path="shortDesc" cssClass="error"/></td>
  </tr>
    <tr>
      <td valign="top" align="right"><fmt:message key="longDesc" />:</td>
      <td><form:textarea rows="8" cols="50" path="longDesc" htmlEscape="true" /></td>       
    </tr>
</table>

<div align="center">
  <input type="submit" value="<fmt:message key="saveChanges"/>">
  <input type="submit" value="<fmt:message key="cancel"/>" name="_cancel">
</div>

</form:form>

  </tiles:putAttribute>
</tiles:insertDefinition>




