<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="/WEB-INF/tlds/spring.tld" prefix="spring" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>

<c:if test="${gSiteConfig['gSUB_ACCOUNTS']}">
<tiles:insertDefinition name="${_template}" flush="true">
  <tiles:putAttribute name="content" type="string">

<link href="assets/SmoothGallery/formMultiBox.css" rel="stylesheet" type="text/css"> 
<script type="text/javascript">
<!--
window.addEvent('domready', function(){
	var addCreditBox = new multiBox('mbAddCredit', {showControls : false, useOverlay: false, showNumbers: false });
});	
function creditValue(credit,userId) {
	document.getElementById('credit_'+userId).value = credit;
}	
function addCredit(userId) {
	var addCredit =  parseFloat($('credit_'+userId).value);
	var managerCredit = document.getElementById('managerCredit').value;
	
	if(addCredit > managerCredit) {
		alert("There are no sufficient funds to make the transfer.");
	}else {
		var request = new Request({
			url: "customer-ajax-credit.jhtm?userId="+userId+"&addCredit="+addCredit,
			method: 'post',
			onComplete: function(response) { 
				location.reload(true);
			}
		}).send();
	}
}
function deductCredit(userId) {
	var deductCredit = $('credit_'+userId).value;
	var request = new Request({
		url: "customer-ajax-credit.jhtm?userId="+userId+"&deductCredit="+deductCredit,
		method: 'post',
		onComplete: function(response) { 
			location.reload(true);
		}
	}).send();
}
//-->
</script>
<c:set value="${model.subAccountLayout.headerHtml}" var="headerHtml"/>
<c:set value="${fn:replace(headerHtml, '#credit#', model.customerCredit)}" var="headerHtml"/>

<c:out value="${headerHtml}" escapeXml="false"/>
 
<br><br><br>
<form action="account_subs.jhtm" id="list" method="post">
<input type="hidden" id="cid" name="cid"> 
<input type="hidden" id="sort" name="sort" value="${subAccountCustomerSearch.sort}" />
<input type="hidden" id="managerCredit" name="managerCredit" value="${model.customerCredit}"/>
<table width="90%" align="center" cellspacing="5" cellpadding="0" border="0">
  <tr>
	<td>
	<select name="parent" onChange="submit()">
		<option value=""><fmt:message key="subAccounts" /></option>
		<c:forEach items="${model.familyTree}" var="customer">
		<option value="${customer.id}"
		<c:if test="${customer.id == subAccountCustomerSearch['parent']}">selected</c:if>>
		<fmt:message key="subAccounts" /> <fmt:message key="of" /> <c:out value="${customer.username}" /> 
	  </option>
	</c:forEach>
	</select>	
	</td>
	<td>
<div class="pageNavi">Page 
		<select name="page" onchange="submit()">
		<c:forEach begin="1" end="${model.accounts.pageCount}" var="page">
  	        <option value="${page}" <c:if test="${page == (model.accounts.page+1)}">selected</c:if>>${page}</option>
		</c:forEach>
		</select>
of <c:out value="${model.accounts.pageCount}"/></div>
	</td>
  </tr>
  <tr>
	<td colspan="2">
<table border="0" cellpadding="3" cellspacing="1" width="100%" class="listings"> 
  <tr class="listingsHdr2">
    <c:set var="cols" value="0"/>
    <c:if test="${fn:contains(siteConfig['CUSTOMIZE_SUBACCOUNT_LIST'].value,'email')}">
      <c:choose>
   	      <c:when test="${subAccountCustomerSearch.sort == 'username desc'}">
   	        <td class="listingsHdr3">
   	        <a class="sortLink" href="#" onClick="document.getElementById('sort').value='username';document.getElementById('list').submit()"><fmt:message key="email" /></a>
   	        </td>
   	        <!--  <td><img src="../graphics/up.gif" border="0"></td>  -->
   	      </c:when>
   	      <c:when test="${subAccountCustomerSearch.sort == 'username'}">
   	        <td class="listingsHdr3">
   	        <a class="sortLink" href="#" onClick="document.getElementById('sort').value='username desc';document.getElementById('list').submit()"><fmt:message key="email" /></a>
   	        </td>
   	        <!--  <td><img src="../graphics/down.gif" border="0"></td>  -->
   	      </c:when>
   	      <c:otherwise>
   	        <td class="listingsHdr3">
   	        <a class="sortLink" href="#" onClick="document.getElementById('sort').value='username desc';document.getElementById('list').submit()"><fmt:message key="email" /></a>
   	        </td>
   	      </c:otherwise>
   	  </c:choose>  	  
  	</c:if>
  	<c:if test="${fn:contains(siteConfig['CUSTOMIZE_SUBACCOUNT_LIST'].value,'lastName')}">
      <c:choose>
   	      <c:when test="${subAccountCustomerSearch.sort == 'last_name desc'}">
   	        <td class="listingsHdr3">
   	        <a class="sortLink" href="#" onClick="document.getElementById('sort').value='last_name';document.getElementById('list').submit()"><fmt:message key="lastName" /></a>
   	        </td>
   	        <!--  <td><img src="../graphics/up.gif" border="0"></td>  -->
   	      </c:when>
   	      <c:when test="${subAccountCustomerSearch.sort == 'last_name'}">
   	        <td class="listingsHdr3">
   	        <a class="sortLink" href="#" onClick="document.getElementById('sort').value='last_name desc';document.getElementById('list').submit()"><fmt:message key="lastName" /></a>
   	        </td>
   	        <!--  <td><img src="../graphics/down.gif" border="0"></td>  -->
   	      </c:when>
   	      <c:otherwise>
   	        <td class="listingsHdr3">
   	        <a class="sortLink" href="#" onClick="document.getElementById('sort').value='last_name desc';document.getElementById('list').submit()"><fmt:message key="lastName" /></a>
   	        </td>
   	      </c:otherwise>
   	  </c:choose>  	  
  	</c:if>
  	<c:if test="${fn:contains(siteConfig['CUSTOMIZE_SUBACCOUNT_LIST'].value,'firstName')}">
      <c:choose>
   	      <c:when test="${subAccountCustomerSearch.sort == 'first_name desc'}">
   	        <td class="listingsHdr3">
   	        <a class="sortLink" href="#" onClick="document.getElementById('sort').value='first_name';document.getElementById('list').submit()"><fmt:message key="firstName" /></a>
   	        </td>
   	        <!--  <td><img src="../graphics/up.gif" border="0"></td>  -->
   	      </c:when>
   	      <c:when test="${subAccountCustomerSearch.sort == 'first_name'}">
   	        <td class="listingsHdr3">
   	        <a class="sortLink" href="#" onClick="document.getElementById('sort').value='first_name desc';document.getElementById('list').submit()"><fmt:message key="firstName" /></a>
   	        </td>
   	        <!--  <td><img src="../graphics/down.gif" border="0"></td>  -->
   	      </c:when>
   	      <c:otherwise>
   	        <td class="listingsHdr3">
   	        <a class="sortLink" href="#" onClick="document.getElementById('sort').value='first_name desc';document.getElementById('list').submit()"><fmt:message key="firstName" /></a>
   	        </td>
   	      </c:otherwise>
   	  </c:choose>  	  
  	</c:if>
  	<c:if test="${fn:contains(siteConfig['CUSTOMIZE_SUBACCOUNT_LIST'].value,'accountNumber')}">
  	  <c:choose>
   	      <c:when test="${subAccountCustomerSearch.sort == 'account_number desc'}">
   	        <td class="listingsHdr3">
   	        <a class="hdrLink" href="#" onClick="document.getElementById('sort').value='account_number';document.getElementById('list').submit()"><fmt:message key="accountNumber" /></a>
   	        </td>
   	        <!--  <td><img src="../graphics/up.gif" border="0"></td>  -->
   	      </c:when>
   	      <c:when test="${subAccountCustomerSearch.sort == 'account_number'}">
   	        <td class="listingsHdr3">
   	        <a class="hdrLink" href="#" onClick="document.getElementById('sort').value='account_number desc';document.getElementById('list').submit()"><fmt:message key="accountNumber" /></a>
   	        </td>
   	        <!--  <td><img src="../graphics/down.gif" border="0"></td>  -->
   	      </c:when>
   	      <c:otherwise>
   	        <td class="listingsHdr3">
   	        <a class="hdrLink" href="#" onClick="document.getElementById('sort').value='account_number desc';document.getElementById('list').submit()"><fmt:message key="accountNumber" /></a>
   	        </td>
   	      </c:otherwise>
   	  </c:choose> 
  	</c:if>
  	<c:if test="${fn:contains(siteConfig['CUSTOMIZE_SUBACCOUNT_LIST'].value,'companyName')}">
  	  <c:choose>
   	      <c:when test="${subAccountCustomerSearch.sort == 'company desc'}">
   	        <td class="listingsHdr3">
   	        <a class="hdrLink" href="#" onClick="document.getElementById('sort').value='company';document.getElementById('list').submit()"><fmt:message key="companyName" /></a>
   	        </td>
   	        <!--  <td><img src="../graphics/up.gif" border="0"></td>  -->
   	      </c:when>
   	      <c:when test="${subAccountCustomerSearch.sort == 'company'}">
   	        <td class="listingsHdr3">
   	        <a class="hdrLink" href="#" onClick="document.getElementById('sort').value='company desc';document.getElementById('list').submit()"><fmt:message key="companyName" /></a>
   	        </td>
   	        <!--  <td><img src="../graphics/down.gif" border="0"></td>  -->
   	      </c:when>
   	      <c:otherwise>
   	        <td class="listingsHdr3">
   	        <a class="hdrLink" href="#" onClick="document.getElementById('sort').value='company desc';document.getElementById('list').submit()"><fmt:message key="companyName" /></a>
   	        </td>
   	      </c:otherwise>
   	  </c:choose>  	  
  	</c:if>
  	<c:if test="${gSiteConfig['gSHOPPING_CART']}">
  	<c:if test="${fn:contains(siteConfig['CUSTOMIZE_SUBACCOUNT_LIST'].value,'orders')}">
  	  <td class="nameCol"><fmt:message key="orderCount" /></td>
  	  <c:set var="cols" value="${cols+1}"/>
  	</c:if>
  	<c:if test="${fn:contains(siteConfig['CUSTOMIZE_SUBACCOUNT_LIST'].value,'orderTotal')}">
  	  <td class="nameCol"><fmt:message key="orderTotal" /></td>
  	  <c:set var="cols" value="${cols+1}"/>
	</c:if>	
	</c:if>
	<c:if test="${fn:contains(siteConfig['CUSTOMIZE_SUBACCOUNT_LIST'].value,'subAccounts')}">
  	  <td class="nameCol"><fmt:message key="subAccounts" /></td>
  	  <td class="nameCol"><fmt:message key="credit" /> <fmt:message key="available" /></td>
  	</c:if>
  	<c:if test="${fn:contains(siteConfig['CUSTOMIZE_SUBACCOUNT_LIST'].value,'quote')}">
  	  <td class="nameCol"><fmt:message key="quote" /></td>
  	  <c:set var="cols" value="${cols+1}"/>
  	</c:if>
  	<c:if test="${siteConfig['SUBACCOUNT_LOGINAS_FRONTEND'].value == 'true' and fn:contains(siteConfig['CUSTOMIZE_SUBACCOUNT_LIST'].value,'loginAs')}">
  	  <c:set var="cols" value="${cols+1}"/>
 	  <td class="nameCol">&nbsp;</td>
  	</c:if>
  	<c:if test="${gSiteConfig['gBUDGET_BRAND']}">
  	<c:if test="${fn:contains(siteConfig['CUSTOMIZE_SUBACCOUNT_LIST'].value,'brand')}">
  	<c:set var="cols" value="${cols+1}"/>
 	<td class="nameCol">&nbsp;</td>
	</c:if>
	</c:if>
  </tr>
<c:forEach items="${model.accounts.pageList}" var="account" varStatus="status">
  <tr class="row${status.index % 2}">
    <c:if test="${fn:contains(siteConfig['CUSTOMIZE_SUBACCOUNT_LIST'].value,'email')}">
      <td class="nameCol"><c:out value="${account.username}"/></td>			
    </c:if>
    <c:if test="${fn:contains(siteConfig['CUSTOMIZE_SUBACCOUNT_LIST'].value,'lastName')}">
      <td class="nameCol"><c:out value="${account.address.lastName}"/></td>
    </c:if>
    <c:if test="${fn:contains(siteConfig['CUSTOMIZE_SUBACCOUNT_LIST'].value,'firstName')}">  	
      <td class="nameCol"><c:out value="${account.address.firstName}"/></td>
    </c:if> 
    <c:if test="${fn:contains(siteConfig['CUSTOMIZE_SUBACCOUNT_LIST'].value,'accountNumber')}">  	
      <td class="nameCol"><c:out value="${account.accountNumber}"/></td>
    </c:if> 
    <c:if test="${fn:contains(siteConfig['CUSTOMIZE_SUBACCOUNT_LIST'].value,'companyName')}">  	
      <td class="nameCol"><c:out value="${account.address.company}"/></td>
    </c:if>  
    <c:if test="${gSiteConfig['gSHOPPING_CART']}">
    <c:if test="${fn:contains(siteConfig['CUSTOMIZE_SUBACCOUNT_LIST'].value,'orders')}">
      <td align="center"><c:out value="${account.orderCount}"/></td>
    </c:if>
    <c:if test="${fn:contains(siteConfig['CUSTOMIZE_SUBACCOUNT_LIST'].value,'orderTotal')}">
	<td align="right"><c:if test="${account.ordersGrandTotal != null}">&nbsp;</c:if>
		<c:if test="${account.ordersGrandTotal != null}"><a href="account_orders.jhtm?id=${account.id}" style="color:blue;"><fmt:message key="${siteConfig['CURRENCY'].value}" /><fmt:formatNumber value="${account.ordersGrandTotal}" pattern="#,##0.00" /></a></c:if></td>
	</c:if>
	</c:if>
	<c:if test="${fn:contains(siteConfig['CUSTOMIZE_SUBACCOUNT_LIST'].value,'subAccounts')}">
      <td align="center">
    	<c:if test="${account.subCount > 0}"><a href="account_subs.jhtm?parent=${account.id}" style="color:blue;"><c:out value="${account.subCount}"/></a></c:if>
    	<c:if test="${account.subCount == 0}">0</c:if>
      </td>	
      <td align="center">
      	<c:if test="${gSiteConfig['gBUDGET']}">      	
          	<a href="#${account.id}" rel="type:element" id="mb${account.id}" class="mbAddCredit"><fmt:message key="${siteConfig['CURRENCY'].value}" /><fmt:formatNumber value="${account.credit}" pattern="#,##0.00" /></a>
          	<div id="${account.id}" class="miniWindowWrapper addCreditBox">
          	<div class="header"><fmt:message key="add"/> <fmt:message key="credit"/></div>
   	  		<fieldset class="field top">
   	    		<label class="AStitle"><fmt:message key="customer"/> <fmt:message key="name"/></label>
   	    		<input name="name" type="text" value="<c:out value="${account.address.firstName}"/> <c:out value="${account.address.lastName}"/>" disabled="disabled"/>
   	  		</fieldset>
   	  		<fieldset class="field">
   	    		<label class="AStitle"><fmt:message key="credit"/> <fmt:message key="available"/></label>
   	    		<input name="creditAvailable" type="text" value="<fmt:formatNumber value="${account.credit}" pattern="#,##0.00" />" disabled="disabled"/>
   	  		</fieldset>
   	  		<fieldset class="field bottom">
   	    		<label class="AStitle"><fmt:message key="credit"/></label>
   	    		<input name="__credit_" type="text" id="credit_${account.id}" onkeyup="creditValue(this.value, ${account.id});"/> 
   	  		</fieldset>
   	  		<div class="button">
				<input type="submit" value="Add"  name="__add_credit_" onclick="addCredit('${account.id}');"/>
				<%-- If we provide 'Deduct' then we need to add more code to put back the amount to the account manager's credit.
				<c:if test="${account.credit > 0}">
					<input type="submit" value="Deduct"  name="__deduct_credit_" onclick="deductCredit('${account.id}');"/>
				</c:if>
				 --%>
				</div>
   	  		</div>
        </c:if>
      </td>	
    </c:if>
    <c:if test="${fn:contains(siteConfig['CUSTOMIZE_SUBACCOUNT_LIST'].value,'quote')}">
     <td align="right"><c:if test="${account.ordersGrandTotal != null}">&nbsp;</c:if>
		<a href="account_orders.jhtm?getQuoteList=true&id=${account.id}" style="color:blue;">Quotes</a>
	 </td>
	</c:if>
    <c:if test="${siteConfig['SUBACCOUNT_LOGINAS_FRONTEND'].value == 'true' and fn:contains(siteConfig['CUSTOMIZE_SUBACCOUNT_LIST'].value,'loginAs')}">
  	  <td align="center"><a href="loginAsCustomer.jhtm?cid=${account.token}" target="_blank"><fmt:message key="loginAsCustomer" /></a></td>
  	</c:if>
	<c:if test="${gSiteConfig['gBUDGET_BRAND']}">
	<c:if test="${fn:contains(siteConfig['CUSTOMIZE_SUBACCOUNT_LIST'].value,'brand')}">
	  <td align="center"><a href="reportByBrands.jhtm?id=${account.id}"><fmt:message key="reportByBrands" /></a></td>	  
	</c:if>
	</c:if>
  </tr>
</c:forEach>
<c:if test="${model.accounts.nrOfElements == 0}">
  <tr class="emptyList"><td colspan="${4+cols}">&nbsp;</td></tr>
</c:if>
</table>
	</td>
  </tr>
</table>
</form>

<c:out value="${model.subAccountLayout.footerHtml}" escapeXml="false"/>

  </tiles:putAttribute>
</tiles:insertDefinition>
</c:if>