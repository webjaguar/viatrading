<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="/WEB-INF/tlds/spring.tld" prefix="spring" %>

<tiles:insertDefinition name="${_template}" flush="true">
  <tiles:putAttribute name="content" type="string">

<spring:hasBindErrors name="addressForm">
<span class="error"><fmt:message key="form.hasErrors" /></span>
</spring:hasBindErrors>

<form method="post">
<table border="0" cellpadding="3" cellspacing="1" width="100%" class="accountEdit">
  <c:choose>
   <c:when test="${siteConfig['EDIT_EMAIL_ON_FRONTEND'].value == 'true'}">
    <tr><td colspan="2" class="addressBook"><fmt:message key="emailAddressAndPassword"/></td></tr>
   </c:when>
   <c:otherwise>
    <tr><td colspan="2" class="addressBook"><fmt:message key="password"/></td></tr>
   </c:otherwise>
  </c:choose>
  <c:if test="${siteConfig['EDIT_EMAIL_ON_FRONTEND'].value == 'true'}" >
  <tr>
    <td class="formNameReq"><fmt:message key="emailAddress"/>:</td>
    <td>
      <spring:bind path="customerForm.customer.username">
      <input type="text" name="<c:out value="${status.expression}"/>" value="<c:out value="${status.value}"/>"/>
      <span class="error"><c:out value="${status.errorMessage}"/></span>
      </spring:bind>
    </td>
  </tr>
  </c:if>
  <c:if test="${siteConfig['CURRENT_PASSWORD_CHECK'].value == 'true'}">
	  <tr>
	    <td class="formNameReq"><fmt:message key="currentPassword"/>:</td>
	    <td>
	      <spring:bind path="customerForm.currentPassword">
	      <input type="password" name="<c:out value="${status.expression}"/>" value="<c:out value="${status.value}"/>"/>
	      <span class="error"><c:out value="${status.errorMessage}" escapeXml="false"/></span>
	      </spring:bind>
	    </td>
	  </tr>
  </c:if>
  <tr>
    <td class="formName"><fmt:message key="newPassword"/>:</td>
    <td>
      <spring:bind path="customerForm.customer.password">
      <input type="password" name="<c:out value="${status.expression}"/>" value="<c:out value="${status.value}"/>"/>
      <span class="error"><c:out value="${status.errorMessage}"/></span>
      </spring:bind>
    </td>
  </tr>
  <tr>
    <td class="formName"><fmt:message key="confirmPassword"/>:</td>
    <td>
      <spring:bind path="customerForm.confirmPassword">
      <input type="password" name="<c:out value="${status.expression}"/>" value="<c:out value="${status.value}"/>"/>
      <span class="error"><c:out value="${status.errorMessage}"/></span>
      </spring:bind>
    </td>
  </tr>
</table>

<div align="center">
  <input type="submit" value="<spring:message code="saveChanges"/>">
  <input type="submit" value="<spring:message code="cancel"/>" name="_cancel">
</div>

</form>

  </tiles:putAttribute>
</tiles:insertDefinition>
