<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="/WEB-INF/tlds/spring.tld" prefix="spring" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>

<tiles:insertDefinition name="${_template}" flush="true">
  <tiles:putAttribute name="content" type="string">

<c:if test="${gSiteConfig['gBUDGET_PRODUCT']}">

<form method="post">
<table width="90%" align="center" cellspacing="5" cellpadding="0" border="0">
<c:if test="${model.subAccount != null}">
  <tr>
  	<td colspan="2">
  	  <table cellspacing="0" cellpadding="0" border="0" class="listingsHdr1">
  	    <tr>
  	      <td><fmt:message key="subAccount" />:</td>
  	      <td>&nbsp;&nbsp;</td>  	      
  	      <td><c:out value="${model.subAccount.username}"/></td>
  	    </tr>
  	    <c:if test="${model.subAccount.address.company != null and model.subAccount.address.company != ''}">
  	    <tr>
  	      <td><fmt:message key="company" />:</td>
  	      <td>&nbsp;&nbsp;</td>  	     
  	      <td><c:out value="${model.subAccount.address.company}"/></td>
  	    </tr>
  	    </c:if>
  	    <tr>
  	      <td colspan="2">&nbsp;</td>  	      
  	      <td><c:out value="${model.subAccount.address.firstName}"/> <c:out value="${model.subAccount.address.lastName}"/></td>
  	    </tr>
  	  </table>  	      
  	</td>
  </tr>
  <tr><td colspan="2">&nbsp;</td></tr>
</c:if>
  <tr>
  	<td class="listingsHdr1"><fmt:message key="reportByProducts" /></td>
	<td align="right">
		<select name="year" onchange="submit()">
		<c:forEach items="${model.years}" var="year">
        	<option value="${year}" <c:if test="${model.orderSearch.year == year}">selected</c:if>><c:out value="${year}"/></option>
    	</c:forEach>
		</select>
	</td>
  </tr>
  <tr>
	<td colspan="2">
<table border="0" cellpadding="3" cellspacing="1" width="100%" class="listings"> 
  <tr class="listingsHdr2">
  	<td class="nameCol"><fmt:message key="orderDate" /></td>
  	<td class="nameCol"><fmt:message key="invoice" /></td>
  	<c:set var="colspan" value="0"/>
    <c:forEach items="${model.budgetProducts}" var="budgetProduct">
      <c:set var="colspan" value="${colspan + 1}"/>
	  <td align="center"><c:out value="${budgetProduct.product.sku}"/></td>
    </c:forEach>
    <td class="nameCol"><fmt:message key="shipping" /></td>
  </tr>
<c:set var="shippingTotal" value="0.0" />  
<c:forEach items="${model.orders}" var="order" varStatus="status">
  <tr class="row${status.index % 2}">
    <td class="nameCol"><fmt:formatDate type="date" timeStyle="default" value="${order.dateOrdered}"/></td>			
	<td class="nameCol"><a href="invoice.jhtm?order=${order.orderId}" class="nameLink"><c:out value="${order.orderId}"/></a></td>
    <c:forEach items="${model.budgetProducts}" var="budgetProduct">
	  <td align="center">
	    <c:if test="${model.budgetProductMap[order.orderId][budgetProduct] != null}">
	      <c:out value="${model.budgetProductMap[order.orderId][budgetProduct]}" />
	    </c:if>
	    <c:if test="${model.budgetProductMap[order.orderId][budgetProduct] == null}">-</c:if>
	  </td>
    </c:forEach>
	  <td align="right">
	    <c:if test="${order.shippingCost != null}">
	    <c:set var="shippingTotal" value="${order.shippingCost + shippingTotal}" />
        <fmt:message key="${siteConfig['CURRENCY'].value}" /><fmt:formatNumber value="${order.shippingCost}" pattern="#,##0.00" />
        </c:if>
        <c:if test="${order.shippingCost == null}">-</c:if>
	  </td>
  </tr>
</c:forEach>
<c:if test="${empty model.orders}">
  <tr>
    <td colspan="${3 + colspan}" class="row0">&nbsp;</td>
  </tr>
</c:if>
  <tr class="listingsHdr2">
  	<td colspan="2" align="center"><fmt:message key="total" /></td>
    <c:forEach items="${model.budgetProducts}" var="budgetProduct">
	<td align="center">
      <c:out value="${budgetProduct.ordered}"/>
	</td>
    </c:forEach>
	  <td align="right">
        <fmt:message key="${siteConfig['CURRENCY'].value}" /><fmt:formatNumber value="${shippingTotal}" pattern="#,##0.00" />
	  </td>
  </tr>
  <tr class="listingsHdr2">
  	<td colspan="2" align="center"><fmt:message key="yearlyBudget" /></td>
    <c:forEach items="${model.budgetProducts}" var="budgetProduct">
	<td align="center">
      <c:out value="${budgetProduct.qtyTotal}"/>
	</td>
	</c:forEach>
    <td>&nbsp;</td>
  </tr>
  <tr class="listingsHdr2">
  	<td colspan="2" align="center"><fmt:message key="balance" /></td>
    <c:forEach items="${model.budgetProducts}" var="budgetProduct">
	<td align="center">
      <c:out value="${budgetProduct.balance}"/>
	</td>
    </c:forEach>
    <td>&nbsp;</td>
  </tr>
</table>
	</td>
  </tr>
</table>
</form>

</c:if>

  </tiles:putAttribute>
</tiles:insertDefinition>
