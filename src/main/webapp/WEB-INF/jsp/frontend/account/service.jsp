<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ taglib uri="/WEB-INF/tlds/custom.tld" prefix="wj" %>

<tiles:insertDefinition name="${_template}" flush="true">
  <tiles:putAttribute name="content" type="string">
<%--Spring3 DONE --%>          
<c:if test="${model.message != null}">
  <div class="message"><fmt:message key="${model.message}" /></div>
</c:if>

<c:if test="${model.service != null}">
<fieldset class="register_fieldset">
<legend><fmt:message key="serviceRequest" /></legend>
<table border="0" cellpadding="3">
  <tr>
    <td align="right"><fmt:message key="serviceRequest" /> #:</td>
    <td><b><c:out value="${model.service.serviceNum}"/></b></td>
  </tr>
  <tr>
    <td align="right"><fmt:message key="service"/> <fmt:message key="type" />:</td>
    <td><c:out value="${model.service.type}"/></td>
  </tr>
  <tr>
    <td align="right"><fmt:message key="reportDate" />:</td>
    <td><fmt:formatDate type="date" timeStyle="default" value="${model.service.reportDate}"/></td>
  </tr>
  <tr>
    <td align="right"><fmt:message key="status" />:</td>
    <td><c:out value="${model.service.status}"/></td>
  </tr>
  <c:if test="${model.service.completeDate != null}">
  <tr>
    <td align="right"><fmt:message key="completeDate" />:</td>
    <td><fmt:formatDate type="date" timeStyle="default" value="${model.service.completeDate}"/></td>
  </tr>
  </c:if>
  <tr>
    <td align="right"><c:out value="${siteConfig['SERVICEABLE_ITEM_TITLE'].value}"/> ID:</td>
    <td><c:out value="${model.service.item.itemId}"/></td>
  </tr>
  <tr>
    <td align="right"><fmt:message key="S/N" />:</td>
    <td><c:out value="${model.service.item.serialNum}"/></td>
  </tr>
  <c:if test="${model.service.item.address != null}">
  <tr>
	<td valign="top" align="right"><c:out value="${siteConfig['SERVICEABLE_ITEM_TITLE'].value}"/> <fmt:message key="location" />:</td>	
	<td>
	<c:if test="${model.service.item.address.company != ''}">
<c:out value="${model.service.item.address.company}"/><br/>
</c:if>
<c:out value="${model.service.item.address.firstName}"/> <c:out value="${model.service.item.address.lastName}"/><br/>
<c:out value="${model.service.item.address.addr1}"/> <c:out value="${model.service.item.address.addr2}"/><br/>
<c:out value="${model.service.item.address.city}"/>, <c:out value="${model.service.item.address.stateProvince}"/> <c:out value="${model.service.item.address.zip}"/><br/>
<c:out value="${model.countries[model.service.item.address.country]}"/><br/>
<c:if test="${model.service.item.address.phone != ''}">
<fmt:message key="f_phone" />: <c:out value="${model.service.item.address.phone}"/><br/>
</c:if>
<fmt:message key="f_fax" />: <c:out value="${model.service.item.address.fax}"/>
	</td>
  </tr>
  </c:if>
  <tr>
    <td align="right"><fmt:message key="sku" />:</td>
    <td><c:out value="${model.service.item.sku}"/></td>
  </tr>
  <tr>
    <td align="right"><fmt:message key="description" />:</td>
    <td><c:out value="${model.product.name}"/></td>
  </tr>
<c:forEach items="${model.product.productFields}" var="productField" varStatus="status">
  <tr>
    <td align="right"><c:out value="${productField.name}"/>:</td>
    <c:choose>
		<c:when test="${productField.formatType != null and productField.formatType != '' and productField.fieldType == 'number' }">
			<td class="details_field_value_row${row.index % 2}">
			<c:catch var="catchFormatterException">
				<fmt:formatNumber value="${wj:trimAndSplit(productField.value, '|' , false, ',', true, true)}" pattern="${productField.formatType}" />
			</c:catch>
			<c:if test = "${catchFormatterException != null}"><c:out value="${wj:trimAndSplit(productField.value, '|' , false, ',', true, true)}" escapeXml="false"/></c:if>
			</td>
		</c:when>
		<c:otherwise>
			<td class="details_field_value_row${row.index % 2}"><c:out value="${wj:trimAndSplit(productField.value, '|' , false, ',', true, true)}" escapeXml="false"/></td>
		</c:otherwise>
	</c:choose>      
  </tr>
</c:forEach>
  <tr>
    <td valign="top" align="right"><fmt:message key="problem" />:</td>
    <td><c:out value="${model.service.problem}"/></td>
  </tr>
  <tr>
    <td valign="top" align="right"><fmt:message key="comments" />:</td>
    <td> <c:set var="serviceComments" value="${model.service.comments}"/>
         <c:if test="${model.service.comments != null and model.service.comments != ''}">
           ${fn:replace(model.service.comments,"\\n", "<br>")}
         </c:if>    
    </td>
  </tr>
  <c:if test="${model.service.trackNumIn != null and model.service.trackNumIn != ''}">
  <tr>
    <td colspan="2">Inbound <fmt:message key="trackNum" /></td>
  </tr>
  <tr>
    <td align="right"><c:out value="${model.service.trackNumInCarrier}"/>:</td>
    <td>
	<c:choose>
	 <c:when test="${model.service.trackNumInCarrier == 'UPS'}">
	  <a href="http://wwwapps.ups.com/WebTracking/processInputRequest?HTMLVersion=5.0&sort_by=status&tracknums_displayed=10&TypeOfInquiryNumber=T&loc=en_US&InquiryNumber1=<c:out value='${model.service.trackNumIn}' />&AgreeToTermsAndConditions=yes"><c:out value="${model.service.trackNumIn}" /></a>
	 </c:when>
	 <c:when test="${model.service.trackNumInCarrier == 'FedEx'}">
	  <a href="http://fedex.com/Tracking?action=track&language=english&cntry_code=us&mps=y&tracknumbers=<c:out value='${model.service.trackNumIn}' />&ascend_header=1"><c:out value="${model.service.trackNumIn}" /></a>
	 </c:when>
	 <c:when test="${model.service.trackNumInCarrier == 'USPS'}">
	  <a href="http://trkcnfrm1.smi.usps.com/PTSInternetWeb/InterLabelInquiry.do?strOrigTrackNum=<c:out value='${model.service.trackNumIn}' />"><c:out value="${model.service.trackNumIn}" /></a>
	 </c:when>
	 <c:otherwise><c:out value="${model.service.trackNumIn}" /></c:otherwise>
	</c:choose>
	</td>
  </tr>
  </c:if>
  <c:if test="${model.service.trackNumOut != null and model.service.trackNumOut != ''}">
  <tr>
    <td colspan="2">Outbound <fmt:message key="trackNum" /></td>
  </tr>
  <tr>
    <td align="right"><c:out value="${model.service.trackNumOutCarrier}"/>:</td>
    <td>
	<c:choose>
	 <c:when test="${model.service.trackNumOutCarrier == 'UPS'}">
	  <a href="http://wwwapps.ups.com/WebTracking/processInputRequest?HTMLVersion=5.0&sort_by=status&tracknums_displayed=10&TypeOfInquiryNumber=T&loc=en_US&InquiryNumber1=<c:out value='${model.service.trackNumOut}' />&AgreeToTermsAndConditions=yes"><c:out value="${model.service.trackNumOut}" /></a>
	 </c:when>
	 <c:when test="${model.service.trackNumOutCarrier == 'FedEx'}">
	  <a href="http://fedex.com/Tracking?action=track&language=english&cntry_code=us&mps=y&tracknumbers=<c:out value='${model.service.trackNumOut}' />&ascend_header=1"><c:out value="${model.service.trackNumOut}" /></a>
	 </c:when>
	 <c:when test="${model.service.trackNumOutCarrier == 'USPS'}">
	  <a href="http://trkcnfrm1.smi.usps.com/PTSInternetWeb/InterLabelInquiry.do?strOrigTrackNum=<c:out value='${model.service.trackNumOut}' />"><c:out value="${model.service.trackNumOut}" /></a>
	 </c:when>
	 <c:otherwise><c:out value="${model.service.trackNumOut}" /></c:otherwise>
	</c:choose>
	</td>
  </tr>
  </c:if>
  <c:if test="${model.service.itemFrom != null and model.service.itemFrom != ''}">
  <tr>
    <td colspan="2">Printer From City, State, Zip</td>
  </tr>
  <tr>
    <td>&nbsp;</td>
    <td><c:out value="${model.service.itemFrom}"/></td>       
  </tr>
  </c:if>
  <c:if test="${model.service.itemTo != null and model.service.itemTo != ''}">
  <tr>
    <td colspan="2">Printer To City, State, Zip</td>
  </tr>
  <tr>
    <td>&nbsp;</td>
    <td><c:out value="${model.service.itemTo}"/></td>       
  </tr>
  </c:if>
  <c:if test="${model.service.shipDate != null}">
  <tr>
    <td align="right"><fmt:message key="shipDate" />:</td>
    <td><fmt:formatDate type="date" timeStyle="default" value="${model.service.shipDate}"/></td>      
  </tr>
  </c:if>
  <c:if test="${model.service.purchaseOrder != ''}">
  <tr>
    <td align="right"><fmt:message key="purchaseOrder" />:</td>
    <td><c:out value="${model.service.purchaseOrder}"/></td>      
  </tr>
  </c:if>  
  <c:if test="${model.service.wo3rd != ''}">
  <tr>
    <td align="right">3rd Party Work Order:</td>
    <td><c:out value="${model.service.wo3rd}"/></td>      
  </tr>
  </c:if>
</table>
</fieldset>
</c:if>

  </tiles:putAttribute>
</tiles:insertDefinition>
