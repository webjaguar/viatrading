<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ taglib uri="/WEB-INF/tlds/custom.tld" prefix="wj" %>

<tiles:insertDefinition name="${_template}" flush="true">
  <tiles:putAttribute name="content" type="string">

<c:if test="${gSiteConfig['gMYLIST']}">
<c:out value="${model.mylistLayout.headerHtml}" escapeXml="false"/>

<c:if test="${model.products.nrOfElements > 0}">
<script language="JavaScript">
<!--
function toggleAll(el) {
	var ids = document.getElementsByName("__selected_id");	
  	if (ids.length == 1)
      document.mylist.__selected_id.checked = el.checked;
    else
      for (i = 0; i < ids.length; i++)
        document.mylist.__selected_id[i].checked = el.checked;	
}  

function deleteSelected() {
	var ids = document.getElementsByName("__selected_id");	
  	if (ids.length == 1) {
      if (document.mylist.__selected_id.checked) {
    	return confirm('Delete permanently?');
      }
    } else {
      for (i = 0; i < ids.length; i++) {
        if (document.mylist.__selected_id[i].checked) {
    	  return confirm('Delete permanently?');
        }
      }
    }

    alert("Please select product(s) to delete.");       
    return false;
}

function addToCart(aform) {
    aform.action = 'addToCart.jhtm';
    submit();
}
//-->
</script>
</c:if>
<form name="mylist" method="post">
<table width="90%" align="center" class="myList" cellspacing="5" cellpadding="0" border="0">
  <tr>
	<td>&nbsp;</td>
	<td>
<div class="pageNavi">Page 
		<select name="page" onchange="submit()">
		<c:forEach begin="1" end="${model.products.pageCount}" var="page">
  	        <option value="${page}" <c:if test="${page == (model.products.page+1)}">selected</c:if>>${page}</option>
		</c:forEach>
		</select>
of <c:out value="${model.products.pageCount}"/></div>
	</td>
  </tr>
  <tr>
	<td colspan="2">
<table border="0" cellpadding="3" cellspacing="1" width="100%" class="listings"> 
  <tr class="listingsHdr2">
  <c:set var="cols" value="0"/>
<c:if test="${model.products.nrOfElements > 0}">
	<c:set var="cols" value="${cols+1}"/>
    <td align="center"><input type="checkbox" onclick="toggleAll(this)"></td>
</c:if>
	<c:if test="${fn:contains(siteConfig['CUSTOMIZE_MYLIST'].value,'sku')}"> 
      <c:set var="cols" value="${cols+1}"/>
      <th class="nameCol"><fmt:message key="f_sku"/></th>
    </c:if>
    <c:if test="${fn:contains(siteConfig['CUSTOMIZE_MYLIST'].value,'name')}"> 
      <c:set var="cols" value="${cols+1}"/>
      <td class="nameCol"><fmt:message key="f_name"/></td>
    </c:if>
    <c:if test="${fn:contains(siteConfig['CUSTOMIZE_MYLIST'].value,'shortDescription')}"> 
      <c:set var="cols" value="${cols+1}"/>
      <td class="nameCol"><fmt:message key="f_shortDescription"/></td>
	</c:if>
	<c:forEach items="${model.productFields}" var="productField">
      <c:if test="${productField.showOnMyList}">
        <th class="nameCol"><c:out value="${productField.name}" escapeXml="false" /></th>
      </c:if>
    </c:forEach>
	<c:if test="${fn:contains(siteConfig['CUSTOMIZE_MYLIST'].value,'price')}"> 
      <c:if test="${model.showPriceColumn == null or (model.showPriceColumn != null and model.showPriceColumn)}">    
	  <c:set var="cols" value="${cols+1}"/>
      <th class="nameCol"><fmt:message key="f_price"/></th>
      </c:if>
    </c:if>
    <c:if test="${fn:contains(siteConfig['CUSTOMIZE_MYLIST'].value,'dateAdded')}"> 
      <c:set var="cols" value="${cols+1}"/>
      <td class="nameCol" style="white-space: nowrap"><fmt:message key="f_added"/></td>
    </c:if>
    <c:if test="${fn:contains(siteConfig['CUSTOMIZE_MYLIST'].value,'quantity')}"> 
      <c:if test="${gSiteConfig['gSHOPPING_CART'] and model.showQtyColumn}">
      <c:set var="cols" value="${cols+1}"/>
      <th class="nameCol"><fmt:message key="f_quantity"/></th>
      </c:if>
    </c:if>
  </tr>
<c:forEach items="${model.products.pageList}" var="product" varStatus="status">
  <tr class="row${status.index % 2}">
<c:if test="${model.products.nrOfElements > 0}">
    <td align="center"><input name="__selected_id" value="${product.id}" type="checkbox"></td>
</c:if>
	<c:if test="${fn:contains(siteConfig['CUSTOMIZE_MYLIST'].value,'sku')}"> 
    <td class="nameCol">			
        <c:out value="${product.sku}"/>
	</td>			
	</c:if>
    <c:if test="${fn:contains(siteConfig['CUSTOMIZE_MYLIST'].value,'name')}"> 
    <td class="nameCol"><a href="product.jhtm?id=${product.id}" 
	  			class="nameLink"><c:out value="${product.name}" /></a></td>
	</c:if>
    <c:if test="${fn:contains(siteConfig['CUSTOMIZE_MYLIST'].value,'shortDescription')}"> 
    <td class="nameCol">			
        <c:out value="${product.shortDesc}"/>
	</td>		
	</c:if>
	<c:forEach items="${product.productFields}" var="productField">
	  <c:if test="${productField.showOnMyList}">  
	  <c:choose>
		<c:when test="${productField.formatType != null and productField.formatType != '' and productField.fieldType == 'number' }">
			<td align="center">
			<c:catch var="catchFormatterException">
				<fmt:formatNumber value="${wj:trimAndSplit(productField.value, '|' , false, ',', true, true)}" pattern="${productField.formatType}" />
			</c:catch>
			<c:if test = "${catchFormatterException != null}"><c:out value="${wj:trimAndSplit(productField.value, '|' , false, ',', true, true)}" escapeXml="false"/></c:if></td>
		</c:when>
		<c:otherwise>
			<td align="center"><c:out value="${wj:trimAndSplit(productField.value, '|' , false, ',', true, true)}" escapeXml="false"/></td>
		</c:otherwise>
	</c:choose>  
	  </c:if>
  	</c:forEach>
	<c:if test="${fn:contains(siteConfig['CUSTOMIZE_MYLIST'].value,'price')}"> 
    <c:if test="${model.showPriceColumn == null or (model.showPriceColumn != null and model.showPriceColumn)}">	
    <td width="30%" >
	<input type="hidden" name="product.id" value="${product.id}">
	<%@ include file="/WEB-INF/jsp/frontend/common/price.jsp" %>
	<%@ include file="/WEB-INF/jsp/frontend/common/productOptions.jsp" %>
    </td>
    </c:if>
    </c:if>
    <c:if test="${fn:contains(siteConfig['CUSTOMIZE_MYLIST'].value,'dateAdded')}"> 
    <td class="nameCol" style="white-space: nowrap"><fmt:formatDate type="date" timeStyle="default" value="${product.created}"/></td>	
    </c:if>
    <c:if test="${fn:contains(siteConfig['CUSTOMIZE_MYLIST'].value,'quantity')}"> 
    <c:if test="${gSiteConfig['gSHOPPING_CART'] and model.showQtyColumn}">
    <td width="15%" style="white-space: nowrap">
      <c:choose>
      <c:when test="${product.slaveCount!= null and product.slaveCount > 0 }">
      	<a href="product.jhtm?id=${product.id}" class="nameLink"><fmt:message key="f_clickToBuy"/></a>
      </c:when>
      <c:otherwise>      	
		  <c:if test="${gSiteConfig['gSHOPPING_CART'] and !empty product.price}">  
		  Qty: <input type="text" size="6" maxlength="5" name="quantity_${product.id}" id="quantity_${product.id}" value="<c:out value="${cartItem.quantity}"/>" ><c:if test="${product.packing != ''}"> <c:out value="${product.packing}"/> </c:if>
		  </c:if>&nbsp;
      </c:otherwise>
      </c:choose>
  	</td>
    </c:if>
    </c:if>
  </tr>
</c:forEach>
<c:choose>
 <c:when test="${model.products.nrOfElements == 0}">
   <tr class="emptyList"><td colspan="${cols}">&nbsp;</td></tr>
 </c:when>
 <c:otherwise>
   <tr>
	  <td></td>
	  <td>
	   <select name="_groupId">
	     <option value="0">Select Group</option>    
	    <c:forEach items="${model.groupList}" var="group">
	     <c:if test="${group.id != model.viewGroupId}">
		  <option value="${group.id}"><c:out value="${group.name}" /></option>
		 </c:if>    
	    </c:forEach>
	   </select>
	  </td>
	  <td><input type="image" value="Add To Group" name="_addToGroup" src="assets/Image/Layout/button_addToGroup.gif" ></td>
</tr>
 </c:otherwise>
</c:choose>
</table>
	</td>
  </tr>
<c:if test="${model.products.nrOfElements > 0}">
  <tr>
	<td><input type="submit" name="__delete" value="Delete Selected Products" onClick="return deleteSelected()"></td>
	<c:if test="${gSiteConfig['gSHOPPING_CART']}">
	<td align="right"><input type="image" value="Add to Cart" name="_addtocart_quick" class="_addtocart_quick" src="assets/Image/Layout/button_addtocart${_lang}.gif" onClick="addToCart(this.form);" ></td>
	</c:if>
  </tr>
</c:if>
</table>
</form>

<c:out value="${model.mylistLayout.footerHtml}" escapeXml="false"/>
</c:if>

  </tiles:putAttribute>
</tiles:insertDefinition>
