<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="/WEB-INF/tlds/spring.tld" prefix="spring" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>

<tiles:insertDefinition name="${_template}" flush="true">
  <tiles:putAttribute name="content" type="string">

<c:if test="${fn:trim(model.sugarSyncLayout.headerHtml) != ''}">
  <c:out value="${model.sugarSyncLayout.headerHtml}" escapeXml="false"/>
</c:if>

<c:set value="${model.accessToken}" var="accessToken"/>

<div id="fileWrapper">
<div class="listingsHdr1">My Files</div>
<div class="sugarSyncSearchKey">
<form id="searchKey" action="account_sugarSyncSearch.jhtm" method="get">
<input type="text" name="keyword" id="searchBox" value="<c:out value="${model.searchKeyWord}"/>"/> 
<input type="submit" id="searchButton" value="Search" />
</form>
</div>
<c:if test="${model.back != false}">
	<a class="hdrLink" href="account_sugarSync.jhtm?back=true">Back</a>
</c:if>
<ul class="files">
<c:forEach items="${model.subFolders}" var="folderName" varStatus="status">

	<c:set value="refText_${status.index}" var="refText"/>
	<c:set value="fileData_${status.index}" var="fileData"/>
	<c:set value="name_${status.index}" var="className"/>
	<c:choose >
	<c:when test="${fn:contains(folderName, '_fileName') }">
		<li class="fileName ${model[className]}"><a class="hdrLink" href="account_sugarSync.jhtm?fileDownload=true&fileName=${folderName}&fileData=${model[fileData]}&contentsUrl=${model[contentsUrl]}"> <c:out value="${fn:split(folderName, '_')[0]}"/></a></li>
	</c:when>
	<c:otherwise>
		<li class="folderName ${model[className]}"><a class="hdrLink" href="account_sugarSync.jhtm?sub=true&index=${status.index}&accessToken=${accessToken}&refText=${model[refText]}&contentsUrl=${model[contentsUrl]}"> <c:out value="${folderName}"/></a></li>
	</c:otherwise>
	</c:choose >
</c:forEach>
</ul>
</div>

<c:if test="${fn:trim(model.sugarSyncLayout.footerHtml) != ''}">
  <c:out value="${model.sugarSyncLayout.footerHtml}" escapeXml="false"/>
</c:if>

  </tiles:putAttribute>
</tiles:insertDefinition>