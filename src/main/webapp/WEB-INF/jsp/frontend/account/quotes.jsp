<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="/WEB-INF/tlds/spring.tld" prefix="spring" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>

<tiles:insertDefinition name="${_template}" flush="true">
  <tiles:putAttribute name="content" type="string">

<c:out value="${model.myQuoteHistoryLayout.headerHtml}" escapeXml="false"/>

<form method="post" id="list">
<input type="hidden" id="sort" name="sort" value="${orderSearch.sort}" /> 
<table width="90%" align="center" cellspacing="5" cellpadding="0" border="0">
<c:if test="${model.subAccount != null}">
  <tr>
  	<td colspan="2">
  	  <table cellspacing="0" cellpadding="0" border="0" class="listingsHdr1">
  	    <tr>
  	      <td><fmt:message key="subAccount" />:</td>
  	      <td>&nbsp;&nbsp;</td>  	      
  	      <td><c:out value="${model.subAccount.username}"/></td>
  	    </tr>
  	    <c:if test="${model.subAccount.address.company != null and model.subAccount.address.company != ''}">
  	    <tr>
  	      <td><fmt:message key="company" />:</td>
  	      <td>&nbsp;&nbsp;</td>  	     
  	      <td><c:out value="${model.subAccount.address.company}"/></td>
  	    </tr>
  	    </c:if>
  	    <tr>
  	      <td colspan="2">&nbsp;</td>  	      
  	      <td><c:out value="${model.subAccount.address.firstName}"/> <c:out value="${model.subAccount.address.lastName}"/></td>
  	    </tr>
  	  </table>  	      
  	</td>
  </tr>
  <tr><td colspan="2">&nbsp;</td></tr>
</c:if>
  <tr>
	<td>
<div class="pageNavi">Page 
		<select name="page" onchange="submit()">
		<c:forEach begin="1" end="${model.quotes.pageCount}" var="page">
  	        <option value="${page}" <c:if test="${page == (model.quotes.page+1)}">selected</c:if>>${page}</option>
		</c:forEach>
		</select>
of <c:out value="${model.quotes.pageCount}"/></div>
	</td>
  </tr>
  <tr>
	<td colspan="2">
<table border="0" cellpadding="3" cellspacing="1" width="100%" class="listings"> 
<!-- 
  <tr class="listingsHdr2">
    <c:set var="cols" value="0"/>
    <c:if test="${fn:contains(siteConfig['CUSTOMIZE_VIEW_MY_ORDER_HISTORY_LIST'].value,'orderDate')}">
  	  <td class="nameCol">&nbsp;</td>
  	</c:if>
  	<c:if test="${fn:contains(siteConfig['CUSTOMIZE_VIEW_MY_ORDER_HISTORY_LIST'].value,'invoice')}">
  	  <td><input name="orderNum" type="text" value="<c:out value='${model.orderSearch.orderNum}' />" size="10" /></td>
  	</c:if>
  	<c:if test="${fn:contains(siteConfig['CUSTOMIZE_ORDER_LIST'].value,'mas200')}">
  	  <td>&nbsp;</td>
  	</c:if>
  	<c:if test="${fn:contains(siteConfig['CUSTOMIZE_VIEW_MY_ORDER_HISTORY_LIST'].value,'po')}">
  	  <td><input name="purchaseOrder" type="text" value="<c:out value='${model.orderSearch.purchaseOrder}' />" size="10" /></td>
  	</c:if>
  	<c:if test="${fn:contains(siteConfig['CUSTOMIZE_VIEW_MY_ORDER_HISTORY_LIST'].value,'status') and gSiteConfig['gACCOUNTING'] != 'EVERGREEN'}">  
	    <td class="nameCol">
	      <c:if test="${not fn:contains(siteConfig['CUSTOMIZE_ORDER_LIST'].value,'mas200')}">
	      <select name="status">
		  	<option value=""><fmt:message key="allOrders" /></option>
		  	<option value="p" <c:if test="${model.orderSearch.status == 'p'}">selected</c:if>><fmt:message key="orderStatus_p" /></option>
	  	    <option value="pr" <c:if test="${model.orderSearch.status == 'pr'}">selected</c:if>><fmt:message key="orderStatus_pr" /></option>
	  	    <option value="s" <c:if test="${model.orderSearch.status == 's'}">selected</c:if>><fmt:message key="orderStatus_s" /></option>
	  	    <option value="x" <c:if test="${model.orderSearch.status == 'x'}">selected</c:if>><fmt:message key="orderStatus_x" /></option>
	      </select>
	      </c:if>
	    </td>
    </c:if>
      <c:if test="${fn:contains(siteConfig['CUSTOMIZE_VIEW_MY_ORDER_HISTORY_LIST'].value,'substatus') and siteConfig['SUB_STATUS_ON_FRONTEND'].value == 'true'}">
        <td>&nbsp;</td>
    </c:if>
    <c:if test="${gSiteConfig['gREORDER']}">
  	    <c:set var="cols" value="${cols+1}"/>
  	</c:if>
  	<c:if test="${gSiteConfig['gPAYMENTS']}">
        <c:set var="cols" value="${cols+1}"/>
    </c:if>
  	<td class="nameCol" colspan="${1+cols}" align="right"><input type="submit" value="Search" /></td>
  </tr>
   -->
  <tr class="listingsHdr2">
	  	<td>
		   	<table cellspacing="0" cellpadding="1">
		   	  <tr>
		   	    <c:choose>
				   	<c:when test ="${model.quoteSearch.sort == 'date_ordered desc' }">
					  	<td class="listingsHdr3">
					  	<a class="sortLink" href="#" onClick="document.getElementById('sort').value='0';document.getElementById('list').submit()"> <fmt:message key="quoteOrdered" /></a>
					  	</td>
					  	<td><img src="/assets/Image/Layout/up.gif" border="0"></td>
				  	</c:when>
				  	<c:when test ="${model.quoteSearch.sort == 'date_ordered' }">
					  	<td class="listingsHdr3">
					  	<a class="sortLink" href="#" onClick="document.getElementById('sort').value='1';document.getElementById('list').submit()"> <fmt:message key="quoteOrdered" /></a>				  	
					  	</td>
					  	<td><img src="/assets/Image/Layout/down.gif" border="0"></td>
				  	</c:when>
				  	<c:otherwise>
					  	<td class="listingsHdr3">
					  	<a class="sortLink" href="#" onClick="document.getElementById('sort').value='1';document.getElementById('list').submit()"> <fmt:message key="quoteOrdered" /></a>
					  	</td>	
					  	<td><img src="/assets/Image/Layout/down.gif" border="0"></td>			  
				  	</c:otherwise>
			  	</c:choose>
		   	  </tr>
		   	 </table>
	    </td>
  
  	<td class="nameCol">  	
  	  	  <fmt:message key="invoice" />  	  
  	</td>
	<td class="nameCol">
	  	<fmt:message key="total" />
	</td>
  </tr>
<c:forEach items="${model.quotes.pageList}" var="quote" varStatus="status">
  <tr class="row${status.index % 2}">
    <td class="nameCol"><fmt:formatDate type="date" timeStyle="default" value="${quote.dateOrdered}"/></td>	  	
	<td class="nameCol"><a href="invoice.jhtm?quote=${quote.orderId}" class="nameLink"><c:out value="${quote.orderId}"/></a>
		<c:if test="${quote.pdfUrl != null}">&nbsp;&nbsp;<a href="assets/pdf/<c:out value="${quote.pdfUrl}"/>" target="_blank"><img src="assets/Image/Layout/pdf.gif" border="0"/></a></c:if>
	</td> 
	<td class="nameCol" align="right"><fmt:message key="${siteConfig['CURRENCY'].value}" /><fmt:formatNumber value="${quote.grandTotal}" pattern="#,##0.00" /></td>
  </tr>
</c:forEach>

<c:if test="${model.quotes.nrOfElements == 0}">
  <tr class="emptyList"><td colspan="${5+cols}">&nbsp;</td></tr>
</c:if>
</table>

<table border="0" cellpadding="0" cellspacing="1" width="100%">
  <tr>
  <td align="right">
  <select name="size" onchange="submit()">
    <c:forTokens items="10,25,50" delims="," var="current">
  	  <option value="${current}" <c:if test="${current == model.quotes.pageSize}">selected</c:if>>${current} <fmt:message key="perPage" /></option>
    </c:forTokens>
  </select>
  </td>
  </tr>
</table>
	</td>
  </tr>
</table>
</form>

  <c:out value="${model.myQuoteHistoryLayout.footerHtml}" escapeXml="false"/>

  </tiles:putAttribute>
</tiles:insertDefinition>
