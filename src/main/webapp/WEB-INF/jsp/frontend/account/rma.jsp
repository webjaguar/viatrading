<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>

<tiles:insertDefinition name="${_template}" flush="true">
  <tiles:putAttribute name="content" type="string">
  
<c:if test="${gSiteConfig['gRMA']}">
  
<c:if test="${model.message != null}">
  <div class="message"><fmt:message key="${model.message}" /></div>
</c:if>

<c:if test="${model.rma != null}">
<fieldset class="register_fieldset">
<legend><fmt:message key="rmaRequest" /></legend>
<table border="0" cellpadding="3">
  <tr>
    <td align="right"><fmt:message key="rmaRequest" /> #:</td>
    <td><b><c:out value="${model.rma.rmaNum}"/></b></td>
  </tr>
  <tr>
    <td align="right"><fmt:message key="reportDate" />:</td>
    <td><fmt:formatDate type="date" timeStyle="default" value="${model.rma.reportDate}"/></td>
  </tr>
  <tr>
    <td align="right"><fmt:message key="status" />:</td>
    <td><c:out value="${model.rma.status}"/></td>
  </tr>
  <c:if test="${model.rma.completeDate != null}">
  <tr>
    <td align="right"><fmt:message key="completeDate" />:</td>
    <td><fmt:formatDate type="date" timeStyle="default" value="${model.rma.completeDate}"/></td>
  </tr>
  </c:if>
  <tr>
    <td align="right"><fmt:message key="order" /> #:</td>
    <td><c:out value="${model.rma.orderId}"/></td>
  </tr>
  <tr>
    <td align="right"><fmt:message key="S/N" />:</td>
    <td><c:out value="${model.rma.serialNum}"/></td>
  </tr>
  <tr>
    <td align="right"><fmt:message key="sku" />:</td>
    <td><c:out value="${model.rma.lineItem.product.sku}"/></td>
  </tr>
  <tr>
    <td align="right"><fmt:message key="description" />:</td>
    <td><c:out value="${model.rma.lineItem.product.name}"/></td>
  </tr>
  <tr>
    <td valign="top" align="right"><fmt:message key="action" />:</td>
    <td><c:out value="${model.rma.action}"/></td>
  </tr>
  <tr>
    <td valign="top" align="right"><fmt:message key="problem" />:</td>
    <td><c:out value="${model.rma.problem}"/></td>
  </tr>
  <tr>
    <td valign="top" align="right"><fmt:message key="comments" />:</td>
    <td><c:out value="${model.rma.comments}"/></td>
  </tr>
</table>
</fieldset>
</c:if>

</c:if>

  </tiles:putAttribute>
</tiles:insertDefinition>
