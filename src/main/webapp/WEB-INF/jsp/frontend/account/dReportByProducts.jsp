<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="/WEB-INF/tlds/spring.tld" prefix="spring" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>

<tiles:insertDefinition name="${_template}" flush="true">
  <tiles:putAttribute name="content" type="string">

<c:if test="${gSiteConfig['gBUDGET_PRODUCT']}">

<form method="post">
<table width="90%" align="center" cellspacing="5" cellpadding="0" border="0">
<c:if test="${model.subAccount != null}">
  <tr>
  	<td colspan="2">
  	  <table cellspacing="0" cellpadding="0" border="0" class="listingsHdr1">
  	    <tr>
  	      <td><fmt:message key="subAccount" />:</td>
  	      <td>&nbsp;&nbsp;</td>  	      
  	      <td><c:out value="${model.subAccount.username}"/></td>
  	    </tr>
  	    <c:if test="${model.subAccount.address.company != null and model.subAccount.address.company != ''}">
  	    <tr>
  	      <td><fmt:message key="company" />:</td>
  	      <td>&nbsp;&nbsp;</td>  	     
  	      <td><c:out value="${model.subAccount.address.company}"/></td>
  	    </tr>
  	    </c:if>
  	    <tr>
  	      <td colspan="2">&nbsp;</td>  	      
  	      <td><c:out value="${model.subAccount.address.firstName}"/> <c:out value="${model.subAccount.address.lastName}"/></td>
  	    </tr>
  	  </table>  	      
  	</td>
  </tr>
  <tr><td colspan="2">&nbsp;</td></tr>
</c:if>
  <tr>
  	<td class="listingsHdr1"><fmt:message key="dReportByProducts" /></td>
	<td align="right">
		<select name="year" onchange="submit()">
		  <c:forEach items="${model.years}" var="year">
      		<option value="${year}" <c:if test="${model.year == year}">selected</c:if>><c:out value="${year}"/></option>
  		  </c:forEach>
		</select>
	</td>
  </tr>
  <tr>
	<td colspan="2">
<table border="0" cellpadding="3" cellspacing="1" width="100%" class="listings"> 
  <tr class="listingsHdr2">
  	<td class="nameCol"><fmt:message key="salesRep" /></td>
    <c:forEach items="${model.budgetProducts}" var="budgetProduct">
	  <td align="center"><c:out value="${budgetProduct.product.sku}"/></td>
    </c:forEach>
  </tr>
<c:forEach items="${model.subAccounts}" var="subAccount" varStatus="status">
  <tr class="row${status.index % 2}">
	<td class="nameCol" style="white-space: nowrap"><a href="reportByProducts.jhtm?id=${subAccount.id}" class="nameLink"><c:out value="${subAccount.address.firstName}"/> <c:out value="${subAccount.address.lastName}"/></a>
		<c:if test="${subAccount.subCount > 0}">(<a href="dReportByProducts.jhtm?id=${subAccount.id}" class="nameLink">manager</a>)</c:if>
	</td>
    <c:forEach items="${model.budgetProducts}" var="budgetProduct">
	  <td align="center">
	    <c:if test="${model.subAccountMap[subAccount.id][budgetProduct.product.sku].ordered != null}">
	      <b><c:out value="${model.subAccountMap[subAccount.id][budgetProduct.product.sku].ordered}"/></b>
	    </c:if>
	    <c:if test="${model.subAccountMap[subAccount.id][budgetProduct.product.sku].ordered == null}">-</c:if>
	  </td>    
    </c:forEach>
  </tr>
  <tr class="row${status.index % 2}">
	<td class="nameCol" align="right"><fmt:message key="yearlyBudget" /></td>
    <c:forEach items="${model.budgetProducts}" var="budgetProduct">
	  <td align="center">
	    <c:if test="${model.subAccountMap[subAccount.id][budgetProduct.product.sku].qtyTotal != null}">
	      <b><c:out value="${model.subAccountMap[subAccount.id][budgetProduct.product.sku].qtyTotal}"/></b>
	    </c:if>
	    <c:if test="${model.subAccountMap[subAccount.id][budgetProduct.product.sku].qtyTotal == null}">-</c:if>
	  </td>    
    </c:forEach>
  </tr>
  <tr class="row${status.index % 2}">
	<td class="nameCol" align="right"><fmt:message key="balance" /></td>
    <c:forEach items="${model.budgetProducts}" var="budgetProduct">
	  <td align="center">
	    <c:if test="${model.subAccountMap[subAccount.id][budgetProduct.product.sku].qtyTotal != null}">
	      <b><c:out value="${model.subAccountMap[subAccount.id][budgetProduct.product.sku].balance}"/></b>
	    </c:if>
	    <c:if test="${model.subAccountMap[subAccount.id][budgetProduct.product.sku].qtyTotal == null}">-</c:if>
	  </td>    
    </c:forEach>
  </tr>
</c:forEach>
<c:if test="${empty model.subAccounts}">
  <tr>
    <td colspan="${2 + colspan}" class="row0">&nbsp;</td>
  </tr>
</c:if>
  <tr class="listingsHdr2">
  	<td align="center"><fmt:message key="total" /></td>
    <c:forEach items="${model.budgetProducts}" var="budgetProduct">
	<td align="center">
      <c:out value="${budgetProduct.ordered}" />
	</td>
    </c:forEach>
  </tr>
</table>
	</td>
  </tr>
</table>
</form>

</c:if>

  </tiles:putAttribute>
</tiles:insertDefinition>
