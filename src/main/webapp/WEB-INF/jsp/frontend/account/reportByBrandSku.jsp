<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="/WEB-INF/tlds/spring.tld" prefix="spring" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>

<tiles:insertDefinition name="${_template}" flush="true">
  <tiles:putAttribute name="content" type="string">

<c:if test="${model.brand != null}">

<table width="90%" align="center" cellspacing="5" cellpadding="0" border="0">
  <tr>
  	<td class="listingsHdr1">
  	  <c:out value="${model.brand.name}"/> <fmt:message key="report" /><br/>
  	  <fmt:message key="productSku" />: <a href="product.jhtm?sku=<c:out value="${param.sku}"/>" class="nameLink"><c:out value="${param.sku}"/></a>
  	</td>
	<td align="right">&nbsp;</td>
  </tr>
  <tr>
	<td colspan="2">
<table border="0" cellpadding="3" cellspacing="1" width="100%" class="listings"> 
  <tr class="listingsHdr2">
  	<td class="nameCol"><fmt:message key="f_billTo" /></td>
  	<td align="center"><fmt:message key="company" /></td>
  	<td align="center"><fmt:message key="invoice" /></td>
  	<td align="center"><fmt:message key="orderDate" /></td>
    <td align="center"><fmt:message key="productPrice" /></td>
    <td align="center"><fmt:message key="quantity" /></td>
    <td align="center"><fmt:message key="total" /></td>
  </tr>
<c:set var="total_price" value="0.0" />
<c:set var="total_quantity" value="0" />
<c:forEach items="${model.lineItems}" var="lineItem" varStatus="status">
  <tr class="row${status.index % 2}">
    <td class="nameCol"><c:out value="${lineItem['bill_to_first_name']}"/> <c:out value="${lineItem['bill_to_last_name']}"/></td>
    <td align="center"><c:out value="${lineItem['bill_to_company']}"/></td>
    <td align="center"><c:out value="${lineItem['order_id']}"/></td>
    <td align="center"><fmt:formatDate type="date" timeStyle="default" value="${lineItem['date_ordered']}"/></td>			
	<td align="right"><fmt:formatNumber value="${lineItem['unit_price']}" pattern="#,##0.00" /></td>
	<td align="center">
	  <c:set var="total_quantity" value="${lineItem['total_quantity'] + total_quantity}" />
	  <c:out value="${lineItem['total_quantity']}"/>
	</td>
	<td align="right">
	  <c:choose>
	  <c:when test="${lineItem['total_price'] > 0}">
	    <c:set var="total_price" value="${lineItem['total_price'] + total_price}" />
	    <fmt:message key="${siteConfig['CURRENCY'].value}" /><fmt:formatNumber value="${lineItem['total_price']}" pattern="#,##0.00" />
	  </c:when>
	  <c:otherwise>-</c:otherwise>
	  </c:choose>
	</td>
  </tr>
</c:forEach>
<c:if test="${empty model.lineItems}">
  <tr>
    <td colspan="7" class="row0">&nbsp;</td>
  </tr>
</c:if>
  <tr class="listingsHdr2">
  	<td colspan="5" align="center"><fmt:message key="total" /></td>
	<td align="center"><c:out value="${total_quantity}"/></td>
	<td align="right">
        <fmt:message key="${siteConfig['CURRENCY'].value}" /><fmt:formatNumber value="${total_price}" pattern="#,##0.00" />
	</td>
  </tr>
</table>
	</td>
  </tr>
</table>

</c:if>

  </tiles:putAttribute>
</tiles:insertDefinition>
