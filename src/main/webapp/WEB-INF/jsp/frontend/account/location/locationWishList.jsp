<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>

<tiles:insertDefinition name="${_template}" flush="true">
  <tiles:putAttribute name="content" type="string">

<c:if test="${gSiteConfig['gLOCATION']}"> 

<c:if test="${model.locationWishList.nrOfElements > 0}">
<script language="JavaScript">
<!--
function toggleAll(el) {
	var ids = document.getElementsByName("__selected_id");	
  	if (ids.length == 1)
      document.mylist.__selected_id.checked = el.checked;
    else
      for (i = 0; i < ids.length; i++)
        document.mylist.__selected_id[i].checked = el.checked;	
}  

function deleteSelected() {
	var ids = document.getElementsByName("__selected_id");	
  	if (ids.length == 1) {
      if (document.mylist.__selected_id.checked) {
    	return confirm('Delete permanently?');
      }
    } else {
      for (i = 0; i < ids.length; i++) {
        if (document.mylist.__selected_id[i].checked) {
    	  return confirm('Delete permanently?');
        }
      }
    }

    alert("Please select to delete.");       
    return false;
}
//-->
</script>
</c:if>
<form name="mylist" method="post">
<table width="90%" align="center" cellspacing="5" cellpadding="0" border="0">
  <tr>
	<td>&nbsp;</td>
	<td>
<div class="pageNavi">Page 
		<select name="page" onchange="submit()">
		<c:forEach begin="1" end="${model.locationWishList.pageCount}" var="page">
  	        <option value="${page}" <c:if test="${page == (model.locationWishList.page+1)}">selected</c:if>>${page}</option>
		</c:forEach>
		</select>
of <c:out value="${model.locationWishList.pageCount}"/></div>
	</td>
  </tr>
  <tr>
	<td colspan="2">
<table border="0" cellpadding="3" cellspacing="1" width="100%" class="listings"> 
  <tr class="listingsHdr2">
  <c:set var="cols" value="0"/>
<c:if test="${model.locationWishList.nrOfElements > 0}">
	<c:set var="cols" value="${cols+1}"/>
    <td align="center"><input type="checkbox" onclick="toggleAll(this)"></td>
</c:if>
    <td class="nameCol">Name</td>
    <td class="nameCol">Address</td>
    <td class="nameCol" style="white-space: nowrap">Added</td>
  </tr>
<c:forEach items="${model.locationWishList.pageList}" var="location" varStatus="status">
  <tr class="row${status.index % 2}">
<c:if test="${model.locationWishList.nrOfElements > 0}">
    <td align="center"><input name="__selected_id" value="${location.id}" type="checkbox"></td>
</c:if>
    <td class="nameCol"><c:out value="${location.name}" /></td>
	<td class="nameCol"><c:out value="${location.addressToString}"/></td>			
    <td class="nameCol" style="white-space: nowrap"><fmt:formatDate type="date" timeStyle="default" value="${location.created}"/></td>	
  </tr>
</c:forEach>
<c:choose>
 <c:when test="${model.locationWishList.nrOfElements == 0}">
   <tr class="emptyList"><td colspan="${3+cols}">&nbsp;</td></tr>
 </c:when>
</c:choose>
</table>
	</td>
  </tr>
<c:if test="${model.locationWishList.nrOfElements > 0}">
  <tr>
	<td><input type="submit" name="__delete" value="Delete Selected." onClick="return deleteSelected()"></td>
  </tr>
</c:if>
</table>
</form>

</c:if>

  </tiles:putAttribute>
</tiles:insertDefinition>
