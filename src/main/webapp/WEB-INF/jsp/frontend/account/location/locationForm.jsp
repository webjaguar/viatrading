<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>

<tiles:insertDefinition name="${_template}" flush="true">
  <tiles:putAttribute name="content" type="string">

<c:if test="${gSiteConfig['gLOCATION']}"> 

<script language="JavaScript">
<!--

//-->
</script>

<form:form  commandName="locationForm" method="post">
<table border="0" cellpadding="3" cellspacing="1" width="100%">
  <tr>
    <td class="formNameReq"><fmt:message key="keywords" />:</td>
    <td>
      <form:textarea path="location.keywords" rows="10" cols="40" cssClass="textfield" htmlEscape="true"/>
      <form:errors path="location.keywords" cssClass="error" />
    </td>
  </tr>
</table>  

<div align="center">
  <input type="submit" value="<fmt:message key="update" />">
  <input type="submit" value="<fmt:message key="cancel" />" name="_cancel">
</div>
</form:form>

</c:if>
  </tiles:putAttribute>
</tiles:insertDefinition>
