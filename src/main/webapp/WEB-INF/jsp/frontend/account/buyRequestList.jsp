<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>

<tiles:insertDefinition name="${_template}" flush="true">
  <tiles:putAttribute name="content" type="string">
  
<br><br><br>
<form method="post">
<table width="90%" align="center" cellspacing="5" cellpadding="0" border="0">
  <tr>
  	<td class="listingsHdr1">Buy Request</td>
	<td>
<div class="pageNavi">Page 
		<select name="page" onchange="submit()">
		<c:forEach begin="1" end="${model.buyRequestList.pageCount}" var="page">
  	        <option value="${page}" <c:if test="${page == (model.buyRequestList.page+1)}">selected</c:if>>${page}</option>
		</c:forEach>
		</select>
of <c:out value="${model.buyRequestList.pageCount}"/></div>
	</td>
  </tr>
  <tr>
	<td colspan="2">
<table border="0" cellpadding="3" cellspacing="1" width="100%" class="listings"> 
  <tr class="listingsHdr2">
    <td class="nameCol"><select name="status">
        <option value="open" <c:if test="${buyRequestSearch.status == 'open'}">selected</c:if>>Open</option>
        <option value="close" <c:if test="${buyRequestSearch.status == 'close'}">selected</c:if>>Close</option> 
        <option value="" <c:if test="${buyRequestSearch.status == ''}">selected</c:if>>All</option> 
      </select>
    </td>
  	<td class="nameCol"><input name="buyrequest_id" type="text" value="<c:out value='${buyRequestSearch.buyRequestId}' />" size="15" /></td>
  	<td class="nameCol"><input name="name" type="text" value="<c:out value='${buyRequestSearch.name}' />" size="15" /></td>
  	<td class="nameCol" colspan="2" align="center"><input type="submit" value="Search" /></td>
  </tr>
  <tr class="listingsHdr2">
    <td class="nameCol"><fmt:message key="status" /></td>
  	<td class="nameCol"><fmt:message key="id" /></td>
  	<td class="nameCol"><fmt:message key="Name" /></td>
  	<td class="nameCol"><fmt:message key="activeFrom" /></td>
  	<td align="center"><fmt:message key="activeTo" /></td>
  </tr>
<c:forEach items="${model.buyRequestList.pageList}" var="buyRequest" varStatus="status">
  <tr class="row${status.index % 2}">
    <td class="nameCol"><c:out value="${buyRequest.status}"/></td>
	<td class="nameCol"><a href="buyRequest.jhtm?id=${buyRequest.id}" class="nameLink"><c:out value="${buyRequest.id}"/></a></td>
    <td class="nameCol"><a href="buyRequestUpdate.jhtm?id=${buyRequest.id}" class="nameLink"><c:out value="${buyRequest.name}"/></a></td>
    <td class="nameCol"><fmt:formatDate type="both" timeStyle="full" pattern="MM/dd/yyyy hh:mm a" value="${buyRequest.activeFrom}"/></td>	
    <td class="nameCol"><fmt:formatDate type="both" timeStyle="full" pattern="MM/dd/yyyy hh:mm a" value="${buyRequest.activeTo}"/></td>	
  </tr>
</c:forEach>
<c:if test="${model.buyRequest.nrOfElements == 0}">
  <tr class="emptyList"><td colspan="4">&nbsp;</td></tr>
</c:if>
</table>
<br/>
	</td>
  </tr>
</table>
</form>

  </tiles:putAttribute>
</tiles:insertDefinition>
