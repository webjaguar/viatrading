<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<tiles:insertDefinition name="${_template}" flush="true">
  <tiles:putAttribute name="content" type="string">

<%-- 
make sure you add on vhost.conf
Alias /exportFile "/usr/java/webjaguar/_____/ROOT/temp/exportFiles"
--%>

	<a href="exportFile/<c:out value="${model.exportedFile}"/>"><c:out value="${model.exportedFile}"/></a> 

  </tiles:putAttribute>
</tiles:insertDefinition>
