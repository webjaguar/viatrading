<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="/WEB-INF/tlds/spring.tld" prefix="spring" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>

<tiles:insertDefinition name="${_template}" flush="true">
  <tiles:putAttribute name="content" type="string">
<script type="text/JavaScript">
<!--
function checkCode(){
  if ( document.getElementById('__claimCode').value == "" ) {
  	alert("<fmt:message key='giftcard.empty' />");
  	return false;
  }
}
//-->
</script>
  
<c:if test="${model.message != null}">
  <div class="message"><fmt:message key="${model.message}" /></div>
</c:if>


<form method="post">
<table width="100%" border="0" cellpadding="3" class="giftcardBox">

  <tr>
    <td colspan="2" style="font-weight:700;color:#DD3204"><fmt:message key="viewYourBalanceOrRedeemANewGiftCertificate/Card" /></td>
  </tr>
  <tr>
    <td colspan="2" style="font-weight:700;color:#1C1C1C"><fmt:message key="currentCredit"></fmt:message> <fmt:message key="${siteConfig['CURRENCY'].value}"/><c:out value="${model.credit}"/></td>
  </tr>  
  <tr>
    <td>
     <ol>
	    <li><fmt:message key="wantToRedeemANewGiftCertificate/card" />
	    <table class="box">
	     <tr>
	      <td><fmt:message key="giftCard" /> <fmt:message key="code" />:</td>
	      <td><input id="__claimCode"  type="text" name="__claimCode" size="20"/></td>
	     </tr>
	    </table> 
	    </li>
      </ol>
     </td> 
  </tr>
  <tr align="center"> 
  	 <td colspan="2">
	  <c:if test="${model.giftCardSuccessful == true }">	  
	  	<div class="message"><fmt:message key="giftCardSuccessful">
	  	<c:choose>
	  		<c:when test="${siteConfig['CURRENCY'].value == 'EUR'}">
	  			<fmt:param value="&#8364;" /> 
	  		</c:when>
	  		<c:when test="${siteConfig['CURRENCY'].value == 'GBP'}">
	  			<fmt:param value="&#163;"/> 
	  		</c:when>
	  		<c:when test="${siteConfig['CURRENCY'].value == 'JPY'}">
	  			<fmt:param value="&#165;"/> 
	  		</c:when>
	  		<c:when test="${siteConfig['CURRENCY'].value == 'CAD'}">
	  			<fmt:param value="CAD&#36;"/> 
	  		</c:when>
	  		<c:otherwise>
	  		    <fmt:param value="&#36;" />
	  		</c:otherwise>
	  	</c:choose><fmt:param value="${model.credit}"/></fmt:message></div>
	  </c:if>
  	 </td>
  </tr>
  <tr align="center">
    <td colspan="2" id="buttonWrapper">
	  <input class="f_button" type="submit" name="_redeem" onclick="return checkCode();" value="<fmt:message key="redeem" />" />
    </td>
  </tr>
  </table>
  
</form>

  </tiles:putAttribute>
</tiles:insertDefinition>