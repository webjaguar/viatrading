<%@ page import="java.util.Random" %>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="/WEB-INF/tlds/spring.tld" prefix="spring" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>

<%
pageContext.setAttribute("randomNum", new Random().nextInt(1000));
%>

<tiles:insertDefinition name="${_template}" flush="true">
  <tiles:putAttribute name="content" type="string">
  
<style type="text/css">
 .productPreValueField {width: 300px;}
 .list0 {overflow:hidden;width: 550px;}
 .listfl {float:left;line-height:17px;text-align:right;width:120px;}
 .listp {float:left;line-height:17px;overflow:hidden;width:425px;}
</style>

<script type="text/javascript" src="javascript/mootools-1.2.5-core.js"></script>
<script language="JavaScript" type="text/JavaScript">
<!--
<%-- comment out not in use for marketsanctuary 
var field_array = new Array(<c:forEach items="${productForm.productFields}" var="productField" varStatus="status">'${productField.id}'<c:if test="${not status.last}">, </c:if></c:forEach>);
function updateCategory(id, selected) {
		var request = new Request.JSON({
			url: "json.jhtm?action=category&bid=<c:out value="${siteConfig['SUPPLIER_PRODUCT_CATEGORY'].value}"/>&cid=" + selected,
			onRequest: function() { 
				document.getElementById(id + "_spinner").style.display="block";
			},
			onComplete: function(jsonObj) {
				document.getElementById(id + "_spinner").style.display="none";
				var categoryIds = document.getElementById(id + "_categoryIds");
				if (jsonObj.subcats.length > 0) {
					while (categoryIds.length > 0) {
    					categoryIds.remove(0);
					} 		
					i = 0;
					jsonObj.subcats.each(function(category) {
						categoryIds.options[i++] = new Option(category.name, category.id);
					});				
					var parentId = document.getElementById(id + "_parentId");
					while (parentId.length > 1) {
	    				parentId.remove(1);
					} 
					if (jsonObj.tree.length > 0) {		
						i = 1;
						jsonObj.tree.each(function(category) {
							parentId.options[i++] = new Option(category.name, category.id, false, true);
						});				
					}
				}
				for (x=0; x<field_array.length; x++) {
				  var show = 'false';
				  for (y=0; y<${model.numOfCategories}; y++) {
				  	var selected_id = document.getElementById('cat' + y + '_categoryIds').value;
				  	if (selected_id != '') {
				  	  if (document.getElementById('field' + field_array[x] + '_catIds').value.indexOf(selected_id) > -1) {
				  		show = 'true';
				  		break;
				  	  }
				  	}
				  }
				  if (show == 'true') {
					document.getElementById("product.field" + field_array[x] + "_div").style.display="block";								  
				  } else {
					document.getElementById("product.field" + field_array[x] + "_div").style.display="none";			  
				  }
				}
			}
		}).send();
}

window.onload = function() {
  for (i=${gSiteConfig['gPRICE_TIERS']}; i>1; i--) {
    updateQtyBreak(i);
  }
}

function updateQtyBreak(i) {
  var priceFrom = document.getElementById('priceFrom_'+i).value;
  if (priceFrom > 0) {
    if ((i<${gSiteConfig['gPRICE_TIERS']}) && (document.getElementById('priceFrom_'+(i+1)).value > 0)) {
      document.getElementById('priceTo_'+i).value = document.getElementById('priceFrom_'+(i+1)).value-1;    
    } else {
      document.getElementById('priceTo_'+i).value = "";
    }
    document.getElementById('priceTo_'+(i-1)).value = priceFrom-1;    
  } else {
    document.getElementById('priceTo_'+i).value = "";
  }
}
--%>
function confirmDelete() {
	reply = confirm('Delete permanently?');
	if (reply == true) {
		document.getElementById('_delete').value="true";
		return true;
	}
    return false;
}
//-->
</script>

<br><br>

<spring:hasBindErrors name="productForm">
<div class="error">Please fix all errors!</div>
</spring:hasBindErrors>

<form:form commandName="productForm" method="post" name="productForm"> 
<input type="hidden" name="_delete" id="_delete"> 
<input type="hidden" name="selected_image" id="selected_image">

<fieldset class="register_fieldset">
<legend>Product</legend>
<table class="productForm" width="560px" cellspacing="0" cellpadding="0" border="0">
  <tr>
    <td align="right" class="fieldName productName"><fmt:message key="productName" />: </td>
    <td>
	  <form:input path="product.name" cssClass="fieldValue productName" htmlEscape="true"/>
	  <form:errors path="product.name" cssClass="error" />
    </td>
  </tr>
  <tr>
    <td align="right" class="fieldName productSku"><fmt:message key="productSku" />: </td>
    <td>
	  <form:input path="product.sku" cssClass="fieldValue productSku" htmlEscape="true"/>
	  <form:errors path="product.sku" cssClass="error" />
	    <div class="helpNote">
			  <p>Do not use special character or space in SKU. SKU will be visible on URL.</p>
		  </div>
    </td>
  </tr>
  <tr>
    <td align="right">&nbsp;</td>
    <td><div class="error">required prefix: <c:out value="${productForm.skuPrefix}"/>, example: <c:out value="${productForm.skuPrefix}"/>12345</div></td>
  </tr>
  <tr>
    <td align="right" class="fieldName productShortDesc"><fmt:message key="productShortDesc" />: </td>
    <td>
	  <form:input path="product.shortDesc" cssClass="fieldValue productShortDesc" htmlEscape="true"/>
	  <form:errors path="product.shortDesc" cssClass="error" />
    </td>
  </tr>
  <tr>
    <td align="right" valign="top" class="fieldName productLongDesc"><fmt:message key="productLongDesc" />: </td>
    <td>
	  <form:textarea path="product.longDesc" cssClass="fieldValue productLongDesc" htmlEscape="true"/>
	  <form:errors path="product.longDesc" cssClass="error" />
    </td>
  </tr>
  <tr>
    <td align="right" class="fieldName msrp"><c:choose><c:when test="${siteConfig['MSRP_TITLE'].value != ''}"><c:out value="${siteConfig['MSRP_TITLE'].value}" /></c:when><c:otherwise><fmt:message key="productMsrp" /></c:otherwise></c:choose>: </td>
    <td>
	  <form:input path="product.msrp" cssClass="fieldValue msrp" />
	  <form:errors path="product.msrp" cssClass="error" />  
    </td>
  </tr>
  <tr>
    <td align="right" class="fieldName productPrice"><fmt:message key="productPrice" /><c:if test="${gSiteConfig['gPRICE_TIERS'] > 1}"> 1</c:if>: </td>
    <td>
	  <form:input path="product.price1" cssClass="fieldValue productPrice" />
	  <%-- comment out not in use for marketsanctuary --%>
	  <c:if test="${false and gSiteConfig['gPRICE_TIERS'] > 1}">
	  <input type="text" id="priceFrom_1" size="5" value="1" readonly="readonly" class="qtyBreak">
	  to <input type="text" id="priceTo_1" size="5" readonly="readonly" class="qtyBreak">
	  </c:if>
	  <form:errors path="product.price1" cssClass="error" delimiter=", "/>
    </td>
  </tr>  
  <%-- comment out not in use for marketsanctuary --%>
  <c:if test="${false and gSiteConfig['gPRICE_TIERS'] > 1}">
  <c:forEach begin="2" end="${gSiteConfig['gPRICE_TIERS']}" var="priceTier">
  <tr>
    <td align="right" class="fieldName productPrice"><fmt:message key="productPrice" /> ${priceTier}: </td>
    <td>
	  <form:input path="product.price${priceTier}" cssClass="fieldValue productPrice"/>
	  <form:input path="product.qtyBreak${priceTier-1}" size="5" maxlength="10" onblur="updateQtyBreak(${priceTier})" id="priceFrom_${priceTier}" cssClass="qtyBreak"/>
	  to <input type="text" id="priceTo_${priceTier}" class="textfield" size="5" readonly="readonly" class="qtyBreak">
	  <form:errors path="product.price${priceTier}" cssClass="error" delimiter=", "/>
    </td>
  </tr>  
  </c:forEach>
  </c:if> 
  
  <%-- comment out not in use for marketsanctuary 
  <tr>
    <td align="right"><fmt:message key="listType" />: </td>
    <td>
	  <form:select path="product.listType">
	    <form:option value="" label="Pleas Select"></form:option>
	    <form:option value="1" label="label1"></form:option>
	    <form:option value="2" label="label2"></form:option>
	    <form:option value="3" label="label3"></form:option>
	  </form:select>
	  <form:errors path="product.listType" cssClass="error" delimiter=", "/>
    </td>
  </tr>--%>
  <%-- 
  <c:forEach items="${productForm.parentIds}" var="parentId" varStatus="status">
  <tr>
    <td align="right">&nbsp;<fmt:message key="category" />: </td>
    <td>
      <table>
      <tr><td>
	    <select name="parentIds" id="cat${status.index}_parentId" style="width:300px;" onchange="updateCategory('cat${status.index}', this.value)">
	      <c:forEach items="${model.tree[status.index]}" var="category" varStatus="treeStatus">
	      <c:if test="${treeStatus.first and category.id != siteConfig['SUPPLIER_PRODUCT_CATEGORY'].value}">
  	      <option value="<c:out value="${siteConfig['SUPPLIER_PRODUCT_CATEGORY'].value}"/>"><fmt:message key="categoryMain" /></option>
	      </c:if>
  	      <option value="${category.id}" <c:if test="${category.id == parentId}">selected</c:if>>
  	        <c:choose>
  	          <c:when test="${treeStatus.first and category.id == siteConfig['SUPPLIER_PRODUCT_CATEGORY'].value}"><fmt:message key="categoryMain" /></c:when>
  	          <c:otherwise><c:out value="${category.name}"/></c:otherwise>
  	        </c:choose>
  	      </option>
	      </c:forEach>
	    </select>
	  </td><td>
	    <img id="cat${status.index}_spinner" src="assets/Image/Layout/spinner.gif" style="display:none">
	  </td></tr>
	  </table>
    </td>
  </tr>   
  <tr>
    <td align="right">&nbsp;</td>
    <td>
	  <select name="cat${status.index}_categoryIds" id="cat${status.index}_categoryIds" size="5" style="width:300px;" onchange="updateCategory('cat${status.index}', this.value)">	  
	    <c:forEach items="${model.categories[status.index]}" var="category">
  	    <option value="${category.id}" <c:if test="${category.id == productForm.categoryIds[status.index]}">selected</c:if>><c:out value="${category.name}"/></option>
	    </c:forEach>
	  </select>
    </td>
  </tr>  
  </c:forEach>
--%>
  <%-- <tr>
  <td colspan="2">
  <c:forEach items="${productForm.productFields}" var="productField" varStatus="status"> 
  <c:set var="show" value="false"/>
  <c:forEach items="${productForm.categoryIds}" var="id">
    <c:set var="string_id" value=",${id},"/>
  	<c:if test="${fn:contains(productField.categoryIds, string_id)}">
  	  <c:set var="show" value="true"/>
  	</c:if>
  </c:forEach>
	<input type="hidden" id="field${productField.id}_catIds" value="${productField.categoryIds}">
     <div id="product.field${productField.id}_div" class="list0" <c:if test="${not show}">style="display:none"</c:if>>
      <div class="listfl"><c:out value="${productField.name}"/>:</div>
      <div class="listp">		  	
	 <c:choose>
     <c:when test="${!empty productField.preValue}">
       <form:select path="product.field${productField.id}" cssClass="productPreValueField">
         <form:option value="" label="Please Select"/>
         <c:forTokens items="${productField.preValue}" delims="," var="dropDownValue">
           <form:option value="${dropDownValue}" />
         </c:forTokens>
       </form:select>
     </c:when>
     <c:otherwise>
       <form:input  path="product.field${productField.id}" cssClass="textfield" maxlength="255" size="50" htmlEscape="true"/>
     </c:otherwise>
     </c:choose>
     </div>		  	
     </div>		  	
  </c:forEach>  
    </td>
  </tr>
    --%>
  <%-- Product Fields --%>
  <c:forEach items="${productForm.productFields}" var="productField" varStatus="status">
  <c:if test="${productField.showOnProductFormFrontend}">
  	<tr>
   	   <td align="right" width="120px" class="fieldName productField"><c:out value="${productField.name}"/>:</td>
       <td><form:input path="product.field${productField.id}" cssClass="fieldValue productField" htmlEscape="true"/></td>
    </tr>
  </c:if>
  </c:forEach>
  
  <%--Inventory --%>
  <c:if test="${gSiteConfig['gINVENTORY']}">
  	<tr>
  	   <td class="fieldName inventoryAFS"><fmt:message key="available_for_sale" />:</td>
  	   <td><form:input path="product.inventoryAFS" cssClass="fieldValue inventoryAFS" /></td>
    </tr>
    <tr>
  	   <td class="fieldName negInventory"><fmt:message key="allowToBuyNegInventory" />:</td>
       <td><form:checkbox path="product.negInventory" /></td>
    </tr>
    <tr>
       <td class="fieldName showNegInventory"><fmt:message key="showNegInventory" />:</td>
 	   <td><form:checkbox path="product.showNegInventory" /></td>
    </tr>
  </c:if>
  
  <c:if test="${gSiteConfig['gMINIMUM_INCREMENTAL_QTY']}">
	  <tr>
	  	 <td class="fieldName minimumQty"><fmt:message key="minimumToOrder" /> / <fmt:message key="incremental" />:</td>
	     <td><form:input path="product.minimumQty"  cssClass="fieldValue minimumQty" htmlEscape="true" /> / <form:input path="product.incrementalQty"  cssClass="fieldValue incrementalQty" htmlEscape="true"/>
	  		 <form:errors path="product.minimumQty" cssClass="error"/>
	  		 <form:errors path="product.incrementalQty" cssClass="error"/></td>
	  </tr>
  </c:if>
  
  <c:if test="${gSiteConfig['gRECOMMENDED_LIST']}">
	  <tr>
	 	 <td width="120px" class="fieldName recommendedList"><fmt:message key="recommendedList" /> <fmt:message key="title" />:</td>
	 	 <td><form:input path="product.recommendedList" cssClass="fieldValue recommendedList" htmlEscape="true" /></td>
	  </tr>
	  <tr>
	  	 <td class="fieldName productSkus"><fmt:message key="productSkus" />:</td>
	  	 <td><form:textarea path="product.recommendedList" cssClass="fieldValue productSkus" htmlEscape="true" /></td>
	  </tr>
  </c:if>
  
  <tr>
	  <td class="fieldName packing"><fmt:message key="packing" />:</td>
	  <td><form:input path="product.packing" cssClass="fieldValue packing" htmlEscape="true"/></td>
  </tr>
  
  <c:if test="${gSiteConfig['gCASE_CONTENT']}">
	  <tr>
		  <td class="fieldName caseContent"><fmt:message key="caseContent" />:</td>
		  <td><form:input path="product.caseContent"  cssClass="fieldValue caseContent" htmlEscape="true" />
		  	  <form:errors path="product.caseContent" cssClass="error"/></td>
	  </tr>
  </c:if>
  
  <c:if test="${gSiteConfig['gPRICE_TIERS'] > 1}">
	  <tr>
		  <td class="fieldName code"><fmt:message key="crossItems" /><fmt:message key="code" />:</td>
		  <td><form:input path="product.crossItemsCode" cssClass="fieldValue crossItemsCode" htmlEscape="true"/>
		 	  <form:errors path="product.crossItemsCode" cssClass="error" /></td>
	  </tr>
  </c:if>
  
  <c:forEach items="${productForm.productImages}" var="image" varStatus="status">
  <tr>
    <td align="right" valign="top">&nbsp;<c:if test="${status.first}"><fmt:message key="image"/>: </c:if></td>
	<td>
<div style="background-color:#C2C29E;float:left;margin:0px 5px 5px 0px;padding:5px;text-align:center;">
	<c:if test="${image.imageUrl != null}">
	<div style="margin: 10px;padding: 2px;border:1px #ffffff solid;">
	  <img src="<c:if test="${!product.thumbnail.absolute}">assets/Image/Product/detailsbig/</c:if><c:out value="${image.imageUrl}"/>?rnd=${randomNum}" border="0" height="100">
	</div>
	<div>
	  <c:choose>
	    <c:when test="${fn:startsWith(image.imageUrl, model.supplierDirectory)}">
	  	  <c:out value="${fn:substringAfter(image.imageUrl, model.supplierDirectory)}"/>
	    </c:when>
	    <c:otherwise>
	  	  <c:out value="${image.imageUrl}"/>
	    </c:otherwise>
	  </c:choose>
	</div>
	<input type="submit" name="_remove" value="<fmt:message key="remove" />" onClick="document.getElementById('selected_image').value=${status.index}">
	</c:if>
	<input type="submit" value="Choose from Gallery" name="_target1" onclick="document.getElementById('selected_image').value=${status.index}"/>
</div>
    </td>
  </tr>
  </c:forEach> 
</table>
</fieldset>

<div align="center">
<c:if test="${productForm.newProduct}">
  <input type="submit" name="_finish" value="<spring:message code="productAdd"/>">
</c:if>
<c:if test="${!productForm.newProduct}">
  <input type="submit" name="_finish" value="<spring:message code="productUpdate"/>">
  <input type="submit" value="<spring:message code="productDelete"/>" name="_finish" onClick="return confirmDelete()">
</c:if>
  <input type="submit" value="<spring:message code="cancel"/>" name="_cancel">
</div>

</form:form>

  </tiles:putAttribute>    
</tiles:insertDefinition>