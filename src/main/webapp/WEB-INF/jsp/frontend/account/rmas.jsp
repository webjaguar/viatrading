<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>

<tiles:insertDefinition name="${_template}" flush="true">
  <tiles:putAttribute name="content" type="string">
  
<c:if test="${gSiteConfig['gRMA']}">
  
<br><br><br>
<form method="post">
<table width="90%" align="center" cellspacing="5" cellpadding="0" border="0">
  <tr>
  	<td class="listingsHdr1"><fmt:message key="rmaRequest" /> <fmt:message key="historyStatus" /></td>
	<td>
<div class="pageNavi">Page 
		<select name="page" onchange="submit()">
		<c:forEach begin="1" end="${model.rmas.pageCount}" var="page">
  	        <option value="${page}" <c:if test="${page == (model.rmas.page+1)}">selected</c:if>>${page}</option>
		</c:forEach>
		</select>
of <c:out value="${model.rmas.pageCount}"/></div>
	</td>
  </tr>
  <tr>
	<td colspan="2">
<table border="0" cellpadding="3" cellspacing="1" width="100%" class="listings"> 
  <tr class="listingsHdr2">
  	<td><input name="rmaNum" type="text" value="<c:out value='${model.rmaSearch.rmaNum}' />" size="10" /></td>
  	<td><input name="orderId" type="text" value="<c:out value='${model.rmaSearch.orderId}' />" size="10" /></td>
  	<td><input name="serialNum" type="text" value="<c:out value='${model.rmaSearch.serialNum}' />" size="10" /></td>
    <td><select name="status" style="width:100px">
        <option value=""><fmt:message key="all" /></option> 
	    <c:forTokens items="Pending,Awaiting Receipt,Evaluation,Processing,Canceled,Completed" delims="," var="status">
          <option value="${status}" <c:if test="${model.rmaSearch.status == status}">selected</c:if>><c:out value="${status}"/></option>
        </c:forTokens>
      </select>
    </td>
  	<td colspan="2" align="right"><input type="submit" value="Search" /></td>
  </tr>
  <tr class="listingsHdr2">
    <c:set var="cols" value="0"/>
  	<td class="nameCol"><fmt:message key="rma" /> #</td>
  	<td class="nameCol"><fmt:message key="order" /> #</td>
  	<td class="nameCol"><fmt:message key="S/N" /></td>
  	<td class="nameCol"><fmt:message key="status" /></td>
  	<td><fmt:message key="reportDate" /></td>
  	<td><fmt:message key="completeDate" /></td>
  </tr>
<c:forEach items="${model.rmas.pageList}" var="rma" varStatus="status">
  <tr class="row${status.index % 2}">
	<td class="nameCol"><a href="rma.jhtm?num=${rma.rmaNum}" class="nameLink"><c:out value="${rma.rmaNum}"/></a></td>
	<td class="nameCol"><a href="invoice.jhtm?order=${rma.orderId}" class="nameLink"><c:out value="${rma.orderId}"/></a></td>
    <td class="nameCol"><c:out value="${rma.serialNum}"/></td>	
	<td class="nameCol"><c:out value="${rma.status}"/></td>
    <td><fmt:formatDate type="date" timeStyle="default" value="${rma.reportDate}"/></td>	
    <td><fmt:formatDate type="date" timeStyle="default" value="${rma.completeDate}"/></td>	
  </tr>
</c:forEach>
<c:if test="${model.rmas.nrOfElements == 0}">
  <tr class="emptyList"><td colspan="7">&nbsp;</td></tr>
</c:if>
</table>
<table border="0" cellpadding="0" cellspacing="1" width="100%">
  <tr>
  <td>
	<input type="submit" name="__add" value="<fmt:message key="new" /> <fmt:message key="rmaRequest" />">
  </td>
  <td align="right">
  <select name="size" onchange="submit()">
    <c:forTokens items="10,25,50" delims="," var="current">
  	  <option value="${current}" <c:if test="${current == model.rmas.pageSize}">selected</c:if>>${current} <fmt:message key="perPage" /></option>
    </c:forTokens>
  </select>
  </td>
  </tr>
</table> 
	</td>
  </tr>
</table>
</form>

</c:if>

  </tiles:putAttribute>
</tiles:insertDefinition>
