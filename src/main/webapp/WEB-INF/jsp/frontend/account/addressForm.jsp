<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="/WEB-INF/tlds/spring.tld" prefix="spring" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>

<tiles:insertDefinition name="${_template}" flush="true">
  <tiles:putAttribute name="content" type="string">

<c:if test="${siteConfig['EDIT_ADDRESS_ON_FRONTEND'].value == 'true'}" >
<spring:hasBindErrors name="addressForm">
<span class="error">Please fix all errors!</span>
</spring:hasBindErrors>

<form:form  commandName="addressForm" method="post">
<table border="0" cellpadding="3" cellspacing="1" width="100%">
  <tr><td colspan="2" class="addressBook"><c:if test="${addressForm.newAddress}"><fmt:message key="addressNew" /></c:if><c:if test="${!addressForm.newAddress}"><fmt:message key="addressEdit" /></c:if></td>
  </tr>
  <tr>
    <td class="formNameReq"><fmt:message key="firstName" />:</td>
    <td>
      <spring:bind path="addressForm.address.firstName">
      <input type="text" name="<c:out value="${status.expression}"/>" value="<c:out value="${status.value}"/>"/>
      <span class="error"><c:out value="${status.errorMessage}"/></span>
      </spring:bind>
    </td>
  </tr>
  <tr>
    <td class="formNameReq"><fmt:message key="lastName" />:</td>
    <td>
      <spring:bind path="addressForm.address.lastName">
      <input type="text" name="<c:out value="${status.expression}"/>" value="<c:out value="${status.value}"/>"/>
      <span class="error"><c:out value="${status.errorMessage}"/></span>
      </spring:bind>
    </td>
  </tr>
  <tr>
    <td class="formNameReq"><fmt:message key='country' />:</td>
    <td>
      <spring:bind path="addressForm.address.country">
		<select id="country" name="<c:out value="${status.expression}"/>" onChange="toggleStateProvince(this)">
  	      <option value="">Please Select</option>
	      <c:forEach items="${model.countrylist}" var="country">
  	        <option value="${country.code}" <c:if test="${country.code == status.value}">selected</c:if>>${country.name}</option>
		  </c:forEach>
		</select>
		<span class="error"><c:out value="${status.errorMessage}"/></span>
      </spring:bind>
    </td>
  </tr>
  <tr>
    <td class="formName<c:if test="${siteConfig['CUSTOMERS_REQUIRED_COMPANY'].value == 'true'}">Req</c:if>"><fmt:message key="company" />:</td>
    <td>
      <spring:bind path="addressForm.address.company">
      <input type="text" name="<c:out value="${status.expression}"/>" value="<c:out value="${status.value}"/>"/>
      <span class="error"><c:out value="${status.errorMessage}"/></span>
      </spring:bind>
    </td>
  </tr>
  <tr>
    <td class="formNameReq"><fmt:message key="address" /> 1:</td>
    <td>
      <spring:bind path="addressForm.address.addr1">
      <input type="text" name="<c:out value="${status.expression}"/>" value="<c:out value="${status.value}"/>"/>
      <span class="error"><c:out value="${status.errorMessage}"/></span>
      </spring:bind>
    </td>
  </tr>
  <tr>
    <td class="formName"><fmt:message key="address" /> 2:</td>
    <td>
      <spring:bind path="addressForm.address.addr2">
      <input type="text" name="<c:out value="${status.expression}"/>" value="<c:out value="${status.value}"/>"/>
      </spring:bind>
    </td>
  </tr>
  <tr>
    <td class="formNameReq"><fmt:message key="city" />:</td>
    <td>
      <spring:bind path="addressForm.address.city">
      <input type="text" name="<c:out value="${status.expression}"/>" value="<c:out value="${status.value}"/>"/>
      <span class="error"><c:out value="${status.errorMessage}"/></span>
      </spring:bind>
    </td>
  </tr>
  <tr>
    <td class="formNameReq"><fmt:message key="stateProvince" />:</td>
    <td>
      <spring:bind path="addressForm.address.stateProvince">
      <table cellspacing="0" cellpadding="0">
       <tr>
       <td>
		<select id="state" name="<c:out value="${status.expression}"/>">
  	      <option value="">Please Select</option>
	      <c:forEach items="${model.statelist}" var="state">
  	        <option value="${state.code}" <c:if test="${state.code == status.value}">selected</c:if>>${state.name}</option>
		  </c:forEach>      
        </select>
		<select id="ca_province" name="<c:out value="${status.expression}"/>">
  	      <option value="">Please Select</option>
	      <c:forEach items="${model.caProvinceList}" var="province">
  	        <option value="${province.code}" <c:if test="${province.code == status.value}">selected</c:if>>${province.name}</option>
		  </c:forEach>      
        </select>        
      <input id="province" type="text" name="<c:out value="${status.expression}"/>" value="<c:out value="${status.value}"/>">
       </td>
       <td><div id="stateProvinceNA"><form:checkbox path="address.stateProvinceNA" id="addressStateProvinceNA" value="true" /> Not Applicable</div></td>
       <td>&nbsp;</td>
       <td>
      <span class="error"><c:out value="${status.errorMessage}"/></span>
       </td>
       </tr>
      </table>
      </spring:bind>
    </td>
  </tr>
  <tr>
    <td class="formNameReq"><fmt:message key="zipCode" />:</td>
    <td>
      <spring:bind path="addressForm.address.zip">
      <input type="text" name="<c:out value="${status.expression}"/>" value="<c:out value="${status.value}"/>"/>
      <span class="error"><c:out value="${status.errorMessage}"/></span>
      </spring:bind>
    </td>
  </tr>
  <c:if test="${siteConfig['ADDRESS_RESIDENTIAL_COMMERCIAL'].value >= 2}">
    <tr>
    <td class="formNameReq"><fmt:message key="deliveryType" />:</td>
    <td><form:radiobutton path="address.residential" value="true"/>: <fmt:message key="residential" /><br /><form:radiobutton path="address.residential" value="false"/>: <fmt:message key="commercial" /> </td>
    <td></td>
    </tr>
    <c:if test="${siteConfig['SHIPPING_LTL'].value == 'true'}">
      <tr>
      <td class="formNameReq"><fmt:message key="liftGateDelivery" />:</td>
      <td><form:checkbox path="address.liftGate" value="true"/><a href="category.jhtm?cid=307" onclick="window.open(this.href,'','width=600,height=300,resizable=yes'); return false;"><img class="toolTipImg" src="assets/Image/Layout/question.gif" border="0" /></a> </td>
      </tr>
    </c:if>  
  </c:if>
  <tr>
    <td class="formNameReq"><fmt:message key="phone" />:</td>
    <td>
      <spring:bind path="addressForm.address.phone">
      <input type="text" name="<c:out value="${status.expression}"/>" value="<c:out value="${status.value}"/>"/>
      <span class="error"><c:out value="${status.errorMessage}"/></span>
      </spring:bind>
    </td>
  </tr>
    <tr>
    <td class="formName"><fmt:message key="cellPhone" />:</td>
    <td>
      <form:input path="address.cellPhone"/>
    </td>
  </tr>
  
  <tr>
    <td class="formName"><fmt:message key="fax" />:</td>
    <td>
      <spring:bind path="addressForm.address.fax">
      <input type="text" name="<c:out value="${status.expression}"/>" value="<c:out value="${status.value}"/>"/>
      </spring:bind>
    </td>
  </tr>
</table>

<c:if test="${not addressForm.address.primary}">
  <spring:bind path="addressForm.setAsPrimary">
    <input type="checkbox" name="<c:out value="${status.expression}"/>" value="true" <c:if test="${status.value}">checked</c:if>>
		<fmt:message key="setAsPrimaryAddress"/>
  </spring:bind>
</c:if>

<div align="center">
<c:if test="${addressForm.newAddress}">
  <input type="submit" value="<spring:message code="addressAdd"/>">
</c:if>
<c:if test="${!addressForm.newAddress}">
  <input type="submit" value="<spring:message code="addressUpdate"/>">
  <c:if test="${not addressForm.address.primary}">
  <input type="submit" value="<spring:message code="addressDelete"/>" name="_delete" onClick="return confirm('Delete permanently?')">
  </c:if>
</c:if>
  <input type="submit" value="<spring:message code="cancel"/>" name="_cancel">
</div>

</form:form>

<script language="JavaScript">
<!--
function toggleStateProvince(el) {
  if (el.value == "US") {
      document.getElementById('state').disabled=false;
      document.getElementById('state').style.display="block";
      document.getElementById('ca_province').disabled=true;
      document.getElementById('ca_province').style.display="none";
      document.getElementById('province').disabled=true;
      document.getElementById('province').style.display="none";
      document.getElementById('addressStateProvinceNA').disabled=true;
      document.getElementById('stateProvinceNA').style.display="none";
  } else if (el.value == "CA") {
      document.getElementById('state').disabled=true;
      document.getElementById('state').style.display="none";
      document.getElementById('ca_province').disabled=false;
      document.getElementById('ca_province').style.display="block";
      document.getElementById('province').disabled=true;
      document.getElementById('province').style.display="none";
      document.getElementById('addressStateProvinceNA').disabled=true;
      document.getElementById('stateProvinceNA').style.display="none";
  } else {
      document.getElementById('state').disabled=true;
      document.getElementById('state').style.display="none";
      document.getElementById('ca_province').disabled=true;
      document.getElementById('ca_province').style.display="none";
      document.getElementById('province').disabled=false;
      document.getElementById('province').style.display="block";
      document.getElementById('addressStateProvinceNA').disabled=false;
      document.getElementById('stateProvinceNA').style.display="block";
  }
}
toggleStateProvince(document.getElementById('country'));
<c:if test="${siteConfig['SHIPPING_LTL'].value == 'true'}">
var theTips = new TipsX3 ($$('.toolTipImg'), {showDelay: 0, fixed: true});
</c:if>
//-->
</script>

</c:if>
  </tiles:putAttribute>
</tiles:insertDefinition>