<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>

<tiles:insertDefinition name="${_template}" flush="true">
  <tiles:putAttribute name="content" type="string">
 
<c:out value="${model.viewMyBalanceLayout.headerHtml}" escapeXml="false"/>

<form>
<table width="90%" align="center" cellspacing="5" cellpadding="0" border="0">
  <tr>
	<td>
	<div class="pageNavi">Page 
		<select name="page" onchange="submit()">
		<c:forEach begin="1" end="${model.creditHistory.pageCount}" var="page">
  	        <option value="${page}" <c:if test="${page == (model.creditHistory.page+1)}">selected</c:if>>${page}</option>
		</c:forEach>
		</select>
		of <c:out value="${model.creditHistory.pageCount}"/></div>
	</td>
  </tr>
  <tr>
	<td colspan="2">
<table border="0" cellpadding="3" cellspacing="1" width="100%" class="listings"> 
 <tr class="listingsHdr2">
	<td align="center" class="listingsHdr3"><fmt:message key="reference" /> #</td>    
	<td align="center" class="listingsHdr3"><fmt:message key="date" /></td>
	<td align="center" class="listingsHdr3"><fmt:message key="f_amountCredited" /></td>
	<td align="center" class="listingsHdr3"><fmt:message key="f_amountDebited" /></td>
	<td align="center" class="listingsHdr3"><fmt:message key="balance"/></td>
 </tr>
<c:forEach items="${model.creditHistory.pageList}" var="transaction" varStatus="status">
  <c:if test=""></c:if>
  <c:if test=""></c:if>
  <tr bgcolor="<c:if test="${transaction.date != null}">#FFFFFF</c:if><c:if test="${transaction.date == null}">#E0FFFF</c:if>"> 
	<td class="numberCol" align="center" style="white-space: nowrap">
	<c:choose>
		<c:when test="${transaction.reference == 'Order Payment' or transaction.reference == 'Order Payment Cancel'}">	
			<a class="nameLink" href="invoice.jhtm?order=${transaction.transactionId}"><c:out value="${transaction.transactionId}"/></a>
		</c:when>
		<c:otherwise>		
			<c:out value="${transaction.transactionId}"/>
		</c:otherwise>
	</c:choose>
	</td>
    <td class="numberCol" align="center" style="white-space: nowrap"><fmt:formatDate type="date" timeStyle="default" value="${transaction.date}" />
	<td class="numberCol" align="center" style="white-space: nowrap">
	<c:choose>
		<c:when test="${transaction.credit > 0}">
			<fmt:message key="${siteConfig['CURRENCY'].value}" /><fmt:formatNumber value="${transaction.credit}" pattern="#,##0.00" />
		</c:when>
		<c:otherwise>
			<%--Do nothing --%>
		</c:otherwise>
	</c:choose>
	</td>
	<td class="numberCol" align="center" style="white-space: nowrap;color:#FF0000">
	<c:choose>
		<c:when test="${transaction.credit < 0}">
			<fmt:message key="${siteConfig['CURRENCY'].value}" /><fmt:formatNumber value="${transaction.credit}" pattern="#,##0.00" />
		</c:when>
		<c:otherwise>
			<%--Do nothing --%>
		</c:otherwise>
	</c:choose>
	</td>
	<td class="numberCol" align="center" style="white-space: nowrap;font-weight:bold">	
		<fmt:message key="${siteConfig['CURRENCY'].value}" /><fmt:formatNumber value="${transaction.total}" pattern="#,##0.00" />
	</td>
  </tr>
</c:forEach>
</table>
	</td>
  </tr>   
  
</table>
</form>

<c:out value="${model.viewMyBalanceLayout.footerHtml}" escapeXml="false"/>
  </tiles:putAttribute>
</tiles:insertDefinition>
