<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="/WEB-INF/tlds/spring.tld" prefix="spring" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>

<tiles:insertDefinition name="${_template}" flush="true">
  <tiles:putAttribute name="content" type="string">
  
<c:if test="${fn:trim(model.supplierSalesLayout.headerHtml) != ''}">
  <c:set value="${model.supplierSalesLayout.headerHtml}" var="headerHtml"/>
  <c:set value="${fn:replace(headerHtml, '#firstname#', userSession.firstName)}" var="headerHtml"/>
  <c:set value="${fn:replace(headerHtml, '#lastname#', userSession.lastName)}" var="headerHtml"/>
  <c:set value="${fn:replace(headerHtml, '#email#', userSession.username)}" var="headerHtml"/>
  <c:out value="${headerHtml}" escapeXml="false"/>
</c:if>
  
<form:form commandName="packingListForm" method="post">
<form:errors path="*" cssClass="error" />

<c:if test="${message != null}">
  <div class="message"><fmt:message key="${message}" /></div>
</c:if>

<c:if test="${order != null}">
<c:out value="${invoiceLayout.headerHtml}" escapeXml="false"/>

<table border="0" width="100%" cellspacing="3" cellpadding="1">
<tr>
<tr valign="top">
<td>
<b>Billing Information:</b>
<c:set value="${order.billing}" var="address"/>
<%@ include file="/WEB-INF/jsp/frontend/common/address.jsp" %>
</td>
<td>&nbsp;</td>
<td>
<b><c:choose><c:when test="${order.workOrderNum != null}"><fmt:message key="service" /> <fmt:message key="location" /></c:when><c:otherwise><fmt:message key="shippingInformation" /></c:otherwise></c:choose>:</b>
<c:set value="${order.shipping}" var="address"/>
<%@ include file="/WEB-INF/jsp/frontend/common/address.jsp" %>
</td>
<td>&nbsp;</td>
<td align="right">
<table>
  <tr>
    <td><fmt:message key="invoice"/> #</td>
    <td>:&nbsp;&nbsp;</td>
    <td><b><c:out value="${order.orderId}"/></b></td>
  </tr>
  <tr>
    <td><fmt:message key="date" /></td>
    <td>:&nbsp;&nbsp;</td>
    <td><fmt:formatDate type="date" timeStyle="default" value="${order.dateOrdered}"/></td>
  </tr>
  <c:if test="${siteConfig['DELIVERY_TIME'].value == 'true' and !empty order.dueDate}">
  <tr>
    <td><fmt:message key="deliveryTime" /></td>
    <td>:&nbsp;&nbsp;</td>
    <td class="deliveryTime"><fmt:formatDate type="date" timeStyle="default" value="${order.dueDate}"/></td>
  </tr>
  </c:if>
  <c:if test="${customer.accountNumber != '' and customer.accountNumber != null}" >
  <tr>
    <td><fmt:message key="account"/> #</td>
    <td>:&nbsp;&nbsp;</td>
    <td><c:out value="${customer.accountNumber}"/></td>
  </tr>
  </c:if>
  <c:if test="${gSiteConfig['gINVOICE_APPROVAL'] and !empty order.approval}">
  <tr>
    <td><fmt:message key="approval"/></td>
    <td>:&nbsp;&nbsp;</td>
    <td><fmt:message key="approvalStatus_${order.approval}" /></td>
  </tr>  
  </c:if>
</table>
</tr>
</table>
<p>
<div class="emailAddress"><b><fmt:message key="emailAddress" />:</b>&nbsp;<c:out value="${customer.username}"/></div>
<table border="0" cellpadding="2" cellspacing="1" width="100%" class="invoice">
  <tr>
    <c:set var="cols" value="0"/>  
    <th width="5%" class="invoice"><fmt:message key="line" />#</th>
    <th class="invoice"><fmt:message key="productSku" /></th>
    <th class="invoice"><fmt:message key="productName" /></th>
    <th width="10%" class="invoice"><fmt:message key="quantity" /></th>
    <th width="10%" class="invoice"><fmt:message key="shipped" /></th>
    <c:if test="${order.hasPacking}">
      <c:set var="cols" value="${cols+1}"/>
      <th width="10%" class="invoice"><fmt:message key="packing" /></th>
    </c:if>
    <c:if test="${order.hasContent}">
      <c:set var="cols" value="${cols+1}"/>
      <th class="invoice"><fmt:message key="content" /></th>
    </c:if>
    <th class="invoice"><fmt:message key="cost" /></th>
    <th class="invoice"><fmt:message key="productPrice" /><c:if test="${order.hasContent}">/<c:out value="${siteConfig['CASE_UNIT_TITLE'].value}" /></c:if></th>
    <th class="invoice"><fmt:message key="total" /></th>
  </tr>
  <c:set var="subtotal" value="0.0" />
<c:forEach var="lineItem" items="${order.lineItems}" varStatus="status">

<tr valign="top">
  <td class="invoice" align="center"><c:out value="${status.count}"/></td>
  <td class="invoice">
    <c:if test="${siteConfig['SHOW_IMAGE_ON_INVOICE'].value == 'true' and lineItem.product.thumbnail != null}">
	  <img class="invoiceImage" border="0" <c:if test="${!lineItem.product.thumbnail.absolute}"> src="<c:url value="/assets/Image/Product/thumb/${lineItem.product.thumbnail.imageUrl}"/>" </c:if> <c:if test="${lineItem.product.thumbnail.absolute}"> src="<c:url value="${lineItem.product.thumbnail.imageUrl}"/>" </c:if> />
	</c:if><c:out value="${lineItem.product.sku}"/></td>
  <td class="invoice"><c:out value="${lineItem.product.name}"/>
   <table border="0" cellspacing="1" cellpadding="0">
	  <c:forEach items="${lineItem.productAttributes}" var="productAttribute" varStatus="productAttributeStatus">
		<tr class="invoice_lineitem_attributes${productAttributeStatus.count%2}">
		<td class="optionName" style="white-space: nowrap" align="right"><c:out value="${productAttribute.optionName}"/>: </td>
		<td class="optionValue" style="padding-left:5px;"><c:out value="${productAttribute.valueString}" escapeXml="false"/></td>
		<c:if test="${(! empty productAttribute.optionPriceOriginal) && productAttribute.optionPriceOriginal != 0}">
		<td class="optionPrice"><fmt:formatNumber value="${productAttribute.optionPriceOriginal}" pattern="#,##0.00" /></td>
		<td class="optionNote" style="padding-left:5px;"><fmt:message key="${productAttribute.optionPriceMessageF}"/></td>
		</c:if>
		</tr>
	  </c:forEach>
   </table>   	
	<c:if test="${siteConfig['SHOW_IMAGE_ON_INVOICE'].value == 'true' and lineItem.hasProductAttributesImage}" >
	  <c:forEach items="${lineItem.productAttributes}" var="productAttribute">
	    <c:if test="${!empty productAttribute.imageUrl}" > 
	      <c:if test="${!productAttribute.absolute}"><img src="<c:url value="/assets/Image/Product/options/${productAttribute.imageUrl}"/>" border="0" class="invoiceOptionImage" /></c:if>
	      <c:if test="${productAttribute.absolute}"><img src="<c:url value="${productAttribute.imageUrl}"/>" border="0" class="invoiceOptionImage" /></c:if>
	    </c:if>
	  </c:forEach>
	</c:if>
  </td> 
  <td class="invoice" align="center"><c:out value="${lineItem.quantity}"/></td>
  <c:choose>
   <c:when test="${lineItem.processed == lineItem.quantity }">
     <td class="invoice" align="center" style="background-color:#bbff99;"><c:out value="${lineItem.quantity}"/></td>
   </c:when>
   <c:otherwise>
     <td class="invoice" align="center">
       <select name="__quantity_<c:out value='${lineItem.lineNumber}'/>" style="width:65px;">
         <option value="" ></option>
         <option value="<c:out value='${lineItem.quantity}'/>"><c:out value="${lineItem.quantity}"/></option>
       </select> 
     </td>
   </c:otherwise>
  </c:choose>
  <c:if test="${order.hasPacking}">
  <td class="invoice" align="center"><c:out value="${lineItem.product.packing}" /></td>
  </c:if>
  <c:if test="${order.hasContent}">
    <td class="invoice" align="center"><c:out value="${lineItem.product.caseContent}" /></td>
  </c:if>
  <td class="invoice" align="right"><fmt:formatNumber value="${lineItem.consignmentValue}" pattern="#,##0.00" /></td><c:set var="percentTotal" value="${(lineItem.consignmentValue * lineItem.quantity) + percentTotal}" />
  <td class="invoice" align="right"><fmt:formatNumber value="${lineItem.unitPrice}" pattern="#,##0.00"/></td>
  <td class="invoice" align="right"><fmt:formatNumber value="${lineItem.totalPrice}" pattern="#,##0.00"/></td><c:set var="totalSubTotal" value="${lineItem.totalPrice + totalSubTotal}" />
</tr>
<c:if test="${not empty lineItem.trackNum or lineItem.dateShipped != null or not empty lineItem.shippingMethod}">
<tr bgcolor="#FFFFFF">
  <td colspan="${6+cols}">
    <c:if test="${not empty lineItem.shippingMethod}"><c:out value="${lineItem.shippingMethod}"/></c:if>
    <c:if test="${lineItem.dateShipped != null}"><fmt:message key="dateShipped"/>: <fmt:formatDate type="date" value="${lineItem.dateShipped}" pattern="MM/dd/yyyy"/></c:if>
    <c:if test="${not empty lineItem.trackNum}"><fmt:message key="trackNum"/>: <c:out value="${lineItem.trackNum}"/></c:if>
  </td>
</tr>		
</c:if>	

</c:forEach>
  <tr bgcolor="#BBBBBB">
	<td colspan="${8+cols}">&nbsp;</td>
  </tr>
  <tr>
    <td class="invoice" colspan="${7+cols}" align="right"><fmt:message key="subTotal" />:</td>
    <td class="invoice" align="right"><fmt:formatNumber value="${totalSubTotal}" pattern="#,##0.00" /></td>
  </tr>  
</table>

<c:out value="${siteConfig['INVOICE_MESSAGE'].value}" escapeXml="false"/>

<c:if test="${gSiteConfig['gPAYMENTS'] and showPaymentHistory}">
<c:set var="totalPayments" value="0"/>
<c:forEach var="payment" items="${order.paymentHistory}" varStatus="paymentStatus">
<c:if test="${paymentStatus.first}">
<table>
  <tr>
    <td colspan="5"><b><fmt:message key="paymentHistory"/></b>:</td>
  </tr>
</c:if>
  <tr>
    <td><fmt:formatDate type="date" timeStyle="default" value="${payment.date}"/></td>
    <td>&nbsp;</td>
	<td align="right"><fmt:message key="${siteConfig['CURRENCY'].value}" /><fmt:formatNumber value="${payment.amount}" pattern="#,##0.00" /></td>
	<c:set var="totalPayments" value="${totalPayments + payment.amount}"/>
    <td>&nbsp;</td>
	<td><c:if test="${payment.paymentMethod != ''}"><c:out value="${payment.paymentMethod}"/> </c:if><c:out value="${payment.memo}"/></td>
  </tr>
<c:if test="${paymentStatus.last}">
  <tr>
    <td class="status"><b><fmt:message key="total" />:</b></td>
    <td>&nbsp;</td>
	<td align="right" class="status"><b><fmt:message key="${siteConfig['CURRENCY'].value}" /><fmt:formatNumber value="${totalPayments}" pattern="#,##0.00" /></b></td>
    <td colspan="2">&nbsp;</td>
  </tr>
</table>
</c:if>
</c:forEach>
</c:if>

<c:if test="${order.invoiceNote != null and order.invoiceNote != ''}">
	<table width="100%">
		<tr >
		  <td valign="top"><br /> 
		    <b><c:out value="${siteConfig['SPECIAL_INSTRUCTIONS'].value}"/>:</b>  	
		  </td>
		</tr>
		<tr>
		  <td>
		  <c:out value="${order.invoiceNote}" escapeXml="false"/>
		  </td>
		</tr>
	</table>
</c:if>

<tr>
 <td>Consignment Total:</td>
 <td><fmt:formatNumber value="${percentTotal}" pattern="#,##0.00" /></td>
</tr>
<tr>
 <td>Track Number:</td>
 <td><form:input path="orderStatus.trackNum" /></td>
</tr>
<tr>
 <td>Comment:</td>
 <td><form:textarea path="orderStatus.comments" rows="5" cols="80" htmlEscape="true"/></td>
</tr>
<tr>
 <td colspan="2"><input type="submit" name="__status" value="Update Status"></td>
</tr>

<c:out value="${invoiceLayout.footerHtml}" escapeXml="false"/>
</c:if>

</form:form>

<c:if test="${fn:trim(model.supplierSalesLayout.footerHtml) != ''}">
  <c:set value="${model.supplierSalesLayout.footerHtml}" var="footerHtml"/>
  <c:set value="${fn:replace(footerHtml, '#firstname#', userSession.firstName)}" var="footerHtml"/>
  <c:set value="${fn:replace(footerHtml, '#lastname#', userSession.lastName)}" var="footerHtml"/>
  <c:set value="${fn:replace(footerHtml, '#email#', userSession.username)}" var="footerHtml"/>
  <c:out value="${footerHtml}" escapeXml="false"/>
</c:if>

  </tiles:putAttribute>
</tiles:insertDefinition>