<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>

<tiles:insertDefinition name="${_template}" flush="true">
  <tiles:putAttribute name="content" type="string">
  
<br><br><br>
<form method="post">
<table width="90%" align="center" cellspacing="5" cellpadding="0" border="0">
  <tr>
  	<td class="listingsHdr1"><c:out value="${siteConfig['SERVICEABLE_ITEM_TITLE'].value}"/>s</td>
	<td>
<div class="pageNavi">Page 
		<select name="page" onchange="submit()">
		<c:forEach begin="1" end="${model.items.pageCount}" var="page">
  	        <option value="${page}" <c:if test="${page == (model.items.page+1)}">selected</c:if>>${page}</option>
		</c:forEach>
		</select>
of <c:out value="${model.items.pageCount}"/></div>
	</td>
  </tr>
  <tr>
	<td colspan="2">
<table border="0" cellpadding="3" cellspacing="1" width="100%" class="listings"> 
  <tr class="listingsHdr2">
  	<td class="nameCol"><input name="itemId" type="text" value="<c:out value='${model.search.itemId}' />" size="12" /></td>
  	<td class="nameCol"><input name="serialNum" type="text" value="<c:out value='${model.search.serialNum}' />" size="12" /></td>
  	<td class="nameCol"><input name="sku" type="text" value="<c:out value='${model.search.sku}' />" size="12" /></td>
  	<td class="nameCol" colspan="1" align="right"><input type="submit" value="Search" /></td>
  </tr>
  <tr class="listingsHdr2">
    <c:set var="cols" value="0"/>
   	<td class="nameCol"><c:out value="${siteConfig['SERVICEABLE_ITEM_TITLE'].value}"/> ID</td>
  	<td class="nameCol"><fmt:message key="S/N" /></td>
  	<td class="nameCol"><fmt:message key="sku" /></td>
  	<td class="nameCol"><fmt:message key="service" /> <fmt:message key="total" /></td>
  </tr>
<c:forEach items="${model.items.pageList}" var="item" varStatus="status">
  <tr class="row${status.index % 2}">
    <td class="nameCol"><a href="serviceHistory.jhtm?id=${item.id}" class="nameLink"><c:out value="${item.itemId}"/></a></td>	
	<td class="nameCol"><c:out value="${item.serialNum}"/></td>
	<td class="nameCol"><c:out value="${item.sku}"/></td>
	<td class="nameCol">
	  <c:choose>
		<c:when test="${item.serviceTotal > 0}">
		  <a href="account_services.jhtm?itemId=${item.itemId}" class="nameLink"><fmt:message key="${siteConfig['CURRENCY'].value}" /><fmt:formatNumber value="${item.serviceTotal}" pattern="#,##0.00" /></a>
		</c:when>
		<c:otherwise>&nbsp;</c:otherwise>
	  </c:choose></td>
  </tr>
</c:forEach>
<c:if test="${model.items.nrOfElements == 0}">
  <tr class="emptyList"><td colspan="4">&nbsp;</td></tr>
</c:if>
</table>
<table border="0" cellpadding="0" cellspacing="1" width="100%">
  <tr>
  <td align="right">
  <select name="size" onchange="submit()">
    <c:forTokens items="10,25,50" delims="," var="current">
  	  <option value="${current}" <c:if test="${current == model.items.pageSize}">selected</c:if>>${current} <fmt:message key="perPage" /></option>
    </c:forTokens>
  </select>
  </td>
  </tr>
</table> 
	</td>
  </tr>
</table>
</form>

  </tiles:putAttribute>
</tiles:insertDefinition>
