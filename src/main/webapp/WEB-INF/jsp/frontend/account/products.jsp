<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>


<tiles:insertDefinition name="${_template}" flush="true">
  <tiles:putAttribute name="content" type="string">
<c:out value="${model.viewMyProductsLayout.headerHtml}" escapeXml="false"/>
 
  
<!-- 
<c:if test="${gSiteConfig['gCUSTOMER_SUPPLIER'] and model.supplierId != null }">

<form>
<table width="90%" align="center" cellspacing="5" cellpadding="0" border="0">
  <tr>
	<td>
<div class="pageNavi">Page 
		<c:forEach begin="1" end="${model.products.pageCount}" var="page" varStatus="pageStatus">
	      <c:if test="${pageStatus.first}"><select name="page" onchange="submit()"></c:if>
  	        <option value="${page}" <c:if test="${page == (model.products.page+1)}">selected</c:if>>${page}</option>
		  <c:if test="${pageStatus.last}"></select></c:if>
		</c:forEach>
of <c:out value="${model.products.pageCount}"/></div>
	</td>
  </tr>
  <tr>
	<td colspan="2">
<table border="0" cellpadding="3" cellspacing="1" width="100%" class="listings"> 
  <tr class="listingsHdr2">
  	<td class="nameCol"><fmt:message key="dateCreated"/></td>
    <td class="nameCol"><fmt:message key="productSku" /></td>
    <td class="nameCol"><fmt:message key="productName" /></td>
    <td class="nameCol"><fmt:message key="commORFixed" /></td>
    <td class="nameCol"><fmt:message key="note" /></td>
  </tr>
<c:forEach items="${model.products.pageList}" var="product" varStatus="status">
  <tr class="row${status.index % 2}">
  	<td class="nameCol"><fmt:formatDate type="date"  value="${product.productCreatedDate}"/></td>
	  <td class="nameCol"><c:out value="${product.productSku}"/></td>
    <td class="nameCol"><a href="account_product.jhtm?id=${product.productId}" 
	  			class="nameLink"><c:out value="${product.productName}" /></a></td>
	<td class="nameCol">
		<c:choose>
		<c:when test="${product.costPercent == true}">
			<fmt:formatNumber value="${product.cost}" pattern="#,##0.00" />%
		</c:when>
		<c:otherwise>
			$<fmt:formatNumber value="${product.cost}" pattern="#,##0.00" />
		</c:otherwise>
		</c:choose>
    </td>
    <c:set value="${siteConfig['CONSIGNMENT_NOTE_FIELD'].value }" var="field"/>
	<td class="nameCol"><c:out value="${product.field40}"/></td>	
  </tr>
</c:forEach>
<c:if test="${model.products.nrOfElements == 0}">
  <tr class="emptyList"><td colspan="5">&nbsp;</td></tr>
</c:if>
</table>
	</td>
  </tr>
  
<c:if test="${siteConfig['ADD_PRODUCT_ON_FRONTEND'].value == true }">
  <c:if test="${model.canAdd}">  
  <tr>
	<td colspan="2"><input type="submit" name="__add" value="<fmt:message key="f_addProduct" />"></td>
  </tr>
  </c:if>
  <c:if test="${model.canAdd == false}">  
  <tr>
	<td colspan="2" class="error"><fmt:message key="maxProductUploadMet"><fmt:param value="${model.supplierProdLimit}" /></fmt:message></td>
  </tr>
</c:if> 
</c:if>
</table>
</form>

</c:if>
-->


		<div id="accountSettingsWrapper">
		<div class="row">
			<div class="col-sm-12">
				<h2></h2>
			</div>
			<div class="col-sm-6">
				<div id="account_Manager" class="accountSettingsSection">
					<h1><font size="48" color="#4682B4"><i><b><fmt:message key="f_my" /></b></i></font><font size="48" color="#FFA812"><i><b><fmt:message key="f_products"/></b></i></font></h1>					
				</div>
			</div>
			<div class="col-sm-6">
				<div id="account_Manager" class="accountSettingsSection" style="visibility: visible;">
				<div class="row">
					<div class="col-sm-12 col-md-6">
						<div class="accountSettingsLeft">
						</div>
					</div>
					<div class="col-sm-12 col-md-6">
						<div class="accountSettingsRight">
							<img src="assets/responsive/img/liquidatenow_logo.png" class="img-responsive liquidatenow_logo">
						</div>
					</div>
				</div>
				</div>
			</div>				
					
		</div>
		</div>


		<script type="text/javascript" src="/dv/liveapps/scripts.js.php"></script>
        <link rel="stylesheet" type="text/css" href="/dv/liveapps/myaccount.css">

                <div id="lnowproducts">
                <script type="text/javascript">
                <!--
                        $(document).ready(function() {
                                lnowproducts(0,0,${model.userId});
                        });
                //-->
                </script>
        </div>




<c:out value="${model.viewMyProductsLayout.footerHtml}" escapeXml="false"/>
  </tiles:putAttribute>
</tiles:insertDefinition>
