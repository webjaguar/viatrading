<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="/WEB-INF/tlds/spring.tld" prefix="spring" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>

<tiles:insertDefinition name="${_template}" flush="true">
  <tiles:putAttribute name="content" type="string">

<c:if test="${gSiteConfig['gBUDGET_BRAND']}">

<form method="post">
<table width="90%" align="center" cellspacing="5" cellpadding="0" border="0">
<c:if test="${model.subAccount != null}">
  <tr>
  	<td colspan="2">
  	  <table cellspacing="0" cellpadding="0" border="0" class="listingsHdr1">
  	    <tr>
  	      <td><fmt:message key="subAccount" />:</td>
  	      <td>&nbsp;&nbsp;</td>  	      
  	      <td><c:out value="${model.subAccount.username}"/></td>
  	    </tr>
  	    <c:if test="${model.subAccount.address.company != null and model.subAccount.address.company != ''}">
  	    <tr>
  	      <td><fmt:message key="company" />:</td>
  	      <td>&nbsp;&nbsp;</td>  	     
  	      <td><c:out value="${model.subAccount.address.company}"/></td>
  	    </tr>
  	    </c:if>
  	    <tr>
  	      <td colspan="2">&nbsp;</td>  	      
  	      <td><c:out value="${model.subAccount.address.firstName}"/> <c:out value="${model.subAccount.address.lastName}"/></td>
  	    </tr>
  	  </table>  	      
  	</td>
  </tr>
  <tr><td colspan="2">&nbsp;</td></tr>
</c:if>
  <tr>
  	<td class="listingsHdr1"><fmt:message key="reportByBrands" /></td>
	<td align="right">
		<select name="year" onchange="submit()">
		<c:forEach begin="2006" end="${model.currentYear}" var="year">
  	        <option value="${year}" <c:if test="${year == model.orderSearch.year}">selected</c:if>>${year}</option>
		</c:forEach>
		</select>
	</td>
  </tr>
  <tr>
	<td colspan="2">
<table border="0" cellpadding="3" cellspacing="1" width="100%" class="listings"> 
  <tr class="listingsHdr2">
  	<td class="nameCol" rowspan="3"><fmt:message key="orderDate" /></td>
  	<td class="nameCol" rowspan="3"><fmt:message key="invoice" /></td>
  	<c:set var="colspan" value="0"/>
    <c:forEach items="${model.brands}" var="brand">
  	  <c:set var="colspan" value="${colspan + 1}"/>
      <c:if test="${brand.optionName == null}">
	  <td rowspan="3" align="center"><c:out value="${brand.name}"/></td>
      </c:if>
      <c:if test="${brand.optionName != null and brand.colspan1 != null}">
	    <td align="center" colspan="${brand.colspan1}"><c:out value="${brand.name}"/></td>
      </c:if>
    </c:forEach>
    <td class="nameCol" rowspan="3"><fmt:message key="shipping" /></td>
  </tr>
  <tr class="listingsHdr2">
    <c:forEach items="${model.brands}" var="brand">
      <c:if test="${brand.optionName != null and brand.colspan2 != null}">
	    <td align="center" colspan="${brand.colspan2}"><c:out value="${brand.optionName}"/></td>
      </c:if>
    </c:forEach>
  </tr>
  <tr class="listingsHdr2">
    <c:forEach items="${model.brands}" var="brand">
      <c:if test="${brand.optionName != null}">
	    <td align="center"><c:out value="${brand.valueTitle}"/></td>
      </c:if>
    </c:forEach>
  </tr>
<c:set var="shippingTotal" value="0.0" />  
<c:forEach items="${model.orders}" var="order" varStatus="status">
  <tr class="row${status.index % 2}">
    <td class="nameCol"><fmt:formatDate type="date" timeStyle="default" value="${order.dateOrdered}"/></td>			
	<td class="nameCol"><a href="invoice.jhtm?order=${order.orderId}" class="nameLink"><c:out value="${order.orderId}"/></a></td>
    <c:forEach items="${model.brands}" var="brand">
	  <td align="right">
	    <c:if test="${model.brandsMap[order.orderId][brand] != null}">
	      <fmt:message key="${siteConfig['CURRENCY'].value}" /><fmt:formatNumber value="${model.brandsMap[order.orderId][brand]}" pattern="#,##0.00" />
	    </c:if>
	    <c:if test="${model.brandsMap[order.orderId][brand] == null}">-</c:if>
	  </td>
    </c:forEach>
	  <td align="right">
	    <c:if test="${order.shippingCost != null}">
	    <c:set var="shippingTotal" value="${order.shippingCost + shippingTotal}" />
        <fmt:message key="${siteConfig['CURRENCY'].value}" /><fmt:formatNumber value="${order.shippingCost}" pattern="#,##0.00" />
        </c:if>
        <c:if test="${order.shippingCost == null}">-</c:if>
	  </td>
  </tr>
</c:forEach>
<c:if test="${empty model.orders}">
  <tr>
    <td colspan="${3 + colspan}" class="row0">&nbsp;</td>
  </tr>
</c:if>
  <tr class="listingsHdr2">
  	<td colspan="2" align="center"><fmt:message key="total" /></td>
    <c:forEach items="${model.brands}" var="brand">
	<td align="right">
      <c:if test="${brand.total != null}">
        <fmt:message key="${siteConfig['CURRENCY'].value}" /><fmt:formatNumber value="${brand.total}" pattern="#,##0.00" />
      </c:if>
      <c:if test="${brand.total == null}">&nbsp;</c:if>
	</td>
    </c:forEach>
	  <td align="right">
        <fmt:message key="${siteConfig['CURRENCY'].value}" /><fmt:formatNumber value="${shippingTotal}" pattern="#,##0.00" />
	  </td>
  </tr>
  <c:if test="${model.currentYear == model.orderSearch.year}">
  <tr class="listingsHdr2">
  	<td colspan="2" align="center"><fmt:message key="yearlyBudget" /></td>
    <c:forEach items="${model.brands}" var="brand">
	<td align="right">
      <c:if test="${brand.budget != null}">
        <fmt:message key="${siteConfig['CURRENCY'].value}" /><fmt:formatNumber value="${brand.budget}" pattern="#,##0.00" />
      </c:if>
      <c:if test="${brand.budget == null}">&nbsp;</c:if>
	</td>
    </c:forEach>
    <td>&nbsp;</td>
  </tr>
  <tr class="listingsHdr2">
  	<td colspan="2" align="center"><fmt:message key="balance" /></td>
    <c:forEach items="${model.brands}" var="brand">
	<td align="right">
      <fmt:message key="${siteConfig['CURRENCY'].value}" /><fmt:formatNumber value="${brand.balance}" pattern="#,##0.00" />
	</td>
    </c:forEach>
    <td>&nbsp;</td>
  </tr>
  </c:if>
</table>
	</td>
  </tr>
</table>
</form>

</c:if>

  </tiles:putAttribute>
</tiles:insertDefinition>
