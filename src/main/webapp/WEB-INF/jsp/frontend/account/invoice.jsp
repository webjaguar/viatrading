<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ taglib uri="/WEB-INF/tlds/custom.tld" prefix="wj" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<c:choose>
	<c:when test="${quote != true}">
	<c:out value="${invoiceLayout.headTag}" escapeXml="false"/>
	</c:when>
	<c:otherwise>
		<c:out value="${quoteLayout.headTag}" escapeXml="false"/>	
	</c:otherwise>
</c:choose>
<link href="assets/invoice.css" rel="stylesheet" type="text/css"/>
</head>  
<body class="invoice">
<div style="width:700px;margin:auto;">

<c:if test="${message != null}">
  <div class="message"><fmt:message key="${message}" /></div>
</c:if>

<c:if test="${order != null}">
<c:choose>
	<c:when test="${quote != true}">
		<c:out value="${invoiceLayout.headerHtml}" escapeXml="false"/>
	</c:when>
	<c:otherwise>
		<c:out value="${quoteLayout.headerHtml}" escapeXml="false"/>
	</c:otherwise>
</c:choose>


<table border="0" width="100%" cellspacing="3" cellpadding="1">
<tr valign="top">
<td>
<b><fmt:message key="billingInformation" />:</b>
	<c:set value="${order.billing}" var="address"/>
	<%@ include file="/WEB-INF/jsp/frontend/common/address.jsp" %>
</td>

<td>&nbsp;</td>
<td>
<b><c:choose><c:when test="${order.workOrderNum != null}"><fmt:message key="service" /> <fmt:message key="location" /></c:when><c:otherwise><fmt:message key="shippingInformation" /></c:otherwise></c:choose>:</b>
<c:set value="${order.shipping}" var="address"/>
<%@ include file="/WEB-INF/jsp/frontend/common/address.jsp" %>
</td>
<td>&nbsp;</td>
<td align="right">
<table>
  <tr>
    <td>
    <c:choose>
		<c:when test="${quote != true}">
			<fmt:message key="invoiceN"/>
		</c:when>
		<c:otherwise>
			<fmt:message key="quoteN"/>	
		</c:otherwise>
	</c:choose>    
    </td>
    <td>:&nbsp;&nbsp;</td>
    <td><b><c:out value="${order.orderId}"/></b></td>
  </tr>
  <c:if test="${order.externalOrderId != null}">
  <tr>
    <td><fmt:message key="externalOrderId" /></td>
    <td>:&nbsp;&nbsp;</td>
    <td><b><c:out value="${order.externalOrderId}"/></b></td>
  </tr>
  </c:if>
  <c:if test="${order.amazonIopn.amazonOrderID != null}">
  <tr>
    <td>Amazon Order ID</td>
    <td>:&nbsp;&nbsp;</td>
    <td><b><c:out value="${order.amazonIopn.amazonOrderID}"/></b></td>
  </tr>
  </c:if>
  <tr>
    <td><fmt:message key="date" /></td>
    <td>:&nbsp;&nbsp;</td>
    <td><fmt:formatDate type="date" timeStyle="default" value="${order.dateOrdered}"/></td>
  </tr>
  <c:if test="${siteConfig['DELIVERY_TIME'].value == 'true' and !empty order.dueDate and quote != true}">
  <tr>
    <td><fmt:message key="deliveryTime" /></td>
    <td>:&nbsp;&nbsp;</td>
    <td class="deliveryTime"><fmt:formatDate type="date" timeStyle="default" value="${order.dueDate}"/></td>
  </tr>
  </c:if>
  <c:if test="${customer.accountNumber != '' and customer.accountNumber != null}" >
  <tr>
    <td><fmt:message key="account"/> #</td>
    <td>:&nbsp;&nbsp;</td>
    <td><c:out value="${customer.accountNumber}"/></td>
  </tr>
  </c:if>
  <c:choose>
	<c:when test="${quote != true and gSiteConfig['gINVOICE_APPROVAL'] and !empty order.approval}">
	<tr>
	    <td><fmt:message key="approval"/></td>
	    <td>:&nbsp;&nbsp;</td>
	    <td><fmt:message key="approvalStatus_${order.approval}" /></td>
    </tr>
	</c:when>
	<c:otherwise>
	<c:if test="${gSiteConfig['gINVOICE_APPROVAL'] and quote == true}">
	<tr>
	    <td><fmt:message key="approval"/></td>
	    <td>:&nbsp;&nbsp;</td>
	    <td><fmt:message key="approvalStatus_Quote" /></td>
   	</tr>		
	</c:if>
	</c:otherwise>
  </c:choose>
</table>
</tr>
</table>
<br/><br/>
<c:if test="${order.purchaseOrder != '' and order.purchaseOrder != null}" >
<b><fmt:message key="purchaseOrder" /></b>: <c:out value="${order.purchaseOrder}" />
</c:if>
<c:if test="${order.workOrderNum != null}" >
<br/><b><fmt:message key="workOrder" /> #</b>: <c:out value="${order.workOrderNum}" />
</c:if>
<c:if test="${workOrder != null}" >
<br/><b><c:out value="${siteConfig['SERVICEABLE_ITEM_TITLE'].value}"/> ID</b>: <c:out value="${workOrder.service.item.itemId}" />
<br/><b>In SN</b>: <c:out value="${workOrder.inSN}" />
<br/><b><fmt:message key="bwCount"/></b>: <fmt:formatNumber value="${workOrder.bwCount}" pattern="#,##0"/>
<br/><b><fmt:message key="colorCount"/></b>: <fmt:formatNumber value="${workOrder.colorCount}" pattern="#,##0"/>
<br/><b><fmt:message key="problem" /></b>: <c:out value="${workOrder.service.problem}" />
<c:if test="${workOrder.service.trackNumIn != null and workOrder.service.trackNumIn != ''}">
<br/><b>Inbound <fmt:message key="trackNum" /></b>: <c:out value="${workOrder.service.trackNumInCarrier}" />
	<c:choose>
	 <c:when test="${workOrder.service.trackNumInCarrier == 'UPS' or fn:containsIgnoreCase(order.shippingCarrier,'UPS')}">
	  <a href="http://wwwapps.ups.com/WebTracking/processInputRequest?HTMLVersion=5.0&sort_by=status&tracknums_displayed=10&TypeOfInquiryNumber=T&loc=en_US&InquiryNumber1=<c:out value='${workOrder.service.trackNumIn}' />&AgreeToTermsAndConditions=yes"><c:out value="${workOrder.service.trackNumIn}" /></a>
	 </c:when>
	 <c:when test="${workOrder.service.trackNumInCarrier == 'FedEx' or fn:containsIgnoreCase(order.shippingCarrier,'FedEx')}">
	  <a href="http://fedex.com/Tracking?action=track&language=english&cntry_code=us&mps=y&tracknumbers=<c:out value='${workOrder.service.trackNumIn}' />&ascend_header=1"><c:out value="${workOrder.service.trackNumIn}" /></a>
	 </c:when>
	 <c:when test="${workOrder.service.trackNumInCarrier == 'USPS' or fn:containsIgnoreCase(order.shippingCarrier,'USPS')}">
	  <a href="http://trkcnfrm1.smi.usps.com/PTSInternetWeb/InterLabelInquiry.do?strOrigTrackNum=<c:out value='${workOrder.service.trackNumIn}' />"><c:out value="${workOrder.service.trackNumIn}" /></a>
	 </c:when>
	 <c:otherwise><c:out value="${workOrder.service.trackNumIn}" /></c:otherwise>
	</c:choose>	
</c:if>
<c:if test="${workOrder.service.trackNumOut != null and workOrder.service.trackNumOut != ''}">
<br/><b>Outbound <fmt:message key="trackNum" /></b>: <c:out value="${workOrder.service.trackNumOutCarrier}" />
	<c:choose>
	 <c:when test="${workOrder.service.trackNumOutCarrier == 'UPS' or fn:containsIgnoreCase(order.shippingCarrier,'UPS')}">
	  <a href="http://wwwapps.ups.com/WebTracking/processInputRequest?HTMLVersion=5.0&sort_by=status&tracknums_displayed=10&TypeOfInquiryNumber=T&loc=en_US&InquiryNumber1=<c:out value='${workOrder.service.trackNumOut}' />&AgreeToTermsAndConditions=yes"><c:out value="${workOrder.service.trackNumOut}" /></a>
	 </c:when>
	 <c:when test="${workOrder.service.trackNumOutCarrier == 'FedEx' or fn:containsIgnoreCase(order.shippingCarrier,'FedEx')}">
	  <a href="http://fedex.com/Tracking?action=track&language=english&cntry_code=us&mps=y&tracknumbers=<c:out value='${workOrder.service.trackNumOut}' />&ascend_header=1"><c:out value="${workOrder.service.trackNumOut}" /></a>
	 </c:when>
	 <c:when test="${workOrder.service.trackNumOutCarrier == 'USPS' or fn:containsIgnoreCase(order.shippingCarrier,'USPS')}">
	  <a href="http://trkcnfrm1.smi.usps.com/PTSInternetWeb/InterLabelInquiry.do?strOrigTrackNum=<c:out value='${workOrder.service.trackNumOut}' />"><c:out value="${workOrder.service.trackNumOut}" /></a>
	 </c:when>
	 <c:otherwise><c:out value="${workOrder.service.trackNumOut}" /></c:otherwise>
	</c:choose>	
</c:if>			
</c:if>	
<div class="emailAddress"><b><fmt:message key="emailAddress" />:</b>&nbsp;<c:out value="${customer.username}"/></div>
<c:if test="${gSiteConfig['gBUDGET']}">
	<div class="creditAvailable"><b><fmt:message key="credit" /> <fmt:message key="available" />:</b>&nbsp;<fmt:formatNumber value="${customer.credit}" pattern="#,##0.00"/></div>
</c:if>
<table border="0" cellpadding="2" cellspacing="1" width="100%" class="invoice">
  <tr>
    <c:set var="cols" value="0"/>  
    <th width="5%" class="invoice"><fmt:message key="line" />#</th>
    <th class="invoice"><fmt:message key="productSku" /></th>
    <th class="invoice"><fmt:message key="productName" /></th>
    <c:forEach items="${productFieldsHeader}" var="productField">
      <c:if test="${productField.showOnInvoice}">
      <th class="invoice"><c:out value="${productField.name}" /></th><c:set var="cols" value="${cols+1}"/>
      </c:if>
    </c:forEach>
    <th width="10%" class="invoice"><fmt:message key="quantity" /></th>
    <c:if test="${gSiteConfig['gINVENTORY'] && order.hasLowInventoryMessage}">
      <c:set var="cols" value="${cols+1}"/>
	  <th width="10%" class="invoice"><fmt:message key="lowInventory" /></th>
	</c:if>
	<c:if test="${siteConfig['CASE_CONTENT_LAYOUT'].value == ''}">
    <c:if test="${order.hasPacking}">
      <c:set var="cols" value="${cols+1}"/>
      <th width="10%" class="invoice"><fmt:message key="packing" /></th>
    </c:if>
    <c:if test="${order.hasContent}">
      <c:set var="cols" value="${cols+1}"/>
      <th class="invoice"><fmt:message key="content" /></th>
    </c:if>
    </c:if>
    <%-- <th class="invoice"><fmt:message key="productPrice" /><c:if test="${order.hasContent}">/<c:out value="${siteConfig['CASE_UNIT_TITLE'].value}" /></c:if></th> --%>
    <c:choose>
      <c:when test="${siteConfig['CASE_CONTENT_LAYOUT'].value == ''}">
          <th class="invoice"><div><fmt:message key="productPrice" /><c:if test="${order.hasContent}">/<c:out value="${siteConfig['CASE_UNIT_TITLE'].value}" /></c:if></div></th>
      </c:when>
      <c:otherwise>
          <th class="invoice"><div><fmt:message key="productPrice" /></div></th>
      </c:otherwise>
    </c:choose>
    <th class="invoice"><fmt:message key="total" /></th>
  </tr>
<c:set var="previousItemGroup"/>
<c:set var="previousItemIndex" value="0"/>
<c:set var="currentItemIndex"/>

<c:forEach var="lineItem" items="${order.lineItems}" varStatus="status">
<div>
  
  <c:choose>
    <c:when test="${lineItem.itemGroup != null}">
      <c:choose>
        <c:when test="${previousItemGroup != null && previousItemGroup == lineItem.itemGroup}">
          <c:set var="currentItemIndex" value="${previousItemIndex}"/>
        </c:when>
        <c:otherwise>
          <c:set var="currentItemIndex" value="${previousItemIndex + 1}"/>
            <%@ include file="/WEB-INF/jsp/frontend/checkout/subFinalinvoice.jsp" %>
        </c:otherwise>
      </c:choose>
    </c:when>
    <c:otherwise>
      <c:set var="currentItemIndex" value="${previousItemIndex + 1}"/>
    </c:otherwise>
  </c:choose>
  <c:set var="previousItemIndex" value="${currentItemIndex}"/>
  <c:set var="previousItemGroup" value="${lineItem.itemGroup}"/>
  
  
 <%--  
  <c:if test="${lineItem.itemGroup != null and (previousItemGroup == null or previousItemGroup != lineItem.itemGroup)}">
    <%@ include file="/WEB-INF/jsp/frontend/checkout/subFinalinvoice.jsp" %>
  </c:if>
 --%>
</div>
<tr valign="top" class="invoice${currentItemIndex%2}">
  <td class="invoice" align="center"><c:out value="${lineItem.lineNumber}"/></td>
  <td class="invoice">
	<c:if test="${lineItem.customImageUrl != null}">
      <a href="assets/customImages/${order.orderId}/${lineItem.customImageUrl}" onclick="window.open(this.href,'','width=640,height=480,resizable=yes'); return false;"><img class="invoiceImage" src="assets/customImages/${order.orderId}/${lineItem.customImageUrl}" border="0" /></a>
    </c:if>  
    <c:if test="${siteConfig['SHOW_IMAGE_ON_INVOICE'].value == 'true' and lineItem.product.thumbnail != null}">
	  <img class="invoiceImage" border="0" <c:if test="${!lineItem.product.thumbnail.absolute}"> src="<c:url value="/assets/Image/Product/thumb/${lineItem.product.thumbnail.imageUrl}"/>" </c:if> <c:if test="${lineItem.product.thumbnail.absolute}"> src="<c:url value="${lineItem.product.thumbnail.imageUrl}"/>" </c:if> />
	</c:if><c:out value="${lineItem.product.sku}"/></td>
  <td class="invoice"><c:if test="${lineItem.customXml != null}">Custom Frame - </c:if><c:out value="${lineItem.product.name}"/>
   <table border="0" cellspacing="1" cellpadding="0">
	  <c:forEach items="${lineItem.productAttributes}" var="productAttribute" varStatus="productAttributeStatus">
		<tr class="invoice_lineitem_attributes${productAttributeStatus.count%2}">
		<td class="optionName" style="white-space: nowrap" align="right"><c:out value="${productAttribute.optionName}"/>: </td>
		<td class="optionValue" style="padding-left:5px;"><c:out value="${productAttribute.valueString}" escapeXml="false"/></td>
		<c:if test="${(! empty productAttribute.optionPriceOriginal) && productAttribute.optionPriceOriginal != 0}">
		<td class="optionPrice"><fmt:formatNumber value="${productAttribute.optionPriceOriginal}" pattern="#,##0.00" /></td>
		<td class="optionNote" style="padding-left:5px;"><fmt:message key="${productAttribute.optionPriceMessageF}"/></td>
		</c:if>
		</tr>
	  </c:forEach>
	  <%@ include file="/WEB-INF/jsp/frontend/common/customLinesInvoice.jsp"%>
   </table>
	<c:if test="${lineItem.subscriptionInterval != null}">
	  <c:set var="intervalType" value="${fn:substring(lineItem.subscriptionInterval, 0, 1)}"/>
	  <c:set var="intervalUnit" value="${fn:substring(lineItem.subscriptionInterval, 1, 2)}"/>
	  <div class="invoice_lineitem_subscription">
		<fmt:message key="subscription"/> <fmt:message key="details"/> : <fmt:message key="delivery"/>
		<c:choose>
		  <c:when test="${intervalUnit == '1'}"><fmt:message key="every_${intervalType}"><fmt:param value="${intervalUnit}"/></fmt:message></c:when>
		  <c:otherwise><fmt:message key="every_${intervalType}s"><fmt:param value="${intervalUnit}"/></fmt:message></c:otherwise>
		</c:choose>
	  </div>
	  <div class="invoice_lineitem_subscription"><c:out value="${lineItem.subscriptionCode}"/></div>
	</c:if>
	<c:if test="${siteConfig['SHOW_IMAGE_ON_INVOICE'].value == 'true' and lineItem.hasProductAttributesImage}" >
	  <c:forEach items="${lineItem.productAttributes}" var="productAttribute">
	    <c:if test="${!empty productAttribute.imageUrl}" > 
	      <c:if test="${!productAttribute.absolute}"><img src="<c:url value="/assets/Image/Product/options/${productAttribute.imageUrl}"/>" border="0" class="invoiceOptionImage" /></c:if>
	      <c:if test="${productAttribute.absolute}"><img src="<c:url value="${productAttribute.imageUrl}"/>" border="0" class="invoiceOptionImage" /></c:if>
	    </c:if>
	  </c:forEach>
	</c:if>
  </td> 
  <c:forEach items="${productFieldsHeader}" var="pFHeader" varStatus="status"><c:set var="check" value="0"/>
  <c:if test="${pFHeader.showOnInvoice}">
  <c:forEach items="${lineItem.productFields}" var="productField"> 
   <c:if test="${pFHeader.id == productField.id}">
	<c:choose>
		<c:when test="${productField.formatType != null and productField.formatType != '' and productField.fieldType == 'number' }">
			<td class="details_field_value_row${row.index % 2} invoice">
			<c:catch var="catchFormatterException">
				<fmt:formatNumber value="${wj:trimAndSplit(productField.value, '|' , false, ',', true, true)}" pattern="${productField.formatType}" />
			</c:catch>
			<c:if test = "${catchFormatterException != null}"><c:out value="${wj:trimAndSplit(productField.value, '|' , false, ',', true, true)}" escapeXml="false"/></c:if>
			</td><c:set var="check" value="1"/>
		</c:when>
		<c:otherwise>
			<td class="details_field_value_row${row.index % 2} invoice"><c:out value="${wj:trimAndSplit(productField.value, '|' , false, ',', true, true)}" escapeXml="false"/></td><c:set var="check" value="1"/>
		</c:otherwise>
	</c:choose>  
   </c:if>         
  </c:forEach>
  <c:if test="${check == 0}">
    <td class="invoice">&nbsp;</td>
   </c:if>
  </c:if>
  </c:forEach>  
  <td class="invoice" align="center"><c:out value="${lineItem.quantity}"/><c:if test="${lineItem.priceCasePackQty != null}"><div class="casePackPriceQty">(<c:out value="${lineItem.priceCasePackQty}"/> per <c:out value="${siteConfig['PRICE_CASE_PACK_UNIT_TITLE'].value}" />)</div></c:if></td>
  <c:if test="${gSiteConfig['gINVENTORY'] && order.hasLowInventoryMessage}">
	<td class="invoice" align="center"><c:out value="${lineItem.lowInventoryMessage}" /></td>
  </c:if>
  <c:choose>
	<c:when test="${siteConfig['CASE_CONTENT_LAYOUT'].value == '1'}">
   		<c:choose>
	  		<c:when test="${lineItem.product.caseContent != null}">
	  	 		 <td class="invoice" align="right"><fmt:formatNumber value="${lineItem.unitPrice * lineItem.product.caseContent}" pattern="#,##0.00"/></td>
	 	 	</c:when>
	 	 	<c:otherwise>
	 	 		<td class="invoice" align="right"><fmt:formatNumber value="${lineItem.unitPrice}" pattern="#,##0.00#" maxFractionDigits="${siteConfig['MAX_FRACTION_DIGIT'].value}"/></td>
	 	 	</c:otherwise>
	  	</c:choose>
    </c:when>
    <c:when test="${siteConfig['CASE_CONTENT_LAYOUT'].value == '2'}">
    	<td class="invoice" align="right"><fmt:formatNumber value="${lineItem.unitPrice}" pattern="#,##0.00#" maxFractionDigits="${siteConfig['MAX_FRACTION_DIGIT'].value}"/></td>
    </c:when>
	<c:otherwise>
	  	<c:if test="${order.hasPacking }">
		  <td class="invoice" align="center"><c:out value="${lineItem.product.packing}" /></td>
		</c:if>
		<c:if test="${order.hasContent}">
		    <td class="invoice" align="center"><c:out value="${lineItem.product.caseContent}" /></td>
		</c:if>
		<td class="invoice" align="right"><fmt:formatNumber value="${lineItem.unitPrice}" pattern="#,##0.00#" maxFractionDigits="${siteConfig['MAX_FRACTION_DIGIT'].value}"/></td> 	
	</c:otherwise>
  </c:choose>
  <td class="invoice" align="right"><fmt:formatNumber value="${lineItem.totalPrice}" pattern="#,##0.00"/></td>
</tr>
<c:if test="${not empty lineItem.trackNum or lineItem.dateShipped != null or not empty lineItem.shippingMethod or not empty lineItem.status}">
<tr bgcolor="#FFFFFF">
  <td colspan="${6+cols}">
    <c:if test="${not empty lineItem.status}"><fmt:message key="status"/>: <c:out value="${lineItem.status}"/></c:if>
    <c:if test="${not empty lineItem.shippingMethod}"><c:out value="${lineItem.shippingMethod}"/></c:if>
    <c:if test="${lineItem.dateShipped != null}"><fmt:message key="dateShipped"/>: <fmt:formatDate type="date" value="${lineItem.dateShipped}" pattern="MM/dd/yyyy"/></c:if>
    <c:if test="${not empty lineItem.trackNum}"><fmt:message key="trackNum"/>: <c:out value="${lineItem.trackNum}"/></c:if>
  </td>
</tr>		
</c:if>	
</c:forEach>
  <tr bgcolor="#BBBBBB">
	<td colspan="${6+cols}">&nbsp;</td>
  </tr>
	  <tr>
	    <td class="invoice" colspan="${5+cols}" align="right"><fmt:message key="subTotal" />:</td>
	    <td class="invoice" align="right"><fmt:formatNumber value="${order.subTotal}" pattern="#,##0.00"/></td>
	  </tr>
  	  <c:if test="${order.budgetEarnedCredits > 0.00 }">
	  	<tr class="customerEarnedCreditsBoxId">
	  		<td class="discount" colspan="${5+cols}" align="right">Earned Credits: </td>
	  		<td class="discount" align="right"><fmt:formatNumber value="${order.budgetEarnedCredits}" pattern="-#,##0.00"/></td>
	  	</tr>
      </c:if>
	  <c:if test="${order.promo.title != null and order.promo.discountType eq 'order'}" >
		<tr>
		  <td class="discount" colspan="${5+cols}" align="right">
	      <c:choose>
	        <c:when test="${order.promo.discount == 0}"><fmt:message key="promoCode" /></c:when>
	        <c:otherwise><fmt:message key="discountForPromoCode" /> </c:otherwise>
	      </c:choose>
	      <c:out value="${order.promo.title}" />
		    <c:choose>
		      <c:when test="${order.promo.discount == 0}"></c:when>
	          <c:when test="${order.promo.percent}">
	            (<fmt:formatNumber value="${order.promo.discount}" pattern="#,##0.00"/>%)
	          </c:when>
	          <c:otherwise>
	            (<fmt:formatNumber value="${order.promo.discount}" pattern="#,##0.00"/>)
	          </c:otherwise>
	      	</c:choose>	    
		    </td>
		    <td class="discount" align="right">
		    <c:choose>
		      <c:when test="${order.promo.discount == 0}">&nbsp;</c:when>
	          <c:when test="${order.promo.percent}">
	            (<fmt:formatNumber value="${order.subTotal * ( order.promo.discount / 100.00 )}" pattern="#,##0.00"/>)
	          </c:when>
	          <c:otherwise>
	            (<fmt:formatNumber value="${order.promo.discount}" pattern="#,##0.00"/>)
	          </c:otherwise>
	      	</c:choose>	    
		    </td>
		  </tr>
	  </c:if>
	  <c:choose>
   		<c:when test="${order.taxOnShipping}">
   		  <tr>
	    	<td class="invoice" colspan="${5+cols}" align="right"><fmt:message key="shipping" /> (<c:out value="${order.shippingMethod}"/>):</td>
	        <td class="invoice" align="right"><fmt:formatNumber value="${order.shippingCost}" pattern="#,##0.00"/></td>
	  	  </tr>
	 	  <tr>
	    	<td class="invoice" colspan="${5+cols}" align="right"><fmt:message key="tax" />:</td>
	    	<td class="invoice" align="right"><fmt:formatNumber value="${order.tax}" pattern="#,##0.00"/></td>
	  	  </tr>
	  	</c:when>
   		<c:otherwise>
   		  <tr>
	    	<td class="invoice" colspan="${5+cols}" align="right"><fmt:message key="tax" />:</td>
	    	<td class="invoice" align="right"><fmt:formatNumber value="${order.tax}" pattern="#,##0.00"/></td>
	  	  </tr>
	  	  <tr>
	    	<td class="invoice" colspan="${5+cols}" align="right"><fmt:message key="shipping" /> (<c:out value="${order.shippingMethod}"/>):</td>
	    	<td class="invoice" align="right"><fmt:formatNumber value="${order.shippingCost}" pattern="#,##0.00"/></td>
	  	  </tr>
	    </c:otherwise>
      </c:choose>
   	  <c:if test="${order.promo.title != null and order.promo.discountType eq 'shipping' and order.shippingCost != null}" >
	    <tr>
		  <td class="discount" colspan="${5+cols}" align="right">
	      <c:choose>
	        <c:when test="${order.promo.discount == 0}"><fmt:message key="promoCode" /></c:when>
	        <c:otherwise><fmt:message key="discountForPromoCode" /> </c:otherwise>
	      </c:choose>
	      <c:out value="${order.promo.title}" />
		  <c:choose>
		    <c:when test="${order.promo.discount == 0}"></c:when>
			<c:when test="${order.promo.percent}">
			  (<fmt:formatNumber value="${order.promo.discount}" pattern="#,##0.00"/>%)
			</c:when>
			<c:otherwise>
			  (<fmt:message key="${siteConfig['CURRENCY'].value}" /><fmt:formatNumber value="${order.promo.discount}" pattern="#,##0.00"/>)
			</c:otherwise>
		  </c:choose>	    
		  </td>
		  <td class="discount" align="right">
		    <c:choose>
			<c:when test="${order.promo.discount == 0}">&nbsp;</c:when>
			<c:when test="${order.promo.percent}">
			  (<fmt:formatNumber value="${order.shippingCost * ( order.promo.discount / 100.00 )}" pattern="#,##0.00"/>)
			</c:when>
			<c:otherwise>
			 (<fmt:formatNumber value="${order.promo.discount}" pattern="#,##0.00"/>)
			</c:otherwise>
		  </c:choose>	    
		  </td>
		</tr>
	  </c:if>
	  <c:if test="${order.customShippingCost != null}">
	  <tr>
	    <td class="invoice" colspan="${5+cols}" align="right"><fmt:message key="customShipping" /> (<c:out value="${order.customShippingTitle}" escapeXml="false"/>):</td>
	    <td class="invoice" align="right"><fmt:formatNumber value="${order.customShippingCost}" pattern="#,##0.00"/></td>
	  </tr>
	  </c:if>
	  <c:if test="${order.ccFee != null && order.ccFee != 0}">
	    <td class="invoice" colspan="${5+cols}" align="right"><fmt:message key="creditCardFee" />:</td>
	    <td class="invoice" align="right"><fmt:formatNumber value="${order.ccFee}" pattern="#,##0.00"/></td>
	  </c:if>
	  <c:choose>
	    <c:when test="${gSiteConfig['gBUDGET'] and order.requestForCredit != null and order.creditUsed == null and customer.creditAllowed != null and customer.creditAllowed > 0}">
	    	<tr id="customerCreditBoxId">
		    	<td class="discount" colspan="${5+cols}" align="right">		    	
	    		<fmt:message key="creditRequestAmount">
					<fmt:param>${customer.creditAllowed}</fmt:param>
					<fmt:param><fmt:formatNumber value="${subTotalByPlan}" pattern="#,##0.00"/></fmt:param>
					<fmt:param><fmt:formatNumber value="${(customer.creditAllowed / 100) * subTotalByPlan}" pattern="#,##0.00"/></fmt:param>
		    	</fmt:message>
		    	<br/> 
		    	<fmt:message key="creditApprovalMessage" />
		    	</td>
		    	<c:choose>
	  				<c:when test="${order.requestForCredit > 0 }">
  						<td class="discount" align="right"><fmt:formatNumber value="${order.requestForCredit}" pattern="-#,##0.00"/></td>
	  				</c:when>
	  				<c:otherwise>
	  					<td class="discount" align="right">No credit</td>
	  				</c:otherwise>
	  			</c:choose>
		    </tr>
	  </c:when>
	  <c:when test="${(gSiteConfig['gGIFTCARD'] or gSiteConfig['gVIRTUAL_BANK_ACCOUNT'] or gSiteConfig['gBUDGET']) and order.creditUsed != null}">
		  <tr id="customerCreditBoxId">
		    <td class="discount" colspan="${5+cols}" align="right"><fmt:message key="credit" />:</td>
		    <td class="discount" align="right"><fmt:formatNumber value="${order.creditUsed}" pattern="-#,##0.00"/></td>
		  </tr>
	  </c:when>
      </c:choose>
	  <c:if test="${order.bondCost != null}">
	  <tr>
	    <td class="invoice" colspan="${5+cols}" align="right"><fmt:message key="buySafeBondGuarntee" />&nbsp; <a href="http://www.buysafe.com/questions">Questions?</a>:</td>
	    <td class="invoice" align="right"><fmt:message key="${siteConfig['CURRENCY'].value}" /><fmt:formatNumber value="${order.bondCost}" pattern="#,##0.00"/></td>
	  </tr>
	  </c:if>
  <tr>
    <td class="invoice" colspan="${5+cols}" align="right"><fmt:message key="grandTotal" />:</td>
    <td align="right" bgcolor="#F8FF27"><fmt:message key="${siteConfig['CURRENCY'].value}" /><fmt:formatNumber value="${order.grandTotal}" pattern="#,##0.00"/></td>
  </tr>
</table>
<c:out value="${siteConfig['INVOICE_MESSAGE'].value}" escapeXml="false"/>
<br/><br/>
<div class="partnersBox">
	<c:if test="${partnerList != null and fn:length(partnerList) gt 0}">
    	<h1><fmt message key="partnersList"/></h1>
    	<c:forEach items="${partnerList}" var="partner">
    		<div class="partnerWrapper">
    		<c:out value="${partner.partnerName}"/>
    		<c:out value="${partner.amount}"/>
    		</div>
    	</c:forEach>
    	<div><fmt:message key="total"/>: <fmt:message key="${siteConfig['CURRENCY'].value}" /><fmt:formatNumber value="${order.budgetEarnedCredits}" pattern="#,##0.00"/></div>
    </c:if>
</div>
<table>
  <tr>
    <td>
    <c:if test="${quote != true }">
	    <b><fmt:message key="paymentMethod" />:</b> <c:choose><c:when test="${order.paymentMethod == 'GE Money'}"><c:out value="${siteConfig['GEMONEY_PROGRAM_NAME'].value}"/></c:when><c:otherwise><c:out value="${order.paymentMethod}" /></c:otherwise></c:choose>
	    <p>
	    <c:if test="${order.creditCard != null}">
	    <c:set value="${order.creditCard}" var="creditCard"/>
	    <%@ include file="/WEB-INF/jsp/frontend/common/creditcard.jsp" %>
	    </c:if>
    </c:if>
    </td>
  </tr>
</table>
<c:if test="${order.geMoney.acctNumber != null and order.geMoney.acctNumber != '' and quote != true}">
<fmt:message key="accountNumber" />: <c:out value="${order.geMoney.acctNumber}" />
</c:if>
<c:if test="${gSiteConfig['gPAYMENTS'] and showPaymentHistory and quote != true}">
<c:set var="totalPayments" value="0"/>
<c:forEach var="payment" items="${order.paymentHistory}" varStatus="paymentStatus">
<c:if test="${paymentStatus.first}">
<table>
  <tr>
    <td colspan="5"><b><fmt:message key="paymentHistory"/></b>:</td>
  </tr>
</c:if>
  <tr>
    <td><fmt:formatDate type="date" timeStyle="default" value="${payment.date}"/></td>
    <td>&nbsp;</td>
	<td align="right"><fmt:message key="${siteConfig['CURRENCY'].value}" /><fmt:formatNumber value="${payment.amount}" pattern="#,##0.00" /></td>
	<c:set var="totalPayments" value="${totalPayments + payment.amount}"/>
    <td>&nbsp;</td>
	<td><c:if test="${payment.paymentMethod != ''}"><c:out value="${payment.paymentMethod}"/> </c:if><c:out value="${payment.memo}"/></td>
  </tr>
<c:if test="${paymentStatus.last}">
  <tr>
    <td class="status"><b><fmt:message key="total" />:</b></td>
    <td>&nbsp;</td>
	<td align="right" class="status"><b><fmt:message key="${siteConfig['CURRENCY'].value}" /><fmt:formatNumber value="${totalPayments}" pattern="#,##0.00" /></b></td>
    <td colspan="2">&nbsp;</td>
  </tr>
</table>
</c:if>
</c:forEach>
</c:if>

<c:if test="${order.invoiceNote != null and order.invoiceNote != ''}">
	<table width="100%">
		<tr >
		  <td valign="top"><br /> 
		    <b><c:out value="${siteConfig['SPECIAL_INSTRUCTIONS'].value}"/>:</b>  	
		  </td>
		</tr>
		<tr>
		  <td>
		  <c:out value="${order.invoiceNote}" escapeXml="false"/>
		  </td>
		</tr>
	</table>
</c:if>
<c:if test="${hasTrackNum and quote != true}" >
<table>
  <tr>
    <td valign="top"><b><fmt:message key="trackNum"/>:</b></td>
    <td valign="top"><table>
      <c:forEach var="trackNum" items="${trackNumList}" varStatus="trackNumStatus">
        <tr><td>
        <c:choose>
         <c:when test="${fn:containsIgnoreCase(order.shippingMethod,'ups') or fn:containsIgnoreCase(order.shippingCarrier,'ups')}">
          <a href="http://wwwapps.ups.com/WebTracking/processInputRequest?HTMLVersion=5.0&sort_by=status&tracknums_displayed=10&TypeOfInquiryNumber=T&loc=en_US&InquiryNumber1=<c:out value='${trackNum}' />&AgreeToTermsAndConditions=yes"><c:out value="${trackNum}" /></a>
         </c:when>
         <c:when test="${fn:containsIgnoreCase(order.shippingMethod,'fedex') or fn:containsIgnoreCase(order.shippingCarrier,'fedex')}">
          <a href="http://fedex.com/Tracking?action=track&language=english&cntry_code=us&mps=y&tracknumbers=<c:out value='${trackNum}' />&ascend_header=1"><c:out value="${trackNum}" /></a>
         </c:when>
         <c:when test="${fn:containsIgnoreCase(order.shippingMethod,'usps') or fn:containsIgnoreCase(order.shippingCarrier,'usps')}">
          <a href="http://trkcnfrm1.smi.usps.com/PTSInternetWeb/InterLabelInquiry.do?strOrigTrackNum=<c:out value='${trackNum}' />"><c:out value="${trackNum}" /></a>
         </c:when>
         <c:otherwise><c:out value="${trackNum}" /></c:otherwise>
        </c:choose>
        </td></tr>
      </c:forEach>
    </table></td>
  </tr>
</table>    
</c:if>
<c:choose>
<c:when test="${quoteApproval == null}">
	<c:choose>
	<c:when test="${quote != true}">
		<c:if test="${order.approval == 'ra'}">
		<form action="invoice.jhtm?order=<c:out value="${order.orderId}"/>" method="post">
			<table class="approvalTable" width="100%">
				<tr>
				  <td colspan="2" valign="top"><br /> 
				    <b><fmt:message key="note" />:</b>  	
				  </td>
				</tr>
				<tr>
				  <td colspan="2">
				    <textarea name="_actionNote" cols="50" rows="5" ></textarea>
				  </td>
				</tr>
				<tr>
				  <td>
				    <fmt:message key="checkToApprove" />
				    <input type="checkbox" name="_approvalStatus" value="true"/>
				  </td>
				  <td>
			        <input type="submit" name="_submit" />
				  </td>
				</tr>
			</table>
		</form>	
		</c:if>
	</c:when>
	<c:otherwise>
		<c:if test="${quote == true and order.approval != 'ap'}">
		<form action="invoice.jhtm?quote=<c:out value="${order.orderId}"/>" method="post">
		<input type="hidden" name="quoteApproval" value="${quoteApproval}"/>
			<table class="approvalTable" width="100%">
				<tr>
				  <td colspan="2" valign="top"><br /> 
				    <b><fmt:message key="note" />:</b>  	
				  </td>
				</tr>
				<tr>
				  <td colspan="2">
				    <textarea name="_actionNote" cols="50" rows="5" ></textarea>
				  </td>
				</tr>
				<tr>
				  <td>
				    <fmt:message key="checkToApprove" />
				    <input type="checkbox" name="_approvalStatus" value="1"/>
				  </td>
				  <td>
				    <fmt:message key="checkToDenial" />
				    <input type="checkbox" name="_approvalStatus" value="2"/>
				  </td>
				  <td>
			        <input type="submit" name="_submitQuote" value="Submit"/>
				  </td>
				</tr>
			</table>
		</form>	
		</c:if>
	</c:otherwise>
	</c:choose>
</c:when>
<c:otherwise>
	<div class="approvalStatus">
		<c:out value="${quoteApproval}"/>
	</div>
</c:otherwise>
</c:choose>
<c:choose>
	<c:when test="${quote != true}">	
		<c:out value="${invoiceLayout.footerHtml}" escapeXml="false"/>
	</c:when>
	<c:otherwise>
		<c:out value="${quoteLayout.footerHtml}" escapeXml="false"/>	
	</c:otherwise>
</c:choose>
</c:if>

</div>

</body>
</html>