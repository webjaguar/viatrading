<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="/WEB-INF/tlds/spring.tld" prefix="spring" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<tiles:insertDefinition name="${_template}" flush="true">
  <tiles:putAttribute name="content" type="string">

<!--<form method="post" id="list">
<input type="hidden" id="sort" name="sort" value="${orderSearch.sort}" /> 
<table width="90%" align="center" cellspacing="6" cellpadding="0" border="0">
  <tr>
  	<td class="listingsHdr1"><fmt:message key="f_viewRetrieveManifests" /></td>
	<td>
	<div class="pageNavi">Page 
		<select name="page" onchange="submit()">
		<c:forEach begin="1" end="${model.orders.pageCount}" var="page">
  	        <option value="${page}" <c:if test="${page == (model.orders.page+1)}">selected</c:if>>${page}</option>
		</c:forEach>
		</select>
	of <c:out value="${model.orders.pageCount}"/></div>
	</td>
  </tr>
  <tr>
	<td colspan="2">
		<table border="0" cellpadding="3" cellspacing="1" width="100%" class="listings">
		  <tr class="listingsHdr2">
		  	<td class="nameCol">&nbsp;</td>
		  	<td class="nameCol">&nbsp;</td>
		    <td class="nameCol" colspan="2">
	      		<select name="status">
		  		<option value=""><fmt:message key="allOrders" /></option>
		  		<option value="p" <c:if test="${model.orderSearch.status == 'p'}">selected</c:if>><fmt:message key="orderStatus_p" /></option>
	  	    	<option value="pr" <c:if test="${model.orderSearch.status == 'pr'}">selected</c:if>><fmt:message key="orderStatus_pr" /></option>
	  	    	<option value="s" <c:if test="${model.orderSearch.status == 's'}">selected</c:if>><fmt:message key="orderStatus_s" /></option>
	  	    	<option value="x" <c:if test="${model.orderSearch.status == 'x'}">selected</c:if>><fmt:message key="orderStatus_x" /></option>
	      		</select>
	    	</td>
  	 		<td class="nameCol" colspan="2" align="right"><input type="submit" value="Search" /></td>
  	 	  </tr>
		  <tr class="listingsHdr2">
		  	  <c:if test="${fn:contains(siteConfig['CUSTOMIZE_VIEW_MY_ORDER_HISTORY_LIST'].value,'orderDate')}">
	  			<td>
		   			<table cellspacing="0" cellpadding="1">
		   	 		 <tr>
		   	   		 <c:choose>
				   			<c:when test ="${model.orderSearch.sort == 'date_ordered desc' }">
					  			<td class="listingsHdr3">
					 		 	<a class="sortLink" href="#" onClick="document.getElementById('sort').value='0';document.getElementById('list').submit()"> <fmt:message key="orderDate" /></a>
							  	</td>
							  	<td><img src="/assets/Image/Layout/up.gif" border="0"></td>
				  			</c:when>
				  			<c:when test ="${model.orderSearch.sort == 'date_ordered' }">
							  	<td class="listingsHdr3">
							  	<a class="sortLink" href="#" onClick="document.getElementById('sort').value='1';document.getElementById('list').submit()"> <fmt:message key="orderDate" /></a>				  	
							  	</td>
							  	<td><img src="/assets/Image/Layout/down.gif" border="0"></td>
				  			</c:when>
				  			<c:otherwise>
					 		 	<td class="listingsHdr3">
					 		 	<a class="sortLink" href="#" onClick="document.getElementById('sort').value='1';document.getElementById('list').submit()"> <fmt:message key="orderDate" /></a>
					  			</td>	
					  			<td><img src="../assets/Image/Layout/down.gif" border="0"></td>			  
				  			</c:otherwise>
			  			</c:choose>
		   	 		 </tr>
		   			 </table>
	    		</td> 
   		 	</c:if>
		  	<td class="nameCol"><fmt:message key="invoice" /></td>
		  	<td class="nameCol"><fmt:message key="sku" /></td>
		  	<td class="nameCol"><fmt:message key="name" /></td>
		  	<td class="nameCol"><fmt:message key="quantity" /></td>
		  	<td class="nameCol"><fmt:message key="unitPrice" /></td>
		  	
		  </tr>
		  
		  <c:forEach items="${model.orders.pageList}" var="order" varStatus="status">
		  	<c:forEach items="${order.lineItems }" var="lineItem">
		  <tr class="row${status.index % 2}">	
			<td class="nameCol"><fmt:formatDate type="date" timeStyle="default" value="${order.dateOrdered}"/></td>
			<td class="nameCol"><c:out value="${order.orderId }"></c:out></td>
			<td class="nameCol">
			<c:choose>
				<c:when test="${lineItem.product.belongsToLoadCenter}"><a href="https://www.viatrading.com/dv/manifest/manifestdetails.php?SKU=${lineItem.product.sku}&ID=${lineItem.product.id}" ><c:out value="${lineItem.product.sku }"></c:out></a></c:when>
				<c:otherwise><c:out value="${lineItem.product.sku }"></c:out></c:otherwise>
			</c:choose>
			</td>
			<td class="nameCol"><c:out value="${lineItem.product.name }"></c:out></td>
			<td class="nameCol"><c:out value="${lineItem.quantity }"></c:out></td>
			<td align="nameCol"><fmt:formatNumber value="${lineItem.unitPrice}" pattern="$##,###.00" /></td>	

		  </tr>
		  </c:forEach>
		  </c:forEach>
		</table>
	</td>
  </tr>
</table>
</form>-->


		<script type="text/javascript" src="/dv/liveapps/scripts.js.php"></script>
        <link rel="stylesheet" type="text/css" href="/dv/liveapps/myaccount.css">
	
		<div id="viewmanifests">
		<script type="text/javascript">
		<!--
			$(document).ready(function() {
				viewmanifests(0,0,${model.userId});
			});
		//-->
		</script>
		
		
		</div>




  </tiles:putAttribute>
</tiles:insertDefinition>