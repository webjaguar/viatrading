<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="/WEB-INF/tlds/spring.tld" prefix="spring" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>

<tiles:insertDefinition name="${_template}" flush="true">
  <tiles:putAttribute name="content" type="string">

<c:if test="${gSiteConfig['gSUBSCRIPTION']}">
<c:out value="${model.subscriptionLayout.headerHtml}" escapeXml="false"/>

<form method="post">
<table width="90%" align="center" cellspacing="5" cellpadding="0" border="0">
  <tr>
  	<td>&nbsp;</td>
	<td>
<div class="pageNavi">Page 
		<select name="page" onchange="submit()">
		<c:forEach begin="1" end="${model.list.pageCount}" var="page">
  	        <option value="${page}" <c:if test="${page == (model.list.page+1)}">selected</c:if>>${page}</option>
		</c:forEach>
		</select>
of <c:out value="${model.list.pageCount}"/></div>
	</td>
  </tr>
  <tr>
	<td colspan="2">
<table border="0" cellpadding="3" cellspacing="1" width="100%" class="listings"> 
  <tr class="listingsHdr2">
    <td class="nameCol"><select name="enabled">
        <option value=""><fmt:message key="all" /></option> 
        <option value="true" <c:if test="${model.subscriptionSearch.enabled == 'true'}">selected</c:if>><fmt:message key="yes" /></option>
        <option value="false" <c:if test="${model.subscriptionSearch.enabled == 'false'}">selected</c:if>><fmt:message key="no" /></option>
      </select>
    </td>
  	<td class="nameCol"><input name="code" type="text" value="<c:out value='${model.subscriptionSearch.code}' />" size="18" /></td>
  	<td class="nameCol" colspan="7" align="right"><input type="submit" value="Search" /></td>
  </tr>
  <tr class="listingsHdr2">
  	<td align="center"><fmt:message key="Enabled" /></td>
  	<td class="nameCol"><fmt:message key="code" /></td>
  	<td class="nameCol"><fmt:message key="dateAdded" /></td>
  	<td class="nameCol"><fmt:message key="productSku" /></td>
  	<td class="nameCol"><fmt:message key="description" /></td>
  	<td align="center"><fmt:message key="qty" /></td>
  	<td align="center"><fmt:message key="frequency" /></td>
  	<td class="nameCol"><fmt:message key="lastOrder" /></td>
  	<td class="nameCol"><fmt:message key="nextOrder" /></td>
  </tr>
<c:forEach items="${model.list.pageList}" var="item" varStatus="status">
  <tr class="row${status.index % 2}">
	<td align="center"><input type="checkbox" <c:if test="${item.enabled}">checked</c:if> disabled></td>
    <td class="nameCol"><c:out value="${item.code}"/></td>			
	<td class="nameCol"><fmt:formatDate type="date" timeStyle="default" value="${item.created}"/></td>
    <td class="nameCol"><c:out value="${item.product.sku}"/></td>			
    <td class="nameCol"><c:out value="${item.product.name}"/></td>			
	<td align="center"><c:out value="${item.quantity}"/></td>
	<td align="center">
	  <c:choose>
		<c:when test="${item.intervalUnit != 1}"><fmt:message key="every_${item.intervalType}s"><fmt:param value="${item.intervalUnit}"/></fmt:message></c:when>
		<c:otherwise><fmt:message key="every_${item.intervalType}"><fmt:param value="${item.intervalUnit}"/></fmt:message></c:otherwise>
	  </c:choose>
	</td>
	<td class="nameCol"><fmt:formatDate type="date" timeStyle="default" value="${item.lastOrder}"/></td>
	<td class="nameCol"><fmt:formatDate type="date" timeStyle="default" value="${item.nextOrder}"/></td>
  </tr>
</c:forEach>
<c:if test="${model.list.nrOfElements == 0}">
  <tr class="emptyList"><td colspan="9">&nbsp;</td></tr>
</c:if>
</table>
	</td>
  </tr>
</table>
</form>

<c:out value="${model.subscriptionLayout.footerHtml}" escapeXml="false"/>
</c:if>

  </tiles:putAttribute>
</tiles:insertDefinition>
