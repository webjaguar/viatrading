<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="/WEB-INF/tlds/spring.tld" prefix="spring" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>


<tiles:insertDefinition name="${_template}" flush="true">
  <tiles:putAttribute name="content" type="string">
  
 <script type="text/javascript">
function validatePhone() {
	 var country = document.getElementById('customer_address_country').value;	
	alert(country);
	  if(country  == 'US' || country  == 'CA' ){
	     $("#phone_num").mask("(999)999-9999");	
	  } else {
		 $("#phone_num").unmask("(999)999-9999");
	  }
}

function validateCellPhone() {
var country = document.getElementById('customer_address_country').value;	
if(country  == 'US' || country  == 'CA' ){
	$("#cell_phone_num").mask("(999)999-9999");	
 } else {
	$("#cell_phone_num").unmask("(999)999-9999");	
 }
}
</script>
<script type="text/javascript">
function automateCityState(zipCode) {
	var request = new Request.JSON({
		url: "${_contextpath}/jsonZipCode.jhtm?zipCode="+zipCode,
		onRequest: function() { 
		},
		onComplete: function(jsonObj) {
			document.getElementById('customer.address.city').value = jsonObj.city;
			document.getElementById('state').value = jsonObj.stateAbbv;
			$$('.highlight').each(function(el) {
				var end = el.getStyle('background-color');
				end = (end == 'transparent') ? '#fff' : end;
				var myFx = new Fx.Tween(el, {duration: 500, wait: false});
				myFx.start('background-color', '#f00', end);
			});
		}
	}).send();
}
</script>
	
<div class="col-breadcrumb">
	<ul class="breadcrumb">
		<li><a href="#">My Account</a></li>
		<li class="active">Edit Information</li>
	</ul>
</div>
<div class="col-sm-12">
	<div class="message"></div>
	<form:form id="editCustomerInformationForm" class="form-horizontal" commandName="customerForm" method="post">
		<div class="row">
			<div class="col-sm-12">
				<div class="requiredFieldLabel">* Required Field</div>
			</div>
		</div>
		<div id="editCustomerInformation">
			<div class="row">
				<div class="col-sm-6">
					<div class="form-group">
				 		<div class="col-sm-12">
							<h3><fmt:message key="yourInformation"/></h3>
						</div>
					</div>
					<div class="form-group">
						<div class="col-sm-4">
							<label for="customer_address_firstName" class="control-label">
								<fmt:message key="firstName" /> <sup class="requiredField">*</sup>
							</label>
						</div>
						<div class="col-sm-8">
							<form:input path="customer.address.firstName" id="customer_address_firstName" htmlEscape="true" class="form-control" />
	  						<form:errors path="customer.address.firstName" cssClass="error" />  
						</div>
					</div>
					<div class="form-group">
						<div class="col-sm-4">
							<label for="customer_address_lastName" class="control-label">
								<fmt:message key="lastName" /><sup class="requiredField">*</sup>
							</label>
						</div>
						<div class="col-sm-8">
							 <form:input path="customer.address.lastName" id="customer_address_lastName"  htmlEscape="true" cssErrorClass="errorField" class="form-control"/>
	  						<form:errors path="customer.address.lastName" cssClass="error" />    	
						</div>
					</div>
					<div class="form-group">
						<div class="col-sm-4">
							<label for="customer_address_country" class="control-label">
								<fmt:message key='country' /><sup class="requiredField">*</sup>
							</label>
						</div>
						<div class="col-sm-8">
							 <form:select class="form-control" id="customer_address_country" path="customer.address.country" onchange="toggleStateProvince(this)">
       						 <form:option value="" label="Please Select"/>
      							<form:options items="${model.countrylist}" itemValue="code" itemLabel="name"/>
      						</form:select>
      						<form:errors path="customer.address.country" cssClass="error" />     
						</div>
					</div>
					<div class="form-group">
						<div class="col-sm-4">
							<label for="customer_address_company" class="control-label">
								<fmt:message key="companyName" />
							</label>
						</div>
						<div class="col-sm-8">
							<form:input path="customer.address.company" htmlEscape="true" id="customer_address_company" class="form-control"/>
	  						<form:errors path="customer.address.company" cssClass="error" />   
						</div>
					</div>
					<div class="form-group">
						<div class="col-sm-4">
							<label for="customer_address_addr1" class="control-label">
								<fmt:message key="address" /> 1<sup class="requiredField">*</sup>
							</label>
						</div>
						<div class="col-sm-8">
							<form:input path="customer.address.addr1" htmlEscape="true" class="form-control" id="customer_address_addr1" />
	 					    <form:errors path="customer.address.addr1" cssClass="error" />  
						</div>
					</div>
					<div class="form-group">
						<div class="col-sm-4">
							<label for="customer_address_addr2" class="control-label">
								<fmt:message key="address" /> 2
							</label>
						</div>
						<div class="col-sm-8">
							<form:input path="customer.address.addr2" htmlEscape="true" class="form-control" id="customer_address_addr2"/>
	  						<form:errors path="customer.address.addr2" cssClass="error" /> 
						</div>
					</div>
					<div class="form-group">
						<div class="col-sm-4">
							<label for="customer_address_city" class="control-label">
								<fmt:message key="city" /><sup class="requiredField">*</sup>
							</label>
						</div>
						<div class="col-sm-8">
							<form:input path="customer.address.city" htmlEscape="true" class="form-control" id="customer_address_city" />
	  						<form:errors path="customer.address.city" cssClass="error" /> 
						</div>
					</div>
					<div class="form-group">
						<div class="col-sm-4">
							<label for="" class="control-label">
								<fmt:message key="stateProvince" /><sup class="requiredField">*</sup>
							</label>
						</div>
						<div class="col-sm-8">
							<form:select path="customer.address.stateProvince" class="form-control" id="state" disabled="${customerForm.customer.address.country != 'US'}">
           					<form:option value="" label="Please Select"/>
           					<form:options items="${model.statelist}" itemValue="code" itemLabel="name"/>
        	 				</form:select>
        					<form:select id="ca_province" path="customer.address.stateProvince" disabled="${customerForm.customer.address.country != 'CA'}" class="form-control" >
         					  <form:option value="" label="Please Select"/>
           					  <form:options items="${model.caProvinceList}" itemValue="code" itemLabel="name"/>
         					</form:select>
         					<form:input id="province" path="customer.address.stateProvince" class="form-control"  htmlEscape="true" disabled="${customerForm.customer.address.country == 'US' or customerForm.customer.address.country == 'CA' or customerForm.customer.address.country == '' or customerForm.customer.address.country == null}"/>
						    <div id="stateProvinceNA">&nbsp;<form:checkbox path="customer.address.stateProvinceNA" id="addressStateProvinceNA" value="true"  disabled="${customerForm.customer.address.country == 'US' or customerForm.customer.address.country == 'CA' or customerForm.customer.address.country == '' or customerForm.customer.address.country == null}"/><fmt:message key="notApplicable"/></div> 
						</div>
					</div>
					<div class="form-group">
						<div class="col-sm-4">
							<label for="customer_address_zip" class="control-label">
								<fmt:message key="zipCode" /><sup class="requiredField">*</sup>
							</label>
						</div>
						<div class="col-sm-8">
							<form:input id="automate" path="customer.address.zip" htmlEscape="true"  onkeyup="automateCityState(document.getElementById('automate').value)" class="form-control"/>  
	  						<form:input id="noautomate" path="customer.address.zip" htmlEscape="true"  class="form-control" />
	  						<form:errors path="customer.address.zip" cssClass="error" />    
						</div>
					</div>
					<c:if test="${siteConfig['ADDRESS_RESIDENTIAL_COMMERCIAL'].value > 1}">
					<div class="form-group">
						<div class="col-sm-4">
							<label class="control-label">
								<fmt:message key="deliveryType" />
							</label>
						</div>
						<div class="col-sm-8">
							<div class="radio">
								<label>
									<form:radiobutton path="customer.address.residential" value="true"/>: 
									<fmt:message key="residential" />
								</label>
							</div>
							<div class="radio">
								<label>
									<form:radiobutton path="customer.address.residential" value="false"/>: 
									<fmt:message key="commercial" />
								</label>
							</div>
						</div>
					</div>
					</c:if>
					<c:if test="${siteConfig['SHIPPING_LTL'].value == 'true'}">
					<div class="form-group">
						<div class="col-sm-4">
							<label class="control-label">
								<fmt:message key="liftGateDelivery" />
							</label>
						</div>
						<div class="col-sm-8">
							<div class="checkbox">
								<label>
									<form:checkbox path="customer.address.liftGate" value="true"/>
								</label>
								<a href="${_contextpath}/category.jhtm?cid=307" onclick="window.open(this.href,'','width=600,height=300,resizable=yes'); return false;"><i class="fa fa-question-circle"></i></a>
							</div>
						</div>
					</div>
					</c:if>
					
					
					<div class="form-group">
						<div class="col-sm-4">
							<label for="customer_address_phone" class="control-label">
								<fmt:message key="phone" /> <sup class="requiredField">*</sup>
							</label>
						</div>
						<div class="col-sm-8">
							<form:input path="customer.address.phone" id ="phone_num" onkeyup="validatePhone()" htmlEscape="true" class="form-control" />
	  						<form:errors path="customer.address.phone" cssClass="error" />  
						</div>
					</div>
					
					
					<div class="form-group">
						<div class="col-sm-4">
							<label for="customer_address_cellPhone" class="control-label">
								<fmt:message key="cellPhone" />
							</label>
						</div>
						<div class="col-sm-8">
							<form:input path="customer.address.cellPhone" id ="cell_phone_num" onkeyup="validateCellPhone()" htmlEscape="true" class="form-control" />
	  						<form:errors path="customer.address.cellPhone" cssClass="error" /> 
						</div>
					</div>
					
					
					<div class="form-group">
						<div class="col-sm-4" id="mobileCarrierIdLabel">
							<label for="mobileCarrierId" class="control-label">
								<fmt:message key='mobileCarrier' />
							</label>
						</div>
						<div class="col-sm-8" id="mobileCarrierId" >	
							<form:select path="customer.address.mobileCarrierId" class="form-control">
      						<form:option value="" label="Please Select"/>
      						<form:options items="${model.mobileCarrierList}" itemValue="id" itemLabel="carrier"/>
      						</form:select>
     					    <form:errors path="customer.address.mobileCarrierId" cssClass="error" />   		
						</div>
					</div>
					
					
					<div class="form-group">
						<div class="col-sm-4" id="textMessageNotify">
							<label class="control-label">
								<fmt:message key="textMessageNotification" />
							</label>
						</div>
						<div class="col-sm-8" id="textMessageNotifyMsg">
							<div class="checkbox">
								<label>
									<form:checkbox path="customer.textMessageNotify"/>
								</label>
							</div>
							<small class="help-block"><strong><fmt:message key="disclaimer" />:</strong> 
							<i><fmt:message key="disclaimerExp" /></i></small>
						</div>
					</div>
					
					<div class="form-group">
						<div class="col-sm-4">
							<label for="customer_address_fax" class="control-label">
								<fmt:message key="fax" />
							</label>
						</div>
						<div class="col-sm-8">
							<form:input path="customer.address.fax" htmlEscape="true" class="form-control"/>
	  						<form:errors path="customer.address.fax" cssClass="error" /> 
						</div>
					</div>
					
					<div class="form-group">
						<div class="col-sm-4">
							<label for="customer_languageCode" class="control-label">
								<fmt:message key="language" />
							</label>
						</div>
						<div class="col-sm-8">
							<form:select path="customer.languageCode" class="form-control">
        					<form:option value="en">
        						<fmt:message key="language_en"/>
        					</form:option>
        					<c:forEach items="${model.languageCodes}" var="i18n">         	
          						<option value="${i18n.languageCode}" <c:if test="${customerForm.customer.languageCode == i18n.languageCode}">selected</c:if>>
          							<fmt:message key="language_${i18n.languageCode}"/>
          						</option>
        					</c:forEach>
	  						</form:select>
						</div>
					</div>
					
					<div class="form-group">
						<div class="col-sm-12">
							<label class="control-label">
								<fmt:message key="note"/>
							</label>
						</div>
					</div>
					<div class="form-group">
						<div class="col-sm-4">
						</div>
						<div class="col-sm-8">
							<form:textarea path="customer.note" htmlEscape="true" class="form-control" rows="5" />
	 						<form:errors path="customer.note" cssClass="error" />  
	 						<small class="help-block">Please feel free to use the section above to provide us with any additional information or comments about your company and/or purchasing needs.</small>
						</div>
					</div>	
					
					<div class="form-group">
						<div class="col-sm-12">
							<div id="subscribeCheckBoxId" class="checkbox">
								<label>
								<form:checkbox path="subscribeEmail"  />Please send me email notifications with coupons, sales and special offers
								</label>
							</div>
						</div>
					</div>
					
				</div>
				<div class="col-sm-6"></div>
			</div>
		</div>

		
		<div id="taxId">
			<div class="row">
				<div class="col-sm-12">
					<div class="form-group">
						<div class="col-sm-12">
							<h3><fmt:message key="caTaxInformation"/></h3>
						</div>
					</div>
					<div class="form-group">
						<div class="col-sm-3">
							<label for="customer_taxId" class="control-label">
								<fmt:message key="f_taxId" /> :
							</label>
						</div>
						<div class="col-sm-3">
							<form:input path="customer.taxId" maxlength="30" htmlEscape="true" class="form-control"/>
	  						<form:errors path="customer.taxId" cssClass="error" />   
						</div>
					</div>
					<div class="form-group">
						<div class="col-sm-12">
							<span class="taxIdNote"><fmt:message key="f_taxNote" /></span>
						</div>
					</div>
				</div>
			</div>
		</div>
		
		
		
		<div id="extraInfo">
			<div class="row">
				<div class="col-sm-10 col-md-8 col-lg-6">
					
					<div class="form-group">
						<div class="col-sm-12">
							<h3>Extra Information</h3>
						</div>
					</div>
					
					<c:forEach items="${customerForm.customerFields}" var="customerField">
					<div class="form-group">
						<div class="col-sm-6">
							<label for="customer_field1" class="control-label">
								<c:out value="${customerField.name}"/>:
							</label>
						</div>
						<div class="col-sm-6">
							<c:choose>
						     <c:when test="${!empty customerField.preValue}">
						       <form:select path="customer.field${customerField.id}" class="form-control">
							        <form:option value="" label="Please Select"/>
						           	<c:forTokens items="${customerField.preValue}" delims="," var="dropDownValue">
						          		<form:option value="${dropDownValue}" />
						            </c:forTokens>
						       </form:select>
						     </c:when>
						     <c:otherwise>
						     	<form:input path="customer.field${customerField.id}" htmlEscape="true" maxlength="255" size="20" class="form-control"/> 
						     </c:otherwise>
						    </c:choose>
						    <form:errors path="customer.field${customerField.id}" cssClass="error" />
						</div>
					</div>
					</c:forEach>

						
					<div class="form-group">
						<div class="col-sm-6">
							<label for="customer_amazonName" class="control-label">
								<fmt:message key='amazonName' />:
							</label>
						</div>
						<div class="col-sm-6">
							<form:input path="customer.amazonName"  class="form-control"  htmlEscape="true"/>
             			 	<form:errors path="customer.amazonName" cssClass="error" />
						</div>
					</div>
					
					
					<div class="form-group">
						<div class="col-sm-6">
							<label for="customer_ebayName" class="control-label">
								<fmt:message key='ebayName' />:
							</label>
						</div>
						<div class="col-sm-6">
							<form:input path="customer.ebayName"  htmlEscape="true" class="form-control"/>
             			    <form:errors path="customer.ebayName" cssClass="error" />
						</div>
					</div>
					
					
					<div class="form-group">
						<div class="col-sm-6">
							<label for="customer_websiteUrl" class="control-label">
								<fmt:message key='websiteUrl' />:
							</label>
						</div>
						<div class="col-sm-6">
						   <form:input path="customer.websiteUrl"  htmlEscape="true" class="form-control"/>
              			   <form:errors path="customer.websiteUrl" cssClass="error" />
						</div>
					</div>
						
				</div>
			</div>
		</div>
		<div id="form_buttons">
			<div class="row">
				<div class="col-sm-12">
					<div id="buttons_wrapper">
						<button type="submit" class="btn btn-default"><fmt:message key="saveChanges" /></button>
						<button type="submit" class="btn btn-default"><fmt:message key="cancel" /></button>
					</div>
				</div>
			</div>
		</div>
	</form:form>
</div>


<script language="JavaScript">
<!--
function toggleStateProvince(el) {
  if (el.value == "US") {
      document.getElementById('state').disabled=false;
      document.getElementById('state').style.display="block";
      document.getElementById('ca_province').disabled=true;
      document.getElementById('ca_province').style.display="none";
      document.getElementById('province').disabled=true;
      document.getElementById('province').style.display="none";
      document.getElementById('addressStateProvinceNA').disabled=true;
      document.getElementById('stateProvinceNA').style.display="none";
      document.getElementById('mobileCarrierId').disabled=false;
      document.getElementById('mobileCarrierId').style.display="table-row";
      document.getElementById('mobileCarrierIdLabel').disabled=false;
      document.getElementById('mobileCarrierIdLabel').style.display="table-row";
      document.getElementById('textMessageNotify').disabled=false;
      document.getElementById('textMessageNotify').style.display="table-row";
      document.getElementById('textMessageNotifyMsg').disabled=false;
      document.getElementById('textMessageNotifyMsg').style.display="table-row";
      document.getElementById('automate').disabled=false;
      document.getElementById('automate').style.display="block";
      document.getElementById('noautomate').disabled=true;
      document.getElementById('noautomate').style.display="none";
  } else if (el.value == "CA") {
      document.getElementById('state').disabled=true;
      document.getElementById('state').style.display="none";
      document.getElementById('ca_province').disabled=false;
      document.getElementById('ca_province').style.display="block";
      document.getElementById('province').disabled=true;
      document.getElementById('province').style.display="none";
      document.getElementById('addressStateProvinceNA').disabled=true;
      document.getElementById('stateProvinceNA').style.display="none";
      document.getElementById('mobileCarrierId').disabled=true;
      document.getElementById('mobileCarrierId').style.display="none";
      document.getElementById('mobileCarrierIdLabel').disabled=true;
      document.getElementById('mobileCarrierIdLabel').style.display="none";
      document.getElementById('textMessageNotify').disabled=true;
      document.getElementById('textMessageNotify').style.display="none";
      document.getElementById('textMessageNotifyMsg').disabled=true;
      document.getElementById('textMessageNotifyMsg').style.display="none";
      document.getElementById('automate').disabled=true;
      document.getElementById('automate').style.display="none";
      document.getElementById('noautomate').disabled=false;
      document.getElementById('noautomate').style.display="block";
  } else {
      document.getElementById('state').disabled=true;
      document.getElementById('state').style.display="none";
      document.getElementById('ca_province').disabled=true;
      document.getElementById('ca_province').style.display="none";
      document.getElementById('province').disabled=false;
      document.getElementById('province').style.display="block";
      document.getElementById('addressStateProvinceNA').disabled=false;
      document.getElementById('stateProvinceNA').style.display="block";
      document.getElementById('mobileCarrierId').disabled=true;
      document.getElementById('mobileCarrierId').style.display="none";
      document.getElementById('mobileCarrierIdLabel').disabled=true;
      document.getElementById('mobileCarrierIdLabel').style.display="none";
      document.getElementById('textMessageNotify').disabled=true;
      document.getElementById('textMessageNotify').style.display="none";
      document.getElementById('textMessageNotifyMsg').disabled=true;
      document.getElementById('textMessageNotifyMsg').style.display="none";
      document.getElementById('automate').disabled=true;
      document.getElementById('automate').style.display="none";
      document.getElementById('noautomate').disabled=false;
      document.getElementById('noautomate').style.display="block";
  }
}
toggleStateProvince(document.getElementById('country'));
//-->
</script>

</tiles:putAttribute>
</tiles:insertDefinition>