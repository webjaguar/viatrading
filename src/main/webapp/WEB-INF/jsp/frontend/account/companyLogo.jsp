<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>

<tiles:insertDefinition name="${_template}" flush="true">
  <tiles:putAttribute name="content" type="string">

<form:form method="post" commandName="companyLogoForm" enctype="multipart/form-data" >

<table border="0" cellpadding="3" cellspacing="0" width="100%" class="companyLogo">
<c:if test="${companyLogo != null}" >
  <tr>
    <td colspan="2">
      <img style="border:1px solid #cccccc" src="assets/Image/CompanyLogo/<c:out value="${companyLogo}" />?rnd=${randomNum}" title="" alt="" />
    </td>
  </tr>
</c:if> 
<c:if test="${companyLogo == null}" >
<tr>
    <td colspan="2">
      <dd>
       <dl>Image type: Use jpg or gif image</dl>
       <dl>Image size: 200px by 200px</dl>
      </dd>
    </td>
  </tr>
</c:if>
  <tr>
    <td><input type="file" name="company_logo" /><form:errors path="logoName" cssClass="error" /><br />
    <input type="submit" name="upload" value="upload"/><input type="submit" name="_cancel" value="cancel"/><input type="submit" name="_delete" value="delete"/></td>
  </tr>
</table>

</form:form>

  </tiles:putAttribute>
</tiles:insertDefinition>
