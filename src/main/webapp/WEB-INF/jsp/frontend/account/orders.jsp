<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="/WEB-INF/tlds/spring.tld" prefix="spring" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>

<tiles:insertDefinition name="${_template}" flush="true">
  <tiles:putAttribute name="content" type="string">
  
<c:if test="${fn:trim(model.myOrderHistoryLayout.headerHtml) != ''}">
  <c:set value="${model.myOrderHistoryLayout.headerHtml}" var="headerHtml"/>
  <c:set value="${fn:replace(headerHtml, '#firstname#', userSession.firstName)}" var="headerHtml"/>
  <c:set value="${fn:replace(headerHtml, '#lastname#', userSession.lastName)}" var="headerHtml"/>
  <c:set value="${fn:replace(headerHtml, '#email#', userSession.username)}" var="headerHtml"/>
  <c:set value="${fn:replace(headerHtml, '#lastOrderId#', model.lastOrderId)}" var="headerHtml"/>
  <c:set value="${fn:replace(headerHtml, '#lastOrderStatus#', model.lastStatus)}" var="headerHtml"/>
  <c:set value="${fn:replace(headerHtml, '#lastOrderSubStatus#', model.lastSubStatus)}" var="headerHtml"/>
  <c:out value="${headerHtml}" escapeXml="false"/>
</c:if>
<form method="post" id="list">
<input type="hidden" id="sort" name="sort" value="${orderSearch.sort}" /> 
<table width="90%" align="center" cellspacing="5" cellpadding="0" border="0">
<c:if test="${model.subAccount != null}">
  <tr>
  	<td colspan="2">
  	  <table cellspacing="0" cellpadding="0" border="0" class="listingsHdr1">
  	    <tr>
  	      <td><fmt:message key="subAccount" />:</td>
  	      <td>&nbsp;&nbsp;</td>  	      
  	      <td><c:out value="${model.subAccount.username}"/></td>
  	    </tr>
  	    <c:if test="${model.subAccount.address.company != null and model.subAccount.address.company != ''}">
  	    <tr>
  	      <td><fmt:message key="company" />:</td>
  	      <td>&nbsp;&nbsp;</td>  	     
  	      <td><c:out value="${model.subAccount.address.company}"/></td>
  	    </tr>
  	    </c:if>
  	    <tr>
  	      <td colspan="2">&nbsp;</td>  	      
  	      <td><c:out value="${model.subAccount.address.firstName}"/> <c:out value="${model.subAccount.address.lastName}"/></td>
  	    </tr>
  	  </table>  	      
  	</td>
  </tr>
  <tr><td colspan="2">&nbsp;</td></tr>
</c:if>
  <tr>
  	<td class="listingsHdr1"><fmt:message key="orderHistoryStatus" /></td>
	<td>
<div class="pageNavi">Page 
		<select name="page" onchange="submit()">
		<c:forEach begin="1" end="${model.orders.pageCount}" var="page">
  	        <option value="${page}" <c:if test="${page == (model.orders.page+1)}">selected</c:if>>${page}</option>
		</c:forEach>
		</select>
of <c:out value="${model.orders.pageCount}"/></div>
	</td>
  </tr>
  <tr>
	<td colspan="2">
<table border="0" cellpadding="3" cellspacing="1" width="100%" class="listings"> 
  <tr class="listingsHdr2">
    <c:set var="cols" value="0"/>
    <c:if test="${fn:contains(siteConfig['CUSTOMIZE_VIEW_MY_ORDER_HISTORY_LIST'].value,'orderDate')}">
  	  <td class="nameCol">&nbsp;</td>
  	</c:if>
  	<c:if test="${fn:contains(siteConfig['CUSTOMIZE_VIEW_MY_ORDER_HISTORY_LIST'].value,'invoice')}">
  	  <td><input name="orderNum" type="text" value="<c:out value='${model.orderSearch.orderNum}' />" size="10" /></td>
  	</c:if>
  	<c:if test="${fn:contains(siteConfig['CUSTOMIZE_ORDER_LIST'].value,'mas200')}">
  	  <td>&nbsp;</td>
  	</c:if>
  	<c:if test="${fn:contains(siteConfig['CUSTOMIZE_VIEW_MY_ORDER_HISTORY_LIST'].value,'po')}">
  	  <td><input name="purchaseOrder" type="text" value="<c:out value='${model.orderSearch.purchaseOrder}' />" size="10" /></td>
  	</c:if>
  	<c:if test="${fn:contains(siteConfig['CUSTOMIZE_VIEW_MY_ORDER_HISTORY_LIST'].value,'status') and gSiteConfig['gACCOUNTING'] != 'EVERGREEN'}">  
	    <td class="nameCol">
	      <c:if test="${not fn:contains(siteConfig['CUSTOMIZE_ORDER_LIST'].value,'mas200')}">
	      <select name="status">
		  	<option value=""><fmt:message key="allOrders" /></option>
		  	<option value="p" <c:if test="${model.orderSearch.status == 'p'}">selected</c:if>><fmt:message key="orderStatus_p" /></option>
	  	    <option value="pr" <c:if test="${model.orderSearch.status == 'pr'}">selected</c:if>><fmt:message key="orderStatus_pr" /></option>
	  	    <option value="s" <c:if test="${model.orderSearch.status == 's'}">selected</c:if>><fmt:message key="orderStatus_s" /></option>
	  	    <option value="x" <c:if test="${model.orderSearch.status == 'x'}">selected</c:if>><fmt:message key="orderStatus_x" /></option>
	      </select>
	      </c:if>
	    </td>
    </c:if>
      <c:if test="${fn:contains(siteConfig['CUSTOMIZE_VIEW_MY_ORDER_HISTORY_LIST'].value,'substatus') and siteConfig['SUB_STATUS_ON_FRONTEND'].value == 'true'}">
        <td>&nbsp;</td>
    </c:if>
    <c:if test="${gSiteConfig['gREORDER']}">
  	    <c:set var="cols" value="${cols+1}"/>
  	</c:if>
  	<c:if test="${gSiteConfig['gPAYMENTS']}">
        <c:set var="cols" value="${cols+1}"/>
    </c:if>
  	<td class="nameCol" colspan="${1+cols}" align="right"><input type="submit" value="Search" /></td>
  </tr>
  <tr class="listingsHdr2">
  <c:if test="${fn:contains(siteConfig['CUSTOMIZE_VIEW_MY_ORDER_HISTORY_LIST'].value,'orderDate')}">
	  	<td>
		   	<table cellspacing="0" cellpadding="1">
		   	  <tr>
		   	    <c:choose>
				   	<c:when test ="${model.orderSearch.sort == 'date_ordered desc' }">
					  	<td class="listingsHdr3">
					  	<a class="sortLink" href="#" onClick="document.getElementById('sort').value='0';document.getElementById('list').submit()"> <fmt:message key="orderDate" /></a>
					  	</td>
					  	<td><img src="/assets/Image/Layout/up.gif" border="0"></td>
				  	</c:when>
				  	<c:when test ="${model.orderSearch.sort == 'date_ordered' }">
					  	<td class="listingsHdr3">
					  	<a class="sortLink" href="#" onClick="document.getElementById('sort').value='1';document.getElementById('list').submit()"> <fmt:message key="orderDate" /></a>				  	
					  	</td>
					  	<td><img src="/assets/Image/Layout/down.gif" border="0"></td>
				  	</c:when>
				  	<c:otherwise>
					  	<td class="listingsHdr3">
					  	<a class="sortLink" href="#" onClick="document.getElementById('sort').value='1';document.getElementById('list').submit()"> <fmt:message key="orderDate" /></a>
					  	</td>	
					  	<td><img src="/assets/Image/Layout/down.gif" border="0"></td>			  
				  	</c:otherwise>
			  	</c:choose>
		   	  </tr>
		   	 </table>
	    </td> 
    </c:if>
  <c:if test="${fn:contains(siteConfig['CUSTOMIZE_VIEW_MY_ORDER_HISTORY_LIST'].value,'invoice')}">
  	<td class="nameCol">
  	  <c:choose>
  	    <c:when test="${fn:contains(siteConfig['CUSTOMIZE_ORDER_LIST'].value,'mas200')}">
  	      Web Order
  	    </c:when>
  	    <c:otherwise>
  	  	  <fmt:message key="invoice" />
  	    </c:otherwise>
  	  </c:choose>
  	</td>
  </c:if>
  <c:if test="${fn:contains(siteConfig['CUSTOMIZE_ORDER_LIST'].value,'mas200')}">
  	<td class="nameCol">Sales Order #</td>
  </c:if>
  <c:if test="${fn:contains(siteConfig['CUSTOMIZE_VIEW_MY_ORDER_HISTORY_LIST'].value,'po')}">  	
	  	<td class="nameCol">P.O. #</td>
  </c:if>
  <c:if test="${fn:contains(siteConfig['CUSTOMIZE_VIEW_MY_ORDER_HISTORY_LIST'].value,'status')}">  	  	
	  	<c:if test="${gSiteConfig['gACCOUNTING'] != 'EVERGREEN'}">
	  	  <td align="center"><fmt:message key="status" /></td>
	  	</c:if>
  </c:if>
  <c:if test="${fn:contains(siteConfig['CUSTOMIZE_VIEW_MY_ORDER_HISTORY_LIST'].value,'substatus') and siteConfig['SUB_STATUS_ON_FRONTEND'].value == 'true'}">
  	  <td align="center"><fmt:message key="subStatus" /></td>
  </c:if>
  <c:if test="${fn:contains(siteConfig['CUSTOMIZE_VIEW_MY_ORDER_HISTORY_LIST'].value,'total')}">
	  	<td class="nameCol"><fmt:message key="total" /></td>
  </c:if>
  <c:if test="${fn:contains(siteConfig['CUSTOMIZE_VIEW_MY_ORDER_HISTORY_LIST'].value,'reorder') and gSiteConfig['gREORDER']}">
	  		<td class="nameCol"><fmt:message key="f_reorder" /></td>
  </c:if>
  <c:if test="${fn:contains(siteConfig['CUSTOMIZE_VIEW_MY_ORDER_HISTORY_LIST'].value,'payment') and gSiteConfig['gPAYMENTS'] and gSiteConfig['gACCOUNTING'] != 'EVERGREEN'}">
	    <td class="nameCol"><fmt:message key="amountPaid" /></td>
  </c:if>
  <c:if test="${fn:contains(siteConfig['CUSTOMIZE_VIEW_MY_ORDER_HISTORY_LIST'].value,'shipDate')}"> 	
  	<td class="nameCol">Ship Date</td>
  </c:if>
  <c:if test="${fn:contains(siteConfig['CUSTOMIZE_VIEW_MY_ORDER_HISTORY_LIST'].value,'trackcode')}"> 	
  	<td class="nameCol">Tracking No.</td>
  </c:if>
  <c:if test="${fn:contains(siteConfig['CUSTOMIZE_VIEW_MY_ORDER_HISTORY_LIST'].value,'carrier')}"> 	
  	<td class="nameCol">Carrier</td>
  </c:if>
  </tr>
<c:forEach items="${model.orders.pageList}" var="order" varStatus="status">
  <tr class="row${status.index % 2}">
  <c:if test="${fn:contains(siteConfig['CUSTOMIZE_VIEW_MY_ORDER_HISTORY_LIST'].value,'orderDate')}">
    <td class="nameCol"><fmt:formatDate type="date" timeStyle="default" value="${order.dateOrdered}"/></td>	
  </c:if>	
  <c:if test="${fn:contains(siteConfig['CUSTOMIZE_VIEW_MY_ORDER_HISTORY_LIST'].value,'invoice')}">	
  <c:choose>
  <c:when test="${model.getQuoteList == true}">
		<td class="nameCol"><a href="invoice.jhtm?quote=${order.orderId}" class="nameLink"><c:out value="${order.orderId}"/></a>
			<c:if test="${order.pdfUrl != null}">&nbsp;&nbsp;<a href="assets/pdf/<c:out value="${order.pdfUrl}"/>" target="_blank"><img src="assets/Image/Layout/pdf.gif" border="0"/></a></c:if>
		</td>
  </c:when>
  <c:otherwise>
		<td class="nameCol"><a href="invoice.jhtm?order=${order.orderId}" class="nameLink"><c:out value="${order.orderId}"/></a>
			<c:if test="${order.pdfUrl != null}">&nbsp;&nbsp;<a href="assets/pdf/<c:out value="${order.pdfUrl}"/>" target="_blank"><img src="assets/Image/Layout/pdf.gif" border="0"/></a></c:if>
			<c:if test="${order.servicePdfUrl != null}">&nbsp;&nbsp;<a href="assets/pdf/<c:out value="${order.servicePdfUrl}"/>" target="_blank"><img src="assets/Image/Layout/pdf.gif" border="0"/></a></c:if>
		</td>
  </c:otherwise>
  </c:choose>
  </c:if>
  <c:if test="${fn:contains(siteConfig['CUSTOMIZE_ORDER_LIST'].value,'mas200')}">
	<td class="nameCol"><a href="jbd_order.jhtm?id=${order.mas200orderNo}" class="nameLink"><c:out value="${order.mas200orderNo}"/></a></td>
  </c:if>
  <c:if test="${fn:contains(siteConfig['CUSTOMIZE_VIEW_MY_ORDER_HISTORY_LIST'].value,'po')}">
	   <td class="nameCol"><c:out value="${order.purchaseOrder}"/></td>
  </c:if>
  <c:if test="${fn:contains(siteConfig['CUSTOMIZE_VIEW_MY_ORDER_HISTORY_LIST'].value,'status') and gSiteConfig['gACCOUNTING'] != 'EVERGREEN'}">
	  <td align="center">
  	    <c:choose>
  	    <c:when test="${fn:contains(siteConfig['CUSTOMIZE_ORDER_LIST'].value,'mas200')}">
			<c:choose>
			<c:when test="${order.mas200status == 'A'}">
		        Active
			</c:when>
			<c:when test="${order.mas200status == 'C'}">
		        Completed
			</c:when>
			<c:when test="${order.mas200status == 'X'}">
		        Cancelled
			</c:when>
		    <c:otherwise>
		        <c:out value="${order.mas200status}"/>
		    </c:otherwise>
		    </c:choose>   	    
  	    </c:when>
  	    <c:otherwise>
  	    	<fmt:message key="orderStatus_${order.status}"/>
  	    </c:otherwise>
  	    </c:choose>
	  </td>
  </c:if>  
  <c:if test="${fn:contains(siteConfig['CUSTOMIZE_VIEW_MY_ORDER_HISTORY_LIST'].value,'substatus') and siteConfig['SUB_STATUS_ON_FRONTEND'].value == 'true'}">
  	  <td align="center"><c:choose><c:when test="${order.subStatus != null}"><fmt:message key="orderSubStatus_${order.subStatus}" /></c:when><c:otherwise>&nbsp;</c:otherwise></c:choose></td>
  </c:if>
  <c:if test="${fn:contains(siteConfig['CUSTOMIZE_VIEW_MY_ORDER_HISTORY_LIST'].value,'total')}">
	  	<td class="nameCol" align="right"><fmt:message key="${siteConfig['CURRENCY'].value}" /><fmt:formatNumber value="${order.grandTotal}" pattern="#,##0.00" /></td>
  </c:if>
  <c:if test="${fn:contains(siteConfig['CUSTOMIZE_VIEW_MY_ORDER_HISTORY_LIST'].value,'reorder') and gSiteConfig['gREORDER']}">
		<td class="nameCol"><a href="reorder.jhtm?order=${order.orderId}" class="nameLink"><fmt:message key="f_reorderItems" /></a>
  </c:if>
  <c:if test="${fn:contains(siteConfig['CUSTOMIZE_VIEW_MY_ORDER_HISTORY_LIST'].value,'payment') and gSiteConfig['gPAYMENTS'] and gSiteConfig['gACCOUNTING'] != 'EVERGREEN'}">
		<c:choose><c:when test="${order.amountPaid != 0 and order.amountPaid != null}">
		  <td class="nameCol" align="right"><fmt:message key="${siteConfig['CURRENCY'].value}" /><fmt:formatNumber value="${order.amountPaid}" pattern="#,##0.00" /></td></c:when>
		  <c:otherwise><td class="nameCol" align="right">&nbsp;</td></c:otherwise>
		</c:choose>
  </c:if>
  <c:if test="${fn:contains(siteConfig['CUSTOMIZE_VIEW_MY_ORDER_HISTORY_LIST'].value,'shipDate')}">
    	<td class="nameCol"><c:out value="${order.shipped}"/></td>
  </c:if>
  <c:if test="${fn:contains(siteConfig['CUSTOMIZE_VIEW_MY_ORDER_HISTORY_LIST'].value,'trackcode')}">
    	<td class="nameCol"><c:out value="${order.trackcode}"/></td>
  </c:if>
  <c:if test="${fn:contains(siteConfig['CUSTOMIZE_VIEW_MY_ORDER_HISTORY_LIST'].value,'carrier')}">
    	<td class="nameCol"><c:out value="${order.shippingCarrier}"/></td>
  </c:if>
  </tr>
</c:forEach>
<c:if test="${model.orders.nrOfElements == 0}">
  <tr class="emptyList"><td colspan="${5+cols}">&nbsp;</td></tr>
</c:if>
</table>
<table border="0" cellpadding="0" cellspacing="1" width="100%">
  <tr>
  <td align="right">
  <select name="size" onchange="submit()">
    <c:forTokens items="10,25,50" delims="," var="current">
  	  <option value="${current}" <c:if test="${current == model.orders.pageSize}">selected</c:if>>${current} <fmt:message key="perPage" /></option>
    </c:forTokens>
  </select>
  </td>
  </tr>
</table>
	</td>
  </tr>
</table>
</form>

<c:if test="${fn:trim(model.myOrderHistoryLayout.footerHtml) != ''}">
  <c:set value="${model.myOrderHistoryLayout.footerHtml}" var="footerHtml"/>
  <c:set value="${fn:replace(footerHtml, '#firstname#', userSession.firstName)}" var="footerHtml"/>
  <c:set value="${fn:replace(footerHtml, '#lastname#', userSession.lastName)}" var="footerHtml"/>
  <c:set value="${fn:replace(footerHtml, '#email#', userSession.username)}" var="footerHtml"/>
  <c:set value="${fn:replace(footerHtml, '#lastOrderId#', model.lastOrderId)}" var="footerHtml"/>
  <c:set value="${fn:replace(footerHtml, '#lastOrderStatus#', model.lastStatus)}" var="footerHtml"/>
  <c:set value="${fn:replace(footerHtml, '#lastOrderSubStatus#', model.lastSubStatus)}" var="footerHtml"/>
  <c:out value="${footerHtml}" escapeXml="false"/>
</c:if>

  </tiles:putAttribute>
</tiles:insertDefinition>
