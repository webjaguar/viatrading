<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ taglib uri="/WEB-INF/tlds/custom.tld" prefix="wj" %>

<tiles:insertDefinition name="${_template}" flush="true">
  <tiles:putAttribute name="content" type="string">
  <form  action="partnersReport.jhtm" method="post">
  <table width="90%" align="center" class="myList" cellspacing="5" cellpadding="0" border="0">
  <tr class="listingsHdr2">
	<td>&nbsp;</td>
	<td>
<div class="pageNavi">Page 
		<select name="page" onchange="submit()">
		<c:forEach begin="1" end="${model.partnersList.pageCount}" var="page">
  	        <option value="${page}" <c:if test="${page == (model.partnersList.page+1)}">selected</c:if>>${page}</option>
		</c:forEach>
		</select>
of <c:out value="${model.partnersList.pageCount}"/></div>
	</td>
  </tr>
  <tr>
	<td colspan="2">
<table border="0" cellpadding="3" cellspacing="1" width="100%" class="listings"> 
  <tr class="listingsHdr2">
      <td class="nameCol"><fmt:message key="partner"/> <fmt:message key="name"/></td>
      <td class="nameCol"><fmt:message key="amount"/></td>
      <td class="nameCol"><fmt:message key="date"/></td>
  </tr>
<c:forEach items="${model.partnersList.pageList}" var="partner" varStatus="status">
  <tr class="row${status.index % 2}">
	<td class="nameCol"><c:out value="${partner.partnerName}"/></td>	
    <td class="nameCol"><fmt:message key="${siteConfig['CURRENCY'].value}" /> <fmt:formatNumber value="${partner.amount}" pattern="#,##0"></fmt:formatNumber></td>	
  	<td class="nameCol"><fmt:formatDate type="date" pattern="MM/dd/yyyy" value='${partner.date}'/> </td>
  </tr>
</c:forEach>
</table>
	</td>
  </tr>
</table>  
  </form>
  
  </tiles:putAttribute>
</tiles:insertDefinition>