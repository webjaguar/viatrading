<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="/WEB-INF/tlds/spring.tld" prefix="spring" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>

<tiles:insertDefinition name="${_template}" flush="true">
  <tiles:putAttribute name="content" type="string">

<form method="post">
<table width="90%" align="center" cellspacing="5" cellpadding="0" border="0">
  <tr>
  	<td class="listingsHdr1"><fmt:message key="orderHistoryStatus" /></td>
	<td>
<div class="pageNavi">Page 
		<select name="page" onchange="submit()">
		<c:forEach begin="1" end="${model.orders.pageCount}" var="page">
  	        <option value="${page}" <c:if test="${page == (model.orders.page+1)}">selected</c:if>>${page}</option>
		</c:forEach>
		</select>
of <c:out value="${model.orders.pageCount}"/></div>
	</td>
  </tr>
  <tr>
	<td colspan="2">
<table border="0" cellpadding="3" cellspacing="1" width="100%" class="listings"> 
  <tr class="listingsHdr2">
    <c:set var="cols" value="0"/>
  	<td class="nameCol">&nbsp;</td>
  	<td><input name="orderNum" type="text" value="<c:out value='${model.orderSearch.orderNum}' />" size="10" /></td>
  	<td class="nameCol" colspan="${1+cols}" align="right"><input type="submit" value="Search" /></td>
  </tr>
  <tr class="listingsHdr2">
  	<td class="nameCol"><fmt:message key="orderDate" /></td>
  	<td class="nameCol" colspan="2"><fmt:message key="invoice" /></td>
  </tr>
<c:forEach items="${model.orders.pageList}" var="order" varStatus="status">
  <tr class="row${status.index % 2}">
    <td class="nameCol"><fmt:formatDate type="date" timeStyle="default" value="${order.dateOrdered}"/></td>			
	<td class="nameCol" colspan="2"><a href="supplierInvoice.jhtm?orderId=${order.orderId}" class="nameLink"><c:out value="${order.orderId}"/></a>
		<c:if test="${order.pdfUrl != null}">&nbsp;&nbsp;<a href="assets/pdf/<c:out value="${order.pdfUrl}"/>" target="_blank"><img src="assets/Image/Layout/pdf.gif" border="0"/></a></c:if></td>
  </tr>
</c:forEach>
<c:if test="${model.orders.nrOfElements == 0}">
  <tr class="emptyList"><td colspan="${4+cols}">&nbsp;</td></tr>
</c:if>
</table>
<table border="0" cellpadding="0" cellspacing="1" width="100%">
  <tr>
  <td align="right">
  <select name="size" onchange="submit()">
    <c:forTokens items="10,25,50" delims="," var="current">
  	  <option value="${current}" <c:if test="${current == model.orders.pageSize}">selected</c:if>>${current} <fmt:message key="perPage" /></option>
    </c:forTokens>
  </select>
  </td>
  </tr>
</table>
	</td>
  </tr>
</table>
</form>

  </tiles:putAttribute>
</tiles:insertDefinition>
