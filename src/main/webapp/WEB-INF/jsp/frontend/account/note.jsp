<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>

<tiles:insertDefinition name="${_template}" flush="true">
  <tiles:putAttribute name="content" type="string">

<c:choose>
	<c:when test="${param.note == 1}">
		<div class="customerCustomNoteHdr"><c:out value="${siteConfig['CUSTOMER_CUSTOM_NOTE_1'].value}"/></div>
		<hr><br>
		<c:out value="${model.customer['customNote1']}" escapeXml="false" />
	</c:when>
	<c:when test="${param.note == 2}">
		<div class="customerCustomNoteHdr"><c:out value="${siteConfig['CUSTOMER_CUSTOM_NOTE_2'].value}"/></div>
		<hr><br>
		<c:out value="${model.customer['customNote2']}" escapeXml="false" />
	</c:when>
	<c:when test="${param.note == 3}">
		<div class="customerCustomNoteHdr"><c:out value="${siteConfig['CUSTOMER_CUSTOM_NOTE_3'].value}"/></div>
		<hr><br>
		<c:out value="${model.customer['customNote3']}" escapeXml="false" />
	</c:when>
</c:choose>


  </tiles:putAttribute>
</tiles:insertDefinition>
