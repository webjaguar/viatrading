<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>

<%--Spring3 DONE --%>
<tiles:insertDefinition name="${_template}" flush="true">
  <tiles:putAttribute name="content" type="string">
  
<br><br><br>
<form method="post">
<table width="90%" align="center" cellspacing="5" cellpadding="0" border="0">
  <tr>
  	<td class="listingsHdr1"><fmt:message key="serviceRequest" /> <fmt:message key="historyStatus" /></td>
	<td>
<div class="pageNavi">Page 
		<select name="page" onchange="submit()">
		<c:forEach begin="1" end="${model.services.pageCount}" var="page">
  	        <option value="${page}" <c:if test="${page == (model.services.page+1)}">selected</c:if>>${page}</option>
		</c:forEach>
		</select>
of <c:out value="${model.services.pageCount}"/></div>
	</td>
  </tr>
  <tr>
	<td colspan="2">
<table border="0" cellpadding="3" cellspacing="1" width="100%" class="listings"> 
  <tr class="listingsHdr2">
  	<td><input name="serviceNum" type="text" value="<c:out value='${model.servicesSearch.serviceNum}' />" size="10" /></td>
    <td align="center"><select name="itemId" style="width:80px">
        <option value=""><fmt:message key="all" /></option> 
	  	<c:forEach items="${model.serviceItems}" var="item">
          <option value="${item.itemId}" <c:if test="${model.servicesSearch.itemId == item.itemId}">selected</c:if>><c:out value="${item.itemId}"/></option>
		</c:forEach>
      </select>
    </td>  	
    <td align="center"><select name="status" style="width:80px">
        <option value=""><fmt:message key="all" /></option> 

		<c:forEach var="status" items="${model.statuses}">
          <option value="${status}" <c:if test="${model.servicesSearch.status == status}">selected</c:if>><c:out value="${status}"/></option>
        </c:forEach>  
      </select>
    </td>
  	<td align="center"><input name="purchaseOrder" type="text" value="<c:out value='${model.servicesSearch.purchaseOrder}' />" size="10" /></td>
  	<td align="center"><input name="wo3rd" type="text" value="<c:out value='${model.servicesSearch.wo3rd}' />" size="10" /></td>
  	<td colspan="3" align="center"><input type="submit" value="Search" /></td>
	<c:if test="${model.hasTrackNumIn}">
  	<td align="center"><input name="trackNumIn" type="text" value="<c:out value='${model.servicesSearch.trackNumIn}' />" size="10" /></td>
	</c:if>
	<c:if test="${model.hasTrackNumOut}">
  	<td align="center"><input name="trackNumOut" type="text" value="<c:out value='${model.servicesSearch.trackNumOut}' />" size="10" /></td>
	</c:if>
	<c:if test="${model.hasItemFrom}">
  	<td align="center"><input name="itemFrom" type="text" value="<c:out value='${model.servicesSearch.itemFrom}' />" size="10" /></td>
	</c:if>
	<c:if test="${model.hasItemTo}">
  	<td align="center"><input name="itemTo" type="text" value="<c:out value='${model.servicesSearch.itemTo}' />" size="10" /></td>
	</c:if>
	<c:if test="${model.hasShipDate}">
  	<td>&nbsp;</td>
	</c:if>
  </tr>
  <tr class="listingsHdr2">
    <c:set var="cols" value="0"/>
  	<td class="nameCol"><fmt:message key="serviceRequest" /> #</td>
   	<td align="center"><c:out value="${siteConfig['SERVICEABLE_ITEM_TITLE'].value}"/> ID</td>
  	<td align="center"><fmt:message key="status" /></td>
  	<td align="center">P.O.</td>
  	<td align="center"><fmt:message key="3rdParty" /> W.O.</td>  	
  	<td align="center"><fmt:message key="order" /></td>
  	<td align="center"><fmt:message key="reportDate" /></td>
  	<td align="center"><fmt:message key="completeDate" /></td>
	<c:if test="${model.hasTrackNumIn}">
	<c:set var="cols" value="${cols+1}"/>
  	<td align="center">Inbound <fmt:message key="trackNum" /></td>
	</c:if>
	<c:if test="${model.hasTrackNumOut}">
	<c:set var="cols" value="${cols+1}"/>
  	<td align="center">Outbound <fmt:message key="trackNum" /></td>
	</c:if>
	<c:if test="${model.hasItemFrom}">
	<c:set var="cols" value="${cols+1}"/>
  	<td align="center">Printer From City, State, Zip</td>
	</c:if>
	<c:if test="${model.hasItemTo}">
	<c:set var="cols" value="${cols+1}"/>
  	<td align="center">Printer To City, State, Zip</td>
	</c:if>
	<c:if test="${model.hasShipDate}">
	<c:set var="cols" value="${cols+1}"/>
  	<td align="center"><fmt:message key="shipDate" /></td>
	</c:if>
  </tr>
<c:forEach items="${model.services.pageList}" var="service" varStatus="status">
  <tr class="row${status.index % 2}">
	<td class="nameCol"><a href="service.jhtm?num=${service.serviceNum}" class="nameLink"><c:out value="${service.serviceNum}"/></a>
		<c:if test="${service.servicePdfUrl != null}"><a href="assets/pdf/<c:out value="${service.servicePdfUrl}"/>" target="_blank"><img src="assets/Image/Layout/pdf.gif" border="0"/></a></c:if></td>
    <td align="center"><c:out value="${service.item.itemId}"/></td>	
	<td align="center"><c:out value="${service.status}"/></td>
	<td align="center"><c:out value="${service.purchaseOrder}"/>
		<c:if test="${service.purchaseOrderPdfUrl != null}"><a href="assets/pdf/<c:out value="${service.purchaseOrderPdfUrl}"/>" target="_blank"><img src="assets/Image/Layout/pdf.gif" border="0"/></a></c:if></td>
	<td align="center"><c:out value="${service.wo3rd}"/>
		<c:if test="${service.wo3rdPdfUrl != null}"><a href="assets/pdf/<c:out value="${service.wo3rdPdfUrl}"/>" target="_blank"><img src="assets/Image/Layout/pdf.gif" border="0"/></a></c:if></td>
	<td><c:if test="${service.orderId != null}"><a href="invoice.jhtm?order=${service.orderId}" class="nameLink"><c:out value="${service.orderId}"/></a></c:if>
		<c:if test="${service.pdfUrl != null}">&nbsp;&nbsp;<a href="assets/pdf/<c:out value="${service.pdfUrl}"/>" target="_blank"><img src="assets/Image/Layout/pdf.gif" border="0"/></a></c:if>
    	<c:if test="${service.servicePdfUrl != null}"><a href="assets/pdf/<c:out value="${service.servicePdfUrl}"/>" target="_blank"><img src="assets/Image/Layout/pdf.gif" border="0"/></a></c:if></td>
    <td align="center"><fmt:formatDate type="date" pattern="M/dd/yy h:mm a" value="${service.reportDate}"/></td>	
    <td align="center"><fmt:formatDate type="date" pattern="M/dd/yy" value="${service.serviceDate}"/>
    		<fmt:formatDate type="time" timeStyle="short" value="${service.startTime}"/>
    		</td>	
	<c:if test="${model.hasTrackNumIn}">
	<td align="center">
	<c:choose>
	 <c:when test="${service.trackNumInCarrier == 'UPS'}">
	  <a href="http://wwwapps.ups.com/WebTracking/processInputRequest?HTMLVersion=5.0&sort_by=status&tracknums_displayed=10&TypeOfInquiryNumber=T&loc=en_US&InquiryNumber1=<c:out value='${service.trackNumIn}' />&AgreeToTermsAndConditions=yes"><c:out value="${service.trackNumIn}" /></a>
	 </c:when>
	 <c:when test="${service.trackNumInCarrier == 'FedEx'}">
	  <a href="http://fedex.com/Tracking?action=track&language=english&cntry_code=us&mps=y&tracknumbers=<c:out value='${service.trackNumIn}' />&ascend_header=1"><c:out value="${service.trackNumIn}" /></a>
	 </c:when>
	 <c:when test="${service.trackNumInCarrier == 'USPS'}">
	  <a href="http://trkcnfrm1.smi.usps.com/PTSInternetWeb/InterLabelInquiry.do?strOrigTrackNum=<c:out value='${service.trackNumIn}' />"><c:out value="${service.trackNumIn}" /></a>
	 </c:when>
	 <c:otherwise><c:out value="${service.trackNumIn}" /></c:otherwise>
	</c:choose>	
	</td>
	</c:if>
	<c:if test="${model.hasTrackNumOut}">
	<td align="center">
	<c:choose>
	 <c:when test="${service.trackNumOutCarrier == 'UPS'}">
	  <a href="http://wwwapps.ups.com/WebTracking/processInputRequest?HTMLVersion=5.0&sort_by=status&tracknums_displayed=10&TypeOfInquiryNumber=T&loc=en_US&InquiryNumber1=<c:out value='${service.trackNumOut}' />&AgreeToTermsAndConditions=yes"><c:out value="${service.trackNumOut}" /></a>
	 </c:when>
	 <c:when test="${service.trackNumOutCarrier == 'FedEx'}">
	  <a href="http://fedex.com/Tracking?action=track&language=english&cntry_code=us&mps=y&tracknumbers=<c:out value='${service.trackNumOut}' />&ascend_header=1"><c:out value="${service.trackNumOut}" /></a>
	 </c:when>
	 <c:when test="${service.trackNumOutCarrier == 'USPS'}">
	  <a href="http://trkcnfrm1.smi.usps.com/PTSInternetWeb/InterLabelInquiry.do?strOrigTrackNum=<c:out value='${service.trackNumOut}' />"><c:out value="${service.trackNumOut}" /></a>
	 </c:when>
	 <c:otherwise><c:out value="${service.trackNumOut}" /></c:otherwise>
	</c:choose>	
	</td>
	</c:if>
	<c:if test="${model.hasItemFrom}">
	<td align="center"><c:out value="${service.itemFrom}"/></td>
	</c:if>
	<c:if test="${model.hasItemTo}">
	<td align="center"><c:out value="${service.itemTo}"/></td>
	</c:if>
	<c:if test="${model.hasShipDate}">
  	<td align="center"><fmt:formatDate type="date" timeStyle="default" value="${service.shipDate}"/></td>
	</c:if>
  </tr>
</c:forEach>
<c:if test="${model.services.nrOfElements == 0}">
  <tr class="emptyList"><td colspan="${7+cols}">&nbsp;</td></tr>
</c:if>
</table>
<table border="0" cellpadding="0" cellspacing="1" width="100%">
  <tr>
  <td>
	<input type="submit" name="__add" value="<fmt:message key="new" /> <fmt:message key="serviceRequest" />" <c:if test="${sessionCustomer.suspended}">onClick="return confirm('Account is suspended. Create Service Request?')"</c:if>>
  </td>
  <td align="right">
  <select name="size" onchange="submit()">
    <c:forTokens items="10,25,50" delims="," var="current">
  	  <option value="${current}" <c:if test="${current == model.services.pageSize}">selected</c:if>>${current} <fmt:message key="perPage" /></option>
    </c:forTokens>
  </select>
  </td>
  </tr>
</table> 
	</td>
  </tr>
</table>
</form>

  </tiles:putAttribute>
</tiles:insertDefinition>
