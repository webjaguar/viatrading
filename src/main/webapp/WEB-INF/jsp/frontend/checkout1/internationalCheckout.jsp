<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="/WEB-INF/tlds/spring.tld" prefix="spring" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>

<tiles:insertDefinition name="${_template}" flush="true">
  <tiles:putAttribute name="content" type="string">
<c:out value="${model.shippingLayout.headerHtml}" escapeXml="false"/>


<div align="center">
<iframe src="${_contextpath}/inck.jsp" width="400" height="400" frameborder="0"></iframe>
</div>



<form name="icForm" id="icForm" method="post"  action="${siteConfig['INTERNATIONAL_CHECKOUT'].value}" > 
	<c:forEach items="${cart.cartItems}" var="lineItem" varStatus="status">
		<input type="hidden" name="ItemDescription${status.index+1}" value="${lineItem.product.name}"> 
		<input type="hidden" name="ItemSKU${status.index+1}" value="${lineItem.product.sku}"> 
		<input type="hidden" name="ItemSize${status.index+1}" value="${lineItem.product.boxSize}"> 
		<input type="hidden" name="ImgHeight${status.index+1}" value="${lineItem.product.packageH}">
		<input type="hidden" name="ImgWidth${status.index+1}" value="${lineItem.product.packageW}">
		<input type="hidden" name="ItemQuantity${status.index+1}" value="${lineItem.quantity }"> 
		<input type="hidden" name="ItemPrice${status.index+1}" value="${lineItem.unitPrice}"> 
		<input type="hidden" name="ItemImage${status.index+1}" value="${lineItem.product.thumbnail}"> 
		<input type="hidden" name="p" value="${siteConfig['INTERNATIONAL_CHECKOUT_P_VALUE'].value}">
	</c:forEach>
</form>


<script>document.getElementById('icForm').submit();</script>

 </tiles:putAttribute>
</tiles:insertDefinition>