<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>

<c:set var="centinelOrderId" value="<%= request.getAttribute(\"centinelOrderId\") %>" />
<tiles:insertDefinition name="${_template}" flush="true">
  <tiles:putAttribute name="content" type="string">

<c:out value="${model.cardinalCentinelLayout.headerHtml}" escapeXml="false"/>

<div align="center">
<iframe src="${_contextpath}/centinel.jsp" width="400" height="400" frameborder="0"></iframe>
</div>

<c:out value="${model.cardinalCentinelLayout.footerHtml}" escapeXml="false"/>

<script type="text/javascript">
<!--
function submitForm(PaRes) {
	document.getElementById('_PaRes').value=PaRes;
	document.getElementById('centinelForm').submit();
}
//-->
</script>

<form id="centinelForm" method="post">
<input type="hidden" name="_page" value="4"/>
<input type="hidden" name="centinelOrderId" value="${centinelOrderId}" />
<input type="hidden" id="_PaRes" name="_PaRes" value=""/>
<input type="hidden" name="_centinelDebug" value="false"/>
<input type="hidden" name="_finish"/>
</form>

<iframe src="${_contextpath}/sessionKeepAlive.jsp" width="0" height="0" frameborder="0"></iframe>

  </tiles:putAttribute>
</tiles:insertDefinition>