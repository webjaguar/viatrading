<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib uri="/WEB-INF/tlds/spring.tld" prefix="spring" %>
<%@ taglib uri="/WEB-INF/tlds/custom.tld" prefix="wj" %>

<tiles:insertDefinition name="${_template}" flush="true">
  <tiles:putAttribute name="content" type="string">

<c:if test="${gSiteConfig['gSALES_PROMOTIONS']}"> 
<script type="text/javascript" >
<!--
<c:if test="${gSiteConfig['gSHOW_ELIGIBLE_PROMOS']}">
window.addEvent('domready', function() {
	Element.implement({
		show: function() {
			this.setStyle('display','');
		},
		hide: function() {
			this.setStyle('display','none');
		}
	});
});
function showPromo( dropdown ){
	var eliPromo = $$('div.eligiblePromoBox');
	eliPromo.each(function(el) {
		$(el).hide();
	});
	var myindex  = dropdown.selectedIndex;
    var SelValue = dropdown.options[myindex].value;
    var SelText = dropdown.options[myindex].text
    if (myindex!=0) {
    	$('order.promoCode').value = SelText;
    	$("promo"+SelValue).show();
    } else {
    	$('order.promoCode').value = "";
    }
}
</c:if>
function loadPromoCode( aform ) {
	if ( document.getElementById('order.promoCode').value == '') {
		alert("Promo code is empty!");
		return false;
	}
}
-->	
</script>
</c:if>
<script type="text/javascript" >
<!--
function checkPO( poRequired ) {
	if ( poRequired == 'warning' && document.getElementById('order.purchaseOrder').value == '' ) {
		reply = confirm("You have not typed a Purchase Order #. Purchase anyway?");
    	if (reply == false) {
     	document.getElementById('order.purchaseOrder').focus();
     	return false;
     	}
     	if (reply == true) document.getElementById('order.invoiceNote').value = "Call to request PO before shipping. \n" +  document.getElementById('order.invoiceNote').value;
	} else if ( poRequired == 'required' && document.getElementById('order.purchaseOrder').value == '' ) {
		alert("Please enter Purchase Order #");
   		document.getElementById('order.purchaseOrder').focus();
   		return false;
	}
}
function openW(thisURL) {
	var options="toolbar=no,location=no,directories=no,status=no,"
		+ "menubar=no,scrollbars=yes,resizable=no,copyhistory=no,"
		+ "width=550,height=300";
	cvv2 = window.open(thisURL,"cvv2",options);
	cvv2.focus();
	return true;
}
-->	
</script>   
<body>
<div style="margin:auto;" class="preliminaryWrapper">

<c:out value="${invoiceLayout.headerHtml}" escapeXml="false"/>
<c:out value="${model.cardinalCentinelLayout.headerHtml}" escapeXml="false"/>

<div class="customerInfoBox" id="customerInfoBoxId">
<div class="customerShippingInfoBox">
	<form:form commandName="orderForm" action="checkout1.jhtm" method="post">
	<input type="hidden" name="newShippingAddress" value="true"> 
	<div class="headerTitle shippingInfo"><span>1.</span> <p><fmt:message key="shippingInformation" /></p></div>
	<div class="customerShippingInfo">
	<c:set value="${orderForm.order.shipping}" var="address"/>
	<%@ include file="/WEB-INF/jsp/frontend/common/address.jsp" %>
	<input type="image" border="0" name="_target0" src="assets/Image/Layout/button_change${_lang}.gif" />
	</div>
	<div class="footerTitle shippingInfo"></div>
	</form:form>
</div>
<div class="customerBillingInfoBox">
	<form:form commandName="orderForm" action="checkout1.jhtm" method="post">
	<input type="hidden" name="newBillingAddress" value="true">
	<div class="headerTitle billingInfo"><span>2.</span> <p><fmt:message key="billingInformation" /></p></div>
	<div class="customerBillingInfo">
	<c:set value="${orderForm.order.billing}" var="address"/>
	<c:set value="true" var="billing" />
	<%@ include file="/WEB-INF/jsp/frontend/common/address.jsp" %>
	<div id="emailAddress"><b><fmt:message key="emailAddress" />:</b>&nbsp;<c:out value="${orderForm.username}"/></div>
	<c:set value="false" var="billing" />
	<input type="image" border="0" name="_target1" src="assets/Image/Layout/button_change${_lang}.gif" />
	</div>
	<div class="footerTitle billingInfo"></div>
	</form:form>
</div>
</div>

<div class="mainFormWrapper">
<form:form commandName="orderForm" action="checkout1.jhtm" method="post">
<input type="hidden" name="_centinelDebug" value="false">

<div class="shippingBox">
<div class="headerTitle shippingMethod"><span>3.</span> <p><fmt:message key="shippingMethod" /></p></div>
<c:if test="${orderForm.customRatesList != null and orderForm.order.hasRegularShipping}">
<div class="subHeaderTitle">
<fmt:message key="forLineItem(s)"/>
<c:forEach var="lineItem" items="${orderForm.order.lineItems}" varStatus="lineItemStatus">
<c:if test="${empty lineItem.customShippingCost}">
  <c:out value="${lineItem.lineNumber}"/>,
 </c:if>
 </c:forEach>
 </div>
</c:if>

<c:if test="${orderForm.order.hasRegularShipping}">
<spring:bind path="orderForm.shippingRateIndex">
<div class="shippingWrapper1" id="shippingWrapperId1">
<span class="error"><c:out value="${status.errorMessage}"/></span>
<table class="shippingTable1" cellpadding="0" cellspacing="0">
  <c:forEach items="${orderForm.ratesList}" var="shippingrate" varStatus="loopStatus">
    <tr>
      <td align="center" height="45px">
        <c:if test="${shippingrate.carrier == 'fedex'}" >
        <c:set var="fedEx" value="true" />
        <c:choose>
         <c:when test="${fn:contains(shippingrate.code,'EXPRESS') or fn:contains(shippingrate.code,'OVERNIGHT') or fn:contains(shippingrate.code,'2_DAY')}"><img width="80%" class="carrier fedex" src="assets/Image/Layout/FedEx_Express_Normal_2Color_Positive_20.gif" /></c:when>
         <c:when test="${fn:contains(shippingrate.code,'GROUND_HOME')}"><img width="80%" class="carrier fedex" src="assets/Image/Layout/FedEx_HomeDelivery_Normal_2Color_Positive_20.gif" /></c:when>
         <c:when test="${fn:contains(shippingrate.code,'GROUND')}"><img width="80%" class="carrier fedex" src="assets/Image/Layout/FedEx_Ground_Normal_2Color_Positive_20.gif" /></c:when>
         <c:when test="${fn:contains(shippingrate.code,'FREIGHT')}"><img width="80%" class="carrier fedex" src="assets/Image/Layout/FedEx_Freight_Normal_2Color_Positive_20.gif" /></c:when>
         <c:otherwise><img width="80%" class="carrier fedex" src="assets/Image/Layout/FedEx_MultipleServices_Normal_2Color_Positive_20.gif" /></c:otherwise>
        </c:choose>
        </c:if>
        <c:if test="${shippingrate.carrier == 'ups'}" >
         <c:set var="ups" value="true" />
         <img width="30%" class="carrier ups" src="assets/Image/Layout/UPS_LOGO_S.gif" />
        </c:if>
      </td>
      <td>
       <input type="radio" id="scr_${loopStatus.index}" name="<c:out value="${status.expression}"/>" value="${loopStatus.index}" <c:if test="${(status.value == loopStatus.index) || (shippingrate.title == 'Free')}">checked</c:if> onclick="submit()">
      </td>
      <td>
        <c:out value="${shippingrate.title}" escapeXml="false"/>
      </td>
      <td align="right">
        <c:if test="${shippingrate.price != null}"><fmt:message key="${siteConfig['CURRENCY'].value}" /></c:if><fmt:formatNumber value="${shippingrate.price}" pattern="#,##0.00"/>
      </td>
      <c:if test="${siteConfig['DELIVERY_TIME'].value == 'true' and shippingrate.carrier == 'ups'}">
      <td style="padding-left:10px;"align="center">
        <c:choose>
 	 	  <c:when test="${shippingrate.code == '03' and empty shippingrate.deliveryDate}"><a onclick="window.open(this.href,'','width=740,height=550'); return false;" href="category.jhtm?cid=49">See Map</a></c:when>
 	 	  <c:when test="${shippingrate.code == '03' and !empty shippingrate.deliveryDate}">Delivery Date: <fmt:formatDate type="date" timeStyle="default" value="${shippingrate.deliveryDate}"/><div style="font-size:10px;color:#444444;">( Not Guaranteed )</div></c:when>
 	 	  <c:otherwise>Delivery Date: <fmt:formatDate type="date" timeStyle="default" value="${shippingrate.deliveryDate}"/></c:otherwise>
 		</c:choose>
      </td>
      </c:if> 
      <c:if test="${shippingrate.description != null}">
      <td>
        <div class="shippingDescWrapper"><c:out value="${shippingrate.description}" escapeXml="false"/></div>
      </td>
      </c:if>
    </tr>
  </c:forEach>
</table>
</div>
<c:if test="${fedEx}">
 <c:out value="FedEx<sup>&#174;</sup> service marks are owned by Federal Express Corporation and used with permission." escapeXml="false"/>
</c:if>
<c:if test="${ups}"><br />
<div class="upsDisclaimer">
	 <c:out value="UPS, the UPS brand mark, and the Color Brown are trademarks of 
	United Parcel Service of America, Inc. All Rights Reserved." escapeXml="false"/>
</div>
</c:if>
</spring:bind>
</c:if>


<c:if test="${orderForm.customRatesList != null}">
<c:if test="${orderForm.order.hasRegularShipping}">
<div class="subHeaderTitle">
<fmt:message key="forLineItem(s)"/>
<c:forEach var="lineItem" items="${orderForm.order.lineItems}" varStatus="lineItemStatus">
<c:if test="${!empty lineItem.customShippingCost}">
<c:out value="${lineItem.lineNumber}"/>,
</c:if>
</c:forEach>
</div>
</c:if>

<spring:bind path="orderForm.customShippingRateIndex">
<span class="error"><c:out value="${status.errorMessage}"/></span>
<div class="shippingWrapper2" id="shippingWrapperId2">
<table class="shippingTable2" cellpadding="0" cellspacing="0">
  <c:forEach items="${orderForm.customRatesList}" var="shippingrate" varStatus="loopStatus">
    <tr>
      <td>
        <input type="radio" id="ccr_${loopStatus.index}" name="<c:out value="${status.expression}"/>" value="${loopStatus.index}" <c:if test="${status.value == loopStatus.index}">checked</c:if> onclick="submit()">
      </td>
      <td>
        <c:out value="${shippingrate.title}" escapeXml="false"/>
      </td>
      <td align="right">
        <c:if test="${shippingrate.price != null}"><fmt:message key="${siteConfig['CURRENCY'].value}" /></c:if><fmt:formatNumber value="${shippingrate.price}" pattern="#,##0.00"/>
      </td>  
    </tr>
  </c:forEach>
</table>
</div>
</spring:bind>
</c:if>
<div class="footerTitle shippingMethod"></div>
</div>

<div class="paymentBox">
<div class="headerTitle paymentMethod"><span>4.</span> <p><fmt:message key="f_paymentMethod" /></p></div>

<c:choose>

<c:when test="${orderForm.order.promoAvailable && orderForm.order.grandTotal <= 0}">

<!-- If Promo is available and Grand Total == 0 then disable the payment method -->
  <fmt:message key="f_paymentNotRequired"></fmt:message>
</c:when>

<c:otherwise>
<!-- Payment Method -->
<div class="paymentWrapper" id="paymentWrapperId">
<spring:bind path="orderForm.order.paymentMethod">
<span class="error"><c:out value="${status.errorMessage}"/></span>
</spring:bind>

<c:choose>
  <c:when test="${orderForm.custPayment != null and orderForm.custPayment != ''}">
<div><input type="radio" value="<c:out value="${orderForm.custPayment}"/>" name="order.paymentMethod" checked> <b><c:out value="${orderForm.custPayment}"/></b></div>
  </c:when>
  <c:otherwise>
<div class="paymentChoice" id="paymentChoiceId">
<table class="paymentTable" cellpadding="0" cellspacing="0">

<c:if test="${orderForm.order.grandTotal > 0.0}">	    
<c:if test="${siteConfig['GEMONEY_DOMAIN'].value != ''
	and fn:trim(siteConfig['GEMONEY_MERCHANTID'].value) != ''
	and fn:trim(siteConfig['GEMONEY_PASSWORD'].value) != ''
	and gSiteConfig['gPAYPAL'] < 2
	}">
	
<c:set value="true" var="gemoneybox"/>
<tr id="gemoneyPayment">
 <td><form:radiobutton path="order.paymentMethod" id="ge_radio" value="GE Money" onclick="togglePayment()"/></td>  
 <td class="paymentMethod"><c:out value="${siteConfig['GEMONEY_PROGRAM_NAME'].value}"/></td>
</tr>
</c:if>

<c:if test="${gSiteConfig['gPAYPAL'] < 2 and siteConfig['CREDIT_CARD_PAYMENT'].value != ''}">
<c:set value="true" var="creditcardbox"/>
<tr id="creditCardPayment">
 <td><form:radiobutton path="order.paymentMethod" id="cc_radio" value="Credit Card" onclick="togglePayment()"/></td>  
 <td class="paymentMethod"><span><fmt:message key="shoppingcart.creditcard" /></span><img  src="assets/Image/Layout/creditCard.jpg" border="0" alt="<fmt:message key='shoppingcart.creditcard' />"></td>
</tr>
</c:if>

<c:if test="${gSiteConfig['gPAYPAL'] > 0}">
<tr id="paypalPayment">
 <td><form:radiobutton path="order.paymentMethod" value="PayPal" onclick="togglePayment()"/></td>  
 <td><a href="#" onclick="javascript:window.open('https://www.paypal.com/cgi-bin/webscr?cmd=xpt/Marketing/popup/OLCWhatIsPayPal-outside','olcwhatispaypal','toolbar=no, location=no, directories=no, status=no, menubar=no, scrollbars=yes, resizable=yes, width=400, height=350');"><img  src="https://www.paypal.com/en_US/i/logo/PayPal_mark_60x38.gif" border="0" alt="Acceptance Mark"></a>
 <div id="paypalNote" class="note"><c:out value="${siteConfig['PAYPAL_PAYMENT_METHOD_NOTE'].value}" escapeXml="false"/></div>
 </td>
</tr>
</c:if>
</c:if>

<c:if test="${gSiteConfig['gPAYPAL'] < 2}">
<c:if test="${orderForm.order.grandTotal > 0.0}">

<c:if test="${siteConfig['AMAZON_URL'].value != ''
	and fn:trim(siteConfig['AMAZON_MERCHANT_ID'].value) != ''
	and fn:trim(siteConfig['AMAZON_ACCESS_KEY'].value) != ''
	and fn:trim(siteConfig['AMAZON_SECRET_KEY'].value) != ''
	}">
<tr id="amazonPayment">
 <td><form:radiobutton path="order.paymentMethod" value="Amazon" onclick="togglePayment()"/></td>  
 <td><img src="assets/Image/Layout/amazon-payments.jpg" border="0" alt="Amazon Payments"></td>
</tr>		
</c:if>

<c:if test="${gSiteConfig['gCREDIT_CARD_PAYMENT'] == 'netcommerce'}">
<tr id="netcommercePayment">
 <td><form:radiobutton path="order.paymentMethod" value="NetCommerce" onclick="togglePayment()"/></td>  
 <td class="paymentMethod">NetCommerce</td>
</tr>
</c:if>

<c:if test="${gSiteConfig['gCREDIT_CARD_PAYMENT'] == 'bankaudi'}">
<tr id="bankaudiPayment">
 <td><form:radiobutton path="order.paymentMethod" value="BankAudi" onclick="togglePayment()"/></td>  
 <td class="paymentMethod">BankAudi</td>
</tr>
</c:if>

<c:if test="${siteConfig['EBILLME_URL'].value != ''
	and fn:trim(siteConfig['EBILLME_MERCHANTTOKEN'].value) != ''
	and fn:trim(siteConfig['EBILLME_CANCEL_URL'].value) != ''
	and fn:trim(siteConfig['EBILLME_ERROR_URL'].value) != ''
	}">
<c:set value="true" var="ebillmebox"/>	
<tr id="ebillmePayment">
 <td><form:radiobutton path="order.paymentMethod" id="ebillme_radio" value="eBillme" onclick="togglePayment()"/></td>  
 <td><img src="https://www.ebillme.com/checkout/eBillme_logo_large_95x40.gif" alt="eBillme - Secure Cash Payments" title="eBillme - Secure Cash Payments" width="95" height="40" border="0" /></td>
</tr>	
</c:if>

<c:if test="${siteConfig['GOOGLE_CHECKOUT_URL'].value != ''}">
<c:set value="true" var="googlebox"/>
<tr id="googlePayment">
 <td><form:radiobutton path="order.paymentMethod" id="google_radio" value="Google" onclick="togglePayment()"/></td>  
 <td><img src="https://checkout.google.com/buttons/checkout.gif?merchant_id=${siteConfig['GOOGLE_MERCHANT_ID'].value}&w=168&h=44&style=trans&variant=text&loc=en_US" alt="Google Checkout" /></td>
</tr>	
</c:if>
</c:if>

<!-- Custome Payments -->
<c:forEach items="${model.customPayments}" var="paymentMethod">
<c:if test="${paymentMethod.enabled and not paymentMethod.internal and (not (fn:toLowerCase(fn:trim(paymentMethod.title)) == 'credit card') or siteConfig['CREDIT_CARD_PAYMENT'].value == '')}">
<tr class="${fn:replace(paymentMethod.title, ' ', '')}Checkout">
 <td><form:radiobutton path="order.paymentMethod" value="${paymentMethod.title}" onclick="submit()"/></td>  
 <%--check paymentMethod viatrading only use custome paymentMethod, chage the onclick function to submit() & if is "paypal" or "credit card", add 3% CC Fee  --%>
 <td class="paymentMethod"><c:out value="${paymentMethod.title}"/></td> 
</tr>
</c:if>
</c:forEach>

</c:if>
</table>
</div>  
<div class="paymentSelectd" id="paymentSelectedId">
<div class="spacer"></div>

<c:if test="${creditcardbox}">
<div id="creditcardbox" <c:if test="${orderForm.order.paymentMethod != 'Credit Card'}">style="display:none;"</c:if>> 

<c:if test="${centinelAuthenticationFailure}">
<div align="center">
<b>Authentication Failed</b><br/><br/>
Your financial institution has indicated that it could not successfully authenticate this transaction. To protect against unauthorized use, this card cannot be used to complete your purchase. You may complete the purchase by selecting another form of payment.
<br/><br/>
</div>
</c:if>

<form:errors path="order.creditCard.expireMonth">
<c:if test="${siteConfig['CC_FAILURE_URL'].value != ''}">
<c:catch var="ccFailureUrlException">
<c:import url="${siteConfig['CC_FAILURE_URL'].value}"/>
</c:catch>
</c:if>
<c:if test="${siteConfig['CC_FAILURE_URL'].value == '' or ccFailureUrlException != null}">
<div id="ccInfoError">
	<table border="0" cellspacing="0" cellpadding="0" align="center" >
	  <tr>
	    <td><b><fmt:message key="shoppingcart.pleaseCallUs" /><fmt:message key="shoppingcart.thereSeemsToBeAProblemWithYourCCInformation" /></b><br />
			<fmt:message key="shoppingcart.ForYourReferenceATransmissionErrorEmailHasBeenForwardedToYou" />
			</td>
	  </tr>
	  <tr>
	    <td><b><fmt:message key="shoppingcart.mostErrorsAreSimplyTypographical" /></b><br />
			<fmt:message key="shoppingcart.weAskYouToReviewTheFollowing" /></td>
	  </tr>
	  <tr>
	    <td><div style="color:red;text-align:left">
	      <ul>
	        <li><fmt:message key="shoppingcart.creditCardNumber" /></li>
	        <li><fmt:message key="shoppingcart.ccidNumberCVV2" />
	          <ul>
	              <li><fmt:message key="shoppingcart.visaMasterCardLast3difitsOnBackOfCard" /></li>
	            <li><fmt:message key="shoppingcart.americanExp4DigitsInSmallPrintInTheUpper" /><br />
	              <fmt:message key="shoppingcart.rightSectionOfTheFrontOfTheCard" /></li>
	          </ul>
	        </li>
	        <li><fmt:message key="expirationDate" /></li>
	      </ul>
	    </div></td>
	  </tr>
	  <tr>
	    <td><div align="center"><fmt:message key="shoppingcart.ifYouContinueToGetThisError" /></div></td>
	  </tr>
	  <tr>
	    <td><div align="center"><b><fmt:message key="shoppingcart.needHelp" /><br />
	      <fmt:message key="shoppingcart.pleaseCallUs" />
	      <c:out value="${siteConfig['COMPANY_PHONE'].value}"/>
	    </b></div></td>
	  </tr>
	</table>
</div>
</c:if>
</form:errors>

<c:choose>
<c:when test="${gSiteConfig['gCREDIT_CARD_PAYMENT'] == 'ebizcharge' and siteConfig['PCI_FRIENDLY_EBIZCHARGE'].value == 'true'}">
	<div id="ccInfo">
		<c:import url="/WEB-INF/jsp/frontend/checkout/eBizChargeBillingMethod.jsp" />
	</div>
</c:when>
<c:otherwise>
<table border="0" cellspacing="0" cellpadding="0" class="ccInfoForm">
  <tr>
    <td colspan="2" class="enterInfo"><fmt:message key="enterYourInformationHere" />:</td>
  </tr>
  <tr>
    <td class="formName"><fmt:message key="cardType" />:*</td>
    <td>
	  <spring:bind path="orderForm.order.creditCard.type">
	  <select name="<c:out value="${status.expression}"/>">
	    <c:forTokens items="${siteConfig['CREDIT_CARD_PAYMENT'].value}" delims="," var="cc">
          <option value="${cc}" <c:if test="${status.value == cc}">selected</c:if>><fmt:message key="${cc}"/></option>
        </c:forTokens>
	  </select>
      <font color = "red"><c:out value="${status.errorMessage}"/></font>
      </spring:bind>   
	</td>
   </tr>
   <tr>
     <td class="formName"><fmt:message key="cardNumber" />:*</td>
     <td>
	   <spring:bind path="orderForm.order.creditCard.number">
	   <input id="ccNum" type="text" name="<c:out value="${status.expression}"/>" value="<c:out value="${status.value}"/>" <c:if test="${orderForm.order.paymentMethod != 'Credit Card'}">disabled</c:if> maxlength="16" size="16">
       <font color = "red"><c:out value="${status.errorMessage}"/></font>
       </spring:bind>   
       <!-- debug: <c:out value="${orderForm.order.creditCard.paymentNote}"/> -->
	 </td>
   </tr>
   <tr>
     <td class="formName"><fmt:message key="cardVerificationCode" />:*</td>
     <td>
	   <spring:bind path="orderForm.order.creditCard.cardCode">
	   <input type="text" name="<c:out value="${status.expression}"/>" value="<c:out value="${status.value}"/>" maxlength="4" size="4">
       <font color = "red"><c:out value="${status.errorMessage}"/></font>
       </spring:bind>   
	   <a class="cvv2" href="#" onClick="openW('assets/CVV2.html')"><fmt:message key="shoppingcart.whereDoIFindthisCode" /></a>
	 </td>
   </tr>
   <tr>
     <td class="formName"><fmt:message key="expDate" /> (MM/YY):*</td>
     <td>
	   <spring:bind path="orderForm.order.creditCard.expireMonth">
	   <select name="<c:out value="${status.expression}"/>">
	    <c:forTokens items="01,02,03,04,05,06,07,08,09,10,11,12" delims="," var="ccExpireMonth">
          <option value="${ccExpireMonth}" <c:if test="${status.value==ccExpireMonth}">selected</c:if>><c:out value="${ccExpireMonth}"/></option>
        </c:forTokens>
	   </select>
       </spring:bind> 
       <spring:bind path="orderForm.order.creditCard.expireYear">
	   <select name="<c:out value="${status.expression}"/>">
	    <c:forEach items="${model.expireYears}" var="ccExpireYear">
         <option value="${ccExpireYear}" <c:if test="${status.value==ccExpireYear}">selected</c:if>><c:out value="${ccExpireYear}"/></option>
	    </c:forEach>
	   </select>
	   <font color="red"><c:out value="${status.errorMessage}"/></font>
       </spring:bind>  
	 </td>
   </tr>
<c:if test="${siteConfig['CENTINEL_URL'].value != ''
	and fn:trim(siteConfig['CENTINEL_PROCESSORID'].value) != ''
	and fn:trim(siteConfig['CENTINEL_MERCHANTID'].value) != ''
	and fn:trim(siteConfig['CENTINEL_TRANSACTIONPWD'].value) != ''
	}">
	<tr>
	 <td colspan="2">
	  <table border="0" cellspacing="0" cellpadding="0" >
	    <tr>
	      <td colspan="2">
	        Your card may be eligible or enrolled in Verified by Visa or MasterCard<sup>&reg;</sup> SecureCode&#153;
	        payer authentication programs. On the next page, after clicking "Purchase", your Card
	        Issuer may prompt you for your payer authentication password to complete your order. 
	      </td>
	    </tr>
	    <tr>
	      <td><a href="javascript:void window.open('assets/VerifiedByVisa/popup.html','serviceDescription','width=550,height=350')"><img src="assets/VerifiedByVisa/logo.gif" border="0"/></a></td>
	      <td><a href="javascript:void window.open('assets/MasterCardSecureCode/popup.html','serviceDescription','width=550,height=350')"><img src="assets/MasterCardSecureCode/logo.gif" border="0"/></a></td>
	    </tr>
	  </table>
	 </td>
	</tr>
</c:if>   
</table>
<c:if test="${siteConfig['CC_BILLING_ADDRESS'].value == 'true'}" >
<div class="ccBilling">
	<lable><fmt:message key="f_ccBillingAddres"/>: </lable>
	<form:textarea path="order.ccBillingAddress" rows="3" cols="40" htmlEscape="true" cssClass="textfield" />
</div>
</c:if>
</c:otherwise>
</c:choose>
</div>
</c:if>

<c:if test="${gemoneybox}">
<div id="gemoneybox" <c:if test="${orderForm.order.paymentMethod != 'GE Money'}">style="display:none;"</c:if>> 
<table border="0" cellspacing="0" cellpadding="0" width="100%">
  <tr>
    <td colspan="2" class="enterInfo"><fmt:message key="enterYourInformationHere" />:</td>
  </tr>
   <form:errors path="order.geMoney.acctNumber">
   <tr>
     <td class="formName" colspan="2">
       <form:errors path="order.geMoney.acctNumber" cssClass="error" />
	 </td>
   </tr>
   </form:errors>
   <tr>
     <td class="formName"><fmt:message key="accountNumber" />:*</td>
     <td>
       <form:input path="order.geMoney.acctNumber" maxlength="16" size="16" htmlEscape="true" />
	 </td>
   </tr>
   <tr>
     <td class="formName">&nbsp;</td>
     <td>-- AND/OR --</td>
   </tr>
   <form:errors path="order.geMoney.ssn">
   <tr>
     <td class="formName" colspan="2">
       <form:errors path="order.geMoney.ssn" cssClass="error" />
	 </td>
   </tr>
   </form:errors>
   <tr>
     <td class="formName"><fmt:message key="ssn" />:*</td>
     <td>
       <form:input path="order.geMoney.ssn" maxlength="9" size="16" htmlEscape="true" />
	 </td>
   </tr>   
   <form:errors path="order.billing.phone">   
   <tr>
     <td class="formName" colspan="2">
       <font color="red">Home Phone Number must be exactly 10 numbers and cannot start with a 1.</font>
	 </td>
   </tr>
   <tr>
     <td class="formName"><fmt:message key="billing" /> <fmt:message key="phone" />:*</td>
     <td>
       <form:input path="order.billing.phone" maxlength="10" size="16" htmlEscape="true"/>
	 </td>
   </tr>
   </form:errors>
   <form:errors path="order.billing.zip">
   <tr>
     <td class="formName">
       <font color="red">Zip Code must be 5 numbers. Please go to My Account to correct your zipcode.</font>
	 </td>
   </tr>
   <tr>
     <td class="formName"><fmt:message key="billing" /> <fmt:message key="zipCode" />:</td>
     <td>
       <c:out value="${orderForm.order.billing.zip}"/>
	 </td>
   </tr>
   </form:errors>   
   <tr>
     <td class="formName">&nbsp;</td><td>&nbsp;</td>
   </tr>
   <tr>
     <td class="formName" colspan="2">
Already have a finance account with us? Just enter in your account #<br/><br/>
Need to apply for financing? Just enter your SS# and you will be prompted to the application process once checkout is complete.<br/><br/>
Before we can process your order, the customer MUST fax or email a copy of your driver's license and a utility bill with your name and address to 1-636-438-1351 OR finance@number1direct.com.     
	 </td>
   </tr>
</table>
</div>
</c:if>

<c:if test="${ebillmebox}">
<div id="ebillmebox" <c:if test="${orderForm.order.paymentMethod != 'eBillme'}">style="display:none;"</c:if>>
 <div id="eBillmeNone" class="note"><p class="para" style="font-size: 12px; line-height: 13px; font-family: Arial, Helvetica, sans-serif; color: #000000; margin:0;"><strong>eBillme&#8482; Secure Cash Payments</strong><br />            <span class="para" style="font-size: 12px; line-height: 13px; font-family: Arial, Helvetica, sans-serif; color: #000000; margin:0;"><a href="https://about.ebillme.com/index.php/about/webjaguar" onclick="window.open('https://about.ebillme.com/index.php/about/webjaguar', 'eBillme_Learnmore', 'width=500,height=800, scrollbars=1, resizable=1'); return false">Learn more</a></span>.</div> 
</div>
</c:if>
</div>
  </c:otherwise>
</c:choose>
</div>
</c:otherwise>
</c:choose>

<div class="footerTitle paymentMethod"></div>

<c:if test="${fn:contains(siteConfig['USER_EXPECTED_DELIVERY_TIME'].value, 'true') or siteConfig['REQUESTED_CANCEL_DATE'].value == 'true'}" >
<link rel="stylesheet" type="text/css" media="all" href="javascript/jscalendar-1.0/skins/aqua/theme.css" title="Aqua" /> <!-- calendar stylesheet -->
<script type="text/javascript" src="javascript/jscalendar-1.0/calendar.js"></script> <!-- main calendar program -->
<script type="text/javascript" src="javascript/jscalendar-1.0/lang/calendar-en.js"></script>
<script type="text/javascript" src="javascript/jscalendar-1.0/calendar-setup.js"></script>
</c:if>

<c:if test="${fn:contains(siteConfig['USER_EXPECTED_DELIVERY_TIME'].value, 'true')}" >
<div class="userExpectedDueDate">
  <b><fmt:message key="userExpectedDueDate" />:</b>
  <br />
  <form:input path="order.userDueDate" size="10" maxlength="10" />
  <img src="assets/Image/Layout/calendarIcon.jpg" class="calendarImage" id="time_start_trigger"/>
	<c:if test="${!fn:contains(siteConfig['USER_EXPECTED_DELIVERY_TIME'].value, 'script')}" >
	  <script type="text/javascript">
   		Calendar.setup({
       			inputField     :    "order.userDueDate",   
       			showsTime      :    false,
       			ifFormat       :    "%m-%d-%Y",   
       			button         :    "time_start_trigger"   
   		});	
	  </script> 
	</c:if>
	<form:errors path="order.userDueDate" cssClass="error"/>	
  
</div>
</c:if>
	
<c:if test="${siteConfig['REQUESTED_CANCEL_DATE'].value == 'true'}" >
<div class="requestedCancelDate">
  <b><fmt:message key="requestedCancelDate" />:</b>
  <br />
  <form:input path="order.requestedCancelDate" size="10" maxlength="10" />
	  <img src="assets/Image/Layout/calendarIcon.jpg" class="calendarImage" id="requestedCancelDate_trigger"/>
	  <script type="text/javascript">
   		Calendar.setup({
       			inputField     :    "order.requestedCancelDate",   
       			showsTime      :    false,
       			ifFormat       :    "%m-%d-%Y",   
       			button         :    "requestedCancelDate_trigger"   
   		});	
	  </script> 
	<form:errors path="order.requestedCancelDate" cssClass="error"/>
</div>
</c:if>	


</div>

<c:if test="${orderForm.order.shippingMessage != null and !empty orderForm.order.shippingMessage}">
<div class="shippingMessage" id="shippingMessageId">
<c:out value="${orderForm.order.shippingMessage}" escapeXml="false"></c:out>
</div>
</c:if>
<div class="cartHeaderTitle reviewOrder"><span>5.</span> <p><fmt:message key="reviewOrder" /></p></div>
<div class="reviewOrderBox" >
<table border="0" cellpadding="2" cellspacing="0" width="100%" class="invoice">
  <tr class="shoppingCart">
	<c:set var="cols" value="0"/>
    <th width="5%" class="invoice"><fmt:message key="line" />#</th>
    <th class="invoice"><fmt:message key="productSku" /></th>
    <th class="invoice"><fmt:message key="productName" /></th>
    <c:forEach items="${orderForm.productFieldsHeader}" var="productField">
      <c:if test="${productField.showOnInvoice}">
      <th class="invoice"><c:out value="${productField.name}" /></th><c:set var="cols" value="${cols+1}"/>
      </c:if>
    </c:forEach>
    <th width="10%" class="invoice"><fmt:message key="quantity" /></th>
    <c:if test="${gSiteConfig['gINVENTORY'] && orderForm.order.hasLowInventoryMessage}">
      <c:set var="cols" value="${cols+1}"/>
	  <th width="10%" class="invoice"><fmt:message key="lowInventory" /></th>
	</c:if>
    <c:if test="${siteConfig['CASE_CONTENT_LAYOUT'].value == ''}">	
	    <c:if test="${orderForm.order.hasPacking}">
		  <c:set var="cols" value="${cols+1}"/>
	      <th width="10%" class="invoice">1<fmt:message key="packing" /></th>
	    </c:if>
	    <c:if test="${orderForm.order.hasContent}">
		  <c:set var="cols" value="${cols+1}"/>
	      <th class="invoice"><fmt:message key="content" /></th>
	    </c:if>
	</c:if>	 
	<c:choose>
      <c:when test="${siteConfig['CASE_CONTENT_LAYOUT'].value == ''}">
          <th class="invoice"><div><fmt:message key="productPrice" /><c:if test="${orderForm.order.hasContent}">/<c:out value="${siteConfig['CASE_UNIT_TITLE'].value}" /></c:if></div></th>
      </c:when>
      <c:otherwise>
          <th class="invoice"><div><fmt:message key="productPrice" /></div></th>
      </c:otherwise>
    </c:choose>
    <c:if test="${orderForm.order.hasParentDeal}">
    <c:choose>
      <c:when test="${siteConfig['CASE_CONTENT_LAYOUT'].value == ''}">
          <th class="invoice"><div><fmt:message key="originalPrice" /><c:if test="${orderForm.order.hasContent}">/<c:out value="${siteConfig['CASE_UNIT_TITLE'].value}" /></c:if></div></th>
      </c:when>
      <c:otherwise>
          <th class="invoice"><div><fmt:message key="originalPrice" /></div></th>
      </c:otherwise>
    </c:choose>
    <c:choose>
      <c:when test="${siteConfig['CASE_CONTENT_LAYOUT'].value == ''}">
          <th class="invoice"><div><fmt:message key="parentDealDiscount" /><c:if test="${orderForm.order.hasContent}">/<c:out value="${siteConfig['CASE_UNIT_TITLE'].value}" /></c:if></div></th>
      </c:when>
      <c:otherwise>
          <th class="invoice"><div><fmt:message key="parentDealDiscount" /></div></th>
      </c:otherwise>
    </c:choose>
    <c:set var="cols" value="${cols+2}"/>
    </c:if>
    <c:if test="${orderForm.order.lineItemPromos != null and fn:length(orderForm.order.lineItemPromos) gt 0}">
      <c:set var="cols" value="${cols+1}"/>
	  <th class="invoice"><fmt:message key="discount" /></th>
	</c:if>
    <th class="invoice"><fmt:message key="total" /></th>
  </tr>
<c:set var="previousItemGroup"/>
<c:set var="nextItemGroup"/>
<c:forEach var="lineItem" items="${orderForm.order.lineItems}" varStatus="lineItemStatus">
<c:set var="nextItemGroup" value="${orderForm.order.lineItems[lineItemStatus.index +1].itemGroup}"/>
<div>
  <c:if test="${lineItem.itemGroup != null and (previousItemGroup == null or previousItemGroup != lineItem.itemGroup)}">
    <c:set var="displaySkuAndName" value="true"/>
    <%@ include file="/WEB-INF/jsp/frontend/checkout/subPreliminary.jsp" %>
    <c:set var="displaySkuAndName" value=""/>
  </c:if>
  <c:set var="previousItemGroup" value="${lineItem.itemGroup}"/>
</div>
<tr class="shoppingCart${lineItemStatus.index % 2}" valign="top">
  <td class="invoice" align="center"><c:out value="${lineItem.lineNumber}"/></td>
  <td class="invoice">
	<c:if test="${lineItem.customImageUrl != null}">
      <a href="framer/pictureframer/images/FRAMED/${lineItem.customImageUrl}" onclick="window.open(this.href,'','width=640,height=480,resizable=yes'); return false;"><img class="invoiceImage" src="framer/pictureframer/images/FRAMED/${lineItem.customImageUrl}" border="0" /></a>
    </c:if>
    <c:if test="${lineItem.attachment != null}">
      <a href="${_contextpath}/temp/Cart/customer/${orderForm.order.userId}/${lineItem.attachment}">
    	<c:out value="${lineItem.attachment}"/>
      </a>
    </c:if>
	<c:if test="${siteConfig['SHOW_IMAGE_ON_INVOICE'].value == 'true' and lineItem.product.thumbnail != null}">
      <img class="invoiceImage" src="<c:if test="${!lineItem.product.thumbnail.absolute}">${_contextpath}/assets/Image/Product/thumb/</c:if>${lineItem.product.thumbnail.imageUrl}" border="0" /><br />
    </c:if><div class="sku"><c:out value="${lineItem.product.sku}"/></div></td>
  <td class="invoice"><c:if test="${lineItem.customXml != null}">Custom Frame - </c:if><c:out value="${lineItem.product.name}"/>
    <table border="0" cellspacing="1" cellpadding="0">
	  <c:forEach items="${lineItem.productAttributes}" var="productAttribute" varStatus="productAttributeStatus">
		<tr class="invoice_lineitem_attributes${productAttributeStatus.count%2}">
		<td class="optionName" style="white-space: nowrap" align="right"><c:out value="${productAttribute.optionName}"/>: </td>
		<td class="optionValue"> <c:out value="${productAttribute.valueString}" escapeXml="false"/></td>
		<c:if test="${(! empty productAttribute.optionPriceOriginal) && productAttribute.optionPriceOriginal != 0}">
		<td class="optionName"><fmt:formatNumber value="${productAttribute.optionPriceOriginal}" pattern="#,##0.00" /></td>
		<td class="optionNote" style="padding-left:5px;"><fmt:message key="${productAttribute.optionPriceMessageF}"/></td>
		</c:if>
		</tr>
	  </c:forEach>
	  <c:forEach items="${lineItem.asiProductAttributes}" var="asiProductAttribute" varStatus="asiProductAttributeStatus">
		<tr class="invoice_lineitem_attributes${asiProductAttributeStatus.count%2}">
		<td style="white-space: nowrap" align="right"><c:out value="${asiProductAttribute.asiOptionName}"/>: </td>
		<td style="width:100%;padding-left:5px;"><c:out value="${asiProductAttribute.asiOptionValue}" escapeXml="false"/></td>
		</tr>
	  </c:forEach>
	  <%@ include file="/WEB-INF/jsp/frontend/common/customLinesInvoice.jsp"%>
    </table>
	<c:if test="${lineItem.subscriptionInterval != null}">
	  <c:set var="intervalType" value="${fn:substring(lineItem.subscriptionInterval, 0, 1)}"/>
	  <c:set var="intervalUnit" value="${fn:substring(lineItem.subscriptionInterval, 1, 2)}"/>
	  <div class="invoice_lineitem_subscription">
		<fmt:message key="subscription"/> <fmt:message key="details"/> : <fmt:message key="delivery"/>
		<c:choose>
		  <c:when test="${intervalUnit == '1'}"><fmt:message key="every_${intervalType}"><fmt:param value="${intervalUnit}"/></fmt:message></c:when>
		  <c:otherwise><fmt:message key="every_${intervalType}s"><fmt:param value="${intervalUnit}"/></fmt:message></c:otherwise>
		</c:choose>
	  </div>
	</c:if>
  <c:if test="${siteConfig['SHOW_IMAGE_ON_INVOICE'].value == 'true' and lineItem.hasProductAttributesImage}" >
  <c:forEach items="${lineItem.productAttributes}" var="productAttribute">
    <c:if test="${!empty productAttribute.imageUrl}" > 
      <c:if test="${!cartItem.product.thumbnail.absolute}"><img src="<c:url value="/assets/Image/Product/options/${productAttribute.imageUrl}"/>" border="0" class="invoiceOptionImage" /></c:if>
      <c:if test="${cartItem.product.thumbnail.absolute}"><img src="<c:url value="${productAttribute.imageUrl}"/>" border="0" class="invoiceOptionImage" /></c:if>
    </c:if>
  </c:forEach>
  </c:if> 
  </td>  
  <c:forEach items="${orderForm.productFieldsHeader}" var="pFHeader" varStatus="status"><c:set var="check" value="0"/>
  <c:if test="${pFHeader.showOnInvoice}">
  <c:forEach items="${lineItem.productFields}" var="productField"> 
   <c:if test="${pFHeader.id == productField.id}">
    <c:choose>
		<c:when test="${productField.formatType != null and productField.formatType != '' and productField.fieldType == 'number' }">
			<td class="details_field_value_row${row.index % 2} invoice"><fmt:formatNumber value="${wj:trimAndSplit(productField.value, '|' , false, ',', true, true)}" pattern="${productField.formatType }" /></td><c:set var="check" value="1"/>
		</c:when>
		<c:otherwise>
			<td class="details_field_value_row${row.index % 2} invoice"><c:out value="${wj:trimAndSplit(productField.value, '|' , false, ',', true, true)}" escapeXml="false"/></td><c:set var="check" value="1"/>
		</c:otherwise>
	</c:choose>
   </c:if>       
  </c:forEach>
  <c:if test="${check == 0}">
    <td class="invoice">&nbsp;</td>
   </c:if>
  </c:if>
  </c:forEach> 
  <td class="invoice" align="center">
    <c:choose>
        <c:when test="${gSiteConfig['gVARIABLE_PRICE'] and lineItem.product.priceByCustomer}"></c:when>
        <c:otherwise><c:out value="${lineItem.quantity}"/></c:otherwise>
    </c:choose>       
  </td>
  <c:if test="${gSiteConfig['gINVENTORY'] && orderForm.order.hasLowInventoryMessage}">
	<td class="invoice" align="center"><c:out value="${lineItem.lowInventoryMessage}" /></td>
  </c:if>
  <c:choose>
	<c:when test="${siteConfig['CASE_CONTENT_LAYOUT'].value == '1'}">
		<c:choose>
	  		<c:when test="${lineItem.product.caseContent != null}">
	  	 		 <td class="invoice" align="right"><fmt:formatNumber value="${lineItem.unitPrice * lineItem.product.caseContent}" pattern="#,##0.00#" maxFractionDigits="${siteConfig['MAX_FRACTION_DIGIT'].value}"/></td>
	 	 	</c:when>
	 	 	<c:otherwise>
	 	 		<td class="invoice" align="right"><fmt:formatNumber value="${lineItem.unitPrice}" pattern="#,##0.00#" maxFractionDigits="${siteConfig['MAX_FRACTION_DIGIT'].value}"/></td>
	 	 	</c:otherwise>
	  	</c:choose>
	 </c:when>
	 <c:when test="${siteConfig['CASE_CONTENT_LAYOUT'].value == '2'}">
    	<td class="invoice" align="right"><fmt:formatNumber value="${lineItem.unitPrice}" pattern="#,##0.00#" maxFractionDigits="${siteConfig['MAX_FRACTION_DIGIT'].value}"/></td>
     </c:when>
	 <c:otherwise>
	 	<c:if test="${orderForm.order.hasPacking}">
		  <td class="invoice" align="center"><c:out value="${lineItem.product.packing}" /></td>
		  </c:if>
		  <c:if test="${orderForm.order.hasContent}">
		    <td class="invoice" align="center"><c:out value="${lineItem.product.caseContent}" /></td>
		  </c:if>
		  <td class="invoice" align="right"><fmt:formatNumber value="${lineItem.unitPrice}" pattern="#,##0.00#" maxFractionDigits="${siteConfig['MAX_FRACTION_DIGIT'].value}"/></td> 	
	 </c:otherwise>
  </c:choose>
  <c:if test="${orderForm.order.hasParentDeal}">
  <c:choose>
	<c:when test="${siteConfig['CASE_CONTENT_LAYOUT'].value == '1'}">
		<c:choose>
	  		<c:when test="${lineItem.product.caseContent != null}">
	  	 		 <td class="invoice" align="right"><fmt:formatNumber value="${lineItem.parentSkuOriginalPrice * lineItem.product.caseContent}" pattern="#,##0.00#" maxFractionDigits="${siteConfig['MAX_FRACTION_DIGIT'].value}"/></td>
	 	 	</c:when>
	 	 	<c:otherwise>
	 	 		<td class="invoice" align="right"><fmt:formatNumber value="${lineItem.parentSkuOriginalPrice}" pattern="#,##0.00#" maxFractionDigits="${siteConfig['MAX_FRACTION_DIGIT'].value}"/></td>
	 	 	</c:otherwise>
	  	</c:choose>
	 </c:when>
	 <c:when test="${siteConfig['CASE_CONTENT_LAYOUT'].value == '2'}">
    	<td class="invoice" align="right"><fmt:formatNumber value="${lineItem.parentSkuOriginalPrice}" pattern="#,##0.00#" maxFractionDigits="${siteConfig['MAX_FRACTION_DIGIT'].value}"/></td>
     </c:when>
	 <c:otherwise>
		  <td class="invoice" align="right"><fmt:formatNumber value="${lineItem.parentSkuOriginalPrice}" pattern="#,##0.00#" maxFractionDigits="${siteConfig['MAX_FRACTION_DIGIT'].value}"/></td> 	
	 </c:otherwise>
  </c:choose>
  <c:choose>
	<c:when test="${siteConfig['CASE_CONTENT_LAYOUT'].value == '1'}">
		<c:choose>
	  		<c:when test="${lineItem.product.caseContent != null}">
	  	 		 <td class="invoice" align="right"><fmt:formatNumber value="${lineItem.parentSkuDiscount * lineItem.product.caseContent}" pattern="#,##0.00#" maxFractionDigits="${siteConfig['MAX_FRACTION_DIGIT'].value}"/></td>
	 	 	</c:when>
	 	 	<c:otherwise>
	 	 		<td class="invoice" align="right"><fmt:formatNumber value="${lineItem.parentSkuDiscount}" pattern="#,##0.00#" maxFractionDigits="${siteConfig['MAX_FRACTION_DIGIT'].value}"/></td>
	 	 	</c:otherwise>
	  	</c:choose>
	 </c:when>
	 <c:when test="${siteConfig['CASE_CONTENT_LAYOUT'].value == '2'}">
    	<td class="invoice" align="right"><fmt:formatNumber value="${lineItem.parentSkuDiscount}" pattern="#,##0.00#" maxFractionDigits="${siteConfig['MAX_FRACTION_DIGIT'].value}"/></td>
     </c:when>
	 <c:otherwise>
		  <td class="invoice" align="right"><fmt:formatNumber value="${lineItem.parentSkuDiscount}" pattern="#,##0.00#" maxFractionDigits="${siteConfig['MAX_FRACTION_DIGIT'].value}"/></td> 	
	 </c:otherwise>
  </c:choose>
  </c:if>
  <c:if test="${orderForm.order.lineItemPromos != null and fn:length(orderForm.order.lineItemPromos) gt 0}">
     <c:choose>
       <c:when test="${lineItem.promo.percent}">
         <td class="discount" align="right"><c:if test="${lineItem.promo != null}"><c:out value="${lineItem.promo.title}" /> : <fmt:formatNumber value="${(lineItem.totalPrice * lineItem.promo.discount) / 100}" pattern="#,##0.00"/></c:if></td>
       </c:when>
       <c:otherwise>
         <td class="discount" align="right"><c:if test="${lineItem.promo != null}"><c:out value="${lineItem.promo.title}" /> : <fmt:formatNumber value="${lineItem.promo.discount}" pattern="#,##0.00"/></c:if></td>
       </c:otherwise>
     </c:choose>
  </c:if>
  <td class="invoice" align="right"><fmt:formatNumber value="${lineItem.totalPrice}" pattern="#,##0.00"/></td>
</tr>
<div>
  <c:if test="${lineItem.itemGroup != null and (nextItemGroup == null or nextItemGroup != lineItem.itemGroup)}">
    <c:set var="showQtyAndPrice" value="true"/>
    <%@ include file="/WEB-INF/jsp/frontend/checkout/subPreliminary.jsp" %>
    <c:set var="showQtyAndPrice" value=""/>
  </c:if>
</div>
</c:forEach>
  <tr bgcolor="#BBBBBB">
	<td colspan="${6+cols}">&nbsp;</td>
  </tr>
  <tr>
    <td class="invoice" colspan="${5+cols}" align="right"><fmt:message key="subTotal" />:</td>
    <td class="invoice" align="right"><fmt:formatNumber value="${orderForm.order.subTotal}" pattern="#,##0.00"/></td>
  </tr>
  <c:if test="${orderForm.order.promo != null and orderForm.order.promo.discountType eq 'order'}">
    <tr>
      <td class="discount" colspan="${5+cols}" align="right">
      <c:choose>
        <c:when test="${orderForm.order.promoAmount == 0}"><fmt:message key="promoCode" /></c:when>
        <c:otherwise><fmt:message key="discountForPromoCode" /></c:otherwise>
      </c:choose>
      <c:out value="${orderForm.order.promo.title}" />
      <c:choose>
          <c:when test="${orderForm.order.promoAmount == 0}"></c:when>
          <c:when test="${orderForm.order.promo.percent}">
            (<fmt:formatNumber value="${orderForm.order.promo.discount}" pattern="#,##0.00"/>%)
          </c:when>
          <c:otherwise>
            (<fmt:message key="${siteConfig['CURRENCY'].value}" /><fmt:formatNumber value="${orderForm.order.promo.discount}" pattern="#,##0.00"/>)
          </c:otherwise>
      	</c:choose>
      </td>
      <td class="discount" align="right">
      <c:choose>
        <c:when test="${orderForm.order.promoAmount == 0}">&nbsp;</c:when>
        <c:otherwise>(<fmt:message key="${siteConfig['CURRENCY'].value}" /><fmt:formatNumber value="${orderForm.order.promoAmount}" pattern="#,##0.00"/>)</c:otherwise>
      </c:choose>
      </td>
    </tr>  
  </c:if>
  <c:if test="${orderForm.order.lineItemPromos != null and fn:length(orderForm.order.lineItemPromos) gt 0}">
    <c:forEach items="${orderForm.order.lineItemPromos}" var="itemPromo" varStatus="status">
    <tr>
      <td class="discount" colspan="${5+cols}" align="right">
      <c:choose>
        <c:when test="${itemPromo.value == 0}"><fmt:message key="promoCode" /></c:when>
        <c:otherwise><fmt:message key="discountForPromoCode" /></c:otherwise>
      </c:choose>
      <c:out value="${itemPromo.key}" />
      </td>
      <td class="discount" align="right">
      <c:choose>
        <c:when test="${itemPromo.value == 0}">&nbsp;</c:when>
        <c:otherwise>(<fmt:message key="${siteConfig['CURRENCY'].value}" /><fmt:formatNumber value="${itemPromo.value}" pattern="#,##0.00"/>)</c:otherwise>
      </c:choose>
      </td>
    </tr>
    </c:forEach>  
  </c:if>
  <c:choose>
    <c:when test="${orderForm.order.taxOnShipping}">
      <tr>
    	<td class="invoice" colspan="${5+cols}" align="right"><fmt:message key="shippingHandling" /> (<c:out value="${orderForm.order.shippingMethod}" escapeXml="false"/>):</td>
    	<td class="invoice" align="right"><fmt:formatNumber value="${orderForm.order.shippingCost}" pattern="#,##0.00"/></td>
  	  </tr>
  	  <tr>
    	<td class="invoice" colspan="${5+cols}" align="right"><fmt:message key="tax" /></td>
    	<td class="invoice" align="right"><fmt:formatNumber value="${orderForm.order.tax}" pattern="#,##0.00"/></td>
  	  </tr>
  	</c:when>
    <c:otherwise>
      <tr>
    	<td class="invoice" colspan="${5+cols}" align="right"><fmt:message key="tax" /></td>
    	<td class="invoice" align="right"><fmt:formatNumber value="${orderForm.order.tax}" pattern="#,##0.00"/></td>
  	  </tr>
  	  <tr>
    	<td class="invoice" colspan="${5+cols}" align="right"><fmt:message key="shippingHandling" /> (<c:out value="${orderForm.order.shippingMethod}" escapeXml="false"/>):</td>
    	<td class="invoice" align="right"><fmt:formatNumber value="${orderForm.order.shippingCost}" pattern="#,##0.00"/></td>
  	  </tr> 
    </c:otherwise>
  </c:choose>
  <c:if test="${orderForm.order.hasCustomShipping}">
    <td class="invoice" colspan="${5+cols}" align="right"><fmt:message key="customShipping" /> (<c:out value="${orderForm.order.customShippingTitle}" escapeXml="false"/>):</td>
    <td class="invoice" align="right"><fmt:formatNumber value="${orderForm.order.customShippingCost}" pattern="#,##0.00"/></td>
  </c:if>
  <c:if test="${orderForm.order.promo != null and orderForm.order.promo.discountType eq 'shipping'}">
    <tr>
      <td class="discount" colspan="${5+cols}" align="right">
      <c:choose>
        <c:when test="${orderForm.order.promoAmount == 0}"><fmt:message key="promoCode" /></c:when>
        <c:otherwise><fmt:message key="discountForPromoCode" /></c:otherwise>
      </c:choose>
      <c:out value="${orderForm.order.promo.title}" />
      <c:choose>
          <c:when test="${orderForm.order.promoAmount == 0}"></c:when>
          <c:when test="${orderForm.order.promo.percent}">
            (<fmt:formatNumber value="${orderForm.order.promo.discount}" pattern="#,##0.00"/>%)
          </c:when>
          <c:otherwise>
            (<fmt:message key="${siteConfig['CURRENCY'].value}" /><fmt:formatNumber value="${orderForm.order.promo.discount}" pattern="#,##0.00"/>)
          </c:otherwise>
      	</c:choose>
      </td>
      <td class="discount" align="right">
      <c:choose>
        <c:when test="${orderForm.order.promoAmount == 0}">&nbsp;</c:when>
        <c:otherwise>(<fmt:message key="${siteConfig['CURRENCY'].value}" /><fmt:formatNumber value="${orderForm.order.promoAmount}" pattern="#,##0.00"/>)</c:otherwise>
      </c:choose>
      </td>
    </tr>  
  </c:if>
  <c:if test="${orderForm.order.ccFee != null and orderForm.order.ccFee > 0}">
    <td class="invoice" colspan="${5+cols}" align="right"><fmt:message key="creditCardFee" />:</td>
    <td class="invoice" align="right"><fmt:formatNumber value="${orderForm.order.ccFee}" pattern="#,##0.00"/></td>
  </c:if>
  <c:if test="${(gSiteConfig['gGIFTCARD'] or gSiteConfig['gVIRTUAL_BANK_ACCOUNT']) and orderForm.order.creditUsed != null}">
  <tr>
    <td class="discount" colspan="${5+cols}" align="right"><fmt:message key="credit" />:</td>
    <td class="discount" align="right"><fmt:formatNumber value="${orderForm.order.creditUsed}" pattern="#,##0.00"/></td>
  </tr>
  </c:if>
  <c:if test="${siteConfig['BUY_SAFE_URL'].value != ''}">
  <tr>
    <td class="invoice" colspan="${5+cols}" align="right"><fmt:message key="optionalBuySafe3In1Guarntee" /> <a href="viewCart.jhtm">( change )</a>:</td>
    <td class="invoice" align="right">
      <c:choose>
        <c:when test="${orderForm.order.wantsBond}"><fmt:formatNumber value="${orderForm.order.bondCost}" pattern="#,##0.00"/></c:when>
        <c:otherwise><fmt:formatNumber value="0.00"/></c:otherwise>
      </c:choose>
    </td>
  </tr>
  </c:if>
  <tr class="grandTotalRow">
    <td class="invoice" colspan="${5+cols}" align="right"><fmt:message key="grandTotal" />:</td>
    <td class="grandTotal" align="right"><fmt:message key="${siteConfig['CURRENCY'].value}" /><fmt:formatNumber value="${orderForm.order.grandTotal}" pattern="#,##0.00"/></td>
  </tr>
</table>

</div>
<div class="cartFooterTitle reviewOrder"></div>

<div class="messageWrapper"><c:out value="${siteConfig['INVOICE_MESSAGE'].value}" escapeXml="false"/></div>


<c:if test="${ gSiteConfig['gGIFTCARD'] or gSiteConfig['gVIRTUAL_BANK_ACCOUNT'] }">
<div class="creditWrapper">
<div class="creditHd"><fmt:message key="f_credit" /></div>
<c:choose>
 <c:when test="${orderForm.customerCredit > 0}">
   <c:choose>
    <c:when test="${orderForm.customerCredit > orderForm.order.grandTotal}">
      <form:radiobutton value="vba" path="order.paymentMethod"  onclick="togglePayment()" />
      <fmt:message key="applyOfMyCreditToThisOrder">
	      <c:choose>
	  		<c:when test="${siteConfig['CURRENCY'].value == 'EUR'}">
	  			<fmt:param value="&#8364;" /> 
	  		</c:when>
	  		<c:when test="${siteConfig['CURRENCY'].value == 'GBP'}">
	  			<fmt:param value="&#163;"/> 
	  		</c:when>
	  		<c:when test="${siteConfig['CURRENCY'].value == 'JPY'}">
	  			<fmt:param value="&#165;"/> 
	  		</c:when>
	  		<c:when test="${siteConfig['CURRENCY'].value == 'CAD'}">
	  			<fmt:param value="CAD&#36;"/> 
	  		</c:when>
	  		<c:otherwise>
	  		    <fmt:param value="&#36;" />
	  		</c:otherwise>
		  </c:choose><fmt:param value="${orderForm.customerCredit}"/>
	   </fmt:message>
    </c:when>
    <c:otherwise>
      <form:checkbox path="applyUserPoint" value="true"  />
      <fmt:message key="applyOfMyCreditToThisOrder">
	      <c:choose>
	  		<c:when test="${siteConfig['CURRENCY'].value == 'EUR'}">
	  			<fmt:param value="&#8364;" /> 
	  		</c:when>
	  		<c:when test="${siteConfig['CURRENCY'].value == 'GBP'}">
	  			<fmt:param value="&#163;"/> 
	  		</c:when>
	  		<c:when test="${siteConfig['CURRENCY'].value == 'JPY'}">
	  			<fmt:param value="&#165;"/> 
	  		</c:when>
	  		<c:when test="${siteConfig['CURRENCY'].value == 'CAD'}">
	  			<fmt:param value="CAD&#36;"/> 
	  		</c:when>
	  		<c:otherwise>
	  		    <fmt:param value="&#36;" />
	  		</c:otherwise>
		  </c:choose><fmt:param value="${orderForm.customerCredit}"/>
	   </fmt:message>
    </c:otherwise>
   </c:choose>
 </c:when>
</c:choose>
</div>
<c:if test="${gSiteConfig['gGIFTCARD']}">
  <div class="giftCardContainer">
	<div id="giftCardMesage">
	  <fmt:message key="enterNewGiftCards" /><br />
	  <fmt:message key="IfYouHaveMoreThanOneCodeEnterItBelowAndClickTheApplyButtonToUpdateThisPage" />
	</div>
	<div class="applyGiftCardCode">
	  <b><fmt:message key="f_giftCardHeader"/>:</b>
	  <br/>
	  <form:input path="tempGiftCardCode" size="20"/>
	</div>
	<div id="giftCardButton">
	  <input type="image" border="0" id="__claimCode" name="_redeem" onclick="return checkCode();" src="assets/Image/Layout/button_apply.gif" />
	</div>  
  </div>
</c:if>
</c:if>

<script language="JavaScript">
<!--
function togglePayment() {
<c:if test="${gSiteConfig['gPAYPAL'] < 2 and siteConfig['CREDIT_CARD_PAYMENT'].value != ''}">
  if (document.getElementById('cc_radio').checked) {
    document.getElementById('creditcardbox').style.display="block";
    document.getElementById('ccNum').disabled=false;    
  } else {	
    document.getElementById('creditcardbox').style.display="none";
    document.getElementById('ccNum').disabled=true;    
  }  
</c:if>
<c:if test="${gemoneybox}">
  if (document.getElementById('ge_radio').checked) {
    document.getElementById('gemoneybox').style.display="block";
  } else {	
    document.getElementById('gemoneybox').style.display="none";
  }  
</c:if>
<c:if test="${googlebox}">
  if (document.getElementById('google_radio').checked) {
    document.getElementById('creditcardbox').style.display="none";
    document.getElementById('ccNum').disabled=true;
  }
</c:if>
<c:if test="${ebillmebox}">
if (document.getElementById('ebillme_radio').checked) {
  document.getElementById('ebillmebox').style.display="block";
} else {
  document.getElementById('ebillmebox').style.display="none";
}
</c:if>
}
//-->
</script>

<c:if test="${gSiteConfig['gSALES_PROMOTIONS']}">
<div class="promoWrapper" id="promoWrapperId"> 
    <c:if test="${gSiteConfig['gSHOW_ELIGIBLE_PROMOS']}">
    <div id="eligiblePromoContainer">
	  <b><fmt:message key="eligiblePromos" />:</b>
	    <select name="eligiblePromo" onchange="showPromo(this.form.eligiblePromo)" id="eligiblePromo">
	    <option value=""><c:out value="Select Promo"/></option>
		<c:forEach items="${eligiblePromos}" var="promo" varStatus="status">
		  <option value='${promo.promoId}'><c:out value="${promo.title}"/></option>
	    </c:forEach>
	    </select>
	 </div>   
	 </c:if>  
	 <div id="promoContainer">
	    <div id="promoInput"> 
	      <b><fmt:message key="f_enterPromoCode"/>:</b>
	      <br />
	      <form:input path="order.promoCode" htmlEscape="true"/>
	    </div>
	    <div id="promoButton">
	      <input type="image" src="assets/Image/Layout/button_apply_promocode${_lang}.gif" name="_target2" onclick="return loadPromoCode(this.form)" />
	    </div>
	    <c:if test="${orderForm.tempPromoErrorMessage != null}"><span class="error"><fmt:message key="${orderForm.tempPromoErrorMessage}"/></span></c:if>
	 </div>
	 <c:if test="${gSiteConfig['gSHOW_ELIGIBLE_PROMOS']}">   
	    <c:forEach items="${eligiblePromos}" var="promo" varStatus="status">
		  <div class="eligiblePromoBox" id="promo<c:out value="${promo.promoId}"/>" style="display:none;"><c:out value="${promo.htmlCode}" escapeXml="false"/></div>
	    </c:forEach>
	 </c:if> 
</div>
</c:if>


<div id="before_purchaseButton"></div>

<div id="purchaseButton" class="nbButton" style="margin:5px;">
<input type="image" src="assets/Image/Layout/button_purchase${_lang}.gif" id="button_purchase" name="_finish" onclick="return checkPO('<c:out value='${orderForm.poRequired}'/>');">
</div>

<div class="invoiceNote">
  <b><c:out value="${siteConfig['SPECIAL_INSTRUCTIONS'].value}"/></b>
  <br/>  	
  <form:textarea path="order.invoiceNote"  rows="3" cols="40" htmlEscape="true" cssClass="textfield" />
</div>

	
<div class="purchaseOrder">
  <b><fmt:message key="shoppingcart.purchaseOrder" /></b> 
  <br/> 
  <form:input path="order.purchaseOrder" maxlength="50" htmlEscape="true"/>
  <form:errors path="order.purchaseOrder" cssClass="error" />
</div>

<c:if test="${gSiteConfig['gORDER_FILEUPLOAD'] > 0}">
<div class="attachment">
<table align="center">
<c:forEach items="${attachedFiles}" var="file" varStatus="status">
  <c:if test="${status.first}">
  <tr>
    <td align="center">
	  <b>Attached File(s):</b>
	</td>
  </tr>
  </c:if>
  <tr>
    <td align="center">
      <c:out value="${file['file'].name}"/> 
      <c:choose>
        <c:when test="${file['size'] > (1024*1024)}">
    	  (<fmt:formatNumber value="${file['size']/1024/1024}" pattern="#,##0.0"/> MB)
    	</c:when>
        <c:when test="${file['size'] > 1024}">
    	  (<fmt:formatNumber value="${file['size']/1024}" pattern="#,##0"/> KB)
    	</c:when>
    	<c:otherwise>
    	  (1 KB)
    	</c:otherwise>
      </c:choose>
    </td>
  </tr>
</c:forEach>
  <tr>
    <td align="center"><input type="image" border="0"
    	src="<c:choose><c:when test="${attachedFiles != null}">assets/Image/Layout/button_edit_attachedFiles${_lang}.gif</c:when><c:otherwise>assets/Image/Layout/button_attachFiles.gif</c:otherwise></c:choose>" name="_target3"/></td>
  </tr>

  <tr>
    <td>&nbsp;</td>
  </tr>  
</table>
</div>
</c:if>	
</form:form>
</div>

</div>


<c:out value="${model.cardinalCentinelLayout.footerHtml}" escapeXml="false"/>
<c:out value="${invoiceLayout.footerHtml}" escapeXml="false"/>

<iframe src="${_contextpath}/sessionKeepAlive.jsp" width="0" height="0" frameborder="0"></iframe>

  </tiles:putAttribute>
</tiles:insertDefinition>