<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="/WEB-INF/tlds/spring.tld" prefix="spring" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>

<tiles:insertDefinition name="${_template}" flush="true">
  <tiles:putAttribute name="content" type="string">
  
<c:out value="${model.billingShippingLayout.headerHtml}" escapeXml="false"/>  

<div style="margin:auto;" class="billingShippingWrapper">
<div class="returningCustomerBox">
<form action="billingShipping.jhtm" method="POST">
<div class="headerTitle returningCustomer"><p><fmt:message key="returningCustomer"/></p></div>
<div class="loginInfoBox">
<div class="headerLoginTitle login"><p><fmt:message key="login"/></p></div>
<div class="loginInfoTable">
<div class="note"><fmt:message key="f_pleaseEnterYourEmailAddressAndPasswordBelow"/></div>
<c:if test="${!empty message}">
  <div class="error"><b><font color="RED"><fmt:message key="${message}"><fmt:param value="${siteConfig['IP_ADDRESS_ERROR_MESSAGE'].value}"/></fmt:message></font></b></div>
</c:if>
<div class="username"><fmt:message key="emailAddress"/><input type="text" name="username" value="${param.username}" /></div>
<div class="password"><fmt:message key="password"/><input type="password" name="password" value="" /></div>
<div class="forgetPass"><a href="forgetPassword.jhtm" class="breadcrumb_home"><fmt:message key="forgotYourPassword?"/></a></div>
<div class="submit"><input type="submit" value="<fmt:message key="logIn"/>" name="_login" /></div>
</div>
<div class="footerLoginTitle login"></div>
</div>
</form>
</div>

<c:if test="${!(gSiteConfig['gREGISTRATION_DISABLED'] or siteConfig['PROTECTED_HOST'].value == header['host'])}">

<form:form commandName="customerForm" action="billingShipping.jhtm" method="post">
<div class="headerTitle newCustomer">
<p><fmt:message key="f_checkout1_newCustomer"/></p>
<spring:hasBindErrors name="customerForm">
<p class="fixAllError"><span class="error">Please fix all errors!</span></p>
</spring:hasBindErrors>
</div>

<div class="billingInfoBox">
<div class="headerTitle billingInfo"><p><fmt:message key="billingInformation"/></p></div>
<div class="billingInfoTable">
<table class="billingInfo" width="100%" border="0" cellpadding="3" cellspacing="1">
  <tr>
    <td class="fieldName"><span class="requiredAsterisk">*</span><fmt:message key="firstName" /></td>
    <td>
	  <form:input path="customer.address.firstName" htmlEscape="true"/>
	  <form:errors path="customer.address.firstName" cssClass="error" />    
    </td>
  </tr>
  <tr>
    <td class="fieldName"><span class="requiredAsterisk">*</span><fmt:message key="lastName" /></td>
    <td>
	  <form:input path="customer.address.lastName" htmlEscape="true"/>
	  <form:errors path="customer.address.lastName" cssClass="error" />    
    </td>
  </tr>
  <tr>
    <td class="fieldName"><span class="requiredAsterisk">*</span><fmt:message key='country' /></td>
    <td>
      <form:select id="country" path="customer.address.country" onchange="toggleStateProvince(this)" cssStyle="width: 150px">
       <form:option value="" label="Please Select"/>
      <form:options items="${model.countrylist}" itemValue="code" itemLabel="name"/>
      </form:select>
      <form:errors path="customer.address.country" cssClass="error" />      
    </td>
  </tr>
  <tr>
    <td class="fieldName"><c:if test="${siteConfig['CUSTOMERS_REQUIRED_COMPANY'].value == 'true'}"><span class="requiredAsterisk">*</span></c:if><fmt:message key="company" /></td>
    <td>
	  <form:input path="customer.address.company" htmlEscape="true"/>
	  <form:errors path="customer.address.company" cssClass="error" />    
    </td>
  </tr>
  <tr>
    <td class="fieldName"><span class="requiredAsterisk">*</span><fmt:message key="address" /> 1</td>
    <td>
	  <form:input path="customer.address.addr1" htmlEscape="true"/>
	  <form:errors path="customer.address.addr1" cssClass="error" />    
    </td>
  </tr>
  <tr>
    <td class="fieldName"><fmt:message key="address" /> 2</td>
    <td>
	  <form:input path="customer.address.addr2" htmlEscape="true"/>
	  <form:errors path="customer.address.addr2" cssClass="error" />    
    </td>
  </tr>
  <tr>
    <td class="fieldName"><span class="requiredAsterisk">*</span><fmt:message key="city" /></td>
    <td>
	  <form:input path="customer.address.city" htmlEscape="true"/>
	  <form:errors path="customer.address.city" cssClass="error" />    
    </td>
  </tr>
  <tr> 
    <td class="fieldName"><span class="requiredAsterisk">*</span><fmt:message key="stateProvince" /></td>
    <td>
         <form:select id="state" path="customer.address.stateProvince" disabled="${customerForm.customer.address.country != 'US'}" cssStyle="width:150px">
           <form:option value="" label="Please Select"/>
           <form:options items="${model.statelist}" itemValue="code" itemLabel="name"/>
         </form:select>
         <form:select id="ca_province" path="customer.address.stateProvince" disabled="${customerForm.customer.address.country != 'CA'}" cssStyle="width:150px">
           <form:option value="" label="Please Select"/>
           <form:options items="${model.caProvinceList}" itemValue="code" itemLabel="name"/>
         </form:select>
         <form:input id="province" path="customer.address.stateProvince" htmlEscape="true" disabled="${customerForm.customer.address.country == 'US' or customerForm.customer.address.country == 'CA' or customerForm.customer.address.country == '' or customerForm.customer.address.country == null}"/>
         <form:errors path="customer.address.stateProvince" cssClass="error" />  
    </td>
  </tr>
  <tr id="stateProvinceNA">
    <td><span>&nbsp;<form:checkbox path="customer.address.stateProvinceNA" id="addressStateProvinceNA" value="true"  disabled="${customerForm.customer.address.country == 'US' or customerForm.customer.address.country == 'CA' or customerForm.customer.address.country == '' or customerForm.customer.address.country == null}"/> Not Applicable</span>
    </td>
  </tr>
  <tr>
    <td class="fieldName"><span class="requiredAsterisk">*</span><fmt:message key="zipCode" /></td>
    <td>
	  <form:input path="customer.address.zip" htmlEscape="true"/>
	  <form:errors path="customer.address.zip" cssClass="error" />    
    </td>
  </tr>
  <c:if test="${siteConfig['ADDRESS_RESIDENTIAL_COMMERCIAL'].value >= 2}">
    <tr>
    <td class="fieldName"><fmt:message key="deliveryType" /></td>
    <td><form:radiobutton path="customer.address.residential" value="true"/>: <fmt:message key="residential" /><br /><form:radiobutton path="customer.address.residential" value="false"/>: <fmt:message key="commercial" /> </td>
    </tr>
    <c:if test="${siteConfig['SHIPPING_LTL'].value == 'true'}">
      <tr>
      <td class="fieldName"><fmt:message key="liftGateDelivery" /></td>
      <td><form:checkbox path="customer.address.liftGate" value="true"/><a href="category.jhtm?cid=307" onclick="window.open(this.href,'','width=600,height=300,resizable=yes'); return false;"><img class="toolTipImg" src="assets/Image/Layout/question.gif" border="0" /></a> </td>
      </tr>
    </c:if>
  </c:if>
  <tr>
    <td class="fieldName"><span class="requiredAsterisk">*</span><fmt:message key="phone" /></td>
    <td>
	  <form:input path="customer.address.phone" htmlEscape="true"/>
	  <form:errors path="customer.address.phone" cssClass="error" />    
    </td>
  </tr>
  <tr class="cellPhone">
    <td class="fieldName"><fmt:message key="cellPhone" />: </td>
    <td>            
      <form:input path="customer.address.cellPhone" htmlEscape="true"/>
	  <form:errors path="customer.address.cellPhone" cssClass="error" />    
    </td>
  </tr>
  <tr class="fax">
    <td class="fieldName"><fmt:message key="fax" /></td>
    <td>
      <form:input path="customer.address.fax" htmlEscape="true"/>
	  <form:errors path="customer.address.fax" cssClass="error" />    
    </td>
  </tr>
  <tr>
    <td class="fieldName"><span class="requiredAsterisk">*</span><fmt:message key="email" /></td>
    <td>
	  <form:input path="customer.username" maxlength="80" htmlEscape="true"/>
	  <form:errors path="customer.username" cssClass="error" />    
    </td>
  </tr>
  <tr>
    <td class="fieldName"><span class="requiredAsterisk">*</span><fmt:message key="password" /></td>
    <td>
	  <form:password path="customer.password" htmlEscape="true"/>
	  <form:errors path="customer.password" cssClass="error" />
    </td>
  </tr>
  <tr>
    <td class="fieldName"><span class="requiredAsterisk">*</span><fmt:message key="confirmPassword" /></td>
    <td>
	  <form:password path="confirmPassword" htmlEscape="true"/>
	  <form:errors path="confirmPassword" cssClass="error" />    
    </td>
  </tr>
  <tr>
    <td>&nbsp;</td>
    <td>
      <span class="error"><fmt:message key="password" /> <fmt:message key="form.charLessMin"><fmt:param value="5"/></fmt:message></span>
    </td>
  </tr>
  <c:if test="${gSiteConfig['gMASS_EMAIL'] or siteConfig['CUSTOMER_MASS_EMAIL'].value == 'true'}">
  	<tr class="subscribeCheckBox" id="subscribeCheckBoxId">  	
      <td colspan="2" align="left"><form:checkbox path="subscribeEmail"  /><span><fmt:message key="f_subscribeMailingList" /></span></td>
  </c:if>
</table>
</div>
<div class="footerTitle billingInfo"></div>
</div>
<div class="shippingInfoBox">
<div class="headerTitle shippingInfo"><p><fmt:message key="shippingInformation"/></p></div>
<div class="sameasbilling" id="sameasbillingId"><form:checkbox path="sameAsBilling" onclick="toggleShipping(this)" disabled="${siteConfig['ADD_ADDRESS_ON_CHECKOUT'].value != 'true'}"/><fmt:message key="sameAsBilling"/></div>
<div class="shippingInfoTable">
<table class="shippingInfo" width="100%" border="0" cellpadding="3" cellspacing="1">
  <tr>
    <td class="fieldName"><span class="requiredAsterisk">*</span><fmt:message key="firstName" /></td>
    <td>
	  <form:input path="shipping.firstName" htmlEscape="true"/>
	  <form:errors path="shipping.firstName" cssClass="error" />    
    </td>
  </tr>
  <tr>
    <td class="fieldName"><span class="requiredAsterisk">*</span><fmt:message key="lastName" /></td>
    <td>
	  <form:input path="shipping.lastName" htmlEscape="true"/>
	  <form:errors path="shipping.lastName" cssClass="error" />    
    </td>
  </tr>
  <tr>
    <td class="fieldName"><span class="requiredAsterisk">*</span><fmt:message key='country' /></td>
    <td>
      <form:select id="shippingCountry" path="shipping.country" onchange="toggleShippingStateProvince(this)" cssStyle="width:150px">
       <form:option value="" label="Please Select"/>
      <form:options items="${model.countrylist}" itemValue="code" itemLabel="name"/>
      </form:select>
      <form:errors path="shipping.country" cssClass="error" />      
    </td>
  </tr>
  <tr>
    <td class="fieldName"><c:if test="${siteConfig['CUSTOMERS_REQUIRED_COMPANY'].value == 'true'}"><span class="requiredAsterisk">*</span></c:if><fmt:message key="company" /></td>
    <td>
	  <form:input path="shipping.company" htmlEscape="true"/>
	  <form:errors path="shipping.company" cssClass="error" />    
    </td>
  </tr>
  <tr>
    <td class="fieldName"><span class="requiredAsterisk">*</span><fmt:message key="address" /> 1</td>
    <td>
	  <form:input path="shipping.addr1" htmlEscape="true"/>
	  <form:errors path="shipping.addr1" cssClass="error" />    
    </td>
  </tr>
  <tr>
    <td class="fieldName"><fmt:message key="address" /> 2</td>
    <td>
	  <form:input path="shipping.addr2" htmlEscape="true"/>
	  <form:errors path="shipping.addr2" cssClass="error" />    
    </td>
  </tr>
  <tr>
    <td class="fieldName"><span class="requiredAsterisk">*</span><fmt:message key="city" /></td>
    <td>
	  <form:input path="shipping.city" htmlEscape="true"/>
	  <form:errors path="shipping.city" cssClass="error" />    
    </td>
  </tr>
  <tr> 
    <td class="fieldName"><span class="requiredAsterisk">*</span><fmt:message key="stateProvince" /></td>
    <td>
         <form:select id="shipping_state" path="shipping.stateProvince" disabled="${customerForm.shipping.country != 'US'}" cssStyle="width:150px">
           <form:option value="" label="Please Select"/>
           <form:options items="${model.statelist}" itemValue="code" itemLabel="name"/>
         </form:select>
         <form:select id="shipping_ca_province" path="shipping.stateProvince" disabled="${customerForm.shipping.country != 'CA'}" cssStyle="width:150px">
           <form:option value="" label="Please Select"/>
           <form:options items="${model.caProvinceList}" itemValue="code" itemLabel="name"/>
         </form:select>
         <form:input id="shipping_province" path="shipping.stateProvince" htmlEscape="true" disabled="${customerForm.shipping.country == 'US' or customerForm.shipping.country == 'CA' or customerForm.shipping.country == '' or customerForm.shipping.country == null}"/>
         <form:errors path="shipping.stateProvince" cssClass="error" />
    </td>
  </tr>
  <tr id="shipping_stateProvinceNA">
    <td colspan="2"><span>&nbsp;<form:checkbox path="shipping.stateProvinceNA" id="shipping_addressStateProvinceNA" value="true"  disabled="${customerForm.shipping.country == 'US' or customerForm.shipping.country == 'CA' or customerForm.shipping.country == '' or customerForm.shipping.country == null}"/> Not Applicable</span></td>
  </tr>
  <tr>
    <td class="fieldName"><span class="requiredAsterisk">*</span><fmt:message key="zipCode" /></td>
    <td>
	  <form:input path="shipping.zip" htmlEscape="true"/>
	  <form:errors path="shipping.zip" cssClass="error" />    
    </td>
  </tr>
  <c:if test="${siteConfig['ADDRESS_RESIDENTIAL_COMMERCIAL'].value >= 2}">
    <tr>
    <td class="fieldName"><fmt:message key="deliveryType" /></td>
    <td><form:radiobutton path="shipping.residential" value="true"/>: <fmt:message key="residential" /><br /><form:radiobutton path="shipping.residential" value="false"/>: <fmt:message key="commercial" /> </td>
    </tr>
    <c:if test="${siteConfig['SHIPPING_LTL'].value == 'true'}">
      <tr>
      <td><span class="requiredAsterisk"></span></td>
      <td class="fieldName"><fmt:message key="liftGateDelivery" /></td>
      <td><form:checkbox path="shipping.liftGate" value="true"/><a href="category.jhtm?cid=307" onclick="window.open(this.href,'','width=600,height=300,resizable=yes'); return false;"><img class="toolTipImg" src="assets/Image/Layout/question.gif" border="0" /></a> </td>
      </tr>
    </c:if>
  </c:if>
  <tr>
    <td class="fieldName"><span class="requiredAsterisk">*</span><fmt:message key="phone" /></td>
    <td>
	  <form:input path="shipping.phone" htmlEscape="true"/>
	  <form:errors path="shipping.phone" cssClass="error" />    
    </td>
  </tr>
  <tr>
    <td class="fieldName"><fmt:message key="cellPhone" />: </td>
    <td>            
      <form:input path="shipping.cellPhone" htmlEscape="true"/>
	  <form:errors path="shipping.cellPhone" cssClass="error" />    
    </td>
  </tr>
  <tr>
    <td class="fieldName"><fmt:message key="fax" /></td>
    <td>
      <form:input path="shipping.fax" htmlEscape="true"/>
	  <form:errors path="shipping.fax" cssClass="error" />    
    </td>
  </tr>
</table>
</div>
<div class="footerTitle shippingInfo"></div>
</div>
<c:if test="${gSiteConfig['gTAX_EXEMPTION']}">
<div class="taxExemptionBox" id="taxExemptionBoxId">
<div class="headerTitle"><fmt:message key="taxInformation"/></div>
<table class="taxInfo" width="100%" border="0" cellpadding="3" cellspacing="1">
  <tr>
    <td class="fieldName"><c:if test="${siteConfig['TAX_ID_REQUIRED'].value == 'true'}"><span class="requiredAsterisk">*</span></c:if><fmt:message key="taxId" /></td>
    <td>
      <form:input path="customer.taxId" maxlength="30" htmlEscape="true"/>
	  <form:errors path="customer.taxId" cssClass="error" />      
    </td>
  </tr>
  <tr>
    <td colspan="2">
      <div style="color:#FF0000"><c:out value="${siteConfig['TAX_ID_NOTE'].value}" /></div>
    </td>
  </tr>
</table>
</div>
</c:if>

<div class="nextStepClass" align="right">
    <input type="image" border="0" name="_next" src="${_contextpath}/assets/Image/Layout/button_next${_lang}.gif" <c:if test="${sessionCustomer.suspended}">onClick="return confirm('Account is suspended. Continue Checkout?')"</c:if>>
</div>
</form:form>

</c:if>
</div>
<c:out value="${model.billingShippingLayout.footerHtml}" escapeXml="false"/>

<script language="JavaScript">
<!--
function toggleStateProvince(el) {
  if (el.value == "US") {
      document.getElementById('state').disabled=false;
      document.getElementById('state').style.display="block";
      document.getElementById('ca_province').disabled=true;
      document.getElementById('ca_province').style.display="none";
      document.getElementById('province').disabled=true;
      document.getElementById('province').style.display="none";
      document.getElementById('addressStateProvinceNA').disabled=true;
      document.getElementById('stateProvinceNA').style.display="none";
  } else if (el.value == "CA") {
      document.getElementById('state').disabled=true;
      document.getElementById('state').style.display="none";
      document.getElementById('ca_province').disabled=false;
      document.getElementById('ca_province').style.display="block";
      document.getElementById('province').disabled=true;
      document.getElementById('province').style.display="none";
      document.getElementById('addressStateProvinceNA').disabled=true;
      document.getElementById('stateProvinceNA').style.display="none";
  } else {
      document.getElementById('state').disabled=true;
      document.getElementById('state').style.display="none";
      document.getElementById('ca_province').disabled=true;
      document.getElementById('ca_province').style.display="none";
      document.getElementById('province').disabled=false;
      document.getElementById('province').style.display="block";
      document.getElementById('addressStateProvinceNA').disabled=false;
      document.getElementById('stateProvinceNA').style.display="block";
  }
}
toggleStateProvince(document.getElementById('country'));

function toggleShippingStateProvince(el) {
  if (el.value == "US") {
      document.getElementById('shipping_state').disabled=false;
      document.getElementById('shipping_state').style.display="block";
      document.getElementById('shipping_ca_province').disabled=true;
      document.getElementById('shipping_ca_province').style.display="none";
      document.getElementById('shipping_province').disabled=true;
      document.getElementById('shipping_province').style.display="none";
      document.getElementById('shipping_addressStateProvinceNA').disabled=true;
      document.getElementById('shipping_stateProvinceNA').style.display="none";
  } else if (el.value == "CA") {
      document.getElementById('shipping_state').disabled=true;
      document.getElementById('shipping_state').style.display="none";
      document.getElementById('shipping_ca_province').disabled=false;
      document.getElementById('shipping_ca_province').style.display="block";
      document.getElementById('shipping_province').disabled=true;
      document.getElementById('shipping_province').style.display="none";
      document.getElementById('shipping_addressStateProvinceNA').disabled=true;
      document.getElementById('shipping_stateProvinceNA').style.display="none";
  } else {
      document.getElementById('shipping_state').disabled=true;
      document.getElementById('shipping_state').style.display="none";
      document.getElementById('shipping_ca_province').disabled=true;
      document.getElementById('shipping_ca_province').style.display="none";
      document.getElementById('shipping_province').disabled=false;
      document.getElementById('shipping_province').style.display="block";
      document.getElementById('shipping_addressStateProvinceNA').disabled=false;
      document.getElementById('shipping_stateProvinceNA').style.display="block";
  }
}
toggleShippingStateProvince(document.getElementById('shippingCountry'));

function toggleShipping(el) {
  if (el.checked) {
      document.getElementById('shipping.firstName').disabled=true;
      document.getElementById('shipping.lastName').disabled=true;
      document.getElementById('shippingCountry').disabled=true;
      document.getElementById('shipping.company').disabled=true;
      document.getElementById('shipping.addr1').disabled=true;
      document.getElementById('shipping.addr2').disabled=true;
      document.getElementById('shipping.city').disabled=true;
      document.getElementById('shipping.zip').disabled=true;
      document.getElementById('shipping.phone').disabled=true;
      document.getElementById('shipping.cellPhone').disabled=true;
      document.getElementById('shipping.fax').disabled=true;
	  document.getElementById('shipping_state').disabled=true;
      document.getElementById('shipping_ca_province').disabled=true;
      document.getElementById('shipping_province').disabled=true;
      document.getElementById('shipping_addressStateProvinceNA').disabled=true;
      <c:if test="${siteConfig['ADDRESS_RESIDENTIAL_COMMERCIAL'].value >= 2}">
      document.getElementById('shipping.residential1').disabled=true;
      document.getElementById('shipping.residential2').disabled=true;
      </c:if>
  } else {
      document.getElementById('shipping.firstName').disabled=false;
      document.getElementById('shipping.lastName').disabled=false;
      document.getElementById('shippingCountry').disabled=false;
      document.getElementById('shipping.company').disabled=false;
      document.getElementById('shipping.addr1').disabled=false;
      document.getElementById('shipping.addr2').disabled=false;
      document.getElementById('shipping.city').disabled=false;
      document.getElementById('shipping.zip').disabled=false;
      document.getElementById('shipping.phone').disabled=false;
      document.getElementById('shipping.cellPhone').disabled=false;
      document.getElementById('shipping.fax').disabled=false;
	  document.getElementById('shipping_state').disabled=false;
      document.getElementById('shipping_ca_province').disabled=false;
      document.getElementById('shipping_province').disabled=false;
      document.getElementById('shipping_addressStateProvinceNA').disabled=false;	
      <c:if test="${siteConfig['ADDRESS_RESIDENTIAL_COMMERCIAL'].value >= 2}">
      document.getElementById('shipping.residential1').disabled=false;
      document.getElementById('shipping.residential2').disabled=false;
      </c:if>
      toggleShippingStateProvince(document.getElementById('shippingCountry'));
  }
}
toggleShipping(document.getElementById('sameAsBilling1'));

//-->
</script>

<iframe src="${_contextpath}/sessionKeepAlive.jsp" width="0" height="0" frameborder="0"></iframe>

  </tiles:putAttribute>
</tiles:insertDefinition>
