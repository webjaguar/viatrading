<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<script type="text/javascript" src="javascript/mootools-1.2.5-core.js"></script>

<tiles:insertDefinition name="${_template}" flush="true">
  <tiles:putAttribute name="content" type="string">
<c:if test="${gSiteConfig['gPRODUCT_REVIEW']}">
<c:out value="${model.productReviewLayout.headerHtml}" escapeXml="false"/>

<c:if test="${model.product != null}">
<div class="product">
 <div class="productimg">
  <c:forEach items="${model.product.images}" var="image" varStatus="status">
	<c:if test="${status.first}">
		 <a href="product.jhtm?id=${model.product.id}" ><img width="150" src="<c:if test="${!image.absolute}">assets/Image/Product/detailsbig/</c:if><c:out value="${image.imageUrl}"/>" border="0" id="_image" name="_image" class="review_image"
		  alt="<c:out value="${product.name}"/><c:if test="${product.shortDesc != ''}"> - <c:out value="${product.shortDesc}"/></c:if>"></a>
	</c:if>	 	
  </c:forEach>	
 </div>

</div>
</c:if>
<c:if test="${model.product.enableRate}">
<form:form commandName="productReviewForm" method="post">
<input type="hidden" name="_page" value="0">

<table style="productReviewForm">
 <tr>
  <td align="left">1.</td>
  <td width="10" align="left"></td>
  <td><span class="fieldtitle"><fmt:message key="displayName"/></span><br />
  <form:input path="productReview.userName"/>
  <form:errors path="productReview.userName" cssClass="error" />
  </td>
 </tr>
 <tr>
  <td align="left">2.</td>
  <td width="10" align="left"></td>
  <td><span class="fieldtitle"><fmt:message key="overalRating"/></span><br />
  <form:input path="productReview.rate"/>
  <form:errors path="productReview.rate" cssClass="error" />
  <ul id="rate100" class="rating nostar">
  <li id="1" class="rate one"><span title="1 Star (Poor)"></span></li>
  <li id="2" class="rate two"><span title="2 Stars (Fair)"></span></li>
  <li id="3" class="rate three"><span title="3 Stars (Average)"></span></li>
  <li id="4" class="rate four"><span title="4 Stars (Good)"></span></li>
  <li id="5" class="rate five"><span title="5 Stars (Excellent)"></span></li>
  </ul>
  </td>
 </tr>
 <tr>
  <td align="left">3.</td>
  <td width="10" align="left"></td>
  <td><span class="fieldtitle"><fmt:message key="pleaseEnterATitleForYourReview"/></span><br />
  <form:input path="productReview.title"/>
  <form:errors path="productReview.title" cssClass="error" />
  </td>
 </tr>
 <tr>
  <td align="left">4.</td>
  <td width="10" align="left"></td>
  <td><span class="fieldtitle"><fmt:message key="pleaseEnterYourReviewInTheSpaceBelow"/>:</span><br />
  <form:textarea path="productReview.text" cssClass="reviewTextarea"/>
  <form:errors path="productReview.text" cssClass="error" />
  </td>
 </tr>

</table>
<div align="center">
    <input type="submit" name="_target1" value="Next" />
</div>

<script language="JavaScript">
$$('.rate').each(function(element,i){
	element.addEvent('click', function(){
		var myStyles = [ '', 'twostar',  'fourstar', 'sixstar', 'eightstar', 'tenstar'];
		myStyles.each(function(myStyle){
			if(element.getParent().hasClass(myStyle)){
				element.getParent().removeClass(myStyle);
			}
		});		
		myStyles.each(function(myStyle, index){
			if(index == element.id){
				element.getParent().toggleClass(myStyle);
				$('productReview.rate').set('value',element.id);
			}
		});		
		
	});
});
</script>
</form:form>
</c:if>
<c:if test="${model.product.enableRate!=true}">
At present, this product does not require your feedback.Try again in future.
</c:if>
<c:out value="${model.productReviewLayout.footerHtml}" escapeXml="false"/>
</c:if>
  </tiles:putAttribute>
</tiles:insertDefinition>