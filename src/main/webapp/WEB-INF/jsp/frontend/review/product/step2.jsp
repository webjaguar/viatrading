<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>

<tiles:insertDefinition name="${_template}" flush="true">
  <tiles:putAttribute name="content" type="string">
<c:if test="${gSiteConfig['gPRODUCT_REVIEW'] and model.product.enableRate!=false}">
<c:out value="${model.productReviewLayout.headerHtml}" escapeXml="false"/>
<div class="product">
 <div class="productimg">
  <c:forEach items="${model.product.images}" var="image" varStatus="status">
	<c:if test="${status.first}">
		 <img width="150" src="<c:if test="${!image.absolute}">assets/Image/Product/detailsbig/</c:if><c:out value="${image.imageUrl}"/>" border="0" id="_image" name="_image" class="review_image"
		  alt="<c:out value="${product.name}"/><c:if test="${product.shortDesc != ''}"> - <c:out value="${product.shortDesc}"/></c:if>">
	</c:if>	 	
  </c:forEach>	
 </div>
</div>
<table class="reviewList" border="0" cellpadding="1" cellspacing="0" width="100%">
  <tr class="row${status.index % 2}"><td>
    <table border="0" cellpadding="0" cellspacing="0" width="100%">
      <tr>
      	<td align="right" class="reviewDate"><fmt:formatDate type="date" pattern="MM/dd/yyyy" value="${model.now}"/></td>
      </tr> 	
      <tr>  
	    <td class="reviewRate"><c:choose>
	     <c:when test="${productReviewForm.productReview.rate == 0}"><img src="${_contextpath}/assets/Image/Layout/star_0.gif" alt=<c:out value="${product.rate.average}"/> title="<c:out value="${product.rate.average}"/>" border="0" /></c:when>
	     <c:when test="${productReviewForm.productReview.rate >= 1 and productReviewForm.productReview.rate < 2}"><img src="${_contextpath}/assets/Image/Layout/star_1.gif" alt=<c:out value="${productReviewForm.productReview.rate}"/> title="<c:out value="${productReviewForm.productReview.rate}"/>" border="0" /></c:when>
	     <c:when test="${productReviewForm.productReview.rate >= 2 and productReviewForm.productReview.rate < 3}"><img src="${_contextpath}/assets/Image/Layout/star_2.gif" alt=<c:out value="${productReviewForm.productReview.rate}"/> title="<c:out value="${productReviewForm.productReview.rate}"/>" border="0" /></c:when>
	     <c:when test="${productReviewForm.productReview.rate >= 3 and productReviewForm.productReview.rate < 4}"><img src="${_contextpath}/assets/Image/Layout/star_3.gif" alt=<c:out value="${productReviewForm.productReview.rate}"/> title="<c:out value="${productReviewForm.productReview.rate}"/>" border="0" /></c:when>
	     <c:when test="${productReviewForm.productReview.rate >= 4 and productReviewForm.productReview.rate < 5}"><img src="${_contextpath}/assets/Image/Layout/star_4.gif" alt=<c:out value="${productReviewForm.productReview.rate}"/> title="<c:out value="${productReviewForm.productReview.rate}"/>" border="0" /></c:when>
	     <c:when test="${productReviewForm.productReview.rate == 5}"><img src="${_contextpath}/assets/Image/Layout/star_5.gif" alt=<c:out value="${productReviewForm.productReview.rate}"/> title="<c:out value="${productReviewForm.productReview.rate}"/>" border="0" /></c:when>
	   </c:choose></td>
	  </tr>
	  <tr>
	    <td class="reviewTitle"><c:out value="${productReviewForm.productReview.title}" /></td>
	  </tr>
	  <tr>  
	    <td class="reviewer">By: <c:out value="${productReviewForm.productReview.userName}" /></td>
	  </tr>
	  <tr>  
	    <td class="reviewText"><span><c:out value="${productReviewForm.productReview.text}" escapeXml="false"/></span></td>  				
	  </tr>
     </td></table>
  </tr>
</table>
<form:form commandName="productReviewForm" method="post">
<input type="hidden" name="_page" value="1">
<div align="center">
    <input type="submit" name="_target0" value="back" />
    <input type="submit" name="_finish" value="submit" />
</div>
</form:form>
<c:out value="${model.productReviewLayout.footerHtml}" escapeXml="false"/>
</c:if>
  </tiles:putAttribute>
</tiles:insertDefinition>
