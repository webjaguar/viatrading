<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>

<tiles:insertDefinition name="${_template}" flush="true">
  <tiles:putAttribute name="content" type="string">
<c:if test="${gSiteConfig['gCOMPANY_REVIEW']}">
<c:out value="${model.companyReviewLayout.headerHtml}" escapeXml="false"/>

<c:if test="${model.account == null}">
Member not found.
</c:if>

<c:if test="${model.account != null}">
<fieldset class="register_fieldset">
<legend><fmt:message key="company" /></legend>
<table border="0" cellpadding="0" cellspacing="2" width="100%">
  <tr>
    <td style="width:200px;">
      <div class="companyImage">
        <img src="assets/Image/CompanyLogo/logo_3.gif" border="0" width="200" />
      </div>
    </td>
    <td>
      <table border="0" cellpadding="0" cellspacing="2" width="100%">
      <tr>
	    <td width="20%" align="right"><fmt:message key="companyName" />:</td>
	    <td>&nbsp;</td>
	    <td><c:out value="${model.account.address.company}"/></td>
	  </tr>
	  <tr>
	    <td align="right"><fmt:message key="contactPerson" />:</td>
	    <td>&nbsp;</td>
	    <td><c:out value="${model.account.address.firstName}"/> <c:out value="${model.account.address.lastName}"/></td>
	  </tr>
	  <tr>
	    <td align="right"><fmt:message key="address" />:</td>
	    <td>&nbsp;</td>
	    <td><c:out value="${model.account.address.addr1}"/> <c:out value="${model.account.address.addr2}"/></td>
	  </tr>
	  <tr>
	    <td>&nbsp;</td>
	    <td>&nbsp;</td>
	    <td><c:out value="${model.account.address.city}"/>, <c:out value="${model.account.address.stateProvince}"/>
	    		<c:out value="${model.account.address.country}"/>
	    </td>
	  </tr>
	  <tr>
	    <td align="right"><fmt:message key="zipCode" />:</td>
	    <td>&nbsp;</td>
	    <td><c:out value="${model.account.address.zip}"/></td>
	  </tr>
	  <tr>
	    <td align="right"><fmt:message key="phone" />:</td>
	    <td>&nbsp;</td>
	    <td><c:out value="${model.account.address.phone}"/></td>
	  </tr>
	  <tr>
	    <td align="right"><fmt:message key="fax" />:</td>
	    <td>&nbsp;</td>
	    <td><c:out value="${model.account.address.fax}"/></td>
	  </tr>
	  <tr>
	    <td>&nbsp;</td>
	    <td>&nbsp;</td>
	    <td>&nbsp;</td>
	  </tr>
	  <tr>
	    <td align="right">Website:</td>
	    <td>&nbsp;</td>
	    <td><a href="<c:out value="${model.account.field8}"/>" target="_blank"><c:out value="${model.account.field8}"/></a></td>
	  </tr>
	  </table>
    </td>
  </tr>
</table>  
</fieldset>
</c:if>

<form:form commandName="companyReviewForm" method="post">
<input type="hidden" name="_page" value="0">

<table style="companyReviewForm">
 <tr>
  <td align="left">1.</td>
  <td width="10" align="left"></td>
  <td><span class="fieldtitle">Display Name</span><br />
  <form:input path="companyReview.userName"/>
  <form:errors path="companyReview.userName" cssClass="error" />
  </td>
 </tr>
 <tr>
  <td align="left">2.</td>
  <td width="10" align="left"></td>
  <td><span class="fieldtitle">Overal Rating</span><br />
  <form:select path="companyReview.rate">
   <form:option value="0">-</form:option>
   <form:option value="5">5 Stars (Excellent)</form:option>
   <form:option value="4">4 Stars (Good)</form:option>
   <form:option value="3">3 Stars (Average)</form:option>
   <form:option value="2">2 Stars (Fair)</form:option>
   <form:option value="1">1 Star (Poor)-</form:option>
  </form:select>
  <form:errors path="companyReview.rate" cssClass="error" />
  </td>
 </tr>
 <tr>
  <td align="left">3.</td>
  <td width="10" align="left"></td>
  <td><span class="fieldtitle">Please enter a title for your review:</span><br />
  <form:input path="companyReview.title"/>
  <form:errors path="companyReview.title" cssClass="error" />
  </td>
 </tr>
 <tr>
  <td align="left">4.</td>
  <td width="10" align="left"></td>
  <td><span class="fieldtitle">Please enter your review in the space below:</span><br />
  <form:textarea path="companyReview.text" cssClass="reviewTextarea"/>
  <form:errors path="companyReview.text" cssClass="error" />
  </td>
 </tr>

</table>
<div align="center">
    <input type="submit" name="_target1" value="Next" />
</div>
</form:form>
<c:out value="${model.companyReviewLayout.footerHtml}" escapeXml="false"/>
</c:if>
  </tiles:putAttribute>
</tiles:insertDefinition>