<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>

<tiles:insertDefinition name="${_template}" flush="true">
  <tiles:putAttribute name="content" type="string">
<c:if test="${gSiteConfig['gCOMPANY_REVIEW']}">
<c:out value="${model.companyReviewLayout.headerHtml}" escapeXml="false"/>

<c:if test="${model.account != null}">
<fieldset class="register_fieldset">
<legend><fmt:message key="company" /></legend>
<table border="0" cellpadding="0" cellspacing="2" width="100%">
  <tr>
    <td style="width:200px;">
      <div class="companyImage">
        <img src="assets/Image/CompanyLogo/logo_3.gif" border="0" width="200" />
      </div>
    </td>
    <td>
      <table border="0" cellpadding="0" cellspacing="2" width="100%">
      <tr>
	    <td width="20%" align="right"><fmt:message key="companyName" />:</td>
	    <td>&nbsp;</td>
	    <td><c:out value="${model.account.address.company}"/></td>
	  </tr>
	  <tr>
	    <td align="right"><fmt:message key="contactPerson" />:</td>
	    <td>&nbsp;</td>
	    <td><c:out value="${model.account.address.firstName}"/> <c:out value="${model.account.address.lastName}"/></td>
	  </tr>
	  <tr>
	    <td align="right"><fmt:message key="address" />:</td>
	    <td>&nbsp;</td>
	    <td><c:out value="${model.account.address.addr1}"/> <c:out value="${model.account.address.addr2}"/></td>
	  </tr>
	  <tr>
	    <td>&nbsp;</td>
	    <td>&nbsp;</td>
	    <td><c:out value="${model.account.address.city}"/>, <c:out value="${model.account.address.stateProvince}"/>
	    		<c:out value="${model.account.address.country}"/>
	    </td>
	  </tr>
	  <tr>
	    <td align="right"><fmt:message key="zipCode" />:</td>
	    <td>&nbsp;</td>
	    <td><c:out value="${model.account.address.zip}"/></td>
	  </tr>
	  <tr>
	    <td align="right"><fmt:message key="phone" />:</td>
	    <td>&nbsp;</td>
	    <td><c:out value="${model.account.address.phone}"/></td>
	  </tr>
	  <tr>
	    <td align="right"><fmt:message key="fax" />:</td>
	    <td>&nbsp;</td>
	    <td><c:out value="${model.account.address.fax}"/></td>
	  </tr>
	  <tr>
	    <td>&nbsp;</td>
	    <td>&nbsp;</td>
	    <td>&nbsp;</td>
	  </tr>
	  <tr>
	    <td align="right">Website:</td>
	    <td>&nbsp;</td>
	    <td><a href="<c:out value="${model.account.field8}"/>" target="_blank"><c:out value="${model.account.field8}"/></a></td>
	  </tr>
	  </table>
    </td>
  </tr>
</table>  
</fieldset>
</c:if>

<table border="0" cellpadding="1" cellspacing="0" width="100%">
  <tr class="row${status.index % 2}"><td>
    <table border="0" cellpadding="0" cellspacing="0" width="100%">
      <tr>
      	<td align="right" class="reviewDate"><fmt:formatDate type="date" pattern="MM/dd/yyyy" value="${model.now}"/></td>
      </tr> 	
      <tr>  
	    <td class="reviewRate"><c:choose>
	     <c:when test="${companyReviewForm.companyReview.rate == 0}"><img src="assets/Image/Layout/star_0.gif" alt=<c:out value="${product.rate.average}"/> title="<c:out value="${product.rate.average}"/>" border="0" /></c:when>
	     <c:when test="${companyReviewForm.companyReview.rate >= 1 and companyReviewForm.companyReview.rate < 2}"><img src="assets/Image/Layout/star_1.gif" alt=<c:out value="${companyReviewForm.companyReview.rate}"/> title="<c:out value="${companyReviewForm.companyReview.rate}"/>" border="0" /></c:when>
	     <c:when test="${companyReviewForm.companyReview.rate >= 2 and companyReviewForm.companyReview.rate < 3}"><img src="assets/Image/Layout/star_2.gif" alt=<c:out value="${companyReviewForm.companyReview.rate}"/> title="<c:out value="${companyReviewForm.companyReview.rate}"/>" border="0" /></c:when>
	     <c:when test="${companyReviewForm.companyReview.rate >= 3 and companyReviewForm.companyReview.rate < 4}"><img src="assets/Image/Layout/star_3.gif" alt=<c:out value="${companyReviewForm.companyReview.rate}"/> title="<c:out value="${companyReviewForm.companyReview.rate}"/>" border="0" /></c:when>
	     <c:when test="${companyReviewForm.companyReview.rate >= 4 and companyReviewForm.companyReview.rate < 5}"><img src="assets/Image/Layout/star_4.gif" alt=<c:out value="${companyReviewForm.companyReview.rate}"/> title="<c:out value="${companyReviewForm.companyReview.rate}"/>" border="0" /></c:when>
	     <c:when test="${companyReviewForm.companyReview.rate == 5}"><img src="assets/Image/Layout/star_5.gif" alt=<c:out value="${companyReviewForm.companyReview.rate}"/> title="<c:out value="${companyReviewForm.companyReview.rate}"/>" border="0" /></c:when>
	   </c:choose></td>
	  </tr>
	  <tr>
	    <td class="reviewTitle"><c:out value="${companyReviewForm.companyReview.title}" /></td>
	  </tr>
	  <tr>  
	    <td class="reviewer">By: <c:out value="${companyReviewForm.companyReview.userName}" /></td>
	  </tr>
	  <tr>  
	    <td class="reviewText"><pre><c:out value="${companyReviewForm.companyReview.text}" escapeXml="true"/></pre></td>  				
	  </tr>
     </td></table>
  </tr>
</table>
<form:form commandName="companyReviewForm" method="post">
<input type="hidden" name="_page" value="1">
<div align="center">
    <input type="submit" name="_target0" value="back" />
    <input type="submit" name="_finish" value="submit" />
</div>
</form:form>
<c:out value="${model.companyReviewLayout.footerHtml}" escapeXml="false"/>
</c:if>
  </tiles:putAttribute>
</tiles:insertDefinition>
