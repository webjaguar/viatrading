<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>

<tiles:insertDefinition name="${_template}" flush="true">
  <tiles:putAttribute name="content" type="string">

<c:if test="${fn:trim(model.myaccountLayout.headerHtml) != ''}">
  <c:set value="${model.myaccountLayout.headerHtml}" var="headerHtml"/>
  <c:set value="${fn:replace(headerHtml, '#firstname#', userSession.firstName)}" var="headerHtml"/>
  <c:set value="${fn:replace(headerHtml, '#lastname#', userSession.lastName)}" var="headerHtml"/>
  <c:set value="${fn:replace(headerHtml, '#email#', userSession.username)}" var="headerHtml"/>
  <c:set value="${fn:replace(headerHtml, '#accountnumber#', model.customer.accountNumber)}" var="headerHtml"/>
  <c:set value="${fn:replace(headerHtml, '#addr1#', model.customer.address.addr1)}" var="headerHtml"/>
  <c:set value="${fn:replace(headerHtml, '#addr2#', model.customer.address.addr1)}" var="headerHtml"/>
  <c:set value="${fn:replace(headerHtml, '#city#', model.customer.address.city)}" var="headerHtml"/>
  <c:set value="${fn:replace(headerHtml, '#state#', model.customer.address.stateProvince)}" var="headerHtml"/>
  <c:set value="${fn:replace(headerHtml, '#zip#', model.customer.address.zip)}" var="headerHtml"/>
  <c:set value="${fn:replace(headerHtml, '#country#', model.customer.address.country)}" var="headerHtml"/>
  <c:set value="${fn:replace(headerHtml, '#phone#', model.customer.address.phone)}" var="headerHtml"/>
  <c:if test="${gSiteConfig['gSALES_REP']}">
    <c:set value="${fn:replace(headerHtml, '#salesRepName#', model.salesRep.name)}" var="headerHtml"/>
    <c:set value="${fn:replace(headerHtml, '#salesRepEmail#', model.salesRep.email)}" var="headerHtml"/>
    <c:set value="${fn:replace(headerHtml, '#salesRepPhone#', model.salesRep.phone)}" var="headerHtml"/>
    <c:set value="${fn:replace(headerHtml, '#salesRepCellPhone#', model.salesRep.cellPhone)}" var="headerHtml"/>
    <c:set value="${fn:replace(headerHtml, '#salesRepAddress1#', model.salesRep.address.addr1)}" var="headerHtml"/>
    <c:set value="${fn:replace(headerHtml, '#salesRepAddress2#', model.salesRep.address.addr2)}" var="headerHtml"/>
    <c:set value="${fn:replace(headerHtml, '#salesRepCity#', model.salesRep.address.city)}" var="headerHtml"/>
    <c:set value="${fn:replace(headerHtml, '#salesRepCountry#', model.salesRep.address.country)}" var="headerHtml"/>
    <c:set value="${fn:replace(headerHtml, '#salesRepStateProvince#', model.salesRep.address.stateProvince)}" var="headerHtml"/>
    <c:set value="${fn:replace(headerHtml, '#salesRepZip#', model.salesRep.address.zip)}" var="headerHtml"/>
    <c:set value="${fn:replace(headerHtml, '#salesRepFax#', model.salesRep.address.fax)}" var="headerHtml"/>
    <c:set value="${fn:replace(headerHtml, '#salesRepCompany#', model.salesRep.address.company)}" var="headerHtml"/>
  </c:if>
  <c:out value="${headerHtml}" escapeXml="false"/>
</c:if>

<%@ include file="/WEB-INF/jsp/frontend/evergreen/credit.jsp" %>

<fieldset class="myaccount_fieldset" id="myAccountId">
<legend class="myaccount_legend"><fmt:message key="myAccount" /></legend>
<a href="account_edit.jhtm" class="myaccount_link">
<c:choose>
 <c:when test="${siteConfig['EDIT_EMAIL_ON_FRONTEND'].value == 'true'}"><fmt:message key="shoppingcart.changeYourEmailOrPassword" /></c:when>
 <c:otherwise><fmt:message key="shoppingcart.changeYourPassword" /></c:otherwise>
</c:choose>
</a><br>
<a href="account_addresses.jhtm" class="myaccount_link"><fmt:message key="shoppingcart.manageStoredAddress" /></a>
<br>
<c:if test="${gSiteConfig['gSUGAR_SYNC'] && siteConfig['SUGAR_SYNC_CREDENTIALS'].value != '' && siteConfig['SUGAR_SYNC_CREDENTIALS'].value != ''}">
<a href="account_sugarSync.jhtm" class="myaccount_link">Sugar Sync</a><br>
</c:if>
<a href="account_information_edit.jhtm" class="myaccount_link"><fmt:message key="shoppingcart.editMyInformation"/></a><br>
<a href="account_viewCustomerIDCard_${model.customer.id}.pdf" class="myaccount_link"><fmt:message key="shoppingcart.viewCustomerIDCard"/></a><br>
<a href="loginEventRegister.jhtm" class="myaccount_link"><fmt:message key="shoppingcart.registerforevents"/></a>
</fieldset>

<c:if test="${gSiteConfig['gBUY_REQUEST']}">
<fieldset class="myaccount_fieldset" id="myAccountBuyRequestId">
<legend class="myaccount_legend"><fmt:message key="f_requestToBuy"/></legend>
<a href="buyRequest.jhtm" class="myaccount_link"><fmt:message key="f_newBuyRequests"/></a><br />
<a href="buyRequestList.jhtm" class="myaccount_link"><fmt:message key="f_buyRequests"/></a><br />
<a href="buyRequestWishList.jhtm" class="myaccount_link"><fmt:message key="f_myWishList"/></a>
</fieldset>
</c:if>

<c:if test="${gSiteConfig['gLOCATION']}">
<fieldset class="myaccount_fieldset" id="myAccountLocationId">
<legend class="myaccount_legend"><fmt:message key="location" /></legend>
<a href="locationWishList.jhtm" class="myaccount_link"><fmt:message key="shoppingcart.myFavoritelocations" /></a><br />
</fieldset>
<%-- MTZ
<fieldset class="myaccount_fieldset" id="myAccountupdateLocationId">
<legend class="myaccount_legend"><fmt:message key="f_setting" /></legend>
<a href="location.jhtm" class="myaccount_link"><fmt:message key="f_updateLocation" /></a>
</fieldset>
--%>
</c:if>

<c:if test="${gSiteConfig['gMYLIST']}">
<fieldset class="myaccount_fieldset" id="myAccountMyListId">
<legend class="myaccount_legend"><c:choose><c:when test="${siteConfig['MY_LIST_TITLE'].value != ''}"><c:out value="${siteConfig['MY_LIST_TITLE'].value}" /></c:when><c:otherwise><fmt:message key="myList" /></c:otherwise></c:choose></legend>
<a href="viewList.jhtm" class="myaccount_link"><fmt:message key="view" />&nbsp;<c:choose><c:when test="${siteConfig['MY_LIST_TITLE'].value != ''}"><c:out value="${siteConfig['MY_LIST_TITLE'].value}" /></c:when><c:otherwise><fmt:message key="myList" /></c:otherwise></c:choose></a><br />
</fieldset>
</c:if>

<c:choose>
  <c:when test="${gSiteConfig['gACCOUNTING'] == 'EVERGREEN'}">
    <c:if test="${gSiteConfig['gMYORDER_LIST'] || gSiteConfig['gSHOPPING_CART']}">
	<fieldset class="myaccount_fieldset" id="myAccountOrderListId">
	<legend class="myaccount_legend"><fmt:message key="myOrderHistory" /></legend>
	<c:if test="${gSiteConfig['gMYORDER_LIST']}">
	<a href="viewMyOrderList.jhtm" class="myaccount_link"><c:choose><c:when test="${siteConfig['VIEW_MY_ORDER_LIST_TITLE'].value != ''}"><c:out value="${siteConfig['VIEW_MY_ORDER_LIST_TITLE'].value}" /></c:when><c:otherwise><fmt:message key="f_viewMyOrderList" /></c:otherwise></c:choose></a>
	<br /><a href="account_orders.jhtm" class="myaccount_link"><fmt:message key="f_viewProductsByInvoice" />View&nbsp;Products&nbsp;by&nbsp;Invoice</a>
	</c:if>
	
	<c:if test="${gSiteConfig['gSHOPPING_CART']}">
	<br/><a href="account_orders.jhtm" class="myaccount_link"><c:choose><c:when test="${siteConfig['VIEW_MY_ORDER_HISTORY_STATUS_TITLE'].value != ''}"><c:out value="${siteConfig['VIEW_MY_ORDER_HISTORY_STATUS_TITLE'].value}" /></c:when><c:otherwise><fmt:message key="view" />&nbsp;<fmt:message key="f_myOrderHistoryStatus" /></c:otherwise></c:choose></a>

	<br/><a href="evergreen_orders.jhtm" class="myaccount_link"><fmt:message key="f_viewAllEvergreenProcessedOrders" /></a>
	<br/><a href="evergreen_orderFulfillmentSchedule.jhtm" class="myaccount_link"><fmt:message key="f_myOrderFulfillmentSchedule" /></a>
	<br/><a href="evergreen_myCreditsDamagedReportsList.jhtm" class="myaccount_link"><fmt:message key="f_myCredits/DamagedReports" /></a>
	<br/><a href="evergreen_claimForm.jhtm" class="myaccount_link"><fmt:message key="f_reportAProblemwithOrder" /></a>
	</c:if>
	<br/><a href="account_manifests.jhtm" class="account_manifests"><fmt:message key="f_viewRetrieveManifests" /></a>
	</fieldset>
	</c:if>
	
  </c:when>
  <c:otherwise>
	<c:if test="${gSiteConfig['gMYORDER_LIST'] || gSiteConfig['gSHOPPING_CART']}">
	<fieldset class="myaccount_fieldset" id="myAccountOrderListId">
	<legend class="myaccount_legend"><fmt:message key="myOrderHistory" /></legend>
	<c:if test="${gSiteConfig['gMYORDER_LIST']}">
		<a href="viewMyOrderList.jhtm" class="myaccount_link"><c:choose><c:when test="${siteConfig['VIEW_MY_ORDER_LIST_TITLE'].value != ''}"><c:out value="${siteConfig['VIEW_MY_ORDER_LIST_TITLE'].value}" /></c:when><c:otherwise><fmt:message key="f_viewMyOrderList" /></c:otherwise></c:choose></a>
	</c:if>
	<c:if test="${gSiteConfig['gSHOPPING_CART']}">
		<br/><a href="account_orders.jhtm" class="myaccount_link"><c:choose><c:when test="${siteConfig['VIEW_MY_ORDER_HISTORY_STATUS_TITLE'].value != ''}"><c:out value="${siteConfig['VIEW_MY_ORDER_HISTORY_STATUS_TITLE'].value}" /></c:when><c:otherwise><fmt:message key="f_viewMyOrderHistoryStatus" /></c:otherwise></c:choose></a>
		<c:if test="${siteConfig['MAS200'].value != ''}">
		<br/><a href="jbd_orders.jhtm" class="myaccount_link"><fmt:message key="f_viewJoanBakerInvoices" /></a>
		<br/><a href="jbd_invoices.jhtm" class="myaccount_link"><fmt:message key="f_viewJoanBakerInvoices" /></a>
		</c:if>
	</c:if>
	<br/><a href="account_manifests.jhtm" class="account_manifests"><fmt:message key="f_viewRetrieveManifests" /></a>
	</fieldset>
	</c:if>
		
	<c:if test="${siteConfig['PRODUCT_QUOTE'].value == 'true'}">
	<fieldset class="myaccount_fieldset" id="myAccountProductQuoteId">
	<legend class="myaccount_legend"><fmt:message key="myQuoteHistory" /></legend>
	<a href="account_quotes.jhtm" class="myaccount_link"><fmt:message key="view" />&nbsp;<fmt:message key="myQuoteHistory" /></a>
	</fieldset>
	</c:if>
  </c:otherwise>
</c:choose>



<c:if test="${gSiteConfig['gSUBSCRIPTION'] and model.hasSubscriptions}">
<fieldset class="myaccount_fieldset" id="myAccountSubscriptionId">
<legend class="myaccount_legend"><fmt:message key="f_subscriptions" /></legend>
<a href="account_subscriptions.jhtm" class="myaccount_link"><fmt:message key="view" />&nbsp;<fmt:message key="f_subscriptions" /></a>
</fieldset>
</c:if>

<c:if test="${gSiteConfig['gSUB_ACCOUNTS'] and model.hasSubAccounts}">
<fieldset class="myaccount_fieldset" id="myAccountSubAccountsId">
<legend class="myaccount_legend"><fmt:message key="f_subAccounts" /></legend>
<a href="account_subs.jhtm" class="myaccount_link"><fmt:message key="view" />&nbsp;<fmt:message key="f_subAccounts" /></a>
</fieldset>
</c:if>

<c:if test="${gSiteConfig['gBUDGET_BRAND'] or gSiteConfig['gBUDGET_PRODUCT']}">
<fieldset class="myaccount_fieldset" id="myAccountBudgetId">
<legend class="myaccount_legend"><fmt:message key="f_budget" />&nbsp;<fmt:message key="f_reports" /></legend>
<c:if test="${gSiteConfig['gBUDGET_BRAND']}">
<a href="reportByBrands.jhtm" class="myaccount_link"><fmt:message key="f_reportByBrands" /></a><br/>
<c:if test="${gSiteConfig['gSUB_ACCOUNTS'] and model.hasSubAccounts}">
<a href="dReportByBrands.jhtm" class="myaccount_link"><fmt:message key="f_dReportByBrands" /></a><br/>
<a href="exportByBrands.jhtm" class="myaccount_link"><fmt:message key="f_export" />&nbsp;<fmt:message key="f_brands" /></a><br/>
</c:if>
<c:forEach items="${model.brands}" var="brand">
<c:set var="skuPrefix" value=",${brand.skuPrefix},"/>
<c:if test="${fn:contains(model.customer.brandReport, skuPrefix)}">
<a href="reportByBrand.jhtm?brand=<c:out value="${brand.skuPrefix}"/>" class="myaccount_link"><c:out value="${brand.name}"/>&nbsp;<fmt:message key='f_report' /></a><br/>
</c:if>
</c:forEach>
</c:if>
<c:if test="${gSiteConfig['gBUDGET_PRODUCT']}">
<a href="reportByProducts.jhtm" class="myaccount_link"><fmt:message key="f_reportByProducts" /></a><br/>
<a href="dReportByProducts.jhtm" class="myaccount_link"><fmt:message key="f_dReportByProducts" /></a><br/>
</c:if>
</fieldset>
</c:if>

<c:if test="${gSiteConfig['gSERVICE']}">
<fieldset class="myaccount_fieldset" id="myAccountServiceId">
<legend class="myaccount_legend"><fmt:message key="f_service" /></legend>
<a href="account_services.jhtm" class="myaccount_link"><fmt:message key="f_serviceRequest" /></a><br/>
<a href="account_serviceItems.jhtm" class="myaccount_link"><c:out value="${siteConfig['SERVICEABLE_ITEM_TITLE'].value}"/>s</a>
</fieldset>
</c:if>

<c:if test="${gSiteConfig['gRMA']}">
<fieldset class="myaccount_fieldset" id="myAccountRmaId">
<legend class="myaccount_legend"><fmt:message key="f_rma" /></legend>
<a href="account_rmas.jhtm" class="myaccount_link"><fmt:message key="f_rmaRequest" /></a><br/>
</fieldset>
</c:if>

<c:if test="${gSiteConfig['gTICKET']}">
<fieldset class="myaccount_fieldset" id="myAccountTicketId">
<legend class="myaccount_legend"><fmt:message key="f_support" /></legend>
<a href="account_ticket.jhtm" class="myaccount_link"><fmt:message key="newTicket" /></a><br />
<a href="account_ticketList.jhtm" class="myaccount_link"><fmt:message key="tickets" /></a>
</fieldset>
</c:if>

<c:if test="${gSiteConfig['gGIFTCARD']}">
<fieldset class="myaccount_fieldset" id="myAccountGiftCardId">
<legend class="myaccount_legend"><fmt:message key="giftCard" /></legend>
<a href="giftcard.jhtm" class="myaccount_link"><fmt:message key="f_buyAGiftCard" /></a><br />
<a href="giftcardBalance.jhtm" class="myaccount_link"><fmt:message key="f_viewGiftCardBalance" /></a>
</fieldset>
</c:if>

<c:if test="${model.protectedAccessLinkStatus}">
<fieldset class="myaccount_fieldset" id="myAccountLinkId">
<legend class="myaccount_legend"><fmt:message key="myLink" /></legend>
<a href="https://www.viatrading.com/dv/manifest/lotnotification.php?id=${model.customer.id}" class="myaccount_link"><fmt:message key="link" /></a>
</fieldset>
</c:if>

<c:if test="${gSiteConfig['gLOYALTY'] and siteConfig['LOYALTY_SHOW_ON_MYACCOUNT'].value == 'true'}">
<fieldset class="myaccount_fieldset">
<legend class="myaccount_legend"><fmt:message key="f_loyalty" /></legend>
<div>
   <fmt:message key="totalPoint(s)" />:&nbsp;<fmt:formatNumber value="${model.customer.loyaltyPoint}" pattern="#,##0.00" />
</div>
</fieldset>
</c:if>

<c:if test="${model.hasCustomNote1 or model.hasCustomNote2 or model.hasCustomNote3}">
<fieldset class="myaccount_fieldset" id="myAccountCustomNoteId">
<legend class="myaccount_legend"><fmt:message key="misc" /></legend>
<c:if test="${model.hasCustomNote1}">
<a href="account.jhtm?note=1" class="myaccount_link"><c:out value="${siteConfig['CUSTOMER_CUSTOM_NOTE_1'].value}"/></a>
</c:if>
<c:if test="${model.hasCustomNote2}">
<br/>
<a href="account.jhtm?note=2" class="myaccount_link"><c:out value="${siteConfig['CUSTOMER_CUSTOM_NOTE_2'].value}"/></a>
</c:if>
<c:if test="${model.hasCustomNote3}">
<br/>
<a href="account.jhtm?note=3" class="myaccount_link"><c:out value="${siteConfig['CUSTOMER_CUSTOM_NOTE_3'].value}"/></a>
</c:if>
</fieldset>
</c:if>

<c:if test="${gSiteConfig['gTAX_EXEMPTION']}">
<fieldset class="myaccount_fieldset" id="myAccountTaxExemptionId">
<legend class="myaccount_legend"><fmt:message key="f_taxInfo" /></legend>
  <fmt:message key="taxId" />: <c:out value="${model.customer.taxId}" /><br />
  <c:if test="${model.customer.taxId == null or empty model.customer.taxId}" >
    <div style="color:#FF0000"><fmt:message key="f_taxNote" /></div>
  </c:if>
  <br />
</fieldset>
</c:if>

<c:if test="${gSiteConfig['gAFFILIATE'] > 0 and model.affiliateCustomer}">
<fieldset class="myaccount_fieldset" id="myAccountAffiliateId">
<legend class="myaccount_legend">Affiliate Program</legend>
<a href="account_affiliate.jhtm" class="myaccount_link"><fmt:message key="f_myCommissionLevel" /> 1</a>
<div>
   <fmt:message key="totalCommission" />:&nbsp;<fmt:message key="${siteConfig['CURRENCY'].value}" /><fmt:formatNumber value="${model.level1.commissionTotal}" pattern="#,##0.00" />
</div><br />
<c:if test="${gSiteConfig['gAFFILIATE'] > 1}">
<a href="account_affiliate.jhtm?l=2" class="myaccount_link"><fmt:message key="f_myCommissionLevel" /> 2</a>
<div>
   <fmt:message key="f_totalCommission" />:&nbsp;<fmt:message key="${siteConfig['CURRENCY'].value}" /><fmt:formatNumber value="${model.level2.commissionTotal}" pattern="#,##0.00" />
</div>
</c:if><br />
<fmt:message key="sourceCode" />: <br />       
        <c:if test="${model.customer.affiliate.promoTitle != null}">
          <c:out value="${siteConfig['SITE_URL'].value}"/>home.jhtm?promocode=<c:out value="${model.customer.affiliate.promoTitle}"/>
        </c:if>
        <c:if test="${model.customer.affiliate.categoryId != null and model.customer.affiliate.promoTitle != null}">
          <c:out value="${siteConfig['SITE_URL'].value}"/>category.jhtm?cid=<c:out value="${model.customer.affiliate.categoryId}"/>&promcode=<c:out value="${model.customer.affiliate.promoTitle}"/>
        </c:if>
        <c:if test="${model.customer.affiliate.domainName != null}">
          <c:out value="${model.customer.affiliate.domainName}"/>
        </c:if> 
</fieldset>
</c:if>

<c:if test="${gSiteConfig['gGIFTCARD'] or (gSiteConfig['gVIRTUAL_BANK_ACCOUNT'] and model.customer.supplierId != null) or gSiteConfig['gBUDGET']}">
<fieldset class="myaccount_fieldset"  id="myAccountCreditId">
<legend class="myaccount_legend"><fmt:message key="f_credit" /></legend>
<div>
   <fmt:message key="f_totalCredit" />:&nbsp;<fmt:formatNumber value="${model.customer.credit}" pattern="#,##0.00" /><br />
	<a href="credit_history.jhtm" class="myaccount_link"><fmt:message key="f_viewMyBalance" /></a>
</div>
</fieldset>
</c:if>

<c:if test="${gSiteConfig['gBUDGET'] and siteConfig['BUDGET_ADD_PARTNER'].value == 'true'}">
<fieldset class="myaccount_fieldset"  id="myAccountPartnersId">
<legend class="myaccount_legend"><fmt:message key="partnersList" /></legend>
<div>
	<a href="partnersReport.jhtm" class="myaccount_link"><fmt:message key="partnersList" /></a>
</div>
</fieldset>
</c:if>

<c:if test="${gSiteConfig['gCUSTOMER_SUPPLIER'] and  model.customer.supplierId != null}">
	<fieldset class="myaccount_fieldset"  id="myAccountVbaId">
	<legend class="myaccount_legend"><fmt:message key="f_vba" /></legend>
	<a href="account_products.jhtm" class="myaccount_link"><fmt:message key="f_viewMyProducts" /></a><br />
	<c:if test="${siteConfig['ADD_PRODUCT_ON_FRONTEND'].value == true }">
	  <a href="account_product.jhtm" class="myaccount_link"><fmt:message key="f_addProduct" /></a><br />
	</c:if>
	<%-- MTZ <a href="account_sales.jhtm" class="myaccount_link"><fmt:message key="f_viewMySales"/>(Order based)</a><br /> --%>
	<a href="consignment_sales.jhtm" class="myaccount_link"><fmt:message key="f_viewMySales"/></a><br />
	<a href="consignment_report.jhtm" class="myaccount_link"><fmt:message key="f_viewMyReport" /></a><br />
	</fieldset>
</c:if>

<c:if test="${model.customer.id == 85}">
<fieldset class="myaccount_fieldset">
    <div id="report"></div>
</fieldset>
</c:if>

<fieldset class="myaccount_fieldset">
    <a href="logout.jhtm" class="myaccount_link"><fmt:message key="logout" /></a>
</fieldset>



<c:if test="${fn:trim(model.myaccountLayout.footerHtml) != ''}">
  <c:set value="${model.myaccountLayout.footerHtml}" var="footerHtml"/>
  <c:set value="${fn:replace(footerHtml, '#firstname#', userSession.firstName)}" var="footerHtml"/>
  <c:set value="${fn:replace(footerHtml, '#lastname#', userSession.lastName)}" var="footerHtml"/>
  <c:set value="${fn:replace(footerHtml, '#email#', userSession.username)}" var="footerHtml"/>
  <c:set value="${fn:replace(footerHtml, '#accountnumber#', model.customer.accountNumber)}" var="footerHtml"/>
  <c:if test="${gSiteConfig['gSALES_REP']}">
    <c:set value="${fn:replace(footerHtml, '#salesRepName#', model.salesRep.name)}" var="footerHtml"/>
    <c:set value="${fn:replace(footerHtml, '#salesRepEmail#', model.salesRep.email)}" var="footerHtml"/>
    <c:set value="${fn:replace(footerHtml, '#salesRepPhone#', model.salesRep.phone)}" var="footerHtml"/>
    <c:set value="${fn:replace(footerHtml, '#salesRepCellPhone#', model.salesRep.cellPhone)}" var="footerHtml"/>
    <c:set value="${fn:replace(footerHtml, '#salesRepAddress1#', model.salesRep.address.addr1)}" var="footerHtml"/>
    <c:set value="${fn:replace(footerHtml, '#salesRepAddress2#', model.salesRep.address.addr2)}" var="footerHtml"/>
    <c:set value="${fn:replace(footerHtml, '#salesRepCity#', model.salesRep.address.city)}" var="footerHtml"/>
    <c:set value="${fn:replace(footerHtml, '#salesRepCountry#', model.salesRep.address.country)}" var="footerHtml"/>
    <c:set value="${fn:replace(footerHtml, '#salesRepStateProvince#', model.salesRep.address.stateProvince)}" var="footerHtml"/>
    <c:set value="${fn:replace(footerHtml, '#salesRepZip#', model.salesRep.address.zip)}" var="footerHtml"/>
    <c:set value="${fn:replace(footerHtml, '#salesRepFax#', model.salesRep.address.fax)}" var="footerHtml"/>
    <c:set value="${fn:replace(footerHtml, '#salesRepCompany#', model.salesRep.address.company)}" var="footerHtml"/>
  </c:if>
  <c:out value="${footerHtml}" escapeXml="false"/>
</c:if>


<script type="text/javascript">
<!--
//retrieve data from php
window.addEvent('domready', function(){
	$('report').load('/dv/orderreportnewdb/customerinfo.php?Action=CustomerTotals&id='+${model.customer.id});
});
//-->
</script>
  </tiles:putAttribute>
</tiles:insertDefinition>
