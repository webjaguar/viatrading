<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>

<tiles:insertDefinition name="${_template}" flush="true">
  <tiles:putAttribute name="leftbar" value="/WEB-INF/jsp/frontend/layout/template${_leftBar}/leftbar.jsp" />
  <tiles:putAttribute name="content" type="string">
<c:set value="${false}" var="multibox"/>
<link rel="stylesheet" href="${_contextpath}/assets/SmoothGallery/multiboxASI.css" type="text/css"/>
<script type="text/javascript">
<!--
var summaryBoxTop = 0;
window.addEvent('scroll',function() {
<%-- actual script on framestore has 50 and 180 instead of -200 and 100 --%>
	if(window.getScroll().y > $('multiOptions').getSize().y + 50) {
		$('summaryBoxId').setStyle('position', 'absolute');
		if(summaryBoxTop <= 180) {
			summaryBoxTop = window.getScroll().y + 180;
		}
	}
	if(window.getScroll().y < $('multiOptions').getSize().y + 50 ) {
		$('summaryBoxId').setStyle('position', 'fixed');
		$('summaryBoxId').setStyle('margin-top', '10px');
		summaryBoxTop = 180;
	}
	$('summaryBoxId').setStyle('top', summaryBoxTop);
});
function getOption(productId, radioButton){

	$('multiOptions').fade('0.2');
    if(radioButton != null) {
    	$(radioButton).checked = true;
    }
	(function() {
        var urlappend = '';

    	$$('input.selectedOption').each(function(radio){
    		if(radio.checked) {
    			urlappend = urlappend + '&option='+radio.name+'-'+radio.value;
    		}
    	});
    	if($('customWidth') != null && $('customHeight') != null) {
    		if($('customWidth').value != 0 || $('customHeight').value != 0) {
    			$('customSize').value = $('customWidth').value+'X'+$('customHeight').value;
    			urlappend = urlappend + '&option='+$('customSize').name+'-'+$('customSizeOptIndex').value+'_'+$('customSize').value;
    		}
    	}
		//alert(urlappend);
    	var requestHTMLData = new Request.HTML ({
    		url: "${_contextpath}/ajax-customframe-options.jhtm?&productId="+productId+"&"+urlappend,
			update: $('newOption'),
    		onSuccess: function(response){
				new Fx.Scroll(window).set(0, $('newOption').getSize().y - 100);
        	}
    	}).send();
     }).delay(200);
}

function assignValue(ele, value) {
	if($('customWidth') != null && $('customHeight') != null) {
		if($('customWidth').value != 0 || $('customHeight').value != 0) {
			$(ele).value = $('customWidth').value+'X'+$('customHeight').value;
			getOption($('productId').value, null);
		}
	}
}

function toggleSelection(radioSelected){
	if(radioSelected != true){
		$$('input.selectedOption').each(function(radio){
			if(radio.name.indexOf('Choose Your Diploma Size') != -1 && radio.checked) {
				radio.checked = false;
			}
		});
	} else {
		$('customWidth').value = 0;
		$('customHeight').value = 0;
	}
}
/*
function enableAddToCartOption(){

	var selectedOption = 0;	
	$$('input.selectedOption').each(function(radio){
		if(radio.checked) {
			selectedOption = selectedOption + 1;
		}
	});
	if($('customWidth') != null && $('customHeight') != null) {
		if($('customWidth').value != 0 && $('customHeight').value != 0) {
			selectedOption = selectedOption + 1;
		}
	}

	if( ( parseInt($('totalOption').value) == parseInt(selectedOption) ) || ( parseInt($('totalOption').value) == 5)) { 
		$('customAddToCart').setStyle('display', 'block');
	} else {
		$('customAddToCart').setStyle('display', 'none');
	}
	
}
*/
//comparison
function compareProduct(productId){
	if(parseInt($('currentComparison').value) < parseInt($('maxComparisons').value)) {
		var urlappend = '';
    	$$('input.selectedOption').each(function(radio){
    		if(radio.checked && radio.name != name) {
    			urlappend = urlappend + '&option='+radio.name+'-'+radio.value;
    		}
    	});
    	if($('customWidth') != null && $('customHeight') != null) {
    		if($('customWidth').value != 0 || $('customHeight').value != 0) {
    			$('customSize').value = $('customWidth').value+'X'+$('customHeight').value;
    			urlappend = urlappend + '&option='+$('customSize').name+'-'+$('customSizeOptIndex').value+'_'+$('customSize').value;
    		}
    	}
    	//alert(urlappend);
    	var requestHTMLData = new Request.HTML ({
    		url: "${_contextpath}/ajaxAddToCompare.jhtm?&productId="+productId+"&"+urlappend,
    		onSuccess: function(response){ 
				$('product'+productId).set('html', '<input type=\"image\" class="compare" src=\"${_contextpath}/assets/Image/Layout/button_addedToCompare.png\" width=\"170px;\" height=\"50px;\" onclick=\"return false;\"/>');
				$('currentComparison').value = parseInt($('currentComparison').value)+ 1;
				$('viewComparison').setStyle('display', 'block');
			}
    	}).send();
	} else {
		alert('Max comparison limit reached.');
	}
}
//-->
</script>

<c:if test="${model.mootools and param.hideMootools == null}">
<script src="${_contextpath}/javascript/mootools-1.2.5.1-more.js" type="text/javascript"></script>
</c:if>

<c:if test="${fn:trim(model.productLayout.headerHtml) != ''}">
  <c:set value="${model.productLayout.headerHtml}" var="headerHtml"/>
  <%@ include file="/WEB-INF/jsp/frontend/common/headerDynamicElement.jsp" %>
  <div><c:out value="${headerHtml}" escapeXml="false"/></div>
</c:if>

<c:if test="${model.message != null}">
  <div class="message"><fmt:message key="${model.message}" /></div>
</c:if>

<c:if test="${model.product != null}">
<c:set value="${model.product}" var="product"/>

<table class="container-details" border="0" cellpadding="0" cellspacing="0">

<tr class="middle">
<td class="middleMiddle">
<!-- details start -->

<div class="details5" style="float:left;">

<c:if test="${product.longDesc != ''}">
  <div class="details_long_desc"><c:out value="${product.longDesc}" escapeXml="false" /></div>  
</c:if>
<input type="hidden" name="productId" value="${product.id}" id="productId"/>
 
<form action="${_contextpath}/addToCart.jhtm" name="this_form" method="post">
  <%@ include file="/WEB-INF/jsp/frontend/framestore/productOptions.jsp"%>
</form>
</div>
<!-- details end -->
</td>
</tr>
</table>

</c:if>

<c:if test="${fn:trim(model.productLayout.footerHtml) != ''}">
  <c:set value="${model.productLayout.footerHtml}" var="footerHtml"/>
  <%@ include file="/WEB-INF/jsp/frontend/common/footerDynamicElement.jsp" %>
  <div style="float:left;width:100%"><c:out value="${footerHtml}" escapeXml="false"/></div>
</c:if>
  </tiles:putAttribute>
</tiles:insertDefinition>