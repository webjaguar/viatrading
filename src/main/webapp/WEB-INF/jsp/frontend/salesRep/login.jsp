<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="/WEB-INF/tlds/spring.tld" prefix="spring" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>

<tiles:insertDefinition name="${_template}" flush="true">
  <tiles:putAttribute name="content" type="string">
  
<c:out value="${model.loginLayout.headerHtml}" escapeXml="false"/>

<c:if test="${param.noMessage != 't'}">
	<c:if test="${!empty model.message}">
	  <div><br/><b><font color="RED"><fmt:message key="${model.message}"><fmt:param value="${siteConfig['IP_ADDRESS_ERROR_MESSAGE'].value}"/></fmt:message></font></b></div>
	</c:if>
</c:if>

<table align="center" border="0">
<tr valign="top">
<td>
<fieldset class="register_returnCustomer_fieldset">
<legend>Please Sign In</legend>
<form action="srLogin.jhtm" method="POST">
<c:if test="${!empty signonForwardAction}">
<input type="hidden" name="forwardAction" value="${signonForwardAction}"/>
</c:if>
<table align="center" border="0" height="110">
<tr>
<td><fmt:message key="accountNumber"/>:</td>
<td><input type="text" name="accountNumber" value="${param.accountNumber}" /></td>
</tr>
<tr>
<td><fmt:message key="password" />:</td>
<td><input type="password" name="password" value="" /></td>
</tr>
<tr>
<td colspan="2" align="center"><input type="submit" value="<fmt:message key="logIn"/>" /></td>
</tr>
</table>
</form>
</fieldset>
</td>
</tr>
</table>

<c:out value="${model.loginLayout.footerHtml}" escapeXml="false"/>

  </tiles:putAttribute>
</tiles:insertDefinition>
