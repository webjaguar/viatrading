<%@ page import="com.webjaguar.model.*,org.springframework.web.bind.ServletRequestUtils"%>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="/WEB-INF/tlds/spring.tld" prefix="spring" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%--Spring3 DONE--%>
<tiles:insertDefinition name="${_template}" flush="true">
  <tiles:putAttribute name="content" type="string">

<c:if test="${fn:trim(model.salesrepLayout.headerHtml) != ''}">
  <c:set value="${model.salesrepLayout.headerHtml}" var="headerHtml"/>
  <c:set value="${fn:replace(headerHtml, '#salesRepName#', model.salesRep.name)}" var="headerHtml"/>
  <c:set value="${fn:replace(headerHtml, '#salesRepEmail#', model.salesRep.email)}" var="headerHtml"/>
  <c:set value="${fn:replace(headerHtml, '#salesRepPhone#', model.salesRep.phone)}" var="headerHtml"/>
  <c:set value="${fn:replace(headerHtml, '#salesRepCellPhone#', model.salesRep.cellPhone)}" var="headerHtml"/>
  <c:set value="${fn:replace(headerHtml, '#salesRepAddress1#', model.salesRep.address.addr1)}" var="headerHtml"/>
  <c:set value="${fn:replace(headerHtml, '#salesRepAddress2#', model.salesRep.address.addr2)}" var="headerHtml"/>
  <c:set value="${fn:replace(headerHtml, '#salesRepCity#', model.salesRep.address.city)}" var="headerHtml"/>
  <c:set value="${fn:replace(headerHtml, '#salesRepCountry#', model.salesRep.address.country)}" var="headerHtml"/>
  <c:set value="${fn:replace(headerHtml, '#salesRepStateProvince#', model.salesRep.address.stateProvince)}" var="headerHtml"/>
  <c:set value="${fn:replace(headerHtml, '#salesRepZip#', model.salesRep.address.zip)}" var="headerHtml"/>
  <c:set value="${fn:replace(headerHtml, '#salesRepFax#', model.salesRep.address.fax)}" var="headerHtml"/>
  <c:set value="${fn:replace(headerHtml, '#salesRepCompany#', model.salesRep.address.company)}" var="headerHtml"/>
  <c:out value="${headerHtml}" escapeXml="false"/>
</c:if>

<script type="text/javascript">
function toggleAll(el) {
	var ids = document.getElementsByName("__selected_id");	
  	if (ids.length == 1)
      document.list_form.__selected_id.checked = el.checked;
    else
      for (i = 0; i < ids.length; i++)
        document.list_form.__selected_id[i].checked = el.checked;	
} 
function optionsSelected() {
	var ids = document.getElementsByName("__selected_id");	
	if (ids.length == 1) {
      if (document.list_form.__selected_id.checked) {
      	return true;
      }
    } else {
      for (i = 0; i < ids.length; i++) {
        if (document.list_form.__selected_id[i].checked) {
        	return true;
        }
      }
    }
}
</script>
<style type="text/css">
.sortLink {
	color: #990000;
	text-decoration: none;
	}
.sortLink:hover {
	text-decoration: underline;
	}
.sortLinkOff {
	color: #FFFFFF;
	text-decoration: none;
	}
.sortLinkOff:hover {
	text-decoration: underline;
	}
.rowOff {
	background: #FFCCCC
	}
.loginAs {
	color: #003366;
	text-decoration: none;
	}
.loginAs:hover {
	text-decoration: underline;
	}
.salesRepLink {
	color: #003366;
	text-decoration: none;
	}
.salesRepLink:hover {
	text-decoration: underline;
	}
.salesRepLinkSelected {
	color: #003366;
	text-decoration: underline;
	font-weight: bold;
	}
.salesRepMain {
	color: #990000;
	text-decoration: none;
	font-weight: bold;
	}
.salesRepMain:hover {
	text-decoration: underline;
	}
</style>  

<c:if test="${not empty model.salesRep.subAccounts}">
<fieldset class="myaccount_fieldset">
<legend class="myaccount_legend">Sales Reps Tree</legend>
<c:set value="${model.salesRep}" var="modelSalesRep"/>
<div style="padding-bottom: 5px;">
<a href="salesRep.jhtm" class="salesRepMain"><c:out value="${model.salesRep.name}"/></a>
</div>
<c:set var="salesRepList" value="${model.salesRep.subAccounts}" scope="request"/>

<c:set var="level" value="0" scope="request"/>
<jsp:include page="/WEB-INF/jsp/frontend/salesRep/recursion.jsp"/>
</fieldset>
</c:if>

<fieldset class="myaccount_fieldset">
<legend class="myaccount_legend">Customers</legend>
<div style="padding-bottom: 5px;">
<a href="salesRep.jhtm" class="salesRepMain">Customers</a>
</div>
</fieldset>

<c:if test="${gSiteConfig['gPRESENTATION']}">
<fieldset class="myaccount_fieldset">
<legend class="myaccount_legend">Presentation</legend>
<div style="padding-bottom: 5px;">
<a href="presentationList.jhtm" class="salesRepMain">View Presentations</a>
</div>
</fieldset>
</c:if>

<fieldset class="myaccount_fieldset">
<legend class="myaccount_legend">Logout</legend>
<div style="padding-bottom: 5px;">
<a href="srLogout.jhtm" class="logout">Logout</a>
</div>
</fieldset>


<c:if test="${fn:trim(model.salesrepLayout.footerHtml) != ''}">
  <c:set value="${model.salesrepLayout.footerHtml}" var="footerHtml"/>
  <c:set value="${fn:replace(footerHtml, '#salesRepName#', model.salesRep.name)}" var="footerHtml"/>
  <c:set value="${fn:replace(footerHtml, '#salesRepEmail#', model.salesRep.email)}" var="footerHtml"/>
  <c:set value="${fn:replace(footerHtml, '#salesRepPhone#', model.salesRep.phone)}" var="footerHtml"/>
  <c:set value="${fn:replace(footerHtml, '#salesRepCellPhone#', model.salesRep.cellPhone)}" var="footerHtml"/>
  <c:set value="${fn:replace(footerHtml, '#salesRepAddress1#', model.salesRep.address.addr1)}" var="footerHtml"/>
  <c:set value="${fn:replace(footerHtml, '#salesRepAddress2#', model.salesRep.address.addr2)}" var="footerHtml"/>
  <c:set value="${fn:replace(footerHtml, '#salesRepCity#', model.salesRep.address.city)}" var="footerHtml"/>
  <c:set value="${fn:replace(footerHtml, '#salesRepCountry#', model.salesRep.address.country)}" var="footerHtml"/>
  <c:set value="${fn:replace(footerHtml, '#salesRepStateProvince#', model.salesRep.address.stateProvince)}" var="footerHtml"/>
  <c:set value="${fn:replace(footerHtml, '#salesRepZip#', model.salesRep.address.zip)}" var="footerHtml"/>
  <c:set value="${fn:replace(footerHtml, '#salesRepFax#', model.salesRep.address.fax)}" var="footerHtml"/>
  <c:set value="${fn:replace(footerHtml, '#salesRepCompany#', model.salesRep.address.company)}" var="footerHtml"/>
  <c:out value="${footerHtml}" escapeXml="false"/>
</c:if>

  </tiles:putAttribute>
</tiles:insertDefinition>
<%-- Spring2.5 Old Code
<%!
void buildTree(SalesRep salesRep, int level, JspWriter out, int paramId) throws Exception {
	for (SalesRep thisSalesRep: salesRep.getSubAccounts()) {
		out.print("<div style=\"padding-bottom:5px;padding-left:" + (20+level*20) + "px;\">");
		out.print("<a href=\"salesRep.jhtm?id=" + thisSalesRep.getId() + "\" class=\"salesRepLink");
		if (thisSalesRep.getId().compareTo(paramId) == 0) out.print("Selected");
		out.print("\">");
		out.print(thisSalesRep.getName());
		out.print("</a>");
		out.println("</div>");
				
		buildTree(thisSalesRep, level+1, out, paramId);				
   }
}
%> 
--%>