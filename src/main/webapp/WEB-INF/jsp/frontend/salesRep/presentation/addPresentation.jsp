<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="/WEB-INF/tlds/spring.tld" prefix="spring" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles" %>

<tiles:insertDefinition name="${_template}" flush="true">
  <tiles:putAttribute name="content" type="string">
  
   <!-- breadcrumb -->
	  <p class="breadcrumb">
	    <a href="presentationList.jhtm"><fmt:message key="presentations" /></a> &gt; <fmt:message key="f_newPresentationForm" />
	  </p>
	  
	 <c:out value="${model.salesrepLayout.headerHtml}" escapeXml="false"/>
	  

		<form name="addPresentation" method="POST" action="addPresentation.jhtm">
		<div class="form_box" id="presentationBoxId">
				<ol>
					<li>
						<div class="listfl"><strong><fmt:message key='name' />:</strong></div>
						<form:input path="presentationForm.presentation.name" cssClass="textfield" size="50" maxlength="120" htmlEscape="true" />
						<form:errors path="presentationForm.presentation.name" cssClass="error"/>
					</li>
					<li>
						<div class="listfl"><strong><fmt:message key='f_presentationTitle' />:</strong></div>
						<div class="listp">
						    <form:input path="presentationForm.presentation.title" cssClass="textfield" size="50" maxlength="120" htmlEscape="true" /></div>
					</li>
					<li>
						<form:select path="presentationForm.presentation.templateId">
							<form:option value="0">Select Template</form:option>
							<c:forEach items="${presentationForm.templateList}" var="templateList">
								<form:option value="${templateList.id}">
									<c:out value="${templateList.name}" />
									<c:out value="( Product Per Page ${templateList.noOfProduct})" />
								</form:option>
							</c:forEach>
						</form:select>
						<form:errors path="presentationForm.presentation.templateId" cssClass="error"/>
					</li>
					<li>
						<div class="listfl"><strong><fmt:message key='note' />: This will show on presentation below products list.</strong></div>
						<div class="listp">
							<form:textarea rows="16" cols="70" cssClass="textfield" 
							path="presentationForm.presentation.note" htmlEscape="true" /></div>
					</li>
					<li>
						<table class="button_box">
							<tr>
								<c:if test="${presentationForm.newPresentation != 'false'}">
									<td><input class="presentation_add_presentation_button"
										type="submit" value="<spring:message code="add"/>" name="add" />
									</td>
								</c:if>
								<c:if test="${presentationForm.newPresentation != 'true'}">
									<td><input class="presentation_edit_presentation_button"
										type="submit" value="Update" name="update" /></td>
									<td><input class="presentation_delete_presentation_button"
										type="submit" value="Delete" name="_delete" onClick="return confirm('Delete permanently?')"/></td>
								</c:if>
								<td><input class="presentation_delete_presentation_button"
									type="submit" value="Cancel" name="_cancel" /></td>
							</tr>
						</table>
					</li>
				</ol>
		</div>
		</form>

<c:out value="${model.salesrepLayout.footerHtml}" escapeXml="false"/>

		
	</tiles:putAttribute>
</tiles:insertDefinition>