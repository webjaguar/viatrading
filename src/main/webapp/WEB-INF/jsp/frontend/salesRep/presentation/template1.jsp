<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="/WEB-INF/tlds/spring.tld" prefix="spring" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ taglib uri="/WEB-INF/tlds/custom.tld" prefix="wj" %>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<link rel="stylesheet" href="assets/presentation.css" type="text/css"/>
<c:if test="${siteConfig['PRODUCT_QUOTE'].value == 'true'}">
<script type="text/javascript" src="javascript/mootools-1.2.5-core.js"></script>
<script type="text/javascript">
function toggleAll(el) {
	var ids = document.getElementsByName("__selected_product_id");	
  	if (ids.length == 1)
      document.testView_form.__selected_product_sku.checked = el.checked;
    else
      for (i = 0; i < ids.length; i++)
        document.test_form.__selected_product_sku[i].checked = el.checked;	
}
function addToQuoteList(productId){
	var requestHTMLData = new Request.HTML ({
		url: "${_contextpath}/quoteList.jhtm?productId="+productId,
		onSuccess: function(response){ 
			$('product'+productId).set('html', '<fmt:message key="f_addedToQuoteList" />');
	 	}
	}).send();
}

</script>
</c:if>
</head> 
<body class="presentationWrapper">
<c:set var="presentation" value="${model.presentation}"/>
<div>
<div class="previous_page">
	<c:if test="${presentation.page >1}">
	<c:choose>
	<c:when test="${presentation.editUrl != null}">
		<a href="${presentation.editUrl}&page=${presentation.page - 1}">previous</a>
	</c:when>
	<c:otherwise>
		<a href="${presentation.url}&page=${presentation.page - 1}">previous</a>
	</c:otherwise>
	</c:choose>
	</c:if><c:if test="${!(presentation.page >1)}">previous</c:if>
</div> 
<div class="back">
	<a href="presentationList.jhtm">Presentation List</a>
</div>
<div class="next_page">
	<c:if test="${presentation.pageCount > (presentation.page)}">
	<c:choose>
	<c:when test="${presentation.editUrl != null}">
	<a href="${presentation.editUrl}&page=${presentation.page + 1}">next</a>
	</c:when>
	<c:otherwise>
	<a href="${presentation.url}&page=${presentation.page + 1}">next</a>
	</c:otherwise>
	</c:choose>
	</c:if><c:if test="${! (presentation.pageCount > (presentation.page))}">next</c:if>
</div>
<div style="clear:both"/>
</div>
<c:if test="${presentation.editPrice == 'true'}">
	<form name="addPresentation" method="POST" action="${presentation.editUrl}">
</c:if>
<div class="presentation_template_design_box" id="presentationTemplateDesignBoxId">
 	<c:out value="${presentation.design}" escapeXml="false" />
</div>
<div class="presentation_info_table_box" id="presentationInfoTableBoxId">
	<table border="1" bordercolor=grey cellpadding="3" cellspacing="0" width="100%" class="presentation_info_table" id="presentationInfoTableId"> 
	  <tr class="listingsHdr2">
	  	<c:set var="cols" value="0"/>
	  	<c:set var="cols" value="${cols+1}"/>
	    <c:if test="${siteConfig['PRODUCT_QUOTE'].value == 'true' and (model.showProductQuoteColumn == null or model.showProductQuoteColumn)}" >
  			<th class="nameCol"><fmt:message key="f_addToQuote" /></th>
  		</c:if>
	      <c:set var="cols" value="${cols+1}"/>
	       <th class="nameCol">Item No</th> 
	       <c:forEach items="${model.productFields}" var="productField">
			  <c:if test="${productField.showOnPresentation}">
		  		<th class="nameCol"><c:out value="${productField.name}" escapeXml="false" /></th>
			  </c:if>
		  </c:forEach>
		  
		  <c:set var="cols" value="${cols+1}"/>
	       <th class="nameCol">Quantity Available</th> 
	       
	      <th class="nameCol"><c:out value="Price" escapeXml="false" /></th>
	      <c:set var="cols" value="${cols+1}"/>
	       <th class="nameCol">Notes</th> 
	      <c:if test="${presentation.editPrice == 'true'}">
		   <th class="nameCol"><c:out value="Remove" escapeXml="false" /></th>
		  </c:if>
	</tr>
	<c:forEach items="${presentation.product}" var="product" varStatus="status">
	  <tr class="row${status.index % 2}">
	    <c:if test="${siteConfig['PRODUCT_QUOTE'].value == 'true' and (model.showProductQuoteColumn == null or model.showProductQuoteColumn)}" >
  			<td class="nameCol" id="product${product.id}" align="center">
  			<c:if test="${product.quote}">
			 <input name="__selected_product_id" value="${product.id}" type="checkbox" onchange="addToQuoteList('${product.id}');" />	 	
  			</c:if>
  			</td>
  		</c:if>
	    <td class="nameCol" align="center">			
	        <c:out  value="${product.sku}"/>
			<input type="hidden" name="sku${status.index}" value="<c:out  value="${product.sku}"/>" />
		</td>	
    	<c:forEach items="${product.productFields}" var="productField">
		  <c:if test="${productField.showOnPresentation}">
		    <c:choose>
				<c:when test="${productField.formatType != null and productField.formatType != '' and productField.fieldType == 'number' }">
					<td class="nameCol" align="center"><fmt:formatNumber value="${wj:trimAndSplit(productField.value, '|' , false, ',', true, true)}" pattern="${productField.formatType }" /></td>
				</c:when>
			<c:otherwise>
					<td class="nameCol" align="center"><c:out value="${wj:trimAndSplit(productField.value, '|' , false, ',', true, true)}" escapeXml="false"/></td>
			</c:otherwise>
			</c:choose> 
		  </c:if>
		</c:forEach>
		<td class="nameCol" align="center">
			<c:out value="${product.inventoryAFS}"/>
		</td>
		
		<td class="nameCol" align="center">
		<c:if test="${presentation.editPrice == 'true'}">
		<c:if test="${presentation.managerUpdated == 'false' or presentation.manager == 'true'}">
		 <select onchange="document.getElementById('price${status.index}').value=this.value;">
		     <option value="0">Select Price</option>
		       <c:if test="${! empty product.priceTable1}">
	         <option value="${product.priceTable1}" <c:if test="${product.priceTable1 == product.msrp}">selected</c:if>> <fmt:message key="${siteConfig['CURRENCY'].value}" /><fmt:formatNumber value="${product.priceTable1}" pattern="#,##0.00" /> Price Table1</option>
	         </c:if>
	         <c:if test="${! empty product.priceTable2}">
	         <option value="${product.priceTable2}" <c:if test="${product.priceTable2 == product.msrp}">selected</c:if>> <fmt:message key="${siteConfig['CURRENCY'].value}" /><fmt:formatNumber value="${product.priceTable2}" pattern="#,##0.00" /> Price Table2</option>
	         </c:if>
	         <c:if test="${! empty product.priceTable3}">
	         <option value="${product.priceTable3}" <c:if test="${product.priceTable3 == product.msrp}">selected</c:if>> <fmt:message key="${siteConfig['CURRENCY'].value}" /><fmt:formatNumber value="${product.priceTable3}" pattern="#,##0.00" /> Price Table3</option>
	         </c:if>
	         <c:if test="${! empty product.priceTable4}">
	         <option value="${product.priceTable4}" <c:if test="${product.priceTable4 == product.msrp}">selected</c:if>> <fmt:message key="${siteConfig['CURRENCY'].value}" /><fmt:formatNumber value="${product.priceTable4}" pattern="#,##0.00" /> Price Table4</option>
	         </c:if>
	         <c:if test="${! empty product.priceTable5}">
	         <option value="${product.priceTable5}" <c:if test="${product.priceTable5 == product.msrp}">selected</c:if>> <fmt:message key="${siteConfig['CURRENCY'].value}" /><fmt:formatNumber value="${product.priceTable5}" pattern="#,##0.00" /> Price Table5</option>
	         </c:if>
	         <c:if test="${! empty product.priceTable6}">
	         	<option value="${product.priceTable6}" <c:if test="${product.priceTable6 == product.msrp}">selected</c:if>> <fmt:message key="${siteConfig['CURRENCY'].value}" /><fmt:formatNumber value="${product.priceTable6}" pattern="#,##0.00" /> Price Table6</option>
	         </c:if>
	         <c:if test="${! empty product.priceTable7}">
	         <option value="${product.priceTable7}" <c:if test="${product.priceTable7 == product.msrp}">selected</c:if>> <fmt:message key="${siteConfig['CURRENCY'].value}" /><fmt:formatNumber value="${product.priceTable7}" pattern="#,##0.00" /> Price Table7</option>
	         </c:if>
	         <c:if test="${! empty product.priceTable8}">
	         <option value="${product.priceTable8}" <c:if test="${product.priceTable8 == product.msrp}">selected</c:if>> <fmt:message key="${siteConfig['CURRENCY'].value}" /><fmt:formatNumber value="${product.priceTable8}" pattern="#,##0.00" /> Price Table8</option>
	         </c:if>
	         <c:if test="${! empty product.priceTable9}">
	         <option value="${product.priceTable9}" <c:if test="${product.priceTable9 == product.msrp}">selected</c:if>> <fmt:message key="${siteConfig['CURRENCY'].value}" /><fmt:formatNumber value="${product.priceTable9}" pattern="#,##0.00" /> Price Table9</option>
	         </c:if>
	         <c:if test="${! empty product.priceTable10}">
	         <option value="${product.priceTable10}" <c:if test="${product.priceTable10 == product.msrp}">selected</c:if>> <fmt:message key="${siteConfig['CURRENCY'].value}" /><fmt:formatNumber value="${product.priceTable10}" pattern="#,##0.00" /> Price Table10</option>
	         </c:if>
	   	 </select>
	   	 </c:if>
	   	 <c:choose>
	   	 <c:when test="${presentation.manager == true}">
	   	 	<input id="price${status.index}" name="price${status.index}" cssClass="textfield" size="10" maxlength="20" htmlEscape="false" value="${product.msrp}"/>
	   	 </c:when>
	   	 <c:otherwise>
	   	 	<input type="hidden" id="price${status.index}" name="price${status.index}"  cssClass="textfield" size="10" maxlength="20" htmlEscape="false" value="${product.msrp}"/>
	   	 </c:otherwise>
	   	 </c:choose>
	   </c:if>
	   <c:if test="${presentation.editPrice == 'false'}">
	   		<fmt:message key="${siteConfig['CURRENCY'].value}" /><fmt:formatNumber value="${product.msrp}" pattern="#,##0.00" />
	   </c:if>
	   </td>
	   <td class="nameCol" align="center">
			<hr width="200px" color="#fff">
	   </td>
	   <c:if test="${presentation.editPrice == 'true'}">
	   <td class="nameCol" align="center">
	   	<input name="remove${status.index}" type="checkbox" />
	   </td>
	   </c:if>
	  </tr>
	</c:forEach>
	</table> 
	<div class="presentation_edit_price_button">
	<c:if test="${presentation.editPrice == 'true'}">
		<input  type="hidden" name="__update"  value="__update"/>
	   <input class="presentation_edit_price_button" type="submit" value="Update"   name="edit"/> 
	</c:if>
	</div>
	<c:if test="${presentation.editPrice == 'true'}">
		</form>
	</c:if>
	
	<c:if test="${siteConfig['PRODUCT_QUOTE'].value == 'true'and (model.showProductQuoteColumn == null or model.showProductQuoteColumn)}" >
	<div id="quoteButton" class="quoteButton"> 
		<form action="${_contextpath}/quoteViewList.jhtm">
			<input class="add_to_quote_button" type="submit" value="Add To Quote" />
		</form>
	</div>
	</c:if>
	
</div>
<%--
<c:if test="${presentation.note != null}">
<div class="presentation_note_container" id="presentationNoteContainerId">
 	<c:out value="${presentation.note}" escapeXml="false" />
</div>
</c:if> --%>
<c:if test="${presentation.footerHtmlCode != ''}">
	<div class="presentation_template_footer_html" id="presentationTemplateFooterHtmlId">
	 	<c:out value="${presentation.footerHtmlCode}" escapeXml="false" />
	</div>
</c:if>
</body>
</html>