<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ taglib uri="/WEB-INF/tlds/custom.tld" prefix="wj" %>

<tiles:insertDefinition name="${_template}" flush="true">
  <tiles:putAttribute name="content" type="string">
  
  <!-- breadcrumb -->
  <p class="breadcrumb">
    <a href="salesRepAccount.jhtm"><fmt:message key="myAccount" /></a> &gt; <a href="presentationList.jhtm"><fmt:message key="presentations" /> </a> &gt; <c:out value="${model.account.name}"/>
  <c:if test="${model.account.name == null}"><c:out value="${model.salesRep.name}"/></c:if>
  </p>
  <c:out value="${model.salesrepLayout.headerHtml}" escapeXml="false"/>
	  
<c:if test="${not empty model.salesRep.subAccounts}">
<fieldset class="myaccount_fieldset">
<legend class="myaccount_legend">Sales Reps Tree</legend>
<c:set value="${model.salesRep}" var="modelSalesRep"/>
<div style="padding-bottom: 5px;">
<a href="presentationList.jhtm" class="salesRepMain"><c:out value="${model.salesRep.name}"/></a>
</div>
<c:set var="salesRepList" value="${model.salesRep.subAccounts}" scope="request"/>

<c:set var="level" value="0" scope="request"/>
<jsp:include page="/WEB-INF/jsp/frontend/salesRep/recursion.jsp"/>
</fieldset>
</c:if>

<form name="myPresentationList" method="post" id="list">
<input type="hidden" id="sort" name="sort" value="${templateSearch.sort}" />
<table  align="center" class="my_presentation_list" id="myPresentationListId" cellspacing="5" cellpadding="0" border="0">
  <tr><td>
<div class="salesPageNavi">Page 
		<select name="page" onchange="submit()">
		<c:forEach begin="1" end="${model.presentationList.pageCount}" var="page">
  	        <option value="${page}" <c:if test="${page == (model.presentationList.page+1)}">selected</c:if>>${page}</option>
		</c:forEach>
		</select>
of <c:out value="${model.presentationList.pageCount}"/></div>
	</td>
  <td><c:out value="Search Presentation By Name:  "/>
  	<input type="text" name="presentationName" value="<c:out value="${model.presentationName}"/>" size="30">
  </td>
  </tr>
  <tr>
	<td colspan="2">
<table border="0" width="100%" cellpadding="3" cellspacing="1"  class="my_presentation_listtings_table" id="myPresentationListtingsTableId"> 
  <tr class="listingsHdr2">
  <c:set var="cols" value="0"/>
      <c:set var="cols" value="${cols+1}"/>
       <td class="nameCol">Name</td>
      <c:set var="cols" value="${cols+1}"/>
       <td class="nameCol">Template</td>
      <c:set var="cols" value="${cols+1}"/>
       <td class="nameCol">No Of Products</td>
      <c:set var="cols" value="${cols+1}"/>
       <td class="nameCol">Products Per Page</td>
      <c:set var="cols" value="${cols+1}"/>
       <td class="nameCol"><fmt:message key="created" /></td>
      <c:set var="cols" value="${cols+1}"/>
       <td class="nameCol"><fmt:message key="lastModified" /></td>
       <c:set var="cols" value="${cols+1}"/>
       <td class="nameCol"><fmt:message key="f_noOfView" /></td>
       <c:set var="cols" value="${cols+1}"/>
       <td class="nameCol"><fmt:message key="updatedBy" /></td>
      <c:set var="cols" value="${cols+1}"/>
       <td class="nameCol"><fmt:message key="printerFriendly" /></td>
  </tr>
<c:forEach items="${model.presentationList.pageList}" var="presentation" varStatus="status">
  <tr class="row${status.index % 2}">
	<td class="nameCol">			
        <a href="addPresentation.jhtm?id=${presentation.id}" class="nameLink"><c:out value="${presentation.name}"/></a>
	</td>
	<td class="nameCol">			
        <c:out value="${presentation.template}"/>
	</td>
	<td class="nameCol">			
        <c:out value="${presentation.noOfProduct}"/>
	</td>
	<td class="nameCol">			
        <c:out value="${presentation.maxNoOfProduct}"/>
	</td>
	<td class="nameCol">			
        <fmt:formatDate type="both" timeStyle="full" pattern="MM/dd/yyyy" value="${presentation.created}"/>
	</td>
	<td class="nameCol">			
        <fmt:formatDate type="both" timeStyle="full" pattern="MM/dd/yyyy" value="${presentation.modified}"/>
	</td>
	<td class="nameCol">			
        <c:out value="${presentation.viewed}"/>
	</td>
	<td class="nameCol">			
        <c:out value="${presentation.lastUpdatedByName}"/>
	</td>
	<td class="nameCol">		
		<a href="<c:out value="${presentation.url}"/>" class="nameLink">HTML</a>
		<a href="<c:out value="${presentation.url}&view=PDF"/>" class="nameLink">PDF</a>
		<c:if test="${presentation.editUrl != null}">
			<a href="<c:out value="${presentation.editUrl}"/>" class="nameLink"><fmt:message key="edit" /></a>
		</c:if>
	 </td>
  </tr>
</c:forEach>
   <tr>
	  <td>
	  	<FORM>
			<INPUT type="button" value="<fmt:message key="f_newPresentation" />" onClick="location.href='addPresentation.jhtm'" class="presentation_new_button" id="presentationNewButtonId">
		</FORM>
	  </td>
	  <td></td>
	  <td></td>
	  <td>
	  </td>
</tr>
</table>
	</td>
  </tr>
</table>
</form>

<c:out value="${model.salesrepLayout.footerHtml}" escapeXml="false"/>


  </tiles:putAttribute>
</tiles:insertDefinition>
