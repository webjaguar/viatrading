<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ taglib uri="/WEB-INF/tlds/custom.tld" prefix="wj" %>

<tiles:insertDefinition name="${_template}" flush="true">
  <tiles:putAttribute name="content" type="string">

<!-- main box -->
  <div id="mboxfullpopup">
      <div class="title">
        <span class="mboxfullpopup_title">Add Product To Presentation</span>
      </div>
   <form action="${_contextpath}/addToPresentation.jhtm" method="post">
	<fieldset class="top">
	<table>
		<tr>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
			<td><label class="AStitle"><fmt:message key="productName" />:</label></td>
			<td>&nbsp;</td>
			<td><label class="AStitle"><fmt:message key="productSku" />:</label></td>
		</tr>
		<c:forEach items="${model.productPresentationForm.product}" var="product">
		<tr>
			<td align="center"><input type="hidden" name="__selected_sku" value="${product.sku}"></td>
			<td>&nbsp;</td>
			<td><c:out value="${product.name}" /></td>
			<td>&nbsp;</td>
			<td><c:out value="${product.sku}" /></td>
		</tr>
		</c:forEach>
	</table>
	</fieldset>
	<c:if test="${model.productPresentationForm.added == 'false'}">
		<div class="presentation_detail_list">
		<c:if test="${! empty model.productPresentationForm.presentationList}">
		<fieldset>
		 <table id="presentationListId">
			<tr>
				<td><div class="presentation_header"></div></td>
				<td><div class="presentation_header"><fmt:message key="f_presentationName" /></div></td>
				<td><div class="presentation_header"><fmt:message key="f_presentationTemplate" /></div></td>
				<td><div class="presentation_header"><c:out value="Current" /></div></td>
			</tr>
			<c:forEach items="${model.productPresentationForm.presentationList}" var="presentationList">
		      <div class="list${classIndex % 2}"><c:set var="classIndex" value="${classIndex+1}" />
		        <tr>
		         <td><input type="checkbox" name="presentationId" value="${presentationList.id}" ></td>
		         <td><div class="presentation_name"><c:out value="${presentationList.name   }" /></div></td>
		         <td><div class="presentation_template"><c:out value="${presentationList.template}" /></div></td>
		         <td><div class="presentation_no_of_oroduct"><c:out value="${presentationList.noOfProduct}" /></div></td>
		        </tr>
		      </div>
		    </c:forEach>
		  </table>
		 </fieldset>
		</c:if>
		</div>
		<c:if test="${! empty model.productPresentationForm.presentationList}">
		<fieldset>
			<div id="button">
				<input type="submit" value="<fmt:message key="addProductToPresentation"/>"/>
			</div>
		</fieldset>
		</c:if>
		<c:if test="${empty model.productPresentationForm.presentationList}">
		<fieldset>
			<div id="noPresentationAvailableId">
				<c:out value="No Presentation Available."/>
			</div>
		</fieldset>
		</c:if>
		</c:if>
		<c:if test="${model.productPresentationForm.added == 'true'}">
		<fieldset>
			<div id="addedToPresentationId">
				<c:out value="Added To Presentation"/><br>
				<a href="<c:out value="${_contextpath}/presentationList.jhtm"/> " target="_parent"  class="nameLink"> View Presentation List</a>
			</div>
		</fieldset>
		</c:if>
	</form>
  
<!-- end main box -->  
  </div>

  </tiles:putAttribute>
</tiles:insertDefinition>