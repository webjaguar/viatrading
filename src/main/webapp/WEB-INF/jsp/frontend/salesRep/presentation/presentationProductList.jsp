<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ taglib uri="/WEB-INF/tlds/custom.tld" prefix="wj" %>

<tiles:insertDefinition name="${_template}" flush="true">
  <tiles:putAttribute name="content" type="string">

  <!-- breadcrumb -->
  <p class="breadcrumb">
    <a href="salesRepAccount.jhtm"><fmt:message key="myAccount" /></a> &gt; <a href="listPresentationProduct.jhtm">Products In Presentation List</a>
  </p>

<c:if test="${gSiteConfig['gPRESENTATION']}">
<c:out value="${model.salesrepLayout.headerHtml}" escapeXml="false"/>

<c:if test="${model.products.nrOfElements > 0}">
<script language="JavaScript">
<!--
function toggleAll(el) {
	var ids = document.getElementsByName("__selected_sku");	
  	if (ids.length == 1)
      document.myPresentation.__selected_sku.checked = el.checked;
    else
      for (i = 0; i < ids.length; i++)
        document.myPresentation.__selected_sku[i].checked = el.checked;	
}  

function deleteSelected() {
	var ids = document.getElementsByName("__selected_sku");	
  	if (ids.length == 1) {
      if (document.myPresentation.__selected_sku.checked) {
    	return confirm('Delete permanently?');
      }
    } else {
      for (i = 0; i < ids.length; i++) {
        if (document.myPresentation.__selected_sku[i].checked) {
    	  return confirm('Delete permanently?');
        }
      }
    }

    alert("Please select product(s) to delete.");       
    return false;
}

function submitform()
{
  document.myPresentation.submit();
}
-->
</script>
</c:if>
<c:forEach items="${model.addPresentationError}" var="error" varStatus="status">
	<c:out value="${error}"/><br/>
</c:forEach>
<div class="presentation_continue_browsing_div">
<c:choose>
<c:when test="${model.continueShoppingUrl != null and !empty model.continueShoppingUrl and model.continueShoppingUrl != '/home.jhtm?null'}"><a class="presentation_continue_browsing" href="<c:out value='${model.continueShoppingUrl}' />"><fmt:message key="continueShopping" /></a></c:when>
<c:otherwise><a href="javascript:history.back(1);"><fmt:message key="continueBrowsing" /></a></c:otherwise>
</c:choose>
</div>
<table width="100%" align="center" class="my_presentation" id="myPresentationId" cellspacing="5" cellpadding="0" border="0">
<form name="myPresentation" method="post" id="list">
<input type="hidden" id="sort" name="sort" value="id desc" />
  <tr>
	<td>&nbsp;</td>
	<td>
<div class="salesPageNavi">Page 
		<select name="page" onchange="submit()">
		<c:forEach begin="1" end="${model.products.pageCount}" var="page">
  	        <option value="${page}" <c:if test="${page == (model.products.page+1)}">selected</c:if>>${page}</option>
		</c:forEach>
		</select>
of <c:out value="${model.products.pageCount}"/></div>
	</td>
  </tr>
  <tr>
<table border="0" cellpadding="3" cellspacing="1" width="100%" class="my_presentation_listings"> 
  <tr class="listingsHdr2">
  <c:set var="cols" value="0"/>
<c:if test="${model.products.nrOfElements > 0}">
	<c:set var="cols" value="${cols+1}"/>
    <td align="center"><input type="checkbox" onclick="toggleAll(this)"></td>
</c:if>
      <c:set var="cols" value="${cols+1}"/>
       <td class="nameCol">Image</td>
      <c:set var="cols" value="${cols+1}"/>
		<c:choose>
			<c:when test="${model.sort == 'sku desc'}">
				<td class="nameCol"><a class="hdrLink" href="#"
					onClick="document.getElementById('sort').value='sku';document.getElementById('list').submit()"><fmt:message
					key="sku" /></a></td>
			</c:when>
			<c:when test="${model.sort == 'sku'}">
				<td class="nameCol"><a class="hdrLink" href="#"
					onClick="document.getElementById('sort').value='sku desc';document.getElementById('list').submit()"><fmt:message
					key="sku" /></a></td>
			</c:when>
			<c:otherwise>
				<td class="nameCol"><a class="hdrLink"
					href="#"
					onClick="document.getElementById('sort').value='sku';document.getElementById('list').submit()"><fmt:message
					key="sku" /></a></td>
			</c:otherwise>
		</c:choose>
	 <c:set var="cols" value="${cols+1}"/>
       <td class="nameCol">Name</td>
      <c:set var="cols" value="${cols+1}"/>
       <td class="nameCol">Short Description</td>
  </tr>
<c:forEach items="${model.products.pageList}" var="product" varStatus="status">
  <tr class="row${status.index % 2}">
    <td align="center"><input name="__selected_sku" value="${product.sku}" type="checkbox"></td>
    <td class="nameCol">
	  <div align="center">
	    <c:if test="${product.product.thumbnail.imageUrl != null}">
	  	   <img src="${_contextpath}/assets/Image/Product/thumb/${product.product.thumbnail.imageUrl}" border="0" class="presentation_product_image" class="presentationProductImageId"
	  	   	<c:if test="${siteConfig['ALSO_CONSIDER_THUMB_HEIGHT'].value != ''}">
	  	   		height="<c:out value="${siteConfig['ALSO_CONSIDER_THUMB_HEIGHT'].value}"/>
	  	   	</c:if>
	  	   ></img>
	    </c:if>
	  </div>
	</td>
	<td class="nameCol">
	</td>
    <td class="nameCol">			
        <c:out value="${product.sku}"/>
	</td>	
	<td class="nameCol">			
        <c:out value="${product.product.name}"/>
    </td>
	<td class="nameCol">
	<c:if test="${! empty product.product.shortDesc}">		
        <c:out value="${product.product.shortDesc}"/>
    </c:if>	
	<c:if test="${empty product.product.shortDesc}">		
        <c:out value=" "/>
    </c:if>
	</td>
  </tr>
</c:forEach>
<c:choose>
 <c:when test="${model.products.nrOfElements == 0}">
   <tr class="emptyList"><td colspan="${cols}">&nbsp;</td></tr>
 </c:when>
 <c:otherwise>
   <tr>
	  <td></td>
	  <td>
	   <select name="_presentationId">
	     <option value="0">Select Presentation</option>    
	    <c:forEach items="${model.presentationList}" var="presentation">
	     <c:if test="${presentation.id != model.viewPresentationId}">
		  <option value="${presentation.id}"><c:out value="${presentation.name}" /></option>
		 </c:if>    
	    </c:forEach>
	   </select>
	  </td>
	  
	  <td><input type="submit" value="<fmt:message key="f_addToPresentation" />" name="_addToPresentation" class="presentation_add_button" id="presentationAddButtonId"></td>
	  <td><input type="submit" value="<fmt:message key="f_removeFromPresentation" />" name="_removeFromList" class="presentation_remove_product_button" id="presentationRemoveProductButtonId"></td>
	  
	</tr>
	
 </c:otherwise>
</c:choose>
</table>
  </tr>
  
</form>
<tr>
	<td class="presentation_buttons">
	  	<FORM>
			<INPUT type="button" value="<fmt:message key="f_newPresentation" />" onClick="location.href='addPresentation.jhtm'" class="presentation_new_button" id="presentationNewButtonId">
		</FORM>
	</td>
	<td>
	  	<FORM>
			<INPUT type="button" value="<fmt:message key="f_searchPresentation" />" onClick="location.href='presentationList.jhtm'" class="presentation_view_button" id="presentationViewButtonId">
		</FORM>
	</td>
</tr>
</table>

</c:if>

  </tiles:putAttribute>
</tiles:insertDefinition>
