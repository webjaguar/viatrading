<%@ page import="com.webjaguar.model.*,org.springframework.web.bind.ServletRequestUtils"%>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="/WEB-INF/tlds/spring.tld" prefix="spring" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>

<tiles:insertDefinition name="${_template}" flush="true">
  <tiles:putAttribute name="content" type="string">
  
  <!-- breadcrumb -->
	  <p class="breadcrumb">
	    <a href="salesRepAccount.jhtm"><fmt:message key="myAccount" /></a> &gt; <fmt:message key="customers" />  &gt; <c:out value="${model.account.name}"/>
	  <c:if test="${model.account.name == null}"><c:out value="${model.salesRep.name}"/></c:if>
	  </p>

<c:if test="${fn:trim(model.salesrepLayout.headerHtml) != ''}">
  <c:set value="${model.salesrepLayout.headerHtml}" var="headerHtml"/>
  <c:set value="${fn:replace(headerHtml, '#salesRepName#', model.salesRep.name)}" var="headerHtml"/>
  <c:set value="${fn:replace(headerHtml, '#salesRepEmail#', model.salesRep.email)}" var="headerHtml"/>
  <c:set value="${fn:replace(headerHtml, '#salesRepPhone#', model.salesRep.phone)}" var="headerHtml"/>
  <c:set value="${fn:replace(headerHtml, '#salesRepCellPhone#', model.salesRep.cellPhone)}" var="headerHtml"/>
  <c:set value="${fn:replace(headerHtml, '#salesRepAddress1#', model.salesRep.address.addr1)}" var="headerHtml"/>
  <c:set value="${fn:replace(headerHtml, '#salesRepAddress2#', model.salesRep.address.addr2)}" var="headerHtml"/>
  <c:set value="${fn:replace(headerHtml, '#salesRepCity#', model.salesRep.address.city)}" var="headerHtml"/>
  <c:set value="${fn:replace(headerHtml, '#salesRepCountry#', model.salesRep.address.country)}" var="headerHtml"/>
  <c:set value="${fn:replace(headerHtml, '#salesRepStateProvince#', model.salesRep.address.stateProvince)}" var="headerHtml"/>
  <c:set value="${fn:replace(headerHtml, '#salesRepZip#', model.salesRep.address.zip)}" var="headerHtml"/>
  <c:set value="${fn:replace(headerHtml, '#salesRepFax#', model.salesRep.address.fax)}" var="headerHtml"/>
  <c:set value="${fn:replace(headerHtml, '#salesRepCompany#', model.salesRep.address.company)}" var="headerHtml"/>
  <c:out value="${headerHtml}" escapeXml="false"/>
</c:if>

<script type="text/javascript">
function toggleAll(el) {
	var ids = document.getElementsByName("__selected_id");	
  	if (ids.length == 1)
      document.list_form.__selected_id.checked = el.checked;
    else
      for (i = 0; i < ids.length; i++)
        document.list_form.__selected_id[i].checked = el.checked;	
} 
function optionsSelected() {
	var ids = document.getElementsByName("__selected_id");	
	if (ids.length == 1) {
      if (document.list_form.__selected_id.checked) {
      	return true;
      }
    } else {
      for (i = 0; i < ids.length; i++) {
        if (document.list_form.__selected_id[i].checked) {
        	return true;
        }
      }
    }
}
</script>
<style type="text/css">
.sortLink {
	color: #990000;
	text-decoration: none;
	}
.sortLink:hover {
	text-decoration: underline;
	}
.sortLinkOff {
	color: #FFFFFF;
	text-decoration: none;
	}
.sortLinkOff:hover {
	text-decoration: underline;
	}
.rowOff {
	background: #FFCCCC
	}
.loginAs {
	color: #003366;
	text-decoration: none;
	}
.loginAs:hover {
	text-decoration: underline;
	}
.salesRepLink {
	color: #003366;
	text-decoration: none;
	}
.salesRepLink:hover {
	text-decoration: underline;
	}
.salesRepLinkSelected {
	color: #003366;
	text-decoration: underline;
	font-weight: bold;
	}
.salesRepMain {
	color: #990000;
	text-decoration: none;
	font-weight: bold;
	}
.salesRepMain:hover {
	text-decoration: underline;
	}
</style>  

<c:if test="${not empty model.salesRep.subAccounts}">
<fieldset class="myaccount_fieldset">
<legend class="myaccount_legend">Sales Reps</legend>
<c:set value="${model.salesRep}" var="modelSalesRep"/>
<div style="padding-bottom: 5px;">
<a href="salesRep.jhtm" class="salesRepMain"><c:out value="${model.salesRep.name}"/></a>
</div>
<c:set var="salesRepList" value="${model.salesRep.subAccounts}" scope="request"/>

<c:set var="level" value="0" scope="request"/>
<jsp:include page="/WEB-INF/jsp/frontend/salesRep/recursion.jsp"/>
</fieldset>
</c:if>

<%@ include file="/WEB-INF/jsp/frontend/jbd/repSales.jsp" %>

<form id="list" method="post" name="list_form">
<input type="hidden" id="sort" name="sort" value="${model.sort}" />
<c:if test="${model.count > 0 and 'EVERGREEN' == gSiteConfig['gACCOUNTING']}">
  <div class="sendEmail">
	<input type="submit" value="send email" name="__sendEmail" onClick="optionsSelected();"></input>
  </div>
</c:if>
<table width="90%" align="center" cellspacing="5" cellpadding="0" border="0">
  <tr>
	<td>&nbsp;</td>
	<td>
<div class="pageNavi">Page 
		<select id="page" name="page" onchange="submit()">
		<c:forEach begin="1" end="${model.customers.pageCount}" var="page">
  	        <option value="${page}" <c:if test="${page == (model.customers.page+1)}">selected</c:if>>${page}</option>
		</c:forEach>
		</select>
of <c:out value="${model.customers.pageCount}"/>

<select name="size" onchange="document.getElementById('page').value=1;submit()">
<c:forTokens items="10,25,50,100,500" delims="," var="current">
 <option value="${current}" <c:if test="${current == model.customers.pageSize}">selected</c:if>>${current} <fmt:message key="perPage" /></option>
</c:forTokens>
</select>
</div>
	</td>
  </tr>
  <tr>
	<td colspan="2">
<table border="0" cellpadding="3" cellspacing="1" width="100%" class="listings"> 
  <tr class="listingsHdr2">
  	  <c:if test="${model.count > 0 and 'EVERGREEN' == gSiteConfig['gACCOUNTING']}">
	    <td align="left"><input type="checkbox" onclick="toggleAll(this)"></td>
      </c:if>
      <c:choose>
   	      <c:when test="${model.sort == 'company desc'}">
   	        <td class="nameCol">
   	        <a class="sortLink" href="#" onClick="document.getElementById('sort').value='company';document.getElementById('list').submit()"><fmt:message key="companyName" /></a>
   	        </td>
   	      </c:when>
   	      <c:when test="${model.sort == 'company'}">
   	        <td class="nameCol">
   	        <a class="sortLink" href="#" onClick="document.getElementById('sort').value='company desc';document.getElementById('list').submit()"><fmt:message key="companyName" /></a>
   	        </td>
   	      </c:when>
   	      <c:otherwise>
   	        <td class="nameCol">
   	        <a class="sortLinkOff" href="#" onClick="document.getElementById('sort').value='company desc';document.getElementById('list').submit()"><fmt:message key="companyName" /></a>
   	        </td>
   	      </c:otherwise>
   	  </c:choose>  	  
  	  <c:choose>
   	      <c:when test="${model.sort == 'account_number desc'}">
   	        <td align="center">
   	        <a class="sortLink" href="#" onClick="document.getElementById('sort').value='account_number';document.getElementById('list').submit()"><fmt:message key="accountNumber" /></a>
   	        </td>
   	      </c:when>
   	      <c:when test="${model.sort == 'account_number'}">
   	        <td align="center">
   	        <a class="sortLink" href="#" onClick="document.getElementById('sort').value='account_number desc';document.getElementById('list').submit()"><fmt:message key="accountNumber" /></a>
   	        </td>
   	      </c:when>
   	      <c:otherwise>
   	        <td align="center">
   	        <a class="sortLinkOff" href="#" onClick="document.getElementById('sort').value='account_number';document.getElementById('list').submit()"><fmt:message key="accountNumber" /></a>
   	        </td>
   	      </c:otherwise>
   	  </c:choose> 
      <c:choose>
   	      <c:when test="${model.sort == 'username desc'}">
   	        <td class="nameCol">
   	        <a class="sortLink" href="#" onClick="document.getElementById('sort').value='username';document.getElementById('list').submit()"><fmt:message key="email" /></a>
   	        </td>
   	      </c:when>
   	      <c:when test="${model.sort == 'username'}">
   	        <td class="nameCol">
   	        <a class="sortLink" href="#" onClick="document.getElementById('sort').value='username desc';document.getElementById('list').submit()"><fmt:message key="email" /></a>
   	        </td>
   	      </c:when>
   	      <c:otherwise>
   	        <td class="nameCol">
   	        <a class="sortLinkOff" href="#" onClick="document.getElementById('sort').value='username';document.getElementById('list').submit()"><fmt:message key="email" /></a>
   	        </td>
   	      </c:otherwise>
   	  </c:choose>  	  
      <c:choose>
   	      <c:when test="${model.sort == 'num_of_logins desc'}">
   	        <td class="nameCol">
   	        <a class="sortLink" href="#" onClick="document.getElementById('sort').value='num_of_logins';document.getElementById('list').submit()"><fmt:message key="numOfLogins" /></a>
   	        </td>
   	      </c:when>
   	      <c:when test="${model.sort == 'num_of_logins'}">
   	        <td class="nameCol">
   	        <a class="sortLink" href="#" onClick="document.getElementById('sort').value='num_of_logins desc';document.getElementById('list').submit()"><fmt:message key="numOfLogins" /></a>
   	        </td>
   	      </c:when>
   	      <c:otherwise>
   	        <td class="nameCol">
   	        <a class="sortLinkOff" href="#" onClick="document.getElementById('sort').value='num_of_logins desc';document.getElementById('list').submit()"><fmt:message key="numOfLogins" /></a>
   	        </td>
   	      </c:otherwise>
   	  </c:choose>
   	  <td align="center">Web <fmt:message key="orderCount" /></td>
   	  <td align="center">Web <fmt:message key="orderTotal" /></td>
 	  <td class="nameCol">&nbsp;</td>
  </tr>
<c:forEach items="${model.customers.pageList}" var="customer" varStatus="status">
  <tr class="<c:choose><c:when test="${userSession.userid == customer.id}">rowOff</c:when><c:otherwise>row${status.index % 2}</c:otherwise></c:choose>">
    <c:if test="${'EVERGREEN' == gSiteConfig['gACCOUNTING']}">
    <td align="left"><input name="__selected_id" value="${customer.id}" type="checkbox" ></td>
    </c:if>
	<td class="nameCol"><c:out value="${customer.address.company}"/></td>
    <td align="center"><c:out value="${customer.accountNumber}"/></td>
    <td class="nameCol"><c:out value="${customer.username}"/></td>
    <td align="center"><c:out value="${customer.numOfLogins}"/></td>	
    <td align="center"><c:out value="${customer.orderCount}"/></td>
    <td align="right"><fmt:formatNumber value="${customer.ordersGrandTotal}" pattern="#,##0.00" /></td>
    <c:if test="${userSession.userid != customer.id}">
  	<td align="center"><a href="srLoginAsCustomer.jhtm?tc=salesRep&cid=${customer.token}" class="loginAs"><fmt:message key="loginAsCustomer" /></a></td>
    </c:if>		
    <c:if test="${userSession.userid == customer.id}">
  	<td align="center"><fmt:message key="loginAsCustomer" /></td>
    </c:if>		
  </tr>
</c:forEach>
<c:if test="${model.customers.nrOfElements == 0}">
  <tr class="emptyList"><td colspan="7">&nbsp;</td></tr>
</c:if>
</table>
	</td>
  </tr>
</table>
</form>

<c:if test="${fn:trim(model.salesrepLayout.footerHtml) != ''}">
  <c:set value="${model.salesrepLayout.footerHtml}" var="footerHtml"/>
  <c:set value="${fn:replace(footerHtml, '#salesRepName#', model.salesRep.name)}" var="footerHtml"/>
  <c:set value="${fn:replace(footerHtml, '#salesRepEmail#', model.salesRep.email)}" var="footerHtml"/>
  <c:set value="${fn:replace(footerHtml, '#salesRepPhone#', model.salesRep.phone)}" var="footerHtml"/>
  <c:set value="${fn:replace(footerHtml, '#salesRepCellPhone#', model.salesRep.cellPhone)}" var="footerHtml"/>
  <c:set value="${fn:replace(footerHtml, '#salesRepAddress1#', model.salesRep.address.addr1)}" var="footerHtml"/>
  <c:set value="${fn:replace(footerHtml, '#salesRepAddress2#', model.salesRep.address.addr2)}" var="footerHtml"/>
  <c:set value="${fn:replace(footerHtml, '#salesRepCity#', model.salesRep.address.city)}" var="footerHtml"/>
  <c:set value="${fn:replace(footerHtml, '#salesRepCountry#', model.salesRep.address.country)}" var="footerHtml"/>
  <c:set value="${fn:replace(footerHtml, '#salesRepStateProvince#', model.salesRep.address.stateProvince)}" var="footerHtml"/>
  <c:set value="${fn:replace(footerHtml, '#salesRepZip#', model.salesRep.address.zip)}" var="footerHtml"/>
  <c:set value="${fn:replace(footerHtml, '#salesRepFax#', model.salesRep.address.fax)}" var="footerHtml"/>
  <c:set value="${fn:replace(footerHtml, '#salesRepCompany#', model.salesRep.address.company)}" var="footerHtml"/>
  <c:out value="${footerHtml}" escapeXml="false"/>
</c:if>

  </tiles:putAttribute>
</tiles:insertDefinition>