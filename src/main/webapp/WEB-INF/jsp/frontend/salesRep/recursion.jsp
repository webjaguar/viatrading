<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<c:forEach var="salesRep" items="${salesRepList}">
	<div style="padding-bottom:5px;padding-left: ${20 +level*20}px">
	<c:if test="${model.currentPage == 'customer'}">
	  <a href="salesRep.jhtm?id=${salesRep.id}" class="salesRepLink<c:if test="${salesRep.id == paramId}">Selected</c:if>">
	    <c:out value="${salesRep.name}"/>
	  </a>
	</c:if>
	<c:if test="${model.currentPage == 'presentation'}">
	  <a href="presentationList.jhtm?id=${salesRep.id}" class="salesRepLink<c:if test="${salesRep.id == paramId}">Selected</c:if>">
	    <c:out value="${salesRep.name}"/>
	  </a>
	</c:if>
	<c:if test="${model.currentPage == null}">
	  <a href="salesRep.jhtm?id=${salesRep.id}" class="salesRepLink<c:if test="${salesRep.id == paramId}">Selected</c:if>">
	    <c:out value="${salesRep.name}"/>
	  </a>
	</c:if>
	</div>
	
	<c:if test="${salesRep.subAccounts != null and fn:length(salesRep.subAccounts) gt 0}">
    	<c:set var="level" value="${level + 1}" scope="request"/>
    	<c:set var="salesRepList" value="${salesRep.subAccounts}" scope="request"/>
	    <jsp:include page="/WEB-INF/jsp/frontend/salesRep/recursion.jsp"/>
	    <c:set var="level" value="${level - 1}" scope="request"/>
    </c:if>
</c:forEach>