<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>

<tiles:insertDefinition name="${_template}" flush="true">
<c:if test="${siteConfig['LEFTBAR_DEFAULT_TYPE'].value == '3' }">
  <c:set value="false" var="hideMootools" />
</c:if> 
  <tiles:putAttribute name="leftbar" value="/WEB-INF/jsp/frontend/layout/template${_leftBar}/leftbar.jsp" />
  <tiles:putAttribute name="content" type="string">
  
<c:out value="${model.productSearchLayout.headerHtml}" escapeXml="false"/>    
  
<script type="text/javascript">
<!--
function checkNumber( aNumber )
{
	var goodChars = "0123456789";
	var i = 0;
	if ( aNumber == "" )
		return 0; //empty
	
	for ( i = 0 ; i <= aNumber.length - 1 ; i++ )
	{
		if ( goodChars.indexOf( aNumber.charAt(i) ) == -1 ) 
			return -1; //invalid number			
	}	
	return 1; //valid number
}
function checkForm()
{
	var productList = document.getElementsByName('product.id');
	var allQtyEmpty = true;
	for(var i=0; i<productList.length; i++) 
	{
	    var el = document.getElementById("quantity_" + productList[i].value);
		if ( el != null )
		{
			if ( checkNumber( el.value ) == 1  )
			{
				allQtyEmpty = false;
			}
			else if ( checkNumber( el.value ) == -1 ) 
			{
				alert("invalid Quantity");
				el.focus();
				return false;
			}			
		}	
	}
	if ( allQtyEmpty )
	{
		alert("Please Enter Quantity.");
		return false;
	}
	else
		return true;	
}
//-->
</script>
<%-- bread crumbs --%>
<c:import url="/WEB-INF/jsp/frontend/layout/${_template}/breadcrumbs.jsp" />
<form action="${model.searchAction}" method="get" name="search">
<div class="searchFormContainer">
<div class="searchFormHeader"></div>
<div class="searchForm">
<input type="hidden" name="cid" value="${param.cid}" />
<c:if test="${model.searchTree == null}">
<input type="hidden" name="category" value="<c:out value="${frontProductSearch.category}"/>" />
<input type="hidden" name="includeSubs" value="${frontProductSearch.includeSubs}" />
</c:if>
<%--If there is custom search, redirect to custom search page --%>
<c:choose>
<c:when test="${siteConfig['CUSTOM_SEARCH_JSP'].value == 'true' }">
	<c:import url="/WEB-INF/jsp/frontend/searchCustom.jsp" />
</c:when>
<c:otherwise>	
	<c:import url="/WEB-INF/jsp/frontend/layout/${_template}/searchBox.jsp" />
</c:otherwise>
</c:choose>
</div>
</div>
</form>

<c:if test="${model.minCharsMet}">
<div class="searchTitle"><fmt:message key="f_searchResults"/>:</div>

<c:if test="${model.count == 0}"><div class="searchNotFound"><fmt:message key="f_nothingFound"/></div></c:if>
</c:if>
<c:if test="${model.count > 0}">

<c:set var="paramView" value=""/>
<c:choose>
  <c:when test="${param.view != null and !empty param.view}">
    <c:set var="paramView" value="${param.view}"/>
  </c:when>
  <c:otherwise>
    <c:set var="paramView" value="${siteConfig['SEARCH_DISPLAY_MODE'].value}"/>
  </c:otherwise>
</c:choose>

<div id="searchWrapperBox" >
<c:set var="pageShowing">
<form action="${model.searchAction}" method="get">
<input type="hidden" id="view" name="view" value="" />
<table border="0" cellpadding="0" cellspacing="0" width="100%">
  <tr>
  <td class="pageShowing">
  <fmt:message key="showing">
	<fmt:param value="${frontProductSearch.offset + 1}"/>
	<fmt:param value="${model.pageEnd}"/>
	<fmt:param value="${model.count}"/>
  </fmt:message>
  </td>
  <td class="pageNavi">
	<input type="hidden" name="view" value="<c:out value='${param.view}' />" />
	Page 
	<c:choose>
	<c:when test="${model.pageCount <= 100 and model.pageCount > 0}">
		    <select name="page" id="page" onchange="submit()">
			<c:forEach begin="1" end="${model.pageCount}" var="page">
	  	        <option value="${page}" <c:if test="${page == (frontProductSearch.page)}">selected</c:if>>${page}</option>
			</c:forEach>
			</select>
	</c:when>
	<c:otherwise>
			<input type="text" id="page" name="page" value="${frontProductSearch.page}" size="5" class="textfield50" />
			<input type="submit" value="go"/>
	</c:otherwise>
	</c:choose>
	of <c:out value="${model.pageCount}"/>
	| 
	<c:if test="${frontProductSearch.page == 1}"><span class="pageNaviDead"><fmt:message key="f_previous" /></span></c:if>
	<c:if test="${frontProductSearch.page != 1}"><a href="${model.searchAction}?page=${frontProductSearch.page-1}&view=<c:out value='${paramView}' />" class="pageNaviLink"><fmt:message key="f_previous" /></a></c:if>
	| 
	<c:if test="${frontProductSearch.page == model.pageCount}"><span class="pageNaviDead"><fmt:message key="f_next" /></span></c:if>
	<c:if test="${frontProductSearch.page != model.pageCount}"><a href="${model.searchAction}?page=${frontProductSearch.page+1}&view=<c:out value='${paramView}' />" class="pageNaviLink"><fmt:message key="f_next" /></a></c:if>
    </td>
  </tr>
</table>
<c:if test="${siteConfig['PRODUCT_SORT_BY_FRONTEND'].value == 'true'}">
<table id="sort_box" border="0" cellpadding="0" cellspacing="0" width="100%">
<tr>
  <td id="sortby"><span><fmt:message key="f_sortby"/></span>
    <select id="" name="sort" onchange="document.getElementById('view').value='${paramView}';submit();">
      <option value="0">${model.sortBy}</option>
      <option value="10" <c:if test="${frontProductSearch.sort == 'name'}">selected="selected"</c:if>>Name</option>
      <option value="20" <c:if test="${frontProductSearch.sort == 'price_1 ASC'}">selected="selected"</c:if>>Price ($ to $$)</option>
      <option value="30" <c:if test="${frontProductSearch.sort == 'price_1 DESC'}">selected="selected"</c:if>>Price ($$ to $)</option>
      <c:if test="${siteConfig['PRODUCT_RATE'].value =='true' or gSiteConfig['gPRODUCT_REVIEW']}">
        <option value="40" <c:if test="${frontProductSearch.sort == 'rate DESC'}">selected="selected"</c:if>>Rating</option>
      </c:if>  
    </select>
  </td>
  <td id="pagesize"><span><fmt:message key="f_pageSize"/></span>
  <select name="size" onchange="document.getElementById('page').value=1;document.getElementById('view').value='${paramView}';submit();">   
    <option value="" ></option>
	<c:forTokens items="${siteConfig['PRODUCT_PAGE_SIZE'].value}" delims="," var="current">
	  <option value="${current}" <c:if test="${current == frontProductSearch.pageSize}">selected</c:if>>${current} <fmt:message key="perPage" /></option>
	</c:forTokens>
  </select>
  </td>
</tr>
</table>
</c:if>
</form>
</c:set>
<c:out value="${pageShowing}" escapeXml="false" />
</c:if>

<form action="${_contextpath}/addToCart.jhtm" method="post" onsubmit="return checkForm()">
<c:choose>
<c:when test="${siteConfig['GROUP_SEARCH'].value != 'true' or !fn:contains(model.searchAction,'gsearch.jhtm')}">

<c:forEach items="${model.products}" var="product" varStatus="status">

<c:set var="productLink">${_contextpath}/product.jhtm?id=${product.id}<c:if test="${model.product == null or siteConfig['RECOMMENDED_LIST_SHOW_BREADCRUMB'].value == 'true'}">&cid=<c:out value="${param.cid}"/></c:if></c:set>
<c:if test="${gSiteConfig['gMOD_REWRITE'] == '1' and not fn:contains(product.sku, '/')}">
  <c:set var="productLink">${_contextpath}/${siteConfig['MOD_REWRITE_PRODUCT'].value}/${product.encodedSku}/${product.encodedName}.html<c:if test="${model.product == null or siteConfig['RECOMMENDED_LIST_SHOW_BREADCRUMB'].value == 'true'}">?cid=<c:out value="${param.cid}"/></c:if></c:set>
</c:if>

<c:set var="numCul" value="${siteConfig['NUMBER_PRODUCT_PER_ROW'].value}" />
<c:choose>
 <c:when test="${(paramView == '1') or ( empty paramView and siteConfig['SEARCH_DISPLAY_MODE'].value == '1')}">
    <%@ include file="/WEB-INF/jsp/frontend/thumbnail/view1.jsp" %>
 </c:when>
 <c:when test="${(paramView == '2') or ( empty paramView and siteConfig['SEARCH_DISPLAY_MODE'].value == '2')}">
    <%@ include file="/WEB-INF/jsp/frontend/thumbnail/view2.jsp" %>
 </c:when>
 <c:when test="${(paramView == '3') or ( empty paramView and siteConfig['SEARCH_DISPLAY_MODE'].value == '3')}">
	<%@ include file="/WEB-INF/jsp/frontend/thumbnail/view3.jsp" %>
 </c:when>
 <c:when test="${(paramView == '4') or ( empty paramView and siteConfig['SEARCH_DISPLAY_MODE'].value == '4')}">
	<%@ include file="/WEB-INF/jsp/frontend/thumbnail/view4.jsp" %>
 </c:when>
 <c:when test="${(paramView == '5') or ( empty paramView and siteConfig['SEARCH_DISPLAY_MODE'].value == '5')}">
	<%@ include file="/WEB-INF/jsp/frontend/thumbnail/view5.jsp" %>
 </c:when>
 <c:when test="${(paramView == '6') or ( empty paramView and siteConfig['SEARCH_DISPLAY_MODE'].value == '6')}">
	<%@ include file="/WEB-INF/jsp/frontend/thumbnail/view6.jsp" %>
 </c:when>
 <c:when test="${(paramView == '7') or ( empty paramView and siteConfig['SEARCH_DISPLAY_MODE'].value == '7')}">
	<%@ include file="/WEB-INF/jsp/frontend/thumbnail/view7.jsp" %>
 </c:when>
 <c:when test="${(paramView == '7-1') or ( empty paramView and siteConfig['SEARCH_DISPLAY_MODE'].value == '7-1')}">
	<%@ include file="/WEB-INF/jsp/frontend/thumbnail/view7-1.jsp" %>
 </c:when>
 <c:when test="${(paramView == '8') or ( empty paramView and siteConfig['SEARCH_DISPLAY_MODE'].value == '8')}">
	<%@ include file="/WEB-INF/jsp/frontend/thumbnail/view8.jsp" %>
 </c:when>
 <c:when test="${(paramView == '8-1') or ( empty paramView and siteConfig['SEARCH_DISPLAY_MODE'].value == '8-1')}">
	<%@ include file="/WEB-INF/jsp/frontend/thumbnail/view8-1.jsp" %>
 </c:when>
 <c:when test="${(paramView == '9') or ( empty paramView and siteConfig['SEARCH_DISPLAY_MODE'].value == '9')}">
	<%@ include file="/WEB-INF/jsp/frontend/thumbnail/view9.jsp" %>
 </c:when>
 <c:when test="${(paramView == '10') or ( empty paramView and siteConfig['SEARCH_DISPLAY_MODE'].value == '10')}">
    <%@ include file="/WEB-INF/jsp/frontend/thumbnail/view10.jsp" %>
 </c:when>
 <c:when test="${(paramView == '11') or ( empty paramView and siteConfig['SEARCH_DISPLAY_MODE'].value == '11')}">
    <%@ include file="/WEB-INF/jsp/frontend/thumbnail/view11.jsp" %>
 </c:when>
 <c:when test="${(paramView == '12') or ( empty paramView and siteConfig['SEARCH_DISPLAY_MODE'].value == '12')}">
    <%@ include file="/WEB-INF/jsp/frontend/thumbnail/view12.jsp" %>
 </c:when>
 <c:when test="${ (gSiteConfig['gSITE_DOMAIN'] == 'viatrading.com' or gSiteConfig['gSITE_DOMAIN'] == 'test.com') and  (( paramView == '15') or ( empty paramView and siteConfig['SEARCH_DISPLAY_MODE'].value == '15'))}">
    <%@ include file="/WEB-INF/jsp/frontend/viatrading/thumbnail/view1.jsp" %>
 </c:when>
 <c:when test="${(paramView == 'q' || paramView == 'quick') or ( empty paramView and siteConfig['SEARCH_DISPLAY_MODE'].value == 'quick')}">
    <%@ include file="/WEB-INF/jsp/frontend/quickmode/view1.jsp" %>
 </c:when>
 <c:when test="${(paramView == 'q2' || paramView == 'quick2') or ( empty paramView and siteConfig['SEARCH_DISPLAY_MODE'].value == 'quick2')}">
    <%@ include file="/WEB-INF/jsp/frontend/quickmode/view2.jsp" %>
 </c:when>
 <c:when test="${(paramView == 'q2-1' || paramView == 'quick2-1') or ( empty paramView and siteConfig['SEARCH_DISPLAY_MODE'].value == 'quick2-1')}">
	  <%@ include file="/WEB-INF/jsp/frontend/quickmode/view2-1.jsp" %>
 </c:when>
 <c:when test="${(paramView == 'q2-2' || paramView == 'quick2-2') or ( empty paramView and siteConfig['SEARCH_DISPLAY_MODE'].value == 'quick2-2')}">
	  <%@ include file="/WEB-INF/jsp/frontend/quickmode/view2-2.jsp" %>
 </c:when>
 <c:otherwise>
 	<%@ include file="/WEB-INF/jsp/frontend/quickmode/view1.jsp" %>
 </c:otherwise>
</c:choose>
</c:forEach>
</c:when>

<c:otherwise>
 	<%@ include file="/WEB-INF/jsp/frontend/quickmode/groupView.jsp" %>
</c:otherwise>
</c:choose>
</form>

<c:if test="${model.count > 0}">
<c:out value="${pageShowing}" escapeXml="false" />
</div>
</c:if>
    
<c:out value="${model.productSearchLayout.footerHtml}" escapeXml="false"/>     
    
  </tiles:putAttribute>
</tiles:insertDefinition>
