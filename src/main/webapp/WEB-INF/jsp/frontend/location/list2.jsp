<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>

<tiles:insertDefinition name="${_template}" flush="true">
  <tiles:putAttribute name="content" type="string">
  
<c:if test="${gSiteConfig['gLOCATION']}">
<c:out value="${model.locationLayout.headerHtml}" escapeXml="false"/>
<form method="post">
<div id="dealerLocatorSearch">
<table border="0" >
<tr>
 <td>Keyword:</td>
 <td>Category:</td>
 <td>Zipcode:</td>
 <td>With in:</td>
</tr>
<tr>
 <td><input type="text" name="keywords" value="<c:out value='${model.locationSearch.keywords }' />" /></td>
 <td><input type="text" name="categoryId" value="<c:out value='${model.locationSearch.categoryId }' />" /></td>
 <td><input type="text" name="zipcode" value="<c:out value='${model.locationSearch.zipcode }' />" /></td>
 <td>
  <select name="radius" style="width:150px;">
   <option value="-1">Show All</option>
   <option value="5" <c:if test="${model.locationSearch.radius == 5}">selected</c:if> >5 miles</option>
   <option value="10" <c:if test="${model.locationSearch.radius == 10}">selected</c:if> >10 miles</option>
   <option value="20" <c:if test="${model.locationSearch.radius == 20}">selected</c:if> >20 miles</option>
   <option value="50" <c:if test="${model.locationSearch.radius == 50}">selected</c:if> >50 miles</option>
   <option value="70" <c:if test="${model.locationSearch.radius == 70}">selected</c:if> >70 miles</option>
   <option value="100" <c:if test="${model.locationSearch.radius == 100}">selected</c:if> >100 miles</option>
  </select>
 </td>
 <td><input type="image" border="0" name="_submit" src="assets/Image/Layout/button_findLocation.gif" /></td>
</tr>
</table>
</div>

<table width="100%" align="center" cellspacing="5" cellpadding="0" border="0">
  <tr>
	<td>
	  <c:if test="${model.locations.pageCount > 1}" >
      <div class="pageNavi">Page 
		<select name="page" onchange="submit()">
		<c:forEach begin="1" end="${model.locations.pageCount}" var="page">
  	        <option value="${page}" <c:if test="${page == (model.locations.page+1)}">selected</c:if>>${page}</option>
		</c:forEach>
		</select>
      of <c:out value="${model.locations.pageCount}"/></div>
      </c:if>
	</td>
  </tr>
  <tr>
	<td valign="top" colspan="2">
	<table border="0" cellpadding="3" cellspacing="1" width="100%" id="locationList"> 
	<c:forEach items="${model.locations.pageList}" var="location" varStatus="status">
	  <tr id="row<c:out value="${status.index}"/>" class="company">
	    <td class="index"><c:out value="${status.index + 1}"/><div class="companyLogo"><img border="0" src="assets/Image/CompanyLogo/logo_${location.userId}.gif" /></div></td>
		<td class="nameCol">
		  <div class="storeName"><c:out value="${location.name}"/></div>
		  <div class="storeAddress"><c:out value="${location.addr1}"/><c:if test="${!empty location.addr2}" > <c:out value="${location.addr2}"/></c:if><br />
		    <c:out value="${location.city}"/>, <c:out value="${location.stateProvince}"/> <c:out value="${location.zip}"/> <c:out value="${location.country}"/>
		   </div>
		  <div class="storePhone"><c:out value="${location.phone}"/></div>
		  <div class="storeFax"><c:out value="${location.fax}"/></div>
		  <div class="storeNote"><c:out value="${location.htmlCode}" escapeXml="false"/></div>
		  <c:if test="${!empty siteConfig['GOOGLE_MAP_KEY'].value}" >
		  <input type="hidden" id="address<c:out value="${status.index}"/>" name="address<c:out value="${status.index}"/>" value="<c:out value="${location.addr1}"/><c:if test="${!empty location.addr2}" > <c:out value="${location.addr2}"/></c:if>, <c:out value="${location.city}"/>, <c:out value="${location.stateProvince}"/> <c:out value="${location.zip}"/> <c:out value="${location.country}"/>" />
		  </c:if>
		  <div class="addToList"><a href="location_addToList.jhtm?l_id=${location.id}" >Add to List</a></div>
		</td>
	  </tr>
	</c:forEach>
	</table>
	</td>
  </tr>
</table>
</form>
<c:out value="${model.locationLayout.footerHtml}" escapeXml="false"/>
</c:if>

  </tiles:putAttribute>
</tiles:insertDefinition>