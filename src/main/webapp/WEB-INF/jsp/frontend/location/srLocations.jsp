<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>

<tiles:insertDefinition name="${_template}" flush="true">
  <tiles:putAttribute name="content" type="string">

<c:if test="${gSiteConfig['gLOCATION'] and gSiteConfig['gSALES_REP']}">
<c:out value="${model.locationLayout.headerHtml}" escapeXml="false"/>
<c:if test="${!empty siteConfig['GOOGLE_MAP_KEY'].value}" >
<script type="text/javascript" src="javascript/mootools-1.2.5-core.js"></script>
<script src="http://maps.google.com/maps?file=api&amp;v=2&amp;key=<c:out value="${siteConfig['GOOGLE_MAP_KEY'].value}"/>"
      type="text/javascript"></script>
<script type="text/javascript">

    var map = null;
    var geocoder = null;
    
	window.onload=function(){
		if (GBrowserIsCompatible()) {
	        map = new GMap2(document.getElementById("map_canvas"));
	        map.setCenter(new GLatLng(38, -99), 3);
	        map.addControl(new GSmallMapControl());
	        geocoder = new GClientGeocoder();
	      }
	}
	
	function panTo(id) {
        var trs = $$('tr.row1');
	    trs.each(function(el) {
		  $(el).removeClass('row1');
	    });
	    
        $('row'+id).addClass('row1');
    }   
    var gmarkers = [];
    function createMarker(point,name,html) {
        var letter = String.fromCharCode("A".charCodeAt(0) + (gmarkers.length));
        var myIcon = new GIcon(G_DEFAULT_ICON, "http://www.google.com/mapfiles/marker" + letter + ".png");
        myIcon.printImage = "http://maps.google.com/mapfiles/marker"+letter+"ie.gif"
        myIcon.mozPrintImage = "http://maps.google.com/mapfiles/marker"+letter+"ff.gif"
 
        var marker = new GMarker(point, {icon:myIcon});

        GEvent.addListener(marker, "click", function() {
          marker.openInfoWindowHtml(html);
        });
        // save the info we need to use later for the side_bar
        gmarkers.push(marker);
        return marker;
      }

    function showAddress(id, address) {
      panTo(id);
      if (geocoder) {
        geocoder.getLatLng(
          address,
          function(point) {
            if (!point) {
              alert(address + " not found");
            } else {
              map.setCenter(point, 13);
              var marker = createMarker(point,"Marker" + id,address);
              map.addOverlay(marker);
            }
          }
        );
      }
    }
    </script>
</c:if>

<form method="post">
<div id="srLocatorSearch">
<table border="0" >
<tr>
 <td>Zipcode:</td>
 <td>With in:</td>
</tr>
<tr>
 <td><input type="text" name="zipcode" value="<c:out value='${model.srLocationSearch.zipcode }' />" /></td>
 <td>
  <select name="radius" style="width:150px;">
   <option value="-1">Show All</option>
   <option value="5" <c:if test="${model.srLocationSearch.radius == 5}">selected</c:if> >5 miles</option>
   <option value="10" <c:if test="${model.srLocationSearch.radius == 10}">selected</c:if> >10 miles</option>
   <option value="20" <c:if test="${model.srLocationSearch.radius == 20}">selected</c:if> >20 miles</option>
   <option value="50" <c:if test="${model.srLocationSearch.radius == 50}">selected</c:if> >50 miles</option>
   <option value="70" <c:if test="${model.srLocationSearch.radius == 70}">selected</c:if> >70 miles</option>
   <option value="100" <c:if test="${model.srLocationSearch.radius == 100}">selected</c:if> >100 miles</option>
  </select>
 </td>
 <td><input type="image" border="0" name="_submit" src="assets/Image/Layout/button_findLocation.gif" /></td>
</tr>
</table>
</div>

<table width="100%" align="center" cellspacing="5" cellpadding="0" border="0">
  <tr>
	<td>
	  <c:if test="${model.srLocations.pageCount > 1}" >
      <div class="pageNavi">Page 
		<select name="page" onchange="submit()">
		<c:forEach begin="1" end="${model.srLocations.pageCount}" var="page">
  	        <option value="${page}" <c:if test="${page == (model.srLocations.page+1)}">selected</c:if>>${page}</option>
		</c:forEach>
		</select>
      of <c:out value="${model.srLocations.pageCount}"/></div>
      </c:if>
	</td>
  </tr>
  <tr>
	<td valign="top" colspan="2">
	<table border="0" cellpadding="3" cellspacing="1" width="100%" id="locationList"> 
	<c:forEach items="${model.srLocations.pageList}" var="location" varStatus="status">
	  <tr id="row<c:out value="${status.index}"/>">
	    <td class="index"><c:out value="${status.index + 1}"/></td>
		<td class="nameCol">
		  <div class="srName"><c:out value="${location.name}"/></div>
		  <div class="srAddress"><c:out value="${location.address.addr1}"/><c:if test="${!empty location.address.addr2}" > <c:out value="${location.address.addr2}"/></c:if><br />
		    <c:out value="${location.address.city}"/>, <c:out value="${location.address.stateProvince}"/> <c:out value="${location.address.zip}"/> <c:out value="${location.address.country}"/>
		   </div>
		  <div class="srPhone"><c:out value="${location.phone}"/></div>
		  <c:if test="${!empty siteConfig['GOOGLE_MAP_KEY'].value}" >
		  <input type="hidden" id="address<c:out value="${status.index}"/>" name="address<c:out value="${status.index}"/>" value="<c:out value="${location.address.addr1}"/><c:if test="${!empty location.address.addr2}" > <c:out value="${location.address.addr2}"/></c:if>, <c:out value="${location.address.city}"/>, <c:out value="${location.address.stateProvince}"/> <c:out value="${location.address.zip}"/> <c:out value="${location.address.country}"/>" />
          <input type="button" value="Get Map!" onclick="showAddress(<c:out value="${status.index}"/>, document.getElementById('address<c:out value="${status.index}"/>').value); return false"/>
		  </c:if>
		</td>
	  </tr>
	</c:forEach>
	</table>
	</td>
	<c:if test="${model.srLocations.nrOfElements != 0 and !empty siteConfig['GOOGLE_MAP_KEY'].value}">
	<td valign="top"><div id="map_canvas" style="width: 500px; height: 300px"></div></td>	
    </c:if>
  </tr>
</table>
</form>
<c:out value="${model.locationLayout.footerHtml}" escapeXml="false"/>
</c:if>

  </tiles:putAttribute>
</tiles:insertDefinition>