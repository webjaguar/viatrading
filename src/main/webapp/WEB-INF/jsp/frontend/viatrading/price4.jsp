<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>

	<div class="qtyBreakTitlediv">
		<c:out value="${siteConfig['PRICE_BEFORE_TITLE'].value}" />
	</div>
	<div class="qtyBreakTitlediv">
		<c:out value="${siteConfig['PRICE_ONSALE_TITLE'].value}" />
	</div>
		<c:forEach items="${product.price}" var="price" varStatus="statusPrice">
	  <c:if test="${statusPrice.first and (price.amt > 0 || siteConfig['SHOW_ZERO_PRICE'].value == 'true')}">
        <c:set var="multiplier" value="1"/>
        <c:if test="${siteConfig['CASE_CONTENT_LAYOUT'].value == '1' and product.caseContent > 0}">
	  	 <c:set var="multiplier" value="${product.caseContent}"/>	
        </c:if>
		 	<span class="old_price" style="text-decoration: line-through"><fmt:message key="${siteConfig['CURRENCY'].value}" /><fmt:formatNumber value="${price.amt*multiplier}" pattern="#,##0.00" /></span>
		 	<span class="price"><fmt:message key="${siteConfig['CURRENCY'].value}" /><fmt:formatNumber value="${price.discountAmt*multiplier}" pattern="#,##0.00" /></span>
	  </c:if>
	  </c:forEach>  
	<c:if test="${ product.salesTag.image and !empty product.price }" >
        <div class="salesTagImage"><img align="right"  src="${_contextpath}/assets/Image/PromoSalesTag/salestag_${product.salesTag.tagId}.gif" /></div>
    </c:if>	