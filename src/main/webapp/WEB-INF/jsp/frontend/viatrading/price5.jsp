<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>

	<c:forEach items="${product.price}" var="price" varStatus="statusPrice">
	  <c:if test="${statusPrice.first and (price.amt > 0 || siteConfig['SHOW_ZERO_PRICE'].value == 'true')}">
        <c:set var="multiplier" value="1"/>
        <c:if test="${siteConfig['CASE_CONTENT_LAYOUT'].value == '1' and product.caseContent > 0}">
	  	 <c:set var="multiplier" value="${product.caseContent}"/>	
        </c:if>
		 	<span class="price"><fmt:message key="${siteConfig['CURRENCY'].value}" />
		 	<fmt:formatNumber value="${price.discountAmt*multiplier}" pattern="#,##0.00" /></span>
	  </c:if>
	</c:forEach>  
	