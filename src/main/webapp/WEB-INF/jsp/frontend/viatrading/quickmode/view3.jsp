<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ taglib uri="/WEB-INF/tlds/custom.tld" prefix="wj" %>

<c:if test="${siteConfig['PRODUCT_QUOTE'].value == 'true'}">
<script type="text/javascript">
function toggleAll(el) {
	var ids = document.getElementsByName("__selected_product_id");	
  	if (ids.length == 1)
      document.testView_form.__selected_product_sku.checked = el.checked;
    else
      for (i = 0; i < ids.length; i++)
        document.test_form.__selected_product_sku[i].checked = el.checked;	
}
function addToQuoteList(productId){
	var requestHTMLData = new Request.HTML ({
		url: "${_contextpath}/quoteList.jhtm?productId="+productId,
		onSuccess: function(response){ 
			$('product'+productId).set('html', '<fmt:message key="f_addedToQuoteList" />');
	 	}
	}).send();
}
</script>
</c:if>

<c:set var="productLink">${_contextpath}/product.jhtm?id=${product.id}<c:if test="${model.product == null or siteConfig['RECOMMENDED_LIST_SHOW_BREADCRUMB'].value == 'true'}">&cid=<c:out value="${param.cid}"/></c:if></c:set>
<c:if test="${gSiteConfig['gMOD_REWRITE'] == '1' and not fn:contains(product.sku, '/')}">
 <c:set var="productLink">${_contextpath}/${siteConfig['MOD_REWRITE_PRODUCT'].value}/${product.encodedSku}/${product.encodedName}.html<c:if test="${model.product == null or siteConfig['RECOMMENDED_LIST_SHOW_BREADCRUMB'].value == 'true'}">?cid=<c:out value="${param.cid}"/></c:if></c:set>
</c:if>	

<c:if test="${status.first}">
<form action="${_contextpath}/addToCart.jhtm" method="post" onsubmit="return checkForm()" id="view1">
<c:if test="${siteConfig['PRODUCT_QUOTE'].value == 'true' and (model.showProductQuoteColumn == null or model.showProductQuoteColumn)}" >
<div id="quoteButton"> 
   <a href="${_contextpath}/quoteViewList.jhtm"> <img alt="View Quote" src="${_contextpath}/assets/Image/Layout/button_viewQuoteList.jpg"> </a>
</div>
</c:if>

<c:if test="${gSiteConfig['gSHOPPING_CART']}">
	<div id="quickmode_v1_addtocart" class="quickmode_v1_addtocart_top" align="right" style="display:none;">
	<input type="image" value="Add to Cart" name="_addtocart_quick" class="_addtocart_quick" src="${_contextpath}/assets/Image/Layout/button_addtocart${_lang}.gif" />
	</div>
</c:if>

<table border="0" cellpadding="3" cellspacing="1" align="center" class="quickmode">
  <tr>  
  <c:if test="${siteConfig['PRODUCT_QUOTE'].value == 'true' and (model.showProductQuoteColumn == null or model.showProductQuoteColumn)}" >
 	<th class="quickmodeQuote"><fmt:message key="f_addToQuote" /></th>
 </c:if>
  <c:if test="${(showImage == null or showImage) and (siteConfig['QUICK_MODE_THUMB_HEIGHT'].value > 0 or siteConfig['QUICK_MODE_THUMB_HEIGHT'].value == '')}" >
   <th class="quickmode image"><fmt:message key="image" /></th>
  </c:if> 
  <c:if test="${siteConfig['SHOW_SKU'].value == 'true'}">
   <th class="quickmode sku"><fmt:message key="productSku" /></th>
  </c:if> 
   <th class="quickmode name"><fmt:message key="f_productName" /></th>

<c:choose>
  <c:when test="${model.slavesList != null}">
    <c:forEach items="${model.slavesProductFields}" var="productField">
      <c:if test="${productField.quickModeField}">
        <th class="quickmode"><c:out value="${productField.name}" escapeXml="false" /></th>
      </c:if>
    </c:forEach>
  </c:when>
  <c:otherwise>
    <c:forEach items="${model.productFields}" var="productField">
      <c:if test="${productField.id==7 or productField.id==8}">
        <th class="quickmode"><c:out value="${productField.name}" escapeXml="false" /></th>
      </c:if>
    </c:forEach>
  </c:otherwise>
</c:choose>
<c:if test="${model.showPriceColumn == null or model.showPriceColumn or model.slavesList != null}">
<th class="quickmode price" id="priceHeaderId"><fmt:message key="f_price"/></th>
</c:if>
<c:if test="${gSiteConfig['gSHOPPING_CART'] and ((model.showQtyColumn == null or model.showQtyColumn ) or (gSiteConfig['gMASTER_SKU'] and (model.slavesShowQtyColumn == null or model.slavesShowQtyColumn)))}">
<th class="quickmode qty"><fmt:message key="f_quantity"/></th>
</c:if>
 </tr>
</c:if>

<tr class="row${status.index % 2}">


 <c:if test="${siteConfig['PRODUCT_QUOTE'].value == 'true' and (model.showProductQuoteColumn == null or model.showProductQuoteColumn)}" >
 <td class="quickmodeQuote" id="product${product.id}">
 <c:if test="${product.quote}">
	 <input name="__selected_product_id" value="${product.id}" type="checkbox" onchange="addToQuoteList('${product.id}');" />	 	
 </c:if>
 </td>
</c:if>
 
 <c:if test="${(showImage == null or showImage) and (siteConfig['QUICK_MODE_THUMB_HEIGHT'].value > 0 or siteConfig['QUICK_MODE_THUMB_HEIGHT'].value == '')}" >
	<td class="quickmode image">
  <div align="center">
    <c:if test="${product.thumbnail == null}">&nbsp;</c:if>
    <c:if test="${product.thumbnail != null}">
	  <a href="${productLink}">
 	  <c:choose>
  	   <c:when test="${product.asiId != null}">
  	   <img src="<c:out value="${fn:replace(product.thumbnail.imageUrl, 'prodimgs', 'prodthumbs')}"/>" border="0" class="thumbnail_image" alt="<c:out value="${product.name}"/><c:if test="${product.shortDesc != ''}"> - <c:out value="${product.shortDesc}"/></c:if>">
  	   </c:when>
  	   <c:otherwise>
  	   <img src="<c:if test="${!product.thumbnail.absolute}">${_contextpath}/assets/Image/Product/thumb/</c:if><c:out value="${fn:replace(product.thumbnail.imageUrl, 'http://www.dollaritem.com/wms/images/catalog/', 'http://www.dollaritem.com/wms/images/thumbs/')}"/>" border="0" class="thumbnail_image" alt="<c:out value="${product.name}"/><c:if test="${product.shortDesc != ''}"> - <c:out value="${product.shortDesc}"/></c:if>">
  	   </c:otherwise>
  	  </c:choose>
  	  </a>
    </c:if>
  </div>
</td>
 </c:if>
 <c:if test="${siteConfig['SHOW_SKU'].value == 'true'}">
   <td class="quickmode sku"><c:out value="${product.sku}" escapeXml="false" /></td>
 </c:if>
   <td class="quickmode name">
 	  <c:choose>
 		<c:when test="${hideLink}"><c:out value="${product.name}" escapeXml="false" /></c:when>
 		<c:otherwise>
   	  <a href="${productLink}" class="thumbnail_item_name"><c:out value="${product.name}" escapeXml="false" /></a>
 		</c:otherwise>
 	  </c:choose>
   </td>
   <td align="center" class="quickmode">
     <c:out value="${wj:trimAndSplit(product.field7, '|' , false, ',', true, true)}" escapeXml="false"/>
   </td>
   <td align="center" class="quickmode">
     <c:out value="${wj:trimAndSplit(product.field8, '|' , false, ',', true, true)}" escapeXml="false"/>
   </td>

 <c:if test="${model.showPriceColumn == null or model.showPriceColumn or model.slavesList != null}">
 <td width="30%" style="white-space: nowrap" class="quickmode3-price">
<input type="hidden" name="product.id" value="${product.id}">
<c:choose>
<c:when test="${product.salesTag != null and !product.salesTag.percent}">
  <%@ include file="/WEB-INF/jsp/frontend/common/price3.jsp" %>
</c:when>
<c:otherwise>
  <%@ include file="/WEB-INF/jsp/frontend/common/price.jsp" %>
</c:otherwise>
</c:choose>
<%@ include file="/WEB-INF/jsp/frontend/common/productOptions.jsp" %>
<%@ include file="/WEB-INF/jsp/frontend/common/masterSku.jsp" %>
 </td>	
 </c:if>
 
 <c:if test="${gSiteConfig['gSHOPPING_CART'] and ((model.showQtyColumn == null or model.showQtyColumn) or (gSiteConfig['gMASTER_SKU'] and (model.slavesShowQtyColumn == null or model.slavesShowQtyColumn)))}">
 <c:set var="zeroPrice" value="false"/>
 <c:forEach items="${product.price}" var="price">
   <c:if test="${price.amt == 0}">
     <c:set var="zeroPrice" value="true"/>
   </c:if>
 </c:forEach> 
 <td width="15%" style="white-space: nowrap" class="quickmode qty">
 <c:if test="${ (gSiteConfig['gSHOPPING_CART'] and (!empty product.price or product.priceByCustomer)) and (!product.loginRequire or userSession != null)}"> 
 <c:choose>
  <c:when test="${product.type == 'box'}">
    <a href="${productLink}"><img border="0" src="${_contextpath}/assets/Image/Layout/button_buildthebox.gif" /></a>
  </c:when>
  <c:when test="${gSiteConfig['gVARIABLE_PRICE'] and product.priceByCustomer}">  
   <c:set var="showAddToCart" value="true" />
   <input type="checkbox" name="quantity_${product.id}" id="quantity_${product.id}" value="1" />
  </c:when>
  <c:when test="${!empty product.numCustomLines}">
    <a href="${productLink}"><img border="0" src="${_contextpath}/assets/Image/Layout/button_continue.gif" /></a>
  </c:when>
  <c:otherwise>
    <c:choose>
    <c:when test="${gSiteConfig['gINVENTORY'] and product.inventory != null and !product.negInventory and product.inventory <= 0}" >
      <c:out value="${siteConfig['OUT_OF_STOCK_TITLE'].value}" escapeXml="false"/>
    </c:when>
	<c:when test="${zeroPrice and siteConfig['ZERO_PRICE_HTML'].value != ''}">
      <c:out value="${siteConfig['ZERO_PRICE_HTML'].value}" escapeXml="false"/>
    </c:when>
    <c:otherwise>
    <c:set var="showAddToCart" value="true" />
      <fmt:message key="qty" />:&nbsp; 
      <c:choose>
        <c:when test="${siteConfig['PRODUCT_QTY_DROPDOWN'].value != ''}">
          <c:set var="startEmpty" value="true" />
          <%@ include file="/WEB-INF/jsp/frontend/common/qtyDropDown.jsp" %>
        </c:when>
        <c:otherwise>
          <%-- On Intermediate page we want to show the Qty on input, that's why we use cartItem.quantity --%>
          <input type="text" class="quantity" size="6" maxlength="5" name="quantity_${product.id}" id="quantity_${product.id}" value="<c:out value="${cartItem.quantity}"/>">
        </c:otherwise>
      </c:choose>
	  <c:if test="${product.packing != ''}"> <c:out value="${product.packing}"/> </c:if>          
	           
      <c:if test="${gSiteConfig['gINVENTORY'] and product.inventory != null and product.inventory < product.lowInventory}">
       <c:out value="${siteConfig['LOW_STOCK_TITLE'].value}" escapeXml="false"/>
      </c:if>	      
    </c:otherwise>
 </c:choose>    
  </c:otherwise>
 </c:choose>
 </c:if>&nbsp;
 </td>
 </c:if>
</tr>
<c:if test="${status.last}">
</table>

<c:if test="${gSiteConfig['gSHOPPING_CART'] and showAddToCart}">
  <div id="quickmode_v1_addtocart" class="quickmode_v1_addtocart_bottom" align="right">
  	<input type="image" value="Add to Cart" name="_addtocart_quick" class="_addtocart_quick" src="${_contextpath}/assets/Image/Layout/button_addtocart${_lang}.gif" >
  	<script>
   	 $('quickmode_v1_addtocart').style.display='block';
  	</script>
  </div>
</c:if>
</form>
</c:if>

