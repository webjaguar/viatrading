<%@ page import="java.util.*" %>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="/WEB-INF/tlds/spring.tld" prefix="spring" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ taglib uri="/WEB-INF/tlds/custom.tld" prefix="wj" %>

<link href="assets/quickmode2.css" rel="stylesheet" type="text/css" />


<div class="quickmode2_details_wrapper">

<div class="quickmode2_image_wrapper">
<c:forEach items="${model.product.images}" var="image" varStatus="status">
  <c:if test="${status.first}">
    <c:set var="pLink">${_contextpath}/product.jhtm?id=${model.product.id}</c:set>
	<c:if test="${gSiteConfig['gMOD_REWRITE'] == '1' and not fn:contains(model.product.sku, '/')}">
  		<c:set var="pLink">${_contextpath}/${siteConfig['MOD_REWRITE_PRODUCT'].value}/${model.product.encodedSku}/${model.product.encodedName}.html</c:set>
	</c:if>	
	<a href="${pLink}" target="_blank">
    <img src="<c:if test="${!image.absolute}">${_contextpath}/assets/Image/Product/detailsbig/</c:if><c:out value="${image.imageUrl}"/>" border="0" id="_image" name="_image" class="quickmode2_image"
	     alt="<c:out value="${product.name}"/><c:if test="${product.shortDesc != ''}"> - <c:out value="${product.shortDesc}"/></c:if>">
    </a>
  </c:if>
</c:forEach>
</div>
<div class="quickmode2_fields_wrapper">
  <div class="quickmode2_item_name"><c:out value="${model.product.name}" escapeXml="false" /></div>
  <div class="quickmode2_field field24"><c:out value="${wj:removeChar(model.product.field24, ',', true, true)}" escapeXml="false" /></div>
  <div class="quickmode2_field field14"><c:out value="${wj:removeChar(model.product.field14, ',', true, true)}" escapeXml="false" /></div>
  <div class="quickmode2_field field11"><c:out value="${wj:removeChar(model.product.field11, ',', true, true)}" escapeXml="false" /></div>
</div>

</div>
<div style="clear: both;"></div>
<div class="quickmode2_field field27"><c:out value="${wj:removeChar(model.product.field27, ',', true, true)}" escapeXml="false" /></div>