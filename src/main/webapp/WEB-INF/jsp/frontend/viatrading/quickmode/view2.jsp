<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ taglib uri="/WEB-INF/tlds/custom.tld" prefix="wj" %>

<c:if test="${userSession != null}">
<c:if test="${siteConfig['PRODUCT_QUOTE'].value == 'true'}">
<script type="text/javascript">
function toggleAll(el) {
	var ids = document.getElementsByName("__selected_product_id");	
  	if (ids.length == 1)
      document.testView_form.__selected_product_sku.checked = el.checked;
    else
      for (i = 0; i < ids.length; i++)
        document.test_form.__selected_product_sku[i].checked = el.checked;	
}
function addToQuoteList(productId){
	var requestHTMLData = new Request.HTML ({
		url: "${_contextpath}/quoteList.jhtm?productId="+productId,
		onSuccess: function(response){ 
			$('product'+productId).set('html', '<fmt:message key="f_addedToQuoteList" />');
	 	}
	}).send();
}
</script>
</c:if>
<script type="text/javascript">
function changeme(fieldId){
	document.getElementById('sort').value=fieldId;
	document.forms["sortFieldForm"].submit();
}
<%-- 
window.addEvent('domready', function(){
	$$('div.productQuickView').each(function(div){
		var sku = div.get('id').split("_")[0];
		var id = div.get('id').split("_")[1];
		var actionList = div.getParent(); 
		var productQuickView = actionList.getElements('div.action-hover')[0];
		productQuickView.set('opacity',0);
		var mouseEntered = false;
		div.addEvent('mouseenter',function() {
			this.setStyle('cursor','pointer'); 
			if(!mouseEntered){
				var request = new Request({
					url: "${_contextpath}/ajaxProduct.jhtm?sku="+sku,
					method: 'post',
					onComplete: function(response) { 
						$('productQuickView'+id).innerHTML = "<span style='color:#c00000;'>"+response+"</span>";
					}
				}).send();
				mouseEntered = true;
			}
			productQuickView.setStyle('display','block').fade('in');
		});
		productQuickView.addEvent('mouseleave',function() {
			productQuickView.fade('out');
			this.setStyle('cursor',''); 
		});
		actionList.addEvent('mouseleave',function() {
			productQuickView.fade('out');
			this.setStyle('cursor',''); 
		});
	});	
});
--%>


function showDetailBox(divId){
	
	var sku = divId.split("_")[0];
	var id = divId.split("_")[1];
	var ele = $(divId);
	var actionList = ele.getParent(); 
	var productQuickView = actionList.getElements('div.action-hover')[0];
	productQuickView.set('opacity',0);
	var mouseEntered = false;
	ele.setStyle('cursor','pointer'); 
		if(!mouseEntered){
			var request = new Request({
				url: "${_contextpath}/ajaxProduct.jhtm?sku="+sku,
				method: 'post',
				onComplete: function(response) { 
					$('productQuickView'+id).innerHTML = "<span style='color:#c00000;'>"+response+"</span>";
				}
			}).send();
			mouseEntered = true;
			productQuickView.setStyle('display','block').fade('in');
		}
	productQuickView.addEvent('mouseleave',function() {
		if(mouseEntered){
			productQuickView.setStyle('display','none').fade('out');
			$('productQuickView'+id).innerHTML = "";
			ele.setStyle('cursor',''); 
		}
	});
	actionList.addEvent('mouseleave',function() {
		if(mouseEntered){
			productQuickView.setStyle('display','none').fade('out');
			ele.setStyle('cursor',''); 
		}
	});
}
</script><%--Need more work for passing the parent's name in Mod_Rewrite --%>

<c:set var="productLink">${_contextpath}/product.jhtm?<c:if test="${product.masterSku != '' and product.masterSku != null}">sku=${product.masterSku}</c:if><c:if test="${product.masterSku == '' or product.masterSku == null}">id=${product.id}</c:if><c:if test="${model.product == null or siteConfig['RECOMMENDED_LIST_SHOW_BREADCRUMB'].value == 'true'}">&cid=<c:out value="${param.cid}"/></c:if></c:set>
<c:if test="${gSiteConfig['gMOD_REWRITE'] == '1' and not fn:contains(product.sku, '/')}">
  <c:set var="productLink">${_contextpath}/${siteConfig['MOD_REWRITE_PRODUCT'].value}/<c:if test="${product.masterSku != '' and product.masterSku != null}">${product.encodedMasterSku}</c:if><c:if test="${product.masterSku == '' or product.masterSku == null}">${product.encodedSku}</c:if>/${product.encodedName}.html<c:if test="${model.product == null or siteConfig['RECOMMENDED_LIST_SHOW_BREADCRUMB'].value == 'true'}">?cid=<c:out value="${param.cid}"/></c:if></c:set>
</c:if>	

<c:if test="${status.first}">
<c:if test="${siteConfig['PRODUCT_QUOTE'].value == 'true' and (model.showProductQuoteColumn == null or model.showProductQuoteColumn)}" >
<div id="quoteButton"> 
   <a href="${_contextpath}/quoteViewList.jhtm"> <img alt="View Quote" src="${_contextpath}/assets/Image/Layout/button_viewQuoteList.jpg"> </a>
</div>
</c:if>
<table border="0" cellpadding="3" cellspacing="1" align="center" class="quickmode" id="quickModeTable">
  <tr id="quickModeHeader"> 
  <form id="sortFieldForm" name="sortFieldForm" action="${_contextpath}/<c:choose><c:when test="${gSiteConfig['gMOD_REWRITE'] == '1'}">${siteConfig['MOD_REWRITE_CATEGORY'].value}/${model.thisCategory.id}/${model.thisCategory.encodedName}.html</c:when><c:otherwise>category.jhtm</c:otherwise></c:choose>" method="post">
	<c:if test="${gSiteConfig['gMOD_REWRITE'] != '1'}">
    <input type="hidden" name="cid" value="${model.thisCategory.id}">
    </c:if>
    <input type="hidden" name="page" value="${frontEndProductSearch.page}">
    <input type="hidden" name="size" value="${frontEndProductSearch.pageSize}">
    <input type="hidden" name="sort" id="sort" value='${frontEndProductSearch.sort}'>
  <c:if test="${siteConfig['PRODUCT_QUOTE'].value == 'true' and (model.showProductQuoteColumn == null or model.showProductQuoteColumn)}" >
  	<th class="quickmodeQuote"><fmt:message key="f_addToQuote" /></th>
  </c:if>
  <c:if test="${siteConfig['SHOW_SKU'].value == 'true'}">
    <th class="quickmode">
    	<c:if test="${frontEndProductSearch.sort == 'sku ASC'}">
    		<a class="sort_acs" onclick="changeme('50')"><fmt:message key="productSku" /></a>
    	</c:if>
    	<c:if test="${frontEndProductSearch.sort == 'sku DESC'}">
    		<a  class="sort_decs" onclick="changeme('60')"><fmt:message key="productSku" /></a>
    	</c:if>
    	<c:if test="${frontEndProductSearch.sort != 'sku DESC' and frontEndProductSearch.sort != 'sku ASC'}">
     		<a class="unsort" onclick="changeme('50')"><fmt:message key="productSku" /></a>
    	</c:if>
    </th>
   </c:if> 
    <th class="quickmode"><fmt:message key="f_productName" /></th>
	 <c:forEach items="${model.productFields}" var="productField">
	  <c:if test="${productField.quickMode2Field}">
	  <th class="quickmode">
	  <c:set var="sort_acs" value="field_${productField.id} ASC"/>
	  <c:set var="sort_decs" value="field_${productField.id} DESC"/>
	  <c:choose>
	  	<c:when test="${fn:contains(frontEndProductSearch.sort,sort_acs)}">
			<a class="sort_acs" onclick="changeme('${productField.id + 200}')">${productField.name}</a>
	  	</c:when>
	  	<c:when test="${fn:contains(frontEndProductSearch.sort,sort_decs)}">
	  		<a class="sort_decs" onclick="changeme('${productField.id + 100}')">${productField.name}</a>
	  	</c:when>
	  	<c:otherwise><a class="unsort" onclick="changeme('${productField.id + 100}')">${productField.name}</a>
	  	</c:otherwise>
	  	</c:choose>
	  </th>
	</c:if>
	</c:forEach>
</form>
	
	
	
	<c:if test="${model.showPriceColumn == null or model.showPriceColumn}">
	<th class="quickmode" id="priceHeaderId">Price</th>
	</c:if>
	<c:if test="${gSiteConfig['gSHOPPING_CART'] and (model.showQtyColumn == null or model.showQtyColumn)}">
	<th class="quickmode">Add to Cart</th>
	</c:if>
	<th class="quickmode" id="manifestDownloadCheckboxId">Download Manifest</th>
  </tr>
</c:if>
<form action="${_contextpath}/addToCart.jhtm" method="post" >
<tr class="row${status.index % 2}">


  <c:if test="${siteConfig['PRODUCT_QUOTE'].value == 'true' and (model.showProductQuoteColumn == null or model.showProductQuoteColumn)}" >
  <td class="quickmodeQuote" id="product${product.id}">
  <c:if test="${product.quote}">
		 <input name="__selected_product_id" value="${product.id}" type="checkbox" onchange="addToQuoteList('${product.id}');" />	 	
  </c:if>
  </td>
  </c:if>
  <c:if test="${siteConfig['SHOW_SKU'].value == 'true'}">
    <td><a href="${productLink}" class="thumbnail_item_name" target="_blank"><c:out value="${product.sku}" escapeXml="false" /></a> </td>
  </c:if>
    <td>
      <div <c:if test="${product.masterSku != '' and product.masterSku != null}">class="productQuickView"  id="${product.masterSku}_${product.id}"</c:if>  onclick="showDetailBox('${product.masterSku}_${product.id}')">
  	  <c:choose>
  		<c:when test="${hideLink}"><c:out value="${product.name}" escapeXml="false" /></c:when>
  		<c:otherwise>
    	  <div class="action-hover" style="width: 400px;" id="productQuickView${product.id}"></div>
		  <c:out value="${product.name}" escapeXml="false" />
  		</c:otherwise>
  	  </c:choose>
  	  </div>
    </td>
  <c:forEach items="${product.productFields}" var="productField">
  
  <c:if test="${productField.quickMode2Field}">
	  <c:choose>
	  
		  <c:when test="${productField.id == '14' and ( productField.value == ',Manifested,' or productField.value == ',Partially Manifested,' )}">
		  	<td align="center"><a href="https://www.viatrading.com/dv/manifest/manifestdetails.php?SKU=${product.sku}&id=${product.id}&parentId=${product.parentId}&cid=${model.userId}" class="thumbnail_item_name" target="_blank"><c:out value="${wj:removeChar(productField.value, ',' , true, true) }" escapeXml="false" /></a></td>
		  </c:when>
		  <c:otherwise>
		  	<td align="center" >
       			<c:choose>
				<c:when test="${productField.formatType != null and productField.formatType != '' and productField.fieldType == 'number'}">
				<c:catch var="catchFormatterException">
				<fmt:formatNumber value="${wj:trimAndSplit(productField.value, '|' , false, ',', true, true)}" pattern="${productField.formatType}" />
				</c:catch>
				<c:if test = "${catchFormatterException != null}"><c:out value="${wj:trimAndSplit(productField.value, '|' , false, ',', true, true)}" escapeXml="false"/></c:if>
				</c:when>
				<c:otherwise>
					<c:out value="${wj:trimAndSplit(productField.value, '|' , false, ',', true, true)}" escapeXml="false"/>
				</c:otherwise>
				</c:choose> 
    		</td>
		  </c:otherwise>
	  </c:choose>
  </c:if>
  </c:forEach>

  <c:if test="${model.showPriceColumn == null or model.showPriceColumn}">
  <td width="30%" style="white-space: nowrap">
	<input type="hidden" name="product.id" value="${product.id}">
	<c:choose>
	<c:when test="${product.salesTag != null and !product.salesTag.percent}">
	  <%@ include file="/WEB-INF/jsp/frontend/common/price3.jsp" %>
	</c:when>
	<c:otherwise>
	  <%@ include file="/WEB-INF/jsp/frontend/common/price.jsp" %>
	</c:otherwise>
	</c:choose>
	<%@ include file="/WEB-INF/jsp/frontend/common/productOptions.jsp" %>
	<%@ include file="/WEB-INF/jsp/frontend/common/masterSku.jsp" %>
  </td>	
  </c:if>
  
  <c:if test="${gSiteConfig['gSHOPPING_CART'] and (model.showQtyColumn == null or model.showQtyColumn)}">
  <c:set var="zeroPrice" value="false"/>
  <c:forEach items="${product.price}" var="price">
    <c:if test="${price.amt == 0}">
      <c:set var="zeroPrice" value="true"/>
    </c:if>
  </c:forEach> 
  <td width="15%" style="white-space: nowrap">
   <c:if test="${ (gSiteConfig['gSHOPPING_CART'] and (!empty product.price or product.priceByCustomer)) and (!product.loginRequire or userSession != null)}"> 
  <c:choose>
   <c:when test="${product.type == 'box'}">
     <a href="${productLink}"><img border="0" src="${_contextpath}/assets/Image/Layout/button_buildthebox.gif" /></a>
   </c:when>
   <c:when test="${gSiteConfig['gVARIABLE_PRICE'] and product.priceByCustomer}">  
     <c:set var="showAddToCart" value="true" />
     <input type="checkbox" name="quantity_${product.id}" id="quantity_${product.id}" value="1"/>
     <input type="hidden" name="quantity_${product.id}_${product.sku}" id="quantity_${product.id}_${product.sku}" value="${product.sku}"/>
   </c:when>
   <c:when test="${!empty product.numCustomLines}">
     <a href="${productLink}"><img border="0" src="${_contextpath}/assets/Image/Layout/button_continue.gif" /></a>
   </c:when>
   <c:otherwise>
     <c:choose>
	    <c:when test="${gSiteConfig['gINVENTORY'] and product.inventory != null and !product.negInventory and product.inventory <= 0}" >
	      <c:out value="${siteConfig['OUT_OF_STOCK_TITLE'].value}" escapeXml="false"/>
	    </c:when>
		<c:when test="${zeroPrice and siteConfig['ZERO_PRICE_HTML'].value != ''}">
	      <c:out value="${siteConfig['ZERO_PRICE_HTML'].value}" escapeXml="false"/>
	    </c:when>
	    <c:otherwise>
	      <c:set var="showAddToCart" value="true" />
	      <input name="quantity_${product.id}" value="1" type="checkbox" >
	      <input type="hidden" name="quantity_${product.id}_${product.sku}" id="quantity_${product.id}_${product.sku}" value="${product.sku}"/>
	      <c:if test="${gSiteConfig['gINVENTORY'] and product.inventory != null and product.inventory < product.lowInventory}">
	       <c:out value="${siteConfig['LOW_STOCK_TITLE'].value}" escapeXml="false"/>
	      </c:if>	      
	    </c:otherwise>
	 </c:choose>    
   </c:otherwise>
  </c:choose>
  </c:if>&nbsp;
  </td>
  </c:if>
  
  <td id="manifestDownloadCheckboxId" width="15%" style="white-space: nowrap">
  <c:forEach items="${product.productFields}" var="productField">
  	 <c:if test="${productField.id == '14' and ( productField.value == ',Manifested,' or productField.value == ',Partially Manifested,')}">  
     <c:set var="showAddToCart" value="true" />
     <input type="checkbox" name="quantity_${product.id}" id="quantity_${product.id}" value="1"/>
     <input type="hidden" name="quantity_${product.id}_${product.sku}" id="quantity_${product.id}_${product.sku}" value="${product.sku}"/>
   </c:if>
  </c:forEach>
  </td>
  
  
</tr>
<c:if test="${status.last}">
</table>
<c:if test="${gSiteConfig['gSHOPPING_CART'] and showAddToCart}">
<div id="quickmode_v1_addtocart" align="right">
<input type="image" value="Add to Cart" name="_addtocart_quick" class="_addtocart_quick" src="${_contextpath}/assets/Image/Layout/button_addtocart${_lang}.gif" >
</div>

</form>
</c:if>
</c:if>
</c:if>