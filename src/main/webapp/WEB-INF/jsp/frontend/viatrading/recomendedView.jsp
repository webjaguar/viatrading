<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ taglib uri="/WEB-INF/tlds/custom.tld" prefix="wj" %>

<c:set var="productLink">${_contextpath}/product.jhtm?id=${product.id}<c:if test="${model.product == null or siteConfig['RECOMMENDED_LIST_SHOW_BREADCRUMB'].value == 'true'}">&cid=<c:out value="${param.cid}"/></c:if></c:set>
<c:if test="${gSiteConfig['gMOD_REWRITE'] == '1' and not fn:contains(product.sku, '/')}">
  <c:set var="productLink">${_contextpath}/${siteConfig['MOD_REWRITE_PRODUCT'].value}/${product.encodedSku}/${product.encodedName}.html<c:if test="${model.product == null or siteConfig['RECOMMENDED_LIST_SHOW_BREADCRUMB'].value == 'true'}">?cid=<c:out value="${param.cid}"/></c:if></c:set>
</c:if>	

<c:if test="${status.first}">
<form action="${_contextpath}/addToCart.jhtm" method="post" onsubmit="return checkForm()">
<table border="0" cellpadding="3" cellspacing="1" align="center" class="quickmode">
  <tr>
   <c:if test="${siteConfig['SHOW_SKU'].value == 'true'}">
    <th class="quickmode"><fmt:message key="productSku" /></th>
   </c:if> 
	<c:forEach items="${model.slavesProductFields}" var="productField">
	  <c:if test="${productField.quickModeField}">
	    <th class="quickmode"><c:out value="${productField.name}" escapeXml="false" /></th>
	  </c:if>
	</c:forEach>
	<c:if test="${model.showPriceColumn == null or model.showPriceColumn}">
	<th class="quickmode">Price</th>
	</c:if>
	<c:if test="${gSiteConfig['gSHOPPING_CART'] and (model.slavesShowQtyColumn == null or model.slavesShowQtyColumn)}">
	<th class="quickmode">Add to Cart</th>
	</c:if>
  </tr>
</c:if>

<tr class="row${status.index % 2}">
  <c:if test="${siteConfig['SHOW_SKU'].value == 'true'}">
    <td><c:out value="${product.sku}" escapeXml="false" /></td>
  </c:if>
  <c:forEach items="${product.productFields}" var="productField">
  <c:if test="${productField.quickModeField}">
    <td align="center">
  	<c:choose>
		  <c:when test="${productField.id == '14' and ( productField.value == ',Manifested,' or productField.value == ',Partially Manifested,' )}">
		  	<a href="https://www.viatrading.com/dv/manifest/manifestdetails.php?SKU=${product.sku}&id=${product.id}" class="thumbnail_item_name" target="_blank"><c:out value="${wj:removeChar(productField.value, ',' , true, true) }" escapeXml="false" /></a>
		  </c:when>
		  <c:otherwise>
		  	<c:choose>
				<c:when test="${productField.formatType != null and productField.formatType != '' and productField.fieldType == 'number'}">
				<c:catch var="catchFormatterException">
					<fmt:formatNumber value="${wj:trimAndSplit(productField.value, '|' , false, ',', true, true)}" pattern="${productField.formatType}" />
				</c:catch>
					<c:if test = "${catchFormatterException != null}"><c:out value="${wj:trimAndSplit(productField.value, '|' , false, ',', true, true)}" escapeXml="false"/></c:if>
				</c:when>
				<c:otherwise>
					<c:out value="${wj:trimAndSplit(productField.value, '|' , false, ',', true, true)}" escapeXml="false"/>
				</c:otherwise>
			</c:choose> 
		  </c:otherwise>
	 </c:choose>
	 </td>
  </c:if>
  </c:forEach>

  <c:if test="${model.showPriceColumn == null or model.showPriceColumn}">
  <td  class="recommPrice">
	<input type="hidden" name="product.id" value="${product.id}">
	<c:choose>
	<c:when test="${product.salesTag != null and !product.salesTag.percent}">
	  <%@ include file="/WEB-INF/jsp/frontend/viatrading/price4.jsp" %>
	</c:when>
	<c:otherwise>
	  <%@ include file="/WEB-INF/jsp/frontend/common/price.jsp" %>
	</c:otherwise>
	</c:choose>
	<%@ include file="/WEB-INF/jsp/frontend/common/productOptions.jsp" %>
	<%@ include file="/WEB-INF/jsp/frontend/common/masterSku.jsp" %>
  </td>	
  </c:if>
  
  <c:if test="${gSiteConfig['gSHOPPING_CART'] and (model.slavesShowQtyColumn == null or model.slavesShowQtyColumn)}">
  <c:set var="zeroPrice" value="false"/>
  <c:forEach items="${product.price}" var="price">
    <c:if test="${price.amt == 0}">
      <c:set var="zeroPrice" value="true"/>
    </c:if>
  </c:forEach> 
  <td align="center"  width="15%" style="white-space: nowrap;">
  <c:if test="${ (gSiteConfig['gSHOPPING_CART'] and (!empty product.price or product.priceByCustomer)) and (!product.loginRequire or userSession != null)}"> 
  <c:choose>
   <c:when test="${product.type == 'box'}">
     <a href="${productLink}"><img border="0" src="${_contextpath}/assets/Image/Layout/button_buildthebox.gif" /></a>
   </c:when>
   <c:when test="${gSiteConfig['gVARIABLE_PRICE'] and product.priceByCustomer}">  
     <c:set var="showAddToCart" value="true" />
     <input type="checkbox" name="quantity_${product.id}" id="quantity_${product.id}" value="1"/>
     <input type="hidden" name="quantity_${product.id}_${product.sku}" id="quantity_${product.id}_${product.sku}" value="${product.sku}"/>
   </c:when>
   <c:when test="${!empty product.numCustomLines}">
     <a href="${productLink}"><img border="0" src="${_contextpath}/assets/Image/Layout/button_continue.gif" /></a>
   </c:when>
   <c:otherwise>
     <c:choose>
	    <c:when test="${gSiteConfig['gINVENTORY'] and product.inventory != null and !product.negInventory and product.inventory <= 0}" >
	      <c:out value="${siteConfig['OUT_OF_STOCK_TITLE'].value}" escapeXml="false"/>
	    </c:when>
		<c:when test="${zeroPrice and siteConfig['ZERO_PRICE_HTML'].value != ''}">
	      <c:out value="${siteConfig['ZERO_PRICE_HTML'].value}" escapeXml="false"/>
	    </c:when>
	    <c:otherwise>
	      <c:set var="showAddToCart" value="true" />
	      <input name="quantity_${product.id}" value="1" type="checkbox" >
	      <input type="hidden" name="quantity_${product.id}_${product.sku}" id="quantity_${product.id}_${product.sku}" value="${product.sku}"/>
	      <c:if test="${gSiteConfig['gINVENTORY'] and product.inventory != null and product.inventory < product.lowInventory}">
	       <c:out value="${siteConfig['LOW_STOCK_TITLE'].value}" escapeXml="false"/>
	      </c:if>	      
	    </c:otherwise>
	 </c:choose>    
   </c:otherwise>
  </c:choose>
  </c:if>&nbsp;
  </td>
  </c:if>
</tr>

<c:if test="${status.last}">
</table>
<c:if test="${gSiteConfig['gSHOPPING_CART'] and showAddToCart}">
<div id="quickmode_v1_addtocart" align="right">
<input type="image" value="Add to Cart" name="_addtocart_quick" class="_addtocart_quick" src="${_contextpath}/assets/Image/Layout/button_addtocart${_lang}.gif" >
</div>
</c:if>
</form>
</c:if>