<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="/WEB-INF/tlds/custom.tld" prefix="wj" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">

<c:if test="${model.featuredProducts != null}">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<link rel="stylesheet" href="${_contextpath}/assets/SmoothGallery/<c:out value="${model.stylesheet}" />.css" type="text/css" media="screen" />

<script type="text/javascript" src="${_contextpath}/javascript/mootools-1.2.5-core.js"></script>
<script type="text/javascript">		
	//<![CDATA[
	window.addEvent('domready', function(){
	var numItem=<c:out value="${model.numItem}" />;
	var page=<c:out value="${model.page}" />;
	var currentPage=1;
	var totIncrement=0;
	var boxRight=<c:out value="${model.numBlock}" />;
	var boxLeft=1;
	var increment=<c:out value="${model.increment}" />;
	var maxRightIncrement=increment*(-<c:out value="${model.numBlock}" />);
	var next = function() {
			totIncrement-=increment;
			fx.start('margin-left',totIncrement);
			boxRight+=<c:out value="${model.numBlock}" />;
			boxLeft+=<c:out value="${model.numBlock}" />;
			currentPage++;
			if(currentPage == page){
				$('next').setStyle('backgroundImage','url(${_contextpath}/assets/Image/Layout/nextl.jpg)');
				$('next').removeEvents('click');
			} 
			if(currentPage != 1){
				$('previous').setStyle('backgroundImage','url(${_contextpath}/assets/Image/Layout/prev.jpg)');}
			$('pageNav').highlight('#ddf');
			$('pageNav').set('html', boxLeft + ' - '+ boxRight + ' of ' + numItem);
			$('previous').addEvent('click',prev);
	}
	var prev = function() {
			totIncrement+=increment;
			fx.start('margin-left',totIncrement);
			boxRight-=<c:out value="${model.numBlock}" />;
			boxLeft-=<c:out value="${model.numBlock}" />;
			currentPage--;
			if(currentPage == 1){
				$('previous').setStyle('backgroundImage','url(${_contextpath}/assets/Image/Layout/prevl.jpg)');
				$('next').setStyle('backgroundImage','url(${_contextpath}/assets/Image/Layout/next.jpg)');
				$('previous').removeEvents('click');	
			} 
			if(currentPage != page){
				$('next').setStyle('backgroundImage','url(${_contextpath}/assets/Image/Layout/next.jpg)');}
			$('pageNav').highlight('#ddf');
			$('pageNav').set('html', boxLeft + ' - '+ boxRight + ' of ' + numItem);		
			$('next').addEvent('click',next);
	}
	
	var fx=new Fx.Tween($('myList'),{
		duration:1000,
		transition:'back:in:out'});
		$('previous').setStyle('backgroundImage','url(${_contextpath}/assets/Image/Layout/prevl.jpg)');
		$('pageNav').set('html', boxLeft + ' - '+ boxRight + ' of ' + numItem);	
		if (currentPage != page) {
			$('next').addEvent('click',next);
		} else {
			$('next').setStyle('backgroundImage','url(${_contextpath}/assets/Image/Layout/nextl.jpg)');
		}
});
//]]>
</script>
</head>
<body style="padding:0;margin:0;">

<div id="container">
<div id="stage">
<ul id="myList">
<c:forEach items="${model.featuredProducts}" var="product" varStatus="status">
	<c:if test="${product.field32 != ''}">
				    	<div class="thumbnail_compact_container_${product.field32}">
	</c:if>
  <li id="l${status.index+1}">
  <div class="box">
    <div>
      <div class="product_image">
        <a href="product.jhtm?id=${product.id}" target="_top" >
          <c:choose>
           <c:when test="${model.detailsbig}">
             <c:forEach items="${product.images}" var="image" varStatus="statusImage">
	  			<c:if test="${statusImage.first}">
	    		<img src="<c:if test="${!image.absolute}">${_contextpath}/assets/Image/Product/detailsbig/</c:if><c:out value="${image.imageUrl}"/>" alt="" border="0"/> 
	  			</c:if>
			</c:forEach> 
           </c:when>
           <c:otherwise>
             <img src="<c:if test="${!product.thumbnail.absolute}">${_contextpath}/assets/Image/Product/thumb/</c:if><c:out value="${product.thumbnail.imageUrl}" />" alt="" border="0"/> 
           </c:otherwise>
          </c:choose>
		</a>
      </div> 
      <div class="product_name"><a href="product.jhtm?id=${product.id}" target="_top" ><c:out value="${product.name}" /></a></div>	
		  <c:if test="${product.field24 != ''}">
			    <div class="product_field24">
			    	<c:out value="${wj:removeChar(product.field24, ',' , true, true) }" escapeXml="false" />
				</div>	
		  </c:if>  
		  <c:if test="${product.field7 != '' and ( product.field36 == '1' or product.field36 == '3')}">
			    <div class="product_field7">
			    	<c:out value="${product.field7}"/>	
				</div>	
		  </c:if>
		    		    
		  <c:if test="${product.field14 != '' and product.field36 == '2'}">
			    <div class="product_field14">
			    	<c:out value="${wj:removeChar(product.field14, ',' , true, true) }"/>
				</div>	
		  </c:if>
		  <c:if test="${product.field5 != '' and product.field36 == '2'}">
			    <div class="product_field5">
			    	<c:out value="${product.field5}"/>	
				</div>	
		  </c:if>
		  <c:if test="${product.field11 != '' and product.field36 == '2'}">
			    <div class="product_field11">
			    	<c:out value="${product.field11}"/>	
				</div>	
		  </c:if>		  
		  <c:if test="${product.salesTag != null and !product.salesTag.percent and product.field6 != '' and product.field36 == '3'}">
 			 <%@ include file="/WEB-INF/jsp/frontend/viatrading/price4.jsp" %>/<c:out value="${product.field6}"/>	
		  </c:if>	
		  <c:if test="${product.field6 != '' and product.field36 == '1'}">
			    <div class="product_field6">			    		
	 			  <c:if test="${product.priceRangeMinimum != null}">
				  <c:choose>
				  	<c:when test="${!product.loginRequire or (product.loginRequire and userSession != null)}" >
				  	  <c:if test="${product.priceRangeMinimum > 0 || siteConfig['SHOW_ZERO_PRICE'].value == 'true'}">
				  	  
						<c:set var="multiplier" value="1"/>
						<c:if test="${siteConfig['CASE_CONTENT_LAYOUT'].value == '1' and product.caseContent > 0}">
						  <c:set var="multiplier" value="${product.caseContent}"/>	
						</c:if>  	  
				  	  
				  	  <div class="price_range">
					  <fmt:message key="${siteConfig['CURRENCY'].value}" /><fmt:formatNumber value="${product.priceRangeMinimum*multiplier}" pattern="#,##0.00"/>
					  <c:if test="${product.priceRangeMaximum != null}">-<fmt:message key="${siteConfig['CURRENCY'].value}"/><fmt:formatNumber value="${product.priceRangeMaximum*multiplier}" pattern="#,##0.00"/></c:if>
					  /<c:out value="${product.field6}"/>
					  </div>
					  </c:if>
					  <c:if test="${gSiteConfig['gINVENTORY'] and product.inventory != null}" >
				        <c:if test="${!empty siteConfig['INVENTORY_ON_HAND'].value}" ><div class="inventory_onhand"><c:out value="${siteConfig['INVENTORY_ON_HAND'].value}" escapeXml="false"/>&nbsp;<c:out value="${product.inventory}" /></div></c:if>
				      </c:if>
					</c:when>
					<c:otherwise>
					  <a href="${_contextpath}/login.jhtm?noMessage=t&forwardAction=<c:out value='${model.url}'/>" ><img border="0" name="_logintoviewprice" class="_logintoviewprice" src="${_contextpath}/assets/Image/Layout/login_to_view_price${_lang}.gif" /></a>
					</c:otherwise>
				  </c:choose>
				</c:if>
				</div>	
		  </c:if>
    </div>
    
  </div>
  </li>
  <c:if test="${product.field32 != ''}">
		</div>
	</c:if>
</c:forEach>  
</ul>
</div>
<div id="pageNav"></div>
</div>
<div id="slider-buttons">
<div id="previous" class="prev"></div><div id="next" class="next"></div>
</div>

</body>
</html>
</c:if>
