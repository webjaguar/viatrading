<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="/WEB-INF/tlds/custom.tld" prefix="wj" %>
 
<c:if test="${product.thumbnail != null}">
<c:if test="${status.first}" >
<div class="thumbnail_compact_wrapper">
</c:if>
	<c:forEach items="${product.productFields}" var="productField">
		<c:if test="${productField.id == 32}">
			<c:set var="class1" value="${productField.value}" />
		</c:if>
	</c:forEach>
	<c:forEach items="${product.productFields}" var="productField">
		<c:if test="${productField.id == 33}">
			<c:set var="view" value="${productField.value}" />
		</c:if>
	</c:forEach>
	<c:choose>
		<c:when test="${class1 != null and class1 != ''}">
			<div class="thumbnail_compact_container_${class1}">
		</c:when>
		<c:otherwise>
			<div class="thumbnail_compact_container">
		</c:otherwise>
	</c:choose>
		<c:if test="${product.sku != null}">
				<c:if test="${siteConfig['SHOW_SKU'].value == 'true' }">
			<div class="thumbnail_compact_productsku">
				<div class="thumbnail_item_sku_link">
	        			<c:out value="${product.sku}" escapeXml="false"/>
	      		</div>
			</div>
		</c:if>
		</c:if>
		<div class="thumbnail_compact_productimage">
		      <div class="thumbnail_image_link">
		      	<c:set var="productURLDirect" value="false"/>
		      	<c:forEach items="${product.productFields}" var="productField">	
					
					<c:if test="${productField.id == siteConfig['PRODUCT_URL_FIELD_REDIRECT'].value and productField.value != null and productField.value != ''}">	    	
						<a href="${productField.value}">
						<c:set var="productURLDirect" value="true"/>
					</c:if>		    
				</c:forEach>
					<c:if test="${productURLDirect == 'false'}"><a href="${productLink}"></c:if>
			  	  	<c:choose>
			  	   		<c:when test="${product.asiId != null}">
			  	   			<img src="<c:out value="${fn:replace(product.thumbnail.imageUrl, 'prodimgs', 'prodthumbs')}"/>" border="0" class="thumbnail_image_qv" name="<c:out value="${product.name}"/><c:if test="${product.shortDesc != ''}"> - <c:out value="${product.shortDesc}"/></c:if>" alt="<c:out value="${product.name}"/><c:if test="${product.shortDesc != ''}"> - <c:out value="${product.shortDesc}"/></c:if>">
			  	  		 </c:when>
			  	  	 <c:otherwise>
			  	   		<img src="<c:if test="${!product.thumbnail.absolute}">${_contextpath}/assets/Image/Product/thumb/</c:if><c:out value="${fn:replace(product.thumbnail.imageUrl, 'http://www.dollaritem.com/wms/images/catalog/', 'http://www.dollaritem.com/wms/images/thumbs/')}"/>" border="0" class="thumbnail_image_qv" alt="<c:out value="${product.name}"/><c:if test="${product.shortDesc != ''}"> - <c:out value="${product.shortDesc}"/></c:if>" name="<c:out value="${product.name}"/><c:if test="${product.shortDesc != ''}"> - <c:out value="${product.shortDesc}"/></c:if>">
			  	   	 </c:otherwise>
			  	  	</c:choose>
			  	  </a>
		  	  </div>     
		</div>
		<div class="thumbnail_image_qv_link"><a href="${_contextpath}/qvproduct.jhtm?id=${product.id}" title="Quick View" class="quickViewsh15" rel="width:740,height:460" id="quickViewsh15${product.id}"><img  align="middle"  class="qvImg" alt="quickview" name="quickview" src="${_contextpath}/assets/Image/Layout/quick_view_inactive.gif"></a></div>
		<div class="thumbnail_compact_productname">
			<div class="thumbnail_item_name_link">
				<c:set var="productURLDirectName" value="false"/>
				<c:forEach items="${product.productFields}" var="productField">	
					<c:if test="${productField.id == siteConfig['PRODUCT_URL_FIELD_REDIRECT'].value and productField.value != null and productField.value != ''}">	    	
						<a href="${productField.value}" class="thumbnail_item_name"><c:out value="${product.name}" escapeXml="false"/></a>
						<c:set var="productURLDirectName" value="true"/>
					</c:if>		    
				</c:forEach>
					<c:if test="${productURLDirectName == 'false'}"><a href="${productLink}" class="thumbnail_item_name"><c:out value="${product.name}" escapeXml="false"/></a></c:if>
				
			</div>
		</div>
		<c:forEach items="${product.productFields}" var="productField">	
			<c:if test="${productField.id == 24 and productField.value != ''}">	    	
				<div class="thumbnail_compact_product_customfield24">
			    	<c:out value="${wj:removeChar(productField.value, ',' , true, true) }"/>	
				</div>
			</c:if>		    
		</c:forEach>
		<c:forEach items="${product.productFields}" var="productField">
		  <c:if test="${productField.id == 7 and productField.value != '' and (view == '1' or view == '3')}">
			    <div class="thumbnail_compact_product_customfield7">
			    	<c:out value="${wj:removeChar(productField.value, ',' , true, true) }"/>/<c:out value="${product.packing}"/>
				</div>	
		  </c:if>		    
		  <c:if test="${productField.id == 8 and productField.value != '' and (view == '1' or view == '3')}">
			    <div class="thumbnail_compact_product_customfield8">
			    	<c:out value="${wj:removeChar(productField.value, ',' , true, true) }"/>	
				</div>	
		  </c:if>		    
		</c:forEach>
		   <c:forEach items="${product.productFields}" var="productField">
		    <c:if test="${productField.id == 14 and productField.value != '' and view == '2'}">
			    <div class="thumbnail_compact_product_customfield14">
			    	<c:out value="${wj:removeChar(productField.value, ',' , true, true) }"/>	
				</div>	
		    </c:if>		    
		  </c:forEach>
			<c:forEach items="${product.productFields}" var="productField">
		 	 <c:if test="${productField.id == 5 and productField.value != '' and view == '2'}">
			    <div class="thumbnail_compact_product_customfield5">
			    	<c:out value="${wj:removeChar(productField.value, ',' , true, true) }"/>	
				</div>	
		  	 </c:if>		    
		   </c:forEach>
		   <c:forEach items="${product.productFields}" var="productField">
		   	<c:if test="${productField.id == 6}">
		   		<c:set var="field6" value="${wj:removeChar(productField.value, ',' , true, true) }" />
		   	</c:if>
		   </c:forEach>
		   <c:forEach items="${product.productFields}" var="productField">
		 	 <c:if test="${productField.id == 6 and productField.value != '' and view == '3'}">
			    <div class="thumbnail_compact_product_customfield6">
			    	<c:if test="${product.salesTag != null and !product.salesTag.percent}">
			 			 <%@ include file="/WEB-INF/jsp/frontend/viatrading/price4.jsp" %><span class="thumbnailField6">/${field6}</span>
				    </c:if>
				</div>	
		  	 </c:if>		    
		   </c:forEach>
		    <c:if test="${(product.price1 > 0 || siteConfig['SHOW_ZERO_PRICE'].value == 'true') and view == '2'}">
				<c:forEach items="${product.productFields}" var="productField">
					    	<c:if test="${productField.id == 11 and productField.value != ''}">
								<div class="thumbnail_compact_product_pricecustomfield11">
					    			<c:out value="${wj:removeChar(productField.value, ',' , true, true) }"/>
								</div>	
					    	</c:if>		    
				 </c:forEach>
		    </c:if>
		<c:if test="${(product.price1 > 0 || siteConfig['SHOW_ZERO_PRICE'].value == 'true') and (view == '1')}">
			<div class="thumbnail_compact_product_pricecustomfield6">
				<c:if test="${product.priceRangeMinimum != null}">
				  <c:choose>
				  	<c:when test="${!product.loginRequire or (product.loginRequire and userSession != null)}" >
				  	  <c:if test="${product.priceRangeMinimum > 0 || siteConfig['SHOW_ZERO_PRICE'].value == 'true'}">
				  	  
						<c:set var="multiplier" value="1"/>
						<c:if test="${siteConfig['CASE_CONTENT_LAYOUT'].value == '1' and product.caseContent > 0}">
						  <c:set var="multiplier" value="${product.caseContent}"/>	
						</c:if>  	  
				  	  
				  	  <div class="price_range">
					  <fmt:message key="${siteConfig['CURRENCY'].value}" /><fmt:formatNumber value="${product.priceRangeMinimum*multiplier}" pattern="#,##0.00"/>
					  <c:if test="${product.priceRangeMaximum != null}">-<fmt:message key="${siteConfig['CURRENCY'].value}"/><fmt:formatNumber value="${product.priceRangeMaximum*multiplier}" pattern="#,##0.00"/></c:if>
					  <c:forEach items="${product.productFields}" var="productField">
							<c:if test="${productField.id == 6 and productField.value != ''}">/<c:out value="${wj:removeChar(productField.value, ',' , true, true) }"/>	
							</c:if>		    
						</c:forEach>
					  </div>
					  </c:if>
					  <c:if test="${gSiteConfig['gINVENTORY'] and product.inventory != null}" >
				        <c:if test="${!empty siteConfig['INVENTORY_ON_HAND'].value}" ><div class="inventory_onhand"><c:out value="${siteConfig['INVENTORY_ON_HAND'].value}" escapeXml="false"/>&nbsp;<c:out value="${product.inventory}" /></div></c:if>
				      </c:if>
					</c:when>
					<c:otherwise>
					  <a href="${_contextpath}/login.jhtm?noMessage=t&forwardAction=<c:out value='${model.url}'/>" ><img border="0" name="_logintoviewprice" class="_logintoviewprice" src="${_contextpath}/assets/Image/Layout/login_to_view_price${_lang}.gif" /></a>
					</c:otherwise>
				  </c:choose>
				</c:if>
			  </div>
		</c:if>
			</div>
		</c:if>
	
<c:if test="${status.last}" >
</div>
</c:if>