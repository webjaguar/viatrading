<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="/WEB-INF/tlds/spring.tld" prefix="spring"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib uri="/WEB-INF/tlds/custom.tld" prefix="wj" %>

<tiles:insertDefinition name="${_template}" flush="true">  
<tiles:putAttribute name="leftbar" value="/WEB-INF/jsp/frontend/layout/template${_leftBar}/leftbar.jsp" />

	<tiles:putAttribute name="content" type="string">
<script type="text/javascript">
	window.addEvent('domready', function(){
		var tabs1 = new SimpleTabs($('tab-block-1'), { selector: 'h4'});
		var elementReview = $('productRateId');
		elementReview.getElement('a').addEvent('click', function(event){
	        event.stop();
	        tabs1.select(4);
	        new Fx.Scroll(window, { offset : {'x' : 0, 'y' : 100} }).toBottom();
		});
	});
	<c:if test="${siteConfig['DETAILS_IMAGE_WIDTH'].value != '' }" >
	function zoomIn() {
		window.open('${_contextpath}/productImage.jhtm?id=${product.id}&layout=2&imgUrl=' + document.getElementById("_image").src,'name','width=800,height=600,resizable=yes,scrollbars=1');
	}
	</c:if>
		
</script>
		<c:if test="${model.message != null}">
			<div class="message"><fmt:message key="${model.message}" /></div>
		</c:if>
		<c:if test="${model.product != null}">
			<c:if test="${fn:trim(model.productLayout.headerHtml) != ''}">
				<c:set value="${model.productLayout.headerHtml}" var="headerHtml" />
				<%@ include file="/WEB-INF/jsp/frontend/common/headerDynamicElement.jsp"%>
				<div><c:out value="${headerHtml}" escapeXml="false" /></div>
			</c:if>
			<c:set value="${model.product}" var="product" />
			<script language="JavaScript" type="text/JavaScript">
			<c:if test="${siteConfig['DETAILS_IMAGE_WIDTH'].value != '' }" >
			function zoomIn() {
				window.open('${_contextpath}/productImage.jhtm?id=${product.id}&layout=2&imgUrl=' + document.getElementById("_image").src,'name','width=800,height=600,resizable=yes,scrollbars=1');
			}
			</c:if>
			</script>
			<c:set var="details_images_style" value="" />
			<c:set var="details_desc_style" value="" />
			<c:set var="productImage">
				<c:choose>
					<c:when
						test="${fn:contains(siteConfig['PRODUCT_IMAGE_LAYOUTS'].value,'s2') and 
 		(product.imageLayout == 's2' or (empty product.imageLayout and siteConfig['PRODUCT_IMAGE_LAYOUT'].value == 's2'))}">
						<link rel="stylesheet"
							href="${_contextpath}/assets/SmoothGallery/slideshow.css"
							type="text/css" media="screen" />
						<script type="text/javascript"
							src="${_contextpath}/javascript/slideshow.js"></script>
						<script type="text/javascript">		
	//<![CDATA[
	  window.addEvent('domready', function(){
	    var data = [<c:forEach items="${product.images}" var="image" varStatus="status">'<c:if test="${!image.absolute}">${_contextpath}/assets/Image/Product/detailsbig/</c:if><c:out value="${image.imageUrl}"/>'<c:if test="${! status.last}" >,</c:if></c:forEach> ];
	    var myShow = new Slideshow('show', data, { controller: true, height: 300, hu: '', thumbnails: true, width: 400 });
		});
	//]]>
	</script>

						<div id="show" class="slideshow"></div>
						<div id="slideshow_margin"></div>
					</c:when>
					<c:when
						test="${fn:contains(siteConfig['PRODUCT_IMAGE_LAYOUTS'].value,'mb') and 
 		(param.multibox == null or param.multibox != 'off') and
 		(product.imageLayout == 'mb' or (empty product.imageLayout and siteConfig['PRODUCT_IMAGE_LAYOUT'].value == 'mb'))}">
						<link rel="stylesheet"
							href="${_contextpath}/assets/SmoothGallery/multibox.css"
							type="text/css" media="screen" />
						<c:set value="${true}" var="multibox" />
						<script src="${_contextpath}/javascript/overlay.js"
							type="text/javascript"></script>
						<script src="${_contextpath}/javascript/multibox.js"
							type="text/javascript"></script>
						<script type="text/javascript">
	window.addEvent('domready', function(){
		var box = new multiBox('mbxwz', {overlay: new overlay()});
	});
 </script>
						<div class="details_image_box" id="details_image_boxId"
							style="${details_images_style}
	 <c:if test="${siteConfig['DETAILS_IMAGE_WIDTH'].value != '' }">width: <c:out value="${siteConfig['DETAILS_IMAGE_WIDTH'].value}"/>px;</c:if>">

						<c:forEach items="${model.product.images}" var="image"
							varStatus="status">
							<c:if test="${status.first}">
								<a
									href="<c:if test="${!image.absolute}">${_contextpath}/assets/Image/Product/detailsbig/</c:if><c:out value="${image.imageUrl}"/>"
									id="mb0" class="mbxwz" title=""><img
									src="<c:if test="${!image.absolute}">${_contextpath}/assets/Image/Product/detailsbig/</c:if><c:out value="${image.imageUrl}"/>"
									border="0"
									style="<c:if test="${siteConfig['DETAILS_IMAGE_WIDTH'].value != '' }">width: <c:out value="${siteConfig['DETAILS_IMAGE_WIDTH'].value}"/>px</c:if>"
									alt="<c:out value="${product.name}"/><c:if test="${product.shortDesc != ''}"> - <c:out value="${product.shortDesc}"/></c:if>" /></a>
							</c:if>
						</c:forEach>
						<div style="clear: both;"><c:forEach
							items="${model.product.images}" var="image" varStatus="status">
							<c:if test="${status.count > 1}">
								<a
									href="<c:if test="${!image.absolute}">${_contextpath}/assets/Image/Product/detailsbig/</c:if><c:out value="${image.imageUrl}"/>"
									id="mb${status.count - 1}" class="mbxwz" title=""><img
									src="<c:if test="${!image.absolute}">${_contextpath}/assets/Image/Product/detailsbig/</c:if><c:out value="${image.imageUrl}"/>"
									border="0" class="details_thumbnail"
									alt="<c:out value="${product.name}"/><c:if test="${product.shortDesc != ''}"> - <c:out value="${product.shortDesc}"/></c:if>" /></a>
							</c:if>
						</c:forEach></div>
						</div>
					</c:when>
					<c:when
						test="${fn:contains(siteConfig['PRODUCT_IMAGE_LAYOUTS'].value,'qb') and 
 		(product.imageLayout == 'qb' or (empty product.imageLayout and siteConfig['PRODUCT_IMAGE_LAYOUT'].value == 'qb'))}">
						<link rel="stylesheet"
							href="${_contextpath}/assets/SmoothGallery/QuickBox/quickbox.css"
							type="text/css" media="screen" />
						<c:set value="${true}" var="quickbox" />
						<script src="${_contextpath}/javascript/QuickBox.js"
							type="text/javascript"></script>
						<script type="text/javascript">
	window.addEvent('domready', function(){
		new QuickBox();
	});
 </script>
						<div class="details_image_box" id="details_image_boxId" style="${details_images_style} <c:if test="${siteConfig['DETAILS_IMAGE_WIDTH'].value != '' }">width: <c:out value="${siteConfig['DETAILS_IMAGE_WIDTH'].value}"/>px;</c:if>">
						<c:forEach items="${model.product.images}" var="image"
							varStatus="status">
							<c:if test="${status.first}">
								<a
									href="<c:if test="${!image.absolute}">${_contextpath}/assets/Image/Product/detailsbig/</c:if><c:out value="${image.imageUrl}"/>"
									id="" class="" title="" rel="quickbox"><img
									src="<c:if test="${!image.absolute}">${_contextpath}/assets/Image/Product/detailsbig/</c:if><c:out value="${image.imageUrl}"/>"
									border="0"
									style="<c:if test="${siteConfig['DETAILS_IMAGE_WIDTH'].value != '' }">width: <c:out value="${siteConfig['DETAILS_IMAGE_WIDTH'].value}"/>px</c:if>"
									alt="<c:out value="${product.name}"/><c:if test="${product.shortDesc != ''}"> - <c:out value="${product.shortDesc}"/></c:if>" /></a>
							</c:if>
						</c:forEach>
						<div style="clear: both;"><c:forEach
							items="${model.product.images}" var="image" varStatus="status">
							<c:if test="${status.count > 1}">
								<a
									href="<c:if test="${!image.absolute}">${_contextpath}/assets/Image/Product/detailsbig/</c:if><c:out value="${image.imageUrl}"/>"
									id="qb${status.count - 1}" class="" title="" rel="quickbox"><img
									src="<c:if test="${!image.absolute}">${_contextpath}/assets/Image/Product/detailsbig/</c:if><c:out value="${image.imageUrl}"/>"
									border="0" class="details_thumbnail"
									alt="<c:out value="${product.name}"/><c:if test="${product.shortDesc != ''}"> - <c:out value="${product.shortDesc}"/></c:if>" /></a>
							</c:if>
						</c:forEach></div>
						</div>
					</c:when>
					<c:otherwise>
						<div class="details_image_box" id="details_image_boxId" style="${details_images_style} <c:if test="${siteConfig['DETAILS_IMAGE_WIDTH'].value != '' }">width: <c:out value="${siteConfig['DETAILS_IMAGE_WIDTH'].value}"/>px;</c:if>">
						<c:forEach items="${model.product.images}" var="image"
							varStatus="status">
							<c:if test="${status.first}">
								<img
									src="<c:if test="${!image.absolute}">${_contextpath}/assets/Image/Product/detailsbig/</c:if><c:out value="${image.imageUrl}"/>"
									border="0" id="_image" name="_image" class="details_image"
									style="<c:if test="${siteConfig['DETAILS_IMAGE_WIDTH'].value != '' }">width: <c:out value="${siteConfig['DETAILS_IMAGE_WIDTH'].value}"/>px</c:if>"
									alt="<c:out value="${product.name}"/><c:if test="${product.shortDesc != ''}"> - <c:out value="${product.shortDesc}"/></c:if>">
								<c:if test="${siteConfig['DETAILS_IMAGE_WIDTH'].value != '' }">
								</c:if>
							</c:if>

							<c:if test="${status.last and (status.count != 1)}">
								<br>
								<c:forEach items="${model.product.images}" var="thumb"
									varStatus="thumbstatus">
									<c:if test="${thumbstatus.index < 4}">
										<a href="#" class="details_thumbnail_anchor"
											onClick="_image.src='<c:if test="${!thumb.absolute}">${_contextpath}/assets/Image/Product/detailsbig/</c:if><c:out value="${thumb.imageUrl}"/>';return false;">
										<img
											src="<c:if test="${!thumb.absolute}">${_contextpath}/assets/Image/Product/detailsbig/</c:if><c:out value="${thumb.imageUrl}"/>"
											class="details_thumbnail"
											alt="<c:out value="${product.name}"/><c:if test="${product.shortDesc != ''}"> - <c:out value="${product.shortDesc}"/></c:if>">
										</a>
									</c:if>
								</c:forEach>
								<div class="moreImageWrapper" align="right"><a
									onclick="zoomIn()" title="View More Images"><span>View
								More Images</span></a></div>
							</c:if>
						</c:forEach></div>
						<c:if test="${fn:contains(siteConfig['PRODUCT_IMAGE_LAYOUTS'].value,'ss') and (product.imageLayout == 'ss'  or (empty product.imageLayout and siteConfig['PRODUCT_IMAGE_LAYOUT'].value == 'ss'))}">
							<object classid="clsid:D27CDB6E-AE6D-11cf-96B8-444553540000"
								codebase="http://download.macromedia.com/pub/shockwave/cabs/flash/swflash.cab#version=6,0,29,0"
								width="750" height="475">
								<param name="movie"
									value="${_contextpath}/assets/Flash/slideshow.swf?file=thumbnailer1.jhtm?id=${product.id}">
								<param name="quality" value="high">
								<embed
									src="${_contextpath}/assets/Flash/slideshow.swf?file=thumbnailer1.jhtm?id=${product.id}"
									quality="high"
									pluginspage="http://www.macromedia.com/go/getflashplayer"
									type="application/x-shockwave-flash" width="750" height="475"></embed>
							</object>
						</c:if>
					</c:otherwise>
				</c:choose>
			</c:set>


			<%-- bread crumbs --%>
			<c:import url="/WEB-INF/jsp/frontend/layout/${_template}/breadcrumbs.jsp" />
			<div class="details_main_description">
			<div class="details_main_upper_container">
			<div class="details_product_image_container"><c:out value="${productImage}" escapeXml="false" /></div>
			<div class="details_product_addtocart_container">
			<div class="details_product_addtocart_desc">
			<c:if test="${siteConfig['SHOW_SKU'].value == 'true' and product.sku != null}">
				<div class="details_product_addtocart_productsku"><c:out value="${product.sku}" /></div>
			</c:if>
			<div class="details_product_addtocart_productname">
			<c:out value="${product.name}" escapeXml="false" />
			</div>
			</div>
			
			<c:if test="${(product.field31 ==  '1' || product.field31 ==  '3') && product.field24 != null}">
				<div class="details_product_addtocart_feild24">	
				 <c:out value="${wj:removeChar(product.field24, ',' , true, true) }" />	
				</div>
			</c:if>
			
			<c:if test="${ product.field3 != null}">
				<div class="details_product_addtocart_feild3">			
				 <c:out value="${product.field3}" /> 			
				</div>
			</c:if>
			
			
			<c:if test="${(product.field31 ==  '1' || product.field31 ==  '3') && product.field7 != null}">
				<div class="details_product_addtocart_feild7">			
				 <c:out value="${product.field7}" />/<c:out value="${product.packing}" />			
				</div>
			</c:if>
			<c:if test="${(product.field31 ==  '1' || product.field31 ==  '3') && product.field8 != null}">
				<div class="details_product_addtocart_feild8">			
				 <c:out value="${product.field8}" />			
				</div>
			</c:if>
			<c:if test="${product.field31 ==  '2' && product.field24 != null}">
				<div class="details_product_addtocart_feild24">	
				 <c:out value="${wj:removeChar(product.field24, ',' , true, true) }" />		
				</div>
			</c:if>
			<c:if test="${product.field31 ==  '2' && product.field14 != null}">
				<div class="details_product_addtocart_feild14">		
				 <c:out value="${wj:removeChar(product.field14, ',' , true, true) }"  />			
				</div>
			</c:if>
			<c:if test="${product.field31 ==  '2' && product.field11 != null}">
				<div class="details_product_addtocart_feild11">	
				 <c:out value="${wj:removeChar(product.field11, ',' , true, true) }" /> 			
				</div>
			</c:if>	
			
			<div  <c:if test="${product.rate != null}"> id="productRateId" </c:if> class="details_product_addtocart_review">
			  <%@ include file="/WEB-INF/jsp/frontend/common/quickProductReview.jsp"%>
			</div>
			<c:if test="${product.salesTag != null and !product.salesTag.percent and product.field31 == '3'}">
 			 <%@ include file="/WEB-INF/jsp/frontend/viatrading/price4.jsp" %>/${product.field6}
		   </c:if>	
			<c:if test="${ (gSiteConfig['gSHOPPING_CART'] and (!empty product.price or product.priceByCustomer)) and (!product.loginRequire or userSession != null) and product.field31 == '1'}">
				<div id="details_prices_id" class="details_prices">
				  <c:out value="Price: " />$<fmt:formatNumber value="${product.price1}" pattern="#,##0.00"/><c:if test="${product.field6 != null}">/<c:out value="${product.field6}" /></c:if>
				</div>
			</c:if>	
			<c:if test="${product.field31 ==  '1'}">
			<c:catch var="e">
			<fmt:parseNumber  var="field7" type="number" value="${product.field7}"  />
			<div class="details_product_addtocart_price">
			<c:if test="${ (gSiteConfig['gSHOPPING_CART'] and (!empty product.price or product.priceByCustomer)) and (!product.loginRequire or userSession != null)}">
				<c:if test="${product.price2 != null}">
					<div class="details_product_pricesaving">
						<div class="details_product_pricesaving_msg">
							<c:out value="BUY MORE AND SAVE!"/>
						</div>
						<c:forEach items="${product.price}" var="price">
							<c:if test="${price.amt != 0 and price.qtyFrom > 1}">
								<div>
									 Buy <c:out value="${price.qtyFrom}"/> or more: $<fmt:formatNumber value="${price.amt/field7}" pattern="#,##0.00"/>/<c:out value="${siteConfig['CASE_UNIT_TITLE'].value}" /> - $<fmt:formatNumber value="${price.amt * product.caseContent}" pattern="#,##0.00"/>/lot (Save <fmt:formatNumber value="${(product.price1 - price.amt)/product.price1*100}" pattern="#0"/>% or $<fmt:formatNumber value="${(product.price1 - price.amt)*product.caseContent}" pattern="#,##0.00"/> )
								</div>
							</c:if>
						</c:forEach>
					</div>
				</c:if>
			</c:if>
			</div>
			</c:catch>
			</c:if>
			
			
			<div class="details_product_addtocart_button">
			<form action="${_contextpath}/addToCart.jhtm" name="this_form" method="post" onsubmit="return checkForm()">
			<c:if test="${product.field31 ==  '1'}">
				<c:if test="${siteConfig['DETAILS_IMAGE_LOCATION'].value == 'middle'}">
					<div class="details_hdr">Selectable Options</div>
					<table width="100%">
					<tr>
					<td valign="top" align="center" width="100%" class="detailsImageBox">
					<c:out value="${productImage}" escapeXml="false" />
					</td>
					<td valign="top">
					<%@ include file="/WEB-INF/jsp/frontend/common/productOptions2.jsp" %>
					</td>
					</tr>
					</table>
					<%@ include file="/WEB-INF/jsp/frontend/common/productOptionsSize.jsp" %>
				</c:if>
				
				<c:if test="${siteConfig['DETAILS_IMAGE_LOCATION'].value != 'middle'}">
					<c:choose>
					<c:when test="${siteConfig['SMART_DROPDOWN'].value == 'true'}">
					<%@ include file="/WEB-INF/jsp/frontend/common/productOptions5.jsp" %>
					</c:when>
					<c:otherwise>
					<%@ include file="/WEB-INF/jsp/frontend/common/productOptions.jsp" %>
					<%@ include file="/WEB-INF/jsp/frontend/common/productOptionsDoor.jsp" %>
					</c:otherwise>
					</c:choose>
				</c:if>
			</c:if>
			
			<c:if test="${ (gSiteConfig['gSHOPPING_CART'] and (!empty product.price or product.priceByCustomer)) and (!product.loginRequire or userSession != null)}">
				<input type="hidden" name="product.id" value="${product.id}">
				<div class="details_addInput">Quantity: <input type="text" name="quantity_${product.id}" id="quantity_${product.id}" value="1"></div>
				<div class="details_addButton">
				<c:set var="zeroPrice" value="false" /> 
				<c:forEach items="${product.price}" var="price">
					<c:if test="${price.amt == 0}">
						<c:set var="zeroPrice" value="true" />
					</c:if>
				</c:forEach> 
				<c:choose>
					<c:when test="${product.htmlAddToCart != null and product.htmlAddToCart != ''}">
						<c:out value="${product.htmlAddToCart}" escapeXml="false" />
					</c:when>
					<c:otherwise>
						<input type="image" border="0" name="_addtocart" class="_addtocart" src="${_contextpath}/assets/Image/Layout/button_addtocart${_lang}.gif" />
					</c:otherwise>
				</c:choose> 
				<c:if test="${gSiteConfig['gINVENTORY'] and product.inventory != null and product.inventory < product.lowInventory}">
					<c:out value="${siteConfig['LOW_STOCK_TITLE'].value}" escapeXml="false" />
				</c:if>
				</div>
				<div style="clear:both;"></div>
			</c:if>
			</form>
			</div>
			<c:if test="${(gSiteConfig['gMYLIST'] && product.addToList) or (gSiteConfig['gCOMPARISON'] && product.compare)}">
			<div class="details_product_buttons_container">
			<!-- myList -->
			<c:if test="${gSiteConfig['gMYLIST'] && product.addToList}">
				<div class="details_product_compare_addtolist">
				<form action="${_contextpath}/addToList.jhtm">
				<input type="hidden" name="product.id" value="${product.id}">
				<div id="details_addToList" class="details_addToList">
				<input type="image" border="0" src="${_contextpath}/assets/Image/Layout/button_addtolist.gif">
				</div>
				</form>
				</div>
			</c:if>
			<!-- compare -->
			<c:if test="${gSiteConfig['gCOMPARISON'] && product.compare}">
				<div class="details_product_compare">
				<form action="${_contextpath}/comparison.jhtm" method="get">
				<%--  <input type="hidden" name="forwardAction" value="<c:out value='${model.url}'/>"> --%> 
				<input type="hidden" name="product.id" value="${product.id}">
				<div id="details_compare" class="details_compare">
				 <input type="image" border="0" src="${_contextpath}/assets/Image/Layout/button_compare.gif">
				</div>
				</form>
				</div>
			</c:if>
			
			</div>
			</c:if>
			</div>
			</div>
			
			
			<c:if test="${gSiteConfig['gTAB'] > 0 and !empty model.tabList}">
			<div class="details_product_tab_container">
			<div class="details_tab">
				<script src="${_contextpath}/javascript/SimpleTabs1.2.js" type="text/javascript"></script>
				
				<!-- tabs -->
				<div id="tab-block-1">
				<c:forEach items="${model.tabList}" var="tab" varStatus="status">
				<c:choose>
					<c:when test="${tab.tabNumber == 3}">
						<c:if test="${userSession != null}">
							<h4 id="${tab.tabNumber}" title="<c:out value="${tab.tabName}" escapeXml="false"/>"> <c:out value="${tab.tabName}" escapeXml="false" /></h4>
						</c:if>
					</c:when>
					<c:otherwise>
						<h4 id="${tab.tabNumber}" title="<c:out value="${tab.tabName}" escapeXml="false"/>"> <c:out value="${tab.tabName}" escapeXml="false" /></h4>
					</c:otherwise>
				</c:choose>				
					<div>
					<!-- start tab --> 
					<c:choose>
					  <c:when test="${tab.tabNumber == 1}">
					  	<div class="details_product_long_desc_tab"><c:out value="${product.longDesc}" escapeXml="false" /></div>
					  </c:when>
					  <c:when test="${tab.tabNumber == 2}">
					    <c:forEach items="${product.productFields}" var="productField" varStatus="row">
							<c:if test="${row.first}">
							<c:if test="${siteConfig['PRODUCT_FIELDS_TITLE'].value != ''}">
							<div class="details_fields_title"><c:out value="${siteConfig['PRODUCT_FIELDS_TITLE'].value}"/></div>
							</c:if>
							<table class="details_fields">
							</c:if>
							<c:if test="${productField.id != '5'and productField.id != '6'and productField.id != '30'and productField.id != '31' and productField.id != 32 and productField.id != '33' and productField.id != '34' and productField.id != '36'}">
							<tr>
							<td class="details_field_name_row${row.index % 2}"><c:out value="${productField.name}" escapeXml="false" /> </td>
							<c:choose>
							<c:when test="${productField.formatType != null and productField.formatType != '' and productField.fieldType == 'number' }">
							<td class="details_field_value_row${row.index % 2}">
							<c:catch var="catchFormatterException">
							<fmt:formatNumber value="${wj:trimAndSplit(productField.value, '|' , false, ',', true, true)}" pattern="${productField.formatType}" />
							</c:catch>
							<c:if test = "${catchFormatterException != null}"><c:out value="${wj:trimAndSplit(productField.value, '|' , false, ',', true, true)}" escapeXml="false"/></c:if>
							</td>
							</c:when>
							<c:otherwise>
							<td class="details_field_value_row${row.index % 2}"><c:out value="${wj:trimAndSplit(productField.value, '|' , false, ',', true, true)}" escapeXml="false"/></td>
							</c:otherwise>
							</c:choose>
							</tr>
							</c:if>
							<c:if test="${row.last}">
							</table>
							</c:if>
						</c:forEach>
					  </c:when>
					  <c:when test="${tab.tabNumber == 5}">
					    <c:if test="${gSiteConfig['gPRODUCT_REVIEW'] and siteConfig['PRODUCT_RATE'].value != 'true' and model.product.enableRate}">
						<div style="float:left;width:100%">
						<%@ include file="/WEB-INF/jsp/frontend/common/productReviewList.jsp" %>
						</div>
						<div class="clearboth">&nbsp;</div>
						</c:if>
					  </c:when>
					  <c:when test="${tab.tabNumber == 3 and ! empty model.slavesList and userSession != null}">
					  		<div style="width:100%">
                                <c:choose>                                                                 
                                 <c:when test="${!empty product.recommendedListTitle }">
                                  <div class="recommended_list"><c:out value="${product.recommendedListTitle}"/></div>
                                 </c:when>
                                 <c:otherwise>                                                                  
                                  <c:if test="${siteConfig['RECOMMENDED_LIST_TITLE'].value != ''}">
                                   <div class="recommended_list"><c:out value="${siteConfig['RECOMMENDED_LIST_TITLE'].value}"/></div>
                                  </c:if>
                                 </c:otherwise>
								</c:choose>
					  			<c:forEach items="${model.slavesList}" var="product" varStatus="status">							
									<c:set var="productLink">${_contextpath}/product.jhtm?id=${product.id}<c:if test="${model.product == null or siteConfig['RECOMMENDED_LIST_SHOW_BREADCRUMB'].value == 'true'}">&cid=<c:out value="${param.cid}"/></c:if></c:set>
									<c:if test="${gSiteConfig['gMOD_REWRITE'] == '1' and not fn:contains(product.sku, '/')}">
									  <c:set var="productLink">${_contextpath}/${siteConfig['MOD_REWRITE_PRODUCT'].value}/${product.encodedSku}/${product.encodedName}.html<c:if test="${model.product == null or siteConfig['RECOMMENDED_LIST_SHOW_BREADCRUMB'].value == 'true'}">?cid=<c:out value="${param.cid}"/></c:if></c:set>
									</c:if>								
					  				 <%@ include file="/WEB-INF/jsp/frontend/viatrading/recomendedView.jsp" %>
								</c:forEach>
						  	</div>
					  			<%-- Via DON'T USE RECOMMANDED ANYMORE, THEY USE PARENT SKU
								<c:if test="${model.recommendedList != null and hideRecommendedList == null}">
								<div style="width:100%">
								<c:choose>
								 <c:when test="${!empty product.recommendedListTitle }">
								  <div class="recommended_list"><c:out value="${product.recommendedListTitle}"/></div>
								 </c:when>
								 <c:otherwise>
								  <c:if test="${siteConfig['RECOMMENDED_LIST_TITLE'].value != ''}">
								   <div class="recommended_list"><c:out value="${siteConfig['RECOMMENDED_LIST_TITLE'].value}"/></div>
								  </c:if>
								 </c:otherwise>
								</c:choose>
								<c:set var="numCul" value="${siteConfig['NUMBER_PRODUCT_PER_ROW'].value}" />
								<c:forEach items="${model.recommendedList}" var="product" varStatus="status">
								
								<c:set var="productLink">${_contextpath}/product.jhtm?id=${product.id}<c:if test="${model.product == null or siteConfig['RECOMMENDED_LIST_SHOW_BREADCRUMB'].value == 'true'}">&cid=<c:out value="${param.cid}"/></c:if></c:set>
								<c:if test="${gSiteConfig['gMOD_REWRITE'] == '1' and not fn:contains(product.sku, '/')}">
								  <c:set var="productLink">${_contextpath}/${siteConfig['MOD_REWRITE_PRODUCT'].value}/${product.encodedSku}/${product.encodedName}.html<c:if test="${model.product == null or siteConfig['RECOMMENDED_LIST_SHOW_BREADCRUMB'].value == 'true'}">?cid=<c:out value="${param.cid}"/></c:if></c:set>
								</c:if>
								<c:choose>
								 <c:when test="${( model.product.recommendedListDisplay == 'quick') or (empty model.product.recommendedListDisplay and siteConfig['RECOMMENDED_LIST_DISPLAY'].value == 'quick')}">
								    <%@ include file="/WEB-INF/jsp/frontend/quickmode/view1.jsp" %>
								 </c:when>
								 <c:when test="${( model.product.recommendedListDisplay == '2') or (empty model.product.recommendedListDisplay and siteConfig['RECOMMENDED_LIST_DISPLAY'].value == '2')}">
								    <%@ include file="/WEB-INF/jsp/frontend/thumbnail/view2.jsp" %>
								 </c:when>
								 <c:when test="${( model.product.recommendedListDisplay == '3') or (empty model.product.recommendedListDisplay and siteConfig['RECOMMENDED_LIST_DISPLAY'].value == '3')}">
									<%@ include file="/WEB-INF/jsp/frontend/thumbnail/view3.jsp" %>
								 </c:when>
								 <c:when test="${( model.product.recommendedListDisplay == '4') or (empty model.product.recommendedListDisplay and siteConfig['RECOMMENDED_LIST_DISPLAY'].value == '4')}">
									<%@ include file="/WEB-INF/jsp/frontend/thumbnail/view4.jsp" %>
								 </c:when>
								 <c:when test="${( model.product.recommendedListDisplay == '5') or (empty model.product.recommendedListDisplay and siteConfig['RECOMMENDED_LIST_DISPLAY'].value == '5')}">
									<%@ include file="/WEB-INF/jsp/frontend/thumbnail/view5.jsp" %>
								 </c:when>
								 <c:when test="${( model.product.recommendedListDisplay == '6') or (empty model.product.recommendedListDisplay and siteConfig['RECOMMENDED_LIST_DISPLAY'].value == '6')}">
									<%@ include file="/WEB-INF/jsp/frontend/thumbnail/view6.jsp" %>
								 </c:when>
								 <c:when test="${( model.product.recommendedListDisplay == '7') or (empty model.product.recommendedListDisplay and siteConfig['RECOMMENDED_LIST_DISPLAY'].value == '7')}">
									<%@ include file="/WEB-INF/jsp/frontend/thumbnail/view7.jsp" %>
								 </c:when>
								 <c:when test="${( model.product.recommendedListDisplay == '8') or (empty model.product.recommendedListDisplay and siteConfig['RECOMMENDED_LIST_DISPLAY'].value == '8')}">
									<%@ include file="/WEB-INF/jsp/frontend/thumbnail/view8.jsp" %>
								 </c:when>
								 <c:when test="${( model.product.recommendedListDisplay == '9') or (empty model.product.recommendedListDisplay and siteConfig['RECOMMENDED_LIST_DISPLAY'].value == '9')}">
									<%@ include file="/WEB-INF/jsp/frontend/thumbnail/view9.jsp" %>
								 </c:when>
								 <c:when test="${( model.product.recommendedListDisplay == '10') or (empty model.product.recommendedListDisplay and siteConfig['RECOMMENDED_LIST_DISPLAY'].value == '10')}">
								   <%@ include file="/WEB-INF/jsp/frontend/thumbnail/view10.jsp" %>
								 </c:when>
								 <c:when test="${( model.product.recommendedListDisplay == '11') or (empty model.product.recommendedListDisplay and siteConfig['RECOMMENDED_LIST_DISPLAY'].value == '11')}">
								    <%@ include file="/WEB-INF/jsp/frontend/thumbnail/view11.jsp" %>
								 </c:when>
								 <c:when test="${( model.product.recommendedListDisplay == '12') or (empty model.product.recommendedListDisplay and siteConfig['RECOMMENDED_LIST_DISPLAY'].value == '12')}">
								   <%@ include file="/WEB-INF/jsp/frontend/thumbnail/view12.jsp" %>
								 </c:when>
								 <c:when test="${( model.product.recommendedListDisplay == '15') or (empty model.product.recommendedListDisplay and siteConfig['RECOMMENDED_LIST_DISPLAY'].value == '15')}">
								    <%@ include file="/WEB-INF/jsp/frontend/viatrading/recomendedView.jsp" %>
								 </c:when>
								 <c:otherwise>
									<%@ include file="/WEB-INF/jsp/frontend/thumbnail/view1.jsp" %>
								 </c:otherwise>
								</c:choose>
								</c:forEach>
								</div>
								</c:if>
								 --%>
					  </c:when>
					  <c:otherwise>
					  <c:out value="${tab.tabContent}" escapeXml="false" /> 
					  </c:otherwise>
					</c:choose>
					<!-- end tab -->
					</div>
				</c:forEach>
				</div>
			</div>
			</div>	
			</c:if>
			</div>
			</c:if>
	</tiles:putAttribute>
</tiles:insertDefinition>