<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ taglib uri="/WEB-INF/tlds/spring.tld" prefix="spring" %>

<tiles:insertDefinition name="${_template}" flush="true">
  <tiles:putAttribute name="content" type="string">
  
<div><c:out value="${model.shippingLayout.headerHtml}" escapeXml="false"/></div>  

<div style="width:95%;margin:auto;">

<table border="0" cellpadding="3" cellspacing="1" width="100%">
  <tr><td colspan="2" class="addressBook"><fmt:message key="shoppingcart.shipTo" /></td></tr>
  <tr><td>
    <b><c:out value="${orderForm.order.shipping.firstName}"/> <c:out value="${orderForm.order.shipping.lastName}"/></b><br />
    <span class="addressBook"><c:if test="${orderForm.order.shipping.company != ''}"><c:out value="${orderForm.order.shipping.company}"/>,</c:if>
      <c:out value="${orderForm.order.shipping.addr1}"/><c:if test="${!empty orderForm.order.shipping.addr2}"> <c:out value="${orderForm.order.shipping.addr2}"/></c:if>,
      <c:out value="${orderForm.order.shipping.city}"/>, 
      <c:out value="${orderForm.order.shipping.stateProvince}"/> <c:out value="${orderForm.order.shipping.zip}"/>,
      <c:out value="${countries[orderForm.order.shipping.country]}" /></span>
  </td>
  <td align="right">
    <form action="checkout2.jhtm" method="post">
    <input type="submit" value="<fmt:message key="shoppingcart.shipToAnotherAddress" />" name="_target0"> 
    <input type="hidden" name="newShippingAddress" value="true"> 
    </form>
  </td>
  </tr>
</table> 
<p>

<form action="checkout2.jhtm" method="post" name="shippingForm">
<c:if test="${gSiteConfig['gWEATHER_CHANNEL'] and (orderForm.order.weather != null) }" >
<table border="0" cellpadding="3" cellspacing="1" width="100%">
  <tr><td class="addressBook"><fmt:message key="weather" /></td></tr>
</table>
<table cellpadding="5" cellspacing="5" bgcolor="#DDDDDD">
<tr align="center">
<td>
  <img src="assets/Image/TWC/32x32/<c:out value='${orderForm.order.weather.ccIcon}' />.png" /> <br />
  <c:out value="${orderForm.order.weather.ccTextCondition}" />   
</td>
<td>
  <h4 style="color:#F46227">High</h4> <br /><c:out value="${orderForm.order.weather.high}" />&deg; F
</td>
<td>
  <h4 style="color:#00B2EB">Low</h4> <br /><c:out value="${orderForm.order.weather.low}" />&deg; F
</td>
<td><div style="float:left"><a href="http://www.weather.com/?prod=xoap&par=1044739873" target="blank"><img border="0" src="assets/Image/TWC/logos/TWClogo_64px.png" /></a></div></td>
</tr>
</table>
</c:if>

<table border="0" cellpadding="3" cellspacing="1" width="100%">
  <tr><td class="addressBook"><fmt:message key="shippingMethod" /></td></tr>
</table>

<c:if test="${orderForm.customRatesList != null and orderForm.order.hasRegularShipping}">
<br/><br/>
 <table border="0" cellpadding="2" cellspacing="0" width="100%" class="shoppingCart">
  <tr class="shoppingCart">
	<c:set var="cols" value="0"/>
    <th width="5%" class="invoice"><fmt:message key="line" />#</th>
    <th class="invoice"><fmt:message key="productSku" /></th>
    <th class="invoice"><fmt:message key="productName" /></th>
    <th width="10%" class="invoice"><fmt:message key="quantity" /></th>
    <c:if test="${gSiteConfig['gINVENTORY'] && orderForm.order.hasLowInventoryMessage}">
      <c:set var="cols" value="${cols+1}"/>
	  <th width="10%" class="invoice"><fmt:message key="lowInventory" /></th>
	</c:if>
    <c:if test="${orderForm.order.hasPacking}">
	  <c:set var="cols" value="${cols+1}"/>
      <th width="10%" class="invoice"><fmt:message key="packing" /></th>
    </c:if>
    <c:if test="${orderForm.order.hasContent}">
	  <c:set var="cols" value="${cols+1}"/>
      <th class="invoice"><fmt:message key="content" /></th>
    </c:if>
    <th class="invoice"><fmt:message key="productPrice" /><c:if test="${orderForm.order.hasContent}">/<c:out value="${siteConfig['CASE_UNIT_TITLE'].value}" /></c:if></th>
    <th class="invoice"><fmt:message key="total" /></th>
  </tr>
<c:forEach var="lineItem" items="${orderForm.order.lineItems}" varStatus="lineItemStatus">
<c:if test="${empty lineItem.customShippingCost}">
<tr valign="top" class="shoppingCart${lineItemStatus.index % 2}">
  <td class="invoice" align="center"><c:out value="${lineItem.lineNumber}"/></td>
  <td class="invoice">
    <c:if test="${siteConfig['SHOW_IMAGE_ON_INVOICE'].value == 'true' and lineItem.product.thumbnail != null}">
      <img class="cartImage" src="<c:if test="${!lineItem.product.thumbnail.absolute}">assets/Image/Product/thumb/</c:if>${lineItem.product.thumbnail.imageUrl}" border="0" />
    </c:if><c:out value="${lineItem.product.sku}"/></td>
  <td class="invoice"><c:out value="${lineItem.product.name}"/>
    <table border="0" cellspacing="1" cellpadding="0">
	  <c:forEach items="${lineItem.productAttributes}" var="productAttribute" varStatus="productAttributeStatus">
		<tr class="invoice_lineitem_attributes${productAttributeStatus.count%2}">
		<td class="optionName" style="white-space: nowrap" align="right"><c:out value="${productAttribute.optionName}"/>: </td>
		<td class="optionValue" style="padding-left:5px;"><c:out value="${productAttribute.valueString}" escapeXml="false"/></td>
		<c:if test="${(! empty productAttribute.optionPriceOriginal) && productAttribute.optionPriceOriginal != 0}">
		<td class="optionPrice"><fmt:formatNumber value="${productAttribute.optionPriceOriginal}" pattern="#,##0.00" /></td>
		<td class="optionNote" style="padding-left:5px;"><fmt:message key="${productAttribute.optionPriceMessageF}"/></td>
		</c:if>
		</tr>
	  </c:forEach>
	  <%@ include file="/WEB-INF/jsp/frontend/common/customLinesInvoice.jsp"%>
    </table>
  <c:if test="${siteConfig['SHOW_IMAGE_ON_INVOICE'].value == 'true' and lineItem.hasProductAttributesImage}" >
  <c:forEach items="${lineItem.productAttributes}" var="productAttribute">
    <c:if test="${!empty productAttribute.imageUrl}" > 
      <c:if test="${!cartItem.product.thumbnail.absolute}"><img src="<c:url value="/assets/Image/Product/options/${productAttribute.imageUrl}"/>" border="0" class="invoiceOptionImage" /></c:if>
      <c:if test="${cartItem.product.thumbnail.absolute}"><img src="<c:url value="${productAttribute.imageUrl}"/>" border="0" class="invoiceOptionImage" /></c:if>
    </c:if>
  </c:forEach>
  </c:if> 
  </td>  
  <td class="invoice" align="center"><c:out value="${lineItem.quantity}"/></td>
  <c:if test="${gSiteConfig['gINVENTORY'] && orderForm.order.hasLowInventoryMessage}">
	<td class="invoice" align="center"><c:out value="${lineItem.lowInventoryMessage}" /></td>
  </c:if>
  <c:if test="${orderForm.order.hasPacking}">
  <td class="invoice" align="center"><c:out value="${lineItem.product.packing}" /></td>
  </c:if>
  <c:if test="${orderForm.order.hasContent}">
    <td class="invoice" align="center"><c:out value="${lineItem.product.caseContent}" /></td>
  </c:if>
  <td class="invoice" align="right"><fmt:formatNumber value="${lineItem.unitPrice}" pattern="#,##0.00"/></td>
  <td class="invoice" align="right"><fmt:formatNumber value="${lineItem.totalPrice}" pattern="#,##0.00"/></td>
 </tr>
 </c:if>
 </c:forEach>
 </table>
</c:if>

<c:if test="${orderForm.order.hasRegularShipping}">
<spring:bind path="orderForm.shippingRateIndex">
<span class="error"><c:out value="${status.errorMessage}"/></span>
<table border="0" cellpadding="2">
  <c:forEach items="${orderForm.ratesList}" var="shippingrate" varStatus="loopStatus">
    <tr>
      <td align="center" height="45px">
        <c:if test="${shippingrate.carrier == 'fedex'}" >
        <c:set var="fedEx" value="true" />
        <c:choose>
         <c:when test="${fn:contains(shippingrate.code,'EXPRESS') or fn:contains(shippingrate.code,'OVERNIGHT') or fn:contains(shippingrate.code,'2_DAY')}"><img width="80%" src="assets/Image/Layout/FedEx_Express_Normal_2Color_Positive_20.gif" /></c:when>
         <c:when test="${fn:contains(shippingrate.code,'GROUND_HOME')}"><img width="80%" src="assets/Image/Layout/FedEx_HomeDelivery_Normal_2Color_Positive_20.gif" /></c:when>
         <c:when test="${fn:contains(shippingrate.code,'GROUND')}"><img width="80%" src="assets/Image/Layout/FedEx_Ground_Normal_2Color_Positive_20.gif" /></c:when>
         <c:when test="${fn:contains(shippingrate.code,'FREIGHT')}"><img width="80%" src="assets/Image/Layout/FedEx_Freight_Normal_2Color_Positive_20.gif" /></c:when>
         <c:otherwise><img width="80%" src="assets/Image/Layout/FedEx_MultipleServices_Normal_2Color_Positive_20.gif" /></c:otherwise>
        </c:choose>
        </c:if>
        <c:if test="${shippingrate.carrier == 'ups'}" >
         <c:set var="ups" value="true" />
         <img width="30%" src="assets/Image/Layout/UPS_LOGO_S.gif" />
        </c:if>
      </td>
      <td>
       <input type="radio" id="scr_${loopStatus.index}" name="<c:out value="${status.expression}"/>" value="${loopStatus.index}" <c:if test="${(status.value == loopStatus.index) || (shippingrate.title == 'Free')}">checked</c:if>>
      </td>
      <td>
        <c:out value="${shippingrate.title}" escapeXml="false"/>
      </td>
      <td align="right">
        <c:if test="${shippingrate.price != null}"><fmt:message key="${siteConfig['CURRENCY'].value}" /></c:if><fmt:formatNumber value="${shippingrate.price}" pattern="#,##0.00"/>
      </td>
      <c:if test="${siteConfig['DELIVERY_TIME'].value == 'true' and shippingrate.carrier == 'ups'}">
      <td style="padding-left:10px;"align="center">
        <c:choose>
 	 	  <c:when test="${shippingrate.code == '03' and empty shippingrate.deliveryDate}"><a onclick="window.open(this.href,'','width=740,height=550'); return false;" href="category.jhtm?cid=49">See Map</a></c:when>
 	 	  <c:when test="${shippingrate.code == '03' and !empty shippingrate.deliveryDate}">Delivery Date: <fmt:formatDate type="date" timeStyle="default" value="${shippingrate.deliveryDate}"/><div style="font-size:10px;color:#444444;">( Not Guaranteed )</div></c:when>
 	 	  <c:otherwise>Delivery Date: <fmt:formatDate type="date" timeStyle="default" value="${shippingrate.deliveryDate}"/></c:otherwise>
 		</c:choose>
      </td>
      </c:if> 
      <c:if test="${shippingrate.description != null}">
      <td>
        <c:out value="${shippingrate.description}" escapeXml="false"/>
      </td>
      </c:if>
    </tr>
  </c:forEach>
</table>
</spring:bind>
<c:if test="${fedEx}">
 <c:out value="FedEx<sup>&#174;</sup> service marks are owned by Federal Express Corporation and used with permission." escapeXml="false"/>
</c:if>
<c:if test="${ups}"><br />
 <c:out value="UPS, the UPS brand mark, and the Color Brown are trademarks of 
United Parcel Service of America, Inc. All Rights Reserved." escapeXml="false"/>
</c:if>
</c:if>


<c:if test="${orderForm.customRatesList != null}">
<c:if test="${orderForm.order.hasRegularShipping}">
<br/><br/>
<table border="0" cellpadding="2" cellspacing="0" width="100%" class="shoppingCart">
  <tr class="shoppingCart">
	<c:set var="cols" value="0"/>
    <th width="5%" class="invoice"><fmt:message key="line" />#</th>
    <th class="invoice"><fmt:message key="productSku" /></th>
    <th class="invoice"><fmt:message key="productName" /></th>
    <th width="10%" class="invoice"><fmt:message key="quantity" /></th>
    <c:if test="${gSiteConfig['gINVENTORY'] && orderForm.order.hasLowInventoryMessage}">
      <c:set var="cols" value="${cols+1}"/>
	  <th width="10%" class="invoice"><fmt:message key="lowInventory" /></th>
	</c:if>
    <c:if test="${orderForm.order.hasPacking}">
	  <c:set var="cols" value="${cols+1}"/>
      <th width="10%" class="invoice"><fmt:message key="packing" /></th>
    </c:if>
    <c:if test="${orderForm.order.hasContent}">
	  <c:set var="cols" value="${cols+1}"/>
      <th class="invoice"><fmt:message key="content" /></th>
    </c:if>
    <th class="invoice"><fmt:message key="productPrice" /><c:if test="${orderForm.order.hasContent}">/<c:out value="${siteConfig['CASE_UNIT_TITLE'].value}" /></c:if></th>
    <th class="invoice"><fmt:message key="total" /></th>
  </tr>
<c:forEach var="lineItem" items="${orderForm.order.lineItems}" varStatus="lineItemStatus">
<c:if test="${!empty lineItem.customShippingCost}">
<tr valign="top" class="shoppingCart${lineItemStatus.index % 2}">
  <td class="invoice" align="center"><c:out value="${lineItem.lineNumber}"/></td>
  <td class="invoice">
    <c:if test="${siteConfig['SHOW_IMAGE_ON_INVOICE'].value == 'true' and lineItem.product.thumbnail != null}">
      <img class="cartImage" src="<c:if test="${!lineItem.product.thumbnail.absolute}">assets/Image/Product/thumb/</c:if>${lineItem.product.thumbnail.imageUrl}" border="0" />
    </c:if><c:out value="${lineItem.product.sku}"/></td>
  <td class="invoice"><c:out value="${lineItem.product.name}"/>
    <table border="0" cellspacing="1" cellpadding="0">
	  <c:forEach items="${lineItem.productAttributes}" var="productAttribute" varStatus="productAttributeStatus">
		<tr class="invoice_lineitem_attributes${productAttributeStatus.count%2}">
		<td class="optionName" style="white-space: nowrap" align="right"><c:out value="${productAttribute.optionName}"/>: </td>
		<td class="optionValue" style="padding-left:5px;"><c:out value="${productAttribute.valueString}" escapeXml="false"/></td>
		<c:if test="${(! empty productAttribute.optionPriceOriginal) && productAttribute.optionPriceOriginal != 0}">
		<td class="optionPrice"><fmt:formatNumber value="${productAttribute.optionPriceOriginal}" pattern="#,##0.00" /></td>
		<td class="optionNote" style="padding-left:5px;"><fmt:message key="${productAttribute.optionPriceMessageF}"/></td>
		</c:if>
		</tr>
	  </c:forEach>
	  <%@ include file="/WEB-INF/jsp/frontend/common/customLinesInvoice.jsp"%>
    </table>
  <c:if test="${siteConfig['SHOW_IMAGE_ON_INVOICE'].value == 'true' and lineItem.hasProductAttributesImage}" >
  <c:forEach items="${lineItem.productAttributes}" var="productAttribute">
    <c:if test="${!empty productAttribute.imageUrl}" > 
      <c:if test="${!cartItem.product.thumbnail.absolute}"><img src="<c:url value="/assets/Image/Product/options/${productAttribute.imageUrl}"/>" border="0" class="invoiceOptionImage" /></c:if>
      <c:if test="${cartItem.product.thumbnail.absolute}"><img src="<c:url value="${productAttribute.imageUrl}"/>" border="0" class="invoiceOptionImage" /></c:if>
    </c:if>
  </c:forEach>
  </c:if> 
  </td>  
  <td class="invoice" align="center"><c:out value="${lineItem.quantity}"/></td>
  <c:if test="${gSiteConfig['gINVENTORY'] && orderForm.order.hasLowInventoryMessage}">
	<td class="invoice" align="center"><c:out value="${lineItem.lowInventoryMessage}" /></td>
  </c:if>
  <c:if test="${orderForm.order.hasPacking}">
  <td class="invoice" align="center"><c:out value="${lineItem.product.packing}" /></td>
  </c:if>
  <c:if test="${orderForm.order.hasContent}">
    <td class="invoice" align="center"><c:out value="${lineItem.product.caseContent}" /></td>
  </c:if>
  <td class="invoice" align="right"><fmt:formatNumber value="${lineItem.unitPrice}" pattern="#,##0.00"/></td>
  <td class="invoice" align="right"><fmt:formatNumber value="${lineItem.totalPrice}" pattern="#,##0.00"/></td>
</tr>
</c:if>
</c:forEach>
</table>
</c:if>
<spring:bind path="orderForm.customShippingRateIndex">
<span class="error"><c:out value="${status.errorMessage}"/></span>
<table border="0" cellpadding="2">
  <c:forEach items="${orderForm.customRatesList}" var="shippingrate" varStatus="loopStatus">
    <tr>
      <td>
        <input type="radio" id="ccr_${loopStatus.index}" name="<c:out value="${status.expression}"/>" value="${loopStatus.index}" <c:if test="${status.value == loopStatus.index}">checked</c:if>>
      </td>
      <td>
        <c:out value="${shippingrate.title}" escapeXml="false"/>
      </td>
      <td align="right">
        <c:if test="${shippingrate.price != null}"><fmt:message key="${siteConfig['CURRENCY'].value}" /></c:if><fmt:formatNumber value="${shippingrate.price}" pattern="#,##0.00"/>
      </td>  
    </tr>
  </c:forEach>
</table>
</spring:bind>
</c:if>
<div align="center" class="nbButton" style="margin:5px;">
<div align="right" class="next"><input type="image" src="assets/Image/Layout/button_next${_lang}.gif" name="_target3"></div>
<div align="left" class="back" style="display:none;"><input type="image" src="assets/Image/Layout/button_prev${_lang}.gif" name="_viewcart"></div>
</div>
</form>

</div>
<c:out value="${model.shippingLayout.footerHtml}" escapeXml="false"/>

</tiles:putAttribute>
</tiles:insertDefinition>
