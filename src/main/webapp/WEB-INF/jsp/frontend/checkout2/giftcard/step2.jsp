<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="/WEB-INF/tlds/spring.tld" prefix="spring" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>

<tiles:insertDefinition name="${_template}" flush="true">
  <tiles:putAttribute name="content" type="string">
<script type="text/JavaScript">
<!--

//-->
</script>
  
<c:if test="${model.message != null}">
  <div class="message"><fmt:message key="${model.message}" /></div>
</c:if>

<form:form  commandName="giftCardForm" method="post">
<input type="hidden" name="_page" value="1">
<table width="100%" border="0" cellpadding="3" class="giftcardBox">

  <tr>
    <td colspan="2" style="font-weight:700;color:#DD3204"><fmt:message key="giftCard" /></td>
  </tr>
  <tr>
    <td colspan="2" style="font-weight:700;color:#1C1C1C"><fmt:message key="fillInTheInformationBelowAndClickContinue" /></td>
  </tr>  
  <tr>
    <td><form:errors path="giftCardOrder.creditCard.*" cssClass="errorBox"/></td>
  </tr>
  <tr>
    <td>
	  <ol>
	   <li style="list-style-type:none"><fmt:message key="billingInformation" />
	    <table class="box">
	     <tr>
	      <td><fmt:message key="firstName" />:</td>
	      <td><form:input path="giftCardOrder.billing.firstName" size="20"/><form:errors path="giftCardOrder.billing.firstName" /></td>
	     </tr>
	     <tr>
	      <td><fmt:message key="lastName" />:</td>
	      <td><form:input path="giftCardOrder.billing.lastName" size="20"/><form:errors path="giftCardOrder.billing.lastName" /></td>
	     </tr>
	     <tr>
	      <td><fmt:message key="country" />:</td>
	      <td><form:input path="giftCardOrder.billing.country" size="20"/><form:errors path="giftCardOrder.billing.country" /></td>
	     </tr>
	     <tr>
	      <td><fmt:message key="address" />1:</td>
	      <td><form:input path="giftCardOrder.billing.addr1" size="20"/><form:errors path="giftCardOrder.billing.addr1" /></td>
	     </tr>
	     <tr>
	      <td><fmt:message key="address" />2:</td>
	      <td><form:input path="giftCardOrder.billing.addr2" size="20"/><form:errors path="giftCardOrder.billing.addr2" /></td>
	     </tr>
	     <tr>
	      <td><fmt:message key="city" />:</td>
	      <td><form:input path="giftCardOrder.billing.city" size="20"/><form:errors path="giftCardOrder.billing.city" /></td>
	     </tr>
	     <tr>
	      <td><fmt:message key="stateProvince" />:</td>
	      <td>
	        <form:select path="giftCardOrder.billing.stateProvince">
	          <c:forEach items="${model.statelist}" var="state">
  	            <option value="${state.code}" <c:if test="${state.code == status.value}">selected</c:if> >${state.name}</option>
		      </c:forEach>  
		    </form:select>   
	          <form:errors path="giftCardOrder.billing.stateProvince" />
	      </td>
	     </tr>
	     <tr>
	      <td><fmt:message key="zipPostalCode" />:</td>
	      <td><form:input path="giftCardOrder.billing.zip" size="20"/><form:errors path="giftCardOrder.billing.zip" /></td>
	     </tr>
	     <tr>
	      <td><fmt:message key="phone" />:</td>
	      <td><form:input path="giftCardOrder.billing.phone" size="20"/><form:errors path="giftCardOrder.billing.phone" /></td>
	     </tr>
	    </table> 
	    </li>
	    
	    <li style="list-style-type:none"><fmt:message key="payWithCreditCard" />
	    <table class="box">
	     <tr>
	      <td><fmt:message key="cardType" />:*</td>
	      <td>
	      <form:select path="giftCardOrder.creditCard.type">
	       <c:forTokens items="VISA,AMEX,MC,DISC" delims="," var="cc">
             <option value="${cc}" <c:if test="${status.value == cc}">selected</c:if>><fmt:message key="${cc}"/></option>
           </c:forTokens>
	      </form:select>
	      </td>
	     </tr>
	     <tr>
	      <td><fmt:message key="cardNumber" />:*</td>
	      <td><form:input path="giftCardOrder.creditCard.number" maxlength="16" size="16"/><form:errors path="giftCardOrder.creditCard.number" cssClass="error"/></td>
	     </tr>
	     <tr>
	      <td><fmt:message key="cardVerificationCode" />:*</td>
	      <td><form:input path="giftCardOrder.creditCard.cardCode" maxlength="4" size="4"/><form:errors path="giftCardOrder.creditCard.cardCode" cssClass="error"/></td>
	     </tr>
	     <tr>
	      <td><fmt:message key="expDate" /> (MM/YY):*</td>
	      <td>
	      <form:select path="giftCardOrder.creditCard.expireMonth">
	       <c:forTokens items="01,02,03,04,05,06,07,08,09,10,11,12" delims="," var="ccExpireMonth">
            <option value="${ccExpireMonth}" <c:if test="${status.value==ccExpireMonth}">selected</c:if>><c:out value="${ccExpireMonth}"/></option>
           </c:forTokens>
	      </form:select>
	      <form:select path="giftCardOrder.creditCard.expireYear">
	       <c:forTokens items="07,08,09,10,11,12,13,14,15" delims="," var="ccExpireYear">
            <option value="${ccExpireYear}" <c:if test="${status.value==ccExpireYear}">selected</c:if>><c:out value="${ccExpireYear}"/></option>
           </c:forTokens>
	      </form:select>
	      </td>
	     </tr>
	    </table> 
	    </li>
	  </ol>
	</td>
    <td></td>
  </tr>  
  <tr align="center">
    <td colspan="2">
	  <input type="submit" name="_target0" value="<fmt:message key="back" />" />
	  <input type="submit" name="_target2" value="<fmt:message key="next" />" />
    </td>
  </tr>
  </table>
  
</form:form>

  </tiles:putAttribute>
</tiles:insertDefinition>