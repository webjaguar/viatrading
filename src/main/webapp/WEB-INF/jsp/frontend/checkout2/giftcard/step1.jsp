<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="/WEB-INF/tlds/spring.tld" prefix="spring" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>

<tiles:insertDefinition name="${_template}" flush="true">
  <tiles:putAttribute name="content" type="string">
<script type="text/JavaScript">
<!--

//-->
</script>
  
<c:if test="${model.message != null}">
  <div class="message"><fmt:message key="${model.message}" /></div>
</c:if>

<form:form  commandName="giftCardForm" method="post">
<input type="hidden" name="_page" value="0">
<table width="100%" border="0" cellpadding="3" class="giftcardBox">

  <tr>
    <td colspan="2" style="font-weight:700;color:#DD3204"><fmt:message key="giftCard" /></td>
  </tr>
  <tr>
    <td colspan="2" style="font-weight:700;color:#1C1C1C"><fmt:message key="fillInTheInformationBelowAndClickContinue" /></td>
  </tr>  

  <tr>
    <td>
	  <ol><!--
	  	<li><fmt:message key="f_pleaseSelectGiftCardType"/>
	  	<table class="box">
	  	<tr>
	  	  <td><fmt:message key="f_emailAGiftCard"/>:</td>
	      <td><form:radiobutton path="giftCard.type" value="false"/><div class="note">(Specify the amount in both dollars and cents, or just in dollars.)</div></td>
	    </tr>
	    <tr>
	      <td><fmt:message key="f_mailAGiftCard"/>:</td>
	      <td><form:radiobutton path="giftCard.type" value="true"/></td>
	  	</tr>
	  	</table>
	  	</li>  -->
	    <li><fmt:message key="chooseAnAmountBetween"><fmt:param value="${siteConfig['CURRENCY'].value}"/><fmt:param value="${siteConfig['GIFTCARD_MIN_AMOUNT'].value}"/><fmt:param value="${siteConfig['CURRENCY'].value}"/><fmt:param value="${siteConfig['GIFTCARD_MAX_AMOUNT'].value}"/></fmt:message>
	    <table class="box">
	     <tr>
	      <td><fmt:message key="amount" />:</td>
	      <td><form:input path="giftCard.amount" size="5"/><form:errors path="giftCard.amount" cssClass="error"/><div class="note">(<fmt:message key="specifyTheAmountInBothDollarsAndCentsOrJustInDollars" />)</div></td>
	     </tr>
	     <tr>
	      <td><fmt:message key="quantity" />:</td>
	      <td><form:input path="giftCardOrder.quantity" size="5" maxlength="3"/><form:errors path="giftCardOrder.quantity" cssClass="error"/></td>
	     </tr>
	    </table> 
	    </li>
	    <li><fmt:message key="enterToAndFromAsYou'dLikeThemToAppearOnTheGiftCard" />
	    <table class="box">
	     <tr>
	      <td><fmt:message key="to" />:</td>
	      <td><form:input path="giftCard.recipientFirstName"/><div class="note">(recipient's first name here)</div></td>
	      <td><form:input path="giftCard.recipientLastName"/><form:errors path="giftCard.recipientFirstName" cssClass="error"/> <form:errors path="giftCard.recipientLastName" cssClass="error"/><div class="note">(last name here)</div></td>
	     </tr>
	     <tr>
	      <td><fmt:message key="from" />:</td>
	      <td><form:input path="giftCard.senderFirstName"/><div class="note">(your first name here)</div></td>
	      <td><form:input path="giftCard.senderLastName"/><form:errors path="giftCard.senderFirstName" cssClass="error"/> <form:errors path="giftCard.senderLastName" cssClass="error"/><div class="note">(last name here)</div></td>
	     </tr>
	    </table>
	    </li>
	    <li><fmt:message key="whatIsTheRecipientsEmailAddress" />
	    <table class="box">
	     <tr>
	      <td><fmt:message key="email" />:</td>
	      <td><form:input path="giftCard.recipientEmail"/><form:errors path="giftCard.recipientEmail" cssClass="error"/></td>
	     </tr>
	     <tr> 
	      <td><fmt:message key="confirmEmail" />:</td>
	      <td><form:input path="confirmEmail"/><form:errors path="confirmEmail" cssClass="error"/></td>
	     </tr>
	    </table>
	    </li>
	    <li><fmt:message key="wouldYouLikeToIncludeAMessageForTheRecipient" /> (<fmt:message key="optional" />)
	    <table class="box">
	     <tr>
	      <td><fmt:message key="message" />:</td>
	      <td><form:textarea path="giftCard.message" rows="9" cols="40"/></td>
	     </tr>
	    </table>
		</li>
	  </ol>
	</td>
    <td></td>
  </tr>  
  <tr align="center">
    <td colspan="2">
      <input type="submit" class="f_button" name="_cancel" value="<fmt:message key="cancel" />" />
	  <input type="submit" name="_target1" value="<fmt:message key="next" />" />
    </td>
  </tr>
  </table>
  
</form:form>

  </tiles:putAttribute>
</tiles:insertDefinition>