<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib uri="/WEB-INF/tlds/spring.tld" prefix="spring" %>

<tiles:insertDefinition name="${_template}" flush="true">
  <tiles:putAttribute name="content" type="string">
<script type="text/JavaScript">
<!--
<c:if test="${gSiteConfig['gGIFTCARD']}">
function checkCode(){
  if ( document.getElementById('tempGiftCardCode').value == "" ) {
  	alert("<fmt:message key='giftcard.empty' />");
  	return false;
  }
}
</c:if>
function openW(thisURL) {
	var options="toolbar=no,location=no,directories=no,status=no,"
		+ "menubar=no,scrollbars=yes,resizable=no,copyhistory=no,"
		+ "width=550,height=300";
	cvv2 = window.open(thisURL,"cvv2",options);
	cvv2.focus();
	return true;
}
//-->
</script>

<div><c:out value="${model.billingLayout.headerHtml}" escapeXml="false"/></div>
<div style="width:95%;margin:auto;">

<c:if test="${!empty orderForm.errorMessage}" >
<font color="red"><fmt:message key="${orderForm.errorMessage}" /></font>
</c:if>
<table border="0" cellpadding="3" cellspacing="1" width="100%">

  <tr><td colspan="2" class="addressBook"><fmt:message key="shoppingcart.billTo" /></td></tr>
  <tr><td>
    <b><c:out value="${orderForm.order.billing.firstName}"/> <c:out value="${orderForm.order.billing.lastName}"/></b><br>
    <span class="addressBook"><c:if test="${orderForm.order.billing.company != ''}"><c:out value="${orderForm.order.billing.company}"/>,</c:if>
      <c:out value="${orderForm.order.billing.addr1}"/><c:if test="${!empty orderForm.order.billing.addr2}"> <c:out value="${orderForm.order.billing.addr2}"/></c:if>,
      <c:out value="${orderForm.order.billing.city}"/>, 
      <c:out value="${orderForm.order.billing.stateProvince}"/> <c:out value="${orderForm.order.billing.zip}"/>,
      <c:out value="${countries[orderForm.order.billing.country]}" /></span>
  </td>
  <td align="right">
    <form action="checkout2.jhtm" method="post">
    <input type="submit" value="<fmt:message key="billToAnotherAddress" />" name="_target2">  
    <input type="hidden" name="newBillingAddress" value="true">
    </form>
  </td>
  </tr>
</table> 
<p>

<c:if test="${siteConfig['SHOW_GRANDTOTAL_BILLING_METHOD'].value == 'true'}">
<table class="orderSummary" border="0" cellpadding="3" cellspacing="1" width="100%">
  <tr><td colspan="2" class="addressBook"><fmt:message key="shoppingcart.orderSummery" /></td></tr>
  <tr><td>
  <table class="orderSummaryInside" border="0" cellpadding="3" cellspacing="1" width="25%">
  <tr>
    <td><fmt:message key="subTotal"/>:</td>
  	<td><fmt:message key="${siteConfig['CURRENCY'].value}" /><fmt:formatNumber value="${orderForm.order.subTotal}" pattern="#,##0.00"/></td>
  </tr>
  <c:if test="${orderForm.order.shippingCost != null}">
  <tr>
    <td><fmt:message key="shipping"/>:</td>
  	<td><fmt:message key="${siteConfig['CURRENCY'].value}" /><fmt:formatNumber value="${orderForm.order.shippingCost}" pattern="#,##0.00"/></td>
  </tr>
  </c:if>
  <c:if test="${orderForm.order.hasCustomShipping}">
  <tr>
    <td><fmt:message key="customShipping" /> (<c:out value="${orderForm.order.customShippingTitle}" escapeXml="false"/>):</td>
    <td><fmt:message key="${siteConfig['CURRENCY'].value}" /><fmt:formatNumber value="${orderForm.order.customShippingCost}" pattern="#,##0.00"/></td>
  </tr>  
  </c:if>
  <c:if test="${siteConfig['BUY_SAFE_URL'].value != ''}">
  <tr>
    <td>buySAFE :</td>
  	<td><fmt:message key="${siteConfig['CURRENCY'].value}" />      <c:choose>
        <c:when test="${orderForm.order.wantsBond}"><fmt:formatNumber value="${orderForm.order.bondCost}" pattern="#,##0.00"/>
        </c:when><c:otherwise><fmt:formatNumber value="0.00"/></c:otherwise></c:choose> <a href="viewCart.jhtm">( change )</a>
	</td>
  </tr>
  </c:if>
  <tr>
    <td><b><fmt:message key="total"/>:</b></td>
  	<td><b><fmt:message key="${siteConfig['CURRENCY'].value}" /><fmt:formatNumber value="${orderForm.order.grandTotal}" pattern="#,##0.00"/></b></td>
  </tr>
  </table>
  </td></tr>
</table>
</c:if>

<form:form commandName="orderForm" action="checkout2.jhtm" method="post">

<table border="0" cellpadding="3" cellspacing="1" width="100%">
  <tr><td class="addressBook"><fmt:message key="f_paymentMethod" /></td></tr>
</table>

<spring:bind path="orderForm.order.paymentMethod">
<span class="error"><c:out value="${status.errorMessage}"/></span>
</spring:bind>

<c:choose>
  <c:when test="${orderForm.custPayment != null and orderForm.custPayment != ''}">
<div><input type="radio" value="<c:out value="${orderForm.custPayment}"/>" name="order.paymentMethod" checked> <b><c:out value="${orderForm.custPayment}"/></b></div>
  </c:when>
  
  <c:otherwise>
<div id="paymentWrapper">
	    
<c:if test="${siteConfig['GEMONEY_DOMAIN'].value != ''
	and fn:trim(siteConfig['GEMONEY_MERCHANTID'].value) != ''
	and fn:trim(siteConfig['GEMONEY_PASSWORD'].value) != ''
	}">
<c:set value="true" var="gemoneybox"/>
<div id="creditCard" class="payment">
 <div class="ritWrapper">
   <div class="radio"><form:radiobutton path="order.paymentMethod" id="ge_radio" value="GE Money" onclick="togglePayment()"/></div> 
   <div id="geMoneyTextImageBox" class="imgText">
     <div id="geMoneyText" class="text">
       <b><c:out value="${siteConfig['GEMONEY_PROGRAM_NAME'].value}"/></b>
     </div>
     <div id="geMoneyImage" class="image"></div>
     <div id="geMoneyNone" class="note"></div>
   </div>
 </div>
</div>
<div id="gemoneybox" <c:if test="${orderForm.order.paymentMethod != 'GE Money'}">style="display:none;"</c:if>> 
<div id="ccInfo">
<table border="0" cellspacing="0" cellpadding="0" width="100%">
<tr>
<td valign="top">
<table border="0" cellspacing="0" cellpadding="0" width="100%">
   <tr>
     <td class="formName"><fmt:message key="accountNumber" />:*</td>
     <td>
       <form:input path="order.geMoney.acctNumber" maxlength="16" size="16" htmlEscape="true" />
       <form:errors path="order.geMoney.acctNumber" cssClass="error" />
	 </td>
   </tr>
   <tr>
     <td class="formName">&nbsp;</td>
     <td>-- AND/OR --</td>
   </tr>
   <tr>
     <td class="formName"><fmt:message key="ssn" />:*</td>
     <td>
       <form:input path="order.geMoney.ssn" maxlength="9" size="16" htmlEscape="true" />
       <form:errors path="order.geMoney.ssn" cssClass="error" />
	 </td>
   </tr>   
   <form:errors path="order.billing.phone">   
   <tr>
     <td class="formName"><fmt:message key="billing" /> <fmt:message key="phone" />:*</td>
     <td>
       <form:input path="order.billing.phone" maxlength="10" size="16" htmlEscape="true"/>
       <font color="red">Home Phone Number must be exactly 10 numbers and can not start with a 1.</font>
	 </td>
   </tr>
   </form:errors>
   <form:errors path="order.billing.zip">
   <tr>
     <td class="formName"><fmt:message key="billing" /> <fmt:message key="zipCode" />:</td>
     <td>
       <c:out value="${orderForm.order.billing.zip}"/>
       <font color="red">Zip Code must be 5 numbers. Please go to My Account to correct your zipcode.</font>
	 </td>
   </tr>
   </form:errors>   
   <tr>
     <td class="formName">&nbsp;</td><td>&nbsp;</td>
   </tr>
</table>
</td>
<td width="400" valign="top">
Already have a finance account with us? Just enter in your account #<br/><br/>
Need to apply for financing? Just enter your SS# and you will be prompted to the application process once checkout is complete.<br/><br/>
Before we can process your order, the customer MUST fax or email a copy of your driver's license and a utility bill with your name and address to 1-636-438-1351 OR finance@number1direct.com.
</td>
</tr>
</table>
</div>
</div>
</c:if>

<c:if test="${gSiteConfig['gPAYPAL'] < 2 and siteConfig['CREDIT_CARD_PAYMENT'].value != ''}">
<spring:bind path="orderForm.order.paymentMethod">
<div id="creditCard" class="payment">
 <div class="ritWrapper">
   <div class="radio"><input type="radio" value="Credit Card" name="<c:out value="${status.expression}"/>" id="cc_radio" onclick="togglePayment()" <c:if test="${status.value == 'Credit Card'}">checked</c:if>></div> 
   <div id="ccTextImageBox" class="imgText">
     <div id="ccText" class="text">
       <b><fmt:message key="shoppingcart.creditcard" /></b>
     </div>
     <div id="ccImage" class="image"></div>
     <div id="paypalNone" class="note"></div>
   </div>
 </div>
</div>
</spring:bind>
<div id="creditcardbox" <c:if test="${orderForm.order.paymentMethod != 'Credit Card'}">style="display:none;"</c:if>> 

<c:if test="${centinelAuthenticationFailure}">
<div align="center">
<b>Authentication Failed</b><br/><br/>
Your financial institution has indicated that it could not successfully authenticate this transaction. To protect against unauthorized use, this card cannot be used to complete your purchase. You may complete the purchase by selecting another form of payment.
<br/><br/>
</div>
</c:if>

<form:errors path="order.creditCard.expireMonth">
<c:if test="${siteConfig['CC_FAILURE_URL'].value != ''}">
<c:catch var="ccFailureUrlException">
<c:import url="${siteConfig['CC_FAILURE_URL'].value}"/>
</c:catch>
</c:if>
<c:if test="${siteConfig['CC_FAILURE_URL'].value == '' or ccFailureUrlException != null}">
<div id="ccInfoError">
	<table border="0" cellspacing="10" cellpadding="0" align="center" >
	  <tr>
	    <td><b><fmt:message key="shoppingcart.pleaseCallUs" /><fmt:message key="shoppingcart.thereSeemsToBeAProblemWithYourCCInformation" /></b><br />
			<fmt:message key="shoppingcart.ForYourReferenceATransmissionErrorEmailHasBeenForwardedToYou" />
			</td>
	  </tr>
	  <tr>
	    <td><b><fmt:message key="shoppingcart.mostErrorsAreSimplyTypographical" /></b><br />
			<fmt:message key="shoppingcart.weAskYouToReviewTheFollowing" /></td>
	  </tr>
	  <tr>
	    <td><div style="color:red;text-align:left">
	      <ul>
	        <li><fmt:message key="shoppingcart.creditCardNumber" /></li>
	        <li><fmt:message key="shoppingcart.ccidNumberCVV2" />
	          <ul>
	              <li><fmt:message key="shoppingcart.visaMasterCardLast3difitsOnBackOfCard" /></li>
	            <li><fmt:message key="shoppingcart.americanExp4DigitsInSmallPrintInTheUpper" /><br />
	              <fmt:message key="shoppingcart.rightSectionOfTheFrontOfTheCard" /></li>
	          </ul>
	        </li>
	        <li><fmt:message key="expirationDate" /></li>
	      </ul>
	    </div></td>
	  </tr>
	  <tr>
	    <td><div align="center"><fmt:message key="shoppingcart.ifYouContinueToGetThisError" /></div></td>
	  </tr>
	  <tr>
	    <td><div align="center"><b><fmt:message key="shoppingcart.needHelp" /><br />
	      <fmt:message key="shoppingcart.pleaseCallUs" />
	      <c:out value="${siteConfig['COMPANY_PHONE'].value}"/>
	    </b></div></td>
	  </tr>
	</table>
</div>
</c:if>
</form:errors>

<div id="ccInfo">
<table border="0" cellspacing="0" cellpadding="0" class="ccInfoForm">
  <tr>
    <td class="formName"><fmt:message key="cardType" />:*</td>
    <td>
	  <spring:bind path="orderForm.order.creditCard.type">
	  <select name="<c:out value="${status.expression}"/>">
	    <c:forTokens items="${siteConfig['CREDIT_CARD_PAYMENT'].value}" delims="," var="cc">
          <option value="${cc}" <c:if test="${status.value == cc}">selected</c:if>><fmt:message key="${cc}"/></option>
        </c:forTokens>
	  </select>
      <font color = "red"><c:out value="${status.errorMessage}"/></font>
      </spring:bind>   
	</td>
<c:if test="${siteConfig['CENTINEL_URL'].value != ''
	and fn:trim(siteConfig['CENTINEL_PROCESSORID'].value) != ''
	and fn:trim(siteConfig['CENTINEL_MERCHANTID'].value) != ''
	and fn:trim(siteConfig['CENTINEL_TRANSACTIONPWD'].value) != ''
	}">
	<td rowspan="5">
	  <table border="0" cellspacing="0" cellpadding="0" width="250">
	    <tr>
	      <td colspan="2">
	        Your card may be eligible or enrolled in Verified by Visa or MasterCard<sup>&reg;</sup> SecureCode&#153;
	        payer authentication programs. On the next page, after clicking "Purchase", your Card
	        Issuer may prompt you for your payer authentication password to complete your order. 
	      </td>
	    </tr>
	    <tr>
	      <td><a href="javascript:void window.open('assets/VerifiedByVisa/popup.html','serviceDescription','width=550,height=350')"><img src="assets/VerifiedByVisa/logo.gif" border="0"/></a></td>
	      <td><a href="javascript:void window.open('assets/MasterCardSecureCode/popup.html','serviceDescription','width=550,height=350')"><img src="assets/MasterCardSecureCode/logo.gif" border="0"/></a></td>
	    </tr>
	  </table>
	</td>
</c:if>
   </tr>
   <tr>
     <td class="formName"><fmt:message key="cardNumber" />:*</td>
     <td>
	   <spring:bind path="orderForm.order.creditCard.number">
	   <input id="ccNum" type="text" name="<c:out value="${status.expression}"/>" value="<c:out value="${status.value}"/>" <c:if test="${orderForm.order.paymentMethod != 'Credit Card'}">disabled</c:if> maxlength="16" size="16">
       <font color = "red"><c:out value="${status.errorMessage}"/></font>
       </spring:bind>   
       <!-- debug: <c:out value="${orderForm.order.creditCard.paymentNote}"/> -->
	 </td>
   </tr>
   <tr>
     <td class="formName"><fmt:message key="cardVerificationCode" />:*</td>
     <td>
	   <spring:bind path="orderForm.order.creditCard.cardCode">
	   <input type="text" name="<c:out value="${status.expression}"/>" value="<c:out value="${status.value}"/>" maxlength="4" size="4">
       <font color = "red"><c:out value="${status.errorMessage}"/></font>
       </spring:bind>   
	   <a class="cvv2" href="#" onClick="openW('assets/CVV2.html')"><fmt:message key="shoppingcart.whereDoIFindthisCode" /></a>
	 </td>
   </tr>
   <tr>
     <td class="formName"><fmt:message key="expDate" /> (MM/YY):*</td>
     <td>
	   <spring:bind path="orderForm.order.creditCard.expireMonth">
	   <select name="<c:out value="${status.expression}"/>">
	    <c:forTokens items="01,02,03,04,05,06,07,08,09,10,11,12" delims="," var="ccExpireMonth">
          <option value="${ccExpireMonth}" <c:if test="${status.value==ccExpireMonth}">selected</c:if>><c:out value="${ccExpireMonth}"/></option>
        </c:forTokens>
	   </select>
       </spring:bind> 
       <spring:bind path="orderForm.order.creditCard.expireYear">
	   <select name="<c:out value="${status.expression}"/>">
	    <c:forEach items="${model.expireYears}" var="ccExpireYear">
         <option value="${ccExpireYear}" <c:if test="${status.value==ccExpireYear}">selected</c:if>><c:out value="${ccExpireYear}"/></option>
	    </c:forEach>
	   </select>
	   <font color="red"><c:out value="${status.errorMessage}"/></font>
       </spring:bind>  
	 </td>
   </tr>
   <tr><td colspan="2"><font color="red"><fmt:message key="shoppingcart.cardNote" /></font></td></tr>
</table>
</div>
</div>
</c:if>

<c:if test="${gSiteConfig['gPAYPAL'] > 0}">
<spring:bind path="orderForm.order.paymentMethod">
<div class="clear"></div>
<div id="paypal" class="payment">
 <div class="ritWrapper">
   <div class="radio"><input type="radio" value="PayPal" name="<c:out value="${status.expression}"/>"  onclick="togglePayment()" <c:if test="${status.value == 'PayPal'}">checked</c:if>></div> 
   <div id="paypalTextImageBox" class="imgText">
     <div id="paypalText" class="text"></div>
     <div id="paypalImage" class="image"><a href="#" onclick="javascript:window.open('https://www.paypal.com/cgi-bin/webscr?cmd=xpt/Marketing/popup/OLCWhatIsPayPal-outside','olcwhatispaypal','toolbar=no, location=no, directories=no, status=no, menubar=no, scrollbars=yes, resizable=yes, width=400, height=350');"><img  src="https://www.paypal.com/en_US/i/logo/PayPal_mark_60x38.gif" border="0" alt="Acceptance Mark"></a></div>
     <div id="paypalNone" class="note"><c:out value="${siteConfig['PAYPAL_PAYMENT_METHOD_NOTE'].value}" escapeXml="false"/></div>
   </div>
 </div>
</div>
</spring:bind>
</c:if>

<c:if test="${gSiteConfig['gPAYPAL'] < 2}">
<spring:bind path="orderForm.order.paymentMethod">
<c:if test="${siteConfig['AMAZON_URL'].value != ''
	and fn:trim(siteConfig['AMAZON_MERCHANT_ID'].value) != ''
	and fn:trim(siteConfig['AMAZON_ACCESS_KEY'].value) != ''
	and fn:trim(siteConfig['AMAZON_SECRET_KEY'].value) != ''
	}">
<div class="clear"></div>
<div id="amazon" class="payment">
 <div class="ritWrapper">
   <div class="radio"><input type="radio" value="Amazon" name="<c:out value="${status.expression}"/>"  onclick="togglePayment()" <c:if test="${status.value == 'Amazon'}">checked</c:if>></div> 
   <div id="paypalTextImageBox" class="imgText">
     <div id="paypalText" class="text"></div>
     <div id="paypalImage" class="image"><img  src="assets/Image/Layout/amazon-payments.jpg" border="0" alt="Amazon Payments"></div>
     <div id="paypalNone" class="note"></div>
   </div>
 </div>
</div>
</c:if>
<c:if test="${gSiteConfig['gCREDIT_CARD_PAYMENT'] == 'netcommerce'}">
<div class="clear"></div>
<div id="netCommerce" class="payment">
 <div class="ritWrapper">
   <div class="radio"><input type="radio" value="NetCommerce" name="<c:out value="${status.expression}"/>"  onclick="togglePayment()" <c:if test="${status.value == 'NetCommerce'}">checked</c:if>></div> 
   <div id="netcommerceTextImageBox" class="imgText">
     <div id="netcommerceText" class="text"><b></b></div>
     <div id="netcommerceImage" class="image"><img src="http://www.netcommerce.com.lb/logo/NCseal_S.gif" border="0" alt="NetCommerce Security Seal"></div>
     <div id="netcommerceNone" class="note">NetCommerce</div>
   </div>
 </div>
</div>
</c:if>
<c:if test="${gSiteConfig['gCREDIT_CARD_PAYMENT'] == 'bankaudi'}">
<div class="clear"></div>
<div id="bankAudi" class="payment">
 <div class="ritWrapper">
   <div class="radio"><input type="radio" value="BankAudi" name="<c:out value="${status.expression}"/>"  onclick="togglePayment()" <c:if test="${status.value == 'BankAudi'}">checked</c:if>></div> 
   <div id="bankaudiTextImageBox" class="imgText">
     <div id="bankaudiText" class="text"><b>BankAudi</b></div>
     <div id="bankaudiImage" class="image"></div>
     <div id="bankaudiNone" class="note"></div>
   </div>
 </div>
</div>
</c:if>
<c:if test="${siteConfig['EBILLME_URL'].value != ''
	and fn:trim(siteConfig['EBILLME_MERCHANTTOKEN'].value) != ''
	and fn:trim(siteConfig['EBILLME_CANCEL_URL'].value) != ''
	and fn:trim(siteConfig['EBILLME_ERROR_URL'].value) != ''
	}">
<div class="clear"></div>
<div id="eBillme" class="payment">
 <div class="ritWrapper">
   <div class="radio"><input type="radio" value="eBillme" name="<c:out value="${status.expression}"/>"  onclick="togglePayment()" <c:if test="${status.value == 'eBillme'}">checked</c:if>></div> 
   <div id="eBillmeTextImageBox" class="imgText">
     <div id="eBillmeText" class="text"></div>
     <div id="eBillmeImage" class="image">
      <img src="https://www.ebillme.com/checkout/eBillme_logo_large_95x40.gif" alt="eBillme - Secure Cash Payments" title="eBillme - Secure Cash Payments" width="95" height="40" border="0" /></div>
     <div id="eBillmeNone" class="note"><p class="para" style="font-size: 12px; line-height: 13px; font-family: Arial, Helvetica, sans-serif; color: #000000; margin:0;"><strong>eBillme&#8482; Secure Cash Payments</strong><br />            <span class="para" style="font-size: 12px; line-height: 13px; font-family: Arial, Helvetica, sans-serif; color: #000000; margin:0;"><a href="https://about.ebillme.com/index.php/about/webjaguar" onclick="window.open('https://about.ebillme.com/index.php/about/webjaguar', 'eBillme_Learnmore', 'width=500,height=800, scrollbars=1, resizable=1'); return false">Learn more</a></span>.</div>
   </div>
 </div>
</div>
<%-- 
<table border="0" cellspacing="0" cellpadding="0">
  <tr>
	<td width="10"><input type="radio" value="eBillme" name="<c:out value="${status.expression}"/>"  onclick="toggle('creditcard')" <c:if test="${status.value == 'eBillme'}">checked</c:if>></td>
	<td width="95" valign="middle"><img src="https://www.ebillme.com/checkout/eBillme_logo_large_95x40.gif" alt="eBillme - Secure Cash Payments" title="eBillme - Secure Cash Payments" width="95" height="40" border="0" /></td>
	<td width="20" valign="top"><div align="center"><img src="https://www.ebillme.com/checkout/vert_line.gif" width="1" height="50" /></div></td>
	<td valign="middle"><p class="para" style="font-size: 12px; line-height: 13px; font-family: Arial, Helvetica, sans-serif; color: #000000; margin:0;"><strong>eBillme&#8482; Secure Cash Payments</strong><br />            <span class="para" style="font-size: 12px; line-height: 13px; font-family: Arial, Helvetica, sans-serif; color: #000000; margin:0;"><a href="https://about.ebillme.com/index.php/about/webjaguar" onclick="window.open('https://about.ebillme.com/index.php/about/webjaguar', 'eBillme_Learnmore', 'width=500,height=800, scrollbars=1, resizable=1'); return false">Learn more</a></span>.</td>
  </tr>
</table>
--%>
</c:if>

<c:if test="${siteConfig['GOOGLE_CHECKOUT_URL'].value != ''}">
<c:set value="true" var="googlebox"/>
<div class="clear"></div>
<div id="googleCheckout" class="payment">
 <div class="ritWrapper">
   <div class="radio"><input type="radio" id="google_radio" value="Google" name="<c:out value="${status.expression}"/>"  onclick="togglePayment()" <c:if test="${status.value == 'Google'}">checked</c:if>></div> 
   <div id="googleTextImageBox" class="imgText">
     <div id="googleText" class="text"></div>
     <div id="googleImage" class="image"><img  src="https://checkout.google.com/buttons/checkout.gif?merchant_id=${siteConfig['GOOGLE_MERCHANT_ID'].value}&w=168&h=44&style=trans&variant=text&loc=en_US" alt="Google Checkout" /></div>
     <div id="googleNone" class="note"></div>
   </div>
 </div>
</div>
</c:if>


<c:forEach items="${model.customPayments}" var="paymentMethod">
<c:if test="${paymentMethod.enabled and not paymentMethod.internal and (not (fn:toLowerCase(fn:trim(paymentMethod.title)) == 'credit card') or siteConfig['CREDIT_CARD_PAYMENT'].value == '')}">
<div class="clear"></div>
<div id="${fn:replace(paymentMethod.title, ' ', '')}Checkout" class="payment">
 <div class="ritWrapper">
   <div class="radio"><input type="radio" value="<c:out value="${paymentMethod.title}"/>" name="<c:out value="${status.expression}"/>"  onclick="togglePayment()" <c:if test="${status.value == paymentMethod.title}">checked</c:if>></div> 
   <div id="customPaymentsTextImageBox" class="imgText">
     <div id="customPaymentsText" class="text"><c:out value="${paymentMethod.title}"/></div>
     <div id="customPaymentsImage" class="image"></div>
     <div id="customPaymentsNone" class="note"></div>
   </div>
 </div>
</div>
</c:if>
</c:forEach>
</spring:bind>
</c:if>
</div> <!-- paymentWrapper end  -->
  </c:otherwise>
</c:choose>
<c:if test="${gSiteConfig['gGIFTCARD'] or gSiteConfig['gVIRTUAL_BANK_ACCOUNT']}">
<table border="0" cellpadding="3" cellspacing="1" width="100%">
  <tr><td class="addressBook"><fmt:message key="f_credit" /></td></tr>
</table>

<c:choose>
 <c:when test="${orderForm.customerCredit > 0}">
   <c:choose>
    <c:when test="${orderForm.customerCredit > orderForm.order.grandTotal}">
      <form:radiobutton value="vba" path="order.paymentMethod"  onclick="togglePayment()" />
      <fmt:message key="applyOfMyCreditToThisOrder">
	      <c:choose>
	  		<c:when test="${siteConfig['CURRENCY'].value == 'EUR'}">
	  			<fmt:param value="&#8364;" /> 
	  		</c:when>
	  		<c:when test="${siteConfig['CURRENCY'].value == 'GBP'}">
	  			<fmt:param value="&#163;"/> 
	  		</c:when>
	  		<c:when test="${siteConfig['CURRENCY'].value == 'JPY'}">
	  			<fmt:param value="&#165;"/> 
	  		</c:when>
	  		<c:when test="${siteConfig['CURRENCY'].value == 'CAD'}">
	  			<fmt:param value="CAD&#36;"/> 
	  		</c:when>
	  		<c:otherwise>
	  		    <fmt:param value="&#36;" />
	  		</c:otherwise>
		  </c:choose><fmt:param value="${orderForm.customerCredit}"/>
	   </fmt:message>
    </c:when>
    <c:otherwise>
      <form:checkbox path="applyUserPoint" value="true"  />
      <fmt:message key="applyOfMyCreditToThisOrder">
	      <c:choose>
	  		<c:when test="${siteConfig['CURRENCY'].value == 'EUR'}">
	  			<fmt:param value="&#8364;" /> 
	  		</c:when>
	  		<c:when test="${siteConfig['CURRENCY'].value == 'GBP'}">
	  			<fmt:param value="&#163;"/> 
	  		</c:when>
	  		<c:when test="${siteConfig['CURRENCY'].value == 'JPY'}">
	  			<fmt:param value="&#165;"/> 
	  		</c:when>
	  		<c:when test="${siteConfig['CURRENCY'].value == 'CAD'}">
	  			<fmt:param value="CAD&#36;"/> 
	  		</c:when>
	  		<c:otherwise>
	  		    <fmt:param value="&#36;" />
	  		</c:otherwise>
		  </c:choose><fmt:param value="${orderForm.customerCredit}"/>
	   </fmt:message>
    </c:otherwise>
   </c:choose>
 </c:when>
</c:choose>
<div>
<c:if test="${gSiteConfig['gGIFTCARD']}">
<div style="margin:10px;"><fmt:message key="enterNewGiftCards" /><br />
<fmt:message key="IfYouHaveMoreThanOneCodeEnterItBelowAndClickTheApplyButtonToUpdateThisPage" />
</div>
<div class="applyGiftCardCode">
 <table border="0"><tr>
	<td><b><fmt:message key="enterCode" /></b></td><td><form:input path="tempGiftCardCode" size="20"/></td><td><input type="image" border="0" id="__claimCode" name="_redeem" onclick="return checkCode();" src="assets/Image/Layout/button_apply.gif" /></td>
 </tr></table>	
</div>	
</c:if>
</div>
</c:if>

<div align="center" class="nbButton" style="margin:5px;">
<div align="right" class="next"><input type="image" src="assets/Image/Layout/button_next${_lang}.gif" name="_target4" /></div>
<div align="right" class="back" style="display:none;"><input type="image" src="assets/Image/Layout/button_prev${_lang}.gif" name="_target1" /></div>
</div>
</form:form>

</div>
<c:out value="${model.billingLayout.footerHtml}" escapeXml="false"/>

<script language="JavaScript">
<!--
function togglePayment() {
<c:if test="${gSiteConfig['gPAYPAL'] < 2 and siteConfig['CREDIT_CARD_PAYMENT'].value != ''}">
  if (document.getElementById('cc_radio').checked) {
    document.getElementById('creditcardbox').style.display="block";
    document.getElementById('ccNum').disabled=false;    
  } else {	
    document.getElementById('creditcardbox').style.display="none";
    document.getElementById('ccNum').disabled=true;    
  }  
</c:if>
<c:if test="${gemoneybox}">
  if (document.getElementById('ge_radio').checked) {
    document.getElementById('gemoneybox').style.display="block";
  } else {	
    document.getElementById('gemoneybox').style.display="none";
  }  
</c:if>
<c:if test="${googlebox}">
  if (document.getElementById('google_radio').checked) {
    document.getElementById('creditcardbox').style.display="none";
    document.getElementById('ccNum').disabled=true;
  }
</c:if>
}
//-->
</script>


</tiles:putAttribute>
</tiles:insertDefinition>

