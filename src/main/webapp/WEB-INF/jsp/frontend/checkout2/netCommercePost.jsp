<%@ page import="com.webjaguar.model.*, java.text.*" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<html>
<body>
<form name="netCommerceForm" method="POST" action="https://www.netcommercepay.com/iPAY/Default.asp">

<% 
 NumberFormat nf = new DecimalFormat("#0.00");
 Order order = (Order) request.getAttribute("order");
 pageContext.setAttribute("orderGrandTotal", nf.format(order.getGrandTotal()));

 String secureUrl = request.getScheme() + "://" + request.getHeader("host") + request.getContextPath() + "/"; 
%>

<input type="hidden" name="txtAmount" value="<c:out value="${orderGrandTotal}" />">
<input type="hidden" name="txtCurrency" value="840"> <%-- 840 for USD 422 for LBP --%>
<input type="hidden" name="txtIndex" value="<c:out value="${order.orderId}"/>">
<input type="hidden" name="txtMerchNum" value="<c:out value="${siteConfig['CREDIT_ACCOUNT'].value}"/>">
<input type="hidden" name="txthttp" value="<c:out value="<%= secureUrl %>netcommerce.jhtm"/>">

<noscript>
<input type="submit" value="Please click to continue paying by NetCommerce">
</noscript>

</form>
</body>
<script type="text/javascript" language="JavaScript">
<!--
document.netCommerceForm.submit();
//-->
</script>
</html>