<%@ page import="com.webjaguar.model.*, java.text.*" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<html>
<body>
<% 
 NumberFormat nf = new DecimalFormat("#0.00");
 Order order = (Order) request.getAttribute("order");
 pageContext.setAttribute("orderGrandTotal", nf.format(order.getGrandTotal()));
 
 String secureUrl = request.getScheme() + "://" + request.getHeader("host") + request.getContextPath() + "/"; 
%>

<form name="geMoneyForm" method="POST" action="https://<c:out value="${siteConfig['GEMONEY_DOMAIN'].value}"/>/process/shoppingCartProcessor.do">

<input type="hidden" name="shopperId" value="<%= request.getSession().getId() %>"/>
<input type="hidden" name="merchantId" value="<c:out value="${siteConfig['GEMONEY_MERCHANTID'].value}"/>"/>
<input type="hidden" name="homeUrl" value="<c:out value="${siteConfig['SITE_URL'].value}"/>"/>
<input type="hidden" name="clientTransactionId" value="<c:out value="${order.orderId}"/>"/>
<input type="hidden" name="purchaseNotificationUrl" value="<%= secureUrl %>geMoney/purchaseNotification.jhtm"/>
<input type="hidden" name="clientUnsuccessPurchaseUrl" value="<%= secureUrl %>checkout.jhtm"/>
<input type="hidden" name="clientSuccessfulPurchaseUrl" value="<%= secureUrl %>invoice.jhtm?order=${order.orderId}"/>
<input type="hidden" name="clientSuccessfulApplyUrl" value="<%= secureUrl %>invoice.jhtm?order=${order.orderId}"/>
<input type="hidden" name="clientUnsuccessApplyUrl" value="<%= secureUrl %>checkout.jhtm"/>
<input type="hidden" name="creditApplyNotificationUrl" value="<%= secureUrl %>geCapital/creditApplyNotification.jhtm"/>
<input type="hidden" name="billToFirstName" value="${order.billing.firstName}"/>
<input type="hidden" name="billToMiddleInitial" value=""/>
<input type="hidden" name="billToLastName" value="${order.billing.lastName}"/>
<input type="hidden" name="billToAddress1" value="${order.billing.addr1}"/>
<input type="hidden" name="billToAddress2" value="${order.billing.addr2}"/>
<input type="hidden" name="billToCity" value="${order.billing.city}"/>
<input type="hidden" name="billToState" value="${order.billing.stateProvince}"/>
<input type="hidden" name="billToZipCode" value="${fn:trim(order.billing.zip)}"/>
<input type="hidden" name="billToHomePhone" value="${fn:trim(order.billing.phone)}"/>
<input type="hidden" name="transactionAmount" value="${orderGrandTotal}"/>
<input type="hidden" name="promoCode" value="<c:out value="${siteConfig['GEMONEY_PROMOCODE'].value}"/>"/>
<input type="hidden" name="clientTestFlag" value="<c:choose><c:when test="${siteConfig['GEMONEY_DOMAIN'].value == 'www.secureb2c.com'}">N</c:when><c:otherwise>Y</c:otherwise></c:choose>"/>
<input type="hidden" name="clientToken" value="<c:out value="${geMoneyStrToken}"/>"/>
<input type="hidden" name="billToAccountNumber" value="<c:out value="${order.geMoney.acctNumber}"/>"/>
<input type="hidden" name="billToSsn" value="<c:out value="${order.geMoney.ssn}"/>"/>

<noscript>
<input type="submit" value="Please click to continue paying by <c:out value="${siteConfig['GEMONEY_PROGRAM_NAME'].value}"/>">
</noscript>

</form>

<%@ include file="/WEB-INF/jsp/frontend/checkout2/geMoneyPostCustom.jsp" %> 
</body>
<script type="text/javascript" language="JavaScript">
<!--
document.geMoneyForm.submit();
//-->
</script>
</html>
