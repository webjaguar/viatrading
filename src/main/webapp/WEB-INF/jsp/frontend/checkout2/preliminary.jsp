<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib uri="/WEB-INF/tlds/spring.tld" prefix="spring" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<c:out value="${invoiceLayout.headTag}" escapeXml="false"/>
</head> 
<c:if test="${gSiteConfig['gSALES_PROMOTIONS']}"> 
<script src="javascript/mootools-1.2.5.1-more.js" type="text/javascript"></script> 
<script type="text/javascript" >
<!--
<c:if test="${gSiteConfig['gSHOW_ELIGIBLE_PROMOS']}">
window.addEvent('domready', function() {
	Element.implement({
		show: function() {
			this.setStyle('display','');
		},
		hide: function() {
			this.setStyle('display','none');
		}
	});
});
function showPromo( dropdown ){
	var eliPromo = $$('div.eligiblePromoBox');
	eliPromo.each(function(el) {
		$(el).hide();
	});
	var myindex  = dropdown.selectedIndex;
    var SelValue = dropdown.options[myindex].value;
    var SelText = dropdown.options[myindex].text
    if (myindex!=0) {
    	$('order.promoCode').value = SelText;
    	$("promo"+SelValue).show();
    } else {
    	$('order.promoCode').value = "";
    }
}
</c:if>
function loadPromoCode( aform ) {
	if ( document.getElementById('order.promoCode').value == '') {
		alert("Promo code is empty!");
		return false;
	}
}
-->	
</script>
</c:if>
<script type="text/javascript" >
<!--
function checkPO( poRequired ) {
	if ( poRequired == 'warning' && document.getElementById('order.purchaseOrder').value == '' ) {
		reply = confirm("You have not typed a Purchase Order #. Purchase anyway?");
    	if (reply == false) {
     	document.getElementById('order.purchaseOrder').focus();
     	return false;
     	}
     	if (reply == true) document.getElementById('order.invoiceNote').value = "Call to request PO before shipping. \n" +  document.getElementById('order.invoiceNote').value;
	} else if ( poRequired == 'required' && document.getElementById('order.purchaseOrder').value == '' ) {
		alert("Please enter Purchase Order #");
   		document.getElementById('order.purchaseOrder').focus();
   		return false;
	}
}
-->	
</script>   
<body class="invoice">
<div style="width:700px;margin:auto;">

<c:out value="${invoiceLayout.headerHtml}" escapeXml="false"/>

<table class="invoiceHeader">
<tr valign="top">
<td>
<form:form commandName="orderForm" action="checkout2.jhtm" method="post">
<input type="hidden" name="newBillingAddress" value="true">
<b><fmt:message key="billingInformation" />:</b>
<c:set value="${orderForm.order.billing}" var="address"/>
<c:set value="true" var="billing" />
<%@ include file="/WEB-INF/jsp/frontend/common/address.jsp" %>
<c:set value="false" var="billing" />
<input type="image" border="0" name="_target2" src="assets/Image/Layout/button_change${_lang}.gif">
</form:form>
</td>
<td>&nbsp;</td>
<td>
<form:form commandName="orderForm" action="checkout2.jhtm" method="post">
<input type="hidden" name="newShippingAddress" value="true"> 
<b><fmt:message key="shippingInformation" />:</b>
<c:set value="${orderForm.order.shipping}" var="address"/>
<%@ include file="/WEB-INF/jsp/frontend/common/address.jsp" %>
<input type="image" border="0" name="_target0" src="assets/Image/Layout/button_change${_lang}.gif">
</form:form>
</td>
<c:if test="${siteConfig['DELIVERY_TIME'].value == 'true' and !empty orderForm.order.dueDate}">
<td>&nbsp;</td>
<td>
<b><fmt:message key="f_deliveryDate" />:</b>
<div class="deliveryDate"><fmt:formatDate type="date" timeStyle="default" value="${orderForm.order.dueDate}"/></div>
</td>
</c:if>
</tr>
</table>
<br/><br/>
<div id="helperHeadBox">
  <div id="emailAddress"><b><fmt:message key="emailAddress" />:</b>&nbsp;<c:out value="${orderForm.username}"/></div>
  <div id="viewcart"><a href="viewCart.jhtm" ><fmt:message key="shoppingcart.changeQuantities" /></a></div>
</div>
<div style="clear:both;"></div>
<form:form commandName="orderForm" action="checkout2.jhtm" method="post">
<input type="hidden" name="_centinelDebug" value="false">
<table border="0" cellpadding="2" cellspacing="1" width="100%" class="invoice">
  <tr>
	<c:set var="cols" value="0"/>
    <th width="5%" class="invoice"><fmt:message key="line" />#</th>
    <th class="invoice"><fmt:message key="productSku" /></th>
    <th class="invoice"><fmt:message key="productName" /></th>
    <c:forEach items="${orderForm.productFieldsHeader}" var="productField">
      <c:if test="${productField.showOnInvoice}">
      <th class="invoice"><c:out value="${productField.name}" /></th><c:set var="cols" value="${cols+1}"/>
      </c:if>
    </c:forEach>
    <th width="10%" class="invoice"><fmt:message key="quantity" /></th>
    <c:if test="${gSiteConfig['gINVENTORY'] && orderForm.order.hasLowInventoryMessage}">
      <c:set var="cols" value="${cols+1}"/>
	  <th width="10%" class="invoice"><fmt:message key="lowInventory" /></th>
	</c:if>
    <c:if test="${orderForm.order.hasPacking}">
	  <c:set var="cols" value="${cols+1}"/>
      <th width="10%" class="invoice"><fmt:message key="packing" /></th>
    </c:if>
    <c:if test="${orderForm.order.hasContent}">
	  <c:set var="cols" value="${cols+1}"/>
      <th class="invoice"><fmt:message key="content" /></th>
    </c:if>
    <c:if test="${orderForm.order.hasCustomShipping}">
	  <c:set var="cols" value="${cols+1}"/>
      <th class="invoice"><fmt:message key="customShipping" /></th>
    </c:if>
    <th class="invoice"><fmt:message key="productPrice" /><c:if test="${orderForm.order.hasContent}">/<c:out value="${siteConfig['CASE_UNIT_TITLE'].value}" /></c:if></th>
    <c:if test="${orderForm.order.lineItemPromos != null and fn:length(orderForm.order.lineItemPromos) gt 0}">
      <c:set var="cols" value="${cols+1}"/>
	  <th class="invoice"><fmt:message key="discount" /></th>
	</c:if>
    <th class="invoice"><fmt:message key="total" /></th>
  </tr>
<c:forEach var="lineItem" items="${orderForm.order.lineItems}">
<tr valign="top">
  <td class="invoice" align="center"><c:out value="${lineItem.lineNumber}"/></td>
  <td class="invoice">
	<c:if test="${lineItem.customImageUrl != null}">
      <a href="framer/pictureframer/images/FRAMED/${lineItem.customImageUrl}" onclick="window.open(this.href,'','width=640,height=480,resizable=yes'); return false;"><img class="invoiceImage" src="framer/pictureframer/images/FRAMED/${lineItem.customImageUrl}" border="0" /></a>
    </c:if>  
    <c:if test="${siteConfig['SHOW_IMAGE_ON_INVOICE'].value == 'true' and lineItem.product.thumbnail != null}">
      <img class="invoiceImage" src="<c:if test="${!lineItem.product.thumbnail.absolute}">assets/Image/Product/thumb/</c:if>${lineItem.product.thumbnail.imageUrl}" border="0" /><br />
    </c:if><div class="sku"><c:out value="${lineItem.product.sku}"/></div></td>
  <td class="invoice"><c:if test="${lineItem.customXml != null}">Custom Frame - </c:if><c:out value="${lineItem.product.name}"/>
    <table border="0" cellspacing="1" cellpadding="0">
	  <c:forEach items="${lineItem.productAttributes}" var="productAttribute" varStatus="productAttributeStatus">
		<tr class="invoice_lineitem_attributes${productAttributeStatus.count%2}">
		<td class="optionName" style="white-space: nowrap" align="right"><c:out value="${productAttribute.optionName}"/>: </td>
		<td class="optionValue" style="padding-left:5px;"><c:out value="${productAttribute.valueString}" escapeXml="false"/></td>
		<c:if test="${(! empty productAttribute.optionPriceOriginal) && productAttribute.optionPriceOriginal != 0}">
		<td class="optionPrice"><fmt:formatNumber value="${productAttribute.optionPriceOriginal}" pattern="#,##0.00" /></td>
		<td class="optionName" style="padding-left:5px;"><fmt:message key="${productAttribute.optionPriceMessageF}"/></td>
		</c:if>
		</tr>
	  </c:forEach>
	  <c:forEach items="${lineItem.asiProductAttributes}" var="asiProductAttribute" varStatus="asiProductAttributeStatus">
		<tr class="invoice_lineitem_attributes${asiProductAttributeStatus.count%2}">
		<td style="white-space: nowrap" align="right"><c:out value="${asiProductAttribute.asiOptionName}"/>: </td>
		<td style="width:100%;padding-left:5px;"><c:out value="${asiProductAttribute.asiOptionValue}" escapeXml="false"/></td>
		</tr>
	  </c:forEach>
	  <%@ include file="/WEB-INF/jsp/frontend/common/customLinesInvoice.jsp"%>
    </table>
	<c:if test="${lineItem.subscriptionInterval != null}">
	  <c:set var="intervalType" value="${fn:substring(lineItem.subscriptionInterval, 0, 1)}"/>
	  <c:set var="intervalUnit" value="${fn:substring(lineItem.subscriptionInterval, 1, 2)}"/>
	  <div class="invoice_lineitem_subscription">
		<fmt:message key="subscription"/> <fmt:message key="details"/> : <fmt:message key="delivery"/>
		<c:choose>
		  <c:when test="${intervalUnit == '1'}"><fmt:message key="every_${intervalType}"><fmt:param value="${intervalUnit}"/></fmt:message></c:when>
		  <c:otherwise><fmt:message key="every_${intervalType}s"><fmt:param value="${intervalUnit}"/></fmt:message></c:otherwise>
		</c:choose>
	  </div>
	</c:if>
  <c:if test="${siteConfig['SHOW_IMAGE_ON_INVOICE'].value == 'true' and lineItem.hasProductAttributesImage}" >
  <c:forEach items="${lineItem.productAttributes}" var="productAttribute">
    <c:if test="${!empty productAttribute.imageUrl}" > 
      <c:if test="${!cartItem.product.thumbnail.absolute}"><img src="<c:url value="/assets/Image/Product/options/${productAttribute.imageUrl}"/>" border="0" class="invoiceOptionImage" /></c:if>
      <c:if test="${cartItem.product.thumbnail.absolute}"><img src="<c:url value="${productAttribute.imageUrl}"/>" border="0" class="invoiceOptionImage" /></c:if>
    </c:if>
  </c:forEach>
  </c:if> 
  </td>  
  <c:forEach items="${orderForm.productFieldsHeader}" var="pFHeader" varStatus="status"><c:set var="check" value="0"/>
  <c:if test="${pFHeader.showOnInvoice}">
  <c:forEach items="${lineItem.productFields}" var="productField"> 
   <c:if test="${pFHeader.id == productField.id}">
    <td class="invoice"><c:out value="${productField.value}" /></td><c:set var="check" value="1"/>
   </c:if>       
  </c:forEach>
  <c:if test="${check == 0}">
    <td class="invoice">&nbsp;</td>
   </c:if>
  </c:if>
  </c:forEach> 
  <td class="invoice" align="center">
    <c:choose>
        <c:when test="${gSiteConfig['gVARIABLE_PRICE'] and lineItem.product.priceByCustomer}"></c:when>
        <c:otherwise><c:out value="${lineItem.quantity}"/></c:otherwise>
    </c:choose>       
  </td>
  <c:if test="${gSiteConfig['gINVENTORY'] && orderForm.order.hasLowInventoryMessage}">
	<td class="invoice" align="center"><c:out value="${lineItem.lowInventoryMessage}" /></td>
  </c:if>
  <c:if test="${orderForm.order.hasPacking}">
  <td class="invoice" align="center"><c:out value="${lineItem.product.packing}" /></td>
  </c:if>
  <c:if test="${orderForm.order.hasContent}">
    <td class="invoice" align="center"><c:out value="${lineItem.product.caseContent}" /></td>
  </c:if>
  <c:if test="${orderForm.order.hasCustomShipping}">
    <td class="invoice" align="center">
    <c:if test="${!empty lineItem.customShippingCost}">
     <c:out value="${orderForm.order.customShippingTitle}" />
    </c:if>
    </td>
  </c:if>
  <td class="invoice" align="right"><fmt:formatNumber value="${lineItem.unitPrice}" pattern="#,##0.00"/></td>
  <c:if test="${orderForm.order.lineItemPromos != null and fn:length(orderForm.order.lineItemPromos) gt 0}">
     <c:choose>
       <c:when test="${lineItem.promo.percent}">
         <td class="discount" align="right"><c:if test="${lineItem.promo != null}"><c:out value="${lineItem.promo.title}" /> : <fmt:formatNumber value="${(lineItem.totalPrice * lineItem.promo.discount) / 100}" pattern="#,##0.00"/></c:if></td>
       </c:when>
       <c:otherwise>
         <td class="discount" align="right"><c:if test="${lineItem.promo != null}"><c:out value="${lineItem.promo.title}" /> : <fmt:formatNumber value="${lineItem.promo.discount}" pattern="#,##0.00"/></c:if></td>
       </c:otherwise>
     </c:choose>
  </c:if>
  <td class="invoice" align="right"><fmt:formatNumber value="${lineItem.totalPrice}" pattern="#,##0.00"/></td>
</tr>
</c:forEach>
  <tr bgcolor="#BBBBBB">
	<td colspan="${6+cols}">&nbsp;</td>
  </tr>
  <tr>
    <td class="invoice" colspan="${5+cols}" align="right"><fmt:message key="subTotal" />:</td>
    <td class="invoice" align="right"><fmt:formatNumber value="${orderForm.order.subTotal}" pattern="#,##0.00"/></td>
  </tr>
  <c:if test="${orderForm.order.promo != null and orderForm.order.promo.discountType eq 'order'}">
    <tr>
      <td class="discount" colspan="${5+cols}" align="right">
      <c:choose>
        <c:when test="${orderForm.order.promoAmount == 0}"><fmt:message key="promoCode" /></c:when>
        <c:otherwise><fmt:message key="discountForPromoCode" /></c:otherwise>
      </c:choose>
      <c:out value="${orderForm.order.promo.title}" />
      <c:choose>
          <c:when test="${orderForm.order.promoAmount == 0}"></c:when>
          <c:when test="${orderForm.order.promo.percent}">
            (<fmt:formatNumber value="${orderForm.order.promo.discount}" pattern="#,##0.00"/>%)
          </c:when>
          <c:otherwise>
            ($<fmt:formatNumber value="${orderForm.order.promo.discount}" pattern="#,##0.00"/>)
          </c:otherwise>
      	</c:choose>
      </td>
      <td class="discount" align="right">
      <c:choose>
        <c:when test="${orderForm.order.promoAmount == 0}">&nbsp;</c:when>
        <c:otherwise>($<fmt:formatNumber value="${orderForm.order.promoAmount}" pattern="#,##0.00"/>)</c:otherwise>
      </c:choose>
      </td>
    </tr>  
  </c:if>
  <c:if test="${orderForm.order.lineItemPromos != null and fn:length(orderForm.order.lineItemPromos) gt 0}">
    <c:forEach items="${orderForm.order.lineItemPromos}" var="itemPromo" varStatus="status">
    <tr>
      <td class="discount" colspan="${5+cols}" align="right">
      <c:choose>
        <c:when test="${itemPromo.value == 0}"><fmt:message key="promoCode" /></c:when>
        <c:otherwise><fmt:message key="discountForPromoCode" /></c:otherwise>
      </c:choose>
      <c:out value="${itemPromo.key}" />
      </td>
      <td class="discount" align="right">
      <c:choose>
        <c:when test="${itemPromo.value == 0}">&nbsp;</c:when>
        <c:otherwise>($<fmt:formatNumber value="${itemPromo.value}" pattern="#,##0.00"/>)</c:otherwise>
      </c:choose>
      </td>
    </tr>
    </c:forEach>  
  </c:if>
  <tr>
    <td class="invoice" colspan="${5+cols}" align="right"><fmt:message key="tax" /></td>
    <td class="invoice" align="right"><fmt:formatNumber value="${orderForm.order.tax}" pattern="#,##0.00"/></td>
  </tr>
  <tr>
    <td class="invoice" colspan="${5+cols}" align="right"><fmt:message key="shippingHandling" /> (<c:out value="${orderForm.order.shippingMethod}" escapeXml="false"/>):</td>
    <td class="invoice" align="right"><fmt:formatNumber value="${orderForm.order.shippingCost}" pattern="#,##0.00"/></td>
  </tr>
  <c:if test="${orderForm.order.hasCustomShipping}">
    <td class="invoice" colspan="${5+cols}" align="right"><fmt:message key="customShipping" /> (<c:out value="${orderForm.order.customShippingTitle}" escapeXml="false"/>):</td>
    <td class="invoice" align="right"><fmt:formatNumber value="${orderForm.order.customShippingCost}" pattern="#,##0.00"/></td>
  </c:if>
  <c:if test="${orderForm.order.promo != null and orderForm.order.promo.discountType eq 'shipping'}">
    <tr>
      <td class="discount" colspan="${5+cols}" align="right">
      <c:choose>
        <c:when test="${orderForm.order.promoAmount == 0}"><fmt:message key="promoCode" /></c:when>
        <c:otherwise><fmt:message key="discountForPromoCode" /></c:otherwise>
      </c:choose>
      <c:out value="${orderForm.order.promo.title}" />
      <c:choose>
          <c:when test="${orderForm.order.promoAmount == 0}"></c:when>
          <c:when test="${orderForm.order.promo.percent}">
            (<fmt:formatNumber value="${orderForm.order.promo.discount}" pattern="#,##0.00"/>%)
          </c:when>
          <c:otherwise>
            ($<fmt:formatNumber value="${orderForm.order.promo.discount}" pattern="#,##0.00"/>)
          </c:otherwise>
      	</c:choose>
      </td>
      <td class="discount" align="right">
      <c:choose>
        <c:when test="${orderForm.order.promoAmount == 0}">&nbsp;</c:when>
        <c:otherwise>($<fmt:formatNumber value="${orderForm.order.promoAmount}" pattern="#,##0.00"/>)</c:otherwise>
      </c:choose>
      </td>
    </tr>  
  </c:if>
  <c:if test="${orderForm.order.ccFee != null and orderForm.order.ccFee > 0}">
    <td class="invoice" colspan="${5+cols}" align="right"><fmt:message key="creditCardFee" />:</td>
    <td class="invoice" align="right"><fmt:formatNumber value="${orderForm.order.ccFee}" pattern="#,##0.00"/></td>
  </c:if>
  <c:if test="${(gSiteConfig['gGIFTCARD'] or gSiteConfig['gVIRTUAL_BANK_ACCOUNT']) and orderForm.order.creditUsed != null}">
  <tr>
    <td class="discount" colspan="${5+cols}" align="right"><fmt:message key="credit" />:</td>
    <td class="discount" align="right"><fmt:formatNumber value="${orderForm.order.creditUsed}" pattern="#,##0.00"/></td>
  </tr>
  </c:if>
  <c:if test="${siteConfig['BUY_SAFE_URL'].value != ''}">
  <tr>
    <td class="invoice" colspan="${5+cols}" align="right"><fmt:message key="optionalBuySafe3In1Guarntee" /> <a href="viewCart.jhtm">( change )</a>:</td>
    <td class="invoice" align="right">
      <c:choose>
        <c:when test="${orderForm.order.wantsBond}"><fmt:formatNumber value="${orderForm.order.bondCost}" pattern="#,##0.00"/></c:when>
        <c:otherwise><fmt:formatNumber value="0.00"/></c:otherwise>
      </c:choose>
    </td>
  </tr>
  </c:if>
  <tr>
    <td class="invoice" colspan="${5+cols}" align="right"><fmt:message key="grandTotal" />:</td>
    <td align="right" bgcolor="#F8FF27"><fmt:message key="${siteConfig['CURRENCY'].value}" /><fmt:formatNumber value="${orderForm.order.grandTotal}" pattern="#,##0.00"/></td>
  </tr>
</table>
<c:out value="${siteConfig['INVOICE_MESSAGE'].value}" escapeXml="false"/>
<br/><br/>
<table border="0" cellpadding="0" cellspacing="0" width="100%" class="paymentBox">
<tr>
<td>
    <b><fmt:message key="paymentMethod" />:</b> <c:choose><c:when test="${orderForm.order.paymentMethod == 'GE Money'}"><c:out value="${siteConfig['GEMONEY_PROGRAM_NAME'].value}"/></c:when><c:otherwise><c:out value="${orderForm.order.paymentMethod}" /></c:otherwise></c:choose>
 <input type="image" border="0" name="_target3" src="assets/Image/Layout/button_change${_lang}.gif">
    <p>
<c:if test="${orderForm.order.creditCard.number != null and orderForm.order.creditCard.number != ''}">
 <c:set value="${orderForm.order.creditCard}" var="creditCard"/>
 <%@ include file="/WEB-INF/jsp/frontend/common/creditcard.jsp" %>
</c:if>
<c:if test="${orderForm.order.geMoney.acctNumber != null and orderForm.order.geMoney.acctNumber != ''}">
<fmt:message key="accountNumber" />: <c:out value="${orderForm.order.geMoney.acctNumber}" />
</c:if>
</td>
</tr>
</table>
<c:if test="${gSiteConfig['gSALES_PROMOTIONS']}">
<table border="0" cellpadding="0" cellspacing="0" width="100%" class="promoBox">
<tr>
    <td>
    <c:if test="${gSiteConfig['gSHOW_ELIGIBLE_PROMOS']}">
	  <b><fmt:message key="eligiblePromos" />:</b>
	    <select name="eligiblePromo" onchange="showPromo(this.form.eligiblePromo)" id="eligiblePromo">
	    <option value=""><c:out value="Select Promo"/></option>
		<c:forEach items="${eligiblePromos}" var="promo" varStatus="status">
		  <option value='${promo.promoId}'><c:out value="${promo.title}"/></option>
	    </c:forEach>
	    </select>
	 </c:if>  
	  <div id="promoContainer">
	    <div id="promoInput"> 
	    <form:input path="order.promoCode" htmlEscape="true"/>
	    </div>
	    <div id="promoButton">
	    <input type="image" src="assets/Image/Layout/button_apply_promocode${_lang}.gif" name="_target4" onclick="return loadPromoCode(this.form)">
	    </div>
	  </div>  
	    <span class="error"><c:if test="${orderForm.tempPromoErrorMessage != null}"><fmt:message key="${orderForm.tempPromoErrorMessage}"/></c:if></span>
	 <c:if test="${gSiteConfig['gSHOW_ELIGIBLE_PROMOS']}">   
	    <c:forEach items="${eligiblePromos}" var="promo" varStatus="status">
		  <div class="eligiblePromoBox" id="promo<c:out value="${promo.promoId}"/>" style="display:none;"><c:out value="${promo.htmlCode}" escapeXml="false"/></div>
	    </c:forEach>
	 </c:if>   
	</td>
</tr>
</table>
</c:if>
<div class="invoiceNote" align="center">
  <b><c:out value="${siteConfig['SPECIAL_INSTRUCTIONS'].value}"/></b>
  <br/>  	
  <form:textarea path="order.invoiceNote"  rows="3" cols="40" htmlEscape="true" cssClass="textfield" />
</div>
<c:if test="${siteConfig['USER_EXPECTED_DELIVERY_TIME'].value == 'true' or siteConfig['REQUESTED_CANCEL_DATE'].value == 'true'}" >
<link rel="stylesheet" type="text/css" media="all" href="javascript/jscalendar-1.0/skins/aqua/theme.css" title="Aqua" /> <!-- calendar stylesheet -->
<script type="text/javascript" src="javascript/jscalendar-1.0/calendar.js"></script> <!-- main calendar program -->
<script type="text/javascript" src="javascript/jscalendar-1.0/lang/calendar-en.js"></script>
<script type="text/javascript" src="javascript/jscalendar-1.0/calendar-setup.js"></script>
</c:if>

<c:if test="${siteConfig['USER_EXPECTED_DELIVERY_TIME'].value == 'true'}" >
<%
boolean papayaart = false;
if (request.getHeader("host").equalsIgnoreCase("www.papayaart.com") || request.getHeader("host").equalsIgnoreCase("papayaart.wjserver10.com")) {
	papayaart = true;
}
pageContext.setAttribute("papayaart", papayaart);
%>
<c:if test="${siteConfig['USER_EXPECTED_DELIVERY_TIME'].value == 'true' and not papayaart}" >
<table>
<tr>
<td>&nbsp;</td>
<td>
<b><fmt:message key="userExpectedDueDate" />:</b>
<div class="userExpectedDueDate">
	<form:input path="order.userDueDate" size="10" maxlength="10" />
	  <img src="assets/Image/Layout/calendarIcon.jpg" class="calendarImage" id="time_start_trigger"/>
	  <script type="text/javascript">
   		Calendar.setup({
       			inputField     :    "order.userDueDate",   
       			showsTime      :    false,
       			ifFormat       :    "%m-%d-%Y",   
       			button         :    "time_start_trigger"   
   		});	
	  </script> 
	<form:errors path="order.userDueDate" cssClass="error"/>
</div>
</td>
</tr>
</table>
</c:if>
</c:if>
	
<c:if test="${siteConfig['REQUESTED_CANCEL_DATE'].value == 'true'}" >
<table>
<tr>
<td>&nbsp;</td>
<td>
<b><fmt:message key="requestedCancelDate" />:</b>
<div class="requestedCancelDate">
	<form:input path="order.requestedCancelDate" size="10" maxlength="10" />
	  <img src="assets/Image/Layout/calendarIcon.jpg" class="calendarImage" id="requestedCancelDate_trigger"/>
	  <script type="text/javascript">
   		Calendar.setup({
       			inputField     :    "order.requestedCancelDate",   
       			showsTime      :    false,
       			ifFormat       :    "%m-%d-%Y",   
       			button         :    "requestedCancelDate_trigger"   
   		});	
	  </script> 
	<form:errors path="order.requestedCancelDate" cssClass="error"/>
</div>
</td>
</tr>
</table>
</c:if>	
	
<c:if test="${gSiteConfig['gORDER_FILEUPLOAD'] > 0}">
<table align="center">
<c:forEach items="${attachedFiles}" var="file" varStatus="status">
  <c:if test="${status.first}">
  <tr>
    <td align="center">
	  <b>Attached File(s):</b>
	</td>
  </tr>
  </c:if>
  <tr>
    <td align="center">
      <c:out value="${file['file'].name}"/> 
      <c:choose>
        <c:when test="${file['size'] > (1024*1024)}">
    	  (<fmt:formatNumber value="${file['size']/1024/1024}" pattern="#,##0.0"/> MB)
    	</c:when>
        <c:when test="${file['size'] > 1024}">
    	  (<fmt:formatNumber value="${file['size']/1024}" pattern="#,##0"/> KB)
    	</c:when>
    	<c:otherwise>
    	  (1 KB)
    	</c:otherwise>
      </c:choose>
    </td>
  </tr>
</c:forEach>
  <tr>
    <td align="center"><input type="image" border="0"
    	src="<c:choose><c:when test="${attachedFiles != null}">assets/Image/Layout/button_edit_attachedFiles${_lang}.gif</c:when><c:otherwise>assets/Image/Layout/button_attachFiles.gif</c:otherwise></c:choose>" name="_target5"/></td>
  </tr>

  <tr>
    <td>&nbsp;</td>
  </tr>  
</table>
</c:if>	
	
<div class="purchaseOrder" align="center"><b><fmt:message key="shoppingcart.purchaseOrder" /></b> <form:input path="order.purchaseOrder" maxlength="50" htmlEscape="true"/><form:errors path="order.purchaseOrder" cssClass="error" /></div>

<div id="before_purchaseButton"></div>

<div id="purchaseButton" align="center" class="nbButton" style="margin:5px;">
<div align="right" class="next"><input type="image" src="assets/Image/Layout/button_purchase${_lang}.gif" id="button_purchase" name="_finish" onclick="return checkPO('<c:out value='${orderForm.poRequired}'/>');"></div>
<div align="left" class="back"><input type="image" src="assets/Image/Layout/button_prev${_lang}.gif" name="_target3"></div>
</div>
</form:form>

<c:out value="${invoiceLayout.footerHtml}" escapeXml="false"/>

</div>
</body>
</html>