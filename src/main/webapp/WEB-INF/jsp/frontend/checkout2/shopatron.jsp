<%@ page import="java.io.*,java.util.*" %>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>

<tiles:insertDefinition name="${_template}" flush="true">
  <tiles:putAttribute name="content" type="string">
  
<c:if test="${model.exception != null}">  
<div class="shopatron_error">
There was an error submitting your order to shopatron. Please try again later.
</div>
<div class="shopatron_error_debug">
<%
Map<String, Object> model = (HashMap<String, Object>) request.getAttribute("model");
String message = ((Exception) model.get("exception")).getMessage();
out.println(message.replaceAll("\n", "<br/>\n"));
//((Exception) model.get("exception")).printStackTrace();
%>
</div>
</c:if>

<c:if test="${model.exception == null}">    

<c:redirect url="http://www.shopatron.com/xmlCheckout1.phtml?order_id=${model.order_id}" />

<div class="shopatron">
Your order has been successfully sent.<br/>
Your Shopatron Order ID: ${model.order_id}<br/>
If you are not automatically redirected in 10 seconds, click <a href="http://www.shopatron.com/xmlCheckout1.phtml?order_id=${model.order_id}">here</a>.<br/>
</div>

<script language="JavaScript"> 
<!--
window.open("http://www.shopatron.com/xmlCheckout1.phtml?order_id=${model.order_id}",'','width=980,height=750,resizable=yes,scrollbars=yes');
//--> 
</script>
<noscript>
If you are not automatically redirected in 10 seconds, click <a href="http://www.shopatron.com/xmlCheckout1.phtml?order_id=${model.order_id}">here</a>.
</noscript>
</c:if>

  </tiles:putAttribute>
</tiles:insertDefinition>