<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ taglib uri="/WEB-INF/tlds/custom.tld" prefix="wj" %>

<c:set var="productLink">${_contextpath}/product.jhtm?id=${product.id}<c:if test="${model.product == null or siteConfig['RECOMMENDED_LIST_SHOW_BREADCRUMB'].value == 'true'}">&cid=<c:out value="${param.cid}"/></c:if></c:set>
<c:if test="${gSiteConfig['gMOD_REWRITE'] == '1' and not fn:contains(product.sku, '/')}">
  <c:set var="productLink">${_contextpath}/${siteConfig['MOD_REWRITE_PRODUCT'].value}/${product.encodedSku}/${product.encodedName}.html<c:if test="${model.product == null or siteConfig['RECOMMENDED_LIST_SHOW_BREADCRUMB'].value == 'true'}">?cid=<c:out value="${param.cid}"/></c:if></c:set>
</c:if>	

<c:if test="${status.first}">
<form action="${_contextpath}/addToCart.jhtm" method="post" onsubmit="return checkForm()">
<table border="0" cellpadding="3" cellspacing="1" align="center" class="quickmode">
  <tr>
  	<th class="quickmode">Line#</th>
   <c:if test="${(showImage == null or showImage) and (siteConfig['QUICK_MODE_THUMB_HEIGHT'].value > 0 or siteConfig['QUICK_MODE_THUMB_HEIGHT'].value == '')}" >
    <th class="quickmode image"><fmt:message key="image" /></th>
   </c:if> 
   <c:if test="${siteConfig['SHOW_SKU'].value == 'true'}">
    <th class="quickmode"><fmt:message key="productSku" /></th>
   </c:if> 
    <th class="quickmode"><fmt:message key="f_productName" /></th>
   <c:if test="${model.evergreen}">
	<th class="quickmode"><fmt:message key="f_availability" /></th>
   </c:if>
	<c:forEach items="${model.productFields}" var="productField">
	  <c:if test="${productField.quickModeField}">
	    <th class="quickmode"><c:out value="${productField.name}" escapeXml="false" /></th>
	  </c:if>
	</c:forEach>
	<c:if test="${model.showPriceColumn == null or model.showPriceColumn}">
	<th class="quickmode">Price</th>
	</c:if>
	<c:if test="${gSiteConfig['gSHOPPING_CART'] and (model.showQtyColumn == null or model.showQtyColumn)}">
	<th class="quickmode">Quantity</th>
	</c:if>
  </tr>
</c:if>

<tr class="row${status.index % 2}">
<td><c:set value="${count+1}" var="count"/><c:out value ="${count}" /> </td>
  <c:if test="${(showImage == null or showImage) and (siteConfig['QUICK_MODE_THUMB_HEIGHT'].value > 0 or siteConfig['QUICK_MODE_THUMB_HEIGHT'].value == '')}" >
 	<td class="image">
	  <div align="center">
	    <c:if test="${product.thumbnail == null}"><c:if test="${product.active}">&nbsp;</c:if><c:if test="${!(product.active)}">Image not available</c:if></c:if>
	    <c:if test="${product.thumbnail != null}">
		  <a href="${productLink}">
	  	  <c:choose>
	  	   <c:when test="${product.asiId != null}">
	  	   <img src="<c:out value="${fn:replace(product.thumbnail.imageUrl, 'prodimgs', 'prodthumbs')}"/>" border="0" class="thumbnail_image" alt="<c:out value="${product.name}"/><c:if test="${product.shortDesc != ''}"> - <c:out value="${product.shortDesc}"/></c:if>">
	  	   </c:when>
	  	   <c:otherwise>
	  	   <img src="<c:if test="${!product.thumbnail.absolute}">${_contextpath}/assets/Image/Product/thumb/</c:if><c:out value="${fn:replace(product.thumbnail.imageUrl, 'http://www.dollaritem.com/wms/images/catalog/', 'http://www.dollaritem.com/wms/images/thumbs/')}"/>" border="0" class="thumbnail_image" alt="<c:out value="${product.name}"/><c:if test="${product.shortDesc != ''}"> - <c:out value="${product.shortDesc}"/></c:if>">
	  	   </c:otherwise>
	  	  </c:choose>
	  	  </a>
	    </c:if>
	  </div>
	</td>
  </c:if>
  <c:if test="${siteConfig['SHOW_SKU'].value == 'true'}">
    <td><c:out value="${product.sku}" escapeXml="false" /></td>
  </c:if>
    <td>
  	  <c:choose>
  		<c:when test="${hideLink}"><c:out value="${product.name}" escapeXml="false" /></c:when>
  		<c:otherwise>
  			<c:if test="${!(product.active)}"><c:out value="${product.name}" escapeXml="false" /></c:if>
  			<c:if test="${product.active}">
  				<a href="${productLink}" class="thumbnail_item_name"><c:out value="${product.name}" escapeXml="false" /></a> 
  			</c:if> 
  		</c:otherwise>
  	  </c:choose>
    </td>
  <c:if test="${model.evergreen}">
  <td><c:out value="${product.field6}" escapeXml="false" /></td>
  </c:if>
  <c:forEach items="${product.productFields}" var="productField">
  <c:if test="${productField.quickModeField}">
  <c:choose>
	<c:when test="${productField.formatType != null and productField.formatType != '' and productField.fieldType == 'number' }">
		<td align="center"><fmt:formatNumber value="${wj:trimAndSplit(productField.value, '|' , false, ',', true, true)}" pattern="${productField.formatType }" /></td>
	</c:when>
	<c:otherwise>
		<td align="center"><c:out value="${wj:trimAndSplit(productField.value, '|' , false, ',', true, true)}" escapeXml="false"/></td>
	</c:otherwise>
	</c:choose> 
  </c:if>
  </c:forEach>

  <c:if test="${model.showPriceColumn == null or model.showPriceColumn}">
  <td width="30%" style="white-space: nowrap">
	<input type="hidden" name="product.id" value="${product.id}">
	<c:choose>
	<c:when test="${product.salesTag != null and !product.salesTag.percent}">
	  <%@ include file="/WEB-INF/jsp/frontend/common/price3.jsp" %>
	</c:when>
	<c:otherwise>
	  <%@ include file="/WEB-INF/jsp/frontend/common/price.jsp" %>
	</c:otherwise>
	</c:choose>
	<%@ include file="/WEB-INF/jsp/frontend/common/productOptions.jsp" %>
	<%@ include file="/WEB-INF/jsp/frontend/common/masterSku.jsp" %>
  </td>	
  </c:if>
  
  <c:if test="${gSiteConfig['gSHOPPING_CART'] and (model.showQtyColumn == null or model.showQtyColumn)}">
  <c:set var="zeroPrice" value="false"/>
  <c:forEach items="${product.price}" var="price">
    <c:if test="${price.amt == 0}">
      <c:set var="zeroPrice" value="true"/>
    </c:if>
  </c:forEach> 
  <td width="15%" style="white-space: nowrap">
  <c:if test="${ (gSiteConfig['gSHOPPING_CART'] and (!empty product.price or product.priceByCustomer)) and (!product.loginRequire or userSession != null)}"> 
  <c:choose>
   <c:when test="${product.type == 'box'}">
     <a href="${productLink}"><img border="0" src="${_contextpath}/assets/Image/Layout/button_buildthebox.gif" /></a>
   </c:when>
   <c:when test="${gSiteConfig['gVARIABLE_PRICE'] and product.priceByCustomer}">  
     <c:set var="showAddToCart" value="true" />
     	<input type="checkbox" name="quantity_${product.id}" id="quantity_${product.id}" value="1" />
   </c:when>
   <c:when test="${!empty product.numCustomLines}">
     <a href="${productLink}"><img border="0" src="${_contextpath}/assets/Image/Layout/button_continue.gif" /></a>
   </c:when>
   <c:otherwise>
     <c:choose>
	    <c:when test="${gSiteConfig['gINVENTORY'] and product.inventory != null and !product.negInventory and product.inventory <= 0}" >
	      <c:out value="${siteConfig['OUT_OF_STOCK_TITLE'].value}" escapeXml="false"/>
	    </c:when>
		<c:when test="${zeroPrice and siteConfig['ZERO_PRICE_HTML'].value != ''}">
	      <c:out value="${siteConfig['ZERO_PRICE_HTML'].value}" escapeXml="false"/>
	    </c:when>
	    <c:otherwise>
	      <c:set var="showAddToCart" value="true" />
	      <c:if test="${product.active}">
	      	<fmt:message key="qty" />: <input type="text" size="6" maxlength="5" name="quantity_${product.id}" id="quantity_${product.id}" value="<c:out value="${cartItem.quantity}"/>" > 
	      </c:if>
	      <c:if test="${gSiteConfig['gINVENTORY'] and product.inventory != null and product.inventory < product.lowInventory}">
	       <c:out value="${siteConfig['LOW_STOCK_TITLE'].value}" escapeXml="false"/>
	      </c:if>	      
	    </c:otherwise>
	 </c:choose>    
   </c:otherwise>
  </c:choose>
  <c:if test="${gSiteConfig['gPRICE_CASEPACK'] and product.priceCasePackQty != null}">
    <div class="casePrice"><c:out value="${siteConfig['PRICE_CASE_PACK_UNIT_TITLE'].value}" /> <input name="casePrice_${product.id}" type="checkbox" value="true" /></div>
  </c:if>
  </c:if>&nbsp;
  </td>
  </c:if>
</tr>

<c:if test="${status.last}">
</table>
<c:if test="${gSiteConfig['gSHOPPING_CART'] and showAddToCart}">
<div id="quickmode_v1_addtocart" align="right">
<input type="image" value="Add to Cart" name="_addtocart_quick" class="_addtocart_quick" src="${_contextpath}/assets/Image/Layout/button_addtocart${_lang}.gif" >
</div>
</c:if>
</form>
</c:if>