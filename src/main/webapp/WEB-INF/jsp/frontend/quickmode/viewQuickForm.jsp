<%@ page session="false" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ taglib uri="/WEB-INF/tlds/custom.tld" prefix="wj" %>
 
<c:set var="productLink">${_contextpath}/product.jhtm?id=${cartItem.product.id}</c:set>
<c:if test="${gSiteConfig['gMOD_REWRITE'] == '1' and not fn:contains(cartItem.product.sku, '/')}">
	<c:set var="productLink">${_contextpath}/${siteConfig['MOD_REWRITE_PRODUCT'].value}/${cartItem.product.encodedSku}/${cartItem.product.encodedName}.html</c:set>
</c:if> 
<div class="col-sm-12 col-md-1">
<div class="listImageHeader visible-xs visible-sm">&nbsp;</div>
<div class="listImageWrapper">
 <c:if test="${(showImage == null or showImage) and (siteConfig['QUICK_MODE_THUMB_HEIGHT'].value > 0 or siteConfig['QUICK_MODE_THUMB_HEIGHT'].value == '')}" >
    <c:if test="${product.thumbnail == null}">&nbsp;</c:if>
    <c:if test="${product.thumbnail != null}">
	  <a href="${productLink}">
 	  <c:choose>
  	   <c:when test="${product.asiId != null}">
  	   <img src="<c:out value="${fn:replace(product.thumbnail.imageUrl, 'prodimgs', 'prodthumbs')}"/>" class="listImage img-responsive">
  	   </c:when>
  	   <c:otherwise>
  	   <img src="<c:if test="${!product.thumbnail.absolute}">${_contextpath}/assets/Image/Product/thumb/</c:if><c:out value="${fn:replace(product.thumbnail.imageUrl, 'http://www.dollaritem.com/wms/images/catalog/', 'http://www.dollaritem.com/wms/images/thumbs/')}"/>" class="listImage img-responsive">
  	   </c:otherwise>
  	  </c:choose>
  	  </a>
    </c:if>
 </c:if>
</div>
<div class="clearfix"></div>
</div> 
 
<div class="col-sm-12 col-md-2">
<div class="listSkuHeader visible-xs visible-sm">SKU</div>
<div class="listSku">
  <c:if test="${siteConfig['SHOW_SKU'].value == 'true' and siteConfig['SHOW_MASTER_SKU_ON_PRODUCT'].value != 'true'}">
    <c:out value="${product.sku}" escapeXml="false" />
  </c:if>
 
  <c:if test="${siteConfig['SHOW_MASTER_SKU_ON_PRODUCT'].value == 'true'}">
  	<c:set var="id" value="${product.id}"/>
  	<c:out value="${model.masterSkuField[id]}"/>
  </c:if>
</div>
<div class="clearfix"></div>
</div>  
  
 
<div class="col-sm-12 col-md-4">
<div class="listNameHeader visible-xs visible-sm">Product</div>
<div class="listName"> 
<c:choose>
<c:when test="${hideLink}"><c:out value="${product.name}" escapeXml="false" /></c:when>
<c:otherwise>
	  <a href="${productLink}" class="thumbnail_item_name"><c:out value="${product.name}" escapeXml="false" /></a>
</c:otherwise>
</c:choose>
</div>
<div class="clearfix"></div> 
<div class="productOptionsWrapper">
	<%@ include file="/WEB-INF/jsp/frontend/layout/template6/productOptions2.jsp" %>
	</div>
	<div class="clearfix"></div>
</div>   
   

<div class="col-sm-12 col-md-3">
<div class="listDescriptionHeader visible-xs visible-sm">Description</div>
<div class="listDescription">   
<c:if test="${siteConfig['QUICK_MODE_SHORT_DESC_TITLE'].value != '' and (showDesc == null or showDesc)}">
 <c:out value="${product.shortDesc}" escapeXml="false" />
</c:if>
</div>
<div class="clearfix"></div>
</div>
 
 
<div class="col-sm-12 col-md-1">
<div class="listPriceHeader visible-xs visible-sm">Price</div>
<div class="listPrice">
<c:if test="${model.showPriceColumn == null or model.showPriceColumn or model.slavesList != null}">
<input type="hidden" name="product.id" value="${product.id}">
<c:choose>
<c:when test="${product.salesTag != null and !product.salesTag.percent}">
  <%@ include file="/WEB-INF/jsp/frontend/common/price3.jsp" %>
</c:when>
<c:otherwise>
  <%@ include file="/WEB-INF/jsp/frontend/layout/template6/price/price2.jsp" %>
</c:otherwise>
</c:choose>
<%@ include file="/WEB-INF/jsp/frontend/common/masterSku.jsp" %>	
 </c:if>
</div>
<div class="clearfix"></div>
</div> 


