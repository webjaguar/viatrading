<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ taglib uri="/WEB-INF/tlds/custom.tld" prefix="wj" %>

<c:if test="${siteConfig['PRODUCT_QUOTE'].value == 'true'}">
<script type="text/javascript">
function addToQuoteList(productId){
	var requestHTMLData = new Request.HTML ({
		url: "${_contextpath}/quoteList.jhtm?productId="+productId,
		onSuccess: function(response) { 
			$('product'+productId).set('html', '<fmt:message key="f_addedToQuoteList" />');
	 	}
	}).send();
}
</script>
</c:if>
<c:if test="${siteConfig['PRODUCT_QUOTE'].value == 'true' and (model.showProductQuoteColumn == null or model.showProductQuoteColumn)}" >
<div id="quoteButton"> 
   <a href="${_contextpath}/quoteViewList.jhtm"> <img alt="View Quote" src="${_contextpath}/assets/Image/Layout/button_viewQuoteList.jpg"> </a>
</div>
</c:if>

<c:forEach items="${model.groupProductsMap}" var="groupProductMap" >
<div class="tableTitle" align="center">Type: <c:out value="${groupProductMap.key}"></c:out></div>
  
<form action="${_contextpath}/addToCart.jhtm" method="post" onsubmit="return checkForm()">
<table border="0" cellpadding="3" cellspacing="1" align="center" class="quickmode">
  <tr>
  <c:if test="${siteConfig['PRODUCT_QUOTE'].value == 'true' and (model.showProductQuoteColumn == null or model.showProductQuoteColumn)}" >
  	<th class="quickmodeQuote"><fmt:message key="f_addToQuote" /></th>
  </c:if>
   <c:if test="${(showImage == null or showImage) and (siteConfig['QUICK_MODE_THUMB_HEIGHT'].value > 0 or siteConfig['QUICK_MODE_THUMB_HEIGHT'].value == '')}" >
    <th class="quickmode image"><fmt:message key="image" /></th>
   </c:if> 
   <c:if test="${siteConfig['SHOW_SKU'].value == 'true'}">
    <th class="quickmode"><fmt:message key="productSku" /></th>
   </c:if> 
    <th class="quickmode"><fmt:message key="f_productName" /></th>
<c:if test="${siteConfig['QUICK_MODE_SHORT_DESC_TITLE'].value != '' and (showDesc == null or showDesc)}">
    <th class="quickmode"><c:out value="${siteConfig['QUICK_MODE_SHORT_DESC_TITLE'].value}"/></th>
</c:if>
	<c:forEach items="${model.productFieldsMap[groupProductMap.key]}" var="productField">
	  <c:if test="${productField.quickModeField}">
	    <th class="quickmode"><c:out value="${productField.name}" escapeXml="false" /></th>
	  </c:if>
	</c:forEach>
	<c:if test="${model.showPriceColumn == null or model.showPriceColumn}">
	<th class="quickmode">Price</th>
	</c:if>
  </tr>

<c:forEach items="${groupProductMap.value}" var="product" varStatus="status">
<c:set var="productLink">${_contextpath}/product.jhtm?id=${product.id}<c:if test="${model.product == null or siteConfig['RECOMMENDED_LIST_SHOW_BREADCRUMB'].value == 'true'}">&cid=<c:out value="${param.cid}"/></c:if></c:set>
<c:if test="${gSiteConfig['gMOD_REWRITE'] == '1' and not fn:contains(product.sku, '/')}">
  <c:set var="productLink">${_contextpath}/${siteConfig['MOD_REWRITE_PRODUCT'].value}/${product.encodedSku}/${product.encodedName}.html<c:if test="${model.product == null or siteConfig['RECOMMENDED_LIST_SHOW_BREADCRUMB'].value == 'true'}">?cid=<c:out value="${param.cid}"/></c:if></c:set>
</c:if>	

<tr class="row${status.index % 2}">
  <c:if test="${siteConfig['PRODUCT_QUOTE'].value == 'true' and (model.showProductQuoteColumn == null or model.showProductQuoteColumn)}" >
  <td class="quickmodeQuote" id="product${product.id}">
  <c:if test="${product.quote}">
		 <input name="__selected_product_id" value="${product.id}" type="checkbox" onchange="addToQuoteList('${product.id}');" /> 	
  </c:if>
  </td>
  </c:if>
  <c:if test="${(showImage == null or showImage) and (siteConfig['QUICK_MODE_THUMB_HEIGHT'].value > 0 or siteConfig['QUICK_MODE_THUMB_HEIGHT'].value == '')}" >
 	<td class="image">
	  <div align="center">
	    <c:if test="${product.thumbnail == null}">&nbsp;</c:if>
	    <c:if test="${product.thumbnail != null}">
		  <a href="${productLink}">
	  	  <c:choose>
	  	   <c:when test="${product.asiId != null}">
	  	   <img src="<c:out value="${fn:replace(product.thumbnail.imageUrl, 'prodimgs', 'prodthumbs')}"/>" border="0" class="thumbnail_image" alt="<c:out value="${product.name}"/><c:if test="${product.shortDesc != ''}"> - <c:out value="${product.shortDesc}"/></c:if>">
	  	   </c:when>
	  	   <c:otherwise>
	  	   <img src="<c:if test="${!product.thumbnail.absolute}">${_contextpath}/assets/Image/Product/thumb/</c:if><c:out value="${fn:replace(product.thumbnail.imageUrl, 'http://www.dollaritem.com/wms/images/catalog/', 'http://www.dollaritem.com/wms/images/thumbs/')}"/>" border="0" class="thumbnail_image" alt="<c:out value="${product.name}"/><c:if test="${product.shortDesc != ''}"> - <c:out value="${product.shortDesc}"/></c:if>">
	  	   </c:otherwise>
	  	  </c:choose>
	  	  </a>
	    </c:if>
	  </div>
	</td>
  </c:if>
  <c:if test="${siteConfig['SHOW_SKU'].value == 'true'}">
    <td><c:out value="${product.sku}" escapeXml="false" /></td>
  </c:if>
    <td>
  	  <c:choose>
  		<c:when test="${hideLink}"><c:out value="${product.name}" escapeXml="false" /></c:when>
  		<c:otherwise>
    	  <a href="${productLink}" class="thumbnail_item_name"><c:out value="${product.name}" escapeXml="false" /></a>
  		</c:otherwise>
  	  </c:choose>
    </td>
<c:if test="${siteConfig['QUICK_MODE_SHORT_DESC_TITLE'].value != '' and (showDesc == null or showDesc)}">
  <td><c:out value="${product.shortDesc}" escapeXml="false" /></td>
</c:if>
  <c:forEach items="${product.productFields}" var="productField">
  <c:if test="${productField.quickModeField}"> 
  <c:choose>
	<c:when test="${productField.formatType != null and productField.formatType != '' and productField.fieldType == 'number' }">
		<td align="center"><fmt:formatNumber value="${wj:trimAndSplit(productField.value, '|' , false, ',', true, true)}" pattern="${productField.formatType }" /></td>
	</c:when>
	<c:otherwise>
		<td align="center"><c:out value="${wj:trimAndSplit(productField.value, '|' , false, ',', true, true)}" escapeXml="false"/></td>
	</c:otherwise>
	</c:choose> 
  </c:if>
  </c:forEach>

  <c:if test="${model.showPriceColumn == null or model.showPriceColumn}">
  <td width="30%" style="white-space: nowrap">
	<input type="hidden" name="product.id" value="${product.id}">
	<c:choose>
	<c:when test="${product.salesTag != null and !product.salesTag.percent}">
	  <%@ include file="/WEB-INF/jsp/frontend/common/price3.jsp" %>
	</c:when>
	<c:otherwise>
	  <%@ include file="/WEB-INF/jsp/frontend/common/price.jsp" %>
	</c:otherwise>
	</c:choose>
	<%@ include file="/WEB-INF/jsp/frontend/common/productOptions.jsp" %>
	<%@ include file="/WEB-INF/jsp/frontend/common/masterSku.jsp" %>
  </td>	
  </c:if>
  
  <c:if test="${gSiteConfig['gSHOPPING_CART'] and (model.showQtyColumn == null or model.showQtyColumn)}">
  <c:set var="zeroPrice" value="false"/>
  <c:forEach items="${product.price}" var="price">
    <c:if test="${price.amt == 0}">
      <c:set var="zeroPrice" value="true"/>
    </c:if>
  </c:forEach> 
  </c:if>
</tr>
</c:forEach>
</table>
</form>

</c:forEach>