<%@ page import="java.util.*" %>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="/WEB-INF/tlds/spring.tld" prefix="spring" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ taglib uri="/WEB-INF/tlds/custom.tld" prefix="wj" %>
<%-- PPG --%>
<tiles:insertDefinition name="${_template}" flush="true">
  <tiles:putAttribute name="leftbar" value="/WEB-INF/jsp/frontend/layout/template${_leftBar}/leftbar.jsp" />
  <tiles:putAttribute name="content" type="string">

<c:if test="${fn:trim(model.productLayout.headerHtml) != ''}">
  <c:set value="${model.productLayout.headerHtml}" var="headerHtml"/>
  <%@ include file="/WEB-INF/jsp/frontend/common/headerDynamicElement.jsp" %>
  <div><c:out value="${headerHtml}" escapeXml="false"/></div>
</c:if>

<c:if test="${model.message != null}">
  <div class="message"><fmt:message key="${model.message}" /></div>
</c:if>

<c:if test="${model.product != null}">
<c:set value="${model.product}" var="product"/>

<c:if test="${siteConfig['PRODUCT_QUOTE'].value == 'true' and product.quote}">  
<script type="text/javascript">
function addToQuoteList(productId){
	var requestHTMLData = new Request.HTML ({
		url: "${_contextpath}/quoteList.jhtm?productId="+productId,
		onSuccess: function(response){
			$('addToQuoteListWrapperId'+productId).set('html', '<h4><fmt:message key="f_addedToQuoteList" /></h4> <a href="${_contextpath}/quoteViewList.jhtm" rel="type:element" id="quoteListId" class="quoteList"> <input type="image" border="0" name="_viewQuoteList" class="_viewQuoteList" src="${_contextpath}/assets/Image/Layout/button_viewQuoteList.jpg" /> </a> ');
		}
	}).send();
}
//-->
</script>
</c:if>

<script type="text/javascript">
<!--
window.addEvent('domready', function() {
	 var tabs1 = new SimpleTabs($('tab-block-1'), { selector: 'h4'});
	 $$('.priceGrid').each(function(ele){
		if(ele.id != 'priceGridId0') {
			ele.style.display= "none";
		}
	});
	if($('toggle') != null) {
		$('toggle').addEvent('click', function(e){
			if($('toggle').get('text') == 'Show All'){
				$$('.priceGrid').each(function(ele){
					ele.style.display = "";
				});
				$('toggle').set('text', 'Hide');
			} else {
				$$('.priceGrid').each(function(ele){
					if(ele.id != 'priceGridId0') {
						ele.style.display = "none";
					}
				});
				$('toggle').set('text', 'Show All');
			}
			e.stop();
		});
	}
});

function updateSteps(productId) {
	if($('childSelect') != null && $('childSelect').value  == ''){
		alert('Please select a product');
		return false;
	}
	$('newSteps').set('style','background:url(/assets/Image/Layout/spinner.gif) no-repeat center 50%; background-color : #EEEEEE;');

	$('customizeButton').set('text', 'Loading...');
	try { $('saveSettings').set('text', 'Saving...'); }
	catch(err) {
	  //Handle errors here
	}
	if($('productOptionsId') != null) {
		$('productOptionsId').fade('0.1');
	}
	var urlappend = '';	
	$$('select.optionSelect').each(function(select){
		urlappend = urlappend + '&option='+select.name+'-'+select.value;
	});
	
	$$('textarea.optionTextArea').each(function(textarea){
		urlappend = urlappend + '&option_cus='+textarea.name+'-'+escape(textarea.value);
	});
	$$('input.quantityBox').each(function(input){
		urlappend = urlappend +'&'+ input.name+'='+input.value;
	});
	$$('input.colorSwatch').each(function(input){
		urlappend = urlappend +'&'+ input.name+'='+input.value;
		if($('selectedImprintColors'+input.value) != null){
			urlappend = urlappend +'&selectedImprintColors'+input.value+'='+$('selectedImprintColors'+input.value).value;
		}
	});
	
	if($('childSelect') != null){
		urlappend = urlappend +'&selectedChild='+$('childSelect').value;
	}
//	alert(urlappend);
	var requestHTMLData = new Request.HTML ({
		url: "${_contextpath}/ajax-premier-options.jhtm?&productId="+productId+urlappend,
		update: $('newSteps'),
		onSuccess: function(response){
			$('productOptionsId').fade('1.0');
			$('newSteps').set('style','background:url(none)');
			$('customizeButton').set('text', 'Customize Now');
			$('saveSettings').set('text', 'Save');
		},
		onFailure: function(response){
			$('productOptionsId').fade('1.0');
			$('newSteps').set('style','background:url(none)');
			alert('An error occured during processing request. Please try again later.');
    	}
	}).send();
}

function getOption(productId){
	$('newOption').set('style','background:url(/assets/Image/Layout/spinner.gif) no-repeat center 50%; background-color : #EEEEEE;');
	$('productOptionsId').fade('0.1');
	var urlappend = '';	
	$$('select.optionSelect').each(function(select){
		urlappend = urlappend + '&option='+select.name+'-'+select.value;
	});
	var requestHTMLData = new Request.HTML ({
		url: "${_contextpath}/show-ajax-options.jhtm?&productId="+productId+urlappend,
		update: $('newOption'),
		onSuccess: function(response){
			$('productOptionsId').fade('1.0');
			$('newOption').set('style','background:url(none)');
		},
		onFailure: function(response){
			$('productOptionsId').fade('1.0');
			$('newOption').set('style','background:url(none)');
			alert('An error occured during processing request. Please try again later.');
    	}
	}).send();
}
function checkForm() {
	var productList = document.getElementsByName('product.id');
	var allQtyEmpty = true;
	for(var i=0; i<productList.length; i++) {
		if(productList[i].value == '' || productList[i].value == null){
			alert("Select Product");
			return false;
		}
		
		var el = $("quantity_"+productList[i].value);
		if ( el != null ) {
			if ( checkNumber( el.value ) == 1  ) {
				allQtyEmpty = false;
			} else if ( checkNumber( el.value ) == -1 )  {
				alert("invalid Quantity");
				el.focus();
				return false;
			}			
		}	
	}
	if ( allQtyEmpty ) {
		alert("Please Enter Quantity.");
		return false;
	} else {
		return true;	
	}
}
function checkNumber( aNumber ) {
	var goodChars = "0123456789";
	var i = 0;
	if ( aNumber == "" )
		return 0; //empty
	
	for ( i = 0 ; i <= aNumber.length - 1 ; i++ ) {
		if ( goodChars.indexOf( aNumber.charAt(i) ) == -1 ) 
			return -1; //invalid number			
	}	
	return 1; //valid number
}
//-->
</script>
<c:if test="${model.product.productLayout == '018' or (model.product.productLayout == '' and siteConfig['PRODUCT_DEFAULT_DETAIL_LAYOUT'].value == '018')}">
<script type="text/javascript">
<!--
function updateChildProduct(productId){
	if(productId == null || productId == ''){
		return false;
	}
	var requestHTMLData = new Request.HTML ({
		url: "${_contextpath}/ajaxUpdateChildProduct2.jhtm?id="+productId,
		onSuccess: function(responseTree, responseElements, responseHTML, responseJavaScript){
			responseElements.each(function(el) {
	       	 	// get the id
	            var id = el.get("id");
	            if(id != null){
	            	if(el.nodeName == 'DIV' || el.nodeName == 'SPAN' ){
		            	// remove the id from the element so dom selector works correctly
			            el.set("id", null);
			            // look for dom element that matches
			            // if found, update html
			            if ($(id)) {
			            	$(id).set("html", el.get("html"));
			            }
	            	} else {
		            	// look for dom element that matches
			            // if found, replace element
			            if ($(id)) {
			            	el.replaces($(id));
			            }
	            	}
	            }
	        });
			new multiBox('mbxwz', {overlay: new overlay()});
	    }
	}).send();
	
	$$('.priceGrid').each(function(ele){
		ele.style.display = 'none';		
	});
	
	$$('.gridId'+productId).each(function(ele){
		ele.style.display = '';		
	});
	try { $('casePriceId').set('name', 'casePrice_'+productId);  }
	catch(err) {
	  //Handle errors here
	  }
}
//-->
</script>
</c:if>

<%-- bread crumbs --%>
<c:import url="/WEB-INF/jsp/frontend/layout/${_template}/breadcrumbs.jsp" />

<div class="details15">
<c:set var="productImage">
<c:choose>
 <c:when test="${fn:contains(siteConfig['PRODUCT_IMAGE_LAYOUTS'].value,'mz') and 
                (product.imageLayout == 'mz' or (empty product.imageLayout and siteConfig['PRODUCT_IMAGE_LAYOUT'].value == 'mz'))}">
   <link rel="stylesheet" href="${_contextpath}/assets/SmoothGallery/MagicZoomPlus/magiczoomplus.css" type="text/css" media="screen" />
   <script src="${_contextpath}/javascript/magiczoomplus.js" type="text/javascript" ></script>
   <div class="details_image_box" id="details_image_boxId" style="${details_images_style}
        <c:if test="${siteConfig['DETAILS_IMAGE_WIDTH'].value != '' }">width: <c:out value="${siteConfig['DETAILS_IMAGE_WIDTH'].value}"/>px;</c:if>">
     <c:forEach items="${model.product.images}" var="image" varStatus="status">
       <c:if test="${status.first}">
         <a href="<c:if test="${!image.absolute}">${_contextpath}/assets/Image/Product/superBig/</c:if><c:out value="${fn:replace(image.imageUrl, 'detailsbig', 'superBig')}"/>" rel="zoom-width:480px;zoom-height:360px;expand-align:screen; expand-position:top=0; expand-size:original; selectors-effect:pounce; zoom-position:#zoomImageId;" class="MagicZoomPlus" id="Zoomer" >
           <img alt="" src="<c:if test="${!image.absolute}">${_contextpath}/assets/Image/Product/detailsbig/</c:if><c:out value="${fn:replace(product.thumbnail.imageUrl, 'size=large', 'size=normal')}"/>" border="0" class="normal_image" alt="<c:out value="${product.name}"/><c:if test="${product.shortDesc != ''}"> - <c:out value="${product.shortDesc}"/></c:if>" />
         </a>
       </c:if>
     </c:forEach>
     <div style="clear: both;" >
       <div id="thumbnailWrapper">
         <c:forEach items="${model.product.images}" var="thumbImage" varStatus="status">
           <c:if test="${status.count > 0}">
             <c:choose>
               <c:when test="${product.asiId != null or true}">
                 <a class="thumb_link" rev="<c:if test="${!thumbImage.absolute}">${_contextpath}/assets/Image/Product/detailsbig/</c:if><c:out value="${fn:replace(thumbImage.imageUrl, 'size=large', 'size=normal')}"/>" rel="zoom-id:Zoomer;zoom-width:480px;zoom-height:360px;zoom-position:#zoomImageId;" href="<c:if test="${!thumbImage.absolute}">${_contextpath}/assets/Image/Product/superBig/</c:if><c:out value="${fn:replace(thumbImage.imageUrl, 'detailsbig', 'superBig')}"/>" style="outline: 0pt none; display: inline-block;">
                   <img alt="" src="<c:if test="${!thumbImage.absolute}">${_contextpath}/assets/Image/Product/thumb/</c:if><c:out value="${fn:replace(thumbImage.imageUrl, 'size=large', 'size=small')}"/>">
                 </a>
               </c:when>
               <c:otherwise>
                 <a href="<c:if test="${!image.absolute}">${_contextpath}/assets/Image/Product/detailsbig/</c:if><c:out value="${image.imageUrl}"/>" rel="zoom-id:Zoomer;zoom-width:480px;zoom-height:360px;zoom-position:#zoomImageId;"  rev="<c:if test="${!image.absolute}">${_contextpath}/assets/Image/Product/thumb/</c:if><c:out value="${image.imageUrl}"/>" title="" >
                 <img src="<c:if test="${!image.absolute}">${_contextpath}/assets/Image/Product/thumb/</c:if><c:out value="${image.imageUrl}"/>" border="0" class="zoom_details_thumbnail" alt="<c:out value="${product.name}"/><c:if test="${product.shortDesc != ''}"> - <c:out value="${product.shortDesc}"/></c:if>"/></a>
               </c:otherwise>
             </c:choose>
           </c:if>
         </c:forEach>
       </div>
     </div>
   </div>
 </c:when>
 <c:when test="${fn:contains(siteConfig['PRODUCT_IMAGE_LAYOUTS'].value,'qb') and 
 		(product.imageLayout == 'qb' or (empty product.imageLayout and siteConfig['PRODUCT_IMAGE_LAYOUT'].value == 'qb'))}">
   <link rel="stylesheet" href="${_contextpath}/assets/SmoothGallery/QuickBox/quickbox.css" type="text/css" media="screen" />
   <c:set value="${true}" var="quickbox"/>
   <script src="javascript/QuickBox.js" type="text/javascript" ></script>
   <script type="text/javascript">
	  window.addEvent('domready', function(){
		new QuickBox();
	  });
   </script>
   <div class="details_image_box" id="details_image_boxId" style="${details_images_style}
	    <c:if test="${siteConfig['DETAILS_IMAGE_WIDTH'].value != '' }">width: <c:out value="${siteConfig['DETAILS_IMAGE_WIDTH'].value}"/>px;</c:if>">
	 <c:forEach items="${model.product.images}" var="image" varStatus="status">
	   <c:if test="${status.first}">
	     <a href="<c:if test="${!image.absolute}">${_contextpath}/assets/Image/Product/detailsbig/</c:if><c:out value="${image.imageUrl}"/>" id="" class="" title="" rel="quickbox"><img src="<c:if test="${!image.absolute}">${_contextpath}/assets/Image/Product/detailsbig/</c:if><c:out value="${image.imageUrl}"/>" border="0" style="<c:if test="${siteConfig['DETAILS_IMAGE_WIDTH'].value != '' }">width: <c:out value="${siteConfig['DETAILS_IMAGE_WIDTH'].value}"/>px</c:if>" alt="<c:out value="${product.name}"/><c:if test="${product.shortDesc != ''}"> - <c:out value="${product.shortDesc}"/></c:if>"/></a>
	   </c:if>
	 </c:forEach>
     <div style="clear: both;" >
       <c:forEach items="${model.product.images}" var="image" varStatus="status">
	     <c:if test="${status.count > 1}">
	       <a href="<c:if test="${!image.absolute}">${_contextpath}/assets/Image/Product/detailsbig/</c:if><c:out value="${image.imageUrl}"/>" id="qb${status.count - 1}" class="" title="" rel="quickbox"><img src="<c:if test="${!image.absolute}">${_contextpath}/assets/Image/Product/detailsbig/</c:if><c:out value="${image.imageUrl}"/>" border="0" class="details_thumbnail" alt="<c:out value="${product.name}"/><c:if test="${product.shortDesc != ''}"> - <c:out value="${product.shortDesc}"/></c:if>"/></a>
	     </c:if>
	   </c:forEach>
	 </div>
   </div>        
 </c:when>
 <c:otherwise>
   <link rel="stylesheet" href="${_contextpath}/assets/SmoothGallery/multibox.css" type="text/css" media="screen" />
   <c:set value="${true}" var="multibox"/>
   <script src="javascript/overlay.js" type="text/javascript" ></script>
   <script src="javascript/multibox.js" type="text/javascript" ></script>
   <script type="text/javascript">
	 window.addEvent('domready', function(){
	   var box = new multiBox('mbxwz', {overlay: new overlay()});
	 });
   </script>
   <div class="details_image_box" id="details_image_boxId" style="${details_images_style}
	 <c:if test="${siteConfig['DETAILS_IMAGE_WIDTH'].value != '' }">width: <c:out value="${siteConfig['DETAILS_IMAGE_WIDTH'].value}"/>px;</c:if>">
	 <c:forEach items="${model.product.images}" var="image" varStatus="status">
	   <c:if test="${status.first}">
	     <a href="<c:if test="${!image.absolute}">${_contextpath}/assets/Image/Product/detailsbig/</c:if><c:out value="${image.imageUrl}"/>" id="mb0" class="mbxwz" title=""><img src="<c:if test="${!image.absolute}">${_contextpath}/assets/Image/Product/detailsbig/</c:if><c:out value="${image.imageUrl}"/>" border="0" style="<c:if test="${siteConfig['DETAILS_IMAGE_WIDTH'].value != '' }">width: <c:out value="${siteConfig['DETAILS_IMAGE_WIDTH'].value}"/>px</c:if>" alt="<c:out value="${product.name}"/><c:if test="${product.shortDesc != ''}"> - <c:out value="${product.shortDesc}"/></c:if>"/></a>
	   </c:if>
	 </c:forEach>
     <div style="clear: both;" >
       <c:forEach items="${model.product.images}" var="image" varStatus="status">
	    <c:if test="${status.count > 1}">
	       <a href="<c:if test="${!image.absolute}">${_contextpath}/assets/Image/Product/detailsbig/</c:if><c:out value="${image.imageUrl}"/>" id="mb${status.count - 1}" class="mbxwz" title=""><img src="<c:if test="${!image.absolute}">${_contextpath}/assets/Image/Product/detailsbig/</c:if><c:out value="${image.imageUrl}"/>" border="0" class="details_thumbnail" alt="<c:out value="${product.name}"/><c:if test="${product.shortDesc != ''}"> - <c:out value="${product.shortDesc}"/></c:if>"/></a>
	     </c:if>
	   </c:forEach>
	 </div>
   </div>        
 </c:otherwise>
</c:choose>
</c:set>

<!-- image start -->
   <c:out value="${productImage}" escapeXml="false" />
<!-- image end -->

<!-- Details Wrapper Start -->
<div id="detailsWrapperId">
  <div id="zoomImageId"></div>
  <c:if test="${siteConfig['SHOW_SKU'].value == 'true'}">
    <c:if test="${product.sku != null}">
      <div class="details_sku" id="details_sku_id"><c:out value="${product.sku}" /></div>
    </c:if>
  </c:if>

<div class="details_item_name" id="details_item_name_id">
  <h1><c:out value="${product.name}" escapeXml="false" /></h1>
</div>

<c:if test="${siteConfig['DETAILS_SHORT_DESC'].value == 'true' and model.product.shortDesc != ''}">
  <div class="details_short_desc" id="details_short_desc_id"><c:out value="${model.product.shortDesc}" escapeXml="false" /></div>
</c:if>

<c:if test="${siteConfig['TECHNOLOGO'].value != ''}">
  <div id="technologoWrapper">
    <a href="http://www.technologo.com/techno.OnDemand?&email=<c:out value="${siteConfig['TECHNOLOGO'].value}" />&sku=${model.product.id}&name=${model.product.nameWithoutQuotes}&imagelocation_0=${wj:asiBigImageName(product.thumbnail.imageUrl)}&whiteout=true&orientation=10000&removelogo=true" target="_blank">
      <img src="${_contextpath}/assets/Image/Layout/button_create_virtual.gif"  border="0" alt="create virtual" />
    </a>
  </div>
</c:if>

<c:if test="${gSiteConfig['gDEALS'] > 0 and model.product.deal.htmlCode != null}">
  <c:out value="${model.product.deal.htmlCode}" escapeXml="false"/>
</c:if>
<c:if test="${gSiteConfig['gDEALS'] > 0 and product.deal.parentHtmlCode != null}">
	<c:out value="${product.deal.parentHtmlCode}" escapeXml="false"/>
</c:if>

<div class="boxWrapper" align="right">
<c:choose>
<c:when test="${gSiteConfig['gPRODUCT_REVIEW'] and model.product.enableRate}">
<%@ include file="/WEB-INF/jsp/frontend/common/quickProductReview.jsp" %>
</c:when>
<c:when test="${siteConfig['PRODUCT_RATE'].value == 'true' and model.product.enableRate}">
<%@ include file="/WEB-INF/jsp/frontend/common/productRate.jsp" %>
</c:when>
</c:choose>
</div>
<c:if test="${ (model.product.salesTag.image and !empty model.product.price) and (!model.product.loginRequire or userSession != null) }" >
  <div class="salesTagImageWrapper">
    <div class="salesTagImage">
      <img align="right"  src="${_contextpath}/assets/Image/PromoSalesTag/salestag_${model.product.salesTag.tagId}.gif" />
    </div>
  </div>
</c:if>
</div>
<div class="clear" id="clear1"></div>

<div id="priceAddToCartFormWrapper">
<c:if test="${ (gSiteConfig['gSHOPPING_CART'] and (!empty product.price or product.priceByCustomer)) and (!product.loginRequire or userSession != null)}">
<%--Start Price Container --%>
<div id="priceContainerById" class="price_container">
  <div id="priceGridContainer">
    <div class="hiddenTable" id="standardPricing">
      <c:choose>
        <c:when test="${empty model.slavesList}">
          <table cellspacing="0" cellpadding="0" border="0" class="priceGrid" id="priceGridId0" width="100%">
            <tr class="qtyRow">
              <td class="bold left qty">Quantity</td>
              <td class="space"></td>
	          <c:forEach items="${product.price}" var="price" varStatus="statusPrice">
	            <td class="bold center">
	              <c:choose>
	                <c:when test="${price.qtyFrom == null}"><c:out value='${product.minimumQty}' /></c:when>
	                <c:otherwise><c:out value="${price.qtyFrom}" /></c:otherwise>
	              </c:choose>
	              <c:choose>
	                <c:when test="${price.qtyTo == null}">+</c:when>
	                <c:otherwise>- <c:out value="${price.qtyTo}" /></c:otherwise>
	              </c:choose>
	            </td>
	            <c:if test="${!statusPrice.last}">
	              <td class="space"></td>
	            </c:if>
	          </c:forEach>
	        </tr>
	        <tr>
	          <td class="spacer"></td>
	        </tr>
	        <c:if test="${product.salesTag != null and product.salesTag.discount != 0.0}" >
	          <tr class="priceRow">
	            <td class="bold left">Retail</td><td class="space"></td>
	            <c:forEach items="${product.price}" var="price" varStatus="statusPrice">
	              <td class="bold center" style="text-decoration: line-through">
	                <fmt:message key="${siteConfig['CURRENCY'].value}" /><fmt:formatNumber value="${price.amt}" pattern="#,##0.00" />
	              </td>
	              <c:if test="${!statusPrice.last}">
	                <td class="space"></td>
	              </c:if>
	            </c:forEach>
	          </tr>
	          <tr>
	            <td class="spacer"></td>
	          </tr>
	        </c:if>
	        <tr class="salesRow" <c:if test="${fn:length(childProduct.price) > 0 and product.endQtyPricing and !(product.salesTag != null and product.salesTag.discount != 0.0)}">style="text-decoration: line-through;"</c:if>>
	          <td class="bold left"> <fmt:message key="f_youPay" /> </td>
	          <td class="space"></td>
	          <c:forEach items="${product.price}" var="price" varStatus="statusPrice">
	            <c:choose>
	              <c:when test="${product.salesTag != null and product.salesTag.discount != 0.0}">
	                <td class="bold center"><fmt:message key="${siteConfig['CURRENCY'].value}" /><fmt:formatNumber value="${price.discountAmt}" pattern="#,##0.00" /></td>
	              </c:when>
	              <c:otherwise>
	                <td class="bold center">
	                  <fmt:message key="${siteConfig['CURRENCY'].value}" /><fmt:formatNumber value="${price.amt}" pattern="#,##0.00" />
	                  <c:set var="lastPrice" value="${price.amt}"/>
	                </td>
	              </c:otherwise>
	            </c:choose>
	            <c:if test="${!statusPrice.last}">
	              <td class="space"></td>
	            </c:if>
	          </c:forEach>
	        </tr>
	        <c:if test="${fn:length(childProduct.price) > 0 and product.endQtyPricing and !(product.salesTag != null and product.salesTag.discount != 0.0)}">
	          <tr class="salesRow">
	            <td class="bold left"><fmt:message key="f_youPay" /></td>
	            <td class="space"></td>
	            <c:forEach items="${product.price}" var="price" varStatus="statusPrice">
	              <td class="bold center">
	                <fmt:message key="${siteConfig['CURRENCY'].value}" /><fmt:formatNumber value="${lastPrice}" pattern="#,##0.00" />
	              </td>
	              <c:if test="${!statusPrice.last}">
	                <td class="space"></td>
	              </c:if>
	            </c:forEach>
	          </tr>
	        </c:if>
	        <tr>
	          <td class="spacer"></td>
	        </tr>
	      </table>
	    </c:when>
        <c:otherwise>
          <c:forEach items="${model.slavesList}" var="childProduct" varStatus="priceGridStatus">
	        <c:if test="${priceGridStatus.index == 0}">
	          <div class="toggleButton" align="right">
                <a id="toggle" href="#">Show All</a>
              </div>
            </c:if>
	        <div class="priceWrapper">
	          <c:choose>
	            <c:when test="${fn:length(childProduct.price) == 1}">
	              <!-- If only one price is available, dont show price grid. Just display price1 -->
				  <div class="priceGrid gridId${childProduct.id}" id="priceGridId${priceGridStatus.index}">
				  <div class="priceGridHeading">
			          <c:out value="${childProduct.name}" /> <c:if test="${!empty childProduct.field76}" ><c:out value="${childProduct.field76}" /> </c:if> <c:if test="${!empty product.field67 and ( model.product.productLayout == '018' or (model.product.productLayout == '' and siteConfig['PRODUCT_DEFAULT_DETAIL_LAYOUT'].value == '018'))}"><c:out value="${product.field67}" /> </c:if>
			      </div>
			      <c:set value="${childProduct}" var="product" />
				  <c:choose>
					<c:when test="${product.salesTag != null and !product.salesTag.percent}">
		  			  <%@ include file="/WEB-INF/jsp/frontend/common/price3.jsp" %>
					</c:when>
					<c:otherwise>
		  			  <%@ include file="/WEB-INF/jsp/frontend/common/price.jsp" %>
					</c:otherwise>
				  </c:choose>
			      </div>
			    </c:when>
	            <c:otherwise>
				  <!-- If mulitple prices are available, show price grid.-->
			      <table cellspacing="0" cellpadding="0" border="0" width="100%"  class="priceGrid gridId${childProduct.id}" id="priceGridId${priceGridStatus.index}">
			        <tr class="priceGridHeading">
			          <th align="center" colspan="${fn:length(childProduct.price) * 2 + 1}"><c:out value="${childProduct.name}" escapeXml="false"/> <c:if test="${!empty childProduct.field76}" ><c:out value="${childProduct.field76}" /> </c:if> <c:if test="${!empty product.field67 and ( model.product.productLayout == '018' or (model.product.productLayout == '' and siteConfig['PRODUCT_DEFAULT_DETAIL_LAYOUT'].value == '018'))}" ><c:out value="${product.field67}" /> </c:if></th>
			        </tr>
			        <tr class="qtyRow">
			          <td class="bold left qty">Quantity</td>
			          <td class="space"></td>
			          <c:choose>
			            <c:when test="${childProduct.priceSize < 2}">
			              <td class="bold center"> <c:out value='${childProduct.minimumQty}' />+ </td>
			            </c:when>
			            <c:otherwise>
			              <c:forEach items="${childProduct.price}" var="price" varStatus="statusPrice">
			                <td class="bold center">
			                  <c:choose>
			                    <c:when test="${price.qtyFrom == null}"><c:out value='${childProduct.minimumQty}' /></c:when>
			                    <c:otherwise><c:out value="${price.qtyFrom}" /></c:otherwise>
			                  </c:choose>
			                  <c:choose>
			                    <c:when test="${price.qtyTo == null}">+</c:when>
			                    <c:otherwise>-<c:out value="${price.qtyTo}" /></c:otherwise>
			                  </c:choose>
			                </td>
			                <c:if test="${!statusPrice.last}">
			                  <td class="space"></td>
			                </c:if>
			              </c:forEach>
			            </c:otherwise>
			          </c:choose>
			        </tr>
			        <tr>
			          <td class="spacer"></td>
			        </tr>
			        <c:if test="${childProduct.salesTag != null and childProduct.salesTag.discount != 0.0}" >
			          <tr class="priceRow">
			            <td class="bold left">Retail</td><td class="space"></td>
			            <c:forEach items="${childProduct.price}" var="price" varStatus="statusPrice">
			              <td class="bold center" style="text-decoration: line-through">
			                <fmt:message key="${siteConfig['CURRENCY'].value}" /><fmt:formatNumber value="${price.amt}" pattern="#,##0.00" />
			              </td>
			              <c:if test="${!statusPrice.last}">
			                <td class="space"></td>
			              </c:if>
			            </c:forEach>
			          </tr>
			          <tr>
			            <td class="spacer"></td>
			          </tr>
			        </c:if>
			        <tr class="salesRow" <c:if test="${fn:length(childProduct.price) > 0 and childProduct.endQtyPricing and !(childProduct.salesTag != null and childProduct.salesTag.discount != 0.0)}">style="text-decoration: line-through;"</c:if>>
			          <td class="bold left"><fmt:message key="f_youPay" /></td>
			          <td class="space"></td>
			          <c:forEach items="${childProduct.price}" var="price" varStatus="statusPrice">
			            <c:choose>
			              <c:when test="${childProduct.salesTag != null and childProduct.salesTag.discount != 0.0}">
			                <td class="bold center"><fmt:message key="${siteConfig['CURRENCY'].value}" /><fmt:formatNumber value="${price.discountAmt}" pattern="#,##0.00" /></td>
			              </c:when>
			              <c:otherwise>
			                <td class="bold center">
			                  <fmt:message key="${siteConfig['CURRENCY'].value}" /><fmt:formatNumber value="${price.amt}" pattern="#,##0.00" />
			                  <c:set var="lastPrice" value="${price.amt}"/>
			                </td>
			              </c:otherwise>
			            </c:choose>
			            <c:if test="${!statusPrice.last}">
			              <td class="space"></td>
			            </c:if>
			          </c:forEach>
			        </tr>
			        <c:if test="${fn:length(childProduct.price) > 0 and childProduct.endQtyPricing and !(childProduct.salesTag != null and childProduct.salesTag.discount != 0.0)}">
			          <tr class="salesRow">
			            <td class="bold left"><fmt:message key="f_youPay" /></td>
			            <td class="space"></td>
			            <c:forEach items="${childProduct.price}" var="price" varStatus="statusPrice">
			              <td class="bold center">
			                <fmt:message key="${siteConfig['CURRENCY'].value}" /><fmt:formatNumber value="${lastPrice}" pattern="#,##0.00" />
			              </td>
			              <c:if test="${!statusPrice.last}">
			                <td class="space"></td>
			              </c:if>
			            </c:forEach>
			          </tr>
			        </c:if>
			        <tr>
			          <td class="spacer"></td>
			        </tr>
			      </table>
	            </c:otherwise>
	          </c:choose>
	        </div>
	    </c:forEach>
      </c:otherwise>
    </c:choose>
    </div>
  </div>
</div>
<%--End Price Container --%>
</c:if>

<c:if test="${siteConfig['PRODUCT_CONFIGURATOR'].value != 'true' or (gSiteConfig['gSITE_DOMAIN'] == 'pinatas.com' and model.product.sku != 'custompinata-master')  }">
<!-- Start form -->
<div class="formWrapper">
<form action="${_contextpath}/addToCart.jhtm" method="post" onsubmit="return checkForm()" id="addToCartForm" enctype="multipart/form-data">
<!--Start Quantity Container -->
<div class="qunatityTable">
  <%@ include file="/WEB-INF/jsp/frontend/common/childQuantityInput.jsp" %>
</div>

<div class="optionCartButtonWrapper">

<%@ include file="/WEB-INF/jsp/frontend/common/productOptions5.jsp" %>
 
<!-- if only one prduct needs to be added at a time than move qty box above add to cart button and below options -->
<c:if test="${model.product.productLayout == '018' or (model.product.productLayout == '' and siteConfig['PRODUCT_DEFAULT_DETAIL_LAYOUT'].value == '018')}">
  <div class="quantityTitle"> 
	<fmt:message key="quantity" />:
  </div>
  <div class="quantitybox">
	<span id="quantityboxId">
	  <c:choose>
	    <c:when test="${empty model.slavesList}">
		  <input type="text" size="5" maxlength="5" name="quantity_${product.id}" id="quantity_${product.id}" value="${product.minimumQty}" >
 		</c:when>
 		<c:otherwise>
		  <input type="text" size="5" maxlength="5" name="quantity_${model.slavesList[0].id}" id="quantity_${model.slavesList[0].id}" value="${model.slavesList[0].minimumQty}" >
 		</c:otherwise>
	  </c:choose>
    </span>
	<span id="inventoryMessage"></span>
  </div>
</c:if>        
<div class="buttonContainer" id="buttonContainerId" align="right">
  <c:if test="${gSiteConfig['gSHOPPING_CART'] and showAddToCart}">
    <div id="quickmode_v1_addtocart" align="right">
      <input type="image" value="Add to Cart" name="_addtocart_quick" class="_addtocart_quick" src="${_contextpath}/assets/Image/Layout/button_addtocart${_lang}.gif" >
    </div>
  </c:if>
</div>
<!-- div to clear float, if any -->
<div class="clear"></div>

<c:if test="${gSiteConfig['gPRICE_CASEPACK'] and product.priceCasePackQty != null and caseAmt}">
    <div class="casePrice"><c:out value="${siteConfig['PRICE_CASE_PACK_UNIT_TITLE'].value}" /> <input id="casePriceId" type="checkbox" value="true" /></div>
</c:if>

</div>
<!--End Quantity Container -->
</form>
</div>
<!-- End Form -->
</c:if>

<!--Start Button Container -->
<div class="buttonContainer_addToList" align="right">
  <c:if test="${gSiteConfig['gMYLIST']}">
    <form action="${_contextpath}/addToList.jhtm">
      <input type="hidden" name="product.id" value="${product.id}">
      <div id="addToList" align="right">
        <input type="image" class="_addToList" border="0" src="${_contextpath}/assets/Image/Layout/button_addtolist${_lang}.gif">
      </div>
    </form>
  </c:if>
  <c:if test="${siteConfig['PRODUCT_QUOTE'].value == 'true' and product.quote}">
    <div class="addToQuoteListWrapper" id="addToQuoteListWrapperId${product.id}">
	  <input type="image" border="0" name="addtoquoteList" id="addtoquoteList" class="_addtoquoteList" value="${product.id}" src="${_contextpath}/assets/Image/Layout/button_addToQuoteList.jpg" onClick="addToQuoteList('${product.id}');" /> 	
	</div>	
  </c:if>
</div>
<!--End Button Container -->
<c:if test="${siteConfig['PRODUCT_CONFIGURATOR'].value == 'true' and (gSiteConfig['gSITE_DOMAIN'] != 'pinatas.com' or (gSiteConfig['gSITE_DOMAIN'] == 'pinatas.com' and model.product.sku == 'custompinata-master'))  }">
  <c:if test="${!empty model.product.price}">
	<div class="buttonContainer_customize">
    	<a href="javascript:void(0);" onclick="updateSteps(${model.product.id});" id="customizeButton">Customize Now</a>
	</div>
  </c:if>
</c:if>

</div>

<c:if test="${siteConfig['PRODUCT_CONFIGURATOR'].value == 'true' and (gSiteConfig['gSITE_DOMAIN'] != 'pinatas.com' or (gSiteConfig['gSITE_DOMAIN'] == 'pinatas.com' and model.product.sku == 'custompinata-master'))  }">
<!-- Start form -->
<div class="formWrapper">
<form action="${_contextpath}/addToCart.jhtm" method="post" onsubmit="return checkForm()" id="addToCartForm" enctype="multipart/form-data">
<!-- Configurator Starts -->
  <div id="newSteps"></div>
<!-- Configurator Ends -->
</form>
</div>
<!-- End Form -->
</c:if>

<div class="clear" id="clear2"></div>

<!--Start Tabs -->
<div class="details_product_tab_container">
  <div class="details_tab">
	<script src="${_contextpath}/javascript/SimpleTabs1.2.js" type="text/javascript"></script>
	<!-- tabs -->
	<div id="tab-block-1">
	  <c:set var="tabId" value="1" />
		
	  <c:if test="${siteConfig['DETAILS_LONG_DESC_LOCATION'].value != 'above' and model.product.longDesc != ''}">
		<h4 id="${tabId}"><fmt:message key="description" /></h4>
		<div>
		  <!-- start tab --> 
  		    <div class="details_long_desc" id="details_long_desc_id"><c:out value="${model.product.longDesc}" escapeXml="false" /></div>
		  <!-- end tab -->
		  <c:set var="tabId" value="${1+tabId}" />
		</div>
	  </c:if>
		
	  <%-- Fields Start--%>
	  <c:if test="${fn:length(model.product.productFields) > 0}">
	    <h4 id="${tabId}"><c:out value="${siteConfig['PRODUCT_FIELDS_TITLE'].value}"/></h4>
	    <div class="productFields" id="productFieldsId">
		  <!-- start tab --> 
		  <table class="details_fields">
		    <c:forEach items="${model.product.productFields}" var="productField" varStatus="row">
		      <tr>
		        <td class="details_field_name_row${row.index % 2}"><c:out value="${productField.name}" escapeXml="false" /> </td>
		        <c:choose>
				<c:when test="${productField.formatType != null and productField.formatType != '' and productField.fieldType == 'number'}">
				<c:catch var="catchFormatterException">
				<td class="details_field_value_row${row.index % 2}"><fmt:formatNumber value="${wj:trimAndSplit(productField.value, '|' , false, ',', true, true)}" pattern="${productField.formatType}" /></td>
				</c:catch>
				<c:if test = "${catchFormatterException != null}"><td class="details_field_value_row${row.index % 2}"><c:out value="${wj:trimAndSplit(productField.value, '|' , false, ',', true, true)}" escapeXml="false"/></td></c:if>
				</c:when>
				<c:otherwise>
					<td class="details_field_value_row${row.index % 2}"><c:out value="${wj:trimAndSplit(productField.value, '|' , false, ',', true, true)}" escapeXml="false"/></td>
				</c:otherwise>
				</c:choose>
		      </tr>
		    </c:forEach>
		  </table>
		  <!-- end tab -->
		  <c:set var="tabId" value="${1+tabId}" />
	    </div>
	  </c:if>
	  <%-- Fields End --%>
		
	  <%--Product Review List Start--%>
	  <c:if test="${gSiteConfig['gPRODUCT_REVIEW'] and siteConfig['PRODUCT_RATE'].value != 'true' and model.product.enableRate}">
		<h4 id="${tabId}"><fmt:message key="reviews" /></h4>
	    <!-- start tab --> 
		<div class="productReview">
		  <%@ include file="/WEB-INF/jsp/frontend/common/productReviewList.jsp" %>
		</div>
	    <!-- end tab -->
	  	<c:set var="tabId" value="${1+tabId}" />
	  </c:if>
	  <%--Product Review List End--%>
		
	  <!--  Product Tabs -->
	  <c:forEach items="${model.tabList}" var="tab" varStatus="status">
		<h4 id="${tab.tabNumber+tabId}" title="<c:out value="${tab.tabName}" escapeXml="false"/>"> <c:out value="${tab.tabName}" escapeXml="false" /></h4>
		<div>
		  <!-- start tab --> 
		    <c:out value="${tab.tabContent}" escapeXml="false" /> 
		  <!-- end tab -->
		</div>
	  </c:forEach>
	</div>
  </div>
</div>	

<c:set var="recommendedList_HTML">
<c:if test="${model.recommendedList != null}">
<div style="float:left;width:100%" id="recommandedWrapper">
<c:choose>
 <c:when test="${!empty product.recommendedListTitle }">
  <div class="recommended_list"><c:out value="${model.product.recommendedListTitle}"/></div>
 </c:when>
 <c:otherwise>
  <c:if test="${siteConfig['RECOMMENDED_LIST_TITLE'].value != ''}">
   <div class="recommended_list"><c:out value="${siteConfig['RECOMMENDED_LIST_TITLE'].value}"/></div>
  </c:if>
 </c:otherwise>
</c:choose>
<c:set var="numCul" value="${siteConfig['NUMBER_PRODUCT_PER_ROW'].value}" />
<c:forEach items="${model.recommendedList}" var="product" varStatus="status">

<c:set var="productLink">${_contextpath}/product.jhtm?id=${product.id}<c:if test="${model.product == null or siteConfig['RECOMMENDED_LIST_SHOW_BREADCRUMB'].value == 'true'}">&cid=<c:out value="${param.cid}"/></c:if></c:set>
<c:if test="${gSiteConfig['gMOD_REWRITE'] == '1' and not fn:contains(product.sku, '/')}">
  <c:set var="productLink">${_contextpath}/${siteConfig['MOD_REWRITE_PRODUCT'].value}/${product.encodedSku}/${product.encodedName}.html<c:if test="${model.product == null or siteConfig['RECOMMENDED_LIST_SHOW_BREADCRUMB'].value == 'true'}">?cid=<c:out value="${param.cid}"/></c:if></c:set>
</c:if>

<c:choose>
 <c:when test="${( model.product.recommendedListDisplay == 'quick') or (empty model.product.recommendedListDisplay and siteConfig['RECOMMENDED_LIST_DISPLAY'].value == 'quick')}">
    <%@ include file="/WEB-INF/jsp/frontend/quickmode/view1.jsp" %>
 </c:when>
 <c:when test="${( model.product.recommendedListDisplay == '2') or (empty model.product.recommendedListDisplay and siteConfig['RECOMMENDED_LIST_DISPLAY'].value == '2')}">
    <%@ include file="/WEB-INF/jsp/frontend/thumbnail/view2.jsp" %>
 </c:when>
 <c:when test="${( model.product.recommendedListDisplay == '3') or (empty model.product.recommendedListDisplay and siteConfig['RECOMMENDED_LIST_DISPLAY'].value == '3')}">
        <%@ include file="/WEB-INF/jsp/frontend/thumbnail/view3.jsp" %>
 </c:when>
 <c:when test="${( model.product.recommendedListDisplay == '4') or (empty model.product.recommendedListDisplay and siteConfig['RECOMMENDED_LIST_DISPLAY'].value == '4')}">
        <%@ include file="/WEB-INF/jsp/frontend/thumbnail/view4.jsp" %>
 </c:when>
 <c:when test="${( model.product.recommendedListDisplay == '5') or (empty model.product.recommendedListDisplay and siteConfig['RECOMMENDED_LIST_DISPLAY'].value == '5')}">
        <%@ include file="/WEB-INF/jsp/frontend/thumbnail/view5.jsp" %>
 </c:when>
 <c:when test="${( model.product.recommendedListDisplay == '6') or (empty model.product.recommendedListDisplay and siteConfig['RECOMMENDED_LIST_DISPLAY'].value == '6')}">
        <%@ include file="/WEB-INF/jsp/frontend/thumbnail/view6.jsp" %>
  </c:when>
 <c:when test="${( model.product.recommendedListDisplay == '7') or (empty model.product.recommendedListDisplay and siteConfig['RECOMMENDED_LIST_DISPLAY'].value == '7')}">
        <%@ include file="/WEB-INF/jsp/frontend/thumbnail/view7.jsp" %>
 </c:when>
 <c:when test="${( model.product.recommendedListDisplay == '8') or (empty model.product.recommendedListDisplay and siteConfig['RECOMMENDED_LIST_DISPLAY'].value == '8')}">
        <%@ include file="/WEB-INF/jsp/frontend/thumbnail/view8.jsp" %>
 </c:when>
 <c:when test="${( model.product.recommendedListDisplay == '9') or (empty model.product.recommendedListDisplay and siteConfig['RECOMMENDED_LIST_DISPLAY'].value == '9')}">
        <%@ include file="/WEB-INF/jsp/frontend/thumbnail/view9.jsp" %>
 </c:when>
 <c:when test="${( model.product.recommendedListDisplay == '10') or (empty model.product.recommendedListDisplay and siteConfig['RECOMMENDED_LIST_DISPLAY'].value == '10')}">
    <%@ include file="/WEB-INF/jsp/frontend/thumbnail/view10.jsp" %>
 </c:when>
 <c:when test="${( model.product.recommendedListDisplay == '11') or (empty model.product.recommendedListDisplay and siteConfig['RECOMMENDED_LIST_DISPLAY'].value == '11')}">
    <%@ include file="/WEB-INF/jsp/frontend/thumbnail/view11.jsp" %>
 </c:when>
 <c:when test="${( model.product.recommendedListDisplay == '12') or (empty model.product.recommendedListDisplay and siteConfig['RECOMMENDED_LIST_DISPLAY'].value == '12')}">
    <%@ include file="/WEB-INF/jsp/frontend/thumbnail/view12.jsp" %>
 </c:when>
 <c:otherwise>
        <%@ include file="/WEB-INF/jsp/frontend/thumbnail/view1.jsp" %>
 </c:otherwise>
</c:choose>
</c:forEach>
</div>
</c:if>
</c:set>

<%--Recommended List Start --%>
<c:out value="${recommendedList_HTML}" escapeXml="false" />
<%--Recommended List End --%>

</div>
<!-- Details Wrapper End -->

</c:if>
</tiles:putAttribute>
</tiles:insertDefinition>