<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="/WEB-INF/tlds/spring.tld" prefix="spring" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ taglib uri="http://www.springframework.org/security/tags" prefix="sec" %>

<tiles:insertDefinition name="${_template}" flush="true">
  <tiles:putAttribute name="content" type="string">

<script type="text/javascript" src="<c:url value="/FCKeditor/fckeditor.js"/>"></script>
<script type="text/javascript" src="javascript/mootools-1.2.5-core.js"></script>
	
<form:form commandName="form" >
<input type="hidden" id="send" value="true">
<table cellpadding="0" cellspacing="0" border="0" id="salesRepMassEmail" >
  <tr>
    <td>
      <c:if test="${message != null}">
		<div class="message"><fmt:message key="${message}"><fmt:param value="${form.numEmails}"/></fmt:message> </div>
	  </c:if>
	  <spring:hasBindErrors name="form">
		<div class="error"><fmt:message key='form.hasErrors'/></div>
	  </spring:hasBindErrors>
	  <c:if test="${pointError != null}">
		<div class="message"><fmt:message key="${pointError}"><fmt:param value="${form.numEmails}" /><fmt:param value="${massEmailPoint}" /></fmt:message></div>
	  </c:if>
    </td>
  </tr>
  <tr>
    <td></td>
	<td><c:out value="${form.numEmails}"/> (Number of Email to send)</td>
  </tr>
  <tr>
    <td>Html:</td>
    <td><form:checkbox id="html1" path="html" onchange="showHtmlEditor(this)"/>
		<c:forEach var="message" items="${messages}">
		<input type="hidden" id="htmlID${message.messageId}" value="<c:out value="${message.html}"/>" />
		</c:forEach>
	</td>
  </tr>
  <tr>
    <td><fmt:message key="from"/>:</td>
    <td><c:out value="${form.from}"/><form:errors path="from" cssClass="error" delimiter=", "/></td>
  </tr>
  <tr>
    <td><fmt:message key="subject"/>:</td>
    <td>
      <form:input id="subject" path="subject" size="50" maxlength="50" htmlEscape="true"/>
		  <c:forEach var="message" items="${messages}">
 		  <input type="text" disabled style="display:none;" id="subject${message.messageId}" name="subject" value="<c:out value="${message.subject}"/>" size="50" maxlength="50">
		  </c:forEach>
      <form:errors path="subject" cssClass="error"/>
    </td>
  </tr>
  <tr>
    <td><fmt:message key="message"/>:</td>
    <td>
      <form:textarea id="message" path="message" rows="15" cols="60" htmlEscape="true"/>
 	  <div name="htmlLink" class="htmlEditorLink" style="text-align:right;width:455px;display:none;" id="htmlEditor_link"><a href="#" onClick="loadEditor('message',-1)" class="htmleditor">- <fmt:message key="htmlEditor" /> -</a></div>
		 <c:forEach var="message" items="${messages}">
  			<textarea disabled style="display:none;" id="message${message.messageId}" name="message" rows="15" cols="60"><c:out value="${message.message}"/></textarea>
  		    <div name="htmlLink" class="htmlEditorLink" style="text-align:right;width:455px;display:none;" id="htmlEditor_link${message.messageId}"><a href="#" onClick="loadEditor('message${message.messageId}',${message.messageId})" class="htmleditor">- <fmt:message key="htmlEditor" /> -</a></div>
  		 </c:forEach>
         <form:errors path="message" cssClass="error"/>
         <div id="messageSelect">
      <select id="messageSelected" onChange="chooseMessage(this)">
        <option value="">choose a message</option>
	  <c:forEach var="message" items="${messages}">
	    <option value="${message.messageId}"><c:out value="${message.messageName}"/></option>
	  </c:forEach>
	  </select>
     </div>
    </td>
  </tr>
  <tr>
    <td>
    <div align="left" class="button">
	  <c:if test="${!scheduleDone}" >
	    <input type="submit" name="__sendnow" value="<fmt:message key="send" /> Now" onclick="return checkMessage(1)" />
	  </c:if>
	  <c:if test="${scheduleDone}" >
	    <input type="submit" name="__back" value="<fmt:message key="back" />" />
	  </c:if>
	</div>
    </td>
  </tr>
</table>
</form:form>	
  			  	

<script language="JavaScript"> 
<!--
function trimString(str){
    var returnVal = "";
    for(var i = 0; i < str.length; i++){
      if(str.charAt(i) != ' '){
        returnVal += str.charAt(i)
      }
    }
    return returnVal;
}
function chooseMessage(el) {
	var htmlEditorLinks = $$('#preLoadMessage div.htmlEditorLink');
	    htmlEditorLinks.each(function(el) {
		$(el).setStyles({'display': 'none'});
});
	var iFrames = $$('iframe');
		iFrames.each(function(el) {
		$(el).setStyles({'display': 'none'});
});
		
	messageList = document.getElementsByName('message');
	for(var i=0; i<messageList.length; i++) {
		messageList[i].disabled=true;
		messageList[i].style.display="none";
	}
   	subjectList = document.getElementsByName('subject');
	for(var i=0; i<subjectList.length; i++) {
		subjectList[i].disabled=true;
		subjectList[i].style.display="none";
	}
	htmlLinkList = document.getElementsByName('htmlLink');
	for(var i=0; i<htmlLinkList.length; i++) {
		htmlLinkList[i].disabled=true;
		htmlLinkList[i].style.display="none";
	}

   	document.getElementById('message'+el.value).disabled=false;
       document.getElementById('message'+el.value).style.display="block"; 
   	document.getElementById('subject'+el.value).disabled=false;
       document.getElementById('subject'+el.value).style.display="block";
       
    if (document.getElementById('htmlID'+el.value).value == 'true') {
    	document.getElementById('html1').checked = true;
    	document.getElementById('htmlEditor_link'+el.value).style.display="block";
    } else {
    	document.getElementById('html1').checked = false;
    }
                  
}		
function checkMessage(type) {
    if (document.getElementById('send').value == 'true') {
      var message = document.getElementById('message' + document.getElementById('messageSelected').value);
      var subject = document.getElementById('subject' + document.getElementById('messageSelected').value);
      if (trimString(subject.value) == "") {
        alert("Please put a subject");
        subject.focus();
    	return false;
      }
      if (trimString(message.value) == "") {
        alert("Please put a message");
        message.focus();
    	return false;
      }
    }
    if ( type == 1)
    {
      return confirm("Are you sure to send email(s) now?");
    } else {
      return confirm("Are you sure to send email(s) on the provided schedule time?");
    }
}
function loadEditor(el,id) {
	  var oFCKeditor = new FCKeditor(el);
	  oFCKeditor.BasePath = "<c:url value="/FCKeditor/"/>";
	  oFCKeditor.Config["CustomConfigurationsPath"] = '<c:url value="/admin/javascript/fckconfig.js"/>';
	  oFCKeditor.ToolbarSet = 'Html';
	  oFCKeditor.Width = 650;
	  oFCKeditor.Height = 400;
	  oFCKeditor.ReplaceTextarea();
	  document.getElementById('htmlEditor_link' + id).style.display="none";
}
function showHtmlEditor(el) {
	  if ( document.getElementById('messageSelected').value == '') {
	    if ( el.checked) {
	    	document.getElementById('htmlEditor_link').style.display="block";
	    } else {
	    	document.getElementById('htmlEditor_link').style.display="none";
	    }
	  }
}
//-->
</script>
   
  </tiles:putAttribute>    
</tiles:insertDefinition>