<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<c:if  test="${model.assignedProduct != null}">
<link href="assets/stylesheet.css" rel="stylesheet" type="text/css">

  <div class="optionWrapper">
  
  <div class="optionTitleBox">
    4.<fmt:message key="f_product12_step4"/>
  </div>

  <div class="optionMultiValuesBox">
  <!-- Image -->
  
  <div style="display: none;">
	<c:set var="size"/>
    <c:forEach items="${model.selectedOptions}" var="selectedOption">
      <c:if test="${fn:contains(selectedOption.key, 'size') or fn:contains(selectedOption.key, 'Size')}">
        <c:set var="size" value="${selectedOption.value}"/>	      
      </c:if>
    </c:forEach>
  </div>

  <div class="reviewBoxImage">
    <div class="reviewBoxTitle">Product Images</div> 
    <div class="details_image_box" style="width: 330px;">
		<c:forEach items="${model.assignedProduct.images}" var="image" varStatus="status">
		  <c:if test="${status.first}">
		    <a  style="cursor: pointer;" onclick="zoomIn('${model.assignedProduct.id}');" title="View Larger Images"><img id="_image" src="<c:if test="${!image.absolute}">${_contextpath}/assets/Image/Product/detailsbig/</c:if><c:out value="${image.imageUrl}"/>" border="0" style="<c:if test="${siteConfig['DETAILS_IMAGE_WIDTH'].value != '' }">width: <c:out value="${siteConfig['DETAILS_IMAGE_WIDTH'].value}"/>px</c:if>" alt="<c:out value="${model.assignedProduct.name}"/><c:if test="${model.assignedProduct.shortDesc != ''}"> - <c:out value="${model.assignedProduct.shortDesc}"/></c:if>"/></a>
		  </c:if>
		</c:forEach>
	    <div>
	    <c:forEach items="${model.assignedProduct.images}" var="image" varStatus="status">
		  <a href="#" class="details_thumbnail_anchor" onClick="return updateImage('${image.absolute}','${image.imageUrl}');">
		    <img src="<c:if test="${!image.absolute}">${_contextpath}/assets/Image/Product/detailsbig/</c:if><c:out value="${image.imageUrl}"/>" class="details_thumbnail" alt="<c:out value="${model.assignedProduct.name}"/><c:if test="${model.assignedProduct.shortDesc != ''}"> - <c:out value="${model.assignedProduct.shortDesc}"/></c:if>">
	      </a>
		</c:forEach> 
		<span class="moreImageWrapper">
		  <a onclick="zoomIn('${model.assignedProduct.id}');" title="View Larger Images"><span>View Larger Images</span></a>
		</span>
	  </div>
	</div>        
	
	<div class="pdWrapper">
	  <div class="pdTitle">Product Description</div>
	  <div class="pdItem">
        <div class="pdName">Diploma Size:</div> 
        <div class="pdValue"><c:out value="${size}"/></div> 
	  </div>

      <div style="display: none;">
	  <c:set var="outSideWidth"/>
	  <c:set var="outSideHeight"/>
	  <c:choose>
	    <c:when test="${customWidthOptValue != null and customWidthOptValue != '' and customHeightOptValue != null and customHeightOptValue != ''}">
	      <c:set var="outSideWidth" value="${customWidthOptValue + model.assignedProduct.field15}"/>
	      <c:set var="outSideHeight" value="${customHeightOptValue + model.assignedProduct.field15}"/>
	    </c:when>
	    <c:otherwise>
	      <c:set var="outSideWidth" value="${model.assignedProduct.field4 + model.assignedProduct.field15}"/>
	      <c:set var="outSideHeight" value="${model.assignedProduct.field5 + model.assignedProduct.field15}"/>
	    </c:otherwise>
	  </c:choose>
      </div>
	  
	  <div class="pdItem">
        <div class="pdName">Frame Outside Dimension:</div> 
        <div class="pdValue">
          <fmt:formatNumber value="${outSideWidth}" pattern="#,##0.00#" maxFractionDigits="2" />" X <fmt:formatNumber value="${outSideHeight}" pattern="#,##0.00#" maxFractionDigits="2" />"
        </div>
      </div>

	  <div class="pdItem">
        <div class="pdName">Moulding:</div> 
        <div class="pdValue"><c:out value="${model.assignedProduct.field6}"/></div> 
	  </div>

	  <div class="pdItem">
        <div class="pdName">Top Mat:</div> 
        <div class="pdValue"><c:out value="${model.assignedProduct.field7}"/></div> 
	  </div>

	  <div class="pdItem">
        <div class="pdName">Fillet:</div> 
        <div class="pdValue"><c:out value="${model.assignedProduct.field12}"/></div> 
	  </div>
	
	  <div class="pdItem">
        <div class="pdName">Glass:</div> 
        <div class="pdValue"><c:out value="${model.assignedProduct.field8}"/></div> 
	  </div>
	</div>
	
  </div>
  <!-- Description -->
  
  <div class="reviewBoxSummary">
    <div class="reviewBoxTitle">Order Summary</div> 
    <br/>

    <div class="reviewBoxItem">
      <div class="reviewBoxName" style="font-weight: bold;"><c:out value="${model.assignedProduct.name}"/></div> 
      <div class="reviewBoxValue"></div> 
    </div>
    <%-- 
    <div class="reviewBoxItem">
      <div class="reviewBoxName">(<c:out value="${selectedStyle}"/> <c:out value="${selectedDesign}"/>)</div> 
      <div class="reviewBoxValue"></div> 
    </div>

    <div class="reviewBoxItem">
      <div class="reviewBoxName">Diploma Frame</div> 
      <div class="reviewBoxValue"></div> 
    </div>
	
    <div class="reviewBoxItem">
      <div class="reviewBoxName"><c:out value="${size}"/> diploma</div> 
      <div class="reviewBoxValue"></div> 
    </div>
    --%>
    <c:if test="${model.salesTag.discount  != null}">
    <c:set var="discount" value="0"/>
    <c:if test="${model.salesTag != null}">
      <c:choose>
        <c:when test="${model.salesTag.percent}">
          <c:set var="discount" value="${model.price * model.salesTag.discount / 100}"/>
        </c:when>
        <c:otherwise>
          <c:choose>
            <c:when test="${model.salesTag.discount > model.price}">
              <c:set var="discount" value="${model.price}"/>
            </c:when>
            <c:otherwise>
              <c:set var="discount" value="${model.salesTag.discount}"/>
            </c:otherwise>
          </c:choose>
        </c:otherwise>
      </c:choose>
    </c:if>
    </c:if>
    
    <div style="height: 25px; clear: both;"> </div>
    <div class="reviewBoxItem">
      <div class="reviewBoxName">Retail Price</div> 
      <div class="reviewBoxValue"  <c:if test="${discount > 0}"> style="text-decoration: line-through;" </c:if>>
           $<fmt:formatNumber value="${model.price}" pattern="#,##0.00#" maxFractionDigits="${siteConfig['MAX_FRACTION_DIGIT'].value}" />
      </div> 
    </div>
    
    <div class="reviewBoxItem">
      <div class="reviewBoxName" style="color: RED;">FrameStoreDirect Price</div> 
      <div class="reviewBoxValue" style="color: RED;">
        <c:choose>
          <c:when test="${model.discountedPrice == null}">$<fmt:formatNumber value="${model.price}" pattern="#,##0.00#" maxFractionDigits="${siteConfig['MAX_FRACTION_DIGIT'].value}" /></c:when>
          <c:otherwise>$<fmt:formatNumber value="${model.discountedPrice}" pattern="#,##0.00#" maxFractionDigits="${siteConfig['MAX_FRACTION_DIGIT'].value}" /></c:otherwise>
        </c:choose>
      </div> 
    </div>  

    <div style="height: 25px; clear: both;"> </div>
    <div class="reviewBoxItem">
      <div class="reviewBoxName" style="font-weight: bold;">Shipping </div> 
      <div class="reviewBoxValue"></div> 
    </div>

    <div class="reviewBoxItem">
      <div class="reviewBoxName">Standard US</div> 
      <div class="reviewBoxValue" style="color: RED;">FREE</div> 
    </div>  

    <div style="clear: both;"></div>
    <div class="cartCompareWrapper">
    <div id="customAddToCart">
      <input type="image" border="0" name="_addtocart" id="addToCart" src="${_contextpath}/assets/Image/Layout/button_continue.png"/>
    </div>
    <div class="addToCompare">
      <c:choose>
        <c:when test="${fn:length(model.cartItemsComparisonList) >=  model.maxComparisons}"></c:when>
        <c:when test="${model.isItemAddedToList == true}">
            <input type="image" border="0" class="compare" src="${_contextpath}/assets/Image/Layout/button_addedToCompare.png" onclick="return false;"/>
  		</c:when>
        <c:otherwise>
          <div id="product${model.assignedProduct.id}">
            <input type="image" border="0" class="compare" src="${_contextpath}/assets/Image/Layout/button_addToCompare.png" onclick="compareProduct('${model.assignedProduct.id}'); return false;"/>
  		  </div>
        </c:otherwise>
      </c:choose>
	  <input type="hidden" id="maxComparisons" value="${model.maxComparisons}">
	  <input type="hidden" id="currentComparison" value="${fn:length(model.cartItemsComparisonList)}">
    </div>
    <div style="clear: both;"></div>
    <c:set var="display" value="none"/>
    <c:if test="${fn:length(model.cartItemsComparisonList) > 0}">
      <c:set var="display" value="block"></c:set>
    </c:if>
    <div class="viewComparison" id="viewComparison" style="float:right; display: ${display}" >
	  <a href="${_contextpath}/comparison.jhtm?optionComparison=true" rel="width:970,height:750" class="mbCategory"><img alt="" src="${_contextpath}/assets/Image/Layout/button_viewComparison.png"></a>
    </div>
    
    </div>
    
    <div style="font-size: 14px;margin-top:70px;color: red;">
      Note: Please scroll up to change your selection
    </div> 
    
    <div id="verisignSeal">
	<embed width="115" height="82" align="" allowscriptaccess="always" pluginspage="https://www.macromedia.com/go/getflashplayer" type="application/x-shockwave-flash" name="s_m" swliveconnect="FALSE" wmode="transparent" quality="best" menu="false" loop="false" src="https://seal.verisign.com/getseal?at=1&sealid=1&dn=www.framestoredirect.com&lang=en">
    </div> 
    

  </div>
  
  </div>
  <div style="clear: both;"></div>
  
<!-- Assign Options selection to assigned product so that assigned product is sent to cart and order -->
<input type="hidden" name="product.id" value="${model.assignedProduct.id}"/>
<input type="hidden" name="quantity_${model.assignedProduct.id}" id="quantity_${model.assignedProduct.id}" value="1">
      
<c:if test="${model.optionList != null and not empty model.optionList}">
<c:forEach items="${model.optionList}" var="productOption">

  <div style="display:none;">
  <c:set var="optName" value="${productOption.optionCode}-option_values_${model.productId}_${productOption.index}"></c:set>
  <c:set var="optValue"></c:set>
  <c:forEach var="entry" items="${model.originalIndexSelection}">
     <c:if test="${optName == entry.key}">
       <c:set var="optValue" value="${entry.value}"></c:set>  
     </c:if>
  </c:forEach>
  </div>

  <c:if test="${!empty productOption.values}">
    <input type="hidden" name="<c:out value="${productOption.optionCode}"/>-option_${model.assignedProduct.id}" value="<c:out value="${productOption.index}"/>"/>
    <input type="hidden" name="optionCode_${model.assignedProduct.id}" value="<c:out value="${productOption.optionCode}"/>"/>	
    <c:forEach items="${productOption.values}" var="prodOptValue" varStatus="povStatus">
       <c:if test="${prodOptValue.index == optValue}">
       <input type="hidden" 
              name="<c:out value="${productOption.optionCode}"/>-option_values_${model.assignedProduct.id}_<c:out value="${productOption.index}"/>" 
              value="<c:out value="${prodOptValue.index}"/>">
	   </c:if>
    </c:forEach>   
  </c:if>  

<c:if test="${customSizeOptValue != null and customSizeOptValue != ''}">
  <input type="hidden" name="<c:out value="${model.optionList[1].optionCode}"/>-option_values_cus_with_index_${model.assignedProduct.id}_${model.optionList[1].index}" value="${customSizeOptIndex}_${customSizeOptValue}" id="customSize"/>
</c:if>
</c:forEach>

</c:if>      
</div>
</c:if>