<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ taglib uri="/WEB-INF/tlds/custom.tld" prefix="wj" %>

<tiles:insertDefinition name="${_template}" flush="true">
	<tiles:putAttribute name="content" type="string">
<link href="${_contextpath}/assets/stylesheet.css" rel="stylesheet" type="text/css">
<link href="${_contextpath}/assets/SmoothGallery/multiboxASI.css" rel="stylesheet"  type="text/css"/>
<script type="text/javascript">
var box2 = {};
window.addEvent('domready',function() {
	  box2 = new multiBox('mbCategory', {waitDuration: 5,showControls: false,overlay: new overlay()});
	  var tabs1 = new SimpleTabs($('tab-block-1'), { selector: 'h4'});
	  $('_addtocart').addEvent('click',function(){
		  if($('childProducts').value == '') {
			  alert('Please Select size');
			  return false;
		  }	
		  $('childProducts').value == '';
	  });
		
});
function zoomIn(productId) {
	window.open('${_contextpath}/productImage.jhtm?id='+productId+'&layout=2&imgUrl=' + document.getElementById("_image").src,'name','width=800,height=600,resizable=yes,scrollbars=1');
}
function updateImage(absoluteImage, imageUrl){
    if(absoluteImage == 'false') {
		$('_image').setProperty('src','${_contextpath}/assets/Image/Product/detailsbig/'+imageUrl);
	} else {
		$('_image').setProperty('src',imageUrl);
	}
	return false;
}
//comparison
function updateProduct(sku){
	if(sku != null && sku != '') {
		var requestHTMLData = new Request.HTML ({
	    	url: "${_contextpath}/ajaxProduct.jhtm?sku="+sku,
			onSuccess: function(response){ 
			},
			update : $('priceCartWrapperId')
	    }).send();
	} else {
		$('childProducts').value == '';
	}
}
</script>
<div class="detail18">

<%-- bread crumbs --%>
<c:import url="/WEB-INF/jsp/frontend/layout/${_template}/breadcrumbs.jsp" />
<c:import url="/WEB-INF/jsp/frontend/etilize/breadcrumbs.jsp" />

  <!-- Recommended Products Start -->
  <c:if test="${model.recommendedList != null and !empty model.recommendedList}">
  
  <div class="rcmdProductsWrapper">
    <div class="rcmdProductsTitle"> <c:out value="${siteConfig['RECOMMENDED_LIST_TITLE'].value}"></c:out>  </div>
    <div class="rcmdProducts">
      <c:forEach items="${model.recommendedList}" var="rcmdProduct" varStatus="rcmdStatus">
        <c:set var="productLink">${_contextpath}/product.jhtm?id=${rcmdProduct.id}</c:set>
		<c:if test="${gSiteConfig['gMOD_REWRITE'] == '1' and not fn:contains(rcmdProduct.sku, '/')}">
		  <c:set var="productLink">${_contextpath}/${siteConfig['MOD_REWRITE_PRODUCT'].value}/${rcmdProduct.encodedSku}/${rcmdProduct.encodedName}.html</c:set>
		</c:if>	
        <span class="rcmdProduct <c:if test="${rcmdProduct.id ==  model.product.id}">selected</c:if>" id="rcmdProduct${rcmdStatus.index + 1}">
          <c:forEach items="${rcmdProduct.images}" var="image" varStatus="status">
		    <c:if test="${status.first}">
		    <a href="${productLink}">
		      <img src="<c:if test="${!image.absolute}">${_contextpath}/assets/Image/Product/detailsbig/</c:if><c:out value="${image.imageUrl}"/>" class="details18Thumbnail" alt="<c:out value="${rcmdProduct.name}"/><c:if test="${rcmdProduct.shortDesc != ''}"> - <c:out value="${rcmdProduct.shortDesc}"/></c:if>">
	        </a>
	        </c:if>
		  </c:forEach>
	    </span> 
      </c:forEach>
    </div>
  </div>
  </c:if>
  
  <!-- Recommended Products End -->
  
  <!-- Image and Content Start -->
  <div class="imageContentWrapper">
    <div class="imageMediaWrapper" style="float: left;">
      <!-- Image Start -->
	  <div class="detailsImageBox" style="width: 330px;">
		<c:forEach items="${model.product.images}" var="image" varStatus="status">
		  <c:if test="${status.first}">
			<a  style="cursor: pointer;" onclick="zoomIn('${model.product.id}');" title="View Larger Images"><img id="_image" src="<c:if test="${!image.absolute}">${_contextpath}/assets/Image/Product/detailsbig/</c:if><c:out value="${image.imageUrl}"/>" border="0" style="<c:if test="${siteConfig['DETAILS_IMAGE_WIDTH'].value != '' }">width: <c:out value="${siteConfig['DETAILS_IMAGE_WIDTH'].value}"/>px</c:if>" alt="<c:out value="${model.product.name}"/><c:if test="${model.product.shortDesc != ''}"> - <c:out value="${model.product.shortDesc}"/></c:if>"/></a>
		  </c:if>
		</c:forEach>
		<div>
		  <c:forEach items="${model.product.images}" var="image" varStatus="status">
		    <a href="#" class="detailsThumbnailAnchor" onClick="return updateImage('${image.absolute}','${image.imageUrl}');">
			  <img  src="<c:if test="${!image.absolute}">${_contextpath}/assets/Image/Product/detailsbig/</c:if><c:out value="${image.imageUrl}"/>" class="details_thumbnail" alt="<c:out value="${model.product.name}"/><c:if test="${model.product.shortDesc != ''}"> - <c:out value="${model.product.shortDesc}"/></c:if>">
		    </a>
		  </c:forEach> 
		  <%--
		  <span class="moreImageWrapper">
			<a onclick="zoomIn('${model.product.id}');" title="View Larger Images"><span>View Larger Images</span></a>
		  </span> --%>
		</div>
	  </div>
	  <!-- Image End -->
	  
	  <!-- Socail Media Logos Start -->
	  <div id="detailsSocialMedia">
		
		<c:set var="productLink">${siteConfig['SITE_URL'].value}product.jhtm?id=${model.product.id}<c:if test="${model.product == null or siteConfig['RECOMMENDED_LIST_SHOW_BREADCRUMB'].value == 'true'}">&cid=<c:out value="${param.cid}"/></c:if></c:set>
		<c:if test="${gSiteConfig['gMOD_REWRITE'] == '1' and not fn:contains(model.product.sku, '/')}">
  		  <c:set var="productLink">${siteConfig['SITE_URL'].value}${siteConfig['MOD_REWRITE_PRODUCT'].value}/${model.product.encodedSku}/${model.product.encodedName}.html<c:if test="${model.product == null or siteConfig['RECOMMENDED_LIST_SHOW_BREADCRUMB'].value == 'true'}">?cid=<c:out value="${param.cid}"/></c:if></c:set>
		</c:if>	
		
		<!-- Google +1 Starts-->
		<a href="https://plusone.google.com/_/+1/confirm?hl=en&url=${productLink}"><img src="http://www.framestoredirect.com/assets/Image/Layout/ico_google.png"/>
</a>
		<!-- Google +1 Ends -->
		
		<a href="http://pinterest.com" target="_blank"><img src="/assets/Image/Layout/ico_pin_it.png" /></a>
	
		<!-- Facebook Starts-->
		<a href="http://www.facebook.com/share.php?u=${productLink}" target="_blank"><img src="/assets/Image/Layout/ico_facebook.png"  /></a>

		<!-- Facebook Ends -->
		
		<!-- Tweeter Starts-->
		<a href="https://twitter.com/share" class="twitter-share-button" data-via="FrameStoreDirec">Tweet</a>
		<script>!function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0];if(!d.getElementById(id)){js=d.createElement(s);js.id=id;js.src="//platform.twitter.com/widgets.js";fjs.parentNode.insertBefore(js,fjs);}}(document,"script","twitter-wjs");</script>
		<!-- Tweeter Ends -->
	  </div>        
	  <!-- Socail Media Logos End -->        
	</div>
    
    
    <div class="contentWrapper" style="float: left;">
      <div class="productName"><c:out value="${model.product.name}"/></div> 
      <div class="priceCartWrapper" id="priceCartWrapperId">
        
        <div class="priceWrapper">
          <c:set var="discount" value="0"/>
	      <c:if test="${model.product.salesTag != null}">
	        <c:choose>
	          <c:when test="${model.product.salesTag.percent}">
	            <c:set var="discount" value="${model.product.price1 * model.product.salesTag.discount / 100}"/>
	          </c:when>
	          <c:otherwise>
	            <c:choose>
	              <c:when test="${model.product.salesTag.discount > model.product.price1}">
	                <c:set var="discount" value="${model.product.price1}"/>
	              </c:when>
	              <c:otherwise>
	                <c:set var="discount" value="${model.product.salesTag.discount}"/>
	              </c:otherwise>
	            </c:choose>
	          </c:otherwise>
	        </c:choose>
	      </c:if>
          <div class="prItem">
	        <div class="prName" >Retail Price:</div> 
	        <div class="prValue" style="float: left; <c:if test="${discount > 0}">text-decoration: line-through;</c:if>">
	          <c:if test="${model.product.price1 != null}">
	            $<fmt:formatNumber value="${model.product.price1}" pattern="#,##0.00#" maxFractionDigits="${siteConfig['MAX_FRACTION_DIGIT'].value}" />
		      </c:if>
	        </div>
		    <div style="clear: both;"></div> 
		  </div>
        
          <c:if test="${discount > 0}">
          <div class="prItem">
	        <div class="prName" id="discountTitle">FrameStoreDirect Price:</div> 
	        <div class="prValue" id="discountPrice">
	          $<fmt:formatNumber value="${model.product.price1 - discount}" pattern="#,##0.00#" maxFractionDigits="${siteConfig['MAX_FRACTION_DIGIT'].value}" />
		    </div>
		    <div style="clear: both;"></div> 
		  </div>
          </c:if>
          
          <div class="prItem">
	        <div class="prName" id="couponMessage">Price Before any Applicable Discounts</div>
	       <div style="clear: both;"></div> 
		  </div>
          <c:if test="${gSiteConfig['gPRODUCT_REVIEW'] and siteConfig['PRODUCT_RATE'].value != 'true' and model.product.enableRate}">
		    <div class="prItem">
	          <div>
	            <c:set value="${model.product}" var="product"></c:set>
		    	<%@ include file="/WEB-INF/jsp/frontend/common/quickProductReview.jsp" %>
		       </div>
		    </div>
		    <div style="clear: both;"></div> 
		  </c:if>
	    </div>
        
        <div class="cartWrapper">
          <div class="sizeOption">
	        <select name="childProducts" onchange="updateProduct(this.value)" id="childProducts">
			  <option value="">Select Size</option>
			  <c:forEach items="${model.childProducts}" var="childProduct" varStatus="povStatus">
			    <option value="${childProduct.sku}" <c:if test="${childProduct.sku == model.product.sku}">selected="selected"</c:if>  ><c:out value="${childProduct.field3}"></c:out></option>
			  </c:forEach>
			</select>
	      </div>
          <div id="addToCart">
          <form action="${_contextpath}/addToCart.jhtm" method="post">
		    <input type="hidden" name="optionNVPair" class="addComparison" value="Size_value_${fn:escapeXml(model.product.field3)}"/>
		    <input type="hidden" name="optionNVPair" class="addComparison" value="Style_value_${fn:escapeXml(model.product.field9)}"/>
		    <input type="hidden" name="optionNVPair" value="Design_value_${model.product.field10}"/>
		    <input type="hidden" name="optionNVPair" class="addComparison" value="Design Package_value_${fn:escapeXml(model.product.field10)}"/>
		    <input type="hidden" name="optionNVPair" class="addComparison" value="Frame Selected_value_${fn:escapeXml(model.product.name)}"/>
		    <input type="hidden" name="product.id" value="${model.product.id}"/>
		    <input type="hidden" name="quantity_${model.product.id}" id="quantity_${model.product.id}" value="1">
	        <input type="image" border="0" name="_addtocart" id="_addtocart" class="_addtocart" src="${_contextpath}/assets/Image/Layout/btn_add_to_cart.png" />
          </form>
          </div>
          
          <div id="shippingMessage">
            Qualifies for
            <div id="freeShipping">
             	FREE Shipping
            </div>
          </div>
        </div>      
          <div style="clear: both;"></div> 
	  </div>
    
      <div class="productNameField">
        <c:out value="${model.product.field10}"/> - <c:out value="${model.product.field9}"/>
      </div> 
      
      <div class="productNameSdesc">
        <c:out value="${model.product.shortDesc}"/>
      </div> 
      
      
      
      
      
      <c:if test="${gSiteConfig['gTAB'] > 0 and !empty model.tabList}">
	  <div class="details_product_tab_container">
		<div class="details_tab">
		  <script src="${_contextpath}/javascript/SimpleTabs1.2.js" type="text/javascript"></script>
		  <!-- tabs -->
		  <div id="tab-block-1">
			<c:forEach items="${model.tabList}" var="tab" varStatus="status">
			  <h4 id="${tab.tabNumber}" title="<c:out value="${tab.tabName}" escapeXml="false"/>"> <c:out value="${tab.tabName}" escapeXml="false" /></h4>
			  <div>
			    <!-- start tab --> 
				<c:choose>
				  <c:when test="${tab.tabNumber == 1}">
					<c:choose>
					  <c:when test="${tab.tabContent != null and fn:trim(tab.tabContent) != ''}">
						<c:out value="${tab.tabContent}" escapeXml="false" /> 
					  </c:when>
					  <c:otherwise>
						<c:forEach items="${model.product.productFields}" var="productField" varStatus="row">
						  <c:if test="${fn:contains(productField.name, 'Moulding') or fn:contains(productField.name, 'Mat') or fn:contains(productField.name, 'Fillet') or fn:contains(productField.name, 'Glass')}">
						    <div id="productFieldDesc">
							  <div id="productFieldName">
							  	<c:out value="${productField.name}"/> :  
							  </div>
							  <div id="productFieldValue">
						    	<c:choose>
									<c:when test="${productField.formatType != null and productField.formatType != '' and productField.fieldType == 'number' }">
										<fmt:formatNumber value="${wj:trimAndSplit(productField.value, '|' , false, ',', true, true)}" pattern="${productField.formatType }" />
									</c:when>
									<c:otherwise>
										<c:out value="${wj:trimAndSplit(productField.value, '|' , false, ',', true, true)}" escapeXml="false"/>
									</c:otherwise>
								</c:choose>
						   	  </div>
					   	    </div>
					    	<br/>
						  </c:if>
						</c:forEach>
					  </c:otherwise>
					</c:choose>	
				  </c:when>
				  <c:when test="${tab.tabNumber == 2}">
				    <c:out value="${model.product.longDesc}" escapeXml="false"/>
				  </c:when>
				  <c:otherwise>
					<c:out value="${tab.tabContent}" escapeXml="false" /> 
				  </c:otherwise>
				</c:choose>
				<!-- end tab -->
			  </div>
			 </c:forEach>
			</div>
		  </div>
		</div>	
		</c:if>
    
<%--Product Review List --%>
<c:if test="${gSiteConfig['gPRODUCT_REVIEW'] and siteConfig['PRODUCT_RATE'].value != 'true' and model.product.enableRate}">
<div class="productReview">
<%@ include file="/WEB-INF/jsp/frontend/common/productReviewList.jsp" %>
</div>
</c:if>
<%--Product Review List --%>
    
    
    
    </div>
    
    <div style="clear: both; display: none;">
    </div>
  </div>
  <!-- Image and Content End -->
  
  <div style="clear: both;"></div>
  
<!-- Assign Options selection to assigned product so that assigned product is sent to cart and order -->
<input type="hidden" name="product.id" value="${model.product.id}"/>
<input type="hidden" name="quantity_${model.product.id}" id="quantity_${model.product.id}" value="1">
      
<c:if test="${model.optionList != null and not empty model.optionList}">
<c:forEach items="${model.optionList}" var="productOption">

  <div style="display:none;">
  <c:set var="optName" value="${productOption.optionCode}-option_values_${model.productId}_${productOption.index}"></c:set>
  <c:set var="optValue"></c:set>
  <c:forEach var="entry" items="${model.originalIndexSelection}">
     <c:if test="${optName == entry.key}">
       <c:set var="optValue" value="${entry.value}"></c:set>  
     </c:if>
  </c:forEach>
  </div>

  <c:if test="${!empty productOption.values}">
    <input type="hidden" name="<c:out value="${productOption.optionCode}"/>-option_${model.product.id}" value="<c:out value="${productOption.index}"/>"/>
    <input type="hidden" name="optionCode_${model.product.id}" value="<c:out value="${productOption.optionCode}"/>"/>	
    <c:forEach items="${productOption.values}" var="prodOptValue" varStatus="povStatus">
       <c:if test="${prodOptValue.index == optValue}">
       <input type="hidden" 
              name="<c:out value="${productOption.optionCode}"/>-option_values_${model.product.id}_<c:out value="${productOption.index}"/>" 
              value="<c:out value="${prodOptValue.index}"/>">
	   </c:if>
    </c:forEach>   
  </c:if>  

<c:if test="${customSizeOptValue != null and customSizeOptValue != ''}">
  <input type="hidden" name="<c:out value="${model.optionList[1].optionCode}"/>-option_values_cus_with_index_${model.product.id}_${model.optionList[1].index}" value="${customSizeOptIndex}_${customSizeOptValue}" id="customSize"/>
</c:if>
</c:forEach>

</c:if>  
</div>

	</tiles:putAttribute>
</tiles:insertDefinition>