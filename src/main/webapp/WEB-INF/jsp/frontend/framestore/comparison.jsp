<link href="${_contextpath}/assets/stylesheet.css" rel="stylesheet" type="text/css">
<body style="background: white;">
<div id="newOption">
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<c:if test="${gSiteConfig['gCOMPARISON']}">

<div class="comparisonTitle"><c:out value="${siteConfig['COMPARISON_TITLE'].value}"/></div>

<c:if test="${model.message != null}">
  <div class="message"><fmt:message key="${model.message}"><fmt:param value="${siteConfig['COMPARISON_MAX'].value}"/></fmt:message></div>
</c:if>

<c:if test="${fn:length(model.cartItems) == 0}">
  <div class="message"><fmt:message key="comparison.empty" /></div>
</c:if>

<script src="javascript/mootools-1.2.5-core.js" type="text/javascript"></script>
<script src="javascript/mootools-1.2.5.1-more.js" type="text/javascript"></script>
 
<c:if test="${fn:length(model.cartItems) > 0}">
<script language="JavaScript">
<!--
function deleteSelected() {
	var isIdSelected;
	var urlAppend='__delete=true';
	$$('input.deleteSelected').each(function(input){
		if(input.checked) {
	  		urlAppend = urlAppend + '&__selected_id='+input.value;
			if(!isIdSelected){
				isIdSelected = true;
			}
		}
	});
	if(isIdSelected) {
		var requestHTMLData = new Request.HTML ({
  			url: "${_contextpath}/comparison.jhtm?optionComparison=true?&"+urlAppend,
  			update: $('newOption'),
  			onSuccess: function(response){ }
  		}).send();
  	} else {
  	    alert("Please select product(s) to remove.");       
  	    return false;
  	}
}
function checkForm(index) {
	$('addToCartForm'+index).send();
	setTimeout(function() {parent.box2.close();},2250);
	setTimeout(function() {parent.location = "${_contextpath}/viewCart.jhtm";},2250);
	//parent.box2.close();
	//parent.location = "${_contextpath}/viewCart.jhtm";
}
//-->
</script>

<table width="90%" align="center" cellspacing="5" cellpadding="0" border="0">
  <tr>
	<td>
<table border="0" cellpadding="8" cellspacing="0" width="100%" class="comparison"> 
  <tr class="comparisonRow">
    <td class="comparisonHdr">Image</td>
	<c:forEach items="${model.cartItems}" var="cartItem" varStatus="status">
    <td class="comparisonCol${status.index % 2}">
      <c:if test="${cartItem.product.thumbnail != null}">
	    <img class="compareImage" style="max-width: 130px; max-height: 130px;" border="0" <c:if test="${!cartItem.product.thumbnail.absolute}"> src="${_contextpath}/assets/Image/Product/thumb/${cartItem.product.thumbnail.imageUrl}" </c:if> <c:if test="${cartItem.product.thumbnail.absolute}"> src="<c:url value="${cartItem.product.thumbnail.imageUrl}"/>" </c:if> />
	  </c:if>
    </td>
	</c:forEach> 
  </tr>
  <tr class="comparisonRow" bordercolor="red;" style="border: 10px solid;">
    <td class="comparisonHdr">Name</td>
	<c:forEach items="${model.cartItems}" var="cartItem" varStatus="status">
    <td class="comparisonCol${status.index % 2}"><a href="product.jhtm?id=${cartItem.product.id}" 
	  			class="comparisonNameLink"><c:out value="${cartItem.product.name}" /></a></td>
	</c:forEach> 
  </tr>
  
  <tr class="comparisonRow">
    <td class="comparisonHdr">Price</td>
	<c:forEach items="${model.cartItems}" var="cartItem" varStatus="status">
    <td class="comparisonCol${status.index % 2}">
  	  <c:set var="discount" value="0"/>
      <c:if test="${cartItem.product.salesTag != null}">
      <div style="display: none;">
  	  <c:choose>
        <c:when test="${cartItem.product.salesTag.percent}">
          <c:set var="discount" value="${cartItem.product.priceTable1 * cartItem.product.salesTag.discount / 100}"/>
        </c:when>
        <c:otherwise>
          <c:choose>
            <c:when test="${cartItem.product.salesTag.discount > cartItem.product.priceTable1}">
              <c:set var="discount" value="${cartItem.product.priceTable1}"/>
            </c:when>
            <c:otherwise>
              <c:set var="discount" value="${cartItem.product.salesTag.discount}"/>
            </c:otherwise>
          </c:choose>
        </c:otherwise>
      </c:choose>
      </div>
  	  </c:if>
  	  <div class="comparisonPrice">
	  <fmt:message key="${siteConfig['CURRENCY'].value}" /><fmt:formatNumber value="${cartItem.product.priceTable1 - discount}" pattern="#,##0.00" />
	  </div>
    </td>
	</c:forEach>   
  </tr>
  <c:forEach items="${model.productFields}" var="productField">
  <c:if test="${productField.comparisonField}">
  <tr class="comparisonRow">
  	<td class="comparisonHdr"><c:out value="${productField.name}" escapeXml="false" /></td>
    <c:set var="status" value="0"/>
	<c:forEach items="${model.cartItems}" var="cartItem" varStatus="test">
	  <c:forEach items="${cartItem.product.productFields}" var="productField2" varStatus="test2"> 
	    <c:if test="${productField2.comparisonField and (productField.id == productField2.id)}">
          <td class="comparisonCol${status % 2}"><c:out value="${productField2.value}" escapeXml="false" />&nbsp;</td>
          <c:set var="status" value="${status+1}"/>
        </c:if>
      </c:forEach>
	</c:forEach> 
  </tr>
  </c:if>
  </c:forEach>

  <c:forEach items="${model.cartItems[0].productAttributes}" var="productAttribute">
  <tr class="comparisonRow">
  	<td class="comparisonHdr"><c:out value="${productAttribute.optionName}" escapeXml="false" /></td>
    <c:set var="status" value="0"/>
	<c:forEach items="${model.cartItems}" var="cartItem" varStatus="test"> 
	  <c:forEach items="${cartItem.productAttributes}" var="productAttribute2"> 
	    <c:if test="${(productAttribute2.optionName == productAttribute.optionName ) and (productAttribute2.optionIndex == productAttribute.optionIndex)}">
          <td class="comparisonCol${status % 2}"><c:out value="${productAttribute2.valueName}" escapeXml="false" />&nbsp;</td>
          <c:set var="status" value="${status+1}"/>
        </c:if>
      </c:forEach>
	</c:forEach> 
  </tr>
  </c:forEach>

  <tr class="comparisonRow">
    <td class="comparisonHdr">&nbsp;</td>
	<c:forEach items="${model.cartItems}" var="cartItem" varStatus="test"> 
    <td>
	  <form action="addToCart.jhtm" method="post" id="addToCartForm${test.index}" onsubmit="checkForm('${test.index}'); return false;">
      <c:set property="price1" value="${cartItem.product.priceTable1 - discount}" target="${cartItem.product}"/>
       <c:forEach items="${cartItem.productAttributes}" var="attr"> 
        <input type="hidden" name="optionCode_${cartItem.product.id}" value="<c:out value="${attr.optionCode}"/>"/>	
        <input type="hidden" name="<c:out value="${attr.optionCode}"/>-option_${cartItem.product.id}" value="<c:out value="${attr.optionIndex}"/>"/>
            
        <c:choose>
          <c:when test="${attr.optionCusValue != null}">
            <input type="hidden" name="<c:out value="${attr.optionCode}"/>-option_values_cus_with_index_${cartItem.product.id}_${attr.optionIndex}" value="${attr.optionIndex}_${attr.optionCusValue}"/>
          </c:when>
          <c:otherwise>
            <input type="hidden" 
                   name="<c:out value="${attr.optionCode}"/>-option_values_${cartItem.product.id}_<c:out value="${attr.optionIndex}"/>" 
                   value="<c:out value="${attr.valueIndex}"/>">
          </c:otherwise>
        </c:choose>
        <%--
        <input type="hidden" name="optionNVPair" value="${fn:escapeXml(attr.optionName)}_value_${fn:escapeXml(attr.valueName)}"/>  
	   --%>
	   </c:forEach>
	  <input type="hidden" name="product.id" value="${cartItem.product.id}">
	  <input type="hidden" name="quantity_${cartItem.product.id}" value="1">
	  <input type="submit" value="Add To Cart">
	  </form>
    </td>
	</c:forEach> 
  </tr>

  <tr class="comparisonRow">
    <td class="comparisonHdr">&nbsp;</td>
	<c:forEach items="${model.cartItems}" varStatus="status" var="cartItem">
    <td class="comparisonCol${status.index % 2}"><input name="__selected_id" value="${status.index}" type="checkbox" class="deleteSelected"></td>
	</c:forEach> 
  </tr>

</table>
	</td>
  </tr>
  <tr>
	<td align="right"><input type="submit" name="__delete" value="Remove Selected Products" onClick="return deleteSelected()"></td>
  </tr>

</table>

</c:if>

</c:if>
</div>
</body>