<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>

<tiles:insertDefinition name="${_template}" flush="true">
	<tiles:putAttribute name="content" type="string">
<link href="${_contextpath}/assets/stylesheet.css" rel="stylesheet" type="text/css">
<link href="${_contextpath}/assets/SmoothGallery/multiboxASI.css" rel="stylesheet"  type="text/css"/>
<script type="text/javascript">
var box2 = {};
window.addEvent('domready',function() {
	  box2 = new multiBox('mbCategory', {waitDuration: 5,showControls: false,overlay: new overlay()});
});
function zoomIn(productId) {
	window.open('${_contextpath}/productImage.jhtm?id='+productId+'&layout=2&imgUrl=' + document.getElementById("_image").src,'name','width=800,height=600,resizable=yes,scrollbars=1');
}
function updateImage(absoluteImage, imageUrl){
    if(absoluteImage == 'false') {
		$('_image').setProperty('src','${_contextpath}/assets/Image/Product/detailsbig/'+imageUrl);
	} else {
		$('_image').setProperty('src',imageUrl);
	}
	return false;
}
//comparison
function compareProduct(productId){
	if(parseInt($('currentComparison').value) < parseInt($('maxComparisons').value)) {
		var urlappend = 'option=true';
    	$$('input.addComparison').each(function(input){
    		urlappend = urlappend +'&optionNVPair='+input.value;
    	});
    	//alert(urlappend);
    	var requestHTMLData = new Request.HTML ({
    		url: "${_contextpath}/ajaxAddToCompare.jhtm?&productId="+productId+"&"+urlappend,
    		onSuccess: function(response){ 
				$('product'+productId).set('html', '<input type=\"image\" class="compare" src=\"${_contextpath}/assets/Image/Layout/button_addedToCompare.png\" width=\"170px;\" height=\"50px;\" onclick=\"return false;\"/>');
				$('currentComparison').value = parseInt($('currentComparison').value)+ 1;
				$('viewComparison').setStyle('display', 'block');
			}
    	}).send();
	} else {
		alert('Max comparison limit reached.');
	}
}
</script>

<div class="detail14" style="background-color: white; border-radius : 5px; border: 1px solid gray;">


  <div class="optionMultiValuesBox">
  <!-- Image -->
  
  <div style="display: none;">
	<c:set var="size"/>
    <c:forEach items="${model.selectedOptions}" var="selectedOption">
      <c:if test="${fn:contains(selectedOption.key, 'size') or fn:contains(selectedOption.key, 'Size')}">
        <c:set var="size" value="${selectedOption.value}"/>	      
      </c:if>
    </c:forEach>
  </div>

  <div class="reviewBoxImage">
    <div class="reviewBoxTitle">Product Images</div> 
    <div class="details_image_box" style="width: 330px;">
		<c:forEach items="${model.product.images}" var="image" varStatus="status">
		  <c:if test="${status.first}">
		    <a  style="cursor: pointer;" onclick="zoomIn('${model.product.id}');" title="View Larger Images"><img id="_image" src="<c:if test="${!image.absolute}">${_contextpath}/assets/Image/Product/detailsbig/</c:if><c:out value="${image.imageUrl}"/>" border="0" style="<c:if test="${siteConfig['DETAILS_IMAGE_WIDTH'].value != '' }">width: <c:out value="${siteConfig['DETAILS_IMAGE_WIDTH'].value}"/>px</c:if>" alt="<c:out value="${model.product.name}"/><c:if test="${model.product.shortDesc != ''}"> - <c:out value="${model.product.shortDesc}"/></c:if>"/></a>
		  </c:if>
		</c:forEach>
	    <div>
	    <c:forEach items="${model.product.images}" var="image" varStatus="status">
		  <a href="#" class="details_thumbnail_anchor" onClick="return updateImage('${image.absolute}','${image.imageUrl}');">
		    <img src="<c:if test="${!image.absolute}">${_contextpath}/assets/Image/Product/detailsbig/</c:if><c:out value="${image.imageUrl}"/>" class="details_thumbnail" alt="<c:out value="${model.product.name}"/><c:if test="${model.product.shortDesc != ''}"> - <c:out value="${model.product.shortDesc}"/></c:if>">
	      </a>
		</c:forEach> 
		<span class="moreImageWrapper">
		  <a onclick="zoomIn('${model.product.id}');" title="View Larger Images"><span>View Larger Images</span></a>
		</span>
	  </div>
	</div>        
	
	<div class="pdWrapper">
	  <div class="pdTitle">Product Description</div>
	  <div class="pdItem">
        <div class="pdName">Diploma Size:</div> 
        <div class="pdValue"><c:out value="${model.product.field3}"/></div> 
	  </div>

      <div style="display: none;">
	  <c:set var="outSideWidth"/>
	  <c:set var="outSideHeight"/>
	  <c:choose>
	    <c:when test="${customWidthOptValue != null and customWidthOptValue != '' and customHeightOptValue != null and customHeightOptValue != ''}">
	      <c:set var="outSideWidth" value="${customWidthOptValue + model.product.field15}"/>
	      <c:set var="outSideHeight" value="${customHeightOptValue + model.product.field15}"/>
	    </c:when>
	    <c:otherwise>
	      <c:set var="outSideWidth" value="${model.product.field4 + model.product.field15}"/>
	      <c:set var="outSideHeight" value="${model.product.field5 + model.product.field15}"/>
	    </c:otherwise>
	  </c:choose>
      </div>
	  
	  <div class="pdItem">
        <div class="pdName">Frame Outside Dimension:</div> 
        <div class="pdValue">
          <fmt:formatNumber value="${outSideWidth}" pattern="#,##0.00#" maxFractionDigits="2" />" X <fmt:formatNumber value="${outSideHeight}" pattern="#,##0.00#" maxFractionDigits="2" />"
        </div>
      </div>

	  <div class="pdItem">
        <div class="pdName">Moulding:</div> 
        <div class="pdValue"><c:out value="${model.product.field6}"/></div> 
	  </div>

	  <div class="pdItem">
        <div class="pdName">Top Mat:</div> 
        <div class="pdValue"><c:out value="${model.product.field7}"/></div> 
	  </div>

	  <div class="pdItem">
        <div class="pdName">Fillet:</div> 
        <div class="pdValue"><c:out value="${model.product.field12}"/></div> 
	  </div>
	
	  <div class="pdItem">
        <div class="pdName">Glass:</div> 
        <div class="pdValue"><c:out value="${model.product.field8}"/></div> 
	  </div>
	</div>
	
  </div>
  <!-- Description -->
  
  <div class="reviewBoxSummary">

    <div class="reviewBoxItem">
      <div class="reviewBoxName" style="font-weight: bold; font-size: 18px;"><c:out value="${model.product.name}"/></div> 
      <div class="reviewBoxValue"></div> 
    </div>
    
    <div style="height: 25px; clear: both;"> </div>
    <c:if test="${model.product.salesTag.discount  != null}">
    <div class="reviewBoxItem">
      <div class="reviewBoxName">Retail Price</div> 
      <div class="reviewBoxValue" <c:if test="${discount > 0}"> text-decoration: line-through; </c:if>">
           $<fmt:formatNumber value="${model.product.price1}" pattern="#,##0.00#" maxFractionDigits="${siteConfig['MAX_FRACTION_DIGIT'].value}" />
      </div> 
    </div>
    
    <c:set var="discount" value="0"/>
    <c:if test="${model.product.salesTag != null}">
      <c:choose>
        <c:when test="${model.product.salesTag.percent}">
          <c:set var="discount" value="${model.product.price1 * model.product.salesTag.discount / 100}"/>
        </c:when>
        <c:otherwise>
          <c:choose>
            <c:when test="${model.product.salesTag.discount > model.product.price1}">
              <c:set var="discount" value="${model.product.price1}"/>
            </c:when>
            <c:otherwise>
              <c:set var="discount" value="${model.product.salesTag.discount}"/>
            </c:otherwise>
          </c:choose>
        </c:otherwise>
      </c:choose>
    </c:if>
    
    <div class="reviewBoxItem">
      <div class="reviewBoxName" style="color: red;">FrameStore Direct <fmt:formatNumber value="${model.product.salesTag.discount}" maxFractionDigits="0" /><c:if test="${model.salesTag.percent}">%</c:if> Discount:</div> 
      <div class="reviewBoxValue">
          - $<fmt:formatNumber value="${discount}" pattern="#,##0.00#" maxFractionDigits="${siteConfig['MAX_FRACTION_DIGIT'].value}" />
      </div> 
    </div>
    </c:if>
    
    <c:if test="${model.product.price1 > 0}">
    <div class="reviewBoxItem">
      <div class="reviewBoxName" style="color: red;">Retail Price</div> 
      <div class="reviewBoxValue" style="color: RED;">
        <c:choose>
          <c:when test="${discount == null}">$<fmt:formatNumber value="${model.product.price1}" pattern="#,##0.00#" maxFractionDigits="${siteConfig['MAX_FRACTION_DIGIT'].value}" /></c:when>
          <c:otherwise>$<fmt:formatNumber value="${model.product.price1 - discount}" pattern="#,##0.00#" maxFractionDigits="${siteConfig['MAX_FRACTION_DIGIT'].value}" /></c:otherwise>
        </c:choose>
      </div> 
    </div>  
	</c:if>
    
    <div style="height: 25px; clear: both;"> </div>
    <div class="reviewBoxItem">
      <div class="reviewBoxName" style="font-weight: bold;">Shipping </div> 
      <div class="reviewBoxValue"></div> 
    </div>

    <div class="reviewBoxItem">
      <div class="reviewBoxName">Standard US</div> 
      <div class="reviewBoxValue" style="color: RED;">FREE</div> 
    </div>  

    <div style="clear: both;"></div>
	<c:if test="${fn:contains(model.product.sku, '-P')}">
	<form action="${_contextpath}/addToCart.jhtm" method="post">
    <input type="hidden" id="maxComparisons" value="5">
    <input type="hidden" id="currentComparison" value="${fn:length(model.cartItemsComparisonList)}">
    <input type="hidden" name="optionNVPair" class="addComparison" value="Size_value_${fn:escapeXml(model.product.field3)}"/>
    <input type="hidden" name="optionNVPair" class="addComparison" value="Style_value_${fn:escapeXml(model.product.field9)}"/>
    <input type="hidden" name="optionNVPair" value="Design_value_${model.product.field10}"/>
    <!-- This is for Comparison -->
    <input type="hidden" name="optionNVPair" class="addComparison" value="Design Package_value_${fn:escapeXml(model.product.field10)}"/>
    <input type="hidden" name="optionNVPair" class="addComparison" value="Frame Selected_value_${fn:escapeXml(model.product.name)}"/>
    <input type="hidden" name="product.id" value="${model.product.id}"/>
    <input type="hidden" name="quantity_${model.product.id}" value="1"/>
    <div class="cartCompareWrapper">
    <div id="customAddToCart">
      <input type="image" border="0" name="_addtocart" id="addToCart" src="${_contextpath}/assets/Image/Layout/button_continue.png"/>
    </div>
    <div class="addToCompare">
      <c:choose>
        <c:when test="${fn:length(model.cartItemsComparisonList) >=  model.maxComparisons}"></c:when>
        <c:when test="${model.isItemAddedToList == true}">
            <input type="image" border="0" class="compare" src="${_contextpath}/assets/Image/Layout/button_addedToCompare.png" onclick="return false;"/>
  		</c:when>
        <c:otherwise>
          <div id="product${model.product.id}">
            <input type="image" border="0" class="compare" src="${_contextpath}/assets/Image/Layout/button_addToCompare.png" onclick="compareProduct('${model.product.id}'); return false;"/>
  		  </div>
        </c:otherwise>
      </c:choose>
    </div>
    <div style="clear: both;"></div>
    <c:set var="display" value="none"/>
    <c:if test="${fn:length(model.cartItemsComparisonList) > 0}">
      <c:set var="display" value="block"></c:set>
    </c:if>
    <div class="viewComparison" id="viewComparison" style="float:right; display: ${display}" >
	  <a href="${_contextpath}/comparison.jhtm?optionComparison=true" rel="width:970,height:750" class="mbCategory"><img alt="" src="${_contextpath}/assets/Image/Layout/button_viewComparison.png"></a>
    </div> 
    </div> 
    </form>
	</c:if>
  </div>
  
  </div>
  <div style="clear: both;"></div>
  
<!-- Assign Options selection to assigned product so that assigned product is sent to cart and order -->
<input type="hidden" name="product.id" value="${model.product.id}"/>
<input type="hidden" name="quantity_${model.product.id}" id="quantity_${model.product.id}" value="1">
      
<c:if test="${model.optionList != null and not empty model.optionList}">
<c:forEach items="${model.optionList}" var="productOption">

  <div style="display:none;">
  <c:set var="optName" value="${productOption.optionCode}-option_values_${model.productId}_${productOption.index}"></c:set>
  <c:set var="optValue"></c:set>
  <c:forEach var="entry" items="${model.originalIndexSelection}">
     <c:if test="${optName == entry.key}">
       <c:set var="optValue" value="${entry.value}"></c:set>  
     </c:if>
  </c:forEach>
  </div>

  <c:if test="${!empty productOption.values}">
    <input type="hidden" name="<c:out value="${productOption.optionCode}"/>-option_${model.product.id}" value="<c:out value="${productOption.index}"/>"/>
    <input type="hidden" name="optionCode_${model.product.id}" value="<c:out value="${productOption.optionCode}"/>"/>	
    <c:forEach items="${productOption.values}" var="prodOptValue" varStatus="povStatus">
       <c:if test="${prodOptValue.index == optValue}">
       <input type="hidden" 
              name="<c:out value="${productOption.optionCode}"/>-option_values_${model.product.id}_<c:out value="${productOption.index}"/>" 
              value="<c:out value="${prodOptValue.index}"/>">
	   </c:if>
    </c:forEach>   
  </c:if>  

<c:if test="${customSizeOptValue != null and customSizeOptValue != ''}">
  <input type="hidden" name="<c:out value="${model.optionList[1].optionCode}"/>-option_values_cus_with_index_${model.product.id}_${model.optionList[1].index}" value="${customSizeOptIndex}_${customSizeOptValue}" id="customSize"/>
</c:if>
</c:forEach>

</c:if>  
</div>

	</tiles:putAttribute>
</tiles:insertDefinition>