<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<div class="priceWrapper" style="float: left;">
          <c:set var="discount" value="0"/>
	      <c:if test="${model.product.salesTag != null}">
	        <c:choose>
	          <c:when test="${model.product.salesTag.percent}">
	            <c:set var="discount" value="${model.product.price1 * model.product.salesTag.discount / 100}"/>
	          </c:when>
	          <c:otherwise>
	            <c:choose>
	              <c:when test="${model.product.salesTag.discount > model.product.price1}">
	                <c:set var="discount" value="${model.product.price1}"/>
	              </c:when>
	              <c:otherwise>
	                <c:set var="discount" value="${model.product.salesTag.discount}"/>
	              </c:otherwise>
	            </c:choose>
	          </c:otherwise>
	        </c:choose>
	      </c:if>
        
          <div class="prItem">
	        <div class="prName" style="float: left;">Retail Price:</div> 
	        <div class="prValue" style="float: left; <c:if test="${discount > 0}">text-decoration: line-through;</c:if>">
	          $<fmt:formatNumber value="${model.product.price1}" pattern="#,##0.00#" maxFractionDigits="${siteConfig['MAX_FRACTION_DIGIT'].value}" />
		    </div>
		    <div style="clear: both;"></div> 
		  </div>
        
          <c:if test="${discount > 0}">
          <div class="prItem">
	        <div class="prName" id="discountTitle" style="float: left;">FrameStoreDirect Price:</div> 
	        <div class="prValue" id="discountPrice" style="float: left;">
	          $<fmt:formatNumber value="${model.product.price1 - discount}" pattern="#,##0.00#" maxFractionDigits="${siteConfig['MAX_FRACTION_DIGIT'].value}" />
		    </div>
		    <div style="clear: both;"></div> 
		  </div>
          </c:if>
          
          <div class="prItem">
	        <div class="prName" id="couponMessage" style="float: left; width: 200px;">Price Before any Applicable Discounts</div>
	       <div style="clear: both;"></div> 
		  </div>
          <c:if test="${gSiteConfig['gPRODUCT_REVIEW'] and siteConfig['PRODUCT_RATE'].value != 'true' and model.product.enableRate}">
		    <div class="prItem">
	          <div>
		    	<c:set value="${model.product}" var="product"></c:set>
		    	<%@ include file="/WEB-INF/jsp/frontend/common/quickProductReview.jsp" %>
		       </div>
		    </div>
		    <div style="clear: both;"></div> 
		  </c:if>
	    </div>
        
        <div class="cartWrapper" style="float: left;">
          <div class="option">
	        <select name="childProducts" onchange="updateProduct(this.value)" id="childProducts">
			  <c:forEach items="${model.childProducts}" var="childProduct" varStatus="povStatus">
				<option value="${childProduct.sku}" <c:if test="${childProduct.sku == model.product.sku}">selected="selected"</c:if>  ><c:out value="${childProduct.field3}"></c:out></option>
			  </c:forEach>
			</select>
	       </div>
          <div id="addToCart">
          <form action="${_contextpath}/addToCart.jhtm" method="post">
		    <input type="hidden" name="optionNVPair" class="addComparison" value="Size_value_${fn:escapeXml(model.product.field3)}"/>
		    <input type="hidden" name="optionNVPair" class="addComparison" value="Style_value_${fn:escapeXml(model.product.field9)}"/>
		    <input type="hidden" name="optionNVPair" class="addComparison" value="Design Package_value_${fn:escapeXml(model.product.field10)}"/>
		    <input type="hidden" name="optionNVPair" class="addComparison" value="Frame Selected_value_${fn:escapeXml(model.product.name)}"/>
		    <input type="hidden" name="product.id" value="${model.product.id}"/>
		    <input type="hidden" name="quantity_${model.product.id}" id="quantity_${model.product.id}" value="1">
	        <input type="image" border="0" name="_addtocart" class="_addtocart" id="_addtocart" src="${_contextpath}/assets/Image/Layout/btn_add_to_cart.png" />
          </form>
          </div>
          
          <div id="shippingMessage">
            Qualifies for 
            <div id="freeShipping">
             	FREE Shipping
            </div>
          </div>
        </div>      
          <div style="clear: both;"></div> 
	  </div>