<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>

<script type="text/javascript">
var box2 = {};
window.addEvent('domready',function() {
	  box2 = new multiBox('mbCategory', {waitDuration: 5,showControls: false,overlay: new overlay()});
});
</script>
<c:if test="${model.assignedProduct != null}">
		
<script type="text/javascript">
function zoomIn(productId) {
	window.open('${_contextpath}/productImage.jhtm?id='+productId+'&layout=2&imgUrl=' + document.getElementById("_image").src,'name','width=800,height=600,resizable=yes,scrollbars=1');
}
function updateImage(absoluteImage, imageUrl){
    if(absoluteImage == 'false') {
		$('_image').setProperty('src','${_contextpath}/assets/Image/Product/detailsbig/'+imageUrl);
	} else {
		$('_image').setProperty('src',imageUrl);
	}
	return false;
}
</script>
</c:if>
<c:if test="${model.optionList != null and not empty model.optionList}">
<div class="multiOptionsWrapper" id="multiOptionsWrapperId">
<div class="multiOptions" id="multiOptions">

<input type="hidden" id="totalOption" value="${fn:length(model.optionList)}"/>

  <!-- Step 1 Starts-->
  <c:set var="productOption" value="${model.optionList[0]}"></c:set>
  <input type="hidden" name="optionCode_${model.productId}" value="<c:out value="${productOption.optionCode}"/>"/>	
  <div style="display:none;">
  <c:set var="optName" value="${productOption.optionCode}-option_values_${model.productId}_${productOption.index}"></c:set>
  <c:set var="optValue"></c:set>
  <c:forEach var="entry" items="${model.originalIndexSelection}">
     <c:if test="${optName == entry.key}">
       <c:set var="optValue" value="${entry.value}"></c:set>  
     </c:if>
  </c:forEach>
  </div>
  
  <c:if test="${!empty productOption.values}">
  <input type="hidden" name="<c:out value="${productOption.optionCode}"/>-option_${model.productId}" value="<c:out value="${productOption.index}"/>"/>
  <div class="optionWrapper">
    <div class="optionTitleBox">
      1.<fmt:message key="f_product12_step1"/>
    </div>
    <div class="optionMultiValuesBox">
     
    <c:forEach items="${productOption.values}" var="prodOptValue" varStatus="povStatus">
      <c:set value="optionValueBox" var="valueClass"/>
      <c:if test="${prodOptValue.index == optValue}">
        <c:set value="optionValueBox optionSelectedValueBox" var="valueClass"></c:set>
        <!-- Will be used in step 3 -->
        <c:set var="selectedStyle" value="${prodOptValue.name}"/>
      </c:if>
      <div class="${valueClass}" align="center">
  	    <!-- Title -->
  	    <div class="optionValueName" <c:if test="${prodOptValue.index != optValue}">style="opacity:0.3; filter: alpha(opacity = 30);"</c:if>><c:out value="${prodOptValue.name}"/></div>
        
        <!-- Image -->
        <img class="optionBigImageBox" 
             <c:if test="${prodOptValue.index != optValue}">style="opacity:0.3; filter: alpha(opacity = 30);"</c:if>
             onclick="getOption('${model.productId}','${productOption.optionCode}_${productOption.index}_${prodOptValue.index}'); return false;"
             src="${_contextpath}/assets/Image/Product/options/${prodOptValue.imageUrl}"/>
        
        <!-- Description -->
        <div class="optionValueDescStep1" <c:if test="${prodOptValue.index != optValue}">style="opacity:0.3; filter: alpha(opacity = 30);"</c:if>><c:out value="${prodOptValue.description}" escapeXml="flase"/></div>
        
        <!-- Select Button -->
        <div class="optionSelectBox">
           <c:set value="button_selectStyle.png" var="source"/>
           <c:if test="${prodOptValue.index == optValue}"><c:set value="button_styleSelected.png" var="source"/></c:if>

           <input type="image"  class="optionButton" 
                  src="${_contextpath}/assets/Image/Layout/${source}" 
                  onclick="getOption('${model.productId}','${productOption.optionCode}_${productOption.index}_${prodOptValue.index}'); return false;"/>
           <input type="radio"  style="display: none;" 
                  class="selectedOption" 
                  id = "<c:out value="${productOption.optionCode}"/>_<c:out value="${productOption.index}"/>_<c:out value="${prodOptValue.index}"/>"
                  name="<c:out value="${productOption.optionCode}"/>-option_values_${model.productId}_<c:out value="${productOption.index}"/>" 
                  value="<c:out value="${prodOptValue.index}"/>"
          	      <c:if test="${prodOptValue.index == optValue}">checked="checked"</c:if>/>
        </div> 
      </div>
      <c:if test="${(povStatus.index+1) % 3 == 0}">
        <div style="clear: both;"></div><br/>
      </c:if>
      
    </c:forEach>
    <div style="clear: both;"></div>
    <div class="optionLink">
      <a href="${_contextpath}/category/53/detailed-style-description.html" rel="width:1000,height:700" id="mb0" class="mbCategory">Click here for detailed style description</a>
    </div>
    </div>
  </div>
  </c:if>
  <!-- Step 1 Ends -->

  <!-- Step 2 Starts-->
  
  <!-- Size Starts-->
  <c:if test="${model.optionList[1] != null}">
  <c:set var="productOption" value="${model.optionList[1]}"></c:set>
  <input type="hidden" name="optionCode_${model.productId}" value="<c:out value="${productOption.optionCode}"/>"/>	
  <div style="display:none;">
  <c:set var="optName" value="${productOption.optionCode}-option_values_${model.productId}_${productOption.index}"></c:set>
  <c:set var="optValue"></c:set>
  <c:forEach var="entry" items="${model.originalIndexSelection}">
     <c:if test="${optName == entry.key}">
       <c:set var="optValue" value="${entry.value}"></c:set>  
     </c:if>
  </c:forEach>
  <c:set var="custOptName" value="${productOption.optionCode}-option_values_cus_with_index_${model.productId}_${productOption.index}"></c:set>
  <c:set var="customWidthOptValue"></c:set>
  <c:set var="customHeightOptValue"></c:set>
  <c:set var="customSizeOptValue"></c:set>
  <c:set var="customSizeOptIndex" value="0"></c:set>
     <c:forEach var="entry" items="${model.originalValueSelection}">
       <c:if test="${custOptName == entry.key}">
         <c:set var="customSizeOptValue" value="${fn:split(entry.value,'_')[1]}"></c:set>  
         <c:set var="customSizeOptIndex" value="${fn:split(entry.value,'_')[0]}"></c:set>  
         <c:set var="customWidthOptValue" value="${fn:split(customSizeOptValue,'X')[0]}"></c:set>  
         <c:set var="customHeightOptValue" value="${fn:split(customSizeOptValue,'X')[1]}"></c:set>  
     </c:if>
  </c:forEach>
  <c:if test="${customSizeOptIndex == -1}">
    <c:set var="customSizeOptIndex" value="0"></c:set>
  </c:if>     
  </div>
  
  <c:if test="${!empty productOption.values}">
  <input type="hidden" name="<c:out value="${productOption.optionCode}"/>-option_${model.productId}" value="<c:out value="${productOption.index}"/>"/>
  <div class="optionWrapper">
    <div class="optionTitleBox">
      2.<fmt:message key="f_product12_step2"/>
    </div>
    <div class="optionMultiValuesBox">
    <div class="customSizeTitle">Size of YOUR DIPLOMA:</div>
  	 
    <c:forEach items="${productOption.values}" var="prodOptValue" varStatus="povStatus">
      <c:set value="optionValueBox" var="valueClass"/>
      <c:if test="${prodOptValue.index == optValue}">
        <c:set value="optionValueBox optionSelectedValueBox" var="valueClass"></c:set>
      </c:if>
      <div class="${valueClass}" align="center" style="width: 140px;">
  	    <!-- Title -->
  	    <div class="optionValueName" <c:if test="${prodOptValue.index != optValue and optValue != ''}">style="opacity:0.3; filter: alpha(opacity = 30);"</c:if>><c:out value="${prodOptValue.name}"/></div>
        
        <!-- Description -->
        <div class="optionValueDescStep2" <c:if test="${prodOptValue.index != optValue and optValue != ''}">style="opacity:0.3; filter: alpha(opacity = 30);"</c:if>><c:out value="${prodOptValue.description}" escapeXml="flase"/></div>
        
        <!-- Select Button -->
        <div class="optionSelectBox">
           <c:set value="button_selectSize.png" var="source"/>
           <c:if test="${prodOptValue.index == optValue}"><c:set value="button_sizeSelected.png" var="source"/></c:if>
           <input type="image" class="optionButton"
                  src="${_contextpath}/assets/Image/Layout/${source}" 
                  onclick="toggleSelection(true); getOption('${model.productId}','${productOption.optionCode}_${productOption.index}_${prodOptValue.index}'); return false;"/>
           <input type="radio"  style="display: none;" 
                  class="selectedOption" 
                  id = "<c:out value="${productOption.optionCode}"/>_<c:out value="${productOption.index}"/>_<c:out value="${prodOptValue.index}"/>"
                  name="<c:out value="${productOption.optionCode}"/>-option_values_${model.productId}_<c:out value="${productOption.index}"/>" 
                  value="<c:out value="${prodOptValue.index}"/>"
          	      <c:if test="${prodOptValue.index == optValue}">checked="checked"</c:if>/>
      			     
        </div> 
      </div>
    </c:forEach>
    <div style="clear: both;"></div>
    
    <!-- Size in Drop Down Starts -->
    <input type="hidden" name="<c:out value="${productOption.optionCode}"/>-option_values_cus_with_index_${model.productId}_${productOption.index}" value="${customSizeOptIndex}_${customSizeOptValue}" id="customSize"/>
    <input type="hidden" name="customSizeOptIndex" value="${customSizeOptIndex}" id="customSizeOptIndex"/>
    <div class="customSizeOptionValueBox">
    Other Size in inches: <select class="customSizeSelect" onchange="toggleSelection(false); assignValue('customSize', this.value);"  name="customWidth" id="customWidth">
      <option value="0">W</option>
      <c:forEach begin="600" end="1600" var="customWidth" step="25">
        <option value="${customWidth/100}" <c:if test="${customWidth/100 == customWidthOptValue}">selected="selected"</c:if> ><c:out value="${customWidth/100}"/>"</option>
      </c:forEach>
    </select> X
    <select class="customSizeSelect" onchange="toggleSelection(false); assignValue('customSize', this.value); " name="customHeight" id="customHeight">
      <option value="0">H</option>
      <c:forEach begin="600" end="2000" var="customHeight" step="25">
        <option value="${customHeight/100}" <c:if test="${customHeight/100 == customHeightOptValue}">selected="selected"</c:if>><c:out value="${customHeight/100}"/>"</option>
      </c:forEach>
    </select>
    <span class="optionLink">
      <a href="${_contextpath}/category/54/how-to-measure-diploma.html" rel="width:1000,height:500" id="mb0" class="mbCategory">How to measure your diploma</a>
    </span>
    </div>
	<!-- Size in Drop Down Ends -->  
    <!-- Size Ends-->
    <!-- Design Package Starts-->
    <c:if test="${model.optionList[2] != null}">
    <div class="customSizeTitle">Choose a design level and price range</div>
    
    <c:set var="productOption" value="${model.optionList[2]}"></c:set>
    <input type="hidden" name="optionCode_${model.productId}" value="<c:out value="${productOption.optionCode}"/>"/>	
    <div style="display:none;">
    <c:set var="optName" value="${productOption.optionCode}-option_values_${model.productId}_${productOption.index}"></c:set>
    <c:set var="optValue"></c:set>
    <c:forEach var="entry" items="${model.originalIndexSelection}">
       <c:if test="${optName == entry.key}">
         <c:set var="optValue" value="${entry.value}"></c:set>  
       </c:if>
    </c:forEach>
    </div>
  
    <c:if test="${!empty productOption.values}">
    <input type="hidden" name="<c:out value="${productOption.optionCode}"/>-option_${model.productId}" value="<c:out value="${productOption.index}"/>"/>
    <div style="padding-top: 10px;">
     
      <c:forEach items="${productOption.values}" var="prodOptValue" varStatus="povStatus">
        <c:set value="optionValueBox" var="valueClass"/>
        <c:if test="${prodOptValue.index == optValue}">
          <c:set value="optionValueBox optionSelectedValueBox" var="valueClass"></c:set>
          <!-- Will be used in step 3 -->
          <c:set var="selectedDesign" value="${prodOptValue.name}"/>
        </c:if>
        <div class="${valueClass}" align="center">
  	      <!-- Title -->
  	      <div class="optionValueName" <c:if test="${prodOptValue.index != optValue and optValue != ''}">style="opacity:0.3; filter: alpha(opacity = 30);"</c:if>><c:out value="${prodOptValue.name}"/></div>
          
          <!-- Pricing -->
          <c:if test="${prodOptValue.optionPrice != null}">
            <c:set var="discount" value="0"/>
            <c:if test="${model.salesTag != null and model.salesTag.inEffect}">
              <c:choose>
                <c:when test="${model.salesTag.percent}">
                  <c:set var="discount" value="${prodOptValue.optionPrice * model.salesTag.discount / 100}"/>
                </c:when>
                <c:otherwise>
                  <c:choose>
                    <c:when test="${model.salesTag.discount > prodOptValue.optionPrice}">
                      <c:set var="discount" value="${prodOptValue.optionPrice}"/>
                    </c:when>
                    <c:otherwise>
                      <c:set var="discount" value="${model.salesTag.discount}"/>
                    </c:otherwise>
                  </c:choose>
                </c:otherwise>
              </c:choose>
            </c:if>

            <div class="optionPricing" <c:if test="${prodOptValue.index != optValue and optValue != ''}">style="opacity:0.3; filter: alpha(opacity = 30);"</c:if>>
              <div class="optionPricingTitle">Pricing</div>
              <div>
                <div class="optionPricingName">FramestoreDirect Price:</div> 
                <div class="optionPricingValue">
                  $<fmt:formatNumber value="${prodOptValue.optionPrice - discount}" pattern="#,##0.00#" maxFractionDigits="${siteConfig['MAX_FRACTION_DIGIT'].value}" />
                </div>
              </div>
              <div style="clear: both;"></div>
             </div>
          </c:if>
          
          <!-- Image -->
          <img class="optionBigImageBox" 
               <c:if test="${prodOptValue.index != optValue and optValue != ''}">style="opacity:0.3; filter: alpha(opacity = 30);"</c:if> 
               onclick="getOption('${model.productId}','${productOption.optionCode}_${productOption.index}_${prodOptValue.index}'); return false;"
               src="${_contextpath}/assets/Image/Product/options/${prodOptValue.imageUrl}"/>
          
          <!-- Description -->
          <div class="optionValueDescStep3" <c:if test="${prodOptValue.index != optValue and optValue != ''}">style="opacity:0.3; filter: alpha(opacity = 30);"</c:if>><c:out value="${prodOptValue.description}" escapeXml="flase"/></div>
          
          
          <!-- Select Button -->
          <div class="optionSelectBox">
             <c:set value="button_select.png" var="source"/>
             <c:choose>
	           <c:when test="${prodOptValue.index == optValue}">
	             <c:choose>
	               <c:when test="${fn:contains(prodOptValue.name, 'Standard') or fn:contains(prodOptValue.name, 'Classic')}">
	                 <c:set value="button_selectedClassic.png" var="source"/>  
	               </c:when>
	               <c:when test="${fn:contains(prodOptValue.name, 'Premium')}">
	                 <c:set value="button_selectedPremium.png" var="source"/>  
	               </c:when>
	               <c:when test="${fn:contains(prodOptValue.name, 'Deluxe')}">
	                 <c:set value="button_selectedDelux.png" var="source"/>  
	               </c:when>
	             </c:choose>
	           </c:when>
	           <c:otherwise>
	             <c:choose>
	               <c:when test="${fn:contains(prodOptValue.name, 'Standard') or fn:contains(prodOptValue.name, 'Classic')}">
	                 <c:set value="button_selectClassic.png" var="source"/>  
	               </c:when>
	               <c:when test="${fn:contains(prodOptValue.name, 'Premium')}">
	                 <c:set value="button_selectPremium.png" var="source"/>  
	               </c:when>
	               <c:when test="${fn:contains(prodOptValue.name, 'Deluxe')}">
	                 <c:set value="button_selectDelux.png" var="source"/>  
	               </c:when>
	             </c:choose>
	           </c:otherwise>
	         </c:choose>
             <input type="image" class="optionButton" 
                  src="${_contextpath}/assets/Image/Layout/${source}" 
                  onclick="getOption('${model.productId}','${productOption.optionCode}_${productOption.index}_${prodOptValue.index}'); return false;"/>
             <input type="radio"  style="display: none;" 
                  class="selectedOption" 
                  id = "<c:out value="${productOption.optionCode}"/>_<c:out value="${productOption.index}"/>_<c:out value="${prodOptValue.index}"/>"
                  name="<c:out value="${productOption.optionCode}"/>-option_values_${model.productId}_<c:out value="${productOption.index}"/>" 
                  value="<c:out value="${prodOptValue.index}"/>"
          	      <c:if test="${prodOptValue.index == optValue}">checked="checked"</c:if>/>
          </div> 
        </div>
        <c:if test="${(povStatus.index+1) % 3 == 0}">
          <div style="clear: both;"></div><br/>
        </c:if>
      </c:forEach>
      
      <div style="clear: both;"></div>
    </div>
    </c:if>
    <!-- Design Package Ends -->
    <div class="optionLink">
      <a href="${_contextpath}/category/55/detailed-design-package-description.html" rel="width:1000,height:700" id="mb0" class="mbCategory">Click here for detailed design package description</a>
    </div>
     </c:if> 
    </div>
  </div>
  </c:if>
  
  </c:if>
  <!-- Step 2 Ends -->


  <!-- Step 3 Starts-->

  <c:if test="${model.optionList[3] != null}">
  <c:set var="productOption" value="${model.optionList[3]}"></c:set>
  <input type="hidden" name="optionCode_${model.productId}" value="<c:out value="${productOption.optionCode}"/>"/>	
  <div style="display:none;">
  <c:set var="optName" value="${productOption.optionCode}-option_values_${model.productId}_${productOption.index}"></c:set>
  <c:set var="optValue"></c:set>
  <c:forEach var="entry" items="${model.originalIndexSelection}">
     <c:if test="${optName == entry.key}">
       <c:set var="optValue" value="${entry.value}"></c:set>  
     </c:if>
  </c:forEach>
  </div>
  
  <c:if test="${!empty productOption.values}">
  <input type="hidden" name="<c:out value="${productOption.optionCode}"/>-option_${model.productId}" value="<c:out value="${productOption.index}"/>"/>
  <div class="optionWrapper">
    <div class="optionTitleBox">
      3.<fmt:message key="f_product12_step3"/>
    </div>
    <div class="optionMultiValuesBox">
     
    <c:forEach items="${productOption.values}" var="prodOptValue" varStatus="povStatus">
      <c:set value="optionValueBox" var="valueClass"/>
      <c:if test="${prodOptValue.index == optValue}">
        <c:set value="optionValueBox optionSelectedValueBox" var="valueClass"></c:set>
      </c:if>
      <div class="${valueClass}" align="center"  style="width: 300px;">
  	    
        <!-- Image -->
        <img class="optionBigImageBox" 
             style="width: 280px; height:280px; 
             <c:if test="${prodOptValue.index != optValue and optValue != ''}">opacity:0.3;filter: alpha(opacity = 30);</c:if>"
             onclick="getOption('${model.productId}','${productOption.optionCode}_${productOption.index}_${prodOptValue.index}'); return false;"
             src="${_contextpath}/assets/Image/Product/options/${prodOptValue.imageUrl}"/>
          
        <!-- Description -->
        <div class="optionValueDescStep4" style="width: 280px; <c:if test="${prodOptValue.index != optValue and optValue != ''}">opacity:0.3;filter: alpha(opacity = 30);</c:if>"><c:out value="${prodOptValue.description}" escapeXml="flase"/></div>
        
        <!-- Select Button -->
        <div class="optionSelectBox">
           <!-- Title -->
  	       <div class="optionValueName">
  	         <c:out value="${prodOptValue.name}"/>
  	       </div>
           <c:set value="button_selectFrame.png" var="source"/>
	       <c:if test="${prodOptValue.index == optValue}">
	         <c:set value="button_frameSelected.png" var="source"/>  
	       </c:if>
           <input type="image" class="optionButton"
                  src="${_contextpath}/assets/Image/Layout/${source}" 
                  onclick="getOption('${model.productId}','${productOption.optionCode}_${productOption.index}_${prodOptValue.index}'); return false;"/>
           <input type="radio"  style="display: none;" 
                  class="selectedOption" 
                  id = "<c:out value="${productOption.optionCode}"/>_<c:out value="${productOption.index}"/>_<c:out value="${prodOptValue.index}"/>"
                  name="<c:out value="${productOption.optionCode}"/>-option_values_${model.productId}_<c:out value="${productOption.index}"/>" 
                  value="<c:out value="${prodOptValue.index}"/>"
          	      <c:if test="${prodOptValue.index == optValue}">checked="checked"</c:if>/>
        </div> 
      </div>
      <c:if test="${(povStatus.index+1) % 2 == 0}">
        <div style="clear: both;"></div><br/>
      </c:if>
      
    </c:forEach>
    <div style="clear: both;"></div>
  </div>
  </div>
    
  </c:if>
  <c:if  test="${model.assignedProduct != null}">
    <%@ include file="/WEB-INF/jsp/frontend/framestore/customProduct.jsp" %>
  </c:if>
  </c:if>
  <!-- Step 3 Ends -->
</div>

<div id="summaryBoxId">
  <div id="summaryTitle">Summary</div>
  <div id="summaryContent"> 
    <c:forEach items="${model.selectedOptions}" var="selectedOption">
      <div class="selectedOptionTitle">
      <c:choose>
        <c:when test="${selectedOption.key == 'Style'}">Style:  </c:when>
        <c:when test="${fn:contains(selectedOption.key,'Size')}">Size: </c:when>
        <c:when test="${selectedOption.key == 'Design Package'}">Design Package: </c:when>
        <c:when test="${selectedOption.key == 'Frame Selected'}">Frame Selected: </c:when>
      </c:choose>
      </div>
      <div class="selectedOptionValue"> <c:out value="${selectedOption.value}"></c:out> </div>
      <div style="clear: both;"></div>
    </c:forEach>
    <br/>
    <c:if test="${model.price != null and model.price > 0}">
      <div  id="summaryPrice">
        <div class="selectedOptionTitle" id="summaryPriceTitle">FramestoreDirect Price: </div>
        <div class="selectedOptionValue" id="summarySalePriceTitleValue">
          $<fmt:formatNumber value="${model.discountedPrice}" pattern="#,##0.00#" maxFractionDigits="${siteConfig['MAX_FRACTION_DIGIT'].value}" />
        </div>
	    <div style="clear: both;"></div>
      </div>
	  <div id="summarySalePrice">
        <div class="selectedOptionTitle" id="summarySalePriceTitle">Shipping: </div>
        <div class="selectedOptionValue" id="summarySalePriceTitleValue">
          FREE
        </div>
	    <div style="clear: both;"></div>
      </div>
    </c:if>
  </div>
</div>

<div style="clear: both;"></div> 
</div>

</c:if>