<div id="newOption">
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>

<c:if test="${not empty product.productOptions}">
<script type="text/javascript">
window.addEvent('domready',function() {
	  var box2 = new multiBox('mbCategory', {descClassName: 'multiBoxDesc',waitDuration: 5,showControls: false,overlay: new overlay()});
});
</script>
<div class="multiOptionsWrapper" id="multiOptionsWrapperId">
<div class="multiOptions" id="multiOptions">
<input type="hidden" id="totalOption" value="${fn:length(product.productOptions)}"/>

<!-- Step 1 Starts-->
  <c:set var="productOption" value="${product.productOptions[0]}"></c:set>
  <input type="hidden" name="optionCode_${product.id}" value="<c:out value="${productOption.optionCode}"/>"/>	
  <div class="optionWrapper">
    <div class="optionTitleBox">
      <c:out value="${status.index + 1}"/>.<fmt:message key="f_product12_step1"/>
    </div>
    <div class="optionMultiValuesBox">
    <c:forEach items="${productOption.values}" var="prodOptValue" varStatus="povStatus">
  	  <div class="optionValueBox" align="center">
  	    <div class="optionValueName"><c:out value="${prodOptValue.name}"/></div>
        <!-- Image -->
        <img class="optionBigImageBox" 
             src="${_contextpath}/assets/Image/Product/options/${prodOptValue.imageUrl}"
             onclick="getOption('${product.id}','${productOption.optionCode}_${productOption.index}_${prodOptValue.index}'); return false;">
        
        <div class="optionValueDescStep1"><c:out value="${prodOptValue.description}" escapeXml="flase"/></div>
        <div class="optionSelectBox">
           <c:set value="button_select.png" var="source"/>
           <c:if test="${prodOptValue.index == optValue}"><c:set value="button_selected.png" var="source"/></c:if>
           <input type="image" class="optionButton" 
                  src="${_contextpath}/assets/Image/Layout/${source}" 
                  onclick="getOption('${product.id}','${productOption.optionCode}_${productOption.index}_${prodOptValue.index}'); return false;"/>
           <input type="radio"  style="display: none;" 
                  class="selectedOption" 
                  id = "<c:out value="${productOption.optionCode}"/>_<c:out value="${productOption.index}"/>_<c:out value="${prodOptValue.index}"/>"
                  name="<c:out value="${productOption.optionCode}"/>-option_values_${product.id}_<c:out value="${productOption.index}"/>" 
                  value="<c:out value="${prodOptValue.index}"/>"
          	      <c:if test="${prodOptValue.index == optValue}">checked="checked"</c:if>/>
        </div> 
      </div>
      <c:if test="${(povStatus.index+1) % 3 == 0}">
       <br/>
      </c:if>
    </c:forEach>
    <div style="clear: both;"></div> 
    <div class="optionLink">
      <a href="${_contextpath}/category/53/detailed-style-description.html" rel="width:1000,height:750" id="mb0" class="mbCategory">Click here for detailed style description</a>
    </div>
    </div>
  </div>
<!-- Step 1 Ends -->


</div>
</div>
<div id="summaryBoxId">
  <div id="summaryTitle">Summary</div>
  <div id="summaryContent"> 
    <div id="summaryText">Your pricing will be created in this Summary box as you choose options in the order form.</div>
    
    <c:if test="${model.newPrice != null and model.newPrice > 0}">
    <span id="summaryPrice"> Price : $ 
      <c:out value="${model.newPrice}"></c:out>
    </span> <br/>
    </c:if>
    <c:forEach items="${model.selectedOptions}" var="selectedOption">
      <span> <c:out value="${selectedOption}"></c:out> </span> <br/>
    </c:forEach>
  
  </div>

</div>

<div style="clear: both;"></div> 
</c:if>
</div>