<%@ page import="com.webjaguar.model.*, java.text.NumberFormat, java.util.*" %>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>

<%
 NumberFormat nf = NumberFormat.getInstance( );
 nf.setMaximumFractionDigits(2);
 nf.setMinimumFractionDigits(2);
 Map<String, Object> model = (HashMap<String, Object>) request.getAttribute("model");
 Cart cart = (Cart) model.get("cart");
 pageContext.setAttribute("cartSubTotal", nf.format(cart.getSubTotal()));
%>

<tiles:insertDefinition name="${_template}" flush="true">
  <tiles:putAttribute name="content" type="string">
<c:if test="${siteConfig['BUY_SAFE_URL'].value != ''}">
<script type="text/javascript">
<!--
function buySAFEOnClick(NewWantsBondValue) {
	document.cart.WantsBondField.value = NewWantsBondValue;
    document.cart.submit();
}
//-->
</script>
</c:if>

<script src="${_contextpath}/javascript/overlay.js" type="text/javascript" ></script>
<script src="${_contextpath}/javascript/multibox.js" type="text/javascript" ></script> 
<link rel="stylesheet" href="${_contextpath}/assets/SmoothGallery/multibox.css" type="text/css" media="screen" />
<script type="text/javascript">
<!--
window.addEvent('domready', function(){
	var imageBox = new multiBox('imageMb',  {showNumbers:false, showControls:false, initialWidth:710, overlay: new overlay({opacity:'0.3'})});
});

function toggleChildSkus(skuGroup) {
	var show = false;
	$$('.'+skuGroup).each(function(ele){
		if(ele.style.display == ''){
			ele.style.display= "none";
			show = true;
			$('toggleBoxId'+skuGroup).set('text', '<fmt:message key="details" />');
		} else {
			ele.style.display= "";
			show = false;
			$('toggleBoxId'+skuGroup).set('text', '<fmt:message key="hide" />');
		}
	});
	if(!show){
		$('parentSkuTotalQtyHdr'+skuGroup).style.display = 'none';
		$('parentSkuTotalPriceHdr'+skuGroup).style.display = 'none';
		$('parentSkuTotalQtyFtr'+skuGroup).style.display = '';
		$('parentSkuTotalPriceFtr'+skuGroup).style.display = '';
	} else {
		$('parentSkuTotalQtyHdr'+skuGroup).style.display = '';
		$('parentSkuTotalPriceHdr'+skuGroup).style.display = '';
		$('parentSkuTotalQtyFtr'+skuGroup).style.display = 'none';
		$('parentSkuTotalPriceFtr'+skuGroup).style.display = 'none';
	}
}
//-->
</script>

<c:if test="${siteConfig['MINI_CART'].value == 'true'}">
<script type="text/javascript">
parent.showCart();
</script>
</c:if>
<div class="cartWrapper" id="cartWrapperId" style="width:100%;margin:auto;" class="cartWrapper" id="cartWrapperId">
<div style="float:right;" class="shoppingcart_continue_shopping_div">
<c:choose>
<c:when test="${model.continueShoppingUrl != null and !empty model.continueShoppingUrl and model.continueShoppingUrl != '/home.jhtm?null'}"><a class="shoppingcart_continue_shopping" href="<c:out value='${model.continueShoppingUrl}' />"><fmt:message key="continueShopping" /></a></c:when>
<c:otherwise><a href="javascript:history.back(1);"><fmt:message key="continueShopping" /></a></c:otherwise>
</c:choose>
</div><br />

<c:if test="${fn:trim(model.cartLayout.headerHtml) != ''}">
  <c:set value="${model.cartLayout.headerHtml}" var="headerHtml"/>
  <c:set value="${fn:replace(headerHtml, '#cartsubtotal#', cartSubTotal)}" var="headerHtml"/>
  <div><c:out value="${headerHtml}" escapeXml="false"/></div>
</c:if>

<c:if test="${!empty model.message}">
  <div class="message"><fmt:message key="${model.message}" /></div>
</c:if>
<c:if test="${!empty model.cart.minOrderMessage}">
  <div class="message"><fmt:message key="${model.cart.minOrderMessage}" /> <fmt:message key="${siteConfig['CURRENCY'].value}" /><fmt:formatNumber value="${model.cart.minOrderAmt}" pattern="#,##0.00"/></div>
</c:if>
<c:if test="${!empty model.cart.minIncMessage}">
  <div class="message"><fmt:message key="${model.cart.minIncMessage}" /></div>
</c:if>

<c:if test="${model.cart.numberOfItems == 0}">
  <div class="message"><b><fmt:message key="shoppingcart.empty" /></b></div><br/><br/>
</c:if>

<c:if test="${model.cart.numberOfItems > 0}">

<form action="viewCart.jhtm" method="post" name="cart">
<input type="hidden" value="Update" name="_update">
<table class="brandInfo" border="0" cellpadding="2" cellspacing="0" width="100%">
  <c:if test="${model.isUnderManufactureMin}">
    <tr>
      <td class="brandInfoHeader">To continue checking out, order a minimum amount from each Brand.(See Brand Column) </td>
    </tr>
  </c:if>
<tr><td> 
<c:forEach items="${model.cart.cartItems}" var="cartItem" varStatus="cartStatus">
<c:if test="${model.cart.hasManufacturer and cartItem.manufacturer.minOrder > cartItem.manufacturer.accumulator}"> 	
  <c:if test="${(cartStauts.index!=0) and (model.cart.cartItems[cartStatus.index].product.manufactureName!=model.cart.cartItems[cartStatus.index-1].product.manufactureName)}">
      <div class="brandInfoBox" style="float:left;">
        <b><fmt:message key="brand"></fmt:message>: <c:out value="${cartItem.product.manufactureName}"/></b><br>
      	   <u>Minimum <fmt:message key="${siteConfig['CURRENCY'].value}" /><fmt:formatNumber value="${cartItem.manufacturer.minOrder}" pattern="#,##0.00"/></u><br/>
      	   <u>Current <span><fmt:message key="${siteConfig['CURRENCY'].value}" /><fmt:formatNumber value="${cartItem.manufacturer.accumulator}" pattern="#,##0.00"/></span></u>
      </div>
  </c:if>
</c:if>
</c:forEach>
</td></tr>
</table>

<input type="hidden" name="WantsBondField" value="${model.cart.wantsBond}">
<c:set var="previousItemGroup"/>
<c:set var="nextItemGroup"/>
<c:set var="previousItemIndex" value="0"/>
<c:set var="currentItemIndex"/>
<c:set var="cols" value="0"/>
    
<c:forEach items="${model.cart.cartItems}" var="cartItem" varStatus="cartStatus">
<c:choose>
  <c:when test="${cartItem.index > 0}"><c:set var="cartItemIndex" value="${cartItem.index}"/></c:when>
  <c:otherwise><c:set var="cartItemIndex" value="${cartStatus.index}"/></c:otherwise>
</c:choose>

<c:set var="productLink">${_contextpath}/product.jhtm?id=${cartItem.product.id}</c:set>
<c:if test="${gSiteConfig['gMOD_REWRITE'] == '1' and not fn:contains(cartItem.product.sku, '/')}">
  <c:set var="productLink">${_contextpath}/${siteConfig['MOD_REWRITE_PRODUCT'].value}/${cartItem.product.encodedSku}/${cartItem.product.encodedName}.html</c:set>
</c:if>	

<c:if test="${cartStatus.first}">
<table border="0" cellpadding="2" cellspacing="0" width="100%" class="shoppingCart">
  <tr class="shoppingCart">
    <th class="cartNameHeader"><fmt:message key="product" /></th>
    <th class="cartQtyHeader"><fmt:message key="qty" /></th>
    <c:if test="${siteConfig['CASE_CONTENT_LAYOUT'].value == ''}">
	    <c:if test="${model.cart.hasPacking}">
	      <c:set var="cols" value="${cols+1}"/>
	      <th><fmt:message key="packing" /></th>
	    </c:if>
	    <c:if test="${model.cart.hasContent}">
	      <c:set var="cols" value="${cols+1}"/>
	      <th><fmt:message key="content" /></th>
	    </c:if>
    </c:if>
    <c:choose>
      <c:when test="${siteConfig['CASE_CONTENT_LAYOUT'].value == ''}">
          <th class="cartPriceHeader"><div><fmt:message key="price" /><c:if test="${model.cart.hasContent}">/<c:out value="${siteConfig['CASE_UNIT_TITLE'].value}" /></c:if></div></th>
      </c:when>
      <c:otherwise>
          <th class="cartPriceHeader"><div><fmt:message key="price" /></div></th>
      </c:otherwise>
    </c:choose>
    <c:if test="${model.cart.hasParentDeal == 'true'}">
    <c:choose>
      <c:when test="${siteConfig['CASE_CONTENT_LAYOUT'].value == ''}">
          <th class="cartPriceHeader"><div><fmt:message key="originalPrice" /><c:if test="${model.cart.hasContent}">/<c:out value="${siteConfig['CASE_UNIT_TITLE'].value}" /></c:if></div></th>
      </c:when>
      <c:otherwise>
          <th class="cartPriceHeader"><div><fmt:message key="originalPrice" /></div></th>
      </c:otherwise>
    </c:choose>
    <c:set var="cols" value="${cols+1}"/>
    <c:choose>
      <c:when test="${siteConfig['CASE_CONTENT_LAYOUT'].value == ''}">
          <th class="cartPriceHeader"><div><fmt:message key="discount" /><c:if test="${model.cart.hasContent}">/<c:out value="${siteConfig['CASE_UNIT_TITLE'].value}" /></c:if></div></th>
      </c:when>
      <c:otherwise>
          <th class="cartPriceHeader"><div><fmt:message key="discount" /></div></th>
      </c:otherwise>
    </c:choose>
     <c:set var="cols" value="${cols+1}"/>
    </c:if>
    <c:set var="subTotalColumn" value="${cols}"></c:set>
    <th class="cartTotalHeader"><div><fmt:message key="total" /></div></th>
    <c:if test="${model.brandsMap != null or model.budgetProduct != null}">	
      <c:set var="cols" value="${cols+1}"/>
      <th align="right"><fmt:message key="budget" /></th>
    </c:if>
    <c:if test="${model.cart.hasManufacturer}">	
      <c:set var="cols" value="${cols+1}"/>
      <th align="center"><fmt:message key="brand" /></th>
    </c:if>
    <c:set var="updateColumn" value="${cols}"></c:set>
    <th><fmt:message key="remove" /></th>
  </tr>
  <c:if test="${model.isOverBudget}">
  <tr class="shoppingCart${cartStatus.index % 2}">
      <td colspan="${5+cols}" align="center"><div style="color:#EE0000;font-weight:bold;">You have orders that exceed your budget.</div></td>
  </tr>
  </c:if>
</c:if>
  
  <!-- Set the cartItem Group and check if the current item and previous item has the same group  -->
  <c:choose>
    <c:when test="${cartItem.itemGroup != null}">
      <c:choose>
        <c:when test="${previousItemGroup != null && previousItemGroup == cartItem.itemGroup}">
          <c:set var="currentItemIndex" value="${previousItemIndex}"/>
        </c:when>
        <c:otherwise>
          <c:set var="currentItemIndex" value="${previousItemIndex + 1}"/>
          <c:set var="displaySkuAndName" value="${true}"/>
          <%@ include file="/WEB-INF/jsp/frontend/subCart.jsp" %>
          <c:set var="displaySkuAndName" value=""/>
          <tr class="shoppingCart${currentItemIndex % 2}">
          <td colspan="${5+cols}">
            <div onclick="toggleChildSkus(${cartItem.itemGroup});" id="toggleBoxId${cartItem.itemGroup}" class="toggleChildItemsButton">
              <fmt:message key="hide" />
            </div>
          </td>
          </tr>
        </c:otherwise>
      </c:choose>
    </c:when>
    <c:otherwise>
      <c:set var="currentItemIndex" value="${previousItemIndex + 1}"/>
    </c:otherwise>
  </c:choose>
  <c:set var="previousItemIndex" value="${currentItemIndex}"/>
  <c:set var="previousItemGroup" value="${cartItem.itemGroup}"/>
  <c:set var="nextItemGroup" value="${model.cart.cartItems[cartStatus.index +1].itemGroup}"/>
  
  <tr valign="top" class="shoppingCart${currentItemIndex % 2}  ${cartItem.itemGroup}" >
    <td class="cartName" valign="top" style="margin-left: 5px;">
      <c:if test="${siteConfig['SHOW_IMAGE_ON_INVOICE'].value == 'true' and cartItem.product.thumbnail != null and (cartItem.itemGroup == null or !cartItem.itemGroupMainItem)}">
        <div class="cartImageWrapper">
          <a href="<c:if test="${!cartItem.product.thumbnail.absolute}">${_contextpath}/assets/Image/Product/thumb/</c:if>${cartItem.product.thumbnail.imageUrl}" rel="width:600,height:515" id="imageMb${cartStatus.index}" class="imageMb shoppingcart_item_image">
            <img class="cartImage" src="<c:if test="${!cartItem.product.thumbnail.absolute}">${_contextpath}/assets/Image/Product/thumb/</c:if>${cartItem.product.thumbnail.imageUrl}" border="0" />
          </a>
        </div>
      </c:if>
      <c:if test="${cartItem.customXml != null}">
    	<div class="shoppingcart_custom_name">Custom Frame - <c:out value="${cartItem.product.name}"/></div>
		<c:if test="${cartItem.customImageUrl != null}">
      	  <a href="framer/pictureframer/images/FRAMED/${cartItem.customImageUrl}" onclick="window.open(this.href,'','width=640,height=480,resizable=yes'); return false;"><img class="cartImage" src="framer/pictureframer/images/FRAMED/${cartItem.customImageUrl}" border="0" /></a>
    	</c:if>
      </c:if>
      <c:if test="${cartItem.customXml == null}">
    	<div class="cartItemSkuNameWrapper">
    	  <a  href="${productLink}" class="shoppingcart_item_name"><c:out value="${cartItem.product.name}" escapeXml="false"/></a>
		  <br/>
		  <c:if test="${siteConfig['SHOW_SKU'].value == 'true' and cartItem.product.sku != null}">
	  	    <a  href="${productLink}" class="shoppingcart_item_sku"><c:out value="${cartItem.product.sku}"/></a>
		  </c:if>
    	</div>
	  </c:if>
	  <!-- clear float, if any -->
	  <div style="clear: both;" class="clear"></div>  
	  <c:if test="${cartItem.attachment != null}">
        Imprint:
        <c:choose>
          <c:when test="${model.cart.userId != null}">
	        <a href="${_contextpath}/temp/Cart/customer/${model.cart.userId}/${cartItem.attachment}" rel="width:600,height:515" id="imageMb${status.index}" class="imageMb">
              <!-- 
              <img class="cartImage" src="${_contextpath}/temp/Cart/customer/${model.cart.userId}/${cartItem.attachment}" border="0" />
          	   -->
          	   <c:out value="${cartItem.attachment}"/>
      	    </a>
          </c:when>
          <c:otherwise>
            <a href="${_contextpath}/temp/Cart/session/${model.sessionId}/${cartItem.attachment}" rel="width:600,height:515" id="imageMb${status.index}" class="imageMb">
    	      <!-- 
              <img class="cartImage" src="${_contextpath}/temp/Cart/session/${model.sessionId}/${cartItem.attachment}" border="0" />
              -->
              <c:out value="${cartItem.attachment}"/>
      	    </a>
          </c:otherwise>
        </c:choose>
      </c:if>
	  <c:if test="${gSiteConfig['gMINIMUM_INCREMENTAL_QTY'] and cartItem.showMinIncMessage}">
        <c:set value="${siteConfig['MINIMUM_MESSAGE'].value}" var="minMessage"/>
        <c:set value="${fn:replace(minMessage, '#min#', cartItem.product.minimumQty)}" var="minMessage"/>
        <c:set value="${siteConfig['INCREMENTAL_MESSAGE'].value}" var="incMessage"/>
        <c:set value="${fn:replace(incMessage, '#inc#', cartItem.product.incrementalQty)}" var="incMessage"/>
        <c:if test="${cartItem.priceCasePackQty == null}">
          <div id="minIncMessage"><c:if test="${!empty cartItem.product.minimumQty}" >&nbsp; <c:out value="${minMessage}" escapeXml="false"/></c:if><c:if test="${!empty cartItem.product.incrementalQty}" ><c:out value="${incMessage}" escapeXml="false"/></c:if></div>
        </c:if>
      </c:if>
	  <!-- Do not display attributes only if cartItem is child sku and parent sku is displayed on the cart  -->
	  <c:if test="${(cartItem.productAttributes != null  or cartItem.asiProductAttributes != null) and (cartItem.itemGroup == null or !cartItem.itemGroupMainItem)}">
    	<table border="0" cellspacing="1" cellpadding="0">
	  	  <c:forEach items="${cartItem.productAttributes}" var="productAttribute" varStatus="productAttributeStatus">
			<tr class="shoppingCartOption${productAttributeStatus.count%2}">
			  <td class="optionName" style="white-space: nowrap" align="right"><c:out value="${productAttribute.optionName}"/>: </td>
			  <td class="optionValue" style="padding-left:5px;"><c:out value="${productAttribute.valueString}" escapeXml="false"/></td>
			  <c:if test="${(! empty productAttribute.optionPriceOriginal) && productAttribute.optionPriceOriginal != 0}">
			  <td class="optionPrice"><fmt:formatNumber value="${productAttribute.optionPriceOriginal}" pattern="#,##0.00" /></td>
			  <td class="optionNote" style="padding-left:5px;"><fmt:message key="${productAttribute.optionPriceMessageF}"/></td>
			  </c:if>
			</tr>
	  	  </c:forEach>
	  	  <c:forEach items="${cartItem.asiProductAttributes}" var="asiProductAttribute" varStatus="asiProductAttributeStatus">
			<tr class="shoppingCartOption${asiProductAttributeStatus.count%2}">
			  <td class="optionName" style="white-space: nowrap" align="right"><c:out value="${asiProductAttribute.asiOptionName}"/>: </td>
			  <td class="optionValue" style="padding-left:5px;"><c:out value="${asiProductAttribute.asiOptionValue}" escapeXml="false"/></td>
			</tr>
	  	  </c:forEach>
		</table>  		
	  </c:if>
	  <c:if test="${cartItem.subscriptionInterval != null}">
		<c:set var="intervalType" value="${fn:substring(cartItem.subscriptionInterval, 0, 1)}"/>
		<c:set var="intervalUnit" value="${fn:substring(cartItem.subscriptionInterval, 1, 2)}"/>
		<div style="color: #EE0000;"><fmt:message key="subscription"/> <fmt:message key="details"/>: <fmt:message key="delivery"/>
	      <c:choose>
	    	<c:when test="${intervalUnit == '1'}"><fmt:message key="every_${intervalType}"><fmt:param value="${intervalUnit}"/></fmt:message></c:when>
	    	<c:otherwise><fmt:message key="every_${intervalType}s"><fmt:param value="${intervalUnit}"/></fmt:message></c:otherwise>
	  	  </c:choose>
	  	  <c:if test="${cartItem.product.subscriptionDiscount != null}">
	    	(Save <fmt:formatNumber value="${cartItem.product.subscriptionDiscount}" pattern="#,##0.00" />%)
	  	  </c:if>
		</div>  					
	  </c:if>
	  <c:if test="${siteConfig['CASE_CONTENT_LAYOUT'].value == '2' and cartItem.priceCasePackQty != null}">
	  	<div><c:out value="${siteConfig['PRICE_CASE_PACK_UNIT_TITLE'].value}" /> Qty: <c:out value="${cartItem.priceCasePackQty}" /></div>
	  </c:if>
	</td>
    <td class="cartQty" style="align:left; padding-left:10px ">
      <input type="hidden" name="index" value="<c:out value="${cartItem.index}"/>">
      <input type="hidden" name="pid_${cartItem.index}" value="${cartItem.product.id}">
      <table>
      <c:choose>
        <c:when test="${gSiteConfig['gVARIABLE_PRICE'] and cartItem.variableUnitPrice != null}">
          <tr><td><input type="hidden" name="qty_${cartItemIndex}" value="1"></td></tr>
	    </c:when>
        <c:when test="${cartItem.customLines != null}">
        <tr>
          <td <c:if test="${!empty cartItem.inventoryMessage or !empty cartItem.minimumIncrementalQtyMessage}">class="errorQty" </c:if>><input type="hidden" name="qty_${cartItemIndex}" value="<c:out value="${cartItem.quantity}"/>"><c:out value="${cartItem.quantity}"/></td>
          <td valign="top"><c:if test="${!empty cartItem.inventoryMessage or !empty cartItem.minimumIncrementalQtyMessage}"><div class="errorQty" style="color:#FF3366;">*</div></c:if></td>
	    </tr>
	    </c:when>
	    <c:otherwise>
	    <tr>
	      <td <c:if test="${!empty cartItem.inventoryMessage or !empty cartItem.minimumIncrementalQtyMessage}">class="errorQty" </c:if>><input type="text" size="4" maxlength="5" name="qty_${cartItemIndex}" value="<c:out value="${cartItem.quantity}"/>"  <c:if test="${cartItem.asiProductAttributes != null or (cartItem.product.feed == 'ASI') or (cartItem.itemGroup != null and !cartItem.itemGroupMainItem)}">readonly="readonly"</c:if>></td>
	      <td valign="top"><c:if test="${!empty cartItem.inventoryMessage or !empty cartItem.minimumIncrementalQtyMessage}"><div class="errorQty" style="color:#FF3366;">*</div></c:if></td>
	    </tr>
	    <c:if test="${gSiteConfig['gPRICE_CASEPACK'] and cartItem.priceCasePackQty != null}">
	    <tr>
	      <td colspan="2"><input type="checkbox" name="casePacing" checked="checked" disabled="disabled"/><c:out value="${siteConfig['PRICE_CASE_PACK_UNIT_TITLE'].value}" /></td>
	    </tr>
	    </c:if>
	    </c:otherwise>  
	  </c:choose>
	  </table>
	</td>
	<c:choose>
	<c:when test="${siteConfig['CASE_CONTENT_LAYOUT'].value == '1'}">
		<td class="cartPrice" align="center">   
	      <c:choose>
	        <c:when test="${gSiteConfig['gVARIABLE_PRICE'] and cartItem.variableUnitPrice != null}">
	          <div><input type="text" size="4" maxlength="5" name="_price_by_user_${cartItemIndex}" value="<fmt:formatNumber value='${cartItem.variableUnitPrice}' pattern="#,##0.00"/>" /></div>
	        </c:when>
	        <c:otherwise>	
	        <c:choose>
	         <c:when test="${cartItem.product.caseContent == null}">	
		      <div><fmt:message key="${siteConfig['CURRENCY'].value}" /><fmt:formatNumber value="${cartItem.unitPrice}" pattern="#,##0.00#" maxFractionDigits="${siteConfig['MAX_FRACTION_DIGIT'].value}"/></div>
		    </c:when>
		    <c:otherwise>
		      <div><fmt:message key="${siteConfig['CURRENCY'].value}" /><fmt:formatNumber value="${cartItem.unitPrice * cartItem.product.caseContent}" pattern="#,##0.00#" maxFractionDigits="${siteConfig['MAX_FRACTION_DIGIT'].value}"/></div>
		    </c:otherwise>
		    </c:choose>
		    </c:otherwise>
	      </c:choose> 
	    </td>
    </c:when>
    <c:when test="${siteConfig['CASE_CONTENT_LAYOUT'].value == '2'}">
    	<td class="cartPrice" align="center">   
    	  <c:if test="${cartItem.priceCasePackQty != null}">
	      	<div><fmt:message key="${siteConfig['CURRENCY'].value}" /><fmt:formatNumber value="${cartItem.unitPrice * cartItem.priceCasePackQty}" pattern="#,##0.00#" maxFractionDigits="${siteConfig['MAX_FRACTION_DIGIT'].value}"/></div> 
	      </c:if>
	      <c:if test="${cartItem.priceCasePackQty == null}">
	      	<div><fmt:message key="${siteConfig['CURRENCY'].value}" /><fmt:formatNumber value="${cartItem.unitPrice}" pattern="#,##0.00#" maxFractionDigits="${siteConfig['MAX_FRACTION_DIGIT'].value}"/></div> 
	      </c:if>
	    </td>
    </c:when>
    <c:otherwise> 
			<c:if test="${model.cart.hasPacking}">
				<td align="center"><c:out value="${cartItem.product.packing}"/></td>
			</c:if>
			<c:if test="${model.cart.hasContent}">
			  <td align="center"><c:choose><c:when test="${empty cartItem.product.packing and !empty cartItem.product.caseContent}"> X </c:when><c:otherwise></c:otherwise></c:choose><c:out value="${cartItem.product.caseContent}"/></td>
			</c:if>
		    <td class="cartPrice" align="center">
		      <c:choose>
		        <c:when test="${gSiteConfig['gVARIABLE_PRICE'] and cartItem.variableUnitPrice != null}">
		          <div><input type="text" size="4" maxlength="5" name="_price_by_user_${cartItemIndex}" value="<fmt:formatNumber value='${cartItem.variableUnitPrice}' pattern="#,##0.00"/>" /></div>
		        </c:when>
		        <c:otherwise>
		          <div><fmt:message key="${siteConfig['CURRENCY'].value}" /><fmt:formatNumber value="${cartItem.unitPrice}" pattern="#,##0.00#" maxFractionDigits="${siteConfig['MAX_FRACTION_DIGIT'].value}"/></div>
		        </c:otherwise>
		      </c:choose>
		    </td>
    </c:otherwise>
    </c:choose>
    <c:if test="${model.cart.hasParentDeal == 'true'}">
    <c:choose>
	<c:when test="${siteConfig['CASE_CONTENT_LAYOUT'].value == '1'}">
		<td class="cartPrice" align="center">   
	      <c:choose>
	        <c:when test="${gSiteConfig['gVARIABLE_PRICE'] and cartItem.variableUnitPrice != null}">
	          <div><input type="text" size="4" maxlength="5" name="_price_by_user_${cartItemIndex}" value="<fmt:formatNumber value='${cartItem.variableUnitPrice}' pattern="#,##0.00"/>" /></div>
	        </c:when>
	        <c:otherwise>	
	        <c:choose>
	         <c:when test="${cartItem.product.caseContent == null}">	
		      <div><fmt:message key="${siteConfig['CURRENCY'].value}" /><fmt:formatNumber value="${cartItem.originalPrice}" pattern="#,##0.00#" maxFractionDigits="${siteConfig['MAX_FRACTION_DIGIT'].value}"/></div>
		    </c:when>
		    <c:otherwise>
		      <div><fmt:message key="${siteConfig['CURRENCY'].value}" /><fmt:formatNumber value="${cartItem.originalPrice * cartItem.product.caseContent}" pattern="#,##0.00#" maxFractionDigits="${siteConfig['MAX_FRACTION_DIGIT'].value}"/></div>
		    </c:otherwise>
		    </c:choose>
		    </c:otherwise>
	      </c:choose> 
	    </td>
    </c:when>
    <c:when test="${siteConfig['CASE_CONTENT_LAYOUT'].value == '2'}">
    	<td class="cartPrice" align="center">   
    	  <c:if test="${cartItem.priceCasePackQty != null}">
	      	<div><fmt:message key="${siteConfig['CURRENCY'].value}" /><fmt:formatNumber value="${cartItem.originalPrice * cartItem.priceCasePackQty}" pattern="#,##0.00#" maxFractionDigits="${siteConfig['MAX_FRACTION_DIGIT'].value}"/></div> 
	      </c:if>
	      <c:if test="${cartItem.priceCasePackQty == null}">
	      	<div><fmt:message key="${siteConfig['CURRENCY'].value}" /><fmt:formatNumber value="${cartItem.originalPrice}" pattern="#,##0.00#" maxFractionDigits="${siteConfig['MAX_FRACTION_DIGIT'].value}"/></div> 
	      </c:if>
	    </td>
    </c:when>
    <c:otherwise> 
		    <td class="cartPrice" align="center">
		      <c:choose>
		        <c:when test="${gSiteConfig['gVARIABLE_PRICE'] and cartItem.variableUnitPrice != null}">
		          <div><input type="text" size="4" maxlength="5" name="_price_by_user_${cartItemIndex}" value="<fmt:formatNumber value='${cartItem.variableUnitPrice}' pattern="#,##0.00"/>" /></div>
		        </c:when>
		        <c:otherwise>
		          <div><fmt:message key="${siteConfig['CURRENCY'].value}" /><fmt:formatNumber value="${cartItem.originalPrice}" pattern="#,##0.00#" maxFractionDigits="${siteConfig['MAX_FRACTION_DIGIT'].value}"/></div>
		        </c:otherwise>
		      </c:choose>
		    </td>
    </c:otherwise>
    </c:choose>
    <c:choose>
	<c:when test="${siteConfig['CASE_CONTENT_LAYOUT'].value == '1'}">
		<td class="cartPrice" align="center">   
	      <c:choose>
	        <c:when test="${gSiteConfig['gVARIABLE_PRICE'] and cartItem.variableUnitPrice != null}">
	          <div><input type="text" size="4" maxlength="5" name="_price_by_user_${cartItemIndex}" value="<fmt:formatNumber value='${cartItem.variableUnitPrice}' pattern="#,##0.00"/>" /></div>
	        </c:when>
	        <c:otherwise>	
	        <c:choose>
	         <c:when test="${cartItem.product.caseContent == null}">	
		      <div><fmt:message key="${siteConfig['CURRENCY'].value}" /><fmt:formatNumber value="${cartItem.discount}" pattern="#,##0.00#" maxFractionDigits="${siteConfig['MAX_FRACTION_DIGIT'].value}"/></div>
		    </c:when>
		    <c:otherwise>
		      <div><fmt:message key="${siteConfig['CURRENCY'].value}" /><fmt:formatNumber value="${cartItem.discount * cartItem.product.caseContent}" pattern="#,##0.00#" maxFractionDigits="${siteConfig['MAX_FRACTION_DIGIT'].value}"/></div>
		    </c:otherwise>
		    </c:choose>
		    </c:otherwise>
	      </c:choose> 
	    </td>
    </c:when>
    <c:when test="${siteConfig['CASE_CONTENT_LAYOUT'].value == '2'}">
    	<td class="cartPrice" align="center">   
    	  <c:if test="${cartItem.priceCasePackQty != null}">
	      	<div><fmt:message key="${siteConfig['CURRENCY'].value}" /><fmt:formatNumber value="${cartItem.discount * cartItem.priceCasePackQty}" pattern="#,##0.00#" maxFractionDigits="${siteConfig['MAX_FRACTION_DIGIT'].value}"/></div> 
	      </c:if>
	      <c:if test="${cartItem.priceCasePackQty == null}">
	      	<div><fmt:message key="${siteConfig['CURRENCY'].value}" /><fmt:formatNumber value="${cartItem.discount}" pattern="#,##0.00#" maxFractionDigits="${siteConfig['MAX_FRACTION_DIGIT'].value}"/></div> 
	      </c:if>
	    </td>
    </c:when>
    <c:otherwise> 
		    <td class="cartPrice" align="center">
		      <c:choose>
		        <c:when test="${gSiteConfig['gVARIABLE_PRICE'] and cartItem.variableUnitPrice != null}">
		          <div><input type="text" size="4" maxlength="5" name="_price_by_user_${cartItemIndex}" value="<fmt:formatNumber value='${cartItem.variableUnitPrice}' pattern="#,##0.00"/>" /></div>
		        </c:when>
		        <c:otherwise>
		          <div><fmt:message key="${siteConfig['CURRENCY'].value}" /><fmt:formatNumber value="${cartItem.discount}" pattern="#,##0.00#" maxFractionDigits="${siteConfig['MAX_FRACTION_DIGIT'].value}"/></div>
		        </c:otherwise>
		      </c:choose>
		    </td>
    </c:otherwise>
    </c:choose>
    </c:if>
    <td class="cartTotal" align="center">
      <c:choose>
        <c:when test="${gSiteConfig['gVARIABLE_PRICE'] and cartItem.variableUnitPrice != null}">
           <div><fmt:message key="${siteConfig['CURRENCY'].value}" /><fmt:formatNumber value="${cartItem.totalVariablePrice}" pattern="#,##0.00"/></div></c:when>
        <c:otherwise>
          <div><fmt:message key="${siteConfig['CURRENCY'].value}" /><fmt:formatNumber value="${cartItem.total}" pattern="#,##0.00"/></div>        
        </c:otherwise>
      </c:choose>
    </td>
    <c:if test="${model.brandsMap != null or model.budgetProduct != null}"> 	
      <td align="right" nowrap>
        <c:if test="${cartItem.brand != null}">
      	<fmt:message key="${siteConfig['CURRENCY'].value}" /><fmt:formatNumber value="${cartItem.brand.balance}" pattern="#,##0.00"/><br/>
	    <u>-<fmt:formatNumber value="${model.brandsMap[cartItem.brand]}" pattern="#,##0.00" /></u><br/>
	    <c:set var="balance" value="${cartItem.brand.balance - model.brandsMap[cartItem.brand]}"/>
	    <span <c:if test="${balance < 0}">style="color:#EE0000;font-weight:bold;"</c:if>><fmt:formatNumber value="${balance}" pattern="#,##0.00"/></span><br/>
	    </c:if>
        <c:if test="${model.budgetProduct != null}">
      	<c:out value="${model.budgetProduct[cartItem.product.sku].balance}"/><br/>
      	<u>-<c:out value="${model.budgetProduct[cartItem.product.sku].qty}"/></u><br/>
      	<c:set var="balance" value="${model.budgetProduct[cartItem.product.sku].balance - model.budgetProduct[cartItem.product.sku].qty}"/>
      	<span <c:if test="${balance < 0}">style="color:#EE0000;font-weight:bold;"</c:if>><c:out value="${balance}"/></span><br/>      	
	    </c:if>	    
	    &nbsp;
      </td>
    </c:if>
    <c:if test="${model.cart.hasManufacturer}"> 	
      <td align="right">
        <span class="brandName"><c:out value="${cartItem.product.manufactureName}"/></span>
      </td>
    </c:if>
    <td align="center"><c:if test="${!cartItem.dealMainItem and (cartItem.itemGroup == null or (cartItem.itemGroup != null and cartItem.itemGroupMainItem))}"><input type="checkbox" name="remove" value="${cartItemIndex}"></c:if></td>
  </tr>

  <c:if test="${(cartItem.numOfPrices > 1  or cartItem.product.salesTag.tagId != null) and (cartItem.itemGroup == null) }">
  <tr class="shoppingCart${currentItemIndex % 2}">
      <td colspan="${5+cols}"> 
      	<c:if test="${cartItem.numOfPrices == 1}" >
		  <c:forEach items="${cartItem.product.price}" var="price">
		    <c:set value="${price.amt}" var="originalPrice"/>
		  </c:forEach>
		</c:if>
		<c:if test="${cartItem.numOfPrices > 1}">
	    <table border="0" class="priceBreak">
		  <c:forEach items="${cartItem.product.price}" var="price" varStatus="statusPrice">
		  <c:set var="currentQtyBreak" value="false"/>
		  <c:if test="${(cartItem.crossItemsQty >= price.qtyFrom) && (cartItem.crossItemsQty <= price.qtyTo || price.qtyTo == null)}">
		    <c:set var="currentQtyBreak" value="true"/>
		  </c:if>
		  <tr>
		    <td <c:if test="${currentQtyBreak}">class="shoppingcart_current_qtyBreak"</c:if>>
		      <c:if test="${cartItem.numOfPrices != 1}" >
		      	${price.qtyFrom}<c:if test="${price.qtyTo != null}">-${price.qtyTo}</c:if><c:if test="${price.qtyTo == null}">+</c:if> <c:out value="${cartItem.product.packing}" />
		      </c:if>&nbsp;	
		    </td> 
		
		    <td <c:if test="${currentQtyBreak}">class="shoppingcart_current_qtyBreak"</c:if>
		    	<c:if test="${cartItem.product.endQtyPricing and cartItem.numOfPrices != 1 and cartItem.product.salesTag.tagId == null}">style="text-decoration: line-through"</c:if>>
		      <c:if test="${cartItem.numOfPrices != 1}" >
		        <c:set var="thisPrice" value="${price.amt + cartItem.totalPriceOption}"/>
		        <c:if test="${cartItem.product.salesTag.tagId != null}"><c:set var="thisPrice" value="${price.discountAmt}"/></c:if>
		        <fmt:message key="${siteConfig['CURRENCY'].value}" /><fmt:formatNumber value="${thisPrice}" pattern="#,##0.00#" maxFractionDigits="${siteConfig['MAX_FRACTION_DIGIT'].value}"/><c:if test="${cartItem.product.caseContent != null}">/<c:out value="${siteConfig['CASE_UNIT_TITLE'].value}" /></c:if>     
		      </c:if>
		    </td>
			<c:if test="${cartItem.product.endQtyPricing and cartItem.numOfPrices != 1 and cartItem.product.salesTag.tagId == null}">
		    <td <c:if test="${currentQtyBreak}">class="shoppingcart_current_qtyBreak"</c:if>>
		      <fmt:message key="${siteConfig['CURRENCY'].value}" /><fmt:formatNumber value="${cartItem.unitPrice}" pattern="#,##0.00#" maxFractionDigits="${siteConfig['MAX_FRACTION_DIGIT'].value}"/><c:if test="${cartItem.product.caseContent != null}">/<c:out value="${siteConfig['CASE_UNIT_TITLE'].value}" /></c:if>     
		    </td>	    
		    </c:if>	    
	
		    <c:if test="${model.cart.hasContent && cartItem.numOfPrices != 1 && (cartItem.product.caseContent != null)}">
		    <td <c:if test="${currentQtyBreak}">class="shoppingcart_current_qtyBreak"</c:if>>
		      (<c:out value="${cartItem.product.caseContent}" /> <c:out value="${siteConfig['CASE_UNIT_TITLE'].value}" />/<c:out value="${cartItem.product.packing}" />)
		    </td>
		    </c:if>
		    <td>
		      <c:if test="${currentQtyBreak}">
		        <img src="${_contextpath}/assets/Image/Layout/select_8x8.gif" border="0">
		        <c:set value="${price.amt}" var="originalPrice"/>
		      </c:if>&nbsp;
		    </td>
		  </tr>
		  </c:forEach>   
	    </table>
		</c:if>
  <c:if test="${cartItem.product.salesTag.tagId != null}" >
	  <table align="center" class="salesTagBox">
	   <tr  class="shoppingCart${currentItemIndex % 2}">
	    <c:if test="${cartItem.product.salesTag.image}" >
	  	<td>
	        <img align="right"  src="${_contextpath}/assets/Image/PromoSalesTag/salestag_${cartItem.product.salesTag.tagId}.gif" />  
	  	</td>
	  	</c:if>
	  	<td>
	  	<div class="discountedPrice">
		<c:choose>		
		  	<c:when test="${gSiteConfig['gPRICE_CASEPACK'] and siteConfig['CASE_CONTENT_LAYOUT'].value == '2' and cartItem.priceCasePackQty != null and cartItem.asiUnitPrice == null}">
		  	 <c:forEach items="${cartItem.product.price}" var="price">
		  	  <div class="originalPrice">
                <label><fmt:message key="shoppingcart.original.price" />: </label><h4><fmt:message key="${siteConfig['CURRENCY'].value}" /><fmt:formatNumber value="${(cartItem.quantity * cartItem.priceCasePackQty * price.caseAmt) + (cartItem.totalOriginalPriceOption + cartItem.boxExtraPrice)}" pattern="#,##0.00#" maxFractionDigits="${siteConfig['MAX_FRACTION_DIGIT'].value}"/> <c:set var = "price" value="${(cartItem.quantity * cartItem.priceCasePackQty * price.caseAmt) + (cartItem.totalOriginalPriceOption + cartItem.boxExtraPrice)}" /></h4>
		  	  </div>
		  	  <div style="font-size: 12px;	text-decoration: none; color: #EE0000; font-weight: bold;">
		  	    <label><fmt:message key="youSave" />: </label><h4><fmt:message key="${siteConfig['CURRENCY'].value}" /><fmt:formatNumber value="${price - discountedPrice}" pattern="#,##0.00#" maxFractionDigits="${siteConfig['MAX_FRACTION_DIGIT'].value}"/></h4>
		  	  </div>
		  	  <div class="discountPrice">
		  	    <label><fmt:message key="shoppingcart.discounted.price" />: </label><h4><fmt:message key="${siteConfig['CURRENCY'].value}" /><fmt:formatNumber value="${cartItem.quantity * cartItem.product.priceCasePackQty * cartItem.unitPrice}" pattern="#,##0.00#" maxFractionDigits="${siteConfig['MAX_FRACTION_DIGIT'].value}"/><c:set var = "discountedPrice" value="${cartItem.quantity * cartItem.product.priceCasePackQty * cartItem.unitPrice}" /></h4>
		  	  </div>
		  	  </c:forEach>
		  	</c:when>
		  	<c:when test="${gSiteConfig['gCASE_CONTENT'] and cartItem.product.caseContent != null and cartItem.asiUnitPrice == null}">
		  	 <c:forEach items="${cartItem.product.price}" var="price">
		  	  <div class="originalPrice">
                <label><fmt:message key="shoppingcart.original.price" />: </label><h4><fmt:message key="${siteConfig['CURRENCY'].value}" /><fmt:formatNumber value="${(cartItem.quantity * cartItem.product.caseContent * price.amt) + (cartItem.totalOriginalPriceOption + cartItem.boxExtraPrice)}" pattern="#,##0.00#" maxFractionDigits="${siteConfig['MAX_FRACTION_DIGIT'].value}"/> <c:set var = "price" value="${(cartItem.quantity *cartItem.product.caseContent * price.amt) + (cartItem.totalOriginalPriceOption + cartItem.boxExtraPrice)}" /></h4>
		  	  </div>
		  	  <div style="font-size: 12px;	text-decoration: none; color: #EE0000; font-weight: bold;">
		  	    <label><fmt:message key="youSave" />: </label><h4><fmt:message key="${siteConfig['CURRENCY'].value}" /><fmt:formatNumber value="${price - discountedPrice}" pattern="#,##0.00#" maxFractionDigits="${siteConfig['MAX_FRACTION_DIGIT'].value}"/></h4>
		  	  </div>
		  	  <div class="discountPrice">
		  	    <label><fmt:message key="shoppingcart.discounted.price" />: </label><h4><fmt:message key="${siteConfig['CURRENCY'].value}" /><fmt:formatNumber value="${cartItem.quantity * cartItem.product.caseContent * cartItem.unitPrice}" pattern="#,##0.00#" maxFractionDigits="${siteConfig['MAX_FRACTION_DIGIT'].value}"/><c:set var = "discountedPrice" value="${cartItem.quantity * cartItem.product.caseContent * cartItem.unitPrice}" /></h4>
		  	  </div>
		  	  </c:forEach>
		  	</c:when>	
		  	<c:when test="${cartItem.asiUnitPrice != null}">
		  	  <div class="originalPrice">
                <label><fmt:message key="shoppingcart.original.price" />: </label><h4><fmt:message key="${siteConfig['CURRENCY'].value}" /><fmt:formatNumber value="${cartItem.asiOriginalPrice}" pattern="#,##0.00#" maxFractionDigits="${siteConfig['MAX_FRACTION_DIGIT'].value}"/> <c:set var = "price" value="${cartItem.asiOriginalPrice}" /></h4>
		  	  </div>
		  	  <div style="font-size: 12px;	text-decoration: none; color: #EE0000; font-weight: bold;">
		  	    <label><fmt:message key="youSave" />: </label><h4><fmt:message key="${siteConfig['CURRENCY'].value}" /><fmt:formatNumber value="${price - cartItem.unitPrice}" pattern="#,##0.00#" maxFractionDigits="${siteConfig['MAX_FRACTION_DIGIT'].value}"/></h4>
		  	  </div>
		  	  <div class="discountPrice">
		  	    <label><fmt:message key="shoppingcart.discounted.price" />: </label><h4><fmt:message key="${siteConfig['CURRENCY'].value}" /><fmt:formatNumber value="${cartItem.unitPrice}" pattern="#,##0.00#" maxFractionDigits="${siteConfig['MAX_FRACTION_DIGIT'].value}"/><c:set var = "discountedPrice" value="${cartItem.unitPrice}" /></h4>
		  	  </div>
		  	</c:when>		  	
		  	<c:otherwise> 
				<div class="originalPrice">
                   <label><fmt:message key="shoppingcart.original.price" />: </label><h4><fmt:message key="${siteConfig['CURRENCY'].value}" /><fmt:formatNumber value="${originalPrice + (cartItem.totalOriginalPriceOption + cartItem.boxExtraPrice)}" pattern="#,##0.00#" maxFractionDigits="${siteConfig['MAX_FRACTION_DIGIT'].value}"/></h4>
                </div>
                <div style="font-size: 12px;height:21px;text-decoration: none; color: #EE0000; font-weight: bold;">
                   <label><fmt:message key="youSave" />: </label><h4><fmt:message key="${siteConfig['CURRENCY'].value}" /><fmt:formatNumber value="${originalPrice + (cartItem.totalOriginalPriceOption + cartItem.boxExtraPrice) - cartItem.unitPrice}" pattern="#,##0.00#" maxFractionDigits="${siteConfig['MAX_FRACTION_DIGIT'].value}"/></h4>
                </div>
                <div class="discountPrice">
                   <label><fmt:message key="shoppingcart.discounted.price" />: </label><h4><fmt:message key="${siteConfig['CURRENCY'].value}" /><fmt:formatNumber value="${cartItem.unitPrice}" pattern="#,##0.00#" maxFractionDigits="${siteConfig['MAX_FRACTION_DIGIT'].value}"/></h4>
                </div>
            </c:otherwise>
	  	</c:choose> 
	  	</div>  
	   	</td>
	  </tr>
	  </table>
  </c:if>
  
    </td>
  </tr>
  </c:if>
  <c:if test="${gSiteConfig['gINVENTORY'] and !empty cartItem.inventoryMessage}">
  <tr  class="shoppingCart${currentItemIndex % 2} ${cartItem.itemGroup}">
   <td class="shoppingCart_inventory_adjust" colspan="${5+cols}"><div style="color:#FF3366;">&sup1;&nbsp;<c:out value="${cartItem.inventoryMessage}" /></div></td>
  </tr>
  </c:if>
  
  <c:if test="${siteConfig['SHOW_IMAGE_ON_INVOICE'].value == 'true'}">
  <tr class="shoppingCart${currentItemIndex % 2} ${cartItem.itemGroup}">
   <td colspan="${5+cols}">
	<c:forEach items="${cartItem.productAttributes}" var="productAttribute">
		<c:if test="${!empty productAttribute.imageUrl}" >
		  <div style="float:left;">
		    <img src="${_contextpath}/assets/Image/Product/options/<c:out value="${productAttribute.imageUrl}"/>" class="cartOptionImage" />
		  </div>
		</c:if>
	</c:forEach>
   </td>
  </tr>
  </c:if>

  <c:if test="${cartItem.customLines != null}">
  <tr class="shoppingCart${currentItemIndex % 2} ${cartItem.itemGroup}">
   <td colspan="${5+cols}">
	<%@ include file="/WEB-INF/jsp/frontend/common/customLinesCart.jsp"%>
   </td>
  </tr>
  </c:if>
  
  <!-- Set the cartItem Group and check if the current item and next item has the same group  -->
  <c:if test="${cartItem.itemGroup != null and ( nextItemGroup == null or nextItemGroup != cartItem.itemGroup)}">
    <c:set var="showQtyAndPrice" value="${true}"/>
    <%@ include file="/WEB-INF/jsp/frontend/subCart.jsp" %>
    <c:set var="showQtyAndPrice" value=""/>
  </c:if>
  
<c:if test="${cartStatus.last}">
  
  <c:if test="${gSiteConfig['gSALES_PROMOTIONS'] and siteConfig['PROMO_ON_SHOPPING_CART'].value == 'true' and model.cart.promoAmount > 0}">
  <tr class="promoCodeFtr">
    <td  align="left" ></td>
    <td class="discountFooter" align="right" colspan="${subTotalColumn+3}" ><div><fmt:message key="discount" />: <fmt:message key="${siteConfig['CURRENCY'].value}" /><fmt:formatNumber value="${model.cart.promoAmount}" pattern="#,##0.00"/></div></td>
    <td class="cartUpdateFooter" align="right" colspan="${updateColumn+1-subTotalColumn}"></td>
  </tr>
  </c:if>
  <tr class="shoppingCartFtr">
    <td class="cartWeightFooter" align="left" ><div><c:if test="${model.cart.weight > 0.0}" ><fmt:message key="totalWeight" />: <fmt:formatNumber value="${model.cart.weight}" pattern="#,##0.00"/></c:if></div></td>
    <td class="cartSubtotalFooter" align="right" colspan="${subTotalColumn+3}" ><div><fmt:message key="subTotal" />: <fmt:message key="${siteConfig['CURRENCY'].value}" /><fmt:formatNumber value="${model.cart.subTotal}" pattern="#,##0.00"/></div></td>
    <td class="cartUpdateFooter" align="right" colspan="${updateColumn+1-subTotalColumn}"><input type="submit" value="<fmt:message key="update" />" name="_update"></td>
  </tr>
  <c:if test="${siteConfig['BUY_SAFE_URL'].value != '' and model.buySafeResponse != null}">
  <tr class="buySafeDetails"> 
  	<td>
  	  <span id="buySAFE_Kicker" name="buySAFE_Kicker" type="Kicker Recommended Green Arrow 335x55"></span>
  	  <div><a href="${model.buySafeResponse.cartDetailsUrl}" target="_blank">${model.buySafeResponse.cartDetailsDisplayText}</a></div>
	</td>
    <td class="buySafeButton">${model.buySafeResponse.bondingSignal}
    </td>
	<td class="buySafeCost" align="right" colspan="${subTotalColumn+2}">
	  ${model.buySafeResponse.bondCostDisplayText}
	</td>
	<td align="right" colspan="${updateColumn+1-subTotalColumn}"></td>
  </tr>
 
  <tr class="buySafeSubTotal">
    <td class="buySafeButton"></td>
	<td class="TotalCost" align="right" colspan="${subTotalColumn+3}">
	  <div><fmt:message key="total" />:<fmt:message key="${siteConfig['CURRENCY'].value}" />
	    <c:if test="${model.buySafeResponse.bondCostDisplayText != ''}"><fmt:formatNumber value="${model.cart.subTotal+ model.buySafeResponse.totalBondCost}" pattern="#,##0.00"/></c:if>
	    <c:if test="${model.buySafeResponse.bondCostDisplayText == ''}"><fmt:formatNumber value="${model.cart.subTotal}" pattern="#,##0.00"/></c:if>  </div>
	</td>
	<td align="right" colspan="${updateColumn+1-subTotalColumn}"></td>
  </tr>
  </c:if>
  
</table>
</c:if>
</c:forEach>

<div class="proceed_checkout">
<c:if test="${not empty manufacturerPass}">
<div id="manufacturerList">
Please select which Brand(s) you would like to checkout: 
<select name="manufacturerName">
  <option value=""><fmt:message key="all" /> <fmt:message key="brands" /></option>
<c:forEach items="${manufacturerPass}" var="manufacturerName">
  <option value="<c:out value="${manufacturerName}"/>"><c:out value="${manufacturerName}"/></option>
</c:forEach>
</select>
</div>
</c:if>
<c:if test="${model.cXmlEncodedCart == null}">
<c:choose>
  <c:when test="${gSiteConfig['gSHOPPING_CART_1']}">
    <input type="image" border="0" name="_checkout1" src="${_contextpath}/assets/Image/Layout/button_checkout${_lang}.gif" <c:if test="${sessionCustomer.suspended}">onClick="return confirm('Account is suspended. Continue Checkout?')"</c:if>>
  </c:when>
  <c:otherwise>
    <input type="image" border="0" name="_checkout" src="${_contextpath}/assets/Image/Layout/button_checkout${_lang}.gif" <c:if test="${sessionCustomer.suspended}">onClick="return confirm('Account is suspended. Continue Checkout?')"</c:if>>
  </c:otherwise>
</c:choose>
</c:if>
</div>
<c:if test="${!empty siteConfig['INTERNATIONAL_CHECKOUT'].value}">
	<div class="proceed_international_checkout">
		<a href="${_contextpath}/checkout1.jhtm?internationalCheckout=true" class="shoppingcart_international_checkout_link">
  		<img src="${_contextpath}/assets/Image/Layout/button_international_checkout.gif" border="0"/>
  		</a>
	</div>
</c:if>

<div class="shoppingcart_continue_bottom">
<c:choose>
<c:when test="${model.continueShoppingUrl != null and !empty model.continueShoppingUrl and model.continueShoppingUrl != '/home.jhtm?null'}"><a href="<c:out value='${model.continueShoppingUrl}' />"><fmt:message key="continueShopping" /></a></c:when>
<c:otherwise><a href="javascript:history.back(1);"><fmt:message key="continueShopping" /></a></c:otherwise>
</c:choose>
</div>


<c:if test="${gSiteConfig['gSALES_PROMOTIONS'] and siteConfig['PROMO_ON_SHOPPING_CART'].value == 'true'}">
  <div id="promoContainer">
    <b><fmt:message key="f_enterPromoCode"/>:</b>
	<br />
	<input type="text" name="promoCode" value="${model.cart.promoCode}" />
  <div id="promoButton">
	<input type="image" src="assets/Image/Layout/button_apply_promocode${_lang}.gif" id="promoCodeImage" />
  </div>
  <c:if test="${model.cart.tempPromoErrorMessage != null}"><span class="error"><fmt:message key="${model.cart.tempPromoErrorMessage}"/></span></c:if>
  </div>  
</c:if>

</form>

<!-- CXML Cart -->
<c:if test="${model.cXmlEncodedCart != null}">
<form method="post" action="${model.browserFormPost}">
<input type="hidden" name="cxml-urlencoded" value="${model.cXmlEncodedCart}"> 
<input type="image" border="0" name="_checkout" src="${_contextpath}/assets/Image/Layout/button_checkout${_lang}.gif" <c:if test="${sessionCustomer.suspended}">onClick="return confirm('Account is suspended. Continue Checkout?')"</c:if>>
</form>
</c:if>

<br/><br/>

<c:if test="${( (gSiteConfig['gRECOMMENDED_LIST'] and siteConfig['SHOPPING_CART_RECOMMENDED_LIST'].value == 'true') or (siteConfig['UPSELL_RECOMMENDED_LIST'].value == 'true')) and fn:length(model.recommendedList)!=0 }">
  <div style="float:left;width:100%">
    <div class="recommended_list"><fmt:message key="f_cart_recommendedList" /></div>
    <c:set var="numCul" value="${siteConfig['NUMBER_PRODUCT_PER_ROW'].value}" />
    <c:forEach items="${model.recommendedList}" var="product" varStatus="status" end="${model.limit-1}">
    <c:set var="productLink">${_contextpath}/product.jhtm?id=${product.id}</c:set>
	<c:if test="${gSiteConfig['gMOD_REWRITE'] == '1' and not fn:contains(product.sku, '/')}">
  	  <c:set var="productLink">${_contextpath}/${siteConfig['MOD_REWRITE_PRODUCT'].value}/${product.encodedSku}/${product.encodedName}.html</c:set>
	</c:if>	
    <c:choose>
	 <c:when test="${siteConfig['RECOMMENDED_LIST_DISPLAY'].value == 'quick'}">
	    <%@ include file="/WEB-INF/jsp/frontend/quickmode/view1.jsp" %>
	 </c:when>
	 <c:when test="${siteConfig['RECOMMENDED_LIST_DISPLAY'].value == '2'}">
	    <%@ include file="/WEB-INF/jsp/frontend/thumbnail/view2.jsp" %>
	 </c:when>
	 <c:when test="${siteConfig['RECOMMENDED_LIST_DISPLAY'].value == '3'}">
		<%@ include file="/WEB-INF/jsp/frontend/thumbnail/view3.jsp" %>
	 </c:when>
	 <c:when test="${siteConfig['RECOMMENDED_LIST_DISPLAY'].value == '4'}">
		<%@ include file="/WEB-INF/jsp/frontend/thumbnail/view4.jsp" %>
	 </c:when>
	 <c:when test="${siteConfig['RECOMMENDED_LIST_DISPLAY'].value == '5'}">
		<%@ include file="/WEB-INF/jsp/frontend/thumbnail/view5.jsp" %>
	 </c:when>
	 <c:when test="${siteConfig['RECOMMENDED_LIST_DISPLAY'].value == '6'}">
		<%@ include file="/WEB-INF/jsp/frontend/thumbnail/view6.jsp" %>
	 </c:when>
	 <c:when test="${siteConfig['RECOMMENDED_LIST_DISPLAY'].value == '7'}">
		<%@ include file="/WEB-INF/jsp/frontend/thumbnail/view7.jsp" %>
	 </c:when>
	 <c:when test="${siteConfig['RECOMMENDED_LIST_DISPLAY'].value == '8'}">
		<%@ include file="/WEB-INF/jsp/frontend/thumbnail/view8.jsp" %>
	 </c:when>
	 <c:when test="${siteConfig['RECOMMENDED_LIST_DISPLAY'].value == '9'}">
		<%@ include file="/WEB-INF/jsp/frontend/thumbnail/view9.jsp" %>
	 </c:when>
	 <c:when test="${siteConfig['RECOMMENDED_LIST_DISPLAY'].value == '10'}">
	    <%@ include file="/WEB-INF/jsp/frontend/thumbnail/view10.jsp" %>
	 </c:when>
	 <c:when test="${siteConfig['RECOMMENDED_LIST_DISPLAY'].value == '11'}">
	    <%@ include file="/WEB-INF/jsp/frontend/thumbnail/view11.jsp" %>
	 </c:when>
	 <c:when test="${siteConfig['RECOMMENDED_LIST_DISPLAY'].value == '12'}">
	    <%@ include file="/WEB-INF/jsp/frontend/thumbnail/view12.jsp" %>
	 </c:when>
	 <c:otherwise>
		<%@ include file="/WEB-INF/jsp/frontend/thumbnail/view1.jsp" %>
	 </c:otherwise>
    </c:choose>
    </c:forEach>  
  </div>
</c:if>
</c:if> 

<c:if test="${fn:trim(model.cartLayout.footerHtml) != ''}">
  <c:set value="${model.cartLayout.footerHtml}" var="footerHtml"/>
  <c:set value="${fn:replace(footerHtml, '#cartsubtotal#', cartSubTotal)}" var="footerHtml"/>
  <c:out value="${footerHtml}" escapeXml="false"/>
</c:if>

</div>
<%@ include file="/WEB-INF/jsp/frontend/cartCustom.jsp" %>
  </tiles:putAttribute>
</tiles:insertDefinition>
