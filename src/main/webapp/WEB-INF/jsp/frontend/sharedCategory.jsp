<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>

<tiles:insertDefinition name="${_template}" flush="true">
  <tiles:putAttribute name="content" type="string">

<style type="text/css">
	.cell {float:left; list-style:none;width:130px;padding-left:15px}
</style>
<%-- bread crumbs --%>
<c:forEach items="${model.breadCrumbs}" var="category" varStatus="status">
<c:if test="${status.first}">
<!-- bread crumbs -->
<table border="0" cellpadding="0" cellspacing="0" class="breadcrumbs">
  <tr>
</c:if>
<c:if test="${not status.first}">
	> 
</c:if>
	<a href="scategory.jhtm?sid=${category.id}"	 class="breadcrumb"><c:out value="${category.name}" escapeXml="false" /></a>
<c:if test="${status.last}">
<c:if test="${model.product != null}">
	>
	<span class="breadcrumb_productName"><c:out value="${model.product.name}" escapeXml="false" /></span>
</c:if>
	</td>
  </tr>
</table>
</c:if>
</c:forEach>

<c:if test="${model.products.nrOfElements > 0}">
<c:forEach items="${model.productFields}" var="productField">
  <c:if test="${productField.search}">
    <c:set value="true" var="showFields"/>
  </c:if>
</c:forEach>
<c:if test="${!empty model.productFields and showFields}" >
<form method="post">
<div id="dropDownList_main" >
<p id="dropDownList_title">Narrow Results By:</p>
<p class="dropDownList_p1">
<c:forEach items="${model.productFields}" var="productField">
<c:set value="field_${productField.id}" var="field"/>
<c:set value="cat_${model.category.id}" var="sharedCat"/>
<c:if test="${productField.search}">
<select name="field_${productField.id}" class="dropDownList_select" onchange="submit();">
  <option value=""><c:out value="${productField.name}"/> (ALL)</option>
  <c:forEach items="${productField.values}" var="value">
  <option value="<c:out value="${value}"/>" <c:if test="${sharedProductFieldMap[sharedCat][field].value == value}">selected</c:if>><c:out value="${value}"/></option>
  </c:forEach>
</select>
</c:if>
</c:forEach>
</p>
</div>
</form>
</c:if>
</c:if>

<%-- subcategories --%>
<br />
<c:choose>
 <c:when test="${siteConfig['SHARED_CATEGORIES_LAYOUT'].value == '2'}">
 
<c:if test="${model.category.hasImage}"><div align="center"><img  src="${gSiteConfig['gSHARED_CATEGORIES']}assets/Image/Category/cat_${model.category.id}_big.gif" border="0" /></div></c:if>

<c:if test="${model.category.subcatLocation == ''}">
<c:catch var="exception">
<c:import url="${gSiteConfig['gSHARED_CATEGORIES']}xmlService.jhtm">
  <c:param name="action" value="html" />
  <c:param name="sid" value="${param.sid}" />
</c:import>
</c:catch>
</c:if>

<c:forEach items="${model.category.subCategories}" var="category" varStatus="status">
<c:if test="${status.first}">
<table class="subCategories" >
<tr>
<td valign="top">

</c:if>

   <c:if test="${category.linkType != 'NONLINK'}">      
	  <a href="scategory.jhtm?sid=${category.id}" <c:if test="${category.urlTarget == '_blank'}">target="_blank"</c:if> class="subCatLink">
   </c:if>
   <c:if test="${category.hasImage}">
	  <div align="center"><img src="${gSiteConfig['gSHARED_CATEGORIES']}assets/Image/Category/cat_${category.id}.gif" border="0" /></div>
   </c:if>
   <c:out value="${category.name}" escapeXml="false" /><br>	
   <c:if test="${category.linkType != 'NONLINK'}">      
	  </a> 
   </c:if>		
   <c:forEach items="${category.subCategories}" var="subCat" varStatus="subCatStatus">
     <div <c:if test="${subCat.linkType == 'NONLINK'}">class="subSubCatLink_nonlink"</c:if>>
  	 <c:if test="${subCat.linkType != 'NONLINK'}"><a href="scategory.jhtm?sid=${subCat.id}" <c:if test="${subCat.urlTarget == '_blank'}">target="_blank"</c:if> class="subSubCatLink<c:if test="${subCat.hasSubcats}">Plus</c:if>"></c:if>
  	 <c:out value="${subCat.name}" escapeXml="false" />
  	 <c:if test="${subCat.linkType != 'NONLINK'}"></a></c:if> 
	</div><c:if test="${subCatStatus.last}"><br></c:if>	
   </c:forEach>
<c:if test="${status.count % model.subcatLinksPerCol == 0}">
</td>
<c:if test="${status.count % model.subcatCols == 0 and model.category.subcatDisplay == 'row'}">
</tr>
<tr>
</c:if>
<td valign="top">
</c:if>
<c:if test="${status.last}">
</td>
</tr>
</table>
</c:if>
</c:forEach>

<c:if test="${model.category.subcatLocation == '1'}">
<c:catch var="exception">
<c:import url="${gSiteConfig['gSHARED_CATEGORIES']}xmlService.jhtm">
  <c:param name="action" value="html" />
  <c:param name="sid" value="${param.sid}" />
</c:import>
</c:catch>
</c:if>

 </c:when> 
 <c:when test="${model.category.subcatLevels == 1}">
 <c:forEach items="${model.category.subCategories}" var="category" varStatus="status">
 <c:if test="${status.index % model.subcatLinksPerCol == 0}" > <dl class="cell"> </c:if>
  <lh class="">
      <a class="shared_level1" href="scategory.jhtm?sid=${category.id}">
      <c:out value="${category.name}" escapeXml="false" />
      </a>
  </lh>
  <c:if test="${( status.index % model.subcatLinksPerCol == model.subcatLinksPerCol-1 ) or status.last}" > </dl> </c:if>  
 </c:forEach>
 </c:when>
 <c:when test="${model.category.subcatLevels == 2}">
<c:forEach items="${model.category.subCategories}" var="category" varStatus="status">
 <c:if test="${status.first}" >
  <div class="shared_topHeader_level1">Models:  </div>
 </c:if>
 <c:if test="${status.index % model.subcatLinksPerCol == 0}" > <dl class="cell"> </c:if>
  <lh>
       <a href="#<c:out value='${category.name}' />" class="shared_level1"><c:out value="${category.name}" escapeXml="false" /></a>
  </lh>
  <c:if test="${( status.index % model.subcatLinksPerCol == model.subcatLinksPerCol-1 ) or status.last}" > </dl> </c:if>  
</c:forEach>
<br />
<c:forEach items="${model.category.subCategories}" var="category" varStatus="status">
<c:if test="${status.first}">
<table border="0" width="100%" class="subCategories" style="clear:both;">
<tr>
<td valign="top">
<%-- Spring3 DONE --%>
</c:if>
   <table border="0" width="100%" cellspacing="0" cellpadding="0" style="clear:both;"> <tr><td class="shared_header<c:if test='${model.category.subcatLevels == 2}'>_level2</c:if>">
   <a <c:if test="${model.category.subcatLevels == 2}"> name="<c:out value='${category.name}'/>" </c:if> href="scategory.jhtm?sid=${category.id}" class="subCatLink">
   <div ><c:out value="${category.name}" escapeXml="false" /><br /></div>
   </a></td><td class="shared_header<c:if test='${model.category.subcatLevels == 2}'>_level2</c:if>"> 
   <c:if test="${model.category.subcatLevels == 2}"><a href="#" class="shared_top">Back to top</a></c:if>
   </td></tr></table>
   <%-- int counter = 0; --%>
   <c:set var="counter" value="0" />
   <c:forEach items="${category.subCategories}" var="subCat" varStatus="subCatStatus">
     <%-- counter = counter + 1; --%>
     <c:set var="counter" value="${counter + 1 }" />
   </c:forEach>
 <%-- counter = counter / 4; int  index = 0;--%>
 <c:set var="counter" value="${counter / 4 }" />
 <c:set var="index" value="0" />
 
   <c:forEach items="${category.subCategories}" var="subCat" varStatus="subCatStatus">
   <c:set var="index3" value="${index+1}" />
	   <c:if test="${status.first }">
	     <dl class="cell">
	   </c:if>
	  <%-- if (index == 0) { --%> <%-- <dl class="cell"> --%> <%-- } --%> <%-- index = index + 1; --%>
      <lh>
        <a href="scategory.jhtm?sid=${subCat.id}" class="shared_level2">
        <c:out value="${subCat.name}" escapeXml="false" />
        </a>
      </lh>
      <c:if test="${status.last }">
	     </dl>
	   </c:if>
      <%-- if (0 == index % (Math.round(counter)+1)) { index = 0;--%> <%-- </dl> --%> <%-- } --%> 
      <c:if test="${ ( subCatStatus.last ) }"> </dl> </c:if> 
   </c:forEach>  	
<br/>
<c:if test="${status.last}">
</tr>
</table>
</c:if>
</c:forEach>
 </c:when>
</c:choose>

<c:if test="${param.showResults == 'true'}">
<table border="1" style="clear:both;">
  <tr>
    <td>id</td>
    <td>sku</td>
  </tr>
<c:forEach items="${model.sharedProductList}" var="product">
  <tr>
    <td><a href="${gSiteConfig['gSHARED_CATEGORIES']}product.jhtm?id=${product['id']}" target="_blank"><c:out value="${product['id']}"/></a></td>
    <td><c:out value="${product['sku']}"/></td>
  </tr>
</c:forEach>
</table>
</c:if>
    
<c:if test="${model.products.nrOfElements > 0}">
<script type="text/javascript">
<!--
function checkNumber( aNumber )
{
	var goodChars = "0123456789";
	var i = 0;
	if ( aNumber == "" )
		return 0; //empty
	
	for ( i = 0 ; i <= aNumber.length - 1 ; i++ )
	{
		if ( goodChars.indexOf( aNumber.charAt(i) ) == -1 ) 
			return -1; //invalid number			
	}	
	return 1; //valid number
}
function checkForm()
{
	var productList = document.getElementsByName('product.id');
	var allQtyEmpty = true;
	for(var i=0; i<productList.length; i++) 
	{
	    var el = document.getElementById("quantity_" + productList[i].value);
		if ( el != null )
		{
			if ( checkNumber( el.value ) == 1  )
			{
				allQtyEmpty = false;
			}
			else if ( checkNumber( el.value ) == -1 ) 
			{
				alert("invalid Quantity");
				el.focus();
				return false;
			}			
		}	
	}
	if ( allQtyEmpty )
	{
		alert("Please Enter Quantity.");
		return false;
	}
	else
		return true;	
}
//-->
</script>
<c:set var="pageShowing">
<form action="scategory.jhtm">
<table border="0" cellpadding="0" cellspacing="1" width="100%">
  <tr>
	  <td class="pageShowing">
	  <fmt:message key="showing">
		<fmt:param value="${model.products.firstElementOnPage + 1}"/>
		<fmt:param value="${model.products.lastElementOnPage + 1}"/>
		<fmt:param value="${model.products.nrOfElements}"/>
	  </fmt:message>
  	  </td>
  	  <td class="pageNavi">
		
		<input type="hidden" name="sid" value="${param.sid}">
		Page 
		        <select name="page" onchange="submit()">
				<c:forEach begin="1" end="${model.products.pageCount}" var="page">
		  	        <option value="${page}" <c:if test="${page == (model.products.page+1)}">selected</c:if>>${page}</option>
				</c:forEach>
				</select>
		of <c:out value="${model.products.pageCount}"/>
		| 
	    <c:if test="${model.products.firstPage}"><span class="pageNaviDead"><fmt:message key="f_previous" /></span></c:if>
	    <c:if test="${not model.products.firstPage}"><a href="scategory.jhtm?page=${model.products.page}&sid=${param.sid}" class="pageNaviLink"><fmt:message key="f_previous" /></a></c:if>
	    | 
	    <c:if test="${model.products.lastPage}"><span class="pageNaviDead"><fmt:message key="f_next" /></span></c:if>
	    <c:if test="${not model.products.lastPage}"><a href="scategory.jhtm?page=${model.products.page+2}&sid=${param.sid}" class="pageNaviLink"><fmt:message key="f_next" /></a></c:if>
  	  </td>
  </tr>
</table>
</form>
</c:set>
<c:out value="${pageShowing}" escapeXml="false" />
</c:if>
<c:set var="numCul" value="${siteConfig['NUMBER_PRODUCT_PER_ROW'].value}" />
<c:forEach items="${model.products.pageList}" var="product" varStatus="status">
  <c:choose>
      <c:when test="${siteConfig['CATEGORY_DISPLAY_MODE'].value == 'quick'}">
        <%@ include file="/WEB-INF/jsp/frontend/quickmode/view1.jsp" %>
      </c:when>
	  <c:when test="${siteConfig['CATEGORY_DISPLAY_MODE'].value == '2'}">
	    <%@ include file="/WEB-INF/jsp/frontend/thumbnail/view2.jsp" %>
      </c:when>
	  <c:when test="${siteConfig['CATEGORY_DISPLAY_MODE'].value == '3'}">
		<%@ include file="/WEB-INF/jsp/frontend/thumbnail/view3.jsp" %>
	  </c:when>
	  <c:when test="${siteConfig['CATEGORY_DISPLAY_MODE'].value == '4'}">
		<%@ include file="/WEB-INF/jsp/frontend/thumbnail/view4.jsp" %>
	  </c:when>
	  <c:when test="${siteConfig['CATEGORY_DISPLAY_MODE'].value == '5'}">
		<%@ include file="/WEB-INF/jsp/frontend/thumbnail/view5.jsp" %>
	  </c:when>
	  <c:when test="${siteConfig['CATEGORY_DISPLAY_MODE'].value == '6'}">
		<%@ include file="/WEB-INF/jsp/frontend/thumbnail/view6.jsp" %>
	  </c:when>
	  <c:when test="${siteConfig['CATEGORY_DISPLAY_MODE'].value == '7'}">
		<%@ include file="/WEB-INF/jsp/frontend/thumbnail/view7.jsp" %>
	  </c:when>
	  <c:when test="${siteConfig['CATEGORY_DISPLAY_MODE'].value == '8'}">
		<%@ include file="/WEB-INF/jsp/frontend/thumbnail/view8.jsp" %>
	  </c:when>
	  <c:when test="${siteConfig['CATEGORY_DISPLAY_MODE'].value == '9'}">
		<%@ include file="/WEB-INF/jsp/frontend/thumbnail/view9.jsp" %>
	  </c:when>
	  <c:otherwise>
	    <%@ include file="/WEB-INF/jsp/frontend/thumbnail/view1.jsp" %>
      </c:otherwise>
  </c:choose>
</c:forEach>   
<c:if test="${model.products.nrOfElements > 0 && model.products.pageCount > 1}">
<c:out value="${pageShowing}" escapeXml="false" />
</c:if> 
    
  </tiles:putAttribute>
</tiles:insertDefinition>
