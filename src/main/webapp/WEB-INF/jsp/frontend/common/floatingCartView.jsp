<%@ page session="false" %>
<%@ page import="com.webjaguar.model.*, java.text.NumberFormat, java.util.*" %>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>

	
							<div id="rightShoppingCartBox" style="position: relative; width: 214px; top: 0px;">
												<div id="right_shopping_cart">
													
													<div class="clearfix" id="right_shopping_cart_header">
														<div class="sc_hdr_label">Cart Items</div>
													</div>
													
													<div class="clearfix" id="right_shopping_cart_items">
														
														<div class="product clearfix">
															<div class="product_sku">
																3081
															</div>
															<div class="product_tableGroup">
																<div class="product_image_cell">
																	<div class="product_image">
																		<a href="#"><img class="img-responsive" src="https://www.viatrading.com/assets/Image/Product/detailsbig/maybelline1.gif"></a>
																	</div>
																</div>
																<div class="product_name_cell">
																	<div class="product_name">
																		<a href="#">L'Oreal Cosmetic Lots</a>
																	</div>
																	<div class="quantity_and_price">
																		<span class="quantity">1</span> x <span class="price">50$</span>
																	</div>
																</div>
															</div>
														</div>
														
														<div class="product clearfix">
															<div class="product_sku">
																SKU-AB-12345
															</div>
															<div class="product_tableGroup">
																<div class="product_image_cell">
																	<div class="product_image">
																		<a href="#"><img class="img-responsive" src="https://www.viatrading.com/assets/Image/Product/detailsbig/motherboards-cr-1.gif"></a>
																	</div>
																</div>
																<div class="product_name_cell">
																	<div class="product_name">
																		<a href="#">Desktop Computer Motherboards</a>
																	</div>
																	<div class="quantity_and_price">
																		<span class="quantity">1</span> x <span class="price">1750$</span>
																	</div>
																</div>
															</div>
														</div>
														
														<div class="product clearfix">
															<div class="product_sku">
																LOAD-CTC
															</div>
															<div class="product_tableGroup">
																<div class="product_image_cell">
																	<div class="product_image">
																		<a href="#"><img class="img-responsive" src="https://www.viatrading.com/assets/Image/Product/detailsbig/load-ctc.gif"></a>
																	</div>
																</div>
																<div class="product_name_cell">
																	<div class="product_name">
																		<a href="#">CTC Loads</a>
																	</div>
																	<div class="quantity_and_price">
																		<span class="quantity">1</span> x <span class="price">750$</span>
																	</div>
																</div>
															</div>
														</div>
														
													</div>
													
													
													
													<div class="clearfix" id="right_shopping_cart_footer">
														
														<div class="sc_ftr_info_box sc_subtotal clearfix">
															<div class="sc_ftr_info_label_cell">
																<div class="sc_ftr_info_label subtotal_label">
																	Subtotal
																</div>
															</div>
															<div class="sc_ftr_info_value_cell">
																<div class="sc_ftr_info_value subtotal_value">
																	$0.00
																</div>
															</div>
														</div>
														
														<div class="sc_ftr_info_box sc_discount clearfix">
															<div class="sc_ftr_info_label_cell">
																<div class="sc_ftr_info_label discount_label">
																	Discount
																</div>
															</div>
															<div class="sc_ftr_info_value_cell">
																<div class="sc_ftr_info_value discount_value">
																	$0.00
																</div>
															</div>
														</div>
														
														<div class="sc_ftr_info_box sc_tax clearfix">
															<div class="sc_ftr_info_label_cell">
																<div class="sc_ftr_info_label tax_label">
																	Tax
																</div>
															</div>
															<div class="sc_ftr_info_value_cell">
																<div class="sc_ftr_info_value tax_value">
																	$0.00
																</div>
															</div>
														</div>
														
														<div class="sc_ftr_info_box sc_shippingAndHandling clearfix">
															<div class="sc_ftr_info_label_cell">
																<div class="sc_ftr_info_label shippingAndHandling_label">
																	Shipping &amp; Handling
																</div>
															</div>
															<div class="sc_ftr_info_value_cell">
																<div class="sc_ftr_info_value shippingAndHandling_value">
																	$0.00
																</div>
															</div>
														</div>
														
														<div class="sc_ftr_info_box sc_creditCardFee clearfix">
															<div class="sc_ftr_info_label_cell">
																<div class="sc_ftr_info_label creditCardFee_label">
																	Credit Card Fee (3%)
																</div>
															</div>
															<div class="sc_ftr_info_value_cell">
																<div class="sc_ftr_info_value creditCardFee_value">
																	$0.00
																</div>
															</div>
														</div>
														
														<div class="sc_ftr_info_box sc_grandTotal clearfix">
															<div class="sc_ftr_info_label_cell">
																<div class="sc_ftr_info_label grandTotal_label">
																	Grand Total:
																</div>
															</div>
															<div class="sc_ftr_info_value_cell">
																<div class="sc_ftr_info_value grandTotal_value">
																	$0.00
																</div>
															</div>
														</div>
														
														<div class="buttons clearfix">
															<a href="/viewCart.jhtm" class="btn viewCart_btn">View Cart</a>
															<a href="/checkout.jhtm" class="btn checkout_btn">Checkout</a>
														</div>
													</div>
												</div>

												<div id="sc_relatedProducts">
													<div id="sc_relatedProducts_header">
														<div class="sc_hdr_label">
															Related Products
														</div>
													</div>
													<div id="sc_relatedProducts_content">
														<script type="text/javascript">
															$(function(){
																$(".sc_relatedProducts_carouse").owlCarousel({
																	themeClass: 'sc-carousel-theme',
																	navText: [],
																	loop: true,
																	dots: false,
																	autoplay: true,
																	margin: 30,
																	autoplayTimeout: 5000,
																	items: 1,
																	slideBy: 1,
																	nav: true
																});
															});
														</script>
														
														
														<div class="sc_relatedProducts_carousel_wrapper">
															<div class="sc_relatedProducts_carouse owl-carousel">
																
																<div class="sc_product">
																	<div class="sc_product_image">
																		<a href="#"><img src="https://www.viatrading.com/assets/Image/Product/detailsbig/load-ctc.gif" class="img-responsive center-block"></a>
																	</div>
																	<div class="sc_product_name">
																		<a href="#">Product Name</a>
																	</div>
																	<div class="sc_product_sku">
																		SKU-AB-12345
																	</div>
																	<div class="sc_product_price">
																		$0.00
																	</div>
																</div>
																
																<div class="sc_product">
																	<div class="sc_product_image">
																		<a href="#"><img src="https://www.viatrading.com/assets/Image/Product/detailsbig/load-ctc.gif" class="img-responsive center-block"></a>
																	</div>
																	<div class="sc_product_name">
																		<a href="#">Product Name</a>
																	</div>
																	<div class="sc_product_sku">
																		SKU-AB-12345
																	</div>
																	<div class="sc_product_price">
																		$0.00
																	</div>
																</div>
																
															</div>
														</div>													
		
														
														
														
														
														
														
														
														
													</div>
												</div>
												
												
												
												
												
												
												
												
												
												
												
												
												
												
											</div>