<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>


<form method="post">
<table id="reviews" width="100%" align="center" cellspacing="0" cellpadding="0" border="0">
  <c:if test="${model.companyReviewList.pageCount > 1}">
  <tr>
	<td>
<div class="pageNavi">Page 
		<select name="page" onchange="submit()">
		<c:forEach begin="1" end="${model.companyReviewList.pageCount}" var="page">
  	        <option value="${page}" <c:if test="${page == (model.companyReviewList.page+1)}">selected</c:if>>${page}</option>
		</c:forEach>
		</select>
of <c:out value="${model.companyReviewList.pageCount}"/></div>
	</td>
  </tr>
  </c:if>
  <tr>
	<td colspan="2">
<table border="0" cellpadding="1" cellspacing="0" width="100%">
  <tr><td colspan="2" class="productReview">
  	<table class="companyReviewTitle" border="0" cellpadding="0" cellspacing="0" width="100%">
	  <tr>
	  	<td class="companyReview"><fmt:message key="companyReviews" /></td>
	  	<td align="right" class="writeReview"><a href="addCompanyReview.jhtm?id=${model.id}&url=${model.returnUrl}" ><fmt:message key="reviewThisCompany" /></a></td>
	  </tr>
	</table></td>  	
  </tr>
<c:forEach items="${model.companyReviewList.pageList}" var="review" varStatus="status">
  <tr class="row${status.index % 2}"><td>
    <table border="0" cellpadding="0" cellspacing="0" width="100%">
      <tr>
      	<td align="right" class="reviewDate"><fmt:formatDate type="date" pattern="MM/dd/yyyy" value="${review.created}"/></td>
      </tr> 	
      <tr>  
	    <td class="reviewRate"><c:choose>
	     <c:when test="${review.rate == null}"><img src="assets/Image/Layout/star_0.gif" alt=<c:out value="${review.rate}"/> title="<c:out value="${review.rate}"/>" border="0" /></c:when>
	     <c:when test="${review.rate > 0 and review.rate <= 0.6}"><img src="assets/Image/Layout/star_0h.gif" alt=<c:out value="${review.rate}"/> title="<c:out value="${review.rate}"/>" border="0" /></c:when>
	     <c:when test="${review.rate > 0.6 and review.rate <= 1}"><img src="assets/Image/Layout/star_1.gif" alt=<c:out value="${review.rate}"/> title="<c:out value="${review.rate}"/>" border="0" /></c:when>
	     <c:when test="${review.rate > 1 and review.rate <= 1.6}"><img src="assets/Image/Layout/star_1h.gif" alt=<c:out value="${review.rate}"/> title="<c:out value="${review.rate}"/>" border="0" /></c:when>
	     <c:when test="${review.rate > 1.6 and review.rate <= 2}"><img src="assets/Image/Layout/star_2.gif" alt=<c:out value="${review.rate}"/> title="<c:out value="${review.rate}"/>" border="0" /></c:when>
	     <c:when test="${review.rate > 2 and review.rate <= 2.6}"><img src="assets/Image/Layout/star_2h.gif" alt=<c:out value="${review.rate}"/> title="<c:out value="${review.rate}"/>" border="0" /></c:when>
	     <c:when test="${review.rate > 2.6 and review.rate <= 3}"><img src="assets/Image/Layout/star_3.gif" alt=<c:out value="${review.rate}"/> title="<c:out value="${review.rate}"/>" border="0" /></c:when>
	     <c:when test="${review.rate > 3 and review.rate <= 3.6}"><img src="assets/Image/Layout/star_3h.gif" alt=<c:out value="${review.rate}"/> title="<c:out value="${review.rate}"/>" border="0" /></c:when>
	     <c:when test="${review.rate > 3.6 and review.rate <= 4}"><img src="assets/Image/Layout/star_4.gif" alt=<c:out value="${review.rate}"/> title="<c:out value="${review.rate}"/>" border="0" /></c:when>
	     <c:when test="${review.rate > 4 and review.rate <= 4.6}"><img src="assets/Image/Layout/star_4h.gif" alt=<c:out value="${review.rate}"/> title="<c:out value="${review.rate}"/>" border="0" /></c:when>
	     <c:when test="${review.rate > 4.6}"><img src="assets/Image/Layout/star_5.gif" alt=<c:out value="${review.rate}"/> title="<c:out value="${review.rate}"/>" border="0" /></c:when>
	   </c:choose></td>
	  </tr>
	  <tr>
	    <td class="reviewTitle"><c:out value="${review.title}"/></td>
	  </tr>
	  <tr>  
	    <td class="reviewer">By: <c:out value="${review.userName}"/></td>
	  </tr>
	  <tr>  
	    <td class="reviewText"><pre><c:out value="${review.text}"/></pre></td>  				
	  </tr>
     </td></table>
  </tr>

</c:forEach>
<c:if test="${model.companyReviewList.nrOfElements == 0}">
  <tr class="emptyList"><td colspan="2">&nbsp;</td></tr>
</c:if>
</table>
	</td>
  </tr>
</table>
</form>
