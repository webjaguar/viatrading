<div id="newOption">
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>

<c:if test="${not empty product.productOptions}">

<div class="multiOptionsWrapper" id="multiOptionsWrapperId">
<div class="multiOptions">
<input type="hidden" id="totalOption" value="${fn:length(product.productOptions)}">
<c:forEach items="${product.productOptions}" var="productOption" varStatus="status">
  <input type="hidden" name="optionCode_${product.id}" value="<c:out value="${productOption.optionCode}"/>">	
  <div class="optionWrapper">
    <div class="optionTitleBox">
      <c:out value="${status.index + 1}"/>. &nbsp; <c:out value="${productOption.name}"/>
    </div>
    <div class="optionMultiValuesBox">
    <c:forEach items="${productOption.values}" var="prodOptValue" varStatus="povStatus">
  	  <div class="optionValueBox" align="center">
  	    <img class="optionBigImageBox" src="${_contextpath}/assets/Image/Product/options/${prodOptValue.imageUrl}">
        <div class="optionValueName"><c:out value="${prodOptValue.name}"/></div>
        <div class="optionValueDesc"><c:out value="${prodOptValue.description}" escapeXml="flase"/></div>
        <div class="optionSelectBox">
          Select : <input type="radio" class="selectedOption" name="<c:out value="${productOption.optionCode}"/>-option_values_${product.id}_<c:out value="${productOption.index}"/>" value="<c:out value="${prodOptValue.index}"/>" onclick="getOption('${product.id}');"> 
        </div> 
      </div>
      <c:if test="${(povStatus.index+1) % 3 == 0}">
       <br/>
      </c:if>
    </c:forEach>
    <div style="clear: both;"></div> 
    </div>
  </div>
</c:forEach>
</div>
</div>
<div id="summaryBoxId">
  <div id="summaryTitle">Summary</div>
  <div id="summaryContent"> 
    <span id="summaryPrice"> Price : $ 
      <c:choose>
        <c:when test="${model.newPrice == null}"> 0.0 </c:when>
        <c:otherwise><c:out value="${model.newPrice}"></c:out></c:otherwise>
      </c:choose>
	</span> <br/>
    <c:forEach items="${model.selectedOptions}" var="selectedOption">
      <span> <c:out value="${selectedOption}"></c:out> </span> <br/>
    </c:forEach>
  
  </div>

</div>

<div style="clear: both;"></div> 
</c:if>
</div>