<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>

<c:if test="${product.priceRangeMinimum != null}">
  <c:choose>
  	<c:when test="${!product.loginRequire or (product.loginRequire and userSession != null)}" >
  	  <c:if test="${product.priceRangeMinimum > 0 || siteConfig['SHOW_ZERO_PRICE'].value == 'true'}">
  	  
		<c:set var="multiplier" value="1"/>
		<c:if test="${siteConfig['CASE_CONTENT_LAYOUT'].value == '1' and product.caseContent > 0}">
		  <c:set var="multiplier" value="${product.caseContent}"/>	
		</c:if>  	  
  	  
  	  <div class="price_range">
  	  <span class="priceTitle"><fmt:message key="f-price-range" />: </span>
	  <span class="p_from_id"><fmt:message key="${siteConfig['CURRENCY'].value}" /><fmt:formatNumber value="${product.priceRangeMinimum*multiplier}" pattern="#,##0.00" /></span>
	  <c:if test="${product.priceRangeMaximum != null}">
	  <span class="price_range_separator">-</span>
	  <span class="p_to_id"><fmt:message key="${siteConfig['CURRENCY'].value}" /><fmt:formatNumber value="${product.priceRangeMaximum*multiplier}" pattern="#,##0.00" /></span>
	  </c:if>
	  </div>
	  </c:if>
	  <%-- MOVED to Product Detail and Thumbnail
	  <c:if test="${gSiteConfig['gINVENTORY'] and product.inventory != null}" >
        <c:if test="${!empty siteConfig['INVENTORY_ON_HAND'].value}" ><div class="inventory_onhand"><c:out value="${siteConfig['INVENTORY_ON_HAND'].value}" escapeXml="false"/>&nbsp;<span><c:out value="${product.inventory}" /></span></div></c:if>
      </c:if>
      --%>
	</c:when>
	<c:otherwise>
	  <a href="${_contextpath}/login.jhtm?noMessage=t&forwardAction=<c:out value='${model.url}'/>" ><img border="0" name="_logintoviewprice" class="_logintoviewprice" src="${_contextpath}/assets/Image/Layout/login_to_view_price${_lang}.gif" /></a>
	</c:otherwise>
  </c:choose>
</c:if>
