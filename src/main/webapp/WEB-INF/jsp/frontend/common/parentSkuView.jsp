<%@ page session="false" %>
<%@ page import="com.webjaguar.model.*, java.text.NumberFormat, java.util.*" %>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>


<c:set var="productLink">${_contextpath}/product.jhtm?<c:if test="${model.product.masterSku != '' and model.product.masterSku != null}">sku=${model.product.masterSku}</c:if><c:if test="${model.product.masterSku == '' or model.product.masterSku == null}">id=${model.product.id}</c:if><c:if test="${model.product == null or siteConfig['RECOMMENDED_LIST_SHOW_BREADCRUMB'].value == 'true'}">&cid=<c:out value="${param.cid}"/></c:if></c:set>
<c:if test="${gSiteConfig['gMOD_REWRITE'] == '1' and not fn:contains(product.sku, '/')}">
 <c:set var="productLink">${_contextpath}/${siteConfig['MOD_REWRITE_PRODUCT'].value}/<c:if test="${model.product.masterSku != '' and model.product.masterSku != null}">${model.product.encodedMasterSku}</c:if><c:if test="${model.product.masterSku == '' or model.product.masterSku == null}">${model.product.encodedSku}</c:if>/${model.product.encodedName}.html<c:if test="${model.product == null or siteConfig['RECOMMENDED_LIST_SHOW_BREADCRUMB'].value == 'true'}">?cid=<c:out value="${param.cid}"/></c:if></c:set>
</c:if>	

<div class="clearfix" id="cart-dropdown2-content">
	<div class="dropdownCart_table">

		<div class="dropdownCartDetails">
			
				<div class="col-xs-4 col-sm-4">	
					<div class="cartImageWrapper">
                      <c:if test="${fn:length(model.product.images) > 0}">
                         <c:forEach items="${model.product.images}" var="image" varStatus="status">
	                         <c:if test="${status.index == 0}">
		                         <div class="thumbnail_image">
		                         <a href="<c:if test="${!image.absolute}">${_contextpath}/assets/Image/Product/thumb/</c:if><c:out value="${image.imageUrl}"/>" data-hover="swap-img" data-target=".item_image">
		                              <img class="item_image img-responsive center-block"  src="<c:if test="${!image.absolute}">${_contextpath}/assets/Image/Product/thumb/</c:if><c:out value="${image.imageUrl}"/>" class="img-responsive center-block">
		                         </a>
		                         </div>
	                         </c:if>
                         </c:forEach>
                     </c:if>
                    </div>	
					<div class="clearfix"></div>
				</div>
				
				<div class="col-xs-5 col-sm-5">
					<div class="cartName">
						<div class="cartItemNameSkuWrapper">
							<a class="cart_item_name" href="${productLink}">${model.product.name}</a>
							<br>
						</div>
					</div>
					<div class="clearfix"></div>
				</div>
				
				<div class="col-xs-3 col-sm-3">
					<div class="cartName">
						<div class="cartItemNameSkuWrapper">
							${model.product.field27}
						</div>
					</div>
					<div class="clearfix"></div>
				</div>
						
			</div>
		</div>
	</div>

													