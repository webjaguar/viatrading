<%@ page import="java.util.*" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="/WEB-INF/tlds/custom.tld" prefix="wj" %>
<c:set var="responseCode" value="${wj:bankAudiResponseCode()}" />
<table>
 <tr>
  <td align="right">Transaction Response:</td>
  <td><c:out value="${order.bankAudi.vpc_TxnResponseCode}"/> - <c:out value="${responseCode[order.bankAudi.vpc_TxnResponseCode]}"/></td>
 </tr>
 <tr>
  <td align="right">Transaction Number:</td>
  <td><c:out value="${order.bankAudi.vpc_TransactionNo}"/></td>
 </tr>
 <tr>
  <td align="right">Message:</td>
  <td><c:out value="${order.bankAudi.vpc_Message}"/></td>
 </tr>
 <tr>
  <td align="right">Payment Date:</td>
  <td><fmt:formatDate type="both" timeStyle="full" value="${order.bankAudi.paymentDate}"/></td>
 </tr>
</table>
