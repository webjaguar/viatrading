<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<c:set var="price1" value="0.0" scope="page" />
<table class="prices" border="0">
  <c:if test="${product.msrp != null}">
	<tr id="msrpBox" class="msrpBox">
	  <td class="msrpTitle"><c:choose><c:when test="${siteConfig['MSRP_TITLE'].value != ''}"><c:out value="${siteConfig['MSRP_TITLE'].value}" /></c:when><c:otherwise><fmt:message key="productMsrp" /></c:otherwise></c:choose>:</td>
	  <td class="msrp_space_td">&nbsp;</td>
	  <td class="msrp"><fmt:message key="${siteConfig['CURRENCY'].value}" /> <fmt:formatNumber value="${product.msrp}" pattern="#,##0.00" /></td>
	  <td class="caseContent" style="font-size:10px; white-space: nowrap">
	    <c:choose>
		  <c:when test="${product.caseContent != null}">(<c:out value="${product.caseContent}" /> <c:out value="${siteConfig['CASE_UNIT_TITLE'].value}" />/<c:out value="${product.packing}" />)</c:when>
		  <c:otherwise><c:out value="${product.packing}" /></c:otherwise>
		</c:choose>
	  </td>
	</tr>
  </c:if>
  <c:choose>
  	<c:when test="${!product.loginRequire or userSession != null && !product.altLoginRequire}" >
	  <c:forEach items="${product.price}" var="price" varStatus="statusPrice">
	  <c:if test="${price.amt > 0 || siteConfig['SHOW_ZERO_PRICE'].value == 'true'}">
	  <c:choose>
	    <c:when test="${gSiteConfig['gVARIABLE_PRICE'] and product.priceByCustomer}">
	    <tr class="a">  
		    <td colspan="4">&nbsp;</td>
	    </tr>  	
	    </c:when>
	  	<c:when test="${(statusPrice.first and price.qtyFrom != null and product.salesTag != null)}">
	  	<tr class="b">  
		    <td>&nbsp;</td>
		    <td class="qtyBreakTitle">Quantity</td>
		    <td>&nbsp;</td>
		    <td class="qtyBreakTitle"><c:out value="${siteConfig['PRICE_ONSALE_TITLE'].value}" /><c:if test="${product.caseContent != null and siteConfig['CASE_CONTENT_LAYOUT'].value == ''}"><span class="unitPerCase">/<c:out value="${siteConfig['CASE_UNIT_TITLE'].value}" /></span></c:if></td>
		    
	    </tr>
	  	</c:when>
	  	<c:when test="${ (statusPrice.first and price.qtyFrom == null and product.salesTag != null and product.salesTag.discount != 0.0)}">
	  	<tr class="c">  
		    <td>&nbsp;</td>
		    <td class="qtyBreakTitle">&nbsp;</td>
		    <td class="qtyBreakTitle"><c:out value="${siteConfig['PRICE_BEFORE_TITLE'].value}" /></td>
	    	<td class="qtyBreakTitle"><c:out value="${siteConfig['PRICE_ONSALE_TITLE'].value}" /><c:if test="${product.caseContent != null and siteConfig['CASE_CONTENT_LAYOUT'].value == ''}">/<c:out value="${siteConfig['CASE_UNIT_TITLE'].value}" /></c:if></td>
		    
	    </tr>  	
	  	</c:when>
	  	<c:when test="${ (statusPrice.first and price.qtyFrom == null and product.salesTag == null)}">
	  	<tr class="d">   
		    <td colspan="4">&nbsp;</td>
	    </tr>  	
	  	</c:when>
	  </c:choose>
	    <c:choose>
	      <c:when test="${gSiteConfig['gVARIABLE_PRICE'] and product.priceByCustomer}">
	        <tr><td colspan="4">&nbsp;</td></tr>
	      </c:when>
	      <c:when test="${gSiteConfig['gPRICE_CASEPACK'] and product.priceCasePackQty != null}">
	      <c:set var="multiplier" value="1"/>
	      <c:set var="caseAmt" value="false"/>
	        <c:if test="${siteConfig['CASE_CONTENT_LAYOUT'].value == '1' and product.priceCasePackQty > 0}">
			  <c:set var="multiplier" value="${product.caseContent}"/>	
	        </c:if>
	        <tr id="priceTableWrapper">
	        <td class="priceTitle"><c:if test="${statusPrice.first}"><span class="priceTitleSpan"><fmt:message key="f-price" />:</span></c:if><c:if test="${statusPrice.first == false}">&nbsp;</c:if></td>
	        <td class="price" <c:if test="${product.salesTag != null and product.salesTag.discount != 0.0}" >style="text-decoration: line-through"</c:if>><fmt:message key="${siteConfig['CURRENCY'].value}" /><fmt:formatNumber value="${price.amt*multiplier}" pattern="#,##0.00" /></td><c:set var="price1" value="${price.amt}" />
	        <c:if test="${product.salesTag != null and product.salesTag.discount != 0.0 }" >
		      <td class="price"><fmt:message key="${siteConfig['CURRENCY'].value}" /><fmt:formatNumber value="${price.discountAmt*multiplier}" pattern="#,##0.00#" maxFractionDigits="${siteConfig['MAX_FRACTION_DIGIT'].value}" /></td>
		      <c:set var="price1" value="${price.discountAmt}" />
		    </c:if>
	        </tr>
	        <c:if test="${price.caseAmt != null}"><c:set var="caseAmt" value="true" />
	        <tr id="casePackWrapper">
	        <td class="priceTitle"><c:if test="${statusPrice.first}"><c:out value="${product.packing}" /> <fmt:message key="f_casePackPrice" /> (<fmt:message key="qty" />:<c:out value="${product.priceCasePackQty}"/>):</c:if><c:if test="${statusPrice.first == false}">&nbsp;</c:if></td>
	        <td class="price" <c:if test="${product.salesTag != null and product.salesTag.discount != 0.0}" >style="text-decoration: line-through"</c:if>><fmt:message key="${siteConfig['CURRENCY'].value}" /><fmt:formatNumber value="${price.caseAmt * product.priceCasePackQty}" pattern="#,##0.00#" maxFractionDigits="${siteConfig['MAX_FRACTION_DIGIT'].value}" /></td><c:set var="price1" value="${price.caseAmt}" />
	        <c:if test="${product.salesTag != null and product.salesTag.discount != 0.0 }" >
		      <td class="price"><fmt:message key="${siteConfig['CURRENCY'].value}" /><fmt:formatNumber value="${price.caseDiscountAmt*multiplier}" pattern="#,##0.00#" maxFractionDigits="${siteConfig['MAX_FRACTION_DIGIT'].value}" /></td>
		    </c:if>
	        </tr>
	        </c:if>
	      </c:when>
	      <c:otherwise>
	        <c:set var="multiplier" value="1"/>
	        <c:if test="${siteConfig['CASE_CONTENT_LAYOUT'].value == '1' and product.caseContent > 0}">
			  <c:set var="multiplier" value="${product.caseContent}"/>	
	        </c:if>
	        <tr>
	        <td class="priceTitle"><c:if test="${statusPrice.first}"><span class="priceTitleSpan"><fmt:message key="f-price" />:</span></c:if><c:if test="${statusPrice.first == false}">&nbsp;</c:if></td>
		    <td class="qtyBreak">
		    <c:if test="${price.qtyFrom != null}">
		    	${price.qtyFrom}<c:if test="${price.qtyTo != null}">-${price.qtyTo}</c:if><c:if test="${price.qtyTo == null}">+</c:if> <c:choose><c:when test="${product.caseContent != 1}"> <c:out value="${product.packing}" /></c:when><c:otherwise> units</c:otherwise></c:choose>
		    </c:if>
		    &nbsp;</td>
		    <td class="price" <c:if test="${product.salesTag != null and product.salesTag.discount != 0.0}" >style="text-decoration: line-through"</c:if>><fmt:message key="${siteConfig['CURRENCY'].value}" /><fmt:formatNumber value="${price.amt*multiplier}" pattern="#,##0.00#" maxFractionDigits="${siteConfig['MAX_FRACTION_DIGIT'].value}" /></td><c:set var="price1" value="${price.amt}" />
			<c:if test="${product.salesTag != null and product.salesTag.discount != 0.0 }" >
		      <td class="price"><fmt:message key="${siteConfig['CURRENCY'].value}" /><fmt:formatNumber value="${price.discountAmt*multiplier}" pattern="#,##0.00#" maxFractionDigits="${siteConfig['MAX_FRACTION_DIGIT'].value}" /></td>
		      <c:set var="price1" value="${price.discountAmt}" />
		    </c:if>
		    <td class="caseContent" style="color:#990000; font-size:10px; white-space: nowrap">
		      <c:choose>
		        <c:when test="${product.caseContent != null}">(<c:out value="${product.caseContent}" /> <c:out value="${siteConfig['CASE_UNIT_TITLE'].value}" />/<c:out value="${product.packing}" />)</c:when>
		        <c:otherwise><c:out value="${product.packing}" /></c:otherwise>
		      </c:choose>
		    </td>
		    </tr>
		  </c:otherwise>
	    </c:choose>
	  </c:if>
	  </c:forEach> 
	  <c:if test="${empty product.price and gSiteConfig['gMASTER_SKU'] and not empty product.masterSku}"><a href="${_contextpath}/category.jhtm?cid=852" class="clickForQuote">CLICK FOR QUOTE</a></c:if> 
	</c:when>
	<c:otherwise>
	  <c:choose>
	    <c:when test="${product.altLoginRequire}">
	      <img border="0" name="_altLogintoviewprice" class="_altLogintoviewprice" src="${_contextpath}/assets/Image/Layout/altLogin_to_view_price${_lang}.gif" />
	    </c:when>
	    <c:otherwise>
	      <a href="${_contextpath}/login.jhtm?noMessage=t&forwardAction=<c:out value='${model.url}'/>" ><img border="0" name="_logintoviewprice" class="_logintoviewprice" src="${_contextpath}/assets/Image/Layout/login_to_view_price${_lang}.gif" /></a>
	    </c:otherwise>
	  </c:choose>
	</c:otherwise>
  </c:choose>
  <%-- MOVED to Product Detail and Thumbnail
  <c:if test="${gSiteConfig['gINVENTORY'] and product.inventory != null}" >
    <c:if test="${!empty siteConfig['INVENTORY_ON_HAND'].value}" ><div class="inventory_onhand"><c:out value="${siteConfig['INVENTORY_ON_HAND'].value}" escapeXml="false"/>&nbsp;<span><c:out value="${product.inventory}" /></span></div></c:if>
  </c:if> 
  --%>	 
  <c:if test="${ (product.salesTag.image and !empty product.price) and (!product.loginRequire or userSession != null) }" >
    <tr>
      <td colspan="3"><img align="right" class="salesTagImage" src="${_contextpath}/assets/Image/PromoSalesTag/salestag_${product.salesTag.tagId}.gif" /></td>
    </tr>  
  </c:if>
  <c:if test="${product.savingsAmount != null}">
  <tr>
    <td class="youSaveTitle">You Save:</td>
    <td class="youSave"><c:out value="${product.savingsAmount}" />
    	(<fmt:formatNumber maxFractionDigits="2"><c:out value="${product.savingsPercentage}" /></fmt:formatNumber>%)</td>
  </tr>
  </c:if>
  <%@ include file="/WEB-INF/jsp/frontend/common/priceCustom.jsp" %>
</table>
