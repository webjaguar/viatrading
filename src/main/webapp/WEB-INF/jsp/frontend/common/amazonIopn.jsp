<%@ page import="java.util.*,java.text.*" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<table>
 <tr>
  <td align="right">Order Channel:</td>
  <td><c:out value="${order.amazonIopn.orderChannel}"/></td>
 </tr>
 <tr>
  <td align="right">Order ID:</td>
  <td><c:out value="${order.amazonIopn.amazonOrderID}"/></td>
 </tr>
 <tr>
  <td align="right">Notification Type:</td>
  <td><c:out value="${order.amazonIopn.notificationType}"/></td>
 </tr>
 <tr>
  <td align="right">Timestamp:</td>
  <td><c:out value="${order.amazonIopn.timestamp}"/></td>
 </tr>
</table>
