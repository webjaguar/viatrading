<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<select name="quantity_${product.id}" id="quantity_${product.id}">
	<c:if test="${startEmpty}">
		<option value=""></option>
	</c:if>
	<c:forEach begin="${product.dropDownBegin}" end="${product.dropDownEnd}" step="${product.dropDownStep}" var="qty">
		<option value="${qty}">${qty}</option>
	</c:forEach>
</select>