<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<script type="text/javascript" src="${_contextpath}/javascript/mootools-1.2.5-core.js"></script>

<table class="productRate" border="0" cellpadding="0" cellspacing="0" width="100%">
 <tr>
  <td style="width:50px;"><div class="ratingTitle"><fmt:message key="rating" />:</div></td>
  <td align="left"><div id="reviewStar" >
<div id="stars">
<ul id="rate100" class="rating <c:out value="${model.productRate.style}" />">
  <li id="1" class="rate one"><span title="1 Star"></span></li>
  <li id="2" class="rate two"><span title="2 Stars"></span></li>
  <li id="3" class="rate three"><span title="3 Stars"></span></li>
  <li id="4" class="rate four"><span title="4 Stars"></span></li>
  <li id="5" class="rate five"><span title="5 Stars"></span></li>
</ul>
</div>
<div id="respondRate">
  <c:if test="${model.productRate.numReview != null}">
    <b><c:out value="${model.productRate.numReview}" /></b><span> vote(s)</span>
  </c:if>
</div>
</div>
 </td>
 </tr>
</table>
<script language="JavaScript">
var sku = '<c:out value="${product.sku}" />';
$$('.rate').each(function(element,i){
	element.addEvent('click', function(){
		var myStyles = ['nostar', 'onestar', 'twostar', 'threestar', 'fourstar', 'fivestar', 'sixstar', 'sevenstar', 'eightstar', 'ninestar', 'tenstar'];
		myStyles.each(function(myStyle){
			if(element.getParent().hasClass(myStyle)){
				element.getParent().removeClass(myStyle);
			}
		});		
		myStyles.each(function(myStyle, index){
			if(index == element.id){
			         var request = new Request.JSON({
			        	url: 'productRate.jhtm?rate='+element.id+'&sku='+sku,
						onRequest: function() { $('reviewStar').fade('out'); },
						onComplete: function(jsonObj) {
							if (jsonObj.message != 'error') {
								$('respondRate').innerHTML = "<b>" + jsonObj.productReview.numReview + "</b><span> rating(s)</span>";
								element.getParent().toggleClass(jsonObj.productReview.style);
							} else {
								$('respondRate').innerHTML = "<b>You Had Already Rated This Product.</b>";
								$('stars').setStyle('display', 'none');
							}
							fadeIn.delay(500);
						}

			         }).send();    
				exit;
			}
		});		
		
	});
});
var fadeIn = function() {$('reviewStar').fade('in');}
</script>