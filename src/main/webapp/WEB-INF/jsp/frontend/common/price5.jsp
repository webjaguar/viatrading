<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<div id="priceContainer">
<div id="priceContainerBdy" class="bdy">
	<div id="priceGridContainer">
		<div class="hiddenTable" id="standardPricing">
			<table cellspacing="0" cellpadding="0" border="0" class="priceGrid" style="width: 100%;">
			<tbody>
			<tr class="qtyRow">
			  <td class="bold left qty">Quantity</td><td class="space"></td><c:set var="pcals" value="2"/>
			  <c:set var="multiPrice" value="false"/>
			<c:forEach items="${product.price}" var="price" varStatus="statusPrice"><c:set var="pcals" value="${pcals+1}"/>
			  <td class="bold center"><c:choose><c:when test="${price.qtyFrom == null}"><c:out value='${product.minimumQty}' /></c:when><c:otherwise><c:out value="${price.qtyFrom}" /></c:otherwise></c:choose><c:choose><c:when test="${price.qtyTo == null}">+</c:when><c:otherwise>-<c:out value="${price.qtyTo}" /></c:otherwise></c:choose></td>
			  <c:if test="${!statusPrice.last}">
			    <td class="space"></td><c:set var="pcals" value="${pcals+1}"/>
			  </c:if>
			  <c:if test="${statusPrice.count > 1}">
			    <c:set var="multiPrice" value="true"/>
			  </c:if>			  
			</c:forEach>	
			</tr>
			<tr>
				<td colspan="${pcals}" class="spacer"></td>
			</tr>
			<c:if test="${product.salesTag != null and product.salesTag.discount != 0.0}" >
			<tr class="priceRow">
				<td class="bold left">Retail</td><td class="space"></td>
			<c:forEach items="${product.price}" var="price" varStatus="statusPrice">	
				<td class="bold center" style="text-decoration: line-through"><fmt:message key="${siteConfig['CURRENCY'].value}" /><fmt:formatNumber value="${price.amt}" pattern="#,##0.00" /></td>
			  <c:if test="${!statusPrice.last}">
			    <td class="space"></td>
			  </c:if>
	        </c:forEach>
			</tr>
			<tr>
				<td colspan="${pcals}" class="spacer"></td>
			</tr>
			</c:if>
			<tr class="salesRow" <c:if test="${multiPrice and product.endQtyPricing and !(product.salesTag != null and product.salesTag.discount != 0.0)}">style="text-decoration: line-through;"</c:if>>
				<td class="bold left"><fmt:message key="f_quickView_youPay" /></td><td class="space"></td>
				<c:forEach items="${product.price}" var="price" varStatus="statusPrice"><c:set var="pcals" value="${pcals+1}"/>
				  <c:set var="multiplier" value="1"/>
			      	<c:if test="${siteConfig['CASE_CONTENT_LAYOUT'].value == '1' and product.caseContent > 0}">
			  			<c:set var="multiplier" value="${product.caseContent}"/>	
			      	</c:if>
				  <c:choose>
				    <c:when test="${product.salesTag != null and product.salesTag.discount != 0.0}"><td class="bold center"><fmt:message key="${siteConfig['CURRENCY'].value}" /><fmt:formatNumber value="${price.discountAmt}" pattern="#,##0.00" /></td></c:when>
				    <c:otherwise>
				      <td class="bold center">
				        <fmt:message key="${siteConfig['CURRENCY'].value}" /><fmt:formatNumber value="${price.amt*multiplier}" pattern="#,##0.00" />
				        <c:set var="lastPrice" value="${price.amt*multiplier}"/>
				      </td>
				    </c:otherwise>
				  </c:choose>
			      <c:if test="${!statusPrice.last}">
			  		<td class="space"></td>
			  	  </c:if>
			    </c:forEach>
			</tr>
			<c:if test="${multiPrice and product.endQtyPricing and !(product.salesTag != null and product.salesTag.discount != 0.0)}">
			<tr class="salesRow">
				<td class="bold left"><fmt:message key="f_quickView_youPay" /></td><td class="space"></td>
				<c:forEach items="${product.price}" var="price" varStatus="statusPrice"><c:set var="pcals" value="${pcals+1}"/>
				    <td class="bold center">
				      <fmt:formatNumber value="${lastPrice}" pattern="#,##0.00" />
				    </td>
			      <c:if test="${!statusPrice.last}">
			  		<td class="space"></td>
			  	  </c:if>
			    </c:forEach>
			</tr>
			</c:if>
			<tr>
				<td colspan="${pcals}" class="spacer"></td>
			</tr>
			</tbody>
			</table>
		</div>
	</div>
	<div id="inputFormContainer">
		<div class="hiddenTable" id="standardPricing">
		<form action="${_contextpath}/addToCart.jhtm" name="this_form" method="post" onsubmit="return checkForm()">
		<input type="hidden" name="product.id" value="${product.id}">
		<table cellspacing="0" cellpadding="0" border="0" class="price5Form" style="width: 100%;">
			<tbody>
			<tr>
				<td class="qty">Quantity:</td>
				<td class="qtyInput" align="right">
				<input type="text" maxlength="5" name="quantity_${product.id}" id="quantity_${product.id}" value="<c:out value='${product.minimumQty}' />"></td>
			</tr>
			<tr>
				<td style="border-bottom: 1px solid rgb(90, 131, 160); font-size: 1px; padding: 2px 0pt;" colspan="2">&nbsp;</td>
			</tr>
			<tr>
			  <td class="options" colspan="2">
				<%@ include file="/WEB-INF/jsp/frontend/common/productOptions.jsp" %>
			  </td>
			</tr>
			<c:if test="${ gSiteConfig['gSHOPPING_CART'] and !empty product.price and !product.loginRequire or userSession != null}">
			<tr>
			<td id="addToCartButton" colspan="2">
			  <table cellspacing="0" cellpadding="0" border="0">
			    <tr>
		      		<td id="addToCart">
						<c:choose>
						   <c:when test="${gSiteConfig['gINVENTORY'] and product.inventory != null and !product.negInventory and product.inventory <= 0}" >
						     <c:out value="${siteConfig['OUT_OF_STOCK_TITLE'].value}" escapeXml="false"/>
						   </c:when>
						   <c:when test="${zeroPrice and siteConfig['ZERO_PRICE_HTML'].value != ''}">
						     <c:out value="${siteConfig['ZERO_PRICE_HTML'].value}" escapeXml="false"/>
						   </c:when>
						   <c:otherwise>
						       <input type="image" border="0" name="_addtocart" class="_addtocart" src="${_contextpath}/assets/Image/Layout/button_addtocart${_lang}.gif" onClick="return checkQty(${product.id})" />
						        <c:if test="${gSiteConfig['gINVENTORY'] and product.inventory != null and product.inventory < product.lowInventory}">
						         <span><c:out value="${siteConfig['LOW_STOCK_TITLE'].value}" escapeXml="false"/></span> 
						        </c:if>      
						   </c:otherwise>
						 </c:choose>
					 </td>
			    </tr>
			  </table>
			</td>
			</tr>
			</c:if>
		</tbody>
		</table>
		</form>
	</div>
	</div>
</div>
	<table cellspacing="0" cellpadding="0" border="0" id="buttons">
	<tr><%-- ASI layout have MyList for all products --%>
	<c:if test="${gSiteConfig['gMYLIST']}">
		 <td id="addToList">
			<form action="${_contextpath}/addToList.jhtm">
				<input type="hidden" name="product.id" value="${product.id}">
				<div id="addToList">
				<input type="image" border="0" src="${_contextpath}/assets/Image/Layout/button_addtolist${_lang}.gif">
				</div>
			</form>
		</td>	
	</c:if>
	<c:if test="${gSiteConfig['gCOMPARISON'] && product.compare}">
		<td id="comparison">
			<form action="${_contextpath}/comparison.jhtm" method="get">
			<%-- 
			<input type="hidden" name="forwardAction" value="<c:out value='${model.url}'/>">
			--%>
			<input type="hidden" name="product.id" value="${product.id}">
			<div id="comparison">
            <input type="image" border="0" src="${_contextpath}/assets/Image/Layout/button_compare${_lang}.gif">
            </div>
			</form>
		</td>	
	</c:if>
	<c:if test="${ (product.salesTag.image and !empty product.price) and (!product.loginRequire or userSession != null) }" >
		<td id="salesTagId">
		    <img align="right"  src="${_contextpath}/assets/Image/PromoSalesTag/salestag_${product.salesTag.tagId}.gif" />
		</td>
	</c:if>
	</tr>
	</table>
</div>