<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="/WEB-INF/tlds/custom.tld" prefix="wj" %>

<script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1.7.2/jquery.min.js"></script>
<script type="text/javascript" src="${_contextpath}/javascript/countDown.js"></script>
<link href="${_contextpath}/assets/countdown.css" rel="stylesheet" type="text/css">

<script language="javascript" type="text/javascript">
	jQuery("document").ready(function() {
		countdown();
	});
</script>
<!-- Countdown dashboard start -->
<input type="hidden" value="${model.product.salesTag.endDate}" id="endDateValue"/>
	<div id="countdown_dashboard">
	    <div class="title" id="titleBox"><fmt:message key="f_timeLeft" /></div>
		<div class="digit day" id="daysBox"></div>
		<div class="colon" id="daysColon">:</div>
		<div class="digit hour" id="hoursBox"></div>
		<div class="colon" id="daysColon">:</div>
		<div class="digit min" id="minsBox"></div>
		<div class="colon" id="daysColon">:</div>
		<div class="digit sec" id="secsBox"></div>
	</div>
<!-- Countdown dashboard end -->
<div id="complete_info_message"></div>