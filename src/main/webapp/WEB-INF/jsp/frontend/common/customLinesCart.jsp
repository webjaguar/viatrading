<%@page import="com.webjaguar.model.CartItem"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>

<div align="center" class="customLine">
<%--Spring3 DONE--%>

<c:if test="${cartItem.customLines != null}" >
<%-- hack to split by \n --%>
<c:set var="temp" value="one
two"/>
 <c:set var="newline" value="${fn:substring(temp,3,4)}"/>
<table>
<c:forEach items="${cartItem.customLines}" var="customLine" varStatus="key">
 <tr><td align="center"><b>Inscription ${key.index+1}</b></td></tr>
 <tr>
 <td align="right">
   <c:forEach items="${fn:split(customLine, newline)}" var="value" varStatus="valuesIndex">
     <c:if test="${not valuesIndex.last}">
       Line # ${valuesIndex.index+1} <input name="customLine_${cartItemIndex}_${key.index}" type="text" style="text-align:center;"  size="50" value="<c:out value="${value}"/>"
		maxlength="<c:choose><c:when test="${cartItem.product.customLineCharacter != null}"><c:out value="${cartItem.product.customLineCharacter}"/></c:when><c:otherwise>10</c:otherwise></c:choose>"><br/>	  
     </c:if>
     <c:if test="${valuesIndex.last}">
       <b>Qty</b> <input type="text" size="4" maxlength="5" style="border:1px solid #334334;margin:1px;" name="customLine_qty_${cartItemIndex}"  value="<c:out value="${fn:substringAfter(value, '=')}"/>" />
     </c:if>
   </c:forEach>
 </td>
 </tr>
</c:forEach>
</table>
</c:if>

<%--Spring3
CartItem cartItem = (CartItem) pageContext.getAttribute("cartItem");

if (cartItem.getCustomLines() != null) {
  int key = 0;
  for (String customLine: cartItem.getCustomLines().keySet()) {	
	out.println("<table>");  	
	out.println("<tr><td align=\"center\"><b>Inscription " + (key+1) + "</b></td></tr>"); 
	out.println("<tr>");  	
	out.println("<td align=\"right\">");
	String[] values = customLine.split("\n");
	for (int x=0; x<values.length; x++) {
		pageContext.setAttribute("value", values[x].trim());
		pageContext.setAttribute("key", key);
		out.println("Line #" + (x+1));

<input name="customLine_${cartItemIndex}_${key}" type="text" style="text-align:center;"  size="50" value="<c:out value="${value}"/>"
		maxlength="<c:choose><c:when test="${cartItem.product.customLineCharacter != null}"><c:out value="${cartItem.product.customLineCharacter}"/></c:when><c:otherwise>10</c:otherwise></c:choose>"><br/>	  

	}
	out.println("<b>Qty</b> <input type=\"text\" size=\"4\" maxlength=\"5\"" + 
		//" style=\"border:1px solid #334334;margin:1px;\"" +
		" name=\"customLine_qty_" + pageContext.getAttribute("cartItemIndex") + "\"" +
		" value=\"" + cartItem.getCustomLines().get(customLine) + "\">");
	out.println("</td>"); 
	out.println("</tr>");  	
	out.println("</table>");  	
	key++;
  }
}
--%>
</div>


