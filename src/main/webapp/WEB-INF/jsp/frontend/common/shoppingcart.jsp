<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>

<c:if test="${userSession == null}">
<table class="cartTotal">
  <tr>
    <td><a href="viewCart.jhtm"><img src="${_contextpath}/assets/Image/Layout/cart.gif" border="0"></a></td>
    <td><a href="viewCart.jhtm" class="viewCartLink"><c:out value="${sessionCart.numberOfItems + 0}"/> 
    		<c:choose>
    		  <c:when test="${sessionCart.numberOfItems == 1}"><fmt:message key="item" /></c:when>
    		  <c:otherwise><fmt:message key="items" /></c:otherwise>    		  
    		</c:choose><c:if test="${sessionCart.subTotal != 0}"> : <fmt:formatNumber value="${sessionCart.subTotal}" pattern="#,##0.00"/></c:if></td>
  </tr>
</table>
</c:if>
<c:if test="${userSession != null}">
<table class="cartTotal">
  <tr>
    <td><a href="viewCart.jhtm"><img src="${_contextpath}/assets/Image/Layout/cart.gif" border="0"></a></td>
    <td><a href="viewCart.jhtm" class="viewCartLink"><c:out value="${userCart.numberOfItems + 0}"/> 
    		<c:choose>
    		  <c:when test="${userCart.numberOfItems == 1}"><fmt:message key="item" /></c:when>
    		  <c:otherwise><fmt:message key="items" /></c:otherwise>    		  
    		</c:choose><c:if test="${userCart.subTotal != 0}"> : <fmt:formatNumber value="${userCart.subTotal}" pattern="#,##0.00"/></c:if></td>
  </tr>
</table>
</c:if>
