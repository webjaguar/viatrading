<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>

<c:if test="${gSiteConfig['gMASTER_SKU'] and product.slaveCount > 0}">
<a href="product.jhtm?id=${product.id}&amp;cid=<c:out value="${param.cid}"/>" class="seller"><fmt:message key="from" /> 
	<c:if test="${product.slaveLowestPrice != null}"><span class="seller_price"><fmt:message key="${siteConfig['CURRENCY'].value}" /><fmt:formatNumber value="${product.slaveLowestPrice}" pattern="#,##0.00" /><c:if test="${product.slaveHighestPrice != null and product.slaveHighestPrice != product.slaveLowestPrice}"> - <fmt:formatNumber value="${product.slaveHighestPrice}" pattern="#,##0.00" /></c:if></span> (</c:if><c:out value="${product.slaveCount}"/><c:choose> <c:when test="${product.slaveCount > 1}"><fmt:message key="sellers"/></c:when><c:otherwise><fmt:message key="seller"/></c:otherwise></c:choose><c:if test="${product.slaveLowestPrice != null}">)</c:if>
</a>
</c:if>
