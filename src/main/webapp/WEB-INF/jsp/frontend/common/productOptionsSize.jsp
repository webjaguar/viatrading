<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%-- size option--%>
<c:set var="showPrice" value="true"/>

<c:forEach items="${product.productOptions}" var="productOption">
<c:if test="${productOption.type != 'box' and fn:toLowerCase(productOption.name) == 'size'}" >

<c:forEach items="${productOption.values}" var="prodOptValue" varStatus="povStatus">

<c:if test="${povStatus.first}">
<div class="details_hdr">Select <c:out value="${productOption.name}"/></div>
<input type="hidden" name="<c:out value="${productOption.optionCode}"/>-option_${product.id}" value="<c:out value="${productOption.index}"/>">
<c:set var="showPrice" value="false"/>
<table cellspacing="0" cellpadding="2" align="center" id="sizeOptionTable">
<tr>
  <td>&nbsp;</td>  
  <td><b><c:out value="${productOption.name}"/></b></td>  
  
  <c:if test="${!product.loginRequire or userSession != null}" >
  <c:forEach items="${product.price}" var="price">
    <c:if test="${price.qtyFrom != null}">
      <td><b>${price.qtyFrom}<c:if test="${price.qtyTo != null}">-${price.qtyTo}</c:if><c:if test="${price.qtyTo == null}">+</c:if> <c:choose><c:when test="${product.caseContent != 1}"> <c:out value="${product.packing}" /></c:when><c:otherwise> units</c:otherwise></c:choose></b></td>
    </c:if>
  </c:forEach>
  </c:if>
  
</tr>
</c:if>
<tr>
  <td><input type="radio" name="<c:out value="${productOption.optionCode}"/>-option_values_${product.id}_${productOption.index}" value="${prodOptValue.index}" <c:if test="${povStatus.first}">checked</c:if>></td>
  <td><c:out value="${prodOptValue.name}"/></td>

  <c:set var="optionPrice" value="0"/>
  <c:if test="${prodOptValue.optionPrice != null}">
    <c:set var="optionPrice" value="${prodOptValue.optionPrice}"/>
  </c:if>
  
  <c:choose>
  	<c:when test="${!product.loginRequire or userSession != null}" >
  <c:forEach items="${product.price}" var="price">
    <td>
    <span <c:if test="${product.salesTag != null}" >style="text-decoration: line-through"</c:if>>
      <fmt:message key="${siteConfig['CURRENCY'].value}" /><fmt:formatNumber value="${price.amt+optionPrice}" pattern="#,##0.00" />
    </span>
	<c:if test="${product.salesTag != null }" >
	  <fmt:message key="${siteConfig['CURRENCY'].value}" /><fmt:formatNumber value="${price.discountAmt+optionPrice}" pattern="#,##0.00" />
	</c:if>
	</td>
  </c:forEach>
    </c:when>
	<c:otherwise>
	  <td><a href="${_contextpath}/login.jhtm?noMessage=t&forwardAction=<c:out value='${model.url}'/>" ><img border="0" name="_logintoviewprice" class="_logintoviewprice" src="${_contextpath}/assets/Image/Layout/login_to_view_price${_lang}.gif" /></a></td>
	</c:otherwise>
  </c:choose>

</tr>
<c:if test="${povStatus.last}">  	  
</table>
</c:if>

</c:forEach>

</c:if>
</c:forEach>

<c:if test="${(product.salesTag.image and !empty product.price) and (!product.loginRequire or userSession != null) }" >
<div align="center"><img  src="${_contextpath}/assets/Image/PromoSalesTag/salestag_${product.salesTag.tagId}.gif" /></div>
</c:if>

<c:if test="${showPrice}">
<c:choose>
<c:when test="${product.salesTag != null and !product.salesTag.percent}">
  <%@ include file="/WEB-INF/jsp/frontend/common/price3.jsp" %>
</c:when>
<c:otherwise>
  <%@ include file="/WEB-INF/jsp/frontend/common/price.jsp" %>
</c:otherwise>
</c:choose>
</c:if>