<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<c:if test="${gSiteConfig['gTAB'] > 0 and !empty model.tabList}" >
<script src="${_contextpath}/javascript/SimpleTabs1.2.js" type="text/javascript"></script>
<script type="text/javascript">
	window.addEvent('domready', function(){
		var tabs1 = new SimpleTabs($('tab-block-1'), { selector: 'h4'});
	});
</script>
<!-- tabs -->
<div id="tab-block-1">
<c:forEach items="${model.tabList}" var="tab" varStatus="status">
<h4 id="${tab.tabNumber}" title="<c:out value="${tab.tabName}" escapeXml="false"/>"><c:out value="${tab.tabName}" escapeXml="false" /></h4>
	<div>
	<!-- start tab -->
	    <c:out value="${tab.tabContent}" escapeXml="false"/>
	<!-- end tab -->
	</div>
</c:forEach>
</div>
</c:if>