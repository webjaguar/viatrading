<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>

<div class="productOption" id="productOptionsId">
<c:forEach items="${model.optionList}" var="productOption" varStatus="optionStatus">
  <input type="hidden" name="optionCode_${productOption.productId}" value="<c:out value="${productOption.optionCode}"/>"/>	
  <div style="display:none;">
    <c:set var="optName" value="${productOption.optionCode}-option_values_${productOption.productId}_${productOption.index}"></c:set>
    <c:set var="optValue"></c:set>
    <c:forEach var="entry" items="${model.originalSelection}" varStatus="status">
      <c:if test="${optName == entry.key}">
        <c:set var="optValue" value="${entry.value}"></c:set>  
      </c:if>
    </c:forEach>
  </div>
  
  <div class="productOptionNVPair" id="productOptionNVPairId${optionStatus.index}">
    <c:set var="includedProducts" value=""/>
	
	<!-- option name starts -->
	<div class="optionName">
	  <c:out value="${productOption.name}"/>:
	</div>
	<!-- option name ends -->
	
	<div class="optionValue">
	  
	  <!-- option value start -->
	  <c:if test="${!empty productOption.values}">
	  <input type="hidden" name="<c:out value="${productOption.optionCode}"/>-option_${productOption.productId}" value="<c:out value="${productOption.index}"/>"/>
	  <select onchange="getOption('${model.productId}');"  name="<c:out value="${productOption.optionCode}"/>-option_values_${productOption.productId}_<c:out value="${productOption.index}" />" class="optionSelect">
	  <c:forEach items="${productOption.values}" var="prodOptValue" varStatus="povStatus">
	  	  <option  <c:if test="${prodOptValue.index == optValue}">selected="selected"</c:if> value="<c:out value="${prodOptValue.index}"/>"><c:out value="${prodOptValue.name}"/><c:if test="${prodOptValue.optionPrice != null}"><c:choose><c:when test="${prodOptValue.optionPrice > 0}"> + </c:when><c:otherwise> - </c:otherwise></c:choose><fmt:message key="${siteConfig['CURRENCY'].value}" /><fmt:formatNumber value="${prodOptValue.absoluteOptionPrice}" pattern="#,##0.00" /></c:if></option>
	      <c:if test="${prodOptValue.index == optValue}">
	 	    <c:set var="includedProducts" value="${prodOptValue.includedProductList}"/>	    
	      </c:if>
	  </c:forEach>
	  </select>
	  </c:if>
	  <!-- option value ends -->
	  
	  <!-- custom text start -->
	  <c:if test="${siteConfig['CUSTOM_TEXT_PRODUCT'].value == 'true'}">
	    <c:if test="${empty productOption.values}">
	      <input type="hidden" name="<c:out value="${productOption.optionCode}"/>-option_${productOption.productId}" value="<c:out value="${productOption.index}"/>"/>
	      <textarea name="<c:out value="${productOption.optionCode}"/>-option_values_cus_${productOption.productId}_<c:out value="${productOption.index}" />" class="optionTextArea"></textarea>
	    </c:if>
	  </c:if>
	  <!-- custom text end -->
	  
	</div>

    <!-- class to clear the float, if any -->
    <div class="clear"></div>

	<!-- attachment for included products start -->
	<c:if test="${includedProducts != null && !empty includedProducts}">
	  <div class="optionName">
	    <c:out value="${productOption.name}"/>&nbsp;<fmt:message key="attachment" />:
	  </div>
	  <div class="attachment">
	    <c:forEach items="${includedProducts}" var="includedProduct" varStatus="productStatus">
	      <c:if test="${includedProduct.attachment}">
		    <input value="browse" type="file" name="attachment_${includedProduct.sku}"/>
		  </c:if>
	   	</c:forEach>
	  </div>
      <!-- class to clear the float, if any -->
      <div class="clear"></div>
	</c:if>
	<!-- attachment for included products end -->
  </div>
</c:forEach>
</div>

<c:if test="${!empty model.imageUrlSet}">
<div id="images" >
<c:forEach items="${model.imageUrlSet}" var="imageUrl">
  <div style="display: inline;"><img class="optionImage" <c:if test="${imageUrl != null}">src="${_contextpath}/assets/Image/Product/options/${imageUrl}"</c:if>></div>
</c:forEach>
</div>
</c:if>

<div style="clear: both;"></div> 
<input type="hidden" name="newPrice" value="<fmt:formatNumber value="${model.newPrice}" pattern="#,##0.00"/>" id="newPrice"/>