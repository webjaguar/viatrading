<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>

<c:if test="${product.subscription and gSiteConfig['gSUBSCRIPTION']}">
<div class="subscription">
<table>
<tr>
  <td class="title" colspan="5"><fmt:message key="subscribe"/>:
   <c:if test="${product.subscriptionDiscount != null}">
    Save an Additional <fmt:formatNumber value="${product.subscriptionDiscount}" pattern="#,##0.00" />%
   </c:if>
  </td>
</tr>
<tr>
  <td class="text" colspan="5">A reminder email will be sent 1 week before your order ships with the option to increase, decrease, or cancel your order.</td>
</tr>
<tr>
  <td>Send me this product: </td>
  <td><input type="radio" name="subscriptionInterval_${product.id}" value="m1"></td>
  <td><div class="option"><fmt:message key="every_m"><fmt:param value="1"/></fmt:message></div></td>
  <td><input type="radio" name="subscriptionInterval_${product.id}" value="m2"></td>
  <td><div class="option"><fmt:message key="every_ms"><fmt:param value="2"/></fmt:message></div></td>
</tr>
</table>
</div>
</c:if>
