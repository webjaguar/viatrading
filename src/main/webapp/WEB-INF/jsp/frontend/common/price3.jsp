<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>

<table class="prices3" border="0">
  <c:choose>
  	<c:when test="${!product.loginRequire or userSession != null && !product.altLoginRequire}" >
  	  <tr class="c">  
		    <td colspan="2">&nbsp;</td>
		    <td class="qtyBreakTitle"><c:out value="${siteConfig['PRICE_BEFORE_TITLE'].value}" /></td>
	    	<td class="qtyBreakTitle"><c:out value="${siteConfig['PRICE_ONSALE_TITLE'].value}" /><c:if test="${product.caseContent != null and siteConfig['CASE_CONTENT_LAYOUT'].value == ''}">/<c:out value="${siteConfig['CASE_UNIT_TITLE'].value}" /></c:if></td>
	  </tr>
	  <c:forEach items="${product.price}" var="price" varStatus="statusPrice">
	  <c:if test="${statusPrice.first and (price.amt > 0 || siteConfig['SHOW_ZERO_PRICE'].value == 'true')}">
        <c:set var="multiplier" value="1"/>
        <c:if test="${siteConfig['CASE_CONTENT_LAYOUT'].value == '1' and product.caseContent > 0}">
	  	 <c:set var="multiplier" value="${product.caseContent}"/>	
        </c:if>
	  <tr>
	    <td colspan="2" class="priceTitle"><fmt:message key="f-price" />:</td>
		<td class="price" style="text-decoration: line-through"><fmt:message key="${siteConfig['CURRENCY'].value}" /><fmt:formatNumber value="${price.amt*multiplier}" pattern="#,##0.00#" maxFractionDigits="${siteConfig['MAX_FRACTION_DIGIT'].value}" /></td>
		<td class="price"><fmt:message key="${siteConfig['CURRENCY'].value}" /><fmt:formatNumber value="${price.discountAmt*multiplier}" pattern="#,##0.00#" maxFractionDigits="${siteConfig['MAX_FRACTION_DIGIT'].value}" /></td>
	    <c:if test="${product.caseContent != null}"><td class="caseContent" style="color:#990000; font-size:10px; white-space: nowrap">(<c:out value="${product.caseContent}" /> <c:out value="${siteConfig['CASE_UNIT_TITLE'].value}" />/<c:out value="${product.packing}" />)</td></c:if>
	  </tr>
	  </c:if>
	  </c:forEach>  
	  <c:if test="${ product.salesTag.image and !empty product.price }" >
      <tr>
        <td colspan="3"><img align="right"  src="${_contextpath}/assets/Image/PromoSalesTag/salestag_${product.salesTag.tagId}.gif" /></td>
      </tr>  
      </c:if>
	</c:when>
	<c:otherwise>
	  <c:choose>
	    <c:when test="${product.altLoginRequire}">
	      <img border="0" name="_altLogintoviewprice" class="_altLogintoviewprice" src="${_contextpath}/assets/Image/Layout/altLogin_to_view_price${_lang}.gif" />
	    </c:when>
	    <c:otherwise>
	      <a href="${_contextpath}/login.jhtm?noMessage=t&forwardAction=<c:out value='${model.url}'/>" ><img border="0" name="_logintoviewprice" class="_logintoviewprice" src="${_contextpath}/assets/Image/Layout/login_to_view_price${_lang}.gif" /></a>
	    </c:otherwise>
	  </c:choose>
	</c:otherwise>
  </c:choose>	
  <%-- MOVED to Product Detail and Thumbnail
  <c:if test="${gSiteConfig['gINVENTORY'] and product.inventory != null}" >
    <c:if test="${!empty siteConfig['INVENTORY_ON_HAND'].value}" ><div class="inventory_onhand"><c:out value="${siteConfig['INVENTORY_ON_HAND'].value}" escapeXml="false"/>&nbsp;<span><c:out value="${product.inventory}" /></span></div></c:if>
  </c:if>  
  --%>
</table>
