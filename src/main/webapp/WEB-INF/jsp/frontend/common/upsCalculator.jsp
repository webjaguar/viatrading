<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<c:if test="${siteConfig['UPS_CALCULATOR'].value == 'true'}">
<script type="text/javascript" src="${_contextpath}/javascript/mootools-1.2.5-core.js"></script>
<script language="JavaScript">
function calculateShipping() {
	letter = 'num';
	var toZip = document.getElementById("toZip").value;
	var toCountry = document.getElementById("toCountry").value;
	var qty = 1;
	var qtyE = document.getElementById("quantity_<c:out value="${product.id}"/>");
	if (qtyE != null != null && checkNumber(qtyE.value) > 0) {
		qty = qtyE.value;
	}

	var request = new Request.JSON({
		url: "${_contextpath}/json.jhtm?action=ups&sku=<c:out value="${product.sku}"/>&toZip=" + toZip + "&toCountry=" + toCountry + "&qty=" + qty,
		onRequest: function() { 
			$("shippingResults").empty();
			var img = document.createElement("img");
			img.setAttribute("src", "${_contextpath}/admin/graphics/spinner.gif");
			document.getElementById("shippingResults").appendChild(img); 
		},
		onComplete: function(jsonObj) {
			document.getElementById("shippingResults").empty();
			var table = document.createElement("table");
			var count = 0;
			jsonObj.rates.each(function(rate) {
				var tr = document.createElement("tr");
				var td = document.createElement("td");
				var cellText = document.createTextNode("$" + rate.price);
				td.appendChild(cellText);
				tr.appendChild(td);
				td = document.createElement("td");
				td.setAttribute("style", "padding-left: 10px");
				cellText = document.createTextNode(rate.title);
				td.appendChild(cellText);
				tr.appendChild(td);
				document.getElementById("shippingResults").appendChild(tr);
				count += 1;
			});
			if (count > 0) {
				var tr = document.createElement("tr");
				var td = document.createElement("td");
				var cellText = document.createTextNode(" ");
				td.appendChild(cellText);
				tr.appendChild(td);
				td = document.createElement("td");
				td.setAttribute("style", "color: #FF0000");
				cellText = document.createTextNode("Note: Prices quoted are aprroximate.");
				td.appendChild(cellText);
				tr.appendChild(td);
				document.getElementById("shippingResults").appendChild(tr);
			}
			document.getElementById("shippingResults").appendChild(table);
		}
	}).send();
}
</script>
<table style="border: 1px solid #000000" bgcolor="#F2F2F2" align="center" width="250">
<tr>
<td colspan="2" style="background: #C92B2A; color: #FFFFFF; font-weight: bold" align="center">Shipping Calculator</td>
</tr>
<tr>
<td>Zip Code:</td>
<td><input id="toZip"/></td>
</tr>
<tr>
<td>Country:</td>
<td>
  <select id="toCountry">
    <option value="US">United States</option>
  </select>
</td>
</tr>
<tr>
<td>&nbsp;</td>
<td><input type="button" value="Calculate" title="Calculate" onClick="calculateShipping()"/></td>
</tr>
<tr>
<td colspan="2" id="shippingResults"></td>
</tr>
</table>
</c:if>