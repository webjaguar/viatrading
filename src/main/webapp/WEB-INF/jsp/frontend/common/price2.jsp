<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>

<table class="prices" border="0">
  <c:choose>
  	<c:when test="${!product.loginRequire or userSession != null}" >
	  <c:forEach items="${product.price}" var="price" varStatus="statusPrice">
	  <c:if test="${statusPrice.first and (price.amt > 0 || siteConfig['SHOW_ZERO_PRICE'].value == 'true')}">
	  <tr>
		<td class="price">
		 <fmt:message key="${siteConfig['CURRENCY'].value}" /><fmt:formatNumber value="${price.amt}" pattern="#,##0.00#" maxFractionDigits="${siteConfig['MAX_FRACTION_DIGIT'].value}" />
		</td>
		<c:if test="${!empty product.msrp}">
		<td class="msrp"> 
		 <div style="text-decoration: line-through"> <fmt:message key="${siteConfig['CURRENCY'].value}" /><fmt:formatNumber value="${product.msrp}" pattern="#,##0.00" /></div>
		</td>
		<td class="youSave"> 
		 You Save  <fmt:formatNumber value="${(1 - price.amt / product.msrp) * 100}" pattern="#,##0.0"/> %
		</td>
		</c:if>
	  </tr>
	  </c:if>
	  </c:forEach>  
	</c:when>
	<c:otherwise>
	  <a href="${_contextpath}/login.jhtm?noMessage=t&forwardAction=<c:out value='${model.url}'/>" ><img border="0" name="_logintoviewprice" class="_logintoviewprice" src="${_contextpath}/assets/Image/Layout/login_to_view_price${_lang}.gif" /></a>
	</c:otherwise>
  </c:choose>	
  <%-- MOVED to Product Detail and Thumbnail
  <c:if test="${gSiteConfig['gINVENTORY'] and product.inventory != null}" >
    <c:if test="${!empty siteConfig['INVENTORY_ON_HAND'].value}" ><div class="inventory_onhand"><c:out value="${siteConfig['INVENTORY_ON_HAND'].value}" escapeXml="false"/>&nbsp;<span><c:out value="${product.inventory}" /></span></div></c:if>
  </c:if>  
  --%>
</table>
