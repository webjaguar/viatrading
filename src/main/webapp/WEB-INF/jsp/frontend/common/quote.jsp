<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Insert title here</title>
</head>
<script>
<c:if test="${siteConfig['PRODUCT_QUOTE'].value == 'true' and product.quote}" >
function emailCheck(str){
	 if(/^[a-z0-9._%-]+@[a-z0-9.-]+\.[a-z]{2,4}$/i.test(str) == false) {
		alert('Invalid Email');
		return false;
	 }
	 return true;
}
function assignValue(ele, value) {
$(ele).value = value;
}
function phoneCheck(str){
	if (str.search(/^\d{10}$/)==-1) {
		alert("Please enter a valid 10 digit number");
		return false;
	} 
}
function saveContact(){
	try {
		if( !validateContactInfo() ) {
			return;
		}
	}catch(err){}
	
	var sku = $('sku').value;
	var name = $('name').value;
	var firstName = $('firstName').value;
	var lastName = $('lastName').value;
	var company = $('company').value;
	var address = $('address').value;
	var city = $('city').value;
	var state = $('state').value;
	var zipCode = $('zipCode').value;
	var country = $('country').value;
	var email = $('email').value;
	var phone = $('phone').value;
	var note = $('note').value;
	var request = new Request({
	   url: "${_contextpath}/insert-ajax-contact.jhtm?firstName="+firstName+"&lastName="+lastName+"&company="+company+"&address="+address+"&city="+city+"&state="+state+"&zipCode="+zipCode+"&country="+country+"&email="+email+"&phone="+phone+"&note="+note+"&sku="+sku+"&name="+name+"&leadSource=quote",
       method: 'post',
       onFailure: function(response){
           saveContactBox.close();
           $('mbQuote').style.display = 'none';
           $('successMessage').set('html', '<div style="color : RED; text-align : LEFT;">We are experincing problem in saving your quote. <br/> Please try again later.</div>');
       },
       onSuccess: function(response) {
            saveContactBox.close();
            $('mbQuote').style.display = 'none';
            $('successMessage').set('html', response);
       }
	}).send();
}
</c:if>
//-->
</script>
<body>

<c:if test="${siteConfig['PRODUCT_QUOTE'].value == 'true' and product.quote}">
						<link rel="stylesheet" href="${_contextpath}/assets/SmoothGallery/multiboxASI.css" type="text/css" media="screen" />
<c:if test="${!multibox}">
 <script src="${_contextpath}/javascript/overlay.js" type="text/javascript" ></script>
 <script src="${_contextpath}/javascript/multibox.js" type="text/javascript" ></script>
</c:if>
<script type="text/javascript">
window.addEvent('domready', function(){			
	saveContactBox = new multiBox('mbQuote', {showControls : false, useOverlay: false, showNumbers: false });
});
</script>
  <a href="#${product.id}" rel="type:element" id="mbQuote" class="mbQuote"><input type="image" border="0" name="_addtocart" class="_addtocart" src="${_contextpath}/assets/Image/Layout/button_addtoquote${_lang}.gif" /></a>
  <div id="${product.id}" class="miniWindowWrapper mbPriceQuote" style="height:700px">
	<form autocomplete="off" onsubmit="saveContact(); return false;">
	<input type="hidden" value="${product.sku}" id="sku"/>
	<input type="hidden" value="${product.name}" id="name"/>
	
  	<div class="header">Please provide your contact information</div>
	<fieldset class="top">
		<label class="AStitle"><fmt:message key="firstName"/></label>
		<input name="firstName" id="firstName" type="text" size="15" onkeyup="assignValue('firstName', this.value);"/>
	</fieldset>
	<fieldset class="bottom">
		<label class="AStitle"><fmt:message key="lastName"/></label>
		<input name="lastName" id="lastName" type="text" size="15" onkeyup="assignValue('lastName', this.value);"/>
	</fieldset>
	<fieldset class="bottom">
		<label class="AStitle"><fmt:message key="company"/></label>
		<input name="company" id="company" type="text" size="15" onkeyup="assignValue('company', this.value);"/>
	</fieldset>
	<fieldset class="bottom">
		<label class="AStitle"><fmt:message key="address"/></label>
		<input name="address" id="address" type="text" size="15" onkeyup="assignValue('address', this.value);"/>
	</fieldset>
	<fieldset class="bottom">
		<label class="AStitle"><fmt:message key="city"/></label>
		<input name="city" id="city" type="text" size="15" onkeyup="assignValue('city', this.value);"/>
	</fieldset>
	<fieldset class="bottom">
		<label class="AStitle"><fmt:message key="state"/></label>
		<input name="state" id="state" type="text" size="15" onkeyup="assignValue('state', this.value);"/>
	</fieldset>
	<fieldset class="bottom">
		<label class="AStitle"><fmt:message key="zipCode"/></label>
		<input name="zipCode" id="zipCode" type="text" size="15" onkeyup="assignValue('zipCode', this.value);"/>
	</fieldset>
	<fieldset class="bottom">
		<label class="AStitle"><fmt:message key="country"/></label>
		<input name="country" id="country" type="text" size="15" onkeyup="assignValue('country', this.value);"/>
	</fieldset>
	<fieldset class="bottom">
		<label class="AStitle"><fmt:message key="email"/></label>
		<input name="email" id="email" type="text" size="15" onkeyup="assignValue('email', this.value);"/>
	</fieldset>
	<fieldset class="bottom">
		<label class="AStitle"><fmt:message key="phone"/></label>
		<input name="phone" id="phone" type="text" size="15" onkeyup="assignValue('phone', this.value);"/>
	</fieldset>
	<fieldset class="bottom">
		<label class="AStitle"><fmt:message key="message"/></label>
		<textarea name="note" id="note" onkeyup="assignValue('note', this.value);"></textarea>
	</fieldset>
	<fieldset>
		<div id="button">
			<input type="submit" value="Send"/>
		</div>
	</fieldset>
	</form>
  </div>
  <div id="successMessage"></div>	
				</c:if>
</body>
</html>