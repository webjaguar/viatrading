<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<c:set var="index" value="0" />
<input type="hidden" id="box_total_qty" name="box_total_qty" value="">
 <c:forEach items="${model.boxContentsList}" var="productContent" varStatus="status">
  <c:if test="${status.first}">
  <table border="0" width="100%" >
  </c:if>
  <c:if test="${ index == 0 }">	<tr> </c:if> 
  <td valign="top" id="box_content">
    <div>
      <div><img  id="box_content_img" border="0"  src="${_contextpath}/assets/Image/Product/thumb/<c:out value="${productContent.thumbnail.imageUrl}"/>" class="thumbnail_image"></div><br />
      <div id="box_content_product_name" >
       <c:out value="${productContent.name}" escapeXml="false"/>
      </div>
      <p id="box_content_dis">
       <c:out value="${productContent.shortDesc}" escapeXml="false"/>
      </p>
    </div>
    <br />
    <div>
      <input type="hidden" name="<c:out value="${productContent.optionCode}"/>-option_${product.id}" value="<c:out value="${productContent.optionIndex}"/>">
      <input type="hidden" name="<c:out value="${productContent.optionCode}"/>-option_weight_${product.id}_<c:out value="${productContent.optionIndex}" />" value="<c:out value="${productContent.weight}"/>">
      <input type="hidden" name="box_content.id" value="<c:out value="${productContent.optionCode}"/>-${productContent.id}">
      <input type="hidden" name="<c:out value="${productContent.optionCode}"/>-product_name_${product.id}_<c:out value="${productContent.optionIndex}"/>" value="${productContent.name}">
      Qty: <input id="qty_<c:out value="${productContent.optionCode}"/>-${productContent.id}" name="<c:out value="${productContent.optionCode}"/>-option_values_box_${product.id}_<c:out value="${productContent.optionIndex}" />" size="8" maxlength="10"></input>
    </div>
   </td>
   <c:set var="index" value="${index+1}" />
   <c:if test="${index % 4 == 0 }">
    </tr>
    <c:set var="index" value="0" />
   </c:if>
  
  <c:if test="${status.last}">
<c:if test="${index != 0}">
    </tr>
</c:if>
   </table>
  </c:if>
 </c:forEach>
