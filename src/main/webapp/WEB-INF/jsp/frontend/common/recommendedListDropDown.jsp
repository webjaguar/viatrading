<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>

<table cellspacing="0" cellpadding="0" border="0" class="recommendedList_dropdown">
<tr>
<td>
<c:choose>
 <c:when test="${!empty product.recommendedListTitle }"><c:out value="${product.recommendedListTitle}"/>:&nbsp;</c:when>
 <c:otherwise>
  <c:if test="${siteConfig['RECOMMENDED_LIST_TITLE'].value != ''}">
   <c:out value="${siteConfig['RECOMMENDED_LIST_TITLE'].value}"/>:&nbsp;
  </c:if>
 </c:otherwise>
</c:choose>
</td>
<td>
<c:set var="rProductFound" value="false"/>
<select onChange="location.href=this.value">
<c:forEach items="${model.recommendedList}" var="rproduct">

<c:set var="productLink">${_contextpath}/product.jhtm?id=${rproduct.id}<c:if test="${model.product == null or siteConfig['RECOMMENDED_LIST_SHOW_BREADCRUMB'].value == 'true'}">&cid=<c:out value="${param.cid}"/></c:if></c:set>
<c:choose>
<c:when test="${quickView}">
  <c:set var="productLink">${_contextpath}/qvproduct.jhtm?id=${rproduct.id}</c:set>
</c:when>
<c:when test="${gSiteConfig['gMOD_REWRITE'] == '1' and not fn:contains(rproduct.sku, '/')}">
  <c:set var="productLink">${_contextpath}/${siteConfig['MOD_REWRITE_PRODUCT'].value}/${rproduct.encodedSku}/${rproduct.encodedName}.html<c:if test="${model.product == null or siteConfig['RECOMMENDED_LIST_SHOW_BREADCRUMB'].value == 'true'}">?cid=<c:out value="${param.cid}"/></c:if></c:set>
</c:when>	
</c:choose>

  <option value="${productLink}"
  		<c:if test="${rproduct.id == model.product.id}">selected<c:set var="rProductFound" value="true"/></c:if>>
  		<c:out value="${rproduct[model.product.recommendedListDisplay]}" /> 
  		<c:if test="${rproduct.inventory > 0 and false}">(<c:out value="${rproduct.inventoryAFS}" /> in stock)</c:if>
  </option>
</c:forEach>
<c:if test="${rProductFound == false}">
  <option value="" selected><fmt:message key="pleaseSelect"/></option>
</c:if>
</select>
</td>
</tr>
</table>