<%@ page import="java.util.*,com.webjaguar.model.*" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%--Spring3 DONE--%>
<c:if test="${not empty model.preHangingDoorOptions}">
<table cellspacing="0" cellpadding="0" border="0" class="productOption" id="preHangingDoorOptions" style="display:none;">
<tr>
<td>&nbsp;</td>
<td><img id="preHangingDoorImage" border="0"/></td>
</tr>
<c:forEach items="${model.preHangingDoorOptions}" var="productOption">
<input type="hidden" name="optionCode_${product.id}" value="<c:out value="${productOption.optionCode}"/>">	
<c:if test="${productOption.type != 'box'}" >
<tr>
<td class="optionName" align="right"><c:out value="${productOption.name}"/>: </td>
<td>
  <input type="hidden" name="<c:out value="${productOption.optionCode}"/>-option_${product.id}" value="<c:out value="${productOption.index}"/>">
  <select class="optionSelect" 
          disabled="disabled"
          id="OPTION-${productOption.name}" 
          name="<c:out value="${productOption.optionCode}"/>-option_values_${product.id}_<c:out value="${productOption.index}" />" 
          onChange="togglePrehanging()" >
  	  
  	  <c:forEach items="${productOption.values}" var="prodOptValue" varStatus="povStatus">
  	    <option value="<c:out value="${prodOptValue.index}"/>"><c:out value="${prodOptValue.name}"/><c:if test="${prodOptValue.optionPrice != null}"><c:choose><c:when test="${prodOptValue.optionPrice > 0}"> + </c:when><c:otherwise> - </c:otherwise></c:choose><fmt:message key="${siteConfig['CURRENCY'].value}" /><fmt:formatNumber value="${prodOptValue.absoluteOptionPrice}" pattern="#,##0.00" /></c:if></option>
	  </c:forEach>
  </select>
</td>
</tr>

<c:if test="${siteConfig['CUSTOM_TEXT_PRODUCT'].value == 'true'}">
  <c:if test="${empty productOption.values}">
   <tr>
    <td class="optionName" align="right"><c:out value="${productOption.name}"/>: </td>
    <td>
    <input type="hidden" id="OPTION-${productOption.name}" name="<c:out value="${productOption.optionCode}"/>-option_${product.id}" value="<c:out value="${productOption.index}"/>">
    <input name="<c:out value="${productOption.optionCode}"/>-option_values_cus_${product.id}_<c:out value="${productOption.index}" />" maxlength="110"></input>
    </td>
   </tr>
  </c:if>
</c:if>
</c:if>
</c:forEach>

<tr>
  <td colspan="2">&nbsp;</td>
</tr>
<tr>
  <td colspan="2" bgcolor="black">&nbsp;</td>
</tr>
<tr>
  <td class="optionName" align="right">Price</td>
  <td align="right"><div id="price1"></div></td>
</tr>
<tr>
  <td class="optionName" align="right">Pre-Hanging</td>
  <td align="right"><div id="preHanging"></div></td>
</tr>
<tr>
  <td class="optionName" align="right">Additional Options</td>
  <td align="right"><div id="additionalOptions"></div></td>
</tr>
<tr>
  <td colspan="2"><hr/></td>
</tr>
<tr>
  <td class="optionName" align="right" style="color:red;font-weight:bold;">TOTAL</td>
  <td align="right" style="color:red;font-weight:bold;"><div id="totalDoor"></div></td>
</tr>
<tr>
  <td colspan="2">&nbsp;</td>
</tr>
<tr>
  <td class="optionName" align="right">Net Frame Dimension</td>
  <td align="right"><div id="netFrameDimension"></div></td>
</tr>
<tr>
  <td colspan="2" bgcolor="black">&nbsp;</td>
</tr>
<tr>
  <td colspan="2">&nbsp;</td>
</tr>

</table>


<c:set var="optionCodes"/>
<c:set var="preHangingDoorOptionsWithBox"/>
<c:forEach items="${product.productOptions}" var="productOption">
  <c:if test="${productOption.type != 'box'}">
    <c:forEach items="${productOption.values}" var="value" varStatus="status">
      <c:if test="${productOption.name == 'Species' or productOption.name == 'Width & Height' or productOption.name == 'Jamb'}">
        <input type="hidden" id="OPTION-${productOption.name}-VALUE-${value.index}" value="${fn:escapeXml(value.name)}"></input>
      </c:if>
      
      <c:if test="${value.optionPrice != null}">
        <c:set var="tempOption" value="OPTION-${productOption.name}"/>
        <c:if test="${!fn:contains(optionCodes, tempOption)}">
          <c:set var="optionCodes" value="${optionCodes};OPTION-${productOption.name}"></c:set>  
        </c:if>
        <input type="hidden" id="OPTION-${productOption.name}-PRICE-${value.index}" value="${value.optionPrice}"></input>
      </c:if>
     </c:forEach>
  </c:if>

</c:forEach>
<c:forEach items="${model.preHangingDoorOptions}" var="productOption">
  <c:if test="${productOption.type != 'box'}">
    <c:forEach items="${productOption.values}" var="value" varStatus="status">
      <c:if test="${productOption.name == 'Species' or productOption.name == 'Width & Height' or productOption.name == 'Jamb'}">
        <input type="hidden" id="OPTION-${productOption.name}-VALUE-${value.index}" value="${fn:escapeXml(value.name)}"></input>
      </c:if>
      <c:if test="${value.optionPrice != null}">
        <c:set var="tempOption" value="OPTION-${productOption.name}"/>
        <c:if test="${!fn:contains(optionCodes, tempOption)}">
          <c:set var="optionCodes" value="${optionCodes};OPTION-${productOption.name}"></c:set>  
        </c:if>
        <input type="hidden" id="OPTION-${productOption.name}-PRICE-${value.index}" value="${value.optionPrice}"></input>
      </c:if>
    </c:forEach>
  <c:set var="preHangingDoorOptionsWithBox" value="${preHangingDoorOptionsWithBox};${productOption.name}"/>
  </c:if>
</c:forEach>

<c:forEach items="${model.preHanging}" var="preHanging">
    <input type="hidden" id="ph_PRICE-${preHanging['species']}-${preHanging['width_height']}" value="${preHanging['price']}"></input>
    <input type="hidden" id="ph_DIMENSION-${preHanging['species']}-${preHanging['width_height']}" value="${preHanging['dimensions']}"></input>
</c:forEach>

<input type="hidden" id="preHangingDoorOptionsWithBox" name="preHangingDoorOptionsWithBox" value="${preHangingDoorOptionsWithBox}"></input>
<input type="hidden" id="optionCodes" name="optionCodes" value="${optionCodes}"></input>
 <script type="text/javascript">
<!--
function togglePrehanging() {
	  if ($('OPTION-Pre-Hanging').value == '1') {
        $('preHangingDoorImage').setStyle('display', 'block');
        $('preHangingDoorOptions').setStyle('display', 'block');
		
		var preHangingDoorOptions = $('preHangingDoorOptionsWithBox').value.split(";");
		for(var i=1; i< preHangingDoorOptions.length; i++) {
	    	$('OPTION-'+preHangingDoorOptions[i]).disabled = false;
	    }
	  } else {
		document.getElementById('preHangingDoorImage').style.display="none";
		document.getElementById('preHangingDoorOptions').style.display="none";
		var preHangingDoorOptions = document.getElementById('preHangingDoorOptionsWithBox').value.split(";");
		for(var i=1; i< preHangingDoorOptions.length; i++) {
			document.getElementById('OPTION-'+preHangingDoorOptions[i]).disabled=true;
		}

	  }	 
	  if (document.getElementById('OPTION-Handing').value == '0') {
		document.getElementById('preHangingDoorImage').setAttribute("src", "${_contextpath}/assets/Doors/${model.product.field1}-Left Swing.jpg");
	  } else {
		document.getElementById('preHangingDoorImage').setAttribute("src", "${_contextpath}/assets/Doors/${model.product.field1}-Right Swing.jpg");
	  }	

	  // additional options
	  var additionalOptions = 0.00;
      var optionCodes = document.getElementById('optionCodes').value.split(';');
	  
      for(var i=1; i< optionCodes.length; i++) {
    	  var el = document.getElementById(optionCodes[i] + '-PRICE-' + document.getElementById(optionCodes[i]).value);
    	  if (el != null) {
			  additionalOptions = additionalOptions + parseFloat(el.value);
		  }
	   }
	   document.getElementById("additionalOptions").innerHTML = CurrencyFormatted(additionalOptions);


	 // pre-hanging
	 var preHanging = 0.00;
	 var netFrameDimension = "";
	 var elSpecies = document.getElementById('OPTION-Species');
	 var elWidthHeight = document.getElementById('OPTION-Width & Height');
	 var elJamb = document.getElementById('OPTION-Jamb');
	 if (elSpecies != null && elWidthHeight != null) {
		 species = document.getElementById("OPTION-Species-VALUE-" + elSpecies.value).value;
		 widthHeight = document.getElementById("OPTION-Width & Height-VALUE-" + elWidthHeight.value).value;
		 var ph_PRICE = document.getElementById("ph_PRICE-" + species + "-" + widthHeight);
		 if (ph_PRICE != null) {
			 jamb = document.getElementById("OPTION-Jamb-VALUE-" + elJamb.value).value;
			 if (jamb.indexOf("10%") > 0) {
				 preHanging = ph_PRICE.value * 1.1; // add 10%
			 } else {
				 preHanging = ph_PRICE.value;
			 }	 
		 }
		 var ph_DIMENSION = document.getElementById("ph_DIMENSION-" + species + "-" + widthHeight);
		 if (ph_DIMENSION != null) {
			 netFrameDimension = ph_DIMENSION.value;
		 }
	 }
	 document.getElementById("preHanging").innerHTML = CurrencyFormatted(preHanging);
	 document.getElementById("netFrameDimension").innerHTML = netFrameDimension; 

	 document.getElementById("price1").innerHTML = CurrencyFormatted(<c:out value="${price1}"/>);
	 document.getElementById("totalDoor").innerHTML = "<fmt:message key="${siteConfig['CURRENCY'].value}" />" + CurrencyFormatted(parseFloat(preHanging) + parseFloat(additionalOptions) + parseFloat(<c:out value="${price1}"/>)); 
	}
function CurrencyFormatted(amount)
{
	var i = parseFloat(amount);
	if(isNaN(i)) { i = 0.00; }
	var minus = '';
	if(i < 0) { minus = '-'; }
	i = Math.abs(i);
	i = parseInt((i + .005) * 100);
	i = i / 100;
	s = new String(i);
	if(s.indexOf('.') < 0) { s += '.00'; }
	if(s.indexOf('.') == (s.length - 2)) { s += '0'; }
	s = minus + s;
	return s;
}
//-->
</script>








<%--Old Code before Spring3 
<%
Set<String> optionCode = new HashSet<String>();
List<ProductOption> productOptions = ((Product) pageContext.getAttribute("product")).getProductOptions();
List<ProductOption> preHangingDoorOptions = (List<ProductOption>) ((Map<String, Object>) request.getAttribute("model")).get("preHangingDoorOptions");
productOptions.addAll(preHangingDoorOptions);
for (ProductOption productOption: productOptions) {
	if (!productOption.getType().equals("box")) {
		for (ProductOptionValue prodOptValue: productOption.getValues()) {
			if (productOption.getName().equalsIgnoreCase("Species") || productOption.getName().equalsIgnoreCase("Width & Height") || productOption.getName().equalsIgnoreCase("Jamb")) {
				out.println("<input type=\"hidden\" id=\"OPTION-" + productOption.getName() + "-VALUE-" + prodOptValue.getIndex() + "\" value=\"" + prodOptValue.getName() + "\">");				
			}
			if (prodOptValue.getOptionPrice() != null) {
				optionCode.add("OPTION-" + productOption.getName());
				out.println("<input type=\"hidden\" id=\"OPTION-" + productOption.getName() + "-PRICE-" + prodOptValue.getIndex() + "\" value=\"" + prodOptValue.getOptionPrice() + "\">");				
			}
		}
	}
}
for (Map<String, Object> preHanging: (List<Map<String, Object>>) ((Map<String, Object>) request.getAttribute("model")).get("preHanging")) {
	out.println("<input type=\"hidden\" id=\"ph_PRICE-" + preHanging.get("species") + "-" + preHanging.get("width_height") + "\" value=\"" + preHanging.get("price") + "\">");	
	out.println("<input type=\"hidden\" id=\"ph_DIMENSION-" + preHanging.get("species") + "-" + preHanging.get("width_height") + "\" value=\"" + preHanging.get("dimensions") + "\">");	
}
%>

<script language="JavaScript">
<!--
function togglePrehanging() {
  if (document.getElementById('OPTION-Pre-Hanging').value == '1') {
	document.getElementById('preHangingDoorImage').style.display="block";
	document.getElementById('preHangingDoorOptions').style.display="block";
<%
for (ProductOption productOption: preHangingDoorOptions) {
	if (!productOption.getType().equals("box")) {
		out.println("\tdocument.getElementById('OPTION-" + productOption.getName() + "').disabled=false;");				
	}
}
%>
  } else {
	document.getElementById('preHangingDoorImage').style.display="none";
	document.getElementById('preHangingDoorOptions').style.display="none";
<%
for (ProductOption productOption: preHangingDoorOptions) {
	if (!productOption.getType().equals("box")) {
		out.println("\tdocument.getElementById('OPTION-" + productOption.getName() + "').disabled=true;");				
	}
}
%>	
  }	 
  if (document.getElementById('OPTION-Handing').value == '0') {
	document.getElementById('preHangingDoorImage').setAttribute("src", "${_contextpath}/assets/Doors/${model.product.field1}-Left Swing.jpg");
  } else {
	document.getElementById('preHangingDoorImage').setAttribute("src", "${_contextpath}/assets/Doors/${model.product.field1}-Right Swing.jpg");
  }	

  // additional options
  var additionalOptions = 0.00;
<%
for (String code: optionCode) {
  out.println("  var el = document.getElementById(\"" + code + "-PRICE-\" + document.getElementById(\"" + code + "\").value);");
  out.println("  if (el != null) {");
  out.println("    additionalOptions = additionalOptions + parseFloat(el.value);");
  out.println("  }");
}
%>
 document.getElementById("additionalOptions").innerHTML = CurrencyFormatted(additionalOptions);

 // pre-hanging
 var preHanging = 0.00;
 var netFrameDimension = "";
 var elSpecies = document.getElementById('OPTION-Species');
 var elWidthHeight = document.getElementById('OPTION-Width & Height');
 var elJamb = document.getElementById('OPTION-Jamb');
 if (elSpecies != null && elWidthHeight != null) {
	 species = document.getElementById("OPTION-Species-VALUE-" + elSpecies.value).value;
	 widthHeight = document.getElementById("OPTION-Width & Height-VALUE-" + elWidthHeight.value).value;
	 var ph_PRICE = document.getElementById("ph_PRICE-" + species + "-" + widthHeight);
	 if (ph_PRICE != null) {
		 jamb = document.getElementById("OPTION-Jamb-VALUE-" + elJamb.value).value;
		 if (jamb.indexOf("10%") > 0) {
			 preHanging = ph_PRICE.value * 1.1; // add 10%
		 } else {
			 preHanging = ph_PRICE.value;
		 }	 
	 }
	 var ph_DIMENSION = document.getElementById("ph_DIMENSION-" + species + "-" + widthHeight);
	 if (ph_DIMENSION != null) {
		 netFrameDimension = ph_DIMENSION.value;
	 }
 }
 document.getElementById("preHanging").innerHTML = CurrencyFormatted(preHanging);
 document.getElementById("netFrameDimension").innerHTML = netFrameDimension; 

 document.getElementById("price1").innerHTML = CurrencyFormatted(<c:out value="${price1}"/>);
 document.getElementById("totalDoor").innerHTML = "<fmt:message key="${siteConfig['CURRENCY'].value}" />" + CurrencyFormatted(parseFloat(preHanging) + parseFloat(additionalOptions) + parseFloat(<c:out value="${price1}"/>)); 
}
function CurrencyFormatted(amount)
{
	var i = parseFloat(amount);
	if(isNaN(i)) { i = 0.00; }
	var minus = '';
	if(i < 0) { minus = '-'; }
	i = Math.abs(i);
	i = parseInt((i + .005) * 100);
	i = i / 100;
	s = new String(i);
	if(s.indexOf('.') < 0) { s += '.00'; }
	if(s.indexOf('.') == (s.length - 2)) { s += '0'; }
	s = minus + s;
	return s;
}
// end of function CurrencyFormatted()
//-->
</script>
--%>


</c:if>
