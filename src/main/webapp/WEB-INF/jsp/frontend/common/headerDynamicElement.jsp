<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>

<c:set value="${fn:replace(headerHtml, '#sku#', model.product.sku)}" var="headerHtml"/>
<c:set value="${fn:replace(headerHtml, '#weight#', model.product.weight)}" var="headerHtml"/>
<c:set value="${fn:replace(headerHtml, '#id#', model.product.id)}" var="headerHtml"/>
<c:set value="${fn:replace(headerHtml, '#name#', model.product.name)}" var="headerHtml"/>
<c:set value="${fn:replace(headerHtml, '#namewithoutquotes#', model.product.nameWithoutQuotes)}" var="headerHtml"/>
<c:set value="${fn:replace(headerHtml, '#msrp#', model.product.msrp)}" var="headerHtml"/>
<c:set value="${fn:replace(headerHtml, '#price1#', model.product.price1)}" var="headerHtml"/>
<c:set value="${fn:replace(headerHtml, '#email#', userSession.username)}" var="headerHtml"/>
<c:set value="${fn:replace(headerHtml, '#firstname#', userSession.firstName)}" var="headerHtml"/>
<c:set value="${fn:replace(headerHtml, '#lastname#', userSession.lastName)}" var="headerHtml"/>