<%@ page import="com.webjaguar.thirdparty.payment.ebillme.*,org.jdom.*,org.jdom.input.SAXBuilder,java.io.*,java.util.*,java.text.*" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<table>
 <c:if test="${frontend == null}">
 <tr>
  <td align="right">Auth Status:</td>
  <td><c:out value="${order.eBillme.authstatus}"/></td>
 </tr>
 </c:if>
 <tr>
  <td align="right">Order Ref ID:</td>
  <td><c:out value="${order.eBillme.orderrefid}"/></td>
 </tr>
 <tr>
  <td align="right">Buyer's Account #:</td>
  <td><c:out value="${order.eBillme.buyer_accountnumber}"/></td>
 </tr>
<c:set var="eBillme" value="${order.eBillme}"/>
<c:out value="${ebillmeXml}" escapeXml="false"></c:out>
<%--
EBillme eBillme = (EBillme) pageContext.getAttribute("eBillme");
if (eBillme.getXmlFile() != null && (pageContext.getAttribute("frontend") == null)) {
	File eBillmeDir = new File(getServletContext().getRealPath("/eBillme/processed"));	
	SAXBuilder builder = new SAXBuilder(false);

	String xmlFile[] = eBillme.getXmlFile().split(",");
	for (int xml=0; xml<xmlFile.length; xml++) {
		Document doc = builder.build(new File(eBillmeDir, xmlFile[xml]));
		Element paymentchanges = doc.getRootElement().getChild("paymentchanges", Namespace.getNamespace("http://ebillme.com/ws/v3"));
		Iterator orders = paymentchanges.getChildren().iterator();
		while (orders.hasNext()) {
			Element order = (Element) orders.next();
			if (eBillme.getOrderrefid().equals(order.getChildText("orderrefid", Namespace.getNamespace("http://ebillme.com/ws/v3")))) {
				// matching orderrefid

				// show only on last payment
				if (xml == 0) {
					
					Double amountowing = Double.parseDouble(order.getChildText("amountowing", Namespace.getNamespace("http://ebillme.com/ws/v3")));
					String paystatus = order.getChildText("paystatus", Namespace.getNamespace("http://ebillme.com/ws/v3"));
					out.println("<tr>");
					out.println("<td valign=\"top\" align=\"right\">Pay Status:</td>");
					if (paystatus.equals("F") && amountowing < 0) {
						out.println("<td><span style=\"color:#FF0000;font-weight:bold\">Overpayment</span> - Please note the customer has paid more than the order amount.<br/>You may wish to initiate a refund, provide a credit notice for the customer.</td>");						
					} else if (paystatus.equals("F") && amountowing == 0) {
						out.println("<td><span style=\"color:#FF0000;font-weight:bold\">Exact payment</span> - Please note the full payment has been made and you can fulfill the order.</td>");						
					} else if (paystatus.equals("P") && amountowing > 0) {
						out.println("<td><span style=\"color:#FF0000;font-weight:bold\">Underpayment</span> - Please note the full payment has not been received yet, please contact<br/>the customer and request they send the balance in order for them to receive their order.</td>");						
					} else {
						out.println("<td>" + paystatus + "</td>");						
					}
					out.println("</tr>");			

					out.println("<tr>");
					out.println("<td align=\"right\">Payment Date:</td>");
					out.println("<td>" + order.getChildText("paymentdate", Namespace.getNamespace("http://ebillme.com/ws/v3")) + "</td>");
					out.println("</tr>");	
					
					out.println("<tr>");
					out.println("<td align=\"right\">Amount Paid to Date:</td>");
					out.println("<td>" + order.getChildText("amountpaidtodate", Namespace.getNamespace("http://ebillme.com/ws/v3")) + "</td>");
					out.println("</tr>");
					
					if (amountowing != 0) {
						NumberFormat nf = new DecimalFormat("#0.00");
						out.println("<tr>");
						out.println("<td align=\"right\">Amount Owing:</td>");
						out.println("<td>" + nf.format(amountowing) + "</td>");
						out.println("</tr>");						
					}
					
					Element suspectdetails = order.getChild("suspectdetails", Namespace.getNamespace("http://ebillme.com/ws/v3"));
					if (suspectdetails != null) {
						out.println("<tr>");
						out.println("<td align=\"right\">Suspect Status:</td>");
						out.println("<td>" + suspectdetails.getChildText("suspectstatus", Namespace.getNamespace("http://ebillme.com/ws/v3")) + "</td>");
						out.println("</tr>");	
						out.println("<tr>");
						out.println("<td align=\"right\">Suspect Reasons:</td>");
						out.println("<td>" + suspectdetails.getChild("suspectreasons", Namespace.getNamespace("http://ebillme.com/ws/v3")).getChildText("reasoncode", Namespace.getNamespace("http://ebillme.com/ws/v3")) + "</td>");
						out.println("</tr>");							
					}
					
					out.println("<tr>");
					out.print("<td align=\"right\" style=\"font-weight:bold\">Recent Payments</td>");				
					out.println("<td>&nbsp;</td>");
					out.println("</tr>");				
				} else if (xml == 1) {
					out.println("<tr>");
					out.print("<td align=\"right\" style=\"font-weight:bold\">Old Payments</td>");				
					out.println("<td>&nbsp;</td>");
					out.println("</tr>");									
				}
				
				Element payments = order.getChild("payments", Namespace.getNamespace("http://ebillme.com/ws/v3"));
				Iterator paymentdetailsIter = payments.getChildren().iterator();
				while (paymentdetailsIter.hasNext()) {
					Element paymentdetails = (Element) paymentdetailsIter.next();
					out.println("<tr>");
					out.println("<td align=\"right\">Name:</td>");
					out.println("<td>" + paymentdetails.getChildText("name", Namespace.getNamespace("http://ebillme.com/ws/v3")) + "</td>");
					out.println("</tr>");				
					out.println("<tr>");
					out.println("<td align=\"right\">Amount Paid:</td>");
					out.println("<td>" + paymentdetails.getChildText("amountpaid", Namespace.getNamespace("http://ebillme.com/ws/v3")) + "</td>");
					out.println("</tr>");				
					out.println("<tr>");
					out.println("<td align=\"right\">Payment Date:</td>");
					out.println("<td>" + paymentdetails.getChildText("paymentdate", Namespace.getNamespace("http://ebillme.com/ws/v3")) + "</td>");
					out.println("</tr>");				
					out.println("<tr>");
					out.println("<td align=\"right\">Settlement Date:</td>");
					out.println("<td>" + paymentdetails.getChildText("settlementdate", Namespace.getNamespace("http://ebillme.com/ws/v3")) + "</td>");
					out.println("</tr>");				
					out.println("<tr>");
					out.println("<td align=\"right\">Payment Source:</td>");
					out.println("<td>" + paymentdetails.getChildText("paymentsource", Namespace.getNamespace("http://ebillme.com/ws/v3")) + "</td>");
					out.println("</tr>");								
					out.println("<td>&nbsp;</td>");
					out.println("<td>-------------------</td>");
					out.println("</tr>");
				}
				
			}
		}
	}
}
--%>
</table>
