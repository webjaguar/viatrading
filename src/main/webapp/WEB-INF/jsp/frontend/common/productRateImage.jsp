<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<%
double productRateAverage = 0;
try {
  productRateAverage = Double.parseDouble(request.getParameter("productRateAverage"));
} catch (Exception e) {}
pageContext.setAttribute("productRateAverage" , productRateAverage);
%>

<c:choose>
   <c:when test="${productRateAverage > 0 and productRateAverage <= 0.6}"><c:set var="star" value="0h"/></c:when>
   <c:when test="${productRateAverage > 0.6 and productRateAverage <= 1}"><c:set var="star" value="1"/></c:when>
   <c:when test="${productRateAverage > 1 and productRateAverage <= 1.6}"><c:set var="star" value="1h"/></c:when>
   <c:when test="${productRateAverage > 1.6 and productRateAverage <= 2}"><c:set var="star" value="2"/></c:when>
   <c:when test="${productRateAverage > 2 and productRateAverage <= 2.6}"><c:set var="star" value="2h"/></c:when>
   <c:when test="${productRateAverage > 2.6 and productRateAverage <= 3}"><c:set var="star" value="3"/></c:when>
   <c:when test="${productRateAverage > 3 and productRateAverage <= 3.6}"><c:set var="star" value="3h"/></c:when>
   <c:when test="${productRateAverage > 3.6 and productRateAverage <= 4}"><c:set var="star" value="4"/></c:when>
   <c:when test="${productRateAverage > 4 and productRateAverage <= 4.6}"><c:set var="star" value="4h"/></c:when>
   <c:when test="${productRateAverage > 4.6}"><c:set var="star" value="5"/></c:when>
   <c:otherwise><c:set var="star" value="0"/></c:otherwise>
</c:choose>
<img src="${_contextpath}/assets/Image/Layout/star_${star}.gif" alt="<c:out value="${param.productRateAverage}"/>" title="<c:out value="${param.productRateAverage}"/>" border="0" />
      