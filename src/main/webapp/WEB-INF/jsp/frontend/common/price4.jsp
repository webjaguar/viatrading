<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>

<jsp:useBean id="now" class="java.util.Date" />
<c:if test="${product.msrp > 0 || siteConfig['SHOW_ZERO_PRICE'].value == 'true'}">
    <div id="msrp_id" class="msrp">
	<div class="details_reg_priceTitle" >Regular Price: </div><div class="details_reg_price" ><fmt:message key="${siteConfig['CURRENCY'].value}" /> <fmt:formatNumber value="${product.msrp}" pattern="#,##0.00" /></div>
	  <c:choose>
	    <c:when test="${product.salesTag != null and product.salesTag.discount != 0.0}">
	      <div class="details_discountTitle" >Instant Savings <div class="discount_date">through <fmt:formatDate value="${now}"  pattern="MM-dd" />:</div> </div><div class="details_discount" > - <fmt:message key="${siteConfig['CURRENCY'].value}" /> <fmt:formatNumber value="${product.msrp - product.price[0].discountAmt}" pattern="#,##0.00" /></div>
	      <div class="details_sale_priceTitle" ><fmt:message key="f-price" />:</div><div class="details_sale_price" ><fmt:message key="${siteConfig['CURRENCY'].value}" /> <fmt:formatNumber value="${product.price[0].discountAmt}" pattern="#,##0.00" /></div>
	      <input id="hidden_sale_price" type="hidden" value="${product.price[0].discountAmt}">
	    </c:when>
	    <c:otherwise>
	      <div class="details_discountTitle" >Instant Savings <div class="discount_date">through <fmt:formatDate value="${now}"  pattern="MM-dd" />:</div> </div><div class="details_discount" > - <fmt:message key="${siteConfig['CURRENCY'].value}" /> <fmt:formatNumber value="${product.msrp - product.price1}" pattern="#,##0.00" /></div>
	      <div class="details_sale_priceTitle" ><fmt:message key="f-price" />:</div><div class="details_sale_price" id="sale_price"><fmt:message key="${siteConfig['CURRENCY'].value}" /> <fmt:formatNumber value="${product.price1}" pattern="#,##0.00" /></div>
	      <input id="hidden_sale_price" type="hidden" value="${product.price1}">
	    </c:otherwise>
	  </c:choose>
	  <c:if test="${product.mailInRebate.discount != null}">
	    <c:choose>
	      <c:when test="${product.mailInRebate.percent}">
	        <c:choose>
	          <c:when test="${product.salesTag != null and product.salesTag.discount != 0.0}">
	            <c:set var="rebateDiscount" value="${product.price[0].discountAmt*product.mailInRebate.discount/100}" />
	            <div class="details_rebateTitle" >Mail-In Rebate(s) :</div><div class="details_rebate" >- <fmt:message key="${siteConfig['CURRENCY'].value}" /> <fmt:formatNumber value="${rebateDiscount}" pattern="#,##0.00" /></div>
	            <div class="details_net_priceTitle" >Price After Rebate(s): </div><div class="details_net_price" ><fmt:message key="${siteConfig['CURRENCY'].value}" /><fmt:formatNumber value="${product.price[0].discountAmt - rebateDiscount}" pattern="#,##0.00" /></div>
	            <input id="hidden_sale_price" type="hidden" value="${product.price[0].discountAmt - rebateDiscount}">
	          </c:when>
	          <c:otherwise>
	            <c:set var="rebateDiscount" value="${product.price1*product.mailInRebate.discount/100}" />
	            <div class="details_rebateTitle" >Mail-In Rebate(s) :</div><div class="details_rebate" >- <fmt:message key="${siteConfig['CURRENCY'].value}" /> <fmt:formatNumber value="${rebateDiscount}" pattern="#,##0.00" /></div>
	            <div class="details_net_priceTitle" >Price After Rebate(s): </div><div class="details_net_price" ><fmt:message key="${siteConfig['CURRENCY'].value}" /><fmt:formatNumber value="${product.price1 - rebateDiscount}" pattern="#,##0.00" /></div>
	            <input id="hidden_sale_price" type="hidden" value="${product.price1 - rebateDiscount}">
	          </c:otherwise>
	        </c:choose>
	      </c:when>
	      <c:otherwise>
	        <c:choose>
	          <c:when test="${product.salesTag != null and product.salesTag.discount != 0.0}">
	            <div class="details_rebateTitle" >Mail-In Rebate(s) :</div><div class="details_rebate" >- <fmt:message key="${siteConfig['CURRENCY'].value}" /> <fmt:formatNumber value="${product.mailInRebate.discount}" pattern="#,##0.00" /></div>
	            <div class="details_net_priceTitle" >Price After Rebate(s): </div><div class="details_net_price" ><fmt:message key="${siteConfig['CURRENCY'].value}" /><fmt:formatNumber value="${product.price[0].discountAmt - product.mailInRebate.discount}" pattern="#,##0.00" /></div>
	            <input id="hidden_sale_price" type="hidden" value="${product.price[0].discountAmt - product.mailInRebate.discount}">
	          </c:when>
	          <c:otherwise>
	            <div class="details_rebateTitle" >Mail-In Rebate(s) :</div><div class="details_rebate" >- <fmt:message key="${siteConfig['CURRENCY'].value}" /> <fmt:formatNumber value="${product.mailInRebate.discount}" pattern="#,##0.00" /></div>
	            <div class="details_net_priceTitle" >Price After Rebate(s): </div><div class="details_net_price" ><fmt:message key="${siteConfig['CURRENCY'].value}" /><fmt:formatNumber value="${product.price1 - product.mailInRebate.discount}" pattern="#,##0.00" /></div>
	            <input id="hidden_sale_price" type="hidden" value="${product.price1 - product.mailInRebate.discount}">
	          </c:otherwise>
	        </c:choose>
	      </c:otherwise>
	    </c:choose>
     </c:if>
	</div>
</c:if>