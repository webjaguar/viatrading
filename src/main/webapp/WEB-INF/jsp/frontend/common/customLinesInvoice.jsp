<%@page import="com.webjaguar.model.LineItem"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ taglib uri="/WEB-INF/tlds/custom.tld" prefix="wj" %>

<%--Spring3 DONE--%>
<c:set var="tempFirstLine"/>
<c:set var="newLine" value="\n"/>
<c:if test="${lineItem.customLines != null}" >
<c:set var="num" value="1" />
<c:set var="size" value="${fn:length(lineItem.customLines)}" />
	<c:forEach items="${lineItem.customLines}" var="customLine" varStatus="key">
		<tr class="invoice_lineitem_attributes" >
			<td align="right" valign="top" style="white-space: nowrap">
				<c:if test="${key.index == 0}">
				  <c:set var="tempFirstLine" value="${customLine.key}"/>
				  <c:set var="tempFirstLine" value="${fn:replace(tempFirstLine, newLine, '')}"/>
				</c:if>
				
				<c:choose>
					<c:when test="${(empty tempFirstLine) and (key.index == 0)}">
						<span class="optionName" >Without Inscription</span>
						<c:if test="${size > 1}"><span class=optionName" >:</span></c:if>
					</c:when>
					<c:otherwise>
						<span class="optionName" >Inscription</span>
						<c:if test="${size > 1}">
							<c:out value="${num}"></c:out>
							<c:set var="num" value="${num + 1}" />
						</c:if>
						<span class="optionName" >:</span>
					</c:otherwise>
				</c:choose>
			</td>
			<td>
				<c:if test="${size > 1}">				
					<c:out value="Quantity"></c:out><c:out value="${customLine.value}"></c:out>
					<br/>
				</c:if>
				
				<c:if test="${!((tempFirstLine == null or tempFirstLine == '') and num == 1)}">
				   <c:forEach var="value" items="${wj:split(customLine.key, newLine)}" varStatus="status">
				     <c:out value="${status.index + 1} - ${fn:trim(value)}"/>
				     <br/>
				   </c:forEach>   
				</c:if>
			</td>
		</tr>
	</c:forEach>
</c:if>


   
<%--
LineItem lineItem = (LineItem) pageContext.getAttribute("lineItem");

if (lineItem.getCustomLines() != null) {
  int key = 0;
  int num = 1;
  int size = lineItem.getCustomLines().size();
  boolean woutInsc = lineItem.getCustomLines().firstKey().replace("\n", "").trim().equals("");
  for (String customLine: lineItem.getCustomLines().keySet()) {		
    out.println("<tr class=\"invoice_lineitem_attributes\">");
	out.println("<td align=\"right\" valign=\"top\" style=\"white-space: nowrap\">");
	if (woutInsc && key == 0) {
	  out.print("<span class=\"optionName\" >Without Inscription</span>");
	  if (size > 1) {
	    out.println("<span class=\"optionName\" >:</span>");
	  }
	} else {
	  out.print("<span class=\"optionName\" >Inscription</span>");
	  if (size>1) {
	    out.print(" " + num++);
	  }
	  out.println("<span class=\"optionName\" >:</span>");
	}
	out.println("</td>"); 
	out.println("<td>"); 
	if (size > 1) {
	  out.println("Quantity " + lineItem.getCustomLines().get(customLine) + "<br/>");
	}
	if (!(woutInsc && num == 1)) {
	  String[] values = customLine.split("\n");
	  for (int x=0; x<values.length; x++) {
		out.println((x+1) + "- " + values[x].trim() + "<br/>");
	  }
	}
	out.println("</td>"); 
    out.println("</tr>");
	key++;
  }
}

--%>
