<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>

<c:if test="${ model.optionList != null and not empty model.optionList}">

<div class="multiOptionsWrapper" id="multiOptionsWrapperId">
<div class="multiOptions">

<input type="hidden" id="totalOption" value="${fn:length(model.optionList)}">

<c:forEach items="${model.optionList}" var="productOption" varStatus="status">
<c:choose>
  <c:when test="${!fn:contains(productOption.optionCode, 'Review Detail')}">
  <input type="hidden" name="optionCode_${model.productId}" value="<c:out value="${productOption.optionCode}"/>">	
  <div style="display:none;">
  <c:set var="optName" value="${productOption.optionCode}-option_values_${model.productId}_${productOption.index}"></c:set>
  <c:set var="optValue"></c:set>
  <c:forEach var="entry" items="${model.originalIndexSelection}">
     <c:if test="${optName == entry.key}">
       <c:set var="optValue" value="${entry.value}"></c:set>  
     </c:if>
  </c:forEach>
  <c:set var="custOptName" value="${productOption.optionCode}-option_values_cus_with_index_${model.productId}_${productOption.index}"></c:set>
  <c:set var="customWidthOptValue"></c:set>
  <c:set var="customHeightOptValue"></c:set>
  <c:set var="customSizeOptValue"></c:set>
  <c:set var="customSizeOptIndex" value="0"></c:set>
     <c:forEach var="entry" items="${model.originalValueSelection}">
       <c:if test="${custOptName == entry.key}">
         <c:set var="customSizeOptValue" value="${fn:split(entry.value,'_')[1]}"></c:set>  
         <c:set var="customSizeOptIndex" value="${fn:split(entry.value,'_')[0]}"></c:set>  
         <c:set var="customWidthOptValue" value="${fn:split(customSizeOptValue,'X')[0]}"></c:set>  
         <c:set var="customHeightOptValue" value="${fn:split(customSizeOptValue,'X')[1]}"></c:set>  
     </c:if>
  </c:forEach>
  <c:if test="${customSizeOptIndex == -1}">
    <c:set var="customSizeOptIndex" value="0"></c:set>
  </c:if>     
  </div>
  
  <c:if test="${!empty productOption.values}">
  <input type="hidden" name="<c:out value="${productOption.optionCode}"/>-option_${model.productId}" value="<c:out value="${productOption.index}"/>"/>
  <div class="optionWrapper">
    <div class="optionTitleBox">
      <c:out value="${status.index + 1}"/>. <c:out value="${productOption.name}"/>
    </div>
    <div class="optionMultiValuesBox">
    <c:if test="${fn:contains(productOption.optionCode, 'Choose Your Diploma Size')}">
     <input type="hidden" name="<c:out value="${productOption.optionCode}"/>-option_values_cus_with_index_${model.productId}_${productOption.index}" value="${customSizeOptIndex}_${customSizeOptValue}" id="customSize"/>
     <input type="hidden" name="customSizeOptIndex" value="${customSizeOptIndex}" id="customSizeOptIndex"/>
     <div class="customSizeTitle">Choose Custom Size:</div>
  	 <div class="customSizeOptionValueBox">
     <select class="customSizeSelect" onchange="toggleSelection(false); assignValue('customSize', this.value);"  name="customWidth" id="customWidth">
     	<option value="0">W</option>
        <c:forEach begin="600" end="2000" var="customWidth" step="25">
       	  <option value="${customWidth/100}" <c:if test="${customWidth/100 == customWidthOptValue}">selected="selected"</c:if> ><c:out value="${customWidth/100}"/>"</option>
        </c:forEach>
      </select>
    
      <select class="customSizeSelect" onchange="toggleSelection(false); assignValue('customSize', this.value); " name="customHeight" id="customHeight">
     	<option value="0">H</option>
        <c:forEach begin="600" end="2000" var="customHeight" step="25">
       	  <option value="${customHeight/100}" <c:if test="${customHeight/100 == customHeightOptValue}">selected="selected"</c:if>><c:out value="${customHeight/100}"/>"</option>
        </c:forEach>
      </select>
      </div>
	  <div class="line"></div>
      
      <div class="customSizeTitle">Choose Popular Size:</div>
  	</c:if>
     
    <c:forEach items="${productOption.values}" var="prodOptValue" varStatus="povStatus">
      <c:set value="optionValueBox" var="valueClass"/>
      <c:if test="${prodOptValue.index == optValue}">
        <c:set value="optionValueBox optionSelectedValueBox" var="valueClass"></c:set>
      </c:if>
      <div class="${valueClass}" align="center">
  	    <img class="optionBigImageBox" src="${_contextpath}/assets/Image/Product/options/${prodOptValue.imageUrl}">
        <div class="optionValueName"><c:out value="${prodOptValue.name}"/></div>
        <div class="optionValueDesc"><c:out value="${prodOptValue.description}" escapeXml="flase"/></div>
        <div class="optionSelectBox">
           Select : <input type="radio" class="selectedOption" name="<c:out value="${productOption.optionCode}"/>-option_values_${model.productId}_<c:out value="${productOption.index}"/>" value="<c:out value="${prodOptValue.index}"/>"  <c:if test="${prodOptValue.index == optValue}">checked="checked"</c:if> 
           onclick="
    	     <c:if test="${fn:contains(productOption.optionCode, 'Choose Your Diploma Size')}">
    	       toggleSelection(true);
    	     </c:if> 
    	    getOption('${model.productId}');"> 
        </div> 
      </div>
      <c:if test="${(povStatus.index+1) % 3 == 0}">
        <div style="clear: both;"></div><br/>
      </c:if>
      
    </c:forEach>
    <div style="clear: both;"></div>
    </div>
  </div>
  </c:if>
  </c:when>
  <c:otherwise>
    <%@ include file="/WEB-INF/jsp/frontend/framestore/customProduct.jsp" %>
  </c:otherwise>
</c:choose>

</c:forEach>
</div>

<div id="summaryBoxId">
  <div id="summaryTitle">Summary</div>
  <div id="summaryContent"> 
    <span id="summaryPrice" > 
      <span id="summaryPriceTitle">Price: </span>
      <span id="summaryPriceTitleValue" <c:if test="${model.discountedPrice != null }"> style="text-decoration: line-through;" </c:if>>$
        <c:choose>
          <c:when test="${model.price == null}">0.00</c:when>
          <c:otherwise><fmt:formatNumber value="${model.price}" pattern="#,##0.00#" maxFractionDigits="${siteConfig['MAX_FRACTION_DIGIT'].value}" /></c:otherwise>
        </c:choose>
      </span>
	</span>
	<br/>
    <c:if test="${model.discountedPrice != null }">
    <span id="summarySalePrice">
      <span id="summarySalePriceTitle">Sale Price: </span>
      <span id="summarySalePriceTitleValue">$
        <c:choose>
          <c:when test="${model.discountedPrice == null}">0.00</c:when>
          <c:otherwise><fmt:formatNumber value="${model.discountedPrice}" pattern="#,##0.00#" maxFractionDigits="${siteConfig['MAX_FRACTION_DIGIT'].value}" /></c:otherwise>
        </c:choose>
	  </span>
	</span>
	<br/>
    </c:if>
    
    <c:forEach items="${model.selectedOptions}" var="selectedOption">
      <span> <c:out value="${selectedOption.value}"></c:out> </span> <br/>
    </c:forEach>
  
  </div>

</div>

<div style="clear: both;"></div> 
</div>

</c:if>