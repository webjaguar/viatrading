<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<script type="text/javascript">
<!--
function showAttachmentInput(prodOptIndex, valueIndex){
	$$('td.attachment_'+prodOptIndex).each(function(attachment){
		attachment.destroy();
	});
	$$('input.assignedSku'+prodOptIndex+'_'+valueIndex).each(function(input){
		var sku = input.value;
		if(sku != null && sku != ''){
			var row=$(prodOptIndex).getParent().getParent();
			var td = document.createElement('td');
			td.setAttribute("class", "attachment_"+prodOptIndex); 
			row.appendChild(td);
			td.innerHTML='<input value="browse" type="file" name="attachment_'+sku+'"/>';
		}
	});
}
//-->
</script>

<c:if test="${empty product.numCustomLines and not empty product.productOptions}">
<table cellspacing="0" cellpadding="0" border="0" class="productOption">


<c:forEach items="${product.productOptions}" var="productOption" varStatus="ponStatus">
<input type="hidden" name="optionCode_${product.id}" value="<c:out value="${productOption.optionCode}"/>">	
<c:forEach items="${productOption.values}" var="prodOptValue" varStatus="povStatus">
  <c:if test="${prodOptValue.includedProductList != null}">
	<c:forEach items="${prodOptValue.includedProductList}" var="includedProduct" varStatus="productStatus">
	  <c:if test="${includedProduct.attachment}">
	    <div style="display: none; visibility: hidden;">
		  <input type="hidden" id="optionIndex${ponStatus.index }_${prodOptValue.index}" value="${productOption.index}">	
		  <input type="hidden" id="valueIndex${ponStatus.index}_${prodOptValue.index}" value="${prodOptValue.index}">	
		  <input type="hidden" class="assignedSku${ponStatus.index}_${prodOptValue.index}" value="${includedProduct.sku}">	
	    </div>
  	  </c:if>
  	</c:forEach>
  </c:if>
</c:forEach>


<c:if test="${productOption.type != 'box'}" >
<c:forEach items="${productOption.values}" var="prodOptValue" varStatus="povStatus">

<c:if test="${povStatus.first}">
<tr>
<td class="optionName" align="right"><c:out value="${productOption.name}"/>: </td>
<td>
  <input type="hidden" name="<c:out value="${productOption.optionCode}"/>-option_${product.id}" value="<c:out value="${productOption.index}"/>">
  <c:set var="productOptionValue" value="optionValue_${product.id}"></c:set>
  <select name="<c:out value="${productOption.optionCode}"/>-option_values_${product.id}_<c:out value="${productOption.index}" />" 
          class="optionSelect"
          <c:choose>
          <c:when test="${not empty model.preHangingDoorOptions}">
    		onChange="togglePrehanging()"
    		id="OPTION-${productOption.name}"
  		  </c:when>
  		  <c:otherwise>
  		    onchange="showAttachmentInput('${ponStatus.index}', this.value)"
    		id="${ponStatus.index}"
  		  </c:otherwise>
  		  </c:choose>
  >
</c:if>
  	  <option value="<c:out value="${prodOptValue.index}"/>" 
  	          <c:if test="${prodOptValue.index == model[productOptionValue]}" >selected="selected"</c:if>
  	           id="${ponStatus.index}-${productOption.index}-${prodOptValue.index}" 
  	  > 
  	          <c:out value="${prodOptValue.name}"/>
  	          <c:if test="${prodOptValue.optionPrice != null}">
  	            <c:choose>
  	              <c:when test="${prodOptValue.optionPrice > 0}"> + </c:when>
  	              <c:otherwise> - </c:otherwise>
  	            </c:choose>
  	            <fmt:message key="${siteConfig['CURRENCY'].value}" /><fmt:formatNumber value="${prodOptValue.absoluteOptionPrice}" pattern="#,##0.00" />
  	          </c:if>
<c:if test="${povStatus.last}">  	  
  </select>
</td>
</tr>
</c:if>
</c:forEach>
<c:if test="${siteConfig['CUSTOM_TEXT_PRODUCT'].value == 'true'}">
  <c:if test="${empty productOption.values}">
   <tr>
    <td class="optionName" align="right"><c:out value="${productOption.name}"/>: </td>
    <td>
    <input type="hidden" name="<c:out value="${productOption.optionCode}"/>-option_${product.id}" value="<c:out value="${productOption.index}"/>">
    <c:set var="productOptionCusValue" value="optionValueCus_${product.id}"></c:set>
    <input class="optionCustomText" value="${model[productOptionCusValue]}" name="<c:out value="${productOption.optionCode}"/>-option_values_cus_${product.id}_<c:out value="${productOption.index}" />" maxlength="110"></input>
    </td>
   </tr>
  </c:if>
</c:if>
</c:if>
</c:forEach>
</table>
</c:if>