<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>

<div class="qtyBlock">
  <c:choose>
    <c:when test="${empty model.slavesList}">
      <c:set var="showAddToCart" value="true" />
      <input type="hidden" value="${product.id}" name="product.id"/>
    </c:when>
    <c:otherwise>
      <c:choose>
        <c:when test="${model.product.productLayout == '018' or (model.product.productLayout == '' and siteConfig['PRODUCT_DEFAULT_DETAIL_LAYOUT'].value == '018')}">
          <!-- Row starts -->
  		  <div class="fieldRow">
	        <!-- column starts -->
    	    <div class="bold left qty"></div>
    	    <!-- column ends -->
    
    	    <!-- column starts -->
    	    <div class="space"></div>
    	    <!-- column ends -->

      	    <!-- column starts -->
  	  		<div class="bold center"></div>
     	    <!-- column ends -->
    	  </div>
    	<!-- Row ends -->
    	
    	<!-- Row stars -->
   		<div class="childSkuRow">
          <!-- column starts -->
    	  <div class="bold left"> 
			<fmt:message key="select" />:
		  </div>
          <!-- column ends -->
    	  	  
    	  <!-- column starts -->
          <div class="bold">
            <select name="childSelect" onchange="updateChildProduct(this.value)">
			  <option value="">Please Select</option>
			  <c:forEach items="${model.slavesList}" var="slaveProduct" varStatus="statusPrice">
		  	    <c:if test="${(!empty slaveProduct.price or slaveProduct.priceByCustomer) and (!slaveProduct.loginRequire or userSession != null)}">
		          <c:if test="${(slaveProduct.type != 'box') and (empty slaveProduct.numCustomLines)}">
			        <c:if test="${!(gSiteConfig['gINVENTORY'] and slaveProduct.inventory != null and !slaveProduct.negInventory and slaveProduct.inventory <= 0)}">
			          <c:set var="showAddToCart" value="true" />
		              <option value="${slaveProduct.id}">
			          <c:forEach items="${slaveProduct.productFields}" var="productField">
			            <c:out value="${productField.value}" escapeXml="false" />
			          </c:forEach>
			 	      </option>
		            </c:if>
			      </c:if>
		        </c:if>
		      </c:forEach>
			</select>
		    <input type="hidden" value="" name="product.id" id="product.id" />
		  </div>
	      <!-- column ends -->
        </div>
        <!-- Row ends -->
        </c:when>
        <c:otherwise>
          <table border="0" cellpadding="0" cellspacing="0" class="qtyGrid">
            <tr class="fieldRow">
              <td class="bold left qty"></td>
              
              <c:forEach items="${model.slavesList}" var="slaveFieldName" varStatus="statusPrice">
      	      <!-- column starts -->
    		  <td class="bold center">
    		    <c:forEach items="${slaveFieldName.productFields}" var="productField">
	              <c:out value="${productField.value}" escapeXml="false" />
	            </c:forEach>
    		  </td>
    		  </c:forEach>
            </tr>
            <tr class="qtyRow" <c:if test="${multiPrice and product.endQtyPricing and !(product.salesTag != null and product.salesTag.discount != 0.0)}">style="text-decoration: line-through;"</c:if>>
              <td class="bold left"> 
			    <fmt:message key="quantity" />:
			  </td>
           	  
    	  	  <c:forEach items="${model.slavesList}" var="slaveProduct" varStatus="statusPrice">
		      <input type="hidden" value="${slaveProduct.id}" name="product.id"/>
		      <c:if test="${gSiteConfig['gSHOPPING_CART'] and (model.slavesShowQtyColumn ==  null or model.slavesShowQtyColumn)}">
		        <c:set var="zeroPrice" value="false"/>
		        <c:forEach items="${slaveProduct.price}" var="price">
		          <c:if test="${price.amt == 0}">
		            <c:set var="zeroPrice" value="true"/>
		          </c:if>
		        </c:forEach>
	    	    <!-- column starts -->
    			<td class="bold center"> 
    			  <c:choose>
		            <c:when test="${slaveProduct.type == 'box'}">
		              <a href="${productLink}"><img border="0" src="${_contextpath}/assets/Image/Layout/button_buildthebox.gif" /></a>
		            </c:when>
		            <c:when test="${gSiteConfig['gVARIABLE_PRICE'] and slaveProduct.priceByCustomer}">
		              <c:set var="showAddToCart" value="true" />
		              <input type="checkbox" name="quantity_${slaveProduct.id}" id="quantity_${slaveProduct.id}" value="${slaveProduct.minimumQty}" />
		            </c:when>
		            <c:when test="${!empty slaveProduct.numCustomLines}">
		              <a href="${productLink}"><img border="0" src="${_contextpath}/assets/Image/Layout/button_continue.gif" /></a>
		            </c:when>
		            <c:otherwise>
		              <c:choose>
		                <c:when test="${gSiteConfig['gINVENTORY'] and slaveProduct.inventory != null and !slaveProduct.negInventory and slaveProduct.inventory <= 0}" >
		                  <c:out value="${siteConfig['OUT_OF_STOCK_TITLE'].value}" escapeXml="false"/>
		                </c:when>
		                <c:when test="${zeroPrice and siteConfig['ZERO_PRICE_HTML'].value != ''}">
		                  <c:out value="${siteConfig['ZERO_PRICE_HTML'].value}" escapeXml="false"/>
		                </c:when>
		                <c:otherwise>
		                  <c:set var="showAddToCart" value="true" />
		                  <input type="text" size="5" maxlength="5" name="quantity_${slaveProduct.id}" id="quantity_${slaveProduct.id}" value="<c:out value="${cartItem.quantity}"/>" ><c:if test="${slaveProduct.packing != ''}"> <c:out value="${slaveProduct.packing}"/> </c:if>
		                  <c:if test="${gSiteConfig['gINVENTORY'] and slaveProduct.inventory != null and slaveProduct.inventory < slaveProduct.lowInventory}">
		                    <c:out value="${siteConfig['LOW_STOCK_TITLE'].value}" escapeXml="false"/>
		                  </c:if>
		                </c:otherwise>
		              </c:choose>
		            </c:otherwise>
		          </c:choose>
		        </td>
        	   	<!-- column ends -->
		      </c:if>
		      </c:forEach>
		    </tr>
          </table>
        </c:otherwise>
      </c:choose>
    </c:otherwise>
  </c:choose>
</div>