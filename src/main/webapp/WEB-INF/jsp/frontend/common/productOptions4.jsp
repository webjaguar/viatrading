<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>

<c:if test="${empty product.numCustomLines}">
<div class="dynamic_options">

<c:forEach items="${product.productOptions}" var="productOption">
  <input type="hidden" name="optionCode_${product.id}" value="<c:out value="${productOption.optionCode}"/>">
  <c:set var="optionPriceList"></c:set>
  <c:if test="${productOption.type != 'box'}" >
    <c:if test="${fn:length(productOption.values) > 0}">
      <div class="dynamic_option">
        <div class="dynamic_optionName"><c:out value="${productOption.name}"/>: </div>
        <div class="dynamic_optionValue">
          <input type="hidden" name="<c:out value="${productOption.optionCode}"/>-option_${product.id}" value="<c:out value="${productOption.index}"/>">
          <select name="<c:out value="${productOption.optionCode}"/>-option_values_${product.id}_<c:out value="${productOption.index}" />" class="optionSelect" onchange="updateRegularPrice();">
            <c:forEach items="${productOption.values}" var="prodOptValue" varStatus="povStatus">
              <option value="<c:out value="${prodOptValue.index}"/>"><c:out value="${prodOptValue.name}"/><c:if test="${prodOptValue.optionPrice != null}"><c:choose><c:when test="${prodOptValue.optionPrice > 0}"> + </c:when><c:otherwise> - </c:otherwise></c:choose><fmt:message key="${siteConfig['CURRENCY'].value}" /><fmt:formatNumber value="${prodOptValue.absoluteOptionPrice}" pattern="#,##0.00" /></c:if></option>
              <div style="display: none;">
               <c:choose>
		  	     <c:when test="${optionPriceList == null or optionPriceList == ''}">
		  	       <c:choose>
		  	         <c:when test="${prodOptValue.optionPrice == null or prodOptValue.optionPrice == ''}">
		  	           <c:set var="optionPriceList" value="0.00"/>
		  	         </c:when>
		  	         <c:otherwise>
		  	           <c:set var="optionPriceList" value="${prodOptValue.optionPrice}"/>
		  	         </c:otherwise>
		  	       </c:choose>
		  	     </c:when>
		  	     <c:otherwise>
		  	       <c:choose>
		  	         <c:when test="${prodOptValue.optionPrice == null or prodOptValue.optionPrice == ''}">
		  	           <c:set var="optionPriceList" value="${optionPriceList},0.00"/>
		  	         </c:when>
		  	         <c:otherwise>
		  	           <c:set var="optionPriceList" value="${optionPriceList},${prodOptValue.optionPrice}"/>
		  	         </c:otherwise>
		  	       </c:choose>
		  	     </c:otherwise>
		  	   </c:choose>
	           </div>
            </c:forEach>
          </select>
          <input type="hidden" name="list_<c:out value="${productOption.optionCode}"/>-option_values_${product.id}_<c:out value="${productOption.index}" />" value="${optionPriceList}" class="optionNameValues" />
        </div>
      </div>
    </c:if>
  </c:if>
  <c:if test="${siteConfig['CUSTOM_TEXT_PRODUCT'].value == 'true'}">
    <c:if test="${empty productOption.values}">
      <div class="dynamic_option">
        <div class="dynamic_optionName"><c:out value="${productOption.name}"/>: </div>
        <div class="dynamic_optionValue">
          <input type="hidden" name="<c:out value="${productOption.optionCode}"/>-option_${product.id}" value="<c:out value="${productOption.index}"/>">
          <input name="<c:out value="${productOption.optionCode}"/>-option_values_cus_${product.id}_<c:out value="${productOption.index}" />" maxlength="110"></input>
        </div>
      </div>
    </c:if>
  </c:if>
</c:forEach>

</div>
</c:if>