<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>

<table class="subscription">
<c:choose>
<c:when test="${param.subscriptionDiscount != ''}">
<tr>
  <td><div class="subscribe"><fmt:message key="subscribe"/>: </div></td>
  <td style="color:#990000;font-weight:bold;">Save <fmt:formatNumber value="${param.subscriptionDiscount}" pattern="#,##0.00" />%</td>
</tr>
<tr>
  <td>&nbsp;</td>
  <td><input type="radio" name="subscriptionInterval_${param.productId}" value="m1"> <fmt:message key="delivery"/> <fmt:message key="every_m"><fmt:param value="1"/></fmt:message><br/></td>
</tr>
</c:when>
<c:otherwise>
<tr>
  <td><div class="subscribe"><fmt:message key="subscribe"/>: </div></td>
  <td><input type="radio" name="subscriptionInterval_${param.productId}" value="m1"> <fmt:message key="delivery"/> <fmt:message key="every_m"><fmt:param value="1"/></fmt:message><br/></td>
</tr>
</c:otherwise>
</c:choose>
<tr>
  <td>&nbsp;</td>
  <td><input type="radio" name="subscriptionInterval_${param.productId}" value="m2"> <fmt:message key="delivery"/> <fmt:message key="every_ms"><fmt:param value="2"/></fmt:message></td>
</tr>
</table>