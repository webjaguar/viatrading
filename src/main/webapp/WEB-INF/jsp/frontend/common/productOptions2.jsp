<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>

<c:set var="hasOptionImage" value="${product.hasOptionImage}"/>

<c:if test="${hasOptionImage}">
<script type="text/javascript">
<!--
function toggleImage(optionCode,productId,optionIndex,ImageIndex) {
 if (document.getElementById(optionCode+'-image_'+productId+'_'+optionIndex+'_'+ImageIndex).value == '') {
   document.getElementById(optionCode+'-productOption_'+optionIndex).style.display = 'none';
 } else {
   if (document.getElementById(optionCode+'-productOption_'+optionIndex).style.display == 'none') document.getElementById(optionCode+'-productOption_'+optionIndex).style.display = 'block';
   document.getElementById(optionCode+'-productOption_'+optionIndex).src = '${_contextpath}/assets/Image/Product/options/' + document.getElementById(optionCode+'-image_'+productId+'_'+optionIndex+'_'+ImageIndex).value;
 }
}
//-->
</script>
</c:if>

<table border="0" cellspacing="0" cellpadding="0" class="optionTable">
<c:forEach items="${product.productOptions}" var="productOption">
<input type="hidden" name="optionCode_${product.id}" value="<c:out value="${productOption.optionCode}"/>">	
<c:if test="${productOption.type != 'box'}" >

<div style="display:none;">
<c:forEach items="${productOption.values}" var="prodOptValue" varStatus="povStatus">
  <input type="hidden" id="<c:out value="${productOption.optionCode}"/>-image_${product.id}_${productOption.index}_${prodOptValue.index}" value="${prodOptValue.imageUrl}" />
</c:forEach>
</div>
<c:forEach items="${productOption.values}" var="prodOptValue" varStatus="povStatus">

<c:if test="${fn:toLowerCase(productOption.name) != 'size'}" >

<c:if test="${povStatus.first}">
<c:set var="imageUrl" value="${prodOptValue.imageUrl}" />
<tr>
<td valign="top">
 <table width="100%" cellspacing="0" cellpadding="0" border="0">
  <tr>
   <td class="optionName2"><c:out value="${productOption.name}"/>:</td>
  </tr>
  <tr>
   <td valign="top" <c:if test="${not productOption.hasImage}">colspan="2"</c:if>>
   <input type="hidden" name="<c:out value="${productOption.optionCode}"/>-option_${product.id}" value="<c:out value="${productOption.index}"/>">
    <select name="<c:out value="${productOption.optionCode}"/>-option_values_${product.id}_${productOption.index}" class="optionSelect" <c:if test="${productOption.hasImage}">onchange="toggleImage('<c:out value="${productOption.optionCode}"/>',${product.id},${productOption.index},this.value)"</c:if> >
   </c:if>
  	  <option value="${prodOptValue.index}"><c:out value="${prodOptValue.name}"/><c:if test="${prodOptValue.optionPrice != null}"><c:choose><c:when test="${prodOptValue.optionPrice > 0}"> + </c:when><c:otherwise> - </c:otherwise></c:choose><fmt:message key="${siteConfig['CURRENCY'].value}" /><fmt:formatNumber value="${prodOptValue.absoluteOptionPrice}" pattern="#,##0.00" /></c:if></option>
   <c:if test="${povStatus.last}">  	  
    </select>
   </td>
  </tr>
 </table>
</td> 
<c:if test="${productOption.hasImage}">
<td><img class="optionImage" id="<c:out value="${productOption.optionCode}"/>-productOption_${productOption.index}" <c:if test="${imageUrl != null}">src="${_contextpath}/assets/Image/Product/options/${imageUrl}"</c:if>></td>
</c:if> 
</tr>
<tr>
</tr>
</c:if>

</c:if>

</c:forEach>

<c:if test="${siteConfig['CUSTOM_TEXT_PRODUCT'].value == 'true'}">
  <c:if test="${empty productOption.values}">
   <tr>
    <td class="optionName2" colspan="2"><c:out value="${productOption.name}"/>: </td>
   </tr>
   <tr>
    <td colspan="2">
    <input type="hidden" name="<c:out value="${productOption.optionCode}"/>-option_${product.id}" value="${productOption.index}">
    <input name="<c:out value="${productOption.optionCode}"/>-option_values_cus_${product.id}_${productOption.index}" maxlength="110"></input>
    </td>
  </c:if>
</c:if>

</c:if>
</c:forEach>
</table>