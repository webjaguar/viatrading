<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<c:if test="${product.priceSize > 1}" >
<table class="priceBreak" border="0">
  <tr valign="middle">
   <td style="padding-left:15px;"><img width="65" src="${_contextpath}/assets/Image/Layout/buyInBulkSave.gif" /></td>
   <td>
   <c:choose>
  	<c:when test="${!product.loginRequire or userSession != null}" >
  	 <table width="100%">
	  <c:forEach items="${product.price}" var="price" varStatus="statusPrice">
	  <c:if test="${price.amt > 0 || siteConfig['SHOW_ZERO_PRICE'].value == 'true'}">
	  <tr valign="middle">
	    <td>
	    <c:if test="${!empty price.qtyFrom}">
	    	Buy <b>${price.qtyFrom}</b> or more:
	    </c:if>
	    <b><fmt:message key="${siteConfig['CURRENCY'].value}" /><fmt:formatNumber value="${price.amt }" pattern="#,##0.00" /></b> ea. (Save <fmt:formatNumber value="${(1 - price.amt / product.msrp) * 100}" pattern="#,##0.0"/>%)
	    </td>
	  </tr>
	  </c:if>
	  </c:forEach>
	  </table>  
	</c:when>
	<c:otherwise>
	  <a href="${_contextpath}/login.jhtm?noMessage=t&forwardAction=<c:out value='${model.url}'/>" ><img border="0" name="_logintoviewprice" class="_logintoviewprice" src="${_contextpath}/assets/Image/Layout/login_to_view_price${_lang}.gif" /></a>
	</c:otherwise>
   </c:choose>
   </td>
  </tr>
</table>
</c:if>
