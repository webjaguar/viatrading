<%@ page session="false" %>
<%@ page import="com.webjaguar.model.*, java.text.NumberFormat, java.util.*" %>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>



<div class="clearfix" id="cart-dropdown-header">
	<strong>Cart Items</strong>
</div>
<div class="clearfix" id="cart-dropdown-content">
	<div class="dropdownCart_table">
		<div class="dropdownCartHdr">
			<div class="row">
				<div class="col-xs-2 col-sm-2">
					<div class="cartImageHeader">Image</div>
				</div>
				<div class="col-xs-4 col-sm-4">
					<div class="cartNameHeader">Name/SKU</div>
				</div>
				<div class="col-xs-2 col-sm-2">
					<div class="cartQtyHeader">Quantity</div>
				</div>
				<div class="col-xs-3 col-sm-3">
					<div class="cartPriceHeader">Price</div>
				</div>
			</div>
		</div>

		<div class="dropdownCartDetails">

			<c:forEach items="${model.cart.cartItems}" var="cartItem" varStatus="cartStatus">
			
				<div <c:choose><c:when test="${cartStatus.index % 2 == 0}">class="odd_row clearfix"</c:when><c:otherwise>class="even_row clearfix"</c:otherwise></c:choose>>
			
				<div class="col-xs-2 col-sm-2">	
					<div class="cartImageWrapper">
                      <a href="#">
                          <img src="${_contextpath}/assets/Image/Product/thumb/${cartItem.product.thumbnail.imageUrl}" class="cartImage img-responsive">
                      </a>
                    </div>	
					<div class="clearfix"></div>
				</div>
				
				<div class="col-xs-4 col-sm-4">
					<div class="cartName">
						<div class="cartItemNameSkuWrapper">
							<a class="cart_item_name" href="#">${cartItem.product.name}</a>
							<br>
							<a class="cart_item_sku" href="#">
								<span class="sku_label">SKU:</span>
								${cartItem.product.sku}
							</a>
						</div>
					</div>
					<div class="clearfix"></div>
				</div>
				
				<div class="col-xs-2 col-sm-2">
				<div class="input-group bootstrap-touchspin">
				<span class="input-group-addon bootstrap-touchspin-prefix" style="display: none;">
				</span>
				<input type="text" class="touchspin form-control" value="${cartItem.quantity}" style="display: block;"/>
				<span class="input-group-addon bootstrap-touchspin-postfix" style="display: none;">
				</span>
				<span class="input-group-btn-vertical">
				<button type="button" class="btn btn-default bootstrap-touchspin-up">
				<i class="glyphicon glyphicon-plus"></i>
				</button>
				<button type="button" class="btn btn-default bootstrap-touchspin-down">
				<i class="glyphicon glyphicon-minus"></i>
				</button>
				</span>
				</div>
				</div>

				<div class="col-xs-3 col-sm-3">
					<div class="cartPrice"><fmt:message key="${siteConfig['CURRENCY'].value}" /><fmt:formatNumber value="${cartItem.total}" pattern="###,##0.00"/></div>
					<div class="clearfix"></div>
				</div>
			</div>
			
			</c:forEach>
			
		</div>

		<div class="dropdownCartFtr">
			<div class="row">
				<div class="col-xs-offset-5 col-xs-3 col-sm-offset-5 col-sm-3">
					<div class="total_label">TOTAL</div>
				</div>
				<div class="col-xs-3 col-sm-3">
					<div class="total_value"><fmt:message key="${siteConfig['CURRENCY'].value}" /><fmt:formatNumber value="${model.cart.subTotal}" pattern="###,##0.00"/></div>
				</div>
			</div>
		</div>
		
	
	</div>
</div>
<div class="clearfix" id="cart-dropdown-footer">
	<div class="buttons_wrapper clearfix">
		<div class="btn_wrapper viewCart_btn_wrapper">
			<a href="/viewCart.jhtm" class="btn btn-lg viewCart_btn">
				View Cart
			</a>
		</div>
	</div>
</div>
													