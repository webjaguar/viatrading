<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>

<div id="priceContainer">
<div id="priceContainerBdy" class="bdy">
	<div id="priceGridContainer">
	<c:choose>
  	<c:when test="${!product.loginRequire or userSession != null and !product.altLoginRequire}" >
  	
	<div class="hiddenTable" id="standardPricing">
		<table cellspacing="0" cellpadding="0" border="0" class="priceGrid" style="width: 100%;">
		<tbody>
		<c:if test="${fn:length(product.price) gt 1}">
			<tr class="qtyRow">
			  <td class="bold left qty">Quantity</td><td class="space"></td><c:set var="pcals" value="2"/>
			  <c:set var="multiPrice" value="false"/>
			<c:forEach items="${product.price}" var="price" varStatus="statusPrice"><c:set var="pcals" value="${pcals+1}"/>		
			  <td class="bold center">
				  <c:choose><c:when test="${price.qtyFrom == null}"><c:out value='${product.minimumQty}' /></c:when><c:otherwise><c:out value="${price.qtyFrom}" /></c:otherwise></c:choose><c:choose><c:when test="${price.qtyTo == null}">+</c:when><c:otherwise>-<c:out value="${price.qtyTo}" /></c:otherwise></c:choose>
			  </td>
			  <c:if test="${!statusPrice.last}">
			    <td class="space"></td><c:set var="pcals" value="${pcals+1}"/>
			  </c:if>
			  <c:if test="${statusPrice.count > 1}">
			    <c:set var="multiPrice" value="true"/>
			  </c:if>			  
			</c:forEach>	
			</tr>
		</c:if>
		<tr>
			<td colspan="${pcals}" class="spacer"></td>
		</tr>
		<c:if test="${product.salesTag != null and product.salesTag.discount != 0.0}" >
		<c:set var="multiplier" value="1"/>
        <c:if test="${siteConfig['CASE_CONTENT_LAYOUT'].value == '1' and product.caseContent > 0}">
		  <c:set var="multiplier" value="${product.caseContent}"/>	
        </c:if>
		<tr class="priceRow">
			<td class="bold left"><fmt:message key="f_retail" /></td><td class="space"></td>
		<c:forEach items="${product.price}" var="price" varStatus="statusPrice">			
			<td class="bold center" style="text-decoration: line-through"><fmt:message key="${siteConfig['CURRENCY'].value}" /><fmt:formatNumber value="${price.amt*multiplier}" pattern="#,##0.00#" maxFractionDigits="${siteConfig['MAX_FRACTION_DIGIT'].value}" /></td>			
		  <c:if test="${!statusPrice.last}">
		    <td class="space"></td>
		  </c:if>
        </c:forEach>
		</tr>
		<tr>
			<td colspan="${pcals}" class="spacer"></td>
		</tr>
		</c:if>
		<tr class="salesRow" <c:if test="${multiPrice and product.endQtyPricing and !(product.salesTag != null and product.salesTag.discount != 0.0)}">style="text-decoration: line-through;"</c:if>>
			<td class="bold left"><fmt:message key="f_quickView_youPay" /></td><td class="space"></td>
			<c:forEach items="${product.price}" var="price" varStatus="statusPrice"><c:set var="pcals" value="${pcals+1}"/>
			<c:set var="multiplier" value="1"/>
	      	<c:if test="${siteConfig['CASE_CONTENT_LAYOUT'].value == '1' and product.caseContent > 0}">
	  			<c:set var="multiplier" value="${product.caseContent}"/>	
	      	</c:if>	
			  <c:choose>
			    <c:when test="${product.salesTag != null and product.salesTag.discount != 0.0}"><td class="bold center"><fmt:message key="${siteConfig['CURRENCY'].value}" /><fmt:formatNumber value="${price.discountAmt*multiplier}" pattern="#,##0.00" /></td></c:when>
			    <c:otherwise>
			      <td class="bold center">
			        <fmt:message key="${siteConfig['CURRENCY'].value}" /><fmt:formatNumber value="${price.amt*multiplier}" pattern="#,##0.00" />
			        <c:set var="lastPrice" value="${price.amt*multiplier}"/>
			      </td>
			    </c:otherwise>
			  </c:choose>
		      <c:if test="${!statusPrice.last}">
		  		<td class="space"></td>
		  	  </c:if>
		    </c:forEach>
		</tr>
		<c:if test="${multiPrice and product.endQtyPricing and !(product.salesTag != null and product.salesTag.discount != 0.0)}">
		<tr class="salesRow">
			<td class="bold left"><fmt:message key="f_quickView_youPay" /></td><td class="space"></td>
			<c:forEach items="${product.price}" var="price" varStatus="statusPrice"><c:set var="pcals" value="${pcals+1}"/>
			
			    <td class="bold center">
			      <fmt:message key="${siteConfig['CURRENCY'].value}" /><fmt:formatNumber value="${lastPrice}" pattern="#,##0.00" />
			    </td>
		      <c:if test="${!statusPrice.last}">
		  		<td class="space"></td>
		  	  </c:if>
		    </c:forEach>
		</tr>
		</c:if>
		<tr>
			<td colspan="${pcals}" class="spacer"></td>
		</tr>
		</tbody>
		</table>
	</div>

	<form action="${_contextpath}/addToCart.jhtm" name="this_form" id="this_form" method="post">
	<div id="optionContainer">
	  <%@ include file="/WEB-INF/jsp/frontend/common/quickView/productOptions.jsp" %>
	</div>
	<div id="quantityContainer">
		<div class="hiddenTable" id="standardPricing">
		<input type="hidden" id="product.id" name="product.id" value="${product.id}">
		<table class="qtyBoxWrapper" cellpadding="0" cellspacing="0">
	 		<tr valign="middle">                                                                                                   
		     <c:set var="zeroPrice" value="false"/>
			 <c:forEach items="${product.price}" var="price">
			   <c:if test="${price.amt == 0}">
		     	<c:set var="zeroPrice" value="true"/>
			   </c:if>
		 	 </c:forEach> 
		     <c:choose>
			   <c:when test="${gSiteConfig['gINVENTORY'] and product.inventory != null and !product.negInventory and product.inventory <= 0}" >
			     <td><c:out value="${siteConfig['OUT_OF_STOCK_TITLE'].value}" escapeXml="false"/></td> 
			   </c:when>
			   <c:when test="${zeroPrice and siteConfig['ZERO_PRICE_HTML'].value != ''}">
			     <td><c:out value="${siteConfig['ZERO_PRICE_HTML'].value}" escapeXml="false"/></td>
			   </c:when>
			   <c:otherwise>
			       <td class="quantityBlock" style="white-space: nowrap">
			         <div class="quantity"><fmt:message key="f_quantity" />: <input type="text" size="6" maxlength="5" name="quantity_${product.id}" id="quantity_${product.id}" value=""><c:if test="${product.packing != ''}"> <c:out value="${product.packing}"/> </c:if></div>
			       </td>
			       <c:choose>
			         <c:when test="${!empty product.numCustomLines}">
					   <td><input type="image" id="continueButton" border="0" src="${_contextpath}/assets/Image/Layout/button_continue.gif" onClick="return continueButton(${product.id})" ></td>
					 </c:when>
					 <c:otherwise>
			           <td><input type="image" border="0" name="_addtocart" class="_addtocart" src="${_contextpath}/assets/Image/Layout/button_addtocart${_lang}.gif" onClick="return addToCart()" /></td>
			         </c:otherwise>
			       </c:choose>
			        <c:if test="${gSiteConfig['gINVENTORY'] and product.inventory != null and product.inventory < product.lowInventory}">
			         <td><c:out value="${siteConfig['LOW_STOCK_TITLE'].value}" escapeXml="false"/></td> 
			        </c:if>      
			   </c:otherwise>
			 </c:choose>                                                                                         
		 </tr>
		</table>
		</div>
	</div>
	</form>
	</c:when>
	<c:otherwise>
		  <c:choose>
		    <c:when test="${product.altLoginRequire}">
		      <img border="0" name="_altLogintoviewprice" class="_altLogintoviewprice" src="${_contextpath}/assets/Image/Layout/altLogin_to_view_price${_lang}.gif" />
		    </c:when>
		    <c:otherwise>
		      <a href="${_contextpath}/login.jhtm?noMessage=t&forwardAction=<c:out value='${model.url}'/>" onClick="return login(this.href)" ><img border="0" name="_logintoviewprice" class="_logintoviewprice" src="${_contextpath}/assets/Image/Layout/login_to_view_price${_lang}.gif" /></a>
		    </c:otherwise>
		   </c:choose>
   </c:otherwise>
  </c:choose>
  </div>
</div>
</div>