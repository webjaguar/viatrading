<%@ page import="java.util.*" %>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="/WEB-INF/tlds/spring.tld" prefix="spring" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ taglib uri="/WEB-INF/tlds/custom.tld" prefix="wj" %>

<link href="${_contextpath}/assets/SmoothGallery/QuickView/quickview.css" rel="stylesheet" type="text/css">
<script src="${_contextpath}/javascript/mootools-1.2.5-core.js" type="text/javascript"></script>
<c:if test="${model.product != null}">
<c:set value="${model.product}" var="product"/>
<c:set var="detailProductLink">${_contextpath}/product.jhtm?id=${product.id}<c:if test="${model.product == null or siteConfig['RECOMMENDED_LIST_SHOW_BREADCRUMB'].value == 'true'}">&cid=<c:out value="${param.cid}"/></c:if></c:set>
<c:if test="${gSiteConfig['gMOD_REWRITE'] == '1' and not fn:contains(product.sku, '/')}">
  <c:set var="detailProductLink">${_contextpath}/${siteConfig['MOD_REWRITE_PRODUCT'].value}/${product.encodedSku}/${product.encodedName}.html<c:if test="${model.product == null or siteConfig['RECOMMENDED_LIST_SHOW_BREADCRUMB'].value == 'true'}">?cid=<c:out value="${param.cid}"/></c:if></c:set>
</c:if>
<script language="JavaScript" type="text/JavaScript">
window.addEvent('domready', function(){
	var productDetailViewDiv = $('productDetailView'); 
	var productLink = $('productLinkId').get('value'); 
	
	productDetailViewDiv.addEvent('click',function() {
		window.top.quickViewBox.close();
		window.parent.location.href = productLink;
	});
});
function addToCart() {
        if (checkForm()) {
                document.this_form.submit();
                window.top.callCartQV.delay(2000, 3500);
                window.top.quickViewBox.close();
                return true;
        }
        return false;
}
function addToList() {
    document.addToList_form.submit();
    $('addToList').setStyle('display','block').fade('out');
    window.top.quickViewBox.close();
}
function login(href) {
	window.top.quickViewBox.close();
	window.parent.location.href = href;	
}
function checkNumber( aNumber )
{
	var goodChars = "0123456789";
	var i = 0;
	if ( aNumber == "" )
		return 0; //empty
	
	for ( i = 0 ; i <= aNumber.length - 1 ; i++ )
	{
		if ( goodChars.indexOf( aNumber.charAt(i) ) == -1 ) 
			return -1; //invalid number			
	}
	return 1; //valid number
}
function checkQty( pid, boxExtraAmt ){
	var product = document.getElementById('quantity_' + pid);
	if ( checkNumber( product.value ) == -1 || product.value == 0 ) {
	    alert("invalid Quantity");
		product.focus();
		return false;
	}
	return true;
}
function checkForm()
{
	var productList = document.getElementsByName('product.id');
	var allQtyEmpty = true;
	for(var i=0; i<productList.length; i++) 
	{
	    var el = document.getElementById("quantity_" + productList[i].value);
		if ( el != null )
		{
			if ( checkNumber( el.value ) == 1  )
			{
				allQtyEmpty = false;
			}
			else if ( checkNumber( el.value ) == -1 ) 
			{
				alert("invalid Quantity");
				el.focus();
				return false;
			}			
		}	
	}
	if ( allQtyEmpty )
	{
		alert("Please Enter Quantity.");
		return false;
	}
	else
		return true;	
}
</script>

<div id="quickView-details-wrapper">
<div id="quickView-image">
    <!-- image start -->
	<div class="details_image_box">
	 
	<c:forEach items="${model.product.images}" var="image" varStatus="status">
	<c:if test="${status.first}">
		 <img src="<c:if test="${!image.absolute}">${_contextpath}/assets/Image/Product/detailsbig/</c:if><c:out value="${image.imageUrl}"/>" border="0" id="_image" name="_image" class="details_image"
		 	style="<c:if test="${siteConfig['DETAILS_IMAGE_WIDTH'].value != '' }">width: <c:out value="${siteConfig['DETAILS_IMAGE_WIDTH'].value}"/>px</c:if>" alt="<c:out value="${product.name}"/><c:if test="${product.shortDesc != ''}"> - <c:out value="${product.shortDesc}"/></c:if>">
	<c:if test="${siteConfig['DETAILS_IMAGE_WIDTH'].value != '' }" >
	</c:if>
	</c:if>
	
	<c:if test="${status.last and (status.count != 1)}">
	<br>
	<c:forEach items="${model.product.images}" var="thumb">
	<a href="#" class="details_thumbnail_anchor" onClick="_image.src='<c:if test="${!thumb.absolute}">${_contextpath}/assets/Image/Product/detailsbig/</c:if><c:out value="${thumb.imageUrl}"/>';return false;">
		 <img src="<c:if test="${!thumb.absolute}">${_contextpath}/assets/Image/Product/detailsbig/</c:if><c:out value="${thumb.imageUrl}"/>" class="details_thumbnail" alt="<c:out value="${product.name}"/><c:if test="${product.shortDesc != ''}"> - <c:out value="${product.shortDesc}"/></c:if>">
	</a>
	</c:forEach>
	</c:if>
	</c:forEach>
	</div>
	<!-- image end -->
</div>
<div class="spacer-v"></div>
<div id="quickView-info">
<div class="details_item_name"><h1><c:out value="${product.name}" escapeXml="false" /></h1></div>
<c:if test="${siteConfig['SHOW_SKU'].value == 'true' and product.sku != null}">
<div class="details_sku"><c:out value="${product.sku}" /></div>
</c:if>
 
<c:if test="${not empty model.recommendedList and fn:startsWith(model.product.recommendedListDisplay, 'field')}">
<c:set var="quickView" value="true"/>
<%@ include file="/WEB-INF/jsp/frontend/common/recommendedListDropDown.jsp" %>
<c:set var="hideRecommendedList" value="true"/>
</c:if>
 
<c:if test="${gSiteConfig['gPRODUCT_REVIEW'] and product.enableRate}">
<%@ include file="/WEB-INF/jsp/frontend/common/quickView/reviewQuickView.jsp" %>
</c:if>
<c:if test="${product.price != null}">
<%@ include file="/WEB-INF/jsp/frontend/common/quickView/price5.jsp" %>
</c:if>

<c:if test="${gSiteConfig['gMYLIST'] && product.addToList && userSession != null}">
<div class="details_hdr"><fmt:message key="f_addToList" /></div>
<form action="${_contextpath}/addToList.jhtm" name="addToList_form" id="addToList_form" method="post">
<input type="hidden" name="product.id" value="${product.id}">
<div id="addToList">
<input type="image" border="0" src="${_contextpath}/assets/Image/Layout/button_addtolist${_lang}.gif" onClick="return addToList()" />
</div>
</form>
</c:if>

<div id="descWrapper">
<div class="details_item_short_desc"><c:out value="${product.shortDesc}" escapeXml="false" /></div>
<div class="details_item_long_desc"><c:out value="${product.longDesc}" escapeXml="false" /></div>
</div>

<div id="fieldWrapper">
<c:forEach items="${product.productFields}" var="productField" varStatus="row">
<c:if test="${row.first}">
<c:if test="${siteConfig['PRODUCT_FIELDS_TITLE'].value != ''}">
<div class="details_fields_title"><c:out value="${siteConfig['PRODUCT_FIELDS_TITLE'].value}"/></div>
</c:if>
<table class="details_fields">
</c:if>
<tr>
<td class="details_field_name_row${row.index % 2}"><c:out value="${productField.name}" escapeXml="false" /> </td>
<c:choose>
<c:when test="${productField.formatType != null and productField.formatType != '' and productField.fieldType == 'number' }">
<td class="details_field_value_row${row.index % 2}">
<c:catch var="catchFormatterException">
	<fmt:formatNumber value="${wj:trimAndSplit(productField.value, '|' , false, ',', true, true)}" pattern="${productField.formatType}" />
</c:catch>
<c:if test = "${catchFormatterException != null}"><c:out value="${wj:trimAndSplit(productField.value, '|' , false, ',', true, true)}" escapeXml="false"/></c:if>				
</td>
</c:when>
<c:otherwise>
<td class="details_field_value_row${row.index % 2}"><c:out value="${wj:trimAndSplit(productField.value, '|' , false, ',', true, true)}" escapeXml="false"/></td>
</c:otherwise>
</c:choose></tr>
<c:if test="${row.last}">
</table>
</c:if>
</c:forEach>
</div>

</div>
</div>

<div id="productDetailView" >
<fmt:message key="f_productDetailView" />
<input id="productLinkId" name="productLink" type="hidden" value="${detailProductLink}" />
</div>

</c:if>