<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>

<c:if test="${empty product.numCustomLines}">
<table cellspacing="0" cellpadding="0" border="0" class="productOption">
<c:forEach items="${product.productOptions}" var="productOption">
<input type="hidden" name="optionCode_${product.id}" value="<c:out value="${productOption.optionCode}"/>">	
<c:if test="${productOption.type != 'box'}" >
<c:forEach items="${productOption.values}" var="prodOptValue" varStatus="povStatus">
<c:if test="${povStatus.first}">
<tr>
<td class="optionName"><c:out value="${productOption.name}"/>: </td>
<td>
  <input type="hidden" name="<c:out value="${productOption.optionCode}"/>-option_${product.id}" value="<c:out value="${productOption.index}"/>">
  <select name="<c:out value="${productOption.optionCode}"/>-option_values_${product.id}_<c:out value="${productOption.index}" />" class="optionSelect">
</c:if>
  	  <option value="<c:out value="${prodOptValue.index}"/>"><c:out value="${prodOptValue.name}"/><c:if test="${prodOptValue.optionPrice != null}"><c:choose><c:when test="${prodOptValue.optionPrice > 0}"> + </c:when><c:otherwise> - </c:otherwise></c:choose><fmt:message key="${siteConfig['CURRENCY'].value}" /><fmt:formatNumber value="${prodOptValue.absoluteOptionPrice}" pattern="#,##0.00" /></c:if></option>
<c:if test="${povStatus.last}">  	  
  </select>
</td>
</tr>
</c:if>
</c:forEach>
<c:if test="${siteConfig['CUSTOM_TEXT_PRODUCT'].value == 'true'}">
  <c:if test="${empty productOption.values}">
   <tr>
    <td class="optionName" align="right"><c:out value="${productOption.name}"/>: </td>
    <td>
    <input type="hidden" name="<c:out value="${productOption.optionCode}"/>-option_${product.id}" value="<c:out value="${productOption.index}"/>">
    <input name="<c:out value="${productOption.optionCode}"/>-option_values_cus_${product.id}_<c:out value="${productOption.index}" />" maxlength="110"></input>
    </td>
  </c:if>
</c:if>
</c:if>
</c:forEach>
</table>
</c:if>