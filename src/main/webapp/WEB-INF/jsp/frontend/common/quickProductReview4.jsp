<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>

<div class="dynamic_quickProductReview">
  <div class="dynamic_ratingTitle"><fmt:message key="rating" />:</div>
  <div class="dynamic_reviewStar"><jsp:include page="/WEB-INF/jsp/frontend/common/productRateImage.jsp"><jsp:param name="productRateAverage" value="${product.rate.average}" /></jsp:include></div>
  <div class="dynamic_readReviewLink">
  	<c:choose>
      <c:when test="${product.rate == null}">
        <c:if test="${siteConfig['PRODUCT_REVIEW_TYPE'].value != 'anonymous'}">
        (<a href="${_contextpath}/addProductReview.jhtm?sku=${model.product.sku}&cid=${model.cid}" ><fmt:message key="firstReview" /></a>)
        </c:if>
        <c:if test="${siteConfig['PRODUCT_REVIEW_TYPE'].value == 'anonymous'}">
        (<a href="${_contextpath}/productReviewAnon.jhtm?sku=${model.product.sku}&cid=${model.cid}" ><fmt:message key="firstReview" /></a>)
        </c:if>
      </c:when>
      <c:otherwise>
        (<c:out value="${product.rate.numReview}" /> <fmt:message key="reviews" />, <a href="${model.url}#reviews"><fmt:message key="f_readAll" /></a>)
      </c:otherwise>
    </c:choose>  
  </div>
</div>
