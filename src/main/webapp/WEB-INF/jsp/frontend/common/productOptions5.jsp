<div id="newOption">
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<c:if test="${not empty product.productOptions}">
<div class="mulitoptionContainer productOption" id="productOptionsId">

<div class="mulitoptionTable">
<c:forEach items="${product.productOptions}" var="productOption">
  <input type="hidden" name="optionCode_${product.id}" value="<c:out value="${productOption.optionCode}"/>">	
  <div>
    <div class="optionName">
      <c:out value="${productOption.name}"/>:
    </div>
    <div class="optionValue">
      <input type="hidden" name="<c:out value="${productOption.optionCode}"/>-option_${product.id}" value="<c:out value="${productOption.index}"/>">
	  <c:if test="${!empty productOption.values}">
	  <select onchange="getOption('${product.id}');" name="<c:out value="${productOption.optionCode}"/>-option_values_${product.id}_<c:out value="${productOption.index}" />" class="optionSelect">
	    <c:forEach items="${productOption.values}" var="prodOptValue" varStatus="povStatus">
	  	  <option value="<c:out value="${prodOptValue.index}"/>"><c:out value="${prodOptValue.name}"/><c:if test="${prodOptValue.optionPrice != null}"><c:choose><c:when test="${prodOptValue.optionPrice > 0}"> + </c:when><c:otherwise> - </c:otherwise></c:choose><fmt:message key="${siteConfig['CURRENCY'].value}" /><fmt:formatNumber value="${prodOptValue.absoluteOptionPrice}" pattern="#,##0.00" /></c:if></option>
	    </c:forEach>
	  </select>
	  </c:if>
	  <c:if test="${siteConfig['CUSTOM_TEXT_PRODUCT'].value == 'true'}">
	    <c:if test="${empty productOption.values}">
	      <input name="<c:out value="${productOption.optionCode}"/>-option_values_cus_${product.id}_<c:out value="${productOption.index}" />" maxlength="110"></input>
	    </c:if>
	  </c:if>
    </div>
    <!-- class to clear float, if any -->
    <div class="clear"></div>
  </div>
</c:forEach>
</div>

<!-- Images Starts -->
<c:if test="${!empty model.imageUrlSet}">
  <div id="images" >
    <c:forEach items="${model.imageUrlSet}" var="imageUrl">
  	  <div style="display: inline;"><img class="optionImage" <c:if test="${imageUrl != null}">src="${_contextpath}/assets/Image/Product/options/${imageUrl}"</c:if>></div>
	</c:forEach>
  </div>
  <div style="clear: both;"></div> 
</c:if> 
<!-- Images Ends -->
</div>
</c:if>
</div>