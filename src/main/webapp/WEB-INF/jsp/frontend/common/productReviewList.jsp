<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>

<c:if test="${model.productReviewList.nrOfElements > 0}">
<form method="post">
<table id="reviews" width="100%" align="center" cellspacing="0" cellpadding="0" border="0">
  <c:if test="${model.productReviewList.pageCount > 1}">
  <tr>
	<td>
<div class="pageNavi">Page 
		<select name="page" onchange="submit()">
		<c:forEach begin="1" end="${model.productReviewList.pageCount}" var="page">
  	        <option value="${page}" <c:if test="${page == (model.productReviewList.page+1)}">selected</c:if>>${page}</option>
		</c:forEach>
		</select>
of <c:out value="${model.productReviewList.pageCount}"/></div>
	</td>
  </tr>
  </c:if>
  <tr>
	<td colspan="2">
<table border="0" cellpadding="1" cellspacing="0" width="100%">
  <tr><td colspan="2" class="productReview">
  	<table class="productReviewTitle" border="0" cellpadding="0" cellspacing="0" width="100%">
	  <tr>
	  	<td class="productReview"><fmt:message key="productReviews" /></td>
	  	<td align="right" class="writeReview">
	  	<c:if test="${siteConfig['PRODUCT_REVIEW_TYPE'].value != 'anonymous'}">
          <a href="${_contextpath}/addProductReview.jhtm?sku=${model.product.sku}&cid=${model.cid}" ><fmt:message key="reviewThisProduct" /></a>
          </c:if>
          <c:if test="${siteConfig['PRODUCT_REVIEW_TYPE'].value == 'anonymous'}">
          <a href="${_contextpath}/productReviewAnon.jhtm?sku=${model.product.sku}&cid=${model.cid}" ><fmt:message key="reviewThisProduct" /></a>
        </c:if>
        </td>
	  </tr>
	</table></td>  	
  </tr>
<c:forEach items="${model.productReviewList.pageList}" var="review" varStatus="status">
  <tr class="row${status.index % 2}"><td>
    <table border="0" cellpadding="0" cellspacing="0" width="100%">
      <tr>
      	<td align="right" class="reviewDate"><fmt:formatDate type="date" pattern="MM/dd/yyyy" value="${review.created}"/></td>
      </tr> 	
      <tr>  
	    <td class="reviewRate"><c:choose>
	     <c:when test="${review.rate == null}"><img src="${_contextpath}/assets/Image/Layout/star_0.gif" alt=<c:out value="${review.rate}"/> title="<c:out value="${review.rate}"/>" border="0" /></c:when>
	     <c:when test="${review.rate > 0 and review.rate <= 0.6}"><img src="${_contextpath}/assets/Image/Layout/star_0h.gif" alt=<c:out value="${review.rate}"/> title="<c:out value="${review.rate}"/>" border="0" /></c:when>
	     <c:when test="${review.rate > 0.6 and review.rate <= 1}"><img src="${_contextpath}/assets/Image/Layout/star_1.gif" alt=<c:out value="${review.rate}"/> title="<c:out value="${review.rate}"/>" border="0" /></c:when>
	     <c:when test="${review.rate > 1 and review.rate <= 1.6}"><img src="${_contextpath}/assets/Image/Layout/star_1h.gif" alt=<c:out value="${review.rate}"/> title="<c:out value="${review.rate}"/>" border="0" /></c:when>
	     <c:when test="${review.rate > 1.6 and review.rate <= 2}"><img src="${_contextpath}/assets/Image/Layout/star_2.gif" alt=<c:out value="${review.rate}"/> title="<c:out value="${review.rate}"/>" border="0" /></c:when>
	     <c:when test="${review.rate > 2 and review.rate <= 2.6}"><img src="${_contextpath}/assets/Image/Layout/star_2h.gif" alt=<c:out value="${review.rate}"/> title="<c:out value="${review.rate}"/>" border="0" /></c:when>
	     <c:when test="${review.rate > 2.6 and review.rate <= 3}"><img src="${_contextpath}/assets/Image/Layout/star_3.gif" alt=<c:out value="${review.rate}"/> title="<c:out value="${review.rate}"/>" border="0" /></c:when>
	     <c:when test="${review.rate > 3 and review.rate <= 3.6}"><img src="${_contextpath}/assets/Image/Layout/star_3h.gif" alt=<c:out value="${review.rate}"/> title="<c:out value="${review.rate}"/>" border="0" /></c:when>
	     <c:when test="${review.rate > 3.6 and review.rate <= 4}"><img src="${_contextpath}/assets/Image/Layout/star_4.gif" alt=<c:out value="${review.rate}"/> title="<c:out value="${review.rate}"/>" border="0" /></c:when>
	     <c:when test="${review.rate > 4 and review.rate <= 4.6}"><img src="${_contextpath}/assets/Image/Layout/star_4h.gif" alt=<c:out value="${review.rate}"/> title="<c:out value="${review.rate}"/>" border="0" /></c:when>
	     <c:when test="${review.rate > 4.6}"><img src="${_contextpath}/assets/Image/Layout/star_5.gif" alt=<c:out value="${review.rate}"/> title="<c:out value="${review.rate}"/>" border="0" /></c:when>
	   </c:choose></td>
	  </tr>
	  <tr>
	    <td class="reviewTitle"><c:out value="${review.title}"/></td>
	  </tr>
	  <tr>  
	    <td class="reviewer">By: <c:out value="${review.userName}"/></td>
	  </tr>
	  <tr>  
	    <td class="reviewText"><span><c:out value="${review.text}" escapeXml="false"/></span></td>  				
	  </tr>
     </table>
  </tr>
</c:forEach>
</table>
	</td>
  </tr>
</table>
</form>
</c:if>
