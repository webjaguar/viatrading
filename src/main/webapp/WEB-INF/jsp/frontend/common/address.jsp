<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<table border="0" cellpadding="0" cellspacing="0">
<tr><td><c:out value="${address.firstName}"/>&nbsp;<c:out value="${address.lastName}"/></td></tr>
<c:if test="${address.company != ''}">
<tr><td><c:out value="${address.company}"/></td></tr>
</c:if>
<tr><td><c:out value="${address.addr1}"/></td></tr>
<c:if test="${address.addr2 != ''}">
<tr><td><c:out value="${address.addr2}"/></td></tr>
</c:if>
<tr><td><c:out value="${address.city}"/>,&nbsp;<c:out value="${address.stateProvince}"/>&nbsp;<c:out value="${address.zip}"/></td></tr>
<tr><td><c:out value="${countries[address.country]}"/></td></tr>
<c:if test="${billing != 'true'}">
<tr><td><c:out value="${address.resToString}"/></td></tr>
<c:if test="${siteConfig['SHIPPING_LTL'].value == 'true'}">
<tr><td><c:out value="${address.liftgateToString}"/></td></tr>
</c:if>
</c:if>
<c:if test="${address.phone != ''}">
<tr><td><fmt:message key="f_phone" />:&nbsp;<c:out value="${address.phone}"/></td></tr>
</c:if>
<c:if test="${address.cellPhone != ''}">
<tr><td><fmt:message key="f_mobilePhone" />:&nbsp;<c:out value="${address.cellPhone}"/></td></tr>
</c:if>
<c:if test="${address.fax != ''}">
<tr><td><fmt:message key="f_fax" />:&nbsp;<c:out value="${address.fax}"/></td></tr>
</c:if>
</table>