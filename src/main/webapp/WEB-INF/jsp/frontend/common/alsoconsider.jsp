<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>

<c:set var="productLink">${_contextpath}/product.jhtm?id=${product.id}</c:set>
<c:if test="${gSiteConfig['gMOD_REWRITE'] == '1' and not fn:contains(product.sku, '/')}">
  <c:set var="productLink">${_contextpath}/${siteConfig['MOD_REWRITE_PRODUCT'].value}/${product.encodedSku}/${product.encodedName}.html</c:set>
</c:if>

<div id="also_consider_id" class="also_consider" style="float:left">
<c:if test="${siteConfig['ALSO_CONSIDER_TITLE'].value != ''}">
  <div class="also_consider_title"><c:out value="${siteConfig['ALSO_CONSIDER_TITLE'].value}"/></div>
</c:if>
  <div align="center" style="padding:5px" class="also_consider_body">
  <a href="${productLink}" 
	  			class="also_consider_item_name"><c:out value="${product.name}" escapeXml="false" /></a>
    <c:if test="${product.thumbnail != null}">
    <br>
    <a href="${productLink}">
	<img border="0" src="<c:if test="${!product.thumbnail.absolute}">${_contextpath}/assets/Image/Product/thumb/</c:if><c:out value="${product.thumbnail.imageUrl}"/>" class="also_consider_image"
			<c:if test="${siteConfig['ALSO_CONSIDER_THUMB_HEIGHT'].value != ''}">height="<c:out value="${siteConfig['ALSO_CONSIDER_THUMB_HEIGHT'].value}"/>"</c:if>>
	</a>
    </c:if>
    <br>
    <%@ include file="/WEB-INF/jsp/frontend/common/priceRange.jsp" %>
    <a href="${productLink}"><img border="0" src="${_contextpath}/assets/Image/Layout/button_alsoconsider.gif"></a>    
  </div>
</div>