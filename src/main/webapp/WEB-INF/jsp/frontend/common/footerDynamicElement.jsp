<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>

<c:set value="${fn:replace(footerHtml, '#sku#', model.product.sku)}" var="footerHtml"/>
<c:set value="${fn:replace(footerHtml, '#weight#', model.product.weight)}" var="footerHtml"/>
<c:set value="${fn:replace(footerHtml, '#id#', model.product.id)}" var="footerHtml"/>
<c:set value="${fn:replace(footerHtml, '#name#', model.product.name)}" var="footerHtml"/>
<c:set value="${fn:replace(footerHtml, '#namewithoutquotes#', model.product.nameWithoutQuotes)}" var="footerHtml"/>
<c:set value="${fn:replace(footerHtml, '#msrp#', model.product.msrp)}" var="footerHtml"/>
<c:set value="${fn:replace(footerHtml, '#price1#', model.product.price1)}" var="footerHtml"/>
<c:set value="${fn:replace(footerHtml, '#email#', userSession.username)}" var="footerHtml"/>
<c:set value="${fn:replace(footerHtml, '#firstname#', userSession.firstName)}" var="footerHtml"/>
<c:set value="${fn:replace(footerHtml, '#lastname#', userSession.lastName)}" var="footerHtml"/>
