<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>

<tiles:insertDefinition name="${_template}" >

    <c:if test="${_leftBar == '3' || _leftBar == '2'}">
    	<tiles:putAttribute name="leftbar" value="/WEB-INF/jsp/frontend/layout/template${_leftBar}/leftbar.jsp" />
    </c:if>
    
  <tiles:putAttribute name="content" type="string">

<c:if test="${gSiteConfig['gSHARED_CATEGORIES'] != ''}">
<c:if test="${param.sid != null}">
<c:catch var="exception">
<c:forEach items="${paramValues.sid}" var="sid" varStatus="status">
<c:if test="${status.last}">
<c:import url="${gSiteConfig['gSHARED_CATEGORIES']}sharedCategory.jsp">
  <c:param name="sid" value="${sid}" />
</c:import>
</c:if>
</c:forEach>
</c:catch>
</c:if>
</c:if>

<c:if test="${(model != null) && (model.thisCategory != null)}">

<%-- bread crumbs --%>
<c:import url="/WEB-INF/jsp/frontend/layout/${_template}/breadcrumbs.jsp" />
<%-- global category header --%>
<c:if test="${model.categoryLayout.headerHtml != ''}">
  <c:out value="${model.categoryLayout.headerHtml}" escapeXml="false"/>
</c:if>
<c:if test="${model.imageBigExist}"><div align="center"><img src="${_contextpath}/assets/Image/Category/cat_${model.thisCategory.id}_big.gif" border="0" alt="<c:out value="${model.thisCategory.name}"/>"/></div></c:if>

<%-- subcategories --%>
<c:if test="${model.thisCategory.subcatLocation == '1' && not empty model.subCategories}">
<c:choose>
 <c:when test="${siteConfig['SUBCAT_DISPLAY'].value == 'row'}"><c:import url="/WEB-INF/jsp/frontend/layout/template1/subcategoriesRow.jsp" /></c:when>
 <c:otherwise><c:import url="/WEB-INF/jsp/frontend/layout/template1/subcategories.jsp" /></c:otherwise>
</c:choose>
</c:if>


<table border="0" cellpadding="0" cellspacing="0" class="categoryHtml" id="categoryHtml">
  <tr>
    <td><c:out value="${model.thisCategory['htmlCode']}" escapeXml="false" /></td>
  </tr>
</table>


<%-- subcategories --%>
<c:if test="${model.thisCategory.subcatLocation != '1' && not empty model.subCategories}">
<c:choose>
 <c:when test="${siteConfig['SUBCAT_DISPLAY'].value == 'row'}"><c:import url="/WEB-INF/jsp/frontend/layout/template1/subcategoriesRow.jsp" /></c:when>
 <c:otherwise><c:import url="/WEB-INF/jsp/frontend/layout/template1/subcategories.jsp" /></c:otherwise>
</c:choose>
</c:if>
<c:if test="${model.productFieldSearchType > 0}" >
<%-- Narrow Results --%>
<c:set var="actionLink">${_contextpath}/category.jhtm</c:set>
<c:if test="${gSiteConfig['gMOD_REWRITE'] == '1'}">
  <c:set var="actionLink">${_contextpath}/${siteConfig['MOD_REWRITE_CATEGORY'].value}/${model.thisCategory.id}/${model.thisCategory.encodedName}.html</c:set>
</c:if>
<form  action="${actionLink}" method="get">
	<c:if test="${gSiteConfig['gMOD_REWRITE']!= '1' }">
		<input type="hidden" name="cid" value="${model.thisCategory.id}" />
	</c:if>
	<%--Narrow Results  with Drop downs --%>
	<c:if test="${model.productFieldSearchType == '1'}">
		<c:import url="/WEB-INF/jsp/frontend/layout/template1/dropDownList.jsp" />
	</c:if>
	<%--Narrow Results with Check boxes --%>
	<c:if test="${model.productFieldSearchType == '2' and model.thisCategory.productFieldSearchPosition == 'top'}">
		<c:import url="/WEB-INF/jsp/frontend/layout/template4/topCheckBoxFilter.jsp" /> 
	</c:if>
	<%--Search Form (number fields MIN-MAX search) --%>
	<c:if test="${model.productFieldSearchType == '3'}">
		<%--custom category --%>
		<%@ include file="/WEB-INF/jsp/frontend/categoryCustom.jsp" %>
	</c:if>
</form>
</c:if>


<%-- products --%>
<c:import url="/WEB-INF/jsp/frontend/layout/template1/products.jsp" />

<%-- accounts  My tradeZone
<c:import url="/WEB-INF/jsp/frontend/layout/template1/accounts.jsp" />
--%>

<c:if test="${not empty model.thisCategory['footerHtmlCode']}">
<table border="0" cellpadding="0" cellspacing="0" class="categoryHtml">
  <tr>
    <td><c:out value="${model.thisCategory['footerHtmlCode']}" escapeXml="false" /></td>
  </tr>
</table>
</c:if>

<%-- global category footer --%>
<c:if test="${model.categoryLayout.footerHtml != ''}">
  <c:out value="${model.categoryLayout.footerHtml}" escapeXml="false"/>
</c:if>
</c:if>

<%-- default content --%>
<c:if test="${(model != null) && (model.thisCategory == null)}">
&nbsp;
</c:if>
    
  </tiles:putAttribute>
</tiles:insertDefinition>
