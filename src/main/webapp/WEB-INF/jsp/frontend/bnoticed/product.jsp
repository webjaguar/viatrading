<%@ page import="java.util.*" %>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="/WEB-INF/tlds/spring.tld" prefix="spring" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ taglib uri="/WEB-INF/tlds/custom.tld" prefix="wj" %>

<tiles:insertDefinition name="${_template}" flush="true">
  <tiles:putAttribute name="leftbar" value="/WEB-INF/jsp/frontend/layout/template${_leftBar}/leftbar.jsp" />

<tiles:putAttribute name="content" type="string">
<c:set value="false" var="multibox"/>

<c:if test="${model.mootools and param.hideMootools == null and hideMootools == null}">
<script src="${_contextpath}/javascript/mootools-1.2.5.1-more.js" type="text/javascript"></script>
</c:if>

<link rel="stylesheet" href="${_contextpath}/assets/asi_product.css" type="text/css" media="screen" />

<c:if test="${fn:trim(model.productLayout.headerHtml) != ''}">
  <c:set value="${model.productLayout.headerHtml}" var="headerHtml"/>
  <%@ include file="/WEB-INF/jsp/frontend/common/headerDynamicElement.jsp" %>
  <div><c:out value="${headerHtml}" escapeXml="false"/></div>
</c:if>

<c:if test="${model.message != null}">
  <div class="message"><fmt:message key="${model.message}" /></div>
</c:if>

<c:if test="${model.product != null}">
<c:set value="${model.product}" var="product"/>
<script language="JavaScript" type="text/JavaScript">
<%--window.addEvent('domready', function(){
	var tabs1 = new SimpleTabs($('tab-block-1'), { selector: 'h4'});
});--%>
function closeBox() {
	parent.asiOptionsBox.close();
	setTimeout(function() { location.href = "${_contextpath}/viewCart.jhtm";},500);
}

window.addEvent('domready',function() {
	  var box2 = new multiBox('mbCategory', {descClassName: 'multiBoxDesc',waitDuration: 5,showControls: false,overlay: new overlay()});
});
</script>

<c:if test="${siteConfig['ASI_POPUP_DISPLAY'].value == 'true' }" >
<script type="text/javascript">
var asiOptionsBox;
window.addEvent('domready', function(){
	asiOptionsBox = new multiBox('showCartmb',  {showNumbers:false, showControls:false, initialWidth:710, overlay: new overlay({opacity:'0.3'})});
});
</script>
</c:if>

<c:if test="${siteConfig['DETAILS_IMAGE_WIDTH'].value != '' }" >
<script language="JavaScript" type="text/JavaScript">
function zoomIn() {
	window.open('${_contextpath}/productImage.jhtm?id=${product.id}&imgUrl=' + document.getElementById("_image").src,'name','width=800,height=600,resizable=yes,scrollbars=1');
}
</script>
</c:if>

<script language="JavaScript" type="text/JavaScript">
<!--
var saveContactBox;
window.addEvent('domready', function(){			 
	saveContactBox = new multiBox('mbAsiPrice', {showControls : false, useOverlay: false, showNumbers: false });
});
function checkNumber( aNumber )
{
	var goodChars = "0123456789";
	var i = 0;
	if ( aNumber == "" )
		return 0; //empty
	
	for ( i = 0 ; i <= aNumber.length - 1 ; i++ ) {
		if ( goodChars.indexOf( aNumber.charAt(i) ) == -1 ) 
			return -1; //invalid number			
	}	
	return 1; //valid number
}
function checkQty( pid, boxExtraAmt ){
	var product = document.getElementById('quantity_' + pid);
	if ( checkNumber( product.value ) == -1 || product.value == 0 ) {
	    alert("invalid Quantity");
		product.focus();
		return false;
	}
	return true;
}
function checkForm()
{
	var productList = document.getElementsByName('product.id');
	var allQtyEmpty = true;
	for(var i=0; i<productList.length; i++) 
	{
	    var el = document.getElementById("quantity_" + productList[i].value);
		if ( el != null )
		{
			if ( checkNumber( el.value ) == 1  )
			{
				allQtyEmpty = false;
			}
			else if ( checkNumber( el.value ) == -1 ) 
			{
				alert("invalid Quantity");
				el.focus();
				return false;
			}			
		}	
	}
	if ( allQtyEmpty )
	{
		alert("Please Enter Quantity.");
		return false;
	}
	else
		return true;	
}
function loadConfigurator(productId, variant, variantIndex){
	var urlAppend = '?id='+productId;	
	var updateDivId = 'configurator';
	if(variant != '') {
		urlAppend = urlAppend +'&variant='+encodeURIComponent(variant);
		updateDivId = updateDivId+variantIndex;
	}
	//alert(urlAppend);
	//alert(updateDivId);
	$$('.configurator').each(function(ele){
		ele.innerHTML = '';
	});
	var requestHTMLData = new Request.HTML ({
		url: "${_contextpath}/showAsiOptions3.jhtm"+urlAppend,
		onComplete: function(response){
			$('configuratorButton'+variantIndex).destroy();
		},
      	update: $(updateDivId)
	}).send();
}
function emailCheck(str){
	 if(/^[a-z0-9._%-]+@[a-z0-9.-]+\.[a-z]{2,4}$/i.test(str) == false) {
		alert('Invalid Email');
		return false;
	 }
	 return true;
}
function assignValue(ele, value) {
$(ele).value = value;
}
function phoneCheck(str){
	if (str.search(/^\d{10}$/)==-1) {
		alert("Please enter a valid 10 digit number");
		return false;
	} 
}


window.addEvent('domready', function(){			
	new multiBox('mbQuote', {showControls : false, useOverlay: false, showNumbers: false });
});


function saveContact(){
	try {
		if( !validateContactInfo() ) {
			return;
		}
	}catch(err){}
	
	var sku = $('sku').value;
	var name = $('name').value;
	var firstName = $('firstName').value;
	var lastName = $('lastName').value;
	var company = $('company').value;
	var address = $('address').value;
	var city = $('city').value;
	var state = $('state').value;
	var zipCode = $('zipCode').value;
	var country = $('country').value;
	var email = $('email').value;
	var phone = $('phone').value;
	var note = $('note').value;
	var request = new Request({
		   url: "${_contextpath}/insert-ajax-contact.jhtm?firstName="+firstName+"&lastName="+lastName+"&company="+company+"&address="+address+"&city="+city+"&state="+state+"&zipCode="+zipCode+"&country="+country+"&email="+email+"&phone="+phone+"&note="+note+"&sku="+sku+"&name="+name+"&leadSource=quote",
	       method: 'post',
	       onFailure: function(response){
	           saveContactBox.close();
	           $('successMessage').set('html', '<div style="color : RED; text-align : LEFT;">We are experincing problem in saving your quote. <br/> Please try again later.</div>');
	       },
	       onSuccess: function(response) {
	            saveContactBox.close();
	            $('mbQuote').style.display = 'none';
	            $('successMessage').set('html', response);
	       }
		}).send();
}
function addToQuoteList(productId){
	var requestHTMLData = new Request.HTML ({
		url: "${_contextpath}/quoteList.jhtm?productId="+productId,
		onSuccess: function(response){
			$('addToQuoteListWrapperId'+productId).set('html', '<h4><fmt:message key="f_addedToQuoteList" /></h4> <a href="${_contextpath}/quoteViewList.jhtm" rel="type:element" id="quoteListId" class="quoteList"> <input type="image" border="0" name="_viewQuoteList" class="_viewQuoteList" src="${_contextpath}/assets/Image/Layout/button_viewQuoteList.jpg" /> </a> ');
		}
	}).send();
}
//-->
</script>

<c:choose>
  <c:when test="${siteConfig['DETAILS_IMAGE_LOCATION'].value == 'left' }">
    <c:set var="details_images_style" value="float:left;padding:3px 15px 15px 3px;" />
    <c:set var="details_desc_style" value="float:left;padding:0 0 0 15px;" />
  </c:when>
</c:choose>

<%-- bread crumbs --%>
<c:import url="/WEB-INF/jsp/frontend/layout/${_template}/breadcrumbs.jsp" />

<div class="detailsBnoticed">
<table>
<tr valign="top">
<td>
<c:set var="productImage">
<c:choose>
 <c:when test="${fn:contains(siteConfig['PRODUCT_IMAGE_LAYOUTS'].value,'mb') and 
 		(param.multibox == null or param.multibox != 'off') and
 		(product.imageLayout == 'mb' or (empty product.imageLayout and siteConfig['PRODUCT_IMAGE_LAYOUT'].value == 'mb'))}">
 <link rel="stylesheet" href="${_contextpath}/assets/SmoothGallery/multibox.css" type="text/css" media="screen" />
 <c:set value="${true}" var="multibox"/>
 <script src="${_contextpath}/javascript/overlay.js" type="text/javascript" ></script>
 <script src="${_contextpath}/javascript/multibox.js" type="text/javascript" ></script>
 <script type="text/javascript">
	window.addEvent('domready', function(){
		var box = new multiBox('mbxwz', {overlay: new overlay()});
	});
 </script>
 	<div class="details_image_box" id="details_image_boxId" style="${details_images_style}
	 <c:if test="${siteConfig['DETAILS_IMAGE_WIDTH'].value != '' }">width: <c:out value="${siteConfig['DETAILS_IMAGE_WIDTH'].value}"/>px;</c:if>">
	 
	<c:forEach items="${model.product.images}" var="image" varStatus="status">
	  <c:if test="${status.first}">
	      <c:choose>
	  	   <c:when test="${product.asiId != null}">
	  	   <a href="<c:out value="${fn:replace(product.thumbnail.imageUrl, 'prodimgs', 'prodbigimgs')}"/>" id="mb0" class="mbxwz" title="<c:out value="${product.name}"/><c:if test="${product.shortDesc != ''}"> - <c:out value="${product.shortDesc}"/></c:if>"><img src="<c:out value="${fn:replace(product.thumbnail.imageUrl, 'prodimgs', 'prodbigimgs')}"/>" border="0" style="<c:if test="${siteConfig['DETAILS_IMAGE_WIDTH'].value != '' }">width: <c:out value="${siteConfig['DETAILS_IMAGE_WIDTH'].value}"/>px</c:if>" alt="<c:out value="${product.name}"/><c:if test="${product.shortDesc != ''}"> - <c:out value="${product.shortDesc}"/></c:if>"/></a>
	  	   </c:when>
	  	   <c:otherwise>
	  	   <a href="<c:if test="${!image.absolute}">${_contextpath}/assets/Image/Product/detailsbig/</c:if><c:out value="${image.imageUrl}"/>" id="mb0" class="mbxwz" title="<c:out value="${product.name}"/>"><img src="<c:if test="${!image.absolute}">${_contextpath}/assets/Image/Product/detailsbig/</c:if><c:out value="${image.imageUrl}"/>" border="0" style="<c:if test="${siteConfig['DETAILS_IMAGE_WIDTH'].value != '' }">width: <c:out value="${siteConfig['DETAILS_IMAGE_WIDTH'].value}"/>px</c:if>" alt="<c:out value="${product.name}"/><c:if test="${product.shortDesc != ''}"> - <c:out value="${product.shortDesc}"/></c:if>"/></a>
	  	   </c:otherwise>
	  	  </c:choose>
	  </c:if>
	</c:forEach>
    <div style="clear: both;" >
    <c:forEach items="${model.product.images}" var="image" varStatus="status">
	  <c:if test="${status.count > 1}">
	      <c:choose>
	  	   <c:when test="${product.asiId != null}">
	  	   <a href="<c:out value="${fn:replace(product.thumbnail.imageUrl, 'prodimgs', 'prodbigimgs')}"/>" id="mb${status.count - 1}" class="mbxwz" title="<c:out value="${product.name}"/><c:if test="${product.shortDesc != ''}"> - <c:out value="${product.shortDesc}"/></c:if>"><img src="<c:out value="${fn:replace(product.thumbnail.imageUrl, 'prodimgs', 'prodbigimgs')}"/>" border="0" class="details_thumbnail"  alt="<c:out value="${product.name}"/><c:if test="${product.shortDesc != ''}"> - <c:out value="${product.shortDesc}"/></c:if>"/></a>
	  	   </c:when>
	  	   <c:otherwise>
	  	   <a href="<c:if test="${!image.absolute}">${_contextpath}/assets/Image/Product/detailsbig/</c:if><c:out value="${image.imageUrl}"/>" id="mb${status.count - 1}" class="mbxwz" title="<c:out value="${product.name}"/><c:if test="${product.shortDesc != ''}"> - <c:out value="${product.shortDesc}"/></c:if>"><img src="<c:if test="${!image.absolute}">${_contextpath}/assets/Image/Product/detailsbig/</c:if><c:out value="${image.imageUrl}"/>" border="0" class="details_thumbnail" alt="<c:out value="${product.name}"/><c:if test="${product.shortDesc != ''}"> - <c:out value="${product.shortDesc}"/></c:if>"/></a>
	  	   </c:otherwise>
	  	  </c:choose>
	  </c:if>
	</c:forEach>
	</div>
	</div>        
 </c:when>
 <c:when test="${fn:contains(siteConfig['PRODUCT_IMAGE_LAYOUTS'].value,'qb') and 
 		(product.imageLayout == 'qb' or (empty product.imageLayout and siteConfig['PRODUCT_IMAGE_LAYOUT'].value == 'qb'))}">
 <link rel="stylesheet" href="${_contextpath}/assets/SmoothGallery/QuickBox/quickbox.css" type="text/css" media="screen" />
 <c:set value="${true}" var="quickbox"/>
 <script src="${_contextpath}/javascript/QuickBox.js" type="text/javascript" ></script>
 <script type="text/javascript">
	window.addEvent('domready', function(){
		new QuickBox();
	});
 </script>
 	<div class="details_image_box" style="${details_images_style}
	 <c:if test="${siteConfig['DETAILS_IMAGE_WIDTH'].value != '' }">width: <c:out value="${siteConfig['DETAILS_IMAGE_WIDTH'].value}"/>px;</c:if>">
	 
	<c:forEach items="${model.product.images}" var="image" varStatus="status">
	  <c:if test="${status.first}">
	    <a href="<c:if test="${!image.absolute}">${_contextpath}/assets/Image/Product/detailsbig/</c:if><c:out value="${image.imageUrl}"/>" id="" class="" title="" rel="lightbox-atomium"><img src="<c:if test="${!image.absolute}">${_contextpath}/assets/Image/Product/detailsbig/</c:if><c:out value="${image.imageUrl}"/>" border="0" style="<c:if test="${siteConfig['DETAILS_IMAGE_WIDTH'].value != '' }">width: <c:out value="${siteConfig['DETAILS_IMAGE_WIDTH'].value}"/>px</c:if>" alt="<c:out value="${product.name}"/><c:if test="${product.shortDesc != ''}"> - <c:out value="${product.shortDesc}"/></c:if>"/></a>
	  </c:if>
	</c:forEach>
    <div style="clear: both;" >
    <c:forEach items="${model.product.images}" var="image" varStatus="status">
	  <c:if test="${status.count > 1}">
	      <a href="<c:if test="${!image.absolute}">${_contextpath}/assets/Image/Product/detailsbig/</c:if><c:out value="${image.imageUrl}"/>" id="sb${status.count - 1}" class="" title="" rel="lightbox-atomium"><img src="<c:if test="${!image.absolute}">${_contextpath}/assets/Image/Product/detailsbig/</c:if><c:out value="${image.imageUrl}"/>" border="0" class="details_thumbnail" alt="<c:out value="${product.name}"/><c:if test="${product.shortDesc != ''}"> - <c:out value="${product.shortDesc}"/></c:if>"/></a>
	  </c:if>
	</c:forEach>
	</div>
	</div>        
 </c:when>
 <c:when test="${fn:contains(siteConfig['PRODUCT_IMAGE_LAYOUTS'].value,'mz') and 
 		(product.imageLayout == 'mz' or (empty product.imageLayout and siteConfig['PRODUCT_IMAGE_LAYOUT'].value == 'mz'))}">
 <link rel="stylesheet" href="${_contextpath}/assets/SmoothGallery/MagicZoomPlus/magiczoomplus.css" type="text/css" media="screen" />
 <script src="${_contextpath}/javascript/magiczoomplus.js" type="text/javascript" ></script>
 	<div class="details_image_box" style="${details_images_style}
	 <c:if test="${siteConfig['DETAILS_IMAGE_WIDTH'].value != '' }">width: <c:out value="${siteConfig['DETAILS_IMAGE_WIDTH'].value}"/>px;</c:if>">
	 
	<c:forEach items="${model.product.images}" var="image" varStatus="status">
	  <c:if test="${status.first}">
	    <a href="<c:out value="${image.imageUrl}"/>" rel="zoom-width:350px;zoom-height:200px;expand-align:screen; expand-position:top=0; expand-size:original; selectors-effect:pounce; zoom-position:#zoomImageId;" class="MagicZoomPlus" id="Zoomer" >
	      <img alt="" src="<c:out value="${fn:replace(product.thumbnail.imageUrl, 'size=large', 'size=normal')}"/>" border="0" class="normal_image" alt="<c:out value="${product.name}"/><c:if test="${product.shortDesc != ''}"> - <c:out value="${product.shortDesc}"/></c:if>" />
	    </a>
	  </c:if>
	</c:forEach>
    <div style="clear: both;" >
    <c:forEach items="${model.product.images}" var="thumbImage" varStatus="status">
	  <c:if test="${status.count > 0}">
	  <c:choose>
  	   <c:when test="${product.asiId != null}">
  	     <a class="thumb_link" rev="<c:out value="${fn:replace(thumbImage.imageUrl, 'size=large', 'size=normal')}"/>" rel="zoom-id:Zoomer;zoom-width:350px;zoom-height:200px;zoom-position:#zoomImageId;" href="<c:out value="${thumbImage.imageUrl}" />" style="outline: 0pt none; display: inline-block;">
			<img alt="" src="<c:out value="${fn:replace(thumbImage.imageUrl, 'size=large', 'size=small')}"/>">
		 </a>
  	   </c:when>
  	   <c:otherwise>
  	   	  <a href="<c:if test="${!image.absolute}">${_contextpath}/assets/Image/Product/detailsbig/</c:if><c:out value="${image.imageUrl}"/>" rel="zoom-id:Zoomer;zoom-width:350px;zoom-height:200px;zoom-position:#zoomImageId;"  rev="<c:if test="${!image.absolute}">${_contextpath}/assets/Image/Product/thumb/</c:if><c:out value="${image.imageUrl}"/>" title="" ><img src="<c:if test="${!image.absolute}">${_contextpath}/assets/Image/Product/thumb/</c:if><c:out value="${image.imageUrl}"/>" border="0" class="zoom_details_thumbnail" alt="<c:out value="${product.name}"/><c:if test="${product.shortDesc != ''}"> - <c:out value="${product.shortDesc}"/></c:if>"/></a>
  	   </c:otherwise>
  	  </c:choose>
	  </c:if>
	</c:forEach>
	</div>
	</div>
 </c:when>
 <c:otherwise>
    <div class="details_image_box" style="cursor: pointer;">
	  <c:forEach items="${model.product.images}" var="image" varStatus="status">
	    <c:if test="${status.first}">
		  <a style="cursor: pointer;" onclick="zoomIn('${model.product.id}');" title="View Larger Images"><img id="_image" src="<c:if test="${!image.absolute}">${_contextpath}/assets/Image/Product/detailsbig/</c:if><c:out value="${image.imageUrl}"/>" border="0" style="<c:if test="${siteConfig['DETAILS_IMAGE_WIDTH'].value != '' }">width: <c:out value="${siteConfig['DETAILS_IMAGE_WIDTH'].value}"/>px</c:if>" alt="<c:out value="${model.assignedProduct.name}"/><c:if test="${model.assignedProduct.shortDesc != ''}"> - <c:out value="${model.assignedProduct.shortDesc}"/></c:if>"/></a>
		</c:if>
	  </c:forEach>
	  <div>
	  <c:forEach items="${model.product.images}" var="image" varStatus="status">
		<c:if test="${status.index > 0}">
		<a href="#" class="details_thumbnail_anchor" onClick="return updateImage('${image.absolute}','${image.imageUrl}');">
		  <img src="<c:if test="${!image.absolute}">${_contextpath}/assets/Image/Product/detailsbig/</c:if><c:out value="${image.imageUrl}"/>" class="details_thumbnail" alt="<c:out value="${model.assignedProduct.name}"/><c:if test="${model.assignedProduct.shortDesc != ''}"> - <c:out value="${model.assignedProduct.shortDesc}"/></c:if>">
		</a>
		</c:if>
	  </c:forEach> 
	  <c:if test="${fn:length(model.product.images) > 1}">
	  <span class="moreImageWrapper">
		<a onclick="zoomIn('${model.product.id}');" title="View Larger Images"><span>View Larger Images</span></a>
	  </span>
	  </c:if>
	  </div>
	</div>
 </c:otherwise>
</c:choose>
</c:set>
<!-- image start -->
   <c:out value="${productImage}" escapeXml="false" />    
<!-- image end -->
</td>
<td>
<div id="detailsWrapperId">
<div id="zoomImageId"></div>

<div class="details_item_name"><h1><c:out value="${product.name}" escapeXml="false" /></h1></div>
<c:if test="${siteConfig['SHOW_SKU'].value == 'true'}">
<c:if test="${product.sku != null}">
<div class="details_sku"><c:out value="${product.sku}" /></div>  
</c:if>
</c:if>

<div class="asiDetailBox" style="float:right;">
 <c:if test="${product.field39 == 'Yes' or  product.field39 == 'true' or product.field39 == '1'}">
 <img src="${_contextpath}/assets/Image/Layout/button_rushIcon.gif" alt="RushImage"/>
 </c:if>
  </div>  
 <c:choose>
<c:when test="${product.tab1Content != ''}">
<div class="details_tab_content"><p></p><b><c:out value="${product.tab1}" escapeXml="false" /></b><br><c:out value="${product.tab1Content}" escapeXml="false" /></div>  
</c:when>
<c:otherwise>
<div class="details_long_desc"><p></p><b>Description:</b><br><c:out value="${product.longDesc}" escapeXml="false" /></div>  
</c:otherwise>
</c:choose>

<br>
<c:if test="${siteConfig['DETAILS_SHORT_DESC'].value == 'true' and product.shortDesc != ''}">
<div class="details_short_desc"><b>Special Information:</b><br><c:out value="${product.shortDesc}" escapeXml="false" /></div>  
</c:if>


<c:if test="${siteConfig['TECHNOLOGO'].value != ''}">	   
<div id="technologoWrapper">
 <a href="http://www.technologo.com/techno.OnDemand?&email=<c:out value="${siteConfig['TECHNOLOGO'].value}" />&sku=${product.id}&name=${product.nameWithoutQuotes}&imagelocation_0=${wj:asiBigImageName(product.thumbnail.imageUrl)}&whiteout=true&orientation=10000&removelogo=true" target="_blank">
 <img src="${_contextpath}/assets/Image/Layout/button_create_virtual.gif"  border="0" alt="create virtual" />
 </a>
</div>
</c:if>

<c:if test="${gSiteConfig['gDEALS'] > 0 and product.deal.htmlCode != null}">
	<c:out value="${product.deal.htmlCode}" escapeXml="false"/>
</c:if>
 <c:if test="${gSiteConfig['gDEALS'] > 0 and product.deal.parentHtmlCode != null}">
	<c:out value="${product.deal.parentHtmlCode}" escapeXml="false"/>
  </c:if>		 

<div class="boxWrapper" align="right">

<c:if test="${ product.salesTag.image and !empty product.price }" >
<div class="saleTagImage">
 <img src="${_contextpath}/assets/Image/PromoSalesTag/salestag_${product.salesTag.tagId}.gif" />
</div>
</c:if>

<%--
<div id="socialNetworkLogoId" class="socialNetworkLogo">
    <input type="image" border="0" src="${_contextpath}/assets/Image/Layout/SocialNetworkLogo.jpg" alt="SocialNetworkLogo">
</div>
 --%>


</div>
</div>
<br/>
</td>

</tr>
<tr>
<td valign="top" colspan="2">
 <div class="asiDetailBox_wrapper">
  <div class="asiDetailBox" id="asiDetailPrice">
    <div class="asiDetailTitle" id="asiDetailPriceTitle">
    <c:choose>
      <c:when test="${model.asiProduct.lowestPrice != null and model.asiProduct.lowestPrice.isUndefined}"><fmt:message key="quote" /></c:when>
      <c:otherwise><fmt:message key="price" /></c:otherwise>
    </c:choose>
    </div>
    <div class="asiDetailValue" id="asiDetailPriceValue">
      <c:choose>
        <c:when test="${model.product.asiIgnorePrice}">
        	<table class="asiPriceGrid" width="100%">
				  <tr class="apgHeader">
				    <td colspan="${fn:length(model.product.price) + 1}" class="apgHeading"><c:out value="Prices" /></td>
				  </tr>
				  <tr>
				    <th class="apgTitle"><fmt:message key="quantity" /></th>
				    <c:forEach items="${model.product.price}" var="price" varStatus="priceStatus">
		              <td class="apgValue"><c:out value="${price.qtyFrom}" /></td>
		            </c:forEach>
				  </tr>
				  <tr>
				    <th class="apgTitle"><fmt:message key="price" /></th>
				    <c:forEach items="${model.product.price}" var="price" varStatus="priceStatus">
		              <td class="apgValue" <c:if test="${product.salesTag != null and product.salesTag.discount != 0.0}" >style="text-decoration: line-through"</c:if> >
		                 <fmt:message key="${siteConfig['CURRENCY'].value}" /><fmt:formatNumber value="${price.amt}" pattern="#,##0.00#" maxFractionDigits="${siteConfig['MAX_FRACTION_DIGIT'].value}" />
				      </td>
		            </c:forEach>
				  </tr>
				  <c:if test="${model.product.salesTag != null and model.product.salesTag.discount != 0.0}">
	  			  <tr>
	  			    <th class="apgTitle"><fmt:message key="f_youPay" /></th>
			    	<c:forEach items="${model.product.price}" var="price" varStatus="priceStatus">
			      	  <td class="apgValue">
	  			    	<c:choose>
	  				  	  <c:when test="${product.salesTag.percent}">
	  				        <fmt:message key="${siteConfig['CURRENCY'].value}" /><fmt:formatNumber value="${price.amt *( (100 - product.salesTag.discount) / 100)}" pattern="#,##0.00" maxFractionDigits="2" />
	  				  	  </c:when>
	  				  	  <c:otherwise>
	  				    	<fmt:message key="${siteConfig['CURRENCY'].value}" /><fmt:formatNumber value="${price.amt - product.salesTag.discount}" pattern="#,##0.00" maxFractionDigits="2" />
	  				  	  </c:otherwise>
	  					</c:choose>
	  			  	  </td>
	  				</c:forEach>
	  			  </tr>
			      </c:if>
			  <tr align="right">
			    <td colspan="${fn:length(model.product.price) + 1}">
			      <c:choose>
			        <c:when test="${siteConfig['ASI_POPUP_DISPLAY'].value == 'true' }">
			          <a href="${_contextpath}/showAsiOptions3.jhtm?id=${product.id}" rel="width:600,height:515" id="showCartmb${status.index}" class="showCartmb" title="<fmt:message key='session' />"><input type="image" border="0" name="_addtocart" class="_addtocart" src="${_contextpath}/assets/Image/Layout/button_addtocart${_lang}.gif" /></a>
			        </c:when>
			        <c:otherwise>
			          <img alt="" src="${_contextpath}/assets/Image/Layout/button_addtocart${_lang}.gif" id="configuratorButton" onclick="loadConfigurators('${product.id}', '', '');" >
			          <div id="configurator" class="configurator"></div>
			        </c:otherwise>
			      </c:choose>  
			    </td>
			    <td>
					<c:if test="${gSiteConfig['gMYLIST']}">
					<form action="${_contextpath}/addToList.jhtm">
					  <input type="hidden" name="product.id" value="${product.id}">
					  <div id="addToListId" class="addToList">
					    <input type="image" border="0" src="${_contextpath}/assets/Image/Layout/button_addtolist${_lang}.gif">
					  </div>
					</form>
					</c:if>
					</td>
					<td>
					<c:if test="${siteConfig['PRODUCT_QUOTE'].value == 'true' and product.quote}">
					<link rel="stylesheet" href="${_contextpath}/assets/SmoothGallery/multiboxASI.css" type="text/css" media="screen" />
					<c:if test="${!multibox}">
					 <script src="${_contextpath}/javascript/overlay.js" type="text/javascript" ></script>
					 <script src="${_contextpath}/javascript/multibox.js" type="text/javascript" ></script>
					</c:if>
					<script type="text/javascript">
					window.addEvent('domready', function(){			
						new multiBox('mbQuote', {showControls : false, useOverlay: false, showNumbers: false });
					});
					</script>
					  <a href="#${product.id}" rel="type:element" id="mbQuote" class="mbQuote"><input type="image" border="0" name="_addtocart" class="_addtocart" src="${_contextpath}/assets/Image/Layout/button_requestInfo${_lang}.gif" /></a>
					  <div id="${product.id}" class="miniWindowWrapper mbPriceQuote">
						<form autocomplete="off" onsubmit="saveContact(); return false;">
						<input type="hidden" value="${product.sku}" id="sku"/>
						<input type="hidden" value="${product.name}" id="name"/>
						
					  	<div class="header">Please provide your contact information</div>
						<fieldset class="top">
							<label class="AStitle"><fmt:message key="firstName"/></label>
							<input name="firstName" id="firstName" type="text" size="15" onkeyup="assignValue('firstName', this.value);"/>
						</fieldset>
						<fieldset class="bottom">
							<label class="AStitle"><fmt:message key="lastName"/></label>
							<input name="lastName" id="lastName" type="text" size="15" onkeyup="assignValue('lastName', this.value);"/>
						</fieldset>
						<fieldset class="bottom">
							<label class="AStitle"><fmt:message key="company"/></label>
							<input name="company" id="company" type="text" size="15" onkeyup="assignValue('company', this.value);"/>
						</fieldset>
						<fieldset class="bottom">
							<label class="AStitle"><fmt:message key="address"/></label>
							<input name="address" id="address" type="text" size="15" onkeyup="assignValue('address', this.value);"/>
						</fieldset>
						<fieldset class="bottom">
							<label class="AStitle"><fmt:message key="city"/></label>
							<input name="city" id="city" type="text" size="15" onkeyup="assignValue('city', this.value);"/>
						</fieldset>
						<fieldset class="bottom">
							<label class="AStitle"><fmt:message key="state"/></label>
							<input name="state" id="state" type="text" size="15" onkeyup="assignValue('state', this.value);"/>
						</fieldset>
						<fieldset class="bottom">
							<label class="AStitle"><fmt:message key="zipCode"/></label>
							<input name="zipCode" id="zipCode" type="text" size="15" onkeyup="assignValue('zipCode', this.value);"/>
						</fieldset>
						<fieldset class="bottom">
							<label class="AStitle"><fmt:message key="country"/></label>
							<input name="country" id="country" type="text" size="15" onkeyup="assignValue('country', this.value);"/>
						</fieldset>
						<fieldset class="bottom">
							<label class="AStitle"><fmt:message key="email"/></label>
							<input name="email" id="email" type="text" size="15" onkeyup="assignValue('email', this.value);"/>
						</fieldset>
						<fieldset class="bottom">
							<label class="AStitle"><fmt:message key="phone"/></label>
							<input name="phone" id="phone" type="text" size="15" onkeyup="assignValue('phone', this.value);"/>
						</fieldset>
						<fieldset class="bottom">
							<label class="AStitle"><fmt:message key="message"/></label>
							<textarea name="note" id="note" onkeyup="assignValue('note', this.value);"></textarea>
						</fieldset>
						<fieldset>
							<div id="button">
								<input type="submit" value="Send"/>
							</div>
						</fieldset>
						</form>
					  </div>
					  <div id="successMessage"></div>
					</c:if>
					</td>						  	
					<td>
					<c:if test="${model.asiProduct.lowestPrice != null and model.asiProduct.lowestPrice.isUndefined}">
					<link rel="stylesheet" href="${_contextpath}/assets/SmoothGallery/multiboxASI.css" type="text/css" media="screen" />
					<c:if test="${!multibox}">
					 <script src="${_contextpath}/javascript/overlay.js" type="text/javascript" ></script>
					 <script src="${_contextpath}/javascript/multibox.js" type="text/javascript" ></script>
					</c:if>
					<script type="text/javascript">
					window.addEvent('domready', function(){			
						new multiBox('mbQuote', {showControls : false, useOverlay: false, showNumbers: false });
					});
					</script>
					  <a href="#${product.id}" rel="type:element" id="mbQuote" class="mbQuote"><input type="image" border="0" name="_addtocart" class="_addtocart" src="${_contextpath}/assets/Image/Layout/button_addtoquote${_lang}.gif" /></a>
					  <div id="${product.id}" class="miniWindowWrapper mbPriceQuote">
						<form autocomplete="off" onsubmit="saveContact(); return false;">
						<input type="hidden" value="${product.sku}" id="sku"/>
						<input type="hidden" value="${product.name}" id="name"/>
						
					  	<div class="header">Please provide your contact information</div>
						<fieldset class="top">
							<label class="AStitle"><fmt:message key="firstName"/></label>
							<input name="firstName" id="firstName" type="text" size="15" onkeyup="assignValue('firstName', this.value);"/>
						</fieldset>
						<fieldset class="bottom">
							<label class="AStitle"><fmt:message key="lastName"/></label>
							<input name="lastName" id="lastName" type="text" size="15" onkeyup="assignValue('lastName', this.value);"/>
						</fieldset>
						<fieldset class="bottom">
							<label class="AStitle"><fmt:message key="company"/></label>
							<input name="company" id="company" type="text" size="15" onkeyup="assignValue('company', this.value);"/>
						</fieldset>
						<fieldset class="bottom">
							<label class="AStitle"><fmt:message key="address"/></label>
							<input name="address" id="address" type="text" size="15" onkeyup="assignValue('address', this.value);"/>
						</fieldset>
						<fieldset class="bottom">
							<label class="AStitle"><fmt:message key="city"/></label>
							<input name="city" id="city" type="text" size="15" onkeyup="assignValue('city', this.value);"/>
						</fieldset>
						<fieldset class="bottom">
							<label class="AStitle"><fmt:message key="state"/></label>
							<input name="state" id="state" type="text" size="15" onkeyup="assignValue('state', this.value);"/>
						</fieldset>
						<fieldset class="bottom">
							<label class="AStitle"><fmt:message key="zipCode"/></label>
							<input name="zipCode" id="zipCode" type="text" size="15" onkeyup="assignValue('zipCode', this.value);"/>
						</fieldset>
						<fieldset class="bottom">
							<label class="AStitle"><fmt:message key="country"/></label>
							<input name="country" id="country" type="text" size="15" onkeyup="assignValue('country', this.value);"/>
						</fieldset>
						<fieldset class="bottom">
							<label class="AStitle"><fmt:message key="email"/></label>
							<input name="email" id="email" type="text" size="15" onkeyup="assignValue('email', this.value);"/>
						</fieldset>
						<fieldset class="bottom">
							<label class="AStitle"><fmt:message key="phone"/></label>
							<input name="phone" id="phone" type="text" size="15" onkeyup="assignValue('phone', this.value);"/>
						</fieldset>
						<fieldset class="bottom">
							<label class="AStitle"><fmt:message key="message"/></label>
							<textarea name="note" id="note" onkeyup="assignValue('note', this.value);"></textarea>
						</fieldset>
						<fieldset>
							<div id="button">
								<input type="submit" value="Send"/>
							</div>
						</fieldset>
						</form>
					  </div>
					  <div id="successMessage"></div>
					</c:if>
					</td>
			  </tr>
			  </table>
        </c:when>
        <c:otherwise>
          <c:choose>
        	<c:when test="${model.asiProduct.variants != null && fn:length(model.asiProduct.variants.variant) > 0}">
	        <!-- Products with variants -->
          		<c:forEach items="${model.asiProduct.variants.variant}" var="variant" varStatus="variantStatus">
                <table class="asiPriceGrid" width="100%" border="0" cellpadding="0" cellspacing="0">
				  <tr class="apgHeader">
				    <td colspan="${fn:length(variant.prices.price) + 1}" class="apgHeading"><c:out value="${variant.description}"  escapeXml="false" /></td>
				  </tr>
				  <tr>
				    <th class="apgTitle"><fmt:message key="quantity" /></th>
				    <c:forEach items="${variant.prices.price}" var="price" varStatus="priceStatus">
		              <td class="apgValue"><c:out value="${variant.prices.price[priceStatus.index].quantity.range.from}" /></td>
		            </c:forEach>
				  </tr>
				  <tr>
				    <th class="apgTitle"><fmt:message key="price" /></th>
				    <c:forEach items="${variant.prices.price}" var="price" varStatus="priceStatus">
		              <td class="apgValue" <c:if test="${(product.salesTag != null and product.salesTag.discount != 0.0) or (siteConfig.get['ASI_PRICE_MARK_UP_CHANGE_BASE_PRICE'].value != 'true' and model.asiProduct2 != null)}" >style="text-decoration: line-through"</c:if> >
		                 <fmt:message key="${siteConfig['CURRENCY'].value}" /><fmt:formatNumber value="${variant.prices.price[priceStatus.index].price}" pattern="#,##0.00#" maxFractionDigits="${siteConfig['MAX_FRACTION_DIGIT'].value}" />
				      </td>
		            </c:forEach>
				  </tr>
				  <c:choose>
				    <c:when test="${product.salesTag != null and product.salesTag.discount != 0.0}">
		  			  <c:forEach items="${model.asiProduct.variants.variant}" var="variant2" varStatus="variant2Status">
		          	    <c:if test="${variant2Status.index == variantStatus.index}">
		          	  	  <tr>
		  			        <th class="apgTitle"><fmt:message key="f_youPay" /></th>
				    	    <c:forEach items="${variant2.prices.price}" var="price" varStatus="priceStatus">
				      	      <td class="apgValue">
		  			    	    <c:choose>
		  				  	      <c:when test="${product.salesTag.percent}">
		  				            <fmt:message key="${siteConfig['CURRENCY'].value}" /><fmt:formatNumber value="${variant2.prices.price[priceStatus.index].price *( (100 - product.salesTag.discount) / 100)}" pattern="#,##0.00" maxFractionDigits="2" />
		  				  	      </c:when>
		  				  	      <c:otherwise>
		  				    	    <fmt:message key="${siteConfig['CURRENCY'].value}" /><fmt:formatNumber value="${variant2.prices.price[priceStatus.index].price - product.salesTag.discount}" pattern="#,##0.00" maxFractionDigits="2" />
		  				  	      </c:otherwise>
		  					    </c:choose>
		  			  	      </td>
		  				    </c:forEach>
		  			      </tr>
				        </c:if>
				      </c:forEach>
				    </c:when>
				    <c:when test="${siteConfig.get['ASI_PRICE_MARK_UP_CHANGE_BASE_PRICE'].value != 'true' and model.asiProduct2 != null}">
		  			  <c:forEach items="${model.asiProduct2.variants.variant}" var="variant2" varStatus="variant2Status">
		          	    <c:if test="${variant2Status.index == variantStatus.index}">
		          	      <tr>
			  		        <th class="apgTitle"><fmt:message key="f_youPay" /></th>
				            <c:forEach items="${variant2.prices.price}" var="price" varStatus="priceStatus">
				              <td class="apgValue">
		  			            <fmt:message key="${siteConfig['CURRENCY'].value}" /><fmt:formatNumber value="${variant2.prices.price[priceStatus.index].price}" pattern="#,##0.00" maxFractionDigits="2" />
		  			          </td>
		  			        </c:forEach>
		  			      </tr>
				        </c:if>
		          	  </c:forEach>
				    </c:when>
				  </c:choose>
				  <tr align="right">
					<td>
					<form action="${_contextpath}/addToList.jhtm">
					  <input type="hidden" name="product.id" value="${product.id}">
					  <div id="addToListId" class="addToList">
					    <input type="image" border="0" src="${_contextpath}/assets/Image/Layout/button_addtolist${_lang}.gif">
					  </div>
					</form>
					</td>
					
					<td>
					<link rel="stylesheet" href="${_contextpath}/assets/SmoothGallery/multiboxASI.css" type="text/css" media="screen" />
					<c:if test="${!multibox}">
					 <script src="${_contextpath}/javascript/overlay.js" type="text/javascript" ></script>
					 <script src="${_contextpath}/javascript/multibox.js" type="text/javascript" ></script>
					</c:if>
					
					  <a href="#${variant.description}" rel="type:element" id="mbQuote${variant.description}" class="mbQuote"><input type="image" border="0" name="_addtocart" class="_addtocart" src="${_contextpath}/assets/Image/Layout/button_requestInfo${_lang}.gif" /></a>
					  <div id="${variant.description}" class="miniWindowWrapper mbPriceQuote">
						<form autocomplete="off" id="${variant.description}" onsubmit="saveContact(); return false;">
						<input type="hidden" value="${product.sku}" id="sku"/>
						<input type="hidden" value="${product.name}" id="name"/>
						
					  	<div class="header">Please provide your contact information</div>
						<fieldset class="top">
							<label class="AStitle"><fmt:message key="firstName"/></label>
							<input name="firstName" id="firstName" type="text" size="15" onkeyup="assignValue('firstName', this.value);"/>
						</fieldset>
						<fieldset class="bottom">
							<label class="AStitle"><fmt:message key="lastName"/></label>
							<input name="lastName" id="lastName" type="text" size="15" onkeyup="assignValue('lastName', this.value);"/>
						</fieldset>
						<fieldset class="bottom">
							<label class="AStitle"><fmt:message key="company"/></label>
							<input name="company" id="company" type="text" size="15" onkeyup="assignValue('company', this.value);"/>
						</fieldset>
						<fieldset class="bottom">
							<label class="AStitle"><fmt:message key="address"/></label>
							<input name="address" id="address" type="text" size="15" onkeyup="assignValue('address', this.value);"/>
						</fieldset>
						<fieldset class="bottom">
							<label class="AStitle"><fmt:message key="city"/></label>
							<input name="city" id="city" type="text" size="15" onkeyup="assignValue('city', this.value);"/>
						</fieldset>
						<fieldset class="bottom">
							<label class="AStitle"><fmt:message key="state"/></label>
							<input name="state" id="state" type="text" size="15" onkeyup="assignValue('state', this.value);"/>
						</fieldset>
						<fieldset class="bottom">
							<label class="AStitle"><fmt:message key="zipCode"/></label>
							<input name="zipCode" id="zipCode" type="text" size="15" onkeyup="assignValue('zipCode', this.value);"/>
						</fieldset>
						<fieldset class="bottom">
							<label class="AStitle"><fmt:message key="country"/></label>
							<input name="country" id="country" type="text" size="15" onkeyup="assignValue('country', this.value);"/>
						</fieldset>
						<fieldset class="bottom">
							<label class="AStitle"><fmt:message key="email"/></label>
							<input name="email" id="email" type="text" size="15" onkeyup="assignValue('email', this.value);"/>
						</fieldset>
						<fieldset class="bottom">
							<label class="AStitle"><fmt:message key="phone"/></label>
							<input name="phone" id="phone" type="text" size="15" onkeyup="assignValue('phone', this.value);"/>
						</fieldset>
						<fieldset class="bottom">
							<label class="AStitle"><fmt:message key="message"/></label>
							<textarea name="note" id="note" onkeyup="assignValue('note', this.value);"></textarea>
						</fieldset>
						<fieldset>
							<div id="button">
								<input type="submit" value="Send"/>
							</div>
						</fieldset>
						</form>
					  </div>
					  <div id="successMessage"></div>
					</td>						  	
					<td>
					<a href="#RQ_${variant.description}" rel="type:element" id="mbQuoteRequest${variant.description}" class="mbQuote"><input type="image" border="0" name="_addtocart" class="_addtocart" src="${_contextpath}/assets/Image/Layout/button_requestQuote${_lang}.gif" /></a>
				  <div id="RQ_${variant.description}" class="miniWindowWrapper mbPriceQuote">
						<form autocomplete="off" id="RQ_${variant.description}" onsubmit="saveContact(); return false;">
						<input type="hidden" value="${product.sku}" id="sku"/>
						<input type="hidden" value="${product.name}" id="name"/>
						
					  	<div class="header">Please provide your contact information</div>
						<fieldset class="top">
							<label class="AStitle"><fmt:message key="firstName"/></label>
							<input name="firstName" id="firstName" type="text" size="15" onkeyup="assignValue('firstName', this.value);"/>
						</fieldset>
						<fieldset class="bottom">
							<label class="AStitle"><fmt:message key="lastName"/></label>
							<input name="lastName" id="lastName" type="text" size="15" onkeyup="assignValue('lastName', this.value);"/>
						</fieldset>
						<fieldset class="bottom">
							<label class="AStitle"><fmt:message key="company"/></label>
							<input name="company" id="company" type="text" size="15" onkeyup="assignValue('company', this.value);"/>
						</fieldset>
						<fieldset class="bottom">
							<label class="AStitle"><fmt:message key="address"/></label>
							<input name="address" id="address" type="text" size="15" onkeyup="assignValue('address', this.value);"/>
						</fieldset>
						<fieldset class="bottom">
							<label class="AStitle"><fmt:message key="city"/></label>
							<input name="city" id="city" type="text" size="15" onkeyup="assignValue('city', this.value);"/>
						</fieldset>
						<fieldset class="bottom">
							<label class="AStitle"><fmt:message key="state"/></label>
							<input name="state" id="state" type="text" size="15" onkeyup="assignValue('state', this.value);"/>
						</fieldset>
						<fieldset class="bottom">
							<label class="AStitle"><fmt:message key="zipCode"/></label>
							<input name="zipCode" id="zipCode" type="text" size="15" onkeyup="assignValue('zipCode', this.value);"/>
						</fieldset>
						<fieldset class="bottom">
							<label class="AStitle"><fmt:message key="country"/></label>
							<input name="country" id="country" type="text" size="15" onkeyup="assignValue('country', this.value);"/>
						</fieldset>
						<fieldset class="bottom">
							<label class="AStitle"><fmt:message key="email"/></label>
							<input name="email" id="email" type="text" size="15" onkeyup="assignValue('email', this.value);"/>
						</fieldset>
						<fieldset class="bottom">
							<label class="AStitle"><fmt:message key="phone"/></label>
							<input name="phone" id="phone" type="text" size="15" onkeyup="assignValue('phone', this.value);"/>
						</fieldset>
						<fieldset class="bottom">
							<label class="AStitle"><fmt:message key="message"/></label>
							<textarea name="note" id="note" onkeyup="assignValue('note', this.value);"></textarea>
						</fieldset>
						<fieldset>
							<div id="button">
								<input type="submit" value="Send"/>
							</div>
						</fieldset>
						</form>
					  </div>
					  <div id="successMessage"></div>
					</td>								    
				    <td>
			        <c:choose>
			          <c:when test="${siteConfig['ASI_POPUP_DISPLAY'].value == 'true' }">
				        <a href="${_contextpath}/showAsiOptions3.jhtm?id=${product.id}&variant=${variant.description}" rel="width:600,height:515" id="showCartmb${status.index}" class="showCartmb" title="<fmt:message key='session' />"><input type="image" border="0" name="_addtocart" class="_addtocart" src="${_contextpath}/assets/Image/Layout/button_addtocart${_lang}.gif" /></a>
			          </c:when>
			          <c:otherwise>
				        <img alt="" src="${_contextpath}/assets/Image/Layout/button_addtocart${_lang}.gif" onclick="loadConfigurator('${product.id}', '${variant.description}', '${variantStatus.index}');" id="configuratorButton${variantStatus.index}">
			            <div id="configurator${variantStatus.index}" class="configurator"></div>
			          </c:otherwise>
			        </c:choose>
				    </td>
				  </tr>
				  </table>
				</c:forEach>
            </c:when>
            <c:otherwise>
            <!-- Products w/o variants -->
		        <table class="asiPriceGrid" width="100%" border="0" cellpadding="0" cellspacing="0">
				  <tr class="apgHeader">
				    <td colspan="${fn:length(model.asiProduct.prices.price) + 1}" class="apgHeading"><c:out value="Prices" /></td>
				  </tr>
				  <tr>
				    <th class="apgTitle"><fmt:message key="quantity" /></th>
				    <c:forEach items="${model.asiProduct.prices.price}" var="price" varStatus="priceStatus">
		              <td class="apgValue"><c:out value="${model.asiProduct.prices.price[priceStatus.index].quantity.range.from}" /></td>
		            </c:forEach>
				  </tr>
				  <tr>
				    <th class="apgTitle"><fmt:message key="price" /></th>
				    <c:forEach items="${model.asiProduct.prices.price}" var="price" varStatus="priceStatus">
		              <td class="apgValue" <c:if test="${(product.salesTag != null and product.salesTag.discount != 0.0) or (siteConfig.get['ASI_PRICE_MARK_UP_CHANGE_BASE_PRICE'].value != 'true' and model.asiProduct2 != null)}" >style="text-decoration: line-through"</c:if> >
		                 <fmt:message key="${siteConfig['CURRENCY'].value}" /><fmt:formatNumber value="${model.asiProduct.prices.price[priceStatus.index].price}" pattern="#,##0.00#" maxFractionDigits="${siteConfig['MAX_FRACTION_DIGIT'].value}" />
				      </td>
		            </c:forEach>
				  </tr>
				  <c:choose>
				    <c:when test="${product.salesTag != null and product.salesTag.discount != 0.0}">
		  			  <tr>
		  			    <th class="apgTitle"><fmt:message key="f_youPay" /></th>
				    	<c:forEach items="${model.asiProduct.prices.price}" var="price" varStatus="priceStatus">
				      	  <td class="apgValue">
		  			    	<c:choose>
		  				  	  <c:when test="${product.salesTag.percent}">
		  				        <fmt:message key="${siteConfig['CURRENCY'].value}" /><fmt:formatNumber value="${model.asiProduct.prices.price[priceStatus.index].price *( (100 - product.salesTag.discount) / 100)}" pattern="#,##0.00" maxFractionDigits="2" />
		  				  	  </c:when>
		  				  	  <c:otherwise>
		  				    	<fmt:message key="${siteConfig['CURRENCY'].value}" /><fmt:formatNumber value="${model.asiProduct.prices.price[priceStatus.index].price - product.salesTag.discount}" pattern="#,##0.00" maxFractionDigits="2" />
		  				  	  </c:otherwise>
		  					</c:choose>
		  			  	  </td>
		  				</c:forEach>
		  			  </tr>
				    </c:when>
				    <c:when test="${siteConfig.get['ASI_PRICE_MARK_UP_CHANGE_BASE_PRICE'].value != 'true' and model.asiProduct2 != null}">
		  			<tr>
			  		  <th class="apgTitle"><fmt:message key="f_youPay" /></th>
				      <c:forEach items="${model.asiProduct2.prices.price}" var="price" varStatus="priceStatus">
				        <td class="apgValue">
		  			      <fmt:message key="${siteConfig['CURRENCY'].value}" /><fmt:formatNumber value="${model.asiProduct2.prices.price[priceStatus.index].price}" pattern="#,##0.00" maxFractionDigits="2" />
		  			    </td>
		  			  </c:forEach>
		  			</tr>
				    </c:when>
				  </c:choose>
				  <tr align="right">
					<td>
					<form action="${_contextpath}/addToList.jhtm">
					  <input type="hidden" name="product.id" value="${product.id}">
					  <div id="addToListId" class="addToList">
					    <input type="image" border="0" src="${_contextpath}/assets/Image/Layout/button_addtolist${_lang}.gif">
					  </div>
					</form>
					</td>
					<td>
					<link rel="stylesheet" href="${_contextpath}/assets/SmoothGallery/multiboxASI.css" type="text/css" media="screen" />
					<c:if test="${!multibox}">
					 <script src="${_contextpath}/javascript/overlay.js" type="text/javascript" ></script>
					 <script src="${_contextpath}/javascript/multibox.js" type="text/javascript" ></script>
					</c:if>
				
					  <a href="#${product.id}" rel="type:element" id="mbQuote" class="mbQuote"><input type="image" border="0" name="_addtocart" class="_addtocart" src="${_contextpath}/assets/Image/Layout/button_addtoquote${_lang}.gif" /></a>
					  <div id="${product.id}" class="miniWindowWrapper mbPriceQuote">
						<form autocomplete="off" onsubmit="saveContact(); return false;">
						<input type="hidden" value="${product.sku}" id="sku"/>
						<input type="hidden" value="${product.name}" id="name"/>
						
					  	<div class="header">Please provide your contact information</div>
						<fieldset class="top">
							<label class="AStitle"><fmt:message key="firstName"/></label>
							<input name="firstName" id="firstName" type="text" size="15" onkeyup="assignValue('firstName', this.value);"/>
						</fieldset>
						<fieldset class="bottom">
							<label class="AStitle"><fmt:message key="lastName"/></label>
							<input name="lastName" id="lastName" type="text" size="15" onkeyup="assignValue('lastName', this.value);"/>
						</fieldset>
						<fieldset class="bottom">
							<label class="AStitle"><fmt:message key="company"/></label>
							<input name="company" id="company" type="text" size="15" onkeyup="assignValue('company', this.value);"/>
						</fieldset>
						<fieldset class="bottom">
							<label class="AStitle"><fmt:message key="address"/></label>
							<input name="address" id="address" type="text" size="15" onkeyup="assignValue('address', this.value);"/>
						</fieldset>
						<fieldset class="bottom">
							<label class="AStitle"><fmt:message key="city"/></label>
							<input name="city" id="city" type="text" size="15" onkeyup="assignValue('city', this.value);"/>
						</fieldset>
						<fieldset class="bottom">
							<label class="AStitle"><fmt:message key="state"/></label>
							<input name="state" id="state" type="text" size="15" onkeyup="assignValue('state', this.value);"/>
						</fieldset>
						<fieldset class="bottom">
							<label class="AStitle"><fmt:message key="zipCode"/></label>
							<input name="zipCode" id="zipCode" type="text" size="15" onkeyup="assignValue('zipCode', this.value);"/>
						</fieldset>
						<fieldset class="bottom">
							<label class="AStitle"><fmt:message key="country"/></label>
							<input name="country" id="country" type="text" size="15" onkeyup="assignValue('country', this.value);"/>
						</fieldset>
						<fieldset class="bottom">
							<label class="AStitle"><fmt:message key="email"/></label>
							<input name="email" id="email" type="text" size="15" onkeyup="assignValue('email', this.value);"/>
						</fieldset>
						<fieldset class="bottom">
							<label class="AStitle"><fmt:message key="phone"/></label>
							<input name="phone" id="phone" type="text" size="15" onkeyup="assignValue('phone', this.value);"/>
						</fieldset>
						<fieldset class="bottom">
							<label class="AStitle"><fmt:message key="message"/></label>
							<textarea name="note" id="note" onkeyup="assignValue('note', this.value);"></textarea>
						</fieldset>
						<fieldset>
							<div id="button">
								<input type="submit" value="Send"/>
							</div>
						</fieldset>
						</form>
					  </div>
					  <div id="successMessage"></div>
					</td>						  	
					<td>
					<link rel="stylesheet" href="${_contextpath}/assets/SmoothGallery/multiboxASI.css" type="text/css" media="screen" />
					<c:if test="${!multibox}">
					 <script src="${_contextpath}/javascript/overlay.js" type="text/javascript" ></script>
					 <script src="${_contextpath}/javascript/multibox.js" type="text/javascript" ></script>
					</c:if>
					<a href="#RQ2_${variant.description}" rel="type:element" id="mbQuoteRQ2${variant.description}" class="mbQuote"><input type="image" border="0" name="_addtocart" class="_addtocart" src="${_contextpath}/assets/Image/Layout/button_addtoquote${_lang}.gif" /></a>
					  <div id="RQ2_${variant.description}" class="miniWindowWrapper mbPriceQuote">
						<form autocomplete="off" id="${variant.description}" onsubmit="saveContact(); return false;">
						<input type="hidden" value="${product.sku}" id="sku"/>
						<input type="hidden" value="${product.name}" id="name"/>
						
					  	<div class="header">Please provide your contact information</div>
						<fieldset class="top">
							<label class="AStitle"><fmt:message key="firstName"/></label>
							<input name="firstName" id="firstName" type="text" size="15" onkeyup="assignValue('firstName', this.value);"/>
						</fieldset>
						<fieldset class="bottom">
							<label class="AStitle"><fmt:message key="lastName"/></label>
							<input name="lastName" id="lastName" type="text" size="15" onkeyup="assignValue('lastName', this.value);"/>
						</fieldset>
						<fieldset class="bottom">
							<label class="AStitle"><fmt:message key="company"/></label>
							<input name="company" id="company" type="text" size="15" onkeyup="assignValue('company', this.value);"/>
						</fieldset>
						<fieldset class="bottom">
							<label class="AStitle"><fmt:message key="address"/></label>
							<input name="address" id="address" type="text" size="15" onkeyup="assignValue('address', this.value);"/>
						</fieldset>
						<fieldset class="bottom">
							<label class="AStitle"><fmt:message key="city"/></label>
							<input name="city" id="city" type="text" size="15" onkeyup="assignValue('city', this.value);"/>
						</fieldset>
						<fieldset class="bottom">
							<label class="AStitle"><fmt:message key="state"/></label>
							<input name="state" id="state" type="text" size="15" onkeyup="assignValue('state', this.value);"/>
						</fieldset>
						<fieldset class="bottom">
							<label class="AStitle"><fmt:message key="zipCode"/></label>
							<input name="zipCode" id="zipCode" type="text" size="15" onkeyup="assignValue('zipCode', this.value);"/>
						</fieldset>
						<fieldset class="bottom">
							<label class="AStitle"><fmt:message key="country"/></label>
							<input name="country" id="country" type="text" size="15" onkeyup="assignValue('country', this.value);"/>
						</fieldset>
						<fieldset class="bottom">
							<label class="AStitle"><fmt:message key="email"/></label>
							<input name="email" id="email" type="text" size="15" onkeyup="assignValue('email', this.value);"/>
						</fieldset>
						<fieldset class="bottom">
							<label class="AStitle"><fmt:message key="phone"/></label>
							<input name="phone" id="phone" type="text" size="15" onkeyup="assignValue('phone', this.value);"/>
						</fieldset>
						<fieldset class="bottom">
							<label class="AStitle"><fmt:message key="message"/></label>
							<textarea name="note" id="note" onkeyup="assignValue('note', this.value);"></textarea>
						</fieldset>
						<fieldset>
							<div id="button">
								<input type="submit" value="Send"/>
							</div>
						</fieldset>
						</form>
					  </div>
					  <div id="successMessage"></div>
					</td>								    
				    <td colspan="${fn:length(variant.prices.price) + 1}">
			        <c:choose>
			          <c:when test="${siteConfig['ASI_POPUP_DISPLAY'].value == 'true' }">
				        <a href="${_contextpath}/showAsiOptions3.jhtm?id=${product.id}&variant=${variant.description}" rel="width:600,height:515" id="showCartmb${status.index}" class="showCartmb" title="<fmt:message key='session' />"><input type="image" border="0" name="_addtocart" class="_addtocart" src="${_contextpath}/assets/Image/Layout/button_addtocart${_lang}.gif" /></a>
			          </c:when>
			          <c:otherwise>
				        <img alt="" src="${_contextpath}/assets/Image/Layout/button_addtocart${_lang}.gif" onclick="loadConfigurator('${product.id}', '${variant.description}', '${variantStatus.index}');" id="configuratorButton${variantStatus.index}">
			            <div id="configurator${variantStatus.index}" class="configurator"></div>
			          </c:otherwise>
			        </c:choose>
				    </td>
				 </tr>
				  
		        </table>
		    </c:otherwise> 
		  </c:choose>
		</c:otherwise>   
	</c:choose>    
    </div>
  </div>
</div>
</td>
</tr>

</table>
</div>

<div class="optionLink" style="float:right">
   <a href="${_contextpath}/assets/Image/Layout/price_table${_lang}.gif" rel="width:1000,height:750" id="mb0" class="mbCategory"> <img src="${_contextpath}/assets/Image/Layout/button_help${_lang}.gif"/>About Price Tables</a>
</div>

<div class="boxWrapper2">
<c:choose>
<c:when test="${gSiteConfig['gPRODUCT_REVIEW'] and product.enableRate}">
<tr><td><%@ include file="/WEB-INF/jsp/frontend/common/quickProductReview.jsp" %></td></tr>
</c:when>
<c:when test="${siteConfig['PRODUCT_RATE'].value == 'true' and product.enableRate}">
<tr><td><%@ include file="/WEB-INF/jsp/frontend/common/productRate.jsp" %></td></tr>
</c:when>
</c:choose>
</div>

<!-- details start -->
<div class="asi_details_desc" id="details_desc">

<div class="details_product_fields">
   <c:if test="${product.productFields !=null}">
	<h2><c:out value="${siteConfig['PRODUCT_FIELDS_TITLE'].value}"/><p></h2>
	<c:forEach items="${product.productFields}" var="productField" varStatus="row">
	<tr>
	<td><b><c:out value="${productField.name}" escapeXml="false" /></b>:</td>
	<td><c:out value="${productField.value}" escapeXml="false" /></td><br>
	</tr>
	</c:forEach>
	</c:if>
</div>

<div class="asiDetailBox_main">
  <c:if test="${model.productColors != null}">
  <div class="asiDetailBox">
    <div class="asiDetailTitle" onclick="slideOut('asiDetailColor',0,0);">Colors <span class="toggleDiv"></span> </div>
    <div class="asiDetailValue" id="asiDetailColor">
      <c:forEach items="${model.productColors}" var="color" varStatus="status">
        <c:out value="${color}" escapeXml="false"/><c:if test="${(status.index+1) < fn:length(model.productColors)}">,&nbsp;</c:if>  
      </c:forEach>
      <br/> 
    </div>
  </div>
  </c:if>
  
  <c:if test="${model.imprintOptions != null}">
  <div class="asiDetailBox" style="clear:both;">
    <div class="asiDetailTitle" onclick="slideOut('asiDetailImpOpt',0,0);">Imprint Options <span class="toggleDiv"></span></div>
    <div class="asiDetailValue" id="asiDetailImpOpt">
      <c:forEach items="${model.imprintOptions}" var="imprint" varStatus="status">
        <c:out value="${imprint}" escapeXml="false"/><c:if test="${(status.index+1) < fn:length(model.imprintOptions)}">,&nbsp;</c:if>
      </c:forEach>
      <br/> 
    </div>
  </div>
  </c:if>
  
  <c:if test="${model.variantList != null}">
  <div class="asiDetailBox" style="clear:both;">
    <div class="asiDetailTitle" onclick="slideOut('asiDetailSize',0,0);">${model.variant} <span class="toggleDiv"></span></div>
    <div class="asiDetailValue" id="asiDetailSize">
      <c:forEach items="${model.variantList}" var="size" varStatus="status">
        <c:out value="${size}" escapeXml="false"/><c:if test="${(status.index+1) < fn:length(model.variantList)}">,&nbsp;</c:if>
      </c:forEach>
      <br/> 
    </div>
  </div>
  </c:if>
  
  <c:if test="${model.priceIncludes != null}">
    <div class="asiDetailBox" style="clear:both;">
    <div class="asiDetailTitle" onclick="slideOut('asiDetailPriceInclude',0,0);">Price Includes <span class="toggleDiv"></span></div>
    <div class="asiDetailValue" id="asiDetailPriceInclude"> <c:out value="${model.priceIncludes}" escapeXml="false"/> </div>
  	</div>
  </c:if>
  
  <c:if test="${model.additionalOptions != null}">
    <div class="asiDetailBox" style="clear:both;">
      <div class="asiDetailTitle" onclick="slideOut('asiDetailAddlOpt',0,0);">Additional Options <span class="toggleDiv"></span></div>
      <div class="asiDetailValue"  id="asiDetailAddlOpt">
        <c:forEach items="${model.additionalOptions}" var="addlOption">
          <span class="asiDetailVTitle"><c:out value="${addlOption.key}"/>:</span>
          <br/> 
          <div class="asiDetailVValue">
            <c:forEach items="${addlOption.value}" var="value">
              <c:out value="${value.key}" escapeXml="false"/> : <c:out value="${value.value}" escapeXml="false"/>
              <br/>  
            </c:forEach>
          </div>
          <br/>
        </c:forEach>
      </div>
    </div>
  </c:if>
  
  <c:if test="${model.productionTime != null}">
    <div class="asiDetailBox" style="clear:both;">
    <div class="asiDetailTitle" onclick="slideOut('asiDetailProductionTime',0,0);">Production Time <span class="toggleDiv"></span></div>
    <div class="asiDetailValue" id="asiDetailProductionTime"> <c:out value="${model.productionTime}" escapeXml="false"/> </div>
  	</div>
  </c:if>
  
  <c:if test="${model.packaging != null}">
    <div class="asiDetailBox" style="clear:both;">
    <div class="asiDetailTitle" onclick="slideOut('asiDetailPackaging',0,0);">Packaging <span class="toggleDiv"></span></div>
    <div class="asiDetailValue" id="asiDetailPackaging"> <c:out value="${model.packaging}" escapeXml="false"/> </div>
  	</div>
  </c:if>
  
  <c:if test="${model.shippingFobPoints != null}">
    <div class="asiDetailBox" style="clear:both;">
    <div class="asiDetailTitle" onclick="slideOut('asiDetailShipping',0,0);">Shipping <span class="toggleDiv"></span></div>
    <div class="asiDetailValue" id="asiDetailShipping"> FOB Points: <br/>
      <c:forEach items="${model.shippingFobPoints}" var="fob">
         <c:out value="${fob}" escapeXml="false"/> &nbsp;
      </c:forEach>
    </div>
  	</div>
  </c:if>
  
  </div>

</div>

<!-- details end -->

<div class="optionLink" style="float:left;">
  <a href="${_contextpath}/assets/Image/Layout/price_table${_lang}.gif" rel="width:1000,height:750" id="mb0" class="mbCategory">Additional Charges that May Apply <img src="${_contextpath}/assets/Image/Layout/button_help${_lang}.gif"/></a>
</div>

<div class="optionLink" style="float:left;clear:both;">
  <a href="${_contextpath}/assets/Image/Layout/price_table${_lang}.gif" rel="width:1000,height:750" id="mb0" class="mbCategory">Lead Time(s)<img src="${_contextpath}/assets/Image/Layout/button_help${_lang}.gif"/></a>
</div>

</c:if>

<c:if test="${fn:trim(model.productLayout.footerHtml) != ''}">
  <c:set value="${model.productLayout.footerHtml}" var="footerHtml"/>
  <%@ include file="/WEB-INF/jsp/frontend/common/footerDynamicElement.jsp" %>
  <div style="float:left;width:100%"><c:out value="${footerHtml}" escapeXml="false"/></div>
</c:if>

</tiles:putAttribute>
</tiles:insertDefinition>