<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<c:forEach items="${results}" var="result" varStatus="status">
Product ID:  ${result['sku']} was <c:choose><c:when test="${result['result'] == 1}">successfully updated</c:when><c:otherwise>unaffected</c:otherwise></c:choose>.</c:forEach>