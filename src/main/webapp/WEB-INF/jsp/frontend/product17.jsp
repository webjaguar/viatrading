<%@ page import="java.util.*" %>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="/WEB-INF/tlds/spring.tld" prefix="spring" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ taglib uri="/WEB-INF/tlds/custom.tld" prefix="wj" %>

<tiles:insertDefinition name="${_template}" flush="true">
  <tiles:putAttribute name="leftbar" value="/WEB-INF/jsp/frontend/layout/template${_leftBar}/leftbar.jsp" />
  <tiles:putAttribute name="content" type="string">

<c:if test="${fn:trim(model.productLayout.headerHtml) != ''}">
  <c:set value="${model.productLayout.headerHtml}" var="headerHtml"/>
  <%@ include file="/WEB-INF/jsp/frontend/common/headerDynamicElement.jsp" %>
  <div><c:out value="${headerHtml}" escapeXml="false"/></div>
</c:if>

<c:if test="${model.message != null}">
  <div class="message"><fmt:message key="${model.message}" /></div>
</c:if>

<c:if test="${model.product != null}">
<c:set value="${model.product}" var="product"/>

<c:if test="${siteConfig['PRODUCT_QUOTE'].value == 'true' and product.quote}">  
<script type="text/javascript">
function addToQuoteList(productId){
	var requestHTMLData = new Request.HTML ({
		url: "${_contextpath}/quoteList.jhtm?productId="+productId,
		onSuccess: function(response){
			$('addToQuoteListWrapperId'+productId).set('html', '<h4><fmt:message key="f_addedToQuoteList" /></h4> <a href="${_contextpath}/quoteViewList.jhtm" rel="type:element" id="quoteListId" class="quoteList"> <input type="image" border="0" name="_viewQuoteList" class="_viewQuoteList" src="${_contextpath}/assets/Image/Layout/button_viewQuoteList.jpg" /> </a> ');
		}
	}).send();
}
//-->
</script>
</c:if>
<script type="text/javascript">
function updateProduct(productId){
	    var requestHTMLData = new Request.HTML ({
			url: "${_contextpath}/ajaxUpdateChildProduct.jhtm?id="+productId,
			update: $('ajaxUpdate')
		}).send();
}
function allQtyBreak(show) {
	$$('div.hiddenTable').each(function(ele){
		if(show){
			ele.setStyle('display','block');
		} else {
			ele.setStyle('display','none');
		}
	});
	if(show){
		$('showMoreLink').set('html','<a onclick="allQtyBreak(false);">Less..</a> ');
	} else {
		$('showMoreLink').set('html','<a onclick="allQtyBreak(true);">More..</a> ');
		$('standardPricing0').setStyle('display','block');
	}
}

//-->
</script>

<%-- bread crumbs --%>
<c:import url="/WEB-INF/jsp/frontend/layout/${_template}/breadcrumbs.jsp" />

<div class="details17">
<div class="productDetailTitle"> <fmt:message key="f_productDetails"/> </div>
<div id="ajaxUpdate">
<div id="productDetailWrapper">
  <div id="imageWrapperId">
    <c:set var="productImage">
	  <c:choose>
	    <c:when test="${fn:contains(siteConfig['PRODUCT_IMAGE_LAYOUTS'].value,'mz') and 
	                (model.masterProduct.imageLayout == 'mz' or (empty model.masterProduct.imageLayout and siteConfig['PRODUCT_IMAGE_LAYOUT'].value == 'mz'))}">
	      <link rel="stylesheet" href="${_contextpath}/assets/SmoothGallery/MagicZoomPlus/magiczoomplus.css" type="text/css" media="screen" />
	      <script src="${_contextpath}/javascript/magiczoomplus.js" type="text/javascript" ></script>
          <div class="details_image_box" style="${details_images_style}
            <c:if test="${siteConfig['DETAILS_IMAGE_WIDTH'].value != '' }">width: <c:out value="${siteConfig['DETAILS_IMAGE_WIDTH'].value}"/>px;</c:if>">
            <c:forEach items="${model.masterProduct.images}" var="image" varStatus="status">
              <c:if test="${status.first}">
	            <a href="<c:if test="${!image.absolute}">${_contextpath}/assets/Image/Product/superBig/</c:if><c:out value="${fn:replace(image.imageUrl, 'detailsbig', 'superBig')}"/>" rel="zoom-width:480px;zoom-height:360px;expand-align:screen; expand-position:top=0; expand-size:original; selectors-effect:pounce; zoom-position:#zoomImageId;" class="MagicZoomPlus" id="Zoomer" >
	              <img alt="" src="<c:if test="${!image.absolute}">${_contextpath}/assets/Image/Product/detailsbig/</c:if><c:out value="${fn:replace(product.thumbnail.imageUrl, 'size=large', 'size=normal')}"/>" border="0" class="normal_image" alt="<c:out value="${product.name}"/><c:if test="${product.shortDesc != ''}"> - <c:out value="${product.shortDesc}"/></c:if>" />
	            </a>
              </c:if>
            </c:forEach>
		    <div style="clear: both;" >
		      <div id="thumbnailWrapper">
		        <c:forEach items="${model.masterProduct.images}" var="thumbImage" varStatus="status">
		          <c:if test="${status.count > 0}">
		            <c:choose>
                      <c:when test="${product.asiId != null or true}">
                        <a class="thumb_link" rev="<c:if test="${!thumbImage.absolute}">${_contextpath}/assets/Image/Product/detailsbig/</c:if><c:out value="${fn:replace(thumbImage.imageUrl, 'size=large', 'size=normal')}"/>" rel="zoom-id:Zoomer;zoom-width:480px;zoom-height:360px;zoom-position:#zoomImageId;" href="<c:if test="${!thumbImage.absolute}">${_contextpath}/assets/Image/Product/superBig/</c:if><c:out value="${fn:replace(thumbImage.imageUrl, 'detailsbig', 'superBig')}"/>" style="outline: 0pt none; display: inline-block;">
                          <img alt="" src="<c:if test="${!thumbImage.absolute}">${_contextpath}/assets/Image/Product/thumb/</c:if><c:out value="${fn:replace(thumbImage.imageUrl, 'size=large', 'size=small')}"/>">
                        </a>
                      </c:when>
                      <c:otherwise>
                        <a href="<c:if test="${!image.absolute}">${_contextpath}/assets/Image/Product/detailsbig/</c:if><c:out value="${image.imageUrl}"/>" rel="zoom-id:Zoomer;zoom-width:480px;zoom-height:360px;zoom-position:#zoomImageId;"  rev="<c:if test="${!image.absolute}">${_contextpath}/assets/Image/Product/thumb/</c:if><c:out value="${image.imageUrl}"/>" title="" >
                        <img src="<c:if test="${!image.absolute}">${_contextpath}/assets/Image/Product/thumb/</c:if><c:out value="${image.imageUrl}"/>" border="0" class="zoom_details_thumbnail" alt="<c:out value="${product.name}"/><c:if test="${product.shortDesc != ''}"> - <c:out value="${product.shortDesc}"/></c:if>"/></a>
                      </c:otherwise>
                    </c:choose>
                  </c:if>
                </c:forEach>
              </div>
            </div>
          </div>
        </c:when>
        <c:when test="${fn:contains(siteConfig['PRODUCT_IMAGE_LAYOUTS'].value,'qb') and 
 		    (model.masterProduct.imageLayout == 'qb' or (empty model.masterProduct.imageLayout and siteConfig['PRODUCT_IMAGE_LAYOUT'].value == 'qb'))}">
          <link rel="stylesheet" href="assets/SmoothGallery/QuickBox/quickbox.css" type="text/css" media="screen" />
          <c:set value="${true}" var="quickbox"/>
		  <script src="javascript/QuickBox.js" type="text/javascript" ></script>
		   <script type="text/javascript">
			  window.addEvent('domready', function(){
				new QuickBox();
			  });
		   </script>
 	       <div class="details_image_box" id="details_image_boxId" style="${details_images_style}
	         <c:if test="${siteConfig['DETAILS_IMAGE_WIDTH'].value != '' }">width: <c:out value="${siteConfig['DETAILS_IMAGE_WIDTH'].value}"/>px;</c:if>">
	         
	         <c:forEach items="${model.masterProduct.images}" var="image" varStatus="status">
			   <c:if test="${status.first}">
			     <a href="<c:if test="${!image.absolute}">${_contextpath}/assets/Image/Product/detailsbig/</c:if><c:out value="${image.imageUrl}"/>" id="" class="" title="" rel="quickbox"><img src="<c:if test="${!image.absolute}">${_contextpath}/assets/Image/Product/detailsbig/</c:if><c:out value="${image.imageUrl}"/>" border="0" style="<c:if test="${siteConfig['DETAILS_IMAGE_WIDTH'].value != '' }">width: <c:out value="${siteConfig['DETAILS_IMAGE_WIDTH'].value}"/>px</c:if>" alt="<c:out value="${product.name}"/><c:if test="${product.shortDesc != ''}"> - <c:out value="${product.shortDesc}"/></c:if>"/></a>
			   </c:if>
	         </c:forEach>
		     <div style="clear: both;" >
		     <c:forEach items="${model.masterProduct.images}" var="image" varStatus="status">
			   <c:if test="${status.count > 1}">
			     <a href="<c:if test="${!image.absolute}">${_contextpath}/assets/Image/Product/detailsbig/</c:if><c:out value="${image.imageUrl}"/>" id="qb${status.count - 1}" class="" title="" rel="quickbox"><img src="<c:if test="${!image.absolute}">${_contextpath}/assets/Image/Product/detailsbig/</c:if><c:out value="${image.imageUrl}"/>" border="0" class="details_thumbnail" alt="<c:out value="${product.name}"/><c:if test="${product.shortDesc != ''}"> - <c:out value="${product.shortDesc}"/></c:if>"/></a>
			   </c:if>
		     </c:forEach>
		     </div>
		   </div>        
         </c:when>
         <c:otherwise>
		   <link rel="stylesheet" href="${_contextpath}/assets/SmoothGallery/multibox.css" type="text/css" media="screen" />
		   <c:set value="${true}" var="multibox"/>
		   <script src="javascript/overlay.js" type="text/javascript" ></script>
		   <script src="javascript/multibox.js" type="text/javascript" ></script>
		   <script type="text/javascript">
			  window.addEvent('domready', function(){
				  var box = new multiBox('mbxwz', {overlay: new overlay()});
			  });
		   </script>
		   <div class="details_image_box" id="details_image_boxId" style="${details_images_style}
			 <c:if test="${siteConfig['DETAILS_IMAGE_WIDTH'].value != '' }">width: <c:out value="${siteConfig['DETAILS_IMAGE_WIDTH'].value}"/>px;</c:if>">
			 
			 <c:forEach items="${model.masterProduct.images}" var="image" varStatus="status">
			   <c:if test="${status.first}">
			     <a href="<c:if test="${!image.absolute}">${_contextpath}/assets/Image/Product/detailsbig/</c:if><c:out value="${image.imageUrl}"/>" id="mb0" class="mbxwz" title=""><img src="<c:if test="${!image.absolute}">${_contextpath}/assets/Image/Product/detailsbig/</c:if><c:out value="${image.imageUrl}"/>" border="0" style="<c:if test="${siteConfig['DETAILS_IMAGE_WIDTH'].value != '' }">width: <c:out value="${siteConfig['DETAILS_IMAGE_WIDTH'].value}"/>px</c:if>" alt="<c:out value="${product.name}"/><c:if test="${product.shortDesc != ''}"> - <c:out value="${product.shortDesc}"/></c:if>"/></a>
			   </c:if>
			 </c:forEach>
		     <div style="clear: both;" >
		       <c:forEach items="${model.masterProduct.images}" var="image" varStatus="status">
			   <c:if test="${status.count > 1}">
			       <a href="<c:if test="${!image.absolute}">${_contextpath}/assets/Image/Product/detailsbig/</c:if><c:out value="${image.imageUrl}"/>" id="mb${status.count - 1}" class="mbxwz" title=""><img src="<c:if test="${!image.absolute}">${_contextpath}/assets/Image/Product/detailsbig/</c:if><c:out value="${image.imageUrl}"/>" border="0" class="details_thumbnail" alt="<c:out value="${product.name}"/><c:if test="${product.shortDesc != ''}"> - <c:out value="${product.shortDesc}"/></c:if>"/></a>
			   </c:if>
			   </c:forEach>
			 </div>
		   </div>        
		 </c:otherwise>
       </c:choose>
     </c:set>
     <!-- image start -->
       
       <c:out value="${productImage}" escapeXml="false" />
     <!-- image end -->
  </div>
  
  <!-- Details Wrapper Start -->
  <div id="detailsWrapperId">
    <div id="zoomImageId"></div>
<c:if test="${siteConfig['SHOW_SKU'].value == 'true'}">
<c:if test="${product.sku != null}">
<div class="details_sku"><c:out value="${product.sku}" /></div>
</c:if>
</c:if>

<div class="details_item_name"><h1><c:out value="${product.name}" escapeXml="false" /></h1></div>

<c:if test="${siteConfig['DETAILS_SHORT_DESC'].value == 'true' and product.shortDesc != ''}">
<div class="details_short_desc"><c:out value="${product.shortDesc}" escapeXml="false" /></div>
</c:if>

<c:if test="${siteConfig['TECHNOLOGO'].value != ''}">
<div id="technologoWrapper">
 <a href="http://www.technologo.com/techno.OnDemand?&email=<c:out value="${siteConfig['TECHNOLOGO'].value}" />&sku=${product.id}&name=${product.nameWithoutQuotes}&imagelocation_0=${wj:asiBigImageName(product.thumbnail.imageUrl)}&whiteout=true&orientation=10000&removelogo=true" target="_blank">
 <img src="${_contextpath}/assets/Image/Layout/button_create_virtual.gif"  border="0" alt="create virtual" />
 </a>
</div>
</c:if>

<c:if test="${gSiteConfig['gDEALS'] > 0 and product.deal.htmlCode != null}">
        <c:out value="${product.deal.htmlCode}" escapeXml="false"/>
</c:if>
<c:if test="${gSiteConfig['gDEALS'] > 0 and product.deal.parentHtmlCode != null}">
	<c:out value="${product.deal.parentHtmlCode}" escapeXml="false"/>
</c:if>

<c:if test="${siteConfig['DETAILS_LONG_DESC_LOCATION'].value != 'above' and product.longDesc != ''}">
<div class="details_long_desc"><c:out value="${product.longDesc}" escapeXml="false" /></div>
</c:if>

<div class="boxWrapper" align="right">
<c:choose>
<c:when test="${gSiteConfig['gPRODUCT_REVIEW'] and product.enableRate}">
<%@ include file="/WEB-INF/jsp/frontend/common/quickProductReview.jsp" %>
</c:when>
<c:when test="${siteConfig['PRODUCT_RATE'].value == 'true' and product.enableRate}">
<%@ include file="/WEB-INF/jsp/frontend/common/productRate.jsp" %>
</c:when>
</c:choose>
<%--
<div id="socialNetworkLogoId" class="socialNetworkLogo">
    <input type="image" border="0" src="${_contextpath}/assets/Image/Layout/SocialNetworkLogo.jpg" alt="SocialNetworkLogo">
</div>
 --%>
</div>
<c:if test="${ (product.salesTag.image and !empty product.price) and (!product.loginRequire or userSession != null) }" >
<div class="salesTagImageWrapper">
        <div class="salesTagImage">
                <img align="right"  src="${_contextpath}/assets/Image/PromoSalesTag/salestag_${product.salesTag.tagId}.gif" />
        </div>
</div>
</c:if>

<%--Start Price Container --%>
<div id="priceContainerBdyId" class="price_container">
  <div id="priceGridContainer">
    <c:forEach items="${model.slavesList}" var="product" varStatus="priceStatus">
    <div class="hiddenTable" id="standardPricing${priceStatus.index}" <c:if test="${priceStatus.index > 0}">style="display: none;" </c:if>>
      <div class="priceGridTitle"> <fmt:message key="f_price"/>: <c:out value="${product.name}"></c:out> 
      <c:if test="${priceStatus.index == 0 and fn:length(model.slavesList) > 1}">
        <div id="showMoreLink">
          <a onclick="allQtyBreak(true);">More..</a> 
        </div>
      </c:if>
      </div>
      
      <table cellspacing="0" cellpadding="0" border="0" class="priceGrid">
        <tr class="qtyRow">
          <td class="bold left qty">Quantity</td>
          <td class="space"></td>
            <c:set var="pcals" value="2"/>
            <c:set var="multiPrice" value="false"/>
            <c:choose>
              <c:when test="${empty model.slavesList and product.priceSize < 2}">
                <td class="bold center"> 1+ </td>
              </c:when>
              <c:otherwise>
                <c:forEach items="${product.price}" var="price" varStatus="statusPrice"><c:set var="pcals" value="${pcals+1}"/>
                  <td class="bold center">
                    <c:choose>
                      <c:when test="${price.qtyFrom == null}"><c:out value='${product.minimumQty}' /></c:when>
                      <c:otherwise><c:out value="${price.qtyFrom}" /></c:otherwise>
                    </c:choose>
                    <c:choose>
                      <c:when test="${price.qtyTo == null}">+</c:when>
                      <c:otherwise>-<c:out value="${price.qtyTo}" /></c:otherwise>
                    </c:choose>
                  </td>
                  <c:if test="${!statusPrice.last}">
                    <td class="space"></td><c:set var="pcals" value="${pcals+1}"/>
                  </c:if>
                  <c:if test="${statusPrice.count > 1}">
                    <c:set var="multiPrice" value="true"/>
                  </c:if>
                </c:forEach>
              </c:otherwise>
            </c:choose>
          </tr>
          <tr>
            <td colspan="${pcals}" class="spacer"></td>
          </tr>
          <c:if test="${product.salesTag != null and product.salesTag.discount != 0.0}" >
          <tr class="priceRow">
            <td class="bold left">Retail</td>
            <td class="space"></td>
            <c:forEach items="${product.price}" var="price" varStatus="statusPrice">
              <td class="bold center" style="text-decoration: line-through"><fmt:message key="${siteConfig['CURRENCY'].value}" /><fmt:formatNumber value="${price.amt}" pattern="#,##0.00" /></td>
              <c:if test="${!statusPrice.last}">
                <td class="space"></td>
              </c:if>
            </c:forEach>
          </tr>
          <tr>
            <td colspan="${pcals}" class="spacer"></td>
          </tr>
          </c:if>
          <tr class="salesRow" <c:if test="${multiPrice and product.endQtyPricing and !(product.salesTag != null and product.salesTag.discount != 0.0)}">style="text-decoration: line-through;"</c:if>>
            <td class="bold left"><fmt:message key="f_youPay" /></td>
            <td class="space"></td>
            <c:forEach items="${product.price}" var="price" varStatus="statusPrice"><c:set var="pcals" value="${pcals+1}"/>
              <c:choose>
                <c:when test="${product.salesTag != null and product.salesTag.discount != 0.0}"><td class="bold center"><fmt:message key="${siteConfig['CURRENCY'].value}" /><fmt:formatNumber value="${price.discountAmt}" pattern="#,##0.00" /></td></c:when>
                <c:otherwise>
                  <td class="bold center">
                    <fmt:formatNumber value="${price.amt}" pattern="#,##0.00" />
                    <c:set var="lastPrice" value="${price.amt}"/>
                  </td>
                </c:otherwise>
              </c:choose>
              <c:if test="${!statusPrice.last}">
                <td class="space"></td>
              </c:if>
            </c:forEach>
          </tr>
          <c:if test="${multiPrice and product.endQtyPricing and !(product.salesTag != null and product.salesTag.discount != 0.0)}">
            <tr class="salesRow">
              <td class="bold left"><fmt:message key="f_youPay" /></td><td class="space"></td>
              <c:forEach items="${product.price}" var="price" varStatus="statusPrice"><c:set var="pcals" value="${pcals+1}"/>
              <td class="bold center">
                <fmt:formatNumber value="${lastPrice}" pattern="#,##0.00" />
              </td>
              <c:if test="${!statusPrice.last}">
                <td class="space"></td>
              </c:if>
              </c:forEach>
            </tr>
          </c:if>
          <tr>
            <td colspan="${pcals}" class="spacer"></td>
          </tr>
        </table>
      </div>
      </c:forEach>
    
    </div>
</div>
<%--End Price Container --%>
<!-- Start Colors Selection -->

<div class="colorContainer" id="colorContainerId">
  <div id="colorTitle">
    <fmt:message key="f_selectColor"/>
  </div>
  <div id="colorBoxWrapper">
    <c:forEach items="${model.colorsMap}" var="color" varStatus="colorStatus">
      <div style="padding: 2px; float: left; <c:if test="${color.value.name == model.selectedColor}">border: 1px solid gray;</c:if>">
        <div id="colorBox${colorStatus.index}" class="colorBox" style="background-color: ${color.value.value}; height: 20px; width: 20px; margin: 3px; border: 1px solid gray;" onclick="updateProduct('${color.key}');"></div>
      </div>
    </c:forEach>
    <div style="clear:both;"></div>
  </div>
</div>

<!-- End Colors Selection -->
<form action="${_contextpath}/addToCart.jhtm" method="post" onsubmit="return checkForm()" id="addToCartForm">
<%--Start Quantity Container --%>

<div id="qtyTitle">
    <fmt:message key="f_provideQuantity"/>
</div>
<div class="qunatityTable">
  <div class="sizeSelection" align="left">
    <c:choose>
      <c:when test="${empty model.slavesList}">
	    <c:set var="showAddToCart" value="true" />
        <ul class="sizeTable">
	      <li>
		    <div class="size">
			  <span id=""> <c:out value="${model.slaveFieldValue}"></c:out> </span>
			</div>
			<div class="qty">
			  <input type="hidden" value="${product.id}" name="product.id"/>
              <input type="text" size="5" maxlength="5" name="quantity_${product.id}" id="quantity_${product.id}" value="<c:out value="${cartItem.quantity}"/>" ><c:if test="${product.packing != ''}"> <c:out value="${product.packing}"/> </c:if>
              <c:if test="${gSiteConfig['gINVENTORY'] and product.inventory != null and product.inventory < product.lowInventory}">
                <c:out value="${siteConfig['LOW_STOCK_TITLE'].value}" escapeXml="false"/>
              </c:if>
            </div>
	      </li>
	    </ul>
      </c:when>
      <c:otherwise>
        <c:forEach items="${model.slavesList}" var="slaveProduct">
	      <ul class="sizeTable">
	        <li>
			  <div class="size">
			    <span id=""> <c:out value="${model.sizeMap[slaveProduct.id]}"></c:out> </span>
			  </div>
			  <div class="qty">
			    <input type="hidden" value="${slaveProduct.id}" name="product.id"/>
        	    <c:if test="${gSiteConfig['gSHOPPING_CART'] and (model.slavesShowQtyColumn ==  null or model.slavesShowQtyColumn)}">
                  <c:set var="zeroPrice" value="false"/>
                  <c:forEach items="${slaveProduct.price}" var="price">
                    <c:if test="${price.amt == 0}">
                      <c:set var="zeroPrice" value="true"/>
                    </c:if>
                  </c:forEach>
                  <c:if test="${ (gSiteConfig['gSHOPPING_CART'] and (!empty slaveProduct.price or slaveProduct.priceByCustomer)) and (!slaveProduct.loginRequire or userSession != null)}">
                    <c:choose>
                      <c:when test="${slaveProduct.type == 'box'}">
                        <a href="${productLink}"><img border="0" src="${_contextpath}/assets/Image/Layout/button_buildthebox.gif" /></a>
                      </c:when>
                      <c:when test="${gSiteConfig['gVARIABLE_PRICE'] and slaveProduct.priceByCustomer}">
                        <c:set var="showAddToCart" value="true" />
                        <input type="checkbox" name="quantity_${slaveProduct.id}" id="quantity_${slaveProduct.id}" value="1" />
                      </c:when>
                      <c:when test="${!empty slaveProduct.numCustomLines}">
                        <a href="${productLink}"><img border="0" src="${_contextpath}/assets/Image/Layout/button_continue.gif" /></a>
                      </c:when>
                      <c:otherwise>
                        <c:choose>
                          <c:when test="${gSiteConfig['gINVENTORY'] and slaveProduct.inventory != null and !slaveProduct.negInventory and slaveProduct.inventory <= 0}" >
                            <c:out value="${siteConfig['OUT_OF_STOCK_TITLE'].value}" escapeXml="false"/>
                          </c:when>
                          <c:when test="${zeroPrice and siteConfig['ZERO_PRICE_HTML'].value != ''}">
                            <c:out value="${siteConfig['ZERO_PRICE_HTML'].value}" escapeXml="false"/>
                          </c:when>
                          <c:otherwise>
                            <c:set var="showAddToCart" value="true" />
                            <input name="quantity_${slaveProduct.id}" value="0" id="quantity_${slaveProduct.id}" type="text">
	                        <c:if test="${gSiteConfig['gINVENTORY'] and slaveProduct.inventory != null and slaveProduct.inventory < slaveProduct.lowInventory}">
                              <c:out value="${siteConfig['LOW_STOCK_TITLE'].value}" escapeXml="false"/>
                            </c:if>
                          </c:otherwise>
                        </c:choose>
                      </c:otherwise>
                    </c:choose>
                  </c:if>
                </c:if>
              </div>
	        </li>
	      </ul>
	    </c:forEach>
      </c:otherwise>
    </c:choose>
    <div style="clear: both;"></div>
  </div>
</div>
<%-- PRODUCT OPTIONS --%>
<c:set value="${model.masterProduct}" var="product"/>
<c:if test="${siteConfig['DETAILS_IMAGE_LOCATION'].value == 'middle'}">
<div class="details_hdr">Selectable Options</div>
<table width="100%">
<tr>
<td valign="top" align="center" width="100%" class="detailsImageBox">
<c:out value="${productImage}" escapeXml="false" />
</td>
<td valign="top">
<%@ include file="/WEB-INF/jsp/frontend/common/productOptions2.jsp" %>
</td>
</tr>
</table>
<%@ include file="/WEB-INF/jsp/frontend/common/productOptionsSize.jsp" %>
</c:if>

<c:if test="${siteConfig['DETAILS_IMAGE_LOCATION'].value != 'middle'}">
<c:choose>
<c:when test="${siteConfig['SMART_DROPDOWN'].value == 'true'}">
<%@ include file="/WEB-INF/jsp/frontend/common/productOptions5.jsp" %>
</c:when>
<c:otherwise>
<%@ include file="/WEB-INF/jsp/frontend/common/productOptions.jsp" %>
<%@ include file="/WEB-INF/jsp/frontend/common/productOptionsDoor.jsp" %>
</c:otherwise>
</c:choose>
</c:if>
<div class="buttonContainer" id="buttonContainerId" align="right">
  <c:if test="${gSiteConfig['gSHOPPING_CART'] and showAddToCart}">
    <div id="quickmode_v1_addtocart" align="right">
      <input type="image" value="Add to Cart" name="_addtocart_quick" class="_addtocart_quick" src="${_contextpath}/assets/Image/Layout/button_addtocart${_lang}.gif" >
    </div>
  </c:if>
</div>
<%--End Quantity Container --%>
</form>



<%--Start Button Container --%>
<div class="buttonContainer_addToList" align="right">
        <c:if test="${gSiteConfig['gMYLIST']}">
            <form action="${_contextpath}/addToList.jhtm">
            	<input type="hidden" name="product.id" value="${product.id}">
                <div id="addToList" align="right">
                <input type="image" class="_addToList" border="0" src="${_contextpath}/assets/Image/Layout/button_addtolist${_lang}.gif">
                </div>
        	</form>
        </c:if>
        <c:if test="${siteConfig['PRODUCT_QUOTE'].value == 'true' and product.quote}">
            <div class="addToQuoteListWrapper" id="addToQuoteListWrapperId${product.id}">
				<input type="image" border="0" name="addtoquoteList" id="addtoquoteList" class="_addtoquoteList" value="${product.id}" src="${_contextpath}/assets/Image/Layout/button_addToQuoteList.jpg" onClick="addToQuoteList('${product.id}');" /> 	
			</div>	
        </c:if>
</div>
<%--End Button Container --%>

</div>
</div>

</div>

<%-- End Form --%>

<%--Start Tabs --%>
<jsp:include page="/WEB-INF/jsp/frontend/common/productTabs.jsp"></jsp:include>
<%--End Tabs --%>

<c:set var="recommendedList_HTML">
<c:if test="${model.recommendedList != null}">
<div style="float:left;width:100%" id="recommandedWrapper">
<c:choose>
 <c:when test="${!empty product.recommendedListTitle }">
  <div class="recommended_list"><c:out value="${product.recommendedListTitle}"/></div>
 </c:when>
 <c:otherwise>
  <c:if test="${siteConfig['RECOMMENDED_LIST_TITLE'].value != ''}">
   <div class="recommended_list"><c:out value="${siteConfig['RECOMMENDED_LIST_TITLE'].value}"/></div>
  </c:if>
 </c:otherwise>
</c:choose>
<c:set var="numCul" value="${siteConfig['NUMBER_PRODUCT_PER_ROW'].value}" />
<c:forEach items="${model.recommendedList}" var="product" varStatus="status">

<c:set var="productLink">${_contextpath}/product.jhtm?id=${product.id}<c:if test="${model.product == null or siteConfig['RECOMMENDED_LIST_SHOW_BREADCRUMB'].value == 'true'}">&cid=<c:out value="${param.cid}"/></c:if></c:set>
<c:if test="${gSiteConfig['gMOD_REWRITE'] == '1' and not fn:contains(product.sku, '/')}">
  <c:set var="productLink">${_contextpath}/${siteConfig['MOD_REWRITE_PRODUCT'].value}/${product.encodedSku}/${product.encodedName}.html<c:if test="${model.product == null or siteConfig['RECOMMENDED_LIST_SHOW_BREADCRUMB'].value == 'true'}">?cid=<c:out value="${param.cid}"/></c:if></c:set>
</c:if>

<c:choose>
 <c:when test="${( model.product.recommendedListDisplay == 'quick') or (empty model.product.recommendedListDisplay and siteConfig['RECOMMENDED_LIST_DISPLAY'].value == 'quick')}">
    <%@ include file="/WEB-INF/jsp/frontend/quickmode/view1.jsp" %>
 </c:when>
 <c:when test="${( model.product.recommendedListDisplay == '2') or (empty model.product.recommendedListDisplay and siteConfig['RECOMMENDED_LIST_DISPLAY'].value == '2')}">
    <%@ include file="/WEB-INF/jsp/frontend/thumbnail/view2.jsp" %>
 </c:when>
 <c:when test="${( model.product.recommendedListDisplay == '3') or (empty model.product.recommendedListDisplay and siteConfig['RECOMMENDED_LIST_DISPLAY'].value == '3')}">
        <%@ include file="/WEB-INF/jsp/frontend/thumbnail/view3.jsp" %>
 </c:when>
 <c:when test="${( model.product.recommendedListDisplay == '4') or (empty model.product.recommendedListDisplay and siteConfig['RECOMMENDED_LIST_DISPLAY'].value == '4')}">
        <%@ include file="/WEB-INF/jsp/frontend/thumbnail/view4.jsp" %>
 </c:when>
 <c:when test="${( model.product.recommendedListDisplay == '5') or (empty model.product.recommendedListDisplay and siteConfig['RECOMMENDED_LIST_DISPLAY'].value == '5')}">
        <%@ include file="/WEB-INF/jsp/frontend/thumbnail/view5.jsp" %>
 </c:when>
 <c:when test="${( model.product.recommendedListDisplay == '6') or (empty model.product.recommendedListDisplay and siteConfig['RECOMMENDED_LIST_DISPLAY'].value == '6')}">
        <%@ include file="/WEB-INF/jsp/frontend/thumbnail/view6.jsp" %>
  </c:when>
 <c:when test="${( model.product.recommendedListDisplay == '7') or (empty model.product.recommendedListDisplay and siteConfig['RECOMMENDED_LIST_DISPLAY'].value == '7')}">
        <%@ include file="/WEB-INF/jsp/frontend/thumbnail/view7.jsp" %>
 </c:when>
 <c:when test="${( model.product.recommendedListDisplay == '8') or (empty model.product.recommendedListDisplay and siteConfig['RECOMMENDED_LIST_DISPLAY'].value == '8')}">
        <%@ include file="/WEB-INF/jsp/frontend/thumbnail/view8.jsp" %>
 </c:when>
 <c:when test="${( model.product.recommendedListDisplay == '9') or (empty model.product.recommendedListDisplay and siteConfig['RECOMMENDED_LIST_DISPLAY'].value == '9')}">
        <%@ include file="/WEB-INF/jsp/frontend/thumbnail/view9.jsp" %>
 </c:when>
 <c:when test="${( model.product.recommendedListDisplay == '10') or (empty model.product.recommendedListDisplay and siteConfig['RECOMMENDED_LIST_DISPLAY'].value == '10')}">
    <%@ include file="/WEB-INF/jsp/frontend/thumbnail/view10.jsp" %>
 </c:when>
 <c:when test="${( model.product.recommendedListDisplay == '11') or (empty model.product.recommendedListDisplay and siteConfig['RECOMMENDED_LIST_DISPLAY'].value == '11')}">
    <%@ include file="/WEB-INF/jsp/frontend/thumbnail/view11.jsp" %>
 </c:when>
 <c:when test="${( model.product.recommendedListDisplay == '12') or (empty model.product.recommendedListDisplay and siteConfig['RECOMMENDED_LIST_DISPLAY'].value == '12')}">
    <%@ include file="/WEB-INF/jsp/frontend/thumbnail/view12.jsp" %>
 </c:when>
 <c:otherwise>
        <%@ include file="/WEB-INF/jsp/frontend/thumbnail/view1.jsp" %>
 </c:otherwise>
</c:choose>
</c:forEach>
</div>
</c:if>
</c:set>

<%--Recommended List Start --%>
<c:out value="${recommendedList_HTML}" escapeXml="false" />
<%--Recommended List End --%>

</div>
<!-- Details Wrapper End -->

</c:if>
</tiles:putAttribute>
</tiles:insertDefinition>