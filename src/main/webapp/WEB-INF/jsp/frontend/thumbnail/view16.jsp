<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
	       
<c:if test="${status.first}" >
<table border="0" class="multipleProductPerRow" cellspacing="10">
</c:if>
  <c:if test="${ status.index % numCul == 0 }">
    <tr>
  </c:if> 
  <td align="center" valign="top" class="thumbnail_compact_cell">
   <table  width="100%" border="0" cellpadding="0" cellspacing="0">
    <c:if test="${product.thumbnail != null}">
    <tr class="imageTr">
     <td align="center">
      <div align="center" class="thumbnail_image_link">
      <a href="${productLink}">
  	  <c:choose>
  	   <c:when test="${product.asiId != null}">
  	   <img src="<c:out value="${fn:replace(product.thumbnail.imageUrl, 'prodimgs', 'prodthumbs')}"/>" border="0" class="thumbnail_image_qv" alt="<c:out value="${product.name}"/><c:if test="${product.shortDesc != ''}"> - <c:out value="${product.shortDesc}"/></c:if>">
  	   </c:when>
  	   <c:otherwise>
  	   <img src="<c:if test="${!product.thumbnail.absolute}">${_contextpath}/assets/Image/Product/thumb/</c:if><c:out value="${fn:replace(product.thumbnail.imageUrl, 'http://www.dollaritem.com/wms/images/catalog/', 'http://www.dollaritem.com/wms/images/thumbs/')}"/>" border="0" class="thumbnail_image_qv" alt="<c:out value="${product.name}"/><c:if test="${product.shortDesc != ''}"> - <c:out value="${product.shortDesc}"/></c:if>">
  	   </c:otherwise>
  	  </c:choose>
  	  </a>
  	  <a href="${_contextpath}/qvproduct.jhtm?id=${product.id}" title="Quick View" class="quickViewsh16" rel="width:740,height:460" id="quickViewsh16${product.id}"></a>
 	  </div>
     </td>
    </tr>
    </c:if>
    <tr>
     <td align="center" class="thumbnail_item_name"><div class="thumbnail_item_name_link"><a href="${productLink}" 
	  			class="thumbnail_item_name"><c:out value="${product.name}" escapeXml="false"/></a></div></td>
	</tr>
    <tr> 
     <td align="center" class="thumbnail_item_sku">
      <div class="thumbnail_item_sku_link">
      <c:choose>
        <c:when test="${siteConfig['SHOW_SKU'].value == 'true' and (product.sku != null) }"><c:out value="${product.sku}" escapeXml="false"/></c:when>
        <c:otherwise>&nbsp;</c:otherwise>
      </c:choose>
      </div>
      <div class="thumbnail_item_price_wrapper">
	  <c:choose>
	  	<c:when test="${!product.loginRequire or userSession != null and !product.altLoginRequire}" >
		  <c:forEach items="${product.price}" var="price" varStatus="statusPrice">
		  <c:if test="${statusPrice.first and (price.amt > 0 || siteConfig['SHOW_ZERO_PRICE'].value == 'true')}">
		  <div class="price">
			 <fmt:message key="${siteConfig['CURRENCY'].value}" /><fmt:formatNumber value="${price.amt}" pattern="#,##0.00" />
		  </div>
		  </c:if>
		  </c:forEach>  
		</c:when>
		<c:otherwise>
		  <c:choose>
		    <c:when test="${product.altLoginRequire}">
		      <img border="0" name="_altLogintoviewprice" class="_altLogintoviewprice" src="${_contextpath}/assets/Image/Layout/altLogin_to_view_price${_lang}.gif" />
		    </c:when>
		    <c:otherwise>
		      <a href="${_contextpath}/login.jhtm?noMessage=t&forwardAction=<c:out value='${model.url}'/>" ><img border="0" name="_logintoviewprice" class="_logintoviewprice" src="${_contextpath}/assets/Image/Layout/login_to_view_price${_lang}.gif" /></a>
		    </c:otherwise>
		   </c:choose>
		</c:otherwise>
	  </c:choose>	
	  <c:if test="${gSiteConfig['gINVENTORY'] and product.inventory != null}" >
	    <c:if test="${!empty siteConfig['INVENTORY_ON_HAND'].value}" ><div class="inventory_onhand"><c:out value="${siteConfig['INVENTORY_ON_HAND'].value}" escapeXml="false"/>&nbsp;<span><c:out value="${product.inventory}" /></span></c:if>
	  </c:if>
      </div>
     </td>
    </tr>
   </table> 
  </td>
  <c:if test="${ status.index % numCul == numCul - 1 }">
    </tr>
  </c:if>

<c:if test="${status.last}" >
</table>
</c:if>