<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
    
<c:if test="${status.first}" >
<table border="0" class="multipleProductPerRow" cellspacing="10">
</c:if>
  <c:if test="${ status.index % numCul == 0 }">
    <tr>
  </c:if> 
  <td align="center" valign="top" class="thumbnail_compact_cell">
   <table  width="100%" border="0" cellpadding="0" cellspacing="0">
    <c:if test="${product.thumbnail != null}">
    <tr class="imageTr">
     <td align="center">
      <div align="center" class="thumbnail_image_link">
      <a href="${productLink}">
  	  <c:choose>
  	   <c:when test="${product.asiId != null}">
  	   <img src="<c:out value="${fn:replace(product.thumbnail.imageUrl, 'prodimgs', 'prodthumbs')}"/>" border="0" class="thumbnail_image_qv" alt="<c:out value="${product.name}"/><c:if test="${product.shortDesc != ''}"> - <c:out value="${product.shortDesc}"/></c:if>">
  	   </c:when>
  	   <c:otherwise>
  	   <img src="<c:if test="${!product.thumbnail.absolute}">${_contextpath}/assets/Image/Product/thumb/</c:if><c:out value="${fn:replace(product.thumbnail.imageUrl, 'http://www.dollaritem.com/wms/images/catalog/', 'http://www.dollaritem.com/wms/images/thumbs/')}"/>" border="0" class="thumbnail_image_qv" alt="<c:out value="${product.name}"/><c:if test="${product.shortDesc != ''}"> - <c:out value="${product.shortDesc}"/></c:if>">
  	   </c:otherwise>
  	  </c:choose>
  	  </a>
  	  </div>
 	  	<div class="thumbnail_image_qv_link"><a href="${_contextpath}/qvproduct.jhtm?id=${product.id}" title="Quick View" class="quickViewsh14" rel="width:740,height:460" id="quickViewsh14${product.id}"><img  align="middle"  class="qvImg" alt="quickview" src="${_contextpath}/assets/Image/Layout/quick_view_inactive.gif"></a></div>
     </td>
    </tr>
    </c:if>
    <tr>
     <td align="center" class="thumbnail_item_name"><div class="thumbnail_item_name_link"><a href="${productLink}" 
	  			class="thumbnail_item_name"><c:out value="${product.name}" escapeXml="false"/></a></div></td>
	</tr>
    <tr> 
     <td align="center" class="thumbnail_item_sku">
     <c:if test="${product.price != null}">
      <div class="thumbnail_item_sku_link">
      <c:choose>
        <c:when test="${siteConfig['SHOW_SKU'].value == 'true' and (product.sku != null) }"><c:out value="${product.sku}" escapeXml="false"/></c:when>
        <c:otherwise>&nbsp;</c:otherwise>
      </c:choose>
      </div>
      <div class="thumbnail_item_price_wrapper">
      <c:choose>
  	  <c:when test="${!product.loginRequire or (product.loginRequire and userSession != null)}" >
        <c:choose>
          <c:when test="${product.salesTag != null and product.salesTag.discount != 0.0 }">
            <div class="beforeSalePrice"><span id="priceWas">Was: </span><fmt:message key="${siteConfig['CURRENCY'].value}" /><fmt:formatNumber value="${product.price1}" pattern="#,##0.00" /></div>
        	<div class="afterSalePrice"><span id="priceNow">Now: </span><fmt:message key="${siteConfig['CURRENCY'].value}" /><fmt:formatNumber value="${product.price[0].discountAmt}" pattern="#,##0.00" /></div>
          </c:when>
          <c:otherwise>
            <c:if test="${product.price1 > 0 || siteConfig['SHOW_ZERO_PRICE'].value == 'true'}">
			<div class=""><fmt:message key="${siteConfig['CURRENCY'].value}" /><fmt:formatNumber value="${product.price1}" pattern="#,##0.00" /></div>
			</c:if>
		  </c:otherwise>
        </c:choose>
      </c:when>
      <c:otherwise>
	    <a href="${_contextpath}/login.jhtm?noMessage=t&forwardAction=<c:out value='${model.url}'/>" ><img border="0" name="_logintoviewprice" class="_logintoviewprice" src="${_contextpath}/assets/Image/Layout/login_to_view_price${_lang}.gif" /></a>
	  </c:otherwise>
      </c:choose>  
      </div>
      </c:if>
     </td>
    </tr>
   </table> 
  </td>
  <c:if test="${ status.index % numCul == numCul - 1 }">
    </tr>
  </c:if>

<c:if test="${status.last}" >
</table>
</c:if>