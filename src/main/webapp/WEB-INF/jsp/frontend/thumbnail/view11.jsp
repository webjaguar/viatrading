<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>

<c:if test="${status.first}">
<script type="text/javascript" >
<!--
function checkThumbnailForm(id)
{
	var el = document.getElementById("quantity_" + id);
	if (el != null) {
		if (checkNumber(el.value) == 1) {
			return true;
		} else {
			// default to 1
			el.value = 1;
			return true;
		}			
	}
	return false;
}
//-->
</script>
<table class="dynamic_thumbnail_contaiter" border="0" cellspacing="0" cellpadding="0">
</c:if>

<tr class="row"><td class="cell">
<div class="dynamic_thumbnail_item">
  <table class="dynamic_thumbnail_item_table" border="0" cellspacing="0" cellpadding="0">
  <tr valign="top">
    <td class="image">
    <div class="dynamic_thumbnail">
	  <div>
	    <c:if test="${product.thumbnail == null}">&nbsp;</c:if>
	    <c:if test="${product.thumbnail != null}">
	  	  <a href="${productLink}">
	  	  <c:choose>
	  	   <c:when test="${product.asiId != null}">
	  	   <img src="<c:out value="${fn:replace(product.thumbnail.imageUrl, 'prodimgs', 'prodthumbs')}"/>" border="0" class="thumbnail_image" alt="<c:out value="${product.name}"/><c:if test="${product.shortDesc != ''}"> - <c:out value="${product.shortDesc}"/></c:if>">
	  	   </c:when>
	  	   <c:otherwise>
	  	   <img src="<c:if test="${!product.thumbnail.absolute}">${_contextpath}/assets/Image/Product/thumb/</c:if><c:out value="${fn:replace(product.thumbnail.imageUrl, 'http://www.dollaritem.com/wms/images/catalog/', 'http://www.dollaritem.com/wms/images/thumbs/')}"/>" border="0" class="thumbnail_image" alt="<c:out value="${product.name}"/><c:if test="${product.shortDesc != ''}"> - <c:out value="${product.shortDesc}"/></c:if>">
	  	   </c:otherwise>
	  	  </c:choose>
	  	  </a>
	    </c:if>
	  </div>
	  <c:if test="${(gSiteConfig['gPRODUCT_REVIEW'] or siteConfig['PRODUCT_RATE'].value == 'true' ) and product.enableRate and siteConfig['PRODUCT_REVIEW_THIRDPARTY'].value == 'false'}">
	  <div class="dynamic_quickProductReview" align="center">
	    <a href="${productLink}#reviews"><jsp:include page="/WEB-INF/jsp/frontend/common/productRateImage.jsp"><jsp:param name="productRateAverage" value="${product.rate.average}" /></jsp:include></a>
	  </div>
	  </c:if>
	  <c:if test="${(gSiteConfig['gPRODUCT_REVIEW'] or siteConfig['PRODUCT_RATE'].value == 'true' ) and product.enableRate and siteConfig['PRODUCT_REVIEW_THIRDPARTY'].value == 'true'}">
	  <div class="dynamic_quickProductReview_tp">
	    <%@ include file="/WEB-INF/jsp/frontend/common/quickProductReviewFreeCode.jsp" %>
	  </div>
	  </c:if>
	</div>
    </td>
    <td class="content">
    <div class="dynamic_thumbnail_content"> 
		<c:if test="${siteConfig['SHOW_SKU'].value == 'true'}">
		<c:if test="${product.sku != null}">
		<div class="dynamic_thumbnail_item_sku"><c:out value="${product.sku}" /></div>  
		</c:if>
		</c:if> 
		<c:if test="${product.shortDesc != null}">
		  <div class="dynamic_thumbnail_item_desc">
		    <c:out value="${product.shortDesc}" escapeXml="false" />
		  </div>  
		  </c:if> 
		  
		  <div class="dynamic_thumbnail_item_name"><a href="${productLink}" 
			  			class="dynamic_thumbnail_item_name"><c:out value="${product.name}" escapeXml="false" /></a></div>
		  <form action="${_contextpath}/addToCart.jhtm" method="post" onsubmit="return checkThumbnailForm(${product.id})">
		  <input type="hidden" name="product.id" value="${product.id}">
		  
			  
		<div id="priceContainer">
		<div id="priceContainerBdy" class="bdy">
			<div id="priceGridContainer">
				<div class="hiddenTable" id="standardPricing">
					<table cellspacing="0" cellpadding="0" border="0" class="priceGrid" style="width: 100%;">
						<tbody>
						<tr class="qtyRow">
						  <td class="bold left qty">Quantity</td><td class="space"></td><c:set var="pcals" value="2"/>
						<c:forEach items="${product.price}" var="price" varStatus="statusPrice"><c:set var="pcals" value="${pcals+1}"/>
						  <td class="bold center"><c:choose><c:when test="${price.qtyFrom == null}"><c:out value='${product.minimumQty}' /></c:when><c:otherwise><c:out value="${price.qtyFrom}" /></c:otherwise></c:choose><c:choose><c:when test="${price.qtyTo == null}">+</c:when><c:otherwise>-<c:out value="${price.qtyTo}" /></c:otherwise></c:choose></td>
						  <c:if test="${!statusPrice.last}">
						    <td class="space"></td><c:set var="pcals" value="${pcals+1}"/>
						  </c:if>
						</c:forEach>	
						</tr>
						<tr>
							<td colspan="${pcals}" class="spacer"></td>
						</tr>
						<c:if test="${product.salesTag != null and product.salesTag.discount != 0.0}" >
						<tr class="priceRow">
							<td class="bold left">Retail</td><td class="space"></td>
						<c:forEach items="${product.price}" var="price" varStatus="statusPrice">	
							<td class="bold center" style="text-decoration: line-through"><fmt:message key="${siteConfig['CURRENCY'].value}" /><fmt:formatNumber value="${price.amt}" pattern="#,##0.00" /></td>
						  <c:if test="${!statusPrice.last}">
						    <td class="space"></td>
						  </c:if>
						</c:forEach>
						</tr>
						<tr>
							<td colspan="${pcals}" class="spacer"></td>
						</tr>
						</c:if>
						<tr class="salesRow">
							<td class="bold left"><fmt:message key="f_youPay" /></td><td class="space"></td>
							<c:forEach items="${product.price}" var="price" varStatus="statusPrice">
							  <c:choose>
							    <c:when test="${product.salesTag != null and product.salesTag.discount != 0.0}"><td class="bold center"><fmt:message key="${siteConfig['CURRENCY'].value}" /><fmt:formatNumber value="${price.discountAmt}" pattern="#,##0.00" /></td></c:when>
							    <c:otherwise><td class="bold center"><c:out value="${price.amt}" /></td></c:otherwise>
							  </c:choose>
						      <c:if test="${!statusPrice.last}">
						  		<td class="space"></td>
						  	  </c:if>
						    </c:forEach>
						</tr>
						<tr>
							<td colspan="${pcals}" class="spacer"></td>
						</tr>
						</tbody>
					</table>
				</div>
			</div>
		</div>
		</div>
		  <c:if test="${gSiteConfig['gMAIL_IN_REBATES'] > 0 and product.mailInRebate.htmlCode != null}">
		    <div class="details_rebate_htmlCode"><c:out value="${product.mailInRebate.htmlCode}" escapeXml="false"/></div>
		  </c:if>
		  <c:if test="${product.type != 'box'}"><%@ include file="/WEB-INF/jsp/frontend/common/productOptions4.jsp" %></c:if>
		
		  <c:if test="${ (gSiteConfig['gSHOPPING_CART'] and (!empty product.price or product.priceByCustomer)) and (!product.loginRequire or userSession != null)}">  
			<c:set var="zeroPrice" value="false"/>
			<c:forEach items="${product.price}" var="price">
			  <c:if test="${price.amt == 0}">
			    <c:set var="zeroPrice" value="true"/>
			  </c:if>
			</c:forEach> 
			<div class="dynamic_thumbnail_prices">
			  <c:choose>
			  <c:when test="${gSiteConfig['gVARIABLE_PRICE'] and product.priceByCustomer}">  
		        <input type="checkbox" name="quantity_${product.id}" id="quantity_${product.id}" value="1" />
		        <div class="dynamic_thumbnail_addtocart">	    
				<c:choose>
			      <c:when test="${product.htmlAddToCart != null and product.htmlAddToCart != ''}">
			        <c:out value="${product.htmlAddToCart}" escapeXml="false"/>
			      </c:when>
			      <c:otherwise>
			        <input type="image" value="Add to Cart" name="_addtocart_thumb" class="_addtocart_thumb" src="${_contextpath}/assets/Image/Layout/button_addtocart${_lang}.gif" >
			      </c:otherwise>
			    </c:choose>
			    </div>
		      </c:when>
			  <c:otherwise>
			   <c:choose>
			     <c:when test="${gSiteConfig['gINVENTORY'] and product.inventory != null and !product.negInventory and product.inventory <= 0}">
			       <div class="dynamic_thumbnail_no_stock"><c:out value="${siteConfig['OUT_OF_STOCK_TITLE'].value}" escapeXml="false"/></div> 
			     </c:when>   
			     <c:when test="${zeroPrice and siteConfig['ZERO_PRICE_HTML'].value != ''}">
			       <div class="dynamic_thumbnail_zero_price"><c:out value="${siteConfig['ZERO_PRICE_HTML'].value}" escapeXml="false"/></div>
			     </c:when>
			     <c:otherwise>
			       <input type="hidden" name="quantity_${product.id}" id="quantity_${product.id}" value="1" />
			       <div class="dynamic_thumbnail_addtocart">	    
				    <c:choose>
			          <c:when test="${product.htmlAddToCart != null and product.htmlAddToCart != ''}">
			            <c:out value="${product.htmlAddToCart}" escapeXml="false"/>
			          </c:when>
			          <c:otherwise>
			            <input type="image" value="Add to Cart" name="_addtocart_thumb" class="_addtocart_thumb" src="${_contextpath}/assets/Image/Layout/button_addtocart${_lang}.gif" >
			          </c:otherwise>
			        </c:choose>
			       </div>
			       <c:if test="${gSiteConfig['gINVENTORY'] and product.inventory != null and product.inventory < product.lowInventory}">
			         <div class="dynamic_thumbnail_low_stock"><c:out value="${siteConfig['LOW_STOCK_TITLE'].value}" escapeXml="false"/></div> 
			       </c:if>
			       <c:if test="${gSiteConfig['gINVENTORY'] and product.inventory != null and !empty siteConfig['INVENTORY_ON_HAND'].value}" >
			         <div class="dynamic_inventory_onhand"><c:out value="${siteConfig['INVENTORY_ON_HAND'].value}" escapeXml="false"/>&nbsp;<span><c:out value="${product.inventory}" /></span></div> 
			       </c:if>
			     </c:otherwise>
			   </c:choose> 
			  </c:otherwise>
			  </c:choose>
			</div>
		  </c:if>
		  </form>
		  
		  <c:if test="${gSiteConfig['gDEALS'] > 0 and product.deal.htmlCode != null}">
		  	<c:out value="${product.deal.htmlCode}" escapeXml="false" /> 
		  </c:if>
		  <c:if test="${gSiteConfig['gDEALS'] > 0 and product.deal.parentHtmlCode != null}">
				<c:out value="${product.deal.parentHtmlCode}" escapeXml="false"/>
		  </c:if>
		  
		  <c:if test="${gSiteConfig['gMYLIST'] && product.addToList && (siteConfig['MYLIST_THUMBNAIL'].value == 'true')}">
		  <form action="${_contextpath}/addToList.jhtm">
		  <input type="hidden" name="product.id" value="${product.id}">
		  <input type="image" border="0" src="${_contextpath}/assets/Image/Layout/button_addtolist${_lang}.gif">
		  </form>
		  </c:if>
    </div>
    </td>
  </tr>
  </table>
  <c:if test="${model.alsoConsiderMap[product.id] != null && (siteConfig['ALSO_CONSIDER_DETAILS_ONLY'].value == 'false')}">
   <div class="dynamic_also_consider">
   <c:set var="product" value="${model.alsoConsiderMap[product.id]}" />
   <%@ include file="/WEB-INF/jsp/frontend/common/alsoconsider.jsp" %>
   </div>
   </c:if>
</div>
</td></tr>
<c:if test="${status.last}">
</table>
</c:if>
