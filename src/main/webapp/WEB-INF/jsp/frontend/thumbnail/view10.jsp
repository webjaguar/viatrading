<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>

<c:if test="${status.first}">
<script type="text/javascript" >
<!--
function checkThumbnailForm(id)
{
	var el = document.getElementById("quantity_" + id);
	if (el != null) {
		if (checkNumber(el.value) == 1) {
			return true;
		} else {
			// default to 1
			el.value = 1;
			return true;
		}			
	}
	return false;
}
//-->
</script>
<table class="dynamic_thumbnail_contaiter" border="0" cellspacing="0" cellpadding="0">
</c:if>

<tr class="row"><td class="cell">
<div class="dynamic_thumbnail_item <c:out value="${product.productFields[3].value}"/>">
  <div class="dynamic_thumbnail">
  <div>
    <c:if test="${product.thumbnail == null}">&nbsp;</c:if>
    <c:if test="${product.thumbnail != null}">
  	  <a href="${productLink}">
  	  <c:choose>
  	   <c:when test="${product.asiId != null}">
  	   <img src="<c:out value="${fn:replace(product.thumbnail.imageUrl, 'prodimgs', 'prodthumbs')}"/>" border="0" class="thumbnail_image" alt="<c:out value="${product.name}"/><c:if test="${product.shortDesc != ''}"> - <c:out value="${product.shortDesc}"/></c:if>">
  	   </c:when>
  	   <c:otherwise>
  	   <img src="<c:if test="${!product.thumbnail.absolute}">${_contextpath}/assets/Image/Product/thumb/</c:if><c:out value="${fn:replace(product.thumbnail.imageUrl, 'http://www.dollaritem.com/wms/images/catalog/', 'http://www.dollaritem.com/wms/images/thumbs/')}"/>" border="0" class="thumbnail_image" alt="<c:out value="${product.name}"/><c:if test="${product.shortDesc != ''}"> - <c:out value="${product.shortDesc}"/></c:if>">
  	   </c:otherwise>
  	  </c:choose>
  	  </a>
    </c:if>
  </div>
  <c:if test="${(gSiteConfig['gPRODUCT_REVIEW'] or siteConfig['PRODUCT_RATE'].value == 'true' ) and product.enableRate and siteConfig['PRODUCT_REVIEW_THIRDPARTY'].value == 'false'}">
  <div class="dynamic_quickProductReview" align="center">
    <a href="${productLink}#reviews"><jsp:include page="/WEB-INF/jsp/frontend/common/productRateImage.jsp"><jsp:param name="productRateAverage" value="${product.rate.average}" /></jsp:include></a>
  </div>
  </c:if>
  <c:if test="${(gSiteConfig['gPRODUCT_REVIEW'] or siteConfig['PRODUCT_RATE'].value == 'true' ) and product.enableRate and siteConfig['PRODUCT_REVIEW_THIRDPARTY'].value == 'true'}">
  <div class="dynamic_quickProductReview_tp">
    <%@ include file="/WEB-INF/jsp/frontend/common/quickProductReviewFreeCode.jsp" %>
  </div>
  </c:if>
  </div>
  <div class="dynamic_thumbnail_content">
  
<c:if test="${siteConfig['SHOW_SKU'].value == 'true'}">
<c:if test="${product.sku != null}">
<div class="dynamic_thumbnail_item_sku"><c:out value="${product.sku}" /></div>  
</c:if>
</c:if>  
  
  <div class="dynamic_thumbnail_item_name"><a href="${productLink}" 
	  			class="dynamic_thumbnail_item_name"><c:out value="${product.name}" escapeXml="false" /></a></div>
  <form action="${_contextpath}/addToCart.jhtm" method="post" onsubmit="return checkThumbnailForm(${product.id})">
  <input type="hidden" name="product.id" value="${product.id}">
  <c:choose>
	<c:when test="${product.salesTag != null and !product.salesTag.percent}">
	  <%@ include file="/WEB-INF/jsp/frontend/common/price3.jsp" %>
	</c:when>
	<c:otherwise>
	  <%@ include file="/WEB-INF/jsp/frontend/common/price.jsp" %>
	</c:otherwise>
  </c:choose>
  <c:if test="${gSiteConfig['gMAIL_IN_REBATES'] > 0 and product.mailInRebate.htmlCode != null}">
    <div class="details_rebate_htmlCode"><c:out value="${product.mailInRebate.htmlCode}" escapeXml="false"/></div>
  </c:if>
  <c:if test="${product.type != 'box'}"><%@ include file="/WEB-INF/jsp/frontend/common/productOptions4.jsp" %></c:if>

  <c:if test="${ (gSiteConfig['gSHOPPING_CART'] and (!empty product.price or product.priceByCustomer)) and (!product.loginRequire or userSession != null)}">  
	<c:set var="zeroPrice" value="false"/>
	<c:forEach items="${product.price}" var="price">
	  <c:if test="${price.amt == 0}">
	    <c:set var="zeroPrice" value="true"/>
	  </c:if>
	</c:forEach> 
	<div class="dynamic_thumbnail_prices">
	  <c:choose>
	  <c:when test="${gSiteConfig['gVARIABLE_PRICE'] and product.priceByCustomer}">  
        <input type="checkbox" name="quantity_${product.id}" id="quantity_${product.id}" value="1" />
        <div class="dynamic_thumbnail_addtocart">	    
		<c:choose>
	      <c:when test="${product.htmlAddToCart != null and product.htmlAddToCart != ''}">
	        <c:out value="${product.htmlAddToCart}" escapeXml="false"/>
	      </c:when>
	      <c:otherwise>
	        <input type="image" value="Add to Cart" name="_addtocart_thumb" class="_addtocart_thumb" src="${_contextpath}/assets/Image/Layout/button_addtocart${_lang}.gif" >
	      </c:otherwise>
	    </c:choose>
	    </div>
      </c:when>
	  <c:when test="${product.type == 'box'}">
	   <div class="dynamic_thumbnail_box">	    
		<a href="${productLink}"  ><img border="0" src="${_contextpath}/assets/Image/Layout/button_buildthebox.gif" /></a>
	   </div>
	  </c:when>
	  <c:when test="${!empty product.numCustomLines}">
	   <div class="dynamic_thumbnail_continue">	   
        <a href="${productLink}"  ><img border="0" src="${_contextpath}/assets/Image/Layout/button_continue.gif" /></a>
       </div>
      </c:when>
	  <c:otherwise>
	   <c:choose>
	     <c:when test="${gSiteConfig['gINVENTORY'] and product.inventory != null and !product.negInventory and product.inventory <= 0}">
	       <div class="dynamic_thumbnail_no_stock"><c:out value="${siteConfig['OUT_OF_STOCK_TITLE'].value}" escapeXml="false"/></div> 
	     </c:when>   
	     <c:when test="${zeroPrice and siteConfig['ZERO_PRICE_HTML'].value != ''}">
	       <div class="dynamic_thumbnail_zero_price"><c:out value="${siteConfig['ZERO_PRICE_HTML'].value}" escapeXml="false"/></div>
	     </c:when>
	     <c:otherwise>
	       <input type="hidden" name="quantity_${product.id}" id="quantity_${product.id}" value="1" />
	       <div class="dynamic_thumbnail_addtocart">	    
		    <c:choose>
	          <c:when test="${product.htmlAddToCart != null and product.htmlAddToCart != ''}">
	            <c:out value="${product.htmlAddToCart}" escapeXml="false"/>
	          </c:when>
	          <c:otherwise>
	            <input type="image" value="Add to Cart" name="_addtocart_thumb" class="_addtocart_thumb" src="${_contextpath}/assets/Image/Layout/button_addtocart${_lang}.gif" >
	          </c:otherwise>
	        </c:choose>
	       </div>
	       <c:if test="${gSiteConfig['gINVENTORY'] and product.inventory != null and product.inventory < product.lowInventory}">
	         <div class="dynamic_thumbnail_low_stock"><c:out value="${siteConfig['LOW_STOCK_TITLE'].value}" escapeXml="false"/></div> 
	       </c:if>
	       <c:if test="${gSiteConfig['gINVENTORY'] and product.inventory != null and !empty siteConfig['INVENTORY_ON_HAND'].value}" >
	         <div class="dynamic_inventory_onhand"><c:out value="${siteConfig['INVENTORY_ON_HAND'].value}" escapeXml="false"/>&nbsp;<span><c:out value="${product.inventory}" /></span></div> 
	       </c:if>
	     </c:otherwise>
	   </c:choose> 
	  </c:otherwise>
	  </c:choose>
	</div>
  </c:if>
  </form>
  <c:if test="${product.shortDesc != null}">
  <div class="dynamic_thumbnail_item_desc">
    <c:out value="${product.shortDesc}" escapeXml="false" />
  </div>  
  </c:if>
  <c:if test="${gSiteConfig['gDEALS'] > 0 and product.deal.htmlCode != null}">
  	<c:out value="${product.deal.htmlCode}" escapeXml="false" /> 
  </c:if>
  <c:if test="${gSiteConfig['gDEALS'] > 0 and product.deal.parentHtmlCode != null}">
	<c:out value="${product.deal.parentHtmlCode}" escapeXml="false"/>
</c:if>
  <%-- start etilize --%>
  <c:if test="${model.productSummaryMap[status.index] != null}">
  <div class="dynamic_thumbnail_skus">
  <c:forEach items="${model.productSummaryMap[status.index].skus.sku}" var="sku" varStatus="skuStatus">
  	  <c:out value="${sku.type}" escapeXml="false"/>: <c:out value="${sku.number}" escapeXml="false"/><c:if test="${not skuStatus.last}">, </c:if>
  </c:forEach>
  </div>
  <div class="dynamic_thumbnail_manufacturer">
    Mfg Number: <c:out value="${model.productSummaryMap[status.index].manufacturer.number}" escapeXml="false" />
  </div>
  <div class="dynamic_thumbnail_etilize">
    Etilize ID: <c:out value="${model.productSummaryMap[status.index].id}" escapeXml="false" />
  </div>
  </c:if>
  <%-- end etilize --%>
  
  <c:if test="${gSiteConfig['gMYLIST'] && product.addToList && (siteConfig['MYLIST_THUMBNAIL'].value == 'true')}">
  <form action="${_contextpath}/addToList.jhtm">
  <input type="hidden" name="product.id" value="${product.id}">
  <input type="image" border="0" src="${_contextpath}/assets/Image/Layout/button_addtolist${_lang}.gif">
  </form>
  </c:if>
  </div>
   <div class="dynamic_also_consider">
   <c:if test="${model.alsoConsiderMap[product.id] != null && (siteConfig['ALSO_CONSIDER_DETAILS_ONLY'].value == 'false')}">
   <c:set var="product" value="${model.alsoConsiderMap[product.id]}" />
   <%@ include file="/WEB-INF/jsp/frontend/common/alsoconsider.jsp" %>
   </c:if>&nbsp;
   </div>
</div>
</td></tr>
<c:if test="${status.last}">
</table>
</c:if>
