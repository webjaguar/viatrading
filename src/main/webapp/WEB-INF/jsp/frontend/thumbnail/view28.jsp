<%@ page session="false" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>

<c:if test="${status.first}">
<script type="text/javascript" >
<!--
function checkThumbnailForm(id)
{
	var el = document.getElementById("quantity_" + id);
	if (el != null) {
		if (checkNumber(el.value) == 1) {
			return true;
		} else {
			// default to 1
			el.value = 1;
			return true;
		}			
	}
	return false;
}
//-->
</script>
</c:if>

<div class="col-sm-2">
	<div class="recommendedImageWrapper">
	<c:if test="${product.thumbnail == null}">&nbsp;</c:if>
    <c:if test="${product.thumbnail != null}">
  	  <a href="${productLink}">
  	  <c:choose>
  	   <c:when test="${product.asiId != null}">
  	   <img class="recommendedImage img-responsive" src="<c:out value="${fn:replace(product.thumbnail.imageUrl, 'prodimgs', 'prodthumbs')}"/>" border="0" class="thumbnail_image" alt="<c:out value="${product.name}"/><c:if test="${product.shortDesc != ''}"> - <c:out value="${product.shortDesc}"/></c:if>">
  	   </c:when>
  	   <c:otherwise>
  	   <img class="recommendedImage img-responsive" src="<c:if test="${!product.thumbnail.absolute}">${_contextpath}/assets/Image/Product/thumb/</c:if><c:out value="${fn:replace(product.thumbnail.imageUrl, 'http://www.dollaritem.com/wms/images/catalog/', 'http://www.dollaritem.com/wms/images/thumbs/')}"/>" border="0" class="thumbnail_image" alt="<c:out value="${product.name}"/><c:if test="${product.shortDesc != ''}"> - <c:out value="${product.shortDesc}"/></c:if>">
  	   </c:otherwise>
  	  </c:choose>
  	  </a>
    </c:if>
	</div>
	
	<c:if test="${gSiteConfig['gPRODUCT_REVIEW'] and product.enableRate}">
		<%@ include file="/WEB-INF/jsp/frontend/layout/template6/productReview2.jsp" %>
	  </c:if>
	
</div>
<div class="col-sm-10">
	<div class="details_sku">
	
	<c:if test="${siteConfig['SHOW_SKU'].value == 'true'}">
	<c:if test="${product.sku != null}">
		<div class="thumbnail_item_sku"><c:out value="${product.sku}" /></div>  
	</c:if>
	</c:if> 
	
	</div>
	<div class="details_item_name">
		<h1>${product.name}</h1>
	</div>
	<div class="prices">
		<c:choose>
		<c:when test="${product.salesTag != null and !product.salesTag.percent}">
	 		 <%@ include file="/WEB-INF/jsp/frontend/common/price3.jsp" %>
		</c:when>
		<c:otherwise>
	  		<%@ include file="/WEB-INF/jsp/frontend/common/price.jsp" %>
		</c:otherwise>
  		</c:choose>
	</div>
	<div class="productOptionName">
		<%@ include file="/WEB-INF/jsp/frontend/common/productOptions.jsp" %>
	</div>
	<form id="addToCartForm" action="/addToCart.jhtm" method="post" class="clearfix">
		 <div class="quantity_wrapper">
				<span class="quantity_title"><fmt:message key="quantity" /></span>
				<input name="quantity_${product.id}" size="5" maxlength="5" class="quantity_input" 
				<c:choose>
					<c:when test="${product.minimumQty > 0}">
						value="${product.minimumQty}"
					</c:when>
					<c:otherwise>
						value="1"
					</c:otherwise>
				</c:choose>
				type="text">
			  </div>
		<div class="addToCart_btn_wrapper">
			<button type="submit" class="btn addToCart_btn">
				Add To Cart &nbsp;<span class="addToCart_icon"></span>
			</button>
		</div>
	</form>
	
	
	<c:if test="${gSiteConfig['gINVENTORY'] and product.inventory != null and !empty siteConfig['INVENTORY_ON_HAND'].value}">
	<div class="inventory_onhand">
		<span class="inventory_onhand_title"><c:out value="${siteConfig['INVENTORY_ON_HAND'].value}" escapeXml="false"/></span>
		<span class="inventory_onhand_value"><c:out value="${product.inventory}" /></span>
	</div>
	</c:if>
	
</div>
