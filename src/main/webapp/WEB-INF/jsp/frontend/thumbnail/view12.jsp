<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>

<div id="view12">
<c:if test="${status.first}">
<form action="${_contextpath}/addToCart.jhtm" method="post" onsubmit="return checkForm()">
<c:if test="${gSiteConfig['gSHOPPING_CART']}">
<input type="image" value="Add to Cart" name="_addtocart_thumb" class="_addtocart_thumb" src="${_contextpath}/assets/Image/Layout/button_addtocart${_lang}.gif" >
</c:if>
<table border="0" class="multipleProductPerRow" cellspacing="5">
</c:if>
  <c:if test="${ status.index % numCul == 0 }">
    <tr>
  </c:if> 
  <td valign="top" align="center" class="thumbnail_compact_cell">
   <table width="100%" border="0" cellpadding="0" cellspacing="0">
    <c:if test="${siteConfig['SHOW_SKU'].value == 'true' and (product.sku != null) }">
    <tr> 
     <td align="center" class="thumbnail_item_sku"><c:out value="${product.sku}" escapeXml="false"/></td>
    </tr>
    </c:if>
    <c:if test="${product.thumbnail != null}">
    <tr>
     <td align="center">
      <a href="${productLink}">
  	  <c:choose>
  	   <c:when test="${product.asiId != null}">
  	   <img src="<c:out value="${fn:replace(product.thumbnail.imageUrl, 'prodimgs', 'prodthumbs')}"/>" border="0" class="thumbnail_image" alt="<c:out value="${product.name}"/><c:if test="${product.shortDesc != ''}"> - <c:out value="${product.shortDesc}"/></c:if>">
  	   </c:when>
  	   <c:otherwise>
  	   <img src="<c:if test="${!product.thumbnail.absolute}">${_contextpath}/assets/Image/Product/thumb/</c:if><c:out value="${fn:replace(product.thumbnail.imageUrl, 'http://www.dollaritem.com/wms/images/catalog/', 'http://www.dollaritem.com/wms/images/thumbs/')}"/>" border="0" class="thumbnail_image" alt="<c:out value="${product.name}"/><c:if test="${product.shortDesc != ''}"> - <c:out value="${product.shortDesc}"/></c:if>">
  	   </c:otherwise>
  	  </c:choose>
  	  </a>
     </td>
    </tr>
    </c:if>
    <tr>
     <td align="center" class="thumbnail_item_name" ><a href="${productLink}" 
	  			class="thumbnail_item_name"><c:out value="${product.name}" escapeXml="false"/></a><c:if test="${ (gSiteConfig['gSHOPPING_CART'] and !empty product.price) and (!product.loginRequire or userSession != null) and !empty product.salesTag }"><img width="25px"  src="${_contextpath}/assets/Image/Layout/SALE.gif" /></c:if>
	  			<c:if test="${gSiteConfig['gBUDGET_PRODUCT']}">
	  			<div class="budget_balance"><fmt:message key="balance" />: <c:choose><c:when test="${model.budgetProduct[product.sku] != null}"><c:out value="${model.budgetProduct[product.sku].balance}"/></c:when><c:otherwise>0</c:otherwise></c:choose></div>
	  			</c:if>
	  			</td>
    </tr>
    <c:if test="${gSiteConfig['gDEALS'] > 0 and product.deal.htmlCode != null}">
	<tr>
	  <td><c:out value="${product.deal.htmlCode}" escapeXml="false"/></td>
	</tr>
	</c:if>
	<c:if test="${gSiteConfig['gDEALS'] > 0 and product.deal.parentHtmlCode != null}">
		<tr>
		<td><c:out value="${product.deal.parentHtmlCode}" escapeXml="false"/></td>
		</tr>
	</c:if>
  <c:choose>
  	<c:when test="${!product.loginRequire or userSession != null}" >
	  <c:if test="${product.msrp > 0}">
	     <tr id="thumbnail_v12_price">
	       <td align="center">
	         <div class="msrpTitle"><c:choose><c:when test="${siteConfig['MSRP_TITLE'].value != ''}"><c:out value="${siteConfig['MSRP_TITLE'].value}" /></c:when><c:otherwise><fmt:message key="productMsrp" /></c:otherwise></c:choose>&nbsp;<fmt:message key="${siteConfig['CURRENCY'].value}" /><fmt:formatNumber value="${product.msrp }" pattern="#,##0.00" /></div>
	         <c:if test="${gSiteConfig['gINVENTORY'] and product.inventory != null}" >
                <c:if test="${!empty siteConfig['INVENTORY_ON_HAND'].value}" ><div class="inventory_onhand"><c:out value="${siteConfig['INVENTORY_ON_HAND'].value}" escapeXml="false"/>&nbsp;<span><c:out value="${product.inventory}" /></span></div></c:if>
             </c:if>
	       </td>
	     </tr>
	  </c:if>    
	  <c:if test="${empty product.msrp}">	    
	  	  <tr id="thumbnail_v12_price"><td colspan="4">&nbsp;</td></tr> 
	  </c:if>  
	</c:when>
	<c:otherwise>
	  <tr id="thumbnail_v12_price"><td align="center"><a href="${_contextpath}/login.jhtm?noMessage=t&forwardAction=<c:out value='${model.url}'/>"  ><img border="0" style="padding-bottom:0px" name="_logintoviewprice" class="_logintoviewprice" src="${_contextpath}/assets/Image/Layout/login_to_view_price${_lang}.gif" /></a></td></tr>
	</c:otherwise>
  </c:choose> 
  <c:choose>
    <c:when test="${ (gSiteConfig['gSHOPPING_CART'] and !empty product.msrp) and (!product.loginRequire or userSession != null)}">
	<c:set var="zeroPrice" value="false"/>
	<c:forEach items="${product.price}" var="price">
	  <c:if test="${price.amt == 0}">
	    <c:set var="zeroPrice" value="true"/>
	  </c:if>
	</c:forEach> 
      <input type="hidden" name="product.id" value="${product.id}">
	 <tr id="thumbnail_v12_qty">
	  <c:choose>
	   <c:when test="${product.type == 'box'}">
	    <td align="center">	    
		 <a href="${productLink}"  ><img border="0" src="${_contextpath}/assets/Image/Layout/button_buildthebox.gif" /></a>
	    </td>
	   </c:when>
	   <c:otherwise>
	      <c:choose>
		     <c:when test="${gSiteConfig['gINVENTORY'] and product.inventory != null and !product.negInventory and product.inventory <= 0}" >
		        <td align="center" class="thumbnail_item_outofstock"><c:out value="${siteConfig['OUT_OF_STOCK_TITLE'].value}" escapeXml="false"/></td> 
		     </c:when>
			 <c:when test="${zeroPrice and siteConfig['ZERO_PRICE_HTML'].value != ''}">
	      		<td align="center"><c:out value="${siteConfig['ZERO_PRICE_HTML'].value}" escapeXml="false"/></td>
	    	  </c:when>		     
		     <c:otherwise>
		        <td align="center" class="thumbnail_item_qty">
				  <fmt:message key="qty" />: <input type="text" size="6" maxlength="5" name="quantity_${product.id}" id="quantity_${product.id}" value="" >
				   <c:choose><c:when test="${!empty product.packing}"><c:out value="${product.packing}"/></c:when><c:otherwise><c:if test="${!empty product.caseContent}"> X <c:out value="${product.caseContent}"/></c:if></c:otherwise></c:choose><br />
		          <c:if test="${gSiteConfig['gINVENTORY'] and product.inventory != null and product.inventory < product.lowInventory}">
		           <div align="center" class="thumbnail_item_lowStock"><c:out value="${siteConfig['LOW_STOCK_TITLE'].value}" escapeXml="false"/></div> 
		          </c:if>
				</td>
		     </c:otherwise>
	      </c:choose> 
	  </c:otherwise>
	  </c:choose>
	 </tr>    
    </c:when>
    <c:otherwise>
      <tr><td class="thumbnail_item_name" height="30px"></td>
	 </tr>    
    </c:otherwise>  
  </c:choose>

   </table> 
  </td>
  <c:if test="${ status.index % numCul == numCul - 1 }">
    </tr>
  </c:if>
 
<c:if test="${status.last}">
</table>
<c:if test="${gSiteConfig['gSHOPPING_CART']}">
<input type="image" value="Add to Cart" name="_addtocart_thumb" class="_addtocart_thumb" src="${_contextpath}/assets/Image/Layout/button_addtocart${_lang}.gif" >
</c:if>
</form>
</c:if>
</div>