<%@ page session="false" %>
<%@ page import="java.io.*,java.util.*" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
        
<div id="pageHeader">	
  <div id="pageBodyWrapper">
	<div id="pageBody" class="bothSidebars">
	  <div id="contentWrapper" class="bothSidebars">
		<div id="content">
          <style>
            .clearboth {
             height: 0;
             margin: 0;
             padding: 0;
             }
          </style>
		 <br class="clear"/>
		 <!-- Featured Products Begin -->
		 <div class="list-carousel responsive">
		   <ul id="foo4">
		     <c:forEach items="${model.products}" var="product" varStatus="status"> 
		       <li class="product-grid-container">					
		         <div class="product-item">						
			       <div class="title">
			         <span class="title-container">
				       <a href="${_contextpath}/product.jhtm?id=${product.id}&cid=${param.cid}">${product.name}</a>
			         </span>
			       </div>					
	    	       <div class="image mosaic-block bar" style="display: block;">
			         <div class="price"><fmt:message key="${siteConfig['CURRENCY'].value}" /><fmt:formatNumber value="${product.price[0].amt}" pattern="#,##0.00"  /></div>
			         <a href="${_contextpath}/product.jhtm?id=${product.id}&cid=${param.cid}" target="_blank" class="mosaic-overlay">
				       <div class="details">
				         <c:out value="${product.longDesc}" />
				       </div>								
			         </a>
			         <a href="${_contextpath}/product.jhtm?id=${product.id}&cid=${param.cid}">
			           <img src="<c:if test="${!product.thumbnail.absolute}">${_contextpath}/assets/Image/Product/thumb/</c:if><c:out value="${product.thumbnail.imageUrl}"/>" border="0">
  	   			     </a>
			       </div>
		     	   <div class="info">
			         
			         
		             <span class="button-small">
					   <a href="${_contextpath}/product.jhtm?id=${product.id}&cid=${param.cid}"><fmt:message key="f_viewDetails" /></a>
					 </span>
	     	       </div>
		         </div>
	           </li>					
	         </c:forEach>			
		    </ul>
			<br class="clear"/>
			<div id="pager2" class="pager"></div>
		</div>
		<br class="clear"/>
		</div> 
		<div class="clearboth">&nbsp;</div><!-- stop floating (if any) -->
	</div><!-- end content -->
   </div><!-- end contentWrapperThreeCol -->
   <div class="clearboth">&nbsp;</div><!-- stop floating (if any) -->
  </div><!-- end pageBody -->
</div><!-- end pageBodyWrapper -->
<script type="text/javascript">
	$(window).load(function(){

		//	Responsive layout, resizing the items
		$('#foo4').carouFredSel({
			responsive: true,
			auto: {
					timeoutDuration : 4000,     //  5 * auto.duration
			},
			width: '100%',
			scroll: {
				items : 'auto',     //  5 * auto.duration
				pauseOnHover    : true,     //  scroll.pauseOnHover
				},
			prev: '#prev2',
			next: '#next2',
			pagination: "#pager2",
			items: {
				width: 250,
				height: 'auto',	//	optionally resize item-height
				visible: {
					min: 1,
					max: 4
				}
			}
		});

    });
</script>
<div class="clearboth">&nbsp;</div><!-- stop floating (if any) -->	