<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ taglib uri="/WEB-INF/tlds/custom.tld" prefix="wj" %>

<%--  This javascript must be available on jsp only once hence put status.first condition on if statement --%>
<c:if test="${gSiteConfig['gCOMPARISON'] and siteConfig['COMPARISON_ON_CATEGORY'].value == 'true' and status.first}">
<script type="text/javascript">
function compareProduct(productId){
	if(parseInt($('currentComparison').value) < parseInt($('maxComparisons').value)) {
		var requestHTMLData = new Request.HTML ({
			url: "${_contextpath}/ajaxAddToCompare.jhtm?productId="+productId,
			onSuccess: function(response){ 
				$('product'+productId).set('html', 'Added to Compare');
				$('currentComparison').value = parseInt($('currentComparison').value)+ 1;
		 	}
		}).send();
	} else {
		alert('Max comparison limit reached.');
	}
}
</script>
</c:if>	   
<div id="view7_1">   
<c:if test="${status.first}" >
<c:if test="${gSiteConfig['gCOMPARISON'] and siteConfig['COMPARISON_ON_CATEGORY'].value == 'true'}">
<div id="compareButton"> 
   <a href="${_contextpath}/comparison.jhtm"> <img alt="" src="${_contextpath}/assets/Image/Layout/button_compare.gif"> </a>
   <input type="hidden" id="maxComparisons" value="${model.maxComparisons}">
   <input type="hidden" id="currentComparison" value="${fn:length(model.comparisonMap)}">
</div>
</c:if>
<table border="0" class="multipleProductPerRow" cellspacing="10">
</c:if>
  <c:if test="${ status.index % numCul == 0 }">
    <tr>
  </c:if> 
  <td valign="top" class="thumbnail_compact_cell">
   <table width="100%" border="0" cellpadding="0" cellspacing="0">
    <c:if test="${product.thumbnail != null}">
    <tr>
     <td align="center">
     <div class="cat_image_wrapper">
     <a href="${productLink}">
  	  <c:choose>
  	   <c:when test="${product.asiId != null}">
  	     <c:set value="${wj:asiBigImageName(product.thumbnail.imageUrl)}" var="imageUrl" />
  	     <c:set value="${fn:replace(imageUrl, '?size=large', '')}" var="imageUrl" />
  	     <img src="<c:if test="${!product.thumbnail.absolute}">${_contextpath}/assets/Image/Product/thumb/</c:if><c:out value="${imageUrl}"/>" border="0" class="thumbnail_image" alt="<c:out value="${product.name}"/><c:if test="${product.shortDesc != ''}"> - <c:out value="${product.shortDesc}"/></c:if>">
  	   </c:when>
  	   <c:otherwise>
  	   <img src="<c:if test="${!product.thumbnail.absolute}">${_contextpath}/assets/Image/Product/thumb/</c:if><c:out value="${fn:replace(product.thumbnail.imageUrl, 'http://www.dollaritem.com/wms/images/catalog/', 'http://www.dollaritem.com/wms/images/thumbs/')}"/>" border="0" class="thumbnail_image" alt="<c:out value="${product.name}"/><c:if test="${product.shortDesc != ''}"> - <c:out value="${product.shortDesc}"/></c:if>">
  	   </c:otherwise>
  	  </c:choose>
  	  </a>
  	  </div>
     </td>
    </tr>	
    </c:if>
    <tr>
     <td align="center">
     <div class="cat_name_wrapper">
       <a href="${productLink}" class="thumbnail_item_name"><c:out value="${product.name}" escapeXml="false"/></a>
     </div>
     </td>
    </tr>
    <c:if test="${siteConfig['SHOW_SKU'].value == 'true' and (product.sku != null) }">
    <tr> 
     <td align="center" class="thumbnail_item_sku"><c:out value="${product.sku}" escapeXml="false"/></td>
    </tr>
    </c:if>
    <%-- Display product field only in PPG Apparel Layout and if field id is abvailable on siteConfig --%>
    <c:if test="${siteConfig['SEARCH_RESULT_DISPALY_FIELD_ID'].value != '' and ( product.productLayout == '014' or product.productLayout == null or  (product.productLayout == '' and siteConfig['PRODUCT_DEFAULT_DETAIL_LAYOUT'].value == '014')) }">
      <tr>
        <td align="center">
	      <div class="product_field">
	        <c:forEach items="${product.productFields}" var="productField" varStatus="row">
			  <c:if test="${productField.id == siteConfig['SEARCH_RESULT_DISPALY_FIELD_ID'].value}">
			    <c:out value="${productField.value}" escapeXml="false"/>
	          </c:if>
		    </c:forEach>
	      </div>
        </td>
      </tr>
    </c:if>
    <tr>
     <td align="center"><%@ include file="/WEB-INF/jsp/frontend/common/priceRange.jsp" %></td>
    </tr>
    <c:if test="${empty product.masterSku}">
    <tr class="thumbnail_item_select_box">
     <td align="center"><a href="${productLink}" class="thumbnail_item_select"><fmt:message key="f_selectProduct"/></a></td>
    </tr>
    </c:if>
    <c:if test="${gSiteConfig['gCOMPARISON'] and siteConfig['COMPARISON_ON_CATEGORY'].value == 'true' and product.compare}">
    <tr>
     <td align="center" id="product${product.id}">
       <div class="cat_compare_wrapper">
       <c:choose>
         <c:when test="${model.comparisonMap[product.id] != null}"><fmt:message key="addedToCompare"/></c:when>
         <c:otherwise><input type="checkbox" title="compare" class="compare" value="${product.id}"  onchange="compareProduct('${product.id}');"/>compare</c:otherwise>
       </c:choose>
       </div>
     </td>
    </tr>
    </c:if>
   </table> 
  </td>
  <c:if test="${ status.index % numCul == numCul - 1 }">
    </tr>
  </c:if>

<c:if test="${status.last}" >
</table>
</c:if>
</div>
