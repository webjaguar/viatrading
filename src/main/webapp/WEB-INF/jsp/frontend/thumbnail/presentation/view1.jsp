<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>

<c:if test="${status.first}">
<form action="${_contextpath}/addToPresentation.jhtm" method="post">
<c:if test="${gSiteConfig['gSHOPPING_CART']}">
<input type="image" value="Add to Presentation" name="_addtocart_thumb" class="_addtocart_thumb" src="${_contextpath}/assets/Image/Layout/button_addtopresentationlist${_lang}.gif" >
</c:if>
<table border="0" class="multipleProductPerRow" cellspacing="5">
</c:if>
  <c:if test="${ status.index % numCul == 0 }">
    <tr>
  </c:if> 
  
<input type="hidden" name="product.id" value="${product.id}">
  <td valign="top" align="center" class="thumbnail_compact_cell">
   <table width="100%" border="0" cellpadding="0" cellspacing="0">
    <c:if test="${siteConfig['SHOW_SKU'].value == 'true' and (product.sku != null) }">
    <tr> 
     <td  class="thumbnail_item_sku"><c:out value="${product.sku}" escapeXml="false"/></td>
    </tr>
    </c:if>
    <c:if test="${product.thumbnail != null}">
    <tr>
     <td >
     <a href="${productLink}">
  	  <c:choose>
  	   <c:when test="${product.asiId != null}">
  	   <img src="<c:out value="${fn:replace(product.thumbnail.imageUrl, 'prodimgs', 'prodthumbs')}"/>" border="0" class="thumbnail_image" alt="<c:out value="${product.name}"/><c:if test="${product.shortDesc != ''}"> - <c:out value="${product.shortDesc}"/></c:if>">
  	   </c:when>
  	   <c:otherwise>
  	   <img src="<c:if test="${!product.thumbnail.absolute}">${_contextpath}/assets/Image/Product/thumb/</c:if><c:out value="${fn:replace(product.thumbnail.imageUrl, 'http://www.dollaritem.com/wms/images/catalog/', 'http://www.dollaritem.com/wms/images/thumbs/')}"/>" border="0" class="thumbnail_image" alt="<c:out value="${product.name}"/><c:if test="${product.shortDesc != ''}"> - <c:out value="${product.shortDesc}"/></c:if>">
  	   </c:otherwise>
  	  </c:choose>
  	  </a>
     </td>
    </tr>
    </c:if>
    <tr>
     <td  class="thumbnail_item_name" ><a href="${productLink}" 
	  			class="thumbnail_item_name"><c:out value="${product.name}" escapeXml="false"/></a><c:if test="${ (gSiteConfig['gSHOPPING_CART'] and !empty product.price) and (!product.loginRequire or userSession != null) and !empty product.salesTag }"><img width="25px"  src="${_contextpath}/assets/Image/Layout/SALE.gif" /></c:if>
	  			<c:if test="${gSiteConfig['gBUDGET_PRODUCT']}">
	  			<div class="budget_balance"><fmt:message key="balance" />: <c:choose><c:when test="${model.budgetProduct[product.sku] != null}"><c:out value="${model.budgetProduct[product.sku].balance}"/></c:when><c:otherwise>0</c:otherwise></c:choose></div>
	  			</c:if>
	 </td>
    </tr>
    <c:if test="${gSiteConfig['gDEALS'] > 0 and product.deal.htmlCode != null}">
	<tr>
	  <td><c:out value="${product.deal.htmlCode}" escapeXml="false"/></td>
	</tr>
	</c:if>
		 <c:if test="${gSiteConfig['gDEALS'] > 0 and product.deal.htmlCode != null}">
	<tr>
	  <td><c:out value="${product.deal.htmlCode}" escapeXml="false"/></td>
	</tr>
	</c:if>
	 <c:if test="${gSiteConfig['gDEALS'] > 0 and product.deal.htmlCode != null}">
	<tr>
	  <td><c:out value="${product.deal.htmlCode}" escapeXml="false"/></td>
	</tr>
	</c:if>
	<c:if test="${product.msrp != null}">
	  <tr class="thumbnail_v8_msrp">
	    <td><div class="price_msrp"><c:choose><c:when test="${siteConfig['MSRP_TITLE'].value != ''}"><c:out value="${siteConfig['MSRP_TITLE'].value}" /></c:when><c:otherwise><fmt:message key="productMsrp" /></c:otherwise></c:choose>: <fmt:message key="${siteConfig['CURRENCY'].value}" /> <fmt:formatNumber value="${product.msrp}" pattern="#,##0.00" /></div></td>	
	  </tr>
    </c:if>
  <c:choose>
  	<c:when test="${!product.loginRequire or userSession != null}" >
	  <c:forEach items="${product.price}" var="price" varStatus="statusPrice">
	  <c:set var="multiplier" value="1"/>
	  <c:if test="${siteConfig['CASE_CONTENT_LAYOUT'].value == '1' and product.caseContent > 0}">
	     <c:set var="multiplier" value="${product.caseContent}"/>	
	  </c:if>
	    <c:if test="${statusPrice.first and (price.amt > 0 || siteConfig['SHOW_ZERO_PRICE'].value == 'true')}">
	    <c:choose>
	     <c:when test="${price.qtyFrom != null}"><tr  class="thumbnail_v8_price"><td><%@ include file="/WEB-INF/jsp/frontend/common/priceRange.jsp" %></td></tr></c:when>
	     <c:otherwise><tr class="thumbnail_v8_price">
	       <td >
	         <c:choose>
	         <c:when test="${product.salesTag != null and product.salesTag.discount != 0.0 }" >
		      <div class="price_range"><fmt:message key="f-price" />: <fmt:message key="${siteConfig['CURRENCY'].value}" /><fmt:formatNumber value="${price.discountAmt*multiplier}" pattern="#,##0.00" /></div>
		     </c:when>
		     <c:otherwise>
		     <div class="price_range"><fmt:message key="f-price" />: <fmt:message key="${siteConfig['CURRENCY'].value}" /><fmt:formatNumber value="${price.amt*multiplier}" pattern="#,##0.00" /></div>
		     </c:otherwise>
		     </c:choose>
	         <c:if test="${gSiteConfig['gINVENTORY'] and product.inventory != null}" >
                <c:if test="${!empty siteConfig['INVENTORY_ON_HAND'].value}" ><div class="inventory_onhand"><c:out value="${siteConfig['INVENTORY_ON_HAND'].value}" escapeXml="false"/>&nbsp;<span><c:out value="${product.inventory}" /></span></div></c:if>
             </c:if>
	       </td></tr>
	     </c:otherwise>
	    </c:choose>	 
	    </c:if>    
	  </c:forEach>
	  <c:if test="${empty product.price}">	    
	  	  <tr id="thumbnail_v8_price"><td colspan="4">&nbsp;</td></tr> 
	  </c:if>  
	</c:when>
	<c:otherwise>
	  <tr id="thumbnail_v8_price"><td><a href="${_contextpath}/login.jhtm?noMessage=t&forwardAction=<c:out value='${model.url}'/>"  ><img border="0" style="padding-bottom:0px" name="_logintoviewprice" class="_logintoviewprice" src="${_contextpath}/assets/Image/Layout/login_to_view_price${_lang}.gif" /></a></td></tr>
	</c:otherwise>
  </c:choose>	  
  <c:choose>
    <c:when test="${ (gSiteConfig['gSHOPPING_CART'] and !empty product.price) and (!product.loginRequire or userSession != null)}">
	<c:set var="zeroPrice" value="false"/>
	<c:forEach items="${product.price}" var="price">
	  <c:if test="${price.amt == 0}">
	    <c:set var="zeroPrice" value="true"/>
	  </c:if>
	</c:forEach>    
    </c:when>
    <c:otherwise>
      <tr><td class="thumbnail_item_name" height="30px"></td>
	 </tr>    
    </c:otherwise>  
  </c:choose>
  <tr>
  	<td>
  		<div class="add_buttons_wrapper_box">
			<c:if test="${product.addToPresentation}">
			  <div class="presentation_add_button">
				<a href="${_contextpath}/addToPresentation.jhtm?id=${product.id}" rel="width:400,height:380"   class="add_to_presentation">Add to Presentation</a>
			  </div>
			  <div class="presentation_add_checkbox">
			  <c:out value="Add to Presentation List "/><input name="__selected_${product.id}" value="true" type="checkbox" />
			 </div>
			</c:if>
		</div>
  	</td>
  </tr>

   </table> 
  </td>
  <c:if test="${ status.index % numCul == numCul - 1 }">
    </tr>
  </c:if>
 
<c:if test="${status.last}">
</table>
<input type="image" value="Add to Presentation" name="_addtopresentation_thumb" class="_addtopresentation_thumb" src="${_contextpath}/assets/Image/Layout/button_addtopresentationlist${_lang}.gif" >
</form>
</c:if>