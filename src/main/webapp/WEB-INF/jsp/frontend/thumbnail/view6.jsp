<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>

<script type="text/javascript">
<!--
// popup for View6
var IE = document.all ? 1 : 0;
var ns = document.layers ? 1 : 0;
var cX , cY ;
if (IE) { // grab the x-y pos.s if browser is IE
  cX = event.clientX + document.body.scrollLeft
  cY = event.clientY + document.body.scrollTop
} else { // grab the x-y pos.s if browser is NS
  cX = event.pageX
  cY = event.pageY
}

function toggleBox(szDivID, iState) // 1 visible, 0 hidden
{   
    if(ns)	   //NN4+
    {
       document.layers[szDivID].style.left = ( cX+10 ); 
       document.layers[szDivID].style.top = ( cY+10 ); 
       document.layers[szDivID].visibility = iState ? "show" : "hide";
    }
    else if(document.getElementById)	  //gecko(NN6) + IE 5+
    {
        var obj = document.getElementById(szDivID);
        obj.style.left = ( cX+10 ); 
        obj.style.top = ( cY+10 ); 
        obj.style.visibility = iState ? "visible" : "hidden";
    }
    else if(IE)	// IE 4
    {
        szDivID.style.left = ( cX+10 ); 
        szDivID.style.top = ( cY+10 ); 
        document.all[szDivID].style.visibility = iState ? "visible" : "hidden";
    }
}
//-->
</script>
	        
<c:if test="${status.first}" >
<form action="${_contextpath}/addToCart.jhtm" method="post" >
<table border="0" class="multipleProductPerRow" >
</c:if>
  <c:if test="${ status.index % numCul == 0 }">
    <tr>
  </c:if>  
  <td valign="top" class="thumbnail_compact_cell">
   <table width="100%" border="0" cellpadding="0" cellspacing="0">
    <tr>
     <td class="thumbnail_item_name" ><a href="${productLink}" 
	  			class="thumbnail_item_name"><c:out value="${product.name}" escapeXml="false"/></a><c:if test="${ (gSiteConfig['gSHOPPING_CART'] and !empty product.price) and (!product.loginRequire or userSession != null) and !empty product.salesTag }"><img width="25px"  src="${_contextpath}/assets/Image/Layout/SALE.gif" /></c:if></td>
    </tr>
    <tr> 
     <td class="thumbnail_item_sku">
      <c:choose>
        <c:when test="${siteConfig['SHOW_SKU'].value == 'true' and (product.sku != null) }"><c:out value="${product.sku}" escapeXml="false"/></c:when>
        <c:otherwise>&nbsp;</c:otherwise>
      </c:choose>
     </td>
    </tr>
    <c:if test="${product.thumbnail != null}">
    <tr>
     <td>
     <a href="${productLink}">
  	  <c:choose>
  	   <c:when test="${product.asiId != null}">
  	   <img src="<c:out value="${fn:replace(product.thumbnail.imageUrl, 'prodimgs', 'prodthumbs')}"/>" border="0" class="thumbnail_image" onmouseover="toggleBox('demodiv<c:out value="${product.id}" />',1);" alt="<c:out value="${product.name}"/><c:if test="${product.shortDesc != ''}"> - <c:out value="${product.shortDesc}"/></c:if>">
  	   </c:when>
  	   <c:otherwise>
  	   <img src="<c:if test="${!product.thumbnail.absolute}">${_contextpath}/assets/Image/Product/thumb/</c:if><c:out value="${fn:replace(product.thumbnail.imageUrl, 'http://www.dollaritem.com/wms/images/catalog/', 'http://www.dollaritem.com/wms/images/thumbs/')}"/>" border="0" class="thumbnail_image" onmouseover="toggleBox('demodiv<c:out value="${product.id}" />',1);" alt="<c:out value="${product.name}"/><c:if test="${product.shortDesc != ''}"> - <c:out value="${product.shortDesc}"/></c:if>">
  	   </c:otherwise>
  	  </c:choose>
  	  </a>
  	  </td>
    </tr>
   <div id="demodiv<c:out value="${product.id}" />" class="demo" onmouseout="toggleBox('demodiv<c:out value="${product.id}" />',0);">
     <c:out value="${product.name}" escapeXml="false"/><br />
     <a href="${productLink}" ><img border="1"  src="<c:if test="${!product.thumbnail.absolute}">${_contextpath}/assets/Image/Product/thumb/</c:if><c:out value="${fn:replace(product.thumbnail.imageUrl, 'http://www.dollaritem.com/wms/images/catalog/', 'http://www.dollaritem.com/wms/images/thumbs/')}"/>" height="200" alt="<c:out value="${product.name}"/><c:if test="${product.shortDesc != ''}"> - <c:out value="${product.shortDesc}"/></c:if>"></a><br />
    
     <c:out value="${product.shortDesc}" escapeXml="false"/><br />
     <c:if test="${product.caseContent != null}"> <c:out value='${product.packing}'/> contains <c:out value='${product.caseContent}'/> <c:out value="${siteConfig['CASE_UNIT_TITLE'].value}" /></c:if> 
     <c:if test="${ (product.salesTag.image and !empty product.price) and (!product.loginRequire or userSession != null) }" >
      <img align="center"  src="${_contextpath}/assets/Image/PromoSalesTag/salestag_${product.salesTag.tagId}.gif" /><br />
     </c:if>
   </div>
   </c:if>
  <c:choose>
  	<c:when test="${!product.loginRequire or userSession != null}" >
	  <c:forEach items="${product.price}" var="price" varStatus="statusPrice">
	    <c:if test="${statusPrice.first and (price.amt > 0 || siteConfig['SHOW_ZERO_PRICE'].value == 'true')}">
	    <c:choose>
	     <c:when test="${price.qtyFrom != null}"><td align="center"><%@ include file="/WEB-INF/jsp/frontend/common/priceRange.jsp" %></td></c:when>
	     <c:otherwise>
		<c:set var="multiplier" value="1"/>
		<c:if test="${siteConfig['CASE_CONTENT_LAYOUT'].value == '1' and product.caseContent > 0}">
		  <c:set var="multiplier" value="${product.caseContent}"/>	
		</c:if>
	     <td class="price_range"><span class="priceTitle"><fmt:message key="f-price" />: </span><fmt:message key="${siteConfig['CURRENCY'].value}" /><fmt:formatNumber value="${price.amt*multiplier}" pattern="#,##0.00" /></td>
	     </c:otherwise>
	    </c:choose>	 
	    </c:if>    
	  </c:forEach>
	  <c:if test="${empty product.price}">	    
	  	  <tr><td colspan="4">&nbsp;</td></tr> 
	  </c:if>  
	</c:when>
	<c:otherwise>
	  <tr><td ><a href="${_contextpath}/login.jhtm?noMessage=t&forwardAction=<c:out value='${model.url}'/>"  ><img border="0" style="padding-bottom:0px" name="_logintoviewprice" class="_logintoviewprice" src="${_contextpath}/assets/Image/Layout/login_to_view_price${_lang}.gif" /></a></td></tr>
	</c:otherwise>
  </c:choose>	  
    
            
  <c:choose>
    <c:when test="${ (gSiteConfig['gSHOPPING_CART'] and !empty product.price) and (!product.loginRequire or userSession != null)}">
	<c:set var="zeroPrice" value="false"/>
	<c:forEach items="${product.price}" var="price">
	  <c:if test="${price.amt == 0}">
	    <c:set var="zeroPrice" value="true"/>
	  </c:if>
	</c:forEach>     
      <input type="hidden" name="product.id" value="${product.id}">
	 <tr>
	  <c:choose>
	   <c:when test="${product.type == 'box'}">
	    <td>	    
		 <a href="${productLink}"  ><img border="0" src="${_contextpath}/assets/Image/Layout/button_buildthebox.gif" /></a>
	    </td>
	   </c:when>
	   <c:otherwise>
	      <c:choose>
		     <c:when test="${gSiteConfig['gINVENTORY'] and product.inventory != null and !product.negInventory and product.inventory <= 0}" >
		        <td><c:out value="${siteConfig['OUT_OF_STOCK_TITLE'].value}" escapeXml="false"/></td> 
		     </c:when>
		     <c:when test="${zeroPrice and siteConfig['ZERO_PRICE_HTML'].value != ''}">
	      	    <td><c:out value="${siteConfig['ZERO_PRICE_HTML'].value}" escapeXml="false"/></td>
	    	 </c:when>
		     <c:otherwise>
		        <td class="thumbnail_item_name">
			     <fmt:message key="qty" />: <input type="text" size="6" maxlength="5" name="quantity_${product.id}" id="quantity_${product.id}" value="" >
			      <c:if test="${product.packing != ''}"> <c:out value="${product.packing}"/> </c:if><br />
			     <input type="image" value="Add to Cart" name="_addtocart_thumb" class="_addtocart_thumb" src="${_contextpath}/assets/Image/Layout/button_addtocart${_lang}.gif" >
		         <c:if test="${gSiteConfig['gINVENTORY'] and product.inventory != null and product.inventory < product.lowInventory}">
		           <div><c:out value="${siteConfig['LOW_STOCK_TITLE'].value}" escapeXml="false"/></div> 
		         </c:if>
			    </td>
		     </c:otherwise>
	      </c:choose> 
	  </c:otherwise>
	  </c:choose>
	 </tr>    
    </c:when>
    <c:otherwise>
      <tr><td class="thumbnail_item_name" height="30px"></td>
	 </tr>    
    </c:otherwise>  
  </c:choose>

   </table> 
  </td>
  <c:if test="${ status.index % numCul == numCul - 1 }">
    </tr>
  </c:if>
 
<c:if test="${status.last}" >
</table>
</form>
</c:if>