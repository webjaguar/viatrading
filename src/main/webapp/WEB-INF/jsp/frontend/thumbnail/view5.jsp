<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
	        
<c:if test="${status.first}" >
<table border="0" class="multipleProductPerRow" cellspacing="10">
</c:if>
  <c:if test="${ status.index % numCul == 0 }">
    <tr>
  </c:if> 
  <td align="center" valign="top" class="thumbnail_compact_cell">
   <table  width="100%" border="0" cellpadding="0" cellspacing="0">
    <c:if test="${product.thumbnail != null}">
    <tr> 
     <td align="center">
      <a href="${productLink}">
  	  <c:choose>
  	   <c:when test="${product.asiId != null}">
  	   <img src="<c:out value="${fn:replace(product.thumbnail.imageUrl, 'prodimgs', 'prodthumbs')}"/>" border="0" class="thumbnail_image" alt="<c:out value="${product.name}"/><c:if test="${product.shortDesc != ''}"> - <c:out value="${product.shortDesc}"/></c:if>">
  	   </c:when>
  	   <c:otherwise>
  	   <img src="<c:if test="${!product.thumbnail.absolute}">${_contextpath}/assets/Image/Product/thumb/</c:if><c:out value="${fn:replace(product.thumbnail.imageUrl, 'http://www.dollaritem.com/wms/images/catalog/', 'http://www.dollaritem.com/wms/images/thumbs/')}"/>" border="0" class="thumbnail_image" alt="<c:out value="${product.name}"/><c:if test="${product.shortDesc != ''}"> - <c:out value="${product.shortDesc}"/></c:if>">
  	   </c:otherwise>
  	  </c:choose>
  	  </a>
     </td>
    </tr>
    </c:if>
    <tr>
     <td align="center" class="thumbnail_item_name"><a href="${productLink}" 
	  			class="thumbnail_item_name"><c:out value="${product.name}" escapeXml="false"/></a></td>
    </tr>
    <tr> 
     <td align="center" class="thumbnail_item_sku">
      <c:choose>
        <c:when test="${siteConfig['SHOW_SKU'].value == 'true' and (product.sku != null) }"><c:out value="${product.sku}" escapeXml="false"/></c:when>
        <c:otherwise>&nbsp;</c:otherwise>
      </c:choose>
     </td>
    </tr>
   </table> 
  </td>
  <c:if test="${ status.index % numCul == numCul - 1 }">
    </tr>
  </c:if>

<c:if test="${status.last}" >
</table>
</c:if>
