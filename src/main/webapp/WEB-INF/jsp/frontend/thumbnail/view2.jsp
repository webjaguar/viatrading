<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>

<table border="0" class="thumbnail_item">
  <tr>
  <td class="thumbnail">
  <div align="center">
    <c:if test="${product.thumbnail == null}">&nbsp;</c:if>
    <c:if test="${product.thumbnail != null}">
  	  <a href="${productLink}">
  	  <c:choose>
  	   <c:when test="${product.asiId != null}">
  	   <img src="<c:out value="${fn:replace(product.thumbnail.imageUrl, 'prodimgs', 'prodthumbs')}"/>" border="0" class="thumbnail_image" alt="<c:out value="${product.name}"/><c:if test="${product.shortDesc != ''}"> - <c:out value="${product.shortDesc}"/></c:if>">
  	   </c:when>
  	   <c:otherwise>
  	   <img src="<c:if test="${!product.thumbnail.absolute}">${_contextpath}/assets/Image/Product/thumb/</c:if><c:out value="${fn:replace(product.thumbnail.imageUrl, 'http://www.dollaritem.com/wms/images/catalog/', 'http://www.dollaritem.com/wms/images/thumbs/')}"/>" border="0" class="thumbnail_image" alt="<c:out value="${product.name}"/><c:if test="${product.shortDesc != ''}"> - <c:out value="${product.shortDesc}"/></c:if>">
  	   </c:otherwise>
  	  </c:choose>
  	  </a>
    </c:if>
  </div>
  <c:if test="${(gSiteConfig['gPRODUCT_REVIEW'] or siteConfig['PRODUCT_RATE'].value == 'true' ) and product.enableRate and siteConfig['PRODUCT_REVIEW_THIRDPARTY'].value == 'false'}">
  <div class="quickProductReview" align="center">
    <a href="${productLink}#reviews"><jsp:include page="/WEB-INF/jsp/frontend/common/productRateImage.jsp"><jsp:param name="productRateAverage" value="${product.rate.average}" /></jsp:include></a>
  </div>
  </c:if>
  <c:if test="${(gSiteConfig['gPRODUCT_REVIEW'] or siteConfig['PRODUCT_RATE'].value == 'true' ) and product.enableRate and siteConfig['PRODUCT_REVIEW_THIRDPARTY'].value == 'true'}">
  <div class="dynamic_quickProductReview_tp">
    <%@ include file="/WEB-INF/jsp/frontend/common/quickProductReviewFreeCode.jsp" %>
  </div>
  </c:if>
  </td>
  <td class="thumbnail_content">
  
<c:if test="${siteConfig['SHOW_SKU'].value == 'true'}">
<c:if test="${product.sku != null}">
<div class="thumbnail_item_sku"><c:out value="${product.sku}" /></div>  
</c:if>
</c:if>  

  <div class="thumbnail_item_name"><a href="${productLink}" 
	  			class="thumbnail_item_name"><c:out value="${product.name}" escapeXml="false" /></a></div>
  <c:if test="${product.shortDesc != null}">
  <div class="thumbnail_item_desc">
    <c:out value="${product.shortDesc}" escapeXml="false" />
  </div>  
  </c:if>
	<c:choose>
	<c:when test="${product.salesTag != null and !product.salesTag.percent}">
	  <%@ include file="/WEB-INF/jsp/frontend/common/price3.jsp" %>
	</c:when>
	<c:otherwise>
	  <%@ include file="/WEB-INF/jsp/frontend/common/price.jsp" %>
	</c:otherwise>
	</c:choose>
  </td>
   <td>
   <c:if test="${model.alsoConsiderMap[product.id] != null}">
   <c:set var="product" value="${model.alsoConsiderMap[product.id]}" />
   <%@ include file="/WEB-INF/jsp/frontend/common/alsoconsider.jsp" %>
   </c:if>&nbsp;
   </td>
  </tr>
</table>
