<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>

<c:if test="${status.first}">
<script type="text/javascript" >
<!--
function checkThumbnailForm(id)
{
	var el = document.getElementById("quantity_" + id);
	if (el != null) {
		if (checkNumber(el.value) == 1) {
			return true;
		} else {
			// default to 1
			el.value = 1;
			return true;
		}			
	}
	return false;
}
//-->
</script>
</c:if>

<table border="0" class="thumbnail_item">
  <tr>
  <td class="thumbnail">
  <div align="center">
    <c:if test="${product.thumbnail == null}">&nbsp;</c:if>
    <c:if test="${product.thumbnail != null}">
  	  <a href="${productLink}">
  	  <c:choose>
  	   <c:when test="${product.asiId != null}">
  	   <img src="<c:out value="${fn:replace(product.thumbnail.imageUrl, 'prodimgs', 'prodthumbs')}"/>" border="0" class="thumbnail_image" alt="<c:out value="${product.name}"/><c:if test="${product.shortDesc != ''}"> - <c:out value="${product.shortDesc}"/></c:if>">
  	   </c:when>
  	   <c:otherwise>
  	   <img src="<c:if test="${!product.thumbnail.absolute}">${_contextpath}/assets/Image/Product/thumb/</c:if><c:out value="${fn:replace(product.thumbnail.imageUrl, 'http://www.dollaritem.com/wms/images/catalog/', 'http://www.dollaritem.com/wms/images/thumbs/')}"/>" border="0" class="thumbnail_image" alt="<c:out value="${product.name}"/><c:if test="${product.shortDesc != ''}"> - <c:out value="${product.shortDesc}"/></c:if>">
  	   </c:otherwise>
  	  </c:choose>
  	  </a>
    </c:if>
  </div>
  <c:if test="${(gSiteConfig['gPRODUCT_REVIEW'] or siteConfig['PRODUCT_RATE'].value == 'true' ) and product.enableRate and siteConfig['PRODUCT_REVIEW_THIRDPARTY'].value == 'false'}">
  <div class="quickProductReview" align="center">
    <a href="${productLink}#reviews"><jsp:include page="/WEB-INF/jsp/frontend/common/productRateImage.jsp"><jsp:param name="productRateAverage" value="${product.rate.average}" /></jsp:include></a>
  </div>
  </c:if>
  <c:if test="${(gSiteConfig['gPRODUCT_REVIEW'] or siteConfig['PRODUCT_RATE'].value == 'true' ) and product.enableRate and siteConfig['PRODUCT_REVIEW_THIRDPARTY'].value == 'true'}">
  <div class="dynamic_quickProductReview_tp">
    <%@ include file="/WEB-INF/jsp/frontend/common/quickProductReviewFreeCode.jsp" %>
  </div>
  </c:if>
  </td>
  <td class="thumbnail_content">
  
<c:if test="${siteConfig['SHOW_SKU'].value == 'true'}">
<c:if test="${product.sku != null}">
<div class="thumbnail_item_sku"><c:out value="${product.sku}" /></div>  
</c:if>
</c:if>  
  
  <div class="thumbnail_item_name"><a href="${productLink}" 
	  			class="thumbnail_item_name"><c:out value="${product.name}" escapeXml="false" /></a></div>
  <c:if test="${product.shortDesc != null}">
  <div class="thumbnail_item_desc">
    <c:out value="${product.shortDesc}" escapeXml="false" />
  </div>  
  </c:if>
  <c:if test="${gSiteConfig['gDEALS'] > 0 and product.deal.htmlCode != null}">
  	<c:out value="${product.deal.htmlCode}" escapeXml="false" /> 
  </c:if>
  <c:if test="${gSiteConfig['gDEALS'] > 0 and product.deal.parentHtmlCode != null}">
	<c:out value="${product.deal.parentHtmlCode}" escapeXml="false"/>
</c:if>
  <%-- start etilize --%>
  <c:if test="${model.productSummaryMap[status.index] != null}">
  <div class="thumbnail_skus">
  <c:forEach items="${model.productSummaryMap[status.index].skus.sku}" var="sku" varStatus="skuStatus">
  	  <c:out value="${sku.type}" escapeXml="false"/>: <c:out value="${sku.number}" escapeXml="false"/><c:if test="${not skuStatus.last}">, </c:if>
  </c:forEach>
  </div>
  <div class="thumbnail_manufacturer">
    Mfg Number: <c:out value="${model.productSummaryMap[status.index].manufacturer.number}" escapeXml="false" />
  </div>
  <div class="thumbnail_etilize">
    Etilize ID: <c:out value="${model.productSummaryMap[status.index].id}" escapeXml="false" />
  </div>
  </c:if>
  <%-- end etilize --%>
  <form action="${_contextpath}/addToCart.jhtm" method="post" onsubmit="return checkThumbnailForm(${product.id})">
  <input type="hidden" name="product.id" value="${product.id}">
  <c:choose>
	<c:when test="${product.salesTag != null and !product.salesTag.percent}">
	  <%@ include file="/WEB-INF/jsp/frontend/common/price3.jsp" %>
	</c:when>
	<c:otherwise>
	  <%@ include file="/WEB-INF/jsp/frontend/common/price.jsp" %>
	</c:otherwise>
  </c:choose>
  <c:if test="${product.type != 'box'}">
  	<%@ include file="/WEB-INF/jsp/frontend/common/productOptions.jsp" %>
  </c:if>



  <c:if test="${ (gSiteConfig['gSHOPPING_CART'] and (!empty product.price or product.priceByCustomer)) and (!product.loginRequire or userSession != null)}">  
	<c:set var="zeroPrice" value="false"/>
	<c:forEach items="${product.price}" var="price">
	  <c:if test="${price.amt == 0}">
	    <c:set var="zeroPrice" value="true"/>
	  </c:if>
	</c:forEach> 
	<table class="addToCartBox">
	 <tr valign="middle" class="addToCart">
	  <c:choose>
	  <c:when test="${gSiteConfig['gVARIABLE_PRICE'] and product.priceByCustomer}">  
        <input type="checkbox" name="quantity_${product.id}" id="quantity_${product.id}" value="1" />
        <td>	    
		<input type="image" value="Add to Cart" name="_addtocart_thumb" class="_addtocart_thumb" src="${_contextpath}/assets/Image/Layout/button_addtocart${_lang}.gif" >
	   </td>
      </c:when>
	  <c:when test="${product.type == 'box'}">
	   <td>	    
		<a href="${productLink}"  ><img border="0" src="${_contextpath}/assets/Image/Layout/button_buildthebox.gif" /></a>
	   </td>
	  </c:when>
	  <c:when test="${!empty product.numCustomLines}">
	   <td>	   
        <a href="${productLink}"  ><img border="0" src="${_contextpath}/assets/Image/Layout/button_continue.gif" /></a>
       </td>
      </c:when>
	  <c:otherwise>
	   <c:choose>
	     <c:when test="${gSiteConfig['gINVENTORY'] and product.inventory != null and !product.negInventory and product.inventory <= 0}">
	       <td><c:out value="${siteConfig['OUT_OF_STOCK_TITLE'].value}" escapeXml="false"/></td> 
	     </c:when>   
	     <c:when test="${zeroPrice and siteConfig['ZERO_PRICE_HTML'].value != ''}">
	       <td><c:out value="${siteConfig['ZERO_PRICE_HTML'].value}" escapeXml="false"/></td>
	     </c:when>
	     <c:otherwise>    
	       <td style="white-space: nowrap" class="quickmode qty">
	        <fmt:message key="qty" />:&nbsp;
	           <c:choose>
	             <c:when test="${siteConfig['PRODUCT_QTY_DROPDOWN'].value != ''}">
				       <c:set var="startEmpty" value="true" />
	               <%@ include file="/WEB-INF/jsp/frontend/common/qtyDropDown.jsp" %>
	             </c:when>
	             <c:otherwise>
	               <input type="text" size="6" maxlength="5" name="quantity_${product.id}" id="quantity_${product.id}" value="" >
	             </c:otherwise>
	           </c:choose>    
	       </td>
	       <td>	    
		    <input type="image" value="Add to Cart" name="_addtocart_thumb" class="_addtocart_thumb" src="${_contextpath}/assets/Image/Layout/button_addtocart${_lang}.gif" >
	       </td>
	       <c:if test="${gSiteConfig['gINVENTORY'] and product.inventory != null and product.inventory < product.lowInventory}">
	         <td><c:out value="${siteConfig['LOW_STOCK_TITLE'].value}" escapeXml="false"/></td> 
	       </c:if>
	     </c:otherwise>
	   </c:choose> 
	  </c:otherwise>
	  </c:choose>
	 </tr>
	 <c:if test="${gSiteConfig['gINVENTORY'] and product.inventory != null and !empty siteConfig['INVENTORY_ON_HAND'].value}" >
	 <tr class="inventory_onhand_wrapper">
	   <td><div class="inventory_onhand"><c:out value="${siteConfig['INVENTORY_ON_HAND'].value}" escapeXml="false"/>&nbsp;<span><c:out value="${product.inventory}" /></span></div></td>     
	 </tr>
	 </c:if>
	</table>
  </c:if>
  </form>
  <c:if test="${gSiteConfig['gMYLIST'] && product.addToList && (siteConfig['MYLIST_THUMBNAIL'].value == 'true')}">
  <form action="${_contextpath}/addToList.jhtm">
  <input type="hidden" name="product.id" value="${product.id}">
  <input type="image" border="0" src="${_contextpath}/assets/Image/Layout/button_addtolist${_lang}.gif">
  </form>
  </c:if>
  </td>
   <td>
   <c:if test="${model.alsoConsiderMap[product.id] != null && (siteConfig['ALSO_CONSIDER_DETAILS_ONLY'].value == 'false')}">
   <c:set var="product" value="${model.alsoConsiderMap[product.id]}" />
   <%@ include file="/WEB-INF/jsp/frontend/common/alsoconsider.jsp" %>
   </c:if>&nbsp;
   </td>
  </tr>
</table>
