<link rel="stylesheet" href="assets/asi_product.css" type="text/css" media="screen" />
<div id="mb${model.optionValueMap['initialDependencyId']}">
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>

<script type="text/javascript" src="javascript/mootools-1.2.5-core.js"></script>
<script src="javascript/mootools-1.2.5.1-more.js" type="text/javascript"></script>
<script type="text/javascript" src="javascript/multibox.js"></script>

<script type="text/javascript">
function setAsiOption(el, criteriaId){
		$(criteriaId).value = el.value;
		var element = $(el).getParent('td').getParent('tr');
		var cIds = document.getElementsByName("asi_criteria_id");	
		var urlappend = '';	
		for (i = 0; i < cIds.length; i++) {
			if($(cIds[i].value).value != null &&  $(cIds[i].value).value != ''){
				urlappend = urlappend + '&criteriaId='+cIds[i].value+'&value='+encodeURIComponent($(cIds[i].value).value);
			}
		}
		var productId = $('product.id').value;
		var quantity = $('quantity_'+productId).value;
		var priceGridId = $('priceGridId').value;
		var initialDependencyId = $('initialDependencyId').value;
		var initialDependencyValue = $('initialDependencyValue').value;
		var requestHTMLData = new Request.HTML ({
			url: "showAsiOptions.jhtm?id="+productId+"&quantity="+quantity+"&priceGridId="+priceGridId+"&initialDependencyId="+initialDependencyId+"&initialDependencyValue="+initialDependencyValue+urlappend,
			onComplete: function(response){
			 	var scroll = new Fx.Scroll($('scroll'), { wait: false });
	        	scroll.toElement(element.id);
	        	$(element.id).highlight('#ddf', '#ccc');
	        },
	      	update: $('mb'+initialDependencyId)
		}).send();
}
window.addEvent('domready', function(){ 
	$$('select.optionSelect').each(function(select){
		select.addEvent('change',function(){
			var element = select.getParent('td').getParent('tr');
			var cIds = document.getElementsByName("asi_criteria_id");	
			var urlappend = '';	
			for (i = 0; i < cIds.length; i++) {
				if($(cIds[i].value).value != null &&  $(cIds[i].value).value != ''){
					urlappend = urlappend + '&criteriaId='+cIds[i].value+'&value='+encodeURIComponent($(cIds[i].value).value);
				}
			}
			var productId = $('product.id').value;
			var quantity = $('quantity_'+productId).value;
			var priceGridId = $('priceGridId').value;
			var initialDependencyId = $('initialDependencyId').value;
			var initialDependencyValue = $('initialDependencyValue').value;
			var requestHTMLData = new Request.HTML ({
				url: "showAsiOptions.jhtm?id="+productId+"&quantity="+quantity+"&priceGridId="+priceGridId+"&initialDependencyId="+initialDependencyId+"&initialDependencyValue="+initialDependencyValue+urlappend,
				onComplete: function(response){
					var scroll = new Fx.Scroll($('scroll'), { wait: false });
		        	scroll.toElement(element.id);
		        	$(element.id).highlight('#ddf', '#ccc');
			    },
		      	update: $('mb'+initialDependencyId)
			}).send();
		});
	});
});

function checkForm() {
	parent.quickViewBox.close();
	setTimeout(function() {parent.location = "${_contextpath}/viewCart.jhtm";},250);
}
</script>

	<c:set value="${model.optionValueMap['initialDependencyId']}" var="initialId"></c:set>
	<input type="hidden" id="initialDependencyId" value="${initialId}">
	<input type="hidden" id="initialDependencyValue" value="${fn:escapeXml(model.optionValueMap[initialId])}">
	<div id="multiBoxASIHeader"></div>
	<div class="multiBoxASIMessage" id="mb${model.optionValueMap[initialId]}"><fmt:message key="selectASIOptions"></fmt:message></div>
	<div class="hrLine"></div>
	<form action="addASIToCart.jhtm" name="this_form${model.optionValueMap[initialId]}" method="get" onsubmit="return checkForm()">
		<div class="scroll" id="scroll">
		<input type="hidden" name="additionalCharge" value="${model.additionalCharge}">
		<input type="hidden" name="product.id" id="product.id" value="${model.product.id}">
		<table class="mbASIOptions" cellpadding="0" cellspacing="0" border="0">
		<tr class="mbASIOptions">
			<td class="optionTitle"><fmt:message key="quantity"></fmt:message></td>
			<td class="optionValues"><input type="text" maxlength="5" name="quantity_${model.product.id}" id="quantity_${model.product.id}" value="<c:out value='${model.quantity}' />">
			  <span id="unitPrice">@ $<c:out value="${model.unitPrice}"></c:out></span>
			</td>
		</tr>
		<c:forEach items="${model.asiProductOption}" var="productOption" varStatus="poSatus">
			<input type="hidden" name="asi_option" value="<c:out value="${productOption.name}"/>">
			<input type="hidden" name="asi_criteria_id" value="<c:out value="${productOption.asiCriteriaSetId}"/>">
			<c:if test="${model.priceDependecyMap[productOption.asiCriteriaSetId] != null}">
			  <input type="hidden" name="asi_option_price_${productOption.asiCriteriaSetId}" value="${model.priceDependecyMap[productOption.asiCriteriaSetId]}"/>
			</c:if>
			<tr class="mbASIOptions ${productOption.name}" id="highlight${poSatus.index}">
				<td class="optionName">
					<div class="optionTitle"><c:out value="${productOption.name}"/>: </div> 
					<c:if test="${productOption.asiDescription != null}">
					<div class="optionDescription"><c:out value="${productOption.asiDescription}"/></div>
					</c:if>
				</td>
				<td class="optionValues">
				 <c:choose>
				    <c:when test="${fn:contains(model.defaultDropDownList, productOption.name) or (!fn:contains(model.defaultDropDownList, productOption.name) and fn:length(productOption.values) > 1 )}">
				      <select style="width: 157;" name="asi_option_values_${productOption.asiCriteriaSetId}" class="optionSelect" id="${productOption.asiCriteriaSetId}">
			  		    <c:if test="${fn:length(productOption.values) > 1 or !fn:contains(model.defaultSelectionList, productOption.name)}">
						  <option value="">Select</option>
			  		    </c:if>
					    <c:forEach items="${productOption.values}" var="prodOptValue" varStatus="povStatus">
			     		  <option value="${fn:escapeXml(prodOptValue.name)}" <c:if test="${model.optionValueMap[productOption.asiCriteriaSetId] == prodOptValue.name}">selected="selected"</c:if>><c:out value="${prodOptValue.name}"/></option>
			  		    </c:forEach>
					  </select>
				    </c:when>
				    <c:otherwise>
				        <input type="radio"  value="" id="${poSatus.index}" onclick="setAsiOption(this, '${productOption.asiCriteriaSetId }');" <c:if test="${model.optionValueMap[productOption.asiCriteriaSetId] == prodOptValue.name}">checked="checked"</c:if>></input> None<br>
						<c:forEach items="${productOption.values}" var="prodOptValue" varStatus="povStatus">
			     	  	  <input  type="radio" value="${fn:escapeXml(prodOptValue.name)}" id="${poSatus.index}" onclick="setAsiOption(this,'${productOption.asiCriteriaSetId}');" <c:if test="${model.optionValueMap[productOption.asiCriteriaSetId] == prodOptValue.name}">checked="checked"</c:if> ></input> <c:out value="${prodOptValue.name}"/>
				        </c:forEach>
				        <input type="hidden" value="${fn:escapeXml(model.optionValueMap[productOption.asiCriteriaSetId])}" name="asi_option_values_${productOption.asiCriteriaSetId}" id="${productOption.asiCriteriaSetId}">
					</c:otherwise>
				  </c:choose>
				  <c:if test="${model.priceDependecyMap[productOption.asiCriteriaSetId] != null}">@   <c:if test="${model.priceDependecyMap[productOption.asiCriteriaSetId] != 'QUR'}"> $</c:if><c:out value="${model.priceDependecyMap[productOption.asiCriteriaSetId]}"></c:out></c:if>
				</td>
			</tr>
		</c:forEach>
		</table>
		<input type="hidden" name="optionPrice_${model.product.id}" value="<c:out value='${model.optionValueMap[initialId]}' />">
		<input type="hidden" name="priceGridId" id="priceGridId" value="${model.optionValueMap['priceGridId']}">
		</div>
		
		<!-- Pass Initial Dependency Value as hidden option  -->
		<c:if test="${model.asiInitialDependencyOption != null}">
		  <input type="hidden" name="asi_option" value="<c:out value="${model.asiInitialDependencyOption.name}"/>">
		  <input type="hidden" name="asi_criteria_id" value="<c:out value="${model.asiInitialDependencyOption.asiCriteriaSetId}"/>">
		  <input type="hidden" name="asi_option_values_${model.asiInitialDependencyOption.asiCriteriaSetId}" id="${model.asiInitialDependencyOption.asiCriteriaSetId}" value="<c:out value="${fn:escapeXml(model.optionValueMap[initialId])}"/>">
		</c:if>
		
		<div class="hrLine"></div>
		<input type="image" border="0" id="_addtocart" name="_addtocart" class="mbAddToCart" src="${_contextpath}/assets/Image/Layout/button_addtocart${_lang}.gif" />
    </form>
</div>