<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>

 <link href="assets/stylesheet_asi.css" rel="stylesheet" type="text/css"> 
  
<script type="text/javascript">
<!--
function submitSearch(filter, value, searchTerm){
	$('asiFilter'+filter).value = value;
	$('asiFilterH'+filter).value = searchTerm;
	$('asiSearch').submit();
}
function removeFilter(filter){
	$('asiFilter'+filter).value = null;
	$('asiFilterH'+filter).value = null;
	$('asiSearch').submit();
}
function showMore(show, filter) {
	$$('div.filterIValue'+filter).each(function(ele){
		if(show){
			ele.setStyle('display','block');
		} else {
			ele.setStyle('display','none');
		}
	});
	if(show){
		$('showMoreLink').set('html','<a onclick="showMore(false,'+filter+');">Show Less..</a> ');
	} else {
		$('showMoreLink').set('html','<a onclick="showMore(true,'+filter+');">Show More..</a> ');
	}
}
//-->
</script>
<td valign="top" class="leftbar">
<form action="${_contextpath}/asiProducts.jhtm" method="post" id="asiSearch">
<div class="asiSearchWrapper">
  <div class="asiSearchBox">
      <div class="asiSearchName"><fmt:message key="keywords" />:</div>
      <div class="asiSearchInput"><input type="text" name="q" value="<c:out value="${model.q}"/>" maxlength="100"></div>
      <div class="asiSubmitSearch"><input type="submit" value="Go" class="f_button" /></div>
      <div class="clear"></div>
       
       <input type="hidden" name="q" value="<c:out value="${model.q}"/>" maxlength="10">
      <c:if test="${model.color != null or model.size != null or model.shape != null or model.material != null or model.category != null }">
      <div class="asiSearchSelectionWrapper">
        <div class="filterTitle">Your Selection</div>
        <c:if test="${model.category != null}">
          <div class="asiSearchSelectionTitle">Category</div>
          <span style="float: left;"> <input type="checkbox" checked="checked" onclick="removeFilter('category');"></input> </span>
          <span class="asiSearchSelection">${model.categoryH}</span>
          <div style="clear:both;"></div>
        </c:if>
        <c:if test="${model.color != null}">
          <div class="asiSearchSelectionTitle">Color</div>
          <span style="float: left;"> <input type="checkbox" checked="checked" onclick="removeFilter('color');"></input> </span>
          <span class="asiSearchSelection">${model.colorH}</span>
          <div style="clear:both;"></div>
        </c:if>
        <c:if test="${model.size != null}">
          <div class="asiSearchSelectionTitle">Size</div>
          <span style="float: left;"> <input type="checkbox" checked="checked" onclick="removeFilter('size');"></input> </span>
 	      <span class="asiSearchSelection">${model.sizeH}</span>
          <div style="clear:both;"></div>
 	    </c:if>
        <c:if test="${model.shape != null}">
          <div class="asiSearchSelectionTitle">Shape</div>
          <span style="float: left;"> <input type="checkbox" checked="checked" onclick="removeFilter('shape');"></input> </span>
          <span class="asiSearchSelection">${model.shapeH}</span>
          <div style="clear:both;"></div>
        </c:if>
        <c:if test="${model.material != null}">
          <div class="asiSearchSelectionTitle">Material</div>
          <span style="float: left;"> <input type="checkbox" checked="checked" onclick="removeFilter('material');"></input> </span>
          <span class="asiSearchSelection">${model.materialH}</span>
          <div style="clear:both;"></div>
        </c:if>
       </div>
       </c:if>
       <div id="asiFilterSelWrapper" style="display: none;">
         <input type="hidden" name="color" id="asiFiltercolor" value="${fn:escapeXml(model.color)}"></input>
	     <input type="hidden" name="colorH" id="asiFilterHcolor" value="${fn:escapeXml(model.colorH)}"></input>
	  
	     <input type="hidden" name="size" id="asiFiltersize" value="${fn:escapeXml(model.size)}"></input>
	     <input type="hidden" name="sizeH" id="asiFilterHsize" value="${fn:escapeXml(model.sizeH)}"></input>
	  
	     <input type="hidden" name="shape" id="asiFiltershape" value="${fn:escapeXml(model.shape)}"></input>
	     <input type="hidden" name="shapeH" id="asiFilterHshape" value="${fn:escapeXml(model.shapeH)}"></input>
	  
	     <input type="hidden" name="material" id="asiFiltermaterial" value="${fn:escapeXml(model.material)}"></input>
	     <input type="hidden" name="materialH" id="asiFilterHmaterial" value="${fn:escapeXml(model.materialH)}"></input>

	     <input type="hidden" name="category" id="asiFiltercategory" value="${fn:escapeXml(model.category)}"></input>
	     <input type="hidden" name="categoryH" id="asiFilterHcategory" value="${fn:escapeXml(model.categoryH)}"></input>
	   </div>
       <c:if test="${model.filters != null}">
        <div class="asiSearchFilters">
        
        <c:forEach items="${model.filters}" var="filter" varStatus="status">
        <div class="asiSearchFilter">
          <div class="filterTitle">${filter.key}</div>
          <div class="flilterValues">
            <c:forEach items="${filter.value}" var="value" varStatus="statusI">
              <div class="filterValue <c:if test="${statusI.index > 5}">filterIValue filterIValue${status.index}</c:if>">
                <a href="#" onclick="submitSearch('${filter.key}','${value.value}', '${fn:escapeXml(value.key)}');">
                  <c:out value="${fn:split(value.key, '(')[0]}" escapeXml="false"/>
                  <span class="filterValueCount">
                  (<c:out value="${fn:split(value.key, '(')[1]}" escapeXml="false"/>
                  </span>
                </a>
              </div>
            </c:forEach>
            <c:if test="${fn:length(filter.value) > 5}">
              <span id="showMoreLink"><a onclick="showMore(true, '${status.index}');">Show More..</a></span>
            </c:if>
          </div>
          <%--
          <select name="filter${status.index}" >
            <option value=""> <c:out value="${filter.key}"/></option>
            <c:forEach items="${filter.value}" var="value">
              <option value="${value.value}" onclick="submitSearch('${filter.key}',this.value, '${fn:escapeXml(value.key)}');"><c:out value="${value.key}"/></option>
            </c:forEach>
          </select>
           --%>
        </div>
        </c:forEach>
        
	  </div>
    </c:if>
 </div>
</div>
<div style="float: clear;"></div>
</form>
</td>