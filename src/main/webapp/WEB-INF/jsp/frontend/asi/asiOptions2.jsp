<link rel="stylesheet" href="assets/asi_product.css" type="text/css" media="screen" />
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>

<script language="JavaScript" type="text/JavaScript">
function allQtyBreak(show) {
	$$('div.asiConfigQBWrapper').each(function(ele){
		if(show){
			ele.setStyle('display','block');
		} else {
			ele.setStyle('display','none');
		}
	});
	if(show){
		$('showMoreLink').set('html','<a onclick="allQtyBreak(false);">Less..</a> ');
	} else {
		$('showMoreLink').set('html','<a onclick="allQtyBreak(true);">More..</a> ');
		$('asiConfigQBWrapper0').setStyle('display','block');
	}
}
function updateColor(color, scrollTo) {
	$('asi_option_color').value = color;
	$('asi_option_colorScrollTo').value = scrollTo;
	setAsiOption();
}
function updateQuantity(size, qty) {
	if(size != null) {
		$('asi_option_qty_'+size).value = qty;
	}
	setAsiOption();
}
function updateImprintColor(color, index) {
	//alert(index);
	$('asi_option_imColor'+index).value = color;
	setAsiOption();
}
function addImprintColor() {
	var total = 0;
	$$('div.asiConfigIMColorBox').each(function(ele){
		total = total + 1;
	});
    var newDiv = document.createElement("div");
	newDiv.className ="asiConfigIMColorBox";
	newDiv.id = "asiConfigImprint"+total;
	newDiv.innerHTML = "Color "+total;
	$('asiConfigSelectedImprintColors').appendChild(newDiv);

	var newDiv = document.createElement("div");
	newDiv.style.clear ="both";
	$('asiConfigSelectedImprintColors').appendChild(newDiv);
}
function assignText(txt, id){
	$(id).value = 'Imprint Text_value_'+txt;
}

function removeImprintColor(index, color, selected){
	//$('asiConfigSelectedImprintColors').removeChild($('asiIMColor'+index));
		var newInput = document.createElement("input");
	newInput.type = "hidden";
	newInput.value = "imColor"+index;
	newInput.className = "asi_option";
	
	var newInput2 = document.createElement("input");
	newInput2.type = "hidden";
	newInput2.value = color;
	newInput2.name = "asi_option_imColor"+index;
	newInput2.id = "asi_option_imColor"+index;
	if(selected) {
		$('asiConfigSelectedImprintColors').appendChild(newInput);
		$('asiConfigSelectedImprintColors').appendChild(newInput2);
	} else {
		alert(index);
		$('asiIMColor'+index).removeChild($('asiConfigHidden'+index));
	}
	
	setAsiOption();
}
</script>
<form action="addASIToCart.jhtm" name="this_form${model.optionValueMap[initialId]}" method="get" onsubmit="return checkForm()">
<div class="asiConfigWrapper">
  <c:set var="stepNum" value="1"/>
  <!-- Step 1 : Color Selection -->
  <c:if test="${model.productColors != null}">
  <div class="asiConfigColor">
    <div class="asiConfigTitle">${stepNum}. Choose Color</div>
    <div class="asiConfigValues">
      <c:set var="height" value="200px;"></c:set>
      <c:if test="${fn:length(model.productColors) < 7}">
       <c:set var="height" value="${(fn:length(model.productColors) * 30)+20}px;"></c:set>
      </c:if>
      <div class="asiConfigColorWrapper" id="asiConfigColorWrapperId" style="height: ${height}">
        <input type="hidden" name="asi_option" class="asi_option" value="color"/>
		<input type="hidden" name="asi_option_color" id = "asi_option_color" value="${model.selectedColor}"/>
		<input type="hidden" name="asi_option_colorScrollTo" id = "asi_option_colorScrollTo" value="${model.scrollToColor}"/>
		<c:forEach items="${model.productColors}" var="color" varStatus="status">
		  <div class="asiConfigColorBlock" id="asiConfigColorBlock${status.index}" <c:if test="${color.key == model.selectedColor}">style="border: 2px solid red;"</c:if>>
		    <div class="asiConfigColorBox" style="background-color: ${color.value};" onclick="updateColor('${color.key}','asiConfigColorBlock${status.index}');"></div>
            <div class="asiConfigColorName"> <c:out value="${color.key}"/> </div>
		  </div>
          <div style="clear: both;"></div>
        </c:forEach>
      </div>
      <c:if test="${model.selectedColor != null}">
		<c:forEach items="${model.productColors}" var="color" varStatus="status">
	      <c:if test="${color.key == model.selectedColor}">
	        <div class="asiConfigColorBox" style="background-color: ${color.value};margin-top:5px;">Selected</div>
            <div style="clear: both;"></div>
          </c:if>
        </c:forEach>
	  </c:if>
    </div>
    <c:if test="${model.selectedColor != null}">
      <input type="hidden" name="optionNVPair" value="Color_value_${model.selectedColor}"/>
    </c:if>
  </div>
  <div class="hrLine"></div>
  <c:set var="stepNum" value="${stepNum + 1}"/>
  </c:if>	
  <!-- Step 2 : Quantity / size Selection -->
  <c:if test="${model.selectedColor != null or model.productColors == null}">
  <div class="asiConfigSizeQty">
    <div class="asiConfigTitle">${stepNum}. Provide Quantity</div>
    <div class="asiConfigValues">
      <input type="hidden" name="asi_option" class="asi_option" value="quantity"/>
	  <input type="hidden" name="quantity_${model.product.id}" id="quantity_${model.product.id}" value="${model.quantity}"/>
	  <c:choose>
        
        
        <c:when test="${model.variantPriceQtyMap != null and fn:length(model.variantPriceQtyMap) > 0}">
          <c:forEach items="${model.variantQtyMap}" var="size">
            <input type="hidden" name="asi_option" class="asi_option" value="qty_${size.key}"/>
	 		<div class="asiConfigQtyWrapper">
              <div><c:out value="${size.key}"></c:out> </div>  
              <div><input type="text"  value="${size.value}" name="asi_option_qty_${size.key}" id="asi_option_qty_${size.key}" maxlength="5" size="5" class="asiConfigInput" onchange="updateQuantity('${size.key}',this.value);"/></div>  
            </div>
            <c:if test="${size.value > 0}">
       		  <input type="hidden" name="optionNVPair" value="size(${size.key})_value_${size.value}"/>
 		    </c:if>
          </c:forEach>
		  <div style="clear:both;"></div>
          <c:forEach items="${model.variantPriceQtyMap}" var="variant" varStatus="status">
            <div class="asiQBWrapper" align="center" id="asiQBWrapperId${status.index}">
              <c:set var="quantity" value="Quantity"></c:set> 
  		      <c:set var="listPrice" value="Price"></c:set>
  		      <c:set var="startBreak" value="${model.minOrderQty}"/>
    	      <c:if test="${status.index == 0 and fn:length(model.variantPriceQtyMap) > 1}">
                <div id="showMoreLink">
                  <a onclick="allQtyBreak(true);">More..</a> 
                </div>
              </c:if>
        	  
        	  <div class="asiConfigQBWrapper" id="asiConfigQBWrapper${status.index}" align="center" <c:if test="${status.index > 0}">style="display: none;" </c:if>>
              <div class="asiConfigQBHeading"> <c:out value="${variant.key}"></c:out></div>
              
              <div class="asiConfigQBTitleWrapper">
                <div class="asiConfigQBTitle">Quantity</div>
    		    <div class="asiConfigQBTitle">Regular Price</div>
    		    <div class="asiConfigQBTitle">Sale Price</div>
    		  </div>
    		  <c:set var="startBreak" value="${model.minOrderQty}"/>
    		  <c:forEach items="${variant.value}" var="break" varStatus="status">
                <div class="asiConfigQBValueWrapper">
                  <c:choose>
                    <c:when test="${fn:length(variant.value) > status.index + 1}">
                      <div class="asiConfigQBValue">${startBreak} -  ${break.key}</div>
    		        </c:when>
                    <c:otherwise>
                      <div class="asiConfigQBValue"> ${startBreak}+</div>
    		        </c:otherwise>
                  </c:choose>
                  <div class="asiConfigQBValue">
                    <c:choose>
                      <c:when test="${model.product.salesTag != null}">
                        <c:choose>
                          <c:when test="${model.product.salesTag.percent}">
                    		$<fmt:formatNumber value="${break.value *( 100 / 100 - model.product.salesTag.discount)}" pattern="#,##0.00#" maxFractionDigits="${siteConfig['MAX_FRACTION_DIGIT'].value}" /> 
    		              </c:when>
                          <c:otherwise>
                  			$<fmt:formatNumber value="${break.value + model.product.salesTag.discount}" pattern="#,##0.00#" maxFractionDigits="${siteConfig['MAX_FRACTION_DIGIT'].value}" /> 
    		              </c:otherwise>
                        </c:choose>
                      </c:when>
                      <c:otherwise>
                  		$<fmt:formatNumber value="${break.value}" pattern="#,##0.00#" maxFractionDigits="${siteConfig['MAX_FRACTION_DIGIT'].value}" /> 
    		          </c:otherwise>
                    </c:choose>
                  </div>
                  <div class="asiConfigQBValue asiConfigDiscount">$<fmt:formatNumber value="${break.value}" pattern="#,##0.00#" maxFractionDigits="${siteConfig['MAX_FRACTION_DIGIT'].value}" /></div>
    		      <c:set var="startBreak" value="${break.key + 1}"/>
    		    </div>
    		  </c:forEach>
    		  <div style="clear: both;"></div> 
    		</div>
    		</div>
          </c:forEach>
	    </c:when>
	    <c:when test="${model.variantQtyMap != null and fn:length(model.variantQtyMap) > 0}">
          <c:forEach items="${model.variantQtyMap}" var="size">
            <input type="hidden" name="asi_option" class="asi_option" value="qty_${fn:escapeXml(size.key)}"/>
	 		<div class="asiConfigQtyWrapper">
              <div><c:out value="${size.key}"></c:out> </div>  
              <div><input type="text"  value="${size.value}" name="asi_option_qty_${fn:escapeXml(size.key)}" id="asi_option_qty_${fn:escapeXml(size.key)}" maxlength="5" size="5" class="asiConfigInput" onchange="updateQuantity('${fn:escapeXml(size.key)}',this.value);"/></div>  
            </div>
            <c:if test="${size.value > 0}">
       		  <input type="hidden" name="optionNVPair" value="size(${fn:escapeXml(size.key)})_value_${size.value}"/>
 		    </c:if>
            
          </c:forEach>
          <div style="clear: both;"></div>
          <c:forEach items="${model.variantQtyMap}" var="size" varStatus="status">
            <c:if test="${status.index == 0 and fn:length(model.variantQtyMap) > 1}">
              <div id="showMoreLink">
                <a onclick="allQtyBreak(true);">More..</a> 
              </div>
            </c:if>
            <div class="asiConfigQBWrapper" id="asiConfigQBWrapper${status.index}" align="center" <c:if test="${status.index > 0}">style="display: none;" </c:if>>
              <div class="asiConfigQBHeading">  <c:out value="${size.key}"></c:out></div>
              
              <div class="asiConfigQBTitleWrapper">
                <div class="asiConfigQBTitle">Quantity</div>
    		    <div class="asiConfigQBTitle">Regular Price</div>
    		    <div class="asiConfigQBTitle">Sale Price</div>
    		  </div>
    		  <c:set var="startBreak" value="${model.minOrderQty}"/>
    		  <c:forEach items="${model.priceQtyMap}" var="break" varStatus="status">
                <div class="asiConfigQBValueWrapper">
                  <c:choose>
                    <c:when test="${fn:length(model.priceQtyMap) > status.index + 1}">
                      <div class="asiConfigQBValue">${startBreak} -  ${break.key}</div>
    		        </c:when>
                    <c:otherwise>
                      <div class="asiConfigQBValue"> ${startBreak}+</div>
    		        </c:otherwise>
                  </c:choose>
                  <div class="asiConfigQBValue">
                    <c:choose>
                      <c:when test="${model.product.salesTag != null}">
                        <c:choose>
                          <c:when test="${model.product.salesTag.percent}">
                    		$<fmt:formatNumber value="${break.value *( 100 / 100 - model.product.salesTag.discount)}" pattern="#,##0.00#" maxFractionDigits="${siteConfig['MAX_FRACTION_DIGIT'].value}" /> 
    		              </c:when>
                          <c:otherwise>
                  			$<fmt:formatNumber value="${break.value + model.product.salesTag.discount}" pattern="#,##0.00#" maxFractionDigits="${siteConfig['MAX_FRACTION_DIGIT'].value}" /> 
    		              </c:otherwise>
                        </c:choose>
                      </c:when>
                      <c:otherwise>
                  		$<fmt:formatNumber value="${break.value}" pattern="#,##0.00#" maxFractionDigits="${siteConfig['MAX_FRACTION_DIGIT'].value}" /> 
    		          </c:otherwise>
                    </c:choose>
                  </div>
                  <div class="asiConfigQBValue asiConfigDiscount">$<fmt:formatNumber value="${break.value}" pattern="#,##0.00#" maxFractionDigits="${siteConfig['MAX_FRACTION_DIGIT'].value}" /></div>
    		      <c:set var="startBreak" value="${break.key + 1}"/>
    		    </div>
    		  </c:forEach>
    		  <div style="clear: both;"></div> 
    		</div>
    	  </c:forEach>
        </c:when>
        <c:otherwise>
          <div align="center" style="margin: 10px 0 10px 0;"><fmt:message key="quantity"/>: <input type="text"  value="${model.quantity}" id="quantity" name="quantity" maxlength="5" size="5" class="asiConfigInput" onchange="updateQuantity(null,this.value);"/> </div>
          <div style="clear: both;"></div> 
          <div class="asiConfigQBWrapper" id="asiConfigQBWrapper0" align="center">
          <div class="asiConfigQBTitleWrapper">
             <div class="asiConfigQBTitle">Quantity</div>
    		 <div class="asiConfigQBTitle">Regular Price</div>
    		 <div class="asiConfigQBTitle">Sale Price</div>
    	  </div>
    	  <c:set var="startBreak" value="${model.minOrderQty}"/>
    		<c:forEach items="${model.priceQtyMap}" var="break" varStatus="status">
              <div class="asiConfigQBValueWrapper">
                <c:choose>
                  <c:when test="${fn:length(model.priceQtyMap) > status.index + 1}">
                    <div class="asiConfigQBValue">${startBreak} -  ${break.key}</div>
    		      </c:when>
                  <c:otherwise>
                    <div class="asiConfigQBValue"> ${startBreak}+</div>
    		      </c:otherwise>
                </c:choose>
                <div class="asiConfigQBValue">
                <c:choose>
                   <c:when test="${model.product.salesTag != null}">
                     <c:choose>
                       <c:when test="${model.product.salesTag.percent}">
                         $<fmt:formatNumber value="${break.value *( 100 / (100 - model.product.salesTag.discount))}" pattern="#,##0.00#" maxFractionDigits="${siteConfig['MAX_FRACTION_DIGIT'].value}" /> 
    		           </c:when>
                       <c:otherwise>
                  		 $<fmt:formatNumber value="${break.value + model.product.salesTag.discount}" pattern="#,##0.00#" maxFractionDigits="${siteConfig['MAX_FRACTION_DIGIT'].value}" /> 
    		           </c:otherwise>
                     </c:choose>
                     </c:when>
                      <c:otherwise>
                  		$<fmt:formatNumber value="${break.value}" pattern="#,##0.00#" maxFractionDigits="${siteConfig['MAX_FRACTION_DIGIT'].value}" /> 
    		          </c:otherwise>
                    </c:choose>
                </div>
    		    <div class="asiConfigQBValue asiConfigDiscount">$<fmt:formatNumber value="${break.value}" pattern="#,##0.00#" maxFractionDigits="${siteConfig['MAX_FRACTION_DIGIT'].value}" /></div>
    		    <c:set var="startBreak" value="${break.key + 1}"/>
    		  </div>
    		</c:forEach>
    		<div style="clear: both;"></div> 
          </div>
        
        </c:otherwise>
      </c:choose>
      <div style="clear: both;"></div>
      
      <c:if test="${model.unitPrice != null}">
      <input type="hidden" name="asiUnitPrice" value="${model.unitPrice}"/>
      <div class="asiConfigPriceWrapper">
        <div class="asiConfigPriceTitle"> <fmt:message key="quantity"/></div>
        <div class="asiConfigPriceValue"> <c:out value="${model.quantity}"/> </div>
        <div style="clear: both;"></div>
        
        <div class="asiConfigPriceTitle"> <fmt:message key="unitPrice"/> </div>
        <div class="asiConfigPriceValue"> $<fmt:formatNumber value="${model.unitPrice}" pattern="#,##0.00#" maxFractionDigits="${siteConfig['MAX_FRACTION_DIGIT'].value}" /> </div>
        <div style="clear: both;"></div>
        
        <div class="asiConfigPriceTitle"><fmt:message key="subTotal"/></div>
        <div class="asiConfigPriceValue">$<fmt:formatNumber value="${model.unitPrice * model.quantity}" pattern="#,##0.00#" maxFractionDigits="${siteConfig['MAX_FRACTION_DIGIT'].value}" /></div>
        <div style="clear: both;"></div>
        
      </div>
      </c:if>
    </div>
  </div>
  <c:set var="stepNum" value="${stepNum +1}"/>
  </c:if>
  
  <!-- Step 3 : Imprint Selection -->
  <c:if test="${model.unitPrice != null}">
  <c:choose>
    <c:when test="${model.imprintOptions != null}">
  	<div class="asiConfigImprint">
    	<div class="asiConfigTitle">${stepNum}. Choose Imprint Color
      	<span style="float: right;">
      	<c:if test="${fn:length(model.imprintOptions) > 0}">
        	<select name="imMethod" id="imMethod" onchange="setAsiOption();">
          	<c:forEach items="${model.imprintOptions}" var="method">
            	<option value="${method}" label="${method}" <c:if test="${method == model.imMethod}"> selected="selected"</c:if> >${method}</option>
          	</c:forEach>
        	</select>
        	<input type="hidden" name="optionNVPair" value="Imprint Method_value_${model.imMethod}"/>
      	</c:if>
      	</span>
    	</div>
      
   	 	<div class="asiConfigValues asiConfigImprintWrapper" style="width: 400px;">
        <div class="asiConfigSelectedImprintColors" id="asiConfigSelectedImprintColors" style="width:400px;">
        	<c:set var="selected" value="true"/>
	        <c:forEach items="${model.colors}" var="color" varStatus="status">
          	<div id="asiIMColor${status.index +1}" style="width: 90px; float: left;">
	          <c:set var="selected" value="false"/>
	          <c:forEach items="${model.selectedImprint}" var="selectedImprint">
	            <c:if test="${selectedImprint.key == color.name}">
	              <div id="asiConfigHidden${status.index +1}">
	              <input type="hidden" name="asi_option" class="asi_option" value="imColor${status.index + 1}"/>
			      <input type="hidden" name="asi_option_imColor${status.index + 1}" id = "asi_option_imColor${status.index + 1}" value="${color.name}"/>
			      <input type="hidden" name="optionNVPair" value="Imprint Color_value_${selectedImprint.key}"/>
 		          </div>
 		          <c:set var="selected" value="true"/>
	            </c:if>
	          </c:forEach>
	          <div style="cursor: pointer; float: left;"> 
	            <input type="checkbox" <c:if test="${selected}">checked="checked"</c:if> onclick="removeImprintColor('${status.index +1}', '${color.name}', this.checked);"/> 
	          </div>
			  <div class="asiConfigIMColorBox" id="asiConfigImprint${status.index + 1}" style="background-color: ${color.value}; padding:10px; margin: 2px 0 10 3px; width: 30px;"></div> 
	        </div>
	         <c:if test="${(status.index + 1) % 3 == 0}">
	            <div style="clear: both;"></div>
	          </c:if>
	        </c:forEach>
	      </div>
	      <div style="clear: both;"></div>
	    <%--
        <div class="asiConfigSelectedImprintColors" id="asiConfigSelectedImprintColors">
        	<c:forEach items="${model.selectedImprint}" var="selectedImprint" varStatus="status">
          	<div id="asiIMColor${status.index +1}">
	            <input type="hidden" name="asi_option" class="asi_option" value="imColor${status.index + 1}"/>
			    <input type="hidden" name="asi_option_imColor${status.index + 1}" id = "asi_option_imColor${status.index + 1}" value="${selectedImprint.key}"/>
			    <input type="hidden" name="optionNVPair" value="Imprint Color_value_${selectedImprint.key}">
 		        <span style="cursor: pointer;"> <img src="${_contextpath}/assets/Image/Layout/button_remove.png" onclick="removeImprintColor('${status.index +1}');"> </span>
			    <div class="asiConfigIMColorBox" id="asiConfigImprint${status.index + 1}" style="background-color: ${selectedImprint.value};"></div> 
	            <div style="clear: both;"></div>
	          </div>
	        </c:forEach>
	        
	        <c:if test="${fn:length(model.selectedImprint) < 10}">
	          <input type="hidden" name="asi_option" class="asi_option" value="imColor${fn:length(model.selectedImprint) + 1}"/>
			  <input type="hidden" name="asi_option_imColor${fn:length(model.selectedImprint) + 1}" id = "asi_option_imColor${fn:length(model.selectedImprint) + 1}"/>
			  <div class="asiConfigIMColorBox" id="asiConfigImprint${fn:length(model.selectedImprint) + 1}"></div>
	          <div style="clear: both;"></div>
	        </c:if>
	      </div>
	      <div class="asiConfigImprintColors">
	        <c:forEach items="${model.colors}" var="color" varStatus="status">
			  <div class="asiConfigColorBox" style="background-color: ${color.value};" onclick="updateImprintColor('${color.name}','${fn:length(model.selectedImprint) + 1}');">Add</div>
	          <c:if test="${(status.index + 1) % 4 == 0}">
			    <div style="clear: both;"></div>
			  </c:if>
	        </c:forEach>
	      </div>
	      <div style="clear: both;"></div>
	         --%>
	      <div style="margin: 10px 0 10px 0">
	        Provide Imprint Text: <input type="text" style="border: 1px solid; border-radius: 5px; padding: 5px; height: 30px; width: 200px;" maxlength="50" name="imText" id="imText" value="${model.imText}" onkeyup="assignText(this.value,'imTextNV');"/>
      	    <input type="hidden" name="optionNVPair" id="imTextNV" value="Imprint Text_value_${fn:escapeXml(model.imText)}"/>
 		  </div>
	      
	    </div>
	    
	    <c:if test="${fn:length(model.selectedImprint) > 0}">
	      <div class="asiConfigImPriceWrapper">
	        <c:if test="${model.setupCharge != null}">
	          <div class="asiConfigImPriceTitle"> Setup Charge</div>
	          <div class="asiConfigImPriceValue"> 1 X $<fmt:formatNumber value="${model.setupCharge}" pattern="#,##0.00#" maxFractionDigits="${siteConfig['MAX_FRACTION_DIGIT'].value}" /> = $<fmt:formatNumber value="${model.setupCharge * 1}" pattern="#,##0.00#" maxFractionDigits="${siteConfig['MAX_FRACTION_DIGIT'].value}" /> </div>
	          <div style="clear: both;"></div>
	       	  <input type="hidden" name="optionNVPair" value="Setup Charge_value_${model.setupCharge}"/>
      	   </c:if>
	        
	        <c:if test="${model.runningCharge != null}">
	          <div class="asiConfigImPriceTitle"> Running Charge </div>
	          <div class="asiConfigImPriceValue"> 1 X ${model.quantity} X $<fmt:formatNumber value="${model.runningCharge}" pattern="#,##0.00#" maxFractionDigits="${siteConfig['MAX_FRACTION_DIGIT'].value}" /> = $<fmt:formatNumber value="${model.runningCharge * 1 * model.quantity}" pattern="#,##0.00#" maxFractionDigits="${siteConfig['MAX_FRACTION_DIGIT'].value}" /> </div>
	          <div style="clear: both;"></div>
	          <input type="hidden" name="optionNVPair" value="Running Charge_value_${model.runningCharge * model.quantity}"/>
      	    </c:if>
	
	        <c:if test="${model.additionalSetupCharge != null}">
	          <div class="asiConfigImPriceTitle">Add'l Setup Charge</div>
	          <div class="asiConfigImPriceValue"> <c:out value="${fn:length(model.selectedImprint) - 1}"/> X $${model.additionalSetupCharge} = $<fmt:formatNumber value="${model.additionalSetupCharge * (fn:length(model.selectedImprint) - 1)}" pattern="#,##0.00#" maxFractionDigits="${siteConfig['MAX_FRACTION_DIGIT'].value}" /> </div>
	          <div style="clear: both;"></div>
	          <input type="hidden" name="optionNVPair" value="Add'l Setup Charge_value_${model.additionalSetupCharge * (fn:length(model.selectedImprint) - 1)}"/>
      	    </c:if>
	        
	        <c:if test="${model.additionalRunningCharge != null}">
	          <div class="asiConfigImPriceTitle">Add'l Running Charge </div>
	          <div class="asiConfigImPriceValue"> <c:out value="${fn:length(model.selectedImprint) - 1}"/> X ${model.quantity} X $${model.additionalRunningCharge} = $<fmt:formatNumber value="${model.additionalRunningCharge * model.quantity * (fn:length(model.selectedImprint) - 1)}" pattern="#,##0.00#" maxFractionDigits="${siteConfig['MAX_FRACTION_DIGIT'].value}" /> </div>
	          <div style="clear: both;"></div>
	          <input type="hidden" name="optionNVPair" value="Add'l Running Charge_value_${model.additionalRunningCharge * model.quantity * (fn:length(model.selectedImprint) - 1)}"/>
      	    </c:if>
	        
	        <c:if test="${model.imprintCharge > 0}">
	          <div class="asiConfigImPriceTitle" style="font-weight: bolder;">Total Charge</div>
	          <div class="asiConfigImPriceValue" style="font-weight: bolder;">$<fmt:formatNumber value="${model.imprintCharge}" pattern="#,##0.00#" maxFractionDigits="${siteConfig['MAX_FRACTION_DIGIT'].value}" /> </div>
	          <div style="clear: both;"></div>
	        </c:if>
	      </div>
	      <input type="hidden" name="additionalCharge" value="${model.imprintCharge}"/>
      	</c:if>
	</div>
	
    </c:when>
    <c:otherwise>
    
    </c:otherwise>
  </c:choose>
   <input type="image" border="0" id="_addtocart" name="_addtocart" src="${_contextpath}/assets/Image/Layout/button_addtocart${_lang}.gif" />
  
  
  </c:if>
  <input type="hidden" name="product.id" id="product.id" value="${model.product.id}"/>
</div>
</form>