<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>

<tiles:insertDefinition name="${_template}" flush="true">
<c:if test="${siteConfig['LEFTBAR_DEFAULT_TYPE'].value == '3' }">
  <c:set value="false" var="hideMootools" />
</c:if> 
  <tiles:putAttribute name="leftbar" value="/WEB-INF/jsp/frontend/asi/searchFilters.jsp" />
  <tiles:putAttribute name="content" type="string">
 <link href="assets/asi_product.css" rel="stylesheet" type="text/css"> 
 <script src="javascript/mootools-1.2.5-core.js" type="text/javascript"></script>
 <script src="javascript/mootools-1.2.5.1-more.js" type="text/javascript"></script>

<c:out value="${model.productSearchLayout.headerHtml}" escapeXml="false"/>    
  
<script type="text/javascript">
<!--
function submitSearch(filter, value, searchTerm){
	$('asiFilter'+filter).value = value;
	$('asiFilterH'+filter).value = searchTerm;
	$('asiSearch').submit();
}
function removeFilter(filter){
	$('asiFilter'+filter).value = null;
	$('asiFilterH'+filter).value = null;
	$('asiSearch').submit();
}
function submitForm(page){
	$('pageLink').value = page;
	document.getElementById('navForm').submit();
}
//-->
</script>
<c:if test="${ model.searchResult != null and model.totalResult > 0}">
<c:set var="pageShowing">
<table border="0" cellpadding="0" cellspacing="0" width="100%">
  <tr>
  <td class="pageShowing">
  <fmt:message key="showing">
	<fmt:param value="${model.pageStart}"/>
	<fmt:param value="${model.pageEnd}"/>
	<fmt:param value="${model.totalResult}"/>
  </fmt:message>
  </td>
  <c:if test="${siteConfig['PRODUCT_SORT_BY_FRONTEND'].value == 'true'}">
  <td id="sortby">
    <form action="${_contextpath}/asiProducts.jhtm" method="post">
    <div style="display: none;">
      <input type="hidden" name="q" value="${model.q}" />
      <input type="hidden" name="page" value="${model.page}" />
      <input type="hidden" name="rpp" value="${model.rpp}" />
      <input type="hidden" name="color" value="${fn:escapeXml(model.color)}"></input>
	  <input type="hidden" name="colorH" value="${fn:escapeXml(model.colorH)}"></input>
	  <input type="hidden" name="size" value="${fn:escapeXml(model.size)}"></input>
	  <input type="hidden" name="sizeH" value="${fn:escapeXml(model.sizeH)}"></input>
	  <input type="hidden" name="shape" value="${fn:escapeXml(model.shape)}"></input>
	  <input type="hidden" name="shapeH" value="${fn:escapeXml(model.shapeH)}"></input>
	  <input type="hidden" name="material" value="${fn:escapeXml(model.material)}"></input>
      <input type="hidden" name="materialH" value="${fn:escapeXml(model.materialH)}"></input>
      <input type="hidden" name="category" value="${fn:escapeXml(model.category)}"></input>
      <input type="hidden" name="categoryH" value="${fn:escapeXml(model.categoryH)}"></input>
    </div>
    <select name="sort" onchange="submit()">
      <option value="DFLT"><fmt:message key="f_sortby"/></option>
      <option value="PRNM" <c:if test="${model.sort == 'PRNM'}">selected="selected"</c:if>>Name</option>
      <option value="PRLH" <c:if test="${model.sort == 'PRLH'}">selected="selected"</c:if>>Price ($ to $$)</option>
      <option value="PRHL" <c:if test="${model.sort == 'PRHL'}">selected="selected"</c:if>>Price ($$ to $)</option>
      <c:if test="${siteConfig['PRODUCT_RATE'].value =='true' or gSiteConfig['gPRODUCT_REVIEW']}">
        <option value="SPRT" <c:if test="${model.sort == 'SPRT'}">selected="selected"</c:if>>Rating</option>
      </c:if>  
    </select>
    </form>			
  </td>
  </c:if>
  <td class="pageNavi">
	<div style="float: right;">
	<div style="float: left; margin-right:5px;">
	  Page 
	</div>
	<div style="float: left; margin-right:5px;">
	<form action="${_contextpath}/asiProducts.jhtm" method="post">
    <div style="display: none;">
      <input type="hidden" name="q" value="${model.q}" />
	  <input type="hidden" name="sort" value="${model.sort}" />
	  <input type="hidden" name="rpp" value="${model.rpp}" />
	  <input type="hidden" name="color" value="${fn:escapeXml(model.color)}"></input>
	  <input type="hidden" name="colorH" value="${fn:escapeXml(model.colorH)}"></input>
	  <input type="hidden" name="size" value="${fn:escapeXml(model.size)}"></input>
	  <input type="hidden" name="sizeH" value="${fn:escapeXml(model.sizeH)}"></input>
	  <input type="hidden" name="shape" value="${fn:escapeXml(model.shape)}"></input>
	  <input type="hidden" name="shapeH" value="${fn:escapeXml(model.shapeH)}"></input>
	  <input type="hidden" name="material" value="${fn:escapeXml(model.material)}"></input>
	  <input type="hidden" name="materialH" value="${fn:escapeXml(model.materialH)}"></input>
	  <input type="hidden" name="category" value="${fn:escapeXml(model.category)}"></input>
	  <input type="hidden" name="categoryH" value="${fn:escapeXml(model.categoryH)}"></input>
    </div>
    <c:choose>
	<c:when test="${model.pages <= 100 and model.pages > 0}">
		  <select name="page" id="page" onchange="submit()">
			<c:forEach begin="1" end="${model.pages}" var="page">
	  	      <option value="${page}" <c:if test="${page == model.page}">selected</c:if>>${page}</option>
			</c:forEach>
		  </select>  
	</c:when>
	<c:otherwise>
			<input type="text" id="page" name="page" value="${model.page}" size="5" class="textfield50" />
			<input type="submit" value="go"/>
	</c:otherwise>
	</c:choose>
	</form>
	</div>
	<div style="float: left; margin-right:5px;">
	of <c:out value="${model.pages}"/>
	| 
	</div>
	<div style="float: left; margin-right:5px;">
	<form action="${_contextpath}/asiProducts.jhtm" method="post" id="navForm" name="navForm">
    <div style="display: none;">
      <input type="hidden" name="page" id="pageLink" value="1"/>
	  <input type="hidden" name="q" value="${model.q}" />
	  <input type="hidden" name="sort" value="${model.sort}" />
	  <input type="hidden" name="rpp" value="${model.rpp}" />
	  <input type="hidden" name="color" value="${fn:escapeXml(model.color)}"></input>
	  <input type="hidden" name="colorH" value="${fn:escapeXml(model.colorH)}"></input>
	  <input type="hidden" name="size" value="${fn:escapeXml(model.size)}"></input>
	  <input type="hidden" name="sizeH" value="${fn:escapeXml(model.sizeH)}"></input>
	  <input type="hidden" name="shape" value="${fn:escapeXml(model.shape)}"></input>
	  <input type="hidden" name="shapeH" value="${fn:escapeXml(model.shapeH)}"></input>
	  <input type="hidden" name="material" value="${fn:escapeXml(model.material)}"></input>
	  <input type="hidden" name="materialH" value="${fn:escapeXml(model.materialH)}"></input>
	  <input type="hidden" name="category" value="${fn:escapeXml(model.category)}"></input>
	  <input type="hidden" name="categoryH" value="${fn:escapeXml(model.categoryH)}"></input>
    </div>
    <c:choose>
      <c:when test="${model.page == 1}">
        <span class="pageNaviDead"><fmt:message key="f_previous" /></span>
      </c:when>
      <c:otherwise>
        <a style="cursor: pointer;text-decoration: underline" class="pageNaviLink" onclick="submitForm('${model.page - 1}');" ><fmt:message key="f_previous" /></a>
	  </c:otherwise>
    </c:choose>
    | 
	<c:choose>
      <c:when test="${model.page == model.pages}">
        <span class="pageNaviDead"><fmt:message key="f_next" /></span>
      </c:when>
      <c:otherwise>
       <a style="cursor: pointer;text-decoration: underline" onclick="submitForm('${model.page + 1}');" class="pageNaviLink"><fmt:message key="f_next" /></a>
	  </c:otherwise>
    </c:choose>
    </form>
	</div>
	</div>
	</td>
  </tr>
</table>

</c:set>
<c:out value="${pageShowing}" escapeXml="false" />
</c:if>

<form action="${_contextpath}/addToCart.jhtm" method="post" onsubmit="return checkForm()">
	<c:set var="numCul" value="${siteConfig['NUMBER_PRODUCT_PER_ROW'].value}" />
	<c:choose>
	<c:when test="${param.view == 'asi'}">
		<table border="0" class="multipleProductPerRow" cellspacing="10">
		<c:forEach items="${model.asiProducts}" var="product" varStatus="status">
		  <c:if test="${ status.index % numCul == 0 }">
		    <tr>
		  </c:if> 
		  <td valign="top" class="thumbnail_compact_cell">
		   <table width="100%" border="0" cellpadding="0" cellspacing="0">
		    <c:if test="${product.imageUrl != null}">
		    <tr>
		     <td align="center">
		     <div class="cat_image_wrapper">
		       <a href="${_contextpath}/asiProduct.jhtm?id=${product.id}">
		         <img src="http://api.asicentral.com/v1/<c:out value="${product.imageUrl}"/>" border="0" class="thumbnail_image" alt="<c:out value="${product.name}"/>">
		  	   </a>
		      </div>
		     </td>
		    </tr>	
		    </c:if>
		    <tr>
		     <td align="center">
		     <div class="cat_name_wrapper">
		       <a href="${_contextpath}/asiProduct.jhtm?id=${product.id}" class="thumbnail_item_name"><c:out value="${product.name}" escapeXml="false"/></a>
		     </div>
		     </td>
		    </tr>
		    <tr>
		     <td align="center"> 
		       <c:choose>
		         <c:when test="${product.price.currencyCode != null}">
		           <fmt:message key="${product.price.currencyCode}"/><fmt:formatNumber value="${product.price.price}" pattern="#,##0.00#" maxFractionDigits="${siteConfig['MAX_FRACTION_DIGIT'].value}" />
		         </c:when>
		         <c:otherwise>
		          $<fmt:formatNumber value="${product.price.price}" pattern="#,##0.00#" maxFractionDigits="${siteConfig['MAX_FRACTION_DIGIT'].value}" />
		    	 </c:otherwise>
		       </c:choose>
		       </td>
		    </tr>
		   </table> 
		  </td>
		  <c:if test="${ status.index % numCul == numCul - 1 }">
		    </tr>
		  </c:if>
		</c:forEach>
		</table>
	</c:when>
	<c:otherwise>		
		<c:forEach items="${model.products}" var="product" varStatus="status">
		<c:set var="productLink" value="${_contextpath}/product.jhtm?id=${product.id}"/>
		 <%@ include file="/WEB-INF/jsp/frontend/thumbnail/view7.jsp" %>
		</c:forEach>		
	</c:otherwise>
	</c:choose>
</form>

<c:if test="${model.iSearchResult != null and model.iSearchResult.resultsTotal > 0}">
<c:out value="${pageShowing}" escapeXml="false" />
</c:if>
    
<c:out value="${model.productSearchLayout.footerHtml}" escapeXml="false"/>     
    
  </tiles:putAttribute>
</tiles:insertDefinition>
