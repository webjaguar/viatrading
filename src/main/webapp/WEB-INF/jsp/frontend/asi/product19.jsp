<%@ page import="java.util.*" %>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="/WEB-INF/tlds/spring.tld" prefix="spring" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ taglib uri="/WEB-INF/tlds/custom.tld" prefix="wj" %>

<tiles:insertDefinition name="${_template}" flush="true">
  <tiles:putAttribute name="leftbar" value="/WEB-INF/jsp/frontend/layout/template${_leftBar}/leftbar.jsp" />
  
  <tiles:putAttribute name="content" type="string">
<c:set value="${false}" var="multibox"/>

<link rel="stylesheet" href="${_contextpath}/assets/SmoothGallery/multiboxASI.css" type="text/css" media="screen" />
<link rel="stylesheet" href="${_contextpath}/assets/asi_product.css" type="text/css" media="screen" />

 <c:if test="${model.mootools and param.hideMootools == null and hideMootools == null}">
<script src="/javascript/mootools-1.2.5.1-more.js" type="text/javascript"></script>
</c:if>

<c:if test="${fn:trim(model.productLayout.headerHtml) != ''}">
  <c:set value="${model.productLayout.headerHtml}" var="headerHtml"/>
  <%@ include file="/WEB-INF/jsp/frontend/common/headerDynamicElement.jsp" %>
  <div><c:out value="${headerHtml}" escapeXml="false"/></div>
</c:if>

<c:if test="${model.message != null}">
  <div class="message"><fmt:message key="${model.message}" /></div>
</c:if>

<c:if test="${model.product != null}">
<c:set value="${model.product}" var="product"/>
<script type="text/javascript">
window.addEvent('domready', function(){
	var tabs1 = new SimpleTabs($('tab-block-1'), { selector: 'h4'});
});
function closeBox() {
	parent.asiOptionsBox.close();
	setTimeout(function() { location.href = "${_contextpath}/viewCart.jhtm";},500);
}
</script>
<c:if test="${siteConfig['ASI_POPUP_DISPLAY'].value == 'true' }" >
<script type="text/javascript">
var asiOptionsBox;
window.addEvent('domready', function(){
	asiOptionsBox = new multiBox('showCartmb',  {showNumbers:false, showControls:false, initialWidth:710, overlay: new overlay({opacity:'0.3'})});
});
</script>
</c:if>



<c:if test="${siteConfig['DETAILS_IMAGE_WIDTH'].value != '' }" >
<script language="JavaScript" type="text/JavaScript">
function zoomIn() {
	window.open('${_contextpath}/productImage.jhtm?id=${product.id}&imgUrl=' + document.getElementById("_image").src,'name','width=800,height=600,resizable=yes,scrollbars=1');
}
</script>
</c:if>

<script language="JavaScript" type="text/JavaScript">
<!--
var saveContactBox;
window.addEvent('domready', function(){			 
	saveContactBox = new multiBox('mbAsiPrice', {showControls : false, useOverlay: false, showNumbers: false });
});
function checkNumber( aNumber )
{
	var goodChars = "0123456789";
	var i = 0;
	if ( aNumber == "" )
		return 0; //empty
	
	for ( i = 0 ; i <= aNumber.length - 1 ; i++ ) {
		if ( goodChars.indexOf( aNumber.charAt(i) ) == -1 ) 
			return -1; //invalid number			
	}	
	return 1; //valid number
}
function checkQty( pid, boxExtraAmt ){
	var product = document.getElementById('quantity_' + pid);
	if ( checkNumber( product.value ) == -1 || product.value == 0 ) {
	    alert("invalid Quantity");
		product.focus();
		return false;
	}
	return true;
}
function checkForm()
{
	var productList = document.getElementsByName('product.id');
	var allQtyEmpty = true;
	for(var i=0; i<productList.length; i++) 
	{
	    var el = document.getElementById("quantity_" + productList[i].value);
		if ( el != null )
		{
			if ( checkNumber( el.value ) == 1  )
			{
				allQtyEmpty = false;
			}
			else if ( checkNumber( el.value ) == -1 ) 
			{
				alert("invalid Quantity");
				el.focus();
				return false;
			}			
		}	
	}
	if ( allQtyEmpty )
	{
		alert("Please Enter Quantity.");
		return false;
	}
	else
		return true;	
}
function loadConfigurator(productId, variant, variantIndex){
	var urlAppend = '?id='+productId;	
	var updateDivId = 'configurator';
	if(variant != '') {
		urlAppend = urlAppend +'&variant='+encodeURIComponent(variant);
		updateDivId = updateDivId+variantIndex;
	}
	//alert(urlAppend);
	//alert(updateDivId);
	$$('.configurator').each(function(ele){
		ele.innerHTML = '';
	});
	var requestHTMLData = new Request.HTML ({
		url: "${_contextpath}/showAsiOptions3.jhtm"+urlAppend,
		onComplete: function(response){
			$('configuratorButton'+variantIndex).destroy();
		},
      	update: $(updateDivId)
	}).send();
}
//-->
</script>


<c:choose>
  <c:when test="${siteConfig['DETAILS_IMAGE_LOCATION'].value == 'left' }">
    <c:set var="details_images_style" value="float:left;padding:3px 15px 15px 3px;" />
    <c:set var="details_desc_style" value="float:left;padding:0 0 0 15px;" />
  </c:when>
</c:choose>

<%-- bread crumbs --%>
<c:import url="/WEB-INF/jsp/frontend/layout/${_template}/breadcrumbs.jsp" />

<table class="container-details" border="0" cellpadding="0" cellspacing="0">
<tr class="top">
<td class="topLeft"><img src="${_contextpath}/assets/Image/Layout/topleft.gif" border="0" alt="" /></td>
<td class="topTop"></td>
<td class="topRight"><img src="${_contextpath}/assets/Image/Layout/topright.gif" border="0" alt="" /></td>
</tr>
<tr class="middle"><td class="middleLeft"></td>
<td class="middleMiddle">

<div class="details19">
<table>
<tr valign="top">
<td>
<c:set var="productImage">
<c:choose>
 <c:when test="${fn:contains(siteConfig['PRODUCT_IMAGE_LAYOUTS'].value,'mb') and 
 		(param.multibox == null or param.multibox != 'off') and
 		(product.imageLayout == 'mb' or (empty product.imageLayout and siteConfig['PRODUCT_IMAGE_LAYOUT'].value == 'mb'))}">
 <link rel="stylesheet" href="${_contextpath}/assets/SmoothGallery/multibox.css" type="text/css" media="screen" />
 <c:set value="${true}" var="multibox"/>
 <script src="${_contextpath}/javascript/overlay.js" type="text/javascript" ></script>
 <script src="${_contextpath}/javascript/multibox.js" type="text/javascript" ></script>
 <script type="text/javascript">
	window.addEvent('domready', function(){
		var box = new multiBox('mbxwz', {overlay: new overlay()});
	});
 </script>
 	<div class="details_image_box" id="details_image_boxId" style="${details_images_style}
	 <c:if test="${siteConfig['DETAILS_IMAGE_WIDTH'].value != '' }">width: <c:out value="${siteConfig['DETAILS_IMAGE_WIDTH'].value}"/>px;</c:if>">
	 
	<c:forEach items="${model.product.images}" var="image" varStatus="status">
	  <c:if test="${status.first}">
	      <c:choose>
	  	   <c:when test="${product.asiId != null}">
	  	   <a href="<c:out value="${fn:replace(product.thumbnail.imageUrl, 'prodimgs', 'prodbigimgs')}"/>" id="mb0" class="mbxwz" title="<c:out value="${product.name}"/><c:if test="${product.shortDesc != ''}"> - <c:out value="${product.shortDesc}"/></c:if>"><img src="<c:out value="${fn:replace(product.thumbnail.imageUrl, 'prodimgs', 'prodbigimgs')}"/>" border="0" style="<c:if test="${siteConfig['DETAILS_IMAGE_WIDTH'].value != '' }">width: <c:out value="${siteConfig['DETAILS_IMAGE_WIDTH'].value}"/>px</c:if>" alt="<c:out value="${product.name}"/><c:if test="${product.shortDesc != ''}"> - <c:out value="${product.shortDesc}"/></c:if>"/></a>
	  	   </c:when>
	  	   <c:otherwise>
	  	   <a href="<c:if test="${!image.absolute}">${_contextpath}/assets/Image/Product/detailsbig/</c:if><c:out value="${image.imageUrl}"/>" id="mb0" class="mbxwz" title="<c:out value="${product.name}"/>"><img src="<c:if test="${!image.absolute}">${_contextpath}/assets/Image/Product/detailsbig/</c:if><c:out value="${image.imageUrl}"/>" border="0" style="<c:if test="${siteConfig['DETAILS_IMAGE_WIDTH'].value != '' }">width: <c:out value="${siteConfig['DETAILS_IMAGE_WIDTH'].value}"/>px</c:if>" alt="<c:out value="${product.name}"/><c:if test="${product.shortDesc != ''}"> - <c:out value="${product.shortDesc}"/></c:if>"/></a>
	  	   </c:otherwise>
	  	  </c:choose>
	  </c:if>
	</c:forEach>
    <div style="clear: both;" >
    <c:forEach items="${model.product.images}" var="image" varStatus="status">
	  <c:if test="${status.count > 1}">
	      <c:choose>
	  	   <c:when test="${product.asiId != null}">
	  	   <a href="<c:out value="${fn:replace(product.thumbnail.imageUrl, 'prodimgs', 'prodbigimgs')}"/>" id="mb${status.count - 1}" class="mbxwz" title="<c:out value="${product.name}"/><c:if test="${product.shortDesc != ''}"> - <c:out value="${product.shortDesc}"/></c:if>"><img src="<c:out value="${fn:replace(product.thumbnail.imageUrl, 'prodimgs', 'prodbigimgs')}"/>" border="0" class="details_thumbnail"  alt="<c:out value="${product.name}"/><c:if test="${product.shortDesc != ''}"> - <c:out value="${product.shortDesc}"/></c:if>"/></a>
	  	   </c:when>
	  	   <c:otherwise>
	  	   <a href="<c:if test="${!image.absolute}">${_contextpath}/assets/Image/Product/detailsbig/</c:if><c:out value="${image.imageUrl}"/>" id="mb${status.count - 1}" class="mbxwz" title="<c:out value="${product.name}"/><c:if test="${product.shortDesc != ''}"> - <c:out value="${product.shortDesc}"/></c:if>"><img src="<c:if test="${!image.absolute}">${_contextpath}/assets/Image/Product/detailsbig/</c:if><c:out value="${image.imageUrl}"/>" border="0" class="details_thumbnail" alt="<c:out value="${product.name}"/><c:if test="${product.shortDesc != ''}"> - <c:out value="${product.shortDesc}"/></c:if>"/></a>
	  	   </c:otherwise>
	  	  </c:choose>
	  </c:if>
	</c:forEach>
	</div>
	</div>        
 </c:when>
 <c:when test="${fn:contains(siteConfig['PRODUCT_IMAGE_LAYOUTS'].value,'qb') and 
 		(product.imageLayout == 'qb' or (empty product.imageLayout and siteConfig['PRODUCT_IMAGE_LAYOUT'].value == 'qb'))}">
 <link rel="stylesheet" href="${_contextpath}/assets/SmoothGallery/QuickBox/quickbox.css" type="text/css" media="screen" />
 <c:set value="${true}" var="quickbox"/>
 <script src="${_contextpath}/javascript/QuickBox.js" type="text/javascript" ></script>
 <script type="text/javascript">
	window.addEvent('domready', function(){
		new QuickBox();
	});
 </script>
 	<div class="details_image_box" style="${details_images_style}
	 <c:if test="${siteConfig['DETAILS_IMAGE_WIDTH'].value != '' }">width: <c:out value="${siteConfig['DETAILS_IMAGE_WIDTH'].value}"/>px;</c:if>">
	 
	<c:forEach items="${model.product.images}" var="image" varStatus="status">
	  <c:if test="${status.first}">
	    <a href="<c:if test="${!image.absolute}">${_contextpath}/assets/Image/Product/detailsbig/</c:if><c:out value="${image.imageUrl}"/>" id="" class="" title="" rel="lightbox-atomium"><img src="<c:if test="${!image.absolute}">${_contextpath}/assets/Image/Product/detailsbig/</c:if><c:out value="${image.imageUrl}"/>" border="0" style="<c:if test="${siteConfig['DETAILS_IMAGE_WIDTH'].value != '' }">width: <c:out value="${siteConfig['DETAILS_IMAGE_WIDTH'].value}"/>px</c:if>" alt="<c:out value="${product.name}"/><c:if test="${product.shortDesc != ''}"> - <c:out value="${product.shortDesc}"/></c:if>"/></a>
	  </c:if>
	</c:forEach>
    <div style="clear: both;" >
    <c:forEach items="${model.product.images}" var="image" varStatus="status">
	  <c:if test="${status.count > 1}">
	      <a href="<c:if test="${!image.absolute}">${_contextpath}/assets/Image/Product/detailsbig/</c:if><c:out value="${image.imageUrl}"/>" id="sb${status.count - 1}" class="" title="" rel="lightbox-atomium"><img src="<c:if test="${!image.absolute}">${_contextpath}/assets/Image/Product/detailsbig/</c:if><c:out value="${image.imageUrl}"/>" border="0" class="details_thumbnail" alt="<c:out value="${product.name}"/><c:if test="${product.shortDesc != ''}"> - <c:out value="${product.shortDesc}"/></c:if>"/></a>
	  </c:if>
	</c:forEach>
	</div>
	</div>        
 </c:when>
 <c:when test="${fn:contains(siteConfig['PRODUCT_IMAGE_LAYOUTS'].value,'mz') and 
 		(product.imageLayout == 'mz' or (empty product.imageLayout and siteConfig['PRODUCT_IMAGE_LAYOUT'].value == 'mz'))}">
 <link rel="stylesheet" href="${_contextpath}/assets/SmoothGallery/MagicZoomPlus/magiczoomplus.css" type="text/css" media="screen" />
 <script src="${_contextpath}/javascript/magiczoomplus.js" type="text/javascript" ></script>
 	<div class="details_image_box" style="${details_images_style}
	 <c:if test="${siteConfig['DETAILS_IMAGE_WIDTH'].value != '' }">width: <c:out value="${siteConfig['DETAILS_IMAGE_WIDTH'].value}"/>px;</c:if>">
	 
	<c:forEach items="${model.product.images}" var="image" varStatus="status">
	  <c:if test="${status.first}">
	    <a href="<c:out value="${image.imageUrl}"/>" rel="zoom-width:350px;zoom-height:200px;expand-align:screen; expand-position:top=0; expand-size:original; selectors-effect:pounce; zoom-position:#zoomImageId;" class="MagicZoomPlus" id="Zoomer" >
	      <img alt="" src="<c:out value="${fn:replace(product.thumbnail.imageUrl, 'size=large', 'size=normal')}"/>" border="0" class="normal_image" alt="<c:out value="${product.name}"/><c:if test="${product.shortDesc != ''}"> - <c:out value="${product.shortDesc}"/></c:if>" />
	    </a>
	  </c:if>
	</c:forEach>
    <div style="clear: both;" >
    <c:forEach items="${model.product.images}" var="thumbImage" varStatus="status">
	  <c:if test="${status.count > 0}">
	  <c:choose>
  	   <c:when test="${product.asiId != null}">
  	     <a class="thumb_link" rev="<c:out value="${fn:replace(thumbImage.imageUrl, 'size=large', 'size=normal')}"/>" rel="zoom-id:Zoomer;zoom-width:350px;zoom-height:200px;zoom-position:#zoomImageId;" href="<c:out value="${thumbImage.imageUrl}" />" style="outline: 0pt none; display: inline-block;">
			<img alt="" src="<c:out value="${fn:replace(thumbImage.imageUrl, 'size=large', 'size=small')}"/>">
		 </a>
  	   </c:when>
  	   <c:otherwise>
  	   	  <a href="<c:if test="${!image.absolute}">${_contextpath}/assets/Image/Product/detailsbig/</c:if><c:out value="${image.imageUrl}"/>" rel="zoom-id:Zoomer;zoom-width:350px;zoom-height:200px;zoom-position:#zoomImageId;"  rev="<c:if test="${!image.absolute}">${_contextpath}/assets/Image/Product/thumb/</c:if><c:out value="${image.imageUrl}"/>" title="" ><img src="<c:if test="${!image.absolute}">${_contextpath}/assets/Image/Product/thumb/</c:if><c:out value="${image.imageUrl}"/>" border="0" class="zoom_details_thumbnail" alt="<c:out value="${product.name}"/><c:if test="${product.shortDesc != ''}"> - <c:out value="${product.shortDesc}"/></c:if>"/></a>
  	   </c:otherwise>
  	  </c:choose>
	  </c:if>
	</c:forEach>
	</div>
	</div>
 </c:when>
 <c:otherwise>
    <div class="details_image_box" style="cursor: pointer;">
	  <c:forEach items="${model.product.images}" var="image" varStatus="status">
	    <c:if test="${status.first}">
		  <a style="cursor: pointer;" onclick="zoomIn('${model.product.id}');" title="View Larger Images"><img id="_image" src="<c:if test="${!image.absolute}">${_contextpath}/assets/Image/Product/detailsbig/</c:if><c:out value="${image.imageUrl}"/>" border="0" style="<c:if test="${siteConfig['DETAILS_IMAGE_WIDTH'].value != '' }">width: <c:out value="${siteConfig['DETAILS_IMAGE_WIDTH'].value}"/>px</c:if>" alt="<c:out value="${model.assignedProduct.name}"/><c:if test="${model.assignedProduct.shortDesc != ''}"> - <c:out value="${model.assignedProduct.shortDesc}"/></c:if>"/></a>
		</c:if>
	  </c:forEach>
	  <div>
	  <c:forEach items="${model.product.images}" var="image" varStatus="status">
		<c:if test="${status.index > 0}">
		<a href="#" class="details_thumbnail_anchor" onClick="return updateImage('${image.absolute}','${image.imageUrl}');">
		  <img src="<c:if test="${!image.absolute}">${_contextpath}/assets/Image/Product/detailsbig/</c:if><c:out value="${image.imageUrl}"/>" class="details_thumbnail" alt="<c:out value="${model.assignedProduct.name}"/><c:if test="${model.assignedProduct.shortDesc != ''}"> - <c:out value="${model.assignedProduct.shortDesc}"/></c:if>">
		</a>
		</c:if>
	  </c:forEach> 
	  <c:if test="${fn:length(model.product.images) > 1}">
	  <span class="moreImageWrapper">
		<a onclick="zoomIn('${model.product.id}');" title="View Larger Images"><span>View Larger Images</span></a>
	  </span>
	  </c:if>
	  </div>
	</div>
 </c:otherwise>
</c:choose>
</c:set>
<!-- image start -->
   <c:out value="${productImage}" escapeXml="false" />    
<!-- image end -->
</td>
<td>
<div id="detailsWrapperId">
<div id="zoomImageId"></div>
<c:if test="${siteConfig['SHOW_SKU'].value == 'true'}">
<c:if test="${product.sku != null}">
<div class="details_sku"><c:out value="${product.sku}" /></div>  
</c:if>
</c:if>


<div class="details_item_name"><h1><c:out value="${product.name}" escapeXml="false" /></h1></div>

<c:if test="${siteConfig['DETAILS_SHORT_DESC'].value == 'true' and product.shortDesc != ''}">
<div class="details_short_desc"><c:out value="${product.shortDesc}" escapeXml="false" /></div>  
</c:if>


<c:if test="${siteConfig['DETAILS_LONG_DESC_LOCATION'].value == 'above' and product.longDesc != ''}">
<div class="details_long_desc"><c:out value="${product.longDesc}" escapeXml="false" /></div> 
</c:if>



<c:if test="${siteConfig['TECHNOLOGO'].value != ''}">	   
<div id="technologoWrapper">
 <a href="http://www.technologo.com/techno.OnDemand?&email=<c:out value="${siteConfig['TECHNOLOGO'].value}" />&sku=${product.id}&name=${product.nameWithoutQuotes}&imagelocation_0=${wj:asiBigImageName(product.thumbnail.imageUrl)}&whiteout=true&orientation=10000&removelogo=true" target="_blank">
 <img src="${_contextpath}/assets/Image/Layout/button_create_virtual.gif"  border="0" alt="create virtual" />
 </a>
</div>
</c:if>

<c:if test="${gSiteConfig['gDEALS'] > 0 and product.deal.htmlCode != null}">
	<c:out value="${product.deal.htmlCode}" escapeXml="false"/>
</c:if>
<c:if test="${gSiteConfig['gDEALS'] > 0 and product.deal.parentHtmlCode != null}">
	<c:out value="${product.deal.parentHtmlCode}" escapeXml="false"/>
</c:if>


<c:if test="${siteConfig['DETAILS_LONG_DESC_LOCATION'].value != 'above' and product.longDesc != ''}">
<div class="details_long_desc"><c:out value="${product.longDesc}" escapeXml="false" /></div>  
</c:if>

<div class="boxWrapper" align="right">
<c:choose>
<c:when test="${gSiteConfig['gPRODUCT_REVIEW'] and product.enableRate}">
<%@ include file="/WEB-INF/jsp/frontend/common/quickProductReview.jsp" %>
</c:when>
<c:when test="${siteConfig['PRODUCT_RATE'].value == 'true' and product.enableRate}">
<%@ include file="/WEB-INF/jsp/frontend/common/productRate.jsp" %>
</c:when>
</c:choose>


<c:if test="${ product.salesTag.image and !empty product.price }" >
<div class="saleTagImage">
 <img src="${_contextpath}/assets/Image/PromoSalesTag/salestag_${product.salesTag.tagId}.gif" />
</div>
</c:if>

<%--
<div id="socialNetworkLogoId" class="socialNetworkLogo">
    <input type="image" border="0" src="${_contextpath}/assets/Image/Layout/SocialNetworkLogo.jpg" alt="SocialNetworkLogo">
</div>
 --%>
<%-- ASI layout have MyList for all products --%>
<c:if test="${gSiteConfig['gMYLIST']}">
<form action="${_contextpath}/addToList.jhtm">
  <input type="hidden" name="product.id" value="${product.id}">
  <div id="addToListId" class="addToList" align="center">
    <input type="image" border="0" src="${_contextpath}/assets/Image/Layout/button_addtolist${_lang}.gif">
  </div>
</form>
</c:if>
</div>
</div>
<br/>
</td>

</tr>
<tr>
<td valign="top" width="45%" style="padding-right: 20px;">
<!-- details start -->
<div class="asi_details_desc" id="details_desc">

<div class="asiDetailBox_main">
  <c:if test="${model.productColors != null}">
  <div class="asiDetailBox">
    <div class="asiDetailTitle" onclick="slideOut('asiDetailColor',0,0);">Colors <span class="toggleDiv"></span> </div>
    <div class="asiDetailValue" id="asiDetailColor">
      <c:forEach items="${model.productColors}" var="color" varStatus="status">
        <c:out value="${color}" escapeXml="false"/><c:if test="${(status.index+1) < fn:length(model.productColors)}">,&nbsp;</c:if>  
      </c:forEach>
      <br/> 
    </div>
  </div>
  </c:if>
  
  <c:if test="${model.imprintOptions != null}">
  <div class="asiDetailBox">
    <div class="asiDetailTitle" onclick="slideOut('asiDetailImpOpt',0,0);">Imprint Options <span class="toggleDiv"></span></div>
    <div class="asiDetailValue" id="asiDetailImpOpt">
      <c:forEach items="${model.imprintOptions}" var="impsrint" varStatus="status">
        <c:out value="${imprint}" escapeXml="false"/><c:if test="${(status.index+1) < fn:length(model.imprintOptions)}">,&nbsp;</c:if>
      </c:forEach>
      <br/> 
    </div>
  </div>
  </c:if>
  
  <c:if test="${model.variantList != null}">
  <div class="asiDetailBox">
    <div class="asiDetailTitle" onclick="slideOut('asiDetailSize',0,0);">${model.variant} <span class="toggleDiv"></span></div>
    <div class="asiDetailValue" id="asiDetailSize">
      <c:forEach items="${model.variantList}" var="size" varStatus="status">
        <c:out value="${size}" escapeXml="false"/><c:if test="${(status.index+1) < fn:length(model.variantList)}">,&nbsp;</c:if>
      </c:forEach>
      <br/> 
    </div>
  </div>
  </c:if>
  
  <c:if test="${model.priceIncludes != null}">
    <div class="asiDetailBox">
    <div class="asiDetailTitle" onclick="slideOut('asiDetailPriceInclude',0,0);">Price Includes <span class="toggleDiv"></span></div>
    <div class="asiDetailValue" id="asiDetailPriceInclude"> <c:out value="${model.priceIncludes}" escapeXml="false"/> </div>
  	</div>
  </c:if>
  
  <c:if test="${model.additionalOptions != null}">
    <div class="asiDetailBox">
      <div class="asiDetailTitle" onclick="slideOut('asiDetailAddlOpt',0,0);">Additional Options <span class="toggleDiv"></span></div>
      <div class="asiDetailValue"  id="asiDetailAddlOpt">
        <c:forEach items="${model.additionalOptions}" var="addlOption">
          <span class="asiDetailVTitle"><c:out value="${addlOption.key}"/>:</span>
          <br/> 
          <div class="asiDetailVValue">
            <c:forEach items="${addlOption.value}" var="value">
              <c:out value="${value.key}" escapeXml="false"/> : <c:out value="${value.value}" escapeXml="false"/>
              <br/>  
            </c:forEach>
          </div>
          <br/>
        </c:forEach>
      </div>
    </div>
  </c:if>
  
  <c:if test="${model.productionTime != null}">
    <div class="asiDetailBox">
    <div class="asiDetailTitle" onclick="slideOut('asiDetailProductionTime',0,0);">Production Time <span class="toggleDiv"></span></div>
    <div class="asiDetailValue" id="asiDetailProductionTime"> <c:out value="${model.productionTime}" escapeXml="false"/> </div>
  	</div>
  </c:if>
  
  <c:if test="${model.packaging != null}">
    <div class="asiDetailBox">
    <div class="asiDetailTitle" onclick="slideOut('asiDetailPackaging',0,0);">Packaging <span class="toggleDiv"></span></div>
    <div class="asiDetailValue" id="asiDetailPackaging"> <c:out value="${model.packaging}" escapeXml="false"/> </div>
  	</div>
  </c:if>
  
  <c:if test="${model.shippingFobPoints != null}">
    <div class="asiDetailBox">
    <div class="asiDetailTitle" onclick="slideOut('asiDetailShipping',0,0);">Shipping <span class="toggleDiv"></span></div>
    <div class="asiDetailValue" id="asiDetailShipping"> FOB Points: <br/>
      <c:forEach items="${model.shippingFobPoints}" var="fob">
         <c:out value="${fob}" escapeXml="false"/> &nbsp;
      </c:forEach>
    </div>
  	</div>
  </c:if>
  
  <div class="asiDetailBox" >
  <div class="asiDetailTitle" onclick="slideOut('asiDetailRushService',0,0);">Rush Service <span class="toggleDiv"></span></div>
  <div class="asiDetailValue" id="asiDetailRushService">
    <c:choose>
      <c:when test="${model.rushService == null or !model.rushService}">N/A</c:when>
      <c:otherwise>Available</c:otherwise>
    </c:choose>
  </div>
  </div>  
  </div>

</div>

<!-- details end -->

</td>
<td valign="top" width="55%">
 <div class="asiDetailBox_wrapper">
  <div class="asiDetailBox" id="asiDetailPrice">
    <div class="asiDetailTitle" id="asiDetailPriceTitle">
    <c:choose>
      <c:when test="${model.asiProduct.lowestPrice != null and model.asiProduct.lowestPrice.isUndefined}"><fmt:message key="quote" /></c:when>
      <c:otherwise><fmt:message key="price" /></c:otherwise>
    </c:choose>
    </div>
    <div class="asiDetailValue" id="asiDetailPriceValue">
      <c:choose>
        <c:when test="${siteConfig['PRODUCT_QUOTE'].value == 'true' and model.product.quote}">
        	<%@ include file="/WEB-INF/jsp/frontend/asi/quote.jsp" %>
        </c:when>
        <c:when test="${model.product.asiIgnorePrice}">
        	<table class="asiPriceGrid" width="100%">
				  <tr class="apgHeader">
				    <td colspan="${fn:length(model.product.price) + 1}" class="apgHeading"><c:out value="Prices" /></td>
				  </tr>
				  <tr>
				    <th class="apgTitle"><fmt:message key="quantity" /></th>
				    <c:forEach items="${model.product.price}" var="price" varStatus="priceStatus">
		              <td class="apgValue"><c:out value="${price.qtyFrom}" /></td>
		            </c:forEach>
				  </tr>
				  <tr>
				    <th class="apgTitle"><fmt:message key="price" /></th>
				    <c:forEach items="${model.product.price}" var="price" varStatus="priceStatus">
		              <td class="apgValue" <c:if test="${product.salesTag != null and product.salesTag.discount != 0.0}" >style="text-decoration: line-through"</c:if> >
		                 <fmt:message key="${siteConfig['CURRENCY'].value}" /><fmt:formatNumber value="${price.amt}" pattern="#,##0.00#" maxFractionDigits="${siteConfig['MAX_FRACTION_DIGIT'].value}" />
				      </td>
		            </c:forEach>
				  </tr>
				  <c:if test="${model.product.salesTag != null and model.product.salesTag.discount != 0.0}">
	  			  <tr>
	  			    <th class="apgTitle"><fmt:message key="f_youPay" /></th>
			    	<c:forEach items="${model.product.price}" var="price" varStatus="priceStatus">
			      	  <td class="apgValue">
	  			    	<c:choose>
	  				  	  <c:when test="${product.salesTag.percent}">
	  				        <fmt:message key="${siteConfig['CURRENCY'].value}" /><fmt:formatNumber value="${price.amt *( (100 - product.salesTag.discount) / 100)}" pattern="#,##0.00" maxFractionDigits="2" />
	  				  	  </c:when>
	  				  	  <c:otherwise>
	  				    	<fmt:message key="${siteConfig['CURRENCY'].value}" /><fmt:formatNumber value="${price.amt - product.salesTag.discount}" pattern="#,##0.00" maxFractionDigits="2" />
	  				  	  </c:otherwise>
	  					</c:choose>
	  			  	  </td>
	  				</c:forEach>
	  			  </tr>
			      </c:if>
			  <tr align="right">
			    <td colspan="${fn:length(model.product.price) + 1}">
			      <c:choose>
			        <c:when test="${siteConfig['ASI_POPUP_DISPLAY'].value == 'true' }">
			          <a href="${_contextpath}/showAsiOptions3.jhtm?id=${product.id}" rel="width:600,height:515" id="showCartmb${status.index}" class="showCartmb" title="<fmt:message key='session' />"><input type="image" border="0" name="_addtocart" class="_addtocart" src="${_contextpath}/assets/Image/Layout/button_addtocart${_lang}.gif" /></a>
			        </c:when>
			        <c:otherwise>
			          <img alt="" src="${_contextpath}/assets/Image/Layout/button_addtocart${_lang}.gif" id="configuratorButton" onclick="loadConfigurators('${product.id}', '', '');" >
			          <div id="configurator" class="configurator" style="width: 400px;"></div>
			        </c:otherwise>
			      </c:choose>  
			    </td>
			  </tr>
			  </table>
        </c:when>
        <c:otherwise>
          <c:choose>
            <c:when test="${model.asiProduct.lowestPrice != null and model.asiProduct.lowestPrice.isUndefined}">
               <%@ include file="/WEB-INF/jsp/frontend/asi/quote.jsp" %>
            </c:when>
        	<c:when test="${model.asiProduct.variants != null && fn:length(model.asiProduct.variants.variant) > 0}">
	        <!-- Products with variants -->
          		<c:forEach items="${model.asiProduct.variants.variant}" var="variant" varStatus="variantStatus">
                <table class="asiPriceGrid" width="100%" border="0" cellpadding="0" cellspacing="0">
				  <tr class="apgHeader">
				    <td colspan="${fn:length(variant.prices.price) + 1}" class="apgHeading"><c:out value="${variant.description}"  escapeXml="false" /></td>
				  </tr>
				  <tr>
				    <th class="apgTitle"><fmt:message key="quantity" /></th>
				    <c:forEach items="${variant.prices.price}" var="price" varStatus="priceStatus">
		              <td class="apgValue"><c:out value="${variant.prices.price[priceStatus.index].quantity.range.from}" /></td>
		            </c:forEach>
				  </tr>
				  <tr>
				    <th class="apgTitle"><fmt:message key="price" /></th>
				    <c:forEach items="${variant.prices.price}" var="price" varStatus="priceStatus">
		              <td class="apgValue" <c:if test="${(product.salesTag != null and product.salesTag.discount != 0.0) or (siteConfig.get['ASI_PRICE_MARK_UP_CHANGE_BASE_PRICE'].value != 'true' and model.asiProduct2 != null)}" >style="text-decoration: line-through"</c:if> >
		                 <fmt:message key="${siteConfig['CURRENCY'].value}" /><fmt:formatNumber value="${variant.prices.price[priceStatus.index].price}" pattern="#,##0.00#" maxFractionDigits="${siteConfig['MAX_FRACTION_DIGIT'].value}" />
				      </td>
		            </c:forEach>
				  </tr>
				  <c:choose>
				    <c:when test="${product.salesTag != null and product.salesTag.discount != 0.0}">
		  			  <c:forEach items="${model.asiProduct.variants.variant}" var="variant2" varStatus="variant2Status">
		          	    <c:if test="${variant2Status.index == variantStatus.index}">
		          	  	  <tr>
		  			        <th class="apgTitle"><fmt:message key="f_youPay" /></th>
				    	    <c:forEach items="${variant2.prices.price}" var="price" varStatus="priceStatus">
				      	      <td class="apgValue">
		  			    	    <c:choose>
		  				  	      <c:when test="${product.salesTag.percent}">
		  				            <fmt:message key="${siteConfig['CURRENCY'].value}" /><fmt:formatNumber value="${variant2.prices.price[priceStatus.index].price *( (100 - product.salesTag.discount) / 100)}" pattern="#,##0.00" maxFractionDigits="2" />
		  				  	      </c:when>
		  				  	      <c:otherwise>
		  				    	    <fmt:message key="${siteConfig['CURRENCY'].value}" /><fmt:formatNumber value="${variant2.prices.price[priceStatus.index].price - product.salesTag.discount}" pattern="#,##0.00" maxFractionDigits="2" />
		  				  	      </c:otherwise>
		  					    </c:choose>
		  			  	      </td>
		  				    </c:forEach>
		  			      </tr>
				        </c:if>
				      </c:forEach>
				    </c:when>
				    <c:when test="${siteConfig.get['ASI_PRICE_MARK_UP_CHANGE_BASE_PRICE'].value != 'true' and model.asiProduct2 != null}">
		  			  <c:forEach items="${model.asiProduct2.variants.variant}" var="variant2" varStatus="variant2Status">
		          	    <c:if test="${variant2Status.index == variantStatus.index}">
		          	      <tr>
			  		        <th class="apgTitle"><fmt:message key="f_youPay" /></th>
				            <c:forEach items="${variant2.prices.price}" var="price" varStatus="priceStatus">
				              <td class="apgValue">
		  			            <fmt:message key="${siteConfig['CURRENCY'].value}" /><fmt:formatNumber value="${variant2.prices.price[priceStatus.index].price}" pattern="#,##0.00" maxFractionDigits="2" />
		  			          </td>
		  			        </c:forEach>
		  			      </tr>
				        </c:if>
		          	  </c:forEach>
				    </c:when>
				  </c:choose>
				  <tr align="right">
				    <td colspan="${fn:length(variant.prices.price) + 1}">
			        <c:choose>
			          <c:when test="${siteConfig['ASI_POPUP_DISPLAY'].value == 'true' }">
				        <a href="${_contextpath}/showAsiOptions3.jhtm?id=${product.id}&variant=${variant.description}" rel="width:600,height:515" id="showCartmb${status.index}" class="showCartmb" title="<fmt:message key='session' />"><input type="image" border="0" name="_addtocart" class="_addtocart" src="${_contextpath}/assets/Image/Layout/button_addtocart${_lang}.gif" /></a>
			          </c:when>
			          <c:otherwise>
				        <img alt="" src="${_contextpath}/assets/Image/Layout/button_addtocart${_lang}.gif" onclick="loadConfigurator('${product.id}', '${variant.description}', '${variantStatus.index}');" id="configuratorButton${variantStatus.index}">
			            <div id="configurator${variantStatus.index}" class="configurator"></div>
			          </c:otherwise>
			        </c:choose>
				    </td>
				  </tr>
				  </table>
				</c:forEach>
            </c:when>
            <c:otherwise>
            <!-- Products w/o variants -->
		        <table class="asiPriceGrid" width="100%" border="0" cellpadding="0" cellspacing="0">
				  <tr class="apgHeader">
				    <td colspan="${fn:length(model.asiProduct.prices.price) + 1}" class="apgHeading"><c:out value="Prices" /></td>
				  </tr>
				  <tr>
				    <th class="apgTitle"><fmt:message key="quantity" /></th>
				    <c:forEach items="${model.asiProduct.prices.price}" var="price" varStatus="priceStatus">
		              <td class="apgValue"><c:out value="${model.asiProduct.prices.price[priceStatus.index].quantity.range.from}" /></td>
		            </c:forEach>
				  </tr>
				  <tr>
				    <th class="apgTitle"><fmt:message key="price" /></th>
				    <c:forEach items="${model.asiProduct.prices.price}" var="price" varStatus="priceStatus">
		              <td class="apgValue" <c:if test="${(product.salesTag != null and product.salesTag.discount != 0.0) or (siteConfig.get['ASI_PRICE_MARK_UP_CHANGE_BASE_PRICE'].value != 'true' and model.asiProduct2 != null)}" >style="text-decoration: line-through"</c:if> >
		                 <fmt:message key="${siteConfig['CURRENCY'].value}" /><fmt:formatNumber value="${model.asiProduct.prices.price[priceStatus.index].price}" pattern="#,##0.00#" maxFractionDigits="${siteConfig['MAX_FRACTION_DIGIT'].value}" />
				      </td>
		            </c:forEach>
				  </tr>
				  <c:choose>
				    <c:when test="${product.salesTag != null and product.salesTag.discount != 0.0}">
		  			  <tr>
		  			    <th class="apgTitle"><fmt:message key="f_youPay" /></th>
				    	<c:forEach items="${model.asiProduct.prices.price}" var="price" varStatus="priceStatus">
				      	  <td class="apgValue">
		  			    	<c:choose>
		  				  	  <c:when test="${product.salesTag.percent}">
		  				        <fmt:message key="${siteConfig['CURRENCY'].value}" /><fmt:formatNumber value="${model.asiProduct.prices.price[priceStatus.index].price *( (100 - product.salesTag.discount) / 100)}" pattern="#,##0.00" maxFractionDigits="2" />
		  				  	  </c:when>
		  				  	  <c:otherwise>
		  				    	<fmt:message key="${siteConfig['CURRENCY'].value}" /><fmt:formatNumber value="${model.asiProduct.prices.price[priceStatus.index].price - product.salesTag.discount}" pattern="#,##0.00" maxFractionDigits="2" />
		  				  	  </c:otherwise>
		  					</c:choose>
		  			  	  </td>
		  				</c:forEach>
		  			  </tr>
				    </c:when>
				    <c:when test="${siteConfig.get['ASI_PRICE_MARK_UP_CHANGE_BASE_PRICE'].value != 'true' and model.asiProduct2 != null}">
		  			<tr>
			  		  <th class="apgTitle"><fmt:message key="f_youPay" /></th>
				      <c:forEach items="${model.asiProduct2.prices.price}" var="price" varStatus="priceStatus">
				        <td class="apgValue">
		  			      <fmt:message key="${siteConfig['CURRENCY'].value}" /><fmt:formatNumber value="${model.asiProduct2.prices.price[priceStatus.index].price}" pattern="#,##0.00" maxFractionDigits="2" />
		  			    </td>
		  			  </c:forEach>
		  			</tr>
				    </c:when>
				  </c:choose>
		          <tr align="right">
				    <td colspan="${fn:length(model.asiProduct.prices.price) + 1}">
			        <c:choose>
			          <c:when test="${siteConfig['ASI_POPUP_DISPLAY'].value == 'true' }">
					    <a href="${_contextpath}/showAsiOptions3.jhtm?id=${product.id}" rel="width:600,height:515" id="showCartmb${status.index}" class="showCartmb" title="<fmt:message key='session' />"><input type="image" border="0" name="_addtocart" class="_addtocart" src="${_contextpath}/assets/Image/Layout/button_addtocart${_lang}.gif" /></a>
				      </c:when>
			          <c:otherwise>
				        <img alt="" src="${_contextpath}/assets/Image/Layout/button_addtocart${_lang}.gif" onclick="loadConfigurator('${product.id}', '', '');" id=configuratorButton">
			          	<div id="configurator" class="configurator"></div>
			          </c:otherwise>
			        </c:choose>
				    </td>
				  </tr>
				</table>
		    </c:otherwise> 
		  </c:choose>
		</c:otherwise>   
	</c:choose>    
    </div>
  </div>
</div>
</td>
</tr>
</table>


	</div>

<c:if test="${multibox != 'true'}">
<script src="${_contextpath}/javascript/overlay.js" type="text/javascript" ></script>
<script src="${_contextpath}/javascript/multibox.js" type="text/javascript" ></script>
</c:if>

</td><td class="middleRight"></td>
</tr>
<tr class="bottom">
<td class="bottomLeft"><img src="${_contextpath}/assets/Image/Layout/bottomleft.gif"  border="0" alt="" /></td>
<td class="bottomBottom"></td>
<td class="bottomRight"><img src="${_contextpath}/assets/Image/Layout/bottomright.gif"  border="0" alt="" /></td>
</tr>
</table>

      <div class="details_product_tab_container">
		<div class="details_tab">
		  <script src="${_contextpath}/javascript/SimpleTabs1.2.js" type="text/javascript"></script>
		  <!-- tabs -->
		  <div id="tab-block-1">
			<c:if test="${fn:length(product.productFields) > 0}">
  			  <h4 id="0" title="<c:out value="${siteConfig['PRODUCT_FIELDS_TITLE'].value}" escapeXml="false"/>"> <c:out value="${siteConfig['PRODUCT_FIELDS_TITLE'].value}" escapeXml="false" /></h4>
			  <div>
			    <!-- start tab --> 
  			    <table class="details_fields">
    			  <c:forEach items="${product.productFields}" var="productField" varStatus="row">
    			  <tr>
      			    <td class="details_field_name_row${row.index % 2}"><c:out value="${productField.name}" escapeXml="false" /> </td>
      			    <c:choose>
						<c:when test="${productField.formatType != null and productField.formatType != '' and productField.fieldType == 'number' }">
							<td class="details_field_value_row${row.index % 2}"><fmt:formatNumber value="${wj:trimAndSplit(productField.value, '|' , false, ',', true, true)}" pattern="${productField.formatType }" /></td>
						</c:when>
						<c:otherwise>
							<td class="details_field_value_row${row.index % 2}"><c:out value="${wj:trimAndSplit(productField.value, '|' , false, ',', true, true)}" escapeXml="false"/></td>
						</c:otherwise>
					</c:choose>
    			  </tr>
  				  </c:forEach>
  			    </table>
  			    <!-- end tab -->
			  </div>
  			</c:if>


			<c:if test="${model.recommendedList != null}">
			  <c:choose>
			    <c:when test="${!empty product.recommendedListTitle }">
				  <h4 id="1" title="Reviews"> <c:out value="${product.recommendedListTitle}"/></h4>
			    </c:when>
				<c:otherwise>
				  <h4 id="1" title="Reviews"> <c:out value="${siteConfig['RECOMMENDED_LIST_TITLE'].value}"/></h4>
			    </c:otherwise>
			  </c:choose>
			  <div>
			    <!-- start tab --> 
  			    <c:set var="recommendedList_HTML">
					<div style="float:left;width:100%">
					<c:set var="numCul" value="${siteConfig['NUMBER_PRODUCT_PER_ROW'].value}" />
					<c:forEach items="${model.recommendedList}" var="product" varStatus="status">
					
					<c:set var="productLink">${_contextpath}/product.jhtm?id=${product.id}<c:if test="${model.product == null or siteConfig['RECOMMENDED_LIST_SHOW_BREADCRUMB'].value == 'true'}">&cid=<c:out value="${param.cid}"/></c:if></c:set>
					<c:if test="${gSiteConfig['gMOD_REWRITE'] == '1' and not fn:contains(product.sku, '/')}">
					  <c:set var="productLink">${_contextpath}/${siteConfig['MOD_REWRITE_PRODUCT'].value}/${product.encodedSku}/${product.encodedName}.html<c:if test="${model.product == null or siteConfig['RECOMMENDED_LIST_SHOW_BREADCRUMB'].value == 'true'}">?cid=<c:out value="${param.cid}"/></c:if></c:set>
					</c:if>
					
					<c:choose>
					 <c:when test="${( model.product.recommendedListDisplay == 'quick') or (empty model.product.recommendedListDisplay and siteConfig['RECOMMENDED_LIST_DISPLAY'].value == 'quick')}">
					    <%@ include file="/WEB-INF/jsp/frontend/quickmode/view1.jsp" %>
					 </c:when>
					 <c:when test="${( model.product.recommendedListDisplay == '2') or (empty model.product.recommendedListDisplay and siteConfig['RECOMMENDED_LIST_DISPLAY'].value == '2')}">
					    <%@ include file="/WEB-INF/jsp/frontend/thumbnail/view2.jsp" %>
					 </c:when>
					 <c:when test="${( model.product.recommendedListDisplay == '3') or (empty model.product.recommendedListDisplay and siteConfig['RECOMMENDED_LIST_DISPLAY'].value == '3')}">
						<%@ include file="/WEB-INF/jsp/frontend/thumbnail/view3.jsp" %>
					 </c:when>
					 <c:when test="${( model.product.recommendedListDisplay == '4') or (empty model.product.recommendedListDisplay and siteConfig['RECOMMENDED_LIST_DISPLAY'].value == '4')}">
						<%@ include file="/WEB-INF/jsp/frontend/thumbnail/view4.jsp" %>
					 </c:when>
					 <c:when test="${( model.product.recommendedListDisplay == '5') or (empty model.product.recommendedListDisplay and siteConfig['RECOMMENDED_LIST_DISPLAY'].value == '5')}">
						<%@ include file="/WEB-INF/jsp/frontend/thumbnail/view5.jsp" %>
					 </c:when>
					 <c:when test="${( model.product.recommendedListDisplay == '6') or (empty model.product.recommendedListDisplay and siteConfig['RECOMMENDED_LIST_DISPLAY'].value == '6')}">
						<%@ include file="/WEB-INF/jsp/frontend/thumbnail/view6.jsp" %>
					 </c:when>
					 <c:when test="${( model.product.recommendedListDisplay == '7') or (empty model.product.recommendedListDisplay and siteConfig['RECOMMENDED_LIST_DISPLAY'].value == '7')}">
						<%@ include file="/WEB-INF/jsp/frontend/thumbnail/view7.jsp" %>
					 </c:when>
					 <c:when test="${( model.product.recommendedListDisplay == '8') or (empty model.product.recommendedListDisplay and siteConfig['RECOMMENDED_LIST_DISPLAY'].value == '8')}">
						<%@ include file="/WEB-INF/jsp/frontend/thumbnail/view8.jsp" %>
					 </c:when>
					 <c:when test="${( model.product.recommendedListDisplay == '9') or (empty model.product.recommendedListDisplay and siteConfig['RECOMMENDED_LIST_DISPLAY'].value == '9')}">
						<%@ include file="/WEB-INF/jsp/frontend/thumbnail/view9.jsp" %>
					 </c:when>
					 <c:when test="${( model.product.recommendedListDisplay == '10') or (empty model.product.recommendedListDisplay and siteConfig['RECOMMENDED_LIST_DISPLAY'].value == '10')}">
					    <%@ include file="/WEB-INF/jsp/frontend/thumbnail/view10.jsp" %>
					 </c:when>
					 <c:when test="${( model.product.recommendedListDisplay == '11') or (empty model.product.recommendedListDisplay and siteConfig['RECOMMENDED_LIST_DISPLAY'].value == '11')}">
					    <%@ include file="/WEB-INF/jsp/frontend/thumbnail/view11.jsp" %>
					 </c:when>
					 <c:when test="${( model.product.recommendedListDisplay == '12') or (empty model.product.recommendedListDisplay and siteConfig['RECOMMENDED_LIST_DISPLAY'].value == '12')}">
					    <%@ include file="/WEB-INF/jsp/frontend/thumbnail/view12.jsp" %>
					 </c:when>
					 <c:otherwise>
						<%@ include file="/WEB-INF/jsp/frontend/thumbnail/view1.jsp" %>
					 </c:otherwise>
					</c:choose>
					</c:forEach>
				</div>
				</c:set>
				<c:out value="${recommendedList_HTML}" escapeXml="false" />
  			    
  			    <!-- end tab -->
			  </div>
  			</c:if>

			<c:if test="${gSiteConfig['gPRODUCT_REVIEW'] and siteConfig['PRODUCT_RATE'].value != 'true' and model.product.enableRate and model.productReviewList.nrOfElements > 0}">
			  <h4 id="2" title="Reviews"> <c:out value="Reviews"/></h4>
			  <div>
			    <!-- start tab --> 
  			    <div class="asi_product_review_list_box_outer">
				  <div class="asi_product_review_list_box">
					<%@ include file="/WEB-INF/jsp/frontend/common/productReviewList.jsp" %>
				  </div>
				</div>
  			    <!-- end tab -->
			  </div>
  			</c:if>


			
			
			<c:forEach items="${model.tabList}" var="tab" varStatus="status">
			  <h4 id="${tab.tabNumber + 2}" title="<c:out value="${tab.tabName}" escapeXml="false"/>"> <c:out value="${tab.tabName}" escapeXml="false" /></h4>
			  <div>
			    <!-- start tab --> 
				  <c:out value="${tab.tabContent}" escapeXml="false" /> 
				<!-- end tab -->
			  </div>
			 </c:forEach>
			</div>
		  </div>
		</div>	

<c:if test="${model.alsoConsiderMap[product.id] != null}">
   <c:set var="product" value="${model.alsoConsiderMap[product.id]}" />
   <%@ include file="/WEB-INF/jsp/frontend/common/alsoconsider.jsp" %>
   <c:set value="${model.product}" var="product"/>
</c:if>
</c:if>

<c:if test="${fn:trim(model.productLayout.footerHtml) != ''}">
  <c:set value="${model.productLayout.footerHtml}" var="footerHtml"/>
  <%@ include file="/WEB-INF/jsp/frontend/common/footerDynamicElement.jsp" %>
  <div style="float:left;width:100%"><c:out value="${footerHtml}" escapeXml="false"/></div>
</c:if>

  </tiles:putAttribute>
</tiles:insertDefinition>