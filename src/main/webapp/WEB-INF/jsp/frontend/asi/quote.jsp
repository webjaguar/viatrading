<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>

<link rel="stylesheet" href="${_contextpath}/assets/quote.css" type="text/css" media="screen" />
<script type="text/JavaScript">
<!--
function emailCheck(str){
	 if(/^[a-z0-9._%-]+@[a-z0-9.-]+\.[a-z]{2,4}$/i.test(str) == false) {
		alert('Invalid Email');
		return false;
	 }
	 return true;
}
function assignValue(ele, value) {
$(ele).value = value;
}
function phoneCheck(str){
	if (str.search(/^\d{10}$/)==-1) {
		alert("Please enter a valid 10 digit number");
		return false;
	} 
}
function saveContact(){
	if($('firstName').value == '' || $('lastName').value == '') {
		alert('Provide your first and last name.');
		return false;
	}
	if($('phone').value == '' || $('email').value == '') {
		alert('Provide ( Email and Phone)');
		return false;
	}
	if ($('email').value != '' && emailCheck($('email').value)==false){
		$('email').value="";
		$('email').focus();
		return false;
	}
	if ($('phone').value != '' && phoneCheck($('phone').value)==false){
		$('phone').value="";
		$('phone').focus();
		return false;
	}
	var sku = $('sku').value;
	var name = $('name').value;
	var firstName = $('firstName').value;
	var lastName = $('lastName').value;
	var company = $('company').value;
	var address = $('address').value;
	var city = $('city').value;
	var state = $('state').value;
	var zipCode = $('zipCode').value;
	var country = $('country').value;
	var email = $('email').value;
	var phone = $('phone').value;
	var note = $('note').value;
	var q="firstName="+firstName+"&lastName="+lastName+"&company="+company+"&address="+address+"&city="+city+"&state="+state+"&zipCode="+zipCode+"&country="+country+"&email="+email+"&phone="+phone+"&note="+note+"&sku="+sku+"&name="+name+"&leadSource=quote";
	//alert(q);
	var requestQuote = new Request({
	   url: "${_contextpath}/insert-ajax-contact.jhtm?" + q,
       method: 'post',
       onFailure: function(response){
    	   $('syncSpinner').style.display = 'block';
    	   $('priceQuoteBox').fade(1);
    	   $('quoteButton').style.display = 'block';
           $('successMessage').set('html', '<div style="color : RED; text-align : LEFT;">We are experincing problem in saving your quote. <br/> Please try again later.</div>');
       },
       onRequest: function() {
    	   new Element('img', {src:'${_contextpath}/assets/Image/Layout/spinner.gif'}, {id:'syncSpinner'}).inject($('quoteButton'),'after');
    	   $('priceQuoteBox').fade(0.4);
    	   $('quoteButton').style.display = 'none';
       },
       onSuccess: function(response) {
    	   //alert("done.");
    	   $('quoteForm').style.display = 'none';
           $('successMessage').set('html', response);
       }
	}).send();
}
//-->
</script>
  <div id="priceQuoteBox" class="priceQuote">
	<form id="quoteForm" autocomplete="off" onsubmit="saveContact(); return false;">
	<input type="hidden" value="${product.sku}" id="sku"/>
	<input type="hidden" value="${product.name}" id="name"/>
	
  	<div class="header">Please provide your contact information</div>
	<fieldset class="top">
		<label class="title"><fmt:message key="firstName"/></label>
		<input name="firstName" id="firstName" type="text" size="15" onkeyup="assignValue('firstName', this.value);"/>
	</fieldset>
	<fieldset class="bottom">
		<label class="title"><fmt:message key="lastName"/></label>
		<input name="lastName" id="lastName" type="text" size="15" onkeyup="assignValue('lastName', this.value);"/>
	</fieldset>
	<fieldset class="bottom">
		<label class="title"><fmt:message key="company"/></label>
		<input name="company" id="company" type="text" size="15" onkeyup="assignValue('company', this.value);"/>
	</fieldset>
	<fieldset class="bottom">
		<label class="title"><fmt:message key="address"/></label>
		<input name="address" id="address" type="text" size="15" onkeyup="assignValue('address', this.value);"/>
	</fieldset>
	<fieldset class="bottom">
		<label class="title"><fmt:message key="city"/></label>
		<input name="city" id="city" type="text" size="15" onkeyup="assignValue('city', this.value);"/>
	</fieldset>
	<fieldset class="bottom">
		<label class="title"><fmt:message key="state"/></label>
		<input name="state" id="state" type="text" size="15" onkeyup="assignValue('state', this.value);"/>
	</fieldset>
	<fieldset class="bottom">
		<label class="title"><fmt:message key="zipCode"/></label>
		<input name="zipCode" id="zipCode" type="text" size="15" onkeyup="assignValue('zipCode', this.value);"/>
	</fieldset>
	<fieldset class="bottom">
		<label class="title"><fmt:message key="country"/></label>
		<input name="country" id="country" type="text" size="15" onkeyup="assignValue('country', this.value);"/>
	</fieldset>
	<fieldset class="bottom">
		<label class="title"><fmt:message key="email"/></label>
		<input name="email" id="email" type="text" size="15" onkeyup="assignValue('email', this.value);"/>
	</fieldset>
	<fieldset class="bottom">
		<label class="title"><fmt:message key="phone"/></label>
		<input name="phone" id="phone" type="text" size="15" onkeyup="assignValue('phone', this.value);"/>
	</fieldset>
	<fieldset class="bottom">
		<label class="title"><fmt:message key="message"/></label>
		<textarea name="note" id="note" onkeyup="assignValue('note', this.value);"></textarea>
	</fieldset>
	<fieldset>
		<div id="button">
			<input type="image" id="quoteButton" border="0" name="_addtocart" class="_addtocart" src="${_contextpath}/assets/Image/Layout/button_addtoquote${_lang}.gif" />
		</div>
	</fieldset>
	</form>
  </div>
  <div id="successMessage"></div>