<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
	  
	  <input type="hidden" name="product.id" id="product.id" value="${model.product.id}" />
	  <c:if test="${model.additionalCharge != null}">
	    <input type="hidden" name="additionalCharge" value="${model.additionalCharge}" />
	  </c:if>
	  <input type="hidden" name="asiUnitPrice" value="${model.unitPrice}" />
	  <input type="hidden" name="asiOriginalPrice" value="${model.asiOriginalPrice}" />
	  <c:if test="${model.variant != null}">
	    <input type="hidden" name="variant" id="variant" value="${fn:escapeXml(model.variant)}" />
	  	<input type="hidden" name="optionNVPair" value="Variant_value_${fn:escapeXml(model.variant)}"/>
	  </c:if>
	     
	  <table class="mbASIOptions" cellpadding="0" cellspacing="0" border="0">
		
		<tr class="mbASIOptions">
		  <td class="optionTitle"><fmt:message key="quantity"></fmt:message></td>
		  <td class="optionValues"><input type="text" maxlength="5" name="quantity_${model.product.id}" class="optionValueInput" id="quantity_${model.product.id}" value="<c:out value='${model.quantity}' />" onchange="updateOptions()"/>
			<span id="unitPrice">
			    @ $<fmt:formatNumber value="${model.unitPrice}" pattern="#,##0.00#" maxFractionDigits="${siteConfig['MAX_FRACTION_DIGIT'].value}" />
		    </span>
		  </td>
		</tr>
		
		<!-- Color Selection Starts -->
		<c:if test="${model.productColors != null}">
		<tr class="mbASIOptions color" id="highlightColor">
			<td class="optionName">
				<div class="optionTitle"><c:out value="Color"/>: </div> 
			</td>
			<td class="optionValues">
			  <c:choose>
			    <c:when test="${fn:length(model.productColors) > 1}">
				  <select style="width: 157;" name="asi_option_values_color" class="optionValueSelect" id="color" onchange="updateOptions()">
			  		<option value="">Please Select</option>
			  		<c:forEach items="${model.productColors}" var="color" varStatus="poSatus">
					  <option value="${color}" <c:if test="${model.selectedColor == color}">selected="selected"</c:if>><c:out value="${color}"/></option>
			  		</c:forEach>
				  </select>
		        </c:when>
				<c:otherwise>
				  <c:out value="${model.productColors[0]}"/>
			  	  <input type="hidden" name="asi_option_values_color" id="color" value="${model.productColors[0]}" />
				</c:otherwise>	  
			  </c:choose>
		  	  <input type="hidden" name="optionNVPair" value="Color_value_${fn:escapeXml(model.selectedColor)}"/>
	    	</td>
		</tr>
		</c:if>
		<!-- Color Selection Ends -->
		
		<!-- Material Selection Starts -->
		<c:if test="${model.materials != null}">
		<tr class="mbASIOptions color" id="highlightColor">
			<td class="optionName">
				<div class="optionTitle"><c:out value="Material"/>: </div> 
			</td>
			<td class="optionValues">
			  <c:choose>
			    <c:when test="${fn:length(model.materials) > 1}">
				  <select style="width: 157;" name="asi_option_values_material" class="optionValueSelect" id="material" onchange="updateOptions()">
			  		<option value="">Please Select</option>
			  		<c:forEach items="${model.materials}" var="material" varStatus="poSatus">
					  <option value="${material}" <c:if test="${model.selectedMaterial == material}">selected="selected"</c:if>><c:out value="${material}"/></option>
			  		</c:forEach>
				  </select>
		  	    </c:when>
				<c:otherwise>
				  <c:out value="${model.materials[0]}"/>
			  	  <input type="hidden" name="asi_option_values_material" value="${model.materials[0]}" id="material" />
				</c:otherwise>	  
			  </c:choose>
			  <input type="hidden" name="optionNVPair" value="Material_value_${fn:escapeXml(model.selectedMaterial)}"/>
	  		</td>
		</tr>
		</c:if>
		<!-- Material Selection Ends -->
		
		<!-- Size Selection Starts -->
		<c:if test="${model.sizes != null}">
		<tr class="mbASIOptions color" id="highlightColor">
			<td class="optionName">
				<div class="optionTitle"><c:out value="Size"/>: </div> 
			</td>
			<td class="optionValues">
			  <c:choose>
			    <c:when test="${fn:length(model.sizes) > 1}">
				  <select style="width: 157;" name="asi_option_values_material" class="optionValueSelect" id="size" onchange="updateOptions()">
			  		<option value="">Please Select</option>
			  		<c:forEach items="${model.sizes}" var="size" varStatus="poSatus">
					  <option value="${size}" <c:if test="${model.selectedSize == size}">selected="selected"</c:if>><c:out value="${size}"/></option>
			  		</c:forEach>
				  </select>
			  	</c:when>
				<c:otherwise>
				  <c:out value="${model.sizes[0]}"/>
			  	  <input type="hidden" name="asi_option_values_size" value="${model.sizes[0]}" id="size" />
			    </c:otherwise>	  
			  </c:choose>
	  	 	  <input type="hidden" name="optionNVPair" value="Size_value_${fn:escapeXml(model.selectedSize)}"/>
			</td>
		</tr>
		</c:if>
		<!-- Size Selection Ends -->
		
		<!-- Shape Selection Starts -->
		<c:if test="${model.shapes != null}">
		<tr class="mbASIOptions color" id="highlightColor">
			<td class="optionName">
				<div class="optionTitle"><c:out value="Shape"/>: </div> 
			</td>
			<td class="optionValues">
			  <c:choose>
			    <c:when test="${fn:length(model.shapes) > 1}">
				  <select style="width: 157;" name="asi_option_values_shape" class="optionValueSelect" id="shape" onchange="updateOptions()">
			  		<option value="">Please Select</option>
			  		<c:forEach items="${model.shapes}" var="shape" varStatus="poSatus">
					  <option value="${shape}" <c:if test="${model.selectedShape == shape}">selected="selected"</c:if>><c:out value="${shape}"/></option>
			  		</c:forEach>
				  </select>
			    </c:when>
				<c:otherwise>
				  <c:out value="${model.shapes[0]}"/>
			  	  <input type="hidden" name="asi_option_values_shape" value="${model.shapes[0]}" id="shape" />
				</c:otherwise>	  
			  </c:choose>
			  <input type="hidden" name="optionNVPair" value="Shape_value_${fn:escapeXml(model.selectedShape)}"/>
			</td>
		</tr>
		</c:if>
		<!-- Shape Selection Ends -->
		
		<!-- Imprint Method Selection Starts -->
		<c:if test="${model.imprintMethods != null}">
		<tr class="mbASIOptions color" id="highlightColor">
			<td class="optionName">
				<div class="optionTitle"><c:out value="Imprint Method"/>: </div> 
			</td>
			<td class="optionValues">
			  <c:choose>
			    <c:when test="${fn:length(model.imprintMethods) > 1}">
				  <select style="width: 157;" name="asi_option_values_imprintMethod" class="optionValueSelect" id="imprintMethod" onchange="updateOptions()">
			  		<option value="">Please Select</option>
			  		<c:forEach items="${model.imprintMethods}" var="imprintMethod" varStatus="poSatus">
					  <option value="${imprintMethod}" <c:if test="${model.selectedImprintMethod == imprintMethod}">selected="selected"</c:if>><c:out value="${imprintMethod}"/></option>
			  		</c:forEach>
				  </select>
			    </c:when>
				<c:otherwise>
				  <c:out value="${model.imprintMethods[0]}"/>
			  	  <input type="hidden" name="asi_option_values_imprintMethod" value="${model.imprintMethods[0]}" id="imprintMethod" />
				</c:otherwise>	  
			  </c:choose>
			  <c:choose>
			    <c:when test="${model.additionalCharge != null}">
			      <span id="imprintCharges">
			        $<fmt:formatNumber value="${model.additionalCharge}" pattern="#,##0.00#" maxFractionDigits="${siteConfig['MAX_FRACTION_DIGIT'].value}" />
		          </span>
			      <input type="hidden" name="optionNVPair" value="Imprint Method_value_${fn:escapeXml(model.selectedImprintMethod)} ($${model.additionalCharge})"/>
			    </c:when>
			    <c:otherwise>
			      <input type="hidden" name="optionNVPair" value="Imprint Method_value_${fn:escapeXml(model.selectedImprintMethod)}"/>
			    </c:otherwise>
			  </c:choose>
			  
			  <c:if test="${model.additionalCharge != null}">
			  </c:if>
			</td>
		</tr>
		
		<!-- Hard Coded Values for Imprint Color Selection -->
		<tr class="mbASIOptions color" id="highlightColor">
			<td class="optionName">
				<div class="optionTitle"><c:out value="Imprint Colors"/>: </div>
				<div class="disclaimer">additional charges may apply.</div> 
			</td>
			<td class="optionValues">
			  <input type="radio" name="multiColorImprint" value="Single Color" <c:if test="${model.selectedMultiColorImprint == 'Single Color'}">checked="checked"</c:if> class="optionValueRadio" onclick="updateRadioOptions(this.name, this.value)"/> Single Color
			  <input type="radio" name="multiColorImprint" value="Multi Color" <c:if test="${model.selectedMultiColorImprint == 'Multi Color'}">checked="checked"</c:if> class="optionValueRadio" onclick="updateRadioOptions(this.name, this.value)" /> Multiple Colors
			  <input type="hidden" id="multiColorImprint" value="${model.selectedMultiColorImprint}" />
			  <input type="hidden" name="optionNVPair" value="Imprint Color Type_value_${fn:escapeXml(model.selectedMultiColorImprint)}"/>
			</td>
		</tr>
		
		<tr class="mbASIOptions color" id="highlightColor">
			<td class="optionName">
				<div class="optionTitle"><c:out value="Provide Imprint Colors"/>: </div>
			</td>
			<td class="optionValues">
			  <textarea name="imprintColors" id="imprintColors" cols="25" rows="2" onchange="updateOptions()"><c:out value="${model.selectedImprintColors}"/></textarea> 
			  <input type="hidden" name="optionNVPair" value="Imprint Colors_value_${fn:escapeXml(model.selectedImprintColors)}"/>
			</td>
		</tr>
		
		<tr class="mbASIOptions color" id="highlightColor">
			<td class="optionName">
				<div class="optionTitle"><c:out value="Additional Locations"/>: </div>
				<div class="disclaimer">additional charges may apply.</div> 
			</td>
			<td class="optionValues">
			  <input type="radio" name="multiLocationImprint" value="Yes" class="optionValueRadio" <c:if test="${model.selectedImprintLocation == 'Yes'}">checked="checked"</c:if> onclick="updateRadioOptions(this.name, this.value)" /> <fmt:message key="yes"/>
			  <input type="radio" name="multiLocationImprint" value="No" class="optionValueRadio" <c:if test="${model.selectedImprintLocation == 'No'}">checked="checked"</c:if> onclick="updateRadioOptions(this.name, this.value)" /> <fmt:message key="no"/>
			  <input type="hidden" id="multiLocationImprint" value="${model.selectedImprintLocation}"/>
			  <input type="hidden" name="optionNVPair" value="Imprint Location_value_${fn:escapeXml(model.selectedImprintLocation)}"/>
			</td>
		</tr>
		
		<tr class="mbASIOptions color" id="highlightColor">
			<td class="optionName">
				<div class="optionTitle"><c:out value="Additional Information"/>: </div>
			</td>
			<td class="optionValues">
			  <textarea name="additionalInfo" id="additionalInfo" cols="25" rows="2" onchange="updateOptions()"><c:out value="${model.selectedAdditionalInfo}"/></textarea> 
			  <input type="hidden" name="optionNVPair" value="Additional Info_value_${fn:escapeXml(model.selectedAdditionalInfo)}"/>
			</td>
		</tr>
		</c:if>
		<!-- Imprint Method Selection Ends -->
		</table>
		
<%--		
		<div>
		<div class="mbASIOptions color" id="highlightColor">
			<div class="optionName">
				<div class="optionTitle"><fmt:message key="attachment"/>: </div>
			</div>
			<div class="optionValues">
			  <input value="TEST" type="file" name="attachment_${model.product.sku}" />
			</div>
		</div>
		</div>
--%>