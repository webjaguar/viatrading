<%@ page import="java.util.*" %>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="/WEB-INF/tlds/spring.tld" prefix="spring" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ taglib uri="/WEB-INF/tlds/custom.tld" prefix="wj" %>
<%
Map map = (HashMap) request.getAttribute("model");
map.put("productFields", map.get("slavesProductFields"));
map.put("showPriceColumn", true);
map.put("showQtyColumn", map.get("slavesShowQtyColumn"));
%>

<tiles:insertDefinition name="${_template}" flush="true">
  <tiles:putAttribute name="leftbar" value="/WEB-INF/jsp/frontend/layout/template${_leftBar}/leftbar.jsp" />
  <tiles:putAttribute name="content" type="string">
<c:set value="${false}" var="multibox"/>

<c:if test="${model.mootools and param.hideMootools == null}">
<script src="${_contextpath}/javascript/mootools-1.2.5.1-more.js" type="text/javascript"></script>
</c:if>

<c:if test="${fn:trim(model.productLayout.headerHtml) != ''}">
  <c:set value="${model.productLayout.headerHtml}" var="headerHtml"/>
  <%@ include file="/WEB-INF/jsp/frontend/common/headerDynamicElement.jsp" %>
  <div><c:out value="${headerHtml}" escapeXml="false"/></div>
</c:if>

<c:if test="${model.message != null}">
  <div class="message"><fmt:message key="${model.message}" /></div>
</c:if>

<c:if test="${model.product != null}">
<c:set value="${model.product}" var="product"/>
<script language="JavaScript" type="text/JavaScript">
<!--
<c:if test="${siteConfig['DETAILS_IMAGE_WIDTH'].value != '' }" >
function zoomIn() {
	window.open('${_contextpath}/productImage.jhtm?id=${product.id}&imgUrl=' + document.getElementById("_image").src,'name','width=800,height=600,resizable=yes,scrollbars=1');
}
</c:if>
function checkNumber( aNumber )
{
	var goodChars = "0123456789";
	var i = 0;
	if ( aNumber == "" )
		return 0; //empty
	
	for ( i = 0 ; i <= aNumber.length - 1 ; i++ )
	{
		if ( goodChars.indexOf( aNumber.charAt(i) ) == -1 ) 
			return -1; //invalid number			
	}	
	return 1; //valid number
}
<c:if test="${!empty product.numCustomLines}">
function copyCustomLine(numCustomLine, pid, el) {
	iValue = document.getElementById("line_1_" + numCustomLine).value;
	el.checked = false;
	if (iValue == "") {
		alert("You have no text to be applied to other lines.");
		return false;
	}
	var qty = document.getElementById('quantity_' + pid).value;
	for (i=2; i<=qty; i++) {
		document.getElementById("line_" + i + "_" + numCustomLine).value = iValue;
	}
}
function continueButton(pid) {
	if ( !checkQty(pid,0) ) {
		document.getElementById('customLineBox').style.display="none";
		document.getElementById('continueButton').src="${_contextpath}/assets/Image/Layout/button_continue.gif";
		return false;
	} else if (document.getElementById('customLineBox').style.display=="block") {
		return true;
	} else {
		document.getElementById('customLineBox').style.display="block";
		document.getElementById('continueButton').src="${_contextpath}/assets/Image/Layout/button_addtocart${_lang}.gif";
		var qty = document.getElementById('quantity_' + pid).value;
		for (var i=2; i<=qty; i++) {
			var div = document.createElement("div");
			div.setAttribute("align", "center");		
			var divText = document.createTextNode("Inscription " + i);
			div.appendChild(divText);
	        document.getElementById('customLineBox').appendChild(div);
	        var ul = document.createElement("ul");
	        for (var j=1; j<=${product.numCustomLines}; j++) {
	        	var li = document.createElement("li");
	            var cellText = document.createTextNode("Line# " + j + ".");
	            li.appendChild(cellText);
				var textfield = document.createElement("input"); 
				textfield.setAttribute("type", "text"); 
				textfield.setAttribute("name", "line_" + i + "_" + j); 
				textfield.setAttribute("id", "line_" + i + "_" + j); 
				textfield.style.textAlign="center";
				textfield.setAttribute("size", "50");		
				textfield.setAttribute("maxlength", "<c:choose><c:when test="${product.customLineCharacter != null}"><c:out value="${product.customLineCharacter}"/></c:when><c:otherwise>10</c:otherwise></c:choose>"); 
	            li.appendChild(textfield);
	            ul.appendChild(li);
	        }
	        document.getElementById('customLineBox').appendChild(ul);
	        
		}
		document.getElementById('customAddToCart').style.display="block";
		return false;
	}
}
</c:if>
function checkQty( pid, boxExtraAmt )
{
<c:if test="${model.boxContentsList != null}" >
    var productList = document.getElementsByName('box_content.id');
    var totalQty = 0;
	for(var i=0; i<productList.length; i++) 
	{
	    var el = document.getElementById("qty_" + productList[i].value);
		if ( el != null )
		{
			if ( checkNumber( el.value ) == 1  )
			{
				totalQty = totalQty + parseInt(el.value);
			}
			else if ( checkNumber( el.value ) == -1 ) 
			{
				alert("invalid Quantity");
				el.focus();
				return false;
			}			
		}
	}
	document.getElementById("box_total_qty").value = totalQty;
	if ( boxExtraAmt == -999 && document.getElementById("box_limit").value != totalQty ) {
		alert("Please add exactly " + document.getElementById("box_limit").value + " items." );
    	return false;
	} else if ( boxExtraAmt != -999 && document.getElementById("box_limit").value > totalQty ){
		alert("Please add " + document.getElementById("box_limit").value + " items or more. ( $" + boxExtraAmt + " extra for each additional item. )" );
    	return false;
	}
</c:if>	
	var product = document.getElementById('quantity_' + pid);
	if ( checkNumber( product.value ) == -1 || product.value == 0 ) {
	    alert("invalid Quantity");
		product.focus();
		return false;
	}
	return true;
}
function checkForm()
{
	var productList = document.getElementsByName('product.id');
	var allQtyEmpty = true;
	for(var i=0; i<productList.length; i++) 
	{
	    var el = document.getElementById("quantity_" + productList[i].value);
		if ( el != null )
		{
			if ( checkNumber( el.value ) == 1  )
			{
				allQtyEmpty = false;
			}
			else if ( checkNumber( el.value ) == -1 ) 
			{
				alert("invalid Quantity");
				el.focus();
				return false;
			}			
		}	
	}
	if ( allQtyEmpty )
	{
		alert("Please Enter Quantity.");
		return false;
	}
	else
		return true;	
}
//-->
</script>


<c:choose>
  <c:when test="${siteConfig['DETAILS_IMAGE_LOCATION'].value == 'left' }">
    <c:set var="details_images_style" value="float:left;padding:3px 15px 15px 3px;" />
    <c:set var="details_desc_style" value="float:left;padding:0 0 0 15px;" />
  </c:when>
  <c:otherwise>
    <c:set var="details_images_style" value="padding:0 0 15px 0;" />
    <c:set var="details_desc_style" value="" />
  </c:otherwise>
</c:choose>

<%-- bread crumbs --%>
<c:import url="/WEB-INF/jsp/frontend/layout/${_template}/breadcrumbs.jsp" />
<c:import url="/WEB-INF/jsp/frontend/etilize/breadcrumbs.jsp" />

<table class="container-details" border="0" cellpadding="0" cellspacing="0">
<tr class="top">
<td class="topLeft"><img src="${_contextpath}/assets/Image/Layout/topleft.gif" border="0" alt="" /></td>
<td class="topTop"></td>
<td class="topRight"><img src="${_contextpath}/assets/Image/Layout/topright.gif" border="0" alt="" /></td>
</tr>
<tr class="middle"><td class="middleLeft"></td>
<td class="middleMiddle">
<!-- details start -->

<div class="details5" style="float:left;">

<c:set var="productImage">
<c:choose>
 <c:when test="${fn:contains(siteConfig['PRODUCT_IMAGE_LAYOUTS'].value,'s2') and 
 		(product.imageLayout == 's2' or (empty product.imageLayout and siteConfig['PRODUCT_IMAGE_LAYOUT'].value == 's2'))}">
    <link rel="stylesheet" href="${_contextpath}/assets/SmoothGallery/slideshow.css" type="text/css" media="screen" />
	<script type="text/javascript" src="${_contextpath}/javascript/slideshow.js"></script>
	<script type="text/javascript">		
	//<![CDATA[
	  window.addEvent('domready', function(){
	    var data = [<c:forEach items="${product.images}" var="image" varStatus="status">'<c:if test="${!image.absolute}">${_contextpath}/assets/Image/Product/detailsbig/</c:if><c:out value="${image.imageUrl}"/>'<c:if test="${! status.last}" >,</c:if></c:forEach> ];
	    var myShow = new Slideshow('show', data, { controller: true, height: 302, hu: '', thumbnails: true ,width:300, resize:'length'});
		});
	//]]>
	</script>
	
	<div id="show" class="slideshow"> </div>
    <div id="slideshow_margin"/>
 </c:when>
 <c:when test="${fn:contains(siteConfig['PRODUCT_IMAGE_LAYOUTS'].value,'mb') and 
 		(param.multibox == null or param.multibox != 'off') and
 		(product.imageLayout == 'mb' or (empty product.imageLayout and siteConfig['PRODUCT_IMAGE_LAYOUT'].value == 'mb'))}">
 <link rel="stylesheet" href="${_contextpath}/assets/SmoothGallery/multibox.css" type="text/css" media="screen" />
 <c:set value="${true}" var="multibox"/>
 <script src="${_contextpath}/javascript/overlay.js" type="text/javascript" ></script>
 <script src="${_contextpath}/javascript/multibox.js" type="text/javascript" ></script>
 <script type="text/javascript">
	window.addEvent('domready', function(){
		var box = new multiBox('mbxwz', {overlay: new overlay()});
	});
 </script>
 	<div class="details_image_box" id="details_image_boxId" style="${details_images_style}
	 <c:if test="${siteConfig['DETAILS_IMAGE_WIDTH'].value != '' }">width: <c:out value="${siteConfig['DETAILS_IMAGE_WIDTH'].value}"/>px;</c:if>">
	 
	<c:forEach items="${model.product.images}" var="image" varStatus="status">
	  <c:if test="${status.first}">
	    <a href="<c:if test="${!image.absolute}">${_contextpath}/assets/Image/Product/detailsbig/</c:if><c:out value="${image.imageUrl}"/>" id="mb0" class="mbxwz" title=""><img src="<c:if test="${!image.absolute}">${_contextpath}/assets/Image/Product/detailsbig/</c:if><c:out value="${image.imageUrl}"/>" border="0" style="<c:if test="${siteConfig['DETAILS_IMAGE_WIDTH'].value != '' }">width: <c:out value="${siteConfig['DETAILS_IMAGE_WIDTH'].value}"/>px</c:if>" alt="<c:out value="${product.name}"/><c:if test="${product.shortDesc != ''}"> - <c:out value="${product.shortDesc}"/></c:if>"/></a>
	  </c:if>
	</c:forEach>
    <div style="clear: both;" >
    <c:forEach items="${model.product.images}" var="image" varStatus="status">
	  <c:if test="${status.count > 1}">
	      <a href="<c:if test="${!image.absolute}">${_contextpath}/assets/Image/Product/detailsbig/</c:if><c:out value="${image.imageUrl}"/>" id="mb${status.count - 1}" class="mbxwz" title=""><img src="<c:if test="${!image.absolute}">${_contextpath}/assets/Image/Product/detailsbig/</c:if><c:out value="${image.imageUrl}"/>" border="0" class="details_thumbnail" alt="<c:out value="${product.name}"/><c:if test="${product.shortDesc != ''}"> - <c:out value="${product.shortDesc}"/></c:if>"/></a>
	  </c:if>
	</c:forEach>
	</div>
	</div>        
 </c:when>
 <c:when test="${fn:contains(siteConfig['PRODUCT_IMAGE_LAYOUTS'].value,'qb') and 
 		(product.imageLayout == 'qb' or (empty product.imageLayout and siteConfig['PRODUCT_IMAGE_LAYOUT'].value == 'qb'))}">
 <link rel="stylesheet" href="${_contextpath}/assets/SmoothGallery/QuickBox/quickbox.css" type="text/css" media="screen" />
 <c:set value="${true}" var="quickbox"/>
 <script src="${_contextpath}/javascript/QuickBox.js" type="text/javascript" ></script>
 <script type="text/javascript">
	window.addEvent('domready', function(){
		new QuickBox();
	});
 </script>
 	<div class="details_image_box" id="details_image_boxId" style="${details_images_style}
	 <c:if test="${siteConfig['DETAILS_IMAGE_WIDTH'].value != '' }">width: <c:out value="${siteConfig['DETAILS_IMAGE_WIDTH'].value}"/>px;</c:if>">
	 
	<c:forEach items="${model.product.images}" var="image" varStatus="status">
	  <c:if test="${status.first}">
	    <a href="<c:if test="${!image.absolute}">${_contextpath}/assets/Image/Product/detailsbig/</c:if><c:out value="${image.imageUrl}"/>" id="" class="" title="" rel="quickbox"><img src="<c:if test="${!image.absolute}">${_contextpath}/assets/Image/Product/detailsbig/</c:if><c:out value="${image.imageUrl}"/>" border="0" style="<c:if test="${siteConfig['DETAILS_IMAGE_WIDTH'].value != '' }">width: <c:out value="${siteConfig['DETAILS_IMAGE_WIDTH'].value}"/>px</c:if>" alt="<c:out value="${product.name}"/><c:if test="${product.shortDesc != ''}"> - <c:out value="${product.shortDesc}"/></c:if>"/></a>
	  </c:if>
	</c:forEach>
    <div style="clear: both;" >
    <c:forEach items="${model.product.images}" var="image" varStatus="status">
	  <c:if test="${status.count > 1}">
	      <a href="<c:if test="${!image.absolute}">${_contextpath}/assets/Image/Product/detailsbig/</c:if><c:out value="${image.imageUrl}"/>" id="qb${status.count - 1}" class="" title="" rel="quickbox"><img src="<c:if test="${!image.absolute}">${_contextpath}/assets/Image/Product/detailsbig/</c:if><c:out value="${image.imageUrl}"/>" border="0" class="details_thumbnail" alt="<c:out value="${product.name}"/><c:if test="${product.shortDesc != ''}"> - <c:out value="${product.shortDesc}"/></c:if>"/></a>
	  </c:if>
	</c:forEach>
	</div>
	</div>        
 </c:when>
 <c:otherwise>
    <div class="details_image_box" id="details_image_boxId" style="${details_images_style}
	 <c:if test="${siteConfig['DETAILS_IMAGE_WIDTH'].value != '' }">width: <c:out value="${siteConfig['DETAILS_IMAGE_WIDTH'].value}"/>px;</c:if>">
	 
	<c:forEach items="${model.product.images}" var="image" varStatus="status">
	<c:if test="${status.first}">
		 <img src="<c:if test="${!image.absolute}">${_contextpath}/assets/Image/Product/detailsbig/</c:if><c:out value="${image.imageUrl}"/>" border="0" id="_image" name="_image" class="details_image"
		 	style="<c:if test="${siteConfig['DETAILS_IMAGE_WIDTH'].value != '' }">width: <c:out value="${siteConfig['DETAILS_IMAGE_WIDTH'].value}"/>px</c:if>" alt="<c:out value="${product.name}"/><c:if test="${product.shortDesc != ''}"> - <c:out value="${product.shortDesc}"/></c:if>">
	<c:if test="${siteConfig['DETAILS_IMAGE_WIDTH'].value != '' }" >
	<div align="right">
	<a onclick="zoomIn()" title="Zoom in"><img src="${_contextpath}/assets/Image/Layout/zoom-icon.gif" alt="Zoom In" width="16" height="16" /></a>
	</div>
	</c:if>
	</c:if>
	
	<c:if test="${status.last and (status.count != 1)}">
	<br>
	<c:forEach items="${model.product.images}" var="thumb">
	<a href="#" class="details_thumbnail_anchor" onClick="_image.src='<c:if test="${!thumb.absolute}">${_contextpath}/assets/Image/Product/detailsbig/</c:if><c:out value="${thumb.imageUrl}"/>';return false;">
		 <img src="<c:if test="${!thumb.absolute}">${_contextpath}/assets/Image/Product/detailsbig/</c:if><c:out value="${thumb.imageUrl}"/>" class="details_thumbnail" alt="<c:out value="${product.name}"/><c:if test="${product.shortDesc != ''}"> - <c:out value="${product.shortDesc}"/></c:if>">
	</a>
	</c:forEach>
	</c:if>
	</c:forEach>
	</div>
	<c:if test="${fn:contains(siteConfig['PRODUCT_IMAGE_LAYOUTS'].value,'ss') and 
			(product.imageLayout == 'ss'  or (empty product.imageLayout and siteConfig['PRODUCT_IMAGE_LAYOUT'].value == 'ss'))}">
	<object classid="clsid:D27CDB6E-AE6D-11cf-96B8-444553540000" codebase="http://download.macromedia.com/pub/shockwave/cabs/flash/swflash.cab#version=6,0,29,0" width="750" height="475">
	  <param name="movie" value="${_contextpath}/assets/Flash/slideshow.swf?file=thumbnailer1.jhtm?id=${product.id}">
	  <param name="quality" value="high">
	  <embed src="${_contextpath}/assets/Flash/slideshow.swf?file=thumbnailer1.jhtm?id=${product.id}" quality="high" pluginspage="http://www.macromedia.com/go/getflashplayer" type="application/x-shockwave-flash" width="750" height="475"></embed>
	</object>
	</c:if>
 </c:otherwise>
</c:choose>
</c:set>
<c:if test="${siteConfig['DETAILS_IMAGE_LOCATION'].value != 'middle'}">
<c:out value="${productImage}" escapeXml="false" />
</c:if>

<div style="${details_desc_style}" class="details_desc">

<c:if test="${siteConfig['SHOW_SKU'].value == 'true'}">
<c:if test="${product.sku != null}">
<div class="details_sku"><c:out value="${product.sku}" /></div>  
</c:if>
</c:if>

<div class="details_item_name"><h1><c:out value="${product.name}" escapeXml="false" /></h1></div>

<c:if test="${siteConfig['DETAILS_SHORT_DESC'].value == 'true' and product.shortDesc != ''}">
<div class="details_short_desc"><c:out value="${product.shortDesc}" escapeXml="false" /></div>  
</c:if>

<c:if test="${gSiteConfig['gBUDGET_PRODUCT']}">
<div class="budget_balance"><fmt:message key="balance" />: <c:choose><c:when test="${model.budgetProduct[product.sku] != null}"><c:out value="${model.budgetProduct[product.sku].balance}"/></c:when><c:otherwise>0</c:otherwise></c:choose></div>
</c:if>

<c:if test="${siteConfig['DETAILS_LONG_DESC_LOCATION'].value == 'above' and product.longDesc != ''}">
<div class="details_long_desc"><c:out value="${product.longDesc}" escapeXml="false" /></div> 
</c:if>

<c:if test="${siteConfig['DETAILS_IMAGE_LOCATION'].value != 'middle'}">
<c:choose>
<c:when test="${product.salesTag != null and !product.salesTag.percent}">
  <%@ include file="/WEB-INF/jsp/frontend/common/price3.jsp" %>
</c:when>
<c:otherwise>
  <%@ include file="/WEB-INF/jsp/frontend/common/price.jsp" %>
</c:otherwise>
</c:choose>
</c:if>
<c:if test="${siteConfig['GOOGLE_CURRENCY_CONVERTER'].value == 'true'}">
<c:if test="${!multibox}">
<link rel="stylesheet" href="${_contextpath}/assets/SmoothGallery/multibox.css" type="text/css" media="screen" />
 <script src="${_contextpath}/javascript/overlay.js" type="text/javascript" ></script>
 <script src="${_contextpath}/javascript/multibox.js" type="text/javascript" ></script>
</c:if>
<script type="text/javascript">
window.addEvent('domready', function(){			
	var boxCurrConv = new multiBox('currConv', {descClassName: 'multiBoxDesc'});
});
</script>
<a id="currConv1" class="currConv" title="Currency Converter" rel="width:350,height:200" href="http://www.google.com/finance/converter?a=${price1}&from=USD&to=AUD">Currency</a>
</c:if>

<c:if test="${gSiteConfig['gDEALS'] > 0 and product.deal.htmlCode != null}">
	<c:out value="${product.deal.htmlCode}" escapeXml="false"/>
</c:if>
<c:if test="${gSiteConfig['gDEALS'] > 0 and product.deal.parentHtmlCode != null}">
	<c:out value="${product.deal.parentHtmlCode}" escapeXml="false"/>
</c:if>

<c:choose>
<c:when test="${gSiteConfig['gPRODUCT_REVIEW'] and product.enableRate}">
<%@ include file="/WEB-INF/jsp/frontend/common/quickProductReview.jsp" %>
</c:when>
<c:when test="${siteConfig['PRODUCT_RATE'].value == 'true' and product.enableRate}">
<%@ include file="/WEB-INF/jsp/frontend/common/productRate.jsp" %>
</c:when>
</c:choose>

<c:if test="${model.boxContentsList == null}" >
<form action="${_contextpath}/addToCart.jhtm" name="this_form" method="post" onsubmit="return checkForm()">
<input type="hidden" name="product.id" value="${product.id}">

<c:if test="${siteConfig['DETAILS_IMAGE_LOCATION'].value == 'middle'}">
<div class="details_hdr">Selectable Options</div>
<table width="100%">
<tr>
<td valign="top" align="center" width="100%" class="detailsImageBox">
<c:out value="${productImage}" escapeXml="false" />
</td>
<td valign="top">
<%@ include file="/WEB-INF/jsp/frontend/common/productOptions2.jsp" %>
</td>
</tr>
</table>
<%@ include file="/WEB-INF/jsp/frontend/common/productOptionsSize.jsp" %>
</c:if>

<c:if test="${siteConfig['DETAILS_IMAGE_LOCATION'].value != 'middle'}">
  <c:choose>
	<c:when test="${siteConfig['SMART_DROPDOWN'].value == 'true'}">
	  <%@ include file="/WEB-INF/jsp/frontend/common/productOptions5.jsp" %>
	</c:when>
	<c:otherwise>
	  <%@ include file="/WEB-INF/jsp/frontend/common/productOptions.jsp" %>
	</c:otherwise>
  </c:choose>
</c:if>

<c:if test="${product.subscription and gSiteConfig['gSUBSCRIPTION']}">
<jsp:include page="/WEB-INF/jsp/frontend/common/subscriptions.jsp">
  <jsp:param name="productId" value="${product.id}" />
  <jsp:param name="subscriptionDiscount" value="${product.subscriptionDiscount}" />
</jsp:include>
</c:if>

<c:if test="${ (gSiteConfig['gSHOPPING_CART'] and (!empty product.price or product.priceByCustomer)) and (!product.loginRequire or userSession != null)}">
<div class="details_hdr">
<table>
 <tr valign="middle">                                                                                                   
 <c:choose>
   <c:when test="${gSiteConfig['gVARIABLE_PRICE'] and product.priceByCustomer}">   
     <td colspan="2">
       <input type="hidden" name="quantity_${product.id}" id="quantity_${product.id}" value="1">
       <input type="image" border="0" name="_addtocart" class="_addtocart" src="${_contextpath}/assets/Image/Layout/button_addtocart${_lang}.gif" />
     </td>
   </c:when>
   <c:otherwise>
     <c:set var="zeroPrice" value="false"/>
	 <c:forEach items="${product.price}" var="price">
	   <c:if test="${price.amt == 0}">
     	<c:set var="zeroPrice" value="true"/>
	   </c:if>
 	 </c:forEach> 
     <c:choose>
	   <c:when test="${gSiteConfig['gINVENTORY'] and product.inventory != null and !product.negInventory and product.inventory <= 0}" >
	     <td><c:out value="${siteConfig['OUT_OF_STOCK_TITLE'].value}" escapeXml="false"/></td> 
	   </c:when>
	   <c:when test="${zeroPrice and siteConfig['ZERO_PRICE_HTML'].value != ''}">
	     <td><c:out value="${siteConfig['ZERO_PRICE_HTML'].value}" escapeXml="false"/></td>
	   </c:when>
	   <c:otherwise>
	       <td class="quantityBlock" style="white-space: nowrap">
	         <div class="quantity"><fmt:message key="quantity" />: <input type="text" size="6" maxlength="5" name="quantity_${product.id}" id="quantity_${product.id}" value=""><c:if test="${product.packing != ''}"> <c:out value="${product.packing}"/> </c:if></div>
	       </td>
	       <c:choose>
	         <c:when test="${!empty product.numCustomLines}">
			   <td><input type="image" id="continueButton" border="0" src="${_contextpath}/assets/Image/Layout/button_continue.gif" onClick="return continueButton(${product.id})" ></td>
			 </c:when>
			 <c:otherwise>
	           <td><input type="image" border="0" name="_addtocart" class="_addtocart" src="${_contextpath}/assets/Image/Layout/button_addtocart${_lang}.gif" onClick="return checkQty(${product.id})" /></td>
	         </c:otherwise>
	       </c:choose>
	        <c:if test="${gSiteConfig['gINVENTORY'] and product.inventory != null and product.inventory < product.lowInventory}">
	         <td><c:out value="${siteConfig['LOW_STOCK_TITLE'].value}" escapeXml="false"/></td> 
	        </c:if>      
	   </c:otherwise>
	 </c:choose> 
   </c:otherwise>
 </c:choose>                                                                                          
 </tr>
 <c:if test="${gSiteConfig['gINVENTORY'] and product.inventory != null and !empty siteConfig['INVENTORY_ON_HAND'].value}" >
   <tr class="inventory_onhand_wrapper">
     <td><div class="inventory_onhand"><c:out value="${siteConfig['INVENTORY_ON_HAND'].value}" escapeXml="false"/>&nbsp;<span><c:out value="${product.inventory}" /></span></div></td>     
   </tr>
 </c:if>
</table>  
</div>
<c:if test="${!empty product.numCustomLines}">
<div id="customLineBox" style="display:none">
   <div align="center">Inscription 1 <div class="applyToAll">Apply To All</div></div>
   <ul>   
     <c:forEach begin="1" end="${product.numCustomLines}" var="numCustomLine" >
	   <li>
         Line# ${numCustomLine}.<input type="text" style="text-align:center;" id="line_1_${numCustomLine}" name="line_1_${numCustomLine}" size="50" maxlength="<c:choose><c:when test="${product.customLineCharacter != null}"><c:out value="${product.customLineCharacter}"/></c:when><c:otherwise>10</c:otherwise></c:choose>">
          <input type="checkbox" onclick="copyCustomLine(${numCustomLine}, ${product.id}, this)"/>
       </li>
     </c:forEach>
   </ul>  
</div> 
<div id="customAddToCart" style="display:none;">
  <input type="image" border="0" name="_addtocart" class="_addtocart" src="${_contextpath}/assets/Image/Layout/button_addtocart${_lang}.gif" />
</div> 
</c:if>
</c:if>
</form>
</c:if>

<c:if test="${gSiteConfig['gMYLIST'] && product.addToList}">
<form action="${_contextpath}/addToList.jhtm">
<input type="hidden" name="product.id" value="${product.id}">
<div id="addToList">
<input type="image" border="0" src="${_contextpath}/assets/Image/Layout/button_addtolist${_lang}.gif">
</div>
</form>
</c:if>

<c:if test="${gSiteConfig['gCOMPARISON'] && product.compare}">
<form action="${_contextpath}/comparison.jhtm" method="get">
<%-- 
<input type="hidden" name="forwardAction" value="<c:out value='${model.url}'/>">
--%>
<input type="hidden" name="product.id" value="${product.id}">
<input type="image" border="0" src="${_contextpath}/assets/Image/Layout/button_compare${_lang}.gif">
</form>
</c:if>

<c:if test="${siteConfig['DETAILS_LONG_DESC_LOCATION'].value != 'above' and product.longDesc != ''}">
<div class="details_long_desc"><c:out value="${product.longDesc}" escapeXml="false" /></div>  
</c:if>
            
<c:forEach items="${product.productFields}" var="productField" varStatus="row">
<c:if test="${row.first}">
<c:if test="${siteConfig['PRODUCT_FIELDS_TITLE'].value != ''}">
<div class="details_fields_title"><c:out value="${siteConfig['PRODUCT_FIELDS_TITLE'].value}"/></div>
</c:if>
<table class="details_fields">
</c:if>
<tr>
<td class="details_field_name_row${row.index % 2}"><c:out value="${productField.name}" escapeXml="false" /> </td>
<c:choose>
<c:when test="${productField.formatType != null and productField.formatType != '' and productField.fieldType == 'number' }">
<td class="details_field_value_row${row.index % 2}"><fmt:formatNumber value="${wj:trimAndSplit(productField.value, '|' , false, ',', true, true)}" pattern="${productField.formatType }" /></td>
</c:when>
<c:otherwise>
<td class="details_field_value_row${row.index % 2}"><c:out value="${wj:trimAndSplit(productField.value, '|' , false, ',', true, true)}" escapeXml="false"/></td>
</c:otherwise>
</c:choose>
</tr>
<c:if test="${row.last}">
</table>
</c:if>
</c:forEach>

</div>
</div>
<!-- details end -->
</td><td class="middleRight"></td>
</tr>
<tr class="bottom">
<td class="bottomLeft"><img src="${_contextpath}/assets/Image/Layout/bottomleft.gif"  border="0" alt="" /></td>
<td class="bottomBottom"></td>
<td class="bottomRight"><img src="${_contextpath}/assets/Image/Layout/bottomright.gif"  border="0" alt="" /></td>
</tr>
</table>

<%@ include file="/WEB-INF/jsp/frontend/common/productTabs.jsp" %>

<c:if test="${model.alsoConsiderMap[product.id] != null}">
   <c:set var="product" value="${model.alsoConsiderMap[product.id]}" />
   <%@ include file="/WEB-INF/jsp/frontend/common/alsoconsider.jsp" %>
   <c:set value="${model.product}" var="product"/>
</c:if>

<c:if test="${model.boxContentsList != null}">
<input type="hidden" id="box_limit" value="${model.boxSize}" />
<div style="float:left;width:100%">
<form action="${_contextpath}/addToCart.jhtm" method="post" onsubmit="return checkForm()">
<%@ include file="/WEB-INF/jsp/frontend/common/subscriptions.jsp" %>
<input type="hidden" name="product.id" value="${product.id}">
<%@ include file="/WEB-INF/jsp/frontend/common/productOptions.jsp" %>
<div class="box_content_body">
<div class="box_contents">Box Contents...
<c:choose>
 <c:when test="${model.boxErrorMessage != null}">
   <div class="message"><c:out value="${model.boxErrorMessage}" /></div>
 </c:when>
 <c:otherwise>
   <c:choose>
     <c:when test="${product.boxExtraAmt != null}">
       <fmt:message key="boxContentExtraMessage"><fmt:param value="${model.boxSize}"/><fmt:param value="${product.boxExtraAmt}"/></fmt:message>
     </c:when>
     <c:otherwise>
       <fmt:message key="boxContentMessage"><fmt:param value="${model.boxSize}"/></fmt:message>
     </c:otherwise>
   </c:choose>  
 </c:otherwise>
</c:choose>
</div>
<c:if test="${ (gSiteConfig['gSHOPPING_CART'] and !empty product.price) and (!product.loginRequire or userSession != null)}">
 <table>
 <tr valign="middle"> 
   <td>
    <input type="hidden" size="6" maxlength="5" name="quantity_${product.id}" id="quantity_${product.id}" value="1" />
    <input type="image" border="0" name="_addtocart" class="_addtocart" src="${_contextpath}/assets/Image/Layout/button_addtocart${_lang}.gif" onClick="return checkQty(${product.id},<c:choose><c:when test="${empty product.boxExtraAmt}">-999</c:when><c:otherwise><c:out value="${product.boxExtraAmt}"/></c:otherwise></c:choose> )" />
   </td>
 </tr>
</table> 
</c:if>
<%@ include file="/WEB-INF/jsp/frontend/common/productOptionsBox.jsp" %>
<c:if test="${ (gSiteConfig['gSHOPPING_CART'] and !empty product.price) and (!product.loginRequire or userSession != null)}">
 <table>
 <tr valign="middle"> 
   <td>
    <input type="hidden" size="6" maxlength="5" name="quantity_${product.id}" id="quantity_${product.id}" value="1" />
    <input type="image" border="0" src="${_contextpath}/assets/Image/Layout/button_addtocart${_lang}.gif" onClick="return checkQty(${product.id},<c:choose><c:when test="${empty product.boxExtraAmt}">-999</c:when><c:otherwise><c:out value="${product.boxExtraAmt}"/></c:otherwise></c:choose> )" />
   </td>
 </tr>
</table> 
</c:if>
</div>
</form>
</div>
</c:if>



<c:set var="recommendedList_HTML">
<c:if test="${model.recommendedList != null}">
<div style="float:left;width:100%">
<c:choose>
 <c:when test="${!empty product.recommendedListTitle }">
  <div class="recommended_list"><c:out value="${product.recommendedListTitle}"/></div>
 </c:when>
 <c:otherwise>
  <c:if test="${siteConfig['RECOMMENDED_LIST_TITLE'].value != ''}">
   <div class="recommended_list"><c:out value="${siteConfig['RECOMMENDED_LIST_TITLE'].value}"/></div>
  </c:if>
 </c:otherwise>
</c:choose>
<c:set var="numCul" value="${siteConfig['NUMBER_PRODUCT_PER_ROW'].value}" />
<c:forEach items="${model.recommendedList}" var="product" varStatus="status">

<c:set var="productLink">${_contextpath}/product.jhtm?id=${product.id}<c:if test="${model.product == null or siteConfig['RECOMMENDED_LIST_SHOW_BREADCRUMB'].value == 'true'}">&cid=<c:out value="${param.cid}"/></c:if></c:set>
<c:if test="${gSiteConfig['gMOD_REWRITE'] == '1' and not fn:contains(product.sku, '/')}">
  <c:set var="productLink">${_contextpath}/${siteConfig['MOD_REWRITE_PRODUCT'].value}/${product.encodedSku}/${product.encodedName}.html<c:if test="${model.product == null or siteConfig['RECOMMENDED_LIST_SHOW_BREADCRUMB'].value == 'true'}">?cid=<c:out value="${param.cid}"/></c:if></c:set>
</c:if>

<c:choose>
 <c:when test="${( model.product.recommendedListDisplay == 'quick') or (empty model.product.recommendedListDisplay and siteConfig['RECOMMENDED_LIST_DISPLAY'].value == 'quick')}">
    <%@ include file="/WEB-INF/jsp/frontend/quickmode/view1.jsp" %>
 </c:when>
 <c:when test="${( model.product.recommendedListDisplay == '2') or (empty model.product.recommendedListDisplay and siteConfig['RECOMMENDED_LIST_DISPLAY'].value == '2')}">
    <%@ include file="/WEB-INF/jsp/frontend/thumbnail/view2.jsp" %>
 </c:when>
 <c:when test="${( model.product.recommendedListDisplay == '3') or (empty model.product.recommendedListDisplay and siteConfig['RECOMMENDED_LIST_DISPLAY'].value == '3')}">
	<%@ include file="/WEB-INF/jsp/frontend/thumbnail/view3.jsp" %>
 </c:when>
 <c:when test="${( model.product.recommendedListDisplay == '4') or (empty model.product.recommendedListDisplay and siteConfig['RECOMMENDED_LIST_DISPLAY'].value == '4')}">
	<%@ include file="/WEB-INF/jsp/frontend/thumbnail/view4.jsp" %>
 </c:when>
 <c:when test="${( model.product.recommendedListDisplay == '5') or (empty model.product.recommendedListDisplay and siteConfig['RECOMMENDED_LIST_DISPLAY'].value == '5')}">
	<%@ include file="/WEB-INF/jsp/frontend/thumbnail/view5.jsp" %>
 </c:when>
 <c:when test="${( model.product.recommendedListDisplay == '6') or (empty model.product.recommendedListDisplay and siteConfig['RECOMMENDED_LIST_DISPLAY'].value == '6')}">
	<%@ include file="/WEB-INF/jsp/frontend/thumbnail/view6.jsp" %>
 </c:when>
 <c:when test="${( model.product.recommendedListDisplay == '7') or (empty model.product.recommendedListDisplay and siteConfig['RECOMMENDED_LIST_DISPLAY'].value == '7')}">
	<%@ include file="/WEB-INF/jsp/frontend/thumbnail/view7.jsp" %>
 </c:when>
 <c:when test="${( model.product.recommendedListDisplay == '8') or (empty model.product.recommendedListDisplay and siteConfig['RECOMMENDED_LIST_DISPLAY'].value == '8')}">
	<%@ include file="/WEB-INF/jsp/frontend/thumbnail/view8.jsp" %>
 </c:when>
 <c:when test="${( model.product.recommendedListDisplay == '9') or (empty model.product.recommendedListDisplay and siteConfig['RECOMMENDED_LIST_DISPLAY'].value == '9')}">
	<%@ include file="/WEB-INF/jsp/frontend/thumbnail/view9.jsp" %>
 </c:when>
 <c:when test="${( model.product.recommendedListDisplay == '10') or (empty model.product.recommendedListDisplay and siteConfig['RECOMMENDED_LIST_DISPLAY'].value == '10')}">
    <%@ include file="/WEB-INF/jsp/frontend/thumbnail/view10.jsp" %>
 </c:when>
 <c:when test="${( model.product.recommendedListDisplay == '11') or (empty model.product.recommendedListDisplay and siteConfig['RECOMMENDED_LIST_DISPLAY'].value == '11')}">
    <%@ include file="/WEB-INF/jsp/frontend/thumbnail/view11.jsp" %>
 </c:when>
 <c:when test="${( model.product.recommendedListDisplay == '12') or (empty model.product.recommendedListDisplay and siteConfig['RECOMMENDED_LIST_DISPLAY'].value == '12')}">
    <%@ include file="/WEB-INF/jsp/frontend/thumbnail/view12.jsp" %>
 </c:when>
 <c:otherwise>
	<%@ include file="/WEB-INF/jsp/frontend/thumbnail/view1.jsp" %>
 </c:otherwise>
</c:choose>
</c:forEach>
</div>
</c:if>
</c:set>

<c:if test="${model.slavesList == null and gSiteConfig['gMASTER_SKU'] and empty product.masterSku}">
<c:if test="${siteConfig['MASTERSKU_OUT_OF_STOCK_URL'].value != ''}">
<c:catch var="masterSkuUrlException">
<c:import url="${siteConfig['MASTERSKU_OUT_OF_STOCK_URL'].value}"/>
</c:catch>
</c:if>
<c:if test="${siteConfig['MASTERSKU_OUT_OF_STOCK_URL'].value == '' or masterSkuUrlException != null}">
<div style="float:left;width:100%">
  <div class="masterSku_out_of_stock">OUT OF STOCK</div>
</div>
</c:if>
</c:if>
<c:if test="${model.slavesList != null}">

<c:set value="true" var="hideLink"/>
<c:set value="false" var="showImage"/>
<c:set value="false" var="showDesc"/>
<div style="float:left;width:100%">
<c:if test="${!empty model.slavesProductFields}" >
<form method="post">
<div id="dropDownList_main" >
<p id="dropDownList_title">Narrow Results By:</p>
<p class="dropDownList_p1">
<c:forEach items="${model.slavesProductFields}" var="productField">
<c:set value="field_${productField.id}" var="field"/>
<c:if test="${productField.quickModeField}">
<select name="field_${productField.id}" class="dropDownList_select" onchange="submit();">
  <option value=""><c:out value="${productField.name}"/> (ALL)</option>
  <c:forEach items="${productField.values}" var="value">
  <option value="<c:out value="${value}"/>" <c:if test="${model.slavesSearch.productFieldMap[field].value == value}">selected</c:if>><c:out value="${value}"/></option>
  </c:forEach>
</select>
</c:if>
</c:forEach>
</p>
</div>
</form>
</c:if>
<c:forEach items="${model.slavesList}" var="product" varStatus="status">
  <%@ include file="/WEB-INF/jsp/frontend/quickmode/view1.jsp" %>
</c:forEach>
</div>
</c:if>
<c:out value="${recommendedList_HTML}" escapeXml="false" />


<c:if test="${gSiteConfig['gPRODUCT_REVIEW'] and siteConfig['PRODUCT_RATE'].value != 'true' and model.product.enableRate}">
<div style="float:left;width:100%">
<%@ include file="/WEB-INF/jsp/frontend/common/productReviewList.jsp" %>
</div>
</c:if>
</c:if>

<c:if test="${fn:trim(model.productLayout.footerHtml) != ''}">
  <c:set value="${model.productLayout.footerHtml}" var="footerHtml"/>
  <%@ include file="/WEB-INF/jsp/frontend/common/footerDynamicElement.jsp" %>
  <div style="float:left;width:100%"><c:out value="${footerHtml}" escapeXml="false"/></div>
</c:if>
    
  </tiles:putAttribute>
</tiles:insertDefinition>