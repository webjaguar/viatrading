<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="/WEB-INF/tlds/spring.tld" prefix="spring" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>

<tiles:insertDefinition name="${_template}" flush="true">
<tiles:putAttribute name="content" type="string">

<div class="orderLookupHeader"></div>  
<form action="orderLookup.jhtm" method="post">
  <table border="0" cellspacing="0" cellpadding="5" align="center">
  <tr align="center">
  	<td id="title"><fmt:message key="f_customerPO" />: </td>   
    <td>
    	<input type="text" name="customerPO" value="<c:out value="${model.customerPO}"/>" size="15">
    </td>
    <td colspan="2" align="center">
    	<input type="submit" name="_find" value="Find" />
    	<input type="button" value="Reset" onclick="window.location='orderLookup.jhtm'">
    </td>
     <!-- Error Message -->
	  <c:if test="${model.message != null}">
		  <div class="message"><fmt:message key="${model.message}"/></div>
	  </c:if>
  </tr>
  <tr><td>&nbsp;</td></tr>
  </table>
  <table border="0" cellpadding="3" cellspacing="1" width="100%" class="listings">
  <tr class="listingsHdr2">
  	<td class="nameCol"><fmt:message key="f_customerPO" /> #</td>
  	<td class="nameCol"><fmt:message key="item"/> #</td>
  	<td class="nameCol"><fmt:message key="accountNumber" /> #</td>
  	<td class="nameCol"><fmt:message key="accountName"/> #</td>
  	<td class="nameCol"><fmt:message key="date"/> <fmt:message key="shipped"/></td>
  	<td class="nameCol"><fmt:message key="f_trackingNumber"/></td>
  	<td class="nameCol"># Of Ctns</td>
  	<td class="nameCol"><fmt:message key="totalWeight" /></td>
  	<td class="nameCol">Type</td>
  	<td class="nameCol">Truck Company</td>
  </tr>
  
<c:forEach items="${model.orderLookup}" var="order">
  <tr class="row${status.index % 2}">
    <td class="nameCol"><c:out value="${order['customerPO']}"/></td>
    <td class="nameCol"><c:out value="${order['lineItems']}"/></td>
    <td class="nameCol"><c:out value="${order['accountNumber']}"/></td>
    <td class="nameCol"><c:out value="${order['companyName']}"/></td>
    <td class="nameCol"><fmt:formatDate type="date" timeStyle="default" value="${order['orderShipDate']}"/></td>
    <td class="nameCol">
    <c:choose>
  	  <c:when test="${fn:containsIgnoreCase(order['shippingCompany'],'Third') ||fn:containsIgnoreCase(order['shippingCompany'],'Collect') || fn:containsIgnoreCase(order['shippingCompany'],'Prepaid')}">
	  	 <c:out value="${order['trackingNumber']}" />
      </c:when>
  	  <c:when test="${fn:containsIgnoreCase(order['shippingCompany'],'FedEx')}">
	  	 <a href="http://fedex.com/Tracking?action=track&language=english&cntry_code=us&mps=y&tracknumbers=<c:out value="${order['trackingNumber']}" />&ascend_header=1"><c:out value="${order['trackingNumber']}" /></a>
       </c:when>
      <c:otherwise>
          <a href="http://wwwapps.ups.com/WebTracking/processInputRequest?HTMLVersion=5.0&sort_by=status&tracknums_displayed=10&TypeOfInquiryNumber=T&loc=en_US&InquiryNumber1=<c:out value="${order['trackingNumber']}"/>&AgreeToTermsAndConditions=yes"><c:out value="${order['trackingNumber']}"/></a>
       </c:otherwise>
  	</c:choose>
  	</td>
    <td class="nameCol"><c:out value="${order['numberPackages']}"/></td>
    <td class="nameCol"><c:out value="${order['weight']}"/></td>
    <td class="nameCol"><c:out value="${order['shippingCompany']}"/></td>
    <td class="nameCol"><c:out value="${order['truckCompany']}"/></td>				
  </tr>   
</c:forEach>
  </table> 
</form>  

</tiles:putAttribute>
</tiles:insertDefinition>