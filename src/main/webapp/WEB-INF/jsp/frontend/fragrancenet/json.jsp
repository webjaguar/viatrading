<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ taglib prefix="sql" uri="http://java.sun.com/jstl/sql" %>
<sql:setDataSource
        driver="com.mysql.jdbc.Driver"
        url="jdbc:mysql://localhost/webjaguar_webapp"
        user="mytradez_webapp"
        password="webjaguar1"
/>
<sql:query var="result">
  SELECT
        field_4
  FROM
        product
        <c:if test="${param.category > 0}">
         , category_product
        </c:if>
  WHERE
        feed = 'FRAGRANCENET'
        and active
        <c:if test="${param.category > 0}">
          and id = category_product.product_id
          and category_id = ${param.category}
        </c:if>
        <c:if test="${param.letter != 'num'}">
          and field_4 like '${param.letter}%'
        </c:if>
        <c:if test="${param.letter == 'num'}">
          and ABS(field_4) > 0
        </c:if>
  GROUP BY
  		field_4
  ORDER BY
        field_4
</sql:query>

{"links":[
<c:forEach items="${result.rows}" var="brand" varStatus="status">
	{"name":"<c:out value="${fn:replace(brand.field_4,'\"', '')}" escapeXml="false" />", "link":"<c:out value="${brand.field_4}" />"}
	<c:if test="${not status.last}">,</c:if>	
</c:forEach>
]}




<%-- 

<style type="text/css">
	#nav a {
		text-decoration: none;
		font-size: 11px;
		font-family: Verdana, Arial, Helvetica, sans-serif; 
		color: #993399;
		border-bottom: #E3E3E3 1px solid;
		display: block;
		padding: 3px;
	}
	#nav a:hover {
		text-decoration: underline;
	}
	#nav span {
		font-size: 14px;
		font-family: Verdana, Arial, Helvetica, sans-serif; 
		color: #0C6D9F;
		display: block;
		padding: 3px;
		font-weight: bold;
		background-color: #D9E2FF;
	}
</style>
<script type="text/javascript" src="javascript/mootools-1.2.5-core.js"></script>
<script language="JavaScript">

function syncProduct(letter) {
		if (letter == '#') letter = 'num';
		var request = new Request.JSON({
			url: "json.jsp?category=2&letter=" + letter,
			onRequest: function() { 
			    $("nav").empty();
				var img = document.createElement("img");
         		img.setAttribute("src", "assets/Image/Layout/spinner.gif");
         		document.getElementById("nav").appendChild(img); 
			},
			onComplete: function(jsonObj) {
				if (letter == 'num') letter = '#';
			    $("nav").empty();
				var span = document.createElement("span");
	           	var cellText = document.createTextNode(letter);
	           	span.appendChild(cellText);
	           	document.getElementById("nav").appendChild(span);
				jsonObj.links.each(function(link) {
					var div = document.createElement("div");
					var a = document.createElement("a");
	           		var cellText = document.createTextNode(link.name);
	           		a.setAttribute("href", "search.jhtm?field_5=WOMEN&field_4=" + link.link);
	            	a.appendChild(cellText);
	            	div.appendChild(a);
					document.getElementById("nav").appendChild(div);
				});
			}
		}).send();
}
window.onload = function() {
  syncProduct('A');
}
</script>

<img id="letters" class="" width="196" height="62" border="0" style="cursor: pointer;" usemap="#js_letters" alt="" src="http://www.fragrancenet.com//images/sn.letters.gif"/>
<map name="js_letters">
	<area shape="rect" alt="a" title="a" nohref onclick="syncProduct('A');" coords="5,6,24,21" />
	<area shape="rect" alt="b" title="b" nohref onclick="syncProduct('B');" coords="26,6,45,21" />
	<area shape="rect" alt="c" title="c" nohref onclick="syncProduct('C');" coords="47,6,66,21" />
	<area shape="rect" alt="d" title="d" nohref onclick="syncProduct('D');" coords="68,6,87,21" />
	<area shape="rect" alt="e" title="e" nohref onclick="syncProduct('E');" coords="89,6,108,21" />
	<area shape="rect" alt="f" title="f" nohref onclick="syncProduct('F');" coords="110,6,129,21" />
	<area shape="rect" alt="g" title="g" nohref onclick="syncProduct('G');" coords="131,6,150,21" />
	<area shape="rect" alt="h" title="h" nohref onclick="syncProduct('H');" coords="152,6,171,21" />
	<area shape="rect" alt="i" title="i" nohref onclick="syncProduct('I');" coords="173,6,192,21" />
	<area shape="rect" alt="j" title="j" nohref onclick="syncProduct('J');" coords="5,23,24,38" />
	<area shape="rect" alt="k" title="k" nohref onclick="syncProduct('K');" coords="26,23,45,38" />
	<area shape="rect" alt="l" title="l" nohref onclick="syncProduct('L');" coords="47,23,66,38" />
	<area shape="rect" alt="m" title="m" nohref onclick="syncProduct('M');" coords="68,23,87,38" />
	<area shape="rect" alt="n" title="n" nohref onclick="syncProduct('N');" coords="89,23,108,38" />
	<area shape="rect" alt="o" title="o" nohref onclick="syncProduct('O');" coords="110,23,129,38" />
	<area shape="rect" alt="p" title="p" nohref onclick="syncProduct('P');" coords="131,23,150,38" />
	<area shape="rect" alt="q" title="q" nohref onclick="syncProduct('Q');" coords="152,23,171,38" />
	<area shape="rect" alt="r" title="r" nohref onclick="syncProduct('R');" coords="173,23,192,38" />
	<area shape="rect" alt="s" title="s" nohref onclick="syncProduct('S');" coords="5,40,24,55" />
	<area shape="rect" alt="t" title="t" nohref onclick="syncProduct('T');" coords="26,40,45,55" />
	<area shape="rect" alt="u" title="u" nohref onclick="syncProduct('U');" coords="47,40,66,55" />
	<area shape="rect" alt="v" title="v" nohref onclick="syncProduct('V');" coords="68,40,87,55" />
	<area shape="rect" alt="w" title="w" nohref onclick="syncProduct('W');" coords="89,40,108,55" />
	<area shape="rect" alt="x" title="x" nohref onclick="syncProduct('X');" coords="110,40,129,55" />
	<area shape="rect" alt="y" title="y" nohref onclick="syncProduct('Y');" coords="131,40,150,55" />
	<area shape="rect" alt="z" title="z" nohref onclick="syncProduct('Z');" coords="152,40,171,55" />
	<area shape="rect" alt="#" title="#" nohref onclick="syncProduct('#');" coords="173,40,192,55" />
</map>
<div id="nav" style="height:250px;width:194px;overflow:auto;border:1px solid #CEACE1;background-color:#FFFFFF;">
</div>

--%>

