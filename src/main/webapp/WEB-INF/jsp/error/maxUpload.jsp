<%@ page import="java.util.*,java.io.*" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>

<% 
Properties prop = new Properties();
prop.load(new FileInputStream(getServletContext().getRealPath("WEB-INF/properties/site.properties")));
pageContext.setAttribute("maxUploadSize", prop.get("site.maxUploadSize"));
%>

<div style="color:#FF0000">
Max upload size of 
<fmt:formatNumber value="${maxUploadSize/1024/1024}" pattern="##0.00" />
MB exceeded, <c:out value="${fn:substringAfter(maxUploadException.message, 'SizeLimitExceededException:')}"/>.
</div>

<div style="color:#FFFFFF">
<c:out value="${maxUploadException.message}"/>
</div>