<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<div id="footer"  align="center" class="quickMode">
  <div class="footer_r"></div>
  <div class="footer_l">
  <div class="foottxt">
  <div><a href="../config/configuration.jhtm">Site Info</a> 
  &nbsp;|&nbsp; 
  <a href="http://advancedemedia.com/faq.jhtm" target="_blank">FAQ</a> 
  &nbsp;|&nbsp; 
  <a href="../catalog/dashboard.jhtm?newAdmin=true" target="_blank">Responsive (Beta) version</a> 
  </div>Copyright &copy; 2000-<fmt:formatDate value="${now}" pattern="yyyy"/> Advanced E Media, inc. All rights reserved. 
  </div>
  </div>
</div>