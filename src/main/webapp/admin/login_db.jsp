<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="sql" uri="http://java.sun.com/jstl/sql" %>
<%@ page import="java.io.*,java.util.*,org.jdom.input.*,org.jdom.*,java.text.SimpleDateFormat,java.math.*,java.security.*,com.webjaguar.web.domain.Constants" %>
<%
Properties prop = new Properties();
prop.load(new FileInputStream(getServletContext().getRealPath("WEB-INF/properties/site.properties")));
pageContext.setAttribute("site_id", prop.get("site.id"));

Map<String, Object> gSiteConfig = new HashMap<String, Object>();
pageContext.setAttribute("gSiteConfig", gSiteConfig);
%>
<%-- 
<sql:setDataSource
        driver="com.mysql.jdbc.Driver"
        url="jdbc:mysql://localhost/global"
        user="global"
        password="webjaguar1"
/>
<sql:query var="result">
  SELECT
          gSITE_VERSION
        , gRESELLER
        , gSITE_VERSION_DATE
        , gADMIN_LOGIN_TIPS
  FROM
        aem_site_config
  WHERE
        site_id = '${site_id}'
</sql:query>
<c:forEach items="${result.rows}" var="row">
<c:set var="gRESELLER" value="${row.gRESELLER}" />
<c:set var="gSITE_VERSION" value="${row.gSITE_VERSION}" />
<c:set var="gSITE_VERSION_DATE" value="${row.gSITE_VERSION_DATE}" />
<c:set var="gADMIN_LOGIN_TIPS" value="${row.gADMIN_LOGIN_TIPS}" />
<%
gSiteConfig.put("gRESELLER", pageContext.getAttribute("gRESELLER"));
gSiteConfig.put("gSITE_VERSION", pageContext.getAttribute("gSITE_VERSION"));
gSiteConfig.put("gSITE_VERSION_DATE", pageContext.getAttribute("gSITE_VERSION_DATE"));
gSiteConfig.put("gADMIN_LOGIN_TIPS", pageContext.getAttribute("gADMIN_LOGIN_TIPS"));
%>
</c:forEach>
--%>
<%

SimpleDateFormat dateFormatter = new SimpleDateFormat("yyyy-MM-dd");

SAXBuilder builder = new SAXBuilder(false);
Document doc = builder.build("https://ws.webjaguar.com/global/getSiteConfig/" + prop.get("site.id") + "/" + Constants.md5((String) prop.get("secret.key"), null));
List<Element> elements = doc.getRootElement().getChildren("Column");
for (Element column: elements) {
	String name = column.getChildText("Name");
	Object value = null;
	String dataType = column.getChildText("DataType");
	if (dataType.contains("java.lang.String")) {
		value = new String(column.getChildText("Value"));
	} else if (dataType.contains("java.lang.Boolean")) {
		value = column.getChildText("Value").equals("true");
	} else if (dataType.contains("java.sql.Date")) {
		value = new java.sql.Date(dateFormatter.parse(column.getChildText("Value")).getTime());
	} else if (dataType.contains("java.lang.Integer")) {
		value = Integer.parseInt(column.getChildText("Value"));
	} else if (dataType.contains("java.lang.Long")) {
		value = Long.parseLong(column.getChildText("Value"));
	}
	gSiteConfig.put(name, value);
}

%>

<c:if test="${gSiteConfig['gADMIN_LOGIN_TIPS']}" >
<sql:setDataSource 
        driver="com.mysql.jdbc.Driver"
        url="jdbc:mysql://192.168.100.11/advancedemedia_com"
        user="advancedemedia"
        password="webjaguar1"
/>  
    
<sql:query var="tips">
  SELECT name, html_code FROM category WHERE id = 305 AND  protected_level = 512;
</sql:query>
</c:if>
