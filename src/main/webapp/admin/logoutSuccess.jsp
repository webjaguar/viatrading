<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Sign Off</title>
</head>
<body>
<div style="font-family:Verdana,Arial,Helvetica,sans-serif;font-size:12px;">
<div style="font-size:15px;font-weight:bold;padding:10px 0px 20px 0px">Signed Off</div>

Your session has ended.<br/>
In order to access the admin section, you will need to <a href="../admin/">Sign on</a> again.
</div>
</body>
</html>