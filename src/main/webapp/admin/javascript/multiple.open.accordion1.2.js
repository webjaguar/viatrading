var MultipleOpenAccordion = new Class({
		Extends: Accordion,
		options: {
			allowMultipleOpen: true,
			display: 100
		},
		initialize: function(togglers,togglees,options){
			this.parent(togglers,togglees,this.options);
			// console.log(this.options.toSource());
		},
		display: function(index){
			index = ($type(index) == 'element') ? this.elements.indexOf(index) : index;
			if ((this.timer && this.options.wait) || (index === this.previous && !this.options.alwaysHide)) return this;
			
			var obj = {};
			if(this.options.allowMultipleOpen){
				var el = this.elements[index];
				// console.log($type(el));
				obj[index] = {};
				var hide = (el.offsetHeight > 0);
				this.fireEvent(hide ? 'onBackground' : 'onActive', [this.togglers[index], el]);
				for (var fx in this.effects) obj[index][fx] = hide ? 0 : el[this.effects[fx]];
			}else{
				this.previous = index;
				this.elements.each(function(el, i){
					obj[i] = {};
					var hide = (i != index) || (this.options.alwaysHide && (el.offsetHeight > 0));
					this.fireEvent(hide ? 'onBackground' : 'onActive', [this.togglers[i], el]);
					for (var fx in this.effects) obj[i][fx] = hide ? 0 : el[this.effects[fx]];
				}, this);
				
			}
			return this.start(obj);
		}
	});
