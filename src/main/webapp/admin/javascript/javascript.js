function changeStyleClass(object,styleClass) {
  object.className = styleClass;
}

function toggleInfo(id) {
  if (document.getElementById('info'+id+'image').src.indexOf("expand.gif")>0) {
    document.getElementById('info'+id).style.display="block";
    document.getElementById('info'+id+'image').src="../graphics/collapse.gif";
  } else {
    document.getElementById('info'+id).style.display="none";
    document.getElementById('info'+id+'image').src="../graphics/expand.gif";
  }
}

function toggle(el) {
  if (document.getElementById(el+'Image').src.indexOf("expand.gif")>0) {
    document.getElementById(el).style.display="block";
	document.getElementById(el+'Image').src="../graphics/collapse.gif";
  } else {	
    document.getElementById(el).style.display="none";
    document.getElementById(el+'Image').src="../graphics/expand.gif";
  }
}

