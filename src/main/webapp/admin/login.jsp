<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<c:catch var="exposeError">
<%@ include file="/admin/login_db.jsp" %>
</c:catch>

<c:choose>
<c:when test="${gSiteConfig['gRESELLER'] == 'roi'}">
  <c:import url="reseller/roi/login.jsp"></c:import>
</c:when>
<c:when test="${gSiteConfig['gRESELLER'] == 'datamagic'}">
  <c:import url="reseller/datamagic/login.jsp"></c:import>
</c:when>
<c:otherwise>
<html>
<head>
<title>Login</title>
<c:choose>
<c:when test="${gSiteConfig['gADMIN_LOGIN_TIPS']}">
<link href="https://www.webjaguar.net/aem/style.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="//ajax.googleapis.com/ajax/libs/mootools/1.2.4/mootools.js"></script>
<script type="text/javascript" charset="utf-8" src="https://www.webjaguar.net/aem/tips.js"></script>
</c:when>
<c:otherwise><link href="https://f50ce8b91dd1f94c5ec2-3e285bfa4e7ff77b7136a6d2aeecab08.ssl.cf5.rackcdn.com/styleNoTip.css" rel="stylesheet" type="text/css" /></c:otherwise>
</c:choose>
</head>
<body onload="document.getElementsByTagName('input')[0].focus();">
<div id="wrapper">
  <div id="mainContent">
     <div id="loginDivBox">
     <div id="logo">
       <div style="background:url(https://www.webjaguar.com/logo/<c:out value="${gSiteConfig['gRESELLER']}" />.gif) no-repeat center;height:140px; margin: 0 auto;" ></div>
     </div>
     <div id="formDiv">
     <form action="logincheck.jsp" method="POST">
	    <label>
		  <strong class="emailLabel">Username</strong>
		  <input type="text" class="input" id="j_username" name="j_username" value="<c:if test="${not empty param.login_error}"><c:out value="${SPRING_SECURITY_LAST_USERNAME}"/></c:if>" />
		</label>
		<label>
		  <strong class="passLabel">Password</strong>
		  <input type="password" class="input" id="j_password" name="j_password"  value="">
		</label>
		<c:if test="${not empty param.login_error}">
	     <span id="message" style="color:#C00000;">
	            Your login attempt was not successful, try again.<br/>
	            <%-- Reason: <c:out value="${SPRING_SECURITY_LAST_EXCEPTION.message}"/>.--%>
	     </span>
	 </c:if>
		<input class="login" name="submit" type="submit" value="Login"/>   
	 </form>               
     </div>
     </div>  
     <c:if test="${gSiteConfig['gADMIN_LOGIN_TIPS']}" >
     <div id="tipDivBox">
        <div id="tipDiv">
	        <c:if test="true">
	        <div id=tips>
	        <c:if test="${!empty tips.rows and tips.rows != null}">
		        <c:forEach items="${tips.rows}" var="tip">
					<c:out value="${tip.html_code}" escapeXml="false"/>
				</c:forEach>
        	</c:if>
        	</div>
	        </c:if>
        </div>
     </div>
     </c:if>
</div>
  
</div>
<div id="footer">
   <ul><li>Version <c:out value="${gSiteConfig['gSITE_VERSION']}" escapeXml="false" /></li></ul>
</div>
</body>
</html>
</c:otherwise>
</c:choose>