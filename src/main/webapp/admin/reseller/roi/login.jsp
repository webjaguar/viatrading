<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<c:catch var="exposeError">
<%@ include file="/admin/login_db.jsp" %>
</c:catch>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Login</title>
<link href="https://www.webjaguar.net/roi/style.css" rel="stylesheet" type="text/css" />
</head>
<body>
<table width="100%" border="0" cellspacing="0" cellpadding="0" id="contenttable">
  <tr>
    <td valign="middle">
    	<div id="mainContianer">
    	<div class="top"></div>
            <div class="midd">
                <img src="https://www.webjaguar.net/roi/images/roilogo.jpg" width="278" height="78" alt="ROI" class="logo" />
              <form action="logincheck.jsp" method="POST">
                <c:if test="${not empty param.login_error}">
          		  <div style="color:#C00000;font:normal 11px/22px Verdana, Geneva, sans-serif;">
            		Your login attempt was not successful, try again.<br/>
            		Reason: <c:out value="${SPRING_SECURITY_LAST_EXCEPTION.message}"/>.<br/><br/>
          		  </div>
        		</c:if>
               	<p><label>User Name:</label>
                  <input name="j_username" type="text" class="int" id="username" value="<c:if test="${not empty param.login_error}"><c:out value="${SPRING_SECURITY_LAST_USERNAME}"/></c:if>"/></p>
                <p><label>Password:</label>
                  <input name="j_password" type="password" class="int" id="password" /></p>
                  <p class="login"><input type="image" src="https://www.webjaguar.net/roi/images/login.jpg" alt="login" /></p>
                </form>
            </div>
        <div class="bottom">Version <c:out value="${gSiteConfig['gSITE_VERSION']}" escapeXml="false" /></div>	
    </div>
    
    </td>
  </tr>
</table>
</body>
</html>