function decodeHTML(encoded){
	var tmp = document.createElement('div');
	tmp.innerHTML = encoded;
	return tmp.innerHTML;
}

function toggleSubDropdownItems(event,element){
	 event.stopPropagation();
	 var $sub = $(element.getAttribute('data-target'));
	 $sub.toggle();
}

function cleanDropdowns(){
	$('.dropdown').removeClass('show');
}

$('.left-sidebar .has-children').click(function(){
	var $collapse = $(this).next();
	$(this).toggleClass('is-open');
	$collapse.toggleClass('show');
});

$('td.unlink').click(function(e){
	cleanDropdowns();
	e.stopPropagation();
});

$('.dropdown-toggle').click(function(e){
	var isShown = $(this).parent().hasClass('show'); 
	cleanDropdowns();
	if(isShown)
		$(this).parent().removeClass('show');
	else
		$(this).parent().addClass('show');
	e.stopPropagation();
});

$('.toggle-layout').click(function(){
	if($('[data-layout]').attr('data-collapsed')=="false")
		$('[data-layout]').attr('data-collapsed','true');
	else
		$('[data-layout]').attr('data-collapsed','false');
});

$('.nav-item .dropdown-menu .dropdown-item').click(function(){
	var dropown_lnk = $(this).parent().parent().children('.nav-link');
	dropown_lnk.addClass('active')
	var $ul = $(this).parent().parent().parent().parent();
	var children = $ul.find(".nav-item .dropdown .dropdown-menu .dropdown-item");
	$(children).removeClass('active');
	$(children).attr('aria-expanded','false');
});

$('.nav-item .nav-link').click(function(){
	var $ul = $(this).parent().parent();
	var dropown_lnk = $ul.find(".nav-item .dropdown").children('.nav-link');
	dropown_lnk.removeClass('active');
	var children = $ul.find(".nav-item .dropdown .dropdown-menu .dropdown-item");
	$(children).removeClass('active');
	$(children).attr('aria-expanded','false');
});

$('.dropdown-setting .dropdown-menu').click(function(e){
	e.stopPropagation();
});

$('.dropdown-full .dropdown-menu').click(function(e){
	e.stopPropagation();
});

$(window).resize(function(){
	var w = $('.page-body').width();
	if($(window).width()<=768)
		w -= 30;
	$('.dropdown.dropdown-full .dropdown-menu').css('width',w);
});

$(document).click(function(){
	cleanDropdowns();
});

$(document).ready(function(){
	$('[data-toggle="tooltip"]').tooltip();
	$(window).resize();
	var $actions_dropdown = $(".dropdown-actions");
	if($actions_dropdown.children(".dropdown-menu").children().length==0)
		$actions_dropdown.hide();
	var $tabs_dropdown = $('.nav.nav-tabs .dropdown');
	if($tabs_dropdown.children(".dropdown-menu").children().length==0)
		$tabs_dropdown.hide();
});