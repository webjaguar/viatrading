(function ($) {
  'use strict';

  $(function () {
    $('.editor_wrapper').each(function () {
      let $this = $(this);
      let editor_id = $this.data('editor');

      // Init CodeEditor
      CodeEditor.init(editor_id);
    });
  });
})(jQuery);

let CodeEditor = function () {
  return {
    init: function (editor_id) {
      let textarea = $('#' + editor_id);
      textarea.hide();
      let code_Editor_id = editor_id + '_ace_editor';
      let ace_editor_div = $('<div>', {
        id: code_Editor_id,
        width: '100%',
        height: '200px'
      });
      textarea.before(ace_editor_div);
      let ace_editor = ace.edit(code_Editor_id);
      ace_editor.setTheme('ace/theme/chrome');
      ace_editor.getSession().setMode('ace/mode/html');
      ace_editor.setFontSize(11);
      ace_editor.setOptions({
        enableEmmet: true,
        enableBasicAutocompletion: true,
        enableSnippets: true,
        enableLiveAutocompletion: true
      });
      ace_editor.$blockScrolling = Infinity;
      ace_editor.getSession().setValue(textarea.val(), -1);
      ace_editor.getSession().on('change', function () {
        textarea.val(ace_editor.getSession().getValue());
      });
      let session = ace_editor.getSession();
      session.on("changeAnnotation", function () {
        let annotations = session.getAnnotations() || [], len = annotations.length, i = len;
        while (i--) {
          if (/doctype first\. Expected/.test(annotations[i].text)) {
            annotations.splice(i, 1);
          }
        }
        if (len > annotations.length) {
          session.setAnnotations(annotations);
        }
      });
      textarea.on('change keyup paste', function () {
        ace_editor.setValue(textarea.val(), -1);
      });
    },
    update: function (editor_id, content) {
      let code_Editor_id = editor_id + '_ace_editor';
      let ace_editor = ace.edit(code_Editor_id);
      ace_editor.getSession().setValue(content, -1);
    },
    close: function (editor_id) {
      let textarea = $('#' + editor_id);
      let code_Editor_id = editor_id + '_ace_editor';
      let ace_editor = ace.edit(code_Editor_id);
      ace_editor.destroy();
      textarea.unbind();
      $('#' + code_Editor_id).remove();
    }
  }
}();