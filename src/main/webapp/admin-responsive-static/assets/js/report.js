function UpdateStartEndDate(type){
	var myDate = new Date();
	$('#endDate').val((myDate.getMonth()+1) + "/" + myDate.getDate() + "/" + myDate.getFullYear());
	if ( type == 2 ) {
		myDate.setDate(myDate.getDate() - myDate.getDay());
		$('#startDate').val((myDate.getMonth()+1) + "/" + myDate.getDate() + "/" + myDate.getFullYear());
	} else if ( type == 3 ) {
		myDate.setDate(1);
		$('#startDate').val((myDate.getMonth()+1) + "/" + myDate.getDate() + "/" + myDate.getFullYear());
	}else if ( type == 10 ) {
		myDate.setMonth(0, 1);
		$('#startDate').val((myDate.getMonth()+1) + "/" + myDate.getDate() + "/" + myDate.getFullYear());
	}else if ( type == 20 ) {
		$('#startDate').val("");
		$('#endDate').val("");
	}else{
		$('#startDate').val((myDate.getMonth()+1) + "/" + myDate.getDate() + "/" + myDate.getFullYear());
	}
}