package com.webjaguar.model;

public class MailInRebateSearch {

	// sort
	private String sort= "rebate_id DESC";
	public String getSort() { return sort; }
	public void setSort(String sort) { this.sort = sort; }
	
	// active 
	private boolean activeOnly = false;
	public boolean isActiveOnly() { return activeOnly; }
	public void setActiveOnly(boolean activeOnly) { this.activeOnly = activeOnly; }
	
	// page 
	private int page = 1;
	public int getPage() { return page; }
	public void setPage(int page) { this.page = page; }
	
	// page size
	private int pageSize = 10;
	public int getPageSize() { return pageSize; }
	public void setPageSize(int pageSize) { this.pageSize = pageSize; }

}
