package com.webjaguar.model;

import java.util.Date;

public class SalesRepReport
{
	private String name;
	private Integer month;
	private Integer day;
	private Date orderDate;
	private Double grandTotalShipped;
	private Double grandTotalPenProc;
	private Integer userId;
	private Integer salesRepId;
	private Integer numOrderShipped;
	private Integer uniqueUserShipped;
	private Integer numOrderPenProc;
	private Integer uniqueUserPenProc;

	
	public SalesRepReport( Integer month, Double grandTotalShipped, Double grandTotalPenProc, Integer salesRepId, Integer userId )
	{
		super();
		this.month = month;
		this.grandTotalShipped = grandTotalShipped;
		this.grandTotalPenProc = grandTotalPenProc;
		this.salesRepId = salesRepId;
		this.userId = userId;
	}
	public SalesRepReport( Date orderDate, Double grandTotalShipped, Double grandTotalPenProc, Integer numOrderShipped, Integer uniqueUserShipped, Integer numOrderPenProc, Integer uniqueUserPenProc, Integer salesRepId, Integer userId )
	{
		super();
		this.orderDate = orderDate;
		this.grandTotalShipped = grandTotalShipped;
		this.grandTotalPenProc = grandTotalPenProc;
		this.salesRepId = salesRepId;
		this.userId = userId;
		this.numOrderShipped = numOrderShipped;
		this.uniqueUserShipped = uniqueUserShipped;
		this.numOrderPenProc = numOrderPenProc;
		this.uniqueUserPenProc = uniqueUserPenProc;
	}
	public Double getGrandTotalShipped()
	{
		return grandTotalShipped;
	}
	public void setGrandTotalShipped(Double grandTotalShipped)
	{
		this.grandTotalShipped = grandTotalShipped;
	}
	public Double getGrandTotalPenProc()
	{
		return grandTotalPenProc;
	}
	public void setGrandTotalPenProc(Double grandTotalPenProc)
	{
		this.grandTotalPenProc = grandTotalPenProc;
	}
	public Integer getUserId()
	{
		return userId;
	}
	public void setUserId(Integer userId)
	{
		this.userId = userId;
	}
	public SalesRepReport()
	{
		super();
	}
	public Integer getMonth()
	{
		return month;
	}
	public void setMonth(Integer month)
	{
		this.month = month;
	}
	public Integer getSalesRepId()
	{
		return salesRepId;
	}
	public void setSalesRepId(Integer salesRepId)
	{
		this.salesRepId = salesRepId;
	}
	public Integer getDay()
	{
		return day;
	}
	public void setDay(Integer day)
	{
		this.day = day;
	}
	public Date getOrderDate()
	{
		return orderDate;
	}
	public void setOrderDate(Date orderDate)
	{
		this.orderDate = orderDate;
	}
	public Integer getNumOrderShipped()
	{
		return numOrderShipped;
	}
	public void setNumOrderShipped(Integer numOrderShipped)
	{
		this.numOrderShipped = numOrderShipped;
	}
	public Integer getUniqueUserShipped()
	{
		return uniqueUserShipped;
	}
	public void setUniqueUserShipped(Integer uniqueUserShipped)
	{
		this.uniqueUserShipped = uniqueUserShipped;
	}
	public Integer getUniqueUserPenProc()
	{
		return uniqueUserPenProc;
	}
	public void setUniqueUserPenProc(Integer uniqueUserPenProc)
	{
		this.uniqueUserPenProc = uniqueUserPenProc;
	}
	public Integer getNumOrderPenProc()
	{
		return numOrderPenProc;
	}
	public void setNumOrderPenProc(Integer numOrderPenProc)
	{
		this.numOrderPenProc = numOrderPenProc;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}

}
