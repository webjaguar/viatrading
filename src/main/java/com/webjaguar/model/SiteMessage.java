package com.webjaguar.model;

import java.util.List;

public class SiteMessage
{
	private Integer messageId;
	private String messageName;
	private String message;
	private String subject;
	private boolean html;
	private Integer groupId;
    private List<Integer> groupIdList;
    private String groupName;
    private String sourceUrl;
	
	public boolean isHtml()
	{
		return html;
	}
	public void setHtml(boolean html)
	{
		this.html = html;
	}
	public String getMessage()
	{
		return message;
	}
	public void setMessage(String message)
	{
		this.message = message;
	}
	public Integer getMessageId()
	{
		return messageId;
	}
	public void setMessageId(Integer messageId)
	{
		this.messageId = messageId;
	}
	public String getMessageName()
	{
		return messageName;
	}
	public void setMessageName(String messageName)
	{
		this.messageName = messageName;
	}
	public String getSubject()
	{
		return subject;
	}
	public void setSubject(String subject)
	{
		this.subject = subject;
	}
	public Integer getGroupId() {
		return groupId;
	}
	public void setGroupId(Integer groupId) {
		this.groupId = groupId;
	}
	public List<Integer> getGroupIdList() {
		return groupIdList;
	}
	public void setGroupIdList(List<Integer> groupIdList) {
		this.groupIdList = groupIdList;
	}
	public String getGroupName() {
		return groupName;
	}
	public void setGroupName(String groupName) {
		this.groupName = groupName;
	}
	public String getSourceUrl() {
		return sourceUrl;
	}
	public void setSourceUrl(String sourceUrl) {
		this.sourceUrl = sourceUrl;
	}
}
