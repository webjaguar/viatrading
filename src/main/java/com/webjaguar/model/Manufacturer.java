/* Copyright 2008 Advanced E-Media Solutions
 * @author Shahin Naji
 * @since 03.11.2009
 */

package com.webjaguar.model;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.Date;

public class Manufacturer implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private Integer id;
	private String name;
	private String imageUrl;
	private Date dateAdded;
	private Timestamp lastModified;
	private Double minOrder;
	private double accumulator;
	
	public Integer getId()
	{
		return id;
	}
	public void setId(Integer id)
	{
		this.id = id;
	}
	public String getName()
	{
		return name;
	}
	public void setName(String name)
	{
		this.name = name;
	}
	public String getImageUrl()
	{
		return imageUrl;
	}
	public void setImageUrl(String imageUrl)
	{
		this.imageUrl = imageUrl;
	}
	public Date getDateAdded()
	{
		return dateAdded;
	}
	public void setDateAdded(Date dateAdded)
	{
		this.dateAdded = dateAdded;
	}
	public Timestamp getLastModified()
	{
		return lastModified;
	}
	public void setLastModified(Timestamp lastModified)
	{
		this.lastModified = lastModified;
	}
	public Double getMinOrder()
	{
		return minOrder;
	}
	public void setMinOrder(Double minOrder)
	{
		this.minOrder = minOrder;
	}
	public double getAccumulator()
	{
		return accumulator;
	}
	public void setAccumulator(double accumulator)
	{
		this.accumulator = accumulator;
	}
}
