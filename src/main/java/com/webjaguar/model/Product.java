/*
 * Copyright 2005, 2006 Advanced E-Media Solutions
 * 
 */

package com.webjaguar.model;

import java.beans.IntrospectionException;
import java.beans.PropertyDescriptor;
import java.io.Serializable;
import java.io.UnsupportedEncodingException;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlSeeAlso;
import javax.xml.bind.annotation.XmlTransient;

import org.codehaus.jackson.map.annotate.JsonSerialize;
import org.codehaus.jackson.map.annotate.JsonSerialize.Inclusion;

import com.webjaguar.model.api.ApiProduct;

@XmlRootElement(name = "product")
@XmlAccessorType(XmlAccessType.FIELD)
@XmlSeeAlso({ ProductImage.class, IncludedProduct.class })
@JsonSerialize(include = Inclusion.NON_NULL)
public class Product implements Serializable {

	/**
	 * 
	 */

	public Product() {
	}

	public Product(String sku, String createdBy) {
		this.sku = sku;
		this.name = sku;
		this.createdBy = createdBy;
	}

	public Product(ApiProduct apiProduct) {
		System.out.println("product constructor called::");
		this.sku = apiProduct.getSku();
		this.masterSku = apiProduct.getMasterSku();
		this.upc = apiProduct.getUpc();
		this.name = apiProduct.getName();
		this.shortDesc = apiProduct.getShortDesc();
		this.longDesc = apiProduct.getLongDesc();

		this.active = apiProduct.isActive();
		this.packing = apiProduct.getPacking();

		this.field1 = apiProduct.getField1();
		this.field2 = apiProduct.getField2();
		this.field3 = apiProduct.getField3();
		this.field4 = apiProduct.getField4();
		this.field5 = apiProduct.getField5();
		this.field6 = apiProduct.getField6();
		this.field7 = apiProduct.getField7();
		this.field8 = apiProduct.getField8();
		this.field9 = apiProduct.getField9();
		this.field10 = apiProduct.getField10();
		this.field11 = apiProduct.getField11();
		this.field12 = apiProduct.getField12();
		this.field13 = apiProduct.getField13();
		this.field14 = apiProduct.getField14();
		this.field15 = apiProduct.getField15();
		this.field16 = apiProduct.getField16();
		this.field17 = apiProduct.getField17();
		this.field18 = apiProduct.getField18();
		this.field19 = apiProduct.getField19();
		this.field20 = apiProduct.getField20();
		this.field21 = apiProduct.getField21();
		this.field22 = apiProduct.getField22();
		this.field23 = apiProduct.getField23();
		this.field24 = apiProduct.getField24();
		this.field25 = apiProduct.getField25();
		this.field26 = apiProduct.getField26();
		this.field27 = apiProduct.getField27();
		this.field28 = apiProduct.getField28();
		this.field29 = apiProduct.getField29();
		this.field30 = apiProduct.getField30();
		this.field31 = apiProduct.getField31();
		this.field32 = apiProduct.getField32();
		this.field33 = apiProduct.getField33();
		this.field34 = apiProduct.getField34();
		this.field35 = apiProduct.getField35();
		this.field36 = apiProduct.getField36();
		this.field37 = apiProduct.getField37();
		this.field38 = apiProduct.getField38();
		this.field39 = apiProduct.getField39();
		this.field40 = apiProduct.getField40();
		this.field41 = apiProduct.getField41();
		this.field42 = apiProduct.getField42();
		this.field43 = apiProduct.getField43();
		this.field44 = apiProduct.getField44();
		this.field45 = apiProduct.getField45();
		this.field46 = apiProduct.getField46();
		this.field47 = apiProduct.getField47();
		this.field48 = apiProduct.getField48();
		this.field49 = apiProduct.getField49();
		this.field50 = apiProduct.getField50();
		this.field51 = apiProduct.getField51();
		this.field52 = apiProduct.getField52();
		this.field53 = apiProduct.getField53();
		this.field54 = apiProduct.getField54();
		this.field55 = apiProduct.getField55();
		this.field56 = apiProduct.getField56();
		this.field57 = apiProduct.getField57();
		this.field58 = apiProduct.getField58();
		this.field59 = apiProduct.getField59();
		this.field60 = apiProduct.getField60();
		this.field61 = apiProduct.getField61();
		this.field62 = apiProduct.getField62();
		this.field63 = apiProduct.getField63();
		this.field64 = apiProduct.getField64();
		this.field65 = apiProduct.getField65();
		this.field66 = apiProduct.getField66();
		this.field67 = apiProduct.getField67();
		this.field68 = apiProduct.getField68();
		this.field69 = apiProduct.getField69();
		this.field70 = apiProduct.getField70();
		this.field71 = apiProduct.getField71();
		this.field72 = apiProduct.getField72();
		this.field73 = apiProduct.getField73();
		this.field74 = apiProduct.getField74();
		this.field75 = apiProduct.getField75();
		this.field76 = apiProduct.getField76();
		this.field77 = apiProduct.getField77();
		this.field78 = apiProduct.getField78();
		this.field79 = apiProduct.getField79();
		this.field80 = apiProduct.getField80();
		this.field81 = apiProduct.getField81();
		this.field82 = apiProduct.getField82();
		this.field83 = apiProduct.getField83();
		this.field84 = apiProduct.getField84();
		this.field85 = apiProduct.getField85();
		this.field86 = apiProduct.getField86();
		this.field87 = apiProduct.getField87();
		this.field88 = apiProduct.getField88();
		this.field89 = apiProduct.getField89();
		this.field90 = apiProduct.getField90();
		this.field91 = apiProduct.getField91();
		this.field92 = apiProduct.getField92();
		this.field93 = apiProduct.getField93();
		this.field94 = apiProduct.getField94();
		this.field95 = apiProduct.getField95();
		this.field96 = apiProduct.getField96();
		this.field97 = apiProduct.getField97();
		this.field98 = apiProduct.getField98();
		this.field99 = apiProduct.getField99();
		this.field100 = apiProduct.getField100();

		this.price1 = apiProduct.getPrice1();
		this.price2 = apiProduct.getPrice2();
		this.price3 = apiProduct.getPrice3();
		this.price4 = apiProduct.getPrice4();
		this.price5 = apiProduct.getPrice5();
		this.price6 = apiProduct.getPrice6();
		this.price7 = apiProduct.getPrice7();
		this.price8 = apiProduct.getPrice8();
		this.price9 = apiProduct.getPrice9();
		this.price10 = apiProduct.getPrice10();

		this.qtyBreak1 = apiProduct.getQtyBreak1();
		this.qtyBreak2 = apiProduct.getQtyBreak2();
		this.qtyBreak3 = apiProduct.getQtyBreak3();
		this.qtyBreak4 = apiProduct.getQtyBreak4();
		this.qtyBreak5 = apiProduct.getQtyBreak5();
		this.qtyBreak6 = apiProduct.getQtyBreak6();
		this.qtyBreak7 = apiProduct.getQtyBreak7();
		this.qtyBreak8 = apiProduct.getQtyBreak8();
		this.qtyBreak9 = apiProduct.getQtyBreak9();
		/*
		 * try {
		 * System.out.println("apiProduct.getWeight()::>> "+apiProduct.getWeight());
		 * this.weight = Double.parseDouble(apiProduct.getWeight()); }catch
		 * (NullPointerException e) { System.out.println(weight+" :180: "+e.getCause());
		 * this.weight = weight; }catch (Exception e) {
		 * System.out.println("183: "+e.getCause()); this.weight = null; }
		 */

		this.packageL = apiProduct.getPackageL();
		this.packageW = apiProduct.getPackageW();
		this.packageH = apiProduct.getPackageH();
		this.caseContent = apiProduct.getCaseContent();

		this.inventory = apiProduct.getInventory();
		this.inventoryAFS = apiProduct.getInventoryAFS();
		this.negInventory = apiProduct.isNegInventory() == null ? false : apiProduct.isNegInventory();
		this.lowInventory = apiProduct.getLowInventory();
		this.showNegInventory = apiProduct.isShowNegInventory() == null ? false : apiProduct.isShowNegInventory();
		this.minimumQty = apiProduct.getMinimumQty();
		this.incrementalQty = apiProduct.getIncrementalQty();
		this.upsMaxItemsInPackage = apiProduct.getUpsMaxItemsInPackage();
		this.uspsMaxItemsInPackage = apiProduct.getUspsMaxItemsInPackage();
		this.defaultSupplierId = apiProduct.getDefaultSupplierId();
		this.fobZipCode = apiProduct.getFobZipCode();
		this.softLink = apiProduct.isSoftLink();
		this.categoryIds = apiProduct.getCategoryIds();
		this.redirectUrlEn = apiProduct.getRedirectUrlEn();
		this.redirectUrlEs = apiProduct.getRedirectUrlEs();
		this.classField = apiProduct.getClassField();

		this.i18nProduct = apiProduct.getI18nProduct();

	}

	/*
	 * public void setProduct(ApiProduct apiProduct) throws
	 * IllegalArgumentException, IllegalAccessException, InvocationTargetException {
	 * 
	 * int index = -1; Field[] fList = this.getClass().getDeclaredFields(); for
	 * (Field f : fList) { index++; if(f.getName().equals("serialVersionUID"))
	 * continue;
	 * 
	 * System.out.println("fields:: "+f.getName()+" - "+f.getType()); for (Method m
	 * : apiProduct.getClass().getMethods()) { try { if
	 * (m.getName().startsWith("get") && !m.getName().equals("getClass")) { if
	 * (m.invoke(apiProduct) == null) { continue; }
	 * 
	 * if(f.getType().toString().contains("double")) f.set(apiProduct, ((Double)
	 * m.invoke(apiProduct))); else if(f.getType().toString().contains("boolean"))
	 * f.set(apiProduct, ((Boolean) m.invoke(apiProduct))); else
	 * if(f.getType().toString().contains("string")) f.set(apiProduct, ((String)
	 * m.invoke(apiProduct))); else if(f.getType().toString().contains("integer"))
	 * f.set(apiProduct, ((Integer) m.invoke(apiProduct))); else f.set(apiProduct,
	 * m.invoke(apiProduct));
	 * 
	 * break; } } catch (NullPointerException e) {
	 * System.out.println(weight+" :180: "+e.getMessage()); //this.weight = weight;
	 * f.set(apiProduct, m.invoke(this));
	 * 
	 * }catch (Exception e) { System.out.println("183: "+e.getMessage());
	 * //this.weight = null; fList[index] = null; } } }
	 * 
	 * }
	 */

	/*
	 * public void setProduct(ApiProduct apiProduct) {
	 * 
	 * this.sku = apiProduct.getSku(); this.masterSku = apiProduct.getMasterSku();
	 * this.upc = apiProduct.getUpc(); this.name = apiProduct.getName();
	 * this.shortDesc = apiProduct.getShortDesc(); this.longDesc =
	 * apiProduct.getLongDesc();
	 * 
	 * this.active = apiProduct.isActive(); this.packing = apiProduct.getPacking();
	 * 
	 * this.field1 = apiProduct.getField1(); this.field2 = apiProduct.getField2();
	 * this.field3 = apiProduct.getField3(); this.field4 = apiProduct.getField4();
	 * this.field5 = apiProduct.getField5(); this.field6 = apiProduct.getField6();
	 * this.field7 = apiProduct.getField7(); this.field8 = apiProduct.getField8();
	 * this.field9 = apiProduct.getField9(); this.field10 = apiProduct.getField10();
	 * this.field11 = apiProduct.getField11(); this.field12 =
	 * apiProduct.getField12(); this.field13 = apiProduct.getField13(); this.field14
	 * = apiProduct.getField14(); this.field15 = apiProduct.getField15();
	 * this.field16 = apiProduct.getField16(); this.field17 =
	 * apiProduct.getField17(); this.field18 = apiProduct.getField18(); this.field19
	 * = apiProduct.getField19(); this.field20 = apiProduct.getField20();
	 * this.field21 = apiProduct.getField21(); this.field22 =
	 * apiProduct.getField22(); this.field23 = apiProduct.getField23(); this.field24
	 * = apiProduct.getField24(); this.field25 = apiProduct.getField25();
	 * this.field26 = apiProduct.getField26(); this.field27 =
	 * apiProduct.getField27(); this.field28 = apiProduct.getField28(); this.field29
	 * = apiProduct.getField29(); this.field30 = apiProduct.getField30();
	 * this.field31 = apiProduct.getField31(); this.field32 =
	 * apiProduct.getField32(); this.field33 = apiProduct.getField33(); this.field34
	 * = apiProduct.getField34(); this.field35 = apiProduct.getField35();
	 * this.field36 = apiProduct.getField36(); this.field37 =
	 * apiProduct.getField37(); this.field38 = apiProduct.getField38(); this.field39
	 * = apiProduct.getField39(); this.field40 = apiProduct.getField40();
	 * this.field41 = apiProduct.getField41(); this.field42 =
	 * apiProduct.getField42(); this.field43 = apiProduct.getField43(); this.field44
	 * = apiProduct.getField44(); this.field45 = apiProduct.getField45();
	 * this.field46 = apiProduct.getField46(); this.field47 =
	 * apiProduct.getField47(); this.field48 = apiProduct.getField48(); this.field49
	 * = apiProduct.getField49(); this.field50 = apiProduct.getField50();
	 * this.field51 = apiProduct.getField51(); this.field52 =
	 * apiProduct.getField52(); this.field53 = apiProduct.getField53(); this.field54
	 * = apiProduct.getField54(); this.field55 = apiProduct.getField55();
	 * this.field56 = apiProduct.getField56(); this.field57 =
	 * apiProduct.getField57(); this.field58 = apiProduct.getField58(); this.field59
	 * = apiProduct.getField59(); this.field60 = apiProduct.getField60();
	 * this.field61 = apiProduct.getField61(); this.field62 =
	 * apiProduct.getField62(); this.field63 = apiProduct.getField63(); this.field64
	 * = apiProduct.getField64(); this.field65 = apiProduct.getField65();
	 * this.field66 = apiProduct.getField66(); this.field67 =
	 * apiProduct.getField67(); this.field68 = apiProduct.getField68(); this.field69
	 * = apiProduct.getField69(); this.field70 = apiProduct.getField70();
	 * this.field71 = apiProduct.getField71(); this.field72 =
	 * apiProduct.getField72(); this.field73 = apiProduct.getField73(); this.field74
	 * = apiProduct.getField74(); this.field75 = apiProduct.getField75();
	 * this.field76 = apiProduct.getField76(); this.field77 =
	 * apiProduct.getField77(); this.field78 = apiProduct.getField78(); this.field79
	 * = apiProduct.getField79(); this.field80 = apiProduct.getField80();
	 * this.field81 = apiProduct.getField81(); this.field82 =
	 * apiProduct.getField82(); this.field83 = apiProduct.getField83(); this.field84
	 * = apiProduct.getField84(); this.field85 = apiProduct.getField85();
	 * this.field86 = apiProduct.getField86(); this.field87 =
	 * apiProduct.getField87(); this.field88 = apiProduct.getField88(); this.field89
	 * = apiProduct.getField89(); this.field90 = apiProduct.getField90();
	 * this.field91 = apiProduct.getField91(); this.field92 =
	 * apiProduct.getField92(); this.field93 = apiProduct.getField93(); this.field94
	 * = apiProduct.getField94(); this.field95 = apiProduct.getField95();
	 * this.field96 = apiProduct.getField96(); this.field97 =
	 * apiProduct.getField97(); this.field98 = apiProduct.getField98(); this.field99
	 * = apiProduct.getField99(); this.field100 = apiProduct.getField100();
	 * 
	 * this.price1 = apiProduct.getPrice1(); this.price2 = apiProduct.getPrice2();
	 * this.price3 = apiProduct.getPrice3(); this.price4 = apiProduct.getPrice4();
	 * this.price5 = apiProduct.getPrice5(); this.price6 = apiProduct.getPrice6();
	 * this.price7 = apiProduct.getPrice7(); this.price8 = apiProduct.getPrice8();
	 * this.price9 = apiProduct.getPrice9(); this.price10 = apiProduct.getPrice10();
	 * 
	 * this.qtyBreak1 = apiProduct.getQtyBreak1(); this.qtyBreak2 =
	 * apiProduct.getQtyBreak2(); this.qtyBreak3 = apiProduct.getQtyBreak3();
	 * this.qtyBreak4 = apiProduct.getQtyBreak4(); this.qtyBreak5 =
	 * apiProduct.getQtyBreak5(); this.qtyBreak6 = apiProduct.getQtyBreak6();
	 * this.qtyBreak7 = apiProduct.getQtyBreak7(); this.qtyBreak8 =
	 * apiProduct.getQtyBreak8(); this.qtyBreak9 = apiProduct.getQtyBreak9();
	 * 
	 * 
	 * 
	 * this.packageL = apiProduct.getPackageL(); this.packageW
	 * =apiProduct.getPackageW(); this.packageH = apiProduct.getPackageH();
	 * this.caseContent = apiProduct.getCaseContent();
	 * 
	 * this.inventory = apiProduct.getInventory(); this.inventoryAFS =
	 * apiProduct.getInventoryAFS(); this.negInventory = apiProduct.isNegInventory()
	 * == null ? false : apiProduct.isNegInventory(); this.lowInventory =
	 * apiProduct.getLowInventory(); this.showNegInventory =
	 * apiProduct.isShowNegInventory() == null ? false :
	 * apiProduct.isShowNegInventory(); this.minimumQty =
	 * apiProduct.getMinimumQty(); this.incrementalQty =
	 * apiProduct.getIncrementalQty(); this.upsMaxItemsInPackage =
	 * apiProduct.getUpsMaxItemsInPackage(); this.uspsMaxItemsInPackage =
	 * apiProduct.getUspsMaxItemsInPackage(); this.defaultSupplierId =
	 * apiProduct.getDefaultSupplierId(); this.fobZipCode =
	 * apiProduct.getFobZipCode(); this.softLink = apiProduct.isSoftLink();
	 * this.categoryIds = apiProduct.getCategoryIds(); this.redirectUrlEn =
	 * apiProduct.getRedirectUrlEn(); this.redirectUrlEs =
	 * apiProduct.getRedirectUrlEs(); this.classField = apiProduct.getClassField();
	 * 
	 * this.i18nProduct = apiProduct.getI18nProduct();
	 * 
	 * }
	 */

	public void setProduct(ApiProduct apiProduct) {

		try {
			System.out.println("apiProduct.getWeight()::>> " + apiProduct.getWeight());
			this.weight = Double.parseDouble(apiProduct.getWeight());
		} catch (NullPointerException e) {
			System.out.println(weight + " :180: " + e.getCause());
			this.weight = weight;
		} catch (Exception e) {
			System.out.println("weight: " + e.getMessage());
			this.weight = null;
		}

		try {
			this.sku = apiProduct.getSku();
		} catch (NullPointerException e) {
			this.sku = sku;
		} catch (Exception e) {
			System.out.println("sku: " + e.getMessage());
			this.sku = null;
		}

		try {
			this.masterSku = apiProduct.getMasterSku();
		} catch (NullPointerException e) {
			this.masterSku = masterSku;
		} catch (Exception e) {
			System.out.println("masterSku: " + e.getMessage());
			this.masterSku = null;
		}

		try {
			this.upc = apiProduct.getUpc();
		} catch (NullPointerException e) {
			this.upc = upc;
		} catch (Exception e) {
			System.out.println("upc: " + e.getMessage());
			this.upc = null;
		}

		try {
			this.name = apiProduct.getName();
		} catch (NullPointerException e) {
			this.name = name;
		} catch (Exception e) {
			System.out.println("name: " + e.getMessage());
			this.name = null;
		}

		try {
			this.shortDesc = apiProduct.getShortDesc();
		} catch (NullPointerException e) {
			this.shortDesc = shortDesc;
		} catch (Exception e) {
			System.out.println("shortDesc: " + e.getMessage());
			this.shortDesc = null;
		}

		try {
			this.longDesc = apiProduct.getLongDesc();
		} catch (NullPointerException e) {
			this.longDesc = longDesc;
		} catch (Exception e) {
			System.out.println("longDesc: " + e.getMessage());
			this.longDesc = null;
		}

		try {
			this.active = apiProduct.isActive();
		} catch (NullPointerException e) {
			this.active = active;
		} catch (Exception e) {
			System.out.println("active: " + e.getMessage());
			// this.active = null;
		}

		try {
			this.packing = apiProduct.getPacking();
		} catch (NullPointerException e) {
			this.packing = packing;
		} catch (Exception e) {
			System.out.println("packing: " + e.getMessage());
			this.packing = null;
		}

		try {
			this.field1 = apiProduct.getField1();
		} catch (NullPointerException e) {
			this.field1 = field1;
		} catch (Exception e) {
			System.out.println("field1: " + e.getMessage());
			this.field1 = null;
		}

		try {
			this.field2 = apiProduct.getField2();
		} catch (NullPointerException e) {
			this.field2 = field2;
		} catch (Exception e) {
			System.out.println("field2: " + e.getMessage());
			this.field2 = null;
		}

		try {
			this.field3 = apiProduct.getField3();
		} catch (NullPointerException e) {
			this.field3 = field3;
		} catch (Exception e) {
			System.out.println("field3: " + e.getMessage());
			this.field3 = null;
		}

		try {
			this.field4 = apiProduct.getField4();
		} catch (NullPointerException e) {
			this.field4 = field4;
		} catch (Exception e) {
			System.out.println("field4: " + e.getMessage());
			this.field4 = null;
		}

		try {
			this.field5 = apiProduct.getField5();
		} catch (NullPointerException e) {
			this.field5 = field5;
		} catch (Exception e) {
			System.out.println("field5: " + e.getMessage());
			this.field5 = null;
		}

		try {
			this.field6 = apiProduct.getField6();
		} catch (NullPointerException e) {
			this.field6 = field6;
		} catch (Exception e) {
			System.out.println("field6: " + e.getMessage());
			this.field6 = null;
		}

		try {
			this.field7 = apiProduct.getField7();
		} catch (NullPointerException e) {
			this.field7 = field7;
		} catch (Exception e) {
			System.out.println("field7: " + e.getMessage());
			this.field7 = null;
		}

		try {
			this.field8 = apiProduct.getField8();
		} catch (NullPointerException e) {
			this.field8 = field8;
		} catch (Exception e) {
			System.out.println("field8: " + e.getMessage());
			this.field8 = null;
		}

		try {
			this.field9 = apiProduct.getField9();
		} catch (NullPointerException e) {
			this.field9 = field9;
		} catch (Exception e) {
			System.out.println("field9: " + e.getMessage());
			this.field9 = null;
		}

		try {
			this.field10 = apiProduct.getField10();
		} catch (NullPointerException e) {
			this.field10 = field10;
		} catch (Exception e) {
			System.out.println("field10: " + e.getMessage());
			this.field10 = null;
		}

		try {
			this.field11 = apiProduct.getField11();
		} catch (NullPointerException e) {
			this.field11 = field11;
		} catch (Exception e) {
			System.out.println("field11: " + e.getMessage());
			this.field11 = null;
		}

		try {
			this.field12 = apiProduct.getField12();
		} catch (NullPointerException e) {
			this.field12 = field12;
		} catch (Exception e) {
			System.out.println("field12: " + e.getMessage());
			this.field12 = null;
		}

		try {
			this.field13 = apiProduct.getField13();
		} catch (NullPointerException e) {
			this.field13 = field13;
		} catch (Exception e) {
			System.out.println("field13: " + e.getMessage());
			this.field13 = null;
		}

		try {
			this.field14 = apiProduct.getField14();
		} catch (NullPointerException e) {
			this.field14 = field14;
		} catch (Exception e) {
			System.out.println("field14: " + e.getMessage());
			this.field14 = null;
		}

		try {
			this.field15 = apiProduct.getField15();
		} catch (NullPointerException e) {
			this.field15 = field15;
		} catch (Exception e) {
			System.out.println("field15: " + e.getMessage());
			this.field15 = null;
		}

		try {
			this.field16 = apiProduct.getField16();
		} catch (NullPointerException e) {
			this.field16 = field16;
		} catch (Exception e) {
			System.out.println("field16: " + e.getMessage());
			this.field16 = null;
		}

		try {
			this.field17 = apiProduct.getField17();
		} catch (NullPointerException e) {
			this.field17 = field17;
		} catch (Exception e) {
			System.out.println("field17: " + e.getMessage());
			this.field17 = null;
		}

		try {
			this.field18 = apiProduct.getField18();
		} catch (NullPointerException e) {
			this.field18 = field18;
		} catch (Exception e) {
			System.out.println("field18: " + e.getMessage());
			this.field18 = null;
		}

		try {
			this.field19 = apiProduct.getField19();
		} catch (NullPointerException e) {
			this.field19 = field19;
		} catch (Exception e) {
			System.out.println("field19: " + e.getMessage());
			this.field19 = null;
		}

		try {
			this.field20 = apiProduct.getField20();
		} catch (NullPointerException e) {
			this.field20 = field20;
		} catch (Exception e) {
			System.out.println("field20: " + e.getMessage());
			this.field20 = null;
		}

		try {
			this.field21 = apiProduct.getField21();
		} catch (NullPointerException e) {
			this.field21 = field21;
		} catch (Exception e) {
			System.out.println("field21: " + e.getMessage());
			this.field21 = null;
		}

		try {
			this.field22 = apiProduct.getField22();
		} catch (NullPointerException e) {
			this.field22 = field22;
		} catch (Exception e) {
			System.out.println("field22: " + e.getMessage());
			this.field22 = null;
		}

		try {
			this.field23 = apiProduct.getField23();
		} catch (NullPointerException e) {
			this.field23 = field23;
		} catch (Exception e) {
			System.out.println("field23: " + e.getMessage());
			this.field23 = null;
		}

		try {
			this.field24 = apiProduct.getField24();
		} catch (NullPointerException e) {
			this.field24 = field24;
		} catch (Exception e) {
			System.out.println("field24: " + e.getMessage());
			this.field24 = null;
		}

		try {
			this.field25 = apiProduct.getField25();
		} catch (NullPointerException e) {
			this.field25 = field25;
		} catch (Exception e) {
			System.out.println("field25: " + e.getMessage());
			this.field25 = null;
		}

		try {
			this.field26 = apiProduct.getField26();
		} catch (NullPointerException e) {
			this.field26 = field26;
		} catch (Exception e) {
			System.out.println("field26: " + e.getMessage());
			this.field26 = null;
		}

		try {
			this.field27 = apiProduct.getField27();
		} catch (NullPointerException e) {
			this.field27 = field27;
		} catch (Exception e) {
			System.out.println("field27: " + e.getMessage());
			this.field27 = null;
		}

		try {
			this.field28 = apiProduct.getField28();
		} catch (NullPointerException e) {
			this.field28 = field28;
		} catch (Exception e) {
			System.out.println("field28: " + e.getMessage());
			this.field28 = null;
		}

		try {
			this.field29 = apiProduct.getField29();
		} catch (NullPointerException e) {
			this.field29 = field29;
		} catch (Exception e) {
			System.out.println("field29: " + e.getMessage());
			this.field29 = null;
		}

		try {
			this.field30 = apiProduct.getField30();
		} catch (NullPointerException e) {
			this.field30 = field30;
		} catch (Exception e) {
			System.out.println("field30: " + e.getMessage());
			this.field30 = null;
		}

		try {
			this.field31 = apiProduct.getField31();
		} catch (NullPointerException e) {
			this.field31 = field31;
		} catch (Exception e) {
			System.out.println("field31: " + e.getMessage());
			this.field31 = null;
		}

		try {
			this.field32 = apiProduct.getField32();
		} catch (NullPointerException e) {
			this.field32 = field32;
		} catch (Exception e) {
			System.out.println("field32: " + e.getMessage());
			this.field32 = null;
		}

		try {
			this.field33 = apiProduct.getField33();
		} catch (NullPointerException e) {
			this.field33 = field33;
		} catch (Exception e) {
			System.out.println("field33: " + e.getMessage());
			this.field33 = null;
		}

		try {
			this.field34 = apiProduct.getField34();
		} catch (NullPointerException e) {
			this.field34 = field34;
		} catch (Exception e) {
			System.out.println("field34: " + e.getMessage());
			this.field34 = null;
		}

		try {
			this.field35 = apiProduct.getField35();
		} catch (NullPointerException e) {
			this.field35 = field35;
		} catch (Exception e) {
			System.out.println("field35: " + e.getMessage());
			this.field35 = null;
		}

		try {
			this.field36 = apiProduct.getField36();
		} catch (NullPointerException e) {
			this.field36 = field36;
		} catch (Exception e) {
			System.out.println("field36: " + e.getMessage());
			this.field36 = null;
		}

		try {
			this.field37 = apiProduct.getField37();
		} catch (NullPointerException e) {
			this.field37 = field37;
		} catch (Exception e) {
			System.out.println("field37: " + e.getMessage());
			this.field37 = null;
		}

		try {
			this.field38 = apiProduct.getField38();
		} catch (NullPointerException e) {
			this.field3 = field38;
		} catch (Exception e) {
			System.out.println("field38: " + e.getMessage());
			this.field38 = null;
		}

		try {
			this.field39 = apiProduct.getField39();
		} catch (NullPointerException e) {
			this.field39 = field39;
		} catch (Exception e) {
			System.out.println("field39: " + e.getMessage());
			this.field39 = null;
		}

		try {
			this.field40 = apiProduct.getField40();
		} catch (NullPointerException e) {
			this.field40 = field40;
		} catch (Exception e) {
			System.out.println("field40: " + e.getMessage());
			this.field40 = null;
		}

		try {
			this.field41 = apiProduct.getField41();
		} catch (NullPointerException e) {
			this.field41 = field41;
		} catch (Exception e) {
			System.out.println("field41: " + e.getMessage());
			this.field41 = null;
		}

		try {
			this.field42 = apiProduct.getField42();
		} catch (NullPointerException e) {
			this.field42 = field42;
		} catch (Exception e) {
			System.out.println("field42: " + e.getMessage());
			this.field42 = null;
		}

		try {
			this.field43 = apiProduct.getField43();
		} catch (NullPointerException e) {
			this.field43 = field43;
		} catch (Exception e) {
			System.out.println("field43: " + e.getMessage());
			this.field43 = null;
		}

		try {
			this.field44 = apiProduct.getField44();
		} catch (NullPointerException e) {
			this.field44 = field44;
		} catch (Exception e) {
			System.out.println("field44: " + e.getMessage());
			this.field44 = null;
		}

		try {
			this.field45 = apiProduct.getField45();
		} catch (NullPointerException e) {
			this.field45 = field45;
		} catch (Exception e) {
			System.out.println("field45: " + e.getMessage());
			this.field45 = null;
		}

		try {
			this.field46 = apiProduct.getField46();
		} catch (NullPointerException e) {
			this.field46 = field46;
		} catch (Exception e) {
			System.out.println("field46: " + e.getMessage());
			this.field46 = null;
		}

		try {
			this.field47 = apiProduct.getField47();
		} catch (NullPointerException e) {
			this.field47 = field47;
		} catch (Exception e) {
			System.out.println("field47: " + e.getMessage());
			this.field47 = null;
		}

		try {
			this.field48 = apiProduct.getField48();
		} catch (NullPointerException e) {
			this.field48 = field48;
		} catch (Exception e) {
			System.out.println("field48: " + e.getMessage());
			this.field48 = null;
		}

		try {
			this.field49 = apiProduct.getField49();
		} catch (NullPointerException e) {
			this.field49 = field49;
		} catch (Exception e) {
			System.out.println("field49: " + e.getMessage());
			this.field49 = null;
		}

		try {
			this.field50 = apiProduct.getField50();
		} catch (NullPointerException e) {
			this.field50 = field50;
		} catch (Exception e) {
			System.out.println("field50: " + e.getMessage());
			this.field50 = null;
		}

		try {
			this.field51 = apiProduct.getField51();
		} catch (NullPointerException e) {
			this.field51 = field51;
		} catch (Exception e) {
			System.out.println("field51: " + e.getMessage());
			this.field51 = null;
		}

		try {
			this.field52 = apiProduct.getField52();
		} catch (NullPointerException e) {
			this.field52 = field52;
		} catch (Exception e) {
			System.out.println("field52: " + e.getMessage());
			this.field52 = null;
		}

		try {
			this.field53 = apiProduct.getField53();
		} catch (NullPointerException e) {
			this.field53 = field53;
		} catch (Exception e) {
			System.out.println("field53: " + e.getMessage());
			this.field53 = null;
		}

		try {
			this.field54 = apiProduct.getField54();
		} catch (NullPointerException e) {
			this.field54 = field54;
		} catch (Exception e) {
			System.out.println("field54: " + e.getMessage());
			this.field54 = null;
		}

		try {
			this.field55 = apiProduct.getField55();
		} catch (NullPointerException e) {
			this.field55 = field55;
		} catch (Exception e) {
			System.out.println("field55: " + e.getMessage());
			this.field55 = null;
		}

		try {
			this.field56 = apiProduct.getField56();
		} catch (NullPointerException e) {
			this.field56 = field56;
		} catch (Exception e) {
			System.out.println("field56: " + e.getMessage());
			this.field56 = null;
		}

		try {
			this.field57 = apiProduct.getField57();
		} catch (NullPointerException e) {
			this.field57 = field57;
		} catch (Exception e) {
			System.out.println("field57: " + e.getMessage());
			this.field57 = null;
		}

		try {
			this.field58 = apiProduct.getField58();
		} catch (NullPointerException e) {
			this.field58 = field58;
		} catch (Exception e) {
			System.out.println("field58: " + e.getMessage());
			this.field58 = null;
		}

		try {
			this.field59 = apiProduct.getField59();
		} catch (NullPointerException e) {
			this.field59 = field59;
		} catch (Exception e) {
			System.out.println("field59: " + e.getMessage());
			this.field59 = null;
		}

		try {
			this.field60 = apiProduct.getField60();
		} catch (NullPointerException e) {
			this.field60 = field60;
		} catch (Exception e) {
			System.out.println("field60: " + e.getMessage());
			this.field60 = null;
		}

		try {
			this.field61 = apiProduct.getField61();
		} catch (NullPointerException e) {
			this.field61 = field61;
		} catch (Exception e) {
			System.out.println("field61: " + e.getMessage());
			this.field61 = null;
		}

		try {
			this.field62 = apiProduct.getField62();
		} catch (NullPointerException e) {
			this.field62 = field62;
		} catch (Exception e) {
			System.out.println("field62: " + e.getMessage());
			this.field62 = null;
		}

		try {
			this.field63 = apiProduct.getField63();
		} catch (NullPointerException e) {
			this.field63 = field63;
		} catch (Exception e) {
			System.out.println("field63: " + e.getMessage());
			this.field63 = null;
		}

		try {
			this.field64 = apiProduct.getField64();
		} catch (NullPointerException e) {
			this.field64 = field64;
		} catch (Exception e) {
			System.out.println("field64: " + e.getMessage());
			this.field64 = null;
		}

		try {
			this.field65 = apiProduct.getField65();
		} catch (NullPointerException e) {
			this.field65 = field65;
		} catch (Exception e) {
			System.out.println("field65: " + e.getMessage());
			this.field65 = null;
		}

		try {
			this.field66 = apiProduct.getField66();
		} catch (NullPointerException e) {
			this.field66 = field66;
		} catch (Exception e) {
			System.out.println("field66: " + e.getMessage());
			this.field66 = null;
		}

		try {
			this.field67 = apiProduct.getField67();
		} catch (NullPointerException e) {
			this.field67 = field67;
		} catch (Exception e) {
			System.out.println("field67: " + e.getMessage());
			this.field67 = null;
		}

		try {
			this.field68 = apiProduct.getField68();
		} catch (NullPointerException e) {
			this.field68 = field68;
		} catch (Exception e) {
			System.out.println("field68: " + e.getMessage());
			this.field68 = null;
		}

		try {
			this.field69 = apiProduct.getField69();
		} catch (NullPointerException e) {
			this.field69 = field69;
		} catch (Exception e) {
			System.out.println("field69: " + e.getMessage());
			this.field69 = null;
		}

		try {
			this.field70 = apiProduct.getField70();
		} catch (NullPointerException e) {
			this.field70 = field70;
		} catch (Exception e) {
			System.out.println("field70: " + e.getMessage());
			this.field70 = null;
		}

		try {
			this.field71 = apiProduct.getField71();
		} catch (NullPointerException e) {
			this.field71 = field71;
		} catch (Exception e) {
			System.out.println("field71: " + e.getMessage());
			this.field71 = null;
		}

		try {
			this.field72 = apiProduct.getField72();
		} catch (NullPointerException e) {
			this.field72 = field72;
		} catch (Exception e) {
			System.out.println("field72: " + e.getMessage());
			this.field72 = null;
		}

		try {
			this.field73 = apiProduct.getField73();
		} catch (NullPointerException e) {
			this.field73 = field73;
		} catch (Exception e) {
			System.out.println("field73: " + e.getMessage());
			this.field73 = null;
		}

		try {
			this.field74 = apiProduct.getField74();
		} catch (NullPointerException e) {
			this.field7 = field74;
		} catch (Exception e) {
			System.out.println("field74: " + e.getMessage());
			this.field74 = null;
		}
		try {
			this.field75 = apiProduct.getField75();
		} catch (NullPointerException e) {
			this.field75 = field75;
		} catch (Exception e) {
			System.out.println("field71: " + e.getMessage());
			this.field75 = null;
		}

		try {
			this.field76 = apiProduct.getField76();
		} catch (NullPointerException e) {
			this.field76 = field76;
		} catch (Exception e) {
			System.out.println("field76: " + e.getMessage());
			this.field76 = null;
		}

		try {
			this.field77 = apiProduct.getField77();
		} catch (NullPointerException e) {
			this.field77 = field77;
		} catch (Exception e) {
			System.out.println("field77: " + e.getMessage());
			this.field77 = null;
		}

		try {
			this.field78 = apiProduct.getField78();
		} catch (NullPointerException e) {
			this.field78 = field78;
		} catch (Exception e) {
			System.out.println("field78: " + e.getMessage());
			this.field78 = null;
		}

		try {
			this.field79 = apiProduct.getField79();
		} catch (NullPointerException e) {
			this.field79 = field79;
		} catch (Exception e) {
			System.out.println("field79: " + e.getMessage());
			this.field79 = null;
		}

		try {
			this.field80 = apiProduct.getField80();
		} catch (NullPointerException e) {
			this.field80 = field80;
		} catch (Exception e) {
			System.out.println("field80: " + e.getMessage());
			this.field80 = null;
		}

		try {
			this.field81 = apiProduct.getField81();
		} catch (NullPointerException e) {
			this.field81 = field81;
		} catch (Exception e) {
			System.out.println("field81: " + e.getMessage());
			this.field81 = null;
		}

		try {
			this.field82 = apiProduct.getField82();
		} catch (NullPointerException e) {
			this.field82 = field82;
		} catch (Exception e) {
			System.out.println("field82: " + e.getMessage());
			this.field82 = null;
		}

		try {
			this.field83 = apiProduct.getField83();
		} catch (NullPointerException e) {
			this.field83 = field83;
		} catch (Exception e) {
			System.out.println("field83: " + e.getMessage());
			this.field83 = null;
		}

		try {
			this.field84 = apiProduct.getField84();
		} catch (NullPointerException e) {
			this.field84 = field84;
		} catch (Exception e) {
			System.out.println("field84: " + e.getMessage());
			this.field84 = null;
		}

		try {
			this.field85 = apiProduct.getField85();
		} catch (NullPointerException e) {
			this.field85 = field85;
		} catch (Exception e) {
			System.out.println("field85: " + e.getMessage());
			this.field85 = null;
		}

		try {
			this.field86 = apiProduct.getField86();
		} catch (NullPointerException e) {
			this.field86 = field86;
		} catch (Exception e) {
			System.out.println("field86: " + e.getMessage());
			this.field86 = null;
		}

		try {
			this.field87 = apiProduct.getField87();
		} catch (NullPointerException e) {
			this.field87 = field87;
		} catch (Exception e) {
			System.out.println("field87: " + e.getMessage());
			this.field87 = null;
		}

		try {
			this.field88 = apiProduct.getField88();
		} catch (NullPointerException e) {
			this.field88 = field88;
		} catch (Exception e) {
			System.out.println("field88: " + e.getMessage());
			this.field88 = null;
		}

		try {
			this.field89 = apiProduct.getField89();
		} catch (NullPointerException e) {
			this.field89 = field89;
		} catch (Exception e) {
			System.out.println("field89: " + e.getMessage());
			this.field89 = null;
		}

		try {
			this.field90 = apiProduct.getField90();
		} catch (NullPointerException e) {
			this.field90 = field90;
		} catch (Exception e) {
			System.out.println("field90: " + e.getMessage());
			this.field90 = null;
		}

		try {
			this.field91 = apiProduct.getField91();
		} catch (NullPointerException e) {
			this.field91 = field91;
		} catch (Exception e) {
			System.out.println("field91: " + e.getMessage());
			this.field91 = null;
		}

		try {
			this.field92 = apiProduct.getField92();
		} catch (NullPointerException e) {
			this.field92 = field92;
		} catch (Exception e) {
			System.out.println("field92: " + e.getMessage());
			this.field92 = null;
		}

		try {
			this.field93 = apiProduct.getField93();
		} catch (NullPointerException e) {
			this.field93 = field93;
		} catch (Exception e) {
			System.out.println("field93: " + e.getMessage());
			this.field93 = null;
		}

		try {
			this.field94 = apiProduct.getField94();
		} catch (NullPointerException e) {
			this.field94 = field94;
		} catch (Exception e) {
			System.out.println("field94: " + e.getMessage());
			this.field94 = null;
		}

		try {
			this.field95 = apiProduct.getField95();
		} catch (NullPointerException e) {
			this.field95 = field95;
		} catch (Exception e) {
			System.out.println("field95: " + e.getMessage());
			this.field95 = null;
		}

		try {
			this.field96 = apiProduct.getField96();
		} catch (NullPointerException e) {
			this.field96 = field96;
		} catch (Exception e) {
			System.out.println("field96: " + e.getMessage());
			this.field96 = null;
		}

		try {
			this.field97 = apiProduct.getField97();
		} catch (NullPointerException e) {
			this.field97 = field97;
		} catch (Exception e) {
			System.out.println("field97: " + e.getMessage());
			this.field97 = null;
		}

		try {
			this.field98 = apiProduct.getField98();
		} catch (NullPointerException e) {
			this.field98 = field98;
		} catch (Exception e) {
			System.out.println("field98: " + e.getMessage());
			this.field98 = null;
		}

		try {
			this.field99 = apiProduct.getField99();
		} catch (NullPointerException e) {
			this.field99 = field99;
		} catch (Exception e) {
			System.out.println("field99: " + e.getMessage());
			this.field99 = null;
		}

		try {
			this.field100 = apiProduct.getField100();
		} catch (NullPointerException e) {
			this.field100 = field100;
		} catch (Exception e) {
			System.out.println("field100: " + e.getMessage());
			this.field100 = null;
		}

		try {
			this.price1 = apiProduct.getPrice1();
		} catch (NullPointerException e) {
			this.price1 = price1;
		} catch (Exception e) {
			System.out.println("price1: " + e.getMessage());
			this.price1 = null;
		}

		try {
			this.price2 = apiProduct.getPrice2();
		} catch (NullPointerException e) {
			this.price2 = price2;
		} catch (Exception e) {
			System.out.println("price2: " + e.getMessage());
			this.price2 = null;
		}

		try {
			this.price3 = apiProduct.getPrice3();
		} catch (NullPointerException e) {
			this.price3 = price3;
		} catch (Exception e) {
			System.out.println("price3: " + e.getMessage());
			this.price3 = null;
		}

		try {
			this.price4 = apiProduct.getPrice4();
		} catch (NullPointerException e) {
			this.price4 = price4;
		} catch (Exception e) {
			System.out.println("price4: " + e.getMessage());
			this.price4 = null;
		}

		try {
			this.price5 = apiProduct.getPrice5();
		} catch (NullPointerException e) {
			this.price5 = price5;
		} catch (Exception e) {
			System.out.println("price5: " + e.getMessage());
			this.price5 = null;
		}

		try {
			this.price6 = apiProduct.getPrice6();
		} catch (NullPointerException e) {
			this.price6 = price6;
		} catch (Exception e) {
			System.out.println("price6: " + e.getMessage());
			this.price6 = null;
		}

		try {
			this.price7 = apiProduct.getPrice7();
		} catch (NullPointerException e) {
			this.price7 = price7;
		} catch (Exception e) {
			System.out.println("price7: " + e.getMessage());
			this.price7 = null;
		}

		try {
			this.price8 = apiProduct.getPrice8();
		} catch (NullPointerException e) {
			this.price8 = price8;
		} catch (Exception e) {
			System.out.println("price8: " + e.getMessage());
			this.price8 = null;
		}

		try {
			this.price9 = apiProduct.getPrice9();
		} catch (NullPointerException e) {
			this.price9 = price9;
		} catch (Exception e) {
			System.out.println("price9: " + e.getMessage());
			this.price9 = null;
		}

		try {
			this.price10 = apiProduct.getPrice10();
		} catch (NullPointerException e) {
			this.price10 = price10;
		} catch (Exception e) {
			System.out.println("price10: " + e.getMessage());
			this.price10 = null;
		}

		try {
			this.qtyBreak1 = apiProduct.getQtyBreak1();
		} catch (NullPointerException e) {
			this.qtyBreak1 = qtyBreak1;
		} catch (Exception e) {
			System.out.println("qtyBreak1: " + e.getMessage());
			this.qtyBreak1 = null;
		}

		try {
			this.qtyBreak2 = apiProduct.getQtyBreak2();
		} catch (NullPointerException e) {
			this.qtyBreak2 = qtyBreak2;
		} catch (Exception e) {
			System.out.println("qtyBreak2: " + e.getMessage());
			this.qtyBreak2 = null;
		}

		try {
			this.qtyBreak3 = apiProduct.getQtyBreak3();
		} catch (NullPointerException e) {
			this.qtyBreak3 = qtyBreak3;
		} catch (Exception e) {
			System.out.println("qtyBreak3: " + e.getMessage());
			this.qtyBreak3 = null;
		}

		try {
			this.qtyBreak4 = apiProduct.getQtyBreak4();
		} catch (NullPointerException e) {
			this.qtyBreak4 = qtyBreak4;
		} catch (Exception e) {
			System.out.println("qtyBreak4: " + e.getMessage());
			this.qtyBreak4 = null;
		}

		try {
			this.qtyBreak5 = apiProduct.getQtyBreak5();
		} catch (NullPointerException e) {
			this.qtyBreak5 = qtyBreak5;
		} catch (Exception e) {
			System.out.println("qtyBreak5: " + e.getMessage());
			this.qtyBreak5 = null;
		}

		try {
			this.qtyBreak6 = apiProduct.getQtyBreak6();
		} catch (NullPointerException e) {
			this.qtyBreak6 = qtyBreak6;
		} catch (Exception e) {
			System.out.println("qtyBreak6: " + e.getMessage());
			this.qtyBreak6 = null;
		}

		try {
			this.qtyBreak7 = apiProduct.getQtyBreak7();
		} catch (NullPointerException e) {
			this.qtyBreak7 = qtyBreak7;
		} catch (Exception e) {
			System.out.println("qtyBreak7: " + e.getMessage());
			this.qtyBreak7 = null;
		}

		try {
			this.qtyBreak8 = apiProduct.getQtyBreak8();
		} catch (NullPointerException e) {
			this.qtyBreak8 = qtyBreak8;
		} catch (Exception e) {
			System.out.println("qtyBreak8: " + e.getMessage());
			this.qtyBreak8 = null;
		}

		try {
			this.qtyBreak9 = apiProduct.getQtyBreak9();
		} catch (NullPointerException e) {
			this.qtyBreak9 = qtyBreak9;
		} catch (Exception e) {
			System.out.println("qtyBreak9: " + e.getMessage());
			this.qtyBreak9 = null;
		}

		try {
			this.packageL = apiProduct.getPackageL();
		} catch (NullPointerException e) {
			this.packageL = packageL;
		} catch (Exception e) {
			System.out.println("packageL: " + e.getMessage());
			this.packageL = null;
		}

		try {
			this.packageW = apiProduct.getPackageW();
		} catch (NullPointerException e) {
			this.packageW = packageW;
		} catch (Exception e) {
			System.out.println("packageW: " + e.getMessage());
			this.packageW = null;
		}

		try {
			this.packageH = apiProduct.getPackageH();
		} catch (NullPointerException e) {
			this.packageH = packageH;
		} catch (Exception e) {
			System.out.println("packageH: " + e.getMessage());
			this.packageH = null;
		}

		try {
			this.caseContent = apiProduct.getCaseContent();
		} catch (NullPointerException e) {
			this.caseContent = caseContent;
		} catch (Exception e) {
			System.out.println("caseContent: " + e.getMessage());
			this.caseContent = null;
		}

		try {
			this.inventory = apiProduct.getInventory();
		} catch (NullPointerException e) {
			this.inventory = inventory;
		} catch (Exception e) {
			System.out.println("inventory: " + e.getMessage());
			this.inventory = null;
		}

		try {
			this.inventoryAFS = apiProduct.getInventoryAFS();
		} catch (NullPointerException e) {
			this.inventoryAFS = inventoryAFS;
		} catch (Exception e) {
			System.out.println("inventoryAFS: " + e.getMessage());
			this.inventoryAFS = null;
		}

		try {
			this.negInventory = apiProduct.isNegInventory() == null ? false : apiProduct.isNegInventory();
		} catch (NullPointerException e) {
			this.negInventory = negInventory;
		} catch (Exception e) {
			System.out.println("negInventory: " + e.getMessage());
			this.negInventory = (Boolean) null;
		}

		try {
			this.lowInventory = apiProduct.getLowInventory();
		} catch (NullPointerException e) {
			this.lowInventory = lowInventory;
		} catch (Exception e) {
			System.out.println("lowInventory: " + e.getMessage());
			this.lowInventory = null;
		}

		try {
			this.showNegInventory = apiProduct.isShowNegInventory() == null ? false : apiProduct.isShowNegInventory();
		} catch (NullPointerException e) {
			this.showNegInventory = showNegInventory;
		} catch (Exception e) {
			System.out.println("lowInventory: " + e.getMessage());
			this.lowInventory = null;
		}

		try {
			this.minimumQty = apiProduct.getMinimumQty();
		} catch (NullPointerException e) {
			this.minimumQty = minimumQty;
		} catch (Exception e) {
			System.out.println("minimumQty: " + e.getMessage());
			this.minimumQty = null;
		}

		try {
			this.incrementalQty = apiProduct.getIncrementalQty();
		} catch (NullPointerException e) {
			this.incrementalQty = incrementalQty;
		} catch (Exception e) {
			System.out.println("incrementalQty: " + e.getMessage());
			this.incrementalQty = null;
		}

		try {
			this.incrementalQty = apiProduct.getIncrementalQty();
		} catch (NullPointerException e) {
			this.incrementalQty = incrementalQty;
		} catch (Exception e) {
			System.out.println("incrementalQty: " + e.getMessage());
			this.incrementalQty = null;
		}

		try {
			this.upsMaxItemsInPackage = apiProduct.getUpsMaxItemsInPackage();
		} catch (NullPointerException e) {
			this.upsMaxItemsInPackage = upsMaxItemsInPackage;
		} catch (Exception e) {
			System.out.println("upsMaxItemsInPackage: " + e.getMessage());
			this.upsMaxItemsInPackage = null;
		}

		try {
			this.uspsMaxItemsInPackage = apiProduct.getUspsMaxItemsInPackage();
		} catch (NullPointerException e) {
			this.uspsMaxItemsInPackage = upsMaxItemsInPackage;
		} catch (Exception e) {
			System.out.println("upsMaxItemsInPackage: " + e.getMessage());
			this.upsMaxItemsInPackage = null;
		}

		try {
			this.defaultSupplierId = apiProduct.getDefaultSupplierId();
		} catch (NullPointerException e) {
			this.defaultSupplierId = defaultSupplierId;
		} catch (Exception e) {
			System.out.println("defaultSupplierId: " + e.getMessage());
			this.defaultSupplierId = 0;
		}

		try {
			this.fobZipCode = apiProduct.getFobZipCode();
		} catch (NullPointerException e) {
			this.fobZipCode = fobZipCode;
		} catch (Exception e) {
			System.out.println("fobZipCode: " + e.getMessage());
			this.fobZipCode = null;
		}

		try {
			this.softLink = apiProduct.isSoftLink();
		} catch (NullPointerException e) {
			this.softLink = softLink;
		} catch (Exception e) {
			System.out.println("softLink: " + e.getMessage());
			// this.softLink = null;
		}

		try {
			this.categoryIds = apiProduct.getCategoryIds();
		} catch (NullPointerException e) {
			this.categoryIds = categoryIds;
		} catch (Exception e) {
			System.out.println("categoryIds: " + e.getMessage());
			this.categoryIds = null;
		}

		try {
			this.redirectUrlEn = apiProduct.getRedirectUrlEn();
		} catch (NullPointerException e) {
			this.redirectUrlEn = redirectUrlEn;
		} catch (Exception e) {
			System.out.println("redirectUrlEn: " + e.getMessage());
			this.redirectUrlEn = null;
		}

		try {
			this.redirectUrlEs = apiProduct.getRedirectUrlEs();
		} catch (NullPointerException e) {
			this.redirectUrlEs = redirectUrlEs;
		} catch (Exception e) {
			System.out.println("redirectUrlEs: " + e.getMessage());
			this.redirectUrlEs = null;
		}

		try {
			this.classField = apiProduct.getClassField();
		} catch (NullPointerException e) {
			this.classField = classField;
		} catch (Exception e) {
			System.out.println("classField: " + e.getMessage());
			this.classField = null;
		}

		try {
			this.i18nProduct = apiProduct.getI18nProduct();
		} catch (NullPointerException e) {
			this.i18nProduct = i18nProduct;
		} catch (Exception e) {
			System.out.println("i18nProduct: " + e.getMessage());
			this.i18nProduct = null;
		}

	}

	private static final long serialVersionUID = 1L;
	@XmlAttribute
	private Integer id;
	private Set<Object> supplierIds = new HashSet<Object>();
	private String name;
	@XmlElement(name = "sku", required = true)
	private String sku;
	private String parentSku;
	private String parentId;
	private String note;
	private String redirectUrlEn;
	private String redirectUrlEs;

	private boolean restrictPriceChange = false;

	// master sku
	private String masterSku;
	@XmlTransient
	private Integer slaveCount;
	@XmlTransient
	private Double slaveLowestPrice;
	@XmlTransient
	private Double slaveHighestPrice;

	// etilize
	private String upc;
	private Integer etilizeId;

	private Integer asiId;
	private String asiXML;
	private Integer asiUniqueId;
	private boolean feedFreeze;
	private boolean asiIgnorePrice;

	private String shortDesc;
	private String longDesc;
	private Double weight;
	private Double msrp;
	private boolean hideMsrp;
	private List<Price> price;
	// price enter by customer
	private boolean priceByCustomer;
	private Set<Object> catIds = new HashSet<Object>();
	private Set<String> breadCrumb = new HashSet<String>();
	private String categoryIds;
	// price breaks
	private Double price1;

	private Double originalPriceRangeMinimum;
	private Double originalPriceRangeMaximum;

	private String caseUnitTitle;

	private Double price2;
	private Double price3;
	private Double price4;
	private Double price5;
	private Double price6;
	private Double price7;
	private Double price8;
	private Double price9;
	private Double price10;
	private Double cost1;
	private Double cost2;
	private Double cost3;
	private Double cost4;
	private Double cost5;
	private Double cost6;
	private Double cost7;
	private Double cost8;
	private Double cost9;
	private Double cost10;
	private Double marginPercent1;
	private Double marginPercent2;
	private Double marginPercent3;
	private Double marginPercent4;
	private Double marginPercent5;
	private Double marginPercent6;
	private Double marginPercent7;
	private Double marginPercent8;
	private Double marginPercent9;
	private Double marginPercent10;
	private Integer qtyBreak1;
	private Integer qtyBreak2;
	private Integer qtyBreak3;
	private Integer qtyBreak4;
	private Integer qtyBreak5;
	private Integer qtyBreak6;
	private Integer qtyBreak7;
	private Integer qtyBreak8;
	private Integer qtyBreak9;
	// price breaks
	private Double priceTable1;
	private Double priceTable2;
	private Double priceTable3;
	private Double priceTable4;
	private Double priceTable5;
	private Double priceTable6;
	private Double priceTable7;
	private Double priceTable8;
	private Double priceTable9;
	private Double priceTable10;
	// price case pack
	private boolean priceCasePack;
	// not saved in DB, but get the value from one of the fields
	private Integer priceCasePackQty;
	private Double priceCasePack1;
	private Double priceCasePack2;
	private Double priceCasePack3;
	private Double priceCasePack4;
	private Double priceCasePack5;
	private Double priceCasePack6;
	private Double priceCasePack7;
	private Double priceCasePack8;
	private Double priceCasePack9;
	private Double priceCasePack10;

	private boolean quote;
	// commission table
	@XmlTransient
	private ProductAffiliateCommission commissionTables;
	// images
	private ProductImage thumbnail;
	private List<ProductImage> images;
	private String imageFolder;
	private String imageLayout;
	// dates
	private Date created;
	private Date lastModified;
	private String createdBy;
	private String lastModifiedBy;
	// extra fields
	private String field1;
	private String field2;
	private String field3;
	private String field4;
	private String field5;
	private String field6;
	private String field7;
	private String field8;
	private String field9;
	private String field10;
	private String field11;
	private String field12;
	private String field13;
	private String field14;
	private String field15;
	private String field16;
	private String field17;
	private String field18;
	private String field19;
	private String field20;
	private String field21;
	private String field22;
	private String field23;
	private String field24;
	private String field25;
	private String field26;
	private String field27;
	private String field28;
	private String field29;
	private String field30;
	private String field31;
	private String field32;
	private String field33;
	private String field34;
	private String field35;
	private String field36;
	private String field37;
	private String field38;
	private String field39;
	private String field40;
	private String field41;
	private String field42;
	private String field43;
	private String field44;
	private String field45;
	private String field46;
	private String field47;
	private String field48;
	private String field49;
	private String field50;
	private String field51;
	private String field52;
	private String field53;
	private String field54;
	private String field55;
	private String field56;
	private String field57;
	private String field58;
	private String field59;
	private String field60;
	private String field61;
	private String field62;
	private String field63;
	private String field64;
	private String field65;
	private String field66;
	private String field67;
	private String field68;
	private String field69;
	private String field70;
	private String field71;
	private String field72;
	private String field73;
	private String field74;
	private String field75;
	private String field76;
	private String field77;
	private String field78;
	private String field79;
	private String field80;
	private String field81;
	private String field82;
	private String field83;
	private String field84;
	private String field85;
	private String field86;
	private String field87;
	private String field88;
	private String field89;
	private String field90;
	private String field91;
	private String field92;
	private String field93;
	private String field94;
	private String field95;
	private String field96;
	private String field97;
	private String field98;
	private String field99;
	private String field100;
	private List<ProductField> productFields;
	// keywords
	private String keywords = "";
	// protected
	private String protectedLevel = "0";
	private List<ProductOption> productOptions;
	// Kit
	@XmlTransient
	private List<KitParts> kitParts;

	// SalesTag
	@XmlTransient
	private SalesTag salesTag;
	@XmlTransient
	private Integer salesTagId;
	// Mail In Rebate
	@XmlTransient
	private MailInRebate mailInRebate;
	@XmlTransient
	private Integer rebateId;
	// unit
	private Integer caseContent;
	private String packing;
	private Integer minimumQty;
	private Integer incrementalQty;

	private boolean loginRequire;
	private boolean hidePrice;
	// myevergreen if PriceTable6 is empty hide price and show an image. it is
	// better to hide the product.
	private boolean altLoginRequire;
	// recommended items
	private String recommendedList;
	private String recommendedListTitle;
	private String recommendedListDisplay;
	// also consider
	private String alsoConsider;
	// Category-Product rank
	private Integer rank;
	// Category-Product rank
	private Integer searchRank;
	// layout
	private boolean hideHeader;
	private boolean hideTopBar;
	private boolean hideLeftBar;
	private boolean hideRightBar;
	private boolean hideFooter;
	private boolean hideBreadCrumbs;
	private String headTag;
	private String htmlAddToCart;
	private String productLayout;
	// add To List
	private boolean addToList;
	private boolean addToPresentation;
	// box
	private Integer boxSize;
	private Double boxExtraAmt;
	// Product Type
	@XmlTransient
	private String type = "";
	private Integer optionIndex;
	// weather channel
	private Integer temperature;
	// product comparison
	private boolean compare;
	// views
	private int viewed;
	// special pricing
	private Double specialPricing;
	private boolean taxable = true;
	private boolean searchable = true;

	private Boolean includeRetailDisplay;
	private Boolean includeRetailEnabled;

	private String optionCode;
	// inventory
	private Integer inventory;
	private boolean negInventory;
	private Integer lowInventory;
	private boolean showNegInventory;
	private Integer inventoryAFS;
	private boolean backToNull;
	private Integer orderPendingQty;
	private Integer purchasePendingQty;
	// loyalty
	private Double loyaltyPoint;
	// custom shipping
	private boolean customShippingEnabled = true;
	private Integer numCustomLines;
	private Integer customLineCharacter;

	// subscription
	private boolean subscription;
	private Double subscriptionDiscount;

	// cross items quantity code (for price breaks)
	private String crossItemsCode;

	// active product
	private boolean active = true;
	// Tabs
	private String tab1;
	private String tab1Content;
	private String tab2;
	private String tab2Content;
	private String tab3;
	private String tab3Content;
	private String tab4;
	private String tab4Content;
	private String tab5;
	private String tab5Content;
	private String tab6;
	private String tab6Content;
	private String tab7;
	private String tab7Content;
	private String tab8;
	private String tab8Content;
	private String tab9;
	private String tab9Content;
	private String tab10;
	private String tab10Content;
	// Manufacturer
	private String manufactureName;
	// Review
	@XmlTransient
	private ProductReview rate;
	private boolean enableRate = true;

	// i18n
	private String lang;
	private String i18nName;
	private String i18nShortDesc;
	private String i18nLongDesc;
	private Map<String, Product> i18nProduct;

	private String feed;

	// Deal
	@XmlTransient
	private Deal deal;

	// package dimensions (for shipping calculations)
	private Double packageL;
	private Double packageW;
	private Double packageH;
	private Double upsMaxItemsInPackage;
	private Double uspsMaxItemsInPackage;

	// listType
	private Integer listType;

	// pdfs (etilize)
	private Map<String, String> pdfs;

	private boolean endQtyPricing;

	private boolean retainOnCart = true;

	// SEO
	private Double siteMapPriority = 0.5;

	// product variant
	private List<Product> variants;

	// consignment product
	private int defaultSupplierId;
	private String defaultSupplierName;
	private Double cost;
	private Boolean costPercent;

	// dropdown Helper
	private Integer dropDownBegin;
	private Integer dropDownEnd;
	private Integer dropDownStep;

	private String alt;
	private String fobZipCode;

	// For via trading, on frontend view&retrieve manifests if sku belongs to Load
	// Center(cid=738), add a link to view manifestdetails.php.
	private boolean belongsToLoadCenter;

	private boolean softLink;

	private String classField;

	private String program;

	public boolean isSoftLink() {
		return softLink;
	}

	public void setSoftLink(boolean softLink) {
		this.softLink = softLink;
	}

	public Double getCost() {
		return cost;
	}

	public void setCost(Double cost) {
		this.cost = cost;
	}

	public boolean isActive() {
		return active;
	}

	public void setActive(boolean active) {
		this.active = active;
	}

	public Integer getNumCustomLines() {
		return numCustomLines;
	}

	public void setNumCustomLines(Integer numCustomLines) {
		this.numCustomLines = numCustomLines;
	}

	public Double getLoyaltyPoint() {
		return loyaltyPoint;
	}

	public void setLoyaltyPoint(Double loyaltyPoint) {
		this.loyaltyPoint = loyaltyPoint;
	}

	public Integer getLowInventory() {
		return lowInventory;
	}

	public void setLowInventory(Integer lowInventory) {
		this.lowInventory = lowInventory;
	}

	public Integer getInventory() {
		return inventory;
	}

	public void setInventory(Integer inventory) {
		this.inventory = inventory;
	}

	public Integer getInventoryAFS() {
		return inventoryAFS;
	}

	public void setInventoryAFS(Integer inventoryAFS) {
		this.inventoryAFS = inventoryAFS;
	}

	public boolean isNegInventory() {
		return negInventory;
	}

	public boolean isBackToNull() {
		return backToNull;
	}

	public void setBackToNull(boolean backToNull) {
		this.backToNull = backToNull;
	}

	public Integer getOrderPendingQty() {
		return orderPendingQty;
	}

	public void setOrderPendingQty(Integer orderPendingQty) {
		this.orderPendingQty = orderPendingQty;
	}

	public Integer getPurchasePendingQty() {
		return purchasePendingQty;
	}

	public void setPurchasePendingQty(Integer purchasePendingQty) {
		this.purchasePendingQty = purchasePendingQty;
	}

	public void setNegInventory(boolean negInventory) {
		this.negInventory = negInventory;
	}

	public boolean isTaxable() {
		return taxable;
	}

	public void setTaxable(boolean taxable) {
		this.taxable = taxable;
	}

	public Double getSpecialPricing() {
		return specialPricing;
	}

	public void setSpecialPricing(Double specialPricing) {
		this.specialPricing = specialPricing;
	}

	public Integer getTemperature() {
		return temperature;
	}

	public void setTemperature(Integer temperature) {
		this.temperature = temperature;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public boolean isAddToList() {
		return addToList;
	}

	public void setAddToList(boolean addToList) {
		this.addToList = addToList;
	}

	public String getHeadTag() {
		return headTag;
	}

	public void setHeadTag(String headTag) {
		this.headTag = headTag;
	}

	public void setHtmlAddToCart(String htmlAddToCart) {
		this.htmlAddToCart = htmlAddToCart;
	}

	public String getHtmlAddToCart() {
		return htmlAddToCart;
	}

	public void setProductLayout(String productLayout) {
		this.productLayout = productLayout;
	}

	public String getProductLayout() {
		return productLayout;
	}

	public boolean isHideBreadCrumbs() {
		return hideBreadCrumbs;
	}

	public void setHideBreadCrumbs(boolean hideBreadCrumbs) {
		this.hideBreadCrumbs = hideBreadCrumbs;
	}

	public boolean isHideFooter() {
		return hideFooter;
	}

	public void setHideFooter(boolean hideFooter) {
		this.hideFooter = hideFooter;
	}

	public boolean isHideHeader() {
		return hideHeader;
	}

	public void setHideHeader(boolean hideHeader) {
		this.hideHeader = hideHeader;
	}

	public boolean isHideLeftBar() {
		return hideLeftBar;
	}

	public void setHideLeftBar(boolean hideLeftBar) {
		this.hideLeftBar = hideLeftBar;
	}

	public boolean isHideRightBar() {
		return hideRightBar;
	}

	public void setHideRightBar(boolean hideRightBar) {
		this.hideRightBar = hideRightBar;
	}

	public boolean isHideTopBar() {
		return hideTopBar;
	}

	public void setHideTopBar(boolean hideTopBar) {
		this.hideTopBar = hideTopBar;
	}

	public Integer getRank() {
		return rank;
	}

	public void setRank(Integer rank) {
		this.rank = rank;
	}

	public boolean isHidePrice() {
		return hidePrice;
	}

	public void setHidePrice(boolean hidePrice) {
		this.hidePrice = hidePrice;
	}

	public Integer getSalesTagId() {
		return salesTagId;
	}

	public void setSalesTagId(Integer salesTagId) {
		this.salesTagId = salesTagId;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public void setSupplierIds(Set<Object> supplierIds) {
		this.supplierIds = supplierIds;
	}

	public Set<Object> getSupplierIds() {
		return supplierIds;
	}

	public String getName() {
		return name;
	}

	public String getNameWithoutQuotes() {
		if (this.name != null && !this.name.isEmpty()) {
			StringBuffer buffer = new StringBuffer(this.name);
			StringBuffer output = new StringBuffer();
			for (int i = 0; i < buffer.length(); i++) {
				if (buffer.charAt(i) != '\"' && buffer.charAt(i) != '\'')
					output.append(buffer.charAt(i));
			}
			return output.toString();
		}
		return "";
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getNote() {
		return note;
	}

	public void setNote(String note) {
		this.note = note;
	}

	public String getSku() {
		return sku;
	}

	public void setSku(String sku) {
		this.sku = sku;
	}

	public String getShortDesc() {
		return shortDesc;
	}

	public void setShortDesc(String shortDesc) {
		this.shortDesc = shortDesc;
	}

	public String getLongDesc() {
		return longDesc;
	}

	public String getLongDescCustom() {
		String longDescCustom = "";
		if (longDesc != null) {
			longDescCustom = longDesc.substring(200);
		}
		return longDescCustom;
	}

	public void setLongDesc(String longDesc) {
		this.longDesc = longDesc;
	}

	public String getCategoryIds() {
		return categoryIds;
	}

	public void setCategoryIds(String categoryIds) {
		this.categoryIds = categoryIds;
	}

	public Set<Object> getCatIds() {
		return catIds;
	}

	public void setCatIds(Set<Object> catIds) {
		this.catIds = catIds;
	}

	public void addCategory(Category category) {
		if (category == null) {
			throw new IllegalArgumentException("Null category");
		}
		// category.getItems().add(this);
		// catIds.add(category);
	}

	public Double getMsrp() {
		return msrp;
	}

	public void setMsrp(Double msrp) {
		this.msrp = msrp;
	}

	public void setHideMsrp(boolean hideMsrp) {
		this.hideMsrp = hideMsrp;
	}

	public boolean isHideMsrp() {
		return hideMsrp;
	}

	public List<Price> getPrice() {
		return price;
	}

	public void setPrice(List<Price> price) {
		this.price = price;
	}

	public Double getPrice1() {
		return price1;
	}

	public void setPrice1(Double price1) {
		this.price1 = price1;
	}

	public Double getPrice2() {
		return price2;
	}

	public void setPrice2(Double price2) {
		this.price2 = price2;
	}

	public Double getPrice3() {
		return price3;
	}

	public void setPrice3(Double price3) {
		this.price3 = price3;
	}

	public Double getPrice4() {
		return price4;
	}

	public void setPrice4(Double price4) {
		this.price4 = price4;
	}

	public Double getPrice5() {
		return price5;
	}

	public void setPrice5(Double price5) {
		this.price5 = price5;
	}

	public Integer getQtyBreak1() {
		return qtyBreak1;
	}

	public void setQtyBreak1(Integer qtyBreak1) {
		this.qtyBreak1 = qtyBreak1;
	}

	public Integer getQtyBreak2() {
		return qtyBreak2;
	}

	public void setQtyBreak2(Integer qtyBreak2) {
		this.qtyBreak2 = qtyBreak2;
	}

	public Integer getQtyBreak3() {
		return qtyBreak3;
	}

	public void setQtyBreak3(Integer qtyBreak3) {
		this.qtyBreak3 = qtyBreak3;
	}

	public Integer getQtyBreak4() {
		return qtyBreak4;
	}

	public void setQtyBreak4(Integer qtyBreak4) {
		this.qtyBreak4 = qtyBreak4;
	}

	public List<ProductImage> getImages() {
		return images;
	}

	public void setImages(List<ProductImage> images) {
		this.images = images;
	}

	public int getImageCount() {
		if (this.images == null)
			return 0;
		else
			return this.images.size();
	}

	public void addImage(ProductImage image) {
		if (images == null)
			images = new ArrayList<ProductImage>();
		images.add(image);
	}

	public Date getCreated() {
		return created;
	}

	public void setCreated(Date created) {
		this.created = created;
	}

	public Date getLastModified() {
		return lastModified;
	}

	public void setLastModified(Date lastModified) {
		this.lastModified = lastModified;
	}

	public Double getSavingsAmount() {
		// if (price1 != null && msrp != null) {
		// if (msrp.compareTo(price1) > 0) {
		// return msrp.subtract(price1);
		// }
		// }

		return null;
	}

	public double getSavingsPercentage() {
		double msrp = this.msrp.doubleValue();
		double price1 = this.price1.doubleValue();
		double savings = msrp - price1;
		return (savings / msrp * 100);
	}

	public Double getPriceTable1() {
		return priceTable1;
	}

	public void setPriceTable1(Double priceTable1) {
		this.priceTable1 = priceTable1;
	}

	public Double getPriceTable2() {
		return priceTable2;
	}

	public void setPriceTable2(Double priceTable2) {
		this.priceTable2 = priceTable2;
	}

	public Double getPriceTable3() {
		return priceTable3;
	}

	public void setPriceTable3(Double priceTable3) {
		this.priceTable3 = priceTable3;
	}

	public Double getPriceTable4() {
		return priceTable4;
	}

	public void setPriceTable4(Double priceTable4) {
		this.priceTable4 = priceTable4;
	}

	public Double getPriceTable5() {
		return priceTable5;
	}

	public void setPriceTable5(Double priceTable5) {
		this.priceTable5 = priceTable5;
	}

	public String getField1() {
		return field1;
	}

	public Double getPriceTable6() {
		return priceTable6;
	}

	public void setPriceTable6(Double priceTable6) {
		this.priceTable6 = priceTable6;
	}

	public Double getPriceTable7() {
		return priceTable7;
	}

	public void setPriceTable7(Double priceTable7) {
		this.priceTable7 = priceTable7;
	}

	public Double getPriceTable8() {
		return priceTable8;
	}

	public void setPriceTable8(Double priceTable8) {
		this.priceTable8 = priceTable8;
	}

	public Double getPriceTable9() {
		return priceTable9;
	}

	public void setPriceTable9(Double priceTable9) {
		this.priceTable9 = priceTable9;
	}

	public Double getPriceTable10() {
		return priceTable10;
	}

	public void setPriceTable10(Double priceTable10) {
		this.priceTable10 = priceTable10;
	}

	public boolean isPriceCasePack() {
		return priceCasePack;
	}

	public void setPriceCasePack(boolean priceCasePack) {
		this.priceCasePack = priceCasePack;
	}

	public Integer getPriceCasePackQty() {
		return priceCasePackQty;
	}

	public void setPriceCasePackQty(Integer priceCasePackQty) {
		this.priceCasePackQty = priceCasePackQty;
	}

	public Double getPriceCasePack1() {
		return priceCasePack1;
	}

	public void setPriceCasePack1(Double priceCasePack1) {
		this.priceCasePack1 = priceCasePack1;
	}

	public Double getPriceCasePack2() {
		return priceCasePack2;
	}

	public void setPriceCasePack2(Double priceCasePack2) {
		this.priceCasePack2 = priceCasePack2;
	}

	public Double getPriceCasePack3() {
		return priceCasePack3;
	}

	public void setPriceCasePack3(Double priceCasePack3) {
		this.priceCasePack3 = priceCasePack3;
	}

	public Double getPriceCasePack4() {
		return priceCasePack4;
	}

	public void setPriceCasePack4(Double priceCasePack4) {
		this.priceCasePack4 = priceCasePack4;
	}

	public Double getPriceCasePack5() {
		return priceCasePack5;
	}

	public void setPriceCasePack5(Double priceCasePack5) {
		this.priceCasePack5 = priceCasePack5;
	}

	public Double getPriceCasePack6() {
		return priceCasePack6;
	}

	public void setPriceCasePack6(Double priceCasePack6) {
		this.priceCasePack6 = priceCasePack6;
	}

	public Double getPriceCasePack7() {
		return priceCasePack7;
	}

	public void setPriceCasePack7(Double priceCasePack7) {
		this.priceCasePack7 = priceCasePack7;
	}

	public Double getPriceCasePack8() {
		return priceCasePack8;
	}

	public void setPriceCasePack8(Double priceCasePack8) {
		this.priceCasePack8 = priceCasePack8;
	}

	public Double getPriceCasePack9() {
		return priceCasePack9;
	}

	public void setPriceCasePack9(Double priceCasePack9) {
		this.priceCasePack9 = priceCasePack9;
	}

	public Double getPriceCasePack10() {
		return priceCasePack10;
	}

	public void setPriceCasePack10(Double priceCasePack10) {
		this.priceCasePack10 = priceCasePack10;
	}

	public void setField1(String field1) {
		this.field1 = field1;
	}

	public String getField2() {
		return field2;
	}

	public void setField2(String field2) {
		this.field2 = field2;
	}

	public String getField3() {
		return field3;
	}

	public void setField3(String field3) {
		this.field3 = field3;
	}

	public String getField4() {
		return field4;
	}

	public void setField4(String field4) {
		this.field4 = field4;
	}

	public String getField5() {
		return field5;
	}

	public void setField5(String field5) {
		this.field5 = field5;
	}

	public String getField6() {
		return field6;
	}

	public void setField6(String field6) {
		this.field6 = field6;
	}

	public String getField7() {
		return field7;
	}

	public void setField7(String field7) {
		this.field7 = field7;
	}

	public String getField8() {
		return field8;
	}

	public void setField8(String field8) {
		this.field8 = field8;
	}

	public String getField9() {
		return field9;
	}

	public void setField9(String field9) {
		this.field9 = field9;
	}

	public String getField10() {
		return field10;
	}

	public void setField10(String field10) {
		this.field10 = field10;
	}

	public String getField11() {
		return field11;
	}

	public void setField11(String field11) {
		this.field11 = field11;
	}

	public String getField12() {
		return field12;
	}

	public void setField12(String field12) {
		this.field12 = field12;
	}

	public String getField13() {
		return field13;
	}

	public void setField13(String field13) {
		this.field13 = field13;
	}

	public String getField14() {
		return field14;
	}

	public void setField14(String field14) {
		this.field14 = field14;
	}

	public String getField15() {
		return field15;
	}

	public void setField15(String field15) {
		this.field15 = field15;
	}

	public String getField16() {
		return field16;
	}

	public void setField16(String field16) {
		this.field16 = field16;
	}

	public String getField17() {
		return field17;
	}

	public void setField17(String field17) {
		this.field17 = field17;
	}

	public String getField18() {
		return field18;
	}

	public void setField18(String field18) {
		this.field18 = field18;
	}

	public String getField19() {
		return field19;
	}

	public void setField19(String field19) {
		this.field19 = field19;
	}

	public String getField20() {
		return field20;
	}

	public void setField20(String field20) {
		this.field20 = field20;
	}

	public List<ProductField> getProductFields() {
		return productFields;
	}

	public void setProductFields(List<ProductField> productFields) {
		this.productFields = productFields;
	}

	public void addProductField(ProductField productField) {
		if (productFields == null)
			productFields = new ArrayList<ProductField>();
		productFields.add(productField);
	}

	public String getKeywords() {
		return keywords;
	}

	public void setKeywords(String keywords) {
		this.keywords = keywords;
	}

	public String getProtectedLevel() {
		return protectedLevel;
	}

	public void setProtectedLevel(String protectedLevel) {
		this.protectedLevel = protectedLevel;
	}

	public List<ProductOption> getProductOptions() {
		return productOptions;
	}

	public void setProductOptions(List<ProductOption> productOptions) {
		this.productOptions = productOptions;
	}

	public void addProductOption(ProductOption productOption) {
		if (productOptions == null)
			productOptions = new ArrayList<ProductOption>();
		productOptions.add(productOption);
	}

	public List<KitParts> getKitParts() {
		return kitParts;
	}

	public void setKitParts(List<KitParts> kitParts) {
		this.kitParts = kitParts;
	}

	public void addkitParts(KitParts kitPart) {
		if (kitParts == null)
			kitParts = new ArrayList<KitParts>();
		kitParts.add(kitPart);
	}

	public ProductImage getThumbnail() {
		return thumbnail;
	}

	public void setThumbnail(ProductImage thumbnail) {
		this.thumbnail = thumbnail;
	}

	public String getField21() {
		return field21;
	}

	public void setField21(String field21) {
		this.field21 = field21;
	}

	public String getField22() {
		return field22;
	}

	public void setField22(String field22) {
		this.field22 = field22;
	}

	public String getField23() {
		return field23;
	}

	public void setField23(String field23) {
		this.field23 = field23;
	}

	public String getField24() {
		return field24;
	}

	public void setField24(String field24) {
		this.field24 = field24;
	}

	public String getField25() {
		return field25;
	}

	public void setField25(String field25) {
		this.field25 = field25;
	}

	public String getField26() {
		return field26;
	}

	public void setField26(String field26) {
		this.field26 = field26;
	}

	public String getField27() {
		return field27;
	}

	public void setField27(String field27) {
		this.field27 = field27;
	}

	public String getField28() {
		return field28;
	}

	public void setField28(String field28) {
		this.field28 = field28;
	}

	public String getField29() {
		return field29;
	}

	public void setField29(String field29) {
		this.field29 = field29;
	}

	public String getField30() {
		return field30;
	}

	public void setField30(String field30) {
		this.field30 = field30;
	}

	public String getField31() {
		return field31;
	}

	public void setField31(String field31) {
		this.field31 = field31;
	}

	public String getField32() {
		return field32;
	}

	public void setField32(String field32) {
		this.field32 = field32;
	}

	public String getField33() {
		return field33;
	}

	public void setField33(String field33) {
		this.field33 = field33;
	}

	public String getField34() {
		return field34;
	}

	public void setField34(String field34) {
		this.field34 = field34;
	}

	public String getField35() {
		return field35;
	}

	public void setField35(String field35) {
		this.field35 = field35;
	}

	public String getField36() {
		return field36;
	}

	public void setField36(String field36) {
		this.field36 = field36;
	}

	public String getField37() {
		return field37;
	}

	public void setField37(String field37) {
		this.field37 = field37;
	}

	public String getField38() {
		return field38;
	}

	public void setField38(String field38) {
		this.field38 = field38;
	}

	public String getField39() {
		return field39;
	}

	public void setField39(String field39) {
		this.field39 = field39;
	}

	public String getField40() {
		return field40;
	}

	public void setField40(String field40) {
		this.field40 = field40;
	}

	public String getField41() {
		return field41;
	}

	public void setField41(String field41) {
		this.field41 = field41;
	}

	public String getField42() {
		return field42;
	}

	public void setField42(String field42) {
		this.field42 = field42;
	}

	public String getField43() {
		return field43;
	}

	public void setField43(String field43) {
		this.field43 = field43;
	}

	public String getField44() {
		return field44;
	}

	public void setField44(String field44) {
		this.field44 = field44;
	}

	public String getField45() {
		return field45;
	}

	public void setField45(String field45) {
		this.field45 = field45;
	}

	public String getField46() {
		return field46;
	}

	public void setField46(String field46) {
		this.field46 = field46;
	}

	public String getField47() {
		return field47;
	}

	public void setField47(String field47) {
		this.field47 = field47;
	}

	public String getField48() {
		return field48;
	}

	public void setField48(String field48) {
		this.field48 = field48;
	}

	public String getField49() {
		return field49;
	}

	public void setField49(String field49) {
		this.field49 = field49;
	}

	public String getField50() {
		return field50;
	}

	public void setField50(String field50) {
		this.field50 = field50;
	}

	public String getField51() {
		return field51;
	}

	public void setField51(String field51) {
		this.field51 = field51;
	}

	public String getField52() {
		return field52;
	}

	public void setField52(String field52) {
		this.field52 = field52;
	}

	public String getField53() {
		return field53;
	}

	public void setField53(String field53) {
		this.field53 = field53;
	}

	public String getField54() {
		return field54;
	}

	public void setField54(String field54) {
		this.field54 = field54;
	}

	public String getField55() {
		return field55;
	}

	public void setField55(String field55) {
		this.field55 = field55;
	}

	public String getField56() {
		return field56;
	}

	public void setField56(String field56) {
		this.field56 = field56;
	}

	public String getField57() {
		return field57;
	}

	public void setField57(String field57) {
		this.field57 = field57;
	}

	public String getField58() {
		return field58;
	}

	public void setField58(String field58) {
		this.field58 = field58;
	}

	public String getField59() {
		return field59;
	}

	public void setField59(String field59) {
		this.field59 = field59;
	}

	public String getField60() {
		return field60;
	}

	public void setField60(String field60) {
		this.field60 = field60;
	}

	public String getField61() {
		return field61;
	}

	public void setField61(String field61) {
		this.field61 = field61;
	}

	public String getField62() {
		return field62;
	}

	public void setField62(String field62) {
		this.field62 = field62;
	}

	public String getField63() {
		return field63;
	}

	public void setField63(String field63) {
		this.field63 = field63;
	}

	public String getField64() {
		return field64;
	}

	public void setField64(String field64) {
		this.field64 = field64;
	}

	public String getField65() {
		return field65;
	}

	public void setField65(String field65) {
		this.field65 = field65;
	}

	public String getField66() {
		return field66;
	}

	public void setField66(String field66) {
		this.field66 = field66;
	}

	public String getField67() {
		return field67;
	}

	public void setField67(String field67) {
		this.field67 = field67;
	}

	public String getField68() {
		return field68;
	}

	public void setField68(String field68) {
		this.field68 = field68;
	}

	public String getField69() {
		return field69;
	}

	public void setField69(String field69) {
		this.field69 = field69;
	}

	public String getField70() {
		return field70;
	}

	public void setField70(String field70) {
		this.field70 = field70;
	}

	public String getField71() {
		return field71;
	}

	public void setField71(String field71) {
		this.field71 = field71;
	}

	public String getField72() {
		return field72;
	}

	public void setField72(String field72) {
		this.field72 = field72;
	}

	public String getField73() {
		return field73;
	}

	public void setField73(String field73) {
		this.field73 = field73;
	}

	public String getField74() {
		return field74;
	}

	public void setField74(String field74) {
		this.field74 = field74;
	}

	public String getField75() {
		return field75;
	}

	public void setField75(String field75) {
		this.field75 = field75;
	}

	public String getField76() {
		return field76;
	}

	public void setField76(String field76) {
		this.field76 = field76;
	}

	public String getField77() {
		return field77;
	}

	public void setField77(String field77) {
		this.field77 = field77;
	}

	public String getField78() {
		return field78;
	}

	public void setField78(String field78) {
		this.field78 = field78;
	}

	public String getField79() {
		return field79;
	}

	public void setField79(String field79) {
		this.field79 = field79;
	}

	public String getField80() {
		return field80;
	}

	public void setField80(String field80) {
		this.field80 = field80;
	}

	public String getField81() {
		return field81;
	}

	public void setField81(String field81) {
		this.field81 = field81;
	}

	public String getField82() {
		return field82;
	}

	public void setField82(String field82) {
		this.field82 = field82;
	}

	public String getField83() {
		return field83;
	}

	public void setField83(String field83) {
		this.field83 = field83;
	}

	public String getField84() {
		return field84;
	}

	public void setField84(String field84) {
		this.field84 = field84;
	}

	public String getField85() {
		return field85;
	}

	public void setField85(String field85) {
		this.field85 = field85;
	}

	public String getField86() {
		return field86;
	}

	public void setField86(String field86) {
		this.field86 = field86;
	}

	public String getField87() {
		return field87;
	}

	public void setField87(String field87) {
		this.field87 = field87;
	}

	public String getField88() {
		return field88;
	}

	public void setField88(String field88) {
		this.field88 = field88;
	}

	public String getField89() {
		return field89;
	}

	public void setField89(String field89) {
		this.field89 = field89;
	}

	public String getField90() {
		return field90;
	}

	public void setField90(String field90) {
		this.field90 = field90;
	}

	public String getField91() {
		return field91;
	}

	public void setField91(String field91) {
		this.field91 = field91;
	}

	public String getField92() {
		return field92;
	}

	public void setField92(String field92) {
		this.field92 = field92;
	}

	public String getField93() {
		return field93;
	}

	public void setField93(String field93) {
		this.field93 = field93;
	}

	public String getField94() {
		return field94;
	}

	public void setField94(String field94) {
		this.field94 = field94;
	}

	public String getField95() {
		return field95;
	}

	public void setField95(String field95) {
		this.field95 = field95;
	}

	public String getField96() {
		return field96;
	}

	public void setField96(String field96) {
		this.field96 = field96;
	}

	public String getField97() {
		return field97;
	}

	public void setField97(String field97) {
		this.field97 = field97;
	}

	public String getField98() {
		return field98;
	}

	public void setField98(String field98) {
		this.field98 = field98;
	}

	public String getField99() {
		return field99;
	}

	public void setField99(String field99) {
		this.field99 = field99;
	}

	public String getField100() {
		return field100;
	}

	public void setField100(String field100) {
		this.field100 = field100;
	}

	public Double getWeight() {
		return weight;
	}

	public void setWeight(Double weight) {
		this.weight = weight;
	}

	public SalesTag getSalesTag() {
		return salesTag;
	}

	public void setSalesTag(SalesTag salesTag) {
		this.salesTag = salesTag;
	}

	public void setMailInRebate(MailInRebate mailInRebate) {
		this.mailInRebate = mailInRebate;
	}

	public MailInRebate getMailInRebate() {
		return mailInRebate;
	}

	public void setRebateId(Integer rebateId) {
		this.rebateId = rebateId;
	}

	public Integer getRebateId() {
		return rebateId;
	}

	public boolean isHasPrice() {
		if (price.isEmpty()) {
			return false;
		}
		return true;
	}

	public Integer getPriceSize() {
		if (price == null || price.isEmpty()) {
			return 0;
		} else
			return price.size();
	}

	public boolean isLoginRequire() {
		return loginRequire;
	}

	public boolean isAltLoginRequire() {
		return altLoginRequire;
	}

	public void setAltLoginRequire(boolean altLoginRequire) {
		this.altLoginRequire = altLoginRequire;
	}

	public void setLoginRequire(boolean loginRequire) {
		this.loginRequire = loginRequire;
	}

	public String getRecommendedList() {
		return recommendedList;
	}

	public void setRecommendedList(String recommendedList) {
		this.recommendedList = recommendedList;
	}

	public String getAlsoConsider() {
		return alsoConsider;
	}

	public void setAlsoConsider(String alsoConsider) {
		this.alsoConsider = alsoConsider;
	}

	public Double getPriceRangeMinimum() {
		Double minimum = null;
		if (price != null && !price.isEmpty()) {
			if (salesTag != null) {
				minimum = ((Price) price.get(price.size() - 1)).getDiscountAmt();
			} else {
				minimum = ((Price) price.get(price.size() - 1)).getAmt();
			}
		}
		return minimum;
	}

	public Double getPriceRangeMaximum() {
		Double maximum = null;
		if (price != null && !price.isEmpty() && price.size() != 1) {
			if (salesTag != null) {
				maximum = ((Price) price.get(0)).getDiscountAmt();
			} else {
				maximum = ((Price) price.get(0)).getAmt();
			}
		}
		return maximum;
	}

	public Integer getProtectedLevelAsNumber() {
		if (protectedLevel.contains("1")) {
			return protectedLevel.length();
		} else {
			return 0;
		}
	}

	public Integer getCaseContent() {
		return caseContent;
	}

	public void setCaseContent(Integer caseContent) {
		this.caseContent = caseContent;
	}

	public String getPacking() {
		return packing;
	}

	public void setPacking(String packing) {
		this.packing = packing;
	}

	public ProductAffiliateCommission getCommissionTables() {
		return commissionTables;
	}

	public void setCommissionTables(ProductAffiliateCommission commissionTables) {
		this.commissionTables = commissionTables;
	}

	public String getRecommendedListDisplay() {
		return recommendedListDisplay;
	}

	public void setRecommendedListDisplay(String recommendedListDisplay) {
		this.recommendedListDisplay = recommendedListDisplay;
	}

	public String getRecommendedListTitle() {
		return recommendedListTitle;
	}

	public void setRecommendedListTitle(String recommendedListTitle) {
		this.recommendedListTitle = recommendedListTitle;
	}

	public String getImageFolder() {
		return imageFolder;
	}

	public void setImageFolder(String imageFolder) {
		this.imageFolder = imageFolder;
	}

	public Integer getBoxSize() {
		return boxSize;
	}

	public void setBoxSize(Integer boxSize) {
		this.boxSize = boxSize;
	}

	public boolean isCompare() {
		return compare;
	}

	public void setCompare(boolean compare) {
		this.compare = compare;
	}

	public int getViewed() {
		return viewed;
	}

	public void setViewed(int viewed) {
		this.viewed = viewed;
	}

	public String getImageLayout() {
		return imageLayout;
	}

	public void setImageLayout(String imageLayout) {
		this.imageLayout = imageLayout;
	}

	public boolean isPriceByCustomer() {
		return priceByCustomer;
	}

	public void setPriceByCustomer(boolean priceByCustomer) {
		this.priceByCustomer = priceByCustomer;
	}

	public boolean isSearchable() {
		return searchable;
	}

	public void setSearchable(boolean searchable) {
		this.searchable = searchable;
	}

	public Integer getOptionIndex() {
		return optionIndex;
	}

	public void setOptionIndex(Integer optionIndex) {
		this.optionIndex = optionIndex;
	}

	public String getOptionCode() {
		return optionCode;
	}

	public void setOptionCode(String optionCode) {
		this.optionCode = optionCode;
	}

	public Double getBoxExtraAmt() {
		return boxExtraAmt;
	}

	public void setBoxExtraAmt(Double boxExtraAmt) {
		this.boxExtraAmt = boxExtraAmt;
	}

	public boolean isShowNegInventory() {
		return showNegInventory;
	}

	public void setShowNegInventory(boolean showNegInventory) {
		this.showNegInventory = showNegInventory;
	}

	public boolean isCustomShippingEnabled() {
		return customShippingEnabled;
	}

	public void setCustomShippingEnabled(boolean customShippingEnabled) {
		this.customShippingEnabled = customShippingEnabled;
	}

	public Integer getCustomLineCharacter() {
		return customLineCharacter;
	}

	public boolean isSubscription() {
		return subscription;
	}

	public void setSubscription(boolean subscription) {
		this.subscription = subscription;
	}

	public void setCustomLineCharacter(Integer customLineCharacter) {
		this.customLineCharacter = customLineCharacter;
	}

	public boolean ishasOptionImage() {
		if (productOptions != null) {
			for (ProductOption productOption : productOptions) {
				if (productOption.ishasImage()) {
					return true;
				}
			}
		}
		return false;
	}

	public String getCrossItemsCode() {
		return crossItemsCode;
	}

	public void setCrossItemsCode(String crossItemsCode) {
		this.crossItemsCode = crossItemsCode;
	}

	public Double getSubscriptionDiscount() {
		return subscriptionDiscount;
	}

	public void setSubscriptionDiscount(Double subscriptionDiscount) {
		this.subscriptionDiscount = subscriptionDiscount;
	}

	public Double getPrice6() {
		return price6;
	}

	public void setPrice6(Double price6) {
		this.price6 = price6;
	}

	public Double getPrice7() {
		return price7;
	}

	public void setPrice7(Double price7) {
		this.price7 = price7;
	}

	public Double getPrice8() {
		return price8;
	}

	public void setPrice8(Double price8) {
		this.price8 = price8;
	}

	public Double getPrice9() {
		return price9;
	}

	public void setPrice9(Double price9) {
		this.price9 = price9;
	}

	public Double getPrice10() {
		return price10;
	}

	public void setPrice10(Double price10) {
		this.price10 = price10;
	}

	public Integer getQtyBreak5() {
		return qtyBreak5;
	}

	public void setQtyBreak5(Integer qtyBreak5) {
		this.qtyBreak5 = qtyBreak5;
	}

	public Integer getQtyBreak6() {
		return qtyBreak6;
	}

	public void setQtyBreak6(Integer qtyBreak6) {
		this.qtyBreak6 = qtyBreak6;
	}

	public Integer getQtyBreak7() {
		return qtyBreak7;
	}

	public void setQtyBreak7(Integer qtyBreak7) {
		this.qtyBreak7 = qtyBreak7;
	}

	public Integer getQtyBreak8() {
		return qtyBreak8;
	}

	public void setQtyBreak8(Integer qtyBreak8) {
		this.qtyBreak8 = qtyBreak8;
	}

	public Integer getQtyBreak9() {
		return qtyBreak9;
	}

	public void setQtyBreak9(Integer qtyBreak9) {
		this.qtyBreak9 = qtyBreak9;
	}

	public String getTab1() {
		return tab1;
	}

	public void setTab1(String tab1) {
		this.tab1 = tab1;
	}

	public String getTab2() {
		return tab2;
	}

	public void setTab2(String tab2) {
		this.tab2 = tab2;
	}

	public String getTab3() {
		return tab3;
	}

	public void setTab3(String tab3) {
		this.tab3 = tab3;
	}

	public String getTab4() {
		return tab4;
	}

	public void setTab4(String tab4) {
		this.tab4 = tab4;
	}

	public String getTab5() {
		return tab5;
	}

	public void setTab5(String tab5) {
		this.tab5 = tab5;
	}

	public String getTab6() {
		return tab6;
	}

	public void setTab6(String tab6) {
		this.tab6 = tab6;
	}

	public String getTab7() {
		return tab7;
	}

	public void setTab7(String tab7) {
		this.tab7 = tab7;
	}

	public String getTab8() {
		return tab8;
	}

	public void setTab8(String tab8) {
		this.tab8 = tab8;
	}

	public String getTab9() {
		return tab9;
	}

	public void setTab9(String tab9) {
		this.tab9 = tab9;
	}

	public String getTab10() {
		return tab10;
	}

	public void setTab10(String tab10) {
		this.tab10 = tab10;
	}

	public String getTab1Content() {
		return tab1Content;
	}

	public void setTab1Content(String tab1Content) {
		this.tab1Content = tab1Content;
	}

	public String getTab2Content() {
		return tab2Content;
	}

	public void setTab2Content(String tab2Content) {
		this.tab2Content = tab2Content;
	}

	public String getTab3Content() {
		return tab3Content;
	}

	public void setTab3Content(String tab3Content) {
		this.tab3Content = tab3Content;
	}

	public String getTab4Content() {
		return tab4Content;
	}

	public void setTab4Content(String tab4Content) {
		this.tab4Content = tab4Content;
	}

	public String getTab5Content() {
		return tab5Content;
	}

	public void setTab5Content(String tab5Content) {
		this.tab5Content = tab5Content;
	}

	public String getTab6Content() {
		return tab6Content;
	}

	public void setTab6Content(String tab6Content) {
		this.tab6Content = tab6Content;
	}

	public String getTab7Content() {
		return tab7Content;
	}

	public void setTab7Content(String tab7Content) {
		this.tab7Content = tab7Content;
	}

	public String getTab8Content() {
		return tab8Content;
	}

	public void setTab8Content(String tab8Content) {
		this.tab8Content = tab8Content;
	}

	public String getTab9Content() {
		return tab9Content;
	}

	public void setTab9Content(String tab9Content) {
		this.tab9Content = tab9Content;
	}

	public String getTab10Content() {
		return tab10Content;
	}

	public void setTab10Content(String tab10Content) {
		this.tab10Content = tab10Content;
	}

	public Integer getMinimumQty() {
		return minimumQty;
	}

	public void setMinimumQty(Integer minimumQty) {
		this.minimumQty = minimumQty;
	}

	public Integer getIncrementalQty() {
		return incrementalQty;
	}

	public void setIncrementalQty(Integer incrementalQty) {
		this.incrementalQty = incrementalQty;
	}

	public Integer getDropDownBegin() {
		return (minimumQty == null) ? getDropDownStep() : minimumQty;
	}

	public Integer getDropDownEnd() {
		if (getDropDownBegin() != 1) {
			return (10 * this.getDropDownStep()) + this.getDropDownBegin();
		} else {
			return 10 * this.getDropDownBegin();
		}
	}

	public Integer getDropDownStep() {
		return (incrementalQty == null) ? 1 : incrementalQty;
	}

	public void setDropDownBegin(Integer dropDownBegin) {
		this.dropDownBegin = dropDownBegin;
	}

	public void setDropDownEnd(Integer dropDownEnd) {
		this.dropDownEnd = dropDownEnd;
	}

	public void setDropDownStep(Integer dropDownStep) {
		this.dropDownStep = dropDownStep;
	}

	public String getMasterSku() {
		return masterSku;
	}

	public void setMasterSku(String masterSku) {
		this.masterSku = masterSku;
	}

	public Integer getSlaveCount() {
		return slaveCount;
	}

	public void setSlaveCount(Integer slaveCount) {
		this.slaveCount = slaveCount;
	}

	public Double getSlaveLowestPrice() {
		return slaveLowestPrice;
	}

	public void setSlaveLowestPrice(Double slaveLowestPrice) {
		this.slaveLowestPrice = slaveLowestPrice;
	}

	public Double getSlaveHighestPrice() {
		return slaveHighestPrice;
	}

	public void setSlaveHighestPrice(Double slaveHighestPrice) {
		this.slaveHighestPrice = slaveHighestPrice;
	}

	public String getManufactureName() {
		return manufactureName;
	}

	public void setManufactureName(String manufactureName) {
		this.manufactureName = manufactureName;
	}

	public ProductReview getRate() {
		return rate;
	}

	public void setRate(ProductReview rate) {
		this.rate = rate;
	}

	public String getLang() {
		return lang;
	}

	public void setLang(String lang) {
		this.lang = lang;
	}

	public String getI18nName() {
		return i18nName;
	}

	public void setI18nName(String name) {
		i18nName = name;
	}

	public String getI18nShortDesc() {
		return i18nShortDesc;
	}

	public void setI18nShortDesc(String shortDesc) {
		i18nShortDesc = shortDesc;
	}

	public String getI18nLongDesc() {
		return i18nLongDesc;
	}

	public void setI18nLongDesc(String longDesc) {
		i18nLongDesc = longDesc;
	}

	public Map<String, Product> getI18nProduct() {
		return i18nProduct;
	}

	public void setI18nProduct(Map<String, Product> product) {
		i18nProduct = product;
	}

	public void addI18nProduct(String lang, Product product) {
		if (i18nProduct == null) {
			i18nProduct = new HashMap<String, Product>();
		}
		i18nProduct.put(lang, product);
	}

	public void setFeed(String feed) {
		this.feed = feed;
	}

	public String getFeed() {
		return feed;
	}

	public void setEnableRate(boolean enableRate) {
		this.enableRate = enableRate;
	}

	public boolean isEnableRate() {
		return enableRate;
	}

	public void setDeal(Deal deal) {
		this.deal = deal;
	}

	public Deal getDeal() {
		return deal;
	}

	public String getUpc() {
		return upc;
	}

	public void setUpc(String upc) {
		this.upc = upc;
	}

	public Integer getEtilizeId() {
		return etilizeId;
	}

	public void setEtilizeId(Integer etilizeId) {
		this.etilizeId = etilizeId;
	}

	public Integer getAsiId() {
		return asiId;
	}

	public void setAsiId(Integer asiId) {
		this.asiId = asiId;
	}

	public String getAsiXML() {
		return asiXML;
	}

	public void setAsiXML(String asiXML) {
		this.asiXML = asiXML;
	}

	public Integer getAsiUniqueId() {
		return asiUniqueId;
	}

	public void setAsiUniqueId(Integer asiUniqueId) {
		this.asiUniqueId = asiUniqueId;
	}

	public boolean isFeedFreeze() {
		return feedFreeze;
	}

	public void setFeedFreeze(boolean feedFreeze) {
		this.feedFreeze = feedFreeze;
	}

	public Double getPackageL() {
		return packageL;
	}

	public void setPackageL(Double packageL) {
		this.packageL = packageL;
	}

	public Double getPackageW() {
		return packageW;
	}

	public void setPackageW(Double packageW) {
		this.packageW = packageW;
	}

	public Double getPackageH() {
		return packageH;
	}

	public void setPackageH(Double packageH) {
		this.packageH = packageH;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	public void setListType(Integer listType) {
		this.listType = listType;
	}

	public Integer getListType() {
		return listType;
	}

	public Map<String, String> getPdfs() {
		return pdfs;
	}

	public void setPdfs(Map<String, String> pdfs) {
		this.pdfs = pdfs;
	}

	public void addPdf(String type, String url) {
		if (pdfs == null)
			pdfs = new TreeMap<String, String>();
		pdfs.put(type, url);
	}

	public boolean isEndQtyPricing() {
		return endQtyPricing;
	}

	public void setEndQtyPricing(boolean endQtyPricing) {
		this.endQtyPricing = endQtyPricing;
	}

	public boolean isRetainOnCart() {
		return retainOnCart;
	}

	public void setRetainOnCart(boolean retainOnCart) {
		this.retainOnCart = retainOnCart;
	}

	public String getEncodedSku() throws UnsupportedEncodingException {
		String encoded = "";
		if (sku != null) {
			encoded = sku;
			encoded = encoded.replace(" ", "---___---");
			encoded = URLEncoder.encode(encoded, "UTF-8");
			encoded = encoded.replace("/", "-");
			encoded = encoded.replace("%2C", ","); // keep commas
			encoded = encoded.replace("%2F", "%252F"); // replace %2F to %252F - /
			encoded = encoded.replace("%26", "%2526"); // replace %26 to %2526 = &
			encoded = encoded.replace("---___---", " "); // retain spaces
		}
		return encoded;
	}

	public String getEncodedName() throws UnsupportedEncodingException {
		String encoded = "";
		if (name != null) {
			encoded = name.trim().replace("/", "-");
			encoded = encoded.trim().replace(" ", "-");
			encoded = URLEncoder.encode(encoded, "UTF-8");
			encoded = encoded.replace("%2C", ","); // keep commas
			encoded = encoded.replace("%26", "%2526"); // replace %26 to %2526 = &
		}
		return encoded;
	}

	public String getEncodedMasterSku() throws UnsupportedEncodingException {
		String encoded = "";
		if (masterSku != null) {
			encoded = masterSku;
			encoded = encoded.replace(" ", "---___---");
			encoded = URLEncoder.encode(encoded, "UTF-8");
			encoded = encoded.replace("/", "-");
			encoded = encoded.replace("%2C", ","); // keep commas
			encoded = encoded.replace("%2F", "%252F"); // replace %2F to %252F - /
			encoded = encoded.replace("%26", "%2526"); // replace %26 to %2526 = &
			encoded = encoded.replace("---___---", " "); // retain spaces
		}
		return encoded;
	}

	public void setSiteMapPriority(Double siteMapPriority) {
		this.siteMapPriority = siteMapPriority;
	}

	public Double getSiteMapPriority() {
		return siteMapPriority;
	}

	public List<Product> getVariants() {
		return variants;
	}

	public void setVariants(List<Product> variants) {
		this.variants = variants;
	}

	public void setQuote(boolean quote) {
		this.quote = quote;
	}

	public boolean isQuote() {
		return quote;
	}

	public void setBreadCrumb(Set<String> breadCrumb) {
		this.breadCrumb = breadCrumb;
	}

	public Set<String> getBreadCrumb() {
		return breadCrumb;
	}

	public void setUpsMaxItemsInPackage(Double upsMaxItemsInPackage) {
		this.upsMaxItemsInPackage = upsMaxItemsInPackage;
	}

	public Double getUpsMaxItemsInPackage() {
		return upsMaxItemsInPackage;
	}

	public void setUspsMaxItemsInPackage(Double uspsMaxItemsInPackage) {
		this.uspsMaxItemsInPackage = uspsMaxItemsInPackage;
	}

	public Double getUspsMaxItemsInPackage() {
		return uspsMaxItemsInPackage;
	}

	public void setAsiIgnorePrice(boolean asiIgnorePrice) {
		this.asiIgnorePrice = asiIgnorePrice;
	}

	public boolean isAsiIgnorePrice() {
		return asiIgnorePrice;
	}

	public boolean isAddToPresentation() {
		return addToPresentation;
	}

	public void setAddToPresentation(boolean addToPresentation) {
		this.addToPresentation = addToPresentation;
	}

	public int getDefaultSupplierId() {
		return defaultSupplierId;
	}

	public void setDefaultSupplierId(int defaultSupplierId) {
		this.defaultSupplierId = defaultSupplierId;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	public String getCreatedBy() {
		return createdBy;
	}

	public void setLastModifiedBy(String lastModifiedBy) {
		this.lastModifiedBy = lastModifiedBy;
	}

	public String getLastModifiedBy() {
		return lastModifiedBy;
	}

	public Double getCost1() {
		return cost1;
	}

	public void setCost1(Double cost1) {
		this.cost1 = cost1;
	}

	public Double getCost2() {
		return cost2;
	}

	public void setCost2(Double cost2) {
		this.cost2 = cost2;
	}

	public Double getCost3() {
		return cost3;
	}

	public void setCost3(Double cost3) {
		this.cost3 = cost3;
	}

	public Double getCost4() {
		return cost4;
	}

	public void setCost4(Double cost4) {
		this.cost4 = cost4;
	}

	public Double getCost5() {
		return cost5;
	}

	public void setCost5(Double cost5) {
		this.cost5 = cost5;
	}

	public Double getCost6() {
		return cost6;
	}

	public void setCost6(Double cost6) {
		this.cost6 = cost6;
	}

	public Double getCost7() {
		return cost7;
	}

	public void setCost7(Double cost7) {
		this.cost7 = cost7;
	}

	public Double getCost8() {
		return cost8;
	}

	public void setCost8(Double cost8) {
		this.cost8 = cost8;
	}

	public Double getCost9() {
		return cost9;
	}

	public void setCost9(Double cost9) {
		this.cost9 = cost9;
	}

	public Double getCost10() {
		return cost10;
	}

	public void setCost10(Double cost10) {
		this.cost10 = cost10;
	}

	public Double getMarginPercent1() {
		if (price1 != null && cost1 != null && cost1 != 0) {
			marginPercent1 = ((price1 - cost1) / price1) * 100;
		}
		return marginPercent1;
	}

	public Double getMarginPercent2() {
		if (price2 != null && cost2 != null && cost2 != 0) {
			marginPercent2 = ((price2 - cost2) / price2) * 100;
		}
		return marginPercent2;
	}

	public Double getMarginPercent3() {
		if (price3 != null && cost3 != null && cost3 != 0) {
			marginPercent3 = ((price3 - cost3) / price3) * 100;
		}
		return marginPercent3;
	}

	public Double getMarginPercent4() {
		if (price4 != null && cost4 != null && cost4 != 0) {
			marginPercent4 = ((price4 - cost4) / price4) * 100;
		}
		return marginPercent4;
	}

	public Double getMarginPercent5() {
		if (price5 != null && cost5 != null && cost5 != 0) {
			marginPercent5 = ((price5 - cost5) / price5) * 100;
		}
		return marginPercent5;
	}

	public Double getMarginPercent6() {
		if (price6 != null && cost6 != null && cost6 != 0) {
			marginPercent6 = ((price6 - cost6) / price6) * 100;
		}
		return marginPercent6;
	}

	public Double getMarginPercent7() {
		if (price7 != null && cost7 != null && cost7 != 0) {
			marginPercent7 = ((price7 - cost7) / price7) * 100;
		}
		return marginPercent7;
	}

	public Double getMarginPercent8() {
		if (price8 != null && cost8 != null && cost8 != 0) {
			marginPercent8 = ((price8 - cost8) / price8) * 100;
		}
		return marginPercent8;
	}

	public Double getMarginPercent9() {
		if (price9 != null && cost9 != null && cost9 != 0) {
			marginPercent9 = ((price9 - cost9) / price9) * 100;
		}
		return marginPercent9;
	}

	public Double getMarginPercent10() {
		if (price10 != null && cost10 != null && cost10 != 0) {
			marginPercent10 = ((price10 - cost10) / price10) * 100;
		}
		return marginPercent10;
	}

	public Integer getSearchRank() {
		return searchRank;
	}

	public void setSearchRank(Integer searchRank) {
		this.searchRank = searchRank;
	}

	public String getDefaultSupplierName() {
		return defaultSupplierName;
	}

	public void setDefaultSupplierName(String defaultSupplierName) {
		this.defaultSupplierName = defaultSupplierName;
	}

	public Boolean getCostPercent() {
		return costPercent;
	}

	public void setCostPercent(Boolean costPercent) {
		this.costPercent = costPercent;
	}

	public String getParentSku() {
		return parentSku;
	}

	public void setParentSku(String parentSku) {
		this.parentSku = parentSku;
	}

	public boolean isBelongsToLoadCenter() {
		return belongsToLoadCenter;
	}

	public void setBelongsToLoadCenter(boolean belongsToLoadCenter) {
		this.belongsToLoadCenter = belongsToLoadCenter;
	}

	public String getParentId() {
		return parentId;
	}

	public void setParentId(String parentId) {
		this.parentId = parentId;
	}

	public String getAlt() {
		return alt;
	}

	public void setAlt(String alt) {
		this.alt = alt;
	}

	public Double getOriginalPriceRangeMinimum() {
		return originalPriceRangeMinimum;
	}

	public void setOriginalPriceRangeMinimum(Double originalPriceRangeMinimum) {
		this.originalPriceRangeMinimum = originalPriceRangeMinimum;
	}

	public Double getOriginalPriceRangeMaximum() {
		return originalPriceRangeMaximum;
	}

	public void setOriginalPriceRangeMaximum(Double originalPriceRangeMaximum) {
		this.originalPriceRangeMaximum = originalPriceRangeMaximum;
	}

	public String getCaseUnitTitle() {
		return caseUnitTitle;
	}

	public void setCaseUnitTitle(String caseUnitTitle) {
		this.caseUnitTitle = caseUnitTitle;
	}

	public Double getUnitPriceDiscount() {
		Double discountUnit = 0.0;
		if (getSalesTag() != null && getSalesTag().getDiscount() != 0.0) {
			if (getSalesTag().isPercent()) {
				discountUnit = 0.0 * (100 - getSalesTag().getDiscount()) / 100;
			} else {
				discountUnit = 0.0 - getSalesTag().getDiscount();
			}
		}
		return discountUnit;
	}

	public Double getTotalPriceDiscount() {
		Double discount = 0.0;
		if (getSalesTag() != null && getSalesTag().getDiscount() != 0.0) {
			if (getSalesTag().isPercent()) {
				discount = getPrice1() * (100 - getSalesTag().getDiscount()) / 100;
			} else {
				discount = getPrice1() - getSalesTag().getDiscount();
			}
		} else {
			discount = getPrice1();
		}
		return discount;
	}

	public Double getLotPriceDiscount() {
		Double discount = 0.0;

		if (getSalesTag() != null && getSalesTag().getDiscount() != 0.0) {
			if (getSalesTag().isPercent()) {
				discount = getPrice1() * (100 - getSalesTag().getDiscount()) / 100;
			} else {
				discount = getPrice1() - getSalesTag().getDiscount();
			}
		} else {
			discount = getPrice1();
		}
		return discount;
	}

	private boolean isMarkUpApplied;

	public void setMarkUpApplied(boolean isMarkUpApplied) {
		this.isMarkUpApplied = isMarkUpApplied;
	}

	public boolean isMarkUpApplied() {
		return isMarkUpApplied;
	}

	private ProductFieldUnlimitedNameValue masterSkuProductFieldUnlimited;

	public void setMasterSkuProductFieldUnlimited(ProductFieldUnlimitedNameValue masterSkuProductFieldUnlimited) {
		this.masterSkuProductFieldUnlimited = masterSkuProductFieldUnlimited;
	}

	public Boolean getIncludeRetailDisplay() {
		return includeRetailDisplay;
	}

	public void setIncludeRetailDisplay(Boolean includeRetailDisplay) {
		this.includeRetailDisplay = includeRetailDisplay;
	}

	public Boolean getIncludeRetailEnabled() {
		return includeRetailEnabled;
	}

	public void setIncludeRetailEnabled(Boolean includeRetailEnabled) {
		this.includeRetailEnabled = includeRetailEnabled;
	}

	public String getFobZipCode() {
		return fobZipCode;
	}

	public void setFobZipCode(String fobZipCode) {
		this.fobZipCode = fobZipCode;
	}

	public boolean isRestrictPriceChange() {
		return restrictPriceChange;
	}

	public void setRestrictPriceChange(boolean restrictPriceChange) {
		this.restrictPriceChange = restrictPriceChange;
	}

	public String getRedirectUrlEn() {
		return redirectUrlEn;
	}

	public void setRedirectUrlEn(String redirectUrlEn) {
		this.redirectUrlEn = redirectUrlEn;
	}

	public String getRedirectUrlEs() {
		return redirectUrlEs;
	}

	public void setRedirectUrlEs(String redirectUrlEs) {
		this.redirectUrlEs = redirectUrlEs;
	}

	public String getClassField() {
		return classField;
	}

	public void setClassField(String classField) {
		this.classField = classField;
	}

	public String getProgram() {
		return program;
	}

	public void setProgram(String program) {
		this.program = program;
	}

}
