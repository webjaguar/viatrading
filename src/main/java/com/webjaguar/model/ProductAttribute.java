/* Copyright 2006 Advanced E-Media Solutions
 * @author Eduardo Asprec
 * @since 10.02.2006
 */

package com.webjaguar.model;

import java.io.Serializable;

public class ProductAttribute implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String optionCode;
	private int id;
	private int optionIndex;
	private int valueIndex;
	private String optionName;
	private String valueName;
	private Double optionPrice;
	private boolean oneTimePrice;
	private Double optionPriceOriginal;
	private Double optionWeight;
	private String imageUrl;
	private String type;
	// custom text
	private String optionCusValue;
	// box
	private String optionBoxValue;
	private boolean dealItem;
	private String productName;
	// asi
	private String asiOptionName;
	private String asiOptionValue;

	//attribute not available in database
	private boolean isCustomAttribute;
	
	private String eventType;
	
	/* Constructors */
	public ProductAttribute() {}
	
	public ProductAttribute(String asiOptionName, String asiOptionValue, int optionIndex) {
		this.asiOptionName = asiOptionName;
		this.asiOptionValue = asiOptionValue;
		this.optionIndex = optionIndex;
	}
	
	public ProductAttribute(String optionName, String valueName, boolean isCustomAttribute) {
		this.optionName = optionName;
		this.valueName = valueName;
		this.isCustomAttribute = isCustomAttribute;
	}
	
	public ProductAttribute(String optionCode, int optionIndex, int valueIndex, String imageUrl) {
		this.optionCode = optionCode;
		this.optionIndex = optionIndex;
		this.valueIndex = valueIndex;
		this.imageUrl = imageUrl;
	}
	
	public ProductAttribute(String optionCode, int optionIndex, String optionCusValue, int valueIndex) {
		this.optionCode = optionCode;
		this.optionIndex = optionIndex;
		this.valueIndex = valueIndex;
		this.optionCusValue = optionCusValue;
	}
	
	public ProductAttribute(String optionCode, int optionIndex, String optionBoxValue, String productName) {
		this.optionCode = optionCode;
		this.optionIndex = optionIndex;
		this.valueIndex = 0;
		this.optionBoxValue = optionBoxValue;
		this.productName = productName;
	}
	
	public String getOptionCode() {
		return optionCode;
	}

	public void setOptionCode(String optionCode) {
		this.optionCode = optionCode;
	}

	public int getOptionIndex() {
		return optionIndex;
	}

	public void setOptionIndex(int optionIndex) {
		this.optionIndex = optionIndex;
	}
	
	public int getValueIndex() {
		return valueIndex;
	}
	
	public void setValueIndex(int valueIndex) {
		this.valueIndex = valueIndex;
	}

	public String getOptionName() {
		return optionName;
	}

	public void setOptionName(String optionName) {
		this.optionName = optionName;
	}

	public String getValueName() {
		return valueName;
	}

	public void setValueName(String valueName) {
		this.valueName = valueName;
	}

	public Double getOptionPrice()
	{
		return optionPrice;
	}

	public void setOptionPrice(Double optionPrice)
	{
		this.optionPrice = optionPrice;
	}
	public Double getOptionPriceOriginal() {
		return optionPriceOriginal;
	}

	public void setOptionPriceOriginal(Double optionPriceOriginal) {
		this.optionPriceOriginal = optionPriceOriginal;
	}

	public String getOptionBoxValue()
	{
		return optionBoxValue;
	}
	public void setOptionBoxValue(String optionBoxValue)
	{
		this.optionBoxValue = optionBoxValue;
	}
	public void setDealItem(boolean dealItem) {
		this.dealItem = dealItem;
	}

	public boolean isDealItem() {
		return dealItem;
	}

	public String getProductName()
	{
		return productName;
	}
	public void setProductName(String productName)
	{
		this.productName = productName;
	}
	public String getValueString() {
		if ( this.optionCusValue != null )
			return this.optionCusValue;
		else if ( this.optionPrice != null )
			return this.valueName;
		else if ( this.optionBoxValue != null )
			return this.optionBoxValue.toString();
		else if ( this.asiOptionValue != null )
			return this.asiOptionValue.toString();
		else
			return this.valueName;
	}
	public String getOptionCusValue()
	{
		return optionCusValue;
	}
	public void setOptionCusValue(String optionCusValue)
	{
		this.optionCusValue = optionCusValue;
	}

	public String getImageUrl()
	{
		return imageUrl;
	}

	public void setImageUrl(String imageUrl)
	{
		this.imageUrl = imageUrl;
	}
	
	public boolean isAbsolute() {
		if (imageUrl != null && (imageUrl.contains("//") || imageUrl.startsWith("/"))) {
			return true;
		} else {
			return false;			
		}
	}

	public String getType()
	{
		return type;
	}

	public void setType(String type)
	{
		this.type = type;
	}

	public String getAsiOptionName() {
		return asiOptionName;
	}

	public void setAsiOptionName(String asiOptionName) {
		this.asiOptionName = asiOptionName;
	}

	public String getAsiOptionValue() {
		return asiOptionValue;
	}

	public void setAsiOptionValue(String asiOptionValue) {
		this.asiOptionValue = asiOptionValue;
	}

	public void setOptionWeight(Double optionWeight) {
		this.optionWeight = optionWeight;
	}

	public Double getOptionWeight() {
		return optionWeight;
	}

	public void setOneTimePrice(boolean oneTimePrice) {
		this.oneTimePrice = oneTimePrice;
	}

	public boolean isOneTimePrice() {
		return oneTimePrice;
	}

	public void setCustomAttribute(boolean isCustomAttribute) {
		this.isCustomAttribute = isCustomAttribute;
	}

	public boolean isCustomAttribute() {
		return isCustomAttribute;
	}
	
	public String getOptionPriceMessageF(){
		if(this.optionPriceOriginal != null && this.optionPriceOriginal != 0) {
			if(this.optionPriceOriginal < 0){
				return "f_optionPriceDeduction";
			} else {
				return "f_optionPriceAddition";
			}
		}
		return null;
	}

	public String getEventType() {
		return eventType;
	}

	public void setEventType(String eventType) {
		this.eventType = eventType;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}
	
}
