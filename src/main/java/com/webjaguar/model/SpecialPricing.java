package com.webjaguar.model;

public class SpecialPricing
{
	private Integer id;
	private String sku;
	private Integer customerId;
	private Double price;
	private boolean enable;
	
	public SpecialPricing()
	{
		super();
	}
	public SpecialPricing( String sku, Integer customerId, Double price, boolean enable )
	{
		super();
		this.sku = sku;
		this.customerId = customerId;
		this.price = price;
		this.enable = enable;
	}
	public Integer getId()
	{
		return id;
	}
	public void setId(Integer id)
	{
		this.id = id;
	}
	public String getSku()
	{
		return sku;
	}
	public void setSku(String sku)
	{
		this.sku = sku;
	}
	public Integer getCustomerId()
	{
		return customerId;
	}
	public void setCustomerId(Integer customerId)
	{
		this.customerId = customerId;
	}
	public Double getPrice()
	{
		return price;
	}
	public void setPrice(Double price)
	{
		this.price = price;
	}
	public boolean isEnable()
	{
		return enable;
	}
	public void setEnable(boolean enable)
	{
		this.enable = enable;
	}
	
	
}
