/* Copyright 2008 Advanced E-Media Solutions
 * @author Eduardo Asprec
 * @since 01.15.2008
 */

package com.webjaguar.model;

import java.sql.Time;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Set;

public class WorkOrder {
	
	private ServiceWork service;
	private Timestamp created;
	private Timestamp lastModified;
	private String technician;
	private Date date;
	private Date serviceDate;
	private Time startTime;
	private Time stopTime;
	private Integer bwCount;
	private Integer colorCount;
	private Integer installPercentage;
	private String inSN;
	private String outSN;	
	private List<WorkOrderLineItem> lineItems = new ArrayList<WorkOrderLineItem>();
	private String type;

	// for work order form
	private String startHour;
	private String startMin;
	private String stopHour;
	private String stopMin;
	private Set<Integer> deletedLineItems;	// deleted line items
	private List<WorkOrderLineItem> insertedLineItems; // inserted line items
	private boolean newWorkOrder;
	
	public ServiceWork getService()
	{
		return service;
	}
	public void setService(ServiceWork service)
	{
		this.service = service;
	}
	public Timestamp getCreated()
	{
		return created;
	}
	public void setCreated(Timestamp created)
	{
		this.created = created;
	}
	public Timestamp getLastModified()
	{
		return lastModified;
	}
	public void setLastModified(Timestamp lastModified)
	{
		this.lastModified = lastModified;
	}
	public String getTechnician()
	{
		return technician;
	}
	public void setTechnician(String technician)
	{
		this.technician = technician;
	}
	public Date getDate()
	{
		return date;
	}
	public void setDate(Date date)
	{
		this.date = date;
	}
	public Date getServiceDate()
	{
		return serviceDate;
	}
	public void setServiceDate(Date serviceDate)
	{
		this.serviceDate = serviceDate;
	}
	public Time getStartTime()
	{
		return startTime;
	}
	public void setStartTime(Time startTime)
	{
		this.startTime = startTime;
	}
	public Time getStopTime()
	{
		return stopTime;
	}
	public void setStopTime(Time stopTime)
	{
		this.stopTime = stopTime;
	}
	public List<WorkOrderLineItem> getLineItems()
	{
		return lineItems;
	}
	public void setLineItems(List<WorkOrderLineItem> lineItems)
	{
		this.lineItems = lineItems;
	}
	public String getStartHour()
	{
		return startHour;
	}
	public void setStartHour(String startHour)
	{
		this.startHour = startHour;
	}
	public String getStartMin()
	{
		return startMin;
	}
	public void setStartMin(String startMin)
	{
		this.startMin = startMin;
	}
	public String getStopHour()
	{
		return stopHour;
	}
	public void setStopHour(String stopHour)
	{
		this.stopHour = stopHour;
	}
	public String getStopMin()
	{
		return stopMin;
	}
	public void setStopMin(String stopMin)
	{
		this.stopMin = stopMin;
	}
	public Set<Integer> getDeletedLineItems()
	{
		return deletedLineItems;
	}
	public void setDeletedLineItems(Set<Integer> deletedLineItems)
	{
		this.deletedLineItems = deletedLineItems;
	}
	public List<WorkOrderLineItem> getInsertedLineItems()
	{
		return insertedLineItems;
	}
	public void setInsertedLineItems(List<WorkOrderLineItem> insertedLineItems)
	{
		this.insertedLineItems = insertedLineItems;
	}
	public boolean isNewWorkOrder()
	{
		return newWorkOrder;
	}
	public void setNewWorkOrder(boolean newWorkOrder)
	{
		this.newWorkOrder = newWorkOrder;
	}
	
	public void insertLineItem(Product product)
	{
		if (insertedLineItems == null) {
			insertedLineItems = new ArrayList<WorkOrderLineItem>();
		}
		WorkOrderLineItem lineItem = new WorkOrderLineItem();
		lineItem.setQuantity(1);	// default to 1
		lineItem.setProduct(product);
		insertedLineItems.add(lineItem);
	}
	
	public boolean ishasContent()
	{
		for ( WorkOrderLineItem lineItem : lineItems )
		{
			if ( lineItem.getProduct().getCaseContent() != null )
			{
				return true;
			}
		}
		if (insertedLineItems != null) {
			for ( WorkOrderLineItem lineItem : insertedLineItems ) {
				if ( lineItem.getProduct().getCaseContent() != null )
				{
					return true;
				}				
			}
		}		
		return false;
	}

	public boolean ishasPacking()
	{
		for ( WorkOrderLineItem lineItem : lineItems )
		{
			if ( lineItem.getProduct().getPacking() != null && !lineItem.getProduct().getPacking().equals( "" ) )
			{
				return true;
			}
		}
		if (insertedLineItems != null) {
			for ( WorkOrderLineItem lineItem : insertedLineItems ) {
				if ( lineItem.getProduct().getPacking() != null && !lineItem.getProduct().getPacking().equals( "" ) )
				{
					return true;
				}				
			}
		}
		return false;
	}
	public Integer getInstallPercentage()
	{
		return installPercentage;
	}
	public void setInstallPercentage(Integer installPercentage)
	{
		this.installPercentage = installPercentage;
	}
	public String getInSN()
	{
		return inSN;
	}
	public void setInSN(String inSN)
	{
		this.inSN = inSN;
	}
	public String getOutSN()
	{
		return outSN;
	}
	public void setOutSN(String outSN)
	{
		this.outSN = outSN;
	}
	public Integer getBwCount()
	{
		return bwCount;
	}
	public void setBwCount(Integer bwCount)
	{
		this.bwCount = bwCount;
	}
	public Integer getColorCount()
	{
		return colorCount;
	}
	public void setColorCount(Integer colorCount)
	{
		this.colorCount = colorCount;
	}
	public String getType()
	{
		return type;
	}
	public void setType(String type)
	{
		this.type = type;
	}
}
