/* Copyright 2005, 2006 Advanced E-Media Solutions
 *
 */

package com.webjaguar.model;

import java.sql.Timestamp;
import java.util.Date;

public class CommissionReport {
	
	//user
	private Integer userId;
	private String firstName;
	private String lastName;
	//order
	private Integer orderId;
	private Timestamp orderDate;
	private String orderStatus;
	private Double subTotal;
	
	private Integer affiliateIdLevel1;
	private Integer affiliateIdLevel2;
	private Double commissionLevel1;
	private Double commissionLevel2;
	private Double myCommission;
	private String commissionLevel1Status;
	private String commissionLevel2Status;
	private String commissionLevel1Note;
	private String commissionLevel2Note;
	
	private Double commissionTotal;
	private Double commissionTotalOrder;
	private Integer numOrder;
	
	private Double commissionDueLevel1;
	private Double commissionDueLevel2;
	private Double commissionPaidLevel1;
	private Double commissionPaidLevel2;
	private Double commissionDueTotal;
	private Double commissionPaidTotal;
	private String checkNumberLevel1;
	private String checkNumberLevel2;
	private Date dateLevel1;
	private Date dateLevel2;
	
	
	
	public Integer getNumOrder()
	{
		return numOrder;
	}
	public void setNumOrder(Integer numOrder)
	{
		this.numOrder = numOrder;
	}
	public Double getCommissionTotal()
	{
		return commissionTotal;
	}
	public void setCommissionTotal(Double commissionTotal)
	{
		this.commissionTotal = commissionTotal;
	}
	public Double getCommissionTotalOrder()
	{
		return commissionTotalOrder;
	}
	public void setCommissionTotalOrder(Double commissionTotalOrder)
	{
		this.commissionTotalOrder = commissionTotalOrder;
	}
	public Double getCommissionLevel1()
	{
		return commissionLevel1;
	}
	public void setCommissionLevel1(Double commissionLevel1)
	{
		this.commissionLevel1 = commissionLevel1;
	}
	public String getCommissionLevel1Status()
	{
		return commissionLevel1Status;
	}
	public void setCommissionLevel1Status(String commissionLevel1Status)
	{
		this.commissionLevel1Status = commissionLevel1Status;
	}
	public String getCommissionLevel2Status()
	{
		return commissionLevel2Status;
	}
	public void setCommissionLevel2Status(String commissionLevel2Status)
	{
		this.commissionLevel2Status = commissionLevel2Status;
	}
	public Double getCommissionLevel2()
	{
		return commissionLevel2;
	}
	public void setCommissionLevel2(Double commissionLevel2)
	{
		this.commissionLevel2 = commissionLevel2;
	}
	public String getFirstName()
	{
		return firstName;
	}
	public void setFirstName(String firstName)
	{
		this.firstName = firstName;
	}
	public String getLastName()
	{
		return lastName;
	}
	public void setLastName(String lastName)
	{
		this.lastName = lastName;
	}
	public Timestamp getOrderDate()
	{
		return orderDate;
	}
	public void setOrderDate(Timestamp orderDate)
	{
		this.orderDate = orderDate;
	}
	public Integer getOrderId()
	{
		return orderId;
	}
	public void setOrderId(Integer orderId)
	{
		this.orderId = orderId;
	}
	public Double getSubTotal()
	{
		return subTotal;
	}
	public void setSubTotal(Double subTotal)
	{
		this.subTotal = subTotal;
	}
	public Integer getUserId()
	{
		return userId;
	}
	public void setUserId(Integer userId)
	{
		this.userId = userId;
	}
	public Double getMyCommission()
	{
		return myCommission;
	}
	public void setMyCommission(Double myCommission)
	{
		this.myCommission = myCommission;
	}
	public String getCommissionLevel1StatusToString()
	{
		if ( this.commissionLevel1Status.equals( "p" ) )
			return "Pending";
		else
			return "Paid";
	}	
	public String getCommissionLevel2StatusToString()
	{
		if ( this.commissionLevel2Status.equals( "p" ) )
			return "Pending";
		else
			return "Paid";
	}
	public String getOrderStatus()
	{
		return orderStatus;
	}
	public void setOrderStatus(String orderStatus)
	{
		this.orderStatus = orderStatus;
	}
	public String getOrderStatusToString()
	{
		if ( this.orderStatus.equals( "p" ))
			return "Pending";
		else if ( this.orderStatus.equals( "x" ))
			return "canceled";	
		else if ( this.orderStatus.equals( "xp" ))
			return "paypal canceled";
		else if ( this.orderStatus.equals( "s" ))
			return "shipped";
		else 
			return "processing";
	}
	public Integer getAffiliateIdLevel1()
	{
		return affiliateIdLevel1;
	}
	public void setAffiliateIdLevel1(Integer affiliateIdLevel1)
	{
		this.affiliateIdLevel1 = affiliateIdLevel1;
	}
	public Integer getAffiliateIdLevel2()
	{
		return affiliateIdLevel2;
	}
	public void setAffiliateIdLevel2(Integer affiliateIdLevel2)
	{
		this.affiliateIdLevel2 = affiliateIdLevel2;
	}
	public String getCommissionLevel1Note()
	{
		return commissionLevel1Note;
	}
	public void setCommissionLevel1Note(String commissionLevel1Note)
	{
		this.commissionLevel1Note = commissionLevel1Note;
	}
	public String getCommissionLevel2Note()
	{
		return commissionLevel2Note;
	}
	public void setCommissionLevel2Note(String commissionLevel2Note)
	{
		this.commissionLevel2Note = commissionLevel2Note;
	}
	public Double getCommissionDueLevel1()
	{
		return commissionDueLevel1;
	}
	public void setCommissionDueLevel1(Double commissionDueLevel1)
	{
		this.commissionDueLevel1 = commissionDueLevel1;
	}
	public Double getCommissionDueLevel2()
	{
		return commissionDueLevel2;
	}
	public void setCommissionDueLevel2(Double commissionDueLevel2)
	{
		this.commissionDueLevel2 = commissionDueLevel2;
	}
	public Double getCommissionPaidLevel1()
	{
		return commissionPaidLevel1;
	}
	public void setCommissionPaidLevel1(Double commissionPaidLevel1)
	{
		this.commissionPaidLevel1 = commissionPaidLevel1;
	}
	public Double getCommissionPaidLevel2()
	{
		return commissionPaidLevel2;
	}
	public void setCommissionPaidLevel2(Double commissionPaidLevel2)
	{
		this.commissionPaidLevel2 = commissionPaidLevel2;
	}
	public Double getCommissionDueTotal()
	{
		return commissionDueTotal;
	}
	public void setCommissionDueTotal(Double commissionDueTotal)
	{
		this.commissionDueTotal = commissionDueTotal;
	}
	public Double getCommissionPaidTotal()
	{
		return commissionPaidTotal;
	}
	public void setCommissionPaidTotal(Double commissionPaidTotal)
	{
		this.commissionPaidTotal = commissionPaidTotal;
	}
	public String getCheckNumberLevel1() {
		return checkNumberLevel1;
	}
	public void setCheckNumberLevel1(String checkNumberLevel1) {
		this.checkNumberLevel1 = checkNumberLevel1;
	}
	public String getCheckNumberLevel2() {
		return checkNumberLevel2;
	}
	public void setCheckNumberLevel2(String checkNumberLevel2) {
		this.checkNumberLevel2 = checkNumberLevel2;
	}
	public Date getDateLevel1() {
		return dateLevel1;
	}
	public void setDateLevel1(Date dateLevel1) {
		this.dateLevel1 = dateLevel1;
	}
	public Date getDateLevel2() {
		return dateLevel2;
	}
	public void setDateLevel2(Date dateLevel2) {
		this.dateLevel2 = dateLevel2;
	}

	
}