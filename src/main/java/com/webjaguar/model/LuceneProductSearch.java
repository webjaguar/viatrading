/* Copyright 2006 Advanced E-Media Solutions
 *
 */

package com.webjaguar.model;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class LuceneProductSearch extends ProductSearch {
	
	private Map<String, String> extraFieldMap;
	public Map<String, String> getExtraFieldMap() { return extraFieldMap; }
	public void setExtraFieldMap(Map<String, String> extraFieldMap) { this.extraFieldMap = extraFieldMap; }
	
	private String supplierAccountNumber;
	public String getSupplierAccountNumber() { return supplierAccountNumber; }
	public void setSupplierAccountNumber(String supplierAccountNumber) { this.supplierAccountNumber = supplierAccountNumber; }
	
	private Integer searchRank;
	public Integer getSearchRank() { return searchRank; }
	public void setSearchRank(Integer searchRank) { this.searchRank = searchRank; }
	
	private int searchRankOperator;
	public int getSearchRankOperator() { return searchRankOperator; }
	public void setSearchRankOperator(int searchRankOperator) { this.searchRankOperator = searchRankOperator; }
	
	private List<FilterAttribute> filters = new ArrayList<FilterAttribute>();
	public List<FilterAttribute> getFilters() { return filters; }
	public void setFilters(List<FilterAttribute> filters) { this.filters = filters; }
	
	private Integer minQty;
	public Integer getMinQty() { return minQty; }
	public void setMinQty(Integer minQty) { this.minQty = minQty; }
	
	private Integer maxQty;
	public Integer getMaxQty() { return maxQty; }
	public void setMaxQty(Integer maxQty) { this.maxQty = maxQty; }

	private Map<String, List<FilterAttribute>> rangeFilters;
	public Map<String, List<FilterAttribute>> getRangeFilters() { return rangeFilters; }
	public void setRangeFilters(Map<String, List<FilterAttribute>> rangeFilters) { this.rangeFilters = rangeFilters; }
	
	private Boolean salesTagActive;
	public Boolean getSalesTagActive() {	return salesTagActive; }
	public void setSalesTagActive(Boolean salesTagActive) {	this.salesTagActive = salesTagActive;	}
	
	private Integer salesTagId;
	public Integer getSalesTagId() { return salesTagId; }
	public void setSalesTagId(Integer salesTagId) { this.salesTagId = salesTagId; }

	private Boolean specialPricingAvailable;
	public Boolean getSpecialPricingAvailable() { return specialPricingAvailable; }
	public void setSpecialPricingAvailable(Boolean specialPricingAvailable) {this.specialPricingAvailable = specialPricingAvailable; }

	private Boolean noKeywords;
	public Boolean getNoKeywords() { return noKeywords;	}
	public void setNoKeywords(Boolean noKeywords) {	this.noKeywords = noKeywords; }
	
	private Integer specialPricingGroupId;
	public Integer getSpecialPricingGroupId() { return specialPricingGroupId; }
	public void setSpecialPricingGroupId(Integer specialPricingGroupId) { this.specialPricingGroupId = specialPricingGroupId; }

	private Integer colorConfigGroupId;
	public Integer getColorConfigGroupId() { return colorConfigGroupId; }
	public void setColorConfigGroupId(Integer colorConfigGroupId) { this.colorConfigGroupId = colorConfigGroupId; }

}	