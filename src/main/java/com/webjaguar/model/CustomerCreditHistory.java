package com.webjaguar.model;

import java.math.BigDecimal;
import java.util.Date;

import com.webjaguar.web.domain.Utilities;

public class CustomerCreditHistory
{
	private Integer userId;
	private String reference;
    private Date date;
	private Double credit;
	private String transactionId;
	private int type;
	private Double total=0.00;
	private String userName;
	
	public Integer getUserId() {
		return userId;
	}
	public void setUserId(Integer userId) {
		this.userId = userId;
	}
	public String getReference() {
		return reference;
	}
	public void setReference(String reference) {
		this.reference = reference;
	}	
	public Date getDate() {
		return date;
	}
	public void setDate(Date date) {
		this.date = date;
	}
	public Double getCredit() {
		return credit;
	}
	public void setCredit(Double credit) {
		this.credit = credit;
	}
	public String getTransactionId() {
		return transactionId;
	}
	public void setTransactionId(String transactionId) {
		this.transactionId = transactionId;
	}
	public int getType() {
		return type;
	}
	public void setType(int type) {
		this.type = type;
	}
	public Double getTotal() {
		return  Utilities.roundFactory(total,2,BigDecimal.ROUND_HALF_UP);
	}
	public void setTotal(Double total) {
		this.total = total;
	}
	public String getUserName() {
		return userName;
	}
	public void setUserName(String userName) {
		this.userName = userName;
	}	
}