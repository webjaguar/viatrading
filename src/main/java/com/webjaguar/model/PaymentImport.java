/* Copyright 2007 Advanced E-Media Solutions
 * @author Eduardo Asprec
 * @since 07.26.2007
 */

package com.webjaguar.model;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.webjaguar.web.domain.Utilities;

public class PaymentImport {
	
	private Integer id;
	private Double amount;
	private Date date;
	private Customer customer;
	private String memo;
	private String paymentMethod;
	//private Double balance;
	private String message;
	private Integer userId;
	private boolean cancelPayment;
	private String cancelledBy;
	private Double cancelledAmt;
	private List<PaymentImport> lineItems = new ArrayList<PaymentImport>();
	private Double totalOrdersPaymentAmout;
	private Integer salesRepId;
	private Character exportSuccess;


	public String getCancelledBy() {
		return cancelledBy;
	}

	public void setCancelledBy(String cancelledBy) {
		this.cancelledBy = cancelledBy;
	}

	public Double getCancelledAmt() {
		return cancelledAmt;
	}

	public void setCancelledAmt(Double cancelledAmt) {
		this.cancelledAmt = cancelledAmt;
	}

	// should be a list of orderIds because one payment might have number of orders.
	private Integer orderId;
	
	public PaymentImport() {}
	
	public PaymentImport(Customer customer, Date date) {
		this.customer = customer;
		this.date = date;
	}	
	
	public Integer getId()
	{
		return id;
	}

	public void setId(Integer id)
	{
		this.id = id;
	}

	public Double getAmount()
	{
		return  Utilities.roundFactory(amount,2,BigDecimal.ROUND_HALF_UP);
	}

	public void setAmount(Double amount)
	{
		this.amount = amount;
	}

	public Date getDate()
	{
		return date;
	}

	public void setDate(Date date)
	{
		this.date = date;
	}

	public Customer getCustomer()
	{
		return customer;
	}

	public void setCustomer(Customer customer)
	{
		this.customer = customer;
		if (customer != null) {
			this.userId = customer.getId();
		}
	}

	public String getMemo()
	{
		return memo;
	}

	public void setMemo(String memo)
	{
		this.memo = memo;
	}
	/*
	public Double getBalance()
	{
		return balance;
	}

	public void setBalance(Double balance)
	{
		this.balance = balance;
	}
	*/
	public String getPaymentMethod()
	{
		return paymentMethod;
	}

	public void setPaymentMethod(String paymentMethod)
	{
		this.paymentMethod = paymentMethod;
	}

	public String getMessage()
	{
		return message;
	}

	public void setMessage(String message)
	{
		this.message = message;
	}

	public Integer getUserId() {
		return userId;
	}

	public void setUserId(Integer userId) {
		this.userId = userId;
	}
	public Integer getOrderId() {
		return orderId;
	}

	public void setOrderId(Integer orderId) {
		this.orderId = orderId;
	}

	public boolean isCancelPayment() {
		return cancelPayment;
	}

	public void setCancelPayment(boolean cancelPayment) {
		this.cancelPayment = cancelPayment;
	}

	public List<PaymentImport> getLineItems() {
		return lineItems;
	}

	public void setLineItems(List<PaymentImport> lineItems) {
		this.lineItems = lineItems;
	}

	public Double getTotalOrdersPaymentAmout() {
		return totalOrdersPaymentAmout;
	}

	public void setTotalOrdersPaymentAmout(Double totalOrdersPaymentAmout) {
		this.totalOrdersPaymentAmout = totalOrdersPaymentAmout;
	}

	public Integer getSalesRepId() {
		return salesRepId;
	}

	public void setSalesRepId(Integer salesRepId) {
		this.salesRepId = salesRepId;
	}

	public Character getExportSuccess() {
		return exportSuccess;
	}

	public void setExportSuccess(Character exportSuccess) {
		this.exportSuccess = exportSuccess;
	}
	
}
