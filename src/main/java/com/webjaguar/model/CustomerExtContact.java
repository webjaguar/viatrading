/* Copyright 2005, 2006 Advanced E-Media Solutions
 *
 */

package com.webjaguar.model;

import java.io.Serializable;

public class CustomerExtContact implements  Serializable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private Integer id;
	private Integer userId;
	private String name;
	private String position;
	private String phone1;
	private String phone2;
	private String email;	

	public Integer getId() { return id; }
	public void setId(Integer id) {
		this.id = id;
	}

	public Integer getUserId() { return userId; }
	public void setUserId(Integer userId) {
		this.userId = userId;
	}	
	
	public String getName() { return name; }
	public void setName(String name) {
		this.name = name;
	}
	
	public String getPosition() { return position; }
	public void setPosition(String position) {
		this.position = position;
	}	
	
	public String getPhone1() { return phone1; }
	public void setPhone1(String phone1) {
		this.phone1 = phone1;
	}
	
	public String getPhone2() { return phone2; }
	public void setPhone2(String phone2) {
		this.phone2 = phone2;
	}

	public String getEmail() { return email; }
	public void setEmail(String email)	{
		this.email = email;
	}


}
