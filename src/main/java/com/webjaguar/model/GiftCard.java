/*
 * Copyright 2008 Advanced E-Media Solutions
 * 
 */

package com.webjaguar.model;

import java.util.Date;

public class GiftCard
{
	private Integer giftCardOrderId;
	private String code;
	private Integer userId;
	private Double amount;
	private Double amountRedeemed;
	private Integer redeemedById;
	private String redeemedByFirstName;
	private String redeemedByLastName;
	private String recipientEmail;
	private String recipientFirstName;
	private String recipientLastName;
	private String senderFirstName;
	private String senderLastName;
	private String message;
	private Date dateCreated;
	private Date dateRedeemed;
	private boolean active;
	private String paymentMethod;
	private String status;
	// 0: email 1: physical
	private boolean type = false;
	//private boolean redeemed;
	private CreditCard creditCard;
	public String getCode()
	{
		return code;
	}
	public void setCode(String code)
	{
		this.code = code;
	}
	public Double getAmount()
	{
		return amount;
	}
	public void setAmount(Double amount)
	{
		this.amount = amount;
	}
	public String getRecipientEmail()
	{
		return recipientEmail;
	}
	public void setRecipientEmail(String recipientEmail)
	{
		this.recipientEmail = recipientEmail;
	}
	public String getMessage()
	{
		return message;
	}
	public void setMessage(String message)
	{
		this.message = message;
	}
	public Date getDateCreated()
	{
		return dateCreated;
	}
	public void setDateCreated(Date dateCreated)
	{
		this.dateCreated = dateCreated;
	}
	public boolean isActive()
	{
		return active;
	}
	public void setActive(boolean active)
	{
		this.active = active;
	}
	public boolean isType()
	{
		return type;
	}
	public void setType(boolean type)
	{
		this.type = type;
	}
	public String getRecipientFirstName()
	{
		return recipientFirstName;
	}
	public void setRecipientFirstName(String recipientFirstName)
	{
		this.recipientFirstName = recipientFirstName;
	}
	public String getRecipientLastName()
	{
		return recipientLastName;
	}
	public void setRecipientLastName(String recipientLastName)
	{
		this.recipientLastName = recipientLastName;
	}
	public String getSenderFirstName()
	{
		return senderFirstName;
	}
	public void setSenderFirstName(String senderFirstName)
	{
		this.senderFirstName = senderFirstName;
	}
	public String getSenderLastName()
	{
		return senderLastName;
	}
	public void setSenderLastName(String senderLastName)
	{
		this.senderLastName = senderLastName;
	}
	public Integer getUserId()
	{
		return userId;
	}
	public void setUserId(Integer userId)
	{
		this.userId = userId;
	}
	public Date getDateRedeemed()
	{
		return dateRedeemed;
	}
	public void setDateRedeemed(Date dateRedeemed)
	{
		this.dateRedeemed = dateRedeemed;
	}
	public Double getAmountRedeemed()
	{
		return amountRedeemed;
	}
	public void setAmountRedeemed(Double amountRedeemed)
	{
		this.amountRedeemed = amountRedeemed;
	}
	public Integer getRedeemedById()
	{
		return redeemedById;
	}
	public void setRedeemedById(Integer redeemedById)
	{
		this.redeemedById = redeemedById;
	}
	public String getRedeemedByFirstName()
	{
		return redeemedByFirstName;
	}
	public void setRedeemedByFirstName(String redeemedByFirstName)
	{
		this.redeemedByFirstName = redeemedByFirstName;
	}
	public String getRedeemedByLastName()
	{
		return redeemedByLastName;
	}
	public void setRedeemedByLastName(String redeemedByLastName)
	{
		this.redeemedByLastName = redeemedByLastName;
	}
	public Integer getGiftCardOrderId()
	{
		return giftCardOrderId;
	}
	public void setGiftCardOrderId(Integer giftCardOrderId)
	{
		this.giftCardOrderId = giftCardOrderId;
	}
	public CreditCard getCreditCard()
	{
		return creditCard;
	}
	public void setCreditCard(CreditCard creditCard)
	{
		this.creditCard = creditCard;
	}
	public String getPaymentMethod() {
		return paymentMethod;
	}
	public void setPaymentMethod(String paymentMethod) {
		this.paymentMethod = paymentMethod;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	
	
}
