package com.webjaguar.model;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class VbaPaymentReport {
	private Integer orderId;
	private String productSku;
	private Integer supplierId;
	private Double commission;
	private Double consignment;
	private Timestamp dateOrdered;
	private Integer quantity;
	private Timestamp orderShipDate;
	private String note;
	private Date dateOfPay;
	private Date endDateOfPay;

	private Double amountToPay;
	private String paymentFor;
	private String paymentMethod;
	private Double cost;
	private Integer costPercent;
	private String productConsignmentNote;
	private Double subTotal;
	private String transactionId;
	private boolean vbaPaymentStatus;
	private String orderType;
	private int index;
	private String companyName;
	private Double unitPrice;
	private String reportView = "consignment";
	private Integer userId;
	private List<VirtualBank> vbOrders = new ArrayList<VirtualBank>();
	private Integer orderLineNum;
	// startDate
	private Date startDate;
	public Date getStartDate() { return startDate; }
	public void setStartDate(Date startDate) { this.startDate = startDate; }
	
	// endDate
	private Date endDate;
	public Date getEndDate() { return endDate; }
	public void setEndDate(Date endDate) { this.endDate = endDate; }
	
	// date type
	private String dateType = "orderDate";
	public String getDateType() { return dateType; }
	public void setDateType(String dateType) { this.dateType = dateType; }
	
	// page 
	private int page = 1;
	public int getPage() { return page; }
	public void setPage(int page) { this.page = page; }
	
	// page size
	private int pageSize = 10;
	public int getPageSize() { return pageSize; }
	public void setPageSize(int pageSize) { this.pageSize = pageSize; }
	
	public Double getUnitPrice() {
		return unitPrice;
	}

	public void setUnitPrice(Double unitPrice) {
		this.unitPrice = unitPrice;
	}

	public Double getcost() {
		return cost;
	}

	public void setcost(Double cost) {
		this.cost = cost;
	}

	public String getProductConsignmentNote() {
		return productConsignmentNote;
	}

	public void setProductConsignmentNote(String productConsignmentNote) {
		this.productConsignmentNote = productConsignmentNote;
	}

	public Double getSubTotal() {
		return subTotal;
	}

	public void setSubTotal(Double subTotal) {
		this.subTotal = subTotal;
	}
	
	public Double getAmountToPay() {
		return amountToPay;
	}

	public void setAmountToPay(Double amountToPay) {
		this.amountToPay = amountToPay;
	}

	public String getPaymentFor() {
		return paymentFor;
	}

	public void setPaymentFor(String paymentFor) {
		this.paymentFor = paymentFor;
	}

	public Integer getOrderId() { 
		return orderId;
	}

	public void setOrderId(Integer orderId) {
		this.orderId = orderId;
	}

	public String getProductSku() {
		return productSku;
	}

	public void setProductSku(String productSku) {
		this.productSku = productSku;
	}

	public Double getCommission() {
		return commission;
	}

	public void setCommission(Double commission) {
		this.commission = commission;
	}

	public Double getConsignment() {
		return consignment;
	}

	public void setConsignment(Double consignment) {
		this.consignment = consignment;
	}

	public Timestamp getDateOrdered() {
		return dateOrdered;
	}

	public void setDateOrdered(Timestamp dateOrdered) {
		this.dateOrdered = dateOrdered;
	}

	public Integer getQuantity() {
		return quantity;
	}

	public void setQuantity(Integer quantity) {
		this.quantity = quantity;
	}

	public Timestamp getOrderShipDate() {
		return orderShipDate;
	}

	public void setOrderShipDate(Timestamp orderShipDate) {
		this.orderShipDate = orderShipDate;
	}

	public String getNote() {
		return note;
	}

	public void setNote(String note) {
		this.note = note;
	}

	public Date getDateOfPay() {
		return dateOfPay;
	}

	public void setDateOfPay(Date date) {
		this.dateOfPay = date;
	}

	public String getPaymentMethod() {
		return paymentMethod;
	}

	public void setPaymentMethod(String paymentMethod) {
		this.paymentMethod = paymentMethod;
	}

	public Integer getCostPercent() {
		return costPercent;
	}

	public void setCostPercent(Integer costPercent) {
		this.costPercent = costPercent;
	}

	public String getTransactionId() {
		return transactionId;
	}

	public void setTransactionId(String transactionId) {
		this.transactionId = transactionId;
	}

	public boolean isVbaPaymentStatus() {
		return vbaPaymentStatus;
	}

	public void setVbaPaymentStatus(boolean vbaPaymentStatus) {
		this.vbaPaymentStatus = vbaPaymentStatus;
	}

	public String getOrderType() {
		return orderType;
	}

	public void setOrderType(String orderType) {
		this.orderType = orderType;
	}

	public int getIndex() {
		return index;
	}

	public void setIndex(int index) {
		this.index = index;
	}

	public String getCompanyName() {
		return companyName;
	}

	public void setCompanyName(String companyName) {
		this.companyName = companyName;
	}
	public String getReportView() {
		return reportView;
	}
	public void setReportView(String reportView) {
		this.reportView = reportView;
	}
	public Integer getUserId() {
		return userId;
	}
	public void setUserId(Integer userId) {
		this.userId = userId;
	}
	public List<VirtualBank> getVbOrder() {
		return vbOrders;
	}
	public void setVbOrder(List<VirtualBank> vbOrders) {
		this.vbOrders = vbOrders;
	}
	public Integer getOrderLineNum() {
		return orderLineNum;
	}
	public void setOrderLineNum(Integer orderLineNum) {
		this.orderLineNum = orderLineNum;
	}
	public Integer getSupplierId() {
		return supplierId;
	}
	public void setSupplierId(Integer supplierId) {
		this.supplierId = supplierId;
	}

	public Date getEndDateOfPay() {
		return endDateOfPay;
	}
	public void setEndDateOfPay(Date endDateOfPay) {
		this.endDateOfPay = endDateOfPay;
	}

	
}
