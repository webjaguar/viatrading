/* Copyright 2010 Advanced E-Media Solutions
 * @author Shahin Naji
 * @since 02.15.2010
 */

package com.webjaguar.model.crm;

import java.util.Date;

public class CrmContactSearch {

	// sort
	private String sort = "id desc";
	public String getSort() { return sort; }
	public void setSort(String sort) { this.sort = sort; }
	
	// account Id
	private Integer accountId;
	public Integer getAccountId() { return accountId; }
	public void setAccountId(Integer accountId) { this.accountId = accountId; }
	
	// account Name
	private String accountName;
	public String getAccountName() { return accountName; }
	public void setAccountName(String accountName) { this.accountName = accountName; }
	
	// contact name
	private String contactName;
	public String getContactName() { return contactName; }
	public void setContactName(String contactName) { this.contactName = contactName; }
	
	// convert to customer
	private Integer convertedCustomer;
	public Integer getConvertedCustomer() { return convertedCustomer; }
	public void setConvertedCustomer(Integer convertedCustomer) { this.convertedCustomer = convertedCustomer; }
	
	// date created
	private Date created;
	public Date getCreated() { return created; }
	public void setCreated(Date created) { this.created = created; }
	
	// zipcode
	private String zipcode;
	public String getZipcode() { return zipcode; }
	public void setZipcode(String zipcode) { this.zipcode = zipcode; }
	
	// city
	private String city;
	public String getCity() { return city; }
	public void setCity(String city) { this.city = city; }	
	
	//state
	private String state;
	public String getState() { return state; }
	public void setState(String state) { this.state = state; }
	
	// country
	private String country;
	public String getCountry() { return country; }
	public void setCountry(String country) { this.country = country; }
	
	// email
	private String email;
	public String getEmail() { return email; }
	public void setEmail(String email) { this.email = email; }
	
	// group Id
	private Integer groupId;
	public Integer getGroupId() { return groupId; }
	public void setGroupId(Integer groupId) { this.groupId = groupId; }
	
	// customer group Id
	private Integer customerGroupId;
	public Integer getCustomerGroupId() { return customerGroupId; }
	public void setCustomerGroupId(Integer customerGroupId) { this.customerGroupId = customerGroupId; }
	
	// Access User
	private Integer accessUserId;
	public Integer getAccessUserId() { return accessUserId; }
	public void setAccessUserId(Integer accessUserId) { this.accessUserId = accessUserId; }
	
	// trackcode
	private String trackcode;
	public String getTrackcode() { return trackcode; }
	public void setTrackcode(String trackcode) { this.trackcode = trackcode; }
	
	// leadSource
	private String leadSource;
	public String getLeadSource() { return leadSource; }
	public void setLeadSource(String leadSource) { this.leadSource = leadSource; }
	
	// rating
	private String rating;
	public String getRating() { return rating; }
	public void setRating(String rating) { this.rating = rating; }
	
	// rating
	private String rating1;
	public String getRating1() { return rating1; }
	public void setRating1(String rating1) { this.rating1 = rating1; }
	
	// rating
	private String rating2;
	public String getRating2() { return rating2; }
	public void setRating2(String rating2) { this.rating2 = rating2; }	
	
	// contact field Id
	private Integer contactFieldId;
	public Integer getContactFieldId() { return contactFieldId; }
	public void setContactFieldId(Integer contactFieldId) { this.contactFieldId = contactFieldId; }
	
	// contact field value
	private String contactFieldValue;
	public String getContactFieldValue() { return contactFieldValue; }
	public void setContactFieldValue(String contactFieldValue) { this.contactFieldValue = contactFieldValue; }
	
	// start date
	private Date startDate; 
	public Date getStartDate() { return startDate; }
	public void setStartDate(Date startDate) { this.startDate = startDate; }
	
	// end date
	private Date endDate;
	public Date getEndDate() { return endDate; }
	public void setEndDate(Date endDate) { this.endDate = endDate; }
	
	// used for exporting contacts
	private String fileType;
	public String getFileType() { return fileType; }
	public void setFileType(String fileType) { this.fileType = fileType; }	
	
	// page 
	private int page = 1;
	public int getPage() { return page; }
	public void setPage(int page) { this.page = page; }
	
	// page size
	private int pageSize = 10;
	public int getPageSize() { return pageSize; }
	public void setPageSize(int pageSize) { this.pageSize = pageSize; }
	
	private Integer limit;
	public Integer getLimit() { return limit; }
	public void setLimit(Integer limit) { this.limit = limit; }
	
	// offset
	private int offset;
	public int getOffset() { return offset; }
	public void setOffset(int offset) { this.offset = offset; }	
	

	// task Description
	private String taskDescription;
	public String getTaskDescription() {
		return taskDescription;
	}
	public void setTaskDescription(String taskDescription) {
		this.taskDescription = taskDescription;
	}

	//is Dialing export
	private Boolean isDialingExport;
	public Boolean getIsDialingExport() {
		return isDialingExport;
	}
	public void setIsDialingExport(Boolean isDialingExport) {
		this.isDialingExport = isDialingExport;
	}


	// used for qualifier
	private String qualifier;
	public String getQualifier() { return qualifier; }
	public void setQualifier(String qualifier) { this.qualifier = qualifier; }	
	
	// qualifier
	private Integer qualifierId = -1;
	public Integer getQualifierId() { return qualifierId; }
	public void setQualifierId(Integer qualifierId) { this.qualifierId = qualifierId; }
	
	private Integer accountManager;
	public Integer getAccountManager() {return accountManager;}
	public void setAccountManager(Integer accountManager) {this.accountManager = accountManager;}
	
	private String language;
	public String getLanguage() {return language;}
	public void setLanguage(String language) {this.language = language;}
}
