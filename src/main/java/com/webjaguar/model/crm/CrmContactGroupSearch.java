/* Copyright 2006, 2009 Advanced E-Media Solutions
 *
 */

package com.webjaguar.model.crm;

public class CrmContactGroupSearch {
	
	// sort
	private String sort= "id DESC";
	public String getSort() { return sort; }
	public void setSort(String sort) { this.sort = sort; }
	
	// group
	private String group;
	public String getGroup() { return group; }
	public void setGroup(String group) { this.group = group; }

	// page 
	private int page = 1;
	public int getPage() { return page; }
	public void setPage(int page) { this.page = page; }
	
	// page size
	private int pageSize = 10;
	public int getPageSize() { return pageSize; }
	public void setPageSize(int pageSize) { this.pageSize = pageSize; }	

}
