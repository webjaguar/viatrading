/* Copyright 2010 Advanced E-Media Solutions
 * @author Shahin Naji
 * @since 02.15.2010
 */

package com.webjaguar.model.crm;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class CrmTaskSearch {

	// sort
	private String sort = "due_date";
	public String getSort() { return sort; }
	public void setSort(String sort) { this.sort = sort; }
	
	// contact Id
	private Integer contactId;
	public Integer getContactId() { return contactId; }
	public void setContactId(Integer contactId) { this.contactId = contactId; }

	// contact Name
	private String contactName;
	public String getContactName() { return contactName; }
	public void setContactName(String contactName) { this.contactName = contactName; }

	// list of account Ids
	private List<Integer> accountIds = new ArrayList<Integer>();
	public List<Integer> getAccountIds() { return accountIds; }
	public void setAccountIds(List<Integer> accountIds) { this.accountIds = accountIds; }
	
	// account Id
	private Integer accountId;
	public Integer getAccountId() { return accountId; }
	public void setAccountId(Integer accountId) { this.accountId = accountId; }
	
	// account Name
	private String accountName;
	public String getAccountName() { return accountName; }
	public void setAccountName(String accountName) { this.accountName = accountName; }

	// status
	private String status;
	public String getStatus() { return status; }
	public void setStatus(String status) { this.status = status; }
	
	// title
	private String title;
	public String getTitle() { return title; }
	public void setTitle(String title) { this.title = title; }
	
	// assignedTo Id
	private String assignedToId;
	public String getAssignedToId() { return assignedToId; }
	public void setAssignedToId(String assignedToId) { this.assignedToId = assignedToId; }

	// description 
	private String description;
	public String getDescription() { return description; }
	public void setDescription(String description) { this.description = description; }

	// action 
	private String action;
	public String getAction() { return action; }
	public void setAction(String action) { this.action = action; }

	// priority 
	private String priority;
	public String getPriority() { return priority; }
	public void setPriority(String priority) { this.priority = priority; }

	// type 
	private String type;
	public String getType() { return type; }
	public void setType(String type) { this.type = type; }

	// used for exporting task
	private String fileType;
	public String getFileType() { return fileType; }
	public void setFileType(String fileType) { this.fileType = fileType; }	
	
	// start date
	private Date startDate; 
	public Date getStartDate() { return startDate; }
	public void setStartDate(Date startDate) { this.startDate = startDate; }
	
	// end date
	private Date endDate;
	public Date getEndDate() { return endDate; }
	public void setEndDate(Date endDate) { this.endDate = endDate; }

	// start due date
	private Date startDueDate; 
	public Date getStartDueDate() { return startDueDate; }
	public void setStartDueDate(Date startDueDate) { this.startDueDate = startDueDate; }
	
	// end date
	private Date endDueDate;
	public Date getEndDueDate() { return endDueDate; }
	public void setEndDueDate(Date endDueDate) { this.endDueDate = endDueDate; }

	// page 
	private int page = 1;
	public int getPage() { return page; }
	public void setPage(int page) { this.page = page; }
	
	// page size
	private int pageSize = 10;
	public int getPageSize() { return pageSize; }
	public void setPageSize(int pageSize) { this.pageSize = pageSize; }
	
	private Integer limit;
	public Integer getLimit() { return limit; }
	public void setLimit(Integer limit) { this.limit = limit; }
	
	// offset
	private int offset;
	public int getOffset() { return offset; }
	public void setOffset(int offset) { this.offset = offset; }
		
}
