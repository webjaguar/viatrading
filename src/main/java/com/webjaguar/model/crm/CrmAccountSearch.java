/* Copyright 2010 Advanced E-Media Solutions
 * @author Shahin Naji
 * @since 02.01.2010
 */

package com.webjaguar.model.crm;

public class CrmAccountSearch {

	// sort
	private String sort = "id desc";
	public String getSort() { return sort; }
	public void setSort(String sort) { this.sort = sort; }
	
	// account Id
	private Integer accountId;
	public Integer getAccountId() { return accountId; }
	public void setAccountId(Integer accountId) { this.accountId = accountId; }
	
	// account Name
	private String accountName;
	public String getAccountName() { return accountName; }
	public void setAccountName(String accountName) { this.accountName = accountName; }
	
	// Access User
	private Integer accessUserId;
	public Integer getAccessUserId() { return accessUserId; }
	public void setAccessUserId(Integer accessUserId) { this.accessUserId = accessUserId; }
	
	// page 
	private int page = 1;
	public int getPage() { return page; }
	public void setPage(int page) { this.page = page; }
	
	// page size
	private int pageSize = 10;
	public int getPageSize() { return pageSize; }
	public void setPageSize(int pageSize) { this.pageSize = pageSize; }
	
	private Integer limit;
	public Integer getLimit() { return limit; }
	public void setLimit(Integer limit) { this.limit = limit; }
	
	// offset
	private int offset;
	public int getOffset() { return offset; }
	public void setOffset(int offset) { this.offset = offset; }
		
}
