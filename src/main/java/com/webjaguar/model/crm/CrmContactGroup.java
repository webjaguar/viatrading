package com.webjaguar.model.crm;

import java.sql.Timestamp;
import java.util.Date;

public class CrmContactGroup {
	
	private Integer id;
	private String name;
	private Boolean active;
	private Date created;
	private Timestamp lastModified;
	private Integer numContact;
	private Integer messageId;
	
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public Boolean getActive() {
		return active;
	}
	public void setActive(Boolean active) {
		this.active = active;
	}
	public Date getCreated() {
		return created;
	}
	public void setCreated(Date created) {
		this.created = created;
	}
	public Timestamp getLastModified() {
		return lastModified;
	}
	public void setLastModified(Timestamp lastModified) {
		this.lastModified = lastModified;
	}
	public Integer getNumContact() {
		return numContact;
	}
	public void setNumContact(Integer numContact) {
		this.numContact = numContact;
	}
	public void setMessageId(Integer messageId) {
		this.messageId = messageId;
	}
	public Integer getMessageId() {
		return messageId;
	}
	
	
}