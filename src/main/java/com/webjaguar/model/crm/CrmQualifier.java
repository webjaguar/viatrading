package com.webjaguar.model.crm;

import java.sql.Timestamp;
import java.util.Date;

import com.webjaguar.model.Address;

public class CrmQualifier {
	
	private Integer id;
    private Integer parent;	
	private String accountNumber;
	private String name;
	private String email;
	private String phone;
	private String cellPhone;
	private Address address;
	private String note;
	private String territoryZip;
	private Timestamp created;
	private Timestamp lastModified;
	private String token;
	private String password;
	private Timestamp lastLogin;
	private Integer numOfLogins;
	private Integer subCount;
	private boolean editPrice;
	private String protectedAccess = "0";	
	private boolean assignedToCustomerInCycle;
	private boolean inactive;
	private String salesrepGroup;
	
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public Integer getParent() {
		return parent;
	}
	public void setParent(Integer parent) {
		this.parent = parent;
	}
	public String getAccountNumber() {
		return accountNumber;
	}
	public void setAccountNumber(String accountNumber) {
		this.accountNumber = accountNumber;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getPhone() {
		return phone;
	}
	public void setPhone(String phone) {
		this.phone = phone;
	}
	public String getCellPhone() {
		return cellPhone;
	}
	public void setCellPhone(String cellPhone) {
		this.cellPhone = cellPhone;
	}
	public Address getAddress() {
		return address;
	}
	public void setAddress(Address address) {
		this.address = address;
	}
	public String getNote() {
		return note;
	}
	public void setNote(String note) {
		this.note = note;
	}
	public String getTerritoryZip() {
		return territoryZip;
	}
	public void setTerritoryZip(String territoryZip) {
		this.territoryZip = territoryZip;
	}
	public Timestamp getCreated() {
		return created;
	}
	public void setCreated(Timestamp created) {
		this.created = created;
	}
	public Timestamp getLastModified() {
		return lastModified;
	}
	public void setLastModified(Timestamp lastModified) {
		this.lastModified = lastModified;
	}
	public String getToken() {
		return token;
	}
	public void setToken(String token) {
		this.token = token;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public Timestamp getLastLogin() {
		return lastLogin;
	}
	public void setLastLogin(Timestamp lastLogin) {
		this.lastLogin = lastLogin;
	}
	public Integer getNumOfLogins() {
		return numOfLogins;
	}
	public void setNumOfLogins(Integer numOfLogins) {
		this.numOfLogins = numOfLogins;
	}
	public Integer getSubCount() {
		return subCount;
	}
	public void setSubCount(Integer subCount) {
		this.subCount = subCount;
	}
	public boolean isEditPrice() {
		return editPrice;
	}
	public void setEditPrice(boolean editPrice) {
		this.editPrice = editPrice;
	}
	public String getProtectedAccess() {
		return protectedAccess;
	}
	public void setProtectedAccess(String protectedAccess) {
		this.protectedAccess = protectedAccess;
	}
	public boolean isAssignedToCustomerInCycle() {
		return assignedToCustomerInCycle;
	}
	public void setAssignedToCustomerInCycle(boolean assignedToCustomerInCycle) {
		this.assignedToCustomerInCycle = assignedToCustomerInCycle;
	}
	public boolean isInactive() {
		return inactive;
	}
	public void setInactive(boolean inactive) {
		this.inactive = inactive;
	}
	public String getSalesrepGroup() {
		return salesrepGroup;
	}
	public void setSalesrepGroup(String salesrepGroup) {
		this.salesrepGroup = salesrepGroup;
	}
	
}
