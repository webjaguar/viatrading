package com.webjaguar.model.crm;

import java.util.List;

public class CrmForm {

	private Integer formId;
	private String formName;
	private String recipientEmail;
	private String recipientCcEmail;
	private String recipientBccEmail;
	private String returnUrl = "http://";
	private Integer numberOfFields;
	private String formNumber;
	private boolean doubleOptIn;
	private List<CrmFormField> crmFields;
	private String header;
	private String footer;
	private String sendButton;
	
	public Integer getFormId() {
		return formId;
	}
	public void setFormId(Integer formId) {
		this.formId = formId;
	}
	public String getFormName() {
		return formName;
	}
	public void setFormName(String formName) {
		this.formName = formName;
	}
	public String getRecipientEmail() {
		return recipientEmail;
	}
	public void setRecipientEmail(String recipientEmail) {
		this.recipientEmail = recipientEmail;
	}
	public String getReturnUrl() {
		return returnUrl;
	}
	public void setReturnUrl(String returnUrl) {
		this.returnUrl = returnUrl;
	}
	public void setCrmFields(List<CrmFormField> crmFields) {
		this.crmFields = crmFields;
	}
	public List<CrmFormField> getCrmFields() {
		return crmFields;
	}
	public void setNumberOfFields(Integer numberOfFields) {
		this.numberOfFields = numberOfFields;
	}
	public Integer getNumberOfFields() {
		return numberOfFields;
	}
	public void setFormNumber(String formNumber) {
		this.formNumber = formNumber;
	}
	public String getFormNumber() {
		return formNumber;
	}
	public void setDoubleOptIn(boolean doubleOptIn) {
		this.doubleOptIn = doubleOptIn;
	}
	public boolean isDoubleOptIn() {
		return doubleOptIn;
	}
	public String getHeader() {
		return header;
	}
	public void setHeader(String header) {
		this.header = header;
	}
	public String getFooter() {
		return footer;
	}
	public void setFooter(String footer) {
		this.footer = footer;
	}
	public String getSendButton() {
		return sendButton;
	}
	public void setSendButton(String sendButton) {
		this.sendButton = sendButton;
	}
	public void setRecipientCcEmail(String recipientCcEmail) {
		this.recipientCcEmail = recipientCcEmail;
	}
	public String getRecipientCcEmail() {
		return recipientCcEmail;
	}
	public void setRecipientBccEmail(String recipientBccEmail) {
		this.recipientBccEmail = recipientBccEmail;
	}
	public String getRecipientBccEmail() {
		return recipientBccEmail;
	}
	
	
}
