package com.webjaguar.model.crm;

public class CrmFormField {

	private Integer fieldId;
	private Integer formId;
	private String fieldName;
	private Integer type;
	private boolean required;
	private Integer contactFieldId;
	private String options;
	
	public Integer getFieldId() {
		return fieldId;
	}
	public void setFieldId(Integer fieldId) {
		this.fieldId = fieldId;
	}
	public Integer getFormId() {
		return formId;
	}
	public void setFormId(Integer formId) {
		this.formId = formId;
	}
	public String getFieldName() {
		return fieldName;
	}
	public void setFieldName(String fieldName) {
		this.fieldName = fieldName;
	}
	public Integer getType() {
		return type;
	}
	public void setType(Integer type) {
		this.type = type;
	}
	public void setRequired(boolean required) {
		this.required = required;
	}
	public boolean isRequired() {
		return required;
	}
	public Integer getContactFieldId() {
		return contactFieldId;
	}
	public void setContactFieldId(Integer contactFieldId) {
		this.contactFieldId = contactFieldId;
	}
	public void setOptions(String options) {
		this.options = options;
	}
	public String getOptions() {
		return options;
	}
	
}
