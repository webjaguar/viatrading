/* Copyright 2010 Advanced E-Media Solutions
 * @author Shahin Naji
 * @since 02.01.2010
 */

package com.webjaguar.model.crm;

public class CrmQualifierSearch {

	// sort
	private String sort = "sales_rep_id desc";
	public String getSort() { return sort; }
	public void setSort(String sort) { this.sort = sort; }
	
	// qualifier Id
	private Integer qualifierId;
	public Integer getQualifierId() { return qualifierId; }
	public void setQualifierId(Integer qualifierId) { this.qualifierId = qualifierId; }
	
	// qualifier Name
	private String name;
	public String getName() { return name; }
	public void setName(String name) { this.name = name; }
	
	// page 
	private int page = 1;
	public int getPage() { return page; }
	public void setPage(int page) { this.page = page; }
	
	// page size
	private int pageSize = 10;
	public int getPageSize() { return pageSize; }
	public void setPageSize(int pageSize) { this.pageSize = pageSize; }
	
	private Integer limit;
	public Integer getLimit() { return limit; }
	public void setLimit(Integer limit) { this.limit = limit; }
	
	// offset
	private int offset;
	public int getOffset() { return offset; }
	public void setOffset(int offset) { this.offset = offset; }
		
}
