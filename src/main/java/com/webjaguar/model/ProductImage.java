/* Copyright 2006 Advanced E-Media Solutions
 *
 */

package com.webjaguar.model;

import java.io.Serializable;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;
@XmlRootElement(name = "images")
@XmlAccessorType(XmlAccessType.FIELD)
public class ProductImage implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String imageUrl;
	@XmlTransient
	private byte index;

	public ProductImage() {
	}

	public String getImageUrl() {
		return imageUrl;
	}

	public void setImageUrl(String imageUrl) {
		this.imageUrl = imageUrl;
	}

	public boolean isAbsolute() {
		if (imageUrl != null && (imageUrl.contains("//") || imageUrl.startsWith("/"))) {
			return true;
		} else {
			return false;			
		}
	}

	public byte getIndex() {
		return index;
	}

	public void setIndex(byte index) {
		this.index = index;
	}

}
