/* Copyright 2005, 2008 Advanced E-Media Solutions
 *
 */

package com.webjaguar.model;

import java.util.Date;

import com.webjaguar.web.domain.Utilities;


public class EmailCampaign {
	
	private Integer id;
    private String campaignName;
    private String sender;
    private String subject;
    private String message;
    private Date created;
    private Date scheduled;
    private Date sent;
    private String status;		//sch = scheduled, pro = processing, com = complete
    private Integer emailSent;
    private Integer emailToSend;
    private Integer emailBounced;
    private Integer openedCount;
    private Integer openedUnique;
    private Integer clickUnique;
    private Integer unsubscribeCount;
    
    
	public EmailCampaign(Date created)
	{
		super();
		this.created = created;
	}
	public EmailCampaign()
	{
		super();
	}
	public EmailCampaign( Integer id, String status, Integer emailSent )
	{
		super();
		this.id = id;
		this.status = status;
		this.emailSent = emailSent;
	}
	public Integer getId()
	{
		return id;
	}
	public void setId(Integer id)
	{
		this.id = id;
	}
	public String getCampaignName()
	{
		return campaignName;
	}
	public void setCampaignName(String campaignName)
	{
		this.campaignName = campaignName;
	}
	public String getSender()
	{
		return sender;
	}
	public void setSender(String sender)
	{
		this.sender = sender;
	}
	public String getSubject()
	{
		return subject;
	}
	public void setSubject(String subject)
	{
		this.subject = subject;
	}
	public Date getCreated()
	{
		return created;
	}
	public void setCreated(Date created)
	{
		this.created = created;
	}
	public Date getSent()
	{
		return sent;
	}
	public void setSent(Date sent)
	{
		this.sent = sent;
	}
	public String getStatus()
	{
		return status;
	}
	public void setStatus(String status)
	{
		this.status = status;
	}
	public Integer getEmailSent()
	{
		return emailSent;
	}
	public void setEmailSent(Integer emailSent)
	{
		this.emailSent = emailSent;
	}
	public String getMessage()
	{
		return message;
	}
	public void setMessage(String message)
	{
		this.message = message;
	}
	public Date getScheduled()
	{
		return scheduled;
	}
	public void setScheduled(Date scheduled)
	{
		this.scheduled = scheduled;
	}
	public Integer getEmailToSend()
	{
		return emailToSend;
	}
	public void setEmailToSend(Integer emailToSend)
	{
		this.emailToSend = emailToSend;
	}
	public Integer getEmailBounced() {
		return emailBounced;
	}
	public void setEmailBounced(Integer emailBounced) {
		this.emailBounced = emailBounced;
	}
	public Integer getOpenedCount() {
		return openedCount;
	}
	public void setOpenedCount(Integer openedCount) {
		this.openedCount = openedCount;
	}
	public Integer getOpenedUnique() {
		return openedUnique;
	}
	public void setOpenedUnique(Integer openedUnique) {
		this.openedUnique = openedUnique;
	}
	
	public String getOpenRate() {
		// openUnique / totalEmai * 100
		return Utilities.decimalFormatString(((this.openedUnique / (double) ((this.emailSent==null||this.emailSent == 0 ) ? 1 : this.emailSent)) * 100), "");
	}
	public String getClickThroughRate(){
		return Utilities.decimalFormatString((((double) ((this.clickUnique==null) ? 0 : this.clickUnique) / (double) ((this.openedUnique==null||this.openedUnique == 0 ) ? 1 : this.openedUnique)) * 100), "");
	}
	public Integer getClickUnique() {
		return clickUnique;
	}
	public void setClickUnique(Integer clickUnique) {
		this.clickUnique = clickUnique;
	}
	public Integer getUnsubscribeCount() {
		return unsubscribeCount;
	}
	public void setUnsubscribeCount(Integer unsubscribeCount) {
		this.unsubscribeCount = unsubscribeCount;
	}
	
}
