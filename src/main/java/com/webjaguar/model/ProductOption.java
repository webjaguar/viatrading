/* Copyright 2006 Advanced E-Media Solutions
 * @author Eduardo Asprec
 * @since 09.26.2006
 */

package com.webjaguar.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class ProductOption implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private int productId;
	private int index;
	private String optionCode;
	private int optionId;
	private String name;
	private String message;
	private String valueMessage;
	private String type;
	private List<ProductOptionValue> values;
	private String helpText;
	private int numberOfColors;
	
	//asi
	private String asiCriteriaSetId;
	private String asiDescription;
	
	/* Constructors */

	public ProductOption() {}
	
	public ProductOption(int index) {
		this.index = index;
	}

	public String getName() { return name; }
	public void setName(String name) {
		this.name = name;
	}
	
	public List<ProductOptionValue> getValues() { return values; }
	public void setValues(List<ProductOptionValue> options) { this.values = options; }
	public void addValue(ProductOptionValue pov) { 
		if (values == null) values = new ArrayList<ProductOptionValue>();
		values.add(pov); 
	}
	
	public int getIndex() {
		return index;
	}
	
	public void setIndex(int index) {
		this.index = index;
	}
	
	public String getOptionCode() {
		return optionCode;
	}

	public void setOptionCode(String optionCode) {
		this.optionCode = optionCode;
	}

	public String getValuesToTextArea() {
		StringBuffer sbuff = new StringBuffer();
		if (values != null) {
			for (ProductOptionValue value:values) {
				if ( value.getOptionPrice() != null ) {
					if(value.isOneTimePrice()) {
						sbuff.append(value.getName() + " |$" + value.getOptionPrice());
					} else {
						sbuff.append(value.getName() + " $" + value.getOptionPrice());
					}
				}
				else
					sbuff.append(value.getName());
				if ( value.getImageUrl() != null )
					sbuff.append(" @" + value.getImageUrl());
				if ( value.getOptionWeight() != null )
					sbuff.append(" #" + value.getOptionWeight());
				if ( value.getDependingOptionIds() != null )
					sbuff.append(" ^" + value.getDependingOptionIds());
				if ( value.getDescription() != null )
					sbuff.append(" <txt>" + value.getDescription()+"</txt>");
				if ( value.getAssignedProductId() != null )
					sbuff.append(" <id>" + value.getAssignedProductId()+"</id>");
				if ( value.getIncludedProducts() != null)
					sbuff.append(" "+value.getIncludedProducts());
				sbuff.append("\n");
			}
		}
		return sbuff.toString();
	}

	public int getProductId() {
		return productId;
	}

	public void setProductId(int productId) {
		this.productId = productId;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public String getValueMessage() {
		return valueMessage;
	}

	public void setValueMessage(String valueMessage) {
		this.valueMessage = valueMessage;
	}

	public String getType()
	{
		return type;
	}

	public void setType(String type)
	{
		this.type = type;
	}
	
	public boolean ishasImage()
	{
		if (values != null) {	
			for ( ProductOptionValue value : values ) {
				if ( value.getImageUrl() != null ) {
					return true;
				}
			}
		}
		return false;
	}

	public void setAsiCriteriaSetId(String asiCriteriaSetId) {
		this.asiCriteriaSetId = asiCriteriaSetId;
	}

	public String getAsiCriteriaSetId() {
		return asiCriteriaSetId;
	}

	public void setAsiDescription(String asiDescription) {
		this.asiDescription = asiDescription;
	}

	public String getAsiDescription() {
		return asiDescription;
	}

	public void setHelpText(String helpText) {
		this.helpText = helpText;
	}

	public String getHelpText() {
		return helpText;
	}

	public void setNumberOfColors(int numberOfColors) {
		this.numberOfColors = numberOfColors;
	}

	public int getNumberOfColors() {
		return numberOfColors;
	}
	public int getOptionId() {
		return optionId;
	}

	public void setOptionId(int optionId) {
		this.optionId = optionId;
	}
}