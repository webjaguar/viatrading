/* Copyright 2009 Advanced E-Media Solutions
 * @author Eduardo Asprec
 * @since 10.09.2009
 */

package com.webjaguar.model;

public class SalesRepSearch {

	// sort
	private String sort= "name";
	public String getSort() { return sort; }
	public void setSort(String sort) { this.sort = sort; }	
	
	// name
	private String name;
	public String getName() { return name; }
	public void setName(String name) { this.name = name; }	
	
	// email
	private String email;
	public String getEmail() { return email; }
	public void setEmail(String email) { this.email = email; }

	// account number
	private String accountNumber;
	public String getAccountNumber() { return accountNumber; }
	public void setAccountNumber(String accountNumber) { this.accountNumber = accountNumber; } 	

	// page 
	private int page = 1;
	public int getPage() { return page; }
	public void setPage(int page) { this.page = page; }
	
	// page size
	private int pageSize = 10;
	public int getPageSize() { return pageSize; }
	public void setPageSize(int pageSize) { this.pageSize = pageSize; }
	
	// parent ID
	private Integer parent;
	public Integer getParent() { return parent; }
	public void setParent(Integer parent) { this.parent = parent; }
	
	// show Active SalesRep
	private boolean showActiveSalesRep;
	public boolean isShowActiveSalesRep() { return showActiveSalesRep; }
	public void setShowActiveSalesRep(boolean showActiveSalesRep) { this.showActiveSalesRep = showActiveSalesRep; }
	
	// show Active SalesRep
	private boolean showNonActiveSalesRep;
	public boolean isShowNonActiveSalesRep() { return showNonActiveSalesRep; }
	public void setShowNonActiveSalesRep(boolean showNonActiveSalesRep) { this.showNonActiveSalesRep = showNonActiveSalesRep; }

	// group
	private String salesrepGroup;
	public String getSalesrepGroup() { return salesrepGroup; }
	public void setSalesrepGroup(String salesrepGroup) { this.salesrepGroup = salesrepGroup; }
}
