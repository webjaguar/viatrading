package com.webjaguar.model;

import java.io.Serializable;
import java.util.Date;

public class MailInRebate implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private Integer rebateId;
	private String title;
	private Double discount;
	private boolean percent;
	private Date startDate;
	private Date endDate;
	private String htmlCode;
	
	public boolean isInEffect() {
		// today's date
		com.webjaguar.model.DateBean today = new com.webjaguar.model.DateBean( );

		int start = this.startDate.compareTo( today.getTime() );
		int end = this.endDate.compareTo( today.getTime() );
		if ( start == 0 )
			return true;
		else if ( start > 0 || end < 0  )
			return false;
		else
			return true;
		}
		
	public Integer getRebateId() {
		return rebateId;
	}
	public void setRebateId(Integer rebateId) {
		this.rebateId = rebateId;
	}
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public Double getDiscount() {
		return discount;
	}
	public void setDiscount(Double discount) {
		this.discount = discount;
	}
	public boolean isPercent() {
		return percent;
	}
	public void setPercent(boolean percent) {
		this.percent = percent;
	}
	public Date getStartDate() {
		return startDate;
	}
	public void setStartDate(Date startDate) {
		this.startDate = startDate;
	}
	public Date getEndDate() {
		return endDate;
	}
	public void setEndDate(Date endDate) {
		this.endDate = endDate;
	}

	public void setHtmlCode(String htmlCode) {
		this.htmlCode = htmlCode;
	}

	public String getHtmlCode() {
		return htmlCode;
	}

}
