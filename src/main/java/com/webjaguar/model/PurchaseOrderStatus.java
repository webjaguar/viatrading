/* Copyright 2007 Advanced E-Media Solutions
 * @author Shahin Naji
 * @since 12.21.2007
 */

package com.webjaguar.model;

import java.sql.Timestamp;

public class PurchaseOrderStatus {
	
	private Integer id;
	private Integer poId;
	private String status = "pend";
	private Timestamp dateChanged;
	private String comments;
	private String updatedBy;
	private boolean sendEmail;
	
	public String getUpdatedBy()
	{
		return updatedBy;
	}
	public void setUpdatedBy(String updatedBy)
	{
		this.updatedBy = updatedBy;
	}
	public Integer getId()
	{
		return id;
	}
	public void setId(Integer id)
	{
		this.id = id;
	}
	public Integer getPoId()
	{
		return poId;
	}
	public void setPoId(Integer poId)
	{
		this.poId = poId;
	}
	public String getStatus()
	{
		return status;
	}
	public void setStatus(String status)
	{
		this.status = status;
	}
	public Timestamp getDateChanged()
	{
		return dateChanged;
	}
	public void setDateChanged(Timestamp dateChanged)
	{
		this.dateChanged = dateChanged;
	}
	public String getComments()
	{
		return comments;
	}
	public void setComments(String comments)
	{
		this.comments = comments;
	}
	public boolean isSendEmail() {
		return sendEmail;
	}
	public void setSendEmail(boolean sendEmail) {
		this.sendEmail = sendEmail;
	}

}
