/* Copyright 2006 Advanced E-Media Solutions
 * @author Eduardo Asprec
 * @since 09.27.2006
 */

package com.webjaguar.model;

import java.io.ByteArrayInputStream;
import java.io.Serializable;
import java.util.List;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.Unmarshaller;

public class ProductOptionValue implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private int productId;
	private int optionIndex;
	private int index;
	private String name;
	private Double optionPrice;
	private Double optionPriceOriginal;
	private Double optionWeight;
	private String imageUrl;
	private boolean oneTimePrice;
	private String dependingOptionIds;
	private String description;
	private Integer assignedProductId;
	private String assignedProductSku;
	private boolean assignedSkuAttachment;
	private String includedProducts;
	
	/* Constructors */
	public Double getOptionPrice() {
		return optionPrice;
	}

	public void setOptionPrice(Double optionPrice) {
		this.optionPrice = optionPrice;
	}
	
	public Double getOptionPriceOriginal() {
		return optionPriceOriginal;
	}

	public void setOptionPriceOriginal(Double optionPriceOriginal) {
		this.optionPriceOriginal = optionPriceOriginal;
	}

	public Double getAbsoluteOptionPrice() {
		return Math.abs( this.getOptionPrice() );
	}

	public ProductOptionValue() {}
	
	public ProductOptionValue(int index) {
		this.index = index;
	}
	
	public String getName() { return name; }
	public void setName(String name) {
		this.name = name;
	}
	
	public int getIndex() {
		return index;
	}
	
	public void setIndex(int index) {
		this.index = index;
	}

	public int getOptionIndex() {
		return optionIndex;
	}

	public void setOptionIndex(int optionIndex) {
		this.optionIndex = optionIndex;
	}

	public int getProductId() {
		return productId;
	}

	public void setProductId(int productId) {
		this.productId = productId;
	}
	
	public String getImageUrl() {
		return imageUrl;
	}

	public void setImageUrl(String imageUrl) {
		this.imageUrl = imageUrl;
	}

	public void setOptionWeight(Double optionWeight) {
		this.optionWeight = optionWeight;
	}

	public Double getOptionWeight() {
		return optionWeight;
	}

	public void setOneTimePrice(boolean oneTimePrice) {
		this.oneTimePrice = oneTimePrice;
	}

	public boolean isOneTimePrice() {
		return oneTimePrice;
	}

	public void setDependingOptionIds(String dependingOptionIds) {
		this.dependingOptionIds = dependingOptionIds;
	}

	public String getDependingOptionIds() {
		return dependingOptionIds;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getDescription() {
		return description;
	}

	public void setAssignedProductId(Integer assignedProductId) {
		this.assignedProductId = assignedProductId;
	}

	public Integer getAssignedProductId() {
		return assignedProductId;
	}

	public void setAssignedProductSku(String assignedProductSku) {
		this.assignedProductSku = assignedProductSku;
	}

	public String getAssignedProductSku() {
		return assignedProductSku;
	}

	public void setAssignedSkuAttachment(boolean assignedSkuAttachment) {
		this.assignedSkuAttachment = assignedSkuAttachment;
	}

	public boolean isAssignedSkuAttachment() {
		return assignedSkuAttachment;
	}

	public void setIncludedProducts(String includedProducts) {
		this.includedProducts = includedProducts;
	}

	public String getIncludedProducts() {
		return includedProducts;
	}

	public List<IncludedProduct> getIncludedProductList() {
		if(this.getIncludedProducts() == null) {
			return null;
		}
		
		JAXBContext context;
		IncludedProductXML pXml = null;
		try {
			context = JAXBContext.newInstance(IncludedProductXML.class);
			Unmarshaller unmarshaller = context.createUnmarshaller();
			StringBuffer sb = new StringBuffer(this.getIncludedProducts());
			ByteArrayInputStream bis = new ByteArrayInputStream(sb.toString().getBytes("UTF-8"));
		    pXml = (IncludedProductXML) unmarshaller.unmarshal(bis);
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
		return pXml.getProduct();
	}

}