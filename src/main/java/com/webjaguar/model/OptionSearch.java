/* Copyright 2007 Advanced E-Media Solutions
 * @author Eduardo Asprec
 * @since 10.18.2007
 */

package com.webjaguar.model;

public class OptionSearch {

	private String optionCode;
	private int page = 1;
	private int pageSize = 10;
	// offset
	private int offset;
	// limit
	private Integer limit;
	// sort
	private String sort= "option_code";
	private boolean productCount;
	
	public String getOptionCode()
	{
		return optionCode;
	}
	public void setOptionCode(String optionCode)
	{
		this.optionCode = optionCode;
	}
	public int getPage()
	{
		return page;
	}
	public void setPage(int page)
	{
		this.page = page;
	}
	public int getPageSize()
	{
		return pageSize;
	}
	public void setPageSize(int pageSize)
	{
		this.pageSize = pageSize;
	}
	public String getSort()
	{
		return sort;
	}
	public void setSort(String sort)
	{
		this.sort = sort;
	}
	public boolean isProductCount() {
		return productCount;
	}
	public void setProductCount(boolean productCount) {
		this.productCount = productCount;
	}
	public int getOffset() 
	{ 
		return offset; 
	}
	public void setOffset(int offset) 
	{ 
		this.offset = offset; 
	}	
	public Integer getLimit() 
	{ 
		return limit; 
	}
	public void setLimit(Integer limit) 
	{ 
		this.limit = limit; 
	}
}
