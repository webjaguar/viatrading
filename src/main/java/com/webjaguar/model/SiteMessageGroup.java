package com.webjaguar.model;

import java.util.Date;

public class SiteMessageGroup {
	private Integer id;
	private String name;
	private boolean active;
	private Date created;
	private Integer numMessage;
	
	
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public boolean isActive() {
		return active;
	}
	public void setActive(boolean active) {
		this.active = active;
	}
	public Date getCreated() {
		return created;
	}
	public void setCreated(Date created) {
		this.created = created;
	}
	public Integer getNumMessage() {
		return numMessage;
	}
	public void setNumMessage(Integer numMessage) {
		this.numMessage = numMessage;
	}
}
