/* Copyright 2008 Advanced E-Media Solutions
 * @author Eduardo Asprec
 * @since 03.31.2008
 */

package com.webjaguar.model;

public class SubscriptionSearch {	
	
	// userId
	private Integer userId;
	public Integer getUserId() { return userId; }
	public void setUserId(Integer userId) { this.userId = userId; }		

	// page 
	private int page = 1;
	public int getPage() { return page; }
	public void setPage(int page) { this.page = page; }
	
	// page size
	private int pageSize = 10;
	public int getPageSize() { return pageSize; }
	public void setPageSize(int pageSize) { this.pageSize = pageSize; }	
	
	// sort
	private String sort = "next_order, username";
	public String getSort() { return sort; }
	public void setSort(String sort) { this.sort = sort; }	
	
	// enabled
	private String enabled;
	public String getEnabled() { return enabled; }
	public void setEnabled(String enabled) { this.enabled = enabled; }

	// code
	private String code;
	public String getCode() { return code; }
	public void setCode(String code) { this.code = code; }
}
