/* Copyright 2005, 2006 Advanced E-Media Solutions
 *
 */

package com.webjaguar.model;

public class Policy {

	public Policy() { }
	
    private Integer id;
	private String name;
	private String content;
    private Integer rank;
	
	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}
	
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}

	public Integer getRank() {
		return rank;
	}

	public void setRank(Integer rank) {
		this.rank = rank;
	}


}
