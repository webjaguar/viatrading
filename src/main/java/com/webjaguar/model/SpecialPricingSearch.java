/* Copyright 2006 Advanced E-Media Solutions
 *
 */

package com.webjaguar.model;

import java.util.ArrayList;
import java.util.List;

public class SpecialPricingSearch {
	
	// sku
	private String sku;
	public String getSku() { return sku; }
	public void setSku(String sku) { this.sku = sku; }	
	
	// customer ID
	private Integer customerId;
	public Integer getCustomerId() { return customerId; }
	public void setCustomerId(Integer customerId) { this.customerId = customerId; }
	
	// page 
	private int page = 1;
	public int getPage() { return page; }
	public void setPage(int page) { this.page = page; }
	
	// page size
	private int pageSize = 10;
	public int getPageSize() { return pageSize; }
	public void setPageSize(int pageSize) { this.pageSize = pageSize; }
	
	// constructor
	public SpecialPricingSearch( String sku, Integer customerId, int page, int pageSize )
	{
		super();
		this.sku = sku;
		this.customerId = customerId;
		this.page = page;
		this.pageSize = pageSize;
	}
	public SpecialPricingSearch()
	{
		super();
	}

	
}
