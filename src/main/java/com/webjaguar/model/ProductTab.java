package com.webjaguar.model;

public class ProductTab
{
	private String tabName;
	private String tabContent;
	private int tabNumber;
	public ProductTab( String tabName, String tabContent, int tabNumber )
	{
		super();
		this.tabName = tabName;
		this.tabContent = tabContent;
		this.tabNumber = tabNumber;
	}
	public String getTabName()
	{
		return tabName;
	}
	public void setTabName(String tabName)
	{
		this.tabName = tabName;
	}
	public String getTabContent()
	{
		return tabContent;
	}
	public void setTabContent(String tabContent)
	{
		this.tabContent = tabContent;
	}
	public int getTabNumber() {
		return tabNumber;
	}
	public void setTabNumber(int tabNumber) {
		this.tabNumber = tabNumber;
	}
}
