/*
 * Copyright 2008 Advanced E-Media Solutions
 * 
 */

package com.webjaguar.model;

public class CityReport
{

	private String city;
	private String state;
	private String country;
	private Integer numOrdr;
	private Double grossRev;
	public String getCity() {
		return city;
	}
	public void setCity(String city) {
		this.city = city;
	}
	public String getState() {
		return state;
	}
	public void setState(String state) {
		this.state = state;
	}
	public String getCountry() {
		return country;
	}
	public void setCountry(String country) {
		this.country = country;
	}
	public Integer getNumOrdr() {
		return numOrdr;
	}
	public void setNumOrdr(Integer numOrdr) {
		this.numOrdr = numOrdr;
	}
	public Double getGrossRev() {
		return grossRev;
	}
	public void setGrossRev(Double grossRev) {
		this.grossRev = grossRev;
	}
	
}
