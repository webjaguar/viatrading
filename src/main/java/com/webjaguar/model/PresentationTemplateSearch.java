package com.webjaguar.model;

public class PresentationTemplateSearch {

	// sort
	private String sort= "template_id desc";
	public String getSort() { return sort; }
	public void setSort(String sort) { this.sort = sort; }	
	
	// name
	private String name;
	public String getName() { return name; }
	public void setName(String name) { this.name = name; }	
	
	// noOfProduct 
	private Integer noOfProduct;
	public Integer getNoOfProduct() { return noOfProduct; }
	public void setNoOfProduct(Integer noOfProduct) { this.noOfProduct = noOfProduct; }
	
	// page 
	private int page = 1;
	public int getPage() { return page; }
	public void setPage(int page) { this.page = page; }
	
	// page size
	private int pageSize = 10;
	public int getPageSize() { return pageSize; }
	public void setPageSize(int pageSize) { this.pageSize = pageSize; }
	
}
