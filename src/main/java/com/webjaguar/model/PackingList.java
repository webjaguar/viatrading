/* Copyright 2005, 2008 Advanced E-Media Solutions
 *
 */

package com.webjaguar.model;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

public class PackingList {

    private String packingListNumber;
	private Integer orderId;
	private String tracking;
    private Timestamp created;
    private Timestamp lastModified;
    private Date shipped;
    private Double shippingCost;
    private boolean userNotified;
    private List<LineItem> lineItems = new ArrayList<LineItem>();
    //private List<PackingListLineItem> deletedLineItems = new ArrayList<PackingListLineItem>();
    
    private Address billing;
	private Address shipping;
	private Integer userId;
	private String host;
	private String shippingMethod;
	private Double subTotal;
	private Double grandTotal;
	
	private String cancelReason;

	public String getCancelReason() {
		return cancelReason;
	}
	public void setCancelReason(String cancelReason) {
		this.cancelReason = cancelReason;
	}
	private CreditCard creditCard;
	private Paypal paypal;
	
	private String status;
	private String invoiceNote = "";
	private String paymentMethod;
	private String userEmail;
	
	public String getPackingListNumber()
	{
		return packingListNumber;
	}
	public void setPackingListNumber(String packingListNumber)
	{
		this.packingListNumber = packingListNumber;
	}
	public Integer getOrderId()
	{
		return orderId;
	}
	public void setOrderId(Integer orderId)
	{
		this.orderId = orderId;
	}
	public String getTracking()
	{
		return tracking;
	}
	public void setTracking(String tracking)
	{
		this.tracking = tracking;
	}
	public Timestamp getCreated()
	{
		return created;
	}
	public void setCreated(Timestamp created)
	{
		this.created = created;
	}
	public Timestamp getLastModified()
	{
		return lastModified;
	}
	public void setLastModified(Timestamp lastModified)
	{
		this.lastModified = lastModified;
	}
	public List<LineItem> getLineItems()
	{
		return lineItems;
	}
	public void setLineItems(List<LineItem> lineItems)
	{
		this.lineItems = lineItems;
	}
	public boolean isUserNotified()
	{
		return userNotified;
	}
	public void setUserNotified(boolean userNotified)
	{
		this.userNotified = userNotified;
	}
	public Date getShipped()
	{
		return shipped;
	}
	public void setShipped(Date shipped)
	{
		this.shipped = shipped;
	}
	public void setShippingCost(Double shippingCost) {
		this.shippingCost = shippingCost;
	}
	public Double getShippingCost() {
		return shippingCost;
	}
	public Address getBilling() {
		return billing;
	}
	public void setBilling(Address billing) {
		this.billing = billing;
	}
	public Address getShipping() {
		return shipping;
	}
	public void setShipping(Address shipping) {
		this.shipping = shipping;
	}
	public Integer getUserId() {
		return userId;
	}
	public void setUserId(Integer userId) {
		this.userId = userId;
	}
	public String getHost() {
		return host;
	}
	public void setHost(String host) {
		this.host = host;
	}
	public String getShippingMethod() {
		return shippingMethod;
	}
	public void setShippingMethod(String shippingMethod) {
		this.shippingMethod = shippingMethod;
	}
	public void setSubTotal(Double subTotal) {
		this.subTotal = subTotal;
	}
	public Double getSubTotal() {
		return subTotal;
	}
	public CreditCard getCreditCard() {
		return creditCard;
	}
	public void setCreditCard(CreditCard creditCard) {
		this.creditCard = creditCard;
	}
	public Paypal getPaypal() {
		return paypal;
	}
	public void setPaypal(Paypal paypal) {
		this.paypal = paypal;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public String getInvoiceNote() {
		return invoiceNote;
	}
	public void setInvoiceNote(String invoiceNote) {
		this.invoiceNote = invoiceNote;
	}
	public String getPaymentMethod() {
		return paymentMethod;
	}
	public void setPaymentMethod(String paymentMethod) {
		this.paymentMethod = paymentMethod;
	}
	public void setUserEmail(String userEmail) {
		this.userEmail = userEmail;
	}
	public String getUserEmail() {
		return userEmail;
	}
	public void setGrandTotal(Double grandTotal) {
		this.grandTotal = grandTotal;
	}
	public Double getGrandTotal() {
		return grandTotal;
	}
	public void setGrandTotal()
	{
		double tempTotal = 0.0;
		if(this.getSubTotal()!=null){
			tempTotal = this.getSubTotal();
		}
		double total = Math.floor( (tempTotal) * 100.0 + 0.5 ) / 100.0;
		this.grandTotal = total + ((this.shippingCost == null) ? 0.00 : this.shippingCost);
	}
	
}
