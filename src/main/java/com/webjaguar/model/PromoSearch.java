package com.webjaguar.model;

import java.util.Date;

public class PromoSearch {

	// sort
	private String sort = "promo_id desc";
	public String getSort() { return sort; }
	public void setSort(String sort) { this.sort = sort; }	
	
	// page 
	private int page = 1;
	public int getPage() { return page; }
	public void setPage(int page) { this.page = page; }
	
	// page size
	private int pageSize = 10;
	public int getPageSize() { return pageSize; }
	public void setPageSize(int pageSize) { this.pageSize = pageSize; }
	
	
	// promo type
	private String promoType;
	public String getPromoType() {
		return promoType;
	}
	public void setPromoType(String promoType) {
		this.promoType = promoType;
	}
	
	//product Sku
	private String sku;
	public String getSku() {
		return sku;
	}
	public void setSku(String sku) {
		this.sku = sku;
	}
	
	// prefix
		private String prefix;
		public String getPrefix() {
			return prefix;
		}
		public void setPrefix(String prefix) {
			this.prefix = prefix;
		}
	// userId	
		
		private int userId;
		public int getUserId() {
			return userId;
		}
		public void setUserId(int userId) {
			this.userId = userId;
		}
	
		// start date
		private Date startDate; 
		public Date getStartDate() { return startDate; }
		public void setStartDate(Date startDate) { this.startDate = startDate; }
		
		// end date
		private Date endDate;
		public Date getEndDate() { return endDate; }
		public void setEndDate(Date endDate) { this.endDate = endDate; }
		
		private String dateType = "promoDate";
		public String getDateType() { return dateType; }
		public void setDateType(String dateType) { this.dateType = dateType; }
	
	private Boolean active;

	public PromoSearch() {
		super();
	}
	public PromoSearch(String sort) {
		this.sort = sort;
	}
	public Boolean getActive() {
		return active;
	}
	public void setActive(Boolean active) {
		this.active = active;
	}

}
