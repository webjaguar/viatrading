package com.webjaguar.model.api;

import org.codehaus.jackson.annotate.JsonIgnore;
import org.codehaus.jackson.map.annotate.JsonSerialize;
import org.codehaus.jackson.map.annotate.JsonSerialize.Inclusion;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.webjaguar.model.Address;

@JsonSerialize()
public class ApiAddress {
	
	public ApiAddress() {}

	public ApiAddress(Address wjAddress) {
		this.id = wjAddress.getId();
		this.userId = wjAddress.getUserId();
		this.firstName = wjAddress.getFirstName();
		this.lastName = wjAddress.getLastName();
		this.company = wjAddress.getCompany();
		this.addr1 = wjAddress.getAddr1();
		this.addr2 = wjAddress.getAddr2();
//		this.addr3 = wjAddress.getAddr3();
		this.city = wjAddress.getCity();
		this.stateProvince = wjAddress.getStateProvince();
		this.stateProvinceNA = wjAddress.isStateProvinceNA();
		this.zip = wjAddress.getZip();
		this.country = wjAddress.getCountry();
		this.phone = wjAddress.getPhone();
		this.cellPhone = wjAddress.getCellPhone();
		this.fax = wjAddress.getFax();
		this.email = wjAddress.	getEmail();
		this.primary = wjAddress.isPrimary();
		this.residential = wjAddress.isResidential();  //By default is Commercial 
		this.liftGate = wjAddress.isLiftGate();
		this.defaultShipping = wjAddress.isDefaultShipping();
		//this.billing = wjAddress.isBilling();
		//this.shipping = wjAddress.isShipping();
//		this.soldToAccountNo = wjAddress.getSoldToAccountNo();
//		this.shipToAccountNo = wjAddress.getShipToAccountNo();
//		this.accountType = wjAddress.getAccountType();
		this.code =  wjAddress.getCode();
		//this.taxId = wjAddress.getTaxId(); // not unique
		//this.pushedToAccounting = wjAddress.isPushedToAccounting();
	}

    
	private Integer id;
	private Integer userId;
	private String firstName;
	private String lastName;
	private String company;
	private String addr1;
	private String addr2;
	private String addr3;
	private String city;
	private String stateProvince;
	private boolean stateProvinceNA;
	private String zip;
	private String country;
	private String phone;
	private String cellPhone;
	private String fax;
	private String email;	
	private boolean primary;
	private boolean residential = false;  //By default is Commercial 
	private boolean liftGate;
	private boolean defaultShipping;
	private String code;
	private String soldToAccountNo;
	private String shipToAccountNo;
	private String accountType;
	private String taxId; // not unique
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public Integer getUserId() {
		return userId;
	}
	public void setUserId(Integer userId) {
		this.userId = userId;
	}
	public String getFirstName() {
		return firstName;
	}
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}
	public String getLastName() {
		return lastName;
	}
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}
	public String getCompany() {
		return company;
	}
	public void setCompany(String company) {
		this.company = company;
	}
	public String getAddr1() {
		return addr1;
	}
	public void setAddr1(String addr1) {
		this.addr1 = addr1;
	}
	public String getAddr2() {
		return addr2;
	}
	public void setAddr2(String addr2) {
		this.addr2 = addr2;
	}
	public String getAddr3() {
		return addr3;
	}
	public void setAddr3(String addr3) {
		this.addr3 = addr3;
	}
	public String getCity() {
		return city;
	}
	public void setCity(String city) {
		this.city = city;
	}
	public String getStateProvince() {
		return stateProvince;
	}
	public void setStateProvince(String stateProvince) {
		this.stateProvince = stateProvince;
	}
	public boolean isStateProvinceNA() {
		return stateProvinceNA;
	}
	public void setStateProvinceNA(boolean stateProvinceNA) {
		this.stateProvinceNA = stateProvinceNA;
	}
	public String getZip() {
		return zip;
	}
	public void setZip(String zip) {
		this.zip = zip;
	}
	public String getCountry() {
		return country;
	}
	public void setCountry(String country) {
		this.country = country;
	}
	public String getPhone() {
		return phone;
	}
	public void setPhone(String phone) {
		this.phone = phone;
	}
	public String getCellPhone() {
		return cellPhone;
	}
	public void setCellPhone(String cellPhone) {
		this.cellPhone = cellPhone;
	}
	public String getFax() {
		return fax;
	}
	public void setFax(String fax) {
		this.fax = fax;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public boolean isPrimary() {
		return primary;
	}
	public void setPrimary(boolean primary) {
		this.primary = primary;
	}
	public boolean isResidential() {
		return residential;
	}
	public void setResidential(boolean residential) {
		this.residential = residential;
	}
	public boolean isDefaultShipping() {
		return defaultShipping;
	}
	public void setDefaultShipping(boolean defaultShipping) {
		this.defaultShipping = defaultShipping;
	}

	public String getSoldToAccountNo() {
		return soldToAccountNo;
	}
	public void setSoldToAccountNo(String soldToAccountNo) {
		this.soldToAccountNo = soldToAccountNo;
	}
	public String getShipToAccountNo() {
		return shipToAccountNo;
	}
	public void setShipToAccountNo(String shipToAccountNo) {
		this.shipToAccountNo = shipToAccountNo;
	}
	public String getAccountType() {
		return accountType;
	}
	public void setAccountType(String accountType) {
		this.accountType = accountType;
	}
	public String getTaxId() {
		return taxId;
	}
	public void setTaxId(String taxId) {
		this.taxId = taxId;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public boolean isLiftGate() {
		return liftGate;
	}

	public void setLiftGate(boolean liftGate) {
		this.liftGate = liftGate;
	}

	
}
