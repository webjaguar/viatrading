package com.webjaguar.model.api;


import java.util.ArrayList;
import java.util.List;

import org.codehaus.jackson.map.annotate.JsonSerialize;

import com.webjaguar.model.Promo;


@JsonSerialize()
public class PromosApi {
	
	private List<ApiPromo> dynamicPromoList = new ArrayList<ApiPromo>();

	public List<ApiPromo> getDynamicPromoList() {
		return dynamicPromoList;
	}

	public void setDynamicPromoList(List<ApiPromo> dynamicPromoList) {
		this.dynamicPromoList = dynamicPromoList;
	}

	
}
