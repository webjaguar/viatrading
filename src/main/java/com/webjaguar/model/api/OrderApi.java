package com.webjaguar.model.api;

import java.io.Serializable;


import org.codehaus.jackson.map.annotate.JsonSerialize;
import org.springframework.web.servlet.support.RequestContextUtils;

import java.util.List;

@JsonSerialize()

public class OrderApi implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String promoTitle;
	private String type;
	private String Email;
	private Integer custId;
	private List<LineItemApi> Items;
	private Double promoAmt;
	private String poNumber;
	private Boolean nc;
	private Double taxRate;
	private Double taxAmt;
	private Double orderSubTotal;
	private Double orderGrandTotal;
	private String payMethod;
	private Double ccFee;
	private Integer orderId;

	private String shipMsg;
	private String shipMethod;
	private Double shipCost;
	private String Note;
	private boolean backendOrder;
	private String tckcode;
	private Integer qualifier;
	private Integer srId;
	private Integer srProcessedById;
	private Double creditUsed;
	private AddressApi billingAddress;
	private AddressApi shippingAdress;
	private String agreedPaymentDate;
	private String agreedPickupDate;


	
	public Integer getOrderId() {
		return orderId;
	}

	public void setOrderId(Integer orderId) {
		this.orderId = orderId;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getEmail() {
		return Email;
	}

	public void setEmail(String email) {
		this.Email = email;
	}

	public Integer getCustId() {
		return custId;
	}

	public void setCustId(Integer custId) {
		this.custId = custId;
	}

	public List<LineItemApi> getItems() {
		return Items;
	}

	public void setItems(List<LineItemApi> items) {
		Items = items;
	}


	public Double getPromoAmt() {
		return promoAmt;
	}

	public void setPromoAmt(Double promoAmt) {
		this.promoAmt = promoAmt;
	}

	public String getPoNumber() {
		return poNumber;
	}

	public void setPoNumber(String poNumber) {
		this.poNumber = poNumber;
	}

	public Boolean getNc() {
		return nc;
	}

	public void setNc(Boolean nc) {
		this.nc = nc;
	}

	public Double getTaxRate() {
		return taxRate;
	}

	public void setTaxRate(Double taxRate) {
		this.taxRate = taxRate;
	}

	public Double getTaxAmt() {
		return taxAmt;
	}

	public void setTaxAmt(Double taxAmt) {
		this.taxAmt = taxAmt;
	}

	public Double getOrderSubTotal() {
		return orderSubTotal;
	}

	public void setOrderSubTotal(Double orderSubTotal) {
		this.orderSubTotal = orderSubTotal;
	}

	public Double getOrderGrandTotal() {
		return orderGrandTotal;
	}

	public void setOrderGrandTotal(Double orderGrandTotal) {
		this.orderGrandTotal = orderGrandTotal;
	}

	public String getPayMethod() {
		return payMethod;
	}

	public void setPayMethod(String payMethod) {
		this.payMethod = payMethod;
	}

	public String getShipMsg() {
		return shipMsg;
	}

	public void setShipMsg(String shipMsg) {
		this.shipMsg = shipMsg;
	}

	public String getShipMethod() {
		return shipMethod;
	}

	public void setShipMethod(String shipMethod) {
		this.shipMethod = shipMethod;
	}

	public Double getShipCost() {
		return shipCost;
	}

	public void setShipCost(Double shipCost) {
		this.shipCost = shipCost;
	}

	public String getNote() {
		return Note;
	}

	public void setNote(String note) {
		Note = note;
	}

	public boolean isBackendOrder() {
		return backendOrder;
	}

	public void setBackendOrder(boolean backendOrder) {
		this.backendOrder = backendOrder;
	}

	public String getTckcode() {
		return tckcode;
	}

	public void setTckcode(String tckcode) {
		this.tckcode = tckcode;
	}

	public Integer getQualifier() {
		return qualifier;
	}

	public void setQualifier(Integer qualifier) {
		this.qualifier = qualifier;
	}

	public Integer getSrId() {
		return srId;
	}

	public void setSrId(Integer srId) {
		this.srId = srId;
	}

	public Double getCreditUsed() {
		return creditUsed;
	}

	public void setCreditUsed(Double creditUsed) {
		this.creditUsed = creditUsed;
	}

	public AddressApi getBillingAddress() {
		return billingAddress;
	}

	public void setBillingAddress(AddressApi billingAddress) {
		this.billingAddress = billingAddress;
	}

	public AddressApi getShippingAdress() {
		return shippingAdress;
	}

	public void setShippingAdress(AddressApi shippingAdress) {
		this.shippingAdress = shippingAdress;
	}

	public String getPromoTitle() {
		return promoTitle;
	}

	public void setPromoTitle(String promoTitle) {
		this.promoTitle = promoTitle;
	}

	public Double getCcFee() {
		return ccFee;
	}

	public void setCcFee(Double ccFee) {
		this.ccFee = ccFee;
	}

	public Integer getSrProcessedById() {
		return srProcessedById;
	}

	public void setSrProcessedById(Integer srProcessedById) {
		this.srProcessedById = srProcessedById;
	}

	public String getAgreedPaymentDate() {
		return agreedPaymentDate;
	}

	public void setAgreedPaymentDate(String agreedPaymentDate) {
		this.agreedPaymentDate = agreedPaymentDate;
	}

	public String getAgreedPickupDate() {
		return agreedPickupDate;
	}

	public void setAgreedPickupDate(String agreedPickupDate) {
		this.agreedPickupDate = agreedPickupDate;
	}	
    

}