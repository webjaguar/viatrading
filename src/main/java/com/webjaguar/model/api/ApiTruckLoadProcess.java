package com.webjaguar.model.api;

import java.util.Date;
import java.util.List;

import org.codehaus.jackson.map.annotate.JsonSerialize;
import org.codehaus.jackson.map.annotate.JsonSerialize.Inclusion;
import com.webjaguar.model.Product;

@JsonSerialize(include=Inclusion.NON_NULL)

public class ApiTruckLoadProcess {
	private String name;
	private List<ApiProduct> originalProduct;
	private List<ApiTruckLoadProcessProduct> derivedProduct;
	private String custom1;
	private String custom2;
	private String custom3;
	private String custom4;
	private String pt;
	private String status;
	private Integer createdBy;
	private String type;
	private Double teamHours;
	private Double teamCost;
	private Integer numOfTeamMembers;
	private String dateStarted;
	private String dateEnded;

	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public List<ApiProduct> getOriginalProduct() {
		return originalProduct;
	}
	public void setOriginalProduct(List<ApiProduct> originalProduct) {
		this.originalProduct = originalProduct;
	}
	public List<ApiTruckLoadProcessProduct> getDerivedProduct() {
		return derivedProduct;
	}
	public void setDerivedProduct(List<ApiTruckLoadProcessProduct> derivedProduct) {
		this.derivedProduct = derivedProduct;
	}
	public String getCustom1() {
		return custom1;
	}
	public void setCustom1(String custom1) {
		this.custom1 = custom1;
	}
	public String getCustom2() {
		return custom2;
	}
	public void setCustom2(String custom2) {
		this.custom2 = custom2;
	}
	public String getCustom3() {
		return custom3;
	}
	public void setCustom3(String custom3) {
		this.custom3 = custom3;
	}
	public String getCustom4() {
		return custom4;
	}
	public void setCustom4(String custom4) {
		this.custom4 = custom4;
	}
	public String getPt() {
		return pt;
	}
	public void setPt(String pt) {
		this.pt = pt;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public Integer getCreatedBy() {
		return createdBy;
	}
	public void setCreatedBy(Integer createdBy) {
		this.createdBy = createdBy;
	}
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	public Double getTeamHours() {
		return teamHours;
	}
	public void setTeamHours(Double teamHours) {
		this.teamHours = teamHours;
	}
	public Double getTeamCost() {
		return teamCost;
	}
	public void setTeamCost(Double teamCost) {
		this.teamCost = teamCost;
	}
	public Integer getNumOfTeamMembers() {
		return numOfTeamMembers;
	}
	public void setNumOfTeamMembers(Integer numOfTeamMembers) {
		this.numOfTeamMembers = numOfTeamMembers;
	}
	public String getDateStarted() {
		return dateStarted;
	}
	public void setDateStarted(String dateStarted) {
		this.dateStarted = dateStarted;
	}
	public String getDateEnded() {
		return dateEnded;
	}
	public void setDateEnded(String dateEnded) {
		this.dateEnded = dateEnded;
	}
}
