package com.webjaguar.model.api;


import java.util.Date;
import java.util.HashSet;
import java.util.Set;

import org.codehaus.jackson.map.annotate.JsonSerialize;
import org.codehaus.jackson.map.annotate.JsonSerialize.Inclusion;


@JsonSerialize(include=Inclusion.NON_NULL)
public class ApiPromo {
	
	private int userId;
	private int index;
	private double discount = -1;
	private Double minOrder = 0.0;
	private Date startDate;
	private Date endDate;
	private Date createdOn;
	private String createdBy;
	private String discountType = "order";
	private String prefix;
	private String skus;
	private Integer minQty;
	private String parentSkus;
	private Set<Object> parentSkuSet = new HashSet<Object>();
	private String groups;
	private String groupNote;
	private boolean onetimeUseOnly = false;
	private boolean onetimeUsePerCustomer = false;
	private boolean promoForNewCustomersOnly = false;
	private boolean percent = true;
	private String groupIds;
	private String groupType;
	private String states;
	private String stateType = "include";
	private int minimumQuantity;
	private String brands;
	private Double minimumOrderPerBrands = -1.0;
	private Set<Object> brandSet = new HashSet<Object>();
	
	

	public ApiPromo(){}

	public int getUserId() {
		return userId;
	}
	public void setUserId(int userId) {
		this.userId = userId;
	}
	public double getDiscount() {
		return discount;
	}
	public void setDiscount(int discount) {
		this.discount = discount;
	}

	public Double getMinOrder() {
		return minOrder;
	}

	public void setMinOrder(Double minOrder) {
		this.minOrder = minOrder;
	}

	public Date getStartDate() {
		return startDate;
	}

	public void setStartDate(Date startDate) {
		this.startDate = startDate;
	}

	public Date getEndDate() {
		return endDate;
	}

	public void setEndDate(Date endDate) {
		this.endDate = endDate;
	}

	public String getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	public Date getCreatedOn() {
		return createdOn;
	}

	public void setCreatedOn(Date createdOn) {
		this.createdOn = createdOn;
	}

	public String getDiscountType() {
		return discountType;
	}

	public void setDiscountType(String discountType) {
		this.discountType = discountType;
	}

	public String getPrefix() {
		return prefix;
	}

	public void setPrefix(String prefix) {
		this.prefix = prefix;
	}
	
	public boolean isOnetimeUseOnly() {
		return onetimeUseOnly;
	}

	public void setOnetimeUseOnly(boolean onetimeUseOnly) {
		this.onetimeUseOnly = onetimeUseOnly;
	}

	public boolean isOnetimeUsePerCustomer() {
		return onetimeUsePerCustomer;
	}

	public void setOnetimeUsePerCustomer(boolean onetimeUsePerCustomer) {
		this.onetimeUsePerCustomer = onetimeUsePerCustomer;
	}

	public boolean isPercent() {
		return percent;
	}

	public void setPercent(boolean percent) {
		this.percent = percent;
	}


	public int getMinimumQuantity() {
		return minimumQuantity;
	}

	public void setMinimumQuantity(int minimumQuantity) {
		this.minimumQuantity = minimumQuantity;
	}

	public int getIndex() {
		return index;
	}

	public void setIndex(int index) {
		this.index = index;
	}

	public String getBrands() {
		return brands;
	}

	public void setBrands(String brands) {
		this.brands = brands;
	}

	public double getMinimumOrderPerBrands() {
		return minimumOrderPerBrands;
	}

	public void setMinimumOrderPerBrands(double minimumOrderPerBrands) {
		this.minimumOrderPerBrands = minimumOrderPerBrands;
	}

	public String getGroupIds() {
		return groupIds;
	}

	public void setGroupIds(String groupIds) {
		this.groupIds = groupIds;
	}

	public String getGroupType() {
		return groupType;
	}

	public void setGroupType(String groupType) {
		this.groupType = groupType;
	}

	public String getStates() {
		return states;
	}

	public void setStates(String states) {
		this.states = states;
	}

	public String getStateType() {
		return stateType;
	}

	public void setStateType(String stateType) {
		this.stateType = stateType;
	}

	public String getGroups() {
		return groups;
	}

	public void setGroups(String groups) {
		this.groups = groups;
	}

	public String getGroupNote() {
		return groupNote;
	}

	public void setGroupNote(String groupNote) {
		this.groupNote = groupNote;
	}

	public boolean isPromoForNewCustomersOnly() {
		return promoForNewCustomersOnly;
	}

	public void setPromoForNewCustomersOnly(boolean promoForNewCustomersOnly) {
		this.promoForNewCustomersOnly = promoForNewCustomersOnly;
	}

	public String getSkus() {
		return skus;
	}

	public void setSkus(String skus) {
		this.skus = skus;
	}

	public Integer getMinQty() {
		return minQty;
	}

	public void setMinQty(Integer minQty) {
		this.minQty = minQty;
	}

	public Set<Object> getBrandSet() {
		return brandSet;
	}

	public void setBrandSet(Set<Object> brandSet) {
		this.brandSet = brandSet;
	}

	public String getParentSkus() {
		return parentSkus;
	}

	public void setParentSkus(String parentSkus) {
		this.parentSkus = parentSkus;
	}

	public Set<Object> getParentSkuSet() {
		return parentSkuSet;
	}

	public void setParentSkuSet(Set<Object> parentSkuSet) {
		this.parentSkuSet = parentSkuSet;
	}


	
}
