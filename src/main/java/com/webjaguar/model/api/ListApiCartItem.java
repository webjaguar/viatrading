package com.webjaguar.model.api;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import org.codehaus.jackson.map.annotate.JsonSerialize;
import org.codehaus.jackson.map.annotate.JsonSerialize.Inclusion;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonSerialize(include=Inclusion.NON_NULL)
public class ListApiCartItem implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private List<ApiCartItem> listApiCartItem = new ArrayList<ApiCartItem>();
	
	public List<ApiCartItem> getListApiCartItem() {
		return listApiCartItem;
	}
	public void setListApiCartItem(List<ApiCartItem> listApiCartItem) {
		this.listApiCartItem = listApiCartItem;
	}



}
