package com.webjaguar.model.api;

import java.io.Serializable;

import org.codehaus.jackson.map.annotate.JsonSerialize;
import org.codehaus.jackson.map.annotate.JsonSerialize.Inclusion;

import com.webjaguar.model.Product;



@JsonSerialize(include=Inclusion.NON_NULL)
public class ApiCartItem implements Serializable {
	
	/**
	 * 
	 */
	public ApiCartItem() {};
	
	private static final long serialVersionUID = 1L;

	private Product product;
	private int quantity;
	private Integer priceCasePackQty;
	private Double originalPrice;
	private Double discount;
	private boolean percent = true;
	private Double lineItemTotal;
	
	
	public int getTotalQty() {
		if (product.getCaseContent() != null && this.priceCasePackQty == null)
			return getCaseContentQty() * quantity;
		else if (this.priceCasePackQty != null) 
			return  getCasePackQty() * quantity;
		else
			return  quantity;
	}
	
	public Double getTotalPrice() {
		if ( originalPrice == null )
			return null;
		Double total = new Double( getDiscountedUnitPrice() * getTotalQty() );
//		if(this.getAsiAdditionalCharge() != null) {
//			total = total + this.getAsiAdditionalCharge();
//		}
//		if(this.getOptionFixedCharge() != null) {
//			total = total + this.getOptionFixedCharge();
//		}
		return total;
	}
	
	public int getCaseContentQty() {
		if (product.getCaseContent() != null) {
			return product.getCaseContent();
		} else {
			return 1;
		}
	}
	public int getCasePackQty() {
		if (this.priceCasePackQty != null) {
			return this.priceCasePackQty;
		} else {
			return 1;
		}
	}
	
	public Double getDiscountedUnitPrice() {
		if (this.originalPrice == null) { return null; }
		double discountAmount = 0.0;
		
		if ( this.discount != null ) {
			if ( this.percent ) {
				discountAmount = this.originalPrice * (this.discount / 100.00);
				discountAmount = this.originalPrice - discountAmount;
			}
			else {
				discountAmount = this.originalPrice - this.discount;
			}
		} else {
			discountAmount = this.originalPrice;
		}
		return discountAmount;
	}

	public Product getProduct() {
		return product;
	}

	public void setProduct(Product product) {
		this.product = product;
	}

	public int getQuantity() {
		return quantity;
	}

	public void setQuantity(int quantity) {
		this.quantity = quantity;
	}

	public Double getOriginalPrice() {
		return originalPrice;
	}

	public void setOriginalPrice(Double originalPrice) {
		this.originalPrice = originalPrice;
	}

	public Double getDiscount() {
		return discount;
	}

	public void setDiscount(Double discount) {
		this.discount = discount;
	}

	public boolean isPercent() {
		return percent;
	}

	public void setPercent(boolean percent) {
		this.percent = percent;
	}

	public Double getLineItemTotal() {
		return lineItemTotal;
	}

	public void setLineItemTotal(Double lineItemTotal) {
		this.lineItemTotal = lineItemTotal;
	}

	public Integer getPriceCasePackQty() {
		return priceCasePackQty;
	}

	public void setPriceCasePackQty(Integer priceCasePackQty) {
		this.priceCasePackQty = priceCasePackQty;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}
	
	
}
