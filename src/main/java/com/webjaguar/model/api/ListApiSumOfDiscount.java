package com.webjaguar.model.api;

import java.util.ArrayList;
import java.util.List;

import org.codehaus.jackson.map.annotate.JsonSerialize;

@JsonSerialize()
public class ListApiSumOfDiscount {
	private List<ApiSumOfDiscount> listApiSumOfDiscount = new ArrayList<ApiSumOfDiscount>();

	public List<ApiSumOfDiscount> getListApiSumOfDiscount() {
		return listApiSumOfDiscount;
	}

	public void setListApiSumOfDiscount(List<ApiSumOfDiscount> listApiSumOfDiscount) {
		this.listApiSumOfDiscount = listApiSumOfDiscount;
	}
}
