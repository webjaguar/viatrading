package com.webjaguar.model.api;

import java.io.File;
import java.util.Date;
import java.util.List;

import org.codehaus.jackson.map.annotate.JsonSerialize;
import org.codehaus.jackson.map.annotate.JsonSerialize.Inclusion;

import com.webjaguar.model.DialingNote;
import com.webjaguar.model.crm.CrmContact;
import com.webjaguar.model.crm.CrmContactField;

@JsonSerialize(include=Inclusion.NON_NULL)
public class ApiCrmContact {
	public ApiCrmContact() {}
	public ApiCrmContact(CrmContact wjCrmContact) {
		this.setId(wjCrmContact.getId());
		this.accountId = wjCrmContact.getAccountId();
		this.accountName = wjCrmContact.getAccountName();
		this.accountId = wjCrmContact.getAccountId();
		this.accessUserId = wjCrmContact.getAccessUserId();
		this.contactType = wjCrmContact.getContactType();
		this.createdBy = wjCrmContact.getCreatedBy();
		this.created = wjCrmContact.getCreated();
		this.rating  = wjCrmContact.getRating();
		this.lastModified = wjCrmContact.getLastModified();
		this.leadSource = wjCrmContact.getLeadSource();
		this.title = wjCrmContact.getTitle();
		this.description = wjCrmContact.getDescription();
		this.shortDesc = wjCrmContact.getShortDesc();
		this.department = wjCrmContact.getDepartment();
		this.email1 = wjCrmContact.getEmail1();
		this.email2 = wjCrmContact.getEmail2();
		this.website = wjCrmContact.getWebsite();
		this.bounced = wjCrmContact.isBounced();
		this.unsubscribe = wjCrmContact.isUnsubscribe();
		this.phone1 = wjCrmContact.getPhone1();
		this.phone2 = wjCrmContact.getPhone2();
		this.fax = wjCrmContact.getFax();
		this.street = wjCrmContact.getStreet();
		this.city = wjCrmContact.getCity();
		this.stateProvince = wjCrmContact.getStateProvince();
		this.zip = wjCrmContact.getZip();
		this.country = wjCrmContact.getCountry();
		this.addressType = wjCrmContact.getAddressType();
		this.otherAddressType = wjCrmContact.getOtherAddressType();
		this.otherStreet = wjCrmContact.getOtherStreet();
		this.otherStateProvince = wjCrmContact.getOtherStateProvince();
		this.otherZip = wjCrmContact.getOtherZip();
		this.otherCountry = wjCrmContact.getOtherCountry();
		this.verified = wjCrmContact.isVerified();
		this.trackcode = wjCrmContact.getTrackcode();
		this.userId = wjCrmContact.getUserId();
	}
	
	private Integer id;
	private Integer accountId;
	private String accountName;
	private String contactType;
	private String rating;
	private String firstName;
	private String lastName;
	private String contactName;
	private String createdBy;
	private Date created;
	private Date lastModified;
	private String leadSource;
	private String title;
	private String shortDesc;
	private String description;
	private String department;
	private String email1;
	private String email2;
	private String website;
	private boolean bounced;
	private boolean unsubscribe;
	private String phone1;
	private String phone2;
	private String fax;
	private String street;
	private String city;
	private String stateProvince;
	private String zip;
	private String country;
	private String addressType;
	private String otherStreet;
	private String otherCity;
	private String otherStateProvince;
	private String otherZip;
	private String otherCountry;
	private String otherAddressType;
	private String ipAddress;
	private boolean verified = true;
	private List<CrmContactFieldApi> crmContactFields;
//	private String crmContactField2;
//	private String crmContactField8;
//	private String crmContactField14;
//	private String crmContactField15;
//	private String crmContactField26;
	private String token;
	private Date convertedOn;
	private Integer userId;
	private Integer accessUserId;
	private String trackcode;
	private String rating1;
//	private String rating2;
//	private boolean doNotCall;
	// Prospect Id (SearchEngine Prospect)
//	private String prospectId;
//	private List<String> attachmentList;
//	private File attachment;
//	private Integer qualifier;
	private String language;
//	private Integer duplicateContact;
	
	//Dialing note
//	private DialingNote newDialingNote;
//	private List<DialingNote> dialingNoteHistory;
//	private String dialingNoteHistoryString;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Integer getAccountId() {
		return accountId;
	}

	public void setAccountId(Integer accountId) {
		this.accountId = accountId;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getContactName() {
		return contactName;
	}

	public void setContactName(String contactName) {
		this.contactName = contactName;
	}

	public String getAccountName() {
		return accountName;
	}

	public void setAccountName(String accountName) {
		this.accountName = accountName;
	}

	public String getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	public Date getCreated() {
		return created;
	}

	public void setCreated(Date created) {
		this.created = created;
	}

	public Date getLastModified() {
		return lastModified;
	}

	public void setLastModified(Date lastModified) {
		this.lastModified = lastModified;
	}

	public String getLeadSource() {
		return leadSource;
	}

	public void setLeadSource(String leadSource) {
		this.leadSource = leadSource;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getShortDesc() {
		return shortDesc;
	}

	public void setShortDesc(String shortDesc) {
		this.shortDesc = shortDesc;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getDescriptionTrimmed() {
		if (this.description != null && this.description.length() > 100)
			return this.description.substring(0, 100).concat("...");
		else
			return this.description;
	}

	public String getDepartment() {
		return department;
	}

	public void setDepartment(String department) {
		this.department = department;
	}

	public String getEmail1() {
		return email1;
	}

	public void setEmail1(String email1) {
		this.email1 = email1;
	}

	public String getEmail2() {
		return email2;
	}

	public void setEmail2(String email2) {
		this.email2 = email2;
	}

	public boolean isBounced() {
		return bounced;
	}

	public void setBounced(boolean bounced) {
		this.bounced = bounced;
	}

	public boolean isUnsubscribe() {
		return unsubscribe;
	}

	public void setUnsubscribe(boolean unsubscribe) {
		this.unsubscribe = unsubscribe;
	}

	public String getPhone1() {
		return phone1;
	}

	public void setPhone1(String phone1) {
		this.phone1 = phone1;
	}

	public String getPhone2() {
		return phone2;
	}

	public void setPhone2(String phone2) {
		this.phone2 = phone2;
	}

	public String getFax() {
		return fax;
	}

	public void setFax(String fax) {
		this.fax = fax;
	}

	public String getStreet() {
		return street;
	}

	public void setStreet(String street) {
		this.street = street;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public String getStateProvince() {
		return stateProvince;
	}

	public void setStateProvince(String stateProvince) {
		this.stateProvince = stateProvince;
	}

	public String getZip() {
		return zip;
	}

	public void setZip(String zip) {
		this.zip = zip;
	}

	public String getCountry() {
		return country;
	}

	public void setCountry(String country) {
		this.country = country;
	}

	public String getOtherStreet() {
		return otherStreet;
	}

	public void setOtherStreet(String otherStreet) {
		this.otherStreet = otherStreet;
	}

	public String getOtherCity() {
		return otherCity;
	}

	public void setOtherCity(String otherCity) {
		this.otherCity = otherCity;
	}

	public String getOtherStateProvince() {
		return otherStateProvince;
	}

	public void setOtherStateProvince(String otherStateProvince) {
		this.otherStateProvince = otherStateProvince;
	}

	public String getOtherZip() {
		return otherZip;
	}

	public void setOtherZip(String otherZip) {
		this.otherZip = otherZip;
	}

	public String getOtherCountry() {
		return otherCountry;
	}

	public void setOtherCountry(String otherCountry) {
		this.otherCountry = otherCountry;
	}

//	public void setCrmContactFields(List<CrmContactField> crmContactFields) {
//		this.crmContactFields = crmContactFields;
//	}
//
//	public List<CrmContactField> getCrmContactFields() {
//		return crmContactFields;
//	}

	public void setVerified(boolean verified) {
		this.verified = verified;
	}

	public boolean isVerified() {
		return verified;
	}

	public void setToken(String token) {
		this.token = token;
	}

	public String getToken() {
		return token;
	}

	public void setConvertedOn(Date convertedOn) {
		this.convertedOn = convertedOn;
	}

	public Date getConvertedOn() {
		return convertedOn;
	}

	public void setUserId(Integer userId) {
		this.userId = userId;
	}

	public Integer getUserId() {
		return userId;
	}

	public void setIpAddress(String ipAddress) {
		this.ipAddress = ipAddress;
	}

	public String getIpAddress() {
		return ipAddress;
	}

//	public void setTrackcode(String trackcode) {
//		this.trackcode = trackcode;
//	}
//
//	public String getTrackcode() {
//		return trackcode;
//	}

	public void setAccessUserId(Integer accessUserId) {
		this.accessUserId = accessUserId;
	}

	public Integer getAccessUserId() {
		return accessUserId;
	}

	public void setAddressType(String addressType) {
		this.addressType = addressType;
	}

	public String getAddressType() {
		return addressType;
	}

	public void setOtherAddressType(String otherAddressType) {
		this.otherAddressType = otherAddressType;
	}

	public String getOtherAddressType() {
		return otherAddressType;
	}

	public void setContactType(String contactType) {
		this.contactType = contactType;
	}

	public String getContactType() {
		return contactType;
	}

	public void setRating(String rating) {
		this.rating = rating;
	}

	public String getRating() {
		return rating;
	}

	public void setWebsite(String website) {
		this.website = website;
	}

	public String getWebsite() {
		return website;
	}

//	/**
//	 * @return the prospectId
//	 */
//	public String getProspectId() {
//		return prospectId;
//	}
//
//	/**
//	 * @param prospectId
//	 *        the prospectId to set
//	 */
//	public void setProspectId(String prospectId) {
//		this.prospectId = prospectId;
//	}
//
//	public List<String> getAttachmentList() {
//		return attachmentList;
//	}
//
//	public void setAttachmentList(List<String> attachmentList) {
//		this.attachmentList = attachmentList;
//	}
//
//	public File getAttachment() {
//		return attachment;
//	}
//
//	public void setAttachment(File attachment) {
//		this.attachment = attachment;
//	}

//	public String getCrmContactField2() {
//		return crmContactField2;
//	}
//
//	public void setCrmContactField2(String crmContactField2) {
//		this.crmContactField2 = crmContactField2;
//	}
//
//	public String getCrmContactField8() {
//		return crmContactField8;
//	}
//
//	public void setCrmContactField8(String crmContactField8) {
//		this.crmContactField8 = crmContactField8;
//	}
//
//	public String getCrmContactField14() {
//		return crmContactField14;
//	}
//
//	public void setCrmContactField14(String crmContactField14) {
//		this.crmContactField14 = crmContactField14;
//	}
//
//	public String getCrmContactField15() {
//		return crmContactField15;
//	}
//
//	public void setCrmContactField15(String crmContactField15) {
//		this.crmContactField15 = crmContactField15;
//	}
//
//	public String getCrmContactField26() {
//		return crmContactField26;
//	}
//
//	public void setCrmContactField26(String crmContactField26) {
//		this.crmContactField26 = crmContactField26;
//	}

	public String getRating1() {
		return rating1;
	}

	public void setRating1(String rating1) {
		this.rating1 = rating1;
	}
	public String getTrackcode() {
		return trackcode;
	}
	public void setTrackcode(String trackcode) {
		this.trackcode = trackcode;
	}
	public List<CrmContactFieldApi> getCrmContactFields() {
		return crmContactFields;
	}
	public void setCrmContactFields(List<CrmContactFieldApi> crmContactFields) {
		this.crmContactFields = crmContactFields;
	}

//	public String getRating2() {
//		return rating2;
//	}
//
//	public void setRating2(String rating2) {
//		this.rating2 = rating2;
//	}
//
//	public boolean isDoNotCall() {
//		return doNotCall;
//	}
//
//	public void setDoNotCall(boolean doNotCall) {
//		this.doNotCall = doNotCall;
//	}

//	public List<DialingNote> getDialingNoteHistory() {
//		return dialingNoteHistory;
//	}
//
//	public void setDialingNoteHistory(List<DialingNote> dialingNoteHistory) {
//		this.dialingNoteHistory = dialingNoteHistory;
//	}
//
//	public DialingNote getNewDialingNote() {
//		return newDialingNote;
//	}
//
//	public void setNewDialingNote(DialingNote newDialingNote) {
//		this.newDialingNote = newDialingNote;
//	}
//
//	public String getDialingNoteHistoryString() {
//		return dialingNoteHistoryString;
//	}
//
//	public void setDialingNoteHistoryString(String dialingNoteHistoryString) {
//		this.dialingNoteHistoryString = dialingNoteHistoryString;
//	}
//
//	public Integer getQualifier() {
//		return qualifier;
//	}
//
//	public void setQualifier(Integer qualifier) {
//		this.qualifier = qualifier;
//	}
//
	public String getLanguage() {
		return language;
	}

	public void setLanguage(String language) {
		this.language = language;
	}
//
//	public Integer getDuplicateContact() {
//		return duplicateContact;
//	}
//
//	public void setDuplicateContact(Integer duplicateContact) {
//		this.duplicateContact = duplicateContact;
//	}

	
}
