package com.webjaguar.model.api;

public class OrderprodAttrApi {
	private String optionName;
	private String valueName;
	private Double optionPrice; 
	public String getOptionName() {
		return optionName;
	}
	public void setOptionName(String optionName) {
		this.optionName = optionName;
	}
	public String getValueName() {
		return valueName;
	}
	public void setValueName(String valueName) {
		this.valueName = valueName;
	}
	public Double getOptionPrice() {
		return optionPrice;
	}
	public void setOptionPrice(Double optionPrice) {
		this.optionPrice = optionPrice;
	}
	
	
}
