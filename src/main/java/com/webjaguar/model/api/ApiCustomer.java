/*
 * Copyright 2005, 2006 Advanced E-Media Solutions
 * 
 */

package com.webjaguar.model.api;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.*;

import org.codehaus.jackson.annotate.JsonIgnore;
import org.codehaus.jackson.annotate.JsonProperty;
import org.codehaus.jackson.map.annotate.JsonSerialize;
import org.codehaus.jackson.map.annotate.JsonSerialize.Inclusion;

import com.webjaguar.model.Address;
import com.webjaguar.model.Customer;
import com.webjaguar.model.Language.LanguageCode;


@JsonSerialize(include=Inclusion.NON_NULL)
public class ApiCustomer implements Serializable{
	/**
	 * 
	 */
	public ApiCustomer() {}
	public ApiCustomer(Customer wjCustomer) {
		this.setId(wjCustomer.getId());
		this.username = wjCustomer.getUsername();
		this.unsubscribe = wjCustomer.isUnsubscribe();
		this.extraEmail = wjCustomer.getExtraEmail();
		this.host = wjCustomer.getHost();
		this.credit = wjCustomer.getCredit();
		//this.address = wjCustomer.getAddress();
		this.setApiAddress(new ApiAddress(wjCustomer.getAddress()));

		this.lastLogin = wjCustomer.getLastLogin();
		this.lastOrderDate = wjCustomer.getLastOrderDate();
		this.payment = wjCustomer.getPayment();
		this.note = wjCustomer.getNote();
//		this.accountNumberPrefix = wjCustomer.getAccountNumberPrefix();
		this.accountNumber = wjCustomer.getAccountNumber();
		this.websiteUrl = wjCustomer.getWebsiteUrl();
		this.textMessageNotify = wjCustomer.isTextMessageNotify();
		this.whatsappNotify = wjCustomer.isWhatsappNotify();
//		this.qbTaxCode = wjCustomer.getQbTaxCode();
//		this.guest = wjCustomer.isGuest();
	}
	
	private ApiAddress apiAddress;
	private static final long serialVersionUID = 1L;
	private Integer id;
	private String username;
	private Boolean unsubscribe;
	private String extraEmail;
	private String host;
	private Double credit;
	//private Address address;
	private Timestamp lastLogin;
	private Date lastOrderDate;
	private String payment;
	private String note;
	private String accountNumberPrefix; // account prefix
	private String accountNumber; // not unique
	private String websiteUrl;
	
	
	private Boolean textMessageNotify;
	private Boolean whatsappNotify;
	private String qbTaxCode;
	private Boolean guest;
	//new for Insert API
	private String password;
	
	// language
	
	private LanguageCode language;
	private String registeredBy;

	private String trackcode;
	private String taxId;
	private Integer salesRepId;
	
	private String field1;
	private String field2;
	private String field3;
	private String field4;
	private String field5;
	private String field6;
	private String field7;
	private String field8;
	private String field9;
	private String field10;
	private String field11;
	private String field12;
	private String field13;
	private String field14;
	private String field15;
	private String field16;
	private String field17;
	private String field18;
	private String field19;
	private String field20;
	private String field21;
	private String field22;
	private String field23;
	private String field24;
	private String field25;
	private String field26;
	private String field27;
	private String field28;
	private String field29;
	private String field30;
	
	// ratings
	private String rating1;
	private String rating2;
	
	private String languageField;
	private String ebayName;
	private String amazonName;
	private String unsubscribeReason;
	private Boolean unsubscribeTextMessage;
	
	private Boolean emailNotify;

	private List<ApiAddress> apiAddressList;

	public String getUsername() {
		return username;
	}
	public void setUsername(String username) {
		this.username = username;
	}
	public Boolean isUnsubscribe() {
		return unsubscribe;
	}
	public void setUnsubscribe(Boolean unsubscribe) {
		this.unsubscribe = unsubscribe;
	}
	public String getExtraEmail() {
		return extraEmail;
	}
	public void setExtraEmail(String extraEmail) {
		this.extraEmail = extraEmail;
	}
	public String getHost() {
		return host;
	}
	public void setHost(String host) {
		this.host = host;
	}
	public Double getCredit() {
		return credit;
	}
	public void setCredit(Double credit) {
		this.credit = credit;
	}
/*	public Address getAddress() {
		return address;
	}
	public void setAddress(Address address) {
		this.address = address;
	}*/
	public Timestamp getLastLogin() {
		return lastLogin;
	}
	public void setLastLogin(Timestamp lastLogin) {
		this.lastLogin = lastLogin;
	}
	public Date getLastOrderDate() {
		return lastOrderDate;
	}
	public void setLastOrderDate(Date lastOrderDate) {
		this.lastOrderDate = lastOrderDate;
	}
	public String getPayment() {
		return payment;
	}
	public void setPayment(String payment) {
		this.payment = payment;
	}
	public String getNote() {
		return note;
	}
	public void setNote(String note) {
		this.note = note;
	}
	public String getAccountNumberPrefix() {
		return accountNumberPrefix;
	}
	public void setAccountNumberPrefix(String accountNumberPrefix) {
		this.accountNumberPrefix = accountNumberPrefix;
	}
	public String getAccountNumber() {
		return accountNumber;
	}
	public void setAccountNumber(String accountNumber) {
		this.accountNumber = accountNumber;
	}
	public String getWebsiteUrl() {
		return websiteUrl;
	}
	public void setWebsiteUrl(String websiteUrl) {
		this.websiteUrl = websiteUrl;
	}
	
	public Boolean isTextMessageNotify() {
		return textMessageNotify;
	}
	public void setTextMessageNotify(Boolean textMessageNotify) {
		this.textMessageNotify = textMessageNotify;
	}
	
	public Boolean getWhatsappNotify() {
		return whatsappNotify;
	}
	public void setWhatsappNotify(Boolean whatsappNotify) {
		this.whatsappNotify = whatsappNotify;
	}
	public String getQbTaxCode() {
		return qbTaxCode;
	}
	public void setQbTaxCode(String qbTaxCode) {
		this.qbTaxCode = qbTaxCode;
	}
	public Boolean isGuest() {
		return guest;
	}
	public void setGuest(Boolean guest) {
		this.guest = guest;
	}
	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	
	public ApiAddress getApiAddress() {
		return apiAddress;
	}
	public void setApiAddress(ApiAddress apiAddress) {
		this.apiAddress = apiAddress;

	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}

	public List<ApiAddress> getApiAddressList() {
		return apiAddressList;
	}
	public void setApiAddressList(List<ApiAddress> apiAddressList) {
		this.apiAddressList = apiAddressList;

	}

	public String getRegisteredBy() {
		return registeredBy;
	}

	public void setRegisteredBy(String registeredBy) {
		this.registeredBy = registeredBy;
	}

	public String getTrackcode() {
		return trackcode;
	}
	public void setTrackcode(String trackcode) {
		this.trackcode = trackcode;
	}
	public String getTaxId() {
		return taxId;
	}
	public void setTaxId(String taxId) {
		this.taxId = taxId;
	}
	public Integer getSalesRepId() {
		return salesRepId;
	}
	public void setSalesRepId(Integer salesRepId) {
		this.salesRepId = salesRepId;
	}
	public String getField1() {
		return field1;
	}
	public void setField1(String field1) {
		this.field1 = field1;
	}
	public String getField2() {
		return field2;
	}
	public void setField2(String field2) {
		this.field2 = field2;
	}
	public String getField4() {
		return field4;
	}
	public void setField4(String field4) {
		this.field4 = field4;
	}
	public String getField3() {
		return field3;
	}
	public void setField3(String field3) {
		this.field3 = field3;
	}
	public String getField5() {
		return field5;
	}
	public void setField5(String field5) {
		this.field5 = field5;
	}
	public String getField6() {
		return field6;
	}
	public void setField6(String field6) {
		this.field6 = field6;
	}
	public String getField7() {
		return field7;
	}
	public void setField7(String field7) {
		this.field7 = field7;
	}
	public String getField8() {
		return field8;
	}
	public void setField8(String field8) {
		this.field8 = field8;
	}
	public String getField9() {
		return field9;
	}
	public void setField9(String field9) {
		this.field9 = field9;
	}
	public String getField10() {
		return field10;
	}
	public void setField10(String field10) {
		this.field10 = field10;
	}
	public String getField11() {
		return field11;
	}
	public void setField11(String field11) {
		this.field11 = field11;
	}
	public String getField12() {
		return field12;
	}
	public void setField12(String field12) {
		this.field12 = field12;
	}
	public String getField13() {
		return field13;
	}
	public void setField13(String field13) {
		this.field13 = field13;
	}
	public String getField19() {
		return field19;
	}
	public void setField19(String field19) {
		this.field19 = field19;
	}
	public String getField20() {
		return field20;
	}
	public void setField20(String field20) {
		this.field20 = field20;
	}
	public String getField18() {
		return field18;
	}
	public void setField18(String field18) {
		this.field18 = field18;
	}
	public String getField17() {
		return field17;
	}
	public void setField17(String field17) {
		this.field17 = field17;
	}
	public String getField16() {
		return field16;
	}
	public void setField16(String field16) {
		this.field16 = field16;
	}
	public String getField15() {
		return field15;
	}
	public void setField15(String field15) {
		this.field15 = field15;
	}
	public String getField14() {
		return field14;
	}
	public void setField14(String field14) {
		this.field14 = field14;
	}
	
	
	public String getField21() {
		return field21;
	}
	public void setField21(String field21) {
		this.field21 = field21;
	}
	public String getField22() {
		return field22;
	}
	public void setField22(String field22) {
		this.field22 = field22;
	}
	public String getField23() {
		return field23;
	}
	public void setField23(String field23) {
		this.field23 = field23;
	}
	public String getField24() {
		return field24;
	}
	public void setField24(String field24) {
		this.field24 = field24;
	}
	public String getField25() {
		return field25;
	}
	public void setField25(String field25) {
		this.field25 = field25;
	}
	public String getField26() {
		return field26;
	}
	public void setField26(String field26) {
		this.field26 = field26;
	}
	public String getField27() {
		return field27;
	}
	public void setField27(String field27) {
		this.field27 = field27;
	}
	public String getField28() {
		return field28;
	}
	public void setField28(String field28) {
		this.field28 = field28;
	}
	public String getField29() {
		return field29;
	}
	public void setField29(String field29) {
		this.field29 = field29;
	}
	public String getField30() {
		return field30;
	}
	public void setField30(String field30) {
		this.field30 = field30;
	}
	public Boolean isEmailNotify() {
		return emailNotify;
	}
	public void setEmailNotify(Boolean emailNotify) {
		this.emailNotify = emailNotify;
	}
	public String getRating2() {
		return rating2;
	}
	public void setRating2(String rating2) {
		this.rating2 = rating2;
	}
	public String getRating1() {
		return rating1;
	}
	public void setRating1(String rating1) {
		this.rating1 = rating1;
	}
	public String getLanguageField() {
		return languageField;
	}
	public void setLanguageField(String language) {
		this.languageField = language;
	}
	public String getEbayName() {
		return ebayName;
	}
	public void setEbayName(String ebayName) {
		this.ebayName = ebayName;
	}
	public String getAmazonName() {
		return amazonName;
	}
	public void setAmazonName(String amazonName) {
		this.amazonName = amazonName;
	}
	public String getUnsubscribeReason() {
		return unsubscribeReason;
	}
	public void setUnsubscribeReason(String unsubscribeReason) {
		this.unsubscribeReason = unsubscribeReason;
	}
	public Boolean isUnsubscribeTextMessage() {
		return unsubscribeTextMessage;
	}
	public void setUnsubscribeTextMessage(Boolean unsubscribeTextMessage) {
		this.unsubscribeTextMessage = unsubscribeTextMessage;
	}
	public LanguageCode getLanguage() {
		return language;
	}
	public void setLanguage(LanguageCode language) {
		this.language = language;
	}
}
