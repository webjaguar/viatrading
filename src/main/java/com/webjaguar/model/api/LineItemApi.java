package com.webjaguar.model.api;

import java.io.Serializable;

import java.util.List;

public class LineItemApi implements Serializable {
	private static final long serialVersionUID = 1L;
    private String sku;
    private int qty;
	private Double originalPrice;
	private Double discount;
    private List<OrderprodAttrApi> OrderprodAttrApi;
    private String name;
    private String location;
    private boolean discountTypePercent=true;
	public String getSku() {
		return sku;
	}
	public void setSku(String sku) {
		this.sku = sku;
	}
	public int getQty() {
		return qty;
	}
	public void setQty(int qty) {
		this.qty = qty;
	}
	public Double getOriginalPrice() {
		return originalPrice;
	}
	public void setOriginalPrice(Double originalPrice) {
		this.originalPrice = originalPrice;
	}
	public Double getDiscount() {
		return discount;
	}
	public void setDiscount(Double discount) {
		this.discount = discount;
	}
	public static long getSerialversionuid() {
		return serialVersionUID;
	}
	public void setOrderprodAttrApi(List<OrderprodAttrApi> orderprodAttrApi) {
		  this.OrderprodAttrApi = orderprodAttrApi;
	}
	public List<OrderprodAttrApi> getOrderprodAttrApi() {
	    
		return OrderprodAttrApi;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getLocation() {
		return location;
	}
	public void setLocation(String location) {
		this.location = location;
	}
	public boolean isDiscountTypePercent() {
		return discountTypePercent;
	}
	public void setDiscountType(boolean discountTypePercent) {
		this.discountTypePercent = discountTypePercent;
	}
    
}
