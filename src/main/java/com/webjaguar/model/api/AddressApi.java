package com.webjaguar.model.api;

import java.io.Serializable;

import org.codehaus.jackson.map.annotate.JsonSerialize;

@JsonSerialize()

public class AddressApi implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String fName;
	private String lName;
	private String company;
	private String addr1;
	private String addr2;
	private String city;
	private String stateProvince;
	private boolean stateProvinceNA;
	private String zip;
	private String country;
	private String phone;
	private String cellPhone;
	private Integer mobileCarrierId;
	private String fax;
	private String email;	
	private boolean primary;
	private boolean residential = false;  //By default is Commercial 
	private boolean liftGate;
	private String code;
	private boolean defaultShipping;


	public boolean isLiftGate()
	{
		return liftGate;
	}
	public void setLiftGate(boolean liftGate)
	{
		this.liftGate = liftGate;
	}
	public boolean isResidential()
	{
		return residential;
	}
	public void setResidential(boolean residential)
	{
		this.residential = residential;
	}
	
	public String getfName() { return fName; }
	public void setfName(String fName) {
		this.fName = fName;
	}

	
	public String getCompany() { return company; }
	public void setCompany(String company) {
		this.company = company;
	}	


	public String getAddr1() { return addr1; }
	public void setAddr1(String addr1) {
		this.addr1 = addr1;
	}
	
	public String getAddr2() { return addr2; }
	public void setAddr2(String addr2) {
		this.addr2 = addr2;
	}
	
	public String getCity() { return city; }
	public void setCity(String city) {
		this.city = city;
	}
	
	public String getStateProvince() { return stateProvince; }
	public void setStateProvince(String stateProvince) {
		this.stateProvince = stateProvince;
	}
	
	public String getZip() { return zip; }
	public void setZip(String zip) {
		this.zip = zip;
	}

	public String getCountry() { return country; }
	public void setCountry(String country) {
		this.country = country;
	}
	
	public String getPhone() { return phone; }
	public void setPhone(String phone) {
		this.phone = phone;
	}

	public String getFax() { return fax; }
	public void setFax(String fax) { 
		this.fax = fax;
	}
	public boolean isPrimary() {
		return primary;
	}
	public void setPrimary(boolean primary) {
		this.primary = primary;
	}
	public String getCellPhone()
	{
		return cellPhone;
	}
	public void setCellPhone(String cellPhone)
	{
		this.cellPhone = cellPhone;
	}
	public String getEmail()
	{
		return email;
	}
	public void setEmail(String email)
	{
		this.email = email;
	}
	
	
	public boolean isStateProvinceNA()
	{
		return stateProvinceNA;
	}
	public void setStateProvinceNA(boolean stateProvinceNA)
	{
		this.stateProvinceNA = stateProvinceNA;
	}
	
	public String getCode() {
		return code;
	}
	public void setCode(String code) {
		this.code = code;
	}
	public boolean isDefaultShipping() {
		return defaultShipping;
	}
	public void setDefaultShipping(boolean defaultShipping) {
		this.defaultShipping = defaultShipping;
	}
	public Integer getMobileCarrierId() {
		return mobileCarrierId;
	}
	public void setMobileCarrierId(Integer mobileCarrierId) {
		this.mobileCarrierId = mobileCarrierId;
	}
	public String getlName() {
		return lName;
	}
	public void setlName(String lName) {
		this.lName = lName;
	}


}
