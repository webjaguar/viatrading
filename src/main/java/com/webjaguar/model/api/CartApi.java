package com.webjaguar.model.api;
import java.io.Serializable;
import org.codehaus.jackson.map.annotate.JsonSerialize;


@JsonSerialize()

public class CartApi implements Serializable{
	private static final long serialVersionUID = 1L;
	private String apiIndex;
    private Integer productId;
    private  Integer Qty;
    private Integer UserId;
    private String optionName;
    private String valueName;
    private Integer valueIndex;
   
	public String getOptionName() {
		return optionName;
	}
	public void setOptionName(String optionName) {
		this.optionName = optionName;
	}
	public String getValueName() {
		return valueName;
	}
	public void setValueName(String valueName) {
		this.valueName = valueName;
	}
	public Integer getProductId() {
		return productId;
	}
	public void setProductId(Integer productId) {
		this.productId = productId;
	}
	public Integer getQty() {
		return Qty;
	}
	public void setQty(Integer qty) {
		Qty = qty;
	}

	public Integer getUserId() {
		return UserId;
	}
	public void setUserId(Integer userId) {
		UserId = userId;
	}
	
	public Integer getValueIndex() {
		return valueIndex;
	}
	public void setValueIndex(Integer valueIndex) {
		this.valueIndex = valueIndex;
	}
	public String getApiIndex() {
		return apiIndex;
	}
	public void setApiIndex(String apiIndex) {
		this.apiIndex = apiIndex;
	}

}
