/*
 * Copyright 2005, 2006 Advanced E-Media Solutions
 * 
 */

package com.webjaguar.model.api;

import java.io.Serializable;
import java.util.*;

import org.apache.poi.hssf.record.formula.functions.And;
import org.codehaus.jackson.annotate.JsonIgnore;
import org.codehaus.jackson.annotate.JsonProperty;
import org.codehaus.jackson.map.annotate.JsonSerialize;
import org.codehaus.jackson.map.annotate.JsonSerialize.Inclusion;

import com.webjaguar.model.Category;
import com.webjaguar.model.Deal;
import com.webjaguar.model.KitParts;
import com.webjaguar.model.Price;
import com.webjaguar.model.Product;
import com.webjaguar.model.ProductField;
import com.webjaguar.model.ProductFieldUnlimitedNameValue;
import com.webjaguar.model.ProductImage;
import com.webjaguar.model.ProductOption;
import com.webjaguar.model.ProductReview;
import com.webjaguar.model.SalesTag;

@JsonSerialize(include=Inclusion.NON_NULL)
public class ApiProduct implements Serializable{
	
	public ApiProduct() {}

	public ApiProduct(Product wjProduct) {
		this.sku = wjProduct.getSku();
		this.name = wjProduct.getName();
		this.shortDesc = wjProduct.getShortDesc();
		this.longDesc = wjProduct.getLongDesc();
		this.masterSku = wjProduct.getMasterSku();
		this.type = wjProduct.getType();
		this.upc = wjProduct.getUpc();
		this.weight = wjProduct.getWeight()+"";
		this.packageL=wjProduct.getPackageL();
		this.packageH=wjProduct.getPackageH();
		this.packageW=wjProduct.getPackageW();
		this.caseContent = wjProduct.getCaseContent();
		this.caseUnitTitle = wjProduct.getCaseUnitTitle();
		this.packing = wjProduct.getPacking();
		this.active = wjProduct.isActive();
		this.msrp = wjProduct.getMsrp();
		this.price = wjProduct.getPrice();
		this.negInventory = wjProduct.isNegInventory();
		this.showNegInventory = wjProduct.isShowNegInventory();
		
		this.price1 = wjProduct.getPrice1();
		this.price2 = wjProduct.getPrice2();
		this.price3 = wjProduct.getPrice3();
		this.price4 = wjProduct.getPrice4();
		this.price5 = wjProduct.getPrice5();
		this.price6 = wjProduct.getPrice6();
		this.price7 = wjProduct.getPrice7();
		this.price8 = wjProduct.getPrice8();
		this.price9 = wjProduct.getPrice9();
		this.price10 = wjProduct.getPrice10();
		this.supplierSku = wjProduct.getField59();
		
		this.qtyBreak1 = wjProduct.getQtyBreak1();
		this.qtyBreak2 = wjProduct.getQtyBreak2();
		this.qtyBreak3 = wjProduct.getQtyBreak3();
		this.qtyBreak4 = wjProduct.getQtyBreak4();
		this.qtyBreak5 = wjProduct.getQtyBreak5();
		this.qtyBreak6 = wjProduct.getQtyBreak6();
		this.qtyBreak7 = wjProduct.getQtyBreak7();
		this.qtyBreak8 = wjProduct.getQtyBreak8();
		this.qtyBreak9 = wjProduct.getQtyBreak9();

		this.cost1 = wjProduct.getCost1();
		this.cost2 = wjProduct.getCost2();
		this.cost3 = wjProduct.getCost3();
		this.cost4 = wjProduct.getCost4();
		this.cost5 = wjProduct.getCost5();
		this.cost6 = wjProduct.getCost6();
		this.cost7 = wjProduct.getCost7();
		this.cost8 = wjProduct.getCost8();
		this.cost9 = wjProduct.getCost9();
		this.cost10 = wjProduct.getCost10();

		this.field1=wjProduct.getField1();
		this.field2=wjProduct.getField2();
		this.field3=wjProduct.getField3();
		this.field4=wjProduct.getField4();
		this.field5=wjProduct.getField5();
		this.field6=wjProduct.getField6();
		this.field7=wjProduct.getField7();
		this.field8=wjProduct.getField8();
		this.field9=wjProduct.getField9();
		this.field10=wjProduct.getField10();
		this.field11=wjProduct.getField11();
		this.field12=wjProduct.getField12();
		this.field13=wjProduct.getField13();
		this.field14=wjProduct.getField14();
		this.field15=wjProduct.getField15();
		this.field16=wjProduct.getField16();
		this.field17=wjProduct.getField17();
		this.field18=wjProduct.getField18();
		this.field19=wjProduct.getField19();
		this.field20=wjProduct.getField20();
		this.field21=wjProduct.getField21();
		this.field22=wjProduct.getField22();
		this.field23=wjProduct.getField23();
		this.field24=wjProduct.getField24();
		this.field25=wjProduct.getField25();
		this.field26=wjProduct.getField26();
		this.field27=wjProduct.getField27();
		this.field28=wjProduct.getField28();
		this.field29=wjProduct.getField29();
		this.field30=wjProduct.getField30();
		this.field31=wjProduct.getField31();
		this.field32=wjProduct.getField32();
		this.field33=wjProduct.getField33();
		this.field34=wjProduct.getField34();
		this.field35=wjProduct.getField35();
		this.field36=wjProduct.getField36();
		this.field37=wjProduct.getField37();
		this.field38=wjProduct.getField38();
		this.field39=wjProduct.getField39();
		this.field40=wjProduct.getField40();
		this.field41=wjProduct.getField41();
		this.field42=wjProduct.getField42();
		this.field43=wjProduct.getField43();
		this.field44=wjProduct.getField44();
		this.field45=wjProduct.getField45();
		this.field46=wjProduct.getField46();
		this.field47=wjProduct.getField47();
		this.field48=wjProduct.getField48();
		this.field49=wjProduct.getField49();
		this.field50=wjProduct.getField50();
		this.field51=wjProduct.getField51();
		this.field52=wjProduct.getField52();
		this.field53=wjProduct.getField53();
		this.field54=wjProduct.getField54();
		this.field55=wjProduct.getField55();
		this.field56=wjProduct.getField56();
		this.field57=wjProduct.getField57();
		this.field58=wjProduct.getField58();
		this.field59=wjProduct.getField59();
		this.field60=wjProduct.getField60();
		this.field61=wjProduct.getField61();
		this.field62=wjProduct.getField62();
		this.field63=wjProduct.getField63();
		this.field64=wjProduct.getField64();
		this.field65=wjProduct.getField65();
		this.field66=wjProduct.getField66();
		this.field67=wjProduct.getField67();
		this.field68=wjProduct.getField68();
		this.field69=wjProduct.getField69();
		this.field70=wjProduct.getField70();
		this.field71=wjProduct.getField71();
		this.field72=wjProduct.getField72();
		this.field73=wjProduct.getField73();
		this.field74=wjProduct.getField74();
		this.field75=wjProduct.getField75();
		this.field76=wjProduct.getField76();
		this.field77=wjProduct.getField77();
		this.field78=wjProduct.getField78();
		this.field79=wjProduct.getField79();
		this.field80=wjProduct.getField80();
		this.field81=wjProduct.getField81();
		this.field82=wjProduct.getField82();
		this.field83=wjProduct.getField83();
		this.field84=wjProduct.getField84();
		this.field85=wjProduct.getField85();
		this.field86=wjProduct.getField86();
		this.field87=wjProduct.getField87();
		this.field88=wjProduct.getField88();
		this.field89=wjProduct.getField89();
		this.field90=wjProduct.getField90();
		this.field91=wjProduct.getField91();
		this.field92=wjProduct.getField92();
		this.field93=wjProduct.getField93();
		this.field94=wjProduct.getField94();
		this.field95=wjProduct.getField95();
		this.field96=wjProduct.getField96();
		this.field97=wjProduct.getField97();
		this.field98=wjProduct.getField98();
		this.field99=wjProduct.getField99();
		this.field100=wjProduct.getField100();
		this.priceTable1=wjProduct.getPriceTable1();
		this.priceTable2=wjProduct.getPriceTable2();
		this.priceTable3=wjProduct.getPriceTable3();
		this.priceTable4=wjProduct.getPriceTable4();
		this.priceTable5=wjProduct.getPriceTable5();
		this.priceTable6=wjProduct.getPriceTable6();
		this.priceTable7=wjProduct.getPriceTable7();
		this.priceTable8=wjProduct.getPriceTable8();
		this.priceTable9=wjProduct.getPriceTable9();
		this.priceTable10=wjProduct.getPriceTable10();
		this.priceCasePackQty = wjProduct.getPriceCasePackQty();
		this.inventory = wjProduct.getInventory();
		this.inventoryAFS = wjProduct.getInventoryAFS();
		this.salesTag = wjProduct.getSalesTag();
		this.images = wjProduct.getImages();
		this.catIds = wjProduct.getCatIds();
		this.defaultSupplierId=wjProduct.getDefaultSupplierId();
		this.incrementalQty=wjProduct.getIncrementalQty();
		this.minimumQty=wjProduct.getMinimumQty();
		
		this.upsMaxItemsInPackage=wjProduct.getUpsMaxItemsInPackage();
		this.uspsMaxItemsInPackage=wjProduct.getUspsMaxItemsInPackage();
		this.fobZipCode=wjProduct.getFobZipCode();
		this.softLink=wjProduct.isSoftLink();
		this.categoryIds=wjProduct.getCategoryIds();
		this.redirectUrlEn=wjProduct.getRedirectUrlEn();
		this.redirectUrlEs=wjProduct.getRedirectUrlEs();
		this.classField=wjProduct.getClassField();
		
		this.i18nProduct=wjProduct.getI18nProduct();
		if (wjProduct.getI18nProduct() != null && wjProduct.getI18nProduct().size() > 0) {
			for (String languageCode : wjProduct.getI18nProduct().keySet()) {
				if (languageCode.equals("es")) {
					Product i18nProductEs = wjProduct.getI18nProduct().get("es");
					this.lang=i18nProductEs.getLang();
					this.i18nName=i18nProductEs.getI18nName();
					this.i18nShortDesc=i18nProductEs.getI18nShortDesc();
					this.i18nLongDesc=i18nProductEs.getI18nLongDesc();
					this.i18nProduct=i18nProductEs.getI18nProduct();
					this.i18nField16=i18nProductEs.getField16();
					this.i18nField17=i18nProductEs.getField17();
					this.i18nField18=i18nProductEs.getField18();
					this.i18nField69=i18nProductEs.getField69();
				}
			}
		}
	}
	
	private static final long serialVersionUID = 1L;
	private Integer id;
	private Set<Object> supplierIds = new HashSet<Object>();
	private String name;
	private String sku;
	private String shortDesc;
	private String longDesc;
	private String productType ="";
	// master sku
	private String masterSku;
	private String categoryIds;

	// etilize
	private String upc;
	
	private String weight;
	private Double msrp;
	private boolean hideMsrp;
	private List<Price> price;
	@JsonIgnore
    @JsonProperty(value = "catIds")
    private Set<Object> catIds = new HashSet<Object>();
	
	// price breaks
	private Double price1;
	private Double price2;
	private Double price3;
	private Double price4;
	private Double price5;
	private Double price6;
	private Double price7;
	private Double price8;
	private Double price9;
	private Double price10;
	private Double cost1;
	private Double cost2;
	private Double cost3;
	private Double cost4;
	private Double cost5;
	private Double cost6;
	private Double cost7;
	private Double cost8;
	private Double cost9;
	private Double cost10;
	private Integer qtyBreak1;
	private Integer qtyBreak2;
	private Integer qtyBreak3;
	private Integer qtyBreak4;
	private Integer qtyBreak5;
	private Integer qtyBreak6;
	private Integer qtyBreak7;
	private Integer qtyBreak8;
	private Integer qtyBreak9;
	// price breaks
	private Double priceTable1;
	private Double priceTable2;
	private Double priceTable3;
	private Double priceTable4;
	private Double priceTable5;
	private Double priceTable6;
	private Double priceTable7;
	private Double priceTable8;
	private Double priceTable9;
	private Double priceTable10;
	// price case pack
	private boolean priceCasePack;
	// not saved in DB, but get the value from one of the fields
	private Integer priceCasePackQty;
	private Double priceCasePack1;
	private Double priceCasePack2;
	private Double priceCasePack3;
	private Double priceCasePack4;
	private Double priceCasePack5;
	private Double priceCasePack6;
	private Double priceCasePack7;
	private Double priceCasePack8;
	private Double priceCasePack9;
	private Double priceCasePack10;
	
    private boolean quote;
	// commission table
    // images
	private ProductImage thumbnail;
	private List<ProductImage> images;
	private String imageFolder;
	private String imageLayout;
	// dates
	private Date created;
	
	private String lastModified;
	// extra fields
	private String field1;
	private String field2;
	private String field3;
	private String field4;
	private String field5;
	private String field6;
	private String field7;
	private String field8;
	private String field9;
	private String field10;
	private String field11;
	private String field12;
	private String field13;
	private String field14;
	private String field15;
	private String field16;
	private String field17;
	private String field18;
	private String field19;
	private String field20;
	private String field21;
	private String field22;
	private String field23;
	private String field24;
	private String field25;
	private String field26;
	private String field27;
	private String field28;
	private String field29;
	private String field30;
	private String field31;
	private String field32;
	private String field33;
	private String field34;
	private String field35;
	private String field36;
	private String field37;
	private String field38;
	private String field39;
	private String field40;
	private String field41;
	private String field42;
	private String field43;
	private String field44;
	private String field45;
	private String field46;
	private String field47;
	private String field48;
	private String field49;
	private String field50;	
	private String field51;
	private String field52;
	private String field53;
	private String field54;
	private String field55;
	private String field56;
	private String field57;
	private String field58;
	private String field59;
	private String field60;
	private String field61;
	private String field62;
	private String field63;
	private String field64;
	private String field65;
	private String field66;
	private String field67;
	private String field68;
	private String field69;
	private String field70;
	private String field71;
	private String field72;
	private String field73;
	private String field74;
	private String field75;
	private String field76;
	private String field77;
	private String field78;
	private String field79;
	private String field80;	
	private String field81;
	private String field82;
	private String field83;
	private String field84;
	private String field85;
	private String field86;
	private String field87;
	private String field88;
	private String field89;
	private String field90;
	private String field91;
	private String field92;
	private String field93;
	private String field94;
	private String field95;
	private String field96;
	private String field97;
	private String field98;
	private String field99;
	private String field100;
	private List<ProductField> productFields;
	
	private String classField;
	private String fobZipCode;
	private boolean softLink;
	private String redirectUrlEn;
	private String redirectUrlEs;

	// product fields unlimited
	private List<ProductFieldUnlimitedNameValue> productFieldUnlimitedNameValue;
	private ProductFieldUnlimitedNameValue masterSkuProductFieldUnlimited;
	
	// keywords
	private String keywords = "";
	
	// protected
	private String protectedLevel = "0";
	
	private List<ProductOption> productOptions;
	// Kit
	private List<KitParts> kitParts;
	// SalesTag
	private SalesTag salesTag;
	
	// unit
	private Integer caseContent;
	private String packing;
	private Double cubicSize;
	
	private Integer minimumQty;
	private Integer incrementalQty;
	
	private boolean loginRequire;
	private boolean hidePrice;
	// recommended items
	private String recommendedList;
	private String recommendedListTitle;
	private String recommendedList1;
	private String recommendedListTitle1;
	private String recommendedList2;
	private String recommendedListTitle2;
	private String recommendedListDisplay;
	// also consider
	private String alsoConsider;
	// Category-Product rank
	private Integer searchRank;
	// layout
	private String headTag;
	private Integer optionIndex;
	// product comparison
	private boolean compare;	
	// views
	private int viewed;
	// special pricing
	private Double specialPricing;
	private boolean taxable = true;
	private boolean enableSpecialTax = false;
	private boolean searchable = true;
	private String optionCode;
	// inventory
	private Integer onHand;

	// SEO
	private Double siteMapPriority = 0.5;
	private boolean addToList;

	private Integer inventory;
	private Boolean negInventory;
	private Integer lowInventory;
	private Integer desiredInventory;
	private Boolean showNegInventory;
	private Integer inventoryAFS;
	private Integer orderPendingQty;
	private Integer purchasePendingQty;
	private Boolean inventoryItem;
	// loyalty
	private Double loyaltyPoint;
	// custom shipping
	private boolean customShippingEnabled = true;
	
	// cross items quantity code (for price breaks)
	private String crossItemsCode;
	
	// active product
	private boolean active = true;
	// Tabs
	private String tab1;
	private String tab1Content;
	private String tab2;
	private String tab2Content;
	private String tab3;
	private String tab3Content;
	private String tab4;
	private String tab4Content;
	private String tab5;
	private String tab5Content;
	private String tab6;
	private String tab6Content;
	private String tab7;
	private String tab7Content;
	private String tab8;
	private String tab8Content;
	private String tab9;
	private String tab9Content;
	private String tab10;
	private String tab10Content;
	// Manufacturer
	private String manufactureName;
	
	// Review
	private ProductReview rate;
	
	// i18n
	private String lang;
	private String i18nName;
	private String i18nShortDesc;
	private String i18nLongDesc;
	private Map<String, Product> i18nProduct;
	
	// Deal
	private Deal deal;
	
	private String supplierSku = "";
	// package dimensions (for shipping calculations)
	private Double packageL;
	private Double packageW;
	private Double packageH;
	private Double upsMaxItemsInPackage;
	private Double uspsMaxItemsInPackage;
	
	private String caseUnitTitle;
	
	private Double discountedCost;
	private Double discountedPrice;
	private Date discountStartDate;
	private Date discountEndDate;
	
	private String qbIncomeAccountType;
	private String qbExpenseAccountType;
	private String decoSku;
	private String productCustomerMarkupTitle;
	
	private Integer screenPrintImprintId;
	private Integer fullColorImprintId;
	private Integer embroideryImprintId;
	private Integer laserEngravingImprintId;
	private Integer debossImprintId;
	private Integer EmbossImprintId;
	private Integer heatTransferImprintId;
	private Integer etchingImprintId;
	private Integer hotStampImprintId;
	private Integer blankSkuImprintId;
	private String sampleSku;
	private String blankSku;
	
	private boolean endQtyPricing;
	private String defaultSupplierSku;
	private Integer rank;
	private Integer boxSize;
	private boolean feedFreeze;
	private double kitCost;
	private boolean hideHeader;
	private boolean hideTopBar;
	private boolean hideLeftBar = true;
	private boolean hideRightBar;
	private boolean hideFooter;	
	private boolean hideBreadCrumbs;	
	private String amazonCategoryId;
	private String amazonSubCategoryId;
	
	private String amazonCategoryId1;
	private String amazonSubCategoryId1;
	private String htmlAddToCart;
	private String productLayout;
	// add To List
	private boolean addToPresentation;
	private Integer temperature;
	private boolean discountPercent1;
	private boolean discountPercent2;
	private boolean discountPercent3;
	private boolean discountPercent4;
	private boolean discountPercent5;
	private boolean discountPercent6;
	private boolean discountPercent7;
	private boolean discountPercent8;
	private boolean discountPercent9;
	private boolean discountPercent10;
	private Double marginPercent1;
	private Double marginPercent2;
	private Double marginPercent3;
	private Double marginPercent4;
	private Double marginPercent5;
	private Double marginPercent6;
	private Double marginPercent7;
	private Double marginPercent8;
	private Double marginPercent9;
	private Double marginPercent10;
	private Double markupPercent1;
	private Double markupPercent2;
	private Double markupPercent3;
	private Double markupPercent4;
	private Double markupPercent5;
	private Double markupPercent6;
	private Double markupPercent7;
	private Double markupPercent8;
	private Double markupPercent9;
	private Double markupPercent10;
	private boolean costPercent;
	private Double catalogPrice;
	private boolean pushedToAccounting;
	private String aliasSku;
	private Double groupSpecialPrice;
	private Double slaveLowestPrice;
	private Double slaveHighestPrice;

	private Integer numCustomLines;
	private Integer customLineCharacter;
	private Integer productLevel;
	private Integer hazardousTier;
	private Integer qtyOnPO;
	private boolean backOrderFlag;
	private int eventProtection;
	private boolean priceByCustomer;
	private boolean retainOnCart = true;
	private boolean groundShipping;
	private Integer locationDesiredAmount;
	private String defaultSupplierName;
	private String defaultSupplierAccountNumber;
	private String type = "";
	private Double unitPrice;
	private String configLabel = "product";
	private boolean hideProduct;
	private int defaultSupplierId;
	private Double cost;
	private Integer salesTagId;
	private Integer selectedFieldGroupIndex;
	private Integer limitQty;
	private Integer locationMinAmount;
	private String feed;
	private String note;
	private boolean enableRate=true;
	private boolean altLoginRequire;
	private Integer slaveCount;
	private Double boxExtraAmt;
	private boolean createOnNotFound;
	private Integer maxQtyToOrder;
	
	private String i18nField16;
	private String i18nField17;
	private String i18nField18;
	private String i18nField69;
	
	public boolean isCreateOnNotFound() {
		return createOnNotFound;
	}
	public void setCreateOnNotFound(boolean createOnNotFound) {
		this.createOnNotFound = createOnNotFound;
	}

	public boolean isDiscountPercent1() {
		return discountPercent1;
	}
	public void setDiscountPercent1(boolean discountPercent1) {
		this.discountPercent1 = discountPercent1;
	}
	public boolean isDiscountPercent2() {
		return discountPercent2;
	}
	public void setDiscountPercent2(boolean discountPercent2) {
		this.discountPercent2 = discountPercent2;
	}
	public boolean isDiscountPercent3() {
		return discountPercent3;
	}
	public void setDiscountPercent3(boolean discountPercent3) {
		this.discountPercent3 = discountPercent3;
	}
	public boolean isDiscountPercent4() {
		return discountPercent4;
	}
	public void setDiscountPercent4(boolean discountPercent4) {
		this.discountPercent4 = discountPercent4;
	}
	public boolean isDiscountPercent5() {
		return discountPercent5;
	}
	public void setDiscountPercent5(boolean discountPercent5) {
		this.discountPercent5 = discountPercent5;
	}
	public boolean isDiscountPercent6() {
		return discountPercent6;
	}
	public void setDiscountPercent6(boolean discountPercent6) {
		this.discountPercent6 = discountPercent6;
	}
	public boolean isDiscountPercent7() {
		return discountPercent7;
	}
	public void setDiscountPercent7(boolean discountPercent7) {
		this.discountPercent7 = discountPercent7;
	}
	public boolean isDiscountPercent8() {
		return discountPercent8;
	}
	public void setDiscountPercent8(boolean discountPercent8) {
		this.discountPercent8 = discountPercent8;
	}
	public boolean isDiscountPercent9() {
		return discountPercent9;
	}
	public void setDiscountPercent9(boolean discountPercent9) {
		this.discountPercent9 = discountPercent9;
	}
	public boolean isDiscountPercent10() {
		return discountPercent10;
	}
	public void setDiscountPercent10(boolean discountPercent10) {
		this.discountPercent10 = discountPercent10;
	}
	public Double getMarginPercent1() {
		return marginPercent1;
	}
	public void setMarginPercent1(Double marginPercent1) {
		this.marginPercent1 = marginPercent1;
	}
	public Double getMarginPercent2() {
		return marginPercent2;
	}
	public void setMarginPercent2(Double marginPercent2) {
		this.marginPercent2 = marginPercent2;
	}
	public Double getMarginPercent3() {
		return marginPercent3;
	}
	public void setMarginPercent3(Double marginPercent3) {
		this.marginPercent3 = marginPercent3;
	}
	public Double getMarginPercent4() {
		return marginPercent4;
	}
	public void setMarginPercent4(Double marginPercent4) {
		this.marginPercent4 = marginPercent4;
	}
	public Double getMarginPercent5() {
		return marginPercent5;
	}
	public void setMarginPercent5(Double marginPercent5) {
		this.marginPercent5 = marginPercent5;
	}
	public Double getMarginPercent6() {
		return marginPercent6;
	}
	public void setMarginPercent6(Double marginPercent6) {
		this.marginPercent6 = marginPercent6;
	}
	public Double getMarginPercent7() {
		return marginPercent7;
	}
	public void setMarginPercent7(Double marginPercent7) {
		this.marginPercent7 = marginPercent7;
	}
	public Double getMarginPercent8() {
		return marginPercent8;
	}
	public void setMarginPercent8(Double marginPercent8) {
		this.marginPercent8 = marginPercent8;
	}
	public Double getMarginPercent9() {
		return marginPercent9;
	}
	public void setMarginPercent9(Double marginPercent9) {
		this.marginPercent9 = marginPercent9;
	}
	public Double getMarginPercent10() {
		return marginPercent10;
	}
	public void setMarginPercent10(Double marginPercent10) {
		this.marginPercent10 = marginPercent10;
	}
	public Double getMarkupPercent1() {
		return markupPercent1;
	}
	public void setMarkupPercent1(Double markupPercent1) {
		this.markupPercent1 = markupPercent1;
	}
	public Double getMarkupPercent2() {
		return markupPercent2;
	}
	public void setMarkupPercent2(Double markupPercent2) {
		this.markupPercent2 = markupPercent2;
	}
	public Double getMarkupPercent3() {
		return markupPercent3;
	}
	public void setMarkupPercent3(Double markupPercent3) {
		this.markupPercent3 = markupPercent3;
	}
	public Double getMarkupPercent4() {
		return markupPercent4;
	}
	public void setMarkupPercent4(Double markupPercent4) {
		this.markupPercent4 = markupPercent4;
	}
	public Double getMarkupPercent5() {
		return markupPercent5;
	}
	public void setMarkupPercent5(Double markupPercent5) {
		this.markupPercent5 = markupPercent5;
	}
	public Double getMarkupPercent6() {
		return markupPercent6;
	}
	public void setMarkupPercent6(Double markupPercent6) {
		this.markupPercent6 = markupPercent6;
	}
	public Double getMarkupPercent7() {
		return markupPercent7;
	}
	public void setMarkupPercent7(Double markupPercent7) {
		this.markupPercent7 = markupPercent7;
	}
	public Double getMarkupPercent8() {
		return markupPercent8;
	}
	public void setMarkupPercent8(Double markupPercent8) {
		this.markupPercent8 = markupPercent8;
	}
	public Double getMarkupPercent9() {
		return markupPercent9;
	}
	public void setMarkupPercent9(Double markupPercent9) {
		this.markupPercent9 = markupPercent9;
	}
	public Double getMarkupPercent10() {
		return markupPercent10;
	}
	public void setMarkupPercent10(Double markupPercent10) {
		this.markupPercent10 = markupPercent10;
	}
	public String getHtmlAddToCart() {
		return htmlAddToCart;
	}
	public void setHtmlAddToCart(String htmlAddToCart) {
		this.htmlAddToCart = htmlAddToCart;
	}
	public String getProductLayout() {
		return productLayout;
	}
	public void setProductLayout(String productLayout) {
		this.productLayout = productLayout;
	}
	public boolean isAddToPresentation() {
		return addToPresentation;
	}
	public void setAddToPresentation(boolean addToPresentation) {
		this.addToPresentation = addToPresentation;
	}
	public String getAmazonCategoryId() {
		return amazonCategoryId;
	}
	public void setAmazonCategoryId(String amazonCategoryId) {
		this.amazonCategoryId = amazonCategoryId;
	}
	public String getAmazonSubCategoryId() {
		return amazonSubCategoryId;
	}
	public void setAmazonSubCategoryId(String amazonSubCategoryId) {
		this.amazonSubCategoryId = amazonSubCategoryId;
	}
	public String getAmazonCategoryId1() {
		return amazonCategoryId1;
	}
	public void setAmazonCategoryId1(String amazonCategoryId1) {
		this.amazonCategoryId1 = amazonCategoryId1;
	}
	public String getAmazonSubCategoryId1() {
		return amazonSubCategoryId1;
	}
	public void setAmazonSubCategoryId1(String amazonSubCategoryId1) {
		this.amazonSubCategoryId1 = amazonSubCategoryId1;
	}
	public boolean isHideHeader() {
		return hideHeader;
	}
	public void setHideHeader(boolean hideHeader) {
		this.hideHeader = hideHeader;
	}
	public boolean isHideTopBar() {
		return hideTopBar;
	}
	public void setHideTopBar(boolean hideTopBar) {
		this.hideTopBar = hideTopBar;
	}
	public boolean isHideLeftBar() {
		return hideLeftBar;
	}
	public void setHideLeftBar(boolean hideLeftBar) {
		this.hideLeftBar = hideLeftBar;
	}
	public boolean isHideRightBar() {
		return hideRightBar;
	}
	public void setHideRightBar(boolean hideRightBar) {
		this.hideRightBar = hideRightBar;
	}
	public boolean isHideFooter() {
		return hideFooter;
	}
	public void setHideFooter(boolean hideFooter) {
		this.hideFooter = hideFooter;
	}
	public boolean isHideBreadCrumbs() {
		return hideBreadCrumbs;
	}
	public void setHideBreadCrumbs(boolean hideBreadCrumbs) {
		this.hideBreadCrumbs = hideBreadCrumbs;
	}
	public Integer getRank() {
		return rank;
	}
	public void setRank(Integer rank) {
		this.rank = rank;
	}
	public String getDefaultSupplierSku() {
		return defaultSupplierSku;
	}
	public void setDefaultSupplierSku(String defaultSupplierSku) {
		this.defaultSupplierSku = defaultSupplierSku;
	}
	public boolean isEndQtyPricing() {
		return endQtyPricing;
	}
	public void setEndQtyPricing(boolean endQtyPricing) {
		this.endQtyPricing = endQtyPricing;
	}
	
	private Date expectedDeliveryDate;
	private Integer expectedDeliveryQty;
	
	
	private boolean commissionable;

	public Double getDiscountedCost() {
		return discountedCost;
	}
	public void setDiscountedCost(Double discountedCost) {
		this.discountedCost = discountedCost;
	}
	public Double getDiscountedPrice() {
		return discountedPrice;
	}
	public void setDiscountedPrice(Double discountedPrice) {
		this.discountedPrice = discountedPrice;
	}
	public Date getDiscountStartDate() {
		return discountStartDate;
	}
	public void setDiscountStartDate(Date discountStartDate) {
		this.discountStartDate = discountStartDate;
	}
	public Date getDiscountEndDate() {
		return discountEndDate;
	}
	public void setDiscountEndDate(Date discountEndDate) {
		this.discountEndDate = discountEndDate;
	}
	
	public boolean isActive()
	{
		return active;
	}
	public void setActive(boolean active)
	{
		this.active = active;
	}

	public Double getLoyaltyPoint()
	{
		return loyaltyPoint;
	}

	public void setLoyaltyPoint(Double loyaltyPoint)
	{
		this.loyaltyPoint = loyaltyPoint;
	}

	public Integer getLowInventory()
	{
		return lowInventory;
	}

	public void setLowInventory(Integer lowInventory)
	{
		this.lowInventory = lowInventory;
	}

	public Integer getInventory()
	{
		return inventory;
	}

	public void setInventory(Integer inventory)
	{
		this.inventory = inventory;
	}
	
	public Integer onHand()
	{
		return inventory;
	}

	public void setOnHand(Integer inventory)
	{
		this.onHand = inventory;
	}
	
	public Integer getInventoryAFS()
	{
		return inventoryAFS;
	}

	public void setInventoryAFS(Integer inventoryAFS)
	{
		this.inventoryAFS = inventoryAFS;
	}

	public Boolean isNegInventory()
	{
		return negInventory;
	}
	
	public Integer getOrderPendingQty() {
		return orderPendingQty;
	}

	public void setOrderPendingQty(Integer orderPendingQty) {
		this.orderPendingQty = orderPendingQty;
	}

	public Integer getPurchasePendingQty() {
		return purchasePendingQty;
	}

	public void setPurchasePendingQty(Integer purchasePendingQty) {
		this.purchasePendingQty = purchasePendingQty;
	}

	public void setNegInventory(Boolean negInventory)
	{
		this.negInventory = negInventory;
	}

	public boolean isTaxable()
	{
		return taxable;
	}

	public void setTaxable(boolean taxable)
	{
		this.taxable = taxable;
	}

	public Double getSpecialPricing()
	{
		return specialPricing;
	}

	public void setSpecialPricing(Double specialPricing)
	{
		this.specialPricing = specialPricing;
	}

	public String getHeadTag()
	{
		return headTag;
	}

	public void setHeadTag(String headTag)
	{
		this.headTag = headTag;
	}

	public boolean isHidePrice()
	{
		return hidePrice;
	}

	public void setHidePrice(boolean hidePrice)
	{
		this.hidePrice = hidePrice;
	}

	public Integer getId()
	{
		return id;
	}

	public void setId(Integer id)
	{
		this.id = id;
	}

	public void setSupplierIds(Set<Object> supplierIds) {
		this.supplierIds = supplierIds;
	}

	@JsonIgnore
    @JsonProperty(value = "supplierIds")
    public Set<Object> getSupplierIds() {
		return supplierIds;
	}

	public String getName()
	{
		return name;
	}
	
	public void setName(String name)
	{
		this.name = name;
	}

	public String getSku()
	{
		return sku;
	}

	public void setSku(String sku)
	{
		this.sku = sku;
	}

	public String getShortDesc()
	{
		System.out.println("apipro getShortDesc called:");
		return shortDesc;
	}

	public void setShortDesc(String shortDesc)
	{
		this.shortDesc = shortDesc;
	}

	public String getLongDesc()
	{
		return longDesc;
	}

	public void setLongDesc(String longDesc)
	{
		this.longDesc = longDesc;
	}

	public Set<Object> getCatIds()
	{
		return catIds;
	}

	public void setCatIds(Set<Object> catIds)
	{
		this.catIds = catIds;
	}

	public void addCategory(Category category)
	{
		if ( category == null )
		{
			throw new IllegalArgumentException( "Null category" );
		}
		// category.getItems().add(this);
		// catIds.add(category);
	}

	public Double getMsrp()
	{
		return msrp;
	}

	public void setMsrp(Double msrp)
	{
		this.msrp = msrp;
	}

	public void setHideMsrp(boolean hideMsrp) {
		this.hideMsrp = hideMsrp;
	}

	public boolean isHideMsrp() {
		return hideMsrp;
	}

	public List<Price> getPrice()
	{
		return price;
	}

	public void setPrice(List<Price> price)
	{
		this.price = price;
	}

	public Double getPrice1()
	{
		return price1;
	}

	public void setPrice1(Double price1)
	{
		this.price1 = price1;
	}

	public Double getPrice2()
	{
		return price2;
	}

	public void setPrice2(Double price2)
	{
		this.price2 = price2;
	}

	public Double getPrice3()
	{
		return price3;
	}

	public void setPrice3(Double price3)
	{
		this.price3 = price3;
	}

	public Double getPrice4()
	{
		return price4;
	}

	public void setPrice4(Double price4)
	{
		this.price4 = price4;
	}

	public Double getPrice5()
	{
		return price5;
	}

	public void setPrice5(Double price5)
	{
		this.price5 = price5;
	}

	public Integer getQtyBreak1()
	{
		return qtyBreak1;
	}

	public void setQtyBreak1(Integer qtyBreak1)
	{
		this.qtyBreak1 = qtyBreak1;
	}

	public Integer getQtyBreak2()
	{
		return qtyBreak2;
	}

	public void setQtyBreak2(Integer qtyBreak2)
	{
		this.qtyBreak2 = qtyBreak2;
	}

	public Integer getQtyBreak3()
	{
		return qtyBreak3;
	}

	public void setQtyBreak3(Integer qtyBreak3)
	{
		this.qtyBreak3 = qtyBreak3;
	}

	public Integer getQtyBreak4()
	{
		return qtyBreak4;
	}

	public void setQtyBreak4(Integer qtyBreak4)
	{
		this.qtyBreak4 = qtyBreak4;
	}

	public List<ProductImage> getImages()
	{
		return images;
	}

	public void setImages(List<ProductImage> images)
	{
		this.images = images;
	}
	
	public Date getCreated()
	{
		return created;
	}

	public void setCreated(Date created)
	{
		this.created = created;
	}

	public String getLastModified()
	{
		return lastModified;
	}
	
	public void addImage(ProductImage image)
	{
		if ( images == null )
			images = new ArrayList<ProductImage>();
		images.add( image );
	}

	public void setLastModified(String lastModified)
	{
		this.lastModified = lastModified;
	}

	public Double getPriceTable1()
	{
		return priceTable1;
	}

	public void setPriceTable1(Double priceTable1)
	{
		this.priceTable1 = priceTable1;
	}

	public Double getPriceTable2()
	{
		return priceTable2;
	}

	public void setPriceTable2(Double priceTable2)
	{
		this.priceTable2 = priceTable2;
	}

	public Double getPriceTable3()
	{
		return priceTable3;
	}

	public void setPriceTable3(Double priceTable3)
	{
		this.priceTable3 = priceTable3;
	}

	public Double getPriceTable4()
	{
		return priceTable4;
	}

	public void setPriceTable4(Double priceTable4)
	{
		this.priceTable4 = priceTable4;
	}

	public Double getPriceTable5()
	{
		return priceTable5;
	}

	public void setPriceTable5(Double priceTable5)
	{
		this.priceTable5 = priceTable5;
	}

	public String getField1()
	{
		return field1;
	}

	public Double getPriceTable6()
	{
		return priceTable6;
	}

	public void setPriceTable6(Double priceTable6)
	{
		this.priceTable6 = priceTable6;
	}

	public Double getPriceTable7()
	{
		return priceTable7;
	}

	public void setPriceTable7(Double priceTable7)
	{
		this.priceTable7 = priceTable7;
	}

	public Double getPriceTable8()
	{
		return priceTable8;
	}

	public void setPriceTable8(Double priceTable8)
	{
		this.priceTable8 = priceTable8;
	}

	public Double getPriceTable9()
	{
		return priceTable9;
	}

	public void setPriceTable9(Double priceTable9)
	{
		this.priceTable9 = priceTable9;
	}
	
	public Double getPriceTable10()
	{
		return priceTable10;
	}
	
	public void setPriceTable10(Double priceTable10)
	{
		this.priceTable10 = priceTable10;
	}
	public boolean isPriceCasePack() {
		return priceCasePack;
	}
	public void setPriceCasePack(boolean priceCasePack) {
		this.priceCasePack = priceCasePack;
	}
	public Integer getPriceCasePackQty() {
		return priceCasePackQty;
	}
	public void setPriceCasePackQty(Integer priceCasePackQty) {
		this.priceCasePackQty = priceCasePackQty;
	}
	public Double getPriceCasePack1() {
		return priceCasePack1;
	}
	public void setPriceCasePack1(Double priceCasePack1) {
		this.priceCasePack1 = priceCasePack1;
	}
	public Double getPriceCasePack2() {
		return priceCasePack2;
	}
	public void setPriceCasePack2(Double priceCasePack2) {
		this.priceCasePack2 = priceCasePack2;
	}
	public Double getPriceCasePack3() {
		return priceCasePack3;
	}
	public void setPriceCasePack3(Double priceCasePack3) {
		this.priceCasePack3 = priceCasePack3;
	}
	public Double getPriceCasePack4() {
		return priceCasePack4;
	}
	public void setPriceCasePack4(Double priceCasePack4) {
		this.priceCasePack4 = priceCasePack4;
	}
	public Double getPriceCasePack5() {
		return priceCasePack5;
	}
	public void setPriceCasePack5(Double priceCasePack5) {
		this.priceCasePack5 = priceCasePack5;
	}
	public Double getPriceCasePack6() {
		return priceCasePack6;
	}
	public void setPriceCasePack6(Double priceCasePack6) {
		this.priceCasePack6 = priceCasePack6;
	}
	public Double getPriceCasePack7() {
		return priceCasePack7;
	}
	public void setPriceCasePack7(Double priceCasePack7) {
		this.priceCasePack7 = priceCasePack7;
	}
	public Double getPriceCasePack8() {
		return priceCasePack8;
	}
	public void setPriceCasePack8(Double priceCasePack8) {
		this.priceCasePack8 = priceCasePack8;
	}
	public Double getPriceCasePack9() {
		return priceCasePack9;
	}
	public void setPriceCasePack9(Double priceCasePack9) {
		this.priceCasePack9 = priceCasePack9;
	}
	public Double getPriceCasePack10() {
		return priceCasePack10;
	}
	public void setPriceCasePack10(Double priceCasePack10) {
		this.priceCasePack10 = priceCasePack10;
	}
	public void setField1(String field1)
	{
		this.field1 = field1;
	}

	public String getField2()
	{
		return field2;
	}

	public void setField2(String field2)
	{
		this.field2 = field2;
	}

	public String getField3()
	{
		return field3;
	}

	public void setField3(String field3)
	{
		this.field3 = field3;
	}

	public String getField4()
	{
		return field4;
	}

	public void setField4(String field4)
	{
		this.field4 = field4;
	}

	public String getField5()
	{
		return field5;
	}

	public void setField5(String field5)
	{
		this.field5 = field5;
	}

	public String getField6()
	{
		return field6;
	}

	public void setField6(String field6)
	{
		this.field6 = field6;
	}

	public String getField7()
	{
		return field7;
	}

	public void setField7(String field7)
	{
		this.field7 = field7;
	}

	public String getField8()
	{
		return field8;
	}

	public void setField8(String field8)
	{
		this.field8 = field8;
	}

	public String getField9()
	{
		return field9;
	}

	public void setField9(String field9)
	{
		this.field9 = field9;
	}

	public String getField10()
	{
		return field10;
	}

	public void setField10(String field10)
	{
		this.field10 = field10;
	}

	public String getField11()
	{
		return field11;
	}

	public void setField11(String field11)
	{
		this.field11 = field11;
	}

	public String getField12()
	{
		return field12;
	}

	public void setField12(String field12)
	{
		this.field12 = field12;
	}

	public String getField13()
	{
		return field13;
	}

	public void setField13(String field13)
	{
		this.field13 = field13;
	}

	public String getField14()
	{
		return field14;
	}

	public void setField14(String field14)
	{
		this.field14 = field14;
	}

	public String getField15()
	{
		return field15;
	}

	public void setField15(String field15)
	{
		this.field15 = field15;
	}

	public String getField16()
	{
		return field16;
	}

	public void setField16(String field16)
	{
		this.field16 = field16;
	}

	public String getField17()
	{
		return field17;
	}

	public void setField17(String field17)
	{
		this.field17 = field17;
	}

	public String getField18()
	{
		return field18;
	}

	public void setField18(String field18)
	{
		this.field18 = field18;
	}

	public String getField19()
	{
		return field19;
	}

	public void setField19(String field19)
	{
		this.field19 = field19;
	}

	public String getField20()
	{
		return field20;
	}

	public void setField20(String field20)
	{
		this.field20 = field20;
	}

	public List<ProductField> getProductFields()
	{
		return productFields;
	}

	public void setProductFields(List<ProductField> productFields)
	{
		this.productFields = productFields;
	}

	

	public String getKeywords()
	{
		return keywords;
	}

	public void setKeywords(String keywords)
	{
		this.keywords = keywords;
	}

	public String getProtectedLevel()
	{
		return protectedLevel;
	}

	public void setProtectedLevel(String protectedLevel)
	{
		this.protectedLevel = protectedLevel;
	}

	public void addProductOption(ProductOption productOption)
	{
		if ( productOptions == null )
			productOptions = new ArrayList<ProductOption>();
		productOptions.add( productOption );
	}

	public List<KitParts> getKitParts() {
		return kitParts;
	}

	public void setKitParts(List<KitParts> kitParts) {
		this.kitParts = kitParts;
	}
	
	public void addkitParts(KitParts kitPart)
	{
		if ( kitParts == null )
			kitParts = new ArrayList<KitParts>();
		kitParts.add( kitPart );
	}
	

	public ProductImage getThumbnail()
	{
		return thumbnail;
	}

	public void setThumbnail(ProductImage thumbnail)
	{
		this.thumbnail = thumbnail;
	}

	public String getField21()
	{
		return field21;
	}

	public void setField21(String field21)
	{
		this.field21 = field21;
	}

	public String getField22()
	{
		return field22;
	}

	public void setField22(String field22)
	{
		this.field22 = field22;
	}

	public String getField23()
	{
		return field23;
	}

	public void setField23(String field23)
	{
		this.field23 = field23;
	}

	public String getField24()
	{
		return field24;
	}

	public void setField24(String field24)
	{
		this.field24 = field24;
	}

	public String getField25()
	{
		return field25;
	}

	public void setField25(String field25)
	{
		this.field25 = field25;
	}

	public String getField26()
	{
		return field26;
	}

	public void setField26(String field26)
	{
		this.field26 = field26;
	}

	public String getField27()
	{
		return field27;
	}

	public void setField27(String field27)
	{
		this.field27 = field27;
	}

	public String getField28()
	{
		return field28;
	}

	public void setField28(String field28)
	{
		this.field28 = field28;
	}

	public String getField29()
	{
		return field29;
	}

	public void setField29(String field29)
	{
		this.field29 = field29;
	}

	public String getField30()
	{
		return field30;
	}

	public void setField30(String field30)
	{
		this.field30 = field30;
	}

	public String getField31()
	{
		return field31;
	}

	public void setField31(String field31)
	{
		this.field31 = field31;
	}

	public String getField32()
	{
		return field32;
	}

	public void setField32(String field32)
	{
		this.field32 = field32;
	}

	public String getField33()
	{
		return field33;
	}

	public void setField33(String field33)
	{
		this.field33 = field33;
	}

	public String getField34()
	{
		return field34;
	}

	public void setField34(String field34)
	{
		this.field34 = field34;
	}

	public String getField35()
	{
		return field35;
	}

	public void setField35(String field35)
	{
		this.field35 = field35;
	}

	public String getField36()
	{
		return field36;
	}

	public void setField36(String field36)
	{
		this.field36 = field36;
	}

	public String getField37()
	{
		return field37;
	}

	public void setField37(String field37)
	{
		this.field37 = field37;
	}

	public String getField38()
	{
		return field38;
	}

	public void setField38(String field38)
	{
		this.field38 = field38;
	}

	public String getField39()
	{
		return field39;
	}

	public void setField39(String field39)
	{
		this.field39 = field39;
	}

	public String getField40()
	{
		return field40;
	}

	public void setField40(String field40)
	{
		this.field40 = field40;
	}

	public String getField41()
	{
		return field41;
	}

	public void setField41(String field41)
	{
		this.field41 = field41;
	}

	public String getField42()
	{
		return field42;
	}

	public void setField42(String field42)
	{
		this.field42 = field42;
	}

	public String getField43()
	{
		return field43;
	}

	public void setField43(String field43)
	{
		this.field43 = field43;
	}

	public String getField44()
	{
		return field44;
	}

	public void setField44(String field44)
	{
		this.field44 = field44;
	}

	public String getField45()
	{
		return field45;
	}

	public void setField45(String field45)
	{
		this.field45 = field45;
	}

	public String getField46()
	{
		return field46;
	}

	public void setField46(String field46)
	{
		this.field46 = field46;
	}

	public String getField47()
	{
		return field47;
	}

	public void setField47(String field47)
	{
		this.field47 = field47;
	}

	public String getField48()
	{
		return field48;
	}

	public void setField48(String field48)
	{
		this.field48 = field48;
	}

	public String getField49()
	{
		return field49;
	}

	public void setField49(String field49)
	{
		this.field49 = field49;
	}

	public String getField50()
	{
		return field50;
	}

	public void setField50(String field50)
	{
		this.field50 = field50;
	}

	public String getField51() {
		return field51;
	}

	public void setField51(String field51) {
		this.field51 = field51;
	}

	public String getField52() {
		return field52;
	}

	public void setField52(String field52) {
		this.field52 = field52;
	}

	public String getField53() {
		return field53;
	}

	public void setField53(String field53) {
		this.field53 = field53;
	}

	public String getField54() {
		return field54;
	}

	public void setField54(String field54) {
		this.field54 = field54;
	}

	public String getField55() {
		return field55;
	}

	public void setField55(String field55) {
		this.field55 = field55;
	}

	public String getField56() {
		return field56;
	}

	public void setField56(String field56) {
		this.field56 = field56;
	}

	public String getField57() {
		return field57;
	}

	public void setField57(String field57) {
		this.field57 = field57;
	}

	public String getField58() {
		return field58;
	}

	public void setField58(String field58) {
		this.field58 = field58;
	}

	public String getField59() {
		return field59;
	}

	public void setField59(String field59) {
		this.field59 = field59;
	}

	public String getField60() {
		return field60;
	}

	public void setField60(String field60) {
		this.field60 = field60;
	}

	public String getField61() {
		return field61;
	}

	public void setField61(String field61) {
		this.field61 = field61;
	}

	public String getField62() {
		return field62;
	}

	public void setField62(String field62) {
		this.field62 = field62;
	}

	public String getField63() {
		return field63;
	}

	public void setField63(String field63) {
		this.field63 = field63;
	}

	public String getField64() {
		return field64;
	}

	public void setField64(String field64) {
		this.field64 = field64;
	}

	public String getField65() {
		return field65;
	}

	public void setField65(String field65) {
		this.field65 = field65;
	}

	public String getField66() {
		return field66;
	}

	public void setField66(String field66) {
		this.field66 = field66;
	}

	public String getField67() {
		return field67;
	}

	public void setField67(String field67) {
		this.field67 = field67;
	}

	public String getField68() {
		return field68;
	}

	public void setField68(String field68) {
		this.field68 = field68;
	}

	public String getField69() {
		return field69;
	}

	public void setField69(String field69) {
		this.field69 = field69;
	}

	public String getField70() {
		return field70;
	}

	public void setField70(String field70) {
		this.field70 = field70;
	}

	public String getField71() {
		return field71;
	}

	public void setField71(String field71) {
		this.field71 = field71;
	}

	public String getField72() {
		return field72;
	}

	public void setField72(String field72) {
		this.field72 = field72;
	}

	public String getField73() {
		return field73;
	}

	public void setField73(String field73) {
		this.field73 = field73;
	}

	public String getField74() {
		return field74;
	}

	public void setField74(String field74) {
		this.field74 = field74;
	}

	public String getField75() {
		return field75;
	}

	public void setField75(String field75) {
		this.field75 = field75;
	}

	public String getField76() {
		return field76;
	}

	public void setField76(String field76) {
		this.field76 = field76;
	}

	public String getField77() {
		return field77;
	}

	public void setField77(String field77) {
		this.field77 = field77;
	}

	public String getField78() {
		return field78;
	}

	public void setField78(String field78) {
		this.field78 = field78;
	}

	public String getField79() {
		return field79;
	}

	public void setField79(String field79) {
		this.field79 = field79;
	}

	public String getField80() {
		return field80;
	}

	public void setField80(String field80) {
		this.field80 = field80;
	}

	public String getField81() {
		return field81;
	}

	public void setField81(String field81) {
		this.field81 = field81;
	}

	public String getField82() {
		return field82;
	}

	public void setField82(String field82) {
		this.field82 = field82;
	}

	public String getField83() {
		return field83;
	}

	public void setField83(String field83) {
		this.field83 = field83;
	}

	public String getField84() {
		return field84;
	}

	public void setField84(String field84) {
		this.field84 = field84;
	}

	public String getField85() {
		return field85;
	}

	public void setField85(String field85) {
		this.field85 = field85;
	}

	public String getField86() {
		return field86;
	}

	public void setField86(String field86) {
		this.field86 = field86;
	}

	public String getField87() {
		return field87;
	}

	public void setField87(String field87) {
		this.field87 = field87;
	}

	public String getField88() {
		return field88;
	}

	public void setField88(String field88) {
		this.field88 = field88;
	}

	public String getField89() {
		return field89;
	}

	public void setField89(String field89) {
		this.field89 = field89;
	}

	public String getField90() {
		return field90;
	}

	public void setField90(String field90) {
		this.field90 = field90;
	}

	public String getField91() {
		return field91;
	}

	public void setField91(String field91) {
		this.field91 = field91;
	}

	public String getField92() {
		return field92;
	}

	public void setField92(String field92) {
		this.field92 = field92;
	}

	public String getField93() {
		return field93;
	}

	public void setField93(String field93) {
		this.field93 = field93;
	}

	public String getField94() {
		return field94;
	}

	public void setField94(String field94) {
		this.field94 = field94;
	}

	public String getField95() {
		return field95;
	}

	public void setField95(String field95) {
		this.field95 = field95;
	}

	public String getField96() {
		return field96;
	}

	public void setField96(String field96) {
		this.field96 = field96;
	}

	public String getField97() {
		return field97;
	}

	public void setField97(String field97) {
		this.field97 = field97;
	}

	public String getField98() {
		return field98;
	}

	public void setField98(String field98) {
		this.field98 = field98;
	}

	public String getField99() {
		return field99;
	}

	public void setField99(String field99) {
		this.field99 = field99;
	}

	public String getField100() {
		return field100;
	}

	public void setField100(String field100) {
		this.field100 = field100;
	}

	public String getWeight()
	{
		return weight;
	}

	public void setWeight(String weight)
	{
		this.weight = weight;
	}

	public SalesTag getSalesTag()
	{
		return salesTag;
	}

	

	public void setSalesTag(SalesTag salesTag)
	{
		this.salesTag = salesTag;
	}
	
	public boolean isLoginRequire()
	{
		return loginRequire;
	}

	public void setLoginRequire(boolean loginRequire)
	{
		this.loginRequire = loginRequire;
	}

	public String getRecommendedList()
	{
		return recommendedList;
	}

	public void setRecommendedList(String recommendedList)
	{
		this.recommendedList = recommendedList;
	}

	public String getAlsoConsider()
	{
		return alsoConsider;
	}

	public void setAlsoConsider(String alsoConsider)
	{
		this.alsoConsider = alsoConsider;
	}
	
	public Integer getCaseContent()
	{
		return caseContent;
	}

	public void setCaseContent(Integer caseContent)
	{
		this.caseContent = caseContent;
	}

	public String getPacking()
	{
		return packing;
	}

	public void setPacking(String packing)
	{
		this.packing = packing;
	}

	public String getRecommendedListDisplay()
	{
		return recommendedListDisplay;
	}

	public void setRecommendedListDisplay(String recommendedListDisplay)
	{
		this.recommendedListDisplay = recommendedListDisplay;
	}

	public String getRecommendedListTitle()
	{
		return recommendedListTitle;
	}

	public void setRecommendedListTitle(String recommendedListTitle)
	{
		this.recommendedListTitle = recommendedListTitle;
	}

	public String getImageFolder()
	{
		return imageFolder;
	}

	public void setImageFolder(String imageFolder)
	{
		this.imageFolder = imageFolder;
	}

	public boolean isCompare()
	{
		return compare;
	}

	public void setCompare(boolean compare)
	{
		this.compare = compare;
	}

	public int getViewed()
	{
		return viewed;
	}

	public void setViewed(int viewed)
	{
		this.viewed = viewed;
	}

	public String getImageLayout()
	{
		return imageLayout;
	}

	public void setImageLayout(String imageLayout)
	{
		this.imageLayout = imageLayout;
	}
	public boolean isSearchable()
	{
		return searchable;
	}

	public void setSearchable(boolean searchable)
	{
		this.searchable = searchable;
	}

	public Integer getOptionIndex()
	{
		return optionIndex;
	}

	public void setOptionIndex(Integer optionIndex)
	{
		this.optionIndex = optionIndex;
	}

	public String getOptionCode()
	{
		return optionCode;
	}

	public void setOptionCode(String optionCode)
	{
		this.optionCode = optionCode;
	}

	public Boolean isShowNegInventory()
	{
		return showNegInventory;
	}

	public void setShowNegInventory(Boolean showNegInventory)
	{
		this.showNegInventory = showNegInventory;
	}

	public boolean isCustomShippingEnabled()
	{
		return customShippingEnabled;
	}

	public void setCustomShippingEnabled(boolean customShippingEnabled)
	{
		this.customShippingEnabled = customShippingEnabled;
	}

	public String getCrossItemsCode()
	{
		return crossItemsCode;
	}

	public void setCrossItemsCode(String crossItemsCode)
	{
		this.crossItemsCode = crossItemsCode;
	}

	public Double getPrice6()
	{
		return price6;
	}

	public void setPrice6(Double price6)
	{
		this.price6 = price6;
	}

	public Double getPrice7()
	{
		return price7;
	}

	public void setPrice7(Double price7)
	{
		this.price7 = price7;
	}

	public Double getPrice8()
	{
		return price8;
	}

	public void setPrice8(Double price8)
	{
		this.price8 = price8;
	}

	public Double getPrice9()
	{
		return price9;
	}

	public void setPrice9(Double price9)
	{
		this.price9 = price9;
	}

	public Double getPrice10()
	{
		return price10;
	}

	public void setPrice10(Double price10)
	{
		this.price10 = price10;
	}

	public Integer getQtyBreak5()
	{
		return qtyBreak5;
	}

	public void setQtyBreak5(Integer qtyBreak5)
	{
		this.qtyBreak5 = qtyBreak5;
	}

	public Integer getQtyBreak6()
	{
		return qtyBreak6;
	}

	public void setQtyBreak6(Integer qtyBreak6)
	{
		this.qtyBreak6 = qtyBreak6;
	}

	public Integer getQtyBreak7()
	{
		return qtyBreak7;
	}

	public void setQtyBreak7(Integer qtyBreak7)
	{
		this.qtyBreak7 = qtyBreak7;
	}

	public Integer getQtyBreak8()
	{
		return qtyBreak8;
	}

	public void setQtyBreak8(Integer qtyBreak8)
	{
		this.qtyBreak8 = qtyBreak8;
	}

	public Integer getQtyBreak9()
	{
		return qtyBreak9;
	}

	public void setQtyBreak9(Integer qtyBreak9)
	{
		this.qtyBreak9 = qtyBreak9;
	}

	public String getTab1()
	{
		return tab1;
	}

	public void setTab1(String tab1)
	{
		this.tab1 = tab1;
	}

	public String getTab2()
	{
		return tab2;
	}

	public void setTab2(String tab2)
	{
		this.tab2 = tab2;
	}

	public String getTab3()
	{
		return tab3;
	}

	public void setTab3(String tab3)
	{
		this.tab3 = tab3;
	}

	public String getTab4()
	{
		return tab4;
	}

	public void setTab4(String tab4)
	{
		this.tab4 = tab4;
	}

	public String getTab5()
	{
		return tab5;
	}
	
	public void setTab5(String tab5)
	{
		this.tab5 = tab5;
	}

	
	public String getTab6()
	{
		return tab6;
	}

	public void setTab6(String tab6)
	{
		this.tab6 = tab6;
	}
		
	public String getTab7()
	{
		return tab7;
	}
	

	public void setTab7(String tab7)
	{
		this.tab7 = tab7;
	}
		
	public String getTab8()
	{
		return tab8;
	}
	
	public void setTab8(String tab8)
	{
		this.tab8 = tab8;
	}
		
	public String getTab9()
	{
		return tab9;
	}
	
	public void setTab9(String tab9)
	{
		this.tab9 = tab9;
	}
	
	public String getTab10()
	{
		return tab10;
	}

	public void setTab10(String tab10)
	{
		this.tab10 = tab10;
	}
	
	public String getTab1Content()
	{
		return tab1Content;
	}

	public void setTab1Content(String tab1Content)
	{
		this.tab1Content = tab1Content;
	}

	public String getTab2Content()
	{
		return tab2Content;
	}

	public void setTab2Content(String tab2Content)
	{
		this.tab2Content = tab2Content;
	}

	public String getTab3Content()
	{
		return tab3Content;
	}

	public void setTab3Content(String tab3Content)
	{
		this.tab3Content = tab3Content;
	}

	public String getTab4Content()
	{
		return tab4Content;
	}

	public void setTab4Content(String tab4Content)
	{
		this.tab4Content = tab4Content;
	}

	public String getTab5Content()
	{
		return tab5Content;
	}

	public void setTab5Content(String tab5Content)
	{
		this.tab5Content = tab5Content;
	}

	public String getTab6Content() {
		return tab6Content;
	}

	public void setTab6Content(String tab6Content) {
		this.tab6Content = tab6Content;
	}

	public String getTab7Content() {
		return tab7Content;
	}

	public void setTab7Content(String tab7Content) {
		this.tab7Content = tab7Content;
	}

	public String getTab8Content() {
		return tab8Content;
	}

	public void setTab8Content(String tab8Content) {
		this.tab8Content = tab8Content;
	}

	public String getTab9Content() {
		return tab9Content;
	}

	public void setTab9Content(String tab9Content) {
		this.tab9Content = tab9Content;
	}

	public String getTab10Content() {
		return tab10Content;
	}

	public void setTab10Content(String tab10Content) {
		this.tab10Content = tab10Content;
	}

	public Integer getMinimumQty()
	{
		return minimumQty;
	}

	public void setMinimumQty(Integer minimumQty)
	{
		this.minimumQty = minimumQty;
	}

	public Integer getIncrementalQty()
	{
		return incrementalQty;
	}

	public void setIncrementalQty(Integer incrementalQty)
	{
		this.incrementalQty = incrementalQty;
	}
	
	public String getMasterSku()
	{
		return masterSku;
	}

	public void setMasterSku(String masterSku)
	{
		this.masterSku = masterSku;
	}
	public String getManufactureName()
	{
		return manufactureName;
	}

	public void setManufactureName(String manufactureName)
	{
		this.manufactureName = manufactureName;
	}

	public ProductReview getRate()
	{
		return rate;
	}

	public void setRate(ProductReview rate)
	{
		this.rate = rate;
	}

	public String getLang()
	{
		return lang;
	}

	public void setLang(String lang)
	{
		this.lang = lang;
	}

	public String getI18nName()
	{
		return i18nName;
	}

	public void setI18nName(String name)
	{
		i18nName = name;
	}

	public String getI18nShortDesc()
	{
		return i18nShortDesc;
	}

	public void setI18nShortDesc(String shortDesc)
	{
		i18nShortDesc = shortDesc;
	}

	public String getI18nLongDesc()
	{
		return i18nLongDesc;
	}

	public void setI18nLongDesc(String longDesc)
	{
		i18nLongDesc = longDesc;
	}

	public Map<String, Product> getI18nProduct()
	{
		return i18nProduct;
	}

	public void setI18nProduct(Map<String, Product> apiProduct)
	{
		i18nProduct = apiProduct;
	}

	

	public void setDeal(Deal deal) {
		this.deal = deal;
	}

	public Deal getDeal() {
		return deal;
	}
	
	public String getUpc() {
		return upc;
	}

	public void setUpc(String upc) {
		this.upc = upc;
	}
	public Double getPackageL() {
		return packageL;
	}

	public void setPackageL(Double packageL) {
		this.packageL = packageL;
	}

	public Double getPackageW() {
		return packageW;
	}

	public void setPackageW(Double packageW) {
		this.packageW = packageW;
	}

	public Double getPackageH() {
		return packageH;
	}

	public void setPackageH(Double packageH) {
		this.packageH = packageH;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	public void setQuote(boolean quote) {
		this.quote = quote;
	}
	public boolean isQuote() {
		return quote;
	}
	public void setUpsMaxItemsInPackage(Double upsMaxItemsInPackage) {
		this.upsMaxItemsInPackage = upsMaxItemsInPackage;
	}
	public Double getUpsMaxItemsInPackage() {
		return upsMaxItemsInPackage;
	}
	public void setUspsMaxItemsInPackage(Double uspsMaxItemsInPackage) {
		this.uspsMaxItemsInPackage = uspsMaxItemsInPackage;
	}
	public Double getUspsMaxItemsInPackage() {
		return uspsMaxItemsInPackage;
	}
	public Double getCost1() {
		return cost1;
	}
	public void setCost1(Double cost1) {
		this.cost1 = cost1;
	}
	public Double getCost2() {
		return cost2;
	}
	public void setCost2(Double cost2) {
		this.cost2 = cost2;
	}
	public Double getCost3() {
		return cost3;
	}
	public void setCost3(Double cost3) {
		this.cost3 = cost3;
	}
	public Double getCost4() {
		return cost4;
	}
	public void setCost4(Double cost4) {
		this.cost4 = cost4;
	}
	public Double getCost5() {
		return cost5;
	}
	public void setCost5(Double cost5) {
		this.cost5 = cost5;
	}
	public Double getCost6() {
		return cost6;
	}
	public void setCost6(Double cost6) {
		this.cost6 = cost6;
	}
	public Double getCost7() {
		return cost7;
	}
	public void setCost7(Double cost7) {
		this.cost7 = cost7;
	}
	public Double getCost8() {
		return cost8;
	}
	public void setCost8(Double cost8) {
		this.cost8 = cost8;
	}
	public Double getCost9() {
		return cost9;
	}
	public void setCost9(Double cost9) {
		this.cost9 = cost9;
	}
	public Double getCost10() {
		return cost10;
	}
	public void setCost10(Double cost10) {
		this.cost10 = cost10;
	}
	
	public Integer getSearchRank() {
		return searchRank;
	}
	public void setSearchRank(Integer searchRank) {
		this.searchRank = searchRank;
	}
	
	public List<ProductFieldUnlimitedNameValue> getProductFieldsUnlimitedNameValue() {
		return productFieldUnlimitedNameValue;
	}
	public void setProductFieldsUnlimitedNameValue(List<ProductFieldUnlimitedNameValue> productFieldUnlimitedNameValue) {
		this.productFieldUnlimitedNameValue = productFieldUnlimitedNameValue;
	}
	public ProductFieldUnlimitedNameValue getMasterSkuProductFieldUnlimited() {
		return masterSkuProductFieldUnlimited;
	}
	public void setMasterSkuProductFieldUnlimited(ProductFieldUnlimitedNameValue masterSkuProductFieldUnlimited) {
		this.masterSkuProductFieldUnlimited = masterSkuProductFieldUnlimited;
	}
	
	public boolean isEnableSpecialTax() {
		return enableSpecialTax;
	}
	public void setEnableSpecialTax(boolean enableSpecialTax) {
		this.enableSpecialTax = enableSpecialTax;
	}
	public Integer getDesiredInventory() {
		return desiredInventory;
	}
	public void setDesiredInventory(Integer desiredInventory) {
		this.desiredInventory = desiredInventory;
	}
	
	
	public String getRecommendedList1() {
		return recommendedList1;
	}
	public void setRecommendedList1(String recommendedList1) {
		this.recommendedList1 = recommendedList1;
	}
	public String getRecommendedListTitle1() {
		return recommendedListTitle1;
	}
	public void setRecommendedListTitle1(String recommendedListTitle1) {
		this.recommendedListTitle1 = recommendedListTitle1;
	}
	public String getRecommendedList2() {
		return recommendedList2;
	}
	public void setRecommendedList2(String recommendedList2) {
		this.recommendedList2 = recommendedList2;
	}
	public String getRecommendedListTitle2() {
		return recommendedListTitle2;
	}
	public void setRecommendedListTitle2(String recommendedListTitle2) {
		this.recommendedListTitle2 = recommendedListTitle2;
	}
	public Double getCubicSize() {
		return cubicSize;
	}
	public void setCubicSize(Double cubicSize) {
		this.cubicSize = cubicSize;
	}
	public String getCaseUnitTitle() {
		return caseUnitTitle;
	}
	public void setCaseUnitTitle(String caseUnitTitle) {
		this.caseUnitTitle = caseUnitTitle;
	}
	public String getDecoSku() {
		return decoSku;
	}
	public void setDecoSku(String decoSku) {
		this.decoSku = decoSku;
	}
	public String getProductCustomerMarkupTitle() {
		return productCustomerMarkupTitle;
	}
	public void setProductCustomerMarkupTitle(String productCustomerMarkupTitle) {
		this.productCustomerMarkupTitle = productCustomerMarkupTitle;
	}
	public Boolean isInventoryItem() {
		return inventoryItem;
	}
	public void setInventoryItem(Boolean inventoryItem) {
		this.inventoryItem = inventoryItem;
	}
	public String getQbExpenseAccountType() {
		return qbExpenseAccountType;
	}
	public void setQbExpenseAccountType(String qbExpenseAccountType) {
		this.qbExpenseAccountType = qbExpenseAccountType;
	}
	public String getQbIncomeAccountType() {
		return qbIncomeAccountType;
	}
	public void setQbIncomeAccountType(String qbIncomeAccountType) {
		this.qbIncomeAccountType = qbIncomeAccountType;
	}
	public Integer getFullColorImprintId() {
		return fullColorImprintId;
	}
	public void setFullColorImprintId(Integer fullColorImprintId) {
		this.fullColorImprintId = fullColorImprintId;
	}
	public Integer getEmbroideryImprintId() {
		return embroideryImprintId;
	}
	public void setEmbroideryImprintId(Integer embroideryImprintId) {
		this.embroideryImprintId = embroideryImprintId;
	}
	public Integer getScreenPrintImprintId() {
		return screenPrintImprintId;
	}
	public void setScreenPrintImprintId(Integer screenPrintImprintId) {
		this.screenPrintImprintId = screenPrintImprintId;
	}
	public String getSampleSku() {
		return sampleSku;
	}
	public void setSampleSku(String sampleSku) {
		this.sampleSku = sampleSku;
	}
	public String getBlankSku() {
		return blankSku;
	}
	public void setBlankSku(String blankSku) {
		this.blankSku = blankSku;
	}
	public Integer getLaserEngravingImprintId() {
		return laserEngravingImprintId;
	}
	public void setLaserEngravingImprintId(Integer laserEngravingImprintId) {
		this.laserEngravingImprintId = laserEngravingImprintId;
	}
	public Integer getDebossImprintId() {
		return debossImprintId;
	}
	public void setDebossImprintId(Integer debossImprintId) {
		this.debossImprintId = debossImprintId;
	}
	public Integer getEmbossImprintId() {
		return EmbossImprintId;
	}
	public void setEmbossImprintId(Integer embossImprintId) {
		EmbossImprintId = embossImprintId;
	}
	public Integer getHeatTransferImprintId() {
		return heatTransferImprintId;
	}
	public void setHeatTransferImprintId(Integer heatTransferImprintId) {
		this.heatTransferImprintId = heatTransferImprintId;
	}
	public Integer getEtchingImprintId() {
		return etchingImprintId;
	}
	public void setEtchingImprintId(Integer etchingImprintId) {
		this.etchingImprintId = etchingImprintId;
	}
	public Integer getHotStampImprintId() {
		return hotStampImprintId;
	}
	public void setHotStampImprintId(Integer hotStampImprintId) {
		this.hotStampImprintId = hotStampImprintId;
	}
	public Date getExpectedDeliveryDate() {
		return expectedDeliveryDate;
	}
	public void setExpectedDeliveryDate(Date expectedDeliveryDate) {
		this.expectedDeliveryDate = expectedDeliveryDate;
	}
	public Integer getExpectedDeliveryQty() {
		return expectedDeliveryQty;
	}
	public void setExpectedDeliveryQty(Integer expectedDeliveryQty) {
		this.expectedDeliveryQty = expectedDeliveryQty;
	}
	
	public boolean isCommissionable() {
		return commissionable;
	}
	public void setCommissionable(boolean commissionable) {
		this.commissionable = commissionable;
	}
	public Integer getBlankSkuImprintId() {
		return blankSkuImprintId;
	}
	public void setBlankSkuImprintId(Integer blankSkuImprintId) {
		this.blankSkuImprintId = blankSkuImprintId;
	}
	public String getProductType() {
		return productType;
	}
	public void setProductType(String productType) {
		this.productType = productType;
	}
	public String getCategoryIds() {
		return categoryIds;
	}
	public void setCategoryIds(String categoryIds) {
		this.categoryIds = categoryIds;
	}
	public boolean isAddToList() {
		return addToList;
	}
	public void setAddToList(boolean addToList) {
		this.addToList = addToList;
	}
	public Double getSiteMapPriority() {
		return siteMapPriority;
	}
	public void setSiteMapPriority(Double siteMapPriority) {
		this.siteMapPriority = siteMapPriority;
	}
	public Integer getBoxSize() {
		return boxSize;
	}
	public void setBoxSize(Integer boxSize) {
		this.boxSize = boxSize;
	}
	public boolean isFeedFreeze() {
		return feedFreeze;
	}
	public void setFeedFreeze(boolean feedFreeze) {
		this.feedFreeze = feedFreeze;
	}
	public double getKitCost() {
		return kitCost;
	}
	public void setKitCost(double kitCost) {
		this.kitCost = kitCost;
	}
	public Integer getTemperature() {
		return temperature;
	}
	public void setTemperature(Integer temperature) {
		this.temperature = temperature;
	}
	public boolean isCostPercent() {
		return costPercent;
	}
	public void setCostPercent(boolean costPercent) {
		this.costPercent = costPercent;
	}
	public Double getCatalogPrice() {
		return catalogPrice;
	}
	public void setCatalogPrice(Double catalogPrice) {
		this.catalogPrice = catalogPrice;
	}
	public boolean isPushedToAccounting() {
		return pushedToAccounting;
	}
	public void setPushedToAccounting(boolean pushedToAccounting) {
		this.pushedToAccounting = pushedToAccounting;
	}
	public String getAliasSku() {
		return aliasSku;
	}
	public void setAliasSku(String aliasSku) {
		this.aliasSku = aliasSku;
	}
	public Double getGroupSpecialPrice() {
		return groupSpecialPrice;
	}
	public void setGroupSpecialPrice(Double groupSpecialPrice) {
		this.groupSpecialPrice = groupSpecialPrice;
	}
	public Double getSlaveLowestPrice() {
		return slaveLowestPrice;
	}
	public void setSlaveLowestPrice(Double slaveLowestPrice) {
		this.slaveLowestPrice = slaveLowestPrice;
	}
	public Double getSlaveHighestPrice() {
		return slaveHighestPrice;
	}
	public void setSlaveHighestPrice(Double slaveHighestPrice) {
		this.slaveHighestPrice = slaveHighestPrice;
	}
	public Integer getNumCustomLines() {
		return numCustomLines;
	}
	public void setNumCustomLines(Integer numCustomLines) {
		this.numCustomLines = numCustomLines;
	}
	public Integer getCustomLineCharacter() {
		return customLineCharacter;
	}
	public void setCustomLineCharacter(Integer customLineCharacter) {
		this.customLineCharacter = customLineCharacter;
	}
	public Integer getProductLevel() {
		return productLevel;
	}
	public void setProductLevel(Integer productLevel) {
		this.productLevel = productLevel;
	}
	public Integer getHazardousTier() {
		return hazardousTier;
	}
	public void setHazardousTier(Integer hazardousTier) {
		this.hazardousTier = hazardousTier;
	}
	public Integer getQtyOnPO() {
		return qtyOnPO;
	}
	public void setQtyOnPO(Integer qtyOnPO) {
		this.qtyOnPO = qtyOnPO;
	}
	public boolean isBackOrderFlag() {
		return backOrderFlag;
	}
	public void setBackOrderFlag(boolean backOrderFlag) {
		this.backOrderFlag = backOrderFlag;
	}
	public int getEventProtection() {
		return eventProtection;
	}
	public void setEventProtection(int eventProtection) {
		this.eventProtection = eventProtection;
	}
	public boolean isPriceByCustomer() {
		return priceByCustomer;
	}
	public void setPriceByCustomer(boolean priceByCustomer) {
		this.priceByCustomer = priceByCustomer;
	}
	public boolean isRetainOnCart() {
		return retainOnCart;
	}
	public void setRetainOnCart(boolean retainOnCart) {
		this.retainOnCart = retainOnCart;
	}
	public boolean isGroundShipping() {
		return groundShipping;
	}
	public void setGroundShipping(boolean groundShipping) {
		this.groundShipping = groundShipping;
	}
	public Integer getLocationDesiredAmount() {
		return locationDesiredAmount;
	}
	public void setLocationDesiredAmount(Integer locationDesiredAmount) {
		this.locationDesiredAmount = locationDesiredAmount;
	}
	public String getDefaultSupplierAccountNumber() {
		return defaultSupplierAccountNumber;
	}
	public void setDefaultSupplierAccountNumber(String defaultSupplierAccountNumber) {
		this.defaultSupplierAccountNumber = defaultSupplierAccountNumber;
	}
	public String getDefaultSupplierName() {
		return defaultSupplierName;
	}
	public void setDefaultSupplierName(String defaultSupplierName) {
		this.defaultSupplierName = defaultSupplierName;
	}
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	public Double getUnitPrice() {
		return unitPrice;
	}
	public void setUnitPrice(Double unitPrice) {
		this.unitPrice = unitPrice;
	}
	public String getConfigLabel() {
		return configLabel;
	}
	public void setConfigLabel(String configLabel) {
		this.configLabel = configLabel;
	}
	public boolean isHideProduct() {
		return hideProduct;
	}
	public void setHideProduct(boolean hideProduct) {
		this.hideProduct = hideProduct;
	}
	public Double getCost() {
		return cost;
	}
	public void setCost(Double cost) {
		this.cost = cost;
	}
	public int getDefaultSupplierId() {
		return defaultSupplierId;
	}
	public void setDefaultSupplierId(int defaultSupplierId) {
		this.defaultSupplierId = defaultSupplierId;
	}
	public Integer getSalesTagId() {
		return salesTagId;
	}
	public void setSalesTagId(Integer salesTagId) {
		this.salesTagId = salesTagId;
	}
	public Integer getSelectedFieldGroupIndex() {
		return selectedFieldGroupIndex;
	}
	public void setSelectedFieldGroupIndex(Integer selectedFieldGroupIndex) {
		this.selectedFieldGroupIndex = selectedFieldGroupIndex;
	}
	public Integer getLimitQty() {
		return limitQty;
	}
	public void setLimitQty(Integer limitQty) {
		this.limitQty = limitQty;
	}
	public Integer getLocationMinAmount() {
		return locationMinAmount;
	}
	public void setLocationMinAmount(Integer locationMinAmount) {
		this.locationMinAmount = locationMinAmount;
	}
	public String getFeed() {
		return feed;
	}
	public void setFeed(String feed) {
		this.feed = feed;
	}
	public String getNote() {
		return note;
	}
	public void setNote(String note) {
		this.note = note;
	}
	public boolean isEnableRate() {
		return enableRate;
	}
	public void setEnableRate(boolean enableRate) {
		this.enableRate = enableRate;
	}
	public boolean isAltLoginRequire() {
		return altLoginRequire;
	}
	public void setAltLoginRequire(boolean altLoginRequire) {
		this.altLoginRequire = altLoginRequire;
	}
	public Integer getSlaveCount() {
		return slaveCount;
	}
	public void setSlaveCount(Integer slaveCount) {
		this.slaveCount = slaveCount;
	}
	public Double getBoxExtraAmt() {
		return boxExtraAmt;
	}
	public void setBoxExtraAmt(Double boxExtraAmt) {
		this.boxExtraAmt = boxExtraAmt;
	}
	public String getSupplierSku() {
		return supplierSku;
	}
	public void setSupplierSku(String supplierSku) {
		this.supplierSku = supplierSku;
	}
	public Integer getMaxQtyToOrder() {
		return maxQtyToOrder;
	}
	public void setMaxQtyToOrder(Integer maxQtyToOrder) {
		this.maxQtyToOrder = maxQtyToOrder;
	}

	public String getClassField() {
		return classField;
	}

	public void setClassField(String classField) {
		this.classField = classField;
	}

	public String getFobZipCode() {
		return fobZipCode;
	}

	public void setFobZipCode(String fobZipCode) {
		this.fobZipCode = fobZipCode;
	}

	public boolean isSoftLink() {
		return softLink;
	}

	public void setSoftLink(boolean softLink) {
		this.softLink = softLink;
	}

	public String getRedirectUrlEn() {
		return redirectUrlEn;
	}

	public void setRedirectUrlEn(String redirectUrlEn) {
		this.redirectUrlEn = redirectUrlEn;
	}

	public String getRedirectUrlEs() {
		return redirectUrlEs;
	}

	public void setRedirectUrlEs(String redirectUrlEs) {
		this.redirectUrlEs = redirectUrlEs;
	}

	public String getI18nField16() {
		return i18nField16;
	}

	public void setI18nField16(String i18nField16) {
		this.i18nField16 = i18nField16;
	}

	public String getI18nField17() {
		return i18nField17;
	}

	public void setI18nField17(String i18nField17) {
		this.i18nField17 = i18nField17;
	}

	public String getI18nField18() {
		return i18nField18;
	}

	public void setI18nField18(String i18nField18) {
		this.i18nField18 = i18nField18;
	}

	public String getI18nField69() {
		return i18nField69;
	}

	public void setI18nField69(String i18nField69) {
		this.i18nField69 = i18nField69;
	}
	
}
