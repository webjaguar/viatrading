package com.webjaguar.model.api;

import java.io.Serializable;

import org.codehaus.jackson.map.annotate.JsonSerialize;
@JsonSerialize()

public class WishListApi implements Serializable{
	private static final long serialVersionUID = 1L;

	Integer userId;
	Integer productId;
	public Integer getUserId() {
		return userId;
	}
	public void setUserId(Integer userId) {
		this.userId = userId;
	}
	public Integer getProductId() {
		return productId;
	}
	public void setProductId(Integer productId) {
		this.productId = productId;
	}
	
	
}
