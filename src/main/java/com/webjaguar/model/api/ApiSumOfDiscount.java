package com.webjaguar.model.api;


import java.util.Date;
import java.util.HashSet;
import java.util.Set;

import org.codehaus.jackson.map.annotate.JsonSerialize;
import org.codehaus.jackson.map.annotate.JsonSerialize.Inclusion;


@JsonSerialize(include=Inclusion.NON_NULL)
public class ApiSumOfDiscount {
	
	private int index;
	private int[] productIds;
	private int customerId;
	private String promoCodeTitle;
	private double subTotal;
	
	public int getIndex() {
		return index;
	}
	public void setIndex(int index) {
		this.index = index;
	}
	public int[] getProductIds() {
		return productIds;
	}
	public void setProductIds(int[] productIds) {
		this.productIds = productIds;
	}
	public int getCustomerId() {
		return customerId;
	}
	public void setCustomerId(int customerId) {
		this.customerId = customerId;
	}
	public String getPromoCodeTitle() {
		return promoCodeTitle;
	}
	public void setPromoCodeTitle(String promoCodeTitle) {
		this.promoCodeTitle = promoCodeTitle;
	}
	public double getSubTotal() {
		return subTotal;
	}
	public void setSubTotal(double subTotal) {
		this.subTotal = subTotal;
	} 
	
}
