package com.webjaguar.model.api;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import org.codehaus.jackson.map.annotate.JsonSerialize;

@JsonSerialize()
public class CrmContactFieldApi implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private Integer id;
	private String globalName;
	private String fieldName;
	private String fieldValue;
	private boolean enabled;
	private boolean rank;
	private Integer fieldType;
	private String options;
	private List<String> valueList = new ArrayList<String>();
	
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public String getGlobalName() {
		return globalName;
	}
	public void setGlobalName(String globalName) {
		this.globalName = globalName;
	}
	public String getFieldName() {
		return fieldName;
	}
	public void setFieldName(String fieldName) {
		this.fieldName = fieldName;
	}
	public String getFieldValue() {
		return fieldValue;
	}
	public void setFieldValue(String fieldValue) {
		this.fieldValue = fieldValue;
		
		if(fieldValue != null) {
			for(String value: fieldValue.split(",")){
				this.valueList.add(value);
			}
		}
	}
	public boolean isEnabled() {
		return enabled;
	}
	public void setEnabled(boolean enabled) {
		this.enabled = enabled;
	}
	public boolean isRank() {
		return rank;
	}
	public void setRank(boolean rank) {
		this.rank = rank;
	}
	public void setFieldType(Integer fieldType) {
		this.fieldType = fieldType;
	}
	public Integer getFieldType() {
		return fieldType;
	}
	public void setOptions(String options) {
		this.options = options;
	}
	public String getOptions() {
		return options;
	}
	public void setValueList(List<String> valueList) {
		this.valueList = valueList;
		
		if(valueList != null){
			StringBuffer values = new StringBuffer();
			for(int i=0; i< valueList.size(); i++) {
				if( i < valueList.size()-1){
					values.append(valueList.get(i)+",");
				} else {
					values.append(valueList.get(i));
				}
			}
			setFieldValue(values.toString());
		} else { 
			setFieldValue(null);
		}
	}
	public List<String> getValueList() {
		return valueList;
	}
	
}

