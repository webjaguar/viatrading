package com.webjaguar.model.api;

import com.webjaguar.model.Product;

public class ApiTruckLoadProcessProduct {

	private Integer id;
	private String name;
	private ApiProduct apiProduct;
	private Double unitPrice = 0.00;
	private Double extMsrp;
	private String condition;
	private Integer no_Of_Units;
	private Integer quantity = 0;
	private String custom1;
	private String custom2;
	private String custom3;
	private String custom4;
	private String description;
	private String packing;
	private String numOfPallets;
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public ApiProduct getApiProduct() {
		return apiProduct;
	}
	public void setApiProduct(ApiProduct apiProduct) {
		this.apiProduct = apiProduct;
	}
	public Double getUnitPrice() {
		return unitPrice;
	}
	public void setUnitPrice(Double unitPrice) {
		this.unitPrice = unitPrice;
	}
	public Double getExtMsrp() {
		return extMsrp;
	}
	public void setExtMsrp(Double extMsrp) {
		this.extMsrp = extMsrp;
	}
	public String getCondition() {
		return condition;
	}
	public void setCondition(String condition) {
		this.condition = condition;
	}
	public Integer getNo_Of_Units() {
		return no_Of_Units;
	}
	public void setNo_Of_Units(Integer no_Of_Units) {
		this.no_Of_Units = no_Of_Units;
	}
	public Integer getQuantity() {
		return quantity;
	}
	public void setQuantity(Integer quantity) {
		this.quantity = quantity;
	}
	public String getCustom1() {
		return custom1;
	}
	public void setCustom1(String custom1) {
		this.custom1 = custom1;
	}
	public String getCustom2() {
		return custom2;
	}
	public void setCustom2(String custom2) {
		this.custom2 = custom2;
	}
	public String getCustom3() {
		return custom3;
	}
	public void setCustom3(String custom3) {
		this.custom3 = custom3;
	}
	public String getCustom4() {
		return custom4;
	}
	public void setCustom4(String custom4) {
		this.custom4 = custom4;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public String getPacking() {
		return packing;
	}
	public void setPacking(String packing) {
		this.packing = packing;
	}
	public String getNumOfPallets() {
		return numOfPallets;
	}
	public void setNumOfPallets(String numOfPallets) {
		this.numOfPallets = numOfPallets;
	}	
	
}
