/* Copyright 2011 Advanced E-Media Solutions
 *
 */

package com.webjaguar.model;

// ISO 639-1 Language Code
public class Language {
	
	private String languageCode;
	private Integer newRegistrationId;
	private Integer newOrderId;	
	
	public enum LanguageCode {en, fr, es, de, ar};
	
	public void setLanguageCode(String languageCode) {
		this.languageCode = languageCode;
	}
	public String getLanguageCode() {
		return languageCode;
	}
	public void setNewRegistrationId(Integer newRegistrationId) {
		this.newRegistrationId = newRegistrationId;
	}
	public Integer getNewRegistrationId() {
		return newRegistrationId;
	}
	public void setNewOrderId(Integer newOrderId) {
		this.newOrderId = newOrderId;
	}
	public Integer getNewOrderId() {
		return newOrderId;
	}	
}
