package com.webjaguar.model;

import java.sql.Timestamp;
import java.util.Date;

public class ShoppingListGroup
{
	private Integer id;
	private Integer userId;
	private String name;
	private Integer rank;
	private Date dateCreated;
	private Timestamp lastModified;
	
	public Integer getId()
	{
		return id;
	}
	public void setId(Integer id)
	{
		this.id = id;
	}
	public Integer getUserId()
	{
		return userId;
	}
	public void setUserId(Integer userId)
	{
		this.userId = userId;
	}
	public String getName()
	{
		return name;
	}
	public void setName(String name)
	{
		this.name = name;
	}
	public Integer getRank()
	{
		return rank;
	}
	public void setRank(Integer rank)
	{
		this.rank = rank;
	}
	public Date getDateCreated()
	{
		return dateCreated;
	}
	public void setDateCreated(Date dateCreated)
	{
		this.dateCreated = dateCreated;
	}
	public Timestamp getLastModified()
	{
		return lastModified;
	}
	public void setLastModified(Timestamp lastModified)
	{
		this.lastModified = lastModified;
	}

}
