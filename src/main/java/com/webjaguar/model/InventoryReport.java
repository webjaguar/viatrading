/*
 * Copyright 2010 Advanced E-Media Solutions
 * 
 */

package com.webjaguar.model;

public class InventoryReport
{

	private String sku;
	private String name;
	private Integer unitsPurchasedDelivered;
	private Integer unitsPurchasedInTransit;
	private Integer unitsSoldShipped;
	private Integer unitsSoldInProcess;
	private Integer unitsAdjusted;
	private Integer unitsOnHand;
	private Integer inventoryAFS;
	public Integer getinventoryAFS() {
		return inventoryAFS;
	}
	public void setinventoryAFS(Integer inventoryAFS) {
		this.inventoryAFS = inventoryAFS;
	}
	public String getSku() {
		return sku;
	}
	public void setSku(String sku) {
		this.sku = sku;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public Integer getUnitsOnHand() {
		return unitsOnHand;
	}
	public Integer getUnitsPurchasedDelivered() {
		return unitsPurchasedDelivered;
	}
	public void setUnitsPurchasedDelivered(Integer unitsPurchasedDelivered) {
		this.unitsPurchasedDelivered = unitsPurchasedDelivered;
	}
	public Integer getUnitsPurchasedInTransit() {
		return unitsPurchasedInTransit;
	}
	public void setUnitsPurchasedInTransit(Integer unitsPurchasedInTransit) {
		this.unitsPurchasedInTransit = unitsPurchasedInTransit;
	}
	public Integer getUnitsSoldShipped() {
		return unitsSoldShipped;
	}
	public void setUnitsSoldShipped(Integer unitsSoldShipped) {
		this.unitsSoldShipped = unitsSoldShipped;
	}
	public Integer getUnitsSoldInProcess() {
		return unitsSoldInProcess;
	}
	public void setUnitsSoldInProcess(Integer unitsSoldInProcess) {
		this.unitsSoldInProcess = unitsSoldInProcess;
	}
	public void setUnitsOnHand(Integer unitsOnHand) {
		this.unitsOnHand = unitsOnHand;
	}
	public void setUnitsAdjusted(Integer unitsAdjusted) {
		this.unitsAdjusted = unitsAdjusted;
	}
	public Integer getUnitsAdjusted() {
		return unitsAdjusted;
	}
	
}
