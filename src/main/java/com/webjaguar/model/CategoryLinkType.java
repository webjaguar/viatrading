/* Copyright 2006 Advanced E-Media Solutions
 *
 */

package com.webjaguar.model;

public enum CategoryLinkType {
	VISIBLE, NONLINK, HIDDEN;
}
