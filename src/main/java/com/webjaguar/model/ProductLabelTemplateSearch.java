/* Copyright 2006, 2009 Advanced E-Media Solutions
 *
 */

package com.webjaguar.model;

public class ProductLabelTemplateSearch extends Search {
	
	// active
	private String active;
	public String getActive() { return active; }
	public void setActive(String active) { this.active = active; }

}
