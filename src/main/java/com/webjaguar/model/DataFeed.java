/* Copyright 2007 Advanced E-Media Solutions
 * @author Eduardo Asprec
 * @since 10.04.2007
 */

package com.webjaguar.model;

public class DataFeed {

	private String feed;
	private String fieldName;
	private String methodName;
	private boolean enabled;
	private boolean required;
	private String description;
	private String defaultValue;
	private String excludeRule;
	
	public String getFeed()
	{
		return feed;
	}
	public void setFeed(String feed)
	{
		this.feed = feed;
	}    
	public String getFieldName()
	{
		return fieldName;
	}
	public void setFieldName(String fieldName)
	{
		this.fieldName = fieldName;
	}
	public String getMethodName()
	{
		return methodName;
	}
	public void setMethodName(String methodName)
	{
		this.methodName = methodName;
	}
	public boolean isEnabled()
	{
		return enabled;
	}
	public void setEnabled(boolean enabled)
	{
		this.enabled = enabled;
	}
	public boolean isRequired()
	{
		return required;
	}
	public void setRequired(boolean required)
	{
		this.required = required;
	}
	public String getDescription()
	{
		return description;
	}
	public void setDescription(String description)
	{
		this.description = description;
	}
	public String getDefaultValue()
	{
		return defaultValue;
	}
	public void setDefaultValue(String defaultValue)
	{
		this.defaultValue = defaultValue;
	}
	public String getExcludeRule() {
		return excludeRule;
	}
	public void setExcludeRule(String excludeRule) {
		this.excludeRule = excludeRule;
	}

}
