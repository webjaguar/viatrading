/* Copyright 2006, 2007 Advanced E-Media Solutions
 *
 */

package com.webjaguar.model;

public class CategorySearch {
	
	// sort
	private String sort = "name";
	public String getSort() { return sort; }
	public void setSort(String sort) { this.sort = sort; }	
	
	// parent ID
	private Integer parent;
	public Integer getParent() { return parent; }
	public void setParent(Integer parent) { this.parent = parent; }
	
	// page 
	private int page = 1;
	public int getPage() { return page; }
	public void setPage(int page) { this.page = page; }
	
	// page size
	private int pageSize = 10;
	public int getPageSize() { return pageSize; }
	public void setPageSize(int pageSize) { this.pageSize = pageSize; }	
	
	private Integer limit;
	public Integer getLimit() { return limit; }
	public void setLimit(Integer limit) { this.limit = limit; }
	
	private String setType;
	public String getSetType() { return setType; }
	public void setSetType(String setType) { this.setType = setType; }

	// offset
	private int offset;
	public int getOffset() { return offset; }
	public void setOffset(int offset) { this.offset = offset; }	
	
	// home page
	private boolean homePage;
	public boolean isHomePage() { return homePage; }
	public void setHomePage(boolean homePage) { this.homePage = homePage; }
	
	// multi store
	private boolean multiStore;
	public boolean isMultiStore() { return multiStore; }
	public void setMultiStore(boolean multiStore) { this.multiStore = multiStore; }
	
	private String protectedAccess = "0";
	public String getProtectedAccess() { return protectedAccess; }
	public void setProtectedAccess(String protectedAccess) { this.protectedAccess = protectedAccess; }
	
}
