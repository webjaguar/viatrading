/* Copyright 2006 Advanced E-Media Solutions
 * @author Li Qin
 * @since 10.10.2006
 */

package com.webjaguar.model;

public class ShippingMethod {
	
	private Integer id;
	private String carrier;
	private String shippingCode;
	private String shippingTitle;
	private boolean shippingActive;
	private Integer shippingRank;
	private Integer weatherTemp;
	private Double minPrice;
	private Double maxPrice;
	
    public Integer getWeatherTemp()
	{
		return weatherTemp;
	}
	public void setWeatherTemp(Integer weatherTemp)
	{
		this.weatherTemp = weatherTemp;
	}
	// getter and setter 
	public String getCarrier(){
		return carrier;
	}
	public void setCarrier(String carrier) {
		this.carrier = carrier;
	}
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public boolean getShippingActive() {
		return shippingActive;
	}
	public void setShippingActive(boolean shippingActive) {
		this.shippingActive = shippingActive;
	}
	public String getShippingCode() {
		return shippingCode;
	}
	public void setShippingCode(String shippingCode) {
		this.shippingCode = shippingCode;
	}
	public Integer getShippingRank() {
		return shippingRank;
	}
	public void setShippingRank(Integer shippingRank) {
		this.shippingRank = shippingRank;
	}
	public String getShippingTitle() {
		return shippingTitle;
	}
	public void setShippingTitle(String shippingTitle) {
		this.shippingTitle = shippingTitle;
	}
	public Double getMinPrice()
	{
		return minPrice;
	}
	public void setMinPrice(Double minPrice)
	{
		this.minPrice = minPrice;
	}
	public Double getMaxPrice()
	{
		return maxPrice;
	}
	public void setMaxPrice(Double maxPrice)
	{
		this.maxPrice = maxPrice;
	}
	
}
