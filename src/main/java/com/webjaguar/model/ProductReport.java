/*
 * Copyright 2008 Advanced E-Media Solutions
 * 
 */

package com.webjaguar.model;

import java.sql.Date;

public class ProductReport
{

	private String sku;
	private String name;
	private Integer id;
	private Integer orderId;
	private Customer customer;
	private Date orderDate;
	private String prodName;
	private Integer qtysold;
	private String status;
	private SalesRep salesRep;
	private Date shipped;
	private String shippingMethod;
	private String company;
	private Integer quantity;
	private Double unitPrice;
	private Double promoDiscount;
	private Integer caseContent;
	private int lineItemNum;
	private String host;
	private Double percentage;
	private String orderType;
	private String parentSku;
	private Double grossRev;
	private Integer priceCasepackQty;
	private Integer abandonedCarts;
	private String thumbnailURL;
	
	public String getHost()
	{
		return host;
	}
	public void setHost(String host)
	{
		this.host = host;
	}
	public String getSku()
	{
		return sku;
	}
	public void setSku(String sku)
	{
		this.sku = sku;
	}
	public Integer getOrderId()
	{
		return orderId;
	}
	public void setOrderId(Integer orderId)
	{
		this.orderId = orderId;
	}
	public Customer getCustomer()
	{
		return customer;
	}
	public void setCustomer(Customer customer)
	{
		this.customer = customer;
	}
	public Date getOrderDate()
	{
		return orderDate;
	}
	public void setOrderDate(Date orderDate)
	{
		this.orderDate = orderDate;
	}
	public String getStatus()
	{
		return status;
	}
	public void setStatus(String status)
	{
		this.status = status;
	}
	public SalesRep getSalesRep()
	{
		return salesRep;
	}
	public void setSalesRep(SalesRep salesRep)
	{
		this.salesRep = salesRep;
	}
	public String getShippingMethod()
	{
		return shippingMethod;
	}
	public void setShippingMethod(String shippingMethod)
	{
		this.shippingMethod = shippingMethod;
	}
	public String getCompany()
	{
		return company;
	}
	public void setCompany(String company)
	{
		this.company = company;
	}
	public Integer getQuantity()
	{
		return quantity;
	}
	public void setQuantity(Integer quantity)
	{
		this.quantity = quantity;
	}
	public Double getUnitPrice()
	{
		return unitPrice;
	}
	public void setUnitPrice(Double unitPrice)
	{
		this.unitPrice = unitPrice;
	}
	public Integer getCaseContent()
	{
		return caseContent;
	}
	public void setCaseContent(Integer caseContent)
	{
		this.caseContent = caseContent;
	}
	public int getLineItemNum()
	{
		return lineItemNum;
	}
	public void setLineItemNum(int lineItemNum)
	{
		this.lineItemNum = lineItemNum;
	}
	public Date getShipped()
	{
		return shipped;
	}
	public void setShipped(Date shipped)
	{
		this.shipped = shipped;
	}
	public Double getSubTotal() {
		if ( this.getQuantity() == null || this.getUnitPrice() == null) {
			return null;
		} else 
			return ( this.getQuantity() * ((this.caseContent == null) ? 1 : this.caseContent )) * this.getUnitPrice() - (((this.promoDiscount == null) ? 0.0 : this.promoDiscount) * this.getQuantity());
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public Double getPercentage() {
		return percentage;
	}
	public void setPercentage(Double percentage) {
		this.percentage = percentage;
	}
	public String getOrderType() {
		return orderType;
	}
	public void setOrderType(String orderType) {
		this.orderType = orderType;
	}
	public String getParentSku() {
		return parentSku;
	}
	public void setParentSku(String parentSku) {
		this.parentSku = parentSku;
	}
	public String getProdName() {
		return prodName;
	}
	public void setProdName(String prodName) {
		this.prodName = prodName;
	}
	public Integer getQtysold() {
		return qtysold;
	}
	public void setQtysold(Integer qtysold) {
		this.qtysold = qtysold;
	}
	public Double getGrossRev() {
		return grossRev;
	}
	public void setGrossRev(Double grossRev) {
		this.grossRev = grossRev;
	}
	public Integer getPriceCasepackQty() {
		return priceCasepackQty;
	}
	public void setPriceCasepackQty(Integer priceCasepackQty) {
		this.priceCasepackQty = priceCasepackQty;
	}
	public Integer getAbandonedCarts() {
		return abandonedCarts;
	}
	public void setAbandonedCarts(Integer abandonedCarts) {
		this.abandonedCarts = abandonedCarts;
	}
	public String getThumbnailURL() {
		return thumbnailURL;
	}
	public void setThumbnailURL(String thumbnailURL) {
		this.thumbnailURL = thumbnailURL;
	}
	public Double getPromoDiscount() {
		return promoDiscount;
	}
	public void setPromoDiscount(Double promoDiscount) {
		this.promoDiscount = promoDiscount;
	}
}
