/* Copyright 2006 Advanced E-Media Solutions
 *
 */

package com.webjaguar.model;

import java.util.ArrayList;
import java.util.List;

public class MyListSearch {
	
	// sort
	private String sort;
	public String getSort() { return sort; }
	public void setSort(String sort) { this.sort = sort; }	
	
	// user ID
	private Integer userId;
	public Integer getUserId() { return userId;	}
	public void setUserId(Integer userId) {	this.userId = userId; }

    // protected Access
	private String protectedAccess = "0";
	public String getProtectedAccess() { return protectedAccess; }
	public void setProtectedAccess(String protectedAccess) {
		this.protectedAccess = protectedAccess;
	}
	
	// group Id
	private int groupId = 0;
	public int getGroupId() { return groupId; }
	public void setGroupId(int groupId) { this.groupId = groupId; }

	
}
