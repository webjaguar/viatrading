/*
 * Copyright 2008 Advanced E-Media Solutions
 * 
 */

package com.webjaguar.model;

import java.util.Date;

import org.springframework.web.bind.ServletRequestUtils;

public class ReportFilter
{
	// sort
	private String sort = "username";
	public String getSort() { return sort; }
	public void setSort(String sort) { this.sort = sort; }
	
	// page 
	private int page = 1;
	public int getPage() { return page; }
	public void setPage(int page) { this.page = page; }
	
	// page size
	private int pageSize = 10;
	public int getPageSize() { return pageSize; }
	public void setPageSize(int pageSize) { this.pageSize = pageSize; }
	
	// firstname
	private String firstName;
	public String getFirstName() { return firstName; }
	public void setFirstName(String firstName) { this.firstName = firstName; }
	
	// lastname
	private String lastName;
	public String getLastName() { return lastName; }
	public void setLastName(String lastName) { this.lastName = lastName; }
	
	// state
	private String state;
	public String getState() { return state; }
	public void setState(String state) { this.state = state; }
	
	// country
	private String country;
	public String getCountry() { return country; }
	public void setCountry(String country) { this.country = country; }
	
	// phone
	private String phone;
	public String getPhone() { return phone; }
	public void setPhone(String phone) { this.phone = phone; }
	
	// salesRepId
	private Integer salesRepId = -1;
	public Integer getSalesRepId() { return salesRepId; }
	public void setSalesRepId(Integer salesRepId) { this.salesRepId = salesRepId; }
	
	// ppOrder
	private Integer ppOrder;
	public Integer getPpOrder() { return ppOrder; }
	public void setPpOrder(Integer ppOrder) { this.ppOrder = ppOrder; }
	
	// numOrderOperator
	private Integer numOrderOperator;
	public Integer getNumOrderOperator() { return numOrderOperator; }
	public void setNumOrderOperator(Integer numOrderOperator) { this.numOrderOperator = numOrderOperator; }
	
	// numOrder
	private Integer numOrder;
	public Integer getNumOrder() { return numOrder; }
	public void setNumOrder(Integer numOrder) { this.numOrder = numOrder; }
	
	// totalOrder
	private Double totalOrder;
	public Double getTotalOrder() { return totalOrder; }
	public void setTotalOrder(Double totalOrder) { this.totalOrder = totalOrder; }
	
	// totalOrderOperator
	private Integer totalOrderOperator;
	public Integer getTotalOrderOperator() { return totalOrderOperator; }
	public void setTotalOrderOperator(Integer totalOrderOperator) { this.totalOrderOperator = totalOrderOperator; }
	
	// email
	private String username;
	public String getUsername() { return username; }
	public void setUsername(String username) { this.username = username; }
	
	// startDate
	private Date startDate;
	public Date getStartDate() { return startDate; }
	public void setStartDate(Date startDate) { this.startDate = startDate; }
	
	// endDate
	private Date endDate;
	public Date getEndDate() { return endDate; }
	public void setEndDate(Date endDate) { this.endDate = endDate; }
	
	// created startDate
	private Date createdStartDate;
	public Date getCreatedStartDate() { return createdStartDate; }
	public void setCreatedStartDate(Date createdStartDate) { this.createdStartDate = createdStartDate; }
	
	// created endDate
	private Date createdEndDate;
	public Date getCreatedEndDate() { return createdEndDate; }
	public void setCreatedEndDate(Date createdEndDate) { this.createdEndDate = createdEndDate; }
	
	// quarter
	private String quarter = "1";
	public String getQuarter() { return quarter; }
	public void setQuarter(String quarter) { this.quarter = quarter; }
	
	private String year;
	public String getYear() { return year; }
	public void setYear(String year) { this.year = year; }
	
	private String month;
	public String getMonth() { return month; }
	public void setMonth(String month) { this.month = month; }
	
	// date type
	private String dateType = "orderDate";
	public String getDateType() { return dateType; }
	public void setDateType(String dateType) { this.dateType = dateType; }
	
	private String sku;
	public String getSku() { return sku; }
	public void setSku(String sku) { this.sku = sku; }
	
	private int skuOperator;
	public int getSkuOperator() { return skuOperator; }
	public void setSkuOperator(int skuOperator) { this.skuOperator = skuOperator; }
	
	// billToName
	private String billToName;
	public String getBillToName() { return billToName; }
	public void setBillToName(String billToName) { this.billToName = billToName; }
	
	// status
	private String status;
	public String getStatus() { return status; }
	public void setStatus(String status) { this.status = status; }
	
	// statusTemp
	private String statusTemp;
	public String getStatusTemp() { return statusTemp; }
	public void setStatusTemp(String statusTemp) { this.statusTemp = statusTemp; }
	
	// product field#
	private String productFieldNumber;
	public String getProductFieldNumber() { return productFieldNumber; }
	public void setProductFieldNumber(String productFieldNumber) { this.productFieldNumber = productFieldNumber; }

	// product field value
	private String productField;
	public String getProductField() { return productField; }
	public void setProductField(String productField) { this.productField = productField; }
	
	private Integer limit;
	public Integer getLimit() { return limit; }
	public void setLimit(Integer limit) { this.limit = limit; }

	// offset
	private int offset;
	public int getOffset() { return offset; }
	public void setOffset(int offset) { this.offset = offset; }	
	
	// customer field#
	private String customerFieldNumber;
	public String getCustomerFieldNumber() { return customerFieldNumber; }
	public void setCustomerFieldNumber(String customerFieldNumber) { this.customerFieldNumber = customerFieldNumber; }

	// customer field value
	private String customerField;
	public String getCustomerField() { return customerField; }
	public void setCustomerField(String customerField) { this.customerField = customerField; }

	// backendOrder
	private String backendOrder;
	public String getBackendOrder() { return backendOrder; }
	public void setBackendOrder(String backendOrder) { this.backendOrder = backendOrder; }
	
	// order type
	private String orderType;
	public String getOrderType() { return orderType; }
	public void setOrderType(String orderType) { this.orderType = orderType; }
	
	// shipping method
	private String shippingMethod;
	public String getShippingMethod() { return shippingMethod; }
	public void setShippingMethod(String shippingMethod) { this.shippingMethod = shippingMethod; }

	// type
	private String type;
	public String getType() { return type; }
	public void setType(String type) { this.type = type; }
	
	// group Id
	private Integer groupId;
	public Integer getGroupId() { return groupId; }
	public void setGroupId(Integer groupId) { this.groupId = groupId; }
	
	//lastOrderedDate
	private Date lastOrderedDate;
	public Date getLastOrderedDate() {return lastOrderedDate; }	
	public void setLastOrderedDate(Date lastOrderedDate ) {this.lastOrderedDate = lastOrderedDate;}
	
	//averageOrder
	private Double averageOrder;
	public Double getAverageOrder() { return averageOrder; }
	public void setAverageOrder(Double averageOrder) { this.averageOrder = averageOrder; }

	// averageOrderOperator
	private Integer averageOrderOperator;
	public Integer getAverageOrderOperator() { return averageOrderOperator; }
	public void setAverageOrderOperator(Integer averageOrderOperator) { this.averageOrderOperator = averageOrderOperator; }
	
	// rating1
	private String rating1;	
	public String getRating1() { return rating1; }
	public void setRating1(String rating1) { this.rating1 = rating1; }
	
	// rating2
	private String rating2;
	public String getRating2() { return rating2; }
	public void setRating2(String rating2) { this.rating2 = rating2; }
	
	private String parentSku;
	public String getParentSku() {
		return parentSku;
	}
	public void setParentSku(String parentSku) {
		this.parentSku = parentSku;
	}

	private String productName;
	public String getProductName() {
		return productName;
	}
	public void setProductName(String productName) {
		this.productName = productName;
	}
	
}
