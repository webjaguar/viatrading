package com.webjaguar.model;

import java.util.Map;
import java.util.Set;

public class ConditionShipping {
	
	private Set<Object> groupId;
	private String states;
	private Map<String, Map<Object,String>> condition;
	public Set<Object> getGroupId() {
		return groupId;
	}
	public void setGroupId(Set<Object> groupId) {
		this.groupId = groupId;
	}
	public String getStates() {
		return states;
	}
	public void setStates(String states) {
		this.states = states;
	}
	public Map<String, Map<Object, String>> getCondition() {
		return condition;
	}
	public void setCondition(Map<String, Map<Object, String>> condition) {
		this.condition = condition;
	}
	
}
