/* Copyright 2006 Advanced E-Media Solutions
 * @author Eduardo Asprec
 */

package com.webjaguar.model;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

public class CustomerSearch implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	// sort
	private String sort= "id desc";
	public String getSort() { return sort; }
	public void setSort(String sort) { this.sort = sort; }	
	
	// first Name
	private String firstName;
	public String getFirstName() { return firstName; }
	public void setFirstName(String firstName) { this.firstName = firstName; }
	
	// last Name
	private String lastName;
	public String getLastName()	{ return lastName; }
	public void setLastName(String lastName) { this.lastName = lastName; }
	
	// email
	private String email;
	public String getEmail() {	return email; }
	public void setEmail(String email)	{ this.email = email;	}
	
	// company name
	private String companyName;
	public String getCompanyName() { return companyName; }
	public void setCompanyName(String companyName) { this.companyName = companyName; }
	
	//multiple Search
	private String[] multipleSearch;
	public String[] getMultipleSearch() { return multipleSearch; }
	public void setMultipleSearch(String[] multipleSearch) { this.multipleSearch = multipleSearch; }
	
	private String multipleSearchString;
	public String getMultipleSearchString() { return multipleSearchString; }
	public void setMultipleSearchString(String multipleSearchString) { this.multipleSearchString = multipleSearchString; }
	
	// supplier
	private Integer supplier;
	public Integer getSupplier() { return supplier; }
	public void setSupplier(Integer supplier) { this.supplier = supplier; }
	
	// zipcode
	private String zipcode;
	public String getZipcode() { return zipcode; }
	public void setZipcode(String zipcode) { this.zipcode = zipcode; }
	
	// location
	private LocationSearch locationSearch;
	public LocationSearch getLocationSearch() { return locationSearch; }
	public void setLocationSearch(LocationSearch locationSearch) { this.locationSearch = locationSearch; }	
	
	// phone
	private String phone;
	public String getPhone() { return phone; }
	public void setPhone(String phone) { this.phone = phone; }
	
	// phone
		private String cellPhone;
		public String getCellPhone() { return cellPhone; }
		public void setCellPhone(String cellPhone) { this.cellPhone = cellPhone; }
	
	// city
	private String city;
	public String getCity() { return city; }
	public void setCity(String city) { this.city = city; }	
	
	//state
	private String state;
	public String getState() { return state; }
	public void setState(String state) { this.state = state; }
	
	// country
	private String country;
	public String getCountry() { return country; }
	public void setCountry(String country) { this.country = country; }
	
	// region
	private String region;
	public String getRegion() { return region; }
	public void setRegion(String region) { this.region = region; }
	
	// time zone
	private String timeZone;
	public String getTimeZone() { return timeZone;	}
	public void setTimeZone(String timeZone) { this.timeZone = timeZone; }
	//nationalRegion
	private String nationalRegion;
	public String getNationalRegion() {
		return nationalRegion;
	}
	public void setNationalRegion(String nationalRegion) {
		this.nationalRegion = nationalRegion;
	}
	// affiliate
	private Integer affiliate = -1;
	public Integer getAffiliate() {	return affiliate; }
	public void setAffiliate(Integer affiliate)	{ this.affiliate = affiliate; }
	
	// page 
	private int page = 1;
	public int getPage() { return page; }
	public void setPage(int page) { this.page = page; }
	
	// page size
	private int pageSize = 10;
	public int getPageSize() { return pageSize; }
	public void setPageSize(int pageSize) { this.pageSize = pageSize; }
	
	// parent ID
	private Integer parent;
	public Integer getParent() { return parent; }
	public void setParent(Integer parent) { this.parent = parent; }
	
	// account number
	private String accountNumber;
	public String getAccountNumber() { return accountNumber; }
	public void setAccountNumber(String accountNumber) { this.accountNumber = accountNumber; } 
	
	// sales rep
	private Integer salesRepId = -1;
	public Integer getSalesRepId() { return salesRepId; }
	public void setSalesRepId(Integer salesRepId) { this.salesRepId = salesRepId; }
	
	//accessUserId
	private Integer accessUserId;
	public Integer getAccessUserId() { return accessUserId; }
	public void setAccessUserId(Integer accessUserId) { this.accessUserId = accessUserId; }
	
	// qualifier
	private Integer qualifierId = -1;
	public Integer getQualifierId() { return qualifierId; }
	public void setQualifierId(Integer qualifierId) { this.qualifierId = qualifierId; }
	
	private String unsubscribe;
	public String getUnsubscribe() { return unsubscribe; }
	public void setUnsubscribe(String unsubscribe) { this.unsubscribe = unsubscribe; }
	
	private Integer limit;
	public Integer getLimit() { return limit; }
	public void setLimit(Integer limit) { this.limit = limit; }
	
	// offset
	private int offset;
	public int getOffset() { return offset; }
	public void setOffset(int offset) { this.offset = offset; }	
	
	// category ID
	private Integer category;
	public Integer getCategory() { return category; }
	public void setCategory(Integer category) { this.category = category; }
	
	private List<Integer> categoryIds;
	public List<Integer> getCategoryIds() { return categoryIds; }
	public void setCategoryIds(List<Integer> categoryIds) { this.categoryIds = categoryIds; }
	
	private String setType;
	public String getSetType() { return setType; }
	public void setSetType(String setType) { this.setType = setType; }
	
	private String ajaxSelect;
	public String getAjaxSelect() { return ajaxSelect; }
	public void setAjaxSelect(String ajaxSelect) { this.ajaxSelect = ajaxSelect; }
	
	// export file type
	private String fileType = "xls";
	public String getFileType() { return fileType; }
	public void setFileType(String fileType) { this.fileType = fileType; }
	
	// start date
	private Date startDate; 
	public Date getStartDate() { return startDate; }
	public void setStartDate(Date startDate) { this.startDate = startDate; }
	
	// end date
	private Date endDate;
	public Date getEndDate() { return endDate; }
	public void setEndDate(Date endDate) { this.endDate = endDate; }
	
	// customer field#
	private String customerFieldNumber;
	public String getCustomerFieldNumber() { return customerFieldNumber; }
	public void setCustomerFieldNumber(String customerFieldNumber) { this.customerFieldNumber = customerFieldNumber; }

	// customer field value
	private String customerField;
	public String getCustomerField() { return customerField; }
	public void setCustomerField(String customerField) { this.customerField = customerField; }
	
	// group Id
	private Integer groupId;
	public Integer getGroupId() { return groupId; }
	public void setGroupId(Integer groupId) { this.groupId = groupId; }
	
	// suspended
	private Boolean suspended;
	public void setSuspended(Boolean suspended) { this.suspended = suspended; }
	public Boolean getSuspended() { return suspended; }
	
	// trackcode
	private String trackcode;
	public String getTrackcode() { return trackcode; }
	public void setTrackcode(String trackcode) { this.trackcode = trackcode; }
	
	// trackcodeFieldDropDown
	private String trackcodeFieldDropDown;
	public String getTrackcodeFieldDropDown() { return trackcodeFieldDropDown; }
	public void setTrackcodeFieldDropDown(String trackcodeFieldDropDown) { this.trackcodeFieldDropDown = trackcodeFieldDropDown; }
	
	// registeredBy
	private String registeredBy;
	public String getRegisteredBy() { return registeredBy; }
	public void setRegisteredBy(String registeredBy) { this.registeredBy = registeredBy; }
	
	// Card ID
	private String cardId;
	public String getCardId() { return cardId; }
	public void setCardId(String cardId) { this.cardId = cardId; }

	// cart not empty
	private Boolean cartNotEmpty;
	public Boolean getCartNotEmpty() { return cartNotEmpty; }
	public void setCartNotEmpty(Boolean cartNotEmpty) { this.cartNotEmpty = cartNotEmpty; }

	// cart start date
	private Date cartStartDate; 
	public Date getCartStartDate() { return cartStartDate; }
	public void setCartStartDate(Date cartStartDate) { this.cartStartDate = cartStartDate; }
	
	// end date
	private Date cartEndDate;
	public Date getCartEndDate() { return cartEndDate; }
	public void setCartEndDate(Date cartEndDate) { this.cartEndDate = cartEndDate; }
	
	// login start date
	private Date loginStartDate;
	public Date getLoginStartDate() { return loginStartDate; }
	public void setLoginStartDate(Date loginStartDate) { this.loginStartDate = loginStartDate; }
	
	// login end date
	private Date loginEndDate;
	public Date getLoginEndDate() { return loginEndDate; }
	public void setLoginEndDate(Date loginEndDate) { this.loginEndDate = loginEndDate; }

	// host
	private String host = "";
	public String getHost() { return host; }
	public void setHost(String host) { this.host = host; }	
	
	// taxId
	private String taxId;
	public String getTaxId() { return taxId; }
	public void setTaxId(String taxId) { this.taxId = taxId; }
	
	private int totalOrderOperator;
	public int getTotalOrderOperator() { return totalOrderOperator; }
	public void setTotalOrderOperator(int totalOrderOperator) { this.totalOrderOperator = totalOrderOperator; }
	
	private Double totalOrder;
	public Double getTotalOrder() { return totalOrder; }
	public void setTotalOrder(Double totalOrder) { this.totalOrder = totalOrder; }
	
	private int numOrderOperator;
	public int getNumOrderOperator() { return numOrderOperator; }
	public void setNumOrderOperator(int numOrderOperator) { this.numOrderOperator = numOrderOperator; }
	
	private Integer numOrder;
	public Integer getNumOrder() { return numOrder; }
	public void setNumOrder(Integer numOrder) { this.numOrder = numOrder; }
	
	private boolean baseOnOrder=false;
	public boolean getBaseOnOrder() { return baseOnOrder; }
	public void setBaseOnOrder(boolean baseOnOrder) { this.baseOnOrder = baseOnOrder; }
	
	private Double shoppingCartTotal;
	public Double getShoppingCartTotal() { return shoppingCartTotal; }
	public void setShoppingCartTotal(Double shoppingCartTotal) { this.shoppingCartTotal = shoppingCartTotal; }
	
	private String languageCode;
	public String getLanguageCode() { return languageCode; }
	public void setLanguageCode(String languageCode) { this.languageCode = languageCode; }
	
	private String rating1;
	public String getRating1() { return rating1; }
	public void setRating1(String rating1) { this.rating1 = rating1; }
	
	private String rating2;
	public String getRating2() { return rating2; }
	public void setRating2(String rating2) { this.rating2 = rating2; }
	
	private String loggedInFlag;
	public String getLoggedInFlag() { return loggedInFlag; }
	public void setLoggedInFlag(String loggedInFlag) { this.loggedInFlag = loggedInFlag; }

	private List<Integer> loggedIn;
	public List<Integer> getLoggedIn() { return loggedIn; }
	public void setLoggedIn(List<Integer> loggedIn) { this.loggedIn = loggedIn; }
	
	private String supplierPrefix;
	public String getSupplierPrefix() { return supplierPrefix; }
	public void setSupplierPrefix(String supplierPrefix) { this.supplierPrefix = supplierPrefix; }
	
	// paid
	private Integer paidDropDown;
	public Integer getPaidDropDown() { return paidDropDown; }
	public void setPaidDropDown(Integer paidDropDown) {	this.paidDropDown = paidDropDown; }
		
	private String paid;
	public String getPaid() { return paid; }
	public void setPaid(String paid) { this.paid = paid; }
	
	//languageField
	private String languageField;
	public String getLanguageField() { return languageField; }
	public void setLanguageField(String languageField) { this.languageField = languageField; }
	
	private Integer languageFieldDropDown;
	public Integer getLanguageFieldDropDown() { return languageFieldDropDown; }
	public void setLanguageFieldDropDown(Integer languageFieldDropDown) { this.languageFieldDropDown = languageFieldDropDown; }
	
	//medium
	private String medium;
	public String getMedium() { return medium; }
	public void setMedium(String medium) { this.medium = medium; }
	
	private Integer mediumDropDown;
	public Integer getMediumDropDown() { return mediumDropDown; }
	public void setMediumDropDown(Integer mediumDropDown) { this.mediumDropDown = mediumDropDown; }
	
	//mainSource
	private String mainSource;
	public String getMainSource() { return mainSource; }
	public void setMainSource(String mainSource) { this.mainSource = mainSource; }
	
	private Integer mainSourceDropDown;
	public Integer getMainSourceDropDown() { return mainSourceDropDown; }
	public void setMainSourceDropDown(Integer mainSourceDropDown) { this.mainSourceDropDown = mainSourceDropDown; }

	//text message 
	private Integer textMessage;
	public Integer getTextMessage() { return textMessage; }
	public void setTextMessage(Integer textMessage) { this.textMessage = textMessage; }
		
	private String qualifier;
	public String getQualifier() { return qualifier; }
	public void setQualifier(String qualifier) { this.qualifier = qualifier; }
	
	private Integer accountManager;
	public Integer getAccountManager() {return accountManager;}
	public void setAccountManager(Integer accountManager) {this.accountManager = accountManager;}
	
}
