package com.webjaguar.model;

import java.util.Date;

public class BuyRequestVersion
{
	private int id;
	private int buyRequestId;
	private String dialog;
	private Date created;
	private int ownerId;
	private Integer userId;
	private String userName;
	public String getUserName()
	{
		return userName;
	}
	public void setUserName(String userName)
	{
		this.userName = userName;
	}
	public int getId()
	{
		return id;
	}
	public void setId(int id)
	{
		this.id = id;
	}
	public int getBuyRequestId()
	{
		return buyRequestId;
	}
	public void setBuyRequestId(int buyRequestId)
	{
		this.buyRequestId = buyRequestId;
	}
	public String getDialog()
	{
		return dialog;
	}
	public void setDialog(String dialog)
	{
		this.dialog = dialog;
	}
	public Date getCreated()
	{
		return created;
	}
	public void setCreated(Date created)
	{
		this.created = created;
	}
	public int getOwnerId()
	{
		return ownerId;
	}
	public void setOwnerId(int ownerId)
	{
		this.ownerId = ownerId;
	}
	public Integer getUserId()
	{
		return userId;
	}
	public void setUserId(Integer userId)
	{
		this.userId = userId;
	}
	
}
