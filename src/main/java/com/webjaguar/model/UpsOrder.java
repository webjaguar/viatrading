package com.webjaguar.model;

import java.util.Date;

public class UpsOrder {
	
	private int orderId;
	private String addedBy;
	private String accountNumber;
	private String companyName;
	private Date orderShipDate;
	private String carrierPhone;
	private String customerPO;
	private String customerPO2;
	private String trackingNumber;
	private String addr1;
	private String city;
	private String stateProvince;
	private Double weight;
	private int numberPackages;
	private String lineItems;	
	private String shippingCompany;
	private String truckCompany;
	
	
	public int getOrderId() {
		return orderId;
	}
	public void setOrderId(int orderId) {
		this.orderId = orderId;
	}
	public String getAddedBy() {
		return addedBy;
	}
	public void setAddedBy(String addedBy) {
		this.addedBy = addedBy;
	}
	public String getAccountNumber() {
		return accountNumber;
	}
	public void setAccountNumber(String accountNumber) {
		this.accountNumber = accountNumber;
	}
	public String getCompanyName() {
		return companyName;
	}
	public void setCompanyName(String companyName) {
		this.companyName = companyName;
	}
	public Date getOrderShipDate() {
		return orderShipDate;
	}
	public void setOrderShipDate(Date orderShipDate) {
		this.orderShipDate = orderShipDate;
	}
	public String getCustomerPO() {
		return customerPO;
	}
	public void setCustomerPO(String customerPO) {
		this.customerPO = customerPO;
	}
	public String getCustomerPO2() {
		return customerPO2;
	}
	public void setCustomerPO2(String customerPO2) {
		this.customerPO2 = customerPO2;
	}
	public String getTrackingNumber() {
		return trackingNumber;
	}
	public void setTrackingNumber(String trackingNumber) {
		this.trackingNumber = trackingNumber;
	}
	public String getAddr1() {
		return addr1;
	}
	public void setAddr1(String addr1) {
		this.addr1 = addr1;
	}
	public String getCity() {
		return city;
	}
	public void setCity(String city) {
		this.city = city;
	}
	public String getStateProvince() {
		return stateProvince;
	}
	public void setStateProvince(String stateProvince) {
		this.stateProvince = stateProvince;
	}
	public Double getWeight() {
		return weight;
	}
	public void setWeight(Double weight) {
		this.weight = weight;
	}
	public int getNumberPackages() {
		return numberPackages;
	}
	public void setNumberPackages(int numberPackages) {
		this.numberPackages = numberPackages;
	}
	public String getShippingCompany() {
		return shippingCompany;
	}
	public void setShippingCompany(String shippingCompany) {
		this.shippingCompany = shippingCompany;
	}
	public void setLineItems(String lineItems) {
		this.lineItems = lineItems;
	}
	public String getLineItems() {
		return lineItems;
	}
	public void setCarrierPhone(String carrierPhone) {
		this.carrierPhone = carrierPhone;
	}
	public String getCarrierPhone() {
		return carrierPhone;
	}
	public String getTruckCompany() {
		return truckCompany;
	}
	public void setTruckCompany(String truckCompany) {
		this.truckCompany = truckCompany;
	}
}
