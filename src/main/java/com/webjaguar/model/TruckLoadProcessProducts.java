package com.webjaguar.model;

import com.webjaguar.model.api.ApiProduct;
import com.webjaguar.model.api.ApiTruckLoadProcessProduct;

public class TruckLoadProcessProducts {
	private Integer id;
	private String name;
	private Product product;
	private Double unitPrice = 0.00;
	private Double extMsrp;
	private String condition;
	private Integer no_Of_Units;
	private Integer quantity = 0;
	private String custom1;
	private String custom2;
	private String custom3;
	private String custom4;
	private String description;
	private String packing;
	private String numOfPallets;
	
	public TruckLoadProcessProducts(ApiTruckLoadProcessProduct apiTruckLoadProcessProduct) {		
		this.id = apiTruckLoadProcessProduct.getId();
		this.name = apiTruckLoadProcessProduct.getName();
		this.product = new Product(apiTruckLoadProcessProduct.getApiProduct());
		this.unitPrice = apiTruckLoadProcessProduct.getUnitPrice();
		this.extMsrp = apiTruckLoadProcessProduct.getExtMsrp();
		this.condition = apiTruckLoadProcessProduct.getCondition();
		this.no_Of_Units = apiTruckLoadProcessProduct.getNo_Of_Units();
		this.quantity = apiTruckLoadProcessProduct.getQuantity();
		this.custom1 = apiTruckLoadProcessProduct.getCustom1();
		this.custom2 = apiTruckLoadProcessProduct.getCustom2();
		this.custom3 = apiTruckLoadProcessProduct.getCustom3();
		this.custom4 = apiTruckLoadProcessProduct.getCustom4();
		this.description = apiTruckLoadProcessProduct.getDescription();
		this.packing = apiTruckLoadProcessProduct.getPacking();
		this.numOfPallets = apiTruckLoadProcessProduct.getNumOfPallets();					
	}
	
	public TruckLoadProcessProducts() {}	
	
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public Product getProduct() {
		return product;
	}
	public void setProduct(Product product) {
		this.product = product;
	}
	public Double getUnitPrice() {
		return unitPrice;
	}
	public void setUnitPrice(Double unitPrice) {
		this.unitPrice = unitPrice;
	}
	public Double getExtMsrp() {
		return extMsrp;
	}
	public void setExtMsrp(Double extMsrp) {
		this.extMsrp = extMsrp;
	}
	public Integer getQuantity() {
		return quantity;
	}
	public void setQuantity(Integer quantity) {
		this.quantity = quantity;
	}
	public String getCustom1() {
		return custom1;
	}
	public void setCustom1(String custom1) {
		this.custom1 = custom1;
	}
	public String getCustom2() {
		return custom2;
	}
	public void setCustom2(String custom2) {
		this.custom2 = custom2;
	}
	public String getCustom3() {
		return custom3;
	}
	public void setCustom3(String custom3) {
		this.custom3 = custom3;
	}
	public String getCustom4() {
		return custom4;
	}
	public void setCustom4(String custom4) {
		this.custom4 = custom4;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public String getCondition() {
		return condition;
	}
	public void setCondition(String condition) {
		this.condition = condition;
	}
	public Integer getNo_Of_Units() {
		return no_Of_Units;
	}
	public void setNo_Of_Units(Integer no_of_Units) {
		this.no_Of_Units = no_of_Units;
	}
	public String getPacking() {
		return packing;
	}
	public void setPacking(String packing) {
		this.packing = packing;
	}
	public String getNumOfPallets() {
		return numOfPallets;
	}
	public void setNumOfPallets(String numOfPallets) {
		this.numOfPallets = numOfPallets;
	}

}
