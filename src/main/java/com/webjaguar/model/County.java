/* Copyright 2005, 2007 Advanced E-Media Solutions
 *
 */
package com.webjaguar.model;

public class County
{
	private String country;
	private String state;
	private String county;
	private Double taxRate;
	
	// Constructor
	public County( String country, String state, String county )
	{
		super();
		this.country = country;
		this.state = state;
		this.county = county;
	}
	public County()
	{
		super();
	}
	
	public String getCountry()
	{
		return country;
	}
	public void setCountry(String country)
	{
		this.country = country;
	}
	public String getState()
	{
		return state;
	}
	public void setState(String state)
	{
		this.state = state;
	}
	public String getCounty()
	{
		return county;
	}
	public void setCounty(String county)
	{
		this.county = county;
	}
	public Double getTaxRate()
	{
		return taxRate;
	}
	public void setTaxRate(Double taxRate)
	{
		this.taxRate = taxRate;
	}
	
	

}
