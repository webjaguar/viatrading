/* Copyright 2005, 2006 Advanced E-Media Solutions
 *
 */

package com.webjaguar.model;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.HashSet;
import java.util.Set;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlSeeAlso;
import javax.xml.bind.annotation.XmlTransient;
@XmlRootElement(name = "supplier")
@XmlAccessorType(XmlAccessType.FIELD)
@XmlSeeAlso({Address.class})

public class Supplier implements Serializable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	@XmlAttribute
	private Integer id;
	@XmlTransient
	private Integer userId;
	@XmlTransient
	private Double price;
	@XmlTransient
	private boolean percent;
	@XmlTransient
	private String sku;
	@XmlTransient
	private String supplierSku;
	@XmlTransient
	private Timestamp lastModified;
	private Address address;
	private String AccountNumber;
	private String note;
	@XmlTransient
	private Set catIds = new HashSet();
	private boolean newSupplier;	// use for export
	private boolean endQtyPricing;
	private Integer markupType;
	private Integer markupFormula;
	private Double markup1;
	private Double markup2;
	private Double markup3;
	private Double markup4;
	private Double markup5;
	private Double markup6;
	private Double markup7;
	private Double markup8;
	private Double markup9;
	private Double markup10;
	private String supplierPrefix;
	private boolean active = true;	
	@XmlTransient
	private boolean primary;
	@XmlTransient
	private String group;
	@XmlTransient
	private String dba;
	private String paymentPreference;
	private String mailCheckTo;
	private String transferMoneyTo;

	public Integer getId() { return id; }
	public void setId(Integer id) {
		this.id = id;
	}

	public Integer getUserId() { return userId; }
	public void setUserId(Integer userId) {
		this.userId = userId;
	}

	public Address getAddress() { return address; }
	public void setAddress(Address address) {
		this.address = address;
	}
	
	public Set getCatIds() { return catIds; }
	public void setCatIds(Set catIds) {
		this.catIds = catIds;
	}

	public Double getPrice() {
		return price;
	}

	public void setPrice(Double price) {
		this.price = price;
	}

	public Timestamp getLastModified() {
		return lastModified;
	}

	public void setLastModified(Timestamp lastModified) {
		this.lastModified = lastModified;
	}

	public String getSupplierSku() {
		return supplierSku;
	}

	public void setSupplierSku(String supplierSku) {
		this.supplierSku = supplierSku;
	}
	
	public String getFullAddressToTextArea() {
		StringBuffer sbuff = new StringBuffer();
		if (address != null) {
			sbuff.append(address.getCompany() + "\n\n" + address.getAddr1() + " " + address.getAddr2() + "\n" + address.getCity() + " " + 
					address.getStateProvince() + " " + address.getZip() + " " + address.getCountry() + 
					"\nTel: " + address.getPhone() + "\nFax: " + address.getFax() + "\n" + address.getEmail());
		}
		return sbuff.toString();
	}

	public String getAccountNumber() {
		return AccountNumber;
	}

	public void setAccountNumber(String accountNumber) {
		AccountNumber = accountNumber;
	}

	public String getNote() {
		return note;
	}

	public void setNote(String note) {
		this.note = note;
	}

	public String getSku() {
		return sku;
	}

	public void setSku(String sku) {
		this.sku = sku;
	}

	public boolean isNewSupplier() {
		return newSupplier;
	}

	public void setNewSupplier(boolean newSupplier) {
		this.newSupplier = newSupplier;
	}
	
	public boolean isEndQtyPricing() {
		return endQtyPricing;
	}
	
	public void setEndQtyPricing(boolean endQtyPricing) {
		this.endQtyPricing = endQtyPricing;
	}
	
	public void setMarkupType(Integer markupType) {
		this.markupType = markupType;
	}
	
	public Integer getMarkupType() {
		return markupType;
	}
	
	public Double getMarkup1() {
		return markup1;
	}
	
	public void setMarkup1(Double markup1) {
		this.markup1 = markup1;
	}
	
	public Double getMarkup2() {
		return markup2;
	}
	
	public void setMarkup2(Double markup2) {
		this.markup2 = markup2;
	}
	
	public Double getMarkup3() {
		return markup3;
	}
	
	public void setMarkup3(Double markup3) {
		this.markup3 = markup3;
	}
	
	public Double getMarkup4() {
		return markup4;
	}
	
	public void setMarkup4(Double markup4) {
		this.markup4 = markup4;
	}
	
	public Double getMarkup5() {
		return markup5;
	}
	
	public void setMarkup5(Double markup5) {
		this.markup5 = markup5;
	}
	
	public Double getMarkup6() {
		return markup6;
	}
	
	public void setMarkup6(Double markup6) {
		this.markup6 = markup6;
	}
	
	public Double getMarkup7() {
		return markup7;
	}
	
	public void setMarkup7(Double markup7) {
		this.markup7 = markup7;
	}
	
	public Double getMarkup8() {
		return markup8;
	}
	
	public void setMarkup8(Double markup8) {
		this.markup8 = markup8;
	}
	
	public Double getMarkup9() {
		return markup9;
	}
	
	public void setMarkup9(Double markup9) {
		this.markup9 = markup9;
	}
	
	public Double getMarkup10() {
		return markup10;
	}
	
	public void setMarkup10(Double markup10) {
		this.markup10 = markup10;
	}
	public boolean isPercent() {
		return percent;
	}
	public void setPercent(boolean percent) {
		this.percent = percent;
	}
	public String getSupplierPrefix() {
		return supplierPrefix;
	}
	public void setSupplierPrefix(String supplierPrefix) {
		this.supplierPrefix = supplierPrefix;
	}
	public void setActive(boolean active) {
		this.active = active;
	}
	public boolean isActive() {
		return active;
	}
	public boolean isPrimary() {
		return primary;
	}
	public void setPrimary(boolean primary) {
		this.primary = primary;
	}
	public void setMarkupFormula(Integer markupFormula) {
		this.markupFormula = markupFormula;
	}
	public Integer getMarkupFormula() {
		return markupFormula;
	}
	public String getGroup() {
		return group;
	}
	public void setGroup(String group) {
		this.group = group;
	}
	public String getDba() {
		return dba;
	}
	public void setDba(String dba) {
		this.dba = dba;
	}
	public String getPaymentPreference() {
		return paymentPreference;
	}
	public void setPaymentPreference(String paymentPreference) {
		this.paymentPreference = paymentPreference;
	}
	public String getMailCheckTo() {
		return mailCheckTo;
	}
	public void setMailCheckTo(String mailCheckTo) {
		this.mailCheckTo = mailCheckTo;
	}
	public String getTransferMoneyTo() {
		return transferMoneyTo;
	}
	public void setTransferMoneyTo(String transferMoneyTo) {
		this.transferMoneyTo = transferMoneyTo;
	}
}