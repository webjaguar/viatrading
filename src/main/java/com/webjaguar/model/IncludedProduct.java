package com.webjaguar.model;

import java.io.Serializable;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlSeeAlso;

import org.codehaus.jackson.map.annotate.JsonSerialize;
import org.codehaus.jackson.map.annotate.JsonSerialize.Inclusion;

@XmlRootElement(name = "product")
@XmlAccessorType(XmlAccessType.FIELD)
@XmlSeeAlso({IncludedProductXML.class})
@JsonSerialize(include=Inclusion.NON_NULL)
public class IncludedProduct extends Product implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	@XmlElement(name = "attachment", required = false)
	private boolean attachment;
	@XmlElement(name = "oneTimePrice", required = false)
	private boolean oneTimePrice;
	@XmlElement(name = "qtyMultiplier", required = false)
	private int qtyMultiplier = 1;
	@XmlAttribute(name = "type", required = false)
	private String type;
	
	public IncludedProduct() {
		super();
	}
	public IncludedProduct(String sku) {
		super(sku, null);
	}
	public boolean getAttachment() {
		return attachment;
	}
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	public static long getSerialversionuid() {
		return serialVersionUID;
	}
	public void setAttachment(boolean attachment) {
		this.attachment = attachment;
	}
	public boolean getOneTimePrice() {
		return oneTimePrice;
	}
	public void setOneTimePrice(boolean oneTimePrice) {
		this.oneTimePrice = oneTimePrice;
	}
	public int getQtyMultiplier() {
		return qtyMultiplier;
	}
	public void setQtyMultiplier(int qtyMultiplier) {
		this.qtyMultiplier = qtyMultiplier;
	}
}