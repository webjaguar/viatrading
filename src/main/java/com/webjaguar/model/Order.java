/*
 * Copyright 2006 Advanced E-Media Solutions
 * 
 */

package com.webjaguar.model;

import java.io.Serializable;
import java.math.BigDecimal;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import com.webjaguar.model.Language.LanguageCode;
import com.webjaguar.thirdparty.linkShare.LinkShare;
import com.webjaguar.thirdparty.payment.amazon.Iopn;
import com.webjaguar.thirdparty.payment.bankaudi.BankAudi;
import com.webjaguar.thirdparty.payment.ebillme.EBillme;
import com.webjaguar.thirdparty.payment.geMoney.GEMoney;
import com.webjaguar.thirdparty.payment.netcommerce.NetCommerce;

import com.webjaguar.web.domain.Utilities;

public class Order implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private Integer orderId;
	private String externalOrderId;
	private Integer userId;
	private String host;
	private String username;
	private Address billing;
	private Address shipping;
	private Address source;
	private CreditCard creditCard;
	private Paypal paypal;
	private NetCommerce netCommerce;
	private EBillme eBillme;
	private GoogleCheckOut googleCheckOut;
	private Iopn amazonIopn;
	private BankAudi bankAudi;
	private GEMoney geMoney;
	private LinkShare linkShare;
	private String shippingCarrier;
	private String shippingMethod;
	private String shippingMessage;
	private Integer customShippingId;
	private Double shippingCost;
	private String paymentMethod;
	private Double taxRate;
	private Double tax;
	private boolean taxExemption;
	private boolean taxOnShipping;
	private Double subTotal;
	private Double grandTotal;
	private Double ccFeeRate;
	private Double ccFee;
	private boolean ccFeePercent = true;
	private Double totalWeight;
	private int totalQuantity;
	private List<LineItem> lineItems = new ArrayList<LineItem>();
	private Promo promo;
	private String promoCode;
	private Double promoAmount = 0.0;
	private Double lineItemPromoAmount = 0.0;
	private Map<String, Double> lineItemPromos = new HashMap<String, Double>();
	private Double creditUsed;
	private GiftCard giftCard;
	private boolean firstOrder;
	// weather channel
	private boolean temperatureEnable;
	private int lowestTemp = Integer.MAX_VALUE;
	private Weather weather;
	// dates
	private Timestamp dateOrdered;
	private Timestamp lastModified;
	private Timestamp statusChangeDate;
	private Date shipped;
	// dueDate not saved in database
	private Date dueDate;
	private String dueDateDay;
	private int turnOverday;
	private Integer shippingPeriod;
	// customer prefer dueDate
	private Date userDueDate;
	private Date agreedPaymentDate;
	private Date agreedPickUpDate;
	private String trackUrlApps;
	
	private Double totalProductWeight;
	
	private String cancelReason;

	public String getCancelReason() {
		return cancelReason;
	}

	public void setCancelReason(String cancelReason) {
		this.cancelReason = cancelReason;
	}

	public Double getTotalProductWeight() {
		return totalProductWeight;
	}

	public void setTotalProductWeight(Double totalProductWeight) {
		this.totalProductWeight = totalProductWeight;
	}

	public Date getAgreedPaymentDate() {
		return agreedPaymentDate;
	}

	public void setAgreedPaymentDate(Date agreedPaymentDate) {
		this.agreedPaymentDate = agreedPaymentDate;
	}

	public Date getAgreedPickUpDate() {
		return agreedPickUpDate;
	}

	public void setAgreedPickUpDate(Date agreedPickUpDate) {
		this.agreedPickUpDate = agreedPickUpDate;
	}

	private Date requestedCancelDate;
	private String trackcode;
	private Integer qualifier;
	private String invoiceNote = "";
	// Sales Rep
	private Integer salesRepId;
	private Integer salesRepProcessedById;
	// order status
	private String status;
	private String subStatus;
	private List<OrderStatus> statusHistory;
	// deleted line items
	private List<LineItem> deletedLineItems;
	// inserted line items
	private List<LineItem> insertedLineItems;
	// affiliate
	private Affiliate affiliate;
	// Prospect Id (SearchEngine Prospect)
	private String prospectId;
	
	// payment
	private Double amountPaid;
	private Double payment;
	private List<Payment> paymentHistory;
	private List<Payment> payments;
	
	private String purchaseOrder;
	private String ipAddress;
	// work order
	private Integer workOrderNum;
	// PDF URL (signed, etc.)
	private String pdfUrl;
	// Service PDF URL (signed, etc.)
	private String servicePdfUrl;
	// packingList & DropShip
	private boolean fulfilled = true;
	private boolean processed = false;
	// subscription 
	private String subscriptionCode;
	private boolean printed;
	private String userEmail;
	// Order generated on backend
	private boolean backendOrder;
	// Approval
	private String approval;
	private String customShippingTitle;
	private Double customShippingCost;
	private Double customShippingTotalWeight = 0.0;
	// No commission (not related to affiliate module)
	private Boolean nc;
	private String orderType;
	private Integer flag1;
	// reserved custom Field.
	private String customField1;
	private String customField2;
	private String customField3;
	
	// customer field19
	private String customerField19;
	public String getCustomerField19() {
		return customerField19;
	}

	public void setCustomerField19(String customerField19) {
		this.customerField19 = customerField19;
	}

	// Deal
	private Deal deal;
	
	private String deliveryPerson;
	private int priority;
	
	// mas200
	String mas200orderNo;
	String mas200status;	
	
	// buy safe
	private Boolean wantsBond;
	private Double bondCost;
	private String cartId;
	
	private String manufacturerName;
	private Integer accessUserId;
	
	// virtual bank orders
	private Boolean vbaPaymentStatus;
	private Date vbaDateOfPay;
	private String vbaPaymentMethod;
	private Double vbaAmountToPay;
	private String vbaNote;
	private String vbaTransactionId;
	
	//allocating Payment
	private boolean cancelledPayment;
	private String paymentCancelledBy;
	private Double paymentCancelledAmt;
	
	// budget
	private Double requestForCredit;
	private int actionBy;
	private String actionByName;
	private String actionNote;
	private Timestamp actionDate;
	private String actionType;
	private List<Order> actionHistory;
	
	// budget earned credits
	private Double budgetEarnedCredits;
	// eBizCharge Payment Id
	private String ccToken;
	
	// credit alert
	private boolean paymentAlert;		
	
	// credit card billing address
	private String ccBillingAddress;
	
	//parent SKU deal
	private boolean hasParentDeal;
	
	
	
	
	
	
	//Rush Service
	private Double rushCharge;	
	//Bagging Charge
	private Double baggingCharge;	
	//Less Than Minimum Charge
	private Double lessThanMinCharge;
	
	private String quote;
	
	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}
	
	public Boolean getVbaPaymentStatus() {
		return vbaPaymentStatus;
	}

	public void setVbaPaymentStatus(Boolean vbaPaymentStatus) {
		this.vbaPaymentStatus = vbaPaymentStatus;
	}

	public Date getVbaDateOfPay() {
		return vbaDateOfPay;
	}

	public void setVbaDateOfPay(Date vbaDateOfPay) {
		this.vbaDateOfPay = vbaDateOfPay;
	}

	public String getVbaPaymentMethod() {
		return vbaPaymentMethod;
	}

	public void setVbaPaymentMethod(String vbaPaymentMethod) {
		this.vbaPaymentMethod = vbaPaymentMethod;
	}

	public Double getVbaAmountToPay() {
		return vbaAmountToPay;
	}

	public void setVbaAmountToPay(Double vbaAmountToPay) {
		this.vbaAmountToPay = vbaAmountToPay;
	}

	public String getVbaNote() {
		return vbaNote;
	}

	public void setVbaNote(String vbaNote) {
		this.vbaNote = vbaNote;
	}

	public String getVbaTransactionId() {
		return vbaTransactionId;
	}

	public void setVbaTransactionId(String vbaTransactionId) {
		this.vbaTransactionId = vbaTransactionId;
	}

	//language
	private LanguageCode languageCode = LanguageCode.en;
	
	public Double getCustomShippingTotalWeight() {
		return customShippingTotalWeight;
	}

	public void setCustomShippingTotalWeight(Double customShippingTotalWeight) {
		this.customShippingTotalWeight = customShippingTotalWeight;
	}

	public String getCustomShippingTitle() {
		return customShippingTitle;
	}

	public void setCustomShippingTitle(String customShippingTitle) {
		this.customShippingTitle = customShippingTitle;
	}

	public Double getCustomShippingCost() {
		return customShippingCost;
	}

	public void setCustomShippingCost(Double customShippingCost) {
		this.customShippingCost = customShippingCost;
	}

	public boolean isBackendOrder() {
		return backendOrder;
	}

	public void setBackendOrder(boolean backendOrder) {
		this.backendOrder = backendOrder;
	}

	public String getUserEmail() {
		return userEmail;
	}

	public void setUserEmail(String userEmail) {
		this.userEmail = userEmail;
	}

	public boolean isPrinted() {
		return printed;
	}

	public void setPrinted(boolean printed) {
		this.printed = printed;
	}

	public Integer getOrderId() {
		return orderId;
	}

	public void setOrderId(Integer orderId) {
		this.orderId = orderId;
	}

	public String getExternalOrderId() {
		return externalOrderId;
	}

	public void setExternalOrderId(String externalOrderId) {
		this.externalOrderId = externalOrderId;
	}

	public Integer getUserId() {
		return userId;
	}

	public void setUserId(Integer userId) {
		this.userId = userId;
	}

	public Address getBilling() {
		return billing;
	}

	public void setBilling(Address billing) {
		this.billing = billing;
	}

	public Address getShipping() {
		return shipping;
	}

	public void setShipping(Address shipping) {
		this.shipping = shipping;
	}

	public void setSource(Address source) {
		this.source = source;
	}

	public Address getSource() {
		return source;
	}

	public Double getSubTotal() {
		return subTotal;
	}
	
	public Double getSubTotalAfterDiscount() {
		return this.subTotal - this.promoAmount - this.lineItemPromoAmount;
	}

	public void setSubTotal(Double subTotal) {
		this.subTotal = subTotal;
	}

	public CreditCard getCreditCard() {
		return creditCard;
	}

	public void setCreditCard(CreditCard creditCard) {
		this.creditCard = creditCard;
	}

	public Paypal getPaypal() {
		return paypal;
	}

	public void setPaypal(Paypal paypal) {
		this.paypal = paypal;
	}

	public void setGoogleCheckOut(GoogleCheckOut googleCheckOut) {
		this.googleCheckOut = googleCheckOut;
	}

	public GoogleCheckOut getGoogleCheckOut() {
		return googleCheckOut;
	}

	public String getShippingMethod() {
		return shippingMethod;
	}
	
	public String getShippingMethodPDF() {
		if (shippingMethod != null && shippingMethod.contains("<sup>")) {
			return shippingMethod.replaceAll("<sup>(.*?)</sup>", "");
		} else 
			return shippingMethod;
	}

	public void setShippingMethod(String shippingMethod) {
		this.shippingMethod = shippingMethod;
	}

	public Double getShippingCost() {
		return shippingCost;
	}

	public void setShippingCost(Double shippingCost) {
		this.shippingCost = shippingCost;
	}

	public Double getTaxRate() {
		return taxRate;
	}

	public void setTaxRate(Double taxRate) {
		this.taxRate = taxRate;
	}

	public List<LineItem> getLineItems() {
		return lineItems;
	}

	public void setLineItems(List<LineItem> lineItems) {
		this.lineItems = lineItems;
	}
	// PayPalPro
	public void payPalProOrder(Customer customer,Order order) {		
		orderId = order.getOrderId();
		userId = customer.getId();
		billing = customer.getAddress();
		shipping = customer.getAddress();
		subTotal = order.getSubTotal();
		creditCard = new CreditCard();
		totalWeight =order.getTotalWeight();
		taxExemption = (customer.getTaxId() == null || customer.getTaxId().trim().equals( "" )) ? false : true;
		salesRepId = customer.getSalesRepId();
		salesRepProcessedById = salesRepId;
		totalQuantity = order.getTotalQuantity();
		tax= order.getTax();
		grandTotal = order.getGrandTotal();
		shippingCarrier = order.getShippingCarrier();
		shippingMethod = order.getShippingMethod();
		shippingMessage = order.getShippingMessage();
		customShippingId = order.getCustomShippingId();
		shippingCost = order.getShippingCost();
		dateOrdered = order.getDateOrdered();
		dueDate = order.getDueDate();		
	}

	public void initOrder(Customer customer, Cart cart) {
		userId = customer.getId();
		billing = customer.getAddress();
		shipping = customer.getAddress();
		subTotal = cart.getSubTotal();
		creditCard = new CreditCard();
		totalWeight = cart.getWeight();
		taxExemption = (customer.getTaxId() == null || customer.getTaxId().trim().equals( "" )) ? false : true;
		salesRepId = customer.getSalesRepId();
		salesRepProcessedById = salesRepId;
		totalQuantity = cart.getQuantity();
		promoCode = cart.getPromoCode();
		
		Iterator<CartItem> i = cart.getCartItems().iterator();
		while ( i.hasNext() )
		{
			CartItem cartItem = i.next();
			addLineItem( cartItem );
			if ( cartItem.getProduct().getTemperature() != null ) {
				this.temperatureEnable = true;
				if ( this.lowestTemp > cartItem.getProduct().getTemperature())
					this.lowestTemp = cartItem.getProduct().getTemperature();
			}
		}
	}

	public void addLineItem(CartItem cartItem) {
		LineItem lineItem = new LineItem( lineItems.size() + 1, cartItem );
		addLineItem( lineItem );
	}	

	public void addLineItem(LineItem lineItem) {
		lineItems.add( lineItem );
	}

	public Timestamp getDateOrdered() {
		return dateOrdered;
	}

	public void setDateOrdered(Timestamp dateOrdered) {
		this.dateOrdered = dateOrdered;
	}

	public Double getTax() {
		return this.tax;
	}

	public void setTax(Double tax) {
		this.tax = tax;
	}

	public void setTaxBeforeDiscount() {
		double interTax = getSubTotalForTax() * ((this.taxRate == null) ? 0.00 : this.taxRate) / 100;
		
		interTax = interTax + this.getTaxOnShipping();
		this.tax = Math.floor( interTax * 100.0 + 0.5 ) / 100.0;
	}
	
	public void setTaxAfterDiscount() {
		// discount must be proportional to taxable items
		double interTax;
		if(this.subTotal == 0.0){
			interTax = 0.0;
		} else {
			// if Promo is grater than SubTotal then Tax and GrandTotal will be negative.
			// We need to add more code to add Negative to Customer's Credit.
			double promoAmount = this.getPromoAmount()+this.getLineItemPromoAmount();
			if(promoAmount > getSubTotalForTax() && getSubTotalForTax() > 0){
				promoAmount = getSubTotalForTax();
			}
			interTax = (getSubTotalForTax() - getSubTotalForTax()/this.subTotal * ( promoAmount )) * ((this.taxRate == null) ? 0.00 : this.taxRate) / 100;
		}
		interTax = interTax + this.getTaxOnShipping();
		this.tax = Math.floor( interTax * 100.0 + 0.5 ) / 100.0;
	}
	
	public void setTotalProductWeight(){
		Double total =0.0;
		for(LineItem lineItem : this.getLineItems()){
			total += lineItem.getQuantity()*(lineItem.getProduct().getCaseContent()==null? 1 : lineItem.getProduct().getCaseContent())*1;
		}
		this.totalProductWeight = total;
	}

	private double getTaxOnShipping(){
		
		if(this.getTaxRate() == null || !this.isTaxOnShipping() || this.getShippingCost() == null) {
			return 0.0;
		}
		return this.getShippingCost() * this.getTaxRate() / 100;
	}
	
	public Double getGrandTotal() {
		return Utilities.decimalFormat(this.grandTotal, null);
	}

	public void setGrandTotal(Double grandTotal) {
		this.grandTotal = grandTotal;
	}

	public void setGrandTotal() {
		
		double tempTotal = this.subTotal;
		if(this.getPromo() != null && this.getPromo().getDiscountType() != null){
			if(this.getPromo().getDiscountType().equalsIgnoreCase("order") && tempTotal > 0 && ( this.getPromoAmount()+this.getLineItemPromoAmount() ) > tempTotal  ){
				setPromoAmount(this.subTotal - this.getLineItemPromoAmount());
			}
			
			if(this.getPromo().getDiscountType().equalsIgnoreCase("shipping") && this.getShippingCost() != null && this.getPromoAmount() > this.getShippingCost() ){
				setPromoAmount(this.shippingCost);
			}
		}
		if(this.getBudgetEarnedCredits() != null){
			tempTotal = tempTotal - this.getBudgetEarnedCredits();
		}
		
		double total = Math.floor( (tempTotal - this.getPromoAmount() ) * 100.0 + 0.5 ) / 100.0;
		// removed "((this.creditUsed == null) ? 0.00 : (-1 * this.creditUsed))" because CREDIT shouldn't be deducted from Grand Total 
		this.grandTotal = total  + ((this.tax == null) ? 0.00 : this.tax) + ((this.shippingCost == null) ? 0.00 : this.shippingCost) + ((this.customShippingCost == null) ? 0.00 : this.customShippingCost) + ((this.ccFee == null) ? 0.00 : this.ccFee) + ((this.wantsBond != null && this.wantsBond) ? this.bondCost : 0.0);
	}

	public Double getCcFeeRate() {
		return ccFeeRate;
	}

	public void setCcFeeRate(Double ccFeeRate) {
		this.ccFeeRate = ccFeeRate;
	}

	public Double getCcFee() {
		return ccFee;
	}

	public void setCcFee(Double ccFee) {
		this.ccFee = ccFee;
	}
	
	public void setCcFeeAmount() {
		double interCcFee = 0.00;
		double interGrandTotal = ( this.getSubTotal() + this.getTax() + ((this.shippingCost == null) ? 0.00 : this.shippingCost) - ( ((this.creditUsed == null) ? 0.00 : (-1 * this.creditUsed)) + ( this.getPromoAmount()+this.getLineItemPromoAmount() )) ) ;
		if(this.ccFeePercent){
			interCcFee = Math.floor( interGrandTotal * ((this.ccFeeRate == null) ? 0.00 : this.ccFeeRate ) ) / 100.0;
		} else{
			interCcFee = ((this.ccFeeRate == null) ? 0.00 : this.ccFeeRate );
		}
		this.setCcFee(interCcFee);
	}

	public boolean isCcFeePercent() {
		return ccFeePercent;
	}

	public void setCcFeePercent(boolean ccFeePercent) {
		this.ccFeePercent = ccFeePercent;
	}

	public void setPromoAmount() {
		double promoAmount = 0.0;

		if ( promo != null && promo.getDiscountType() != null ) {
			
			if ( promo.getDiscountType().equalsIgnoreCase("order") ) {
				promoAmount = getOrderPromoAmount();
			}
			else if(promo.getDiscountType().equalsIgnoreCase("shipping")){
				promoAmount = getShippingPromoAmount();
			}
		}
		this.setPromoAmount( promoAmount );
		this.setLineItemPromoAmount(this.lineItemPromoAmount());
		this.setLineItemPromos(this.lineItemPromoMap());
		
	}

	public boolean isPromoAvailable() {
		boolean isAvailable = false;
		if(this.getPromo() != null) {
			isAvailable = true;
		} else {
			for(LineItem lineItem: this.getLineItems()){
				if(lineItem.getPromo() != null) {
					isAvailable = true;
					break;
				}
			}
		}
		return isAvailable;
	}

	public Double getShippingPromoAmount() {
		double promoAmount = 0.0;
		if(promo.isPercent()){
			promoAmount = ((this.shippingCost == null) ? 0.00 : this.shippingCost) * ((promo.getDiscount() == null) ? 0.00 : promo.getDiscount() / 100.00);
		} else{
			promoAmount = (promo.getDiscount() == null) ? 0.00 : promo.getDiscount();
		}
		return promoAmount;
	}
	
	public Double getOrderPromoAmount() {
		double promoAmount = 0.0;
		if(promo.isPercent()){
			promoAmount = this.subTotal * ((promo.getDiscount() == null) ? 0.00 : promo.getDiscount() / 100.00);
		} else{
			promoAmount = (promo.getDiscount() == null) ? 0.00 : promo.getDiscount();
		}
		return promoAmount;
	}
	
	public Double lineItemPromoAmount(){
		double lineItemPromoAmount = 0.0;
		if (insertedLineItems != null) {
			for ( LineItem lineItem : insertedLineItems ) {
				if(lineItem.getPromo() != null) {

					if(lineItem.getPromo().isPercent()){
						lineItemPromoAmount = lineItemPromoAmount + (lineItem.getUnitPrice()*lineItem.getQuantity()) * ((lineItem.getPromo().getDiscount() == null) ? 0.00 : lineItem.getPromo().getDiscount() / 100.00);
					} else {
						lineItemPromoAmount = lineItemPromoAmount +  (( lineItem.getPromo().getDiscount() == null ) ? 0.00 : lineItem.getPromo().getDiscount()) ;
					}
				}
			}
		}
		
		for(LineItem lineItem: this.getLineItems()){
			if(lineItem.getPromo() != null) {

				if(lineItem.getPromo().isPercent()){
					lineItemPromoAmount = lineItemPromoAmount + (lineItem.getUnitPrice()*lineItem.getQuantity()) * ((lineItem.getPromo().getDiscount() == null) ? 0.00 : lineItem.getPromo().getDiscount() / 100.00);
				} else {
					lineItemPromoAmount = lineItemPromoAmount +  (( lineItem.getPromo().getDiscount() == null ) ? 0.00 : lineItem.getPromo().getDiscount() * lineItem.getQuantity()) ;
				}
			}
		}
		return lineItemPromoAmount;
	}
	
	public Map<String, Double> lineItemPromoMap(){
		Map<String, Double> lineItemPromo = new HashMap<String, Double>(); 
		for(LineItem lineItem : this.getLineItems()){

			Double lineItemPromoAmount = 0.0;
			if(lineItem.getPromo() != null) {
				if(lineItem.getPromo().isPercent()){
					lineItemPromoAmount = (lineItem.getUnitPrice()*lineItem.getQuantity()) * ((lineItem.getPromo().getDiscount() == null) ? 0.00 : lineItem.getPromo().getDiscount() / 100.00);
				} else {
					lineItemPromoAmount =  (( lineItem.getPromo().getDiscount() == null ) ? 0.00 : lineItem.getPromo().getDiscount() * lineItem.getQuantity()) ;
				}
				
				if(lineItemPromo.get(lineItem.getPromo().getTitle()) != null){
					lineItemPromoAmount = lineItemPromoAmount + lineItemPromo.get(lineItem.getPromo().getTitle()) ;
					lineItemPromo.remove(lineItem.getPromo().getTitle());
				}
				
				lineItemPromo.put(lineItem.getPromo().getTitle(), lineItemPromoAmount);
			}
		}
		
		if(insertedLineItems!=null){
		for(LineItem lineItem : this.getInsertedLineItems()){
			Double lineItemPromoAmount = 0.0;
			if(lineItem.getPromo() != null) {
				if(lineItem.getPromo().isPercent()){
					lineItemPromoAmount = (lineItem.getUnitPrice()*lineItem.getQuantity()) * ((lineItem.getPromo().getDiscount() == null) ? 0.00 : lineItem.getPromo().getDiscount() / 100.00);
				} else {
					lineItemPromoAmount =  (( lineItem.getPromo().getDiscount() == null ) ? 0.00 : lineItem.getPromo().getDiscount()) ;
				}
				
				if(lineItemPromo.get(lineItem.getPromo().getTitle()) != null){
					lineItemPromoAmount = lineItemPromoAmount + lineItemPromo.get(lineItem.getPromo().getTitle()) ;
					lineItemPromo.remove(lineItem.getPromo().getTitle());
				}
				
				lineItemPromo.put(lineItem.getPromo().getTitle(), lineItemPromoAmount);
			}
		}
	}

		return lineItemPromo;
	}
	public String getStatus(){
		return status;
	}

	public void setStatus(String status){
		this.status = status;
	}

	public String getSubStatus() {
		return subStatus;
	}

	public void setSubStatus(String subStatus) {
		this.subStatus = subStatus;
	}

	public String getPaymentMethod(){
		return paymentMethod;
	}

	public void setPaymentMethod(String paymentMethod){
		this.paymentMethod = paymentMethod;
	}

	public Double getTotalWeight(){
		return totalWeight;
	}

	public void setTotalWeight(Double totalWeight){
		this.totalWeight = totalWeight;
	}

	public com.webjaguar.model.Promo getPromo(){
		return promo;
	}

	public void setPromo(com.webjaguar.model.Promo promo){
		this.promo = promo;
	}

	public Double getPromoAmount(){
		return promoAmount;
	}

	public void setPromoAmount(Double promoAmount){
		this.promoAmount = promoAmount;
	}

	public String getPromoCode(){
		return promoCode;
	}

	public void setPromoCode(String promoTitle){
		this.promoCode = promoTitle;
	}

	public String getTrackcode(){
		return trackcode;
	}

	public void setTrackcode(String trackcode){
		this.trackcode = trackcode;
	}

	public Timestamp getLastModified(){
		return lastModified;
	}

	public void setLastModified(Timestamp lastModified){
		this.lastModified = lastModified;
	}

	public Timestamp getStatusChangeDate(){
		return statusChangeDate;
	}

	public void setStatusChangeDate(Timestamp statusChangeDate){
		this.statusChangeDate = statusChangeDate;
	}

	public List<OrderStatus> getStatusHistory(){
		return statusHistory;
	}

	public void setStatusHistory(List<OrderStatus> statusHistory){
		this.statusHistory = statusHistory;
	}

	public String getInvoiceNote(){
		return invoiceNote;
	}

	public void setInvoiceNote(String invoiceNote){
		this.invoiceNote = invoiceNote;
	}

	public boolean getTaxExemption(){
		return taxExemption;
	}

	public void setTaxExemption(boolean taxExemption){
		this.taxExemption = taxExemption;
	}

	public List<LineItem> getDeletedLineItems(){
		return deletedLineItems;
	}

	public void setDeletedLineItems(List<LineItem> deletedLineItems) {
		this.deletedLineItems = deletedLineItems;
	}

	public List<LineItem> getInsertedLineItems() {
		return insertedLineItems;
	}

	public void setInsertedLineItems(List<LineItem> insertedLineItems) {
		this.insertedLineItems = insertedLineItems;
	}

	public boolean ishasContent() {
		if (lineItems != null) {	
			for ( LineItem lineItem : lineItems )
			{
				if ( lineItem.getProduct().getCaseContent() != null )
				{
					return true;
				}
			}
		}
		if (insertedLineItems != null) {
			for ( LineItem lineItem : insertedLineItems ) {
				if ( lineItem.getProduct().getCaseContent() != null )
				{
					return true;
				}				
			}
		}		
		return false;
	}

	public boolean ishasPacking() {
		if (lineItems != null) {
			for ( LineItem lineItem : lineItems )
			{
				if ( lineItem.getProduct().getPacking() != null && !lineItem.getProduct().getPacking().equals( "" ) )
				{
					return true;
				}
			}
		}
		if (insertedLineItems != null) {
			for ( LineItem lineItem : insertedLineItems ) {
				if ( lineItem.getProduct().getPacking() != null && !lineItem.getProduct().getPacking().equals( "" ) )
				{
					return true;
				}				
			}
		}
		return false;
	}

	public boolean ishasLineItemDiscount() {
		if (lineItems != null) {
			for ( LineItem lineItem : lineItems ) {
				if ( lineItem.getDiscount() != null ) {
					return true;
				}
			}
		}
		return false;
	}
	
	public boolean ishasCustomShipping() {
		if (lineItems != null) {
			for ( LineItem lineItem : lineItems )
			{
				if ( lineItem.getCustomShippingCost() != null )
				{
					return true;
				}
			}
		}
		return false;
	}
	
	public boolean ishasRegularShipping() {
		if (lineItems != null) {
			for ( LineItem lineItem : lineItems )
			{
				if ( lineItem.getCustomShippingCost() == null )
				{
					return true;
				}
			}
		}
		return false;
	}

	
	public boolean ishasLowInventoryMessage() {
		if (lineItems != null) {
			for ( LineItem lineItem : lineItems )
			{
				if ( lineItem.getLowInventoryMessage() != null && !lineItem.getLowInventoryMessage().equals( "" ) )
				{
					return true;
				}
			}
		}
		if (insertedLineItems != null) {
			for ( LineItem lineItem : insertedLineItems ) {
				if ( lineItem.getLowInventoryMessage() != null && !lineItem.getLowInventoryMessage().equals( "" ) )
				{
					return true;
				}				
			}
		}
		return false;
	}

	public boolean ishasCost() {
		if (lineItems != null) {	
			for ( LineItem lineItem : lineItems )
			{
				if ( lineItem.getUnitCost() != null )
				{
					return true;
				}
			}
		}
		if (insertedLineItems != null) {
			for ( LineItem lineItem : insertedLineItems ) {
				if ( lineItem.getUnitCost() != null )
				{
					return true;
				}				
			}
		}		
		return false;
	}

	public Affiliate getAffiliate(){
		return affiliate;
	}

	public void setAffiliate(Affiliate affiliate){
		this.affiliate = affiliate;
	}

	public String getProspectId() {
		return prospectId;
	}

	public void setProspectId(String prospectId) {
		this.prospectId = prospectId;
	}

	public Integer getSalesRepId(){
		return salesRepId;
	}

	public void setSalesRepId(Integer salesRepId){
		this.salesRepId = salesRepId;
	}

	public Integer getSalesRepProcessedById(){
		return salesRepProcessedById;
	}

	public void setSalesRepProcessedById(Integer salesRepProcessedById){
		this.salesRepProcessedById = salesRepProcessedById;
	}

	public int getSize(){
		return this.lineItems.size();
	}

	public Double getAmountPaid(){
		return amountPaid;
	}

	public void setAmountPaid(Double amountPaid){
		this.amountPaid = amountPaid;
	}
	
	public Double getPayment(){
		return Utilities.roundFactory(payment,2,BigDecimal.ROUND_HALF_UP);
	}

	public void setPayment(Double payment){
		this.payment = payment;
	}

	public List<Payment> getPaymentHistory(){
		return paymentHistory;
	}

	public void setPaymentHistory(List<Payment> paymentHistory){
		this.paymentHistory = paymentHistory;
	}

	public boolean isTemperatureEnable(){
		return temperatureEnable;
	}

	public void setTemperatureEnable(boolean temperatureEnable){
		this.temperatureEnable = temperatureEnable;
	}

	public Weather getWeather(){
		return weather;
	}

	public void setWeather(Weather weather){
		this.weather = weather;
	}

	public int getLowestTemp(){
		return lowestTemp;
	}

	public void setLowestTemp(int lowestTemp){
		this.lowestTemp = lowestTemp;
	}

	public double getSubTotalForTax() {
		double subTotalForTax = 0;
		for (LineItem lineItem:this.lineItems) {
			if (lineItem.getTotalPrice() != null && lineItem.getProduct().isTaxable())
				subTotalForTax += lineItem.getTotalPrice().doubleValue();
		}
		if (insertedLineItems != null) {
			for (LineItem lineItem:this.insertedLineItems) {
				if (lineItem.getTotalPrice() != null && lineItem.getProduct().isTaxable())
					subTotalForTax += lineItem.getTotalPrice().doubleValue();
			}			
		}
		return subTotalForTax;
	}

	public String getPurchaseOrder(){
		return purchaseOrder;
	}

	public void setPurchaseOrder(String purchaseOrder){
		this.purchaseOrder = purchaseOrder;
	}

	public String getIpAddress(){
		return ipAddress;
	}

	public void setIpAddress(String ipAddress){
		this.ipAddress = ipAddress;
	}

	public NetCommerce getNetCommerce(){
		return netCommerce;
	}

	public void setNetCommerce(NetCommerce netCommerce){
		this.netCommerce = netCommerce;
	}

	public EBillme geteBillme() {
		return eBillme;
	}

	public void seteBillme(EBillme eBillme) {
		this.eBillme = eBillme;
	}

	public Iopn getAmazonIopn() {
		return amazonIopn;
	}

	public void setAmazonIopn(Iopn amazonIopn) {
		this.amazonIopn = amazonIopn;
	}

	public BankAudi getBankAudi() {
		return bankAudi;
	}

	public void setBankAudi(BankAudi bankAudi) {
		this.bankAudi = bankAudi;
	}

	public GEMoney getGeMoney() {
		return geMoney;
	}

	public void setGeMoney(GEMoney geMoney) {
		this.geMoney = geMoney;
	}

	public LinkShare getLinkShare() {
		return linkShare;
	}

	public void setLinkShare(LinkShare linkShare) {
		this.linkShare = linkShare;
	}

	public Integer getWorkOrderNum(){
		return workOrderNum;
	}

	public void setWorkOrderNum(Integer workOrderNum){
		this.workOrderNum = workOrderNum;
	}

	public String getHost(){
		return host;
	}

	public void setHost(String host){
		this.host = host;
	}

	public String getPdfUrl(){
		return pdfUrl;
	}

	public void setPdfUrl(String pdfUrl){
		this.pdfUrl = pdfUrl;
	}

	public boolean isFulfilled(){
		return fulfilled;
	}

	public void setFulfilled(boolean fulfilled){
		this.fulfilled = fulfilled;
	}

	public boolean isProcessed(){
		return processed;
	}

	public void setProcessed(boolean processed){
		this.processed = processed;
	}

	public GiftCard getGiftCard(){
		return giftCard;
	}

	public void setGiftCard(GiftCard giftCard){
		this.giftCard = giftCard;
	}

	public Double getCreditUsed(){
		return Utilities.roundFactory(creditUsed,2,BigDecimal.ROUND_HALF_UP);
	}

	public void setCreditUsed(Double creditUsed){
		this.creditUsed = creditUsed;
	}
	
	public double getTotalLoyaltyPoint() {
		double loyaltyPoint = 0;
		for (LineItem lineItem:this.lineItems) {
			if (lineItem.getLoyaltyPoint() != null ) {
				loyaltyPoint += ( lineItem.getLoyaltyPoint().doubleValue() * lineItem.getQuantity() );
			}
		}
		if (insertedLineItems != null) {
			for (LineItem lineItem:this.insertedLineItems) {
				if (lineItem.getLoyaltyPoint() != null )
					loyaltyPoint += ( lineItem.getLoyaltyPoint().doubleValue() * lineItem.getQuantity() );
			}			
		}
		return loyaltyPoint;
	}
	
	public String getEncodeStatus() {
		if ( this.getStatus().equals( "pr" ))
			return "orderStatus_pr";
		else if ( this.getStatus().equals( "s" ))
			return "orderStatus_s";
		else if ( this.getStatus().equals( "x" ))
			return "orderStatus_x";
		else if ( this.getStatus().equals( "xp" ))
			return "orderStatus_xp";
		else if ( this.getStatus().equals( "xnc" ))
			return "orderStatus_xnc";
		else if ( this.getStatus().equals( "xq" ))
			return "orderStatus_xq";
		else if ( this.getStatus().equals( "xg" ))
			return "orderStatus_xg";
		else if ( this.getStatus().equals( "pr" ))
			return "orderStatus_pr";
		else if ( this.getStatus().equals( "rls" ))
			return "orderStatus_rls";
		else if ( this.getStatus().equals( "wp" ))
			return "orderStatus_wp";
		else
			return "orderStatus_p";
	}
	
	public boolean isOrderCancel() {
		if (this.getStatus().startsWith("x")) {
			return true;
		}
		return false;
	}
	
	public String getBackEndOrderString() {
		if ( this.isBackendOrder() )
			return "backend";
		else
			return "frontend";
	}

	public String getSubscriptionCode(){
		return subscriptionCode;
	}

	public void setSubscriptionCode(String subscriptionCode){
		this.subscriptionCode = subscriptionCode;
	}

	public Date getShipped(){
		return shipped;
	}

	public void setShipped(Date shipped){
		this.shipped = shipped;
	}

	public String getApproval(){
		return approval;
	}

	public void setApproval(String approval){
		this.approval = approval;
	}

	public Date getDueDate(){
		return dueDate;
	}

	public void setDueDate(Date dueDate){
		this.dueDate = dueDate;
	}
	
	public void setupDueDate(int turnOverday, Integer shippingPeriod) {
		
		GregorianCalendar cal = new GregorianCalendar();
		Date timeOfOrder = this.dateOrdered;
		if ( timeOfOrder == null )
			timeOfOrder = new Date();
		cal.setTimeInMillis( timeOfOrder.getTime() );
		if ( cal.get( GregorianCalendar.AM_PM ) == Calendar.PM ) {
			cal.add( Calendar.DAY_OF_MONTH, 1 );
		}
		if ( cal.get( GregorianCalendar.DAY_OF_WEEK ) == 1 ) {
			cal.add( Calendar.DAY_OF_MONTH, 1 );
		}
		else if ( cal.get( GregorianCalendar.DAY_OF_WEEK ) == 7 ) {
			cal.add( Calendar.DAY_OF_MONTH, 2 );
		}
		for (int i=0; i < turnOverday + ((shippingPeriod==null) ? 0 : shippingPeriod) ; i++) {
			cal.add( Calendar.DAY_OF_MONTH, 1 );
			if ( cal.get( GregorianCalendar.DAY_OF_WEEK ) == 1 ) {
				cal.add( Calendar.DAY_OF_MONTH, 1 );
			}
			else if ( cal.get( GregorianCalendar.DAY_OF_WEEK ) == 7 ) {
				cal.add( Calendar.DAY_OF_MONTH, 2 );
			}
		}
		setDueDate(cal.getTime());
		if (cal.get( GregorianCalendar.DAY_OF_WEEK ) == GregorianCalendar.MONDAY )
			this.setDueDateDay( "M" );
		else if (cal.get( GregorianCalendar.DAY_OF_WEEK ) == GregorianCalendar.TUESDAY )
			this.setDueDateDay( "T" );
		else if (cal.get( GregorianCalendar.DAY_OF_WEEK ) == GregorianCalendar.WEDNESDAY )
			this.setDueDateDay( "W" );
		else if (cal.get( GregorianCalendar.DAY_OF_WEEK ) == GregorianCalendar.THURSDAY )
			this.setDueDateDay( "Th" );
		else if (cal.get( GregorianCalendar.DAY_OF_WEEK ) == GregorianCalendar.FRIDAY )
			this.setDueDateDay( "F" );
	}

	public String getDueDateDay(){
		return dueDateDay;
	}

	public void setDueDateDay(String dueDateDay){
		this.dueDateDay = dueDateDay;
	}

	public int getTurnOverday(){
		return turnOverday;
	}

	public void setTurnOverday(int turnOverday){
		this.turnOverday = turnOverday;
	}

	public Integer getShippingPeriod(){
		return shippingPeriod;
	}

	public void setShippingPeriod(Integer shippingPeriod){
		this.shippingPeriod = shippingPeriod;
	}

	public Boolean getNc(){
		return nc;
	}

	public void setNc(Boolean nc){
		this.nc = nc;
	}

	public String getOrderType(){
		return orderType;
	}

	public void setOrderType(String orderType){
		this.orderType = orderType;
	}

	public Integer getFlag1(){
		return flag1;
	}

	public void setFlag1(Integer flag1){
		this.flag1 = flag1;
	}

	public String getCustomField1() {
		return customField1;
	}

	public void setCustomField1(String customField1) {
		this.customField1 = customField1;
	}

	public String getCustomField2() {
		return customField2;
	}

	public void setCustomField2(String customField2) {
		this.customField2 = customField2;
	}

	public String getCustomField3() {
		return customField3;
	}

	public void setCustomField3(String customField3) {
		this.customField3 = customField3;
	}

	public List<Payment> getPayments(){
		return payments;
	}

	public void setPayments(List<Payment> payments){
		this.payments = payments;
	}
	
	public Double getTotalPayments() {
		Double total = null;
		
		if (this.payments != null) {
			for (Payment payment: this.payments) {
				if (payment.getAmount() != null) {
					if (total == null) total = 0.0;
					total += payment.getAmount();			
				}
			}			
		}
		
		return total;
	}

	public Date getUserDueDate() {
		return userDueDate;
	}

	public void setUserDueDate(Date userDueDate) {
		this.userDueDate = userDueDate;
	}

	public Date getRequestedCancelDate() {
		return requestedCancelDate;
	}

	public void setRequestedCancelDate(Date requestedCancelDate) {
		this.requestedCancelDate = requestedCancelDate;
	}

	public void setDeal(Deal deal) {
		this.deal = deal;
	}

	public Deal getDeal() {
		return deal;
	}

	public int getTotalQuantity() {
		return totalQuantity;
	}

	public void setTotalQuantity(int totalQuantity) {
		this.totalQuantity = totalQuantity;
	}

	public void setLineItemPromoAmount(Double lineItemPromoAmount) {
		this.lineItemPromoAmount = lineItemPromoAmount;
	}

	public Double getLineItemPromoAmount() {
		return lineItemPromoAmount;
	}

	public void setLineItemPromos(Map<String, Double> lineItemPromos) {
		this.lineItemPromos = lineItemPromos;
	}

	public Map<String, Double> getLineItemPromos() {
		return lineItemPromos;
	}
	
	public void setDeliveryPerson(String deliveryPerson) {
		this.deliveryPerson = deliveryPerson;
	}

	public String getDeliveryPerson() {
		return deliveryPerson;
	}

	public void setPriority(int priority) {
		this.priority = priority;
	}

	public int getPriority() {
		return priority;
	}

	public String getMas200orderNo() {
		return mas200orderNo;
	}

	public void setMas200orderNo(String mas200orderNo) {
		this.mas200orderNo = mas200orderNo;
	}

	public String getMas200status() {
		return mas200status;
	}

	public void setMas200status(String mas200status) {
		this.mas200status = mas200status;
	}

	public void setWantsBond(Boolean wantsBond) {
		this.wantsBond = wantsBond;
	}

	public Boolean getWantsBond() {
		return wantsBond;
	}

	public void setBondCost(Double bondCost) {
		this.bondCost = bondCost;
	}

	public Double getBondCost() {
		return bondCost;
	}

	public void setCartId(String cartId) {
		this.cartId = cartId;
	}

	public String getCartId() {
		return cartId;
	}

	public String getManufacturerName() {
		return manufacturerName;
	}

	public void setManufacturerName(String manufacturerName) {
		this.manufacturerName = manufacturerName;
	}
	
	public void setAccessUserId(Integer accessUserId) {
		this.accessUserId = accessUserId;
	}

	public Integer getAccessUserId() {
		return accessUserId;
	}

	public void setCustomShippingId(Integer customShippingId) {
		this.customShippingId = customShippingId;
	}

	public Integer getCustomShippingId() {
		return customShippingId;
	}

	public LanguageCode getLanguageCode() {
		return languageCode;
	}

	public void setLanguageCode(LanguageCode languageCode) {
		this.languageCode = languageCode;
	}

	public void setShippingCarrier(String shippingCarrier) {
		this.shippingCarrier = shippingCarrier;
	}

	public String getShippingCarrier() {
		return shippingCarrier;
	}

	public void setShippingMessage(String shippingMessage) {
		this.shippingMessage = shippingMessage;
	}

	public String getShippingMessage() {
		return shippingMessage;
	}

	public double getCustomShippingSubTotal() {
		double customShippingSubTotal = 0.0;
		for(LineItem lineItem : this.getLineItems())
		{
			Double lineItemTotal = lineItem.getTotalPrice();
			if(lineItem.getProduct().isCustomShippingEnabled() && lineItemTotal != null) {
				customShippingSubTotal = customShippingSubTotal + lineItemTotal;
			}
		}
		
		return customShippingSubTotal;
	}

	public boolean isCancelledPayment() {
		return cancelledPayment;
	}

	public void setCancelledPayment(boolean cancelledPayment) {
		this.cancelledPayment = cancelledPayment;
	}

	public String getPaymentCancelledBy() {
		return paymentCancelledBy;
	}

	public void setPaymentCancelledBy(String paymentCancelledBy) {
		this.paymentCancelledBy = paymentCancelledBy;
	}

	public Double getPaymentCancelledAmt() {
		return paymentCancelledAmt;
	}

	public void setPaymentCancelledAmt(Double paymentCancelledAmt) {
		this.paymentCancelledAmt = paymentCancelledAmt;
	}

	public Double getRequestForCredit() {
		return requestForCredit;
	}

	public void setRequestForCredit(Double requestForCredit) {
		this.requestForCredit = requestForCredit;
	}

	public Double getBalance() {
		if (amountPaid != null) {
			return Utilities.decimalFormat((grandTotal - amountPaid), null);
		} else {
			return grandTotal;
		}
	}
	
	public void setTaxOnShipping(boolean taxOnShipping) {
		this.taxOnShipping = taxOnShipping;
	}

	public boolean isTaxOnShipping() {
		return taxOnShipping;
	}

	public int getActionBy() {
		return actionBy;
	}

	public void setActionBy(int actionBy) {
		this.actionBy = actionBy;
	}

	public String getActionByName() {
		return actionByName;
	}

	public void setActionByName(String actionByName) {
		this.actionByName = actionByName;
	}

	public String getActionNote() {
		return actionNote;
	}

	public void setActionNote(String actionNote) {
		this.actionNote = actionNote;
	}

	public Timestamp getActionDate() {
		return actionDate;
	}

	public void setActionDate(Timestamp actionDate) {
		this.actionDate = actionDate;
	}

	public List<Order> getActionHistory() {
		return actionHistory;
	}

	public void setActionHistory(List<Order> actionHistory) {
		this.actionHistory = actionHistory;
	}

	public String getActionType() {
		return actionType;
	}

	public void setActionType(String actionType) {
		this.actionType = actionType;
	}

	public String getCcToken() {
		return ccToken;
	}

	public void setCcToken(String ccToken) {
		this.ccToken = ccToken;
	}

	public boolean isPaymentAlert() {
		return paymentAlert;
	}

	public void setPaymentAlert(boolean paymentAlert) {
		this.paymentAlert = paymentAlert;
	}

	public String getCcBillingAddress() {
		return ccBillingAddress;
	}

	public void setCcBillingAddress(String ccBillingAddress) {
		this.ccBillingAddress = ccBillingAddress;
	}

	public Double getBudgetEarnedCredits() {
		return budgetEarnedCredits;
	}

	public void setBudgetEarnedCredits(Double budgetEarnedCredits) {
		this.budgetEarnedCredits = budgetEarnedCredits;
	}

	public String getServicePdfUrl() {
		return servicePdfUrl;
	}

	public void setServicePdfUrl(String servicePdfUrl) {
		this.servicePdfUrl = servicePdfUrl;
	}

	public boolean isFirstOrder() {
		return firstOrder;
	}

	public void setFirstOrder(boolean firstOrder) {
		this.firstOrder = firstOrder;
	}

	public boolean isHasParentDeal() {
		return hasParentDeal;
	}

	public void setHasParentDeal(boolean hasParentDeal) {
		this.hasParentDeal = hasParentDeal;
	}

	public Integer getQualifier() {
		return qualifier;
	}

	public void setQualifier(Integer qualifier) {
		this.qualifier = qualifier;
	}
	
	public Double getRushCharge() {
		return rushCharge;
	}

	public void setRushCharge(Double rushCharge) {
		this.rushCharge = rushCharge;
	}

	public Double getBaggingCharge() {
		return baggingCharge;
	}

	public void setBaggingCharge(Double baggingCharge) {
		this.baggingCharge = baggingCharge;
	}

	public Double getLessThanMinCharge() {
		return lessThanMinCharge;
	}

	public void setLessThanMinCharge(Double lessThanMinCharge) {
		this.lessThanMinCharge = lessThanMinCharge;
	}

	public String getQuote() {
		return quote;
	}

	public void setQuote(String quote) {
		this.quote = quote;
	}

	public String getTrackUrlApps() {
		return trackUrlApps;
	}

	public void setTrackUrlApps(String trackUrlApps) {
		this.trackUrlApps = trackUrlApps;
	}
	
}