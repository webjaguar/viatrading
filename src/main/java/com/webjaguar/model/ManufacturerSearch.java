/* Copyright 2006, 2007 Advanced E-Media Solutions
 *
 */

package com.webjaguar.model;

public class ManufacturerSearch {
	
	// sort
	private String sort = "name";
	public String getSort() { return sort; }
	public void setSort(String sort) { this.sort = sort; }	
	
	// id
	private Integer id;
	public Integer getId() { return id; }
	public void setId(Integer id) { this.id = id; }	
	
	//name
	private String name;
	public String getName() { return name; }
	public void setName(String name) { this.name = name; }	
	
	// page 
	private int page = 1;
	public int getPage() { return page; }
	public void setPage(int page) { this.page = page; }
	
	// page size
	private int pageSize = 10;
	public int getPageSize() { return pageSize; }
	public void setPageSize(int pageSize) { this.pageSize = pageSize; }	
	
	// limit
	private Integer limit;
	public Integer getLimit() { return limit; }
	public void setLimit(Integer limit) { this.limit = limit; }
	
	// offset
	private int offset;
	public int getOffset() { return offset; }
	public void setOffset(int offset) { this.offset = offset; }	
}
