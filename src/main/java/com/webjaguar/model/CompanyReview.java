package com.webjaguar.model;

public class CompanyReview extends Review
{
	private int companyId;

	public int getCompanyId()
	{
		return companyId;
	}
	public void setCompanyId(int companyId)
	{
		this.companyId = companyId;
	}

}
