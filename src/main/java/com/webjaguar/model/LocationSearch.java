/* Copyright 2009 Advanced E-Media Solutions
 * @author Shahin Naji
 */

package com.webjaguar.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class LocationSearch implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	// sort
	private String sort = "id";
	public String getSort() { return sort; }
	public void setSort(String sort) { this.sort = sort; }	
	
	// userId
	private Integer userId;
	public Integer getUserId() { return userId; }
	public void setUserId(Integer userId) { this.userId = userId; }
	
	// name
	private String name;
	public String getName() { return name; }
	public void setName(String name) { this.name = name; }
	
	// userId
	private Integer customerLocatorField;
	public Integer getCustomerLocatorField() { return customerLocatorField; }
	public void setCustomerLocatorField(Integer customerLocatorField) { this.customerLocatorField = customerLocatorField; }
	
	// page 
	private int page = 1;
	public int getPage() { return page; }
	public void setPage(int page) { this.page = page; }
	
	// page size
	private int pageSize = 10;
	public int getPageSize() { return pageSize; }
	public void setPageSize(int pageSize) { this.pageSize = pageSize; }
	
	// zipcode
	private String zipcode;
	public String getZipcode() { return zipcode; }
	public void setZipcode(String zipcode) { this.zipcode = zipcode; }
	
	// zipcode Object
	private ZipCode zipcodeObject;
	public ZipCode getZipcodeObject() { return zipcodeObject; }
	public void setZipcodeObject(ZipCode zipcodeObject) { this.zipcodeObject = zipcodeObject; }
	
	// zipcode list
	private ArrayList<String> zipcodeList;
	public ArrayList<String> getZipcodeList() { return zipcodeList; }
	public void setZipcodeList(ArrayList<String> zipcodeList) { this.zipcodeList = zipcodeList; }
	
	// zipcode Search List
	private List<ZipCode> zipcodeSearchList;
	public List<ZipCode> getZipcodeSearchList() { return zipcodeSearchList; }
	public void setZipcodeSearchList(List<ZipCode> zipcodeSearchList) { this.zipcodeSearchList = zipcodeSearchList; }
	
	// radius
	private Integer radius = null;
	public Integer getRadius() { return radius; }
	public void setRadius(Integer radius) { this.radius = radius; }
	
	// keywords
	private String keywords = "";
	public String getKeywords() { return keywords; }
	public void setKeywords(String keywords) { 
		keywordList.clear();	// clear old list
		this.keywords = keywords; 
	}
	
	// keywords minimum characters
	private int keywordsMinChars;
	public int getKeywordsMinChars() { return keywordsMinChars; }
	public void setKeywordsMinChars(int keywordsMinChars) { this.keywordsMinChars = keywordsMinChars; }
	
	// keywords list
	private List<String> keywordList = new ArrayList<String>();
	public void setKeywordList(List<String> keywordList) { this.keywordList = keywordList; }	
	public List<String> getKeywordList() {	return this.keywordList; }
	public void addKeyword(String keyword) {
		keywordList.add(keyword);
	}
	
	// country
	private String country;
	public String getCountry() { return country; }
	public void setCountry(String country) { this.country = country; }
	
	// city
	private String city;
	public String getCity() { return city; }
	public void setCity(String city) { this.city = city; }
	
	// categoryId
	private String categoryId;
	public String getCategoryId() { return categoryId; }
	public void setCategoryId(String categoryId) { this.categoryId = categoryId; }

	// limit
	private Integer limit;
	public Integer getLimit() { return limit; }
	public void setLimit(Integer limit) { this.limit = limit; }
	
	// offset
	private int offset;
	public int getOffset() { return offset; }
	public void setOffset(int offset) { this.offset = offset; }

		
}
