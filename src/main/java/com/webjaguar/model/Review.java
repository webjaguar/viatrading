package com.webjaguar.model;

import java.util.Date;
import java.sql.Timestamp;

public class Review
{
	private int id;
	private boolean active;
	private int userId;
	private String userName;
	private int rate;
	private Date created;
	private Timestamp lastModified;
	private String title;
	private String text;
	
	private int numReview;
	private double average;
	private String reviewType;
	private String ipAddress;
	
	public double getAverage()
	{
		return average;
	}
	public void setAverage(double average)
	{
		this.average = average;
	}
	public int getId()
	{
		return id;
	}
	public void setId(int id)
	{
		this.id = id;
	}
	public boolean isActive()
	{
		return active;
	}
	public void setActive(boolean active)
	{
		this.active = active;
	}
	public int getUserId()
	{
		return userId;
	}
	public void setUserId(int userId)
	{
		this.userId = userId;
	}
	public String getUserName()
	{
		return userName;
	}
	public void setUserName(String userName)
	{
		this.userName = userName;
	}
	public int getRate()
	{
		return rate;
	}
	public void setRate(int rate)
	{
		this.rate = rate;
	}
	public Date getCreated()
	{
		return created;
	}
	public void setCreated(Date created)
	{
		this.created = created;
	}
	public Timestamp getLastModified()
	{
		return lastModified;
	}
	public void setLastModified(Timestamp lastModified)
	{
		this.lastModified = lastModified;
	}
	public String getTitle()
	{
		return title;
	}
	public void setTitle(String title)
	{
		this.title = title;
	}
	public String getText()
	{
		return text;
	}
	public void setText(String text)
	{
		this.text = text;
	}
	public String getShortText() {
		if (this.text.length() > 50 ) {
			return this.text.substring( 0, 50 ).concat( "..." );
		} else
			return this.text;
	}
	public int getNumReview()
	{
		return numReview;
	}
	public void setNumReview(int numReview)
	{
		this.numReview = numReview;
	}
	public String getReviewType() {
		return reviewType;
	}
	public void setReviewType(String reviewType) {
		this.reviewType = reviewType;
	}
	public String getIpAddress() {
		return ipAddress;
	}
	public void setIpAddress(String ipAddress) {
		this.ipAddress = ipAddress;
	}

}
