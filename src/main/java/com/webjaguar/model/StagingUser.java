package com.webjaguar.model;

public class StagingUser {
	
	private Integer id;
	private Integer crmContactId;
	private String username;
	private String rating1;
	private String rating2;
	private DialingNote newDialingNote;
	
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public String getUsername() {
		return username;
	}
	public void setUsername(String username) {
		this.username = username;
	}
	public String getRating1() {
		return rating1;
	}
	public void setRating1(String rating1) {
		this.rating1 = rating1;
	}
	public String getRating2() {
		return rating2;
	}
	public void setRating2(String rating2) {
		this.rating2 = rating2;
	}
	public DialingNote getNewDialingNote() {
		return newDialingNote;
	}
	public void setNewDialingNote(DialingNote newDialingNote) {
		this.newDialingNote = newDialingNote;
	}
	public Integer getCrmContactId() {
		return crmContactId;
	}
	public void setCrmContactId(Integer crmContactId) {
		this.crmContactId = crmContactId;
	}
}
