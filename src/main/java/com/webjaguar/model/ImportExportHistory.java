/* Copyright 2005, 2008 Advanced E-Media Solutions
 *
 */

package com.webjaguar.model;

import java.sql.Timestamp;

public class ImportExportHistory {

	private String type;
	private Timestamp created;
    private String username;
    private boolean imported;
    
	public ImportExportHistory()
	{
		super();
	}
	public ImportExportHistory( String type, String username, boolean imported )
	{
		super();
		this.type = type;
		this.username = username;
		this.imported = imported;
	}
	public String getType()
	{
		return type;
	}
	public void setType(String type)
	{
		this.type = type;
	}
	public Timestamp getCreated()
	{
		return created;
	}
	public void setCreated(Timestamp created)
	{
		this.created = created;
	}
	public String getUsername()
	{
		return username;
	}
	public void setUsername(String username)
	{
		this.username = username;
	}
	public boolean isImported()
	{
		return imported;
	}
	public void setImported(boolean imported)
	{
		this.imported = imported;
	}
	public String getImportedString()
	{
		if (this.imported)
			return "import";
		else
			return "export";
	}
	
	
}
