package com.webjaguar.model;

import java.util.List;

public class AccessGroup
{
	/*
	 * add comment
	 */
    private Integer id;
	private String groupName;
	private Integer numUser;
	private List<String> roles;
	
	public AccessGroup() {
		super();
	}
	public AccessGroup(String groupName, Integer id) {
		super();
		this.groupName = groupName;
		this.id = id;
	}
	public Integer getId()
	{
		return id;
	}
	public void setId(Integer id)
	{
		this.id = id;
	}
	public List<String> getRoles()
	{
		return roles;
	}
	public void setRoles(List<String> roles)
	{
		this.roles = roles;
	}
	public String getGroupName() {
		return groupName;
	}
	public void setGroupName(String groupName) {
		this.groupName = groupName;
	}
	public Integer getNumUser() {
		return numUser;
	}
	public void setNumUser(Integer numUser) {
		this.numUser = numUser;
	}
}