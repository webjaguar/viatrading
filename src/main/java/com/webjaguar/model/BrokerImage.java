package com.webjaguar.model;

import java.io.Serializable;

import javax.xml.bind.annotation.XmlTransient;

public class BrokerImage implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String imageUrl;
	@XmlTransient
	private byte index;
	
	public BrokerImage() {
	}
	public String getImageUrl() {
		return imageUrl;
	}
	public void setImageUrl(String imageUrl) {
		this.imageUrl = imageUrl;
	}
	public byte getIndex() {
		return index;
	}
	public void setIndex(byte index) {
		this.index = index;
	}
	
	public boolean isAbsolute() {
		if (imageUrl != null && (imageUrl.contains("//") || imageUrl.startsWith("/"))) {
			return true;
		} else {
			return false;			
		}
	}
}
