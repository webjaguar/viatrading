/* Copyright 2005, 2007 Advanced E-Media Solutions
 *
 */

package com.webjaguar.model;

public class PurchaseOrderLineItem extends LineItem {

	private Integer poId;
	private String sku;
	private String productName;
	private String supplierSku;
	private Integer qty = 1;
	private Integer deltaQty = 0;
	private Double cost;
	private boolean productCostTier;
	
	public Integer getDeltaQty()
	{
		return deltaQty;
	}
	public void setDeltaQty(Integer deltaQty)
	{
		this.deltaQty = deltaQty;
	}
	public PurchaseOrderLineItem()
	{
	}
	public PurchaseOrderLineItem( String sku )
	{
		super();
		this.sku = sku;
	}
	public PurchaseOrderLineItem( int lineNumber, PurchaseOrderLineItem poLineItem )
	{
		super();
		this.setLineNumber( lineNumber );
		this.cost = poLineItem.getCost();
		this.sku = poLineItem.getSku();
		this.productName = poLineItem.getProductName();
		this.supplierSku = poLineItem.getSupplierSku();
		this.qty = poLineItem.getQty();
		this.setProduct( poLineItem.getProduct() );
		this.setProductAttributes( poLineItem.getProductAttributes() );
		this.setItemGroup( poLineItem.getItemGroup() );
		this.setItemGroupMainItem( poLineItem.isItemGroupMainItem() );
	}
	public String toString()
	{
		StringBuffer sbuff = new StringBuffer();
		sbuff.append(this.getSku());
		// get product attributes
		if (this.getProductAttributes() != null) {
			for (ProductAttribute prodAttr:this.getProductAttributes()) {
				sbuff.append("{" + prodAttr.getOptionName() + "}");
				sbuff.append(prodAttr.getValueName());
				sbuff.append("(" + prodAttr.getOptionCusValue() + ")");
				sbuff.append("(" + prodAttr.getOptionBoxValue() + ")");
			}			
		}
		return sbuff.toString();
	}
	public String getSku()
	{
		return sku;
	}
	public void setSku(String sku)
	{
		this.sku = sku;
	}
	public String getSupplierSku()
	{
		return supplierSku;
	}
	public void setSupplierSku(String supplierSku)
	{
		this.supplierSku = supplierSku;
	}
	public Double getCost()
	{
		return cost;
	}
	public void setCost(Double cost)
	{
		this.cost = cost;
	}
	public Integer getQty()
	{
		return qty;
	}
	public void setQty(Integer qty)
	{
		this.qty = qty;
	}
	public Double getTotalCost()
	{
		if ( this.cost == null )
			return null;
		Double total = new Double( this.cost.doubleValue() * this.qty );
		return total;
	}
	public Integer getPoId()
	{
		return poId;
	}
	public void setPoId(Integer poId)
	{
		this.poId = poId;
	}
	public String getProductName() {
		return productName;
	}
	public void setProductName(String productName) {
		this.productName = productName;
	}
	public boolean isProductCostTier() {
		return productCostTier;
	}
	public void setProductCostTier(boolean productCostTier) {
		this.productCostTier = productCostTier;
	}

}
