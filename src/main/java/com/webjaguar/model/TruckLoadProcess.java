package com.webjaguar.model;

import java.util.Date;
import java.util.List;


public class TruckLoadProcess {

	private Integer id;
	private String name;
	private Date startDate;
	private Date lastDate;
	private List<Product> originalProduct;
	private List<TruckLoadProcessProducts> derivedProduct;
	private String custom1;
	private String custom2;
	private String custom3;
	private String custom4;
	private String pt;
	private String status;
	private Integer createdBy;
	private String createdByName;
	private Double extMsrp;
	private String type;
	private Double teamHours;
	private Double teamCost;
	private Integer numOfTeamMembers;
	private String dateStarted;
	private String dateEnded;
	
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public Date getStartDate() {
		return startDate;
	}
	public void setStartDate(Date startDate) {
		this.startDate = startDate;
	}
	public Date getLastDate() {
		return lastDate;
	}
	public void setLastDate(Date lastDate) {
		this.lastDate = lastDate;
	}
	public List<Product> getOriginalProduct() {
		return originalProduct;
	}
	public void setOriginalProduct(List<Product> originalProduct) {
		this.originalProduct = originalProduct;
	}
	public List<TruckLoadProcessProducts> getDerivedProduct() {
		return derivedProduct;
	}
	public void setDerivedProduct(List<TruckLoadProcessProducts> derivedProduct) {
		this.derivedProduct = derivedProduct;
	}
	public String getCustom1() {
		return custom1;
	}
	public void setCustom1(String custom1) {
		this.custom1 = custom1;
	}
	public String getCustom2() {
		return custom2;
	}
	public void setCustom2(String custom2) {
		this.custom2 = custom2;
	}
	public String getCustom3() {
		return custom3;
	}
	public void setCustom3(String custom3) {
		this.custom3 = custom3;
	}
	public String getCustom4() {
		return custom4;
	}
	public void setCustom4(String custom4) {
		this.custom4 = custom4;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public Integer getCreatedBy() {
		return createdBy;
	}
	public void setCreatedBy(Integer createdBy) {
		this.createdBy = createdBy;
	}
	public Double getExtMsrp() {
		return extMsrp;
	}
	public void setExtMsrp(Double extMsrp) {
		this.extMsrp = extMsrp;
	}
	public String getCreatedByName() {
		return createdByName;
	}
	public void setCreatedByName(String createdByName) {
		this.createdByName = createdByName;
	}
	public String getPt() {
		return pt;
	}
	public void setPt(String pt) {
		this.pt = pt;
	}
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	public Double getTeamHours() {
		return teamHours;
	}
	public void setTeamHours(Double teamHours) {
		this.teamHours = teamHours;
	}
	public Double getTeamCost() {
		return teamCost;
	}
	public void setTeamCost(Double teamCost) {
		this.teamCost = teamCost;
	}
	public Integer getNumOfTeamMembers() {
		return numOfTeamMembers;
	}
	public void setNumOfTeamMembers(Integer numOfTeamMembers) {
		this.numOfTeamMembers = numOfTeamMembers;
	}
	public String getDateStarted() {
		return dateStarted;
	}
	public void setDateStarted(String dateStarted) {
		this.dateStarted = dateStarted;
	}
	public String getDateEnded() {
		return dateEnded;
	}
	public void setDateEnded(String dateEnded) {
		this.dateEnded = dateEnded;
	}
	
}
