/* Copyright 2006 Advanced E-Media Solutions
 * @author Li Qin
 * @since 10.10.2006
 */

package com.webjaguar.model;

public class State {

	private String name;
	private String code;
	private Double taxRate;
	private boolean applyShippingTax;
	private String region;
	
	public String getCode() {
		return code;
	}
	public void setCode(String code) {
		this.code = code;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public Double getTaxRate() {
		return taxRate;
	}
	public void setTaxRate(Double taxRate) {
		this.taxRate = taxRate;
	}
	public void setApplyShippingTax(boolean applyShippingTax) {
		this.applyShippingTax = applyShippingTax;
	}
	public boolean isApplyShippingTax() {
		return applyShippingTax;
	}
	public String getRegion() {
		return region;
	}
	public void setRegion(String region) {
		this.region = region;
	}
	
}