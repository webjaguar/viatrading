/* Copyright 2007 Advanced E-Media Solutions
 * @author Eduardo Asprec
 * @since 07.26.2007
 */

package com.webjaguar.model;

import java.util.Date;

public class PaymentSearch {

	// sort
	private String sort= "payment_date DESC";
	private Integer id;
	private Integer userId;
	private int page = 1;
	private int pageSize = 10;
	private Integer orderId;
	private String email;
	private Integer salesRepId;
	
	public Integer getId()
	{
		return id;
	}
	public void setId(Integer id)
	{
		this.id = id;
	}
	public Integer getUserId()
	{
		return userId;
	}
	public void setUserId(Integer userId)
	{
		this.userId = userId;
	}
	public int getPage()
	{
		return page;
	}
	public void setPage(int page)
	{
		this.page = page;
	}
	public int getPageSize()
	{
		return pageSize;
	}
	public void setPageSize(int pageSize)
	{
		this.pageSize = pageSize;
	}
	public String getSort()
	{
		return sort;
	}
	public void setSort(String sort)
	{
		this.sort = sort;
	}
	public Integer getOrderId() {
		return orderId;
	}
	public void setOrderId(Integer orderId) {
		this.orderId = orderId;
	}
	
	// exportSuccess 
	private String exportSuccess;
	public String getExportSuccess() { return exportSuccess; }
	public void setExportSuccess(String exportSuccess) { this.exportSuccess = exportSuccess; }
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	
	public Integer getSalesRepId() {
		return salesRepId;
	}
	public void setSalesRepId(Integer salesRepId) {
		this.salesRepId = salesRepId;
	}
	
}
