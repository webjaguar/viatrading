/* Copyright 2005, 2006 Advanced E-Media Solutions
 *
 */

package com.webjaguar.model;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.sql.Timestamp;
import java.util.*;

public class Category {

	public Category() { }
	
    private Integer id;
	private String name;
    private Integer parent;
    private Integer rank;
    private Boolean shared;
	private Category parentCategory;
	private String htmlCode;
	private String footerHtmlCode;
	private String leftbarType;
	private String rightBarTopHtmlCode;
	private String rightBarBottomHtmlCode;
	private String leftBarTopHtmlCode;
	private String leftBarBottomHtmlCode;
	private boolean hasHtmlCode;
	private String host;
	private String protectedHtmlCode;
	private String url;
	private String urlEs;
	private String urlTarget;
	private boolean redirect301;
	private String imageUrl;
	private String imageUrlAltName;
	private Integer productCount;
	private Integer subCount;
	private Integer numOfClicks;
	private Timestamp created;
	private Timestamp lastModified;
	private List<Category> subCategories = new ArrayList<Category>();
	private Integer subcatCols;
	private Integer subcatLevels;
	private String subcatLocation;
	private String subcatDisplay;
	private boolean homePage;
	private CategoryLinkType linkType = CategoryLinkType.VISIBLE;
	// images
	private boolean hasLinkImage;
	private boolean hasLinkImageOver;	
	private String displayMode;
	private String displayModeGrid;
	private boolean hasImage;
	//layout
	private boolean hideHeader;
	private boolean hideTopBar;
	private boolean hideLeftBar;
	private boolean hideRightBar;
	private boolean hideFooter;	
	private boolean hideBreadCrumbs;	
	private int layoutId;
	private String headTag;
	private Integer productPerPage;
	private Integer productPerRow;
	
	private boolean showSubcats;
	private boolean selected;

	// protected
	private String protectedLevel = "0";
	private boolean protect;

	private String categoryIds;
	private String setType = "intersection";
	private String sortBy;
	private Integer limit;	
	private Date startDate;
	private Date endDate;
	private String salesTagTitle;
	
	// Extra Product Field Search
	private boolean productFieldSearch;
	private String productFieldSearchType;
	private String productFieldSearchPosition="left";
	
	// i18n
	private String lang;
	private String i18nName;
	private String i18nHtmlCode;
	private String i18nProtectedHtmlCode;
	private String i18nHeadTag;	
	
	private boolean showOnSearch;
	
	// SEO
	private Double siteMapPriority = 0.5;
	
	// Extra Fields
	private String field1;
	private String field2;
	private String field3;
	private String field4;
	private String field5;
	private String feed;
	
	private Integer parentChildDisplay;
	
	private boolean showFullWidth;
	
	public String getField1() {
		return field1;
	}

	public void setField1(String field1) {
		this.field1 = field1;
	}

	public String getField2() {
		return field2;
	}

	public void setField2(String field2) {
		this.field2 = field2;
	}

	public String getField3() {
		return field3;
	}

	public void setField3(String field3) {
		this.field3 = field3;
	}

	public String getField4() {
		return field4;
	}

	public void setField4(String field4) {
		this.field4 = field4;
	}

	public String getField5() {
		return field5;
	}

	public void setField5(String field5) {
		this.field5 = field5;
	}

	public boolean isProductFieldSearch()
	{
		return productFieldSearch;
	}

	public void setProductFieldSearch(boolean productFieldSearch)
	{
		this.productFieldSearch = productFieldSearch;
	}
	
	public String getSortBy()
	{
		return sortBy;
	}

	public void setSortBy(String sortBy)
	{
		this.sortBy = sortBy;
	}

	public boolean isProtect()
	{
		return protect;
	}

	public void setProtect(boolean protect)
	{
		this.protect = protect;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}
	
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Category getParentCategory() {
		return parentCategory;
	}

	public void setParentCategory(Category parentCategory) {
		this.parentCategory = parentCategory;
	}

	public Integer getParent() {
		return parent;
	}

	public void setParent(Integer parent) {
		this.parent = parent;
	}

	public Integer getRank() {
		return rank;
	}

	public void setRank(Integer rank) {
		this.rank = rank;
	}

	public String getHtmlCode() {
		return htmlCode;
	}

	public void setHtmlCode(String htmlCode) {
		this.htmlCode = htmlCode;
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	public Integer getProductCount() {
		return productCount;
	}

	public void setProductCount(Integer productCount) {
		this.productCount = productCount;
	}

	public Integer getSubCount() {
		return subCount;
	}

	public void setSubCount(Integer subCount) {
		this.subCount = subCount;
	}

	public Integer getNumOfClicks() {
		return numOfClicks;
	}

	public void setNumOfClicks(Integer numOfClicks) {
		this.numOfClicks = numOfClicks;
	}

	public Timestamp getCreated() {
		return created;
	}

	public void setCreated(Timestamp created) {
		this.created = created;
	}

	public Timestamp getLastModified() {
		return lastModified;
	}

	public void setLastModified(Timestamp lastModified) {
		this.lastModified = lastModified;
	}

	public List<Category> getSubCategories() {
		return subCategories;
	}

	public void setSubCategories(List<Category> subCategories) {
		this.subCategories = subCategories;
	}

	public Integer getSubcatCols() {
		return subcatCols;
	}

	public void setSubcatCols(Integer subcatCols) {
		this.subcatCols = subcatCols;
	}

	public boolean isHomePage() {
		return homePage;
	}

	public void setHomePage(boolean homePage) {
		this.homePage = homePage;
	}

	public CategoryLinkType getLinkType() {
		return linkType;
	}

	public void setLinkType(CategoryLinkType linkType) {
		this.linkType = linkType;
	}
	
	public boolean isHasSubcats() {
		if (subCategories.size() > 0) {
			return true;
		} else {
			return false;
		}
	}

	public String getDisplayMode() {
		return displayMode;
	}

	public void setDisplayMode(String displayMode) {
		this.displayMode = displayMode;
	}

	public String getDisplayModeGrid() {
		return displayModeGrid;
	}

	public void setDisplayModeGrid(String displayModeGrid) {
		this.displayModeGrid = displayModeGrid;
	}

	public boolean isHideFooter() {
		return hideFooter;
	}

	public void setHideFooter(boolean hideFooter) {
		this.hideFooter = hideFooter;
	}

	public boolean isHideHeader() {
		return hideHeader;
	}

	public void setHideHeader(boolean hideHeader) {
		this.hideHeader = hideHeader;
	}

	public boolean isHideLeftBar() {
		return hideLeftBar;
	}

	public void setHideLeftBar(boolean hideLeftBar) {
		this.hideLeftBar = hideLeftBar;
	}

	public boolean isHideRightBar() {
		return hideRightBar;
	}

	public void setHideRightBar(boolean hideRightBar) {
		this.hideRightBar = hideRightBar;
	}

	public boolean isHideTopBar() {
		return hideTopBar;
	}

	public void setHideTopBar(boolean hideTopBar) {
		this.hideTopBar = hideTopBar;
	}

	public boolean isHideBreadCrumbs() {
		return hideBreadCrumbs;
	}

	public void setHideBreadCrumbs(boolean hideBreadCrumbs) {
		this.hideBreadCrumbs = hideBreadCrumbs;
	}

	public int getLayoutId() {
		return layoutId;
	}

	public void setLayoutId(int layoutId) {
		this.layoutId = layoutId;
	}

	public Integer getSubcatLevels() {
		return subcatLevels;
	}

	public void setSubcatLevels(Integer subcatLevels) {
		this.subcatLevels = subcatLevels;
	}

	public String getUrlTarget() {
		return urlTarget;
	}

	public void setUrlTarget(String urlTarget) {
		this.urlTarget = urlTarget;
	}

	public boolean isHasLinkImage()
	{
		return hasLinkImage;
	}

	public void setHasLinkImage(boolean hasLinkImage)
	{
		this.hasLinkImage = hasLinkImage;
	}

	public boolean isHasLinkImageOver()
	{
		return hasLinkImageOver;
	}

	public void setHasLinkImageOver(boolean hasLinkImageOver)
	{
		this.hasLinkImageOver = hasLinkImageOver;
	}

	public boolean isHasImage()
	{
		return hasImage;
	}

	public void setHasImage(boolean hasImage)
	{
		this.hasImage = hasImage;
	}

	public String getProtectedLevel()
	{
		return protectedLevel;
	}

	public void setProtectedLevel(String protectedLevel)
	{
		this.protectedLevel = protectedLevel;
	}

	public String getProtectedHtmlCode()
	{
		return protectedHtmlCode;
	}

	public void setProtectedHtmlCode(String protectedHtmlCode)
	{
		this.protectedHtmlCode = protectedHtmlCode;
	}

	public String getHeadTag()
	{
		return headTag;
	}

	public void setHeadTag(String headTag)
	{
		this.headTag = headTag;
	}

	public Integer getProductPerPage()
	{
		return productPerPage;
	}

	public void setProductPerPage(Integer productPerPage)
	{
		this.productPerPage = productPerPage;
	}

	public boolean isShowSubcats()
	{
		return showSubcats;
	}

	public void setShowSubcats(boolean showSubcats)
	{
		this.showSubcats = showSubcats;
	}

	public boolean isSelected()
	{
		return selected;
	}

	public void setSelected(boolean selected)
	{
		this.selected = selected;
	}

	public boolean isHasHtmlCode()
	{
		return hasHtmlCode;
	}

	public void setHasHtmlCode(boolean hasHtmlCode)
	{
		this.hasHtmlCode = hasHtmlCode;
	}

	public String getSubcatLocation()
	{
		return subcatLocation;
	}

	public void setSubcatLocation(String subcatLocation)
	{
		this.subcatLocation = subcatLocation;
	}

	public String getCategoryIds()
	{
		return categoryIds;
	}

	public void setCategoryIds(String categoryIds)
	{
		this.categoryIds = categoryIds;
	}

	public Integer getLimit()
	{
		return limit;
	}

	public void setLimit(Integer limit)
	{
		this.limit = limit;
	}

	public Date getStartDate()
	{
		return startDate;
	}

	public void setStartDate(Date startDate)
	{
		this.startDate = startDate;
	}

	public Date getEndDate()
	{
		return endDate;
	}

	public void setEndDate(Date endDate)
	{
		this.endDate = endDate;
	}

	public String getSubcatDisplay()
	{
		return subcatDisplay;
	}

	public void setSubcatDisplay(String subcatDisplay)
	{
		this.subcatDisplay = subcatDisplay;
	}

	public Integer getProductPerRow()
	{
		return productPerRow;
	}

	public void setProductPerRow(Integer productPerRow)
	{
		this.productPerRow = productPerRow;
	}

	public String getSalesTagTitle()
	{
		return salesTagTitle;
	}

	public void setSalesTagTitle(String salesTagTitle)
	{
		this.salesTagTitle = salesTagTitle;
	}

	public String getHost()
	{
		return host;
	}

	public void setHost(String host)
	{
		this.host = host;
	}

	public String getLang()
	{
		return lang;
	}

	public void setLang(String lang)
	{
		this.lang = lang;
	}

	public String getI18nName()
	{
		return i18nName;
	}

	public void setI18nName(String name)
	{
		i18nName = name;
	}

	public String getI18nHtmlCode()
	{
		return i18nHtmlCode;
	}

	public void setI18nHtmlCode(String htmlCode)
	{
		i18nHtmlCode = htmlCode;
	}

	public String getI18nProtectedHtmlCode()
	{
		return i18nProtectedHtmlCode;
	}

	public void setI18nProtectedHtmlCode(String protectedHtmlCode)
	{
		i18nProtectedHtmlCode = protectedHtmlCode;
	}

	public String getI18nHeadTag()
	{
		return i18nHeadTag;
	}

	public void setI18nHeadTag(String headTag)
	{
		i18nHeadTag = headTag;
	}

	public boolean isShowOnSearch()
	{
		return showOnSearch;
	}

	public void setShowOnSearch(boolean showOnSearch)
	{
		this.showOnSearch = showOnSearch;
	}
	
	public Integer getProtectedLevelAsNumber() {
		if (protectedLevel.contains( "1" )) {
			return protectedLevel.length();
		} else {
			return 0;
		}
	}

	public String getFooterHtmlCode()
	{
		return footerHtmlCode;
	}

	public void setFooterHtmlCode(String footerHtmlCode)
	{
		this.footerHtmlCode = footerHtmlCode;
	}

	public String getRightBarTopHtmlCode()
	{
		return rightBarTopHtmlCode;
	}

	public void setRightBarTopHtmlCode(String rightBarTopHtmlCode)
	{
		this.rightBarTopHtmlCode = rightBarTopHtmlCode;
	}
	public void setRightBarBottomHtmlCode(String rightBarBottomHtmlCode) {
		this.rightBarBottomHtmlCode = rightBarBottomHtmlCode;
	}

	public String getRightBarBottomHtmlCode() {
		return rightBarBottomHtmlCode;
	}

	public void setLeftBarTopHtmlCode(String leftBarTopHtmlCode) {
		this.leftBarTopHtmlCode = leftBarTopHtmlCode;
	}

	public String getLeftBarTopHtmlCode() {
		return leftBarTopHtmlCode;
	}

	public void setLeftBarBottomHtmlCode(String leftBarBottomHtmlCode) {
		this.leftBarBottomHtmlCode = leftBarBottomHtmlCode;
	}

	public String getLeftBarBottomHtmlCode() {
		return leftBarBottomHtmlCode;
	}

	public Boolean getShared()
	{
		return shared;
	}
	public void setShared(Boolean shared)
	{
		this.shared = shared;
	}

	public String getSetType()
	{
		return setType;
	}
	public void setSetType(String setType)
	{
		this.setType = setType;
	}

	public boolean isRedirect301() {
		return redirect301;
	}

	public void setRedirect301(boolean redirect301) {
		this.redirect301 = redirect301;
	}
	
	public String getEncodedName() throws UnsupportedEncodingException {
		String encoded = "";
		if (name != null) {
			encoded = name.trim().replace("/", "-");
			encoded = encoded.trim().replace(" ", "-");
			encoded = URLEncoder.encode(encoded, "UTF-8");
			encoded = encoded.replace("%2C", ",");		// keep commas
		}
		return encoded;
	}

	public void setSiteMapPriority(Double siteMapPriority) {
		this.siteMapPriority = siteMapPriority;
	}

	public Double getSiteMapPriority() {
		return siteMapPriority;
	}

	public String getProductFieldSearchPosition() {
		return productFieldSearchPosition;
	}

	public void setProductFieldSearchPosition(String productFieldSearchPosition) {
		this.productFieldSearchPosition = productFieldSearchPosition;
	}

	public String getProductFieldSearchType() {
		return productFieldSearchType;
	}

	public void setProductFieldSearchType(String productFieldSearchType) {
		this.productFieldSearchType = productFieldSearchType;
	}

	public String getImageUrl() {
		return imageUrl;
	}

	public void setImageUrl(String imageUrl) {
		this.imageUrl = imageUrl;
	}
	
	public boolean isAbsolute() {
		if (imageUrl != null && (imageUrl.contains("//") || imageUrl.startsWith("/"))) {
			return true;
		} else {
			return false;			
		}
	}

	public String getImageUrlAltName() {
		return imageUrlAltName;
	}

	public Integer getParentChildDisplay() {
		return parentChildDisplay;
	}

	public void setParentChildDisplay(Integer parentChildDisplay) {
		this.parentChildDisplay = parentChildDisplay;
	}

	public void setImageUrlAltName(String imageUrlAltName) {
		if(imageUrlAltName != null) {
			this.imageUrlAltName = imageUrlAltName;	
		} else {
			this.imageUrlAltName = name;
		}
	}

	public String getFeed() {
		return feed;
	}

	public void setFeed(String feed) {
		this.feed = feed;
	}

	public String getLeftbarType() {
		return leftbarType;
	}

	public void setLeftbarType(String leftbarType) {
		this.leftbarType = leftbarType;
	}
	
	public boolean isShowFullWidth() {
		return showFullWidth;
	}

	public void setShowFullWidth(boolean showFullWidth) {
		this.showFullWidth = showFullWidth;
	}

	public String getUrlEs() {
		return urlEs;
	}

	public void setUrlEs(String urlEs) {
		this.urlEs = urlEs;
	}
	
}