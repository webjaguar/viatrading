package com.webjaguar.model;

import java.util.Date;

public class CustomerGroup {
	
	private Integer id;
	private String name;
	private boolean active;
	private Date created;
	private Integer numCustomer;
	private String createdBy;
	
	public Integer getNumCustomer()
	{
		return numCustomer;
	}
	public void setNumCustomer(Integer numCustomer)
	{
		this.numCustomer = numCustomer;
	}
	public Integer getId()
	{
		return id;
	}
	public void setId(Integer id)
	{
		this.id = id;
	}
	public String getName()
	{
		return name;
	}
	public void setName(String name)
	{
		this.name = name;
	}
	public boolean isActive()
	{
		return active;
	}
	public void setActive(boolean active)
	{
		this.active = active;
	}
	public Date getCreated()
	{
		return created;
	}
	public void setCreated(Date created)
	{
		this.created = created;
	}
	public String getCreatedBy() {
		return createdBy;
	}
	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

}
