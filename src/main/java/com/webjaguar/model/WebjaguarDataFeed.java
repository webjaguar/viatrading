/* Copyright 2007 Advanced E-Media Solutions
 * @author Eduardo Asprec
 * @since 10.04.2007
 */

package com.webjaguar.model;

public class WebjaguarDataFeed {

	private Integer id;
	private String name;
	private String token;
	private String server;
	private String username;
	private String password;
	private String filename;
	private String filetype;
	private String prefix;
	private Integer category;
	private Double price;
	private Integer inventory;
	private boolean subscriber;
	private boolean active = true;
	private String feedType;
	
	public void setId(Integer id) {
		this.id = id;
	}
	public Integer getId() {
		return id;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getName() {
		return name;
	}
	public String getServer() {
		return server;
	}
	public void setServer(String server) {
		this.server = server;
	}
	public String getUsername() {
		return username;
	}
	public void setUsername(String username) {
		this.username = username;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public String getFilename() {
		return filename;
	}
	public void setFilename(String filename) {
		this.filename = filename;
	}
	public String getPrefix() {
		return prefix;
	}
	public void setPrefix(String prefix) {
		this.prefix = prefix;
	}
	public Integer getCategory() {
		return category;
	}
	public void setCategory(Integer category) {
		this.category = category;
	}
	public Double getPrice() {
		return price;
	}
	public void setPrice(Double price) {
		this.price = price;
	}
	public Integer getInventory() {
		return inventory;
	}
	public void setInventory(Integer inventory) {
		this.inventory = inventory;
	}
	public void setToken(String token) {
		this.token = token;
	}
	public String getToken() {
		return token;
	}
	public void setActive(boolean active) {
		this.active = active;
	}
	public boolean isActive() {
		return active;
	}
	public void setSubscriber(boolean subscriber) {
		this.subscriber = subscriber;
	}
	public boolean isSubscriber() {
		return subscriber;
	}
	public void setFiletype(String filetype) {
		this.filetype = filetype;
	}
	public String getFiletype() {
		return filetype;
	}
	public String getFeedType() {
		return feedType;
	}
	public void setFeedType(String feedType) {
		this.feedType = feedType;
	}
}