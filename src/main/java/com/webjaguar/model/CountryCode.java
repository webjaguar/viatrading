package com.webjaguar.model;

public class CountryCode {

	String code;
	String name;
	public CountryCode(){
	}
	public String getCode() {
		return code;
	}
	public void setCode(String code) {
		this.code = code;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	
}
