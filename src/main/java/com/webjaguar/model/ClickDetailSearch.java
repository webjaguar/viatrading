package com.webjaguar.model;

import java.io.Serializable;

public class ClickDetailSearch implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private Integer clickId;
	private Integer salesRepId;
	// sort
	private String sort= "id desc";
	// page 
		private int page = 1;
	// page size
	private int pageSize = 10;
	
	
	public Integer getClickId() {
		return clickId;
	}
	public void setClickId(Integer clickId) {
		this.clickId = clickId;
	}
	public String getSort() {
		return sort;
	}
	public void setSort(String sort) {
		this.sort = sort;
	}
	public int getPage() {
		return page;
	}
	public void setPage(int page) {
		this.page = page;
	}
	public int getPageSize() {
		return pageSize;
	}
	public void setPageSize(int pageSize) {
		this.pageSize = pageSize;
	}
	public Integer getSalesRepId() {
		return salesRepId;
	}
	public void setSalesRepId(Integer salesRepId) {
		this.salesRepId = salesRepId;
	}

	
}
