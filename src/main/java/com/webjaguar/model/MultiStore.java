/* Copyright 2008 Advanced E-Media Solutions
 * @author Eduardo Asprec
 * @since 03.09.2008
 */

package com.webjaguar.model;

public class MultiStore {
	
	private String host;
	private String storeId;
	private String contactEmail;
	private Integer midNewRegistration;
	private String registrationSuccessUrl;
	private Integer midNewOrders;
	private Integer midShippedOrders;
	private Double minOrderAmt;
	private Double minOrderAmtWholesale;
	
	public String getHost()
	{
		return host;
	}
	public void setHost(String host)
	{
		this.host = host;
	}
	public String getStoreId() {
		return storeId;
	}
	public void setStoreId(String storeId) {
		this.storeId = storeId;
	}
	public String getContactEmail()
	{
		return contactEmail;
	}
	public void setContactEmail(String contactEmail)
	{
		this.contactEmail = contactEmail;
	}
	public Integer getMidNewRegistration()
	{
		return midNewRegistration;
	}
	public void setMidNewRegistration(Integer midNewRegistration)
	{
		this.midNewRegistration = midNewRegistration;
	}
	public Integer getMidNewOrders()
	{
		return midNewOrders;
	}
	public void setMidNewOrders(Integer midNewOrders)
	{
		this.midNewOrders = midNewOrders;
	}
	public Integer getMidShippedOrders()
	{
		return midShippedOrders;
	}
	public void setMidShippedOrders(Integer midShippedOrders)
	{
		this.midShippedOrders = midShippedOrders;
	}
	public Double getMinOrderAmt() {
		return minOrderAmt;
	}
	public void setMinOrderAmt(Double minOrderAmt) {
		this.minOrderAmt = minOrderAmt;
	}
	public Double getMinOrderAmtWholesale() {
		return minOrderAmtWholesale;
	}
	public void setMinOrderAmtWholesale(Double minOrderAmtWholesale) {
		this.minOrderAmtWholesale = minOrderAmtWholesale;
	}
	public void setRegistrationSuccessUrl(String registrationSuccessUrl) {
		this.registrationSuccessUrl = registrationSuccessUrl;
	}
	public String getRegistrationSuccessUrl() {
		return registrationSuccessUrl;
	}
}
