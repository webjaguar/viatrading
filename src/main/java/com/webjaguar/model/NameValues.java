package com.webjaguar.model;

import java.util.*;


public class NameValues
{
	private int fieldId;
	private String name;
	private Set value = Collections.synchronizedSortedSet(new TreeSet());
	public String getName()
	{
		return name;
	}
	public void setName(String name)
	{
		this.name = name;
	}
	public Set getValue()
	{
		return value;
	}
	public void setValue(Set value)
	{
		this.value = value;
	}
	public int getFieldId()
	{
		return fieldId;
	}
	public void setFieldId(int fieldId)
	{
		this.fieldId = fieldId;
	}

	
}
