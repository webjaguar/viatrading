/* Copyright 2008 Advanced E-Media Solutions
 * @author Eduardo Asprec
 * @since 9.22.2008
 */

package com.webjaguar.model;

public class CsvFeed {

	private String feed;
	private String headerName;
	private String columnName;
	Integer column;
	private boolean zeroAsNull;
	private boolean numeric;
	private boolean wholeNum;
	private String overrideValue;
	
	public CsvFeed() {}

	public CsvFeed(String columnName) {
		this.columnName = columnName;
	}
	
	public String getFeed()
	{
		return feed;
	}
	public void setFeed(String feed)
	{
		this.feed = feed;
	}
	public String getHeaderName()
	{
		return headerName;
	}
	public void setHeaderName(String headerName)
	{
		this.headerName = headerName;
	}
	public String getColumnName()
	{
		return columnName;
	}
	public void setColumnName(String columnName)
	{
		this.columnName = columnName;
	}
	public Integer getColumn()
	{
		return column;
	}
	public void setColumn(Integer column)
	{
		this.column = column;
	}
	public boolean isZeroAsNull()
	{
		return zeroAsNull;
	}
	public void setZeroAsNull(boolean zeroAsNull)
	{
		this.zeroAsNull = zeroAsNull;
	}

	public boolean isNumeric()
	{
		return numeric;
	}

	public void setNumeric(boolean numeric)
	{
		this.numeric = numeric;
	}

	public boolean isWholeNum()
	{
		return wholeNum;
	}

	public void setWholeNum(boolean wholeNum)
	{
		this.wholeNum = wholeNum;
	}

	public String getOverrideValue() {
		return overrideValue;
	}

	public void setOverrideValue(String overrideValue) {
		this.overrideValue = overrideValue;
	}

}
