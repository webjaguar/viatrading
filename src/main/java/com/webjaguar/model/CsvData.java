package com.webjaguar.model;

public class CsvData {
	
	private String type;
	private Integer index;
	private String value;
	private String columnName;
	private String headerName;
	private String name;
	
	
	
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public CsvData() {	
	}
	
	public CsvData(String headerName, String columnName, String type, Integer index, String value){
		this.type = type;
		this.index = index;
		this.columnName = columnName;
		this.headerName = headerName;
		this.value = value;
	}
	
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	public Integer getIndex() {
		return index;
	}
	public void setIndex(Integer index) {
		this.index = index;
	}
	public String getValue() {
		return value;
	}
	public void setValue(String value) {
		this.value = value;
	}
	public String getColumnName() {
		return columnName;
	}
	public void setColumnName(String columnName) {
		this.columnName = columnName;
	}
	public String getHeaderName() {
		return headerName;
	}
	public void setHeaderName(String headerName) {
		this.headerName = headerName;
	}
	
}
