/*
 * Copyright 2008 Advanced E-Media Solutions
 * 
 */

package com.webjaguar.model;

import java.util.Date;

public class CustomerReport
{
	private Date dateOrdered;
	private Date customerCreated;
	private Integer orderId;
	private String sku;
	private String productName;
	private Integer quantity;
	private Double unitPrice;
	private Double subTotal;
	private Double grandTotal;
	private String shippingMethod;
	private Double shippingCost;
	private String paymentMethod;
	private Double taxRate;
	private Double tax;
	private Date shipped;
	private String salesRepName;
	private String orderStatus;
	private Integer userId;
	private String firstName;
	private String lastName;
	private Address address;
	private String username;
	private Integer numOfLogins;
	private Double ordersGrandTotal;
	private Integer orderCount;
	private Integer salesRepId;
	private Integer month;
	private Date lastOrderedDate;
	private Double averageOrder;
	// crm
	private Integer crmContactId;
	private Integer crmAccountId;
	private boolean suspendedEvent;
	
	private String cancelReason;

	public String getCancelReason() {
		return cancelReason;
	}
	public void setCancelReason(String cancelReason) {
		this.cancelReason = cancelReason;
	}
	
	public Integer getMonth()
	{
		return month;
	}
	public void setMonth(Integer month)
	{
		this.month = month;
	}
	public Integer getSalesRepId()
	{
		return salesRepId;
	}
	public void setSalesRepId(Integer salesRepId)
	{
		this.salesRepId = salesRepId;
	}
	public Integer getNumOfLogins()
	{
		return numOfLogins;
	}
	public void setNumOfLogins(Integer numOfLogins)
	{
		this.numOfLogins = numOfLogins;
	}
	public String getFirstName()
	{
		return firstName;
	}
	public void setFirstName(String firstName)
	{
		this.firstName = firstName;
	}
	public String getLastName()
	{
		return lastName;
	}
	public void setLastName(String lastName)
	{
		this.lastName = lastName;
	}
	public String getUsername()
	{
		return username;
	}
	public void setUsername(String username)
	{
		this.username = username;
	}
	public Date getDateOrdered()
	{
		return dateOrdered;
	}
	public void setDateOrdered(Date dateOrdered)
	{
		this.dateOrdered = dateOrdered;
	}
	public Integer getOrderId()
	{
		return orderId;
	}
	public void setOrderId(Integer orderId)
	{
		this.orderId = orderId;
	}
	public String getSku()
	{
		return sku;
	}
	public void setSku(String sku)
	{
		this.sku = sku;
	}
	public String getProductName()
	{
		return productName;
	}
	public void setProductName(String productName)
	{
		this.productName = productName;
	}
	public Integer getQuantity()
	{
		return quantity;
	}
	public void setQuantity(Integer quantity)
	{
		this.quantity = quantity;
	}
	public Double getUnitPrice()
	{
		return unitPrice;
	}
	public void setUnitPrice(Double unitPrice)
	{
		this.unitPrice = unitPrice;
	}
	public Double getSubTotal()
	{
		return subTotal;
	}
	public void setSubTotal(Double subTotal)
	{
		this.subTotal = subTotal;
	}
	public Double getGrandTotal()
	{
		return grandTotal;
	}
	public void setGrandTotal(Double grandTotal)
	{
		this.grandTotal = grandTotal;
	}
	public String getShippingMethod()
	{
		return shippingMethod;
	}
	public void setShippingMethod(String shippingMethod)
	{
		this.shippingMethod = shippingMethod;
	}
	public Double getShippingCost()
	{
		return shippingCost;
	}
	public void setShippingCost(Double shippingCost)
	{
		this.shippingCost = shippingCost;
	}
	public String getPaymentMethod()
	{
		return paymentMethod;
	}
	public void setPaymentMethod(String paymentMethod)
	{
		this.paymentMethod = paymentMethod;
	}
	public Double getTaxRate()
	{
		return taxRate;
	}
	public void setTaxRate(Double taxRate)
	{
		this.taxRate = taxRate;
	}
	public Double getTax()
	{
		return tax;
	}
	public void setTax(Double tax)
	{
		this.tax = tax;
	}
	public Date getShipped()
	{
		return shipped;
	}
	public void setShipped(Date shipped)
	{
		this.shipped = shipped;
	}
	public String getSalesRepName()
	{
		return salesRepName;
	}
	public void setSalesRepName(String salesRepName)
	{
		this.salesRepName = salesRepName;
	}
	public String getOrderStatus()
	{
		return orderStatus;
	}
	public void setOrderStatus(String orderStatus)
	{
		this.orderStatus = orderStatus;
	}
	public String getEncodeStatus() {
		if ( this.getOrderStatus().equals( "pr" ))
			return "orderStatus_pr";
		else if ( this.getOrderStatus().equals( "s" ))
			return "orderStatus_s";
		else if ( this.getOrderStatus().equals( "x" ))
			return "orderStatus_x";
		else if ( this.getOrderStatus().equals( "xp" ))
			return "orderStatus_xp";
		else if ( this.getOrderStatus().equals( "xnc" ))
			return "orderStatus_xnc";
		else if ( this.getOrderStatus().equals( "xg" ))
			return "orderStatus_xg";
		else if ( this.getOrderStatus().equals( "xq" ))
			return "orderStatus_xq";
		else if ( this.getOrderStatus().equals( "pr" ))
			return "orderStatus_pr";
		else
			return "orderStatus_p";
	}
	public Integer getUserId()
	{
		return userId;
	}
	public void setUserId(Integer userId)
	{
		this.userId = userId;
	}
	public Address getAddress()
	{
		return address;
	}
	public void setAddress(Address address)
	{
		this.address = address;
	}
	public Date getCustomerCreated()
	{
		return customerCreated;
	}
	public void setCustomerCreated(Date customerCreated)
	{
		this.customerCreated = customerCreated;
	}
	public Double getOrdersGrandTotal()
	{
		return ordersGrandTotal;
	}
	public void setOrdersGrandTotal(Double ordersGrandTotal)
	{
		this.ordersGrandTotal = ordersGrandTotal;
	}
	public Integer getOrderCount()
	{
		return orderCount;
	}
	public void setOrderCount(Integer orderCount)
	{
		this.orderCount = orderCount;
	}
	public String getAddressToString()
	{
		if (this.address == null)
			return "";
		else {
			return this.address.getAddr1() + " " + this.address.getAddr2() + " " +
					  this.address.getCity() + " " + this.address.getZip() + " " + this.address.getStateProvince();
		}
	}
	public void setCrmContactId(Integer crmContactId) {
		this.crmContactId = crmContactId;
	}
	public Integer getCrmContactId() {
		return crmContactId;
	}
	public void setCrmAccountId(Integer crmAccountId) {
		this.crmAccountId = crmAccountId;
	}
	public Integer getCrmAccountId() {
		return crmAccountId;
	}
	public void setLastOrderedDate(Date lastOrderedDate) {
		this.lastOrderedDate = lastOrderedDate;
	}
	public Date getLastOrderedDate() {
		return lastOrderedDate;
	}
	public void setAverageOrder(Double averageOrder) {
		this.averageOrder = averageOrder;
	}
	public Double getAverageOrder() {
		return averageOrder;
	}
	public boolean isSuspendedEvent() {
		return suspendedEvent;
	}
	public void setSuspendedEvent(boolean suspendedEvent) {
		this.suspendedEvent = suspendedEvent;
	}
	
	private boolean havingWalkIn;
	private boolean payingWalkIn;
	private boolean holdLoads;

	public boolean isHavingWalkIn() {
		return havingWalkIn;
	}

	public void setHavingWalkIn(boolean havingWalkIn) {
		this.havingWalkIn = havingWalkIn;
	}

	public boolean isPayingWalkIn() {
		return payingWalkIn;
	}

	public void setPayingWalkIn(boolean payingWalkIn) {
		this.payingWalkIn = payingWalkIn;
	}

	public boolean isHoldLoads() {
		return holdLoads;
	}

	public void setHoldLoads(boolean holdLoads) {
		this.holdLoads = holdLoads;
	}

}
