package com.webjaguar.model;

import java.util.List;

public class AccessPrivilege
{
	private AccessUser user;
	// privileges
	private List<String> role;
	// SiteInfo
	private boolean roleSiteInfo;
	// Customer
	private boolean roleCustomerEdit;
	private boolean roleCustomerCreate;
	private boolean roleCustomerDelete;
	private boolean roleCustomerImportExport;
	private boolean roleCustomerLoginAs;
	private boolean roleCustomerMyListAdd;
	private boolean roleCustomerMyListDelete;
	private boolean roleCustomerMyListView;
	private boolean roleCustomerAddInvoice;
	private boolean roleCustomerViewList;
	private boolean roleCustomerField;
	private boolean roleCustomerAffiliate;
	private boolean roleCustomerCheckboxEdit;
	private boolean roleCustomerCreditUpdateDelete;
	private boolean roleCustomerUpdateTrackCode;

	// Invoice
	private boolean roleInvoiceEdit;
	private boolean roleShippedInvoiceEdit;
	private boolean roleInvoiceViewList;
	private boolean roleInvoiceStatus;
	private boolean roleInvoicePackingList;
	private boolean roleInvoiceImportExport;
	// Payments
	private boolean roleIncomingPaymentViewList;
	private boolean roleIncomingPaymentCreate;
	private boolean roleIncomingPaymentCancel;
	private boolean roleOverridePaymentAlert;
	private boolean roleOutgoingPaymentViewList;
	private boolean roleOutgoingPaymentCreate;
	private boolean roleOutgoingPaymentCancel;
	
	// Product
	private boolean roleProductEdit;
	private boolean roleProductCreate;
	private boolean roleProductDelete;
	private boolean roleProductImportExport;
	private boolean roleProductFieldEdit;
	private boolean roleProductViewList;
	private boolean roleProductViewListXX;
	// Category
	private boolean roleCategoryEdit;
	private boolean roleCategoryCreate;
	private boolean roleCategoryDelete;
	private boolean roleCategoryUpdateRank;
	private boolean roleCategoryUpdateHeadTag;
	// Sales Rep
	private boolean roleSalesRepEdit;
	private boolean roleSalesRepCreate;
	private boolean roleSalesRepDelete;
	private boolean roleSalesRepViewList;
	// Store Locator
	private boolean roleStoreLocatorUpdate;
	private boolean roleStoreLocatorCreate;
	private boolean roleStoreLocatorImportExport;
	private boolean roleStoreLocatorDelete;
	private boolean roleStoreLocatorViewList;
	// Layout
	private boolean roleLayoutEdit;
	private boolean roleLayoutHeadTag;
	// Promotion
	private boolean rolePromotionEdit;
	private boolean rolePromotionCreate;
	private boolean rolePromotionDelete;
	// Sales Tag
	private boolean roleSalesTagEdit;
	private boolean roleSalesTagCreate;
	private boolean roleSalesTagDelete;
	// MailInRebate
	private boolean roleMailInRebateCreate;
	private boolean roleMailInRebateDelete;
	// Deal
	private boolean roleDealCreate;
	private boolean roleDealDelete;
	// FAQ
	private boolean roleFAQEdit;
	private boolean roleFAQCreate;
	private boolean roleFAQDelete;
	// Policy
	private boolean rolePolicyEdit;
	private boolean rolePolicyCreate;
	private boolean rolePolicyDelete;
	// Ticket
	private boolean roleTicketViewList;
	private boolean roleTicketUpdate;
	private boolean roleTicketCreate;
	// Purchase Order
	private boolean rolePurchaseOrderCreate;
	private boolean rolePurchaseOrderUpdate;
	private boolean rolePurchaseOrderViewList;
	// Inventory
	private boolean roleInventoryImportExport;
	// EchoSign
	private boolean roleEchoSignView;
	// Drop ship
	private boolean roleDropShipCreate;
	// Supplier
	private boolean roleSupplierCreate;
	private boolean roleSupplierUpdate;
	private boolean roleSupplierDelete;
	private boolean roleSupplierViewList;
	// Service
	private boolean roleServiceViewList;
	// Report
	private boolean roleReportCustomers;
	private boolean roleReportShippedOrder;
	private boolean roleReportOrdersNeedAttention;
	private boolean roleReportOrdersOverview;
	private boolean roleReportShippedOverview;
	private boolean roleReportOrderPenProc;
	private boolean roleReportShippedDaily;
	private boolean roleReportSalesRep;
	private boolean rolereportSalesRepDaily;
	private boolean roleReportSalesRepOverviewII;
	private boolean roleReportCustomerMetric;
	private boolean roleReportTrackCode;
	private boolean roleReportImpExp;
	private boolean roleReportAffiliateCommission;
	private boolean roleReportProduct;
	private boolean roleReportInventory;
	private boolean roleReportProcessing;
	private boolean roleReportOrdersDaily;
	private boolean roleReportAbandonedShoppingCart;
	private boolean roleReportLAY;
	private boolean roleReportEventMemberList;
	
	// TruckLoadProcessing
	private boolean roleProcessingCreate;
	private boolean roleProcessingUpdate;
	private boolean roleProcessingDelete;
	private boolean roleProcessingViewList;
	
	// Email Manager
	private boolean roleEmailManagerCreate;
	private boolean roleEmailManagerViewHistory;
	private boolean roleEmailManagerBuyCredit;
	// Event
	private boolean roleEventCreate;
	private boolean roleEventUpdate;
	private boolean roleEventDelete;
	private boolean roleEventViewList;
	private boolean roleEventMemberViewList;
	// CRM
	private boolean roleCRM;
	private boolean roleCRMField;
	private boolean roleCRMGroup;
	private boolean roleCRMForm;
	private boolean roleCRMImportExport;
	// Data Feed
	private boolean roleDataFeed;
	// Quick Export
	private boolean roleQuickExport;

	public boolean isRoleSiteInfo() {
		return roleSiteInfo;
	}
	public void setRoleSiteInfo(boolean roleSiteInfo) {
		this.roleSiteInfo = roleSiteInfo;
	}
	public boolean isRoleEmailManagerCreate()
	{
		return roleEmailManagerCreate;
	}
	public void setRoleEmailManagerCreate(boolean roleEmailManagerCreate)
	{
		this.roleEmailManagerCreate = roleEmailManagerCreate;
	}
	public boolean isRoleEmailManagerViewHistory()
	{
		return roleEmailManagerViewHistory;
	}
	public void setRoleEmailManagerViewHistory(boolean roleEmailManagerViewHistory)
	{
		this.roleEmailManagerViewHistory = roleEmailManagerViewHistory;
	}
	public boolean isRoleEmailManagerBuyCredit()
	{
		return roleEmailManagerBuyCredit;
	}
	public void setRoleEmailManagerBuyCredit(boolean roleEmailManagerBuyCredit)
	{
		this.roleEmailManagerBuyCredit = roleEmailManagerBuyCredit;
	}
	public boolean isRoleReportCustomers()
	{
		return roleReportCustomers;
	}
	public void setRoleReportCustomers(boolean roleReportCustomers)
	{
		this.roleReportCustomers = roleReportCustomers;
	}
	public AccessUser getUser()
	{
		return user;
	}
	public void setUser(AccessUser user)
	{
		this.user = user;
	}
	public AccessPrivilege()
	{
		this.user = new AccessUser();
	}
	public List<String> getRole()
	{
		return role;
	}
	public void setRole(List<String> role)
	{
		this.role = role;
	}
	
	public void syncRoles()
	{
		boolean result = false;
	    for (String r : role)	{
	    	// site Info
	    	if ( r.equals( "ROLE_SITE_INFO" ) )
	    		this.roleSiteInfo = true;
	    	// customer
	    	if ( r.equals( "ROLE_CUSTOMER_CREATE" ) )
	    		this.roleCustomerCreate = true;
	    	if ( r.equals( "ROLE_CUSTOMER_EDIT" ) )
	            this.roleCustomerEdit = true;
	    	if ( r.equals( "ROLE_CUSTOMER_DELETE" ) )
	            this.roleCustomerDelete = true;
	    	if ( r.equals( "ROLE_CUSTOMER_IMPORT_EXPORT" ) )
	            this.roleCustomerImportExport = true;
	    	if ( r.equals( "ROLE_CUSTOMER_LOGINAS" ) )
	            this.roleCustomerLoginAs = true;
	    	if ( r.equals( "ROLE_CUSTOMER_MYLIST_ADD" ) )
	    		this.roleCustomerMyListAdd = true;
	    	if ( r.equals( "ROLE_CUSTOMER_MYLIST_DELETE" ) )
	    		this.roleCustomerMyListDelete = true;
	    	if ( r.equals( "ROLE_CUSTOMER_MYLIST_VIEW" ) )
	    		this.roleCustomerMyListView = true;
	    	if ( r.equals( "ROLE_CUSTOMER_ADD_INVOICE" ) )
	    		this.roleCustomerAddInvoice = true;
	    	if ( r.equals( "ROLE_CUSTOMER_VIEW_LIST" ) )
	    		this.roleCustomerViewList = true;
	    	if ( r.equals( "ROLE_CUSTOMER_FIELD" ) )
	    		this.roleCustomerField = true;
	    	if ( r.equals( "ROLE_CUSTOMER_AFFILIATE" ) )
	    		this.roleCustomerAffiliate = true;
	    	if ( r.equals( "ROLE_CUSTOMER_CHECKBOX_EDIT" ) )
	    		this.roleCustomerCheckboxEdit = true;
	    	if ( r.equals( "ROLE_CUSTOMER_CREDIT_UPDATE_DELETE" ) )
	    	this.roleCustomerCreditUpdateDelete = true;
	    	if ( r.equals( "ROLE_CUSTOMER_UPDATE_TRACKCODE" ) )
		    	this.setRoleCustomerUpdateTrackCode(true);	    	
	    	// invoice
	    	if ( r.equals( "ROLE_INVOICE_EDIT" ) )
	    		this.roleInvoiceEdit = true;
	    	if ( r.equals( "ROLE_SHIPPED_INVOICE_EDIT" ) )
		    		this.roleShippedInvoiceEdit = true;
	    	if ( r.equals( "ROLE_INVOICE_STATUS" ) )
	    		this.roleInvoiceStatus = true;
	    	if ( r.equals( "ROLE_INVOICE_VIEW_LIST" ) )
	    		this.roleInvoiceViewList = true;
	    	if ( r.equals( "ROLE_INVOICE_PACKING_LIST" ) )
	    		this.roleInvoicePackingList = true;
	    	if ( r.equals( "ROLE_INVOICE_IMPORT_EXPORT" ) )
	    		this.roleInvoiceImportExport = true;
	    	// payments
	    	if ( r.equals( "ROLE_INCOMING_PAYMENT_VIEW_LIST" ) )
	    		this.roleIncomingPaymentViewList = true;
	    	if ( r.equals( "ROLE_INCOMING_PAYMENT_CREATE" ) )
	    		this.roleIncomingPaymentCreate = true;
	    	if ( r.equals( "ROLE_INCOMING_PAYMENT_CANCEL" ) )
	    		this.roleIncomingPaymentCancel = true;
	    	if ( r.equals( "ROLE_OUTGOING_PAYMENT_VIEW_LIST" ) )
	    		this.roleOutgoingPaymentViewList = true;
	    	if ( r.equals( "ROLE_OUTGOING_PAYMENT_CREATE" ) )
	    		this.roleOutgoingPaymentCreate = true;
	    	if ( r.equals( "ROLE_OUTGOING_PAYMENT_CANCEL" ) )
	    		this.roleOutgoingPaymentCancel = true;
	    	if ( r.equals( "ROLE_OVERRIDE_PAYMENT_ALERT" ) )
	    		this.roleOverridePaymentAlert = true;
	    	// product
	    	if ( r.equals( "ROLE_PRODUCT_CREATE" ) )
	    		this.roleProductCreate = true;
	    	if ( r.equals( "ROLE_PRODUCT_EDIT" ) )
	    		this.roleProductEdit = true;
	    	if ( r.equals( "ROLE_PRODUCT_DELETE" ) )
	    		this.roleProductDelete = true;
	    	if ( r.equals( "ROLE_PRODUCT_VIEW_LIST" ) )
	    		this.roleProductViewList = true;
	    	if ( r.equals( "ROLE_PRODUCT_VIEW_LISTXX" ) )
		    	this.roleProductViewListXX = true;
	    	if ( r.equals( "ROLE_PRODUCT_IMPORT_EXPORT" ) )
	    		this.roleProductImportExport = true;
	    	if ( r.equals( "ROLE_PRODUCT_FIELD_EDIT" ) )
	    		this.roleProductFieldEdit = true;
	    	// category
	    	if ( r.equals( "ROLE_CATEGORY_CREATE" ) )
	    		this.roleCategoryCreate = true;
	    	if ( r.equals( "ROLE_CATEGORY_EDIT" ) )
	    		this.roleCategoryEdit = true;
	    	if ( r.equals( "ROLE_CATEGORY_DELETE" ) )
	    		this.roleCategoryDelete = true;
	    	if ( r.equals( "ROLE_CATEGORY_UPDATE_RANK" ) )
	    		this.roleCategoryUpdateRank = true;
	    	if ( r.equals( "ROLE_CATEGORY_UPDATE_HEAD_TAG" ) )
	    		this.roleCategoryUpdateHeadTag = true;
	    	// sales rep
	    	if ( r.equals( "ROLE_SALES_REP_CREATE" ) )
	    		this.roleSalesRepCreate = true;
	    	if ( r.equals( "ROLE_SALES_REP_EDIT" ) )
	    		this.roleSalesRepEdit = true;
	    	if ( r.equals( "ROLE_SALES_REP_DELETE" ) )
	    		this.roleSalesRepDelete = true;
	    	if ( r.equals( "ROLE_SALES_REP_VIEW_LIST" ) )
			    this.roleSalesRepViewList = true;
	    	// store locator
	    	if ( r.equals( "ROLE_STORE_LOCATER_CREATE" ) )
	    		this.roleStoreLocatorCreate = true;
	    	if ( r.equals( "ROLE_STORE_LOCATER_IMPORT_EXPORT" ) )
	    		this.roleStoreLocatorImportExport = true;
	    	if ( r.equals( "ROLE_STORE_LOCATER_UPDATE" ) )
	    		this.roleStoreLocatorUpdate = true;
	    	if ( r.equals( "ROLE_STORE_LOCATER_DELETE" ) )
	    		this.roleStoreLocatorDelete = true;
	    	if ( r.equals( "ROLE_STORE_LOCATER_VIEW_LIST" ) )
			    this.roleStoreLocatorViewList = true;
	    	// layout
	    	if ( r.equals( "ROLE_LAYOUT_EDIT" ) )
	    		this.roleLayoutEdit = true;
	    	if ( r.equals( "ROLE_LAYOUT_HEAD_TAG" ) )
	    		this.roleLayoutHeadTag = true;
	    	// promotion
	    	if ( r.equals( "ROLE_PROMOTION_CREATE" ) )
	    		this.rolePromotionCreate = true;
	    	if ( r.equals( "ROLE_PROMOTION_EDIT" ) )
		    	this.rolePromotionEdit = true;
		    if ( r.equals( "ROLE_PROMOTION_DELETE" ) )
			    this.rolePromotionDelete = true;
		    // sales tag
	    	if ( r.equals( "ROLE_SALES_TAG_CREATE" ) )
	    		this.roleSalesTagCreate = true;
		    if ( r.equals( "ROLE_SALES_TAG_EDIT" ) )
		    	this.roleSalesTagEdit = true;
			if ( r.equals( "ROLE_SALES_TAG_DELETE" ) )
			    this.roleSalesTagDelete = true;
			// mailInRebate
	    	if ( r.equals( "ROLE_MAILINREBATE_CREATE" ) )
	    		this.roleMailInRebateCreate = true;
			if ( r.equals( "ROLE_MAILINREBATE_DELETE" ) )
			    this.roleMailInRebateDelete = true;
			// deal
	    	if ( r.equals( "ROLE_DEAL_CREATE" ) )
	    		this.roleDealCreate = true;
			if ( r.equals( "ROLE_DEAL_DELETE" ) )
			    this.roleDealDelete = true;
			// faq
			if ( r.equals( "ROLE_FAQ_CREATE" ) )
			    this.roleFAQCreate = true;
			if ( r.equals( "ROLE_FAQ_EDIT" ) )
				this.roleFAQEdit = true;
		    if ( r.equals( "ROLE_FAQ_DELETE" ) )
			    this.roleFAQDelete = true;
		    // policy
		    if ( r.equals( "ROLE_POLICY_CREATE" ) )
			    this.rolePolicyCreate = true;
			if ( r.equals( "ROLE_POLICY_EDIT" ) )
				this.rolePolicyEdit = true;
		    if ( r.equals( "ROLE_POLICY_DELETE" ) )
			    this.rolePolicyDelete = true;	 
		    // ticket
		    if ( r.equals( "ROLE_TICKET_VIEW_LIST" ) )
			    this.roleTicketViewList = true;
			if ( r.equals( "ROLE_TICKET_UPDATE" ) )
				this.roleTicketUpdate = true;
			if ( r.equals( "ROLE_TICKET_CREATE" ) )
				this.roleTicketCreate = true;
		    // purchase order
		    if ( r.equals( "ROLE_PURCHASE_ORDER_VIEW_LIST" ) )
			    this.rolePurchaseOrderViewList = true;
			if ( r.equals( "ROLE_PURCHASE_ORDER_CREATE" ) )
				this.rolePurchaseOrderCreate = true;	
			if ( r.equals( "ROLE_PURCHASE_ORDER_UPDATE" ) )
				this.rolePurchaseOrderUpdate = true;
			// inventory 
			if ( r.equals( "ROLE_INVENTORY_IMPORT_EXPORT" ) )
				this.roleInventoryImportExport = true;
			// echosign 
			if ( r.equals( "ROLE_ECHOSIGN_VIEW" ) )
				this.roleEchoSignView = true;
			// drop ship
	    	if ( r.equals( "ROLE_DROP_SHIP_CREATE" ) )
	    		this.roleDropShipCreate = true;
	    	// Supplier
	    	if ( r.equals( "ROLE_SUPPLIER_CREATE" ) )
			    this.roleSupplierCreate = true;
			if ( r.equals( "ROLE_SUPPLIER_UPDATE" ) )
				this.roleSupplierUpdate = true;	
			if ( r.equals( "ROLE_SUPPLIER_DELETE" ) )
				this.roleSupplierDelete = true;	
			if ( r.equals( "ROLE_SUPPLIER_VIEW_LIST" ) )
				this.roleSupplierViewList = true;
			// Service
			if ( r.equals( "ROLE_SERVICE_VIEW_LIST" ) )
				this.roleServiceViewList = true;
			// reports
	    	if ( r.equals( "ROLE_REPORT_CUSTOMERS" ) )
	    		this.roleReportCustomers = true;
	    	if ( r.equals( "ROLE_REPORT_SHIPPED_ORDER" ) )
	    		this.roleReportShippedOrder = true;
	    	if ( r.equals( "ROLE_REPORT_ORDER_NEED_ATTENTION" ) )
	    		this.roleReportOrdersNeedAttention = true;
	    	if ( r.equals( "ROLE_REPORT_ORDER_OVERVIEW" ) )
	    		this.roleReportOrdersOverview = true;
	    	if ( r.equals( "ROLE_REPORT_SHIPPED_OVERVIEW" ) )
	    		this.roleReportShippedOverview = true;
	    	if ( r.equals( "ROLE_REPORT_ORDER_PEN_PROC" ) )
	    		this.roleReportOrderPenProc = true;
	    	if ( r.equals( "ROLE_REPORT_SHIPPED_DAILY" ) )
	    		this.roleReportShippedDaily = true;
	    	if ( r.equals( "ROLE_REPORT_SALESREP" ) )
	    		this.roleReportSalesRep = true;
	    	if ( r.equals( "ROLE_REPORT_SALESREP_DAILY" ) )
	    		this.rolereportSalesRepDaily = true;
	    	if ( r.equals( "ROLE_REPORT_SALESREP_OVERVIEWII" ) )
	    		this.roleReportSalesRepOverviewII = true;
	    	if ( r.equals( "ROLE_REPORT_CUSTOMER_METRIC" ) )
	    		this.roleReportCustomerMetric=true;
	    	if ( r.equals( "ROLE_REPORT_TRACKCODE" ) )
	    		this.roleReportTrackCode = true;
	    	if ( r.equals( "ROLE_REPORT_IMP_EXP" ) )
	    		this.roleReportImpExp = true;
	    	if ( r.equals( "ROLE_REPORT_AFFILIATE_COMMISSION" ))
	    		this.roleReportAffiliateCommission = true;
	    	if ( r.equals( "ROLE_REPORT_PRODUCT" ))
	    		this.roleReportProduct = true;
	    	if ( r.equals( "ROLE_REPORT_INVENTORY" ))
	    		this.roleReportInventory = true;
	    	if ( r.equals( "ROLE_REPORT_PROCESSING" ))
	    		this.roleReportProcessing = true;
	    	if ( r.equals( "ROLE_REPORT_ORDERS_DAILY" ))
	    		this.roleReportOrdersDaily = true;
	    	if ( r.equals( "ROLE_REPORT_ABANDONED_SHOPPING_CART" ))
	    		this.roleReportAbandonedShoppingCart = true;
	    	
	    	// TruckLoadProcessing
	    	if ( r.equals( "ROLE_PROCESSING_CREATE" ))
	    		this.roleProcessingCreate = true;
	    	if ( r.equals( "ROLE_PROCESSING_UPDATE" ))
	    		this.roleProcessingUpdate = true;
	    	if ( r.equals( "ROLE_PROCESSING_DELETE" ))
	    		this.roleProcessingDelete = true;
	    	if ( r.equals( "ROLE_PROCESSING_VIEW_LIST" ))
	    		this.roleProcessingViewList = true;
	    	if ( r.equals( "ROLE_REPORT_LAY" ))
	    		this.roleReportLAY = true;
	    	if ( r.equals( "ROLE_REPORT_EVENT_MEMBER_LIST" ))
		    	this.roleReportEventMemberList = true;
	    	
	    	// email Manager
	    	if ( r.equals( "ROLE_EMAILMANAGER_CREATE" ) )
	    		this.roleEmailManagerCreate = true;
	    	if ( r.equals( "ROLE_EMAILMANAGER_VIEW_HISTORY" ) )
	    		this.roleEmailManagerViewHistory = true;
	    	if ( r.equals( "ROLE_EMAILMANAGER_BUY_CREDIT" ) )
	    		this.roleEmailManagerBuyCredit = true;
	    	// event
	    	if ( r.equals( "ROLE_EVENT_CREATE" ) )
	    		this.roleEventCreate = true;
	    	if ( r.equals( "ROLE_EVENT_UPDATE" ) )
	    		this.roleEventUpdate = true;
	    	if ( r.equals( "ROLE_EVENT_DELETE" ) )
	    		this.roleEventDelete = true;
	    	if ( r.equals( "ROLE_EVENT_VIEW_LIST" ) )
	    		this.roleEventViewList = true;
	    	if ( r.equals( "ROLE_EVENT_MEMBER_VIEW_LIST" ) )
	    		this.roleEventMemberViewList = true;
	    	// crm
	    	if (r.equals( "ROLE_CRM" ) )
	    		this.roleCRM = true;
	    	if (r.equals( "ROLE_CRM_FIELD" ) )
	    		this.roleCRMField = true;
	    	if (r.equals( "ROLE_CRM_GROUP" ) )
	    		this.roleCRMGroup = true;
	    	if (r.equals( "ROLE_CRM_FORM" ) )
	    		this.roleCRMForm = true;
	    	if (r.equals( "ROLE_CRM_IMPORT_EXPORT" ) )
	    		this.roleCRMImportExport = true;
	    	//Data Feed
	    	if ( r.equals( "ROLE_DATA_FEED" ) )
    		   this.roleDataFeed = true;
	    	//Quick Export
	    	if ( r.equals( "ROLE_QUICK_EXPORT"))
	    		this.roleQuickExport= true;	    	
			}
	    		    	
	}
	public boolean isRoleCategoryCreate()
	{
		return roleCategoryCreate;
	}
	public void setRoleCategoryCreate(boolean roleCategoryCreate)
	{
		this.roleCategoryCreate = roleCategoryCreate;
	}
	public boolean isRoleCategoryDelete()
	{
		return roleCategoryDelete;
	}
	public void setRoleCategoryDelete(boolean roleCategoryDelete)
	{
		this.roleCategoryDelete = roleCategoryDelete;
	}
	public boolean isRoleCategoryEdit()
	{
		return roleCategoryEdit;
	}
	public void setRoleCategoryEdit(boolean roleCategoryEdit)
	{
		this.roleCategoryEdit = roleCategoryEdit;
	}
	public boolean isRoleCategoryUpdateRank()
	{
		return roleCategoryUpdateRank;
	}
	public void setRoleCategoryUpdateRank(boolean roleCategoryUpdateRank)
	{
		this.roleCategoryUpdateRank = roleCategoryUpdateRank;
	}
	public boolean isRoleCustomerCreate()
	{
		return roleCustomerCreate;
	}
	public void setRoleCustomerCreate(boolean roleCustomerCreate)
	{
		this.roleCustomerCreate = roleCustomerCreate;
	}
	public boolean isRoleCustomerDelete()
	{
		return roleCustomerDelete;
	}
	public void setRoleCustomerDelete(boolean roleCustomerDelete)
	{
		this.roleCustomerDelete = roleCustomerDelete;
	}
	public boolean isRoleCustomerEdit()
	{
		return roleCustomerEdit;
	}
	public void setRoleCustomerEdit(boolean roleCustomerEdit)
	{
		this.roleCustomerEdit = roleCustomerEdit;
	}
	public boolean isRoleCustomerImportExport()
	{
		return roleCustomerImportExport;
	}
	public void setRoleCustomerImportExport(boolean roleCustomerImportExport)
	{
		this.roleCustomerImportExport = roleCustomerImportExport;
	}
	public boolean isRoleCustomerLoginAs()
	{
		return roleCustomerLoginAs;
	}
	public void setRoleCustomerLoginAs(boolean roleCustomerLoginAs)
	{
		this.roleCustomerLoginAs = roleCustomerLoginAs;
	}
	public boolean isRoleFAQCreate()
	{
		return roleFAQCreate;
	}
	public void setRoleFAQCreate(boolean roleFAQCreate)
	{
		this.roleFAQCreate = roleFAQCreate;
	}
	public boolean isRoleFAQDelete()
	{
		return roleFAQDelete;
	}
	public void setRoleFAQDelete(boolean roleFAQDelete)
	{
		this.roleFAQDelete = roleFAQDelete;
	}
	public boolean isRoleFAQEdit()
	{
		return roleFAQEdit;
	}
	public void setRoleFAQEdit(boolean roleFAQEdit)
	{
		this.roleFAQEdit = roleFAQEdit;
	}
	public boolean isRoleInvoiceEdit()
	{
		return roleInvoiceEdit;
	}
	public void setRoleInvoiceEdit(boolean roleInvoiceEdit)
	{
		this.roleInvoiceEdit = roleInvoiceEdit;
	}
	public boolean isRoleShippedInvoiceEdit()
	{
		return roleShippedInvoiceEdit;
	}
	public void setRoleShippedInvoiceEdit(boolean roleShippedInvoiceEdit)
	{
		this.roleShippedInvoiceEdit = roleShippedInvoiceEdit;
	}
	public boolean isRoleInvoiceStatus()
	{
		return roleInvoiceStatus;
	}
	public void setRoleInvoiceStatus(boolean roleInvoiceStatus)
	{
		this.roleInvoiceStatus = roleInvoiceStatus;
	}
	public boolean isRoleInvoiceViewList()
	{
		return roleInvoiceViewList;
	}
	public void setRoleInvoiceViewList(boolean roleInvoiceViewList)
	{
		this.roleInvoiceViewList = roleInvoiceViewList;
	}
	public boolean isRoleLayoutEdit()
	{
		return roleLayoutEdit;
	}
	public void setRoleLayoutEdit(boolean roleLayoutEdit)
	{
		this.roleLayoutEdit = roleLayoutEdit;
	}
	public boolean isRolePolicyCreate()
	{
		return rolePolicyCreate;
	}
	public void setRolePolicyCreate(boolean rolePolicyCreate)
	{
		this.rolePolicyCreate = rolePolicyCreate;
	}
	public boolean isRolePolicyDelete()
	{
		return rolePolicyDelete;
	}
	public void setRolePolicyDelete(boolean rolePolicyDelete)
	{
		this.rolePolicyDelete = rolePolicyDelete;
	}
	public boolean isRolePolicyEdit()
	{
		return rolePolicyEdit;
	}
	public void setRolePolicyEdit(boolean rolePolicyEdit)
	{
		this.rolePolicyEdit = rolePolicyEdit;
	}
	public boolean isRoleProductCreate()
	{
		return roleProductCreate;
	}
	public void setRoleProductCreate(boolean roleProductCreate)
	{
		this.roleProductCreate = roleProductCreate;
	}
	public boolean isRoleProductDelete()
	{
		return roleProductDelete;
	}
	public void setRoleProductDelete(boolean roleProductDelete)
	{
		this.roleProductDelete = roleProductDelete;
	}
	public boolean isRoleProductEdit()
	{
		return roleProductEdit;
	}
	public void setRoleProductEdit(boolean roleProductEdit)
	{
		this.roleProductEdit = roleProductEdit;
	}
	public boolean isRoleProductFieldEdit()
	{
		return roleProductFieldEdit;
	}
	public void setRoleProductFieldEdit(boolean roleProductFieldEdit)
	{
		this.roleProductFieldEdit = roleProductFieldEdit;
	}
	public boolean isRoleProductImportExport()
	{
		return roleProductImportExport;
	}
	public void setRoleProductImportExport(boolean roleProductImportExport)
	{
		this.roleProductImportExport = roleProductImportExport;
	}
	public boolean isRoleProductViewList()
	{
		return roleProductViewList;
	}
	public void setRoleProductViewList(boolean roleProductViewList)
	{
		this.roleProductViewList = roleProductViewList;
	}
	public boolean isRolePromotionCreate()
	{
		return rolePromotionCreate;
	}
	public void setRolePromotionCreate(boolean rolePromotionCreate)
	{
		this.rolePromotionCreate = rolePromotionCreate;
	}
	public boolean isRolePromotionDelete()
	{
		return rolePromotionDelete;
	}
	public void setRolePromotionDelete(boolean rolePromotionDelete)
	{
		this.rolePromotionDelete = rolePromotionDelete;
	}
	public boolean isRolePromotionEdit()
	{
		return rolePromotionEdit;
	}
	public void setRolePromotionEdit(boolean rolePromotionEdit)
	{
		this.rolePromotionEdit = rolePromotionEdit;
	}
	public boolean isRoleSalesRepCreate()
	{
		return roleSalesRepCreate;
	}
	public void setRoleSalesRepCreate(boolean roleSalesRepCreate)
	{
		this.roleSalesRepCreate = roleSalesRepCreate;
	}
	public boolean isRoleSalesRepDelete()
	{
		return roleSalesRepDelete;
	}
	public void setRoleSalesRepDelete(boolean roleSalesRepDelete)
	{
		this.roleSalesRepDelete = roleSalesRepDelete;
	}
	public boolean isRoleSalesRepEdit()
	{
		return roleSalesRepEdit;
	}
	public void setRoleSalesRepEdit(boolean roleSalesRepEdit)
	{
		this.roleSalesRepEdit = roleSalesRepEdit;
	}
	public boolean isRoleSalesTagCreate()
	{
		return roleSalesTagCreate;
	}
	public void setRoleSalesTagCreate(boolean roleSalesTagCreate)
	{
		this.roleSalesTagCreate = roleSalesTagCreate;
	}
	public boolean isRoleSalesTagDelete()
	{
		return roleSalesTagDelete;
	}
	public void setRoleSalesTagDelete(boolean roleSalesTagDelete)
	{
		this.roleSalesTagDelete = roleSalesTagDelete;
	}
	public boolean isRoleSalesTagEdit()
	{
		return roleSalesTagEdit;
	}
	public void setRoleSalesTagEdit(boolean roleSalesTagEdit)
	{
		this.roleSalesTagEdit = roleSalesTagEdit;
	}
	public boolean isRoleMailInRebateCreate() {
		return roleMailInRebateCreate;
	}
	public void setRoleMailInRebateCreate(boolean roleMailInRebateCreate) {
		this.roleMailInRebateCreate = roleMailInRebateCreate;
	}
	public boolean isRoleMailInRebateDelete() {
		return roleMailInRebateDelete;
	}
	public boolean isRoleDealCreate() {
		return roleDealCreate;
	}
	public void setRoleDealCreate(boolean roleDealCreate) {
		this.roleDealCreate = roleDealCreate;
	}
	public boolean isRoleDealDelete() {
		return roleDealDelete;
	}
	public void setRoleDealDelete(boolean roleDealDelete) {
		this.roleDealDelete = roleDealDelete;
	}
	public void setRoleMailInRebateDelete(boolean roleMailInRebateDelete) {
		this.roleMailInRebateDelete = roleMailInRebateDelete;
	}
	public boolean isRoleCustomerMyListAdd()
	{
		return roleCustomerMyListAdd;
	}
	public void setRoleCustomerMyListAdd(boolean roleCustomerMyListAdd)
	{
		this.roleCustomerMyListAdd = roleCustomerMyListAdd;
	}
	public boolean isRoleCustomerMyListDelete()
	{
		return roleCustomerMyListDelete;
	}
	public void setRoleCustomerMyListDelete(boolean roleCustomerMyListDelete)
	{
		this.roleCustomerMyListDelete = roleCustomerMyListDelete;
	}
	public boolean isRoleCustomerMyListView()
	{
		return roleCustomerMyListView;
	}
	public void setRoleCustomerMyListView(boolean roleCustomerMyListView)
	{
		this.roleCustomerMyListView = roleCustomerMyListView;
	}
	public boolean isRoleCustomerAddInvoice()
	{
		return roleCustomerAddInvoice;
	}
	public void setRoleCustomerAddInvoice(boolean roleCustomerAddInvoice)
	{
		this.roleCustomerAddInvoice = roleCustomerAddInvoice;
	}
	public boolean isRoleCustomerViewList()
	{
		return roleCustomerViewList;
	}
	public void setRoleCustomerViewList(boolean roleCustomerViewList)
	{
		this.roleCustomerViewList = roleCustomerViewList;
	}
	public boolean isRoleCustomerField() {
		return roleCustomerField;
	}
	public void setRoleCustomerViewField(boolean roleCustomerField) {
		this.roleCustomerField = roleCustomerField;
	}
	public boolean isRoleCustomerAffiliate() {
		return roleCustomerAffiliate;
	}
	public void setRoleCustomerAffiliate(boolean roleCustomerAffiliate) {
		this.roleCustomerAffiliate = roleCustomerAffiliate;
	}
	public boolean isRoleLayoutHeadTag()
	{
		return roleLayoutHeadTag;
	}
	public void setRoleLayoutHeadTag(boolean roleLayoutHeadTag)
	{
		this.roleLayoutHeadTag = roleLayoutHeadTag;
	}
	public boolean isRoleTicketViewList()
	{
		return roleTicketViewList;
	}
	public void setRoleTicketViewList(boolean roleTicketViewList)
	{
		this.roleTicketViewList = roleTicketViewList;
	}
	public boolean isRoleTicketUpdate()
	{
		return roleTicketUpdate;
	}
	public void setRoleTicketUpdate(boolean roleTicketUpdate)
	{
		this.roleTicketUpdate = roleTicketUpdate;
	}
	public boolean isRoleTicketCreate() {
		return roleTicketCreate;
	}
	public void setRoleTicketCreate(boolean roleTicketCreate) {
		this.roleTicketCreate = roleTicketCreate;
	}
	public boolean isRolePurchaseOrderUpdate()
	{
		return rolePurchaseOrderUpdate;
	}
	public void setRolePurchaseOrderUpdate(boolean rolePurchaseOrderUpdate)
	{
		this.rolePurchaseOrderUpdate = rolePurchaseOrderUpdate;
	}
	public boolean isRolePurchaseOrderCreate()
	{
		return rolePurchaseOrderCreate;
	}
	public void setRolePurchaseOrderCreate(boolean rolePurchaseOrderCreate)
	{
		this.rolePurchaseOrderCreate = rolePurchaseOrderCreate;
	}
	public boolean isRolePurchaseOrderViewList()
	{
		return rolePurchaseOrderViewList;
	}
	public void setRolePurchaseOrderViewList(boolean rolePurchaseOrderViewList)
	{
		this.rolePurchaseOrderViewList = rolePurchaseOrderViewList;
	}
	public boolean isRoleInventoryImportExport() {
		return roleInventoryImportExport;
	}
	public boolean isRoleEchoSignView() {
		return roleEchoSignView;
	}
	public void setRoleInventoryImportExport(boolean roleInventoryImportExport) {
		this.roleInventoryImportExport = roleInventoryImportExport;
	}
	public void setRoleEchoSignView(boolean roleEchoSignView) {
		this.roleEchoSignView = roleEchoSignView;
	}
	public boolean isRoleSalesRepViewList()
	{
		return roleSalesRepViewList;
	}
	public void setRoleSalesRepViewList(boolean roleSalesRepViewList)
	{
		this.roleSalesRepViewList = roleSalesRepViewList;
	}
	public boolean isRoleInvoicePackingList()
	{
		return roleInvoicePackingList;
	}
	public void setRoleInvoicePackingList(boolean roleInvoicePackingList)
	{
		this.roleInvoicePackingList = roleInvoicePackingList;
	}
	public boolean isRoleDropShipCreate()
	{
		return roleDropShipCreate;
	}
	public void setRoleDropShipCreate(boolean roleDropShipCreate)
	{
		this.roleDropShipCreate = roleDropShipCreate;
	}
	public boolean isRoleInvoiceImportExport()
	{
		return roleInvoiceImportExport;
	}
	public void setRoleInvoiceImportExport(boolean roleInvoiceImportExport)
	{
		this.roleInvoiceImportExport = roleInvoiceImportExport;
	}
	public boolean isRoleCategoryUpdateHeadTag()
	{
		return roleCategoryUpdateHeadTag;
	}
	public void setRoleCategoryUpdateHeadTag(boolean roleCategoryUpdateHeadTag)
	{
		this.roleCategoryUpdateHeadTag = roleCategoryUpdateHeadTag;
	}
	public boolean isRoleSupplierCreate()
	{
		return roleSupplierCreate;
	}
	public void setRoleSupplierCreate(boolean roleSupplierCreate)
	{
		this.roleSupplierCreate = roleSupplierCreate;
	}
	public boolean isRoleSupplierUpdate()
	{
		return roleSupplierUpdate;
	}
	public void setRoleSupplierUpdate(boolean roleSupplierUpdate)
	{
		this.roleSupplierUpdate = roleSupplierUpdate;
	}
	public boolean isRoleSupplierViewList()
	{
		return roleSupplierViewList;
	}
	public void setRoleSupplierViewList(boolean roleSupplierViewList)
	{
		this.roleSupplierViewList = roleSupplierViewList;
	}
	public boolean isRoleSupplierDelete()
	{
		return roleSupplierDelete;
	}
	public void setRoleSupplierDelete(boolean roleSupplierDelete)
	{
		this.roleSupplierDelete = roleSupplierDelete;
	}
	public boolean isRoleServiceViewList()
	{
		return roleServiceViewList;
	}
	public void setRoleServiceViewList(boolean roleServiceViewList)
	{
		this.roleServiceViewList = roleServiceViewList;
	}
	public boolean isRoleReportOrdersNeedAttention()
	{
		return roleReportOrdersNeedAttention;
	}
	public void setRoleReportOrdersNeedAttention(boolean roleReportOrdersNeedAttention)
	{
		this.roleReportOrdersNeedAttention = roleReportOrdersNeedAttention;
	}
	public boolean isRoleReportOrdersOverview()
	{
		return roleReportOrdersOverview;
	}
	public void setRoleReportOrdersOverview(boolean roleReportOrdersOverview)
	{
		this.roleReportOrdersOverview = roleReportOrdersOverview;
	}
	public boolean isRoleReportSalesRep()
	{
		return roleReportSalesRep;
	}
	public void setRoleReportSalesRep(boolean roleReportSalesRep)
	{
		this.roleReportSalesRep = roleReportSalesRep;
	}
	public boolean isRolereportSalesRepDaily()
	{
		return rolereportSalesRepDaily;
	}
	public void setRolereportSalesRepDaily(boolean rolereportSalesRepDaily)
	{
		this.rolereportSalesRepDaily = rolereportSalesRepDaily;
	}
	public boolean isRoleReportSalesRepOverviewII()
	{
		return roleReportSalesRepOverviewII;
	}
	public void setRoleReportSalesRepOverviewII(boolean roleReportSalesRepOverviewII)
	{
		this.roleReportSalesRepOverviewII = roleReportSalesRepOverviewII;
	}
	public boolean isRoleReportTrackCode()
	{
		return roleReportTrackCode;
	}
	public void setRoleReportTrackCode(boolean roleReportTrackCode)
	{
		this.roleReportTrackCode = roleReportTrackCode;
	}
	public boolean isRoleReportImpExp()
	{
		return roleReportImpExp;
	}
	public void setRoleReportImpExp(boolean roleReportImpExp)
	{
		this.roleReportImpExp = roleReportImpExp;
	}
	public boolean isRoleReportShippedOrder()
	{
		return roleReportShippedOrder;
	}
	public void setRoleReportShippedOrder(boolean roleReportShippedOrder)
	{
		this.roleReportShippedOrder = roleReportShippedOrder;
	}
	public boolean isRoleReportShippedOverview()
	{
		return roleReportShippedOverview;
	}
	public void setRoleReportShippedOverview(boolean roleReportShippedOverview)
	{
		this.roleReportShippedOverview = roleReportShippedOverview;
	}
	public boolean isRoleReportOrderPenProc()
	{
		return roleReportOrderPenProc;
	}
	public void setRoleReportOrderPenProc(boolean roleReportOrderPenProc)
	{
		this.roleReportOrderPenProc = roleReportOrderPenProc;
	}
	public boolean isRoleReportShippedDaily()
	{
		return roleReportShippedDaily;
	}
	public void setRoleReportShippedDaily(boolean roleReportShippedDaily)
	{
		this.roleReportShippedDaily = roleReportShippedDaily;
	}
	public boolean isRoleReportAffiliateCommission()
	{
		return roleReportAffiliateCommission;
	}
	public void setRoleReportAffiliateCommission(boolean roleReportAffiliateCommission)
	{
		this.roleReportAffiliateCommission = roleReportAffiliateCommission;
	}
	public boolean isRoleReportProduct()
	{
		return roleReportProduct;
	}
	public void setRoleReportProduct(boolean roleReportProduct)
	{
		this.roleReportProduct = roleReportProduct;
	}
	public void setRoleReportInventory(boolean roleReportInventory) {
		this.roleReportInventory = roleReportInventory;
	}
	public boolean isRoleReportInventory() {
		return roleReportInventory;
	}
	public boolean isRoleEventCreate() {
		return roleEventCreate;
	}
	public void setRoleEventCreate(boolean roleEventCreate) {
		this.roleEventCreate = roleEventCreate;
	}
	public boolean isRoleEventUpdate() {
		return roleEventUpdate;
	}
	public void setRoleEventUpdate(boolean roleEventUpdate) {
		this.roleEventUpdate = roleEventUpdate;
	}
	public boolean isRoleEventDelete() {
		return roleEventDelete;
	}
	public void setRoleEventDelete(boolean roleEventDelete) {
		this.roleEventDelete = roleEventDelete;
	}
	public boolean isRoleEventViewList() {
		return roleEventViewList;
	}
	public void setRoleEventViewList(boolean roleEventViewList) {
		this.roleEventViewList = roleEventViewList;
	}
	public boolean isRoleEventMemberViewList() {
		return roleEventMemberViewList;
	}
	public void setRoleEventMemberViewList(boolean roleEventMemberViewList) {
		this.roleEventMemberViewList = roleEventMemberViewList;
	}
	public boolean isRoleCRM() {
		return roleCRM;
	}
	public void setRoleCRM(boolean roleCRM) {
		this.roleCRM = roleCRM;
	}
	public boolean isRoleCRMForm() {
		return roleCRMForm;
	}
	public void setRoleCRMForm(boolean roleCRMForm) {
		this.roleCRMForm = roleCRMForm;
	}
	public boolean isRoleCRMField() {
		return roleCRMField;
	}
	public void setRoleCRMField(boolean roleCRMField) {
		this.roleCRMField = roleCRMField;
	}
	public boolean isRoleCRMGroup() {
		return roleCRMGroup;
	}
	public void setRoleCRMGroup(boolean roleCRMGroup) {
		this.roleCRMGroup = roleCRMGroup;
	}
	public void setRoleDataFeed(boolean roleDataFeed) {
		this.roleDataFeed = roleDataFeed;
	}
	public boolean isRoleDataFeed() {
		return roleDataFeed;
	}
	public void setRoleReportCustomerMetric(boolean roleReportCustomerMetric) {
		this.roleReportCustomerMetric = roleReportCustomerMetric;
	}
	public boolean isRoleReportCustomerMetric() {
		return roleReportCustomerMetric;
	}
	public boolean isRoleReportProcessing() {
		return roleReportProcessing;
	}
	public void setRoleReportProcessing(boolean roleReportProcessing) {
		this.roleReportProcessing = roleReportProcessing;
	}
	public boolean isRoleReportOrdersDaily() {
		return roleReportOrdersDaily;
	}
	public void setRoleReportOrdersDaily(boolean roleReportOrdersDaily) {
		this.roleReportOrdersDaily = roleReportOrdersDaily;
	}
	public boolean isRoleProcessingCreate() {
		return roleProcessingCreate;
	}
	public void setRoleProcessingCreate(boolean roleProcessingCreate) {
		this.roleProcessingCreate = roleProcessingCreate;
	}
	public boolean isRoleProcessingUpdate() {
		return roleProcessingUpdate;
	}
	public void setRoleProcessingUpdate(boolean roleProcessingUpdate) {
		this.roleProcessingUpdate = roleProcessingUpdate;
	}
	public boolean isRoleProcessingDelete() {
		return roleProcessingDelete;
	}
	public void setRoleProcessingDelete(boolean roleProcessingDelete) {
		this.roleProcessingDelete = roleProcessingDelete;
	}
	public boolean isRoleProcessingViewList() {
		return roleProcessingViewList;
	}
	public void setRoleProcessingViewList(boolean roleProcessingViewList) {
		this.roleProcessingViewList = roleProcessingViewList;
	}
	public boolean isRoleStoreLocatorUpdate() {
		return roleStoreLocatorUpdate;
	}
	public void setRoleStoreLocatorUpdate(boolean roleStoreLocatorUpdate) {
		this.roleStoreLocatorUpdate = roleStoreLocatorUpdate;
	}
	public boolean isRoleStoreLocatorCreate() {
		return roleStoreLocatorCreate;
	}
	public void setRoleStoreLocatorCreate(boolean roleStoreLocatorCreate) {
		this.roleStoreLocatorCreate = roleStoreLocatorCreate;
	}
	public boolean isRoleStoreLocatorDelete() {
		return roleStoreLocatorDelete;
	}
	public void setRoleStoreLocatorDelete(boolean roleStoreLocatorDelete) {
		this.roleStoreLocatorDelete = roleStoreLocatorDelete;
	}
	public boolean isRoleStoreLocatorViewList() {
		return roleStoreLocatorViewList;
	}
	public void setRoleStoreLocatorViewList(boolean roleStoreLocatorViewList) {
		this.roleStoreLocatorViewList = roleStoreLocatorViewList;
	}
	public boolean isRoleStoreLocatorImportExport() {
		return roleStoreLocatorImportExport;
	}
	public void setRoleStoreLocatorImportExport(boolean roleStoreLocatorImportExport) {
		this.roleStoreLocatorImportExport = roleStoreLocatorImportExport;
	}
	public boolean isRoleIncomingPaymentViewList() {
		return roleIncomingPaymentViewList;
	}
	public void setRoleIncomingPaymentViewList(boolean roleIncomingPaymentViewList) {
		this.roleIncomingPaymentViewList = roleIncomingPaymentViewList;
	}
	public boolean isRoleIncomingPaymentCreate() {
		return roleIncomingPaymentCreate;
	}
	public void setRoleIncomingPaymentCreate(boolean roleIncomingPaymentCreate) {
		this.roleIncomingPaymentCreate = roleIncomingPaymentCreate;
	}
	public boolean isRoleIncomingPaymentCancel() {
		return roleIncomingPaymentCancel;
	}
	public void setRoleIncomingPaymentCancel(boolean roleIncomingPaymentCancel) {
		this.roleIncomingPaymentCancel = roleIncomingPaymentCancel;
	}
	public boolean isRoleOutgoingPaymentViewList() {
		return roleOutgoingPaymentViewList;
	}
	public void setRoleOutgoingPaymentViewList(boolean roleOutgoingPaymentViewList) {
		this.roleOutgoingPaymentViewList = roleOutgoingPaymentViewList;
	}
	public boolean isRoleOutgoingPaymentCreate() {
		return roleOutgoingPaymentCreate;
	}
	public void setRoleOutgoingPaymentCreate(boolean roleOutgoingPaymentCreate) {
		this.roleOutgoingPaymentCreate = roleOutgoingPaymentCreate;
	}
	public boolean isRoleOutgoingPaymentCancel() {
		return roleOutgoingPaymentCancel;
	}
	public void setRoleOutgoingPaymentCancel(boolean roleOutgoingPaymentCancel) {
		this.roleOutgoingPaymentCancel = roleOutgoingPaymentCancel;
	}
	public boolean isRoleCustomerCreditUpdateDelete() {
		return roleCustomerCreditUpdateDelete;
	}
	public void setRoleCustomerCreditUpdateDelete(
			boolean roleCustomerCreditUpdateDelete) {
		this.roleCustomerCreditUpdateDelete = roleCustomerCreditUpdateDelete;
	}
	public boolean isRoleOverridePaymentAlert() {
		return roleOverridePaymentAlert;
	}
	public void setRoleOverridePaymentAlert(boolean roleOverridePaymentAlert) {
		this.roleOverridePaymentAlert = roleOverridePaymentAlert;
	}
	public boolean isRoleCRMImportExport() {
		return roleCRMImportExport;
	}
	public void setRoleCRMImportExport(boolean roleCRMImportExport) {
		this.roleCRMImportExport = roleCRMImportExport;
	}
	public boolean isRoleQuickExport() {
		return roleQuickExport;
	}
	public void setRoleQuickExport(boolean roleQuickExport) {
		this.roleQuickExport = roleQuickExport;
	}
	public boolean isRoleReportAbandonedShoppingCart() {
		return roleReportAbandonedShoppingCart;
	}
	public void setRoleReportAbandonedShoppingCart(boolean roleReportAbandonedShoppingCart) {
		this.roleReportAbandonedShoppingCart = roleReportAbandonedShoppingCart;
	}
	public boolean isRoleReportLAY() {
		return roleReportLAY;
	}
	public void setRoleReportLAY(boolean roleReportLAY) {
		this.roleReportLAY = roleReportLAY;
	}
	public boolean isRoleCustomerUpdateTrackCode() {
		return roleCustomerUpdateTrackCode;
	}
	public void setRoleCustomerUpdateTrackCode(boolean roleCustomerUpdateTrackCode) {
		this.roleCustomerUpdateTrackCode = roleCustomerUpdateTrackCode;
	}
	public boolean isRoleProductViewListXX() {
		return roleProductViewListXX;
	}
	public void setRoleProductViewListXX(boolean roleProductViewListXX) {
		this.roleProductViewListXX = roleProductViewListXX;
	}
	public boolean isRoleReportEventMemberList() {
		return roleReportEventMemberList;
	}
	public void setRoleReportEventMemberList(boolean roleReportEventMemberList) {
		this.roleReportEventMemberList = roleReportEventMemberList;
	}
	public boolean isRoleCustomerCheckboxEdit() {
		return roleCustomerCheckboxEdit;
	}
	public void setRoleCustomerCheckboxEdit(boolean roleCustomerCustomerEdit) {
		this.roleCustomerCheckboxEdit = roleCustomerCustomerEdit;
	}
	
}
