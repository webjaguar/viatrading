/* Copyright 2005, 2006 Advanced E-Media Solutions
 *
 */

package com.webjaguar.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.TreeMap;

public class CartItem implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private Product product = new Product();
	private int quantity;
	private int crossItemsQty;
	private Double unitPrice;
	private Double originalPrice;
	private Double discount;
	// typed by user
	private Double variableUnitPrice;
	private Double tempCartTotal;
	private Double tempItemTotal;
	private Double unitWeight;
	private Integer userId;
	private int index;
	private List<ProductAttribute> productAttributes;
	private int boxExtraQuantity;
	private double boxContentWeight;
	// inventory
	private String inventoryMessage;
	private String lowInventoryMessage;
	// subscription
	private String subscriptionInterval;
	private TreeMap<String, Integer> customLines;
	// minimumQty/incremental
	private String minimumIncrementalQtyMessage;
	private boolean showMinIncMessage;
	
	//Cart Index for API
	private String apiIndex;
	private Brand brand;
	private Manufacturer manufacturer;
	
	// custom frame
	private String customXml;
	private String customImageUrl;
	
	private Boolean includeRetailDisplay;
	private Date dateCreated;
	
	private String dealGroup;
	private boolean dealMainItem = false;
	// just used for calculating price where qty to display is different than calculating price
	// for example, setup charge ( qty to display is 1 but price is calculated based on main (shirts) qty.
	private Integer qtyForPriceBreak;
	private Integer qtyMultiplier;
	
	// group
	private String itemGroup;
	private boolean itemGroupMainItem = true;
	
	// asi
	private List<ProductAttribute> asiProductAttributes;
	private Double asiAdditionalCharge;
	private Double asiOriginalPrice;
	private Double asiUnitPrice;
	// product variant
	private String variantSku;
	// option fixed charge
	private Double optionFixedCharge;
	private Integer priceCasePackQty;
	private String shippingDays;
	
	private String attachment;
	
	public CartItem() {
		super();
	}
	public CartItem( int quantity, Double variableUnitPrice, Integer userId, int index ) {
		super();
		this.quantity = quantity;
		this.variableUnitPrice = variableUnitPrice;
		this.userId = userId;
		this.index = index;
	}
	
	public boolean isShowMinIncMessage() {
		return showMinIncMessage;
	}
	public void setShowMinIncMessage(boolean showMinIncMessage) {
		this.showMinIncMessage = showMinIncMessage;
	}
	public String getMinimumIncrementalQtyMessage() {
		return minimumIncrementalQtyMessage;
	}
	public void setMinimumIncrementalQtyMessage(String minimumIncrementalQtyMessage) {
		this.minimumIncrementalQtyMessage = minimumIncrementalQtyMessage;
	}
	public String getLowInventoryMessage() {
		return lowInventoryMessage;
	}
	public void setLowInventoryMessage(String lowInventoryMessage) {
		this.lowInventoryMessage = lowInventoryMessage;
	}
	public String getInventoryMessage() {
		return inventoryMessage;
	}
	public void setInventoryMessage(String inventoryMessage) {
		this.inventoryMessage = inventoryMessage;
	}
	public Product getProduct() { 
		return product;
	}
	public void setProduct(Product product) {
		this.product = product;
	}
	
	public int getQuantity() { 
		if ( this.variableUnitPrice != null )
			return this.quantity = 1;
		else
			return quantity; 
	}
	public void setQuantity(int quantity) {
		this.quantity = quantity;
	}

	public Double getUnitPrice() { 
		if ( this.variableUnitPrice != null )
			return this.variableUnitPrice;
		else if (unitPrice == null)
			return null; 
		else
			return unitPrice + this.getTotalPriceOption() + this.getBoxExtraPrice(); 
	}
	public void setUnitPrice(Double unitPrice) {
		this.unitPrice = unitPrice;
	}
	
	public int incrementQuantity(int quantity) {
		if (quantity < 1) {
			++this.quantity;
		} else {
			this.quantity += quantity;
		}
		return this.getQuantity();
	}

	public int incrementQuantity2(int quantity) {
		this.quantity += quantity;
		return this.getQuantity();
	}

	public int getNumOfPrices() {
		if (this.product.getPrice() != null) {
			return this.product.getPrice().size();
		}
		return 0;
	}

	public Integer getUserId() { 
		return userId;
	}
	public void setUserId(Integer userId) {
		this.userId = userId;
	}

	public List<ProductAttribute> getProductAttributes() {
		return productAttributes;
	}
	public void setProductAttributes(List<ProductAttribute> productAttributes) {
		this.productAttributes = productAttributes;
	}	
	public void addProductAttribute(ProductAttribute attr) {
		if (productAttributes == null) productAttributes = new ArrayList<ProductAttribute>();
		productAttributes.add(attr);
	}
	public void addAsiProductAttribute(ProductAttribute asiAttr) {
		if (asiProductAttributes == null) asiProductAttributes = new ArrayList<ProductAttribute>();
		asiProductAttributes.add(asiAttr);
	}
	
	public String toString() {
		StringBuffer sbuff = new StringBuffer();
		sbuff.append(this.getProduct().getId());
		//sbuff.append("-" + priceCasePackQty + "-");  uncomment this to separately add the same sku with CasePackPrice and without CasePackPrice
		sbuff.append("-" + variantSku + "-");	// custom sku		
		// subscription
		if (subscriptionInterval != null && !subscriptionInterval.trim().equals("")) {
			sbuff.append("-" + subscriptionInterval + "-");
		}
		// get product attributes
		if (this.getProductAttributes() != null) {
			for (ProductAttribute prodAttr:this.getProductAttributes()) {
				sbuff.append("{" + prodAttr.getOptionCode() + "}");
				sbuff.append("{" + prodAttr.getOptionIndex() + "}");
				sbuff.append(prodAttr.getValueIndex());
				sbuff.append("(" + prodAttr.getOptionCusValue() + ")");
				sbuff.append("(" + prodAttr.getOptionBoxValue() + ")");
				sbuff.append("(" + prodAttr.isDealItem() + ")");
				sbuff.append("(" + prodAttr.getAsiOptionName() + ")");
				sbuff.append("(" + prodAttr.getAsiOptionValue() + ")");
				
				// check if attribute is custom attribute
				if ( prodAttr.isCustomAttribute() ) {
					sbuff.append("(" + prodAttr.getOptionName() + ")");
					sbuff.append("(" + prodAttr.getValueName() + ")");
				}
			}			
		}
		// get ASI product attributes
		if (this.getAsiProductAttributes() != null) {
			for (ProductAttribute asiProdAttr:this.getAsiProductAttributes()) {
				sbuff.append("(" + asiProdAttr.getAsiOptionName() + ")");
				sbuff.append("(" + asiProdAttr.getAsiOptionValue() + ")");
			}			
		}
		
		// check if group is available
		if ( this.itemGroup != null ) {
			sbuff.append("(" + this.itemGroup + ")");
		}
		return sbuff.toString();
	}
	public int getIndex() {
		return index;
	}
	public void setIndex(int index) {
		this.index = index;
	}
	public Double getTotalWeight() {
		if (unitWeight == null) return null;
		//getCasePackQty always returns 1 or more
		Double totalWeight = new Double(this.getUnitWeight().doubleValue() *  quantity * getCasePackQty());
		if (this.getProduct().getCaseContent() != null) {
			totalWeight = totalWeight * getCaseContentQty();			
		}
		return totalWeight;

	}
	private int getCaseContentQty() {
		int caseContentQty = 1;
		if (product.getCaseContent() != null) {
			caseContentQty = product.getCaseContent();
		} 
		return caseContentQty;
	}
	private int getCasePackQty() {
		int casePackQty = 1;
		if (product.getPriceCasePackQty() != null && product.isPriceCasePack()) {
			casePackQty = product.getPriceCasePackQty();
		}
		return casePackQty;
	}
	public Double getTotal () {
		if (unitPrice == null) return null;
		Double total = new Double(this.getUnitPrice().doubleValue() * getCaseContentQty() * getCasePackQty() * quantity);
		
		if(this.getAsiAdditionalCharge() != null) {
			total = total +this.getAsiAdditionalCharge();
		}
		if(this.getOptionFixedCharge() != null) {
			total = total +this.getOptionFixedCharge();
		}
		return total;
	}
	public Double getTotalVariablePrice () {
		if (variableUnitPrice == null) return null;
		return new Double(this.getVariableUnitPrice().doubleValue() );
	}
	public double getTotalPriceOption() {
		double totalPriceOption = 0.0;
		if (this.getProductAttributes() != null) {
			for (ProductAttribute prodAttr:this.getProductAttributes()) {
				if ( prodAttr.getOptionPrice() != null && !prodAttr.isOneTimePrice() ) {
					totalPriceOption += prodAttr.getOptionPrice();
				}
			}
		}
		return totalPriceOption;
	}
	public double getTotalOriginalPriceOption() {
		double totalPriceOption = 0.0;
		if (this.getProductAttributes() != null) {
			for (ProductAttribute prodAttr:this.getProductAttributes()) {
				if (prodAttr.getOptionPriceOriginal() != null) {
					totalPriceOption += prodAttr.getOptionPriceOriginal();
				} else if (prodAttr.getOptionPrice() != null) {
					totalPriceOption += prodAttr.getOptionPrice();
				}
			}
		}
		return totalPriceOption;
	}	
	public double getTotalWeightOption() {
		double totalWeightOption = 0.0;
		if (this.getProductAttributes() != null) {
			for (ProductAttribute prodAttr:this.getProductAttributes()) {
				if ( prodAttr.getOptionWeight() != null ) {
					totalWeightOption += prodAttr.getOptionWeight();
				}
			}
		}
		return totalWeightOption;
	}
	public Double getBoxExtraPrice() {
		double totalBoxExtraPrice = 0.0;
		if ( this.product.getBoxExtraAmt() != null && this.product.getBoxSize() != null ) {
			totalBoxExtraPrice = this.getBoxExtraQuantity() * this.product.getBoxExtraAmt();
			if ( product.getSalesTag() != null && product.getSalesTag().isInEffect() && product.getSalesTag().isPercent() ) {
				totalBoxExtraPrice = totalBoxExtraPrice - ( totalBoxExtraPrice * product.getSalesTag().getDiscount()/100);
			}
		}
		return totalBoxExtraPrice;
	}
	
	public Double getUnitWeight() {
		return unitWeight + this.getBoxContentWeight() + this.getTotalWeightOption();
	}
	public void setUnitWeight(Double unitWeight) {
		this.unitWeight = unitWeight;
	}
	public Double getVariableUnitPrice() {
		return variableUnitPrice;
	}
	public void setVariableUnitPrice(Double variableUnitPrice) {
		this.variableUnitPrice = variableUnitPrice;
	}
	public int getBoxExtraQuantity() {
		return boxExtraQuantity;
	}
	public void setBoxExtraQuantity(int boxExtraQuantity) {
		this.boxExtraQuantity = boxExtraQuantity;
	}
	public double getBoxContentWeight() {
		return boxContentWeight;
	}
	public void setBoxContentWeight(double boxContentWeight) {
		this.boxContentWeight = boxContentWeight;
	}
	public String getSubscriptionInterval() {
		return subscriptionInterval;
	}
	public void setSubscriptionInterval(String subscriptionInterval) {
		this.subscriptionInterval = subscriptionInterval;
	}
	public TreeMap<String, Integer> getCustomLines() {
		return customLines;
	}
	public void setCustomLines(TreeMap<String, Integer> customLines) {
		this.customLines = customLines;
	}
	public void addCustomLine(String customLine, int quantity) {
		if (customLines == null) {
			customLines = new TreeMap<String, Integer>();
		}
		if (customLines.containsKey(customLine)) {
			customLines.put(customLine, customLines.get(customLine)+quantity);
		} else {
			customLines.put(customLine, quantity);
		}
	}
	public int getCrossItemsQty() {
		return crossItemsQty;
	}
	public void setCrossItemsQty(int crossItemsQty) {
		this.crossItemsQty = crossItemsQty;
	}
	public Brand getBrand() {
		return brand;
	}
	public void setBrand(Brand brand) {
		this.brand = brand;
	}
	public Manufacturer getManufacturer() {
		return manufacturer;
	}
	public void setManufacturer(Manufacturer manufacturer) {
		this.manufacturer = manufacturer;
	}
	public String getCustomXml() {
		return customXml;
	}
	public void setCustomXml(String customXml) {
		this.customXml = customXml;
	}
	public String getCustomImageUrl() {
		return customImageUrl;
	}
	public void setCustomImageUrl(String customImageUrl) {
		this.customImageUrl = customImageUrl;
	}
	public Date getDateCreated() {
		return dateCreated;
	}
	public void setDateCreated(Date dateCreated) {
		this.dateCreated = dateCreated;
	}
	public void setDealGroup(String dealGroup) {
		this.dealGroup = dealGroup;
	}
	public String getDealGroup() {
		return dealGroup;
	}
	public void setDealMainItem(boolean dealMainItem) {
		this.dealMainItem = dealMainItem;
	}
	public boolean isDealMainItem() {
		return dealMainItem;
	}
	public List<ProductAttribute> getAsiProductAttributes() {
		return asiProductAttributes;
	}
	public void setAsiProductAttributes(List<ProductAttribute> asiProductAttributes) {
		this.asiProductAttributes = asiProductAttributes;
	}
	public void setAsiAdditionalCharge(Double asiAdditionalCharge) {
		this.asiAdditionalCharge = asiAdditionalCharge;
	}
	public Double getAsiAdditionalCharge() {
		return asiAdditionalCharge;
	}
	public void setAsiUnitPrice(Double asiUnitPrice) {
		this.asiUnitPrice = asiUnitPrice;
	}
	public Double getAsiUnitPrice() {
		return asiUnitPrice;
	}
	public String getVariantSku() {
		return variantSku;
	}
	public void setVariantSku(String variantSku) {
		this.variantSku = variantSku;
	}
	public void setOptionFixedCharge(Double optionFixedCharge) {
		this.optionFixedCharge = optionFixedCharge;
	}
	public Double getOptionFixedCharge() {
		this.optionFixedCharge = 0.0;
		if (this.getProductAttributes() != null) {
			for (ProductAttribute prodAttr:this.getProductAttributes()) {
				if ( prodAttr.getOptionPrice() != null && prodAttr.isOneTimePrice() ) {
					optionFixedCharge = optionFixedCharge + prodAttr.getOptionPrice();
				}
			}
		}
		return optionFixedCharge;
	}
	public void setTempCartTotal(Double tempCartTotal) {
		this.tempCartTotal = tempCartTotal;
	}
	public Double getTempCartTotal() {
		return tempCartTotal;
	}
	public Integer getPriceCasePackQty() {
		return priceCasePackQty;
	}
	public void setPriceCasePackQty(Integer priceCasePackQty) {
		this.priceCasePackQty = priceCasePackQty;
	}
	public String getShippingDays() {
		return shippingDays;
	}
	public void setShippingDays(String shippingDays) {
		this.shippingDays = shippingDays;
	}
	public void setAsiOriginalPrice(Double asiOriginalPrice) {
		this.asiOriginalPrice = asiOriginalPrice;
	}
	public Double getAsiOriginalPrice() {
		return asiOriginalPrice;
	}
	public void setItemGroup(String itemGroup) {
		this.itemGroup = itemGroup;
	}
	public String getItemGroup() {
		return itemGroup;
	}
	public void setItemGroupMainItem(boolean itemGroupMainItem) {
		this.itemGroupMainItem = itemGroupMainItem;
	}
	public boolean isItemGroupMainItem() {
		return itemGroupMainItem;
	}
	public void setAttachment(String attachment) {
		this.attachment = attachment;
	}
	public String getAttachment() {
		return attachment;
	}
	public void setTempItemTotal(Double tempItemTotal) {
		this.tempItemTotal = tempItemTotal;
	}
	public Double getTempItemTotal() {
		return tempItemTotal;
	}
	public void setQtyForPriceBreak(Integer qtyForPriceBreak) {
		this.qtyForPriceBreak = qtyForPriceBreak;
	}
	public Integer getQtyForPriceBreak() {
		return qtyForPriceBreak;
	}
	public void setQtyMultiplier(Integer qtyMultiplier) {
		this.qtyMultiplier = qtyMultiplier;
	}
	public Integer getQtyMultiplier() {
		return qtyMultiplier;
	}
	public Double getOriginalPrice() {
		return originalPrice;
	}
	public void setOriginalPrice(Double originalPrice) {
		this.originalPrice = originalPrice;
	}
	public Double getDiscount() {
		return discount;
	}
	public void setDiscount(Double discount) {
		this.discount = discount;
	}
	public Boolean getIncludeRetailDisplay() {
		return includeRetailDisplay;
	}
	public void setIncludeRetailDisplay(Boolean includeRetailDisplay) {
		this.includeRetailDisplay = includeRetailDisplay;
	}
	
	public String getApiIndex() {
		return apiIndex;
	}
	public void setApiIndex(String apiIndex) {
		this.apiIndex = apiIndex;
	}
	
}