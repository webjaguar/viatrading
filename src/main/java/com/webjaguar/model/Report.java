package com.webjaguar.model;

import java.sql.Date;

public class Report
{
	private Integer month;
	private Date date;
	private Double grandTotal;
	private Integer numOrder;
	private Double ppOrder;
	private Integer uniqueUser;
	private String trackcode;
	private Double ccFee;
	private Double tax;
	private Integer newUser;
	private Integer newUserActive;
	private Double shippingCost;
	private Double merchandize;
	private Double avgGrandTotal;
	private Double avgPerCustomer;
	private Integer numCustomerRetained;
	private Integer numCustomerActivated;
	private Integer salesRepId;
	private String promoCode;
	private Double discount;
	
	
	public String getTrackcode()
	{
		return trackcode;
	}
	public void setTrackcode(String trackcode)
	{
		this.trackcode = trackcode;
	}
	public Integer getNumOrder()
	{
		return numOrder;
	}
	public void setNumOrder(Integer numOrder)
	{
		this.numOrder = numOrder;
	}
	public Integer getUniqueUser()
	{
		return uniqueUser;
	}
	public void setUniqueUser(Integer uniqueUser)
	{
		this.uniqueUser = uniqueUser;
	}
	public Report( Integer month, Double grandTotal )
	{
		super();
		this.month = month;
		this.grandTotal = grandTotal;
	}
	
	public Report( Integer month, Double grandTotal, Integer numOrder, Integer uniqueUser, Double ccFee, Double tax, Integer newUser, Double avgGrandTotal, Double avgPerCustomer,
			Integer numCustomerRetained, Integer numCustomerActivated )
	{
		super();
		this.month = month;
		this.grandTotal = grandTotal;
		this.numOrder = numOrder;
		this.uniqueUser = uniqueUser;
		this.ccFee = ccFee;
		this.tax = tax;
		this.newUser = newUser;
		this.avgGrandTotal = avgGrandTotal;
		this.avgPerCustomer = avgPerCustomer;
		this.numCustomerRetained = numCustomerRetained;
		this.numCustomerActivated = numCustomerActivated;
	}
	public Report()
	{
		super();
	}
	public Integer getMonth()
	{
		return month;
	}
	public void setMonth(Integer month)
	{
		this.month = month;
	}
	public Date getDate()
	{
		return date;
	}
	public void setDate(Date date)
	{
		this.date = date;
	}
	public Double getGrandTotal()
	{
		return grandTotal;
	}
	public void setGrandTotal(Double grandTotal)
	{
		this.grandTotal = grandTotal;
	}
	public Double getCcFee()
	{
		return ccFee;
	}
	public void setCcFee(Double ccFee)
	{
		this.ccFee = ccFee;
	}
	public Double getTax()
	{
		return tax;
	}
	public void setTax(Double tax)
	{
		this.tax = tax;
	}
	public Integer getNewUser()
	{
		return newUser;
	}
	public Integer getNewUserActive()
	{
		return newUserActive;
	}
	public Double getShippingCost()
	{
		return shippingCost;
	}
	public void setShippingCost(Double shippingCost)
	{
		this.shippingCost = shippingCost;
	}
	public Double getMerchandize()
	{
		return merchandize;
	}
	public void setMerchandize(Double merchandize)
	{
		this.merchandize = merchandize;
	}
	public void setNewUser(Integer newUser)
	{
		this.newUser = newUser;
	}
	public Double getAvgGrandTotal()
	{
		return avgGrandTotal;
	}
	public void setAvgGrandTotal(Double avgGrandTotal)
	{
		this.avgGrandTotal = avgGrandTotal;
	}
	public Double getAvgPerCustomer()
	{
		return avgPerCustomer;
	}
	public void setAvgPerCustomer(Double avgPerCustomer)
	{
		this.avgPerCustomer = avgPerCustomer;
	}
	public Integer getNumCustomerRetained()
	{
		return numCustomerRetained;
	}
	public void setNumCustomerRetained(Integer numCustomerRetained)
	{
		this.numCustomerRetained = numCustomerRetained;
	}
	public Integer getNumCustomerActivated()
	{
		return numCustomerActivated;
	}
	public void setNumCustomerActivated(Integer numCustomerActivated)
	{
		this.numCustomerActivated = numCustomerActivated;
	}
	public Integer getSalesRepId()
	{
		return salesRepId;
	}
	public void setSalesRepId(Integer salesRepId)
	{
		this.salesRepId = salesRepId;
	}
	public void setPromoCode(String promoCode) {
		this.promoCode = promoCode;
	}
	public String getPromoCode() {
		return promoCode;
	}
	public void setDiscount(Double discount) {
		this.discount = discount;
	}
	public Double getDiscount() {
		return discount;
	}
	public void setPpOrder(Double ppOrder) {
		this.ppOrder = ppOrder;
	}
	public Double getPpOrder() {
		return ppOrder;
	}
}
