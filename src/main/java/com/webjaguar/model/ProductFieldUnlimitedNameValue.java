/* Copyright 2006 Advanced E-Media Solutions
 *
 */

package com.webjaguar.model;

import java.io.Serializable;

public class ProductFieldUnlimitedNameValue extends ProductFieldUnlimited  implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String name;
	public String getDisplayName() {
		return displayName;
	}

	public void setDisplayName(String displayName) {
		this.displayName = displayName;
	}

	private String displayName;
	private Integer fieldId;
	private String sku;
	private String displayValue;
	private String calculationValue;
	
	public ProductFieldUnlimitedNameValue() {
		super();
	}

	public ProductFieldUnlimitedNameValue(String displayValue, String calculationValue) {
		this.displayValue = displayValue;
		this.calculationValue = calculationValue;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	
	public String getDisplayValue() {
		return displayValue;
	}

	public void setDisplayValue(String displayValue) {
		this.displayValue = displayValue;
	}

	public String getCalculationValue() {
		return calculationValue;
	}

	public void setCalculationValue(String calculationValue) {
		this.calculationValue = calculationValue;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	public String getSku() {
		return sku;
	}

	public void setSku(String sku) {
		this.sku = sku;
	}
	
	public Integer getFieldId() {
		return fieldId;
	}

	public void setFieldId(Integer fieldId) {
		this.fieldId = fieldId;
	}

}