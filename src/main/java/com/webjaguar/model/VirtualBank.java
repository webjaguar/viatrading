package com.webjaguar.model;

import java.sql.Timestamp;
import java.util.Date;

public class VirtualBank {
	private Integer orderId;
	private String productSku;
	private Integer orderLineNum;
	private Integer supplierId;
	private Double commission;
	private Double consignment;
	private Timestamp dateOrdered;
	private Integer quantity;
	private Timestamp orderShipDate;
	private String note;
	private Timestamp dateOfPay;
	private Double amountToPay;
	private String paymentFor;
	private String paymentMethod;
	private Double cost;
	private Integer costPercent;
	private String productConsignmentNote;
	private Double subTotal;
	private String transactionId;
	private boolean vbaPaymentStatus;
	private String orderType;
	private int index;
	private String companyName;
	private Integer userId;
	private Date firstOrderDate;
	private Integer affiliateId;

	public String getProductConsignmentNote() {
		return productConsignmentNote;
	}

	public void setProductConsignmentNote(String productConsignmentNote) {
		this.productConsignmentNote = productConsignmentNote;
	}

	public Double getSubTotal() {
		return subTotal;
	}

	public void setSubTotal(Double subTotal) {
		this.subTotal = subTotal;
	}
	
	public Double getAmountToPay() {
		return amountToPay;
	}

	public void setAmountToPay(Double amountToPay) {
		this.amountToPay = amountToPay;
	}

	public String getPaymentFor() {
		return paymentFor;
	}

	public void setPaymentFor(String paymentFor) {
		this.paymentFor = paymentFor;
	}

	public Integer getOrderId() { 
		return orderId;
	}

	public void setOrderId(Integer orderId) {
		this.orderId = orderId;
	}

	public String getProductSku() {
		return productSku;
	}

	public void setProductSku(String productSku) {
		this.productSku = productSku;
	}

	public Double getCommission() {
		return commission;
	}

	public void setCommission(Double commission) {
		this.commission = commission;
	}

	public Double getConsignment() {
		return consignment;
	}

	public void setConsignment(Double consignment) {
		this.consignment = consignment;
	}

	public Timestamp getDateOrdered() {
		return dateOrdered;
	}

	public void setDateOrdered(Timestamp dateOrdered) {
		this.dateOrdered = dateOrdered;
	}

	public Integer getQuantity() {
		return quantity;
	}

	public void setQuantity(Integer quantity) {
		this.quantity = quantity;
	}

	public Timestamp getOrderShipDate() {
		return orderShipDate;
	}

	public void setOrderShipDate(Timestamp orderShipDate) {
		this.orderShipDate = orderShipDate;
	}

	public String getNote() {
		return note;
	}

	public void setNote(String note) {
		this.note = note;
	}

	public Timestamp getDateOfPay() {
		return dateOfPay;
	}

	public void setDateOfPay(Timestamp dateOfPay) {
		this.dateOfPay = dateOfPay;
	}

	public String getPaymentMethod() {
		return paymentMethod;
	}

	public void setPaymentMethod(String paymentMethod) {
		this.paymentMethod = paymentMethod;
	}

	public String getTransactionId() {
		return transactionId;
	}

	public void setTransactionId(String transactionId) {
		this.transactionId = transactionId;
	}

	public boolean isVbaPaymentStatus() {
		return vbaPaymentStatus;
	}

	public void setVbaPaymentStatus(boolean vbaPaymentStatus) {
		this.vbaPaymentStatus = vbaPaymentStatus;
	}

	public String getOrderType() {
		return orderType;
	}

	public void setOrderType(String orderType) {
		this.orderType = orderType;
	}

	public int getIndex() {
		return index;
	}

	public void setIndex(int index) {
		this.index = index;
	}

	public String getCompanyName() {
		return companyName;
	}

	public void setCompanyName(String companyName) {
		this.companyName = companyName;
	}

	public Integer getUserId() {
		return userId;
	}

	public void setUserId(Integer userId) {
		this.userId = userId;
	}

	public Integer getOrderLineNum() {
		return orderLineNum;
	}

	public void setOrderLineNum(Integer orderLineNum) {
		this.orderLineNum = orderLineNum;
	}

	public Integer getSupplierId() {
		return supplierId;
	}

	public void setSupplierId(Integer supplierId) {
		this.supplierId = supplierId;
	}

	public Double getcost() {
		return cost;
	}

	public void setcost(Double cost) {
		this.cost = cost;
	}

	public Integer getCostPercent() {
		return costPercent;
	}

	public void setCostPercent(Integer costPercent) {
		this.costPercent = costPercent;
	}

	public Date getFirstOrderDate() {
		return firstOrderDate;
	}

	public void setFirstOrderDate(Date firstOrderDate) {
		this.firstOrderDate = firstOrderDate;
	}

	public Integer getAffiliateId() {
		return affiliateId;
	}

	public void setAffiliateId(Integer affiliateId) {
		this.affiliateId = affiliateId;
	}
	
}
