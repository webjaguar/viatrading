package com.webjaguar.model;

import java.sql.Date;

public class TicketReport {
	private Date createdDate;
	private Integer count;
	
	public Date getCreatedDate() {
		return createdDate;
	}
	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}
	public Integer getCount() {
		return count;
	}
	public void setCount(Integer count) {
		this.count = count;
	}
}
