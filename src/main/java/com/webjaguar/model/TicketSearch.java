/* Copyright 2007 Advanced E-Media Solutions
 * @author Shahin Naji
 */

package com.webjaguar.model;

public class TicketSearch {

	// sort
	private String sort = "created DESC";
	public String getSort() { return sort; }
	public void setSort(String sort) { this.sort = sort; }	
	
	// page 
	private int page = 1;
	public int getPage() { return page; }
	public void setPage(int page) { this.page = page; }
	
	// page size
	private int pageSize = 10;
	public int getPageSize() { return pageSize; }
	public void setPageSize(int pageSize) { this.pageSize = pageSize; }
	
	// subject
	private String subject;
	public String getSubject() { return subject; }
	public void setSubject(String subject) { this.subject = subject; }
	
	// company
	private String company;
	public String getCompany() { return company; }
	public void setCompany(String company) { this.company = company; }
	
	// number
	private String ticketId;
	public String getTicketId() { return ticketId; }
	public void setTicketId(String ticketId) { this.ticketId = ticketId; }
	
	// status
	private String status = "open";
	public String getStatus() { return status; }
	public void setStatus(String status) { this.status = status; }
	
	// userid
	private Integer userId = -1;
	public Integer getUserId() { return userId; }
	public void setUserId(Integer userId) { this.userId = userId; }
	
	// version createdBy Type
	private String versionCreatedByType;
	public String getVersionCreatedByType() { return versionCreatedByType; }
	public void setVersionCreatedByType(String versionCreatedByType) { this.versionCreatedByType = versionCreatedByType; }
	
}
