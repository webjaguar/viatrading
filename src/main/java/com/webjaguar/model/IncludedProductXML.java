package com.webjaguar.model;

import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlSeeAlso;

@XmlRootElement(name = "products")
@XmlAccessorType(XmlAccessType.FIELD)
@XmlSeeAlso({Product.class, IncludedProduct.class})
public class IncludedProductXML {
	@XmlElement(name = "token", required = false)
	private String token;
	private List<IncludedProduct> product;
	
	public IncludedProductXML() {
		this.product = new ArrayList();
	}
	public void setToken(String token) {
		this.token = token;
	}
	public String getToken() {
		return token;
	}
	public void setProduct(List<IncludedProduct> product) {
		this.product = product;
	}
	public List<IncludedProduct> getProduct() {
		return product;
	}
	public void addToList(List<IncludedProduct> list) {
		this.product.addAll(list);
	}
}