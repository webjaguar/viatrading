/* Copyright 2005, 2008 Advanced E-Media Solutions
 *
 */
package com.webjaguar.model;

import java.util.Date;

public class EmailCampaignBalance
{
	private Integer id;
	private Date created;
	private String user;
	private String note;
	private Double amount;
	private Integer point;
	private String paymentMethod;
	private String transId;
	private String paymentStatus;
	public Integer getId()
	{
		return id;
	}
	public void setId(Integer id)
	{
		this.id = id;
	}
	public Date getCreated()
	{
		return created;
	}
	public void setCreated(Date created)
	{
		this.created = created;
	}
	public String getUser()
	{
		return user;
	}
	public void setUser(String user)
	{
		this.user = user;
	}
	public String getNote()
	{
		return note;
	}
	public void setNote(String note)
	{
		this.note = note;
	}
	public Double getAmount()
	{
		return amount;
	}
	public void setAmount(Double amount)
	{
		this.amount = amount;
	}
	public Integer getPoint()
	{
		return point;
	}
	public void setPoint(Integer point)
	{
		this.point = point;
	}
	public String getPaymentMethod()
	{
		return paymentMethod;
	}
	public void setPaymentMethod(String paymentMethod)
	{
		this.paymentMethod = paymentMethod;
	}
	public String getTransId()
	{
		return transId;
	}
	public void setTransId(String transId)
	{
		this.transId = transId;
	}
	public String getPaymentStatus()
	{
		return paymentStatus;
	}
	public void setPaymentStatus(String paymentStatus)
	{
		this.paymentStatus = paymentStatus;
	}
	

}
