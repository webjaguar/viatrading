package com.webjaguar.model;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.Date;

public class ProductReview implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private int id;
	private boolean active;
	private int userId;
	private String userName;
	private int rate;
	private Date created;
	private Date lastModified;
	private String title;
	private String text;
	private int numReview;
	private double average;
	private String reviewType;
	private String ipAddress;
	
	private int productId;
	private String productSku;
	private Integer categoryId;
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public boolean isActive() {
		return active;
	}
	public void setActive(boolean active) {
		this.active = active;
	}
	public int getUserId() {
		return userId;
	}
	public void setUserId(int userId) {
		this.userId = userId;
	}
	public String getUserName() {
		return userName;
	}
	public void setUserName(String userName) {
		this.userName = userName;
	}
	public int getRate() {
		return rate;
	}
	public void setRate(int rate) {
		this.rate = rate;
	}
	public Date getCreated() {
		return created;
	}
	public void setCreated(Date created) {
		this.created = created;
	}
	public Date getLastModified() {
		return lastModified;
	}
	public void setLastModified(Date lastModified) {
		this.lastModified = lastModified;
	}
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public String getText() {
		return text;
	}
	public void setText(String text) {
		this.text = text;
	}
	public int getNumReview() {
		return numReview;
	}
	public void setNumReview(int numReview) {
		this.numReview = numReview;
	}
	public double getAverage() {
		return average;
	}
	public void setAverage(double average) {
		this.average = average;
	}
	public String getReviewType() {
		return reviewType;
	}
	public void setReviewType(String reviewType) {
		this.reviewType = reviewType;
	}
	public String getIpAddress() {
		return ipAddress;
	}
	public void setIpAddress(String ipAddress) {
		this.ipAddress = ipAddress;
	}
	public int getProductId() {
		return productId;
	}
	public void setProductId(int productId) {
		this.productId = productId;
	}
	public String getProductSku() {
		return productSku;
	}
	public void setProductSku(String productSku) {
		this.productSku = productSku;
	}
	public Integer getCategoryId() {
		return categoryId;
	}
	public void setCategoryId(Integer categoryId) {
		this.categoryId = categoryId;
	}
	public String getShortText() {
		if ( this.text != null && this.text.length() > 50 ) {
			return this.text.substring( 0, 50 ).concat( "..." );
		} else
			return this.text;
	}
	public int getStarIndex() {
		if (this.average < 0.5) {
			return 0;	
		} else if (this.average >= 0.5 && this.average <= 1) {
			return 1;
		} else if (this.average >= 1 && this.average < 1.5) {
			return 2;
		} else if (this.average >= 1.5 && this.average < 2) {
			return 3;
		} else if (this.average >= 2 && this.average < 2.5) {
			return 4;
		} else if (this.average >= 2.5 && this.average < 3) {
			return 5;
		} else if (this.average >= 3 && this.average < 3.5) {
			return 6;
		} else if (this.average >= 3.5 && this.average < 4) {
			return 7;
		} else if (this.average >= 4 && this.average < 4.5) {
			return 8;
		} else {
			return 9;
		}
	}
	public int getElementId() {
		if (this.getStarIndex() <= 2) {
			return 1;
		} else if (this.getStarIndex() <= 4) {
			return 2;
		} else if (this.getStarIndex() <= 6) {
			return 3;
		} else if (this.getStarIndex() <= 8) {
			return 4;
		} else {
			return 5;
		}
	}
	public String getStyle() {
		String[] style = { "nostar", "onestar", "twostar", "threestar", "fourstar", "fivestar", "sixstar", "sevenstar", "eightstar", "ninestar", "tenstar" };
		return style[this.getStarIndex()];
	}
}