package com.webjaguar.model;

import java.util.ArrayList;
import java.util.List;

public class FaqSearch {

	// group ID
	private Integer groupId;
	public Integer getGroupId() { return groupId; }
	public void setGroupId(Integer groupId) { this.groupId = groupId; }
	
	// keyword
	private String keyword;
	public String getKeyword() { return keyword; }
	public void setKeyword(String keyword) { 
		keywordList.clear();	// clear old list
		this.keyword = keyword;
	}
	
	private List<String> keywordList = new ArrayList<String>();
	public void setKeywordList(List<String> keywordList) { this.keywordList = keywordList; }	
	public List<String> getKeywordList() {	return this.keywordList; }
	public void addKeyword(String keyword) { keywordList.add(keyword); }
	
	// question 
	private String question;
	public String getQuestion() { return question; }
	public void setQuestion(String question) { this.question = question; }

	// search conjuction
	private String conjunction;
	public String getConjunction() { return conjunction; }
	public void setConjunction(String conjunction) { this.conjunction = conjunction; }
	
	// protected Access
	private String protectedAccess = "0";
	public String getProtectedAccess() { return protectedAccess; }
	public void setProtectedAccess(String protectedAccess) { this.protectedAccess = protectedAccess; }
	
	// page 
	private int page = 1;
	public int getPage() { return page; }
	public void setPage(int page) { this.page = page; }
	
	// page size
	private int pageSize = 20;
	public int getPageSize() { return pageSize; }
	public void setPageSize(int pageSize) { this.pageSize = pageSize; }
}