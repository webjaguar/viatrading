package com.webjaguar.model;

public class MetricReport {

	private Integer newRegistrations;
	private Integer newActivations;
	private Integer totalSpend;
	private Integer totalRegistration;
	private Integer totalActivations;
	private Integer touchScreenRegistrations;
	private Integer touchScreenActivations;
	private Integer frontendRegistrations;
	private Integer frontendActivations;
	private Integer landingPageLeads;
	private Integer landingPageRegistrations;
	private Integer landingPageActivations;
	private Integer allOtherRegistrations;
	private Integer allOtherActivations;
	private Double merchandize;
	private Double shipping;
	private Double grandTotal;
	private Double ccFee;
	private Integer customersPurchased;
	private Integer totalOrders;
	private Double avgOrders;
	private Double avgCustomers;
	
	public void setNewRegistrations(Integer newRegistrations) {
		this.newRegistrations = newRegistrations;
	}
	public Integer getNewRegistrations() {
		return newRegistrations;
	}
	public void setNewActivations(Integer newActivations) {
		this.newActivations = newActivations;
	}
	public Integer getNewActivations() {
		return newActivations;
	}
	public double getNewCloseRate() {
		double newCloseRate=0;
		if(getNewRegistrations().equals(0)) {
			newCloseRate = 0;
		}else {
			newCloseRate = (this.getNewActivations()/this.getNewRegistrations())*100;
		}
		return newCloseRate;
	}
	public void setTotalRegistration(Integer totalRegistration) {
		this.totalRegistration = totalRegistration;
	}
	public Integer getTotalRegistration() {
		return totalRegistration;
	}
	public void setTotalActivations(Integer totalActivations) {
		this.totalActivations = totalActivations;
	}
	public Integer getTotalActivations() {
		return totalActivations;
	}
	public double getTotalCloseRate() {
		double totalCloseRate=0;
		if(getTotalRegistration().equals(0)) {
			totalCloseRate = 0;
		} else {
			totalCloseRate = (this.getTotalActivations()/this.getTotalRegistration())*100;
		}
		return totalCloseRate;
	}
	public Integer getTouchScreenRegistrations() {
		return touchScreenRegistrations;
	}
	public void setTouchScreenRegistrations(Integer touchScreenRegistrations) {
		this.touchScreenRegistrations = touchScreenRegistrations;
	}
	public Integer getTouchScreenActivations() {
		return touchScreenActivations;
	}
	public void setTouchScreenActivations(Integer touchScreenActivations) {
		this.touchScreenActivations = touchScreenActivations;
	}
	public double getTouchScreenCloseRate() {
		double touchScreenCloseRate = 0;
		if(getTouchScreenRegistrations().equals(0)) {
			touchScreenCloseRate =0;
		} else {
			touchScreenCloseRate = (this.getTouchScreenActivations()/this.getTouchScreenRegistrations())*100;
		}
		return touchScreenCloseRate;
	}
	public Integer getFrontendRegistrations() {
		return frontendRegistrations;
	}
	public void setFrontendRegistrations(Integer frontendRegistrations) {
		this.frontendRegistrations = frontendRegistrations;
	}
	public Integer getFrontendActivations() {
		return frontendActivations;
	}
	public void setFrontendActivations(Integer frontendActivations) {
		this.frontendActivations = frontendActivations;
	}
	public double getFrontendCloseRate() {
		double frontendCloseRate=0;
		if (getFrontendRegistrations().equals(0)) {
			frontendCloseRate = 0;
		} else {
			frontendCloseRate = (this.getFrontendActivations()/this.getFrontendRegistrations())*100;
		}
		return frontendCloseRate;
	}
	public Integer getLandingPageRegistrations() {
		return landingPageRegistrations;
	}
	public void setLandingPageRegistrations(Integer landingPageRegistrations) {
		this.landingPageRegistrations = landingPageRegistrations;
	}
	public Integer getLandingPageActivations() {
		return landingPageActivations;
	}
	public void setLandingPageActivations(Integer landingPageActivations) {
		this.landingPageActivations = landingPageActivations;
	}
	public double getLandingPageCloseRate() {
		double landingPageCloseRate=0;
		if (getLandingPageRegistrations().equals(0)) {
			landingPageCloseRate = 0;
		} else {
			landingPageCloseRate = (this.getLandingPageActivations()/this.getLandingPageRegistrations())*100;
		}
		return landingPageCloseRate;
	}
	public Integer getAllOtherRegistrations() {
		return allOtherRegistrations;
	}
	public void setAllOtherRegistrations(Integer allOtherRegistrations) {
		this.allOtherRegistrations = allOtherRegistrations;
	}
	public Integer getAllOtherActivations() {
		return allOtherActivations;
	}
	public void setAllOtherActivations(Integer allOtherActivations) {
		this.allOtherActivations = allOtherActivations;
	}
	public double getAllOtherCloseRate() {
		double allOtherCloseRate=0;
		if(getAllOtherRegistrations().equals(0)) {
			allOtherCloseRate = 0;
		} else {
			allOtherCloseRate = (this.getAllOtherActivations()/this.getAllOtherRegistrations()) * 100;
		}
		return allOtherCloseRate;
	}
	public Double getMerchandize() {
		return merchandize;
	}
	public void setMerchandize(Double merchandize) {
		this.merchandize = merchandize;
	}
	public Double getShipping() {
		return shipping;
	}
	public void setShipping(Double shipping) {
		this.shipping = shipping;
	}
	public Double getGrandTotal() {
		return grandTotal;
	}
	public void setGrandTotal(Double grandTotal) {
		this.grandTotal = grandTotal;
	}
	public Double getCcFee() {
		return ccFee;
	}
	public void setCcFee(Double ccFee) {
		this.ccFee = ccFee;
	}
	public Integer getTotalOrders() {
		return totalOrders; 
	}
	public void setTotalOrders(Integer totalOrders) {
		this.totalOrders = totalOrders;
	}
	public Double getAvgOrders() {
		return avgOrders;
	}
	public void setAvgOrders(Double avgOrders) {
		this.avgOrders = avgOrders;
	}
	public Double getAvgCustomers() {
		return avgCustomers;
	}
	public void setAvgCustomers(Double avgCustomers) {
		this.avgCustomers = avgCustomers;
	}
	public void setCustomersPurchased(Integer customersPurchased) {
		this.customersPurchased = customersPurchased;
	}
	public Integer getCustomersPurchased() {
		return customersPurchased;
	}
	public void setLandingPageLeads(Integer landingPageLeads) {
		this.landingPageLeads = landingPageLeads;
	}
	public Integer getLandingPageLeads() {
		return landingPageLeads;
	}
	public void setTotalSpend(Integer totalSpend) {
		this.totalSpend = totalSpend;
	}
	public Integer getTotalSpend() {
		return totalSpend;
	}

}