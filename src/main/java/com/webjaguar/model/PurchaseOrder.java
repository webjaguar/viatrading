/* Copyright 2005, 2008 Advanced E-Media Solutions
 *
 */

package com.webjaguar.model;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class PurchaseOrder {
	
	private Integer poId;
	private String poNumber;
	private String status;
	private Date created;
	private Date dueDate;
	private Date scheduledDate;
	private Date dockedDate;
	private String dockedIn;
	private String dockedOut;
	private Integer supplierId;
	private String company;
	private Supplier supplier;
	private String shippingAddress;
	private String billingAddress;
	private String orderBy;
	private String shipVia;
	private Integer shipViaContactId;
	private Double shippingQuote;
	private String supplierInvoiceNumber;
	private String note;
	private Double subTotal;
	private Integer orderId;
	private boolean dropShip;
	private String subject;	
	private String from;
	private String to;
	private String cc;
	private String bcc;
	private String message;	
	private String specialInstruction;
	private boolean html;
	private String flag1;
	private boolean comingToVia;
	private boolean pictureNeeded;
	private String poSold;
	private String trailer;
	private String checkBeforeStaging;	
	private String checkNotes;
	private String pricingInstructions;
	private String locAfterStaging;
	private String docks;
	private String resources;
	private Integer totalNumOfPalletsReceived;
	private Date arrivedDate;
	private Date stagedDate;
	
	public String getDocks() {
		return docks;
	}
	public void setDocks(String docks) {
		this.docks = docks;
	}
	public String getResources() {
		return resources;
	}
	public void setResources(String resources) {
		this.resources = resources;
	}
	private List<PurchaseOrderStatus> statusHistory;
	private List<PurchaseOrderLineItem> poLineItems;
	// deleted line items
	private List<PurchaseOrderLineItem> poDeletedLineItems;
	// inserted line items
	private List<PurchaseOrderLineItem> poInsertedLineItems;
	private Integer accessUserId;
	private String pt;
	

	public void addPOInsertedLineItem(PurchaseOrderLineItem poLineItem)
	{
		PurchaseOrderLineItem lineItem = new PurchaseOrderLineItem( poInsertedLineItems.size() + 1, poLineItem );
		poInsertedLineItems.add( lineItem );
	}
	public void addPOLineItem(PurchaseOrderLineItem poLineItem)
	{
		PurchaseOrderLineItem lineItem = new PurchaseOrderLineItem( poLineItem.getLineNumber() , poLineItem );
		poLineItems.add( lineItem );
	}
	public PurchaseOrder()
	{
		this.poInsertedLineItems = new ArrayList();
		this.poLineItems = new ArrayList();
		this.poDeletedLineItems = new ArrayList();
	}
	public boolean containsLineItem(PurchaseOrderLineItem poLineItem) {
		for (PurchaseOrderLineItem lineItem:this.poLineItems) {
			if ( lineItem.toString().equals( poLineItem.toString() ))
				return true;
		}
		for (PurchaseOrderLineItem lineItem:this.poInsertedLineItems) {
			if ( lineItem.toString().equals( poLineItem.toString() ))
				return true;
		}
		return false;
	}
	public Integer getPoId()
	{
		return poId;
	}
	public void setPoId(Integer poId)
	{
		this.poId = poId;
	}
	public String getPoNumber()
	{
		return poNumber;
	}
	public void setPoNumber(String poNumber)
	{
		this.poNumber = poNumber;
	}
	public String getStatus()
	{
		return status;
	}
	public void setStatus(String status)
	{
		this.status = status;
	}
	public String getStatusName()
	{
		if (this.status.equals( "send" ))
			return "po_send";
		else if ( this.status.equals( "send01" ))
			return "po_send01";
		else if ( this.status.equals( "send05" ))
			return "po_send05";
		else if ( this.status.equals( "send10" ))
			return "po_send10";
		/*else if ( this.status.equals( "prec" ))
			return "Partially Received";*/
		else if ( this.status.equals( "rece" ))
			return "po_rece";
		else if ( this.status.equals( "x" ))
			return "po_x";
		else if ( this.status.equals( "send15" ))
			return "po_send15";
		else 
			return "po_pend"; 
	}
	public Date getCreated()
	{
		return created;
	}
	public void setCreated(Date created)
	{
		this.created = created;
	}
	public Date getDueDate() {
		return dueDate;
	}
	public void setDueDate(Date dueDate) {
		this.dueDate = dueDate;
	}
	public Date getScheduledDate() {
		return scheduledDate;
	}
	public void setScheduledDate(Date scheduledDate) {
		this.scheduledDate = scheduledDate;
	}
	public Integer getSupplierId()
	{
		return supplierId;
	}
	public void setSupplierId(Integer supplierId)
	{
		this.supplierId = supplierId;
	}
	public String getCompany()
	{
		return company;
	}
	public void setCompany(String company)
	{
		this.company = company;
	}
	public Supplier getSupplier()
	{
		return supplier;
	}
	public void setSupplier(Supplier supplier)
	{
		this.supplier = supplier;
	}
	public String getShippingAddress()
	{
		return shippingAddress;
	}
	public void setShippingAddress(String shippingAddress)
	{
		this.shippingAddress = shippingAddress;
	}
	public String getNote()
	{
		return note;
	}
	public void setNote(String note)
	{
		this.note = note;
	}
	public String getNoteTrimmed()
	{
		if(this.note == null){
			return "";
		}if (this.note.length() > 30)
			return this.note.substring( 0, 30 ).concat( "..." );	
		else
			return this.note;
	}
	public List<PurchaseOrderLineItem> getPoLineItems()
	{
		return poLineItems;
	}
	public void setPoLineItems(List<PurchaseOrderLineItem> poLineItems)
	{
		this.poLineItems = poLineItems;
	}
	public int getSize()
	{
		return this.poLineItems.size();
	}
	public String getBillingAddress()
	{
		return billingAddress;
	}
	public void setBillingAddress(String billingAddress)
	{
		this.billingAddress = billingAddress;
	}
	public void setSubTotal(Double subTotal)
	{
		this.subTotal = subTotal;
	}
	public double getSubTotal() {
		return subTotal;
	}
	public double getRunningSubTotal() {
		double subTotal = 0;
		for (PurchaseOrderLineItem poLineItem:this.poLineItems) {
			if (poLineItem.getTotalCost() != null) {
				subTotal += poLineItem.getTotalCost().doubleValue();
			}
		}
		for (PurchaseOrderLineItem poLineItem:this.poInsertedLineItems) {
			if (poLineItem.getTotalCost() != null) {
				subTotal += poLineItem.getTotalCost().doubleValue();
			}
		}
		return subTotal;
	}
	public String getOrderBy()
	{
		return orderBy;
	}
	public void setOrderBy(String orderBy)
	{
		this.orderBy = orderBy;
	}
	public String getShipVia()
	{
		return shipVia;
	}
	public void setShipVia(String shipVia)
	{
		this.shipVia = shipVia;
	}
	public Integer getShipViaContactId() {
		return shipViaContactId;
	}
	public void setShipViaContactId(Integer shipViaContactId) {
		this.shipViaContactId = shipViaContactId;
	}
	public List<PurchaseOrderStatus> getStatusHistory()
	{
		return statusHistory;
	}
	public void setStatusHistory(List<PurchaseOrderStatus> statusHistory)
	{
		this.statusHistory = statusHistory;
	}
	public Integer getOrderId()
	{
		return orderId;
	}
	public void setOrderId(Integer orderId)
	{
		this.orderId = orderId;
	}
	public List<PurchaseOrderLineItem> getPoDeletedLineItems()
	{
		return poDeletedLineItems;
	}
	public void setPoDeletedLineItems(List<PurchaseOrderLineItem> poDeletedLineItems)
	{
		this.poDeletedLineItems = poDeletedLineItems;
	}
	public List<PurchaseOrderLineItem> getPoInsertedLineItems()
	{
		return poInsertedLineItems;
	}
	public void setPoInsertedLineItems(List<PurchaseOrderLineItem> poInsertedLineItems)
	{
		this.poInsertedLineItems = poInsertedLineItems;
	}
	public boolean isDropShip()
	{
		return dropShip;
	}
	public void setDropShip(boolean dropShip)
	{
		this.dropShip = dropShip;
	}
	public String getSubject()
	{
		return subject;
	}
	public void setSubject(String subject)
	{
		this.subject = subject;
	}
	public String getFrom() {
		return from;
	}
	public void setFrom(String from) {
		this.from = from;
	}
	public String getTo() {
		return to;
	}
	public void setTo(String to) {
		this.to = to;
	}
	public String getCc() {
		return cc;
	}
	public void setCc(String cc) {
		this.cc = cc;
	}
	public String getBcc() {
		return bcc;
	}
	public void setBcc(String bcc) {
		this.bcc = bcc;
	}
	public String getMessage()
	{
		return message;
	}
	public void setMessage(String message)
	{
		this.message = message;
	}
	public boolean isHtml()
	{
		return html;
	}
	public void setHtml(boolean html)
	{
		this.html = html;
	}
	
	public boolean isComingToVia() {
		return comingToVia;
	}
	public void setComingToVia(boolean comingToVia) {
		this.comingToVia = comingToVia;
	}
	public boolean isPictureNeeded() {
		return pictureNeeded;
	}
	public void setPictureNeeded(boolean pictureNeeded) {
		this.pictureNeeded = pictureNeeded;
	}
	public String getPoSold() {
		return poSold;
	}
	public void setPoSold(String poSold) {
		this.poSold = poSold;
	}
	public String getCheckBeforeStaging() {
		return checkBeforeStaging;
	}
	public void setCheckBeforeStaging(String checkBeforeStaging) {
		this.checkBeforeStaging = checkBeforeStaging;
	}
	public String getCheckNotes() {
		return checkNotes;
	}
	public void setCheckNotes(String checkNotes) {
		this.checkNotes = checkNotes;
	}
	public String getPricingInstructions() {
		return pricingInstructions;
	}
	public void setPricingInstructions(String pricingInstructions) {
		this.pricingInstructions = pricingInstructions;
	}
	public String getLocAfterStaging() {
		return locAfterStaging;
	}
	public void setLocAfterStaging(String locAfterStaging) {
		this.locAfterStaging = locAfterStaging;
	}
	public Double getShippingQuote() {
		return shippingQuote;
	}
	public void setShippingQuote(Double shippingQuote) {
		this.shippingQuote = shippingQuote;
	}
	public String getSupplierInvoiceNumber() {
		return supplierInvoiceNumber;
	}
	public void setSupplierInvoiceNumber(String supplierInvoiceNumber) {
		this.supplierInvoiceNumber = supplierInvoiceNumber;
	}
	public String getFlag1() {
		return flag1;
	}
	public void setFlag1(String flag1) {
		this.flag1 = flag1;
	}
	public void setAccessUserId(Integer accessUserId) {
		this.accessUserId = accessUserId;
	}
	public Integer getAccessUserId() {
		return accessUserId;
	}
	public String getSpecialInstruction() {
		return specialInstruction;
	}
	public void setSpecialInstruction(String specialInstruction) {
		this.specialInstruction = specialInstruction;
	}
	public String getPt() {
		return pt;
	}
	public void setPt(String pt) {
		this.pt = pt;
	}
	public String getTrailer() {
		return trailer;
	}
	public void setTrailer(String trailer) {
		this.trailer = trailer;
	}
	public Date getDockedDate() {
		return dockedDate;
	}
	public void setDockedDate(Date dockedDate) {
		this.dockedDate = dockedDate;
	}
	public String getDockedIn() {
		return dockedIn;
	}
	public void setDockedIn(String dockedIn) {
		this.dockedIn = dockedIn;
	}
	public String getDockedOut() {
		return dockedOut;
	}
	public void setDockedOut(String dockedOut) {
		this.dockedOut = dockedOut;
	}
	public Integer getTotalNumOfPalletsReceived() {
		return totalNumOfPalletsReceived;
	}
	public void setTotalNumOfPalletsReceived(Integer totalNumOfPalletsReceived) {
		this.totalNumOfPalletsReceived = totalNumOfPalletsReceived;
	}
	public Date getArrivedDate() {
		return arrivedDate;
	}
	public void setArrivedDate(Date arrivedDate) {
		this.arrivedDate = arrivedDate;
	}
	public Date getStagedDate() {
		return stagedDate;
	}
	public void setStagedDate(Date stagedDate) {
		this.stagedDate = stagedDate;
	}
	
}
