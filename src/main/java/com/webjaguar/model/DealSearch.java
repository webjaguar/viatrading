package com.webjaguar.model;

public class DealSearch {

	// sort
	private String sort= "deal_id DESC";
	public String getSort() { return sort; }
	public void setSort(String sort) { this.sort = sort; }
	
	// view type
	private String viewType;
	public String getViewType() { return viewType; }
	public void setViewType(String viewType) { this.viewType = viewType; }
	
	// active 
	private boolean activeOnly = false;
	public boolean isActiveOnly() { return activeOnly; }
	public void setActiveOnly(boolean activeOnly) { this.activeOnly = activeOnly; }
	
	//parent sku
	private Boolean parentSku;
	public Boolean getParentSku() { return parentSku; }
	public void setParentSku(Boolean parentSku) { this.parentSku = parentSku; }
	
	// page 
	private int page = 1;
	public int getPage() { return page; }
	public void setPage(int page) { this.page = page; }
	
	// page size
	private int pageSize = 10;
	public int getPageSize() { return pageSize; }
	public void setPageSize(int pageSize) { this.pageSize = pageSize; }

	

}
