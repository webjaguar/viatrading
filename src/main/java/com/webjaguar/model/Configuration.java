/* Copyright 2006 Advanced E-Media Solutions
 * @author Eduardo Asprec
 * @since 10.30.2006
 */

package com.webjaguar.model;

public class Configuration {
	
	private String key;
	private String value;
	
	public String getKey() {
		return key;
	}
	
	public void setKey(String key) {
		this.key = key;
	}

	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}
	
}
