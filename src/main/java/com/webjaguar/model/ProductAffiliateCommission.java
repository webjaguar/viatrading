package com.webjaguar.model;

public class ProductAffiliateCommission
{
	private Integer productId;
	private Double commissionTable1;
	private Double commissionTable2;
	private Double commissionTable3;
	private Double commissionTable4;
	private Double commissionTable5;
	private Double commissionTable6;
	private Double commissionTable7;
	private Double commissionTable8;
	private Double commissionTable9;
	private Double commissionTable10;
	public Double getCommissionTable1()
	{
		return commissionTable1;
	}
	public void setCommissionTable1(Double commissionTable1)
	{
		this.commissionTable1 = commissionTable1;
	}
	public Double getCommissionTable10()
	{
		return commissionTable10;
	}
	public void setCommissionTable10(Double commissionTable10)
	{
		this.commissionTable10 = commissionTable10;
	}
	public Double getCommissionTable2()
	{
		return commissionTable2;
	}
	public void setCommissionTable2(Double commissionTable2)
	{
		this.commissionTable2 = commissionTable2;
	}
	public Double getCommissionTable3()
	{
		return commissionTable3;
	}
	public void setCommissionTable3(Double commissionTable3)
	{
		this.commissionTable3 = commissionTable3;
	}
	public Double getCommissionTable4()
	{
		return commissionTable4;
	}
	public void setCommissionTable4(Double commissionTable4)
	{
		this.commissionTable4 = commissionTable4;
	}
	public Double getCommissionTable5()
	{
		return commissionTable5;
	}
	public void setCommissionTable5(Double commissionTable5)
	{
		this.commissionTable5 = commissionTable5;
	}
	public Double getCommissionTable6()
	{
		return commissionTable6;
	}
	public void setCommissionTable6(Double commissionTable6)
	{
		this.commissionTable6 = commissionTable6;
	}
	public Double getCommissionTable7()
	{
		return commissionTable7;
	}
	public void setCommissionTable7(Double commissionTable7)
	{
		this.commissionTable7 = commissionTable7;
	}
	public Double getCommissionTable8()
	{
		return commissionTable8;
	}
	public void setCommissionTable8(Double commissionTable8)
	{
		this.commissionTable8 = commissionTable8;
	}
	public Double getCommissionTable9()
	{
		return commissionTable9;
	}
	public void setCommissionTable9(Double commissionTable9)
	{
		this.commissionTable9 = commissionTable9;
	}
	public Integer getProductId()
	{
		return productId;
	}
	public void setProductId(Integer productId)
	{
		this.productId = productId;
	}
	public void getCommissionTable(int commissionTableNumber)
	{
		switch (commissionTableNumber){
			case 1:
				getCommissionTable1(); break;
			case 2:
				getCommissionTable2(); break;
			case 3:
				getCommissionTable3(); break;
			case 4:
				getCommissionTable4(); break;
			case 5:
				getCommissionTable5(); break;
			case 6:
				getCommissionTable6(); break;
			case 7:
				getCommissionTable7(); break;
			case 8:
				getCommissionTable8(); break;
			case 9:
				getCommissionTable9(); break;
			case 10:
				getCommissionTable10(); break;
		}
	}

}
