/* Copyright 2006 Advanced E-Media Solutions
 * @author Li Qin
 * @since 10.10.2006
 */

package com.webjaguar.model;

public class Country {
	
	private String code;
	private String name;
	private boolean enabled;
	private Integer rank;
	private Double taxRate;
	private Integer customerCount;
	private Integer orderCount;
	private String region;
	
	public Integer getRank() {
		return rank;
	}
	public void setRank(Integer rank) {
		this.rank = rank;
	}
	public Double getTaxRate() {
		return taxRate;
	}
	public void setTaxRate(Double taxRate) {
		this.taxRate = taxRate;
	}
	public String getCode() {
		return code;
	}
	public void setCode(String code) {
		this.code = code;
	}
	public boolean getEnabled() {
		return enabled;
	}
	public void setEnabled(boolean enabled) {
		this.enabled = enabled;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public Integer getCustomerCount()
	{
		return customerCount;
	}
	public void setCustomerCount(Integer customerCount)
	{
		this.customerCount = customerCount;
	}
	public Integer getOrderCount()
	{
		return orderCount;
	}
	public void setOrderCount(Integer orderCount)
	{
		this.orderCount = orderCount;
	}
	public String getRegion() {
		return region;
	}
	public void setRegion(String region) {
		this.region = region;
	}
}