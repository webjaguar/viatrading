package com.webjaguar.model;

import java.util.Date;

/**
 * The SearchEngineProspectEntity is the domain class
 * for prospect information.
 * 
 * @author mjbaron
 *
 */
public class SearchEngineProspectEntity {

	/**
	 * The Entity id field
	 */
	private int id;
	
	/**
	 * The prospect identifier
	 */
	private String prospectId;
	
	/**
	 * The normalized referrer domain.
	 */
	private String referrer;
	
	/**
	 * The search string, if recorded
	 */
	private String queryString;
	
	/**
	 * The Date and Time the prospect information was recorded
	 */
	private Date timestamp;
	
	/**
	 * @return the id
	 */
	public int getId() {
		return id;
	}

	/**
	 * @param id the id to set
	 */
	public void setId(int id) {
		this.id = id;
	}

	/**
	 * @return the prospectId
	 */
	public String getProspectId() {
		return prospectId;
	}

	/**
	 * @param prospectId the prospectId to set
	 */
	public void setProspectId(String prospectId) {
		this.prospectId = prospectId;
	}

	/**
	 * @return the referrer
	 */
	public String getReferrer() {
		return referrer;
	}

	/**
	 * @param referrer the referrer to set
	 */
	public void setReferrer(String referrer) {
		this.referrer = referrer;
	}

	/**
	 * @return the queryString
	 */
	public String getQueryString() {
		return queryString;
	}

	/**
	 * @param queryString the queryString to set
	 */
	public void setQueryString(String queryString) {
		this.queryString = queryString;
	}

	/**
	 * @return the timestamp
	 */
	public Date getTimestamp() {
		return timestamp;
	}

	/**
	 * @param timestamp the timestamp to set
	 */
	public void setTimestamp(Date timestamp) {
		this.timestamp = timestamp;
	}


	@Override
	public String toString() {
		return "SearchEngineProspectEntity [id=" + id + ", prospectId="
				+ prospectId + ", referrer=" + referrer + ", queryString="
				+ queryString + ", timestamp=" + timestamp + "]";
	}
}