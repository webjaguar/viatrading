/* Copyright 2008 Advanced E-Media Solutions
 * @author Eduardo Asprec
 * @since 09.10.2008
 */

package com.webjaguar.model;

import java.sql.Timestamp;
import java.util.Date;
import java.util.TreeSet;

public class Rma {
	
	private Integer rmaNum;
	private Integer orderId;
	private String serialNum;
	private LineItem lineItem;
	private String status;	
	private String action;	
	private String problem;
	private String comments;
	private Timestamp reportDate;
	private Timestamp lastModified;
	private Date completeDate;
	private TreeSet<String> serialNums;
	
	public Rma() {}

	public Rma(int rmaNum) {
		this.rmaNum = rmaNum;
	}
	
	public Rma(int orderId, String serialNum) {
		this.orderId = orderId;
		this.serialNum = serialNum;
	}

	public Integer getRmaNum()
	{
		return rmaNum;
	}

	public void setRmaNum(Integer rmaNum)
	{
		this.rmaNum = rmaNum;
	}

	public Integer getOrderId()
	{
		return orderId;
	}

	public void setOrderId(Integer orderId)
	{
		this.orderId = orderId;
	}

	public String getSerialNum()
	{
		return serialNum;
	}

	public void setSerialNum(String serialNum)
	{
		this.serialNum = serialNum;
	}

	public LineItem getLineItem()
	{
		return lineItem;
	}

	public void setLineItem(LineItem lineItem)
	{
		this.lineItem = lineItem;
	}

	public String getStatus()
	{
		return status;
	}

	public void setStatus(String status)
	{
		this.status = status;
	}

	public String getAction()
	{
		return action;
	}

	public void setAction(String action)
	{
		this.action = action;
	}

	public String getProblem()
	{
		return problem;
	}

	public void setProblem(String problem)
	{
		this.problem = problem;
	}

	public String getComments()
	{
		return comments;
	}

	public void setComments(String comments)
	{
		this.comments = comments;
	}

	public Timestamp getReportDate()
	{
		return reportDate;
	}

	public void setReportDate(Timestamp reportDate)
	{
		this.reportDate = reportDate;
	}

	public Timestamp getLastModified()
	{
		return lastModified;
	}

	public void setLastModified(Timestamp lastModified)
	{
		this.lastModified = lastModified;
	}

	public Date getCompleteDate()
	{
		return completeDate;
	}

	public void setCompleteDate(Date completeDate)
	{
		this.completeDate = completeDate;
	}

	public TreeSet<String> getSerialNums()
	{
		return serialNums;
	}

	public void setSerialNums(TreeSet<String> serialNums)
	{
		this.serialNums = serialNums;
	}
	

}
