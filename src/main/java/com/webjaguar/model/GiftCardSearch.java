/* Copyright 2008 Advanced E-Media Solutions
 * @author Shahin Naji
 * @since 03.20.2008
 */

package com.webjaguar.model;

public class GiftCardSearch {

	// sort
	private String sort = "date_created DESC";
	public String getSort() { return sort; }
	public void setSort(String sort) { this.sort = sort; }	
	
	// claim code
	private String claimCode;
	public String getClaimCode() { return claimCode; }
	public void setClaimCode(String claimCode) { this.claimCode = claimCode; }
		
	// view type
	private String viewType = "order";
	public String getViewType() { return viewType; }
	public void setViewType(String viewType) { this.viewType = viewType; }
	
	// giftcardOrderId
	private Integer giftcardOrderId;
	public Integer getGiftcardOrderId() { return giftcardOrderId; }
	public void setGiftcardOrderId(Integer giftcardOrderId) { this.giftcardOrderId = giftcardOrderId; }
	
	// page 
	private int page = 1;
	public int getPage() { return page; }
	public void setPage(int page) { this.page = page; }
	
	// page size
	private int pageSize = 10;
	public int getPageSize() { return pageSize; }
	public void setPageSize(int pageSize) { this.pageSize = pageSize; }
	
	
	
}
