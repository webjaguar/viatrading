/* Copyright 2009 Advanced E-Media Solutions
 * @author Eduardo Asprec
 * @since 12.14.2009
 */

package com.webjaguar.model;

public class BudgetProductSearch {

	private Integer id;
	public Integer getId() { return id; }
	public void setId(Integer id) { this.id = id; }

	// userId
	private Integer userId;
	public Integer getUserId() { return userId; }
	public void setUserId(Integer userId) { this.userId = userId; }
	
	// year
	private Integer year; 
	public Integer getYear() { return year; }
	public void setYear(Integer year) { this.year = year; }
	
	// page 
	private int page = 1;
	public int getPage() { return page; }
	public void setPage(int page) { this.page = page; }
	
	// page size
	private int pageSize = 10;
	public int getPageSize() { return pageSize; }
	public void setPageSize(int pageSize) { this.pageSize = pageSize; }

	// sort
	private String sort = "product_sku";
	public String getSort() { return sort; }
	public void setSort(String sort) { this.sort = sort; }	
	
	private String sku;
	public String getSku() { return sku; }
	public void setSku(String sku) { this.sku = sku; }
	
	// parent ID
	private Integer parent;
	public Integer getParent() { return parent; }
	public void setParent(Integer parent) { this.parent = parent; }
		
}
