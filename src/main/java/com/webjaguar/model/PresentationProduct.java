package com.webjaguar.model;

public class PresentationProduct {
	private Integer id;
	private String sku;
	private String name;
	private Integer presentationId;
	private Integer saleRepId;
	private double price;
	private Product product;
	private String thumbnail;
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public String getSku() {
		return sku;
	}
	public void setSku(String sku) {
		this.sku = sku;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public Integer getPresentationId() {
		return presentationId;
	}
	public void setPresentationId(Integer presentationId) {
		this.presentationId = presentationId;
	}
	public Integer getSaleRepId() {
		return saleRepId;
	}
	public void setSaleRepId(Integer saleRepId) {
		this.saleRepId = saleRepId;
	}
	public Product getProduct() {
		return product;
	}
	public void setProduct(Product product) {
		this.product = product;
	}
	public double getPrice() {
		return price;
	}
	public void setPrice(double price) {
		this.price = price;
	}
	public String getThumbnail() {
		return thumbnail;
	}
	public void setThumbnail(String thumbnail) {
		this.thumbnail = thumbnail;
	}
	

}
