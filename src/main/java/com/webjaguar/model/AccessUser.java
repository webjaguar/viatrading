package com.webjaguar.model;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class AccessUser
{
    private Integer id;
	private String username;
	private String password;
	private boolean enable;
	private String firstName;
	private String lastName;
	private String email;
	private String ipAddress;
	private List<String> roles;
	private Integer salesRepId;
	private boolean salesRepFilter;
	
	//groups
	private Set<Object> groupSetIds = new HashSet<Object>();
	private Set<Object> groupSetNames = new HashSet<Object>();
	private String groupIds;
	
	private List<AccessUser> subAccounts = new ArrayList<AccessUser>();
	
	public AccessUser() {
		super();
	}
	public AccessUser(String username, Integer id) {
		super();
		this.username = username;
		this.id = id;
	}
	public String getName() {
		return this.firstName + " " + this.lastName;
	}
	public String getIpAddress()
	{
		return ipAddress;
	}
	public void setIpAddress(String ipAddress)
	{
		this.ipAddress = ipAddress;
	}
	public boolean isEnable()
	{
		return enable;
	}
	public void setEnable(boolean enable)
	{
		this.enable = enable;
	}
	public String getFirstName() {
		return firstName;
	}
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}
	public String getLastName() {
		return lastName;
	}
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public Integer getId()
	{
		return id;
	}
	public void setId(Integer id)
	{
		this.id = id;
	}
	public String getPassword()
	{
		return password;
	}
	public void setPassword(String password)
	{
		this.password = password;
	}
	public String getUsername()
	{
		return username;
	}
	public void setUsername(String username)
	{
		this.username = username;
	}
	public List<String> getRoles()
	{
		return roles;
	}
	public void setRoles(List<String> roles)
	{
		this.roles = roles;
	}
	public void setSalesRepId(Integer salesRepId) {
		this.salesRepId = salesRepId;
	}
	public Integer getSalesRepId() {
		return salesRepId;
	}
	public Set<Object> getGroupSetIds() {
		return groupSetIds;
	}
	public void setGroupSetIds(Set<Object> groupSetIds) {
		this.groupSetIds = groupSetIds;
	}
	public String getGroupIds() {
		return groupIds;
	}
	public void setGroupIds(String groupIds) {
		this.groupIds = groupIds;
	}
	public boolean isSalesRepFilter() {
		return salesRepFilter;
	}
	public void setSalesRepFilter(boolean salesRepFilter) {
		this.salesRepFilter = salesRepFilter;
	}
	public void setSubAccounts(List<AccessUser> subAccounts) {
		this.subAccounts = subAccounts;
	}
	public List<AccessUser> getSubAccounts() {
		return subAccounts;
	}
	public void setGroupSetNames(Set<Object> groupSetNames) {
		this.groupSetNames = groupSetNames;
	}
	public Set<Object> getGroupSetNames() {
		return groupSetNames;
	}
	
}