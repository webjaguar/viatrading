package com.webjaguar.model;

import java.io.Serializable;

public class Affiliate implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String firstName;
	private String lastName;
	private Integer nodeId;
	
	// Commissions product
	private Integer productCommissionLevel1;
	private Integer productCommissionLevel2;
	// Commission invoice
	private Double invoiceCommissionLevel1;
	private Double invoiceCommissionLevel2;
	private Boolean percentLevel1 = true;
	private Boolean percentLevel2 = true;
	private String commissionLevel1Status;
	private String commissionLevel2Status;
	private Integer affiliateLevel1Id;
	private Integer affiliateLevel2Id;
	// invoiveCommission + productCommission
	private Double totalCommissionLevel1;
	private Double totalCommissionLevel2;
	// promotion code Id
	private String promoTitle;
	// Affiliate landing page
	private Integer categoryId;
	private String DomainName;
	// Commission based on Order
	private Double firstOrderCommission;
	private Boolean firstOrderPercent = true;
	private Double otherOrderCommission;
	private Boolean otherOrderPercent = true;

	// Default Constructor
	public Affiliate() {}
	
	
	public Boolean getPercentLevel1()
	{
		return percentLevel1;
	}
	public void setPercentLevel1(Boolean percentLevel1)
	{
		this.percentLevel1 = percentLevel1;
    }
	public Boolean getPercentLevel2()
	{
		return percentLevel2;
	}
	public void setPercentLevel2(Boolean percentLevel2)
	{
		this.percentLevel2 = percentLevel2;
	}
	public Affiliate( Integer nodeId )
	{
		this.nodeId = nodeId;
	}
	public Integer getNodeId()
	{
		return nodeId;
	}
	public void setNodeId(Integer nodeId)
	{
		this.nodeId = nodeId;
	}
	public String getFirstName()
	{
		return firstName;
	}
	public void setFirstName(String firstName)
	{
		this.firstName = firstName;
	}
	public String getLastName()
	{
		return lastName;
	}
	public void setLastName(String lastName)
	{
		this.lastName = lastName;
	}
	public Double getInvoiceCommissionLevel1()
	{
		return invoiceCommissionLevel1;
	}
	public void setInvoiceCommissionLevel1(Double invoiceCommissionLevel1)
	{
		this.invoiceCommissionLevel1 = invoiceCommissionLevel1;
	}
	public Double getInvoiceCommissionLevel2()
	{
		return invoiceCommissionLevel2;
	}
	public void setInvoiceCommissionLevel2(Double invoiceCommissionLevel2)
	{
		this.invoiceCommissionLevel2 = invoiceCommissionLevel2;
	}
	public Integer getProductCommissionLevel1()
	{
		return productCommissionLevel1;
	}
	public void setProductCommissionLevel1(Integer productCommissionLevel1)
	{
		this.productCommissionLevel1 = productCommissionLevel1;
	}
	public Integer getProductCommissionLevel2()
	{
		return productCommissionLevel2;
	}
	public void setProductCommissionLevel2(Integer productCommissionLevel2)
	{
		this.productCommissionLevel2 = productCommissionLevel2;
	}
	
	public Integer getAffiliateLevel1Id()
	{
		return affiliateLevel1Id;
	}
	public void setAffiliateLevel1Id(Integer affiliateLevel1Id)
	{
		this.affiliateLevel1Id = affiliateLevel1Id;
	}
	public Integer getAffiliateLevel2Id()
	{
		return affiliateLevel2Id;
	}
	public void setAffiliateLevel2Id(Integer affiliateLevel2Id)
	{
		this.affiliateLevel2Id = affiliateLevel2Id;
	}
	public Double getTotalCommissionLevel1()
	{
		return totalCommissionLevel1;
	}
	public void setTotalCommissionLevel1(Double totalCommissionLevel1)
	{
		this.totalCommissionLevel1 = totalCommissionLevel1;
	}
	public Double getTotalCommissionLevel2()
	{
		return totalCommissionLevel2;
	}
	public void setTotalCommissionLevel2(Double totalCommissionLevel2)
	{
		this.totalCommissionLevel2 = totalCommissionLevel2;
	}
	public String getPromoTitle()
	{
		return promoTitle;
	}
	public void setPromoTitle(String promoTitle)
	{
		this.promoTitle = promoTitle;
	}


	public String getCommissionLevel1Status()
	{
		return commissionLevel1Status;
	}


	public void setCommissionLevel1Status(String commissionLevel1Status)
	{
		this.commissionLevel1Status = commissionLevel1Status;
	}


	public String getCommissionLevel2Status()
	{
		return commissionLevel2Status;
	}


	public void setCommissionLevel2Status(String commissionLevel2Status)
	{
		this.commissionLevel2Status = commissionLevel2Status;
	}


	public Integer getCategoryId()
	{
		return categoryId;
	}


	public void setCategoryId(Integer categoryId)
	{
		this.categoryId = categoryId;
	}


	public String getDomainName()
	{
		return DomainName;
	}


	public void setDomainName(String domainName)
	{
		DomainName = domainName;
	}


	public Double getFirstOrderCommission() {
		return firstOrderCommission;
	}


	public void setFirstOrderCommission(Double firstOrderCommission) {
		this.firstOrderCommission = firstOrderCommission;
	}


	public Double getOtherOrderCommission() {
		return otherOrderCommission;
	}


	public void setOtherOrderCommission(Double otherOrderCommission) {
		this.otherOrderCommission = otherOrderCommission;
	}


	public Boolean getFirstOrderPercent() {
		return firstOrderPercent;
	}


	public void setFirstOrderPercent(Boolean firstOrderPercent) {
		this.firstOrderPercent = firstOrderPercent;
	}


	public Boolean getOtherOrderPercent() {
		return otherOrderPercent;
	}


	public void setOtherOrderPercent(Boolean otherOrderPercent) {
		this.otherOrderPercent = otherOrderPercent;
	}




}
