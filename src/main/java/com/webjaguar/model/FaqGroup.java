package com.webjaguar.model;

public class FaqGroup {
	
	public FaqGroup(){}
	
	private Integer id;
	private String name;
	private Integer rank;
	private int faqCount;
	// protected
	private String protectedLevel = "0";
	
	// gettter and setter
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public Integer getRank() {
		return rank;
	}
	public void setRank(Integer rank) {
		this.rank = rank;
	}
	public int getFaqCount() {
		return faqCount;
	}
	public void setFaqCount(int faqCount) {
		this.faqCount = faqCount;
	}
	public String getProtectedLevel()
	{
		return protectedLevel;
	}
	public void setProtectedLevel(String protectedLevel)
	{
		this.protectedLevel = protectedLevel;
	}
	public Integer getProtectedLevelAsNumber() {
		if (protectedLevel.contains( "1" )) {
			return protectedLevel.length();
		} else {
			return 0;
		}
	}

}
