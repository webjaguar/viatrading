/* Copyright 2007 Advanced E-Media Solutions
 * @author Eduardo Asprec
 * @since 02.10.2007
 */

package com.webjaguar.model;

import java.sql.Timestamp;

public class OrderStatus {
	
	private Integer orderId;
	private String status = "p";
	private String subStatus;
	private Timestamp dateChanged;
	private boolean userNotified;
	private String comments;
	private String carrier;
	private String subject;	
	private String message;	
	private boolean htmlMessage;
	private String trackNum;
	private String trackNumVoid;
	private String from;
	private String cc;
	private String username;
	
	public String cancelReason;
	
	public String getCancelReason() {
		return cancelReason;
	}
	public void setCancelReason(String cancelReason) {
		this.cancelReason = cancelReason;
	}
	// amazon checkout - Document Transaction ID
	private Long amazonDocTransID;
	
	private String deliveryPerson;
	private Integer priority;
	
	public String getComments()
	{
		return comments;
	}
	public void setComments(String comments)
	{
		this.comments = comments;
	}
	public Timestamp getDateChanged()
	{
		return dateChanged;
	}
	public void setDateChanged(Timestamp dateChanged)
	{
		this.dateChanged = dateChanged;
	}
	public Integer getOrderId()
	{
		return orderId;
	}
	public void setOrderId(Integer orderId)
	{
		this.orderId = orderId;
	}
	public String getStatus()
	{
		return status;
	}
	public void setStatus(String status)
	{
		this.status = status;
	}
	public boolean isUserNotified()
	{
		return userNotified;
	}
	public void setUserNotified(boolean userNotified)
	{
		this.userNotified = userNotified;
	}
	public String getMessage()
	{
		return message;
	}
	public void setMessage(String message)
	{
		this.message = message;
	}
	public String getTrackNum()
	{
		return trackNum;
	}
	public void setTrackNum(String trackNum)
	{
		this.trackNum = trackNum;
	}
	public String getSubject()
	{
		return subject;
	}
	public void setSubject(String subject)
	{
		this.subject = subject;
	}
	public String getCc()
	{
		return cc;
	}
	public void setCc(String cc)
	{
		this.cc = cc;
	}
	public String getFrom() {
		return from;
	}
	public void setFrom(String from) {
		this.from = from;
	}
	public OrderStatus( Integer orderId, String status, boolean userNotified, String comments, String subject, String message, String trackNum, String cc )
	{
		super();
		this.orderId = orderId;
		this.status = status;
		this.userNotified = userNotified;
		this.comments = comments;
		this.subject = subject;
		this.message = message;
		this.trackNum = trackNum;
		this.cc = cc;
	}
	public OrderStatus( Integer orderId, String status, String subject, String message )
	{
		super();
		this.orderId = orderId;
		this.status = status;
		this.subject = subject;
		this.message = message;
	}
	public OrderStatus()
	{
		super();
	}
	public String getTrackNumVoid()
	{
		return trackNumVoid;
	}
	public void setTrackNumVoid(String trackNumVoid)
	{
		this.trackNumVoid = trackNumVoid;
	}
	public boolean isHtmlMessage()
	{
		return htmlMessage;
	}
	public void setHtmlMessage(boolean htmlMessage)
	{
		this.htmlMessage = htmlMessage;
	}
	public String getSubStatus() {
		return subStatus;
	}
	public void setSubStatus(String subStatus) {
		this.subStatus = subStatus;
	}
	public String getUsername() {
		return username;
	}
	public void setUsername(String username) {
		this.username = username;
	}
	public Long getAmazonDocTransID() {
		return amazonDocTransID;
	}
	public void setAmazonDocTransID(Long amazonDocTransID) {
		this.amazonDocTransID = amazonDocTransID;
	}
	public String getDeliveryPerson() {
		return deliveryPerson;
	}
	public void setDeliveryPerson(String deliveryPerson) {
		this.deliveryPerson = deliveryPerson;
	}
	public Integer getPriority() {
		return priority;
	}
	public void setPriority(Integer priority) {
		this.priority = priority;
	}
	
	public boolean isOrderCancel() {
		if (this.getStatus().startsWith("x")) {
			return true;
		}
		return false;
	}
	public String getCarrier() {
		return carrier;
	}
	public void setCarrier(String carrier) {
		this.carrier = carrier;
	}
}