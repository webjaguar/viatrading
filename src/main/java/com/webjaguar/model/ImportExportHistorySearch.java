/* Copyright 2008 Advanced E-Media Solutions
 * @author Shahin Naji
 */

package com.webjaguar.model;

public class ImportExportHistorySearch {

	// sort
	private String sort = "created DESC";
	public String getSort() { return sort; }
	public void setSort(String sort) { this.sort = sort; }	
	
	// page 
	private int page = 1;
	public int getPage() { return page; }
	public void setPage(int page) { this.page = page; }
	
	// page size
	private int pageSize = 10;
	public int getPageSize() { return pageSize; }
	public void setPageSize(int pageSize) { this.pageSize = pageSize; }
	
	// username
	private String username;
	public String getUsername() { return username; }
	public void setUsername(String username) { this.username = username; }
	
	// type
	private String type = "";
	public String getType() { return type; }
	public void setType(String type) { this.type = type; }
	
	//import
	private String imported;
	public String getImported() { return imported; }
	public void setImported(String imported) { this.imported = imported; }
	
	
}
