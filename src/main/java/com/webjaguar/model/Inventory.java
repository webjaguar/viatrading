/*
 * Copyright 2005, 2006 Advanced E-Media Solutions
 * 
 */
package com.webjaguar.model;

public class Inventory {
	private Integer inventory;
	private Integer inventoryAFS;
	private Integer lowInventory;
	private boolean showNegInventory;
	private boolean negInventory;
	private boolean backToNull;
	private String sku;
	
	public boolean isBackToNull() {
		return backToNull;
	}
	public void setBackToNull(boolean backToNull) {
		this.backToNull = backToNull;
	}
	public boolean isNegInventory() {
		return negInventory;
	}
	public void setNegInventory(boolean negInventory) {
		this.negInventory = negInventory;
	}
	public Integer getLowInventory() {
		return lowInventory;
	}
	public void setLowInventory(Integer lowInventory) {
		this.lowInventory = lowInventory;
	}
	public boolean isShowNegInventory() {
		return showNegInventory;
	}
	public void setShowNegInventory(boolean showNegInventory) {
		this.showNegInventory = showNegInventory;
	}
	public String getSku() {
		return sku;
	}
	public void setSku(String sku) {
		this.sku = sku;
	}
	public Integer getInventory() {
		return inventory;
	}
	public void setInventory(Integer inventory) {
		this.inventory = inventory;
	}
	public Integer getInventoryAFS() {
		return inventoryAFS;
	}
	public void setInventoryAFS(Integer inventoryAFS) {
		this.inventoryAFS = inventoryAFS;
	}
}
