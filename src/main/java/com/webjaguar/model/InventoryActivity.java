/* Copyright 2010 Advanced E-Media Solutions
 * @author Jwalant Patel
 * @since 09.10.2010
 */

package com.webjaguar.model;

import java.util.Date;

public class InventoryActivity {
	
	private Integer id;
	private String sku;
	private String type;
	private Date date;
	private String reference;
	private Integer quantity;
	private Integer inventory;
	private Integer inventoryAFS;
	
	// this variable is only for report. Fetches inv afs from product table directly.For maintaining audit.
	//Should be deleted once the bug in inv afs is found.
	private Integer invAFS; 
	
	private Integer accessUserId;
	private String accessUser;
	private String note;
	private Integer lowInventory;
	private boolean showNegInventory;
	private boolean negInventory;
	private boolean backToNull;
	private boolean adjustAvForSaleOnly;
	
	public boolean isBackToNull() {
		return backToNull;
	}
	public void setBackToNull(boolean backToNull) {
		this.backToNull = backToNull;
	}
	public Integer getinventoryAFS() {
		return inventoryAFS;
	}
	public void setinventoryAFS(Integer inventoryAFS) {
		this.inventoryAFS = inventoryAFS;
	}
	public Integer getLowInventory() {
		return lowInventory;
	}
	public void setLowInventory(Integer lowInventory) {
		this.lowInventory = lowInventory;
	}
	public boolean isShowNegInventory() {
		return showNegInventory;
	}
	public void setShowNegInventory(boolean showNegInventory) {
		this.showNegInventory = showNegInventory;
	}
	public boolean isNegInventory() {
		return negInventory;
	}
	public void setNegInventory(boolean negInventory) {
		this.negInventory = negInventory;
	}
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public String getSku() {
		return sku;
	}
	public void setSku(String sku) {
		this.sku = sku;
	}
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	public Date getDate() {
		return date;
	}
	public void setDate(Date date) {
		this.date = date;
	}
	public String getReference() {
		return reference;
	}
	public void setReference(String reference) {
		this.reference = reference;
	}
	public Integer getQuantity() {
		return quantity;
	}
	public void setQuantity(Integer quantity) {
		this.quantity = quantity;
	}
	public Integer getAccessUserId() {
		return accessUserId;
	}
	public void setAccessUserId(Integer accessUserId) {
		this.accessUserId = accessUserId;
	}
	public String getNote() {
		return note;
	}
	public void setNote(String note) {
		this.note = note;
	}
	public void setInventory(Integer inventory) {
		this.inventory = inventory;
		// set inventory on hand here.
	}
	public Integer getInventory() {
		return inventory;
	}
	public Integer getInventoryAFS() {
		return inventoryAFS;
	}
	public void setInventoryAFS(Integer inventoryAFS) {
		this.inventoryAFS = inventoryAFS;
	}
	public void setAccessUser(String accessUser) {
		this.accessUser = accessUser;
	}
	public String getAccessUser() {
		return accessUser;
	}
	public boolean isAdjustAvForSaleOnly() {
		return adjustAvForSaleOnly;
	}
	public void setAdjustAvForSaleOnly(boolean adjustAvForSaleOnly) {
		this.adjustAvForSaleOnly = adjustAvForSaleOnly;
	}
	public Integer getInvAFS() {
		return invAFS;
	}
	public void setInvAFS(Integer invAFS) {
		this.invAFS = invAFS;
	}
}