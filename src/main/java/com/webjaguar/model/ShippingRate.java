/* Copyright 2006 Advanced E-Media Solutions
 * @author Li Qin
 * @since 10.10.2006
 */

package com.webjaguar.model;

import java.util.Date;
import java.util.HashSet;
import java.util.Set;

public class ShippingRate {
	
	private int id;
	private Integer carrierId;
	private String carrier;
	private boolean ignoreCarriers;
	private String code;
	private String title;
	private String price;
	private int percent = 0;
	private Integer rank;
	private boolean isAvailable;
	private String temperature;
	private boolean internal;
	private Double minPrice;
	private Double maxPrice;
	private Double minWeight;
	private Double maxWeight;
	private Double handling;
	private String country;
	private String host;
	private String guaranteedDaysToDelivery;
	private Date deliveryDate;
	private String shippingPeriod;
	private Integer customerType;
	private String description;
	
	//CustomShipping info
	private String companyName;
	private String companyContactName;
	private String companyEmail;
	private String companyTel1;
	private String companyTel2;
	
	private Set<Object> groupIdSet = new HashSet<Object>();
	private String groupIds;
	private String groupType = "include";
	private String states;
	private String stateType = "include";
	private String message;
	
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
	// category
	private Integer categoryId;
	
	public String getShippingPeriod()
	{
		return shippingPeriod;
	}
	public void setShippingPeriod(String shippingPeriod)
	{
		this.shippingPeriod = shippingPeriod;
	}
	public String getGuaranteedDaysToDelivery()
	{
		return guaranteedDaysToDelivery;
	}
	public void setGuaranteedDaysToDelivery(String guaranteedDaysToDelivery)
	{
		this.guaranteedDaysToDelivery = guaranteedDaysToDelivery;
	}
	public String getTemperature()
	{
		return temperature;
	}
	public void setTemperature(String temperature)
	{
		this.temperature = temperature;
	}
	public ShippingRate()
	{
		super();
	}
	public String getCarrier(){
		return carrier;
	}
	public void setCarrier(String carrier){
		this.carrier = carrier;
	}
	
	public String getCode() {
		return code;
	}
	
	public void setCode(String code) {
		this.code = code;
	}
	
	public String getTitle() {
		return title;
	}
	
	public void setTitle(String title) {
		this.title = title;
	}
	
	public String getPrice() {
		return price;
	}
	
	public void setPrice(String price) {
		this.price = price;
	}
	
	public void setPercent(int percent) {
		this.percent = percent;
	}
	public int getPercent() {
		return percent;
	}
	public boolean getIsAvailable() {
		return isAvailable;
	}
	
	public void setIsAvailable(boolean isAvailable) {
		this.isAvailable = isAvailable;
	}
	
	public Integer getRank() {
		return rank;
	}
	
	public void setRank(Integer rank) {
		this.rank = rank;
	}
	public ShippingRate( String title, String price )
	{
		super();
		this.title = title;
		this.price = price;
	}
	public int getId()
	{
		return id;
	}
	public void setId(int id)
	{
		this.id = id;
	}
	public boolean isInternal()
	{
		return internal;
	}
	public void setInternal(boolean internal)
	{
		this.internal = internal;
	}
	public Double getMinPrice()
	{
		return minPrice;
	}
	public void setMinPrice(Double minPrice)
	{
		this.minPrice = minPrice;
	}
	public Double getMaxPrice()
	{
		return maxPrice;
	}
	public void setMaxPrice(Double maxPrice)
	{
		this.maxPrice = maxPrice;
	}
	public String getCountry()
	{
		return country;
	}
	public void setCountry(String country)
	{
		this.country = country;
	}
	public void setAvailable(boolean isAvailable)
	{
		this.isAvailable = isAvailable;
	}
	public String getHost()
	{
		return host;
	}
	public void setHost(String host)
	{
		this.host = host;
	}
	public Date getDeliveryDate()
	{
		return deliveryDate;
	}
	public void setDeliveryDate(Date deliveryDate)
	{
		this.deliveryDate = deliveryDate;
	}
	public Integer getCarrierId()
	{
		return carrierId;
	}
	public void setCarrierId(Integer carrierId)
	{
		this.carrierId = carrierId;
	}
	public Integer getCustomerType()
	{
		return customerType;
	}
	public void setCustomerType(Integer customerType)
	{
		this.customerType = customerType;
	}
	public String getCompanyName() {
		return companyName;
	}
	public void setCompanyName(String companyName) {
		this.companyName = companyName;
	}
	public String getCompanyContactName() {
		return companyContactName;
	}
	public void setCompanyContactName(String companyContactName) {
		this.companyContactName = companyContactName;
	}
	public String getCompanyEmail() {
		return companyEmail;
	}
	public void setCompanyEmail(String companyEmail) {
		this.companyEmail = companyEmail;
	}
	public String getCompanyTel1() {
		return companyTel1;
	}
	public void setCompanyTel1(String companyTel1) {
		this.companyTel1 = companyTel1;
	}
	public String getCompanyTel2() {
		return companyTel2;
	}
	public void setCompanyTel2(String companyTel2) {
		this.companyTel2 = companyTel2;
	}
	public Set<Object> getGroupIdSet() {
		return groupIdSet;
	}
	public void setGroupIdSet(Set<Object> groupIdSet) {
		this.groupIdSet = groupIdSet;
	}
	public String getGroupIds() {
		return groupIds;
	}
	public void setGroupIds(String groupIds) {
		this.groupIds = groupIds;
	}
	public String getGroupType() {
		return groupType;
	}
	public void setGroupType(String groupType) {
		this.groupType = groupType;
	}
	public String getStates() {
		return states;
	}
	public void setStates(String states) {
		this.states = states;
	}
	public String getStateType() {
		return stateType;
	}
	public void setStateType(String stateType) {
		this.stateType = stateType;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public void setHandling(Double handling) {
		this.handling = handling;
	}
	public Double getHandling() {
		return handling;
	}
	public void setCategoryId(Integer categoryId) {
		this.categoryId = categoryId;
	}
	public Integer getCategoryId() {
		return categoryId;
	}
	public void setMaxWeight(Double maxWeight) {
		this.maxWeight = maxWeight;
	}
	public Double getMaxWeight() {
		return maxWeight;
	}
	public void setMinWeight(Double minWeight) {
		this.minWeight = minWeight;
	}
	public Double getMinWeight() {
		return minWeight;
	}
	public void setIgnoreCarriers(boolean ignoreCarriers) {
		this.ignoreCarriers = ignoreCarriers;
	}
	public boolean isIgnoreCarriers() {
		return ignoreCarriers;
	}
}