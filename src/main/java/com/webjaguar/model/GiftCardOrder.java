/*
 * Copyright 2008 Advanced E-Media Solutions
 * 
 */

package com.webjaguar.model;

import java.sql.Timestamp;
import java.util.Date;

public class GiftCardOrder
{

	private Integer giftCardOrderId;
	private Double subTotal;
	private Integer quantity = 1;
	private CreditCard creditCard = new CreditCard();
	private Address billing = new Address();
	private Date dateCreated;
	private String paymentMethod;
	private String status;
	private Timestamp statusChangeDate;
	private GiftCardStatus statusHistory;
	public Integer getGiftCardOrderId()
	{
		return giftCardOrderId;
	}
	public void setGiftCardOrderId(Integer giftCardOrderId)
	{
		this.giftCardOrderId = giftCardOrderId;
	}
	public Double getSubTotal()
	{
		return subTotal;
	}
	public void setSubTotal(Double subTotal)
	{
		this.subTotal = subTotal;
	}
	public Integer getQuantity()
	{
		return quantity;
	}
	public void setQuantity(Integer quantity)
	{
		this.quantity = quantity;
	}
	public CreditCard getCreditCard()
	{
		return creditCard;
	}
	public void setCreditCard(CreditCard creditCard)
	{
		this.creditCard = creditCard;
	}
	public Address getBilling()
	{
		return billing;
	}
	public void setBilling(Address billing)
	{
		this.billing = billing;
	}
	public Date getDateCreated()
	{
		return dateCreated;
	}
	public void setDateCreated(Date dateCreated)
	{
		this.dateCreated = dateCreated;
	}
	public String getPaymentMethod() {
		return paymentMethod;
	}
	public void setPaymentMethod(String paymentMethod) {
		this.paymentMethod = paymentMethod;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public Timestamp getStatusChangeDate() {
		return statusChangeDate;
	}
	public void setStatusChangeDate(Timestamp statusChangeDate) {
		this.statusChangeDate = statusChangeDate;
	}
	public GiftCardStatus getStatusHistory() {
		return statusHistory;
	}
	public void setStatusHistory(GiftCardStatus statusHistory) {
		this.statusHistory = statusHistory;
	}
	
}
