package com.webjaguar.model;

import java.util.Date;

public class EventMember {
	
	private Integer id;
	private Integer userId;
	private Integer eventId;
	private String username;
	private String firstName;
	private String lastName;
	private String cardId;
	private Integer userIndex;
	private Integer salesRepId;
	private Date registerDate;
	private String htmlCode;
	private String comment;
	private String field1;
	private String field20;
	private Integer numOf300PalRegister;
	private Integer numOfAuctionRegister;
	private Integer numOf300PalOrders;
	private Integer numOfAuctionOrders;
	private Integer sales300Pal;
	private Integer salesAuction;
	
	//list
	private Integer orderCount;
	private Double ordersGrandTotal;
	private Date lastOrderDate;
	private String accountNumber;
	private String phone;
	private String taxId;
	
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public Integer getUserId() {
		return userId;
	}
	public void setUserId(Integer userId) {
		this.userId = userId;
	}
	public Integer getEventId() {
		return eventId;
	}
	public void setEventId(Integer eventId) {
		this.eventId = eventId;
	}
	public String getUsername() {
		return username;
	}
	public void setUsername(String username) {
		this.username = username;
	}
	public String getFirstName() {
		return firstName;
	}
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}
	public String getLastName() {
		return lastName;
	}
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}
	public String getCardId() {
		return cardId;
	}
	public void setCardId(String cardId) {
		this.cardId = cardId;
	}
	public Integer getUserIndex() {
		return userIndex;
	}
	public void setUserIndex(Integer userIndex) {
		this.userIndex = userIndex;
	}
	public Integer getSalesRepId() {
		return salesRepId;
	}
	public void setSalesRepId(Integer salesRepId) {
		this.salesRepId = salesRepId;
	}
	public Date getRegisterDate() {
		return registerDate;
	}
	public void setRegisterDate(Date registerDate) {
		this.registerDate = registerDate;
	}
	public String getHtmlCode() {
		return htmlCode;
	}
	public void setHtmlCode(String htmlCode) {
		this.htmlCode = htmlCode;
	}
	public String getComment() {
		return comment;
	}
	public void setComment(String comment) {
		this.comment = comment;
	}
	public String getField1() {
		return field1;
	}
	public void setField1(String field1) {
		this.field1 = field1;
	}
	public Integer getOrderCount() {
		return orderCount;
	}
	public void setOrderCount(Integer orderCount) {
		this.orderCount = orderCount;
	}
	public Double getOrdersGrandTotal() {
		return ordersGrandTotal;
	}
	public void setOrdersGrandTotal(Double ordersGrandTotal) {
		this.ordersGrandTotal = ordersGrandTotal;
	}
	public Date getLastOrderDate() {
		return lastOrderDate;
	}
	public void setLastOrderDate(Date lastOrderDate) {
		this.lastOrderDate = lastOrderDate;
	}
	public String getAccountNumber() {
		return accountNumber;
	}
	public void setAccountNumber(String accountNumber) {
		this.accountNumber = accountNumber;
	}
	public String getPhone() {
		return phone;
	}
	public void setPhone(String phone) {
		this.phone = phone;
	}
	public Integer getNumOf300PalRegister() {
		return numOf300PalRegister;
	}
	public void setNumOf300PalRegister(Integer numOf300PalRegister) {
		this.numOf300PalRegister = numOf300PalRegister;
	}
	public Integer getNumOfAuctionRegister() {
		return numOfAuctionRegister;
	}
	public void setNumOfAuctionRegister(Integer numOfAuctionRegister) {
		this.numOfAuctionRegister = numOfAuctionRegister;
	}
	public Integer getNumOf300PalOrders() {
		return numOf300PalOrders;
	}
	public void setNumOf300PalOrders(Integer numOf300PalOrders) {
		this.numOf300PalOrders = numOf300PalOrders;
	}
	public Integer getNumOfAuctionOrders() {
		return numOfAuctionOrders;
	}
	public void setNumOfAuctionOrders(Integer numOfAuctionOrders) {
		this.numOfAuctionOrders = numOfAuctionOrders;
	}
	public Integer getSales300Pal() {
		return sales300Pal;
	}
	public void setSales300Pal(Integer sales300Pal) {
		this.sales300Pal = sales300Pal;
	}
	public Integer getSalesAuction() {
		return salesAuction;
	}
	public void setSalesAuction(Integer salesAuction) {
		this.salesAuction = salesAuction;
	}
	public String getTaxId() {
		return taxId;
	}
	public void setTaxId(String taxId) {
		this.taxId = taxId;
	}
	public String getField20() {
		return field20;
	}
	public void setField20(String field20) {
		this.field20 = field20;
	}

	
}