package com.webjaguar.model;

import java.util.Date;
import java.util.List;


public class Presentation {
	private Integer id;
	private String name;
	private String title;
	private Integer saleRepId;
	private List<Product> product;
	private Date created;
	private Date modified;
	private Integer templateId;
	private Integer userId;
	private String template;
	private Integer noOfProduct;
	private String userEmail;
	private String printerFriendly;
	private Integer maxNoOfProduct;
	private String design;
	private Integer viewed;
	private boolean editPrice;
	private boolean managerUpdated;
	private boolean manager;
	private String url;
	private String editUrl;
	private String note;
	private String sort = "id desc";
	private Integer lastUpdatedBy;
	private String lastUpdatedByName;
	private String footerHtmlCode;
	private Integer page;
	private Integer pageCount;
	private Integer pageSize;
	private Integer offSet;
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public Integer getSaleRepId() {
		return saleRepId;
	}
	public void setSaleRepId(Integer saleRepId) {
		this.saleRepId = saleRepId;
	}
	public List<Product> getProduct() {
		return product;
	}
	public void setProduct(List<Product> presentationProductList) {
		this.product = presentationProductList;
	}
	public Date getCreated() {
		return created;
	}
	public void setCreated(Date created) {
		this.created = created;
	}
	public Date getModified() {
		return modified;
	}
	public void setModified(Date modified) {
		this.modified = modified;
	}
	public Integer getTemplateId() {
		return templateId;
	}
	public void setTemplateId(Integer templateId) {
		this.templateId = templateId;
	}
	public Integer getUserId() {
		return userId;
	}
	public void setUserId(Integer userId) {
		this.userId = userId;
	}
	public String getTemplate() {
		return template;
	}
	public void setTemplate(String template) {
		this.template = template;
	}
	public Integer getNoOfProduct() {
		return noOfProduct;
	}
	public void setNoOfProduct(Integer noOfProduct) {
		this.noOfProduct = noOfProduct;
	}
	public String getUserEmail() {
		return userEmail;
	}
	public void setUserEmail(String userEmail) {
		this.userEmail = userEmail;
	}
	public String getPrinterFriendly() {
		return printerFriendly;
	}
	public void setPrinterFriendly(String printerFriendly) {
		this.printerFriendly = printerFriendly;
	}
	public Integer getMaxNoOfProduct() {
		return maxNoOfProduct;
	}
	public void setMaxNoOfProduct(Integer maxNoOfProduct) {
		this.maxNoOfProduct = maxNoOfProduct;
	}
	public String getDesign() {
		return design;
	}
	public void setDesign(String design) {
		this.design = design;
	}
	public Integer getViewed() {
		return viewed;
	}
	public void setViewed(Integer viewed) {
		this.viewed = viewed;
	}
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public boolean isEditPrice() {
		return editPrice;
	}
	public void setEditPrice(boolean editPrice) {
		this.editPrice = editPrice;
	}
	public String getUrl() {
		return url;
	}
	public void setUrl(String url) {
		this.url = url;
	}
	public String getEditUrl() {
		return editUrl;
	}
	public void setEditUrl(String editUrl) {
		this.editUrl = editUrl;
	}
	public String getNote() {
		return note;
	}
	public void setNote(String note) {
		this.note = note;
	}
	public String getSort() {
		return sort;
	}
	public void setSort(String sort) {
		this.sort = sort;
	}
	public Integer getLastUpdatedBy() {
		return lastUpdatedBy;
	}
	public void setLastUpdatedBy(Integer lastUpdatedBy) {
		this.lastUpdatedBy = lastUpdatedBy;
	}
	public String getLastUpdatedByName() {
		return lastUpdatedByName;
	}
	public void setLastUpdatedByName(String lastUpdatedByName) {
		this.lastUpdatedByName = lastUpdatedByName;
	}
	public boolean isManagerUpdated() {
		return managerUpdated;
	}
	public void setManagerUpdated(boolean managerUpdated) {
		this.managerUpdated = managerUpdated;
	}
	public boolean isManager() {
		return manager;
	}
	public void setManager(boolean manager) {
		this.manager = manager;
	}
	public String getFooterHtmlCode() {
		return footerHtmlCode;
	}
	public void setFooterHtmlCode(String footerHtmlCode) {
		this.footerHtmlCode = footerHtmlCode;
	}
	public Integer getPage() {
		return page;
	}
	public void setPage(Integer page) {
		this.page = page;
	}
	public Integer getPageCount() {
		return pageCount;
	}
	public void setPageCount(Integer pageCount) {
		this.pageCount = pageCount;
	}
	public Integer getPageSize() {
		return pageSize;
	}
	public void setPageSize(Integer pageSize) {
		this.pageSize = pageSize;
	}
	public Integer getOffSet() {
		return offSet;
	}
	public void setOffSet(Integer offSet) {
		this.offSet = offSet;
	}

}
