/* Copyright 2007 Advanced E-Media Solutions
 * @author Eduardo Asprec
 * @since 09.24.2007
 */

package com.webjaguar.model;

import java.util.ArrayList;
import java.util.List;

public class ServiceableItemSearch {

	private String itemId;
	private String serialNum;
	private String sku;
	private Integer userId;
	private List<Integer> userIdList;
	private int page = 1;
	private int pageSize = 10;
	// sort
	private String sort= "item_id";
	// company name
	private String companyName;
	private boolean withServiceTotal;
	
	public String getItemId()
	{
		return itemId;
	}
	public void setItemId(String itemId)
	{
		this.itemId = itemId;
	}
	public String getSerialNum()
	{
		return serialNum;
	}
	public void setSerialNum(String serialNum)
	{
		this.serialNum = serialNum;
	}
	public String getSku()
	{
		return sku;
	}
	public void setSku(String sku)
	{
		this.sku = sku;
	}
	public int getPage()
	{
		return page;
	}
	public void setPage(int page)
	{
		this.page = page;
	}
	public int getPageSize()
	{
		return pageSize;
	}
	public void setPageSize(int pageSize)
	{
		this.pageSize = pageSize;
	}
	public String getSort()
	{
		return sort;
	}
	public void setSort(String sort)
	{
		this.sort = sort;
	}
	public Integer getUserId()
	{
		return userId;
	}
	public void setUserId(Integer userId)
	{
		this.userId = userId;
	}
	public List<Integer> getUserIdList()
	{
		return userIdList;
	}
	public void setUserIdList(List<Integer> userIdList)
	{
		this.userIdList = userIdList;
	}
	public void addUserId(Integer userId) 
	{
		if (this.userIdList == null) {
			this.userIdList = new ArrayList<Integer>(); 
		}
		this.userIdList.add( userId );
	}
	public String getCompanyName()
	{
		return companyName;
	}
	public void setCompanyName(String companyName)
	{
		this.companyName = companyName;
	}
	public boolean isWithServiceTotal()
	{
		return withServiceTotal;
	}
	public void setWithServiceTotal(boolean withServiceTotal)
	{
		this.withServiceTotal = withServiceTotal;
	}

}
