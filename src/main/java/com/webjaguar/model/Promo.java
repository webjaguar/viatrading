/*
 * Copyright 2006 Advanced E-Media Solutions @author Shahin Naji
 * @author Shahin Naji
 * @since 01.23.2007
 */

package com.webjaguar.model;

import java.util.Date;
import java.util.HashSet;
import java.util.Set;



public class Promo
{

	private Integer promoId;
	private Integer userId;
	private String title;
	private String sku;
	
	private boolean percent = true;
	private boolean onetimeUsePerCustomer = false;
	private boolean promoForNewCustomersOnly = false  ;
	public boolean getpromoForNewCustomersOnly() {
		return promoForNewCustomersOnly;
	}

	public void setpromoForNewCustomersOnly(boolean promoForNewCustomersOnly) {
		this.promoForNewCustomersOnly = promoForNewCustomersOnly;
	}

	private boolean onetimeUseOnly = false;
	private boolean isDynamic;
	private String prefix;
	private Double discount;
	private Double minOrder;
	private Double minOrderPerBrand;
	private Integer minQty;
	private Date startDate;
	private Date endDate;
	private Date createdOn;
	private String createdBy;
	private Integer rank;
    private String htmlCode;
	private Set<Object> groupIdSet = new HashSet<Object>();
	private String groupIds;
	private String groups;
	private String groupNote;
	private String groupType = "include";
	private String discountType = "order";
	private String states;
	private String stateType = "include";
	private String skus;
	private Set<Object> skuSet = new HashSet<Object>();
	private String brands;
	private String parentSkus;
	private Set<Object> parentSkuSet = new HashSet<Object>();
	
	private String categoryIds;
	private Set<Object> categoryIdSet = new HashSet<Object>();

	private Set<Object> brandSet = new HashSet<Object>();
	private Set<Object> shippingIdSet;
    private  boolean productAvailability = false;
    
    // used for campaign
   private int count;
    
	
	public Promo() {
		super();
	}
	
	public Promo( String title, boolean percent, Double discount ) {
		super();
		this.title = title;
		this.percent = percent;
		this.discount = discount;
	}

	public boolean isInEffect() {
		// today's date
		com.webjaguar.model.DateBean today = new com.webjaguar.model.DateBean( );

		try{
		int start = this.startDate.compareTo(today.getTime());
		int end = this.endDate.compareTo(today.getTime());
		if ( start == 0 )
			return true;
		else if ( start > 0 || end < 0  )
			return false;
		else
			return true;
		}catch(Exception ex){
			return false;
		}
	}

	public Integer getPromoId()	{
		return promoId;
	}

	public void setPromoId(Integer promoId) {
		this.promoId = promoId;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public boolean isPercent() {
		return percent;
	}

	public void setPercent(boolean percent) {
		this.percent = percent;
	}

	public boolean isOnetimeUsePerCustomer() {
		return onetimeUsePerCustomer;
	}

	public void setOnetimeUsePerCustomer(boolean onetimeUsePerCustomer) {
		this.onetimeUsePerCustomer = onetimeUsePerCustomer;
	}

	public Double getDiscount() {
		return discount;
	}

	public void setDiscount(Double discount) {
		this.discount = discount;
	}

	public Double getMinOrder() {
		return minOrder;
	}

	public void setMinOrder(Double minOrder) {
		this.minOrder = minOrder;
	}

	public Date getStartDate() {
		return startDate;
	}

	public void setStartDate(Date startDate) {
		this.startDate = startDate;
	}

	public Date getEndDate() {
		return endDate;
	}

	public void setEndDate(Date endDate) {
		this.endDate = endDate;
	}

	public void setRank(Integer rank) {
		this.rank = rank;
	}

	public Integer getRank() {
		return rank;
	}

	public void setHtmlCode(String htmlCode) {
		this.htmlCode = htmlCode;
	}

	public String getHtmlCode() {
		return htmlCode;
	}

	public Set<Object> getGroupIdSet() {
		return groupIdSet;
	}

	public void setGroupIdSet(Set<Object> groupIdSet) {
		this.groupIdSet = groupIdSet;
	}

	public String getGroupIds() {
		return groupIds;
	}

	public void setGroupIds(String groupIds) {
		this.groupIds = groupIds;
	}

	public String getGroupType() {
		return groupType;
	}

	public void setGroupType(String groupType) {
		this.groupType = groupType;
	}

	public void setDiscountType(String discountType) {
		this.discountType = discountType;
	}

	public String getDiscountType() {
		return discountType;
	}

	public void setStates(String states) {
		this.states = states;
	}

	public String getStates() {
		return states;
	}

	public void setStateType(String stateType) {
		this.stateType = stateType;
	}

	public String getStateType() {
		return stateType;
	}

	public String getSkus() {
		return skus;
	}

	public void setSkus(String skus) {
		this.skus = skus;
	}

	public void setSkuSet(Set<Object> skuSet) {
		this.skuSet = skuSet;
	}

	public Set<Object> getSkuSet() {
		return skuSet;
	}

	public String getGroupNote() {
		return groupNote;
	}

	public void setGroupNote(String groupNote) {
		this.groupNote = groupNote;
	}

	public void setBrands(String brands) {
		this.brands = brands;
	}

	public String getBrands() {
		return brands;
	}

	public void setBrandSet(Set<Object> brandSet) {
		this.brandSet = brandSet;
	}

	public Set<Object> getBrandSet() {
		return brandSet;
	}

	public Set<Object> getShippingIdSet() {
		return shippingIdSet;
	}

	public void setShippingIdSet(Set<Object> shippingIdSet) {
		this.shippingIdSet = shippingIdSet;
	}

	public void setProductAvailability(boolean productAvailability) {
		this.productAvailability = productAvailability;
	}

	public boolean isProductAvailability() {
		return productAvailability;
	}

	public void setMinOrderPerBrand(Double minOrderPerBrand) {
		this.minOrderPerBrand = minOrderPerBrand;
	}

	public Double getMinOrderPerBrand() {
		return minOrderPerBrand;
	}

	public void setOnetimeUseOnly(boolean onetimeUseOnly) {
		this.onetimeUseOnly = onetimeUseOnly;
	}

	public boolean isOnetimeUseOnly() {
		return onetimeUseOnly;
	}

	public String getGroups() {
		return groups;
	}

	public void setGroups(String groups) {
		this.groups = groups;
	}

	public Integer getMinQty() {
		return minQty;
	}

	public void setMinQty(Integer minQty) {
		this.minQty = minQty;
	}
	
	public String getParentSkus() {
		return parentSkus;
	}

	public void setParentSkus(String parentSkus) {
		this.parentSkus = parentSkus;
	}

	public Set<Object> getParentSkuSet() {
		return parentSkuSet;
	}

	public void setParentSkuSet(Set<Object> parentSkuSet) {
		this.parentSkuSet = parentSkuSet;
	}

	public boolean isDynamic() {
		return isDynamic;
	}

	public void setDynamic(boolean isDynamic) {
		this.isDynamic = isDynamic;
	}

	public Integer getUserId() {
		return userId;
	}

	public void setUserId(Integer userId) {
		this.userId = userId;
	}

	public String getPrefix() {
		return prefix;
	}

	public void setPrefix(String prefix) {
		this.prefix = prefix;
	}



	public Date getCreatedOn() {
		return createdOn;
	}

	public void setCreatedOn(Date createdOn) {
		this.createdOn = createdOn;
	}

	public String getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	public String getSku() {
		return sku;
	}

	public void setSku(String sku) {
		this.sku = sku;
	}

	public int getCount() {
		return count;
	}

	public void setCount(int count) {
		this.count = count;
	}

	public String getCategoryIds() {
		return categoryIds;
	}

	public void setCategoryIds(String categoryIds) {
		this.categoryIds = categoryIds;
	}

	public Set<Object> getCategoryIdSet() {
		return categoryIdSet;
	}

	public void setCategoryIdSet(Set<Object> categoryIdSet) {
		this.categoryIdSet = categoryIdSet;
	}

}
