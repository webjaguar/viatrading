package com.webjaguar.model;

public class ShippingObj {

	public ShippingObj() {}
	
	public ShippingObj(String title, Double charge, String message, String note) {
		this.title = title;
		this.totalCharges = totalCharges;
		this.message = message;
		this.note = note;
	}
	
	private String title;
	private Double totalCharges;
	private String message;
	private String note;
	
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
	public String getNote() {
		return note;
	}
	public void setNote(String note) {
		this.note = note;
	}
	public Double getTotalCharges() {
		return totalCharges;
	}
	public void setTotalCharges(Double totalCharges) {
		this.totalCharges = totalCharges;
	}

}
