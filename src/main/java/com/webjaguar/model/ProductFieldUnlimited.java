/* Copyright 2006 Advanced E-Media Solutions
 *
 */

package com.webjaguar.model;

import java.io.Serializable;

public class ProductFieldUnlimited implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private Integer id;
	private Integer rank;
	private String name;
	private String displayName;
	private String groupName;
	private String value;
	private boolean empty;
	private boolean enabled;
	private boolean showOnInvoice;
	private boolean showOnInvoiceBackend;
	private boolean showOnInvoiceExport;
	private boolean showOnMyList;
	private boolean showOnDropDown;
	private boolean packingField;
	private boolean search;
	private String fieldType;
	// protected
	private String protectedLevel = "0";
	private boolean comparisonField;
	private boolean detailsField;
	private boolean productExport;
	
	// filter
	private boolean indexForFilter;
	private boolean indexForSearch;
	private boolean rangeFilter;
	private boolean indexForPredictiveSearch;

	public ProductFieldUnlimited() {
		empty = true;
		enabled = false;
	}

	public ProductFieldUnlimited(int id, String value) {
		this.id = id;
		this.value = value;
	}

	public ProductFieldUnlimited(int id) {
		this.id = id;
	}

	public Integer getId() {
		return id;
	}

	public Integer getRank() {
		return rank;
	}

	public void setRank(Integer rank) {
		this.rank = rank;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}

	public boolean isEmpty() {
		return empty;
	}

	public void setEmpty(boolean empty) {
		this.empty = empty;
	}

	public boolean isEnabled() {
		return enabled;
	}

	public void setEnabled(boolean enabled) {
		this.enabled = enabled;
	}

	public String getProtectedLevel() {
		return protectedLevel;
	}

	public void setProtectedLevel(String protectedLevel) {
		this.protectedLevel = protectedLevel;
	}

	public boolean isComparisonField() {
		return comparisonField;
	}

	public void setComparisonField(boolean comparisonField) {
		this.comparisonField = comparisonField;
	}

	public boolean isShowOnInvoice() {
		return showOnInvoice;
	}

	public void setShowOnInvoice(boolean showOnInvoice) {
		this.showOnInvoice = showOnInvoice;
	}

	public boolean isDetailsField() {
		return detailsField;
	}

	public void setDetailsField(boolean detailsField) {
		this.detailsField = detailsField;
	}

	public String getMethodName() {
		return "getField" + this.id;
	}

	public boolean isSearch() {
		return search;
	}

	public void setSearch(boolean search) {
		this.search = search;
	}

	/*
	 * public String toString() { return "" + this.getName(); }
	 */

	public boolean isPackingField() {
		return packingField;
	}

	public void setPackingField(boolean packingField) {
		this.packingField = packingField;
	}

	public boolean isShowOnInvoiceBackend() {
		return showOnInvoiceBackend;
	}

	public void setShowOnInvoiceBackend(boolean showOnInvoiceBackend) {
		this.showOnInvoiceBackend = showOnInvoiceBackend;
	}

	public boolean isProductExport() {
		return productExport;
	}

	public void setProductExport(boolean productExport) {
		this.productExport = productExport;
	}

	public void setShowOnInvoiceExport(boolean showOnInvoiceExport) {
		this.showOnInvoiceExport = showOnInvoiceExport;
	}

	public boolean isShowOnInvoiceExport() {
		return showOnInvoiceExport;
	}

	public String getFieldType() {
		return fieldType;
	}

	public void setFieldType(String fieldType) {
		this.fieldType = fieldType;
	}

	public boolean isShowOnMyList() {
		return showOnMyList;
	}

	public void setShowOnMyList(boolean showOnMyList) {
		this.showOnMyList = showOnMyList;
	}

	public boolean isShowOnDropDown() {
		return showOnDropDown;
	}

	public void setShowOnDropDown(boolean showOnDropDown) {
		this.showOnDropDown = showOnDropDown;
	}
	
	public boolean isIndexForFilter() {
		return indexForFilter;
	}

	public void setIndexForFilter(boolean indexForFilter) {
		this.indexForFilter = indexForFilter;
	}

	public boolean isIndexForSearch() {
		return indexForSearch;
	}

	public void setIndexForSearch(boolean indexForSearch) {
		this.indexForSearch = indexForSearch;
	}
	
	public boolean isRangeFilter() {
		return rangeFilter;
	}

	public void setRangeFilter(boolean rangeFilter) {
		this.rangeFilter = rangeFilter;
	}

	public String getGroupName() {
		return groupName;
	}

	public void setGroupName(String groupName) {
		this.groupName = groupName;
	}

	public boolean isIndexForPredictiveSearch() {
		return indexForPredictiveSearch;
	}

	public void setIndexForPredictiveSearch(boolean indexForPredictiveSearch) {
		this.indexForPredictiveSearch = indexForPredictiveSearch;
	}

	public String getDisplayName() {
		return displayName;
	}

	public void setDisplayName(String displayName) {
		this.displayName = displayName;
	}
}