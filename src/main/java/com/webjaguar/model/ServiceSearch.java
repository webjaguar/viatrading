/* Copyright 2007 Advanced E-Media Solutions
 * @author Eduardo Asprec
 * @since 09.27.2007
 */

package com.webjaguar.model;

import java.util.ArrayList;
import java.util.List;

public class ServiceSearch {

	private String serviceNum;
	private List<Integer> userIdList;
	private int page = 1;
	private int pageSize = 10;
	// sort
	private String sort= "report_date DESC";
	private String status;
	private String itemId;
	private String itemIdLike;
	private String purchaseOrder;
	// company name
	private String companyName;
	private String workOrderType;
	
	private String trackNumIn;
	private String trackNumOut;
	private String itemFrom;
	private String itemTo;
	
	private String technician;
	
	private String wo3rd;
	private String poSupplier;
	private String refNum;
	private String manufWarranty;
	
	public String getServiceNum()
	{
		return serviceNum;
	}
	public void setServiceNum(String serviceNum)
	{
		this.serviceNum = serviceNum;
	}
	public int getPage()
	{
		return page;
	}
	public void setPage(int page)
	{
		this.page = page;
	}
	public int getPageSize()
	{
		return pageSize;
	}
	public void setPageSize(int pageSize)
	{
		this.pageSize = pageSize;
	}
	public String getSort()
	{
		return sort;
	}
	public void setSort(String sort)
	{
		this.sort = sort;
	}
	public List<Integer> getUserIdList()
	{
		return userIdList;
	}
	public void setUserIdList(List<Integer> userIdList)
	{
		this.userIdList = userIdList;
	}
	public void addUserId(Integer userId) 
	{
		if (this.userIdList == null) {
			this.userIdList = new ArrayList<Integer>(); 
		}
		this.userIdList.add( userId );
	}
	public String getStatus()
	{
		return status;
	}
	public void setStatus(String status)
	{
		this.status = status;
	}
	public String getItemId()
	{
		return itemId;
	}
	public void setItemId(String itemId)
	{
		this.itemId = itemId;
	}
	public String getItemIdLike() {
		return itemIdLike;
	}
	public void setItemIdLike(String itemIdLike) {
		this.itemIdLike = itemIdLike;
	}
	public String getPurchaseOrder()
	{
		return purchaseOrder;
	}
	public void setPurchaseOrder(String purchaseOrder)
	{
		this.purchaseOrder = purchaseOrder;
	}
	public String getCompanyName()
	{
		return companyName;
	}
	public void setCompanyName(String companyName)
	{
		this.companyName = companyName;
	}
	public String getWorkOrderType()
	{
		return workOrderType;
	}
	public void setWorkOrderType(String workOrderType)
	{
		this.workOrderType = workOrderType;
	}
	public String getTrackNumIn() {
		return trackNumIn;
	}
	public void setTrackNumIn(String trackNumIn) {
		this.trackNumIn = trackNumIn;
	}
	public String getTrackNumOut() {
		return trackNumOut;
	}
	public void setTrackNumOut(String trackNumOut) {
		this.trackNumOut = trackNumOut;
	}
	public String getItemFrom() {
		return itemFrom;
	}
	public void setItemFrom(String itemFrom) {
		this.itemFrom = itemFrom;
	}
	public String getItemTo() {
		return itemTo;
	}
	public void setItemTo(String itemTo) {
		this.itemTo = itemTo;
	}
	public String getTechnician() {
		return technician;
	}
	public void setTechnician(String technician) {
		this.technician = technician;
	}
	public String getWo3rd() {
		return wo3rd;
	}
	public void setWo3rd(String wo3rd) {
		this.wo3rd = wo3rd;
	}
	public String getPoSupplier() {
		return poSupplier;
	}
	public void setPoSupplier(String poSupplier) {
		this.poSupplier = poSupplier;
	}
	public String getRefNum() {
		return refNum;
	}
	public void setRefNum(String refNum) {
		this.refNum = refNum;
	}
	public String getManufWarranty() {
		return manufWarranty;
	}
	public void setManufWarranty(String manufWarranty) {
		this.manufWarranty = manufWarranty;
	}

}
