package com.webjaguar.model;

public class City
{
	private String country = "US";
	private String state;
	private String county;
	private String city;
	private String zip;
	private Double taxRate;
	
	// Constructors
	public City() {}
	public City(String state, String county) {
		this.state = state;
		this.county = county;
	}
	public City(String country, String state, String county, String city) {
		this.country = country;
		this.state = state;
		this.county = county;
		this.city = city;
	}
	
	public String getState()
	{
		return state;
	}
	public void setState(String state)
	{
		this.state = state;
	}
	public String getCounty()
	{
		return county;
	}
	public void setCounty(String county)
	{
		this.county = county;
	}
	public String getCity()
	{
		return city;
	}
	public void setCity(String city)
	{
		this.city = city;
	}
	public Double getTaxRate()
	{
		return taxRate;
	}
	public void setTaxRate(Double taxRate)
	{
		this.taxRate = taxRate;
	}
	public String getZip()
	{
		return zip;
	}
	public void setZip(String zip)
	{
		this.zip = zip;
	}
	public String getCountry()
	{
		return country;
	}
	public void setCountry(String country)
	{
		this.country = country;
	}
	
}
