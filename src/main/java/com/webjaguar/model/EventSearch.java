package com.webjaguar.model;

public class EventSearch {
	// sort
	private String sort = "start_date DESC";
	public String getSort() { return sort; }
	public void setSort(String sort) { this.sort = sort; }	
	
	// eventId
	private String eventId;
	public String getEventId() { return eventId; }
	public void setEventId(String eventId) { this.eventId = eventId; }

	// page 
	private int page = 1;
	public int getPage() { return page; }
	public void setPage(int page) { this.page = page; }
	
	// page size
	private int pageSize = 10;
	public int getPageSize() { return pageSize; }
	public void setPageSize(int pageSize) { this.pageSize = pageSize; }	
	
	private Integer limit;
	public Integer getLimit() { return limit; }
	public void setLimit(Integer limit) { this.limit = limit; }
	
	// offset
	private int offset;
	public int getOffset() { return offset; }
	public void setOffset(int offset) { this.offset = offset; }	
	
	// name
	private String eventName;
	public void setEventName(String eventName) { this.eventName = eventName; }
	public String getEventName() { return eventName; }

	// active
	private String active;
	public String getActive() { return active; }
	public void setActive(String active) { this.active = active; }
	
	// inEffect
	private String inEffect;
	public String getInEffect() { return inEffect; }
	public void setInEffect(String inEffect) { this.inEffect = inEffect; }
	
	// Card ID
	private String cardId;
	public String getCardId() { return cardId; }
	public void setCardId(String cardId) { this.cardId = cardId; }
	
	// first Name
	private String firstName;
	public String getFirstName() { return firstName; }
	public void setFirstName(String firstName) { this.firstName = firstName; }
	
	// last Name
	private String lastName;
	public String getLastName()	{ return lastName; }
	public void setLastName(String lastName) { this.lastName = lastName; }
	
	// email
	private String email;
	public String getEmail() {	return email; }
	public void setEmail(String email)	{ this.email = email;	}
	
	// sales rep
	private Integer salesRepId = -1;
	public Integer getSalesRepId() { return salesRepId; }
	public void setSalesRepId(Integer salesRepId) { this.salesRepId = salesRepId; }
}
