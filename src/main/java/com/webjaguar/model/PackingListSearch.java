/* Copyright 2008 Advanced E-Media Solutions
 * @author Shahin Naji
 * @since 02.13.2008
 */

package com.webjaguar.model;

import java.util.Date;
import java.util.List;

public class PackingListSearch {
	
	// sort
	private String sort = "created DESC";
	public String getSort() { return sort; }
	public void setSort(String sort) { this.sort = sort; }	
	
	// orderId
	private Integer orderId;
	public Integer getOrderId() { return orderId; }
	public void setOrderId(Integer orderId) { this.orderId = orderId; }
	
	// page 
	private int page = 1;
	public int getPage() { return page; }
	public void setPage(int page) { this.page = page; }
	
	// page size
	private int pageSize = 10;
	public int getPageSize() { return pageSize; }
	public void setPageSize(int pageSize) { this.pageSize = pageSize; }
	
	// shipped
	private Date shipped;
	public Date getShipped() { return shipped; }
	public void setShipped(Date shipped) { this.shipped = shipped; }
	
	// start date
	private Date startDate; 
	public Date getStartDate() { return startDate; }
	public void setStartDate(Date startDate) { this.startDate = startDate; }
	
	// end date
	private Date endDate;
	public Date getEndDate() { return endDate; }
	public void setEndDate(Date endDate) { this.endDate = endDate; }
	
	// host
	private String host = "";
	public String getHost() { return host; }
	public void setHost(String host) { this.host = host; }	
	
	// hosts
	private List<String> hosts;
	public List<String> getHosts() { return hosts; }
	public void setHost(List<String> hosts) { this.hosts = hosts; }	
	
	
	

}
