package com.webjaguar.model;

import java.util.Date;

public class TruckLoadProcessSearch {
	
	private String name;
	private String custom;
	private String id;
	private String status;
	private String statusTemp;
	private String sort = "date_created DESC";
	private int page = 1;
	private int pageSize = 10;
	private String originalSku;
	private String producedSku;
	private String custom1;
	private String custom2;
	private String custom3;
	private String custom4;
	private String pt;
	private Date startDate;
	private Date endDate;
	
	public Date getEndDate() { return endDate; }
	
	public void setEndDate(Date endDate) { this.endDate = endDate; }
	
	public Date getStartDate() { return startDate; }
	
	public void setStartDate(Date startDate) { this.startDate = startDate; }
	
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getCustom() {
		return custom;
	}
	public void setCustom(String custom) {
		this.custom = custom;
	}
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public int getPage() {
		return page;
	}
	public void setPage(int page) {
		this.page = page;
	}
	public int getPageSize() {
		return pageSize;
	}
	public void setPageSize(int pageSize) {
		this.pageSize = pageSize;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public String getSort() {
		return sort;
	}
	public void setSort(String sort) {
		this.sort = sort;
	}
	public String getOriginalSku() {
		return originalSku;
	}
	public void setOriginalSku(String originalSku) {
		this.originalSku = originalSku;
	}
	public String getProducedSku() {
		return producedSku;
	}
	public void setProducedSku(String producedSku) {
		this.producedSku = producedSku;
	}
	public String getCustom1() {
		return custom1;
	}
	public void setCustom1(String custom1) {
		this.custom1 = custom1;
	}
	public String getCustom2() {
		return custom2;
	}
	public void setCustom2(String custom2) {
		this.custom2 = custom2;
	}
	public String getCustom3() {
		return custom3;
	}
	public void setCustom3(String custom3) {
		this.custom3 = custom3;
	}
	public String getCustom4() {
		return custom4;
	}
	public void setCustom4(String custom4) {
		this.custom4 = custom4;
	}

	public String getStatusTemp() {
		return statusTemp;
	}

	public void setStatusTemp(String statusTemp) {
		this.statusTemp = statusTemp;
	}

	public String getPt() {
		return pt;
	}

	public void setPt(String pt) {
		this.pt = pt;
	}
}
