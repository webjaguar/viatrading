/* Copyright 2006 Advanced E-Media Solutions
 * @author Eduardo Asprec
 * @since 11.05.2006
 */

package com.webjaguar.model;

public class Layout {

	private int id;
	private String host = "";
	private String lang = "";
	private String name = "";
	private String type;
	// logout
	private String headerHtml;
	private String topBarHtml;
	private String leftBarTopHtml;
	private String leftBarBottomHtml;
	private String rightBarTopHtml;
	private String rightBarBottomHtml;
	private String footerHtml;
	private String aemFooter;
	// login
	private String headerHtmlLogin;
	private String topBarHtmlLogin;
	private String leftBarTopHtmlLogin;
	private String leftBarBottomHtmlLogin;
	private String rightBarTopHtmlLogin;
	private String rightBarBottomHtmlLogin;
	private String footerHtmlLogin;
	
	private boolean hideLeftBar;
	private String leftPaneWidth;
	
	// logout mobile
	private String headerHtmlMobile;
	private String topBarHtmlMobile;
	private String leftBarTopHtmlMobile;
	private String leftBarBottomHtmlMobile;
	private String rightBarTopHtmlMobile;
	private String rightBarBottomHtmlMobile;
	private String footerHtmlMobile;
	private String aemFooterMobile;
	// login mobile
	private String headerHtmlLoginMobile;
	private String topBarHtmlLoginMobile;
	private String leftBarTopHtmlLoginMobile;
	private String leftBarBottomHtmlLoginMobile;
	private String rightBarTopHtmlLoginMobile;
	private String rightBarBottomHtmlLoginMobile;
	private String footerHtmlLoginMobile;	
		
	// meta Tag
	private String headTag;
	// meta Tag mobile
	private String headTagMobile;
	// mobile hidden content
	private boolean hideContent;
	
	private boolean showFullWidth;
	
	
	public Layout() {}
	
	public Layout(int id) {
		this.id = id;
	}

	public Layout(int id, String host, String lang) {
		this.id = id;
		this.host = host;
		this.lang = lang;
	}	
	
	public Layout(String type, String host, String lang) {
		this.type = type;
		this.host = host;
		this.lang = lang;
	}
	
	public String getHeadTag()
	{
		return headTag;
	}

	public void setHeadTag(String headTag)
	{
		this.headTag = headTag;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}
	
	public String getHeaderHtml() {
		return headerHtml;
	}

	public void setHeaderHtml(String headerHtml) {
		this.headerHtml = headerHtml;
	}

	public String getTopBarHtml() {
		return topBarHtml;
	}

	public void setTopBarHtml(String topBarHtml) {
		this.topBarHtml = topBarHtml;
	}

	public String getFooterHtml() {
		return footerHtml;
	}

	public void setFooterHtml(String footerHtml) {
		this.footerHtml = footerHtml;
	}

	public String getLeftBarBottomHtml() {
		return leftBarBottomHtml;
	}

	public void setLeftBarBottomHtml(String leftBarBottomHtml) {
		this.leftBarBottomHtml = leftBarBottomHtml;
	}

	public String getLeftBarTopHtml() {
		return leftBarTopHtml;
	}

	public void setLeftBarTopHtml(String leftBarTopHtml) {
		this.leftBarTopHtml = leftBarTopHtml;
	}

	public String getRightBarBottomHtml() {
		return rightBarBottomHtml;
	}

	public void setRightBarBottomHtml(String rightBarBottomHtml) {
		this.rightBarBottomHtml = rightBarBottomHtml;
	}

	public String getRightBarTopHtml() {
		return rightBarTopHtml;
	}

	public void setRightBarTopHtml(String rightBarTopHtml) {
		this.rightBarTopHtml = rightBarTopHtml;
	}
	
	public void replace(String target, String replacement) {
		if (replacement != null) {
			if (this.headerHtml != null) this.headerHtml = this.headerHtml.replace(target, replacement);				
			if (this.topBarHtml != null) this.topBarHtml = this.topBarHtml.replace(target, replacement);
			if (this.rightBarTopHtml != null) this.rightBarTopHtml = this.rightBarTopHtml.replace(target, replacement);
			if (this.rightBarBottomHtml != null) this.rightBarBottomHtml = this.rightBarBottomHtml.replace(target, replacement);
			if (this.leftBarTopHtml != null) this.leftBarTopHtml = this.leftBarTopHtml.replace(target, replacement);
			if (this.leftBarBottomHtml != null) this.leftBarBottomHtml = this.leftBarBottomHtml.replace(target, replacement);
			if (this.footerHtml != null) this.footerHtml = this.footerHtml.replace(target, replacement);
			if (this.headTag != null) this.headTag = this.headTag.replace(target, replacement);			
		}
	}

	public boolean isHideLeftBar() {
		return hideLeftBar;
	}

	public void setHideLeftBar(boolean hideLeftBar) {
		this.hideLeftBar = hideLeftBar;
	}

	public String getLeftPaneWidth() {
		return leftPaneWidth;
	}

	public void setLeftPaneWidth(String leftPaneWidth) {
		this.leftPaneWidth = leftPaneWidth;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getFooterHtmlLogin()
	{
		return footerHtmlLogin;
	}

	public void setFooterHtmlLogin(String footerHtmlLogin)
	{
		this.footerHtmlLogin = footerHtmlLogin;
	}

	public String getHeaderHtmlLogin()
	{
		return headerHtmlLogin;
	}

	public void setHeaderHtmlLogin(String headerHtmlLogin)
	{
		this.headerHtmlLogin = headerHtmlLogin;
	}

	public String getLeftBarBottomHtmlLogin()
	{
		return leftBarBottomHtmlLogin;
	}

	public void setLeftBarBottomHtmlLogin(String leftBarBottomHtmlLogin)
	{
		this.leftBarBottomHtmlLogin = leftBarBottomHtmlLogin;
	}

	public String getLeftBarTopHtmlLogin()
	{
		return leftBarTopHtmlLogin;
	}

	public void setLeftBarTopHtmlLogin(String leftBarTopHtmlLogin)
	{
		this.leftBarTopHtmlLogin = leftBarTopHtmlLogin;
	}

	public String getRightBarBottomHtmlLogin()
	{
		return rightBarBottomHtmlLogin;
	}

	public void setRightBarBottomHtmlLogin(String rightBarBottomHtmlLogin)
	{
		this.rightBarBottomHtmlLogin = rightBarBottomHtmlLogin;
	}

	public String getRightBarTopHtmlLogin()
	{
		return rightBarTopHtmlLogin;
	}

	public void setRightBarTopHtmlLogin(String rightBarTopHtmlLogin)
	{
		this.rightBarTopHtmlLogin = rightBarTopHtmlLogin;
	}

	public String getTopBarHtmlLogin()
	{
		return topBarHtmlLogin;
	}

	public void setTopBarHtmlLogin(String topBarHtmlLogin)
	{
		this.topBarHtmlLogin = topBarHtmlLogin;
	}
	
	public void initLayout(boolean isMobile)
	{
		if ( this.getHeaderHtmlLogin() != null && !this.getHeaderHtmlLogin().equals( "" ) )
			this.setHeaderHtml( this.getHeaderHtmlLogin() );
		if ( this.getTopBarHtmlLogin() != null && !this.getTopBarHtmlLogin().equals( "" ) )
			this.setTopBarHtml( this.getTopBarHtmlLogin() );
		if ( this.getLeftBarTopHtmlLogin() != null && !this.getLeftBarTopHtmlLogin().equals( "" ) )
			this.setLeftBarTopHtml( this.getLeftBarTopHtmlLogin() );
		if ( this.getLeftBarBottomHtmlLogin() != null && !this.getLeftBarBottomHtmlLogin().equals( "" ) )
			this.setLeftBarBottomHtml( this.getLeftBarBottomHtmlLogin() );
		if ( this.getRightBarTopHtmlLogin() != null && !this.getRightBarTopHtmlLogin().equals( "" ) )
			this.setRightBarTopHtml( this.getRightBarTopHtmlLogin() );
		if ( this.getRightBarBottomHtmlLogin() != null && !this.getRightBarBottomHtmlLogin().equals( "" ) )
			this.setRightBarBottomHtml( this.getRightBarBottomHtmlLogin() );
		if ( this.getFooterHtmlLogin() != null && !this.getFooterHtmlLogin().equals( "" ) )
			this.setFooterHtml( this.getFooterHtmlLogin() );
		
		// mobile
		if(isMobile) {
			if (this.getHeaderHtmlLoginMobile() != null && !this.getHeaderHtmlLoginMobile().isEmpty())
				this.setHeaderHtml( this.getHeaderHtmlLoginMobile() );
			if (this.getTopBarHtmlLoginMobile() != null && !this.getTopBarHtmlLoginMobile().isEmpty())
				this.setTopBarHtml( this.getTopBarHtmlLoginMobile() );
			if (this.getLeftBarTopHtmlLoginMobile() != null && !this.getLeftBarTopHtmlLoginMobile().isEmpty())
				this.setTopBarHtml( this.getLeftBarTopHtmlLoginMobile() );
			if (this.getLeftBarBottomHtmlLoginMobile() != null && !this.getLeftBarBottomHtmlLoginMobile().isEmpty())
				this.setLeftBarBottomHtml( this.getLeftBarBottomHtmlLoginMobile() );
			if (this.getRightBarTopHtmlLoginMobile() != null && !this.getRightBarTopHtmlLoginMobile().isEmpty())
				this.setRightBarTopHtml( this.getRightBarTopHtmlLoginMobile() );
			if (this.getRightBarBottomHtmlLoginMobile() != null && !this.getRightBarBottomHtmlLoginMobile().isEmpty())
				this.setRightBarBottomHtml( this.getRightBarBottomHtmlLoginMobile() );
			if (this.getFooterHtmlLoginMobile() != null && !this.getFooterHtmlLoginMobile().isEmpty())
				this.setFooterHtml( this.getFooterHtmlLoginMobile() );
		}
				
	}

	public String getType()
	{
		return type;
	}

	public void setType(String type)
	{
		this.type = type;
	}

	public String getHost()
	{
		return host;
	}

	public void setHost(String host)
	{
		this.host = host;
	}

	public String getLang()
	{
		return lang;
	}

	public void setLang(String lang)
	{
		this.lang = lang;
	}

	public String getAemFooter() {
		return aemFooter;
	}

	public void setAemFooter(String aemFooter) {
		this.aemFooter = aemFooter;
	}

	public String getHeaderHtmlMobile() {
		return headerHtmlMobile;
	}

	public void setHeaderHtmlMobile(String headerHtmlMobile) {
		this.headerHtmlMobile = headerHtmlMobile;
	}

	public String getTopBarHtmlMobile() {
		return topBarHtmlMobile;
	}

	public void setTopBarHtmlMobile(String topBarHtmlMobile) {
		this.topBarHtmlMobile = topBarHtmlMobile;
	}

	public String getLeftBarTopHtmlMobile() {
		return leftBarTopHtmlMobile;
	}

	public void setLeftBarTopHtmlMobile(String leftBarTopHtmlMobile) {
		this.leftBarTopHtmlMobile = leftBarTopHtmlMobile;
	}

	public String getLeftBarBottomHtmlMobile() {
		return leftBarBottomHtmlMobile;
	}

	public void setLeftBarBottomHtmlMobile(String leftBarBottomHtmlMobile) {
		this.leftBarBottomHtmlMobile = leftBarBottomHtmlMobile;
	}

	public String getRightBarTopHtmlMobile() {
		return rightBarTopHtmlMobile;
	}

	public void setRightBarTopHtmlMobile(String rightBarTopHtmlMobile) {
		this.rightBarTopHtmlMobile = rightBarTopHtmlMobile;
	}

	public String getRightBarBottomHtmlMobile() {
		return rightBarBottomHtmlMobile;
	}

	public void setRightBarBottomHtmlMobile(String rightBarBottomHtmlMobile) {
		this.rightBarBottomHtmlMobile = rightBarBottomHtmlMobile;
	}

	public String getFooterHtmlMobile() {
		return footerHtmlMobile;
	}

	public void setFooterHtmlMobile(String footerHtmlMobile) {
		this.footerHtmlMobile = footerHtmlMobile;
	}

	public String getAemFooterMobile() {
		return aemFooterMobile;
	}

	public void setAemFooterMobile(String aemFooterMobile) {
		this.aemFooterMobile = aemFooterMobile;
	}

	public String getHeaderHtmlLoginMobile() {
		return headerHtmlLoginMobile;
	}

	public void setHeaderHtmlLoginMobile(String headerHtmlLoginMobile) {
		this.headerHtmlLoginMobile = headerHtmlLoginMobile;
	}

	public String getTopBarHtmlLoginMobile() {
		return topBarHtmlLoginMobile;
	}

	public void setTopBarHtmlLoginMobile(String topBarHtmlLoginMobile) {
		this.topBarHtmlLoginMobile = topBarHtmlLoginMobile;
	}

	public String getLeftBarTopHtmlLoginMobile() {
		return leftBarTopHtmlLoginMobile;
	}

	public void setLeftBarTopHtmlLoginMobile(String leftBarTopHtmlLoginMobile) {
		this.leftBarTopHtmlLoginMobile = leftBarTopHtmlLoginMobile;
	}

	public String getLeftBarBottomHtmlLoginMobile() {
		return leftBarBottomHtmlLoginMobile;
	}

	public void setLeftBarBottomHtmlLoginMobile(String leftBarBottomHtmlLoginMobile) {
		this.leftBarBottomHtmlLoginMobile = leftBarBottomHtmlLoginMobile;
	}

	public String getRightBarTopHtmlLoginMobile() {
		return rightBarTopHtmlLoginMobile;
	}

	public void setRightBarTopHtmlLoginMobile(String rightBarTopHtmlLoginMobile) {
		this.rightBarTopHtmlLoginMobile = rightBarTopHtmlLoginMobile;
	}

	public String getRightBarBottomHtmlLoginMobile() {
		return rightBarBottomHtmlLoginMobile;
	}

	public void setRightBarBottomHtmlLoginMobile(String rightBarBottomHtmlLoginMobile) {
		this.rightBarBottomHtmlLoginMobile = rightBarBottomHtmlLoginMobile;
	}

	public String getFooterHtmlLoginMobile() {
		return footerHtmlLoginMobile;
	}

	public void setFooterHtmlLoginMobile(String footerHtmlLoginMobile) {
		this.footerHtmlLoginMobile = footerHtmlLoginMobile;
	}

	public String getHeadTagMobile() {
		return headTagMobile;
	}

	public void setHeadTagMobile(String headTagMobile) {
		this.headTagMobile = headTagMobile;
	}

	public boolean isHideContent() {
		return hideContent;
	}

	public void setHideContent(boolean hideContent) {
		this.hideContent = hideContent;
	}
	
	public boolean isShowFullWidth() {
		return showFullWidth;
	}

	public void setShowFullWidth(boolean showFullWidth) {
		this.showFullWidth = showFullWidth;
	}

}
