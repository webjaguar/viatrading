/* Copyright 2006 Advanced E-Media Solutions
 * @author Rewati Raman
 * @since 09.26.2006
 */

package com.webjaguar.model;

import java.io.Serializable;

public class KitParts implements Serializable{
	
	private static final long serialVersionUID = 1L;
	private String kitSku;
	private String kitPartsSku;
	private Integer quantity;
	private boolean temp = true;
	
	public String getKitSku() {
		return kitSku;
	}
	public void setKitSku(String kitSku) {
		this.kitSku = kitSku;
	}
	public String getKitPartsSku() {
		return kitPartsSku;
	}
	public void setKitPartsSku(String kitPartsSku) {
		this.kitPartsSku = kitPartsSku;
	}
	public Integer getQuantity() {
		return quantity;
	}
	public void setQuantity(Integer quantity) {
		this.quantity = quantity;
	}
	public boolean isTemp() {
		return temp;
	}
	public void setTemp(boolean temp) {
		this.temp = temp;
	}
}
