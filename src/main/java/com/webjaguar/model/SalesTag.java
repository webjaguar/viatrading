/*
 * Copyright 2006 Advanced E-Media Solutions @author Shahin Naji
 * 
 * @since 01.26.2007
 */

package com.webjaguar.model;

import java.io.Serializable;
import java.util.Calendar;
import java.util.Date;

public class SalesTag implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private Integer tagId;
	private String title;
	private Double discount;
	private boolean percent = true;
	private Date startDate;
	private Date endDate;
	private boolean image;

	public boolean isImage()
	{
		return image;
	}

	public void setImage(boolean image)
	{
		this.image = image;
	}

	public boolean isInEffect()
	{
		// today's date
		Calendar today = Calendar.getInstance();
		int start = this.startDate.compareTo( today.getTime() );
		int end = this.endDate.compareTo( today.getTime() );

		if ( start == 0 )
			return true;
		else if ( start > 0 || end < 0 )
			return false;
		else
			return true;
	}

	public Integer getTagId()
	{
		return tagId;
	}

	public void setTagId(Integer tagId)
	{
		this.tagId = tagId;
	}

	public String getTitle()
	{
		return title;
	}

	public void setTitle(String title)
	{
		this.title = title;
	}

	public Double getDiscount()
	{
		return discount;
	}

	public void setDiscount(Double discount)
	{
		this.discount = discount;
	}

	public boolean isPercent()
	{
		return percent;
	}

	public void setPercent(boolean percent)
	{
		this.percent = percent;
	}

	public Date getStartDate()
	{
		return startDate;
	}

	public void setStartDate(Date startDate)
	{
		this.startDate = startDate;
	}

	public Date getEndDate()
	{
		return endDate;
	}

	public void setEndDate(Date endDate)
	{
		this.endDate = endDate;
	}
}
