/* Copyright 2007 Advanced E-Media Solutions
 * @author Eduardo Asprec
 * @since 07.12.2007
 */

package com.webjaguar.model;

import java.io.Serializable;

public class CustomerField implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private int id;
	private String name;
	private String nameEs;
	private String value;
	private boolean enabled;
	private boolean required;
	private boolean publicField;
	private boolean showOnInvoice;
	private boolean showOnInvoiceExport;
	private boolean multiSelecOnAdmin;
	private String preValue;
	
	public int getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}	
	
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}

	public boolean isEnabled()
	{
		return enabled;
	}

	public void setEnabled(boolean enabled)
	{
		this.enabled = enabled;
	}

	public boolean isPublicField()
	{
		return publicField;
	}

	public void setPublicField(boolean publicField)
	{
		this.publicField = publicField;
	}

	public boolean isShowOnInvoice()
	{
		return showOnInvoice;
	}

	public void setShowOnInvoice(boolean showOnInvoice)
	{
		this.showOnInvoice = showOnInvoice;
	}

	public String getPreValue()
	{
		return preValue;
	}

	public void setPreValue(String preValue)
	{
		this.preValue = preValue;
	}

	public boolean isRequired()
	{
		return required;
	}

	public void setRequired(boolean required)
	{
		this.required = required;
	}

	public void setId(int id)
	{
		this.id = id;
	}

	public void setShowOnInvoiceExport(boolean showOnInvoiceExport) {
		this.showOnInvoiceExport = showOnInvoiceExport;
	}

	public boolean isShowOnInvoiceExport() {
		return showOnInvoiceExport;
	}

	public String getNameEs() {
		return nameEs;
	}

	public void setNameEs(String nameEs) {
		this.nameEs = nameEs;
	}

	public boolean isMultiSelecOnAdmin() {
		return multiSelecOnAdmin;
	}

	public void setMultiSelecOnAdmin(boolean multiSelecOnAdmin) {
		this.multiSelecOnAdmin = multiSelecOnAdmin;
	}
	
}
