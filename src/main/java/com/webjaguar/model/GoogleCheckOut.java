package com.webjaguar.model;

import java.util.Date;

public class GoogleCheckOut {
	
	private Integer orderId;
	private Long googleOrderId;
	private Date googlePaymentDate; 

	public void setOrderId(Integer orderId) {
		this.orderId = orderId;
	}

	public Integer getOrderId() {
		return orderId;
	}

	public void setGoogleOrderId(Long googleOrderId) {
		this.googleOrderId = googleOrderId;
	}

	public Long getGoogleOrderId() {
		return googleOrderId;
	}

	public void setGooglePaymentDate(Date googlePaymentDate) {
		this.googlePaymentDate = googlePaymentDate;
	}

	public Date getGooglePaymentDate() {
		return googlePaymentDate;
	}
	
}
