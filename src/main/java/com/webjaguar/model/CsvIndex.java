package com.webjaguar.model;

public class CsvIndex {
	
	private String type;
	private Integer index;
	private String value;
	private String columnName;
	private String headerName;
	
	public CsvIndex(String headerName, String columnName, String type, Integer index){
		this.type = type;
		this.index = index;
		this.columnName = columnName;
		this.headerName = headerName;
	}
	
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	public Integer getIndex() {
		return index;
	}
	public void setIndex(Integer index) {
		this.index = index;
	}
	public String getValue() {
		return value;
	}
	public void setValue(String value) {
		this.value = value;
	}
	public String getColumnName() {
		return columnName;
	}
	public void setColumnName(String columnName) {
		this.columnName = columnName;
	}
	public String getHeaderName() {
		return headerName;
	}
	public void setHeaderName(String headerName) {
		this.headerName = headerName;
	}
	
}
