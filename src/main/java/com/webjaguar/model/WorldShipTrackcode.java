/* Copyright 2008 Advanced E-Media Solutions
 * @author Shahin Naji
 * @since 12.03.2008
 */

package com.webjaguar.model;

public class WorldShipTrackcode {
	
	private String orderId;
	private String trackNum;
	private String trackNumVoid;

	public String getOrderId()
	{
		return orderId;
	}
	public void setOrderId(String orderId)
	{
		this.orderId = orderId;
	}
	public String getTrackNum()
	{
		return trackNum;
	}
	public void setTrackNum(String trackNum)
	{
		this.trackNum = trackNum;
	}
	public String getTrackNumVoid()
	{
		return trackNumVoid;
	}
	public void setTrackNumVoid(String trackNumVoid)
	{
		this.trackNumVoid = trackNumVoid;
	}
}
