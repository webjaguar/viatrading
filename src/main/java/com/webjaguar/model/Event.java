package com.webjaguar.model;

import java.util.Date;

import com.webjaguar.web.domain.Constants;

public class Event {

	private Integer id;
	private String eventName;
	private Date startDate;
	private Date endDate;
	private Integer registered;
	private Boolean active;
	private Boolean activeUserOnly;
	private String htmlCode;
	private String htmlCodeEs;
	
	public boolean isInEffect() {
		return Constants.isInEffect(this.startDate, this.endDate);
	}
	
	public void setId(Integer id) {
		this.id = id;
	}
	public Integer getId() {
		return id;
	}
	public String getEventName() {
		return eventName;
	}
	public void setEventName(String eventName) {
		this.eventName = eventName;
	}
	public Date getStartDate() {
		return startDate;
	}
	public void setStartDate(Date startDate) {
		this.startDate = startDate;
	}
	public Date getEndDate() {
		return endDate;
	}
	public void setEndDate(Date endDate) {
		this.endDate = endDate;
	}
	public Integer getRegistered() {
		return registered;
	}
	public void setRegistered(Integer registered) {
		this.registered = registered;
	}
	public Boolean getActive() {
		return active;
	}
	public void setActive(Boolean active) {
		this.active = active;
	}
	public Boolean getActiveUserOnly() {
		return activeUserOnly;
	}
	public void setActiveUserOnly(Boolean activeUserOnly) {
		this.activeUserOnly = activeUserOnly;
	}
	public String getHtmlCode() {
		return htmlCode;
	}
	public void setHtmlCode(String htmlCode) {
		this.htmlCode = htmlCode;
	}

	public String getHtmlCodeEs() {
		return htmlCodeEs;
	}

	public void setHtmlCodeEs(String htmlCodeEs) {
		this.htmlCodeEs = htmlCodeEs;
	}
	
}
