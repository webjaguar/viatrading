/* Copyright 2009 Advanced E-Media Solutions
 * @author Eduardo Asprec
 * @since 12.09.2009
 */

package com.webjaguar.model;

import java.util.Date;

public class BudgetProduct {

	private Integer id;
	private Integer userId;
	private Integer qty;
	private Integer qtyTotal;
	private Integer ordered;	
	private Date entered;
	private Product product;
	
	public BudgetProduct() {}

	public BudgetProduct(String sku, int qty) {
		this.product = new Product(sku, null);
		this.qty = qty;
	}

	public BudgetProduct(Product product, Integer ordered) {
		this.product = product;
		this.ordered = ordered;
	}
	
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public Integer getUserId() {
		return userId;
	}
	public void setUserId(Integer userId) {
		this.userId = userId;
	}
	public Integer getQty() {
		return qty;
	}
	public void setQty(Integer qty) {
		this.qty = qty;
	}
	public Integer getQtyTotal() {
		return qtyTotal;
	}
	public void setQtyTotal(Integer qtyTotal) {
		this.qtyTotal = qtyTotal;
	}
	public Integer getOrdered() {
		return ordered;
	}
	public void setOrdered(Integer ordered) {
		this.ordered = ordered;
	}
	public Date getEntered() {
		return entered;
	}
	public void setEntered(Date entered) {
		this.entered = entered;
	}
	public Product getProduct() {
		return product;
	}
	public void setProduct(Product product) {
		this.product = product;
	}
	public int getBalance() {
		int balance = 0;
		if (qtyTotal != null) {
			balance = qtyTotal;
		}		
		if (ordered != null) {
			balance = balance - ordered;
		}
		if (balance < 0) {
			balance = 0;
		}
		
		return balance;
	}
	public boolean addQty(int qty) {
		if (this.qty == null) {
			this.qty = qty;
		} else {
			this.qty += qty;			
		}
		return (getBalance() - this.qty < 0);
	}
	public void addOrdered(Integer ordered) {
		if (ordered != null) {
			if (this.ordered == null) {
				this.ordered = ordered;
			} else {
				this.ordered += ordered;			
			}			
		}
	}
}
