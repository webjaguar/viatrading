/* Copyright 2005, 2006 Advanced E-Media Solutions
 *
 */

package com.webjaguar.model;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.BitSet;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import com.webjaguar.model.Language.LanguageCode;

public class Customer implements Serializable {

	private static final long serialVersionUID = 1L;

	private String username;
	private String username2;
	private String password;
	private boolean unsubscribe;
	private Boolean usernameDisabled;
	private boolean registerCellPhone;
	private String unsubscribeReason;
	private boolean unsubscribeTextMessage;
	private String extraEmail;
	private Integer id;
	private String host;
	private Double credit;
	private Double loyaltyPoint;
	private Integer pointThreshold;
	private Double pointValue;
	private Integer parent;
	private boolean hideSubAccts;
	private Integer subCount;
	private Address address;
	private Timestamp created;
	private Timestamp lastModified;
	private Timestamp lastLogin;
	private Timestamp lastPasswordReset;
	private Integer passwordValidity;
	private Date lastOrderDate;
	private Integer numOfLogins;
	private Integer priceTable = 0;
	private String payment;
	private String taxId; // not unique
	private String note;
	private String accountNumber; // not unique
	private String cardId;
	private Integer cardIdCount;
	private boolean keepMeLogin;
	private String gatewayToken;
	private Map<Integer, String> gatewayPaymentMethods;
	private String budgetPlan;
	private boolean autoGenerateAccountNumber;
	private String websiteUrl;
	private String ebayName;
	private String amazonName;
	private boolean updateNewInformation;
	private BrokerImage brokerImage;
	private boolean removeBrokerImage;
	private Integer customerType;
	private String paid;
	private String language;
	private String medium;
	private String mainSource;
	private String referrer;
	private String queryString;
	private boolean doNotCall;
	private Integer accessUserId;
	
	private String crmId;
	private String userId;
	
	private String customFieldValue;
	
	public String getCustomFieldValue() {
		return customFieldValue;
	}

	public void setCustomFieldValue(String customFieldValue) {
		this.customFieldValue = customFieldValue;
	}

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public String getCrmId() {
		return crmId;
	}

	public void setCrmId(String crmId) {
		this.crmId = crmId;
	}

	private Integer qualifier;
	/*
	 * Plan-A: Apply MDF on Sub-Total Plan-B: Apply MDF on (Sub-Total - Earned Credits)
	 */
	private List<CustomerBudgetPartner> budgetPartnersList;

	public Boolean getUsernameDisabled() {
		return usernameDisabled;
	}

	public void setUsernameDisabled(Boolean usernameDisabled) {
		this.usernameDisabled = usernameDisabled;
	}

	// budget (customers can not apply more that this(creditAllowed) credit)
	private Double creditAllowed;

	// payment alert (User can not process the order once the customers exceed this(paymentAlert) limit.)
	private Double paymentAlert;

	// protected
	private String protectedAccess = "0";
	private String trackcode;
	private boolean seeHiddenPrice;
	private boolean imported;
	private String shippingTitle;
	// affiliate
	private Boolean isAffiliate = false;
	private Integer affiliateParent;
	private Affiliate affiliate;
	private Integer affiliateNumChild;
	private Double commissionTotal;
	private Integer affiliateNumOrder;
	private boolean suspended;
	private boolean suspendedEvent;
	private String customNote1;
	private String customNote2;
	private String customNote3;
	private boolean emailNotify = true;
	private boolean textMessageNotify = false;
	private boolean whatsappNotify = false;
	private String ipAddress;
	private String poRequired;

	private String token;
	private String loginSuccessURL;
	private Integer salesRepId;

	// orders
	private Integer orderCount;
	private Double ordersGrandTotal;

	// quotes
	private Integer quoteCount;

	private String registeredBy;

	// supplier
	private boolean convertToSupplier;
	private Integer supplierId;
	private String supplierPrefix;
	private Integer supplierProdLimit;
	
	private boolean havingWalkIn;
	private boolean payingWalkIn;
	private boolean holdLoads;

	public boolean isHavingWalkIn() {
		return havingWalkIn;
	}

	public void setHavingWalkIn(boolean havingWalkIn) {
		this.havingWalkIn = havingWalkIn;
	}

	public boolean isPayingWalkIn() {
		return payingWalkIn;
	}

	public void setPayingWalkIn(boolean payingWalkIn) {
		this.payingWalkIn = payingWalkIn;
	}

	public boolean isHoldLoads() {
		return holdLoads;
	}

	public void setHoldLoads(boolean holdLoads) {
		this.holdLoads = holdLoads;
	}

	// extra fields
	private String field1;
	private String field2;
	private String field3;
	private String field4;
	private String field5;
	private String field6;
	private String field7;
	private String field8;
	private String field9;
	private String field10;
	private String field11;
	private String field12;
	private String field13;
	private String field14;
	private String field15;
	private String field16;
	private String field17;
	private String field18;
	private String field19;
	private String field20;
	
	private String field21;
	private String field22;
	private String field23;
	private String field24;
	private String field25;
	private String field26;
	private String field27;
	private String field28;
	private String field29;
	private String field30;
	private List<CustomerField> customerFields;
	
	// field value set
	private Set<String> field1Set;
	private Set<String> field2Set;
	private Set<String> field3Set;
	private Set<String> field4Set;
	private Set<String> field5Set;
	private Set<String> field6Set;
	private Set<String> field7Set;
	private Set<String> field8Set;
	private Set<String> field9Set;
	private Set<String> field10Set;
	private Set<String> field11Set;
	private Set<String> field12Set;
	private Set<String> field13Set;
	private Set<String> field14Set;
	private Set<String> field15Set;
	private Set<String> field16Set;
	private Set<String> field17Set;
	private Set<String> field18Set;
	private Set<String> field19Set;
	private Set<String> field20Set;

	private Set<String> field21Set;
	private Set<String> field22Set;
	private Set<String> field23Set;
	private Set<String> field24Set;
	private Set<String> field25Set;
	private Set<String> field26Set;
	private Set<String> field27Set;
	private Set<String> field28Set;
	private Set<String> field29Set;
	private Set<String> field30Set;
	
	
	// category ids
	private Set<Object> catIds;
	// Review
	private Review rate;

	// description
	private String shortDesc;
	private String longDesc;

	private String brandReport;

	// shopping cart
	private Integer numOfCartItems;

	// crm
	private Integer crmContactId;
	private Integer crmAccountId;

	// language
	private LanguageCode languageCode = LanguageCode.en;

	// admin to customer
	private Double supplierPay;

	// ratings
	private String rating1;
	private String rating2;
	// Prospect Id (SearchEngine Prospect)
	private String prospectId;
	
	//Dialing note
	private DialingNote newDialingNote;
	private List<DialingNote> dialingNoteHistory;
	private String lastDialingNote;
	
	private Boolean cellPhoneReg;
	
	private String cellPhone;

	public String getPoRequired() {
		return poRequired;
	}

	public void setPoRequired(String poRequired) {
		this.poRequired = poRequired;
	}

	public String getIpAddress() {
		return ipAddress;
	}

	public void setIpAddress(String ipAddress) {
		this.ipAddress = ipAddress;
	}

	public Integer getAffiliateParent() {
		return affiliateParent;
	}

	public void setAffiliateParent(Integer affiliateParent) {
		this.affiliateParent = affiliateParent;
	}

	public Integer getSalesRepId() {
		return salesRepId;
	}

	public void setSalesRepId(Integer salesRepId) {
		this.salesRepId = salesRepId;
	}

	public String getLoginSuccessURL() {
		return loginSuccessURL;
	}

	public void setLoginSuccessURL(String loginSuccessURL) {
		this.loginSuccessURL = loginSuccessURL;
	}

	public String getToken() {
		return token;
	}

	public void setToken(String token) {
		this.token = token;
	}

	public boolean isSeeHiddenPrice() {
		return seeHiddenPrice;
	}

	public void setSeeHiddenPrice(boolean seeHiddenPrice) {
		this.seeHiddenPrice = seeHiddenPrice;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Address getAddress() {
		return address;
	}

	public void setAddress(Address address) {
		this.address = address;
	}

	public Timestamp getCreated() {
		return created;
	}

	public void setCreated(Timestamp created) {
		this.created = created;
	}

	public Integer getNumOfLogins() {
		return numOfLogins;
	}

	public void setNumOfLogins(Integer numOfLogins) {
		this.numOfLogins = numOfLogins;
	}

	public Timestamp getLastLogin() {
		return lastLogin;
	}

	public void setLastLogin(Timestamp lastLogin) {
		this.lastLogin = lastLogin;
	}

	public void setLastPasswordReset(Timestamp lastPasswordReset) {
		this.lastPasswordReset = lastPasswordReset;
	}

	public Timestamp getLastPasswordReset() {
		return lastPasswordReset;
	}

	public void setPasswordValidity(Integer passwordValidity) {
		this.passwordValidity = passwordValidity;
	}

	public Integer getPasswordValidity() {
		return passwordValidity;
	}

	public boolean hasExpiredPassword() {
		// password never expires
		if (getPasswordValidity() == null || getLastPasswordReset() == null || getPasswordValidity() == -1) {
			return false;
		}

		Long validDuration = Long.parseLong(getPasswordValidity().toString()) * 24 * 60 * 60 * 1000;
		Calendar calendar = GregorianCalendar.getInstance();
		calendar.setTime(getLastPasswordReset());
		// set time to midnight
		Long lastPasswordReset = getLastPasswordReset().getTime() - calendar.get(Calendar.HOUR_OF_DAY) * 60 * 60 * 1000 - calendar.get(Calendar.MINUTE) * 60 * 1000 - calendar.get(Calendar.SECOND)
				* 1000;

		if (lastPasswordReset + validDuration < new Date().getTime()) {
			return true;
		}
		return false;
	}

	public Timestamp getLastModified() {
		return lastModified;
	}

	public void setLastModified(Timestamp lastModified) {
		this.lastModified = lastModified;
	}

	public Date getLastOrderDate() {
		return lastOrderDate;
	}

	public void setLastOrderDate(Date lastOrderDate) {
		this.lastOrderDate = lastOrderDate;
	}

	public Integer getPriceTable() {
		return priceTable;
	}

	public void setPriceTable(Integer priceTable) {
		this.priceTable = priceTable;
	}

	public String getProtectedAccess() {
		return protectedAccess;
	}

	public void setProtectedAccess(String protectedAccess) {
		this.protectedAccess = protectedAccess;
	}

	public String getPayment() {
		return payment;
	}

	public void setPayment(String payment) {
		this.payment = payment;
	}

	public String getTrackcode() {
		return trackcode;
	}

	public void setTrackcode(String trackcode) {
		this.trackcode = trackcode;
	}

	public String getTaxId() {
		return taxId;
	}

	public void setTaxId(String taxId) {
		this.taxId = taxId;
	}

	public String getNote() {
		return note;
	}

	public void setNote(String note) {
		this.note = note;
	}

	public String getAccountNumber() {
		return accountNumber;
	}

	public void setAccountNumber(String accountNumber) {
		this.accountNumber = accountNumber;
	}

	public String getCardId() {
		return cardId;
	}

	public void setCardId(String cardId) {
		this.cardId = cardId;
	}

	public boolean isKeepMeLogin() {
		return keepMeLogin;
	}

	public void setKeepMeLogin(boolean keepMeLogin) {
		this.keepMeLogin = keepMeLogin;
	}

	public void setCardIdCount(Integer cardIdCount) {
		this.cardIdCount = cardIdCount;
	}

	public Integer getCardIdCount() {
		return cardIdCount;
	}

	public boolean isImported() {
		return imported;
	}

	public void setImported(boolean imported) {
		this.imported = imported;
	}

	public Affiliate getAffiliate() {
		return affiliate;
	}

	public void setAffiliate(Affiliate affiliate) {
		this.affiliate = affiliate;
	}

	public Boolean getIsAffiliate() {
		return isAffiliate;
	}

	public void setIsAffiliate(Boolean isAffiliate) {
		this.isAffiliate = isAffiliate;
	}

	public BitSet getProtectedAccessAsBitSet() {
		BitSet bitSet = new BitSet();
		for (int i = 0; i < protectedAccess.length(); i++) {
			if (this.protectedAccess.charAt(protectedAccess.length() - 1 - i) == '1') {
				bitSet.set(i);
			}
		}
		return bitSet;
	}

	public Double getOrdersGrandTotal() {
		return ordersGrandTotal;
	}

	public void setOrdersGrandTotal(Double ordersGrandTotal) {
		this.ordersGrandTotal = ordersGrandTotal;
	}

	public Integer getOrderCount() {
		return orderCount;
	}

	public void setOrderCount(Integer orderCount) {
		this.orderCount = orderCount;
	}

	public String getField1() {
		return field1;
	}

	public void setField1(String field1) {
		this.field1 = field1;
	}

	public String getField2() {
		return field2;
	}

	public void setField2(String field2) {
		this.field2 = field2;
	}

	public String getField3() {
		return field3;
	}

	public void setField3(String field3) {
		this.field3 = field3;
	}

	public String getField4() {
		return field4;
	}

	public void setField4(String field4) {
		this.field4 = field4;
	}

	public String getField5() {
		return field5;
	}

	public void setField5(String field5) {
		this.field5 = field5;
	}

	public String getField6() {
		return field6;
	}

	public void setField6(String field6) {
		this.field6 = field6;
	}

	public String getField7() {
		return field7;
	}

	public void setField7(String field7) {
		this.field7 = field7;
	}

	public String getField8() {
		return field8;
	}

	public void setField8(String field8) {
		this.field8 = field8;
	}

	public String getField9() {
		return field9;
	}

	public void setField9(String field9) {
		this.field9 = field9;
	}

	public String getField10() {
		return field10;
	}

	public void setField10(String field10) {
		this.field10 = field10;
	}

	public String getField11() {
		return field11;
	}

	public void setField11(String field11) {
		this.field11 = field11;
	}

	public String getField12() {
		return field12;
	}

	public void setField12(String field12) {
		this.field12 = field12;
	}

	public String getField13() {
		return field13;
	}

	public void setField13(String field13) {
		this.field13 = field13;
	}

	public String getField14() {
		return field14;
	}

	public void setField14(String field14) {
		this.field14 = field14;
	}

	public String getField15() {
		return field15;
	}

	public void setField15(String field15) {
		this.field15 = field15;
	}

	public String getField16() {
		return field16;
	}

	public void setField16(String field16) {
		this.field16 = field16;
	}

	public String getField17() {
		return field17;
	}

	public void setField17(String field17) {
		this.field17 = field17;
	}

	public String getField18() {
		return field18;
	}

	public void setField18(String field18) {
		this.field18 = field18;
	}

	public String getField19() {
		return field19;
	}

	public void setField19(String field19) {
		this.field19 = field19;
	}

	public String getField20() {
		return field20;
	}

	public void setField20(String field20) {
		this.field20 = field20;
	}

	public String getField21() {
		return field21;
	}

	public void setField21(String field21) {
		this.field21 = field21;
	}

	public String getField22() {
		return field22;
	}

	public void setField22(String field22) {
		this.field22 = field22;
	}

	public String getField23() {
		return field23;
	}

	public void setField23(String field23) {
		this.field23 = field23;
	}

	public String getField24() {
		return field24;
	}

	public void setField24(String field24) {
		this.field24 = field24;
	}

	public String getField25() {
		return field25;
	}

	public void setField25(String field25) {
		this.field25 = field25;
	}

	public String getField26() {
		return field26;
	}

	public void setField26(String field26) {
		this.field26 = field26;
	}

	public String getField27() {
		return field27;
	}

	public void setField27(String field27) {
		this.field27 = field27;
	}

	public String getField28() {
		return field28;
	}

	public void setField28(String field28) {
		this.field28 = field28;
	}

	public String getField29() {
		return field29;
	}

	public void setField29(String field29) {
		this.field29 = field29;
	}

	public String getField30() {
		return field30;
	}

	public void setField30(String field30) {
		this.field30 = field30;
	}

	public List<CustomerField> getCustomerFields() {
		return customerFields;
	}

	public void setCustomerFields(List<CustomerField> customerFields) {
		this.customerFields = customerFields;
	}

	public void addCustomerField(CustomerField customerField) {
		if (customerFields == null)
			customerFields = new ArrayList<CustomerField>();
		customerFields.add(customerField);
	}

	public Integer getParent() {
		return parent;
	}

	public void setParent(Integer parent) {
		this.parent = parent;
	}

	public Integer getSubCount() {
		if (subCount == null)
			return 0;
		return subCount;
	}

	public void setSubCount(Integer subCount) {
		this.subCount = subCount;
	}

	public String getShippingTitle() {
		return shippingTitle;
	}

	public void setShippingTitle(String shippingTitle) {
		this.shippingTitle = shippingTitle;
	}

	public boolean isSuspended() {
		return suspended;
	}

	public void setSuspended(boolean suspended) {
		this.suspended = suspended;
	}

	public void setSuspendedEvent(boolean suspendedEvent) {
		this.suspendedEvent = suspendedEvent;
	}

	public boolean isSuspendedEvent() {
		return suspendedEvent;
	}

	public String getCustomNote1() {
		return customNote1;
	}

	public void setCustomNote1(String customNote1) {
		this.customNote1 = customNote1;
	}

	public String getCustomNote2() {
		return customNote2;
	}

	public void setCustomNote2(String customNote2) {
		this.customNote2 = customNote2;
	}

	public boolean isEmailNotify() {
		return emailNotify;
	}

	public void setEmailNotify(boolean emailNotify) {
		this.emailNotify = emailNotify;
	}

	public String getCustomNote3() {
		return customNote3;
	}

	public void setCustomNote3(String customNote3) {
		this.customNote3 = customNote3;
	}

	public String getHost() {
		return host;
	}

	public void setHost(String host) {
		this.host = host;
	}

	public Double getCredit() {
		return credit;
	}

	public void setCredit(Double credit) {
		this.credit = credit;
	}

	public Double getLoyaltyPoint() {
		return loyaltyPoint;
	}

	public void setLoyaltyPoint(Double loyaltyPoint) {
		this.loyaltyPoint = loyaltyPoint;
	}

	public Integer getPointThreshold() {
		return pointThreshold;
	}

	public void setPointThreshold(Integer pointThreshold) {
		this.pointThreshold = pointThreshold;
	}

	public Double getPointValue() {
		return pointValue;
	}

	public void setPointValue(Double pointValue) {
		this.pointValue = pointValue;
	}

	public boolean isHideSubAccts() {
		return hideSubAccts;
	}

	public void setHideSubAccts(boolean hideSubAccts) {
		this.hideSubAccts = hideSubAccts;
	}

	public String getRegisteredBy() {
		return registeredBy;
	}

	public void setRegisteredBy(String registeredBy) {
		this.registeredBy = registeredBy;
	}

	public String getExtraEmail() {
		return extraEmail;
	}

	public void setExtraEmail(String extraEmail) {
		this.extraEmail = extraEmail;
	}

	public boolean isConvertToSupplier() {
		return convertToSupplier;
	}

	public void setConvertToSupplier(boolean convertToSupplier) {
		this.convertToSupplier = convertToSupplier;
	}

	public Integer getSupplierId() {
		return supplierId;
	}

	public void setSupplierId(Integer supplierId) {
		this.supplierId = supplierId;
	}

	public String getSupplierPrefix() {
		return supplierPrefix;
	}

	public void setSupplierPrefix(String supplierPrefix) {
		this.supplierPrefix = supplierPrefix;
	}

	public Integer getSupplierProdLimit() {
		return supplierProdLimit;
	}

	public void setSupplierProdLimit(Integer supplierProdLimit) {
		this.supplierProdLimit = supplierProdLimit;
	}

	public Integer getAffiliateNumChild() {
		return affiliateNumChild;
	}

	public void setAffiliateNumChild(Integer affiliateNumChild) {
		this.affiliateNumChild = affiliateNumChild;
	}

	public Double getCommissionTotal() {
		return commissionTotal;
	}

	public void setCommissionTotal(Double commissionTotal) {
		this.commissionTotal = commissionTotal;
	}

	public Integer getAffiliateNumOrder() {
		return affiliateNumOrder;
	}

	public void setAffiliateNumOrder(Integer affiliateNumOrder) {
		this.affiliateNumOrder = affiliateNumOrder;
	}

	public boolean isUnsubscribe() {
		return unsubscribe;
	}

	public void setUnsubscribe(boolean unsubscribe) {
		this.unsubscribe = unsubscribe;
	}

	public Set<Object> getCatIds() {
		return catIds;
	}

	public void setCatIds(Set<Object> catIds) {
		this.catIds = catIds;
	}

	public Review getRate() {
		return rate;
	}

	public void setRate(Review rate) {
		this.rate = rate;
	}

	public String getShortDesc() {
		return shortDesc;
	}

	public void setShortDesc(String shortDesc) {
		this.shortDesc = shortDesc;
	}

	public String getLongDesc() {
		return longDesc;
	}

	public void setLongDesc(String longDesc) {
		this.longDesc = longDesc;
	}

	public void setBrandReport(String brandReport) {
		this.brandReport = brandReport;
	}

	public String getBrandReport() {
		return brandReport;
	}

	public void setNumOfCartItems(Integer numOfCartItems) {
		this.numOfCartItems = numOfCartItems;
	}

	public Integer getNumOfCartItems() {
		return numOfCartItems;
	}

	public void setCrmContactId(Integer crmContactId) {
		this.crmContactId = crmContactId;
	}

	public Integer getCrmContactId() {
		return crmContactId;
	}

	public void setCrmAccountId(Integer crmAccountId) {
		this.crmAccountId = crmAccountId;
	}

	public Integer getCrmAccountId() {
		return crmAccountId;
	}

	public void setLanguageCode(LanguageCode languageCode) {
		this.languageCode = languageCode;
	}

	public LanguageCode getLanguageCode() {
		return languageCode;
	}

	public void setQuoteCount(Integer quoteCount) {
		this.quoteCount = quoteCount;
	}

	public Integer getQuoteCount() {
		return quoteCount;
	}

	public Double getSupplierPay() {
		return supplierPay;
	}

	public void setSupplierPay(Double supplierPay) {
		this.supplierPay = supplierPay;
	}

	public String getRating1() {
		return rating1;
	}

	public void setRating1(String rating1) {
		this.rating1 = rating1;
	}

	public String getRating2() {
		return rating2;
	}

	public void setRating2(String rating2) {
		this.rating2 = rating2;
	}

	public Double getCreditAllowed() {
		return creditAllowed;
	}

	public void setCreditAllowed(Double creditAllowed) {
		this.creditAllowed = creditAllowed;
	}

	public Double getPaymentAlert() {
		return paymentAlert;
	}

	public void setPaymentAlert(Double paymentAlert) {

		this.paymentAlert = paymentAlert;
	}

	/**
	 * @return the prospectId
	 */
	public String getProspectId() {
		return prospectId;
	}

	/**
	 * @param prospectId
	 *        the prospectId to set
	 */
	public void setProspectId(String prospectId) {
		this.prospectId = prospectId;
	}

	public String getGatewayToken() {
		return gatewayToken;
	}

	public void setGatewayToken(String gatewayToken) {
		this.gatewayToken = gatewayToken;
	}

	public Map<Integer, String> getGatewayPaymentMethods() {
		return gatewayPaymentMethods;
	}

	public void setGatewayPaymentMethods(Map<Integer, String> gatewayPaymentMethods) {
		this.gatewayPaymentMethods = gatewayPaymentMethods;
	}

	public String getBudgetPlan() {
		return budgetPlan;
	}

	public void setBudgetPlan(String budgetPlan) {
		this.budgetPlan = budgetPlan;
	}

	public List<CustomerBudgetPartner> getBudgetPartnersList() {
		return budgetPartnersList;
	}

	public void setBudgetPartnersList(List<CustomerBudgetPartner> budgetPartnersList) {
		this.budgetPartnersList = budgetPartnersList;
	}

	public boolean isAutoGenerateAccountNumber() {
		return autoGenerateAccountNumber;
	}

	public void setAutoGenerateAccountNumber(boolean autoGenerateAccountNumber) {
		this.autoGenerateAccountNumber = autoGenerateAccountNumber;
	}

	public String getWebsiteUrl() {
		return websiteUrl;
	}

	public void setWebsiteUrl(String websiteUrl) {
		this.websiteUrl = websiteUrl;
	}

	public String getEbayName() {
		return ebayName;
	}

	public void setEbayName(String ebayName) {
		this.ebayName = ebayName;
	}

	public String getAmazonName() {
		return amazonName;
	}

	public void setAmazonName(String amazonName) {
		this.amazonName = amazonName;
	}

	public String getUnsubscribeReason() {
		return unsubscribeReason;
	}

	public void setUnsubscribeReason(String unsubscribeReason) {
		this.unsubscribeReason = unsubscribeReason;
	}

	public boolean isUnsubscribeTextMessage() {
		return unsubscribeTextMessage;
	}

	public void setUnsubscribeTextMessage(boolean unsubscribeTextMessage) {
		this.unsubscribeTextMessage = unsubscribeTextMessage;
	}

	public boolean isTextMessageNotify() {
		return textMessageNotify;
	}

	public void setTextMessageNotify(boolean textMessageNotify) {
		this.textMessageNotify = textMessageNotify;
	}

	public boolean isWhatsappNotify() {
		return whatsappNotify;
	}

	public void setWhatsappNotify(boolean whatsappNotify) {
		this.whatsappNotify = whatsappNotify;
	}

	public boolean isUpdateNewInformation() {
		return updateNewInformation;
	}

	public void setUpdateNewInformation(boolean updateNewInformation) {
		this.updateNewInformation = updateNewInformation;
	}

	public BrokerImage getBrokerImage() {
		return brokerImage;
	}

	public void setBrokerImage(BrokerImage brokerImage) {
		this.brokerImage = brokerImage;
	}

	public boolean isRemoveBrokerImage() {
		return removeBrokerImage;
	}

	public void setRemoveBrokerImage(boolean removeBrokerImage) {
		this.removeBrokerImage = removeBrokerImage;
	}

	public Integer getCustomerType() {
		return customerType;
	}

	public void setCustomerType(Integer customerType) {
		this.customerType = customerType;
	}

	public String getPaid() { return paid; }

	public void setPaid(String paid) { this.paid = paid; }

	public String getLanguage() { return language; }

	public void setLanguage(String language) { this.language = language; }

	public String getMedium() { return medium; }

	public void setMedium(String medium) { this.medium = medium; }

	public String getMainSource() { return mainSource; }

	public void setMainSource(String mainSource) { this.mainSource = mainSource; }

	public String getReferrer() { return referrer; }

	public void setReferrer(String referrer) { this.referrer = referrer; }

	public String getQueryString() { return queryString; }

	public void setQueryString(String queryString) { this.queryString = queryString; }

	public boolean isDoNotCall() { return doNotCall; }

	public void setDoNotCall(boolean doNotCall) { this.doNotCall = doNotCall; }

	public List<DialingNote> getDialingNoteHistory() { return dialingNoteHistory; }

	public void setDialingNoteHistory(List<DialingNote> dialingNoteHistory) { this.dialingNoteHistory = dialingNoteHistory; }

	public DialingNote getNewDialingNote() { return newDialingNote; }

	public void setNewDialingNote(DialingNote newDialingNote) { this.newDialingNote = newDialingNote; }

	public String getLastDialingNote() {
		return lastDialingNote;
	}

	public void setLastDialingNote(String lastDialingNote) {
		this.lastDialingNote = lastDialingNote;
	}

	public Integer getQualifier() {
		return qualifier;
	}

	public void setQualifier(Integer qualifier) {
		this.qualifier = qualifier;
	}
	
	private List<Integer> groupIdList;

	public List<Integer> getGroupIdList() {
		return groupIdList;
	}

	public void setGroupIdList(List<Integer> groupIdList) {
		this.groupIdList = groupIdList;
	}
 
	public String getUsername2() {
		return username2;
	}

	public void setUsername2(String username2) {
		this.username2 = username2;
	}

	public boolean isRegisterCellPhone() {
		return registerCellPhone;
	}

	public void setRegisterCellPhone(boolean registerCellPhone) {
		this.registerCellPhone = registerCellPhone;
	}

	public Boolean getCellPhoneReg() {
		return cellPhoneReg;
	}

	public void setCellPhoneReg(Boolean cellPhoneReg) {
		this.cellPhoneReg = cellPhoneReg;
	}

	public String getCellPhone() {
		return cellPhone;
	}

	public void setCellPhone(String cellPhone) {
		this.cellPhone = cellPhone;
	}

	public Integer getAccessUserId() {
		return accessUserId;
	}

	public void setAccessUserId(Integer accessUserId) {
		this.accessUserId = accessUserId;
	}

	public Set<String> getField1Set() {		
		if (field1Set != null) {
			return field1Set;
		}
		String value = this.getField1();
		Set<String> field1Set = new HashSet<String>();
		if(value != null) {
			String[] arr = value.split(",");
			for(String sv : arr) {
				field1Set.add(sv);
			}
		}
		this.setField1Set(field1Set);		
		return field1Set;
	}

	public void setField1Set(Set<String> field1Set) {
		this.field1Set = field1Set;
	}

	public Set<String> getField2Set() {
		if (field2Set != null) {
			return field2Set;
		}
		String value = this.getField2();
		Set<String> field2Set = new HashSet<String>();
		if(value != null) {
			String[] arr = value.split(",");
			for(String sv : arr) {
				field2Set.add(sv);
			}
		}
		this.setField2Set(field2Set);		
		return field2Set;
	}

	public void setField2Set(Set<String> field2Set) {
		this.field2Set = field2Set;
	}

	public Set<String> getField3Set() {
		if (field3Set != null) {
			return field3Set;
		}
		String value = this.getField3();
		Set<String> field3Set = new HashSet<String>();
		if(value != null) {
			String[] arr = value.split(",");
			for(String sv : arr) {
				field3Set.add(sv);
			}
		}
		this.setField3Set(field3Set);		
		return field3Set;
	}

	public void setField3Set(Set<String> field3Set) {
		this.field3Set = field3Set;
	}

	public Set<String> getField4Set() {
		if (field4Set != null) {
			return field4Set;
		}
		String value = this.getField4();
		Set<String> field4Set = new HashSet<String>();
		if(value != null) {
			String[] arr = value.split(",");
			for(String sv : arr) {
				field4Set.add(sv);
			}
		}
		this.setField4Set(field4Set);		
		return field4Set;
	}

	public void setField4Set(Set<String> field4Set) {
		this.field4Set = field4Set;
	}

	public Set<String> getField5Set() {
		if (field5Set != null) {
			return field5Set;
		}
		String value = this.getField5();
		Set<String> field5Set = new HashSet<String>();
		if(value != null) {
			String[] arr = value.split(",");
			for(String sv : arr) {
				field5Set.add(sv);
			}
		}
		this.setField5Set(field5Set);		
		return field5Set;
	}

	public void setField5Set(Set<String> field5Set) {
		this.field5Set = field5Set;
	}

	public Set<String> getField6Set() {
		if (field6Set != null) {
			return field6Set;
		}
		String value = this.getField6();
		Set<String> field6Set = new HashSet<String>();
		if(value != null) {
			String[] arr = value.split(",");
			for(String sv : arr) {
				field6Set.add(sv);
			}
		}
		this.setField6Set(field6Set);		
		return field6Set;
	}

	public void setField6Set(Set<String> field6Set) {
		this.field6Set = field6Set;
	}

	public Set<String> getField7Set() {
		if (field7Set != null) {
			return field7Set;
		}
		String value = this.getField7();
		Set<String> field7Set = new HashSet<String>();
		if(value != null) {
			String[] arr = value.split(",");
			for(String sv : arr) {
				field7Set.add(sv);
			}
		}
		this.setField7Set(field7Set);		
		return field7Set;
	}

	public void setField7Set(Set<String> field7Set) {
		this.field7Set = field7Set;
	}

	public Set<String> getField8Set() {
		if (field8Set != null) {
			return field8Set;
		}
		String value = this.getField8();
		Set<String> field8Set = new HashSet<String>();
		if(value != null) {
			String[] arr = value.split(",");
			for(String sv : arr) {
				field8Set.add(sv);
			}
		}
		this.setField8Set(field8Set);		
		return field8Set;
	}

	public void setField8Set(Set<String> field8Set) {
		this.field8Set = field8Set;
	}

	public Set<String> getField9Set() {
		if (field9Set != null) {
			return field9Set;
		}
		String value = this.getField9();
		Set<String> field9Set = new HashSet<String>();
		if(value != null) {
			String[] arr = value.split(",");
			for(String sv : arr) {
				field9Set.add(sv);
			}
		}
		this.setField9Set(field9Set);		
		return field9Set;
	}

	public void setField9Set(Set<String> field9Set) {
		this.field9Set = field9Set;
	}

	public Set<String> getField10Set() {
		if (field10Set != null) {
			return field10Set;
		}
		String value = this.getField10();
		Set<String> field10Set = new HashSet<String>();
		if(value != null) {
			String[] arr = value.split(",");
			for(String sv : arr) {
				field10Set.add(sv);
			}
		}
		this.setField10Set(field10Set);		
		return field10Set;
	}

	public void setField10Set(Set<String> field10Set) {
		this.field10Set = field10Set;
	}

	public Set<String> getField11Set() {
		if (field11Set != null) {
			return field11Set;
		}
		String value = this.getField11();
		Set<String> field11Set = new HashSet<String>();
		if(value != null) {
			String[] arr = value.split(",");
			for(String sv : arr) {
				field11Set.add(sv);
			}
		}
		this.setField11Set(field11Set);		
		return field11Set;
	}

	public void setField11Set(Set<String> field11Set) {
		this.field11Set = field11Set;
	}

	public Set<String> getField12Set() {
		if (field12Set != null) {
			return field12Set;
		}
		String value = this.getField12();
		Set<String> field12Set = new HashSet<String>();
		if(value != null) {
			String[] arr = value.split(",");
			for(String sv : arr) {
				field12Set.add(sv);
			}
		}
		this.setField12Set(field12Set);		
		return field12Set;
	}

	public void setField12Set(Set<String> field12Set) {
		this.field12Set = field12Set;
	}

	public Set<String> getField13Set() {
		if (field13Set != null) {
			return field13Set;
		}
		String value = this.getField13();
		Set<String> field13Set = new HashSet<String>();
		if(value != null) {
			String[] arr = value.split(",");
			for(String sv : arr) {
				field13Set.add(sv);
			}
		}
		this.setField13Set(field13Set);		
		return field13Set;
	}

	public void setField13Set(Set<String> field13Set) {
		this.field13Set = field13Set;
	}

	public Set<String> getField14Set() {
		if (field14Set != null) {
			return field14Set;
		}
		String value = this.getField14();
		Set<String> field14Set = new HashSet<String>();
		if(value != null) {
			String[] arr = value.split(",");
			for(String sv : arr) {
				field14Set.add(sv);
			}
		}
		this.setField14Set(field14Set);		
		return field14Set;
	}

	public void setField14Set(Set<String> field14Set) {
		this.field14Set = field14Set;
	}

	public Set<String> getField15Set() {
		if (field15Set != null) {
			return field15Set;
		}
		String value = this.getField15();
		Set<String> field15Set = new HashSet<String>();
		if(value != null) {
			String[] arr = value.split(",");
			for(String sv : arr) {
				field15Set.add(sv);
			}
		}
		this.setField15Set(field15Set);		
		return field15Set;
	}

	public void setField15Set(Set<String> field15Set) {
		this.field15Set = field15Set;
	}

	public Set<String> getField16Set() {
		if (field16Set != null) {
			return field16Set;
		}
		String value = this.getField16();
		Set<String> field16Set = new HashSet<String>();
		if(value != null) {
			String[] arr = value.split(",");
			for(String sv : arr) {
				field16Set.add(sv);
			}
		}
		this.setField16Set(field16Set);		
		return field16Set;
	}

	public void setField16Set(Set<String> field16Set) {
		this.field16Set = field16Set;
	}

	public Set<String> getField17Set() {
		if (field17Set != null) {
			return field17Set;
		}
		String value = this.getField17();
		Set<String> field17Set = new HashSet<String>();
		if(value != null) {
			String[] arr = value.split(",");
			for(String sv : arr) {
				field17Set.add(sv);
			}
		}
		this.setField17Set(field17Set);		
		return field17Set;
	}

	public void setField17Set(Set<String> field17Set) {
		this.field17Set = field17Set;
	}

	public Set<String> getField18Set() {
		if (field18Set != null) {
			return field18Set;
		}
		String value = this.getField18();
		Set<String> field18Set = new HashSet<String>();
		if(value != null) {
			String[] arr = value.split(",");
			for(String sv : arr) {
				field18Set.add(sv);
			}
		}
		this.setField18Set(field18Set);		
		return field18Set;
	}

	public void setField18Set(Set<String> field18Set) {
		this.field18Set = field18Set;
	}

	public Set<String> getField19Set() {
		if (field19Set != null) {
			return field19Set;
		}
		String value = this.getField19();
		Set<String> field19Set = new HashSet<String>();
		if(value != null) {
			String[] arr = value.split(",");
			for(String sv : arr) {
				field19Set.add(sv);
			}
		}
		this.setField19Set(field19Set);		
		return field19Set;
	}

	public void setField19Set(Set<String> field19Set) {
		this.field19Set = field19Set;
	}

	public Set<String> getField20Set() {
		if (field20Set != null) {
			return field20Set;
		}
		String value = this.getField20();
		Set<String> field20Set = new HashSet<String>();
		if(value != null) {
			String[] arr = value.split(",");
			for(String sv : arr) {
				field20Set.add(sv);
			}
		}
		this.setField20Set(field20Set);		
		return field20Set;
	}

	public void setField20Set(Set<String> field20Set) {
		this.field20Set = field20Set;
	}

	public Set<String> getField21Set() {
		if (field21Set != null) {
			return field21Set;
		}
		String value = this.getField21();
		Set<String> field21Set = new HashSet<String>();
		if(value != null) {
			String[] arr = value.split(",");
			for(String sv : arr) {
				field21Set.add(sv);
			}
		}
		this.setField21Set(field21Set);	
		return field21Set;
	}

	public void setField21Set(Set<String> field21Set) {
		this.field21Set = field21Set;
	}

	public Set<String> getField22Set() {
		if (field22Set != null) {
			return field22Set;
		}
		String value = this.getField22();
		Set<String> field22Set = new HashSet<String>();
		if(value != null) {
			String[] arr = value.split(",");
			for(String sv : arr) {
				field22Set.add(sv);
			}
		}
		this.setField20Set(field22Set);	
		return field22Set;
	}

	public void setField22Set(Set<String> field22Set) {
		this.field22Set = field22Set;
	}

	public Set<String> getField23Set() {
		if (field23Set != null) {
			return field23Set;
		}
		String value = this.getField23();
		Set<String> field23Set = new HashSet<String>();
		if(value != null) {
			String[] arr = value.split(",");
			for(String sv : arr) {
				field23Set.add(sv);
			}
		}
		this.setField23Set(field23Set);	
		return field23Set;
	}

	public void setField23Set(Set<String> field23Set) {
		this.field23Set = field23Set;
	}

	public Set<String> getField24Set() {
		if (field24Set != null) {
			return field24Set;
		}
		String value = this.getField24();
		Set<String> field24Set = new HashSet<String>();
		if(value != null) {
			String[] arr = value.split(",");
			for(String sv : arr) {
				field24Set.add(sv);
			}
		}
		this.setField24Set(field24Set);	
		return field24Set;
	}

	public void setField24Set(Set<String> field24Set) {
		this.field24Set = field24Set;
	}

	public Set<String> getField25Set() {
		if (field25Set != null) {
			return field25Set;
		}
		String value = this.getField25();
		Set<String> field25Set = new HashSet<String>();
		if(value != null) {
			String[] arr = value.split(",");
			for(String sv : arr) {
				field25Set.add(sv);
			}
		}
		this.setField25Set(field25Set);	
		return field25Set;
	}

	public void setField25Set(Set<String> field25Set) {
		this.field25Set = field25Set;
	}

	public Set<String> getField26Set() {
		if (field26Set != null) {
			return field26Set;
		}
		String value = this.getField26();
		Set<String> field26Set = new HashSet<String>();
		if(value != null) {
			String[] arr = value.split(",");
			for(String sv : arr) {
				field26Set.add(sv);
			}
		}
		this.setField26Set(field26Set);	
		return field26Set;
	}

	public void setField26Set(Set<String> field26Set) {
		this.field26Set = field26Set;
	}

	public Set<String> getField27Set() {
		if (field27Set != null) {
			return field27Set;
		}
		String value = this.getField27();
		Set<String> field27Set = new HashSet<String>();
		if(value != null) {
			String[] arr = value.split(",");
			for(String sv : arr) {
				field27Set.add(sv);
			}
		}
		this.setField27Set(field27Set);	
		return field27Set;
	}

	public void setField27Set(Set<String> field27Set) {
		this.field27Set = field27Set;
	}

	public Set<String> getField28Set() {
		if (field28Set != null) {
			return field28Set;
		}
		String value = this.getField28();
		Set<String> field28Set = new HashSet<String>();
		if(value != null) {
			String[] arr = value.split(",");
			for(String sv : arr) {
				field28Set.add(sv);
			}
		}
		this.setField28Set(field28Set);	
		return field28Set;
	}

	public void setField28Set(Set<String> field28Set) {
		this.field28Set = field28Set;
	}

	public Set<String> getField29Set() {
		if (field29Set != null) {
			return field29Set;
		}
		String value = this.getField29();
		Set<String> field29Set = new HashSet<String>();
		if(value != null) {
			String[] arr = value.split(",");
			for(String sv : arr) {
				field29Set.add(sv);
			}
		}
		this.setField29Set(field29Set);	
		return field29Set;
	}

	public void setField29Set(Set<String> field29Set) {
		this.field29Set = field29Set;
	}

	public Set<String> getField30Set() {
		if (field30Set != null) {
			return field30Set;
		}
		String value = this.getField30();
		Set<String> field30Set = new HashSet<String>();
		if(value != null) {
			String[] arr = value.split(",");
			for(String sv : arr) {
				field30Set.add(sv);
			}
		}
		this.setField30Set(field30Set);	
		return field30Set;
	}

	public void setField30Set(Set<String> field30Set) {
		this.field30Set = field30Set;
	}
	
	
}
