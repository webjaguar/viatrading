/* Copyright 2006 Advanced E-Media Solutions
 *
 */

package com.webjaguar.model;

public class DataFeedSearch {
	
	
	private Integer limit;
	public Integer getLimit() { return limit; }
	public void setLimit(Integer limit) { this.limit = limit; }

	// offset
	private int offset;
	public int getOffset() { return offset; }
	public void setOffset(int offset) { this.offset = offset; }	
	
	private Integer inventory;
	public Integer getInventory() { return inventory; }
	public void setInventory(Integer inventory) { this.inventory = inventory; }
	
	private Double price;
	public Double getPrice() { return price; }
	public void setPrice(Double price) { this.price = price; }
	
	private String masterSkuImage;
	public String getMasterSkuImage() { return masterSkuImage; }
	public void setMasterSkuImage(String masterSkuImage) { this.masterSkuImage = masterSkuImage; }
	
	// i18n
	private String lang;
	public String getLang() { return lang; }
	public void setLang(String lang) { this.lang = lang; }
	
	// category ID
	private Integer category;
	public Integer getCategory() { return category; }
	public void setCategory(Integer category) { this.category = category; }
	
	// sort
	private String sort = "id desc";
	public String getSort() { return sort; }
	public void setSort(String sort) { this.sort = sort; }	
	
	// page 
	private int page = 1;
	public int getPage() { return page; }
	public void setPage(int page) { this.page = page; }
	
	// page size
	private int pageSize = 10;
	public int getPageSize() { return pageSize; }
	public void setPageSize(int pageSize) { this.pageSize = pageSize; }

	// active
	private Boolean active;
	public Boolean getActive() { return active; }
	public void setActive(Boolean active) { this.active = active; }

	// subscriber
	private Boolean subscriber;
	public Boolean getSubscriber() { return subscriber; }
	public void setSubscriber(Boolean subscriber) { this.subscriber = subscriber; }

	// feed type
	private String feedType;
	public String getFeedType() { return feedType; }
	public void setFeedType(String feedType) { this.feedType = feedType; }

}