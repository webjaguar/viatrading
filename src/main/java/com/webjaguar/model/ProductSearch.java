/* Copyright 2006 Advanced E-Media Solutions
 *
 */

package com.webjaguar.model;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

import spexlive.etilize.com.SearchCriteria;

public class ProductSearch {
	
	// sort
	private String sort = "id desc";
	public String getSort() { return sort; }
	public void setSort(String sort) { this.sort = sort; }	
	
	// Field Type
	private String fieldType;
	public String getFieldType() { return fieldType; }
	public void setFieldType(String fieldType) { this.fieldType = fieldType; }
		
	//Order(ASC/DESC)
	private String orderBy;
	public String getOrderBy() { return orderBy; }
	public void setOrderBy(String orderBy) { this.orderBy = orderBy; }

	// sort temp
	private String sortTemp;
	public String getSortTemp() { return sortTemp; }
	public void setSortTemp(String sortTemp) { this.sortTemp = sortTemp; }	

	// category ID
	private Integer category;
	public Integer getCategory() { return category; }
	public void setCategory(Integer category) { this.category = category; }
	
	// Caregory URL
	private String CategoryURL;
	public String getCategoryURL() { return CategoryURL; }
	public void setCategoryURL(String categoryURL) { CategoryURL = categoryURL; }

	// keywords
	private String keywords = "";
	public String getKeywords() { return keywords; }
	public void setKeywords(String keywords) { 
		keywordList.clear();	// clear old list
		this.keywords = keywords; 
	}
	
	// excludeKeywords
	private String excludeKeywords;
	public String getExcludeKeywords() { return excludeKeywords; }
	public void setExcludeKeywords(String excludeKeywords) { this.excludeKeywords = excludeKeywords; }
	
	// keywords minimum characters
	private int keywordsMinChars;
	public int getKeywordsMinChars() { return keywordsMinChars; }
	public void setKeywordsMinChars(int keywordsMinChars) { this.keywordsMinChars = keywordsMinChars; }
	
	private List<String> keywordList = new ArrayList<String>();
	public void setKeywordList(List<String> keywordList) { this.keywordList = keywordList; }	
	public List<String> getKeywordList() {	return this.keywordList; }
	public void addKeyword(String keyword) { keywordList.add(keyword); }

	// plural->singular keyword
	private boolean inflector;
	public boolean isInflector() {return inflector;}
	public void setInflector(boolean inflector) {this.inflector = inflector;}
	
	//quote
	private boolean quote;
	public boolean isQuote() {return quote;}
	public void setQuote(boolean quote) {this.quote = quote;}

	private String protectedAccess = "0";
	public String getProtectedAccess() { return protectedAccess; }
	public void setProtectedAccess(String protectedAccess) {
		this.protectedAccess = protectedAccess;
	}
	
	private String productId;
	public String getProductId() {	return productId; }
	public void setProductId(String productId)	{ this.productId = productId; }
	
	private String productName;
	public String getProductName() { return productName; }
	public void setProductName(String productName) { this.productName = productName; }
	
	private String sku;
	public String getSku() { return sku; }
	public void setSku(String sku) { this.sku = sku; }
	
	private int skuOperator;
	public int getSkuOperator() { return skuOperator; }
	public void setSkuOperator(int skuOperator) { this.skuOperator = skuOperator; }
	
	private boolean masterSku;
	public boolean isMasterSku() { return masterSku; }
	public void setMasterSku(boolean masterSku) { this.masterSku = masterSku; }
	
	private String parentSku;
	public String getParentSku() { return parentSku; }
	public void setParentSku(String parentSku) { this.parentSku = parentSku; }
	
	private int parentSkuOperator;
	public int getParentSkuOperator() { return parentSkuOperator; }
	public void setParentSkuOperator(int parentSkuOperator) { this.parentSkuOperator = parentSkuOperator; }
	
	private boolean searchable;
	public boolean isSearchable() { return searchable; }
	public void setSearchable(boolean searchable) { this.searchable = searchable; }
	
	private String salesTagTitle;
	public String getSalesTagTitle() { return salesTagTitle; }
	public void setSalesTagTitle(String salesTagTitle) { this.salesTagTitle = salesTagTitle; }
	
	private String supplierCompany;
	public String getSupplierCompany() { return supplierCompany; }
	public void setSupplierCompany(String supplierCompany) { this.supplierCompany = supplierCompany; }
	
	private String supplierAccountNumber;
	public String getSupplierAccountNumber() { return supplierAccountNumber; }
	public void setSupplierAccountNumber(String supplierAccountNumber) { this.supplierAccountNumber = supplierAccountNumber; }
	
	private String manufacturer;
	public String getManufacturer() { return manufacturer; }
	public void setManufacturer(String manufacturer) { this.manufacturer = manufacturer; }
	
	// page 
	private int page = 1;
	public int getPage() { return page; }
	public void setPage(int page) { this.page = page; }
	
	// page size
	private int pageSize = 10;
	public int getPageSize() { return pageSize; }
	public void setPageSize(int pageSize) { this.pageSize = pageSize; }

	// search conjuction
	private String conjunction;
	public String getConjunction() { return conjunction; }
	public void setConjunction(String conjunction) {
		this.conjunction = conjunction;
	}
	
	// keyword delimiter
	private String delimiter;
	public String getDelimiter() { return delimiter; }
	public void setDelimiter(String delimiter) {
		this.delimiter = delimiter;
	}
	
	private boolean endDelimiter = true;
	public boolean isEndDelimiter() { return endDelimiter; }
	public void setEndDelimiter(boolean endDelimiter) { this.endDelimiter = endDelimiter; }

	// pre sort on category
	private String categorySort;
	public String getCategorySort() { return categorySort; }
	public void setCategorySort(String categorySort) { this.categorySort = categorySort; }
	
	private List<Integer> categoryIds;
	public List<Integer> getCategoryIds() { return categoryIds; }
	public void setCategoryIds(List<Integer> categoryIds) { this.categoryIds = categoryIds; }
	
	private Date startDate; 
	public Date getStartDate() { return startDate; }
	public void setStartDate(Date startDate) { this.startDate = startDate; }
	
	private Date endDate;
	public Date getEndDate() { return endDate; }
	public void setEndDate(Date endDate) { this.endDate = endDate; }
	
	private Integer limit;
	public Integer getLimit() { return limit; }
	public void setLimit(Integer limit) { this.limit = limit; }
	
	private String setType;
	public String getSetType() { return setType; }
	public void setSetType(String setType) { this.setType = setType; }
	
	/*
	 * default has to be false.
	 * iBatic getProductById 
	 * on admin we want inventory as inventory and on hand as on hand
	 * on frontend if baseOnHand is True inventory represent on hand
	 */
	private boolean basedOnHand;
	public boolean isBasedOnHand() { return basedOnHand; }
	public void setBasedOnHand(boolean basedOnHand) { this.basedOnHand = basedOnHand; }
	
	// offset
	private int offset;
	public int getOffset() { return offset; }
	public void setOffset(int offset) { this.offset = offset; }	
	
	private String inventory = null;
	public String getInventory() { return inventory; }
	public void setInventory(String inventory) { this.inventory = inventory; }
	
	private String inventoryAFS = null;
	public String getinventoryAFS() { return inventoryAFS; }
	public void setinventoryAFS(String inventory) { this.inventory = inventoryAFS; }

	private int allowNegInventory = 0;
	public int getAllowNegInventory() { return allowNegInventory; }
	public void setAllowNegInventory(int allowNegInventory) { this.allowNegInventory = allowNegInventory; }
	
	// export
	private boolean withProductAndImageUrl;
	public boolean isWithProductAndImageUrl() { return withProductAndImageUrl; }
	public void setWithProductAndImageUrl(boolean withProductAndImageUrl) { this.withProductAndImageUrl = withProductAndImageUrl; }	

	private boolean withLongDesc = true;
	public boolean isWithLongDesc() { return withLongDesc; }
	public void setWithLongDesc(boolean withLongDesc) { this.withLongDesc = withLongDesc; }
	
	private boolean withProductName;
	public boolean isWithProductName() { return withProductName; }
	public void setWithProductName(boolean withProductName) { this.withProductName = withProductName; }

	private boolean withProductId;
	public boolean isWithProductId() { return withProductId; }
	public void setWithProductId(boolean withProductId) { this.withProductId = withProductId; }
	
	private boolean withI18n;
	public boolean isWithI18n() { return withI18n; }
	public void setWithI18n(boolean withI18n) { this.withI18n = withI18n; }
	
	private boolean withRecommendedList = true;
	public boolean isWithRecommendedList() { return withRecommendedList; }
	public void setWithRecommendedList(boolean withRecommendedList) { this.withRecommendedList = withRecommendedList; }

	private List<ProductField> productField;
	public List<ProductField> getProductField() { return productField; }
	public void setProductField(List<ProductField> productField) { this.productField = productField; }

	private Map<String, ProductField> productFieldMap;
	public Map<String, ProductField> getProductFieldMap() { return productFieldMap; }
	public void setProductFieldMap(Map<String, ProductField> productFieldMap) { this.productFieldMap = productFieldMap; }
	
	private Double minPrice;
	public Double getMinPrice() { return minPrice; }
	public void setMinPrice(Double minPrice) { this.minPrice = minPrice; }

	private Double maxPrice;
	public Double getMaxPrice() { return maxPrice; }
	public void setMaxPrice(Double maxPrice) { this.maxPrice = maxPrice; }
	
	
	private Integer minQty;
	public Integer getMinQty() { return minQty; }
	public void setMinQty(Integer minQty) { this.minQty = minQty; }

	private boolean includeSubs;
	public boolean isIncludeSubs() { return includeSubs; }
	public void setIncludeSubs(boolean includeSubs) { this.includeSubs = includeSubs; }
	
	private boolean checkInventory;
	public boolean isCheckInventory() { return checkInventory; }
	public void setCheckInventory(boolean checkInventory) { this.checkInventory = checkInventory; }

	// import
	private Integer defaultSupplierId;
	public Integer getDefaultSupplierId() { return defaultSupplierId; }
	public void setDefaultSupplierId(Integer defaultSupplierId) { this.defaultSupplierId = defaultSupplierId; }
	
	// i18n
	private String lang;
	public String getLang() { return lang; }
	public void setLang(String lang) { this.lang = lang; }
	
	// active
	private String active;
	public String getActive() { return active; }
	public void setActive(String active) { this.active = active; }
	
	// type
	private String type;
	public String getType() { return type; }
	public void setType(String type) { this.type = type; }

	// etilize
	private SearchCriteria etilizeSearchCriteria;
	public void setEtilizeSearchCriteria(SearchCriteria etilizeSearchCriteria) { this.etilizeSearchCriteria = etilizeSearchCriteria; }
	public SearchCriteria getEtilizeSearchCriteria() { return etilizeSearchCriteria; }
	
	List<Product> etilizeSkus;
	public List<Product> getEtilizeSkus() { return etilizeSkus; }
	public void setEtilizeSkus(List<Product> etilizeSkus) { this.etilizeSkus = etilizeSkus; }
	
	// budget by products
	private Integer userIdBudget;
	public Integer getUserIdBudget() { return userIdBudget; }
	public void setUserIdBudget(Integer userIdBudget) { this.userIdBudget = userIdBudget; }
	
	private int priceTable;
	public int getPriceTable() { return priceTable; }
	public void setPriceTable(int priceTable) { this.priceTable = priceTable; }
	
	// file for product import
	private Map<String, Object> importFileMap;
	public Map<String, Object> getImportFileMap() { return importFileMap; }
	public void setImportFileMap(Map<String, Object> importFileMap) { this.importFileMap = importFileMap; }
	
	
	private List<String> searchFields;
	public List<String> getSearchFields() { return searchFields; }
	public void setSearchFields(List<String> searchFields) { this.searchFields = searchFields; }
	
	//multiple values can be on productField.
	private boolean searchFieldsMultiple;
	public boolean getSearchFieldsMultiple() { return searchFieldsMultiple; }
	public void setSearchFieldsMultiple(boolean searchFieldsMultiple) { this.searchFieldsMultiple = searchFieldsMultiple; }

    // list type
	private Integer listType;
	public Integer getListType() { return listType; }
	public void setListType(Integer listType) { this.listType = listType; }
	
	// used for exporting product
	private String fileType;
	public String getFileType() { return fileType; }
	public void setFileType(String fileType) { this.fileType = fileType; }
	
	// Product Feed Type
	private String[] productFeedType;
	public String[] getProductFeedType() { return productFeedType; }
	public void setProductFeedType(String[] productFeedType) { this.productFeedType = productFeedType; }
	
	// group by results
	private String groupSearch;
	public String getGroupSearch() {	return groupSearch; }
	public void setGroupSearch(String groupSearch) { this.groupSearch = groupSearch; }
	
	// consignment
	private String consignment;
	public String getConsignment() { return consignment; }
	public void setConsignment(String consignment) { this.consignment = consignment; }
	
	// include/exclude Mail In Reabte from search result.
	private boolean mailInRebate;
	public void setMailInRebate(boolean mailInRebate) { this.mailInRebate = mailInRebate; }
	public boolean isMailInRebate() { return mailInRebate; }

	// include/exclude deals from search result.
	private boolean deals;
	public void setDeals(boolean deals) { this.deals = deals; }
	public boolean isDeals() { return deals; }

	// include/exclude Product Review from search result.
	private boolean productReview;
	public void setProductReview(boolean productReview) { this.productReview = productReview; }
	public boolean isProductReview() { return productReview; }
	
	// total product fields
	private int gPRODUCT_FIELDS;
	public int getgPRODUCT_FIELDS() { return gPRODUCT_FIELDS; }
	public void setgPRODUCT_FIELDS(int gPRODUCT_FIELDS) { this.gPRODUCT_FIELDS = gPRODUCT_FIELDS; }
	
	// short description
	private String shortDesc;
	public String getShortDesc() { return shortDesc; }
	public void setShortDesc(String shortDesc) { this.shortDesc = shortDesc; }
	
	//hasPrimarySupplier
	private String hasPrimarySupplier ;
	public String getHasPrimarySupplier() { return hasPrimarySupplier; }
	public void setHasPrimarySupplier(String hasPrimarySupplier) { this.hasPrimarySupplier = hasPrimarySupplier; }
	
	//has Parent Sku
	private String hasParentSku;
	public String getHasParentSku() { return hasParentSku; }
	public void setHasParentSku(String hasParentSku) { this.hasParentSku = hasParentSku; }

}	