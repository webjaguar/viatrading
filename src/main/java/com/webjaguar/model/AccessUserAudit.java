package com.webjaguar.model;

import java.util.Date;

public class AccessUserAudit
{
	private Integer id;
	private String username;
	private Date pageVisitDate;
	private String pageWorkTime;
	private String pageSeen;
	private String ipAddress;
	
	public String getUsername()
	{
		return username;
	}
	public void setUsername(String username)
	{
		this.username = username;
	}
	public Date getPageVisitDate()
	{
		return pageVisitDate;
	}
	public void setPageVisitDate(Date pageVisitDate)
	{
		this.pageVisitDate = pageVisitDate;
	}
	public String getPageWorkTime()
	{
		return pageWorkTime;
	}
	public void setPageWorkTime(String pageWorkTime)
	{
		this.pageWorkTime = pageWorkTime;
	}
	public String getPageSeen()
	{
		return pageSeen;
	}
	public void setPageSeen(String pageSeen)
	{
		this.pageSeen = pageSeen;
	}
	public String getIpAddress()
	{
		return ipAddress;
	}
	public void setIpAddress(String ipAddress)
	{
		this.ipAddress = ipAddress;
	}
	public AccessUserAudit( String username, String pageSeen, String ipAddress )
	{
		super();
		this.username = username;
		this.pageSeen = pageSeen;
		this.ipAddress = ipAddress;
	}
	public AccessUserAudit()
	{
		super();
	}
	public Integer getId()
	{
		return id;
	}
	public void setId(Integer id)
	{
		this.id = id;
	}
	

}
