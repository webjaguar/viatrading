package com.webjaguar.model;

public class ShippingHandling
{

	private Double priceLimit;
	private Double newPriceCost;
	private String newShippingTitle;

	private Double shippingBase;
	private Double shippingIncremental;
	private Double handlingBase;
	private Double handlingIncremental;
	private String handlingType;
	
	private String shippingMethod;
	private String shippingTitle;
	
	// To be Determine
	private boolean tbdEnable;
	private Double weightLimit;
	private String tbdTitle = "To Be Determine";
	
	// for carrier
	private String zeroWeightNewShippingTitle;
	private Double zeroWeightNewPriceCost;
	
	public String getTbdTitle()
	{
		return tbdTitle;
	}
	public void setTbdTitle(String tbdTitle)
	{
		this.tbdTitle = tbdTitle;
	}
	public Double getWeightLimit()
	{
		return weightLimit;
	}
	public void setWeightLimit(Double weightLimit)
	{
		this.weightLimit = weightLimit;
	}
	public Double getHandlingBase()
	{
		return handlingBase;
	}
	public void setHandlingBase(Double handlingBase)
	{
		this.handlingBase = handlingBase;
	}
	public Double getHandlingIncremental()
	{
		return handlingIncremental;
	}
	public void setHandlingIncremental(Double handlingIncremental)
	{
		this.handlingIncremental = handlingIncremental;
	}
	public Double getNewPriceCost()
	{
		return newPriceCost;
	}
	public void setNewPriceCost(Double newPriceCost)
	{
		this.newPriceCost = newPriceCost;
	}
	public Double getPriceLimit()
	{
		return priceLimit;
	}
	public void setPriceLimit(Double priceLimit)
	{
		this.priceLimit = priceLimit;
	}
	public Double getShippingBase()
	{
		return shippingBase;
	}
	public void setShippingBase(Double shippingBase)
	{
		this.shippingBase = shippingBase;
	}
	public Double getShippingIncremental()
	{
		return shippingIncremental;
	}
	public void setShippingIncremental(Double shippingIncremental)
	{
		this.shippingIncremental = shippingIncremental;
	}
	public String getShippingMethod()
	{
		return shippingMethod;
	}
	public void setShippingMethod(String shippingMethod)
	{
		this.shippingMethod = shippingMethod;
	}
	public String getShippingTitle()
	{
		return shippingTitle;
	}
	public void setShippingTitle(String shippingTitle)
	{
		this.shippingTitle = shippingTitle;
	}
	public String getHandlingType()
	{
		return handlingType;
	}
	public void setHandlingType(String handlingType)
	{
		this.handlingType = handlingType;
	}

	public String getNewShippingTitle()
	{
		return newShippingTitle;
	}

	public void setNewShippingTitle(String newShippingTitle)
	{
		this.newShippingTitle = newShippingTitle;
	}

	public Double getShippingTotal(Double subTotal, Double weight)
	{
		Double shippingTotal = this.shippingBase;

		if ( this.shippingIncremental != null )
		{
			if ( this.shippingMethod.equals( "prc" ) )
			{
				shippingTotal = shippingTotal + this.shippingIncremental * subTotal;
			}
			else
			{
				shippingTotal = shippingTotal + this.shippingIncremental * weight;
			}
		}

		return shippingTotal;
	}

	public Double getHandlingTotal(Double subTotal, Double weight)
	{
		Double handlingTotal = this.handlingBase;
		if ( handlingTotal == null )
		{
			return 0.0;
		}
		if ( this.handlingIncremental != null )
		{
			if ( this.handlingType.equals( "weight" ) )
			{
				handlingTotal = handlingTotal + this.handlingIncremental * weight;
			}
			else
			{
				handlingTotal = handlingTotal + this.handlingIncremental * subTotal;
			}
		}

		return handlingTotal;
	}
	public boolean isTbdEnable()
	{
		return tbdEnable;
	}
	public void setTbdEnable(boolean tbdEnable)
	{
		this.tbdEnable = tbdEnable;
	}
	public String getZeroWeightNewShippingTitle()
	{
		return zeroWeightNewShippingTitle;
	}
	public void setZeroWeightNewShippingTitle(String zeroWeightNewShippingTitle)
	{
		this.zeroWeightNewShippingTitle = zeroWeightNewShippingTitle;
	}
	public Double getZeroWeightNewPriceCost()
	{
		return zeroWeightNewPriceCost;
	}
	public void setZeroWeightNewPriceCost(Double zeroWeightNewPriceCost)
	{
		this.zeroWeightNewPriceCost = zeroWeightNewPriceCost;
	}

}
