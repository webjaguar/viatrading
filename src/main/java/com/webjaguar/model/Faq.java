/* Copyright 2005, 2006 Advanced E-Media Solutions
 *
 */

package com.webjaguar.model;

import java.util.List;

public class Faq {
	
	public Faq() {}
	
    private Integer id;
	private String question;
	private String answer;
    private Integer rank;
    private Integer groupId;
    private List<Integer> groupIdList;
    private String groupName;
    
	private String protectedLevel = "0";
	
	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}
	
	public String getQuestion() {
		return question;
	}

	public void setQuestion(String question) {
		this.question = question;
	}

	public String getAnswer() {
		return answer;
	}

	public void setAnswer(String answer) {
		this.answer = answer;
	}

	public Integer getRank() {
		return rank;
	}

	public void setRank(Integer rank) {
		this.rank = rank;
	}

	public Integer getGroupId() {
		return groupId;
	}

	public void setGroupId(Integer groupId) {
		this.groupId = groupId;
	}

	public String getGroupName() {
		return groupName;
	}

	public void setGroupName(String groupName) {
		this.groupName = groupName;
	}

	public void setGroupIdList(List<Integer> groupIdList) {
		this.groupIdList = groupIdList;
	}

	public List<Integer> getGroupIdList() {
		return groupIdList;
	}

	public String getProtectedLevel() {
		return protectedLevel;
	}

	public void setProtectedLevel(String protectedLevel) {
		this.protectedLevel = protectedLevel;
	}
	
	public Integer getProtectedLevelAsNumber() {
		if (protectedLevel.contains( "1" )) {
			return protectedLevel.length();
		} else {
			return 0;
		}
	}

}
