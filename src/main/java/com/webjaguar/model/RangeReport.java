package com.webjaguar.model;
import java.util.Calendar;
import java.util.Date;

public class RangeReport{
	
	private String reportingPeriodGroup;
	
	private Date startDate;
	
	private Date endDate;
	
	private Integer count;
	
	private Double total;
	
	public RangeReport() {
		this.count = 0;
		this.total = 0.0;
	}

	public Integer getCount() {
		return count;
	}

	public void setCount(Integer count) {
		this.count = count;
	}

	public Date getEndDate() {
		return endDate;
	}

	public void setEndDate(Date endDate) {
		this.endDate = endDate;
	}

	public Date getStartDate() {
		return startDate;
	}

	public void setStartDate(Date startDate) {
		this.startDate = startDate;
	}

	public String getStartLabel() {
		String ret = "";
		String[] days = {"","Sun","Mon","Tue","Wed","Thu","Fri","Sat"};
		String[] months = {"Jan","Feb","Mar","Apr","May","Jun","Jul","Aug","Sep","Oct","Nov","Dec"};
		Calendar start = Calendar.getInstance();
		start.setTime(this.startDate);
		int month = start.get(Calendar.MONTH);
		int day_of_week = start.get(Calendar.DAY_OF_WEEK);
		if(this.getReportingPeriodGroup().equals("d"))
			ret = days[day_of_week];
		if(this.getReportingPeriodGroup().equals("w"))
			ret = months[month]+" "+start.get(Calendar.DATE);
		if(this.getReportingPeriodGroup().equals("m"))
			ret = months[month];
		return ret;
	}
	
	public String getEndLabel() {
		String ret = "";
		String[] days = {"","Sun","Mon","Tue","Wed","Thu","Fri","Sat"};
		String[] months = {"Jan","Feb","Mar","Apr","May","Jun","Jul","Aug","Sep","Oct","Nov","Dec"};
		Calendar end = Calendar.getInstance();
		end.setTime(this.endDate);
		int month = end.get(Calendar.MONTH);
		int day_of_week = end.get(Calendar.DAY_OF_WEEK);
		if(this.getReportingPeriodGroup().equals("d"))
			ret = days[day_of_week];
		if(this.getReportingPeriodGroup().equals("w"))
			ret = months[month]+" "+end.get(Calendar.DATE);
		if(this.getReportingPeriodGroup().equals("m"))
			ret = months[month];
		return ret;
	}

	public String getReportingPeriodGroup() {
		return reportingPeriodGroup;
	}

	public void setReportingPeriodGroup(String reportingPeriodGroup) {
		this.reportingPeriodGroup = reportingPeriodGroup;
	}

	public Double getTotal() {
		return total;
	}

	public void setTotal(Double total) {
		this.total = total;
	}
	
	public String getSummary() {
		String ret = "";
		if(this.getReportingPeriodGroup().equals("d")) {
			Calendar startDateCal = Calendar.getInstance();
			startDateCal.setTime(this.getStartDate());
			ret = getStartLabel() + " " + startDateCal.get(Calendar.MONTH) + "/" + startDateCal.get(Calendar.DATE);
		}
		if(this.getReportingPeriodGroup().equals("w")) {
			ret = getStartLabel() +  " - "  + getEndLabel();
		}
		if(this.getReportingPeriodGroup().equals("m")) {
			Calendar startDateCal = Calendar.getInstance();
			startDateCal.setTime(this.getStartDate());
			ret = getStartLabel() + " " + startDateCal.get(Calendar.YEAR);
		}
		return ret;
	}
}
