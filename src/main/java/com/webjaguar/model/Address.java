/* Copyright 2005, 2006 Advanced E-Media Solutions
 *
 */

package com.webjaguar.model;

import java.io.Serializable;

public class Address implements Comparable, Serializable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private Integer id;
	private Integer userId;
	private String firstName;
	private String lastName;
	private String company;
	private String addr1;
	private String addr2;
	private String city;
	private String stateProvince;
	private boolean stateProvinceNA;
	private String zip;
	private String country;
	private String phone;
	private String cellPhone;
	private Integer mobileCarrierId;
	private String fax;
	private String email;	
	private boolean primary;
	private boolean residential = false;  //By default is Commercial 
	private boolean liftGate;
	private String code;
	private boolean defaultShipping;
	
	// supplier
	private Integer supplierId;


	public boolean isLiftGate()
	{
		return liftGate;
	}
	public void setLiftGate(boolean liftGate)
	{
		this.liftGate = liftGate;
	}
	public boolean isResidential()
	{
		return residential;
	}
	public void setResidential(boolean residential)
	{
		this.residential = residential;
	}
	public Integer getId() { return id; }
	public void setId(Integer id) {
		this.id = id;
	}

	public Integer getUserId() { return userId; }
	public void setUserId(Integer userId) {
		this.userId = userId;
	}	
	
	public String getFirstName() { return firstName; }
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}
	
	public String getLastName() { return lastName; }
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}
	
	public String getCompany() { return company; }
	public void setCompany(String company) {
		this.company = company;
	}	


	public String getAddr1() { return addr1; }
	public void setAddr1(String addr1) {
		this.addr1 = addr1;
	}
	
	public String getAddr2() { return addr2; }
	public void setAddr2(String addr2) {
		this.addr2 = addr2;
	}
	
	public String getCity() { return city; }
	public void setCity(String city) {
		this.city = city;
	}
	
	public String getStateProvince() { return stateProvince; }
	public void setStateProvince(String stateProvince) {
		this.stateProvince = stateProvince;
	}
	
	public String getZip() { return zip; }
	public void setZip(String zip) {
		this.zip = zip;
	}

	public String getCountry() { return country; }
	public void setCountry(String country) {
		this.country = country;
	}
	
	public String getPhone() { return phone; }
	public void setPhone(String phone) {
		this.phone = phone;
	}

	public String getFax() { return fax; }
	public void setFax(String fax) { 
		this.fax = fax;
	}
	public boolean isPrimary() {
		return primary;
	}
	public void setPrimary(boolean primary) {
		this.primary = primary;
	}
	public String getCellPhone()
	{
		return cellPhone;
	}
	public void setCellPhone(String cellPhone)
	{
		this.cellPhone = cellPhone;
	}
	public String getEmail()
	{
		return email;
	}
	public void setEmail(String email)
	{
		this.email = email;
	}
	public String getResToString()
	{
		if (this.isResidential())
			return "Residential";
		else
			return "Commercial";
	}
	public String getLiftgateToString()
	{
		if ( this.isLiftGate())
			return "Liftgate needed";
		else
			return "";
	}
	public boolean isStateProvinceNA()
	{
		return stateProvinceNA;
	}
	public void setStateProvinceNA(boolean stateProvinceNA)
	{
		this.stateProvinceNA = stateProvinceNA;
	}
	public Integer getSupplierId() {
		return supplierId;
	}
	public void setSupplierId(Integer supplierId) {
		this.supplierId = supplierId;
	}

	public void replace(String target, String replacement) {
		if (replacement != null) {
			if (this.firstName != null) this.firstName = this.firstName.replace(target, replacement);				
			if (this.lastName != null) this.lastName = this.lastName.replace(target, replacement);
			if (this.company != null) this.company = this.company.replace(target, replacement);
			if (this.addr1 != null) this.addr1 = this.addr1.replace(target, replacement);
			if (this.addr2 != null) this.addr2 = this.addr2.replace(target, replacement);
			if (this.city != null) this.city = this.city.replace(target, replacement);
			if (this.stateProvince != null) this.stateProvince = this.stateProvince.replace(target, replacement);
			if (this.zip != null) this.zip = this.zip.replace(target, replacement);
			if (this.country != null) this.country = this.country.replace(target, replacement);			
			if (this.phone != null) this.phone = this.phone.replace(target, replacement);			
			if (this.cellPhone != null) this.cellPhone = this.cellPhone.replace(target, replacement);			
			if (this.fax != null) this.fax = this.fax.replace(target, replacement);			
		}
	}
	
	public int compareTo(Object obj) {
		if (obj instanceof Address) {
			Address address = (Address) obj;
			if ( this.addr1.equals( address.getAddr1() ) &&
					this.city.equals( address.getCity() ) &&
					this.stateProvince.equals( address.getStateProvince() ) &&
					this.zip.equals( address.getZip() ) &&
					this.country.equals( address.getCountry() )) {
				return 1;
			} else 
				return 0;
		}
		return 0;
	}
	
	public String getStringAddress() {
		return this.company + "\n\n" + this.addr1 + " " + this.addr2 + "\n" +
			this.city + " " + this.zip + " " + this.country + "\n" +
			"Tel: " + this.phone + "\n" + 
			"Fax: " + this.fax;
	}
	public String getCode() {
		return code;
	}
	public void setCode(String code) {
		this.code = code;
	}
	public boolean isDefaultShipping() {
		return defaultShipping;
	}
	public void setDefaultShipping(boolean defaultShipping) {
		this.defaultShipping = defaultShipping;
	}
	public Integer getMobileCarrierId() {
		return mobileCarrierId;
	}
	public void setMobileCarrierId(Integer mobileCarrierId) {
		this.mobileCarrierId = mobileCarrierId;
	}

}
