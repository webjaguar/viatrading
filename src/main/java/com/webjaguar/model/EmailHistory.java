package com.webjaguar.model;

import java.util.Date;

public class EmailHistory {

	private Integer messageId;
	private Integer crmContactId;
	private Integer userId;
	private Date created;
	
	public Integer getMessageId() {
		return messageId;
	}
	public void setMessageId(Integer messageId) {
		this.messageId = messageId;
	}
	public Integer getCrmContactId() {
		return crmContactId;
	}
	public void setCrmContactId(Integer crmContactId) {
		this.crmContactId = crmContactId;
	}
	public Integer getUserId() {
		return userId;
	}
	public void setUserId(Integer userId) {
		this.userId = userId;
	}
	public Date getCreated() {
		return created;
	}
	public void setCreated(Date created) {
		this.created = created;
	}
	
}
