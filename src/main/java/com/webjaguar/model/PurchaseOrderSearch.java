/* Copyright 2008 Advanced E-Media Solutions
 * @author Shahin Naji
 */

package com.webjaguar.model;

import java.util.Date;

public class PurchaseOrderSearch {

	// sort
	private String sort = "created DESC";
	public String getSort() { return sort; }
	public void setSort(String sort) { this.sort = sort; }	
	
	// page 
	private int page = 1;
	public int getPage() { return page; }
	public void setPage(int page) { this.page = page; }
	
	// page size
	private int pageSize = 10;
	public int getPageSize() { return pageSize; }
	public void setPageSize(int pageSize) { this.pageSize = pageSize; }
	
	// purchase order number
	private String poNumber = "";
	public String getPoNumber() { return poNumber; }
	public void setPoNumber(String poNumber) { this.poNumber = poNumber; }
	
	// supplier
	private String supplier;
	public String getSupplier() { return supplier; }
	public void setSupplier(String supplier) { this.supplier = supplier; }
	
	// order by
	private String orderBy;
	public String getOrderBy() { return orderBy; }
	public void setOrderBy(String orderBy) { this.orderBy = orderBy; }
	
	// status
	private String status = "pend";
	public String getStatus() { return status; }
	public void setStatus(String status) { this.status = status; }
	
	// statusTemp
	private String statusTemp;
	public String getStatusTemp() { return statusTemp; }
	public void setStatusTemp(String statusTemp) { this.statusTemp = statusTemp; }
	
	// sku
	private String sku;
	public String getSku() { return sku; }
	public void setSku(String sku) { this.sku = sku; }
	
	// ship via
	private String shipVia;
	public String getShipVia() { return shipVia; }
	public void setShipVia(String shipVia) { this.shipVia = shipVia; }
	
	// contact info
	private String contactInfo;
	public String getContactInfo() { return contactInfo; }
	public void setContactInfo(String contactInfo) { this.contactInfo = contactInfo; }
	
	// start date
	private Date startDate; 
	public Date getStartDate() { return startDate; }
	public void setStartDate(Date startDate) { this.startDate = startDate; }
	
	// end date
	private Date endDate;
	public Date getEndDate() { return endDate; }
	public void setEndDate(Date endDate) { this.endDate = endDate; }
	
	// order id
	private Integer orderId;
	public Integer getOrderId() { return orderId; }
	public void setOrderId(Integer orderId) { this.orderId = orderId; }

	// pt
	private String pt;
	public String getPt() { return pt; }
	public void setPt(String pt) { this.pt = pt; }

	private String dateType = "PODate";
	public String getDateType() { return dateType; }
	public void setDateType(String dateType) { this.dateType = dateType; }
	
	// history status
	private String historyStatus = "";
	public String getHistoryStatus() { return historyStatus; }
	public void setHistoryStatus(String historyStatus) { this.historyStatus = historyStatus; }
}
