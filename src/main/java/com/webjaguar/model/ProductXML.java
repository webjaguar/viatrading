package com.webjaguar.model;

import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlSeeAlso;

@XmlRootElement(name = "Products")
@XmlAccessorType(XmlAccessType.FIELD)
@XmlSeeAlso({Product.class, IncludedProduct.class})
public class ProductXML {
	@XmlElement(name = "token", required = false)
	private String token;
	private List<Product> product;
	
	public ProductXML() {
		this.product = new ArrayList();
	}
	public void setToken(String token) {
		this.token = token;
	}
	public String getToken() {
		return token;
	}
	public void setProduct(List<Product> product) {
		this.product = product;
	}
	public List<Product> getProduct() {
		return product;
	}
	public void addToList(List<Product> list) {
		this.product.addAll(list);
	}
}