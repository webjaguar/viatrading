/*
 * Copyright 2005, 2006 Advanced E-Media Solutions
 * 
 */

package com.webjaguar.model;

import java.math.BigDecimal;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;

import com.webjaguar.web.domain.Utilities;

@XmlRootElement(name = "Price")
@XmlAccessorType(XmlAccessType.FIELD)
public class Price
{
	private Double amt;
	private Double discountAmt;
	private Integer qtyFrom;
	private Integer qtyTo;
	private Double caseAmt;
	private Double caseDiscountAmt;
	private Double cost;
	
	private Double dicscountAmt;
	
	public Price() {
		super();
	}
	
	public Price( Double amt, Double caseAmt, SalesTag salesTag, Integer qtyFrom, Integer qtyTo, Double cost )
	{
		this.amt = amt;
		this.cost = cost;
		this.caseAmt = caseAmt;
		if ( salesTag != null && salesTag.getDiscount() != null && amt != null ) {
			if (salesTag.isPercent()) {
				this.discountAmt = amt - (amt * salesTag.getDiscount() / 100);
				if (caseAmt != null) {
					this.caseDiscountAmt = caseAmt - (caseAmt * salesTag.getDiscount() / 100);
				}
			}
			else {
				this.discountAmt = ((amt - salesTag.getDiscount()) < 0 ) ? 0.0 : amt - salesTag.getDiscount();
				if (caseAmt != null) {
					this.caseDiscountAmt = ((caseAmt - salesTag.getDiscount()) < 0 ) ? 0.0 : caseAmt - salesTag.getDiscount();
				}
			}
		}
		this.qtyFrom = qtyFrom;
		if ( qtyTo != null ) {
			this.qtyTo = new Integer( qtyTo.intValue() - 1 );
		}
	}

	public Double getAmt()
	{
		return Utilities.roundFactory(amt,2,BigDecimal.ROUND_HALF_UP);
	}

	public void setAmt(Double amt)
	{
		this.amt = amt;
	}

	public Integer getQtyFrom()
	{
		return qtyFrom;
	}

	public void setQtyFrom(Integer qtyFrom)
	{
		this.qtyFrom = qtyFrom;
	}

	public Integer getQtyTo()
	{
		return qtyTo;
	}

	public void setQtyTo(Integer qtyTo)
	{
		this.qtyTo = qtyTo;
	}

	public Double getDiscountAmt()
	{
		return Utilities.roundFactory(discountAmt,2,BigDecimal.ROUND_HALF_UP);
	}

	public void setDiscountAmt(Double discountAmt)
	{
		this.discountAmt = discountAmt;
	}

	public Double getCaseAmt() {
		return Utilities.roundFactory(caseAmt,2,BigDecimal.ROUND_HALF_UP);
	}

	public void setCaseAmt(Double caseAmt) {
		this.caseAmt = caseAmt;
	}

	public Double getCaseDiscountAmt() {
		return Utilities.roundFactory(caseDiscountAmt,2,BigDecimal.ROUND_HALF_UP);
	}

	public void setCaseDiscountAmt(Double caseDiscountAmt) {
		this.caseDiscountAmt = caseDiscountAmt;
	}

	public void setCost(Double cost) {
		this.cost = cost;
	}

	public Double getCost() {
		return cost;
	}

	public Double getDicscountAmt() {
		return dicscountAmt;
	}

	public void setDicscountAmt(Double dicscountAmt) {
		this.dicscountAmt = dicscountAmt;
	}
	
	
}