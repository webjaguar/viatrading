/*
 * Copyright 2008 Advanced E-Media Solutions
 * 
 */

package com.webjaguar.model;

import java.sql.Date;

public class OrderReport
{

	private Date dateOrdered;
	private Double subTotal;
	private Double grandTotal;
	private Integer count;
	public Date getDateOrdered()
	{
		return dateOrdered;
	}
	public void setDateOrdered(Date dateOrdered)
	{
		this.dateOrdered = dateOrdered;
	}
	public Double getSubTotal()
	{
		return subTotal;
	}
	public void setSubTotal(Double subTotal)
	{
		this.subTotal = subTotal;
	}
	public Double getGrandTotal()
	{
		return grandTotal;
	}
	public void setGrandTotal(Double grandTotal)
	{
		this.grandTotal = grandTotal;
	}
	public Integer getCount()
	{
		return count;
	}
	public void setCount(Integer count)
	{
		this.count = count;
	}

}
