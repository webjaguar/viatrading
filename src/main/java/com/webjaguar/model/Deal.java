package com.webjaguar.model;

import java.io.Serializable;
import java.util.Date;

public class Deal implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private Integer dealId;
	private String title;
	private String buySku;
	private Double buyAmount;
	private Integer buyQuantity;
	private String getSku;
	private Integer getQuantity;
	private Double discount;
	private boolean discountType;
	private Date startDate;
	private Date endDate;
	private String htmlCode;
	private boolean parentSku;
	private String parentHtmlCode;
	
	public Deal() {
		super();
	}
	
	public Deal( String title ) {
		super();
		this.title = title;
	}

	public boolean isInEffect() {
	// today's date
	com.webjaguar.model.DateBean today = new com.webjaguar.model.DateBean( );

	int start = this.startDate.compareTo( today.getTime() );
	int end = this.endDate.compareTo( today.getTime() );
	if ( start == 0 )
		return true;
	else if ( start > 0 || end < 0  )
		return false;
	else
		return true;
	}
	
	public Integer getDealId() {
		return dealId;
	}

	public void setDealId(Integer dealId) {
		this.dealId = dealId;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}


	public void setBuySku(String buySku) {
		this.buySku = buySku;
	}

	public String getBuySku() {
		return buySku;
	}

	public void setBuyAmount(Double buyAmount) {
		this.buyAmount = buyAmount;
	}

	public Double getBuyAmount() {
		return buyAmount;
	}

	public void setBuyQuantity(Integer buyQuantity) {
		this.buyQuantity = buyQuantity;
	}

	public Integer getBuyQuantity() {
		return buyQuantity;
	}

	public String getGetSku() {
		return getSku;
	}

	public void setGetSku(String getSku) {
		this.getSku = getSku;
	}

	public void setGetQuantity(Integer getQuantity) {
		this.getQuantity = getQuantity;
	}

	public Integer getGetQuantity() {
		return getQuantity;
	}

	public Double getDiscount() {
		return discount;
	}

	public void setDiscount(Double discount) {
		this.discount = discount;
	}

	public void setDiscountType(boolean discountType) {
		this.discountType = discountType;
	}

	public boolean isDiscountType() {
		return discountType;
	}

	public Date getStartDate() {
		return startDate;
	}

	public void setStartDate(Date startDate) {
		this.startDate = startDate;
	}

	public Date getEndDate() {
		return endDate;
	}

	public void setEndDate(Date endDate) {
		this.endDate = endDate;
	}

	public void setHtmlCode(String htmlCode) {
		this.htmlCode = htmlCode;
	}

	public String getHtmlCode() {
		return htmlCode;
	}

	public boolean isParentSku() {
		return parentSku;
	}

	public void setParentSku(boolean parentSku) {
		this.parentSku = parentSku;
	}

	public String getParentHtmlCode() {
		return parentHtmlCode;
	}

	public void setParentHtmlCode(String parentHtmlCode) {
		this.parentHtmlCode = parentHtmlCode;
	}

}