/* Copyright 2007 Advanced E-Media Solutions
 * @author Eduardo Asprec
 * @since 09.27.2007
 */

package com.webjaguar.model;

import java.sql.Time;
import java.sql.Timestamp;
import java.util.Date;

public class ServiceWork {
	
	private Integer serviceNum;
	private ServiceableItem item;
	private String status;	
	private String type;
	private String comments;
	private Timestamp reportDate;
	private Timestamp lastModified;
	private Date completeDate;
	private boolean hasWorkOrder;
	private Integer orderId;
	private String problem;
	// PDF URL (signed, etc.)
	private String pdfUrl;
	private String servicePdfUrl;
	private String host;
	private String purchaseOrder;
	private String purchaseOrderPdfUrl;

	// tracking number
	private String trackNumIn;
	private String trackNumInCarrier;
	private String trackNumOut;
	private String trackNumOutCarrier;
	
	private String itemFrom;
	private String itemTo;
	private Date shipDate;	
	
	private Date serviceDate;
	private Time startTime;
	private String technician;
	private Boolean printed;
	
	private String notes;
	private String notesTemp;
	
	private int priority;
	
	private String wo3rd;
	private String wo3rdPdfUrl;
	private String poSupplier;
	private String notes2;
	private String refNum;
	private String manufWarranty;
	
	private String alternateContact;
	
	public Integer getServiceNum()
	{
		return serviceNum;
	}
	public void setServiceNum(Integer serviceNum)
	{
		this.serviceNum = serviceNum;
	}
	public ServiceableItem getItem()
	{
		return item;
	}
	public void setItem(ServiceableItem item)
	{
		this.item = item;
	}
	public String getStatus()
	{
		return status;
	}
	public void setStatus(String status)
	{
		this.status = status;
	}
	public Timestamp getReportDate()
	{
		return reportDate;
	}
	public void setReportDate(Timestamp reportDate)
	{
		this.reportDate = reportDate;
	}
	public Timestamp getLastModified()
	{
		return lastModified;
	}
	public void setLastModified(Timestamp lastModified)
	{
		this.lastModified = lastModified;
	}
	public Date getCompleteDate()
	{
		return completeDate;
	}
	public void setCompleteDate(Date completeDate)
	{
		this.completeDate = completeDate;
	}
	public String getType()
	{
		return type;
	}
	public void setType(String type)
	{
		this.type = type;
	}
	public String getComments()
	{
		return comments;
	}
	public void setComments(String comments)
	{
		this.comments = comments;
	}
	public boolean isHasWorkOrder()
	{
		return hasWorkOrder;
	}
	public void setHasWorkOrder(boolean hasWorkOrder)
	{
		this.hasWorkOrder = hasWorkOrder;
	}
	public String getProblem()
	{
		return problem;
	}
	public void setProblem(String problem)
	{
		this.problem = problem;
	}
	public Integer getOrderId()
	{
		return orderId;
	}
	public void setOrderId(Integer orderId)
	{
		this.orderId = orderId;
	}
	public String getPdfUrl()
	{
		return pdfUrl;
	}
	public void setPdfUrl(String pdfUrl)
	{
		this.pdfUrl = pdfUrl;
	}
	public String getServicePdfUrl() {
		return servicePdfUrl;
	}
	public void setServicePdfUrl(String servicePdfUrl) {
		this.servicePdfUrl = servicePdfUrl;
	}
	public String getHost()
	{
		return host;
	}
	public void setHost(String host)
	{
		this.host = host;
	}
	public String getPurchaseOrder()
	{
		return purchaseOrder;
	}
	public void setPurchaseOrder(String purchaseOrder)
	{
		this.purchaseOrder = purchaseOrder;
	}
	public String getPurchaseOrderPdfUrl() {
		return purchaseOrderPdfUrl;
	}
	public void setPurchaseOrderPdfUrl(String purchaseOrderPdfUrl) {
		this.purchaseOrderPdfUrl = purchaseOrderPdfUrl;
	}
	public String getTrackNumIn() {
		return trackNumIn;
	}
	public void setTrackNumIn(String trackNumIn) {
		this.trackNumIn = trackNumIn;
	}
	public String getTrackNumInCarrier() {
		return trackNumInCarrier;
	}
	public void setTrackNumInCarrier(String trackNumInCarrier) {
		this.trackNumInCarrier = trackNumInCarrier;
	}
	public String getTrackNumOut() {
		return trackNumOut;
	}
	public void setTrackNumOut(String trackNumOut) {
		this.trackNumOut = trackNumOut;
	}
	public String getTrackNumOutCarrier() {
		return trackNumOutCarrier;
	}
	public void setTrackNumOutCarrier(String trackNumOutCarrier) {
		this.trackNumOutCarrier = trackNumOutCarrier;
	}
	public String getItemFrom() {
		return itemFrom;
	}
	public void setItemFrom(String itemFrom) {
		this.itemFrom = itemFrom;
	}
	public String getItemTo() {
		return itemTo;
	}
	public void setItemTo(String itemTo) {
		this.itemTo = itemTo;
	}
	public Date getShipDate() {
		return shipDate;
	}
	public void setShipDate(Date shipDate) {
		this.shipDate = shipDate;
	}
	public Date getServiceDate() {
		return serviceDate;
	}
	public void setServiceDate(Date serviceDate) {
		this.serviceDate = serviceDate;
	}
	public Time getStartTime() {
		return startTime;
	}
	public void setStartTime(Time startTime) {
		this.startTime = startTime;
	}
	public void setTechnician(String technician) {
		this.technician = technician;
	}
	public String getTechnician() {
		return technician;
	}
	public Boolean getPrinted() {
		return printed;
	}
	public void setPrinted(Boolean printed) {
		this.printed = printed;
	}
	public String getNotes() {
		return notes;
	}
	public void setNotes(String notes) {
		this.notes = notes;
	}
	public String getNotesTemp() {
		return notesTemp;
	}
	public void setNotesTemp(String notesTemp) {
		this.notesTemp = notesTemp;
	}
	public int getPriority() {
		return priority;
	}
	public void setPriority(int priority) {
		this.priority = priority;
	}
	public String getWo3rd() {
		return wo3rd;
	}
	public void setWo3rd(String wo3rd) {
		this.wo3rd = wo3rd;
	}
	public String getWo3rdPdfUrl() {
		return wo3rdPdfUrl;
	}
	public void setWo3rdPdfUrl(String wo3rdPdfUrl) {
		this.wo3rdPdfUrl = wo3rdPdfUrl;
	}
	public String getPoSupplier() {
		return poSupplier;
	}
	public void setPoSupplier(String poSupplier) {
		this.poSupplier = poSupplier;
	}
	public String getNotes2() {
		return notes2;
	}
	public void setNotes2(String notes2) {
		this.notes2 = notes2;
	}
	public String getRefNum() {
		return refNum;
	}
	public void setRefNum(String refNum) {
		this.refNum = refNum;
	}
	public String getManufWarranty() {
		return manufWarranty;
	}
	public void setManufWarranty(String manufWarranty) {
		this.manufWarranty = manufWarranty;
	}
	public String getAlternateContact() {
		return alternateContact;
	}
	public void setAlternateContact(String alternateContact) {
		this.alternateContact = alternateContact;
	}


}
