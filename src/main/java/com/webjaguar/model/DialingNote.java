package com.webjaguar.model;

import java.sql.Timestamp;



public class DialingNote {
	private Integer id;
	private String note;
	private Timestamp created;
	private Integer crmId;
	private Integer customerId;
	
	
	public String getNote() {
		return note;
	}
	public void setNote(String note) {
		this.note = note;
	}
	public Timestamp getCreated() {
		return created;
	}
	public void setCreated(Timestamp created) {
		this.created = created;
	}
	public Integer getCrmId() {
		return crmId;
	}
	public void setCrmId(Integer crmId) {
		this.crmId = crmId;
	}
	public Integer getCustomerId() {
		return customerId;
	}
	public void setCustomerId(Integer customerId) {
		this.customerId = customerId;
	}
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
}
