/* Copyright 2007 Advanced E-Media Solutions
 * @author Eduardo Asprec
 * @since 09.21.2007
 */

package com.webjaguar.model;

public class ServiceableItem {
	
	private Integer id;
	private String itemId;	
	private String serialNum;
	private String sku;
	private Customer customer;
	private Address address;
	private Double serviceTotal;
	
	// active
	private boolean active = true;
	
	private String alternateContacts;
	
	public Integer getId()
	{
		return id;
	}
	public void setId(Integer id)
	{
		this.id = id;
	}
	public String getItemId()
	{
		return itemId;
	}
	public void setItemId(String itemId)
	{
		this.itemId = itemId;
	}
	public String getSerialNum()
	{
		return serialNum;
	}
	public void setSerialNum(String serialNum)
	{
		this.serialNum = serialNum;
	}
	public String getSku()
	{
		return sku;
	}
	public void setSku(String sku)
	{
		this.sku = sku;
	}
	public Customer getCustomer()
	{
		return customer;
	}
	public void setCustomer(Customer customer)
	{
		this.customer = customer;
	}
	public Address getAddress()
	{
		return address;
	}
	public void setAddress(Address address)
	{
		this.address = address;
	}
	public Double getServiceTotal()
	{
		return serviceTotal;
	}
	public void setServiceTotal(Double serviceTotal)
	{
		this.serviceTotal = serviceTotal;
	}
	public boolean isActive() {
		return active;
	}
	public void setActive(boolean active) {
		this.active = active;
	}
	public String getAlternateContacts() {
		return alternateContacts;
	}
	public void setAlternateContacts(String alternateContacts) {
		this.alternateContacts = alternateContacts;
	}
	
}
