package com.webjaguar.model;

import java.util.Date;

public class PresentationTemplate {
	private Integer id;
	private String name;
	private Integer noOfProduct;
	private String design;
	private Date created;
	private Date modified;
	private String footerHtmlCode;
	
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public Integer getNoOfProduct() {
		return noOfProduct;
	}
	public void setNoOfProduct(Integer noOfProduct) {
		this.noOfProduct = noOfProduct;
	}
	public String getDesign() {
		return design;
	}
	public void setDesign(String design) {
		this.design = design;
	}
	public Date getCreated() {
		return created;
	}
	public void setCreated(Date created) {
		this.created = created;
	}
	public Date getModified() {
		return modified;
	}
	public void setModified(Date modified) {
		this.modified = modified;
	}
	public String getFooterHtmlCode() {
		return footerHtmlCode;
	}
	public void setFooterHtmlCode(String footerHtmlCode) {
		this.footerHtmlCode = footerHtmlCode;
	}

}
