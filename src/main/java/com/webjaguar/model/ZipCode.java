package com.webjaguar.model;

public class ZipCode
{
	private String zipCode;
	private String stateAbbv;
	private Double latitude;
	private Double longitude;
	private String city;
	private String county;
	private String zipClass;
	private Double distance;
	
	public String getZipCode()
	{
		return zipCode;
	}
	public void setZipCode(String zipCode)
	{
		this.zipCode = zipCode;
	}
	public String getStateAbbv()
	{
		return stateAbbv;
	}
	public void setStateAbbv(String stateAbbv)
	{
		this.stateAbbv = stateAbbv;
	}
	public Double getLatitude()
	{
		return latitude;
	}
	public void setLatitude(Double latitude)
	{
		this.latitude = latitude;
	}
	public Double getLongitude()
	{
		return longitude;
	}
	public void setLongitude(Double longitude)
	{
		this.longitude = longitude;
	}
	public String getCity()
	{
		return city;
	}
	public void setCity(String city)
	{
		this.city = city;
	}
	public String getCounty()
	{
		return county;
	}
	public void setCounty(String county)
	{
		this.county = county;
	}
	public String getZipClass()
	{
		return zipClass;
	}
	public void setZipClass(String zipClass)
	{
		this.zipClass = zipClass;
	}
	public Double getDistance()
	{
		return distance;
	}
	public void setDistance(Double distance)
	{
		this.distance = distance;
	}
	
}
