/* Copyright 2007 Advanced E-Media Solutions
 * @author Eduardo Asprec
 * @since 10.16.07
 */

package com.webjaguar.model;

import java.sql.Timestamp;
import java.util.List;

public class Option implements Comparable<Option>{
	private Integer id;
	private String code;
	private List<ProductOption> productOptions;
	private Integer productCount;
	private Timestamp created;
	private Timestamp lastModified;
	private String oldCode;
	// protected
	private String protectedLevel = "0";
	
	public Integer getId()
	{
		return id;
	}
	public void setId(Integer id)
	{
		this.id = id;
	}
	public String getCode()
	{
		return code;
	}
	public void setCode(String code)
	{
		this.code = code;
	}
	public List<ProductOption> getProductOptions()
	{
		return productOptions;
	}
	public void setProductOptions(List<ProductOption> productOptions)
	{
		this.productOptions = productOptions;
	}
	public Integer getProductCount()
	{
		return productCount;
	}
	public void setProductCount(Integer productCount)
	{
		this.productCount = productCount;
	}
	public Timestamp getCreated()
	{
		return created;
	}
	public void setCreated(Timestamp created)
	{
		this.created = created;
	}
	public Timestamp getLastModified()
	{
		return lastModified;
	}
	public void setLastModified(Timestamp lastModified)
	{
		this.lastModified = lastModified;
	}
	public String getOldCode()
	{
		return oldCode;
	}
	public void setOldCode(String oldCode)
	{
		this.oldCode = oldCode;
	}
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Option other = (Option) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}
	public int compareTo(Option o) {
		return this.id - o.id;
	}
	public String getProtectedLevel() {
		return protectedLevel;
	}
	public void setProtectedLevel(String protectedLevel) {
		this.protectedLevel = protectedLevel;
	}
	public Integer getProtectedLevelAsNumber() {
		if (protectedLevel.contains( "1" )) {
			return protectedLevel.length();
		} else {
			return 0;
		}
	}

}
