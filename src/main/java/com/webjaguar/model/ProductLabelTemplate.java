package com.webjaguar.model;

public class ProductLabelTemplate {
	
	private int id;
	private boolean active = true;
	private String name;
	private String row1;
	private String row2;
	private String row3;
	private String row4;
	private String row5;
	private String row6;
	private String topLeft;
	private String topRight;
	private String bottomLeft;
	private String bottomRight;
	private float fontSize;
	private int bold;
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public boolean isActive() {
		return active;
	}
	public void setActive(boolean active) {
		this.active = active;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getRow1() {
		return row1;
	}
	public void setRow1(String row1) {
		this.row1 = row1;
	}
	public String getRow2() {
		return row2;
	}
	public void setRow2(String row2) {
		this.row2 = row2;
	}
	public String getRow3() {
		return row3;
	}
	public void setRow3(String row3) {
		this.row3 = row3;
	}
	public String getRow4() {
		return row4;
	}
	public void setRow4(String row4) {
		this.row4 = row4;
	}
	public String getRow5() {
		return row5;
	}
	public void setRow5(String row5) {
		this.row5 = row5;
	}
	public String getRow6() {
		return row6;
	}
	public void setRow6(String row6) {
		this.row6 = row6;
	}
	public String getTopLeft() {
		return topLeft;
	}
	public void setTopLeft(String topLeft) {
		this.topLeft = topLeft;
	}
	public String getTopRight() {
		return topRight;
	}
	public void setTopRight(String topRight) {
		this.topRight = topRight;
	}
	public String getBottomLeft() {
		return bottomLeft;
	}
	public void setBottomLeft(String bottomLeft) {
		this.bottomLeft = bottomLeft;
	}
	public String getBottomRight() {
		return bottomRight;
	}
	public void setBottomRight(String bottomRight) {
		this.bottomRight = bottomRight;
	}
	public float getFontSize() {
		return fontSize;
	}
	public void setFontSize(float fontSize) {
		this.fontSize = fontSize;
	}
	public int getBold() {
		return bold;
	}
	public void setBold(int bold) {
		this.bold = bold;
	}
}
