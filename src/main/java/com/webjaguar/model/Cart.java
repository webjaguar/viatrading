/* Copyright 2005, 2006 Advanced E-Media Solutions
 *
 */

package com.webjaguar.model;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class Cart {
	
	private List<CartItem> cartItems = new ArrayList<CartItem>();
	private List<String> cartContents = new ArrayList<String>();
	private boolean continueCart = true;
	private Integer userId;
	private String minOrderMessage;
	private String minIncMessage;
	private Double minOrderAmt;
	private boolean hasManufacturer;
	private String cartId;
	private Boolean wantsBond;
	private Double bondCost;
	private Promo promo;
	private String promoCode;
	private String tempPromoErrorMessage;
	private String productEventValidationMessage;
	private String rewardPointsErrorMessage;

	
	private boolean hasParentDeal;
	
	public boolean containsCartItem(CartItem cartItem) {
		int index = 0;
		for (String cartContent : this.cartContents) {
			if (cartContent.equals(cartItem.toString())) {
				try{
					cartItem.setIndex(this.cartItems.get(index).getIndex());
					return true;
				}catch(Exception ex){
					
				}
			}
			index++;
		}
		return false;
	}
	  
	public int getNumberOfItems() { return cartItems.size(); }
	  
	public void addCartItem(CartItem cartItem) {		
		cartContents.add(cartItem.toString());
		cartItems.add(cartItem);
	}

	public void addCartItem(int index, CartItem cartItem) {		
		cartContents.add(index, cartItem.toString());
		cartItems.add(index, cartItem);
	}

	public List<CartItem> getCartItems() {
		return cartItems;
	}
	
	public double getSubTotal() {
		double subTotal = 0;
		for (CartItem cartItem:this.cartItems) {
			if (cartItem.getTotal() != null) {
				subTotal += cartItem.getTotal().doubleValue();
			} else if ( cartItem.getVariableUnitPrice() != null ) {
				subTotal += cartItem.getVariableUnitPrice().doubleValue();
			}
		}
		subTotal = Math.floor( (subTotal - this.getPromoAmount()) * 100.0 + 0.5 ) / 100.0;
		
		return subTotal;
	}
	
	public double getWeight() {
		double totalWeight = 0;
		for (CartItem cartItem:this.cartItems) {
			if (cartItem.getTotalWeight() != null )
				totalWeight += cartItem.getTotalWeight().doubleValue();
		}
		return totalWeight;
	}
	
	public int getQuantity() {
		int totalQuantity = 0;
		for (CartItem cartItem:this.cartItems) {
			totalQuantity += cartItem.getQuantity();
		}
		return totalQuantity;
	}
	
	public void incrementQuantityByCartItem(CartItem cartItem) {
		int index = 0;
		for (String cartContent:this.cartContents) {
			if (cartContent.equals(cartItem.toString())) {
				this.cartItems.get(index).incrementQuantity(cartItem.getQuantity());
				// custom lines
				if (cartItem.getCustomLines() != null) {
					for (String customLine: cartItem.getCustomLines().keySet()) {
						this.cartItems.get(index).addCustomLine(customLine, cartItem.getCustomLines().get(customLine));
					}
				}
				return;
			}
			index++;
		}		
	}
	
	public void incrementQuantityByCartItem2(CartItem cartItem) {
		int index = 0;
		for (String cartContent:this.cartContents) {
			if (cartContent.equals(cartItem.toString())) {
				this.cartItems.get(index).incrementQuantity2(cartItem.getQuantity());
				if(this.cartItems.get(index).getQuantity()<1){
					try{
						removeCartItem(index);
					}catch(Exception ex){
					}
				}
				return;
			}
			index++;
		}		
	}

	public void removeCartItem(int index) throws Exception {
		cartContents.remove(index);
		cartItems.remove(index);
	}	

	
	public static Double unitPriceByQty(int qty, Product product) {
		Double unitPriceByQty = null;

		Iterator<Price> iter = product.getPrice().iterator();
		while (iter.hasNext()) {
			Price price = iter.next();
			if ( price.getDiscountAmt() == null  ){
				unitPriceByQty = price.getAmt();
			}
			else {
				unitPriceByQty = price.getDiscountAmt();
			}
			if (!product.isEndQtyPricing() && (price.getQtyTo() == null || (price.getQtyTo().intValue() >= qty))) {
				break;
			}
		}
		if (unitPriceByQty != null && product.getSubscriptionDiscount() != null) {
			unitPriceByQty = unitPriceByQty - (unitPriceByQty * product.getSubscriptionDiscount() / 100);
		}
		
		return unitPriceByQty;
	}
	
	public boolean ishasContent() {
		for ( CartItem cartItem : cartItems ) {
			if ( cartItem.getProduct().getCaseContent() != null ) {
				return true;
			}
		}
		return false;
	}

	public boolean ishasPacking() {
		for ( CartItem cartItem : cartItems ) {
			if ( cartItem.getProduct().getPacking() != null && !cartItem.getProduct().getPacking().equals( "" ) ) {
				return true;
			}
		}
		return false;
	}
	
	public boolean isContinueCart()
	{
		return continueCart;
	}

	public void setContinueCart(boolean continueCart)
	{
		this.continueCart = continueCart;
	}
	public Integer getUserId()
	{
		return userId;
	}
	public void setUserId(Integer userId)
	{
		this.userId = userId;
	}
	public String getMinOrderMessage()
	{
		return minOrderMessage;
	}
	public void setMinOrderMessage(String minOrderMessage)
	{
		this.minOrderMessage = minOrderMessage;
	}
	public Double getMinOrderAmt() {
		return minOrderAmt;
	}
	public void setMinOrderAmt(Double minOrderAmt) {
		this.minOrderAmt = minOrderAmt;
	}

	public boolean isHasManufacturer()
	{
		return hasManufacturer;
	}

	public void setHasManufacturer(boolean hasManufacturer)
	{
		this.hasManufacturer = hasManufacturer;
	}

	public void setCartId(String cartId) {
		this.cartId = cartId;
	}

	public String getCartId() {
		return cartId;
	}

	public void setWantsBond(Boolean wantsBond) {
		this.wantsBond = wantsBond;
	}

	public Boolean getWantsBond() {
		return wantsBond;
	}

	public void setBondCost(Double bondCost) {
		this.bondCost = bondCost;
	}

	public Double getBondCost() {
		return bondCost;
	}

	public void setMinIncMessage(String minIncMessage) {
		this.minIncMessage = minIncMessage;
	}

	public String getMinIncMessage() {
		return minIncMessage;
	}
	
	public Promo getPromo() {
		return promo;
	}

	public void setPromo(Promo promo) {
		this.promo = promo;
	}

	public String getPromoCode() {
		return promoCode;
	}

	public void setPromoCode(String promoCode) {
		this.promoCode = promoCode;
	}

	public Double getPromoAmount() {
		
		double subTotal = 0.0;
		for (CartItem cartItem:this.cartItems) {
			if (cartItem.getTotal() != null) {
				subTotal += cartItem.getTotal().doubleValue();
			} else if ( cartItem.getVariableUnitPrice() != null ) {
				subTotal += cartItem.getVariableUnitPrice().doubleValue();
			}
		}
		
		double promoAmount = 0.0;
		if ( promo != null && promo.getDiscountType() != null ) {
			if ( promo.getDiscountType().equalsIgnoreCase("order") ) {
				if(promo.isPercent()){
					promoAmount = subTotal * ((promo.getDiscount() == null) ? 0.00 : promo.getDiscount() / 100.00);
				} else{
					promoAmount = (promo.getDiscount() == null) ? 0.00 : promo.getDiscount();
				}
			}
		}
		
		if(promoAmount  > subTotal){
			promoAmount = subTotal;
		}
		return promoAmount;
	}

	public void setTempPromoErrorMessage(String tempPromoErrorMessage) {
		this.tempPromoErrorMessage = tempPromoErrorMessage;
	}

	public String getTempPromoErrorMessage() {
		return tempPromoErrorMessage;
	}

	public boolean isHasParentDeal() {
		return hasParentDeal;
	}

	public void setHasParentDeal(boolean hasParentDeal) {
		this.hasParentDeal = hasParentDeal;
	}

	public String getProductEventValidationMessage() {
		return productEventValidationMessage;
	}

	public void setProductEventValidationMessage(String productEventValidationMessage) {
		this.productEventValidationMessage = productEventValidationMessage;
	}

	public String getRewardPointsErrorMessage() {
		return rewardPointsErrorMessage;
	}

	public void setRewardPointsErrorMessage(String rewardPointsErrorMessage) {
		this.rewardPointsErrorMessage = rewardPointsErrorMessage;
	}
	
	
	
}