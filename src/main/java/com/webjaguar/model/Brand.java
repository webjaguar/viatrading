/* Copyright 2008 Advanced E-Media Solutions
 * @author Eduardo Asprec
 * @since 12.03.2008
 */

package com.webjaguar.model;

import java.io.Serializable;

public class Brand implements Serializable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private int id;
	private String name;
	private String skuPrefix;
	private String optionName;
	private String valueName;
	private String valueTitle;	
	private Integer colspan1;
	private Integer colspan2;
	private Double budget;
	private Double total;
	
	public int getId()
	{
		return id;
	}
	public void setId(int id)
	{
		this.id = id;
	}
	public String getName()
	{
		return name;
	}
	public void setName(String name)
	{
		this.name = name;
	}
	public String getSkuPrefix()
	{
		return skuPrefix;
	}
	public void setSkuPrefix(String skuPrefix)
	{
		this.skuPrefix = skuPrefix;
	}
	public String getOptionName()
	{
		return optionName;
	}
	public void setOptionName(String optionName)
	{
		this.optionName = optionName;
	}
	public String getValueName()
	{
		return valueName;
	}
	public void setValueName(String valueName)
	{
		this.valueName = valueName;
	}
	public String getValueTitle()
	{
		return valueTitle;
	}
	public void setValueTitle(String valueTitle)
	{
		this.valueTitle = valueTitle;
	}
	public Integer getColspan1()
	{
		return colspan1;
	}
	public void setColspan1(Integer colspan1)
	{
		this.colspan1 = colspan1;
	}
	public Integer getColspan2()
	{
		return colspan2;
	}
	public void setColspan2(Integer colspan2)
	{
		this.colspan2 = colspan2;
	}
	public Double getBudget()
	{
		return budget;
	}
	public void setBudget(Double budget)
	{
		if (budget == null) {
			this.budget = 0.0;
		} else {
			this.budget = budget;			
		}
	}
	public Double getTotal()
	{
		return total;
	}
	public void setTotal(Double total)
	{
		this.total = total;
	}
	public Double getBalance() {
		Double balance = budget;
		if (total != null) {
			balance = budget - total;
			if (balance < 0) {
				balance = 0.0;
			}
		}
		
		return balance;
	}

}
