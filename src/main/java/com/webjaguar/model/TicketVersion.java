/*
 * Copyright 2007 Advanced E-Media Solutions
 * 
 */

package com.webjaguar.model;

import java.sql.Timestamp;

public class TicketVersion {
	private int ticketVersionId;
	private Integer ownerId;
	private String questionDescription;
	private String createdByType;
	private Integer accessUserId;
	private String createdBy;
	private Timestamp created;
	private String attachment;
	
	public int getTicketVersionId() {
		return ticketVersionId;
	}
	public void setTicketVersionId(int ticketVersionId) {
		this.ticketVersionId = ticketVersionId;
	}
	public Integer getOwnerId() {
		return ownerId;
	}
	public void setOwnerId(Integer ownerId) {
		this.ownerId = ownerId;
	}
	public String getQuestionDescription() {
		return questionDescription;
	}
	public void setQuestionDescription(String questionDescription) {
		this.questionDescription = questionDescription;
	}
	public String getCreatedByType() {
		return createdByType;
	}
	public void setCreatedByType(String createdByType) {
		this.createdByType = createdByType;
	}
	public Integer getAccessUserId() {
		return accessUserId;
	}
	public void setAccessUserId(Integer accessUserId) {
		this.accessUserId = accessUserId;
	}
	public Timestamp getCreated() {
		return created;
	}
	public void setCreated(Timestamp created) {
		this.created = created;
	}
	public String getCreatedBy() {
		return createdBy;
	}
	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}
	public void setAttachment(String attachment) {
		this.attachment = attachment;
	}
	public String getAttachment() {
		return attachment;
	}
}