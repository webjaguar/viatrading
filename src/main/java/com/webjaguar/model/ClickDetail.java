package com.webjaguar.model;

import java.util.Date;

public class ClickDetail {
	
	private Integer clickId;
	private Integer userId;
	private Integer contactId;
	private String email;
	private String ipAddress;
	private Date date;
	private Integer openedCount;
	private Integer customerSalesRepId;
	private String customerSalesRepName;
	private Integer contactAccessUserId;
	private Integer contactSalesRepId;
	private String contactSalesRepName;
	
	public Integer getClickId() {
		return clickId;
	}
	public void setClickId(Integer clickId) {
		this.clickId = clickId;
	}
	public Integer getUserId() {
		return userId;
	}
	public void setUserId(Integer userId) {
		this.userId = userId;
	}
	public Integer getContactId() {
		return contactId;
	}
	public void setContactId(Integer contactId) {
		this.contactId = contactId;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getIpAddress() {
		return ipAddress;
	}
	public void setIpAddress(String ipAddress) {
		this.ipAddress = ipAddress;
	}
	public Date getDate() {
		return date;
	}
	public void setDate(Date date) {
		this.date = date;
	}
	public Integer getOpenedCount() {
		return openedCount;
	}
	public void setOpenedCount(Integer openedCount) {
		this.openedCount = openedCount;
	}
	public Integer getCustomerSalesRepId() {
		return customerSalesRepId;
	}
	public void setCustomerSalesRepId(Integer customerSalesRepId) {
		this.customerSalesRepId = customerSalesRepId;
	}
	public String getCustomerSalesRepName() {
		return customerSalesRepName;
	}
	public void setCustomerSalesRepName(String customerSalesRepName) {
		this.customerSalesRepName = customerSalesRepName;
	}
	public Integer getContactAccessUserId() {
		return contactAccessUserId;
	}
	public void setContactAccessUserId(Integer contactAccessUserId) {
		this.contactAccessUserId = contactAccessUserId;
	}
	public Integer getContactSalesRepId() {
		return contactSalesRepId;
	}
	public void setContactSalesRepId(Integer contactSalesRepId) {
		this.contactSalesRepId = contactSalesRepId;
	}
	public String getContactSalesRepName() {
		return contactSalesRepName;
	}
	public void setContactSalesRepName(String contactSalesRepName) {
		this.contactSalesRepName = contactSalesRepName;
	}
}
