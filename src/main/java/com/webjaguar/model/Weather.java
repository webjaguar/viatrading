package com.webjaguar.model;

public class Weather
{
	private String high;
	private String low;
	//day
	private String dayIcon;
	private String dayTextCondition;
	//night
	private String nightIcon;
	private String nightTextCondition;
	//current condition
	private String ccTemp;
	private String ccIcon;
	private String ccTextCondition;
	public String getHigh()
	{
		return high;
	}
	public void setHigh(String high)
	{
		this.high = high;
	}
	public String getLow()
	{
		return low;
	}
	public void setLow(String low)
	{
		this.low = low;
	}
	public String getDayIcon()
	{
		return dayIcon;
	}
	public void setDayIcon(String dayIcon)
	{
		this.dayIcon = dayIcon;
	}
	public String getDayTextCondition()
	{
		return dayTextCondition;
	}
	public void setDayTextCondition(String dayTextCondition)
	{
		this.dayTextCondition = dayTextCondition;
	}
	public String getNightIcon()
	{
		return nightIcon;
	}
	public void setNightIcon(String nightIcon)
	{
		this.nightIcon = nightIcon;
	}
	public String getNightTextCondition()
	{
		return nightTextCondition;
	}
	public void setNightTextCondition(String nightTextCondition)
	{
		this.nightTextCondition = nightTextCondition;
	}
	public String getCcTemp()
	{
		return ccTemp;
	}
	public void setCcTemp(String ccTemp)
	{
		this.ccTemp = ccTemp;
	}
	public String getCcIcon()
	{
		return ccIcon;
	}
	public void setCcIcon(String ccIcon)
	{
		this.ccIcon = ccIcon;
	}
	public String getCcTextCondition()
	{
		return ccTextCondition;
	}
	public void setCcTextCondition(String ccTextCondition)
	{
		this.ccTextCondition = ccTextCondition;
	}
	
}
