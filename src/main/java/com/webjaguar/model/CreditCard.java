/* 
 * Copyright 2006 Advanced E-Media Solutions
 */

package com.webjaguar.model;

import java.io.Serializable;
import java.sql.Timestamp;

public class CreditCard implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String type;
	private String number;
	private String expireMonth;
	private String expireYear;
	private String cardCode;
	private String transId;
	private String authCode;
	private String avsCode;
	private String cvv2Code;
	private String paymentStatus;
	private String paymentNote;
	private Timestamp paymentDate;
	
	// cardinal centinel
	private String eci;
	private String cavv;
	private String xid;
	
	public String getExpireMonth() {
		return expireMonth;
	}
	public void setExpireMonth(String expireMonth) {
		this.expireMonth = expireMonth;
	}
	public String getExpireYear() {
		return expireYear;
	}
	public void setExpireYear(String expireYear) {
		this.expireYear = expireYear;
	}
	public String getNumber() {
		return number;
	}
	public void setNumber(String number) {
		this.number = number;
	}
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	public String getCardCode() {
		return cardCode;
	}
	public void setCardCode(String cardCode) {
		this.cardCode = cardCode;
	}
	public String getTransId() {
		return transId;
	}
	public void setTransId(String transId) {
		this.transId = transId;
	}
	public String getAuthCode() {
		return authCode;
	}
	public void setAuthCode(String authCode) {
		this.authCode = authCode;
	}
	public String getAvsCode() {
		return avsCode;
	}
	public void setAvsCode(String avsCode) {
		this.avsCode = avsCode;
	}
	public String getCvv2Code() {
		return cvv2Code;
	}
	public void setCvv2Code(String cvv2Code) {
		this.cvv2Code = cvv2Code;
	}
	public String getPaymentNote()
	{
		return paymentNote;
	}
	public void setPaymentNote(String paymentNote)
	{
		this.paymentNote = paymentNote;
	}
	public String getPaymentStatus()
	{
		return paymentStatus;
	}
	public void setPaymentStatus(String paymentStatus)
	{
		this.paymentStatus = paymentStatus;
	}
	public Timestamp getPaymentDate()
	{
		return paymentDate;
	}
	public void setPaymentDate(Timestamp paymentDate)
	{
		this.paymentDate = paymentDate;
	}
	public String getEci() {
		return eci;
	}
	public void setEci(String eci) {
		this.eci = eci;
	}
	public String getCavv() {
		return cavv;
	}
	public void setCavv(String cavv) {
		this.cavv = cavv;
	}
	public String getXid() {
		return xid;
	}
	public void setXid(String xid) {
		this.xid = xid;
	}
	
}
