package com.webjaguar.model;

import java.util.ArrayList;
import java.util.List;

public class LuceneCustomerSearch extends CustomerSearch{
	
	private Boolean noKeywords;
	public Boolean getNoKeywords() { return noKeywords;	}
	public void setNoKeywords(Boolean noKeywords) {	this.noKeywords = noKeywords; }

	// keywords
	private String keywords = "";
	public String getKeywords() { return keywords; }
	public void setKeywords(String keywords) { 
		keywordList.clear();	// clear old list
		this.keywords = keywords; 
	}
	
	private List<String> keywordList = new ArrayList<String>();
	public void setKeywordList(List<String> keywordList) { this.keywordList = keywordList; }	
	public List<String> getKeywordList() {	return this.keywordList; }
	public void addKeyword(String keyword) { keywordList.add(keyword); }
}
