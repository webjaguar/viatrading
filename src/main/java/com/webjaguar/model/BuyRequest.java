package com.webjaguar.model;

import java.sql.Timestamp;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;

public class BuyRequest
{
	private int id;
	private int ownerId;
	private String name;
	private String shortDesc;
	private String longDesc;
	private Set<Object> catIdsSet = new HashSet<Object>();
	private String catIds;
	private String status;
	
	private Date created;
	private Timestamp lastModified;
	private Date activeFrom;
	private Date activeTo;
	
	private String keywords = "";
	private String protectedLevel = "0";
	

	public int getId()
	{
		return id;
	}

	public void setId(int id)
	{
		this.id = id;
	}

	public int getOwnerId()
	{
		return ownerId;
	}

	public void setOwnerId(int ownerId)
	{
		this.ownerId = ownerId;
	}

	public String getName()
	{
		return name;
	}

	public void setName(String name)
	{
		this.name = name;
	}

	public String getShortDesc()
	{
		return shortDesc;
	}

	public void setShortDesc(String shortDesc)
	{
		this.shortDesc = shortDesc;
	}

	public String getLongDesc()
	{
		return longDesc;
	}

	public void setLongDesc(String longDesc)
	{
		this.longDesc = longDesc;
	}

	public Set<Object> getCatIdsSet()
	{
		return catIdsSet;
	}

	public void setCatIdsSet(Set<Object> catIdsSet)
	{
		this.catIdsSet = catIdsSet;
	}

	public String getCatIds()
	{
		return catIds;
	}

	public void setCatIds(String catIds)
	{
		this.catIds = catIds;
	}

	public String getStatus()
	{
		return status;
	}

	public void setStatus(String status)
	{
		this.status = status;
	}

	public Date getCreated()
	{
		return created;
	}

	public void setCreated(Date created)
	{
		this.created = created;
	}

	public Timestamp getLastModified()
	{
		return lastModified;
	}

	public void setLastModified(Timestamp lastModified)
	{
		this.lastModified = lastModified;
	}

	public Date getActiveFrom()
	{
		return activeFrom;
	}

	public void setActiveFrom(Date activeFrom)
	{
		this.activeFrom = activeFrom;
	}

	public Date getActiveTo()
	{
		return activeTo;
	}

	public void setActiveTo(Date activeTo)
	{
		this.activeTo = activeTo;
	}

	public String getKeywords()
	{
		return keywords;
	}

	public void setKeywords(String keywords)
	{
		this.keywords = keywords;
	}

	public String getProtectedLevel()
	{
		return protectedLevel;
	}

	public void setProtectedLevel(String protectedLevel)
	{
		this.protectedLevel = protectedLevel;
	}

}
