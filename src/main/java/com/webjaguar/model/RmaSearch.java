/* Copyright 2008 Advanced E-Media Solutions
 * @author Eduardo Asprec
 * @since 09.16.2008
 */

package com.webjaguar.model;

import java.util.ArrayList;
import java.util.List;

public class RmaSearch {

	private String rmaNum;
	private String orderId;	
	private String serialNum;
	private String sku;
	private List<Integer> userIdList;
	private int page = 1;
	private int pageSize = 10;
	// sort
	private String sort= "report_date DESC";
	private String status;
	
	public String getRmaNum()
	{
		return rmaNum;
	}
	public void setRmaNum(String rmaNum)
	{
		this.rmaNum = rmaNum;
	}
	public String getSerialNum()
	{
		return serialNum;
	}
	public void setSerialNum(String serialNum)
	{
		this.serialNum = serialNum;
	}
	public int getPage()
	{
		return page;
	}
	public void setPage(int page)
	{
		this.page = page;
	}
	public int getPageSize()
	{
		return pageSize;
	}
	public void setPageSize(int pageSize)
	{
		this.pageSize = pageSize;
	}
	public String getSort()
	{
		return sort;
	}
	public void setSort(String sort)
	{
		this.sort = sort;
	}
	public List<Integer> getUserIdList()
	{
		return userIdList;
	}
	public void setUserIdList(List<Integer> userIdList)
	{
		this.userIdList = userIdList;
	}
	public void addUserId(Integer userId) 
	{
		if (this.userIdList == null) {
			this.userIdList = new ArrayList<Integer>(); 
		}
		this.userIdList.add( userId );
	}
	public String getStatus()
	{
		return status;
	}
	public void setStatus(String status)
	{
		this.status = status;
	}
	public String getOrderId()
	{
		return orderId;
	}
	public void setOrderId(String orderId)
	{
		this.orderId = orderId;
	}
	public String getSku()
	{
		return sku;
	}
	public void setSku(String sku)
	{
		this.sku = sku;
	}

}
