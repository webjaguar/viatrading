package com.webjaguar.model;

public class Unsubscribe {
	public Unsubscribe(){}
	
	private Integer id;
	private Integer campaignId;
	private String email;
	
	public Integer getCampaignId() {
		return campaignId;
	}
	public void setCampaignId(Integer campaignId) {
		this.campaignId = campaignId;
	}
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	
}