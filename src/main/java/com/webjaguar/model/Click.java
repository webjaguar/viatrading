package com.webjaguar.model;

public class Click {
	public Click(){}
	
	private Integer id;
	private Integer campaignId;
	private String url;
	private int urlClickCounter;	
	private ClickDetail clickDetail;
	
	public Integer getCampaignId() {
		return campaignId;
	}
	public void setCampaignId(Integer campaignId) {
		this.campaignId = campaignId;
	}
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public String getUrl() {
		return url;
	}
	public void setUrl(String url) {
		this.url = url;
	}
	public int getUrlClickCounter() {
		return urlClickCounter;
	}
	public void setUrlClickCounter(int urlClickCounter) {
		this.urlClickCounter = urlClickCounter;
	}
	public ClickDetail getClickDetail() {
		return clickDetail;
	}
	public void setClickDetail(ClickDetail clickDetail) {
		this.clickDetail = clickDetail;
	}
}