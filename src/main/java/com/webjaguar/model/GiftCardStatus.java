package com.webjaguar.model;

import java.sql.Timestamp;

public class GiftCardStatus {

	private Integer giftCardOrderId;
	private String status;
	private Timestamp dateChanged;
	private String comments;
	private String username;
	
	public Integer getGiftCardOrderId() {
		return giftCardOrderId;
	}
	public void setGiftCardOrderId(Integer giftCardOrderId) {
		this.giftCardOrderId = giftCardOrderId;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public Timestamp getDateChanged() {
		return dateChanged;
	}
	public void setDateChanged(Timestamp dateChanged) {
		this.dateChanged = dateChanged;
	}
	public String getComments() {
		return comments;
	}
	public void setComments(String comments) {
		this.comments = comments;
	}
	public String getUsername() {
		return username;
	}
	public void setUsername(String username) {
		this.username = username;
	}
}
