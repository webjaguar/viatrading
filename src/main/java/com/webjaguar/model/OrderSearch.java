/* Copyright 2006 Advanced E-Media Solutions
 * @author Eduardo Asprec
 * @since 09.22.2006
 */

package com.webjaguar.model;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

public class OrderSearch implements Serializable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	// sort
	private String sort = "order_id DESC";
	public String getSort() { return sort; }
	public void setSort(String sort) { this.sort = sort; }	
	
	// userId
	private Integer userId;
	public Integer getUserId() { return userId; }
	public void setUserId(Integer userId) { this.userId = userId; }
	
	// status
	private String status;
	public String getStatus() { return status; }
	public void setStatus(String status) { this.status = status; }
	
	// statusTemp
	private String statusTemp;
	public String getStatusTemp() { return statusTemp; }
	public void setStatusTemp(String statusTemp) { this.statusTemp = statusTemp; }
	
	// sub status
	private String subStatus;
	public String getSubStatus() { return subStatus; }
	public void setSubStatus(String subStatus) { this.subStatus = subStatus; }

	// order id
	private int orderId = 0;
	public int getOrderId() { return orderId;	}
	public void setOrderId(int orderId) {	this.orderId = orderId; }
	
	// order number
	private String orderNum = "";
	public String getOrderNum() { return orderNum;	}
	public void setOrderNum(String orderNum) {	this.orderNum = orderNum; }
	
	// order start number
	private Integer orderStartNumber;
	public Integer getOrderStartNumber() { return orderStartNumber; }
	public void setOrderStartNumber(Integer orderStartNumber) { this.orderStartNumber = orderStartNumber; }
	
	// host
	private String host = "";
	public String getHost() { return host; }
	public void setHost(String host) { this.host = host; }	
	
	// hosts
	private List<String> hosts;
	public List<String> getHosts() { return hosts; }
	public void setHost(List<String> hosts) { this.hosts = hosts; }	
	
	// billToName
	private String billToName;
	public String getBillToName() { return billToName; }
	public void setBillToName(String billToName) { this.billToName = billToName; }
	
	// shipToName
	private String shipToName;
	public String getShipToName() { return shipToName; }
	public void setShipToName(String shipToName) { this.shipToName = shipToName; 
	}
	
	// shippingCarrier
	private String shippingCarrier;
	public String getShippingCarrier() { return shippingCarrier; }
	public void setShippingCarrier(String shippingCarrier) { this.shippingCarrier = shippingCarrier;
	}	
	
	// companyName
	private String companyName;
	public String getCompanyName() { return companyName; }
	public void setCompanyName(String companyName) { this.companyName = companyName; }
	
	// email
	private String email;
	public String getEmail() { return email; }
	public void setEmail(String email) { this.email = email; }
	
	//multiple Search
	private String[] multipleSearch;
	public String[] getMultipleSearch() { return multipleSearch; }
	public void setMultipleSearch(String[] multipleSearch) { this.multipleSearch = multipleSearch; }
	private String multipleSearchString;
	public String getMultipleSearchString() { return multipleSearchString; }
	public void setMultipleSearchString(String multipleSearchString) { this.multipleSearchString = multipleSearchString; }
	
	// rating1
	private String rating1;	
	public String getRating1() { return rating1; }
	public void setRating1(String rating1) { this.rating1 = rating1; }

	// rating2
	private String rating2;
	public String getRating2() { return rating2; }
	public void setRating2(String rating2) { this.rating2 = rating2; }	
	
	// phone
	private String phone;
	public String getPhone() { return phone; }
	public void setPhone(String phone) { this.phone = phone; }
	
	// zipcode
	private String zipcode;
	public String getZipcode() { return zipcode; }
	public void setZipcode(String zipcode) { this.zipcode = zipcode; }
	
	// trackCode
	private String trackCode;
	public String getTrackCode() { return trackCode; }
	public void setTrackCode(String trackCode) { this.trackCode = trackCode; }
	
	// promoCode
	private String promoCode;
	public String getPromoCode() { return promoCode; }
	public void setPromoCode(String promoCode) { this.promoCode = promoCode; }
	
	// paymentMethod
	private String paymentMethod;
	public String getPaymentMethod() { return paymentMethod; }
	public void setPaymentMethod(String paymentMethod) { this.paymentMethod = paymentMethod; }
	
	//state
	private String state;
	public String getState() { return state; }
	public void setState(String state) { this.state = state; }
	
	// country
	private String country;
	public String getCountry() { return country; }
	public void setCountry(String country) { this.country = country; }
	
	// page 
	private int page = 1;
	public int getPage() { return page; }
	public void setPage(int page) { this.page = page; }
	
	// page size
	private int pageSize = 10;
	public int getPageSize() { return pageSize; }
	public void setPageSize(int pageSize) { this.pageSize = pageSize; }
		
	// start date
	private Date startDate; 
	public Date getStartDate() { return startDate; }
	public void setStartDate(Date startDate) { this.startDate = startDate; }
	
	// end date
	private Date endDate;
	public Date getEndDate() { return endDate; }
	public void setEndDate(Date endDate) { this.endDate = endDate; }
	
	private String dateType = "orderDate";
	public String getDateType() { return dateType; }
	public void setDateType(String dateType) { this.dateType = dateType; }
	
	// year
	private Integer year; 
	public Integer getYear() { return year; }
	public void setYear(Integer year) { this.year = year; }
	
	// limit
	private Integer limit;
	public Integer getLimit() { return limit; }
	public void setLimit(Integer limit) { this.limit = limit; }
	
	// offset
	private int offset;
	public int getOffset() { return offset; }
	public void setOffset(int offset) { this.offset = offset; }	

	// used for exporting invoices
	private String fileType;
	public String getFileType() { return fileType; }
	public void setFileType(String fileType) { this.fileType = fileType; }	
	
	// sales rep
	private Integer salesRepId = -1;
	public Integer getSalesRepId() { return salesRepId; }
	public void setSalesRepId(Integer salesRepId) { this.salesRepId = salesRepId; }
	
	// sales rep processesBy
	private Integer salesRepProcessedById = -1;
	public Integer getSalesRepProcessedById() { return salesRepProcessedById; }
	public void setSalesRepProcessedById(Integer salesRepProcessedById) { this.salesRepProcessedById = salesRepProcessedById; }
	
	private String shippingMethod;
	public String getShippingMethod() { return shippingMethod; }
	public void setShippingMethod(String shippingMethod) { this.shippingMethod = shippingMethod; }
	
	// mas90 
	private Boolean mas90;
	public Boolean getMas90() { return mas90; }
	public void setMas90(Boolean mas90) { this.mas90 = mas90; }
	
	// mas200
	private Boolean mas200;
	public Boolean getMas200() { return mas200; }
	public void setMas200(Boolean mas200) { this.mas200 = mas200; }
	
	private String purchaseOrder;
	public String getPurchaseOrder() { return purchaseOrder; }
	public void setPurchaseOrder(String purchaseOrder) { this.purchaseOrder = purchaseOrder; }
	
	// sku
	private String sku;
	public String getSku() { return sku; }
	public void setSku(String sku) { this.sku = sku; }
	
	private String[] multipleSku;	
	public String[] getMultipleSku() { return multipleSku; }
	public void setMultipleSku(String[] multipleSku) { this.multipleSku = multipleSku; }
	
	// sku operator
	private int skuOperator;
	public int getSkuOperator() { return skuOperator; }
	public void setSkuOperator(int skuOperator) { this.skuOperator = skuOperator; }
	
	private int shippingOperator;
	public int getShippingOperator() { return shippingOperator; }
	public void setShippingOperator(int shippingOperator) { this.shippingOperator = shippingOperator; }
	
	// backendOrder
	private String backendOrder;
	public String getBackendOrder() { return backendOrder; }
	public void setBackendOrder(String backendOrder) { this.backendOrder = backendOrder; }
	
	// dsi 
	private Boolean dsi;
	public Boolean getDsi() { return dsi; }
	public void setDsi(Boolean dsi) { this.dsi = dsi; }
	
	// triplefin 
	private Boolean triplefin;
	public Boolean getTriplefin() { return triplefin; }
	public void setTriplefin(Boolean triplefin) { this.triplefin = triplefin; }
	
	// shopworks 
	private Boolean shopworks;
	public Boolean getShopworks() { return shopworks; }
	public void setShopworks(Boolean shopworks) { this.shopworks = shopworks; }
	
	// affiliate id
	private Integer affiliateId = -1;
	public Integer getAffiliateId() { return affiliateId; }
	public void setAffiliateId(Integer affiliateId) { this.affiliateId = affiliateId; }
	
	// product field#
	private String productFieldNumber;
	public String getProductFieldNumber() { return productFieldNumber; }
	public void setProductFieldNumber(String productFieldNumber) { this.productFieldNumber = productFieldNumber; }

	// product field value
	private String productField;
	public String getProductField() { return productField; }
	public void setProductField(String productField) { this.productField = productField; }
	
	// customer field#
	private String customerFieldNumber;
	public String getCustomerFieldNumber() { return customerFieldNumber; }
	public void setCustomerFieldNumber(String customerFieldNumber) { this.customerFieldNumber = customerFieldNumber; }

	// customer field value
	private String customerField;
	public String getCustomerField() { return customerField; }
	public void setCustomerField(String customerField) { this.customerField = customerField; }
	
	// credit card
	private String ccPaymentStatus;
	public String getCcPaymentStatus() { return ccPaymentStatus; }
	public void setCcPaymentStatus(String ccPaymentStatus) { this.ccPaymentStatus = ccPaymentStatus; }
	
	// order type
	private String orderType;
	public String getOrderType() { return orderType; }
	public void setOrderType(String orderType) { this.orderType = orderType; }
	
	// falg1
	private String flag1;
	public String getFlag1() { return flag1; }
	public void setFlag1(String flag1) { this.flag1 = flag1; }
	
	// subTotal
	private Double subTotal;
	public Double getSubTotal() { return subTotal; }
	public void setSubTotal(Double subTotal) { this.subTotal = subTotal; }
	
	// Card ID
	private String cardId;
	public String getCardId() { return cardId; }
	public void setCardId(String cardId) { this.cardId = cardId; }
	
	// group Id
	private Integer groupId;
	public Integer getGroupId() { return groupId; }
	public void setGroupId(Integer groupId) { this.groupId = groupId; }
	
	// business vision 
	private Boolean bv;
	public Boolean getBv() { return bv; }
	public void setBv(Boolean bv) { this.bv = bv; }
	
	// plow & hearth 
	private Boolean pnh;
	public Boolean getPnh() { return pnh; }
	public void setPnh(Boolean pnh) { this.pnh = pnh; }
	
	// start order number
	private String orderNumStart;
	public String getOrderNumStart() { return orderNumStart; }
	public void setOrderNumStart(String orderNumStart) { this.orderNumStart = orderNumStart; }
	
	// end order number
	private String orderNumEnd;
	public String getOrderNumEnd() { return orderNumEnd; }
	public void setOrderNumEnd(String orderNumEnd) { this.orderNumEnd = orderNumEnd; }

	// custom search
	private String customSearch;
	public String getCustomSearch() { return customSearch; }
	public void setCustomSearch(String customSearch) { this.customSearch = customSearch; }

	private String languageCode;
	public String getLanguageCode() { return languageCode; }
	public void setLanguageCode(String languageCode) { this.languageCode = languageCode; }	

	private Address shipping;	
	public Address getShipping() { return shipping; }
	public void setShipping(Address shipping) { this.shipping = shipping; }
	
	private String paidFull;
	public void setPaidFull(String paidFull) { this.paidFull = paidFull; }
	public String getPaidFull() { return paidFull; }
	
	private Boolean exportSuccess;
	public void setExportSuccess(Boolean exportSuccess) { this.exportSuccess = exportSuccess; }
	public Boolean getExportSuccess() { return exportSuccess; }
	
	private Integer firstOrder;
	public Integer getFirstOrder() { return firstOrder; }
	public void setFirstOrder(Integer firstOrder) { this.firstOrder = firstOrder; }
	
	private String parentSku;
	public String getParentSku() { return parentSku; }
	public void setParentSku(String parentSku) { this.parentSku = parentSku; }

	private Integer parentSkuOperator;	
	public Integer getParentSkuOperator() { return parentSkuOperator; }
	public void setParentSkuOperator(Integer parentSkuOperator) { this.parentSkuOperator = parentSkuOperator; }
	
	private String qualifier;
	public String getQualifier() { return qualifier; }
	public void setQualifier(String qualifier) { this.qualifier = qualifier; }

	
	private Integer qualifierId;
	public Integer getQualifierId() {return qualifierId;}
	public void setQualifierId(Integer qualifierId) {this.qualifierId = qualifierId;}
	
	private Integer accountManager;
	public Integer getAccountManager() {return accountManager;}
	public void setAccountManager(Integer accountManager) {this.accountManager = accountManager;}
	
	private String description;
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	
	private String shippingState;
	public String getShippingState() {
		return shippingState;
	}
	public void setShippingState(String shippingState) {
		this.shippingState = shippingState;
	}
	
}
