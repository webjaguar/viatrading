package com.webjaguar.model;

import java.util.ArrayList;
import java.util.List;

public class BuyRequestSearch
{
	// sort
	private String sort = "id DESC";
	public String getSort() { return sort; }
	public void setSort(String sort) { this.sort = sort; }	
	
	// page 
	private int page = 1;
	public int getPage() { return page; }
	public void setPage(int page) { this.page = page; }
	
	// page size
	private int pageSize = 10;
	public int getPageSize() { return pageSize; }
	public void setPageSize(int pageSize) { this.pageSize = pageSize; }
	
	// name
	private String name;
	public String getName() { return name; }
	public void setName(String name) { this.name = name; }

	// userId
	private Integer userId;
	public Integer getUserId() { return userId; }
	public void setUserId(Integer userId) { this.userId = userId; }

	// id
	private String buyRequestId;
	public String getBuyRequestId() { return buyRequestId; }
	public void setBuyRequestId(String buyRequestId) { this.buyRequestId = buyRequestId; }
	
	// status
	private String status = "open";
	public String getStatus() { return status; }
	public void setStatus(String status) { this.status = status; }
	
	// keywords
	private String keywords = "";
	public String getKeywords() { return keywords; }
	public void setKeywords(String keywords) { 
		keywordList.clear();	// clear old list
		this.keywords = keywords; 
	}
	
	// keywords minimum characters
	private int keywordsMinChars;
	public int getKeywordsMinChars() { return keywordsMinChars; }
	public void setKeywordsMinChars(int keywordsMinChars) { this.keywordsMinChars = keywordsMinChars; }
	
	private List<String> keywordList = new ArrayList<String>();
	public void setKeywordList(List<String> keywordList) { this.keywordList = keywordList; }	
	public List<String> getKeywordList() {	return this.keywordList; }
	public void addKeyword(String keyword) { keywordList.add(keyword); }
	
	// keyword delimiter
	private String delimiter;
	public String getDelimiter() { return delimiter; }
	public void setDelimiter(String delimiter) { this.delimiter = delimiter; }
	
	private List<Integer> categoryIds;
	public List<Integer> getCategoryIds() { return categoryIds; }
	public void setCategoryIds(List<Integer> categoryIds) { this.categoryIds = categoryIds; }
	
	// category ID
	private Integer category;
	public Integer getCategory() { return category; }
	public void setCategory(Integer category) { this.category = category; }
	
	private String protectedAccess = "0";
	public String getProtectedAccess() { return protectedAccess; }
	public void setProtectedAccess(String protectedAccess) { this.protectedAccess = protectedAccess; }
	
	private Integer limit;
	public Integer getLimit() { return limit; }
	public void setLimit(Integer limit) { this.limit = limit; }

	// offset
	private int offset;
	public int getOffset() { return offset; }
	public void setOffset(int offset) { this.offset = offset; }	
}
