/* Copyright 2006 Advanced E-Media Solutions
 *
 */

package com.webjaguar.model;

public class Search {
	
	// sort
	private String sort = "id";
	public String getSort() { return sort; }
	public void setSort(String sort) { this.sort = sort; }	
	
	// page 
	private int page = 1;
	public int getPage() { return page; }
	public void setPage(int page) { this.page = page; }
	
	// page size
	private int pageSize = 10;
	public int getPageSize() { return pageSize; }
	public void setPageSize(int pageSize) { this.pageSize = pageSize; }
	
	// limit
	private Integer limit;
	public Integer getLimit() { return limit; }
	public void setLimit(Integer limit) { this.limit = limit; }
	
	// offset
	private int offset;
	public int getOffset() { return offset; }
	public void setOffset(int offset) { this.offset = offset; }
	
	//company name
	private String company;
	public String getCompany() { return company; }
	public void setCompany(String company) { this.company = company; }			
	
}
