/* Copyright 2008 Advanced E-Media Solutions
 * @author Eduardo Asprec
 * @since 12.03.2008
 */

package com.webjaguar.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class FilterAttribute implements Serializable, Comparable<FilterAttribute> {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private Integer filterId;
	private int resultCount;
	private String parent;
	private String parentDisplayName;
	private String name;
	private String value;
	private String displayName;
	private boolean rangeFilter;
	private boolean checkboxFilter;
	private boolean selected;
	private Integer rank;
	private List<FilterAttribute> subFilters = new ArrayList<FilterAttribute>();
	// required for ADI to redirect to sub category
	private String redirectUrl;
	
	
	public Integer getFilterId() {
		return filterId;
	}
	public void setFilterId(Integer filterId) {
		this.filterId = filterId;
	}
	public int getResultCount() {
		return resultCount;
	}
	public void setResultCount(int resultCount) {
		this.resultCount = resultCount;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public List<FilterAttribute> getSubFilters() {
		return subFilters;
	}
	public void setSubFilters(List<FilterAttribute> subFilters) {
		this.subFilters = subFilters;
	}
	public String getDisplayName() {
		return displayName;
	}
	public void setDisplayName(String displayName) {
		this.displayName = displayName;
	}
	public String getParent() {
		return parent;
	}
	public void setParent(String parent) {
		this.parent = parent;
	}
	public String getValue() {
		return value;
	}
	public void setValue(String value) {
		this.value = value;
	}
	
	public int compareTo(FilterAttribute o) {
		/*if(this.resultCount > 0 && o.resultCount > 0) {
		//	return this.resultCount - o.resultCount;
		}*/
		
		if(this.getRank() == null && o.getRank() == null){
			return this.name.compareTo(o.name);
		}
		
		if(this.getRank() != null && o.getRank() == null){
			return 1;
		}
		
		if(this.getRank() == null && o.getRank() != null){
			return -1;
		}
		
		return this.rank - o.rank;
	}
	public boolean isRangeFilter() {
		return rangeFilter;
	}
	public void setRangeFilter(boolean rangeFilter) {
		this.rangeFilter = rangeFilter;
	}
	public String getParentDisplayName() {
		return parentDisplayName;
	}
	public void setParentDisplayName(String parentDisplayName) {
		this.parentDisplayName = parentDisplayName;
	}
	public Integer getRank() {
		return rank;
	}
	public void setRank(Integer rank) {
		this.rank = rank;
	}
	public String getRedirectUrl() {
		return redirectUrl;
	}
	public void setRedirectUrl(String redirectUrl) {
		this.redirectUrl = redirectUrl;
	}
	public boolean isCheckboxFilter() {
		return checkboxFilter;
	}
	public void setCheckboxFilter(boolean checkboxFilter) {
		this.checkboxFilter = checkboxFilter;
	}
	public boolean isSelected() {
		return selected;
	}
	public void setSelected(boolean selected) {
		this.selected = selected;
	}

	
}