/* Copyright 2008 Advanced E-Media Solutions
 * @author Eduardo Asprec
 * @since 01.24.2008
 */

package com.webjaguar.model;

public class WorkOrderLineItem {

	private Integer serviceNum;
	private int lineNumber;
	private int quantity;
	private Double unitPrice;
	private Product product;
	private String notes;
	private String inSN;
	private String outSN;
	private Integer bwEndCount;	
	private Integer bwTotalCount;
	
	public Integer getServiceNum()
	{
		return serviceNum;
	}
	public void setServiceNum(Integer serviceNum)
	{
		this.serviceNum = serviceNum;
	}
	public int getLineNumber()
	{
		return lineNumber;
	}
	public void setLineNumber(int lineNumber)
	{
		this.lineNumber = lineNumber;
	}
	public int getQuantity()
	{
		return quantity;
	}
	public void setQuantity(int quantity)
	{
		this.quantity = quantity;
	}
	public Product getProduct()
	{
		return product;
	}
	public void setProduct(Product product)
	{
		this.product = product;
	}
	public String getNotes()
	{
		return notes;
	}
	public void setNotes(String notes)
	{
		this.notes = notes;
	}
	public Double getUnitPrice()
	{
		return unitPrice;
	}
	public void setUnitPrice(Double unitPrice)
	{
		this.unitPrice = unitPrice;
	}
	public String getInSN()
	{
		return inSN;
	}
	public void setInSN(String inSN)
	{
		this.inSN = inSN;
	}
	public String getOutSN()
	{
		return outSN;
	}
	public void setOutSN(String outSN)
	{
		this.outSN = outSN;
	}
	public Integer getBwEndCount()
	{
		return bwEndCount;
	}
	public void setBwEndCount(Integer bwEndCount)
	{
		this.bwEndCount = bwEndCount;
	}
	public Integer getBwTotalCount()
	{
		return bwTotalCount;
	}
	public void setBwTotalCount(Integer bwTotalCount)
	{
		this.bwTotalCount = bwTotalCount;
	}
	
	public Double getTotalPrice()
	{
		if ( unitPrice == null )
			return null;
		Double total = new Double( unitPrice.doubleValue() * ( ( product.getCaseContent() == null ) ? 1 :product.getCaseContent()) * quantity );
		return total;
	}

}
