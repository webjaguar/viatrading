/* 
 * Copyright 2006 Advanced E-Media Solutions
 */

package com.webjaguar.model;

import java.io.Serializable;

public class Paypal implements Serializable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String invoice;
	private String payment_date;
	private String txn_id;
	private String payment_status;
	
	public String getInvoice()
	{
		return invoice;
	}
	public void setInvoice(String invoice)
	{
		this.invoice = invoice;
	}
	public String getPayment_date()
	{
		return payment_date;
	}
	public void setPayment_date(String payment_date)
	{
		this.payment_date = payment_date;
	}
	public String getPayment_status()
	{
		return payment_status;
	}
	public void setPayment_status(String payment_status)
	{
		this.payment_status = payment_status;
	}
	public String getTxn_id()
	{
		return txn_id;
	}
	public void setTxn_id(String txn_id)
	{
		this.txn_id = txn_id;
	}
	


}
