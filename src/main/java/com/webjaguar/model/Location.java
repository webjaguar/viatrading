/* Copyright 2005, 2009 Advanced E-Media Solutions
 *
 */

package com.webjaguar.model;

import java.util.Date;

public class Location {
	
	private Integer id;
	private Integer userId;
	private String name;
	private String keywords = "";
	private String addr1;
	private String addr2;
	private String city;
	private String stateProvince;
	private String zip;
	private String country;
	private String phone;
	private String fax;
	private String htmlCode;
	private Date created;

	public Integer getId()
	{
		return id;
	}

	public void setId(Integer id)
	{
		this.id = id;
	}

	public Integer getUserId()
	{
		return userId;
	}

	public void setUserId(Integer userId)
	{
		this.userId = userId;
	}

	public String getName()
	{
		return name;
	}

	public void setName(String name)
	{
		this.name = name;
	}

	public String getKeywords()
	{
		return keywords;
	}

	public void setKeywords(String keywords)
	{
		this.keywords = keywords;
	}

	public String getAddr1()
	{
		return addr1;
	}

	public void setAddr1(String addr1)
	{
		this.addr1 = addr1;
	}

	public String getAddr2()
	{
		return addr2;
	}

	public void setAddr2(String addr2)
	{
		this.addr2 = addr2;
	}

	public String getCity()
	{
		return city;
	}

	public void setCity(String city)
	{
		this.city = city;
	}

	public String getStateProvince()
	{
		return stateProvince;
	}

	public void setStateProvince(String stateProvince)
	{
		this.stateProvince = stateProvince;
	}

	public String getZip()
	{
		return zip;
	}

	public void setZip(String zip)
	{
		this.zip = zip;
	}

	public String getCountry()
	{
		return country;
	}

	public void setCountry(String country)
	{
		this.country = country;
	}

	public String getPhone()
	{
		return phone;
	}

	public void setPhone(String phone)
	{
		this.phone = phone;
	}

	public String getFax()
	{
		return fax;
	}

	public void setFax(String fax)
	{
		this.fax = fax;
	}

	public String getHtmlCode()
	{
		return htmlCode;
	}

	public Date getCreated()
	{
		return created;
	}

	public void setCreated(Date created)
	{
		this.created = created;
	}

	public void setHtmlCode(String htmlCode)
	{
		this.htmlCode = htmlCode;
	}

	public String getAddressToString() {
		return this.addr1 + " " + this.addr2 + " " + this.city + " " + this.zip + " " + this.stateProvince + " " + this.country;
	}

}
