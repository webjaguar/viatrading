/* Copyright 2006 Advanced E-Media Solutions
 *
 */

package com.webjaguar.model;


public class ReviewSearch {
	
	// sort
	private String sort = "id DESC";
	public String getSort() { return sort; }
	public void setSort(String sort) { this.sort = sort; }	
	
	private String productSku;
	public String getProductSku() {	return productSku; }
	public void setProductSku(String productSku)	{ this.productSku = productSku; }
	
	private String title;
	public String getTitle() { return title; }
	public void setTitle(String title) { this.title = title; }
	
	// page 
	private int page = 1;
	public int getPage() { return page; }
	public void setPage(int page) { this.page = page; }
	
	// page size
	private int pageSize = 10;
	public int getPageSize() { return pageSize; }
	public void setPageSize(int pageSize) { this.pageSize = pageSize; }
	
	private Integer limit;
	public Integer getLimit() { return limit; }
	public void setLimit(Integer limit) { this.limit = limit; }

	// offset
	private int offset;
	public int getOffset() { return offset; }
	public void setOffset(int offset) { this.offset = offset; }
	
	// status
	private String status;
	public String getStatus() { return status; }
	public void setStatus(String status) { this.status = status; }
	
	// active flag
	private String active;
	public String getActive() { return active; }
	public void setActive(String active) { this.active = active; }
	
	private String companyId;
	public String getCompanyId() { return companyId; }
	public void setCompanyId(String companyId) { this.companyId = companyId; }
	
}
