/*
 * Copyright 2006 Advanced E-Media Solutions
 * 
 */

package com.webjaguar.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.TreeMap;

import org.apache.poi.hssf.record.formula.functions.And;

public class LineItem implements Serializable {
	
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private Integer orderId;
	private String externalOrderId;
	private Integer productId;
	private int lineNumber;
	private int quantity;
	private int originalQuantity;
	private int toBeShipQty;
	private Double originalPrice;
	private boolean percent = true;
	private Double discount;
	private Double unitPrice;
	private Double unitCost;
	private boolean setSpecialPricing;
	private Product product;
	private List<ProductAttribute> productAttributes;
	private List<ProductAttribute> asiProductAttributes;
	private List<ProductAttribute> newProductAttributes;

	private Double commissionLevel1;
	private Double commissionLevel2;
	private List<ProductField> productFields;
	private Supplier supplier;
	// inventory
	private String lowInventoryMessage;
	private int inventoryQuantity;
	private Integer updateOnHandQty;
	private Boolean includeRetailDisplay;
	private String location;


	private int processed;
	private int tempQuantity;
	// PackingList
	private String packingListNumber;
	// loyalty 
	private Double loyaltyPoint;
	// subscription
	private String subscriptionInterval;
	private String subscriptionCode;
	// custom lines
	private TreeMap<String, Integer> customLines;
	
	private List<String> serialNums;
	private Double customShippingCost;
	
	private String trackNum;
	private Date dateShipped;
	private String shippingMethod;
	
	private String manufactureName;
	
	// custom frame
	private String customXml;
	private String customImageUrl;
	
	private String dealGroup;
	// amazon checkout
	private String amazonOrderItemCode;
	// promo
	private Promo promo;
	private String promoCode;
	private Double promoAmount = 0.0;
	// asi additional charge
	private Double asiAdditionalCharge;
	private Double optionFixedCharge;
	private String status;
	// consignment
	private Double cost;
	private boolean costPercent;
	private boolean consignmentProduct;
	
	private Integer priceCasePackQty;
	
	//shippingDays
	private String shippingDays;
	
	// virtual bank orders
	private Boolean vbaPaymentStatus;
	private Date vbaDateOfPay;
	private String vbaPaymentMethod;
	private Double vbaAmountToPay;
	private String vbaNote;
	private String vbaTransactionId;
	private String vbaProductSku;
	
	// group
	private String itemGroup;
	private boolean itemGroupMainItem = true;
	private String attachment;
	
	// parent sku deal
	private Double parentSkuOriginalPrice;
	private Double parentSkuDiscount;
	
	private Double tax;
	
	public Double getTax() {
		return tax;
	}
	
	public Boolean getVbaPaymentStatus() {
		return vbaPaymentStatus;
	}

	public void setVbaPaymentStatus(Boolean vbaPaymentStatus) {
		this.vbaPaymentStatus = vbaPaymentStatus;
	}

	public Date getVbaDateOfPay() {
		return vbaDateOfPay;
	}

	public void setVbaDateOfPay(Date vbaDateOfPay) {
		this.vbaDateOfPay = vbaDateOfPay;
	}

	public String getVbaPaymentMethod() {
		return vbaPaymentMethod;
	}

	public void setVbaPaymentMethod(String vbaPaymentMethod) {
		this.vbaPaymentMethod = vbaPaymentMethod;
	}

	public Double getVbaAmountToPay() {
		return vbaAmountToPay;
	}

	public void setVbaAmountToPay(Double vbaAmountToPay) {
		this.vbaAmountToPay = vbaAmountToPay;
	}

	public String getVbaNote() {
		return vbaNote;
	}

	public void setVbaNote(String vbaNote) {
		this.vbaNote = vbaNote;
	}

	public String getVbaTransactionId() {
		return vbaTransactionId;
	}

	public void setVbaTransactionId(String vbaTransactionId) {
		this.vbaTransactionId = vbaTransactionId;
	}

	/* Constructors */
	public LineItem()
	{}

	public LineItem( int lineNumber, CartItem cartItem )
	{
		this.lineNumber = lineNumber;
		this.quantity = cartItem.getQuantity();
		this.productId = cartItem.getProduct().getId();
		this.unitPrice = cartItem.getUnitPrice();
		this.originalPrice = cartItem.getUnitPrice();
		this.includeRetailDisplay = cartItem.getIncludeRetailDisplay();
		this.product = cartItem.getProduct();
		this.productAttributes = cartItem.getProductAttributes();
		this.asiProductAttributes = cartItem.getAsiProductAttributes();
		this.lowInventoryMessage = cartItem.getLowInventoryMessage();
		this.subscriptionInterval = cartItem.getSubscriptionInterval();
		this.customLines = cartItem.getCustomLines();
		this.supplier = new Supplier();
		this.customXml = cartItem.getCustomXml();
		this.customImageUrl = cartItem.getCustomImageUrl();
		this.setDealGroup(cartItem.getDealGroup());
		if(cartItem.getProduct().getManufactureName() != null) {
			this.manufactureName = cartItem.getProduct().getManufactureName();
		}
		this.asiAdditionalCharge = cartItem.getAsiAdditionalCharge();
		this.setOptionFixedCharge(cartItem.getOptionFixedCharge());
		this.priceCasePackQty = cartItem.getPriceCasePackQty();
		this.attachment = cartItem.getAttachment();
		this.itemGroup = cartItem.getItemGroup();
		this.itemGroupMainItem = cartItem.isItemGroupMainItem();
		this.parentSkuDiscount = cartItem.getDiscount();
		this.parentSkuOriginalPrice = cartItem.getOriginalPrice();
	}
	
	public LineItem(LineItem lineItem, String packingListNumber)
	{
		this.setProduct( new Product() );
		this.setLineNumber( lineItem.getLineNumber() );
		this.setOrderId( lineItem.getOrderId() );
		this.getProduct().setSku( lineItem.getProduct().getSku() );
		this.packingListNumber = packingListNumber;
	}
	
	public LineItem(int orderId, int lineNumber, String amazonOrderItemCode) {
		this.orderId = orderId;
		this.lineNumber = lineNumber;
		this.amazonOrderItemCode = amazonOrderItemCode;
	}	
		
	public Integer getOrderId() {
		return orderId;
	}
	public void setOrderId(Integer orderId) {
		this.orderId = orderId;
	}
	public String getExternalOrderId() {
		return externalOrderId;
	}

	public void setExternalOrderId(String externalOrderId) {
		this.externalOrderId = externalOrderId;
	}

	public Integer getProductId() {
		return productId;
	}
	public void setProductId(Integer productId) {
		this.productId = productId;
	}
	public int getLineNumber() {
		return lineNumber;
	}
	public void setLineNumber(int lineNumber) {
		this.lineNumber = lineNumber;
	}
	public int getQuantity() {
		return quantity;
	}
	public void setQuantity(int quantity) {
		this.quantity = quantity;
	}
	public int getToBeShipQty() {
		return toBeShipQty;
	}

	public void setToBeShipQty(int toBeShipQty) {
		this.toBeShipQty = toBeShipQty;
	}
	
	public List<ProductAttribute> getNewProductAttributes() {
		return newProductAttributes;
	} 
	
	public void setNewProductAttributes(List<ProductAttribute> newProductAttributes) {
		this.newProductAttributes = newProductAttributes;
	}
	public Double getOriginalPrice() {
		return originalPrice;
	}
	public void setOriginalPrice(Double originalPrice) {
		this.originalPrice = originalPrice;
	}
	public boolean isPercent() {
		return percent;
	}
	public void setPercent(boolean percent) {
		this.percent = percent;
	}
	public Double getDiscount() {
		return discount;
	}
	public void setDiscount(Double discount) {
		this.discount = discount;
	}
	public Double getUnitPrice() {
		return unitPrice;
	}
	public void setUnitPrice(Double unitPrice) {
		this.unitPrice = unitPrice;
	}
	public void setUnitCost(Double unitCost) {
		this.unitCost = unitCost;
	}
	public Double getUnitCost() {
		return unitCost;
	}
	public Product getProduct() {
		return product;
	}
	public void setProduct(Product product)	{
		this.product = product;
	}
	public Double getDiscountedUnitPrice() {
		if (this.originalPrice == null) { return null; }
		double discountAmount = 0.0;
		
		if ( this.discount != null ) {
			if ( this.percent ) {
				discountAmount = this.originalPrice * (this.discount / 100.00);
				discountAmount = this.originalPrice - discountAmount;
			}
			else {
				discountAmount = this.originalPrice - this.discount;
			}
		} else {
			discountAmount = this.originalPrice;
		}
		return discountAmount;
	}
	
	public int getTotalQty() {
		if (product.getCaseContent() != null && this.priceCasePackQty == null)
			return getCaseContentQty() * quantity;
		else if (this.priceCasePackQty != null) 
			return  getCasePackQty() * quantity;
		else
			return  quantity;
	}
	
	public Double getTotalPrice() {
		if ( originalPrice == null )
			return null;
		Double total = new Double( getDiscountedUnitPrice() * getTotalQty() );
		if(this.getAsiAdditionalCharge() != null) {
			total = total + this.getAsiAdditionalCharge();
		}
		if(this.getOptionFixedCharge() != null) {
			total = total + this.getOptionFixedCharge();
		}
		if (this.promoAmount != null && this.promoAmount  > 0.0) {
			total = total - this.promoAmount;
		}
		
		return total;
	}
	public Double getPackingListTotalPrice() {
		if ( originalPrice == null )
			return null;
		Double total = new Double( ( getDiscountedUnitPrice() ) * ( getCaseContentQty() ) * ( getCasePackQty() ) * this.getToBeShipQty() );
		if(this.getAsiAdditionalCharge() != null) {
			total = total + this.getAsiAdditionalCharge();
		}
		if(this.getOptionFixedCharge() != null) {
			total = total + this.getOptionFixedCharge();
		}
		return total;
	}
	public int getCaseContentQty() {
		if (product.getCaseContent() != null) {
			return product.getCaseContent();
		} else {
			return 1;
		}
	}
	public int getCasePackQty() {
		if (this.priceCasePackQty != null) {
			return this.priceCasePackQty;
		} else {
			return 1;
		}
	}
	public List<ProductAttribute> getProductAttributes() {
		return productAttributes;
	}
	public void setProductAttributes(List<ProductAttribute> productAttributes) {
		this.productAttributes = productAttributes;
	}
	public Double getCommissionLevel1()	{
		return commissionLevel1;
	}
	public void setCommissionLevel1(Double commissionLevel1) {
		this.commissionLevel1 = commissionLevel1;
	}
	public Double getCommissionLevel2() {
		return commissionLevel2;
	}
	public void setCommissionLevel2(Double commissionLevel2) {
		this.commissionLevel2 = commissionLevel2;
	}
	public boolean isSetSpecialPricing() {
		return setSpecialPricing;
	}
	public void setSetSpecialPricing(boolean setSpecialPricing)	{
		this.setSpecialPricing = setSpecialPricing;
	}
	public Supplier getSupplier() {
		return supplier;
	}
	public void setSupplier(Supplier supplier) {
		this.supplier = supplier;
	}
	public int getInventoryQuantity() {
		return inventoryQuantity;
	}
	public void setInventoryQuantity(int inventoryQuantity)	{
		this.inventoryQuantity = inventoryQuantity;
	}
	public String getLowInventoryMessage() {
		return lowInventoryMessage;
	}
	public void setLowInventoryMessage(String lowInventoryMessage) {
		this.lowInventoryMessage = lowInventoryMessage;
	}
	public int getProcessed() {
		return processed;
	}
	public void setProcessed(int processed) {
		this.processed = processed;
	}
	public int getTempQuantity() {
		return tempQuantity;
	}
	public void setTempQuantity(int tempQuantity) {
		this.tempQuantity = tempQuantity;
	}
	public List<ProductField> getProductFields() {
		return productFields;
	}
	public void setProductFields(List<ProductField> productFields) {
		this.productFields = productFields;
	}
	public String getPackingListNumber() {
		return packingListNumber;
	}
	public void setPackingListNumber(String packingListNumber) {
		this.packingListNumber = packingListNumber;
	}
	public Double getLoyaltyPoint()	{
		return loyaltyPoint;
	}
	public void setLoyaltyPoint(Double loyaltyPoint) {
		this.loyaltyPoint = loyaltyPoint;
	}
	public String getSubscriptionInterval()	{
		return subscriptionInterval;
	}
	public void setSubscriptionInterval(String subscriptionInterval) {
		this.subscriptionInterval = subscriptionInterval;
	}
	public String getSubscriptionCode() {
		return subscriptionCode;
	}
	public void setSubscriptionCode(String subscriptionCode) {
		this.subscriptionCode = subscriptionCode;
	}
	public boolean ishasProductAttributesImage() {
		if (productAttributes != null) {
			for ( ProductAttribute productAttribute : productAttributes ) {
				if ( productAttribute.getImageUrl() != null && !productAttribute.getImageUrl().equals( "" )) {
					return true;
				}
			}
		}
		return false;
	}
	public TreeMap<String, Integer> getCustomLines() {
		return customLines;
	}
	public void setCustomLines(TreeMap<String, Integer> customLines) {
		this.customLines = customLines;
	}
	public void addCustomLine(String customLine, int quantity) {
		if (customLines == null) customLines = new TreeMap<String, Integer>();
		if (customLines.containsKey(customLine)) {
			customLines.put(customLine, customLines.get(customLine)+quantity);
		} else {
			customLines.put(customLine, quantity);
		}
	}
	public List<String> getSerialNums() {
		return serialNums;
	}
	public void setSerialNums(List<String> serialNums) {
		this.serialNums = serialNums;
	}
	public void addSerialNum(String serialNum) {
		if (serialNums == null) serialNums = new ArrayList<String>();
		serialNums.add(serialNum);
	}
	public Double getCustomShippingCost() {
		return customShippingCost;
	}
	public void setCustomShippingCost(Double customShippingCost) {
		this.customShippingCost = customShippingCost;
	}
	public String getTrackNum()	{
		return trackNum;
	}
	public void setTrackNum(String trackNum) {
		this.trackNum = trackNum;
	}
	public Date getDateShipped() {
		return dateShipped;
	}
	public void setDateShipped(Date dateShipped) {
		this.dateShipped = dateShipped;
	}
	public String getShippingMethod() {
		return shippingMethod;
	}
	public void setShippingMethod(String shippingMethod) {
		this.shippingMethod = shippingMethod;
	}
	public String getManufactureName() {
		return manufactureName;
	}
	public void setManufactureName(String manufactureName) {
		this.manufactureName = manufactureName;
	}
	public String getCustomXml() {
		return customXml;
	}
	public void setCustomXml(String customXml) {
		this.customXml = customXml;
	}
	public String getCustomImageUrl() {
		return customImageUrl;
	}
	public void setCustomImageUrl(String customImageUrl) {
		this.customImageUrl = customImageUrl;
	}

	public void setDealGroup(String dealGroup) {
		this.dealGroup = dealGroup;
	}

	public String getDealGroup() {
		return dealGroup;
	}

	public String getAmazonOrderItemCode() {
		return amazonOrderItemCode;
	}

	public void setAmazonOrderItemCode(String amazonOrderItemCode) {
		this.amazonOrderItemCode = amazonOrderItemCode;
	}

	public void setPromo(Promo promo) {
		this.promo = promo;
	}

	public Promo getPromo() {
		return promo;
	}

	public void setPromoCode(String promoCode) {
		this.promoCode = promoCode;
	}

	public String getPromoCode() {
		return promoCode;
	}

	public void setPromoAmount(Double promoAmount) {
		this.promoAmount = promoAmount;
	}

	public void setPromoAmount(){
		double promoAmount = 0.0;
		if ( promo != null ) {
			if ( promo.isPercent() ) {
				promoAmount = ((getUnitPrice() == null) ? 0.00 : getUnitPrice()*getQuantity()) * ((promo.getDiscount() == null) ? 0.00 : promo.getDiscount() / 100.00);
			}
			else {
				promoAmount = (promo.getDiscount() == null) ? 0.00 : promo.getDiscount() * this.getQuantity();
			}
		}
		this.setPromoAmount( promoAmount );
	}
	
	public Double getPromoAmount() {
		return promoAmount;
	}

	public Double getPackageWeight() {
		if (product == null || product.getWeight() == null) {
			return 0.0;
		} else {
			// getCasePackQty always returns 1 or more
			double weight = product.getWeight() * getCasePackQty();
			if (product.getCaseContent() != null) {
				weight = weight * product.getCaseContent();
			}
			return weight;
		}
	}
	
	public Double getCubicSize() {
		if (product == null || 
				product.getPackageL() == null || product.getPackageW() == null || product.getPackageH() == null) {
			return null;
		}
		double L = Math.ceil(product.getPackageL());
		double W = Math.ceil(product.getPackageW());
		double H = Math.ceil(product.getPackageH());

		return (L * W * H);
	}

	public void setAsiProductAttributes(List<ProductAttribute> asiProductAttributes) {
		this.asiProductAttributes = asiProductAttributes;
	}

	public List<ProductAttribute> getAsiProductAttributes() {
		return asiProductAttributes;
	}

	public void setAsiAdditionalCharge(Double asiAdditionalCharge) {
		this.asiAdditionalCharge = asiAdditionalCharge;
	}

	public Double getAsiAdditionalCharge() {
		return asiAdditionalCharge;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}
	
	public void setOptionFixedCharge(Double optionFixedCharge) {
		this.optionFixedCharge = optionFixedCharge;
	}

	public Double getOptionFixedCharge() {
		return optionFixedCharge;
	}

	public Integer getPriceCasePackQty() {
		return priceCasePackQty;
	}

	public void setPriceCasePackQty(Integer priceCasePackQty) {
		this.priceCasePackQty = priceCasePackQty;
	}

	public void setShippingDays(String shippingDays) {
		this.shippingDays = shippingDays;
	}

	public String getShippingDays() {
		return shippingDays;
	}

	public String getVbaProductSku() {
		return vbaProductSku;
	}

	public void setVbaProductSku(String vbaProductSku) {
		this.vbaProductSku = vbaProductSku;
	}

	public void setAttachment(String attachment) {
		this.attachment = attachment;
	}

	public String getAttachment() {
		return attachment;
	}

	public void setItemGroupMainItem(boolean itemGroupMainItem) {
		this.itemGroupMainItem = itemGroupMainItem;
	}

	public boolean isItemGroupMainItem() {
		return itemGroupMainItem;
	}

	public void setItemGroup(String itemGroup) {
		this.itemGroup = itemGroup;
	}

	public String getItemGroup() {
		return itemGroup;
	}

	public boolean isConsignmentProduct() {
		return consignmentProduct;
	}

	public void setConsignmentProduct(boolean consignmentProduct) {
		this.consignmentProduct = consignmentProduct;
	}

	public Double getCost() {
		return cost;
	}

	public void setCost(Double cost) {
		this.cost = cost;
	}

	public boolean isCostPercent() {
		return costPercent;
	}

	public void setCostPercent(boolean costPercent) {
		this.costPercent = costPercent;
	}
	
	public Double getConsignmentValue() {
		if(getCost() == null) {
			return null;
		} else if(isCostPercent()) {
			return getUnitPrice() * getQuantity() * (getCost() / 100);
		} else {
			return getCost() * getQuantity();
		}
	}

	public void setOriginalQuantity(int originalQuantity) {
		this.originalQuantity = originalQuantity;
	}

	public int getOriginalQuantity() {
		return originalQuantity;
	}
	
	public Integer getUpdateOnHandQty() {
		return updateOnHandQty;
	}

	public void setUpdateOnHandQty(Integer updateOnHandQty) {
		this.updateOnHandQty = updateOnHandQty;
	}

	public Double getParentSkuOriginalPrice() {
		return parentSkuOriginalPrice;
	}

	public void setParentSkuOriginalPrice(Double parentSkuOriginalPrice) {
		this.parentSkuOriginalPrice = parentSkuOriginalPrice;
	}

	public Double getParentSkuDiscount() {
		return parentSkuDiscount;
	}

	public void setParentSkuDiscount(Double parentSkuDiscount) {
		this.parentSkuDiscount = parentSkuDiscount;
	}

	public Boolean getIncludeRetailDisplay() {
		return includeRetailDisplay;
	}

	public void setIncludeRetailDisplay(Boolean includeRetailDisplay) {
		this.includeRetailDisplay = includeRetailDisplay;
	}

	public String getLocation() {
		return location;
	}

	public void setLocation(String location) {
		this.location = location;
	}
	
}