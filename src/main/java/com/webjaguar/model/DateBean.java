package com.webjaguar.model;

public class DateBean extends java.util.GregorianCalendar implements java.io.Serializable
{
    private int month;
    private int day;
    private int year;

    // current Time
    public DateBean()
    {
        super();
    }

    public DateBean( java.sql.Timestamp timeStamp )
    {
        this.setTimeInMillis( timeStamp.getTime() );
    }

    public DateBean( int month, int day, int year )
    {
        super(year, month - 1, day, 0, 0, 0 );
        this.set( java.util.Calendar.MILLISECOND, 0 );
    }

    public DateBean( int hour, int min, int second, int tod )
    {
        super();
        this.setHour( hour );
        this.setMin( min );
        this.set( java.util.Calendar.SECOND, second );
        this.setTod( tod );
        this.set( java.util.Calendar.MILLISECOND, 0 );
    }

    public void setTime ( int hour, int min, int second )
    {
    	this.setHour(hour);
    	this.setMin(min);
    	this.setSec(second);
    	
    }
    public DateBean( long time )
    {
        this();
        this.setTimeInMillis( time );
    }

    public DateBean( java.util.GregorianCalendar calendar )
    {
        this();
        this.setTimeInMillis( calendar.getTimeInMillis() );
    }

    public void clearTime()
    {
        this.set( getYear(), getMonth() - 1, getDay(), 0, 0, 0 );
        this.set( java.util.Calendar.MILLISECOND, 0 );
    }

    public int getMonth()
    {
        return this.get( java.util.Calendar.MONTH ) + 1;
    }

    public void setMonth( int month )
    {
        this.set( java.util.Calendar.MONTH, month - 1 );
        this.month = month;
    }

    public int getDay()
    {
        return this.get( java.util.Calendar.DATE );
    }

    public void setDay( int day )
    {
        this.set( java.util.Calendar.DATE, day );
        this.day = day;
    }

    public int getYear()
    {
        return this.get(java.util.Calendar.YEAR);
    }

    public void setYear( int year )
    {
        this.set( java.util.Calendar.YEAR, year );
        this.year = year;
    }

    public int getHour()
    {
        return this.get( java.util.Calendar.HOUR );
    }

    public void setHour( int hour )
    {
        this.set( java.util.Calendar.HOUR, hour );
    }

    public int getMin()
    {
        return this.get( java.util.Calendar.MINUTE );
    }

    public void setMin( int min )
    {
        this.set( java.util.Calendar.MINUTE, min );
    }

    public void setSec( int sec )
    {
        this.set( java.util.Calendar.SECOND, sec );
    }

    public int getTod()
    {
        return this.get( java.util.Calendar.AM_PM );
    }

    public String getTodString()
    {
        return (this.get( java.util.Calendar.AM_PM ) == 0)?"AM":"PM";
    }

    public void setTod( int tod )
    {
        this.set( java.util.Calendar.AM_PM, tod );
    }

    public java.util.GregorianCalendar getCalendar()
    {
        return this;
    }

    public java.sql.Timestamp getTimestamp()
    {
        return new java.sql.Timestamp( super.getTimeInMillis() );
    }

    public java.sql.Date getSqlDate()
    {
        return new java.sql.Date( this.getTimeInMillis() );
    }

    public String getDateString()
    {
        return getMonth() + "/" + getDay() + "/" + getYear();
    }

    public String toString()
    {
        return getMonth() + "/" + getDay() + "/" + getYear() + " (" + getTimeInMillis() + ")";
    }

    public boolean isValid()
    {
        return ( ( this.month == getMonth() ) && ( this.day == getDay() ) && ( this.year == getYear() ) );
    }
}
