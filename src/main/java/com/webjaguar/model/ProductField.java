/* Copyright 2006 Advanced E-Media Solutions
 *
 */

package com.webjaguar.model;

import java.io.Serializable;
import java.util.*;

public class ProductField implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private Integer id;
	private Integer rank;
	private String name;
	private String formatType;
	private String value;
	private Set<String> values = new TreeSet<String>();
	private String[] fields;
	private List<String> selectedValues;
	private boolean empty;
	private boolean enabled;
	private boolean showOnInvoice;
	private boolean showOnInvoiceBackend;
	private boolean showOnInvoiceExport;
	private boolean showOnPresentation;
	private boolean showOnProductFormFrontend;
	private boolean showOnMyList;
	private boolean showOnDropDown;
	private boolean packingField;
	private boolean search;
	private boolean search2;
	private boolean quickModeField;
	private boolean quickMode2Field;
	private String preValue;
	private String fieldType;
	// protected
	private String protectedLevel = "0";
	private boolean comparisonField;
	private boolean serviceField;
	private boolean detailsField;
	private boolean productExport;
	// search operator
	private String operator = "=";

	private String categoryIds;
	private String groupName;

	// i18n
	private String lang;

	// min and max
	private Double minValue;
	private Double maxValue;

	
	// filter
	private boolean indexForFilter;
	private boolean indexForSearch;
	private boolean indexForPredictiveSearch;
	private boolean rangeFilter;
	private boolean checkboxFilter;
		
		
	
	public boolean isIndexForFilter() {
		return indexForFilter;
	}

	public void setIndexForFilter(boolean indexForFilter) {
		this.indexForFilter = indexForFilter;
	}

	public boolean isIndexForSearch() {
		return indexForSearch;
	}

	public void setIndexForSearch(boolean indexForSearch) {
		this.indexForSearch = indexForSearch;
	}

	public boolean isIndexForPredictiveSearch() {
		return indexForPredictiveSearch;
	}

	public void setIndexForPredictiveSearch(boolean indexForPredictiveSearch) {
		this.indexForPredictiveSearch = indexForPredictiveSearch;
	}

	public boolean isRangeFilter() {
		return rangeFilter;
	}

	public void setRangeFilter(boolean rangeFilter) {
		this.rangeFilter = rangeFilter;
	}

	public boolean isCheckboxFilter() {
		return checkboxFilter;
	}

	public void setCheckboxFilter(boolean checkboxFilter) {
		this.checkboxFilter = checkboxFilter;
	}

	public void setSelectedValues(List<String> selectedValues) {
		this.selectedValues = selectedValues;
	}

	public ProductField() {
		empty = true;
		enabled = false;
	}

	public ProductField(int id, String value) {
		this.id = id;
		this.value = value;
	}

	public ProductField(int id, List<String> selectedValues) {
		this.id = id;
		this.selectedValues = selectedValues;
	}

	public ProductField(int id, Double minValue, Double maxValue) {
		this.id = id;
		this.minValue = minValue;
		this.maxValue = maxValue;
	}

	public Integer getId() {
		return id;
	}

	public Integer getRank() {
		return rank;
	}

	public void setRank(Integer rank) {
		this.rank = rank;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}

	public boolean isEmpty() {
		return empty;
	}

	public void setEmpty(boolean empty) {
		this.empty = empty;
	}

	public static List<ProductField> removeEmpty(List<ProductField> productFields) {
		List<ProductField> newList = new ArrayList<ProductField>();
		for (ProductField productField : productFields) {
			if (!productField.isEmpty()) {
				newList.add(productField);
			}
		}
		return newList;
	}

	public boolean isEnabled() {
		return enabled;
	}

	public void setEnabled(boolean enabled) {
		this.enabled = enabled;
	}

	public String getProtectedLevel() {
		return protectedLevel;
	}

	public void setProtectedLevel(String protectedLevel) {
		this.protectedLevel = protectedLevel;
	}

	public boolean isQuickModeField() {
		return quickModeField;
	}

	public void setQuickModeField(boolean quickModeField) {
		this.quickModeField = quickModeField;
	}

	public String getPreValue() {
		return preValue;
	}

	public void setPreValue(String preValue) {
		this.preValue = preValue;
	}

	public boolean isComparisonField() {
		return comparisonField;
	}

	public void setComparisonField(boolean comparisonField) {
		this.comparisonField = comparisonField;
	}

	public boolean isShowOnInvoice() {
		return showOnInvoice;
	}

	public void setShowOnInvoice(boolean showOnInvoice) {
		this.showOnInvoice = showOnInvoice;
	}

	public boolean isServiceField() {
		return serviceField;
	}

	public void setServiceField(boolean serviceField) {
		this.serviceField = serviceField;
	}

	public boolean isDetailsField() {
		return detailsField;
	}

	public void setDetailsField(boolean detailsField) {
		this.detailsField = detailsField;
	}

	public String getMethodName() {
		return "getField" + this.id;
	}

	public boolean isSearch() {
		return search;
	}

	public void setSearch(boolean search) {
		this.search = search;
	}

	public boolean isSearch2() {
		return search2;
	}

	public void setSearch2(boolean search2) {
		this.search2 = search2;
	}

	/*
	 * public String toString() { return "" + this.getName(); }
	 */

	public boolean isPackingField() {
		return packingField;
	}

	public void setPackingField(boolean packingField) {
		this.packingField = packingField;
	}

	public boolean isShowOnInvoiceBackend() {
		return showOnInvoiceBackend;
	}

	public void setShowOnInvoiceBackend(boolean showOnInvoiceBackend) {
		this.showOnInvoiceBackend = showOnInvoiceBackend;
	}

	public boolean isShowOnPresentation() {
		return showOnPresentation;
	}

	public void setShowOnPresentation(boolean showOnPresentation) {
		this.showOnPresentation = showOnPresentation;
	}

	public boolean isShowOnProductFormFrontend() {
		return showOnProductFormFrontend;
	}

	public void setShowOnProductFormFrontend(boolean showOnProductFormFrontend) {
		this.showOnProductFormFrontend = showOnProductFormFrontend;
	}

	public String getOperator() {
		return operator;
	}

	public void setOperator(String operator) {
		this.operator = operator;
	}

	public Set<String> getValues() {
		return values;
	}

	public void setValues(Set<String> values) {
		this.values = values;
	}

	public String getCategoryIds() {
		return categoryIds;
	}

	public void setCategoryIds(String categoryIds) {
		this.categoryIds = categoryIds;
	}

	public String[] getFields() {
		return fields;
	}

	public void setFields(String[] fields) {
		this.fields = fields;
	}

	public List<String> getSelectedValues() {
		return selectedValues;
	}

	public void setSelectedFields(List<String> selectedValues) {
		this.selectedValues = selectedValues;
	}

	public boolean isProductExport() {
		return productExport;
	}

	public void setProductExport(boolean productExport) {
		this.productExport = productExport;
	}

	public void setShowOnInvoiceExport(boolean showOnInvoiceExport) {
		this.showOnInvoiceExport = showOnInvoiceExport;
	}

	public boolean isShowOnInvoiceExport() {
		return showOnInvoiceExport;
	}

	public String getLang() {
		return lang;
	}

	public void setLang(String lang) {
		this.lang = lang;
	}

	public boolean isQuickMode2Field() {
		return quickMode2Field;
	}

	public void setQuickMode2Field(boolean quickMode2Field) {
		this.quickMode2Field = quickMode2Field;
	}

	public String getFieldType() {
		return fieldType;
	}

	public void setFieldType(String fieldType) {
		this.fieldType = fieldType;
	}

	public Double getMinValue() {
		return minValue;
	}

	public void setMinValue(Double minValue) {
		this.minValue = minValue;
	}

	public Double getMaxValue() {
		return maxValue;
	}

	public void setMaxValue(Double maxValue) {
		this.maxValue = maxValue;
	}

	public boolean isShowOnMyList() {
		return showOnMyList;
	}

	public void setShowOnMyList(boolean showOnMyList) {
		this.showOnMyList = showOnMyList;
	}

	public boolean isShowOnDropDown() {
		return showOnDropDown;
	}

	public void setShowOnDropDown(boolean showOnDropDown) {
		this.showOnDropDown = showOnDropDown;
	}
	public String getFormatType() {
		return formatType;
	}

	public void setFormatType(String formatType) {
		this.formatType = formatType;
	}

	public String getGroupName() {
		return groupName;
	}

	public void setGroupName(String groupName) {
		this.groupName = groupName;
	}
	
}
