package com.webjaguar.model;

import java.util.Map;
import java.util.Set;

public class ConditionPromo {
	
	private Set<Object> groupId;
	private String username;
	private Map<String, Map<Object,String>> condition;
	private String states;
	
	public String getStates() {
		return states;
	}
	public void setStates(String states) {
		this.states = states;
	}
	public Set<Object> getGroupId() {
		return groupId;
	}
	public void setGroupId(Set<Object> groupId) {
		this.groupId = groupId;
	}
	public void setUsername(String username) {
		this.username = username;
	}
	public String getUsername() {
		return username;
	}
	public void setCondition(Map<String, Map<Object, String>> condition) {
		this.condition = condition;
	}
	public Map<String, Map<Object, String>> getCondition() {
		return condition;
	}
}
