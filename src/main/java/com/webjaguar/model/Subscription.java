/* Copyright 2008 Advanced E-Media Solutions
 * @author Eduardo Asprec
 * @since 03.26.2008
 */

package com.webjaguar.model;

import java.util.Date;
import java.util.List;
import java.sql.Timestamp;

public class Subscription {

	private String code;
	private int userId;
	private Customer customer;
	private Product product;
	private Timestamp created;
	private Timestamp lastModified;
	private int quantity;
	private Double unitPrice;
	private boolean enabled;
	private int intervalUnit;
	private String intervalType;
	private Date nextOrder;
	private Date lastOrder;
	private Integer orderIdOrig;
	private Integer orderIdLast;
	private List<ProductAttribute> productAttributes;
	
	public String getCode()
	{
		return code;
	}
	public void setCode(String code)
	{
		this.code = code;
	}
	public int getUserId()
	{
		return userId;
	}
	public void setUserId(int userId)
	{
		this.userId = userId;
	}
	public Customer getCustomer()
	{
		return customer;
	}
	public void setCustomer(Customer customer)
	{
		this.customer = customer;
	}
	public Product getProduct()
	{
		return product;
	}
	public void setProduct(Product product)
	{
		this.product = product;
	}
	public Timestamp getCreated()
	{
		return created;
	}
	public void setCreated(Timestamp created)
	{
		this.created = created;
	}
	public Timestamp getLastModified()
	{
		return lastModified;
	}
	public void setLastModified(Timestamp lastModified)
	{
		this.lastModified = lastModified;
	}
	public int getQuantity()
	{
		return quantity;
	}
	public void setQuantity(int quantity)
	{
		this.quantity = quantity;
	}
	public boolean isEnabled()
	{
		return enabled;
	}
	public void setEnabled(boolean enabled)
	{
		this.enabled = enabled;
	}
	public int getIntervalUnit()
	{
		return intervalUnit;
	}
	public void setIntervalUnit(int intervalUnit)
	{
		this.intervalUnit = intervalUnit;
	}
	public String getIntervalType()
	{
		return intervalType;
	}
	public void setIntervalType(String intervalType)
	{
		this.intervalType = intervalType;
	}
	public Date getNextOrder()
	{
		return nextOrder;
	}
	public void setNextOrder(Date nextOrder)
	{
		this.nextOrder = nextOrder;
	}
	public Date getLastOrder()
	{
		return lastOrder;
	}
	public void setLastOrder(Date lastOrder)
	{
		this.lastOrder = lastOrder;
	}
	public Integer getOrderIdOrig()
	{
		return orderIdOrig;
	}
	public void setOrderIdOrig(Integer orderIdOrig)
	{
		this.orderIdOrig = orderIdOrig;
	}
	public Integer getOrderIdLast()
	{
		return orderIdLast;
	}
	public void setOrderIdLast(Integer orderIdLast)
	{
		this.orderIdLast = orderIdLast;
	}
	public Double getUnitPrice()
	{
		return unitPrice;
	}
	public void setUnitPrice(Double unitPrice)
	{
		this.unitPrice = unitPrice;
	}
	public List<ProductAttribute> getProductAttributes()
	{
		return productAttributes;
	}
	public void setProductAttributes(List<ProductAttribute> productAttributes)
	{
		this.productAttributes = productAttributes;
	}

}
