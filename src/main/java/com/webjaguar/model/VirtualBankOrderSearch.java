package com.webjaguar.model;

import java.util.Date;

public class VirtualBankOrderSearch {
	
    // id 
	private String orderId = "";
	public String getOrderId() { return orderId; }
	public void setOrderId(String orderId) { this.orderId = orderId; }
	
	// product sku
	private String productSku;
	public String getProductSku() { return productSku; }
	public void setProductSku(String productSku) { this.productSku = productSku; }	
	
	// page 
	private int page = 1;
	public int getPage() { return page; }
	public void setPage(int page) { this.page = page; }
	
	// page size
	private int pageSize = 10;
	public int getPageSize() { return pageSize; }
	public void setPageSize(int pageSize) { this.pageSize = pageSize; }
		
	// start date
	private Date startDate; 
	public Date getStartDate() { return startDate; }
	public void setStartDate(Date startDate) { this.startDate = startDate; }
	
	// end date
	private Date endDate;
	public Date getEndDate() { return endDate; }
	public void setEndDate(Date endDate) { this.endDate = endDate; }
	
	// user name
	private String userName;
	public String getUserName() { return userName; }
	public void setUserName(String userName) { this.userName = userName; }	
	
	// vba payment for
	private String paymentFor;
	public String getPaymentFor() { return paymentFor; }
	public void setPaymentFor(String paymentFor) { this.paymentFor = paymentFor; }
	
	// user id
	private String userId ="";
	public String getUserId() { return userId; }
	public void setUserId(String userId) { this.userId = userId; }
	
	// supplier Id
	private String supplierId ="";
	public String getSupplierId() { return supplierId; }
	public void setSupplierId(String supplierId) { this.supplierId = supplierId; }	

	// order status
	private String orderStatus;
	public String getOrderStatus() { return orderStatus; }
	public void setOrderStatus(String orderStatus) { this.orderStatus = orderStatus; }
	
	// statusTemp
	private String statusTemp;
	public String getStatusTemp() { return statusTemp; }
	public void setStatusTemp(String statusTemp) { this.statusTemp = statusTemp; }	

	// consingment note
	private String productConsignmentNote;
	public String getProductConsignmentNote() { return productConsignmentNote; 	}
	public void setProductConsignmentNote(String productConsignmentNote) { this.productConsignmentNote = productConsignmentNote; }

	// sort
	private String sort= "date_ordered DESC";
	public String getSort() { return sort; }
	public void setSort(String sort) { this.sort = sort; }
}
