package com.webjaguar.model;

import java.io.Serializable;

public class RangeValue implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private Integer id;
	// min value
	private Double minValue;
	// min value
	private Double maxValue;
	// display value
	private String rangeDisplayValue ;
	
	private String type ;
	
	
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public Double getMinValue() {
		return minValue;
	}
	public void setMinValue(Double minValue) {
		this.minValue = minValue;
	}
	public Double getMaxValue() {
		return maxValue;
	}
	public void setMaxValue(Double maxValue) {
		this.maxValue = maxValue;
	}
	public String getRangeDisplayValue() {
		return rangeDisplayValue;
	}
	public void setRangeDisplayValue(String rangeDisplayValue) {
		this.rangeDisplayValue = rangeDisplayValue;
	}
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	
}