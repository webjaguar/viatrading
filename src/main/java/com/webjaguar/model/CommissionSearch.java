/* Copyright 2007 Advanced E-Media Solutions
 * @author Shahin Naji
 * @since 07.05.2007
 */

package com.webjaguar.model;

public class CommissionSearch {
	
	// sort
	private String sort = "date_ordered DESC";
	public String getSort() { return sort; }
	public void setSort(String sort) { this.sort = sort; }	
	
	// status
	private String status;
	public String getStatus() { return status; }
	public void setStatus(String status) { this.status = status; }
	
	// levels
	private String level = "1";
	public String getLevel() {	return level; }
	public void setLevel(String level) { this.level = level; }
	
	// order number
	private String orderNum = "";
	public String getOrderNum() { return orderNum;	}
	public void setOrderNum(String orderNum) {	this.orderNum = orderNum; }
	
	// page 
	private int page = 1;
	public int getPage() { return page; }
	public void setPage(int page) { this.page = page; }
	
	// page size
	private int pageSize = 10;
	public int getPageSize() { return pageSize; }
	public void setPageSize(int pageSize) { this.pageSize = pageSize; }	
	
}
