package com.webjaguar.model;

import java.util.Date;

public class ViaDeal {

	private static final long serialVersionUID = 1L;
	private Integer dealId;
	private String title;
	private String parentSku;
	private Double buyAmount;
	private Integer buyQuantity;
	private Double discount;
	private boolean discountType;
	private Date startDate;
	private Date endDate;
	private String htmlCode;
	private String parentHtmlCode;
	private boolean triggerType;
	private Double triggerThreshold;
	
	public Integer getDealId() {
		return dealId;
	}
	public void setDealId(Integer dealId) {
		this.dealId = dealId;
	}
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	
	public Double getBuyAmount() {
		return buyAmount;
	}
	public void setBuyAmount(Double buyAmount) {
		this.buyAmount = buyAmount;
	}
	public Integer getBuyQuantity() {
		return buyQuantity;
	}
	public void setBuyQuantity(Integer buyQuantity) {
		this.buyQuantity = buyQuantity;
	}

	public Double getDiscount() {
		return discount;
	}
	public void setDiscount(Double discount) {
		this.discount = discount;
	}
	public boolean isDiscountType() {
		return discountType;
	}
	public void setDiscountType(boolean discountType) {
		this.discountType = discountType;
	}
	public Date getStartDate() {
		return startDate;
	}
	public void setStartDate(Date startDate) {
		this.startDate = startDate;
	}
	public Date getEndDate() {
		return endDate;
	}
	public void setEndDate(Date endDate) {
		this.endDate = endDate;
	}
	public String getHtmlCode() {
		return htmlCode;
	}
	public void setHtmlCode(String htmlCode) {
		this.htmlCode = htmlCode;
	}
	public String getParentSku() {
		return parentSku;
	}
	public void setParentSku(String parentSku) {
		this.parentSku = parentSku;
	}
	public String getParentHtmlCode() {
		return parentHtmlCode;
	}
	public void setParentHtmlCode(String parentHtmlCode) {
		this.parentHtmlCode = parentHtmlCode;
	}
	public boolean isTriggerType() {
		return triggerType;
	}
	public void setTriggerType(boolean triggerType) {
		this.triggerType = triggerType;
	}
	public Double getTriggerThreshold() {
		return triggerThreshold;
	}
	public void setTriggerThreshold(Double triggerThreshold) {
		this.triggerThreshold = triggerThreshold;
	}
	public boolean isInEffect() {
	// today's date
	com.webjaguar.model.DateBean today = new com.webjaguar.model.DateBean( );
	int start = this.startDate.compareTo( today.getTime() );
	int end = this.endDate.compareTo( today.getTime() );
	if ( start == 0 )
		return true;
	else if ( start > 0 || end < 0  )
		return false;
	else
		return true;
	}
		
}
