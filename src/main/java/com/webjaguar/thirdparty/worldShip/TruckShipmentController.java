package com.webjaguar.thirdparty.worldShip;

import java.util.HashMap;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.propertyeditors.CustomNumberEditor;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindException;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.ServletRequestDataBinder;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.support.SessionStatus;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.SimpleFormController;
import org.springframework.web.servlet.view.RedirectView;

import com.webjaguar.dao.OrderDao;
import com.webjaguar.logic.WebJaguarFacade;
import com.webjaguar.model.UpsOrder;
import com.webjaguar.web.form.FaqForm;
import com.webjaguar.web.validator.TruckValidator;

public class TruckShipmentController extends SimpleFormController {
	
	private WebJaguarFacade webJaguar;
	public void setWebJaguar(WebJaguarFacade webJaguar) { this.webJaguar = webJaguar; }
	
	public TruckShipmentController() {
		setSessionForm(true);
		setCommandName("form");
		setCommandClass(UpsOrder.class);
		setFormView("frontend/truck/form");
		setSuccessView("truck.jhtm");
	}
	
	public ModelAndView onSubmit(HttpServletRequest request, HttpServletResponse 
			response, Object command, BindException errors)  throws ServletException, Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		UpsOrder form = (UpsOrder) command;
		
		if(request.getParameter("_reset") != null) {
			return new ModelAndView(new RedirectView(getSuccessView()));
		}

		form.setAddedBy("Form");
		webJaguar.importUpsOrders(form);
		map.put("message", "Your form successfully submitted.");

		return showForm(request, response, errors, map);
	}

	protected boolean suppressBinding(HttpServletRequest request) {
		if (request.getParameter("_cancel") != null || request.getParameter("_reset") != null) {
			return true;			
		}
		return false;
	}
	
	protected void initBinder(HttpServletRequest request, ServletRequestDataBinder binder) throws Exception {

	}	
	
    protected Map referenceData(HttpServletRequest request) throws Exception {
    	Map<String, Object> map = new HashMap<String, Object>();
    	Map<String, Object> myModel = new HashMap<String, Object>();
    	myModel.put("mainCategories", this.webJaguar.getMainCategoryLinks(request,null));
		map.put("model", myModel);  

		return map;
    }  	
	
	protected Object formBackingObject(HttpServletRequest request) throws Exception {
		
		UpsOrder form = new UpsOrder();
		return form;
	}
}