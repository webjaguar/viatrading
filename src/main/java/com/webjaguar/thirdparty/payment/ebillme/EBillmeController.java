/* Copyright 2010 Advanced E-Media Solutions
 * @author Eduardo Asprec
 * @since 01.19.2010
 */


package com.webjaguar.thirdparty.payment.ebillme;

import java.io.*;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.mail.internet.MimeMessage;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.mail.javamail.JavaMailSenderImpl;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.web.bind.ServletRequestUtils;
import org.springframework.web.context.support.WebApplicationObjectSupport;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.Controller;

import com.webjaguar.logic.WebJaguarFacade;
import com.webjaguar.model.Configuration;
import com.webjaguar.model.Customer;
import com.webjaguar.model.Layout;
import com.webjaguar.model.MultiStore;
import com.webjaguar.model.Order;
import com.webjaguar.model.OrderStatus;
import com.webjaguar.model.SiteMessage;
import com.webjaguar.model.UserSession;

public class EBillmeController extends WebApplicationObjectSupport implements Controller {
	
	WebJaguarFacade webJaguar;
	public void setWebJaguar(WebJaguarFacade webJaguar){ this.webJaguar = webJaguar; }
	
	private JavaMailSenderImpl mailSender;
	public void setMailSender(JavaMailSenderImpl mailSender) { this.mailSender = mailSender; }

	public ModelAndView handleRequest(HttpServletRequest request, HttpServletResponse response) throws Exception {	
    	
		// site configuration
		Map<String, Configuration> siteConfig = (Map<String, Configuration>) request.getAttribute("siteConfig");
		Map<String, Object> model = new HashMap<String, Object>();
		
		// net commerce response
		// making this an AbstractCommandController w/ setCommandClass EBillme causes duplicated values
		// due to the silent and regular post. i.e. trans.ordernumber = "1069142,1069142"
		EBillme trans = new EBillme();
		trans.setOrdernumber(ServletRequestUtils.getIntParameter(request, "ordernumber", -1));
		trans.setOrderrefid(ServletRequestUtils.getStringParameter(request, "orderrefid", ""));
		trans.setBuyer_accountnumber(ServletRequestUtils.getStringParameter(request, "buyer_accountnumber", ""));
		trans.setAuthstatus(ServletRequestUtils.getStringParameter(request, "authstatus", ""));
		String silentpost = ServletRequestUtils.getStringParameter(request, "silentpost", "");
		
		Order order = null;
		try {
			order = this.webJaguar.getOrder(trans.getOrdernumber(), null);
		} catch (Exception e) {
			// do nothing
		}
		
		if (request.getMethod().equalsIgnoreCase("POST") && order != null 
						&& order.getPaymentMethod().equalsIgnoreCase("ebillme")) {
			if (silentpost.equals("yes")) {
				if (order.geteBillme() == null) {
					// update at first silent post only
					// update status
					OrderStatus orderStatus = new OrderStatus();
	    			orderStatus.setOrderId(order.getOrderId());
	    			orderStatus.setStatus("p"); // pending
	    			SiteMessage siteMessage = null;
	    			try {
	    				Integer messageId = null;
	    				// multi store
	    				List<MultiStore> multiStores = this.webJaguar.getMultiStore(order.getHost());
	    				MultiStore multiStore = null;
	    				if (multiStores.size() > 0) {
	    					multiStore = multiStores.get(0);
	    					messageId = multiStore.getMidNewOrders();
	    				} else {
		    				messageId = Integer.parseInt(siteConfig.get("SITE_MESSAGE_ID_FOR_NEW_ORDERS").getValue());		    					
	    				}
	    				siteMessage = this.webJaguar.getSiteMessageById(messageId);
	    				if (siteMessage != null) {
	    					// send email
	    					orderStatus.setSubject(siteMessage.getSubject());
	    					orderStatus.setMessage(siteMessage.getMessage());
	    					orderStatus.setHtmlMessage(siteMessage.isHtml());
	    					if (notifyCustomer(orderStatus, siteConfig, order, multiStore)) {
		    					orderStatus.setUserNotified(true);		    							
	    					}
	    				}
	    			} catch (NumberFormatException e) {
	    				// do nothing
	    			}	
	    			this.webJaguar.updateEbillmePayment(trans, orderStatus);
					
					// buy safe
					if( !siteConfig.get("BUY_SAFE_URL").getValue().equals("") ) {
						try {
							this.webJaguar.setShoppingCartCheckout( order, mailSender );
						} catch (Exception e) { 
							e.printStackTrace();
							OrderStatus status = new OrderStatus();
							status.setOrderId( order.getOrderId() );
							status.setStatus( "p" ); // pending
							if( (order.getWantsBond() != null) && (order.getWantsBond() == true) ) {
								status.setComments("BuySafe Bond was selected but cancelled as system failed to connect Buysafe. Please check the amount charged and refund the bond cost to Customer.");
							} else {
								status.setComments("BuySafe Bond was not selected. System failed to connect Buysafe");
							}
							order.setWantsBond(false);
							order.setBondCost(null);
							order.setGrandTotal();
							
							this.webJaguar.cancelBuySafeBond(order, status);
						}
					}
					// clear shopping cart items from database
					this.webJaguar.deleteShoppingCart(order.getUserId(), order.getManufacturerName());
				}
				saveEBillmeTransaction(request);
			} else {
				UserSession userSession = this.webJaguar.getUserSession(request);
				if (userSession != null && order.getUserId().compareTo(userSession.getUserid()) == 0) {
					// show order if still logged in
					model.put("order", order);
					model.put("username", userSession.getUsername());
					model.put("countries", this.webJaguar.getCountryMap());
					
					// multi store
					model.put("invoiceLayout", this.webJaguar.getSystemLayout("finvoice", request.getHeader("host"), request));
					return new ModelAndView("frontend/checkout/finalinvoice", model);
				} else {
					model.put("order", order);
					
					Map<String, Object> model2 = new HashMap<String, Object>();
					model2.put("mainCategories", this.webJaguar.getMainCategoryLinks(request,null));
					model.put("model", model2);
					
					Layout layout = (Layout) request.getAttribute("layout");
					if (siteConfig.get("LEFTBAR_HIDE_ON_SYSTEM_PAGES").getValue().equals("true")) {
						layout.setLeftBarTopHtml("");
						layout.setLeftBarBottomHtml("");
						layout.setHideLeftBar(true);
					}
					
					return new ModelAndView("frontend/checkout/eBillmeInvalid", model);
				}
			}
		}
		
		return null;
    }
    
    private void saveEBillmeTransaction(HttpServletRequest request) throws Exception {
		File eBillmeDir = new File(getServletContext().getRealPath("/eBillme"));	
		if (!eBillmeDir.exists()) {
			eBillmeDir.mkdir();
		}
    	FileOutputStream fo = new FileOutputStream(new File(eBillmeDir, "silentpost"), true);
    	PrintWriter pw = new PrintWriter(fo);
    	pw.write("IP Address=" + request.getRemoteAddr() + "\n");
		Enumeration en = request.getParameterNames();
		while(en.hasMoreElements()){
		    String paramName = (String)en.nextElement();
		    String paramValue = request.getParameter(paramName);
		    pw.write(paramName + "=" + paramValue + "\n");
		}
		pw.write("--------------------------------------------\n");
		pw.close();
    	fo.close();
    }
    
	private boolean notifyCustomer(OrderStatus orderStatus, Map<String, Configuration> siteConfig, Order order, MultiStore multiStore) {

		Customer customer = this.webJaguar.getCustomerById(order.getUserId());
		
		if (!customer.isEmailNotify()) {
			// do not send email
			return false;
		}
		
		// send email
    	String contactEmail = siteConfig.get("CONTACT_EMAIL").getValue();
    	String secureUrl =this.webJaguar.getSecureUrl(siteConfig, multiStore);
    	SimpleDateFormat dateFormatter = new SimpleDateFormat("MMM d, yyyy HH:mm:ss a z");

    	StringBuffer messageBuffer = new StringBuffer();
    	messageBuffer.append(dateFormatter.format(new Date()) + "\n\n");
    	
    	// replace dyamic elements
    	try {
    		this.webJaguar.replaceDynamicElement(orderStatus, customer, null, order, secureUrl, null);
    	} catch (Exception e) {e.printStackTrace();}
    	messageBuffer.append(orderStatus.getMessage());
    	
    	try {
	    	// construct email message
	       	MimeMessage mms = mailSender.createMimeMessage();
			MimeMessageHelper helper = new MimeMessageHelper(mms, true, "UTF-8");
			helper.setTo(customer.getUsername());
			helper.setFrom(contactEmail);
			helper.setBcc(contactEmail);
			helper.setSubject(orderStatus.getSubject());
	    	helper.setText(messageBuffer.toString(), orderStatus.isHtmlMessage());
			
			mailSender.send(mms);
		} catch (Exception ex) {
			// do nothing, customer not notified
			return false;
		} 		
		return true;
	}
}
