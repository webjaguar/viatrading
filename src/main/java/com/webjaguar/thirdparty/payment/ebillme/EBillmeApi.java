/* Copyright 2010 Advanced E-Media Solutions
 * @author Eduardo Asprec
 * @since 09.08.2010
 */

package com.webjaguar.thirdparty.payment.ebillme;

import java.io.StringReader;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.Map;

import org.jdom.Document;
import org.jdom.Element;
import org.jdom.Namespace;
import org.jdom.input.SAXBuilder;

import com.webjaguar.model.Configuration;
import com.webjaguar.model.Order;

public class EBillmeApi {
	
    public EBillme submitOrder(Order order,Map<String, Configuration> siteConfig, String email, String ipAddress) throws Exception {
    	EBillme trans = new EBillme();
    	
    	NumberFormat nf = new DecimalFormat("#0.00");
    	
		StringBuffer xml = new StringBuffer();
		xml.append("<submitorder_request xmlns=\"http://ebillme.com/ws/v3\">");
		xml.append("  <apiversion>3.0</apiversion>");
		if (siteConfig.get("EBILLME_URL").getValue().equals("https://my.eBillme.com/Auth/")) {
			// production
			xml.append("  <transtype>P</transtype>");
		} else {
			// test
			xml.append("  <transtype>T</transtype>");				
		}
		xml.append("  <authorization>");
		xml.append("    <merchanttoken>" + siteConfig.get("EBILLME_MERCHANTTOKEN").getValue() + "</merchanttoken>");
		xml.append("  </authorization>");
		xml.append("  <order>");
		xml.append("    <ordernumber>" + order.getOrderId() + "</ordernumber>");
		xml.append("    <totalprice>" + nf.format(order.getGrandTotal()) + "</totalprice>");
		xml.append("    <currency>" + siteConfig.get("CURRENCY").getValue() + "</currency>");
		xml.append("    <commandtype>_cTrans</commandtype>");
		xml.append("    <ipaddress>" + ipAddress + "</ipaddress>");
		xml.append("    <buyerdetails>");
		xml.append("      <firstname>" + order.getBilling().getFirstName() + "</firstname>");
		xml.append("      <lastname>" + order.getBilling().getLastName() + "</lastname>");
		xml.append("      <email>" + email + "</email>");
		xml.append("      <address1>" + order.getBilling().getAddr1() + "</address1>");
		xml.append("      <address2>" + order.getBilling().getAddr2() + "</address2>");
		xml.append("      <city>" + order.getBilling().getCity() + "</city>");
		xml.append("      <state>" + order.getBilling().getStateProvince() + "</state>");
		xml.append("      <country>" + order.getBilling().getCountry() + "</country>");
		xml.append("      <zipcode>" + order.getBilling().getZip() + "</zipcode>");
		xml.append("      <phone1>" + order.getBilling().getPhone() + "</phone1>");
		xml.append("      <merchantrating>3</merchantrating>");
		xml.append("    </buyerdetails>");
		xml.append("    <shippingdetails>");
		xml.append("      <firstname>" + order.getShipping().getFirstName() + "</firstname>");
		xml.append("      <lastname>" + order.getShipping().getLastName() + "</lastname>");
		xml.append("      <email>" + email + "</email>");
		xml.append("      <address1>" + order.getShipping().getAddr1() + "</address1>");
		xml.append("      <address2>" + order.getShipping().getAddr2() + "</address2>");
		xml.append("      <city>" + order.getShipping().getCity() + "</city>");
		xml.append("      <state>" + order.getShipping().getStateProvince() + "</state>");
		xml.append("      <country>" + order.getShipping().getCountry() + "</country>");
		xml.append("      <zipcode>" + order.getShipping().getZip() + "</zipcode>");
		xml.append("      <shippingmethod>STANDARD</shippingmethod>");
		xml.append("    </shippingdetails>");
		xml.append("  </order>");
		xml.append("</submitorder_request>");
		String responseXml = null;
		
		if (siteConfig.get("EBILLME_URL").getValue().equals("https://my.eBillme.com/Auth/")) {
			// production
			com.ebillme.production.v3.EBillmeServiceV3 ebillmeServiceV3 = new com.ebillme.production.v3.EBillmeServiceV3ServiceLocator().geteBillmeServiceV3();
			responseXml = ebillmeServiceV3.submitOrder(xml.toString());							
		} else {
			// test
			com.ebillme.v3.uat.EBillmeServiceV3 ebillmeServiceV3 = new com.ebillme.v3.uat.EBillmeServiceV3ServiceLocator().geteBillmeServiceV3();
			responseXml = ebillmeServiceV3.submitOrder(xml.toString());				
		}
    	
		SAXBuilder builder = new SAXBuilder(false);
		Document doc = builder.build(new StringReader(responseXml));
		Element orderElement = doc.getRootElement().getChild("order", Namespace.getNamespace("http://ebillme.com/ws/v3")); 
		
		trans.setOrdernumber(Integer.parseInt(orderElement.getChildText("ordernumber", Namespace.getNamespace("http://ebillme.com/ws/v3"))));
		trans.setOrderrefid(orderElement.getChildText("orderrefid", Namespace.getNamespace("http://ebillme.com/ws/v3")));
		trans.setBuyer_accountnumber(orderElement.getChild("buyerdetails", Namespace.getNamespace("http://ebillme.com/ws/v3"))
				.getChildText("accountnumber", Namespace.getNamespace("http://ebillme.com/ws/v3")));
		trans.setAuthstatus(orderElement.getChild("authdetails", Namespace.getNamespace("http://ebillme.com/ws/v3"))
				.getChildText("authstatus", Namespace.getNamespace("http://ebillme.com/ws/v3")));
		
		return trans;
    }

}
