/* Copyright 2010 Advanced E-Media Solutions
 * @author Eduardo Asprec
 * @since 01.19.2010
 */


package com.webjaguar.thirdparty.payment.ebillme;

import java.io.Serializable;

public class EBillme implements Serializable {
	
	private int ordernumber;
	private String orderrefid;
	private String buyer_accountnumber;
	private String authstatus;
	private String xmlFile;
	
	public int getOrdernumber() {
		return ordernumber;
	}
	public void setOrdernumber(int ordernumber) {
		this.ordernumber = ordernumber;
	}
	public String getOrderrefid() {
		return orderrefid;
	}
	public void setOrderrefid(String orderrefid) {
		this.orderrefid = orderrefid;
	}
	public String getBuyer_accountnumber() {
		return buyer_accountnumber;
	}
	public void setBuyer_accountnumber(String buyerAccountnumber) {
		buyer_accountnumber = buyerAccountnumber;
	}
	public String getAuthstatus() {
		return authstatus;
	}
	public void setAuthstatus(String authstatus) {
		this.authstatus = authstatus;
	}
	public String getXmlFile() {
		return xmlFile;
	}
	public void setXmlFile(String xmlFile) {
		this.xmlFile = xmlFile;
	}

}
