/* Copyright 2010 Advanced E-Media Solutions
 * @author Eduardo Asprec
 * @since 08.24.2010
 */

package com.webjaguar.thirdparty.payment.cardinal;

import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.springframework.mail.MailException;
import org.springframework.mail.MailSender;
import org.springframework.mail.SimpleMailMessage;

import com.cardinalcommerce.client.CentinelRequest;
import com.cardinalcommerce.client.CentinelResponse;
import com.webjaguar.model.Configuration;
import com.webjaguar.model.Order;

public class CentinelApi {
	
	public boolean lookup(HttpServletRequest request, Order order, Map<String, Configuration> siteConfig, MailSender mailSender) {
		boolean enrolled = false;
		
		try {
			NumberFormat nf = new DecimalFormat("#0");
			
			// cardinal centinel
			CentinelRequest centinelRequest = new CentinelRequest();
			centinelRequest.add("MsgType", "cmpi_lookup"); 
			centinelRequest.add("Version", "1.7"); 
			centinelRequest.add("ProcessorId", siteConfig.get("CENTINEL_PROCESSORID").getValue().trim()); 
			centinelRequest.add("MerchantId", siteConfig.get("CENTINEL_MERCHANTID").getValue().trim()); 
			centinelRequest.add("TransactionPwd", siteConfig.get("CENTINEL_TRANSACTIONPWD").getValue().trim()); 
			centinelRequest.add("TransactionType", "C"); 
			centinelRequest.add("Amount", nf.format(order.getGrandTotal()*100)); 
			centinelRequest.add("CurrencyCode", "840");	// ISO 4217 USD
			centinelRequest.add("CardNumber", order.getCreditCard().getNumber()); 
			centinelRequest.add("CardExpMonth", order.getCreditCard().getExpireMonth()); 
			centinelRequest.add("CardExpYear", "20" + order.getCreditCard().getExpireYear()); 
			centinelRequest.add("OrderNumber", request.getSession().getId()); 
			
			if (request.getParameter("_centinelDebug") != null && request.getParameter("_centinelDebug").equals("true")) {
				System.out.println(centinelRequest.getFormattedRequest());	
			}
			
			CentinelResponse centinelResponse = centinelRequest.sendHTTP(siteConfig.get("CENTINEL_URL").getValue(), 5000, 10000);
			
			if (centinelResponse.getValue("ErrorNo").equals("0")) {
				// additional processing required
				
				order.getCreditCard().setEci(centinelResponse.getValue("EciFlag"));
				
				if (centinelResponse.getValue("Enrolled").equalsIgnoreCase("Y")) {
					// cardholder enrolled
					enrolled = true;
					request.setAttribute("centinelOrderId", centinelResponse.getValue("OrderId"));
					request.getSession().setAttribute("centinelPayload", centinelResponse.getValue("Payload"));
					request.getSession().setAttribute("centinelACSUrl", centinelResponse.getValue("ACSUrl"));
				} else if (centinelResponse.getValue("Enrolled").equalsIgnoreCase("N")) {
					// not enrolled
					
				} else if (centinelResponse.getValue("Enrolled").equalsIgnoreCase("U")) {
					// cardholder authentication unavailable
				}
			} else if (centinelResponse.getValue("ErrorNo").equals("1001") && centinelResponse.getValue("Enrolled").equalsIgnoreCase("U")) {
				order.getCreditCard().setEci(centinelResponse.getValue("EciFlag"));
				// contact support
				notifyAdmin("CENTINEL lookup() on " + siteConfig.get("SITE_URL").getValue(), centinelRequest.getFormattedRequest() + "\n" + centinelResponse.getFormattedResponse(), mailSender);
			}

			if (request.getParameter("_centinelDebug") != null && request.getParameter("_centinelDebug").equals("true")) {
				System.out.println(centinelResponse.getFormattedResponse());	
			}
			
		} catch (Exception e) {
			e.printStackTrace();
		}		
		
		return enrolled;
	}
	
	public boolean authenticate(HttpServletRequest request, Order order, Map<String, Configuration> siteConfig, String OrderId, String PAResPayload) {

		boolean failure = true;
		
		// cardinal centinel
		CentinelRequest centinelRequest = new CentinelRequest();
		centinelRequest.add("MsgType", "cmpi_authenticate"); 
		centinelRequest.add("Version", "1.7"); 
		centinelRequest.add("ProcessorId", siteConfig.get("CENTINEL_PROCESSORID").getValue().trim()); 
		centinelRequest.add("MerchantId", siteConfig.get("CENTINEL_MERCHANTID").getValue().trim()); 
		centinelRequest.add("TransactionPwd", siteConfig.get("CENTINEL_TRANSACTIONPWD").getValue().trim()); 
		centinelRequest.add("TransactionType", "C"); 
		centinelRequest.add("OrderId", OrderId); 
		centinelRequest.add("PAResPayload", PAResPayload); 
		
		if (request.getParameter("_centinelDebug") != null && request.getParameter("_centinelDebug").equals("true")) {
			System.out.println(centinelRequest.getFormattedRequest());	
		}
		
		CentinelResponse centinelResponse = centinelRequest.sendHTTP(siteConfig.get("CENTINEL_URL").getValue(), 5000, 10000);

		if (centinelResponse.getValue("ErrorNo").equals("0")) {

			order.getCreditCard().setEci(centinelResponse.getValue("EciFlag"));
			
			if (centinelResponse.getValue("SignatureVerification").equalsIgnoreCase("Y")) {
				
				if (centinelResponse.getValue("PAResStatus").equalsIgnoreCase("Y")) {
					// Cardholder Authentication completed successfully.
					order.getCreditCard().setCavv(centinelResponse.getValue("Cavv"));
					order.getCreditCard().setXid(centinelResponse.getValue("Xid"));
					failure = false;
				} else if (centinelResponse.getValue("PAResStatus").equalsIgnoreCase("A")) {
					// Cardholder attempts authentication completed successfully.
					order.getCreditCard().setCavv(centinelResponse.getValue("Cavv"));
					order.getCreditCard().setXid(centinelResponse.getValue("Xid"));
					failure = false;						
				} else if (centinelResponse.getValue("PAResStatus").equalsIgnoreCase("N")) {
					// Cardholder failed authentication.
				} else if (centinelResponse.getValue("PAResStatus").equalsIgnoreCase("U")) {
					// Cardholder was unable to be authenticated.
				}
				
			} else {
				// Fraud check failure indicates that the transaction results can not be trusted
				// transaction should not be authorized			
			}
		} else {
			// transaction should not be authorized
		}
		
		if (request.getParameter("_centinelDebug") != null && request.getParameter("_centinelDebug").equals("true")) {
			System.out.println(centinelResponse.getFormattedResponse());	
		}
		
		return failure;
	}
	
	private void notifyAdmin(String subject, String message, MailSender mailSender) {
		// send email notification
    	String contactEmail = "developers@advancedemedia.com";
    	SimpleDateFormat dateFormatter = new SimpleDateFormat("MMM d, yyyy HH:mm:ss a z");
    	
		SimpleMailMessage msg = new SimpleMailMessage();
		msg.setTo(contactEmail);
		msg.setFrom(contactEmail);
		msg.setSubject(subject);
		msg.setText(dateFormatter.format(new Date()) + "\n\n" + message);
		try {
			mailSender.send(msg);
		} catch (MailException ex) {
			// do nothing
		}  		
	}     
}
