/* 
 * Copyright 2009 Advanced E-Media Solutions
 * @author Eduardo Asprec
 * @since 05.11.2009
 */

package com.webjaguar.thirdparty.payment.payrover;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.net.URL;
import java.net.URLConnection;
import java.net.URLEncoder;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.Map;

import com.webjaguar.model.Configuration;
import com.webjaguar.model.Order;

public class PayRoverApi {

	public boolean authorize(Order order, boolean authorizeOnly, Map<String, Configuration> siteConfig) throws Exception {

		boolean authorizeSuccess = false;
		
		String x_login = siteConfig.get("CREDIT_ACCOUNT").getValue();
		String x_description = siteConfig.get("SITE_URL").getValue();
		
		PayRoverResponse payRoverResp = new PayRoverResponse();
		
		// authorize and capture
		String transType = "AUTH_CAPTURE";
		if (authorizeOnly) {
			// authorize only
			transType = "AUTH_ONLY";			
		}
		if (order.getCreditCard().getPaymentStatus() != null &&
				order.getCreditCard().getPaymentStatus().equalsIgnoreCase("AUTHORIZED")) {
			// capture a previously authorized transaction
			transType = "PRIOR_AUTH_CAPTURE";
		}
		
		// construct StringBuffer to send to PayRover
		StringBuffer sb = buildSb(order, transType, x_login, x_description);
		// open secure connection and post the data in the StringBuffer
		URLConnection connection = sendSb(sb);
		// process and read the gateway response
		authorizeSuccess = getResponse(connection, order, payRoverResp);
		// return flag

		order.getCreditCard().setPaymentNote("Response Code " + 
				payRoverResp.getReasonCode() + " - " + payRoverResp.getReasonText());
		
		if (authorizeSuccess) {
			// approved
			order.getCreditCard().setTransId(payRoverResp.getTransID());
			// check transaction type
			if (transType.equalsIgnoreCase("AUTH_CAPTURE")
					|| transType.equalsIgnoreCase("PRIOR_AUTH_CAPTURE")) {
				order.getCreditCard().setPaymentStatus("CAPTURED");							
			} else if (transType.equalsIgnoreCase( "AUTH_ONLY")) {
				order.getCreditCard().setPaymentStatus("AUTHORIZED");								
			}
		}
		
		return authorizeSuccess;
	}

	private StringBuffer buildSb(Order order, String transType, String x_login, String x_description) throws UnsupportedEncodingException {
		NumberFormat nf = new DecimalFormat("#0.00");
		
		// create a new StringBuffer
		StringBuffer sb = new StringBuffer();
		// mandatory name/value pairs for all AIM CC transactions
		sb.append("x_login=" + URLEncoder.encode(x_login, "UTF-8"));
		sb.append("&x_version=3.1");
		// sb.append("&x_test_request=TRUE"); uncomment for test mode
		sb.append("&x_method=CC");
		sb.append("&x_type=" + transType);
		sb.append("&x_amount=" + nf.format(order.getBalance()));
		sb.append("&x_invoice_num=" + order.getOrderId());
		sb.append("&x_delim_data=TRUE");
		sb.append("&x_delim_char=|");
		sb.append("&x_relay_response=FALSE");
		// credit card information
		sb.append("&x_card_num=" + URLEncoder.encode(order.getCreditCard().getNumber(), "UTF-8"));
		sb.append("&x_card_code=" + URLEncoder.encode(order.getCreditCard().getCardCode(), "UTF-8"));
		sb.append("&x_exp_date=" + URLEncoder.encode(order.getCreditCard().getExpireMonth() + order.getCreditCard().getExpireYear(), "UTF-8"));
		sb.append("&x_first_name=" + URLEncoder.encode(order.getBilling().getFirstName(), "UTF-8"));
		sb.append("&x_last_name=" + URLEncoder.encode(order.getBilling().getLastName(), "UTF-8"));
		sb.append("&x_company=" + URLEncoder.encode(order.getBilling().getCompany(), "UTF-8"));
		sb.append("&x_address=" + URLEncoder.encode(order.getBilling().getAddr1(), "UTF-8"));
		sb.append("&x_city=" + URLEncoder.encode(order.getBilling().getCity(), "UTF-8"));
		sb.append("&x_state=" + URLEncoder.encode(order.getBilling().getStateProvince(), "UTF-8"));
		sb.append("&x_zip=" + URLEncoder.encode(order.getBilling().getZip(), "UTF-8"));
		sb.append("&x_country=" + URLEncoder.encode(order.getBilling().getCountry(), "UTF-8"));
		sb.append("&x_phone=" + URLEncoder.encode(order.getBilling().getPhone(), "UTF-8"));
		sb.append("&x_fax=" + URLEncoder.encode(order.getBilling().getFax(), "UTF-8"));
		sb.append("&x_description=" + URLEncoder.encode(x_description, "UTF-8"));
		sb.append("&x_email=none");
		// if prior_auth_capture, need trans_id
		if (transType.equals("PRIOR_AUTH_CAPTURE")) {
			sb.append("&x_trans_id=" + order.getCreditCard().getTransId());
		}
		// return complete StringBuffer
		return sb;
	}

	private URLConnection sendSb(StringBuffer sb) throws IOException {
		// open secure connection
		URL url = new URL("https://secure.usaepay.com/gateway/transact.dll");
		URLConnection connection = url.openConnection();
		connection.setDoOutput(true);
		connection.setUseCaches(false);
		// not necessarily required but fixes a bug with some servers
		connection.setRequestProperty("Content-Type", "application/x-www-form-urlencoded");
		// post the data in the string buffer
		DataOutputStream out = new DataOutputStream(connection.getOutputStream());
		out.write(sb.toString().getBytes());
		out.flush();
		out.close();
		return connection;
	}

	private boolean getResponse(URLConnection connection, Order order, PayRoverResponse payRoverResp) throws IOException {
		// receive response String from payrover
		BufferedReader in = new BufferedReader(new InputStreamReader(connection.getInputStream()));
		String line;
		line = in.readLine();
		in.close();
		
		String[] resp = line.split("[|]");

		payRoverResp.setReasonCode(resp[2].replace("\"", ""));
		payRoverResp.setReasonText(resp[3].replace("\"", ""));
		payRoverResp.setTransID(resp[6].replace("\"", ""));

		if (resp[0].equals("\"1\"")) {
			return true;
		} else {
			return false;
		}
	}
}
