/* 
 * Copyright 2009 Advanced E-Media Solutions
 * @author Eduardo Asprec
 * @since 05.11.2009
 */

package com.webjaguar.thirdparty.payment.payrover;

public class PayRoverResponse {
	
	private String reasonCode;
	private String reasonText;
	private String transID;
	
	public String getReasonCode()
	{
		return reasonCode;
	}
	public void setReasonCode(String reasonCode)
	{
		this.reasonCode = reasonCode;
	}
	public String getReasonText()
	{
		return reasonText;
	}
	public void setReasonText(String reasonText)
	{
		this.reasonText = reasonText;
	}
	public String getTransID()
	{
		return transID;
	}
	public void setTransID(String transID)
	{
		this.transID = transID;
	}
}
