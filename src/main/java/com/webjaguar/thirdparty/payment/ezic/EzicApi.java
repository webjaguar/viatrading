/* Copyright 2008 Advanced E-Media Solutions
 * @author Eduardo Asprec
 * @since 08.05.2008
 */

package com.webjaguar.thirdparty.payment.ezic;

import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.Map;

import com.ezic.direct.V3Client;
import com.ezic.direct.V3Client.Request;
import com.ezic.direct.V3Client.Response;
import com.ezic.net.NetworkClient;
import com.ezic.net.PostConnectionException;
import com.ezic.net.PreConnectionException;
import com.ezic.net.ServerException;

import com.webjaguar.model.Configuration;
import com.webjaguar.model.Order;

public class EzicApi {
	public boolean process(Order order, String custEmail, boolean authorizeOnly, Map<String, Configuration> siteConfig) 
		throws Exception {
		
		NumberFormat nf = new DecimalFormat("#0.00");
		
		boolean success = false;
		
		// authorize and capture
		String transType = "S";	// sale
		if (authorizeOnly) {
			// authorize only
			transType = "A";	// auth	
		}
		if (order.getCreditCard().getPaymentStatus() != null &&
				order.getCreditCard().getPaymentStatus().equalsIgnoreCase("AUTHORIZED")) {
			// capture a previously authorized transaction
			transType = "D";	// capture
		}

		V3Client client = new V3Client();
		client.setProtocol(NetworkClient.PROTOCOL_HTTPS);
		
		Request in = new Request();
		Response out = new Response();
		
		in.set(Request.ACCOUNT_ID, siteConfig.get("CREDIT_ACCOUNT").getValue());	// Your account number
		in.set(Request.AMOUNT, nf.format(order.getBalance()));
		in.set(Request.TRAN_TYPE, transType);
		in.set(Request.PAY_TYPE, "C");	// C: credit card
		in.set(Request.CARD_NUMBER,	order.getCreditCard().getNumber());
		in.set(Request.CARD_EXPIRE,	order.getCreditCard().getExpireMonth() + order.getCreditCard().getExpireYear());	// MMYY format
		in.set(Request.CARD_CVV2, order.getCreditCard().getCardCode());
		in.set(Request.BILL_NAME1, order.getBilling().getFirstName());
		in.set(Request.BILL_NAME2, order.getBilling().getLastName());
		in.set(Request.BILL_STREET, order.getBilling().getAddr1());
		in.set(Request.BILL_CITY, order.getBilling().getCity());
		in.set(Request.BILL_STATE, order.getBilling().getStateProvince());
		in.set(Request.BILL_ZIP, order.getBilling().getZip());
		in.set(Request.BILL_COUNTRY, order.getBilling().getCountry());	
		in.set(Request.CUSTOMER_PHONE, order.getBilling().getPhone());	
		if (custEmail != null) {
			in.set(Request.CUSTOMER_EMAIL, custEmail);				
		}
		if (order.getOrderId() != null) {
			in.set(Request.DESCRIPTION, order.getOrderId().toString());
		}
		
		if (order.getCreditCard().getTransId() != null) {
			in.set(Request.ORIG_ID,	order.getCreditCard().getTransId());			
		}

		try {
		    boolean good = client.doTrans(in,out);	// perform transaction
		    String statusCode = out.get(Response.STATUS_CODE);
		    if (good) {
		    	// The transaction was successful
		    	if (statusCode.equals("1") || statusCode.equals("T")) {
					// approved
					order.getCreditCard().setTransId(out.get(Response.TRANS_ID));
					if (transType.equals("A")) {
						order.getCreditCard().setPaymentStatus("AUTHORIZED");
					} else if (transType.equals("S") || transType.equals("D")) {
						order.getCreditCard().setPaymentStatus("CAPTURED");
					}
					success = true;
		    	}
		    }
			order.getCreditCard().setPaymentNote("Status Code " + statusCode + " - " + out.get(Response.AUTH_MSG));
		} catch (PreConnectionException x) {
			order.getCreditCard().setPaymentNote("Failed to connect to server, transaction not processed.");
		} catch (PostConnectionException x) {
			order.getCreditCard().setPaymentNote("Connection to server lost, transaction may or may not have been processed.");
		} catch (ServerException x) {
		    // the transaction failed, most likely due to incorrect input, the exception contains more details.
			if (x.toString().startsWith("com.ezic.net.ServerException: ")) {
				order.getCreditCard().setPaymentNote(x.toString().substring(30));
			} else {
				order.getCreditCard().setPaymentNote(x.toString());				
			}
		} 
		
		return success;
	}
}
