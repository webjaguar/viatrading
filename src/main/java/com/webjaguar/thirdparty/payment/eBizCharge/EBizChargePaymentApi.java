package com.webjaguar.thirdparty.payment.eBizCharge;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.rmi.RemoteException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.HashMap;
import java.util.Map;

import javax.xml.rpc.ServiceException;

import com.webjaguar.model.Address;
import com.webjaguar.model.Configuration;
import com.webjaguar.model.Customer;
import com.webjaguar.model.LineItem;
import com.webjaguar.model.Order;
import com.webjaguar.thirdparty.eBizCharge.BANKCARD_ADD_CUSTOMERResponseBANKCARD_ADD_CUSTOMERResult;
import com.webjaguar.thirdparty.eBizCharge.BANKCARD_ADD_CUSTOMER_PAYMENT_METHODResponseBANKCARD_ADD_CUSTOMER_PAYMENT_METHODResult;
import com.webjaguar.thirdparty.eBizCharge.BANKCARD_GET_CUSTOMERResponseBANKCARD_GET_CUSTOMERResult;
import com.webjaguar.thirdparty.eBizCharge.BankCardResponse;
import com.webjaguar.thirdparty.eBizCharge.ChargeDetailInput;
import com.webjaguar.thirdparty.eBizCharge.CreditCardInput;
import com.webjaguar.thirdparty.eBizCharge.CustDetailInput;
import com.webjaguar.thirdparty.eBizCharge.CustomerChargeInput;
import com.webjaguar.thirdparty.eBizCharge.CustomerInput;
import com.webjaguar.thirdparty.eBizCharge.EBIZCHARGE_CUSTOMER_PROCESSINGLocator;
import com.webjaguar.thirdparty.eBizCharge.EBIZCHARGE_CUSTOMER_PROCESSINGSoap;
import com.webjaguar.thirdparty.eBizCharge.EBIZCHARGE_TRANSACTION_PROCESSINGLocator;
import com.webjaguar.thirdparty.eBizCharge.EBIZCHARGE_TRANSACTION_PROCESSINGSoap;
import com.webjaguar.thirdparty.eBizCharge.HashInput;
import com.webjaguar.thirdparty.eBizCharge.ItemDetailInput;
import com.webjaguar.thirdparty.eBizCharge.PaymentMethodInput;

public class EBizChargePaymentApi {

	EBIZCHARGE_TRANSACTION_PROCESSINGLocator processingLocator = new EBIZCHARGE_TRANSACTION_PROCESSINGLocator();
	EBIZCHARGE_CUSTOMER_PROCESSINGLocator customerProcessing = new EBIZCHARGE_CUSTOMER_PROCESSINGLocator();

	static final String hashSeed = "ADVANCED12345";
	static final String hashType = "MD5";
	static String merchantId = null;
	static String clientIP = null;
	static String sourceKey = null;
	static String pin = null;

	public EBizChargePaymentApi(Map<String, Configuration> siteConfig,
			String ipAddress) {
		this.clientIP = ipAddress;
		this.merchantId = siteConfig.get("CREDIT_ACCOUNT").getValue();
		String[] keySplit = siteConfig.get("CREDIT_PASSWORD").getValue()
				.split(",");
		this.sourceKey = keySplit[0];
		this.pin = keySplit[1];
	}

	// Customer doesn't want to save the cc info.
	public boolean BankCardTransactions(Order order, boolean authorizeOnly,
			boolean priorAuth, Map<String, Configuration> siteConfig) {
		BankCardResponse response = new BankCardResponse();
		boolean authorizeSuccess = false;

		// authorize and capture
		try {
			if (authorizeOnly) {
				response = bankCardAuthOnly(order);
			} else if (priorAuth) {
				System.out.println("bankCardPriorAuthOnly: " + order);
				response = bankCardPriorAuthOnly(order);
			} else {
				System.out.println("BankCardSale");
				response = bankCardSale(order);
			}
		} catch (ServiceException e) {
			e.printStackTrace();
		}

		String[] sArray = order.getCreditCard().getNumber().split("");
		StringBuffer finalString = new StringBuffer();
		for (int i = 1; i < sArray.length; i++) {
			if (i <= sArray.length - 5) {
				sArray[i] = "*";
			}
			finalString.append(sArray[i]);
		}
		order.getCreditCard().setNumber(finalString.toString());
		// order.getCreditCard().setCardCode("");
		order.getCreditCard().setExpireMonth("**");
		order.getCreditCard().setExpireYear("**");
		order.getCreditCard().setCardCode("***");
		order.getCreditCard().setType("****");
		// If ApprovalIndicator is A:Approved, D:Declined, E:Error
		if (response.getApprovalIndicator() != null
				&& response.getApprovalIndicator().equalsIgnoreCase("A")) {
			authorizeSuccess = true;
			order.getCreditCard().setAuthCode(response.getAuthCode());
			order.getCreditCard().setAvsCode(response.getAVSIndicator());
			order.getCreditCard().setTransId(response.getReferenceNumber());
			order.getCreditCard().setPaymentNote(
					"Approval Indicator: " + response.getApprovalIndicator()
							+ " / Message: " + response.getMessage());
		} else {
			order.getCreditCard().setPaymentNote(
					"Error: " + response.getErrorCode() + " / Message: "
							+ response.getMessage() + " / AVS Indicator:"
							+ response.getAVSIndicator() + " / CVV Indicator:"
							+ response.getCVVIndicator()
							+ " / Approval Indicator: "
							+ response.getApprovalIndicator());
			System.out.println("******Error Code" + response.getErrorCode()
					+ "Error Message" + response.getMessage()
					+ "/ AVS Indicator:" + response.getAVSIndicator()
					+ "/ CVV Indicator:" + response.getCVVIndicator()
					+ "/ Approval Indicator: "
					+ response.getApprovalIndicator());
		}
		// check transaction type
		if (authorizeSuccess) {
			if (authorizeOnly) {
				order.getCreditCard().setPaymentStatus("AUTHORIZED");
			} else {
				order.getCreditCard().setPaymentStatus("CAPTURED");
			}
		}
		return authorizeSuccess;
	}

	// Customer wish to save his cc info.
	public boolean CustomerBankCardTransactions(Order order, Customer customer,
			boolean authorizeOnly, boolean priorAuth,
			Map<String, Configuration> siteConfig) {
		BankCardResponse response = new BankCardResponse();
		boolean authorizeSuccess = false;
		// authorize and capture
		try {
			if (authorizeOnly) {
				response = bankCardCustomerAuthOnly(order, customer);
			} else if (priorAuth) {
				response = bankCardPriorAuthOnly(order);
			} else {
				response = bankCardCustomerSale(order, customer);
			}
		} catch (ServiceException e) {
			e.printStackTrace();
		}

		String[] sArray = order.getCreditCard().getNumber().split("");
		StringBuffer finalString = new StringBuffer();
		for (int i = 1; i < sArray.length; i++) {
			if (i <= sArray.length - 5) {
				sArray[i] = "*";
			}
			finalString.append(sArray[i]);
		}
		order.getCreditCard().setNumber(finalString.toString());
		// orderForm.getOrder().getCreditCard().setCardCode("");
		order.getCreditCard().setExpireMonth("**");
		order.getCreditCard().setExpireYear("**");
		order.getCreditCard().setCardCode("***");
		order.getCreditCard().setType("****");
		if (response.getErrorCode() != null
				&& response.getErrorCode().equalsIgnoreCase("00000")
				&& response.getMessage().equalsIgnoreCase("approved")
				&& response.getApprovalIndicator().equalsIgnoreCase("A")) {
			authorizeSuccess = true;
			order.getCreditCard().setAuthCode(response.getAuthCode());
			order.getCreditCard().setAvsCode(response.getAVSIndicator());
			order.getCreditCard().setTransId(response.getReferenceNumber());
			order.getCreditCard().setPaymentNote(
					"Approval Indicator: " + response.getApprovalIndicator()
							+ " / Message: " + response.getMessage());
		} else {
			System.out.println("******Error Code" + response.getErrorCode()
					+ "Error Message" + response.getMessage());
			order.getCreditCard().setPaymentNote(
					"Error: " + response.getErrorCode() + " / Message: "
							+ response.getMessage() + " / AVS Indicator:"
							+ response.getAVSIndicator() + " / CVV Indicator:"
							+ response.getCVVIndicator()
							+ " / Approval Indicator: "
							+ response.getApprovalIndicator());
		}
		// check transaction type
		if (authorizeSuccess) {
			if (authorizeOnly) {
				order.getCreditCard().setPaymentStatus("AUTHORIZED");
			} else {
				order.getCreditCard().setPaymentStatus("CAPTURED");
			}
		}

		return authorizeSuccess;
	}

	public BankCardResponse bankCardCustomerAuthOnly(Order order,
			Customer customer) throws ServiceException {
		EBIZCHARGE_CUSTOMER_PROCESSINGLocator customerProcessing = new EBIZCHARGE_CUSTOMER_PROCESSINGLocator();
		EBIZCHARGE_CUSTOMER_PROCESSINGSoap customerProcessingSoap = customerProcessing
				.getEBIZCHARGE_CUSTOMER_PROCESSINGSoap();
		BankCardResponse response = new BankCardResponse();

		try {
			response = customerProcessingSoap.BANKCARD_CUSTOMER_AUTHONLY(
					merchantId, hashInput(), clientIP,
					customerChargeInput(order, customer));
		} catch (RemoteException e) {
			e.printStackTrace();
		}

		return response;
	}

	public BankCardResponse bankCardCustomerPriorAuthSale(Order order,
			Customer customer) throws ServiceException {
		EBIZCHARGE_CUSTOMER_PROCESSINGLocator customerProcessing = new EBIZCHARGE_CUSTOMER_PROCESSINGLocator();
		EBIZCHARGE_CUSTOMER_PROCESSINGSoap customerProcessingSoap = customerProcessing
				.getEBIZCHARGE_CUSTOMER_PROCESSINGSoap();
		BankCardResponse response = new BankCardResponse();
		ItemDetailInput[] itemDetail = getItemDetailInput(order);

		try {
			response = customerProcessingSoap
					.BANKCARD_CUSTOMER_PRIOR_AUTH_SALE(merchantId, hashInput(),
							clientIP, order.getCreditCard().getTransId(),
							customerChargeInput(order, customer), itemDetail);
		} catch (RemoteException e) {
			e.printStackTrace();
		}

		return response;
	}

	public BankCardResponse bankCardCustomerSale(Order order, Customer customer)
			throws ServiceException {
		EBIZCHARGE_CUSTOMER_PROCESSINGLocator customerProcessing = new EBIZCHARGE_CUSTOMER_PROCESSINGLocator();
		EBIZCHARGE_CUSTOMER_PROCESSINGSoap customerProcessingSoap = customerProcessing
				.getEBIZCHARGE_CUSTOMER_PROCESSINGSoap();
		BankCardResponse response = new BankCardResponse();
		ItemDetailInput[] itemDetail = getItemDetailInput(order);

		try {
			response = customerProcessingSoap.BANKCARD_CUSTOMER_SALE(
					merchantId, hashInput(), clientIP,
					customerChargeInput(order, customer), itemDetail);
		} catch (RemoteException e) {
			e.printStackTrace();
		}

		return response;
	}

	public void bankCardAddCustomer(Customer customer, Order order)
			throws ServiceException {
		EBIZCHARGE_CUSTOMER_PROCESSINGLocator customerProcessing = new EBIZCHARGE_CUSTOMER_PROCESSINGLocator();
		EBIZCHARGE_CUSTOMER_PROCESSINGSoap customerProcessingSoap = customerProcessing
				.getEBIZCHARGE_CUSTOMER_PROCESSINGSoap();

		String cusDBNum = null;

		try {
			BANKCARD_ADD_CUSTOMERResponseBANKCARD_ADD_CUSTOMERResult result = customerProcessingSoap
					.BANKCARD_ADD_CUSTOMER(merchantId, hashInput(), clientIP,
							customerInfo(order));
			if (result.getError().isEmpty()) {
				cusDBNum = result.getCustDBNum();
				customer.setGatewayToken(cusDBNum);
				// Adding a Payment method
				bankCardCustomerPaymentMethod(customer, order);
			} else {
				System.out.println("Error: " + result.getError());
			}
		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	public void bankCardGetCustomer(Customer customer, Order order,
			boolean updateCreditCard) throws ServiceException {
		EBIZCHARGE_CUSTOMER_PROCESSINGLocator customerProcessing = new EBIZCHARGE_CUSTOMER_PROCESSINGLocator();
		EBIZCHARGE_CUSTOMER_PROCESSINGSoap customerProcessingSoap = customerProcessing
				.getEBIZCHARGE_CUSTOMER_PROCESSINGSoap();
		BANKCARD_GET_CUSTOMERResponseBANKCARD_GET_CUSTOMERResult response = new BANKCARD_GET_CUSTOMERResponseBANKCARD_GET_CUSTOMERResult();
		try {
			response = customerProcessingSoap.BANKCARD_GET_CUSTOMER(merchantId,
					hashInput(), clientIP, customer.getId().toString(),
					customer.getGatewayToken());

			Map<Integer, String> paymentMethods = new HashMap<Integer, String>();
			if (response.getError() == null
					&& response.getPaymentMethods() != null) {
				PaymentMethodInput[] methods = response.getPaymentMethods();
				if (methods.length > 0) {
					for (int i = 0; i < methods.length; i++) {
						paymentMethods.put(
								Integer.parseInt(methods[i].getMethodID()),
								methods[i].getMethodName());

						if (updateCreditCard
								&& !order.getCcToken().equalsIgnoreCase("-1")
								&& !order.getCcToken().equalsIgnoreCase("-2")
								&& methods[i].getMethodID().equalsIgnoreCase(
										order.getCcToken())) {
							order.getCreditCard().setNumber(
									methods[i].getCreditCardInfo()
											.getCreditCardNumber());
							// order.getCreditCard().setNumber(order.getCreditCard().getNumber());
							order.getCreditCard().setExpireMonth("**");
							order.getCreditCard().setExpireYear("**");
							order.getCreditCard().setCardCode("***");
						}
					}
				}
			}
			customer.setGatewayPaymentMethods(paymentMethods);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public BANKCARD_GET_CUSTOMERResponseBANKCARD_GET_CUSTOMERResult bankCardGetCustomer(
			Customer customer) throws ServiceException {
		EBIZCHARGE_CUSTOMER_PROCESSINGLocator customerProcessing = new EBIZCHARGE_CUSTOMER_PROCESSINGLocator();
		EBIZCHARGE_CUSTOMER_PROCESSINGSoap customerProcessingSoap = customerProcessing
				.getEBIZCHARGE_CUSTOMER_PROCESSINGSoap();
		BANKCARD_GET_CUSTOMERResponseBANKCARD_GET_CUSTOMERResult response = new BANKCARD_GET_CUSTOMERResponseBANKCARD_GET_CUSTOMERResult();
		try {
			response = customerProcessingSoap.BANKCARD_GET_CUSTOMER(merchantId,
					hashInput(), clientIP, customer.getId().toString(),
					customer.getGatewayToken());

			Map<Integer, String> paymentMethods = new HashMap<Integer, String>();
			if (response.getError() == null
					&& response.getPaymentMethods() != null) {
				PaymentMethodInput[] methods = response.getPaymentMethods();
				if (methods.length > 0) {
					for (int i = 0; i < methods.length; i++) {
						paymentMethods.put(
								Integer.parseInt(methods[i].getMethodID()),
								methods[i].getMethodName());

					}
				}
			}
			customer.setGatewayPaymentMethods(paymentMethods);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return response;
	}

	public void bankCardCustomerPaymentMethod(Customer customer, Order order)
			throws ServiceException {
		EBIZCHARGE_CUSTOMER_PROCESSINGSoap customerProcessingSoap = customerProcessing
				.getEBIZCHARGE_CUSTOMER_PROCESSINGSoap();
		CreditCardInput cc = creditCardInput(order);
		try {
			order.getCreditCard().setPaymentNote(
					order.getCreditCard().getType()
							+ " ending with "
							+ order.getCreditCard()
									.getNumber()
									.substring(
											order.getCreditCard().getNumber()
													.length() - 4));
			BANKCARD_ADD_CUSTOMER_PAYMENT_METHODResponseBANKCARD_ADD_CUSTOMER_PAYMENT_METHODResult paymentResult = customerProcessingSoap
					.BANKCARD_ADD_CUSTOMER_PAYMENT_METHOD(merchantId,
							hashInput(), clientIP, customer.getGatewayToken(),
							cc, null, order.getCreditCard().getPaymentNote());
			if (paymentResult.getError().isEmpty()) {
				order.setCcToken(paymentResult.getMethodID());
				String[] sArray = order.getCreditCard().getNumber().split("");
				StringBuffer finalString = new StringBuffer();
				for (int i = 1; i < sArray.length; i++) {
					if (i <= sArray.length - 5) {
						sArray[i] = "*";
					}
					finalString.append(sArray[i]);
				}
				order.getCreditCard().setNumber(finalString.toString());
				order.getCreditCard().setExpireMonth("**");
				order.getCreditCard().setExpireYear("**");
				order.getCreditCard().setCardCode("***");
				order.getCreditCard().setType("****");
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public void bankCardDelCustomerPaymentMethod(Customer customer,
			Order order, boolean getPayments) throws ServiceException {
		EBIZCHARGE_CUSTOMER_PROCESSINGSoap customerProcessingSoap = customerProcessing
				.getEBIZCHARGE_CUSTOMER_PROCESSINGSoap();
		try {
			customerProcessingSoap.BANKCARD_DEL_CUSTOMER_PAYMENT_METHOD(
					merchantId, hashInput(), clientIP,
					customer.getGatewayToken(), order.getCcToken());

			if (getPayments) {
				bankCardGetCustomer(customer, order, false);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public boolean bankCardDelCustomerPaymentMethod(Customer customer,
			boolean getPayments, String methodId) throws ServiceException {
		boolean success = false;
		EBIZCHARGE_CUSTOMER_PROCESSINGSoap customerProcessingSoap = customerProcessing
				.getEBIZCHARGE_CUSTOMER_PROCESSINGSoap();
		try {
			customerProcessingSoap.BANKCARD_DEL_CUSTOMER_PAYMENT_METHOD(
					merchantId, hashInput(), clientIP,
					customer.getGatewayToken(), methodId);
			success = true;
			if (getPayments) {
				bankCardGetCustomer(customer);
			}
		} catch (Exception e) {
			success = false;
			e.printStackTrace();
		}
		return success;
	}

	public BankCardResponse bankCardSale(Order order) throws ServiceException {
		EBIZCHARGE_TRANSACTION_PROCESSINGSoap processingSoap = processingLocator
				.getEBIZCHARGE_TRANSACTION_PROCESSINGSoap();

		ItemDetailInput[] itemDetail = getItemDetailInput(order);
		BankCardResponse response = new BankCardResponse();
		try {
			response = processingSoap.BANKCARD_SALE(merchantId, hashInput(),
					clientIP, chargeDetailInput(order),
					custDetailInput(order.getBilling()),
					custDetailInput(order.getShipping()), itemDetail);
		} catch (RemoteException e) {
			e.printStackTrace();
		}
		return response;
	}

	public BankCardResponse bankCardAuthOnly(Order order)
			throws ServiceException {
		EBIZCHARGE_TRANSACTION_PROCESSINGLocator processingLocator = new EBIZCHARGE_TRANSACTION_PROCESSINGLocator();
		EBIZCHARGE_TRANSACTION_PROCESSINGSoap processingSoap = processingLocator
				.getEBIZCHARGE_TRANSACTION_PROCESSINGSoap();

		BankCardResponse response = new BankCardResponse();
		try {
			response = processingSoap.BANKCARD_AUTHONLY(merchantId,
					hashInput(), clientIP, chargeDetailInput(order),
					custDetailInput(order.getBilling()),
					custDetailInput(order.getShipping()));
		} catch (Exception e) {
			e.printStackTrace();
		}
		return response;
	}

	public BankCardResponse bankCardPriorAuthOnly(Order order)
			throws ServiceException {
		EBIZCHARGE_TRANSACTION_PROCESSINGLocator processingLocator = new EBIZCHARGE_TRANSACTION_PROCESSINGLocator();
		EBIZCHARGE_TRANSACTION_PROCESSINGSoap processingSoap = processingLocator
				.getEBIZCHARGE_TRANSACTION_PROCESSINGSoap();

		BankCardResponse response = new BankCardResponse();
		ItemDetailInput[] itemDetail = getItemDetailInput(order);

		try {
			response = processingSoap.BANKCARD_PRIOR_AUTH_SALE(merchantId,
					hashInput(), clientIP, order.getCreditCard().getTransId(),
					chargeDetailInput(order),
					custDetailInput(order.getBilling()),
					custDetailInput(order.getShipping()), itemDetail);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return response;
	}

	public ItemDetailInput[] getItemDetailInput(Order order) {

		ItemDetailInput[] itemDetail = new ItemDetailInput[order.getLineItems()
				.size()];

		// Need to work on casePack quantities and price tables...
		for (LineItem lineItem : order.getLineItems()) {
			ItemDetailInput itemDetailInput = new ItemDetailInput();
			itemDetailInput.setAmount(BigDecimal.valueOf(lineItem
					.getTotalPrice()));
			itemDetailInput
					.setCost(BigDecimal.valueOf(lineItem.getUnitPrice()));
			itemDetailInput
					.setDescription(lineItem.getProduct().getShortDesc());
			itemDetailInput.setQuantity(BigInteger.valueOf(lineItem
					.getQuantity()));
			itemDetailInput.setUnitOfMeasure("N/A");
			itemDetailInput.setUPC("N/A");
		}
		return itemDetail;
	}

	public HashInput hashInput() {
		HashInput hashInput = new HashInput();
		hashInput.setHashSeed(hashSeed);
		hashInput.setHashType(hashType);
		hashInput.setSourcekey(sourceKey);
		String preHashValue = hashInput.getSourcekey()
				+ hashInput.getHashSeed() + pin;

		byte[] bytesOfPreHashValue = preHashValue.getBytes();
		MessageDigest md = null;
		try {
			md = MessageDigest.getInstance("MD5");
		} catch (NoSuchAlgorithmException e1) {
			e1.printStackTrace();
		}
		byte byteData[] = md.digest(bytesOfPreHashValue);
		// convert the byte to hex format method

		StringBuffer hexString = new StringBuffer();
		for (int i = 0; i < byteData.length; i++) {
			String hex = Integer.toHexString(0xff & byteData[i]);
			if (hex.length() == 1)
				hexString.append('0');
			hexString.append(hex);
		}
		hashInput.setPinHash(hexString.toString());
		return hashInput;
	}

	public CustDetailInput custDetailInput(Address address) {
		CustDetailInput custDetailInput = new CustDetailInput();

		custDetailInput.setAddress1(address.getAddr1());
		custDetailInput.setCity(address.getCity());
		custDetailInput.setState(address.getStateProvince());
		custDetailInput.setZip(address.getZip());
		custDetailInput.setCustomerLastName(address.getLastName());
		custDetailInput.setCustomerName(address.getFirstName());
		custDetailInput.setCountry(address.getCountry());
		if (address.getEmail() == null) {
			custDetailInput.setEmailAddress("");
		} else {
			custDetailInput.setEmailAddress(address.getEmail());
		}
		custDetailInput.setPhoneNumber(address.getPhone());
		custDetailInput.setFaxNumber(address.getFax());
		return custDetailInput;
	}

	public CreditCardInput creditCardInput(Order order) {
		CreditCardInput creditCardInput = new CreditCardInput();
		creditCardInput.setCardCode(order.getCreditCard().getCardCode());
		creditCardInput.setCreditCardNumber(order.getCreditCard().getNumber());
		// Not Sure with AVSStreetDigits & AVSZip values
		creditCardInput.setAVSZip(order.getCreditCard().getAvsCode());
		creditCardInput.setCardExpiration(order.getCreditCard()
				.getExpireMonth() + order.getCreditCard().getExpireYear());
		return creditCardInput;
	}

	public ChargeDetailInput chargeDetailInput(Order order) {
		ChargeDetailInput input = new ChargeDetailInput();

		input.setAmount(BigDecimal.valueOf(order.getGrandTotal()));
		input.setCreditCardNumber(order.getCreditCard().getNumber());
		input.setCardExpiration(order.getCreditCard().getExpireMonth()
				+ order.getCreditCard().getExpireYear());
		input.setCardCode(order.getCreditCard().getCvv2Code());
		input.setInvoiceNumber(order.getOrderId() + "");
		input.setOrderNumber(order.getOrderId() + "");
		input.setPONumber(order.getOrderId() + "");
		input.setAVSZip(order.getBilling().getZip());
		return input;
	}

	public PaymentMethodInput paymentMethodInput(Order order) {
		PaymentMethodInput paymentMethodInput = new PaymentMethodInput();
		paymentMethodInput.setCreditCardInfo(creditCardInput(order));
		return paymentMethodInput;
	}

	public CustomerInput customerInfo(Order order) {
		CustomerInput customerInfo = new CustomerInput();
		customerInfo.setBillingDetailInfo(custDetailInput(order.getBilling()));
		customerInfo.setCustomerDescription(order.getBilling().getFirstName()
				+ " " + order.getBilling().getLastName());
		customerInfo.setCustomerID(order.getUserId() + "");
		customerInfo.setPaymentMethod(paymentMethodInput(order));
		return customerInfo;
	}

	public CustomerChargeInput customerChargeInput(Order order,
			Customer customer) {
		CustomerChargeInput customerChargeInput = new CustomerChargeInput();

		customerChargeInput.setPaymentMethodID(order.getCcToken());
		customerChargeInput.setCustDBNum(customer.getGatewayToken());
		customerChargeInput.setCardCode(order.getCreditCard().getCardCode());
		customerChargeInput
				.setAmount(BigDecimal.valueOf(order.getGrandTotal()));
		customerChargeInput.setTaxAmount(BigDecimal.valueOf(order.getTax()));
		customerChargeInput.setOrderNumber(order.getOrderId() + "");
		customerChargeInput.setDescription(order.getBilling().getFirstName()
				+ " " + order.getBilling().getLastName());

		return customerChargeInput;
	}

	public void getCustomerPaymentMethods() {

	}
}