/* Copyright 2010 Advanced E-Media Solutions
 * @author Eduardo Asprec
 * @since 10.21.2010
 */

package com.webjaguar.thirdparty.payment.geMoney;

public class GEMoney {
	
	private String acctNumber;
	private String status;
	private String transDate;
	private Integer clientTransID;
	private String ssn;
	private String authCode;

	public String getAcctNumber() {
		return acctNumber;
	}
	public void setAcctNumber(String acctNumber) {
		this.acctNumber = acctNumber;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public String getTransDate() {
		return transDate;
	}
	public void setTransDate(String transDate) {
		this.transDate = transDate;
	}
	public Integer getClientTransID() {
		return clientTransID;
	}
	public void setClientTransID(Integer clientTransID) {
		this.clientTransID = clientTransID;
	}
	public String getSsn() {
		return ssn;
	}
	public void setSsn(String ssn) {
		this.ssn = ssn;
	}
	public String getAuthCode() {
		return authCode;
	}
	public void setAuthCode(String authCode) {
		this.authCode = authCode;
	}

}
