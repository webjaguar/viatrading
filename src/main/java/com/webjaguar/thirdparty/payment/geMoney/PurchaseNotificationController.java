/* Copyright 2010 Advanced E-Media Solutions
 * @author Eduardo Asprec
 * @since 10.19.2010
 */


package com.webjaguar.thirdparty.payment.geMoney;

import java.io.*;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Enumeration;
import java.util.List;
import java.util.Map;

import javax.mail.internet.MimeMessage;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.mail.javamail.JavaMailSenderImpl;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.web.bind.ServletRequestUtils;
import org.springframework.web.context.support.WebApplicationObjectSupport;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.Controller;

import com.webjaguar.logic.WebJaguarFacade;
import com.webjaguar.model.Configuration;
import com.webjaguar.model.Customer;
import com.webjaguar.model.MultiStore;
import com.webjaguar.model.Order;
import com.webjaguar.model.OrderStatus;
import com.webjaguar.model.SiteMessage;

public class PurchaseNotificationController extends WebApplicationObjectSupport implements Controller {
	
	WebJaguarFacade webJaguar;
	public void setWebJaguar(WebJaguarFacade webJaguar){ this.webJaguar = webJaguar; }
	
	private JavaMailSenderImpl mailSender;
	public void setMailSender(JavaMailSenderImpl mailSender) { this.mailSender = mailSender; }

	public ModelAndView handleRequest(HttpServletRequest request, HttpServletResponse response) throws Exception {	
    	
		// site configuration
		Map<String, Configuration> siteConfig = (Map<String, Configuration>) request.getAttribute("siteConfig");
		
		// transaction
		GEMoney trans = new GEMoney();	
		trans.setClientTransID(ServletRequestUtils.getIntParameter(request, "ClientTransactionID", -1));
		trans.setStatus(ServletRequestUtils.getStringParameter(request, "Status", ""));
		trans.setAcctNumber(ServletRequestUtils.getStringParameter(request, "AccountNumber", ""));
		trans.setTransDate(ServletRequestUtils.getStringParameter(request, "TransactionDate", ""));
		trans.setAuthCode(ServletRequestUtils.getStringParameter(request, "AuthCode", ""));
			
		Order order = null;
		try {
			order = this.webJaguar.getOrder(trans.getClientTransID(), null);
		} catch (Exception e) {
			// do nothing
		}
		
		if (request.getMethod().equalsIgnoreCase("POST") && order != null 
						&& order.getPaymentMethod().equalsIgnoreCase("GE Money")) {
			
			if (trans.getStatus().equalsIgnoreCase("Auth Approved")) {
				OrderStatus orderStatus = new OrderStatus();
				orderStatus.setOrderId(order.getOrderId());
				orderStatus.setStatus("p"); // pending
				SiteMessage siteMessage = null;
				try {
					Integer messageId = null;
					// multi store
					List<MultiStore> multiStores = this.webJaguar.getMultiStore(order.getHost());
					MultiStore multiStore = null;
					if (multiStores.size() > 0) {
						multiStore = multiStores.get(0);
						messageId = multiStore.getMidNewOrders();
					} else {
	    				messageId = Integer.parseInt(siteConfig.get("SITE_MESSAGE_ID_FOR_NEW_ORDERS").getValue());		    					
					}
					siteMessage = this.webJaguar.getSiteMessageById(messageId);
					if (siteMessage != null) {
						// send email
						orderStatus.setSubject(siteMessage.getSubject());
						orderStatus.setMessage(siteMessage.getMessage());
						orderStatus.setHtmlMessage(siteMessage.isHtml());
						if (notifyCustomer(orderStatus, siteConfig, order, multiStore)) {
	    					orderStatus.setUserNotified(true);		    							
						}
					}
				} catch (NumberFormatException e) {
					// do nothing
				}	
				
				this.webJaguar.updateGEMoneyPayment(trans, orderStatus);
				
				// buy safe
				if (!siteConfig.get("BUY_SAFE_URL").getValue().equals("")) {
					try {
						this.webJaguar.setShoppingCartCheckout(order, mailSender);
					} catch (Exception e) { 
						e.printStackTrace();
						OrderStatus status = new OrderStatus();
						status.setOrderId(order.getOrderId());
						status.setStatus("p"); // pending
						if ((order.getWantsBond() != null) && (order.getWantsBond() == true)) {
							status.setComments("BuySafe Bond was selected but cancelled as system failed to connect Buysafe. Please check the amount charged and refund the bond cost to Customer.");
						} else {
							status.setComments("BuySafe Bond was not selected. System failed to connect Buysafe");
						}
						order.setWantsBond(false);
						order.setBondCost(null);
						order.setGrandTotal();
						
						this.webJaguar.cancelBuySafeBond(order, status);
					}
				}
				// clear shopping cart items from database
				this.webJaguar.deleteShoppingCart(order.getUserId(), order.getManufacturerName());	
			} else {
				this.webJaguar.updateGEMoneyPayment(trans, null);				
			}
			saveTransaction(request);
		}
		
		return null;
    }
    
    private void saveTransaction(HttpServletRequest request) throws Exception {
		File baseDir = new File(getServletContext().getRealPath("/geMoney"));	
		if (!baseDir.exists()) {
			baseDir.mkdir();
		}
    	FileOutputStream fo = new FileOutputStream(new File(baseDir, "purchaseNotication"), true);
    	PrintWriter pw = new PrintWriter(fo);
    	pw.write("IP Address=" + request.getRemoteAddr() + "\n");
		Enumeration en = request.getParameterNames();
		while(en.hasMoreElements()){
		    String paramName = (String)en.nextElement();
		    String paramValue = request.getParameter(paramName);
		    pw.write(paramName + "=" + paramValue + "\n");
		}
		pw.write("--------------------------------------------\n");
		pw.close();
    	fo.close();
    }
    
	private boolean notifyCustomer(OrderStatus orderStatus, Map<String, Configuration> siteConfig, Order order, MultiStore multiStore) {

		Customer customer = this.webJaguar.getCustomerById(order.getUserId());
		
		if (!customer.isEmailNotify()) {
			// do not send email
			return false;
		}
		
		// send email
    	String contactEmail = siteConfig.get("CONTACT_EMAIL").getValue();
    	String secureUrl = this.webJaguar.getSecureUrl(siteConfig, multiStore);
		SimpleDateFormat dateFormatter = new SimpleDateFormat("MMM d, yyyy HH:mm:ss a z");

    	StringBuffer messageBuffer = new StringBuffer();
    	messageBuffer.append(dateFormatter.format(new Date()) + "\n\n");
    	
    	// replace dynamic elements
    	try {
    		this.webJaguar.replaceDynamicElement(orderStatus, customer, null, order, secureUrl, null);
    	} catch (Exception e) {e.printStackTrace();}
    	messageBuffer.append(orderStatus.getMessage());
    	
    	try {
	    	// construct email message
	       	MimeMessage mms = mailSender.createMimeMessage();
			MimeMessageHelper helper = new MimeMessageHelper(mms, true, "UTF-8");
			helper.setTo(customer.getUsername());
			helper.setFrom(contactEmail);
			helper.setBcc(contactEmail);
			helper.setSubject(orderStatus.getSubject());
	    	helper.setText(messageBuffer.toString(), orderStatus.isHtmlMessage());
			
			mailSender.send(mms);
		} catch (Exception ex) {
			// do nothing, customer not notified
			return false;
		} 		
		return true;
	}
}
