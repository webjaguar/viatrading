/* Copyright 2010 Advanced E-Media Solutions
 * @author Eduardo Asprec
 * @since 10.20.2010
 */


package com.webjaguar.thirdparty.payment.geMoney;

import java.io.*;
import java.util.Enumeration;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.web.context.support.WebApplicationObjectSupport;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.Controller;

import com.webjaguar.logic.WebJaguarFacade;

public class CreditApplyNotificationController extends WebApplicationObjectSupport implements Controller {
	
	WebJaguarFacade webJaguar;
	public void setWebJaguar(WebJaguarFacade webJaguar){ this.webJaguar = webJaguar; }
	
	public ModelAndView handleRequest(HttpServletRequest request, HttpServletResponse response) throws Exception {	
    	
		if (request.getMethod().equalsIgnoreCase("POST")) {
			
			saveTransaction(request);
		}
		
		return null;
    }
    
    private void saveTransaction(HttpServletRequest request) throws Exception {
		File baseDir = new File(getServletContext().getRealPath("/geMoney"));	
		if (!baseDir.exists()) {
			baseDir.mkdir();
		}
    	FileOutputStream fo = new FileOutputStream(new File(baseDir, "creditApplyNotification"), true);
    	PrintWriter pw = new PrintWriter(fo);
    	pw.write("IP Address=" + request.getRemoteAddr() + "\n");
		Enumeration en = request.getParameterNames();
		while(en.hasMoreElements()){
		    String paramName = (String)en.nextElement();
		    String paramValue = request.getParameter(paramName);
		    pw.write(paramName + "=" + paramValue + "\n");
		}
		pw.write("--------------------------------------------\n");
		pw.close();
    	fo.close();
    }
    
}
