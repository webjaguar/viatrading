/* Copyright 2010 Advanced E-Media Solutions
 * @author Eduardo Asprec
 * @since 10.22.2010
 */

package com.webjaguar.thirdparty.payment.geMoney;

import org.springframework.validation.Errors;

import com.webjaguar.model.Order;

public class GEMoneyValidator {
	public static void validateGEMoneyOrder(Order order, Errors errors) {

		// account number
		if (order.getGeMoney().getAcctNumber() != null) {
			order.getGeMoney().setAcctNumber(order.getGeMoney().getAcctNumber().trim());
		} else {
			order.getGeMoney().setAcctNumber("");
		}
		
		// ss number
		if (order.getGeMoney().getSsn() != null) {
			order.getGeMoney().setSsn(order.getGeMoney().getSsn().trim());
		} else {
			order.getGeMoney().setSsn("");
		}
		
		if (order.getGeMoney().getAcctNumber().length() == 0 && order.getGeMoney().getSsn().length() == 0) {				
			errors.rejectValue("order.geMoney.acctNumber", "----", "Either the Account Number or SSN must be entered.");
		} else {
			// check account number
			if (order.getGeMoney().getAcctNumber().length() > 0) {
				if (!digitsOnly(order.getGeMoney().getAcctNumber())) {
					errors.rejectValue("order.geMoney.acctNumber", "order.exception.gemoney.acctnumber");
				} else if (!(order.getGeMoney().getAcctNumber().length() == 16 || order.getGeMoney().getAcctNumber().length() == 13)) {
					errors.rejectValue("order.geMoney.acctNumber", "order.exception.gemoney.acctnumber");
				}
			}
			
			// check ssn
			if (order.getGeMoney().getSsn().length() > 0) {
				if (!digitsOnly(order.getGeMoney().getSsn())) {
					errors.rejectValue("order.geMoney.ssn", "order.exception.gemoney.ssn");
				} else if (order.getGeMoney().getSsn().length() != 9) {
					errors.rejectValue("order.geMoney.ssn", "order.exception.gemoney.ssn");					
				}
			}
			
		}
		
		// check phone
		if (order.getBilling().getPhone() == null || order.getBilling().getPhone().trim().startsWith("1")
				|| order.getBilling().getPhone().trim().length() != 10) {
			errors.rejectValue("order.billing.phone", "----", "");
		} else if (order.getBilling().getPhone() != null && !digitsOnly(order.getBilling().getPhone().trim())) {
			errors.rejectValue("order.billing.phone", "----", "");
		}
		
		// check zipcode
		if (order.getBilling().getZip() == null || order.getBilling().getZip().trim().length() != 5) {
			errors.rejectValue("order.billing.zip", "----", "");
		} else if (order.getBilling().getZip() != null && !digitsOnly(order.getBilling().getZip().trim())) {
			errors.rejectValue("order.billing.zip", "----", "");
		}
	}
	
	private static boolean digitsOnly(String value) {
		String digits = "0123456789";
		
		StringBuffer sbuff = new StringBuffer(value);
		for (int i=sbuff.length()-1; i>=0; i--) {
			if (digits.indexOf(sbuff.charAt(i)) < 0) {
				return false;
			}
		}
		
		return true;
	}
}
