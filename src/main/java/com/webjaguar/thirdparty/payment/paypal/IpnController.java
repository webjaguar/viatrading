/* 
 * Copyright 2006 Advanced E-Media Solutions
 */

package com.webjaguar.thirdparty.payment.paypal;

import java.io.*;
import java.net.URL;
import java.net.URLConnection;
import java.net.URLEncoder;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Enumeration;
import java.util.List;
import java.util.Map;

import javax.mail.internet.MimeMessage;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.mail.javamail.JavaMailSenderImpl;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.validation.BindException;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.AbstractCommandController;

import com.webjaguar.dao.OrderDao;
import com.webjaguar.logic.WebJaguarFacade;
import com.webjaguar.model.Configuration;
import com.webjaguar.model.Customer;
import com.webjaguar.model.MultiStore;
import com.webjaguar.model.Order;
import com.webjaguar.model.OrderStatus;
import com.webjaguar.model.Paypal;
import com.webjaguar.model.SiteMessage;

public class IpnController extends AbstractCommandController {
	
	WebJaguarFacade webJaguar;
	public void setWebJaguar(WebJaguarFacade webJaguar){ this.webJaguar = webJaguar; }

	private OrderDao orderDao;
	public void setOrderDao(OrderDao orderDao) { this.orderDao = orderDao; }
	
	private JavaMailSenderImpl mailSender;
	public void setMailSender(JavaMailSenderImpl mailSender) { this.mailSender = mailSender; }
    
	public IpnController() {
		setCommandClass(Paypal.class);
	}  

    public ModelAndView handle(HttpServletRequest request, HttpServletResponse response,
			Object command, BindException errors)
        throws Exception {
    	
    	System.out.println(1);
		// site configuration
		Map<String, Configuration> siteConfig = webJaguar.getSiteConfig();
  
        // post back method
		// read post from PayPal system and add 'cmd'
		Enumeration en = request.getParameterNames();
		String str = "cmd=_notify-validate";
		while(en.hasMoreElements()){
		    String paramName = (String)en.nextElement();
		    String paramValue = request.getParameter(paramName);
		    str = str + "&" + paramName + "=" + URLEncoder.encode(paramValue, "UTF-8");
		}
		
		System.out.println(2);
		// post back to PayPal system to validate
		URL u = new URL(siteConfig.get("PAYPAL_URL").getValue() + "cgi-bin/webscr");
		URLConnection uc = u.openConnection();
		uc.setDoOutput(true);
		uc.setRequestProperty("Content-Type","application/x-www-form-urlencoded");
		PrintWriter pw = new PrintWriter(uc.getOutputStream());
		pw.println(str);
		pw.close();
		System.out.println(3);
		
		BufferedReader in = new BufferedReader(
		new InputStreamReader(uc.getInputStream()));
		String res = in.readLine();
		in.close();
		System.out.println(4 +" Response "+res);
		
		// check notification validation
		if(res.equals("VERIFIED")) {
            // check that paymentStatus=Completed
            // check that txnId has not been previously processed
            // check that receiverEmail is your Primary PayPal email
            // check that paymentAmount/paymentCurrency are correct
            // process payment
			Paypal paypal = (Paypal) command;
			try {
				Order order = orderDao.getOrder( Integer.parseInt( paypal.getInvoice() ), null );
				if (order.getPaymentMethod().equalsIgnoreCase( "paypal")) {
		    		OrderStatus orderStatus = null;
		    		if (order.getStatus().equalsIgnoreCase( "xp" )) { // paypal canceled
		    			// update status
		    			orderStatus = new OrderStatus();
		    			orderStatus.setOrderId( order.getOrderId() );
		    			orderStatus.setStatus( "p" ); // pending
		    			SiteMessage siteMessage = null;
		    			try {
		    				Integer messageId = null;
		    				// multi store
		    				List<MultiStore> multiStores = this.webJaguar.getMultiStore(order.getHost());
		    				MultiStore multiStore = null;
		    				if (multiStores.size() > 0) {
		    					multiStore = multiStores.get(0);
		    					messageId = multiStore.getMidNewOrders();
		    				} else {
			    				messageId = Integer.parseInt( siteConfig.get( "SITE_MESSAGE_ID_FOR_NEW_ORDERS" ).getValue() );		    					
		    				}
		    				siteMessage = this.webJaguar.getSiteMessageById( messageId );
		    				if (siteMessage != null) {
		    					// send email
		    					orderStatus.setSubject( siteMessage.getSubject() );
		    					orderStatus.setMessage( siteMessage.getMessage() );
		    					orderStatus.setHtmlMessage(siteMessage.isHtml());
		    					if (notifyCustomer(orderStatus, siteConfig, order, multiStore)) {
			    					orderStatus.setUserNotified( true );		    							
		    					}
		    				}
		    			} catch (NumberFormatException e) {
		    				// do nothing
		    				e.printStackTrace();
		    			}		    		
		    		}
					orderDao.updatePaypal( paypal, orderStatus );
				}
				// buy safe
				if( !siteConfig.get("BUY_SAFE_URL").getValue().equals("") ) {
					try {
						this.webJaguar.setShoppingCartCheckout( order, mailSender );
					} catch (Exception ex) { 
						ex.printStackTrace();
						OrderStatus status = new OrderStatus();
						status.setOrderId( order.getOrderId() );
						status.setStatus( "p" ); // pending
						if( (order.getWantsBond() != null) && (order.getWantsBond() == true) ) {
							status.setComments("BuySafe Bond was selected but cancelled as system failed to connect Buysafe. Please check the amount charged and refund the bond cost to Customer.");
						} else {
							status.setComments("BuySafe Bond was not selected. System failed to connect Buysafe");
						}
						order.setWantsBond(false);
						order.setBondCost(null);
						order.setGrandTotal();
						
						this.webJaguar.cancelBuySafeBond(order, status);
					}
				}
			} catch (Exception e) {
				// do nothing
				e.printStackTrace();
			}
			savePaypalTransaction(request);
		}
		else if(res.equals("INVALID")) {
            // log for investigation
			System.out.println("Invalid Response "+res);
			
		}
		else {
            // error
		     System.out.println("Error "+res);
		}

		return null;
    }
    
    private void savePaypalTransaction(HttpServletRequest request) throws Exception {
    	File baseFile = new File(getServletContext().getRealPath("/"));
    	File ipnFile = new File(baseFile, "paypal_ipn");
    	FileOutputStream fo = new FileOutputStream(ipnFile, true);
    	PrintWriter pw = new PrintWriter(fo);
		pw.write( "IP Address=" + request.getRemoteAddr() + "\n");
		Enumeration en = request.getParameterNames();
		while(en.hasMoreElements()){
		    String paramName = (String)en.nextElement();
		    String paramValue = request.getParameter(paramName);
		    pw.write( paramName + "=" + paramValue + "\n");
		}
		pw.write("--------------------------------------------\n");
		pw.close();
    	fo.close();
    }
	
	private boolean notifyCustomer(OrderStatus orderStatus, Map<String, Configuration> siteConfig, Order order, MultiStore multiStore) {

		Customer customer = this.webJaguar.getCustomerById(order.getUserId());
		
		if (!customer.isEmailNotify()) {
			// do not send email
			return false;
		}
		
		// send email
    	String contactEmail = siteConfig.get("CONTACT_EMAIL").getValue();
    	String secureUrl = this.webJaguar.getSecureUrl(siteConfig, multiStore);
		SimpleDateFormat dateFormatter = new SimpleDateFormat("MMM d, yyyy HH:mm:ss a z");

    	StringBuffer messageBuffer = new StringBuffer();
    	messageBuffer.append( dateFormatter.format(new Date()) + "\n\n" );
    	
    	// replace dynamic elements
    	try {
    		this.webJaguar.replaceDynamicElement( orderStatus, customer, null, order, secureUrl, null);
    	} catch (Exception e) {e.printStackTrace();}
    	messageBuffer.append( orderStatus.getMessage() );
    	
    	try {
	    	// construct email message
	       	MimeMessage mms = mailSender.createMimeMessage();
			MimeMessageHelper helper = new MimeMessageHelper(mms, true, "UTF-8");
			helper.setTo(customer.getUsername());
			helper.setFrom(contactEmail);
			helper.setBcc(contactEmail);
			helper.setSubject(orderStatus.getSubject());
	    	helper.setText(messageBuffer.toString(), orderStatus.isHtmlMessage());
			
			mailSender.send(mms);
		} catch (Exception ex) {
			// do nothing, customer not notified
			return false;
		} 		
		return true;
	}

	
}
