/* 
 * Copyright 2006 Advanced E-Media Solutions
 */

package com.webjaguar.thirdparty.payment.paypal;

import java.io.*;
import java.net.URL;
import java.net.URLConnection;
import java.net.URLEncoder;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Enumeration;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import javax.mail.internet.MimeMessage;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.context.ApplicationContext;
import org.springframework.mail.MailException;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSenderImpl;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.validation.BindException;
import org.springframework.web.bind.ServletRequestUtils;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.AbstractCommandController;
import org.springframework.web.servlet.support.RequestContextUtils;

import com.webjaguar.dao.OrderDao;
import com.webjaguar.logic.WebJaguarFacade;
import com.webjaguar.model.Configuration;
import com.webjaguar.model.Customer;
import com.webjaguar.model.GiftCard;
import com.webjaguar.model.GiftCardOrder;
import com.webjaguar.model.GiftCardSearch;
import com.webjaguar.model.GiftCardStatus;
import com.webjaguar.model.MultiStore;
import com.webjaguar.model.Order;
import com.webjaguar.model.OrderStatus;
import com.webjaguar.model.Paypal;
import com.webjaguar.model.SiteMessage;
import com.webjaguar.model.UserSession;

public class GiftCardIpnController extends AbstractCommandController {
	
	WebJaguarFacade webJaguar;
	public void setWebJaguar(WebJaguarFacade webJaguar){ this.webJaguar = webJaguar; }

	private OrderDao orderDao;
	public void setOrderDao(OrderDao orderDao) { this.orderDao = orderDao; }
	
	private JavaMailSenderImpl mailSender;
	public void setMailSender(JavaMailSenderImpl mailSender) { this.mailSender = mailSender; }
    
	public GiftCardIpnController() {
		setCommandClass(Paypal.class);
	}  

    public ModelAndView handle(HttpServletRequest request, HttpServletResponse response, Object command, BindException errors) throws Exception {
    	System.out.println(1);
		// site configuration
		Map<String, Configuration> siteConfig = webJaguar.getSiteConfig();
		
		List<MultiStore> multiStores = this.webJaguar.getMultiStore(request.getHeader("host"));
		MultiStore multiStore = null;
		if (multiStores.size() > 0) {
			multiStore = multiStores.get(0);
			//set giftCard host	
		}
  
        // post back method
		// read post from PayPal system and add 'cmd'
		Enumeration en = request.getParameterNames();
		String str = "cmd=_notify-validate";
		while(en.hasMoreElements()){
		    String paramName = (String)en.nextElement();
		    String paramValue = request.getParameter(paramName);
		    str = str + "&" + paramName + "=" + URLEncoder.encode(paramValue, "UTF-8");
		}
		
		System.out.println(2);
		// post back to PayPal system to validate
		URL u = new URL(siteConfig.get("PAYPAL_URL").getValue() + "cgi-bin/webscr");
		URLConnection uc = u.openConnection();
		uc.setDoOutput(true);
		uc.setRequestProperty("Content-Type","application/x-www-form-urlencoded");
		PrintWriter pw = new PrintWriter(uc.getOutputStream());
		pw.println(str);
		pw.close(); 
		System.out.println(3);
		
		BufferedReader in = new BufferedReader(new InputStreamReader(uc.getInputStream()));
		String res = in.readLine();
		in.close();
		System.out.println(4 +" Response "+res);
		
		// check notification validation
		if(res.equals("VERIFIED")) {
            // check that paymentStatus=Completed
            // check that txnId has not been previously processed
            // check that receiverEmail is your Primary PayPal email
            // check that paymentAmount/paymentCurrency are correct
            // process payment
			Paypal paypal = (Paypal) command;
			System.out.println("Invoice Number  " +paypal.getInvoice());
			try {
				GiftCardOrder giftCardOrder = this.webJaguar.getGiftCardOrderByGiftCardOrderId(paypal.getInvoice());
				if(giftCardOrder.getPaymentMethod().equalsIgnoreCase("paypal")) {
					GiftCardStatus giftCardStatus = null;
					if(giftCardOrder.getStatus().equalsIgnoreCase("xp")) {
						//update Status
						giftCardStatus = new GiftCardStatus();
						

						//Generate gift card code
						giftCardOrder.getCreditCard().setTransId(paypal.getTxn_id());
						giftCardOrder.getCreditCard().setPaymentStatus(paypal.getPayment_status());	
						giftCardOrder.setStatus("p");
						
						giftCardStatus.setGiftCardOrderId(giftCardOrder.getGiftCardOrderId());
						giftCardStatus.setStatus("p"); //pending
						
						this.webJaguar.updateGiftCardOrderByPayPal(giftCardOrder, giftCardStatus);
						
						GiftCard giftCard = this.webJaguar.getGiftCardById(giftCardOrder.getGiftCardOrderId());
						
						// set Card to active.
						giftCard.setActive(true);
						this.webJaguar.updateGiftCard(giftCard);
						
						//Send Email
						int i = giftCardOrder.getQuantity();
						do {
							try{
								MimeMessage mms = mailSender.createMimeMessage();
								MimeMessageHelper helper = new MimeMessageHelper(mms, true, "UTF-8");
								SiteMessage siteMessage = null;
								try {
									Integer messageId = null;
									messageId = Integer.parseInt( siteConfig.get( "SITE_MESSAGE_ID_FOR_GIFT_CARD" ).getValue() );							
									siteMessage = this.webJaguar.getSiteMessageById( messageId );
									
									if (siteMessage != null) {
										notifyCustomer(helper, giftCard, request, multiStore, getApplicationContext(), siteMessage, siteConfig);
										notifyAdmin(giftCard,siteConfig);
										mailSender.send(mms);
									}
								} catch (Exception e) {
									e.printStackTrace();
								}
								i=i-1;
								
							} catch (Exception e) {
								// duplicate code???
								e.printStackTrace();
								i=i+1;
							}
						} while ( i > 0 );
					}					
				}
			} catch (Exception e) {
				// do nothing
				System.out.println("From " +siteConfig.get("SITE_URL").getValue() + " PayPalError ");
				e.printStackTrace();
			}
			savePaypalTransaction(request);
		}
		else if(res.equals("INVALID")) {
            // log for investigation
			System.out.println("Invalid Response "+res); 			
		}
		else {
            // error
		     System.out.println("Error "+res);
		}
		return null;
    }
    
    private void savePaypalTransaction(HttpServletRequest request) throws Exception {
    	File baseFile = new File(getServletContext().getRealPath("/"));
    	File ipnFile = new File(baseFile, "paypal_ipn");
    	FileOutputStream fo = new FileOutputStream(ipnFile, true);
    	PrintWriter pw = new PrintWriter(fo);
		pw.write( "IP Address=" + request.getRemoteAddr() + "\n");
		Enumeration en = request.getParameterNames();
		while(en.hasMoreElements()){
		    String paramName = (String)en.nextElement();
		    String paramValue = request.getParameter(paramName);
		    pw.write( paramName + "=" + paramValue + "\n");
		}
		pw.write("--------------------------------------------\n");
		pw.close();
    	fo.close();
    }
    
    private void notifyAdmin(GiftCard giftCard, Map<String, Configuration> siteConfig) {
		// send email notification
    	String contactEmail = siteConfig.get( "CONTACT_EMAIL" ).getValue();
    	SimpleDateFormat dateFormatter = new SimpleDateFormat("MMM d, yyyy HH:mm:ss a z");
    	
		SimpleMailMessage msg = new SimpleMailMessage();
		msg.setTo(contactEmail);
		msg.setFrom(contactEmail);
		msg.setSubject("Gift Card Order Number: "+giftCard.getGiftCardOrderId());
		msg.setText(dateFormatter.format(new Date()) + "\n\n Gift Card sent to "+giftCard.getRecipientEmail() );
		try {
			mailSender.send(msg);
		} catch (MailException ex) {
			// do nothing
		}
	}
	
	private void notifyCustomer(MimeMessageHelper helper, GiftCard giftCard, HttpServletRequest request, MultiStore multiStore, ApplicationContext context, SiteMessage siteMessage, Map<String, Configuration> siteConfig ) throws Exception 
	{
		String senderMessage = giftCard.getMessage();
		Customer customer = this.webJaguar.getCustomerById(giftCard.getUserId());

		
		// send email
		String contactEmail = siteConfig.get( "CONTACT_EMAIL" ).getValue();
		String secureUrl = this.webJaguar.getSecureUrl(siteConfig, multiStore);
		/*
		 * String secureUrl = siteConfig.get( "SECURE_URL" ).getValue();
		if (multiStore != null) {
			contactEmail = multiStore.getContactEmail();
			secureUrl = "http://" + multiStore.getHost() + request.getContextPath() + "/";
		}
		 */
		SimpleDateFormat dateFormatter = new SimpleDateFormat("MMM d, yyyy HH:mm:ss a z", RequestContextUtils.getLocale(request));

    	StringBuffer message = new StringBuffer();
    	
    	// message
    	siteMessage.setMessage( siteMessage.getMessage().replace( "#giftCardSender#", giftCard.getSenderFirstName()+ " " +giftCard.getSenderLastName() ) );
    	siteMessage.setMessage( siteMessage.getMessage().replace( "#giftCard#", generateHtml(giftCard, secureUrl, context, request, siteConfig ) ) );
    	siteMessage.setMessage( siteMessage.getMessage().replace( "#giftCardRecipient#", giftCard.getRecipientFirstName()+ " " +giftCard.getRecipientLastName() ) );
    	siteMessage.setMessage( siteMessage.getMessage().replace( "#giftCardMessage#", giftCard.getMessage()) );
    	
    	message.append( siteMessage.getMessage() );
    	
    	helper.setTo( giftCard.getRecipientEmail() );
    	helper.setFrom(customer.getUsername());
    	helper.setSubject( giftCard.getSenderFirstName() + " " + giftCard.getSenderLastName() + " " + context.getMessage( "sentYouAGiftCardFrom", new Object[0], RequestContextUtils.getLocale(request)) + " " +  secureUrl );
    	helper.setText(message.toString(), true);
	}
	
	private String generateHtml(GiftCard giftCard, String secureUrl, ApplicationContext context, HttpServletRequest request, Map<String, Configuration> siteConfig) {
		 
		StringBuffer html = new StringBuffer();
		//html.append( this.webJaguar.getSystemLayout("giftcard", request.getHeader("host")).getHeaderHtml() );
		html.append( "<img src=\"" + secureUrl + "assets/Image/Layout/companylogo.gif\"  border=\"0\" align=\"center\"/>" );
		html.append( "<table align=\"center\" width=\"430\" height=\"280\" border=\"0\" background=\"" + secureUrl + "assets/Image/Layout/gift_card_email.gif\" cellspacing=\"0\" cellpadding=\"0\">" );
		html.append( "<tr> <td> &nbsp; </td> </tr><tr> <td> &nbsp; </td> </tr><tr> <td> &nbsp; </td> </tr><tr> <td> &nbsp; </td> </tr>" );

		html.append( " <tr> " );
		html.append( "<td>" );
		html.append( "<p style=\"margin-left:210px; font-weight:700; font-height:15px; color:#333333; font-size:12px\">" + context.getMessage( "from", new Object[0], RequestContextUtils.getLocale(request))  + ": " + giftCard.getSenderFirstName() + " " + giftCard.getSenderLastName() + "</p>" );
		html.append( " <p style=\"margin-left:210px; font-weight:700; font-height:15px; color:#333333; font-size:12px\">" + context.getMessage( "to", new Object[0], RequestContextUtils.getLocale(request))  + ": " +  giftCard.getRecipientFirstName() + " " + giftCard.getRecipientLastName() + "</p>" );
		html.append( " </td>" );
		html.append( "</tr>" );
		html.append( "<tr>" );

		html.append( "<td>" );
		html.append( "<div style=\"padding:15px 40px 0px 165px; font-weight:700; line-height:25px; color:#333333; font-size:52px\">" + context.getMessage(siteConfig.get("CURRENCY").getValue(), new Object[0], RequestContextUtils.getLocale(request))  + new DecimalFormat("#,###.00").format( giftCard.getAmount().doubleValue() ) + "</div>" );
		html.append( "</td> " );
		html.append( "</tr>" );
		html.append( "<tr>" );
		html.append( " <td>" );
		html.append( "<div style=\"margin-left:100px; padding-top:10px; font-weight:700; font-height:18px; color:#983434; font-size:24px\">" + giftCard.getCode() + "</div>" );
		html.append( " </td>  " );
		html.append( "</tr>" );

		html.append( " <tr> <td> &nbsp; </td> </tr>" );
		html.append( "</table>" );
		
		html.append( "<table width=\"800\" align=\"center\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\">" );
		html.append( "<tr> " );
		html.append( "<td>" );
		html.append( "<div align=\"center\" style=\"font-weight:700;color:#ff2332; font-size:11px \"><a href=\"" + secureUrl + "giftcardBalance.jhtm\">" + context.getMessage( "redeemYourGiftCard", new Object[0], RequestContextUtils.getLocale(request))  + " " + "</a></div>");
		html.append( "</td> " );
		html.append( "</tr>" );
		html.append( "</table>" );
		//html.append( this.webJaguar.getSystemLayout("giftcard", request.getHeader("host")).getFooterHtml() );
		
		return html.toString();
	}

	
}
