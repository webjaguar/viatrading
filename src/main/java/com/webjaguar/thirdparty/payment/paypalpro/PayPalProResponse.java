package com.webjaguar.thirdparty.payment.paypalpro;

public class PayPalProResponse {
	
	private String ack;
	private String timeStamp;
	private String correlationId;
	private String transactionId;	
	private String lErrorCode;
	private String lShortMessage;
	private String lLongMessage;
	private String severityCode;
	private boolean success;
	private String orderTime;
	private String amt;
	private String taxAmt;
	private String feeAmt;
	private String paymentStatus;
	private String pendingReason;
	
	
	
	public String getAck() {
		return ack;
	}
	public void setAck(String ack) {
		this.ack = ack;
	}
	public String getTimeStamp() {
		return timeStamp;
	}
	public void setTimeStamp(String timeStamp) {
		this.timeStamp = timeStamp;
	}
	public String getCorrelationId() {
		return correlationId;
	}
	public void setCorrelationId(String correlationId) {
		this.correlationId = correlationId;
	}
	public String getTransactionId() {
		return transactionId;
	}
	public void setTransactionId(String transactionId) {
		this.transactionId = transactionId;
	}
	public String getLErrorCode() {
		return lErrorCode;
	}
	public void setLErrorCode(String lErrorCode) {
		this.lErrorCode = lErrorCode;
	}
	public String getLShortMessage() {
		return lShortMessage;
	}
	public void setLShortMessage(String lShortMessage) {
		this.lShortMessage = lShortMessage;
	}
	public String getLLongMessage() {
		return lLongMessage;
	}
	public void setLLongMessage(String lLongMessage) {
		this.lLongMessage = lLongMessage;
	}
	public String getSeverityCode() {
		return severityCode;
	}
	public void setSeverityCode(String severityCode) {
		this.severityCode = severityCode;
	}
	public Boolean getSuccess() {
		return success;
	}
	public void setSuccess(boolean success) {
		this.success = success;
	}
	public String getOrderTime() {
		return orderTime;
	}
	public void setOrderTime(String orderTime) {
		this.orderTime = orderTime;
	}
	public String getAmt() {
		return amt;
	}
	public void setAmt(String amt) {
		this.amt = amt;
	}
	public String getTaxAmt() {
		return taxAmt;
	}
	public void setTaxAmt(String taxAmt) {
		this.taxAmt = taxAmt;
	}
	public String getFeeAmt() {
		return feeAmt;
	}
	public void setFeeAmt(String feeAmt) {
		this.feeAmt = feeAmt;
	}
	public String getPaymentStatus() {
		return paymentStatus;
	}
	public void setPaymentStatus(String paymentStatus) {
		this.paymentStatus = paymentStatus;
	}
	public String getPendingReason() {
		return pendingReason;
	}
	public void setPendingReason(String pendingReason) {
		this.pendingReason = pendingReason;
	}	
}
