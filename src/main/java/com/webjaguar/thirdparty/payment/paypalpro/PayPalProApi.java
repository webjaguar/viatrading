package com.webjaguar.thirdparty.payment.paypalpro;

import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.paypal.sdk.core.nvp.NVPDecoder;
import com.paypal.sdk.core.nvp.NVPEncoder;
import com.paypal.sdk.exceptions.PayPalException;
import com.paypal.sdk.profiles.APIProfile;
import com.paypal.sdk.profiles.ProfileFactory;
import com.paypal.sdk.services.NVPCallerServices;
import com.webjaguar.model.Configuration;
import com.webjaguar.model.CreditCard;
import com.webjaguar.model.Order;
import com.webjaguar.model.Address;
import com.webjaguar.web.domain.Utilities;

public class PayPalProApi {	
	
	private NVPCallerServices caller = null;
	private NVPEncoder encoder = new NVPEncoder();
	private NVPDecoder decoder = new NVPDecoder();
	
	public APIProfile setProfile (Map<String, Configuration> siteConfig) {
		APIProfile profile = null;
		try {
			profile = ProfileFactory.createSignatureAPIProfile();
			
			// Set up your API credentials, PayPal end point, API operation and version.
			profile.setAPIUsername(siteConfig.get("CREDIT_ACCOUNT").getValue().trim());
	        profile.setAPIPassword(siteConfig.get("CREDIT_PASSWORD").getValue().trim());
	        profile.setSignature(siteConfig.get("CREDIT_MERCHANT_ID").getValue().trim());
	        profile.setEnvironment(siteConfig.get("PAYPALPRO_ENVIRONMENT").getValue().trim());
	        profile.setSubject("");//merchantEmailAddress
	        
		} catch (PayPalException e) {
			e.printStackTrace();
		}
        return profile;	
	}
	
	public Order setCreditCardTransactionResponse(Order order, PayPalProResponse response, Map<String, Configuration> siteConfig) throws Exception 
	{
		if (response.getAck() != null) {
			if (response.getAck().equalsIgnoreCase("Success")) {
				if (siteConfig.get("CREDIT_AUTO_CHARGE").getValue().equals("auth")){
					order.getCreditCard().setAuthCode(response.getTransactionId());
				} else {
					order.getCreditCard().setTransId(response.getTransactionId());
				}
				order.getCreditCard().setPaymentNote("Success. CorrelationID: " +response.getCorrelationId());	
			} else if (response.getAck().equalsIgnoreCase("SuccessWithWarning")) {
				if (siteConfig.get("CREDIT_AUTO_CHARGE").getValue().equals("auth")){
					order.getCreditCard().setAuthCode(response.getTransactionId());
				} else {
					order.getCreditCard().setTransId(response.getTransactionId());
				}
				order.getCreditCard().setPaymentNote("Error Code " + response.getLErrorCode() + "/ Severity Code " +response.getSeverityCode() + "-" +response.getLLongMessage() + ". CorrelationID: " +response.getCorrelationId() );
			} else if (response.getAck().equalsIgnoreCase("Failure") || response.getAck().equalsIgnoreCase("notSuccess"))  {
				order.getCreditCard().setPaymentNote("Error Code " + response.getLErrorCode() + "/ Severity Code " +response.getSeverityCode() + "-" +response.getLLongMessage() + ".");
			}
		}
		
		// Failure Response
		if (response.getAck() != null && response.getAck().equalsIgnoreCase("Failure") || response.getAck().equalsIgnoreCase("notSuccess") ) {
			System.out.println("Error Code " + response.getLErrorCode());
			System.out.println("Message " + response.getLLongMessage());
			System.out.println("Severity Code " + response.getSeverityCode());					
		} 
		
		return order;
	}
	
	public boolean DoDirectPayment(Order order, Map<String, Configuration> siteConfig, String paymentAction) throws Exception {		

		PayPalProResponse response = new PayPalProResponse();
		
		Address address = order.getBilling();
		CreditCard creditCard = order.getCreditCard();		
		
		// Action
		if (paymentAction != null) {
			paymentAction="Sale";
		} else {
			if (siteConfig.get("CREDIT_AUTO_CHARGE").getValue() !=null) {
				if (siteConfig.get("CREDIT_AUTO_CHARGE").getValue().equals("auth")) {
					paymentAction="Authorization";
				}
				if (siteConfig.get("CREDIT_AUTO_CHARGE").getValue().equals("charge") || siteConfig.get("CREDIT_AUTO_CHARGE").getValue().equals("")) {
					paymentAction="Sale";
				}
			} else {
				paymentAction="Sale";
			}
		}
				
		// Expiration Date 
		String expdate = creditCard.getExpireMonth().concat("20" + order.getCreditCard().getExpireYear());
		
		try
		{
			caller = new NVPCallerServices(); 
			if(setProfile(siteConfig) != null){
				caller.setAPIProfile(setProfile(siteConfig));
			}

			encoder.add("VERSION", "51.0");
			encoder.add("METHOD","DoDirectPayment");

			// Add request-specific fields to the request string.
			encoder.add("PAYMENTACTION",paymentAction);
			encoder.add("AMT",order.getBalance().toString());
			if(creditCard.getType().equalsIgnoreCase("MC")) {
				creditCard.setType("MasterCard"); // PayPal doesn't accept 'MC' it takes only 'MasterCard'
			}
			encoder.add("CREDITCARDTYPE",creditCard.getType());	
			encoder.add("ACCT",creditCard.getNumber());						
			encoder.add("EXPDATE",expdate);
			encoder.add("CVV2",creditCard.getCardCode());
			encoder.add("FIRSTNAME",address.getFirstName());
			encoder.add("LASTNAME",address.getLastName());										
			encoder.add("STREET",address.getAddr1());
			encoder.add("CITY",address.getCity());	
			encoder.add("STATE",address.getStateProvince());			
			encoder.add("ZIP",address.getZip());	
			encoder.add("COUNTRYCODE",address.getCountry());
			encoder.add("CURRENCYCODE",siteConfig.get("CURRENCY").getValue());

			// Execute the API operation and obtain the response.
			String NVPRequest = encoder.encode();
			String NVPResponse =(String) caller.call(NVPRequest);
			decoder.decode(NVPResponse);
			
			// Response 	
			response.setAck(decoder.get("ACK"));
			response.setCorrelationId(decoder.get("CORRELATIONID"));
			response.setTimeStamp(decoder.get("TIMESTAMP"));
			
			if (response.getAck().equalsIgnoreCase("Success")) {
				response.setTransactionId(decoder.get("TRANSACTIONID"));
				order.getCreditCard().setPaymentStatus(paymentAction); // Two possible actions: "Authorization" or "Sale"
			} else if (response.getAck().equalsIgnoreCase("Failure")   || response.getAck().equalsIgnoreCase("notSuccess") ) {
				response.setLErrorCode(decoder.get("L_ERRORCODE0"));
				response.setLLongMessage(decoder.get("L_LONGMESSAGE0"));
				response.setLShortMessage(decoder.get("L_SHORTMESSAGE0"));
				response.setSeverityCode(decoder.get("L_SEVERITYCODE0"));
			} else if (response.getAck().equalsIgnoreCase("SuccessWithWarning")) {
				response.setTransactionId(decoder.get("TRANSACTIONID"));
				response.setLErrorCode(decoder.get("L_ERRORCODE0"));
				response.setLLongMessage(decoder.get("L_LONGMESSAGE0"));
				response.setLShortMessage(decoder.get("L_SHORTMESSAGE0"));
				response.setSeverityCode(decoder.get("L_SEVERITYCODE0"));
				order.getCreditCard().setPaymentStatus(paymentAction); // Two possible actions: "Authorization" or "Sale"
			}
			setCreditCardTransactionResponse(order, response, siteConfig);
			
			if (siteConfig.get("CREDIT_AUTO_CHARGE").getValue().equals("charge") && (response.getAck().equalsIgnoreCase("Failure") || response.getAck().equalsIgnoreCase("notSuccess") )){
				return false;
			} else {
				return true;
			}
			
		} catch(Exception ex) {
			ex.printStackTrace();
			return false;
		}
	}	

	public boolean SetExpressCheckoutCode(Order order, Map<String, Configuration> siteConfig, HttpServletRequest request, HttpServletResponse response, PayPalProResponse payPalResponse)
	{
		String PAYPAL_URL;
		
		if (siteConfig.get("PAYPALPRO_ENVIRONMENT")!= null && siteConfig.get("PAYPALPRO_ENVIRONMENT").getValue().equals("sandbox")) {	       
	        PAYPAL_URL = "https://www.sandbox.paypal.com/webscr?cmd=_express-checkout&token=";
	    } else {
	        PAYPAL_URL = "https://www.paypal.com/cgi-bin/webscr?cmd=_express-checkout&token=";
	    }

		
		NVPEncoder encoder = new NVPEncoder();
		NVPDecoder decoder = new NVPDecoder();
		String paymentAction = "sale"; // Payment using account is always a sale.
		
		try
		{			
			caller = new NVPCallerServices();
			
			if(setProfile(siteConfig) != null){
				caller.setAPIProfile(setProfile(siteConfig));
			} 
			encoder.add("VERSION", "51.0");			
			encoder.add("METHOD","SetExpressCheckout");
			Double amt = Utilities.decimalFormat(order.getBalance(), null);
			
		// Add request-specific fields to the request string.
			encoder.add("RETURNURL",siteConfig.get("SITE_URL").getValue() + "ppProCheckout.jhtm?order_id="+order.getOrderId()+"&amt="+amt);
			encoder.add("CANCELURL",siteConfig.get("PAYPAL_CANCEL_RETURN").getValue());	
			encoder.add("AMT",amt.toString());
			encoder.add("PAYMENTACTION",paymentAction);
			encoder.add("CURRENCYCODE",siteConfig.get("CURRENCY").getValue());
	
		// Execute the API operation and obtain the response.
			String NVPRequest= encoder.encode();
			String NVPResponse =caller.call(NVPRequest);
			decoder.decode(NVPResponse);
			
			if( decoder.get("ACK") != null && decoder.get("ACK").equalsIgnoreCase("success") && decoder.get("TOKEN") != null ) {
				String link = PAYPAL_URL+decoder.get("TOKEN");
				response.sendRedirect(link);
				return true;
			} else  {
				payPalResponse.setLLongMessage(decoder.get("L_LONGMESSAGE0"));
				payPalResponse.setLErrorCode(decoder.get("L_ERRORCODE0"));
				return false;
			}
		} 
		catch (Exception ex)
		{
			ex.printStackTrace();
			return false;
		}		
	} 
	
	public Address getExpressCheckout(String token, Map<String, Configuration> siteConfig) {
		Address shippingAddress = new Address();
		NVPEncoder encoder = new NVPEncoder();
		NVPDecoder decoder = new NVPDecoder();
		try
		{			
			caller = new NVPCallerServices();
			if(setProfile(siteConfig) != null){
				caller.setAPIProfile(setProfile(siteConfig));
			}
			
			// Add request-specific fields to the request string.
			encoder.add("VERSION", "51.0");
			encoder.add("METHOD", "GetExpressCheckoutDetails");
			encoder.add("TOKEN", token);
			
			// Execute the API operation and obtain the response.
			String NVPRequest= encoder.encode();
			String NVPResponse =caller.call(NVPRequest);
			decoder.decode(NVPResponse);
			
			//If success, set the customer's shipping address from paypal account.
			if (decoder.get("ACK") != null && (decoder.get("ACK").equalsIgnoreCase("success") || decoder.get("ACK").equalsIgnoreCase("SuccessWithWarning"))) {
				shippingAddress.setFirstName(decoder.get("SHIPTONAME"));
				shippingAddress.setAddr1(decoder.get("SHIPTOSTREET"));
				shippingAddress.setAddr2(decoder.get("SHIPTOSTREET2"));
				shippingAddress.setCity(decoder.get("SHIPTOCITY"));
				shippingAddress.setStateProvince(decoder.get("SHIPTOSTATE"));
				shippingAddress.setCountry(decoder.get("SHIPTOCOUNTRYCODE"));
				shippingAddress.setZip(decoder.get("SHIPTOZIP"));
				shippingAddress.setCellPhone("");
				shippingAddress.setPhone("");
				shippingAddress.setFax("");
			}
			return shippingAddress;
		} catch (Exception ex)
		{
			ex.printStackTrace();
			return null;
		}		
	}
	
	public PayPalProResponse DoExpressCheckout(String token,String payerId, Map<String, Configuration> siteConfig , Double amt) {
		
		PayPalProResponse response = new PayPalProResponse();
		
		NVPEncoder encoder = new NVPEncoder();
		NVPDecoder decoder = new NVPDecoder();
		try
		{			
			caller = new NVPCallerServices();
			if(setProfile(siteConfig) != null){
				caller.setAPIProfile(setProfile(siteConfig));
			}
			encoder.add("VERSION", "51.0");
			encoder.add("METHOD","DoExpressCheckoutPayment");
			
			// Add request-specific fields to the request string.
			encoder.add("TOKEN",token);
			encoder.add("PAYERID",payerId);
			encoder.add("AMT",amt.toString());
			encoder.add("PAYMENTACTION","sale");
			encoder.add("CURRENCYCODE",siteConfig.get("CURRENCY").getValue());
			
			// Execute the API operation and obtain the response.
			String NVPRequest= encoder.encode();
			String NVPResponse =caller.call(NVPRequest);
			decoder.decode(NVPResponse);
			
			if (decoder.get("ACK").equalsIgnoreCase("success") || decoder.get("ACK").equalsIgnoreCase("SuccessWithWarning")) {
				response.setSuccess(true);
				response.setAmt(decoder.get("AMT"));
				response.setCorrelationId(decoder.get("CORRELATIONID"));
				response.setFeeAmt(decoder.get("FEEAMT"));
				response.setOrderTime(decoder.get("ORDERTIME"));
				response.setPendingReason(decoder.get("PENDINGREASON"));
				response.setTaxAmt(decoder.get("TAXAMT"));				
				response.setTransactionId(decoder.get("TRANSACTIONID"));
				return response;
			} else {
				response.setSuccess(false);
				response.setLErrorCode(decoder.get("L_ERRORCODE0"));
				response.setLLongMessage(decoder.get("L_LONGMESSAGE0"));
				response.setLShortMessage(decoder.get("L_SHORTMESSAGE0"));
				response.setSeverityCode(decoder.get("L_SEVERITYCODE0"));
				return response;
			}
			
		} catch (Exception ex) {
			ex.printStackTrace();
			response.setSuccess(false);
			return response;
		}		
	}
}