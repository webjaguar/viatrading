/* Copyright 2010 Advanced E-Media Solutions
 * @author Eduardo Asprec
 * @since 10.07.2010
 */

package com.webjaguar.thirdparty.payment.paymentech;

import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.Map;

import com.paymentech.orbital.sdk.configurator.Configurator;
import com.paymentech.orbital.sdk.configurator.ConfiguratorIF;
import com.paymentech.orbital.sdk.interfaces.RequestIF;
import com.paymentech.orbital.sdk.interfaces.ResponseIF;
import com.paymentech.orbital.sdk.interfaces.TransactionProcessorIF;
import com.paymentech.orbital.sdk.request.FieldNotFoundException;
import com.paymentech.orbital.sdk.request.Request;
import com.paymentech.orbital.sdk.transactionProcessor.TransactionException;
import com.paymentech.orbital.sdk.transactionProcessor.TransactionProcessor;
import com.paymentech.orbital.sdk.util.exceptions.InitializationException;
import com.webjaguar.model.Configuration;
import com.webjaguar.model.Order;

public class PaymentechApi {
	public boolean process(Order order, boolean authorizeOnly, Map<String, Configuration> siteConfig) 
		throws Exception {
		
		boolean success = false;
		boolean mfc = false;			// marked for capture
		
		// authorize and capture
		String messageType = "AC";
		if (authorizeOnly) {
			// authorize only
			messageType = "A";			
		} else if (order.getCreditCard().getPaymentStatus() != null &&
				order.getCreditCard().getPaymentStatus().equalsIgnoreCase("AUTHORIZED")) {
			// capture a previously authorized transaction
			mfc = true;
		}
		
		try {
		 	ConfiguratorIF configurator = Configurator.getInstance();
		} catch (InitializationException ie) {
			order.getCreditCard().setPaymentNote("Exception while initializing the SDK " + ie.toString());
		}

		NumberFormat nf = new DecimalFormat("#0");
		
		//Create a request object
		//The request object uses the XML templates along with data we provide
		//to generate a String representation of the xml request
		RequestIF requestIF = null;
		try {
		    //Tell the request object which template to use (see RequestIF.java)
			if (mfc) {
				requestIF = new Request(RequestIF.MARK_FOR_CAPTURE_TRANSACTION);
			} else {
			    requestIF = new Request(RequestIF.NEW_ORDER_TRANSACTION);				
			}
		    //If there were no errors preparing the template, we can now specify the data
		    //Basic Auth Fields
		    requestIF.setFieldValue("OrbitalConnectionUsername", siteConfig.get("CREDIT_ACCOUNT").getValue());
		    requestIF.setFieldValue("OrbitalConnectionPassword", siteConfig.get("CREDIT_PASSWORD").getValue());
		    requestIF.setFieldValue("MerchantID", siteConfig.get("CREDIT_MERCHANT_ID").getValue());
		    requestIF.setFieldValue("BIN", siteConfig.get("CREDIT_BIN").getValue());
		    requestIF.setFieldValue("OrderID",  order.getOrderId() + "");
		    requestIF.setFieldValue("Amount", nf.format(order.getBalance()*100));
		    
		    if (mfc) {
		    	requestIF.setFieldValue("TxRefNum", order.getCreditCard().getTransId());
		    } else {
			    requestIF.setFieldValue("IndustryType", "EC");
			    requestIF.setFieldValue("MessageType", messageType);
			    requestIF.setFieldValue("AccountNum", order.getCreditCard().getNumber());
			    requestIF.setFieldValue("CardSecValInd", "1");
			    requestIF.setFieldValue("CardSecVal", order.getCreditCard().getCardCode());
			    requestIF.setFieldValue("Exp", order.getCreditCard().getExpireMonth() + order.getCreditCard().getExpireYear()); 
			    // AVS Information
			    requestIF.setFieldValue("AVSname", order.getBilling().getFirstName() + " " + order.getBilling().getLastName());
			    requestIF.setFieldValue("AVSaddress1", order.getBilling().getAddr1());
			    requestIF.setFieldValue("AVScity", order.getBilling().getCity());
			    requestIF.setFieldValue("AVSstate", order.getBilling().getStateProvince());
			    requestIF.setFieldValue("AVSzip", order.getBilling().getZip());
			    // Additional Information
			    requestIF.setFieldValue("Comments", "");
			    requestIF.setFieldValue("ShippingRef", "");		    	
		    }
		} catch (InitializationException ie) {
			order.getCreditCard().setPaymentNote("Unable to initialize request object " + ie.toString());
		} catch (FieldNotFoundException fnfe) {
			order.getCreditCard().setPaymentNote("Unable to find XML field in template " + fnfe.toString());
		} catch (Exception e) {
			order.getCreditCard().setPaymentNote("Unable to initialize request object " + e.toString());
		}

		//Create a Transaction Processor
		//The Transaction Processor acquires and releases resources and executes transactions.
		//It configures a pool of protocol engines, then uses the pool to execute transactions.
		TransactionProcessorIF tp = null;
		try {
		    tp = new TransactionProcessor();
		} catch (InitializationException iex) {
			order.getCreditCard().setPaymentNote("TransactionProcessor failed to initialize " + iex.toString());
		}
		
		//Process the transaction
		//Pass in the request object (created above), and receive a response object.
		//If the resources required by the Transaction Processor have been exhausted,
		//this code will block until the resources become available.
		//The "TransactionProcessor.poolSize" configuration property specifies how many resources
		//will be available.  The TransactionProcessor acts as a governor, only allowing
		//up to "poolSize" transactions outstanding at any point in time.
		//As transactions are completed, their resources are placed back in the pool.
		ResponseIF resp = null;
		try {
		    resp = tp.process(requestIF);
		    if (resp.isGood()) {
		    	// approved
		    	order.getCreditCard().setTransId(resp.getTxRefNum());
		    	order.getCreditCard().setAuthCode(resp.getAuthCode());
		    	order.getCreditCard().setAvsCode(resp.getAVSResponseCode());
		    	order.getCreditCard().setCvv2Code(resp.getCVV2RespCode());
		    	// check transaction type
		    	if (mfc || requestIF.getField("MessageType").equals("AC") || 
						requestIF.getField("MessageType").equals("FC")) {
					order.getCreditCard().setPaymentStatus("CAPTURED");
				} else if (requestIF.getField("MessageType").equals("A")) {
					order.getCreditCard().setPaymentStatus("AUTHORIZED");
				}
		    	success = true;
		    	order.getCreditCard().setPaymentNote("Your transaction ref. no is " + resp.getTxRefNum());		    	
		    } else {
		    	order.getCreditCard().setPaymentNote("Error while processing " + resp.getMessage());		    	
		    }
		} catch (TransactionException tex) {
			order.getCreditCard().setPaymentNote("Transaction failed, including retries and failover " + tex.toString());
		}
		
		return success;
	}
}
