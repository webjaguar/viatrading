/*
 * Copyright 2007 Advanced E-Media Solutions
 */

package com.webjaguar.thirdparty.payment.authorizenet;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.net.URL;
import java.net.URLConnection;
import java.net.URLEncoder;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.Map;
import java.util.Vector;

import com.webjaguar.model.Configuration;
import com.webjaguar.model.Order;

public class AuthorizeNetApi
{

	public boolean authorize(Order order, boolean authorizeOnly, boolean priorAuth, Map<String, Configuration> siteConfig, Map<String, Object> gSiteConfig) throws Exception
	{
		boolean authorizeSuccess = false;
		
		String x_login = siteConfig.get("CREDIT_ACCOUNT").getValue();
		String x_tran_key = siteConfig.get("CREDIT_PASSWORD").getValue();
		String x_description = siteConfig.get("SITE_URL").getValue();
		boolean testMode = Boolean.parseBoolean(siteConfig.get("AUTHORIZE_TEST_MODE").getValue());
		
		ANetResponse aNetResp = new ANetResponse();
		
		// authorize and capture
		String transType = "AUTH_CAPTURE";
		if (authorizeOnly) {
			// authorize only
			transType = "AUTH_ONLY";			
		} else if (priorAuth && order.getCreditCard().getPaymentStatus() != null &&
				order.getCreditCard().getPaymentStatus().equalsIgnoreCase("AUTHORIZED")) {
			// capture a previously authorized transaction
			transType = "PRIOR_AUTH_CAPTURE";
		}
		
		// construct StringBuffer to send to Authorizenet
		StringBuffer sb = buildSb( order, transType, x_login, x_tran_key, x_description, testMode );
		// open secure connection and post the data in the StringBuffer
		URLConnection connection = sendSb( sb, false, siteConfig, gSiteConfig);
		// process and read the gateway response
		authorizeSuccess = getResponse( connection, order, aNetResp );
		// return flag

		order.getCreditCard().setPaymentNote( "Response Code " + 
				aNetResp.getReasonCode() + " - " + aNetResp.getReasonText());
		
		if (authorizeSuccess) {
			// approved
			order.getCreditCard().setTransId( aNetResp.getTransID() );
			// check transaction type
			if (aNetResp.getTransType().equalsIgnoreCase( "AUTH_CAPTURE" )
					|| aNetResp.getTransType().equalsIgnoreCase( "PRIOR_AUTH_CAPTURE")) {
				order.getCreditCard().setPaymentStatus("CAPTURED");							
			} else if (aNetResp.getTransType().equalsIgnoreCase( "AUTH_ONLY")) {
				order.getCreditCard().setPaymentStatus("AUTHORIZED");								
			}
		}
		
		return authorizeSuccess;
	}
	
	public boolean buyMassEmailCredit(Order order, Map<String, Configuration> siteConfig, Map<String, Object> gSiteConfig) throws Exception
	{
		boolean authorizeSuccess = false;
		
		String x_login = "advancedem5631";
		String x_tran_key = "4Mh56m9Bt49W8zup";
		String x_description = siteConfig.get("SITE_URL").getValue();
		boolean testMode = Boolean.parseBoolean(siteConfig.get("AUTHORIZE_TEST_MODE").getValue());
		
		ANetResponse aNetResp = new ANetResponse();
		
		// authorize and capture
		String transType = "AUTH_CAPTURE";
		
		if (order.getCreditCard().getPaymentStatus() != null &&
				order.getCreditCard().getPaymentStatus().equalsIgnoreCase("AUTHORIZED")) {
			// capture a previously authorized transaction
			transType = "PRIOR_AUTH_CAPTURE";
		}
		
		// construct StringBuffer to send to Authorizenet
		StringBuffer sb = buildSb( order, transType, x_login, x_tran_key, x_description, testMode );
		// open secure connection and post the data in the StringBuffer
		URLConnection connection = sendSb( sb, true, siteConfig, gSiteConfig );
		// process and read the gateway response
		authorizeSuccess = getResponse( connection, order, aNetResp );
		// return flag

		order.getCreditCard().setPaymentNote( "Response Code " + 
				aNetResp.getReasonCode() + " - " + aNetResp.getReasonText());
		
		if (authorizeSuccess) {
			// approved
			order.getCreditCard().setTransId( aNetResp.getTransID() );
			// check transaction type
			if (aNetResp.getTransType().equalsIgnoreCase( "AUTH_CAPTURE" )
					|| aNetResp.getTransType().equalsIgnoreCase( "PRIOR_AUTH_CAPTURE")) {
				order.getCreditCard().setPaymentStatus("CAPTURED");							
			} else if (aNetResp.getTransType().equalsIgnoreCase( "AUTH_ONLY")) {
				order.getCreditCard().setPaymentStatus("AUTHORIZED");								
			}
		}
		
		return authorizeSuccess;
	}

	private Vector split(String pattern, String in)
	{
		int s1 = 0, s2 = -1;
		Vector<String> out = new Vector<String>( 30 );
		while ( true )
		{
			s2 = in.indexOf( pattern, s1 );
			if ( s2 != -1 )
			{
				out.addElement( in.substring( s1, s2 ) );
			}
			else
			{
				// the end part of the string (string not pattern terminated)
				String _ = in.substring( s1 );
				if ( _ != null && !_.equals( "" ) )
				{
					out.addElement( _ );
				}
				break;
			}
			s1 = s2;
			s1 += pattern.length();
		}
		return out;
	}

	public static String toHexString(byte[] b)
	{
		StringBuffer sb = new StringBuffer( b.length * 2 );
		for ( int i = 0; i < b.length; i++ )
		{
			// look up high nibble char
			sb.append( hexChar[(b[i] & 0xf0) >>> 4] );
			// look up low nibble char
			sb.append( hexChar[b[i] & 0x0f] );
		}
		return sb.toString();
	}

	private StringBuffer buildSb(Order order, String transType, String x_login, String x_tran_key, String x_description, boolean testMode) throws UnsupportedEncodingException
	{
		NumberFormat nf = new DecimalFormat("#0.00");
		
		// create a new StringBuffer
		StringBuffer sb = new StringBuffer();
		// mandatory name/value pairs for all AIM CC transactions
		sb.append( "x_login=" + URLEncoder.encode(x_login, "UTF-8") );
		sb.append( "&x_tran_key=" + URLEncoder.encode(x_tran_key, "UTF-8") );
		sb.append( "&x_version=3.1" );
		if (testMode) {
			// test mode
			sb.append( "&x_test_request=TRUE" ); 
		} 
		sb.append( "&x_method=CC" );
		sb.append( "&x_type=" + transType );
		sb.append( "&x_amount=" + nf.format(order.getBalance()));
		sb.append( "&x_invoice_num=" + order.getOrderId() );
		sb.append( "&x_delim_data=TRUE" );
		sb.append( "&x_delim_char=|" );
		sb.append( "&x_relay_response=FALSE" );
		// credit card information
		sb.append( "&x_card_num=" + URLEncoder.encode(order.getCreditCard().getNumber(), "UTF-8") );
		sb.append( "&x_card_code=" + URLEncoder.encode(order.getCreditCard().getCardCode(), "UTF-8") );
		sb.append( "&x_exp_date=" + URLEncoder.encode(order.getCreditCard().getExpireMonth() + order.getCreditCard().getExpireYear(), "UTF-8") );
		// Billing Address
		sb.append( "&x_first_name=" + URLEncoder.encode(order.getBilling().getFirstName(), "UTF-8") );
		sb.append( "&x_last_name=" + URLEncoder.encode(order.getBilling().getLastName(), "UTF-8") );
		sb.append( "&x_company=" + URLEncoder.encode(order.getBilling().getCompany(), "UTF-8") );
		sb.append( "&x_address=" + URLEncoder.encode(order.getBilling().getAddr1(), "UTF-8") );
		sb.append( "&x_city=" + URLEncoder.encode(order.getBilling().getCity(), "UTF-8") );
		sb.append( "&x_state=" + URLEncoder.encode(order.getBilling().getStateProvince(), "UTF-8") );
		sb.append( "&x_zip=" + URLEncoder.encode(order.getBilling().getZip(), "UTF-8") );
		sb.append( "&x_country=" + URLEncoder.encode(order.getBilling().getCountry(), "UTF-8") );
		sb.append( "&x_phone=" + URLEncoder.encode(order.getBilling().getPhone(), "UTF-8") );
		sb.append( "&x_fax=" + URLEncoder.encode(order.getBilling().getFax(), "UTF-8") );
		// Shipping Address OPTIONAL
		if (order.getShipping() != null) {
			sb.append( "&x_ship_to_first_name=" + URLEncoder.encode(order.getShipping().getFirstName(), "UTF-8") );
			sb.append( "&x_ship_to_last_name=" + URLEncoder.encode(order.getShipping().getLastName(), "UTF-8") );
			sb.append( "&x_ship_to_company=" + URLEncoder.encode(order.getShipping().getCompany(), "UTF-8") );
			sb.append( "&x_ship_to_address=" + URLEncoder.encode(order.getShipping().getAddr1(), "UTF-8") );
			sb.append( "&x_ship_to_city=" + URLEncoder.encode(order.getShipping().getCity(), "UTF-8") );
			sb.append( "&x_ship_to_state=" + URLEncoder.encode(order.getShipping().getStateProvince(), "UTF-8") );
			sb.append( "&x_ship_to_zip=" + URLEncoder.encode(order.getShipping().getZip(), "UTF-8") );
			sb.append( "&x_ship_to_country=" + URLEncoder.encode(order.getShipping().getCountry(), "UTF-8") );
		}
		
		sb.append( "&x_description=" + URLEncoder.encode(x_description, "UTF-8") );
		sb.append( "&x_email=none" );
		// if prior_auth_capture, need trans_id
		if ( transType.equals( "PRIOR_AUTH_CAPTURE" ) )
		{
			sb.append( "&x_trans_id=" + order.getCreditCard().getTransId() );
		}
		// cardholder authentication
		if (order.getCreditCard().getEci() != null) {
			try {
				// remove leading zeroes
				sb.append("&x_authentication_indicator=" + Integer.parseInt(order.getCreditCard().getEci()));				
			} catch (Exception e) {
				sb.append("&x_authentication_indicator=" + order.getCreditCard().getEci());
			}			
			if (order.getCreditCard().getCavv() != null) {
				sb.append("&x_cardholder_authentication_value=" + URLEncoder.encode(order.getCreditCard().getCavv(), "UTF-8"));
			}
		}
		// return complete StringBuffer
		return sb;
	}

	private URLConnection sendSb(StringBuffer sb, boolean advancedEmediaTransaction, Map<String, Configuration> siteConfig, Map<String, Object> gSiteConfig) throws IOException
	{
		// open secure connection
		URL url = null;
		boolean testMode = Boolean.parseBoolean(siteConfig.get("AUTHORIZE_TEST_MODE").getValue());
		if(gSiteConfig.get("gCREDIT_CARD_PAYMENT").equals("authorizenet") || advancedEmediaTransaction) {
			url = new URL( "https://secure.authorize.net/gateway/transact.dll" );
		} else if(gSiteConfig.get("gCREDIT_CARD_PAYMENT").equals("ebizcharge")) {
			url = new URL( testMode ? "https://sandbox.ebizcharge.com/gateway/transact.dll" : "https://secure.ebizcharge.com/gateway/transact.dll");
		}
		URLConnection connection = url.openConnection();
		connection.setDoOutput( true );
		connection.setUseCaches( false );
		// not necessarily required but fixes a bug with some servers
		connection.setRequestProperty( "Content-Type", "application/x-www-form-urlencoded" );
		// post the data in the string buffer
		DataOutputStream out = new DataOutputStream( connection.getOutputStream() );
		out.write( sb.toString().getBytes() );
		out.flush();
		out.close();
		return connection;
	}

	private boolean getResponse(URLConnection connection, Order order, ANetResponse aNetResp) throws IOException
	{
		// receive response String from Authorizenet
		BufferedReader in = new BufferedReader( new InputStreamReader( connection.getInputStream() ) );
		String line;
		line = in.readLine();
		in.close();

		// make the reply readable (be sure to use the x_delim_char for the split operation)
		Vector ccrep = split( "|", line );

		aNetResp.setReasonCode( ccrep.elementAt( 2 ).toString() );
		aNetResp.setReasonText( ccrep.elementAt( 3 ).toString() );
		aNetResp.setTransID( ccrep.elementAt( 6 ).toString() );
		aNetResp.setTransType( ccrep.elementAt( 11 ).toString() );

		if ( ((String) ccrep.elementAt( 0 )).equals( "1" ) )
		{
			return true;
		}
		else
		{
			return false;
		}
	}

	// table to convert a nibble to a hex character
	static char[] hexChar = { '0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'A', 'B', 'C', 'D', 'E', 'F' };

}
