/* 
 * Copyright 2007 Advanced E-Media Solutions
 * @author Eduardo Asprec
 * @since 02.07.2007
 */

package com.webjaguar.thirdparty.payment.authorizenet;

public class ANetResponse {
	
	private String reasonCode;
	private String reasonText;
	private String transID;
	private String transType;	
	
	public String getReasonCode()
	{
		return reasonCode;
	}
	public void setReasonCode(String reasonCode)
	{
		this.reasonCode = reasonCode;
	}
	public String getReasonText()
	{
		return reasonText;
	}
	public void setReasonText(String reasonText)
	{
		this.reasonText = reasonText;
	}
	public String getTransID()
	{
		return transID;
	}
	public void setTransID(String transID)
	{
		this.transID = transID;
	}
	public String getTransType()
	{
		return transType;
	}
	public void setTransType(String transType)
	{
		this.transType = transType;
	}
	

}
