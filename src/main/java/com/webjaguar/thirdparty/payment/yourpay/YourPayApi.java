/* Copyright 2008 Advanced E-Media Solutions
 * @author Eduardo Asprec
 * @since 07.15.2008
 */

package com.webjaguar.thirdparty.payment.yourpay;

import java.io.StringReader;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.Map;

import org.jdom.Document;
import org.jdom.Element;
import org.jdom.input.SAXBuilder;

import lp.txn.JLinkPointTransaction;

import com.webjaguar.model.Configuration;
import com.webjaguar.model.Order;

public class YourPayApi {
	public boolean process(Order order, boolean authorizeOnly, Map<String, Configuration> siteConfig, String clientCertPath) 
		throws Exception {
		
		NumberFormat nf = new DecimalFormat("#0.00");
		
		boolean success = false;
		
		// authorize and capture
		String transType = "SALE";
		if (authorizeOnly) {
			// authorize only
			transType = "PREAUTH";			
		}
		if (order.getCreditCard().getPaymentStatus() != null &&
				order.getCreditCard().getPaymentStatus().equalsIgnoreCase("AUTHORIZED")) {
			// capture a previously authorized transaction
			transType = "POSTAUTH";
		}

		JLinkPointTransaction txn = new JLinkPointTransaction(); 

		String sClientCertPath = clientCertPath; 

		txn.setClientCertificatePath(sClientCertPath); 
		txn.setPassword("LinkPoint"); 
		txn.setHost("secure.linkpt.net"); 
		txn.setPort(1129);
		// -- Minimum Required Fields for a Credit Card SALE-->" 
		StringBuffer sbXml = new StringBuffer();
		sbXml.append("<order>");
		sbXml.append("<merchantinfo>");
		//-- Replace with your STORE NUMBER or STORENAME-->" 
		sbXml.append("<configfile>" + siteConfig.get("CREDIT_ACCOUNT").getValue() + "</configfile>"); 
		sbXml.append("</merchantinfo>");
		sbXml.append("<orderoptions>");
		sbXml.append("<ordertype>" + transType + "</ordertype>");
		//-- For a test, set result to GOOD, DECLINE, or DUPLICATE -->" 
		sbXml.append("<result>LIVE</result>");
		sbXml.append("</orderoptions>");
		sbXml.append("<payment>");
		sbXml.append("<chargetotal>" + nf.format(order.getBalance()) + "</chargetotal>");
		sbXml.append("</payment>");
		sbXml.append("<creditcard>"); 
		sbXml.append("<cardnumber>" + order.getCreditCard().getNumber() + "</cardnumber>");
		sbXml.append("<cardexpmonth>" + order.getCreditCard().getExpireMonth() + "</cardexpmonth>");
		sbXml.append("<cardexpyear>" + order.getCreditCard().getExpireYear() + "</cardexpyear>");
		 //-- CVM is the three-digit security code usually found on the signature line on the back of the card
		sbXml.append("<cvmvalue>" + order.getCreditCard().getCardCode() + "</cvmvalue>");
		sbXml.append("<cvmindicator>provided</cvmindicator>");
		sbXml.append("</creditcard>");
		sbXml.append("<billing>");
		
		String company = order.getBilling().getCompany();
		if (company != null) {
			company = company.replace("&", "&amp;");
		}
		sbXml.append("<company>" + company + "</company>");

		sbXml.append("<address1>" + order.getBilling().getAddr1() + "</address1>");
		sbXml.append("<address2>" + order.getBilling().getAddr2() + "</address2>");
		sbXml.append("<city>" + order.getBilling().getCity() + "</city>");
		sbXml.append("<state>" + order.getBilling().getStateProvince() + "</state>");
		sbXml.append("<country>" + order.getBilling().getCountry() + "</country>");
		sbXml.append("<phone>" + order.getBilling().getPhone() + "</phone>");
		sbXml.append("<fax>" + order.getBilling().getFax() + "</fax>");		
		sbXml.append("<email></email>"); 
		
		//-- Required for AVS. If not provided, transactions will downgrade.-->" 
		sbXml.append("<name>" + order.getBilling().getFirstName() + " " + 
				order.getBilling().getLastName() + "</name>");
		sbXml.append("<zip>" + order.getBilling().getZip() + "</zip>"); 
		String addrnum = "";
		if (order.getBilling().getAddr1().trim().length() > 0) {
			String addr1[] = order.getBilling().getAddr1().trim().split(" ");
			addrnum = addr1[0];
		}
		sbXml.append("<addrnum>" + addrnum + "</addrnum>");
		
		sbXml.append("</billing>");
		sbXml.append("<transactiondetails>");
		if (order.getCreditCard().getTransId() != null) {
			//-- Must be a valid order ID from a prior Sale or PreAuth -->" 
			sbXml.append("<oid>" + order.getCreditCard().getTransId() + "</oid>"); 
		} else {
			sbXml.append("<oid>" + siteConfig.get("CREDIT_PASSWORD").getValue().trim() + order.getOrderId() + "</oid>"); 			
		}
		sbXml.append("</transactiondetails>"); 			
		sbXml.append("</order>");
		
		SAXBuilder builder = new SAXBuilder(false);
		Document doc = builder.build(new StringReader("<root>" + txn.send(sbXml.toString()) + "</root>"));

		Element root = doc.getRootElement(); 

		success = root.getChild("r_approved").getText().equals("APPROVED");
		if (success) {
			// approved
			order.getCreditCard().setPaymentNote(root.getChild("r_message").getText());
			order.getCreditCard().setTransId(root.getChild("r_ordernum").getText());
			if (transType.equals("PREAUTH")) {
				order.getCreditCard().setPaymentStatus("AUTHORIZED");
			} else if (transType.equals("SALE") || transType.equals("POSTAUTH")) {
				order.getCreditCard().setPaymentStatus("CAPTURED");
			}
		} else {
			// get error
			order.getCreditCard().setPaymentNote(root.getChild("r_error").getText() + " - " + root.getChild("r_message").getText());
		}
		
		return success;
	}
}
