/* 
 * Copyright 2010 Advanced E-Media Solutions
 * @author Eduardo Asprec
 * @since 04.14.2010
 */

package com.webjaguar.thirdparty.payment.bankaudi;

import java.io.Serializable;
import java.math.BigInteger;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.*;

import javax.servlet.http.HttpServletRequest;

public class BankAudi implements Serializable {
	
	private String accessCode;
	private String merchant;
	private String amount;
	private String merchTxnRef;
	private String orderInfo;
	private String returnURL;
	private String vpc_SecureHash;
	private String vpc_TxnResponseCode;
	private String md5Key;
	
	private String vpc_Locale;
	private String vpc_BatchNo;
	private String vpc_Command;
	private String vpc_Message;
	private String vpc_Version;
	private String vpc_Card;
	private String vpc_ReceiptNo;
	private String vpc_AuthorizeId;
	private String vpc_TransactionNo;
	private String vpc_AcqResponseCode;
	
	private Date paymentDate;
	
	public String getAccessCode() {
		return accessCode;
	}
	public void setAccessCode(String accessCode) {
		this.accessCode = accessCode;
	}
	public String getMerchant() {
		return merchant;
	}
	public void setMerchant(String merchant) {
		this.merchant = merchant;
	}
	public String getAmount() {
		return amount;
	}
	public void setAmount(String amount) {
		this.amount = amount;
	}
	public String getMerchTxnRef() {
		return merchTxnRef;
	}
	public void setMerchTxnRef(String merchTxnRef) {
		this.merchTxnRef = merchTxnRef;
	}
	public String getOrderInfo() {
		return orderInfo;
	}
	public void setOrderInfo(String orderInfo) {
		this.orderInfo = orderInfo;
	}
	public String getReturnURL() {
		return returnURL;
	}
	public void setReturnURL(String returnURL) {
		this.returnURL = returnURL;
	}
	public String getVpc_SecureHash() {
		return vpc_SecureHash;
	}
	public void setVpc_SecureHash(String vpcSecureHash) {
		vpc_SecureHash = vpcSecureHash;
	}
	
	public String getVpc_TxnResponseCode() {
		return vpc_TxnResponseCode;
	}
	public void setVpc_TxnResponseCode(String vpcTxnResponseCode) {
		vpc_TxnResponseCode = vpcTxnResponseCode;
	}
	public String getMd5Key() {
		return md5Key;
	}
	public void setMd5Key(String md5Key) {
		this.md5Key = md5Key;
	}
	public String getVpc_Locale() {
		return vpc_Locale;
	}
	public void setVpc_Locale(String vpcLocale) {
		vpc_Locale = vpcLocale;
	}
	public String getVpc_BatchNo() {
		return vpc_BatchNo;
	}
	public void setVpc_BatchNo(String vpcBatchNo) {
		vpc_BatchNo = vpcBatchNo;
	}
	public String getVpc_Command() {
		return vpc_Command;
	}
	public void setVpc_Command(String vpcCommand) {
		vpc_Command = vpcCommand;
	}
	public String getVpc_Message() {
		return vpc_Message;
	}
	public void setVpc_Message(String vpcMessage) {
		vpc_Message = vpcMessage;
	}
	public String getVpc_Version() {
		return vpc_Version;
	}
	public void setVpc_Version(String vpcVersion) {
		vpc_Version = vpcVersion;
	}
	public String getVpc_Card() {
		return vpc_Card;
	}
	public void setVpc_Card(String vpcCard) {
		vpc_Card = vpcCard;
	}
	public String getVpc_ReceiptNo() {
		return vpc_ReceiptNo;
	}
	public void setVpc_ReceiptNo(String vpcReceiptNo) {
		vpc_ReceiptNo = vpcReceiptNo;
	}
	public String getVpc_AuthorizeId() {
		return vpc_AuthorizeId;
	}
	public void setVpc_AuthorizeId(String vpcAuthorizeId) {
		vpc_AuthorizeId = vpcAuthorizeId;
	}
	public String getVpc_TransactionNo() {
		return vpc_TransactionNo;
	}
	public void setVpc_TransactionNo(String vpcTransactionNo) {
		vpc_TransactionNo = vpcTransactionNo;
	}
	public String getVpc_AcqResponseCode() {
		return vpc_AcqResponseCode;
	}
	public void setVpc_AcqResponseCode(String vpcAcqResponseCode) {
		vpc_AcqResponseCode = vpcAcqResponseCode;
	}
	public Date getPaymentDate() {
		return paymentDate;
	}
	public void setPaymentDate(Date paymentDate) {
		this.paymentDate = paymentDate;
	}
	public boolean isValidated(HttpServletRequest request) {
		boolean validated = false;
		StringBuffer data = new StringBuffer(md5Key);
		Enumeration en = request.getParameterNames();
		Map<String, String> params = new TreeMap<String, String>();
		while (en.hasMoreElements()) {
		    String paramName = (String) en.nextElement();
		    String paramValue = request.getParameter(paramName);
		    params.put(paramName, paramValue);
		}
		for (String key: params.keySet()) {
			if (!key.equals("vpc_SecureHash") && !key.equals("action") && params.get(key).length() > 0) {
				data.append(params.get(key));				
			}
		}
		try {
			validated = vpc_SecureHash.equals(md5(data.toString()));			
		} catch (Exception e) {
			// do nothing
		}
		return validated;
	}	
	
	public String md5(String data) {
		String hashword = null;
		try {
			MessageDigest md5 = MessageDigest.getInstance("MD5");
			md5.update(data.getBytes());
			BigInteger hash = new BigInteger(1, md5.digest());
			hashword = hash.toString(16);
		} catch (NoSuchAlgorithmException nsae) {
			return null;
		}
		hashword = pad(hashword, 32, '0');
		return hashword;
	}

	private String pad(String s, int length, char pad) {
		StringBuffer buffer = new StringBuffer(s);
		while (buffer.length() < length) {
			buffer.insert(0, pad);
		}
		return buffer.toString();
	}
	
}
