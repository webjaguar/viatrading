/* 
 * Copyright 2007 Advanced E-Media Solutions
 * @author Eduardo Asprec
 * @since 12.18.2007
 */

package com.webjaguar.thirdparty.payment.netcommerce;

import java.io.Serializable;
import java.math.BigInteger;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.sql.Timestamp;

public class NetCommerce implements Serializable {
	
	private String respVal;
	private String respMsg;
	private String txtIndex;
	private String txtMerchNum;
	private String txtNumAut;
	private String txtAmount;
	private String txtCurrency;
	private String signature;
	private String errCode;
	private String md5Key;
	private Timestamp paymentDate;
	
	public String getRespVal()
	{
		return respVal;
	}
	public void setRespVal(String respVal)
	{
		this.respVal = respVal;
	}
	public String getRespMsg()
	{
		return respMsg;
	}
	public void setRespMsg(String respMsg)
	{
		this.respMsg = respMsg;
	}
	public String getTxtIndex()
	{
		return txtIndex;
	}
	public void setTxtIndex(String txtIndex)
	{
		this.txtIndex = txtIndex;
	}
	public String getTxtMerchNum()
	{
		return txtMerchNum;
	}
	public void setTxtMerchNum(String txtMerchNum)
	{
		this.txtMerchNum = txtMerchNum;
	}
	public String getTxtNumAut()
	{
		return txtNumAut;
	}
	public void setTxtNumAut(String txtNumAut)
	{
		this.txtNumAut = txtNumAut;
	}
	public String getTxtAmount()
	{
		return txtAmount;
	}
	public void setTxtAmount(String txtAmount)
	{
		this.txtAmount = txtAmount;
	}
	public String getTxtCurrency()
	{
		return txtCurrency;
	}
	public void setTxtCurrency(String txtCurrency)
	{
		this.txtCurrency = txtCurrency;
	}
	public String getSignature()
	{
		return signature;
	}
	public void setSignature(String signature)
	{
		this.signature = signature;
	}
	public String getErrCode()
	{
		return errCode;
	}
	public void setErrCode(String errCode)
	{
		this.errCode = errCode;
	}
	public String getMd5Key()
	{
		return md5Key;
	}
	public void setMd5Key(String md5Key)
	{
		this.md5Key = md5Key;
	}
	public Timestamp getPaymentDate()
	{
		return paymentDate;
	}
	public void setPaymentDate(Timestamp paymentDate)
	{
		this.paymentDate = paymentDate;
	}
	
	
	public boolean isValidated() {
		boolean validated = false;
		try {
			String password = txtMerchNum + txtIndex + txtAmount + txtCurrency + txtNumAut + respVal + respMsg + md5Key;
			validated = signature.equals(md5(password));			
		} catch (Exception e) {
			// do nothing
		}
		return validated;
	}
	
	
	private String md5(String password) {
		String hashword = null;
		try {
			MessageDigest md5 = MessageDigest.getInstance("MD5");
			md5.update(password.getBytes());
			BigInteger hash = new BigInteger(1, md5.digest());
			hashword = hash.toString(16);
		} catch (NoSuchAlgorithmException nsae) {
			return null;
		}
		hashword = pad(hashword, 32, '0');
		return hashword;
	}

	private String pad(String s, int length, char pad) {
		StringBuffer buffer = new StringBuffer(s);
		while (buffer.length() < length) {
			buffer.insert(0, pad);
		}
		return buffer.toString();
	}
}
