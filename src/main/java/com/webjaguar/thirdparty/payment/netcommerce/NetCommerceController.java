/* Copyright 2007 Advanced E-Media Solutions
 * @author Eduardo Asprec
 * @since 12.18.2007
 */


package com.webjaguar.thirdparty.payment.netcommerce;

import java.io.*;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.mail.internet.MimeMessage;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.mail.javamail.JavaMailSenderImpl;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.validation.BindException;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.AbstractCommandController;

import com.webjaguar.dao.OrderDao;
import com.webjaguar.logic.WebJaguarFacade;
import com.webjaguar.model.Configuration;
import com.webjaguar.model.Customer;
import com.webjaguar.model.Layout;
import com.webjaguar.model.MultiStore;
import com.webjaguar.model.Order;
import com.webjaguar.model.OrderStatus;
import com.webjaguar.model.SiteMessage;

public class NetCommerceController extends AbstractCommandController {
	
	WebJaguarFacade webJaguar;
	public void setWebJaguar(WebJaguarFacade webJaguar){ this.webJaguar = webJaguar; }

	private OrderDao orderDao;
	public void setOrderDao(OrderDao orderDao) { this.orderDao = orderDao; }
	
	private JavaMailSenderImpl mailSender;
	public void setMailSender(JavaMailSenderImpl mailSender) { this.mailSender = mailSender; }

	public NetCommerceController() {
		setCommandClass(NetCommerce.class);
	}  

    public ModelAndView handle(HttpServletRequest request, HttpServletResponse response,
			Object command, BindException errors)
        throws Exception {
    	
		// site configuration
		Map<String, Configuration> siteConfig = (Map<String, Configuration>) request.getAttribute("siteConfig");
		Map<String, Object> model = new HashMap<String, Object>();
		
		// net commerce response
		NetCommerce trans = (NetCommerce) command;
		trans.setRespMsg(request.getParameter("RespMsg"));
		trans.setRespVal(request.getParameter("RespVal"));
		trans.setErrCode(request.getParameter("ErrCode"));
		trans.setMd5Key(siteConfig.get("CREDIT_PASSWORD").getValue());
		System.out.println("trans.isValidated() " + trans.isValidated());
		
		if (trans.isValidated()) {
			try {
				// validated
				Order order = orderDao.getOrder(Integer.parseInt(trans.getTxtIndex()), null);
				model.put("order", order);
				if (order.getPaymentMethod().equalsIgnoreCase("netcommerce")) {
					OrderStatus orderStatus = null;
					if (order.getStatus().equalsIgnoreCase("xnc")) {	// netcommerce canceled
						// update status
		    			orderStatus = new OrderStatus();
		    			orderStatus.setOrderId(order.getOrderId());
		    			orderStatus.setStatus("p"); // pending
		    			SiteMessage siteMessage = null;
		    			try {
		    				Integer messageId = null;
		    				// multi store
		    				List<MultiStore> multiStores = this.webJaguar.getMultiStore(order.getHost());
		    				MultiStore multiStore = null;
		    				if (multiStores.size() > 0) {
		    					multiStore = multiStores.get(0);
		    					messageId = multiStore.getMidNewOrders();
		    				} else {
			    				messageId = Integer.parseInt( siteConfig.get( "SITE_MESSAGE_ID_FOR_NEW_ORDERS" ).getValue() );		    					
		    				}
		    				siteMessage = this.webJaguar.getSiteMessageById( messageId );
		    				if (siteMessage != null) {
		    					// send email
		    					orderStatus.setSubject( siteMessage.getSubject() );
		    					orderStatus.setMessage( siteMessage.getMessage() );
		    					orderStatus.setHtmlMessage(siteMessage.isHtml());
		    					if (notifyCustomer(orderStatus, siteConfig, order, multiStore)) {
			    					orderStatus.setUserNotified( true );		    							
		    					}
		    				}
		    			} catch (NumberFormatException e) {
		    				// do nothing
		    			}
					}
					orderDao.updateNetCommercePayment(trans, orderStatus);
					// buy safe
					if( !siteConfig.get("BUY_SAFE_URL").getValue().equals("") ) {
						try {
							this.webJaguar.setShoppingCartCheckout( order, mailSender );
						} catch (Exception e) { 
							e.printStackTrace();
							OrderStatus status = new OrderStatus();
							status.setOrderId( order.getOrderId() );
							status.setStatus( "p" ); // pending
							if( (order.getWantsBond() != null) && (order.getWantsBond() == true) ) {
								status.setComments("BuySafe Bond was selected but cancelled as system failed to connect Buysafe. Please check the amount charged and refund the bond cost to Customer.");
							} else {
								status.setComments("BuySafe Bond was not selected. System failed to connect Buysafe");
							}
							order.setWantsBond(false);
							order.setBondCost(null);
							order.setGrandTotal();
							
							this.webJaguar.cancelBuySafeBond(order, status);
						}
					}
				}
			} catch (Exception e) {
				// do nothing
			}
			saveNetCommerceTransaction(request);

			model.put("netCommerce", trans);
			model.put("mainCategories", this.webJaguar.getMainCategoryLinks(request,null));
			Layout layout = (Layout) request.getAttribute( "layout" );
			if ( ((Configuration) siteConfig.get( "LEFTBAR_HIDE_ON_SYSTEM_PAGES" )).getValue().equals( "true" ) )
			{
				layout.setLeftBarTopHtml( "" );
				layout.setLeftBarBottomHtml( "" );
				layout.setHideLeftBar( true );
			}
		} else {
			Order order = orderDao.getOrder(Integer.parseInt(trans.getTxtIndex()), null);
			model.put("order", order);
			
			model.put("netCommerce", trans);
			model.put("mainCategories", this.webJaguar.getMainCategoryLinks(request,null));
			Layout layout = (Layout) request.getAttribute( "layout" );
			if ( ((Configuration) siteConfig.get( "LEFTBAR_HIDE_ON_SYSTEM_PAGES" )).getValue().equals( "true" ) )
			{
				layout.setLeftBarTopHtml( "" );
				layout.setLeftBarBottomHtml( "" );
				layout.setHideLeftBar( true );
			}
		}
		
		return new ModelAndView("frontend/checkout/netCommerce", "model", model);
    }
    
    private void saveNetCommerceTransaction(HttpServletRequest request) throws Exception {
    	File baseFile = new File(getServletContext().getRealPath("/"));
    	File ipnFile = new File(baseFile, "netcommerce");
    	FileOutputStream fo = new FileOutputStream(ipnFile, true);
    	PrintWriter pw = new PrintWriter(fo);
		pw.write( "IP Address=" + request.getRemoteAddr() + "\n");
		Enumeration en = request.getParameterNames();
		while(en.hasMoreElements()){
		    String paramName = (String)en.nextElement();
		    String paramValue = request.getParameter(paramName);
		    pw.write( paramName + "=" + paramValue + "\n");
		}
		pw.write("--------------------------------------------\n");
		pw.close();
    	fo.close();
    }
	
	private boolean notifyCustomer(OrderStatus orderStatus, Map<String, Configuration> siteConfig, Order order, MultiStore multiStore) {

		Customer customer = this.webJaguar.getCustomerById(order.getUserId());
		
		if (!customer.isEmailNotify()) {
			// do not send email
			return false;
		}
		
		// send email
    	String contactEmail = siteConfig.get("CONTACT_EMAIL").getValue();
    	String secureUrl =this.webJaguar.getSecureUrl(siteConfig, multiStore);
		if (multiStore != null) {
			contactEmail = multiStore.getContactEmail();
			secureUrl = "http://" + multiStore.getHost() + "/";
		}
    	SimpleDateFormat dateFormatter = new SimpleDateFormat("MMM d, yyyy HH:mm:ss a z");

    	StringBuffer messageBuffer = new StringBuffer();
    	messageBuffer.append( dateFormatter.format(new Date()) + "\n\n" );
    	
    	// replace dyamic elements
    	try {
    		this.webJaguar.replaceDynamicElement( orderStatus, customer, null, order, secureUrl, null);
    	} catch (Exception e) {e.printStackTrace();}
    	messageBuffer.append( orderStatus.getMessage() );
		
    	try {
	    	// construct email message
	       	MimeMessage mms = mailSender.createMimeMessage();
			MimeMessageHelper helper = new MimeMessageHelper(mms, true, "UTF-8");
			helper.setTo(customer.getUsername());
			helper.setFrom(contactEmail);
			helper.setBcc(contactEmail);
			helper.setSubject(orderStatus.getSubject());
            helper.setText(messageBuffer.toString(), orderStatus.isHtmlMessage());
			
			mailSender.send(mms);
		} catch (Exception ex) {
			// do nothing, customer not notified
			return false;
		} 		
		
		return true;
	}	
}
