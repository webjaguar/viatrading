/* Copyright 2010 Advanced E-Media Solutions
 * @author Eduardo Asprec
 * @since 03.17.2010
 */

package com.webjaguar.thirdparty.payment.amazon;

import java.io.File;
import java.net.MalformedURLException;
import java.net.URL;
import java.rmi.RemoteException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.TimeZone;

import javax.activation.DataHandler;
import javax.activation.FileDataSource;
import javax.crypto.Mac;
import javax.crypto.spec.SecretKeySpec;
import javax.xml.rpc.ServiceException;
import javax.xml.soap.SOAPException;

import merchantAtAPIs.remote.DocumentProcessingInfo;
import merchantAtAPIs.remote.DocumentSubmissionResponse;
import merchantAtAPIs.remote.Merchant;
import merchantAtAPIs.remote.MerchantInterfaceMimeLocator;
import merchantAtAPIs.remote.MerchantInterface_BindingStub;
import merchantAtAPIs.remote.MerchantInterface_PortType;

import org.apache.axis.holders.OctetStreamHolder;
import org.apache.commons.codec.binary.Base64;

public class AmazonApi {
	private static final String MERCHANT_INTERFACE_PORT_ADDRESS = "https://merchant-api.amazon.com/gateway/merchant-interface-mime";	

	// MessageType strings for XML and Flat file feed processing - these are not all the defined types
	public static String MESSAGE_TYPE_ORDER_ACKNOWLEDGE_FEED_XML = "_POST_ORDER_ACKNOWLEDGEMENT_DATA_";
	public static String MESSAGE_TYPE_ORDER_FULFILLMENT_FEED_XML = "_POST_ORDER_FULFILLMENT_DATA_";
	public static String DOCUMENT_PROCESSING_STATUS_COMPLETE = "_DONE_";
	
	// poll every 10 seconds to see if the document has completed
	private static final int DOCUMENT_PROCESSING_STATUS_POLL_INTERVAL = 10000;
	
	// times out after ten minutes
	private static final int DOCUMENT_PROCESSING_STATUS_POLL_TIMEOUT = 600000;
	
	// MerchantInterface contains session information with Amazon servers
	private MerchantInterface_PortType merchantInterface = null;

	// Merchant object is used to authenticate
	private Merchant merchant = null;
	
	public static String getSignature(String cart, String secretKey) {
		String result = null;
		
		try {

			// get an hmac_sha1 key from the raw key bytes
			SecretKeySpec signingKey = new SecretKeySpec(secretKey.getBytes(), "HmacSHA1");

			// get an hmac_sha1 Mac instance and initialize with the signing key
			Mac mac = Mac.getInstance("HmacSHA1");
			mac.init(signingKey);

			// compute the hmac on input data bytes
			byte[] rawHmac = mac.doFinal(cart.getBytes());

			// base64-encode the hmac
			result = new String(Base64.encodeBase64(rawHmac));
		} catch (Exception e) {
			// do nothing
		}	
		
		return result;
	}
	
	public static boolean validateTimeStamp(String timestamp, long TIMESTAMP_WINDOW) {

		String TIMEZONE = "GMT";
		String TIMESTAMP_FORMAT = "yyyy-MM-dd'T'HH:mm:ss'.'SSS'Z'";
		
		if (timestamp == null) {
			return false;
		}

		SimpleDateFormat dateFormatter = new SimpleDateFormat(TIMESTAMP_FORMAT);
		dateFormatter.setTimeZone(TimeZone.getTimeZone(TIMEZONE));
		
		Date dateFromTimeStamp = null;
		try {
			dateFromTimeStamp = dateFormatter.parse(timestamp);		
		} catch (ParseException e) {
			return false;
		}

		Calendar calendarInstance = GregorianCalendar.getInstance();
		calendarInstance.setTime(dateFromTimeStamp);

		long epocRepresentation = calendarInstance.getTimeInMillis();
		
		// If the timestamp is older than timestamp window then discard the request
		if (epocRepresentation < (System.currentTimeMillis() - TIMESTAMP_WINDOW)) {
			return false;
		}
		return true;

	}
	
	public static String getSignature(String uuid, String timestamp, String secretKey) {
		String result = null;
		
		try {
			// Create the data to be signed
	        String data = uuid + timestamp;

			// get an hmac_sha1 key from the raw key bytes
			SecretKeySpec signingKey = new SecretKeySpec(secretKey.getBytes(), "HmacSHA1");

			// get an hmac_sha1 Mac instance and initialize with the signing key
			Mac mac = Mac.getInstance("HmacSHA1");
			mac.init(signingKey);

			// compute the hmac on input data bytes
			byte[] rawHmac = mac.doFinal(data.getBytes());

			// base64-encode the hmac
			result = new String(Base64.encodeBase64(rawHmac));
		} catch (Exception e) {
			// do nothing
		}	
		
		return result;
	}	
		
	public AmazonApi(String merchantToken, String merchantName, String email, String password)
		throws ServiceException, MalformedURLException {

		// Initialization of merchant object
		merchant = new Merchant();
		merchant.setMerchantIdentifier(merchantToken);
		merchant.setMerchantName(merchantName);

		// Initializes merchantInterface
		// For Amazon.com, use: https://merchant-api.amazon.com/gateway/merchant-interface-mime
		merchantInterface = new MerchantInterfaceMimeLocator()
				.getMerchantInterface(new URL(MERCHANT_INTERFACE_PORT_ADDRESS));

		// Email address and password you use to login into seller central
		((MerchantInterface_BindingStub) merchantInterface).setUsername(email);
		((MerchantInterface_BindingStub) merchantInterface).setPassword(password);		
	}
	
	public Long acknowledgeOrder(File file) throws RemoteException {
		DataHandler dataHandler = new DataHandler(new FileDataSource(file));
		DocumentSubmissionResponse response = merchantInterface.postDocument(merchant, AmazonApi.MESSAGE_TYPE_ORDER_ACKNOWLEDGE_FEED_XML, dataHandler);
		return response.getDocumentTransactionID();
	}
	
	public Long shipOrder(File file) throws RemoteException {
		DataHandler dataHandler = new DataHandler(new FileDataSource(file));
		DocumentSubmissionResponse response = merchantInterface.postDocument(merchant, AmazonApi.MESSAGE_TYPE_ORDER_FULFILLMENT_FEED_XML, dataHandler);
		return response.getDocumentTransactionID();
	}
	
	public DocumentProcessingInfo getDocumentProcessingInfo(Long amazonDocTransID) throws RemoteException, InterruptedException, SOAPException, Exception {
		int processingTime = 0;
		
		DocumentProcessingInfo procInfo = merchantInterface.getDocumentProcessingStatus(merchant, amazonDocTransID);
    	
    	while(!DOCUMENT_PROCESSING_STATUS_COMPLETE.equals(procInfo.getDocumentProcessingStatus())) {
    		if (processingTime >= DOCUMENT_PROCESSING_STATUS_POLL_TIMEOUT) {
				throw new RemoteException("Timed out waiting for document to complete processing: documentID: " +
						amazonDocTransID +
						" status: " + procInfo.getDocumentProcessingStatus());
			}
    		
            System.out.println("Waiting " + DOCUMENT_PROCESSING_STATUS_POLL_INTERVAL +
                    " millis. for transaction: " + amazonDocTransID + " to complete...");	    	    		
    		
            // Waits for DOCUMENT_PROCESSING_STATUS_POLL_INTERVAL to try again 
			Thread.sleep(DOCUMENT_PROCESSING_STATUS_POLL_INTERVAL);
            processingTime += DOCUMENT_PROCESSING_STATUS_POLL_INTERVAL;
    		
    		// retrieve new status
			procInfo = merchantInterface.getDocumentProcessingStatus(merchant, amazonDocTransID);
    	}
    	return procInfo;
	}
	
	public Object[] getDocument(String documentID) throws RemoteException, SOAPException {
		// Downloads the report
		merchantInterface.getDocument(merchant, documentID, new OctetStreamHolder());

		// returns attachments
		return (Object[]) ((MerchantInterface_BindingStub) merchantInterface).getAttachments();
	}

}
