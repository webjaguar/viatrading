/* Copyright 2010 Advanced E-Media Solutions
 * @author Eduardo Asprec
 * @since 03.24.2010
 */

package com.webjaguar.thirdparty.payment.amazon;

public class Iopn {
	
	private String uuid;
	private String timestamp;
	private String signature;	
	private String notificationType;
	private String notificationData;	
	private String awsAccessKeyId;
	private String notificationRefId;
	private String amazonOrderID;
	private String orderChannel;
	private String orderNumber;
	
	public String getUuid() {
		return uuid;
	}
	public void setUuid(String uuid) {
		this.uuid = uuid;
	}
	public String getTimestamp() {
		return timestamp;
	}
	public void setTimestamp(String timestamp) {
		this.timestamp = timestamp;
	}
	public String getSignature() {
		return signature;
	}
	public void setSignature(String signature) {
		this.signature = signature;
	}
	public String getNotificationType() {
		return notificationType;
	}
	public void setNotificationType(String notificationType) {
		this.notificationType = notificationType;
	}
	public String getNotificationData() {
		return notificationData;
	}
	public void setNotificationData(String notificationData) {
		this.notificationData = notificationData;
	}
	public String getAwsAccessKeyId() {
		return awsAccessKeyId;
	}
	public void setAwsAccessKeyId(String awsAccessKeyId) {
		this.awsAccessKeyId = awsAccessKeyId;
	}
	public String getNotificationRefId() {
		return notificationRefId;
	}
	public void setNotificationRefId(String notificationRefId) {
		this.notificationRefId = notificationRefId;
	}
	public String getAmazonOrderID() {
		return amazonOrderID;
	}
	public void setAmazonOrderID(String amazonOrderID) {
		this.amazonOrderID = amazonOrderID;
	}
	public String getOrderChannel() {
		return orderChannel;
	}
	public void setOrderChannel(String orderChannel) {
		this.orderChannel = orderChannel;
	}
	public String getOrderNumber() {
		return orderNumber;
	}
	public void setOrderNumber(String orderNumber) {
		this.orderNumber = orderNumber;
	}

}
