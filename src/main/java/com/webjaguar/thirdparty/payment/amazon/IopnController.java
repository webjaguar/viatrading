/* Copyright 2010 Advanced E-Media Solutions
 * @author Eduardo Asprec
 * @since 03.22.2010
 */

package com.webjaguar.thirdparty.payment.amazon;

import java.io.*;
import java.net.MalformedURLException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Enumeration;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.mail.internet.MimeMessage;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.jdom.Document;
import org.jdom.Element;
import org.jdom.input.SAXBuilder;
import org.springframework.mail.MailException;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSenderImpl;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.web.context.support.WebApplicationObjectSupport;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.Controller;

import com.webjaguar.logic.WebJaguarFacade;
import com.webjaguar.model.Configuration;
import com.webjaguar.model.Customer;
import com.webjaguar.model.LineItem;
import com.webjaguar.model.MultiStore;
import com.webjaguar.model.Order;
import com.webjaguar.model.OrderStatus;
import com.webjaguar.model.SiteMessage;

public class IopnController extends WebApplicationObjectSupport implements Controller {
	
	WebJaguarFacade webJaguar;
	public void setWebJaguar(WebJaguarFacade webJaguar){ this.webJaguar = webJaguar; }
	
	private JavaMailSenderImpl mailSender;
	public void setMailSender(JavaMailSenderImpl mailSender) { this.mailSender = mailSender; }
    
	public ModelAndView handleRequest(HttpServletRequest request, HttpServletResponse response)
		throws Exception {	
    	
		long TIMESTAMP_WINDOW = 15 * 60 * 1000; // 15 minutes
		
		if (!request.getMethod().equals("POST")) {
			response.sendError(HttpServletResponse.SC_METHOD_NOT_ALLOWED, "HTTP POST required");
			return null;
		}
		
		Iopn trans = new Iopn();
		trans.setAwsAccessKeyId(request.getParameter("AWSAccessKeyId"));
		trans.setUuid(request.getParameter("UUID"));
		trans.setTimestamp(request.getParameter("Timestamp"));
		trans.setSignature(request.getParameter("Signature"));
		
		// Authenticate the request using UUID, Timestamp and Signature
		if (trans.getSignature() == null || trans.getUuid() == null || trans.getTimestamp() == null || trans.getAwsAccessKeyId() == null) {
			throw new Exception("UUID/Timestamp/Signature/AWSAccessKeyId is missing in the Notification.");
		}
		
		// site configuration
		Map<String, Configuration> siteConfig = webJaguar.getSiteConfig();
		
		// check if AWSAccessKeyId is valid
        if (!trans.getAwsAccessKeyId().equals(siteConfig.get("AMAZON_ACCESS_KEY").getValue())) {
            response.sendError(HttpServletResponse.SC_FORBIDDEN, 
            		"AWSAccessKeyId is incorrect. Received: " + trans.getAwsAccessKeyId());
            return null;
        }
		
		if (!AmazonApi.validateTimeStamp(trans.getTimestamp(), TIMESTAMP_WINDOW)) {
			// invalid timestamp
			throw new Exception("Timestamp is older than " + (TIMESTAMP_WINDOW / (1000 * 60)) + " minutes");
		}
		
		// Calculate the signature using UUID, timestamp and the awsSecretKey
        String calcSig = AmazonApi.getSignature(trans.getUuid(), trans.getTimestamp(), siteConfig.get("AMAZON_SECRET_KEY").getValue());
        if (!trans.getSignature().equals(calcSig)) {
            response.sendError(HttpServletResponse.SC_FORBIDDEN, 
            		"Signature is incorrect. Received: " + trans.getSignature() + " ; Calculated: " + calcSig);
            return null;
        }
		
        boolean success = false;
        
        try {
            // Once the authentication succeeds, process the request
        	success = processAmazonTransaction(request, trans, siteConfig);

        	if (success) {
                // Send the SC_OK as response status, as the processing is
                // successful
                response.setStatus(HttpServletResponse.SC_OK);        		
                saveAmazonTransaction(request);
        	}
        } catch (Exception ex) {
            // error occurred during processing.
            // Send SC_INTERNAL_SERVER_ERROR as response status
            response.sendError(HttpServletResponse.SC_INTERNAL_SERVER_ERROR, ex.getMessage());
			notifyAdmin("IopnController on " + siteConfig.get("SITE_URL").getValue(), ex.toString());
			ex.printStackTrace();
            return null;
        }
		
        if (!success) {
            response.sendError(HttpServletResponse.SC_SERVICE_UNAVAILABLE);
        }
        
		return null;
    }
    
    private void saveAmazonTransaction(HttpServletRequest request) throws Exception {
    	File amazonDir = new File(getServletContext().getRealPath("/amazonCheckout"));
		if (!amazonDir.exists()) {
			amazonDir.mkdir();
		}
    	File iopnFile = new File(amazonDir, "amazon_iopn");
    	FileOutputStream fo = new FileOutputStream(iopnFile, true);
    	PrintWriter pw = new PrintWriter(fo);
		pw.write("IP Address=" + request.getRemoteAddr() + "\n");
		Enumeration en = request.getParameterNames();
		while(en.hasMoreElements()) {
		    String paramName = (String)en.nextElement();
		    String paramValue = request.getParameter(paramName);
		    pw.write(paramName + "=" + paramValue + "\n");
		}
		pw.write("--------------------------------------------\n");
		pw.close();
    	fo.close();
    }
    
    private boolean processAmazonTransaction(HttpServletRequest request, Iopn trans, Map<String, Configuration> siteConfig) throws Exception {
    	boolean success = false;
    	
    	trans.setNotificationType(request.getParameter("NotificationType"));
    	trans.setNotificationData(request.getParameter("NotificationData"));
		
		// valid NotificationTypes
		// NewOrderNotification, OrderCancelledNotification, OrderReadyToShipNotification
		Set<String> notificationTypes = new HashSet<String>();
		notificationTypes.add("NewOrderNotification");
		notificationTypes.add("OrderCancelledNotification");
		notificationTypes.add("OrderReadyToShipNotification");
		if (!notificationTypes.contains(trans.getNotificationType())) {
			throw new Exception("Unknown notification type: " + trans.getNotificationType());
		}
		
		SAXBuilder builder = new SAXBuilder(false);
		Document doc = builder.build(new StringReader(trans.getNotificationData()));
		Element root = doc.getRootElement(); 		
		
		Element processedOrder = root.getChild("ProcessedOrder", root.getNamespace());
		trans.setOrderChannel(processedOrder.getChildText("OrderChannel", root.getNamespace()));
		List<Element> processedOrderItems = processedOrder.getChild("ProcessedOrderItems", root.getNamespace()).getChildren();
		
		trans.setNotificationRefId(root.getChildText("NotificationReferenceId", root.getNamespace()));
		trans.setAmazonOrderID(processedOrder.getChildText("AmazonOrderID", root.getNamespace()));
		
		if (trans.getOrderChannel().startsWith("Amazon Payment Method")) {
			Element cartCustomData = processedOrderItems.get(0).getChild("CartCustomData", root.getNamespace());
			trans.setOrderNumber(cartCustomData.getChildText("OrderNumber", root.getNamespace()));

			// get order
			Order order = this.webJaguar.getOrder(Integer.parseInt(trans.getOrderNumber()), null);
			
			if (order == null || !order.getPaymentMethod().equalsIgnoreCase("amazon")) {
				throw new Exception("Unknown order: " + trans.getOrderNumber()); 
			}
			
			String orderNotificationType = null;
			if (order.getAmazonIopn() != null) orderNotificationType = order.getAmazonIopn().getNotificationType();
			
			if (trans.getNotificationType().equals("NewOrderNotification")) {
				if (orderNotificationType == null) {
					// get AmazonOrderItemCodes
					List<LineItem> lineItems = new ArrayList<LineItem>();
					for (Element processedOrderItem: processedOrderItems) {
						Element itemCustomData = processedOrderItem.getChild("ItemCustomData", root.getNamespace());
						lineItems.add(new LineItem(order.getOrderId(), Integer.parseInt(itemCustomData.getChildText("LineNumber", root.getNamespace())),
								processedOrderItem.getChildText("AmazonOrderItemCode", root.getNamespace())));
					}
					
					// update status
					OrderStatus orderStatus = new OrderStatus();
	    			orderStatus.setOrderId(order.getOrderId());
	    			orderStatus.setStatus("ano"); // amazon new order
	    			SiteMessage siteMessage = null;
	    			try {
	    				Integer messageId = null;
	    				// multi store
	    				List<MultiStore> multiStores = this.webJaguar.getMultiStore(order.getHost());
	    				MultiStore multiStore = null;
	    				if (multiStores.size() > 0) {
	    					multiStore = multiStores.get(0);
	    					messageId = multiStore.getMidNewOrders();
	    				} else {
		    				messageId = Integer.parseInt(siteConfig.get("SITE_MESSAGE_ID_FOR_NEW_ORDERS").getValue());		    					
	    				}
	    				siteMessage = this.webJaguar.getSiteMessageById(messageId);
	    				if (siteMessage != null) {
	    					// send email
	    					orderStatus.setSubject(siteMessage.getSubject());
	    					orderStatus.setMessage(siteMessage.getMessage());
	    					orderStatus.setHtmlMessage(siteMessage.isHtml());
	    					if (notifyCustomer(orderStatus, siteConfig, order, multiStore)) {
		    					orderStatus.setUserNotified(true);		    							
	    					}
	    				}
	    			} catch (NumberFormatException e) {
	    				// do nothing
	    			}		    		
					this.webJaguar.updateAmazonPayment(trans, orderStatus, lineItems);
					// clear shopping cart items from database
					this.webJaguar.deleteShoppingCart(order.getUserId(), order.getManufacturerName());
					
					success = true;
				} else {
					// out of synch
					throw new Exception("Message out of sequence: " + 
							trans.getOrderNumber() + ", " + trans.getAmazonOrderID() + ", " + trans.getNotificationType());					
				}
			} else if (trans.getNotificationType().equals("OrderReadyToShipNotification")) {
				if (orderNotificationType != null && orderNotificationType.equals("NewOrderNotification")) {
	    			// update status
					OrderStatus orderStatus = new OrderStatus();
	    			orderStatus.setOrderId(order.getOrderId());
	    			orderStatus.setStatus("ars"); // amazon ready to ship
					
	    			this.webJaguar.updateAmazonPayment(trans, orderStatus, null);
	    			
	    			success = true;
					// buy safe
	    			if( !siteConfig.get("BUY_SAFE_URL").getValue().equals("") ) {
	    				try {
	    					this.webJaguar.setShoppingCartCheckout( order, mailSender );
	    				} catch (Exception e) { 
	    					e.printStackTrace();
	    					OrderStatus status = new OrderStatus();
	    					status.setOrderId( order.getOrderId() );
	    					status.setStatus( "ars" ); // amazon ready to ship
	    					if( (order.getWantsBond() != null) && (order.getWantsBond() == true) ) {
	    						status.setComments("BuySafe Bond was selected but cancelled as system failed to connect Buysafe. Please check the amount charged and refund the bond cost to Customer.");
	    					} else {
	    						status.setComments("BuySafe Bond was not selected. System failed to connect Buysafe");
	    					}
	    					order.setWantsBond(false);
	    					order.setBondCost(null);
	    					order.setGrandTotal();
	    					
	    					this.webJaguar.cancelBuySafeBond(order, status);
	    				}
	    			}
	    			
				} else {
					// out of synch
					throw new Exception("Message out of sequence: " + 
							trans.getOrderNumber() + ", " + trans.getAmazonOrderID() + ", " + trans.getNotificationType());					
				}
			} else if (trans.getNotificationType().equals("OrderCancelledNotification")) {
				if (orderNotificationType != null 
						&& (orderNotificationType.equals("NewOrderNotification") || orderNotificationType.equals("OrderReadyToShipNotification"))) {
	    			// update status
					OrderStatus orderStatus = new OrderStatus();
	    			orderStatus.setOrderId(order.getOrderId());
	    			orderStatus.setStatus("x"); // cancelled
					
	    			this.webJaguar.updateAmazonPayment(trans, orderStatus, null);
					success = true;
				} else {
					// out of synch
					throw new Exception("Message out of sequence: " + 
							trans.getOrderNumber() + ", " + trans.getAmazonOrderID() + ", " + trans.getNotificationType());					
				}
			}
		}
    	
    	return success;
    }
	
	private boolean notifyCustomer(OrderStatus orderStatus, Map<String, Configuration> siteConfig, Order order, MultiStore multiStore) {

		Customer customer = this.webJaguar.getCustomerById(order.getUserId());
		
		if (!customer.isEmailNotify()) {
			// do not send email
			return false;
		}
		
		// send email
    	String contactEmail = siteConfig.get("CONTACT_EMAIL").getValue();
    	String secureUrl = this.webJaguar.getSecureUrl(siteConfig, multiStore);
		SimpleDateFormat dateFormatter = new SimpleDateFormat("MMM d, yyyy HH:mm:ss a z");

    	StringBuffer messageBuffer = new StringBuffer();
    	messageBuffer.append( dateFormatter.format(new Date()) + "\n\n" );
    	
    	// replace dyamic elements
    	try {
    		this.webJaguar.replaceDynamicElement( orderStatus, customer, null, order, secureUrl, null);
    	} catch (Exception e) {e.printStackTrace();}
    	messageBuffer.append( orderStatus.getMessage() );
    	
    	try {
	    	// construct email message
	       	MimeMessage mms = mailSender.createMimeMessage();
			MimeMessageHelper helper = new MimeMessageHelper(mms, true, "UTF-8");
			helper.setTo(customer.getUsername());
			helper.setFrom(contactEmail);
			helper.setBcc(contactEmail);
			helper.setSubject(orderStatus.getSubject());
	    	helper.setText(messageBuffer.toString(), orderStatus.isHtmlMessage());
			
			mailSender.send(mms);
		} catch (Exception ex) {
			// do nothing, customer not notified
			return false;
		} 		
		return true;
	}    
    
	private void notifyAdmin(String subject, String message) {
		// send email notification
    	String contactEmail = "developers@advancedemedia.com";
    	SimpleDateFormat dateFormatter = new SimpleDateFormat("MMM d, yyyy HH:mm:ss a z");
    	
		SimpleMailMessage msg = new SimpleMailMessage();
		msg.setTo(contactEmail);
		msg.setFrom(contactEmail);
		msg.setSubject(subject);
		msg.setText(dateFormatter.format(new Date()) + "\n\n" + message);
		try {
			mailSender.send(msg);
		} catch (MailException ex) {
			// do nothing
		}  		
	}  
}
