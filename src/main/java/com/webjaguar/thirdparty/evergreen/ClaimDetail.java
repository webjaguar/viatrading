/* Copyright 2009 Advanced E-Media Solutions
 * @author Eduardo Asprec
 * @since 12.01.2009
 */

package com.webjaguar.thirdparty.evergreen;

public class ClaimDetail {

	private Integer aeClaimID;
    private String claimSku;
    private String claimQty;
    private String claimType;
    private String qty;

	public ClaimDetail() {}
    
	public ClaimDetail(String claimSku, String qty) {
		this.claimSku = claimSku;
		this.qty = qty;
	}
	
	public Integer getAeClaimID() {
		return aeClaimID;
	}
	public void setAeClaimID(Integer aeClaimID) {
		this.aeClaimID = aeClaimID;
	}
    
	public String getClaimSku() {
		return claimSku;
	}
	public void setClaimSku(String claimSku) {
		this.claimSku = claimSku;
	}
	public String getClaimQty() {
		return claimQty;
	}
	public void setClaimQty(String claimQty) {
		this.claimQty = claimQty;
	}
	public String getClaimType() {
		return claimType;
	}
	public void setClaimType(String claimType) {
		this.claimType = claimType;
	}
	public String getQty() {
		return qty;
	}
	public void setQty(String qty) {
		this.qty = qty;
	}

}
