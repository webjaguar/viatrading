/* Copyright 2008 Advanced E-Media Solutions
 * @author Eduardo Asprec
 * @since 11.17.2008
 */

package com.webjaguar.thirdparty.evergreen;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.web.bind.ServletRequestUtils;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.multiaction.MultiActionController;

import com.webjaguar.logic.WebJaguarFacade;
import com.webjaguar.model.Customer;

public class EvergreenController extends MultiActionController {
	
	private WebJaguarFacade webJaguar;
	public void setWebJaguar(WebJaguarFacade webJaguar) { this.webJaguar = webJaguar; }
	
	private IbatisEvergreenDao evergreenDao;
	public void setEvergreenDao(IbatisEvergreenDao evergreenDao) { this.evergreenDao = evergreenDao; }
	
	EvergreenApi evergreen = new EvergreenApi();
		
    public ModelAndView listOrders(HttpServletRequest request, HttpServletResponse response) {   	
		Map<String, Object> gSiteConfig = (Map<String, Object>) request.getAttribute("gSiteConfig");
    	
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("mainCategories", this.webJaguar.getMainCategoryLinks(request,null));

		ModelAndView modelAndView = new ModelAndView("frontend/evergreen/orders", "model", map);

		if (((String) gSiteConfig.get("gACCOUNTING")).equals("EVERGREEN")) {
			Customer customer = (Customer) request.getAttribute("sessionCustomer");
	    	
			if (customer.getAccountNumber() != null && customer.getAccountNumber().length() > 0) {
				map.put("list", evergreen.GetOrderHeaderWithStatus(customer.getAccountNumber()));
				map.put("evergreenLayout", this.webJaguar.getSystemLayout("evergreen-orders", request.getHeader("host"), request));
			}			
		}
				
        return modelAndView;    	
    }

    public ModelAndView viewOrder(HttpServletRequest request, HttpServletResponse response) {   	
		Map<String, Object> gSiteConfig = (Map<String, Object>) request.getAttribute("gSiteConfig");
    	
		Map<String, Object> model = new HashMap<String, Object>();

		if (((String) gSiteConfig.get("gACCOUNTING")).equals("EVERGREEN")) {
			int order_id = ServletRequestUtils.getIntParameter(request, "id", -1);
			
			Customer customer = (Customer) request.getAttribute("sessionCustomer");
			
	    	Map<String, Object> order = null;
	    	List<Map<String, Object>> orderHeaders = evergreen.GetOrderHeaderWithStatus(customer.getAccountNumber());
	    	if (orderHeaders != null) {
		    	for (Map<String, Object> orderHeader: orderHeaders) {
		    		if (Integer.parseInt((String) orderHeader.get("ORDER_ID")) == order_id) {
		    			order = orderHeader;
		    			break;
		    		}
		    	}	    		
	    	}

	    	if (order != null) {
	    		model.put("order", order);
				// lineitems
	    		model.put("lineItems", evergreen.GetOrderDetail(order_id));	   
	    		
	    		// client
	    		model.put("client", evergreen.GetClient(customer.getAccountNumber()));
	    		
				model.put("invoiceLayout", this.webJaguar.getSystemLayout("evergreen-order", request.getHeader("host"), request));
	    	} else {
	    		model.put("message", "order.exception.notfound");
	    	}
		}
				
        return new ModelAndView("frontend/evergreen/order", model);    	
    }
    
    public ModelAndView orderFulfillmentSchedule(HttpServletRequest request, HttpServletResponse response) {   	
		Map<String, Object> gSiteConfig = (Map<String, Object>) request.getAttribute("gSiteConfig");
    	
		Map<String, Object> model = new HashMap<String, Object>();
		model.put("mainCategories", this.webJaguar.getMainCategoryLinks(request,null));

		ModelAndView modelAndView = new ModelAndView("frontend/evergreen/orderFulfillmentSchedule", "model", model);

		if (((String) gSiteConfig.get("gACCOUNTING")).equals("EVERGREEN")) {
			Customer customer = (Customer) request.getAttribute("sessionCustomer");
	    	
			if (customer.getAccountNumber() != null && customer.getAccountNumber().length() > 0) {
				model.put("list", evergreen.GetClientOpenOrderFulfillRate(customer.getAccountNumber()));
				model.put("evergreenLayout", this.webJaguar.getSystemLayout("evergreen-orderFulfillmentSchedule", request.getHeader("host"), request));
			}			
		}
				
        return modelAndView;    	
    }
    
    public ModelAndView orderFulfillmentDetails(HttpServletRequest request, HttpServletResponse response) {   	
		Map<String, Object> gSiteConfig = (Map<String, Object>) request.getAttribute("gSiteConfig");
    	
		Map<String, Object> model = new HashMap<String, Object>();
		model.put("mainCategories", this.webJaguar.getMainCategoryLinks(request,null));

		ModelAndView modelAndView = new ModelAndView("frontend/evergreen/orderFulfillmentDetails", "model", model);

		if (((String) gSiteConfig.get("gACCOUNTING")).equals("EVERGREEN")) {
			int order_id = ServletRequestUtils.getIntParameter(request, "id", -1);
			
			Customer customer = (Customer) request.getAttribute("sessionCustomer");
	    	
			if (customer.getAccountNumber() != null && customer.getAccountNumber().length() > 0) {
				
				List<Map<String, Object>> openOrders = evergreen.GetClientOpenOrderFulfillRate(customer.getAccountNumber());
				if (openOrders != null) {
			    	for (Map<String, Object> orderMap: openOrders) {
			    		if (Integer.parseInt((String) orderMap.get("EveOrderID")) == order_id) {
			    			model.put("list", evergreen.GetOrderFulfillQty(order_id));
			    			model.put("map", evergreen.GetOrderFulfillRate(order_id));
			    			break;
			    		}
			    	}					
				}

		    	model.put("evergreenLayout", this.webJaguar.getSystemLayout("evergreen-orderFulfillmentDetails", request.getHeader("host"), request));
			}			
		}
				
        return modelAndView;    	
    }

    public ModelAndView myCreditsDamagedReportsList(HttpServletRequest request, HttpServletResponse response) {   	
		Map<String, Object> gSiteConfig = (Map<String, Object>) request.getAttribute("gSiteConfig");
    	
		Map<String, Object> model = new HashMap<String, Object>();
		model.put("mainCategories", this.webJaguar.getMainCategoryLinks(request,null));

		ModelAndView modelAndView = new ModelAndView("frontend/evergreen/myCreditsDamagedReportsList", "model", model);

		if (((String) gSiteConfig.get("gACCOUNTING")).equals("EVERGREEN")) {
			Customer customer = (Customer) request.getAttribute("sessionCustomer");
	    	
			if (customer.getAccountNumber() != null && customer.getAccountNumber().length() > 0) {
				
				Map<String, Map<String, Object>> orders = new HashMap<String, Map<String, Object>>();
				List<Map<String, Object>> orderHeaders = evergreen.GetOrderHeaderWithStatus(customer.getAccountNumber());
    			if (orderHeaders != null) {
    				for (Map<String, Object> order: orderHeaders) {
    					orders.put((String) order.get("ORDER_ID"), order);
    				}	
    			}
				
    			List<Map<String, Object>> list = evergreen.GetClaimHeader(customer.getAccountNumber());
    			if (list != null) {
        			for (Map<String, Object> claim: list) {
        				claim.put("order", orders.get(claim.get("ORDERID")));
        			}    				
    			}
				
				model.put("list", list);
				model.put("evergreenLayout", this.webJaguar.getSystemLayout("evergreen-myCreditsDamagedReportsList", request.getHeader("host"), request));
			}			
		}
				
        return modelAndView;    	
    }
    
    public ModelAndView myCreditsDamagedReport(HttpServletRequest request, HttpServletResponse response) {   	
		Map<String, Object> gSiteConfig = (Map<String, Object>) request.getAttribute("gSiteConfig");
    	
		Map<String, Object> model = new HashMap<String, Object>();
		model.put("mainCategories", this.webJaguar.getMainCategoryLinks(request,null));

		ModelAndView modelAndView = new ModelAndView("frontend/evergreen/myCreditsDamagedReport2", "model", model);
		if (ServletRequestUtils.getStringParameter(request, "view", "2").equalsIgnoreCase("1")) {
			modelAndView = new ModelAndView("frontend/evergreen/myCreditsDamagedReport", "model", model);
		}

		if (((String) gSiteConfig.get("gACCOUNTING")).equals("EVERGREEN")) {
			int EveClaimID = ServletRequestUtils.getIntParameter(request, "id", -1);
			
			Customer customer = (Customer) request.getAttribute("sessionCustomer");
	    	
			if (customer.getAccountNumber() != null && customer.getAccountNumber().length() > 0) {
				
				Map<String, Object> claim = null;
				List<Map<String, Object>> claimHeaders = evergreen.GetClaimHeader(customer.getAccountNumber());
				if (claimHeaders != null) {
			    	for (Map<String, Object> map: claimHeaders) {
			    		if (map.get("EVECLAIMID") != null && Integer.parseInt((String) map.get("EVECLAIMID")) == EveClaimID) {
			    			
			    			Map<String, Map<String, Object>> details = new HashMap<String, Map<String, Object>>();
			    			for (Map<String, Object> detail: evergreen.GetClaimDetail(EveClaimID)) {
			    				details.put(((String) detail.get("ClaimSku")).toUpperCase(), detail);
			    			}
			    			model.put("details", details);
			    			
			    			claim = map;
			    			model.put("claim", claim);
			    			break;
			    		}
			    	}					
				}
		    	
		    	if (claim != null) {
		    		int order_id = Integer.parseInt((String) claim.get("ORDERID"));
			    	for (Map<String, Object> map: evergreen.GetOrderHeaderWithStatus(customer.getAccountNumber())) {
			    		if (Integer.parseInt((String) map.get("ORDER_ID")) ==  order_id) {
			    			model.put("order", map);
			    			
							// lineitems
				    		model.put("lineItems", evergreen.GetOrderDetail(order_id));	   
				    		
				    		// client
				    		model.put("client", evergreen.GetClient(customer.getAccountNumber()));
			    			break;
			    		}
			    	}		    		
		    	}

		    	model.put("evergreenLayout", this.webJaguar.getSystemLayout("evergreen-myCreditsDamagedReport", request.getHeader("host"), request));
			}			
		}
				
        return modelAndView;    	
    }
    
    public ModelAndView viewClaim(HttpServletRequest request, HttpServletResponse response) {   	
		Map<String, Object> gSiteConfig = (Map<String, Object>) request.getAttribute("gSiteConfig");
    	
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("mainCategories", this.webJaguar.getMainCategoryLinks(request,null));

		ModelAndView modelAndView = new ModelAndView("frontend/evergreen/claim", "model", map);

		if (((String) gSiteConfig.get("gACCOUNTING")).equals("EVERGREEN")) {
			int aeClaimID = ServletRequestUtils.getIntParameter(request, "id", -1);
			
			Customer customer = (Customer) request.getAttribute("sessionCustomer");
	    	
			if (customer.getAccountNumber() != null && customer.getAccountNumber().length() > 0) {
				ClaimHeader claimHeader = evergreenDao.getEvergreenClaimHeader(aeClaimID);
				if (claimHeader != null && customer.getAccountNumber().equals(claimHeader.getClientID())) {
					map.put("claimHeader", claimHeader);
					map.put("claimDetails", evergreenDao.getEvergreenClaimDetail(aeClaimID));
				}
			}			
		}
				
        return modelAndView;    	
    }
    
}
