/* Copyright 2009 Advanced E-Media Solutions
 * @author Eduardo Asprec
 * @since 12.02.2009
 */

package com.webjaguar.thirdparty.evergreen;

import java.util.List;

import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.orm.ibatis.support.SqlMapClientDaoSupport;

public class IbatisEvergreenDao extends SqlMapClientDaoSupport {

	public void insertEvergreenClaimHeader(ClaimHeader claimHeader) {
		getSqlMapClientTemplate().insert("insertEvergreenClaimHeader", claimHeader);
	}
	
	public void insertEvergreenClaimDetails(List<ClaimDetail> claimDetails, Integer aeClaimID) {
		for (ClaimDetail claimDetail: claimDetails) {
			claimDetail.setAeClaimID(aeClaimID);
			getSqlMapClientTemplate().insert("insertEvergreenClaimDetail", claimDetail);
		}
	}
	
	public ClaimHeader getEvergreenClaimHeader(int aeClaimID) {
		return (ClaimHeader) getSqlMapClientTemplate().queryForObject("getEvergreenClaimHeader", aeClaimID);
	}
	
	public List<ClaimDetail> getEvergreenClaimDetail(int aeClaimID) {
		return getSqlMapClientTemplate().queryForList("getEvergreenClaimDetail", aeClaimID);
	}
	
	public void updateEvergreenClaimHeaderSuc(int aeClaimID, String suc) {
		JdbcTemplate jdbcTemplate = new JdbcTemplate(getSqlMapClientTemplate().getDataSource());
		String query = "UPDATE evergreen_claim_header SET evergreen_suc = ? where ae_claim_id = ?";
		Object[] args = {suc, aeClaimID};
		jdbcTemplate.update(query, args);		
	}
}
