/* Copyright 2009 Advanced E-Media Solutions
 * @author Eduardo Asprec
 * @since 12.01.2009
 */

package com.webjaguar.thirdparty.evergreen;

import java.util.ArrayList;
import java.util.List;

public class ClaimForm {

	private ClaimHeader claimHeader;
	private List<ClaimDetail> claimDetails;
	private List<String> orders;
	
	public ClaimForm() {
		this.claimHeader = new ClaimHeader();
		this.claimDetails = new ArrayList<ClaimDetail>();
	}

	public ClaimHeader getClaimHeader() {
		return claimHeader;
	}

	public void setClaimHeader(ClaimHeader claimHeader) {
		this.claimHeader = claimHeader;
	}

	public List<ClaimDetail> getClaimDetails() {
		return claimDetails;
	}

	public void setClaimDetails(List<ClaimDetail> claimDetails) {
		this.claimDetails = claimDetails;
	}

	public List<String> getOrders() {
		return orders;
	}

	public void setOrders(List<String> orders) {
		this.orders = orders;
	}

}
