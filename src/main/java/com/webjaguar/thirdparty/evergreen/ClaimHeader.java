/* Copyright 2009 Advanced E-Media Solutions
 * @author Eduardo Asprec
 * @since 12.01.2009
 */

package com.webjaguar.thirdparty.evergreen;

import java.sql.Timestamp;

public class ClaimHeader {

    private Integer aeClaimID;
    private String clientID;
    private String orderID;
    private String contactName;
    private String contactPhone;
    private String claimDesc;
    private Timestamp created;
    
	public Integer getAeClaimID() {
		return aeClaimID;
	}
	public void setAeClaimID(Integer aeClaimID) {
		this.aeClaimID = aeClaimID;
	}
	public String getClientID() {
		return clientID;
	}
	public void setClientID(String clientID) {
		this.clientID = clientID;
	}
	public String getOrderID() {
		return orderID;
	}
	public void setOrderID(String orderID) {
		this.orderID = orderID;
	}
	public String getContactName() {
		return contactName;
	}
	public void setContactName(String contactName) {
		this.contactName = contactName;
	}
	public String getContactPhone() {
		return contactPhone;
	}
	public void setContactPhone(String contactPhone) {
		this.contactPhone = contactPhone;
	}
	public String getClaimDesc() {
		return claimDesc;
	}
	public void setClaimDesc(String claimDesc) {
		this.claimDesc = claimDesc;
	}
	public Timestamp getCreated() {
		return created;
	}
	public void setCreated(Timestamp created) {
		this.created = created;
	}

}
