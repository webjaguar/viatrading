/* Copyright 2009 Advanced E-Media Solutions
 * @author Eduardo Asprec
 * @since 12.01.2009
 */

package com.webjaguar.thirdparty.evergreen;

import java.io.File;
import java.io.FileInputStream;
import java.text.SimpleDateFormat;
import java.util.*;

import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.mail.javamail.JavaMailSenderImpl;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.validation.BindException;
import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.web.bind.ServletRequestUtils;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.AbstractWizardFormController;

import com.webjaguar.logic.WebJaguarFacade;
import com.webjaguar.model.Configuration;
import com.webjaguar.model.Customer;
import com.webjaguar.model.Layout;
import com.webjaguar.model.SalesRep;

public class ClaimWizardController extends AbstractWizardFormController {
	
	private WebJaguarFacade webJaguar; 
	public void setWebJaguar(WebJaguarFacade webJaguar) { this.webJaguar = webJaguar; }
	
	private IbatisEvergreenDao evergreenDao;
	public void setEvergreenDao(IbatisEvergreenDao evergreenDao) { this.evergreenDao = evergreenDao; }

	private JavaMailSenderImpl mailSender;
	public void setMailSender(JavaMailSenderImpl mailSender) { this.mailSender = mailSender; }
	
	private Map<String, Configuration> siteConfig;
	
	EvergreenApi evergreen = new EvergreenApi();
	
	public ClaimWizardController() {
		setCommandName("claimForm");
		setCommandClass(ClaimForm.class);
		setPages(new String[] {"frontend/evergreen/claimForm1", "frontend/evergreen/claimForm2"});
	}	
	
	public ModelAndView processFinish(HttpServletRequest request, HttpServletResponse response,
			Object command, BindException errors)
        throws Exception {	

		Map<String, Object> model = new HashMap<String, Object>();
		model.put("mainCategories", this.webJaguar.getMainCategoryLinks(request,null));
		
		ClaimForm claimForm = (ClaimForm) command;
		
		List<ClaimDetail> claimDetails = new ArrayList<ClaimDetail>();
		Map<ClaimDetail, String> claimDetailErrors = new HashMap<ClaimDetail, String>();
		for (ClaimDetail claimDetail: claimForm.getClaimDetails()) {
			claimDetail.setClaimQty(ServletRequestUtils.getStringParameter(request, "claimQty_" + claimDetail.getClaimSku(), "").trim());
			claimDetail.setClaimType(ServletRequestUtils.getStringParameter(request, "claimType_" + claimDetail.getClaimSku(), ""));
			if ((claimDetail.getClaimQty().length() > 0) ^ (claimDetail.getClaimType().length() > 0)) {
				if (claimDetail.getClaimType().length() > 0) {
					//  missing quantity
					claimDetailErrors.put(claimDetail, "claimQty");
				} else {
					//  missing type
					claimDetailErrors.put(claimDetail, "claimType");					
				}
			} else if (claimDetail.getClaimQty().length() > 0) {
				try {
					Integer qty = Integer.parseInt(claimDetail.getClaimQty());
					if (qty > Integer.parseInt(claimDetail.getQty()) || qty <= 0) {
						// over shipped quantity
						claimDetailErrors.put(claimDetail, "claimQty");
					} else {
						claimDetails.add(claimDetail);
					}
				} catch (NumberFormatException e) {
					claimDetailErrors.put(claimDetail, "claimQty");
				}
			}
		}
		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "claimHeader.contactName", "form.required");
		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "claimHeader.contactPhone", "form.required");
		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "claimHeader.claimDesc", "form.required");

		if (errors.hasErrors() || !claimDetailErrors.isEmpty()) {
			request.setAttribute("claimDetailErrors", claimDetailErrors);
			return showPage(request, errors, 1);
		} else if (claimDetails.isEmpty()) {
			request.setAttribute("emptyClaim", true);
			return showPage(request, errors, 1);			
		}
		
		evergreenDao.insertEvergreenClaimHeader(claimForm.getClaimHeader());
		evergreenDao.insertEvergreenClaimDetails(claimDetails, claimForm.getClaimHeader().getAeClaimID());

		try {
			for (ClaimDetail claimDetail: claimDetails) {
				evergreen.SetClaimDetail(claimDetail, request.getParameter("debug") != null);	
			}
			evergreenDao.updateEvergreenClaimHeaderSuc(claimForm.getClaimHeader().getAeClaimID(),
					evergreen.SetClaimHeader(claimForm.getClaimHeader(), request.getParameter("debug") != null));
		} catch (Exception e) {
			request.setAttribute("webServiceError", true);
			if (request.getParameter("debug") != null) {
				e.printStackTrace();				
			}
			return showPage(request, errors, 1);
		}
		
		File attachment = null;
		try {
			File claimFolder = new File(getServletContext().getRealPath("/assets/claim_folder/"));
			Properties prop = new Properties();
			try {
				prop.load( new FileInputStream(getServletContext().getRealPath("WEB-INF/properties/site.properties")));
				if (prop.get("site.root") != null) {
					claimFolder = new File((String) prop.get("site.root"), "assets/claim_folder");
				}
			} catch (Exception e) {}   	
			if (!claimFolder.exists()) {
				claimFolder.mkdir();
			}
			
			MultipartHttpServletRequest multipartRequest = (MultipartHttpServletRequest) request;
    		MultipartFile file = multipartRequest.getFile("file");
    		if (file != null && !file.isEmpty() && (
    						   file.getContentType().equalsIgnoreCase("image/gif") 
    						|| file.getContentType().equalsIgnoreCase("image/jpeg")
    						|| file.getContentType().equalsIgnoreCase("image/pjpeg"))) {
    			attachment = new File(claimFolder, claimForm.getClaimHeader().getAeClaimID().toString() + ".jpg");
				file.transferTo(attachment);
    		}
		} catch (Exception e) {
			// do nothing
		}
		
		// send email
		sendEmail(claimForm.getClaimHeader(), claimDetails, request, attachment);
		
		model.put("claimHeader", claimForm.getClaimHeader());
		model.put("claimDetails", claimDetails);
		
		return new ModelAndView("frontend/evergreen/claim", "model", model);   	
    }
			
	protected Map referenceData(HttpServletRequest request, Object command, Errors errors, int page) 
			throws Exception {
		
       	Map<String, Object> map = new HashMap<String, Object>();

       	Map<String, Object> model = new HashMap<String, Object>();
       	model.put("mainCategories", this.webJaguar.getMainCategoryLinks(request,null));
 
	    map.put("model", model);  
	    
		Layout layout = (Layout) request.getAttribute("layout");
		if (siteConfig.get("LEFTBAR_HIDE_ON_SYSTEM_PAGES").getValue().equals("true")) {
			layout.setLeftBarTopHtml("");
			layout.setLeftBarBottomHtml("");
			layout.setHideLeftBar(true);
		} 
		
	    return map;
    }  
	
    protected void postProcessPage(HttpServletRequest request, Object command, Errors errors, int page) throws Exception {
    	
    	ClaimForm claimForm = (ClaimForm) command;
    	
		switch (page) {
			case 0:
				if (!errors.hasErrors()) {
					// lineitems
					claimForm.getClaimDetails().clear();
					for (Map<String, Object> detail: evergreen.GetOrderDetail(Integer.parseInt(claimForm.getClaimHeader().getOrderID()))) {
						claimForm.getClaimDetails().add(new ClaimDetail((String) detail.get("ITEM_ID"), (String) detail.get("QTY")));
					}					
				}
				break;
		}
    }

    protected void validatePage(Object command, Errors errors, int page) {
    	ClaimForm claimForm = (ClaimForm) command;
		
		switch (page) {
			case 0:
				if (claimForm.getClaimHeader().getOrderID().equals("Select Recent Order")) {
					errors.rejectValue("claimHeader.orderID", "form.required");
				}
				break;
		}
    }
	
	protected Object formBackingObject(HttpServletRequest request) throws Exception {
		siteConfig = (Map<String, Configuration>) request.getAttribute("siteConfig");

		Customer customer = (Customer) request.getAttribute("sessionCustomer");
		
    	ClaimForm claimForm = new ClaimForm();
    	claimForm.getClaimHeader().setClientID(customer.getAccountNumber());
		
 		// get orders
 		List<String> orders = new ArrayList<String>();
		orders.add("Select Recent Order");
    	List<Map<String, Object>> orderHeaders = evergreen.GetOrderHeaderWithStatus(claimForm.getClaimHeader().getClientID());
    	if (orderHeaders != null) {
 
    		// three months from now
    		Calendar cutoff = new GregorianCalendar();
    		cutoff.add(Calendar.MONTH, -3); 
    		cutoff.add(Calendar.DAY_OF_MONTH, -1);
    		
	    	for (Map<String, Object> orderHeader: orderHeaders) {	
	    		if (orderHeader.get("REL_D_DATE") != null) {
	    			Calendar shipDate = new GregorianCalendar();
	    			shipDate.setTime((Date) orderHeader.get("REL_D_DATE"));
	    			if (shipDate.after(cutoff)) {
			    		orders.add((String) orderHeader.get("ORDER_ID"));
	    			}
	    		}
	    	}
    	}
    	claimForm.setOrders(orders);
    	
		return claimForm;
	}
	
	private void sendEmail(ClaimHeader claimHeader, List<ClaimDetail> claimDetails, HttpServletRequest request, File attachment) {
		Customer customer = (Customer) request.getAttribute("sessionCustomer");
		SalesRep salesRep = this.webJaguar.getSalesRepById(customer.getSalesRepId());	
		
		SimpleDateFormat dateFormatter = new SimpleDateFormat("MMM d, yyyy HH:mm:ss a z");

		StringBuffer message = new StringBuffer();
		message.append(dateFormatter.format(new Date()) + "\n\n");
		
		try {
			// construct email message
	       	MimeMessage mms = mailSender.createMimeMessage();
			MimeMessageHelper helper = new MimeMessageHelper(mms, true, "UTF-8");
			helper.setFrom(new InternetAddress("customersupport@myevergreen.com"));
			helper.addTo("customersupport@myevergreen.com");
			if (salesRep != null && salesRep.getEmail() != null && salesRep.getEmail().length() > 0) {
				try {
					helper.addTo(new InternetAddress(salesRep.getEmail()));		
				} catch (Exception e) {
					// do nothing
				}
			}
			helper.addCc(customer.getUsername());
			//helper.addBcc("developers@advancedemedia.com");
			
			helper.setSubject("Claim form submission for order " + claimHeader.getOrderID());
			
			message.append("Hello");
			if (salesRep != null) {
				message.append(" " + salesRep.getName());
			}
			message.append(",\n\n");
			message.append("Customer " + customer.getAddress().getCompany() + " with account number " + customer.getAccountNumber());
			message.append(" submitted a claim form for order " + claimHeader.getOrderID() + "\n\n");	
			message.append("Web Claim #: " + claimHeader.getAeClaimID() + "\n");
			message.append("Contact Name: " + claimHeader.getContactName() + "\n");
			message.append("Contact Phone: " + claimHeader.getContactPhone() + "\n");
			
			// details
			message.append("\nDamaged SKU's:\n");
			for (ClaimDetail claimDetail: claimDetails) {
				message.append("\t" + claimDetail.getClaimSku() + " -- ");
				message.append(claimDetail.getClaimQty() + " -- ");
				message.append(claimDetail.getClaimType() + "\n");
			}
			
			if (claimHeader.getClaimDesc().trim().length() > 0) {
				message.append("\nIssue:\n" + claimHeader.getClaimDesc());		
			}

			message.append("\n\n");
			message.append("Regards,\n"
					+ "Web Customer Service\n"
					+ "Evergreen Enterprises, Inc.\n" 
					+ "customersupport@myevergreen.com\n\n");	
			helper.setText(message.toString());
			
			// atachment
			if (attachment != null && attachment.exists()) {
				helper.addAttachment(attachment.getName(), attachment);
			}
 			
			mailSender.send(mms);
		} catch (Exception ex) {
			// do nothing
			if (request.getParameter("debug") != null) {
				ex.printStackTrace();
			}
		}    					
	}
	
}
