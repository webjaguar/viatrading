/* Copyright 2008 Advanced E-Media Solutions
 * @author Eduardo Asprec
 * @since 11.17.2008
 */

package com.webjaguar.thirdparty.evergreen;

import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;
import java.text.SimpleDateFormat;
import java.util.*;

import org.jdom.Document;
import org.jdom.Element;
import org.jdom.Namespace;
import org.jdom.input.SAXBuilder;

import com.webjaguar.model.Address;
import com.webjaguar.model.Customer;
import com.webjaguar.model.LineItem;
import com.webjaguar.model.Order;

public class EvergreenApi {	
	
	public List<Map<String, Object>> GetOrderHeader(String whole_s_id) {
		List<Map<String, Object>> orders = new ArrayList<Map<String, Object>>();
		
		try {
			URL url = new URL("http://www.myevergreen.com/ae/Service.asmx/GetOrderHeader");
			HttpURLConnection connection = (HttpURLConnection) url.openConnection();

			// Setup HTTP POST parameters
			connection.setDoOutput(true);
			connection.setDoInput(true);
			connection.setUseCaches(false);

			// POST data
			StringBuffer request = new StringBuffer("whole_s_id=" + whole_s_id);
			
			OutputStream oStream = connection.getOutputStream();
			oStream.write(request.toString().getBytes());
			oStream.close();
		
			orders = getResults(connection);

		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return orders;
	}	
	
	public List<Map<String, Object>> GetOrderDetail(int order_id) {
		List<Map<String, Object>> lineItems = new ArrayList<Map<String, Object>>();
		
		try {
			URL url = new URL("http://www.myevergreen.com/ae/Service.asmx/GetOrderDetail");
			HttpURLConnection connection = (HttpURLConnection) url.openConnection();

			// Setup HTTP POST parameters
			connection.setDoOutput(true);
			connection.setDoInput(true);
			connection.setUseCaches(false);
			
			// POST data
			StringBuffer request = new StringBuffer("order_id=" + order_id);
			
			OutputStream oStream = connection.getOutputStream();
			oStream.write(request.toString().getBytes());
			oStream.close();
		
			lineItems = getResults(connection);

		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return lineItems;
	}	
	
	public Map<String, Object> GetClient(String whole_s_id) {
		Map<String, Object> client = new HashMap<String, Object>();
		
		try {
			URL url = new URL("http://www.myevergreen.com/ae/Service.asmx/GetClient");
			HttpURLConnection connection = (HttpURLConnection) url.openConnection();

			// Setup HTTP POST parameters
			connection.setDoOutput(true);
			connection.setDoInput(true);
			connection.setUseCaches(false);
			
			// POST data
			StringBuffer request = new StringBuffer("whole_s_id=" + whole_s_id);
			
			OutputStream oStream = connection.getOutputStream();
			oStream.write(request.toString().getBytes());
			oStream.close();
		
			client = getResults(connection).get(0);

		} catch (Exception e) {
			return null;
		}
		
		return client;
	}	
	
	public String GetInvStatus(String item_id) {
	
		try {
			URL url = new URL("http://www.myevergreen.com/ae/Service.asmx/GetInvStatus");
			HttpURLConnection connection = (HttpURLConnection) url.openConnection();

			// Setup HTTP POST parameters
			connection.setDoOutput(true);
			connection.setDoInput(true);
			connection.setUseCaches(false);

			// POST data
			StringBuffer request = new StringBuffer("item_id=" + item_id);
			
			OutputStream oStream = connection.getOutputStream();
			oStream.write(request.toString().getBytes());
			oStream.close();
		
			return ((String) getResults(connection).get(0).get("INV"));

		} catch (Exception e) {
			return null;
		}
	}	
	
	private List<Map<String, Object>> getResults(HttpURLConnection connection) throws Exception {
		
		SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd");
		
		SAXBuilder builder = new SAXBuilder(false);

		Document doc = builder.build(connection.getInputStream());
		
		List<Map<String, Object>> results = new ArrayList<Map<String, Object>>();
		
		List<Element> elements = doc.getRootElement().getChild("schema", Namespace.getNamespace("xs", "http://www.w3.org/2001/XMLSchema"))
		.getChild("element", Namespace.getNamespace("xs", "http://www.w3.org/2001/XMLSchema"))
		.getChild("complexType", Namespace.getNamespace("xs", "http://www.w3.org/2001/XMLSchema"))
		.getChild("choice", Namespace.getNamespace("xs", "http://www.w3.org/2001/XMLSchema"))
		.getChild("element", Namespace.getNamespace("xs", "http://www.w3.org/2001/XMLSchema"))
		.getChild("complexType", Namespace.getNamespace("xs", "http://www.w3.org/2001/XMLSchema"))
		.getChild("sequence", Namespace.getNamespace("xs", "http://www.w3.org/2001/XMLSchema"))
		.getChildren();
		
		Element dataSet = doc.getRootElement().getChild("diffgram", Namespace.getNamespace("diffgram", "urn:schemas-microsoft-com:xml-diffgram-v1")).getChild("NewDataSet");
		
		Iterator<Element> children = dataSet.getChildren().iterator();
		while (children.hasNext()) {
			Element child = (Element) children.next();
			Map<String, Object> map = new HashMap<String, Object>();
			for (Element element: elements) {
				String name = element.getAttributeValue("name");
				if (child.getChild(name) != null) {
					if (element.getAttributeValue("type").equals("xs:dateTime")) {
						map.put(name, df.parse(child.getChild(name).getText()));
					} else {
						map.put(name, child.getChild(name).getText());							
					}
				}
			}
			results.add(map);
		}
		
		return results;
	}
	
    public String SetOrderHeader(Order order, Customer customer, boolean printUrl) throws Exception {
    	
		URL url = new URL("http://www.myevergreen.com/rti/service.asmx/SetOrderHeaderWithNotes");
		HttpURLConnection connection = (HttpURLConnection) url.openConnection();

		// Setup HTTP POST parameters
		connection.setDoOutput(true);
		connection.setDoInput(true);
		connection.setUseCaches(false);
		
		String CURRENCYCODE = "";
		if (customer.getPriceTable() == 6) {
			// canada currency code hard coded to PriceTable 6
			CURRENCYCODE = "CAD";
		}

		// POST data
		StringBuffer request = new StringBuffer();
		request.append("AE_ORDER_ID=" + order.getOrderId().toString());
		request.append("&AE_CLIENT_ID=" + customer.getId().toString());
		request.append("&ORDER_ID=0");
		int accountNumber = 0;
		try {
			accountNumber = Integer.parseInt(customer.getAccountNumber());
		} catch (NumberFormatException e) {
			// do nothing
		}
		request.append("&WHOLE_S_ID=" + accountNumber);		
		if (order.getBilling().getCompany() != null) {
			request.append("&CLIENTNAME=" + URLEncoder.encode(order.getBilling().getCompany(), "UTF-8"));
		} else {
			request.append("&CLIENTNAME=");
		}
		request.append("&PAY_METHOD=" + URLEncoder.encode(order.getPaymentMethod(), "UTF-8"));		
		request.append("&PONUM=" + URLEncoder.encode(order.getPurchaseOrder(), "UTF-8"));		
		request.append("&SHIP_DATE=01/01/1900");		
		request.append("&CANCEL_DATE=01/01/1900");		
		request.append("&DEPT=");		
		request.append("&FOB_CODE=");		
		request.append("&SHIP_VIA=" + URLEncoder.encode(order.getShippingMethod(), "UTF-8"));		
		request.append("&ORDER_SOURCE=");		
		request.append("&STATUS=");		
		if (order.getShipping().getCompany() != null) {
			request.append("&S_NAME=" + URLEncoder.encode(order.getShipping().getCompany(), "UTF-8"));
		} else {
			request.append("&S_NAME=");
		}	
		request.append("&S_STREET1=" + URLEncoder.encode(order.getShipping().getAddr1(), "UTF-8"));	
		if (order.getShipping().getAddr2() != null) {
			request.append("&S_STREET2=" + URLEncoder.encode(order.getShipping().getAddr2(), "UTF-8"));					
		} else {
			request.append("&S_STREET2=");		
		}
		request.append("&S_CITY=" + URLEncoder.encode(order.getShipping().getCity(), "UTF-8"));		
		if (order.getShipping().getStateProvince() != null) {
			request.append("&S_STATE=" + URLEncoder.encode(order.getShipping().getStateProvince(), "UTF-8"));
		} else {
			request.append("&S_STATE=");
		}
		request.append("&S_COUNTRY=" + URLEncoder.encode(order.getShipping().getCountry(), "UTF-8"));	
		request.append("&CURRENCYCODE=" + URLEncoder.encode(CURRENCYCODE, "UTF-8"));
		request.append("&S_ZIP=" + URLEncoder.encode(order.getShipping().getZip(), "UTF-8"));
		if (order.getCreditCard() != null && order.getCreditCard().getNumber() != null) {
			request.append("&CHARGE_NUM=" + URLEncoder.encode(order.getCreditCard().getNumber(), "UTF-8"));		
			request.append("&CHARGE_EXP=" + URLEncoder.encode(order.getCreditCard().getExpireMonth()
						+ "/" + order.getCreditCard().getExpireYear(), "UTF-8"));				
			request.append("&CHARGE_CVV=" + URLEncoder.encode(order.getCreditCard().getCardCode(), "UTF-8"));				
		} else {
			request.append("&CHARGE_NUM=");		
			request.append("&CHARGE_EXP=01/01/1900");			
			request.append("&CHARGE_CVV=");			
		}
		request.append("&CHARGE_HDR=");
		// promo code
		String promoCode = "";
		if (order.getPromo() != null && order.getPromo().getTitle() != null) {
			promoCode = order.getPromo().getTitle();
		}
		if (order.getLineItemPromos() != null) {
			for (String promoCodeProduct: order.getLineItemPromos().keySet()) {
				if (promoCode.length() > 0) promoCode += ",";
				promoCode += promoCodeProduct;
			}			
		}
		request.append("&PROMO_CODE=" + URLEncoder.encode(promoCode, "UTF-8"));		
		request.append("&NOTES=" + URLEncoder.encode(order.getInvoiceNote(), "UTF-8"));
		request.append("&SOURCE=");	
		
		OutputStream oStream = connection.getOutputStream();
		oStream.write(request.toString().getBytes());
		oStream.close();	
    	
		if (printUrl) {
			//System.out.println("http://www.myevergreen.com/rti/service.asmx/SetOrderHeaderWithNotes?" + request.toString());
		}
		
    	return ((String) getResults(connection).get(0).get("SUC"));	
    }	
    
    public String SetOrderDetail(LineItem lineItem, boolean printUrl) throws Exception {
    	
		URL url = new URL("http://www.myevergreen.com/ae/Service.asmx/SetOrderDetail");
		HttpURLConnection connection = (HttpURLConnection) url.openConnection();

		// Setup HTTP POST parameters
		connection.setDoOutput(true);
		connection.setDoInput(true);
		connection.setUseCaches(false);

		// POST data
		StringBuffer request = new StringBuffer();
		request.append("AE_ORDER_ID=" + URLEncoder.encode(lineItem.getOrderId().toString(), "UTF-8"));
		request.append("&ORDER_ID=0");
		request.append("&ITEM_ID=" + URLEncoder.encode(lineItem.getProduct().getSku(), "UTF-8"));		
		request.append("&ITEM_NAME=" + URLEncoder.encode(lineItem.getProduct().getName(), "UTF-8"));
		request.append("&QTY=" + URLEncoder.encode(String.valueOf(lineItem.getTotalQty()), "UTF-8"));		
		request.append("&U_PRICE=" + URLEncoder.encode(lineItem.getUnitPrice().toString(), "UTF-8"));		
		request.append("&DISCOUNTPERCENTAGE=0");		
		request.append("&LINETOTAL_SHIPPED=0");			
		request.append("&CUSTOM=");
		
		OutputStream oStream = connection.getOutputStream();
		oStream.write(request.toString().getBytes());
		oStream.close();
		
		if (printUrl) {
			//System.out.println("http://www.myevergreen.com/ae/Service.asmx/SetOrderDetail?" + request.toString());
		}
		
		return ((String) getResults(connection).get(0).get("SUC"));
    }
    // need change
    public String SetClient(Customer customer, Address shipping) throws Exception {
    	
		URL url = new URL("http://www.myevergreen.com/ae/Service.asmx/SetClient");
		HttpURLConnection connection = (HttpURLConnection) url.openConnection();

		// Setup HTTP POST parameters
		connection.setDoOutput(true);
		connection.setDoInput(true);
		connection.setUseCaches(false);

		String CURRENCYCODE = "";
		if (customer.getPriceTable() == 6) {
			// canada currency code hard coded to PriceTable 6
			CURRENCYCODE = "CAD";
		}
		
		// POST data
		StringBuffer request = new StringBuffer();
		request.append("AE_CLIENT_ID=" + customer.getId().toString());
		request.append("&WHOLE_S_ID=0");			
		request.append("&CLIENT_POC=" + URLEncoder.encode(customer.getAddress().getFirstName() + " " + customer.getAddress().getLastName(), "UTF-8"));		
		request.append("&BUYER=");		
		request.append("&TELEPHONE=" + URLEncoder.encode(customer.getAddress().getPhone(), "UTF-8"));		
		if (customer.getAddress().getFax() != null) {
			request.append("&FAX_NUMBER=" + URLEncoder.encode(customer.getAddress().getFax(), "UTF-8"));
		} else {
			request.append("&FAX_NUMBER=");
		}
		request.append("&EMAIL_ADDR=" + URLEncoder.encode(customer.getUsername(), "UTF-8"));		
		if (customer.getAddress().getCompany() != null) {
			request.append("&CLIENTNAME=" + URLEncoder.encode(customer.getAddress().getCompany(), "UTF-8"));
		} else {
			request.append("&CLIENTNAME=");
		}
		request.append("&B_STREET1=" + URLEncoder.encode(customer.getAddress().getAddr1(), "UTF-8"));
		if (customer.getAddress().getAddr2() != null) {
			request.append("&B_STREET2=" + URLEncoder.encode(customer.getAddress().getAddr2(), "UTF-8"));
		} else {
			request.append("&B_STREET2=");
		}
		request.append("&B_CITY=" + URLEncoder.encode(customer.getAddress().getCity(), "UTF-8"));
		if (customer.getAddress().getStateProvince() != null) {
			request.append("&B_STATE=" + URLEncoder.encode(customer.getAddress().getStateProvince(), "UTF-8"));
		} else {
			request.append("&B_STATE=");
		}
		request.append("&B_ZIP=" + URLEncoder.encode(customer.getAddress().getZip(), "UTF-8"));
		request.append("&B_COUNTRY=" + URLEncoder.encode(customer.getAddress().getCountry(), "UTF-8"));
		if (shipping.getCompany() != null) {
			request.append("&S_NAME=" + URLEncoder.encode(shipping.getCompany(), "UTF-8"));
		} else {
			request.append("&S_NAME=");
		}
		request.append("&S_STREET1=" + URLEncoder.encode(shipping.getAddr1(), "UTF-8"));
		if (shipping.getAddr2() != null) {
			request.append("&S_STREET2=" + URLEncoder.encode(shipping.getAddr2(), "UTF-8"));
		} else {
			request.append("&S_STREET2=");		
		}
		request.append("&S_CITY=" + URLEncoder.encode(shipping.getCity(), "UTF-8"));
		if (shipping.getStateProvince() != null) {
			request.append("&S_STATE=" + URLEncoder.encode(shipping.getStateProvince(), "UTF-8"));
		} else {
			request.append("&S_STATE=");		
		}	
		request.append("&S_ZIP=" + URLEncoder.encode(shipping.getZip(), "UTF-8"));
		request.append("&S_COUNTRY=" + URLEncoder.encode(shipping.getCountry(), "UTF-8"));
		request.append("&CURRENCYCODE=" + URLEncoder.encode(CURRENCYCODE, "UTF-8"));
		request.append("&CHANNEL_CODE=");
		request.append("&STORE_TYPE=");
		request.append("&REP_ID1=0");
		request.append("&TERRITORY_CODE=");
		request.append("&PAY_METHOD=");
		request.append("&ORD_NOTES=");
		request.append("&PAY_METHOD_CODE=0");
		request.append("&CHARGE_NUM=");
		request.append("&CHARGE_EXP=01/01/1900");
		request.append("&CHARGE_HDR=");
		request.append("&CHARGE_CVV=");
		
		OutputStream oStream = connection.getOutputStream();
		oStream.write(request.toString().getBytes());
		oStream.close();	
    	
    	return ((String) getResults(connection).get(0).get("SUC"));	
    }
    
    // used in MyAccountController
    // Read only
	public Map<String, Object> GetClientCredit(String EveClientID) {
		Map<String, Object> map = new HashMap<String, Object>();
		
		try {
			URL url = new URL("http://www.myevergreen.com/rti/serviceii.asmx/GetClientCredit");
			HttpURLConnection connection = (HttpURLConnection) url.openConnection();

			// Setup HTTP POST parameters
			connection.setDoOutput(true);
			connection.setDoInput(true);
			connection.setUseCaches(false);
			
			// POST data
			StringBuffer request = new StringBuffer("EveClientID=" + EveClientID);
			
			OutputStream oStream = connection.getOutputStream();
			oStream.write(request.toString().getBytes());
			oStream.close();
		
			map = getResults(connection).get(0);

		} catch (Exception e) {
			return null;
		}
		
		return map;
	}    
	
	public List<Map<String, Object>> GetClientOpenOrderFulfillRate(String EveClientID) {
		List<Map<String, Object>> list = new ArrayList<Map<String, Object>>();
		
		try {
			URL url = new URL("http://www.myevergreen.com/rti/serviceii.asmx/GetClientOpenOrderFulfillRate");
			HttpURLConnection connection = (HttpURLConnection) url.openConnection();

			// Setup HTTP POST parameters
			connection.setDoOutput(true);
			connection.setDoInput(true);
			connection.setUseCaches(false);

			// POST data
			StringBuffer request = new StringBuffer("EveClientID=" + EveClientID);
			
			OutputStream oStream = connection.getOutputStream();
			oStream.write(request.toString().getBytes());
			oStream.close();
		
			list = getResults(connection);

		} catch (Exception e) {
			return null;
		}
		
		return list;
	}	
	
	public List<Map<String, Object>> GetOrderFulfillQty(int EveOrderID) {
		List<Map<String, Object>> list = new ArrayList<Map<String, Object>>();
		
		try {
			URL url = new URL("http://www.myevergreen.com/rti/serviceii.asmx/GetOrderFulfillQty");
			HttpURLConnection connection = (HttpURLConnection) url.openConnection();

			// Setup HTTP POST parameters
			connection.setDoOutput(true);
			connection.setDoInput(true);
			connection.setUseCaches(false);

			// POST data
			StringBuffer request = new StringBuffer("EveOrderID=" + EveOrderID);
			
			OutputStream oStream = connection.getOutputStream();
			oStream.write(request.toString().getBytes());
			oStream.close();
		
			list = getResults(connection);

		} catch (Exception e) {
			return null;
		}
		
		return list;
	}	
	
	public Map<String, Object> GetOrderFulfillRate(int EveOrderID) {
		Map<String, Object> map = new HashMap<String, Object>();
		
		try {
			URL url = new URL("http://www.myevergreen.com/rti/serviceii.asmx/GetOrderFulfillRate");
			HttpURLConnection connection = (HttpURLConnection) url.openConnection();

			// Setup HTTP POST parameters
			connection.setDoOutput(true);
			connection.setDoInput(true);
			connection.setUseCaches(false);

			// POST data
			StringBuffer request = new StringBuffer("EveOrderID=" + EveOrderID);
			
			OutputStream oStream = connection.getOutputStream();
			oStream.write(request.toString().getBytes());
			oStream.close();
		
			map = getResults(connection).get(0);

		} catch (Exception e) {
			return null;
		}
		
		return map;
	}	

	public List<Map<String, Object>> GetOrderHeaderWithStatus(String EveClientID) {
		List<Map<String, Object>> list = new ArrayList<Map<String, Object>>();
		
		try {
			URL url = new URL("http://www.myevergreen.com/rti/serviceii.asmx/GetOrderHeaderWithStatus");
			HttpURLConnection connection = (HttpURLConnection) url.openConnection();

			// Setup HTTP POST parameters
			connection.setDoOutput(true);
			connection.setDoInput(true);
			connection.setUseCaches(false);

			// POST data
			StringBuffer request = new StringBuffer("EveClientID=" + EveClientID);
			
			OutputStream oStream = connection.getOutputStream();
			oStream.write(request.toString().getBytes());
			oStream.close();
		
			list = getResults(connection);

		} catch (Exception e) {
			return null;
		}
		
		return list;
	}		
	
	
    public String SetCreditInfo(Order order, Customer customer) throws Exception {
    	
		URL url = new URL("http://www.myevergreen.com/rti/serviceii.asmx/SetCreditInfo");
		HttpURLConnection connection = (HttpURLConnection) url.openConnection();

		// Setup HTTP POST parameters
		connection.setDoOutput(true);
		connection.setDoInput(true);
		connection.setUseCaches(false);

		// POST data
		StringBuffer request = new StringBuffer();
		int accountNumber = 0;
		try {
			accountNumber = Integer.parseInt(customer.getAccountNumber());
		} catch (NumberFormatException e) {
			// do nothing
		}
		request.append("ClientID=" + accountNumber);
		request.append("&OrderID=" + order.getOrderId());
		request.append("&IsNewAddr=0");		
		request.append("&FirstName=");		
		request.append("&MI=");		
		request.append("&LastName=");	
		request.append("&Suffix=");
		request.append("&Street1=");
		request.append("&Street2=");
		request.append("&City=");
		request.append("&State=");
		request.append("&ZipCode=");
		request.append("&Country=");
		request.append("&CardType=" + order.getCreditCard().getType());
		request.append("&CardNumber=" + order.getCreditCard().getNumber());
		request.append("&CardVerCode=" + URLEncoder.encode(order.getCreditCard().getCardCode(), "UTF-8"));
		request.append("&CardExpMM=" + order.getCreditCard().getExpireMonth());
		request.append("&CardExpYY=" + order.getCreditCard().getExpireYear().substring(2));
		
		OutputStream oStream = connection.getOutputStream();
		oStream.write(request.toString().getBytes());
		oStream.close();	
    	
    	return ((String) getResults(connection).get(0).get("SUC"));	
    }
    
	public List<Map<String, Object>> GetClaimHeader(String EveClientID) {
		List<Map<String, Object>> list = new ArrayList<Map<String, Object>>();
		
		try {
			URL url = new URL("http://www.myevergreen.com/rti/serviceii.asmx/GetClaimHeader");
			HttpURLConnection connection = (HttpURLConnection) url.openConnection();

			// Setup HTTP POST parameters
			connection.setDoOutput(true);
			connection.setDoInput(true);
			connection.setUseCaches(false);

			// POST data
			StringBuffer request = new StringBuffer("EveClientID=" + EveClientID);
			
			OutputStream oStream = connection.getOutputStream();
			oStream.write(request.toString().getBytes());
			oStream.close();
		
			list = getResults(connection);

		} catch (Exception e) {
			return null;
		}
		
		return list;
	}		
	
	public List<Map<String, Object>> GetClaimDetail(int EveClaimID) {
		List<Map<String, Object>> list = new ArrayList<Map<String, Object>>();
		
		try {
			URL url = new URL("http://www.myevergreen.com/rti/serviceii.asmx/GetClaimDetail");
			HttpURLConnection connection = (HttpURLConnection) url.openConnection();

			// Setup HTTP POST parameters
			connection.setDoOutput(true);
			connection.setDoInput(true);
			connection.setUseCaches(false);

			// POST data
			StringBuffer request = new StringBuffer("EveClaimID=" + EveClaimID);
			
			OutputStream oStream = connection.getOutputStream();
			oStream.write(request.toString().getBytes());
			oStream.close();
		
			list = getResults(connection);

		} catch (Exception e) {
			return null;
		}
		
		return list;
	}	
	
    public String SetClaimDetail(ClaimDetail claimDetail, boolean debug) throws Exception {
    	
		URL url = new URL("http://www.myevergreen.com/rti/serviceii.asmx/SetClaimDetail");
		HttpURLConnection connection = (HttpURLConnection) url.openConnection();

		// Setup HTTP POST parameters
		connection.setDoOutput(true);
		connection.setDoInput(true);
		connection.setUseCaches(false);

		// POST data
		StringBuffer request = new StringBuffer();
		request.append("AEClaimID=" + claimDetail.getAeClaimID());
		request.append("&ClaimSku=" + URLEncoder.encode(claimDetail.getClaimSku(), "UTF-8"));		
		request.append("&ClaimQty=" + claimDetail.getClaimQty());		
		request.append("&ClaimType=" + URLEncoder.encode(claimDetail.getClaimType().toString(), "UTF-8"));		
		
		OutputStream oStream = connection.getOutputStream();
		oStream.write(request.toString().getBytes());
		oStream.close();
		
		if (debug) {
			System.out.println("http://www.myevergreen.com/rti/serviceii.asmx/SetClaimDetail?" + request.toString());			
		}
		
		return ((String) getResults(connection).get(0).get("SUC"));
    }
    
    public String SetClaimHeader(ClaimHeader claimHeader, boolean debug) throws Exception {
    	
		URL url = new URL("http://www.myevergreen.com/rti/serviceii.asmx/SetClaimHeader");
		HttpURLConnection connection = (HttpURLConnection) url.openConnection();

		// Setup HTTP POST parameters
		connection.setDoOutput(true);
		connection.setDoInput(true);
		connection.setUseCaches(false);

		// POST data
		StringBuffer request = new StringBuffer();
		request.append("AEClaimID=" + claimHeader.getAeClaimID());
		request.append("&ClientID=" + URLEncoder.encode(claimHeader.getClientID(), "UTF-8"));		
		request.append("&OrderID=" + URLEncoder.encode(claimHeader.getOrderID(), "UTF-8"));	
		request.append("&ContactName=" + URLEncoder.encode(claimHeader.getContactName(), "UTF-8"));		
		request.append("&ContactPhone=" + URLEncoder.encode(claimHeader.getContactPhone(), "UTF-8"));				
		request.append("&ClaimDesc=" + URLEncoder.encode(claimHeader.getClaimDesc(), "UTF-8"));		
		request.append("&ClaimImage=");		
		
		OutputStream oStream = connection.getOutputStream();
		oStream.write(request.toString().getBytes());
		oStream.close();	
		
		if (debug) {
			System.out.println("http://www.myevergreen.com/rti/serviceii.asmx/SetClaimHeader?" + request.toString());			
		}
		
    	return ((String) getResults(connection).get(0).get("SUC"));	
    }	
}
