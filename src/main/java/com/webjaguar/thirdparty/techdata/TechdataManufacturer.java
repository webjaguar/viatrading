/* Copyright 2009 Advanced E-Media Solutions
 * @author Eduardo Asprec
 * @since 06.28.2009
 */

package com.webjaguar.thirdparty.techdata;

public class TechdataManufacturer {
	
	private String vendCode;
	private String vendName;
	private boolean restricted;
	
	public String getVendCode()
	{
		return vendCode;
	}
	public void setVendCode(String vendCode)
	{
		this.vendCode = vendCode;
	}
	public String getVendName()
	{
		return vendName;
	}
	public void setVendName(String vendName)
	{
		this.vendName = vendName;
	}
	public boolean isRestricted()
	{
		return restricted;
	}
	public void setRestricted(boolean restricted)
	{
		this.restricted = restricted;
	}

}
