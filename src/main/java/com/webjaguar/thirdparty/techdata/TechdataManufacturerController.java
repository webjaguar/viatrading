/* Copyright 2009 Advanced E-Media Solutions
 * @author Eduardo Asprec
 * @since 06.28.2009
 */

package com.webjaguar.thirdparty.techdata;

import java.util.*;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.web.context.support.WebApplicationObjectSupport;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.Controller;

import com.webjaguar.logic.WebJaguarFacade;

public class TechdataManufacturerController extends WebApplicationObjectSupport implements Controller {

	private WebJaguarFacade webJaguar;
	public void setWebJaguar(WebJaguarFacade webJaguar) { this.webJaguar = webJaguar; }
	
	public ModelAndView handleRequest(HttpServletRequest request, HttpServletResponse response) throws Exception {

		Map<String, Object> map = new HashMap<String, Object>();
		
		map.put("manufs", this.webJaguar.getTechdataManufacturers());

		return new ModelAndView("admin/vendors/techdata/manufacturers", map);
	}
}
