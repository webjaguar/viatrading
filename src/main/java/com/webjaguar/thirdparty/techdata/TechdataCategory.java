/* Copyright 2009 Advanced E-Media Solutions
 * @author Eduardo Asprec
 * @since 06.26.2009
 */

package com.webjaguar.thirdparty.techdata;

import java.util.ArrayList;
import java.util.List;

public class TechdataCategory {
	
	private String code;
	private String grp;
	private String cat;
	private String sub;
	private String descr;
	private Integer catId;
	private Double priceChange;
	List<TechdataCategory> subs;
	
	public String getCode()
	{
		return code;
	}
	public void setCode(String code)
	{
		this.code = code;
	}
	public String getGrp()
	{
		return grp;
	}
	public void setGrp(String grp)
	{
		this.grp = grp;
	}
	public String getCat()
	{
		return cat;
	}
	public void setCat(String cat)
	{
		this.cat = cat;
	}
	public String getSub()
	{
		return sub;
	}
	public void setSub(String sub)
	{
		this.sub = sub;
	}
	public String getDescr()
	{
		return descr;
	}
	public void setDescr(String descr)
	{
		this.descr = descr;
	}
	public Integer getCatId()
	{
		return catId;
	}
	public void setCatId(Integer catId)
	{
		this.catId = catId;
	}
	public Double getPriceChange()
	{
		return priceChange;
	}
	public void setPriceChange(Double priceChange)
	{
		this.priceChange = priceChange;
	}
	public List<TechdataCategory> getSubs()
	{
		return subs;
	}
	public void setSubs(List<TechdataCategory> subs)
	{
		this.subs = subs;
	}
	public void addSub(TechdataCategory sub) 
	{
		if (subs == null) subs = new ArrayList<TechdataCategory>();
		subs.add(sub);
	}

}
