/* Copyright 2009 Advanced E-Media Solutions
 * @author Eduardo Asprec
 * @since 06.26.2009
 */

package com.webjaguar.thirdparty.techdata;

import java.util.*;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.web.bind.ServletRequestUtils;
import org.springframework.web.context.support.WebApplicationObjectSupport;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.Controller;

import com.webjaguar.logic.WebJaguarFacade;

public class TechdataCategoryController extends WebApplicationObjectSupport implements Controller {

	private WebJaguarFacade webJaguar;
	public void setWebJaguar(WebJaguarFacade webJaguar) { this.webJaguar = webJaguar; }
	
	public ModelAndView handleRequest(HttpServletRequest request, HttpServletResponse response) throws Exception {

		Map<String, Object> map = new HashMap<String, Object>();
		List<TechdataCategory> categories = new ArrayList<TechdataCategory>();
		
		if (request.getMethod().equals("POST")) {
			for (String code: request.getParameterValues("code")) {		
				TechdataCategory category = new TechdataCategory();
				category.setCode(code);
				try {
					category.setCatId(ServletRequestUtils.getIntParameter(request, code + "_catId"));
				} catch (Exception e) {
					category.setCatId(null);
				}
				try {
					category.setPriceChange(ServletRequestUtils.getDoubleParameter(request, code + "_priceChange"));
				} catch (Exception e) {
					category.setPriceChange(null);
				}
				categories.add(category);
			}
			this.webJaguar.nonTransactionSafeUpdateTechdataCategories(categories);
		}
		
		categories = this.webJaguar.getTechdataCategories();
		
		Map<String, TechdataCategory> categoryMap = new HashMap<String, TechdataCategory>();
		Iterator<TechdataCategory> iter = categories.iterator();
		while (iter.hasNext()) {
			TechdataCategory category = iter.next();
			if (category.getCat().equals("*")) {
				categoryMap.put(category.getGrp(), category);				
			} else if (category.getSub().equals("*")) {
				categoryMap.put(category.getGrp() + category.getCat(), category);
				if (categoryMap.get(category.getGrp()) != null) {
					categoryMap.get(category.getGrp()).addSub(category);					
				}
				iter.remove();
			} else {
				if (categoryMap.get(category.getGrp() + category.getCat()) != null) {
					categoryMap.get(category.getGrp() + category.getCat()).addSub(category);					
				}
				iter.remove();
			}
		}
		
		map.put("categories", categories);

		return new ModelAndView("admin/vendors/techdata/category", map);
	}
}
