package com.webjaguar.thirdparty.lucene;

import java.io.IOException;

import org.apache.lucene.index.AtomicReaderContext;
import org.apache.lucene.queries.CustomScoreProvider;
import org.apache.lucene.queries.CustomScoreQuery;
import org.apache.lucene.search.Query;

public class ViaSpecialQuery extends CustomScoreQuery {
	private Query query;
	private String keywords;
	
    public ViaSpecialQuery(Query query, String keywords) {
        super(query);
        this.query = query;
        this.keywords = keywords;
    }

    protected CustomScoreProvider getCustomScoreProvider(AtomicReaderContext context) throws IOException {
	    return new ViaQueryScoreProvider(context, query, keywords);
	}
}