/* Copyright 2009 Advanced E-Media Solutions
 * @author Eduardo Asprec
 * @since 03.16.2009
 */

package com.webjaguar.thirdparty.lucene;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.lucene.analysis.Analyzer;
import org.apache.lucene.analysis.standard.StandardAnalyzer;
import org.apache.lucene.document.Document;
import org.apache.lucene.index.IndexReader;
import org.apache.lucene.queryparser.classic.ParseException;
import org.apache.lucene.queryparser.classic.QueryParser;
import org.apache.lucene.search.BooleanQuery.TooManyClauses;
import org.apache.lucene.search.Filter;
import org.apache.lucene.search.IndexSearcher;
import org.apache.lucene.search.NumericRangeFilter;
import org.apache.lucene.search.Query;
import org.apache.lucene.search.ScoreDoc;
import org.apache.lucene.search.Sort;
import org.apache.lucene.search.SortField;
import org.apache.lucene.search.TopFieldDocs;
import org.apache.lucene.store.FSDirectory;
import org.apache.lucene.util.Version;
import org.springframework.web.bind.ServletRequestUtils;
import org.springframework.web.context.support.WebApplicationObjectSupport;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.Controller;
import org.springframework.web.util.WebUtils;

import com.webjaguar.logic.WebJaguarFacade;
import com.webjaguar.model.Category;
import com.webjaguar.model.Configuration;
import com.webjaguar.model.Customer;
import com.webjaguar.model.Layout;
import com.webjaguar.model.LuceneProductSearch;
import com.webjaguar.model.Product;
import com.webjaguar.model.ProductField;
import com.webjaguar.web.domain.Constants;



public class LuceneController extends WebApplicationObjectSupport implements Controller {

	private WebJaguarFacade webJaguar;

	public void setWebJaguar(WebJaguarFacade webJaguar) {
		this.webJaguar = webJaguar;
	}

	private IndexSearcher searcher;
	private QueryParser qp;

	public ModelAndView handleRequest(HttpServletRequest request, HttpServletResponse response) throws Exception {

		Map<String, Configuration> siteConfig = (Map<String, Configuration>) request.getAttribute("siteConfig");
		Map<String, Object> gSiteConfig = (Map<String, Object>) request.getAttribute("gSiteConfig");

		Customer customer = this.webJaguar.getCustomerByRequest(request);

		LuceneProductSearch search = getProductSearch(request);

		// protected access
		if (customer != null) {
			search.setProtectedAccess(customer.getProtectedAccess());
		} else {
			search.setProtectedAccess(siteConfig.get("PROTECTED_ACCESS_ANONYMOUS").getValue());
		}
		// check if protected feature is enabled
		int protectedLevels = (Integer) gSiteConfig.get("gPROTECTED");
		if (protectedLevels == 0) {
			search.setProtectedAccess(Constants.FULL_PROTECTED_ACCESS);
		}
		if ((Boolean) gSiteConfig.get("gMASTER_SKU") && Integer.parseInt(siteConfig.get("PARENT_CHILDERN_DISPLAY").getValue()) == 0) {
			search.setMasterSku(true);
		}

		search.setConjunction(siteConfig.get("SEARCH_DEFAULT_KEYWORDS_CONJUNCTION").getValue());

		Map<String, Object> model = new HashMap<String, Object>();
		model.put("mainCategories", this.webJaguar.getMainCategoryLinks(request,null));

		List<Product> results = new ArrayList<Product>();

		boolean showPriceColumn = false;
		boolean showQtyColumn = false;

		// check number of characters
		search.setKeywordsMinChars(Integer.parseInt(siteConfig.get("SEARCH_KEYWORDS_MINIMUM_CHARACTERS").getValue()));
		boolean minCharsMet = false;
		if (search.getKeywords().trim().length() > 0) {
			for (String token : search.getKeywords().split(" ")) {
				if (token.length() >= search.getKeywordsMinChars()) {
					minCharsMet = true;
				}
			}
		} else if (search.getKeywordsMinChars() == 0) {
			minCharsMet = true;
		}
		model.put("minCharsMet", minCharsMet);

		// default proximity set to 50. If it is greater than 100, set it back to 50
		Integer proximity = Integer.parseInt(siteConfig.get("LUCENE_PROXIMITY_VALUE").getValue());
		if (ServletRequestUtils.getIntParameter(request, "pr") != null) {
			proximity = ServletRequestUtils.getIntParameter(request, "pr", proximity) <= 100 ? ServletRequestUtils.getIntParameter(request, "pr", proximity) : proximity;
		} else if (request.getSession().getAttribute("proximity") != null) {
			proximity = (Integer) request.getSession().getAttribute("proximity");
		}
		request.getSession().setAttribute("proximity", proximity);

		if (search.getKeywords() != null && search.getKeywords().trim().length() > 0 && minCharsMet) {
	
			IndexReader reader = null;
			if (siteConfig.get("SITE_URL").getValue().contains("test.wjserver430.com")) {
				reader = IndexReader.open(FSDirectory.open(new File("/usr/java/webjaguar/test.com/ROOT/lucene_index/products")));
			} else {
				reader = IndexReader.open(FSDirectory.open(new File(getServletContext().getRealPath("/lucene_index/products"))));
			}
			
	
			searcher = new IndexSearcher(reader);

			SortField sfSearchRank = null;
			if (siteConfig.get("SEARCH_FIELDS").getValue().contains("search_rank")) {
				sfSearchRank = new SortField("search_rank", SortField.Type.INT, false);
			}
			// String sortBy = ServletRequestUtils.getStringParameter(request, "sortBy", "search_rank");

			SortField sfName = new SortField("name", SortField.Type.STRING, false);

			Sort sort = new Sort();
			if (sfSearchRank != null) {
				// sort by search rank and name
				sort = new Sort(sfSearchRank, sfName);
			} else {
				// sort by name
				sort = new Sort(sfName);
			}

			StringBuffer keywords = new StringBuffer();

			Analyzer analyzer = new StandardAnalyzer(Version.LUCENE_34);
			qp = new QueryParser(Version.LUCENE_34, "keywords", analyzer);

			// Use || to search multiple search phrases
			// For example, /lsearch.jhtm?keywords=Red+Pen||Red+Pencil will search for "Red Pen" OR "Red Pencil"
			String[] searchPhrases = search.getKeywords().split("\\|\\|");

			// search fields
			if (siteConfig.get("LUCENE_GROUP_FIELDS_INTO_ONE").getValue().equals("true")) {
				keywords.append("( \"" + searchPhrases[0] + "\"~" + proximity);
				for (String searchPhrase : searchPhrases) {
					keywords.append(" OR all_fields_data :\"" + searchPhrase + "\"~" + proximity);
				}
			} else {
				keywords.append("( \"" + searchPhrases[0] + "\"~" + proximity);
				for (String searchPhrase : searchPhrases) {
					keywords.append(" OR name :\"" + searchPhrase + "\"~" + proximity);
				}

				String[] searchFieldsList = siteConfig.get("SEARCH_FIELDS").getValue() != null ? siteConfig.get("SEARCH_FIELDS").getValue().split(",") : null;
				if (searchFieldsList != null) {
					for (String field : searchFieldsList) {
						if (!field.isEmpty() && !(field.equalsIgnoreCase("id") || field.equalsIgnoreCase("name") || field.equalsIgnoreCase("search_rank"))) {
							for (String searchPhrase : searchPhrases) {
								keywords.append(" OR " + field + " :\"" + searchPhrase + "\"~" + proximity);
							}
						}
					}
				}
			}

			if (siteConfig.get("PREMIER_DISTRIBUTOR").getValue().equals("true") || true) {
				if(ServletRequestUtils.getStringParameter(request, "madeIn") != null) {
					keywords.append(" AND (all_fields_data : \"U.S.A.\" OR all_fields_data : \"USA\")" );
				}
				
				if(ServletRequestUtils.getStringParameter(request, "eco") != null) {
					keywords.append(" AND (all_fields_data : \"eco\")" );
				}
				
				if(ServletRequestUtils.getStringParameter(request, "s") != null && !ServletRequestUtils.getStringParameter(request, "s").trim().isEmpty()){
					keywords.append(" AND (account_number : \""+ServletRequestUtils.getStringParameter(request, "s")+"\")" );
				}
				
			}
			
			// complete query bracket
			keywords.append(")");


			
			// min & max price
			Double minPrice = ServletRequestUtils.getDoubleParameter(request, "minPrice", -1.0);
			Double maxPrice = ServletRequestUtils.getDoubleParameter(request, "maxPrice", -1.0);

			List<Filter> filtersList = null;
			List<Integer> logicList = null;
			Filter[] filterArray = null;
			int[] logicArray = null;
			Filter minPriceFilter = null;
			Filter maxPriceFilter = null;

			
			if (minPrice != -1.0 && maxPrice == -1.0) {
				maxPriceFilter = NumericRangeFilter.newDoubleRange("max_price", minPrice, Double.MAX_VALUE, true, true);
				filtersList = new ArrayList<Filter>();
				filtersList.add(maxPriceFilter);
			}
			if (minPrice == -1.0 && maxPrice != -1.0) {
				minPriceFilter = NumericRangeFilter.newDoubleRange("min_price", Double.MIN_VALUE, maxPrice, true, true);
				filtersList = new ArrayList<Filter>();
				filtersList.add(minPriceFilter);
			}
			
			// Minimum Quantity Filter
			Filter minQtyFilter = null;
			int minQty = ServletRequestUtils.getIntParameter(request, "minQty", 0);
			if(minQty > 0){
				
				minQtyFilter = NumericRangeFilter.newIntRange("min_quantity", 1, minQty, true, true);
				
				if(filtersList != null) {
					//System.out.println("FilterList Not Null");
					filtersList.add(minQtyFilter);
					if(logicList != null) {
						logicList.add(1);
					} else {
						logicList = new ArrayList<Integer>();
						logicList.add(1);
						logicList.add(1);
					}
				} else {
					//System.out.println("FilterList Null");
					filtersList = new ArrayList<Filter>();
					filtersList.add(minQtyFilter);
				}
			}
			
			// convert to array of filters and 
			if(filtersList != null) {
				filterArray = new Filter[filtersList.size()];
				for(int i=0; i<filtersList.size(); i++){
					//System.out.println("Adding Filter");
					filterArray[i] = filtersList.get(i);
				}
			}
			if(logicList != null) {
				logicArray = new int[logicList.size()];
				for(int i=0; i<logicList.size(); i++){
					//System.out.println("Adding Logic");
					logicArray[i] = logicList.get(i);
				}
			}
			
			

			if(filterArray != null && logicArray != null) {
				//System.out.println("Adding to Chain");
			//	chainFilter = new ChainedFilter(filterArray, logicArray);
			} else if(filterArray != null && logicArray == null){
				//System.out.println("Adding to Chain without logic");
			//	chainFilter = new ChainedFilter(filterArray);
			}
				
		
			//System.out.println("Search query " + keywords.toString());

			Query query = null;
			TopFieldDocs docs = null;
			try {
				query = qp.parse(keywords.toString());
				docs = searcher.search(query, null, 10000, sort);
			} catch (ParseException e) {
				model.put("tooManyClauses", true);
			} catch (TooManyClauses e) {
				model.put("tooManyClauses", true);
			}
			if (docs != null) {
				ScoreDoc[] hits = docs.scoreDocs;
				
				int count = hits.length;
				int start = (search.getPage() - 1) * search.getPageSize();
				if (start > count) {
					search.setPage(1);
					start = 0;
				}
				int end = start + search.getPageSize();
				if (end > count) {
					end = count;
				}

				int pageCount = count / search.getPageSize();
				if (count % search.getPageSize() > 0) {
					pageCount++;
				}

				model.put("start", start);
				model.put("pageCount", pageCount);
				model.put("count", count);

				for (int i = start; i < end; i++) {
					Document doc = searcher.doc(hits[i].doc);
					Product product = this.webJaguar.getProductById(new Integer(doc.get("id")), request);
					if (product != null) {

						if (search.isMasterSku() && product.getMasterSku() != null && !product.getMasterSku().isEmpty()) {
							// Don't add Child product to result
						} else {
							results.add(product);
						}

						if (!showPriceColumn && ((product.getPrice() != null && !product.getPrice().isEmpty()) || product.getMsrp() != null)) {
							showPriceColumn = true;
						}
						if (!showQtyColumn && ((product.getPrice() != null && !product.getPrice().isEmpty()) && (!product.isLoginRequire() || (product.isLoginRequire() && customer != null)))) {
							showQtyColumn = true;
						}

						// master sku
						if ((Boolean) gSiteConfig.get("gMASTER_SKU")) {
							this.webJaguar.setMasterSkuDetails(product, search.getProtectedAccess(), (Boolean) gSiteConfig.get("gINVENTORY"));
							if (product.getSlaveCount() != null && !showPriceColumn) {
								showPriceColumn = true;
							}
						}
					}
				}
				model.put("pageEnd", start + results.size());
			}
		}

		model.put("showPriceColumn", showPriceColumn);
		model.put("showQtyColumn", showQtyColumn);

		if (siteConfig.get("SEARCH_DISPLAY_MODE").getValue().equals("quick2")) {
			model.put("productFields", this.webJaguar.getProductFields(request, results, false));
		} else {
			model.put("productFields", this.webJaguar.getProductFields(request, results, true));
		}

		// bread crumbs
		if (request.getParameter("cid") != null && !request.getParameter("cid").isEmpty()) {
			List<Category> breadCrumb = this.webJaguar.getCategoryTree(ServletRequestUtils.getIntParameter(request, "cid"), true, request);
			model.put("breadCrumbs", breadCrumb);
			model.put("cid", ServletRequestUtils.getIntParameter(request, "cid"));

			String protectedAccess = "0";
			// check if protected feature is enabled

			if (protectedLevels == 0) {
				protectedAccess = Constants.FULL_PROTECTED_ACCESS;
			} else {
				if (customer != null)  {
					protectedAccess = customer.getProtectedAccess();
				} else {
					protectedAccess = siteConfig.get("PROTECTED_ACCESS_ANONYMOUS").getValue();
				}
			}
			Integer categoryId = ServletRequestUtils.getIntParameter(request, "cid", -1);
			Category thisCategory = this.webJaguar.getCategoryById(categoryId, protectedAccess);
			
			Layout productSearchLayout = this.webJaguar.getSystemLayout("productSearch", request.getHeader("host"), request);
			productSearchLayout.setHeaderHtml(thisCategory.getHtmlCode());
			productSearchLayout.setFooterHtml(thisCategory.getFooterHtmlCode());
			model.put("productSearchLayout", productSearchLayout);
			
		} else {
			Layout productSearchLayout = this.webJaguar.getSystemLayout("productSearch", request.getHeader("host"), request);
			model.put("productSearchLayout", productSearchLayout);
		}

		model.put("results", results);
		
		if(siteConfig.get("TEMPLATE").getValue().equalsIgnoreCase("template6")) {
			return new ModelAndView("frontend/layout/template6/lucene/search", "model", model);
		} else {
			return new ModelAndView("frontend/lucene/search", "model", model);
		}

	}

	private LuceneProductSearch getProductSearch(HttpServletRequest request) {
		LuceneProductSearch search = (LuceneProductSearch) WebUtils.getSessionAttribute(request, "lProductSearch");
		if (search == null) {
			Map<String, Configuration> siteConfig = (Map) request.getAttribute("siteConfig");
			search = new LuceneProductSearch();
			search.setSort(siteConfig.get("SEARCH_SORT").getValue());
			search.setPageSize(new Integer(siteConfig.get("NUMBER_PRODUCT_PER_PAGE_SEARCH").getValue()));
			request.getSession().setAttribute("lProductSearch", search);
		}

		if (request.getParameter("keywords") != null) {
			search.setKeywords(ServletRequestUtils.getStringParameter(request, "keywords", "").replace("*", ""));
		}

		// page
		if (request.getParameter("page") != null) {
			int page = ServletRequestUtils.getIntParameter(request, "page", 1);
			if (page < 1) {
				search.setPage(1);
			} else {
				search.setPage(page);
			}
		}

		// size
		if (request.getParameter("size") != null) {
			int size = ServletRequestUtils.getIntParameter(request, "size", 10);
			if (size < 10) {
				search.setPageSize(10);
			} else {
				search.setPageSize(size);
			}
		}

		// product fields
		int gPRODUCT_FIELDS = (Integer) ((Map<String, Object>) request.getAttribute("gSiteConfig")).get("gPRODUCT_FIELDS");
		Map<String, ProductField> productFieldMap = new HashMap<String, ProductField>();
		boolean setNewMapping = false;
		for (int i = 1; i <= gPRODUCT_FIELDS; i++) {
			if (request.getParameter("field_" + i) != null) {
				setNewMapping = true;
				ProductField pField = new ProductField(i, request.getParameter("field_" + i));
				productFieldMap.put("field_" + i, pField);
			}
		}
		
		if (setNewMapping) {
			search.setProductFieldMap(productFieldMap);
		}
		if (search.getProductFieldMap() != null) {
			search.setProductField(new ArrayList<ProductField>());
			search.getProductField().addAll(search.getProductFieldMap().values());
		}
		Map<String, String> extraFieldMap = new HashMap<String, String>();
		if (request.getParameter("madeIn") != null) {
			extraFieldMap.put("madeIn", request.getParameter("madeIn"));
		}
		if (request.getParameter("eco") != null) {
			extraFieldMap.put("eco", request.getParameter("eco"));
		}
		search.setExtraFieldMap(extraFieldMap);
		
		// min price
		if (request.getParameter("minPrice") != null && !request.getParameter("minPrice").trim().isEmpty()) {
			search.setMinPrice(Double.parseDouble(request.getParameter("minPrice")));
		} else {
			search.setMinPrice(null);
		}
		// max price
		if (request.getParameter("maxPrice") != null && !request.getParameter("maxPrice").trim().isEmpty()) {
			search.setMaxPrice(Double.parseDouble(request.getParameter("maxPrice")));
		} else {
			search.setMaxPrice(null);
		}
		// min price
		if (request.getParameter("minQty") != null && !request.getParameter("minQty").trim().isEmpty()) {
			search.setMinQty(Integer.parseInt(request.getParameter("minQty")));
		} else {
			search.setMinQty(null);
		}
		// supplier account number
		if (request.getParameter("s") != null && !request.getParameter("s").trim().isEmpty()) {
			search.setSupplierAccountNumber(request.getParameter("s"));
		} else {
			search.setSupplierAccountNumber(null);
		}
						
		return search;
	}

}
